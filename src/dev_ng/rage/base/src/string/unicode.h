//
// string/unicode.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef STRING_UNICODE_H
#define STRING_UNICODE_H

#include "string.h"

#include "system/alloca.h"
#include "system/new.h"

#include <wchar.h>

#define __UNICODE 1

namespace rage
{
	typedef u16	char16;
}

#define _C16(x) (reinterpret_cast<const rage::char16*>(L##x))

#define _TO_W(x) reinterpret_cast<wchar_t*>(x)
#define _TO_CW(x) reinterpret_cast<const wchar_t*>(x)
#define _TO_C16(x) reinterpret_cast<rage::char16*>(x)
#define _TO_CC16(x) reinterpret_cast<const rage::char16*>(x)

	inline size_t				wcslen(const rage::char16* s) {return ::wcslen(_TO_CW(s));}
	inline rage::char16*		wcsncpy(rage::char16* dest, const rage::char16* src, size_t count) {return _TO_C16(::wcsncpy(_TO_W(dest), _TO_CW(src), count));}
	inline int					wcscmp(const rage::char16* a, const rage::char16* b) {return ::wcscmp(_TO_CW(a), _TO_CW(b));}
#if __WIN32
	inline int					_wcsicmp(const rage::char16* a, const rage::char16* b) {return ::_wcsicmp(_TO_CW(a), _TO_CW(b));}
#endif
	inline rage::char16*		wcscpy(rage::char16* dest, const rage::char16* src) {return _TO_C16(::wcscpy(_TO_W(dest), _TO_CW(src)));}
	inline int					wcsncmp(const rage::char16* a, const rage::char16* b, size_t count) {return ::wcsncmp(_TO_CW(a), _TO_CW(b), count);}
	inline const rage::char16*	wcschr(const rage::char16* a, rage::char16 b) {return _TO_CC16(::wcschr(_TO_CW(a), static_cast<wchar_t>(b)));}
	inline rage::char16*		wcschr(rage::char16* a, rage::char16 b) {return _TO_C16(::wcschr(_TO_W(a), static_cast<wchar_t>(b)));}
	inline long					wcstol(const rage::char16* a, rage::char16** b, int base) {return ::wcstol(_TO_CW(a), reinterpret_cast<wchar_t**>(b), base);}
	inline const rage::char16*	wcsstr(const rage::char16* a, const rage::char16* b) {return _TO_CC16(::wcsstr(_TO_CW(a), _TO_CW(b)));}

	namespace rage
	{
		inline rage::char16*		vformatf(rage::char16* dest, size_t maxLen, const rage::char16* fmt, va_list args) {return _TO_C16( rage::vformatf( _TO_W(dest), maxLen, _TO_CW(fmt), args) );}
	}
	// Add more as needed

#undef _TO_CC16
#undef _TO_C16
#undef _TO_CW
#undef _TO_W

namespace rage
{
	// PURPOSE: Copies a wide string (using new[])
	char16 *WideStringDuplicate(const char16 *str);

	// PURPOSE: Returns the number of characters in this string (not the same as strlen, that counts bytes)
	int CountUtf8Characters(const char* str);

	// PURPOSE: Returns the number of UTF8 bytes needed to represent this string
	int CountUtf8Bytes(const char16* str);

	// PURPOSE:
	// Helper function for converting a char16 string to a char string that is UTF-8 encoded.  This is used by the W_TO_UTF8 macro.
	// NOTES:
	// Be aware that this may output up to 3 characters per input unicode character.
	// It is not inlined due to size of function
	char *WideToUtf8(char *out,const char16 *in,int length);
	char *WideToUtf8(char *out,const char16 *in,int length, int outBufSize, int* numConverted);

	// PURPOSE:
	// Helper function for converting a char string that is UTF-8 encoded to a char16 string. This is used by the UTF8_TO_W macro.
	char16* Utf8ToWide(char16* out, const char* in, int length);


	// PURPOSE:
	// Converts a single UTF-16 character to UTF-8, returns the new number of bytes the character occupies. out must have space for
	// at least 3 bytes.
	int WideToUtf8Char(char16 in, char* out);

	// PURPOSE:
	// Converts a single UTF-8 character (taking 1 to 3 bytes) into a UTF-16 character. Returns the number of bytes
	// the UTF-8 character used.
	int Utf8ToWideChar(const char* in, char16& out);

	// PURPOSE:
	// Truncates invalid UTF-8 surrogate chars at the end of a string. This can happen when if a string was copied to a smaller buffer
	// and surrogate pairs were split which leads to malformed UTF-8 strings.
	// RETURNS:
	// The number of chars removed or 0 if there are no problems with the string.
	// NOTES:
	// String must be NULL terminated and must be a UTF-8 string (NOT extended ASCII). This alters the passed in string.
	int Utf8RemoveInvalidSurrogates(char* in);


	// PURPOSE:
	// Any function that is going to use A2W, W2A, UTF8_TO_WIDE or WIDE_TO_UTF8 should also have this appear once in the function.  
	// It simply declares a temporary variable for them to use.
	#ifdef USES_CONVERSION
	#undef USES_CONVERSION
	#endif

	#define USES_CONVERSION int _convert=0;

	// PURPOSE:
	// This macro uses Alloca and Utf8ToWide to convert a char string to a char16 string on the stack.
	// NOTES:
	// Because the result is on the stack, it's important not to hold onto the result of this macro after the function
	// returns, or to use it as a return value for a function.
	#ifdef UTF8_TO_WIDE
	#undef UTF8_TO_WIDE
	#endif

	#define UTF8_TO_WIDE(lpa) (\
		((char*)lpa == NULL ? NULL : (\
			_convert = ((int)strlen(lpa)+1),\
			::rage::Utf8ToWide(Alloca(::rage::char16, _convert), lpa, _convert))\
		)\
	)
	
	// PURPOSE:
	// This macro uses Alloca and WideToUtf8 to convert a char16 string to a char string on the stack.
	// NOTES:
	// Because the result is on the stack, it's important not to hold onto the result of this macro after the function
	// returns, or to use it as a return value for a function.
	#ifdef WIDE_TO_UTF8
	#undef WIDE_TO_UTF8
	#endif

	#define WIDE_TO_UTF8(lpw) (\
		((char*)lpw == NULL) ? NULL : (\
			_convert = 3*((int)wcslen(lpw)+1),\
			::rage::WideToUtf8(Alloca(char, _convert), lpw, _convert)\
		)\
	)


	// PURPOSE:
	// This macro uses alloca and A2WHelper() to convert a char string to a char16 string on the stack.  
	// NOTES:
	// Because the result is on the stack, it's important not to hold onto the result of this macro after the function
	// returns, or to use it as a return value for a function.
	#ifdef A2W
	#undef A2W
	#endif

	#define A2W(lpa) (\
		((char*)lpa == NULL) ? NULL : (\
			_convert = ((int)strlen(lpa)+1),\
			::rage::AsciiToWide(Alloca(::rage::char16, _convert), lpa, _convert)\
		)\
	)

	// PURPOSE:
	// This macro uses alloca and W2AHelper() to convert a char16 string to a char string on the stack.  
	// NOTES:
	// Because the result is on the stack, it's important not to hold onto the result of this macro after the function
	// returns, or to use it as a return value for a function.
	#ifdef W2A
	#undef W2A
	#endif

	#define W2A(lpw) (\
		((char*)lpw == NULL) ? NULL : (\
			_convert = ((int)wcslen(lpw)+1),\
			::rage::WideToAscii(Alloca(char, _convert), lpw, _convert)\
		)\
	)


	// PURPOSE: Helper function for converting an ascii char string to a char16 string.  This is used by the A2W macro.
	// NOTES: Do not use if the source string can contain non-ASCII characters. This does NOT NULL-terminate the out
	// string unless you pass strlen+1 for the size.
	inline char16 *AsciiToWide(char16 *out,const char *in,int length)
	{
		for (int i=0;i<length;i++)
		{
			Assertf(((u8)in[i]) < 128, "String %s has non-ASCII characters (char %d = 0x%x). Use Utf8ToWide instead of AsciiToWide", in, i, in[i]);
			out[i]=char16(char16(in[i]) & 0xff); //lint !e571
			if (in[i] == 0)
			break;
		}
		return out;
	}

	// PURPOSE: Helper function for converting a char16 string to an ascii char string.  This is used by the W2A macro.
	// NOTES: Do not use if the source string can contain non-ASCII characters. This does NOT NULL-terminate the out
	// string unless you pass strlen+1 for the size.
	inline char *WideToAscii(char *out,const char16 *in,int length)
	{
		for (int i=0;i<length;i++)
		{
#if __WIN32
			Assertf(in[i] < 128, "String %w has non-ASCII characters (char %d = 0x%x). Use WideToUtf8 instead of WideToAscii", in, i, in[i]);
#endif
			out[i]=char(in[i]&0xff);
			if (in[i] == 0)
				break;
		}
		return out;
	}

	// If this assert ever fails, we need to define our own versions of wcscmp, wcslen, etc. that work with char16s
	CompileTimeAssert(sizeof(wchar_t) == sizeof(rage::char16));


}	// namespace rage

#endif
