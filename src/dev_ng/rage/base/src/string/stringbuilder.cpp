// 
// data/stringbuilder.cpp 
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#include "stringbuilder.h"

#include <string.h>

namespace rage
{

atStringBuilder::atStringBuilder()
{
    this->Init(NULL, 0);
}

atStringBuilder::atStringBuilder(const unsigned flags)
{
    this->Init(NULL, flags);
}

atStringBuilder::atStringBuilder(sysMemAllocator* allocator)
{
    this->Init(allocator, 0);
}

atStringBuilder::atStringBuilder(char* buf, const unsigned bufLen)
{
    this->Init(buf, bufLen, 0);
}


atStringBuilder::~atStringBuilder()
{
    this->Clear();
}

void
atStringBuilder::Init(char* buffer, const unsigned bufLen, const unsigned flags)
{
    m_DBuf.Init(buffer, bufLen, flags | datGrowBuffer::NULL_TERMINATE);
}

void
atStringBuilder::Init(sysMemAllocator* allocator, const unsigned flags)
{
    m_DBuf.Init(allocator, flags | datGrowBuffer::NULL_TERMINATE);
}

void
atStringBuilder::Clear()
{
    m_DBuf.Clear();
}

unsigned
atStringBuilder::GetFlags() const
{
    return (m_DBuf.GetFlags() & datGrowBuffer::NULL_BUFFER) ? NULL_BUFFER : 0;
}

bool
atStringBuilder::IsGrowable() const
{
    return m_DBuf.IsGrowable();
}

bool
atStringBuilder::Preallocate(const unsigned newCapacity)
{
	return m_DBuf.Preallocate(newCapacity);
}

int
atStringBuilder::Append(const char* str, const unsigned len)
{
    return len ? m_DBuf.Append(str, len) : 0;
}

int
atStringBuilder::Append(const char* str)
{
    const unsigned len = unsigned(str ? strlen(str) : 0);
    return this->Append(str, len);
}

int
atStringBuilder::Append(const char c)
{
    return this->Append(&c, 1);
}

bool
atStringBuilder::AppendOrFail(const char* str, const unsigned len)
{
    return len ? m_DBuf.AppendOrFail(str, len) : true;
}

bool
atStringBuilder::AppendOrFail(const char* str)
{
    const unsigned len = unsigned(str ? strlen(str) : 0);
    return this->AppendOrFail(str, len);
}

bool
atStringBuilder::AppendOrFail(const char c)
{
    return this->AppendOrFail(&c, 1);
}

int
atStringBuilder::Prepend(const char* str, const unsigned len)
{
    return len ? m_DBuf.Prepend(str, len) : 0;
}

int
atStringBuilder::Prepend(const char* str)
{
    const unsigned len = unsigned(str ? strlen(str) : 0);
    return this->Prepend(str, len);
}

int
atStringBuilder::Prepend(const char c)
{
    return this->Prepend(&c, 1);
}

bool
atStringBuilder::PrependOrFail(const char* str, const unsigned len)
{
    return len ? m_DBuf.PrependOrFail(str, len) : true;
}

bool
atStringBuilder::PrependOrFail(const char* str)
{
    const unsigned len = unsigned(str ? strlen(str) : 0);
    return this->PrependOrFail(str, len);
}

bool
atStringBuilder::PrependOrFail(const char c)
{
    return this->PrependOrFail(&c, 1);
}

const char*
atStringBuilder::ToString() const
{
    char* str = (char*) m_DBuf.GetBuffer();

    return str ? str : "";
}

void
atStringBuilder::Truncate(const unsigned newLen)
{
    m_DBuf.Truncate(newLen);
}

int
atStringBuilder::IndexOf(const char* str) const
{
    int index = -1;
    const char* myStr = this->ToString();

    if(myStr && str)
    {
        const char* where = strstr(myStr, str);
        if(where)
        {
            index = int(where - myStr);
        }
    }

    return index;
}

bool
atStringBuilder::EndsWith(const char* str) const
{
    const size_t len = strlen(str);
    const unsigned myLen = this->Length();
    if(len <= myLen)
    {
        const char* myStr = this->ToString();
        return !strcmp(str, &myStr[myLen - len]);
    }

    return false;
}

bool
atStringBuilder::StartsWith(const char* str) const
{
    const size_t len = strlen(str);
    const unsigned myLen = this->Length();
    if(len <= myLen)
    {
        const char* myStr = this->ToString();
        return !strncmp(str, myStr, len);
    }

    return false;
}

void
atStringBuilder::Remove(const int from, const unsigned len)
{
    return m_DBuf.Remove(from, len);
}

unsigned
atStringBuilder::Length() const
{
    return m_DBuf.Length();
}

} // namespace rage
