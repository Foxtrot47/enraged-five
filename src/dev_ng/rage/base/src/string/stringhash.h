//
// string/stringhash.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef STRING_STRINGHASH_H
#define STRING_STRINGHASH_H

#include <stddef.h>	// for size_t

namespace rage {

// PURPOSE:	Produce a very robust full 32-bit hash for a string
//			Swat was able to throw several thousand strings at this
//			function with no overlap.  Should be better than storing
//			a full filename, etc, in situations where you know you
//			will not be querying objects that do not exist.
// PARAMS:	string - String to produce hash for; uppercase letters are
//				mapped to lowercase, and if the string starts with
//				a quote it is expected to be terminated with one
//				as well (a NUL always terminates a string even if one
//				is encountered before the closing quote).  Backslashes
//				are also normalized to forward slashes before hashing
//				so that this function is more useful for filenames.
//          initValue - Initial value of the hash.  This has to be a partial hash,
//			Useful for incremental hashing.
// RETURNS:	Full 32 bit hash code for string.
// NOTES:	Some comments from Russ Schaaf: "On a test of all of the .cpp and .h 
//		files in swat, tokenizing via whitespace, there were 781,097 tokens 
//		(180,425 unique) and 24 collisions. Not too bad".  For the actual 
//		in-game use: "We got 0 collisions in 14,826 words"
//<FLAG Component>
u32 atStringHash( const char *string, const u32 initValue = 0 );

// PURPOSE:	Produces the same result as a call to atStringHash with a 
//			string that is the concatination of all passed in strings
// PARAMS:	as many non-null null-terminated strings as is desired to hash
// RETURNS:	Full 32 bit hash code for composite string.
#define atMultiStringHash(...) _atMultiStringHash(__VA_ARGS__, 0)
u32 _atMultiStringHash( const char* first, ... );

// PURPOSE:	Produces a partial hash to be used to combine hash values
//			The final result must be merged with the atStringHash function or FinalizeHash above to be
//			useful.
// PARAMS:	string - String to produce hash for; uppercase letters are
//				mapped to lowercase
//          initValue - Initial value of the hash.  This can be from
//              a previous partial hash.  Useful for incremental hashing.
// RETURNS:	Full 32 bit hash code for string.
u32 atPartialStringHash( const char *string, const u32 initValue = 0);

// PURPOSE: Finalize the supplied partial hash.  This 'avalanches' the final input
//			bits across all output bytes.
// PARAMS:	partialHash - the partial hash value
// RETURNS:	Full 32 bit hash code
inline u32 atFinalizeHash(const u32 partialHashValue)
{
	u32 key = partialHashValue;
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701
	return key;
}

// PURPOSE:	Same as atStringHash, except this accepts a token
//				character which will terminate the loop the same as a
//				NULL terminator.
// PARAMS:	string - String to produce hash for; uppercase letters are
//				mapped to lowercase
//			token - The character you'd like the loop to terminate on
//          initValue - Initial value of the hash.  This can be from
//              a previous partial hash.  Useful for incremental hashing.
// RETURNS:	Full 32 bit hash code for string.
u32 atTokenedStringHash( const char *string, const char token, const u32 initValue = 0 );

// PURPOSE:	Same as atPartialStringHash, except this accepts a token
//				character which will terminate the loop the same as a
//				NULL terminator.
// PARAMS:	string - String to produce hash for; uppercase letters are
//				mapped to lowercase
//			token - The character you'd like the loop to terminate on
//          initValue - Initial value of the hash.  This can be from
//              a previous partial hash.  Useful for incremental hashing.
// RETURNS:	Full 32 bit hash code for string.
u32 atPartialTokenedStringHash( const char *string, const char token, const u32 initValue = 0 );

// PURPOSE:	Produce a very robust full 32-bit hash for a string
// PARAMS:	string - String to produce hash for; Unlike atStringHash
//			this function does not do anything special to the string 
//			like dealing with quotes, mapping upper case to lower,
//			or converting backslashes.
//          initValue - Initial value of the hash.  This can be from
//              a previous hash.  Useful for incremental hashing.
// RETURNS:	Full 32 bit hash code for string.
//<FLAG Component>
u32 atLiteralStringHash( const char *string, const u32 initValue = 0 );

// PURPOSE:	Produce a very robust full 32-bit hash for a block of data
// PARAMS:	data - Data to produce hash for; 
//			size - Size of the input data
//          initValue - Initial value of the hash.  This can be from
//              a previous hash.  Useful for incremental hashing.
// RETURNS:	Full 32 bit hash code for string.
//<FLAG Component>
u32 atDataHash( const char *data, size_t size, const u32 initValue = 0 );

// PURPOSE:	Produce a very robust full 32-bit hash for a block of data
// PARAMS:	data - Data to produce hash for; 
//			size - Size of the input data in bytes
//          initValue - Initial value of the hash.  This can be from
//              a previous hash.  Useful for incremental hashing.
// RETURNS:	Full 32 bit hash code for string.
//<FLAG Component>
u32 atDataHash( const unsigned int *data, size_t size, const u32 initValue = 0 );

// PURPOSE:	Produces a partial hash to be used to combine hash values
//			The final result must be merged with the atDataHash function or FinalizeHash above to be
//			useful.
// PARAMS:	data - Data to produce hash for; 
//			size - Size of the input data in bytes
//          initValue - Initial value of the hash.  This can be from
//              a previous partial hash.  Useful for incremental hashing.
// RETURNS:	Full 32 bit hash code for string.
u32 atPartialDataHash(const char* data, size_t size, const u32 initValue = 0);

// PURPOSE:     Produce a 64-bit hash code from arbitrary data.  Currently uses Google's CityHash64.
extern u64 atDataHash64(const void* data, size_t size);

// PURPOSE:     Produce a 64-bit hash code from arbitrary data, including a supplied seed.  Currently uses Google's CityHash64.
//				Supplying a seed means that the same piece of data will produce two different hash codes.
extern u64 atDataHash64(const void* data, size_t size, u64 seed);

#if __ASSERT && !__SPU && !__XENON
extern u32 atValidateHash(const char *str,u32 hash,const char *file,int line);
#define ATSTRINGHASH(str,code)	rage::atValidateHash(str,code,__FILE__,__LINE__)
extern u32 atValidatePartialHash(const char *str,u32 hash,const char *file,int line);
#define ATPARTIALSTRINGHASH(str,code)	rage::atValidatePartialHash(str,code,__FILE__,__LINE__)
#else
#define ATSTRINGHASH(str,code)	static_cast<rage::u32>(code)
#define ATPARTIALSTRINGHASH(str,code)	static_cast<rage::u32>(code)
#endif

}	// namespace rage

#endif
