//
// string/stringhash.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "stringhash.h"
#include "system/new.h"
#include <stdarg.h>
#include <string.h>

#include "city.h"

namespace rage {

// This g_NormalizeCaseAndSlash table normalizes character case (to lowercase) and backslashes to forward slashes.
// Really only need 128 entries, but I want to guarantee correctness even if weird european characters sneak in.
extern const u8 g_NormalizeCaseAndSlash[256] = {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
	36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 97, 98, 99, 100,
	101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 91, 47, 93, 94, 95,
	96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122,
	123, 124, 125, 126, 127,
	128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143,
	144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
	160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175,
	176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191,
	192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207,
	208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
	224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
	240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255
};

#if 0
u32 hash_and_convert_without_g_NormalizeCaseAndSlash()
{
	// branch-free version that operates on 4 bytes at a time 
	// doesn't cope with quoted strings 
	u32* ptr = (u32*)string; 
	u32 key = initValue; 
	u32 one = 0x1010101; 
	u32 mask = one<<7; 
	u32 toslash = '\\\\\\\\'-'////'; 
	u32 bslash = mask - '\\\\\\\\' 
		u32 a = (one<<6)-one; 
	u32 n4 = *ptr; 
	u32 hasZeroByte = (n4-one)&mask; 
	u32 upper = (((n4+a)&~(n4+bslash+one))&mask)>>2; 
	u32 slash = (n4+bslash)&~(n4+bslash-one)&mask; 
	slash -= slash>>7; 
	n4 += upper-(toslash&slash); 
	while (!hasZeroByte) 
	{ 
		u32 c4 = n4; n4 = *++ptr; 
		hasZeroByte = (n4-one)&mask; 
		upper = (((n4+a)&~(n4+bslash+one))&mask)>>2; 
		slash = (n4+bslash)&~(n4+bslash-one)&mask; 
		slash -= slash>>7; 
		n4 += upper-(toslash&slash); 
		key += c4>>24; key += (key << 10); key ^= (key >> 6); c4 <<= 8; 
		key += c4>>24; key += (key << 10); key ^= (key >> 6); c4 <<= 8; 
		key += c4>>24; key += (key << 10); key ^= (key >> 6); c4 <<= 8; 
		key += c4>>24; key += (key << 10); key ^= (key >> 6); 
	} 
	if (u32 c = n4>>24) 
	{ 
		do {n4 <<= 8; 
		key += c; key += (key << 10); key ^= (key >> 6);                
		} while ((c = n4>>24) != 0); 
	} 
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701
	return key;
}
#endif


__forceinline u32 atPartialStringHash_Inline( const char *string, const u32 initValue )
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)

	// If string is NULL, you're going to crash.
	if (!Verifyf(string,"NULL string passed to atStringHash"))
		return 0;

	bool quotes = (*string == '\"');
	u32 key = initValue;

	if (quotes) 
	{
		string++;
		while (*string && *string != '\"')
		{
			key += g_NormalizeCaseAndSlash[(u8)*string++];
			key += (key << 10);	//lint !e701
			key ^= (key >> 6);	//lint !e702
		}
	}
	else
	{
		// Crazy way to detect a zero byte anywhere in the word
		// Relies on the fact that input doesn't have high bit set anywhere.
		// Worst-case, if it does have weird high-bit characters set, the cleanup
		// loop below will still produce the same answer, but just take longer.
		// Only do this if the string is already aligned; avoids some edge cases
		// with strings near the end of valid memory and also is probably a performance
		// wash when the data is unaligned anyway.
		if (((size_t)string & 3) == 0)	while (!((*(u32*)string - 0x01010101) & 0x80808080))
		{
			key += g_NormalizeCaseAndSlash[(u8)*string++];
			key += (key << 10);	//lint !e701
			key ^= (key >> 6);	//lint !e702
			key += g_NormalizeCaseAndSlash[(u8)*string++];
			key += (key << 10);	//lint !e701
			key ^= (key >> 6);	//lint !e702
			key += g_NormalizeCaseAndSlash[(u8)*string++];
			key += (key << 10);	//lint !e701
			key ^= (key >> 6);	//lint !e702
			key += g_NormalizeCaseAndSlash[(u8)*string++];
			key += (key << 10);	//lint !e701
			key ^= (key >> 6);	//lint !e702
		}
		while (*string)
		{
			key += g_NormalizeCaseAndSlash[(u8)*string++];
			key += (key << 10);	//lint !e701
			key ^= (key >> 6);	//lint !e702
		}
	}
	return key;
}

u32 atPartialStringHash(const char* string, const u32 initValue)
{
	return atPartialStringHash_Inline(string, initValue);
}

u32 _atMultiStringHash( const char* first, ... )
{
	va_list args;
	va_start(args, first);

	const char *final_string = first;

	u32 hash = 0;

	const char* tmp = 0;
	while ((tmp = va_arg(args,const char *))!=NULL)
	{
		hash = atPartialStringHash_Inline(final_string, hash); // guess it wasn't the final one after all
		final_string = tmp; // this one was
	}

	va_end(args);
	return atStringHash(final_string, hash);
}

//extern void Displayf(const char *fmt,...) PRINTF_LIKE_N(1);

u32 atStringHash( const char *string, const u32 initValue /* = 0 */ )
{
	const u32 key = atFinalizeHash(atPartialStringHash_Inline(string, initValue));
	// The original swat code did several tests at this point to make
	// sure that the resulting value was representable as a valid
	// floating-point number (not a negative zero, or a NaN or Inf)
	// and also reserved a nonzero value to indicate a bad key.
	// We don't do this here for generality but the caller could
	// obviously add such checks again in higher-level code.
	return key;
}

u32 atPartialTokenedStringHash( const char *string, const char token, const u32 initValue )
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)

	// If string is NULL, you're going to crash.
	if (!Verifyf(string,"NULL string passed to atStringHash"))
		return 0;

	bool quotes = (*string == '\"');
	u32 key = initValue;

	if (quotes) 
		string++;

	while (*string && *string != token && (!quotes || *string != '\"'))
	{
		char character = *string++;
		if (character >= 'A' && character <= 'Z') 
			character += 'a'-'A';
		else if (character == '\\')
			character = '/';

		key += character;
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}
	return key;
}

u32 atTokenedStringHash( const char *string, const char token, const u32 initValue )
{
	const u32 key = atFinalizeHash(atPartialTokenedStringHash(string, token, initValue));
	// The original swat code did several tests at this point to make
	// sure that the resulting value was representable as a valid
	// floating-point number (not a negative zero, or a NaN or Inf)
	// and also reserved a nonzero value to indicate a bad key.
	// We don't do this here for generality but the caller could
	// obviously add such checks again in higher-level code.
	return key;
}



u32 atLiteralStringHash( const char *string, const u32 initValue )
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)

	u32 key = initValue;

	while (*string)
	{
		key += *string++;
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}

	return atFinalizeHash(key);
}

u32 atPartialDataHash( const char *data, size_t size, const u32 initValue )
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)

	u32 key = initValue;
	for(size_t count = 0; count < size; count++)
	{
		key += data[count];
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}

	return key;
}

u32 atDataHash( const char *data, size_t size, const u32 initValue )
{
	return atFinalizeHash(atPartialDataHash(data, size, initValue));
}

u32 atDataHash( const unsigned int *data, size_t size, const u32 initValue )
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)
	Assert((size % 4) == 0);
	size >>= 2;
	u32 key = initValue;
	for(size_t count = 0; count < size; count++)
	{
		key += data[count];
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}
	
	return atFinalizeHash(key);
}


u64 atDataHash64(const void *s, size_t len) {
	return CityHash64((const char*)s, len);
}

uint64 atDataHash64(const void *s, size_t len,u64 seed) {
	return CityHash64WithSeed((const char*)s, len, seed);
}

#if __ASSERT
u32 atValidateHash(const char *str,u32 hash,const char *file,int line)
{
	u32 check = atStringHash(str);
	Assertf(check == hash,"%s(%d): ATSTRINGHASH(%s,0x%x) macro out of date, second parameter should be 0x%x",file,line,str,hash,check);
	return check;
}
u32 atValidatePartialHash(const char *str,u32 hash, const char *file,int line)
{
	u32 check = atPartialStringHash(str);
	Assertf(check == hash,"%s(%d): ATPARTIALSTRINGHASH(%s,0x%x) macro out of date, second parameter should be 0x%x",file,line,str,hash,check);
	return check;
}
#endif

}	// namespace rage
