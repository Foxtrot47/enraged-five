//
// string/stringutil.cpp
//
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved.
//

#include "string/string.h"
#include "string/stringutil.h"

namespace rage {

static void InitializeCharacterMap(bool (&characterMap)[256], const char* characterArray)
{
	memset(characterMap, 0, sizeof(characterMap));

	for (char const* current = characterArray ; *current != '\0'; ++current)
	{
		characterMap[u8(*current)] = true;
	}
}

// Albert: Thread-safe version of strtok for Prospero, calling strtok on Prospero causes an immediate crash in libc.prx
#if 0
static __thread char *strtokState;
char* safestrtok(char *str, const char* delim) {
	if(str) {
		strtokState = 0;
	}
	return strtok_r(str, delim, &strtokState);
}
#else
char* safestrtok(char *str, const char* delim) { return strtok(str, delim); }
#endif

char* strrpbrk(char* str, const char* pat, char* nomatch)
{
	for (char* s = str + strlen(str) - 1; s >= str; s--)
	{
		if (strchr(pat, *s))
		{
			return s;
		}
	}

	return nomatch;
}

char* strskip(char* str, const char* substr)
{
	const int len = (int)strlen(substr);

	if (strncmp(str, substr, len) == 0)
	{
		return str + len;
	}

	return str;
}

char* striskip(char* str, const char* substr)
{
	const int len = (int)strlen(substr);

	if (strnicmp(str, substr, len) == 0)
	{
		return str + len;
	}

	return str;
}

char* stristr(char* str, const char* substr, char* nomatch)
{
	const int len = (int)strlen(substr);

	for (char* s = str; *s; s++)
	{
		if (strnicmp(s, substr, len) == 0)
		{
			return s;
		}
	}

	return nomatch;
}

char* strrstr(char* str, const char* substr, char* nomatch)
{
	const int len = (int)strlen(substr);

	for (char* s = str + strlen(str) - 1; s >= str; s--)
	{
		if (strncmp(s, substr, len) == 0)
		{
			return s;
		}
	}

	return nomatch;
}

char* strristr(char* str, const char* substr, char* nomatch)
{
	const int len = (int)strlen(substr);

	for (char* s = str + strlen(str) - 1; s >= str; s--)
	{
		if (strnicmp(s, substr, len) == 0)
		{
			return s;
		}
	}

	return nomatch;
}

const char* strrpbrk(const char* str, const char* pat, const char* nomatch)
{
	for (const char* s = str + strlen(str) - 1; s >= str; s--)
	{
		bool bMatch = false;

		for (const char* p = pat; *p; p++)
		{
			if (*s == *p)
			{
				bMatch = true;
				break;
			}
		}

		if (bMatch)
		{
			return s;
		}
	}

	return nomatch;
}

const char* strskip(const char* str, const char* substr)
{
	const int len = (int)strlen(substr);

	if (strncmp(str, substr, len) == 0)
	{
		return str + len;
	}

	return str;
}

const char* striskip(const char* str, const char* substr)
{
	const int len = (int)strlen(substr);

	if (strnicmp(str, substr, len) == 0)
	{
		return str + len;
	}

	return str;
}

const char* stristr(const char* str, const char* substr, const char* nomatch)
{
	const int len = (int)strlen(substr);

	for (const char* s = str; *s; s++)
	{
		if (strnicmp(s, substr, len) == 0)
		{
			return s;
		}
	}

	return nomatch;
}

const char* strrstr(const char* str, const char* substr, const char* nomatch)
{
	const int len = (int)strlen(substr);

	for (const char* s = str + strlen(str) - 1; s >= str; s--)
	{
		if (strncmp(s, substr, len) == 0)
		{
			return s;
		}
	}

	return nomatch;
}

const char* strristr(const char* str, const char* substr, const char* nomatch)
{
	const int len = (int)strlen(substr);

	for (const char* s = str + strlen(str) - 1; s >= str; s--)
	{
		if (strnicmp(s, substr, len) == 0)
		{
			return s;
		}
	}

	return nomatch;
}

// PURPOSE: Compares two strings, one with wild cards (with optional case insensitive comparison)
// RETURNS: false for match, true for mismatch (confusing, but consistent with strcmp)
// NOTES: Supports both '*' wild cards for zero or more characters, 
// and '?' wild cards for exactly one character.
int StringWildcardCompare(const char *wild, const char *string, bool insensitive)
{
	FastAssert(wild && string);

	const char *cp = NULL, *mp = NULL;

	while ((*string) && (*wild != '*')) 
	{
		char wch = *wild;
		char sch = *string;
		if(insensitive)
		{
			if(wch >= 'A' && wch <= 'Z')
			{
				wch += 32;
			}
			if(sch >= 'A' && sch <= 'Z')
			{
				sch += 32;
			}
		}
		if ((wch != sch) && (wch != '?')) 
		{
			return 1;
		}
		wild++;
		string++;
	}

	while (*string) 
	{
		if (*wild == '*') 
		{
			if (!*++wild) 
			{
				return 0;
			}
			mp = wild;
			cp = string+1;
		} 
		else 
		{
			char wch = *wild;
			char sch = *string;
			if(insensitive)
			{
				if(wch >= 'A' && wch <= 'Z')
				{
					wch += 32;
				}
				if(sch >= 'A' && sch <= 'Z')
				{
					sch += 32;
				}
			}
			if ((wch == sch) || (wch == '?')) 
			{
				wild++;
				string++;
			} 
			else 
			{
				wild = mp;
				string = cp++;
			}
		}
	}

	while (*wild == '*') 
	{
		wild++;
	}
	return *wild != 0;
}

char* StringTrim(char* dst, const char* src, const unsigned sizeofDst)
{
    //dst and src could point to the same memory, so be careful
    //when writing to dst.  For example, don't initialize dst like this:
    //dst[0] = '\0';
    if(!src)
    {
        dst[0] = '\0';
        return dst;
    }

    const int len = (int)strlen(src);

    if(0 == len)
    {
        dst[0] = '\0';
        return dst;
    }

    for(int i = 0; i < len && isspace(*src); ++i, ++src)
    {
    }

    safecpy(dst, src, sizeofDst);

    for(int i = (int)strlen(dst) - 1; i >= 0 && isspace(dst[i]); --i)
    {
        dst[i] = '\0';
    }

    return dst;
}

char* StringRTrim(char* dst, const char* src, const int c, const unsigned sizeofDst)
{
    //dst and src could point to the same memory, so be careful
    //when writing to dst.  For example, don't initialize dst like this:
    //dst[0] = '\0';
    if(!src)
    {
        dst[0] = '\0';
        return dst;
    }

    const int len = (int)strlen(src);

    if(0 == len)
    {
        dst[0] = '\0';
        return dst;
    }

    safecpy(dst, src, sizeofDst);

    for(int i = (int)strlen(dst) - 1; i >= 0 && c == dst[i]; --i)
    {
        dst[i] = '\0';
    }

    return dst;
}

bool StringStartsWith(const char* str, const char* substr)
{
    return strstr(str, substr) == str;
}

bool StringStartsWithI(const char* str, const char* substr)
{
    return stristr(str, substr) == str;
}

bool StringEndsWith(const char* str, const char* substr)
{
    return strrstr(str, substr) == (&str[strlen(str)] - strlen(substr));
}

bool StringEndsWithI(const char* str, const char* substr)
{
    return strristr(str, substr) == (&str[strlen(str)] - strlen(substr));
}

bool StringIsEqual(const char* str1, const char* str2, bool insensitive)
{
	if (str1 == nullptr)
		return (str2 == nullptr);
	else if (str2 == nullptr)
		return false;
	else
		return (insensitive ? (stricmp(str1, str2) == 0) : (strcmp(str1, str2) == 0));
}

bool StringNullOrEmpty(const char* str)
{
	return (str == NULL || str[0] == '\0');
}

// Copies src to dst. If src is bigger than dst it will truncate from the start of the string and 
// add 3 dots to the front, e.g. ...yfolder/myfile.txt
// Returns true if it was truncated and false otherwise.
bool StringCopyTruncateFront(char* dst, const char* src, const unsigned int sizeofDst)
{
	unsigned int sizeofSrc = (unsigned int) strlen(src) + 1;

	if(sizeofSrc <= sizeofDst)
	{
		strcpy(dst, src);
		return false;
	}

	const int DOT_COUNT = 3;
	unsigned int srcStartIndex = sizeofSrc + DOT_COUNT - sizeofDst;
	sysMemCpy(dst + DOT_COUNT, src + srcStartIndex, sizeofDst - DOT_COUNT);

	for(unsigned int index = 0 ; index < DOT_COUNT ; ++index)
	{
		dst[ index ] = '.';
	}

	return true;
}

void StringReplaceChars(char* str, const char* charactersToFindArray, char replace)
{
	bool charactersToFindMap[256];

	InitializeCharacterMap(charactersToFindMap, charactersToFindArray);

	for (char* current = str; *current != '\0'; ++current)
	{
		if (charactersToFindMap[u8(*current)] == false)
		{
			continue;
		}

		*current = replace;
	}
}

} // namespace rage

