//
// string/string.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "string.h"
#include "data/resource.h"
#include "data/struct.h"
#include "atl/map.h"

#include <stdio.h>
#include <wchar.h>

#if __WIN32PC && (!__TOOL && !__RESOURCECOMPILER) && (!defined(__RGSC_DLL) || !__RGSC_DLL)
/*
	The Social Club DLL wouldn't link due to the following linker issue: "LINK : warning LNK4052: using .EXP file; ignoring .DEF file".
	This may be a bug in the linker. It prevents our function exports from working and makes the DLL unusable.
	The previous workaround was to enable Whole Program Optimization but our new security software (Arxan) doesn't work with WPO enabled.
	Narrowed down the problem to the "const_seg" #pragma below. This #pragma is now disabled when biulding the Social Club DLL so it can link without WPO.
*/

// Note that .edata is supposedly reserved, but it happens to live immediately between the segments we care about.
#pragma const_seg(".edata")
extern "C" const char _rodata_end[] = "";
#pragma const_seg()
#endif

#if RSG_DURANGO
// Note that .edata is supposedly reserved, but it happens to live immediately between the segments we care about.
// TODO - no idea if this actually works yet!
#pragma const_seg(".edata")
extern "C" const char _rodata_end[] = "";
#pragma const_seg()
#endif

#if __XENON
#pragma const_seg("rodata1")
extern "C" const char _rodata_end[] = "";
#pragma const_seg()
#endif

#if __PS3
// Briefly defined in linker definition file:  
// $CELL_SDK/cell/target/ppu/lib/elf64_lv2_prx.x: Add _rodata_end = .; on new line after:  .rodata1        : { *(.rodata1) }
// But since rodata1 is already there, we'll just use that instead.
#pragma str_seg("rodata1")
extern "C" const char _rodata_end[] = "";
#pragma str_seg("")
#endif

#if RSG_PPU || RSG_ORBIS
extern "C" char *strupr (char *a)
{
	char *ret = a;
	while (*a != '\0')
	{
		if (islower (*a))
			*a = (char)toupper (*a);
		++a;
	}
	return ret;
}
extern "C" char *strlwr (char *a)
{
	char *ret = a;
	while (*a != '\0')
	{
		if (isupper (*a))
			*a = (char)tolower (*a);
		++a;
	}
	return ret;
}
#endif


namespace rage {

void ConstString::vCopy(const char * String)
{
	const char *newString = StringDuplicate(String);
	StringFree(m_String);
	m_String = newString;
}

ConstString::~ConstString()
{
	StringFree(m_String);
}


unsigned atHash(const class ConstString& S) {
	return atHash_const_char(S);
}


ConstString::ConstString(datResource &rsc) {
	rsc.PointerFixup(m_String);
}

#if __DECLARESTRUCT
void ConstString::DeclareStruct(datTypeStruct &s) {
	STRUCT_BEGIN(ConstString);
	STRUCT_FIELD(m_String);
	STRUCT_END();
}
#endif

char* prettyprinter(char *dest, size_t destSize, s64 value )
{
	char temp[64];
	int tl = 0;
	bool negative = value < 0;
	if (negative)
		value = -value;
	do {
		temp[tl++] = (char)(value % 10) + '0';
		value /= 10;
		if (((tl & 3) == 3) && value)
			temp[tl++] = ',';
	} while (value);
	if (negative)
		temp[tl++] = '-';
	char *result = dest;
	while (--destSize && tl)
		*dest++ = temp[--tl];
	*dest = 0;
	return result;
}

}	// namespace rage
