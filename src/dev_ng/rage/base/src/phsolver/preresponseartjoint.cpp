// 
// phsolver/preresponseartjoint.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "forcesolver.h"

#include "phcore/constants.h"
#include "physics/collider.h"
#include "physics/contact.h"
#include "physics/manifold.h"

SOLVER_OPTIMISATIONS()

namespace rage {

void PreResponseArtJoint(phManifold& UNUSED_PARAM(manifold), const phForceSolverGlobals& UNUSED_PARAM(globals))
{
}

} // namespace rage
