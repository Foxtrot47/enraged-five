// 
// sagrender/Light Pre-Pass.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "lightprepass.h"

#include "bank/bank.h"

#include "grmodel/model.h"
#include "grmodel/modelfactory.h"
#include "grcore/state.h"
#include "grcore/device.h"
#include "grcore/texture.h"
#include "lighting/pointlight.h"
#include "sagrender/renderer.h"
#include "sagworldshared/rdrroom.h"
#include "spatialdata/shaft.h"
#include "spatialdata/sphere.h"
#include "system/param.h"
#include "grprofile/pix.h"

#if __XENON
#include "system\xtl.h"
#endif

#define GEOMETRYLIGHTS

// this defines the max resolution of the geometry point light sphere
#define STEPS 10
#define NUMBEROFVERTICESPOINTINVB (STEPS * (STEPS / 2) * 4)

#define CONESTEPS 5
#define CAPSTEPS 5
#define NUMBEROFVERTICESSPOTINVB CONESTEPS * 3 + CONESTEPS * CAPSTEPS * 6 // 165

//#define OPTIMIZEDPOINTLIGHTS


using namespace rage;

extern float g_PixelAspect;


PARAM(nolightprepass, "[sagrender] Disable the Light Pre-Pass System");

static Color32 white(1.0f,1.0f,1.0f,1.0f);

renLightPrePassLightsMgr *renLightPrePassLightsMgr::sm_Instance = NULL;

renLightPrePassLightsMgr::renLightPrePassLightsMgr()
: m_LightBuffer(NULL),
m_CameraDirection(0.0, 0.0, 0.0),
m_CameraPosition(0.0, 0.0, 0.0)
//	, m_NormalBuffer(NULL)
{
	m_TexelSizeId = grcevNONE;
	m_NearFarClipPlaneQId = grcevNONE;
	m_DepthMapId = grcevNONE;
	m_RenderMapId = grcevNONE;
	m_ProjInverseTransposeId = grcevNONE;
	m_PointLightSphereId = grcevNONE;
	m_PointLightColorId = grcevNONE;
	m_PointLightOneOverRangeSqId = grcevNONE;
	m_ViewDirectionId = grcevNONE;
	m_OuterConeInnerConeId = grcevNONE;

#ifdef OPTIMIZEDPOINTLIGHTS
	m_TanHalfFOVAspectId = grcevNONE;
	m_InverseViewProjId = grcevNONE;
#endif

	m_LightPrePassShader = NULL; 
	m_DirectionalLightsOnly = true;

#if __BANK
	m_DirectionalLightsOnly = true;
	m_PointLightsOnly = true;
	m_SpotLightsOnly = true;
	m_LightPrePassPreview = false;
	m_LightPrePassSpecularOnly = false;
#endif // __BANK

	m_Enabled = (PARAM_nolightprepass.Get() != true);
}

renLightPrePassLightsMgr::~renLightPrePassLightsMgr()
{
	delete m_LightPrePassShader;
}

// TODO: Rename renLightPrePassMgr

void renLightPrePassLightsMgr::InitRenderTargets()
{
	Assert(!m_LightBuffer);

	if (m_Enabled)
	{
		int width = GRCDEVICE.GetWidth();
		int height = GRCDEVICE.GetHeight();

#if __PSN || __XENON
		grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
#endif

		grcTextureFactory::CreateParams cp;

		cp.Multisample = GRCDEVICE.GetMSAA();
		cp.IsResolvable = true;	
		cp.UseHierZ = true;
		cp.HasParent = true;
#if __XENON || __PSN
		cp.Parent = textureFactory.GetBackBufferDepth();
#else
		cp.Parent = NULL;
#endif

		cp.Format = grctfA8R8G8B8;

		m_LightBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("Light Buffer", grcrtPermanent, width, height, 32, &cp);

		if (!m_LightBuffer)
		{
			Errorf("Could not create light buffer - disabling Light Pre-Pass");
			m_Enabled = false;
			return;
		}
	}
}

void renLightPrePassLightsMgr::InitShaders()
{
	//	RAGE_TRACK(renLightPrePassLightsMgr);
	if (m_Enabled)
	{
		//
		// Create post fx shader	
		//
		m_LightPrePassShader = grmShaderFactory::GetInstance().Create("rage_lightprepass");
		m_LightPrePassShader->Load("rage_lightprepass",0,0);

		m_LightBufferID = grcEffect::LookupGlobalVar("LightBufferTex");

		m_ViewDirectionId = m_LightPrePassShader->LookupVar("ViewDirection");

		m_DepthMapId = m_LightPrePassShader->LookupVar("DepthMap");

		m_RenderMapId = m_LightPrePassShader->LookupVar("RenderMap");

		// handle for the variable that provides the size of one texel
		m_TexelSizeId = m_LightPrePassShader->LookupVar("TexelSize");

		// 
		m_SpotLightDirectionId = m_LightPrePassShader->LookupVar("SpotLightDirection");

		// point light sphere data
		m_PointLightSphereId = m_LightPrePassShader->LookupVar("PointLightSphere");

		// point light color data
		m_PointLightColorId = m_LightPrePassShader->LookupVar("PointLightColor");

		// outer and inner cone settings that the user set
		m_OuterConeInnerConeId = m_LightPrePassShader->LookupVar("OuterConeInnerCone");

		// depth buffer conversion
		m_NearFarClipPlaneQId  = m_LightPrePassShader->LookupVar("NearFarClipPlaneQ");

#ifdef OPTIMIZEDPOINTLIGHTS
		// depth buffer conversion
		m_TanHalfFOVAspectId  = m_LightPrePassShader->LookupVar("TanHalfFOVAspect");

		m_InverseViewProjId = m_LightPrePassShader->LookupVar("InverseViewProj");
#endif

		// to reconstruct the world position based on the depth buffer
		m_ProjInverseTransposeId = m_LightPrePassShader->LookupVar("WorldViewProjInverse");

		m_PointLightOneOverRangeSqId = m_LightPrePassShader->LookupVar("PointLightOneOverRangeSq");

		// attenuation constants
		m_AttenuationConstantsId = m_LightPrePassShader->LookupVar("AttenuationConstants");

		m_PointLightDrawTechnique = m_LightPrePassShader->LookupTechnique("PointLight");		

#ifdef GEOMETRYLIGHTS
		m_PointLightVolumeDrawTechnique = m_LightPrePassShader->LookupTechnique("PointLightVolume");	
		m_SpotLightVolumeDrawTechnique  = m_LightPrePassShader->LookupTechnique("SpotLightVolume");	
#endif

		m_DirectionalLightDrawTechnique = m_LightPrePassShader->LookupTechnique("DirectionalLight");	

		m_SpotLightDrawTechnique = m_LightPrePassShader->LookupTechnique("SpotLight");		

		m_CopyTechnique = m_LightPrePassShader->LookupTechnique("CopyRT");	

		m_CopyTechniqueSPEC = m_LightPrePassShader->LookupTechnique("CopyRTSPEC");	

		// all the setup work for the directional light variables is done in renderer.cpp in
		// 	((ltLightingMgr *) rmlLightingMgr::GetLightingManager())->SetDirectionalLights();

		if (!m_LightBufferID)
		{
			Warningf("No light buffer shader variable found - shaders don't have support for Light Pre-Pass rendering");
		}
	}
}

void renLightPrePassLightsMgr::DeleteRenderTargets()
{
	if (m_Enabled)
	{
		if (m_LightBuffer)
		{
			m_LightBuffer->Release();
			m_LightBuffer = NULL;
		}
	}
}


//
// The return value is a rectangle stored in a Vector4 like this
//
RECTANGLE renLightPrePassLightsMgr::DetermineClipRectPointLight(const Vector3& position, const float range)
{
	// compute 3D bounding box of light in world space
	Vector3 bbox3D[8];
	bbox3D[0].x = position.x - range;  bbox3D[0].y = position.y + range;  bbox3D[0].z = position.z - range;
	bbox3D[1].x = position.x + range;  bbox3D[1].y = position.y + range;  bbox3D[1].z = position.z - range;
	bbox3D[2].x = position.x - range;  bbox3D[2].y = position.y - range;  bbox3D[2].z = position.z - range;
	bbox3D[3].x = position.x + range;  bbox3D[3].y = position.y - range;  bbox3D[3].z = position.z - range;
	bbox3D[4].x = position.x - range;  bbox3D[4].y = position.y + range;  bbox3D[4].z = position.z + range;
	bbox3D[5].x = position.x + range;  bbox3D[5].y = position.y + range;  bbox3D[5].z = position.z + range;
	bbox3D[6].x = position.x - range;  bbox3D[6].y = position.y - range;  bbox3D[6].z = position.z + range;
	bbox3D[7].x = position.x + range;  bbox3D[7].y = position.y - range;  bbox3D[7].z = position.z + range;

	const grcViewport*	vp = grcViewport::GetCurrent();

	Matrix44 View;
	Matrix44 Projection;
	Matrix44  viewProjMat; 

	// project coordinates
	View.Set( vp->GetViewMtx());
	Projection.Set( vp->GetProjection());

	// we go from right to left 
	viewProjMat.Dot(Projection, View);

	Vector2 projBox[8];

	for (int i = 0; i < 8; ++i)
	{
		Vector4 projPoint;
		Vector4 box = Vector4(bbox3D[i].x, bbox3D[i].y, bbox3D[i].z, 1.0f);;

		// project the box into screen-space
		viewProjMat.Transform(box,projPoint);

		projBox[i].x = projPoint.x / projPoint.w;  
		projBox[i].y = projPoint.y / projPoint.w;

		//clip to extents
		if (projBox[i].x < -1.0f)
			projBox[i].x = -1.0f;
		else if (projBox[i].x > 1.0f)
			projBox[i].x = 1.0f;
		if (projBox[i].y < -1.0f)
			projBox[i].y = -1.0f;
		else if (projBox[i].y > 1.0f)
			projBox[i].y = 1.0f;

		//go to pixel coordinates
		projBox[i].x = ((projBox[i].x + 1.0f) / 2.0f) * GRCDEVICE.GetWidth();
		projBox[i].y = ((-projBox[i].y + 1.0f) / 2.0f) * GRCDEVICE.GetHeight();
	}

	// compute 2D bounding box of projected coordinates
	unsigned int minX = 0xFFFFFFFF;
	unsigned int maxX = 0x00000000;
	unsigned int minY = 0xFFFFFFFF;
	unsigned int maxY = 0x00000000;
	for (int i = 0; i < 8; ++i)
	{
		unsigned int x = static_cast<unsigned int>(projBox[i].x);
		unsigned int y = static_cast<unsigned int>(projBox[i].y);
		if (x < minX)
			minX = x;
		if (x > maxX)
			maxX = x;
		if (y < minY)
			minY = y;
		if (y > maxY)
			maxY = y;
	}
	Rectangle bbox2D;
	bbox2D.top    = minY;
	bbox2D.bottom = maxY;
	bbox2D.left   = minX;
	bbox2D.right  = maxX;

	return bbox2D;
}

//
// The return value is a rectangle stored in a Vector4 like this
// This function constructs a spherical cone and puts a rectangle around it. This is achieved in three easy steps
// 1. Generate the spherical cone geometry
// 2. Project into clip-space
// 3. construct a 2D box in screen-space
//

// constant values
const int coneSteps = 7;
const int capSteps = 5; // 7 * 5 * 4 + 7 * 3 = 171
const int NumberOfVertices = coneSteps * 3 + coneSteps * capSteps * 4;

Vector3 SphericalCone[NumberOfVertices];
Vector2 projBox[NumberOfVertices];

int OuterCounter = 0;
int InnerCounter = 0;

RECTANGLE renLightPrePassLightsMgr::DetermineClipRectSpotLight(const sagSpotLight *light)
{
	//	Vector3 bbox3D[8];

	OuterCounter = 0;
	InnerCounter = 0;

	sysMemSet(SphericalCone, 0, sizeof(Vector3) * NumberOfVertices);
	sysMemSet(projBox, 0, sizeof(Vector2) * NumberOfVertices);

	//
	// the code piece where the spherical cone is approximated needs to be in sync with the debug code in spotlight.cpp
	// both should use the same approximation here
	//

	// approximate the size of the spherical cone
	// change value range so that 0.5 is 1.0 and 1.0 is 0.5
	float InnerCone = 1.5f - light->GetOuterConeInnerCone().x;
	float OuterCone = 1.5f - light->GetOuterConeInnerCone().y;

	//AngleInRadians = LerpAngle((InnerCone + OuterCone / 2.0f), powf(Max(InnerCone, OuterCone), 1.65f), powf(Max(InnerCone, OuterCone), 2.5f));
	float AngleInRadians = powf(Max(InnerCone, OuterCone), 1.65f);

	// build up the spherical cone
	Matrix34 mtrx(M34_IDENTITY);
	if ( fabs( light->GetDirection().y ) < 0.99f)		// check so mess up cross product.
	{
		mtrx.LookAt(light->GetDirection(), Vector3(0, 0, 0), YAXIS);
	}
	else
	{
		mtrx.LookAt(light->GetDirection(), Vector3(0, 0, 0), XAXIS);
	}

	//	mtrx.LookAt(dir, Vector3(0, 0, 0), YAXIS);
	mtrx.d.Set(light->GetPosition());

	int i, j;
	Vector3 p1, p2;
	p2.Zero();
	Vector3 orth, yAxis(0, 1.0f, 0);
	orth.Cross(yAxis, light->GetDirection());
	float f = AngleInRadians;
	float x1, z1;
	float x = 0, z = 0;
	float fs, fc;
	cos_and_sin(fc, fs, f);

	// constant values
	const float radius = light->ComputeRange();

	int OuterLoop = 0;
	int InnerLoop = 0;

	for(i = 0; i <= coneSteps; i++)
	{
		p1 = p2;
		x1 = x;
		z1 = z;
		cos_and_sin(x, z, i/(float)coneSteps * 2 * PI - PI);

		p2.Set(radius * x * fs, radius * z * fs, radius * fc);

		if(i == 0)
			continue;

		SphericalCone[OuterLoop++] = Vector3(0, 0, 0);
		SphericalCone[OuterLoop++] = Vector3(p1);
		SphericalCone[OuterLoop] = Vector3(p2);

		Vector3 p1a, p1b, p2a, p2b;
		p1b.Zero(); p2b.Zero();
		for(j = 0; j <= capSteps; j++)
		{
			p1a.Set(p1b);
			p2a.Set(p2b);

			float q = f * j /(float)capSteps;
			float qc, qs;
			cos_and_sin(qc, qs, q);
			p1b.Set(radius * x * qs, radius * z * qs, radius * qc);
			p2b.Set(radius * x1 * qs, radius * z1 * qs, radius * qc);

			if(j == 0)
				continue;

			SphericalCone[OuterLoop + InnerLoop++] = Vector3(p1a);
			SphericalCone[OuterLoop + InnerLoop++] = Vector3(p2a);
			SphericalCone[OuterLoop + InnerLoop++] = Vector3(p2b);
			SphericalCone[OuterLoop + InnerLoop] = Vector3(p1b);
		}
	}

	//
	// now prepare to project everything into clip-space
	//
	const grcViewport*	vp = grcViewport::GetCurrent();

	Matrix44 View;
	Matrix44 Projection;
	Matrix44  viewProjMat; 

	// project coordinates
	View.Set( vp->GetViewMtx());
	Projection.Set( vp->GetProjection());

	// we go from right to left 
	viewProjMat.Dot(Projection, View);

	for (int i = 0; i < NumberOfVertices; ++i)
	{
		// project spherical cone into world space (?)
		mtrx.Transform(SphericalCone[i]);

		Vector4 projPoint;
		Vector4 box = Vector4(SphericalCone[i].x, SphericalCone[i].y, SphericalCone[i].z, 1.0f);;

		// project the box into screen-space
		viewProjMat.Transform(box,projPoint);

		projBox[i].x = projPoint.x / projPoint.w;  
		projBox[i].y = projPoint.y / projPoint.w;

		//clip to extents
		if (projBox[i].x < -1.0f)
			projBox[i].x = -1.0f;
		else if (projBox[i].x > 1.0f)
			projBox[i].x = 1.0f;
		if (projBox[i].y < -1.0f)
			projBox[i].y = -1.0f;
		else if (projBox[i].y > 1.0f)
			projBox[i].y = 1.0f;

		//go to pixel coordinates
		projBox[i].x = ((projBox[i].x + 1.0f) / 2.0f) * GRCDEVICE.GetWidth();
		projBox[i].y = ((-projBox[i].y + 1.0f) / 2.0f) * GRCDEVICE.GetHeight();
	}

	// now project into a 2D rectangle
	// compute 2D bounding box of projected coordinates
	unsigned int minX = 0xFFFFFFFF;
	unsigned int maxX = 0x00000000;
	unsigned int minY = 0xFFFFFFFF;
	unsigned int maxY = 0x00000000;
	for (int i = 0; i < NumberOfVertices; ++i)
	{
		unsigned int x = static_cast<unsigned int>(projBox[i].x);
		unsigned int y = static_cast<unsigned int>(projBox[i].y);
		if (x < minX)
			minX = x;
		if (x > maxX)
			maxX = x;
		if (y < minY)
			minY = y;
		if (y > maxY)
			maxY = y;
	}

	Rectangle bbox2D;
	bbox2D.top    = minY;
	bbox2D.bottom = maxY;
	bbox2D.left   = minX;
	bbox2D.right  = maxX;

	return bbox2D;
}

void renLightPrePassLightsMgr::RenderLights(const spdShaft &shaft)
{
	//	shaft = 0;

	if (m_Enabled)
	{
		BeginRenderingLights();

		// clear stencil
		//		GRCDEVICE.Clear(false,Color32(0.0f, 0.0f, 0.0f,0.0f), false, 0.0f, true, 0xff);
#if __XENON
		// Initialize the stencil buffers.
		GRCDEVICE.GetCurrent()->Clear( 0, 
			NULL, 
			D3DCLEAR_STENCIL | D3DCLEAR_HISTENCIL_CULL,
			0,
			1.0f,
			0xFF );

#elif __PSN
// need to be implemented
#endif

		//
		// render point lights first ... 
		//
		const sagPointLightMgr::LightArray &lightArray = POINTLIGHTMGR.GetLightArray();
		int count = lightArray.GetCount();

#if __BANK
		if(m_PointLightsOnly && *sagPointLightMgr::GetInstance().GetPointLightEnableBool())
#endif // __BANK
		{
#ifndef GEOMETRYLIGHTS
			// just set it by default to full-screen first
			GRCDEVICE.SetScissor(0, 0, GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight());
#endif
			for (int x=0; x<count; x++)
			{
				const sagPointLight *light = lightArray[x];

				if (light->m_Enabled)
				{
					if (!light->IsInCulledRoom())
					{
						Vector3 color = light->GetFinalColor();

						// Ignore black lights.
						if (color.Mag2() > 0.0f)
						{
							float range = light->ComputeRange(); 

							// Adjust the radius a bit to compensate for slight inaccuracies.
							range *= Clamp(1.2f - light->GetAttentuationConstants().z * 0.2f, 1.0f, 1.5f);

							spdSphere sphere(light->GetPosition(), range);

							if(shaft.IsVisible(sphere))
							{
		#ifndef GEOMETRYLIGHTS
								Rectangle rect = DetermineClipRectPointLight(light->GetPosition(), light->ComputeRange());

								// does not really make a difference in the moment
								float camDistance = m_CameraPosition.Dist(light->GetPosition());
								if(camDistance < 100.0f)
								{

									// this is pretty redundant ... should write a dedicated SetScissor function here
									int width = rect.right - rect.left;
									int height = rect.bottom - rect.top;

									// let's keep out lights smaller than 16 pixels on one side
									if(width > 16 && height > 16)
									{
										int x = rect.left;
										int y = rect.top;
										GRCDEVICE.SetScissor(x, y, width, height);
		#endif
										// bail out if too small
										RenderPointLight(sphere.GetVector4(), color, light->GetAttentuationConstants());
		#ifndef GEOMETRYLIGHTS
									}
								}
		#endif
							}
						}
					}
				}
			}
		}


		//
		// ok now spot lights
		//
		const sagSpotLightMgr::TypedLightArray &SpotLightArray = SPOTLIGHTMGR.GetLightArray();

		count = SpotLightArray.GetCount();

#ifndef GEOMETRYLIGHTS
		// just set it by default to full-screen first
		GRCDEVICE.SetScissor(0, 0, GRCDEVICE.GetWidth(), GRCDEVICE.GetHeight());
#endif

#if __BANK
		if(m_SpotLightsOnly)
#endif // __BANK
		{
			for (int x = 0; x < count; x++)
			{
				const sagSpotLight *light = SpotLightArray[x];

				if (light->m_Enabled)
				{
					if (!light->IsInCulledRoom())
					{
						Vector3 color = light->GetFinalColor();

						// Ignore black lights.
						if (color.Mag2() > 0.0f)
						{
							spdSphere sphere(light->GetPosition(), light->ComputeRange());

							if(shaft.IsVisible(sphere))
							{
		#ifndef GEOMETRYLIGHTS
								// build the scissor box around the light
								Rectangle rect = DetermineClipRectSpotLight(light);

								// this is pretty redundant ... should write a dedicated SetScissor function here
								int width = rect.right - rect.left;
								int height = rect.bottom - rect.top;

								int x = rect.left;
								int y = rect.top;
								GRCDEVICE.SetScissor(x, y, width, height);
		#endif
								RenderSpotLight(sphere.GetVector4(), color,
									light->GetOuterConeInnerCone().x,
									light->GetOuterConeInnerCone().y,
									light->GetDirection(),
									light->GetAttentuationConstants());
							}
						}
					}
				}
			}
		}

#ifndef GEOMETRYLIGHTS
		//
		// render spot lights
		//
		// just switch it off here
		GRCDEVICE.DisableScissor();



#endif
		//
		// render directional lights next ... 
		//
		if(m_DirectionalLightsOnly)
			RenderDirectionalLight();

		EndRenderingLights();
	}
}


grcRenderTarget* renLightPrePassLightsMgr::GrabDepthBuffer(bool forceResolve)
{
	if (m_DepthBuffer && !forceResolve)
		return m_DepthBuffer;

	PIXBegin(0, "Grab Z buffer");

#if __PSN || __XENON
	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
#endif

#if __XENON
	// tiled rendering is off, so we need to resolve the depthbuffer ourselves
	if (RENDERER.IsUsingPredicatedTiling())
	{
		// Do another resolve - we need that for postfx and particles.
		grcResolveFlags resolveFlags;

		resolveFlags.ClearColor = false;
		resolveFlags.ClearDepthStencil = false;
		GRCDEVICE.SaveBackBuffer(NULL, grcTextureFactory::GetInstance().GetBackBufferDepth(false), &resolveFlags);
		m_DepthBuffer = grcTextureFactory::GetInstance().GetBackBufferDepth(false);
	}
	else
	{
		m_DepthBuffer = textureFactory.GetBackBufferDepth(true);
	}

#elif __PSN
	m_DepthBuffer = textureFactory.GetBackBufferDepth();
#endif

	PIXEnd();

	return m_DepthBuffer;
}

bool oldAlphaBlend;
grcBlendSet oldBlendSet;
bool oldDepthTest;
bool oldDepthWrite;
grcCullMode oldCullMode;
#ifdef GEOMETRYLIGHTS
grcCompareFunc oldCompareFunc;
#endif

void renLightPrePassLightsMgr::BeginRenderingLights()
{
	if (m_Enabled)
	{
		PIXBegin(0, "Light Buffer Pass");
		FastAssert(m_LightBuffer);

		// get the depth buffer
		m_DepthBuffer = GrabDepthBuffer(true);

		//
		// for the Z-buffer support get near and far clipping plane from the viewport
		//
		// What we do here is transform the Z value in the Z buffer from NDC to worldviewproj = camera space
		// To achieve this, we implement the following formula in the pixel shader
		// z = -(n * f) / (z_p * (f - n) - f)
		//
		// Q = Zf / Zf - Zn
		// z = Zn * Q / Zd - Q
		// this value is positive because we have an inverted z axis ... so the z axis is opengl style while the rest
		// is D3D style
		// whereas Zd is the value coming from the depth buffer
		const grcViewport *vp = grcViewport::GetCurrent();

		// get near and far clipping plane
		// to send it down to the pixel shader
		Vector3 PlanesAndQ(vp->GetNearClip(), vp->GetFarClip(), vp->GetFarClip() / (vp->GetFarClip() - vp->GetNearClip()));
		m_LightPrePassShader->SetVar(m_NearFarClipPlaneQId, PlanesAndQ);

#ifdef OPTIMIZEDPOINTLIGHTS
		//
		// get the tangent of the half fov
		//

		// hmmm ... this is how it is done in viewport.cpp
		float g_PixelAspect = 1.0f;

		float aspect = vp->GetAspectRatio() * g_PixelAspect; // m_Aspect? m_Aspect : m_AspectRatio;

		// turn in to radians, and get half angle
		float fovy = vp->GetFOVY() * 0.5f * (3.14159265358979323846264338327950288f/180.0f);
		float tanFovy = tanf(fovy);

		Vector2 TanHalfFovAspect;

		TanHalfFovAspect.y = tanFovy;
		TanHalfFovAspect.x = tanFovy * aspect;

		// store the tangent of the half fov
		// in case of fovX it is multiplied already with the aspect
		m_LightPrePassShader->SetVar(m_TanHalfFOVAspectId, TanHalfFovAspect);
#endif

		// set depth buffer
		m_LightPrePassShader->SetVar(m_DepthMapId, m_DepthBuffer);

		// pass down the inverse FullCompositeMtx so that the pixel shader can compute 
		// worldspace coordinates for the position values of the light  
		grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
		Matrix44 Temp;
		Matrix44 WorldViewProj(grcViewport::GetCurrent()->GetFullCompositeMtx());
		Temp = WorldViewProj;
		Temp.Inverse();
		m_LightPrePassShader->SetVar(m_ProjInverseTransposeId, Temp);

#ifdef OPTIMIZEDPOINTLIGHTS
		// pass down the 
		// get ViewProj matrix here
		Matrix44 World(grcViewport::GetCurrent()->GetWorldMtx44());
		World.Inverse();
		// remove the world matrix values
		Matrix44 ViewProj = WorldViewProj.Dot(World);
		ViewProj.Inverse();
		m_LightPrePassShader->SetVar(m_InverseViewProjId, ViewProj);
#endif
// 		Matrix44 CameraMatrix(grcViewport::GetCurrent()->GetViewMtx());
// 		m_LightPrePassShader->SetVar(m_InverseViewProjId, CameraMatrix);


		// for the view direction in the specular calculation
		m_CameraDirection = Vector3(grcViewport::GetCurrent()->GetCameraMtx().c); 
		m_CameraDirection.Normalize();
		m_LightPrePassShader->SetVar(m_ViewDirectionId, m_CameraDirection);

		m_CameraPosition = Vector3(grcViewport::GetCurrent()->GetCameraMtx().d); 

		// provide the size of the target or source render target to the shader
		m_LightPrePassShader->SetVar(m_TexelSizeId, Vector4(1.0f/float(m_LightBuffer->GetWidth()),
									1.0f/float(m_LightBuffer->GetHeight()),
									(float)m_LightBuffer->GetWidth(), 
									(float)m_LightBuffer->GetHeight()));



#if __XENON
		if (!RENDERER.IsUsingPredicatedTiling())
#endif // __XENON
		{
#ifdef GEOMETRYLIGHTS
			grcTextureFactory::GetInstance().LockRenderTarget(0, m_LightBuffer, m_DepthBuffer);
#else
			grcTextureFactory::GetInstance().LockRenderTarget(0, m_LightBuffer, NULL);
			//		grcTextureFactory::GetInstance().LockRenderTarget(0, m_LightBuffer, m_DepthBuffer);
#endif
		}

		// this is the long clear form where we can separate between the clear for the depth and stencil
		//		GRCDEVICE.Clear(true,Color32(0.0f, 0.0f, 0.0f,0.0f), false, 0.0f, 0xFF);
		GRCDEVICE.Clear(true,Color32(0.0f, 0.0f, 0.0f,0.0f), false,  0.0f, true, 0xFF);

	}
}

void renLightPrePassLightsMgr::EndRenderingLights()
{
	if (m_Enabled)
	{
		grcResolveFlags resolveFlags;

#if __XENON
		if (RENDERER.IsUsingPredicatedTiling())
		{

			resolveFlags.ClearColor = false;
			resolveFlags.ClearDepthStencil = false;
			GRCDEVICE.SaveBackBuffer(m_LightBuffer, NULL, &resolveFlags);
		}
		else
#endif // __XENON
		{
			// try to get the free clear from the resolve
			resolveFlags.ClearColor=false;
			resolveFlags.ClearDepthStencil= false; // can't clear depth during depth only resolve if using hierZ
			grcTextureFactory::GetInstance().UnlockRenderTarget(0, &resolveFlags);
		}

		PIXEnd();
	}
}


//
// sets the stencil buffer and the hi-stencil for XBOX 360 or PS3
//
#if __XENON || __PSN
void renLightPrePassLightsMgr::SetHiStencilCullState(int mode)
{
	if (mode == 1) // two-sided volume rendering (increment on back, decrement on front)
	{
#if __XENON
		GRCDEVICE.SetRenderState( D3DRS_HISTENCILENABLE, false );
		GRCDEVICE.SetRenderState( D3DRS_HISTENCILWRITEENABLE, true );		
		GRCDEVICE.SetRenderState( D3DRS_HISTENCILREF, 0 );
		GRCDEVICE.SetRenderState( D3DRS_HISTENCILFUNC, D3DHSCMP_NOTEQUAL );	
#endif
		grcState::SetStencilEnable(true);
		grcState::SetStencilWriteMask(0xFF);
		grcState::SetStencilMask(0xFF);
		grcState::SetStencilRef(0);
		grcState::SetStencilFunc(grccfAlways);
		grcState::SetStencilPass(grcsoKeep);
		grcState::SetStencilFail(grcsoKeep);
		grcState::SetStencilZFail(grcsoIncrementWrap); // wrapping to max value if the new value is less than zero

		grcState::SetTwoSidedStencil(true);
		grcState::SetBackStencilWriteMask(0xFF);
		grcState::SetBackStencilMask(0xFF);
		grcState::SetBackStencilRef(0);
		grcState::SetBackStencilFunc(grccfAlways);
		grcState::SetBackStencilPass(grcsoKeep);
		grcState::SetBackStencilFail(grcsoKeep);
		grcState::SetBackStencilZFail(grcsoDecrementWrap); // wrapping to zero if the new value exceeds the max value
	}
	else if (mode == 0) //use the contents of the hi stencil
	{
#if __XENON
		GRCDEVICE.SetRenderState( D3DRS_HISTENCILENABLE, true);
		GRCDEVICE.SetRenderState( D3DRS_HISTENCILWRITEENABLE, false );	

		// debugSyncronousMode?D3DFHZS_SYNCHRONOUS:D3DFHZS_ASYNCHRONOUS 
		GRCDEVICE.GetCurrent()->FlushHiZStencil(D3DFHZS_ASYNCHRONOUS); 

		// use stencil to render the light - everywhere where the stencil is 0 render the light
		// and set the stencil to 255
		grcState::SetStencilEnable(true);
		grcState::SetStencilWriteMask(0xff);				
		grcState::SetStencilRef(0xff);
		grcState::SetStencilFunc(grccfNotEqual);	

		grcState::SetStencilPass(grcsoReplace);		
		grcState::SetStencilFail(grcsoKeep);
		grcState::SetStencilZFail(grcsoKeep);

		grcState::SetTwoSidedStencil(false);

#endif//__XENON...
#if __PSN
		grcState::SetStencilEnable(true);
		grcState::SetStencilWriteMask(0);				// disable writes
		grcState::SetStencilMask(0xFF);
		grcState::SetStencilRef(0);
		grcState::SetStencilFunc(grccfNotEqual);		// not-equal to zero
		grcState::SetStencilPass(grcsoKeep);
		grcState::SetStencilFail(grcsoKeep);
		grcState::SetStencilZFail(grcsoKeep);

		grcState::SetTwoSidedStencil(true);
		grcState::SetBackStencilWriteMask(0);			// disable writes
		grcState::SetBackStencilMask(0xFF);
		grcState::SetBackStencilRef(0);
		grcState::SetBackStencilFunc(grccfNotEqual);	// not-equal to zero
		grcState::SetBackStencilPass(grcsoKeep);
		grcState::SetBackStencilFail(grcsoKeep);
		grcState::SetBackStencilZFail(grcsoKeep);

		grcState::SetState(grcsColorWrite, grccwRGBA);
		grcState::SetState(grcsAlphaBlend, true);
#endif //__PSN...

	}
	else if (mode == -1) //off
	{
#if __XENON
		GRCDEVICE.SetRenderState( D3DRS_HISTENCILENABLE,	FALSE );
		grcState::SetTwoSidedStencil(false);
		grcState::SetStencilEnable(false);
		grcState::SetStencilPass(grcsoKeep);
		grcState::SetStencilFail(grcsoKeep);
		grcState::SetStencilZFail(grcsoKeep);
		grcState::SetBackStencilPass(grcsoKeep);
		grcState::SetBackStencilFail(grcsoKeep);
		grcState::SetBackStencilZFail(grcsoKeep);
		grcState::SetColorWrite(grccwRGBA);
#endif//__XENON...
#if __PSN
		grcState::SetStencilEnable(false);
		grcState::SetStencilWriteMask(0xFF);
		grcState::SetStencilMask(0xFF);
		grcState::SetStencilRef(0);
		grcState::SetStencilFunc(grccfAlways);
		grcState::SetStencilPass(grcsoKeep);
		grcState::SetStencilFail(grcsoKeep);
		grcState::SetStencilZFail(grcsoKeep);

		grcState::SetTwoSidedStencil(false);
		grcState::SetBackStencilWriteMask(0xFF);
		grcState::SetBackStencilMask(0xFF);
		grcState::SetBackStencilRef(0);
		grcState::SetBackStencilFunc(grccfAlways);
		grcState::SetBackStencilPass(grcsoKeep);
		grcState::SetBackStencilFail(grcsoKeep);
		grcState::SetBackStencilZFail(grcsoKeep);

		grcState::SetState(grcsColorWrite, grccwRGBA);
		grcState::SetState(grcsAlphaBlend, true);
#endif //__PSN...
	}
}
#endif //__XENON || __PSN...

void renLightPrePassLightsMgr::RenderPointLight(Vector4::Vector4Param sphere, 
												Vector3::Vector3Param color,
												Vector3::Vector3Param AttenuationConstants)
{
	if (m_Enabled)
	{
#ifdef GEOMETRYLIGHTS

		// 1. render volume with two-sided test on and for PS3 with depth bound on

#if __PSN && 0
		SetDepthBoundsFromLightVolume(pos, radius);
		grcDevice::SetDepthBoundsTestEnable(true);
#endif
		// render volume
		PIXBegin(0, "Render Point Light Volume");

#if __XENON || __PSN
		//
		SetHiStencilCullState(1);
#endif

		Vector4 Position = sphere;

		// m_LightPrePassShader->DrawSolidSphere(Position.GetVector3(), Position.w, 8, m_PointLightVolumeDrawTechnique);
		m_LightPrePassShader->DrawSolidSphere(Position.GetVector3(), 
												Position.w,
												STEPS, 
												m_PointLightVolumeDrawTechnique);

		PIXEnd();

		// 2. render light

		// render light
		// render volume
		PIXBegin(0, "Blit a point light");

#if __XENON || __PSN
		SetHiStencilCullState(0);
#endif
		// set light position
		Vector4 adjustedSphere(sphere);
		adjustedSphere.w *= adjustedSphere.w;
		m_LightPrePassShader->SetVar(m_PointLightSphereId, adjustedSphere);

		// set light color
		float radius = ((Vector4) sphere).w;
		m_LightPrePassShader->SetVar(m_PointLightColorId, (Vector3 &) color);
		m_LightPrePassShader->SetVar(m_PointLightOneOverRangeSqId, 1.0f / (radius * radius));

		// for better fall-off
		m_LightPrePassShader->SetVar(m_AttenuationConstantsId, (Vector3 &)AttenuationConstants);

		const Color32 * color = &white;					// vertex color | is that still used anywhere????
		float x1 = -1.0;								// x1 - Destination base x
		float y1 = 1.0;									// y1 - Destination base y
		float x2 = 1.0;									// x2 - Destination opposite-corner x
		float y2 = -1.0;								// y2 - Destination opposite-corner y

		// blit
		m_LightPrePassShader->TWODBlit(
			x1,				// x1 - Destination base x
			y1,				// y1 - Destination base y
			x2,				// x2 - Destination opposite-corner x
			y2,				// y2 - Destination opposite-corner y
			0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
			0.0f,			// u1 - Source texture base u (in normalized texels)
			0.0f,			// v1 - Source texture base v (in normalized texels)
			1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
			1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
			*color,			// color - vertex color ... for OpenGL style grcDrawQuadf
			m_PointLightDrawTechnique);


		PIXEnd();

		// 3. reset stencil
#if __XENON || __PSN
		SetHiStencilCullState(-1);
#endif

		// 4. reset depth bounds
#if __PSN & 0
		grcDevice::SetDepthBoundsTestEnable(false);
#endif

#else
		PIXBegin(0, "Blit a point light");

		// set light position
		m_LightPrePassShader->SetVar(m_PointLightSphereId, (Vector4 &) sphere);

		// set light color
		float radius = ((Vector4) sphere).w;
		m_LightPrePassShader->SetVar(m_PointLightColorId, (Vector3 &) color);
		m_LightPrePassShader->SetVar(m_PointLightOneOverRangeSqId, 1.0f / (radius * radius));

		// for better fall-off
		m_LightPrePassShader->SetVar(m_AttenuationConstantsId, (Vector3 &)AttenuationConstants);

		const Color32 * color = &white;					// vertex color | is that still used anywhere????
		float x1 = -1.0;								// x1 - Destination base x
		float y1 = 1.0;									// y1 - Destination base y
		float x2 = 1.0;									// x2 - Destination opposite-corner x
		float y2 = -1.0;								// y2 - Destination opposite-corner y

		// blit
		m_LightPrePassShader->TWODBlit(
			x1,				// x1 - Destination base x
			y1,				// y1 - Destination base y
			x2,				// x2 - Destination opposite-corner x
			y2,				// y2 - Destination opposite-corner y
			0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
			0.0f,			// u1 - Source texture base u (in normalized texels)
			0.0f,			// v1 - Source texture base v (in normalized texels)
			1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
			1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
			*color,			// color - vertex color ... for OpenGL style grcDrawQuadf
			m_PointLightDrawTechnique);

		PIXEnd();
#endif
	}
}


void renLightPrePassLightsMgr::RenderDirectionalLight()
{
	if (m_Enabled)
	{
		PIXBegin(0, "Blit a Directional light");

		// most values here are already set in renderer.cpp

		const Color32 * color = &white;					// vertex color | is that still used anywhere????
		float x1 = -1.0;								// x1 - Destination base x
		float y1 = 1.0;									// y1 - Destination base y
		float x2 = 1.0;									// x2 - Destination opposite-corner x
		float y2 = -1.0;								// y2 - Destination opposite-corner y

		// blit
		m_LightPrePassShader->TWODBlit(
			x1,				// x1 - Destination base x
			y1,				// y1 - Destination base y
			x2,				// x2 - Destination opposite-corner x
			y2,				// y2 - Destination opposite-corner y
			0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
			0.0f,			// u1 - Source texture base u (in normalized texels)
			0.0f,			// v1 - Source texture base v (in normalized texels)
			1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
			1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
			*color,			// color - vertex color ... for OpenGL style grcDrawQuadf
			m_DirectionalLightDrawTechnique);

		PIXEnd();
	}
}


void renLightPrePassLightsMgr::RenderSpotLight(Vector4::Vector4Param PositionNRange, Vector3::Vector3Param color,
											   float OuterCone,
											   float InnerCone,
											   Vector3::Vector3Param SpotLightDirection,
											   Vector3::Vector3Param AttenuationConstants)
{
	if (m_Enabled)
	{
#ifdef GEOMETRYLIGHTS
		//PIXBegin(0, "Blit a spot light volume");
		PIXBegin(0, "Render Spot Light Volume");

		// 1. render volume with two-sided test on and for PS3 with depth bound on

#if __PSN && 0
		SetDepthBoundsFromLightVolume(pos, radius);
		grcDevice::SetDepthBoundsTestEnable(TRUE);
#endif
		// render volume
		//
#if __XENON || __PSN
		SetHiStencilCullState(1);
#endif

		float AngleInRadians = 1.0f; // - light->GetOuterConeInnerCone().x;

		// approximate the size of the spherical cone
		// change value range so that 0.5 is 1.0 and 1.0 is 0.5
		InnerCone = 1.5f - OuterCone;
		OuterCone = 1.5f - InnerCone;

		//AngleInRadians = LerpAngle((InnerCone + OuterCone / 2.0f), powf(Max(InnerCone, OuterCone), 1.65f), powf(Max(InnerCone, OuterCone), 2.5f));
		AngleInRadians = powf(Max(InnerCone, OuterCone), 1.65f);

		Vector3 Position = (Vector3) PositionNRange;

		m_LightPrePassShader->DrawSolidSphericalCone(Position,
													SpotLightDirection, 
													AngleInRadians, 
													Position.w,
													CONESTEPS, 
													CAPSTEPS,
													m_SpotLightVolumeDrawTechnique);


		PIXEnd();

		// 2. render light

		// render light
		// render volume
		PIXBegin(0, "Blit a spot light");

#if __XENON || __PSN
		SetHiStencilCullState(0);
#endif

		// set light position
		m_LightPrePassShader->SetVar(m_PointLightSphereId, (Vector4 &) PositionNRange);

		// set light position
		Vector3 NormalizedSpotLightDirection;
		NormalizedSpotLightDirection.Normalize(SpotLightDirection);
		NormalizedSpotLightDirection = -NormalizedSpotLightDirection;
		m_LightPrePassShader->SetVar(m_SpotLightDirectionId, (Vector3 &) NormalizedSpotLightDirection);

		float radius = ((Vector4 &) PositionNRange).w;
		m_LightPrePassShader->SetVar(m_PointLightOneOverRangeSqId, 1.0f / (radius * radius));

		m_LightPrePassShader->SetVar(m_AttenuationConstantsId, (Vector3 &)AttenuationConstants);

		// outer cone and inner cone
		Vector2 OuterConeInnerCone;
		OuterConeInnerCone.x = OuterCone;
		OuterConeInnerCone.y = InnerCone;

		// set light color
		m_LightPrePassShader->SetVar(m_PointLightColorId, (Vector3 &) color);
		m_LightPrePassShader->SetVar(m_OuterConeInnerConeId, OuterConeInnerCone);

		const Color32 * color = &white;					// vertex color | is that still used anywhere????
		float x1 = -1.0;								// x1 - Destination base x
		float y1 = 1.0;									// y1 - Destination base y
		float x2 = 1.0;									// x2 - Destination opposite-corner x
		float y2 = -1.0;								// y2 - Destination opposite-corner y

		// blit
		m_LightPrePassShader->TWODBlit(
			x1,				// x1 - Destination base x
			y1,				// y1 - Destination base y
			x2,				// x2 - Destination opposite-corner x
			y2,				// y2 - Destination opposite-corner y
			0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
			0.0f,			// u1 - Source texture base u (in normalized texels)
			0.0f,			// v1 - Source texture base v (in normalized texels)
			1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
			1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
			*color,			// color - vertex color ... for OpenGL style grcDrawQuadf
			m_SpotLightDrawTechnique);


		// 3. reset stencil
#if __XENON || __PSN
		SetHiStencilCullState(-1);
#endif

		// 4. reset depth bounds
#if __PSN && 0
		grcDevice::SetDepthBoundsTestEnable(FALSE);
#endif
		PIXEnd();

#else // not geometry lights

		PIXBegin(0, "Blit a spot light");

		// set light position
		m_LightPrePassShader->SetVar(m_PointLightSphereId, (Vector4 &) PositionNRange);

		// set light position
		Vector3 NormalizedSpotLightDirection;
		NormalizedSpotLightDirection.Normalize(SpotLightDirection);
		NormalizedSpotLightDirection = -NormalizedSpotLightDirection;
		m_LightPrePassShader->SetVar(m_SpotLightDirectionId, (Vector3 &) NormalizedSpotLightDirection);

		float radius = ((Vector4 &) PositionNRange).w;
		m_LightPrePassShader->SetVar(m_PointLightOneOverRangeSqId, 1.0f / (radius * radius));

		// outer cone and inner cone
		Vector2 OuterConeInnerCone;
		OuterConeInnerCone.x = OuterCone;
		OuterConeInnerCone.y = InnerCone;

		// set light color
		m_LightPrePassShader->SetVar(m_PointLightColorId, (Vector3 &) color);
		m_LightPrePassShader->SetVar(m_OuterConeInnerConeId, OuterConeInnerCone);

		const Color32 * color = &white;					// vertex color | is that still used anywhere????
		float x1 = -1.0;								// x1 - Destination base x
		float y1 = 1.0;									// y1 - Destination base y
		float x2 = 1.0;									// x2 - Destination opposite-corner x
		float y2 = -1.0;								// y2 - Destination opposite-corner y

		// blit
		m_LightPrePassShader->TWODBlit(
			x1,				// x1 - Destination base x
			y1,				// y1 - Destination base y
			x2,				// x2 - Destination opposite-corner x
			y2,				// y2 - Destination opposite-corner y
			0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
			0.0f,			// u1 - Source texture base u (in normalized texels)
			0.0f,			// v1 - Source texture base v (in normalized texels)
			1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
			1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
			*color,			// color - vertex color ... for OpenGL style grcDrawQuadf
			m_SpotLightDrawTechnique);

		PIXEnd();
#endif
	}
}


void renLightPrePassLightsMgr::BeginMainColorPass()
{
	if (m_Enabled && m_LightBuffer && m_LightBufferID)
	{
		grcEffect::SetGlobalVar(m_LightBufferID, m_LightBuffer);
	}
}

void renLightPrePassLightsMgr::EndMainColorPass()
{
#if __BANK
	ShowRenderTargets();
#endif // __BANK
}

#if __PSN
inline void SetDepthBoundsFromLightVolume(Vector3 m_pos, float m_radius)
{
	// grab camera params
	float nearClip = grcViewport::GetCurrent()->GetNearClip();
	float farClip = grcViewport::GetCurrent()->GetFarClip();

	// super-simple for now: just use sphere radius
	Matrix44 viewMtx = grcViewport::GetCurrent()->GetViewMtx();
	Vector4 worldPos( m_pos.GetX(), m_pos.GetY(), m_pos.GetZ(), 1.0f );
	Vector4 viewPos;
	viewMtx.Transform( worldPos, viewPos );

	float lightCentre = -viewPos.GetZ();
	float lightMin = Clamp( lightCentre - m_radius, nearClip, farClip );
	float lightMax = Clamp( lightCentre + m_radius, nearClip, farClip );

	// convert to z buffer value
	float zScale = farClip/( farClip - nearClip );
	float boundsMin = Clamp( zScale*( lightMin - nearClip )/lightMin, 0.0f, 1.0f );
	float boundsMax = Clamp( zScale*( lightMax - nearClip )/lightMax, 0.0f, 1.0f );

	// set the test
	grcDevice::SetDepthBounds( boundsMin, boundsMax );
}
#endif

void renLightPrePassLightsMgr::InitClass()
{
	Assert(!sm_Instance);
	sm_Instance = rage_new renLightPrePassLightsMgr();
}

void renLightPrePassLightsMgr::ShutdownClass()
{
	delete sm_Instance;
	sm_Instance = NULL;
}
#if __BANK
// shows a render target as a quad on the screen for debugging purposes
// should be relative to the screen size
void renLightPrePassLightsMgr::ShowRT(grcTexture * texture, 
									  grcEffectTechnique technique,					// technique in FX file
									  float DestX, float DestY, float SizeX, float SizeY)								
{
	if (!m_LightPrePassShader || !texture)
	{
		return;
	}

	Color32 white(1.0f,1.0f,1.0f,1.0f);

	// set the chosen render target sampler stages; default is m_RenderMapId
	m_LightPrePassShader->SetVar(m_RenderMapId, texture);

	// provide the size of the target or source render target to the shader
	m_LightPrePassShader->SetVar(m_TexelSizeId, Vector4(1.0f/float(texture->GetWidth()),1.0f/float(texture->GetHeight()),(float)texture->GetWidth(), (float)texture->GetHeight()));

	// blit
	m_LightPrePassShader->TWODBlit(DestX,			// x1 - Destination base x
		DestY,			// y1 - Destination base y
		SizeX,			// x2 - Destination opposite-corner x
		SizeY,			// y1 - Destination opposite-corner y
		0.1f,							// z - Destination z value.  Note that the z value is expected to be in 0..1 space
		0.0f,							// u1 - Source texture base u (in normalized texels)
		0.0f,							// v1 - Source texture base v (in normalized texels)
		1.0f,							// u2 - Source texture opposite-corner u (in normalized texels)
		1.0f,							// v2 - Source texture opposite-corner v (in normalized texels)
		(Color32)white,				// color - reference to Packed color value
		technique);

	//	release texture again
	m_LightPrePassShader->SetVar(m_RenderMapId, (grcTexture*)grcTexture::None);
}

void renLightPrePassLightsMgr::ShowRenderTargets()
{
	// first get screen size
	float PositionUpperLeftCornerX = 0.0f;
	float PositionUpperLeftCornerY = 0.0f;

	// the full-screen is -1..1 in both directions ... which is normalized to 0..2
	const float OriginX = (PositionUpperLeftCornerX) - (1.0f);
	const float OriginY =  (1.0f) - (PositionUpperLeftCornerY);

	// the preview window follows the screen ratio ... how cool is that?
	const float RectWidth = 2.0f;
	const float RectHeight = 2.0f;

	if(m_LightPrePassPreview)
	{
		if(m_LightPrePassSpecularOnly)
		{
			ShowRT(m_LightBuffer, m_CopyTechniqueSPEC,
				OriginX,				// top left x
				OriginY,			// top left y 
				OriginX + RectWidth,		// bottom right x
				OriginY - RectHeight);	// bottom right y
		}
		else
		{
			ShowRT(m_LightBuffer, m_CopyTechnique,
				OriginX,				// top left x
				OriginY,			// top left y 
				OriginX + RectWidth,		// bottom right x
				OriginY - RectHeight);	// bottom right y
		}
	}
}

void renLightPrePassLightsMgr::AddWidgets(bkBank &bk)
{
	bk.PushGroup("Light Pre-pass Renderer");
	bk.AddToggle("Show Light Pre-Pass Preview", &m_LightPrePassPreview);
	bk.AddToggle("Show Point Lights only", &m_PointLightsOnly);
	bk.AddToggle("Show Directional lights only", &m_DirectionalLightsOnly);
	bk.AddToggle("Show Spot lights only", &m_SpotLightsOnly);
	bk.AddToggle("Show only Specular component", &m_LightPrePassSpecularOnly);
	bk.PopGroup();
}
#endif
