// 
// sagrender/lightprepass.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAGRENDER_LIGHTPREPASS_H
#define SAGRENDER_LIGHTPREPASS_H

#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"

#include "grcore/effect_typedefs.h"
#include "vector/vector4.h"
#include "grmodel/shader.h"
#include "grmodel/shaderfx.h"
#include "grmodel/shadergroup.h"
#include "lighting/spotlight.h"


namespace rage
{
	class bkBank;
	class grcRenderTarget;
	class spdShaft;
}

typedef struct RECTANGLE
{
	int left;
	int top;
	int right;
	int bottom;
} 	Rectangle;


class renLightPrePassLightsMgr
{
public:
	renLightPrePassLightsMgr();

	~renLightPrePassLightsMgr();

	void InitRenderTargets();

	void InitShaders();

	void DeleteRenderTargets();

	void BeginRenderingLights();

	void EndRenderingLights();

	void RenderLights(const spdShaft &shaft);

	void RenderPointLight(Vector4::Vector4Param sphere, 
		Vector3::Vector3Param color,
		Vector3::Vector3Param AttenuationConstants);

	void RenderDirectionalLight();

	void RenderSpotLight(Vector4::Vector4Param PositionNRange, Vector3::Vector3Param color,
		float OuterCone,
		float InnerCone,
		Vector3::Vector3Param SpotLightDirection,
		Vector3::Vector3Param AttenuationConstants);


	void BeginMainColorPass();

	void EndMainColorPass();

	static void InitClass();

	static void ShutdownClass();

	grcRenderTarget* GrabDepthBuffer(bool forceResolve);

	RECTANGLE DetermineClipRectPointLight(const Vector3& position, const float range);

	RECTANGLE DetermineClipRectSpotLight(const sagSpotLight* light);
#if __XENON || __PSN
	void SetHiStencilCullState(int mode);
#endif

#if __PSN
	inline void SetDepthBoundsFromLightVolume(Vector3 m_pos, float m_radius);
#endif

	static renLightPrePassLightsMgr &GetInstance()
	{
		Assert(sm_Instance);
		return *sm_Instance;
	}

#if __BANK
	void AddWidgets(bkBank &bank);
#endif // __BANK

protected:
#if __BANK
	// for debugging purposes show all the render targets involved
	void ShowRenderTargets();

	// shows a render target as a quad on the screen for debugging purposes
	void ShowRT(grcTexture * texture, 
		grcEffectTechnique technique,					// technique in FX file
		float DestX, float DestY, float SizeX, float SizeY);
#endif // __BANK

private:
	static renLightPrePassLightsMgr *sm_Instance;

	//! The light buffer into which the light data is rendered into.
	grcRenderTarget		*m_LightBuffer;
	grmShader			*m_LightPrePassShader;	

	//! The normal buffer written during the shadow collector pass.
	//	grcRenderTarget *m_NormalBuffer;

	grcEffectTechnique	m_PointLightDrawTechnique;	
	grcEffectTechnique	m_DirectionalLightDrawTechnique;
	grcEffectTechnique	m_SpotLightDrawTechnique;

	grcEffectTechnique	m_CopyTechnique;
	grcEffectTechnique	m_CopyTechniqueSPEC;

	grcEffectTechnique	m_PointLightVolumeDrawTechnique;
	grcEffectTechnique	m_SpotLightVolumeDrawTechnique;

	// pointer to the shadow collector depth buffer
	grcRenderTarget* m_DepthBuffer;

	//! Shared sampler variable for the light buffer
	grcEffectGlobalVar			m_LightBufferID;

	grcEffectVar				m_SpotLightDirectionId;
	grcEffectVar				m_PointLightOneOverRangeSqId;
	grcEffectVar				m_AttenuationConstantsId;
	grcEffectVar				m_TexelSizeId;				// handle for the variable that provides the size of one texel
	grcEffectVar				m_NearFarClipPlaneQId;		// handle for the near and far clipping plane and the Q parameter for the depth of field calculation
	grcEffectVar				m_DepthMapId;				// handle for specific depth map sampler states
	grcEffectVar				m_RenderMapId;				// generic texture sampler stages
	grcEffectVar				m_ProjInverseTransposeId;
	grcEffectVar				m_PointLightSphereId;
	grcEffectVar				m_PointLightColorId;
	grcEffectVar				m_ViewDirectionId;
	grcEffectVar				m_OuterConeInnerConeId;
	grcEffectVar				m_TanHalfFOVAspectId;
	grcEffectVar				m_InverseViewProjId;

#if __XENON
	// stores the number of pixel threads currently used by the 360
	grcResolveFlags m_ClearParams;
#endif

#if __BANK
	//	bool						m_ShowRenderTargets;		// If true, enable the debug render target display
	bool m_LightPrePassPreview;
	bool m_LightPrePassSpecularOnly;
	bool m_PointLightsOnly;
	bool m_SpotLightsOnly;

#endif // __BANK

	bool m_DirectionalLightsOnly;

	Vector3 m_CameraDirection;
	Vector3 m_CameraPosition;

	//! Global switch to enable the system.
	bool m_Enabled;
};

#define LIGHTPREPASSMGR renLightPrePassLightsMgr::GetInstance()


#endif // SAGRENDER_LIGHTPREPASS_H
