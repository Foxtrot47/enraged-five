// 
// grcustom/customlighting_rage.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef _GRCUSTOM_CUSTOMLIGHTING_RAGE_H_
#define _GRCUSTOM_CUSTOMLIGHTING_RAGE_H_


#include "customlighting.h"

#include "grcore/effect.h"


using namespace rage;


class grcRageLightImplementation : public grcCustomLightImplementation
{
public:
	virtual bool Init();

	virtual void SetLightingGroup(const grcLightGroup &grp);

	virtual const char *GetName() { return "Rage"; }
};



#endif // _GRCUSTOM_CUSTOMLIGHTING_RDR2_H_
