// 
// grcore/customlighting_mp3.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "grcustom/customlighting_mp3.h"

#include "grcore/device.h"
#include "grcore/light.h"
#include "grmodel/shaderfx.h"

using namespace rage;

// r,g,b	Color
// w		Unused
//shared float4 RSV_DirLightColor : Color REGISTER(c16);

// x,y,z	Negative light direction
// w		Unused
//shared float4 RSV_DirLightDir : Direction REGISTER(c17);

// r,g,b	Color
// w		Unused
//shared float4 RSV_AmbientLight : Color REGISTER(c18);

// x,y,z	Position
// w		Decay start
//shared float4 RSV_LightPosition[RSV_MAX_LIGHTS] : Position REGISTER(c19);

// x,y,z	Negative light direction
// w		cos(hotspot_angle) (only applicable for spots, must be < -1 for points)
//shared float4 RSV_LightDirection[RSV_MAX_LIGHTS] : Direction REGISTER(c27);

// r,g,b	Color
// w		cos(falloff_angle) (only applicable for spots, must be < -1 for points)
//shared float4 RSV_LightColor[RSV_MAX_LIGHTS] : Color REGISTER(c35);

void grcMp3LightImplementation::Register()
{
	m_UnlitTechniqueGroup = grmShaderFx::FindTechniqueGroupId("unlit");
	m_Lit2TechniqueGroup = grmShaderFx::RegisterTechniqueGroup("lit2");
	m_Lit4TechniqueGroup = grmShaderFx::RegisterTechniqueGroup("lit4");
	m_Lit8TechniqueGroup = grmShaderFx::RegisterTechniqueGroup("lit8");
}

bool grcMp3LightImplementation::Init()
{
	m_DirLightColor = grcEffect::LookupGlobalVar("RSV_DirLightColor", false);
	m_DirLightDir = grcEffect::LookupGlobalVar("RSV_DirLightDir", false);
	m_AmbientLight = grcEffect::LookupGlobalVar("RSV_AmbientLight", false);
	m_LightPosition = grcEffect::LookupGlobalVar("RSV_LightPosition", false);
	m_LightDirection = grcEffect::LookupGlobalVar("RSV_LightDirection", false);
	m_LightColor = grcEffect::LookupGlobalVar("RSV_LightColor", false);

	bool success = true;

	// This is only successful if there are no missing variables.
	success &= (m_DirLightColor != grcegvNONE);
	success &= (m_DirLightDir != grcegvNONE);
	success &= (m_AmbientLight != grcegvNONE);
	success &= (m_LightPosition != grcegvNONE);
	success &= (m_LightDirection != grcegvNONE);
	success &= (m_LightColor != grcegvNONE);

	return success;
}

void grcMp3LightImplementation::SetLightingGroup(const grcLightGroup &grp)
{
	grp.Validate();

#define RSV_MAX_LIGHTS 8
	Vec4V dirLightColor;
	Vec4V dirLightDir;
	Vec4V positionAndDecay[RSV_MAX_LIGHTS];
	Vec4V directionAndCosHotspot[RSV_MAX_LIGHTS];
	Vec4V colorAndCosFalloff[RSV_MAX_LIGHTS];
	int pointSpotCount = 0;

	for (int i = 0; i < grp.GetActiveCount(); ++i)
	{
		if (grp.IsLightEnabled(i))
		{
			switch (grp.GetLightType(i))
			{
			case grcLightGroup::LTTYPE_DIR:
				{
					dirLightColor = Vec4V(grp.GetColor(i),ScalarV(V_ZERO));
					dirLightDir = Vec4V(grp.GetDirection(i),ScalarV(V_ZERO));
				}
				break;
			case grcLightGroup::LTTYPE_POINT:
				{
					positionAndDecay[pointSpotCount] = Vec4V(grp.GetPosition(i),ScalarV(V_ZERO));
					directionAndCosHotspot[pointSpotCount] = Vec4V(grp.GetDirection(i),ScalarV(V_NEGTWO));
					colorAndCosFalloff[pointSpotCount] = Vec4V(grp.GetColor(i) * ScalarV(grp.GetIntensity(i)),ScalarV(V_NEGTHREE));
					++pointSpotCount;
				}
				break;
			case grcLightGroup::LTTYPE_SPOT:
				{
					positionAndDecay[pointSpotCount] = Vec4V(grp.GetPosition(i),ScalarV(V_ZERO));
					directionAndCosHotspot[pointSpotCount] = Vec4V(grp.GetDirection(i),ScalarV(V_ZERO));
					colorAndCosFalloff[pointSpotCount] = Vec4V(grp.GetColor(i) * ScalarV(grp.GetIntensity(i)),ScalarV(cosf(grp.GetFalloff(i))));
					++pointSpotCount;
				}
				break;
			case grcLightGroup::LTTYPE_COUNT:
				Errorf("Invalid light group type");
				continue;
			}
		}
	}

	for (int i = pointSpotCount; i < RSV_MAX_LIGHTS; ++i)
	{
		positionAndDecay[i].ZeroComponents();
		directionAndCosHotspot[i].ZeroComponents();
		colorAndCosFalloff[i].ZeroComponents();
	}

	grcEffect::SetGlobalVar(m_DirLightColor, &dirLightColor, 1);
	grcEffect::SetGlobalVar(m_DirLightDir, &dirLightDir, 1);
	grcEffect::SetGlobalVar(m_LightPosition, positionAndDecay, RSV_MAX_LIGHTS);
	grcEffect::SetGlobalVar(m_LightDirection, directionAndCosHotspot, RSV_MAX_LIGHTS);
	grcEffect::SetGlobalVar(m_LightColor, colorAndCosFalloff, RSV_MAX_LIGHTS);

	Vec4V ambientCol(V_ZERO);
	if (!grp.DontUseAmbient())
	{
		ambientCol = grp.GetAmbient();
	}
	grcEffect::SetGlobalVar(m_AmbientLight,ambientCol);

	// Force technique group
	if (!pointSpotCount)
	{
		grmShaderFx::SetForcedTechniqueGroupId(m_UnlitTechniqueGroup);
	}
	else if (pointSpotCount <= 2)
	{
		grmShaderFx::SetForcedTechniqueGroupId(m_Lit2TechniqueGroup);
	}
	else
	{
		grmShaderFx::SetForcedTechniqueGroupId(m_Lit4TechniqueGroup);
	}

	//grcState::SetLightingGroup(grp);
}
