// 
// grcustom/customlighting_rdr2.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef _GRCUSTOM_CUSTOMLIGHTING_RDR2_H_
#define _GRCUSTOM_CUSTOMLIGHTING_RDR2_H_


#include "customlighting.h"

#include "grcore/effect.h"
#include "vector/vector3.h"
#include "vector/vector4.h"


using namespace rage;


class Rdr2PointLight
{
public:
	Vector3	m_Position;			// Light position
	Vector4	m_ColorAndRange;	// RGB and range (range is 1/range^2)
};


class grcRdr2LightImplementation : public grcCustomLightImplementation
{
public:
	virtual bool Init();

//	virtual void Shutdown();

	virtual void SetLightingGroup(const grcLightGroup &grp);

	virtual const char *GetName() { return "RDR2"; }

protected:
	void SetupDirectionalLights(Vector3 *directions, Vector4 *colorsAndIntensity);

	void SetupPointLights(const Rdr2PointLight *RESTRICT lightArray);

	grcEffectGlobalVar	m_DirectionalLightX, m_DirectionalLightY, m_DirectionalLightZ, m_DirectionalLightR, m_DirectionalLightG, m_DirectionalLightB;
	grcEffectGlobalVar	m_PrimaryDirectionalDir, m_PrimaryDirectionalColor;
	grcEffectGlobalVar	m_gvLightAmbient;

	grcEffectGlobalVar	m_PointLightX, m_PointLightY, m_PointLightZ, m_PointLightR, m_PointLightG, m_PointLightB;
	grcEffectGlobalVar	m_PointLightOneOverRangeSq;

	grcEffectGlobalVar	m_PointLightX2, m_PointLightY2, m_PointLightZ2, m_PointLightR2, m_PointLightG2, m_PointLightB2;
	grcEffectGlobalVar	m_PointLightOneOverRangeSq2;
};



#endif // _GRCUSTOM_CUSTOMLIGHTING_RDR2_H_
