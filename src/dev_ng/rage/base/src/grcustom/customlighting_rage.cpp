// 
// grcustom/customlighting_rage.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "customlighting_rage.h"

#include "grcore/device.h"
#include "grcore/light.h"

#include "grprofile/pix.h"
#include "system/xtl.h"


using namespace rage;





bool grcRageLightImplementation::Init()
{
	// Just randomly test one of the RAGE-specific variables.
	return (grcEffect::LookupGlobalVar("LightType", false) != grcegvNONE);
}

void grcRageLightImplementation::SetLightingGroup(const grcLightGroup &grp)
{
	grp.Validate();
	grcLightState::SetLightingGroup(grp);
}
