// 
// grcustom/customlighting_mp3.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRCUSTOM_CUSTOMLIGHTING_MP3_H
#define GRCUSTOM_CUSTOMLIGHTING_MP3_H

#include "grcustom/customlighting.h"
#include "grcore/effect.h"

using namespace rage;

class grcMp3LightImplementation : public grcCustomLightImplementation
{
public:
	virtual void Register();
	virtual bool Init();

	//virtual void Shutdown();

	virtual void SetLightingGroup(const grcLightGroup &grp);

	virtual const char* GetName() { return "MP3"; }

protected:
	int m_UnlitTechniqueGroup;
	int m_Lit2TechniqueGroup;
	int m_Lit4TechniqueGroup;
	int m_Lit8TechniqueGroup;

	grcEffectGlobalVar m_DirLightColor;
	grcEffectGlobalVar m_DirLightDir;
	grcEffectGlobalVar m_AmbientLight;
	grcEffectGlobalVar m_LightPosition;
	grcEffectGlobalVar m_LightDirection;
	grcEffectGlobalVar m_LightColor;
};

#endif // GRCUSTOM_CUSTOMLIGHTING_MP3_H
