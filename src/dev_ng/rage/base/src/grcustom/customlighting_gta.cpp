// 
// grcore/customlighting_gta.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "customlighting_gta.h"

#include "grcore/device.h"
#include "grcore/light.h"

#include "grprofile/pix.h"
#include "system/xtl.h"

enum CLightType { LT_POINT, LT_DIR, LT_SPOT, LT_COUNT };


#define POINT_LIGHT_COUNT	4
#define DIR_LIGHT_COUNT		4



using namespace rage;

void lgt2CalculateSpotLightScaleBias( bool isSpot,  float cosInnner,  float cosOuter , float& scale, float& bias )
{
	if (!isSpot )
	{
		bias = 1.0f;
		scale = 0.0f;
		return;
	}

	scale = 1.0f/( cosOuter - cosInnner );
	bias = -cosInnner * scale;

	// now create inverse 1 - ( scale + bias ) = -scale + ( -bias + 1.0f );
	scale = -scale;
	bias = -bias + 1.0f;
}
void CalculateConeScaleAndOffset( const float* type, const Vector4* falloff, Vector4& scale, Vector4& offset )
{
	for ( int i = 0;i < 4; i++ )
	{
		lgt2CalculateSpotLightScaleBias( type[i]==float((int)LT_SPOT), 
			falloff[i].z , 
			falloff[i].w , 
			scale[i], 
			offset[i]);
	}		
}

void Vector4Transpose3( const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4,
					   Vector4& r1,  Vector4& r2, Vector4& r3)
{
	Vector4 p1;
	Vector4 p2;
	Vector4 p3;
	Vector4 p4;

	p1.SetVector3(v1);
	p2.SetVector3(v2);
	p3.SetVector3(v3);
	p4.SetVector3(v4);

	Vector4 t1;
	Vector4 t2;
	Vector4 t3;
	Vector4 t4;

	t1.MergeXY( p1, p3 );
	t2.MergeXY( p2, p4 );
	t3.MergeZW( p1, p3 );
	t4.MergeZW( p2, p4 );

	r1.MergeXY( t1, t2 );
	r2.MergeZW( t1, t2 );
	r3.MergeXY( t3, t4 );
}

void Vector4Transpose4( const Vector4& v1, const Vector4& v2, const Vector4& v3, const Vector4& v4,
					   Vector4& r1,  Vector4& r2, Vector4& r3)
{
	Vector4 p1;
	Vector4 p2;
	Vector4 p3;
	Vector4 p4;

	p1.Set(v1);
	p2.Set(v2);
	p3.Set(v3);
	p4.Set(v4);

	Vector4 t1;
	Vector4 t2;
	Vector4 t3;
	Vector4 t4;

	t1.MergeXY( p1, p3 );
	t2.MergeXY( p2, p4 );
	t3.MergeZW( p1, p3 );
	t4.MergeZW( p2, p4 );

	r1.MergeXY( t1, t2 );
	r2.MergeZW( t1, t2 );
	r3.MergeXY( t3, t4 );
}



bool grcGtaLightImplementation::Init()
{
	// sm_gvDepthFxParams = grcEffect::LookupGlobalVar("DepthFxParams");

	sm_gvLightAmbientColour0 = grcEffect::LookupGlobalVar("LightAmbientColor0", false);
	sm_gvLightAmbientColour1 = grcEffect::LookupGlobalVar("LightAmbientColor1", false);

	sm_gvLightPosX = grcEffect::LookupGlobalVar("LightPositionX", false);
	sm_gvLightPosY = grcEffect::LookupGlobalVar("LightPositionY", false);
	sm_gvLightPosZ = grcEffect::LookupGlobalVar("LightPositionZ", false);

	sm_gvLightDirX = grcEffect::LookupGlobalVar("LightDirX", false);
	sm_gvLightDirY = grcEffect::LookupGlobalVar("LightDirY", false);
	sm_gvLightDirZ = grcEffect::LookupGlobalVar("LightDirZ", false);

	sm_gvLightColR = grcEffect::LookupGlobalVar("LightColR", false);
	sm_gvLightColG = grcEffect::LookupGlobalVar("LightColG", false);
	sm_gvLightColB = grcEffect::LookupGlobalVar("LightColB", false);

	sm_gvLightFallOff = grcEffect::LookupGlobalVar("LightFallOff", false);  
	sm_gvLightConeScale = grcEffect::LookupGlobalVar("LightConeScale", false);
	sm_gvLightConeOffset = grcEffect::LookupGlobalVar("LightConeOffset", false);  

	sm_gvLightPointPosX = grcEffect::LookupGlobalVar("LightPointPositionX", false);
	sm_gvLightPointPosY = grcEffect::LookupGlobalVar("LightPointPositionY", false);
	sm_gvLightPointPosZ = grcEffect::LookupGlobalVar("LightPointPositionZ", false);

	sm_gvLightPointColR = grcEffect::LookupGlobalVar("LightPointColR", false);
	sm_gvLightPointColG  = grcEffect::LookupGlobalVar("LightPointColG", false);
	sm_gvLightPointColB  = grcEffect::LookupGlobalVar("LightPointColB", false);

	sm_gvLightPointFallOff  = grcEffect::LookupGlobalVar("LightPointFallOff", false);

	sm_gvDirectionalLight = grcEffect::LookupGlobalVar("DirectionalLight", false);
	sm_gvDirectionalColor  = grcEffect::LookupGlobalVar("DirectionalColour", false);

	sm_gvLightDir2X = grcEffect::LookupGlobalVar("LightDir2X", false);
	sm_gvLightDir2Y = grcEffect::LookupGlobalVar("LightDir2Y", false);
	sm_gvLightDir2Z = grcEffect::LookupGlobalVar("LightDir2Z", false);
	sm_gvLightConeScale2 = grcEffect::LookupGlobalVar("LightConeScale2", false);
	sm_gvLightConeOffset2 = grcEffect::LookupGlobalVar("LightConeOffset2", false); 

	return sm_gvLightConeOffset2 != grcegvNONE;
}

#define MAX_RENDERED_LIGHTS 9

void grcGtaLightImplementation::SetLightingGroup(const grcLightGroup &grp)
{
	float lightType[MAX_RENDERED_LIGHTS];
	Vector3 lightPos[MAX_RENDERED_LIGHTS];
	Vector3 lightDir[MAX_RENDERED_LIGHTS];
	Vector4 lightColor[MAX_RENDERED_LIGHTS];
	Vector4 lightFallOffs[MAX_RENDERED_LIGHTS];
	Vector4 m_AmbientColour;

	m_AmbientColour = VEC4V_TO_VECTOR4(grp.GetAmbient());
	
	int i;

	for(i=1;i<MAX_RENDERED_LIGHTS;i++)
	{
		lightPos[i] = Vector3(0.0f,0.0f,0.0f);
		lightFallOffs[i]=Vector4(0.0f, 400.0f, 0.0f, 0.0f);
		lightColor[i] = Vector4(0.0f,0.0f,0.0f,-1.0f);
		lightType[i] = float((int)LT_POINT);
		lightDir[i] = Vector3(0.0f,0.0f,-1.0f);
	}

	if(true)
	{
		lightPos[0] = Vector3(-5000.0f,-10000.0f,15000.0f);
		lightDir[0] = Vector3(-5000.0f,-10000.0f,15000.0f);
		lightDir[0].Normalize();
		lightType[0]=float((int)LT_DIR);
		lightColor[0] = Vector4(0.9f,0.9f,0.9f,-0.0f);
	}

	//grcEffect::SetGlobalVar(sm_gvDepthFxParams, Vector4(1.0f,1.0f,100.0f,100.0f));

	// setup directional lights 
	Vector4 dirLightPack;
	dirLightPack.SetVector3(lightDir[0]);
	dirLightPack.w=1.0f;

	grcEffect::SetGlobalVar(sm_gvDirectionalLight, (Vec4V&)dirLightPack);

	Vector4 dirLightColInten= Vector4( lightColor[0].x,lightColor[0].y,lightColor[0].z, 1.0f );
	grcEffect::SetGlobalVar(sm_gvDirectionalColor, (Vec4V&)dirLightColInten );

	Vector4 vX;
	Vector4 vY;
	Vector4 vZ;

	Vector4Transpose3( lightDir[1],
		lightDir[2],
		lightDir[3],
		lightDir[4],
		vX, vY, vZ);

	grcEffect::SetGlobalVar(sm_gvLightDirX, (Vec4V&)vX );
	grcEffect::SetGlobalVar(sm_gvLightDirY, (Vec4V&)vY );
	grcEffect::SetGlobalVar(sm_gvLightDirZ, (Vec4V&)vZ );

	Vector4Transpose4(	lightColor[1],
		lightColor[2],
		lightColor[3],
		lightColor[4],
		vX, vY, vZ);

	grcEffect::SetGlobalVar(sm_gvLightColR, (Vec4V&)vX );
	grcEffect::SetGlobalVar(sm_gvLightColG, (Vec4V&)vY );
	grcEffect::SetGlobalVar(sm_gvLightColB, (Vec4V&)vZ );

	Vector4Transpose3(	lightPos[1],
		lightPos[2],
		lightPos[3],
		lightPos[4],
		vX, vY, vZ);

	grcEffect::SetGlobalVar(sm_gvLightPosX, (Vec4V&)vX );
	grcEffect::SetGlobalVar(sm_gvLightPosY, (Vec4V&)vY );
	grcEffect::SetGlobalVar(sm_gvLightPosZ, (Vec4V&)vZ );

	float FallOffMax = 1000.0f;

	Vector4 fallOffs = Vector4( FallOffMax,FallOffMax,FallOffMax,FallOffMax);
	fallOffs = fallOffs*fallOffs;
	fallOffs.InvertSafe();
	grcEffect::SetGlobalVar(sm_gvLightFallOff, (Vec4V&)fallOffs);


	Vector4 ConeScale = Vector4( 0.0f, 0.0f, 0.0f, 0.0f );
	Vector4 ConeOffset = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
	CalculateConeScaleAndOffset( &lightType[1], &lightFallOffs[1], ConeScale, ConeOffset );

	grcEffect::SetGlobalVar(sm_gvLightConeScale, (Vec4V&)ConeScale );
	grcEffect::SetGlobalVar(sm_gvLightConeOffset, (Vec4V&)ConeOffset );

	// setup 4 point lights
	Vector4Transpose4(	lightColor[5],
		lightColor[6],
		lightColor[7],
		lightColor[8],
		vX, vY, vZ);

	grcEffect::SetGlobalVar(sm_gvLightPointColR, (Vec4V&)vX );
	grcEffect::SetGlobalVar(sm_gvLightPointColG, (Vec4V&)vY );
	grcEffect::SetGlobalVar(sm_gvLightPointColB, (Vec4V&)vZ );

	Vector4Transpose3(	lightPos[5],
		lightPos[6],
		lightPos[7],
		lightPos[8],
		vX, vY, vZ);

	grcEffect::SetGlobalVar(sm_gvLightPointPosX, (Vec4V&)vX );
	grcEffect::SetGlobalVar(sm_gvLightPointPosY, (Vec4V&)vY );
	grcEffect::SetGlobalVar(sm_gvLightPointPosZ, (Vec4V&)vZ );

	Vector4Transpose3(	lightDir[5],
		lightDir[6],
		lightDir[7],
		lightDir[8],
		vX, vY, vZ);

	grcEffect::SetGlobalVar(sm_gvLightDir2X, (Vec4V&)vX );
	grcEffect::SetGlobalVar(sm_gvLightDir2Y, (Vec4V&)vY );
	grcEffect::SetGlobalVar(sm_gvLightDir2Z, (Vec4V&)vZ );

	CalculateConeScaleAndOffset( &lightType[5], &lightFallOffs[5], ConeScale, ConeOffset );

	grcEffect::SetGlobalVar(sm_gvLightConeScale2, (Vec4V&)ConeScale );
	grcEffect::SetGlobalVar(sm_gvLightConeOffset2, (Vec4V&)ConeOffset );

	FallOffMax = 1000.0f;

	fallOffs = Vector4( FallOffMax,FallOffMax,FallOffMax,FallOffMax );
	fallOffs = fallOffs*fallOffs;
	fallOffs.InvertSafe();

	grcEffect::SetGlobalVar(sm_gvLightPointFallOff, (Vec4V&)fallOffs);

	Vector4 AmbientColor2 = Vector4( 0.3f, 0.3f, 0.3f, 1.0f );
	grcEffect::SetGlobalVar(sm_gvLightAmbientColour0,(Vec4V&)m_AmbientColour);

	Vector4 am2 = Vector4(0,0,0,0);

	am2 = AmbientColor2 - m_AmbientColour;

	grcEffect::SetGlobalVar(sm_gvLightAmbientColour1,(Vec4V&)am2);

//	grcEffect::SetGlobalVar(sm_gvPed,1.0f);
}
