// 
// grcore/customlighting.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef _GRCUSTOMLIGHTING_H_
#define _GRCUSTOMLIGHTING_H_


#include "atl/array.h"


namespace rage
{
	class bkBank;
	class grcLightGroup;




/*!
 * PURPOSE: This is the base class for an implementation of a lighting model.
 * Each implementation must at the very least provide GetName() to identify the model,
 * and most probably SetLightingGroup() to implement the lighting model itself.
 */
class grcCustomLightImplementation
{
public:
	// PURPOSE: The constructor. The base implementation automatically registers the
	// model with the manager.
	grcCustomLightImplementation();

	// PURPOSE: The destructor. The base implementation automatically unregisters the
	// model with the manager.
	virtual ~grcCustomLightImplementation();

	// PURPOSE: Initialization of the model. This is a great place to look up shader variables.
	// When implementing, remember that the shaders used are not compatible with this model,
	// so make sure that this call does not create any visible error messages when failing.
	//
	// RETURNS: TRUE if the initialization process went fine (i.e. all shader variables have
	// been found), FALSE if you think the current shaders cannot support this model.
	// NOTE that the user can still select this model even if Init() returns false, so 
	// remember to make the implementation itself failsafe.
	virtual bool Init() {return true;}

	// PURPOSE: Function is called prior to any shaders being loaded by the
	// tool. Useful for e.g. registering technique groups.
	virtual void Register() {}

	// PURPOSE: Destruction of this model.
	virtual void Shutdown() {};

	// PURPOSE: This is the actual implementation of the lighting model. This call should
	// imitate grcState::SetLightingGroup. NOTE that this function could be called even if
	// Init() failed, so be sure to add sufficient error checking.
	virtual void SetLightingGroup(const grcLightGroup &/* grp */) {};

	// RETURNS: A user-friendly identifier for this model.
	virtual const char *GetName() = 0;

	virtual bool WasInitSuccessful() const { return m_InitSuccessful; }

	virtual void SetInitSuccessful(bool initSuccessful)		{ m_InitSuccessful = initSuccessful; }



protected:
	bool m_InitSuccessful;
};



// PURPOSE: This is the manager for all lighting models.
// At the very least, you should instantiate it with InitClass(), call its AddWidget()
// preferably late during the installation (after shaders have been loaded). Call its
// SetLightingGroup instead of grcState::SetLightingGroup to implement lighting itself.
class grcCustomLighting
{
public:
	grcCustomLighting();
	~grcCustomLighting();

	// PURPOSE: Register a lighting model with the manager. This is done automatically
	// in the constructor of a lighting model.
	void RegisterInstance(grcCustomLightImplementation &implementation);

	// PURPOSE: Unregister a lighting model with the manager. This is done automatically
	// in the destructor of a lighting model. If the model is not registered, nothing
	// will happen.
	void UnregisterInstance(grcCustomLightImplementation &implementation);

	// PURPOSE: Set the lighting model to use, based on name
	void SetLightingModel(const char* pcLightingModelName);

	// 
	void SetLightingGroup(const grcLightGroup &grp);

	// PURPOSE: This function will, if this hasn't been done before, create all known
	// lighting models, register them, and make the last one that returned true on
	// Init() the default model.
	// It's safe to call this function more than once.
	void SafeInitImplementations();

	// PURPOSE: This function will call the Register function on all registered
	// instances. This function is called prior to any shaders being loaded.
	void RegisterImplementations();

#if __BANK
	void AddWidgets();
#endif // __BANK

	static void InitClass();

	static void ShutdownClass();

	static grcCustomLighting &GetInstance()
	{
		FastAssert(sm_Instance);
		return *sm_Instance;
	}

	static bool IsInstantiated()
	{
		return sm_Instance != NULL;
	}

protected:
	atArray<grcCustomLightImplementation *> m_LightImplementations;

	int m_CurrentImplementation;

	bool m_ImplementationsInitialized;

#if __BANK
	bkBank *m_LightingBank;

	const char **m_ImplementationNames;
#endif // __BANK

	static grcCustomLighting *sm_Instance;
};



}


#endif // _GRCUSTOMLIGHTING_H_
