// 
// grcore/customlighting_rdr2.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "customlighting_rdr2.h"

#include "grcore/device.h"
#include "grcore/light.h"

#include "grprofile/pix.h"
#include "system/xtl.h"


#define POINT_LIGHT_COUNT	4
#define DIR_LIGHT_COUNT		4



using namespace rage;





bool grcRdr2LightImplementation::Init()
{
	m_PointLightOneOverRangeSq	= grcEffect::LookupGlobalVar("PointLightOneOverRangeSq", false);
	m_PointLightX		= grcEffect::LookupGlobalVar("PointLightX", false);
	m_PointLightY		= grcEffect::LookupGlobalVar("PointLightY", false);
	m_PointLightZ		= grcEffect::LookupGlobalVar("PointLightZ", false);
	m_PointLightR		= grcEffect::LookupGlobalVar("PointLightR", false);
	m_PointLightG		= grcEffect::LookupGlobalVar("PointLightG", false);
	m_PointLightB		= grcEffect::LookupGlobalVar("PointLightB", false);

	m_PointLightOneOverRangeSq2	= grcEffect::LookupGlobalVar("PointLightOneOverRangeSq2", false);
	m_PointLightX2		= grcEffect::LookupGlobalVar("PointLightX2", false);
	m_PointLightY2		= grcEffect::LookupGlobalVar("PointLightY2", false);
	m_PointLightZ2		= grcEffect::LookupGlobalVar("PointLightZ2", false);
	m_PointLightR2		= grcEffect::LookupGlobalVar("PointLightR2", false);
	m_PointLightG2		= grcEffect::LookupGlobalVar("PointLightG2", false);
	m_PointLightB2		= grcEffect::LookupGlobalVar("PointLightB2", false);

	m_DirectionalLightX		= grcEffect::LookupGlobalVar("DirectionalLightX", false);
	m_DirectionalLightY		= grcEffect::LookupGlobalVar("DirectionalLightY", false);
	m_DirectionalLightZ		= grcEffect::LookupGlobalVar("DirectionalLightZ", false);
	m_DirectionalLightR		= grcEffect::LookupGlobalVar("DirectionalLightR", false);
	m_DirectionalLightG		= grcEffect::LookupGlobalVar("DirectionalLightG", false);
	m_DirectionalLightB		= grcEffect::LookupGlobalVar("DirectionalLightB", false);

	m_PrimaryDirectionalDir = grcEffect::LookupGlobalVar("PrimaryDirectionalDir", false);
	m_PrimaryDirectionalColor = grcEffect::LookupGlobalVar("PrimaryDirectionalColor", false);

	m_gvLightAmbient	= grcEffect::LookupGlobalVar("Ambient",false);

	bool success = true;

	// This is only successful if there are no missing variables.
	success &= (m_PointLightOneOverRangeSq != grcegvNONE);
	success &= (m_PointLightX != grcegvNONE);
	success &= (m_PointLightY != grcegvNONE);
	success &= (m_PointLightZ != grcegvNONE);
	success &= (m_PointLightR != grcegvNONE);
	success &= (m_PointLightG != grcegvNONE);
	success &= (m_PointLightB != grcegvNONE);
	success &= (m_PrimaryDirectionalDir != grcegvNONE);
	success &= (m_PrimaryDirectionalColor != grcegvNONE);
	success &= (m_gvLightAmbient != grcegvNONE);

	return success;
}

void grcRdr2LightImplementation::SetupPointLights(const Rdr2PointLight *RESTRICT lightArray)
{
	Vector4		pointLight[7];


	pointLight[0] = Vector4(lightArray[0].m_ColorAndRange.w, lightArray[1].m_ColorAndRange.w, lightArray[2].m_ColorAndRange.w, lightArray[3].m_ColorAndRange.w);
	pointLight[1] = Vector4(lightArray[0].m_Position.x, lightArray[1].m_Position.x, lightArray[2].m_Position.x, lightArray[3].m_Position.x);
	pointLight[2] = Vector4(lightArray[0].m_Position.y, lightArray[1].m_Position.y, lightArray[2].m_Position.y, lightArray[3].m_Position.y);
	pointLight[3] = Vector4(lightArray[0].m_Position.z, lightArray[1].m_Position.z, lightArray[2].m_Position.z, lightArray[3].m_Position.z);
	pointLight[4] = Vector4(lightArray[0].m_ColorAndRange.x, lightArray[1].m_ColorAndRange.x, lightArray[2].m_ColorAndRange.x, lightArray[3].m_ColorAndRange.x);
	pointLight[5] = Vector4(lightArray[0].m_ColorAndRange.y, lightArray[1].m_ColorAndRange.y, lightArray[2].m_ColorAndRange.y, lightArray[3].m_ColorAndRange.y);
	pointLight[6] = Vector4(lightArray[0].m_ColorAndRange.z, lightArray[1].m_ColorAndRange.z, lightArray[2].m_ColorAndRange.z, lightArray[3].m_ColorAndRange.z);



#if __XENON
	GRCDEVICE.GetCurrent()->SetPixelShaderConstantF(m_PointLightOneOverRangeSq & 0xff, (float *) pointLight, 7);
#else // __XENON
	grcEffect::SetGlobalVar(m_PointLightOneOverRangeSq, (Vec4V&)pointLight[0]);
	grcEffect::SetGlobalVar(m_PointLightX, (Vec4V&)pointLight[1]);
	grcEffect::SetGlobalVar(m_PointLightY, (Vec4V&)pointLight[2]);
	grcEffect::SetGlobalVar(m_PointLightZ, (Vec4V&)pointLight[3]);
	grcEffect::SetGlobalVar(m_PointLightR, (Vec4V&)pointLight[4]);
	grcEffect::SetGlobalVar(m_PointLightG, (Vec4V&)pointLight[5]);
	grcEffect::SetGlobalVar(m_PointLightB, (Vec4V&)pointLight[6]);
#endif // __XENON

	// The RAGE lighting system does not support more than 3 point lights,
	// so we can just ignore the extended point light set.
	if (m_PointLightOneOverRangeSq2 != grcegvNONE)
	{
		const Vector4 emptyLights = Vector4(0.0f, 0.0f, 0.0f, 0.0f);

		grcEffect::SetGlobalVar(m_PointLightOneOverRangeSq2, (Vec4V&)emptyLights);
		grcEffect::SetGlobalVar(m_PointLightX2, (Vec4V&)emptyLights);
		grcEffect::SetGlobalVar(m_PointLightY2, (Vec4V&)emptyLights);
		grcEffect::SetGlobalVar(m_PointLightZ2, (Vec4V&)emptyLights);
		grcEffect::SetGlobalVar(m_PointLightR2, (Vec4V&)emptyLights);
		grcEffect::SetGlobalVar(m_PointLightG2, (Vec4V&)emptyLights);
		grcEffect::SetGlobalVar(m_PointLightB2, (Vec4V&)emptyLights);
	}
}

void grcRdr2LightImplementation::SetupDirectionalLights(Vector3 *directions, Vector4 *colorsAndIntensity)
{
	Vector4		dirR, dirG, dirB;
	Vector4		dirX, dirY, dirZ;

	Vector4		colors[4];

	for (int x=0; x<4; x++)
	{
		Vector4 intensity = colorsAndIntensity[x];
		intensity.SplatW();

		colors[x] = colorsAndIntensity[x] * intensity;
	}

	dirR = Vector4(colors[0].x, colors[1].x, colors[2].x, colors[3].x);
	dirG = Vector4(colors[0].y, colors[1].y, colors[2].y, colors[3].y);
	dirB = Vector4(colors[0].z, colors[1].z, colors[2].z, colors[3].z);

	dirX = Vector4(directions[0].x, directions[1].x, directions[2].x, directions[3].x);
	dirY = Vector4(directions[0].y, directions[1].y, directions[2].y, directions[3].y);
	dirZ = Vector4(directions[0].z, directions[1].z, directions[2].z, directions[3].z);

	if (m_DirectionalLightX != grcegvNONE)
	{
		grcEffect::SetGlobalVar(m_DirectionalLightX, (Vec4V&)dirX);
		grcEffect::SetGlobalVar(m_DirectionalLightY, (Vec4V&)dirY);
		grcEffect::SetGlobalVar(m_DirectionalLightZ, (Vec4V&)dirZ);
		grcEffect::SetGlobalVar(m_DirectionalLightR, (Vec4V&)dirR);
		grcEffect::SetGlobalVar(m_DirectionalLightG, (Vec4V&)dirG);
		grcEffect::SetGlobalVar(m_DirectionalLightB, (Vec4V&)dirB);
	}

	// Set up the primary light.
	grcEffect::SetGlobalVar(m_PrimaryDirectionalDir, (Vec4V&)directions[0]);
	colors[0].w = 0.0f;
	grcEffect::SetGlobalVar(m_PrimaryDirectionalColor, (Vec4V&)colors[0]);
}

void grcRdr2LightImplementation::SetLightingGroup(const grcLightGroup &grp)
{
	grp.Validate();

	Vector3 dirLightDirs[DIR_LIGHT_COUNT];
	Vector4 dirLightColors[DIR_LIGHT_COUNT];
	Rdr2PointLight ptLights[POINT_LIGHT_COUNT];

	int dirLightCount = 0;
	int ptLightCount = 0;


	for (int i = 0; i < grp.GetActiveCount(); ++i)
	{
		if (grp.IsLightEnabled(i))
		{
			if ( grp.GetLightType(i) == grcLightGroup::LTTYPE_POINT )
			{
				// It's a point light.
				Assert(ptLightCount < POINT_LIGHT_COUNT);
				ptLights[ptLightCount].m_Position = VEC3V_TO_VECTOR3(grp.GetPosition(i));

				// Get 1/rangeSq
				float falloff = grp.GetFalloff(i);
				falloff *= falloff;
				falloff = 1.0f / falloff;

				Vector3 color = VEC3V_TO_VECTOR3(grp.GetColor(i));

				ptLights[ptLightCount].m_ColorAndRange = Vector4(color.x, color.y, color.z, falloff);

				ptLightCount++;
			}
			else
			{
				// It's a directional light.
				Assert(dirLightCount < DIR_LIGHT_COUNT);

				dirLightDirs[dirLightCount] = VEC3V_TO_VECTOR3(grp.GetDirection(i));

				Vector4 colorAndIntensity;

				colorAndIntensity.SetVector3(VEC3V_TO_VECTOR3(grp.GetColor(i)));
				colorAndIntensity.w = grp.GetIntensity(i);

				dirLightColors[dirLightCount++] = colorAndIntensity;
			}
		}
	}

	// Fill unused lights
	for (int x=dirLightCount; x<DIR_LIGHT_COUNT; x++)
	{
		dirLightColors[x] = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
		dirLightDirs[x] = Vector3(0.0f, 0.0f, 0.0f);
	}

	for (int x=ptLightCount; x<POINT_LIGHT_COUNT; x++)
	{
		ptLights[x].m_Position = Vector3(0.0f, 0.0f, 0.0f);
		ptLights[x].m_ColorAndRange = Vector4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	SetupPointLights(ptLights);
	SetupDirectionalLights(dirLightDirs, dirLightColors);

	if (grp.DontUseAmbient())
	{
		Vec4V noAmb(V_ZERO);
		grcEffect::SetGlobalVar(m_gvLightAmbient, noAmb);
	}
	else
	{
		grcEffect::SetGlobalVar(m_gvLightAmbient, grp.GetAmbient());
	}

	//	grcState::SetLightingGroup(grp);
}
