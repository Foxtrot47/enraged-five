// 
// grcore/customlighting.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "customlighting.h"

#include "customlighting_rdr2.h"
#include "customlighting_rage.h"
#include "customlighting_gta.h"
#include "customlighting_mp3.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"

using namespace rage;


namespace rage
{

grcCustomLighting *grcCustomLighting::sm_Instance = NULL;



grcCustomLighting::grcCustomLighting()
	: m_CurrentImplementation(0)
#if __BANK
	, m_ImplementationNames(NULL)
#endif
	, m_ImplementationsInitialized(false)
{
}

grcCustomLighting::~grcCustomLighting()
{
	// Delete all implementations.
	while (m_LightImplementations.GetCount())
	{
		// The destructor will automatically remove itself from the manager.
		delete m_LightImplementations[0];
	}

#if __BANK
	if (m_ImplementationNames)
	{
		delete[] m_ImplementationNames;
	}
#endif
}


void grcCustomLighting::InitClass()
{
	Assert(!sm_Instance);
	sm_Instance = rage_new grcCustomLighting();

	rage_new grcRageLightImplementation();
	rage_new grcRdr2LightImplementation();
	rage_new grcGtaLightImplementation();
	rage_new grcMp3LightImplementation();
}

void grcCustomLighting::ShutdownClass()
{
	delete sm_Instance;
	sm_Instance = NULL;
}

void grcCustomLighting::RegisterInstance(grcCustomLightImplementation &implementation)
{
	m_LightImplementations.Grow() = &implementation;
}

void grcCustomLighting::UnregisterInstance(grcCustomLightImplementation &implementation)
{
	for (int x=0; x<m_LightImplementations.GetCount(); x++)
	{
		if (m_LightImplementations[x] == &implementation)
		{
			m_LightImplementations.DeleteFast(x);
			return;
		}
	}
}

void grcCustomLighting::SetLightingGroup(const grcLightGroup &grp)
{
	SafeInitImplementations();
	m_LightImplementations[m_CurrentImplementation]->SetLightingGroup(grp);
}

void grcCustomLighting::SafeInitImplementations()
{
	if (!m_ImplementationsInitialized)
	{
		for (int x=0; x<m_LightImplementations.GetCount(); x++)
		{
			if (m_LightImplementations[x]->Init())
			{
				m_CurrentImplementation = x;
			}
		}

		m_ImplementationsInitialized = true;
	}
}

void grcCustomLighting::RegisterImplementations()
{
	for (int x=0; x<m_LightImplementations.GetCount(); x++)
	{
		m_LightImplementations[x]->Register();
	}
}

#if __BANK

void grcCustomLighting::AddWidgets()
{
	bkBank &bank = BANKMGR.CreateBank("Lighting Model");

	AssertMsg(m_LightImplementations.GetCount(), "No light implementations have been registered. Were they not compiled?! Has the module not been instantiated with InitClass()?");

	m_ImplementationNames = rage_new const char *[m_LightImplementations.GetCount()];

	for (int x=0; x<m_LightImplementations.GetCount(); x++)
	{
		m_ImplementationNames[x] = m_LightImplementations[x]->GetName();
	}

	bank.AddCombo("Lighting Model", &m_CurrentImplementation, m_LightImplementations.GetCount(), m_ImplementationNames, NullCB);
}

#endif // __BANK





grcCustomLightImplementation::grcCustomLightImplementation()
{
	// Do we have an instance already?
	if (!grcCustomLighting::IsInstantiated())
	{
		grcCustomLighting::InitClass();
	}

	// Register ourselves.
	grcCustomLighting::GetInstance().RegisterInstance(*this);
}

grcCustomLightImplementation::~grcCustomLightImplementation()
{
	// This assert should never trigger - if it does, it means that somehow an implementation
	// was created after the manager has been deleted.
	Assert(grcCustomLighting::IsInstantiated());

	// Unregister ourselves.
	grcCustomLighting::GetInstance().UnregisterInstance(*this);
}

void grcCustomLighting::SetLightingModel(const char* pcLightingModelName)
{
	for (int x=0; x<m_LightImplementations.GetCount(); x++)
	{
		if(strcmp(m_LightImplementations[x]->GetName(), pcLightingModelName) == 0)
		{
			m_CurrentImplementation = x;
			break;
		}
	}
}


}
