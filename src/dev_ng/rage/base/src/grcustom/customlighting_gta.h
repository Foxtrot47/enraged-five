// 
// grcustom/customlighting_gta.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef _GRCUSTOM_CUSTOMLIGHTING_GTA_H_
#define _GRCUSTOM_CUSTOMLIGHTING_GTA_H_


#include "customlighting.h"

#include "grcore/effect.h"


using namespace rage;


class grcGtaLightImplementation : public grcCustomLightImplementation
{
public:
	virtual bool Init();

	virtual void SetLightingGroup(const grcLightGroup &grp);

	virtual const char *GetName() { return "Gta"; }

	grcEffectGlobalVar sm_gvLightPosX;
	grcEffectGlobalVar sm_gvLightPosY;
	grcEffectGlobalVar sm_gvLightPosZ;

	grcEffectGlobalVar sm_gvLightDirX;
	grcEffectGlobalVar sm_gvLightDirY;
	grcEffectGlobalVar sm_gvLightDirZ;

	grcEffectGlobalVar sm_gvLightColR;
	grcEffectGlobalVar sm_gvLightColG;
	grcEffectGlobalVar sm_gvLightColB;

	grcEffectGlobalVar sm_gvLightFallOff;
	grcEffectGlobalVar sm_gvLightConeScale;
	grcEffectGlobalVar sm_gvLightConeOffset;

	grcEffectGlobalVar sm_gvLightPointPosX;
	grcEffectGlobalVar sm_gvLightPointPosY;
	grcEffectGlobalVar sm_gvLightPointPosZ;

	grcEffectGlobalVar sm_gvLightPointColR;
	grcEffectGlobalVar sm_gvLightPointColG;
	grcEffectGlobalVar sm_gvLightPointColB;

	grcEffectGlobalVar sm_gvLightPointFallOff;

	grcEffectGlobalVar sm_gvDirectionalLight;
	grcEffectGlobalVar sm_gvDirectionalColor;

	grcEffectGlobalVar sm_gvLightDir2X;
	grcEffectGlobalVar sm_gvLightDir2Y;
	grcEffectGlobalVar sm_gvLightDir2Z;
	grcEffectGlobalVar sm_gvLightConeScale2;
	grcEffectGlobalVar sm_gvLightConeOffset2; 
	grcEffectGlobalVar sm_gvLightAmbientColour0;
	grcEffectGlobalVar sm_gvLightAmbientColour1;

	grcEffectGlobalVar sm_gvDepthFxParams;

	grcEffectGlobalVar sm_gvPed;
};



#endif // _GRCUSTOM_CUSTOMLIGHTING_GTA_H_
