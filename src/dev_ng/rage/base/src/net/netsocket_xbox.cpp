// 
// net/netsocket_xbox.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "file/file_config.h"
#if __LIVE

#include "netsocket.h"

#include "netdiag.h"
#include "nethardware.h"
#include "diag/seh.h"
#include "system/memory.h"
#include "system/xtl.h"
#if __XENON
#include <winsockx.h>
#elif __GFWL
#include <winsock2.h>
#include <winxnet.h>
#endif

#define NET_SET_SKTERR(se, e)   while((se)){*(se) = (e);break;}

namespace rage
{

netSocketError
netSocket::GetLastSocketError()
{
    netSocketError err = NET_SOCKERR_NONE;

    const int lasterror = XWSAGetLastError();

    switch(lasterror)
    {
        case 0:
            break;

        case WSAENETDOWN:
            err = NET_SOCKERR_NETWORK_DOWN;
            break;

        case WSAENOBUFS:
            err = NET_SOCKERR_NO_BUFFERS;
            break;

        case WSAEMSGSIZE:
            err = NET_SOCKERR_MESSAGE_TOO_LONG;
            break;

        case WSAEHOSTUNREACH:
            err = NET_SOCKERR_HOST_UNREACHABLE;
            break;

        case WSAECONNRESET:
            err = NET_SOCKERR_CONNECTION_RESET;
            break;

        case WSAEWOULDBLOCK:
            err = NET_SOCKERR_WOULD_BLOCK;
            break;

        case WSAEINPROGRESS:
            err = NET_SOCKERR_IN_PROGRESS;
            break;

        default:
            netError("WSAGetLastError() = %d", lasterror);
            err = NET_SOCKERR_UNKNOWN;
            break;
    }

    return err;
}

unsigned
netSocket::SizeofHeader()
{
    //Sizes taken from the Xbox secure sockets whitepaper.
    //https://xds.xbox.com/BPProgInfo.asp?Page=content/prog_wp_securesock.htm
    static const unsigned SIZEOF_IP_HEADER      = 20;
    static const unsigned SIZEOF_NAT_UDP_HEADER = 8;
    static const unsigned SIZEOF_XFLAGS         = 1;
    static const unsigned SIZEOF_XSP            = 3;
    static const unsigned SIZEOF_XSEQNUM        = 2;
    static const unsigned SIZEOF_XHASH          = 10;

    static const unsigned SIZEOF_INVARIANTS =
        SIZEOF_IP_HEADER
        + SIZEOF_NAT_UDP_HEADER
        + SIZEOF_XFLAGS
        + SIZEOF_XSP
        + SIZEOF_XSEQNUM
        + SIZEOF_XHASH;

    unsigned sizeofPorts;
    const unsigned short port = this->GetPort();

    //Note - these calculations are not strictly correct because
    //they don't take into account the value of the destination port.
    //They simply assume the destination port is the same as the
    //source port.
    if(1000 == port)
    {
        sizeofPorts = 0;
    }
    else if(port > 1000 && port <= 1255)
    {
        sizeofPorts = 2;
    }
    else
    {
        sizeofPorts = 4;
    }

    return SIZEOF_INVARIANTS + sizeofPorts;
}

void
netSocket::SetCryptoEnabled(const bool /*enabled*/)
{
}

bool
netSocket::IsCryptoEnabled() const
{
    return m_CryptoEnabled;
}

unsigned
netSocket::SizeofPacket(const unsigned sizeofPayload)
{
    //Pad the payload out to an 8-byte boundary
    //(per Xbox secure sockets whitepaper)
    return this->SizeofHeader() + ((sizeofPayload + 7) & ~0x07u);
}

//protected:

void
netSocket::NativeShutdown()
{
    this->NativeUnbind();
}

bool
netSocket::NativeGetMyAddress(netSocketAddress* addr) const
{
    bool success = false;

    addr->Clear();

    int skt = this->GetRawSocket();

    if(netVerify(skt >= 0))
    {
        sockaddr_in sin = {0};
        int sin_len = sizeof(sin);
        netIpAddress ip;
        unsigned short port = 0;

        if(netVerify(0 == XSocketGetSockName(skt, (sockaddr*) &sin, &sin_len)))
        {
            if(INADDR_ANY != sin.sin_addr.s_addr
                && INADDR_NONE != sin.sin_addr.s_addr)
            {
				ip = netIpAddress(netIpV4Address(net_ntohl(sin.sin_addr.s_addr)));
				success = true;
            }
            else
            {
                success = netHardware::GetLocalIpAddress(&ip);
            }

            port = net_ntohs(sin.sin_port);
        }

        if(success)
        {
            addr->Init(ip, port);
        }
    }

    return success;
}

bool
netSocket::NativeBind()
{
    SYS_CS_SYNC(m_Cs);

    netAssert(netHardware::IsAvailable());
    netAssert(m_Socket < 0);
    netAssert(NET_PROTO_UDP == this->GetProtocol()
            || NET_PROTO_VDP == this->GetProtocol());

    netDebug("Binding socket on port %d...", m_Port);

    rtry
    {
        //Only UDP sockets can be configured as broadcast.
        char broadcast = 0;

        //Create the socket used for all communication
        if(NET_PROTO_UDP == this->GetProtocol())
        {
            m_Socket = XSocketCreate(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

            broadcast = 1;
        }
        else if(NET_PROTO_VDP == this->GetProtocol())
        {
            m_Socket = XSocketCreate(AF_INET, SOCK_DGRAM, IPPROTO_VDP);
        }
        else
        {
            rthrow(catchall, netError("Invalid socket protocol"));
        }

        rcheck(SOCKET_ERROR != m_Socket,
                catchall,
                netError("socket() failed"));

        //Bind the socket to the requested port and whatever address
        //is given to us from the OS.

        //Bind to INADDR_ANY (i.e. any IP).
        sockaddr_in addr = {0};
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = net_htons(m_Port);

        rcheck(0 == XSocketBind(m_Socket,
                           (const sockaddr*) &addr,
                           sizeof(addr)),
                catchall,
                netError("Failed to bind on port %d", m_Port));

        rverify(this->NativeGetMyAddress(&m_Addr), catchall,);

        m_Port = m_Addr.GetPort();

        netDebug2("I am at " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(m_Addr));

        //Enable broadcasting?

        if(broadcast)
        {
            rcheck(0 == XSocketSetSockOpt(m_Socket,
                                     SOL_SOCKET,
                                     SO_BROADCAST,
                                     &broadcast,
                                     sizeof(BOOL)),
                    catchall,
                    netError("Failed to set socket options"));
        }

        m_CanBroadcast = broadcast ? true : false;

        //Set the blocking mode
        rverify(this->NativeSetBlocking(),
                 catchall,
                 netError("Failed to set socket blocking mode"));
    }
    rcatchall
    {
        if(m_Socket >= 0)
        {
            XSocketClose(m_Socket);
        }

        m_Socket = -1;
    }

    return (m_Socket >= 0);
}

void
netSocket::NativeUnbind()
{
    SYS_CS_SYNC(m_Cs);

    netDebug("Unbinding socket from port %d...", m_Port);

    if(m_Socket >= 0)
    {
        const int skt = m_Socket;
        m_Socket = -1;
        XSocketClose(skt);
    }
}

bool
netSocket::NativeSend(const netSocketAddress& address,
                     const void* buffer,
                     const unsigned bufferSize,
                     netSocketError* sktErr)
{
    int skt = this->GetRawSocket();

    netAssert(skt >= 0);
    netAssert(bufferSize <= MAX_BUFFER_SIZE);

    netSocketError socketErr = NET_SOCKERR_NONE;

    if(address == m_Addr)
    {
        netWarning("Why am I sending a message to myself???");
    }

    sockaddr_in sin = {0};

    sin.sin_addr.s_addr = net_htonl(address.GetIp().ToV4().ToU32());
    sin.sin_port = net_htons(address.GetPort());
    sin.sin_family = AF_INET;

    //FIXME (KB) - remove this hack
    if(NET_PROTO_VDP == m_Proto)
    {
#if __GFWL
		*((u16*)buffer) = net_ntohs(*((u16*)buffer));
#endif

        *((u16*)buffer) -= 2;

    }

    const int result = XSocketSendTo(skt,
                            (const char*) buffer,
                            bufferSize,
                            0,
                            (const struct sockaddr*) &sin,
                            sizeof(sin));

    //FIXME (KB) - remove this hack
    if(NET_PROTO_VDP == m_Proto)
    {
        *((u16*)buffer) += 2;
#if __GFWL
		*((u16*)buffer) = net_htons(*((u16*)buffer));
#endif
    }

    if(SOCKET_ERROR == result)
    {
        //Ignore WSAEWOULDBLOCK
        if(XWSAGetLastError() != WSAEWOULDBLOCK)
        {
            socketErr = netSocket::GetLastSocketError();
            netError("Error sending to [" NET_ADDR_FMT "]: %s",
                        NET_ADDR_FOR_PRINTF(address),
                        netSocketErrorString(socketErr));
        }
    }

    NET_SET_SKTERR(sktErr, socketErr);

    return (NET_SOCKERR_NONE == socketErr);
}

int
netSocket::NativeReceive(netSocketAddress* sender,
                        void *buffer,
                        const int bufferSize,
                        netSocketError* sktErr)
{
    int skt = this->GetRawSocket();

    netAssert(skt >= 0);

    netSocketError socketErr = NET_SOCKERR_NONE;

    int count = 0;

    sockaddr_in sin = {0};
    int fromlen = sizeof(sin);

    count = XSocketRecvFrom(skt,
                    (char*) buffer,
                    bufferSize,
                    0,
                    (struct sockaddr*) &sin,
                    &fromlen);

    if((0 == count) ||
        (SOCKET_ERROR == count && XWSAGetLastError() == WSAEWOULDBLOCK))
    {
        count = 0;
    }
    else if(SOCKET_ERROR == count)
    {
        socketErr = netSocket::GetLastSocketError();
        netError("Error receiving: %s", netSocketErrorString(socketErr));
    }
	else if(NET_PROTO_VDP == m_Proto)
    {
        //FIXME (KB) - remove this hack
        *((u16*)buffer) += 2;
#if __GFWL
		*((u16*)buffer) = net_htons(*((u16*)buffer));
#endif
    }

    if(NET_SOCKERR_NONE == socketErr
        || NET_SOCKERR_CONNECTION_RESET == socketErr)
    {
        sender->Init(netIpAddress(netIpV4Address(net_ntohl(sin.sin_addr.s_addr))), net_ntohs(sin.sin_port));

        //Ignore messages from ourself
        if(*sender == m_Addr)
        {
            count =  0;
        }
    }
    else
    {
        sender->Clear();
    }

    NET_SET_SKTERR(sktErr, socketErr);

    return count;
}

bool
netSocket::NativeSetBlocking()
{
    bool success = false;

    int skt = this->GetRawSocket();

    if(netVerify(skt >= 0))
    {
        u_long nonblock = this->IsBlocking() ? 0 : 1;

        success =
            netVerify(0 == XSocketIOCTLSocket(skt, FIONBIO, &nonblock));

        if(!success)
        {
             netError("Failed to set socket blocking mode");
        }
    }

    return success;
}

bool
netSocket::IsReceivePending() const
{
    bool isPending = false;

    if(this->CanSendReceive())
    {
        int skt = this->GetRawSocket();
        u_long size = 0;

        if(0 == XSocketIOCTLSocket(skt, FIONREAD, &size))
        {
            isPending = size > 0;
        }
        else
        {
             netError("Failed to determine pending data");
        }
    }

    return isPending;
}

}   //namespace rage

#endif  //__LIVE

