// 
// net/peercomplainer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "peercomplainer.h"

#include "diag/output.h"
#include "message.h"
#include "netdiag.h"
#include "system/timer.h"

namespace rage
{

RAGE_DECLARE_CHANNEL(ragenet);
RAGE_DEFINE_SUBCHANNEL(ragenet, peercomplainer)
#undef __rage_channel
#define __rage_channel ragenet_peercomplainer

//PURPOSE
//  Network message used to send a peer complaint.
class netComplaintMsg
{
	friend class netPeerComplainer;

public:
	NET_MESSAGE_DECL(netComplaintMsg, NET_COMPLAINT_MSG);

    void Reset(const u64 myPeerId,
                const u64* complaineeIds,
                const unsigned numComplainees)
    {
		netAssert(numComplainees > 0);
		netAssert(numComplainees <= netPeerComplainer::MAX_PEERS);
        m_ComplainerId = myPeerId;
        m_NumComplainees = numComplainees;

        m_NumComplainees = rage::Min(m_NumComplainees, static_cast<unsigned>(netPeerComplainer::MAX_PEERS));

        for(int i = 0; i < (int) numComplainees; ++i)
        {
            m_ComplaineeIds[i] = complaineeIds[i];
        }
    }

	void Reset(const u64 myPeerId)
	{
		m_ComplainerId = myPeerId;
		m_NumComplainees = 0;
	}

	NET_MESSAGE_SER(bb, msg)
	{
        bool success = false;

        if(bb.SerUns(msg.m_ComplainerId, sizeof(msg.m_ComplainerId)*8)
            && bb.SerUns(msg.m_NumComplainees, 32))
        {
            success = true;

            if (!netVerifyf(msg.m_NumComplainees <= static_cast<unsigned>(netPeerComplainer::MAX_PEERS), "m_NumComplainees[%u] overflow", msg.m_NumComplainees))
            {
                return false;
            }

            for(int i = 0; i < (int) msg.m_NumComplainees; ++i)
            {
                if(!bb.SerUns(msg.m_ComplaineeIds[i], sizeof(msg.m_ComplaineeIds[i])*8))
                {
                    success = false;
                    break;
                }
            }
        }

        return success;
	}

private:
	u64 m_ComplainerId;
    u64 m_ComplaineeIds[netPeerComplainer::MAX_PEERS];
    unsigned m_NumComplainees;
};

NET_MESSAGE_IMPL(netComplaintMsg);

unsigned netPeerComplainer::sm_InitId = 0;

netPeerComplainer::netPeerComplainer()
: m_MyPeerId(0)
, m_pCxnMgr(NULL)
, m_TimeToResendComplaints(0)
, m_bInitialized(false)
, m_bServer(false)
{
	m_ServerEndpointId = NET_INVALID_ENDPOINT_ID;
	m_CxnEventDelegate.Bind(this, &netPeerComplainer::OnCxnEvent);
	m_BootDelegate.Reset();
    m_PeerIds.Reset();
	m_Complaints.Reset();
	m_RegisteredComplainees.Reset();
	m_PeerHasPrevious.Reset();
}

netPeerComplainer::~netPeerComplainer()
{
}

void 
netPeerComplainer::Init(const u64 myPeerId,
                        netConnectionManager* pCxnMgr, 
						const EndpointId serverEndpointId,
						const BootDelegate& appBootHandler,
						const Policies& policies,
						const u64 token)
{
	if(netVerifyf(!m_bInitialized, "Already initialized"))
    {
		m_InitId = sm_InitId++;
		netDebug("[%04u:0x%016" I64FMT "x] Initializing as %s...", m_InitId, token, NET_IS_VALID_ENDPOINT_ID(serverEndpointId) ? "client" : "server");

        m_MyPeerId = myPeerId;
	    m_pCxnMgr = pCxnMgr;
	    m_bServer = !NET_IS_VALID_ENDPOINT_ID(serverEndpointId);
	    if (!m_bServer)
	    {
			netDebug("[%04u:0x%016" I64FMT "x] Server address is " NET_ADDR_FMT "", m_InitId, token, NET_ADDR_FOR_PRINTF(pCxnMgr->GetAddress(serverEndpointId)));
			m_ServerEndpointId = serverEndpointId;
	    }
	    else
	    {
		    m_ServerEndpointId = NET_INVALID_ENDPOINT_ID;
	    }
	    m_BootDelegate = appBootHandler;	

        m_PeerIds.Reset();
	    m_Complaints.Reset();
	    m_RegisteredComplainees.Reset();
		m_PeerHasPrevious.Reset();
	    m_TimeToResendComplaints = 0;
		m_Token = token;
		
		SetPolicies(policies);

	    m_pCxnMgr->AddChannelDelegate(&m_CxnEventDelegate, m_Policies.m_ChannelId);

	    m_bInitialized = true;
    }
}

void
netPeerComplainer::Shutdown()
{	
    if(IsInitialized())
    {
		netDebug("[%04u:0x%016" I64FMT "x] Shutdown...", m_InitId, m_Token);

        m_MyPeerId = 0;
		m_Token = 0;

        if(m_pCxnMgr)
        {
	        m_pCxnMgr->RemoveChannelDelegate(&m_CxnEventDelegate);
	        m_pCxnMgr = NULL;
        }

	    m_ServerEndpointId = NET_INVALID_ENDPOINT_ID;
	    m_BootDelegate.Reset();
        m_PeerIds.Reset();
	    m_Complaints.Reset();
	    m_RegisteredComplainees.Reset();
	    m_TimeToResendComplaints = 0;

        m_bInitialized = false;
    }
}

bool
netPeerComplainer::AddPeer(const u64 peerId
#if !__NO_OUTPUT
						   , const char* friendlyName
#endif
						   )
{
    bool success = false;
    
    if(netVerify(IsInitialized()))
    {
        if(netVerify(!this->HavePeer(peerId))
            && netVerify(!m_PeerIds.IsFull()))
        {
			netDebug2("[%04u:0x%016" I64FMT "x] AddPeer: %s [0x%" I64FMT "x]...", m_InitId, m_Token, friendlyName, peerId);

			Peer* peer = &m_PeerIds.Append();
			peer->Init(peerId OUTPUT_ONLY(, friendlyName));
            success = true;
        }
    }

    return success;
}

void
netPeerComplainer::RemovePeer(const u64 peerId)
{
    if(netVerify(IsInitialized()))
    {
        const int idx = m_PeerIds.Find(peerId);

        if(idx >= 0)
        {
			netDebug2("[%04u:0x%016" I64FMT "x] RemovePeer: %s [0x%" I64FMT "x]...", m_InitId, m_Token, GetFriendlyName(peerId), peerId);

			// remove any complaints about this peer that were registered by the local peer
            this->UnregisterComplaint(peerId);

		    // remove any complaints received for this peer by other remote peers
		    int numComplaints = m_Complaints.GetCount();
		    for(int i = 0; i < numComplaints; ++i)
		    {
			    const Complaint& complaint = m_Complaints[i];
			    if(complaint.m_PeerIdA == peerId || complaint.m_PeerIdB == peerId)
			    {
				    m_Complaints.Delete(i);
				    --i;
				    --numComplaints;
			    }
		    }

		    m_PeerIds.DeleteFast(idx);
        }

		const int hasPrevIdx = m_PeerHasPrevious.Find(peerId);
		
		if(hasPrevIdx >= 0)
		{
			m_PeerHasPrevious.Delete(hasPrevIdx);
		}
    }
}

bool
netPeerComplainer::HavePeer(const u64 peerId) const
{
    return (m_PeerIds.Find(peerId) >= 0);
}

#if !__NO_OUTPUT
static const char* UNKNOWN_PEER = "UNKNOWN PEER";
const char*
netPeerComplainer::GetFriendlyName(const u64 peerId) const
{
	int index = m_PeerIds.Find(peerId);
	
	if(index >= 0)
	{
		return m_PeerIds[index].m_FriendlyName;
	}

	return UNKNOWN_PEER;
}
#endif

void
netPeerComplainer::RegisterComplaint(const u64 otherPeerId)
{
    if(netVerify(IsInitialized()))
    {
        if(netVerify(this->HavePeer(otherPeerId)))
        {
	        //Ignore duplicate complaints
	        const int index = m_RegisteredComplainees.Find(otherPeerId);

	        if (index == -1
                && netVerify(!m_RegisteredComplainees.IsFull()))
	        {
		    	netDebug("[%04u:0x%016" I64FMT "x] Registering complaint for peer %s [0x%" I64FMT "x]...", m_InitId, m_Token, GetFriendlyName(otherPeerId), otherPeerId);

		        //Add complainee to list and send the complaint immediately.
		        m_RegisteredComplainees.Append() = otherPeerId;

		        if(!IsServer())
		        {
			        SendComplaintMessage(otherPeerId);

			        //Schedule a resend if not already scheduled.
			        //All registered complaints will be resent at once.
			        if (m_TimeToResendComplaints == 0)
			        {
				        m_TimeToResendComplaints = sysTimer::GetSystemMsTime() + m_Policies.m_ComplaintResendInterval;
				        m_TimeToResendComplaints |= 0x1; //never 0 when valid
			        }
		        }
	        }
        }
    }
}

void
netPeerComplainer::UnregisterComplaint(const u64 otherPeerId)
{
    if(netVerify(IsInitialized()))
    {
        const int index = m_RegisteredComplainees.Find(otherPeerId);

        if (index != -1)
        {
	   		netDebug("[%04u:0x%016" I64FMT "x] Removing complaint for peer %s [0x%" I64FMT "x]...", m_InitId, m_Token, GetFriendlyName(otherPeerId), otherPeerId);

	        m_RegisteredComplainees.DeleteFast(index);

			//We need to update the server on our complaint list - even if it's now empty
			if(!IsServer())
			{
				netDebug("[%04u:0x%016" I64FMT "x] Sending updated complaint list", m_InitId, m_Token);
				ResendComplaints();
			}
			
            if (m_RegisteredComplainees.GetCount() == 0)
            {
	            m_TimeToResendComplaints = 0;
            }
        }
    }
}

bool
netPeerComplainer::IsComplaintRegistered(const u64 otherPeerId) const
{
	return netVerify(IsInitialized())
            && (m_RegisteredComplainees.Find(otherPeerId) >= 0);
}

void
netPeerComplainer::Update()
{
    if(IsInitialized())
    {
	    if (m_bServer)
	    {
		    CheckComplaints();
		}
	    else
	    {
			u32 curTime = sysTimer::GetSystemMsTime();
		    if (m_TimeToResendComplaints && int(curTime - m_TimeToResendComplaints) >= 0) //handle timer wrapping
		    {
			    //Resend our complaints.
			    netDebug("[%04u:0x%016" I64FMT "x] Time to resend complaints...", m_InitId, m_Token);
			    ResendComplaints();

			    m_TimeToResendComplaints = curTime + m_Policies.m_ComplaintResendInterval;
			    m_TimeToResendComplaints |= 0x1; //never 0 when valid
		    }
	    }
    }
}

void
netPeerComplainer::MigrateServer(const EndpointId serverEndpointId, u64 token)
{
    if(netVerify(IsInitialized()))
    {
		m_Token = token; 
		m_bServer = !NET_IS_VALID_ENDPOINT_ID(serverEndpointId);
		if (!m_bServer)
        {
            netDebug("[%04u:0x%016" I64FMT "x] Migrating server to remote host:" NET_ADDR_FMT "...", m_InitId, m_Token,
                        NET_ADDR_FOR_PRINTF(m_pCxnMgr->GetAddress(serverEndpointId)));

		    m_ServerEndpointId = serverEndpointId;
	    }
	    else
	    {
	        netDebug("[%04u:0x%016" I64FMT "x] Migrating server to local host...", m_InitId, m_Token);
		    m_ServerEndpointId = NET_INVALID_ENDPOINT_ID;
	    }

	    if (m_bServer)
	    {
		    //Stop complaining if we are the new server.
    	    m_RegisteredComplainees.Reset();
            m_TimeToResendComplaints = 0;
            m_Complaints.Reset();
	    }
	    else if(m_RegisteredComplainees.GetCount())
	    {
		    //Clients need to resend complaints to the new server.
		    m_TimeToResendComplaints = (sysTimer::GetSystemMsTime() | 0x1); //never 0 when valid
	    }
    }
}

void 
netPeerComplainer::SetPolicies(const Policies& policies)
{
	netAssert(policies.m_BootDelay > 0);
	netAssert(policies.m_ComplaintResendInterval > 0);
	netAssert(policies.m_ChannelId <= NET_MAX_CHANNEL_ID);
	netAssert(policies.m_ChannelId != NET_INVALID_CHANNEL_ID);
	m_Policies = policies;

	netDebug("[%04u:0x%016" I64FMT "x] SetPolicies :: BootDelay: %u, ResendInterval: %u, ChannelID: %u",
			 m_InitId,	 
			 m_Token,
			 policies.m_BootDelay,
			 policies.m_ComplaintResendInterval,
			 policies.m_ChannelId);
}
void
netPeerComplainer::OnCxnEvent(netConnectionManager* UNUSED_PARAM(cxnMgr),
								const netEvent* evt)
{
	if (NET_EVENT_FRAME_RECEIVED == evt->GetId())
	{
		netComplaintMsg msg;

		if (IsServer()
			&& msg.Import(evt->m_FrameReceived->m_Payload, 
                            evt->m_FrameReceived->m_SizeofPayload)
            && this->HavePeer(msg.m_ComplainerId))
		{
#if !__NO_OUTPUT
            for(int i = 0; i < (int) msg.m_NumComplainees; ++i)
            {
                if(m_MyPeerId == msg.m_ComplaineeIds[i])
                {
                    netDebug("[%04u:0x%016" I64FMT "x] Received complaint against HOST from [" NET_ADDR_FMT "] %s [0x%" I64FMT "x]",
						m_InitId,
						m_Token,
                        NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender),
                        GetFriendlyName(msg.m_ComplainerId),
                        msg.m_ComplainerId);
                }
                else
                {
                    netDebug("[%04u:0x%016" I64FMT "x] Received complaint against %s [0x%" I64FMT "x] from [" NET_ADDR_FMT "] %s [0x%" I64FMT "x]",
						m_InitId,
						m_Token,
						GetFriendlyName(msg.m_ComplaineeIds[i]),
                        msg.m_ComplaineeIds[i],
                        NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender),
                        GetFriendlyName(msg.m_ComplainerId),
                        msg.m_ComplainerId);
                }
            }
#endif  //!__NO_OUTPUT

			ReceiveComplaints(msg.m_ComplainerId,
                            msg.m_ComplaineeIds,
                            msg.m_NumComplainees);
		}
	}
}

void
netPeerComplainer::ReceiveComplaints(const u64 complainerId,
                                    const u64* complaineeIds,
                                    const unsigned numComplainees)
{
    netAssert(numComplainees <= MAX_PEERS);

    if(this->IsServer())
    {
		//numComplainees can be 0 if we are receiving a message from a client
		//that has removed his only complaint
        for(int i = 0; i < (int) numComplainees; ++i)
        {
            if(!this->HavePeer(complaineeIds[i]))
            {
                //We don't know of the complainee - ignore it.
                continue;
            }

	        //Add to list of complaints if it's not a duplicate.
            Complaint tmp;
	        Complaint* comp = NULL;

            tmp.Update(complainerId, complaineeIds[i]);
            const int idx = m_Complaints.Find(tmp);

	        if(idx < 0)
	        {
		        if (netVerify(!m_Complaints.IsFull()))
		        {
			        comp = &m_Complaints.Append();
					comp->Clear();

					//Apply the time this complaint was first received
					comp->m_TimeReceived = sysTimer::GetSystemMsTime();
		        }
	        }
            else
            {
                comp = &m_Complaints[idx];
            }

            if(comp)
            {
                comp->Update(complainerId, complaineeIds[i]);
            }
        }

		//Find complaints made by this peer that are no longer in the 
		//list of received complaints - and remove those
		int numComplaints = m_Complaints.GetCount();
		for(int i = 0; i < numComplaints; ++i)
		{
			Complaint& complaint = m_Complaints[i];
			bool complainerIsA = (complaint.m_PeerIdA == complainerId) && complaint.m_AAgainstB;
			bool complainerIsB = (complaint.m_PeerIdB == complainerId) && complaint.m_BAgainstA;

			if(complainerIsA ||
				complainerIsB)
			{
				//Check list of complainees
				bool found = false;
				for(int j = 0; j < (int) numComplainees; ++j)
				{
					if((complainerIsA && complaint.m_PeerIdB == complaineeIds[j]) ||
						(complainerIsB && complaint.m_PeerIdA == complaineeIds[j]))
					{
						found = true;
						break;
					}
				}

				//Complainer is no longer complaining about the complainee
				if(!found)
				{
					//Remove the complainer from the complaint
					if(complainerIsA)
					{
						complaint.m_AAgainstB = false;
					}

					if(complainerIsB)
					{
						complaint.m_BAgainstA = false;
					}

					//If this complaint is no longer valid - punt it
					if(!complaint.m_AAgainstB && !complaint.m_BAgainstA)
					{
						m_Complaints.Delete(i);
						--i;
						--numComplaints;
					}
				}
			}
		}
    }
}

void
netPeerComplainer::SendComplaintMessage(u64 complaineeId)
{
    if(netVerify(IsInitialized()))
    {
	    //Try to send complaint message to the server.
	    if (netVerify(!IsServer()))
	    {
		    netComplaintMsg msg;
            msg.Reset(m_MyPeerId, &complaineeId, 1);

		    m_pCxnMgr->SendOutOfBand(m_ServerEndpointId, m_Policies.m_ChannelId, msg, 0);
	    }
    }
}

void
netPeerComplainer::ResendComplaints()
{
    if(netVerify(IsInitialized()))
    {
	    //Resend all our registered complaints.
	    if(netVerify(!IsServer()))
	    {
		    netComplaintMsg msg;

			int numComplainees = m_RegisteredComplainees.GetCount();
			if(numComplainees > 0)
			{
				msg.Reset(m_MyPeerId,
					&m_RegisteredComplainees[0],
					m_RegisteredComplainees.GetCount());
			}
			else
			{
				msg.Reset(m_MyPeerId);
			}
            
		    m_pCxnMgr->SendOutOfBand(m_ServerEndpointId, m_Policies.m_ChannelId, msg, 0);
	    }
    }
}

//Used for sorting boot candidates.
struct BootCandidateComparer
{
    //Sort in descending order.
    bool operator()(const netPeerComplainer::BootCandidate& a,
                    const netPeerComplainer::BootCandidate& b) const
    {
        return a.m_NumComplaints > b.m_NumComplaints;
    }
};

void 
netPeerComplainer::CheckComplaints()
{
	u32 curTime = sysTimer::GetSystemMsTime();

	//Drop complaints where one or both players have gone
	int numComplaints = m_Complaints.GetCount();
	for(int i = 0; i < numComplaints; ++i)
	{
		const Complaint& complaint = m_Complaints[i];

		if(!this->HavePeer(complaint.m_PeerIdA)
			|| !this->HavePeer(complaint.m_PeerIdB))
		{
			m_Complaints.Delete(i);
			--i;
			--numComplaints;
		}
	}

	//Check remaining complaints 
	bool bProcessingComplaints = true;
	while(bProcessingComplaints)
	{
		//Assume all complaints have been processed
		bProcessingComplaints = false;

		numComplaints = m_Complaints.GetCount();
		for(int i = 0; i < numComplaints; ++i)
		{
			const Complaint& complaint = m_Complaints[i];

    		//Check if we should deal with this complaint
			if(int(curTime - (complaint.m_TimeReceived + m_Policies.m_BootDelay)) >= 0)
			{
				//Complaints can be deleted within ProcessComplaint. 
				ProcessComplaint(complaint);
				bProcessingComplaints = true;
				break;
			}
		}
	}
}

void 
netPeerComplainer::ProcessComplaint(const Complaint& complaintToProcess)
{
	//Array of candidates to be booted
	atFixedArray<BootCandidate, MAX_PEERS> candidates;
	BootCandidate* bc;

	//Find all other complaints with either the complainer OR complainee present
	int numComplaints = m_Complaints.GetCount();
	for(int i = 0; i < numComplaints; ++i)
	{
		const Complaint& complaint = m_Complaints[i];
		if(!(complaint.m_PeerIdA == complaintToProcess.m_PeerIdA || complaint.m_PeerIdA == complaintToProcess.m_PeerIdB ||
			 complaint.m_PeerIdB == complaintToProcess.m_PeerIdA || complaint.m_PeerIdB == complaintToProcess.m_PeerIdB))
		{
			//Neither player involved in this complaint. The time will come to deal with these guys. 
			continue;
		}

		//Keep a copy of the pair involved in the complaint
		BootCandidate bcA(complaint.m_PeerIdA, 0);
		BootCandidate bcB(complaint.m_PeerIdB, 0);

		netDebug("[%04u:0x%016" I64FMT "x] %s [\t0x%" I64FMT "x] is complaining about %s [0x%" I64FMT "x]", m_InitId, m_Token, GetFriendlyName(complaint.m_PeerIdA), complaint.m_PeerIdA, GetFriendlyName(complaint.m_PeerIdB), complaint.m_PeerIdB);
		if(netVerify(HavePeer(complaint.m_PeerIdA)) && netVerify(HavePeer(complaint.m_PeerIdB)))
		{
			int idx = candidates.Find(bcA);
			if(idx < 0)
			{
				bc = &candidates.Append();
				*bc = bcA;
			}
			else
			{
				bc = &candidates[idx];
			}

			++bc->m_NumComplaints;

			if(m_PeerHasPrevious.Find(bc->m_PeerId))
			{
				bc->m_HasPrevious = true; 
			}

			idx = candidates.Find(bcB);
			if(idx < 0)
			{
				bc = &candidates.Append();
				*bc = bcB;
			}
			else
			{
				bc = &candidates[idx];
			}

			if(m_PeerHasPrevious.Find(bc->m_PeerId))
			{
				bc->m_HasPrevious = true; 
			}

			++bc->m_NumComplaints;
		}
	}

	const unsigned numCandidates = candidates.GetCount();
	if(candidates.GetCount())
	{
#if !__NO_OUTPUT
		netDebug("[%04u:0x%016" I64FMT "x] \tBoot Candidates List:\n", m_InitId, m_Token);
		for(int i = 0; i < (int) numCandidates; ++i)
		{
			netDebug("[%04u:0x%016" I64FMT "x] %s [\t0x%" I64FMT "x] has %d complaints", m_InitId, m_Token, GetFriendlyName(candidates[i].m_PeerId), candidates[i].m_PeerId, candidates[i].m_NumComplaints);
		}
#endif
		//Sort candidates in descending order of the number of complaints they received.
		bc = &candidates[0];
		std::sort(bc, bc + candidates.GetCount(), BootCandidateComparer());

		m_BootDelegate(this, &candidates[0], numCandidates);

		//Find out who was booted
		for(int i = 0; i < (int) numCandidates; ++i)
		{
			BootCandidate& candidate = candidates[i];
			if(candidate.m_Booted)
			{
				//Remove any complaints with a booted player tagged
				int numComplaints = m_Complaints.GetCount();
				for(int j = 0; j < numComplaints; ++j)
				{
					const Complaint& complaint = m_Complaints[j];
					if(complaint.m_PeerIdA == candidate.m_PeerId || complaint.m_PeerIdB == candidate.m_PeerId)
					{
						//If the candidate who made the complaint is booted, flag the remaining peer as having 'previous'
						//This indicates that a player has previously been complained about
						if(((complaint.m_PeerIdA == candidate.m_PeerId && complaint.m_AAgainstB && !complaint.m_BAgainstA) ||
							(complaint.m_PeerIdB == candidate.m_PeerId && complaint.m_BAgainstA && !complaint.m_AAgainstB)) && 
							netVerify(!m_PeerHasPrevious.IsFull()))
						{
							m_PeerHasPrevious.Push(complaint.m_PeerIdB);
						}

						m_Complaints.Delete(j);
						--j;
						--numComplaints;
					}
				}
            }

			//Reset booted tag
			candidate.m_Booted = false;
		}
    }

	// make sure we delete the complaint we're processing
	const int idx = m_Complaints.Find(complaintToProcess);
	if(idx >= 0)
	{
		m_Complaints.Delete(idx);
    }
}

} //namespace rage
