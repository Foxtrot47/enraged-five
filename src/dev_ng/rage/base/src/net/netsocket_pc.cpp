// 
// net/netsocket_pc.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#include "file/file_config.h"

#if (RSG_PC || RSG_DURANGO)

#include "netsocket.h"
#include "diag/seh.h"
#include "netdiag.h"
#include "nethardware.h"
#include "system/memory.h"
#include "system/timer.h"

#define STRICT
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma warning(pop)

#define NET_SET_SKTERR(se, e)   while((se)){*(se) = (e);break;}

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, socket_pc)
#undef __rage_channel
#define __rage_channel ragenet_socket_pc

netSocketError
netSocket::GetLastSocketError()
{
    netSocketError err = NET_SOCKERR_NONE;

    const int lasterror = WSAGetLastError();

    switch(lasterror)
    {
        case 0:
            break;

        case WSAENETDOWN:
            err = NET_SOCKERR_NETWORK_DOWN;
            break;

        case WSAENOBUFS:
            err = NET_SOCKERR_NO_BUFFERS;
            break;

        case WSAEMSGSIZE:
            err = NET_SOCKERR_MESSAGE_TOO_LONG;
            break;

        case WSAEHOSTUNREACH:
            err = NET_SOCKERR_HOST_UNREACHABLE;
            break;

        case WSAECONNRESET:
            err = NET_SOCKERR_CONNECTION_RESET;
            break;

        case WSAEWOULDBLOCK:
            err = NET_SOCKERR_WOULD_BLOCK;
            break;

        case WSAEINPROGRESS:
            err = NET_SOCKERR_IN_PROGRESS;
            break;

		case WSAEINVAL:
			err = NET_SOCKERR_INVALID_ARG;
			break;

        default:
            netError("WSAGetLastError() = %d", lasterror);
            err = NET_SOCKERR_UNKNOWN;
            break;
    }

    return err;
}

unsigned
netSocket::SizeofHeader()
{
/*
	Summary (worst case assumed):
	PC:
		IPv4: IPv4 (20) + UDP (8) = 28 bytes
		or IPv6: IPv6 (40) + UDP (8) + Tunneling (28) = 76 bytes
	Durango w/ Secure Sockets:
		IPv6 (40) + UDP (8) + Tunneling (28) + Security (38) = 114 bytes.

	Note: this doesn't account for Ethernet packet overhead
*/

#if IPv4_IPv6_DUAL_STACK
	// IPv6 packet headers are 40 bytes
	static const unsigned SIZEOF_IP_HEADER = 40;
#else
	// IPv4 packet headers are 20 bytes
	static const unsigned SIZEOF_IP_HEADER = 20;
#endif

	// we're not using Xbox One Secure Sockets
	static const unsigned SIZEOF_SECURITY_HEADERS = 0;

    static const unsigned SIZEOF_UDP_HEADER = 8;

#if IPv4_IPv6_DUAL_STACK
	// if we're communicating with an IPv4 endpoint/proxy
	// IPv4 and UDP encapsulation headers are added to tunnel through the network
	static const unsigned SIZEOF_IPV4_HEADER = 20;
	static const unsigned SIZEOF_TUNNELLING_HEADERS = SIZEOF_IPV4_HEADER + SIZEOF_UDP_HEADER;
#else
	static const unsigned SIZEOF_TUNNELLING_HEADERS = 0;
#endif

	static const unsigned SIZEOF_HEADER = SIZEOF_IP_HEADER +
										  SIZEOF_UDP_HEADER + 
										  SIZEOF_SECURITY_HEADERS + 
										  SIZEOF_TUNNELLING_HEADERS;

    return SIZEOF_HEADER;
}

unsigned
netSocket::SizeofPacket(const unsigned sizeofPayload)
{
    return this->SizeofHeader() + sizeofPayload;
}

//protected:

void
netSocket::NativeUpdate()
{
#if RSG_BANK
	if(CanSendReceive())
	{
		const unsigned curTime = sysTimer::GetSystemMsTime();
		const unsigned elaspedTime = curTime - m_LastDiagnosticCheckTime;
		if(elaspedTime >= 1000)
		{
			m_LastDiagnosticCheckTime = curTime;

			unsigned rcvQueueLength = 0;
			if(NativeGetReceiveBufferQueueLength(rcvQueueLength))
			{
				unsigned sendBufSize;
				unsigned rcvBufSize;
				NativeGetSendReceiveBufferSizes(sendBufSize, rcvBufSize);

				const float warnPct = NET_SOCKET_BUFFER_WARN_PCT / 100.0f;
				const bool warn = (rcvQueueLength > (rcvBufSize * warnPct));

				if(warn)
				{
					netWarning("[%u] rx_buf: %d/%d", m_Id, rcvQueueLength, rcvBufSize);
				}
			}
		}
	}
#endif
}

void
netSocket::NativeShutdown()
{
    this->NativeUnbind();
}

bool
netSocket::NativeGetMyAddress(netSocketAddress* addr) const
{
    bool success = false;

    addr->Clear();

    ptrdiff_t skt = this->GetRawSocket();

	if(netVerify(skt >= 0))
	{
		sockaddr_storage sin = {0};
		socklen_t sin_len = sizeof(sin);

		// getsockname gives us the port that got assigned to this socket.
		// The IP address we get here will be unspecified since we
		// bound to 'any' address. Use netHardware to get the local IP (below).
		if(netVerify(0 == getsockname((int)skt, (sockaddr*)&sin, &sin_len)))
		{
			const netSocketAddress tmpAddr((sockaddr*)&sin);
			const unsigned short port = tmpAddr.GetPort();

			if(port > 0)
			{
				netIpAddress ip;
				netHardware::GetLocalIpAddress(&ip);

				success = addr->Init(ip, port) && netVerify(addr->IsValid());
			}
		}
	}

	return success;
}

bool
netSocket::NativeBind()
{
    SYS_CS_SYNC(m_Cs);

    netAssert(netHardware::IsAvailable());
    netAssert(m_Socket < 0);
    netAssert(NET_PROTO_UDP == this->GetProtocol());

    netDebug("[%u] Binding socket on port %d...", m_Id, m_Port);

	rtry
    {
#if IPv4_IPv6_DUAL_STACK
		int addressFamily = AF_INET6;
#else
		int addressFamily = AF_INET;
#endif
		m_Socket = socket(addressFamily, SOCK_DGRAM, IPPROTO_UDP);

		rcheck(INVALID_SOCKET != m_Socket,
			catchall,
			netError("[%u] socket() failed", m_Id));

		// When an ephemeral port is selected, we use the SO_RANDOMIZE_PORT socket option which 
		// automatically selects a random port in the unreserved ephemeral range (on Xbox One,
		// this range is 57344-65535 as of the July 2017 XDK, also works on Windows Vista and up).
		// This is important for certain NAT traversal features where it is preferred to use a
		// port that does not have an active mapping on the local NAT (i.e. a port that hasn't
		// been recently used). Note, always set this option in case we fall back to ephemeral.

		// Note that on Xbox One, the app manifest must specify a socket description with
		// BoundPort="0" for ephemeral port binding to work.
		u16 randomPort = 1;

		if(0 != setsockopt(m_Socket,
							SOL_SOCKET,
							SO_RANDOMIZE_PORT,
							(const char*)&randomPort,
							sizeof(u16)))
		{
			netError("[%u] Failed to set SO_RANDOMIZE_PORT socket option", m_Id);
		}

#if IPv4_IPv6_DUAL_STACK
		// disable IPv6-only mode, we support IPv4-mapped IPv6 addresses
		int v6only = 0;
		netVerify(setsockopt(m_Socket,
								IPPROTO_IPV6,
								IPV6_V6ONLY,
								(const char*) &v6only,
								sizeof(v6only)) == 0);
#endif

        //Bind the socket to the requested port and whatever address
        //is given to us from the OS.
#if IPv4_IPv6_DUAL_STACK
		//Bind to in6addr_any (i.e. any IP).
		sockaddr_in6 addr = {0};
		addr.sin6_addr = in6addr_any;
		addr.sin6_family = AF_INET6;
		addr.sin6_port = net_htons(m_Port); 
#else
        //Bind to INADDR_ANY (i.e. any IP).
        sockaddr_in addr = {0};
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = net_htons(m_Port);
#endif

		if(0 != bind(m_Socket,
					(const sockaddr*) &addr,
					sizeof(addr)))
		{
			netError("[%u] Failed to bind on port %u (port probably already in use - try 'netstat -anb' to find out which process has it open). Will attempt port 0 (ephemeral port) (LastError: %d)", m_Id, m_Port, netSocket::GetLastSocketError());

#if IPv4_IPv6_DUAL_STACK
			addr.sin6_port = net_htons(0); 
#else
			addr.sin_port = net_htons(0);
#endif

			rcheck(0 == bind(m_Socket,
							(const sockaddr*) &addr,
							sizeof(addr)),
					catchall,
					netError("[%u] Failed to bind on port 0 (LastError: %d)", m_Id, netSocket::GetLastSocketError()));
		}

        rverify(this->NativeGetMyAddress(&m_Addr), catchall,);

        m_Port = m_Addr.GetPort();

        netDebug("[%u] I am at " NET_ADDR_FMT " with port: %u", m_Id, NET_ADDR_FOR_PRINTF(m_Addr), m_Port);

		// broadcasting isn't supported in IPv6
#if IPv4_IPv6_DUAL_STACK
		m_CanBroadcast = false;
#else
        //Enable broadcasting
        char broadcast = 1;

        rcheck(0 == setsockopt(m_Socket,
                                 SOL_SOCKET,
                                 SO_BROADCAST,
                                 (const char*) &broadcast,
                                 sizeof(BOOL)),
                catchall,
                netError("[%u] Failed to set socket options", m_Id));

        m_CanBroadcast = (broadcast == 1);
#endif

		//Set the desired send/receive buffer sizes. Note: if this fails,
		// we fall back to default values and do not return an error.
		this->SetSendReceiveBufferSizes(m_DesiredSendBufferSize, m_DesiredReceiveBufferSize);

        //Set the blocking mode
        rverify(this->NativeSetBlocking(),
                 catchall,
                 netError("[%u] Failed to set socket blocking mode", m_Id));
    }
    rcatchall
    {
        if(m_Socket >= 0)
        {
            closesocket(m_Socket);
        }

        m_Socket = -1;
    }

    return (m_Socket >= 0);
}

void
netSocket::NativeUnbind()
{
    SYS_CS_SYNC(m_Cs);

    netDebug("[%u] Unbinding socket from port %d...", m_Id, m_Port);

    if(m_Socket >= 0)
    {
        const ptrdiff_t skt = m_Socket;
        m_Socket = -1;
        closesocket(skt);
    }
}

bool
netSocket::NativeSend(const netSocketAddress& address,
					  const void* buffer,
					  const unsigned bufferSize,
					  netSocketError* sktErr)
{
    ptrdiff_t skt = this->GetRawSocket();

    netAssert(skt >= 0);
    netAssert(bufferSize <= MAX_BUFFER_SIZE);

    netSocketError socketErr = NET_SOCKERR_NONE;

    if(address == m_Addr)
    {
        netWarning("Why am I sending a message to myself???");
    }

	sockaddr_storage sin;
	address.ToSockAddr((sockaddr*)&sin, sizeof(sin));
	int sin_len = sizeof(sin);

    const int result = sendto(skt,
							  (const char*) buffer,
							  bufferSize,
							  0,
							  (const struct sockaddr*) &sin,
							  sin_len);

    if(SOCKET_ERROR == result)
    {
        //Ignore WSAEWOULDBLOCK
        if(WSAGetLastError() != WSAEWOULDBLOCK)
        {
            socketErr = netSocket::GetLastSocketError();
            netError("[%u] Error sending to [" NET_ADDR_FMT "]: %s (Code: %d, WSA: %d)", 
					 m_Id,
					 NET_ADDR_FOR_PRINTF(address),
					 netSocketErrorString(socketErr),
					 socketErr,
					 WSAGetLastError());
        }
    }

    NET_SET_SKTERR(sktErr, socketErr);

    return (NET_SOCKERR_NONE == socketErr);
}

int
netSocket::NativeReceive(netSocketAddress* sender,
                        void *buffer,
                        const int bufferSize,
                        netSocketError* sktErr)
{
    ptrdiff_t skt = this->GetRawSocket();

    netAssert(skt >= 0);

    netSocketError socketErr = NET_SOCKERR_NONE;

    int count = 0;

	sockaddr_storage sin = {0};
	int sin_len = sizeof(sin);

    count = recvfrom(skt,
					 (char*) buffer,
					 bufferSize,
					 0,
					 (sockaddr*) &sin,
					 &sin_len);

    if((0 == count)
        || ((SOCKET_ERROR == count) && (WSAGetLastError() == WSAEWOULDBLOCK)))
    {
		// there was no data to receive
        count = 0;
    }
    else if(SOCKET_ERROR == count)
    {
        socketErr = netSocket::GetLastSocketError();

#if !__NO_OUTPUT
		netSocketAddress addr((sockaddr*)&sin);

		if (socketErr == NET_SOCKERR_CONNECTION_RESET)
		{
			netDebug1("[%u] Error receiving from " NET_ADDR_FMT ": %s", m_Id, NET_ADDR_FOR_PRINTF(addr), netSocketErrorString(socketErr));
		}
		else
		{
			netError("[%u] Error receiving from " NET_ADDR_FMT ": %s", m_Id, NET_ADDR_FOR_PRINTF(addr), netSocketErrorString(socketErr));
		}
#endif
    }

    if(NET_SOCKERR_NONE == socketErr
        || NET_SOCKERR_CONNECTION_RESET == socketErr)
    {
		if(count > 0)
		{
 			sender->Init((sockaddr*)&sin);
		}

        //Ignore messages from ourself
        if((count != 0) && (*sender == m_Addr))
        {
            count = 0;
        }
    }
    else
    {
        sender->Clear();
    }

    NET_SET_SKTERR(sktErr, socketErr);

    return count;
}

bool
netSocket::NativeSetBlocking()
{
    bool success = false;

    ptrdiff_t skt = this->GetRawSocket();

    if(netVerify(skt >= 0))
    {
        u_long nonblock = this->IsBlocking() ? 0 : 1;

        success =
            netVerify(0 == ioctlsocket(skt, FIONBIO, &nonblock));

        if(!success)
        {
             netError("[%u] Failed to set socket blocking mode", m_Id);
        }
    }

    return success;
}

bool
netSocket::NativeGetSendReceiveBufferSizes(unsigned& sendBufferSize,
										   unsigned& receiveBufferSize) const
{
	rtry
	{
		int sendbuff = 0;
		socklen_t optlen = sizeof(sendbuff);
		int res = getsockopt(m_Socket, SOL_SOCKET, SO_SNDBUF, (char*)&sendbuff, &optlen);
		rverify(res == 0, catchall, netError("[%u] Failed to get send buffer size (LastError: %d)", m_Id, netSocket::GetLastSocketError()));

		int rcvbuff = 0;
		optlen = sizeof(rcvbuff);
		res = getsockopt(m_Socket, SOL_SOCKET, SO_RCVBUF, (char*)&rcvbuff, &optlen);
		rverify(res == 0, catchall, netError("[%u] Failed to get receive buffer size (LastError: %d)", m_Id, netSocket::GetLastSocketError()));

		sendBufferSize = (unsigned)sendbuff;
		receiveBufferSize = (unsigned)rcvbuff;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netSocket::NativeSetSendReceiveBufferSizes(const unsigned sendBufferSize,
										   const unsigned receiveBufferSize)
{
	rtry
	{
		ptrdiff_t skt = this->GetRawSocket();

		rverify(skt >= 0, catchall, );

		int sendBuff = (int)sendBufferSize;
		int res = setsockopt(m_Socket, SOL_SOCKET, SO_SNDBUF, (char*)&sendBuff, sizeof(sendBuff));
		rverify(res == 0, catchall, netError("[%u] Failed to set send buffer size (LastError: %d)", m_Id, netSocket::GetLastSocketError()));

		int rcvBuff = (int)receiveBufferSize;
		res = setsockopt(m_Socket, SOL_SOCKET, SO_RCVBUF, (char*)&rcvBuff, sizeof(rcvBuff));
		rverify(res == 0, catchall, netError("[%u] Failed to set receive buffer size (LastError: %d)", m_Id, netSocket::GetLastSocketError()));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netSocket::NativeGetReceiveBufferQueueLength(unsigned& receiveBufferQueueLength) const
{
	receiveBufferQueueLength = 0;

	const ptrdiff_t skt = this->GetRawSocket();

	if(skt >= 0)
	{
		u_long rcvQueueLength = 0;
		if(ioctlsocket(skt, FIONREAD, &rcvQueueLength) == 0)
		{
			receiveBufferQueueLength = rcvQueueLength;
			return true;
		}
	}

	return false;
}

void
netSocket::NativeGetPortRangeForRandomSelection(unsigned short& lowerBound, unsigned short& upperBound)
{
	// We use the ephemeral port with the SO_RANDOMIZE_PORT socket option which automatically
	// selects a random port in the unreserved ephemeral range.
	lowerBound = 0;
	upperBound = 0;
}

void
netSocket::NativeGetPortRangeForP2pConnections(unsigned short& lowerBound, unsigned short& upperBound)
{
	lowerBound = 0;
	upperBound = 65535;
}

bool
netSocket::IsReceivePending() const
{
    bool isPending = false;

    if(this->CanSendReceive())
    {
        ptrdiff_t skt = this->GetRawSocket();
        u_long size = 0;

        if(0 == ioctlsocket(skt, FIONREAD, &size))
        {
            isPending = size > 0;
        }
        else
        {
             netError("[%u] Failed to determine pending data", m_Id);
        }
    }

    return isPending;
}

}   //namespace rage

#endif
