// 
// net/transaction.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_TRANSACTION_H
#define NET_TRANSACTION_H

#include "net/connectionmanager.h"
#include "net/message.h"
#include "net/packet.h"

namespace rage
{

//PURPOSE
//  a netRequester object enables request/response protocols
//  Before sending a request a callback should be bound
//  to the requester object.  The callback will be called with
//  various events related to the request (e.g. the request was
//  received, the request was canceled, a response was received,
//  etc.).
//  After binding a callback to the requester the request can be
//  sent by calling one of the Send() methods
//
//  Note - this is a work in progress and is subject to change.

class netTransactor;

const unsigned NET_INVALID_TRANSACTION_ID   = ~0u;

enum netTransactionType
{
    NET_TXTYPE_INVALID,
    NET_TXTYPE_REQUEST,
    NET_TXTYPE_RESPONSE,
};

//PURPOSE
//  API for handling network transactions.
class netTransaction
{
    friend class netRequester;

    //Signatures to identify request/response messages from other types of data.
    static const unsigned SIG_RQST =
        NET_MAKE_SIG4('R'-'A', 'Q'-'A', 'S'-'A', 'T'-'A');
    static const unsigned SIG_RESP =
        NET_MAKE_SIG4('R'-'A', 'E'-'A', 'S'-'A', 'P'-'A');
    static const unsigned SIG_INVALID   = SIG_RQST ^ SIG_RESP;

public:

    enum
    {
        MAX_BYTE_SIZEOF_TXN = netFrame::MAX_BYTE_SIZEOF_PAYLOAD,
    };

    enum
    {
        BIT_SIZEOF_SIG      = datBitsNeeded<SIG_RQST>::COUNT,
        BIT_SIZEOF_TXID     = 32,

        BIT_OFFSETOF_SIG    = 0,
        BIT_OFFSETOF_TXID   = BIT_OFFSETOF_SIG + BIT_SIZEOF_SIG,

        BIT_SIZEOF_HEADER   = BIT_OFFSETOF_TXID + BIT_SIZEOF_TXID,

        BYTE_SIZEOF_HEADER  = (BIT_SIZEOF_HEADER + 7) >> 3,

        MAX_BYTE_SIZEOF_PAYLOAD = MAX_BYTE_SIZEOF_TXN - BYTE_SIZEOF_HEADER,
    };

    //PURPOSE
    //  Initializes a buffer as a request.
    //PARAMS
    //  buf         - Destination buffer
    //  sizeofBuf   - Size in bytes of the buffer
    //  txId        - Optional.  On return will contain the transaction id.
    //RETURNS
    //  Number of bytes written to the buffer.
    static unsigned InitRequest(void* buf,
                                const unsigned sizeofBuf,
                                unsigned* txId = 0);

    //PURPOSE
    //  Initializes a buffer as a resent request.
    //PARAMS
    //  buf         - Destination buffer
    //  sizeofBuf   - Size in bytes of the buffer
    //  txId        - Id of the transaction request to be resent.
    //RETURNS
    //  Number of bytes written to the buffer.
    //NOTES
    //  This variant is used when resending transactions.  The id must
    //  have been generated by a prior call to the above variant of
    //  InitRequest().
    static unsigned InitRequest(void* buf,
                                const unsigned sizeofBuf,
                                const unsigned txId);

    //PURPOSE
    //  Initializes a buffer as a response.
    //PARAMS
    //  buf         - Destination buffer
    //  sizeofBuf   - Size in bytes of the buffer
    //  txId        - Transaction id of the request to which the response
    //                corresponds.
    //RETURNS
    //  Number of bytes written to the buffer.
    static unsigned InitResponse(void* buf,
                                const unsigned sizeofBuf,
                                const unsigned txId);

    //PURPOSE
    //  Returns true if the buffer contains a transaction.
    //PARAMS
    //  buf         - Transaction buffer
    //  sizeofBuf   - Size in bytes of the buffer
    static bool IsTransaction(const void* buf,
                                const unsigned sizeofBuf);

    //PURPOSE
    //  Returns true if the buffer contains a request.
    //PARAMS
    //  buf         - Transaction buffer
    //  sizeofBuf   - Size in bytes of the buffer
    static bool IsRequest(const void* buf,
                            const unsigned sizeofBuf);

    //PURPOSE
    //  Returns true if the buffer contains a response.
    //PARAMS
    //  buf         - Transaction buffer
    //  sizeofBuf   - Size in bytes of the buffer
    static bool IsResponse(const void* buf,
                            const unsigned sizeofBuf);

    //PURPOSE
    //  Returns the payload of the transaction contained in the buffer
    //  and writes the size of the payload to sizeofPayload.
    static const void* GetPayload(const void* buf,
                                const unsigned sizeofBuf,
                                unsigned* sizeofPayload);

    //PURPOSE
    //  Extracts the transaction id from the buffer.
    //RETURNS
    //  Type of the transaction contained in the buffer.
    static netTransactionType GetId(const void* buf,
                                    const unsigned sizeofBuf,
                                    unsigned* txId);

private:

    //PURPOSE
    //  Generates a new transaction id.
    static unsigned NextId();

    //PURPOSE
    //  Reads the transaction header from the buffer.
    //RETURNS
    //  Number of bytes read.
    static unsigned ReadHeader(unsigned* sig,
                                unsigned* txId,
                                const void* buf,
                                const unsigned sizeofBuf);

    //PURPOSE
    //  Writes the transaction header to the buffer.
    //RETURNS
    //  Number of bytes written.
    static unsigned WriteHeader(const unsigned sig,
                                const unsigned txId,
                                void* buf,
                                const unsigned sizeofBuf);
};

//PURPOSE
//  Information about a transaction passed to request handlers.  Used
//  to refer to a transaction when responding to a request.
class netTransactionInfo
{
public:

    netTransactionInfo()
    {
        this->Clear();
    }

    void Clear()
    {
        m_TxId = NET_INVALID_TRANSACTION_ID;
        m_Addr.Clear();
		m_EndpointId = NET_INVALID_ENDPOINT_ID;
		m_ChannelId = NET_INVALID_CHANNEL_ID;
		m_CxnId = -1;
    }

    void Reset(const unsigned txId,
                const netAddress& addr,
				const EndpointId endpointId,
                const ChannelId channelId,
				const int cxnId)
    {
        m_TxId = txId;
        m_Addr = addr;
		m_EndpointId = endpointId;
		m_ChannelId = channelId;
		m_CxnId = cxnId;
    }

    unsigned m_TxId;
    netAddress m_Addr;
	EndpointId m_EndpointId;
    ChannelId m_ChannelId;
	int m_CxnId;
};

//PURPOSE
//  Instances of netRequest are passed to request handlers.
class netRequest
{
public:

    netRequest()
        : m_Data(0)
        , m_SizeofData(0)
    {
    }

    netTransactionInfo m_TxInfo;
    const void* m_Data;
    unsigned m_SizeofData;
};

//PURPOSE
//  Instances of netResponse are passed to response handlers.
class netResponse
{
public:

    enum ResponseCode
    {
        REQUEST_RECEIVED,   //Request received by remote peer.
        REQUEST_ANSWERED,   //Request answered by remote peer.
        REQUEST_TIMED_OUT,  //Request timed out.
        REQUEST_CANCELED,  //Request was canceled.
        REQUEST_FAILED      //Request failed.
    };

    netResponse()
        : m_Data(0)
        , m_SizeofData(0)
        , m_Code(-1)
    {
    }

    bool Received() const{return REQUEST_RECEIVED == m_Code;}
    bool Answered() const{return REQUEST_ANSWERED == m_Code;}
    bool TimedOut() const{return REQUEST_TIMED_OUT == m_Code;}
    bool Canceled() const{return REQUEST_CANCELED == m_Code;}
    bool Failed() const{return REQUEST_FAILED == m_Code;}
    bool Complete() const
    {
        return this->Answered()
                || this->TimedOut()
                || this->Canceled()
                || this->Failed();
    }

    netTransactionInfo m_TxInfo;
    const void* m_Data;
    unsigned m_SizeofData;
    union
    {
        int m_Code;
        ResponseCode m_ResponseCode;
    };
};

//PURPOSE
//  Instances of netRequestHandler are registered with an instance of
//  netTransactor and are called back when a request is received.
class netRequestHandler : public atDelegate<void(netTransactor*,
                                                netRequestHandler*,
                                                const netRequest*)>
{
    friend class netTransactor;

public:
    netRequestHandler();

    template<typename X, typename Y>
    netRequestHandler(X* target, Y memberFunc)
        : atDelegate<void(netTransactor*, netRequestHandler*, const netRequest*)>(target, memberFunc)
    {
    }

    explicit netRequestHandler(void (*freeFunc)(netTransactor*,netRequestHandler*,const netRequest*))
        : atDelegate<void(netTransactor*, netRequestHandler*, const netRequest*)>(freeFunc)
    {
    }

    ~netRequestHandler();

private:
    void OnCxnEvent(netConnectionManager* cxnMgr,
                    const netEvent* nevt);

    netTransactor* m_Owner;
    netConnectionManager::Delegate m_CxnDlgt;
    inlist_node<netRequestHandler> m_ListLink;
};

//PURPOSE
//  Instances of netResponseHandler are passed to netTransactor::SendRequest()
//  and are called back with status updates about the request.
class netResponseHandler : public atDelegate<void(netTransactor*,
                                                netResponseHandler*,
                                                const netResponse*)>
{
    friend class netTransactor;

public:
    netResponseHandler();

    ~netResponseHandler();

    bool Answered() const;
    bool Failed() const;
    bool TimedOut() const;
    bool Canceled() const;
    bool Pending() const;

    void Cancel();

private:

    void Received();
    void Fail();
    void Timeout();
    void Abort();

    void OnCxnEvent(netConnectionManager* cxnMgr,
                    const netEvent* nevt);

    netTransactor* m_Owner;
    unsigned m_TxId;
	EndpointId m_EndpointId;
	ChannelId m_ChannelId;
	int m_CxnId;
    netAddress m_Addr;
    int m_Timeout;
    int m_Result;
    netConnectionManager::Delegate m_CxnDlgt;
    inlist_node<netResponseHandler> m_ListLink;
    netSequence m_Seq;
};

//PURPOSE
//  netTransactor is used to send and receive requests and responses.
class netTransactor
{
    friend class netRequestHandler;
    friend class netResponseHandler;
public:

    netTransactor();

    ~netTransactor();

    //PURPOSE
    //  Initialize the transactor.
    //PARAMS
    //  cxnMgr  - Connection manager used to send/receive.
    void Init(netConnectionManager* cxnMgr);

    //PURPOSE
    //  Shutdown the transactor.
    void Shutdown();

    //PURPOSE
    //  Returns true if initialized.
    bool IsInitialized() const;

    //PURPOSE
    //  Update the transactor.  Should be called at lease once per frame.
    //PARAMS
    //  timeStep    - Number of milliseconds since last call to Update().
    void Update(const unsigned timeStep);

    //PURPOSE
    //  Registers a request handler with the transactor.  Request handlers
    //  are called when requests are received.  A request handler should
    //  respond to the request.  At most one request handler should respond
    //  to any given request.
    //PARAMS
    //  handler     - The handler.
    //  channelId   - Channel on which to listen for requests.
    void AddRequestHandler(netRequestHandler* handler,
                            const unsigned channelId);

    //PURPOSE
    //  Removes a request handler.
    void RemoveRequestHandler(netRequestHandler* handler);

    //PURPOSE
    //  Sends a request reliably. A connection must already be established on the specified endpoint.
    //PARAMS
	//  cxnId       - Id of connection on which to send.
    //  endpointId  - Id of the existing endpoint to which to send the request.
	//  channelId	- The channel on which to send the request.
	//  txId        - Optional.  Returned with the id of the transaction.
    //  data        - Request data.
    //  sizeofData  - Number of bytes in the request.
    //  timeout     - Number of milliseconds to wait for a response.  Zero
    //                implies no timeout.
    //  handler     - Callback that is called with status updates of the request,
    //                e.g. if the request was received by the remote peer, when a
    //                response is received from the remote peer, if the request
    //                timed out, etc.
    //NOTES
    //  Requests sent using this function are sent reliably.  Requests
    //  sent using SendRequestOutOfBand() are sent unreliably.
    bool SendRequest(const int cxnId,
					unsigned* txId,
                    const void* data,
                    const unsigned sizeofData,
                    const int timeout,
                    netResponseHandler* handler);

    //PURPOSE
    //  Sends a request reliably. A connection must already be established on the specified endpoint.
	//  The msg parameter is a serializable message, i.e. it must implement an Export() function.
    template<typename T>
	bool SendRequest(const int cxnId,
					unsigned* txId,
                    const T& msg,
                    const unsigned timeout,
                    netResponseHandler* handler)
    {
        u8 buf[netTransaction::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
                && this->SendRequest(cxnId, txId, buf, size, timeout, handler);
    }

    //PURPOSE
    //  Sends a request unreliably. Use this version if an endpoint has already been created
	//  but a connection has not been established on that endpoint.
    //PARAMS
    //  endpointId  - EndpointId of recipient.
    //  channelId   - Id of channel on which request will be sent.
    //  txId        - Optional.  Returned with the id of the transaction,
    //                which can be used in ResendRequestOutOfBand().
    //  data        - Request data.
    //  sizeofData  - Size of request data in bytes.
    //  timeout     - Number of milliseconds to wait for a response.  Zero
    //                implies no timeout.
    //  handler     - Callback that is called with status updates of the request,
    //                e.g. if the request was received by the remote peer, when a
    //                response is received from the remote peer, if the request
    //                timed out, etc.
    //NOTES
    //  Requests sent using this function are sent unreliably.  Requests
    //  sent using SendRequest() are sent reliably.
    bool SendRequestOutOfBand(const EndpointId endpointId,
							const unsigned channelId,
							unsigned* txId,
							const void* data,
							const unsigned sizeofData,
							const unsigned timeout,
							netResponseHandler* handler);

    //PURPOSE
    //  Sends a request unreliably.  The msg parameter is a
    //  serializable message, i.e. it must implement an Export() function.
    template<typename T>
    bool SendRequestOutOfBand(const EndpointId endpointId,
							const unsigned channelId,
							unsigned* txId,
							const T& msg,
							const unsigned timeout,
							netResponseHandler* handler)
    {
        u8 buf[netTransaction::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
                && this->SendRequestOutOfBand(endpointId, channelId, txId, buf, size, timeout, handler);
    }
		
    //PURPOSE
    //  Sends a request unreliably. Use this version if no endpoint has been created
	//  and only an address is available. For example, when starting a transaction
	//  in response to an unsolicited message received from an unknown sender.
    //PARAMS
    //  addr        - Address of recipient.
    //  channelId   - Id of channel on which request will be sent.
    //  txId        - Optional.  Returned with the id of the transaction,
    //                which can be used in ResendRequest().
    //  data        - Request data.
    //  sizeofData  - Size of request data in bytes.
    //  timeout     - Number of milliseconds to wait for a response.  Zero
    //                implies no timeout.
    //  handler     - Callback that is called with status updates of the request,
    //                e.g. if the request was received by the remote peer, when a
    //                response is received from the remote peer, if the request
    //                timed out, etc.
    //NOTES
    //  Requests sent using this function are sent unreliably.  Requests
    //  sent using the variant of SendRequest() that takes an endpointId
    //  instead of a net address are sent reliably.
    bool SendRequestOutOfBand(const netAddress& addr,
							const unsigned channelId,
							unsigned* txId,
							const void* data,
							const unsigned sizeofData,
							const unsigned timeout,
							netResponseHandler* handler);

    //PURPOSE
    //  Sends a request unreliably.  The msg parameter is a
    //  serializable message, i.e. it must implement an Export() function.
    template<typename T>
    bool SendRequestOutOfBand(const netAddress& addr,
							const unsigned channelId,
							unsigned* txId,
							const T& msg,
							const unsigned timeout,
							netResponseHandler* handler)
    {
        u8 buf[netTransaction::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
                && this->SendRequestOutOfBand(addr, channelId, txId, buf, size, timeout, handler);
    }

	//PURPOSE
	//  Resends a request unreliably.
	//PARAMS
	//  endpointId  - EndpointId of recipient.
	//  channelId   - Id of channel on which request will be sent.
	//  txId        - Id of a transaction originally sent with SendRequestOutOfBand().
	//  data        - Request data.
	//  sizeofData  - Size of request data in bytes.
	//  timeout     - Number of milliseconds to wait for a response.  Zero
	//                implies no timeout.
	//  handler     - Callback that is called with status updates of the request,
	//                e.g. if the request was received by the remote peer, when a
	//                response is received from the remote peer, if the request
	//                timed out, etc.
	//NOTES
	//  Requests sent using this function are sent unreliably.
	bool ResendRequestOutOfBand(const EndpointId endpointId,
								const unsigned channelId,
								const unsigned txId,
								const void* data,
								const unsigned sizeofData,
								const unsigned timeout,
								netResponseHandler* handler);

	//PURPOSE
	//  Resends a request unreliably.  The original request must be sent
	//  unreliably as well.  The msg parameter is a serializable message,
	//  i.e. it must implement an Export() function.
	template<typename T>
	bool ResendRequestOutOfBand(const EndpointId endpointId,
								const unsigned channelId,
								const unsigned txId,
								const T& msg,
								const unsigned timeout,
								netResponseHandler* handler)
	{
		u8 buf[netTransaction::MAX_BYTE_SIZEOF_PAYLOAD];
		unsigned size;
		return msg.Export(buf, sizeof(buf), &size)
			&& this->ResendRequestOutOfBand(endpointId, channelId, txId, buf, size, timeout, handler);
	}
	
	//PURPOSE
	//  Resends a request unreliably.
	//PARAMS
	//  addr        - Address of recipient.
	//  channelId   - Id of channel on which request will be sent.
	//  txId        - Id of a transaction originally sent with SendRequest().
	//  data        - Request data.
	//  sizeofData  - Size of request data in bytes.
	//  timeout     - Number of milliseconds to wait for a response.  Zero
	//                implies no timeout.
	//  handler     - Callback that is called with status updates of the request,
	//                e.g. if the request was received by the remote peer, when a
	//                response is received from the remote peer, if the request
	//                timed out, etc.
	//NOTES
	//  Requests sent using this function are sent unreliably.  Requests
	//  sent using the variant of SendRequest() that takes a connection id
	//  instead of a net address are sent reliably.
	bool ResendRequestOutOfBand(const netAddress& addr,
								const unsigned channelId,
								const unsigned txId,
								const void* data,
								const unsigned sizeofData,
								const unsigned timeout,
								netResponseHandler* handler);

	//PURPOSE
	//  Resends a request unreliably.  The original request must be sent
	//  unreliably as well.  The msg parameter is a serializable message,
	//  i.e. it must implement an Export() function.
	template<typename T>
	bool ResendRequestOutOfBand(const netAddress& addr,
								const unsigned channelId,
								const unsigned txId,
								const T& msg,
								const unsigned timeout,
								netResponseHandler* handler)
	{
		u8 buf[netTransaction::MAX_BYTE_SIZEOF_PAYLOAD];
		unsigned size;
		return msg.Export(buf, sizeof(buf), &size)
			&& this->ResendRequestOutOfBand(addr, channelId, txId, buf, size, timeout, handler);
	}

    //PURPOSE
    //  Cancels the request associated with the response handler.
    void CancelRequest(netResponseHandler* handler);

    //PURPOSE
    //  Sends a response to a request.  If the request was sent reliably then
    //  the response will also be sent reliably, and vice versa.
    //PARAMS
    //  txInfo      - Transaction info passed to the request handler callback
    //                that was registered with AddRequestHandler().
    //  data        - Response data.
    //  sizeofData  - Size of response data in bytes.
    bool SendResponse(const netTransactionInfo& txInfo,
                        const void* data,
                        const unsigned sizeofData);

    //PURPOSE
    //  Sends a response to a request.  If the request was sent reliably then
    //  the response will also be sent reliably, and vice versa.  The msg
    //  parameter is a serializable message, i.e. it must implement an Export()
    //  function.
    template<typename T>
    bool SendResponse(const netTransactionInfo& txInfo,
                        const T& msg)
    {
        u8 buf[netTransaction::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
                && this->SendResponse(txInfo, buf, size);
    }

private:

    void AbortRequest(netResponseHandler* handler);

	bool SendRqstBuf(const int cxnId,
                    const void* buf,
                    const unsigned sizeofBuf,
                    const int timeout,
                    netResponseHandler* handler);

    bool SendRqstBufOutOfBand(const EndpointId endpointId,
                    const unsigned channelId,
                    const void* buf,
                    const unsigned sizeofBuf,
                    const int timeout,
                    netResponseHandler* handler);

    bool SendRqstBufOutOfBand(const netAddress& addr,
                    const unsigned channelId,
                    const void* buf,
                    const unsigned sizeofBuf,
                    const int timeout,
                    netResponseHandler* handler);

    bool SendRespBuf(const netTransactionInfo& txInfo,
                    const void* buf,
                    const unsigned sizeofBuf);

    typedef inlist<netRequestHandler, &netRequestHandler::m_ListLink> RqstHandlerList;
    typedef inlist<netResponseHandler, &netResponseHandler::m_ListLink> RespHandlerList;

    netConnectionManager* m_CxnMgr;
    RqstHandlerList m_RqstHandlers;
    RespHandlerList m_RespHandlers;
};

}   //namespace rage

#endif  //NET_TRANSACTION_H
