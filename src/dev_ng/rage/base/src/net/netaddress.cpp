// 
// net/netaddress.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "data/rson.h"
#include "diag/seh.h"
#include "file/file_config.h"
#include "netaddress.h"
#include "net/netdiag.h"
#include "nethardware.h"
#include "string/string.h"
#include "system/memops.h"

#include <stdio.h>
#include <tuple>

#if RSG_PC || RSG_DURANGO
#include "system/xtl.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#elif RSG_NP
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#elif RSG_LINUX || RSG_NX
#include <limits.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#elif RSG_ANDROID
#include <sys/socket.h>
#endif  //RSG_PC || RSG_DURANGO

#if RSG_PC || RSG_DURANGO
#define NET_ADDRESS_SSCANF sscanf_s
#else
#define NET_ADDRESS_SSCANF sscanf
#endif

namespace rage
{

///////////////////////////////////////////////////////////////////////////////
// netIpV4Address
///////////////////////////////////////////////////////////////////////////////
netIpV4Address::netIpV4Address()
{
	Clear();
}

netIpV4Address::netIpV4Address(u32 ip)
{
	this->Init(ip);
}

netIpV4Address::netIpV4Address(const sockaddr* saddr)
{
	this->Init(saddr);
}

netIpV4Address::netIpV4Address(const in_addr* addr4)
{
	this->Init(addr4);
}

netIpV4Address::netIpV4Address(const netIpV4Address& other)
{
	m_Ip = other.m_Ip;
	OUTPUT_ONLY(m_Str[0] = '\0');
}

netIpV4Address&
netIpV4Address::operator=(const netIpV4Address& other)
{
	m_Ip = other.m_Ip;
	OUTPUT_ONLY(m_Str[0] = '\0');
	return *this;
}

bool
netIpV4Address::Init(u32 ip)
{
	m_Ip = ip;
	OUTPUT_ONLY(m_Str[0] = '\0');
	return true;
}

bool 
netIpV4Address::Init(const sockaddr* saddr)
{
	rtry
	{
		rverifyall(saddr);

		OUTPUT_ONLY(m_Str[0] = '\0');

		rverify(saddr->sa_family == AF_INET, catchall, netError("netSocketAddress :: invalid arg - unsupported socket address family %u", saddr->sa_family))

		const struct sockaddr_in* addr4 = (const struct sockaddr_in*)saddr;
		return this->Init(&addr4->sin_addr);
	}
	rcatchall
	{
		Clear();
		return false;
	}
}

bool 
netIpV4Address::Init(const in_addr* addr4)
{
	m_Ip = net_ntohl(addr4->s_addr);
	OUTPUT_ONLY(m_Str[0] = '\0');
	return true;
}

netIpV4Address 
netIpV4Address::GetLoopbackAddress()
{
	return netIpV4Address(NET_INADDR_LOOPBACK);
}

netIpV4Address 
netIpV4Address::GetBroadcastAddress()
{
	return netIpV4Address(NET_INADDR_BROADCAST);
}

void
netIpV4Address::Clear()
{
	m_Ip = NET_INADDR_NONE;
	OUTPUT_ONLY(m_Str[0] = '\0');
}

bool
netIpV4Address::IsValid() const
{
	return (m_Ip != NET_INADDR_NONE) && (m_Ip != NET_INADDR_ANY);
}

u32
netIpV4Address::ToU32() const
{
	return m_Ip;
}

u32
netIpV4Address::ToU32Nbo() const
{
	return net_htonl(ToU32());
}

u32
netIpV4Address::ToU32Le() const
{
#if	__BE
	// swap to little-endian
	return sysEndian::Swap(ToU32());
#else
	return ToU32();
#endif
}

bool
netIpV4Address::IsRfc1918() const
{
	/*
		Private IPv4 address spaces:

		RFC1918 name	IP address range				number of addresses
		24-bit block	10.0.0.0 - 10.255.255.255		16,777,216
		20-bit block	172.16.0.0 - 172.31.255.255		1,048,576
		16-bit block	192.168.0.0 - 192.168.255.255	65,536
	*/

	unsigned a0 = GetOctet(0);
	unsigned a1 = GetOctet(1);
	
	return (a0 == 10) ||
		((a0 == 172) && ((a1 >= 16) && (a1 <= 31))) ||
		((a0 == 192) && (a1 == 168));
}

unsigned
netIpV4Address::GetOctet(const int i) const
{
	netAssert(i >= 0 && i < 4);
	return (m_Ip >> ((3 - i) << 3)) & 0xFF;
}

const char*
netIpV4Address::Format(char* dest,
					   const unsigned sizeofDest) const
{
	netAssert(sizeofDest >= MAX_STRING_BUF_SIZE);

	formatf_sized(dest,
			sizeofDest,
			"%u.%u.%u.%u",
			GetOctet(0),
			GetOctet(1),
			GetOctet(2),
			GetOctet(3));

	return dest;
}

bool
netIpV4Address::FromString(const char* addr, unsigned* size)
{
	bool success = false;
	unsigned a0, a1, a2, a3;
	
	Clear();

	if(size)
	{
		*size = 0;
	}

	if((addr == NULL) || (addr[0] == '\0'))
	{
		return false;
	}

	int numFields;
	int consumed = 0;
	if(size)
	{
		// %n tells us the number of characters consumed
		numFields = NET_ADDRESS_SSCANF(addr, "%u.%u.%u.%u%n", &a0, &a1, &a2, &a3, &consumed);
	}
	else
	{
		numFields = NET_ADDRESS_SSCANF(addr, "%u.%u.%u.%u", &a0, &a1, &a2, &a3);
	}

	if(numFields == 4)
	{
		m_Ip = unsigned((((a0) & 0xFF) << 24)
						| (((a1) & 0xFF) << 16)
						| (((a2) & 0xFF) << 8)
						| (((a3) & 0xFF) << 0));

		success = true;
	}

	if(size){*size = success ? (unsigned)consumed : 0;}

	return success;
}

#if !__NO_OUTPUT
const char*
netIpV4Address::ToString() const
{
    return m_Str[0] ? m_Str : Format(m_Str, sizeof(m_Str));
}
#endif

bool
netIpV4Address::Import(const void* buf,
					   const unsigned sizeofBuf,
					   unsigned* size)
{
	datBitBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf);

	Clear();

	u32 ip;
	const bool success = bb.ReadUns(ip, 32);

	if(success)
	{
		m_Ip = ip;
	}

	if(size){*size = success ? bb.GetNumBytesRead() : 0;}

	return success;
}

bool 
netIpV4Address::Export(void* buf,
					   const unsigned sizeofBuf,
					   unsigned* size) const
{
	datBitBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	sysMemSet(buf, 0, sizeofBuf);

	const bool success = bb.WriteUns(m_Ip, 32);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool
netIpV4Address::operator==(const netIpV4Address& rhs) const
{
	return m_Ip == rhs.m_Ip;
}

bool
netIpV4Address::operator!=(const netIpV4Address& rhs) const
{
	return m_Ip != rhs.m_Ip;
}

bool
netIpV4Address::operator<(const netIpV4Address& rhs) const
{
	return m_Ip < rhs.m_Ip;
}

bool
netIpV4Address::operator>(const netIpV4Address& rhs) const
{
	return m_Ip > rhs.m_Ip;
}

bool
netIpV4Address::operator<=(const netIpV4Address& rhs) const
{
	return m_Ip <= rhs.m_Ip;
}

bool
netIpV4Address::operator>=(const netIpV4Address& rhs) const
{
	return m_Ip >= rhs.m_Ip;
}

#if IPV6_ADDRESSES
///////////////////////////////////////////////////////////////////////////////
// netIpV6Address
///////////////////////////////////////////////////////////////////////////////

netIpV6Address::netIpV6Address()
{
	Clear();
}

netIpV6Address::netIpV6Address(const u8 (&ip)[SIZE_OF_IP_IN_BYTES])
{
	this->Init(ip);
}

netIpV6Address::netIpV6Address(const sockaddr* saddr)
{
	this->Init(saddr);
}

netIpV6Address::netIpV6Address(const in6_addr* addr6)
{
	this->Init(addr6);
}

netIpV6Address::netIpV6Address(const netIpV4Address& ipV4Address)
{
	this->Init(ipV4Address);
}

netIpV6Address::netIpV6Address(const netIpV6Address& other)
{
	memcpy(m_Ip, other.m_Ip, sizeof(m_Ip));
	OUTPUT_ONLY(m_Str[0] = '\0');
}

netIpV6Address&
netIpV6Address::operator=(const netIpV6Address& other)
{
	memcpy(m_Ip, other.m_Ip, sizeof(m_Ip));
	OUTPUT_ONLY(m_Str[0] = '\0');
	return *this;
}

netIpV6Address&
netIpV6Address::operator=(const netIpV4Address& ipV4Address)
{
	Init(ipV4Address);
	return *this;
}

bool netIpV6Address::Init(const u8(&ip)[SIZE_OF_IP_IN_BYTES])
{
	memcpy(m_Ip, ip, sizeof(m_Ip));
	OUTPUT_ONLY(m_Str[0] = '\0');
	return true;
}

bool netIpV6Address::Init(const netIpV4Address& ipV4Address)
{
	const unsigned ipV4 = ipV4Address.ToU32();
	const unsigned char ipV4Bytes[4] = {(unsigned char)((ipV4 >> 24) & 0xFF),
										(unsigned char)((ipV4 >> 16) & 0xFF),
										(unsigned char)((ipV4 >> 8) & 0xFF),
										(unsigned char)(ipV4 & 0xFF)};
			
	const unsigned char ip[netIpV6Address::SIZE_OF_IP_IN_BYTES] = {0x00, 0x00, 0x00, 0x00,
																	0x00, 0x00, 0x00, 0x00,
																	0x00, 0x00, 0xff, 0xff,
																	ipV4Bytes[0], ipV4Bytes[1],
																	ipV4Bytes[2], ipV4Bytes[3]};

	CompileTimeAssert(sizeof(ip) == sizeof(m_Ip));

	memcpy(m_Ip, ip, sizeof(m_Ip));

	OUTPUT_ONLY(m_Str[0] = '\0');
	return true;
}

bool netIpV6Address::Init(const sockaddr* saddr)
{
	rtry
	{
		rverifyall(saddr);

		OUTPUT_ONLY(m_Str[0] = '\0');

#if IPV6_ADDRESSES
		if(saddr->sa_family == AF_INET6)
		{
			const struct sockaddr_in6* addr6 = (const struct sockaddr_in6*)saddr;
			return this->Init(&addr6->sin6_addr);
		}
		else
#endif
		{
			rverify(saddr->sa_family == AF_INET, catchall, netError("netSocketAddress :: invalid arg - unsupported socket address family %u", saddr->sa_family))

			const netIpV4Address ipV4Address(saddr);
			return this->Init(ipV4Address);
		}
	}
	rcatchall
	{
		return false;
	}
}

bool netIpV6Address::Init(const in6_addr* addr6)
{
	memcpy(m_Ip, addr6->s6_addr, sizeof(m_Ip));
	OUTPUT_ONLY(m_Str[0] = '\0');
	return true;
}

void
netIpV6Address::Clear()
{
	memset(m_Ip, 0, sizeof(m_Ip));
	OUTPUT_ONLY(m_Str[0] = '\0');
}

bool
netIpV6Address::IsValid() const
{
	u8 ip[SIZE_OF_IP_IN_BYTES] = {0};
	return memcmp(m_Ip, ip, sizeof(m_Ip)) != 0;
}

bool 
netIpV6Address::IsIpV4Mapped() const
{
	if((m_Ip[10] != 0xFF) || (m_Ip[11] != 0xFF))
	{
		return false;
	}

	for(unsigned i = 0; i < 10; ++i)
	{
		if(m_Ip[i] != 0)
		{
			return false;
		}
	}

	return true;
}

bool 
netIpV6Address::IsTeredo() const
{
	if((m_Ip[0] == 0x20) && (m_Ip[1] == 0x01))
	{
		return true;
	}
	return false;
}

bool
netIpV6Address::IsV4() const
{
	return this->IsIpV4Mapped() || this->IsTeredo();
}

netIpV4Address
netIpV6Address::ToV4() const
{
	if(IsIpV4Mapped())
	{
		// convert this IPv4-mapped IPv6 address to an IPv4 address
		const u8(&ipV6)[netIpV6Address::SIZE_OF_IP_IN_BYTES] = this->ToBytes();
		const unsigned a0 = ipV6[12], a1 = ipV6[13], a2 = ipV6[14], a3 = ipV6[15];
		const u32 ipV4 = unsigned((((a0) & 0xFF) << 24)
									| (((a1) & 0xFF) << 16)
									| (((a2) & 0xFF) << 8)
									| (((a3) & 0xFF) << 0));

		return netIpV4Address(ipV4);
	}
	else if(IsTeredo())
	{
		// convert this Teredo address to an IPv4 address
		const u8(&ipV6)[netIpV6Address::SIZE_OF_IP_IN_BYTES] = this->ToBytes();
		u32 ipV4;
		memcpy(&ipV4, &ipV6[12], 4);
		ipV4 = ~ipV4;

		return netIpV4Address(ipV4);
	}

	netAssertf(0, "Cannot convert this IPv6 address to IPvv4");

	return netIpV4Address();
}

const u8 (&netIpV6Address::ToBytes() const)[SIZE_OF_IP_IN_BYTES]
{
	return m_Ip;
}

const char*
netIpV6Address::Format(char* dest,
					   const unsigned sizeofDest) const
{
	netAssert(dest && sizeofDest >= MAX_STRING_BUF_SIZE);

	if(netVerify(inet_ntop(AF_INET6, (void*)m_Ip, &dest[1], sizeofDest - 2) != NULL))
	{
		dest[0] = '[';
		unsigned len = (unsigned)strlen(dest);
		dest[len] = ']';
		dest[len + 1] = '\0';
	}

	return dest;
}

#if !__NO_OUTPUT
const char* 
netIpV6Address::FormatAttemptIpV4(char* dest, const unsigned sizeofDest) const
{
	if(IsV4())
	{
		return ToV4().Format(dest, sizeofDest);		 
	}

	return Format(dest, sizeofDest);
}
#endif

bool
netIpV6Address::FromString(const char* addr, unsigned* size)
{
	// Note: we use getaddrinfo instead of inet_pton since (at least on Windows 7)
	// it accepts strings with square brackets and ports, whereas inet_pton doesn't.

	// Note: this function must return false if the string is a pure IPv4 address, and must
	//		 return true if the string is an IPv6 addresses or an IPv4-mapped IPv6 addresses.

	/*
		Examples of addresses that are accepted:
		"::1"													// IPv6 loopback address
		"A1B2:C3d4:abcd:ABCD:DEAD:BEEF:0000:999A"				// IPv6 address with mixed case
		"e382:3231::1"											// shortened IPv6 address
		"[e382:3231::1]"										// with square brackets
		"[e382:3231::1]:0"										// square brackets with port
		"[e382:3231::1]:65535"									// max port
		"318F:3231::1123:ABCD"									// another shortened IPv6 address
		"::ffff:10.2.18.98"										// IPv4-mapped IPv6 address
		"[::ffff:10.2.18.98]"									// IPv4-mapped IPv6 address with square brackets
		"[::ffff:10.2.18.98]:11111"								// IPv4-mapped IPv6 address with square brackets and port
		"0000:0000:0000:0000:0000:ffff:123.231.255.100"			// longest IPv4-mapped IPv6 address
		"[0000:0000:0000:0000:0000:ffff:123.231.255.100]:65535"	// longest IPv4-mapped IPv6 address with square brackets and longest port

		Examples of strings that are rejected:
		"[e382:3231::1]:65536"	// port invalid
		"[e382:3231::1]:-7000"	// port invalid
		"10.2.18.98"			// IPv4 IP
		"[10.2.18.98]"
		"10.2.18.98:11111"
		"[10.2.18.98]:11111"
		"www.google.com"		// not an IP
	*/

	bool success = false;

	Clear();

	if(size)
	{
		*size = 0;
	}

	if((addr == NULL) || (addr[0] == '\0'))
	{
		return false;
	}

	ADDRINFO hints;
	memset(&hints, 0, sizeof (hints));
	hints.ai_family = AF_INET6;			// IPv6 address family only
	hints.ai_flags = AI_NUMERICHOST;	// don't do a DNS lookup

	ADDRINFO* addrInfo = NULL;
	if((getaddrinfo(addr, NULL, &hints, &addrInfo) == 0) && (addrInfo != NULL))
	{
		if(netVerify(addrInfo->ai_family == AF_INET6))
		{
			sockaddr_in6 sin = *(sockaddr_in6*)addrInfo->ai_addr;
			memcpy(m_Ip, sin.sin6_addr.s6_addr, sizeof(m_Ip));
			success = true;
		}
		freeaddrinfo(addrInfo);
		addrInfo = NULL;
	}

	// assume we consumed the entire string if there are no square brackets,
	// otherwise, consider the closing square bracket to be the last character consumed
	if(success && size)
	{
		if(addr[0] == '[')
		{
			const char* closingSquareBracket = strchr(addr,']');
			if(closingSquareBracket != NULL)
			{
				ptrdiff_t len = closingSquareBracket - addr + 1;
				if(len <= ((MAX_STRING_BUF_SIZE - 1) + 2)) // + 2 for square brackets
				{
					*size = (unsigned)len;
				}
			}
		}
		
		if(*size == 0)
		{
			*size = (unsigned)strlen(addr);
		}
	}

	return success;
}

#if !__NO_OUTPUT
const char*
netIpV6Address::ToString() const
{
    return m_Str[0] ? m_Str : FormatAttemptIpV4(m_Str, sizeof(m_Str));
}
#endif

bool
netIpV6Address::Import(const void* buf,
					   const unsigned sizeofBuf,
					   unsigned* size)
{
	datBitBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf);

	Clear();

	u8 ip[SIZE_OF_IP_IN_BYTES] = {0};
	const bool success = bb.ReadBytes(ip, sizeof(ip));

	if(success)
	{
		memcpy(m_Ip, ip, sizeof(m_Ip));
	}

	if(size){*size = success ? bb.GetNumBytesRead() : 0;}

	return success;
}

bool 
netIpV6Address::Export(void* buf,
					   const unsigned sizeofBuf,
					   unsigned* size) const
{
	datBitBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	sysMemSet(buf, 0, sizeofBuf);

	const bool success = bb.WriteBytes(m_Ip, sizeof(m_Ip));

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool
netIpV6Address::operator==(const netIpV6Address& rhs) const
{
	//return IN6_ADDR_EQUAL((IN6_ADDR*)m_Ip, (IN6_ADDR*)rhs.m_Ip) != 0;
	return memcmp(m_Ip, rhs.m_Ip, sizeof(m_Ip)) == 0;
}

bool
netIpV6Address::operator!=(const netIpV6Address& rhs) const
{
	//return IN6_ADDR_EQUAL((IN6_ADDR*)m_Ip, (IN6_ADDR*)rhs.m_Ip) == 0;;
	return memcmp(m_Ip, rhs.m_Ip, sizeof(m_Ip)) != 0;
}

bool
netIpV6Address::operator<(const netIpV6Address& rhs) const
{
	return memcmp(m_Ip, rhs.m_Ip, sizeof(m_Ip)) < 0;
}

bool
netIpV6Address::operator>(const netIpV6Address& rhs) const
{
	return memcmp(m_Ip, rhs.m_Ip, sizeof(m_Ip)) > 0;
}

bool
netIpV6Address::operator<=(const netIpV6Address& rhs) const
{
	return memcmp(m_Ip, rhs.m_Ip, sizeof(m_Ip)) <= 0;
}

bool
netIpV6Address::operator>=(const netIpV6Address& rhs) const
{
	return memcmp(m_Ip, rhs.m_Ip, sizeof(m_Ip)) >= 0;
}
#endif

///////////////////////////////////////////////////////////////////////////////
// netIpAddress
///////////////////////////////////////////////////////////////////////////////
netIpAddress::netIpAddress()
: m_IpAddress()
{
}

netIpAddress::netIpAddress(const netIpV4Address& ipv4_address)
: m_IpAddress(ipv4_address)
{
}

#if IPV6_ADDRESSES
netIpAddress::netIpAddress(const netIpV6Address& ipv6_address)
: m_IpAddress(ipv6_address)
{
}
#endif

netIpAddress::netIpAddress(const netIpAddress& other)
: m_IpAddress(other.m_IpAddress)
{
}

netIpAddress::netIpAddress(const sockaddr* saddr)
: m_IpAddress(saddr)
{
}

netIpAddress&
netIpAddress::operator=(const netIpAddress& other)
{
	m_IpAddress = other.m_IpAddress;
	return *this;
}

netIpAddress&
netIpAddress::operator=(const netIpV4Address& ipv4_address)
{
	m_IpAddress = ipv4_address;
	return *this;
}

#if IPV6_ADDRESSES
netIpAddress&
netIpAddress::operator=(const netIpV6Address& ipv6_address)
{
	m_IpAddress = ipv6_address;
	return *this;
}
#endif

bool
netIpAddress::operator==(const netIpAddress& rhs) const
{
	return m_IpAddress == rhs.m_IpAddress;
}

bool
netIpAddress::operator!=(const netIpAddress& rhs) const
{
	return m_IpAddress != rhs.m_IpAddress;
}

bool
netIpAddress::operator<(const netIpAddress& rhs) const
{
	return m_IpAddress < rhs.m_IpAddress;
}

bool 
netIpAddress::Init(const sockaddr* saddr)
{
	return m_IpAddress.Init(saddr);
}

void
netIpAddress::Clear()
{
	m_IpAddress.Clear();
}

bool
netIpAddress::IsValid() const
{
	return m_IpAddress.IsValid();
}

bool
netIpAddress::IsV4() const
{
#if IPV6_ADDRESSES
	return m_IpAddress.IsV4();
#else
	return true;
#endif
}

netIpV4Address
netIpAddress::ToV4() const
{
#if IPV6_ADDRESSES
	return m_IpAddress.ToV4();
#else
	return m_IpAddress;
#endif
}

#if IPV6_ADDRESSES
netIpV6Address
netIpAddress::ToV6() const
{
	return m_IpAddress;
}
#endif

const char*
netIpAddress::Format(char* dest,
					 const unsigned sizeofDest) const
{
	netAssert(dest && sizeofDest >= MAX_STRING_BUF_SIZE);
	return m_IpAddress.Format(dest, sizeofDest);
}

const char* 
netIpAddress::FormatAttemptIpV4(char* dest, const unsigned sizeofDest) const
{
	netAssert(dest && sizeofDest >= MAX_STRING_BUF_SIZE);

	if(IsV4())
	{
		ToV4().Format(dest, sizeofDest);
	}
	else
	{
		m_IpAddress.Format(dest, sizeofDest);
	}

	return dest;
}

bool
netIpAddress::FromString(const char* addr, unsigned* size)
{
#if IPV6_ADDRESSES
	{
		netIpV6Address ip6Address;	
		if(ip6Address.FromString(addr, size))
		{
			m_IpAddress = ip6Address;
			return true;
		}
	}
#endif

	{
		netIpV4Address ip4Address;
		if(ip4Address.FromString(addr, size))
		{
			m_IpAddress = ip4Address;
			return true;
		}
	}

	return false;
}

bool
netIpAddress::WriteRson(const char* name, RsonWriter* rw) const
{
	// [IPv4 specific]
	// Telemetry data engs want IPs as ints for efficiency (32-bit, little-endian). However, that won't work for
	// IPv6 addresses. For now, IPv6 will be sent as 0 since we don't currently support IPv6. In the future we'll 
	// probably need to go back to sending IPs as strings or byte arrays.
	if(IsV4())
	{
		return rw->WriteInt(name, ToV4().ToU32Le());
	}
	else
	{
		return rw->WriteInt(name, 0);
	}
}

#if !__NO_OUTPUT
const char*
netIpAddress::ToString() const
{
	return m_IpAddress.ToString();
}
#endif

bool
netIpAddress::Import(const void* buf,
					 const unsigned sizeofBuf,
					 unsigned* size)
{
	datBitBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf);

	Clear();

	bool success = true;

#if NET_CROSS_PLATFORM_MP
	bool fromIpV6Platform;
	success = bb.ReadBool(fromIpV6Platform);

	if(fromIpV6Platform)
	{
#if !IPV6_ADDRESSES
		// Cross-platform multiplayer:
		// on IPv4 platforms, we only support IPs in IPv4-mapped IPv6 format
		u8 ignore[12] = {0}; // first 10 bytes are zeroes, then 2 bytes of 0xFF, then the last 4 bytes contain the IPv4 IP.
		success = success && bb.ReadBytes(ignore, sizeof(ignore));
#endif

		success = success && datImport(bb, m_IpAddress);
	}
	else
#endif
	{
#if IPV6_ADDRESSES
		// IPv4 address
		netIpV4Address ipV4;
		success = success && datImport(bb, ipV4);
		m_IpAddress = ipV4;
#else
		success = success && datImport(bb, m_IpAddress);
#endif
	}

	if(success == false)
	{
		Clear();
	}

	if(size){*size = success ? bb.GetNumBytesRead() : 0;}

	return success;
}

bool 
netIpAddress::Export(void* buf,
					 const unsigned sizeofBuf,
					 unsigned* size) const
{
	datBitBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	sysMemSet(buf, 0, sizeofBuf);

	bool success = true;

#if	NET_CROSS_PLATFORM_MP
	// Cross-platform multiplayer: we export a bool that tells us whether
	// or not the netIpAddress was serialized from an IPv6 platform.
	bool ipV6Platform = IPV6_ADDRESSES != 0;
	success = success && bb.WriteBool(ipV6Platform);
#endif

	success = success && datExport(bb, m_IpAddress);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
// netSocketAddress
///////////////////////////////////////////////////////////////////////////////

const netSocketAddress netSocketAddress::INVALID_ADDRESS;

netSocketAddress::netSocketAddress()
{
    this->Clear();
}

netSocketAddress::netSocketAddress(const netIpAddress& ip,
								   const unsigned short port)
{
    this->Init(ip, port);
}

netSocketAddress::netSocketAddress(const sockaddr* saddr)
{
	this->Init(saddr);
}

netSocketAddress::netSocketAddress(const char* addr)
{
    this->Init(addr);
}

bool
netSocketAddress::Init(const netIpAddress& ip,
					   const unsigned short port)
{
    this->Clear();

    bool success = true;

	m_Ip = ip;
    m_Port = port;

    return success;
}

bool
netSocketAddress::Init(const char* addr)
{
    this->Clear();

	unsigned consumed = 0;
	bool success = m_Ip.FromString(addr, &consumed);

	if(success)
	{
		// look for the port immediately after the consumed portion of the string
		unsigned port = 0;
		int numFields = NET_ADDRESS_SSCANF(addr + consumed, ":%u", &port);
		
		if(numFields == 1)
		{
			netAssert(port <= USHRT_MAX);
			m_Port = (unsigned short)port;
		}
	}

	return success;
}

bool
netSocketAddress::Init(const sockaddr* saddr)
{
	rtry
	{
		rverifyall(saddr);

		OUTPUT_ONLY(m_Str[0] = '\0');

		rverifyall(m_Ip.Init(saddr));

#if IPV6_ADDRESSES
		if(saddr->sa_family == AF_INET6)
		{
			sockaddr_in6* addr6 = (sockaddr_in6*)saddr;
			m_Port = net_ntohs(addr6->sin6_port);
		}
		else
#endif
		{
			rverify(saddr->sa_family == AF_INET, catchall, netError("netSocketAddress :: invalid arg - unsupported socket address family %u", saddr->sa_family))

			sockaddr_in* addr4 = (sockaddr_in*)saddr;
			m_Port = net_ntohs(addr4->sin_port);
		}
		
		return true;
	}
	rcatchall
	{
		Clear();
		return false;
	}
}

void netSocketAddress::Clear()
{
    m_Ip.Clear();
    m_Port = 0;
    OUTPUT_ONLY(m_Str[0] = '\0');
}

netIpAddress
netSocketAddress::GetIp() const
{
	return m_Ip;
}

unsigned short
netSocketAddress::GetPort() const
{
    return m_Port;
}

bool
netSocketAddress::IsValid() const
{
    return (m_Port != 0) && m_Ip.IsValid();
}

bool netSocketAddress::ToSockAddr(sockaddr* dest, const unsigned sizeofDest) const
{
	if(!netVerify(dest))
	{
		return false;
	}

	memset(dest, 0, sizeofDest);

#if IPV6_ADDRESSES
	if(!netVerify(sizeofDest >= sizeof(sockaddr_in6)))
	{
		return false;
	}

	sockaddr_in6* addr = (sockaddr_in6*)dest;
	addr->sin6_family = AF_INET6;
	memcpy(addr->sin6_addr.s6_addr, m_Ip.ToV6().ToBytes(), netIpV6Address::SIZE_OF_IP_IN_BYTES);
	addr->sin6_port = net_htons(m_Port);
#else
	if(!netVerify(sizeofDest >= sizeof(sockaddr_in)))
	{
		return false;
	}

	sockaddr_in* addr = (sockaddr_in*)dest;
	addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = m_Ip.ToV4().ToU32Nbo();
	addr->sin_port = net_htons(m_Port);
#endif

	return true;
}

const char*
netSocketAddress::Format(char*dest,
						 const unsigned sizeofDest,
						 const bool includePort) const
{
	netAssert(sizeofDest >= (includePort ?
						  MAX_STRING_BUF_SIZE :
						  MAX_STRING_BUF_SIZE_WITHOUT_PORT));

	m_Ip.Format(dest, sizeofDest);

	if(includePort)
	{
		safecatf_sized(dest, sizeofDest, ":%u", GetPort());
	}

	return dest;
}

const char* 
netSocketAddress::FormatAttemptIpV4(char* dest, const unsigned sizeofDest, const bool includePort) const
{
	netAssert(sizeofDest >= (includePort ?
							MAX_STRING_BUF_SIZE :
							MAX_STRING_BUF_SIZE_WITHOUT_PORT));

	m_Ip.FormatAttemptIpV4(dest, sizeofDest);

	if(includePort)
	{
		safecatf_sized(dest, sizeofDest, ":%u", GetPort());
	}

	return dest;
}

bool
netSocketAddress::Export(void* buf,
                            const unsigned sizeofBuf,
                            unsigned* size) const
{
    datBitBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

	sysMemSet(buf, 0, sizeofBuf);

	netIpAddress ip = this->GetIp();
    const bool success = datExport(bb, ip)
                        && bb.WriteUns(this->GetPort(), 16);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool
netSocketAddress::Import(const void* buf,
                        const unsigned sizeofBuf,
                        unsigned* size)
{
    datBitBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

	Clear();

	netIpAddress ip;
	unsigned short port;

    const bool success = datImport(bb, ip)
                        && bb.ReadUns(port, 16)
                        && this->Init(ip, port);

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

bool
netSocketAddress::operator<(const netSocketAddress& rhs) const
{   return m_Ip < rhs.m_Ip || (m_Ip == rhs.m_Ip && m_Port < rhs.m_Port);
}

bool
netSocketAddress::operator==(const netSocketAddress& rhs) const
{
    return (m_Port == rhs.m_Port) && (m_Ip == rhs.m_Ip);
}

bool
netSocketAddress::operator!=(const netSocketAddress& rhs) const
{
    return !this->operator==(rhs);
}

#if !__NO_OUTPUT
const char*
netSocketAddress::ToString() const
{
	return m_Str[0] ? m_Str : FormatAttemptIpV4(m_Str, true);
}
#endif

//////////////////////////////////////////////////////////////////////////
//  netAddress
//////////////////////////////////////////////////////////////////////////
const netAddress netAddress::INVALID_ADDRESS;

netAddress::netAddress()
{
    this->Clear();
}

netAddress::netAddress(const netAddress& rhs)
{
	this->m_ProxyAddr = rhs.m_ProxyAddr;
	this->m_Type = rhs.m_Type;
	if(rhs.m_Type == netAddress::TYPE_PEER_RELAY)
	{
		this->m_TargetPeerId = rhs.m_TargetPeerId;
	}
	else
	{
		this->m_RelayToken = rhs.m_RelayToken;
		this->m_TargetAddr = rhs.m_TargetAddr;
	}

	OUTPUT_ONLY(m_Str[0] = '\0');

	netAssert(SanityCheck());
}

netAddress::netAddress(const netIpAddress& targetIp,
					   const unsigned short targetPort)
{
    this->Init(targetIp, targetPort);
}

netAddress::netAddress(const netSocketAddress& targetAddr)
{
    this->Init(targetAddr);
}

netAddress::netAddress(const netSocketAddress& proxyAddr,
					   const netRelayToken& relayToken,
					   const netSocketAddress& targetAddr)
{
    this->Init(proxyAddr, relayToken, targetAddr);
}

netAddress::netAddress(const netSocketAddress& proxyAddr,
					   const netPeerId& targetPeerId)
{
	this->Init(proxyAddr, targetPeerId);
}

bool
netAddress::Init(const netIpAddress& targetIp,
				 const unsigned short targetPort)
{
    this->Clear();
    m_TargetAddr.Init(targetIp, targetPort);

	m_Type = m_TargetAddr.IsValid() ? netAddress::TYPE_DIRECT : netAddress::TYPE_INVALID;

	netAssert(SanityCheck());

    return true;
}

bool
netAddress::Init(const netSocketAddress& targetAddr)
{
    this->Clear();
    m_TargetAddr = targetAddr;
	m_Type = m_TargetAddr.IsValid() ? netAddress::TYPE_DIRECT : netAddress::TYPE_INVALID;

	netAssert(SanityCheck());

	return true;
}

bool
netAddress::Init(const netSocketAddress& proxyAddr,
				 const netRelayToken& relayToken,
				 const netSocketAddress& targetAddr)
{
    this->Clear();
    m_ProxyAddr = proxyAddr;
	m_RelayToken = relayToken;
    m_TargetAddr = targetAddr;

	// TODO: NS - are there any cases where this function is used to init a direct address? - if not, it's always TYPE_RELAY_SERVER or TYPE_INVALID.
	m_Type = (m_ProxyAddr.IsValid() && (m_TargetAddr.IsValid() || m_RelayToken.IsValid())) ? netAddress::TYPE_RELAY_SERVER :
				m_TargetAddr.IsValid() ? netAddress::TYPE_DIRECT : netAddress::TYPE_INVALID;

	netAssert(SanityCheck());

	return true;
}

bool 
netAddress::Init(const netSocketAddress& proxyAddr,
				 const netPeerId& targetPeerId)
{
	this->Clear();
	m_ProxyAddr = proxyAddr;
	m_TargetPeerId = targetPeerId;
	m_Type = (netVerify(m_ProxyAddr.IsValid()) && netVerify(m_TargetPeerId.IsValid())) ?
			netAddress::TYPE_PEER_RELAY :
			netAddress::TYPE_INVALID;

	netAssert(SanityCheck());

	return true;
}

void
netAddress::Clear()
{
    m_ProxyAddr.Clear();
    m_TargetAddr.Clear();
	m_TargetPeerId.Clear();
	m_RelayToken.Clear();
	m_Type = netAddress::TYPE_INVALID;

	netAssert(!IsValid());

    OUTPUT_ONLY(m_Str[0] = '\0');
}

const netSocketAddress&
netAddress::GetProxyAddress() const
{
    return m_ProxyAddr;
}

const netSocketAddress&
netAddress::GetTargetAddress() const
{
	netAssert(m_Type != netAddress::TYPE_PEER_RELAY);
    return m_TargetAddr;
}

const netRelayToken&
netAddress::GetRelayToken() const
{
	netAssert(m_Type == netAddress::TYPE_RELAY_SERVER);
	return m_RelayToken;
}

const netPeerId&
netAddress::GetTargetPeerId() const
{
	netAssert(m_Type == netAddress::TYPE_PEER_RELAY);
	return m_TargetPeerId;
}

bool
netAddress::IsValid() const
{
	netAssert(SanityCheck());
	return m_Type != netAddress::TYPE_INVALID;
}

bool
netAddress::IsDirectAddr() const
{
	netAssert(SanityCheck());
	return (m_Type == netAddress::TYPE_DIRECT);
}

bool
netAddress::IsPeerRelayAddr() const
{
	netAssert(SanityCheck());
	return (m_Type == netAddress::TYPE_PEER_RELAY);
}

bool
netAddress::IsRelayServerAddr() const
{ 
	netAssert(SanityCheck());
	return (m_Type == netAddress::TYPE_RELAY_SERVER);
}

bool 
netAddress::IsEquivalent(const netAddress& addr) const
{
	if(this->operator==(addr))
	{
		return true;
	}

	if(this->IsRelayServerAddr() && addr.IsRelayServerAddr())
	{
		return ((this->GetProxyAddress().IsValid() && (this->GetProxyAddress() == addr.GetProxyAddress())) &&
				((this->GetTargetAddress().IsValid() && (this->GetTargetAddress() == addr.GetTargetAddress())) ||
				(this->GetRelayToken().IsValid() && (this->GetRelayToken() == addr.GetRelayToken()))));
	}

	return false;
}

const char*
netAddress::Format(char* dest, const unsigned sizeofDest) const
{
	netAssert(sizeofDest >= MAX_STRING_BUF_SIZE);

	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	char proxyAddrString[netSocketAddress::MAX_STRING_BUF_SIZE];
	char targetAddrString[netSocketAddress::MAX_STRING_BUF_SIZE];

	if(m_Type == TYPE_DIRECT)
	{
		return formatf_sized(dest, sizeofDest, "%s",
						m_TargetAddr.Format(targetAddrString, true));

	}

    return formatf_sized(dest, sizeofDest, "%s/%s",
					m_ProxyAddr.Format(proxyAddrString, true),
					(m_Type == TYPE_PEER_RELAY) ?
					m_TargetPeerId.ToHexString(peerIdHexString) :
					m_TargetAddr.Format(targetAddrString, true));
}

#if !__NO_OUTPUT
const char*
netAddress::FormatAttemptIpV4(char* dest, const unsigned sizeofDest) const
{
	netAssert(sizeofDest >= MAX_STRING_BUF_SIZE);

	char proxyAddrString[netSocketAddress::MAX_STRING_BUF_SIZE];
	char targetAddrString[netSocketAddress::MAX_STRING_BUF_SIZE];

	if(m_Type == TYPE_DIRECT)
	{
		return formatf_sized(dest, sizeofDest, "%s",
			m_TargetAddr.FormatAttemptIpV4(targetAddrString, true));
	}
	else if(m_Type == TYPE_RELAY_SERVER)
	{
		char relayTokenHexString[netRelayToken::TO_HEX_STRING_BUFFER_SIZE];

		if(m_RelayToken.IsValid())
		{
			return formatf_sized(dest, sizeofDest, "%s/%s/%s",
				m_ProxyAddr.FormatAttemptIpV4(proxyAddrString, true),
				m_RelayToken.ToHexString(relayTokenHexString),
				m_TargetAddr.FormatAttemptIpV4(targetAddrString, true));
		}
		else
		{
			return formatf_sized(dest, sizeofDest, "%s/%s",
				m_ProxyAddr.FormatAttemptIpV4(proxyAddrString, true),
				m_TargetAddr.FormatAttemptIpV4(targetAddrString, true));
		}
	}

	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
    return formatf_sized(dest, sizeofDest, "%s/%s",
					m_ProxyAddr.FormatAttemptIpV4(proxyAddrString, true),
					(m_Type == TYPE_PEER_RELAY) ?
					m_TargetPeerId.ToHexString(peerIdHexString) :
					m_TargetAddr.FormatAttemptIpV4(targetAddrString, true));
}
#endif

bool
netAddress::Export(void* buf,
                    const unsigned sizeofBuf,
                    unsigned* size) const
{
	netAssert(SanityCheck());

	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	sysMemSet(buf, 0, sizeofBuf);

	// serialize the type as a full byte to improve netAddress compressibility
	// TODO: NS - we don't need the proxy addr when exporting direct addresses
	// but it's needed on RDR to preserve compatibility with the companion app.
	CompileTimeAssert(datBitsNeeded<NUM_TYPES>::COUNT < (sizeof(u8) << 3));
	const bool success = bb.SerUns(m_Type, sizeof(u8) << 3)
						 && bb.SerUser(m_ProxyAddr)
						 // relay server
						 && (m_Type == netAddress::TYPE_RELAY_SERVER) ?
						 bb.SerUser(m_RelayToken) && bb.SerUser(m_TargetAddr) :
						 // peer relay
						 (m_Type == netAddress::TYPE_PEER_RELAY) ?
						 bb.SerUser(m_TargetPeerId)
						 // direct
						 : bb.SerUser(m_TargetAddr);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool
netAddress::Import(const void* buf,
                    const unsigned sizeofBuf,
                    unsigned* size)
{
    netSocketAddress proxyAddr, targetAddr;
	netPeerId targetPeerId;
	netRelayToken relayToken;

	datImportBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf);

	const bool success = bb.SerUns(m_Type, sizeof(u8) << 3)
						 && bb.SerUser(proxyAddr)
						 // relay server
						 && (m_Type == netAddress::TYPE_RELAY_SERVER) ?
						 bb.SerUser(relayToken) && bb.SerUser(targetAddr)
						 && this->Init(proxyAddr, relayToken, targetAddr) :
						 // peer relay
						 (m_Type == netAddress::TYPE_PEER_RELAY) ?
						 bb.SerUser(targetPeerId)
						 && this->Init(proxyAddr, targetPeerId) :
						 // direct
						 bb.SerUser(targetAddr)
						 && this->Init(targetAddr);

	netAssert(SanityCheck());

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

bool
netAddress::operator<(const netAddress& rhs) const
{
	netAssert(SanityCheck());

	bool typeLt = m_Type < rhs.m_Type;
	bool typeEq = m_Type == rhs.m_Type;
	bool peerId = m_Type == TYPE_PEER_RELAY;

	return typeLt ||
		(typeEq && peerId && std::tie(m_Type, m_ProxyAddr, m_TargetPeerId) < std::tie(rhs.m_Type, rhs.m_ProxyAddr, rhs.m_TargetPeerId)) ||
		(typeEq && !peerId && std::tie(m_Type, m_ProxyAddr, m_RelayToken, m_TargetAddr) < std::tie(rhs.m_Type, rhs.m_ProxyAddr, rhs.m_RelayToken, rhs.m_TargetAddr));
}

bool
netAddress::operator==(const netAddress& rhs) const
{
	netAssert(SanityCheck());
	return (m_Type == rhs.m_Type) &&
			(m_ProxyAddr == rhs.m_ProxyAddr) &&
			// relay server
			((m_Type == TYPE_RELAY_SERVER) ?
			((m_TargetAddr == rhs.m_TargetAddr) && (m_RelayToken == rhs.m_RelayToken)) :
			// peer relay
			((m_Type == TYPE_PEER_RELAY) ?
			(m_TargetPeerId == rhs.m_TargetPeerId) :
			// direct
			(m_TargetAddr == rhs.m_TargetAddr)));
}

bool
netAddress::operator!=(const netAddress& rhs) const
{
    return !this->operator==(rhs);
}

netAddress&
netAddress::operator=(const netAddress& rhs)
{
	// do not call this->Clear() or this->Init() here. The rhs arg might be
	// a reference to itself. Clearing it first would result in an empty
	// address after the assignment.
	this->m_ProxyAddr = rhs.m_ProxyAddr;
	this->m_Type = rhs.m_Type;
	if(rhs.m_Type == netAddress::TYPE_PEER_RELAY)
	{
		this->m_TargetPeerId = rhs.m_TargetPeerId;
	}
	else if(rhs.m_Type == netAddress::TYPE_RELAY_SERVER)
	{
		this->m_RelayToken = rhs.m_RelayToken;
		this->m_TargetAddr = rhs.m_TargetAddr;
	}
	else
	{
		this->m_TargetAddr = rhs.m_TargetAddr;
	}

	OUTPUT_ONLY(m_Str[0] = '\0');

	netAssert(SanityCheck());

	return *this;
}

#if USE_NET_ASSERTS
bool
netAddress::SanityCheck() const
{
	return ((m_Type == netAddress::TYPE_INVALID) ||
			((m_Type == netAddress::TYPE_DIRECT) && !m_ProxyAddr.IsValid() && m_TargetAddr.IsValid()) ||
			((m_Type == netAddress::TYPE_RELAY_SERVER) && m_ProxyAddr.IsValid() && (m_TargetAddr.IsValid() || m_RelayToken.IsValid())) ||
			((m_Type == netAddress::TYPE_PEER_RELAY) && m_ProxyAddr.IsValid() && m_TargetPeerId.IsValid()));
}
#endif

#if !__NO_OUTPUT
const char*
netAddress::ToString() const
{
	return m_Str[0] ? m_Str : FormatAttemptIpV4(m_Str);
}

const char*
netAddress::GetTypeString() const
{
	switch(m_Type)
	{
	case TYPE_INVALID: return "invalid"; break;
	case TYPE_DIRECT: return "direct"; break;
	case TYPE_RELAY_SERVER: return "relay server"; break;
	case TYPE_PEER_RELAY: return "peer relay"; break;
	case NUM_TYPES: return "**unhandled type**"; break;
	}

	return "**unhandled type**";
}

#endif

}   //namespace rage
