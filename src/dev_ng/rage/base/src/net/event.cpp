// 
// net/event.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "event.h"
#include "net.h"
#include "system/memory.h"

namespace rage
{

AUTOID_IMPL(netEvent);
AUTOID_IMPL(netEventConnectionRequested);
AUTOID_IMPL(netEventConnectionEstablished);
AUTOID_IMPL(netEventConnectionError);
AUTOID_IMPL(netEventConnectionClosed);
AUTOID_IMPL(netEventFrameReceived);
AUTOID_IMPL(netEventAckReceived);
AUTOID_IMPL(netEventBandwidthExceeded);
AUTOID_IMPL(netEventOutOfMemory);

///////////////////////////////////////////////////////////////////////////////
//  netEventBase
///////////////////////////////////////////////////////////////////////////////
netEventBase::netEventBase(sysMemAllocator* allocator)
    : m_TimeStamp(0)
    , m_Allocator(allocator)
    , m_Queue(NULL)
    , m_Next(NULL)
    , m_Prev(NULL)
    , m_RefCount(0)
    , m_Disposing(0)
	, m_Filtered(0)
{
}

netEventBase::~netEventBase()
{
    netAssert(!m_RefCount);
    netAssert(!m_Queue);
    netAssert(!m_Next);
    netAssert(!m_Prev);
}

void
netEventBase::Dispose()
{
    if(0 == sysInterlockedCompareExchange(&m_Disposing, 1, 0)
        && m_Allocator)
    {
        sysMemAllocator* allocator = m_Allocator;
        this->~netEventBase();
        allocator->Free(this);
    }
}

unsigned
netEventBase::GetTimeStamp() const
{
    return m_TimeStamp;
}

bool
netEventBase::IsQueued() const
{
    return (NULL != m_Queue);
}

bool netEventBase::IsFiltered() const
{
	return m_Filtered != 0;
}

///////////////////////////////////////////////////////////////////////////////
//  netEvent
///////////////////////////////////////////////////////////////////////////////
netEvent::netEvent(const int cxnId,
				   const EndpointId epId,
				   const unsigned channelId,
                   sysMemAllocator* allocator)
    : netEventBase(allocator)
	, m_CxnId(cxnId)
	, m_EndpointId(epId)
	, m_ChannelId(channelId)
{
    m_Event = this;
}

netEvent::~netEvent()
{
}

}   //namespace rage

