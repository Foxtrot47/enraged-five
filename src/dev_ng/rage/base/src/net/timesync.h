// 
// net/timesync.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_TIMESYNC_H
#define NET_TIMESYNC_H

#include "connectionmanager.h"
#include "message.h"

/*
    netTimeSync can be used to synchronize time among peers by designating
    a "server" to which all "clients" will synchronize their time.

    Servers must call netTimeSync::Start() with the FLAG_SERVER bit set
    in the flags parameter.

    Clients must call netTimeSync::Start(), passing the network address of
    the server, and with the FLAG_CLIENT bit set in the flags parameter.

    netTimeSync instances can be both servers and clients.  A client-server
    will synchronize its time with a designated server, and will answer
    time sync requests from other clients.
*/

namespace rage
{

class netTimeSync
{
public:

    //PURPOSE
    //  Flags passed to Start()
    enum Flags
    {
        //Answers time sync requests.
        FLAG_SERVER     = 0x01,
        //Sends time sync requests.
        FLAG_CLIENT     = 0x02,
    };

    enum
    {
        //Default initial ping interval in milliseconds.
        DEFAULT_INITIAL_PING_INTERVAL   = 2 * 1000,
        //Default final ping interval in milliseconds.
        DEFAULT_FINAL_PING_INTERVAL     = 60 * 1000,
    };

    enum
    {
        //Maximum acceptable round trip time for a time pong message to be accepted.
        MAX_ACCEPTED_PONG_RTT       = 300,
        //Round trip time always accepted for pong messages.
        ALWAYS_ACCEPTED_PONG_RTT    = 100,
        //Round trip time deviation multiplier. Used to reject RTT times a factor larger than the average.
        MAX_PONG_RTT_DEV_MULT       = 2,
        //Maximum number of times that we'll reject a pong message before accepting
        MAX_REJECTIONS              = 10,
    };

    netTimeSync();

    virtual ~netTimeSync();

    //PURPOSE
    //  Start managing time.
    //PARAMS
    //  cxnMgr      - Connection manager used for communication.
    //  flags       - Bit combination of flags from Flags enum.
    //  serverEndpointId - EndpointId of the server to which we'll
    //                synchronize our time.  Ignored if FLAG_CLIENT
    //                is not present in flags.
    //  timeToken   - time token - we will only accept clock sync updates for this token when running in client mode
    //                             this can be used to specify a custom value for the token when starting in server mode (
    //  initialServerTime   - Initial server time.  If this is non-NULL the
    //                        the time sync will start in the synched state.
    //  channelId   - Channel on which time sync messages will be
    //                sent/received.  This should be a channel that
    //                is not used by any other system.
    //  initialPingInterval - Initial millisecond interval at which pings
    //                        will be sent.
    //  finalPingInterval   - Final millisecond interval at which pings will
    //                        be sent.
    //NOTES
    //  If FLAG_CLIENT is present in the flags parameter then time
    //  will be synchronized to the peer at serverAddr.
    //  If FLAG_CLIENT is not present in the flags parameter then the
    //  serverAddr parameter is ignored.
    //  If FLAG_SERVER is present in the flags parameter then
    //  time sync requests will be answered.
    //
    //  initialServerTime is typically used when there has been communication
    //  with the server prior to starting time synchronization and the server
    //  time is already known.
    //
    //  The ping interval is initialized to initialPingInterval and then
    //  doubled each time a pong message is received until it reaches
    //  the final ping interval.  This effectively implements a warm up
    //  period where ping times should stabilize.
    bool Start(netConnectionManager* cxnMgr,
                const unsigned flags,
                const EndpointId endpointId,
                const unsigned  timeToken,
                const unsigned* initialServerTime,
                const unsigned channelId,
                const unsigned initialPingInterval,
                const unsigned finalPingInterval);

    //PURPOSE
    //  Restarts time synchronization.
    //PARAMS
    //  flags           - Bit combination of flags from Flags enum.
    //  serverTimeToken - Optional time token to use when restarting in server mode
    //  serverEndpointId - Endpoint ID of the server to which we'll
    //                    synchronize our time.  Ignored if FLAG_CLIENT
    //                    is not present in flags.
    //NOTES
    //  This function is typically used to become a time server, or
    //  to change who is the current time server, e.g. during a host
    //  migration.
    bool Restart(const unsigned    flags,
                 const unsigned   *serverTimeToken,
                 const EndpointId  endpointId);

    //PURPOSE
    //  Stop managing time.
    bool Stop();

	//PURPOSE
	//  Pause the timer.  The current time will be maintained but no messaging
	//  will be processed between client / server. 
	void Pause();

	//PURPOSE
	//  Unpause the timer.
	void Unpause();

    //PURPOSE
    //  Update the current state.  This should be called as often as
    //  possible (at least once per frame).
    void Update();

    //PURPOSE
    //  Returns an estimate of the current time on the server.
    //NOTES
    //  Returns the result of GetLocalTime() until HasSynched() returns true.
    unsigned GetTime() const;

	//PURPOSE
    //  Returns the current local network time.
    static unsigned GetLocalTime();
	static unsigned GetLocalTime(const bool bUseServerSharedTime);

    //PURPOSE
    //  Generates a new token to use when in server mode
    static unsigned GenerateToken();

    //PURPOSE
    //  Returns the current time sync token, this is used to ensure clients
    //  only set their time from updates relating to their current session
    unsigned GetToken() const { return m_Token; }

	//PURPOSE
	//  Returns true if time synchronization has been started
	bool HasStarted() const;

    //PURPOSE
    //  Returns true if there has been a successful synchronization with the server,
    //  or if we're the server and not simultaneously a client.
    bool HasSynched() const;

    //PURPOSE
    //  Returns true if the instance is a server.
    //NOTES
    //  netTimeSync instances can be server and client simultaneously.
    bool IsServer() const;

    //PURPOSE
    //  Returns true if the instance is a client.
    //NOTES
    //  netTimeSync instances can be server and client simultaneously.
    bool IsClient() const;

	//Rolling average of the RTT between ping and pong messages
	u32  GetAverageRTT() const { return m_AverageRTT; }

	//PURPOSE
	//   Change standard behaviours
	static void SetUsingToken(const bool bUseToken);

#if !__NO_OUTPUT
	//PURPOSE
	//   Log clock information
	void Dump();
#endif

private:

    unsigned LocalTimeToServerTime(const unsigned localTime) const;

    void OnNetEvent(netConnectionManager* cxnMgr,
                    const netEvent* evt);

    netConnectionManager* m_CxnMgr;
    EndpointId m_ServerEndpointId;

	//Unique token to identify this clock token
	unsigned m_Token;

    //Difference between our local time and the server time.
    //Server time = local time + offset.
    unsigned m_ServerTimeOffset;

    netConnectionManager::Delegate m_Dlgt;

    //Next time we'll send a ping.
    unsigned m_NextPingTime;

    //Initial ping interval
    unsigned m_InitialPingInterval;

    //Final ping interval.
    unsigned m_FinalPingInterval;

    //Current ping interval.
    unsigned m_PingInterval;

    //Rolling average of the RTT between ping and pong messages.
    unsigned m_AverageRTT;

    //Rolling average of the RTT between ping and pong messages.
    unsigned m_NumRejectedPongs;

    unsigned m_SentSeq;
    unsigned m_RcvdSeq;

    unsigned m_Flags;

    unsigned m_ChannelId;

	mutable unsigned m_LastStableTime;

    bool m_HasSynched               : 1;
    mutable bool m_HasStableTime    : 1;

	bool m_IsPaused; 

	static bool ms_bUseToken;
};

}   //namespace rage

#endif  //NET_TIMESYNC_H
