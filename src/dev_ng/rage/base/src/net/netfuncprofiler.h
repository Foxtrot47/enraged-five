// 
// net/netfuncprofiler.h 
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_NETFUNCPROFILER_H
#define NET_NETFUNCPROFILER_H

#include "file/file_config.h"
#include "atl/array.h"

#define NET_PROFILER	(!__TOOL && !__FINAL && !__RGSC_DLL)

namespace rage
{

////////////////////////////////
// Net Rage Func Profiler
////////////////////////////////

#if NET_PROFILER

//PURPOSE
//	This is class for function profiling that supports hierarchical time bars 
//  Use PushProfiler to create a child profiler in the last not finished profiler
//  PopProfiler finishes the last pushed profiler, use the same name as parameter
//  to ensure that you didn't forget any pop after a push (it will assert otherwise)
//  At the end of the tracked sequence, call Finish on the root profiler.
//  Call Dump on the root profiler to get the full hierarchy outputted.

class netFunctionProfiler
{
public:

    static const unsigned MAX_LABEL_NAME = 256;

    class AutoProfilerPush
    {
    public:

        AutoProfilerPush(netFunctionProfiler &profiler, const char* labelName, bool withAssertOnLongFrames = false) :
        m_Profiler(profiler)
        {
            safecpy(m_Name, labelName);
            m_pushedSuccess = m_Profiler.PushProfiler(labelName, withAssertOnLongFrames);
        }

		AutoProfilerPush(netFunctionProfiler &profiler, const char* labelName, bool condition, bool withAssertOnLongFrames = false) :
			m_Profiler(profiler)
		{
			if(!condition)
			{
				m_pushedSuccess = false;
			}
			else
			{
				safecpy(m_Name, labelName);
				m_pushedSuccess = m_Profiler.PushProfiler(labelName, withAssertOnLongFrames);
			}
		}

        ~AutoProfilerPush()
        {
			if(m_pushedSuccess)
            {
				m_Profiler.PopProfiler(m_Name);
			}
        }

		bool DidPush() const { return m_pushedSuccess; }

    private:

        netFunctionProfiler &m_Profiler;
        char                m_Name[netFunctionProfiler::MAX_LABEL_NAME];
		bool				m_pushedSuccess;
    };

	 netFunctionProfiler(unsigned depth=0);
	~netFunctionProfiler();

	bool IsFinished() const;
	unsigned GetTotalChildDuration() const;
	unsigned GetDuration() const;
	void SetAssertOnLongFrames(bool isActivated);
	
	// Sets the time threshold to output the line on the latest pushed profiler
	void SetTimeThreshold(unsigned threshold);

	// Enable or disable the profiler. Call this only when a profiling frame is over (i.e. terminate has been called or no profiler has been started yet)
	void SetShouldProfile(bool shouldProfile);

	// Statically disable pushing and popping of profilers.
	// If you set it to false, remember to set it back to true once you're done. (See CNetwork::Update for example.)
	static void SetEnablePushAndPop(const bool bEnablePushAndPop) { sm_EnablePushAndPop = bEnablePushAndPop; }

	void StartFrame();
	void TerminateFrame(unsigned dumpIfLongerThanMs = 0);
	
	//! Outputs the profiler hierarchy, finishes the root profiler if it wasn't done
	void Dump(bool bForce = false, int index = 0);

	//! Creates a child Profiler. If the current child profiler has not been finished, creates a child for the deepest active child
	bool PushProfiler(const char* labelName, bool withAssertOnLongFrames = false);
	//! Finishes the last started profiler, could be the root if no child is still on.
	bool PopProfiler(const char* labelName, unsigned dumpIfLongerThanMs = 0);

    unsigned GetSubProfilerCount() const { return m_subprofilerCount; }
	unsigned GetSubProfilerCountForTop();

	//! Creates the profilers pool, needs to be called before using the profiler
	static void Init();
	//! Destroys the pool and free the profilers in it
	static void Shutdown();
	//! Set whether to run the profiler
	static void SetProfilerEnabled(const bool enabled); 
	static bool IsProfilerEnabled() { return sm_Enabled; }

	//! Return name
	const char* GetName() { return m_Name; }

	//! Get top most unfinished profiler
	static netFunctionProfiler* GetCurrentTop();

	// Return base profiler
	static netFunctionProfiler& GetProfiler() { return sm_profiler; }

private:
	
	static const char		SHIFTING_CHARACTER	= '\t';  // char used to shift the nested calls in the output
	static const unsigned	DEFAULT_LOGGING_THRESHOLD = 1;
	static const unsigned   MAX_SUBPROFILERS = 100;		// Maximum of sub profilers to avoid OOM

	void Start(const char* name);
	void Finish(unsigned dumpIfLongerThanMs = 0);
	void Clear();
	void SetIgnoreChildrenBelowMs(unsigned threshold);

    //! Puts the last used profiler back in the pool
	void ReleaseLastProfiler();

	//! Sums all the children to the profiler node and clear the children list (used to free memory when exhausting the pool)
	void Collapse();

	//! Return the current stacked profiler, nullptr if there's none
	netFunctionProfiler* Top();

	netFunctionProfiler* m_parentProfiler;
	netFunctionProfiler* m_SubProfilers[MAX_SUBPROFILERS];
	unsigned m_subprofilerCount;

	char m_Name[MAX_LABEL_NAME];
	unsigned m_TimeStarted;
	unsigned m_TimeFinished;
	unsigned m_Depth;
	bool m_AssertOnLongFrame;
	unsigned m_IgnoreChildrenBelowMs;
	bool m_ShouldProfile;
	bool m_DumpForTooManySubProfilers;

	static bool sm_Started;
	static bool sm_Enabled; 
	static bool sm_EnablePushAndPop;
	
	// Single instance, root of the profiler tree that we're going to build
	static netFunctionProfiler sm_profiler;

	// Pool of profilers so we dont need to instantiate them during runtime
	static const int MAX_PROFILER_COUNT = 256;
	static netFunctionProfiler* sm_profilerPool[MAX_PROFILER_COUNT];
	static int sm_nextAvailableProfilerInPool;

	// Returns a new profiler if the pool is not exhausted, nullptr otherwise
	static netFunctionProfiler* GetNewProfiler(const char* name, unsigned depth);
};
#endif // !__TOOL && !__FINAL && !__RGSC_DLL


#if NET_PROFILER
// combines a symbol with the current line number to create a unique variable name
#define NPROFILE_COMBINE_HELPER(x,y)	x ## y
#define NPROFILE_COMBINE(x,y)	NPROFILE_COMBINE_HELPER(x,y)
#define NPROFILE_UNIQUE(x)		NPROFILE_COMBINE(x,__LINE__)

// Do not wrap the operation (a) inside of a new scope {}. Otherwise it causes destructors to get called at the end of the scope.
// Example: causes critical section tokens to be locked and instantly unlocked
#define NPROFILE(a)					bool NPROFILE_UNIQUE(__rprofile_pushed) = netFunctionProfiler::GetProfiler().PushProfiler(#a);			\
									a;																										\
									if(NPROFILE_UNIQUE(__rprofile_pushed)) netFunctionProfiler::GetProfiler().PopProfiler(#a);

#define NPROFILE_COND(a, c)			bool NPROFILE_UNIQUE(__rprofile_pushed) = c && netFunctionProfiler::GetProfiler().PushProfiler(#a);		\
									a;																										\
									if(NPROFILE_UNIQUE(__rprofile_pushed)) netFunctionProfiler::GetProfiler().PopProfiler(#a);

#define NPROFILE_BLOCK(a)			netFunctionProfiler::AutoProfilerPush profiler(netFunctionProfiler::GetProfiler(), #a);
#define NPROFILE_BLOCK_COND(a, c)	netFunctionProfiler::AutoProfilerPush profiler(netFunctionProfiler::GetProfiler(), #a, c, false);

#define NPROFILE_ONLY(...)          __VA_ARGS__
#else
////////////////////////////////
// Net Rage Func Profiler Disabled
////////////////////////////////
#define NPROFILE(a)					a; 
#define NPROFILE_COND(a, c)			a; 
#define NPROFILE_BLOCK(a)
#define NPROFILE_BLOCK_COND(a, c)
#define NPROFILE_ONLY(...)          
#endif // NET_PROFILER

}   //namespace rage

#endif // NET_NETFUNCPROFILER_H
