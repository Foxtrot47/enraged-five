// 
// net/resolver.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "resolver.h"

#include "tcp.h"

#include "diag/channel.h"
#include "file/file_config.h"
#include "net.h"
#include "netaddress.h"
#include "netdiag.h"
#include "nethardware.h"
#include "netsocket.h"
#include "string/string.h"
#include "system/nelem.h"
#include "system/threadpool.h"
#include "string/stringutil.h"

#define USE_RESOLVER_CACHE 1

#if USE_RESOLVER_CACHE
#include "system/timer.h"
#endif

#if RSG_NP
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net.h>
#elif RSG_PC || RSG_DURANGO
#define STRICT
#pragma warning(disable: 4668)
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma warning(error: 4668)
#endif

namespace rage
{

extern sysThreadPool netThreadPool;

RAGE_DECLARE_CHANNEL(ragenet);
RAGE_DEFINE_SUBCHANNEL(ragenet, resolver)
#undef __rage_channel
#define __rage_channel ragenet_resolver

#if USE_RESOLVER_CACHE
sysCriticalSectionToken m_CacheCs;

class CachedDnsEntry
{
public:
	static const unsigned NET_CACHED_DNS_TTL = 60 * 1000;

	CachedDnsEntry()
	{
		memset(m_Hostname, 0, sizeof(m_Hostname));
		m_Ip.Clear();
		m_Timestamp = 0;
	}

	unsigned GetTimeToLiveMs()
	{
		unsigned elapsed = sysTimer::GetSystemMsTime() - m_Timestamp;
		return (elapsed < NET_CACHED_DNS_TTL) ? NET_CACHED_DNS_TTL - elapsed : 0;
	}

	bool IsValid()
	{
		return (m_Ip.IsValid()) &&
			   ((sysTimer::GetSystemMsTime() - m_Timestamp) < NET_CACHED_DNS_TTL);
	}

	bool IsMatch(const char* hostname)
	{
		return IsValid() && (stricmp(m_Hostname, hostname) == 0);
	}

	void Reset(const char* hostname, const netIpAddress& ip)
	{
		safecpy(m_Hostname, hostname);
		m_Ip = ip;
		m_Timestamp = sysTimer::GetSystemMsTime();
	}

	const char* GetHostname() {return m_Hostname;} 
	const netIpAddress& GetIp() {return m_Ip;}

private:
	char m_Hostname[NET_MAX_HOSTNAME_CHARS];
	netIpAddress m_Ip;
	unsigned m_Timestamp;
};

static const unsigned NET_MAX_CACHED_DNS_ENTRIES = 5;
static CachedDnsEntry s_CachedDnsEntries[NET_MAX_CACHED_DNS_ENTRIES];

static void CacheDnsEntry(const char* hostname, const netIpAddress& ip)
{
	SYS_CS_SYNC(m_CacheCs);

	int cacheIndex = -1;
	for(unsigned i = 0; i < COUNTOF(s_CachedDnsEntries); ++i)
	{
		if(s_CachedDnsEntries[i].IsMatch(hostname))
		{
			cacheIndex = i;
			break;
		}
	}

	if(cacheIndex < 0)
	{
		unsigned lowestTtl = 0xFFFFFFFF;
		for(unsigned i = 0; i < COUNTOF(s_CachedDnsEntries); ++i)
		{
			if(s_CachedDnsEntries[i].IsValid() == false)
			{
				cacheIndex = i;
				break;
			}

			unsigned ttl = s_CachedDnsEntries[i].GetTimeToLiveMs();
			if(ttl < lowestTtl)
			{
				lowestTtl = ttl;
				cacheIndex = i;
			}
		}
	}

	if(netVerify(cacheIndex >= 0))
	{
		s_CachedDnsEntries[cacheIndex].Reset(hostname, ip);
	}
}

static bool GetCachedDnsEntry(const char* hostname, netIpAddress* ip)
{
	SYS_CS_SYNC(m_CacheCs);

	char hostnameStripped[NET_MAX_HOSTNAME_CHARS];
	StringTrim(hostnameStripped, hostname, sizeof(hostnameStripped));

	for(unsigned i = 0; i < COUNTOF(s_CachedDnsEntries); ++i)
	{
		if(s_CachedDnsEntries[i].IsMatch(hostnameStripped))
		{
			*ip = s_CachedDnsEntries[i].GetIp();
			return true;
		}
	}

	return false;
}
#endif

class ResWorkItem : public sysThreadPool::WorkItem
{
public:

    ResWorkItem()
        : m_Ip()
        , m_pIp(NULL)
        , m_State(STATE_NONE)
        , m_Status(NULL)
    {
        m_Hostname[0] = '\0';
    }

    virtual ~ResWorkItem()
    {
        netAssert(!this->Pending());

        this->Clear();
    }

    void Clear()
    {
		m_Ip.Clear();
		m_pIp = NULL;
		m_State = STATE_NONE;
		m_Status = NULL;
		m_Hostname[0] = '\0';
    }

    bool Configure(const char* hostname,
                    netIpAddress* ip,
                    netStatus* status)
    {
        bool success = false;

        status->SetPending();

        if(netVerify(STATE_NONE == m_State || STATE_FINISHED == m_State))
        {
            this->Clear();

			StringTrim(m_Hostname, hostname, sizeof(m_Hostname));

            m_pIp = ip;
            m_State = STATE_RESOLVING;
            m_Status = status;
            success = true;
        }
        else
        {
            status->SetFailed();
        }

        return success;
    }

    virtual void Cancel()
    {
        SYS_CS_SYNC(m_Cs);
        if(STATE_RESOLVING == m_State)
        {
            netDebug("Canceling resolution of host: \"%s\", Active: %s", m_Hostname, Active() ? "True" : "False");
            m_Status->SetCanceled();
			m_Status = NULL;
			m_State = STATE_FINISHED;

			// Only call Clear if the work item is not active (i.e. still queued)
			// Otherwise, allow DoWork to complete
			if(!Active())
			{
				this->Clear();
				Finish();
			}
        }
    }

    virtual void DoWork()
    {
        bool success = false;

        if(m_Ip.FromString(m_Hostname) && m_Ip.IsValid())
        {
            success = true;
        }
        else
        {
            //Host address is a name
#if RSG_ORBIS
            const int memPoolId = sceNetPoolCreate("netResolver", 4*1024, 0);
            if(memPoolId >= 0)
            {
                SceNetId resolverId = sceNetResolverCreate("netResolver", memPoolId, 0);

                if(resolverId >= 0)
                {
                    SceNetInAddr ina;
					SceNetId err = sceNetResolverStartNtoa(resolverId, m_Hostname, &ina, 0, 0, 0);
                    if(0 <= err)
                    {
						netIpV4Address ipV4(net_ntohl(ina.s_addr));
						m_Ip = netIpAddress(ipV4);
                        success = true;
                    }

                    sceNetResolverDestroy(resolverId);
                }

                sceNetPoolDestroy(memPoolId);
            }
#elif RSG_PC || RSG_DURANGO
			ADDRINFO hints;
			memset(&hints, 0, sizeof (hints));
#if IPv4_IPv6_DUAL_STACK
			hints.ai_family = AF_INET6;			// IPv6 address family
			hints.ai_flags = AI_V4MAPPED;		// if no IPv6 address exists, ask for IPv4-mapped IPv6 addresses
#else
			hints.ai_family = AF_INET;			// IPv4 address family
			hints.ai_flags = 0;
#endif

			// DNS lookup
			ADDRINFO* addrInfoList = NULL;
			if(0 == getaddrinfo(m_Hostname, NULL, &hints, &addrInfoList))
			{
				// use the first address in the list
				ADDRINFO* addrInfo = addrInfoList;

				if(addrInfo != NULL)
				{
#if IPv4_IPv6_DUAL_STACK
					netAssert(addrInfo->ai_family == AF_INET6);
					sockaddr_in6 sin = *(sockaddr_in6*)addrInfo->ai_addr;
					m_Ip = netIpAddress(netIpV6Address(sin.sin6_addr.u.Byte));
#else
					netAssert(addrInfo->ai_family == AF_INET);
					sockaddr_in sin = *(sockaddr_in*)addrInfo->ai_addr;
					m_Ip = netIpAddress(netIpV4Address(net_ntohl(sin.sin_addr.s_addr)));
#endif
					freeaddrinfo(addrInfoList);
					addrInfoList = NULL;

					success = true;
				}
			}
			else
			{
				netError("getaddrinfo() failed for %s", m_Hostname);
			}
#else
            struct hostent *he = gethostbyname(m_Hostname);
            if(0 == he)
            {
                netError("gethostbyname() failed for %s", m_Hostname);
            }
            else
            {
				netIpV4Address ipV4(net_ntohl(*(u32*)he->h_addr));
				m_Ip = netIpAddress(ipV4);
                success = true;
            }
#endif
        }

        SYS_CS_SYNC(m_Cs);
        if(m_Status)    //Make sure we weren't canceled.
        {
            if(success)
            {
				*m_pIp = m_Ip;

				netDebug("Resolved host: \"%s\" to " NET_IP_FMT,
                        m_Hostname,
                        NET_IP_FOR_PRINTF(m_Ip));

#if USE_RESOLVER_CACHE
				CacheDnsEntry(m_Hostname, m_Ip);
#endif // USE_RESOLVER_CACHE
			}
            else
            {
                netDebug("Failed to resolve host: \"%s\"", m_Hostname);
				m_pIp->Clear();
            }

            success ? m_Status->SetSucceeded() : m_Status->SetFailed();
        }

        m_State = STATE_FINISHED;
    }

    enum State
    {
        STATE_NONE,
        STATE_RESOLVING,
        STATE_FINISHED
    };

    sysCriticalSectionToken m_Cs;
    char m_Hostname[NET_MAX_HOSTNAME_CHARS];
    netIpAddress m_Ip;
    netIpAddress* m_pIp;
    State m_State;
    netStatus* m_Status;
};

//Maximum number of pending resolve requests. This should match 
//MAX_RESOLVE_REQUEST_QUEUE_LENGTH in rlxlsp.cpp
static const int MAX_NET_RESOLVE_REQUEST_QUEUE_LENGTH	= 8;
static ResWorkItem s_ResWorkItems[MAX_NET_RESOLVE_REQUEST_QUEUE_LENGTH];

bool
netResolver::ResolveHost(const char* hostname,
                            netIpAddress* ip,
                            netStatus* status)
{
    bool success = false;

    netDebug("Resolving: \"%s\"...", hostname);
	
#if USE_RESOLVER_CACHE
	if(GetCachedDnsEntry(hostname, ip))
	{
		netDebug("Resolved host from cache: \"%s\" to " NET_IP_FMT,
				hostname,
				NET_IP_FOR_PRINTF(*ip));

		if(status)
		{
			status->SetPending();
			status->SetSucceeded();
		}
		return true;
	}
#endif // USE_RESOLVER_CACHE

	ResWorkItem* wi = NULL;

    for(int i = 0; i < COUNTOF(s_ResWorkItems); ++i)
    {
        if(!s_ResWorkItems[i].Pending())
        {
            wi = &s_ResWorkItems[i];
            break;
        }
    }

    if(wi)
    {
        if(wi->Configure(hostname, ip, status))
		{
			if(netThreadPool.QueueWork(wi))
			{
				success = true;
			}
			else
			{
				netError("Error queuing host resolution work item");
				wi->Cancel();
			}
		}
		else
		{
			netWarning("Failed to configure the resolver");
			status->SetPending(); status->SetFailed();
		}
    }
    else
    {
        netWarning("Resolver pool is exhausted");
        status->SetPending(); status->SetFailed();
    }

    return success;
}

void
netResolver::Cancel(netStatus* status)
{
    for(int i = 0; i < COUNTOF(s_ResWorkItems); ++i)
    {
        if(status == s_ResWorkItems[i].m_Status)
        {
            netThreadPool.CancelWork(s_ResWorkItems[i].GetId());
            break;
        }
    }
}

} // namespace rage
