// 
// net/netnonce.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_NETNONCE_H
#define NET_NETNONCE_H

namespace rage
{

//PURPOSE
//  Generates a 64-bit nonce.
u64 netGenerateNonce();

}   //namespace rage

#endif  //NET_NONCE_H
