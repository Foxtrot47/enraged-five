//
// net/natpcp.h
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#ifndef NET_NAT_PCP_H
#define NET_NAT_PCP_H

#include "system/criticalsection.h"
#include "net.h"
#include "netaddress.h"
#include "nethardware.h"
#include "netsocket.h"
#include "relay.h"
#include "status.h"

namespace rage
{

//PURPOSE
// Encapsulates information collected about the local NAT's PCP or NAT-PMP capabilities and results of address reservation.
class netNatPcpInfo
{
public:
	enum State
	{
		NAT_PCP_UNATTEMPTED,
		NAT_PCP_IN_PROGRESS,
		NAT_PCP_SUCCEEDED,
		NAT_PCP_FAILED,
		NAT_PCP_NUM_STATES,
	};

	enum ReserveMethod
	{
		NAT_PCP_METHOD_NONE = 0,
		NAT_PCP_METHOD_PMP = 1,
		NAT_PCP_METHOD_PCP = 2,
		NAT_PCP_NUM_METHODS
	};

	static const unsigned MAX_EXPORTED_SIZE_IN_BITS = datBitsNeeded<NAT_PCP_NUM_STATES>::COUNT +				// m_State
													  (netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES << 3) +		// m_RequestedAddress
													  (netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES << 3) +		// m_ReservedAddress
													  datBitsNeeded<NAT_PCP_NUM_METHODS>::COUNT +				// m_ReserveMethod
													  (sizeof(unsigned) << 3) +									// m_PortReservationTimeMs
													  (sizeof(unsigned) << 3) +									// m_PcpNewFlowResultCode
													  (sizeof(u16) << 3) +										// m_NumSuccessfulRefreshes
													  (sizeof(u16) << 3) +										// m_NumFailedRefreshes
													  (sizeof(u16) << 3);										// m_NumSequentialFailedRefreshes

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((MAX_EXPORTED_SIZE_IN_BITS + 7) & ~7) >> 3;		// round up to nearest multiple of 8 then divide by 8

	netNatPcpInfo()
	{
		Clear();
	}

	void Clear()
	{
		m_State = NAT_PCP_UNATTEMPTED;
		m_RequestedAddr.Clear();
		m_ReservedAddr.Clear();
		m_ReserveMethod = NAT_PCP_METHOD_NONE;
		m_PortReservationTimeMs = 0;
		m_PcpNewFlowResultCode = 0;
		m_NumSuccessfulRefreshes = 0;
		m_NumFailedRefreshes = 0;
		m_NumSequentialFailedRefreshes = 0;
	}

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

#if !__NO_OUTPUT
	const char*
	GetReserveMethodString(const ReserveMethod method) const
	{
		switch(method)
		{
		case NAT_PCP_METHOD_NONE: return "None"; break;
		case NAT_PCP_METHOD_PCP: return "Port Control Protocol (PCP)"; break;
		case NAT_PCP_METHOD_PMP: return "Port Mapping Protocol (NAT-PMP)"; break;
		case NAT_PCP_NUM_METHODS: return "NAT_PCP_NUM_METHODS"; break;
		}

		return "NAT_PCP_INVALID_METHOD";
	}

	void DumpInfo() const;
#endif

	void SetState(const State state) { if(m_State != state) { m_State = state; } }
	State GetState() const {return m_State;}

	void SetRequestedAddress(const netSocketAddress& requestedAddr) { m_RequestedAddr = requestedAddr; }
	const netSocketAddress& GetRequestedAddress() const { return m_RequestedAddr; }

	void SetReservedAddress(const netSocketAddress& reservedAddr) { m_ReservedAddr = reservedAddr; }
	const netSocketAddress& GetReservedAddress() const { return m_ReservedAddr; }

	void SetReserveMethod(const ReserveMethod reserveMethod) { m_ReserveMethod = reserveMethod; }
	ReserveMethod GetReserveMethod() const { return m_ReserveMethod; }

	void SetPortReservationTimeMs(const unsigned timeMs) { m_PortReservationTimeMs = timeMs; }
	unsigned GetPortReservationTimeMs() const { return m_PortReservationTimeMs; }
	
	void SetPcpNewFlowResultCode(const unsigned resultCode) { m_PcpNewFlowResultCode = resultCode; }
	unsigned GetPcpNewFlowResultCode() const { return m_PcpNewFlowResultCode; }

	void SetNumSuccessfulRefreshes(const u16 num) { m_NumSuccessfulRefreshes = num; }
	u16 GetNumSuccessfulRefreshes() const { return m_NumSuccessfulRefreshes; }

	void SetNumFailedRefreshes(const u16 num) { m_NumFailedRefreshes = num; }
	u16 GetNumFailedRefreshes() const { return m_NumFailedRefreshes; }

	void SetNumSequentialFailedRefreshes(const u16 num) { m_NumSequentialFailedRefreshes = num; }
	u16 GetNumSequentialFailedRefreshes() const { return m_NumSequentialFailedRefreshes; }

private:
	State m_State;
	netSocketAddress m_RequestedAddr;
	netSocketAddress m_ReservedAddr;
	ReserveMethod m_ReserveMethod;
	unsigned m_PortReservationTimeMs;
	unsigned m_PcpNewFlowResultCode;
	u16 m_NumSuccessfulRefreshes;
	u16 m_NumFailedRefreshes;
	u16 m_NumSequentialFailedRefreshes;
};

//PURPOSE
//  Detects local PCP and NAT-PMP capabilities and sets up address reservation
//  if PCP or NAT-PMP are available.
class netNatPcp
{
public:
	static bool Init();
	static void Shutdown();
	static void Update();

	//PURPOSE
	// Returns true if Init() has been called successfully.
	static bool IsInitialized();

	//PURPOSE
	// Returns true if we've attempted to reserve an address, or if we
	// won't attempt to reserve an address (eg. due to tunables, command line, etc.)
	// Note that refreshes and deletions can still occur after this returns true.
	static bool IsInitialWorkCompleted();

	static const netNatPcpInfo& GetPcpInfo() {return sm_PcpInfo;}

	// deletes any PCP/NAT-PMP address reservation we've set up.
	// Should be called at game shutdown. Game must wait until the status is completed.
	static void DeletePcpAddressReservation(netStatus* status);
	
	// tunables
	static void SetAllowPcpAddressReservation(const bool allowPcpAddressReservation); 
	static void SetReserveRelayMappedAddress(const bool reserveRelayMappedAddr);
	static bool GetReserveRelayMappedAddress();
	static void SetPcpRefreshTimeMs(const unsigned pcpRefreshTimeMs); 
	static void SetTunablesReceived();

	static unsigned GetPcpLeaseDurationSec();
	static unsigned GetDeviceDiscoveryTimeoutMs();

private:
	static bool sm_Initialized;
	static netNatPcpInfo sm_PcpInfo;
	static bool sm_CanBegin;
	static unsigned sm_DeviceDiscoveryTimeoutMs;
	static unsigned sm_PcpRefreshStartTime;
	static unsigned sm_PcpLeaseDurationSec;
	static bool sm_AllowPcpAddressReservation;
	static bool sm_ReserveRelayMappedAddr;
	static unsigned sm_PcpRefreshTimeMs;
	static unsigned sm_MaxSequentialFailedRefreshes;

	static netNatPcpInfo::State m_DeleteState;

	static sysCriticalSectionToken m_Cs;
};

}   //namespace rage

#endif  //NET_NAT_PCP_H
