// 
// net/netutil.h 
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_UTIL_H
#define NET_UTIL_H

namespace rage
{
	template<typename T, unsigned BUCKET_SIZE, unsigned MAX_SIZE>
	class netDynArray
	{
	public:

		netDynArray()
			: m_Allocator(0)
			, m_Buckets(0)
			, m_Size(0)
			, m_Capacity(0)
			, m_BucketSize(1)
			, m_BucketShift(0)
			, m_BucketMask(0)
		{
			while(m_BucketSize < BUCKET_SIZE)
			{
				m_BucketSize <<= 1;
				++m_BucketShift;
			}

			m_BucketMask = m_BucketSize - 1;
		}

		void Init(sysMemAllocator* allocator)
		{
			if(netVerify(!m_Allocator))
			{
				m_Allocator = allocator;
				netAssert(!m_Buckets);
				netAssert(!m_Size);
				netAssert(!m_Capacity);
			}
		}

		void Shutdown()
		{
			if(m_Allocator)
			{
				this->Resize(0);
				m_Allocator = 0;
				netAssert(!m_Buckets);
				netAssert(!m_Size);
				netAssert(!m_Capacity);
			}
		}

		unsigned Size() const
		{
			return m_Size;
		}

		int IndexOf(const T* t) const
		{
			int index = -1;
			const unsigned numBuckets = m_Capacity >> m_BucketShift;
			for(unsigned i = 0; i < numBuckets; ++i)
			{
				if(t >= m_Buckets[i]
				&& t < m_Buckets[i] + m_BucketSize)
				{
					const unsigned base = i << m_BucketShift;
					const unsigned offset = ptrdiff_t_to_int(t - m_Buckets[i]);
					if(base + offset < m_Size)
					{
						index = int(base + offset);
						break;
					}
				}
			}

			return index;
		}

		T& operator[](const unsigned index)
		{
			FastAssert(index < m_Size);
			return m_Buckets[index >> m_BucketShift][index & m_BucketMask];
		}

		const T& operator[](const unsigned index) const
		{
			return const_cast<netDynArray<T, BUCKET_SIZE, MAX_SIZE>*>(this)->operator[](index);
		}

		bool Resize(const unsigned newSize)
		{
			bool success = false;

			const unsigned newCapacity = (newSize + m_BucketSize - 1) & ~m_BucketMask;
			const unsigned numNewBuckets = newCapacity >> m_BucketShift;
			const unsigned numCurBuckets = m_Capacity >> m_BucketShift;

			if(newCapacity > m_Capacity)
			{
				if(netVerify(newCapacity <= MAX_SIZE))
				{
					T** newBuckets = 0;

					if(m_Allocator->GetLargestAvailableBlock() >= numNewBuckets * sizeof(T*))
					{
						newBuckets = (T**) m_Allocator->RAGE_LOG_ALLOCATE(numNewBuckets * sizeof(T*), 0);
					}

					if(newBuckets)
					{
						success = true;

						if(m_Buckets)
						{
							sysMemCpy(newBuckets, m_Buckets, numCurBuckets * sizeof(T**));
							m_Allocator->Free(m_Buckets);
						}

						m_Buckets = newBuckets;

						for(unsigned i = numCurBuckets; i < numNewBuckets; ++i)
						{
							m_Buckets[i] = 0;

							if(m_Allocator->GetLargestAvailableBlock() >= m_BucketSize * sizeof(T))
							{
								m_Buckets[i] = (T*) m_Allocator->RAGE_LOG_ALLOCATE(m_BucketSize * sizeof(T), 0);
							}

							if(m_Buckets[i])
							{
								for(unsigned j = 0; j < m_BucketSize; ++j)
								{
									new (&m_Buckets[i][j]) T;
								}

								m_Capacity += m_BucketSize;
							}
							else
							{
								success = false;
								break;
							}
						}
					}
				}
			}
			else if(newCapacity < m_Capacity)
			{
				for(unsigned i = numNewBuckets; i < numCurBuckets; ++i)
				{
					for(unsigned j = 0; j < m_BucketSize; ++j)
					{
						m_Buckets[i][j].~T();
					}

					m_Allocator->Free(m_Buckets[i]);

					FastAssert(m_Capacity >= m_BucketSize);
					m_Capacity -= m_BucketSize;
				}

				if(numNewBuckets)
				{
					T** newBuckets = 0;

					if(m_Allocator->GetLargestAvailableBlock() >= numNewBuckets * sizeof(T*))
					{
						newBuckets = (T**) m_Allocator->RAGE_LOG_ALLOCATE(numNewBuckets * sizeof(T*), 0);
					}

					if(newBuckets)
					{
						sysMemCpy(newBuckets, m_Buckets, numNewBuckets * sizeof(T**));
						m_Allocator->Free(m_Buckets);
						m_Buckets = newBuckets;

						success = true;
					}
				}
				else
				{
					m_Allocator->Free(m_Buckets);
					m_Buckets = 0;
					success = true;
				}
			}
			else
			{
				success = true;
			}

			if(success)
			{
				m_Size = newSize;
			}
			else if(m_Size > m_Capacity)
			{
				m_Size = m_Capacity;
			}

			netAssert(m_Size <= m_Capacity);

			return success;
		}

		void Clear()
		{
			this->Resize(0);
		}

	private:

		sysMemAllocator* m_Allocator;
		T** m_Buckets;
		unsigned m_Size;
		unsigned m_Capacity;
		unsigned m_BucketSize;
		unsigned m_BucketShift;
		unsigned m_BucketMask;
	};

//PURPOSE
//Maintains an exponential moving average (EMA).
//See https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
//Choose a value for alpha between 0 and 1 that is appropriate for your usage.
//The closer alpha is to 1, the faster the moving average updates in response to new values.
//The closer alpha is to 0, the longer the impact of older values.

template<typename T>
class netMovingAverage
{
public:

	netMovingAverage(const float alpha)
	{
		this->Clear();
		SetAlpha(alpha);
	}

	void Clear()
	{
		m_Ema = 0.0f;
		m_Alpha = 0.0f;
		m_NegAlpha = 0.0f;
		m_HasFirstSample = false;
	}

	void SetAlpha(const float alpha)
	{
		netAssert(alpha > 0.0f && alpha <= 1.0f);

		m_Alpha = alpha;
		m_NegAlpha = 1.0f - m_Alpha;
	}

	void Update(const T sample)
	{
		netAssert(m_Alpha > 0.0f && m_Alpha <= 1.0f);

		if(m_HasFirstSample)
		{
			m_Ema = (m_Alpha * sample) + (m_NegAlpha * m_Ema);
		}
		else
		{
			m_HasFirstSample = true;
			m_Ema = (float)sample;
		}
	}

	T Get() const
	{
		return (T)m_Ema;
	}

private:
	float m_Ema;
	float m_Alpha;
	float m_NegAlpha;
	bool m_HasFirstSample;
};

}   //namespace rage

#endif  //NET_UTIL_H
