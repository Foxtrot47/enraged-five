// 
// net/tcp.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "tcp.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "diag/seh.h"
#include "file/file_config.h"
#include "netdiag.h"
#include "netaddress.h"
#include "nethardware.h"
#include "string/string.h"
#include "math/amath.h"
#include "net/crypto.h"
#include "system/nelem.h"
#include "system/timer.h"
#include "system/memory.h"
#include "profile/rocky.h"

#if RSG_PC || RSG_DURANGO
#if !defined(STRICT)
#define STRICT
#endif
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma warning(pop)
#elif RSG_SCE
#include <errno.h>
#include <net.h>
#define SO_NBIO SCE_NET_SO_NBIO
#include <netinet/in.h>     //NOTE: Must come before sockinfo.h, because it doesn't include it itself.
#include <netinet/tcp.h>
#include <sys/select.h>
#include <sys/time.h>
#endif // RSG_PC || RSG_DURANGO

#if RSG_IOS || RSG_ANDROID || RSG_LINUX
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>
#endif

#if RSG_LINUX
#include <sys/signal.h>
#endif

#include "wolfssl/ssl.h"
#include "wolfssl/error-ssl.h"

#if defined(WSAEWOULDBLOCK)
#define NET_EWOULDBLOCK WSAEWOULDBLOCK
#elif defined(SYS_NET_EWOULDBLOCK)
#define NET_EWOULDBLOCK SYS_NET_EWOULDBLOCK
#else
#define NET_EWOULDBLOCK EWOULDBLOCK
#endif

#if defined(WSAEINPROGRESS)
#define NET_EINPROGRESS WSAEINPROGRESS
#elif defined(SYS_NET_EINPROGRESS)
#define NET_EINPROGRESS SYS_NET_EINPROGRESS
#else
#define NET_EINPROGRESS EINPROGRESS
#endif

#if defined(WSAENOTSOCK)
#define NET_EBADF WSAENOTSOCK
#elif defined(SYS_NET_EBADF)
#define NET_EBADF SYS_NET_EBADF
#else
#define NET_EBADF EBADF
#endif

#if defined(WSAEINTR)
#define NET_EINTR WSAEINTR
#elif defined(SYS_NET_EINTR)
#define NET_EINTR SYS_NET_EINTR
#else
#define NET_EINTR EINTR
#endif

typedef WOLFSSL SSL;  

namespace rage
{

PARAM(netssldump, "Output additional debugging information about SSL connections. Set -netssldump=verbose to dump send/receive buffers as well.");

struct netTcpSocketInfo
{
	netTcpSocketInfo()
		: m_SktFd(NET_INVALID_SOCKET_FD)
		, m_Ssl(NULL)
		, m_IsAllocated(false)
		, m_CloseFdPending(false)
		, m_Disconnected(false)
#if FAKE_DROP_LATENCY
		, m_IgnoreUplinkDisconnect(false)
#endif
		, m_BytesSent(0)
		, m_BytesSentOnWire(0)
#if ENABLE_BANDWIDTH_TELEMETRY
		, m_ConnectReason(CR_INVALID)
#endif // !__NO_OUTPUT
	{
		OUTPUT_ONLY(m_ContextStr[0] = '\0';)
	}
	
#if !__NO_OUTPUT
	char m_ContextStr[64];
#endif // !__NO_OUTPUT

    u32 m_BytesSent;
    u32 m_BytesSentOnWire;
#if ENABLE_BANDWIDTH_TELEMETRY
    ConnectReason m_ConnectReason;
#endif // ENABLE_BANDWIDTH_TELEMETRY

	// protect against simultaneous SSL_read/SSL_write by multiple threads.
	sysCriticalSectionToken m_CsSslReadWrite;
	netSocketFd m_SktFd;
	WOLFSSL* m_Ssl;

	bool m_IsAllocated		: 1;
	bool m_CloseFdPending	: 1;
	bool m_Disconnected		: 1;
#if FAKE_DROP_LATENCY
	bool m_IgnoreUplinkDisconnect : 1;
#endif
};

//Allow this to be defined externally
#ifndef NET_TCP_MAX_SOCKETS
#define NET_TCP_MAX_SOCKETS 64
#endif

struct netTcpSettings
{
    static const unsigned NET_TCP_DEFAULT_MAX_READ_LOOPS = 120; // The number of loops we do on a socket read before continuing
    static const unsigned NET_TCP_DEFAULT_SO_RCV_BUF = 0; // 0 means don't set it
    static const unsigned NET_TCP_MAX_PROCESSING_TIME_MS = 2; // yield CPU time if processing ops takes longer than this
    static const unsigned NET_TCP_THREAD_YIELD_TIME_MS = 20; // the amount of time to yield to other threads between long operations

	netTcpSettings()
		: m_SoRcvBufferSize(NET_TCP_DEFAULT_SO_RCV_BUF)
		, m_MaxReadLoops(NET_TCP_DEFAULT_MAX_READ_LOOPS)
		, m_MaxProcessingTimeMs(NET_TCP_MAX_PROCESSING_TIME_MS)
		, m_ThreadYieldTimeMs(NET_TCP_THREAD_YIELD_TIME_MS)
	{

	}
	
    unsigned m_SoRcvBufferSize;
    unsigned m_MaxReadLoops;
    unsigned m_MaxProcessingTimeMs;
    unsigned m_ThreadYieldTimeMs;
};

static const int MAX_SOCKET_INFOS   = NET_TCP_MAX_SOCKETS;
static netTcpSocketInfo s_SocketInfos[MAX_SOCKET_INFOS];
static netTcpSettings s_TcpSettings;

#if ENABLE_BANDWIDTH_TELEMETRY

class netTcpStat
{
public:
	netTcpStat() : m_Sent(0), m_SentOnWire(0), m_Received(0) {}

public:
	// This is the number of bytes explicitly sent by netTcp::Send. It just counts raw, plaintext data being sent by the calling code.
	u32 m_Sent;

	// This is a best-guess of the minimum number of bytes actually being sent on the wire. This captures TLS overhead, some basic attempt
	// to estimate TCP/IP headers, and data being sent to negotiate opening the connection.
	u32 m_SentOnWire;

	// This is the number of bytes received by netTcp::Receive
	u32 m_Received;
};

class netTcpStats
{
public:
	void Sent(const unsigned bytes BANDWIDTH_TRACKING_ONLY(, ConnectReason reason))
	{
		sysInterlockedAdd(&m_Total.m_Sent, bytes);
#if ENABLE_BANDWIDTH_TRACKING
		sysInterlockedAdd(&m_ByConnectReason[reason].m_Sent, bytes);
#endif
	}

	void SentOnWire(const unsigned bytes BANDWIDTH_TRACKING_ONLY(, ConnectReason reason))
	{
		sysInterlockedAdd(&m_Total.m_SentOnWire, bytes);
#if ENABLE_BANDWIDTH_TRACKING
		sysInterlockedAdd(&m_ByConnectReason[reason].m_SentOnWire, bytes);
#endif
	}

	void Received(const unsigned bytes BANDWIDTH_TRACKING_ONLY(, ConnectReason reason))
	{
		sysInterlockedAdd(&m_Total.m_Received, bytes);
#if ENABLE_BANDWIDTH_TRACKING
		sysInterlockedAdd(&m_ByConnectReason[reason].m_Received, bytes);
#endif
	}

public:
	netTcpStat m_Total;

#if ENABLE_BANDWIDTH_TRACKING
	netTcpStat m_ByConnectReason[CR_COUNT];
#endif
};

static netTcpStats s_TcpStats;

#endif // ENABLE_BANDWIDTH_TELEMETRY

RAGE_DEFINE_SUBCHANNEL(ragenet, tcp)
#undef __rage_channel
#define __rage_channel ragenet_tcp

static int LastError()
{
#if RSG_WIN32
    return WSAGetLastError();
#elif RSG_SCE
    return errno;
#elif RSG_IOS
    return errno;
#elif RSG_ANDROID
    return errno;
#elif RSG_LINUX
	return errno;
#elif RSG_NX
	return errno;
#else
#error "Platform not implemented"
    return -1;
#endif
}

#if !__NO_OUTPUT
const char *netTcp::GetConnectReasonName(const ConnectReason connectReason)
{
    switch (connectReason) {
    case CR_INVALID: return "CR_INVALID";
    case CR_ELASTIC: return "CR_ELASTIC";
    case CR_CDN_UPLOAD_PHOTO: return "CR_CDN_UPLOAD_PHOTO";
    case CR_CDN_UPLOAD_VIDEO: return "CR_CDN_UPLOAD_VIDEO";
    case CR_ORBIS_MEDIA_DECODER: return "CR_ORBIS_MEDIA_DECODER";
    case CR_CLOUD: return "CR_CLOUD";
    case CR_TELEMETRY_UNUSED: return "CR_TELEMETRY_UNUSED";
    case CR_STATS_UNUSED: return "CR_STATS_UNUSED";
    case CR_ENTITLEMENT: return "CR_ENTITLEMENT";
    case CR_SC_CREATE_AUTH_TOKEN: return "CR_SC_CREATE_AUTH_TOKEN";
    case CR_SC_CREATE_LINK_TOKEN: return "CR_SC_CREATE_LINK_TOKEN";
    case CR_ROS_CREATE_TICKET: return "CR_ROS_CREATE_TICKET";
    case CR_ROS_CREDENTIALS_CHANGING: return "CR_ROS_CREDENTIALS_CHANGING";
    case CR_ROS_GET_GEOLOCATION_INFO: return "CR_ROS_GET_GEOLOCATION_INFO";
    case CR_ROS_GET_NAT_DISCOVERY_SERVERS: return "CR_ROS_GET_NAT_DISCOVERY_SERVERS";
    case CR_SOCIAL_CLUB: return "CR_SOCIAL_CLUB";
    case CR_UGC: return "CR_UGC";
    case CR_CLOUD_SAVE: return "CR_CLOUD_SAVE";
    case CR_ACHIEVEMENTS: return "CR_ACHIEVEMENTS";
    case CR_MATCHMAKING: return "CR_MATCHMAKING";
    case CR_FRIENDS: return "CR_FRIENDS";
    case CR_CREW: return "CR_CREW";
    case CR_CONDUCTOR_ROUTE: return "CR_CONDUCTOR_ROUTE";
    case CR_PRESENCE: return "CR_PRESENCE";
    case CR_INBOX: return "CR_INBOX";
    case CR_YOUTUBE: return "CR_YOUTUBE";
    case CR_SAVE_MIGRATION: return "CR_SAVE_MIGRATION";
    case CR_FEED: return "CR_FEED";
    case CR_REGISTER_PUSH_NOTIFICATION_DEVICE: return "CR_REGISTER_PUSH_NOTIFICATION_DEVICE";
    case CR_GAME_SERVER: return "CR_GAME_SERVER";
    case CR_ITEM_DATABASE: return "CR_ITEM_DATABASE";
    case CR_POSSE: return "CR_POSSE";
    case CR_TEST: return "CR_TEST";
    case CR_BUGSTAR: return "CR_BUGSTAR";
    case CR_GOOGLE_ANALYTICS: return "CR_GOOGLE_ANALYTICS";
    case CR_DAILY_OBJECTIVES: return "CR_DAILY_OBJECTIVES";
    case CR_SP_ACTION_PROXY: return "CR_SP_ACTION_PROXY";
    case CR_SESSION_SERVER: return "CR_SESSION_SERVER";
    case CR_CASH_INVENTORY_SERVER: return "CR_CASH_INVENTORY_SERVER";
    case CR_BOUNTY_SERVER: return "CR_BOUNTY_SERVER";
    case CR_ANTI_CHEAT_SERVER: return "CR_ANTI_CHEAT_SERVER";
    case CR_MINIGAME_SERVER: return "CR_MINIGAME_SERVER";
    case CR_MESSAGE_DISTRIBUTION_SERVER: return "CR_MESSAGE_DISTRIBUTION_SERVER";
    case CR_FACEBOOK: return "CR_FACEBOOK";        
    case CR_COUNT: return "CR_COUNT";
    }

    return "UNKNOWN";
}
#endif // !__NO_OUTPUT

//////////////////////////////////////////////////////////////////////////
//  netTcpAsyncOp
//////////////////////////////////////////////////////////////////////////

#if !__NO_OUTPUT
const char*
netTcpAsyncOp::GetOpTypeString(const OpType opType)
{
    switch(opType)
    {
    case OP_CONNECT: return "OP_CONNECT";
    case OP_SSL_CONNECT: return "OP_SSL_CONNECT";
    case OP_RECV_BUFFER: return "OP_RECV_BUFFER";
    case OP_SEND_BUFFER: return "OP_SEND_BUFFER";
    case OP_INVALID:
    case OP_NUM_TYPES:
        return "*** UNKNOWN OP TYPE ***";
    }

	return "*** UNKNOWN OP TYPE ***";
}
#endif  //!!__NO_OUTPUT

netTcpAsyncOp::netTcpAsyncOp()
: m_SktId(NET_TCP_SOCKET_ID_INVALID)	// make sure this is set to invalid, otherwise Clear() reads the
										// uninitialized m_SktId and closes an invalid socket descriptor.
{
    this->Clear();
}

netTcpAsyncOp::~netTcpAsyncOp()
{
    netAssert(!this->Pending());

    if(this->Pending())
    {
        //This will block until no longer pending.
        netTcp::CancelAsync(this);
    }
}

bool
netTcpAsyncOp::Pending() const
{
    return m_MyStatus.Pending();
}

bool
netTcpAsyncOp::Succeeded() const
{
    return m_MyStatus.Succeeded();
}

int
netTcpAsyncOp::GetResultCode() const
{
    return m_MyStatus.GetResultCode();
}

int
netTcpAsyncOp::GetLastError() const
{
	return m_LastError;
}

//private:

void
netTcpAsyncOp::Clear()
{
    netAssert(!this->Pending());

    if(this->Pending())
    {
        //This will block until no longer pending.
        netTcp::CancelAsync(this);
    }

    netAssert(NET_TCP_SOCKET_ID_INVALID == m_SktId);

    sysMemSet((char*)&m_Data, 0, sizeof(m_Data));

    m_Type = OP_INVALID;
    m_SktId = NET_TCP_SOCKET_ID_INVALID;
    m_TimeoutMs = 0;
    m_Id = 0;
	m_LastError = 0;
}

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s: " fmt, contextStr
#endif

bool
netTcpAsyncOp::ResetForConnect(const netSocketFd sktFd,
                                const netSocketAddress& addr,
                                ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                                int* callerSktId,
								const char* contextStr,
                                const unsigned timeoutSecs,
                                const ConnectReason connectReason)
{
	PROFILE

    netAssert(!this->Pending());

    this->Clear();

    if(netVerify(NET_TCP_SOCKET_ID_INVALID == m_SktId)
        && netVerify(NET_IS_VALID_SOCKET_FD(sktFd))
        && netVerify((m_SktId = netTcp::AllocSocketInfo(contextStr, connectReason)) >= 0))
    {
        netTcpSocketInfo* si = netTcp::GetSocketInfo(m_SktId);
        netAssert(!NET_IS_VALID_SOCKET_FD(si->m_SktFd));
        netAssert(NULL == si->m_Ssl);

        si->m_SktFd = sktFd;
        m_Data.m_Connect.m_Addr = (netSocketAddress*) m_Data.m_Connect.m_AddrBuf;
        *m_Data.m_Connect.m_Addr = addr;
        ENABLE_TCP_PROXY_ONLY(m_Data.m_Connect.m_ProxyAddr = (netSocketAddress*)m_Data.m_Connect.m_ProxyAddrBuf);
        ENABLE_TCP_PROXY_ONLY(*m_Data.m_Connect.m_ProxyAddr = proxyAddr);
        m_Data.m_Connect.m_CallerSktId = callerSktId;
        m_Data.m_Connect.m_ConnectReason = connectReason;
        m_Data.m_Connect.m_State = STATE_SOCKET_CONNECT;
        m_TimeoutMs = timeoutSecs * 1000;
        m_Type = OP_CONNECT;
        m_MyStatus.SetPending();
        netDebug2("Connecting to " NET_ADDR_FMT " on socket 0x%08x...",
                NET_ADDR_FOR_PRINTF(addr), m_SktId);

        return true;
    }

    return false;
}

bool
netTcpAsyncOp::SslResetForConnect(SSL_CTX* sslCtx,
                                    const netSocketFd sktFd,
                                    const netSocketAddress& addr,
									ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                                    const char* domainName,
                                    int* callerSktId,
									const char* contextStr,
                                    const unsigned timeoutSecs,
                                    const ConnectReason connectReason)
{
	PROFILE

    netAssert(!this->Pending());

    this->Clear();

    if(netVerify(NULL != sslCtx)
        && netVerify(NET_TCP_SOCKET_ID_INVALID == m_SktId)
        && netVerify(NET_IS_VALID_SOCKET_FD(sktFd))
        && netVerify((m_SktId = netTcp::AllocSocketInfo(contextStr, connectReason)) >= 0))
    {
        netTcpSocketInfo* si = netTcp::GetSocketInfo(m_SktId);
        netAssert(!NET_IS_VALID_SOCKET_FD(si->m_SktFd));
        netAssert(NULL == si->m_Ssl);

        si->m_SktFd = sktFd;
        m_Data.m_SslConnect.m_Addr = (netSocketAddress*) m_Data.m_SslConnect.m_AddrBuf;
        *m_Data.m_SslConnect.m_Addr = addr;
        ENABLE_TCP_PROXY_ONLY(m_Data.m_SslConnect.m_ProxyAddr = (netSocketAddress*) m_Data.m_SslConnect.m_ProxyAddrBuf);
        ENABLE_TCP_PROXY_ONLY(*m_Data.m_SslConnect.m_ProxyAddr = proxyAddr);
        m_Data.m_SslConnect.m_DomainName = domainName;
        m_Data.m_SslConnect.m_CallerSktId = callerSktId;
        m_Data.m_SslConnect.m_State = STATE_SOCKET_CONNECT;
		m_Data.m_SslConnect.m_NextSocketOp = SSL_SOCKET_OP_WRITE;
        m_Data.m_SslConnect.m_SslCtx = sslCtx;
        m_Data.m_SslConnect.m_ConnectReason = connectReason;
        m_TimeoutMs = timeoutSecs * 1000;
        m_Type = OP_SSL_CONNECT;
        m_MyStatus.SetPending();
        netDebug2("SSL: Connecting to " NET_ADDR_FMT " on socket 0x%08x...",
                NET_ADDR_FOR_PRINTF(addr), m_SktId);

        return true;
    }

    return false;
}

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s: " fmt, netTcp::GetSocketContextStr(m_SktId)
#endif

bool
netTcpAsyncOp::ResetForRecvBuffer(const int sktId,
                                void* buf,
                                const unsigned bufSize,
                                const unsigned timeoutSecs)
{
	PROFILE

    netAssert(!this->Pending());

    this->Clear();

    if(netVerify(NET_TCP_SOCKET_ID_INVALID == m_SktId)
        && netVerify(netTcp::IsValidSocketInfo(sktId)))
    {
        m_SktId = sktId;
        m_Data.m_RcvBuf.m_Buf = buf;
        m_Data.m_RcvBuf.m_SizeofBuf = bufSize;
        m_Data.m_RcvBuf.m_NumRcvd = 0;
        m_TimeoutMs = timeoutSecs * 1000;
        m_Type = OP_RECV_BUFFER;
        m_MyStatus.SetPending();
        return true;
    }

    return false;
}

bool
netTcpAsyncOp::ResetForSendBuffer(const int sktId,
                           const void* buf,
                           const unsigned bufSize,
                           const unsigned timeoutSecs)
{
	PROFILE

    netAssert(!this->Pending());

    this->Clear();

    if(netVerify(NET_TCP_SOCKET_ID_INVALID == m_SktId)
        && netVerify(netTcp::IsValidSocketInfo(sktId)))
    {
        m_SktId = sktId;
        m_Data.m_SndBuf.m_Buf = buf;
        m_Data.m_SndBuf.m_SizeofBuf = bufSize;
        m_Data.m_SndBuf.m_NumSent = 0;
        m_TimeoutMs = timeoutSecs * 1000;
        m_Type = OP_SEND_BUFFER;
        m_MyStatus.SetPending();
        return true;
    }

    return false;
}

void
netTcpAsyncOp::Complete(const netStatus::StatusCode statusCode,
                        netTcpResult result,
						int lastError)
{
	PROFILE

    netAssert(this->Pending());

    if(netTcpAsyncOp::OP_CONNECT == m_Type
        || netTcpAsyncOp::OP_SSL_CONNECT == m_Type)
    {
        if(netStatus::NET_STATUS_SUCCEEDED != statusCode)
        {
            netTcp::Close(m_SktId);
            m_SktId = NET_TCP_SOCKET_ID_INVALID;
        }

        if(netTcpAsyncOp::OP_CONNECT == m_Type)
        {
            *m_Data.m_Connect.m_CallerSktId = m_SktId;
        }
        else
        {
            *m_Data.m_SslConnect.m_CallerSktId = m_SktId;
        }
    }

    m_SktId = NET_TCP_SOCKET_ID_INVALID;
	m_LastError = lastError;
    m_MyStatus.SetStatus(statusCode, result);
}

//////////////////////////////////////////////////////////////////////////
//  wolfSSL debugging
//////////////////////////////////////////////////////////////////////////

#if __NO_OUTPUT
#if defined(DEBUG_WOLFSSL)
#error DEBUG_WOLFSSL should not be defined in non-output builds
#endif
#endif

// DEBUG_WOLFSSL is defined in user_settings.h when building the debug wolfSSL lib.
// user_settings.h is at the root of the wolfSSL source, and is included in all wolfSSL compilation units.
#if ((!__NO_OUTPUT) && (!RSG_RSC) && (!RSG_TOOL) && defined(DEBUG_WOLFSSL))
#define NET_WOLFSSL_DEBUG 1
#else
#define NET_WOLFSSL_DEBUG 0
#endif

#if NET_WOLFSSL_DEBUG
#	define NET_WOLFSSL_DEBUG_ONLY(...)    __VA_ARGS__
#else
#	define NET_WOLFSSL_DEBUG_ONLY(...)
#endif

struct TcpThreadLocalContextBytesWritten
{
    u32 m_BytesWritten;
    u32 m_BytesWrittenOnWire;
};

__THREAD TcpThreadLocalContextBytesWritten s_TcpThreadLocalContextBytesWritten;

#if NET_WOLFSSL_DEBUG

// wolfSSL doesn't give us any context in their logging callback (TcpSslLogCb()). Set a
// thread-local context string before relevant wolfSSL function calls and use this
// context in the RAGE_LOG_FMT to output the context string in the logging callback.
struct TcpThreadLocalContextStr
{
	char m_String[64];
};

__THREAD TcpThreadLocalContextStr s_TcpThreadLocalContextStr;

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s: " fmt, ((TcpThreadLocalContextStr&)s_TcpThreadLocalContextStr).m_String
#endif

#define NET_TCP_SET_WOLFSSL_CONTEXT_STR(str) netTcpSslLoggingAutoContext _auto_ssl_logging_ctx(str)
struct netTcpSslLoggingAutoContext
{
    netTcpSslLoggingAutoContext(const char* str)
    : m_OldLoggingCb(netWolfSslHelper::GetSslLoggingCb())
	{
		safecpy(m_OldContext.m_String, ((TcpThreadLocalContextStr&)s_TcpThreadLocalContextStr).m_String);
        safecpy(((TcpThreadLocalContextStr&)s_TcpThreadLocalContextStr).m_String, str);
        netWolfSslHelper::SetSslLoggingCb(netTcp::TcpSslLogCb);
    }

	~netTcpSslLoggingAutoContext()
	{
        netWolfSslHelper::SetSslLoggingCb(m_OldLoggingCb);
        safecpy(((TcpThreadLocalContextStr&)s_TcpThreadLocalContextStr).m_String, m_OldContext.m_String);
	}
private:
    netWolfSslHelper::netWolfSslLoggingCb m_OldLoggingCb;
    TcpThreadLocalContextStr m_OldContext;
};

static int wolfSSL_read_ctx(WOLFSSL* ssl, void* data, int sz, netTcpSocketInfo* socketInfo)
{
	// Prepare TLS data
	NET_TCP_SET_WOLFSSL_CONTEXT_STR(socketInfo->m_ContextStr);

	// Do the read
	const int res = wolfSSL_read(ssl, data, sz);

	return res;
}

#define wolfSSL_read(ssl, data, sz) wolfSSL_read_ctx(ssl, data, sz, &s_SocketInfos[sktId])

// Set to true if we want to dump SSL send/receive bytes to log
static bool s_DumpVerboseSsl = false;

//Dumps contents of receives to debug output.
static int MyReceiveCb(WOLFSSL* ssl, char *buf, int sz, void *ctx)
{
    const int result = EmbedReceive(ssl, buf, sz, ctx);

    if(s_DumpVerboseSsl && result > 0)
    {
		netDebug3("SSL Recv:");
        for(int i = 0; i < result;)
        {
            char str[256];
            char* p = str;
            const int last = i + 32;
            for(; i < last && i < result; ++i, p += 3)
            {
                formatf_sized(p, sizeof(str) - (p-str), "%02x,", (unsigned char)buf[i]);
            }
			netDebug3("   %s", str);
        }
    }

    return result;
}

void netTcp::TcpSslLogCb(const int logLevel, const char *const NOTFINAL_ONLY(logMessage))
{
	// disable spurious output. Code -323 is WANT_READ. Code -327 is WANT_WRITE.
	if((strstr(logMessage, "-323") == nullptr) &&
		(strstr(logMessage, "-327") == nullptr) &&
		(strstr(logMessage, "Embed Receive error") == nullptr) &&
		(strstr(logMessage, "Would block") == nullptr))
	{
		if(ERROR_LOG == logLevel)
		{
			//TODO_ANDI: this is a temporary fix to use netDebug1 till a spammy bugstar error is fixed
			netDebug1("wolfSSL: %s", logMessage);
		}
		else if(INFO_LOG == logLevel
				|| OTHER_LOG == logLevel)
		{
			netDebug3("wolfSSL: %s", logMessage);
		}
	}
}
#else
#define NET_TCP_SET_WOLFSSL_CONTEXT_STR(...)
#endif  //NET_WOLFSSL_DEBUG

static int wolfSSL_write_ctx(WOLFSSL* ssl, const void* data, int sz, netTcpSocketInfo* socketInfo)
{
    // Prepare TLS data
	NET_TCP_SET_WOLFSSL_CONTEXT_STR(socketInfo->m_ContextStr);
    s_TcpThreadLocalContextBytesWritten.m_BytesWritten = 0;
    s_TcpThreadLocalContextBytesWritten.m_BytesWrittenOnWire = 0;

    // Do the write
    const int res = wolfSSL_write(ssl, data, sz);

#if NET_WOLFSSL_DEBUG
    netDebug3("TCP SEND SSL %s:   sz: %d   result: %d   bytes written: %u",
        netTcp::GetConnectReasonName(socketInfo->m_ConnectReason),
        sz,
        res,
        s_TcpThreadLocalContextBytesWritten.m_BytesWritten);
#endif

    // Use and clear TLS data. Do not check res, it will be -1 for partial sends, but we still want
    // to capture however many bytes it *did* send. In error conditions, m_BytesWritten will be 0.
    socketInfo->m_BytesSent += s_TcpThreadLocalContextBytesWritten.m_BytesWritten;
    socketInfo->m_BytesSentOnWire += s_TcpThreadLocalContextBytesWritten.m_BytesWrittenOnWire;
    s_TcpStats.SentOnWire(s_TcpThreadLocalContextBytesWritten.m_BytesWritten BANDWIDTH_TRACKING_ONLY(, socketInfo->m_ConnectReason));
    s_TcpThreadLocalContextBytesWritten.m_BytesWritten = 0;
    s_TcpThreadLocalContextBytesWritten.m_BytesWrittenOnWire = 0;
    return res;
}

#define wolfSSL_write(ssl, data, sz) wolfSSL_write_ctx(ssl, data, sz, &s_SocketInfos[sktId])

//Dumps contents of sends to debug output.
static int MySendCb(WOLFSSL* ssl, char *buf, int sz, void *ctx)
{
    const int result = EmbedSend(ssl, buf, sz, ctx);

    if (result > 0)
    {
        s_TcpThreadLocalContextBytesWritten.m_BytesWritten += result;

        // Add 40 bytes for the TCP/IP header. It could be more than this due to fragmentation and
        // resends but we know it won't be less than this.
        s_TcpThreadLocalContextBytesWritten.m_BytesWrittenOnWire += result + 40;
    }

#if NET_WOLFSSL_DEBUG
    if (s_DumpVerboseSsl && result > 0)
    {
        netDebug3("SSL Send:");
        for (int i = 0; i < result;)
        {
            char str[256];
            char* p = str;
            const int last = i + 32;
            for (; i < last && i < result; ++i, p += 3)
            {
                formatf_sized(p, sizeof(str) - (p - str), "%02x,", (unsigned char)buf[i]);
            }
            netDebug3("   %s", str);
        }
    }
#endif

    return result;
}

//////////////////////////////////////////////////////////////////////////
//  netTcp
//////////////////////////////////////////////////////////////////////////

//Our main crit section
static sysCriticalSectionToken s_CsMain;
static sysCriticalSectionToken s_CsPendingOpsList;

netTcp::AsyncOpList netTcp::sm_OpList;
netTcp::AsyncOpList netTcp::sm_PendingOpList;
bool netTcp::sm_Initialized = false;

static volatile unsigned s_OpId;
static sysIpcEvent s_WakeupEvent = sysIpcCreateEvent();
static SSL_CTX* s_SecureSslCtx(nullptr);
static SSL_CTX* s_InsecureSslCtx(nullptr);
#if !RSG_FINAL
static SSL_CTX* s_DebugSslCtx(nullptr);
#endif

bool
netTcp::Init()
{
#if RSG_LINUX
	IgnoreSigPipe();
#endif

#if NET_WOLFSSL_DEBUG
    netWolfSslHelper::Init();
#endif

    BANK_ONLY(AddWidgets());
    StartWorker(-1);

	sm_Initialized = true;

	return true;
}

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s: " fmt, contextStr
#endif

void
netTcp::Shutdown()
{
	{
		SYS_CS_SYNC(s_CsMain);
		rage::sysCriticalSection csPendingOpsList(s_CsPendingOpsList);

		// cancel any pending ops
		AsyncOpList::iterator it = sm_OpList.begin();
		AsyncOpList::iterator next = it;
		AsyncOpList::const_iterator stop = sm_OpList.end();

		for(++next; stop != it; it = next, ++next)
		{
			netTcpAsyncOp* op = *it;

			OUTPUT_ONLY(const char* contextStr = netTcp::GetSocketContextStr(op->m_SktId));
			netWarning("netTcp::Shutdown :: cancelling an op still in the op list");
			CancelAsync(op);
		}

		it = sm_PendingOpList.begin();
		next = it;
		stop = sm_PendingOpList.end();

		for(++next; stop != it; it = next, ++next)
		{
			netTcpAsyncOp* op = *it;
			OUTPUT_ONLY(const char* contextStr = netTcp::GetSocketContextStr(op->m_SktId));
			netWarning("netTcp::Shutdown :: cancelling an op still in the pending op list");
			CancelAsync(op);
		}

		SSL_CTX* sslCtx = s_SecureSslCtx;
		if(sslCtx != nullptr)
		{
			wolfSSL_CTX_free(sslCtx);
		}

		sslCtx = s_InsecureSslCtx;
		if(sslCtx != nullptr)
		{
			wolfSSL_CTX_free(sslCtx);
		}

#if !RSG_FINAL
		sslCtx = s_DebugSslCtx;
		if(sslCtx != nullptr)
		{
			sslCtx = s_DebugSslCtx;
			wolfSSL_CTX_free(sslCtx);
		}
#endif
		
		sm_Initialized = false;
	}

	// outside of the critical section, since ShutdownWorker waits for the worker thread to exit, 
	// and the worker thread needs the critical section lock to exit
	ShutdownWorker();
}

bool
netTcp::ConnectAsync(const netSocketAddress& addr,
                    ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                    int* sktId,
					const char* contextStr,
                    const unsigned timeoutSecs,
                    const ConnectReason connectReason,
                    netTcpAsyncOp* op)
{
	PROFILE

    bool success = false;
    netSocketFd tmpSktFd = NET_INVALID_SOCKET_FD;
    *sktId = NET_TCP_SOCKET_ID_INVALID;

    rtry
    {
		rcheck(sm_Initialized, catchall, netError("ConnectAsync :: not initialized"));

        netDebug2("Connecting to " NET_ADDR_FMT ", timeout: %u s...",
                NET_ADDR_FOR_PRINTF(addr), timeoutSecs);

        netSocketAddress connectAddr = ENABLE_TCP_PROXY_ONLY(proxyAddr.IsValid() ? proxyAddr :) addr;
        bool ok = netTcp::ConnectFd(connectAddr, &tmpSktFd, contextStr);

        rcheck(ok,
                catchall,
                netWarning("Failed to connect to:" NET_ADDR_FMT "",
                            NET_ADDR_FOR_PRINTF(addr)));

        rverify(op->ResetForConnect(tmpSktFd, addr, ENABLE_TCP_PROXY_ONLY(proxyAddr,) sktId, contextStr, timeoutSecs, connectReason),
                catchall,
                netError("Error initializing async op for connect operation"));

        rverify(netTcp::QueueOp(op),
                catchall,
                netError("Failed to queue async op"));

        netDebug2("Connecting to " NET_ADDR_FMT " at FD 0x%08x on 0x%08x...", NET_ADDR_FOR_PRINTF(addr), (int)tmpSktFd, *sktId);

        success = true;
    }
    rcatchall
    {
        //Always set the status to "FAILED"
        if(op->Pending())
        {
			op->Complete(netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR, LastError());
        }
        else
        {
            op->m_MyStatus.ForceFailed(NETTCP_RESULT_ERROR);
        }

        netAssert(NET_TCP_SOCKET_ID_INVALID == *sktId);
    }

    return success;
}

#define CHECK_TIMED_OUT(str) \
{ \
    s64 curTime = sysTimer::GetSystemMsTime(); \
    timeLeft -= int(curTime - lastTime); \
    lastTime = curTime; \
    rverify(timeLeft > 0, catchall, netError(str NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr))); \
    t.tv_sec = static_cast<long>(timeLeft / 1000); \
    t.tv_usec = static_cast<long>((timeLeft % 1000) * 1000); \
}

bool
netTcp::Connect(const netSocketAddress& addr,
                ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr, )
                int* sktId,
                const char* contextStr,
                const unsigned timeoutSecs,
                const ConnectReason connectReason)
{
    PROFILE

    netSocketFd tmpSktFd = NET_INVALID_SOCKET_FD;
    netTcpSocketInfo* si = nullptr;

    *sktId = NET_TCP_SOCKET_ID_INVALID;

    s64 timeLeft = timeoutSecs * 1000;
    s64 lastTime = sysTimer::GetSystemMsTime();

    netDebug2("Connecting synchronously to " NET_ADDR_FMT ", timeout: %u s...",
        NET_ADDR_FOR_PRINTF(addr), timeoutSecs);

    rtry
    {
        timeval t;
        t.tv_sec = static_cast<long>(timeLeft / 1000);
        t.tv_usec = static_cast<long>((timeLeft % 1000) * 1000);

        netSocketAddress connectAddr = ENABLE_TCP_PROXY_ONLY(proxyAddr.IsValid() ? proxyAddr : ) addr;
        bool ok = netTcp::ConnectFd(connectAddr, &tmpSktFd, contextStr);

        rcheck(ok,
            catchall,
            netWarning("Failed to connect to:" NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(addr)));

        netSocketFd sktFd = tmpSktFd;

        *sktId = netTcp::AllocSocketInfo(contextStr, connectReason);
        rverify(*sktId >= 0, catchall, );

        si = netTcp::GetSocketInfo(*sktId);
        rverify(si, catchall, );

        si->m_SktFd = tmpSktFd;

        fd_set fdsr, fdsw, fdse;

        FD_ZERO(&fdsr);
        FD_ZERO(&fdsw);
        FD_ZERO(&fdse);

        FD_SET(sktFd, &fdse);
        FD_SET(sktFd, &fdsw);

        // Wait for the write to be ready. Also required in non-proxy mode.
        int numSkts = netSocket::Select(int(sktFd + 1), &fdsr, &fdsw, &fdse, &t);
        rverify(numSkts >= 0, catchall, );

        CHECK_TIMED_OUT("Timed out [select - wait for fdsw]");

        rverify(!FD_ISSET(sktFd, &fdse), catchall, );

        rverify(FD_ISSET(sktFd, &fdsw), catchall, netError("Not ready to write " NET_ADDR_FMT,
            NET_ADDR_FOR_PRINTF(addr)));

#if ENABLE_TCP_PROXY
        if (proxyAddr.IsValid())
        {
            rcheck(SetupProxyRoute(addr, nullptr, sktId, contextStr, sktFd, timeLeft), catchall,
                netError("Failed to connect to proxy" NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr)));
        }
#endif //ENABLE_TCP_PROXY

        return true;
    }
    rcatchall
    {
        if (*sktId != NET_TCP_SOCKET_ID_INVALID)
        {
            netTcp::FreeSocketInfo(*sktId);
        }

        if (NET_IS_VALID_SOCKET_FD(tmpSktFd))
        {
            netTcp::CloseFd(tmpSktFd);
        }
    }

    return false;
}

bool
netTcp::SslConnectCommon(SSL_CTX *sslCtx,
                        const netSocketAddress& addr,
                        ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr, )
                        const char* /*domainName*/,
                        int* sktId,
                        const char* contextStr,
                        const unsigned OUTPUT_ONLY(timeoutSecs),
                        netSocketFd& sktFd)
{
    *sktId = NET_TCP_SOCKET_ID_INVALID;
	sktFd = NET_INVALID_SOCKET_FD;

    rtry
    {
		rcheck(sm_Initialized, catchall, netError("SslConnectAsync :: not initialized"));

        rverify(sslCtx != NULL,
                catchall,
                netError("Invalid ssl ctx"));
#if NET_WOLFSSL_DEBUG
        const char* sslDumpOptions = NULL;
        if (PARAM_netssldump.Get(sslDumpOptions))
        {
            if (stricmp(sslDumpOptions, "verbose") == 0)
            {
				s_DumpVerboseSsl = true;
            }
        }

        wolfSSL_SetIORecv(sslCtx, MyReceiveCb);
#endif  // NET_WOLFSSL_DEBUG

        wolfSSL_SetIOSend(sslCtx, MySendCb);

        netDebug2("SSL: Connecting to " NET_ADDR_FMT ", timeout: %u s...",
                NET_ADDR_FOR_PRINTF(addr), timeoutSecs);

        netSocketAddress connectAddr = ENABLE_TCP_PROXY_ONLY(proxyAddr.IsValid() ? proxyAddr : ) addr;
        bool ok = netTcp::ConnectFd(connectAddr, &sktFd, contextStr);

        rcheck(ok, catchall,
                netWarning("Failed to connect to:" NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(addr)));

        return true;
    }
    rcatchall
    {
    }

    return false;
}

bool
netTcp::SslConnectAsync(SSL_CTX *sslCtx,
                        const netSocketAddress& addr,
                        ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                        const char* domainName,
                        int* sktId,
                        const char* contextStr,
                        const unsigned timeoutSecs,
                        const ConnectReason connectReason,
                        netTcpAsyncOp* op)
{
	PROFILE

    bool success = false;

	netSocketFd tmpSktFd = NET_INVALID_SOCKET_FD;

    rtry
    {
        rverify(SslConnectCommon(sslCtx, addr, ENABLE_TCP_PROXY_ONLY(proxyAddr,) domainName, sktId, contextStr, timeoutSecs, tmpSktFd), catchall, );

        rverify(op->SslResetForConnect(sslCtx, tmpSktFd, addr, ENABLE_TCP_PROXY_ONLY(proxyAddr,) domainName, sktId, contextStr, timeoutSecs, connectReason),
                catchall,
                netError("Error initializing async op for SSL connect operation"));

        rverify(netTcp::QueueOp(op),
                catchall,
                netError("Failed to queue async op"));
        
        netDebug2("Connecting (SSL) to " NET_ADDR_FMT " at FD 0x%08x on 0x%08x", NET_ADDR_FOR_PRINTF(addr), (int)tmpSktFd, *sktId);

        success = true;
    }
    rcatchall
    {
        //Always set the status to "FAILED"
        if(op->Pending())
        {
			op->Complete(netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR, LastError());
        }
        else
        {
            op->m_MyStatus.ForceFailed(NETTCP_RESULT_ERROR);
        }

        netAssert(NET_TCP_SOCKET_ID_INVALID == *sktId);
    }

    return success;
}

bool
netTcp::SslConnect(SSL_CTX *sslCtx,
                        const netSocketAddress& addr,
                        ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                        const char* domainName,
                        int* sktId,
                        const char* contextStr,
                        const unsigned timeoutSecs,
                        ConnectReason connectReason)
{
    PROFILE

    netSocketFd tmpSktFd = NET_INVALID_SOCKET_FD;
    netTcpSocketInfo* si = nullptr;
    int wolfResult = SSL_SUCCESS;

    s64 timeLeft = timeoutSecs * 1000;
    s64 lastTime = sysTimer::GetSystemMsTime();

    rtry
    {
        timeval t;
        t.tv_sec = static_cast<long>(timeLeft / 1000);
        t.tv_usec = static_cast<long>((timeLeft % 1000) * 1000);

        rverify(SslConnectCommon(sslCtx, addr, ENABLE_TCP_PROXY_ONLY(proxyAddr,) domainName, sktId, contextStr, timeoutSecs, tmpSktFd), catchall, );
        netSocketFd sktFd = tmpSktFd;

        *sktId = netTcp::AllocSocketInfo(contextStr, connectReason);
        rverify(*sktId >= 0, catchall, );

        si = netTcp::GetSocketInfo(*sktId);
        rverify(si, catchall, );

        si->m_SktFd = tmpSktFd;

        fd_set fdsr, fdsw, fdse;

        FD_ZERO(&fdsr);
        FD_ZERO(&fdsw);
        FD_ZERO(&fdse);

        FD_SET(sktFd, &fdse);
        FD_SET(sktFd, &fdsw);

        // Wait for the write to be ready. Also required in non-proxy mode.
        int numSkts = netSocket::Select(int(sktFd + 1), &fdsr, &fdsw, &fdse, &t);
        rverify(numSkts >= 0, catchall, );

        CHECK_TIMED_OUT("Timed out [select - wait for fdsw]");

        rverify(!FD_ISSET(sktFd, &fdse), catchall, );

        rverify(FD_ISSET(sktFd, &fdsw), catchall, netError("Not ready to write " NET_ADDR_FMT,
            NET_ADDR_FOR_PRINTF(addr)));

#if ENABLE_TCP_PROXY
        if (proxyAddr.IsValid())
        {
            rcheck(SetupProxyRoute(addr, domainName, sktId, contextStr, sktFd, timeLeft), catchall,
                netError("Failed to connect to proxy" NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr)));
        }
#endif //ENABLE_TCP_PROXY

        NET_TCP_SET_WOLFSSL_CONTEXT_STR(contextStr);

        si->m_Ssl = wolfSSL_new(sslCtx);
        rverify(si->m_Ssl != nullptr, catchall, );

		// wolfSSL always requires socket fd to be int
        rverify((wolfResult = wolfSSL_set_fd(si->m_Ssl, int(sktFd))) == SSL_SUCCESS, catchall, );

        if (domainName)
        {
            rverify(wolfSSL_check_domain_name(si->m_Ssl, domainName) == SSL_SUCCESS, catchall, netError("wolfSSL_check_domain_name failed for '%s'", domainName));
        }

        int lastError = SSL_ERROR_WANT_READ;

        while (lastError == SSL_ERROR_WANT_READ || lastError == SSL_ERROR_WANT_WRITE)
        {
            wolfResult = wolfSSL_connect(si->m_Ssl);

            if (wolfResult != SSL_SUCCESS)
            {
                lastError = wolfSSL_get_error(si->m_Ssl, wolfResult);
            }
            else
            {
                lastError = 0;
            }

            CHECK_TIMED_OUT("Timed out [wolfSSL_connect]");
        }

        rverify(wolfResult == SSL_SUCCESS, catchall, );

        return true;
    }
    rcatchall
    {
        if (wolfResult != SSL_SUCCESS)
        {
            OUTPUT_ONLY(int lastError = wolfSSL_get_error(si->m_Ssl, wolfResult));
            OUTPUT_ONLY(char buf[128];)

            netError("Error creating SSL connection: %s (%d)", wolfSSL_ERR_error_string(lastError, buf), lastError);
        }

        if (*sktId != NET_TCP_SOCKET_ID_INVALID)
        {
            netTcp::FreeSocketInfo(*sktId);
        }

        if (NET_IS_VALID_SOCKET_FD(tmpSktFd))
        {
            netTcp::CloseFd(tmpSktFd);
        }
    }

    return false;
}

#if ENABLE_TCP_PROXY
bool
netTcp::SetupProxyRoute(const netSocketAddress& addr,
                        const char* domainName,
                        int* sktId,
                        const char* OUTPUT_ONLY(contextStr),
						netSocketFd sktFd,
                        s64 timeoutMs)
{
    s64 timeLeft = timeoutMs;
    s64 lastTime = sysTimer::GetSystemMsTime();

    timeval t;
    t.tv_sec = static_cast<long>(timeLeft / 1000);
    t.tv_usec = static_cast<long>((timeLeft % 1000) * 1000);

    rtry
    {
		rverifyall(NET_IS_VALID_SOCKET_FD(sktFd));
        rverify(addr.IsValid(), catchall, );

        char buf[512];
        FormatProxyHeader(buf, addr, domainName);

        netTcpResult tcpResult = netTcp::SendBuffer(*sktId, buf, ustrlen(buf), 10);

        rverify(tcpResult == NETTCP_RESULT_OK, catchall, 
            netError("Error sending CONNECT to " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr)));

        CHECK_TIMED_OUT("Timed out [write proxy]");

        fd_set fdsr, fdsw, fdse;

        FD_ZERO(&fdsr);
        FD_ZERO(&fdsw);
        FD_ZERO(&fdse);
        FD_SET(sktFd, &fdse);
        FD_SET(sktFd, &fdsr);

        int numSkts = netSocket::Select(int(sktFd + 1), &fdsr, &fdsw, &fdse, &t);

        CHECK_TIMED_OUT("Timed out [select - read proxy]");

        rverify(numSkts >= 0, catchall, );
        rverify(FD_ISSET(sktFd, &fdsr), catchall, netError("Nothing read " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr)));

        unsigned bytesReceived = 0;
        tcpResult = netTcp::Receive(*sktId, buf, sizeof(buf), &bytesReceived);

        rverify(tcpResult == NETTCP_RESULT_OK, catchall, netError("Error receiving on socket 0x%08x", static_cast<unsigned>(sktFd)));

        rverify((strstr(buf, "HTTP/1.1 200") != NULL) && (strstr(buf, "\r\n\r\n") != NULL), catchall, );

        CHECK_TIMED_OUT("Timed out [read proxy]");

        return true;
    }
    rcatchall
    {

    }

    return false;
}
#endif

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s: " fmt, netTcp::GetSocketContextStr(sktId)
#endif

void
netTcp::Close(const int sktId)
{
    netDebug2("Closing socket 0x%08x...", sktId);

    if(sktId >= 0)
    {
        FreeSocketInfo(sktId);
    }
}

netTcpResult
netTcp::Send(const int sktId,
            const void* buf,
            const unsigned bufSize,
			unsigned* bytesSent,
			int* lastError)
{
	PROFILE 

    netTcpResult result = NETTCP_RESULT_ERROR;
	int error = 0;

    rtry
    {
		rcheck(sm_Initialized, catchall, netError("Send :: not initialized"));

        *bytesSent = 0;

        const netSocketFd sktFd = GetSocketFd(sktId);

        int numSent;

        netTcpSocketInfo* si = GetSocketInfo(sktId);
        rverify(si,catchall,);

#if FAKE_DROP_LATENCY
        if (netSocket::GetFakeUplinkDisconnection() && !si->m_IgnoreUplinkDisconnect)
        {
            // Unfortunately we might miss any actual socket error this way but close enough.
            numSent = 0;
            result = NETTCP_RESULT_OK;
        }
        else
#endif //FAKE_DROP_LATENCY
        if(si->m_Ssl)
        {
			PROFILER_EVENT("wolfSSL_write")
			sysCriticalSection __cssync(si->m_CsSslReadWrite, false);
			rcheck(__cssync.TryEnter(), WouldBlock, );

            numSent = wolfSSL_write(si->m_Ssl, buf, bufSize);

			error = WOLFSSL_ERROR_NONE;
			OUTPUT_ONLY(char szErr[WOLFSSL_MAX_ERROR_SZ]);

			if(numSent <= 0)
			{
				error = wolfSSL_get_error(si->m_Ssl, numSent);
				
				//SSL_ERROR_WANT_WRITE is caused by EWOULDBLOCK.
				if(SSL_ERROR_WANT_WRITE == error)
				{
					numSent = 0;
					error = WOLFSSL_ERROR_NONE;
				}
			}

            rcheck((numSent >= 0) && (error == WOLFSSL_ERROR_NONE),
                catchall,
                netError("netTcp::Send: SSL_write() returned %d (wolfSSL_get_error: %d - %s, LastError: %d) on 0x%08x", numSent, error, wolfSSL_ERR_error_string(error, szErr), LastError(), sktId));

            s_TcpStats.Sent(numSent BANDWIDTH_TRACKING_ONLY(, si->m_ConnectReason));
            // Do not update s_TotalBytesSentOnWire, it is handled in the SSL callback. This way it captures TLS overhead and
            // overhead for negotiating the connection
        }
        else
        {
#if RSG_NP
			numSent = sceNetSend(sktFd, (const char*)buf, bufSize, 0);
#else
			numSent = send(sktFd, (const char*)buf, bufSize, 0);
#endif

            if(numSent < 0)
            {
                error = LastError();
                if(NET_EINPROGRESS == error || NET_EWOULDBLOCK == error || NET_EINTR == error)
                {
                    numSent = 0;
                }
            }

            rcheck(numSent >= 0,
                catchall,
				netError("netTcp::Send: send() error (%d) on 0x%08x", LastError(), sktId));

            s_TcpStats.Sent(numSent BANDWIDTH_TRACKING_ONLY(, si->m_ConnectReason));
            s_TcpStats.SentOnWire(numSent BANDWIDTH_TRACKING_ONLY(, si->m_ConnectReason));
        }

        *bytesSent = numSent;

        result = NETTCP_RESULT_OK;
    }
	rcatch(WouldBlock)
	{
		*bytesSent = 0;
		result = NETTCP_RESULT_OK;
	}
    rcatchall
    {
    }

	if (lastError)
	{
		(*lastError) = error;
	}

    return result;
}

netTcpResult
netTcp::Receive(const int sktId,
                void* buf, 
                const unsigned bufSize,
				unsigned* bytesReceived,
				int* lastError)
{
	PROFILE

    netTcpResult result = NETTCP_RESULT_ERROR;
	int error = 0;

    rtry
    {
		rcheck(sm_Initialized, catchall, netError("Receive :: not initialized"));

        *bytesReceived = 0;

        netTcpSocketInfo* si = GetSocketInfo(sktId);

        rverify(si,catchall,);

#if FAKE_DROP_LATENCY
        if (netSocket::GetFakeUplinkDisconnection() && !si->m_IgnoreUplinkDisconnect)
        {
            // Unfortunately we might miss any actual socket error this way but close enough.
            result = NETTCP_RESULT_OK;
        }
        else
#endif //FAKE_DROP_LATENCY
        if(si->m_Ssl)
        {
			rcheck(!si->m_Disconnected, ClosedSkt, netDebug("netTcp::Receive: Connection closed on 0x%08x. Disconnected flag is set.", sktId));

			PROFILER_EVENT("wolfSSL_read")

			// SSL_read() and SSL_write() need to be mutually exclusive.
			// Using TryEnter() to avoid blocking the main thread if an SSL_write() is occuring on
			// the same socket on the TCP worker thread at the same time. This behaves the same as 
			// a Would Block on the socket. We try to acquire the lock again on the next call.
			sysCriticalSection __cssync(si->m_CsSslReadWrite, false);
			rcheck(__cssync.TryEnter(), WouldBlock, );

            int numRcvd = wolfSSL_read(si->m_Ssl, buf, bufSize);
			error = WOLFSSL_ERROR_NONE;
			OUTPUT_ONLY(char szErr[WOLFSSL_MAX_ERROR_SZ]);

			if(numRcvd <= 0)
			{
				error = wolfSSL_get_error(si->m_Ssl, numRcvd);

				//Connection closed by remote?
				rcheck(0 != numRcvd,
					ClosedSkt,
					netWarning("netTcp::Receive: Connection closed on 0x%08x. SSL_read() returned 0 (wolfSSL_get_error: %d - %s, LastError: %d)", sktId, error, wolfSSL_ERR_error_string(error, szErr), LastError()));

				//SSL_ERROR_WANT_READ is caused by EWOULDBLOCK.
				if(SSL_ERROR_WANT_READ == error)
				{
					numRcvd = 0;
				}
			}

            // There may be issues if we try to read a chunk which is too small so we cap it at 1400 bytes.
            static const int MIN_AVAILABLE_SPACE = 1400;

            const int readLoops = s_TcpSettings.m_MaxReadLoops;
            int sumRcvd = (numRcvd > 0) ? numRcvd : 0;
            int count = 0;

            //  wolfSSL reads one TLS record at a time instead of filling up the user's buffer with multiple data records.
            //  OUTPUT_RECORD_SIZE is 16kb, which is a limit imposed by the TLS specification and that's the upper limit of a record.
            //  In most cases a single record is much smaller though.
            //  In short: Looping through SSL_read a few times speeds up download times without hitting performance too much.
            while((numRcvd > 0) && ((bufSize - sumRcvd) >= MIN_AVAILABLE_SPACE) && (count < readLoops))
            {
                numRcvd = wolfSSL_read(si->m_Ssl, (reinterpret_cast<u8*>(buf) + sumRcvd), bufSize - static_cast<unsigned>(sumRcvd));

				if(numRcvd > 0)
				{
					sumRcvd += numRcvd;
					++count;
				}
				else
				{
					error = wolfSSL_get_error(si->m_Ssl, numRcvd);

					//Connection closed by remote?
					if(numRcvd == 0)
					{
						// If this SSL_read returns 0 we won't return a NETTCP_RESULT_DISCONNECTED as that would mean losing data.
						// Set the disconnected flag. The next call to netTcp::Receive() will return NETTCP_RESULT_DISCONNECTED.
						si->m_Disconnected = true;
						netWarning("netTcp::Receive: Connection closed on 0x%08x (in loop). SSL_read() returned 0 (wolfSSL_get_error: %d - %s, LastError: %d)", sktId, error, wolfSSL_ERR_error_string(error, szErr), LastError());
					}
					//SSL_ERROR_WANT_READ is caused by EWOULDBLOCK.
					else if(SSL_ERROR_WANT_READ == error)
					{
						numRcvd = 0;
					}
				}
            }

			rcheck(numRcvd >= 0,
				catchall,
				netError("netTcp::Receive: SSL_read() error (wolfSSL_get_error: %d - %s, LastError: %d) on 0x%08x", error, wolfSSL_ERR_error_string(error, szErr), LastError(), sktId));

            *bytesReceived = sumRcvd;
            s_TcpStats.Received(sumRcvd BANDWIDTH_TRACKING_ONLY(, si->m_ConnectReason));

            result = NETTCP_RESULT_OK;
        }
        else
        {
            const netSocketFd sktFd = GetSocketFd(sktId);

			int numRcvd = recv(sktFd, (char*)buf, bufSize, 0);

            //Connection closed by remote?
            rcheck(0 != numRcvd,
                ClosedSkt,
                netDebug2("netTcp::Receive: Connection closed"));

            if(numRcvd < 0)
            {
                error = LastError();
                if(NET_EINPROGRESS == error || NET_EWOULDBLOCK == error || NET_EINTR == error)
                {
                    numRcvd = 0;
                }
            }

			rcheck(numRcvd >= 0,
				catchall,
				netError("netTcp::Receive: recv() error (%d) on 0x%08x", LastError(), sktId));

            *bytesReceived = numRcvd;
            s_TcpStats.Received(numRcvd BANDWIDTH_TRACKING_ONLY(, si->m_ConnectReason));

            result = NETTCP_RESULT_OK;
        }
    }
	rcatch(WouldBlock)
	{
		*bytesReceived = 0;
		result = NETTCP_RESULT_OK;
	}
    rcatch(ClosedSkt)
    {
        result = NETTCP_RESULT_DISCONNECTED;
    }
    rcatchall
    {
    }

	if (lastError)
	{
		(*lastError) = error;
	}

    return result;
}

netTcpResult
netTcp::SendBuffer(const int sktId,
                   const void* buf, 
                   const unsigned bufSize,
                   const unsigned timeoutSecs)
{
	PROFILE

    netAssert((sktId >= 0) && buf && bufSize);

    netTcpResult result = NETTCP_RESULT_ERROR;

    rtry
    {
		rcheck(sm_Initialized, catchall, netError("SendBuffer :: not initialized"));

        unsigned total = 0;
        int timeLeft = (int) timeoutSecs * 1000;
        unsigned lastTime = sysTimer::GetSystemMsTime();

        while(total < bufSize) 
        {
            unsigned numBytes;
            const unsigned waitSecs = unsigned((timeLeft + 999) / 1000);

            const netSocketFd sktFd = GetSocketFd(sktId);
			rcheck(NET_IS_VALID_SOCKET_FD(sktFd), catchall, );

            //Determine if we can send.
            bool readySkt, errSkt;
            rcheck(netTcp::WaitForSocketFds(&sktFd,
                                            &readySkt,
                                            &errSkt,
                                            1,
                                            NETTCP_WAIT_WRITE,
                                            waitSecs),
                    catchall,
                    netError("Error calling WaitForSocketFds"));

            //If errSkt is true then send() should generate the error code.
            if(readySkt || errSkt)
            {
                result = netTcp::Send(sktId,
                                        &((const u8*) buf)[total],
                                        bufSize - total,
                                        &numBytes);

                rcheck(NETTCP_RESULT_OK == result, catchall,);

                total += numBytes;
                netAssert(total <= bufSize);
            }

            const unsigned curTime = sysTimer::GetSystemMsTime();
            timeLeft -= int(curTime - lastTime);

            rcheck(timeLeft > 0, TimedOutSkt,);

            lastTime = curTime;
        }

        result = NETTCP_RESULT_OK;
    }
    rcatch(TimedOutSkt)
    {
        result = NETTCP_RESULT_TIMED_OUT;
    }
    rcatchall
    {
    }

    return result;
}

netTcpResult
netTcp::ReceiveBuffer(const int sktId,
                      void* buf, 
                      const unsigned bufSize,
                      const unsigned timeoutSecs)
{
	PROFILE

    netAssert((sktId >= 0) && buf && bufSize);

    netTcpResult result = NETTCP_RESULT_ERROR;

    rtry
    {
		rcheck(sm_Initialized, catchall, netError("ReceiveBuffer :: not initialized"));

        unsigned total = 0;
        int timeLeft = (int) timeoutSecs * 1000;
        unsigned lastTime = sysTimer::GetSystemMsTime();

        while(total < bufSize) 
        {
            unsigned numBytes;
            const unsigned waitSecs = unsigned((timeLeft + 999) / 1000);

            const netSocketFd sktFd = GetSocketFd(sktId);
			rcheck(NET_IS_VALID_SOCKET_FD(sktFd), catchall, );

            //Determine if we can receive.
            bool readySkt, errSkt;
            rcheck(netTcp::WaitForSocketFds(&sktFd,
                                            &readySkt,
                                            &errSkt,
                                            1,
                                            NETTCP_WAIT_READ,
                                            waitSecs),
                    catchall,
                    netError("Error calling WaitForSocketFds"));

            //If errSkt is true then recv() should generate the error code.
            if(readySkt || errSkt)
            {
                result = netTcp::Receive(sktId,
                                        &((u8*) buf)[total],
                                        bufSize - total,
                                        &numBytes);

                rcheck(NETTCP_RESULT_OK == result, catchall,);

                total += numBytes;
                netAssert(total <= bufSize);
            }

            const unsigned curTime = sysTimer::GetSystemMsTime();
            timeLeft -= int(curTime - lastTime);

            rcheck(timeLeft >= 0, TimedOutSkt,);

            lastTime = curTime;
        }

        result = NETTCP_RESULT_OK;
    }
    rcatch(TimedOutSkt)
    {
        result = NETTCP_RESULT_TIMED_OUT;
    }
    rcatchall
    {
    }

    return result;
}

bool
netTcp::SendBufferAsync(const int sktId,
                       const void* buf,
                       const unsigned bufSize,
                       const unsigned timeoutSecs,
                       netTcpAsyncOp* op)
{
	PROFILE

    bool success = false;

    rtry
    {
		rcheck(sm_Initialized, catchall, netError("SendBufferAsync :: not initialized"));

        netDebug3("Sending %d bytes on socket 0x%08x, timeout: %u s...",
                bufSize, sktId, timeoutSecs);

        rverify(op->ResetForSendBuffer(sktId, buf, bufSize, timeoutSecs),
                catchall,
                netError("Error initializing async send operation"));

        rverify(netTcp::QueueOp(op),
                catchall,
                netError("Failed to queue async op"));

        success = true;
    }
    rcatchall
    {
        //Always set the status to "FAILED"
        if(op->Pending())
        {
			op->Complete(netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR, LastError());
        }
        else
        {
            op->m_MyStatus.ForceFailed(NETTCP_RESULT_ERROR);
        }
    }

    return success;
}

bool
netTcp::ReceiveBufferAsync(const int sktId,
                       void* buf,
                       const unsigned bufSize,
                       const unsigned timeoutSecs,
                       netTcpAsyncOp* op)
{
	PROFILE

    bool success = false;

    rtry
    {
		rcheck(sm_Initialized, catchall, netError("ReceiveBufferAsync :: not initialized"));

        netDebug3("Receiving expected %d bytes on socket 0x%08x, timeout: %u s...",
                bufSize, sktId, timeoutSecs);

        rverify(op->ResetForRecvBuffer(sktId, buf, bufSize, timeoutSecs),
                catchall,
                netError("Error initializing async receive operation"));

        rverify(netTcp::QueueOp(op),
                catchall,
                netError("Failed to queue async op"));

        success = true;
    }
    rcatchall
    {
        //Always set the status to "FAILED"
        if(op->Pending())
        {
			op->Complete(netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR, LastError());
        }
        else
        {
            op->m_MyStatus.ForceFailed(NETTCP_RESULT_ERROR);
        }
    }

    return success;
}

void
netTcp::CancelAsync(netTcpAsyncOp* op)
{
	PROFILE

    if(op->Pending())
    {
        netTcp::CompleteOp(op->m_Id,
                            netStatus::NET_STATUS_CANCELED,
                            NETTCP_RESULT_CANCELED);
    }
}

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT RAGE_LOG_FMT_DEFAULT
#endif

SSL_CTX* 
netTcp::GetSecureSslCtx()
{
	SSL_CTX* sslCtx = s_SecureSslCtx;
    if (sslCtx == nullptr)
    {
        SYS_CS_SYNC(s_CsMain);

		// If we're not or no longer initialized we don't create the ctx
		if(!sm_Initialized)
		{
			netWarning("GetSecureSslCtx called while not initialized");
			return nullptr;
		}

        sslCtx = s_SecureSslCtx;
        if (sslCtx == nullptr)
        {
            rtry
            {
                rverify(wolfSSL_Init() == SSL_SUCCESS, catchall, );

				sysMemAllocator* allocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
				sslCtx = wolfSSL_CTX_new_ex(wolfTLSv1_2_client_method(), allocator);
                rverify(sslCtx != nullptr, catchall, );

                wolfSSL_CTX_set_group_messages(sslCtx);

				//Verify peer will require the server to give us a certificate
				//and will automatically fail the connection if it isn't signed
				//by any of the CAs loaded by the caller.
				//Additionally the domain we're connecting to is validated against
				//the domain in the cert via wolfSSL_check_domain_name which happens
				//when processing netTcpAsyncOp::OP_SSL_CONNECT. The domain is passed
				//in to netTcp::SslConnectAsync from callers, such as netHttpRequest.
				wolfSSL_CTX_set_verify(sslCtx, SSL_VERIFY_PEER, NULL);
            }
            rcatchall
            {

            }
        }
    }

	return sslCtx;
}

SSL_CTX* 
netTcp::GetInsecureSslCtx()
{
    SSL_CTX* sslCtx = s_InsecureSslCtx;
    if (sslCtx == nullptr)
    {
        SYS_CS_SYNC(s_CsMain);

        // If we're not or no longer initialized we don't create the ctx
        if (!sm_Initialized)
        {
            netWarning("GetInsecureSslCtx called while not initialized");
            return nullptr;
        }

        sslCtx = s_InsecureSslCtx;
        if (sslCtx == nullptr)
        {
            rtry
            {
                rverify(wolfSSL_Init() == SSL_SUCCESS, catchall, );
                //Note that some things using this ssl context currently don't support tls 1.2
				sysMemAllocator* allocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
                sslCtx = wolfSSL_CTX_new_ex(wolfSSLv23_client_method(), allocator);
                rverify(sslCtx != nullptr, catchall, );
                wolfSSL_CTX_set_group_messages(sslCtx);
                wolfSSL_CTX_set_verify(sslCtx, SSL_VERIFY_NONE, 0);
            }
            rcatchall
            {

            }
        }
    }

    return sslCtx;
}

#if !RSG_FINAL
SSL_CTX* 
netTcp::GetDebugSslCtx()
{
    SSL_CTX* sslCtx = s_DebugSslCtx;
    if (sslCtx == nullptr)
    {
        SYS_CS_SYNC(s_CsMain);

        // If we're not or no longer initialized we don't create the ctx
        if (!sm_Initialized)
        {
            netWarning("GetDebugSslCtx called while not initialized");
            return nullptr;
        }

        sslCtx = s_DebugSslCtx;
        if (sslCtx == nullptr)
        {
            rtry
            {
                rverify(wolfSSL_Init() == SSL_SUCCESS, catchall, );
                //Note that some things using this ssl context currently don't support tls 1.2
				sysMemAllocator* allocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL);
                sslCtx = wolfSSL_CTX_new_ex(wolfSSLv23_client_method(), allocator);
                rverify(sslCtx != nullptr, catchall, );
                wolfSSL_CTX_set_group_messages(sslCtx);
                wolfSSL_CTX_set_verify(sslCtx, SSL_VERIFY_NONE, 0);
            }
            rcatchall
            {

            }
        }
    }

    return sslCtx;
}
#endif

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s: " fmt, netTcp::GetSocketContextStr(sktId)
#endif

//private:
netTcp::UpdateOpsResult
netTcp::UpdateOps()
{
	PROFILE

	const unsigned startTime = sysTimer::GetSystemMsTime();

    MovePendingOps();
	
    fd_set fdsr, fdsw, fdse;

    FD_ZERO(&fdsr);
    FD_ZERO(&fdsw);
    FD_ZERO(&fdse);

    netSocketFd highest = NET_INVALID_SOCKET_FD;

    {
        PROFILER_EVENT("netTcpUpdateFds")

        SYS_CS_SYNC(s_CsMain);

		//See if we have any fd's to free
		for (int sktId = 0; sktId < MAX_SOCKET_INFOS; sktId++)
		{
			netTcpSocketInfo* si = &s_SocketInfos[sktId];
			if (si->m_CloseFdPending)
			{
				netAssert(si->m_IsAllocated);
				netAssert(NET_IS_VALID_SOCKET_FD(si->m_SktFd));

				netTcp::CloseFd(si->m_SktFd);

				si->m_CloseFdPending = false;
				si->m_SktFd = NET_INVALID_SOCKET_FD;
				si->m_Disconnected = false;
				si->m_IsAllocated = false;
			}
		}

        if(sm_OpList.empty())
        {
			// we're done
			return UPDATE_OPS_DONE;
        }

        //Iterate over the ops, for each socket setting the
        //bits in the appropriate fd_set.  We'll then select()
        //and process ops whose sockets are ready.

        bool first = true;

        AsyncOpList::iterator it = sm_OpList.begin();
        AsyncOpList::iterator next = it;
        AsyncOpList::const_iterator stop = sm_OpList.end();

        for(++next; stop != it; it = next, ++next)
        {
            netTcpAsyncOp* op = *it;
            const netSocketFd sktFd = GetSocketFd(op->m_SktId);
            if(!netVerify(NET_IS_VALID_SOCKET_FD(sktFd)))
            {
                netTcp::CompleteOp(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_TIMED_OUT);
                continue;
            }

            switch(op->m_Type)
            {
            case netTcpAsyncOp::OP_CONNECT:
            case netTcpAsyncOp::OP_SEND_BUFFER:
                FD_SET(sktFd, &fdsw);
                FD_SET(sktFd, &fdse);

                if(first){highest = sktFd;first = false;}
                else if(sktFd > highest){highest = sktFd;}
                break;

            case netTcpAsyncOp::OP_RECV_BUFFER:
                FD_SET(sktFd, &fdsr);
                FD_SET(sktFd, &fdse);

                if(first){highest = sktFd;first = false;}
                else if(sktFd > highest){highest = sktFd;}
                break;

            case netTcpAsyncOp::OP_SSL_CONNECT:                
                FD_SET(sktFd, &fdse);

                if(netTcpAsyncOp::SSL_SOCKET_OP_READ == op->m_Data.m_SslConnect.m_NextSocketOp)
                {
                    FD_SET(sktFd, &fdsr);
                }
				else if(netTcpAsyncOp::SSL_SOCKET_OP_WRITE == op->m_Data.m_SslConnect.m_NextSocketOp)
				{
					FD_SET(sktFd, &fdsw);
				}

				if(first){highest = sktFd;first = false;}
                else if(sktFd > highest){highest = sktFd;}
                break;

            case netTcpAsyncOp::OP_NUM_TYPES:
            case netTcpAsyncOp::OP_INVALID:
                break;
            }
        }
    }

    //Unlock the crit section while waiting.

    //Wait for only one second so we don't block too long before
    //checking for recently queued ops (on the staging list).
	//This also impacts how quickly socket fds are freed
    timeval t;
    t.tv_sec  = 1;
    t.tv_usec = 0;

	int numSkts = 0;

	{
		PROFILER_EVENT("netTcpSocketSelect")
		numSkts = netVerify(NET_IS_VALID_SOCKET_FD(highest)) ? netSocket::Select(int(highest + 1), &fdsr, &fdsw, &fdse, &t) : 0;
	}
		
    const unsigned deltaMs = sysTimer::GetSystemMsTime() - startTime;

    if(numSkts <= 0)
    {
        //No sockets are ready.

        PROFILER_EVENT("netTcpNoSocketReady")

        SYS_CS_SYNC(s_CsMain);

        //Any errors?
        if(numSkts < 0)
        {
            const int lastError = LastError();
            if(NET_EWOULDBLOCK != lastError && NET_EINPROGRESS != lastError)
            {
                //Find the bad sockets and complete them.

                AsyncOpList::iterator it = sm_OpList.begin();
                AsyncOpList::iterator next = it;
                AsyncOpList::const_iterator stop = sm_OpList.end();

                for(++next; stop != it; it = next, ++next)
                {
                    netTcpAsyncOp* op = *it;
					const int sktId = op->m_SktId;
                    const netSocketFd sktFd = GetSocketFd(sktId);

					if(!NET_IS_VALID_SOCKET_FD(sktFd))
					{
						netError("Invalid sktFd on socket: 0x%08x", sktId);
					}
                    else if(FD_ISSET(sktFd, &fdsw) || FD_ISSET(sktFd, &fdsr))
                    {
                        netError("Error %d on socket: 0x%08x", lastError, sktId);
                        netTcp::CompleteOp(op->m_Id, netStatus::NET_STATUS_FAILED,
                                            NETTCP_RESULT_ERROR, lastError);
                    }
                }
            }
        }

        //Check for timeouts.

        AsyncOpList::iterator it = sm_OpList.begin();
        AsyncOpList::iterator next = it;
        AsyncOpList::const_iterator stop = sm_OpList.end();

        for(++next; stop != it; it = next, ++next)
        {
            netTcpAsyncOp* op = *it;

            op->m_TimeoutMs -= (int) deltaMs;

            if(op->m_TimeoutMs <= 0)
            {
                netTcp::CompleteOp(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_TIMED_OUT);
            }
        }
    }
    else
    {
        PROFILER_EVENT("netTcpSocketsReady")

        SYS_CS_SYNC(s_CsMain);

        AsyncOpList::iterator it = sm_OpList.begin();
        AsyncOpList::iterator next = it;
        AsyncOpList::const_iterator stop = sm_OpList.end();

        const unsigned maxProcessingTimeMs = s_TcpSettings.m_MaxProcessingTimeMs;
		unsigned opTimerStartTime = 0;
        for(++next; stop != it; it = next, ++next)
        {
			// Some operations (e.g. wolfSSL_connect()) can take several ms of compute time.
			// Yield time between CPU intensive ops to prevent stalling other threads.
			if(opTimerStartTime == 0)
			{
				opTimerStartTime = sysTimer::GetSystemMsTime();
			}
			else if((sysTimer::GetSystemMsTime() - opTimerStartTime) >= maxProcessingTimeMs)
			{
				return UPDATE_OPS_YIELD;
			}

            netTcpAsyncOp* op = *it;
			const int sktId = op->m_SktId;
			NET_TCP_SET_WOLFSSL_CONTEXT_STR(netTcp::GetSocketContextStr(sktId));
			netTcpSocketInfo* si = GetSocketInfo(sktId);
            netAssert(si);
            const netSocketFd sktFd = GetSocketFd(sktId);
			if(!netVerify(NET_IS_VALID_SOCKET_FD(sktFd)))
			{
				netTcp::CompleteOp(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_TIMED_OUT);
				continue;
			}

            bool completed = false;
			int lastError = 0;

#define NETTCP_COMPLETE_OP(opId, status, result)\
netTcp::CompleteOp(opId, status, result, lastError); completed = true;

            switch(op->m_Type)
            {
            case netTcpAsyncOp::OP_CONNECT:
                if(FD_ISSET(sktFd, &fdse))
                {
                    netError("Error connecting to " NET_ADDR_FMT ", indicated by netSocket::Select()",
                            NET_ADDR_FOR_PRINTF(*op->m_Data.m_Connect.m_Addr));
                    NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                }
                else
                {
                    if (netTcpAsyncOp::STATE_SOCKET_CONNECT == op->m_Data.m_Connect.m_State && FD_ISSET(sktFd, &fdsw))
                    {
                        netDebug2("Connected to " NET_ADDR_FMT ". Source addr: " NET_ADDR_FMT "",
                            NET_ADDR_FOR_PRINTF(*op->m_Data.m_Connect.m_Addr), NET_ADDR_FOR_PRINTF(GetSocketAddr(sktFd)));


#if ENABLE_TCP_PROXY
                        if (op->m_Data.m_Connect.m_ProxyAddr->IsValid())
                        {
                            op->m_Data.m_Connect.m_State = netTcpAsyncOp::STATE_SEND_PROXY_REQUEST;
                        }
                        else
#endif //ENABLE_TCP_PROXY
                        {
                            NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_SUCCEEDED, NETTCP_RESULT_OK);
                        }
                    }

#if ENABLE_TCP_PROXY
                    if (netTcpAsyncOp::STATE_SEND_PROXY_REQUEST == op->m_Data.m_Connect.m_State)
                    {
                        char buf[512];
                        FormatProxyHeader(buf, *op->m_Data.m_Connect.m_Addr, nullptr);

#if FAKE_DROP_LATENCY
                        // SendBuffer is blocking so during a fake uplink disconnect where we simply fake it
                        // by not sending any byte this would lock the mutex till it times out and therefore
                        // block the main thread
                        const bool prev = si->m_IgnoreUplinkDisconnect;
                        si->m_IgnoreUplinkDisconnect = true;
#endif
                        netTcpResult result = netTcp::SendBuffer(op->m_SktId, buf, ustrlen(buf), 10);
#if FAKE_DROP_LATENCY
                        si->m_IgnoreUplinkDisconnect = prev;
#endif
                        if (result == NETTCP_RESULT_OK)
                        {
                            op->m_Data.m_Connect.m_State = netTcpAsyncOp::STATE_RECV_PROXY_RESPONSE;
                        }
                        else
                        {
                            netError("Error sending CONNECT to " NET_ADDR_FMT,
                                NET_ADDR_FOR_PRINTF(*op->m_Data.m_Connect.m_ProxyAddr));
                            NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                        }
                    }
                    else if (netTcpAsyncOp::STATE_RECV_PROXY_RESPONSE == op->m_Data.m_Connect.m_State)
                    {
                        char buf[512] = { 0 };
                        unsigned bytesReceived = 0;
                        netTcpResult result = netTcp::Receive(op->m_SktId, buf, sizeof(buf), &bytesReceived);

                        if (NETTCP_RESULT_OK != result)
                        {
                            netError("Error receiving on socket 0x%08x", op->m_SktId);
                            NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, result);
                        }
                        else
                        {
                            if ((strstr(buf, "HTTP/1.1 200") != NULL) && (strstr(buf, "\r\n\r\n") != NULL))
                            {
                                NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_SUCCEEDED, NETTCP_RESULT_OK);
                            }
                            else if (bytesReceived > 0)
                            {
                                netError("Error connecting to " NET_ADDR_FMT ", Invalid proxy response %s",
                                    NET_ADDR_FOR_PRINTF(*op->m_Data.m_Connect.m_Addr), buf);
                                NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                            }
                        }
                    }
#endif //ENABLE_TCP_PROXY
                }
                break;

            case netTcpAsyncOp::OP_SSL_CONNECT:
                if(FD_ISSET(sktFd, &fdse))
                {
					netError("Error connecting to " NET_ADDR_FMT ", indicated by netSocket::Select()",
                            NET_ADDR_FOR_PRINTF(*op->m_Data.m_SslConnect.m_Addr));
                    NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                }
                else
                {
                    if(netTcpAsyncOp::STATE_SOCKET_CONNECT == op->m_Data.m_SslConnect.m_State
                        && FD_ISSET(sktFd, &fdsw))
                    {
						netDebug2("Connected to " NET_ADDR_FMT ". Source addr: " NET_ADDR_FMT ". Initiating SSL handshake.",
                                NET_ADDR_FOR_PRINTF(*op->m_Data.m_SslConnect.m_Addr), NET_ADDR_FOR_PRINTF(GetSocketAddr(sktFd)));

#if ENABLE_TCP_PROXY
						if(op->m_Data.m_SslConnect.m_ProxyAddr->IsValid())
						{
							op->m_Data.m_SslConnect.m_State = netTcpAsyncOp::STATE_SEND_PROXY_REQUEST;
						}
						else
#endif //ENABLE_TCP_PROXY
						{
							op->m_Data.m_SslConnect.m_State = netTcpAsyncOp::STATE_SSL_CONNECT;
						}
                    }

#if ENABLE_TCP_PROXY
					if(netTcpAsyncOp::STATE_SEND_PROXY_REQUEST == op->m_Data.m_SslConnect.m_State)
					{
						char buf[512];
						FormatProxyHeader(buf, *op->m_Data.m_SslConnect.m_Addr, op->m_Data.m_SslConnect.m_DomainName);

#if FAKE_DROP_LATENCY
						// SendBuffer is blocking so during a fake uplink disconnect where we simply fake it
						// by not sending any byte this would lock the mutex till it times out and therefore
						// block the main thread
						const bool prev = si->m_IgnoreUplinkDisconnect;
						si->m_IgnoreUplinkDisconnect = true;
#endif
						netTcpResult result = netTcp::SendBuffer(op->m_SktId, buf, ustrlen(buf), 10);
#if FAKE_DROP_LATENCY
						si->m_IgnoreUplinkDisconnect = prev;
#endif
						if(result == NETTCP_RESULT_OK)
						{
							op->m_Data.m_SslConnect.m_State = netTcpAsyncOp::STATE_RECV_PROXY_RESPONSE;
							op->m_Data.m_SslConnect.m_NextSocketOp = netTcpAsyncOp::SSL_SOCKET_OP_READ;
						}
						else
						{
							netError("Error sending CONNECT to " NET_ADDR_FMT,
								NET_ADDR_FOR_PRINTF(*op->m_Data.m_SslConnect.m_ProxyAddr));
							NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
						}
					}
					else if(netTcpAsyncOp::STATE_RECV_PROXY_RESPONSE == op->m_Data.m_SslConnect.m_State)
					{
						char buf[512] = {0};
						unsigned bytesReceived = 0;
						netTcpResult result = netTcp::Receive(op->m_SktId, buf, sizeof(buf), &bytesReceived);

						if(NETTCP_RESULT_OK != result)
						{
							netError("Error receiving on socket 0x%08x", op->m_SktId);
							NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, result);
						}
						else
						{
							if((strstr(buf, "HTTP/1.1 200") != NULL) && (strstr(buf, "\r\n\r\n") != NULL))
							{
								op->m_Data.m_SslConnect.m_State = netTcpAsyncOp::STATE_SSL_CONNECT;
								op->m_Data.m_SslConnect.m_NextSocketOp = netTcpAsyncOp::SSL_SOCKET_OP_WRITE;
							}
							else if (bytesReceived > 0)
							{
								netError("Error connecting to " NET_ADDR_FMT ", Invalid proxy response %s",
									NET_ADDR_FOR_PRINTF(*op->m_Data.m_SslConnect.m_Addr), buf);
								NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
							}
						}
					}
					else
#endif //ENABLE_TCP_PROXY
					if(netTcpAsyncOp::STATE_SSL_CONNECT == op->m_Data.m_SslConnect.m_State)
                    {
                        int result;
                            
                        if(NULL == si->m_Ssl)
                        {
							si->m_Ssl = wolfSSL_new(op->m_Data.m_SslConnect.m_SslCtx);
                            if(NULL == si->m_Ssl)
                            {
                                netError("Error allocating SSL object");
                                NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                            }
                            else if(SSL_SUCCESS != (result = wolfSSL_set_fd(si->m_Ssl, int(sktFd))))
                            {
                                netError("Error initializing SSL object");
                                NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                            }
                        }

                        if(NULL != si->m_Ssl
                            && (FD_ISSET(sktFd, &fdsw) || FD_ISSET(sktFd, &fdsr)))
                        {
                            //If a domain name was provided, tell wolfSSL to verify it.
                            //This must be done before calling connect. wolfSSL will internally
                            //allocate a copy of the string

                            OUTPUT_ONLY(const utimer_t startTime = sysTimer::GetTicks());
                            if (op->m_Data.m_SslConnect.m_DomainName)
                            {
								result = wolfSSL_check_domain_name(si->m_Ssl, op->m_Data.m_SslConnect.m_DomainName);
                                if (SSL_SUCCESS == result)
                                {
									PROFILER_EVENT("wolfSSL_connect")
                                    result = wolfSSL_connect(si->m_Ssl);
                                }
                                else
                                {
									netError("wolfSSL_check_domain_name failed for '%s'", op->m_Data.m_SslConnect.m_DomainName);
                                }
                            }
                            else
                            {
								PROFILER_EVENT("wolfSSL_connect")
                                result = wolfSSL_connect(si->m_Ssl);
                            }
                            netDebug1("SSL_connect took %u us", static_cast<u32>(sysTimer::GetTicksToMicroseconds()* static_cast<float>(sysTimer::GetTicks() - startTime)));

                            if(SSL_SUCCESS == result)
                            {
                                netDebug2("SSL: Connected to " NET_ADDR_FMT "",
                                        NET_ADDR_FOR_PRINTF(*op->m_Data.m_SslConnect.m_Addr));
								OUTPUT_ONLY(DumpSslConnectionInfo(si->m_Ssl, op, SSL_SUCCESS));

								NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_SUCCEEDED, NETTCP_RESULT_OK);
                            }
                            else
                            {
                                lastError = wolfSSL_get_error(si->m_Ssl, result);
								if(lastError == SSL_ERROR_WANT_READ)
								{
									op->m_Data.m_SslConnect.m_NextSocketOp = netTcpAsyncOp::SSL_SOCKET_OP_READ;
								}
								else if(lastError == SSL_ERROR_WANT_WRITE)
								{
									op->m_Data.m_SslConnect.m_NextSocketOp = netTcpAsyncOp::SSL_SOCKET_OP_WRITE;
								}
								else
                                {
                                    OUTPUT_ONLY(char buf[128];)
                                    netError("Error creating SSL connection: %s (%d)",
                                                wolfSSL_ERR_error_string(lastError, buf), lastError);
									OUTPUT_ONLY(DumpSslConnectionInfo(si->m_Ssl, op, lastError));

                                    NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                                }
                            }
                        }
                    }
                }
                break;

            case netTcpAsyncOp::OP_RECV_BUFFER:
                if(FD_ISSET(sktFd, &fdse))
                {
					netError("Error receiving on socket 0x%08x, indicated by netSocket::Select()", op->m_SktId);
                    NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                }
                else if(FD_ISSET(sktFd, &fdsr))
                {
                    unsigned numRcvd;
                    netTcpResult result =
                        netTcp::Receive(op->m_SktId,
                                        &((u8*) op->m_Data.m_RcvBuf.m_Buf)[op->m_Data.m_RcvBuf.m_NumRcvd],
                                        op->m_Data.m_RcvBuf.m_SizeofBuf - op->m_Data.m_RcvBuf.m_NumRcvd,
                                        &numRcvd, &lastError);

                    if(NETTCP_RESULT_OK != result)
                    {
						netError("Error receiving on socket 0x%08x", op->m_SktId);
                        NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, result);
                    }
                    else
                    {
                        op->m_Data.m_RcvBuf.m_NumRcvd += numRcvd;
                        netAssert(op->m_Data.m_RcvBuf.m_NumRcvd <= op->m_Data.m_RcvBuf.m_SizeofBuf);
                        if(op->m_Data.m_RcvBuf.m_NumRcvd == op->m_Data.m_RcvBuf.m_SizeofBuf)
                        {
                            NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_SUCCEEDED, NETTCP_RESULT_OK);
                        }
                    }
                }
                break;

            case netTcpAsyncOp::OP_SEND_BUFFER:
                if(FD_ISSET(sktFd, &fdse))
                {
					netError("Error sending on socket 0x%08x, indicated by netSocket::Select()", op->m_SktId);
                    NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                }
                else if((numSkts > 0) && FD_ISSET(sktFd, &fdsw))
                {
                    unsigned numSent;
                    netTcpResult result =
                        netTcp::Send(op->m_SktId,
                                        &((const u8*) op->m_Data.m_SndBuf.m_Buf)[op->m_Data.m_SndBuf.m_NumSent],
                                        op->m_Data.m_SndBuf.m_SizeofBuf - op->m_Data.m_SndBuf.m_NumSent,
                                        &numSent, &lastError);

                    if(NETTCP_RESULT_OK != result)
                    {
						netError("Error sending on socket 0x%08x", op->m_SktId);
                        NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, result);
                    }
                    else
                    {
                        op->m_Data.m_SndBuf.m_NumSent += numSent;
                        netAssert(op->m_Data.m_SndBuf.m_NumSent <= op->m_Data.m_SndBuf.m_SizeofBuf);
                        if(op->m_Data.m_SndBuf.m_NumSent == op->m_Data.m_SndBuf.m_SizeofBuf)
                        {
                            NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_SUCCEEDED, NETTCP_RESULT_OK);
                        }
                    }
                }
                break;

            case netTcpAsyncOp::OP_NUM_TYPES:
            case netTcpAsyncOp::OP_INVALID:
                NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
                break;
            }

            if(!completed)
            {
                //Check for timeout.
                op->m_TimeoutMs -= (int) deltaMs;

                if(op->m_TimeoutMs <= 0)
                {
                    NETTCP_COMPLETE_OP(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_TIMED_OUT);
                }
            }

            //Clear the read/write bits so if we have the same socket
            //later in the list, in another op, it isn't processed again.
            //Leave the error bit set so if we come across the same socket
            //later in the list we can complete the op with an error condition.
            FD_CLR(sktFd, &fdsr);
            FD_CLR(sktFd, &fdsw);

#undef NETTCP_COMPLETE_OP
        }
    }

	return UPDATE_OPS_CONTINUE;
}

void
netTcp::Update()
{
	PROFILER_CATEGORY("netTcpWorker", Profiler::CategoryColor::Network);

	//Continue processing until the list of async ops is empty
	while(sm_Initialized)
	{
		PROFILER_EVENT("netTcpWhile")

		const netTcp::UpdateOpsResult result = netTcp::UpdateOps();

		if(result == UPDATE_OPS_DONE)
		{
			break;
		}
		else if(result == UPDATE_OPS_YIELD)
		{
			sysIpcSleep(s_TcpSettings.m_ThreadYieldTimeMs);
		}
	}
}

#if !__NO_OUTPUT
void
netTcp::DumpSslConnectionInfo(WOLFSSL* NET_WOLFSSL_DEBUG_ONLY(ssl), const netTcpAsyncOp* NET_WOLFSSL_DEBUG_ONLY(op), const int NET_WOLFSSL_DEBUG_ONLY(result))
{
#if NET_WOLFSSL_DEBUG
	PROFILE 

	const char* protocol = wolfSSL_get_version(ssl);
	const char* cipher = wolfSSL_get_cipher(ssl);
	const char* ecdhCurve = wolfSSL_get_curve_name(ssl);
  
    const int sktId = op->m_SktId;
	if(ecdhCurve)
	{
		netDebug2("Protocol: %s, Cipher: %s, ECDH Curve: %s", protocol, cipher, ecdhCurve);
	}
	else
	{
		netDebug2("Protocol: %s, Cipher: %s", protocol, cipher);
	}

	WOLFSSL_X509_CHAIN* chain = wolfSSL_get_peer_chain(ssl);
	if(chain)
	{
		char buf[8192] = { 0 };
		char tmp[2048] = { 0 };

		const int chainCount = wolfSSL_get_chain_count(chain);

		// cert hierarchy
		for(int i = chainCount - 1; i >= 0; i--)
		{
			WOLFSSL_X509* cert = wolfSSL_get_chain_X509(chain, i);

			char* issuerCommonName = nullptr;

			// the server doesn't always present the root cert
			if(i == (chainCount - 1))
			{
				WOLFSSL_X509_NAME* issuerName = wolfSSL_X509_get_issuer_name(cert);
				if (issuerName)
				{
					char* issuer = wolfSSL_X509_NAME_oneline(issuerName, tmp, sizeof(tmp));

					if(issuer)
					{
						// assumes the common name section is at the end of the string
						const char delim[] = "/CN=";
						issuerCommonName = strstr(issuer, delim);
						if (issuerCommonName)
						{
							issuerCommonName += sizeof(delim) - 1;
						}
					}
				}
			}

			char* commonName = wolfSSL_X509_get_subjectCN(cert);
			if(commonName)
			{
				if(issuerCommonName && (strcmp(commonName, issuerCommonName) != 0))
				{
					safecatf(buf, "%s%s", issuerCommonName, "  ->  ");
				}

				safecatf(buf, "%s%s", commonName, (i == 0) ? "" : "  ->  ");
			}

			wolfSSL_X509_free(cert);
		}

		const char* domain = op->m_Data.m_SslConnect.m_DomainName;
		if(domain)
		{
			netDebug2("Server's certificate chain for %s: %s", domain, buf);
		}
		else
		{
			netDebug2("Server's certificate chain: %s", buf);
		}

		if ((result == SSL_SUCCESS) && !PARAM_netssldump.Get())
		{
			return;
		}

		netAssertf(result != ASN_NO_SIGNER_E,
					"This SSL context is not configured to trust the certificate chain presented by '%s' (" NET_ADDR_FMT "):\n%s",
					domain,
					NET_ADDR_FOR_PRINTF(*op->m_Data.m_SslConnect.m_Addr),
					buf);

		// full cert hierarchy details
		for(int i = 0; i < chainCount; ++i)
		{
			WOLFSSL_X509* cert = wolfSSL_get_chain_X509(chain, i);
			netDebug3("Certificate #%d of %d", i + 1, chainCount);

			unsigned char serial[64] = {0};
			int serialLen = sizeof(serial);
			int result = wolfSSL_X509_get_serial_number(cert, serial, &serialLen);
			if(result == SSL_SUCCESS)
			{
				char szSerial[128] = {0};
				for(int i = 0; i < serialLen; ++i)
				{
					safecatf(szSerial, "%02x ", serial[i]);
				}
				netDebug3("    Serial Number: %s", szSerial);
			}
			else
			{
				netDebug3("    Failed to retrieve cert's serial number");
			}
			
			char* commonName = wolfSSL_X509_get_subjectCN(cert);
			if(commonName)
			{
				netDebug3("    Common Name: %s", commonName);
			}
			else
			{
				netDebug3("    Failed to retrieve cert's subject common name");
			}

			netDebug3("    Certificate Authority: %s", wolfSSL_X509_get_isCA(cert) ? "true" : "false");

			int x509Version = wolfSSL_X509_version(cert);
			netDebug3("    x509 Version: %d", x509Version);
			
			WOLFSSL_X509_NAME* subjectName = wolfSSL_X509_get_subject_name(cert);

			if(subjectName)
			{
				char* subject = wolfSSL_X509_NAME_oneline(subjectName, tmp, sizeof(tmp));

				if(subject)
				{
					netDebug3("    Subject Name: %s", subject);
				}
				else
				{
					netDebug3("    Failed to retrieve cert's subject name from wolfSSL_X509_NAME_oneline");
				}
			}
			else
			{
				netDebug3("    Failed to retrieve cert's subject name");
			}

			WOLFSSL_X509_NAME* issuerName = wolfSSL_X509_get_issuer_name(cert);

			if(issuerName)
			{
				char* issuer = wolfSSL_X509_NAME_oneline(issuerName, tmp, sizeof(tmp));

				if(issuer)
				{
					netDebug3("    Issuer Name: %s", issuer);
				}
				else
				{
					netDebug3("    Failed to retrieve cert's issuer name from wolfSSL_X509_NAME_oneline");
				}
			}
			else
			{
				netDebug3("    Failed to retrieve cert's issuer name");
			}

			static const unsigned TIMESTAMP_LEN = 256;
			char timestamp[TIMESTAMP_LEN];

			const unsigned char* notBefore = wolfSSL_X509_notBefore(cert);
			if (notBefore)
			{
				netDebug3("    Not Before: %s", netCrypto::AsnDateTimeToString(notBefore, timestamp, sizeof(timestamp)));
			}
			else
			{
				netDebug3("    Failed to retrieve cert's notBefore date/time");
			}

			const unsigned char* notAfter = wolfSSL_X509_notAfter(cert);
			if (notAfter)
			{
				netDebug3("    Not After: %s", netCrypto::AsnDateTimeToString(notAfter, timestamp, sizeof(timestamp)));
			}
			else
			{
				netDebug3("    Failed to retrieve cert's notAfter date/time");
			}

			int outLen = 0;
			result = wolfSSL_get_chain_cert_pem(chain, i, (unsigned char*)buf, sizeof(buf), &outLen);
			if(result == SSL_SUCCESS)
			{
                if(PARAM_netssldump.Get())
                {
                    diagLoggedPrintLn((char*)buf, outLen);
                }
			}
			else
			{
				netDebug3("    Failed to retrieve PEM-encoded cert");
			}

			wolfSSL_X509_free(cert);
		}
	}
#endif
}
#endif

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT RAGE_LOG_FMT_DEFAULT
#endif

netSocketAddress
netTcp::GetSocketAddr(const netSocketFd sktFd)
{
	// get the local ip & port of the socket. This is useful to match up the TCP operation with Wireshark
	// captures (e.g. in Wireshark, apply display filter "tcp.srcport == X || tcp.dstport == X",
	// where X is the port logged out here).
	
	sockaddr_storage saddr = {0};
	int saddr_len = sizeof(saddr);
	if(getsockname(sktFd, (sockaddr*)&saddr, (socklen_t*)&saddr_len) != 0)
	{
		netError("getsockname failed with error:%d", LastError());
	}

	return netSocketAddress((sockaddr*)&saddr);
}

#if IPv4_IPv6_DUAL_STACK
bool
netTcp::SetSockOptIpV6Only(netSocketFd* sktFd, const bool forceIpV6Only)
{
	int v6only = forceIpV6Only ? 1 : 0;
	if(setsockopt(*sktFd, IPPROTO_IPV6, IPV6_V6ONLY, (const char*)&v6only, sizeof(v6only)) != 0)
	{
		netError("Set socket option IPV6_V6ONLY to %s failed with error:%d", LogBool(forceIpV6Only), LastError());
		return false;
	}

	return true;
}
#endif

bool 
netTcp::SetSockOptBlocking(netSocketFd* sktFd, const netSocketBlockingType blockingType)
{
#if RSG_WIN32
	u_long nonblock = (blockingType == netSocketBlockingType::NET_SOCKET_BLOCKING) ? 0 : 1;

	if(ioctlsocket(*sktFd, FIONBIO, &nonblock) != 0)
	{
		netError("Set socket blocking option failed with error:%d", LastError());
		return false;
	}

	return true;
#elif RSG_SCE
	int nonblock = (blockingType == netSocketBlockingType::NET_SOCKET_BLOCKING) ? 0 : 1;

	if(setsockopt(*sktFd, SOL_SOCKET, SO_NBIO, &nonblock, sizeof(nonblock)) != 0)
	{
		netError("Set socket blocking option failed with error:%d", LastError());
		return false;
	}
	
	return true;
#else
	int flags = fcntl(*sktFd, F_GETFL, 0);
	if(flags == -1)
	{
		netError("fcntl - F_GETFL failed with error:%d", LastError());
		return false;
	}

	flags = (blockingType == netSocketBlockingType::NET_SOCKET_BLOCKING) ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
	
	if(fcntl(*sktFd, F_SETFL, flags) != 0)
	{
		netError("Set socket blocking option failed with error:%d", LastError());
		return false;
	}

	return true;
#endif
}

bool
netTcp::SetSockOptAbortiveClose(netSocketFd* sktFd, const bool enabledAbortiveClose)
{
	// TODO: NS - revisit why abortive close is enabled - this will set a RST (connection reset)
	// error on close, and can discard pending data, rather than initiate a graceful close.

	if(enabledAbortiveClose)
	{
		// enable linger with timeout of 0

        struct linger lngr;
        lngr.l_onoff = 1;
        lngr.l_linger = 0;

        if(setsockopt(*sktFd, SOL_SOCKET, SO_LINGER, (char*) &lngr, sizeof(lngr)) != 0)
		{
			netError("Set socket option SO_LINGER failed with error:%d", LastError());
			return false;
		}
	}

	return true;
}

bool
netTcp::SetSockOptDisableSigpipe(netSocketFd* sktFd)
{
	// some systems throw a signal/exception (terminating the process if unhandled) if we write 
	// to a socket/pipe when the remote end has closed. Disable this behaviour and handle socket
	// errors as normal. Note that some systems (e.g. many *nix systems including Stadia) don't
	// support disabling sigpipe on a per-socket basis. Some systems require a per-thread or 
	// per-process sigpipe disabler. See netTcp::IgnoreSigPipe().

#if (RSG_IOS || RSG_OSX)
	int on = 1;
	if(setsockopt(*sktFd, SOL_SOCKET, SO_NOSIGPIPE, (void *)&on, sizeof(int)) != 0)
	{
		netError("Set socket option SO_NOSIGPIPE failed with error:%d", LastError());
		return false;
	}
#else
	(void)sktFd;
#endif

	return true;
}

bool
netTcp::SetSockOptDisableNagle(netSocketFd* sktFd)
{
#if !RSG_NX // NX can do nagle but through a separate call
	int noNagle = 1;
	const int len = sizeof(noNagle);
	if(setsockopt(*sktFd, IPPROTO_TCP, TCP_NODELAY, (const char*)&noNagle, len) != 0)
	{
		netError("Set socket option TCP_NODELAY failed with error:%d", LastError());
		return false;
	}
#else
	(void)sktFd;
#endif

	return true;
}

bool
netTcp::SetSockOptRcvBufferSize(netSocketFd* sktFd, const unsigned rcvBufSizeBytes)
{
	int rcvBuffSet = rcvBufSizeBytes;
	if(setsockopt(*sktFd, SOL_SOCKET, SO_RCVBUF, (char*)&rcvBuffSet, sizeof(rcvBuffSet)) != 0)
	{
		netError("Failed to set receive buffer size to %u bytes. Error:%d", LastError(), rcvBuffSet);
		return false;
	}

	return true;
}

int
netTcp::GetSockOptRcvBufferSize(netSocketFd* sktFd)
{
	int rcvBuffSet = 0;
	socklen_t optlen = sizeof(rcvBuffSet);

	if (getsockopt(*sktFd, SOL_SOCKET, SO_RCVBUF, (char*)&rcvBuffSet, &optlen) != 0)
	{
		netError("Failed to get receive buffer size. Error:%d", LastError());
		return -1;
	}

	return rcvBuffSet;
}

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s: " fmt, contextStr
#endif

bool 
netTcp::CreateSocket(netSocketFd* sktFd, const char* OUTPUT_ONLY(contextStr))
{
	PROFILE

    bool success = false;

    *sktFd = NET_INVALID_SOCKET_FD;

    rtry
    {
		const int addressFamily = IPv4_IPv6_DUAL_STACK_SWITCH(AF_INET6, AF_INET);

		*sktFd = socket(addressFamily, SOCK_STREAM, IPPROTO_TCP);
        rverify(NET_IS_VALID_SOCKET_FD(*sktFd),
				catchall,
				netError("Socket creation failed with error:%d", LastError()));

		// disable IPv6-only mode, we support IPv4-mapped IPv6 addresses
		IPv4_IPv6_DUAL_STACK_ONLY(SetSockOptIpV6Only(sktFd, false));
		SetSockOptBlocking(sktFd, netSocketBlockingType::NET_SOCKET_NONBLOCKING);
		SetSockOptAbortiveClose(sktFd, true);
		SetSockOptDisableSigpipe(sktFd);
		SetSockOptDisableNagle(sktFd);

		if (s_TcpSettings.m_SoRcvBufferSize > 0)
		{
			netDebug("Setting recv buffer from %d to %u", GetSockOptRcvBufferSize(sktFd), s_TcpSettings.m_SoRcvBufferSize);
			// Setting this param is generally a bad idea because it enforces a fixed size instead of a growing buffer
			SetSockOptRcvBufferSize(sktFd, s_TcpSettings.m_SoRcvBufferSize);
		}

        success = true;
    }
    rcatchall
    {
        if(NET_IS_VALID_SOCKET_FD(*sktFd))
        {
            netTcp::CloseFd(*sktFd);
			*sktFd = NET_INVALID_SOCKET_FD;
        }
    }

    return success;
}

bool
netTcp::ConnectFd(const netSocketAddress&addr, netSocketFd* sktFd, const char* contextStr)
{
	PROFILE

    bool success = false;

    *sktFd = NET_INVALID_SOCKET_FD;

    rtry
    {
		rcheckall(CreateSocket(sktFd, contextStr));

		// on Orbis, connect() doesn't work with sockaddr_storage for some reason
		IPv4_IPv6_DUAL_STACK_SWITCH(sockaddr_in6, sockaddr_in) saddr = {0};
		rverifyall(addr.ToSockAddr((sockaddr*)&saddr, sizeof(saddr)));
		int sin_len = sizeof(saddr);

        int err = connect(*sktFd, (struct sockaddr*)&saddr, sin_len);

        rcheck((0 == err)
               || (NET_EWOULDBLOCK == LastError())
               || (NET_EINPROGRESS == LastError()),
                 catchall,
                 netError("netTcp::Connect: connect() failed with error:%d", LastError()));

        success = true;
    }
    rcatchall
    {
		if(NET_IS_VALID_SOCKET_FD(*sktFd))
		{
			netTcp::CloseFd(*sktFd);
			*sktFd = NET_INVALID_SOCKET_FD;
		}
	}

    return success;
}

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT RAGE_LOG_FMT_DEFAULT
#endif

bool
netTcp::WaitForSocketFds(const netSocketFd* sktFds,
                          bool* ready,
                          bool* except,
                          const int numSkts,
                          const netTcpWaitType waitType,
                          const unsigned timeoutSecs)
{
    const bool forReading = (NETTCP_WAIT_READ == waitType);

    rtry
    {
        fd_set fds;
        fd_set exceptionfds;

        FD_ZERO(&fds);
        FD_ZERO(&exceptionfds);

		netSocketFd highest = NET_INVALID_SOCKET_FD;

        for(int i = 0; i < numSkts; ++i)
        {
			if(NET_IS_VALID_SOCKET_FD(sktFds[i]))
			{
				FD_SET(sktFds[i], &fds);
				FD_SET(sktFds[i], &exceptionfds);

				if(!NET_IS_VALID_SOCKET_FD(highest))
				{
					highest = sktFds[i];
				}
				else if(sktFds[i] > highest)
				{
					highest = sktFds[i];
				}
			}
        }

		rcheck(NET_IS_VALID_SOCKET_FD(highest), catchall, );

        timeval t;
        t.tv_sec  = timeoutSecs;
        t.tv_usec = 0;

        const int result = netSocket::Select(int(highest+1),
                                            forReading ? &fds : NULL, 
                                            forReading ? NULL : &fds, 
                                            &exceptionfds, 
                                            &t);

        rcheck(-1 != result,
            catchall, 
            netError("netTcp::WaitForSocket: select() error (%d)", LastError()));

        if(0 == result)
        {
            //timed out
            for(int i = 0; i < numSkts; ++i)
            {
                except[i] = ready[i] = false;
            }
        }
        else
        {
            for(int i = 0; i < numSkts; ++i)
            {
                except[i] = NET_IS_VALID_SOCKET_FD(sktFds[i]) ? !!FD_ISSET(sktFds[i], &exceptionfds) : false;
				ready[i] = NET_IS_VALID_SOCKET_FD(sktFds[i]) ? !!FD_ISSET(sktFds[i], &fds) : false;

                if(except[i])
                {
                    netDebug("Exception on socket FD 0x%08x", (int)sktFds[i]);
                }
            }
        }

		return true;
    }
    rcatchall 
    {
		return false;
    }
}

void
netTcp::CloseFd(const netSocketFd sktFd)
{
    netDebug2("Closing socket FD 0x%08x...", (int)sktFd);

    if(NET_IS_VALID_SOCKET_FD(sktFd))
    {
#if RSG_WIN32
        shutdown(sktFd, SD_BOTH);
#else
        shutdown(sktFd, SHUT_RDWR);
#endif

        int err;

#if RSG_SCE
        OUTPUT_ONLY(PrintSocketInfo(sktFd));
        err = sceNetSocketClose(sktFd);
#elif RSG_WIN32
		err = closesocket(sktFd);
#else
        err = close(sktFd);
#endif

        if(0 != err)
        {
			netError("netTcp::CloseSocket closesocket() failed with error (%d)", LastError());
        }
    }
}

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s: " fmt, netTcp::GetSocketContextStr(sktId)
#endif

bool
netTcp::QueueOp(netTcpAsyncOp* op)
{
	PROFILE

	OUTPUT_ONLY(const int sktId = op->m_SktId;)

	if(!sm_Initialized)
	{
		netError("QueueOp :: not initialized");
		return false;
	}

    netAssert(op->Pending());

    bool success = false;

    unsigned opId;

    do 
    {
        opId = sysInterlockedIncrement(&s_OpId);
    } while(0 == opId);

    op->m_Id = opId;

    netDebug3("Queuing %s (ID %u) on socket 0x%08x...",
                netTcpAsyncOp::GetOpTypeString(op->m_Type),
                op->m_Id,
                op->m_SktId);

    {
		// We don't lock s_CsMain here to avoid blocking the main thread while
		// a CPU intensive op is executing on the worker thread. Queue ops to
		// a pending ops list, and promote it to the main op list on the 
		// worker thread.
        SYS_CS_SYNC(s_CsPendingOpsList);

		// check initialized flag again in critical section, in case Shutdown()
		// gets called after the previous check above.
		if(!sm_Initialized)
		{
			netError("QueueOp :: not initialized");
			return false;
		}

        sm_PendingOpList.push_back(op);
    }

    if(netTcp::WakeupWorker())
    {
        success = true;
    }
    else
    {
        netTcp::CompleteOp(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_ERROR);
    }

    return success;
}

#if !__NO_OUTPUT
static const char* GetResultString(const netTcpResult result)
{
    switch(result)
    {
    case NETTCP_RESULT_CANCELED: return "NETTCP_RESULT_CANCELED";
    case NETTCP_RESULT_ERROR: return "NETTCP_RESULT_ERROR";
    case NETTCP_RESULT_TIMED_OUT: return "NETTCP_RESULT_TIMED_OUT";
    case NETTCP_RESULT_DISCONNECTED: return "NETTCP_RESULT_DISCONNECTED";
    case NETTCP_RESULT_OK: return "NETTCP_RESULT_OK";
	}

	return "*** NETTCP RESULT ***";
}
#endif  //!__NO_OUTPUT

void
netTcp::CompleteOp(const unsigned opId,
                   const netStatus::StatusCode statusCode,
                   netTcpResult result,
				   int lastError)
{
	PROFILE

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT RAGE_LOG_FMT_DEFAULT
#endif

    netAssert(0 != opId);
    netAssert((netStatus::NET_STATUS_SUCCEEDED == statusCode && NETTCP_RESULT_OK == result)
            || (netStatus::NET_STATUS_SUCCEEDED != statusCode && NETTCP_RESULT_OK != result));

#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s: " fmt, netTcp::GetSocketContextStr(sktId)
#endif

    // We need to lock both because otherwise an op could be moved from sm_PendingOpList to sm_OpList
    // just before we start iterating sm_PendingOpList
    // Also due to the order of mutexes in this method if we need to lock both
    // we must always lock s_CsMain first and then s_CsPendingOpsList. Otherwise we risk deadlocks.
    SYS_CS_SYNC(s_CsMain);
    rage::sysCriticalSection csPendingOpsList(s_CsPendingOpsList);

    AsyncOpList::iterator it = sm_OpList.begin();
    AsyncOpList::const_iterator stop = sm_OpList.end();
    netTcpAsyncOp* op = NULL;

    for(; stop != it; ++it)
    {
        if((*it)->m_Id == opId)
        {
            op = *it;
            sm_OpList.erase(op);
            break;
        }
    }

    if (op == nullptr)
    {
        it = sm_PendingOpList.begin();
        stop = sm_PendingOpList.end();

        for (; stop != it; ++it)
        {
            if ((*it)->m_Id == opId)
            {
                op = *it;
				
				OUTPUT_ONLY(const int sktId = op->m_SktId;)
				netDebug3("%s (ID %u) on socket 0x%08x completed from pending queue",
                    netTcpAsyncOp::GetOpTypeString(op->m_Type), op->m_Id, sktId);
					
                sm_PendingOpList.erase(op);
                break;
            }
        }
    }

    if(op)
    {
		OUTPUT_ONLY(const int sktId = op->m_SktId;)
		netAssert(op->Pending());
        netDebug3("%s (ID %u) on socket 0x%08x completed with result \"%s\"",
                    netTcpAsyncOp::GetOpTypeString(op->m_Type),
                    op->m_Id,
                    sktId,
                    GetResultString(result));

        op->Complete(statusCode, result, lastError);
    }
}

void
netTcp::MovePendingOps()
{
    PROFILE

    SYS_CS_SYNC(s_CsMain);
    rage::sysCriticalSection csPendingOpsList(s_CsPendingOpsList);

    sm_OpList.splice(sm_OpList.end(), sm_PendingOpList);
}

sysIpcThreadId s_ThreadId = sysIpcThreadIdInvalid;
bool s_TcpThreadExit = false;

#if RSG_LINUX
void 
netTcp::IgnoreSigPipe()
{
	// ignore Linux's SIGPIPE signal. Otherwise if we write to a socket/pipe
	// when the remote end has closed, the entire process terminates.
	sigset_t set;
	sigset_t old_state;

	// get the current set of ignored signals for this thread
	int r = sigprocmask(SIG_BLOCK, NULL, &old_state);
	if(r >= 0)
	{
		// add SIGPIPE to the set
		set = old_state;
		r = sigaddset(&set, SIGPIPE);
		if(r >= 0)
		{
			// apply the updated set to this thread
			sigprocmask(SIG_BLOCK, &set, NULL);
		}
	}
}
#endif

void
netTcp::StartWorker(const int cpuAffinity)
{
	PROFILE

    SYS_CS_SYNC(s_CsMain);

	if(sysIpcThreadIdInvalid == s_ThreadId)
	{
		s_TcpThreadExit = false;

		s_ThreadId =
			sysIpcCreateThread(&netTcp::WorkerUpdate,
								NULL,
								sysIpcMinThreadStackSize,
								PRIO_NORMAL,
								"[RAGE] netTcp Worker",
								cpuAffinity, 
								"RageNetTcpWorker");
	}
}

void 
netTcp::ShutdownWorker()
{
	//Signal the threads to stop
	s_TcpThreadExit = true;

	if(s_ThreadId != sysIpcThreadIdInvalid)
	{
		//Unblock the thread
		WakeupWorker();

		//Wait for it to stop
		sysIpcWaitThreadExit(s_ThreadId);

		s_ThreadId = sysIpcThreadIdInvalid;
	}

	s_TcpThreadExit = false;
}

bool
netTcp::WakeupWorker()
{
	PROFILE

    bool success = false;

    if(netVerify(sysIpcThreadIdInvalid != s_ThreadId))
    {
        sysIpcSetEvent(s_WakeupEvent);
        success = true;
    }

    return success;
}

void
netTcp::WorkerUpdate(void* /*p*/)
{
    PROFILER_THREAD("netTcpWorker", 1);

    while(!s_TcpThreadExit)
    {
        //Sleep until we have work to do.
        sysIpcWaitEvent(s_WakeupEvent);

		//Check if exit flag was set while we were waiting
		if(s_TcpThreadExit)
		{
			continue;
		}

        netTcp::Update();
    }
}

//////////////////////////////////////////////////////////////////////////
//  Functions for managing socket infos.
//////////////////////////////////////////////////////////////////////////

int
netTcp::AllocSocketInfo(const char* OUTPUT_ONLY(contextStr), ConnectReason connectReason)
{
	PROFILE

    SYS_CS_SYNC(s_CsMain);

    for(int sktId = 0; sktId < MAX_SOCKET_INFOS; ++sktId)
    {
        if(!s_SocketInfos[sktId].m_IsAllocated)
        {
            netAssert(NULL == s_SocketInfos[sktId].m_Ssl);
            netAssert(!NET_IS_VALID_SOCKET_FD(s_SocketInfos[sktId].m_SktFd));
			netAssert(false == s_SocketInfos[sktId].m_Disconnected);
			netAssert(false == s_SocketInfos[sktId].m_CloseFdPending);
			OUTPUT_ONLY(safecpy(s_SocketInfos[sktId].m_ContextStr, contextStr);)
            s_SocketInfos[sktId].m_IsAllocated = true;
            s_SocketInfos[sktId].m_ConnectReason = connectReason;
            s_SocketInfos[sktId].m_BytesSent = 0;
            s_SocketInfos[sktId].m_BytesSentOnWire = 0;
            return sktId;
        }
    }

    return NET_TCP_SOCKET_ID_INVALID;
}

void
netTcp::CancelOpsBySocketId(const int sktId, AsyncOpList& opList)
{
	PROFILE

	AsyncOpList::iterator it = opList.begin();
	AsyncOpList::iterator next = it;
	AsyncOpList::const_iterator stop = opList.end();
	netTcpAsyncOp* op = NULL;

	for(++next; stop != it; it = next, ++next)
	{
		if((*it)->m_SktId == sktId)
		{
			netDebug2("netTcp::CancelOpsBySocketId() cancelling op for deallocated socket");

			op = *it;
			netTcp::CompleteOp(op->m_Id, netStatus::NET_STATUS_FAILED, NETTCP_RESULT_TIMED_OUT);
		}
	}
}

void
netTcp::FreeSocketInfo(const int sktId)
{
	PROFILE

    SYS_CS_SYNC(s_CsMain);

    if(netVerify(sktId >= 0)
        && netVerify(sktId < MAX_SOCKET_INFOS))
    {
        netTcpSocketInfo* si = &s_SocketInfos[sktId];

        if(si->m_IsAllocated)
        {
			//Ensure we don't cleanup this skt a second time (m_SktFd is still set while m_CloseFdPending is true)
			if(!si->m_CloseFdPending)
			{
				if(si->m_Ssl)
				{
					wolfSSL_shutdown(si->m_Ssl);
					wolfSSL_free(si->m_Ssl);
					si->m_Ssl = NULL;
				}

				if(NET_IS_VALID_SOCKET_FD(si->m_SktFd))
				{
					//We have to defer cleanup of the skt fd to netTcp::Update since it may
					//currently be using the fd in a select
					si->m_CloseFdPending = true;
					//Signal the worker to wake up so it can close our socket
					netVerify(netTcp::WakeupWorker());
				}
				else
				{
					si->m_SktFd = NET_INVALID_SOCKET_FD;
					si->m_Disconnected = false;
					si->m_IsAllocated = false;
				}

				// cancel any ops for this socket (from both main and pending lists)
				CancelOpsBySocketId(sktId, sm_OpList);

				{
					rage::sysCriticalSection csPendingOpsList(s_CsPendingOpsList);
					CancelOpsBySocketId(sktId, sm_PendingOpList);
				}
			}
        }
        else
        {
            netAssert(NULL == si->m_Ssl);
            netAssert(!NET_IS_VALID_SOCKET_FD(si->m_SktFd));
        }
    }
}

u32
netTcp::GetBytesSent(const int sktId)
{
    if (netVerify(sktId >= 0)
        && netVerify(sktId < MAX_SOCKET_INFOS))
    {
        return s_SocketInfos[sktId].m_BytesSent;
    }

    return 0;
}

u32
netTcp::GetBytesSentOnWire(const int sktId)
{
    if (netVerify(sktId >= 0)
        && netVerify(sktId < MAX_SOCKET_INFOS))
    {
        return s_SocketInfos[sktId].m_BytesSentOnWire;
    }

    return 0;
}

#if !__NO_OUTPUT
char* 
netTcp::GetSocketContextStr(const int sktId)
{
	if(netVerify(sktId >= 0)
		&& netVerify(sktId < MAX_SOCKET_INFOS))
	{
		return s_SocketInfos[sktId].m_ContextStr;
	}

	return NULL;
}
#endif // !__NO_OUTPUT

#if ENABLE_BANDWIDTH_TELEMETRY
u32
netTcp::GetTotalBytesSent()
{
    return s_TcpStats.m_Total.m_Sent;
}

u32
netTcp::GetTotalBytesSentOnWire()
{
    return s_TcpStats.m_Total.m_SentOnWire;
}

u32
netTcp::GetTotalBytesReceived()
{
    return s_TcpStats.m_Total.m_Received;
}
#endif // ENABLE_BANDWIDTH_TELEMETRY

#if ENABLE_BANDWIDTH_TRACKING
u32
netTcp::GetTotalBytesSentByConnectReason(ConnectReason connectReason)
{
    return s_TcpStats.m_ByConnectReason[connectReason].m_Sent;
}

u32
netTcp::GetTotalBytesSentOnWireByConnectReason(ConnectReason connectReason)
{
    return s_TcpStats.m_ByConnectReason[connectReason].m_SentOnWire;
}

u32
netTcp::GetTotalBytesReceivedByConnectReason(ConnectReason connectReason)
{
    return s_TcpStats.m_ByConnectReason[connectReason].m_Received;
}
#endif // ENABLE_BANDWIDTH_TELEMETRY

netTcpSocketInfo*
netTcp::GetSocketInfo(const int sktId)
{
    if(netVerify(sktId >= 0)
        && netVerify(sktId < MAX_SOCKET_INFOS))
    {
        return &s_SocketInfos[sktId];
    }

    return NULL;
}

bool
netTcp::IsValidSocketInfo(const int sktId)
{
    const netTcpSocketInfo* si = GetSocketInfo(sktId);

    return (si && NET_IS_VALID_SOCKET_FD(si->m_SktFd));
}

netSocketFd
netTcp::GetSocketFd(const int sktId)
{
    const netTcpSocketInfo* si = GetSocketInfo(sktId);

    return si ? si->m_SktFd : NET_INVALID_SOCKET_FD;
}

#if ENABLE_TCP_PROXY
void
netTcp::FormatProxyHeader(char(&buf)[512], const netSocketAddress& addr, const char* domainName)
{
    char addrStr[netSocketAddress::MAX_STRING_BUF_SIZE];
    addr.Format(addrStr);

    if (domainName != nullptr && domainName[0] != 0)
    {
        u16 port = addr.GetPort();
        formatf(buf, "CONNECT %s:%u HTTP/1.1\r\nHost: %s:%u\r\n\r\n", domainName, port, domainName, port);
    }
    else
    {
        formatf(buf, "CONNECT %s HTTP/1.1\r\nHost: %s\r\n\r\n", addrStr, addrStr);
    }
}
#endif

//Memory routines for libraries that would otherwise use malloc/calloc/realloc/free.
//These are called externally through libs such as wolfssl, libpcp, miniupnp.
//heap is always a sysMemAllocator or nullptr. wolfssl can specify an allocator when initializing an SSL_CTX.
//Note that wolfssl is bad about actually routing allocations through the specified heap, so some
//allocations related to the context may still be made with a null heap pointer.
//TODO: NS - these functions could be moved to a more appropriate location

extern "C" void* XMALLOC(size_t size, void* heap, int UNUSED_PARAM(type))
{
    sysMemAllocator* allocator = heap ? (sysMemAllocator*)heap : sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
    return allocator->RAGE_LOG_ALLOCATE(size, sizeof(void*));
}

extern "C" void* XCALLOC(size_t num, size_t size, void* heap, int type)
{
    // the allocation of a zero-initialized memory block of (num * size) bytes
    const size_t allocSize = num * size;
    void* ptr = XMALLOC(allocSize, heap, type);

    if(ptr)
    {
        sysMemSet(ptr, 0, allocSize);
    }

    return ptr;
}

extern "C" void XFREE(void* ptr, void* heap, int UNUSED_PARAM(type))
{
    if(ptr)
    {
        sysMemAllocator* allocator = heap ? (sysMemAllocator*)heap : sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
        allocator->Free(ptr);
    }
}

extern "C" void* XREALLOC(void* ptr, size_t size, void* heap, int type)
{
    // if size is 0, realloc() behaves like free() and NULL is returned
    if(size == 0)
    {
        XFREE(ptr, heap, type);
        return NULL;
    }

    void* newPtr = XMALLOC(size, heap, type);

    if(newPtr)
    {
        // if ptr is null, realloc() behaves like malloc()
        if(ptr)
        {
            const sysMemAllocator* allocator = heap ? (sysMemAllocator*)heap : sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
            const size_t origSize = allocator->GetSize(ptr);
            Assert(origSize > 0);
            sysMemCpy(newPtr, ptr, Min<size_t>(origSize, size));
            XFREE(ptr, heap, type);
        }
        return newPtr;
    }
    else
    {
        return NULL;
    }
}

// Restore default channel output formatting
#if !__NO_OUTPUT
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT RAGE_LOG_FMT_DEFAULT
#endif

#ifdef wolfSSL_read
#undef wolfSSL_read
#endif

#ifdef wolfSSL_write
#undef wolfSSL_write
#endif

#if NET_WOLFSSL_DEBUG
__THREAD netWolfSslHelper::netWolfSslLoggingCb s_WolfSslLoggingCallback = nullptr;

void netWolfSslHelper::Init()
{
    wolfSSL_SetLoggingCb(netWolfSslHelper::SslLogCb);
    wolfSSL_Debugging_ON();
}

void netWolfSslHelper::SetSslLoggingCb(netWolfSslLoggingCb callback)
{
    s_WolfSslLoggingCallback = callback;
}

netWolfSslHelper::netWolfSslLoggingCb netWolfSslHelper::GetSslLoggingCb()
{
    return s_WolfSslLoggingCallback;
}

void netWolfSslHelper::SslLogCb(const int logLevel, const char* const logMessage)
{
    // This is the global Ssl logging callback registered with the wolfSSL lib.
    // Systems that use SSL/DTLS can route logging messages to their own callbacks by calling
    // SetSslLoggingCb prior to calling a wolfSSL function.

    // wolfSSL doesn't always null-terminate the string
    char msg[WOLFSSL_MAX_ERROR_SZ + 1];
    safecpy(msg, logMessage);

    if(s_WolfSslLoggingCallback)
    {
        s_WolfSslLoggingCallback(logLevel, msg);
        return;
    }

    if(ERROR_LOG == logLevel)
    {
        netDebug1("wolfSSL: %s", msg);
    }
    else if(INFO_LOG == logLevel
        || OTHER_LOG == logLevel)
    {
        netDebug3("wolfSSL: %s", msg);
    }
}
#endif

#if RSG_OUTPUT
void
netTcp::PrintSocketInfo(const netSocketFd SCE_ONLY(sktFd))
{
#if RSG_SCE
    if (Channel_ragenet_tcp.MaxLevel >= DIAG_SEVERITY_DEBUG2)
    {
        SceNetSockInfo info = { 0 };
        sceNetGetSockInfo(sktFd, &info, 1, 0);

        netDebug2("Socket info: name[%s] socket_type[%d] policy[%d] priority[%d] recv_queue_length[%d] send_queue_length[%d] state[%d] flags[%d] " \
            "tx_bps[%d] rx_bps[%d] max_tx_bps[%d] max_rx_bps[%d] tx_vbps[%d] rx_vbps[%d] recv_buffer_size[%d] send_buffer_size[%d] tx_drops[%d] rx_drops[%d] tx_wait[%d]",
            info.name, info.socket_type, info.policy, info.priority, info.recv_queue_length, info.send_queue_length, info.state, info.flags,
            info.tx_bps, info.rx_bps, info.max_tx_bps, info.max_rx_bps, info.tx_vbps, info.rx_vbps, info.recv_buffer_size, info.send_buffer_size, info.tx_drops, info.rx_drops, info.tx_wait);
    }
#endif
}
#endif //RSG_OUTPUT

#if RSG_BANK
void
netTcp::AddWidgets()
{
    bkBank *pBank = BANKMGR.FindBank("Network");
    pBank = pBank ? pBank : &BANKMGR.CreateBank("Network");
    if (!pBank) { return; }

    pBank->PushGroup("Tcp", false);

    pBank->AddText("Recv Buffer size (0==use default)", (int*)&s_TcpSettings.m_SoRcvBufferSize);
    pBank->AddText("Read loops", (int*)&s_TcpSettings.m_MaxReadLoops);
    pBank->AddText("Thread yield time ms", (int*)&s_TcpSettings.m_ThreadYieldTimeMs);
    pBank->AddText("Max processing time ms", (int*)&s_TcpSettings.m_MaxProcessingTimeMs);

    pBank->PopGroup();
}
#endif

}; //namespace rage
