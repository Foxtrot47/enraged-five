// 
// net/nethardware.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_NETHARDWARE_H
#define NET_NETHARDWARE_H

#include "data/autoid.h"
#include "netsocket.h"
#include "atl/delegate.h"
#include "atl/inlist.h"
#include "system/criticalsection.h"

namespace rage
{

enum netHardwareEventType
{   
	NET_HARDWARE_EVENT_NETWORK_AVAILABILITY_CHANGED,
	NET_HARDWARE_EVENT_NETWORK_SOCKET_BINDING_CHANGED,
	NET_HARDWARE_EVENT_NETWORK_REACHABILITY_CHANGED,		// mobile: network reachable through WWAN or WiFi changed event
};

#define NET_HARDWARE_EVENT_COMMON_DECL( name )\
	static unsigned EVENT_ID() { return name::GetAutoId(); }\
	virtual unsigned GetId() const { return name::GetAutoId(); }

#define NET_HARDWARE_EVENT_DECL( name, id )\
	AUTOID_DECL_ID( name, rage::netHardwareEvent, id )\
	NET_HARDWARE_EVENT_COMMON_DECL( name )

class netHardwareEvent
{
public:
	AUTOID_DECL_ROOT(netHardwareEvent);

	NET_HARDWARE_EVENT_COMMON_DECL(netHardwareEvent);

	netHardwareEvent() {}
	virtual ~netHardwareEvent() {}
};

class netHardwareEventNetworkAvailabilityChanged : public netHardwareEvent
{
public:
	NET_HARDWARE_EVENT_DECL(netHardwareEventNetworkAvailabilityChanged, NET_HARDWARE_EVENT_NETWORK_AVAILABILITY_CHANGED);

	netHardwareEventNetworkAvailabilityChanged(const bool available)
		: m_Available(available)
	{

	}

	bool m_Available;
};

class netHardwareEventSocketBindingChanged : public netHardwareEvent
{
public:
	NET_HARDWARE_EVENT_DECL(netHardwareEventSocketBindingChanged, NET_HARDWARE_EVENT_NETWORK_SOCKET_BINDING_CHANGED);

	netHardwareEventSocketBindingChanged(const netSocket* skt)
		: m_Skt(skt)
	{

	}

	const netSocket* m_Skt;
};

enum netHardwareReachability
{
	REACHABILITY_NONE,
	REACHABILITY_VIA_WIFI,	// wi-fi
	REACHABILITY_VIA_WWAN,	// cellular
	REACHABILITY_VIA_WIRED,	// wired
};

class netHardwareEventReachabilityChanged : public netHardwareEvent
{
public:
	NET_HARDWARE_EVENT_DECL(netHardwareEventReachabilityChanged, NET_HARDWARE_EVENT_NETWORK_REACHABILITY_CHANGED);

	netHardwareEventReachabilityChanged(const netHardwareReachability type) : m_Reachability(type) {}

	const netHardwareReachability m_Reachability;
};

//PURPOSE
//  This class abstracts hardware-specific aspects of networking.
//  It manages a collection of netSockets, which are used to actually
//  send and receive data on the network.
//
//  Sockets may be created at any time after instantiating netHardware,
//  but cannot be used until the hardware is available, i.e. IsAvailable()
//  returns true.
//
//  If the hardware transitions from available to any other state then all
//  active sockets will become unusable.  Attempts to send/receive on these
//  sockets will fail.  If the network again becomes available all sockets
//  will once again become usable.
class netHardware
{
	typedef atDelegator<void(const netHardwareEvent& event)> Delegator;

public:
	typedef Delegator::Delegate Delegate;

    static bool Init();

#if !__FINAL
	// this is used to initialise the library outside of our expected flow
	// marked separately so that we can exclude this method from our double initialise checks
	static bool InitDebug();
#endif

    static void Shutdown();

    //PURPOSE
    //  Returns the local NAT type.
    //NOTES
    //  The NAT might not be immediately available upon boot as it takes
    //  some time to discover.
    static netNatType GetNatType();

#if !__NO_OUTPUT
	static const char* GetNatTypeString(netNatType natType);
#endif

    //PURPOSE
    //  Retrieves the local MAC address.
    static bool GetMacAddress(u8 (&mac)[6]);

    //PURPOSE
    //  Retrieves the first IP address from the first detected
    //  network card.
    static bool GetLocalIpAddress(netIpAddress* ip);

    //PURPOSE
    //  Retrieves the public IP address for the host, if available.
    //NOTES
    //  The IP might not be immediately available upon boot as it takes
    //  some time to discover.
    static bool GetPublicIpAddress(netIpAddress* ip);

	//PURPOSE
	//  Retrieves the network device (wired, wi-fi, cellular)
	static netHardwareReachability GetReachability();

	//PURPOSE
	//  Sets the public IP address.
	static void SetPublicIpAddress(const netIpAddress& ip);

	//PURPOSE
	//  Retrieves the MTU (Maximum Transmission Unit) in bytes.
	//RETURNS
	//  True if the MTU was retrieved, false otherwise.
	static bool GetMtu(u32& mtu);

    //PURPOSE
    //  Returns true if the system is "online", i.e. can communicate
    //  over the Internet.
    static bool IsOnline();

    //PURPOSE
    //  Returns the connection state of the link (Ethernet cable).
    //RETURNS
    //  True if the link is connected; false otherwise (e.g. if cable unplugged).
    static bool IsLinkConnected();

    //PURPOSE
    //  Convenience function that returns true if the network is available.
    static bool IsAvailable();

	//PURPOSE
	//  Convenience function that returns true if the network is initialized.
	static bool IsInitialized();

    //PURPOSE
    //  Updates the state of the network hardware.  Should be called as
    //  often as possible (at least once per frame).
    static void Update();

	//PURPOSE
	//  Add/remove a delegate that will be called with event notifications.
	static void AddDelegate(netHardware::Delegate* dlgt);
	static void RemoveDelegate(netHardware::Delegate* dlgt);
	static void DispatchEvent(netHardwareEvent* e);

    //PURPOSE
    //  Creates and initializes a netSocket instance.  The socket cannot
    //  be used until the network becomes available.
    //PARAMS
    //  socket  - Socket instance.
    //  port    - Local port in host byte order to which socket will be bound.
    //            Pass zero to bind to a random port.
    //  proto   - Network protocol.
    //  blockingType    - Blocking or non-blocking.
	//  addressFamily - the address family to use when creating the socket.
	//  sendBufferSize - (Optional) the desired size, in bytes, of the send buffer
	//  receiveBufferSize - (Optional) the desired size, in bytes, of the receive buffer
	//NOTES
	//  If sendBufferSize is 0, the current/default send buffer size will be used.
	//  If receiveBufferSize is 0, the current/default receive buffer size will be used.
    //RETURNS
    //  True on success.
    static bool CreateSocket(netSocket* socket,
                            const unsigned short port,
                            const netProtocol proto,
                            const netSocketBlockingType blockingType,
                            const netAddressFamily addressFamily = NET_DEFAULT_ADDRESS_FAMILY,
							const unsigned sendBufferSize = 0,
							const unsigned receiveBufferSize = 0);

    //PURPOSE
    //  Destroys a netSocket that was created by CreateSocket().
    //PARAMS
    //  socket      - the socket to destroy
    //NOTES
    static void DestroySocket(netSocket* socket);

	//PURPOSE
	//	Net socket code notifies netHardware when a socket error occurs due to
	//	a released IP address, such that netHardware can perform a reset.
	//NOTES
	//  This is built specifically for PS4, but could be used for other platforms
	// if notifications are sent using the same method.
	static void NotifyIpReleaseSocketError();
	static bool HasIpReleaseSocketError();
	static void ConsumeIpReleaseSocketError();

	typedef inlist<netSocket, &netSocket::m_ListLink> SocketList;

#if __BANK
	static SocketList& GetActiveSockets() { return sm_ActiveSockets; }
#endif

#if RSG_SCE
#if !__NO_OUTPUT
	static void DumpNetIfConfig();

	struct rlSceNetStatisticsInfo
	{
		int m_KernelMemFreeSize;
		int m_KernelMemFreeMin;
		int m_PacketCount;
		int m_PacketQosCount;
		int m_LibNetFreeSize;
		int m_LibNetFreeMin;
	};
	static void GetNetStatisticsInfo(rlSceNetStatisticsInfo& info);
#endif // !__NO_OUTPUT
#endif // RSG_ORBIS

#if RSG_BANK
	static void CreateBankWidget();
#endif // RSG_BANK
private:

    netHardware();
    netHardware(const netHardware&);
    netHardware& operator=(const netHardware&);

    enum State
    {
        STATE_STOPPED,
        STATE_STARTING,
        STATE_AVAILABLE
    };

    struct NetworkInfo
    {
        enum Flags
        {
            NETINFO_LOCAL_IP    = 0x01,
            NETINFO_PUBLIC_IP   = 0x02,
            NETINFO_MAC_ADDR    = 0x04,
        };

        NetworkInfo()
            : m_Flags(0)
            , m_NatType(NET_NAT_UNKNOWN)
            , m_LocalIp()
            , m_PublicIp()
        {
        }

        void Clear()
        {
            m_Flags = 0;
            m_NatType = NET_NAT_UNKNOWN;
            m_LocalIp.Clear();
            m_PublicIp.Clear();
        }

        void SetOffline()
        {
            m_Flags &= ~(NETINFO_PUBLIC_IP);
			m_PublicIp.Clear();
            m_NatType = NET_NAT_UNKNOWN;
        }

        unsigned m_Flags;

        netNatType m_NatType;
        netIpAddress m_LocalIp;
        netIpAddress m_PublicIp;
        u8 m_MacAddress[6];
    };

    static void SetOnlineState(const bool isOnline);

    static void GetNetworkInfo();

    static bool NativeInit();

    static void NativeShutdown();

    static bool NativeStartNetwork();

    static bool NativeStopNetwork();

    static void NativeUpdate();

    //PURPOSE
    //  Returns the local NAT type.
    //NOTES
    //  The NAT might not be immediately available upon boot as it takes
    //  some time to discover.
    static netNatType NativeGetNatType();

    //PURPOSE
    //  Retrieves the local MAC address.
    static bool NativeGetMacAddress(u8 (&mac)[6]);

    //PURPOSE
    //  Retrieves the first IP address from the first detected
    //  network card.
    static bool NativeGetLocalIpAddress(netIpAddress* ip);

    //PURPOSE
    //  Retrieves the public IP address for the host, if available.
    //NOTES
    //  The IP might not be immediately available upon boot as it takes
    //  some time to discover.
    static bool NativeGetPublicIpAddress(netIpAddress* ip);

	//PURPOSE
	//  Retrieves the MTU (Maximum Transmission Unit) in bytes.
	//RETURNS
	//  True if the MTU was retrieved, false otherwise.
	static bool NativeGetMtu(u32& mtu);

    static SocketList sm_ActiveSockets;

    static sysCriticalSectionToken sm_Cs;

	static netHardware::Delegator sm_Delegator;

    static NetworkInfo sm_NetInfo;

    static State sm_State;

	static bool sm_bIpReleaseSocketError;
};

}   // namespace rage

#endif
