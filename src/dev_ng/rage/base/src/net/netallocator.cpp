//
// net/netallocator.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#include "netallocator.h"
#include "netdiag.h"
#include "diag/seh.h"
#include "system/fixedallocator.h"
#include "system/timer.h"

#define NET_ALLOCATION_PROFILING_FULL (__BANK)
#define NET_ALLOCATION_PROFILING_MINIMAL (1 || NET_ALLOCATION_PROFILING_FULL)

#if NET_ALLOCATION_PROFILING_FULL
#define NET_ALLOCATION_PROFILING_FULL_ONLY(...) __VA_ARGS__
#else
#define NET_ALLOCATION_PROFILING_FULL_ONLY(...)
#endif

#if NET_ALLOCATION_PROFILING_FULL
#include "file/asset.h"
#include "file/limits.h"
#include "file/stream.h"
#include "net/nethardware.h"

// how frequently to output allocation stats to tty/log
static const unsigned NET_ALLOCATION_PROFILING_FLUSH_INTERVAL_MS = (3 * 60 * 1000);
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, memallocator)
#undef __rage_channel
#define __rage_channel ragenet_memallocator

XPARAM(processinstance);
PARAM(netMemAllocatorLogging, "[network] Outputs a log containing the stream of allocations and deallocations. "
							  "Useful for getting a histogram of allocation sizes for the fixed allocator."
							  "You can supply a path to a folder (eg. -netMemAllocatorLogging=x:\\netallocator\\). "
							  "If no folder is specifed, the files will be stored in the current working directory", "Disabled", "All", "");

#if NET_ALLOCATION_PROFILING_MINIMAL
//////////////////////////////////////////////////////////////////////////
// netAllocationProfiler
//////////////////////////////////////////////////////////////////////////
class netAllocationProfiler
{
public:
	utimer_t startAllocTime;
	utimer_t totalAllocTime;


	unsigned numFixedHeapAllocations;
	unsigned numSimpleHeapAllocations;
	unsigned numFixedHeapMisses;
	unsigned numFailedAllocations;
	size_t totalMem;
	size_t totalMemWithOverhead;

#if NET_ALLOCATION_PROFILING_FULL
	utimer_t startTimeDealloc;
	utimer_t totalDeallocTime;
	unsigned lastAllocStatsDumpTime;
	fiStream* m_Stream;
#endif

	netAllocationProfiler()
	{
		startAllocTime = 0;
		totalAllocTime = 0;

		numFixedHeapAllocations = 0;
		numSimpleHeapAllocations = 0;
		numFixedHeapMisses = 0;
		numFailedAllocations = 0;
		totalMem = 0;
		totalMemWithOverhead = 0;

#if NET_ALLOCATION_PROFILING_FULL
		startTimeDealloc = 0;
		totalDeallocTime = 0;
		lastAllocStatsDumpTime = 0;
		m_Stream = NULL;
#endif
	}

#if NET_ALLOCATION_PROFILING_FULL
	void Init()
	{
		const char* folder = NULL;
		if(PARAM_netMemAllocatorLogging.Get(folder))
		{
			if(!m_Stream)
			{
				netIpAddress ip;
				netHardware::GetLocalIpAddress(&ip);
				char ipStr[netIpAddress::MAX_STRING_BUF_SIZE];
				ip.Format(ipStr);

				u32 processInstance = 0;
#if RSG_PC
				// our socket isn't necessarily bound to a port yet, so use -processinstance
				// to differentiate multiple instances on the same PC instead.
				PARAM_processinstance.Get(processInstance);
#endif

				char path[RAGE_MAX_PATH];
				if(folder && (folder[0] != '\0'))
				{
					ASSET.CreateLeadingPath(folder);
					formatf(path, "%s\\net_mem_allocator_%s_%u.log", folder, ipStr, processInstance);
				}
				else
				{
					formatf(path, "net_mem_allocator_%s_%u.log", ipStr, processInstance);
				}

				m_Stream = fiStream::Create(path);
			}
		}
	}

	void LogLine(const char* line)
	{
		if(m_Stream)
		{
			m_Stream->Write(line, (int)strlen(line));
		}
	}

	const char* GetReasonString(netMemAllocator::AllocReason reason)
	{
		switch(reason)
		{
		case netMemAllocator::ALLOC_OTHER:
			return "ALLOC_OTHER";
			break;
		case netMemAllocator::ALLOC_CXN_MGR_OUTFRM_UNRELIABLE:
			return "ALLOC_CXN_MGR_OUTFRM_UNRELIABLE";
			break;
		case netMemAllocator::ALLOC_CXN_MGR_OUTFRM_RELIABLE:
			return "ALLOC_CXN_MGR_OUTFRM_RELIABLE";
			break;
		case netMemAllocator::ALLOC_CXN_OUTFRM_UNRELIABLE:
			return "ALLOC_CXN_OUTFRM_UNRELIABLE";
			break;
		case netMemAllocator::ALLOC_CXN_OUTFRM_RELIABLE:
			return "ALLOC_CXN_OUTFRM_RELIABLE";
			break;
		case netMemAllocator::ALLOC_CXN_INFRM:
			return "ALLOC_CXN_INFRM";
			break;
		case netMemAllocator::ALLOC_CXN_MGR_EVENT:
			return "ALLOC_CXN_MGR_EVENT";
			break;
		case netMemAllocator::ALLOC_CXNLESS_BUNDLE:
			return "ALLOC_CXNLESS_BUNDLE";
			break;
		case netMemAllocator::ALLOC_OOB_FRM_RCVD:
			return "ALLOC_OOB_FRM_RCVD";
			break;
		}

		return "UNKNOWN_REASON";
	}

	void LogLine(const bool alloc, netMemAllocator::AllocReason reason, const unsigned size, const void* ptr)
	{
		if(m_Stream)
		{
			char line[1024];
			unsigned curTime = sysTimer::GetSystemMsTime();
			formatf(line, "%u,%s,%u,%p,%s\r\n", curTime, alloc ? "alloc" : "dealloc", size, ptr, alloc ? GetReasonString(reason) : "");
			LogLine(line);
		}
	}

	void Update(netMemAllocator* allocator)
	{
		if(lastAllocStatsDumpTime == 0)
		{
			lastAllocStatsDumpTime = sysTimer::GetSystemMsTime();
		}

		unsigned elapsedTimeSinceLastStatsDump = sysTimer::GetSystemMsTime() - lastAllocStatsDumpTime;

		if(elapsedTimeSinceLastStatsDump >= NET_ALLOCATION_PROFILING_FLUSH_INTERVAL_MS)
		{
			PrintStats(allocator);
			lastAllocStatsDumpTime = sysTimer::GetSystemMsTime();
		}
	}
#endif //NET_ALLOCATION_PROFILING_FULL

	void StartAlloc(netMemAllocator* /*allocator*/)
	{
		startAllocTime = sysTimer::GetTicks();
	}

	void EndAlloc(netMemAllocator* allocator, NET_ALLOCATION_PROFILING_FULL_ONLY( netMemAllocator::AllocReason reason,) const size_t size, const void* ptr, const bool fixedAllocation)
	{
		totalAllocTime += (sysTimer::GetTicks() - startAllocTime);
		totalMem += size;

		if(fixedAllocation)
		{
			numFixedHeapAllocations++;

			totalMemWithOverhead += allocator->m_FixedAllocator->GetSizeWithOverhead(ptr);
		}
		else
		{
			numSimpleHeapAllocations++;

			totalMemWithOverhead += allocator->m_SimpleAllocator.GetSizeWithOverhead(ptr);

			if(size <= allocator->m_MaxFixedChunkSize)
			{
				numFixedHeapMisses++;
			}
		}

		if(!ptr)
		{
			numFailedAllocations++;
		}

#if NET_ALLOCATION_PROFILING_FULL
		LogLine(true, reason, (unsigned)size, ptr);

		Update(allocator);
#endif //NET_ALLOCATION_PROFILING_FULL
	}

#if NET_ALLOCATION_PROFILING_FULL
	void StartDealloc(netMemAllocator* /*allocator*/, const void* ptr)
	{
		LogLine(false, netMemAllocator::ALLOC_OTHER, 0, ptr);

		startTimeDealloc = sysTimer::GetTicks();
	}

	void EndDealloc(netMemAllocator* /*allocator*/)
	{
		totalDeallocTime += (sysTimer::GetTicks() - startTimeDealloc);
	}

	void PrintStats(netMemAllocator* allocator)
	{
		unsigned numAllocations = numFixedHeapAllocations + numSimpleHeapAllocations;
		float allocTime = sysTimer::GetTicksToMicroseconds() * totalAllocTime;
		float avgAllocTime = (numAllocations > 0) ? (allocTime / 1000.0f / numAllocations) : 0;
		float deallocTime = sysTimer::GetTicksToMicroseconds() * totalDeallocTime;
		float avgDeallocTime = (numAllocations > 0) ? (deallocTime / 1000.0f / numAllocations) : 0;
		float fixedHeapAllocationPct = (numAllocations > 0) ? ((float)numFixedHeapAllocations / (float)numAllocations) * 100.0f : 0;
		float simpleHeapAllocationPct = (numAllocations > 0) ? ((float)numSimpleHeapAllocations / (float)numAllocations) * 100.0f : 0;
		float efficiency = GetEfficiency();

		netDebug("total allocations: %u, "
		         "total mem allocated: %u, "
		         "efficiency: (%.2f%%), "
		         "total fixed heap allocations: %u (%.2f%%), "
		         "total simple heap allocations: %u (%.2f%%), "
		         "total fixed heap misses: %u, "
		         "total failed allocations: %u, "
		         "avg time to alloc: (%.4f ms), "
		         "avg time to dealloc: (%.4f ms), "
		         "total allocation time: (%.2f ms), "
		         "total deallocation time: (%.2f ms)",
		         numAllocations,
		         (unsigned)totalMem,
		         efficiency,
		         numFixedHeapAllocations,
		         fixedHeapAllocationPct,
		         numSimpleHeapAllocations,
		         simpleHeapAllocationPct,
		         numFixedHeapMisses,
		         numFailedAllocations,
		         avgAllocTime,
		         avgDeallocTime,
		         allocTime / 1000.0f,
		         deallocTime / 1000.0f);

		allocator->PrintBasicMemoryUsage();
	}
#endif //NET_ALLOCATION_PROFILING_FULL

	float GetEfficiency() const
	{
		return (totalMemWithOverhead > 0) ? ((float)totalMem / (float)totalMemWithOverhead) * 100.0f : 0.0f;
	}
};

static netAllocationProfiler sm_AllocProfiler;
#endif //NET_ALLOCATION_PROFILING_MINIMAL

netMemAllocator::netMemAllocator(void* heap,
                                 const int heapSize,
                                 const int heapId,
                                 const int numFixedBuckets,
                                 const size_t fixedChunkSizes[],
                                 const u8 fixedChunkCounts[])
	: m_SimpleAllocator(heap, heapSize, heapId, false)
	, m_FixedAllocator(NULL)
	, m_FixedAllocatorHeap(NULL)
	, m_MaxFixedChunkSize(0)
	, m_QuitOnFailure(false)
{
#if NET_ALLOCATION_PROFILING_FULL
	sm_AllocProfiler.Init();
#endif

	m_FixedHeapSize = 0;

	rtry
	{
		m_SimpleAllocator.SetQuitOnFail(m_QuitOnFailure);

		rcheck(numFixedBuckets > 0, catchall,);
		rverify(numFixedBuckets <= MAX_FIXED_BUCKETS, catchall, );
		
		rcheck(fixedChunkSizes, catchall,);
		rcheck(fixedChunkCounts, catchall,);

		m_FixedAllocator = (netMemFixedAllocator*)m_SimpleAllocator.Allocate(sizeof(netMemFixedAllocator), 16);
		rverify(m_FixedAllocator, catchall, netError("Failed to allocate %u bytes for fixed allocator", (unsigned)sizeof(netMemFixedAllocator)));

		unsigned lastChunkSize = 0;
		for(unsigned i = 0; i < (unsigned)numFixedBuckets; ++i)
		{
			unsigned count = fixedChunkCounts[i];
			unsigned size = (unsigned)fixedChunkSizes[i];
			rverify((size % 16) == 0, catchall, netError("Chunk sizes must be a multiple of 16 so we can support 16 byte aligned allocs."));
			rverify(count > 0, catchall,);
			rverify(size > 0, catchall,);
			rverify(size > lastChunkSize, catchall, netError("fixedChunkSizes array must be in increasing order"));
			rverify(count <= MAX_FIXED_CHUNKS_PER_BUCKET, catchall,);
			m_FixedHeapSize += count * size;
			lastChunkSize = size;
		}
		rverify(m_FixedHeapSize < (unsigned)heapSize, catchall,);

		m_FixedAllocatorHeap = m_SimpleAllocator.Allocate(m_FixedHeapSize, 16);
		rverify(m_FixedAllocatorHeap, catchall, netError("Failed to allocate %u bytes for fixed allocator", m_FixedHeapSize));
		new(m_FixedAllocator) netMemFixedAllocator(m_FixedAllocatorHeap, numFixedBuckets, fixedChunkSizes, fixedChunkCounts);
		m_FixedAllocator->SetAllowCascading(true);
		m_MaxFixedChunkSize = (unsigned)fixedChunkSizes[numFixedBuckets - 1];
	}
	rcatchall
	{
		if(m_FixedAllocatorHeap)
		{
			m_SimpleAllocator.Free(m_FixedAllocatorHeap);
			m_FixedAllocatorHeap = NULL;
		}

		if(m_FixedAllocator)
		{
			m_SimpleAllocator.Free(m_FixedAllocator);
			m_FixedAllocator = NULL;
		}

		m_MaxFixedChunkSize = 0;
		m_FixedHeapSize = 0;
	}

#if !__NO_OUTPUT 
	m_LeastMemoryAvailable = GetMemoryAvailable();
#endif

	netDebug("netAllocator total heap size: %u bytes, fixed allocator heap size: %u, simple heap size: %u",
			heapSize, m_FixedHeapSize, (unsigned)m_SimpleAllocator.GetMemoryAvailable());

	netDebug("Used: %d, Available: %d, Largest Block: %d",
		static_cast<int>(this->GetMemoryUsed(-1)),
		static_cast<int>(this->GetMemoryAvailable()),
		static_cast<int>(this->GetLargestAvailableBlock()));

	if(m_FixedAllocator)
	{
		m_FixedAllocator->DumpStats();
	}
}

netMemAllocator::~netMemAllocator()
{
	if(m_FixedAllocatorHeap)
	{
		m_SimpleAllocator.Free(m_FixedAllocatorHeap);
		m_FixedAllocatorHeap = NULL;
	}

	if(m_FixedAllocator)
	{
		m_SimpleAllocator.Free(m_FixedAllocator);
		m_FixedAllocator = NULL;
	}
}

bool netMemAllocator::SetQuitOnFail(const bool quitOnFail)
{
	SYS_CS_SYNC(m_AllocCs);

	m_QuitOnFailure = quitOnFail;
	return m_SimpleAllocator.SetQuitOnFail(quitOnFail);
}

void* netMemAllocator::Allocate(size_t size, AllocReason NET_ALLOCATION_PROFILING_FULL_ONLY(reason), size_t align, int heapIndex /*= 0*/)
{
	SYS_CS_SYNC(m_AllocCs);

#if NET_ALLOCATION_PROFILING_MINIMAL
	bool fixedAllocation = false;
	sm_AllocProfiler.StartAlloc(this);
#endif

	// we guarantee fixed allocs are 16 byte aligned, so we support alignments of 0, 1, 2, 4, 8, 16 bytes.
	netAssert(align == 0 || align == 8 || align == 16);

	void* ptr = NULL;
	if(m_FixedAllocator && (size <= m_MaxFixedChunkSize))
	{
		ptr = m_FixedAllocator->Allocate(size, align, heapIndex);

		netAssert(((size_t)ptr % 16) == 0);

#if NET_ALLOCATION_PROFILING_MINIMAL
		fixedAllocation = ptr != NULL;
#endif
	}

	if(!ptr)
	{
		// the fixed allocator couldn't allocate the memory, try allocating from the simple allocator portion of our heap.
		ptr = m_SimpleAllocator.Allocate(size, align, heapIndex);
	}

	// we only use low water marks for output, don't waste time tracking it in non output builds
#if !__NO_OUTPUT
	size_t memoryAvailable = GetMemoryAvailable();
	if(memoryAvailable < m_LeastMemoryAvailable)
	{
		m_LeastMemoryAvailable = memoryAvailable;
	}

	if(!ptr)
	{
		// the CxnMgr handles allocation failures by chucking out inbound packet queues
		// (which we know the remote peer will resend if necessary), and outbound unreliable frames.
		// Excessive logging here would cause a large amount of output and stall the game, which in
		// turn causes more memory allocation failures as packet queues start backing up.
		static unsigned s_LastMemOutputTime = 0;
		const unsigned MAX_OUTPUT_INTERVAL_MS = (4 * 1000);
		unsigned curTime = sysTimer::GetSystemMsTime();
		unsigned elapsed = curTime - s_LastMemOutputTime;
		if(elapsed > MAX_OUTPUT_INTERVAL_MS)
		{
			s_LastMemOutputTime = curTime;
			netDebug("Out of memory when allocating %" SIZETFMT "u bytes - not critical but alarming. Will suppress these warnings for %u ms.", size, MAX_OUTPUT_INTERVAL_MS);
			sysStack::PrintStackTrace();
			PrintBasicMemoryUsage();
		}
	}
#endif

#if NET_ALLOCATION_PROFILING_MINIMAL
	sm_AllocProfiler.EndAlloc(this, NET_ALLOCATION_PROFILING_FULL_ONLY(reason,) size, ptr, fixedAllocation);
#endif

	return ptr;
}

void* netMemAllocator::Allocate(size_t size, size_t align, int heapIndex /*= 0*/)
{
	SYS_CS_SYNC(m_AllocCs);

	return this->Allocate(size, ALLOC_OTHER, align, heapIndex);
}

void* netMemAllocator::TryAllocate(size_t size, size_t align, int heapIndex)
{
	SYS_CS_SYNC(m_AllocCs);

	const bool quitOnFail = m_QuitOnFailure;
	SetQuitOnFail(false);
	void* mem = this->Allocate(size, align, heapIndex);
	SetQuitOnFail(quitOnFail);
	return mem;
}

void netMemAllocator::Free(const void *ptr)
{
	SYS_CS_SYNC(m_AllocCs);

#if NET_ALLOCATION_PROFILING_FULL
	sm_AllocProfiler.StartDealloc(this, ptr);
#endif

	if(m_FixedAllocator && m_FixedAllocator->IsValidPointer(ptr))
	{
		m_FixedAllocator->Free(ptr);
	}
	else
	{
		m_SimpleAllocator.Free(ptr);
	}

#if NET_ALLOCATION_PROFILING_FULL
	sm_AllocProfiler.EndDealloc(this);
#endif
}

void netMemAllocator::Resize(const void* ptr, size_t newSmallerSize)
{
	SYS_CS_SYNC(m_AllocCs);

	if(m_SimpleAllocator.IsValidPointer(ptr))
	{
		m_SimpleAllocator.Resize(ptr, newSmallerSize);
	}
}

sysMemAllocator* netMemAllocator::GetPointerOwner(const void *ptr)
{
	SYS_CS_SYNC(m_AllocCs);

	if(m_FixedAllocator && m_FixedAllocator->IsValidPointer(ptr))
	{
		return m_FixedAllocator;
	}

	return m_SimpleAllocator.GetPointerOwner(ptr);
}

size_t netMemAllocator::GetMemoryUsed(int bucket /*= -1 */)
{
	SYS_CS_SYNC(m_AllocCs);

	return (m_SimpleAllocator.GetMemoryUsed(bucket) - m_FixedHeapSize) +
	       (m_FixedAllocator ? m_FixedAllocator->GetMemoryUsed(bucket) : 0);
}

size_t netMemAllocator::GetMemoryAvailable()
{
	SYS_CS_SYNC(m_AllocCs);

	return m_SimpleAllocator.GetMemoryAvailable() +
	       (m_FixedAllocator ? m_FixedAllocator->GetMemoryAvailable() : 0);
}

size_t netMemAllocator::GetLargestAvailableBlock()
{
	SYS_CS_SYNC(m_AllocCs);

	size_t sa = m_SimpleAllocator.GetLargestAvailableBlock();
	size_t fa = (m_FixedAllocator ? m_FixedAllocator->GetLargestAvailableBlock() : 0);

	return Max(sa, fa);
}

size_t netMemAllocator::GetMemoryAvailableFixed()
{
	SYS_CS_SYNC(m_AllocCs);

	return m_FixedAllocator ? m_FixedAllocator->GetMemoryAvailable() : 0;
}

size_t netMemAllocator::GetMemoryAvailableSimple()
{
	return m_SimpleAllocator.GetMemoryAvailable();
}

size_t netMemAllocator::GetLargestAvailableBlockFixed()
{
	SYS_CS_SYNC(m_AllocCs);

	return m_FixedAllocator ? m_FixedAllocator->GetLargestAvailableBlock() : 0;
}

size_t netMemAllocator::GetLargestAvailableBlockSimple()
{
	return m_SimpleAllocator.GetLargestAvailableBlock();
}

u8 netMemAllocator::GetNumBucketAllocations(const unsigned bucket)
{
	SYS_CS_SYNC(m_AllocCs);

	return m_FixedAllocator->GetNumBucketAllocations(bucket);
}

size_t netMemAllocator::GetLowWaterMark(bool reset)
{
	SYS_CS_SYNC(m_AllocCs);

	// we only use low water marks for output, don't waste time tracking it in non output builds
#if !__NO_OUTPUT
	size_t result = m_LeastMemoryAvailable;
	if(reset)
	{
		m_LeastMemoryAvailable = GetMemoryAvailable();
	}
	return result;
#else
	return m_SimpleAllocator.GetLowWaterMark(reset);
#endif
}

void netMemAllocator::SanityCheck()
{
	SYS_CS_SYNC(m_AllocCs);

	m_SimpleAllocator.SanityCheck();
	if(m_FixedAllocator)
	{
		m_FixedAllocator->SanityCheck();
	}
}

bool netMemAllocator::IsValidPointer(const void * ptr) const
{
	return m_SimpleAllocator.IsValidPointer(ptr) ||
	       (m_FixedAllocator && m_FixedAllocator->IsValidPointer(ptr));
}

void netMemAllocator::BeginLayer() 
{
	m_SimpleAllocator.BeginLayer();
}

int netMemAllocator::EndLayer(const char *layerName, const char *leakFile) 
{
	// fixed allocator doesn't support Begin/EndLayer, just count the number of allocated chunks.
	int numLeaks = m_FixedAllocator ? m_FixedAllocator->GetNumAllocations() : 0;
	numLeaks += m_SimpleAllocator.EndLayer(layerName, leakFile);
	return numLeaks;
}

size_t netMemAllocator::GetSizeWithOverhead(const void *ptr) const
{
	if(m_FixedAllocator && m_FixedAllocator->IsValidPointer(ptr))
	{
		return m_FixedAllocator->GetSizeWithOverhead(ptr);
	}
	else if(m_SimpleAllocator.IsValidPointer(ptr))
	{
		return m_SimpleAllocator.GetSizeWithOverhead(ptr);
	}

	return 0;
}

size_t netMemAllocator::GetHeapSize() const
{
	return m_SimpleAllocator.GetHeapSize();
}

void* netMemAllocator::GetHeapBase() const
{
	return m_SimpleAllocator.GetHeapBase();
}

size_t netMemAllocator::GetFragmentation()
{
	if(m_SimpleAllocator.GetMemoryAvailable() <= 0)
	{
		return 0;
	}

	// consider the fixed allocator to have 0% external fragmentation
	return 100 - u32(((u64)m_SimpleAllocator.GetLargestAvailableBlock() * 100) / m_SimpleAllocator.GetMemoryAvailable());
}

size_t netMemAllocator::GetFixedHeapMisses() const
{
	return sm_AllocProfiler.numFixedHeapMisses;
}

size_t netMemAllocator::GetFailedAllocations() const
{
	return sm_AllocProfiler.numFailedAllocations;
}

float netMemAllocator::GetEfficiency() const
{
	return sm_AllocProfiler.GetEfficiency();
}

void netMemAllocator::PrintBasicMemoryUsage()
{
#if !__NO_OUTPUT
	netDebug("Allocator: Size: %d, Used: %d, Available: %d, LargestBlock: %d, Fragmentation: %d, Low Watermark: %d",
		static_cast<int>(this->GetHeapSize()),
		static_cast<int>(this->GetMemoryUsed(-1)),
		static_cast<int>(this->GetMemoryAvailable()),
		static_cast<int>(this->GetLargestAvailableBlock()),
		static_cast<int>(this->GetFragmentation()),
		static_cast<int>(this->GetLowWaterMark(false)));

	if(m_FixedAllocator)
	{
		m_FixedAllocator->DumpStats();
	}
#endif
}

void netMemAllocator::PrintMemoryUsage()
{
#if NET_ALLOCATION_PROFILING_FULL
	sm_AllocProfiler.PrintStats(this);
#elif !__NO_OUTPUT
	PrintBasicMemoryUsage();
#if !RSG_FINAL
	m_SimpleAllocator.PrintMemoryUsage();
#endif
#endif //!__NO_OUTPUT
}

}   //namespace rage
