// 
// net/policies.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_POLICIES_H
#define NET_POLICIES_H

#include "netdiag.h"

namespace rage
{

//PURPOSE
//  Policies that control how a connection operates.
class netConnectionPolicies
{
public:

    enum
    {
        //Default time (in milliseconds) after which a connection will
        //time out if it has not received a keepalive.
        DEFAULT_TIMEOUT_INTERVAL    = 8000,

        //Default interval at which we'll send a keep alive message
		DEFAULT_KEEP_ALIVE_INTERVAL	= 1000,

		//Default interval (in milliseconds) between resending reliable frames.
		DEFAULT_RESEND_INTERVAL = 250,

		DEFAULT_DEBUG_SPEW_ENABLED  = true,

		//Default maximum send attempts for a reliable frame before generating a TIMEOUT event.
		DEFAULT_MAX_SEND_COUNT = (netConnectionPolicies::DEFAULT_TIMEOUT_INTERVAL + DEFAULT_RESEND_INTERVAL - 1) / DEFAULT_RESEND_INTERVAL,

        //This will be used for "pending-close" connections
        //when the timeout interval is set to zero.  It will prevent
        //connections with un-ACKed frames from lingering for ever.
        //aka no timeout.
        PENDING_CLOSE_MAX_SEND_COUNT    = 50,
    };

    netConnectionPolicies()
        : m_KeepaliveInterval(DEFAULT_KEEP_ALIVE_INTERVAL)
        , m_TimeoutInterval(DEFAULT_TIMEOUT_INTERVAL)
		, m_ResendInterval(DEFAULT_RESEND_INTERVAL)
		, m_MaxSendCount(DEFAULT_MAX_SEND_COUNT)
        , m_EnableDebugSpew(DEFAULT_DEBUG_SPEW_ENABLED)
    {
        this->SetTimeout(DEFAULT_TIMEOUT_INTERVAL);
    }

	//PURPOSE
	//  Convenience function for setting the timeout interval for
	//  connection policies.  Sets the max send count for reliable messages
	//  based on the current resend interval and the timeout value
	//  provided.
	//PARAMS
	//  timoutMs - Timeout value in milliseconds.  A value of zero implies no timeouts.
	//NOTES
	//  m_ResendInterval should be set to the desired value before calling this
	//  function.
	void SetTimeout(const unsigned timeoutMs)
    {
        if(timeoutMs > 0)
        {
			netAssert(m_ResendInterval > 0);
            m_TimeoutInterval = timeoutMs;
			m_MaxSendCount = (timeoutMs + m_ResendInterval - 1) / m_ResendInterval;
			netAssert(m_MaxSendCount > 0);
        }
        else
        {
            //Never timeout.
            m_TimeoutInterval = 0;
			m_MaxSendCount = ~0u;
            m_KeepaliveInterval = 0;
        }
    }

	void SetKeepAliveInterval(const unsigned intervalMs)
	{
		m_KeepaliveInterval = intervalMs;
	}

    //Interval at which keepalives are sent.
    //Zero disables keepalives.
    unsigned m_KeepaliveInterval;

    //Maximum time to tolerate not receiving a packet.
    //Zero disables timeouts.
    unsigned m_TimeoutInterval;

	//Minimum interval (in milliseconds) between resends of reliable frames.
	unsigned m_ResendInterval;

	//Maximum send attempts for a reliable frame before generating
	//a TIMEOUT event.
	unsigned m_MaxSendCount;

	//Set to true to enable debug spew for the connection.
	bool m_EnableDebugSpew;
};

//PURPOSE
//  Policies that control how a channel operates.
class netChannelPolicies
{
public:

    enum
    {
        //Default outbound bandwidth limit in bytes/sec.
        //Zero implies no limit.
        DEFAULT_OUTBOUND_BANDWIDTH_LIMIT = 0,
        MAX_OUTBOUND_BANDWIDTH_LIMIT = 1024*1024,    //One MB/sec - DON'T INCREASE THIS!
        
		DEFAULT_COMPRESSION_ENABLED = true,
		DEFAULT_ALWAYS_MAINTAIN_KEEP_ALIVE = false,
    };

    netChannelPolicies()
        : m_OutboundBandwidthLimit(DEFAULT_OUTBOUND_BANDWIDTH_LIMIT)
        , m_CompressionEnabled(DEFAULT_COMPRESSION_ENABLED)
		, m_AlwaysMaintainKeepAlive(DEFAULT_ALWAYS_MAINTAIN_KEEP_ALIVE)
    {
    }

    //Maximum number of outbound bytes per second.
    //Zero implies no limit.
    //If this is exceeded outbound unreliable frames will
    //be dropped and a netEventBandwidthExceeded event will
    //be dispatched.
    unsigned m_OutboundBandwidthLimit;

    //If true messages on this channel will be compressed.
    bool m_CompressionEnabled;

	//If true we always maintain a keep alive on this channel
	bool m_AlwaysMaintainKeepAlive; 
};

}   //namespace rage

#endif  //NET_POLICIES_H
