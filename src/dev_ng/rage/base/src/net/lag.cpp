// 
// net/timesync.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "lag.h"

#include "netdiag.h"
#include "profile/rocky.h"
#include "system/timer.h"

namespace rage
{

NET_MESSAGE_IMPL(netLagPingMsg);

netLag::netLag()
    : m_CxnMgr(0)
    , m_NextPingTime(0)
    , m_PingInterval(DEFAULT_PING_INTERVAL)
    , m_ChannelId(NET_INVALID_CHANNEL_ID)
    , m_RttTop(0)
    , m_RttTotal(0)
    , m_RttCount(0)
{
    m_Dlgt.Bind(this, &netLag::OnNetEvent);

    for(int i = 0; i < NUM_RTT_SAMPLES; ++i)
    {
        m_Rtt[i] = 0;
    }
}

netLag::~netLag()
{
    this->Stop();
}

bool
netLag::Start(netConnectionManager* cxnMgr,
                const netAddress& peerAddr,
                const unsigned channelId,
                const unsigned pingInterval)
{
    netAssert(channelId <= NET_MAX_CHANNEL_ID);
    netAssert(pingInterval >= 1000);

    bool success = false;

    if(netVerify(!m_CxnMgr))
    {
        m_CxnMgr = cxnMgr;
        m_NextPingTime = 0;
        m_PingInterval = pingInterval;
        m_ChannelId = channelId;
        m_PeerAddr = peerAddr;
        cxnMgr->AddChannelDelegate(&m_Dlgt, m_ChannelId);

        for(int i = 0; i < NUM_RTT_SAMPLES; ++i)
        {
            m_Rtt[i] = 0;
        }

        m_RttTop = m_RttTotal = m_RttCount = 0;

        success = true;
    }

    return success;
}

bool
netLag::Stop()
{
    if(m_CxnMgr)
    {
        m_CxnMgr->RemoveChannelDelegate(&m_Dlgt);
        m_CxnMgr = 0;
    }

    return true;
}

void
netLag::Update()
{
    if(netVerify(m_CxnMgr))
    {
        const unsigned localTime = this->GetLocalTime();

        if(!m_NextPingTime || int(localTime - m_NextPingTime) >= 0)
        {
            netDebug3("Sending lag ping to:" NET_ADDR_FMT "",
                        NET_ADDR_FOR_PRINTF(m_PeerAddr));

            netLagPingMsg ping;

            ping.m_Type = netLagPingMsg::TYPE_PING;
            ping.m_Timestamp = localTime;

            m_CxnMgr->SendOutOfBand(m_PeerAddr, m_ChannelId, ping, NET_SEND_IMMEDIATE);

            //Make sure next ping time is never zero.
            m_NextPingTime = (localTime + m_PingInterval) | 0x01;
        }
    }
}

unsigned
netLag::GetLocalTime() const
{
    return sysTimer::GetSystemMsTime();
}

unsigned
netLag::GetAvgRtTime() const
{
    return m_RttCount ? m_RttTotal / m_RttCount : 0;
}

const netAddress&
netLag::GetPeerAddress()
{
    return m_PeerAddr;
}

bool
netLag::HasSamples() const
{
    return m_RttCount > 0;
}

//private:

void
netLag::OnNetEvent(netConnectionManager* cxnMgr, const netEvent* evt)
{
	PROFILE;

    netLagPingMsg msg;

    if(NET_EVENT_FRAME_RECEIVED == evt->GetId()
        && msg.Import(evt->m_FrameReceived->m_Payload,
                        evt->m_FrameReceived->m_SizeofPayload))
    {
        if(netLagPingMsg::TYPE_PING == msg.m_Type)
        {
            netDebug3("Received lag ping - sending lag pong to:" NET_ADDR_FMT "",
                        NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));

            msg.m_Type = netLagPingMsg::TYPE_PONG;

            netVerify(cxnMgr->SendOutOfBand(evt->m_FrameReceived->m_Sender,
                                                m_ChannelId,
                                                msg,
                                                NET_SEND_IMMEDIATE));
        }
        else if(netLagPingMsg::TYPE_PONG == msg.m_Type
                && evt->m_FrameReceived->m_Sender == m_PeerAddr)
        {
            const unsigned localTime = this->GetLocalTime();

            netDebug2("Received lag pong from:" NET_ADDR_FMT " - rttime:%d",
                        NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender),
                        localTime - msg.m_Timestamp);

            unsigned rtt = localTime - msg.m_Timestamp;

            if(m_RttCount > 1)
            {
                const unsigned avgRtt = this->GetAvgRtTime();

                //Weak attempt to level out spikes.
                if(rtt > (avgRtt << 1))
                {
                    rtt = (rtt + avgRtt) >> 1;
                }
            }

            m_Rtt[m_RttTop] = rtt;
            m_RttTotal += rtt;

            if(++m_RttTop >= NUM_RTT_SAMPLES)
            {
                m_RttTop = 0;
            }

            if(m_RttCount < NUM_RTT_SAMPLES)
            {
                ++m_RttCount;
            }

            m_RttTotal -= m_Rtt[m_RttTop];
        }
    }
}

}   //namespace rage
