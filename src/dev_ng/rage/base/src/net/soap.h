// 
// net/soap.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_SOAP_H 
#define NET_SOAP_H 

#include "http.h"

namespace rage
{

class sysMemAllocator;

//PURPOSE
//  Used to parse SOAP formatted messages.
//NOTES
//  Ignores XML namespaces.
class netSoapReader
{
public:

    netSoapReader();

    ~netSoapReader();

    //PURPOSE
    //  Clears the reader to its initial state.
    void Clear();

    //PURPOSE
    //  Resets the reader to begin reading a SOAP formatted message, or
    //  an element of a SOAP formatted message.
    //PARAMS
    //  buf     - Points to buffer containing SOAP message.
    //  offset  - Offset within buf of start of SOAP message.
    //  len     - Length of SOAP message.
    //NOTES
    //  XML headers (enclosed in <? ?>) are ignored.
    bool Reset(const char* buf,
                const unsigned offset,
                const unsigned len);

    //PURPOSE
    //  Resets the reader to begin reading a SOAP formatted message, or
    //  an element of a SOAP formatted message.
    //PARAMS
    //  buf         - Points to the beginning of the SOAP message.
    //  endOfBuf    - Points to the end of the SOAP message.
    //NOTES
    //  XML headers (enclosed in <? ?>) are ignored.
    bool Reset(const char* buf,
                const char* endOfBuf);

    //PURPOSE
    //  Returns a pointer to the beginning of the SOAP buffer passed
    //  to Reset().
    //NOTES
    //  Does not include the SOAP header contained between <? ?>.
    const char* GetSoap() const;

    //PURPOSE
    //  Returns the length of the SOAP buffer passed to Reset();
    unsigned GetLength() const;

    //PURPOSE
    //  Retrieves the child element with the given name.
    //PARAMS
    //  name    - Name of child element
    //  reader  - Reader that will be initialized with the child element.
    bool GetElement(const char* name,
                    netSoapReader* reader) const;

    //PURPOSE
    //  Retrieves the first child element.
    //PARAMS
    //  reader  - Reader that will be initialized with the child element.
    bool GetFirst(netSoapReader* reader) const;

    //PURPOSE
    //  Retrieves the next sibling of the given child element.
    //PARAMS
    //  current - Current child element.
    //  next    - Reader that will be initialized with the next child element.
    bool GetNext(const netSoapReader& current,
                netSoapReader* next) const;

    //PURPOSE
    //  Retrieves the name of the element
    //PARAMS
    //  name        - Buffer that will be populated with the name.
    //  maxChars    - Maximum number of chars, including the null terminator.
    unsigned GetName(char* name,
                    const unsigned maxChars) const;

    //PURPOSE
    //  Retrieves a pointer to the content of the element (the bit between tags).
    //PARAMS
    //  length  - On success will contain the content length.
    //NOTES:
    //  The content is *not* XML decoded.
    const char* GetContent(unsigned* length) const;

    //PURPOSE
    //  Converts the current content to a signed integer.
    bool AsInt(int& val) const;

    //PURPOSE
    //  Converts the current content to a signed 64-bit integer.
    bool AsInt64(s64& val) const;

    //PURPOSE
    //  Converts the current content to an unsigned integer.
    bool AsUns(unsigned& val) const;

    //PURPOSE
    //  Converts the current content to an unsigned 64-bit integer.
    bool AsUns64(u64& val) const;

	//PURPOSE
	//  Converts the current value to a float.
	bool AsFloat(float& val) const;

	//PURPOSE
	//  Converts the current content to a double.
	bool AsDouble(double& val) const;

    /*//PURPOSE
    //  Converts the current content to a binary value.
    //PARAMS
    //  val     - Destination buffer
    //  maxLen  - Number of bytes in the destination buffer.
    //  len     - On success contains the number of bytes decoded.
    //            On failure, contains the number of bytes required for
    //            decoding.
    //NOTES
    //  Assumes the value is in base-64 format.
    bool AsBinary(void* val, const int maxLen, int* len) const;*/

    //PURPOSE
    //  Converts the current content to a boolean.
    bool AsBool(bool& val) const;

    //PURPOSE
    //  Converts the current content to a string.
    //PARAMS
    //  val         - Buffer that will be populated with the string.
    //  maxChars    - Maximum number of chars, including the null terminator.
    //RETURNS
    //  Length of string.
    //NOTES
    //  Content is XML decoded before being returned.
    unsigned AsString(char* val, const unsigned maxChars) const;

    //PURPOSE
    //  Converts the current value to the given type.
    //NOTES
    //  This function is specialized in the .cpp.
    template<typename T>
    bool AsValue(T& value) const;

private:

    const char* m_Buf;
    const char* m_Eob;
};

//PURPOSE
//  Interface for building a SOAP envelope.
class netSoapEnvelope
{
public:

    netSoapEnvelope();

    ~netSoapEnvelope();

    //PURPOSE
    //  Clears the request to its initial state
    void Clear();

    //PURPOSE
    //  Begins constructing a new SOAP envelope.  Existing data will be cleared.
    //PARAMS
    //  uri         - Fully qualified URI for request.
    //  methodXmlns - Method XML namespace
    //  methodName  - Method name
    //  sysMemAllocator - Optional allocator.
    //NOTES
    //  Example:
    //  soapEnv.Begin("http://services.aonaware.com/DictService/DictService.asmx", "services.aonaware.com/webservices/", "Define");
    bool Begin(const char* uri,
                const char* methodXmlns,
                const char* methodName,
                sysMemAllocator* allocator);

    //PURPOSE
    //  Ends construction of the SOAP envelope. 
    bool End();

    //PURPOSE
    //  Begins construction of a compound XML element, that is an XML element
    //  that contains other XML elements.
    //PARAMS
    //  name    - Name of the element.
    bool BeginCompound(const char* name);

    //PURPOSE
    //  Ends construction of a compound XML element.
    //PARAMS
    //  name    - Name of the element.
    bool EndCompound(const char* name);

    //PURPOSE
    //  Constructs an XML element that contains the given content.
    //PARAMS
    //  name    - Name of the element.
    //  content - Content of element
    //  len     - Number of chars in the content.
    //NOTES
    //  This differs from WriteString() in that the content is *not*
    //  XML encoded.
    bool WriteElement(const char* name,
                    const char* content,
                    const unsigned len);

    //PURPOSE
    //  Constructs an XML element that contains a string value as content.
    //PARAMS
    //  name    - Name of the element.
    //  value   - String
    //  valLen  - Number of chars in the string
    //NOTES
    //  For example, the following code:
    //      soapRqst.WriteString("Foo", "Bar", 3);
    //  produces the following XML:
    //      <Foo>Bar</Foo>
    //
    //  The string is XML-encoded prior to writing.
    bool WriteString(const char* name, const char* value, const unsigned valLen);

    //PURPOSE
    //  Constructs an XML element that contains the null-terminated string as content.
    //PARAMS
    //  name    - Name of the element.
    //  value   - Null-terminated string
    //NOTES
    //  For example, the following code:
    //      soapRqst.WriteString("Foo", "Bar");
    //  produces the following XML:
    //      <Foo>Bar</Foo>
    //
    //  The string is XML-encoded prior to writing.
    bool WriteString(const char* name, const char* value);

    //PURPOSE
    //  Constructs an XML element the contains an integer value as content.
    //PARAMS
    //  name    - Name of the element.
    //  value   - Element value.
    //NOTES
    //  For example, the following code:
    //      soapRqst.WriteString("Foo", 123);
    //  produces the following XML:
    //      <Foo>123</Foo>
    bool WriteInt(const char* name, const int value);

    //PURPOSE
    //  Constructs an XML element the contains a float value as content.
    //PARAMS
    //  name    - Name of the element.
    //  value   - Element value.
    //NOTES
    //  For example, the following code:
    //      soapRqst.WriteString("Foo", 23.125f);
    //  produces the following XML:
    //      <Foo>23.125</Foo>
    bool WriteFloat(const char* name,
                    const float value,
                    const int precision = netHttpRequest::DEFAULT_FLOAT_PRECISION);

    //PURPOSE
    //  Returns true if the envelope is complete.
    bool IsComplete() const;

    //PURPOSE
    //  Returns the null-terminated SOAP string.
    const char* GetSoap() const;

    //PURPOSE
    //  Returns the length of the SOAP request.
    int GetLength() const;

    const char* GetUri() const;

    const char* GetSoapAction() const;

    //PURPOSE
    //  HTTP POSTs the SOAP request.
    //   
    //PARAMS
    //  proxyAddr   - If non-null overrides global HTTP proxy address
    //                for this request.
    //  timeoutSecs - Timeout in seconds.  If zero use the default HTTP timeout.
    //  status      - Can be polled for completion.
    bool Post(const netAddress* proxyAddr,
                const unsigned timeoutSecs,
                netStatus* status);

    //PURPOSE
    //  Must be called once per frame to update the status of a pending
    //  request.
    void Update();

    //PURPOSE
    //  Returns true while a request is pending.
    bool Pending() const;

    //PURPOSE
    //  Cancels a pending POST.
    void Cancel();

    //PURPOSE
    //  Retrieve the response to a SOAP request.
    //PARAMS
    //  len     - Populated with the length of the response.
    //RETURNS
    //  Pointer to the response buffer.
    //NOTES
    //  The response is *NOT* NULL terminated.
    const char* GetResponse(unsigned* len) const;

    //PURPOSE
    //  Populates the soap reader with the response.
    bool GetResponse(netSoapReader* reader) const;

private:

    enum State
    {
        STATE_NONE,
        STATE_BUILDING_ENVELOPE,
        STATE_FINISHED_ENVELOPE,
        STATE_POSTING,
        STATE_SUCCEEDED,
        STATE_ERROR
    };

    char* Allocate(const unsigned numChars);

    void Free(char* chars);

    //Prevent copying
    netSoapEnvelope(const netSoapEnvelope&);
    netSoapEnvelope& operator=(const netSoapEnvelope&);

    atStringBuilder m_Envelope;

    netHttpRequest m_HttpReq;
    char* m_Uri;
    char* m_Ns;
    char* m_Method;
    char* m_SoapAction;
    State m_State;

    sysMemAllocator* m_Allocator;

    netStatus* m_Status;
    netStatus m_MyStatus;

    bool m_HaveCompleteEnvelope : 1;
};

} // namespace rage

#endif // NET_SOAP_H 
