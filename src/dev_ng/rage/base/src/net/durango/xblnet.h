// 
// xblnet.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLXBL_NET_H
#define RLXBL_NET_H

#include "diag/channel.h"
#include "net/net.h"
#include "string/string.h"
#include "system/criticalsection.h"

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(ragenet, xbl)

#if RSG_DURANGO

class netXbl
{
public:

	static bool Init();
	static void Shutdown();
	static void Update(); 
	
	//PURPOSE
	//  Returns the connection state of the link (Ethernet cable).
	//RETURNS
	//  True if the link is connected; false otherwise (e.g. if cable unplugged).
	static bool IsLinkConnected();

	//PURPOSE
	//  Allows NetworkInformation::GetInternetConnectionProfile to drive connectivity
	static void SetUseConnectivityLevelForLinkConnected(const bool bUseConnectivityLevel);
	
private:
	static sysCriticalSectionToken sm_CS;
	static bool sm_UseConnectivityLevelForLinkConnected;
};

#endif //RSG_DURANGO

} // namespace rage

#endif //RLXBL_NET_H
