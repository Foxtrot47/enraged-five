// 
// xbltask.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

#include "xbltask.h"
#include "string/unicode.h"

namespace rage
{

netXblTask::netXblTask()
{
	m_State = STATE_DO_WORK;
}

void netXblTask::OnCancel()
{
	m_XblStatus.Cancel();
}

netTaskStatus netXblTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_DO_WORK:
		if(WasCanceled())
		{
			netTaskDebug("Canceled - bailing...");
			status = NET_TASKSTATUS_FAILED;
		}
		else if(DoWork())
		{
			m_State = STATE_WORKING;
		}
		else
		{
			status = NET_TASKSTATUS_FAILED;
		}
		break;
	case STATE_WORKING:
		m_XblStatus.Update();

		if (WasCanceled())
		{
			status = NET_TASKSTATUS_FAILED;
			ProcessCancelled();
		}
		else if(m_MyStatus.Succeeded())
		{
			status = NET_TASKSTATUS_SUCCEEDED;
			ProcessSuccess();
		}
		else if(!m_MyStatus.Pending())
		{
			status = NET_TASKSTATUS_FAILED;
			ProcessFailure();
		}
		break;
	}

	return status;
}

} // namespace rage

#endif  //((RSG_DURANGO))
