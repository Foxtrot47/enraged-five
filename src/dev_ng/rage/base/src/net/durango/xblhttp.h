// 
// xblhttp.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_XBL_HTTP_H
#define NET_XBL_HTTP_H

#if RSG_DURANGO

namespace rage
{

class sysMemAllocator;
class netStatus;

//PURPOSE
//  Interface to Durango Http request
class netXblHttpRequest
{
public:
    netXblHttpRequest();
    ~netXblHttpRequest();

	//PURPOSE
	//  Initializes the http request.
	//PARAMS
	//  allocator - Optional. Used to allocate memory. If NULL the global
	//              allocator will be used.
    bool Init(sysMemAllocator* allocator);

	//PURPOSE
	//  Sets the method (verb) and URI of the request.
	//NOTES
	//  Must be called after Init() and before SetRequestHeader().
	bool Open(const char* verb,
			  const char* uri);

	//PURPOSE
	//  Sets the request headers.
	//PARAMS
	//  headers - string containing all the "header: value" pairs,
	//			  pair separated by \r\n. The first line is always skipped
	//			  to match what http.cpp passes in.
	// headersLen - length of headers
	bool SetRequestHeader(const char* headers,
						  const unsigned headersLen);

	//PURPOSE
	//  Queues a chunk of data to the request stream.
	//PARAMS
	//  data - the block of data to queue.
	//  dataLen - the length of data.
	//  status - can be polled for completion/success.
	bool QueueChunk(const u8* data,
					const unsigned dataLen,
					netStatus* status);

	//PURPOSE
	//  Sends the request.
	bool Commit();

	//PURPOSE
	//  Aborts the request.
	void Cancel();

	//PURPOSE
	//  Clear the request and free memory. Should not be called while a
	//  request is pending.
	void Clear();

	bool IsFinished() const;
	bool HasError() const;
	s32 GetErrorCode() const;

	//PURPOSE
	//  Returns true when we've received the response headers.
	bool HaveHeaders() const;

	//PURPOSE
	//  Returns the length of the response headers.
	//NOTES
	//  Only valid after HaveHeaders() returns true.
	bool GetResponseHeaderLength(unsigned* headerLen) const;

	//PURPOSE
	//  Returns response headers.
	//NOTES
	//  Only valid after HaveHeaders() returns true.
	//  Once the headers are read, they are considered to be consumed. Do not call more than once.
	bool GetResponseHeader(char* headerBuf,
						   unsigned* headerLen) const;

	//PURPOSE
	//  Reads data from the response.
	//NOTES
	//  When allContentRead is true upon return, 
	//  there is no more data to read.
	//  Returns false when an error has occurred.
	bool ReadData(u8* data,
				  const unsigned maxLen,
				  unsigned* numRead,
				  bool* allContentRead);

private:
	void* Allocate(const unsigned numBytes) const;
	void Free(void* ptr) const;

	sysMemAllocator* m_Allocator;

	class Impl;
	Impl* m_Impl;
};

class netXblHttp
{
public:
	struct netXblRelyingPartyToken
	{
		netXblRelyingPartyToken()
			: m_Token(NULL)
			, m_Signature(NULL)
		{

		}

		char* m_Token;
		char* m_Signature;
	};

	static const char* GetAuthActorHeaderName() {return "xbl-authz-actor-10";}

	static bool GetAuthToken(const char* httpVerb,
							 const char* uri,
							 const char* headers,
							 const u8* body,
							 const unsigned sizeOfBody,
							 const char* xblUserHash,
							 netXblRelyingPartyToken* authToken,
							 netStatus* status);

	static void FreeAuthToken(netXblRelyingPartyToken* authToken);

	static void CancelAuthTokenRequest(netStatus* status);
};

} // namespace rage

#endif  // RSG_DURANGO

#endif  //NET_XBL_HTTP_H
