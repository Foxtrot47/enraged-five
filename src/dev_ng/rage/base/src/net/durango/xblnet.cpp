// 
// xblnet.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO
#include "xblnet.h"
#include "diag/seh.h"
#include "net/netdiag.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"
#include "rline/durango/rlxbl_interface.h"
#include "rline/durango/rlxblpresence_interface.h"

#if !__NO_OUTPUT
#include "rline/rlpeeraddress.h"
#endif

#pragma warning(push)
#pragma warning(disable: 4668)
#pragma warning(disable: 4265)
#include <robuffer.h>
#include <hstring.h>
#include <inspectable.h>
#include <wrl/client.h>
#pragma warning(pop)

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, xbl)

#undef __rage_channel
#define __rage_channel ragenet_xbl

bool netXbl::sm_UseConnectivityLevelForLinkConnected = true;

bool 
netXbl::IsLinkConnected()
{
	if(sm_UseConnectivityLevelForLinkConnected)
	{
		return g_rlXbl.GetPresenceManager()->IsOnline();
	}
	return true; 
}

void 
netXbl::SetUseConnectivityLevelForLinkConnected(const bool bUseConnectivityLevel)
{
	if(sm_UseConnectivityLevelForLinkConnected != bUseConnectivityLevel)
	{
		netDebug("SetUseConnectivityLevelForLinkConnected :: Setting to %s", bUseConnectivityLevel ? "True" : "False");
		sm_UseConnectivityLevelForLinkConnected = bUseConnectivityLevel;
	}
}

bool
netXbl::Init()
{
	// nothing to do here now that we don't use Xbox One's Secure Sockets
	return true;
}

void
netXbl::Shutdown()
{

}

void 
netXbl::Update()
{

}

} // namespace rage

#endif  //RSG_DURANGO
