// 
// xblhttp.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#if RSG_DURANGO

#include "data/growbuffer.h"
#include "diag/seh.h"
#include "file/device.h"
#include "net/durango/xblnet.h"
#include "net/durango/xblstatus.h"
#include "net/netdiag.h"
#include "net/status.h"
#include "net/task.h"
#include "net/task2.h"
#include "profile/rocky.h"
#include "string/stringutil.h"
#include "string/unicode.h"
#include "system/alloca.h"
#include "system/criticalsection.h"
#include "system/interlocked.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/userlist_durango.winrt.h"
#include "system/xtl.h"

#include "xblhttp.h"
#include "xbltask.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#pragma warning(disable: 4265)
#include <xdk.h>
#include <collection.h>
#include <ixmlhttprequest2.h>
#include <wrl.h>
#pragma warning(pop)

using namespace Windows::Foundation;
using namespace Microsoft::WRL;
using namespace Windows::Foundation::Collections;

extern __THREAD int RAGE_LOG_DISABLE;

#define XBL_HTTP_VERBOSE_OUTPUT (!__NO_OUTPUT && 0)

namespace rage
{

#undef __rage_channel
#define __rage_channel ragenet_xbl

PARAM(nethttpnoipcevent, "[http] Don't use an event for thread synchronization on Xbox One", "Disabled", "XB1", "");

///////////////////////////////////////////////////////////////////////////////
//  netXblHttpRequestStream
//  Encapsulates a request data stream. It inherits ISequentialStream,
//  which the IXMLHTTPRequest2 class uses to read from our buffer. It also 
//  inherits IDispatch, which the IXMLHTTPRequest2 interface on Durango requires 
//  (unlike on Windows, where only ISequentialStream is necessary).
///////////////////////////////////////////////////////////////////////////////
class netXblHttpRequestStream
	: public Microsoft::WRL::RuntimeClass<RuntimeClassFlags<ClassicCom>,
										  ISequentialStream,
										  IDispatch>
{
public:

	netXblHttpRequestStream();
	~netXblHttpRequestStream();

	struct Chunk
	{
		Chunk(const u8* data, const unsigned dataLen, netStatus* status)
			: m_Data(data)
			, m_DataLen(dataLen)
			, m_BytesRead(0)
			, m_Status(status)
		{

		}

		~Chunk()
		{
			Reset();
		}

		void Reset()
		{
			m_Data = nullptr;
			m_DataLen = 0;
			m_BytesRead = 0;
			m_Status = nullptr;
		}

		const u8* m_Data;
		unsigned m_DataLen;
		unsigned m_BytesRead;
		netStatus* m_Status;
		inlist_node<Chunk> m_ListLink;
	};

	typedef inlist<Chunk, &Chunk::m_ListLink> ChunkList;

	void* Allocate(const unsigned numBytes) const;
	void Free(void* ptr) const;
	Chunk* NewChunk(const u8* data, const unsigned dataLen, netStatus* status);
	void DeleteChunk(Chunk* chunk);

	// ISequentialStream
	STDMETHODIMP Read(void *pv, ULONG cb, ULONG *pcbRead);
	STDMETHODIMP Write(const void *pv,  ULONG cb, ULONG *pcbWritten);
	STDMETHODIMP QueueChunk(const u8* data, const unsigned dataLen, netStatus* status);

	//IUnknown
	STDMETHODIMP QueryInterface(REFIID riid, void **ppvObject);

	//IDispatch
	STDMETHODIMP GetTypeInfoCount(unsigned int FAR* pctinfo);
	STDMETHODIMP GetTypeInfo(unsigned int iTInfo,
							 LCID lcid,
							 ITypeInfo FAR* FAR* ppTInfo);
	STDMETHODIMP GetIDsOfNames(REFIID riid,
							   OLECHAR FAR* FAR* rgszNames,
							   unsigned int cNames,
							   LCID lcid,
							   DISPID FAR* rgDispId);
	STDMETHODIMP Invoke(DISPID dispIdMember,
						REFIID riid,
						LCID lcid,
						WORD wFlags,
						DISPPARAMS FAR* pDispParams,
						VARIANT FAR* pVarResult,
						EXCEPINFO FAR* pExcepInfo,
						unsigned int FAR* puArgErr);

	// netXblHttpRequestStream
	bool Init(sysMemAllocator* allocator);
	unsigned Length() const;
	void ClearChunkList(ChunkList& list);
	void ClearChunks();
	void Clear();
	void Cancel();

private:
	sysMemAllocator* m_Allocator;
	ChunkList m_QueuedChunks;
	ChunkList m_SentChunks;
	unsigned m_RequestLength;
	mutable sysCriticalSectionToken m_RequestStreamCs;
};

netXblHttpRequestStream::netXblHttpRequestStream()
: m_Allocator(nullptr)
, m_RequestLength(0)
{
}

netXblHttpRequestStream::~netXblHttpRequestStream()
{
	// destructor can be called from a thread other than the main thread.
	// Be careful not to use m_Allocator since it is not guaranteed
	// to be thread safe.
	netAssert(m_QueuedChunks.empty());
}

bool 
netXblHttpRequestStream::Init(sysMemAllocator* allocator)
{
	SYS_CS_SYNC(m_RequestStreamCs);

	m_Allocator = allocator;

	return true;
}

unsigned
netXblHttpRequestStream::Length() const
{
	SYS_CS_SYNC(m_RequestStreamCs);

	return m_RequestLength;
}

void
netXblHttpRequestStream::ClearChunkList(ChunkList& list)
{
	SYS_CS_SYNC(m_RequestStreamCs);

	while(!list.empty())
	{
		Chunk* chunk = list.front();
		list.pop_front();

		if(chunk)
		{
			netStatus* status = chunk->m_Status;

			// delete the chunk before setting the status to completed
			DeleteChunk(chunk);

			if(status && status->Pending())
			{
				status->SetFailed();
			}
		}
	}
}

void
netXblHttpRequestStream::ClearChunks()
{
	SYS_CS_SYNC(m_RequestStreamCs);

	ClearChunkList(m_QueuedChunks);
	ClearChunkList(m_SentChunks);
}

void netXblHttpRequestStream::Clear()
{
	SYS_CS_SYNC(m_RequestStreamCs);

	ClearChunks();
}

void
netXblHttpRequestStream::Cancel()
{
	SYS_CS_SYNC(m_RequestStreamCs);

	ClearChunks();
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpRequestStream::Read
//  Desc: ISequentialStream overload: Reads data from the buffer
//--------------------------------------------------------------------------------------
STDMETHODIMP
netXblHttpRequestStream::Read(void *pv,
							  ULONG cb,
							  ULONG *pcbNumReadBytes)
{
	SYS_CS_SYNC(m_RequestStreamCs);

	// Note: this gets called from the IXHR2 thread.

    if(pv == nullptr)
    {
        return E_INVALIDARG;
    }

	unsigned totalBytesRead = 0;

	rtry
	{
		unsigned remaining = cb;
		while(remaining > 0)
		{
			rcheckall(!m_QueuedChunks.empty());

			Chunk* chunk = m_QueuedChunks.front();
			rverifyall(chunk);
			rverifyall(chunk->m_Data);
			rverifyall(chunk->m_BytesRead <= chunk->m_DataLen);

			// Note that the chunk can be larger than the supplied buffer. In that case, we
			// copy as much of the chunk as possible, and then return. IXHR2 will call this
			// function again to get the remaining data.
			unsigned chunkRemaining = chunk->m_DataLen - chunk->m_BytesRead;
			
			if(chunkRemaining > 0)
			{
				int numBytesToRead = Min(remaining, chunkRemaining);
				memcpy(((u8*)pv) + totalBytesRead, chunk->m_Data + chunk->m_BytesRead, numBytesToRead);
				chunk->m_BytesRead += numBytesToRead;
				totalBytesRead += numBytesToRead;
				remaining -= numBytesToRead;
			}

			// Once a chunk has been read completely, mark the chunk as succeeded and move it
			// to the sent chunks list. The higher level can then delete the chunk's data.
			// Don't delete chunks in this function since it's called on a thread other than
			// the main thread, and the allocator passed in is not guaranteed to be thread safe.
			chunkRemaining = chunk->m_DataLen - chunk->m_BytesRead;
			if(chunkRemaining == 0)
			{
				m_QueuedChunks.pop_front();
				netStatus* status = chunk->m_Status;

				chunk->Reset();
				m_SentChunks.push_back(chunk);

				if(status && netVerify(status->Pending()))
				{
					status->SetSucceeded();
				}
			}
		}
	}
	rcatchall
	{

	}

	*pcbNumReadBytes += totalBytesRead;

	// S_FALSE indicates the end of the stream has been reached
	return *pcbNumReadBytes == cb ? S_OK : S_FALSE;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpRequestStream::Write
//  Desc: ISequentialStream overload: Writes to the buffer.
//--------------------------------------------------------------------------------------
STDMETHODIMP
netXblHttpRequestStream::Write(const void* UNUSED_PARAM(pv),
							   ULONG UNUSED_PARAM(cb),
							   ULONG* UNUSED_PARAM(pcbWritten))
{
	netAssertf(false, "netXblHttpRequestStream::Write should not be called");
	return S_FALSE;
}

void*
netXblHttpRequestStream::Allocate(const unsigned numBytes) const
{

    if(m_Allocator)
    {
#if __FINAL
		const char* fileName = nullptr;
		int lineNum = 0;
#else
		const char* fileName = __FILE__;
		int lineNum = __LINE__;
#endif

		return m_Allocator->TryLoggedAllocate(numBytes, sizeof(void*), 0, fileName, lineNum);
    }
    else
    {
        return rage_new u8[numBytes];
    }
}

void
netXblHttpRequestStream::Free(void* ptr) const
{
    if(ptr)
    {
        if(m_Allocator)
        {
			netAssert(m_Allocator->IsValidPointer(ptr));
            m_Allocator->Free(ptr);
        }
        else
        {
            delete[] ptr;
        }
    }
}

netXblHttpRequestStream::Chunk*
netXblHttpRequestStream::NewChunk(const u8* data,
								  const unsigned dataLen,
								  netStatus* status)
{
    Chunk* chunk = (Chunk*) this->Allocate(sizeof(Chunk));
    if(chunk)
    {
        new (chunk) Chunk(data, dataLen, status);
    }

    return chunk;
}

void
netXblHttpRequestStream::DeleteChunk(Chunk* chunk)
{
    if(chunk)
    {
        chunk->~Chunk();
        this->Free((char*) chunk);
    }
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpRequestStream::QueueChunk
//  Desc: Queues a chunk of data to send.
//--------------------------------------------------------------------------------------
STDMETHODIMP
netXblHttpRequestStream::QueueChunk(const u8* data,
									const unsigned dataLen,
									netStatus* status)
{
	SYS_CS_SYNC(m_RequestStreamCs);

	if(dataLen > 0)
	{
		Chunk* chunk = NewChunk(data, dataLen, status);
		if(!netVerifyf(chunk, "Cannot allocate memory for a new chunk"))
		{
			if(status)
			{
				status->ForceFailed();
			}

			return STG_E_MEDIUMFULL;
		}

		if(status)
		{
			status->SetPending();
		}

		m_RequestLength += chunk->m_DataLen;

		m_QueuedChunks.push_back(chunk);
	}
	else
	{
		if(status)
		{
			status->ForceSucceeded();
		}
	}

	return S_OK;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpRequestStream::QueryInterface
//  Desc: IUnknown overload: Queries for a particular interface
//--------------------------------------------------------------------------------------
STDMETHODIMP
netXblHttpRequestStream::QueryInterface(REFIID riid,
										void **ppvObject)
{
    if(ppvObject == nullptr)
    { 
        return E_INVALIDARG;
    }

	*ppvObject = nullptr;

	HRESULT hr = S_OK;
    void *pObject = nullptr;

    if (riid == IID_IUnknown)
    {
        pObject = static_cast<IUnknown *>((IDispatch*)this);
    }
	else if (riid == IID_IDispatch)
    {
        pObject = static_cast<IDispatch *>(this);
    }
    else if (riid == IID_ISequentialStream)
    {
        pObject = static_cast<ISequentialStream *>(this);
    }
    else 
    {
        return E_NOINTERFACE;
    }

    AddRef();

    *ppvObject = pObject;
    pObject = nullptr;

    return hr;
} 

//--------------------------------------------------------------------------------------
//  Name: netXblHttpRequestStream::GetTypeInfoCount
//  Desc: IDispatch overload: IXMLHTTPRequest2 expects a complete IDispatch interface,
//  but doesn't actually make use of this.
//--------------------------------------------------------------------------------------
HRESULT
netXblHttpRequestStream::GetTypeInfoCount(unsigned int* pctinfo)
{
    if(pctinfo)
    {
        *pctinfo = 0;
    }

    return E_NOTIMPL;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpRequestStream::GetTypeInfo
//  Desc: IDispatch overload: IXMLHTTPRequest2 expects a complete IDispatch interface,
//  but doesn't actually make use of this.
//--------------------------------------------------------------------------------------
HRESULT
netXblHttpRequestStream::GetTypeInfo(unsigned int UNUSED_PARAM(iTInfo),
									 LCID UNUSED_PARAM(lcid),
									 ITypeInfo** ppTInfo)
{
    if(ppTInfo)
    {
        *ppTInfo = nullptr;
    }

    return E_NOTIMPL;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpRequestStream::GetIDsOfNames
//  Desc: IDispatch overload: IXMLHTTPRequest2 expects a complete IDispatch interface,
//  but doesn't actually make use of this.
//--------------------------------------------------------------------------------------
HRESULT
netXblHttpRequestStream::GetIDsOfNames(REFIID UNUSED_PARAM(riid),
									   OLECHAR** UNUSED_PARAM(rgszNames), 
									   unsigned int UNUSED_PARAM(cNames),
									   LCID UNUSED_PARAM(lcid),
									   DISPID* UNUSED_PARAM(rgDispId))
{
    return DISP_E_UNKNOWNNAME;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpRequestStream::Invoke
//  Desc: IDispatch overload: IXMLHTTPRequest2 expects a complete IDispatch interface,
//  but doesn't actually make use of this.
//--------------------------------------------------------------------------------------
HRESULT
netXblHttpRequestStream::Invoke(DISPID UNUSED_PARAM(dispIdMember),
								REFIID UNUSED_PARAM(riid),
								LCID UNUSED_PARAM(lcid),
								WORD UNUSED_PARAM(wFlags),
								DISPPARAMS* UNUSED_PARAM(pDispParams),
								VARIANT* UNUSED_PARAM(pVarResult),
								EXCEPINFO* UNUSED_PARAM(pExcepInfo),
								unsigned int* UNUSED_PARAM(puArgErr))
{
    return S_OK;
}

///////////////////////////////////////////////////////////////////////////////
//  netXblHttpResponseStream
//  Encapsulates a request data stream. It inherits ISequentialStream,
//  which the IXMLHTTPRequest2 class uses to read from our buffer. It also 
//  inherits IDispatch, which the IXMLHTTPRequest2 interface on Durango requires 
//  (unlike on Windows, where only ISequentialStream is necessary).
///////////////////////////////////////////////////////////////////////////////
class netXblHttpResponseStream
	: public Microsoft::WRL::RuntimeClass<RuntimeClassFlags<ClassicCom>,
										  ISequentialStream,
										  IDispatch>
{
public:

#if !__FINAL
	static unsigned sm_NumEventsCreated;
#endif

	netXblHttpResponseStream();
	~netXblHttpResponseStream();

	// ISequentialStream
	STDMETHODIMP Read(void *pv, ULONG cb, ULONG *pcbRead);
	STDMETHODIMP Write(const void *pv,  ULONG cb, ULONG *pcbWritten);

	//IUnknown
	STDMETHODIMP QueryInterface(REFIID riid, void **ppvObject);

	//IDispatch
	STDMETHODIMP GetTypeInfoCount(unsigned int FAR* pctinfo);
	STDMETHODIMP GetTypeInfo(unsigned int iTInfo,
							 LCID lcid,
							 ITypeInfo FAR* FAR* ppTInfo);
	STDMETHODIMP GetIDsOfNames(REFIID riid,
							   OLECHAR FAR* FAR* rgszNames,
							   unsigned int cNames,
							   LCID lcid,
							   DISPID FAR* rgDispId);
	STDMETHODIMP Invoke(DISPID dispIdMember,
						REFIID riid,
						LCID lcid,
						WORD wFlags,
						DISPPARAMS FAR* pDispParams,
						VARIANT FAR* pVarResult,
						EXCEPINFO FAR* pExcepInfo,
						unsigned int FAR* puArgErr);

	void* Allocate(const unsigned numBytes) const;
	void Free(void* ptr) const;

	// netXblHttpResponseStream
	bool Init(sysMemAllocator* allocator);
	unsigned Length();
	void Clear();
	void Cancel();

private:
	static const unsigned RCV_BUF_SIZE = 8 * 1024;

	sysMemAllocator* m_Allocator;
	void* m_RcvBufMem;
	datGrowBuffer m_RcvBuffer;
	sysIpcEvent m_Event;
	bool m_Cancelled;
	sysCriticalSectionToken m_ResponseStreamCs;

#if !__FINAL
	sysCriticalSectionToken m_WriteCs;
#endif
};

#if !__FINAL
unsigned netXblHttpResponseStream::sm_NumEventsCreated = 0;
#endif

netXblHttpResponseStream::netXblHttpResponseStream()
: m_Allocator(nullptr)
, m_RcvBufMem(nullptr)
, m_Event(nullptr)
, m_Cancelled(false)
{
}

netXblHttpResponseStream::~netXblHttpResponseStream()
{
#if !__FINAL
	// trying to help determine whether url:bugstar:5537746 is caused by this code.
	// - making sure we're not waiting on the event before deleting it here. See comments below.
	bool gotLock = m_WriteCs.TryLock();
	if(netVerify(gotLock))
	{
		m_WriteCs.Unlock();
	}

	bool eventSignaled = false;
#endif

	if(m_Event)
	{
		/*
			It is not valid to call sysIpcSetEvent while something is waiting on the event.
			This destructor gets called when all references are released, so it should not
			be possible to get here if netXblHttpResponseStream::Write() is waiting on the
			event - since the XHR object would still be holding a reference.

			Note that it is possible to create the event (in the non-signaled state) and delete
			the event without signaling it first (e.g. if the HTTP request is aborted early).
			So the event may currently be in the signaled or non-signaled state. As requested
			by systems, we want events to be non-signaled before deleting them, hence the poll.
		*/
		NOTFINAL_ONLY(eventSignaled = )sysIpcPollEvent(m_Event);
		sysIpcDeleteEvent(m_Event);

#if !__FINAL
		sysInterlockedDecrement(&sm_NumEventsCreated);
#endif

		m_Event = nullptr;
	}

#if !__FINAL
	netDebug("~netXblHttpResponseStream(): gotLock: %s, event signaled: %s, num events: %u", gotLock ? "true" : "false", eventSignaled ? "true" : "false", sm_NumEventsCreated);
#endif

	m_RcvBuffer.Clear();

	if(m_RcvBufMem)
	{
		this->Free(m_RcvBufMem);
		m_RcvBufMem = nullptr;
	}

	m_Allocator = nullptr;
}

void*
netXblHttpResponseStream::Allocate(const unsigned numBytes) const
{
    if(m_Allocator)
    {
#if __FINAL
		const char* fileName = nullptr;
		int lineNum = 0;
#else
		const char* fileName = __FILE__;
		int lineNum = __LINE__;
#endif

		return m_Allocator->TryLoggedAllocate(numBytes, sizeof(void*), 0, fileName, lineNum);
    }
    else
    {
        return rage_new u8[numBytes];
    }
}

void
netXblHttpResponseStream::Free(void* ptr) const
{
    if(ptr)
    {
        if(m_Allocator)
        {
			netAssert(m_Allocator->IsValidPointer(ptr));
            m_Allocator->Free(ptr);
        }
        else
        {
            delete[] ptr;
        }
    }
}

bool 
netXblHttpResponseStream::Init(sysMemAllocator* allocator)
{
	rtry
	{
		SYS_CS_SYNC(m_ResponseStreamCs);

		m_Allocator = allocator;

		// the receive buffer is fixed-length. IXHR2 streams the response to this buffer in netXblHttpResponseStream::Write()
		// while the response is consumed via calls to netXblHttpRequest::Impl::ReadData(). If IXHR2 streams faster than 
		// we can consume it, we yield the IXHR2 thread until we have consumed data and freed up capacity.
		// Note: this must be allocated on the main thread, since the supplied allocator is not guaranteed to be thread safe.
		m_RcvBufMem = this->Allocate(RCV_BUF_SIZE);
		rverify(m_RcvBufMem, catchall, netError("Error allocating %u bytes for receive buffer", RCV_BUF_SIZE));
		m_RcvBuffer.Init(m_RcvBufMem, RCV_BUF_SIZE, datGrowBuffer::FIXED_BUFFER);

		if(!PARAM_nethttpnoipcevent.Get())
		{
			m_Event = sysIpcCreateEvent();
			rverify(m_Event, catchall, netError("Failed to create event"));

#if !__FINAL
			// trying to help determine whether url:bugstar:5537746 is caused by this code.
			// - making sure we're not leaking events
			// Note that there can be more in-flight XBL HTTP requests than netHttpRequests because XBL HTTP requests are queued
			// for deletion on a separate thread and it takes time to process them. The max number of pending requests is unknown,
			// but sm_NumEventsCreated should trend towards 0 in the log when there are no in-flight HTTP requests.
			static const unsigned MAX_IN_FLIGHT_REQUESTS = 32;
			if(!netVerify(sysInterlockedIncrement(&sm_NumEventsCreated) <= MAX_IN_FLIGHT_REQUESTS))
			{
				netWarning("Number of events: %u, exceeds expected maximum of %u", sm_NumEventsCreated, MAX_IN_FLIGHT_REQUESTS);
			}
#endif
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

unsigned 
netXblHttpResponseStream::Length()
{
	SYS_CS_SYNC(m_ResponseStreamCs);
	return m_RcvBuffer.Length();
}

void
netXblHttpResponseStream::Clear()
{
	SYS_CS_SYNC(m_ResponseStreamCs);
	m_RcvBuffer.Clear();
}

void 
netXblHttpResponseStream::Cancel()
{
	m_Cancelled = true;

	if(m_Event)
	{
		// unblock the receive thread if it's waiting
		sysIpcSetEvent(m_Event);
	}
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpResponseStream::Read
//  Desc: ISequentialStream overload: Reads data from the buffer
//--------------------------------------------------------------------------------------
STDMETHODIMP
netXblHttpResponseStream::Read(void *pv,
							  ULONG cb,
							  ULONG *pcbNumReadBytes)
{
	SYS_CS_SYNC(m_ResponseStreamCs);

    if(pv == nullptr)
    {
        return E_INVALIDARG;
    }

	// buffer will be null if nothing is written yet
	if((m_RcvBuffer.GetBuffer() != nullptr) && (m_RcvBuffer.Length() > 0))
	{
		const fiDevice* device = m_RcvBuffer.GetFiDevice();
		*pcbNumReadBytes = device->Read(m_RcvBuffer.GetFiHandle(), pv, cb);
	}
	else
	{
		*pcbNumReadBytes = 0;
	}

#if XBL_HTTP_VERBOSE_OUTPUT
	netDebug3("netXblHttpResponseStream::Read() %p: capacity is currently %u bytes, read %u bytes, leaving %u bytes in the buffer", this, m_RcvBuffer.GetCapacity(), *pcbNumReadBytes, m_RcvBuffer.Length());
#endif

	if((m_RcvBuffer.GetCapacity() - m_RcvBuffer.Length()) > 0)
	{
		if(m_Event)
		{
			// signal the IXHR2 thread that we have capacity to receive more data
			sysIpcSetEvent(m_Event);
		}
	}

	// S_FALSE indicates the end of the stream has been reached
	return *pcbNumReadBytes == cb ? S_OK : S_FALSE;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpResponseStream::Write
//  Desc: ISequentialStream overload: Writes to the buffer.
//--------------------------------------------------------------------------------------
STDMETHODIMP
netXblHttpResponseStream::Write(const void *pv,
							   ULONG cb,
							   ULONG *pcbWritten)
{
	// we have no control over how fast IXHR2 feeds us data.
	// we can end up running out of memory if the calling code
	// doesn't consume the data from the buffer fast enough.
	// if we have too much data to consume, we put the thread
	// that's feeding us to sleep until we've consumed the data.

#if !__FINAL
	SYS_CS_SYNC(m_WriteCs);
#endif

	if(pv == nullptr)
	{
		return E_INVALIDARG;
	}
	
	unsigned remainingBytes = cb;

	while(remainingBytes > 0)
	{
		if(m_Cancelled)
		{
#if XBL_HTTP_VERBOSE_OUTPUT
			netDebug3("Leaving netXblHttpResponseStream::Write() %p. Request cancelled.", this);
#endif
			return STG_E_TERMINATED;
		}
		else
		{
			SYS_CS_SYNC(m_ResponseStreamCs);

#if XBL_HTTP_VERBOSE_OUTPUT
			netDebug3("netXblHttpResponseStream::Write() %p: capacity is currently %u bytes, length is %u bytes, receiving %u bytes", this, m_RcvBuffer.GetCapacity(), m_RcvBuffer.Length(), cb);
#endif

			unsigned bytesToWrite = Min(m_RcvBuffer.GetCapacity() - m_RcvBuffer.Length(), (unsigned)remainingBytes);

			if(bytesToWrite > 0)
			{
				unsigned bytesWritten = m_RcvBuffer.Append(((u8*)pv) + *pcbWritten, bytesToWrite);
				netAssert(bytesWritten == bytesToWrite);
				remainingBytes -= bytesToWrite;
				*pcbWritten += bytesWritten;
			}
		}

		if(remainingBytes > 0)
		{
#if XBL_HTTP_VERBOSE_OUTPUT
			unsigned startTime = sysTimer::GetSystemMsTime();
			netDebug3("BLOCKING IXHR2 %p: HTTP response stream capacity: %u bytes, length is %u bytes, remainingBytes: %u", this, m_RcvBuffer.GetCapacity(), m_RcvBuffer.Length(), remainingBytes);
#endif
			if(m_Event)
			{
				sysIpcWaitEvent(m_Event);
			}
			else
			{
				sysIpcSleep(10);
			}
#if XBL_HTTP_VERBOSE_OUTPUT
			netDebug3("UNBLOCKING IXHR2 %p (was blocked for %u ms): HTTP response stream capacity: %u bytes, length is %u bytes, remainingBytes: %u.", this, sysTimer::GetSystemMsTime() - startTime, m_RcvBuffer.GetCapacity(), m_RcvBuffer.Length(), remainingBytes);
#endif
		}
	}

	return netVerify(*pcbWritten == cb) ? S_OK : STG_E_MEDIUMFULL;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpResponseStream::QueryInterface
//  Desc: IUnknown overload: Queries for a particular interface
//--------------------------------------------------------------------------------------
STDMETHODIMP
netXblHttpResponseStream::QueryInterface(REFIID riid,
										void **ppvObject)
{
    if(ppvObject == nullptr)
    { 
        return E_INVALIDARG;
    }

	*ppvObject = nullptr;

	HRESULT hr = S_OK;
    void *pObject = nullptr;

    if (riid == IID_IUnknown)
    {
        pObject = static_cast<IUnknown *>((IDispatch*)this);
    }
	else if (riid == IID_IDispatch)
    {
        pObject = static_cast<IDispatch *>(this);
    }
    else if (riid == IID_ISequentialStream)
    {
        pObject = static_cast<ISequentialStream *>(this);
    }
    else 
    {
        return E_NOINTERFACE;
    }

    AddRef();

    *ppvObject = pObject;
    pObject = nullptr;

    return hr;
} 

//--------------------------------------------------------------------------------------
//  Name: netXblHttpResponseStream::GetTypeInfoCount
//  Desc: IDispatch overload: IXMLHTTPRequest2 expects a complete IDispatch interface,
//  but doesn't actually make use of this.
//--------------------------------------------------------------------------------------
HRESULT
netXblHttpResponseStream::GetTypeInfoCount(unsigned int* pctinfo)
{
    if(pctinfo)
    {
        *pctinfo = 0;
    }

    return E_NOTIMPL;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpResponseStream::GetTypeInfo
//  Desc: IDispatch overload: IXMLHTTPRequest2 expects a complete IDispatch interface,
//  but doesn't actually make use of this.
//--------------------------------------------------------------------------------------
HRESULT
netXblHttpResponseStream::GetTypeInfo(unsigned int UNUSED_PARAM(iTInfo),
									 LCID UNUSED_PARAM(lcid),
									 ITypeInfo** ppTInfo)
{
    if(ppTInfo)
    {
        *ppTInfo = nullptr;
    }

    return E_NOTIMPL;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpResponseStream::GetIDsOfNames
//  Desc: IDispatch overload: IXMLHTTPRequest2 expects a complete IDispatch interface,
//  but doesn't actually make use of this.
//--------------------------------------------------------------------------------------
HRESULT
netXblHttpResponseStream::GetIDsOfNames(REFIID UNUSED_PARAM(riid),
									   OLECHAR** UNUSED_PARAM(rgszNames), 
									   unsigned int UNUSED_PARAM(cNames),
									   LCID UNUSED_PARAM(lcid),
									   DISPID* UNUSED_PARAM(rgDispId))
{
    return DISP_E_UNKNOWNNAME;
}

//--------------------------------------------------------------------------------------
//  Name: netXblHttpResponseStream::Invoke
//  Desc: IDispatch overload: IXMLHTTPRequest2 expects a complete IDispatch interface,
//  but doesn't actually make use of this.
//--------------------------------------------------------------------------------------
HRESULT
netXblHttpResponseStream::Invoke(DISPID UNUSED_PARAM(dispIdMember),
								REFIID UNUSED_PARAM(riid),
								LCID UNUSED_PARAM(lcid),
								WORD UNUSED_PARAM(wFlags),
								DISPPARAMS* UNUSED_PARAM(pDispParams),
								VARIANT* UNUSED_PARAM(pVarResult),
								EXCEPINFO* UNUSED_PARAM(pExcepInfo),
								unsigned int* UNUSED_PARAM(puArgErr))
{
    return S_OK;
}

///////////////////////////////////////////////////////////////////////////////
//  netXblHttpCallback
///////////////////////////////////////////////////////////////////////////////
class netXblHttpCallback
	: public RuntimeClass<RuntimeClassFlags<ClassicCom>,
						  IXMLHTTPRequest2Callback>
{
public:
    // Required functions
    STDMETHODIMP OnRedirect(IXMLHTTPRequest2 *pXHR, const WCHAR *pwszRedirectUrl);
    STDMETHODIMP OnHeadersAvailable(IXMLHTTPRequest2 *pXHR, DWORD dwStatus, const WCHAR *pwszStatus);
    STDMETHODIMP OnDataAvailable(IXMLHTTPRequest2 *pXHR, ISequentialStream *pResponseStream);
    STDMETHODIMP OnResponseReceived(IXMLHTTPRequest2 *pXHR, ISequentialStream *pResponseStream);
    STDMETHODIMP OnError(IXMLHTTPRequest2 *pXHR, HRESULT hrError);

	bool IsFinished() const;
	bool HasError() const;
	s32 GetErrorCode() const;

	bool HaveHeaders() const;
	bool GetResponseHeaderLength(unsigned* headerLen) const;

	// once the headers are read, they are considered to be consumed. Do not call more than once.
	bool GetHeaders(char* headerBuf, unsigned* headerLen) const;

	void* Allocate(const unsigned numBytes) const;
	void Free(void* ptr) const;

    netXblHttpCallback();
    ~netXblHttpCallback();
	bool Init(sysMemAllocator* allocator);

    friend HRESULT MakeAndInitialize<netXblHttpCallback,netXblHttpCallback>(netXblHttpCallback**);

private:
	static const unsigned RCV_BUF_SIZE = 8 * 1024;

	sysMemAllocator* m_Allocator;
	mutable void* m_RcvBufMem;
	mutable datGrowBuffer m_RcvBuffer;
	HRESULT m_ErrorHresult;
    bool m_Finished;
	bool m_HaveHeaders;
	unsigned m_HeadersLength;
	bool m_WasRedirected;
	mutable sysCriticalSectionToken m_HttpCallbackCs;
};

netXblHttpCallback::netXblHttpCallback()
: m_Allocator(nullptr)
, m_RcvBufMem(nullptr)
, m_ErrorHresult(S_OK)
, m_Finished(false)
, m_HaveHeaders(false)
, m_HeadersLength(0)
, m_WasRedirected(false)
{

}

netXblHttpCallback::~netXblHttpCallback()
{
	m_RcvBuffer.Clear();

	if(m_RcvBufMem)
	{
		this->Free(m_RcvBufMem);
		m_RcvBufMem = nullptr;
	}

	m_Allocator = nullptr;
}

void*
netXblHttpCallback::Allocate(const unsigned numBytes) const
{
    if(m_Allocator)
    {
#if __FINAL
		const char* fileName = nullptr;
		int lineNum = 0;
#else
		const char* fileName = __FILE__;
		int lineNum = __LINE__;
#endif

		return m_Allocator->TryLoggedAllocate(numBytes, sizeof(void*), 0, fileName, lineNum);
    }
    else
    {
        return rage_new u8[numBytes];
    }
}

void
netXblHttpCallback::Free(void* ptr) const
{
    if(ptr)
    {
        if(m_Allocator)
        {
			netAssert(m_Allocator->IsValidPointer(ptr));
            m_Allocator->Free(ptr);
        }
        else
        {
            delete[] ptr;
        }
    }
}

bool 
netXblHttpCallback::Init(sysMemAllocator* allocator)
{
	rtry
	{
		SYS_CS_SYNC(m_HttpCallbackCs);

		m_Allocator = allocator;
		
		// Note: this must be allocated on the main thread, since the supplied allocator is not guaranteed to be thread safe.
		m_RcvBufMem = this->Allocate(RCV_BUF_SIZE);
		rverify(m_RcvBufMem, catchall, netError("Error allocating %u bytes for receive buffer", RCV_BUF_SIZE));
		m_RcvBuffer.Init(m_RcvBufMem, RCV_BUF_SIZE, datGrowBuffer::FIXED_BUFFER);

		m_ErrorHresult = S_OK;
		m_Finished = false;
		m_HaveHeaders = false;
		m_HeadersLength = 0;
		m_WasRedirected = false;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netXblHttpCallback::IsFinished() const
{
	return m_Finished;
}

bool 
netXblHttpCallback::HasError() const
{
	return m_Finished && m_ErrorHresult != S_OK;
}

s32  
netXblHttpCallback::GetErrorCode() const
{
	return m_ErrorHresult;
}

bool 
netXblHttpCallback::HaveHeaders() const
{
	return m_HaveHeaders;
}

bool 
netXblHttpCallback::GetResponseHeaderLength(unsigned* headerLen) const
{
	rtry
	{
		rverify(headerLen, catchall, );
		*headerLen = m_HeadersLength;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netXblHttpCallback::GetHeaders(char* headerBuf, unsigned* headerLen) const
{
	rtry
	{
		// once the headers are read, they are considered to be consumed and no longer stored here.

		SYS_CS_SYNC(m_HttpCallbackCs);

		rverify(HaveHeaders(), catchall, netError("Headers are not available"));

		unsigned actualHeaderLen = 0;
		rverify(GetResponseHeaderLength(&actualHeaderLen), catchall, );

		rverify(headerBuf, catchall, );
		rverify(headerLen && (*headerLen >= actualHeaderLen), catchall, );
		
		rverify((m_RcvBuffer.GetBuffer() != nullptr) && (m_RcvBuffer.Length() >= actualHeaderLen), catchall,);

		const fiDevice* device = m_RcvBuffer.GetFiDevice();
		unsigned bytesRead = device->Read(m_RcvBuffer.GetFiHandle(), headerBuf, actualHeaderLen);
		rverify(bytesRead == actualHeaderLen, catchall, );

		m_RcvBuffer.Clear();

		if(m_RcvBufMem)
		{
			this->Free(m_RcvBufMem);
			m_RcvBufMem = nullptr;
		}

		*headerLen = actualHeaderLen;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

// --------------------------------------------------------------------------------------
// Name: netXblHttpCallback::OnRedirect
//
// Desc: The requested URI was redirected by the HTTP server to a new URI.
//
// Arguments:
//     pXHR         - The interface pointer of originating IXMLHTTPRequest2 object.
//     pRedirectUrl - The new URL to for the request.
// --------------------------------------------------------------------------------------
IFACEMETHODIMP 
netXblHttpCallback::OnRedirect(IXMLHTTPRequest2* pXHR,
							   const wchar_t* pRedirectUrl)
{
	HRESULT hr = S_OK;

	rtry
	{
		if(pXHR == nullptr)
		{
			return E_INVALIDARG;
		}

		SYS_CS_SYNC(m_HttpCallbackCs);

		// the calling code in http.cpp handles all redirect calls
		// we're going to fabricate a header from the redirect
		// url and abort this request
		pXHR->Abort();

		unsigned urlLen = (unsigned)wcslen(pRedirectUrl);
		char* url = (char*)Alloca(char, urlLen + 1);
		WideToAscii(url, (char16*)pRedirectUrl, urlLen + 1);

		const char redirectHeaderFmt[] = "HTTP/1.1 301 Moved Permanently\r\n"
										 "Location: %s\r\n"
										 "Content-Length: 0\r\n\r\n";

		unsigned len = ustrlen(redirectHeaderFmt) + urlLen + 1;

		rverify(m_RcvBuffer.GetCapacity() > len,
				catchall,
				m_ErrorHresult = E_OUTOFMEMORY;
				netError("netXblHttpCallback::OnRedirect(): receive buffer is too small for the HTTP response headers"));

		char *asciiHeaders = (char*)Alloca(char, len);
		rverify(asciiHeaders, catchall, m_ErrorHresult = E_OUTOFMEMORY; netError("netXblHttpCallback::OnRedirect(): Error allocating %u bytes for HTTP response headers", len));
		sysMemSet(asciiHeaders, 0, len);

		formatf_sized(asciiHeaders, len, redirectHeaderFmt, url);

		m_HeadersLength = ustrlen(asciiHeaders) + 1; // +1 for null terminator

		rverify(m_RcvBuffer.Append(asciiHeaders, m_HeadersLength) == (int)m_HeadersLength,
				catchall,
				m_ErrorHresult = E_OUTOFMEMORY;
				netError("netXblHttpCallback::OnRedirect(): failed to append headers"));

		m_WasRedirected = true;

		// set this last since we're on a different thread and the calling
		// code needs to know when it can access the headers.
		m_HaveHeaders = true;
	}
	rcatchall
	{
		if(m_ErrorHresult == S_OK)
		{
			m_ErrorHresult = E_FAIL;
		}

		if(pXHR)
		{
			pXHR->Abort();
		}
	}

	return hr;
};

// --------------------------------------------------------------------------------------
// Name: netXblHttpCallback::OnHeadersAvailable
//
// Desc: The HTTP Headers have been downloaded and are ready for parsing. The string that is
//       returned is owned by this function and should be copied or deleted before exit.
//
// Arguments:
//     pXHR       - The interface pointer of originating IXMLHTTPRequest2 object.
//     dwStatus   - The value of HTTP status code, e.g. 200, 404
//     pwszStatus - The description text of HTTP status code.
// --------------------------------------------------------------------------------------
IFACEMETHODIMP
netXblHttpCallback::OnHeadersAvailable(IXMLHTTPRequest2 *pXHR,
									   DWORD dwStatus,
									   const wchar_t *pwszStatus)
{
	HRESULT hr = S_OK;
	wchar_t* headers = nullptr;

	rtry
	{
		SYS_CS_SYNC(m_HttpCallbackCs);

		netAssertf(m_RcvBuffer.Length() == 0, "Response data received before headers?");

		if(pXHR == nullptr)
		{
			return E_INVALIDARG;
		}

		hr = pXHR->GetAllResponseHeaders(&headers);
		rcheck(SUCCEEDED(hr),
				catchall,
				netError("netXblHttpCallback::OnHeadersAvailable(): error calling pXHR->GetAllResponseHeaders(): 0x%08x", hr));

		rverify(headers != nullptr, catchall, );

		// copy the headers
		// the calling code in http.cpp wants the status line as well
		// we don't get back the HTTP version, so I'm faking it here
		wchar_t status[256] = {0};
		formatf(status, L"HTTP/1.1 %d %ls\r\n", dwStatus, pwszStatus);

		unsigned headersLen = (unsigned)wcslen(headers);
		unsigned statusLen = (unsigned)wcslen(status);
		unsigned len = headersLen + statusLen + 1;

		rverify(m_RcvBuffer.GetCapacity() > len,
				catchall,
				m_ErrorHresult = E_OUTOFMEMORY;
				netError("netXblHttpCallback::OnHeadersAvailable(): receive buffer is too small for the HTTP response headers"));

		char *asciiHeaders = (char*)Alloca(char, len);
		rverify(asciiHeaders,
				catchall,
				m_ErrorHresult = E_OUTOFMEMORY;
				netError("netXblHttpCallback::OnHeadersAvailable(): Error allocating %u bytes for HTTP response headers", len));

		sysMemSet(asciiHeaders, 0, len);
		WideToAscii(asciiHeaders, (char16*)status, statusLen + 1);
		WideToAscii(asciiHeaders + statusLen, (char16*)headers, headersLen + 1);
		asciiHeaders[len - 1] = '\0';
		m_HeadersLength = ustrlen(asciiHeaders) + 1; // +1 for null terminator

		rverify(m_RcvBuffer.Append(asciiHeaders, m_HeadersLength) == (int)m_HeadersLength,
				catchall,
				m_ErrorHresult = E_OUTOFMEMORY;
				netError("netXblHttpCallback::OnHeadersAvailable(): failed to append headers"));

		hr = S_OK;

		// set this last since we're on a different thread and the calling
		// code needs to know when it can access the headers.
		m_HaveHeaders = true;
	}
	rcatchall
	{
		if(m_ErrorHresult == S_OK)
		{
			m_ErrorHresult = E_FAIL;
		}

		if(pXHR)
		{
			pXHR->Abort();
		}
	}

	// the header string that was retrieved needs to be deleted here
	if(headers != nullptr)
	{
		::CoTaskMemFree(headers);
		headers = nullptr;
	}

	return hr;
}

// --------------------------------------------------------------------------------------
// Name: netXblHttpCallback::OnDataAvailable
//
// Desc: Part of the HTTP Data payload is available, we can start processing it
//       here or copy it off and wait for the whole request to finish loading.
//
// Arguments:
//    pXHR            - Pointer to the originating IXMLHTTPRequest2 object.
//    pResponseStream - Pointer to the input stream, which may only be part of the
//                      whole stream.
// --------------------------------------------------------------------------------------
IFACEMETHODIMP
netXblHttpCallback::OnDataAvailable(IXMLHTTPRequest2* UNUSED_PARAM(pXHR),
									ISequentialStream* UNUSED_PARAM(pResponseStream))
{
	// nothing to do here since the data is written directly to our custom stream
	return S_OK;
}

// --------------------------------------------------------------------------------------
// Name: netXblHttpCallback::OnResponseReceived
//
// Desc: Called when the entire body has been received.
//       At this point the application can begin processing the data by calling
//       ISequentialStream::Read on the pResponseStream or store a reference to
//       the ISequentialStream for later processing.
//
// Arguments:
//    pXHR            - Pointer to the originating IXMLHTTPRequest2 object.
//    pResponseStream - Pointer to the complete input stream.
// --------------------------------------------------------------------------------------
IFACEMETHODIMP
netXblHttpCallback::OnResponseReceived(IXMLHTTPRequest2* UNUSED_PARAM(pXHR),
									   ISequentialStream* UNUSED_PARAM(pResponseStream))
{
	m_Finished = true;

	return S_OK;
}

// --------------------------------------------------------------------------------------
// Name: netXblHttpCallback::OnError
// Desc: Handle errors that have occurred during the HTTP request.
// Arguments:
//    pXHR - The interface pointer of IXMLHTTPRequest2 object.
//    hrError - The errocode for the httprequest.
// --------------------------------------------------------------------------------------
IFACEMETHODIMP
netXblHttpCallback::OnError(IXMLHTTPRequest2* UNUSED_PARAM(pXHR),
							HRESULT hrError)
{
	if((hrError == STG_E_TERMINATED) || (hrError == E_ABORT))
	{
		netWarning("netXblHttpCallback :: OnError (0x%08x)", hrError);
	}
	else
	{
		netError("netXblHttpCallback :: OnError (0x%08x)", hrError);
	}

	// the request is finished, but an error occurred
	m_Finished = true;

	// if the request was redirected, we'll get an error here
	// signaling that the request was aborted successfully
	if(m_WasRedirected == false)
	{
		// hrError is usually just E_ABORTED. If we already set an error, don't overwrite it here.
		if(m_ErrorHresult == S_OK)
		{
			m_ErrorHresult = hrError;
		}
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////
//  netXblHttpRequest::Impl
///////////////////////////////////////////////////////////////////////////////

class netXblHttpRequest::Impl
{
public:
	Impl();
	~Impl();

	bool Init(sysMemAllocator* allocator);

    bool Open(const char* verb,
			  const char* uri);

	bool AddRequestHeaderValue(const char* name,
							   const unsigned nameLen,
							   const char* value,
							   const unsigned valLen);

	bool SetRequestHeader(const char* headers,
						  const unsigned length);
	
	bool QueueChunk(const u8* data,
					const unsigned dataLen,
					netStatus* status);

	bool Commit();

	void Cancel();
	void Clear();

	bool IsFinished() const;
	bool HasError() const;
	s32 GetErrorCode() const;
	bool HaveHeaders() const;
    bool GetResponseHeaderLength(unsigned* headerLen) const;
    bool GetResponseHeader(char* headerBuf,
						   unsigned* headerLen) const;

	bool ReadData(u8* data,
				  const unsigned maxLen,
				  unsigned* numRead,
				  bool* allContentRead);
private:
	static void QueueForDeletion(netXblHttpRequest::Impl* impl);

	void* Allocate(const unsigned numBytes) const;
	void Free(void* ptr) const;
	bool SetHeadersHelper();

	ComPtr<IXMLHTTPRequest2> m_XHR;
	ComPtr<IXMLHTTPRequest2Callback> m_XHRCallback;
	ComPtr<netXblHttpCallback> m_HttpCallback;
	sysMemAllocator* m_Allocator;
	ComPtr<netXblHttpRequestStream> m_RequestStream;
	ComPtr<netXblHttpResponseStream> m_ResponseStream;
	char* m_Verb;
	char* m_Uri;
	char* m_Headers;
	netStatus m_XblHttpTaskStatus;
	sysCriticalSectionToken m_XhrCs;
	bool m_QueuedForDeletion;
};

netXblHttpRequest::Impl::Impl()
: m_XHR(nullptr)
, m_XHRCallback(nullptr)
, m_HttpCallback(nullptr)
, m_Allocator(nullptr)
, m_RequestStream(nullptr)
, m_ResponseStream(nullptr)
, m_Verb(nullptr)
, m_Uri(nullptr)
, m_Headers(nullptr)
, m_QueuedForDeletion(false)
{

}

netXblHttpRequest::Impl::~Impl()
{
	netAssert(m_Verb == nullptr);
	netAssert(m_Uri == nullptr);
	netAssert(m_Headers == nullptr);
	netAssert(!m_XblHttpTaskStatus.Pending());

	m_XHR = nullptr;
	m_XHRCallback = nullptr;
	m_HttpCallback = nullptr;
	m_Allocator = nullptr;
	m_RequestStream = nullptr;
	m_ResponseStream = nullptr;
	m_Verb = nullptr;
	m_Uri = nullptr;
	m_Headers = nullptr;
}

void*
netXblHttpRequest::Impl::Allocate(const unsigned numBytes) const
{
    if(m_Allocator)
    {
#if __FINAL
		const char* fileName = nullptr;
		int lineNum = 0;
#else
		const char* fileName = __FILE__;
		int lineNum = __LINE__;
#endif

		return m_Allocator->TryLoggedAllocate(numBytes, sizeof(void*), 0, fileName, lineNum);
    }
    else
    {
        return rage_new u8[numBytes];
    }
}

void
netXblHttpRequest::Impl::Free(void* ptr) const
{
    if(ptr)
    {
        if(m_Allocator)
        {
			netAssert(m_Allocator->IsValidPointer(ptr));
            m_Allocator->Free(ptr);
        }
        else
        {
            delete[] ptr;
        }
    }
}

bool netXblHttpRequest::Impl::Init(sysMemAllocator* allocator)
{
	bool success = false;

	try
	{
		rtry
		{
			m_Allocator = allocator;

			rverify(m_Allocator, catchall, );

			success = true;
		}
		rcatchall
		{
		}
	}
	catch(...)
	{

	}

	return success;
}

bool 
netXblHttpRequest::Impl::Open(const char* verb,
							  const char* uri)
{
	PROFILE

	bool success = false;

	try
	{
		rtry
		{
			rverify(m_Allocator, catchall, );
			
			// Note that this function doesn't actually make a connection to the web server. IXHR calls take a long time,
			// so they cannot be called on the main thread. Instead, we copy the verb and url, and prepare to perform 
			// the HTTP request on another thread when Commit() is called.

			// IXHR2 is threaded. The HTTP callback, request stream, and response stream methods are called from IXHR2's
			// internal threads. All memory allocs/frees that use the supplied m_Allocator MUST happen on the main thread,
			// since m_Allocator can be (and sometimes is) a non-thread-safe allocator.

			// These objects are ref counted COM ptrs, and can be Released()'ed from the IXHR2 thread. For that reason,
			// we allow the WRL to allocate these structs from the global allocator (assumed to be thread-safe) instead
			// of via m_Allocator. The buffers we pass to them are allocated via m_Allocator. These are owned by the
			// HTTP request object so they can be freed on the main thread.

			// WRL allocates memory with 'new' internally
			++RAGE_LOG_DISABLE;
		
			// create the IXmlHttpRequest2Callback object and initialize it.
			HRESULT hr = Microsoft::WRL::Details::MakeAndInitialize<netXblHttpCallback>(&m_HttpCallback);
			rverify(SUCCEEDED(hr), catchall, netError("::Details::MakeAndInitialize failed, 0x%08x", hr));

			hr = m_HttpCallback.As(&m_XHRCallback);
			rverify(SUCCEEDED(hr), catchall, netError("m_HttpCallback as m_XHRCallback failed, 0x%08x", hr));

			m_RequestStream = Make<netXblHttpRequestStream>();
			rverify(m_RequestStream != nullptr, catchall, );

			m_ResponseStream = Make<netXblHttpResponseStream>();
			rverify(m_ResponseStream != nullptr, catchall, );

			--RAGE_LOG_DISABLE;

			// Unlike the response, the request needs to be filled and passed to IXHR2 all at once (the request can't be
			// streamed to IXHR2). We don't know in advance how large the request will be, so we use a chunk system.
			// The size of the chunk list is only changed when adding data to the request, which happens on the main thread.

			rverify(m_HttpCallback->Init(m_Allocator), catchall, netError("Error initializing HTTP callback"));
			rverify(m_RequestStream->Init(m_Allocator), catchall, netError("Error initializing request stream"));
			rverify(m_ResponseStream->Init(m_Allocator), catchall, netError("Error initializing response stream"));

			// copy verb and uri so we can use them when we commit the request
			unsigned len = ustrlen(verb) + 1;
			m_Verb = (char*)this->Allocate(len);
			rverify(m_Verb, catchall, );
			safecpy(m_Verb, verb, len);

			len = ustrlen(uri) + 1;
			m_Uri = (char*)this->Allocate(len);
			rverify(m_Uri, catchall, );
			safecpy(m_Uri, uri, len);

			success = true;
		}
		rcatchall
		{
		}
	}
	catch(...)
	{

	}

	return success;
}

bool
netXblHttpRequest::Impl::AddRequestHeaderValue(const char* name,
											   const unsigned nameLen,
											   const char* value,
											   const unsigned valLen)
{
	PROFILE

	bool success = false;

	try
	{
		rtry
		{
			rverify(m_XHR, catchall, );

			unsigned bufLen = nameLen + 1;
			wchar_t* wszName = (wchar_t*)Alloca(wchar_t, bufLen);
			rverify(wszName, catchall, );
			Utf8ToWide((char16*)wszName, name, bufLen - 1);

			bufLen = valLen + 1;
			wchar_t* wszValue = (wchar_t*)Alloca(wchar_t, bufLen);
			rverify(wszValue, catchall, );
			Utf8ToWide((char16*)wszValue, value, bufLen - 1);

			HRESULT hr = m_XHR->SetRequestHeader(wszName, wszValue);
			rverify(SUCCEEDED(hr), catchall, netError("IXMLHttpRequest SetRequestHeader failed, 0x%08x", hr));

			success = true;
		}
		rcatchall
		{
		}
	}
	catch(...)
	{

	}

	return success;
}

bool 
netXblHttpRequest::Impl::SetRequestHeader(const char* headers,
										  const unsigned length)
{
	// copy the headers so we can use them when we commit the request
	rtry
	{
		m_Headers = (char*)this->Allocate(length);
		rverify(m_Headers, catchall,);
		safecpy(m_Headers, headers, length);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netXblHttpRequest::Impl::QueueChunk(const u8* data,
								    const unsigned dataLen,
								    netStatus* status)
{
	PROFILE

	bool success = false;

	try
	{
		rtry
		{
			HRESULT hr = m_RequestStream->QueueChunk(data, dataLen, status);
			rverify(SUCCEEDED(hr), catchall, netError("IXMLHttpRequest QueueChunk failed (length: %u), 0x%08x", dataLen, hr));
			success = true;
		}
		rcatchall
		{
		}
	}
	catch(...)
	{

	}

	return success;
}

bool
netXblHttpRequest::Impl::SetHeadersHelper()
{
	PROFILE

	bool success = false;

	rtry
	{
		rverify(m_Headers, catchall, );

		const char* line = m_Headers;

		const char* END = "\r\n\r\n";

		// skip the first line since http.cpp sends us the verb in the first line
		const char* eol = strstr(m_Headers, CRLF);
		rverify(eol, catchall, );

		do 
		{
			if(strncmp(eol, END, 4) == 0)
			{
				break;
			}

			line = eol + 2;

			eol = strstr(line, CRLF);
			const char* eof = strchr(line, ':');

			if(!eof){break;}
			if(!eol){break;}

			const unsigned nameLen = ptrdiff_t_to_int(eof - line);
			const char* name = line;

			// skip colon and spaces
			const char* v = eof + 1;
			while((v < eol) && isspace(*v))
			{
				++v;
			}

			const unsigned valLen = ptrdiff_t_to_int(eol - v);
			const char* val = v;

			rverify(AddRequestHeaderValue(name, nameLen, val, valLen), catchall, );
		} while(true);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool
netXblHttpRequest::Impl::Commit()
{
	PROFILE
		
	netTask2 httpTask = netTask2::ScheduleTask([this]()
	{
		// do not use m_Allocator in the lambda since it is not guaranteed to be thread safe.

		PROFILE

		if(netTask2::IsCancelRequested())
		{
			return netTaskState::Canceled;
		}

		SYS_CS_SYNC(m_XhrCs);

		try
		{
			rtry
			{
				// create the IXmlHttpRequest2 object.
				HRESULT hr = ::CoCreateInstance(__uuidof(FreeThreadedXMLHTTP60),
													nullptr,
													CLSCTX_SERVER,
													__uuidof(IXMLHTTPRequest2),
													&m_XHR);

				rverify(SUCCEEDED(hr), catchall, netError("::CoCreateInstance failed, 0x%08x", hr));
				rverify(m_XHR, catchall, );

				if(netTask2::IsCancelRequested())
				{
					return netTaskState::Canceled;
				}

				// let the calling code handle the timeouts by aborting us
				m_XHR->SetProperty(XHR_PROP_TIMEOUT, 0xFFFFFFFF);

				unsigned len = ustrlen(m_Verb) + 1;
				wchar_t* wszVerb = (wchar_t*)Alloca(wchar_t, len);
				rverify(wszVerb, catchall, );
				Utf8ToWide((char16*)wszVerb, m_Verb, len);

				len = ustrlen(m_Uri) + 1;
				wchar_t* wszUri = (wchar_t*)Alloca(wchar_t, len);
				rverify(wszUri, catchall, );
				Utf8ToWide((char16*)wszUri, m_Uri, len);

				hr = m_XHR->Open(wszVerb,				// HTTP method
								 wszUri,				// URI
								 m_XHRCallback.Get(),	// callback object from a ComPtr<>
								 nullptr,					// username
								 nullptr,					// password
								 nullptr,					// proxy username
								 nullptr);					// proxy password

				rverify(SUCCEEDED(hr), catchall, netError("IXMLHttpRequest Open failed, 0x%08x", hr));

				if(netTask2::IsCancelRequested())
				{
					return netTaskState::Canceled;
				}

				// Specifies the HTTP stack should never call OnDataAvailable.
				// MS recommends this setting if we don't require the OnDataAvailable callback since by default,
				// the callback (RPC) is called very frequently and slows down the request significantly.
				hr = m_XHR->SetProperty(XHR_PROP_ONDATA_THRESHOLD, XHR_PROP_ONDATA_NEVER);
				netAssertf(SUCCEEDED(hr), "xhr->SetProperty(XHR_PROP_ONDATA_THRESHOLD) failed: 0x%08x", hr); 

				// let the calling code handle the timeouts by aborting us
				hr = m_XHR->SetProperty(XHR_PROP_TIMEOUT, 0xFFFFFFFF);
				netAssertf(SUCCEEDED(hr), "xhr->SetProperty(XHR_PROP_TIMEOUT) failed: 0x%08x", hr); 

				hr = m_XHR->SetCustomResponseStream(m_ResponseStream.Get());
				rverify(SUCCEEDED(hr), catchall, netError("IXMLHttpRequest SetCustomResponseStream failed, 0x%08x", hr));

				if(netTask2::IsCancelRequested())
				{
					return netTaskState::Canceled;
				}

				SetHeadersHelper();

				if(netTask2::IsCancelRequested())
				{
					return netTaskState::Canceled;
				}

				hr = m_XHR->Send(m_RequestStream.Get(), m_RequestStream->Length());
				rverify(SUCCEEDED(hr), catchall, netError("IXMLHttpRequest Send failed, 0x%08x", hr));

				return netTaskState::Succeeded;
			}
			rcatchall
			{
				return netTaskState::Failed;
			}
		}
		catch(...)
		{

		}

		return netTaskState::Failed;
	}, &m_XblHttpTaskStatus, TASK2_DECLT(XblHttpRequest, ragenet_xbl));

	return httpTask.IsInitialized();
}

void
netXblHttpRequest::Impl::Cancel()
{
	PROFILE

	if(m_RequestStream)
	{
		m_RequestStream->Cancel();
	}

	if(m_ResponseStream)
	{
		m_ResponseStream->Cancel();
	}

	if(m_XblHttpTaskStatus.Pending())
	{
		netTask2::CancelTask(&m_XblHttpTaskStatus);
	}

	{
		SYS_CS_SYNC(m_XhrCs);

		if(m_XHR)
		{
			m_XHR->Abort();
		}
	}
}

void netXblHttpRequest::Impl::QueueForDeletion(netXblHttpRequest::Impl* impl)
{
	PROFILE;

	if((impl == nullptr) || impl->m_QueuedForDeletion)
	{
		return;
	}

	impl->m_QueuedForDeletion = true;

	netTask2 clearTask = netTask2::ScheduleTask([impl]()
	{
		try
		{
			if(impl->m_XblHttpTaskStatus.Pending())
			{
				return netTaskState::Pending;
			}

			{
				SYS_CS_SYNC(impl->m_XhrCs);

				if(impl->m_XHR)
				{
					// in case the request is still active
					impl->m_XHR->Abort();
				}

				// this decrements the ref count and releases the xhr object which can
				// take several ms so it can't be released on the main thread.
				impl->m_XHR = nullptr;
			}

			delete impl;
		}
		catch(...)
		{

		}

		return netTaskState::Succeeded;
	}, nullptr, TASK2_DECLT(XblHttpClear, ragenet_xbl));

	netAssert(clearTask.IsInitialized());
}

void
netXblHttpRequest::Impl::Clear()
{
	// note that the underlying http task will continue running until it sees the cancellation request.
	// However, the calling code assumes Clear() happens synchronously. We need to make sure that
	// nothing owned by the caller is accessed after this function exits. This includes m_Allocator
	// and anything allocated by m_Allocator.
	
	if(m_XblHttpTaskStatus.Pending())
	{
		netTask2::CancelTask(&m_XblHttpTaskStatus);
	}

	SYS_CS_SYNC(m_XhrCs);

	if(m_Uri)
	{
		this->Free(m_Uri);
		m_Uri = nullptr;
	}

	if(m_Verb)
	{
		this->Free(m_Verb);
		m_Verb = nullptr;
	}

	if(m_Headers)
	{
		this->Free(m_Headers);
		m_Headers = nullptr;
	}
	
	if(m_RequestStream)
	{
		m_RequestStream->Clear();
	}

	if(m_ResponseStream)
	{
		m_ResponseStream->Clear();
	}

	m_Allocator = nullptr;

	QueueForDeletion(this);
}

bool 
netXblHttpRequest::Impl::IsFinished() const
{
	return m_HttpCallback->IsFinished();
}

bool 
netXblHttpRequest::Impl::HasError() const
{
	return m_HttpCallback->HasError();
}

s32  
netXblHttpRequest::Impl::GetErrorCode() const
{
	return m_HttpCallback->GetErrorCode();
}

bool 
netXblHttpRequest::Impl::HaveHeaders() const
{
	return m_HttpCallback->HaveHeaders();
}

bool 
netXblHttpRequest::Impl::GetResponseHeaderLength(unsigned* headerLen) const
{
	return m_HttpCallback->GetResponseHeaderLength(headerLen);
}

bool 
netXblHttpRequest::Impl::GetResponseHeader(char* headerBuf,
										   unsigned* headerLen) const
{
	return m_HttpCallback->GetHeaders(headerBuf, headerLen);
}

bool 
netXblHttpRequest::Impl::ReadData(u8* data,
								  const unsigned maxLen,
								  unsigned* numRead,
								  bool* allContentRead)
{
	PROFILE

	bool success = false;

	try
	{
		rtry
		{
			if(numRead)
			{
				*numRead = 0;
			}

			// check the finished flag before the read since
			// the finished flag gets set on a different thread
			bool isFinished = IsFinished();

			ULONG bytesRead = 0;
			HRESULT hr = m_ResponseStream->Read(data, maxLen, &bytesRead);
			rverify(SUCCEEDED(hr), catchall, netError("IXMLHttpRequest ReadData failed (max length: %u), 0x%08x", maxLen, hr));

			if(numRead)
			{
				*numRead = (unsigned)bytesRead;
			}

			if(allContentRead)
			{
				*allContentRead = isFinished && (bytesRead < maxLen);
			}

			success = true;
		}
		rcatchall
		{
		}
	}
	catch(...)
	{

	}

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  netXblHttpRequest
///////////////////////////////////////////////////////////////////////////////
netXblHttpRequest::netXblHttpRequest()
: m_Allocator(nullptr)
, m_Impl(nullptr)
{

}

netXblHttpRequest::~netXblHttpRequest()
{
	Clear();
}

void*
netXblHttpRequest::Allocate(const unsigned numBytes) const
{
    if(m_Allocator)
    {
#if __FINAL
		const char* fileName = nullptr;
		int lineNum = 0;
#else
		const char* fileName = __FILE__;
		int lineNum = __LINE__;
#endif

		return m_Allocator->TryLoggedAllocate(numBytes, sizeof(void*), 0, fileName, lineNum);
    }
    else
    {
        return rage_new u8[numBytes];
    }
}

void
netXblHttpRequest::Free(void* ptr) const
{
    if(ptr)
    {
        if(m_Allocator)
        {
			netAssert(m_Allocator->IsValidPointer(ptr));
            m_Allocator->Free(ptr);
        }
        else
        {
            delete[] ptr;
        }
    }
}

bool netXblHttpRequest::Init(sysMemAllocator* allocator)
{
	if(allocator == nullptr)
	{
		allocator = &sysMemAllocator::GetCurrent();
	}

	m_Allocator = allocator;
	
	if(!netVerify(m_Allocator))
	{
		return false;
	}

	return true;
}

bool 
netXblHttpRequest::Open(const char* verb,
						const char* uri)
{
	// Note:  All IXHR calls are RPC calls which block the calling thread for a few ms,
	// which means they cannot be called from the main thread. Furthermore, the calling
	// code expects Cancel/Clear to be synchronous, meaning the netXblHttpRequest object
	// can go out of scope immediately after Cancel() or Clear() is called. To that end,
	// m_Impl actually runs detached from netXblHttpRequest.
	
	// After Cancel() or Clear() is called, the netXblHttpRequest object can go out
	// of scope immediately. We have to hold on to the m_Impl object until the underlying
	// HTTP task (running on a different thread) processes the cancellation request.

	// m_Impl is allocated with the global allocator (assumed to be thread-safe) instead
	// of via m_Allocator, since it needs to outlive a call to Clear(), and
	// gets deleted on a separate thread to avoid stalls.

	rtry
	{
		if(m_Impl)
		{
			m_Impl->Clear();
			m_Impl = nullptr;
		}
		m_Impl = rage_new Impl();
		rcheckall(m_Impl);
		rcheckall(m_Impl->Init(m_Allocator));
		rcheckall(m_Impl->Open(verb, uri));
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netXblHttpRequest::SetRequestHeader(const char* headers,
									const unsigned length)
{
	return m_Impl && m_Impl->SetRequestHeader(headers, length);
}

bool netXblHttpRequest::QueueChunk(const u8* data,
								   const unsigned dataLen,
								   netStatus* status)
{
	return m_Impl && m_Impl->QueueChunk(data, dataLen, status);
}

bool netXblHttpRequest::Commit()
{
	return m_Impl && m_Impl->Commit();
}

void
netXblHttpRequest::Cancel()
{
	if(m_Impl)
	{
		m_Impl->Cancel();
	}
}

void
netXblHttpRequest::Clear()
{
	if(m_Impl)
	{
		m_Impl->Clear();
		m_Impl = nullptr;
	}

	//** DON'T set the m_Allocator to nullptr
	// The request can be reused again without calling Init();
}

bool 
netXblHttpRequest::IsFinished() const
{
	return m_Impl && m_Impl->IsFinished();
}

bool 
netXblHttpRequest::HasError() const
{
	return m_Impl && m_Impl->HasError();
}

s32  
netXblHttpRequest::GetErrorCode() const
{
	return m_Impl ? m_Impl->GetErrorCode() : 0;
}

bool 
netXblHttpRequest::HaveHeaders() const
{
	return m_Impl && m_Impl->HaveHeaders();
}

bool 
netXblHttpRequest::GetResponseHeaderLength(unsigned* headerLen) const
{
	return m_Impl && m_Impl->GetResponseHeaderLength(headerLen);
}

bool 
netXblHttpRequest::GetResponseHeader(char* headerBuf,
									 unsigned* headerLen) const
{
	return m_Impl && m_Impl->GetResponseHeader(headerBuf, headerLen);
}

bool 
netXblHttpRequest::ReadData(u8* data,
							const unsigned maxLen,
							unsigned* numRead,
							bool* allContentRead)
{
	return m_Impl && m_Impl->ReadData(data, maxLen, numRead, allContentRead);
}

///////////////////////////////////////////////////////////////////////////////
//  netXblHttpGetAuthTokenTask
///////////////////////////////////////////////////////////////////////////////
class netXblHttpGetAuthTokenTask : public netTask
{
public:

	NET_TASK_DECL(netXblHttpGetAuthTokenTask);
	NET_TASK_USE_CHANNEL(ragenet_xbl);

	netXblHttpGetAuthTokenTask()
		: m_State(STATE_GET_TOKEN_AND_SIGNATURE)
	{
	}

	Platform::String^ AsciiToWinRtString(const char* s)
	{
		size_t len = strlen(s) + 1;
		wchar_t* w = (wchar_t*)Alloca(wchar_t, len);
		Utf8ToWide((char16*)w, s, (int)len);
		return ref new Platform::String(w);
	}

	bool Configure(const char* httpVerb,
				   const char* uri,
				   const char* headers,
				   const u8* body,
				   const unsigned sizeOfBody,
				   const char* xblUserHash,
				   netXblHttp::netXblRelyingPartyToken* authToken)
	{
		try
		{
			// if we've disabled HTTPS requests, pretend we're going to make an HTTPS
			// request so that GetTokenAndSignatureAsync still works, then we'll send the token over HTTP.
			static const char httpScheme[] = "http://" ;
			bool isHttpScheme = strnicmp(uri, httpScheme, sizeof(httpScheme) - 1) == 0;

			if(isHttpScheme)
			{
				size_t bufSize = strlen(uri) + 1 + 1; // +1 for "https" vs "http", +1 for null terminator
				char* finalUri = Alloca(char, bufSize);
				if(finalUri == nullptr)
				{
					netTaskError("Error allocating %u bytes for uri %s", (unsigned)bufSize, uri);
					return false;
				}

				safecpy(finalUri, "https://", bufSize);
				safecat(finalUri, &uri[sizeof(httpScheme) - 1], bufSize);

				m_Uri = AsciiToWinRtString(finalUri);
			}
			else
			{
				m_Uri = AsciiToWinRtString(uri);
			}

			m_HttpVerb = AsciiToWinRtString(httpVerb);
			m_Headers = AsciiToWinRtString(headers);
			m_XblUserHash = AsciiToWinRtString(xblUserHash);
			m_AuthToken = authToken;

			// NOTE: Even if there is no body to the request, the body parameter
			// is required to be at least a 1 byte array, set to zero
			if(sizeOfBody > 0)
			{
				m_Body = ref new Platform::Array<unsigned char>((unsigned char*)body, (INT)sizeOfBody);
			}
			else
			{
				m_Body = ref new Platform::Array<unsigned char>(1);
				m_Body[0] = 0;
			}
		}
		catch(Platform::Exception^ ex)
		{
			NET_EXCEPTION(ex);
			return false;
		}
		
		return true;
	}

	virtual void OnCancel()
	{
		netTaskDebug("Canceled while in state %d", m_State);
		m_XovStatus.Cancel();
	}

	virtual netTaskStatus OnUpdate(int* /*resultCode*/)
	{
		if(WasCanceled())
		{
			return NET_TASKSTATUS_FAILED;
		}

		netTaskStatus status = NET_TASKSTATUS_PENDING;

		try
		{
			switch(m_State)
			{
			case STATE_GET_TOKEN_AND_SIGNATURE:
				if(GetTokenAndSignatureAsync())
				{
					m_State = STATE_GETTING_TOKEN_AND_SIGNATURE;
				}
				else
				{
					status = NET_TASKSTATUS_FAILED;
				}
				break;

			case STATE_GETTING_TOKEN_AND_SIGNATURE:
				m_XovStatus.Update();
				if(!m_XovStatus.Pending())
				{
					if(m_MyStatus.Succeeded())
					{
						using namespace Windows::Xbox::System;
						m_TokenAndSignatureResult = m_XovStatus.GetResults<GetTokenAndSignatureResult^>();

						status = NET_TASKSTATUS_SUCCEEDED;
					}
					else
					{
						status = NET_TASKSTATUS_FAILED;
					}
				}
				break;
			}
		}
		catch(Platform::Exception^ ex)
		{
			TASK_EXCEPTION(ex);
			status = NET_TASKSTATUS_FAILED;
		}

		if(NET_TASKSTATUS_PENDING != status)
		{
			this->Complete(status);
		}

		return status;
	}

private:

	void Complete(const netTaskStatus status)
	{
		rtry
		{
			rcheck((status == NET_TASKSTATUS_SUCCEEDED) && !WasCanceled(), catchall, );

			unsigned len = m_TokenAndSignatureResult->Token->Length();
			char* ascii = (char*)Alloca(char, len + 1);
			WideToAscii(ascii, (char16*)m_TokenAndSignatureResult->Token->Data(), len + 1);
			m_AuthToken->m_Token = StringDuplicate(ascii);

			len = m_TokenAndSignatureResult->Signature->Length();
			ascii = (char*)Alloca(char, len + 1);
			WideToAscii(ascii, (char16*)m_TokenAndSignatureResult->Signature->Data(), len + 1);
			m_AuthToken->m_Signature = StringDuplicate(ascii);
		}
		rcatchall
		{

		}
	}

	bool GetTokenAndSignatureAsync()
	{
		bool success = false;

		try
		{
			using namespace Windows::Xbox::System;

			User^ user = nullptr;

			const sysDurangoUserInfo* pUserInfo = sysDurangoUserList::GetPlatformInstance().GetSignedInPlatformUserInfo(m_XblUserHash);
			if (pUserInfo != NULL && pUserInfo->GetPlatformUser() != nullptr)
			{
				user = pUserInfo->GetPlatformUser();
			}
			else
			{
				return false;
			}

			success = m_XovStatus.BeginOp<GetTokenAndSignatureResult^>("GetTokenAndSignatureAsync", 
				user->GetTokenAndSignatureAsync(m_HttpVerb, m_Uri, m_Headers, m_Body),
							30 * 1000,
							this,
							&m_MyStatus);
		}
		catch (Platform::Exception^ ex)
		{
			NET_EXCEPTION(ex);
		}

		return success;
	}

	enum State
	{
		STATE_GET_TOKEN_AND_SIGNATURE,
		STATE_GETTING_TOKEN_AND_SIGNATURE,
	};

	State m_State;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XovStatus;
	netStatus m_MyStatus;

	Platform::String^ m_HttpVerb;
	Platform::String^ m_Uri;
	Platform::String^ m_Headers;
	Platform::Array<unsigned char>^ m_Body;
	Platform::String^ m_XblUserHash;
	Windows::Xbox::System::GetTokenAndSignatureResult^ m_TokenAndSignatureResult;
	netXblHttp::netXblRelyingPartyToken* m_AuthToken;
};

///////////////////////////////////////////////////////////////////////////////
//  netXblHttp
///////////////////////////////////////////////////////////////////////////////
bool
netXblHttp::GetAuthToken(const char* httpVerb,
						 const char* uri,
						 const char* headers,
						 const u8* body,
						 const unsigned sizeOfBody,
						 const char* xblUserHash,
						 netXblRelyingPartyToken* authToken,
						 netStatus* status)
{
	bool success = false;

	netXblHttpGetAuthTokenTask* task;

	if(!netTask::Create(&task, status)
		|| !task->Configure(httpVerb, uri, headers, body, sizeOfBody, xblUserHash, authToken)
		|| !netTask::Run(task))
	{
		netTask::Destroy(task);
	}
	else
	{
		success = true;
	}

	return success;
}

void 
netXblHttp::FreeAuthToken(netXblRelyingPartyToken* authToken)
{
	if(authToken)
	{
		if(authToken->m_Token)
		{
			StringFree(authToken->m_Token);
		}

		if(authToken->m_Signature)
		{
			StringFree(authToken->m_Signature);
		}
	}
}

void 
netXblHttp::CancelAuthTokenRequest(netStatus* status)
{
	netTask::Cancel(status);
}

} // namespace rage

#endif  // RSG_DURANGO
