/********************************************************
*                                                       *
*   Copyright (C) Microsoft. All rights reserved.       *
*                                                       *
********************************************************/

/*
	This is the header for a custom DLL Microsoft has provided to us.
	GetGatewayAddress.dll allows us to obtain the IP of the default gateway.
	This is used to enable NAT-PMP / PCP support (see natpcp.cpp) on Xbox One.
	These are NAT port reservation protocols (Port Mapping Protocol, 
	Port Control Protocol). PC and PS4 already provide a method to obtain the IP.
	The intention is to improve NAT traversal success rate and lower
	relay server usage.	If the DLL is not present, we gracefully fallback
	to not supporting NAT-PMP / PCP.
*/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


HRESULT
WINAPI
GetIpv4GatewayAddress(
    _Out_ IN_ADDR * address
    );

typedef HRESULT (WINAPI * GETIPV4GATEWAYADDRESSPROC)(
    _Out_ IN_ADDR * address
    );


#ifdef __cplusplus
}
#endif
