// 
// xbltask.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NETXBL_TASK_H
#define NETXBL_TASK_H

#if RSG_DURANGO

#if !defined(__cplusplus_winrt)
#error "xbltask.h is a private header. Do not #include it in non-WinRT classes"
#endif

#include "atl/array.h"
#include "diag/channel.h"
#include "net/durango/xblstatus.h"
#include "net/task.h"
#include "net/time.h"
#include "rline/rl.h"
#include "system/criticalsection.h"

namespace rage
{

class netXblTask : public netTask
{
public:
	netXblTask();

	virtual void OnCancel();
	virtual void Complete(const netTaskStatus /* status */) {}
	virtual netTaskStatus OnUpdate(int* /*resultCode*/);

protected:
	virtual bool DoWork() = 0;
	virtual void ProcessSuccess() {}
	virtual void ProcessFailure() {}
	virtual void ProcessCancelled() {}

	enum State
	{
		STATE_DO_WORK,
		STATE_WORKING
	};

	State m_State;
	netStatus m_MyStatus;
	netXblStatus<Windows::Foundation::IAsyncInfo^> m_XblStatus;
};

#define CREATE_NET_XBLTASK(T, status, ...)													\
	T* task = NULL;																			\
	rtry																					\
	{																						\
		if(status != NULL)																	\
		{																					\
			rverify(netTask::Create(&task, status), catchall, );							\
		}																					\
		else																				\
		{																					\
			netFireAndForgetTask<T>* fafTask = NULL;										\
			rverify(netTask::Create(&fafTask), catchall, );									\
			task = fafTask;																	\
		}																					\
		rverify(task->Configure(__VA_ARGS__), catchall, );									\
		rverify(netTask::Run(task), catchall, );											\
		return true;																		\
	}																						\
	rcatchall																				\
	{																						\
		if(task != NULL)																	\
		{																					\
			netTask::Destroy(task);															\
		}																					\
		return false;																		\
	}

#define TASK_EXCEPTION_SILENT(ex)															\
	netError("%s[%" SIZETFMT "u:%u]: Exception 0x%08x, Msg: %ls",							\
	this->GetTaskName(), 																	\
	this->GetTaskQueueId(), 																\
	this->GetTaskId(), 																		\
	ex->HResult, 																			\
	ex->Message != nullptr  ? ex->Message->Data() : L"NULL");						

#if __ASSERT	
#define TASK_EXCEPTION(ex)																	\
	netAssertf(false, "%s[%" SIZETFMT "u:%u]: Exception 0x%08x, Msg: %ls",						\
			this->GetTaskName(), 															\
			this->GetTaskQueueId(), 														\
			this->GetTaskId(), 																\
			ex->HResult, 																	\
			ex->Message != nullptr  ? ex->Message->Data() : L"NULL");						
#else	
#define TASK_EXCEPTION(ex) TASK_EXCEPTION_SILENT(ex)					
#endif

#define CANCEL_NET_XBLTASK(status)															\
	if (status.Pending())																	\
	{																						\
		netTask::Cancel(&status);															\
	}

} // namespace rage

#endif  // RSG_DURANGO

#endif  // NETXBL_TASK_H
