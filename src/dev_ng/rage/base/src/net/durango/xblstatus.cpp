// 
// xblstatus.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#include "xblstatus.h"
#include "atl/array.h"
#include "diag/seh.h"
#include "net/status.h"
#include "net/durango/xblnet.h"
#include "net/task.h"
#include "system/criticalsection.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"

#if RSG_DURANGO
using namespace Microsoft::Xbox::Services;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Storage::Streams;
using namespace Windows::Xbox::Multiplayer;
using namespace Windows::Xbox::System;
using namespace Microsoft::Xbox::Services::Multiplayer;

#if !__NO_OUTPUT
#define xblStatusError(fmt, ...)			if(m_ParentTask){netTaskErrorPtr(m_ParentTask, fmt, ##__VA_ARGS__);}else{netError(fmt, ##__VA_ARGS__);}
#define xblStatusDebug(fmt, ...)			if(m_ParentTask){netTaskDebugPtr(m_ParentTask, fmt, ##__VA_ARGS__);}else{netDebug(fmt, ##__VA_ARGS__);}
#define xblStatusDebug1(fmt, ...)			if(m_ParentTask){netTaskDebug1Ptr(m_ParentTask, fmt, ##__VA_ARGS__);}else{netDebug1(fmt, ##__VA_ARGS__);}
#define xblStatusDebug2(fmt, ...)			if(m_ParentTask){netTaskDebug2Ptr(m_ParentTask, fmt, ##__VA_ARGS__);}else{netDebug2(fmt, ##__VA_ARGS__);}
#define xblStatusDebug3(fmt, ...)			if(m_ParentTask){netTaskDebug3Ptr(m_ParentTask, fmt, ##__VA_ARGS__);}else{netDebug3(fmt, ##__VA_ARGS__);}
#define xblStatusAssertf(cond, fmt, ...)	if(m_ParentTask){netTaskAssertfPtr(m_ParentTask, cond, fmt, ##__VA_ARGS__);}else{netAssertf(cond, fmt, ##__VA_ARGS__);}
#else
#define xblStatusError(fmt, ...)
#define xblStatusDebug(fmt, ...)
#define xblStatusDebug1(fmt, ...)
#define xblStatusDebug2(fmt, ...)
#define xblStatusDebug3(fmt, ...)
#define xblStatusAssertf(cond, fmt, ...)
#endif

namespace rage
{
static sysCriticalSectionToken gs_AsyncLock;

// We need this status array to manage the expiry of these tasks (our lambda functions use this->)
// We can't just Cancel and use Completed = nullptr as some tasks won't cancel and we may still run
// into race conditions. 
// See https://forums.xboxlive.com/AnswerPage.aspx?qid=07d100f6-f4f3-44bb-84d1-447665c9553a&tgt=1

struct AsyncTrack
{
	AsyncTrack()
	{
		nStatusId = INVALID_STATUS_ID;
	}

	AsyncTrack(StatusId statusId)
		: nStatusId(statusId)
	{

	}

	StatusId nStatusId;

	bool operator==(const AsyncTrack& that) const
	{
		return (nStatusId == that.nStatusId);
	}
};

atArray<AsyncTrack> g_AsyncNodes;

bool 
IsAsyncActive(StatusId statusId)
{
	SYS_CS_SYNC(gs_AsyncLock);
	return g_AsyncNodes.Find(AsyncTrack(statusId)) >= 0;
}

void 
netXblStatus<Windows::Foundation::IAsyncInfo^>::AddAsyncTrack(StatusId statusId)
{
	SYS_CS_SYNC(gs_AsyncLock);
	g_AsyncNodes.PushAndGrow(AsyncTrack(statusId));
	xblStatusDebug("AddAsyncTrack :: StatusId: %d, Count: %d", statusId, g_AsyncNodes.GetCount());
}

void 
netXblStatus<Windows::Foundation::IAsyncInfo^>::RemoveAsyncTrack(StatusId statusId)
{
	SYS_CS_SYNC(gs_AsyncLock);
	int index = g_AsyncNodes.Find(AsyncTrack(statusId));
	if(index >= 0)
		g_AsyncNodes.Delete(index);

	xblStatusDebug("RemoveAsyncTrack :: StatusId: %d, Found: %s, Count: %d", statusId, index >= 0 ? "True" : "False", g_AsyncNodes.GetCount());
}

static StatusId gs_StatusIdCount = 0;
StatusId
AllocateStatusId()
{
	return gs_StatusIdCount++;
}

bool IsValidStatusId(const StatusId statusId)
{
	return statusId != INVALID_STATUS_ID;
}

///////////////////////////////////////////////////////////////////////////////
//  netXblStatus
///////////////////////////////////////////////////////////////////////////////

template<>
netXblStatus<Windows::Foundation::IAsyncInfo^>::netXblStatus()
	: m_Status(nullptr)
	, m_Flags(0)
	, m_Timeout(0)
	, m_TimeStarted(0)
	, m_Async(nullptr)
	, m_StatusId(INVALID_STATUS_ID)
	, m_bHasTrack(false)
	, m_AsyncStatus(-1)
#if !__NO_OUTPUT
	, m_ParentTask(nullptr)
	, m_AsyncFuncName(nullptr)
#endif
{

}

template<>
netXblStatus<Windows::Foundation::IAsyncInfo^>::~netXblStatus()
{
	Clear();
}

template<>
int
netXblStatus<Windows::Foundation::IAsyncInfo^>::OnBegin(const char* funcName, Windows::Foundation::IAsyncInfo^ t, unsigned timeout, netStatus* status)
{
	return this->OnBegin(funcName, t, timeout, 0, status);
}

template<>
int
netXblStatus<Windows::Foundation::IAsyncInfo^>::OnBegin(const char* OUTPUT_ONLY(funcName),
														Windows::Foundation::IAsyncInfo^ t,
														const unsigned timeout,
														const unsigned flags,
														netStatus* status)
{
	// ensure that netXblStatus clear up correctly when re-used
	if(IsValidStatusId(m_StatusId))
	{
		xblStatusDebug("%s[%d] Begin :: Clearing Previous Request", GetFunctionName(), m_StatusId);
		Clear();
	}

	// assign an Id and function name (latter for logging)
	m_StatusId = AllocateStatusId();

#if !__NO_OUTPUT
	m_AsyncFuncName = funcName;
#endif

	try
	{
		if(!netVerify(status != nullptr))
		{
			xblStatusError("%s[%d] Begin :: Invalid status object!", GetFunctionName(), m_StatusId);
			return INVALID_STATUS_ID;
		}

		if(!netVerify(!status->Pending()))
		{
			xblStatusError("%s[%d] Begin :: Status object pending!", GetFunctionName(), m_StatusId);
			return INVALID_STATUS_ID;
		}

		// mark status pending
		status->SetPending();

		if(!netVerify(t != nullptr))
		{
			xblStatusError("%s[%d] Begin :: Invalid Async object!", GetFunctionName(), m_StatusId);
			status->SetFailed();
			return INVALID_STATUS_ID;
		}

		if(!netVerify(!this->Pending()))
		{
			xblStatusError("%s[%d] Begin :: Already pending!", GetFunctionName(), m_StatusId);
			status->SetFailed();
			return INVALID_STATUS_ID;
		}

		if(!(t->Status == AsyncStatus::Started || t->Status == AsyncStatus::Completed))
		{
			xblStatusError("%s[%d] Begin :: Failed! Error: 0x%08x", GetFunctionName(), m_StatusId, t->ErrorCode.Value);
			status->SetFailed(t->ErrorCode.Value);
			return INVALID_STATUS_ID; 
		}

		m_Async = t;
		m_Status = status;
		m_Flags = flags;
		m_Timeout = timeout;
		m_TimeStarted = sysTimer::GetSystemMsTime();
		{
			SYS_CS_SYNC(gs_AsyncLock);
			m_AsyncStatus = static_cast<int>(t->Status);
		}
		
		AddAsyncTrack(m_StatusId);
		m_bHasTrack = true; 

		xblStatusDebug("%s[%d] Begin :: Async Status: %d, AsyncId: %d, AsyncHash: 0x%08x, Flags: 0x%x, Timeout: %d", GetFunctionName(), m_StatusId, m_Async->Status, m_Async->Id, m_Async->GetHashCode(), m_Flags, m_Timeout);
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
		xblStatusError("%s[%d] Begin :: Exception Thrown", GetFunctionName(), m_StatusId);
		return INVALID_STATUS_ID;
	}

	return m_StatusId;
}

template<>
void
netXblStatus<Windows::Foundation::IAsyncInfo^>::Clear()
{
	SYS_CS_SYNC(gs_AsyncLock);

	// we only clear the status in this function, prevent re-entry
	if(!IsValidStatusId(m_StatusId))
		return; 

	try
	{
		xblStatusDebug("%s[%d] Clear", GetFunctionName(), m_StatusId);
		xblStatusAssertf(!this->Pending(), "%s[%d] Clear :: Task should not be pending!", GetFunctionName(), m_StatusId);

		if(this->Pending())
		{
			this->Cancel();
		}
		else
		{
			this->Close();
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
		xblStatusError("%s[%d] Clear :: Exception Thrown", GetFunctionName(), m_StatusId);
	}

	// failsafe - this should be removed in the cancel / close functions
	if(IsValidStatusId(m_StatusId) && m_bHasTrack)
	{
		RemoveAsyncTrack(m_StatusId);
		m_bHasTrack = false;
	}

#if !__NO_OUTPUT
	m_AsyncFuncName = nullptr;
#endif

	m_Status = nullptr;
	m_Async = nullptr;
	m_StatusId = INVALID_STATUS_ID; 
}

template<>
void
netXblStatus<Windows::Foundation::IAsyncInfo^>::Update()
{
	SYS_CS_SYNC(gs_AsyncLock);

	if(this->Pending())
	{
		switch(m_AsyncStatus)
		{
		case AsyncStatus::Completed:
			{
				xblStatusDebug("%s[%d] Update :: Processing Completed Status", GetFunctionName(), m_StatusId);
				m_Status->SetSucceeded();
				m_Status = nullptr;
			}
			break;
		case AsyncStatus::Canceled:
			{
				xblStatusDebug("%s[%d] Update :: Processing Canceled Status", GetFunctionName(), m_StatusId);
				m_Status->SetCanceled();
				m_Status = nullptr;
			}
			break;
		case AsyncStatus::Error:
			{
				xblStatusDebug("%s[%d] Update :: Processing Error Status", GetFunctionName(), m_StatusId);
				m_Status->SetFailed(GetErrorCode());
				m_Status = nullptr;
			}
			break;
		default:
			{
				// poll for timeout
				if((m_Timeout > 0) && (GetTimeElapsed() > m_Timeout))
				{
					try
					{
						xblStatusError("%s[%d] Timed out (%dms Timeout). Status: %d, Error: 0x%08x", GetFunctionName(), m_StatusId, m_Timeout, m_Async->Status, GetErrorCode());
					}
					catch (Platform::Exception^ ex)
					{
						NET_EXCEPTION(ex);
						xblStatusError("%s[%d] Update :: Exception Thrown", GetFunctionName(), m_StatusId);
					}

					// cancel and clear
					this->Cancel();
					this->Clear();
				}
			}
			break;
		}
	}
}

template<>
bool
netXblStatus<Windows::Foundation::IAsyncInfo^>::Pending() const
{
	SYS_CS_SYNC(gs_AsyncLock);

	return (m_Status != nullptr) && m_Status->Pending();
}

template<>
void
netXblStatus<Windows::Foundation::IAsyncInfo^>::Cancel()
{
	SYS_CS_SYNC(gs_AsyncLock);

	try
	{
		xblStatusDebug("%s[%d] Cancel :: Async Status: %d, CancelAsync: %s, Pending: %s", 
					   GetFunctionName(), 
					   m_StatusId, 
					   m_Async != nullptr ? static_cast<int>(m_Async->Status) : -1, 
					   (m_Async != nullptr && m_Async->Status == AsyncStatus::Started && ((m_Flags & FLAGS_DONT_CANCEL_ASYNC_TASK) == 0)) ? "True" : "False", 
					   this->Pending() ? "True" : "False");

		if((m_Flags & FLAGS_DONT_CANCEL_ASYNC_TASK) == 0)
		{
			if(m_Async != nullptr && m_Async->Status == AsyncStatus::Started)
			{
				m_Async->Cancel();
			}
		}

		m_Async = nullptr;
	}
	catch (Platform::Exception^ ex)
	{
		xblStatusDebug("%s[%d] Cancel :: Exception Thrown", GetFunctionName(), m_StatusId);
		NET_EXCEPTION(ex);
	}

	if(this->Pending())
	{
		m_Status->SetCanceled();
		m_Status = nullptr;
	}

	if(IsValidStatusId(m_StatusId) && m_bHasTrack)
	{
		RemoveAsyncTrack(m_StatusId);
		m_bHasTrack = false;
	}
}

template<>
void
netXblStatus<Windows::Foundation::IAsyncInfo^>::Close()
{
	SYS_CS_SYNC(gs_AsyncLock);

	// if we have some results - chuck these away
	try
	{
		xblStatusAssertf(!this->Pending(), "%s[%d] Close :: Task should not be pending!", GetFunctionName(), m_StatusId);
		xblStatusDebug("%s[%d] Close :: Async Status: %d, CloseAsync: %s, Pending: %s", 
					   GetFunctionName(), 
					   m_StatusId, 
					   m_Async != nullptr ? static_cast<int>(m_Async->Status) : -1, 
					   (m_Async != nullptr && m_Async->Status == AsyncStatus::Completed) ? "True" : "False", 
					   this->Pending() ? "True" : "False");

		if(m_Async != nullptr && m_Async->Status == AsyncStatus::Completed)
		{
			xblStatusDebug("%s[%d] Closing Async", GetFunctionName(), m_StatusId);
			m_Async->Close();
		}
		m_Async = nullptr;
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
		xblStatusError("%s[%d] Close :: Exception Thrown", GetFunctionName(), m_StatusId);
	}

	if(IsValidStatusId(m_StatusId) && m_bHasTrack)
	{
		RemoveAsyncTrack(m_StatusId);
		m_bHasTrack = false;
	}
}

template<>
void 
netXblStatus<Windows::Foundation::IAsyncInfo^>::OnCompleted() 
{
	SYS_CS_SYNC(gs_AsyncLock);

	try
	{
		m_AsyncStatus = static_cast<int>(AsyncStatus::Completed);
		xblStatusDebug("%s[%d] OnCompleted :: Pending: %s", GetFunctionName(), m_StatusId, this->Pending() ? "True" : "False");
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION_SILENT(ex);
		xblStatusError("%s[%d] OnCompleted :: Exception Thrown: 0x%08x", GetFunctionName(), m_StatusId, ex->HResult);
	}
}

template<>
void 
netXblStatus<Windows::Foundation::IAsyncInfo^>::OnError(int OUTPUT_ONLY(nErrorCode))
{
	SYS_CS_SYNC(gs_AsyncLock);

	try
	{
		m_AsyncStatus = static_cast<int>(AsyncStatus::Error);
		xblStatusDebug("%s[%d] OnError :: Pending: %s, Error: 0x%08x", GetFunctionName(), m_StatusId, this->Pending() ? "True" : "False", nErrorCode);
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION_SILENT(ex);
		xblStatusError("%s[%d] OnError :: Exception Thrown: 0x%08x", GetFunctionName(), m_StatusId, ex->HResult);
	}
}

template<>
void 
netXblStatus<Windows::Foundation::IAsyncInfo^>::OnCanceled()
{
	SYS_CS_SYNC(gs_AsyncLock);

	try
	{
		m_AsyncStatus = static_cast<int>(AsyncStatus::Canceled);
		xblStatusDebug("%s[%d] OnCanceled :: Pending: %s", GetFunctionName(), m_StatusId, this->Pending() ? "True" : "False");
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION_SILENT(ex);
		xblStatusError("%s[%d] OnCanceled :: Exception Thrown: 0x%08x", GetFunctionName(), m_StatusId, ex->HResult);
	}
}

template<>
int
netXblStatus<Windows::Foundation::IAsyncInfo^>::GetErrorCode() const
{
	SYS_CS_SYNC(gs_AsyncLock);

	int error = -1;
	try
	{
		if(m_Async != nullptr)
		{
			error = m_Async->ErrorCode.Value;
		}
	}
	catch (Platform::Exception^ ex)
	{
		NET_EXCEPTION(ex);
	}

	return error;
}

template<>
unsigned
netXblStatus<Windows::Foundation::IAsyncInfo^>::GetTimeElapsed()
{
	return (m_TimeStarted > 0) ? (sysTimer::GetSystemMsTime() - m_TimeStarted) : 0;
}

#if !__NO_OUTPUT
template<>
const char*
netXblStatus<Windows::Foundation::IAsyncInfo^>::GetFunctionName() const
{
	return m_AsyncFuncName ? m_AsyncFuncName : "";
}
#endif

} // namespace rage

#endif  // RSG_DURANGO
