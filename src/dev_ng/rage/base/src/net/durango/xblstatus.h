// 
// xblstatus.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef XBL_STATUS_H
#define XBL_STATUS_H

#if RSG_DURANGO

#include "net/netdiag.h"

class netStatus;

#define NO_ASYNC_TIMEOUT 0
#define DEFAULT_ASYNC_TIMEOUT 60000

typedef int StatusId;
static const int INVALID_STATUS_ID = -1;

namespace rage
{

class netTask;

//PURPOSE
//  Checks whether an Async operation is still in-flight on the client
bool IsAsyncActive(StatusId statusId);

//PURPOSE
//  Convenience class for handling Durango async operations.
//  Note: T is always of type Windows::Foundation::IAsyncInfo^.
//  Making it a template here avoids adding WinRT and C++/CX
//  dependencies to the header file, while allowing this class
//  to be reused in multiple .cpp files.
//  This class works with Durango APIs that return either an
//  IAsyncAction^ or an IAsyncOperation^. The latter returns a
//  result of a specified type R, which can be obtained by
//  calling netXblStatus::GetResults<R>().
template<typename T>
struct netXblStatus
{
	enum Flags
	{
		FLAGS_DEFAULT = 0x0,

		//Don't cancel the underlying async operation or action.
		//Some XDK tasks don't support cancellation and canceling them
		//leads to random issues/hangs/crashes (example CreateAssociationAsync).
		FLAGS_DONT_CANCEL_ASYNC_TASK = 0x01,
	};

	 netXblStatus();
	~netXblStatus();

	//PARAMS
	//  funcName    - Name of async function being called.
	//  status      - Status object which can be polled for completion.
	//  parentTask  - The parent task that called this, or NULL if there is no parent task.
	template <typename R> 
	bool BeginOp(const char* funcName,
				 T t,
				 const unsigned timeout,
				 netTask* OUTPUT_ONLY(parentTask),
				 unsigned int flags,
				 netStatus* status)
	{
#if !__NO_OUTPUT
		m_ParentTask = parentTask;
#endif
		StatusId nStatusId = OnBegin(funcName, t, timeout, flags, status);
		if(nStatusId >= 0)
		{
			try
			{
				auto tmp = (IAsyncOperation<R>^)m_Async;

				tmp->Completed = ref new AsyncOperationCompletedHandler<R>(
					[this, nStatusId](IAsyncOperation<R>^ operation, AsyncStatus status)
				{
					try
					{
						if(IsAsyncActive(nStatusId)) 
						{
							// we have access to the function name here so use that
							netDebug("%s[%d] AsyncTrack :: Task Found - AsyncOp Completed (Id: %u, Hash: 0x%08x), Status: %d", this->GetFunctionName(), nStatusId, operation->Id, operation->GetHashCode(), status);  

							switch(status)
							{
							case AsyncStatus::Canceled:		this->OnCanceled();							break;
							case AsyncStatus::Error:		this->OnError(operation->ErrorCode.Value);	break;
							case AsyncStatus::Completed:	this->OnCompleted();						break;
							default: break;
							}
						}
						else
						{
							netDebug("[%d] AsyncTrack :: Task Not Found - AsyncOp Completed (Id: %u, Hash: 0x%08x)", nStatusId, operation->Id, operation->GetHashCode());  
						}
					}
					catch (Platform::Exception^ ex)
					{
						netAssertf(false, "[%d] AsyncTrack :: AsyncOp Exception (CompletedHandler): 0x%08x (Id: %u, Hash: 0x%08x)", nStatusId, ex->HResult, operation->Id, operation->GetHashCode());  
					}
				});
			}
			catch (Platform::Exception^ ex)
			{
				netAssertf(false, "[%d] AsyncTrack :: AsyncOp Exception (OnBegin): 0x%08x", nStatusId, ex->HResult);  
			}
		}
		return nStatusId != INVALID_STATUS_ID;
	}

	//PARAMS
	//  funcName    - Name of async function being called.
	//  status      - Status object which can be polled for completion.
	//  parentTask  - The parent task that called this, or NULL if there is no parent task.
	template <typename R> 
	bool BeginOp(const char* funcName,
				 T t,
				 const unsigned timeout,
				 netTask* parentTask,
				 netStatus* status)
	{
		return BeginOp<R>(funcName, t, timeout, parentTask, FLAGS_DEFAULT, status);
	}

	//PARAMS
	//  funcName    - Name of async function being called.
	//  status      - Status object which can be polled for completion.
	//  parentTask  - The parent task that called this, or NULL if there is no parent task.
	bool BeginAction(const char* funcName,
				     T t,
				     const unsigned timeout,
				     netTask* OUTPUT_ONLY(parentTask),
				     unsigned int flags,
				     netStatus* status)
	{
#if !__NO_OUTPUT
		m_ParentTask = parentTask;
#endif
		StatusId nStatusId = OnBegin(funcName, t, timeout, flags, status);
		if(nStatusId >= 0)
		{
			try
			{
				auto tmp = (IAsyncAction^)m_Async;

				tmp->Completed = ref new AsyncActionCompletedHandler(
					[this, nStatusId](IAsyncAction^ action, AsyncStatus status)
				{
					try
					{
						if(IsAsyncActive(nStatusId)) 
						{
							// we have access to the function name here so use that
							netDebug("%s[%d] AsyncTrack :: Task Found - AsyncAction Completed (Id: %u, Hash: 0x%08x), Status: %d", this->GetFunctionName(), nStatusId, action->Id, action->GetHashCode(), status);  

							switch(status)
							{
							case AsyncStatus::Canceled:		this->OnCanceled();						break;
							case AsyncStatus::Error:		this->OnError(action->ErrorCode.Value);	break;
							case AsyncStatus::Completed:	this->OnCompleted();					break;
							default: break;
							}
						}
						else
						{
							netDebug("[%d] AsyncTrack :: Task Not Found - AsyncAction Completed (Id: %u, Hash: 0x%08x)", nStatusId, action->Id, action->GetHashCode());  
						}
					}
					catch (Platform::Exception^ ex)
					{
						netAssertf(false, "[%d] AsyncTrack :: AsyncAction Exception (CompletedHandler): 0x%08x (Id: %u, Hash: 0x%08x)", nStatusId, ex->HResult, action->Id, action->GetHashCode());  
					}
				});
			}
			catch (Platform::Exception^ ex)
			{
				netAssertf(false, "[%d] AsyncTrack :: AsyncAction Exception (OnBegin): 0x%08x", nStatusId, ex->HResult);  
			}
		}
		return nStatusId != INVALID_STATUS_ID;
	}

	//PARAMS
	//  funcName    - Name of async function being called.
	//  status      - Status object which can be polled for completion.
	//  parentTask  - The parent task that called this, or NULL if there is no parent task.
	bool BeginAction(const char* funcName,
				     T t,
				     const unsigned timeout,
				     netTask* parentTask,
				     netStatus* status)
	{
		return BeginAction(funcName, t, timeout, parentTask, FLAGS_DEFAULT, status);
	}

	void Clear();

	//PURPOSE
	//  Called on a regular interval to poll the status of the async
	//  operation.
	void Update();

	//PURPOSE
	//  Returns true if an operation is pending completion.
	bool Pending() const;

	//PURPOSE
	//  Cancels the currently pending operation.
	void Cancel();

	//PURPOSE
	//  Returns the result of an async
	//  function that returns an IAsyncOperation^.
	template <typename R> R GetResults() const
	{
		auto tmp = (IAsyncOperation<R>^)m_Async;
		return tmp->GetResults();
	}

	//PURPOSE
	//  Returns the time that this task has run for
	unsigned GetTimeElapsed();

#if !__NO_OUTPUT
	//PURPOSE
	//  Returns the name of the function passed to Begin().
	const char* GetFunctionName() const;
#endif

private:

	void AddAsyncTrack(StatusId statusId);
	void RemoveAsyncTrack(StatusId statusId);

	//PARAMS
	//  funcName    - Name of async function being called.
	//  status      - Status object which can be polled for completion.
	int OnBegin(const char* funcName,
		T t,
		const unsigned timeout,
		netStatus* status);

	//PARAMS
	//  funcName    - Name of async function being called.
	//  flags       - Bit combination of values from Flags enum.
	//  status      - Status object which can be polled for completion.
	int OnBegin(const char* funcName,
				T t,
				const unsigned timeout,
				const unsigned flags,
				netStatus* status);

	//PURPOSE
	//  Drop the results
	void Close();

	//PURPOSE
	//  Returns the error code after an operation fails.
	int GetErrorCode() const;

	//PURPOSE
	//  Callback functions
	void OnCompleted();
	void OnError(int nErrorCode);
	void OnCanceled();

	T m_Async;
	netStatus* m_Status;
	unsigned m_Flags;
	unsigned m_Timeout;
	unsigned m_TimeStarted;
	StatusId m_StatusId;
	bool m_bHasTrack; 
	int m_AsyncStatus;

#if !__NO_OUTPUT
	const char* m_AsyncFuncName;
	netTask* m_ParentTask;
#endif
};

} // namespace rage

#endif // RSG_DURANGO

#endif // XBL_STATUS_H
