// 
// net/connectionrouter.h 
// 
// Copyright (C) 1999-2017 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_CONNECTIONROUTER_H
#define NET_CONNECTIONROUTER_H

#include "atl/inlist.h"
#include "atl/inmap.h"
#include "net/event.h"
#include "net/netpeerid.h"
#include "net/netutil.h"

// When enabled, intermediate nodes do not reply to route requests.
// Route requests will be forwarded all the way to the destination node.
#define NET_PEER_RELAY_DESTINATION_ONLY (1)

namespace rage
{

class sysMemAllocator;
class netConnectionManager;
class netRouteRequestMsg;
class netRouteReplyMsg;
class netRouteErrMsg;
class netRouteChangeRequestMsg;
class netRouteChangeReplyMsg;
class netPeerRelayPacket;

//PURPOSE
// Routes P2P connections through the most desirable paths.
// P2P Relay: routes packets through other connected peers when possible, reducing reliance 
// on relay servers when a direct connection to a remote peer cannot be established.
// Broken direct connections are rerouted to the relay server/P2P relay.
class netConnectionRouter
{
public:

	//PURPOSE
	// Implements a monotonically increasing sequence number (can never be decreased).
	// Helps to prevent loops which are critical errors in routing protocols.
	class SequenceNumber
	{
	public:

		static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = sizeof(unsigned); // m_Seq

		//PURPOSE
		// Constructs a sequence number with a default value of 0.
		SequenceNumber();

		//PURPOSE
		// Increment by 1.
		void Inc();

		//PURPOSE
		// Set seq to max(current value, s).
		void Max(const SequenceNumber& s);

		//PURPOSE
		// Returns current value.
		unsigned Get() const;

		//PURPOSE
		// Copies the sequence number of s, but only if it doesn't
		// result in a decrease of the existing sequence number.
		// Equivalent to Max(s) above.
		const SequenceNumber& operator=(const SequenceNumber& s);

		//PURPOSE
		// Comparison operators.
		bool operator==(const SequenceNumber& that) const;
		bool operator!=(const SequenceNumber& that) const;
		bool operator<(const SequenceNumber& that) const;
		bool operator<=(const SequenceNumber& that) const;
		bool operator>(const SequenceNumber& that) const;
		bool operator>=(const SequenceNumber& that) const;

	    //PURPOSE
		//  Exports data in a platform/endian independent format.
		//PARAMS
		//  buf         - Destination buffer.
		//  sizeofBuf   - Size of destination buffer.
		//  size        - Set to number of bytes exported.
		//RETURNS
		//  True on success.
		bool Export(void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0) const;

		//PURPOSE
		//  Imports data exported with Export(), but fails if it would
		//  result in a decrease of the existing sequence number.
		//  buf         - Source buffer.
		//  sizeofBuf   - Size of source buffer.
		//  size        - Set to number of bytes imported.
		//RETURNS
		//  True on success.
		bool Import(const void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0);

	private:
		//PURPOSE
		// Disable copy constructor, since this can lead to decreasing a
		// sequence number - we won't have an existing value to compare.
		SequenceNumber(const SequenceNumber&);

		unsigned m_Seq;
	};

	//PURPOSE
	// The max supported number of nodes in the p2p mesh.
	static const u8 MAX_NUM_PEERS = (64);

	//PURPOSE
	// The default maximum number of hops along any p2p route.
	static const u8 DEFAULT_MAX_HOP_COUNT = 2;

	//PURPOSE
	// The default maximum number of connections a peer can relay simultaneously.
	// Note that a connection is considered to be unilateral. For example, if the local peer B is
	// relaying from A -> B -> C and from C -> B -> A, the local peer would be relaying two connections.
	static const unsigned DEFAULT_MAX_RELAYED_CXNS = 20;

	//PURPOSE
	// The default time at which we fall back to relay if we haven't recovered
	// from a broken p2p relay link.
	static const unsigned DEFAULT_EMERGENCY_RELAY_FALLBACK_TIME_MS = (5 * 1000);

	//PURPOSE
	// The default time at which we fall back to relay if we haven't received
	// from a broken direct link. If 0, do not allow direct connections to fall back to the relay server.
	static const unsigned DEFAULT_EMERGENCY_DIRECT_RECEIVE_TIMEOUT_MS = DEFAULT_EMERGENCY_RELAY_FALLBACK_TIME_MS;

	//PURPOSE
	// If we haven't received a packet from the remote peer within this time,
	// consider the link to be broken and start the route repair protocol.
	static const unsigned BROKEN_LINK_DETECTION_TIME_MS = (DEFAULT_EMERGENCY_RELAY_FALLBACK_TIME_MS / 2);

	//PURPOSE
	// Once we receive a reply to a route change request, we wait this amount of time
	// before switching the connection to the new route.
	static const unsigned DEFAULT_ROUTE_CHANGE_WAIT_TIME_MS = (500);

	netConnectionRouter(netConnectionManager* cxnMgr);
	~netConnectionRouter();

	void Init(sysMemAllocator* allocator);
	void Update();
	void Shutdown();
		
    //PURPOSE
    //  Sets the connection policies.
    //NOTES
    //  The corresponding connection on the remote peer should have
    //  the same policies, especially timeouts.
    void SetPolicies(const netConnectionPolicies& policies);

	//PURPOSE
	// Adds a node to the p2p mesh.
	void AddEndpoint(const EndpointId endpointId);

	//PURPOSE
	// Updates a node when its address/route changes.
	void UpdateEndpoint(const netPeerId& peerId, const EndpointId endpointId);

	//PURPOSE
	// Records the most recent time at which the local node received a packet from the given peer.
	void UpdateLastReceiveTime(const netPeerId& peerId, const netAddress& sender, const unsigned curTime);

	//PURPOSE
	// Removes a node from the p2p mesh.
	void RemoveEndpoint(const netPeerId& peerId, const EndpointId endpointId);

	//PURPOSE
	// Prepares a peer relay packet for forwarding.
	//PARAMS
	//  relayPkt		- The packet to be forwarded.
	//  nextHopAddr		- [out] Will contain the forwarding address of this packet.
	//					  This will be the address of the next hop along the previously discovered 
	//					  p2p route between the source peer and the destination peer.
	//RETURNS
	//  True on success. If false, the packet cannot not be forwarded.
	bool PrepareToForward(netPeerRelayPacket* relayPkt, netAddress* nextHopAddr);

	//PURPOSE
	// Prepares a peer relay packet for reception at the destination peer.
	//PARAMS
	//  relayPkt		- The terminus packet to be received.
	//  sender			- The address of the sender of the packet.
	//  reverseNextHopAddr - [out] Will contain the reply address to which a reply can be sent.
	//					  This will be the address of the next hop along the reverse route towards
	//					  the source peer that originated this received packet.
	//RETURNS
	//  True on success. If false, the packet should be discarded.
	bool PrepareToReceiveTerminus(netPeerRelayPacket* relayPkt,
								  const netSocketAddress& sender,
								  netAddress* reverseNextHopAddr) const;

	//PURPOSE
	// Returns the number of hops along the route to the specified peer.
	// Direct connection = 1 hop, relay server = 2 hops, peer relay >= 2 hops.
	u8 GetHopCount(const EndpointId epId) const;

	//PURPOSE
	// Returns the number of connections currently being relayed by the local peer.
	// NOTE
	// In this case, a connection is considered to be unilateral. If the local peer B is relaying 
	// from A -> B -> C and from C-> B -> A, the local peer would be relaying two connections.
	unsigned GetNumForwardedConnections() const;

	//PURPOSE
	// Fills out the header for a peer relay packet originating at the local peer.
	bool ResetP2pPacketHeader(netPeerRelayPacket* relayPkt,
							  const netPeerId& destPeerId,
							  const unsigned sizeofPayload);

	//////////////////////////////////////////////////////////////////////////
	// Tunables
	//////////////////////////////////////////////////////////////////////////

	//PURPOSE
	// Enables or disables all connection routing (including peer relaying and rerouting broken 
	// direct connections to the relay server).
	static void SetAllowConnectionRouting(const bool allowConnectionRouting);

	//PURPOSE
	// Enables or disables peer relaying.
	static void SetAllowPeerRelay(const bool allowPeerRelay);

	//PURPOSE
	// Along any p2p route, a packet can take as many as N - 1 hops to reach the destination peer,
	// where N is the number of peers in the network. HopCount = 1 indicates direct a connection.
	// This function sets the maximum supported number of hops along any p2p route between a source
	// peer and a destination peer. If a route cannot be found with hopCount <= maxHopCount, route
	// discovery is aborted. Increasing the max number of hops increases the probability that a 
	// route can be found through the p2p network, at the expense of higher latency and heavier 
	// load on each peer. Example: Sending from A to D via A -> B -> C -> D = 3 hops.
	static void SetMaxHopCount(const unsigned maxHopCount);

	//PURPOSE
	// Roughly sets the maximum number of connections a peer can relay simultaneously. Note: AODV
	// has no provision for limiting the number of connections a peer can relay at once. The 
	// actual maximum number of relayed connections can be higher than the set value. The set
	// value is used as a threshold that stops forwarding route requests once the local peer is
	// relaying >= the max desired amount of connections. Note also that, in this case, a
	// connection is considered to be unilateral. For example, if the local peer B is relaying 
	// from A -> B -> C and from C-> B -> A, the local peer would be relaying two connections.
	static void SetMaxRelayedCxns(const unsigned maxRelayedCxns);

	//PURPOSE
	// Override the default emergency relay fallback timeout. When the local peer fails to receive
	// a packet from a remote peer within this interval, the connection is rerouted through the
	// relay server. This should be larger than the broken link detection timeout.
	static void SetEmergencyRelayFallbackTimeMs(const unsigned emergencyRelayFallbackTimeMs);

	//PURPOSE
	// Override the default emergency direct receive timeout. When the local peer fails to receive
	// a packet from a directly-connected remote peer within this interval, the connection is rerouted 
	// through the relay server. This should be larger than the broken link detection timeout.
	static void SetEmergencyDirectReceiveTimeoutMs(const unsigned emergencyDirectReceiveTimeoutMs);

	//PURPOSE
	// Override the default broken link detection timeout. When the local peer fails to receive
	// a packet from a remote peer within this interval, the route is considered broken and
	// any nodes using this route are notified of the break. This should be less than the
	// emergency relay fallback time.
	static void SetBrokenLinkDetectionTimeMs(const unsigned brokenLinkDetectionTimeMs);

	//PURPOSE
	// Override the default route change wait time. Once we receive a reply to a route change
	// request, we wait this amount of time before switching the connection to the new route.
	// This currently applies only to Direct -> Relay Server route switches.
	static void SetRouteChangeWaitTimeMs(const unsigned routeChangeWaitTimeMs);

private:

	//PURPOSE
	// Node Identifier.
	class NodeId
	{
	public:
		NodeId();
		NodeId(const EndpointId endpointId, const netPeerId& peerId);
		~NodeId();
		void Clear();

		const EndpointId GetEndpointId() const;
		const netPeerId& GetPeerId() const;

		bool operator==(const NodeId& that) const;
		bool operator!=(const NodeId& that) const;
		bool operator<(const NodeId& that) const;

	private:
		EndpointId m_EndpointId;
		netPeerId m_PeerId;
	};

	//PURPOSE
	// For each valid route maintained by a node as a routing table entry,
	// the node also maintains a list of precursors that may be forwarding
	// packets on this route.  These precursors will receive notifications
	// from the node in the event of detection of the loss of the next hop
	// link. The list of precursors in a routing table entry contains those
	// neighboring nodes to which a route reply was generated or forwarded.
	class PrecursorNode
	{
	public:
		PrecursorNode(const netPeerId& peerId);
		~PrecursorNode();
		const netPeerId& GetPeerId() const;
		inlist_node<PrecursorNode> m_ListLink;
	private:
		netPeerId m_PeerId;
	};

	typedef inlist<PrecursorNode, &PrecursorNode::m_ListLink> PrecursorList;

	//PURPOSE
	// Nodes represent peers in a p2p mesh. The connections between each node can be
	// considered as the edges in a graph. Each node contains the routing table entry
	// for a specific peer. The collection of Nodes maintain the routing table from
	// the local peer to every other peer of the p2p mesh.
	class Node
	{
	public:
		enum Route
		{
			None,
			Direct,
			RelayServer,
			PeerRelay,
		};

		enum RouteState
		{
			ROUTE_STATE_INACTIVE = 0x01,
			ROUTE_STATE_ACTIVE = 0x02,
		};

#if !__NO_OUTPUT
		static const char* GetRouteTypeString(const Route route);
#endif

		Node(sysMemAllocator* m_Allocator, const EndpointId endpointId, const netPeerId& peerId, const Route route);
		~Node();

		void ClearPrecursorList();
		PrecursorList& GetPrecursorList();

		void SetRoute(const Route route);
		Route GetRoute() const;

		void SetNextHopPeerId(const netPeerId& nextHopPeerId);
		const netPeerId& GetNextHopPeerId() const;

		void SetNextDiscoveryTtl(const u8 ttl);
		u8 GetNextDiscoveryTtl() const;

		void SetNextDiscoveryTime(const unsigned time);
		unsigned GetNextDiscoveryTime() const;

		void IncDiscoveryAttempts();
		void ResetNumDiscoveryAttempts();
		unsigned GetNumDiscoveryAttempts() const;

		void SetLastReceiveTime(const unsigned time, const netAddress& sender);
		unsigned GetLastReceiveTime() const;

		void SetLastDirectReceiveTime(const unsigned time);
		unsigned GetLastDirectReceiveTime() const;

		void SetLastAddressAssignmentTime(const unsigned time);
		unsigned GetLastAddressAssignmentTime() const;

		void SetHopCount(const u8 hopCount);
		u8 GetHopCount() const;
		
		const SequenceNumber& GetDestSeq() const;
		void MaxDestSeq(const SequenceNumber& seq);
		void IncDestSeq();

		void SetDestSeqValid(const bool valid);
		bool IsDestSeqValid() const;

		void SetRouteActive(const bool active);
		bool IsRouteActive() const;

		const netPeerId& GetPeerId() const;
		const EndpointId GetEndpointId() const;
		const NodeId& GetNodeId() const;

		void AddPrecursor(const Node* node);
		void RemovePrecursor(const Node* nodeToRemove);
		void* Alloc(const unsigned size) const;
		void Free(void* ptr) const;

		//PURPOSE
		// When a node detects that a route is unstable, it remembers the next-hop of the failed
		// route and adds it to a blacklist. A node ignores all RREQs received from any node in its
		// blacklist. Nodes are removed from the blacklist set after a timeout period.
		void SetBlacklistExpirationTime(const unsigned expirationTime);
		unsigned GetBlacklistExpirationTime() const;
		bool IsBlacklisted() const;
		u8 GetBlacklistCount() const;
		void IncBlacklistCount();

		void SetChangeRouteSentTime(const unsigned time);
		unsigned GetChangeRouteSentTime() const;

		void SetChangeRouteReceivedReplyTime(const unsigned time);
		unsigned GetChangeRouteReceivedReplyTime() const;

#if !__NO_OUTPUT
		void DumpRoutingEntry() const;
#endif
		
		inlist_node<Node> m_ListLink;
		inmap_node<netPeerId, Node> m_ByPeerIdLink;

	private:
		PrecursorList m_PrecursorList;
		NodeId m_NodeId;
		Route m_Route;
		SequenceNumber m_DestSeq;
		netPeerId m_NextHopPeerId;
		u8 m_HopCount;
		u8 m_NextDiscoveryTtl;
		unsigned m_NextDiscoveryTime;
		u8 m_NumDiscoveryAttempts;
		unsigned m_LastReceiveTime;
		unsigned m_LastDirectReceiveTime;
		unsigned m_LastAddressAssignmentTime;
		sysMemAllocator* m_Allocator;
		u8 m_DestSeqValid;
		u8 m_Active;
		unsigned m_BlacklistExpirationTime;
		u8 m_BlacklistCounter;
		unsigned m_ChangeRouteSentTime;
		unsigned m_ChangeRouteReceivedReplyTime;
	};

	//PURPOSE
	// A route request can be uniquely identified by a (source peer id, route request id) pair.
	// When a node receives a route request, it checks to determine whether it has recently 
	// received a request with the same source peer id and route request id. If such a route
	// request has been received, the node silently discards the newly received route request.
	// This avoids routing loops when forwarding route requests.
	class RouteRequestId
	{
	public:
		RouteRequestId(const netPeerId& srcPeerId, const netConnectionRouter::SequenceNumber& srcRouteRequestId, const unsigned validUntilTime);
		netPeerId m_SrcPeerId;
		netConnectionRouter::SequenceNumber m_SrcRouteRequestId;
		unsigned m_ValidUntilTime;

		inlist_node<RouteRequestId> m_ListLink;
	};

	typedef inlist<RouteRequestId, &RouteRequestId::m_ListLink> RouteRequestBlacklist;

	//PURPOSE
	// Tracks the set of source, dest pairs for which the local peer is relaying.
	class RoutePair
	{
	public:
		class Key
		{
		public:
			Key()
			{

			}

			Key(const netPeerId& srcPeerId, const netPeerId& destPeerId)
			: m_SrcPeerId(srcPeerId)
			, m_DestPeerId(destPeerId)
			{

			}

			bool operator<(const RoutePair::Key& that) const;
			bool operator==(const RoutePair::Key& that) const;

			netPeerId m_SrcPeerId;
			netPeerId m_DestPeerId;
		};

		class Data
		{
		public:
			Data()
			: m_LastForwardTime(0)
			, m_Established(0)
			{
				
			}

			Data(const unsigned lastForwardTime, const bool established)
			: m_LastForwardTime(lastForwardTime)
			, m_Established(established ? 1 : 0)
			{

			}
				
			inmap_node<RoutePair::Key, RoutePair::Data> m_ByRoutePairKeyLink;
			inlist_node<RoutePair::Data> m_ListLink;
			unsigned m_LastForwardTime;
			u8 m_Established;
		};

		RoutePair(const netPeerId& srcPeerId, const netPeerId& destPeerId);

		Key m_Key;
	};

	static const unsigned ROUTE_PAIRS_ALLOC_STEP = 25;
	static const unsigned MAX_ROUTE_PAIRS_IN_POOL = (MAX_NUM_PEERS * MAX_NUM_PEERS);
	typedef netDynArray<RoutePair::Data, ROUTE_PAIRS_ALLOC_STEP, MAX_ROUTE_PAIRS_IN_POOL> RoutePairsPile;
	typedef inlist<RoutePair::Data, &RoutePair::Data::m_ListLink> RoutePairsList;
	typedef inmap<RoutePair::Key, RoutePair::Data, &RoutePair::Data::m_ByRoutePairKeyLink> RoutePairsMap;

	void* Alloc(const unsigned size) const;
	void Free(void* ptr) const;
	bool IsConnectionRoutingEnabled() const;
	bool IsPeerRelayEnabled() const;
	void UpdateProcessReceivedPackets();
	void UpdateRouteDiscovery();
	void UpdateDirectRouteMaintenance(Node* node);
	void UpdateRouteMaintenance();
	bool StartRouteDiscovery(Node* node, u8 ringTtl);
	bool CreateRouteRequest(Node* node, const u8 ringTtl, netRouteRequestMsg* msg);
	bool CreateRouteReply(const netPeerId& srcPeerId, const netPeerId& destPeerId, const SequenceNumber& destSeq, const u8 hopCount, const bool gratuitous, netRouteReplyMsg* msg);
	bool UnicastRouteReply(const netRouteReplyMsg& reply, const EndpointId epId);
	void MulticastRouteRequest(const netRouteRequestMsg& msg);
	void MulticastRouteRequest(const netRouteRequestMsg& msg, const netPeerId& skipPeerId);
	bool UnicastRouteRequest(const netRouteRequestMsg& msg, const EndpointId epId);
	bool UnicastRouteError(const netRouteErrMsg& rerr, const EndpointId epId);
	bool SendRouteChangeRequest(Node* node);
	bool SendRouteChangeReply(Node* node);
	void ReceiveRouteRequest(netRouteRequestMsg* msg, const EndpointId senderEpId, const netAddress& sender);
	void ReceiveRouteReply(netRouteReplyMsg* msg, const EndpointId senderEpId, const netAddress& sender);
	void ReceiveRouteError(netRouteErrMsg* rerr, const EndpointId senderEpId, const netAddress& sender);
	void ReceiveRouteChangeRequest(netRouteChangeRequestMsg* routeChangeRqst, const EndpointId senderEpId, const netAddress& sender);
	void ReceiveRouteChangeReply(netRouteChangeReplyMsg* routeChangeReply, const EndpointId senderEpId, const netAddress& sender);
	void SwitchToRelayServer(const netPeerId& peerId);
	void SwitchToRelayServer(Node* node);
	void SwitchDirectRouteToRelayServer(const netPeerId& peerId);
	void SwitchDirectRouteToRelayServer(Node* node);
	void RouteBroken(Node* node);
	void NewConnectionEstablished();
	void RemovePrecursor(const Node* nodeToRemove);
	void ProcessUnreachableDestinations(Node** unreachableDestinations, const unsigned numUnreachableDestinations);
	Node* GetNextHopNode(const netPeerId& peerId);
	Node* GetNodeByPeerId(const netPeerId& peerId);
	const Node* GetNodeByPeerId(const netPeerId& peerId) const;
	bool GetNextHopPeerId(const netPeerId& peerId, netPeerId* nextHopPeerId) const;

	void OnCxnEvent(netConnectionManager* UNUSED_PARAM(cxnMgr), const netEvent* evt);

	bool BlacklistRouteRequest(const netRouteRequestMsg& msg);
	void RemoveBlacklistedRouteRequest(const netRouteRequestMsg& msg);
	void UpdateRouteRequestBlacklist();

	void BlacklistPeer(const netPeerId& peerId);
	void UpdatePeerBlacklist();
	bool IsPeerBlacklisted(const netPeerId& peerId) const;

	bool TrackRoutePair(const netPeerId& srcPeerId, const netPeerId& destPeerId, const bool established);
	void UpdateRoutePairs();

#if !__NO_OUTPUT
	void DumpRoutingEntry(const Node* node) const;
	void DumpRoutingTable() const;
	void ReportMemory() const;
#endif

	typedef inlist<Node, &Node::m_ListLink> NodeList;
	typedef inmap<netPeerId, Node, &Node::m_ByPeerIdLink> NodesByPeerId;

	// constants
	static const unsigned UPDATE_INTERVAL_MS = (30);
	static const unsigned MIN_ROUTE_DISCOVERY_INTERVAL_MS = (5000);
	static const unsigned MAX_ROUTE_DISCOVERY_INTERVAL_MS = (5 * 60 * 1000);
	static const unsigned NEW_CXN_ROUTE_DISCOVERY_TIME_MS = (30 * 1000);
	static const unsigned PATH_DISCOVERY_TIME_MS = (10 * 1000);
	static const unsigned MIN_BLACKLIST_TIME_MS = (4 * 60 * 1000);
	static const unsigned MAX_BLACKLIST_TIME_MS = (30 * 60 * 1000);
#if NET_PEER_RELAY_DESTINATION_ONLY
	// in destination-only mode, a RREQ with TTL=1 won't get forwarded
	// from the intermediate peer, which  makes the first ring of the
	// expanding ring search a no-op.
	static const unsigned TTL_START = (2);
#else
	static const unsigned TTL_START = (1);
#endif
	static const unsigned TTL_INCREMENT = (1);
	static const unsigned RING_TRAVERSAL_TIME = (2 * 1000);

	// static members
	static bool sm_AllowConnectionRouting;
	static bool sm_AllowPeerRelay;
	static u8 sm_MaxHopCount;
	static unsigned sm_MaxRelayedCxns;
	static unsigned sm_EmergencyRelayFallbackTimeMs;
	static unsigned sm_EmergencyDirectReceiveTimeoutMs;
	static unsigned sm_BrokenLinkDetectionTimeMs;
	static unsigned sm_RouteChangeWaitTimeMs;
	static bool sm_EmergencyDirectReceiveTimeoutOverridden;
	static bool sm_EmergencyRelayFallbackTimeoutOverridden;
	static netRandom sm_Rng;

	// data members
	netConnectionManager* m_CxnMgr;
	sysMemAllocator* m_Allocator;
	unsigned m_LastUpdateTime;
	SequenceNumber m_RouteRequestId;
	unsigned m_RouteReplyId;
	SequenceNumber m_LocalSeq;
	netPeerId m_LocalPeerId;

	// Subscribes to netEvents from the connection manager.
	netEventConsumer<netEvent> m_EventSubscriber;

	NodeList m_Nodes;
	NodesByPeerId m_NodesByPeerId;
	RouteRequestBlacklist m_RouteRequestBlacklist;

	// we use a contiguous, growable pool of RoutePairs to avoid flooding the 
	// allocator with small allocations that stick around for a long time.
	RoutePairsPile m_RoutePairsPile;
	RoutePairsList m_RoutePairsPool;
	RoutePairsMap m_RoutePairs;
	unsigned m_NumForwardedConnections;
	unsigned m_MaxForwardedConnections;
};

}   //namespace rage

#endif  //NET_CONNECTIONROUTER_H
