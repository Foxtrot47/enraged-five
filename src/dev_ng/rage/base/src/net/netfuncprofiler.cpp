// 
// net/net.cpp
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#include "netfuncprofiler.h"

#include "system/memory.h"
#include "system/timer.h"
#include "netdiag.h"

namespace rage
{

#if NET_PROFILER

RAGE_DEFINE_SUBCHANNEL(ragenet, funcprofiler)
#undef __rage_channel
#define __rage_channel ragenet_funcprofiler

bool netFunctionProfiler::sm_Started = false;
bool netFunctionProfiler::sm_Enabled = true;
bool netFunctionProfiler::sm_EnablePushAndPop = true;

netFunctionProfiler netFunctionProfiler::sm_profiler;

int netFunctionProfiler::sm_nextAvailableProfilerInPool = -1;
netFunctionProfiler* netFunctionProfiler::sm_profilerPool[MAX_PROFILER_COUNT] = {0};

void netFunctionProfiler::Init()
{
	sysMemStartDebug(); 
	for(int i = 0; i < MAX_PROFILER_COUNT; i++)
	{
		sm_profilerPool[i] = rage_new netFunctionProfiler();
	}
	sysMemEndDebug(); 
}

void netFunctionProfiler::Shutdown()
{
	sysMemStartDebug(); 
	for(int i = 0; i < MAX_PROFILER_COUNT; i++)
	{
		delete(sm_profilerPool[i]);
		sm_profilerPool[i] = nullptr;
	}
	sysMemEndDebug(); 
}

void netFunctionProfiler::SetProfilerEnabled(const bool enabled)
{
	if(sm_Enabled != enabled)
	{
		netDebug("SetProfilerEnabled :: Profiler %s", enabled ? "Enabled" : "Disabled");
		sm_Enabled = enabled;
	}
}

netFunctionProfiler::netFunctionProfiler(unsigned depth/*=0*/)
	: m_Depth(depth)
	, m_AssertOnLongFrame(false)
	, m_IgnoreChildrenBelowMs(DEFAULT_LOGGING_THRESHOLD)
	, m_ShouldProfile(true)
	, m_parentProfiler(nullptr)
	, m_DumpForTooManySubProfilers(false)
{
	Clear();
}

netFunctionProfiler::~netFunctionProfiler()
{
	Clear();
}

void netFunctionProfiler::Start(const char* name)
{
	if(!sm_Enabled)
	{
		return;
	}

	Clear();
	m_TimeStarted = sysTimer::GetSystemMsTime();
	safecpy(m_Name, name);
}

void netFunctionProfiler::Finish(unsigned dumpIfLongerThanMs /*=0*/)
{
	if(!sm_Enabled)
	{
		return;
	}

	netAssertf(m_Depth != 0, "Finish :: Cannot call Finish on the root profiler");

	m_TimeFinished = sysTimer::GetSystemMsTime();

	if(dumpIfLongerThanMs > 0 && ((m_TimeFinished - m_TimeStarted) > dumpIfLongerThanMs))
	{
        netDebug("Finish :: %s - Time: %dms over threshold %dms", m_Name, m_TimeFinished - m_TimeStarted, dumpIfLongerThanMs);
        Dump();
        netAssertf(!m_AssertOnLongFrame, "Finish :: %s - Time: %dms over threshold %dms", m_Name, m_TimeFinished - m_TimeStarted, dumpIfLongerThanMs);
    }
	if(m_parentProfiler && m_parentProfiler->m_IgnoreChildrenBelowMs > (m_TimeFinished - m_TimeStarted)) 
	{
		// this node didn't go over the threshold, put it back in the pool
		m_parentProfiler->ReleaseLastProfiler();
	}
}

void netFunctionProfiler::StartFrame()
{
	if(!sm_Enabled)
	{
		return;
	}

	netAssertf(m_Depth == 0, "StartFrame :: Cannot call StartFrame on a child profiler");
	netAssertf((m_subprofilerCount == 0) || Top()->IsFinished(), "StartFrame :: Cannot start when there's still a pushed profiler");
	netAssertf(!sm_Started, "StartFrame :: Cannot start again a non terminated Profiler");
	sm_Started = true;
}

void netFunctionProfiler::TerminateFrame(unsigned dumpIfLongerThanMs /*= 0*/)
{
	if(!sm_Enabled)
	{
		return;
	}

	netAssertf(m_Depth == 0, "TerminateFrame :: Cannot call TerminateFrame on a child profiler");
	netAssertf((m_subprofilerCount == 0) || (netVerify(Top()) && Top()->IsFinished()), "TerminateFrame :: Cannot start when there's still a pushed profiler");
	netAssertf(sm_Started, "TerminateFrame :: Cannot start again a non terminated Profiler");
	m_TimeFinished = sysTimer::GetSystemMsTime();
	if(dumpIfLongerThanMs > 0 && ((m_TimeFinished - m_TimeStarted) > dumpIfLongerThanMs))
	{
		Dump();
		netAssertf(!m_AssertOnLongFrame, "TerminateFrame :: %s - Time: %dms over threshold %dms", m_Name, m_TimeFinished - m_TimeStarted, dumpIfLongerThanMs);
	}
	sm_nextAvailableProfilerInPool = 0;
	Clear();
	sm_Started = false;
}

static const char* DUMP_PREFIX = "Dump: ";

void netFunctionProfiler::Dump(bool bForce, int index)
{
	if(!sm_Enabled)
	{
		netAssert(m_subprofilerCount == 0);
		return;
	}

	if(!bForce && (GetDuration() < m_IgnoreChildrenBelowMs))
		return;

	static const unsigned DUMP_PREFIX_LENGTH = static_cast<unsigned>(strlen(DUMP_PREFIX)); 
	static const unsigned DUMP_TEXT_MAX_LENGTH = 512;
	
	char messageShifted[DUMP_TEXT_MAX_LENGTH];
	safecpy(messageShifted, DUMP_PREFIX);
	unsigned i = DUMP_PREFIX_LENGTH;
	for(; (i < DUMP_PREFIX_LENGTH + m_Depth) && (i < DUMP_TEXT_MAX_LENGTH); i++)
	{
		messageShifted[i] = SHIFTING_CHARACTER;
	}

	char message[DUMP_TEXT_MAX_LENGTH];
	formatf(message, "[%02d] %4dms : %s", index, GetDuration(), m_Name);
	safecpy(&messageShifted[i], message, 512-i);
	netDebug1("%s", messageShifted);
	for(unsigned j = 0; j < m_subprofilerCount; j++)
	{
		m_SubProfilers[j]->Dump(bForce, j);
	}
}

void netFunctionProfiler::Clear()
{
	m_TimeStarted = m_TimeFinished = 0;
	memset(m_SubProfilers, 0, sizeof(netFunctionProfiler*) * MAX_SUBPROFILERS);
	m_subprofilerCount = 0;
	m_parentProfiler = nullptr;
	m_DumpForTooManySubProfilers = false;
}

bool netFunctionProfiler::PushProfiler(const char* labelName, bool withAssertOnLongFrames/*=false*/)
{
	if(!sm_Enabled || !sm_EnablePushAndPop || !m_ShouldProfile)
	{
		return false;
	}

	static bool s_bDumpForTooManyProfilers = true; 

	// if the last one was finished, start a new one, otherwise start a child on the current one.
	if((m_subprofilerCount == 0) || (netVerify(Top()) && Top()->IsFinished()))
	{
		// do this here so that we can get the top profiler name
		if(!netVerifyf(labelName != nullptr, "PushProfiler :: Invalid name! Current: %s", labelName))
		{
			return false;
		}

		if(netVerifyf(m_subprofilerCount < MAX_SUBPROFILERS, "PushProfiler :: Already have maximum of %u sub-profilers (Pushing '%s' in '%s')", MAX_SUBPROFILERS, labelName, m_Name))
		{
			// wait until we are clear again before we dump (prevents multiple dumps in a row)
			m_DumpForTooManySubProfilers = true;

			netFunctionProfiler* subprofiler = GetNewProfiler(labelName, m_Depth + 1);
			if(netVerifyf(subprofiler, "PushProfiler :: Pool exhausted. Max: %u (Pushing '%s' in '%s')", MAX_PROFILER_COUNT, labelName, m_Name))
			{
				// wait until we are clear again before we dump (prevents multiple dumps in a row)
				s_bDumpForTooManyProfilers = true;

				subprofiler->SetAssertOnLongFrames(withAssertOnLongFrames);
				subprofiler->SetTimeThreshold(m_IgnoreChildrenBelowMs);
				subprofiler->m_parentProfiler = this;
				m_SubProfilers[m_subprofilerCount] = subprofiler;
				m_subprofilerCount++;
			}
			else if(s_bDumpForTooManyProfilers)
			{
				netError("PushProfiler :: Pool exhausted - Max: %u (Pushing '%s' in '%s'). Dumping...", MAX_PROFILER_COUNT, labelName, m_Name);
				sm_profiler.Dump(true);
				Collapse();
				s_bDumpForTooManyProfilers = false;
				return false;
			}
		}
		else if(m_DumpForTooManySubProfilers)
		{
			netError("PushProfiler :: Already have maximum of %u sub-profilers (Pushing '%s' in '%s'). Dumping...", MAX_SUBPROFILERS, labelName, m_Name);
			sm_profiler.Dump(true);
			m_DumpForTooManySubProfilers = false;
			return false;
		}
	}
	else
	{
		return netVerify(Top()) && Top()->PushProfiler(labelName, withAssertOnLongFrames);
	}
	return true;
}

bool netFunctionProfiler::PopProfiler(const char* labelName, unsigned dumpIfLongerThanMs/*=0*/)
{
	if(!sm_Enabled || !sm_EnablePushAndPop || !m_ShouldProfile)
	{
		return false;
	}

	if((m_subprofilerCount > 0) && netVerify(Top()) && !Top()->IsFinished())
	{
		return Top()->PopProfiler(labelName, dumpIfLongerThanMs);
	}
	else
	{
		netAssertf(labelName != nullptr, "PopProfiler :: Invalid name! Current: %s", m_Name);
		netAssertf(!IsFinished(), "PopProfiler :: Cannot finish a profiler twice, Current: %s, Popping: %s", m_Name, labelName);

		if(labelName && !netVerifyf(strcmp(labelName, m_Name) == 0, "PopProfiler :: Not popping last pushed profiler (%s). Push / pop out of sync. Popping: %s", m_Name, labelName))
		{
			// don't pop this if our label doesn't match the current profiler
			// let null labels through (for now) - those assert above
			return false;
		}
			
		Finish(dumpIfLongerThanMs);
		return true; 
	}
}

unsigned netFunctionProfiler::GetSubProfilerCountForTop()
{
	// work our way to the currently active profiler that we would push on
	if(m_subprofilerCount > 0)
	{
		netFunctionProfiler* pTop = Top(); 
		if(pTop)
		{
			if(!pTop->IsFinished())
			{
				return pTop->GetSubProfilerCountForTop();
			}
		}
	}
	return m_subprofilerCount;
}

netFunctionProfiler* netFunctionProfiler::GetCurrentTop()
{
	netFunctionProfiler* pProfiler = &GetProfiler();
	while(pProfiler->Top() && !pProfiler->Top()->IsFinished())
	{
		pProfiler = pProfiler->Top();
	}

	return pProfiler; 
}

bool netFunctionProfiler::IsFinished() const
{
	return m_TimeFinished != 0;
}

unsigned netFunctionProfiler::GetTotalChildDuration() const
{
	unsigned result=0;
	for(unsigned j = 0; j < m_subprofilerCount; j++)
	{
		result += m_SubProfilers[j]->m_TimeFinished - m_SubProfilers[j]->m_TimeStarted;
	}
	return result;
}

unsigned netFunctionProfiler::GetDuration() const
{
	if(m_TimeStarted == 0)
		return 0;

	if(m_TimeFinished == 0)
		return 0;

	return m_TimeFinished - m_TimeStarted;
}

void netFunctionProfiler::SetIgnoreChildrenBelowMs(unsigned threshold)
{
	m_IgnoreChildrenBelowMs=threshold;
}

netFunctionProfiler* netFunctionProfiler::Top()
{
	if(m_subprofilerCount > 0)
	{
		return m_SubProfilers[m_subprofilerCount-1];
	}
	return nullptr;
}

void netFunctionProfiler::SetTimeThreshold(unsigned threshold)
{
	if((m_subprofilerCount > 0) && netVerify(Top()) && !Top()->IsFinished())
	{
		Top()->SetTimeThreshold(threshold);
	}
	else
	{
		SetIgnoreChildrenBelowMs(threshold);
	}
}

void netFunctionProfiler::SetShouldProfile(bool shouldProfile)
{
	// We ignore this if there's still a profiler going on
	if((m_subprofilerCount == 0) || ((m_subprofilerCount > 0) && netVerify(Top()) && Top()->IsFinished()) )
	{
		m_ShouldProfile = shouldProfile;
	}
}

netFunctionProfiler* netFunctionProfiler::GetNewProfiler(const char* name, unsigned depth)
{
	if(!netVerifyf(sm_profilerPool[0], "GetNewProfiler :: Init has not been called yet"))
		return nullptr;

	if(sm_nextAvailableProfilerInPool + 1 < MAX_PROFILER_COUNT)
	{
		sm_nextAvailableProfilerInPool++;
		netFunctionProfiler* result;
		result = sm_profilerPool[sm_nextAvailableProfilerInPool];
		result->Clear();
		result->m_Depth = depth;
		result->Start(name);
		return result;
	}
	return nullptr;
}

void netFunctionProfiler::ReleaseLastProfiler()
{
	if(netVerifyf(sm_nextAvailableProfilerInPool >= 0, "ReleaseLastProfiler :: Trying to release the root profiler"))
	{
		sm_profilerPool[sm_nextAvailableProfilerInPool]->Clear();
		sm_nextAvailableProfilerInPool--;
	}
	m_subprofilerCount--;
}

void netFunctionProfiler::Collapse()
{
	while(m_subprofilerCount)
	{
		ReleaseLastProfiler();
	}
	memset(m_SubProfilers, 0, sizeof(netFunctionProfiler*) * MAX_SUBPROFILERS);
}

void netFunctionProfiler::SetAssertOnLongFrames(bool isActivated)
{
	if(m_AssertOnLongFrame != isActivated)
	{
		netDebug("SetAssertOnLongFrames :: %s", isActivated ? "Enabled" : "Disabled");
		m_AssertOnLongFrame = isActivated;
	}
}

#endif // NET_PROFILER

}   //namespace rage