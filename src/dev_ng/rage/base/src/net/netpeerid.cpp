// 
// net/netpeerid.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 
#include "netpeerid.h"
#include "data/bitbuffer.h"
#include "diag/seh.h"
#include "netdiag.h"
#include "rline/rl.h"
#include "string/string.h"
#include <string.h>

namespace rage
{

PARAM(netpeerid, "Sets the local peer's peer ID to the specified value. E.g. -netpeerid=0xAAAAAAAAAAAAAAAA");

sysCriticalSectionToken netPeerId::sm_LocalPeerIdCs;
u64 netPeerId::sm_LocalPeerId = netPeerId::NET_INVALID_PEER_ID;

//////////////////////////////////////////////////////////////////////////
// netPeerId
//////////////////////////////////////////////////////////////////////////
const netPeerId netPeerId::INVALID_PEER_ID;

netPeerId::netPeerId()
{
    this->Clear();
}

void
netPeerId::Clear()
{
    m_Id = NET_INVALID_PEER_ID;
}

bool
netPeerId::IsValid() const
{
    return (m_Id != NET_INVALID_PEER_ID);
}

bool
netPeerId::IsLocal() const
{
	return IsValid() && (sm_LocalPeerId == m_Id);
}

u64 netPeerId::GetU64() const
{
	return m_Id;
}

bool
netPeerId::GetLocalPeerId(netPeerId& peerId)
{
	SYS_CS_SYNC(sm_LocalPeerIdCs);

	bool success = true;

	if(sm_LocalPeerId == NET_INVALID_PEER_ID)
	{
#if !RSG_FINAL
		const char* szPeerId = nullptr;
		if(PARAM_netpeerid.Get(szPeerId))
		{
			if(!netVerify(sscanf(szPeerId, "0x%" I64FMT "x", &sm_LocalPeerId) == 1))
			{
				success = false;
			}
		}
		else
#endif
		{
			success = rlCreateUUID(&sm_LocalPeerId);
		}

		netDebug("My netPeerId is 0x%" I64FMT "x", sm_LocalPeerId);
	}

	peerId.m_Id = sm_LocalPeerId;

	return success;
}

bool
netPeerId::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
    if(!netVerify(this->IsValid()))
    {
        return false;
    }

	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	const bool success = bb.SerUns(m_Id, sizeof(m_Id) << 3);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool
netPeerId::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
	datImportBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf);

	const bool success = bb.SerUns(m_Id, sizeof(m_Id) << 3);

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

const char*
netPeerId::ToHexString(char* dst, const unsigned dstLen) const
{
	return formatf_sized(dst, dstLen, "0x%" I64FMT "x", m_Id);
}

bool
netPeerId::operator==(const netPeerId& that) const
{
    return (m_Id == that.m_Id);
}

bool
netPeerId::operator!=(const netPeerId& that) const
{
    return (m_Id != that.m_Id);
}

bool
netPeerId::operator<(const netPeerId& that) const
{
    return m_Id < that.m_Id;
}

bool
netPeerId::operator>(const netPeerId& that) const
{
	return m_Id > that.m_Id;
}

}   //namespace rage
