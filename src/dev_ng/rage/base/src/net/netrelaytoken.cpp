// 
// net/netrelaytoken.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 
#include "netrelaytoken.h"
#include "data/bitbuffer.h"
#include "diag/seh.h"
#include "netdiag.h"
#include "relay.h"
#include "string/string.h"
#include "system/memops.h"

namespace rage
{

CompileTimeAssert(netRelayPacket::SIZEOF_SECURE_TOKEN == netRelayToken::MAX_TOKEN_BUF_SIZE);

//////////////////////////////////////////////////////////////////////////
// netRelayToken
//////////////////////////////////////////////////////////////////////////
const netRelayToken netRelayToken::INVALID_RELAY_TOKEN;

netRelayToken::netRelayToken()
{
	this->Clear();
}

netRelayToken::netRelayToken(const u8 (&tokenBuf)[MAX_TOKEN_BUF_SIZE])
{
    this->Clear();
	sysMemCpy(m_Token, tokenBuf, sizeof(m_Token));
}

void
netRelayToken::Clear()
{
    sysMemSet(m_Token, 0, sizeof(m_Token));
}

bool
netRelayToken::IsValid() const
{
    return this->operator!=(INVALID_RELAY_TOKEN);
}

bool
netRelayToken::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
	datExportBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	const bool success = bb.SerBytes(m_Token, MAX_TOKEN_BUF_SIZE);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool
netRelayToken::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
	datImportBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf);

	const bool success = bb.SerBytes(m_Token, MAX_TOKEN_BUF_SIZE);

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

const char*
netRelayToken::ToHexString(char* dst, const unsigned dstLen) const
{
	if(dst && (dstLen >= TO_HEX_STRING_BUFFER_SIZE))
	{
		safecpy(dst, "0x", dstLen);
		for(unsigned i = 0; i < MAX_TOKEN_BUF_SIZE; ++i)
		{
			formatf_sized((dst+2)+(i*2), 3, "%02x", m_Token[i]);
		}

		return dst;
	}

	return "";
}

bool
netRelayToken::operator==(const netRelayToken& that) const
{
    return memcmp(m_Token, that.m_Token, sizeof(m_Token)) == 0;
}

bool
netRelayToken::operator!=(const netRelayToken& that) const
{
    return memcmp(m_Token, that.m_Token, sizeof(m_Token)) != 0;
}

bool
netRelayToken::operator<(const netRelayToken& that) const
{
    return memcmp(m_Token, that.m_Token, sizeof(m_Token)) < 0;
}

bool
netRelayToken::operator>(const netRelayToken& that) const
{
	return memcmp(m_Token, that.m_Token, sizeof(m_Token)) > 0;
}

}   //namespace rage
