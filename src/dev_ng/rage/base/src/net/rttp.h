// 
// net/rttp.h
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_RTTP_H
#define NET_RTTP_H

#include "tcp.h"

#include "atl/delegate.h"
#include "net/time.h"
#include "zlib/zlib.h"

namespace rage
{

/*
    RTTP is Rockstar Text Transport Protocol.  RTTP message content is
    text, but the header is binary, so technically it's not a 100%
    text protocol.

    RTTP message content is expected to be in RSON format.

    Each RTTP request has a unique transaction id.  When a response is
    received it is matched up with the request based on the transaction id.
    This allows multiple requests to be outstanding.

    RTTP can not only be used for request/response, but also for "push".
    Pushed messages are unsolicited messages, i.e. they are not triggered
    by a request.  Push messages have a transaction id of zero.

    RTTP Format
    ===========

     --------------------------------------------------
    |76543210|76543210765432107654321076543210|76543210|
    |  Mixer |           Signature            | Flags  |
     --------------------------------------------------
    |7654321076543210|765432107654321076543210|76543210....
    | Transaction Id |     Content Size       | Content
     --------------------------------------------------

    Mixer:
    The mixer is an 8-bit value, randomly selected for every message,
    that is used to "mix up" all bytes after the mixer by xoring them
    with the mixer.

    Upon receipt all bytes after the mixer are unmixed by xoring them
    with the mixer.

    Signature:
    32-bit value that identifies the message as an RTTP message.  The
    signature will change when the version changes.

    Flag Bits:
    0   :   1 if content is compressed

    Transaction Id:
    16-bit value that uniquely identifies the message.

    Messages with a non-zero transaction id are considered "request" messages
    and require a response.

    Messages with a transaction id of zero are considered "push" messages
    and require no response.

    Content Size:
    24-bit value indicating the *uncompressed* size of the content.

    Content:
    Message content.  Deflate (RFC 1951) is used for compression.
*/

class netRttp;
class sysMemAllocator;

enum netRttpVersions
{
    RTTP_VERSION_0              = 0,    //Original version
    RTTP_VERSION_1,
    RTTP_VERSION_2,
    RTTP_VERSION_3,
    RTTP_NUM_VERSIONS,

    RTTP_CURRENT_VERSION        = RTTP_VERSION_3,
    RTTP_INVALID_VERSION        = 255
};

//Flags included in the RTTP header.
enum netRttpHeaderFlags
{
    RTTP_HEADERFLAG_STREAM                          = 1 << 0,
    RTTP_HEADERFLAG_KEEPALIVE                       = 1 << 1,

    RTTP_HEADERPFLAG_NUMFLAGS = 8
};

//PURPOSE
//  Value for invalid transaction ids.  Push messages should have
//  this value for their transaction ids.
const u16 RTTP_INVALID_TXID = 0;

//PURPOSE
//  Describes the header that precedes RTTP message content.
//  This tells the receiver everything they need to know to read the data.
class netRttpHeader
{
    //A simple hash from "The Art Of Computer Programming" to create the
    //signature and make it unique for the current version.
    static const unsigned SEED  = 5381;
    static const unsigned SIG0  = ((SEED << 5) ^ (SEED >> 27)) ^ RTTP_CURRENT_VERSION;
    static const unsigned SIG1  = ((SIG0 << 5) ^ (SIG0 >> 27)) ^ 'R';
    static const unsigned SIG2  = ((SIG1 << 5) ^ (SIG1 >> 27)) ^ 'T';
    static const unsigned SIG3  = ((SIG2 << 5) ^ (SIG2 >> 27)) ^ 'T';
    static const unsigned SIG4  = ((SIG3 << 5) ^ (SIG3 >> 27)) ^ 'P';

public:

    //Each element of the header should be byte aligned.
    //This makes it much easier to parse the header on the
    //server, typically written in C#, which likely doesn't
    //have a datBitBuffer class.
    enum
    {
        BIT_SIZEOF_MIXER        = 8,
        BIT_SIZEOF_SIGNATURE    = 32,
        BIT_SIZEOF_FLAGS        = RTTP_HEADERPFLAG_NUMFLAGS,
        BIT_SIZEOF_TXID         = 16,
        BIT_SIZEOF_CONTENT_LEN  = 24,

        BIT_SIZEOF_HEADER   =
            BIT_SIZEOF_MIXER
            + BIT_SIZEOF_SIGNATURE
            + BIT_SIZEOF_FLAGS
            + BIT_SIZEOF_TXID
            + BIT_SIZEOF_CONTENT_LEN,

        BYTE_SIZEOF_HEADER = (BIT_SIZEOF_HEADER + 7) / 8,

        //Maximum length of RTTP content.
        MAX_CONTENT_LENGTH  = (1 << BIT_SIZEOF_CONTENT_LEN) - 1,
    };

    //Make sure our header is byte aligned.
    CompileTimeAssert(BYTE_SIZEOF_HEADER == (BIT_SIZEOF_HEADER >> 3));

    static const unsigned SIGNATURE = SIG4;

    netRttpHeader();

    //PURPOSE
    //  Resets the header for a keepalive message.  A keepalive
    //  is uncompressed, and has no content or txid.
    void ResetForKeepAlive(const u32 signature);
    
    //PURPOSE
    //  Resets the header for a "pushed" (i.e. nontransactional) message.
    void ResetForPush(const u32 signature, 
                      const u32 contentLen,
                      const u8 flags);

    //PURPOSE
    //  Resets the header for a transaction msg (either a reply or a request).
    void ResetForTransaction(const u32 signature, 
                            const u32 contentLen,
                            const u16 txId,
                            const u8 flags);

    //PURPOSE
    //  Clears the header to initial values.
    void Clear();

    bool IsValid() const;
    bool IsTransaction() const;
    bool IsStream() const;

    u32 GetSignature() const;
    u8 GetMixer() const;
    u8 GetFlags() const;

    u16 GetTransactionId() const;
    
    //PURPOSE
    //  Returns the number of *uncompressed* bytes in the message content.
    u32 GetContentLength() const;

    //PURPOSE
    //  Exports a header to bit-compressed format, using the bit
    //  offsets defined above.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports a header from bit-compressed format, using the bit
    //  offsets defined above.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

private:

    static u8 CreateMixer();

    u32 m_Signature;
    u32 m_ContentLength;
    u16 m_TxId;
    u8 m_Mixer;
    u8 m_Flags;
};

//PURPOSE
//  Describes an RTTP message.
class netRttpMessage
{
public:
    netRttpMessage();

    void Clear();

    //PURPOSE
    //  Prepares for a keepalive message.  Keepalives have a header
    //  but no content.
    void ResetForKeepAlive();

    //PURPOSE
    //  Prepares for a transaction (request/response) or push message.
    //NOTES
    //  Push messages must have a transaction id of RTTP_INVALID_TXID.
    void Reset(const void* content,
                const u32 contentLen,
                const u16 txId);

    //PURPOSE
    //  Returns a pointer to the message content.
    const void* GetContent() const;

    //PURPOSE
    //  Returns the number of bytes in the message content.
    unsigned GetContentLength() const;

    //PURPOSE
    //  Returns the transaction id.  For keepalives and push messages
    //  the transaction id will be RTTP_INVALID_TXID.
    u16 GetTransactionId() const;

    //PURPOSE
    //  Returns true if the message is a transaction, i.e. the
    //  transaction id is not RTTP_INVALID_TXID.
    bool IsTransaction() const;

private:

    netRttpMessage(const netRttpMessage&);
    netRttpMessage& operator=(const netRttpMessage&);

    unsigned m_ContentLen;
    const u8* m_Content;
    u16 m_TxId;
};

//PURPOSE
//  Represents the state of an RTTP request.  To send a request users must call
//  netRttp::SendRequest(), passing a pointer to the request content,
//  a pointer to a netRttpRequest object, and the number of seconds before
//  a timeout occurs.
//
//  Neither the request object nor the content buffer can be deallocated while
//  the request is pending.
//
//  The request completes when a response is received, at which time
//  GetResponseMsg() can be called to retrieve the response.
//
//  When finished with the response call Dispose() to release resources.
class netRttpRequest
{
    friend class netRttp;

public:

    netRttpRequest();
    ~netRttpRequest();

    //PURPOSE
    //  Releases resources that were allocated for the response.
    //  Users should call this after they are done with the
    //  response content.
    void Dispose();

    //PURPOSE
    //  Returns true while the request is pending.
    bool Pending() const;

    //PURPOSE
    //  Returns true if the request succeeded.
    bool Succeeded() const;

    //PURPOSE
    //  Returns the request content.
    const netRttpMessage& GetRequestMsg() const;

    //PURPOSE
    //  When the request completes this returns the response content.
    const netRttpMessage& GetResponseMsg() const;

    //PURPOSE
    //  Returns the transaction id.
    u16 GetTransactionId() const;

    //PURPOSE
    //  Returns true if the message is a transaction, i.e. the
    //  transaction id is not RTTP_INVALID_TXID.
    bool IsTransaction() const;

private:

    netRttpRequest(const netRttpRequest&);
    netRttpRequest& operator=(const netRttpRequest&);

    void ResetRequest(const void* content,
                        const u32 contentSize,
                        const u16 txId,
                        const unsigned timeoutSecs);

    void SetResponse(sysMemAllocator* allocator,
                    const void* content,
                    const u32 contentSize);

    void Finish(const bool success);

    //If non-null then the memory pointed to by m_ResponseContent
    //was allocated using the allocator and needs to be freed
    //in Dispose().
    sysMemAllocator* m_Allocator;
    const void* m_ResponseContent;

    netRttpMessage m_RequestMsg;
    unsigned m_TimeoutSecs;     //Timeout used for request writing
    int m_TimeLeftMs;           //Time left before timing out;

    netRttpMessage m_ResponseMsg;

    netStatus m_Status;

    inlist_node<netRttpRequest> m_ListLink;
};

//PURPOSE
//  Encodes/decodes RTTP messages in a stream format.
//  When encoding it's not necessary to have the entire source buffer.
//
//  If the entire buffer is present, begin encoding by calling the variant
//  of BeginEncode() that takes the content length.  Extra checking will
//  be performed that ensures the entire content buffer (and no more) is
//  encoded.
//  If the entire buffer is not present then call the variant of
//  BeginEncode() that takes only a transaction id.
//
//  After calling BeginEncode() call Encode() until the content buffer
//  is consumed.  Continue calling Encode() with a zero source length
//  until IsComplete() returns true.
//
//  To decode call BeginDecode(), and then continue calling Decode()
//  until IsComplete() returns true.  When decoding it's possible to
//  pass NULL for src and zero for srcLen until the RTTP header has been
//  decoded.  Call GetHeader().IsValid() to determine when the header
//  has been decoded.
class netRttpStream
{
public:

    netRttpStream();

    ~netRttpStream();

    //PURPOSE
    //  Releases resources and sets the stream to initial values.
    void Clear();

    //PURPOSE
    //  Begins encoding a new RTTP message.
    //PARAMS
    //  contentLen      - Length of the content buffer
    //  txId            - Transaction id.
    //NOTES
    //  If the content length is unknown pass zero for contentLen.
    //  If contentLen is zero the RTTP header will have the STREAM
    //  bit set and the content length will be zero.
    bool BeginEncode(const unsigned contentLen,
                    const u16 txId);

    //PURPOSE
    //  Begins encoding a new RTTP message.
    //PARAMS
    //  txId            - Transaction id.
    //NOTES
    //  The RTTP header will have the STREAM bit set and the content
    //  length will be zero.
    bool BeginEncode(const u16 txId);

    //PURPOSE
    //  Begins decoding a RTTP message.
    bool BeginDecode();

    //PURPOSE
    //  Encodes data into the current RTTP message.
    //PARAMS
    //  src     - Source buffer.
    //  srcLen  - Number of bytes in the source buffer.
    //  dst     - Destination buffer.
    //  dstLen  - Number of bytes in the destination buffer.
    //  numConsumed - On return contains the number of source bytes consumed.
    //  numProduced - On return contains the number of destination bytes produced.
    //NOTES
    //  When the entire content buffer has been encoded continue calling
    //  Encode() with NULL src and zero srcLen until IsComplete() returns
    //  true, or HasError() returns true.
    void Encode(const void* src,
                const unsigned srcLen,
                void* dst,
                const unsigned dstLen,
                unsigned* numConsumed,
                unsigned* numProduced);

    //PURPOSE
    //  Decodes data from the current RTTP message.
    //PARAMS
    //  src     - Source buffer.
    //  srcLen  - Number of bytes in the source buffer.
    //  dst     - Destination buffer.
    //  dstLen  - Number of bytes in the destination buffer.
    //  numConsumed - On return contains the number of source bytes consumed.
    //  numProduced - On return contains the number of destination bytes produced.
    //NOTES
    //  dst can be NULL and dstLen until the header is decoded.  Call
    //  GetHeader().IsValid() to determine if the header has been decoded.
    //
    //  Continue call Decode() until IsComplete() returns true, or HasError()
    //  returns true.
    void Decode(const void* src,
                const unsigned srcLen,
                void* dst,
                const unsigned dstLen,
                unsigned* numConsumed,
                unsigned* numProduced);

    //PURPOSE
    //  Returns the header for the current RTTP message.
    const netRttpHeader& GetHeader() const;

    //PURPOSE
    //  Returns the total number of bytes consumed by the current
    //  encode/decode operation.
    unsigned GetNumConsumed() const;

    //PURPOSE
    //  Returns the total number of bytes produced by the current
    //  encode/decode operation.
    unsigned GetNumProduced() const;

    //PURPOSE
    //  Returns true if currently encoding.
    bool IsEncoding() const;

    //PURPOSE
    //  Returns true if currently decoding.
    bool IsDecoding() const;

    //PURPOSE
    //  Returns true if the operation (encoding/decoding) completed
    //  successfully.
    bool IsComplete() const;

    //PURPOSE
    //  Returns true if the operation (encoding/decoding) caused an error.
    bool HasError() const;

private:

    enum StreamType
    {
        STREAMTYPE_INVALID  =   -1,
        STREAMTYPE_ENCODE,
        STREAMTYPE_DECODE
    };

    enum State
    {
        STATE_ERROR     = -1,
        STATE_INITIAL,
        STATE_ENCODING_HEADER,
        STATE_ENCODING,
        STATE_DECODING_HEADER,
        STATE_DECODING,
        STATE_COMPLETE,
    };

    State m_State;
    StreamType m_StreamType;
    netRttpHeader m_Header;
    unsigned m_NumConsumed;
    unsigned m_NumProduced;
    u8 m_HeaderBuf[netRttpHeader::BYTE_SIZEOF_HEADER];
    z_stream m_ZlibStrm;
};

//PURPOSE
//  Used to send RTTP requests and receive RTTP responses,
//  as well as receive pushed messages.
//  Can manage multiple outstanding requests simultaneously.
//
//  Pushed messages (i.e. those not triggered by a request) are
//  dispatched to the delegate passed to Init().
class netRttp
{
public:

    //Delegate type which is called when pushed messages are received.
    typedef atDelegate<void (const netRttpMessage& msg)> PushDelegate;

    netRttp();
    ~netRttp();

    //PURPOSE
    //  Initializes the RTTP object with a socket, push delegate, and
    //  keepalive timeout (0 if no keepalives should be sent).
    //  TODO: Get rid of the keepalive timeout? Only used on non-LIVE.
    //NOTES
    //  The socket MUST NOT be used with any other protocol, or to
    //  initialize another RTTP object.
    bool Init(sysMemAllocator* allocator,
              const int skt,
              PushDelegate pushDlgt,
              const unsigned keepAliveMs);

    //PURPOSE
    //  Shuts down the RTTP object and releases resources.
    void Shutdown();

    //PURPOSE
    //  Returns true if the connection is still usable. 
    //  If false, Init() must be called again before using.
    bool IsValid() const;

    //PURPOSE
    //  Must be called on a regular interval.
    void Update();

    //PURPOSE
    //  Sends a RTTP request and waits timeoutSecs for a response.
    //PARAMS
    //  content     - Contents of the request.
    //                DO NOT deallocate the content memory buffer while
    //                the request is pending.
    //  contentSize - Number of bytes in the content buffer.
    //  rqst        - The request.
    //  timeoutSecs - Number of seconds to wait for a response.
    //NOTES
    //  Do not deallocate the memory used by content or by rqst until
    //  the operation has completed.
    bool SendRequest(const void* content,
                     const unsigned contentSize,
                     netRttpRequest* rqst,
                     const unsigned timeoutSecs);

    //PURPOSE
    //  Sends a RTTP message (no response expected).
    //PARAMS
    //  content     - Contents of the request.
    //                DO NOT deallocate the content memory buffer while
    //                the request is pending.
    //  contentSize - Number of bytes in the content buffer.
    //  rqst        - The request.
    //  timeoutSecs - Number of seconds to wait for a response.
    //NOTES
    //  Do not deallocate the memory used by content or by rqst until
    //  the operation has completed.
    bool SendMsg(const void* content,
                 const unsigned contentSize,
                 netRttpRequest* rqst,
                 const unsigned timeoutSecs);

#if __DEV
    static void TestStream();
#endif

private:

    netRttp(const netRttp&);
    netRttp& operator=(const netRttp&);

    //TODO: Do we need a critical section for the queues?

    void SendKeepalive();

    void ResetSend();
    void ResetReceive();

    //Updates sending messages.
    void UpdateSending();

    //Continues sending queued requests.
    void SendRequest();
    
    //Updates receiving messages.
    void UpdateReceiving();

    void CheckForTimeouts();
    
    //Finds receiving request by TXID, optionally removing it from the list.
    netRttpRequest* GetReceivingRequest(const u16 txid, 
                                        const bool remove);

    int m_Skt;
    PushDelegate m_PushDlgt;
    unsigned m_KeepAliveMs;
    
    //List of outstanding requests.
    typedef inlist<netRttpRequest, &netRttpRequest::m_ListLink> RequestList;
    //List of requests/messages waiting to be sent.
    RequestList m_SendingRequests;
    //List of requests that have been sent and are awaiting a response.
    RequestList m_ReceivingRequests;

    //Request that's currently being sent.
    netRttpRequest* m_CurSndRqst;

    //Request that's currently receiving a response.
    netRttpRequest* m_CurRcvRqst;

    netRttpRequest m_PushedRqst;

    netTcpAsyncOp m_KeepaliveTcpOp;
    u8 m_KeepaliveBuf[netRttpHeader::BYTE_SIZEOF_HEADER];

    netTcpAsyncOp m_SendTcpOp;
    unsigned m_KeepAliveAccumMs;

    netRttpStream m_RcvStrm;
    u8 m_RcvBuf[128];   //Receives bytes off the wire.

    netRttpStream m_SndStrm;
    u8 m_SndBuf[128];

    sysMemAllocator* m_Allocator;

    netTimeStep m_TimeStep;

    bool m_Initialized : 1;
};

} //namespace rage

#endif  //NET_RTTP_H
