// 
// net/keyexchange.h 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_KEY_EXCHANGE_H
#define NET_KEY_EXCHANGE_H

#include "atl/inlist.h"
#include "connectionmanager.h"
#include "netaddress.h"
#include "peeraddress.h"
#include "status.h"
#include "system/criticalsection.h"

namespace rage
{

class netKeyExchangeSession;
class netKeyExchangeSessionOffer;
class netKeyExchangeSessionAnswer;
class netKeyExchangeEntry;

//PURPOSE
//  Key exchange/agreement.
class netKeyExchange
{
	friend class netKeyExchangeSession;
public:
    //PARAMS
	//  cxnMgr		- Connection manager used for sending and receiving packets.
    static bool Init(netConnectionManager* cxnMgr);

    static void Shutdown();

	//PURPOSE
	//  Call this on a regular interval to update pending key exchanges.
	static void Update();

	//PARAMS
	//  cxnMgr		- Connection manager used for sending and receiving packets.
	//  peerAddr	- The peer address of the remote peer with whom we're exchanging keys.
	//  addr		- Address of the remote peer with whom we're exchanging keys.
	//  relayAddr	- Relay address of the remote peer with whom we're exchanging keys.
	//				  Pass this if it's known, otherwise pass an invalid netAddress. The
	//				  key entry will track the relay address in case the remote peer
	//				  sends packets via relay server.
	//  status      - Optional.  Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the request.
	//NOTES
	//  status must remain valid for the duration of the asynchronous
	//  request.  They should not be stack variables.
	static bool ExchangeKeys(const netPeerAddress& peerAddr,
								const netAddress& addr,
								const netAddress& relayAddr,
								const bool connectingToSessionHost,
								const unsigned tunnelRequestId,
								netStatus* status);

    //PURPOSE
    //  Cancels a pending key exchange.
	//	This is not an asynchronous function. The status object
	//  is used to look up and cancel the key exchange.
	//  Pass the same status object that was passed to ExchangeKeys().
    static void CancelKeyExchange(netStatus* status);

	//PURPOSE
	//  Free key associated with the given address.
	static bool FreeKey(const netAddress& addr);
	static bool FreeKey(netKeyExchangeEntry* entry);

	//PURPOSE
	//  Given an address for an endpoint with which we have an active key,
	//  retrieve the key used to encrypt P2P traffic between the local peer
	//  and the remote peer.
	//RETURNS
	//  True on success.
	static bool GetP2pKey(const netAddress& addr, netP2pCrypt::Key& key);
	static bool GetP2pKey(const netPeerId& peerId, netP2pCrypt::Key& key);

	//PURPOSE
	//  Update an existing p2p key with new address information.
	static bool UpdateP2pKey(const netPeerId& peerId, const netAddress& newAddr, const netAddress& relayAddr);

	//PURPOSE
	//  Retrieves the relay address associated with a given peerId.
	//RETURNS
	//  True on success.
	static bool GetRelayAddr(const netPeerId& peerId, netAddress& relayAddr);

	//PURPOSE
	//  Sets the amount of time in milliseconds between message resends.
	static void SetTransactionRetryIntervalMs(const unsigned transactionRetryIntervalMs);

	//PURPOSE
	//  Sets the maximum number of retries per message.
	static void SetMaxRetriesPerTransaction(const unsigned maxRetriesPerTransaction);

	//PURPOSE
	//  Sets the maximum amount of time in milliseconds until a key exchange session is terminated.
	static void SetMaxSessionTimeMs(const unsigned maxSessionTimeMs);

	//PURPOSE
	//  If true, resend offer transactions via relay if the remote peer switched to relay.
	static void SetAllowOfferResendViaRelay(const bool allowOfferResendViaRelay);

private:
	static bool InitThreadPool();
	static void ShutdownThreadPool();
	static void OnCxnEvent(netConnectionManager* UNUSED_PARAM(cxnMgr), const netEvent* evt);
	static void OnKeyReceived(const netPeerId& peerId, const netAddress& addr, const netAddress& relayAddr, const netP2pCrypt::Key& key);
	static void UpdateSessions();
	static void FreeSession(netKeyExchangeSession* session);
	static bool OnPeerDiscovered(const netPeerId& peerId, const netAddress& addr, const netAddress& relayAddr);
	static void DeleteExpiredKeys();
	static netKeyExchangeEntry* FindMatchingEntry(const netAddress& addr);
	static void ReceiveOffer(const netKeyExchangeSessionOffer* msg, const netAddress& sender);
	static void ReceiveAnswer(const netKeyExchangeSessionAnswer* msg, const netAddress& sender);
	static netKeyExchangeSession* FindSessionById(const unsigned id);
	static unsigned GetNextSessionId();
	static netKeyExchangeSession* AllocateSession();
	static unsigned GetPeakConcurrentSessions() {return m_PeakConcurrentSessions;}
	static unsigned GetTotalSessions() {return m_TotalSessions;}

	static unsigned sm_TransactionRetryIntervalMs;
	static unsigned sm_MaxRetriesPerTransaction;
	static unsigned sm_MaxSessionTimeMs;
	static bool sm_AllowOfferResendViaRelay;

	static unsigned s_NextId;
	static sysCriticalSectionToken m_Cs;
	static sysCriticalSectionToken m_CsKeyList;	
	static netConnectionManager* m_CxnMgr;
	static netConnectionManager::Delegate m_CxnEventDelegate;
	static unsigned m_LastKeyDeletionTime;
	static bool m_Initialized;
	static unsigned m_NumConcurrentSessions;
	static unsigned m_PeakConcurrentSessions;
	static unsigned m_TotalSessions;
	static unsigned m_NumFailedSessions;
	static unsigned m_NumSuccessfulSessions;
};

} // namespace rage

#endif  // NET_KEY_EXCHANGE_H
