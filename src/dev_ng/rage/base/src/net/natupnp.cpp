//
// net/natupnp.cpp
//
// Copyright (C)  Rockstar Games.  All Rights Reserved.
//

#include "natupnp.h"
#include "upnprockstarcustom.h"

#include "diag/output.h"
#include "diag/seh.h"
#include "netdiag.h"
#include "nethardware.h"
#include "netsocket.h"
#include "packet.h"
#include "status.h"
#include "system/nelem.h"
#include "system/threadpool.h"
#include "system/timer.h"
#include "rline/ros/rlros.h"
#include "rline/rltitleid.h"

#include <time.h>
#include <limits.h>

// NET_ENABLE_UPNP is defined in upnprockstarcustom.h

#if NET_ENABLE_UPNP
// miniupnp includes
#include "miniwget.h"
#include "miniupnpc.h"
#include "upnpcommands.h"
#include "upnperrors.h"
#include "miniupnpcstrings.h"
#endif

/*
For information on:
- UPnP IGD's WANIPConnection commands: http://upnp.org/specs/gw/UPnP-gw-WANIPConnection-v2-Service.pdf
- pcp-upnp-igd-interworking function, see RFC 6970 (https://tools.ietf.org/html/rfc6970)

Our UPnP client implementation uses miniupnpc: (http://miniupnp.free.fr/)

Useful routers for testing purposes:
Routers capable of running Tomato by Shibby 1.38 or later (http://tomato.groov.pl/)
	--> uses miniupnpd 2.0 or later, which supports PCP, NAT-PMP, UPnP 1.0 and 1.1.
	--> also allows user-configuration of the UDP session timeout.
Routers capable of running DD-WRT (mainly to see its UPnP bugs and find workarounds)
	--> at least v24 of this firmware has some UPnP bugs (especially with lease duration)
		which are mitigated in the code below (search for DD-WRT below).
Apple Airport Extreme supports PCP, earlier Apple routers support NAT-PMP.
Many routers support UPnP v1.0.
*/

namespace rage
{

// available in shipped exe
NOSTRIP_PARAM(netnoupnp, "Don't use UPnP");
NOSTRIP_FINAL_LOGGING_PARAM(netupnpcleanup, "Clean up old UPnP entries before adding a port mapping");

extern const rlTitleId* g_rlTitleId;

RAGE_DEFINE_SUBCHANNEL(ragenet, natupnp)
#undef __rage_channel
#define __rage_channel ragenet_natupnp

bool netNatUpNp::sm_Initialized = false;
bool netNatUpNp::sm_CanBegin = false;							// we wait for tunables before starting
bool netNatUpNp::sm_AllowUpNpPortForwarding = true;				// if false, we don't use UPnP
bool netNatUpNp::sm_ReserveRelayMappedPort = true;				// if true, we attempt to reserve/forward the relay mapped port. If false, we reserve the game's canonical socket port.
bool netNatUpNp::sm_CleanUpOldEntries = true;					// if true, we attempt to delete old port reservations before adding our new entry.
unsigned netNatUpNp::sm_DeviceDiscoveryTimeoutMs = (4 * 1000);	// the amount of time to wait for a response to a device discovery query
unsigned netNatUpNp::sm_UpNpRefreshTimeMs = (20 * 60 * 1000);	// the amount of time in ms to wait before refreshing the UPnP port forwarding rule
unsigned netNatUpNp::sm_MaxSequentialFailedRefreshes = 3;		// the max number of refresh failures before halting refreshes
unsigned netNatUpNp::sm_UpNpRefreshStartTime = 0;				// timestamp of the last successful port mapping command, used to time port mapping refreshes
unsigned netNatUpNp::sm_UpNpLeaseDurationSec = 3600;			// port map lease duration (in seconds). UDA spec recommends 3600 seconds (1 hour).
netNatUpNpInfo::State netNatUpNp::m_DeleteState = netNatUpNpInfo::NAT_UPNP_UNATTEMPTED;
netNatUpNpInfo netNatUpNp::sm_UpNpInfo;
sysCriticalSectionToken netNatUpNp::m_Cs;

#if NET_ENABLE_UPNP
extern sysThreadPool netThreadPool;

//////////////////////////////////////////////////////////////////////////
//  uPnPWorkItem
//////////////////////////////////////////////////////////////////////////
class uPnPWorkItem : public sysThreadPool::WorkItem
{
public:

	uPnPWorkItem()
	: m_Status(NULL)
	, m_Success(false)
	, m_RefreshMapping(false)
	, m_DeleteMapping(false)
	, m_StartTime(0)
	, m_Info(NULL)
	{
		m_ControlUrl[0] = '\0';
		m_ServiceType[0] = '\0';
	}

	bool Configure(bool deleteMapping, bool refreshMapping, netNatUpNpInfo& info, netStatus* status)
	{
		m_DeleteMapping = deleteMapping;
		m_RefreshMapping = refreshMapping;
		m_Info = &info;
		m_Status = status;
		m_Success = false;
		m_StartTime = 0;

		if(m_Status)
		{
			if(m_Status->Pending())
			{
				m_Status->SetCanceled();
				m_Status->Reset();
			}

			m_Status->SetPending();
		}

		return true;
	}

	virtual void DoWork()
	{
		if(m_DeleteMapping)
		{
			DeletePortMapping();
		}
		else if(m_RefreshMapping)
		{
			RefreshPortMapping();
		}
		else
		{
			AddPortMapping();
		}
	}

	bool Succeeded() const
	{
		return m_Success;
	}

private:

	// Returns true if an IGD is found
	bool DiscoverInternetGatewayDevice(UPNPUrls& upnp_urls, IGDdatas& upnp_data, char (&lan_address)[64], netNatUpNpInfo::IgdStatus& igdStatus)
	{
		bool success = false;
		UPNPDev *upnp_dev = NULL;

		rtry
		{
			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

			sysMemSet(&upnp_urls, 0, sizeof(upnp_urls));
			sysMemSet(&upnp_data, 0, sizeof(upnp_data));
			sysMemSet(lan_address, 0, sizeof(lan_address));
			igdStatus = netNatUpNpInfo::UPNP_IGD_NOT_DETECTED;

			static const char* const deviceListV1[] =
			{
				"urn:schemas-upnp-org:device:InternetGatewayDevice:1",
				"urn:schemas-upnp-org:service:WANIPConnection:1",
				"urn:schemas-upnp-org:service:WANPPPConnection:1",
				0,
			};

			static const char* const deviceListV2[] =
			{
				"urn:schemas-upnp-org:device:InternetGatewayDevice:2",
				"urn:schemas-upnp-org:service:WANIPConnection:2",
				0,
			};

			int error = UPNPDISCOVER_SUCCESS;
			bool foundValidIgd = false;

			// first try to find v2 IGDs
			upnp_dev = upnpDiscoverDevices(deviceListV2,
											netNatUpNp::GetDeviceDiscoveryTimeoutMs(),		// time to wait in milliseconds
											nullptr,	// multicast interface (or null defaults to 239.255.255.250)
											nullptr,	// path to minissdpd socket (or null defaults to /var/run/minissdpd.sock)
											0,			// source port to use (or zero means any)
											0,			// 0==IPv4, 1==IPv6
											2,			// ttl - UDA v1.1 says: The TTL for the IP packet SHOULD default to 2 and SHOULD be configurable.
											&error,		// error condition
											1);			// search all types

			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

			/*
			*	  status:
			*     < 0 = Error
			*     0 = NO IGD found
			*     1 = A valid connected IGD has been found
			*     2 = A valid IGD has been found but it reported as not connected
			*     3 = A UPnP device has been found but was not recognized as an IGD
			*/
			int status = 0;
			if((error == UPNPDISCOVER_SUCCESS) && (upnp_dev != NULL))
			{
				status = UPNP_GetValidIGD(upnp_dev, &upnp_urls, &upnp_data, lan_address, sizeof(lan_address));
				foundValidIgd = status >= 1 && status <= 2;

				rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));
			}

			if(!foundValidIgd)
			{
				FreeUPNPUrls(&upnp_urls);
				freeUPNPDevlist(upnp_dev);
				upnp_dev = NULL;
				sysMemSet(&upnp_urls, 0, sizeof(upnp_urls));
				sysMemSet(&upnp_data, 0, sizeof(upnp_data));

				// discover v1 devices
				upnp_dev = upnpDiscoverDevices(deviceListV1,
												netNatUpNp::GetDeviceDiscoveryTimeoutMs(),		// time to wait in milliseconds
												nullptr,	// multicast interface (or null defaults to 239.255.255.250)
												nullptr,	// path to minissdpd socket (or null defaults to /var/run/minissdpd.sock)
												0,			// source port to use (or zero means any)
												0,			// 0==IPv4, 1==IPv6
												2,			// ttl - UDA v1.1 says: The TTL for the IP packet SHOULD default to 2 and SHOULD be configurable.
												&error,		// error condition
												1);			// search all types

				rcheck(error == UPNPDISCOVER_SUCCESS, catchall, netDebug2("UPnP: upnpDiscover failed (returned error code %d)", error));
				rcheck(upnp_dev != NULL, catchall, netDebug("UPnP: upnpDiscover failed (returned NULL)"));

				rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

				status = UPNP_GetValidIGD(upnp_dev, &upnp_urls, &upnp_data, lan_address, sizeof(lan_address));
			}

			if(status == 1)
			{
				igdStatus = netNatUpNpInfo::UPNP_IGD_CONNECTED;
			}
			else if(status == 2)
			{
				igdStatus = netNatUpNpInfo::UPNP_IGD_NOT_CONNECTED;
			}
			else if(status == 3)
			{
				igdStatus = netNatUpNpInfo::UPNP_IGD_NON_IGD_DETECTED;
			}
			else
			{
				igdStatus = netNatUpNpInfo::UPNP_IGD_NOT_DETECTED;
			}

			rcheck(status >= 1 && status <= 2, catchall, netDebug("UPnP: No Internet Gateway Device found."));

#if __ASSERT
			netIpAddress ip;
			if(netHardware::GetLocalIpAddress(&ip))
			{
				if(ip.IsValid())
				{
					char ipStr[netIpAddress::MAX_STRING_BUF_SIZE];
					ip.Format(ipStr);
					netAssert(strcmp(ipStr, lan_address) == 0);
				}
			}
#endif

			success = true;
		}
		rcatch(taskcancelled)
		{

		}
		rcatchall
		{

		}

		freeUPNPDevlist(upnp_dev);

		return success;
	}

	void CleanUpPortMappings(const char* controlURL, const char* servicetype, const char* description, const char* OUTPUT_ONLY(externalIp), const char* privateIp, const u16 desiredPort)
	{
		/*
			Before adding a port mapping at startup, go through all UPnP entries and delete the entry if:
			1. If description is ours, and it's mapped to our internal IP (old entry).
			2. The port the relay saw from our ping is set up to forward to a different device.
				Otherwise the port we have open towards the relay could be routed to a different internal address.
				Some routers don't check whether a port is already reserved/forwarded before assigning it to another session.
		*/

		rtry
		{
			rcheck(netNatUpNp::GetAllowCleanUpOldEntries(), tasksucceeded, netDebug("CleanUpPortMappings :: disabled via tunable."))

			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

			const unsigned MaxMappingEntries = 64;
			unsigned numMappingEntries = MaxMappingEntries;
			int error = UPNP_GetPortMappingNumberOfEntries(controlURL,
															servicetype,
															&numMappingEntries);
			if(error != UPNPCOMMAND_SUCCESS)
			{
				netError("UPnP: UPNP_GetPortMappingNumberOfEntries failed (%d)", error);

				// Some UPnP implementations don't support the GetPortMappingNumberOfEntries command.
				// if it failed, iterate through the max number of mappings.
				numMappingEntries = MaxMappingEntries;
			}

			if(numMappingEntries > MaxMappingEntries)
			{
				numMappingEntries = MaxMappingEntries;
			}

			u16 portsToDelete[MaxMappingEntries] = { 0 };
			unsigned numPortsToDelete = 0;

			netDebug("UPnP Mapping Table:");

			for(unsigned i = 0; (i < numMappingEntries) && (i < MaxMappingEntries); ++i)
			{
				char index[64] = { 0 };
				char extPort[64] = { 0 };
				char intClient[64] = { 0 };
				char intPort[64] = { 0 };
				char protocol[64] = { 0 };
				char desc[256] = { 0 };
				char enabled[64] = { 0 };
				char remoteHost[128] = { 0 };
				char duration[64] = { 0 };

				formatf(index, "%u", i);

				rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

				error = UPNP_GetGenericPortMappingEntry(controlURL,
														servicetype,
														index,
														extPort,
														intClient,
														intPort,
														protocol,
														desc,
														enabled,
														remoteHost,
														duration);

				if(error != UPNPCOMMAND_SUCCESS)
				{
					// Some UPnP implementations don't support the GetPortMappingNumberOfEntries command.
					// stop iterating on first failure
					netWarning("UPnP: UPNP_GetGenericPortMappingEntry returned %d", error);
					break;
				}
				else
				{
					OUTPUT_ONLY(bool queueForDeletion = false);
					unsigned port = 0;
					if((sscanf(extPort, "%u", &port) == 1) && (port > 0))
					{
						if(stricmp(desc, description) == 0)
						{
							if(strcmp(intClient, privateIp) == 0)
							{
								portsToDelete[numPortsToDelete++] = (u16)port;
								OUTPUT_ONLY(queueForDeletion = true);
							}
						}
						else if(port == desiredPort)
						{
							// we always attempt to delete the desired port (see below)
							OUTPUT_ONLY(queueForDeletion = true);
						}
					}

					netDebug("    %s. %s:%s -> %s:%s | %s | %s | Enabled: %s | %s seconds%s",
							index, externalIp, extPort, intClient, intPort, protocol,
							desc, enabled, duration,
							queueForDeletion ? " | Queuing for deletion - old entry" : "");
				}
			}

			// if our relay mapped port is being forwarded, we always want to delete it.
			// Note that some routers don't support the GetGenericPortMappingEntry so we always
			// attempt to delete it.
			portsToDelete[numPortsToDelete++] = desiredPort;

			for(unsigned i = 0; (i < numPortsToDelete) && (i < COUNTOF(portsToDelete)); ++i)
			{
				if(portsToDelete[i] > 0)
				{
					char szPortToDelete[64] = { 0 };
					formatf(szPortToDelete, "%u", (unsigned)portsToDelete[i]);

					rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

					error = UPNP_DeletePortMapping(controlURL,
													servicetype,
													szPortToDelete,
													"UDP",
													nullptr);

					if(error == UPNPCOMMAND_SUCCESS)
					{
						netDebug("UPnP: Deleted port mapping for external port %s", szPortToDelete);
					}
				}
			}
		}
		rcatch(taskcancelled)
		{

		}
		rcatch(tasksucceeded)
		{

		}
	}

	void AddPortMapping()
	{
		UPNPUrls upnp_urls = {0};
		IGDdatas upnp_data = {0};

		rtry
		{
			m_StartTime = sysTimer::GetSystemMsTime();
			m_Info->SetReservedPort(0);

			m_ControlUrl[0] = '\0';
			m_ServiceType[0] = '\0';

			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

			char lan_address[64] = {0};
			netNatUpNpInfo::IgdStatus igdStatus = netNatUpNpInfo::UPNP_IGD_NOT_DETECTED;
			rcheck(DiscoverInternetGatewayDevice(upnp_urls, upnp_data, lan_address, igdStatus), taskfailed, );

			m_Info->SetIgdStatus(igdStatus);
			m_Info->SetDeviceFriendlyName(upnp_data.friendlyName);
			m_Info->SetDeviceManufacturer(upnp_data.manufacturer);
			m_Info->SetDeviceModelName(upnp_data.modelName);
			m_Info->SetDeviceModelNumber(upnp_data.modelNumber);

			char szExtIpAddr[netIpAddress::MAX_STRING_BUF_SIZE] = {0};
			int error = UPNP_GetExternalIPAddress(upnp_urls.controlURL,
												  upnp_data.first.servicetype,
												  szExtIpAddr);
			if(error == UPNPCOMMAND_SUCCESS)
			{
				netIpAddress extIpAddr;
				extIpAddr.FromString(szExtIpAddr);
				m_Info->SetExternalIpAddress(extIpAddr);
			}

			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));
			
			// try to reserve the relay mapped port. This will make relayed connections more stable
			// (less chance of the NAT closing the port), and during NAT traversal, it's one of the
			// primary candidate ports we try.
			u16 requestedPort = 0;

 			if(netNatUpNp::GetReserveRelayMappedPort() && netRelay::GetMyRelayAddress().IsRelayServerAddr())
 			{
 				requestedPort = netRelay::GetMyRelayAddress().GetTargetAddress().GetPort();
 			}

			if(requestedPort == 0)
			{
				// if we're not reserving the relay mapped port, then reserve the canonical port.
				// During NAT traversal, we always try the game's canonical port as a candidate.
				requestedPort = netRelay::GetRequestedPort();
			}
			
			rcheck(requestedPort > 0, taskfailed, netError("invalid requestedPort"));

			m_Info->SetRequestedPort(requestedPort);
			
			char szRequestedPort[16] = {0};
			formatf(szRequestedPort, "%u", (unsigned)requestedPort);

			u16 locallyBoundPort = 0;
			if(netRelay::GetSocket() && (netRelay::GetSocket()->GetPort() > 0))
			{
				locallyBoundPort = netRelay::GetSocket()->GetPort();
			}
			else
			{
				locallyBoundPort = requestedPort;
			}

			rcheck(locallyBoundPort > 0, taskfailed, netError("invalid locallyBoundPort"));

			char szLocallyBoundPort[16] = {0};
			formatf(szLocallyBoundPort, "%u", (unsigned)locallyBoundPort);
		
#if RSG_PC
			const char* description = g_rlTitleId->m_RosTitleId.GetTitleDirectoryName();
#else
			const char* description = g_rlTitleId->m_RosTitleId.GetTitleName();
#endif

			CleanUpPortMappings(upnp_urls.controlURL,
								upnp_data.first.servicetype,
								description,
								szExtIpAddr,
								lan_address,
								requestedPort);

			netDebug("UPnP: Requesting UDP port mapping: external port %s to the local host (%s:%s)", szRequestedPort, lan_address, szLocallyBoundPort);

			char szLeaseDurationSec[16] = {0};
			formatf(szLeaseDurationSec, "%u", netNatUpNp::GetUpNpLeaseDurationSec());

			/*
				AddAnyPortMapping is a WANIPConnection:2 action to request any available external port mapping.
				This is useful in environments where multiple devices can be requesting the same external port mapping.
				If the requested port is already in use, the NAT will reserve a different, available port instead,
				and inform us about which port got reserved.
				
				During NAT traversal, this reserved port will be sent to the remote peer during the offer/answer exchange
				(via relay) and the remote peer will attempt to use that port as part of a candidate address.

				This is especially useful in CGN (carrier-grade NAT) environments that support the PCP-UPNP-IGD-interworking
				protocol. The UPnP command is translated into a PCP command that gets forwarded to the CGN at the network edge,
				providing a mechanism for us to reserve/forward a port through multiple layers of NATs.

				Note: the code below assumes that devices that support AddAnyPortMapping also support a limited lease duration
				(unlike the v1 AddPortMapping code below)
			*/
			char szReservedPort[64] = {0};

			error = UPNP_AddAnyPortMapping(upnp_urls.controlURL,
											upnp_data.first.servicetype,
											szRequestedPort,				// external (WAN) port requested
											szLocallyBoundPort,				// internal (LAN) port to which packets will be redirected
											lan_address,					// internal (LAN) address to which packets will be redirected
											description,					// text description to indicate why or who is responsible for the port mapping
											"UDP",							// protocol must be either TCP or UDP
											nullptr,						// remote (peer) host address or nullptr for no restriction
											szLeaseDurationSec,				// port map lease duration (in seconds). Spec recommends 3600 seconds (1 hour).
											szReservedPort);				// The port that got reserved for us.

			m_Info->SetUpNpAddAnyPortMappingError(error);

			unsigned reservedPort = 0;
			if((error == UPNPCOMMAND_SUCCESS) &&
				(szReservedPort[0] != '\0') &&
				(sscanf(szReservedPort, "%u", &reservedPort) == 1) &&
				(reservedPort > 0) &&
				(reservedPort <= USHRT_MAX))
			{
				// let's verify that the port mapping actually exists after a couple of seconds
				sysIpcSleep(2000);

				char intClient[64] = {0};
				char intPort[64] = {0};
				char desc[256] = {0};
				char enabled[64] = {0};
				char leaseDuration[64] = {0};

				int existsError = UPNP_GetSpecificPortMappingEntry(upnp_urls.controlURL,
																	upnp_data.first.servicetype,
																	szReservedPort,					// external (WAN) port requested
																	"UDP",							// protocol must be either TCP or UDP
																	nullptr,						// remote (peer) host address or nullptr for no restriction
																	intClient,
																	intPort,
																	desc,
																	enabled,
																	leaseDuration);

				if((existsError == UPNPCOMMAND_SUCCESS) && (strcmp(intClient, lan_address) == 0))
				{
					m_Info->SetReservedPort((u16)reservedPort);
					m_Info->SetReserveMethod(netNatUpNpInfo::UPNP_METHOD_ADD_ANY_PORT_MAPPING);
					netDebug2("UPnP: UPNP_AddAnyPortMapping succeeded. Reserved Port %u", m_Info->GetReservedPort());
				}
			}

			if(m_Info->GetReservedPort() == 0)
			{
				netDebug2("UPnP: UPNP_AddAnyPortMapping failed. Error code %d", m_Info->GetUpNpAddAnyPortMappingError());

				// fallback to the WANIPConnection:1 AddPortMapping action. If the requested port is already in use by a
				// different client, it fail (or possibly redirects traffic away from the client previously using the port).
				// If the requested port is already in use by the same client, it is refreshed.

				// Note: Some UPnP-enabled NATs don't support a limited lease duration. We first try a limited lease duration
				// (dynamic mapping), and if that fails, we try an unlimited lease (static mapping).

				// NOTE: some routers (including the DD-WRT v24 firmware) will fail to add a port mapping if the lease duration
				// is specified, even though it returns 'success'. In fact, it will add the mapping, then delete it about a second
				// later if the lease duration is specified. For that reason, we wait a couple of seconds and then check to
				// make sure the port mapping actually got created, and if not, fallback to unlimited lease duration.
				for(unsigned i = 0; i < 2; ++i)
				{
					rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

					bool limitLease = (i == 0);
					error = UPNP_AddPortMapping(upnp_urls.controlURL,
												upnp_data.first.servicetype,
												szRequestedPort,				// external (WAN) port requested
												szLocallyBoundPort,				// internal (LAN) port to which packets will be redirected
												lan_address,					// internal (LAN) address to which packets will be redirected
												description,					// text description to indicate why or who is responsible for the port mapping
												"UDP",							// protocol must be either TCP or UDP
												nullptr,						// remote (peer) host address or nullptr for no restriction
												limitLease ? szLeaseDurationSec : NULL);	// port map lease duration (in seconds) or NULL for "as long as possible"

					if(error == UPNPCOMMAND_SUCCESS)
					{
						// let's verify that the port mapping actually exists after a couple of seconds
						sysIpcSleep(2000);

						char intClient[64] = {0};
						char intPort[64] = {0};
						char desc[256] = {0};
						char enabled[64] = {0};
						char leaseDuration[64] = {0};

						int existsError = UPNP_GetSpecificPortMappingEntry(upnp_urls.controlURL,
																			upnp_data.first.servicetype,
																			szRequestedPort,				// external (WAN) port requested
																			"UDP",							// protocol must be either TCP or UDP
																			nullptr,						// remote (peer) host address or nullptr for no restriction
																			intClient,
																			intPort,
																			desc,
																			enabled,
																			leaseDuration);

						if((existsError == UPNPCOMMAND_SUCCESS) && (strcmp(intClient, lan_address) == 0))
						{
							m_Info->SetReservedPort((u16)requestedPort);
							m_Info->SetReserveMethod(limitLease ? netNatUpNpInfo::UPNP_METHOD_ADD_PORT_MAPPING_LIMITED_LEASE : netNatUpNpInfo::UPNP_METHOD_ADD_PORT_MAPPING_UNLIMITED_LEASE);
							break;
						}
					}
				}

				m_Info->SetUpNpAddPortMappingError(error);

				if(m_Info->GetReservedPort() > 0)
				{
					netDebug2("UPnP: UPNP_AddPortMapping succeeded. Reserved Port %u", m_Info->GetReservedPort());
				}
				else if((locallyBoundPort != requestedPort) && (locallyBoundPort == netRelay::GetRequestedPort()))
				{
					netDebug2("UPnP: UPNP_AddPortMapping failed. Error code %d", m_Info->GetUpNpAddPortMappingError());

					// some UPnP-enabled NATs only support AddPortMapping when external port == internal port.
					// fallback to forwarding netRelay::GetRequestedPort() (external) to netRelay::GetRequestedPort() (internal) (eg. 6672 to 6672).
					// During NAT traversal, we always try the 'canonical' netRelay::GetRequestedPort() as a candidate port.
					for(unsigned i = 0; i < 2; ++i)
					{
						rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

						bool limitLease = (i == 0);
						error = UPNP_AddPortMapping(upnp_urls.controlURL,
													upnp_data.first.servicetype,
													szLocallyBoundPort,				// external (WAN) port requested
													szLocallyBoundPort,				// internal (LAN) port to which packets will be redirected
													lan_address,					// internal (LAN) address to which packets will be redirected
													description,					// text description to indicate why or who is responsible for the port mapping
													"UDP",							// protocol must be either TCP or UDP
													nullptr,						// remote (peer) host address or nullptr for no restriction
													limitLease ? szLeaseDurationSec : NULL);	// port map lease duration (in seconds) or NULL for "as long as possible"

						if(error == UPNPCOMMAND_SUCCESS)
						{
							// let's verify that the port mapping actually exists after a couple of seconds
							sysIpcSleep(2000);

							char intClient[64] = {0};
							char intPort[64] = {0};
							char desc[256] = {0};
							char enabled[64] = {0};
							char leaseDuration[64] = {0};

							int existsError = UPNP_GetSpecificPortMappingEntry(upnp_urls.controlURL,
																				upnp_data.first.servicetype,
																				szLocallyBoundPort,				// external (WAN) port requested
																				"UDP",							// protocol must be either TCP or UDP
																				nullptr,						// remote (peer) host address or nullptr for no restriction
																				intClient,
																				intPort,
																				desc,
																				enabled,
																				leaseDuration);

							if((existsError == UPNPCOMMAND_SUCCESS) && (strcmp(intClient, lan_address) == 0))
							{
								m_Info->SetReservedPort((u16)locallyBoundPort);
								m_Info->SetReserveMethod(limitLease ? netNatUpNpInfo::UPNP_METHOD_ADD_PORT_MAPPING_LIMITED_LEASE_2 : netNatUpNpInfo::UPNP_METHOD_ADD_PORT_MAPPING_UNLIMITED_LEASE_2);
								break;
							}
						}
					}

					m_Info->SetUpNpAddPortMappingError(error);

					if(m_Info->GetReservedPort() > 0)
					{
						netDebug2("UPnP: UPNP_AddPortMapping succeeded. Reserved Port %u", m_Info->GetReservedPort());
					}
					else
					{
						netDebug2("UPnP: UPNP_AddPortMapping failed. Error code %d", m_Info->GetUpNpAddPortMappingError());
					}
				}
			}

			rcheck(m_Info->GetReservedPort() > 0, taskfailed, netDebug2("UPnP: Port reservation/forwarding failed"));

			rthrow(tasksucceeded, netDebug("UPnP: Port forwarded successfully."));
		}
		rcatch(taskcancelled)
		{
			// we still clean up if we're canceled (below)
			if(m_Status)
			{
				m_Status->SetCanceled();
			}
		}
		rcatch(taskfailed)
		{
			if(m_Status)
			{
				m_Status->SetFailed();
			}
		}
		rcatch(tasksucceeded)
		{
			safecpy(m_ControlUrl, upnp_urls.controlURL);
			safecpy(m_ServiceType, upnp_data.first.servicetype);

			m_Success = true;

			if(m_Status)
			{
				m_Status->SetSucceeded();
			}
		}

		if(m_Status)
		{
			if(m_Status->Pending())
			{
				m_Status->SetFailed();
			}
		}

		m_Status = NULL;

		FreeUPNPUrls(&upnp_urls);

		m_Info->SetPortReservationTimeMs(sysTimer::GetSystemMsTime() - m_StartTime);
	}

	void RefreshPortMapping()
	{
		UPNPUrls upnp_urls = {0};
		IGDdatas upnp_data = {0};

		rtry
		{
			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

			char lan_address[64] = {0};
			netNatUpNpInfo::IgdStatus igdStatus = netNatUpNpInfo::UPNP_IGD_NOT_DETECTED;
			rcheck(DiscoverInternetGatewayDevice(upnp_urls, upnp_data, lan_address, igdStatus), taskfailed, );

			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

			// request the same port that got reserved by AddPortMapping()
			u16 requestedPort = m_Info->GetReservedPort();
			rcheck(requestedPort > 0, taskfailed, netError("invalid requestedPort"));
			
			char szRequestedPort[16] = {0};
			formatf(szRequestedPort, "%u", (unsigned)requestedPort);

			u16 locallyBoundPort = 0;
			if(netRelay::GetSocket() && (netRelay::GetSocket()->GetPort() > 0))
			{
				locallyBoundPort = netRelay::GetSocket()->GetPort();
			}
			else
			{
				locallyBoundPort = requestedPort;
			}

			rcheck(locallyBoundPort > 0, taskfailed, netError("invalid locallyBoundPort"));

			char szLocallyBoundPort[16] = {0};
			formatf(szLocallyBoundPort, "%u", (unsigned)locallyBoundPort);

			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

			netDebug("UPnP: Refreshing UDP port mapping: external port %s to the local host (%s:%s)", szRequestedPort, lan_address, szLocallyBoundPort);
		
#if RSG_PC
			const char* description = g_rlTitleId->m_RosTitleId.GetTitleDirectoryName();
#else
			const char* description = g_rlTitleId->m_RosTitleId.GetTitleName();
#endif

			bool limitLease = (m_Info->GetReserveMethod() != netNatUpNpInfo::UPNP_METHOD_ADD_PORT_MAPPING_UNLIMITED_LEASE) &&
								(m_Info->GetReserveMethod() != netNatUpNpInfo::UPNP_METHOD_ADD_PORT_MAPPING_UNLIMITED_LEASE_2);

			char szLeaseDurationSec[16] = {0};
			formatf(szLeaseDurationSec, "%u", netNatUpNp::GetUpNpLeaseDurationSec());

			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));

			int error = UPNP_AddPortMapping(upnp_urls.controlURL,
											upnp_data.first.servicetype,
											szRequestedPort,				// external (WAN) port requested
											szLocallyBoundPort,				// internal (LAN) port to which packets will be redirected
											lan_address,					// internal (LAN) address to which packets will be redirected
											description,					// text description to indicate why or who is responsible for the port mapping
											"UDP",							// protocol must be either TCP or UDP
											nullptr,						// remote (peer) host address or nullptr for no restriction
											limitLease ? szLeaseDurationSec : NULL);	// port map lease duration (in seconds) or NULL for "as long as possible"

			if(error == UPNPCOMMAND_SUCCESS)
			{
				rthrow(tasksucceeded, netDebug("UPnP: Port mapping refreshed successfully."));
			}
			else
			{
				rthrow(taskfailed, netDebug("UPnP: Failed to refresh port mapping."));
			}
		}
		rcatch(taskcancelled)
		{
			// we still clean up if we're canceled (below)
			if(m_Status)
			{
				m_Status->SetCanceled();
			}
		}
		rcatch(taskfailed)
		{
			u16 numFailed = m_Info->GetNumFailedRefreshes();
			m_Info->SetNumFailedRefreshes(numFailed + 1);

			numFailed = m_Info->GetNumSequentialFailedRefreshes();
			m_Info->SetNumSequentialFailedRefreshes(numFailed + 1);

			if(m_Status)
			{
				m_Status->SetFailed();
			}
		}
		rcatch(tasksucceeded)
		{
			safecpy(m_ControlUrl, upnp_urls.controlURL);
			safecpy(m_ServiceType, upnp_data.first.servicetype);

			u16 numSuccessful = m_Info->GetNumSuccessfulRefreshes();
			m_Info->SetNumSuccessfulRefreshes(numSuccessful + 1);
			m_Info->SetNumSequentialFailedRefreshes(0);

			m_Success = true;

			if(m_Status)
			{
				m_Status->SetSucceeded();
			}
		}

		if(m_Status)
		{
			if(m_Status->Pending())
			{
				m_Status->SetFailed();
			}
		}

		m_Status = NULL;

		FreeUPNPUrls(&upnp_urls);
	}

	void DeletePortMapping()
	{
		UPNPUrls upnp_urls = {0};

		rtry
		{
			rcheck(!WasCanceled(), taskcancelled, netDebug("UPnPWorkItem was cancelled. Bailing."));
			rverify(m_ControlUrl[0] != '\0', taskfailed, netDebug("m_ControlUrl is invalid. Bailing."));
			rverify(m_ServiceType[0] != '\0', taskfailed, netDebug("m_ServiceType is invalid. Bailing."));

			// Note: we're skipping the IGD discovery process to speed up game shutdown. The control
			// url and service type come from adding/refreshing the port mapping. The assumption is
			// that the IGD does not change location (url) while the game is running.
			u16 portsToDelete[] = {m_Info->GetReservedPort()};
			for(unsigned i = 0; i < COUNTOF(portsToDelete); ++i)
			{
				if(portsToDelete[i] > 0)
				{
					char szPortToDelete[64] = {0};
					formatf(szPortToDelete, "%u", (unsigned)portsToDelete[i]);

					int error = UPNP_DeletePortMapping(m_ControlUrl,
														m_ServiceType,
														szPortToDelete,
														"UDP",
														nullptr);

					if(error == UPNPCOMMAND_SUCCESS)
					{
						netDebug("UPnP: Deleted port mapping for external port %s", szPortToDelete);
					}
				}
			}

			m_Info->SetReservedPort(0);

			rthrow(tasksucceeded, );
		}
		rcatch(taskcancelled)
		{
			// we still clean up if we're canceled (below)
			if(m_Status)
			{
				m_Status->SetCanceled();
			}
		}
		rcatch(taskfailed)
		{
			if(m_Status)
			{
				m_Status->SetFailed();
			}
		}
		rcatch(tasksucceeded)
		{
			m_Success = true;

			if(m_Status)
			{
				m_Status->SetSucceeded();
			}
		}

		if(m_Status)
		{
			if(m_Status->Pending())
			{
				m_Status->SetFailed();
			}
		}

		m_Status = NULL;

		FreeUPNPUrls(&upnp_urls);
	}

	netStatus* m_Status;
	bool m_Success;
	bool m_RefreshMapping;
	bool m_DeleteMapping;
	unsigned m_StartTime;
	netNatUpNpInfo* m_Info;
	char m_ControlUrl[MINIUPNPC_URL_MAXSIZE];
	char m_ServiceType[MINIUPNPC_URL_MAXSIZE];
};

uPnPWorkItem s_uPnPWorkItem;

#endif // NET_ENABLE_UPNP

//////////////////////////////////////////////////////////////////////////
//  netNatUpNp
//////////////////////////////////////////////////////////////////////////

bool
netNatUpNp::Init()
{
	SYS_CS_SYNC(m_Cs);

	sm_Initialized = true;
	return true;
}

void
netNatUpNp::Shutdown()
{
	SYS_CS_SYNC(m_Cs);

	sm_Initialized = false;
}

bool 
netNatUpNp::IsInitialized()
{
	return sm_Initialized;
}

bool 
netNatUpNp::IsInitialWorkCompleted()
{
#if NET_ENABLE_UPNP
	netNatUpNpInfo::State state = sm_UpNpInfo.GetState();
	bool completed = (state == netNatUpNpInfo::NAT_UPNP_SUCCEEDED) || (state == netNatUpNpInfo::NAT_UPNP_FAILED) || (sm_UpNpInfo.GetReservedPort() > 0);
	bool blocked = PARAM_netnoupnp.Get() || (sm_CanBegin && !sm_AllowUpNpPortForwarding);
	return completed || blocked;
#else
	return true;
#endif
}

void
netNatUpNp::Update()
{
#if NET_ENABLE_UPNP
	bool uPnPallowed = sm_Initialized && sm_CanBegin && sm_AllowUpNpPortForwarding && !PARAM_netnoupnp.Get();

	if(!uPnPallowed)
	{
		return;
	}

	bool isAnyOnline = rlRos::IsAnyOnline();
	static bool deletePortMapping = false;
	deletePortMapping = deletePortMapping || ((!isAnyOnline || !netRelay::GetMyRelayAddress().IsRelayServerAddr()) && (sm_UpNpInfo.GetReservedPort() > 0));

	if(deletePortMapping)
	{
		sm_UpNpInfo.SetState(netNatUpNpInfo::NAT_UPNP_UNATTEMPTED);
		sm_UpNpRefreshStartTime = 0;

		switch(m_DeleteState)
		{
		case rage::netNatUpNpInfo::NAT_UPNP_UNATTEMPTED:
			if(s_uPnPWorkItem.Pending())
			{
				netThreadPool.CancelWork(s_uPnPWorkItem.GetId());
			}
			else
			{
				bool configured = s_uPnPWorkItem.Configure(true, false, sm_UpNpInfo, NULL);
				if(configured && netThreadPool.QueueWork(&s_uPnPWorkItem))
				{
					m_DeleteState = netNatUpNpInfo::NAT_UPNP_IN_PROGRESS;
				}
				else
				{
					netError("Failed to queue UPnP work item");
					deletePortMapping = false;
					m_DeleteState = netNatUpNpInfo::NAT_UPNP_UNATTEMPTED;
				}
			}
			break;
		case rage::netNatUpNpInfo::NAT_UPNP_IN_PROGRESS:
			if(s_uPnPWorkItem.Finished() || s_uPnPWorkItem.WasCanceled())
			{
				sm_UpNpInfo.Clear();
				deletePortMapping = false;
				m_DeleteState = netNatUpNpInfo::NAT_UPNP_UNATTEMPTED;
			}
			break;
		default:
			break;
		}

		return;
	}

	switch(sm_UpNpInfo.GetState())
	{
	case netNatUpNpInfo::NAT_UPNP_UNATTEMPTED:
		// wait until we have our mapped address from the relay
		if(netRelay::GetMyRelayAddress().IsRelayServerAddr() && !s_uPnPWorkItem.Pending())
		{
			bool refreshPortForwardRule = sm_UpNpRefreshStartTime > 0;
			bool deletePortForwardRule = false;
			
			bool configured = s_uPnPWorkItem.Configure(deletePortForwardRule, refreshPortForwardRule, sm_UpNpInfo, NULL);
			if(configured && netThreadPool.QueueWork(&s_uPnPWorkItem))
			{
				sm_UpNpInfo.SetState(netNatUpNpInfo::NAT_UPNP_IN_PROGRESS);
			}
			else
			{
				netError("Failed to queue UPnP work item");
				sm_UpNpInfo.SetState(netNatUpNpInfo::NAT_UPNP_FAILED);
			}
		}
		break;
	case netNatUpNpInfo::NAT_UPNP_IN_PROGRESS:
		if(s_uPnPWorkItem.Finished() || s_uPnPWorkItem.WasCanceled())
		{
			// if we fail to refresh, then we set to succeeded and try again later
			if(s_uPnPWorkItem.Succeeded() || (sm_UpNpRefreshStartTime > 0))
			{
				sm_UpNpInfo.SetState(netNatUpNpInfo::NAT_UPNP_SUCCEEDED);
				sm_UpNpRefreshStartTime = sysTimer::GetSystemMsTime() | 1;
			}
			else
			{
				sm_UpNpInfo.SetState(netNatUpNpInfo::NAT_UPNP_FAILED);
			}
		}
		break;
	case netNatUpNpInfo::NAT_UPNP_SUCCEEDED:
		// We need to refresh the lease periodically.
		if((sm_UpNpRefreshTimeMs > 0) && (sm_UpNpRefreshStartTime > 0))
		{
			if(sysTimer::HasElapsedIntervalMs(sm_UpNpRefreshStartTime, sm_UpNpRefreshTimeMs))
			{
				if(sm_UpNpInfo.GetNumSequentialFailedRefreshes() < sm_MaxSequentialFailedRefreshes)
				{
					sm_UpNpInfo.SetState(netNatUpNpInfo::NAT_UPNP_UNATTEMPTED);
				}
				else
				{
					sm_UpNpInfo.SetState(netNatUpNpInfo::NAT_UPNP_FAILED);
				}
			}
		}
		break;
	case netNatUpNpInfo::NAT_UPNP_FAILED:
	case netNatUpNpInfo::NAT_UPNP_NUM_STATES:
		break;
	}
#endif
}

void
netNatUpNp::DeleteUpNpPortForwardRule(netStatus* status)
{
	// note: this should only be called at shutdown

#if NET_ENABLE_UPNP
	// set state to failed so the main update loop doesn't
	// start a refresh during/after we've deleted the mapping
	sm_UpNpInfo.SetState(netNatUpNpInfo::NAT_UPNP_FAILED);

	u32 waitStartTime = sysTimer::GetSystemMsTime();
	while(s_uPnPWorkItem.Pending())
	{
		netThreadPool.CancelWork(s_uPnPWorkItem.GetId());

		if((sysTimer::GetSystemMsTime() - waitStartTime) > 10000)
		{
			if(status && !status->Pending())
			{
				status->ForceFailed();
			}
			return;
		}
	}

	if(sm_UpNpInfo.GetReservedPort() > 0)
	{
		bool configured = s_uPnPWorkItem.Configure(true, false, sm_UpNpInfo, status);
		if(configured)
		{
			netThreadPool.QueueWork(&s_uPnPWorkItem);
		}
	}
#endif

	// if we're not pending by this point, then we're done
	if(status && !status->Pending())
	{
		status->ForceSucceeded();
	}
}

void 
netNatUpNp::SetAllowUpNpPortForwarding(const bool allowUpNpPortForwarding)
{
	if(sm_AllowUpNpPortForwarding != allowUpNpPortForwarding)
	{
		// once any system disables address resservation, don't allow it to be re-enabled by another system
		if(allowUpNpPortForwarding == false)
		{
			netDebug("SetAllowUpNpPortForwarding :: %s", allowUpNpPortForwarding ? "true" : "false");
			sm_AllowUpNpPortForwarding = allowUpNpPortForwarding;
		}
	}
}

void
netNatUpNp::SetReserveRelayMappedPort(const bool reserveRelayMappedPort)
{
	if(sm_ReserveRelayMappedPort != reserveRelayMappedPort)
	{
		netDebug("SetReserveRelayMappedPort :: %s", reserveRelayMappedPort ? "true" : "false");
		sm_ReserveRelayMappedPort = reserveRelayMappedPort;
	}
}

void
netNatUpNp::SetAllowCleanUpOldEntries(const bool allowCleanUpOldEntries)
{
	if(sm_CleanUpOldEntries != allowCleanUpOldEntries)
	{
		netDebug("SetAllowCleanUpOldEntries :: %s", allowCleanUpOldEntries ? "true" : "false");
		sm_CleanUpOldEntries = allowCleanUpOldEntries;
	}
}

bool
netNatUpNp::GetReserveRelayMappedPort()
{
	return sm_ReserveRelayMappedPort;
}

void 
netNatUpNp::SetUpNpPortRefreshTimeMs(const unsigned uPnPrefreshTimeMs)
{
	if(sm_UpNpRefreshTimeMs != uPnPrefreshTimeMs)
	{
		netDebug("SetUpNpPortRefreshTimeMs :: %u", uPnPrefreshTimeMs);
		sm_UpNpRefreshTimeMs = uPnPrefreshTimeMs;
	}
}

void netNatUpNp::SetTunablesReceived()
{
	sm_CanBegin = true;
}

unsigned
netNatUpNp::GetUpNpLeaseDurationSec()
{
	return sm_UpNpLeaseDurationSec;
}

unsigned
netNatUpNp::GetDeviceDiscoveryTimeoutMs()
{
	return sm_DeviceDiscoveryTimeoutMs;
}

bool
netNatUpNp::GetAllowCleanUpOldEntries()
{
#if !__NO_OUTPUT
	// the command line can override the tunable in output builds.
	if(PARAM_netupnpcleanup.Get())
	{
		return true;
	}
#endif

	// use the tunable
	return sm_CleanUpOldEntries;
}

#if !__NO_OUTPUT
void 
netNatUpNpInfo::DumpInfo() const
{
	bool rfc1918 = m_ExternalIpAddress.IsValid() && m_ExternalIpAddress.IsV4() && m_ExternalIpAddress.ToV4().IsRfc1918();
	bool altReservation = ((m_State == NAT_UPNP_SUCCEEDED) && (m_ReservedPort != m_RequestedPort));

	netDebug("         UPnP: %s", (m_State == NAT_UPNP_SUCCEEDED) ? "Enabled, Port Forwarded" : ((m_State == NAT_UPNP_FAILED) ? "Disabled/Failed" : "Not Performed"));
	netDebug("             IGD Status: %s", GetIgdStatusString(m_IgdStatus));
	netDebug("             Device Friendly Name: %s", m_FriendlyName);
	netDebug("             Device Manufacturer: %s", m_Manufacturer);
	netDebug("             Device Model Name: %s", m_ModelName);
	netDebug("             Device Model Number: %s", m_ModelNumber);
	netDebug("             External IP: %s%s", m_ExternalIpAddress.ToString(), rfc1918 ? " (rfc1918) - multiple levels of NAT detected" : "");
	netDebug("             Requested Port: %u", m_RequestedPort);
	netDebug("             Reserved Port: %u%s", m_ReservedPort, altReservation ? " (different than requested)" : "");
	netDebug("             Reserve Method: %s", GetReserveMethodString(m_ReserveMethod));
	netDebug("             AddAnyPortMapping result: %d", m_AddAnyPortMappingError);
	netDebug("             AddPortMapping result: %d", m_AddPortMappingError);
	netDebug("             Elapsed Time: %u ms", m_PortReservationTimeMs);
	netDebug("             Num Successful Refreshes: %u", m_NumSuccessfulRefreshes);
	netDebug("             Num Failed Refreshes: %u", m_NumFailedRefreshes);
}
#endif

bool
netNatUpNpInfo::Import(const void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/)
{
	bool success = false;

    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

	success = bb.SerUns(m_State, datBitsNeeded<NAT_UPNP_NUM_STATES>::COUNT)
				&& bb.SerUns(m_RequestedPort, sizeof(m_RequestedPort) << 3)
				&& bb.SerUns(m_ReservedPort, sizeof(m_ReservedPort) << 3)
				&& bb.SerInt(m_AddAnyPortMappingError, sizeof(m_AddAnyPortMappingError) << 3)
				&& bb.SerInt(m_AddPortMappingError, sizeof(m_AddPortMappingError) << 3)
				&& bb.SerUns(m_ReserveMethod, datBitsNeeded<UPNP_NUM_METHODS>::COUNT)
				&& bb.SerUns(m_PortReservationTimeMs, sizeof(m_PortReservationTimeMs) << 3)
				&& bb.SerUns(m_IgdStatus, datBitsNeeded<UPNP_IGD_NUM_ENUMS>::COUNT)
				&& bb.SerStr(m_FriendlyName, sizeof(m_FriendlyName))
				&& bb.SerStr(m_Manufacturer, sizeof(m_Manufacturer))
				&& bb.SerStr(m_ModelName, sizeof(m_ModelName))
				&& bb.SerStr(m_ModelNumber, sizeof(m_ModelNumber))
				&& bb.SerUser(m_ExternalIpAddress)
				&& bb.SerUns(m_NumSuccessfulRefreshes, sizeof(m_NumSuccessfulRefreshes) << 3)
				&& bb.SerUns(m_NumFailedRefreshes, sizeof(m_NumFailedRefreshes) << 3)
				&& bb.SerUns(m_NumSequentialFailedRefreshes, sizeof(m_NumSequentialFailedRefreshes) << 3);

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}
    return success;
}

bool
netNatUpNpInfo::Export( void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/ ) const
{
    bool success = false;

    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);
	
	success = bb.SerUns(m_State, datBitsNeeded<NAT_UPNP_NUM_STATES>::COUNT)
				&& bb.SerUns(m_RequestedPort, sizeof(m_RequestedPort) << 3)
				&& bb.SerUns(m_ReservedPort, sizeof(m_ReservedPort) << 3)
				&& bb.SerInt(m_AddAnyPortMappingError, sizeof(m_AddAnyPortMappingError) << 3)
				&& bb.SerInt(m_AddPortMappingError, sizeof(m_AddPortMappingError) << 3)
				&& bb.SerUns(m_ReserveMethod, datBitsNeeded<UPNP_NUM_METHODS>::COUNT)
				&& bb.SerUns(m_PortReservationTimeMs, sizeof(m_PortReservationTimeMs) << 3)
				&& bb.SerUns(m_IgdStatus, datBitsNeeded<UPNP_IGD_NUM_ENUMS>::COUNT)
				&& bb.SerStr(m_FriendlyName, sizeof(m_FriendlyName))
				&& bb.SerStr(m_Manufacturer, sizeof(m_Manufacturer))
				&& bb.SerStr(m_ModelName, sizeof(m_ModelName))
				&& bb.SerStr(m_ModelNumber, sizeof(m_ModelNumber))
				&& bb.SerUser(m_ExternalIpAddress)
				&& bb.SerUns(m_NumSuccessfulRefreshes, sizeof(m_NumSuccessfulRefreshes) << 3)
				&& bb.SerUns(m_NumFailedRefreshes, sizeof(m_NumFailedRefreshes) << 3)
				&& bb.SerUns(m_NumSequentialFailedRefreshes, sizeof(m_NumSequentialFailedRefreshes) << 3);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}
    return success;
}

} // namespace rage
