// 
// net/crypto.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "crypto.h"
#include "system/memory.h"
#include "system/timer.h"
#include "system/nelem.h"
#include "diag/seh.h"
#include "net/netdiag.h"
#include "profile/rocky.h"
#include "system/alloca.h"
#include "system/criticalsection.h"
#include "system/new.h"
#include "system/xtl.h"

#include "wolfssl/wolfcrypt/asn.h"
#include "wolfssl/wolfcrypt/ecc.h"
#include "wolfssl/wolfcrypt/rsa.h"
#include <time.h>

#if USE_NET_ASSERTS
#include <wolfssl/check_sizes.h>
#include <wolfssl/ssl.h>
#include <wolfssl/internal.h>
#include <wolfssl/wolfcrypt/aes.h>
#include <wolfssl/wolfcrypt/asn.h>
#include <wolfssl/wolfcrypt/dh.h>
#include <wolfssl/wolfcrypt/md5.h>
#include <wolfssl/wolfcrypt/rsa.h>
#include <wolfssl/wolfcrypt/sha.h>
#endif

#if NET_CRYPTO_USE_BCRYPT
#include <bcrypt.h>
#pragma comment(lib, "bcrypt.lib") 
#else
#include "wolfssl/wolfcrypt/aes.h"
#include "wolfssl/wolfcrypt/dh.h"
#include "wolfssl/wolfcrypt/hmac.h"
#include "wolfssl/wolfcrypt/sha256.h"
#include "wolfssl/wolfcrypt/random.h"
#endif

using namespace rage;

///////////////////////////////////////////////////////////////////////////////
//  netCrypto
///////////////////////////////////////////////////////////////////////////////
#if NET_CRYPTO_USE_BCRYPT
#define NT_SUCCESS(Status) (((NTSTATUS)(Status)) >= 0)
static BCRYPT_ALG_HANDLE s_RngAlgo = NULL;
#else
static sysCriticalSectionToken s_CsRng;
static WC_RNG s_WolfSslRng;
#endif

netRandom netCrypto::sm_WeakRng;
bool netCrypto::sm_Initialized = false;

static void FreeRngInternal(WC_RNG* rng)
{
	wc_FreeRng(rng);
}

bool
netCrypto::Init()
{
	rtry
	{
		rcheck(!sm_Initialized, noErr, );
		NET_ASSERTS_ONLY(CheckWolfSslSizes());

#if NET_CRYPTO_USE_BCRYPT
		NTSTATUS status = BCryptOpenAlgorithmProvider(&s_RngAlgo,
														BCRYPT_RNG_ALGORITHM,
														NULL,
														0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptOpenAlgorithmProvider(BCRYPT_RNG_ALGORITHM)", status));
#else
		{
			SYS_CS_SYNC(s_CsRng);
			int ret = wc_InitRng(&s_WolfSslRng);
			rverify(ret == 0, catchall, netError("Error %d returned by InitRng", ret));
		}
#endif

		rverify(netP2pCrypt::Init(), catchall, netError("Error initializing netP2pCrypt"));

		sm_Initialized = true;
		
		sm_WeakRng.SetRandomSeed();

		return true;
	}
	rcatch(noErr)
	{
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
netCrypto::Shutdown()
{
#if NET_CRYPTO_USE_BCRYPT
	if(s_RngAlgo)
	{
		BCryptCloseAlgorithmProvider(s_RngAlgo, 0);
		s_RngAlgo = NULL;
	}
#else
	{
		SYS_CS_SYNC(s_CsRng);
		FreeRngInternal(&s_WolfSslRng);
	}
#endif

	netP2pCrypt::Shutdown();
	sm_Initialized = false;
}

#if USE_NET_ASSERTS
void netCrypto::CheckWolfSslSizes()
{
#define SSL_CHECK_SIZE(localSize, libSize) rverify(localSize == libSize, catchall, netError(#localSize ": %" SIZETFMT "u. Expected: %" SIZETFMT "u", localSize, libSize))
	rtry
	{
		SSL_CHECK_SIZE(sizeof(RsaKey), GetSizeOfRsaKey());
		SSL_CHECK_SIZE(sizeof(Aes), GetSizeOfAes());
		SSL_CHECK_SIZE(sizeof(WC_RNG), GetSizeOfRng());
		SSL_CHECK_SIZE(sizeof(wc_Md5), GetSizeOfMd5());
		SSL_CHECK_SIZE(sizeof(wc_Sha), GetSizeOfSha());
		SSL_CHECK_SIZE(sizeof(wc_Sha256), GetSizeOfSha256());
		SSL_CHECK_SIZE(sizeof(DhKey), GetSizeOfDhKey());
		SSL_CHECK_SIZE(sizeof(WOLFSSL_CTX), GetSizeOfSslCtx());
		SSL_CHECK_SIZE(sizeof(WOLFSSL), GetSizeOfSsl());
		SSL_CHECK_SIZE(sizeof(WOLFSSL_X509), GetSizeOfX509());
	}
	rcatchall
	{
	}
#undef SSL_CHECK_SIZE
}
#endif

bool 
netCrypto::GenerateRandomBytes(u8* outBytes, const unsigned numBytes)
{
	PROFILE

	if(!sm_Initialized)
	{
		Init();
	}

	rtry
	{
		rverify(sm_Initialized, catchall, );

#if NET_CRYPTO_USE_BCRYPT
		NTSTATUS status = BCryptGenRandom(s_RngAlgo, outBytes, numBytes, 0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGenRandom()", status));
#else
		const int MAX_RNG_REINIT_ATTEMPTS = 5;
		unsigned numAttempts = 0;
		int ret = 0;
		
		SYS_CS_SYNC(s_CsRng);

		do 
		{
			ret = wc_RNG_GenerateBlock(&s_WolfSslRng, outBytes, numBytes);
			
			if(!netVerifyf(ret == 0, "Error %d returned by RNG_GenerateBlock. Will attempt to reinitialize RNG.", ret))
			{
				FreeRngInternal(&s_WolfSslRng);
				const int initRet = wc_InitRng(&s_WolfSslRng);
				rverify(initRet == 0, catchall, netError("GenerateRandomBytes :: Error %d returned by InitRng", initRet));
			}

		} while((ret != 0) && (++numAttempts < MAX_RNG_REINIT_ATTEMPTS));

		rverify(ret == 0, catchall, netError("GenerateRandomBytes failed after %u attempts", numAttempts));
#endif

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void 
netCrypto::GenerateRandomBytesWeak(u8* outBytes, const unsigned numBytes)
{
	if(!sm_Initialized)
	{
		Init();
	}

	for(unsigned i = 0; i < numBytes; ++i)
	{        
		outBytes[i] = (u8)sm_WeakRng.GetRanged(0, 255);
	}
}

#if !__NO_OUTPUT
char* netCrypto::AsnDateTimeToString(const u8* asnDateTime, char* out, unsigned outLen)
{
	rtry
	{
		rverifyall(asnDateTime);
		rverifyall(out);
		out[0] = '\0';
		const u8 format = asnDateTime[0];
		const u8 length = asnDateTime[1];
		tm time;
		rverifyall(wc_GetDateAsCalendarTime(&asnDateTime[2], length, format, &time) == 0);
		strftime(out, outLen - 1, "%B %d, %Y %H:%M:%S", &time);
	}
	rcatchall
	{

	}

	return out;
}
#endif

///////////////////////////////////////////////////////////////////////////////
//  Rc4Context
///////////////////////////////////////////////////////////////////////////////
Rc4Context::Rc4Context()
{
    Clear();
}

bool 
Rc4Context::IsValid() const
{
    return m_IsValid;
}

void 
Rc4Context::Clear()
{
    m_IsValid = false;
}

void 
Rc4Context::Reset(const u8* key, const unsigned size)
{
    netAssert(key && size);

    Clear();

	m_X = 0;
	m_Y = 0;

	for(unsigned i = 0; i < sizeof(m_State); i++)
    {
		m_State[i] = (u8)i;
    }
	
    u8 j = 0;

	for(unsigned i = 0; i < sizeof(m_State); i++)
	{
        j = (u8)(j + m_State[i] + key[i % size]);

        u8 temp = m_State[i];
        m_State[i] = m_State[j];
        m_State[j] = temp;
	}

    m_IsValid = true;
}

void 
Rc4Context::Encrypt(u8* buf, const unsigned size)
{
    netAssert(buf && size);
    netAssert(IsValid());

	for(unsigned i = 0; i < size; i++)
	{
		m_X = (u8)(m_X + 1);
		m_Y = (u8)(m_Y + m_State[m_X]);

        u8 temp = m_State[m_X];
        m_State[m_X] = m_State[m_Y];
        m_State[m_Y] = temp;
		
        buf[i] ^= m_State[(u8)(m_State[m_X] + m_State[m_Y])];
	}
}

//PURPOSE
//  A helper for dealing with wolfcrypt Rsa keys.
class netRsaKey
{
public:
    netRsaKey(const u8* key,
              const unsigned keyLen, 
			  bool isPublic = true)
    {
		NET_ASSERTS_ONLY(int ret =) wc_InitRsaKey(&m_Key, NULL);
		netAssertf(ret == 0, "Error %d returned by wc_InitRsaKey", ret);

        unsigned idx = 0;
		if(isPublic)
		{
			NET_ASSERTS_ONLY(ret =) wc_RsaPublicKeyDecode(key, &idx, &m_Key, keyLen);
			netAssertf(ret == 0, "Error %d returned by wc_RsaPublicKeyDecode", ret);
		}
		else
		{
			NET_ASSERTS_ONLY(ret =) wc_RsaPrivateKeyDecode(key, &idx, &m_Key, keyLen);
			netAssertf(ret == 0, "Error %d returned by wc_RsaPrivateKeyDecode", ret);
		}
    }

    ~netRsaKey()
    {
        wc_FreeRsaKey(&m_Key);
    }

    bool IsValid()
    {
        return m_Key.type == RSA_PUBLIC || m_Key.type == RSA_PRIVATE;
    }

    bool IsPrivate()
    {
        return m_Key.type == RSA_PRIVATE;
    }

    operator RsaKey*()
    {
        return &m_Key;
    }

private:
    RsaKey m_Key;
};

bool 
Rsa::Encrypt(const u8* key, 
             const unsigned keyLen, 
             const u8* const inBuf,
             const unsigned inBufLen,
             u8* outBuf,
             const unsigned outBufSize,
             unsigned* outLen)
{
	PROFILE

    rtry
    {
        netRsaKey decodedKey(key, keyLen);

        rverify(decodedKey.IsValid(), catchall, );

        WC_RNG rng;
        wc_InitRng(&rng);

        int numWritten = wc_RsaPublicEncrypt(inBuf, inBufLen, outBuf, outBufSize, decodedKey, &rng);
        rverify(numWritten >= 0, catchall, );
        *outLen = (unsigned)numWritten;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
Rsa::Decrypt(const u8* key,
             const unsigned keyLen,
             u8* inBuf,
             const unsigned inBufLen,
             u8** outBuf,
             unsigned *outLen)
{
	PROFILE

    rtry
    {
        netRsaKey decodedKey(key, keyLen);

        rverify(decodedKey.IsValid(), catchall, );
        rverify(decodedKey.IsPrivate(), catchall, );

        int numWritten = wc_RsaPrivateDecryptInline(inBuf, inBufLen, outBuf, decodedKey);
        rverify(numWritten >= 0, catchall, );
        *outLen = (unsigned)numWritten;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
Rsa::PrivateEncrypt(const u8* key, 
                    const unsigned keyLen, 
                    const u8* const inBuf,
                    const unsigned inBufLen,
                    u8* outBuf,
                    const unsigned outBufSize,
                    unsigned* outLen)
{
	PROFILE

    rtry
    {
        netRsaKey decodedKey(key, keyLen, false);

        rverify(decodedKey.IsValid(), catchall, );
        rverify(decodedKey.IsPrivate(), catchall, );

        WC_RNG rng;
        wc_InitRng(&rng);

        int numWritten = wc_RsaSSL_Sign(inBuf, inBufLen, outBuf, outBufSize, decodedKey, &rng);
        rverify(numWritten >= 0, catchall, );
        *outLen = (unsigned)numWritten;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
Rsa::PublicDecrypt(const u8* key,
                   const unsigned keyLen,
                   u8* inBuf,
                   const unsigned inBufLen,
                   u8** outBuf,
                   unsigned *outLen)
{
	PROFILE

    rtry
    {
        netRsaKey decodedKey(key, keyLen);

        rverify(decodedKey.IsValid(), catchall, );

        int numWritten = wc_RsaSSL_VerifyInline(inBuf, inBufLen, outBuf, decodedKey);
        rverify(numWritten >= 0, catchall, );
        *outLen = (unsigned)(numWritten);

        return true;
    }
    rcatchall
    {
        return false;
    }
}
    
bool
Rsa::SignSha1(const u8* key,
              const unsigned keyLen,
              const u8 (&hash)[Sha1::SHA1_DIGEST_LENGTH],
              u8* outBuf,
              const unsigned outBufSize,
              unsigned* outLen)
{
	PROFILE

    rtry
    {
        netRsaKey decodedKey(key, keyLen);

        rverify(decodedKey.IsValid(), catchall, );
        rverify(decodedKey.IsPrivate(), catchall, );

        u8 encodedSig[MAX_ENCODED_SIG_SZ];
        word32 encodedSigLen = wc_EncodeSignature(encodedSig, hash, NELEM(hash), SHAh);

        WC_RNG rng;
        wc_InitRng(&rng);

        int numWritten = wc_RsaSSL_Sign(encodedSig, encodedSigLen, outBuf, outBufSize, decodedKey, &rng);
        rverify(numWritten >= 0, catchall, );
        *outLen = (unsigned)numWritten;

        return true;
    }
    rcatchall
    {
        return false;
    }
}


bool
Rsa::VerifySha1(const u8* key,
                const unsigned keyLen,
                u8* inBuf,
                const unsigned inBufLen,
                const u8 (&hash)[Sha1::SHA1_DIGEST_LENGTH])
{
	PROFILE

    rtry
    {
        netRsaKey decodedKey(key, keyLen);

        rverify(decodedKey.IsValid(), catchall, );

        u8* plainSig;
		//@@: location RSA_VERIFYSHA1_INLINEVERIFY
        int plainSigSize = wc_RsaSSL_VerifyInline(inBuf, inBufLen, &plainSig, decodedKey);

        u8 encodedSig[MAX_ENCODED_SIG_SZ];
        word32 encodedSigSize = wc_EncodeSignature(encodedSig, hash, NELEM(hash), SHAh);

		//@@: location RSA_VERIFYSHA1

        rverify(plainSigSize >= 0, catchall, );
        rverify(plainSigSize == (int)encodedSigSize, catchall, );
		//@@: location RSA_VERIFYSHA1_MEMCMP
        rcheck(memcmp(plainSig, encodedSig, plainSigSize) == 0, catchall, );

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
EcDsa::SignSha256(ecc_key* key,
				  const u8* data,
				  const unsigned dataSize,
				  u8* outSignature,
				  unsigned& signatureSize)
{
	bool success = false;

	mp_int r;
	mp_int s;
	WC_RNG rng;
	wc_Sha256 sha256;

	rtry
	{
		rverifyall(key);
		rverifyall(data && (dataSize > 0));
		rverifyall(outSignature);

		const unsigned eccKeySz = key->dp->size;
		rverifyall(signatureSize >= (eccKeySz * 2));

		// calculate SHA-256 digest of data
		unsigned char digest[WC_SHA256_DIGEST_SIZE];
		int ret = wc_InitSha256(&sha256);
		rverifyall(ret == 0);
		ret = wc_Sha256Update(&sha256, data, dataSize);
		rverifyall(ret == 0);
		ret = wc_Sha256Final(&sha256, digest);
		rverifyall(ret == 0);

		// sign the hash
		ret = wc_InitRng(&rng);
		rverifyall(ret == 0);

		ret = mp_init(&r);
		rverifyall(ret == MP_OKAY);

		ret = mp_init(&s);
		rverifyall(ret == MP_OKAY);

		ret = wc_ecc_sign_hash_ex(digest, sizeof(digest), &rng, key, &r, &s);
		rverifyall(ret == 0);
		
		// export r & s
		mp_to_unsigned_bin_len(&r, outSignature, eccKeySz);
		mp_to_unsigned_bin_len(&s, outSignature + eccKeySz, eccKeySz);
		signatureSize = eccKeySz * 2;
		success = true;
	}
	rcatchall
	{

	}

	mp_clear(&r);
	mp_clear(&s);
	FreeRngInternal(&rng);
	wc_Sha256Free(&sha256);

	return success;
}

bool 
EcDsa::VerifySha256(ecc_key* key,
					const u8* data,
					const unsigned dataSize,
					const u8* signature,
					const unsigned signatureSize)
{
	bool success = false;

	mp_int r;
	mp_int s;
	wc_Sha256 sha256;

	rtry
	{
		rverifyall(key);
		rverifyall(data && (dataSize > 0));
		rverifyall(signature);

		const unsigned eccKeySz = key->dp->size;
		rverifyall(signatureSize == (eccKeySz * 2));

		// calculate SHA-256 digest of data		
		unsigned char digest[WC_SHA256_DIGEST_SIZE];
		int ret = wc_InitSha256(&sha256);
		rverifyall(ret == 0);
		ret = wc_Sha256Update(&sha256, data, dataSize);
		rverifyall(ret == 0);
		ret = wc_Sha256Final(&sha256, digest);
		rverifyall(ret == 0);

		// import r & s from signature
		ret = mp_init(&r);
		rverifyall(ret == MP_OKAY);

		ret = mp_init(&s);
		rverifyall(ret == MP_OKAY);

		ret = mp_read_unsigned_bin(&r, signature, eccKeySz);
		rverifyall(ret == MP_OKAY);
		ret = mp_read_unsigned_bin(&s, signature + eccKeySz, eccKeySz);
		rverifyall(ret == MP_OKAY);

		// verify the signature by decrypting the hash and comparing it to the hash calculated above
		int verified = 0;
		ret = wc_ecc_verify_hash_ex(&r, &s, digest, sizeof(digest), &verified, key);
		rverifyall(ret == 0);
		rverifyall(verified == 1);

		success = true;
	}
	rcatchall
	{

	}

	mp_clear(&r);
	mp_clear(&s);
	wc_Sha256Free(&sha256);

	return success;
}

///////////////////////////////////////////////////////////////////////////////
//  netP2pCryptContext
///////////////////////////////////////////////////////////////////////////////
netP2pCryptContext::netP2pCryptContext()
{
	Clear();
}

netP2pCryptContext::~netP2pCryptContext()
{
	Clear();
}

void
netP2pCryptContext::Init(const netP2pCrypt::Key& key,
						 const u8 flags)
{
	Clear();
	m_Key = key;
	m_Flags = flags;
	m_IsValid = true;
}

void
netP2pCryptContext::Clear()
{
	m_IsValid = false;
	m_Flags = 0;
	m_Key.Clear();
}

const 
netP2pCrypt::Key& 
netP2pCryptContext::GetKey() const
{
	return m_Key;
}

u8
netP2pCryptContext::GetFlags() const
{
	return m_Flags;
}

///////////////////////////////////////////////////////////////////////////////
//  netP2pCrypt::Key
///////////////////////////////////////////////////////////////////////////////
#if NET_CRYPTO_USE_BCRYPT
static BCRYPT_ALG_HANDLE s_EncryptionAlgo = NULL;
static BCRYPT_ALG_HANDLE s_HmacAlgo = NULL;
static DWORD s_SizeOfHashObject = 0;
#endif

netP2pCrypt::Key::Key()
{
	Clear();
}

netP2pCrypt::Key::~Key()
{
	Clear();
}

void
netP2pCrypt::Key::Clear()
{
	sysMemSet(m_RawKey, 0, sizeof(m_RawKey));
	m_IsValid = false;
}

bool 
netP2pCrypt::Key::IsValid() const
{
	return m_IsValid;
}

void netP2pCrypt::Key::CombineKeys(const netP2pCrypt::Key& key1, const netP2pCrypt::Key& key2)
{
	netAssert(key1.IsValid() && key2.IsValid());

	u8 combinedKey[netP2pCrypt::Key::KEY_LENGTH_IN_BYTES];

	// XOR keys together
	for(unsigned i = 0; i < sizeof(combinedKey); ++i)
	{
		combinedKey[i] = key1.m_RawKey[i] ^ key2.m_RawKey[i];
	}

	ResetKey(combinedKey, sizeof(combinedKey));
}

void netP2pCrypt::Key::ResetKey(const u8* key, const unsigned keyLengthInBytes)
{
	if(netVerify(key) && netVerify(keyLengthInBytes == sizeof(m_RawKey)))
	{
		Clear();

		sysMemCpy(m_RawKey, key, sizeof(m_RawKey));

		m_IsValid = true;
	}
}

bool 
netP2pCrypt::Key::Export(void* buf,
						const unsigned sizeofBuf,
						unsigned* size) const
{
	datBitBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	const bool success = bb.WriteBytes(m_RawKey, KEY_LENGTH_IN_BYTES);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool
netP2pCrypt::Key::Import(const void* buf,
					   const unsigned sizeofBuf,
					   unsigned* size)
{
	datBitBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf);

	Clear();

	u8 rawKey[KEY_LENGTH_IN_BYTES];
	const bool success = bb.ReadBytes(rawKey, KEY_LENGTH_IN_BYTES);

	if(success)
	{
		ResetKey(rawKey, sizeof(rawKey));
	}

	if(size){*size = success ? bb.GetNumBytesRead() : 0;}

	return success;
}

const u8*
netP2pCrypt::Key::GetRawKeyPtr() const
{
	netAssert(m_IsValid);
	return m_RawKey;
}

void
netP2pCrypt::Key::GetRawKey(u8 (&rawKey)[KEY_LENGTH_IN_BYTES]) const
{
	netAssert(m_IsValid);
	sysMemCpy(rawKey, m_RawKey, sizeof(rawKey));
}

bool 
netP2pCrypt::Key::operator==(const netP2pCrypt::Key& that) const
{
	return memcmp(this->m_RawKey, that.m_RawKey, sizeof(this->m_RawKey)) == 0;
}

///////////////////////////////////////////////////////////////////////////////
//  netP2pCrypt::PublicKey
///////////////////////////////////////////////////////////////////////////////
netP2pCrypt::PublicKey::PublicKey()
{
	Clear();
}

netP2pCrypt::PublicKey::~PublicKey()
{
	Clear();
}

void
netP2pCrypt::PublicKey::Clear()
{
	sysMemSet(m_RawKey, 0, sizeof(m_RawKey));
	m_KeyLengthInBytes = 0;
	m_IsValid = false;
}

bool 
netP2pCrypt::PublicKey::IsValid() const
{
	return m_IsValid;
}

void netP2pCrypt::PublicKey::ResetKey(const u8* key, const unsigned keyLengthInBytes)
{
	if(netVerify(key)
		&& netVerify(keyLengthInBytes > 0)
		&& netVerify(keyLengthInBytes <= sizeof(m_RawKey)))
	{
		Clear();

		m_KeyLengthInBytes = keyLengthInBytes;
		sysMemCpy(m_RawKey, key, keyLengthInBytes);

		m_IsValid = true;
	}
}

bool 
netP2pCrypt::PublicKey::Export(void* buf,
								const unsigned sizeofBuf,
								unsigned* size) const
{
	datBitBuffer bb;
	bb.SetReadWriteBytes(buf, sizeofBuf);

	netAssert(IsValid());

	const bool success = netVerifyf(m_KeyLengthInBytes <= KEY_LENGTH_IN_BYTES, "m_KeyLengthInBytes overflow [%u]", m_KeyLengthInBytes)
						 && bb.WriteUns(m_KeyLengthInBytes, datBitsNeeded<KEY_LENGTH_IN_BYTES>::COUNT)
						 && bb.WriteBytes(m_RawKey, m_KeyLengthInBytes);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

	if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

	return success;
}

bool
netP2pCrypt::PublicKey::Import(const void* buf,
								const unsigned sizeofBuf,
								unsigned* size)
{
	datBitBuffer bb;
	bb.SetReadOnlyBytes(buf, sizeofBuf);

	Clear();

	u8 rawKey[KEY_LENGTH_IN_BYTES];
	const bool success = bb.ReadUns(m_KeyLengthInBytes, datBitsNeeded<KEY_LENGTH_IN_BYTES>::COUNT)
						 && netVerifyf(m_KeyLengthInBytes <= KEY_LENGTH_IN_BYTES, "m_KeyLengthInBytes overflow [%u]", m_KeyLengthInBytes)
						 && bb.ReadBytes(rawKey, m_KeyLengthInBytes);

	if(success)
	{
		ResetKey(rawKey, m_KeyLengthInBytes);
		netAssert(IsValid());
	}

	if(size){*size = success ? bb.GetNumBytesRead() : 0;}

	return success;
}

void
netP2pCrypt::PublicKey::GetRawKey(u8 (&rawKey)[KEY_LENGTH_IN_BYTES], unsigned* keyLengthInBytes) const
{
	netAssert(m_IsValid);
	netAssert(keyLengthInBytes);
	netAssert(*keyLengthInBytes >= m_KeyLengthInBytes);
	*keyLengthInBytes = m_KeyLengthInBytes;
	sysMemCpy(rawKey, m_RawKey, m_KeyLengthInBytes);
}

bool 
netP2pCrypt::PublicKey::operator==(const netP2pCrypt::PublicKey& that) const
{
	return (this->m_KeyLengthInBytes == that.m_KeyLengthInBytes)
			&& memcmp(this->m_RawKey, that.m_RawKey, m_KeyLengthInBytes) == 0;
}

///////////////////////////////////////////////////////////////////////////////
//  netP2pCrypt
///////////////////////////////////////////////////////////////////////////////

u8 netP2pCrypt::sm_HmacKey[HMAC_KEY_MAX_LEN];
u8 netP2pCrypt::sm_HmacSalt[HMAC_SALT_MAX_LEN];
unsigned netP2pCrypt::sm_HmacKeyLen = 0;
unsigned netP2pCrypt::sm_HmacSaltLen = 0;
netP2pCrypt::Key netP2pCrypt::sm_LocalPeerKey;
netP2pCrypt::PublicKey netP2pCrypt::sm_LocalPublicKey;
netCrypto::DiffieHellman netP2pCrypt::sm_DiffieHellman;

bool netP2pCrypt::sm_Initialized = false;

bool
netP2pCrypt::Init()
{
	CompileTimeAssert(MAX_SIZEOF_P2P_CRYPT_OVERHEAD == netP2pCrypt::MAX_SIZEOF_OVERHEAD_IN_BYTES);

	rtry
	{
		rcheck(!sm_Initialized, catchall, );

#if NET_CRYPTO_USE_BCRYPT
		// Set up AES256 in CBC mode
		DWORD desiredKeyLengthInBits = 256;
		NTSTATUS status = BCryptOpenAlgorithmProvider(&s_EncryptionAlgo,
														BCRYPT_AES_ALGORITHM,
														NULL,
														0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptOpenAlgorithmProvider(BCRYPT_AES_ALGORITHM)", status));

		// make sure the block size is 16 bytes as a sanity check
		DWORD cbBlockLen = 0;
		DWORD cbData = 0;
		status = BCryptGetProperty(s_EncryptionAlgo, 
									BCRYPT_BLOCK_LENGTH, 
									(PBYTE)&cbBlockLen, 
									sizeof(cbBlockLen), 
									&cbData, 
									0);

		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGetProperty(BCRYPT_BLOCK_LENGTH)", status));
		rverify(cbBlockLen == 16, catchall, netError("Block length is %u, expected 16", (unsigned)cbBlockLen));

		// make sure 256 bit keys are supported
		BCRYPT_KEY_LENGTHS_STRUCT keyLengths = {0};
		status = BCryptGetProperty(s_EncryptionAlgo, 
									BCRYPT_KEY_LENGTHS, 
									(PBYTE)&keyLengths, 
									sizeof(keyLengths), 
									&cbData, 
									0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGetProperty(BCRYPT_KEY_LENGTHS)", status));
		rverify(keyLengths.dwMaxLength >= desiredKeyLengthInBits, catchall, netError("%u-bit keys are not supported", (unsigned)desiredKeyLengthInBits));

		// set AES chaining mode to CBC (cipher block chaining)
		status = BCryptSetProperty(
                            s_EncryptionAlgo, 
                            BCRYPT_CHAINING_MODE, 
                            (PBYTE)BCRYPT_CHAIN_MODE_CBC, 
                            sizeof(BCRYPT_CHAIN_MODE_CBC), 
                            0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptSetProperty(BCRYPT_CHAINING_MODE)", status));

		// set up HMAC-SHA256
		status = BCryptOpenAlgorithmProvider(&s_HmacAlgo,
											BCRYPT_SHA256_ALGORITHM,
											NULL,
											BCRYPT_ALG_HANDLE_HMAC_FLAG);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptOpenAlgorithmProvider(BCRYPT_SHA256_ALGORITHM)", status));

		status = BCryptGetProperty(s_HmacAlgo, 
									BCRYPT_OBJECT_LENGTH, 
									(PBYTE)&s_SizeOfHashObject,
									sizeof(DWORD), 
									&cbData, 
									0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGetProperty(BCRYPT_OBJECT_LENGTH)", status));

		DWORD sizeOfHashDigest = 0;
		status = BCryptGetProperty(s_HmacAlgo, 
                                    BCRYPT_HASH_LENGTH, 
                                    (PBYTE)&sizeOfHashDigest, 
                                    sizeof(DWORD), 
                                    &cbData, 
                                    0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGetProperty(BCRYPT_HASH_LENGTH)", status));

		netAssert(sizeOfHashDigest == (256 >> 3));
		netAssert(sizeOfHashDigest == P2P_CRYPT_HMAC_LENGTH_IN_BYTES);
		netAssert(MAX_SIZEOF_P2P_CRYPT_OVERHEAD == (4 + cbBlockLen + P2P_CRYPT_TRUNCATED_HMAC_LENGTH_IN_BYTES + 1));
#else
		{
			SYS_CS_SYNC(s_CsRng);
			int ret = wc_InitRng(&s_WolfSslRng);
			rverify(ret == 0, catchall, netError("Error %d returned by InitRng", ret));
		}
#endif

		rverify(sm_DiffieHellman.InitDefault1024BitGroup(), catchall, );
		rverify(sm_DiffieHellman.GenerateKeyPair(), catchall, );

		sm_Initialized = true;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
netP2pCrypt::SetHmacKeyAndSalt(const u8* key, const unsigned keyLen, const u8* salt, const unsigned saltLen)
{
	rtry
	{
		rverify(key, catchall, );
		rverify(salt, catchall, );

		rverify(keyLen > 0, catchall, );
		rverify(saltLen > 0, catchall, );

		rverify(keyLen <= HMAC_KEY_MAX_LEN, catchall, );
		rverify(saltLen <= HMAC_SALT_MAX_LEN, catchall, );

		sysMemSet(sm_HmacKey, 0, HMAC_KEY_MAX_LEN);
		sysMemSet(sm_HmacSalt, 0, HMAC_SALT_MAX_LEN);

		sm_HmacKeyLen = keyLen;
		sm_HmacSaltLen = saltLen;

		sysMemCpy(sm_HmacKey, key, sm_HmacKeyLen);
		sysMemCpy(sm_HmacSalt, salt, sm_HmacSaltLen);
	}
	rcatchall
	{

	}
}

void 
netP2pCrypt::SetHmacKeyAndSalt(const netP2pCrypt::HmacKeySalt& p2pKeySalt)
{
	SetHmacKeyAndSalt(p2pKeySalt, HMAC_KEY_MAX_LEN, p2pKeySalt + HMAC_KEY_MAX_LEN, HMAC_SALT_MAX_LEN);
}

bool 
netP2pCrypt::GenerateHmac(const u8* data,
						  const unsigned sizeOfData,
						  const u8* secret,
						  const unsigned sizeOfSecret,
						  const u8* salt,
						  const unsigned sizeOfSalt,
						  u8* digest)
{
	PROFILE

	bool success = false;

#if NET_CRYPTO_USE_BCRYPT
	BCRYPT_HASH_HANDLE hHash = NULL;

	rtry
	{
		u8* hashObject = (u8*)Alloca(u8, s_SizeOfHashObject);
		rverify(hashObject, catchall, netError("Error allocating %u bytes of memory on the stack", (unsigned)s_SizeOfHashObject));

		// cannot use BCRYPT_HASH_REUSABLE_FLAG on Windows Vista or Windows 7, so we need to create and destroy a new hashing object every time
		NTSTATUS status = BCryptCreateHash(s_HmacAlgo,
											&hHash,
											hashObject,
											s_SizeOfHashObject,
											(PUCHAR)secret,
											sizeOfSecret,
											0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptCreateHash", status));

		// hash the data
		status = BCryptHashData(hHash,
								(PUCHAR)data,
								sizeOfData,
								0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptHashData", status));

		// add salt
		status = BCryptHashData(hHash,
								(PUCHAR)salt,
								sizeOfSalt,
								0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptHashData", status));

		// finalize the hash
		status = BCryptFinishHash(hHash, 
									digest, 
									P2P_CRYPT_HMAC_LENGTH_IN_BYTES, 
									0);

		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptFinishHash", status));

		success = true;
	}
	rcatchall
	{
		
	}

	if(hHash)    
	{
		BCryptDestroyHash(hHash);
	}
#else
	rtry
	{
		// wolfSSL library

		Hmac hmac;
		int ret = wc_HmacSetKey(&hmac, WC_SHA256, secret, sizeOfSecret);
		rverify(ret == 0, catchall, netError("Error %d returned by HmacSetKey", ret));

		ret = wc_HmacUpdate(&hmac, data, sizeOfData);
		rverify(ret == 0, catchall, netError("Error %d returned by HmacUpdate", ret));

		ret = wc_HmacUpdate(&hmac, salt, sizeOfSalt);
		rverify(ret == 0, catchall, netError("Error %d returned by HmacUpdate", ret));

		ret = wc_HmacFinal(&hmac, digest);
		rverify(ret == 0, catchall, netError("Error %d returned by HmacFinal", ret));

		success = true;
	}
	rcatchall
	{

	}
#endif

	return success;
}

bool 
netP2pCrypt::Encrypt(const netP2pCrypt::Key& key,
					 const u8* data,
					 const unsigned sizeOfData,
					 u8* encodedData,
					 unsigned& sizeOfEncodedData)
{
	PROFILE

	rtry
	{
		rverify(data, catchall, );
		rverify(sizeOfData > 0, catchall, );
		rverify(encodedData, catchall, );
		rverify(sizeOfEncodedData >= (sizeOfData + MAX_SIZEOF_P2P_CRYPT_OVERHEAD - 1), catchall, );
		rverify(key.IsValid(), catchall, );
		rverify(sm_HmacKeyLen > 0, catchall, netError("HMAC key not initialized. Call SetHmacKeyAndSalt()."));
		rverify(sm_HmacSaltLen > 0, catchall, netError("HMAC salt not initialized. Call SetHmacKeyAndSalt()."));

		const unsigned BLOCK_SIZE = 16;

		// create a cryptographically random IV which is stored along with the ciphertext.
		// The IV (initialization vector) is used as the first block in AES-CBC.
		const unsigned SIZE_OF_IV = BLOCK_SIZE;

		// generate the IV (initialization vector). Note: the IV buffer is modified during encryption.

		// to reduce packet overhead, we generate a 4 byte crypto random seed, then use netRandom
		// with that seed to generate the IV. This saves 12 bytes per packet.
		// This is a compromise between (1) sending a full 16 byte IV, and (2) using a shared IV  
		// generated during key exchange. This method ensures that the ciphertext is always unique, 
		// even when the same plaintext is encrypted multiple times (i.e. packet resends).
		const unsigned SIZE_OF_IV_SEED = 4;
		u32 seed = 0;
		CompileTimeAssert(sizeof(seed) == SIZE_OF_IV_SEED);
		rverify(GenerateRandomBytes((u8*)&seed, SIZE_OF_IV_SEED), catchall, );
		sysMemCpy(encodedData, &seed, SIZE_OF_IV_SEED);
		u8 iv[SIZE_OF_IV];
		netRandom r((int)seed);
		for(unsigned i = 0; i < SIZE_OF_IV; ++i)
		{
			iv[i] = (u8)r.GetRanged(0, 255);
		}	

		// add plaintext to encrypt
		// TODO: NS - could potentially eliminate this copy and do encrypt-in-place. IV would need
		// to be stored after the encrypted data, and the data buffer would need to be
		// >= sizeOfData + MAX_SIZEOF_P2P_CRYPT_OVERHEAD.
		unsigned sizeOfPlaintext = sizeOfData;
		u8* plaintext = encodedData + SIZE_OF_IV_SEED;
		sysMemCpy(plaintext, data, sizeOfPlaintext);

		// PKCS#7 padding - the value of each added byte is the number of bytes that are added, i.e. N bytes, each of value N are added.
		// Example: if there are four bytes of padding, the padding bytes will be [0x04, 0x04, 0x04, 0x04].
		// There will always be at least 1 byte of padding, so we know how much padding was added when we decrypt the data.
		// if sizeOfPlaintext % BLOCK_SIZE == 0, an extra block is added.
		u8* padding = plaintext + sizeOfPlaintext;
		unsigned paddingSize = BLOCK_SIZE - (sizeOfPlaintext % BLOCK_SIZE);
		sysMemSet(padding, paddingSize, paddingSize);
		sizeOfPlaintext += paddingSize;

		const u8* rawKey = key.GetRawKeyPtr();

		// encrypt the plaintext with the supplied key

#if NET_CRYPTO_USE_BCRYPT
		u8* ciphertext = plaintext;
		unsigned ciphertextBufSize = sizeOfPlaintext;

		// get the size of the key object
		DWORD cbKeyObject = 0;
		DWORD cbData = 0;
		NTSTATUS status = BCryptGetProperty(s_EncryptionAlgo, 
											BCRYPT_OBJECT_LENGTH, 
											(PBYTE)&cbKeyObject, 
											sizeof(DWORD), 
											&cbData, 
											0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGetProperty(BCRYPT_OBJECT_LENGTH)", status));

		// allocate the key object
		u8* bcryptKeyObject = (u8*)Alloca(u8, cbKeyObject);
		rverify(bcryptKeyObject, catchall, netError("Error allocating %ld bytes of memory on the stack", cbKeyObject));

		// generate the key from the raw key bytes
		void* bcryptKeyHandle = NULL;
		status = BCryptGenerateSymmetricKey(s_EncryptionAlgo,
											&bcryptKeyHandle,
											bcryptKeyObject,
											cbKeyObject,
											(PBYTE)rawKey,
											netP2pCrypt::Key::KEY_LENGTH_IN_BYTES,
											0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGenerateSymmetricKey", status));

		ULONG sizeOfCipherText = 0;
		status = BCryptEncrypt(bcryptKeyHandle,
								plaintext,
								sizeOfPlaintext,
								NULL, // must be NULL for symmetric keys
								iv,
								SIZE_OF_IV,
								ciphertext,
								ciphertextBufSize,
								&sizeOfCipherText,
								0);

		// make sure to destroy the key even if encrypt failed
		NTSTATUS destroyKeyStatus = BCryptDestroyKey(bcryptKeyHandle);
		if(!netVerify(NT_SUCCESS(destroyKeyStatus)))
		{
			netError("Error 0x%lx returned by BCryptDestroyKey", status);
		}

		rcheck(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptEncrypt", status));
#else
		Aes aes;
		int ret = wc_AesInit(&aes, nullptr, INVALID_DEVID);
		rverify(ret == 0, catchall, netError("Error %d returned by wc_AesInit", ret));
		ret = wc_AesSetKey(&aes, rawKey, Key::KEY_LENGTH_IN_BYTES, iv, AES_ENCRYPTION);
		rverify(ret == 0, catchall, netError("Error %d returned by AesSetKey", ret));
		ret = wc_AesCbcEncrypt(&aes, plaintext, plaintext, sizeOfPlaintext);
		rverify(ret == 0, catchall, netError("Error %d returned by AesCbcEncrypt", ret));
		unsigned sizeOfCipherText = sizeOfPlaintext;
#endif

		rverify(sizeOfCipherText == sizeOfPlaintext, catchall, );

		// IV + ciphertext
		sizeOfEncodedData = sizeOfCipherText + SIZE_OF_IV_SEED;

		// add HMAC. We use encrypt-then-MAC instead of MAC-then-encrypt.
		// It avoids certain padding oracle attacks.
		// It uses less CPU to reject a malicious/altered packet.
		// https://codeinsecurity.wordpress.com/2013/04/05/quick-crypto-lesson-why-mac-then-encrypt-is-bad/
		// http://crypto.stackexchange.com/questions/202/should-we-mac-then-encrypt-or-encrypt-then-mac
		// "The sore point of encrypt-then-MAC is that you have to be careful about what you MAC: you must not forget the IV...
		// ...otherwise, the attacker could change [it], inducing a plaintext alteration which would be undetected by the MAC."

		// xor the encryption key with the supplied hmac key and use that as the final hmac key
		// TODO: NS - store the HMAC key with the p2p key so we don't have to do this operation for every packet.
		u8 hmacKey[HMAC_KEY_MAX_LEN];
		for(unsigned i = 0; i < sizeof(hmacKey); ++i)
		{
			hmacKey[i] = rawKey[i % netP2pCrypt::Key::KEY_LENGTH_IN_BYTES] ^ sm_HmacKey[i % sm_HmacKeyLen];
		}

		u8 computedDigest[P2P_CRYPT_HMAC_LENGTH_IN_BYTES];
		rverify(GenerateHmac(encodedData, sizeOfEncodedData, hmacKey, sizeof(hmacKey), sm_HmacSalt, sm_HmacSaltLen, computedDigest), catchall, netError("GenerateHmac failed"));
		
		// truncate the HMAC to reduce overhead
		CompileTimeAssert(P2P_CRYPT_TRUNCATED_HMAC_LENGTH_IN_BYTES <= P2P_CRYPT_HMAC_LENGTH_IN_BYTES);
		sysMemCpy(&encodedData[sizeOfEncodedData], computedDigest, P2P_CRYPT_TRUNCATED_HMAC_LENGTH_IN_BYTES);

		sizeOfEncodedData += P2P_CRYPT_TRUNCATED_HMAC_LENGTH_IN_BYTES;

		netAssert(sizeOfEncodedData <= (sizeOfData + MAX_SIZEOF_P2P_CRYPT_OVERHEAD - 1));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netP2pCrypt::Decrypt(const netP2pCrypt::Key& key,
					 u8** buf,
					 unsigned& sizeOfBuf)
{
	PROFILE

	rtry
	{
		rverify(buf, catchall, );
		rverify(*buf, catchall, );
		rverify(sizeOfBuf >= (MAX_SIZEOF_P2P_CRYPT_OVERHEAD - 1), catchall, );
		rverify(key.IsValid(), catchall, );
		rverify(sm_HmacKeyLen > 0, catchall, netError("HMAC key not initialized. Call SetHmacKeyAndSalt()."));
		rverify(sm_HmacSaltLen > 0, catchall, netError("HMAC salt not initialized. Call SetHmacKeyAndSalt()."));

		// verify HMAC
		// xor the encryption key with the supplied hmac key and use that as the final hmac key
		u8 hmacKey[HMAC_KEY_MAX_LEN];

		const u8* rawKey = key.GetRawKeyPtr();
		for(unsigned i = 0; i < sizeof(hmacKey); ++i)
		{
			hmacKey[i] = rawKey[i % netP2pCrypt::Key::KEY_LENGTH_IN_BYTES] ^ sm_HmacKey[i % sm_HmacKeyLen];
		}

		u8 computedDigest[P2P_CRYPT_HMAC_LENGTH_IN_BYTES];
		rverify(GenerateHmac(*buf, sizeOfBuf - P2P_CRYPT_TRUNCATED_HMAC_LENGTH_IN_BYTES, hmacKey, sizeof(hmacKey), sm_HmacSalt, sm_HmacSaltLen, computedDigest), catchall, netError("GenerateHmac failed"));

		u8* digest = *buf + sizeOfBuf - P2P_CRYPT_TRUNCATED_HMAC_LENGTH_IN_BYTES;

		rcheck(memcmp(digest, computedDigest, P2P_CRYPT_TRUNCATED_HMAC_LENGTH_IN_BYTES) == 0, catchall, netWarning("Stored HMAC doesn't match computed HMAC"));

		const unsigned BLOCK_SIZE = 16;
		const unsigned SIZE_OF_IV = BLOCK_SIZE;

		const unsigned SIZE_OF_IV_SEED = 4;

		u32 seed = 0;
		memcpy(&seed, *buf, SIZE_OF_IV_SEED);
		*buf += SIZE_OF_IV_SEED;

		u8 iv[SIZE_OF_IV];
		netRandom r((int)seed);
		for(unsigned i = 0; i < SIZE_OF_IV; ++i)
		{
			iv[i] = (u8)r.GetRanged(0, 255);
		}

		u8* ciphertext = *buf;
		u8* plaintext = ciphertext;
		const unsigned sizeOfCipherText = sizeOfBuf - SIZE_OF_IV_SEED - P2P_CRYPT_TRUNCATED_HMAC_LENGTH_IN_BYTES;

#if NET_CRYPTO_USE_BCRYPT
		// get the size of the key object
		DWORD cbKeyObject = 0;
		DWORD cbData = 0;
		NTSTATUS status = BCryptGetProperty(s_EncryptionAlgo, 
											BCRYPT_OBJECT_LENGTH, 
											(PBYTE)&cbKeyObject, 
											sizeof(DWORD), 
											&cbData, 
											0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGetProperty(BCRYPT_OBJECT_LENGTH)", status));

		// allocate the key object
		u8* bcryptKeyObject = (u8*)Alloca(u8, cbKeyObject);
		rverify(bcryptKeyObject, catchall, netError("Error allocating %ld bytes of memory on the stack", cbKeyObject));

		// generate the key from the raw key bytes
		void* bcryptKeyHandle = NULL;
		status = BCryptGenerateSymmetricKey(s_EncryptionAlgo,
											&bcryptKeyHandle,
											bcryptKeyObject,
											cbKeyObject,
											(PBYTE)rawKey,
											netP2pCrypt::Key::KEY_LENGTH_IN_BYTES,
											0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGenerateSymmetricKey", status));

		DWORD sizeOfPlaintext = 0;
		status = BCryptDecrypt(bcryptKeyHandle,
								ciphertext,
								sizeOfCipherText,
								NULL, // pPaddingInfo - must be NULL for symmetric keys
								iv,
								SIZE_OF_IV,
								ciphertext,
								sizeOfCipherText,
								&sizeOfPlaintext,
								0);

		// make sure to destroy the key even if decrypt failed
		NTSTATUS destroyKeyStatus = BCryptDestroyKey(bcryptKeyHandle);
		if(!netVerify(NT_SUCCESS(destroyKeyStatus)))
		{
			netError("Error 0x%lx returned by BCryptDestroyKey", status);
		}

		rcheck(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptDecrypt", status));
#else
		Aes aes;
		int ret = wc_AesInit(&aes, nullptr, INVALID_DEVID);
		rverify(ret == 0, catchall, netError("Error %d returned by wc_AesInit", ret));
		ret = wc_AesSetKey(&aes, rawKey, Key::KEY_LENGTH_IN_BYTES, iv, AES_DECRYPTION);
		rverify(ret == 0, catchall, netError("Error %d returned by AesSetKey", ret));
		ret = wc_AesCbcDecrypt(&aes, ciphertext, ciphertext, sizeOfCipherText);
		rverify(ret == 0, catchall, netError("Error %d returned by AesCbcDecrypt", ret));
		unsigned sizeOfPlaintext = sizeOfCipherText;
#endif

		rverify(sizeOfPlaintext == sizeOfCipherText, catchall, );

		// the last byte of plaintext contains the padding length
		unsigned paddingSize = plaintext[sizeOfPlaintext - 1];
		rcheck(paddingSize >= 1 && paddingSize <= BLOCK_SIZE, catchall, netError("Invalid padding length %u. Expected in [1,16]", paddingSize));

		sizeOfPlaintext -= paddingSize;

		// verify that our padding bytes are what we expect
		u8* padding = &plaintext[sizeOfPlaintext];
		for(unsigned i = 0; i < paddingSize; ++i)
		{
			rcheck(padding[i] == paddingSize, catchall, netError("Unexpected padding byte %u, expected %u", padding[i], paddingSize));
		}

		sizeOfBuf = sizeOfPlaintext;

 		return true;
	}
	rcatchall
	{
		sizeOfBuf = 0;
		return false;
	}
}

void 
netP2pCrypt::Shutdown()
{
#if NET_CRYPTO_USE_BCRYPT
	if(s_EncryptionAlgo)
	{
		BCryptCloseAlgorithmProvider(s_EncryptionAlgo, 0);
	}

	if(s_HmacAlgo)
	{
		BCryptCloseAlgorithmProvider(s_HmacAlgo, 0);
	}
#endif

	sysMemSet(sm_HmacKey, 0, HMAC_KEY_MAX_LEN);
	sysMemSet(sm_HmacSalt, 0, HMAC_SALT_MAX_LEN);
	sm_HmacKeyLen = 0;
	sm_HmacSaltLen = 0;

	sm_LocalPeerKey.Clear();
	sm_LocalPublicKey.Clear();
	sm_DiffieHellman.Clear();

	sm_Initialized = false;
}

bool 
netP2pCrypt::GenerateRandomBytes(u8* outBytes, const unsigned numBytes)
{
	return netCrypto::GenerateRandomBytes(outBytes, numBytes);
}

bool 
netP2pCrypt::GenerateRandomKey(netP2pCrypt::Key& key)
{
	PROFILE

	rtry
	{
		u8 rawKey[netP2pCrypt::Key::KEY_LENGTH_IN_BYTES];
		rverify(GenerateRandomBytes(rawKey, sizeof(rawKey)), catchall, );
		key.ResetKey(rawKey, sizeof(rawKey));
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const netP2pCrypt::Key&
netP2pCrypt::GetLocalPeerKey()
{
	if(!sm_LocalPeerKey.IsValid())
	{
		netP2pCrypt::GenerateRandomKey(sm_LocalPeerKey);
	}

	netAssert(sm_LocalPeerKey.IsValid());

	return sm_LocalPeerKey;
}

const netP2pCrypt::PublicKey&
netP2pCrypt::GetLocalPublicKey()
{
	if(!sm_LocalPublicKey.IsValid())
	{
		u8 publicKey[netCrypto::DiffieHellman::MAX_PUBLIC_KEY_LENGTH_IN_BYTES];
		unsigned sizeOfPublicKey = sizeof(publicKey);
		sm_DiffieHellman.ExportPublicKey(publicKey, &sizeOfPublicKey);
		sm_LocalPublicKey.ResetKey(publicKey, sizeOfPublicKey);
	}

	netAssert(sm_LocalPublicKey.IsValid());

	return sm_LocalPublicKey;
}

bool 
netP2pCrypt::GenerateP2pKey(const netP2pCrypt::PublicKey& remotePublicKey, netP2pCrypt::Key& outKey)
{
	PROFILE

	rtry
	{
		u8 remoteDhPublicKey[netP2pCrypt::PublicKey::KEY_LENGTH_IN_BYTES];
		unsigned sizeOfRemoteDhPublickKey = sizeof(remoteDhPublicKey);
		remotePublicKey.GetRawKey(remoteDhPublicKey, &sizeOfRemoteDhPublickKey);

		u8 dhAgreeKey[netCrypto::DiffieHellman::MAX_AGREE_KEY_LENGTH_IN_BYTES];
		unsigned dhSizeOfAgreeKey = sizeof(dhAgreeKey);
		rverify(sm_DiffieHellman.Agree(remoteDhPublicKey, sizeOfRemoteDhPublickKey, dhAgreeKey, &dhSizeOfAgreeKey), catchall, );

		outKey.ResetKey(dhAgreeKey, dhSizeOfAgreeKey);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////
//  netCrypto
///////////////////////////////////////////////////////////////////////////////
struct netCrypto::DiffieHellman::Impl
{
	Impl()
	{
#if NET_CRYPTO_USE_BCRYPT
		m_DiffieHellmanAlgo = NULL;
		m_PrivKeyHandle = NULL;
#else
		sysMemSet(&m_WolfSslParams, 0, sizeof(m_WolfSslParams));
		sysMemSet(&m_WolfSslRng, 0, sizeof(m_WolfSslRng));
#endif

		Clear();
	}

	~Impl()
	{
		Clear();
	}

	void Init(const u8* prime,
			const unsigned sizeOfPrime,
			const u8* generator,
			const unsigned sizeOfGenerator)
	{
		Clear();

		m_SizeOfPrime = sizeOfPrime;
		m_SizeOfGenerator = sizeOfGenerator;

#if NET_CRYPTO_USE_BCRYPT
		sysMemCpy(m_Prime, prime, sizeOfPrime);
		sysMemCpy(m_Generator, generator, sizeOfGenerator);
#else
		(void)prime;
		(void)generator;
#endif
	}

	void Clear()
	{
		sysMemSet(m_PublicKey, 0, sizeof(m_PublicKey));

		m_SizeOfPrime = 0;
		m_SizeOfGenerator = 0;
		m_SizeOfPublicKey = 0;

#if NET_CRYPTO_USE_BCRYPT
		sysMemSet(m_Prime, 0, sizeof(m_Prime));
		sysMemSet(m_Generator, 0, sizeof(m_Generator));

		if(m_PrivKeyHandle)
		{
			BCryptDestroyKey(m_PrivKeyHandle);
		}

		if(m_DiffieHellmanAlgo)
		{
			BCryptCloseAlgorithmProvider(m_DiffieHellmanAlgo, 0);
		}

#else
		sysMemSet(m_PrivateKey, 0, sizeof(m_PrivateKey));
		m_SizeOfPrivateKey = 0;

		wc_FreeDhKey(&m_WolfSslParams);
		FreeRngInternal(&m_WolfSslRng);

		sysMemSet(&m_WolfSslParams, 0, sizeof(m_WolfSslParams));
		sysMemSet(&m_WolfSslRng, 0, sizeof(m_WolfSslRng));
#endif
	}

#if NET_CRYPTO_USE_BCRYPT
	u8 m_Prime[netCrypto::DiffieHellman::MAX_PRIME_SIZE_IN_BYTES];
	u8 m_Generator[netCrypto::DiffieHellman::MAX_GENERATOR_SIZE_IN_BYTES];

	BCRYPT_ALG_HANDLE m_DiffieHellmanAlgo;
	BCRYPT_KEY_HANDLE m_PrivKeyHandle;

#else
	DhKey m_WolfSslParams;
	WC_RNG m_WolfSslRng;
	u8 m_PrivateKey[MAX_PRIVATE_KEY_LENGTH_IN_BYTES];
	unsigned m_SizeOfPrivateKey;
#endif

	u8 m_PublicKey[MAX_PUBLIC_KEY_LENGTH_IN_BYTES];

	unsigned m_SizeOfPrime;
	unsigned m_SizeOfGenerator;
	unsigned m_SizeOfPublicKey;
};

netCrypto::DiffieHellman::DiffieHellman()
: m_pImpl(NULL)
{
	Clear();
}

netCrypto::DiffieHellman::~DiffieHellman()
{
	Clear();
}

void
netCrypto::DiffieHellman::Clear()
{
	m_Initialized = false;
	m_GeneratedKeyPair = false;

	if(m_pImpl)
	{
		delete m_pImpl;
		m_pImpl = NULL;
	}
}

bool 
netCrypto::DiffieHellman::Init(const u8* prime,
								const unsigned sizeOfPrime,
								const u8* generator,
								const unsigned sizeOfGenerator)
{
	rtry
	{
		Clear();

		rverify(prime, catchall, );
		rverify(sizeOfPrime <= MAX_PRIME_SIZE_IN_BYTES, catchall, );

		rverify(generator, catchall, );
		rverify(sizeOfGenerator <= MAX_GENERATOR_SIZE_IN_BYTES, catchall, );

		rverify(sizeOfPrime == sizeOfGenerator, catchall, netError("BCrypt requires the generator to be the same length as the key, pad with zeroes if necessary."));

		m_pImpl = rage_new Impl;
		rverify(m_pImpl, catchall, );

		m_pImpl->Init(prime, sizeOfPrime, generator, sizeOfGenerator);

#if NET_CRYPTO_USE_BCRYPT
		NTSTATUS status = BCryptOpenAlgorithmProvider(&m_pImpl->m_DiffieHellmanAlgo,
														BCRYPT_DH_ALGORITHM,
														NULL,
														0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptOpenAlgorithmProvider(BCRYPT_DH_ALGORITHM)", status));
#else
		wc_InitDhKey(&m_pImpl->m_WolfSslParams);
		int ret = wc_DhSetKey(&m_pImpl->m_WolfSslParams,
						  (const byte*)prime,
						  sizeOfPrime,
						  (const byte*)generator,
						  sizeOfGenerator);
		rverify(ret == 0, catchall, netError("Error %d returned by DhSetKey", ret));

		ret = wc_InitRng(&m_pImpl->m_WolfSslRng);
		rverify(ret == 0, catchall, netError("Error %d returned by InitRng", ret));
#endif

		m_Initialized = true;

		return true;
	}
	rcatchall 
	{
		Clear();
		return false;
	}
}

bool netCrypto::DiffieHellman::InitDefault1024BitGroup()
{
	/*
		RFC 2539 Well-Known Group 2 (1024-bit prime).
		https://tools.ietf.org/html/rfc2539#appendix-A.2
	*/
	static const u8 OakleyGroup2P[] = {
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xC9, 0x0F, 0xDA, 0xA2, 0x21, 0x68, 0xC2, 0x34,
		0xC4, 0xC6, 0x62, 0x8B, 0x80, 0xDC, 0x1C, 0xD1, 0x29, 0x02, 0x4E, 0x08, 0x8A, 0x67, 0xCC, 0x74,
		0x02, 0x0B, 0xBE, 0xA6, 0x3B, 0x13, 0x9B, 0x22, 0x51, 0x4A, 0x08, 0x79, 0x8E, 0x34, 0x04, 0xDD,
		0xEF, 0x95, 0x19, 0xB3, 0xCD, 0x3A, 0x43, 0x1B, 0x30, 0x2B, 0x0A, 0x6D, 0xF2, 0x5F, 0x14, 0x37,
		0x4F, 0xE1, 0x35, 0x6D, 0x6D, 0x51, 0xC2, 0x45, 0xE4, 0x85, 0xB5, 0x76, 0x62, 0x5E, 0x7E, 0xC6,
		0xF4, 0x4C, 0x42, 0xE9, 0xA6, 0x37, 0xED, 0x6B, 0x0B, 0xFF, 0x5C, 0xB6, 0xF4, 0x06, 0xB7, 0xED,
		0xEE, 0x38, 0x6B, 0xFB, 0x5A, 0x89, 0x9F, 0xA5, 0xAE, 0x9F, 0x24, 0x11, 0x7C, 0x4B, 0x1F, 0xE6,
		0x49, 0x28, 0x66, 0x51, 0xEC, 0xE6, 0x53, 0x81, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
	};

	static const u8 OakleyGroup2G[] =
	{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02,
	};

	return Init((const u8*)OakleyGroup2P,
				sizeof(OakleyGroup2P),
				(const u8*)OakleyGroup2G,
				sizeof(OakleyGroup2G));
}

bool 
netCrypto::DiffieHellman::GenerateKeyPair()
{
	PROFILE

	rtry
	{
		rverify(m_Initialized, catchall, );

#if NET_CRYPTO_USE_BCRYPT
		DWORD DhParamBlobLength = sizeof(BCRYPT_DH_PARAMETER_HEADER) +
									m_pImpl->m_SizeOfPrime +
									m_pImpl->m_SizeOfGenerator;
 
		u8* DhParamBlob = (u8*)Alloca(u8, DhParamBlobLength);
		rverify(DhParamBlob, catchall, netError("Error allocating %u bytes of memory on the stack", (unsigned)DhParamBlobLength));

		BCRYPT_DH_PARAMETER_HEADER* DhParamHdrPointer = (BCRYPT_DH_PARAMETER_HEADER *)DhParamBlob;
 
		DhParamHdrPointer->cbLength      = DhParamBlobLength;
		DhParamHdrPointer->cbKeyLength   = m_pImpl->m_SizeOfPrime;
		DhParamHdrPointer->dwMagic       = BCRYPT_DH_PARAMETERS_MAGIC;
 
		sysMemCpy(DhParamBlob + sizeof(BCRYPT_DH_PARAMETER_HEADER), m_pImpl->m_Prime, m_pImpl->m_SizeOfPrime);
		sysMemCpy(DhParamBlob + sizeof(BCRYPT_DH_PARAMETER_HEADER) + m_pImpl->m_SizeOfPrime, m_pImpl->m_Generator, m_pImpl->m_SizeOfGenerator);

		NTSTATUS status = BCryptGenerateKeyPair(m_pImpl->m_DiffieHellmanAlgo,	// Algorithm handle
												&m_pImpl->m_PrivKeyHandle,		// Key handle - will be created
												m_pImpl->m_SizeOfPrime << 3,	// Length of the key - in bits
												0);								// Flags
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptGenerateKeyPair", status));
 
		status = BCryptSetProperty(m_pImpl->m_PrivKeyHandle,
											BCRYPT_DH_PARAMETERS,
											DhParamBlob,
											DhParamBlobLength,
											0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptSetProperty", status));
 
		status = BCryptFinalizeKeyPair(m_pImpl->m_PrivKeyHandle,             // Key handle
										0);                         // Flags
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptFinalizeKeyPair", status));
 
		DWORD PubBlobLengthA = 0;
		status = BCryptExportKey(m_pImpl->m_PrivKeyHandle,				// Handle of the key to export
									NULL,                       // Handle of the key used to wrap the exported key
									BCRYPT_DH_PUBLIC_BLOB,      // Blob type (null terminated unicode string)
									NULL,                       // Buffer that receives the key blob
									0,                          // Buffer length (in bytes)
									&PubBlobLengthA,            // Number of bytes copied to the buffer
									0);                         // Flags
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptExportKey", status));

		u8* dhKeyBlobBuffer = (u8*)Alloca(u8, PubBlobLengthA);
		rverify(dhKeyBlobBuffer, catchall, netError("Error allocating %u bytes of memory on the stack", (unsigned)PubBlobLengthA));

		BCRYPT_DH_KEY_BLOB* dhKeyBlob = (BCRYPT_DH_KEY_BLOB*)dhKeyBlobBuffer;
		status = BCryptExportKey(m_pImpl->m_PrivKeyHandle,             // Handle of the key to export
									NULL,                       // Handle of the key used to wrap the exported key
									BCRYPT_DH_PUBLIC_BLOB,      // Blob type (null terminated unicode string)
									(PUCHAR)dhKeyBlob,         // Buffer that receives the key blob
									PubBlobLengthA,             // Buffer length (in bytes)
									&PubBlobLengthA,            // Number of bytes copied to the buffer
									0);                         // Flags
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptExportKey", status));

		rverify(dhKeyBlob->cbKey <= MAX_PUBLIC_KEY_LENGTH_IN_BYTES, catchall, netError("Public key is unexpectedly large"));
		m_pImpl->m_SizeOfPublicKey = dhKeyBlob->cbKey;

		// 	BCRYPT_DH_KEY_BLOB
		// 	Modulus[cbKey]		// Big-endian.
		// 	Generator[cbKey]	// Big-endian.
		// 	Public[cbKey]		// Big-endian.
		sysMemCpy(m_pImpl->m_PublicKey, dhKeyBlobBuffer + sizeof(BCRYPT_DH_KEY_BLOB) + dhKeyBlob->cbKey + dhKeyBlob->cbKey, dhKeyBlob->cbKey);
#else
		// MS's implementation always requires the public key to be the same length as m_SizeOfPrime.
		// WolfSSL can be m_SizeOfPrime or (m_SizeOfPrime - 1). To make cross-platform multiplayer work, make sure
		// we get a public key with size m_SizeOfPrime.

		const int MAX_GEN_KEY_PAIR_ATTEMPTS = 20;
		unsigned numAttempts = 0;
		int ret = 0;

		do 
		{
			m_pImpl->m_SizeOfPrivateKey = sizeof(m_pImpl->m_PrivateKey);
			m_pImpl->m_SizeOfPublicKey = sizeof(m_pImpl->m_PublicKey);

			ret = wc_DhGenerateKeyPair(&m_pImpl->m_WolfSslParams,
									&m_pImpl->m_WolfSslRng,
									(byte*)m_pImpl->m_PrivateKey,
									(word32*)&m_pImpl->m_SizeOfPrivateKey,
									(byte*)m_pImpl->m_PublicKey,
									(word32*)&m_pImpl->m_SizeOfPublicKey);

			if(!netVerifyf(ret == 0, "Error %d returned by DhGenerateKeyPair. Will attempt to reinitialize RNG.", ret))
			{
				FreeRngInternal(&m_pImpl->m_WolfSslRng);
				NET_ASSERTS_ONLY(const int initRet =) wc_InitRng(&m_pImpl->m_WolfSslRng);
				netAssertf(initRet == 0, "DiffieHellman::GenerateKeyPair :: Error %d returned by InitRng", initRet);
			}

		} while((++numAttempts < MAX_GEN_KEY_PAIR_ATTEMPTS) && ((ret != 0) || (m_pImpl->m_SizeOfPublicKey != m_pImpl->m_SizeOfPrime)));

		rverify(ret == 0, catchall, netError("DiffieHellman::GenerateKeyPair failed after %u attempts", numAttempts));
		rverify((m_pImpl->m_SizeOfPrivateKey > 0) && (m_pImpl->m_SizeOfPrivateKey <= m_pImpl->m_SizeOfPublicKey), catchall, );
		rverify(m_pImpl->m_SizeOfPrivateKey <= netCrypto::DiffieHellman::MAX_PRIVATE_KEY_LENGTH_IN_BYTES, catchall, );
#endif

		rverify((m_pImpl->m_SizeOfPublicKey > 0) && (m_pImpl->m_SizeOfPublicKey <= m_pImpl->m_SizeOfPrime), catchall, );

		m_GeneratedKeyPair = true;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netCrypto::DiffieHellman::ExportPublicKey(u8* publicKey,
										  unsigned* sizeOfPublicKey)
{
	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(m_GeneratedKeyPair, catchall, );
		rverify(publicKey, catchall, );
		rverify(sizeOfPublicKey, catchall, );
		rverify(*sizeOfPublicKey >= m_pImpl->m_SizeOfPublicKey, catchall, );

		*sizeOfPublicKey = m_pImpl->m_SizeOfPublicKey;
		sysMemCpy(publicKey, m_pImpl->m_PublicKey, m_pImpl->m_SizeOfPublicKey);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netCrypto::DiffieHellman::Agree(const u8* otherPublicKey,
								const unsigned sizeOfOtherPublicKey,
								u8* agreeKey,
								unsigned* sizeOfAgreeKey)
{
	PROFILE

#if NET_CRYPTO_USE_BCRYPT
	BCRYPT_KEY_HANDLE otherPublicKeyHandle = NULL;
	BCRYPT_SECRET_HANDLE AgreedSecretHandleA = NULL;
#else
	Sha256 sha256;
#endif

	bool success = false;

	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(m_GeneratedKeyPair, catchall, );
		rverify(otherPublicKey, catchall, );
		rverify(sizeOfOtherPublicKey > 0, catchall, );
		rverify(agreeKey, catchall, );
		rverify(sizeOfAgreeKey, catchall, );
		rverify(*sizeOfAgreeKey >= m_pImpl->m_SizeOfPrime, catchall, );

#if NET_CRYPTO_USE_BCRYPT

		netAssert(sizeOfOtherPublicKey == m_pImpl->m_SizeOfPrime);
		netAssert(m_pImpl->m_SizeOfGenerator == m_pImpl->m_SizeOfPrime);

		// 	BCRYPT_DH_KEY_BLOB
		// 	Modulus[cbKey]		// Big-endian.
		// 	Generator[cbKey]	// Big-endian.
		// 	Public[cbKey]		// Big-endian.

		unsigned sizeOfBuf = sizeof(BCRYPT_DH_KEY_BLOB) + (m_pImpl->m_SizeOfPrime * 3);
		u8* dhKeyBlobBuffer = (u8*)Alloca(u8, sizeOfBuf);
		rverify(dhKeyBlobBuffer, catchall, netError("Error allocating %u bytes of memory on the stack", (unsigned)sizeOfBuf));
		
		BCRYPT_DH_KEY_BLOB* dhKeyBlob = (BCRYPT_DH_KEY_BLOB*)dhKeyBlobBuffer;
		dhKeyBlob->cbKey = m_pImpl->m_SizeOfPrime;
		dhKeyBlob->dwMagic = BCRYPT_DH_PUBLIC_MAGIC;
		sysMemCpy(dhKeyBlobBuffer + sizeof(BCRYPT_DH_KEY_BLOB), m_pImpl->m_Prime, m_pImpl->m_SizeOfPrime);
		sysMemCpy(dhKeyBlobBuffer + sizeof(BCRYPT_DH_KEY_BLOB) + dhKeyBlob->cbKey, m_pImpl->m_Generator, m_pImpl->m_SizeOfGenerator);
		sysMemCpy(dhKeyBlobBuffer + sizeof(BCRYPT_DH_KEY_BLOB) + dhKeyBlob->cbKey + dhKeyBlob->cbKey, otherPublicKey, sizeOfOtherPublicKey);

		NTSTATUS status = BCryptImportKeyPair(m_pImpl->m_DiffieHellmanAlgo,	// Alg handle
												NULL,						// Parameter not used
												BCRYPT_DH_PUBLIC_BLOB,		// Blob type (Null terminated unicode string)
												&otherPublicKeyHandle,		// Key handle that will be received
												(PUCHAR)dhKeyBlobBuffer,	// Buffer that points to the key blob
												sizeOfBuf,					// Buffer length in bytes
												0);							// Flags
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptImportKeyPair", status)); 
     
		status = BCryptSecretAgreement(m_pImpl->m_PrivKeyHandle,	// Private key handle
										otherPublicKeyHandle,		// Public key handle
										&AgreedSecretHandleA,		// Handle that represents the secret agreement value
										0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptSecretAgreement", status));

		const DWORD BufferLength = 1; 
		BCryptBuffer BufferArray[BufferLength] = {0}; 

		BufferArray[0].BufferType = KDF_HASH_ALGORITHM; 
		BufferArray[0].pvBuffer = (void*)BCRYPT_SHA256_ALGORITHM;
		BufferArray[0].cbBuffer = sizeof(BCRYPT_SHA256_ALGORITHM);

		BCryptBufferDesc ParameterList = {0}; 
		ParameterList.cBuffers  = BufferLength;
		ParameterList.pBuffers  = BufferArray; 
		ParameterList.ulVersion = BCRYPTBUFFER_VERSION; 

		// derive a 256-bit key from the agreed secret by hashing the secret with SHA256
		ULONG pcbResult = 0;
		status = BCryptDeriveKey(AgreedSecretHandleA,
										BCRYPT_KDF_HASH,
										&ParameterList,
										agreeKey,
										*sizeOfAgreeKey,
										&pcbResult,
										0);
		rverify(NT_SUCCESS(status), catchall, netError("Error 0x%lx returned by BCryptDeriveKey", status));
		rverify(pcbResult == (256 >> 3), catchall, netError("Derived key has unexpected size"));

		*sizeOfAgreeKey = pcbResult;

#else
		u8 agreedSecret[MAX_AGREE_KEY_LENGTH_IN_BYTES];
		unsigned sizeOfAgreedSecret = sizeof(agreedSecret);

		int ret = wc_DhAgree(&m_pImpl->m_WolfSslParams,
						  (byte*)agreedSecret,
						  (word32*)&sizeOfAgreedSecret,
						  (byte*)m_pImpl->m_PrivateKey,
						  (word32)m_pImpl->m_SizeOfPrivateKey,
						  (byte*)otherPublicKey,
						  (word32)sizeOfOtherPublicKey);
		rverify(ret == 0, catchall, netError("Error %d returned by DhAgree", ret));

		// derive a 256-bit key from the agreed secret by hashing the secret with SHA256
		rverify(wc_InitSha256(&sha256) == 0, catchall, );
		rverify(wc_Sha256Update(&sha256, agreedSecret, sizeOfAgreedSecret) == 0, catchall, );
		rverify(wc_Sha256Final(&sha256, agreeKey) == 0, catchall, );

		*sizeOfAgreeKey = SHA256_DIGEST_SIZE;
#endif

		success = true;
	}
	rcatchall
	{
		success = false;
	}

#if NET_CRYPTO_USE_BCRYPT
	if(otherPublicKeyHandle)
	{
		BCryptDestroyKey(otherPublicKeyHandle);
	}

	if(AgreedSecretHandleA)
	{
		BCryptDestroySecret(AgreedSecretHandleA);
	}
#else
	wc_Sha256Free(&sha256);
#endif

	return success;
}
