// 
// net/connectionrouter.cpp 
// 
// Copyright (C) 1999-2018 Rockstar Games.  All Rights Reserved. 
// 

#include "math/amath.h"
#include "diag/seh.h"
#include "net/connectionmanager.h"
#include "net/connectionrouter.h"
#include "net/crypto.h"
#include "net/event.h"
#include "net/message.h"
#include "net/netdiag.h"
#include "profile/rocky.h"
#include "rline/rlpresence.h"
#include "system/criticalsection.h"
#include "system/timer.h"

#include <stdint.h>

/*
	P2P relaying is based on RFC 3561
	Ad-hoc On-Demand Distance Vector (AODV) Routing	
	https://tools.ietf.org/html/rfc3561
	Section numbers in comments in this file refer to the
	above document unless otherwise noted.

	Also uses information from draft 9 of the AODV spec:
	https://tools.ietf.org/html/draft-ietf-manet-aodv-09

	This paper resolves some of the ambiguities in the RFC.
	http://rvg.web.cse.unsw.edu.au/pub/AODVloop.pdf
*/

namespace rage
{

// note: most output is on debug1 severity. This rarely gets tested during 
// development and we need all the logging we can get to debug issues.
RAGE_DEFINE_SUBCHANNEL(ragenet, router)
#undef __rage_channel
#define __rage_channel ragenet_router

PARAM(netnocxnrouting, "Disables all connection routing (including peer relaying and rerouting broken direct connections to the relay server).");
PARAM(netnopeerrelay, "Don't relay packets through other peers");
PARAM(netpeerrelaymaxhops, "Sets the maximum number of hops a packet can take through a p2p relay route");
PARAM(netpeerrelaymaxrelayedcxns, "Roughly sets the maximum number of connections the local peer can relay simultaneously. See documentation for SetMaxRelayedCxns().");

#if !__NO_OUTPUT && USE_NET_ASSERTS
#define ASSERT_CXNMGR_CS_LOCKED netAssert(m_CxnMgr && m_CxnMgr->GetCriticalSectionToken().IsLocked())
#else
#define ASSERT_CXNMGR_CS_LOCKED
#endif

//////////////////////////////////////////////////////////////////////////
//  Static vars
//////////////////////////////////////////////////////////////////////////

#if !__NO_OUTPUT
static size_t sm_MemoryUsed = 0;
static size_t sm_MaxMemoryUsed = 0;
#endif

//////////////////////////////////////////////////////////////////////////
//  netRouteRequestMsg
//////////////////////////////////////////////////////////////////////////
class netRouteRequestMsg
{
public:
	NET_MESSAGE_DECL(netRouteRequestMsg, NET_ROUTE_REQUEST_MSG);

	enum Flags
	{
		RREQ_FLAGS_NONE = 0,
		RREQ_FLAG_INVALID_DEST_SEQ = BIT0,
		RREQ_NUM_FLAGS = 1,
	};

	CompileTimeAssert(RREQ_NUM_FLAGS < (sizeof(u8) << 3));

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													 netConnectionRouter::SequenceNumber::MAX_EXPORTED_SIZE_IN_BYTES +	// m_RouteRequestId
													 netConnectionRouter::SequenceNumber::MAX_EXPORTED_SIZE_IN_BYTES +	// m_SrcSeq
													 netPeerId::MAX_EXPORTED_SIZE_IN_BYTES +							// m_SrcId
													 netConnectionRouter::SequenceNumber::MAX_EXPORTED_SIZE_IN_BYTES +	// m_DestSeq
													 netPeerId::MAX_EXPORTED_SIZE_IN_BYTES +							// m_DestId
													 sizeof(u8) +														// m_HopCount
													 sizeof(u8) +														// m_Ttl
													 sizeof(u8);														// m_Flags

	netRouteRequestMsg()
	{

	}

	netRouteRequestMsg(const netConnectionRouter::SequenceNumber& routeRequestId,
						const netConnectionRouter::SequenceNumber& srcSeq,
						const netPeerId& srcId,
						const netConnectionRouter::SequenceNumber& destSeq,
						const netPeerId& destId,
						const u8 hopCount,
						const u8 ttl,
						const u8 flags)
	{
		m_RouteRequestId = routeRequestId;
		m_SrcSeq = srcSeq;
		m_SrcId = srcId;
		m_DestSeq = destSeq;
		m_DestId = destId;
		m_HopCount = hopCount;
		m_Ttl = ttl;
		m_Flags = flags;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerUser(msg.m_RouteRequestId)
			&& bb.SerUser(msg.m_SrcSeq)
			&& bb.SerUser(msg.m_SrcId)
			&& bb.SerUser(msg.m_DestSeq)
			&& bb.SerUser(msg.m_DestId)
			&& bb.SerUns(msg.m_HopCount, sizeof(msg.m_HopCount) << 3)
			&& bb.SerUns(msg.m_Ttl, sizeof(msg.m_Ttl) << 3)
			&& bb.SerUns(msg.m_Flags, sizeof(msg.m_Flags) << 3);
	}

	bool IsDestSeqValid() const
	{
		return !(m_Flags & RREQ_FLAG_INVALID_DEST_SEQ);
	}

#if !__NO_OUTPUT
	void Dump() const
	{
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		char peerIdHexString2[netPeerId::TO_HEX_STRING_BUFFER_SIZE];

		netDebug1("netRouteRequestMsg: id:%u,src:%s,dest:%s,srcSeq:%u,destSeq:%u%s,hops:%u,ttl:%u,flags:%s",
					m_RouteRequestId.Get(),
					m_SrcId.ToHexString(peerIdHexString),
					m_DestId.ToHexString(peerIdHexString2),
					m_SrcSeq.Get(),
					m_DestSeq.Get(),
					IsDestSeqValid() ? "" : " (invalid)",
					(unsigned)m_HopCount,
					(unsigned)m_Ttl,
					IsDestSeqValid() ? "none" : "invalid destseq");
	}
#endif

	netConnectionRouter::SequenceNumber m_RouteRequestId;
	netConnectionRouter::SequenceNumber m_SrcSeq;
	netPeerId m_SrcId;
	netConnectionRouter::SequenceNumber m_DestSeq;
	netPeerId m_DestId;
	u8 m_HopCount;
	u8 m_Ttl;
	u8 m_Flags;
};

NET_MESSAGE_IMPL(netRouteRequestMsg);

//////////////////////////////////////////////////////////////////////////
//  netRouteReplyMsg
//////////////////////////////////////////////////////////////////////////
class netRouteReplyMsg
{
public:
	NET_MESSAGE_DECL(netRouteReplyMsg, NET_ROUTE_REPLY_MSG);

	enum Flags
	{
		RREP_FLAGS_NONE = 0,
		RREP_FLAG_GRATUITOUS_REPLY = BIT0,
		RREP_NUM_FLAGS = 1,
	};

	CompileTimeAssert(RREP_NUM_FLAGS < (sizeof(u8) << 3));

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													 sizeof(unsigned) +														// m_RouteReplyId
													 netPeerId::MAX_EXPORTED_SIZE_IN_BYTES +								// m_SrcId
													 netConnectionRouter::SequenceNumber::MAX_EXPORTED_SIZE_IN_BYTES  +		// m_DestSeq
													 netPeerId::MAX_EXPORTED_SIZE_IN_BYTES +								// m_DestId
													 sizeof(u8) +															// m_HopCount
													 sizeof(u8) +															// m_Ttl
													 sizeof(u8);															// m_Flags

	netRouteReplyMsg()
	{

	}

	netRouteReplyMsg(const unsigned routeReplyId,
					 const netPeerId& srcId,
					 const netConnectionRouter::SequenceNumber& destSeq,
					 const netPeerId& destId,
					 const u8 hopCount,
					 const u8 ttl,
					 const u8 flags)
	{
		m_RouteReplyId = routeReplyId;
		m_SrcId = srcId;
		m_DestSeq = destSeq;
		m_DestId = destId;
		m_HopCount = hopCount;
		m_Ttl = ttl;
		m_Flags = flags;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerUns(msg.m_RouteReplyId, sizeof(msg.m_RouteReplyId) << 3)
			&& bb.SerUser(msg.m_SrcId)
			&& bb.SerUser(msg.m_DestSeq)
			&& bb.SerUser(msg.m_DestId)
			&& bb.SerUns(msg.m_HopCount, sizeof(msg.m_HopCount) << 3)
			&& bb.SerUns(msg.m_Ttl, sizeof(msg.m_Ttl) << 3)
			&& bb.SerUns(msg.m_Flags, sizeof(msg.m_Flags) << 3);
	}

	bool IsGratuitous() const
	{
		return (m_Flags & RREP_FLAG_GRATUITOUS_REPLY);
	}

#if !__NO_OUTPUT
	void Dump() const
	{
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		char peerIdHexString2[netPeerId::TO_HEX_STRING_BUFFER_SIZE];

		netDebug1("netRouteReplyMsg: id:%u,src:%s,dest:%s,destSeq:%u,hops:%u,ttl:%u,flags:%s",
					m_RouteReplyId,
					m_SrcId.ToHexString(peerIdHexString),
					m_DestId.ToHexString(peerIdHexString2),
					m_DestSeq.Get(),
					(unsigned)m_HopCount,
					(unsigned)m_Ttl,
					IsGratuitous() ? "gratuitous" : "none");
	}
#endif

	// note: route reply id is only used for logging purposes
	unsigned m_RouteReplyId;
	netPeerId m_SrcId;
	netConnectionRouter::SequenceNumber m_DestSeq;
	netPeerId m_DestId;
	u8 m_HopCount;
	u8 m_Ttl;
	u8 m_Flags;
};

NET_MESSAGE_IMPL(netRouteReplyMsg);

//////////////////////////////////////////////////////////////////////////
//  netRouteErrMsg
//////////////////////////////////////////////////////////////////////////
class netRouteErrMsg
{
public:
	NET_MESSAGE_DECL(netRouteErrMsg, NET_ROUTE_ERR_MSG);

	enum Flags
	{
		RERR_FLAG_NO_DELETE = 0x01,
		RERR_NUM_FLAGS = 1,
	};

	CompileTimeAssert(RERR_NUM_FLAGS < (sizeof(u8) << 3));

	static const unsigned MAX_UNREACHABLE_DESTINATIONS = netConnectionRouter::MAX_NUM_PEERS;

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													 sizeof(u8) +															// m_Flags
													 sizeof(u8) +															// m_DestCount
													 (netConnectionRouter::SequenceNumber::MAX_EXPORTED_SIZE_IN_BYTES *		// m_UnreachableDestSeq
													  MAX_UNREACHABLE_DESTINATIONS)  +										
													 (netPeerId::MAX_EXPORTED_SIZE_IN_BYTES *								// m_UnreachableDestId
													  MAX_UNREACHABLE_DESTINATIONS);								

	CompileTimeAssert(MAX_EXPORT_SIZE_IN_BYTES < netMessage::MAX_BYTE_SIZEOF_MESSAGE);

	netRouteErrMsg()
	{

	}

	netRouteErrMsg(const u8 flags,
					 const u8 destCount,
					 const netPeerId* unreachableDestIds,
					 const netConnectionRouter::SequenceNumber* unreachableDestSeqs)
	{
		m_Flags = flags;
		m_DestCount = destCount;
		if(!netVerify(m_DestCount <= MAX_UNREACHABLE_DESTINATIONS))
		{
			m_DestCount = MAX_UNREACHABLE_DESTINATIONS;
		}

		for(unsigned i = 0; i < m_DestCount; ++i)
		{
			m_UnreachableDestIds[i] = unreachableDestIds[i];
			m_UnreachableDestSeqs[i] = unreachableDestSeqs[i];
		}
	}

	NET_MESSAGE_SER(bb, msg)
	{
		rtry
		{
			rverifyall(bb.SerUns(msg.m_Flags, sizeof(msg.m_Flags) << 3));
			rverifyall(bb.SerArraySize(msg.m_DestCount, msg.m_UnreachableDestSeqs ASSERT_ONLY(, "m_DestCount")));

			for(unsigned i = 0; (i < msg.m_DestCount) && (i < COUNTOF(msg.m_UnreachableDestSeqs)); ++i)
			{
				rverifyall(bb.SerUser(msg.m_UnreachableDestSeqs[i]));
			}

			for(unsigned i = 0; (i < msg.m_DestCount) && (i < COUNTOF(msg.m_UnreachableDestIds)); ++i)
			{
				rverifyall(bb.SerUser(msg.m_UnreachableDestIds[i]));
			}

			return true;
		}
		rcatchall
		{
			return false;
		}
	}

#if !__NO_OUTPUT
	void Dump() const
	{
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		netDebug1("netRouteErrMsg: flags:%u, num unreachable destinations: %u",
					(unsigned)m_Flags,
					(unsigned)m_DestCount);

		for(unsigned i = 0; (i < m_DestCount) && (i < MAX_UNREACHABLE_DESTINATIONS); ++i)
		{
			netDebug1("    Unreachable Peer ID: %s, Dest Seq: %u",
						m_UnreachableDestIds[i].ToHexString(peerIdHexString),
						m_UnreachableDestSeqs[i].Get());
		}
	}
#endif

	u8 m_Flags;
	u8 m_DestCount;
	netConnectionRouter::SequenceNumber m_UnreachableDestSeqs[MAX_UNREACHABLE_DESTINATIONS];
	netPeerId m_UnreachableDestIds[MAX_UNREACHABLE_DESTINATIONS];
};

NET_MESSAGE_IMPL(netRouteErrMsg);

//////////////////////////////////////////////////////////////////////////
//  netRouteChangeRequestMsg
//////////////////////////////////////////////////////////////////////////
class netRouteChangeRequestMsg
{
public:
	NET_MESSAGE_DECL(netRouteChangeRequestMsg, NET_ROUTE_CHANGE_REQUEST_MSG);

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER;

	netRouteChangeRequestMsg()
	{

	}

	NET_MESSAGE_SER(UNUSED_PARAM(bb), UNUSED_PARAM(msg))
	{
		return true;
	}

#if !__NO_OUTPUT
	void Dump() const
	{
	}
#endif

};

NET_MESSAGE_IMPL(netRouteChangeRequestMsg);

//////////////////////////////////////////////////////////////////////////
//  netRouteChangeReplyMsg
//////////////////////////////////////////////////////////////////////////
class netRouteChangeReplyMsg
{
public:
	NET_MESSAGE_DECL(netRouteChangeReplyMsg, NET_ROUTE_CHANGE_REPLY_MSG);

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER;

	netRouteChangeReplyMsg()
	{

	}

	NET_MESSAGE_SER(UNUSED_PARAM(bb), UNUSED_PARAM(msg))
	{
		return true;
	}

#if !__NO_OUTPUT
	void Dump() const
	{
	}
#endif
};

NET_MESSAGE_IMPL(netRouteChangeReplyMsg);

//////////////////////////////////////////////////////////////////////////
//  NodeId
//////////////////////////////////////////////////////////////////////////
netConnectionRouter::NodeId::NodeId()
{
	Clear();
}

netConnectionRouter::NodeId::NodeId(const EndpointId endpointId,
									const netPeerId& peerId)
{
	Clear();

	m_EndpointId = endpointId;
	m_PeerId = peerId;
}

netConnectionRouter::NodeId::~NodeId()
{
	Clear();
}

void netConnectionRouter::NodeId::Clear()
{
	m_EndpointId = NET_INVALID_ENDPOINT_ID;

	m_PeerId.Clear();
}

const netPeerId& netConnectionRouter::NodeId::GetPeerId() const
{
	return m_PeerId;
}

const EndpointId netConnectionRouter::NodeId::GetEndpointId() const
{
	return m_EndpointId;
}

bool netConnectionRouter::NodeId::operator==(const NodeId& that) const
{
	return this->m_EndpointId == that.m_EndpointId;
}

bool netConnectionRouter::NodeId::operator!=(const NodeId& that) const
{
	return !this->operator==(that);
}

bool netConnectionRouter::NodeId::operator<(const NodeId& that) const
{
	return this->m_EndpointId < that.m_EndpointId;
}

#if !__NO_OUTPUT
const char* netConnectionRouter::Node::GetRouteTypeString(const Route route)
{
	switch(route)
	{
	case Node::Direct: return "Direct"; break;
	case Node::PeerRelay: return "PeerRelay"; break;
	case Node::RelayServer: return "RelayServer"; break;
	default: return "Unknown route type"; break;
	}
}
#endif

//////////////////////////////////////////////////////////////////////////
//  PrecursorNode
//////////////////////////////////////////////////////////////////////////
netConnectionRouter::PrecursorNode::PrecursorNode(const netPeerId& peerId)
: m_PeerId(peerId)
{

}

netConnectionRouter::PrecursorNode::~PrecursorNode()
{
	m_PeerId.Clear();
}

const netPeerId& netConnectionRouter::PrecursorNode::GetPeerId() const
{
	return m_PeerId;
}

//////////////////////////////////////////////////////////////////////////
//  Node
//////////////////////////////////////////////////////////////////////////
netConnectionRouter::Node::Node(sysMemAllocator* allocator, const EndpointId endpointId, const netPeerId& peerId, const Route route)
: m_NodeId(endpointId, peerId)
, m_Route(None)
, m_DestSeq()
, m_HopCount(0)
, m_NextDiscoveryTtl(TTL_START)
, m_NextDiscoveryTime(0)
, m_NumDiscoveryAttempts(0)
, m_LastReceiveTime(0)
, m_LastDirectReceiveTime(0)
, m_LastAddressAssignmentTime(0)
, m_Allocator(allocator)
, m_DestSeqValid(false)
, m_Active(false)
, m_BlacklistExpirationTime(0)
, m_BlacklistCounter(0)
, m_ChangeRouteSentTime(0)
, m_ChangeRouteReceivedReplyTime(0)
{
	SetRoute(route);
}

netConnectionRouter::Node::~Node()
{
	ClearPrecursorList();
	m_NodeId.Clear();
	m_Route = Node::None;
	m_NextHopPeerId.Clear();
	m_HopCount = 0;
	m_NextDiscoveryTtl = TTL_START;
	m_NextDiscoveryTime = 0;
	m_NumDiscoveryAttempts = 0;
	m_LastReceiveTime = 0;
	m_LastDirectReceiveTime = 0;
	m_LastAddressAssignmentTime = 0;
	m_DestSeqValid = false;
	m_Active = false;;
	m_Allocator = nullptr;
	m_BlacklistExpirationTime = 0;
	m_BlacklistCounter = 0;
	m_ChangeRouteSentTime = 0;
	m_ChangeRouteReceivedReplyTime = 0;
}

void netConnectionRouter::Node::ClearPrecursorList()
{
	while(!m_PrecursorList.empty() && netVerify(m_Allocator))
	{
		PrecursorNode* prenode = m_PrecursorList.front();
		m_PrecursorList.pop_front();

		if(netVerify(prenode))
		{
			prenode->~PrecursorNode();
			Free(prenode);
		}
	}

	m_PrecursorList.clear();
}

netConnectionRouter::PrecursorList& netConnectionRouter::Node::GetPrecursorList()
{
	return m_PrecursorList;
}

void netConnectionRouter::Node::SetRoute(const Route route)
{
	m_Route = route;
}

netConnectionRouter::Node::Route netConnectionRouter::Node::GetRoute() const
{
	return m_Route;
}

void netConnectionRouter::Node::SetNextHopPeerId(const netPeerId& nextHopPeerId)
{
	m_NextHopPeerId = nextHopPeerId;
}

const rage::netPeerId& netConnectionRouter::Node::GetNextHopPeerId() const
{
	return m_NextHopPeerId;
}

const netPeerId& netConnectionRouter::Node::GetPeerId() const
{
	return GetNodeId().GetPeerId();
}

const EndpointId netConnectionRouter::Node::GetEndpointId() const
{
	return GetNodeId().GetEndpointId();
}

const netConnectionRouter::NodeId& netConnectionRouter::Node::GetNodeId() const
{
	return m_NodeId;
}

void netConnectionRouter::Node::SetNextDiscoveryTtl(const u8 ttl)
{
	netAssert(ttl <= sm_MaxHopCount);
	m_NextDiscoveryTtl = (u8)Min(ttl, sm_MaxHopCount);
}

u8 netConnectionRouter::Node::GetNextDiscoveryTtl() const
{
	return m_NextDiscoveryTtl;
}

void netConnectionRouter::Node::SetNextDiscoveryTime(const unsigned time)
{
	// randomize the time slightly (15% earlier or later) to avoid flooding all requests at once
	unsigned curTime = sysTimer::GetSystemMsTime();
	unsigned timeUntilNextDiscovery = time - curTime;
	unsigned adjustment = unsigned(timeUntilNextDiscovery * 0.15f);
	unsigned adjustedTime = sm_Rng.GetRanged(time - adjustment, time + adjustment);

#if !__NO_OUTPUT
	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	netDebug1("SetNextDiscoveryTime : next route discovery for peerId: %s in %u ms",
			  GetPeerId().ToHexString(peerIdHexString), adjustedTime - sysTimer::GetSystemMsTime());
#endif

	m_NextDiscoveryTime = adjustedTime;
}

unsigned netConnectionRouter::Node::GetNextDiscoveryTime() const
{
	return m_NextDiscoveryTime;
}

void netConnectionRouter::Node::IncDiscoveryAttempts()
{
	if(m_NumDiscoveryAttempts < UCHAR_MAX)
	{
		m_NumDiscoveryAttempts++;
	}
}

void netConnectionRouter::Node::ResetNumDiscoveryAttempts()
{
	m_NumDiscoveryAttempts = 0;
}

unsigned netConnectionRouter::Node::GetNumDiscoveryAttempts() const
{
	return (unsigned)m_NumDiscoveryAttempts;
}

void netConnectionRouter::Node::SetLastReceiveTime(const unsigned time, const netAddress& sender)
{
#if !__NO_OUTPUT
	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	if(m_LastReceiveTime == 0)
	{
		netDebug("SetLastReceiveTime :: first packet received from %s at time %u", GetPeerId().ToHexString(peerIdHexString), time);
	}
#if 0
	// spammy but useful for debugging
	else
	{
		netDebug("SetLastReceiveTime :: received from %s at time %u", GetPeerId().ToHexString(peerIdHexString), time);
	}
#endif
#endif

	if(!IsRouteActive() && (GetRoute() == Node::Direct) && sender.IsDirectAddr())
	{
		netAssert(GetHopCount() == 1);
#if !__NO_OUTPUT
		netAssert(time >= m_LastReceiveTime);
		const unsigned elapsed = time - m_LastReceiveTime;
		netDebug("SetLastReceiveTime :: promoting inactive direct route to active. Time since last receive: %u ms.", elapsed);
#endif
		SetRouteActive(true);

#if !__NO_OUTPUT
		DumpRoutingEntry();
#endif
	}

	if(sender.IsDirectAddr())
	{
		SetLastDirectReceiveTime(time);
	}

	m_LastReceiveTime = time;
}

unsigned netConnectionRouter::Node::GetLastReceiveTime() const
{
	return m_LastReceiveTime;
}

void netConnectionRouter::Node::SetLastDirectReceiveTime(const unsigned time)
{
	m_LastDirectReceiveTime = time;
}

unsigned netConnectionRouter::Node::GetLastDirectReceiveTime() const
{
	return m_LastDirectReceiveTime;
}

void netConnectionRouter::Node::SetLastAddressAssignmentTime(const unsigned time)
{
	m_LastAddressAssignmentTime = time;
}

unsigned netConnectionRouter::Node::GetLastAddressAssignmentTime() const
{
	return m_LastAddressAssignmentTime;
}

void netConnectionRouter::Node::SetChangeRouteSentTime(const unsigned time)
{
	m_ChangeRouteSentTime = time;
}

unsigned netConnectionRouter::Node::GetChangeRouteSentTime() const
{
	return m_ChangeRouteSentTime;
}

void netConnectionRouter::Node::SetChangeRouteReceivedReplyTime(const unsigned time)
{
	m_ChangeRouteReceivedReplyTime = time;
}

unsigned netConnectionRouter::Node::GetChangeRouteReceivedReplyTime() const
{
	return m_ChangeRouteReceivedReplyTime;
}

void netConnectionRouter::Node::SetHopCount(const u8 hopCount)
{
	netAssert(hopCount <= sm_MaxHopCount);
	m_HopCount = hopCount;
}

u8 netConnectionRouter::Node::GetHopCount() const
{
	return m_HopCount;
}

const netConnectionRouter::SequenceNumber& netConnectionRouter::Node::GetDestSeq() const
{
	return m_DestSeq;
}

void netConnectionRouter::Node::MaxDestSeq(const netConnectionRouter::SequenceNumber& seq)
{
	m_DestSeq.Max(seq);
}

void netConnectionRouter::Node::IncDestSeq()
{
	m_DestSeq.Inc();
}

void netConnectionRouter::Node::SetDestSeqValid(const bool valid)
{
	m_DestSeqValid = (u8)valid;
}

bool netConnectionRouter::Node::IsDestSeqValid() const
{
	return m_DestSeqValid != 0;
}

void netConnectionRouter::Node::SetRouteActive(const bool active)
{
	m_Active = active;
}

bool netConnectionRouter::Node::IsRouteActive() const
{
#if USE_NET_ASSERTS
	if(m_Active != 0)
	{
		netAssert(GetNextHopPeerId().IsValid());
		netAssert(GetHopCount() <= sm_MaxHopCount);
	}
#endif

	return m_Active != 0;
}

void* netConnectionRouter::Node::Alloc(const unsigned size) const
{
	if(!netVerify(m_Allocator))
	{
		return nullptr;
	}

	void* ptr = m_Allocator->RAGE_LOG_ALLOCATE(size, 0);

#if !__NO_OUTPUT
	if(ptr)
	{
		sm_MemoryUsed += m_Allocator->GetSizeWithOverhead(ptr);
		sm_MaxMemoryUsed = Max(sm_MemoryUsed, sm_MaxMemoryUsed);
	}
#endif

	return ptr;
}

void
netConnectionRouter::Node::Free(void* ptr) const
{
	if(!netVerify(m_Allocator) || !netVerify(ptr))
	{
		return;
	}

#if !__NO_OUTPUT
	sm_MemoryUsed -= m_Allocator->GetSizeWithOverhead(ptr);
	sm_MaxMemoryUsed = Max(sm_MemoryUsed, sm_MaxMemoryUsed);
#endif

	m_Allocator->Free(ptr);
}

void netConnectionRouter::Node::AddPrecursor(const netConnectionRouter::Node* node)
{
	rtry
	{
		rverifyall(m_Allocator);
		rverifyall(node);
		rcheckall(this != node);

		const netPeerId& peerId = node->GetPeerId();
		rverifyall(peerId.IsValid());

		// only add it once
		PrecursorList::iterator it = m_PrecursorList.begin();
		PrecursorList::const_iterator stop = m_PrecursorList.end();
		for(; stop != it; ++it)
		{
			PrecursorNode* prenode = *it;
			if(prenode->GetPeerId() == peerId)
			{
				// this node already has this precursor
				return;
			}
		}

#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		char peerIdHexString2[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
#endif

		netDebug("Adding %s as a precursor to %s", peerId.ToHexString(peerIdHexString), this->GetPeerId().ToHexString(peerIdHexString2));

		PrecursorNode* prenode = (PrecursorNode*)this->Alloc(sizeof(PrecursorNode));
		rverifyall(prenode);

		new(prenode) PrecursorNode(peerId);
		m_PrecursorList.push_back(prenode);
	}
	rcatchall
	{

	}
}

void netConnectionRouter::Node::RemovePrecursor(const Node* nodeToRemove)
{
	rtry
	{
		rverifyall(nodeToRemove);

		const netPeerId& peerId = nodeToRemove->GetPeerId();
		rverifyall(peerId.IsValid());

		PrecursorList::iterator it = m_PrecursorList.begin();
		PrecursorList::const_iterator stop = m_PrecursorList.end();
		for(; stop != it; ++it)
		{
			PrecursorNode* prenode = *it;
			if(prenode->GetPeerId() == peerId)
			{
				m_PrecursorList.erase(it);
				prenode->~PrecursorNode();
				Free(prenode);
				return;
			}
		}
	}
	rcatchall
	{

	}
}

void netConnectionRouter::Node::SetBlacklistExpirationTime(const unsigned expirationTime)
{
	m_BlacklistExpirationTime = expirationTime;
}

unsigned netConnectionRouter::Node::GetBlacklistExpirationTime() const
{
	return m_BlacklistExpirationTime;
}

bool netConnectionRouter::Node::IsBlacklisted() const
{
	return m_BlacklistExpirationTime > 0;
}

u8 netConnectionRouter::Node::GetBlacklistCount() const
{
	return m_BlacklistCounter;
}

void netConnectionRouter::Node::IncBlacklistCount()
{
	// this is used as an exponent when calculating the blacklist expiration time
	const unsigned MAX_BLACKLIST_COUNT = 10;
	if(m_BlacklistCounter < MAX_BLACKLIST_COUNT)
	{
		++m_BlacklistCounter;
	}
}

#if !__NO_OUTPUT
void netConnectionRouter::Node::DumpRoutingEntry() const
{
	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	char peerIdHexString2[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	char precursors[netPeerId::TO_HEX_STRING_BUFFER_SIZE * 32] = {0};

	PrecursorList::const_iterator it = m_PrecursorList.begin();
	PrecursorList::const_iterator stop = m_PrecursorList.end();
	for(; stop != it; ++it)
	{
		const PrecursorNode* prenode = *it;
		safecatf(precursors, "%s,", prenode->GetPeerId().ToHexString(peerIdHexString));
	}
	unsigned len = (unsigned)strlen(precursors);
	if(len > 0)
	{
		precursors[len - 1] = '\0';
	}
	else
	{
		safecpy(precursors, "none");
	}
	
	netAssert(((this->GetHopCount() == 1) && (this->GetRoute() == Node::Direct)) || 
			  ((this->GetHopCount() >= 2) && (this->GetRoute() == Node::PeerRelay)) ||
			  (this->GetRoute() == Node::RelayServer));

	netAssert(this->GetHopCount() <= sm_MaxHopCount);
	netAssert((this->GetPeerId() != this->GetNextHopPeerId()) || ((this->GetHopCount() == 1) && (this->GetRoute() == Node::Direct)));

	netDebug1("    dest=%s,destSeq=%u%s,nextHop=%s,hopCount=%u,route-state=%s,route-type=%s,precursors:%s",
			this->GetPeerId().ToHexString(peerIdHexString),
			this->GetDestSeq().Get(),
			this->IsDestSeqValid() ? "" : "(invalid)",
			this->GetNextHopPeerId().IsValid() ? this->GetNextHopPeerId().ToHexString(peerIdHexString2) : "(invalid)",
			(unsigned)this->GetHopCount(),
			this->IsRouteActive() ? "active" : "inactive",
			Node::GetRouteTypeString(this->GetRoute()),
			precursors);
}
#endif

//////////////////////////////////////////////////////////////////////////
//  netConnectionRouter
//////////////////////////////////////////////////////////////////////////
bool netConnectionRouter::sm_AllowConnectionRouting = true;
bool netConnectionRouter::sm_AllowPeerRelay = true;
u8 netConnectionRouter::sm_MaxHopCount = DEFAULT_MAX_HOP_COUNT;
unsigned netConnectionRouter::sm_MaxRelayedCxns = DEFAULT_MAX_RELAYED_CXNS;
unsigned netConnectionRouter::sm_EmergencyRelayFallbackTimeMs = DEFAULT_EMERGENCY_RELAY_FALLBACK_TIME_MS;
unsigned netConnectionRouter::sm_EmergencyDirectReceiveTimeoutMs = DEFAULT_EMERGENCY_DIRECT_RECEIVE_TIMEOUT_MS;
unsigned netConnectionRouter::sm_BrokenLinkDetectionTimeMs = BROKEN_LINK_DETECTION_TIME_MS;
unsigned netConnectionRouter::sm_RouteChangeWaitTimeMs = DEFAULT_ROUTE_CHANGE_WAIT_TIME_MS;
bool netConnectionRouter::sm_EmergencyRelayFallbackTimeoutOverridden = false;
bool netConnectionRouter::sm_EmergencyDirectReceiveTimeoutOverridden = false;
netRandom netConnectionRouter::sm_Rng;

netConnectionRouter::netConnectionRouter(netConnectionManager* cxnMgr)
: m_CxnMgr(cxnMgr)
, m_LastUpdateTime(0)
, m_RouteRequestId()
, m_LocalSeq()
, m_Allocator(nullptr)
, m_NumForwardedConnections(0)
, m_MaxForwardedConnections(0)
, m_RouteReplyId(0)
{
	sm_Rng.SetFullSeed((u64)sysTimer::GetTicks());
}

netConnectionRouter::~netConnectionRouter()
{
	Shutdown();
}

void*
netConnectionRouter::Alloc(const unsigned size) const
{
	if(!netVerify(m_Allocator))
	{
		return nullptr;
	}

	void* ptr = m_Allocator->RAGE_LOG_ALLOCATE(size, 0);

#if !__NO_OUTPUT
	if(ptr)
	{
		sm_MemoryUsed += m_Allocator->GetSizeWithOverhead(ptr);
		sm_MaxMemoryUsed = Max(sm_MemoryUsed, sm_MaxMemoryUsed);
	}
#endif

	return ptr;
}

void
netConnectionRouter::Free(void* ptr) const
{
	if(!netVerify(m_Allocator) || !netVerify(ptr))
	{
		return;
	}

#if !__NO_OUTPUT
	sm_MemoryUsed -= m_Allocator->GetSizeWithOverhead(ptr);
	sm_MaxMemoryUsed = Max(sm_MemoryUsed, sm_MaxMemoryUsed);
#endif

	m_Allocator->Free(ptr);
}

void netConnectionRouter::UpdateLastReceiveTime(const netPeerId& peerId, const netAddress& sender, const unsigned curTime)
{
	PROFILE

	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	Node* node = GetNodeByPeerId(peerId);
	if(node)
	{
		node->SetLastReceiveTime(curTime, sender);
	}
}

bool netConnectionRouter::TrackRoutePair(const netPeerId& srcPeerId, const netPeerId& destPeerId, const bool established)
{
	ASSERT_CXNMGR_CS_LOCKED;

	RoutePair::Data* data = nullptr;

	rtry
	{
#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		char peerIdHexString2[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
#endif

		rcheckall(srcPeerId != m_LocalPeerId);

		const unsigned numForwardedConnections = m_NumForwardedConnections;
		const unsigned lastForwardTime = sysTimer::GetSystemMsTime();
				
		RoutePair pair(srcPeerId, destPeerId);
		RoutePairsMap::iterator it = m_RoutePairs.find(pair.m_Key);
		if(m_RoutePairs.end() != it)
		{
			RoutePair::Data* entry = it->second;

			// already exists, update the time
			entry->m_LastForwardTime = lastForwardTime;
			if(established && (entry->m_Established == 0))
			{
				m_NumForwardedConnections++;
				m_MaxForwardedConnections = Max(m_NumForwardedConnections, m_MaxForwardedConnections);
				entry->m_Established = 1;
			}
		}
		else
		{
			// doesn't exist. Allocate a new RoutePair, growing the pool if necessary.
			if(m_RoutePairsPool.empty())
			{
				const unsigned numPairs = m_RoutePairsPile.Size();
				rverifyall(m_RoutePairsPile.Resize(numPairs + 1));
				data = &m_RoutePairsPile[numPairs];
			}
			else
			{
				data = m_RoutePairsPool.front();
				m_RoutePairsPool.pop_front();
			}

			rverifyall(data);
			new (data) RoutePair::Data(lastForwardTime, established);

			std::pair<RoutePairsMap::iterator, bool> mapInsertResult = m_RoutePairs.insert(pair.m_Key, data);
			rverify(mapInsertResult.second, catchall, netError("Failed to insert RoutePair into RoutePairsMap"));

			if(established)
			{
				m_NumForwardedConnections++;
				m_MaxForwardedConnections = Max(m_NumForwardedConnections, m_MaxForwardedConnections);
			}
			else
			{
				netDebug1("Reserved forwarding route from %s to %s. Num used slots: %u.",
						srcPeerId.ToHexString(peerIdHexString),
						destPeerId.ToHexString(peerIdHexString2),
						(unsigned)m_RoutePairs.size());
			}
		}

		if(numForwardedConnections != m_NumForwardedConnections)
		{
			netDebug1("Started forwarding packets from %s to %s. Now relaying %u connections (Max was: %u). Num used slots: %u.",
					srcPeerId.ToHexString(peerIdHexString),
					destPeerId.ToHexString(peerIdHexString2),
					m_NumForwardedConnections,
					m_MaxForwardedConnections,
					(unsigned)m_RoutePairs.size());

			Node* nextHopNodeTowardsSource = GetNextHopNode(srcPeerId);
			Node* nextHopNodeTowardsDest = GetNextHopNode(destPeerId);

			if(nextHopNodeTowardsSource && nextHopNodeTowardsDest)
			{
				nextHopNodeTowardsDest->AddPrecursor(nextHopNodeTowardsSource);
			}
		}

		return true;
	}
	rcatchall
	{
		if(data)
		{
			m_RoutePairsPool.push_back(data);
		}

		return false;
	}
}

bool netConnectionRouter::PrepareToForward(netPeerRelayPacket* relayPkt, netAddress* nextHopAddr)
{
	if(!IsPeerRelayEnabled())
	{
		return false;
	}

	if(m_CxnMgr == nullptr)
	{
		return false;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	Node* destNode = nullptr;

	rtry
	{
		rverifyall(nextHopAddr);
		rverifyall(relayPkt->IsRelayPacket() && relayPkt->IsP2pForward());

		const netPeerId& destPeerId = relayPkt->GetDstPeerId();
		const netPeerId& srcPeerId = relayPkt->GetSrcPeerId();

		rverify(srcPeerId.IsValid(),
			catchall,
			netError("PrepareToForward :: Error retrieving source peer id"));

		rverify(destPeerId.IsValid(),
			catchall,
			netError("PrepareToForward :: Error retrieving destination peer id"));

		// attempt to forward even if TrackRoutePair fails, otherwise remote player packets won't 
		// reach their destination and they could keep rediscovering the local peer as a relayer.
		TrackRoutePair(srcPeerId, destPeerId, true);

#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
#endif

		destNode = this->GetNodeByPeerId(destPeerId);
		rcheck(destNode, catchall, netDebug("PrepareToForward :: no route to destination peerId %s", destPeerId.ToHexString(peerIdHexString)));
		rcheck(destNode->IsRouteActive(), sendRouteError, netDebug("PrepareToForward :: route to destination peerId %s is invalid", destPeerId.ToHexString(peerIdHexString)));

		Node* nextHopNode = this->GetNextHopNode(destPeerId);
		rverify(nextHopNode, sendRouteError, netError("PrepareToForward :: no next hop node to destination peerId %s", destPeerId.ToHexString(peerIdHexString)));
		rcheck(nextHopNode->IsRouteActive(), sendRouteError, netWarning("PrepareToForward :: route to next hop node to destination peerId %s is invalid", destPeerId.ToHexString(peerIdHexString)));
		rverify(nextHopNode->GetRoute() == Node::Direct, sendRouteError, netError("PrepareToForward :: route to next hop node to destination peerId %s is not direct", destPeerId.ToHexString(peerIdHexString)));

		const netPeerId& nextHopPeerId = nextHopNode->GetPeerId();
		rverify(nextHopPeerId.IsValid(), sendRouteError, netError("PrepareToForward :: next hop node has invalid peerId for desination %s", destPeerId.ToHexString(peerIdHexString)));

		bool success = m_CxnMgr->GetAddrByPeerId(nextHopPeerId, nextHopAddr);
		rcheck(success && nextHopAddr->IsValid(), sendRouteError, netDebug("PrepareToForward :: no address for next hop peerId %s", destPeerId.ToHexString(peerIdHexString)));
		rverify(nextHopAddr->IsDirectAddr(), sendRouteError, netDebug("PrepareToForward :: address of next hop node to destination peerId %s is not direct", destPeerId.ToHexString(peerIdHexString)));

		if(nextHopPeerId == destPeerId)
		{
			// the next hop is the final destination peer, switch forward flag to terminus flag
			relayPkt->ResetP2pTerminusHeader();
		}

		return true;
	}
	rcatch(sendRouteError)
	{
		/*
			Note: this is 6.11 case (ii)

			(ii) if it gets a data packet destined to a node for which it
			does not have an active route and is not repairing (if
			using local repair)

			For case (ii), there is only one unreachable destination, which is
			the destination of the data packet that cannot be delivered.

			[NS] Local repair is not currently supported
		*/

		rverify(destNode, catchall, netError("PrepareToForward :: can't send route err for unknown destination node"));

		const u8 numUnreachableDestinations = 1;
		Node* unreachableDestinations[1] = {destNode};
		ProcessUnreachableDestinations(unreachableDestinations, numUnreachableDestinations);

		return false;
	}
	rcatchall
	{
		return false;
	}
}

bool netConnectionRouter::PrepareToReceiveTerminus(netPeerRelayPacket* relayPkt, const netSocketAddress& sender, netAddress* reverseNextHopAddr) const
{
	if(m_CxnMgr == nullptr)
	{
		return false;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	rtry
	{
#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
#endif

		rverifyall(reverseNextHopAddr);
		rverifyall(relayPkt->IsRelayPacket() && relayPkt->IsP2pTerminus());

		netPeerId srcId = relayPkt->GetSrcPeerId();
		rverify(srcId.IsValid(),
			catchall,
			netError("Error retrieving source peer id"));

		bool success = reverseNextHopAddr->Init(sender, srcId);
		rcheck(success && reverseNextHopAddr->IsValid(), catchall, netDebug("PrepareToReceiveTerminus :: no address for next hop towards source peerId %s", srcId.ToHexString(peerIdHexString)));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

u8 netConnectionRouter::GetHopCount(const EndpointId epId) const
{
	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	NodeList::const_iterator it = m_Nodes.begin();
	NodeList::const_iterator stop = m_Nodes.end();
	for(; stop != it; ++it)
	{
		const Node* node = *it;

		if(epId == node->GetEndpointId())
		{
			if(node->GetRoute() == Node::Direct)
			{
				return 1;
			}
			else if(node->GetRoute() == Node::RelayServer)
			{
				return 2;
			}
			else if(node->GetRoute() == Node::PeerRelay)
			{
				return node->GetHopCount();
			}
		}
	}

	return 0;
}

unsigned netConnectionRouter::GetNumForwardedConnections() const
{
	return m_NumForwardedConnections;
}

void netConnectionRouter::SetAllowConnectionRouting(const bool allowConnectionRouting)
{
	if(sm_AllowConnectionRouting != allowConnectionRouting)
	{
		netDebug("SetAllowConnectionRouting :: %s", allowConnectionRouting ? "true" : "false");
		sm_AllowConnectionRouting = allowConnectionRouting;
	}
}

void netConnectionRouter::SetAllowPeerRelay(const bool allowPeerRelay)
{
	if(sm_AllowPeerRelay != allowPeerRelay)
	{
		netDebug("SetAllowPeerRelay :: %s", allowPeerRelay ? "true" : "false");
		sm_AllowPeerRelay = allowPeerRelay;
	}
}

void netConnectionRouter::SetMaxHopCount(const unsigned maxHopCount)
{
	if(sm_MaxHopCount != maxHopCount)
	{
		netDebug("SetMaxHopCount :: %u", maxHopCount);
		sm_MaxHopCount = (u8)Min(maxHopCount, (unsigned)UINT8_MAX);
	}
}

void netConnectionRouter::SetMaxRelayedCxns(const unsigned maxRelayedCxns)
{
	if(sm_MaxRelayedCxns != maxRelayedCxns)
	{
		netDebug("SetMaxRelayedCxns :: %u", maxRelayedCxns);
		sm_MaxRelayedCxns = maxRelayedCxns;
	}
}

void netConnectionRouter::SetEmergencyRelayFallbackTimeMs(const unsigned emergencyRelayFallbackTimeMs)
{
	if(sm_EmergencyRelayFallbackTimeMs != emergencyRelayFallbackTimeMs)
	{
		netDebug("SetEmergencyRelayFallbackTimeMs :: %u", emergencyRelayFallbackTimeMs);
		sm_EmergencyRelayFallbackTimeMs = emergencyRelayFallbackTimeMs;
	}

	sm_EmergencyRelayFallbackTimeoutOverridden = true;
}

void netConnectionRouter::SetEmergencyDirectReceiveTimeoutMs(const unsigned emergencyDirectReceiveTimeoutMs)
{
	if(sm_EmergencyDirectReceiveTimeoutMs != emergencyDirectReceiveTimeoutMs)
	{
		netDebug("SetEmergencyDirectReceiveTimeoutMs :: %u", emergencyDirectReceiveTimeoutMs);
		sm_EmergencyDirectReceiveTimeoutMs = emergencyDirectReceiveTimeoutMs;
	}

	sm_EmergencyDirectReceiveTimeoutOverridden = true;
}

void netConnectionRouter::SetBrokenLinkDetectionTimeMs(const unsigned brokenLinkDetectionTimeMs)
{
	if(sm_BrokenLinkDetectionTimeMs != brokenLinkDetectionTimeMs)
	{
		netDebug("SetBrokenLinkDetectionTimeMs :: %u", brokenLinkDetectionTimeMs);
		sm_BrokenLinkDetectionTimeMs = brokenLinkDetectionTimeMs;
	}
}

void netConnectionRouter::SetRouteChangeWaitTimeMs(const unsigned routeChangeWaitTimeMs)
{
	if(sm_RouteChangeWaitTimeMs != routeChangeWaitTimeMs)
	{
		netDebug("SetRouteChangeWaitTimeMs :: %u", routeChangeWaitTimeMs);
		sm_RouteChangeWaitTimeMs = routeChangeWaitTimeMs;
	}
}

void netConnectionRouter::Init(sysMemAllocator* allocator)
{
	if(m_CxnMgr == nullptr)
	{
		return;
	}

	sm_Rng.SetRandomSeed();

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	unsigned maxHops = DEFAULT_MAX_HOP_COUNT;
	if(PARAM_netpeerrelaymaxhops.Get(maxHops))
	{
		netDebug("Setting max hops from -netpeerrelaymaxhops command line");
		SetMaxHopCount(maxHops);
	}

	unsigned maxRelayedCxns = DEFAULT_MAX_RELAYED_CXNS;
	if(PARAM_netpeerrelaymaxrelayedcxns.Get(maxRelayedCxns))
	{
		netDebug("Setting max number of relayed connections from -netpeerrelaymaxrelayedcxns command line");
		SetMaxRelayedCxns(maxRelayedCxns);
	}

	m_Allocator = allocator;

	netPeerId::GetLocalPeerId(m_LocalPeerId);

	m_RoutePairsPile.Init(m_Allocator);
	m_RoutePairsPile.Resize(sm_MaxRelayedCxns);

	for(unsigned i = 0; i < sm_MaxRelayedCxns; ++i)
	{
		m_RoutePairsPool.push_back(&m_RoutePairsPile[i]);
	}

	m_CxnMgr->Subscribe(&m_EventSubscriber);
}

void netConnectionRouter::Update()
{
	PROFILE

	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	rtry
	{
		if(!IsConnectionRoutingEnabled())
		{
			if(m_CxnMgr && m_EventSubscriber.IsSubscribed())
			{
				m_CxnMgr->Unsubscribe(&m_EventSubscriber);
			}

			return;
		}

		const unsigned curTime = sysTimer::GetSystemMsTime();

		netAssert(curTime >= m_LastUpdateTime);
		const unsigned elapsed = curTime - m_LastUpdateTime;

		rcheckall(elapsed > UPDATE_INTERVAL_MS);

		m_LastUpdateTime = curTime;

		UpdateRouteRequestBlacklist();
		UpdatePeerBlacklist();
		UpdateRouteDiscovery();
		UpdateRouteMaintenance();
		UpdateProcessReceivedPackets();
		UpdateRoutePairs();
		OUTPUT_ONLY(ReportMemory());
	}
	rcatchall
	{
	}
}

void netConnectionRouter::Shutdown()
{
	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	while(!m_RouteRequestBlacklist.empty() && netVerify(m_Allocator))
	{
		RouteRequestId* rqst = m_RouteRequestBlacklist.front();
		m_RouteRequestBlacklist.pop_front();

		if(netVerify(rqst))
		{
			rqst->~RouteRequestId();
			Free(rqst);
		}
	}

	m_RouteRequestBlacklist.clear();

	m_NodesByPeerId.clear();

	while(!m_Nodes.empty() && netVerify(m_Allocator))
	{
		Node* node = m_Nodes.front();
		m_Nodes.pop_front();

		if(netVerify(node))
		{
			node->~Node();
			Free(node);
		}
	}

	m_Nodes.clear();

	if(m_CxnMgr && m_EventSubscriber.IsSubscribed())
	{
		m_CxnMgr->Unsubscribe(&m_EventSubscriber);
		m_CxnMgr = nullptr;
	}
	
	m_RoutePairs.clear();
	m_RoutePairsPool.clear();
	netAssert(m_RoutePairsPool.empty());
	m_RoutePairsPile.Shutdown();

	m_Allocator = nullptr;
}

bool netConnectionRouter::IsConnectionRoutingEnabled() const
{
	return sm_AllowConnectionRouting && !PARAM_netnocxnrouting.Get();
}

bool netConnectionRouter::IsPeerRelayEnabled() const
{
	return IsConnectionRoutingEnabled() && sm_AllowPeerRelay && !PARAM_netnopeerrelay.Get();
}

void netConnectionRouter::UpdateProcessReceivedPackets()
{
	PROFILE

	ASSERT_CXNMGR_CS_LOCKED;

    if(m_CxnMgr && m_EventSubscriber.IsSubscribed())
    {
        for(netEvent* evt = m_EventSubscriber.NextEvent(); evt; evt = m_EventSubscriber.NextEvent())
        {
			OnCxnEvent(m_CxnMgr, evt);
        }
    }
}

void netConnectionRouter::UpdateRouteDiscovery()
{
	PROFILE

	ASSERT_CXNMGR_CS_LOCKED;

	if(!IsPeerRelayEnabled())
	{
		return;
	}

	const unsigned curTime = sysTimer::GetSystemMsTime();

	// to reduce routing protocol overhead, only one side of each pair of
	// peers will initiate route discovery. Both sides will discover a 
	// route to each other.
	bool higherPeerIdOnly = true;

	NodeList::iterator it = m_Nodes.begin();
	NodeList::const_iterator stop = m_Nodes.end();
	for(; stop != it; ++it)
	{
		Node* node = *it;

		const netPeerId& destPeerId = node->GetPeerId();
		if(higherPeerIdOnly && (m_LocalPeerId < destPeerId))
		{
			continue;
		}

		if((curTime >= node->GetNextDiscoveryTime()) && 
			((node->GetRoute() == Node::RelayServer) ||
			((node->GetRoute() == Node::PeerRelay) && !node->IsRouteActive())))
		{
			/*
				6.4. Controlling Dissemination of Route Request Messages
				To prevent unnecessary network-wide dissemination of RREQs, the
				originating node SHOULD use an expanding ring search technique.  In
				an expanding ring search, the originating node initially uses a TTL =
				TTL_START in the RREQ packet IP header and sets the timeout for
				receiving a RREP to RING_TRAVERSAL_TIME milliseconds.
				If the RREQ times out without a corresponding RREP, the originator
				broadcasts the RREQ again with the TTL incremented by TTL_INCREMENT.
			*/
			const u8 ttl = node->GetNextDiscoveryTtl();
			StartRouteDiscovery(node, ttl);

			if(ttl < sm_MaxHopCount)
			{
				node->SetNextDiscoveryTtl(ttl + TTL_INCREMENT);
				node->SetNextDiscoveryTime(curTime + RING_TRAVERSAL_TIME);
			}
			else
			{
				/*
					6.3. Generating Route Requests
					To reduce congestion in a network, repeated attempts by a source node
					at route discovery for a single destination MUST utilize a binary
					exponential backoff.
				*/
				node->SetNextDiscoveryTtl(TTL_START);

				const unsigned nextDiscoveryTime = curTime + Min(MAX_ROUTE_DISCOVERY_INTERVAL_MS,
																 MIN_ROUTE_DISCOVERY_INTERVAL_MS * (2 << node->GetNumDiscoveryAttempts()));
				
				node->SetNextDiscoveryTime(nextDiscoveryTime);
				node->IncDiscoveryAttempts();
			}		
		}
	}
}

void netConnectionRouter::UpdateDirectRouteMaintenance(Node* node)
{
	PROFILE

	ASSERT_CXNMGR_CS_LOCKED;
	
	const unsigned lastDirectReceiveTime = node->GetLastDirectReceiveTime();
	if(!netVerify(node->GetRoute() == Node::Direct) || (lastDirectReceiveTime == 0))
	{
		return;
	}

	const unsigned curTime = sysTimer::GetSystemMsTime();
	const unsigned timeSinceLastDirectReceive = curTime - lastDirectReceiveTime;

	if((sm_EmergencyDirectReceiveTimeoutMs > 0) && (timeSinceLastDirectReceive > sm_EmergencyDirectReceiveTimeoutMs))
	{
		const unsigned changeRouteSentTime = node->GetChangeRouteSentTime();
		if(changeRouteSentTime == 0)
		{
			SendRouteChangeRequest(node);
		}
		else
		{
			// we have sent a route change request and are waiting for a reply
			const unsigned changeRouteReplyReceivedTime = node->GetChangeRouteReceivedReplyTime();
			if(changeRouteReplyReceivedTime >= changeRouteSentTime)
			{
				const unsigned timeSinceReplyReceived = curTime - changeRouteReplyReceivedTime;
				if(timeSinceReplyReceived > sm_RouteChangeWaitTimeMs)
				{
					// we received a reply (via relay server) to our route change request and waited
					// an additional period of time but still have not directly received from this node.
					// Switch direct route to relay server.
					SwitchDirectRouteToRelayServer(node);
				}
			}
		}
	}
	else
	{
		// we have received directly from the remote peer, clear our change route sent
		// time to ignore any incoming route replies from this node
		node->SetChangeRouteSentTime(0);
		node->SetChangeRouteReceivedReplyTime(0);
	}
}

void netConnectionRouter::UpdateRouteMaintenance()
{
	PROFILE

	ASSERT_CXNMGR_CS_LOCKED;

	const unsigned curTime = sysTimer::GetSystemMsTime();

#if !__NO_OUTPUT
	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	char peerIdHexString2[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
#endif

	NodeList::iterator it = m_Nodes.begin();
	NodeList::const_iterator stop = m_Nodes.end();
	for(; stop != it; ++it)
	{
		Node* node = *it;

		if(node->GetLastReceiveTime() == 0)
		{
			// don't process routes from which we haven't received yet
			continue;
		}

		if(node->GetRoute() == Node::Direct)
		{
			// attempt to repair any broken direct routes
			UpdateDirectRouteMaintenance(node);
		}

		// active routes are candidates for p2p relaying. Detect if any active route has been broken.
		if(node->IsRouteActive() && IsPeerRelayEnabled())
		{
			netAssert(curTime >= node->GetLastReceiveTime());
			const unsigned timeSinceLastReceive = curTime - node->GetLastReceiveTime();
			const unsigned timeSinceLastRouteChange = curTime - node->GetLastAddressAssignmentTime();
			const bool viaPeerRelay = node->GetRoute() == Node::PeerRelay;
			const unsigned ageOfOldestUnackedFrame = viaPeerRelay ? m_CxnMgr->GetAgeOfOldestUnackedFrame(node->GetEndpointId()) : 0;

			if(timeSinceLastReceive > sm_EmergencyRelayFallbackTimeMs)
			{
				if(sm_EmergencyRelayFallbackTimeMs > 0)
				{
					// emergency timeout. We haven't recovered from a broken link, switch to relay server.
					netError("Emergency receive timeout. Link to %s is broken. Time since last receive: %u ms. Time since last route change: %u ms.",
							node->GetPeerId().ToHexString(peerIdHexString), timeSinceLastReceive, timeSinceLastRouteChange);

					if(viaPeerRelay)
					{
						// blacklist the next hop node
						netWarning("UpdateRouteMaintenance : switching %s from peer relay to relay server. Blacklisting next-hop peer: %s",
							node->GetPeerId().ToHexString(peerIdHexString), node->GetNextHopPeerId().ToHexString(peerIdHexString2));

						BlacklistPeer(node->GetNextHopPeerId());
					}

					SwitchToRelayServer(node);
				}
			}
			else if((timeSinceLastReceive > sm_BrokenLinkDetectionTimeMs) || 
				// Consider a route to be broken if it is routed via peer relay and the age of any unacked frame is greater than the broken link detection time out.
				// We can be receiving frames very slowly and with a large RTT due to a poor quality link or exceeding bandwidth which wouldn't otherwise be detected.
				// However, if we've recently changed routes, give the new route some time before switching back to relay. It may have been the previous route
				// that caused the poor connection quality (in some cases, it's the relay server route itself that was poor).
				(viaPeerRelay && (ageOfOldestUnackedFrame > sm_BrokenLinkDetectionTimeMs) && (timeSinceLastRouteChange > (sm_BrokenLinkDetectionTimeMs / 2))))
			{
				netDebug1("UpdateRouteMaintenance :: Link to %s is broken. Time since last receive: %u ms. Time since last route change: %u ms. Age of oldest unacked frame: %u ms.",
							node->GetPeerId().ToHexString(peerIdHexString), timeSinceLastReceive, timeSinceLastRouteChange, m_CxnMgr->GetAgeOfOldestUnackedFrame(node->GetEndpointId()));

				if(viaPeerRelay)
				{
					// blacklist the next hop node
					netWarning("UpdateRouteMaintenance : switching %s from peer relay to relay server. Blacklisting next-hop peer: %s",
								node->GetPeerId().ToHexString(peerIdHexString), node->GetNextHopPeerId().ToHexString(peerIdHexString2));

					BlacklistPeer(node->GetNextHopPeerId());
				}

				// link considered to be broken, start route repair protocol
				RouteBroken(node);
			}
		}
	}
}

bool netConnectionRouter::StartRouteDiscovery(Node* node, u8 ringTtl)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];

		if(ringTtl == TTL_START)
		{
			netDebug("Starting route discovery for peerId %s with ring TTL: %u", node->GetPeerId().ToHexString(peerIdHexString), ringTtl);
		}
		else
		{
			netDebug("Expanding route discovery for peerId %s with ring TTL: %u", node->GetPeerId().ToHexString(peerIdHexString), ringTtl);
		}
#endif

		netRouteRequestMsg msg;
		rverifyall(CreateRouteRequest(node, ringTtl, &msg));
		MulticastRouteRequest(msg);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool netConnectionRouter::CreateRouteRequest(Node* node, const u8 ringTtl, netRouteRequestMsg* msg)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
		const netPeerId& destPeerId = node->GetPeerId();
		rverifyall(destPeerId.IsValid());

		/*
			6.1 Immediately before a node originates a route discovery, it MUST
				increment its own sequence number.  This prevents conflicts with
				previously established reverse routes towards the originator of a
				RREQ.
		*/
		m_LocalSeq.Inc();

		m_RouteRequestId.Inc();

		u8 hopCount = 0; // always starts at 0 at the source node
		u8 flags = netRouteRequestMsg::RREQ_FLAGS_NONE;
		if(!node->IsDestSeqValid())
		{
			flags |= netRouteRequestMsg::RREQ_FLAG_INVALID_DEST_SEQ;
		}

		netAssert(ringTtl <= sm_MaxHopCount);

		*msg = netRouteRequestMsg(m_RouteRequestId, m_LocalSeq, m_LocalPeerId, node->GetDestSeq(), destPeerId, hopCount, ringTtl, flags);

		/*
			Before broadcasting the RREQ, the originating node buffers the RREQ
			ID and the Originator IP address (its own address) of the RREQ for
			PATH_DISCOVERY_TIME_MS.  In this way, when the node receives the packet
			again from its neighbors, it will not reprocess and re-forward the
			packet.

			[NS] - instead, we always drop a RREQ if we are the source peer of the request.
		*/

#if !__NO_OUTPUT
		netDebug1("CreateRouteRequest:");
		msg->Dump();
#endif

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool netConnectionRouter::CreateRouteReply(const netPeerId& srcPeerId, const netPeerId& destPeerId, const SequenceNumber& destSeq, const u8 hopCount, const bool gratuitous, netRouteReplyMsg* msg)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
		rverifyall(msg);

		m_RouteReplyId = (unsigned)sm_Rng.GetInt();

		u8 flags = netRouteReplyMsg::RREP_FLAGS_NONE;
		if(gratuitous)
		{
			flags |= netRouteReplyMsg::RREP_FLAG_GRATUITOUS_REPLY;
		}

		*msg = netRouteReplyMsg(m_RouteReplyId, srcPeerId, destSeq, destPeerId, hopCount, sm_MaxHopCount, flags);
	
#if !__NO_OUTPUT
		netDebug1("CreateRouteReply:");
		msg->Dump();
#endif

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool netConnectionRouter::UnicastRouteReply(const netRouteReplyMsg& reply, const EndpointId epId)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
#if !__NO_OUTPUT
		const netAddress& destAddr = m_CxnMgr->GetAddress(epId);
		netAssert(destAddr.IsDirectAddr());
		netDebug("Sending route reply to " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(destAddr));
		reply.Dump();
#endif

		rverify(m_CxnMgr->SendOutOfBand(epId, NET_RESERVED_CHANNEL_ID, reply, NET_SEND_IMMEDIATE),
				catchall,
				netError("Failed to send netRouteReplyMsg"));
		
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void netConnectionRouter::MulticastRouteRequest(const netRouteRequestMsg& msg)
{
	ASSERT_CXNMGR_CS_LOCKED;

	MulticastRouteRequest(msg, netPeerId::INVALID_PEER_ID);
}

void netConnectionRouter::MulticastRouteRequest(const netRouteRequestMsg& msg, const netPeerId& skipPeerId)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
		NodeList::iterator it = m_Nodes.begin();
		NodeList::const_iterator stop = m_Nodes.end();
		for(; stop != it; ++it)
		{
			const Node* nextHopCandidate = *it;

			// send the route request to all directly connected neighbours
			if((nextHopCandidate->GetRoute() == Node::Direct) &&
				nextHopCandidate->IsRouteActive() &&
				(!skipPeerId.IsValid() || (nextHopCandidate->GetPeerId() != skipPeerId)))
			{
				const EndpointId epId = nextHopCandidate->GetEndpointId();
				UnicastRouteRequest(msg, epId);
			}
		}

		rverifyall(true);
	}
	rcatchall
	{

	}
}

bool netConnectionRouter::UnicastRouteRequest(const netRouteRequestMsg& msg, const EndpointId epId)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
		rverifyall(NET_IS_VALID_ENDPOINT_ID(epId));

#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		const netAddress& destAddr = m_CxnMgr->GetAddress(epId);
		netAssert(destAddr.IsDirectAddr());
		netDebug1("Sending route request to " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(destAddr));
		msg.Dump();
#endif

		const netPeerId& peerId = m_CxnMgr->GetPeerId(epId);
		rverify(peerId.IsValid(), catchall, netError("Cannot send route request to an unknown sender."));

		rcheck(!IsPeerBlacklisted(peerId), catchall,
				netDebug1("Not sending route request to blacklisted peer %s.", peerId.ToHexString(peerIdHexString)));

		netEndpoint::EndpointQosReason qosReason;
		rcheck(m_CxnMgr->GetQoS(epId, qosReason) == netEndpoint::QOS_GOOD, catchall,
				netDebug1("Not sending route request to %s due to poor QoS. Reason: %d.", peerId.ToHexString(peerIdHexString), static_cast<int>(qosReason)));

		// routing protocol control messages such as route requests and replies need to be sent immediately so
		// that only the fastest route that RREQ and RREP messages can travel is selected for data transmission.
		return m_CxnMgr->SendOutOfBand(epId, NET_RESERVED_CHANNEL_ID, msg, NET_SEND_IMMEDIATE);
	}
	rcatchall
	{
		return false;
	}
}

bool netConnectionRouter::UnicastRouteError(const netRouteErrMsg& rerr, const EndpointId epId)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
#if !__NO_OUTPUT
		const netAddress& destAddr = m_CxnMgr->GetAddress(epId);
		netAssert(destAddr.IsDirectAddr());
		netDebug("Sending route err to " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(destAddr));
		rerr.Dump();
#endif

		rverify(m_CxnMgr->SendOutOfBand(epId, NET_RESERVED_CHANNEL_ID, rerr, NET_SEND_IMMEDIATE),
				catchall,
				netError("Failed to send netRouteErrMsg"));
		
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool netConnectionRouter::SendRouteChangeRequest(Node* node)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
		rverifyall(node);
		rverifyall(node->GetRoute() == Node::Direct);

		const EndpointId epId = node->GetEndpointId();
		rverifyall(NET_IS_VALID_ENDPOINT_ID(epId));

		// record the time at which we attempted to send the msg, even if it fails
		node->SetChangeRouteSentTime(sysTimer::GetSystemMsTime());

		const netAddress& mainAddr = m_CxnMgr->GetAddress(epId);
		rverifyall(mainAddr.IsDirectAddr());

		const netAddress& relayAddr = m_CxnMgr->GetRelayAddress(epId);
		rcheck(relayAddr.IsRelayServerAddr(), catchall, netWarning("Could not send netRouteChangeRequestMsg to epid %u [" NET_ADDR_FMT "] - no valid relay address", epId, NET_ADDR_FOR_PRINTF(mainAddr)));

		const netPeerAddress& peerAddr = m_CxnMgr->GetPeerAddress(epId);
		rverifyall(peerAddr.IsValid());

		netRouteChangeRequestMsg routeChangeRequest;

#if !__NO_OUTPUT
		netDebug("Sending netRouteChangeRequestMsg to " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(relayAddr));
		routeChangeRequest.Dump();
#endif

		// this message needs to be sent as a tunneling packet to prevent the remote peer
		// from switching the connection immediately to relay upon receiving it
		const netP2pCrypt::Key& remotePeerKey = peerAddr.GetPeerKey();
		netP2pCryptContext context;
		context.Init(remotePeerKey, NET_P2P_CRYPT_TUNNELING_PACKET);

		rverify(m_CxnMgr->SendReservedChannelPacket(relayAddr, routeChangeRequest, &context),
				catchall,
				netError("Failed to send netRouteChangeRequestMsg"));
		
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool netConnectionRouter::SendRouteChangeReply(Node* node)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
		rverifyall(node);

		const EndpointId epId = node->GetEndpointId();
		rverifyall(NET_IS_VALID_ENDPOINT_ID(epId));

		const netAddress& relayAddr = m_CxnMgr->GetRelayAddress(epId);
		rcheck(relayAddr.IsRelayServerAddr(), catchall, netWarning("Could not send netRouteChangeReplyMsg to epid %u - no valid relay address", epId));

		const netPeerAddress& peerAddr = m_CxnMgr->GetPeerAddress(epId);
		rverifyall(peerAddr.IsValid());

		netRouteChangeReplyMsg routeChangeReply;

#if !__NO_OUTPUT
		netDebug("Sending netRouteChangeReplyMsg to " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(relayAddr));
		routeChangeReply.Dump();
#endif

		// this message needs to be sent as a tunneling packet to prevent the remote peer
		// from switching the connection immediately to relay upon receiving it
		const netP2pCrypt::Key& remotePeerKey = peerAddr.GetPeerKey();
		netP2pCryptContext context;
		context.Init(remotePeerKey, NET_P2P_CRYPT_TUNNELING_PACKET);

		rverify(m_CxnMgr->SendReservedChannelPacket(relayAddr, routeChangeReply, &context),
				catchall,
				netError("Failed to send netRouteChangeReplyMsg"));
		
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void netConnectionRouter::ReceiveRouteRequest(netRouteRequestMsg* rreq, const EndpointId senderEpId, const netAddress& sender)
{
	if(!IsPeerRelayEnabled())
	{
		return;
	}

	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	rtry
	{
#if !__NO_OUTPUT
		netDebug("Received a route request from " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(sender));
		rreq->Dump();
#endif

		const unsigned numForwardedConnections = (unsigned)m_RoutePairs.size();
		if(numForwardedConnections >= sm_MaxRelayedCxns)
		{
			netDebug1("    We are already relaying %u connections. sm_MaxRelayedCxns: %u. Discarding route request.", numForwardedConnections, sm_MaxRelayedCxns);
			return;
		}

		rverify(sender.IsDirectAddr(), catchall, netError("Received a route request from an endpoint to which we don't have a direct connection"));
	
		if(rreq->m_HopCount >= sm_MaxHopCount)
		{
			// this can happen if the topology changed while the request was propagating
			netDebug1("    Route request has high hop count: %u > max hop count: %u. Discarding.", (unsigned)rreq->m_HopCount, (unsigned)sm_MaxHopCount);
			return;
		}

		/*
			To prevent routing loops when forwarding route requests, discard the route request if:
			- We are the source peer (i.e. we sent the request and someone has forwarded that request
			  back to us (e.g. A -> B -> C -> A))
			- We already received this request (regardless of which source peer originated it).
		*/
		if(m_LocalPeerId == rreq->m_SrcId)
		{
			netDebug1("We received our own route request, discarding.");
			return;
		}

		// get source node
		Node* sourceNode = GetNodeByPeerId(rreq->m_SrcId);
		rcheck(sourceNode, catchall, netDebug1("We don't know about the source node. Discarding."));

		bool destIsLocalPeer = m_LocalPeerId == rreq->m_DestId;

		// get destination node
		Node* destNode = GetNodeByPeerId(rreq->m_DestId);
		rcheck(destNode || destIsLocalPeer, catchall, netDebug1("Route request for an unknown destination node. We may not know about the destination peer yet."));

		// get peerId of sender (not necessarily the source peer)
		const netPeerId& senderPeerId = m_CxnMgr->GetPeerId(senderEpId);
		rverify(senderPeerId.IsValid(), catchall, netError("Received a route request from an unknown sender. Discarding."));

		Node* senderNode = GetNodeByPeerId(senderPeerId);
		rcheck(senderNode, catchall, netDebug1("Received a route request from an unknown node. Discarding."));

		/*
			When a node receives a RREQ, it first creates or updates a route to
			the previous hop without a valid sequence number (see section 6.2)
			then checks to determine whether it has received a RREQ with the same
			Originator IP Address and RREQ ID within at least the last
			PATH_DISCOVERY_TIME_MS.  If such a RREQ has been received, the node
			silently discards the newly received RREQ.
		*/
		bool discard = BlacklistRouteRequest(*rreq);
		if(discard)
		{
			netDebug1("This route request was already received, discarding.");
			return;
		}

		/*
			6.8: A node ignores all RREQs received from any node in its blacklist set.
		*/
		if(IsPeerBlacklisted(senderPeerId))
		{
			netDebug1("The sender of this route request has been blacklisted, discarding.");
			return;
		}

		// discard if it has been too long since we last received anything from the destination peer
		// or if the overall QoS, QoS to the source, or QoS to the destination is poor
		if(destNode && !destIsLocalPeer)
		{
			const unsigned curTime = sysTimer::GetSystemMsTime();
			const unsigned lastReceiveTime = destNode->GetLastReceiveTime();
			const unsigned timeSinceLastReceive = curTime - lastReceiveTime;
			if((lastReceiveTime == 0) || (timeSinceLastReceive > sm_BrokenLinkDetectionTimeMs))
			{
				netDebug1("The route to the destination peer might be broken (time since last receive: %u). Discarding.", timeSinceLastReceive);
				return;
			}

			netConnectionManager::OverallQosReason overallQos;
			if((m_CxnMgr->GetOverallQoS(overallQos) == netConnectionManager::OverallQoS::QOS_BAD))
			{
				netDebug1("Overall QoS below threshold (Reason: %d). Discarding.", static_cast<int>(overallQos));
				return;
			}

			netEndpoint::EndpointQosReason qosReason;
			if((m_CxnMgr->GetQoS(destNode->GetEndpointId(), qosReason) == netEndpoint::QOS_BAD))
			{
				netDebug1("The route to the destination peer has poor QoS (Reason: %d). Discarding.", static_cast<int>(qosReason));
				return;
			}

			if((m_CxnMgr->GetQoS(sourceNode->GetEndpointId(), qosReason) == netEndpoint::QOS_BAD))
			{
				netDebug1("The route to the source peer has poor QoS (Reason: %d). Discarding.", static_cast<int>(qosReason));
				return;
			}
		}

		//////////////////////////////////////////////////////////////////////////
		// Update reverse route towards the source peer
		//////////////////////////////////////////////////////////////////////////

		/*
			First, increment the hop count value in the RREQ by one, to
			account for the new hop through the intermediate node.
		*/
		rreq->m_HopCount++;

		/*
			6.2	The route is only updated if the new sequence number is either:

			(i) higher than the destination sequence number in the route
			table, or

			(ii) the sequence numbers are equal, but the hop count (of the
			new information) plus one, is smaller than the existing hop
			count in the routing table, or

			(iii) the sequence number is unknown.
				[NS] Be careful not to blindly take the request's seq. If it's
				lower than what we have in our routing table, it can lead to decrementing
				a dest seq, causing routing loops.
				See section 3.2 (interpretation 2b) in http://rvg.web.cse.unsw.edu.au/pub/AODVloop.pdf
		*/
		bool sameSeq = rreq->m_SrcSeq == sourceNode->GetDestSeq();
		bool higherSeq = rreq->m_SrcSeq > sourceNode->GetDestSeq();
		bool sameSeqBetterRoute = sameSeq && (rreq->m_HopCount < sourceNode->GetHopCount());
		bool sameSeqActiveRoute = sameSeq && (!sourceNode->IsRouteActive());
		bool rtSeqIsInvalid = sameSeq && !sourceNode->IsDestSeqValid();
		bool updateRoute = higherSeq || sameSeqActiveRoute || sameSeqBetterRoute || rtSeqIsInvalid;

		if(updateRoute)
		{
#if !__NO_OUTPUT
			if(higherSeq) netDebug1("    Route request contains new information about the source peer (higher seq: rreq->m_SrcSeq > sourceNode->GetDestSeq()).");
			if(sameSeqActiveRoute) netDebug1("    Route request contains the same seq as what we've already recorded, but our route is marked as inactive.");
			if(sameSeqBetterRoute) netDebug1("    Route request contains the same seq as what we've already recorded, but route is an improvement.");
 			if(rtSeqIsInvalid) netDebug1("    Our routing table doesn't have a valid destination sequence number for the source peer.");
#endif

			// [NS] AODV can cause routes to worsen, and can even change a direct route to one that takes multiple hops.
			// To avoid this, if this request would worsen our route back to the source, drop it. This is only possible
			// because we have a relay server to fall back on if this causes a link to break.
			if((sourceNode->IsRouteActive() || (sourceNode->GetRoute() == Node::Direct)) && (rreq->m_HopCount > sourceNode->GetHopCount()))
			{
				netDebug1("    Route request would worsen our valid route back to the source peer. Discarding.");

				// if we already have a path to the source, we should get this request along that path soon. We'll use that one instead.
				RemoveBlacklistedRouteRequest(*rreq);
				return;
			}

			if(rreq->m_HopCount > 1)
			{
				netAssert(sourceNode->GetRoute() != Node::Direct);
				netAssert(sourceNode->GetHopCount() != 1);
			}

			// mark this reverse route as active, otherwise we won't forward replies
			// on this route when one comes back.
			sourceNode->SetRouteActive(true);

			/*
				the Originator Sequence Number from the RREQ is compared to the
				corresponding destination sequence number in the route table entry
				and copied if greater than the existing value there.
			*/		
			sourceNode->MaxDestSeq(rreq->m_SrcSeq);

			/*
				the valid sequence number field is set to true;
			*/
			sourceNode->SetDestSeqValid(true);

			/*
				the next hop in the routing table becomes the node from which the
				RREQ was received
			*/
			bool changedNextHopPeer = senderPeerId != sourceNode->GetNextHopPeerId();
			sourceNode->SetNextHopPeerId(senderPeerId);

			/*
				the hop count is copied from the Hop Count in the RREQ message
			*/
			sourceNode->SetHopCount(rreq->m_HopCount);

			// We may have just changed our route back to the source. We can't immediately switch the CxnMgr's
			// connection to use this new route because the remote peer will have no idea we found a path to
			// them if we're an intermediate node. They will still be using the relay server, and cause us to
			// revert back to relay server.

			if(!destIsLocalPeer && changedNextHopPeer && (sourceNode->GetRoute() == Node::PeerRelay))
			{
				netAddress addr(sender.GetTargetAddress(), rreq->m_SrcId);
				m_CxnMgr->UpdateEndpointAddress(rreq->m_SrcId, addr, netAddress::INVALID_ADDRESS);
			}

			netDebug1("Updated reverse route towards source.");
			OUTPUT_ONLY(sourceNode->DumpRoutingEntry());
		}
		else
		{
			netDebug("    Route request contains stale information or no route improvement. Routing table not updated.");
		}

		//////////////////////////////////////////////////////////////////////////
		// End updating reverse route
		//////////////////////////////////////////////////////////////////////////
	
		// now we either respond to the request or forward it
		if(destIsLocalPeer)
		{
			// it is possible that we didn't just update our route back to the source, but we are the destination
			// of the request, so we need to update our actual route back to the source.

			netDebug1("We're the destination peer. Promoting soft state to hard state.");

			rcheck(sourceNode->GetRoute() != Node::Direct, catchall, netDebug1("We're the destination peer. We already have a direct connection to the source. Not updating route."));
			rcheck(sourceNode->IsRouteActive(), catchall, netDebug1("We have no valid route back to the source peer"));

			// our proxy peer is the next hop towards the source
			const Node* nextHopNodeTowardsSource = GetNextHopNode(rreq->m_SrcId);
			rcheck(nextHopNodeTowardsSource, catchall, netDebug("We have no route to next hop peer towards the source"));
			rverify(nextHopNodeTowardsSource->GetRoute() == Node::Direct, catchall, netError("Route back to the next hop towards the source is not direct"));
			rcheck(nextHopNodeTowardsSource->IsRouteActive(), catchall, netDebug("Route back to the next hop towards the source is invalid"));
			const netAddress& proxyNetAddr = m_CxnMgr->GetAddress(nextHopNodeTowardsSource->GetEndpointId());
			netAssert(proxyNetAddr.IsDirectAddr());
			const netSocketAddress& proxyAddr = proxyNetAddr.GetTargetAddress();
			netAddress addr(proxyAddr, rreq->m_SrcId);

			netAssert(rreq->m_SrcId == sourceNode->GetPeerId());
			m_CxnMgr->UpdateEndpointAddress(rreq->m_SrcId, addr, netAddress::INVALID_ADDRESS);

			/*
				6.1: Immediately before a destination node originates a RREP in
				response to a RREQ, it MUST update its own sequence number to the
				maximum of its current sequence number and the destination
				sequence number in the RREQ packet.
			*/
			m_LocalSeq.Max(rreq->m_DestSeq);

#if NET_PEER_RELAY_DESTINATION_ONLY
			// [NS] There is a flaw in the AODV RFC when the 'destination only' option
			// is enabled (i.e. when replying from intermediate nodes is disabled).
			// We need to increment our local seq here, otherwise our next hop won't
			// forward our reply, since the reply won't update their routing table.
			// Note that Draft 9 of the AODV RFC does increment in this case.
			m_LocalSeq.Inc();
#endif

			// send a route reply to the next hop along the reverse path towards the source peer (not necessarily to the sender)
			netAssert(rreq->m_DestId == m_LocalPeerId);

			netRouteReplyMsg reply;
			rverifyall(CreateRouteReply(rreq->m_SrcId, m_LocalPeerId, m_LocalSeq, 0, false, &reply));

			// it's possible that a link along the reverse path got broken while the request
			// was hopping toward us, so we won't have a valid route back to the source
			Node* reverseNextHop = GetNextHopNode(rreq->m_SrcId);
			rcheck(reverseNextHop, catchall, netDebug1("Can't send reply since we don't have a valid route to the source"));

			rverifyall(UnicastRouteReply(reply, reverseNextHop->GetEndpointId()));
		}
		else
		{
			// we are an intermediate node

#if !NET_PEER_RELAY_DESTINATION_ONLY
			// check if we have a valid and fresh enough route to the destination and back to the source

			const bool freshEnough = destNode->GetDestSeq() >= rreq->m_DestSeq;
			bool haveRouteToDest = freshEnough && destNode->IsRouteActive();
			bool haveRouteToSource = false;
			Node* nextHopNodeTowardsSource = nullptr;

			if(haveRouteToDest)
			{
				const u8 hopCount = rreq->m_HopCount + destNode->GetHopCount();
				if((hopCount > sm_MaxHopCount) || (hopCount > (rreq->m_Ttl + 1)))
				{
					netDebug1("We are an intermediate node with a valid route to the destination, but the full route would take too many hops (%u).", (unsigned)hopCount);
					haveRouteToDest = false;
				}
				else
				{
					// check if we still have a valid route back to the source
					nextHopNodeTowardsSource = GetNextHopNode(rreq->m_SrcId);
					haveRouteToSource = nextHopNodeTowardsSource && nextHopNodeTowardsSource->IsRouteActive();

					if(!haveRouteToSource)
					{
						netDebug1("We are an intermediate node with a valid route to the destination, but we have no valid route back to the source.");
					}
				}
			}

			if(haveRouteToDest && haveRouteToSource)
			{
#if !__NO_OUTPUT
				netDebug1("We are an intermediate node with a valid route to the destination:");
				destNode->DumpRoutingEntry();
#endif

				/*
					If the node generating the RREP is not the destination node, but
					instead is an intermediate hop along the path from the originator to
					the destination, it copies its known sequence number for the
					destination into the Destination Sequence Number field in the RREP
					message.

					The intermediate node updates the forward route entry by placing the
					last hop node (from which it received the RREQ, as indicated by the
					source IP address field in the IP header) into the precursor list for
					the forward route entry -- i.e., the entry for the Destination IP
					Address.  The intermediate node also updates its route table entry
					for the node originating the RREQ by placing the next hop towards the
					destination in the precursor list for the reverse route entry --
					i.e., the entry for the Originator IP Address field of the RREQ
					message data.

					The intermediate node places its distance in hops from the
					destination (indicated by the hop count in the routing table) Count
					field in the RREP.
				*/				
				Node* nextHopNodeTowardsDest = GetNextHopNode(rreq->m_DestId);
				rverify(nextHopNodeTowardsDest, catchall, netError("ReceiveRouteRequest :: we have no route to the destination peer"));

				destNode->AddPrecursor(nextHopNodeTowardsSource);
				sourceNode->AddPrecursor(nextHopNodeTowardsDest);

				netRouteReplyMsg reply;
				rverifyall(CreateRouteReply(rreq->m_SrcId, rreq->m_DestId, destNode->GetDestSeq(), destNode->GetHopCount(), false, &reply));
				rverifyall(UnicastRouteReply(reply, nextHopNodeTowardsSource->GetEndpointId()));

				/*
					6.6.3. Generating Gratuitous RREPs
					After a node receives a RREQ and responds with a RREP, it discards
					the RREQ.  If the RREQ has the 'G' flag set, and the intermediate
					node returns a RREP to the originating node, it MUST also unicast a
					gratuitous RREP to the destination node.  The gratuitous RREP that is
					to be sent to the desired destination contains the following values
					in the RREP message fields:

					Hop Count                        The Hop Count as indicated in the
													node's route table entry for the
													originator

					Destination IP Address           The IP address of the node that
													originated the RREQ

					Destination Sequence Number      The Originator Sequence Number from
													the RREQ

					Originator IP Address            The IP address of the Destination
													node in the RREQ
				*/

				netRouteReplyMsg gratuitiousReply;
				rverifyall(CreateRouteReply(rreq->m_DestId, rreq->m_SrcId, rreq->m_SrcSeq, sourceNode->GetHopCount(), true, &gratuitiousReply));
				rverifyall(UnicastRouteReply(gratuitiousReply, nextHopNodeTowardsDest->GetEndpointId()));
			}
			else
#endif
			{
				if(rreq->m_Ttl > 1)
				{
					rreq->m_Ttl--;

					// reserve relay slots in both directions. Note that a route request can be sent to many
					// peers, and each peer reserves slots even though only one relayer is actually chosen to 
					// relay each connection. Slot reservations time out if the local peer doesn't end up
					// forwarding packets for that connection after a certain amount of time elapses.
					// See UpdateRoutePairs(). That means we could have capacity to relay more connections
					// but all of our relay slots are used up by temporary reservations. Peers will retry
					// route discovery with exponential back off, so a repeated route request should
					// eventually get satisfied as temporary reservations expire.
					rverify(TrackRoutePair(sourceNode->GetPeerId(), destNode->GetPeerId(), false),
						catchall,
						netError("TrackRoutePair failed - not forwarding request."));

					rverify(TrackRoutePair(destNode->GetPeerId(), sourceNode->GetPeerId(), false),
						catchall,
						netError("TrackRoutePair failed - not forwarding request."));

					/*
						When forwarding a route request message:
						the Destination Sequence number for the
						requested destination is set to the maximum of the corresponding
						value received in the RREQ message, and the destination sequence
						value currently maintained by the node for the requested destination.
						However, the forwarding node MUST NOT modify its maintained value for
						the destination sequence number, even if the value received in the
						incoming RREQ is larger than the value currently maintained by the
						forwarding node.
					*/
					rreq->m_DestSeq.Max(destNode->GetDestSeq());

					netDebug1("We don't have a valid, fresh enough route to the destination peer. Forwarding route request to directly connected neighbours.");
					MulticastRouteRequest(*rreq, senderPeerId);
				}
				else
				{
					netDebug1("Not forwarding request. TTL = 0.");
				}
			}
		}
	}
	rcatchall
	{

	}
}

void netConnectionRouter::ReceiveRouteReply(netRouteReplyMsg* rrep, const EndpointId senderEpId, const netAddress& sender)
{
	if(!IsPeerRelayEnabled())
	{
		return;
	}

	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	rtry
	{
#if !__NO_OUTPUT
		netDebug1("Received a route reply from " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(sender));
		rrep->Dump();
#endif

		rverify(sender.IsDirectAddr(), catchall, netError("Received a route reply from an endpoint to which we don't have a direct connection"));

		if(rrep->m_HopCount >= sm_MaxHopCount)
		{
			// this can happen if the topology changed while the reply was coming back
			netDebug1("    Route reply has high hop count: %u > max hop count: %u. Discarding.", (unsigned)rrep->m_HopCount, (unsigned)sm_MaxHopCount);
			return;
		}

		bool sourceIsLocalPeer = m_LocalPeerId == rrep->m_SrcId;

		// get source node
		Node* sourceNode = GetNodeByPeerId(rrep->m_SrcId);
		rcheck(sourceNode || sourceIsLocalPeer, catchall, netDebug1("We don't know about the source node. Discarding."));

		// if the destination is us, don't update routing table, but forward it towards the source.
		// Note that this implementation does not allow self-entries in the routing table, since they can cause routing loops.
		bool destIsLocalPeer = m_LocalPeerId == rrep->m_DestId;

		if(destIsLocalPeer)
		{
			netDebug1("We received a route reply even though we are the destination of the route. We will still forward it towards the source.");
		}

		Node* destNode = GetNodeByPeerId(rrep->m_DestId);
		rcheck(destNode || destIsLocalPeer, catchall, netDebug1("Route reply for an unknown destination node. We may not know about the destination peer yet."));

		const netPeerId& senderPeerId = m_CxnMgr->GetPeerId(senderEpId);
		rverify(senderPeerId.IsValid(), catchall, netError("Received a route reply from an unknown sender. Discarding."));

		Node* senderNode = GetNodeByPeerId(senderPeerId);
		rcheck(senderNode, catchall, netDebug1("Received a route reply from an unknown node. Discarding."));

		/*
			Don't accept route replies from blacklisted peers, since they were blacklisted because of problems relaying packets.
		*/
		if(IsPeerBlacklisted(senderPeerId))
		{
			netDebug1("The sender of this route reply has been blacklisted, discarding.");
			return;
		}

		//////////////////////////////////////////////////////////////////////////
		// Update forward route towards the destination peer
		//////////////////////////////////////////////////////////////////////////
		
		// increase hop count
		rrep->m_HopCount++;

		//////////////////////////////////////////////////////////////////////////
		// Update forward route towards the destination peer
		//////////////////////////////////////////////////////////////////////////

		/*
			the node compares the Destination Sequence
			Number in the message with its own stored destination sequence number
			for the Destination IP Address in the RREP message.  Upon comparison,
			the existing entry is updated only in the following circumstances:

			(i) the sequence number in the routing table is marked as
			invalid in route table entry.
			[NS] Be careful not to blindly take the reply's dest seq. If it's lower
			than what we have in our routing table, it can lead to decrementing
			a dest seq, causing routing loops. See section 3.1 (interpretation 1a) in
			http://rvg.web.cse.unsw.edu.au/pub/AODVloop.pdf

			(ii) the Destination Sequence Number in the RREP is greater than
			the node's copy of the destination sequence number and the
			known value is valid, or

			(iii) the sequence numbers are the same, but the route is
			marked as inactive, or

			(iv) the sequence numbers are the same, and the New Hop Count is
			smaller than the hop count in route table entry.
		*/

		bool forward = true;
		bool updateRoute = false;

		if(!destIsLocalPeer)
		{
			bool sameSeq = rrep->m_DestSeq == destNode->GetDestSeq();
			bool rtSeqIsInvalid = sameSeq && !destNode->IsDestSeqValid();
			bool higherSeq = (rrep->m_DestSeq > destNode->GetDestSeq());
			bool sameSeqActiveRoute = sameSeq && (!destNode->IsRouteActive());
			bool sameSeqBetterRoute = sameSeq && (rrep->m_HopCount < destNode->GetHopCount());
			updateRoute = higherSeq || sameSeqActiveRoute || sameSeqBetterRoute || rtSeqIsInvalid;

			if(updateRoute)
			{
#if !__NO_OUTPUT
				if(rtSeqIsInvalid) netDebug1("    Our routing table doesn't have a valid destination sequence number for the destination peer.");
				if(higherSeq) netDebug("    Route reply contains new information about the destination peer (higher destSeq).");
				if(sameSeqActiveRoute) netDebug1("    Route reply contains the same seq as what we've already recorded, but our route is marked as inactive.");
				if(sameSeqBetterRoute) netDebug1("    Route reply contains the same seq as what we've already recorded, but route is an improvement.");
#endif

				// [NS] AODV can cause routes to worsen, and can even change a direct route to one that takes multiple hops.
				// To avoid this, if this request would worsen our route to the dest, drop it. This is only possible because
				// we have a relay server to fall back on if this causes a link to break.
				if((destNode->IsRouteActive() || (destNode->GetRoute() == Node::Direct)) && (rrep->m_HopCount > destNode->GetHopCount()))
				{
 					netDebug1("    Route reply would worsen our valid route to the dest peer. Discarding.");
 					return;
				}

				if(rrep->m_HopCount > 1)
				{
					netAssert(destNode->GetRoute() != Node::Direct);
					netAssert(destNode->GetHopCount() != 1);
				}

				/*
					the route is marked as active,
				*/
				destNode->SetRouteActive(true);

				/*
					the destination sequence number is the Destination Sequence
					Number in the RREP message.
				*/
				destNode->MaxDestSeq(rrep->m_DestSeq);

				/*
					the destination sequence number is marked as valid,
				*/
				destNode->SetDestSeqValid(true);

				/*
					the next hop in the route entry is assigned to be the node from
					which the RREP is received, which is indicated by the source IP
					address field in the IP header,
				*/
				bool changedNextHopPeer = senderPeerId != destNode->GetNextHopPeerId();
				destNode->SetNextHopPeerId(senderPeerId);

				/*
					the hop count is set to the value of the New Hop Count
				*/
				destNode->SetHopCount(rrep->m_HopCount);

				// We may have just changed our route to the dest. We can't immediately switch the CxnMgr's
				// connection to use this new route because the remote peer will have no idea we found a path to
				// them if we're an intermediate node. They will still be using the relay server, and cause us to
				// revert back to relay server.

				if(!sourceIsLocalPeer && changedNextHopPeer && (destNode->GetRoute() == Node::PeerRelay))
				{
					netAddress addr(sender.GetTargetAddress(), rrep->m_DestId);
					m_CxnMgr->UpdateEndpointAddress(rrep->m_DestId, addr, netAddress::INVALID_ADDRESS);
				}

				netDebug1("Updated forward route towards destination.");
				OUTPUT_ONLY(destNode->DumpRoutingEntry());
			}
			else
			{
				netDebug("    Route reply contains stale information or no route improvement. Routing table not updated.");

				/*
					6.7 If the current node is not the node indicated by the Originator IP
					Address in the RREP message AND a forward route has been created or
					updated as described above, the node consults its route table entry
					for the originating node to determine the next hop for the RREP
					packet, and then forwards the RREP towards the originator using the
					information in that route table entry.
				*/

				// this reply did not update our routing table, do not forward.
				forward = false;
			}
		}

		//////////////////////////////////////////////////////////////////////////
		// End updating forward route towards the destination peer
		//////////////////////////////////////////////////////////////////////////

		if(sourceIsLocalPeer)
		{
			// it is possible that we didn't just update our route to the destination, but we are the source
			// of the request that generated this reply, so we need to update our actual route to the destination.

			netDebug1("We're the source peer. Promoting soft state to hard state.");

			rcheck(destNode->GetRoute() != Node::Direct, catchall, netDebug1("We're the source peer. We already have a direct connection to the destination peer. Not updating route."));
			rcheck(destNode->IsRouteActive(), catchall, netDebug1("We have no valid route to the destination peer"));

			// our proxy peer is the next hop towards the destination
			const Node* nextHopNodeTowardsDest = GetNextHopNode(rrep->m_DestId);
			rcheck(nextHopNodeTowardsDest, catchall, netDebug("We have no route to next hop peer towards the destination"));
			rverify(nextHopNodeTowardsDest->GetRoute() == Node::Direct, catchall, netError("Route to the next hop towards the destination is not direct"));
			rcheck(nextHopNodeTowardsDest->IsRouteActive(), catchall, netDebug("Route to the next hop towards the destination is invalid"));
			const netAddress& proxyNetAddr = m_CxnMgr->GetAddress(nextHopNodeTowardsDest->GetEndpointId());
			netAssert(proxyNetAddr.IsDirectAddr());
			const netSocketAddress& proxyAddr = proxyNetAddr.GetTargetAddress();
			netAddress addr(proxyAddr, rrep->m_DestId);

			netAssert(rrep->m_DestId == destNode->GetPeerId());
			m_CxnMgr->UpdateEndpointAddress(rrep->m_DestId, addr, netAddress::INVALID_ADDRESS);
		}
		else if(forward)
		{
			if(netVerify(rrep->m_Ttl > 1))
			{
				rrep->m_Ttl--;

				// we're not the source peer, forward reply to the next hop along the reverse path towards the source peer
				netDebug1("We're not the source peer, forwarding reply to the next hop along the reverse path towards the source peer");

				Node* nextHopNodeTowardsSource = GetNextHopNode(rrep->m_SrcId);
				if(nextHopNodeTowardsSource && nextHopNodeTowardsSource->IsRouteActive())
				{
					if(destNode)
					{
						/*
							When any node transmits a RREP, the precursor list for the
							corresponding destination node is updated by adding to it the next
							hop node to which the RREP is forwarded.  Also, at each node the
							(reverse) route used to forward a RREP has its lifetime changed to be
							the maximum of (existing-lifetime, (current time +
							ACTIVE_ROUTE_TIMEOUT).  Finally, the precursor list for the next hop
							towards the destination is updated to contain the next hop towards
							the source.
						*/
						destNode->AddPrecursor(nextHopNodeTowardsSource);

						Node* nextHopNodeTowardsDest = GetNextHopNode(rrep->m_DestId);
						if(nextHopNodeTowardsDest)
						{
							nextHopNodeTowardsDest->AddPrecursor(nextHopNodeTowardsSource);

							/*
								[NS] Since we only initiate route discovery from the peer with the
								higher peerId as an optimization, the precursor list for the next
								hop towards the source is updated to contain the next hop towards
								the destination.
							*/
							nextHopNodeTowardsSource->AddPrecursor(nextHopNodeTowardsDest);
						}
					}

					UnicastRouteReply(*rrep, nextHopNodeTowardsSource->GetEndpointId());
				}
				else
				{
					netDebug1("Not forwarding reply. No active route back to the source.");
				}
			}
			else
			{
				netDebug1("Not forwarding reply. TTL = 0.");
			}
		}
		else
		{
			netDebug1("Route reply did not update the routing table. Discarding.");
		}
	}
	rcatchall
	{

	}
}

void netConnectionRouter::ReceiveRouteError(netRouteErrMsg* rerr, const EndpointId senderEpId, const netAddress& sender)
{
	if(!IsPeerRelayEnabled())
	{
		return;
	}

	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	rtry
	{
#if !__NO_OUTPUT
		netDebug1("Received a route err from " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(sender));
		rerr->Dump();
#endif

		rverify(sender.IsDirectAddr(), catchall, netError("Received a route err from an endpoint to which we don't have a direct connection"));
		rverify(rerr->m_DestCount > 0, catchall, netError("Received a route err that lists no unreachable destinations"));

		/*
			Note: this is 6.11 case (iii)

			A node initiates processing for a RERR message in three situations:

			(iii) if it receives a RERR from a neighbor for one or more
			active routes.

			For case (iii), the list should consist of those destinations in the RERR
			for which there exists a corresponding entry in the local routing
			table that has the transmitter of the received RERR as the next hop.
		*/

		const netPeerId& senderPeerId = m_CxnMgr->GetPeerId(senderEpId);
		rcheck(senderPeerId.IsValid(), catchall, netDebug1("Received a route err from an unknown peer"));

		u8 numUnreachableDestinations = 0;
		Node* unreachableDestinations[netRouteErrMsg::MAX_UNREACHABLE_DESTINATIONS];

		for(unsigned i = 0; i < rerr->m_DestCount; ++i)
		{
			Node* unreachableNode = GetNodeByPeerId(rerr->m_UnreachableDestIds[i]);
			const SequenceNumber& unreachableDestSeq = rerr->m_UnreachableDestSeqs[i];

			if(unreachableNode &&
				(unreachableNode->GetNextHopPeerId() == senderPeerId) &&
				(unreachableNode->IsRouteActive()) &&
				(unreachableDestSeq >= unreachableNode->GetDestSeq()))
			{
				/*
					For each one of these destinations,
					the corresponding routing table entry is updated as follows:

					1. The destination sequence number of this routing entry, if it
					exists and is valid, is incremented for cases (i) and (ii) above,
					and copied from the incoming RERR in case (iii) above.

					[NS] Be careful not to blindly take the message's seq. If it's
					lower than what we have in our routing table, it can lead to decrementing
					a dest seq, causing routing loops.
					See section 3.4 (interpretation 4a) in http://rvg.web.cse.unsw.edu.au/pub/AODVloop.pdf

					2. The entry is invalidated by marking the route entry as invalid

					// [NS] Note: this implementation doesn't use route lifetimes.
					3. The Lifetime field is updated to current time plus DELETE_PERIOD.
					Before this time, the entry SHOULD NOT be deleted.
				*/

				unreachableNode->MaxDestSeq(unreachableDestSeq);
				// note: we mark the route as invalid in ProcessUnreachableDestinations()

				unreachableDestinations[numUnreachableDestinations] = unreachableNode;
				numUnreachableDestinations++;
			}
		}

		ProcessUnreachableDestinations(unreachableDestinations, numUnreachableDestinations);
	}
	rcatchall
	{

	}
}

void netConnectionRouter::ReceiveRouteChangeRequest(netRouteChangeRequestMsg* OUTPUT_ONLY(routeChangeRqst), const EndpointId senderEpId, const netAddress& sender)
{
	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	rtry
	{
#if !__NO_OUTPUT
		netDebug1("Received a netRouteChangeRequestMsg from " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(sender));
		routeChangeRqst->Dump();
#endif

		rverify(sender.IsRelayServerAddr(), catchall, netError("Received a netRouteChangeRequestMsg from a non-relay server route"));

		rcheck(NET_IS_VALID_ENDPOINT_ID(senderEpId), catchall, netDebug("Received a netRouteChangeRequestMsg but sender endpoint id is invalid. Discarding."));
		const netPeerId& senderPeerId = m_CxnMgr->GetPeerId(senderEpId);
		rverify(senderPeerId.IsValid(), catchall, netError("Received a netRouteChangeRequestMsg from an unknown sender. Discarding."));

		Node* senderNode = GetNodeByPeerId(senderPeerId);
		rcheck(senderNode, catchall, netDebug1("Received a netRouteChangeRequestMsg from an unknown node. Discarding."));

		this->SendRouteChangeReply(senderNode);
	}
	rcatchall
	{

	}
}

void netConnectionRouter::ReceiveRouteChangeReply(netRouteChangeReplyMsg* OUTPUT_ONLY(routeChangeReply), const EndpointId senderEpId, const netAddress& sender)
{
	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	rtry
	{
#if !__NO_OUTPUT
		netDebug1("Received a netRouteChangeReplyMsg from " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(sender));
		routeChangeReply->Dump();
#endif

		rverify(sender.IsRelayServerAddr(), catchall, netError("Received a netRouteChangeReplyMsg from a non-relay server route"));

		// the endpoint may have been destroyed since we sent the route change request
		rcheck(NET_IS_VALID_ENDPOINT_ID(senderEpId), catchall, netDebug("Received a netRouteChangeReplyMsg but sender endpoint id is invalid. Discarding."));
		const netPeerId& senderPeerId = m_CxnMgr->GetPeerId(senderEpId);
		rverify(senderPeerId.IsValid(), catchall, netError("Received a netRouteChangeReplyMsg from an unknown sender. Discarding."));

		Node* senderNode = GetNodeByPeerId(senderPeerId);
		rcheck(senderNode, catchall, netDebug1("Received a netRouteChangeReplyMsg from an unknown node. Discarding."));

		if(senderNode->GetChangeRouteSentTime() > 0)
		{
			// we received a reply to our route change request
			senderNode->SetChangeRouteReceivedReplyTime(sysTimer::GetSystemMsTime());
		}
	}
	rcatchall
	{

	}
}

void netConnectionRouter::SwitchToRelayServer(const netPeerId& peerId)
{
	ASSERT_CXNMGR_CS_LOCKED;

	const netAddress& addr = m_CxnMgr->GetAddress(peerId);
	if(addr.IsPeerRelayAddr())
	{
#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		netDebug1("Switching connection to %s to relay server.", peerId.ToHexString(peerIdHexString));
#endif

		const netAddress& relayAddr = m_CxnMgr->GetRelayAddress(peerId);
		if(relayAddr.IsRelayServerAddr())
		{
			m_CxnMgr->UpdateEndpointAddress(peerId, relayAddr, netAddress::INVALID_ADDRESS);
		}
	}
}

void netConnectionRouter::SwitchToRelayServer(Node* node)
{
	ASSERT_CXNMGR_CS_LOCKED;

	SwitchToRelayServer(node->GetPeerId());
}

void netConnectionRouter::SwitchDirectRouteToRelayServer(const netPeerId& peerId)
{
	ASSERT_CXNMGR_CS_LOCKED;

	const netAddress& addr = m_CxnMgr->GetAddress(peerId);
	if(addr.IsDirectAddr())
	{
#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		netDebug1("Switching direct connection to %s to relay server.", peerId.ToHexString(peerIdHexString));
#endif

		const netAddress& relayAddr = m_CxnMgr->GetRelayAddress(peerId);
		if(relayAddr.IsRelayServerAddr())
		{
			m_CxnMgr->UpdateEndpointAddress(peerId, relayAddr, netAddress::INVALID_ADDRESS);
		}
	}
}

void netConnectionRouter::SwitchDirectRouteToRelayServer(Node* node)
{
	ASSERT_CXNMGR_CS_LOCKED;

	SwitchDirectRouteToRelayServer(node->GetPeerId());
}


void netConnectionRouter::RouteBroken(Node* node)
{
	ASSERT_CXNMGR_CS_LOCKED;

	if(!node->IsRouteActive())
	{
		return;
	}

#if !__NO_OUTPUT
	const unsigned curTime = sysTimer::GetSystemMsTime();
	netAssert(curTime >= node->GetLastReceiveTime());
	const unsigned timeSinceLastReceive = curTime - node->GetLastReceiveTime();
	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	netDebug("Link to %s is broken. Time since last receive: %u ms", node->GetPeerId().ToHexString(peerIdHexString), timeSinceLastReceive);
#endif

	/*
		Note: this is 6.11 case (i)

		6.11: A node initiates processing for a RERR message in three situations:

		(i) if it detects a link break for the next hop of an active
		route in its routing table while transmitting data (and
		route repair, if attempted, was unsuccessful)

		For case (i), the node first makes a list of unreachable destinations
		consisting of the unreachable neighbor and any additional
		destinations (or subnets, see section 7) in the local routing table
		that use the unreachable neighbor as the next hop.
	*/

	const netPeerId& peerId = node->GetPeerId();
	if(!netVerify(peerId.IsValid() && (m_LocalPeerId != peerId)))
	{
		return;
	}

	u8 numUnreachableDestinations = 0;
	Node* unreachableDestinations[netRouteErrMsg::MAX_UNREACHABLE_DESTINATIONS];

	NodeList::iterator it = m_Nodes.begin();
	NodeList::const_iterator stop = m_Nodes.end();
	for(; stop != it; ++it)
	{
		Node* unreachableNode = *it;
		if((unreachableNode->GetPeerId() == peerId) || (unreachableNode->GetNextHopPeerId() == peerId))
		{
			/*
				For each one of these destinations,
				the corresponding routing table entry is updated as follows:

				1. The destination sequence number of this routing entry, if it
				exists and is valid, is incremented for cases (i) and (ii) above,
				and copied from the incoming RERR in case (iii) above.

				2. The entry is invalidated by marking the route entry as invalid

				// [NS] Note: this implementation doesn't use route lifetimes.
				3. The Lifetime field is updated to current time plus DELETE_PERIOD.
				Before this time, the entry SHOULD NOT be deleted.
			*/

			if(unreachableNode->IsDestSeqValid())
			{
				unreachableNode->IncDestSeq();
			}

			// note: we mark the route as invalid in ProcessUnreachableDestinations()

			unreachableDestinations[numUnreachableDestinations] = unreachableNode;
			numUnreachableDestinations++;
		}
	}
	
	ProcessUnreachableDestinations(unreachableDestinations, numUnreachableDestinations);
}

void netConnectionRouter::RemovePrecursor(const Node* nodeToRemove)
{
	ASSERT_CXNMGR_CS_LOCKED;

	NodeList::iterator it = m_Nodes.begin();
	NodeList::const_iterator stop = m_Nodes.end();
	for(; stop != it; ++it)
	{
		Node* node = *it;
		node->RemovePrecursor(nodeToRemove);
	}
}

void netConnectionRouter::ProcessUnreachableDestinations(Node** unreachableDestinations, const unsigned numUnreachableDestinations)
{	
	ASSERT_CXNMGR_CS_LOCKED;

	if(numUnreachableDestinations == 0)
	{
		return;
	}

	netDebug1("ProcessUnreachableDestinations :: %u unreachable destinations", numUnreachableDestinations);

	netAssert(unreachableDestinations != nullptr);
	netAssert(numUnreachableDestinations <= MAX_NUM_PEERS);

	/*
		Some of the unreachable destinations in the list could be used by
		neighboring nodes, and it may therefore be necessary to send a (new)
		RERR.  The RERR should contain those destinations that are part of
		the created list of unreachable destinations and have a non-empty
		precursor list.

		The neighboring node(s) that should receive the RERR are all those
		that belong to a precursor list of at least one of the unreachable
		destination(s) in the newly created RERR.
	*/

	netPeerId unreachableDestIds[netRouteErrMsg::MAX_UNREACHABLE_DESTINATIONS];
	netConnectionRouter::SequenceNumber unreachableDestSeqs[netRouteErrMsg::MAX_UNREACHABLE_DESTINATIONS];

	netPeerId affectedPeerIds[MAX_NUM_PEERS];
	u8 numAffectedPeers = 0;

#if !__NO_OUTPUT
	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	netDebug("Unreachable destinations:");
#endif

	// go through all the unreachable nodes and make a set of all the precursor peers
	for(unsigned i = 0; i < numUnreachableDestinations; ++i)
	{
		Node* unreachableNode = unreachableDestinations[i];
		netAssert(unreachableNode);

		unreachableDestIds[i] = unreachableNode->GetPeerId();
		unreachableDestSeqs[i] = unreachableNode->GetDestSeq();

		netDebug("    %s. %u affected peers:", unreachableNode->GetPeerId().ToHexString(peerIdHexString), (unsigned)unreachableNode->GetPrecursorList().size());

		PrecursorList::iterator it = unreachableNode->GetPrecursorList().begin();
		PrecursorList::const_iterator stop = unreachableNode->GetPrecursorList().end();
		for(; stop != it; ++it)
		{
			PrecursorNode* prenode = *it;

			bool found = false;
			for(unsigned i = 0; i < numAffectedPeers; ++i)
			{
				if(affectedPeerIds[i] == prenode->GetPeerId())
				{
					found = true;
					break;
				}
			}

			if(!found)
			{
				netDebug("        %s", prenode->GetPeerId().ToHexString(peerIdHexString));
				affectedPeerIds[numAffectedPeers] = prenode->GetPeerId();
				numAffectedPeers++;
			}
		}

		unreachableNode->SetRouteActive(false);

		/*
			Note: RFC 3561 never mentions any situation in which a node is removed
			from a precursor list. This will cause precursor lists to contain stale
			information. However, from a previous draft (Draft 9, section 8.11):

			When a node invalidates a route to a neighboring node, it MUST
			also delete that neighbor from any precursor lists for routes to
			other nodes.  This prevents precursor lists from containing stale
			entries of neighbors with which the node is no longer able to
			communicate.  The node does this by inspecting the precursor list of
			each destination entry in its routing table, and deleting the lost
			neighbor from any list in which it appears.
		*/
		if(unreachableNode->GetHopCount() == 1)
		{
			RemovePrecursor(unreachableNode);
		}

		/*
			Also from Draft 9, section 8.11: delete the precursor list of each
			unreachable destination.
		*/
		unreachableNode->ClearPrecursorList();

		// we have an unreachable node, switch the connection back to relay server.
		SwitchToRelayServer(unreachableNode);
	}

	if(numAffectedPeers > 0)
	{
		// send the RERR to each affected peer
		netRouteErrMsg rerr(0, (u8)numUnreachableDestinations, unreachableDestIds, unreachableDestSeqs);

		for(unsigned i = 0; i < numAffectedPeers; ++i)
		{
			Node* affectedNode = GetNodeByPeerId(affectedPeerIds[i]);
			if(netVerify(affectedNode))
			{
				const EndpointId epId = affectedNode->GetEndpointId();
				UnicastRouteError(rerr, epId);
			}
		}
	}

#if !__NO_OUTPUT
	DumpRoutingTable();
#endif
}

void
netConnectionRouter::SetPolicies(const netConnectionPolicies& policies)
{
	// calculate emergency relay fallback timeouts based on the connection timeout policy, unless they've
	// been explicitly set via SetEmergencyRelayFallbackTimeMs() and/or SetEmergencyDirectReceiveTimeoutMs()
	// which can be called directly and controlled via tunables.
	// Note that if timeouts are disabled, we disable emergency relay fallbacks.

	OUTPUT_ONLY(const unsigned oldEmergencyRelayFallbackTimeMs = sm_EmergencyRelayFallbackTimeMs;)
	OUTPUT_ONLY(const unsigned oldEmergencyDirectReceiveTimeoutMs = sm_EmergencyDirectReceiveTimeoutMs;)

    if(policies.m_TimeoutInterval == 0)
    {
		if(!sm_EmergencyRelayFallbackTimeoutOverridden)
		{
			sm_EmergencyRelayFallbackTimeMs = 0;
		}

		if(!sm_EmergencyDirectReceiveTimeoutOverridden)
		{
			sm_EmergencyDirectReceiveTimeoutMs = 0;
		}
    }
	else
	{
		CompileTimeAssert(DEFAULT_EMERGENCY_RELAY_FALLBACK_TIME_MS < netConnectionPolicies::DEFAULT_TIMEOUT_INTERVAL);
		if(!sm_EmergencyRelayFallbackTimeoutOverridden)
		{
			const float defaultFractionOfCxnTimeout = (float)DEFAULT_EMERGENCY_RELAY_FALLBACK_TIME_MS / (float)netConnectionPolicies::DEFAULT_TIMEOUT_INTERVAL;
			sm_EmergencyRelayFallbackTimeMs = (unsigned)(policies.m_TimeoutInterval * defaultFractionOfCxnTimeout);
		}

		CompileTimeAssert(DEFAULT_EMERGENCY_DIRECT_RECEIVE_TIMEOUT_MS < netConnectionPolicies::DEFAULT_TIMEOUT_INTERVAL);
		if(!sm_EmergencyDirectReceiveTimeoutOverridden)
		{
			const float defaultFractionOfCxnTimeout = (float)DEFAULT_EMERGENCY_DIRECT_RECEIVE_TIMEOUT_MS / (float)netConnectionPolicies::DEFAULT_TIMEOUT_INTERVAL;
			sm_EmergencyDirectReceiveTimeoutMs = (unsigned)(policies.m_TimeoutInterval * defaultFractionOfCxnTimeout);
		}
	}

	netDebug2("Setting policies:");
	netDebug2("  Emergency relay fallback timeout: %u (was %u)",
		sm_EmergencyRelayFallbackTimeMs,
		oldEmergencyRelayFallbackTimeMs);

	netDebug2("  Emergency direct receive relay fallback timeout: %u (was %u)",
		sm_EmergencyDirectReceiveTimeoutMs,
		oldEmergencyDirectReceiveTimeoutMs);
}

void netConnectionRouter::AddEndpoint(const EndpointId endpointId)
{
	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	Node* node = nullptr;

	std::pair<NodesByPeerId::iterator, bool> byPeerIdResult;
	byPeerIdResult.second = false;

	rtry
	{
		rcheckall(IsConnectionRoutingEnabled());

		const netAddress& addr = m_CxnMgr->GetAddress(endpointId);
		rverifyall(addr.IsValid());

		const netPeerAddress& peerAddr = m_CxnMgr->GetPeerAddress(endpointId);
		rcheckall(peerAddr.IsValid());

		const netPeerId& peerId = peerAddr.GetPeerId();
		rcheckall(peerId.IsValid());

#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		netDebug1("AddEndpoint : adding endpoint for peer %s", peerAddr.ToString());
#endif

		node = GetNodeByPeerId(peerId);
		if(node)
		{
			netDebug("AddEndpoint : already have endpoint for peerId %s", peerId.ToHexString(peerIdHexString));
			return;
		}

		// check the address type only after ensuring we don't already have this endpoint - we may already
		// have this endpoint, connected via a peer relay address and don't want to assert in that case
		rverifyall(addr.IsDirectAddr() || addr.IsRelayServerAddr());

		node = (Node*)this->Alloc(sizeof(Node));
		rverifyall(node);

		Node::Route route = addr.IsRelayServerAddr() ? Node::RelayServer : Node::Direct;

		new (node) Node(m_Allocator, endpointId, peerId, route);

		// give some time for everyone else to add the endpoint before searching for a peer relay route
		const unsigned curTime = sysTimer::GetSystemMsTime();
		node->SetLastAddressAssignmentTime(curTime);
		node->SetNextDiscoveryTime(curTime + MIN_ROUTE_DISCOVERY_INTERVAL_MS);

		if(route == Node::Direct)
		{
			node->SetHopCount(1);
			node->SetRouteActive(true);
			node->SetNextHopPeerId(peerId);
			NewConnectionEstablished();
		}

		OUTPUT_ONLY(node->DumpRoutingEntry());

		byPeerIdResult = m_NodesByPeerId.insert(peerId, node);

		rverify(byPeerIdResult.second, catchall, netError("Failed to insert node with endpointId %u into NodesByPeerId map", endpointId));

		m_Nodes.push_back(node);
	}
	rcatchall
	{
		if(byPeerIdResult.second)
		{
			m_NodesByPeerId.erase(node);
		}

		if(node)
		{
			node->~Node();
			Free(node);
			node = nullptr;
		}
	}
}

void netConnectionRouter::NewConnectionEstablished()
{
	ASSERT_CXNMGR_CS_LOCKED;

	// Route discovery triggering heuristic:
	// We have a new direct or peer relay connection to a destination peer.
	// This new connection might be able to bridge us to other nodes. To help speed up
	// the discovery process, schedule a route discovery for the near future if
	// we would have otherwise waited longer.
	//
	// For example, if A is connected to C via relay server for a long period of time,
	// they would only be trying to discover a peer relay route after several minutes, 
	// due to exponential backoff. If a new peer B joins the session, B might be able
	// to act as a bridge from A <-> B <-> C, so have C start discovery earlier.
	// However, this means that every time a new node connects, we may trigger route
	// discoveries that still fail. Only allow a single attempt at discovery in the
	// near future, after enough time has passed to allow the new node to complete
	// NAT traversal with all other nodes (30+ seconds).

	unsigned curTime = sysTimer::GetSystemMsTime();

	NodeList::iterator it = m_Nodes.begin();
	NodeList::const_iterator stop = m_Nodes.end();
	for(; stop != it; ++it)
	{
		Node* node = *it;

		const netPeerId& destPeerId = node->GetPeerId();
		if(m_LocalPeerId < destPeerId)
		{
			continue;
		}

		const unsigned nextDiscoveryTime = curTime + NEW_CXN_ROUTE_DISCOVERY_TIME_MS;

		if((node->GetNextDiscoveryTime() > nextDiscoveryTime) &&
			((node->GetRoute() == Node::RelayServer) ||
			((node->GetRoute() == Node::PeerRelay) && !node->IsRouteActive())))
		{
#if !__NO_OUTPUT
			char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
			netDebug1("NewConnectionEstablished : next discovery time for peerId %s would be in %u ms. Allow one attempt in %u ms",
				node->GetPeerId().ToHexString(peerIdHexString),
				node->GetNextDiscoveryTime() - curTime,
				NEW_CXN_ROUTE_DISCOVERY_TIME_MS);
#endif
			
			node->SetNextDiscoveryTime(nextDiscoveryTime);
			node->SetNextDiscoveryTtl(TTL_START);
		}
	}
}

void netConnectionRouter::UpdateEndpoint(const netPeerId& peerId, const EndpointId endpointId)
{
	PROFILE

	if(m_CxnMgr == nullptr)
	{
		return;
	}	

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	rtry
	{

#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
#endif

		netDebug1("UpdateEndpoint : updating endpoint for peerId %s", peerId.ToHexString(peerIdHexString));

		// make sure we already have this node 
		Node* node = GetNodeByPeerId(peerId);
		rcheck(node, catchall, netDebug("Can't update route for unknown node"));

		const netAddress& addr = m_CxnMgr->GetAddress(endpointId);
		rcheckall(addr.IsValid());

		const unsigned curTime = sysTimer::GetSystemMsTime();
		node->SetLastAddressAssignmentTime(curTime);

		bool pullingOffRelayServer = (node->GetRoute() == Node::RelayServer) && (!addr.IsRelayServerAddr());

		if(addr.IsDirectAddr())
		{
			node->SetRoute(Node::Direct);
			node->SetHopCount(1);
			node->SetRouteActive(true);
			node->SetNextHopPeerId(peerId);
		}
		else if(addr.IsRelayServerAddr())
		{
			if(node->IsRouteActive())
			{
				netDebug1("UpdateEndpoint : switched to relay server while route is marked as valid");
				RouteBroken(node);
			}

			netAssert(node->GetPrecursorList().empty());

			node->SetRoute(Node::RelayServer);
			node->SetHopCount(0);
			node->SetRouteActive(false);
			node->SetNextHopPeerId(netPeerId::INVALID_PEER_ID);
			node->ClearPrecursorList();

			// restart route discovery cycle
			node->SetNextDiscoveryTime(curTime + MIN_ROUTE_DISCOVERY_INTERVAL_MS);
			node->ResetNumDiscoveryAttempts();
		}
		else if(addr.IsPeerRelayAddr())
		{
			// route should already be recorded in our routing table
			node->SetRoute(Node::PeerRelay);
			netAssert(node->IsRouteActive());
			netAssert(node->GetHopCount() >= 2);
			netAssert(node->GetNextHopPeerId().IsValid());
		}

		OUTPUT_ONLY(node->DumpRoutingEntry());

		if(pullingOffRelayServer)
		{
			// we switched a route from relay server to direct or peer relay
			NewConnectionEstablished();
		}
	}
	rcatchall
	{

	}
}

void netConnectionRouter::RemoveEndpoint(const netPeerId& peerId, const EndpointId UNUSED_PARAM(endpointId))
{
	if(m_CxnMgr == nullptr)
	{
		return;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	rtry
	{
		rcheckall(IsConnectionRoutingEnabled());

#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		netDebug1("RemoveEndpoint : removing endpoint for peerId %s", peerId.ToHexString(peerIdHexString));
#endif

		Node* node = GetNodeByPeerId(peerId);
		rcheck(node, catchall, netDebug1("Cannot remove unknown endpoint"));

		RouteBroken(node);

		// erase this node from all precursor lists
		RemovePrecursor(node);

		m_NodesByPeerId.erase(node);
		m_Nodes.erase(node);

		// switch this endpoint back to relay server so if we reconnect 
		// we won't think we already have a valid peer relay route
		SwitchToRelayServer(node);

		node->~Node();
		Free(node);
		node = nullptr;
	}
	rcatchall
	{

	}
}

bool
netConnectionRouter::ResetP2pPacketHeader(netPeerRelayPacket* relayPkt,
										  const netPeerId& destPeerId,
										  const unsigned sizeofPayload)
{
	rtry
	{
		rverify(sizeofPayload > 0,
				catchall,
				netError("ResetP2pPacketHeader :: No payload"));

		relayPkt->ResetP2pForwardHeader(sizeofPayload, m_LocalPeerId, destPeerId);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

#if !__NO_OUTPUT
void netConnectionRouter::DumpRoutingTable() const
{
	ASSERT_CXNMGR_CS_LOCKED;

	netDebug1("Routing Table (%u nodes):", (unsigned)m_Nodes.size());
	NodeList::const_iterator it = m_Nodes.begin();
	NodeList::const_iterator stop = m_Nodes.end();
	for(; stop != it; ++it)
	{
		const Node* node = *it;
		node->DumpRoutingEntry();
	}
}

void netConnectionRouter::ReportMemory() const
{
	static size_t lastReportedSize = 0;
	if(sm_MaxMemoryUsed > lastReportedSize)
	{
		netDebug1("Max Memory Used: %" SIZETFMT "u bytes", sm_MaxMemoryUsed);
		lastReportedSize = sm_MaxMemoryUsed;
	}
}
#endif

netConnectionRouter::Node* netConnectionRouter::GetNodeByPeerId(const netPeerId& peerId)
{
	if(m_CxnMgr == nullptr)
	{
		return nullptr;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	netAssert(peerId.IsValid());

	NodesByPeerId::iterator it = m_NodesByPeerId.find(peerId);

	if(m_NodesByPeerId.end() != it)
	{
		return it->second;
	}

	return nullptr;
}

const netConnectionRouter::Node* netConnectionRouter::GetNodeByPeerId(const netPeerId& peerId) const
{
	return const_cast<netConnectionRouter*>(this)->GetNodeByPeerId(peerId);
}

bool netConnectionRouter::GetNextHopPeerId(const netPeerId& peerId, netPeerId* nextHopPeerId) const
{
	if(m_CxnMgr == nullptr)
	{
		return false;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	netAssert(peerId.IsValid());

	NodesByPeerId::const_iterator it = m_NodesByPeerId.find(peerId);

	if(m_NodesByPeerId.end() != it)
	{
		*nextHopPeerId = it->second->GetNextHopPeerId();
		return true;
	}

	return false;
}

netConnectionRouter::Node* netConnectionRouter::GetNextHopNode(const netPeerId& peerId)
{
	if(m_CxnMgr == nullptr)
	{
		return nullptr;
	}

	NET_CS_SYNC(m_CxnMgr->GetCriticalSectionToken());

	netPeerId nextHopPeerId;
	if(GetNextHopPeerId(peerId, &nextHopPeerId) && nextHopPeerId.IsValid())
	{
		NodesByPeerId::iterator it = m_NodesByPeerId.find(nextHopPeerId);

		if(m_NodesByPeerId.end() != it)
		{
			return it->second;
		}
	}

	return nullptr;
}

void
netConnectionRouter::OnCxnEvent(netConnectionManager* UNUSED_PARAM(cxnMgr), const netEvent* evt)
{
	if(IsConnectionRoutingEnabled() && (NET_EVENT_FRAME_RECEIVED == evt->GetId()))
	{
		const netEventFrameReceived* fr = evt->m_FrameReceived;
		const ChannelId channelId = fr->m_ChannelId;
		if(channelId == NET_RESERVED_CHANNEL_ID)
		{
			unsigned msgId;

			if(netMessage::GetId(&msgId, fr->m_Payload, fr->m_SizeofPayload))
			{
				if(netRouteRequestMsg::MSG_ID() == msgId)
				{
					netRouteRequestMsg msg;
					if(netVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload)))
					{
						ReceiveRouteRequest(&msg, fr->GetEndpointId(), fr->m_Sender);
					}
				}
				else if(netRouteReplyMsg::MSG_ID() == msgId)
				{
					netRouteReplyMsg msg;
					if(netVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload)))
					{
						ReceiveRouteReply(&msg, fr->GetEndpointId(), fr->m_Sender);
					}
				}
				else if(netRouteErrMsg::MSG_ID() == msgId)
				{
					netRouteErrMsg msg;
					if(netVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload)))
					{
						ReceiveRouteError(&msg, fr->GetEndpointId(), fr->m_Sender);
					}
				}
				else if(netRouteChangeRequestMsg::MSG_ID() == msgId)
				{
					netRouteChangeRequestMsg msg;
					if(netVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload)))
					{
						ReceiveRouteChangeRequest(&msg, fr->GetEndpointId(), fr->m_Sender);
					}
				}
				else if(netRouteChangeReplyMsg::MSG_ID() == msgId)
				{
					netRouteChangeReplyMsg msg;
					if(netVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload)))
					{
						ReceiveRouteChangeReply(&msg, fr->GetEndpointId(), fr->m_Sender);
					}
				}
			}
		}
	}
}

bool netConnectionRouter::BlacklistRouteRequest(const netRouteRequestMsg& msg)
{
	ASSERT_CXNMGR_CS_LOCKED;

	// note: this function returns true if the route request should be discarded

	rtry
	{
		rverifyall(msg.m_SrcId.IsValid())

		// if this route request is already blacklisted, update its valid-until time
		RouteRequestId* blackListedRqst = nullptr;

		RouteRequestBlacklist::iterator it = m_RouteRequestBlacklist.begin();
		RouteRequestBlacklist::const_iterator stop = m_RouteRequestBlacklist.end();
		for(; stop != it; ++it)
		{
			RouteRequestId* rqst = *it;
			if((rqst->m_SrcPeerId == msg.m_SrcId) &&
				(rqst->m_SrcRouteRequestId == msg.m_RouteRequestId))
			{
				blackListedRqst = rqst;
				break;
			}
		}
		
		const unsigned validUntilTime = sysTimer::GetSystemMsTime() + PATH_DISCOVERY_TIME_MS;

		if(blackListedRqst)
		{
			blackListedRqst->m_ValidUntilTime = validUntilTime;
		}
		else
		{
			RouteRequestId* rqst = (RouteRequestId*)this->Alloc(sizeof(RouteRequestId));
			rverify(rqst, catchall, netError("Failed to add route request id to blacklist - which means we can't detect cycles. Discard route request."));
			new (rqst) RouteRequestId(msg.m_SrcId, msg.m_RouteRequestId, validUntilTime);
			m_RouteRequestBlacklist.push_back(rqst);
			return false; // false means we don't discard the route request
		}

		return true;
	}
	rcatchall
	{
		return true;
	}
}

void netConnectionRouter::RemoveBlacklistedRouteRequest(const netRouteRequestMsg& msg)
{
	ASSERT_CXNMGR_CS_LOCKED;

	if(m_RouteRequestBlacklist.empty())
	{
		return;
	}

	RouteRequestBlacklist::iterator it = m_RouteRequestBlacklist.begin();
	RouteRequestBlacklist::iterator next = it;
	RouteRequestBlacklist::const_iterator stop = m_RouteRequestBlacklist.end();

	for(++next; stop != it; it = next, ++next)
	{
		RouteRequestId* rqst = *it;
		if((rqst->m_SrcPeerId == msg.m_SrcId) &&
			(rqst->m_SrcRouteRequestId == msg.m_RouteRequestId))
		{
			m_RouteRequestBlacklist.erase(rqst);

			rqst->~RouteRequestId();
			Free(rqst);
			rqst = nullptr;

			return;
		}
	}
}

void netConnectionRouter::UpdateRouteRequestBlacklist()
{
	PROFILE

	ASSERT_CXNMGR_CS_LOCKED;	

	if(m_RouteRequestBlacklist.empty())
	{
		return;
	}

	netAssert(IsPeerRelayEnabled());

	const unsigned curTime = sysTimer::GetSystemMsTime();

	// delete any expired entries
	RouteRequestBlacklist::iterator it = m_RouteRequestBlacklist.begin();
	RouteRequestBlacklist::iterator next = it;
	RouteRequestBlacklist::const_iterator stop = m_RouteRequestBlacklist.end();

	for(++next; stop != it; it = next, ++next)
	{
		RouteRequestId* rqst = *it;
		if(curTime > rqst->m_ValidUntilTime)
		{
			m_RouteRequestBlacklist.erase(rqst);

			rqst->~RouteRequestId();
			Free(rqst);
			rqst = nullptr;
		}
	}
}

void netConnectionRouter::BlacklistPeer(const netPeerId& peerId)
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
		rverifyall(peerId.IsValid());

		Node* node = GetNodeByPeerId(peerId);
		rcheck(node, catchall, netDebug("Cannot blacklist non-existant peer."));

		const unsigned curTime = sysTimer::GetSystemMsTime();

		rcheck(!node->IsBlacklisted(), catchall, netDebug("Peer is already blacklisted. Will be removed from the blacklist in %u ms.", node->GetBlacklistExpirationTime() - curTime));

		// this node will be blacklisted for (MIN_BLACKLIST_TIME_MS * 2^N) ms, where
		// N is the number of times this node has been blacklisted by the local node.
		const unsigned blacklistDuration = (node->GetBlacklistCount() == 0) ?
											MIN_BLACKLIST_TIME_MS :
											Min(MAX_BLACKLIST_TIME_MS,
												MIN_BLACKLIST_TIME_MS * (2 << (node->GetBlacklistCount() - 1)));

#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		netDebug1("BlacklistPeer :: blacklisting peer %s for %u ms. Peer has been blacklisted %u times prior.", peerId.ToHexString(peerIdHexString), blacklistDuration, node->GetBlacklistCount());
#endif

		const unsigned expirationTime = curTime + blacklistDuration;
		node->SetBlacklistExpirationTime(expirationTime);
		node->IncBlacklistCount();
	}
	rcatchall
	{

	}
}

void netConnectionRouter::UpdatePeerBlacklist()
{
	PROFILE

	ASSERT_CXNMGR_CS_LOCKED;	

	const unsigned curTime = sysTimer::GetSystemMsTime();

	NodeList::iterator it = m_Nodes.begin();
	NodeList::const_iterator stop = m_Nodes.end();
	for(; stop != it; ++it)
	{
		Node* node = *it;
		if(node->IsBlacklisted() && (curTime >= node->GetBlacklistExpirationTime()))
		{
#if !__NO_OUTPUT
			char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
			netDebug1("UpdatePeerBlacklist :: Removing peer from blacklist: PeerId:%s", node->GetPeerId().ToHexString(peerIdHexString));
#endif

			node->SetBlacklistExpirationTime(0);
		}
	}
}

bool netConnectionRouter::IsPeerBlacklisted(const netPeerId& peerId) const
{
	ASSERT_CXNMGR_CS_LOCKED;

	rtry
	{
		rverifyall(peerId.IsValid());

		const Node* node = GetNodeByPeerId(peerId);
		rcheckall(node);

		return node->IsBlacklisted();
	}
	rcatchall
	{
		return false;
	}
}

void netConnectionRouter::UpdateRoutePairs()
{
	ASSERT_CXNMGR_CS_LOCKED;

	const unsigned curTime = sysTimer::GetSystemMsTime();

	RoutePairsMap::iterator it = m_RoutePairs.begin();
	RoutePairsMap::iterator next = it;
	RoutePairsMap::const_iterator stop = m_RoutePairs.end();

	for(++next; stop != it; it = next, ++next)
	{
		RoutePair::Data* data = it->second;
	
		const unsigned lastForwardTime = data->m_LastForwardTime;
		netAssert(curTime >= lastForwardTime);
		const unsigned elapsed = curTime - lastForwardTime;

		// sm_EmergencyRelayFallbackTimeMs can be 0 when emergency relay fallback is disabled.
		// Make sure we have a non-zero route reservation time.
		const unsigned emergencyRelayFallbackTimeMs = sm_EmergencyRelayFallbackTimeMs > 0 ?
													  sm_EmergencyRelayFallbackTimeMs :
													  DEFAULT_EMERGENCY_RELAY_FALLBACK_TIME_MS;

		const unsigned maxRouteReserveTime = (data->m_Established == 1) ?
											 emergencyRelayFallbackTimeMs :
											 emergencyRelayFallbackTimeMs / 2;

		if(elapsed > maxRouteReserveTime)
		{
#if !__NO_OUTPUT
			char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
			char peerIdHexString2[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
			const RoutePair::Key pair = it->first;
#endif

			bool established = data->m_Established != 0;

			m_RoutePairs.erase(it);
			m_RoutePairsPool.push_back(data);

			if(established)
			{
				if(netVerify(m_NumForwardedConnections > 0))
				{
					--m_NumForwardedConnections;
				}

				netDebug1("No longer forwarding packets from %s to %s. Now relaying %u connections (Max was: %u). Num used slots: %u.",
							pair.m_SrcPeerId.ToHexString(peerIdHexString),
							pair.m_DestPeerId.ToHexString(peerIdHexString2),
							m_NumForwardedConnections,
							m_MaxForwardedConnections,
							(unsigned)m_RoutePairs.size());
			}
			else
			{
				netDebug1("No longer reserving forwarding route from %s to %s. Num used slots: %u.",
							pair.m_SrcPeerId.ToHexString(peerIdHexString),
							pair.m_DestPeerId.ToHexString(peerIdHexString2),
							(unsigned)m_RoutePairs.size());

			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//  RouteRequestId
//////////////////////////////////////////////////////////////////////////
netConnectionRouter::RouteRequestId::RouteRequestId(const netPeerId& srcPeerId, const netConnectionRouter::SequenceNumber& srcRouteRequestId, const unsigned validUntilTime)
: m_SrcPeerId(srcPeerId)
, m_SrcRouteRequestId()
, m_ValidUntilTime(validUntilTime)
{
	m_SrcRouteRequestId = srcRouteRequestId;
}

//////////////////////////////////////////////////////////////////////////
//  SequenceNumber
//////////////////////////////////////////////////////////////////////////
netConnectionRouter::SequenceNumber::SequenceNumber()
: m_Seq(0)
{

}

const netConnectionRouter::SequenceNumber& netConnectionRouter::SequenceNumber::operator=(const SequenceNumber& rhs)
{
	netAssert(rhs.m_Seq >= m_Seq);
	Max(rhs);
	return *this;
}

void netConnectionRouter::SequenceNumber::Inc()
{
	m_Seq++;
}

void netConnectionRouter::SequenceNumber::Max(const SequenceNumber& s)
{
	if(s.m_Seq > this->m_Seq)
	{
		this->m_Seq = s.m_Seq;
	}
}

unsigned netConnectionRouter::SequenceNumber::Get() const
{
	return m_Seq;
}

bool netConnectionRouter::SequenceNumber::Export(void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/) const
{
	rtry
	{
		datExportBuffer bb;
		bb.SetReadWriteBytes(buf, sizeofBuf);

		rverify(bb.SerUns(m_Seq, sizeof(m_Seq) << 3), catchall, );

		netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

		if(size){*size = bb.GetNumBytesWritten();}

		return true;
	}
	rcatchall
	{
		if(size){*size = 0;}
		return false;
	}
}

bool netConnectionRouter::SequenceNumber::Import(const void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/)
{
	rtry
	{
		datImportBuffer bb;
		bb.SetReadOnlyBytes(buf, sizeofBuf);

		rverify(m_Seq == 0, catchall, netError("SequenceNumber::Import would modify an existing sequence number"));

		unsigned seq = 0;
		rverify(bb.SerUns(seq, sizeof(m_Seq) << 3), catchall, );

		// make doubly sure we're not decrementing an existing sequence number
		if(netVerify(seq >= m_Seq))
		{
			m_Seq = seq;
		}

		if(size){*size = bb.GetNumBytesRead();}

		return true;
	}
	rcatchall
	{
		if(size){*size = 0;}
		return false;
	}
}

bool netConnectionRouter::SequenceNumber::operator==(const SequenceNumber& that) const
{
	return this->m_Seq == that.m_Seq;
}

bool netConnectionRouter::SequenceNumber::operator!=(const SequenceNumber& that) const
{
	return this->m_Seq != that.m_Seq;
}

bool netConnectionRouter::SequenceNumber::operator<(const SequenceNumber& that) const
{
	return this->m_Seq < that.m_Seq;
}

bool netConnectionRouter::SequenceNumber::operator<=(const SequenceNumber& that) const
{
	return this->m_Seq <= that.m_Seq;
}

bool netConnectionRouter::SequenceNumber::operator>(const SequenceNumber& that) const
{
	return this->m_Seq > that.m_Seq;
}

bool netConnectionRouter::SequenceNumber::operator>=(const SequenceNumber& that) const
{
	return this->m_Seq >= that.m_Seq;
}

//////////////////////////////////////////////////////////////////////////
//  RoutePair
//////////////////////////////////////////////////////////////////////////
netConnectionRouter::RoutePair::RoutePair(const netPeerId& srcPeerId, const netPeerId& destPeerId)
: m_Key(srcPeerId, destPeerId)
{

}

bool netConnectionRouter::RoutePair::Key::operator<(const RoutePair::Key& that) const
{
	return std::tie(m_SrcPeerId, m_DestPeerId) < std::tie(that.m_SrcPeerId, that.m_DestPeerId);
}

bool netConnectionRouter::RoutePair::Key::operator==(const RoutePair::Key& that) const
{
	return (m_SrcPeerId == that.m_SrcPeerId) && (m_DestPeerId == that.m_DestPeerId);
}

}   //namespace rage
