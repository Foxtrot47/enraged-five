// 
// net/soap.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "soap.h"

#include "diag/channel.h"
#include "diag/output.h"
#include "string/string.h"
#include "system/memory.h"
#include "system/param.h"

namespace rage
{

RAGE_DECLARE_CHANNEL(ragenet);
RAGE_DEFINE_SUBCHANNEL(ragenet, soap)
#define __rage_channel ragenet_soap

#if defined(netDebug)
#undef netDebug
#undef netDebug3
#undef netWarning
#undef netError
#endif

#define netDebug    rageDebugf1
#define netDebug3   rageDebugf3
#define netWarning  rageWarningf
#define netError    rageErrorf

//////////////////////////////////////////////////////////////////////////
//  netSoapReader
//////////////////////////////////////////////////////////////////////////
static const char* SkipWs(const char* buf, const char* eob)
{
    for(; buf < eob && isspace(*buf); ++buf)
    {
    }

    return buf;
}

static const char* SkipString(const char* buf, const char* eob)
{
    const char* p = buf;

    if(p < eob && '\"' == *p)
    {
        for(++p; p < eob && '\"' != *p; ++p)
        {
        }

        if(p < eob)
        {
            ++p;
        }
    }

    return p;
}

static const char* GetTagName(const char* buf, const char* eob)
{
    const char* p = buf;

    if(p < eob && netVerify('<' == *p))
    {
        //Skip the namespace
        while(p < eob && ':' != *p && '>' != *p)
        {
            if('\"' == *p)
            {
                p = SkipString(p, eob);
            }
            else
            {
                ++p;
            }
        }

        if(p < eob && ':' == *p)
        {
            ++p;
        }
        else
        {
            p = buf+1;
        }
    }

    return p;
}

static const char* SkipTag(const char* buf, const char* eob)
{
    netAssert('<' == *buf);

    for(; buf < eob && '>' != *buf; ++buf)
    {
    }

    if(buf < eob && '>' == *buf)
    {
        ++buf;
    }

    return buf;
}

static const char* SkipHeader(const char* buf, const char* eob)
{
    if(buf+1 < eob && '<' == buf[0] && '?' == buf[1])
    {
        for(; buf < eob - 1; ++buf)
        {
            if('?' == buf[0] && '>' == buf[1])
            {
                buf += 2;
                break;
            }
        }
    }

    return buf;
}

static const char* FindEndTag(const char* buf, const char* eob)
{
    const char* p = eob;

    if(buf < eob && netVerify('<' == *buf))
    {
        p = SkipTag(buf, eob);

        if(p < eob)
        {
            netAssert(p-buf >= 2);

            if('/' == p[-2])
            {
                p = buf;
            }
            else
            {
                for(; p < eob && '<' != *p; ++p)
                {
                }

                while(p+1 < eob && '/' != p[1])
                {
                    p = SkipTag(FindEndTag(p, eob), eob);
                }
            }
        }
    }

    return p;
}

static bool GetContentExtents(const char* buf, const char* eob,
                                const char** start, const char** end)
{
    bool success = false;

    if(buf < eob && netVerify('<' == *buf))
    {
        *start = SkipTag(buf, eob);
        *end = FindEndTag(buf, eob);

        if(*start < eob && *end < eob)
        {
            success = true;
        }
    }

    return success;
}

netSoapReader::netSoapReader()
: m_Buf(NULL)
, m_Eob(NULL)
{
}

netSoapReader::~netSoapReader()
{
}

void
netSoapReader::Clear()
{
    m_Buf = NULL;
    m_Eob = NULL;
}

bool
netSoapReader::Reset(const char* buf, const unsigned offset, const unsigned len)
{
    return this->Reset(buf + offset, buf + offset + len);
}

bool
netSoapReader::Reset(const char* buf, const char* endOfBuf)
{
    bool success = false;

    this->Clear();

    if(netVerify(buf <= endOfBuf))
    {
        m_Buf = SkipHeader(buf, endOfBuf);

        while(m_Buf < m_Eob && '<' != *m_Buf)
        {
            ++m_Buf;
        }

        m_Eob = SkipTag(FindEndTag(m_Buf, endOfBuf), endOfBuf);

        if(m_Eob > m_Buf)
        {
            success = true;
        }
        else
        {
            this->Clear();
        }
    }

    return success;
}

const char*
netSoapReader::GetSoap() const
{
    return m_Buf;
}

unsigned
netSoapReader::GetLength() const
{
    return m_Eob - m_Buf;
}

bool
netSoapReader::GetElement(const char* name, netSoapReader* reader) const
{
    bool success = false;

    netSoapReader tmp;

    if(this->GetFirst(&tmp))
    {
        bool done = false;
        const unsigned namelen = strlen(name);

        while(!done)
        {
            const char* p = GetTagName(tmp.m_Buf, tmp.m_Eob);
            if(p >= tmp.m_Eob)
            {
                done = true;
            }
            else if(!strncmp(p, name, namelen))
            {
                *reader = tmp;
                success = done = true;
            }
            else if(!this->GetNext(tmp, &tmp))
            {
                done = true;
            }
        }
    }

    return success;
}

bool
netSoapReader::GetFirst(netSoapReader* reader) const
{
    bool success = false;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
        const char* p = SkipWs(SkipTag(m_Buf, m_Eob), m_Eob);
        if(p < m_Eob && '<' == *p)
        {
            const char* eob = SkipTag(FindEndTag(p, m_Eob), m_Eob);
            success = reader->Reset(p, eob);
        }
    }

    return success;
}

bool
netSoapReader::GetNext(const netSoapReader& current,
                        netSoapReader* next) const
{
    bool success = false;

    if(m_Buf < m_Eob
        && netVerify('<' == *m_Buf)
        && netVerify(current.m_Buf >= m_Buf)
        && netVerify(current.m_Eob <= m_Eob))
    {
        const char* p = SkipWs(current.m_Eob, m_Eob);

        if(p < m_Eob && netVerify('<' == *p))
        {
            if(p+1 < m_Eob && '/' == p[1])
            {
            }
            else
            {
                success = next->Reset(p, SkipTag(FindEndTag(p, m_Eob), m_Eob));
            }
        }
    }

    return success;
}

unsigned
netSoapReader::GetName(char* name,
                        const unsigned maxChars) const
{
    unsigned len = 0;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
        const char* p = GetTagName(m_Buf, m_Eob);
        const char* eon = p;

        for(; eon < m_Eob && !isspace(*eon); ++eon)
        {
        }

        if(eon < m_Eob)
        {
            const unsigned namelen = eon - p + 1;
            len = namelen > maxChars ? maxChars : namelen;
            safecpy(name, p, len);
        }
    }

    return len;
}

const char*
netSoapReader::GetContent(unsigned* length) const
{
    const char* p;
    const char* eoc;
    if(GetContentExtents(m_Buf, m_Eob, &p, &eoc))
    {
        *length = eoc - p;
    }
    else
    {
        *length = 0;
        p = NULL;
    }

    return p;
}

bool
netSoapReader::AsInt(int& val) const
{
    bool success = false;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
        const char* p = SkipWs(SkipTag(m_Buf, m_Eob), m_Eob);
        if(p < m_Eob && '<' != *p)
        {
            if(p+2 < m_Eob && '0' == p[0] && ('x' == p[1] || 'X' == p[1]))
            {
                success = (1 == sscanf(p, "0x%X", &val));
            }
            else
            {
                success = (1 == sscanf(p, "%i", &val));

                if(!success)
                {
                    success = (1 == sscanf(p, "%X", &val));
                }
            }
        }
    }

    return success;
}

bool
netSoapReader::AsInt64(s64& val) const
{
    bool success = false;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
        const char* p = SkipWs(SkipTag(m_Buf, m_Eob), m_Eob);
        if(p < m_Eob && '<' != *p)
        {
            if(p+2 < m_Eob && '0' == p[0] && ('x' == p[1] || 'X' == p[1]))
            {
                success = (1 == sscanf(p, "0x%" I64FMT "X", &val));
            }
            else
            {
                success = (1 == sscanf(p, "%" I64FMT "d", &val));

                if(!success)
                {
                    success = (1 == sscanf(p, "%" I64FMT "X", &val));
                }
            }
        }
    }

    return success;
}

bool
netSoapReader::AsUns(unsigned& val) const
{
    bool success = false;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
        const char* p = SkipWs(SkipTag(m_Buf, m_Eob), m_Eob);
        if(p < m_Eob && '<' != *p)
        {
            if(p+2 < m_Eob && '0' == p[0] && ('x' == p[1] || 'X' == p[1]))
            {
                success = (1 == sscanf(p, "0x%X", &val));
            }
            else
            {
                success = (1 == sscanf(p, "%u", &val));

                if(!success)
                {
                    success = (1 == sscanf(p, "%X", &val));
                }
            }
        }
    }

    return success;
}

bool
netSoapReader::AsUns64(u64& val) const
{
    bool success = false;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
        const char* p = SkipWs(SkipTag(m_Buf, m_Eob), m_Eob);
        if(p < m_Eob && '<' != *p)
        {
            if(p+2 < m_Eob && '0' == p[0] && ('x' == p[1] || 'X' == p[1]))
            {
                success = (1 == sscanf(p, "0x%" I64FMT "X", &val));
            }
            else
            {
                success = (1 == sscanf(p, "%" I64FMT "u", &val));

                if(!success)
                {
                    success = (1 == sscanf(p, "%" I64FMT "X", &val));
                }
            }
        }
    }

    return success;
}

bool
netSoapReader::AsFloat(float& val) const
{
    bool success = false;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
        const char* p = SkipWs(SkipTag(m_Buf, m_Eob), m_Eob);
        if(p < m_Eob && '<' != *p)
        {
            success = (1 == sscanf(p, "%g", &val));
        }
    }

    return success;
}

bool
netSoapReader::AsDouble(double& val) const
{
    bool success = false;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
        const char* p = SkipWs(SkipTag(m_Buf, m_Eob), m_Eob);
        if(p < m_Eob && '<' != *p)
        {
            success = (1 == sscanf(p, "%lg", &val));
        }
    }

    return success;
}

/*bool
netSoapReader::AsBinary(void* val, const int maxLen, int* len) const
{
    bool success = false;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
    }

    return success;
}*/

bool
netSoapReader::AsBool(bool& val) const
{
    bool success = false;

    if(m_Buf < m_Eob && netVerify('<' == *m_Buf))
    {
        const char* p = SkipWs(SkipTag(m_Buf, m_Eob), m_Eob);

        if(p+4 < m_Eob
            && ('t' == p[0] || 'T' == p[0])
            && ('r' == p[1] || 'R' == p[1])
            && ('u' == p[2] || 'U' == p[2])
            && ('e' == p[3] || 'E' == p[3]))
        {
            success = true;
            val = true;
        }
        else if(p+5 < m_Eob
                && ('f' == p[0] || 'F' == p[0])
                && ('a' == p[1] || 'A' == p[1])
                && ('l' == p[2] || 'L' == p[2])
                && ('s' == p[3] || 'S' == p[3])
                && ('e' == p[4] || 'E' == p[4]))
        {
            success = true;
            val = false;
        }
    }

    return success;
}

unsigned
netSoapReader::AsString(char* val, const unsigned maxChars) const
{
    unsigned len = 0;
    unsigned clen = 0;

    const char* content = this->GetContent(&clen);

    if(clen > 0 && clen < maxChars)
    {
        len = maxChars;
        if(!netHttpRequest::XmlDecode(content, clen, val, &len))
        {
            len = 0;
        }
    }

    val[len] = '\0';

    return len;
}

template<>
bool
netSoapReader::AsValue(int& value) const
{
    return this->AsInt(value);
}

template<>
bool
netSoapReader::AsValue(s64& value) const
{
    return this->AsInt64(value);
}

template<>
bool
netSoapReader::AsValue(unsigned& value) const
{
    return this->AsUns(value);
}

template<>
bool
netSoapReader::AsValue(u64& value) const
{
    return this->AsUns64(value);
}

template<>
bool
netSoapReader::AsValue(float& value) const
{
    return this->AsFloat(value);
}

template<>
bool
netSoapReader::AsValue(double& value) const
{
    return this->AsDouble(value);
}

template<>
bool
netSoapReader::AsValue(bool& value) const
{
    return this->AsBool(value);
}

//////////////////////////////////////////////////////////////////////////
//  netSoapEnvelope
//////////////////////////////////////////////////////////////////////////
netSoapEnvelope::netSoapEnvelope()
    : m_Uri(NULL)
    , m_Ns(NULL)
    , m_Method(NULL)
    , m_SoapAction(NULL)
    , m_State(STATE_NONE)
    , m_Allocator(NULL)
    , m_Status(NULL)
    , m_HaveCompleteEnvelope(false)
{
}

netSoapEnvelope::~netSoapEnvelope()
{
    this->Clear();
}

void
netSoapEnvelope::Clear()
{
    netAssert(!this->Pending());

    if(this->Pending())
    {
        this->Cancel();
    }

    m_Envelope.Clear();
    m_HttpReq.Clear();

    if(m_Uri){this->Free(m_Uri);}
    if(m_Ns){this->Free(m_Ns);}
    if(m_Method){this->Free(m_Method);}
    if(m_SoapAction){this->Free(m_SoapAction);}

    m_Uri = NULL;
    m_Ns = NULL;
    m_Method = NULL;
    m_SoapAction = NULL;
    m_State = STATE_NONE;
    m_Allocator = NULL;
    m_Status = NULL;
    m_HaveCompleteEnvelope = false;
}

bool
netSoapEnvelope::Begin(const char* uri,
                        const char* methodXmlns,
                        const char* methodName,
                        sysMemAllocator* allocator)
{
    bool success = false;

    this->Clear();

    if(netVerify(STATE_NONE == m_State))
    {
        m_Allocator = allocator;
        m_Envelope.Init(allocator, 0);
        m_HttpReq.Init(allocator);

        const char* host = uri ? strchr(uri, ':') : NULL;
        if(host && '/' == host[1] && '/' == host[2])
        {
            host = &host[3];
        }
        else
        {
            host = NULL;
        }
        const char* ehost = host ? strchr(host, '/') : NULL;
        if(!ehost && host)
        {
            ehost = host + strlen(host);
        }

        const char* path = (ehost && '/' == *ehost) ? ehost : NULL;
        const char* epath = path ? strchr(path, '?') : NULL;
        if(!epath && path)
        {
            epath = path + strlen(path);
        }

        netAssertf(host && ehost && path && epath, "Invalid URI: \"%s\"", uri);

        const int ulen = uri ? strlen(uri) : 0;
        const int nlen = strlen(methodXmlns);
        const int mlen = strlen(methodName);
        const int salen = nlen + mlen + sizeof("http:///");

        if(ulen && nlen && mlen)
        {
            m_Uri = this->Allocate(ulen + 1);
            m_Ns = this->Allocate(nlen + 1);
            m_Method = this->Allocate(mlen + 1);
            m_SoapAction = this->Allocate(salen + 1);

            if(m_Uri && m_Ns && m_Method)
            {
                safecpy(m_Uri, uri, ulen + 1);
                safecpy(m_Ns, methodXmlns, nlen + 1);
                safecpy(m_Method, methodName, mlen + 1);
                if('/' != methodXmlns[nlen-1])
                {
                    formatf(m_SoapAction, salen + 1, "http://%s/%s", m_Ns, m_Method);
                }
                else
                {
                    formatf(m_SoapAction, salen + 1, "http://%s%s", m_Ns, m_Method);
                }

                m_Envelope.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                m_Envelope.Append("<soap:Envelope");
                //m_Envelope.Append(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
                //m_Envelope.Append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"");
                m_Envelope.Append(" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"");
                m_Envelope.Append(">");
                m_Envelope.Append("<soap:Body>");

                char tmp[256];
                formatf(tmp, "<%s xmlns=\"http://%s\">", m_Method, m_Ns);
                m_Envelope.Append(tmp);

                m_State = STATE_BUILDING_ENVELOPE;

                success = true;
            }
        }

        if(!success)
        {
            this->Clear();
        }
    }

    return success;
}

bool
netSoapEnvelope::End()
{
    if(STATE_BUILDING_ENVELOPE == m_State)
    {
        char tmp[256];

        formatf(tmp, "</%s>", m_Method);
        m_Envelope.Append(tmp);
        m_Envelope.Append("</soap:Body>");
        m_Envelope.Append("</soap:Envelope>");
        m_State = STATE_FINISHED_ENVELOPE;
        m_HaveCompleteEnvelope = true;
    }

    return true;
}

bool
netSoapEnvelope::BeginCompound(const char* name)
{
    bool success = false;

    if(netVerify(STATE_BUILDING_ENVELOPE == m_State))
    {
        char tmp[256];
        formatf(tmp, "<%s>", name);
        m_Envelope.Append(tmp);

        success = true;
    }

    return success;
}

bool
netSoapEnvelope::EndCompound(const char* name)
{
    bool success = false;

    if(netVerify(STATE_BUILDING_ENVELOPE == m_State))
    {
        char tmp[256];
        formatf(tmp, "</%s>", name);
        m_Envelope.Append(tmp);

        success = true;
    }

    return success;
}

bool
netSoapEnvelope::WriteElement(const char* name, const char* content, const unsigned len)
{
    bool success = false;
    if(netVerify(STATE_BUILDING_ENVELOPE == m_State))
    {
        char tmp[256];
        formatf(tmp, "<%s>", name);
        m_Envelope.Append(tmp);
        m_Envelope.Append(content, len);
        formatf(tmp, "</%s>", name);
        m_Envelope.Append(tmp);
        success = true;
    }

    return success;
}

bool
netSoapEnvelope::WriteString(const char* name, const char* value, const unsigned valLen)
{
    bool success = false;
    if(netVerify(STATE_BUILDING_ENVELOPE == m_State))
    {
        char tmp[256];
        formatf(tmp, "<%s>", name);
        m_Envelope.Append(tmp);
        unsigned len;
        netHttpRequest::XmlEncode(value, valLen, &m_Envelope, &len);
        formatf(tmp, "</%s>", name);
        m_Envelope.Append(tmp);
        success = true;
    }

    return success;
}

bool
netSoapEnvelope::WriteString(const char* name, const char* value)
{
    return this->WriteString(name, value, strlen(value));
}

bool
netSoapEnvelope::WriteInt(const char* name, const int value)
{
    bool success = false;
    if(netVerify(STATE_BUILDING_ENVELOPE == m_State))
    {
        char tmp[64];
        formatf(tmp, "%d", value);
        success = this->WriteString(name, tmp, strlen(tmp));
    }

    return success;
}

bool
netSoapEnvelope::WriteFloat(const char* name,
                            const float value,
                            const int precision)
{
    bool success = false;
    if(netVerify(STATE_BUILDING_ENVELOPE == m_State))
    {
        char tmp[64];
        formatf(tmp, "%#.*g", precision, value);
        success = this->WriteString(name, tmp, strlen(tmp));
    }

    return success;
}

bool
netSoapEnvelope::IsComplete() const
{
    return m_HaveCompleteEnvelope;
}

const char*
netSoapEnvelope::GetSoap() const
{
    const char* soap = NULL;
    if(netVerify(this->IsComplete()))
    {
        soap = m_Envelope.GetString();
    }

    return soap;
}

int
netSoapEnvelope::GetLength() const
{
    return netVerify(this->IsComplete())
            ? m_Envelope.GetLength()
            : 0;
}

const char*
netSoapEnvelope::GetUri() const
{
    return m_Uri;
}

const char*
netSoapEnvelope::GetSoapAction() const
{
    return m_SoapAction;
}

bool
netSoapEnvelope::Post(const netAddress* proxyAddr, 
                    const unsigned timeoutSecs,
                    netStatus* status)
{
    bool success = false;

    status->SetPending();

    if(netVerify(!this->Pending())
        && netVerify(STATE_BUILDING_ENVELOPE == m_State
                        || STATE_FINISHED_ENVELOPE == m_State))
    {
        netDebug("Posting SOAP request to: \"%s\"...", m_Uri);

        netAssert(!m_Status);

        if(STATE_BUILDING_ENVELOPE == m_State)
        {
            this->End();
        }

        if(netVerify(STATE_FINISHED_ENVELOPE == m_State))
        {
            if(netVerify(m_HttpReq.BeginSoap(m_Uri, m_SoapAction, proxyAddr))
                && netVerify(m_HttpReq.AddContent(this->GetSoap()))
                && netVerify(m_HttpReq.Commit(timeoutSecs, &m_MyStatus)))
            {
                m_Status = status;
                m_State = STATE_POSTING;
                success = true;
            }
            else
            {
                netError("Error posting request");
            }
        }
    }

    if(!success)
    {
        status->SetFailed();
    }

    return success;
}

void
netSoapEnvelope::Update()
{
    if(this->Pending())
    {
        m_HttpReq.Update();

        if(!m_MyStatus.Pending())
        {
            if(m_MyStatus.Succeeded())
            {
                netDebug("Posting SOAP request to: \"%s\" SUCCEEDED", m_Uri);

                m_Status->SetSucceeded();
                m_State = STATE_SUCCEEDED;
            }
            else
            {
                netDebug("Posting SOAP request to: \"%s\" FAILED", m_Uri);

                m_Status->SetFailed();
                m_State = STATE_ERROR;
            }

            m_Status = NULL;
        }
    }
}

bool
netSoapEnvelope::Pending() const
{
    return (STATE_POSTING == m_State);
}

void
netSoapEnvelope::Cancel()
{
    if(this->Pending())
    {
        netDebug("Canceling SOAP request to: \"%s\"", m_Uri);

        m_HttpReq.Cancel();
        m_Status->SetCanceled();
        m_Status = NULL;
        m_State = STATE_ERROR;
    }
}

const char*
netSoapEnvelope::GetResponse(unsigned* len) const
{
    const char* response;
    if(STATE_SUCCEEDED == m_State)
    {
        response = m_HttpReq.GetResponseContent();
        *len = m_HttpReq.GetResponseContentLength();
    }
    else
    {
        response = NULL;
        *len = 0;
    }

    return response;
}

bool
netSoapEnvelope::GetResponse(netSoapReader* reader) const
{
    unsigned len;
    const char* response = this->GetResponse(&len);
    if(response)
    {
        reader->Reset(response, 0, len);
    }
    else
    {
        reader->Clear();
    }

    return (NULL != response);
}

//private:

char*
netSoapEnvelope::Allocate(const unsigned numChars)
{
    if(m_Allocator)
    {
        return (char*) m_Allocator->TryLoggedAllocate(numChars, 0, 0, __FILE__, __LINE__);
    }
    else
    {
        return rage_new char[numChars];
    }
}

void
netSoapEnvelope::Free(char* chars)
{
    if(m_Allocator)
    {
        m_Allocator->Free(chars);
    }
    else
    {
        delete[] chars;
    }
}

} // namespace rage
