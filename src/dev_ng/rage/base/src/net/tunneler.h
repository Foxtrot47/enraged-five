// 
// net/tunneler.h 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_TUNNELER_H
#define NET_TUNNELER_H

#include "atl/inlist.h"
#include "event.h"
#include "netaddress.h"
#include "peeraddress.h"
#include "status.h"
#include "rline/scpresence/rlscpresence.h"
#include "system/criticalsection.h"

/*
    netTunneler provides a multi-platform interface for creating network
    "tunnels".  A tunnel is a communications channel between two peers.
    Building a tunnel typically involves punching through NAT firewalls,
    authenticating remote peers, and exchanging security keys so that
    encrypted/authenticated communications can take place over a secure
    channel.

    Prior to creating a tunnel the remote peer's netPeerAddress must be known.
    netPeerAddress is an unambiguous identifier that can be used to uniquely
    identify peers on a network.  It is different from an IP address, or a
    netAddress, in that it's valid across NATs and firewalls.  Whereas a
    particular peer can have different IPs and netAdresses depending on one's
    point of view (inside or outside the firewall) a netPeerAddress is
    consistent regardless of point of view.

    A netPeerAddress object is typically obtained via an advertising service
    such as a matchmaking or session server, or via a host peer.

    Tunnels can be online or LAN.  Online tunnels are tunnels to peers outside
    of the local subnet, while LAN tunnels are tunnels to peers on the same
    local subnet.

    It is important to call netTunnel::CloseTunnel() to close a
    tunnel when it is no longer required so resources can be freed.
*/
namespace rage
{

class netConnectionManager;
class netEvent;
class netSocket;
class netTunneler;

//PURPOSE
//  Tunnels can be online (outside the local subnet) or LAN (inside the local subnet).
enum netTunnelType
{
	NET_TUNNELTYPE_INVALID      = -1,
	NET_TUNNELTYPE_LAN,
	NET_TUNNELTYPE_ONLINE
};

//PURPOSE
//  Why we are tunneling.
enum netTunnelReason
{
	NET_TUNNEL_REASON_INVALID = -1,
	NET_TUNNEL_REASON_MATCHMAKING_QUERY,
	NET_TUNNEL_REASON_DIRECT_QUERY,
	NET_TUNNEL_REASON_DIRECT,
	NET_TUNNEL_REASON_QOS,
	NET_TUNNEL_REASON_ESTABLISH,
	NET_TUNNEL_REASON_P2P,
};

//PURPOSE
//  Tunneler error codes.
//  Don't re-order/renumber existing reasons.
//  These match values in our reporting system.
enum 
{
	// general
	NET_TUNNELER_NO_ERROR					= 0x0,			// 0
	NET_TUNNELER_CANCELLED					= 0x1,			// 1
	// direct
	DIRECT_ERROR_FAILED_TO_OPEN				= 0x10000001,	// 268435457
	DIRECT_ERROR_FAILED_TO_OPEN_UNSOLICITED	= 0x10000002,	// 268435458
	DIRECT_ERROR_TIMED_OUT					= 0x10000003,	// 268435459
	// relay
	RELAY_ERROR_QUERY_FAILED				= 0x20000001,	// 536870913
	RELAY_ERROR_DISCOVERY_FAILED			= 0x20000002,	// 536870914
	RELAY_ERROR_NO_ADDRESS_RECORDS			= 0x20000003,	// 536870915
	RELAY_ERROR_INVALID_JSON				= 0x20000004,	// 536870916
	RELAY_ERROR_NO_MAPPED_IP_FIELD			= 0x20000005,	// 536870917
	RELAY_ERROR_NO_MAPPED_PORT_FIELD		= 0x20000006,	// 536870918
	RELAY_ERROR_NO_RELAY_IP_FIELD			= 0x20000007,	// 536870919
	RELAY_ERROR_NO_RELAY_PORT_FIELD			= 0x20000008,	// 536870920
	RELAY_ERROR_INVALID_FIELD				= 0x20000009,	// 536870921
	RELAY_ERROR_CONNECT_FAILED				= 0x2000000A,	// 536870922
	RELAY_ERROR_INVALID_IP					= 0x2000000B,	// 536870923
	RELAY_ERROR_CONNECTION_FAILED			= 0x2000000C,	// 536870924
	RELAY_ERROR_KEY_EXCHANGE_FAILED			= 0x2000000D,	// 536870925
	RELAY_ERROR_NO_RELAY_PATH_FOUND			= 0x2000000E,	// 536870926
	RELAY_ERROR_OUT_OF_DATE_PRES_ATTR		= 0x2000000F,	// 536870927

	// lan
	LAN_ERROR_TIMED_OUT						= 0x30000001,	// 805306369

	// key exchange
	KX_ERROR_FAILED_TO_INITIATE				= 0x30000010,	// 805306384
	KX_ERROR_FAILED							= 0x30000011,	// 805306385
	KX_ERROR_FAILED_DIRECT					= 0x30000012,	// 805306386
	KX_ERROR_FAILED_RELAY					= 0x30000013,	// 805306387
};

//PURPOSE
class netTunnelDesc
{
	friend class netTunneler;
	friend class netTunnelRequest;

public:
	netTunnelDesc() {Clear();}

	//PURPOSE
	//  Parameters used during tunneling.
	//PARAMS
	//  tunnelType    - The type of tunnel being requested.
	//  tunnelReason  - The reason we're opening a tunnel (used for telemetry)
	//  uniqueId      - A value used in telemetry reports to join this tunnel request with other data (such as a specific session).
	//  bilateral	  - Set to true if it is expected that the remote peer will be attempting a connection
	//					to the local peer at the same time. This is only used as hint during tunneling.
	netTunnelDesc(netTunnelType tunnelType, netTunnelReason tunnelReason, u64 uniqueId, bool bilateral)
	: m_TunnelType(tunnelType)
	, m_TunnelReason(tunnelReason)
	, m_UniqueId(uniqueId)
	, m_Bilateral(bilateral)
	{

	}

	~netTunnelDesc() {Clear();}

	void Clear()
	{
		m_TunnelType = NET_TUNNELTYPE_INVALID;
		m_TunnelReason = NET_TUNNEL_REASON_INVALID;
		m_UniqueId = 0;
		m_Bilateral = false;
	}

	netTunnelType m_TunnelType;
	netTunnelReason m_TunnelReason;
	u64 m_UniqueId;
	bool m_Bilateral;
};

//PURPOSE
//  Represents a pending tunnel request.  When calling netTunnel::OpenTunnel
//  an instance of netTunnelRequest must be passed in by the caller.
//  The request object can be monitored for the status of the asynchronous
//  tunnel request.  When the tunnel is successfully opened, the remote
//  peer's endpointId can be obtained with netTunnelRequest::GetEndpointId().
class netTunnelRequest
{
    friend class netTunneler;

public:

    netTunnelRequest();

    ~netTunnelRequest();

    //PURPOSE
    //  Returns true if an asynchronous tunnel request is pending.
    bool Pending() const;

    //PURPOSE
    //  Returns true if the asynchronous tunnel request is complete and
    //  it successfully obtained a valid netAddress for the remote peer.
    bool Succeeded() const;

    //PURPOSE
    //  Cancels the request if pending.
    void Cancel();

	//PURPOSE
	//  Upon successfully opening a tunnel, returns the endpoint id
	//  of the remote peer. Note that this will only be valid if the
	//  tunnel was opened via netConnectionManager::OpenTunnel().
	const EndpointId GetEndpointId() const;

	//PURPOSE
	//  Called by the connection manager to set the resulting
	//  endpoint id. 
	void SetEndpointId(const EndpointId endpointId);

    //PURPOSE
    //  Upon successfully opening a tunnel, returns the network address
    //  of the remote peer. It is recommend to use GetEndpointId()
	//  if you're opening the tunnel via the connection manager, since
	//  addresses can change or be reassigned between relay and direct.
    const netAddress& GetNetAddress() const;
	
	//PURPOSE
	//  Overrides a net address
	//  This can only be done when the tunnel request has completed
	void OverrideNetAddress(const netAddress& netAddr, const netAddress& relayAddr);

	//PURPOSE
	//  Store direct addr found after QuickConnect.
	void SetQuickConnectDirectAddr(const netAddress& netAddr);	

    //PURPOSE
    //  The relay address used for this tunnel.  If a direct tunnel
    //  was not possible then GetRelayAddress() will return the same
    //  value as GetNetAddress().
    const netAddress GetRelayAddress() const;

    //PURPOSE
    //  Returns the address of the peer to which a tunnel is being formed.
    const netPeerAddress& GetPeerAddress() const;

    //PURPOSE
    //  Returns the tunnel ID.  Used for debugging.
    unsigned GetId() const;

	//PURPOSE
	//  Returns whether we are currently exchanging keys
	bool IsExchangingKeys() const;

private:

	/*
	State diagram (using dot/graphvis notation). See https://hub.take2games.com/display/CODE/Tunneling for resulting diagram.
	\dot
		digraph Tunneler {
			label = "Tunneler";
			graph [truecolor bgcolor="#00000000"]
			node [shape=record, fontname=Arial, fontsize=10];

			STATE_NONE [label="STATE_NONE"]
			STATE_TUNNEL_DIRECT [label="STATE_TUNNEL_DIRECT"]
			STATE_DISCOVER_RELAY_ADDRESS [label="STATE_DISCOVER_RELAY_ADDRESS\n(Cache or Presence query)"]
			STATE_CONNECT_TO_RELAY [label="STATE_CONNECT_TO_RELAY\n(Instant if not using DTLS)"]
			STATE_EXCHANGE_KEYS [label="STATE_EXCHANGE_KEYS"]
			STATE_SUCCEEDED [label="STATE_SUCCEEDED"]
			STATE_FAILED [label="STATE_FAILED"]

			STATE_NONE -> STATE_TUNNEL_DIRECT
			STATE_NONE -> STATE_DISCOVER_RELAY_ADDRESS[label="-netforceuserelay"]
			STATE_NONE -> STATE_EXCHANGE_KEYS[label="Endpoint\nalready\nexists."]

			STATE_TUNNEL_DIRECT -> STATE_DISCOVER_RELAY_ADDRESS[label="Direct tunnel failed.\nFallback to relay."]
			STATE_TUNNEL_DIRECT -> STATE_EXCHANGE_KEYS
			STATE_TUNNEL_DIRECT -> STATE_FAILED[label="No direct or relay path found."]

			STATE_DISCOVER_RELAY_ADDRESS -> STATE_CONNECT_TO_RELAY
			STATE_DISCOVER_RELAY_ADDRESS -> STATE_FAILED

			STATE_CONNECT_TO_RELAY -> STATE_EXCHANGE_KEYS
			STATE_CONNECT_TO_RELAY -> STATE_FAILED

			STATE_EXCHANGE_KEYS -> STATE_SUCCEEDED
			STATE_EXCHANGE_KEYS -> STATE_FAILED
		}
	\enddot
	*/
	enum State
	{
		STATE_NONE = 0,
		STATE_TUNNEL_DIRECT,
		STATE_TUNNELING_DIRECT,
		STATE_DISCOVER_RELAY_ADDRESS,
		STATE_DISCOVERING_RELAY_ADDRESS,
		STATE_CONNECT_TO_RELAY,
		STATE_CONNECTING_TO_RELAY,
		STATE_EXCHANGE_KEYS,
		STATE_EXCHANGING_KEYS,
		STATE_SUCCEEDED,
		STATE_FAILED,
	};

	enum CompletionType
	{
		COMPLETE_FAILED,
		COMPLETE_SUCCEEDED,
	};

	enum PresenceAttribIndices
	{
		ATTR_INDEX_PEER_ADDRESS,
		ATTR_MAX_PRESENCE_ATTRIBUTES
	};

	enum PresenceQueryState
	{
		STATE_PQ_UNATTEMPTED = 0,
		STATE_PQ_PENDING = 1,
		STATE_PQ_OUT_OF_DATE = 2,
		STATE_PQ_NO_PEER_ADDR = 3,
		STATE_PQ_NO_RELAY_ADDR = 4,
		STATE_PQ_FAILED_TO_INITIATE = 5,
		STATE_PQ_FAILED = 6,
		STATE_PQ_CANCELED = 7,
		STATE_PQ_SUCCEEDED = 8
	};

    void Clear();

    void Reset(const netPeerAddress& peerAddr,
				const netTunnelDesc& tunnelDesc,
				netStatus* status);

	bool DiscoverRelayAddress();
    bool QueryRelayAddress();
    void UpdateRelayAddressDiscovery();
	const netAddress GetRelayAddressInternal() const;

    bool ConnectToRelay();
	void UpdateConnectingToRelay();

	bool ExchangeKeys();
	void UpdateKeyExchange();

	void SendTelemetry(CompletionType completionType, const int resultCode);
    void Complete(CompletionType completionType, const int resultCode);

#if !__NO_OUTPUT
	const char* GetStateName(State state);
#endif

	void SetState(State state, const int resultCode = NET_TUNNELER_NO_ERROR);

	unsigned m_Id;
	State m_State;
	netStatus* m_Status;

	EndpointId m_EndpointId;
	netPeerAddress m_PeerAddr;
	netAddress m_QuickConnectDirectAddr;
    netAddress m_NetAddr;
    netAddress m_RelayAddr;
	netAddress m_PrevRelayAddress;
	netTunnelDesc m_TunnelDesc;

	inlist_node<netTunnelRequest> m_ListLink;
    netTunneler* m_Tunneler;
	
	rlScPresenceAttribute m_PresenceAttrs[ATTR_MAX_PRESENCE_ATTRIBUTES];
	netStatus m_DirectTunnelingStatus;
	netStatus m_RelayDiscoveryStatus;
	netStatus m_RelayConnectionStatus;
	netStatus m_KeyExchangeStatus;
	
	PresenceQueryState m_PresenceQueryState;
	bool m_ForcePresenceQuery;
	int m_DirectResultCode;
	int m_KeyExchangeResultCode;
	unsigned m_TimeStarted;
	unsigned m_TimeStateStarted;
};

//PURPOSE
//  Manages request to open communication tunnels to remote peers.
class netTunneler
{
    friend class netTunnelRequest;

public:

    netTunneler();

    ~netTunneler();

    //PARAMS
	//  cxnMgr		- Connection manager used for sending receiving packets.
    bool Init(netConnectionManager* cxnMgr);

	//PURPOSE
	//  Call this on a regular interval to update pending tunnel requests.
	void Update();

    void Shutdown();
	
    //PURPOSE
    //  Opens a tunnel to a remote peer.
    //PARAMS
    //  peerAddr    - Peer address for the remote peer.
    //  tunnelDesc  - Tunnel description struct.
    //  rqst        - Tunnel request.  On successful completion call
    //                GetEndpointId() to retrieve the endpointId of the
    //                peer.
    //  status      - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the request.
    //NOTES
    //  rqst and status must remain valid for the duration of the
    //  asynchronous request.  They should not be stack variables.
    bool OpenTunnel(const netPeerAddress& peerAddr,
                    const netTunnelDesc& tunnelDesc,
                    netTunnelRequest* rqst,
                    netStatus* status);

    //PURPOSE
    //  Close the tunnel associated with the given address.
    //  Further sends to that address will not arrive.
    bool CloseTunnel(const netAddress& addr);

    //PURPOSE
    //  Cancels a pending tunnel request.
    void CancelRequest(netTunnelRequest* rqst);

    //PURPOSE
    //  Overrides a pending tunnel request, causing it to succeed
    //  and use the given address as the tunnel address.
    void OverrideRequest(netTunnelRequest* rqst,
                        const netAddress& useAddr);

    //PURPOSE
    //  Removes a cached relay address should it exist. 
    bool RemoveCachedRelayAddress(const netAddress& addr);

	//PURPOSE
	//  Given an address for an endpoint with which we have an open tunnel,
	//  retrieve the key used to encrypt P2P traffic between the local peer
	//  and the remote peer.
	//RETURNS
	//  True on success.
	bool GetP2pKey(const netAddress& addr, netP2pCrypt::Key& key);
	bool GetP2pKey(const netPeerId& peerId, netP2pCrypt::Key& key);

	//PURPOSE
	//  Update an existing p2p key with new address information.
	void UpdateP2pKey(const netPeerId& peerId, const netAddress& newAddr, const netAddress& relayAddr);

	// tunables
	static void SetAllowNetTunnelerTelemetry(const bool allowNetTunnelerTelemetry) {sm_AllowNetTunnelerTelemetry = allowNetTunnelerTelemetry;}
	static void SetAllowKeyExchangeRetry(const bool allowKeyExchangeRetry);

private:

	void SelectStartState(netTunnelRequest* rqst);

	void CancelAllRequests();

    void AddRequest(netTunnelRequest* rqst);

	void UpdateRequest(netTunnelRequest* rqst);

    void RemoveRequest(netTunnelRequest* rqst);

	// platform native functions
    bool NativeInit(netConnectionManager* cxnMgr);

    void NativeShutdown();

    bool NativeOpenTunnel(netTunnelRequest* rqst);

    bool NativeCloseTunnel(const netAddress& addr);

	void NativeCancelRequest(netTunnelRequest* rqst);

	bool NativeHasActiveSession(const netPeerAddress& peerAddr);

    void NativeUpdate();

	void NativeUpdateRequest(netTunnelRequest* rqst);

    // list of pending tunnel requests
    typedef inlist<netTunnelRequest, &netTunnelRequest::m_ListLink> Requests;

    Requests m_Requests;

    mutable sysCriticalSectionToken m_Cs;

	netConnectionManager* m_CxnMgr;

	// tunables
	static bool sm_AllowNetTunnelerTelemetry;
	static bool sm_AllowKeyExchangeRetry;

	bool m_Initialized  : 1;
};

}   //namespace rage

#endif  //NET_TUNNELER_H
