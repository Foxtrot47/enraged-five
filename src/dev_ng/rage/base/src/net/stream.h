// 
// net/stream.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_STREAM_H
#define NET_STREAM_H

#include "connectionmanager.h"

/*
    netStream is used to send/receive large buffers (i.e. buffers
    that are larger than the maximum size of a frame payload).

    To send a buffer using netStream first call PrepareToSend().  This will
    generate a unique stream id.  The stream id and stream length should be
    reliably communicated, along with application specific data, to the
    recipient.  After communicating the stream id and length call Send()
    to send the buffer.  The stream instance can be monitored for completion
    by calling Pending().  When Pending() returns false the operation is
    complete, and resulted in either success or failure.

    The buffer passed to Send() should not be deallocated as long as Pending()
    returns true.

    Upon receiving notification of an inbound stream the stream recipient
    should call Receive().  The stream instance can be monitored for completion
    by calling Pending().  When Pending() returns false the operation is
    complete, and resulted in either success or failure.

    The buffer passed to Receive() should not be deallocated as long as
    Pending() returns true.
*/

namespace rage
{

const unsigned NET_INVALID_STREAM_ID    = ~0u;

//PURPOSE
//  Encapsulates a stream fragment.  Streams are divided into fragments
//  that can fit the maximum size of a frame payload.
class netStreamFragment
{
    //Signature to identify streams from other types of data.
    static const unsigned SIG_STRM =
        NET_MAKE_SIG4('S'-'A', 'T'-'A', 'R'-'A', 'M'-'A');
    static const unsigned SIG_INVALID   = ~SIG_STRM;

public:

    enum
    {
        MAX_BYTE_SIZEOF_FRAGMENT = netFrame::MAX_BYTE_SIZEOF_PAYLOAD,
    };

    enum
    {
        BIT_SIZEOF_SIG          = datBitsNeeded<SIG_STRM>::COUNT,
        BIT_SIZEOF_STREAMID     = 32,
        BIT_SIZEOF_STREAM_SIZE  = 32,

        BIT_OFFSETOF_SIG            = 0,
        BIT_OFFSETOF_STREAMID       = BIT_OFFSETOF_SIG + BIT_SIZEOF_SIG,
        BIT_OFFSETOF_STREAM_SIZE    = BIT_OFFSETOF_STREAMID + BIT_SIZEOF_STREAMID,

        BIT_SIZEOF_HEADER   = BIT_OFFSETOF_STREAM_SIZE + BIT_SIZEOF_STREAM_SIZE,

        BYTE_SIZEOF_HEADER  = (BIT_SIZEOF_HEADER + 7) >> 3,

        MAX_BYTE_SIZEOF_PAYLOAD = MAX_BYTE_SIZEOF_FRAGMENT - BYTE_SIZEOF_HEADER,
    };

    //PURPOSE
    //  Generates a new stream id.
    static unsigned NextId();

    //PURPOSE
    //  Reads the fragment header from the buffer.
    //RETURNS
    //  True on success.
    static bool ReadHeader(const void* buf,
                            const unsigned sizeofBuf,
                            unsigned* streamId,
                            unsigned* streamSize);

    //PURPOSE
    //  Writes the fragment header to the buffer.
    //RETURNS
    //  Number of bytes written.
    static unsigned WriteHeader(void* buf,
                                const unsigned sizeofBuf,
                                const unsigned streamId,
                                const unsigned streamSize);

    //PURPOSE
    //  Returns true if the buffer contains a stream fragment.
    //PARAMS
    //  buf         - Fragment buffer
    //  sizeofBuf   - Size in bytes of the buffer
    static bool IsFragment(const void* buf,
                            const unsigned sizeofBuf);
    //PURPOSE
    //  Returns the payload of the fragment contained in the buffer
    //  and writes the size of the payload to sizeofPayload.
    static const void* GetPayload(const void* buf,
                                const unsigned sizeofBuf,
                                unsigned* sizeofPayload);
};

class netStream
{
public:
    netStream();

    ~netStream();

    //PURPOSE
    //  Prepares to send a stream.
    //PARAMS
    //  streamId    - On success will contain the unique id of the stream.
    //RETURNS
    //  True on success.
    //NOTES
    //  PrepareToSend() must be called before calling Send().  The stream
    //  id returned, as well as the stream length, should be reliably
    //  communicated to the recipient before calling Send().
    bool PrepareToSend(unsigned* streamId);

    //PURPOSE
    //  Streams a buffer to the remote peer identified by the connection id.
    //PARAMS
    //  cxnMgr      - Connection manager used for sending fragments.
    //  cxnId       - Id of connection on which to send the buffer.
    //  buf         - Buffer to send.
    //  sizeofBuf   - Size (in bytes) of buffer.
    //NOTES
    //  This is an asynchronous operation and is not complete until Pending()
    //  returns false.
    //  Do not deallocate buf until the operation completes or is aborted.
    bool Send(netConnectionManager* cxnMgr,
                const int cxnId,
                const void* buf,
                const unsigned sizeofBuf);

    //PURPOSE
    //  Receives a stream from the remote peer identified by the connection id.
    //PARAMS
    //  cxnMgr      - Connection manager used for receiving fragments.
    //  cxnId       - Id of connection on which to receive the buffer.
    //  buf         - Buffer to fill with streamed data.
    //  sizeofBuf   - Size (in bytes) of buffer.
    //NOTES
    //  This is an asynchronous operation and is not complete until Pending()
    //  returns false.
    //  Do not deallocate buf until the operation completes or is aborted.
    bool Receive(netConnectionManager* cxnMgr,
                const int cxnId,
                const unsigned streamId,
                void* buf,
                const unsigned sizeofBuf);

    //PURPOSE
    //  Returns true if currently sending.
    bool Sending() const;

    //PURPOSE
    //  Returns true if currently receiving.
    bool Receiving() const;

    //PURPOSE
    //  Returns true if an operation is pending.
    bool Pending() const;

    //PURPOSE
    //  Returns true if the operation succeeded.
    bool Succeeded() const;

    //PURPOSE
    //  Returns true if the operation failed.
    bool Failed() const;

    //PURPOSE
    //  Returns true if the operation is complete.
    bool Complete() const;

private:

    enum
    {
        STATE_NONE     = -1,
        STATE_PREPARED,
        STATE_SENDING,
        STATE_RECEIVING,
        STATE_SUCCEEDED,
        STATE_FAILED,
    };

    void SetSending();
    void SetReceiving();
    void SetSucceeded();
    void SetFailed();

    void Abort();

    bool SendFrag();

    bool RcvFrag(const void* frag,
                const unsigned sizeofFrag);

    void OnCxnEvent(netConnectionManager* cxnMgr,
                    const netEvent* nevt);

    netConnectionManager* m_CxnMgr;

    int m_State;

    netConnectionManager::Delegate m_CxnDlgt;

    union
    {
        const u8* m_SndPtr;
        u8* m_RcvPtr;
    };

    const u8* m_EndBuf;
    unsigned m_SizeofBuf;

    unsigned m_StreamId;

    int m_CxnId;

    netSequence m_Seq;
};

}   //namespace rage

#endif  //NET_STREAM_H
