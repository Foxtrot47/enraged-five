//
// net/natupnp.h
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#ifndef NET_NAT_UPNP_H
#define NET_NAT_UPNP_H

#include "file/file_config.h"

#include "system/criticalsection.h"
#include "net.h"
#include "netaddress.h"
#include "nethardware.h"
#include "netsocket.h"
#include "relay.h"
#include "status.h"

namespace rage
{

//PURPOSE
// Encapsulates information collected about local UPnP capabilities and results of port reservation/forwarding.
class netNatUpNpInfo
{
public:
	static const unsigned UPNP_DEVICE_STRING_BUF_SIZE = 32;

	enum State
	{
		NAT_UPNP_UNATTEMPTED,
		NAT_UPNP_IN_PROGRESS,
		NAT_UPNP_SUCCEEDED,
		NAT_UPNP_FAILED,
		NAT_UPNP_NUM_STATES,
	};

	enum ReserveMethod
	{
		UPNP_METHOD_NONE = 0,
		UPNP_METHOD_ADD_ANY_PORT_MAPPING = 1,
		UPNP_METHOD_ADD_PORT_MAPPING_LIMITED_LEASE = 2,
		UPNP_METHOD_ADD_PORT_MAPPING_UNLIMITED_LEASE = 3,
		UPNP_METHOD_ADD_PORT_MAPPING_LIMITED_LEASE_2 = 4,
		UPNP_METHOD_ADD_PORT_MAPPING_UNLIMITED_LEASE_2 = 5,
		UPNP_NUM_METHODS
	};

	enum IgdStatus
	{
		UPNP_IGD_NOT_DETECTED = 0,
		UPNP_IGD_CONNECTED = 1,
		UPNP_IGD_NOT_CONNECTED = 2,
		UPNP_IGD_NON_IGD_DETECTED = 3,
		UPNP_IGD_NUM_ENUMS
	};

	static const unsigned MAX_EXPORTED_SIZE_IN_BITS = datBitsNeeded<NAT_UPNP_NUM_STATES>::COUNT +		// m_State
													  (sizeof(u16) << 3) +								// m_RequestedPort
													  (sizeof(u16) << 3) +								// m_ReservedPort
													  (sizeof(int) << 3) +								// m_AddAnyPortMappingError
													  (sizeof(int) << 3) +								// m_AddPortMappingError
													  datBitsNeeded<UPNP_NUM_METHODS>::COUNT +			// m_ReserveMethod
													  (sizeof(unsigned) << 3) +							// m_PortReservationTimeMs
													  datBitsNeeded<UPNP_IGD_NUM_ENUMS>::COUNT +		// m_IgdStatus
													  datMaxBitsNeededForString<UPNP_DEVICE_STRING_BUF_SIZE>::COUNT + // m_FriendlyName
													  datMaxBitsNeededForString<UPNP_DEVICE_STRING_BUF_SIZE>::COUNT + // m_Manufacturer
													  datMaxBitsNeededForString<UPNP_DEVICE_STRING_BUF_SIZE>::COUNT + // m_ModelName
													  datMaxBitsNeededForString<UPNP_DEVICE_STRING_BUF_SIZE>::COUNT + // m_ModelNumber
													  (netIpAddress::MAX_EXPORTED_SIZE_IN_BYTES << 3) +	// m_ExternalIpAddress
													  (sizeof(u16) << 3) +								// m_NumSuccessfulRefreshes
													  (sizeof(u16) << 3) +								// m_NumFailedRefreshes
													  (sizeof(u16) << 3);								// m_NumSequentialFailedRefreshes

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((MAX_EXPORTED_SIZE_IN_BITS + 7) & ~7) >> 3;		// round up to nearest multiple of 8 then divide by 8

	netNatUpNpInfo()
	{
		Clear();
	}

	void Clear()
	{
		m_State = NAT_UPNP_UNATTEMPTED;
		m_RequestedPort = 0;
		m_ReservedPort = 0;
		m_AddAnyPortMappingError = -1;
		m_AddPortMappingError = -1;
		m_ReserveMethod = UPNP_METHOD_NONE;
		m_PortReservationTimeMs = 0;
		m_IgdStatus = UPNP_IGD_NOT_DETECTED;
		sysMemSet(m_FriendlyName, 0, sizeof(m_FriendlyName));
		sysMemSet(m_Manufacturer, 0, sizeof(m_Manufacturer));
		sysMemSet(m_ModelName, 0, sizeof(m_ModelName));
		sysMemSet(m_ModelNumber, 0, sizeof(m_ModelNumber));
		m_ExternalIpAddress.Clear();
		m_NumSuccessfulRefreshes = 0;
		m_NumFailedRefreshes = 0;
		m_NumSequentialFailedRefreshes = 0;
	}

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

#if !__NO_OUTPUT
	const char*
	GetReserveMethodString(const ReserveMethod method) const
	{
		switch(method)
		{
		case UPNP_METHOD_NONE: return "None"; break;
		case UPNP_METHOD_ADD_ANY_PORT_MAPPING: return "AddAnyPortMapping"; break;
		case UPNP_METHOD_ADD_PORT_MAPPING_LIMITED_LEASE: return "AddPortMapping (limited lease)"; break;
		case UPNP_METHOD_ADD_PORT_MAPPING_UNLIMITED_LEASE: return "AddPortMapping (unlimited lease)"; break;
		case UPNP_METHOD_ADD_PORT_MAPPING_LIMITED_LEASE_2: return "AddPortMapping (limited lease 2)"; break;
		case UPNP_METHOD_ADD_PORT_MAPPING_UNLIMITED_LEASE_2: return "AddPortMapping (unlimited lease 2)"; break;
		case UPNP_NUM_METHODS: return "UPNP_NUM_METHODS"; break;
		}

		return "UPNP_INVALID_METHOD";
	}

	const char*
	GetIgdStatusString(const IgdStatus status) const
	{
		switch(status)
		{
		case UPNP_IGD_NOT_DETECTED: return "No IGD Detected"; break;
		case UPNP_IGD_CONNECTED: return "IGD Connected"; break;
		case UPNP_IGD_NOT_CONNECTED: return "IGD Not Connected"; break;
		case UPNP_IGD_NON_IGD_DETECTED: return "Non-IGD UPnP device detected"; break;
		case UPNP_IGD_NUM_ENUMS: return "UPNP_IGD_NUM_ENUMS"; break;
		}

		return "UPNP_IGD_INVALID_ENUM";
	}

	void DumpInfo() const;
#endif

	void SetState(const State state) { if(m_State != state) { m_State = state; } }
	State GetState() const { return m_State; }

	void SetRequestedPort(const u16 port) { m_RequestedPort = port; }
	u16 GetRequestedPort() const { return m_RequestedPort; }

	void SetReservedPort(const u16 port) { m_ReservedPort = port; }
	u16 GetReservedPort() const { return m_ReservedPort; }

	void SetUpNpAddAnyPortMappingError(const int error) { m_AddAnyPortMappingError = error; }
	int GetUpNpAddAnyPortMappingError() const { return m_AddAnyPortMappingError; }

	void SetUpNpAddPortMappingError(const int error) { m_AddPortMappingError = error; }
	int GetUpNpAddPortMappingError() const { return m_AddPortMappingError; }

	void SetReserveMethod(const ReserveMethod reserveMethod) { m_ReserveMethod = reserveMethod; }
	ReserveMethod GetReserveMethod() const { return m_ReserveMethod; }

	void SetPortReservationTimeMs(const unsigned timeMs) { m_PortReservationTimeMs = timeMs; }
	unsigned GetPortReservationTimeMs() const { return m_PortReservationTimeMs; }

	void SetIgdStatus(const IgdStatus status) { m_IgdStatus = status; }
	IgdStatus GetIgdStatus() const { return m_IgdStatus; }

	void SetDeviceFriendlyName(const char* friendlyName) { safecpy(m_FriendlyName, friendlyName); }
	const char* GetDeviceFriendlyName() const { return m_FriendlyName; }

	void SetDeviceManufacturer(const char* manufacturer) { safecpy(m_Manufacturer, manufacturer); }
	const char* GetDeviceManufacturer() const { return m_Manufacturer; }

	void SetDeviceModelName(const char* modelName) { safecpy(m_ModelName, modelName); }
	const char* GetDeviceModelName() const { return m_ModelName; }

	void SetDeviceModelNumber(const char* modelNumber) { safecpy(m_ModelNumber, modelNumber); }
	const char* GetDeviceModelNumber() const { return m_ModelNumber; }

	void SetExternalIpAddress(const netIpAddress& ip) { m_ExternalIpAddress = ip; }
	const netIpAddress& GetExternalIpAddress() const { return m_ExternalIpAddress; }

	void SetNumSuccessfulRefreshes(const u16 num) { m_NumSuccessfulRefreshes = num; }
	u16 GetNumSuccessfulRefreshes() const { return m_NumSuccessfulRefreshes; }

	void SetNumFailedRefreshes(const u16 num) { m_NumFailedRefreshes = num; }
	u16 GetNumFailedRefreshes() const { return m_NumFailedRefreshes; }

	void SetNumSequentialFailedRefreshes(const u16 num) { m_NumSequentialFailedRefreshes = num; }
	u16 GetNumSequentialFailedRefreshes() const { return m_NumSequentialFailedRefreshes; }

private:
	State m_State;
	u16 m_RequestedPort;
	u16 m_ReservedPort;
	int m_AddAnyPortMappingError;
	int m_AddPortMappingError;
	ReserveMethod m_ReserveMethod;
	unsigned m_PortReservationTimeMs;
	IgdStatus m_IgdStatus;
	char m_FriendlyName[UPNP_DEVICE_STRING_BUF_SIZE];
	char m_Manufacturer[UPNP_DEVICE_STRING_BUF_SIZE];
	char m_ModelName[UPNP_DEVICE_STRING_BUF_SIZE];
	char m_ModelNumber[UPNP_DEVICE_STRING_BUF_SIZE];
	netIpAddress m_ExternalIpAddress;
	u16 m_NumSuccessfulRefreshes;
	u16 m_NumFailedRefreshes;
	u16 m_NumSequentialFailedRefreshes;
};

//PURPOSE
//  Detects local UPnP capabilities and sets up port forwarding/port reservation
//  if UPnP is available.
class netNatUpNp
{
public:
	static bool Init();
	static void Shutdown();
	static void Update();

	//PURPOSE
	// Returns true if Init() has been called successfully.
	static bool IsInitialized();

	//PURPOSE
	// Returns true if we've attempted to reserve a port, or if we
	// won't attempt to reserve a port (eg. due to tunables, command line, etc.)
	// Note that refreshes and deletions can still occur after this returns true.
	static bool IsInitialWorkCompleted();

	static const netNatUpNpInfo& GetUpNpInfo() {return sm_UpNpInfo;}

	// deletes any UPnP port forward rule we've set up.
	// Should be called at game shutdown. Game must wait until the status is completed.
	static void DeleteUpNpPortForwardRule(netStatus* status);
	
	// tunables
	static void SetAllowUpNpPortForwarding(const bool allowUpNpPortForwarding); 
	static void SetReserveRelayMappedPort(const bool reserveRelayMappedPort);
	static bool GetReserveRelayMappedPort();
	static void SetUpNpPortRefreshTimeMs(const unsigned uPnPrefreshTimeMs); 
	static void SetAllowCleanUpOldEntries(const bool allowCleanUpOldEntries);
	static void SetTunablesReceived();

	static unsigned GetUpNpLeaseDurationSec();
	static unsigned GetDeviceDiscoveryTimeoutMs();
	static bool GetAllowCleanUpOldEntries();

private:
	static bool sm_Initialized;
	static netNatUpNpInfo sm_UpNpInfo;
	static bool sm_CanBegin;
	static unsigned sm_DeviceDiscoveryTimeoutMs;
	static unsigned sm_UpNpRefreshStartTime;
	static unsigned sm_UpNpLeaseDurationSec;
	static bool sm_AllowUpNpPortForwarding;
	static bool sm_ReserveRelayMappedPort;
	static bool sm_CleanUpOldEntries;
	static unsigned sm_UpNpRefreshTimeMs;
	static unsigned sm_MaxSequentialFailedRefreshes;

	static netNatUpNpInfo::State m_DeleteState;

	static sysCriticalSectionToken m_Cs;
};

}   //namespace rage

#endif  //NET_NAT_UPNP_H
