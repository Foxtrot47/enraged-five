// 
// net/task.h
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_TASK_H
#define NET_TASK_H

#include <stddef.h>

#include "status.h"

namespace rage
{

/*
    netTask provides support for running asynchronous tasks.
    All tasks are run on the main thread, so netTask::Update()
    must be called periodically to update running tasks.
*/

class netTask;
class diagChannel;
class sysMemAllocator;

//PURPOSE
//  Value returned from netTask::Update() to indicate the status of the task.
enum netTaskStatus
{
    NET_TASKSTATUS_NONE,
    NET_TASKSTATUS_PENDING,
    NET_TASKSTATUS_FAILED,
    NET_TASKSTATUS_SUCCEEDED
};

//PURPOSE
//  Base class for netTask.
class netTaskBase
{
    friend class netTask;
private:

    netTaskBase();

    virtual ~netTaskBase();

    //PURPOSE
    //  Queue tb before this.
    void QueueBefore(netTaskBase* tb);

    //PURPOSE
    //  Queue tb after this.
    void QueueAfter(netTaskBase* tb);

    //PURPOSE
    //  Unlink this from its list.
    void Unlink();

    netTaskBase* m_Next;
    netTaskBase* m_Prev;
};

//PURPOSE
//  Used for fire and forget tasks.
//NOTES
//  Use the task ID to cancel a fire and forget task.
//
//EXAMPLE
//  netFireAndForgetTask<SomeOtherTaskType>* task;
//  netTask::Create(&task);
//  task->Configure(param1, param2, param3);
//  netTask::Run(task);
//
template<typename T>
class netFireAndForgetTask : public T
{
    friend class netTask;
private:

    netStatus m_FafStatus;
};

class netTask : public netTaskBase
{
#if RSG_DURANGO
	template<typename T> friend struct netXblStatus;
#endif

public:

    virtual ~netTask();

    //PURPOSE
    //  Returns the task ID.
    unsigned GetTaskId() const;

    //PURPOSE
    //  Initialize the netTask system.
    //PARAMS
    //  allocator   - Used to allocate netTask objects
    static bool Init(sysMemAllocator* allocator);

    //PURPOSE
    //  Shuts down the netTask system.
    static void Shutdown();

	//PURPOSE
	//  Called when we sign out of the local profile
	static void OnSignOut(const int localGamerIndex);

    //PURPOSE
    //  Creates an instance of a task, using the allocator passed
    //  to Init().
    //PARAMS
    //  t       - Will be populated with the netTask instance.
    //  status  - The netStatus object used to poll for completion.
    //NOTES
    //  The status object will not be set to the Pending state until
    //  QueueTask() is called.
    template<typename T>
    static bool Create(T** t, netStatus* status)
    {
        *t = (T*)Allocate(sizeof(T));
        if(netVerifyf(*t, "Error allocating task"))
        {
            new(*t) T();
            (*t)->netTask::m_Status = status;
            return true;
        }

        return false;
    }

    //PURPOSE
    //  Creates an instance of a fire and forget task, using the allocator passed
    //  to Init().
    //PARAMS
    //  t       - Will be populated with the netTask instance.
    template<typename T>
    static bool Create(netFireAndForgetTask<T>** t)
    {
        *t = (netFireAndForgetTask<T>*)Allocate(sizeof(netFireAndForgetTask<T>));
        if(netVerifyf(*t, "Error allocating task"))
        {
            new(*t) netFireAndForgetTask<T>();
            (*t)->netTask::m_Status = &(*t)->m_FafStatus;
            return true;
        }

        return false;
    }

    //PURPOSE
    //  Destroys a task created with Create().
    //NOTES
    //  Don't call DestroyTask() after queuing a task.  Call Cancel()
    //  instead.
    static void Destroy(netTask* task);

    //PURPOSE
    //  Schedules a task for execution.
    //PARAMS
    //  task    - The task
    //  queueId - The ID of the queue on which the task will wait to be run.
    //NOTES
    //  The queue ID can be arbitrary.  It simply indicates in what order
    //  tasks will be executed.
    //  Tasks on the same queue will be executed serially in FIFO order.
    //  Tasks on different queues will run concurrently.
    static bool Schedule(netTask* task, const size_t queueId);

    //PURPOSE
    //  Runs the task now, in parallel to all other running tasks.
    static bool Run(netTask* task);

    //PURPOSE
    //  Updates all running tasks.
    static void Update();

	//PURPOSE
	//  Updates a particular task
	static void UpdateTask(const netStatus* status);

	//PURPOSE
	//  Returns true if task exists
	static bool HasTask(const netStatus* status);

    //PURPOSE
    //  Cancels the task associated with the status object.
    //NOTES
    //  Canceling a task doesn't necessarily destroy it immediately.
    //  Some tasks must run to completion regardless of being canceled.
    //
    //  However, task implementers MUST respect the canceled state of
    //  the task.  Before writing to externally allocated memory
    //  the task should call WasCanceled().  If WasCanceled() returns
    //  true then it is not safe to write to externally allocated memory.
    static void Cancel(const netStatus* status);

    //PURPOSE
    //  Cancels the task with the given task ID.
    //NOTES
    //  Canceling a task doesn't necessarily destroy it immediately.
    //  Some tasks must run to completion regardless of being canceled.
    //
    //  However, task implementers MUST respect the canceled state of
    //  the task.  Before writing to externally allocated memory
    //  the task should call WasCanceled().  If WasCanceled() returns
    //  true then it is not safe to write to externally allocated memory.
    static void Cancel(const unsigned taskId);

	//PURPOSE
	// Cancels all running and queued tasks.
	static void CancelAll();

    //PURPOSE
    //  Returns the time between the last two calls to Update().
    static unsigned GetTimestamp();

    //PURPOSE
    //  Returns the current time from the point of view of netTask.
    static unsigned GetCurrentTime();

protected:

    netTask();

    //PURPOSE
    //  Returns the name of the task.
    virtual const char* GetTaskName() const;

    //PURPOSE
    //  Returns the ID of the queue on which the task is waiting.
    size_t GetTaskQueueId() const;

    //PURPOSE
    //  Returns the channel that netTaskXXXX methods output to.
    //  The default is the ragenet channel, which is used if the
    //  NET_TASK_USE_CHANNEL macro is not present in the subclass
    //  declaration.
    virtual const diagChannel* GetDiagChannel() const;

    //PURPOSE
    //  Returns true if the task was canceled.
    //NOTES
    //  WARNING - Externally allocated memory should not be referenced if
    //  WasCanceled returns true.
    bool WasCanceled() const;

    //PURPOSE
    //  Called when a task is canceled.  Should be overridden by subclasses.
    virtual void OnCancel();

    //PURPOSE
    //  Called when a task is updated.  Should be overridden by subclasses.
	//PARAM
	//  resultCode	Pointer to the result code to use if the update loop finishes the task.
    virtual netTaskStatus OnUpdate(int* resultCode);

    //PURPOSE
    //  Called just prior to destroying a task.
    virtual void OnCleanup();

private:

    enum TaskState
    {
        TASKSTATE_NONE,
        TASKSTATE_QUEUED,
        TASKSTATE_RUNNING
    };

    //PURPOSE
    //  Allocates memory from the allocator passed to Init().
    static void* Allocate(const size_t size);

    //PURPOSE
    //  Frees memory allocated using Allocate()
    static void Free(void* mem);

    //PURPOSE
    //  Generates a new task ID.
    static unsigned NextId();

    //PURPOSE
    //  Finds the task associated with the status object
    //PARAMS
    //  head    - The head of the list to search.
    //  status  - The status object to find.
    static netTask* Find(netTaskBase* head, const netStatus* status);

    //PURPOSE
    //  Finds the task with the given task ID
    //PARAMS
    //  head    - The head of the list to search.
    //  taskId  - The task ID.
    static netTask* Find(netTaskBase* head, const unsigned taskId);

    //PURPOSE
    //  Cancels the task.
    static void Cancel(netTask* task);

    //PURPOSE
    //  Removes a task from the netTask system.
    //  This does not destroy the task.
    static void Remove(netTask* task);

    //Queue of runnable tasks
    static netTaskBase sm_Runnable;

    //The queue ID
    size_t m_QueueId;

    //The task ID
    unsigned m_TaskId;

    //Current state
    TaskState m_TaskState;

    //The task at the head of the queue identified by m_QueueId
    //keeps the list of all waiting tasks on the same queue.
    netTaskBase m_WaitQueue;

    //The status object that's monitored for completion of this task.
    netStatus* m_Status;

#if !__NO_OUTPUT
    //Time at which the task was queued
	unsigned m_QueueTime;
    //Time at which the task started running
	unsigned m_StartTime;
#endif

    //Set to false if netTask::OnCancel is called
    //and has not been overridden by a subclass.
    bool m_OnCancelOverridden   : 1;
    //True if the task was canceled.
    bool m_WasCanceled          : 1;
};

}   //namespace rage

#if !__NO_OUTPUT

//PURPOSE
//  Place this macro in the declaration of a task, so that it's
//  name will be correctly set.
//
#define NET_TASK_DECL(name)\
    virtual const char* GetTaskName() const {return #name;}

//PURPOSE
//  Place NET_TASK_USE_CHANNEL in the definition of a task to set which
//  channel the netTaskXXXX macros (defined below) output to.
//
#define NET_TASK_USE_CHANNEL(tag)\
    virtual const diagChannel* GetDiagChannel() const {return &Channel_##tag;}

//PURPOSE
//  Macros that prefix the task's name and ID to output,
//  which is sent to channel declared by NET_TASK_USE_CHANNEL/SUBCHANNEL.

#define netTaskLogfHelper(task,channel,severity,fmt,...)\
    diagLogfHelper(channel, severity, "%s[%" SIZETFMT "u:%u]: " fmt, (task)->GetTaskName(), (task)->GetTaskQueueId(), (task)->GetTaskId(), ##__VA_ARGS__);

#define netTaskVerifyHelper(task,channel,cond,fmt,...)\
	diagVerifyfHelper(cond, channel, "%s[%" SIZETFMT "u:%u]: " fmt, (task)->GetTaskName(), (task)->GetTaskQueueId(), (task)->GetTaskId(), ##__VA_ARGS__)

#define netTaskAssertHelper(task,channel,cond,fmt,...)\
	diagAssertfHelper(cond, channel, "%s[%" SIZETFMT "u:%u]: " fmt, (task)->GetTaskName(), (task)->GetTaskQueueId(), (task)->GetTaskId(), ##__VA_ARGS__);

//Use this version when calling from outside the task.
#define netTaskDebugPtr(task, fmt, ...)			netTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netTaskDebug1Ptr(task, fmt, ...)		netTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netTaskDebug2Ptr(task, fmt, ...)		netTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_DEBUG2, fmt, ##__VA_ARGS__);
#define netTaskDebug3Ptr(task, fmt, ...)		netTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_DEBUG3, fmt, ##__VA_ARGS__);
#define netTaskWarningPtr(task, fmt, ...)		netTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_WARNING, fmt, ##__VA_ARGS__);
#define netTaskErrorPtr(task, fmt, ...)			netTaskLogfHelper(task,(*(task)->GetDiagChannel()),DIAG_SEVERITY_ERROR, fmt, ##__VA_ARGS__);
#define netTaskVerifyPtr(task, cond)			netTaskVerifyHelper(task,(*(task)->GetDiagChannel()), cond, "")
#define netTaskVerifyfPtr(task, cond, fmt, ...)	netTaskVerifyHelper(task,(*(task)->GetDiagChannel()), cond, fmt, ##__VA_ARGS__)
#define netTaskAssertPtr(task, cond)			netTaskAssertHelper(task,(*(task)->GetDiagChannel()), cond, "");
#define netTaskAssertfPtr(task, cond, fmt, ...)	netTaskAssertHelper(task,(*(task)->GetDiagChannel()), cond, fmt, ##__VA_ARGS__);

#else

#define NET_TASK_DECL(name)\
    virtual const char* GetTaskName() const {return "";}

#define NET_TASK_USE_CHANNEL(tag)\
    virtual const diagChannel* GetDiagChannel() const {return NULL;}

#define netTaskDebugPtr(task, fmt, ...)
#define netTaskDebug1Ptr(task, fmt, ...)
#define netTaskDebug2Ptr(task, fmt, ...)
#define netTaskDebug3Ptr(task, fmt, ...)
#define netTaskWarningPtr(task, fmt, ...)
#define netTaskErrorPtr(task, fmt, ...)
#define netTaskVerifyPtr(task, cond)				(cond)
#define netTaskVerifyfPtr(task, cond, fmt, ...)	(cond)
#define netTaskAssertPtr(task, cond)
#define netTaskAssertfPtr(task, cond, fmt, ...)

#endif //!__NO_OUTPUT

//Use this version when calling from within the task (most common case).
#define netTaskDebug(fmt, ...)			netTaskDebugPtr(this,fmt,##__VA_ARGS__)
#define netTaskDebug1(fmt, ...)			netTaskDebug1Ptr(this,fmt,##__VA_ARGS__)
#define netTaskDebug2(fmt, ...)			netTaskDebug2Ptr(this,fmt,##__VA_ARGS__)
#define netTaskDebug3(fmt, ...)			netTaskDebug3Ptr(this,fmt,##__VA_ARGS__)
#define netTaskWarning(fmt, ...)		netTaskWarningPtr(this,fmt,##__VA_ARGS__)
#define netTaskError(fmt, ...)			netTaskErrorPtr(this,fmt,##__VA_ARGS__)
#define netTaskVerify(cond)				netTaskVerifyPtr(this,cond)
#define netTaskVerifyf(cond, fmt, ...)  netTaskVerifyfPtr(this,cond,fmt,##__VA_ARGS__)
#define netTaskAssert(cond)				netTaskAssertPtr(this,cond)
#define netTaskAssertf(cond, fmt, ...)  netTaskAssertfPtr(this,cond,fmt,##__VA_ARGS__)

#endif  //NET_TASK_H