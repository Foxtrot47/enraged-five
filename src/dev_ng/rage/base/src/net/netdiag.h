// 
// net/netdiag.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_NETDIAG_H
#define NET_NETDIAG_H

#include "diag/channel.h"
#include "rline/rl.h"
#include "system/param.h"
#include "system/stack.h"
#include "string/string.h"

namespace rage
{

RAGE_DECLARE_CHANNEL(ragenet)

#define netDebug(fmt, ...)						RAGE_DEBUGF1(__rage_channel, fmt, ##__VA_ARGS__)
#define netDebug1(fmt, ...)						RAGE_DEBUGF1(__rage_channel, fmt, ##__VA_ARGS__)
#define netDebug2(fmt, ...)						RAGE_DEBUGF2(__rage_channel, fmt, ##__VA_ARGS__)
#define netDebug3(fmt, ...)						RAGE_DEBUGF3(__rage_channel, fmt, ##__VA_ARGS__)
#define netDisplay(fmt, ...)					RAGE_DISPLAYF(__rage_channel, fmt, ##__VA_ARGS__)
#define netWarning(fmt, ...)					RAGE_WARNINGF(__rage_channel, fmt, ##__VA_ARGS__)
#define netError(fmt, ...)						RAGE_ERRORF(__rage_channel, fmt, ##__VA_ARGS__)
#define netCondLogf(cond, severity, fmt, ...)	RAGE_CONDLOGF(__rage_channel, cond, severity, fmt, ##__VA_ARGS__)

#if USE_NET_ASSERTS
#if __ASSERT
#define netVerify(cond)							RAGE_VERIFY(__rage_channel, cond)
#define netVerifyf(cond, fmt, ...)				RAGE_VERIFYF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#else
#define netVerify(cond)							( Likely(cond) || _NetworkVerifyf(RAGE_CAT2(Channel_,__rage_channel), #cond, __FILE__, __LINE__, "") )
#define netVerifyf(cond, fmt, ...)				( Likely(cond) || _NetworkVerifyf(RAGE_CAT2(Channel_,__rage_channel), #cond, __FILE__, __LINE__, fmt, ##__VA_ARGS__) )
#endif  // __ASSERT

#if __ASSERT
#define netAssert(cond)							RAGE_ASSERT(__rage_channel, cond)
#define netAssertf(cond, fmt, ...)				RAGE_ASSERTF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#else
#define netAssert(cond)			do { static bool __FILE__l__LINE__ =false;  \
	if( !Likely(cond) && !(__FILE__l__LINE__) ) { sysStack::PrintStackTrace(); \
	RAGE_ERRORF(__rage_channel, "NetworkAssertf(%s) FAILED", #cond); __FILE__l__LINE__=true;  } } while(false)
#define netAssertf(cond, fmt, ...)	do { static bool __FILE__l__LINE__ =false;  \
	if( !Likely(cond) && !(__FILE__l__LINE__) ) { sysStack::PrintStackTrace(); char formatted[1024]; formatf(formatted, fmt, ##__VA_ARGS__); \
	RAGE_ERRORF(__rage_channel, "NetworkAssertf(%s) FAILED: %s", #cond, formatted); __FILE__l__LINE__=true;  } } while(false)
#endif  // __ASSERT

#else
#define netVerify(cond)							RAGE_VERIFY(__rage_channel, cond)
#define netVerifyf(cond, fmt, ...)				RAGE_VERIFYF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#define netAssert(cond) 						RAGE_ASSERT(__rage_channel, cond)
#define netAssertf(cond, fmt, ...)  			RAGE_ASSERTF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#endif // USE_NET_ASSERTS

//PURPOSE
//  Returns a string corresponding the the socket error code.
const char* netSocketErrorString( const int errorCode );

#define NET_EXCEPTION_SILENT(ex)								\
	netError("Exception 0x%08x, Msg: %ls",						\
	ex->HResult, 												\
	ex->Message != nullptr ? ex->Message->Data() : L"NULL");	

#if __ASSERT
#define NET_EXCEPTION(ex)										\
	netAssertf(false, "Exception 0x%08x, Msg: %ls",				\
	ex->HResult, 												\
	ex->Message != nullptr ? ex->Message->Data() : L"NULL");		
#else
#define NET_EXCEPTION(ex) NET_EXCEPTION_SILENT(ex)
#endif

}   //namespace rage

#endif  //NET_DIAG_H

//This is deliberately outside of the #include guards
//in order to mitigate problems with unity builds.
#undef __rage_channel
#define __rage_channel ragenet
