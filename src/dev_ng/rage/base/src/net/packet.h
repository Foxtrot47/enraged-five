// 
// sample_message_lib/packet.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PACKET_H
#define PACKET_H

#include "atl/inmap.h"
#include "data/bitbuffer.h"
#include "netsocket.h"
#include "netsequence.h"
#include "net.h"
#include "netpeerid.h"
#include "system/noncopyable.h"

namespace rage
{

enum netPacketFlags
{
    NET_PACKETFLAG_RELAY			= 0x01,	// netRelayPacket or netPeerRelayPacket - sent to/from relay server or relaying peer
    NET_PACKETFLAG_COMPRESSED		= 0x02,	// the payload is compressed
    // UNUSED						= 0x04,
	NET_PACKETFLAG_ALT_SEND_PORT	= 0x08, // used during NAT type detection to determine firewall filtering rules
	NET_PACKETFLAG_SECURE_ADDR		= 0x10, // netRelayPacket - using relay tokens/secure addresses rather than real/plaintext addresses

    NET_PACKETFLAG_NUMFLAGS			= 5
};

// if more than 8 flags are needed we will need to allocate more
// than 1 byte for the flags field (see netRelayPacket::SIZEOF_FLAGS)
CompileTimeAssert(NET_PACKETFLAG_NUMFLAGS <= 8);

class netPacket;

enum netP2pCryptFlags
{
	NET_P2P_CRYPT_TUNNELING_PACKET = 0x01,
};

enum netRelayPacketType
{
    NET_RELAYPACKET_PING,           //Set when pinging a relay
    NET_RELAYPACKET_PONG,           //Set when a relay answers a ping
    NET_RELAYPACKET_P2P_FORWARD,    //Set when a P2P packet needs to be forwarded
                                    //to another peer.
    NET_RELAYPACKET_P2P_TERMINUS,   //Set when the local peer is the final recipient
                                    //of a P2P packet
    NET_RELAYPACKET_MESSAGE,        //Set when the packet contains a message
                                    //from the relay server
    NET_RELAYPACKET_PING2,          //Similar to Ping, but contains both a plaintext address and token in the request/response header
    NET_RELAYPACKET_PONG2,          //Similar to Pong, but for Ping2

    NET_RELAYPACKET_NUM_TYPES
};

// if more than 8 types are needed we will need to allocate more
// than 1 byte for the type field (see netRelayPacket::SIZEOF_TYPE)
CompileTimeAssert(NET_RELAYPACKET_NUM_TYPES <= 8);

// packets sent to/from relay servers (different from peer relay packets below)
class netRelayPacket
{
public:

    enum
    {
        MAX_SIZEOF_RELAY_PACKET    = netSocket::MAX_BUFFER_SIZE,
    };

    static const u8 PROTOCOL_VERSION    = 3;

    enum
    {
        SIZEOF_IP           = 4, // [IPv4 specific]
        SIZEOF_PORT         = 2,
    };

    enum
    {
        OFFSETOF_SIZE       = 0,
        SIZEOF_SIZE         = 2,
        OFFSETOF_FLAGS      = OFFSETOF_SIZE + SIZEOF_SIZE,
        SIZEOF_FLAGS        = 1,

        //******
        //Up to this point the header must be the same as the
        //netPacket header.
        //Any changes to the above section header must be reflected
        //in the netPacket header.
        //******

        OFFSETOF_VERSION            = OFFSETOF_FLAGS + SIZEOF_FLAGS,
        SIZEOF_VERSION              = 1,
        OFFSETOF_MIXER              = OFFSETOF_VERSION + SIZEOF_VERSION,
        SIZEOF_MIXER                = 1,
        OFFSETOF_TYPE               = OFFSETOF_MIXER + SIZEOF_MIXER,
        SIZEOF_TYPE                 = 1,

        //In P2P packets this will be the address of the packet recipient
        //In PONG packets this will be the local host's public address
        //with respect to the relay server.
        OFFSETOF_DST_IP             = OFFSETOF_TYPE + SIZEOF_TYPE,
        OFFSETOF_DST_PORT           = OFFSETOF_DST_IP + SIZEOF_IP,
        OFFSETOF_PING_PAYLOAD       = OFFSETOF_DST_PORT + SIZEOF_PORT,
        SIZEOF_PING_HEADER          = OFFSETOF_PING_PAYLOAD,

        //Extra headers in PONG2 packets. PONG2 packets contain an opaque
		//token, followed by the address of the packet recipient.
		OFFSETOF_DST_SECURE_TOKEN   = OFFSETOF_TYPE + SIZEOF_TYPE,
		SIZEOF_SECURE_TOKEN			= 6,
		OFFSETOF_DST_PLAIN_IP		= OFFSETOF_DST_SECURE_TOKEN + SIZEOF_SECURE_TOKEN,
        OFFSETOF_DST_PLAIN_PORT		= OFFSETOF_DST_PLAIN_IP + SIZEOF_IP,
        OFFSETOF_PING2_PAYLOAD		= OFFSETOF_DST_PLAIN_PORT + SIZEOF_PORT,
        SIZEOF_PING2_HEADER			= OFFSETOF_PING2_PAYLOAD,

        //The sender's relay address.  P2P packet recipients will use this
        //to identify the sender and to send packets back.
        OFFSETOF_SRC_PROXY_IP       = OFFSETOF_DST_PORT + SIZEOF_PORT,
        OFFSETOF_SRC_PROXY_PORT     = OFFSETOF_SRC_PROXY_IP + SIZEOF_IP,
        OFFSETOF_SRC_TARGET_IP      = OFFSETOF_SRC_PROXY_PORT + SIZEOF_PORT,
        OFFSETOF_SRC_TARGET_PORT    = OFFSETOF_SRC_TARGET_IP + SIZEOF_IP,
		OFFSETOF_SRC_SECURE_TOKEN	= OFFSETOF_SRC_TARGET_IP,
        OFFSETOF_P2P_PAYLOAD        = OFFSETOF_SRC_TARGET_PORT + SIZEOF_PORT,
        SIZEOF_P2P_HEADER           = OFFSETOF_P2P_PAYLOAD,

        MIN_SIZEOF_HEADER           = SIZEOF_PING_HEADER,

        MAX_SIZEOF_PING_PAYLOAD = MAX_SIZEOF_RELAY_PACKET - SIZEOF_PING_HEADER,

		// using MAX_SIZEOF_CRYPTO_OVERHEAD here since we either use DTLS or P2PCrypt, 
		// never both and never double encrypt as does peer relaying.
        MAX_SIZEOF_P2P_PAYLOAD  = MAX_SIZEOF_RELAY_PACKET - SIZEOF_P2P_HEADER - MAX_SIZEOF_CRYPTO_OVERHEAD,
    };

    void ResetPingHeader(const unsigned sizeofPayload);
	void ResetPing2Header(const unsigned sizeofPayload);

	void ResetAltPortPingHeader(const unsigned sizeofPayload);

    void ResetP2pHeader(const unsigned sizeofPayload,
						const netAddress& dstRelayAddress,
                        const netAddress& srcRelayAddress,
						const netRelayToken& srcRelayToken);

    void Mix();

    void Unmix();

    netIpAddress GetDstIp() const;

    u16 GetDstPort() const;

	netSocketAddress GetDstAddr() const;

	netRelayToken GetDstRelayToken() const;

    bool GetSrcRelayAddress(netAddress* addr) const;

    netRelayPacketType GetType() const;

    unsigned GetSize() const;

    unsigned GetSizeofHeader() const;

    unsigned GetSizeofPayload() const;

    unsigned GetFlags() const;

    unsigned GetVersion() const;

    bool IsRelayPacket() const;

    bool IsPing() const;

	bool IsPing2() const;

    bool IsPong() const;

	bool IsPong2() const;

    bool IsP2pForward() const;

    bool IsP2pTerminus() const;

    bool IsMessage() const;

    void* GetPayload();
    const void* GetPayload() const;

    netPacket* GetPacket();
    const netPacket* GetPacket() const;

protected:

    netRelayPacket();

    u8 m_Data[MAX_SIZEOF_RELAY_PACKET];

    //DO NOT declare additional member variables in this class!

private:
	NON_COPYABLE(netRelayPacket);

};

class netRelayPingPacket : public netRelayPacket
{
public:

    netRelayPingPacket();

    void ResetHeader();
};

class netRelayPing2Packet : public netRelayPacket
{
public:

	netRelayPing2Packet();

	void ResetHeader();
};

class netRelayAltPortPingPacket : public netRelayPacket
{
public:

	netRelayAltPortPingPacket();

	void ResetHeader();
};

class netRelayP2pPacket : public netRelayPacket
{
public:

    netRelayP2pPacket();

    void ResetHeader();
};

// packets sent to/from peer relays (different from relay server packets above)
class netPeerRelayPacket
{
public:
	netPeerRelayPacket();

    enum
    {
        MAX_SIZEOF_RELAY_PACKET    = netSocket::MAX_BUFFER_SIZE,
    };

    static const u8 PROTOCOL_VERSION    = 1;

    enum
    {
        SIZEOF_PEER_ID      = netPeerId::MAX_EXPORTED_SIZE_IN_BYTES,
    };

    enum
    {
        OFFSETOF_SIZE       = 0,
        SIZEOF_SIZE         = netRelayPacket::SIZEOF_SIZE,
        OFFSETOF_FLAGS      = OFFSETOF_SIZE + SIZEOF_SIZE,
        SIZEOF_FLAGS        = netRelayPacket::SIZEOF_FLAGS,

        //******
        //Up to this point the header must be the same as the
        //netPacket and netRelayPacket header.
        //Any changes to the above section header must be reflected
        //in the others.
        //******

        OFFSETOF_VERSION            = OFFSETOF_FLAGS + SIZEOF_FLAGS,
        SIZEOF_VERSION              = 1,
        OFFSETOF_TYPE               = OFFSETOF_VERSION + SIZEOF_VERSION,
        SIZEOF_TYPE                 = 1,

		// source peerId: the origin sending peer
		// destination peerId: the final destination peer
		// routing tables are used at each peer to determine the next hop
        OFFSETOF_SRC_PEER_ID        = OFFSETOF_TYPE + SIZEOF_TYPE,
		OFFSETOF_DST_PEER_ID		= OFFSETOF_SRC_PEER_ID + SIZEOF_PEER_ID,

		OFFSETOF_P2P_PAYLOAD        = OFFSETOF_DST_PEER_ID + SIZEOF_PEER_ID,
        SIZEOF_P2P_HEADER           = OFFSETOF_P2P_PAYLOAD,

		// using MAX_SIZEOF_DOUBLE_P2P_CRYPT_OVERHEAD here since need to P2PEncrypt
		// payloads for the destination peer, and the header for the next hop peer.
        MAX_SIZEOF_P2P_PAYLOAD		= MAX_SIZEOF_RELAY_PACKET - SIZEOF_P2P_HEADER - MAX_SIZEOF_DOUBLE_P2P_CRYPT_OVERHEAD,
    };

	void ResetP2pForwardHeader(const unsigned sizeofPayload,
								const netPeerId& srcPeerId,
								const netPeerId& destPeerId);

	// assumes the header is already configured as a forward header
	void ResetP2pTerminusHeader();

	netPeerId GetSrcPeerId() const;
	netPeerId GetDstPeerId() const;

    netRelayPacketType GetType() const;

    unsigned GetSize() const;

    unsigned GetSizeofHeader() const;

    unsigned GetSizeofPayload() const;

    unsigned GetFlags() const;

    unsigned GetVersion() const;

    bool IsRelayPacket() const;

    bool IsP2pForward() const;

    bool IsP2pTerminus() const;

    void* GetPayload();
    const void* GetPayload() const;

    netPacket* GetPacket();
    const netPacket* GetPacket() const;

protected:

    u8 m_Data[MAX_SIZEOF_RELAY_PACKET];

    //DO NOT declare additional member variables in this class!

private:
	NON_COPYABLE(netPeerRelayPacket);
};

// container that can store any type of packet, much like a sockaddr_storage 
// can contain any type of sockaddr variant. These are not sent on the wire
// directly; a storage object is filled out with payload data, then the
// correct type of header is filled out when routing information is determined.
// The portion of the storage container that contains the packet header and
// payload is sent on the wire. This avoids having to fill out netPackets
// and then copying the payload to the correct packet type. The header of a
// netPacketStorage object is large enough to hold the largest header of any
// of its supported packet types (netPacket, netRelayPacket, netPeerRelayPacket).
class netPacketStorage
{
public:
	netPacketStorage();

    enum
    {
        MAX_SIZEOF_PACKET			= netSocket::MAX_BUFFER_SIZE,
    };

    enum
    {
		MAX_SIZEOF_HEADER			= MAX(netRelayPacket::SIZEOF_P2P_HEADER, netPeerRelayPacket::SIZEOF_P2P_HEADER),

		// using MAX_SIZEOF_DOUBLE_CRYPTO_OVERHEAD here which is the maximum possible amount of crypto overhead.
		// Direct = Single P2PEncrypt
		// Relay Server = Single DTLS encrypt OR single P2PEncrypt depending on whether DTLS relays are used
		// Peer Relay = Double P2PEncrypt (once for destination peer, again for next hop peer)
        MAX_SIZEOF_P2P_PAYLOAD		= MAX_SIZEOF_PACKET - MAX_SIZEOF_HEADER - MAX_SIZEOF_DOUBLE_CRYPTO_OVERHEAD,

		OFFSETOF_RELAY_PACKET		= MAX_SIZEOF_HEADER - netRelayPacket::SIZEOF_P2P_HEADER,
		OFFSETOF_PEER_RELAY_PACKET	= MAX_SIZEOF_HEADER - netPeerRelayPacket::SIZEOF_P2P_HEADER,
		OFFSETOF_PACKET				= MAX_SIZEOF_HEADER,
    };

    netPacket* GetPacket();
    const netPacket* GetPacket() const;

	netRelayPacket* GetRelayPacket();
	const netRelayPacket* GetRelayPacket() const;

	netPeerRelayPacket* GetPeerRelayPacket();
	const netPeerRelayPacket* GetPeerRelayPacket() const;

protected:

    u8 m_Data[MAX_SIZEOF_PACKET];

    //DO NOT declare additional member variables in this class!

private:
	NON_COPYABLE(netPacketStorage);
};

class netPacket
{
public:

    enum
    {
        MAX_BYTE_SIZEOF_PACKET      = netPacketStorage::MAX_SIZEOF_P2P_PAYLOAD,
    };

    //******
    //Any changes to the netPacket header must be reflected in the
    //netRelayPacket and netPeerRelayPacket headers.
    //******

    enum
    {
        //VDP protocol on xbox requires 16-bit size at the start of packets.
        BIT_SIZEOF_SIZE             = netRelayPacket::SIZEOF_SIZE * 8,
        BIT_SIZEOF_FLAGS            = netRelayPacket::SIZEOF_FLAGS * 8,

        BIT_OFFSETOF_SIZE           = netRelayPacket::OFFSETOF_SIZE * 8,
        BIT_OFFSETOF_FLAGS          = netRelayPacket::OFFSETOF_FLAGS * 8,

        BIT_SIZEOF_HEADER           = BIT_OFFSETOF_FLAGS + BIT_SIZEOF_FLAGS,

        BYTE_SIZEOF_HEADER          = (BIT_SIZEOF_HEADER + 7) >> 3,

        MAX_BYTE_SIZEOF_PAYLOAD     = MAX_BYTE_SIZEOF_PACKET - BYTE_SIZEOF_HEADER,
    };

    void ResetHeader(const unsigned sizeofPayload,
                    const unsigned flags);

    unsigned GetSize() const;

    unsigned GetSizeofPayload() const;

    unsigned GetFlags() const;

    bool IsCompressed() const;

    bool IsRelayPacket() const;

    void* GetPayload();
    const void* GetPayload() const;

private:

    netPacket();
	NON_COPYABLE(netPacket);

    u8 m_Data[BYTE_SIZEOF_HEADER];

    //DO NOT declare additional member variables in this class!
};

//PURPOSE
//  A bundle is used to coalesce multiple frames before putting them on
//  the wire.  A bundle also contains ACKs for received frames.
//  Multiple bundles (one per channel) may be contained in a single packet.
//
//  A bundle header contains the sequence number for the first unreliable
//  frame in the bundle.  This permits the sequence number to be optimized
//  out of unreliable frames, saving two bytes per frame.
class netBundle
{
public:

    enum
    {
        MAX_BYTE_SIZEOF_BUNDLE      = netPacket::MAX_BYTE_SIZEOF_PAYLOAD,
    };

    enum Flags
    {
        //Bundle should be used to synchronize inbound sequence number.
        FLAG_SYN            = 0x01,
        //Bundle contains an ACK.
        FLAG_ACK            = 0x02,
        //Connection should be terminated.
        FLAG_TERM           = 0x04,
        //Bundle was sent out of band (connectionless).
        FLAG_OUT_OF_BAND    = 0x08,

        NUM_FLAGS           = 4
    };

    enum
    {
        BIT_SIZEOF_SIZE             = datBitsNeeded<MAX_BYTE_SIZEOF_BUNDLE>::COUNT,
        BIT_SIZEOF_FLAGS            = NUM_FLAGS,
        BIT_SIZEOF_CHANNEL_ID       = datBitsNeeded<NET_MAX_CHANNEL_ID>::COUNT,
        //Sequence number of first unreliable frame in bundle.
        BIT_SIZEOF_FIRST_SEQ        = sizeof(netSequence) << 3,
        BIT_SIZEOF_ACK              = sizeof(netSequence) << 3,

        BIT_OFFSETOF_SIZE           = 0,
        BIT_OFFSETOF_FLAGS          = BIT_OFFSETOF_SIZE + BIT_SIZEOF_SIZE,
        BIT_OFFSETOF_CHANNEL_ID     = BIT_OFFSETOF_FLAGS + BIT_SIZEOF_FLAGS,
        BIT_OFFSETOF_FIRST_SEQ      = BIT_OFFSETOF_CHANNEL_ID + BIT_SIZEOF_CHANNEL_ID,
        BIT_OFFSETOF_ACK            = BIT_OFFSETOF_FIRST_SEQ + BIT_SIZEOF_FIRST_SEQ,
        BIT_OFFSETOF_UO_ACK_BITS    = BIT_OFFSETOF_ACK + BIT_SIZEOF_ACK,

        //We want at least this many bits to to acknowledge frames received
        //out of order.
        MIN_BIT_SIZEOF_UO_ACK_BITS  = 16,
        //Pad out the bundle header to a byte boundary.
        BIT_SIZEOF_UO_ACK_BITS      = ((BIT_OFFSETOF_UO_ACK_BITS+MIN_BIT_SIZEOF_UO_ACK_BITS+7)&0xFFF8)-BIT_OFFSETOF_UO_ACK_BITS,

        BIT_SIZEOF_HEADER           = BIT_OFFSETOF_UO_ACK_BITS + BIT_SIZEOF_UO_ACK_BITS,

        BYTE_SIZEOF_HEADER          = (BIT_SIZEOF_HEADER + 7) >> 3,

        MAX_BYTE_SIZEOF_PAYLOAD     = MAX_BYTE_SIZEOF_BUNDLE - BYTE_SIZEOF_HEADER,
    };

    void ResetHeader(const unsigned sizeofPayload,
                    const unsigned channelId,
                    const netSequence firstUnreliableSeq,
                    const netSequence expectedSeq,
                    const unsigned ackBits,
                    const unsigned flags);

    unsigned GetSize() const;

    unsigned GetSizeofPayload() const;

    unsigned GetFlags() const;

    unsigned GetChannelId() const;

    netSequence GetFirstUnreliableSeq() const;

    netSequence GetExpectedSeq() const;

    unsigned GetUoAckBits() const;

    bool HasSyn() const;
    bool HasAck() const;
    bool HasTerm() const;

    void* GetPayload();
    const void* GetPayload() const;

private:

    netBundle();
	NON_COPYABLE(netBundle);

    //Make sure the bundle header pads out to a byte boundary.
    //This ensures we're maximizing the number of un-ordered ACK
    //bits, which in turn maximizes the receive window and reduces
    //bandwidth required for re-transmits.
    CompileTimeAssert((BYTE_SIZEOF_HEADER << 3) == BIT_SIZEOF_HEADER);

    u8 m_Data[BYTE_SIZEOF_HEADER];

    //DO NOT declare additional member variables in this class!
};

//PURPOSE
//  The basic unit of data we send on the wire.
//  Multiple frames may be contained in a single bundle.
//  Reliable frames have sequence numbers, unreliable frames do not.
//
//  Because unreliable frames are packed into a bundle in strict sequence
//  order we can optimize out the sequence number (saving two bytes per
//  frame) by storing the initial unreliable sequence number in the bundle
//  header.
//
//  Because reliable frames can be sent out of order (due to resends)
//  they must contain their own sequence number.
class netFrame
{
public:

    enum
    {
        MAX_BYTE_SIZEOF_FRAME   = netBundle::MAX_BYTE_SIZEOF_PAYLOAD,
    };

    enum
    {
        BIT_SIZEOF_SIZE         = datBitsNeeded<MAX_BYTE_SIZEOF_FRAME>::COUNT,
        BIT_SIZEOF_SEND_FLAGS   = NET_SEND_NUM_HEADER_FLAGS,
        BIT_SIZEOF_SEQ          = sizeof(netSequence) << 3,

        BIT_OFFSETOF_SIZE       = 0,
        BIT_OFFSETOF_SEND_FLAGS = BIT_OFFSETOF_SIZE + BIT_SIZEOF_SIZE,
        BIT_OFFSETOF_SEQ_DEP    = BIT_OFFSETOF_SEND_FLAGS + BIT_SIZEOF_SEND_FLAGS,
        BIT_OFFSETOF_SEQ        = BIT_OFFSETOF_SEQ_DEP + BIT_SIZEOF_SEQ,

        //Bit size of unreliable header
        BIT_SIZEOF_UHEADER      = BIT_OFFSETOF_SEQ_DEP + BIT_SIZEOF_SEQ,

        //Bit size of reliable header
        BIT_SIZEOF_RHEADER      = BIT_OFFSETOF_SEQ + BIT_SIZEOF_SEQ,

        //Byte size of unreliable header
        BYTE_SIZEOF_UHEADER     = (BIT_SIZEOF_UHEADER + 7) >> 3,

        //Byte size of reliable header
        BYTE_SIZEOF_RHEADER     = (BIT_SIZEOF_RHEADER + 7) >> 3,

        MAX_BYTE_SIZEOF_PAYLOAD = MAX_BYTE_SIZEOF_FRAME - BYTE_SIZEOF_RHEADER,
    };

    netFrame();

    void ResetHeader(const unsigned sizeofPayload,
                    const netSequence seq,
                    const netSequence seqDep,
                    const unsigned sendFlags);

    unsigned GetSize() const;

    unsigned GetSizeofHeader() const;

    unsigned GetSizeofPayload() const;

    netSequence GetSeq() const;

    netSequence GetSeqDep() const;

    unsigned GetSendFlags() const;

    void* GetPayload();
    const void* GetPayload() const;

    bool IsReliable() const;

    static bool Validate(const void* buf,
                        const unsigned sizeofBuf);

private:

	NON_COPYABLE(netFrame);

    u8 m_Data[BYTE_SIZEOF_RHEADER];

    //DO NOT declare additional member variables in this class!
};


//PURPOSE
//  Provides facilities necessary for putting frames on a queue.
struct netOutFrame
{
	netOutFrame(netFrame* frame);

	void Reset(const unsigned sizeofPayload,
				const netSequence seq,
				const netSequence seqDep,
				const unsigned sendFlags,
				const bool notifyAck);

	unsigned GetSize() const;
	unsigned GetSizeofPayload() const;
	netSequence GetSeq() const;
	netSequence GetSeqDep() const;
	unsigned GetSendFlags() const;
	bool IsReliable() const;
	void* GetPayload();
	const void* GetPayload() const;

	netFrame* m_Frame;

	inlist_node<netOutFrame> m_ListLink;

	inmap_node<netSequence, netOutFrame> m_BySeqLink;

	unsigned m_QueueTime;
	unsigned m_LastSendTime;
	unsigned m_SendCount;
	unsigned m_SendFrame; 
	unsigned m_PackFrame; 

	netSequence m_Seq;

	bool m_NotifyAck    : 1;
};


}   //namespace rage

#endif  //PACKET_H
