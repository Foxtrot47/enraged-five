// 
// net/netnonce.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "netnonce.h"

#include "net.h"
#include "system/ipc.h"
#include "system/spinlock.h"
#include "system/timer.h"

#include <stddef.h>

namespace rage
{

u64
netGenerateNonce()
{
    static sysSpinLockToken s_spinToken;
    static size_t s_Context = (size_t) &s_Context;

    u64 nonce;

    netRandom r;
    u8* bytes = (u8*) &nonce;

    size_t seedTime = sysTimer::GetSystemMsTime();

    seedTime = (seedTime << 16) | ((seedTime >> 16) & 0xFFFF);

    size_t seedThis = (size_t) &nonce;

    seedThis = (seedThis << 16) | ((seedThis >> 16) & 0xFFFF);

    size_t newCtx;

    SYS_SPINLOCK_ENTER(s_spinToken);
    {
        newCtx = seedTime
                ^ seedThis
                ^ s_Context
                ^ (size_t) sysIpcGetCurrentThreadId();

        s_Context = newCtx;
    }
    SYS_SPINLOCK_EXIT(s_spinToken);

    r.Reset((int) newCtx);

    for(int i = 0; i < (int)sizeof(nonce); ++i)
    {
        bytes[i] = u8(r.GetInt() & 0xFF);
    }

    return nonce;
}

}   //namespace rage
