// 
// net/pool.h
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_POOL_H
#define NET_POOL_H

#include <array>
#include <iterator>
#include <stdint.h>

#include "netdiag.h"

namespace rage
{

//PURPOSE
//  A fixed sized pool of items of type T.
//  T must have a default constructor.
//  Items are not allocated from the heap.
template<typename T, size_t POOL_SIZE>
class netPool
{
    class ListLink
    {
    public:
        ListLink()
        {
            m_Next = m_Prev = this;
        }

        void Link(ListLink* next)
        {
            netAssert(this == m_Next);
            netAssert(this == m_Prev);

            m_Next = next;
            m_Prev = next->m_Prev;

            m_Prev->m_Next = m_Next->m_Prev = this;
        }

        void Unlink()
        {
            m_Next->m_Prev = m_Prev;
            m_Prev->m_Next = m_Next;
            m_Next = m_Prev = this;
        }

        ListLink* m_Next;
        ListLink* m_Prev;
    };

    class PooledItem : public ListLink
    {
    public:
        uint8_t m_ItemBuf[sizeof(T)];
    };

public:

    netPool()
        : m_NumAllocatedItems(0)
    {
        //Add items to the pool
        for(auto& item : m_Pile)
        {
            item.Link(&m_FreeList);
        }
    }

    //PURPOSE
    //  Iterates over pool items.
    class iterator : public std::iterator<std::forward_iterator_tag, T>
    {
        friend netPool;

    public:
        iterator()
            : m_Item(nullptr)
        {
        }

        iterator(const iterator& that)
            : m_Item(that.m_Item)
        {
        }

        T& operator*() const
        {
            return *(T*)static_cast<PooledItem*>(m_Item)->m_ItemBuf;
        }

        T* operator->() const
        {
            return &operator*();
        }

        //Prefix
        iterator& operator++()
        {
            m_Item = m_Item->m_Next;
            return *this;
        }

        //Postfix
        iterator& operator++(int)
        {
            iterator tmp = *this;
            ++*this;
            return tmp;
        }

        bool operator==(const iterator& that) const
        {
            return that.m_Item == m_Item;
        }

        bool operator!=(const iterator& that) const
        {
            return that.m_Item != m_Item;
        }

    private:

        explicit iterator(ListLink* item)
            : m_Item(item)
        {
        }

        ListLink* m_Item;
    };

    //PURPOSE
    //  Returns an iterator to the first allocated item
    iterator begin()
    {
        return iterator(m_UsedList.m_Next);
    }

    //PURPOSE
    //  Returns an iterator to the end of allocated items
    iterator end()
    {
        return iterator(&m_UsedList);
    }

    //PURPOSE
    //  Allocates an item from the pool and calls the default constructor
    //  Returns null if the pool is empty
    T* alloc()
    {
		uint8_t* itemBuf = allocItem();
		if(nullptr == itemBuf)
		{
			return nullptr;
		}

        //Construct
        return rage_placement_new(itemBuf)T();
    }

	//PURPOSE
	// allocation variant with one constructor argument
	template<class A>
	T* alloc(A a)
	{
		uint8_t* itemBuf = allocItem();
		if(nullptr == itemBuf)
		{
			return nullptr;
		}

        //Construct
        return rage_placement_new(itemBuf)T(a);
	}

	//PURPOSE
	// allocation variant with two constructor arguments
	template<class A, class B>
	T* alloc(A a, B b)
	{
		uint8_t* itemBuf = allocItem();
		if(nullptr == itemBuf)
		{
			return nullptr;
		}

        //Construct
        return rage_placement_new(itemBuf)T(a, b);
	}

	//PURPOSE
	// allocation variant with three constructor arguments
	template<class A, class B, class C>
	T* alloc(A a, B b, C c)
	{
		uint8_t* itemBuf = allocItem();
		if(nullptr == itemBuf)
		{
			return nullptr;
		}

        //Construct
        return rage_placement_new(itemBuf)T(a, b, c);
	}

	//PURPOSE
	// allocation variant with four constructor arguments
	template<class A, class B, class C, class D>
	T* alloc(A a, B b, C c, D d)
	{
		uint8_t* itemBuf = allocItem();
		if (nullptr == itemBuf)
		{
			return nullptr;
		}

		//Construct
		return rage_placement_new(itemBuf)T(a, b, c, d);
	}

    //PURPOSE
    //  Returns an item to the pool and calls the destructor
    void free(T* t)
    {
        netAssert(m_NumAllocatedItems > 0);

        ListLink* link;
        for(link = m_UsedList.m_Next; &m_UsedList != link; link = link->m_Next)
        {
            if((void*)static_cast<PooledItem*>(link)->m_ItemBuf == (void*)t)
            {
                break;
            }
        }

        if(!netVerifyf(&m_UsedList != link, "Attempt to free unallocated item")) return;

        link->Unlink();
        link->Link(&m_FreeList);

        //Destruct.
        ((T*)static_cast<PooledItem*>(link)->m_ItemBuf)->~T();

        --m_NumAllocatedItems;
    }

    //PURPOSE
    //  Returns an iterator to the first allocated item for which the predicate
    //  returns true.
    //  If no item is found, returns the same iterator as end().
    template<typename Predicate>
    iterator find(const Predicate& pred)
    {
        for(iterator it = begin(), stop = end(); stop != it; ++it)
        {
            if(pred(*it))
            {
                return it;
            }
        }

        return end();
    }

    //PURPOSE
    //  Returns the number of allocated items.
    size_t size() const
    {
        return m_NumAllocatedItems;
    }

    //PURPOSE
    //  Returns the total size of the pool.
    size_t capacity() const
    {
        return POOL_SIZE;
    }

private:
	//PURPOSE
	//  allocates an item from the pool and returns a pointer to the item buffer
	uint8_t* allocItem()
    {
        ListLink* item = m_FreeList.m_Next;

        if(!netVerifyf(&m_FreeList != item, "Pool is exhausted - used %u items", (u32)POOL_SIZE))
        {
            return nullptr;
        }

        item->Unlink();

        item->Link(&m_UsedList);

        ++m_NumAllocatedItems;

        return static_cast<PooledItem*>(item)->m_ItemBuf;
    }

    //Non-copy-able
    netPool(const netPool&);
    netPool(const netPool&&);
    netPool& operator=(const netPool&);
    netPool& operator=(const netPool&&);

    std::array<PooledItem, POOL_SIZE> m_Pile;
    ListLink m_FreeList;
    ListLink m_UsedList;
    size_t m_NumAllocatedItems;
};

}   //namespace rage

#endif  //NET_POOL_H