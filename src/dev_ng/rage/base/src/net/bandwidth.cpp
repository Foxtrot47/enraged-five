// 
// net/bandwidth.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "bandwidth.h"

#include "diag/seh.h"
#include "message.h"
#include "netsocket.h"
#include "transaction.h"

namespace rage
{

///////////////////////////////////////////////////////////////////////////////
//  netPacketRecorder
///////////////////////////////////////////////////////////////////////////////

netPacketRecorder::netPacketRecorder()
    : m_Owner(0)
{
}

netPacketRecorder::~netPacketRecorder()
{
    netAssertf(!m_Owner , "Forgot to unregister recorder");

    //NOTE: Don't call Unregister() from here - it is not threadsafe
}

///////////////////////////////////////////////////////////////////////////////
//  netBandwidthRecorder
///////////////////////////////////////////////////////////////////////////////

netBandwidthRecorder::netBandwidthRecorder()
    : m_SampleInterval(DEFAULT_SAMPLE_INTERVAL_MS)
    , m_InBw(0)
    , m_OutBw(0)
    , m_InPkt(0)
    , m_OutPkt(0)
    , m_InBwAccum(0)
    , m_OutBwAccum(0)
    , m_InPktAccum(0)
    , m_OutPktAccum(0)
    , m_LastUpdateTime(0)
{
}

void netBandwidthRecorder::CalculateBandwidth(const unsigned curTimeMs)
{
	const unsigned adjTime = curTimeMs | 0x01;

	if (m_LastUpdateTime)
	{
		const unsigned interval = adjTime - m_LastUpdateTime;

		if (interval >= m_SampleInterval)
		{
			m_InBw = static_cast<unsigned>(((1000.0f * m_InBwAccum) / interval) + 0.5f);
			m_OutBw = static_cast<unsigned>(((1000.0f * m_OutBwAccum) / interval) + 0.5f);
			m_InPkt = static_cast<unsigned>(((1000.0f * m_InPktAccum) / interval) + 0.5f);
			m_OutPkt = static_cast<unsigned>(((1000.0f * m_OutPktAccum) / interval) + 0.5f);

			m_InBwAccum = 0;
			m_OutBwAccum = 0;
			m_InPktAccum = 0;
			m_OutPktAccum = 0;

			m_LastUpdateTime = adjTime;
		}
	}
	else
	{
		m_LastUpdateTime = adjTime;
	}
}

void netBandwidthRecorder::SampleData(const unsigned curTimeMs)
{
    // if the sample interval is non zero this recorder has been setup to
    // use automatic sampling and should not be calling this function.
    // Data will be sampled via the Update() call to this class
    netAssert(m_SampleInterval == 0);

    if(m_SampleInterval == 0)
    {
		CalculateBandwidth(curTimeMs);
    }
}

void
netBandwidthRecorder::Update(const unsigned curTimeMs)
{
    // if the sample interval is set to 0 this recorder is using
    // manual data sampling
    if(m_SampleInterval != 0)
    {
		CalculateBandwidth(curTimeMs);
    }
}

void
netBandwidthRecorder::SetSampleInterval(const unsigned interval)
{
    m_SampleInterval = interval;
}

unsigned
netBandwidthRecorder::GetSampleInterval() const
{
    return m_SampleInterval;
}

void
netBandwidthRecorder::RecordOutboundPacket(const netSocketAddress& /*addr*/,
                                            const void* /*bytes*/,
                                            const unsigned numBytes)
{
    m_OutBwAccum += m_Owner ? m_Owner->SizeofPacket(numBytes) : numBytes;
    m_OutPktAccum++;
}

void
netBandwidthRecorder::RecordInboundPacket(const netSocketAddress& /*addr*/,
                                            const void* /*bytes*/,
                                            const unsigned numBytes)
{
    m_InBwAccum += m_Owner ? m_Owner->SizeofPacket(numBytes) : numBytes;
    m_InPktAccum++;
}

unsigned
netBandwidthRecorder::GetOutboundBandwidth() const
{
    return m_OutBw;
}

unsigned
netBandwidthRecorder::GetInboundBandwidth() const
{
    return m_InBw;
}

unsigned
netBandwidthRecorder::GetOutboundPacketFreq() const
{
    return m_OutPkt;
}

unsigned
netBandwidthRecorder::GetInboundPacketFreq() const
{
    return m_InPkt;
}

unsigned netBandwidthRecorder::GetOutboundAccumulatedBytes() const
{
	return m_OutBwAccum;
}

unsigned netBandwidthRecorder::GetInboundAccmulatedBytes() const
{
	return m_InBwAccum;
}

///////////////////////////////////////////////////////////////////////////////
//  netMessageHistogram
///////////////////////////////////////////////////////////////////////////////

netMessageHistogram::netMessageHistogram()
: m_Allocator(NULL)
, m_MsgsIn(NULL)
, m_MsgsOut(NULL)
, m_NumIn(0)
, m_NumOut(0)
, m_SampleInterval(5000)
, m_LastUpdateTime(0)
{
}

netMessageHistogram::~netMessageHistogram()
{
    this->Shutdown();
}

bool
netMessageHistogram::Init(sysMemAllocator* allocator)
{
    bool success = false;

    if(netVerify(!m_Allocator))
    {
        m_Allocator = allocator;

        const unsigned sizeofBufs =
            sizeof(netMessageCounter) * netMessage::GetMessageCount();

        m_MsgsIn = (netMessageCounter*) m_Allocator->Allocate(sizeofBufs, 0);
        m_MsgsOut = (netMessageCounter*) m_Allocator->Allocate(sizeofBufs, 0);

        if(m_MsgsIn && m_MsgsOut)
        {
            sysMemSet(m_MsgsIn, 0, sizeofBufs);
            sysMemSet(m_MsgsOut, 0, sizeofBufs);
            success = true;
        }
        else
        {
            this->Shutdown();
        }
    }

    return success;
}

void
netMessageHistogram::Shutdown()
{
    if(m_Allocator)
    {
        if(m_MsgsIn)
        {
            m_Allocator->Free(m_MsgsIn);
        }

        if(m_MsgsOut)
        {
            m_Allocator->Free(m_MsgsOut);
        }

        m_MsgsIn = m_MsgsOut = NULL;
        m_NumIn = m_NumOut = 0;
        m_Allocator = NULL;
    }
}

void
netMessageHistogram::Reset()
{
    for(int i = 0; i < (int) m_NumIn; ++i)
    {
        for(int j = 0; j < MSGDISP_NUM_DISPOSITIONS; ++j)
        {
            m_MsgsIn[i].m_Count[j] = 0;
            m_MsgsIn[i].m_NumBytes[j] = 0;
        }
        m_MsgsIn[i].m_CountAccum = 0;
        m_MsgsIn[i].m_NumBytesAccum = 0;
        m_MsgsIn[i].m_Frequency = 0.0f;
        m_MsgsIn[i].m_KilobitsPerSec = 0.0f;
    }

    for(int i = 0; i < (int) m_NumOut; ++i)
    {
        for(int j = 0; j < MSGDISP_NUM_DISPOSITIONS; ++j)
        {
            m_MsgsOut[i].m_Count[j] = 0;
            m_MsgsOut[i].m_NumBytes[j] = 0;
        }
        m_MsgsOut[i].m_CountAccum = 0;
        m_MsgsOut[i].m_NumBytesAccum = 0;
        m_MsgsOut[i].m_Frequency = 0.0f;
        m_MsgsOut[i].m_KilobitsPerSec = 0.0f;
    }
}

void
netMessageHistogram::SampleData(const unsigned /*curTimeMs*/)
{
}

void
netMessageHistogram::Update(const unsigned curTimeMs)
{
//using convention that a kilobit is 1000 bits (not 1024)...because that's how the Xbox 360 TCRs are defined.
#define BYTES_TO_KILOBITS(uBytes) ((uBytes) * (8 / 1000.0f))

    const unsigned adjTime = curTimeMs | 0x01;

    if(m_LastUpdateTime && m_SampleInterval > 0)
    {
        const unsigned interval = adjTime - m_LastUpdateTime;

        if(interval >= m_SampleInterval)
        {
            float invInterval = 1000.0f / interval;	//ms -> seconds

            for(int i = 0; i < (int) m_NumIn; ++i)
            {
                m_MsgsIn[i].m_Frequency = m_MsgsIn[i].m_CountAccum * invInterval;
                m_MsgsIn[i].m_KilobitsPerSec = BYTES_TO_KILOBITS(m_MsgsIn[i].m_NumBytesAccum) * invInterval;

                m_MsgsIn[i].m_CountAccum = 0;
                m_MsgsIn[i].m_NumBytesAccum = 0;
            }

            for(int i = 0; i < (int) m_NumOut; ++i)
            {
                m_MsgsOut[i].m_Frequency = m_MsgsOut[i].m_CountAccum * invInterval;
                m_MsgsOut[i].m_KilobitsPerSec = BYTES_TO_KILOBITS(m_MsgsOut[i].m_NumBytesAccum) * invInterval;

                m_MsgsOut[i].m_CountAccum = 0;
                m_MsgsOut[i].m_NumBytesAccum = 0;
            }

            m_LastUpdateTime = adjTime;
        }
    }
    else
    {
        m_LastUpdateTime = adjTime;
    }
}

void
netMessageHistogram::RecordOutboundPacket(const netSocketAddress& /*addr*/,
                                        const void* bytes,
                                        const unsigned numBytes)
{
    if(netVerify(m_MsgsOut))
    {
        RecordMessages(bytes, numBytes, m_MsgsOut, &m_NumOut);
    }
}

void
netMessageHistogram::RecordInboundPacket(const netSocketAddress& /*addr*/,
                                        const void* bytes,
                                        const unsigned numBytes)
{
    if(netVerify(m_MsgsIn))
    {
        RecordMessages(bytes, numBytes, m_MsgsIn, &m_NumIn);
    }
}

unsigned
netMessageHistogram::GetOutCounts(const netMessageCounter** counters) const
{
    if(m_NumOut)
    {
        *counters = m_MsgsOut;
    }
    else
    {
        *counters = NULL;
    }

    return m_NumOut;
}

unsigned
netMessageHistogram::GetInCounts(const netMessageCounter** counters) const
{
    if(m_NumIn)
    {
        *counters = m_MsgsIn;
    }
    else
    {
        *counters = NULL;
    }

    return m_NumIn;
}

bool
netMessageHistogram::GetOutCsv(char* buf,
                            const unsigned bufLen) const
{
    return netMessageHistogram::GetCsv(buf, bufLen, m_MsgsOut, m_NumOut);
}

bool
netMessageHistogram::GetInCsv(char* buf,
                            const unsigned bufLen) const
{
    return netMessageHistogram::GetCsv(buf, bufLen, m_MsgsIn, m_NumIn);
}

//private:

void
netMessageHistogram::RecordMessages(const void* bytes,
                                    const unsigned numBytes,
                                    netMessageCounter* msgCounts,
                                    unsigned* numIds)
{
    //We do our best to make sure the buffer contains a bundle
    //by verifying various header and buffer sizes.

    rtry
    {
        rcheck(numBytes > netBundle::BYTE_SIZEOF_HEADER,catchall,);

        const netBundle* bndl = (const netBundle*) bytes;
        rcheck(bndl->GetSize() > netBundle::BYTE_SIZEOF_HEADER,catchall,);
        rcheck(bndl->GetSize() <= netBundle::MAX_BYTE_SIZEOF_BUNDLE,catchall,);
        rcheck(bndl->GetSize() <= numBytes,catchall,);

        const unsigned sizeofBndlPld = bndl->GetSizeofPayload();
        rcheck(sizeofBndlPld <= netBundle::MAX_BYTE_SIZEOF_PAYLOAD,catchall,);
        rcheck(sizeofBndlPld <= (numBytes - netBundle::BYTE_SIZEOF_HEADER),catchall,);

        if(bndl->GetFlags() & netBundle::FLAG_OUT_OF_BAND)
        {
            netMessageHistogram::ParsePayload(bndl->GetPayload(),
                                                bndl->GetSizeofPayload(),
                                                msgCounts,
                                                numIds,
                                                MSGDISP_OUT_OF_BAND);
        }
        else
        {
            for(int offset = 0; offset < (int) sizeofBndlPld;)
            {
                const unsigned bytesLeft = sizeofBndlPld - (unsigned) offset;

                rcheck(bytesLeft > netFrame::BYTE_SIZEOF_RHEADER
                        || bytesLeft > netFrame::BYTE_SIZEOF_UHEADER,catchall,);

                const netFrame* frm =
                    (const netFrame*) &((const u8*) bndl->GetPayload())[offset];

                rcheck(frm->GetSize() > netFrame::BYTE_SIZEOF_RHEADER
                        || frm->GetSize() > netFrame::BYTE_SIZEOF_UHEADER,catchall,);
                rcheck(frm->GetSize() <= netFrame::MAX_BYTE_SIZEOF_FRAME,catchall,);
                rcheck(frm->GetSizeofHeader() < bytesLeft,catchall,);
                rcheck(frm->GetSize() <= bytesLeft,catchall,);

                const unsigned sizeofFrmPld = frm->GetSizeofPayload();
                rcheck(sizeofFrmPld <= netFrame::MAX_BYTE_SIZEOF_PAYLOAD,catchall,);
                rcheck(sizeofFrmPld <= (bytesLeft - frm->GetSizeofHeader()),catchall,);

                netMessageHistogram::ParsePayload(frm->GetPayload(),
                                                sizeofFrmPld,
                                                msgCounts,
                                                numIds,
                                                frm->IsReliable() ? MSGDISP_RELIABLE : MSGDISP_UNRELIABLE);

                offset += frm->GetSize();
            }
        }
    }
    rcatchall
    {
    }
}

void
netMessageHistogram::ParsePayload(const void* bytes,
                                const unsigned numBytes,
                                netMessageCounter* msgCounts,
                                unsigned* numIds,
                                const netMessageDisposition msgDisp)
{
    const void* msgBuf = NULL;
    unsigned sizeofMsgBuf = 0;

    //Bundle payloads typically contain either transactions
    //or netMessages.  Transactions, in turn, typically contain
    //netMessages.
    if(netTransaction::IsRequest(bytes, numBytes)
        || netTransaction::IsResponse(bytes, numBytes))
    {
        msgBuf = netTransaction::GetPayload(bytes,
                                            numBytes,
                                            &sizeofMsgBuf);
    }
    else if(netMessage::IsMessage(bytes, numBytes))
    {
        msgBuf = bytes;
        sizeofMsgBuf = numBytes;
    }

    if(msgBuf)
    {
        unsigned msgId;
        if(netMessage::GetId(&msgId, msgBuf, sizeofMsgBuf))
        {
            bool foundIt = false;

            for(int i = 0; i < (int) *numIds; ++i)
            {
                if(msgCounts[i].m_MessageId == msgId)
                {
                    ++msgCounts[i].m_Count[msgDisp];
                    //Overflow?
                    netAssert(msgCounts[i].m_NumBytes[msgDisp] < msgCounts[i].m_NumBytes[msgDisp] + sizeofMsgBuf);
                    msgCounts[i].m_NumBytes[msgDisp] += sizeofMsgBuf;
                    
                    ++msgCounts[i].m_CountAccum;
                    msgCounts[i].m_NumBytesAccum += sizeofMsgBuf;

                    foundIt = true;
                    break;
                }
            }

            if(!foundIt)
            {
                if(netVerify(*numIds < netMessage::GetMessageCount()))
                {
                    msgCounts[*numIds].m_MessageId = msgId;
                    msgCounts[*numIds].m_Count[msgDisp] = 1;
                    msgCounts[*numIds].m_NumBytes[msgDisp] = sizeofMsgBuf;
                    msgCounts[*numIds].m_CountAccum = 1;
                    msgCounts[*numIds].m_NumBytesAccum = sizeofMsgBuf;
                    ++*numIds;
                }
            }
        }
    }
}

bool
netMessageHistogram::GetCsv(char* buf,
                            const unsigned bufLen,
                            netMessageCounter* msgCounts,
                            const unsigned numIds)
{
    char* p = buf;
    const char* eob = buf + bufLen;

    safecpy(p, "Name,R_Count,R_NumBytes,U_Count,U_NumBytes,OOB_Count,OOB_NumBytes\n", (eob-p));
    p += strlen(p);
    int i;

    for(i = 0; i < (int) numIds && p < eob; ++i)
    {
#if !__NO_OUTPUT
        const char* name = netMessage::GetName(msgCounts[i].m_MessageId);
#else
		const char* name = "";
#endif
        if(netVerify(name))
        {
            formatf_sized(p, (eob-p), "%s,%d,%d,%d,%d,%d,%d\n",
                    name,
                    msgCounts[i].m_Count[MSGDISP_RELIABLE],
                    msgCounts[i].m_NumBytes[MSGDISP_RELIABLE],
                    msgCounts[i].m_Count[MSGDISP_UNRELIABLE],
                    msgCounts[i].m_NumBytes[MSGDISP_UNRELIABLE],
                    msgCounts[i].m_Count[MSGDISP_OUT_OF_BAND],
                    msgCounts[i].m_NumBytes[MSGDISP_OUT_OF_BAND]);
            p += strlen(p);
        }
    }

    return (i == (int) numIds);
}

///////////////////////////////////////////////////////////////////////////////
//  netBandwidthTestResult
///////////////////////////////////////////////////////////////////////////////
netBandwidthTestResult::netBandwidthTestResult()
    : m_TestTime(0)
    , m_UploadBps(0)
    , m_DownloadBps(0)
    , m_ResultCode(0)
    , m_Source(0)
{
}

netBandwidthTestResult::netBandwidthTestResult(const unsigned source, const u64 testTime)
    : m_TestTime(testTime)
    , m_UploadBps(0)
    , m_DownloadBps(0)
    , m_ResultCode(0)
    , m_Source(source)
{

}

bool
netBandwidthTestResult::IsValid() const
{
    return m_UploadBps != 0 || m_DownloadBps != 0;
}

}   //namespace rage

