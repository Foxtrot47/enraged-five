// 
// net/netdiag.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "netdiag.h"
#include "netsocket.h"

namespace rage
{

RAGE_DEFINE_CHANNEL(ragenet,
					DIAG_SEVERITY_DEBUG1,
					DIAG_SEVERITY_WARNING, 
					DIAG_SEVERITY_ASSERT,
					CHANNEL_POSIX_ON);

const char*
netSocketErrorString( const int errorCode )
{
    const char* str;
    
#define SKT_ERR_CASE(e) e: str = #e;

    switch( errorCode )
    {
        // no error (m_Error is set to this if the function succeeds)
        case SKT_ERR_CASE(NET_SOCKERR_NONE)
            break;

        // WSAENETDOWN - network shut down, probably due to buffer overflow or memory failure
        case SKT_ERR_CASE(NET_SOCKERR_NETWORK_DOWN)
            break;

        // WSAENOBUFS - outgoing buffers full
        case SKT_ERR_CASE(NET_SOCKERR_NO_BUFFERS)
            break;

        case SKT_ERR_CASE(NET_SOCKERR_MESSAGE_TOO_LONG)
            break;

        // WSAEHOSTUNREACH - Xbox NAT negotiation failure
        case SKT_ERR_CASE(NET_SOCKERR_HOST_UNREACHABLE)
            break;

        // WSAECONNRESET - stop trying to send to somebody who stopped listening
        case SKT_ERR_CASE(NET_SOCKERR_CONNECTION_RESET)
            break;

        case SKT_ERR_CASE(NET_SOCKERR_WOULD_BLOCK)
            break;

        case SKT_ERR_CASE(NET_SOCKERR_INVALID_SOCKET)
            break;

        case SKT_ERR_CASE(NET_SOCKERR_UNKNOWN)
            break;

		// Network disconnection occurred because of an IP address release
        case SKT_ERR_CASE(NET_SOCKERR_INACTIVEDISABLED)
			break;

        default:
            str = "NET_SOCKERR_UNKNOWN";
            break;
    }

    return str;
}

}   //namespace rage
