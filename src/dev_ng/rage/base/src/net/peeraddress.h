// 
// net/peeraddress.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_PEERADDRESS_H
#define NET_PEERADDRESS_H

#include "file/file_config.h"
#include "net/crypto.h"
#include "net/netaddress.h"
#include "net/netpeerid.h"
#include "data/base64.h"
#include "rline/rlgamerhandle.h"
#include "net/durango/xblnet.h"

#define NET_PEER_ADDR_CANDIDATES RSG_GGP

namespace rage
{
enum netCandidateAddrType
{
	ADDR_TYPE_INVALID,

	// address to use for P2P communications on platforms that require
	// a specific address for P2P communications (Stadia). There can
	// be more than one platform P2P address (e.g. one IPv4, one IPv6).
	ADDR_TYPE_PLATFORM_P2P,

	ADDR_TYPE_NUM_TYPES,
};

//PURPOSE
//  This class encapsulates a peer address.
//
//  Call netPeerAddress::GetLocalAddress() to get the peer address
//  for the local machine.
class netPeerAddress
{
private:
#if NET_PEER_ADDR_CANDIDATES
	struct P2pCandidateAddr
	{

		static const unsigned MAX_EXPORTED_SIZE_IN_BITS = (netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES << 3) +					// m_Addr
															datBitsNeeded<netCandidateAddrType::ADDR_TYPE_NUM_TYPES>::COUNT;	// m_Type
		static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((MAX_EXPORTED_SIZE_IN_BITS + 7) & ~7) >> 3;							// round up to nearest multiple of 8 then divide by 8

		void Clear();

		//PURPOSE
		//  Exports data in a platform/endian independent format.
		//PARAMS
		//  buf         - Destination buffer.
		//  sizeofBuf   - Size of destination buffer.
		//  size        - Set to number of bytes exported.
		//RETURNS
		//  True on success.
		bool Export(void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0) const;

		//PURPOSE
		//  Imports data exported with Export().
		//  buf         - Source buffer.
		//  sizeofBuf   - Size of source buffer.
		//  size        - Set to number of bytes imported.
		//RETURNS
		//  True on success.
		bool Import(const void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0);

		netSocketAddress m_Addr;
		netCandidateAddrType m_Type;
	};

	class P2pCandidateAddrs
	{
	public:
		static const unsigned MAX_P2P_CANDIDATE_ADDRS = 4;

		static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = (P2pCandidateAddr::MAX_EXPORTED_SIZE_IN_BYTES * MAX_P2P_CANDIDATE_ADDRS) +	// m_P2pCandidate
														   sizeof(unsigned);															// m_NumCandidates

		P2pCandidateAddrs();

		void Clear();

		//PURPOSE
		//  Exports data in a platform/endian independent format.
		//PARAMS
		//  buf         - Destination buffer.
		//  sizeofBuf   - Size of destination buffer.
		//  size        - Set to number of bytes exported.
		//RETURNS
		//  True on success.
		bool Export(void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0) const;

		//PURPOSE
		//  Imports data exported with Export().
		//  buf         - Source buffer.
		//  sizeofBuf   - Size of source buffer.
		//  size        - Set to number of bytes imported.
		//RETURNS
		//  True on success.
		bool Import(const void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0);
		
		P2pCandidateAddr m_P2pCandidate[MAX_P2P_CANDIDATE_ADDRS];
		unsigned m_NumCandidates;
	};
#endif

public:

    static const netPeerAddress INVALID_PEER_ADDRESS;

    //PURPOSE
    //  Returns true if the local peer has a network.
    static bool HasNetwork();

    //PURPOSE
    //  Retrieves the peer address for the local machine.
    //PARAMS
    //  info        - Peer info.
    //RETURNS
    //  True on success.
    static bool GetLocalPeerAddress(const int localGamerIndex, netPeerAddress* addr);

    netPeerAddress();

    //PURPOSE
    //  Clears/invalidates the peer address.
    void Clear();

	//PURPOSE
	//  Clears the data that we don't advertise on any publicly queryable service.
	void ClearNonAdvertisableData();

    //PURPOSE
    //  Returns true for a valid peer address.
    bool IsValid() const;

    //PURPOSE
    //  Returns true if the peer address represents the local peer.
    bool IsLocal() const;

    //PURPOSE
    //  Returns true if the peer address represents a remote peer.
    bool IsRemote() const;

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

#if RSG_ORBIS
	//PURPOSE
	//  Exports the peer address for NP presence.
	//  Same as Export() but omits some data to get an rlSessionInfo
	//  (which contains a peer address) under the 128 byte limit.
    bool ExportForNpPresence(void* buf,
							 const unsigned sizeofBuf,
							 unsigned* size = 0) const;

	//PURPOSE
	//  Imports a peer address that was exported from 
	//  ExportForNpPresence().
    bool ImportFromNpPresence(const void* buf,
							  const unsigned sizeofBuf,
							  unsigned* size = 0);
#endif

	static const unsigned MAX_EXPORTED_SIZE_IN_BITS = ((netPeerId::MAX_EXPORTED_SIZE_IN_BYTES +					// m_PeerId
														rlGamerHandle::MAX_EXPORTED_SIZE_IN_BYTES +				// m_GamerHandle
														netP2pCrypt::Key::MAX_EXPORTED_SIZE_IN_BYTES +			// m_Key
														netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES +			// m_RelayServerAddress
														netRelayToken::MAX_EXPORTED_SIZE_IN_BYTES +				// m_RelayToken
														netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES +			// m_PublicAddress
														netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES +			// m_PrivateAddress
#if NET_PEER_ADDR_CANDIDATES
														P2pCandidateAddrs::MAX_EXPORTED_SIZE_IN_BYTES			// m_P2pCandidates
#else
														0
#endif
													   ) << 3) + datBitsNeeded<NET_NAT_NUM_TYPES>::COUNT;		// m_NatType

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((MAX_EXPORTED_SIZE_IN_BITS + 7) & ~7) >> 3;				// round up to nearest multiple of 8 then divide by 8

#if RSG_ORBIS
	static const unsigned NP_PRESENCE_DATA_MAX_EXPORTED_SIZE_IN_BYTES = MAX_EXPORTED_SIZE_IN_BYTES +
																		sizeof(rlSceNpAccountId) -						// m_AccoundId
																		rlGamerHandle::MAX_EXPORTED_SIZE_IN_BYTES -		// m_GamerHandle
																		netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES;	// m_PublicAddress
#endif

	static const unsigned TO_STRING_BUFFER_SIZE = DAT_BASE64_MAX_ENCODED_SIZE(MAX_EXPORTED_SIZE_IN_BYTES);

	const char* ToString(char* buf, const unsigned sizeofBuf, unsigned* size = 0) const;
    template<int SIZE>
	const char* ToString(char (&buf)[SIZE], unsigned* size = 0) const
    {
        CompileTimeAssert(SIZE >= TO_STRING_BUFFER_SIZE);
        return ToString(buf, SIZE, size);
    }
	bool FromString(const char* buf, unsigned* size = 0);

#if !__NO_OUTPUT
	//PURPOSE
	//  Convenience function to return a string representation of the IP address
	//  without having to supply a buffer. Used in calls to printf()-like functions.
	//  Note: this is a human readable string rather than the base64-encoded
	//  string output by the ToString(buf) versions above.
	const char* ToString() const;
#endif

	//PURPOSE
	//  Returns the combination of relay server address and relay mapped address.	
	const netAddress GetRelayAddress() const;

	//PURPOSE
	//  Returns the address of the relay server to which the peer is connected.
	const netSocketAddress& GetRelayServerAddress() const;

	//PURPOSE
	//  Returns the peer's public address as seen by the peer's relay server.
	//  Note: Only valid after getting a Social Club ticket.
	//        The port will be 0 (invalid) if the peer could not connect to the relay.
	const netSocketAddress& GetPublicAddress() const;

	//PURPOSE
	//  Returns the peer's private address. Valid after creating the main socket.
	const netSocketAddress& GetPrivateAddress() const;

	//PURPOSE
	//  Returns a P2P candidate address and its type.
	//PARAMS
	//  index        - Index of address to retrieve. Must be < GetNumP2pCandidates().
	//  addr		 - The candidate address to use for P2P communications.
	//  type		 - The type of candidate address.
	//RETURNS
	//  True on success. False if the address is invalid for the given index.
	bool GetP2pCandidateAddress(const unsigned index, netSocketAddress& addr, netCandidateAddrType& type) const;

	//PURPOSE
	//  Returns the number of candidate addresses to use for P2P communications.
	unsigned GetNumP2pCandidates() const;

	//PURPOSE
	//  Returns the NAT type of the peer. This gets serialized so
	//  we know the NAT type of remote peers when connecting to them.
	//  Valid after NAT type detection has completed.
	netNatType GetNatType() const;

	//PURPOSE
	//  Returns the globally unique peer id associated with this peer address.
	//  This is used for p2p relaying.
	const netPeerId& GetPeerId() const;

	//PURPOSE
	//  Returns the gamerhandle associated with this peer address.
	//  This is used to look up the up-to-date relay address since
	//  the data stored in the peer address can change over time (eg.
	//  the relay address). Note, the gamerhandle will only be
	//  valid after the user has signed in. Check .IsValid().
	const rlGamerHandle& GetGamerHandle() const;

	//PURPOSE
	//  Returns the peer key for this object.
	const netP2pCrypt::Key& GetPeerKey() const;

	//PURPOSE
	//  Clears/invalidates the encryption key.
	void ClearKey();

private:
#if NET_PEER_ADDR_CANDIDATES
	//PURPOSE
	//  Gathers an extended set of addresses that remote peers may be able to use
	//  to connect to the local peer. Used in addition to the private/public
	//  address to find a direct path between peers during NAT traversal.
	void GatherLocalP2pCandidates();
#endif

	bool ExportInternal(void* buf,
						const unsigned sizeofBuf,
						const bool forNpPresence,
						unsigned* size = 0) const;

	bool ImportInternal(const void* buf,
						const unsigned sizeofBuf,
						const bool fromNpPresence,
						unsigned* size = 0);

	// Peer addresses aren't comparable. Different peers can have the same
	// IP/ports due to port overloading, shared VPNs, etc., and the same
	// peer can change addresses (especially public ports) mid-session.
	// Compare the individual members that you're interested in instead.
	bool operator==(const netPeerAddress& that) const;
	bool operator!=(const netPeerAddress& that) const;
	bool operator<(const netPeerAddress& that) const;

	netPeerId m_PeerId;
	rlGamerHandle m_GamerHandle;
	netP2pCrypt::Key m_Key;
	netSocketAddress m_RelayServerAddress;
	netRelayToken m_RelayToken;
	netSocketAddress m_PublicAddress;
	netSocketAddress m_PrivateAddress;
#if NET_PEER_ADDR_CANDIDATES
	P2pCandidateAddrs m_P2pCandidates;
#endif
	netNatType m_NatType;

#if !__NO_OUTPUT
	static const unsigned MAX_OUTPUT_STRING = 256;
	mutable char m_Str[MAX_OUTPUT_STRING];
#endif
};

}   //namespace rage

#endif  //NET_PEERADDRESS_H
