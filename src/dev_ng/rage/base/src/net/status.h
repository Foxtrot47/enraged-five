// 
// net/status.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_STATUS_H
#define NET_STATUS_H

#include "atl/inlist.h"
#include "system/interlocked.h"
#include "netdiag.h"

namespace rage
{

//PURPOSE
//  A generic thread-safe status object.
class netStatus
{
public:

    enum StatusCode
    {
        NET_STATUS_NONE,
        NET_STATUS_PENDING,
        NET_STATUS_FAILED,
        NET_STATUS_SUCCEEDED,
        NET_STATUS_CANCELED,
        NET_STATUS_COUNT
    };

    netStatus()
    : m_Status(NET_STATUS_NONE)
    , m_ResultCode(0)
    {
    }

    ~netStatus()
    {
        netAssertf(!this->Pending() , "In netStatus destructor while still pending");
    }

    //PURPOSE
    //  Resets the status to its initial state.  A status object can be reset
    //  at any time.
    void Reset()
    {
        NET_ASSERTS_ONLY(unsigned oldStatus =) sysInterlockedExchange(&m_Status, NET_STATUS_NONE);
        netAssertf(NET_STATUS_PENDING != oldStatus, "Old status: %d", oldStatus);
        m_ResultCode = 0;
    }

    //PURPOSE
    //  Returns the current status code.
    StatusCode GetStatus() const
    {
        return m_StatusCode;
    }

    //PURPOSE
    //  Returns the result code.  This value is an app-apecific value.
    int GetResultCode() const
    {
        return m_ResultCode;
    }

    //PURPOSE
    //  Sets the current status code.
    //NOTES
    //  There are restrictions on what codes can be set at what times.
    //  For example, NET_STATUS_PENDING can only be set when the status
    //  is not already NET_STATUS_PENDING.
    void SetStatus(const StatusCode code)
    {
        switch(code)
        {
            case NET_STATUS_PENDING:
            case NET_STATUS_SUCCEEDED:
                SetStatus(code, 0);
                break;
            case NET_STATUS_FAILED:
            case NET_STATUS_CANCELED:
            default:
                SetStatus(code, -1);
                break;
        }
    }

    void SetStatus(const StatusCode code,
                   const int resultCode)
    {
		unsigned oldStatus;
        switch(code)
        {
            case NET_STATUS_PENDING:
				oldStatus = sysInterlockedExchange(&m_Status, NET_STATUS_PENDING);
                netAssertf(NET_STATUS_PENDING != oldStatus, "Old status: %u", oldStatus);
                m_ResultCode = 0;
                break;
            case NET_STATUS_SUCCEEDED:
				oldStatus =
                    sysInterlockedCompareExchange(&m_Status, NET_STATUS_SUCCEEDED, NET_STATUS_PENDING);
                if(netVerifyf(NET_STATUS_PENDING == oldStatus, "Old status: %u", oldStatus))
                {
                    m_ResultCode = resultCode;
                }
                break;
            case NET_STATUS_FAILED:
				oldStatus =
                    sysInterlockedCompareExchange(&m_Status, NET_STATUS_FAILED, NET_STATUS_PENDING);
                if(netVerifyf(NET_STATUS_PENDING == oldStatus, "Old status: %u", oldStatus))
                {
                    m_ResultCode = resultCode;
                }
                break;
            case NET_STATUS_CANCELED:
                if(NET_STATUS_PENDING ==
                    sysInterlockedCompareExchange(&m_Status,
                                                    NET_STATUS_CANCELED,
                                                    NET_STATUS_PENDING))
                {
                    m_ResultCode = resultCode;
                }
                break;
            default:
                netAssert(false);
                break;
        }
    }

    //PURPOSE
    //  Places a status object in the PENDING state.  This will assert if
    //  the status is already PENDING.
    void SetPending()
    {
        this->SetStatus(NET_STATUS_PENDING);
    }

    //PURPOSE
    //  Places a status object in the SUCCEEDED state if it is currently
    //  in the PENDING state.  This will assert if the status is not PENDING.
    //NOTES
    //  If the status object is not PENDING its state will not change.
    void SetSucceeded(const int resultCode = 0)
    {
        this->SetStatus(NET_STATUS_SUCCEEDED, resultCode);
    }

    //PURPOSE
    //  Places a status object in the FAILED state if it is currently
    //  in the PENDING state.  This will assert if the status is not PENDING.
    //NOTES
    //  If the status object is not PENDING its state will not change.
    void SetFailed(const int resultCode = -1)
    {
        this->SetStatus(NET_STATUS_FAILED, resultCode);
    }

    //PURPOSE
    //  Places a status object in the CANCELED state if it is currently
    //  in the PENDING state.  This will not assert.
    //NOTES
    //  If the status object is not PENDING its state will not change.
    void SetCanceled()
    {
        this->SetStatus(NET_STATUS_CANCELED);
    }

    //PURPOSE
    //  Convenience function for placing the status object in a SUCCEEDED
    //  state from a non-pending state.
    //NOTES
    //  If the status object is PENDING its state will not change.
    void ForceSucceeded(const int resultCode = 0)
    {
        NET_ASSERTS_ONLY(unsigned oldStatus =) sysInterlockedExchange(&m_Status, NET_STATUS_SUCCEEDED);
        netAssertf(NET_STATUS_PENDING != oldStatus, "Old status: %u", oldStatus);
        m_ResultCode = resultCode;
    }

    //PURPOSE
    //  Convenience function for placing the status object in a FAILED
    //  state from a non-pending state.
    //NOTES
    //  If the status object is PENDING its state will not change.
    void ForceFailed(const int resultCode = -1)
    {
        NET_ASSERTS_ONLY(unsigned oldStatus =) sysInterlockedExchange(&m_Status, NET_STATUS_FAILED);
        netAssertf(NET_STATUS_PENDING != oldStatus, "Old status: %u", oldStatus);
        m_ResultCode = resultCode;
    }

    //PURPOSE
    //  Returns true if the status is PENDING.
    bool Pending() const
    {
        return NET_STATUS_PENDING == m_Status;
    }

    //PURPOSE
    //  Returns true if the status is SUCCEEDED.
    bool Succeeded() const
    {
        return NET_STATUS_SUCCEEDED == m_Status;
    }

    //PURPOSE
    //  Returns true if the status is FAILED.
    bool Failed() const
    {
        return NET_STATUS_FAILED == m_Status;
    }

    //PURPOSE
    //  Returns true if the status was CANCELED.
    bool Canceled() const
    {
        return NET_STATUS_CANCELED == m_Status;
    }

	//PURPOSE
	//	Returns true if the status is a success, failure or cancellation.
	//	Returns false if the request is still pending or never started at all.
	bool Finished() const 
	{
		return Succeeded() || Failed() || Canceled();
	}

	//PURPOSE
	//  Returns true if the status is NONE.
	bool None() const
	{
		return NET_STATUS_NONE == m_Status;
	}

#if !__NO_OUTPUT
	//PURPOSE
	//	Helper functions that return the status code as a string.
	static const char *GetStatusCodeString(const StatusCode statusCode);
    static const char *GetStatusCodeString(const netStatus &status) { return GetStatusCodeString(status.m_StatusCode); }
    const char * c_str() const { return GetStatusCodeString(*this); }
#endif //!__NO_OUTPUT

private:
    union
    {
        unsigned m_Status;
        StatusCode m_StatusCode;
    };
    int m_ResultCode;
};

}   //namespace rage

#endif  //NET_STATUS_H
