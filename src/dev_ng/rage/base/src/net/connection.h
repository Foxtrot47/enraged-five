// 
// net/connection.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_CONNECTION_H
#define NET_CONNECTION_H

#include "atl/inlist.h"
#include "atl/inmap.h"
#include "event.h"
#include "netsequence.h"
#include "packet.h"
#include "policies.h"
#include "system/memory.h"

namespace rage
{

class sysMemAllocator;
class netConnectionManager;
class netEndpoint;

//PURPOSE
//  A connection represents a line of communication for sending and receiving
//  frames of data to and from remote network nodes.
//
//  A link between two nodes can have multiple connections.  Each
//  connection can be configured with different policies (e.g. timeout
//  intervals, etc.).
//
//  Data on one connection does not interfere with data on another connection.
//  For example, the data sent on a chat connection will not hold up
//  delivery of data sent on a game data connection.
//
//  Higher level code should call netConnection::NextEvent() on a regular
//  interval.  Events returned by NextEvent() must be disposed by calling
//  netEvent::Dispose().
class netConnection
{
public:

    //DO NOT change the order of these!!
    enum State
    {
        STATE_LISTEN,
        STATE_SYN_SENT,
        STATE_SYN_RECEIVED,
        STATE_ESTABLISHED,
        STATE_CLOSED,
    };

	enum TimeoutReason
	{
		CXN_TIMEOUT_REASON_NONE,
		CXN_TIMEOUT_REASON_UNACKED_FRAMES,
		CXN_TIMEOUT_REASON_NO_KEEPALIVES,
		CXN_TIMEOUT_REASON_LOCAL_STALL,
		CXN_TIMEOUT_REASON_REMOTE_STALL,
		CXN_TIMEOUT_REASON_REMOTE_SUSPENDED,
		CXN_TIMEOUT_REASON_FORCED,
		CXN_TIMEOUT_NUM_REASONS
	};

	enum 
	{
		DEFAULT_RTT_WARNING_THRESHOLD_MS	= 500,
	};

	static void SetRttWarningThresholdMs(const unsigned rttWarningThresholdMs);

    netConnection();

    virtual ~netConnection();

    //PURPOSE
    //  Initializes the connection.
    //PARAMS
    //  eventQ      - Queue to which events will be queued.
    //  allocator   - Allocator used for general memory allocation, including
    //                memory for queued frames.
    bool Init(netConnectionManager* cxnMgs,
                netEventQueue<netEvent>* eventQ,
				sysMemAllocator* allocator);

    //PURPOSE
    //  Shuts down the connection.
    void Shutdown();

    //PURPOSE
    //  Readies the connection to receive frames.
    //PARAMS
    //  epOwner     - The endpoint that owns this connection.
    //  cxnId       - Connection id.
    //  channelId   - Id of channel on which this connection exists.
    //NOTES
    //  Upon return the connection will be in a LISTEN state, in which its
    //  sequence numbers are not yet synchronized with a remote endpoint.
    //  It will only accept bundles with the SYN bit set in the bundle header,
    //  thereby initiating the synchronization phase.
    bool Open(netEndpoint* epOwner,
                const int cxnId,
                const unsigned channelId);

    //PURPOSE
    //  Synchronizes the inbound sequence number with a remote endpoint.
    //PARAMS
    //  nextInboundSeq  - Next expected inbound sequence number.
    //NOTES
    //  This function will fail unless the connection is in the LISTEN state,
    //  i.e. just after calling Open().
    bool Synchronize(const netSequence nextInboundSeq);

    //PURPOSE
    //  Closes the connection.  To re-activate the connection call Reset()
    void Close();

    void Reset();

    //PURPOSE
    //  Returns the current state.
    State GetState() const;

    //PURPOSE
    //  Returns the connection id.
    int GetId() const;

    //PURPOSE
    //  Returns the channel id.
    unsigned GetChannelId() const;

    //PURPOSE
    //  Returns the connection policies.
    const netConnectionPolicies& GetPolicies() const;

    //PURPOSE
    //  Sets the connection policies.
    //NOTES
    //  The corresponding connection on the remote peer should have
    //  the same policies, especially timeouts.
    void SetPolicies(const netConnectionPolicies& policies);

    //PURPOSE
    //  Returns the sequence number that will be used for the next
    //  outbound frame.
    netSequence GetNextOutboundSequence() const;

    //PURPOSE
    //  Returns the expected sequence number for the next inbound frame.
    netSequence GetNextInboundSequence() const;

    //PURPOSE
    //  Returns the bit mask of out-of-order frames that have been ACKed.
    unsigned GetAckBits() const;

    //PURPOSE
    //  Queues a frame of data onto the the connection's outbound queue.
    //  The sequence number of the queued frame will be copied to seq
    //  if it is non-NULL.
    //PARAMS
    //  bytes       - Data to be queued.
    //  numBytes    - Number of bytes to be queued.
    //  sendFlags   - Bits from netSendFlags enum.
    //  frameSeq    - Optional.  Set to sequence number of queued frame.
    //RETURNS
    //  True on success.
    //NOTES
    //  In low memory situations outbound unreliable frames will be dropped.
    //
    //  Reliable frames won't be dropped until an attempt is made to reclaim
    //  memory by freeing already-queued unreliable frames.
    //
    //  In both cases, if there is not enough memory to queue the frame
    //  Send() will return false.
    bool QueueFrame(const void* bytes,
                   const unsigned numBytes,
                   const unsigned sendFlags,
                   netSequence* frameSeq);

    //PURPOSE
    //RETURNS
    //  Number of bytes packed into the bundle.
    unsigned Pack(void* buf,
                    const unsigned maxLen);

    //PURPOSE
    //RETURNS
    //  Number of bytes unpacked from the bundle.
    unsigned Unpack(const void* buf,
                    const unsigned len);

    //PURPOSE
    //  Returns number of frames that have not been ACKed.
    unsigned GetUnAckedCount() const;

    //PURPOSE
    //  Returns the resend count of the oldest un-ACKed frame.
    //  Returns zero if there are no un-ACKed frames
    unsigned GetOldestResendCount() const;

    //PURPOSE
    //  Returns the sequence number of the oldest un-ACKed frame.
    //  Returns zero if there are no un-ACKed frames
    netSequence GetSequenceOfOldestResendCount() const;

	//PURPOSE
	//  Returns the time at which the oldest unacked frame was queued, in milliseconds.
	//  Returns zero if there are no un-ACKed frames
	unsigned GetQueueTimeOfOldestUnackedFrame() const;

	//PURPOSE
	//  Returns the age of the oldest unacked frame, in milliseconds.
	//  Returns zero if there are no un-ACKed frames
	unsigned GetAgeOfOldestUnackedFrame() const;

    //PURPOSE
    //  Returns the current length of the new queue
    unsigned GetNewQueueLength() const;

	//PURPOSE
	//  Returns the current length of the send queues
	unsigned GetResendQueueLength() const;

    //PURPOSE
    //  Returns number frames in the resend queue that have timed out.
    unsigned GetTimeoutCount() const;

	//PURPOSE
	//  Returns the time at which the connection started to observe a reliable
	//  frame RTT over the threshold specified. If 0, the connection
	//  is not currently experiencing an RTT over the threshold
	unsigned GetRttOverThresholdStartTime() const;

	//PURPOSE
	//  Returns the time at which the connection most recently observed a reliable
	//  frame RTT over the threshold specified. If 0, the connection
	//  is not currently experiencing an RTT over the threshold
	unsigned GetRttOverThresholdLastTime() const;

    //PURPOSE
    //  Returns true if an ACK will be sent.
    bool NeedToAck() const;

    //PURPOSE
    //  Returns true if it's time to send a keepalive.
    bool TimeForKeepalive() const;

    //PURPOSE
    //  Resets the time until the next keepalive is sent.
    void ResetKeepalive();

    //PURPOSE
    //  Returns true if the connection timed out.
    //  Call ClearTimeout() to clear the timed out state.
    bool IsTimedOut() const;
	bool IsTimedOut(TimeoutReason* timeoutReason) const;

    //PURPOSE
    //  Clears the timed out state.
    void ClearTimeout();

	//PURPOSE
	//  Returns true if we last received data on this connection more than
	//  our network time in the past
	bool TimeForTimeout();

	//PURPOSE
	//  Queues a timeout on this connection
	//PARAMS
	//  timeoutReason       - Specific cause of the timeout.
	bool QueueTimeout(const TimeoutReason timeoutReason);

#if __BANK
	//PURPOSE
	//  Forces timeout behaviour. We will immediately queue a timeout and ignore any
	//  incoming / outgoing packets
	void ForceTimeout();

	//PURPOSE
	//  Returns true if this connection is forced time out
	bool IsForcedTimeout();
#endif

	//PURPOSE
	//  Queues a OOM on this connection
	bool QueueOutOfMemory(const unsigned requiredBytes);

	//PURPOSE
	//  Returns true if we have failed to allocate critical memory for this connection
	bool IsOutOfMemory() const;

    //PURPOSE
    //  Returns the number of bytes of the largest failed allocation
    unsigned GetOutOfMemoryRequiredBytes() const;

    //PURPOSE
    //  Resets the time until we timeout.
    void ResetTimeoutTimer();

    //PURPOSE
    //  Drops outbound unreliable frames.  This is used when outbound
    //  bandwidth is exceeded and when we need to reclaim memory.
    void DropOutboundUnreliableFrames();

    //PURPOSE
    //  Reclaims memory by freeing up unreliable messages in outbound
    //  and inbound queues.
    void ReclaimMem();

    //PURPOSE
    //  This is only called when there is a fatal mem allocation error.
    //  We need to reclaim enough mem to allocate the OutOfMemory event to
    //  pass to the application.
    //  Reclaims memory by freeing up all messages in outbound/inbound
    //  queues.  After calling this all connections can be assumed to
    //  be out of sync and should be closed.
    void ReclaimAllMem();

    //PURPOSE
    //  Dumps contents of outbound/inbound queues to debug output.
    void DumpQueues();

	//PURPOSE
	//  Update collected stats for this connection.
	void UpdateStats();

protected:
	netEndpoint* m_EpOwner;

private:

    //PURPOSE
    //  Maximum number of frames ahead of our expected sequence which
    //  we can acknowledge.  We can acknowledge frames received within the
    //  receive window, thereby allowing the sender to remove them from the
    //  resend queue.
    static const netSequence RCV_WIN =
        netSequence(netBundle::BIT_SIZEOF_UO_ACK_BITS);

    void SetState(const State state);

    void QueueEvent(netEvent* e);

    bool QueueFrameEvent(const netFrame* f,
                        const netSequence seq);

    bool QueueOutOfOrder(const netFrame* f,
                        const netSequence seq,
						bool& wasDuplicate);

    //PURPOSE
    //  Inbound frame.
    struct InFrame : public netEventFrameReceived
    {
        InFrame(const netConnection* cxn,
				const netAddress& sender,
				const EndpointId epId,
                const void* payload,
                const netFrame* frame,
                const netSequence seq,
                sysMemAllocator* allocator)
            : netEventFrameReceived(sender,
									epId,
                                    cxn->GetId(),
                                    cxn->GetChannelId(),
                                    payload,
                                    frame->GetSizeofPayload(),
                                    seq,
                                    frame->GetSendFlags(),
                                    allocator)
            , m_SeqDep(frame->GetSeqDep())
            , m_IsReliable(frame->IsReliable())
        {
        }

		virtual ~InFrame()
		{

		}

        inmap_node<netSequence, InFrame> m_BySeqLink;

        netSequence m_SeqDep;
        bool m_IsReliable   : 1;
    };

	typedef netOutFrame OutFrame;

    //PURPOSE
    //  Predicate provided to map template declaration.  This predicate
    //  provides an ordering among sequence numbers.
    struct SeqCompare
    {
        bool operator()(const netSequence seq0, const netSequence seq1) const
        {
            return netSeqLt(seq0, seq1);
        }
    };

    typedef inlist<OutFrame, &OutFrame::m_ListLink> OutFrameQ;
    typedef inmap<netSequence, OutFrame, &OutFrame::m_BySeqLink, SeqCompare> OutFramesBySeq;
    typedef inmap<netSequence, InFrame, &InFrame::m_BySeqLink, SeqCompare> InFramesBySeq;

    void ProcessBundleHeader(const netBundle& bundle);

    //PURPOSE
    //  Returns the number of times this frame has timed out.
    unsigned GetTimeoutCount(const OutFrame* outfrm) const;

    void* Alloc(const unsigned size);

    void* AllocCritical(const unsigned size);

    InFrame* AllocInFrame(const netFrame* f,
                        const netSequence seq);

    OutFrame* AllocOutFrame(const unsigned sizeofPayload,
                            const unsigned sendFlags);

    void FreeOutFrame(OutFrame* outfrm);

    int m_Id;

    unsigned m_ChannelId;

    sysMemAllocator* m_Allocator;

    State m_State;

    //Time at which we last sent a keepalive.
    unsigned m_LastKeepaliveTime;

    //Time at which we last received a packet
    unsigned m_LastReceiveTime;

    //Number of timed out frames in the resend queue.
    int m_TimeoutCount;

	// The time at which the connection started to observe a reliable frame
	// RTT over the threshold specified, or 0 if not over the threshold.
	unsigned m_RttOverThresholdStartTime;

	// The time at which the connection most recently observed a reliable frame
	// RTT over the threshold specified, or 0 if m_RttOverThresholdStartTime is 0.
	unsigned m_RttOverThresholdLastTime;

	static unsigned sm_RttWarningThresholdMs;

	TimeoutReason m_TimeoutReason;

    netConnectionPolicies m_Policies;

    //Frames queued in re-send order.
    OutFrameQ m_ResendQ;

    //Newly queued frames.
    OutFrameQ m_NewQ;

    //Un-acked frames in sequence order.
    OutFramesBySeq m_Unacked;

    //Frames received out of order.  They're queued until
    //the gaps are filled.
    InFramesBySeq m_OoRcvd;

    netConnectionManager* m_CxnMgr;

    netEventQueue<netEvent>* m_EventQ;

    //Bits representing frames received out of sequence.
    //Each bit represents a sequence number relative to m_InboundSeq,
    //i.e. if m_InboundSeq is 13 bit zero in m_RcvdBits represents
    //frame 14.
    //Zero bits represent gaps in the sequence of frames we've received.
    u32 m_RcvdBits;

    //Next sequence number for outbound frames.
    netSequence m_OutboundSeq;

    //Next sequence number we expect for inbound frames.
    netSequence m_InboundSeq;

    netSequence m_LastReliableRcvd;
    netSequence m_LastReliableSent;

    bool m_Initialized : 1;
    //True if we need to send an ack.
    bool m_NeedToAck : 1;

    //True if a reliable frame hasn't been acked within the allowable period
    //or if we haven't received a keepalive in some time.
    bool m_IsTimedOut : 1;

#if __BANK
	bool m_IsForcedTimeOut : 1;
#endif

	//True if we have failed to allocate critical memory for this connection
	bool m_IsOutOfMemory : 1;

	bool m_RegisteredMainThreadHang : 1;
	bool m_WasMainThreadDead : 1;

#if !__NO_OUTPUT
	unsigned m_nMaxMainThreadDelta;
#endif

	unsigned m_IsOutOfMemoryRequiredBytes;
	unsigned m_NumFailedAllocs;

    netConnection(const netConnection&);
    netConnection& operator=(const netConnection&);
};

}   //namespace rage

#endif  //NET_CONNECTION_H
