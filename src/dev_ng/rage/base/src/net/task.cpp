// 
// net/task.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "task.h"
#include "diag/tracker.h"
#include "netdiag.h"
#include "time.h"
#include "status.h"
#include "system/memory.h"
#include "system/timer.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, task);
#undef __rage_channel
#define __rage_channel ragenet_task

//////////////////////////////////////////////////////////////////////////
//  netTaskNode
//////////////////////////////////////////////////////////////////////////
netTaskBase::netTaskBase()
{
    m_Next = m_Prev = this;
}

netTaskBase::~netTaskBase()
{
    netAssert(this == m_Next && this == m_Prev);
}

void
netTaskBase::QueueBefore(netTaskBase* tb)
{
    m_Prev->QueueAfter(tb);
}

void
netTaskBase::QueueAfter(netTaskBase* tb)
{
    tb->m_Next = m_Next;
    tb->m_Prev = this;
    tb->m_Next->m_Prev = tb->m_Prev->m_Next = tb;
}

void
netTaskBase::Unlink()
{
    m_Next->m_Prev = m_Prev;
    m_Prev->m_Next = m_Next;
    m_Next = m_Prev = this;
}

//////////////////////////////////////////////////////////////////////////
//  netTask
//////////////////////////////////////////////////////////////////////////
netTaskBase netTask::sm_Runnable;
static sysMemAllocator* s_NetTaskAllocator;
static netTimeStep s_NetTaskTimeStep;
static unsigned volatile s_NetTaskNextTaskId;
static bool s_NetTaskInitialized;

netTask::~netTask()
{
}

bool
netTask::Init(sysMemAllocator* allocator)
{
    if(netVerifyf(!s_NetTaskInitialized, "Already initialized"))
    {
        s_NetTaskTimeStep.Init(sysTimer::GetSystemMsTime());
        s_NetTaskAllocator = allocator;
        s_NetTaskInitialized = true;

        return true;
    }

    return false;
}

void
netTask::Shutdown()
{
    if(s_NetTaskInitialized)
    {
        netAssert(&sm_Runnable == sm_Runnable.m_Next);
        s_NetTaskAllocator = NULL;
        s_NetTaskInitialized = false;
    }
}

void
netTask::Destroy(netTask* task)
{
    if(task)
    {
        if(netVerify(task == task->m_Next && task == task->m_Prev))
        {
            task->OnCleanup();
            task->~netTask();
            Free(task);
        }
    }
}

bool
netTask::Schedule(netTask* task, const size_t queueId)
{
    if(netVerify(task == task->m_Next && task == task->m_Prev))
    {
        task->m_QueueId = queueId;
        task->m_TaskState = TASKSTATE_QUEUED;
        task->m_Status->SetPending();

        OUTPUT_ONLY(task->m_QueueTime = sysTimer::GetSystemMsTime());

        netTaskDebugPtr(task, "Scheduling...");

        //Find a task with the same queue ID
        netTaskBase* tb;
        for(tb = sm_Runnable.m_Next; &sm_Runnable != tb; tb = tb->m_Next)
        {
            if(((netTask*)tb)->m_QueueId == queueId)
            {
                //Found one.  The new task will be added to the end
                //of the queue.
                tb = &((netTask*)tb)->m_WaitQueue;
                break;
            }
        }

        //If we didn't find one tb will be pointing to sm_Runnable.

        //Calling QueueBefore on sm_Runnable or m_WaitQueue
        //will put the task at the end of the list.
		//@@: location NETTASK_SCHEDULE_QUEUE_BEFORE
        tb->QueueBefore(task);

        return true;
    }

    return false;
}

bool
netTask::Run(netTask* task)
{
    return Schedule(task, (size_t)task);
}

void
netTask::Update()
{
    s_NetTaskTimeStep.SetTime(sysTimer::GetSystemMsTime());

    for(netTaskBase* tb = sm_Runnable.m_Next; &sm_Runnable != tb; tb = tb->m_Next)
    {
        netTask* task = (netTask*)tb;

        if(TASKSTATE_QUEUED == task->m_TaskState)
        {
            OUTPUT_ONLY(task->m_StartTime = sysTimer::GetSystemMsTime());

            netTaskDebugPtr(task, "Starting... (%ums in queue)", task->m_StartTime - task->m_QueueTime);
            task->m_TaskState = TASKSTATE_RUNNING;
        }

		int resultCode = 0;
        const netTaskStatus status = task->OnUpdate(&resultCode);

        netAssert(NET_TASKSTATUS_NONE != status);

        bool deleteIt = true;

        switch(status)
        {
        case NET_TASKSTATUS_PENDING:
            deleteIt = false;
            break;

        case NET_TASKSTATUS_FAILED:

            netTaskDebugPtr(task, "FAILED%s (%ums executing, %ums in queue, %ums total), Status: 0x%p",
				task->WasCanceled() ? " (canceled)" : "",
				task->m_StartTime > 0 ? sysTimer::GetSystemMsTime() - task->m_StartTime : 0,
				task->m_StartTime > 0 ? task->m_StartTime - task->m_QueueTime : task->m_QueueTime,
				sysTimer::GetSystemMsTime() - task->m_QueueTime,
				task->m_Status);

            if(task->m_Status)
            {
				netTaskAssertfPtr(task, task->m_Status->Pending(), "Status not pending!");
				netTaskDebugPtr(task, "Setting status to failed");
				task->m_Status->SetFailed(resultCode);
            }
            break;

        case NET_TASKSTATUS_SUCCEEDED:

            netTaskDebugPtr(task, "SUCCEEDED%s (%ums executing, %ums in queue, %ums total), Status: 0x%p",
				task->WasCanceled() ? " (canceled)" : "",
				task->m_StartTime > 0 ? sysTimer::GetSystemMsTime() - task->m_StartTime : 0,
				task->m_StartTime > 0 ? task->m_StartTime - task->m_QueueTime : task->m_QueueTime,
				sysTimer::GetSystemMsTime() - task->m_QueueTime,
				task->m_Status);

            if(task->m_Status)
            {
				//@@: location NETTASK_UPDATE_SET_SUCCEEDED
				netTaskAssertfPtr(task, task->m_Status->Pending(), "Status not pending!");
				netTaskDebugPtr(task, "Setting status to succeeded");
				task->m_Status->SetSucceeded(resultCode);
            }
            break;

        case NET_TASKSTATUS_NONE:
        default:
            netAssert(false);
            break;
        }

        if(deleteIt)
        {
            tb = tb->m_Prev;

            netTaskDebugPtr(task, "Removing...");

            Remove(task);
            Destroy(task);
        }
    }
}

void 
netTask::UpdateTask(const netStatus* status)
{
	netTask* task = Find(&sm_Runnable, status);
	if(!task)
	{
		return;
	}

	if(TASKSTATE_QUEUED == task->m_TaskState)
	{
		OUTPUT_ONLY(task->m_StartTime = sysTimer::GetSystemMsTime());

		netTaskDebugPtr(task, "Starting... (%ums in queue)", task->m_StartTime - task->m_QueueTime);
		task->m_TaskState = TASKSTATE_RUNNING;
	}

	int resultCode = 0;
	const netTaskStatus taskStatus = task->OnUpdate(&resultCode);

	netAssert(NET_TASKSTATUS_NONE != taskStatus);

	switch(taskStatus)
	{
	case NET_TASKSTATUS_PENDING:
		break;

	case NET_TASKSTATUS_FAILED:
		netTaskDebugPtr(task, "FAILED%s (%ums executing, %ums in queue, %ums total), Status: 0x%p",
			task->WasCanceled() ? " (canceled)" : "",
			task->m_StartTime > 0 ? sysTimer::GetSystemMsTime() - task->m_StartTime : 0,
			task->m_StartTime > 0 ? task->m_StartTime - task->m_QueueTime : task->m_QueueTime,
			sysTimer::GetSystemMsTime() - task->m_QueueTime,
			task->m_Status);

		if(task->m_Status)
		{
			netTaskAssertfPtr(task, task->m_Status->Pending(), "Status not pending!");
			netTaskDebugPtr(task, "Setting status to failed");
			task->m_Status->SetFailed(resultCode);
		}
		break;

	case NET_TASKSTATUS_SUCCEEDED:
		netTaskDebugPtr(task, "SUCCEEDED%s (%ums executing, %ums in queue, %ums total), Status: 0x%p",
			task->WasCanceled() ? " (canceled)" : "",
			task->m_StartTime > 0 ? sysTimer::GetSystemMsTime() - task->m_StartTime : 0,
			task->m_StartTime > 0 ? task->m_StartTime - task->m_QueueTime : task->m_QueueTime,
			sysTimer::GetSystemMsTime() - task->m_QueueTime,
			task->m_Status);

		if(task->m_Status)
		{
			netTaskAssertfPtr(task, task->m_Status->Pending(), "Status not pending!");
			netTaskDebugPtr(task, "Setting status to succeeded");
			task->m_Status->SetSucceeded(resultCode);
		}
		break;

	case NET_TASKSTATUS_NONE:
	default:
		netAssert(false);
		break;
	}
}

bool 
netTask::HasTask(const netStatus* status)
{
	netTask* task = Find(&sm_Runnable, status);
	return task != NULL;
}

void
netTask::Cancel(const netStatus* status)
{
    netTask* task = Find(&sm_Runnable, status);
    if(netVerifyf(task, "Can't cancel non-existent task"))
    {
        Cancel(task);
    }
}

void
netTask::Cancel(const unsigned taskId)
{
    netTask* task = Find(&sm_Runnable, taskId);
    if(netVerifyf(task, "Can't cancel non-existent task"))
    {
        Cancel(task);
    }
}

void 
netTask::CancelAll()
{
	// Iterate through tasks, and cancel each one individually
	netTaskBase* tb;
	for(tb = sm_Runnable.m_Next; &sm_Runnable != tb; tb = tb->m_Next)
	{
		netTask* nt = (netTask*)tb;
		if (!nt->WasCanceled())
		{
			netTask::Cancel(nt);
		}
	}
}

unsigned
netTask::GetTimestamp()
{
    return s_NetTaskTimeStep.GetTimeStep();
}

unsigned
netTask::GetCurrentTime()
{
    return s_NetTaskTimeStep.GetCurrent();
}

//protected:

netTask::netTask()
: m_QueueId(0)
, m_TaskId(NextId())
, m_TaskState(TASKSTATE_NONE)
, m_Status(NULL)
, m_WasCanceled(false)
{
#if !__NO_OUTPUT
    m_QueueTime = m_StartTime = 0;
#endif
}

const char*
netTask::GetTaskName() const
{
    return "UNNAMED_TASK";
}

size_t
netTask::GetTaskQueueId() const
{
    return m_QueueId;
}

unsigned
netTask::GetTaskId() const
{
    return m_TaskId;
}

const diagChannel*
netTask::GetDiagChannel() const
{
#if !__NO_OUTPUT
    return &Channel_ragenet_task;
#else
    return NULL;
#endif
}

bool
netTask::WasCanceled() const
{
    return m_WasCanceled;
}

void
netTask::OnCancel()
{
    netTaskWarning("OnCancel was not implemented!!!");
    m_OnCancelOverridden = false;
}

netTaskStatus
netTask::OnUpdate(int* /* resultCode */)
{
    netTaskWarning("OnUpdate was not implemented!!!");
    return NET_TASKSTATUS_NONE;
}

void
netTask::OnCleanup()
{
}

//private:

void*
netTask::Allocate(const size_t size)
{
    if(netVerify(s_NetTaskAllocator))
    {
        RAGE_TRACK(ragenet);RAGE_TRACK(netTask);
        return s_NetTaskAllocator->LoggedAllocate(size, 0, 0, __FILE__, __LINE__);
    }

    return NULL;
}

void
netTask::Free(void* mem)
{
    if(netVerify(s_NetTaskAllocator))
    {
        s_NetTaskAllocator->Free(mem);
    }
}

unsigned
netTask::NextId()
{
    return sysInterlockedIncrement(&s_NetTaskNextTaskId) - 1;
}

netTask*
netTask::Find(netTaskBase* head, const netStatus* status)
{
    netTask* found = NULL;

    for(netTaskBase* tb = head->m_Next; head != tb && !found; tb = tb->m_Next)
    {
        netTask* task = (netTask*)tb;

        if(status == task->m_Status)
        {
            found = task;
        }
        else
        {
            found = Find(&task->m_WaitQueue, status);
        }
    }

    return found;
}

netTask*
netTask::Find(netTaskBase* head, const unsigned taskId)
{
    netTask* found = NULL;

    for(netTaskBase* tb = head->m_Next; head != tb && !found; tb = tb->m_Next)
    {
        netTask* task = (netTask*)tb;

        if(task->GetTaskId() == taskId)
        {
            found = task;
        }
        else
        {
            found = Find(&task->m_WaitQueue, taskId);
        }
    }

    return found;
}

void
netTask::Cancel(netTask* task)
{
    netTaskDebugPtr(task, "Canceling...");

    if(netVerifyf(!task->WasCanceled(), "Task '%s' already canceled", task->GetTaskName()))
    {
        //The default implementation will set m_OnCancelOverridden to false.
        task->m_OnCancelOverridden = true;
        task->OnCancel();

		netTaskDebugPtr(task, "CANCELLED (%ums executing, %ums in queue, %ums total), Status: %p",
			task->m_StartTime > 0 ? sysTimer::GetSystemMsTime() - task->m_StartTime : 0,
			task->m_StartTime > 0 ? task->m_StartTime - task->m_QueueTime : task->m_QueueTime,
			sysTimer::GetSystemMsTime() - task->m_QueueTime,
			task->m_Status);

        if(netVerifyf(task->m_OnCancelOverridden, "OnCancel was not implemented on '%s'", task->GetTaskName()))
        {
			if(task->m_Status)
			{
				netTaskDebugPtr(task, "Setting status to cancelled");
				task->m_Status->SetCanceled();
				task->m_Status = nullptr;
			}

            task->m_WasCanceled = true;
        }
    }
}


void
netTask::Remove(netTask* task)
{
    //Remove the task from its wait queue.
    
    //The first task in the removed task's wait queue is
    //promoted to the next higher wait level,
    //the highest level being the list of running tasks.

    //This is the next task on the same queue.
    netTaskBase* wqnext = task->m_WaitQueue.m_Next;

	//@@: location NETTASK_REMOVE
    if(&task->m_WaitQueue != wqnext)
    {
        //The next next task
        netTaskBase* wqnext2 = wqnext->m_Next;

        //Unlink from the wait queue.
        wqnext->Unlink();

        //Promote it on the next higher wait queue.
        task->QueueAfter(wqnext);

        //Move the wait queue from the task being removed
        //to the task that was just promoted.
        if(&task->m_WaitQueue != wqnext2)
        {
            task->m_WaitQueue.Unlink();
            wqnext2->QueueAfter(&((netTask*)wqnext)->m_WaitQueue);
        }
    }

    //Remove the task from it's wait queue.
    task->Unlink();
}

}   //namespace rage
