// 
// net/netsocketmanager.h
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_SOCKET_MANAGER_H
#define NET_SOCKET_MANAGER_H

#include "atl/delegate.h"
#include "atl/inmap.h"
#include "net/event.h"
#include "net/net.h"
#include "net/netaddress.h"
#include "net/netsocket.h"
#include "net/status.h"
#include "net/time.h"
#include "system/ipc.h"
#include "system/criticalsection.h"

#define SKT_MGR_ALLOW_SYMMETRIC_SOCKETS (1)

namespace rage
{

class sysMemAllocator;

enum netSocketEventType
{
    NET_SOCKET_EVENT_INVALID  = -1,
	NET_SOCKET_EVENT_PACKET_RECEIVED,
	NET_SOCKET_EVENT_RECEIVE_THREAD_TICKED,
    NET_SOCKET_EVENT_NUM_EVENTS
};

class netSocketEvent : public netEventBase
{
public:

    netSocketEvent(netSocketEventType eventType,
                    sysMemAllocator* allocator)
    : netEventBase(allocator)
    , m_EventType(eventType)
    {
    }

    netSocketEventType m_EventType;
};

class netSocketEventPacketReceived : public netSocketEvent
{
public:

    netSocketEventPacketReceived(const netAddress& sender,
                                const void* payload,
                                const unsigned len,
                                sysMemAllocator* allocator)
        : netSocketEvent(NET_SOCKET_EVENT_PACKET_RECEIVED, allocator)
        , m_Sender(sender)
        , m_Payload(payload)
        , m_Length(len)
    {
    }

    netAddress m_Sender;
    const void* m_Payload;
    unsigned m_Length;
};

class netSocketEventReceiveThreadTicked : public netSocketEvent
{
public:

	netSocketEventReceiveThreadTicked(sysMemAllocator* allocator)
		: netSocketEvent(NET_SOCKET_EVENT_RECEIVE_THREAD_TICKED, allocator)
	{
	}
};

//PURPOSE
//  Manages sending/receiving data to/from:
//  - the presence server,
//  - all relay servers,
//  - some p2p endpoints.
//  Data to/from all of the above endpoints are muxed over the same socket.
//
//  It also provides a mechanism to create and manage symmetric sockets.
//  A symmetric socket is a socket that is used to communicate with a single
//  remote endpoint and is not shared, unlike the main socket which is used
//  to communicate with multiple endpoints. Once a symmetric socket is 
//  associated with a destination address, packets sent/received to/from that
//	address will use the associated socket.
//
//  Some p2p endpoints use a symmetric socket instead of the muxed socket.
//
//  Background:
//  Most types of NATs/networks benefit from using a shared socket for all
//  NAT traversals. However, some NATs/networks don't work well with a shared
//  socket approach. For example, a port-preserving symmetric NAT will use
//  the same public port as the socket's private port for the *first* endpoint
//	it sends to (and only the first endpoint) so the mapped port is predictable,
//  but only for the first endpoint to which it sends. For these types of NATs,
//  we attempt to establish a connection over a unique socket, which is only
//  used to connect two peers together, and does not combine traffic to/from
//  other peers. This allows, for example, two port-preserving symmetric NATs
//  to make a direct connection. Some non-symmetric, but non-deterministic
//  NATs also benefit from this separate socket technique.
class netSocketManager
{
    typedef atDelegator<void (const netSocketEvent& event)> Delegator;

public:

	enum netSocketManagerSource
	{
		SOCKET_SOURCE_MAIN_SOCKET,
#if	SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
		SOCKET_SOURCE_SYMMETRIC_SOCKET,
#endif
	};

    typedef Delegator::Delegate Delegate;

	netSocketManager();
	~netSocketManager();

    //PURPOSE
    //  Initializes netSocketManager
    //PARAMS
    //  allocator       - Used to allocate memory for storing packets
    //                    prior to dispatching them to interested delegates.
    //  cpuAffinity     - CPU on which to run the background thread.
	//  mainSocketPort	- the port to use for the main socket.
    bool Init(sysMemAllocator* allocator,
					const int cpuAffinity,
					const u16 mainSocketPort);

    //PURPOSE
    //  Shuts down netSocketManager
    void Shutdown();

    //PURPOSE
    //  Returns true if Init() has been called successfully.
    bool IsInitialized() const;

	//PURPOSE
	//  Returns true if the main socket is ready to send and receive.
	//  False usually means the socket has not been bound to a port or
	//  the hardware isn't available.
	bool CanSendReceive() const;

	// PURPOSE
	// Add Bank Widgets
	void AddWidgets();

    //PURPOSE
    //  Same as AddDelegate() except that the delegate is assumed to be thread
    //  safe and will likely be called from a background thread.
    bool AddConcurrentDelegate(netSocketEventType eventType,
                                        Delegate* dlgt);

    //PURPOSE
    //  Unregisters a delegate that was registered with AddConcurrentDelegate().
    bool RemoveConcurrentDelegate(netSocketEventType eventType,
                                        Delegate* dlgt);

    //PURPOSE
    //  Call at least once per frame to process packets.
    void Update();

	//PURPOSE
	//  Returns the main socket over which packets are sent and received.
	netSocket* GetMainSocket();

#if	SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
	//PURPOSE
	//  Creates a new symmetric socket.
	//PARAMS
	//  port		- Local port in host byte order to which socket will be bound.
	//					Pass zero to bind to a random port.
	//  socketId	- [out] Returns the id of the newly created socket.
	//  status		- Optional. Can be polled for completion.
	//NOTES
	//  A symmetric socket is a socket that is used to communicate with a single
	//  remote endpoint and is not shared, unlike the main socket which is used
	//  to communicate with multiple endpoints. Once a symmetric socket is 
	//  associated with a destination address, packets sent/received to/from that
	//	address will use the new socket.
	//  A symmetric socket will time out if nothing is sent to it for a specified
	//  amount of time. The socket is automatically destroyed if it times out.
	bool CreateSymmetricSocket(const u16 port, unsigned* socketId, netStatus* status);

	//PURPOSE
	//  Associates a symmetric socket with a given destination address. All further
	//  packets to/from that address are sent/received through the associated socket.
	bool AssociateSymmetricSocket(const unsigned socketId, const netSocketAddress& addr);

	//PURPOSE
	//  Destroys a symmetric socket by socketId.
	//NOTES
	//  This is safe to call even if the creation process is still pending.
	void DestroySymmetricSocket(const unsigned socketId);

	//PURPOSE
	//  Destroys a symmetric socket by associated address..
	//NOTES
	//  This is safe to call even if the creation process is still pending.
	void DestroySymmetricSocket(const netSocketAddress& addr);

	//PURPOSE
	//  Returns the socket by sockteId.
	//	NOTE: The socket can be destroyed at any time. This should only be used during
	//  NAT traversal to discover the NAT's mapped port of the socket.
	netSocket* GetSymmetricSocket(const unsigned socketId);

	//PURPOSE
	//  Returns the network address of the socket, which includes an IP address
	//  and a port number.
	const netSocketAddress& GetSymmetricSocketAddress(const unsigned socketId);

	//PURPOSE
	//  Returns true if the specified address is associated with a symmetric socket.
	bool IsUsingSymmetricSocket(const netSocketAddress& addr);

#endif

	//PURPOSE
	//  Returns the port the game requested for the socket. Note: this
	//  can be different than the port to which the socket was bound.
	u16 GetRequestedPort() const;

	//PURPOSE
	//  Returns the thread id of receive thread
	sysIpcCurrentThreadId GetReceiveThreadId() const;

	//PURPOSE
	//  Returns the number of times a socket's send buffer was full when
	//  attempting to send a packet. This gives one measure of how often 
	//  the player's network capacity is exceeded.
	unsigned GetNumSocketSendBufferOverflows() const;

	//PURPOSE
	//  Same as GetNumSocketSendBufferOverflows() but resets the count to
	//  0 after returning the current count.
	unsigned ResetNumSocketSendBufferOverflows();

	//PURPOSE
	//  Returns the size in bytes of the endpoint socket's receive buffer and the number of bytes queued.
	bool GetReceiveBufferInfo(const netAddress& addr, unsigned& rcvBufQueueLen, unsigned& rcvBufSize) const;

    //PURPOSE
    //  Sends a datagram to the specified destination address.
    //  Returns true on success, or false if there's an error.
    //PARAMS
    //  destination     - Destination address
    //  data            - Buffer to send.  Must be <= MAX_BUFFER_SIZE bytes.
    //  length          - Number of bytes in the buffer.
    //                    Must be <= MAX_BUFFER_SIZE bytes.
    //  sktErr          - Optional - receives error code on error.
    bool Send(const netSocketAddress& destination,
               const void *data,
               const unsigned length,
               netSocketError* sktErr = 0);

private:

#if	SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
	class Endpoint
	{
	public:
	
		static const unsigned DEFAULT_TIMEOUT_MS = 1 * 60 * 1000;

		Endpoint();
		~Endpoint();

		void Clear();
		void Update();
		unsigned GetId() const;
		const netSocketAddress& GetAddr() const;
		netSocket* GetSocket();
		const netSocket* GetSocket() const;
		netStatus* GetStatus();
		void SetStatus(netStatus* status);

		void SetTimeout(const int timeoutMs);
		void ResetTimeout();
		bool IsTimedOut() const;

		void SetPendingDestroy();
		bool IsPendingDestroy() const;

		inmap_node<unsigned, Endpoint> m_ByIdLink;
		inmap_node<netSocketAddress, Endpoint> m_ByAddrLink;

	private:
		netSocket m_Skt;
		netTimeout m_Timeout;
		netStatus* m_Status;
		bool m_IsPendingDestroy;
	};

	static const unsigned MAX_ENDPOINTS = 64;
	typedef inmap<unsigned, Endpoint, &Endpoint::m_ByIdLink> EndpointsById;
	typedef inmap<netSocketAddress, Endpoint, &Endpoint::m_ByAddrLink> EndpointsByAddr;

	netSocketManager::Endpoint* GetEndpointById(const unsigned socketId);
	netSocketManager::Endpoint* GetEndpointByAddr(const netSocketAddress& addr);
	const netSocketManager::Endpoint* GetEndpointByAddr(const netSocketAddress& addr) const;
	void DestroyEndpoint(Endpoint* endpoint);
#endif

	enum State
	{
		STATE_CREATE_SOCKET,
		STATE_RUNNING,
	};

	void Clear();
	void DestroySockets();
    void ShutdownThreads();
    static void ProcessSockets(void*);

#if !__NO_OUTPUT
	const char* GetStateString(const State state);
#endif

	void SetState(const State state);

	void OnReceive(const netSocketManagerSource sourceSocket,
						const netSocketAddress& from,
						void* pktBuf,
						const int len);

	void OnReceiveNonRelayPacket(const netSocketAddress& from,
								const void* pktBuf,
								const int len);

	static const unsigned RECEIVE_THREAD_STACK_SIZE = 64 * 1024;
	static const unsigned MAIN_SOCKET_SEND_BUFFER_SIZE_BYTES = 16 * 1024;
	static const unsigned MAIN_SOCKET_RCV_BUFFER_SIZE_BYTES = 64 * 1024;

	static const unsigned CREATE_SOCKET_MIN_RETRY_INTERVAL_SEC = 30;
	static const unsigned CREATE_SOCKET_MAX_RETRY_INTERVAL_SEC = 90;

	bool m_Initialized;
	sysMemAllocator* m_Allocator;

	u16 m_RequestedPort;
	mutable sysCriticalSectionToken m_MainCs;
	sysCriticalSectionToken m_ConcurrentDlgtCs;

	sysIpcThreadId m_RcvThreadHandle;
	sysIpcCurrentThreadId m_RcvThreadId;
	sysIpcSema m_RcvThreadSema;
	bool m_RcvThreadExit;

	State m_State;
	netSocket m_MainSkt;
	netRetryTimer m_CreateSocketRetryTimer;

#if	SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
	static const unsigned SYMMETRIC_SOCKET_SEND_BUFFER_SIZE_BYTES = 8 * 1024;
	static const unsigned SYMMETRIC_SOCKET_RCV_BUFFER_SIZE_BYTES = 8 * 1024;
	static const unsigned SYMMETRIC_SOCKET_RCV_QUEUE_SIZE_BYTES = 8 * 1024;

	EndpointsById m_EndpointsById;
	EndpointsByAddr m_EndpointsByAddr;
#endif

	//Callbacks listening for receipt of packets.
    netSocketManager::Delegator m_ConcurrentDelegators[NET_SOCKET_EVENT_NUM_EVENTS]; //thread safe callbacks
};

} // namespace rage

#endif // NET_SOCKET_MANAGER_H
