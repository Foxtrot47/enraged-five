//
// net/keyexchange.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#include "net.h"
#include "atl/array.h"
#include "keyexchange.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "message.h"
#include "netdiag.h"
#include "profile/rocky.h"
#include "rline/rlpresence.h"
#include "system/nelem.h"
#include "system/threadpool.h"
#include "system/timemgr.h"
#include "system/xtl.h"

#include "wolfssl/wolfcrypt/sha256.h"
#include <time.h>

namespace rage
{

#define KX_TEST_PACKET_LOSS (0 && !__FINAL)

RAGE_DEFINE_SUBCHANNEL(ragenet, keyexchange)
#undef __rage_channel
#define __rage_channel ragenet_keyexchange

PARAM(netkxignoredirectmsgs, "for testing purposes only - ignore key exchange messages received via direct addresses");
PARAM(netkxignoreindirectmsgs, "for testing purposes only - ignore key exchange messages received via relay server addresses");

#if !__NO_OUTPUT
#define netKeyExchangeLogfHelper(logger,severity,fmt,...)\
	diagLogfHelper(Channel_ragenet_keyexchange, severity, "[%u] [KX:%u T:%u]: " fmt,\
	(logger) ? (logger)->GetTunnelRequestId() : 0, (logger) ? (logger)->GetSessionId() : 0, (logger) ? (logger)->GetElapsedTime() : 0, ##__VA_ARGS__);

//Use this version when calling from outside the session.
#define netKeyExchangeDebugPtr(logger, fmt, ...)     netKeyExchangeLogfHelper(logger,DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netKeyExchangeDebug1Ptr(logger, fmt, ...)    netKeyExchangeLogfHelper(logger,DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netKeyExchangeDebug2Ptr(logger, fmt, ...)    netKeyExchangeLogfHelper(logger,DIAG_SEVERITY_DEBUG2, fmt, ##__VA_ARGS__);
#define netKeyExchangeDebug3Ptr(logger, fmt, ...)    netKeyExchangeLogfHelper(logger,DIAG_SEVERITY_DEBUG3, fmt, ##__VA_ARGS__);
#define netKeyExchangeWarningPtr(logger, fmt, ...)   netKeyExchangeLogfHelper(logger,DIAG_SEVERITY_WARNING, fmt, ##__VA_ARGS__);
#define netKeyExchangeErrorPtr(logger, fmt, ...)     netKeyExchangeLogfHelper(logger,DIAG_SEVERITY_ERROR, fmt, ##__VA_ARGS__);

#else
#define netKeyExchangeDebugPtr(logger, fmt, ...)
#define netKeyExchangeDebug1Ptr(logger, fmt, ...)
#define netKeyExchangeDebug2Ptr(logger, fmt, ...)
#define netKeyExchangeDebug3Ptr(logger, fmt, ...)
#define netKeyExchangeWarningPtr(logger, fmt, ...)
#define netKeyExchangeErrorPtr(logger, fmt, ...)
#endif

//Use this version when calling from within the session (most common case).
#define netKeyExchangeDebug(fmt, ...)      netKeyExchangeDebugPtr(this->m_Logger,fmt,##__VA_ARGS__)
#define netKeyExchangeDebug1(fmt, ...)     netKeyExchangeDebug1Ptr(this->m_Logger,fmt,##__VA_ARGS__)
#define netKeyExchangeDebug2(fmt, ...)     netKeyExchangeDebug2Ptr(this->m_Logger,fmt,##__VA_ARGS__)
#define netKeyExchangeDebug3(fmt, ...)     netKeyExchangeDebug3Ptr(this->m_Logger,fmt,##__VA_ARGS__)
#define netKeyExchangeWarning(fmt, ...)    netKeyExchangeWarningPtr(this->m_Logger,fmt,##__VA_ARGS__)
#define netKeyExchangeError(fmt, ...)      netKeyExchangeErrorPtr(this->m_Logger,fmt,##__VA_ARGS__)

/*
	The maximum number of key exchange sessions we can have active simultaneously.
*/
static const unsigned KX_MAX_ACTIVE_SESSIONS = 64;

/*
	The maximum number of keys we can have active simultaneously.
*/
static const unsigned KX_MAX_ACTIVE_KEYS = 128;

/*
	Only offers are sent as a transaction.
*/
static const unsigned KX_MAX_ACTIVE_TRANSACTIONS_PER_SESSION = 1;

/*
	How often we resend offer packets per session.
*/
static const unsigned KX_TRANSACTION_RETRY_INTERVAL_MS = 500;

/*
	The maximum number of times to retry sending transaction messages.
*/
static const unsigned KX_MAX_RETRIES_PER_TRANSACTION = 9;

/*
	The maximum amount of time a session can take before canceling it.
*/
static const unsigned KX_MAX_SESSION_TME_MS = (8 * 1000);

/*
	Transactions wait for a response even after the transaction
	has used up all its retries.
*/
static const unsigned KX_MIN_TRANSACTION_TTL_MS = (8 * 1000);

/*
	Once processing has reached the Completed state, we wait for
	a few seconds to respond to the remote peer's messages.
*/
static const unsigned KX_TERMINATION_TIME_MS = (8 * 1000);

/*
	If we run out of keys, we can evict the least recently used
	key if it hasn't been use for at least this amount of time.
*/
static const unsigned KX_MIN_TIME_BEFORE_EVICTION_MS = (1 * 60 * 1000);

/*
	Unused key deletion occurs at this interval.
*/
static const unsigned KX_KEY_PURGE_INTERVAL_MS = (1 * 60 * 1000);

/*
	Unused keys expire after this amount of time.
*/
static const unsigned KX_KEY_EXPIRATION_TIME_MS = (2 * 60 * 1000);

/*
	The number of threads in the key exchange thread pool.
	Key exchange uses a separate thread pool, since the netThreadPool
	can be busy with long-running tasks. Key exchanges only have a
	few seconds to complete before failing a p2p connection.

*/
static const unsigned KX_THREAD_POOL_NUM_THREADS = 2;

static const unsigned KX_INVALID_TUNNEL_REQUEST_ID = 0;
static const unsigned KX_INVALID_SESSION_ID = 0;
static const unsigned KX_INVALID_TRANSACTION_ID = 0;

static netRandom s_KxRng;
unsigned netKeyExchange::sm_TransactionRetryIntervalMs = KX_TRANSACTION_RETRY_INTERVAL_MS;
unsigned netKeyExchange::sm_MaxRetriesPerTransaction = KX_MAX_RETRIES_PER_TRANSACTION;
unsigned netKeyExchange::sm_MaxSessionTimeMs = KX_MAX_SESSION_TME_MS;
bool netKeyExchange::sm_AllowOfferResendViaRelay = true;

enum FailReason
{
	// only add new reasons to the end, and don't re-order/renumber existing
	// reasons. These match values in our reporting system.
	FAIL_REASON_NONE = 0,
	FAIL_REASON_EMERGENCY_TIMER = 1,
	FAIL_REASON_COULD_NOT_SEND_OFFER = 2,
	FAIL_REASON_OFFER_TIMED_OUT = 3,
	FAIL_REASON_CANCELLED = 4,
	FAIL_REASON_START_AS_OFFERER = 5,
	FAIL_REASON_RECEIVE_OFFER = 6,
	FAIL_REASON_RCV_OFFER_SEND_ANSWER = 7,
	FAIL_REASON_START_AS_OFFERER_STATUS = 8,
	FAIL_REASON_START_AS_OFFERER_NET_ADDR = 9,
	FAIL_REASON_SIMULATED_FAIL = 10,
	FAIL_REASON_ASYNC_TASK_FAILED = 11,
	FAIL_REASON_COULD_NOT_START_KEY_AGREEMENT = 12,
	FAIL_REASON_GEN_P2P_KEY = 13,
	FAIL_REASON_GEN_RANDOM_KEY = 14,
	FAIL_REASON_ENCRYPT_KEY = 15,
	FAIL_REASON_DECRYPT_KEY = 16,
};

#if !__NO_OUTPUT
//////////////////////////////////////////////////////////////////////////
//  netKeyExchangeLogger
//////////////////////////////////////////////////////////////////////////
class netKeyExchangeLogger
{
public:

	netKeyExchangeLogger(netKeyExchangeLogger* logger)
	{
		this->Clear();

		if(logger)
		{
			m_TunnelRequestId = logger->m_TunnelRequestId;
			m_SessionId = logger->m_SessionId;
			m_StopWatch = logger->m_StopWatch;
		}
	}

	netKeyExchangeLogger(unsigned* tunnelRequestId, unsigned* sessionId, netStopWatch* stopWatch)
	{
		this->Clear();

		m_TunnelRequestId = tunnelRequestId;
		m_SessionId = sessionId;
		m_StopWatch = stopWatch;
	}

	void Clear()
	{
		m_TunnelRequestId = NULL;
		m_SessionId = NULL;
		m_StopWatch = NULL;
	}

	unsigned GetTunnelRequestId()
	{
		if(m_TunnelRequestId)
		{
			return *m_TunnelRequestId;
		}
		return 0;
	}

	unsigned GetSessionId()
	{
		if(m_SessionId)
		{
			return *m_SessionId;
		}
		return 0;
	}

	unsigned GetElapsedTime()
	{
		if(m_StopWatch)
		{
			return m_StopWatch->GetElapsedMilliseconds();
		}
		return 0;
	}

private:
	unsigned* m_TunnelRequestId;
	unsigned* m_SessionId;
	netStopWatch* m_StopWatch;
};
#else
class netKeyExchangeLogger
{
public:

	netKeyExchangeLogger(netKeyExchangeLogger* UNUSED_PARAM(logger))
	{
	}

	netKeyExchangeLogger(unsigned* UNUSED_PARAM(sessionId), netStopWatch* UNUSED_PARAM(stopWatch))
	{
	}
};
#endif

//////////////////////////////////////////////////////////////////////////
//  DiffieHellmanWorkItem
//////////////////////////////////////////////////////////////////////////

static sysThreadPool::Thread s_KxThreads[KX_THREAD_POOL_NUM_THREADS];
static sysThreadPool s_KxThreadPool;

// PURPOSE
//  Given the public key of the remote peer, generates the mutually-agreed key.
//  Since the key agreement function can take some time to compute, we move this
//  work to another thread.
class DiffieHellmanWorkItem : public sysThreadPool::WorkItem
{
public:
	static const unsigned MAX_ENCRYPTED_CEK_BUF_SIZE = netP2pCrypt::Key::KEY_LENGTH_IN_BYTES + MAX_SIZEOF_P2P_CRYPT_OVERHEAD;
	DiffieHellmanWorkItem()
	{
		Clear();
		m_FailReason = FAIL_REASON_NONE;
	}

	void Clear()
	{
		m_RemotePublicKey.Clear();
		m_KeyEncryptionKey.Clear();
		m_ContentEncryptionKey.Clear();

		sysMemSet(m_EncryptedCek, 0, sizeof(m_EncryptedCek));
		m_SizeOfEncryptedCek = 0;
		m_AcceptRemoteCek = false;
		
		// don't clear m_FailReason here, since we call Clear() on failure and
		// the caller needs the failReason afterwards.

		m_Logger = NULL;
	}

	bool Configure(netKeyExchangeLogger* logger, const netP2pCrypt::PublicKey& remotePublicKey)
	{
		rtry
		{
			rverify(remotePublicKey.IsValid(), catchall, );
			m_Logger = logger;
			m_RemotePublicKey = remotePublicKey;
			m_AcceptRemoteCek = false;
			m_FailReason = FAIL_REASON_NONE;
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	bool Configure(netKeyExchangeLogger* logger, const netP2pCrypt::PublicKey& remotePublicKey, const u8* encryptedCek, unsigned sizeOfEncryptedCek)
	{
		rtry
		{
			rverify(remotePublicKey.IsValid(), catchall, );
			rverify(encryptedCek, catchall, );
			rverify(sizeOfEncryptedCek > 0, catchall, );
			rverify(sizeOfEncryptedCek <= sizeof(m_EncryptedCek), catchall, );

			m_Logger = logger;
			m_RemotePublicKey = remotePublicKey;
			sysMemCpy(m_EncryptedCek, encryptedCek, sizeOfEncryptedCek);
			m_SizeOfEncryptedCek = sizeOfEncryptedCek;
			m_AcceptRemoteCek = true;
			m_FailReason = FAIL_REASON_NONE;
			return true;
		}
		rcatchall
		{
			return false;
		}
	}

	void GenerateCek()
	{
		PROFILE

		rtry
		{
			netKeyExchangeDebug("DiffieHellmanWorkItem::GenerateCek() starting.");

			rcheck(!WasCanceled(), taskcancelled, netKeyExchangeDebug("DiffieHellmanWorkItem was cancelled. Bailing."));

			// the Diffie-Hellman agreed secret key is only used as a key encryption key (KEK) for the actual content encryption key (CEK)
			rverify(netP2pCrypt::GenerateP2pKey(m_RemotePublicKey, m_KeyEncryptionKey), catchall, m_FailReason = FAIL_REASON_GEN_P2P_KEY);

			rcheck(!WasCanceled(), taskcancelled, netKeyExchangeDebug("DiffieHellmanWorkItem was cancelled. Bailing."));

			// generate a random CEK
			rverify(netP2pCrypt::GenerateRandomKey(m_ContentEncryptionKey), catchall, m_FailReason = FAIL_REASON_GEN_RANDOM_KEY);

			rcheck(!WasCanceled(), taskcancelled, netKeyExchangeDebug("DiffieHellmanWorkItem was cancelled. Bailing."));

			// encrypt the CEK with the KEK
			u8 rawCek[netP2pCrypt::Key::KEY_LENGTH_IN_BYTES];
			m_ContentEncryptionKey.GetRawKey(rawCek);

			m_SizeOfEncryptedCek = sizeof(m_EncryptedCek);
			rverify(netP2pCrypt::Encrypt(m_KeyEncryptionKey, rawCek, sizeof(rawCek), m_EncryptedCek, m_SizeOfEncryptedCek), catchall, m_FailReason = FAIL_REASON_ENCRYPT_KEY);

			netKeyExchangeDebug("DiffieHellmanWorkItem::GenerateCek() finished.");
		}
		rcatch(taskcancelled)
		{
			netKeyExchangeDebug("DiffieHellmanWorkItem::GenerateCek() cancelled.");
		}
		rcatchall
		{
			netKeyExchangeDebug("DiffieHellmanWorkItem::GenerateCek() failed.");

			Clear();
		}
	}

	void AcceptCek()
	{
		PROFILE

		rtry
		{
			netKeyExchangeDebug("DiffieHellmanWorkItem::AcceptCek() starting.");

			rcheck(!WasCanceled(), taskcancelled, netKeyExchangeDebug("DiffieHellmanWorkItem was cancelled. Bailing."));

			// the Diffie-Hellman agreed secret key is only used as a key encryption key (KEK) for the actual content encryption key (CEK)
			rverify(netP2pCrypt::GenerateP2pKey(m_RemotePublicKey, m_KeyEncryptionKey), catchall, m_FailReason = FAIL_REASON_GEN_P2P_KEY);

			rcheck(!WasCanceled(), taskcancelled, netKeyExchangeDebug("DiffieHellmanWorkItem was cancelled. Bailing."));

			// decrypt the CEK with the KEK
			u8* buf = m_EncryptedCek;
			unsigned sizeOfBuf = m_SizeOfEncryptedCek;
			rverify(netP2pCrypt::Decrypt(m_KeyEncryptionKey, &buf, sizeOfBuf), catchall, m_FailReason = FAIL_REASON_DECRYPT_KEY);
			rverify(sizeOfBuf == netP2pCrypt::Key::KEY_LENGTH_IN_BYTES, catchall,);
			m_ContentEncryptionKey.ResetKey(buf, sizeOfBuf);

			netKeyExchangeDebug("DiffieHellmanWorkItem::AcceptCek() finished.");
		}
		rcatch(taskcancelled)
		{
			netKeyExchangeDebug("DiffieHellmanWorkItem::AcceptCek() cancelled.");
		}
		rcatchall
		{
			netKeyExchangeDebug("DiffieHellmanWorkItem::AcceptCek() failed.");

			Clear();
		}
	}

	virtual void DoWork()
	{
		if(m_AcceptRemoteCek)
		{
			AcceptCek();
		}
		else
		{
			GenerateCek();
		}
	}
	
	const netP2pCrypt::Key& GetCek()
	{
		return m_ContentEncryptionKey;
	}

	const u8* GetEncryptedCek(unsigned& sizeOfEncryptedCek)
	{
		sizeOfEncryptedCek = m_SizeOfEncryptedCek;
		return m_EncryptedCek;
	}

	bool GetAcceptRemoteCek() const
	{
		return m_AcceptRemoteCek;
	}

	FailReason GetFailReason() const
	{
		return m_FailReason;
	}

private:
	netKeyExchangeLogger* m_Logger;
	netP2pCrypt::PublicKey m_RemotePublicKey;
	netP2pCrypt::Key m_KeyEncryptionKey;
	netP2pCrypt::Key m_ContentEncryptionKey;
	u8 m_EncryptedCek[MAX_ENCRYPTED_CEK_BUF_SIZE];
	unsigned m_SizeOfEncryptedCek;
	bool m_AcceptRemoteCek;
	FailReason m_FailReason;
};

//////////////////////////////////////////////////////////////////////////
//  netKeyExchangeSessionOffer
//////////////////////////////////////////////////////////////////////////
class netKeyExchangeSessionOffer
{
public:

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													sizeof(unsigned) +												// m_SourceSessionId
													sizeof(unsigned) +												// m_TransactionId
													netPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES +					// m_SourcePeerAddr
													sizeof(u32) +													// m_RoleTieBreaker
													1 +																// m_ConnectingToSessionHost
													netP2pCrypt::PublicKey::MAX_EXPORTED_SIZE_IN_BYTES;				// m_PublicKey

	NET_MESSAGE_DECL(netKeyExchangeSessionOffer, NET_KEY_EXCHANGE_SESSION_OFFER);

	netKeyExchangeSessionOffer()
	{

	}

	netKeyExchangeSessionOffer(const unsigned sourceSessionId,
	                           const unsigned transactionId,
	                           const netPeerAddress& sourcePeerAddr,
	                           const u32 roleTieBreaker,
	                           const bool connectingToSessionHost,
	                           const netP2pCrypt::PublicKey& publicKey)
	{
		netAssert(sourceSessionId != KX_INVALID_SESSION_ID);

		m_SourceSessionId = sourceSessionId;
		m_TransactionId = transactionId;
		m_SourcePeerAddr = sourcePeerAddr;
		m_RoleTieBreaker = roleTieBreaker;
		m_ConnectingToSessionHost = connectingToSessionHost;
		m_PublicKey = publicKey;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		bool success = false;

		if(bb.SerUns(msg.m_SourceSessionId, sizeof(msg.m_SourceSessionId) << 3)
		        && bb.SerUns(msg.m_TransactionId, sizeof(msg.m_TransactionId) << 3)
		        && bb.SerUser(msg.m_SourcePeerAddr)
		        && bb.SerUns(msg.m_RoleTieBreaker, sizeof(msg.m_RoleTieBreaker) << 3)
		        && bb.SerBool(msg.m_ConnectingToSessionHost)
		        && bb.SerUser(msg.m_PublicKey))
		{
			success = true;
		}

		return success;
	}

//private:
	unsigned m_SourceSessionId;
	unsigned m_TransactionId;
	netPeerAddress m_SourcePeerAddr;
	u32 m_RoleTieBreaker;
	bool m_ConnectingToSessionHost;
	netP2pCrypt::PublicKey m_PublicKey;
};

NET_MESSAGE_IMPL(netKeyExchangeSessionOffer);

//////////////////////////////////////////////////////////////////////////
//  netKeyExchangeSessionAnswer
//////////////////////////////////////////////////////////////////////////
class netKeyExchangeSessionAnswer
{
public:

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													sizeof(unsigned) +												// m_SourceSessionId
													sizeof(unsigned) +												// m_DestSessionId
													sizeof(unsigned) +												// m_TransactionId
													netPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES +					// m_SourcePeerAddr
													1 +																// m_SenderHasControllingRole
													netCrypto::DiffieHellman::MAX_PUBLIC_KEY_LENGTH_IN_BYTES +		// m_DhPublicKey
													DiffieHellmanWorkItem::MAX_ENCRYPTED_CEK_BUF_SIZE +				// m_EncryptedCek
													sizeof(unsigned);												// m_SizeOfEncryptedCek

	NET_MESSAGE_DECL(netKeyExchangeSessionAnswer, NET_KEY_EXCHANGE_SESSION_ANSWER);

	netKeyExchangeSessionAnswer()
	{

	}

	netKeyExchangeSessionAnswer(const unsigned sourceSessionId,
	                            const unsigned destSessionId,
	                            const unsigned transactionId,
	                            const netPeerAddress& peerAddr,
	                            const bool senderHasControllingRole,
								const netP2pCrypt::PublicKey& publicKey,
								const u8* encryptedCek,
								unsigned sizeOfEncryptedCek)
	{
		netAssert(sourceSessionId != KX_INVALID_SESSION_ID);
		netAssert(destSessionId != KX_INVALID_SESSION_ID);

		m_SourceSessionId = sourceSessionId;
		m_DestSessionId = destSessionId;
		m_TransactionId = transactionId;
		m_SourcePeerAddr = peerAddr;
		m_SourcePeerAddr.ClearKey();
		m_SenderHasControllingRole = senderHasControllingRole;
		m_PublicKey = publicKey;
		if(encryptedCek && sizeOfEncryptedCek)
		{
			sysMemCpy(m_EncryptedCek, encryptedCek, sizeOfEncryptedCek);
		}
		m_SizeOfEncryptedCek = sizeOfEncryptedCek;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		bool success = false;

		if(bb.SerUns(msg.m_SourceSessionId, sizeof(msg.m_SourceSessionId) << 3)
		        && bb.SerUns(msg.m_DestSessionId, sizeof(msg.m_DestSessionId) << 3)
		        && bb.SerUns(msg.m_TransactionId, sizeof(msg.m_TransactionId) << 3)
		        && bb.SerUser(msg.m_SourcePeerAddr)
		        && bb.SerBool(msg.m_SenderHasControllingRole)
				&& bb.SerUser(msg.m_PublicKey)
				&& bb.SerArraySize(msg.m_SizeOfEncryptedCek, msg.m_EncryptedCek ASSERT_ONLY(, "m_SizeOfEncryptedCek")))
		{
			success = true;

			if(msg.m_SizeOfEncryptedCek > 0)
			{
				success = bb.SerBytes(msg.m_EncryptedCek, msg.m_SizeOfEncryptedCek);
			}
		}

		return success;
	}

//private:
	unsigned m_SourceSessionId;
	unsigned m_DestSessionId;
	unsigned m_TransactionId;
	netPeerAddress m_SourcePeerAddr;
	bool m_SenderHasControllingRole;
	netP2pCrypt::PublicKey m_PublicKey;
	u8 m_EncryptedCek[DiffieHellmanWorkItem::MAX_ENCRYPTED_CEK_BUF_SIZE];
	unsigned m_SizeOfEncryptedCek;
};

NET_MESSAGE_IMPL(netKeyExchangeSessionAnswer);

//////////////////////////////////////////////////////////////////////////
//  netKeyExchangeTransaction
//////////////////////////////////////////////////////////////////////////
class netKeyExchangeTransaction
{
public:
	netKeyExchangeTransaction()
		: m_Logger(NULL)
	{
		this->Clear();
	}

	netKeyExchangeTransaction(netKeyExchangeLogger* logger,
	                          netConnectionManager* cxnMgr,
	                          const unsigned transactionId,
	                          const netP2pCrypt::Key& remotePeerKey,
	                          const netAddress& destAddr,
	                          unsigned maxRetries,
	                          unsigned retryDelay,
	                          const void* data,
	                          const unsigned sizeofData,
	                          const void* userData)
	{
		rtry
		{
			this->Clear();

			rverify(cxnMgr != NULL, catchall,);
			rverify(transactionId != KX_INVALID_TRANSACTION_ID, catchall,);
			rverify(destAddr.IsValid(), catchall,);
			rverify(maxRetries < 50, catchall,);
			rverify(retryDelay >= 20, catchall,);
			rverify(data != NULL, catchall,);
			rverify(sizeofData > 0, catchall,);
			rverify(sizeofData < sizeof(m_MsgBuf), catchall,);

			m_Logger = logger;
			m_CxnMgr = cxnMgr;
			m_TransactionId = transactionId;
			m_RemotePeerKey = remotePeerKey;
			m_DestAddr = destAddr;
			m_MaxRetries = maxRetries;
			m_RetryDelayMs = retryDelay;
			sysMemCpy(m_MsgBuf, data, sizeofData);
			m_SizeOfData = sizeofData;
			m_UserData = userData;
		}
		rcatchall
		{
			this->Clear();
		}
	}

	bool IsValid() const
	{
		return (m_CxnMgr != NULL) && (m_TransactionId != KX_INVALID_TRANSACTION_ID);
	}

	void Clear()
	{
		m_CxnMgr = NULL;
		m_RemotePeerKey.Clear();
		m_DestAddr.Clear();
		m_TransactionId = KX_INVALID_TRANSACTION_ID;
		m_MaxRetries = 0;
		m_RetryDelayMs = 0;
		memset(m_MsgBuf, 0, sizeof(m_MsgBuf));
		m_SizeOfData = 0;
		m_UserData = NULL;

		m_NumRetries = 0;
		m_ResendTimer = netRetryTimer();
		m_ExpirationTimer = netTimeout();
		m_StopWatchSinceStart = netStopWatch();
		m_IsCancelled = false;
		m_Logger = NULL;
	}

	~netKeyExchangeTransaction()
	{
	}

	const netAddress& GetDestAddr() const
	{
		return m_DestAddr;
	}

	unsigned GetTransactionId() const
	{
		return m_TransactionId;
	}

	unsigned GetMaxRetries() const
	{
		return m_MaxRetries;
	}

	const u8* GetMessageData() const
	{
		return m_MsgBuf;
	}

	unsigned GetSizeOfData() const
	{
		return m_SizeOfData;
	}

	const void* GetUserData() const
	{
		return m_UserData;
	}

	unsigned GetNumRetries() const
	{
		return m_NumRetries;
	}

	bool Start()
	{
		m_StopWatchSinceStart.Restart();
		return Send();
	}

	bool Send()
	{
		rtry
		{
			netKeyExchangeDebug2("Sending %s to:" NET_ADDR_FMT ". TransactionId:%u (attempt %u of %u)",
								netMessage::GetName(this->GetMessageData(), this->GetSizeOfData()),
			NET_ADDR_FOR_PRINTF(this->GetDestAddr()),
			m_TransactionId,
			m_NumRetries + 1,
			m_MaxRetries + 1);

			m_ResendTimer.InitMilliseconds(m_RetryDelayMs);

			if(m_ExpirationTimer.IsRunning() == false)
			{
				m_ExpirationTimer.InitMilliseconds(Max(m_RetryDelayMs * (m_MaxRetries + 1), KX_MIN_TRANSACTION_TTL_MS));
			}

#if KX_TEST_PACKET_LOSS
			if(((unsigned)s_KxRng.GetInt()) % 2 == 0)
			{
				return true;
			}
#endif

			netP2pCryptContext context;
			context.Init(m_RemotePeerKey, NET_P2P_CRYPT_TUNNELING_PACKET);

			rcheck(m_CxnMgr->IsInitialized(), catchall, netWarning("m_CxnMgr->IsInitialized() is false"));
			rverify(m_CxnMgr->SendTunnelingPacket(this->GetDestAddr(),
													this->GetMessageData(),
													this->GetSizeOfData(),
													context),
					catchall, netWarning("Sending message failed"));

			return true;
		}
		rcatchall
		{
		}

		return false;
	}

	/*
		7.2.1.4.
		Cancellation means that the agent
		will not retransmit the request, will not treat the lack of
		response to be a failure, but will wait the duration of the
		transaction timeout for a response
	*/
	void Cancel()
	{
		m_IsCancelled = true;
	}

	bool IsCancelled() const
	{
		return m_IsCancelled;
	}

	/*
		A timed out transaction is one that has sent all of its packets including retries.
		However, the transaction still remains active in the timed out state so we can
		process responses that may arrive late.
	*/
	bool IsTimedOut() const
	{
		// if we've sent our last retry and it would be time to retry again, we've timed out
		return (m_NumRetries >= m_MaxRetries) && m_ResendTimer.IsTimeToRetry();
	}

	/*
		An expired transaction is one that has sent all of its packets and waited its
		maximum duration for a response. Cancelled transactions can expire sooner.
	*/
	bool IsExpired() const
	{
		if(m_IsCancelled)
		{
			// a cancelled transaction won't send any more retries, so IsTimedOut() won't return true.
			return IsTimedOut() || m_ExpirationTimer.IsTimedOut() || (m_ExpirationTimer.IsRunning() == false);
		}

		// if we're timed out and not canceled then we're expired
		// (but let it hang around waiting for a response for a minimum duration in case the remote peer has stalled)
		return IsTimedOut() && (m_ExpirationTimer.IsTimedOut() || (m_ExpirationTimer.IsRunning() == false));
	}

	void Update()
	{
		m_ResendTimer.Update();
		m_ExpirationTimer.Update();

		if(m_IsCancelled == false)
		{
			if((m_NumRetries < m_MaxRetries) && m_ResendTimer.IsTimeToRetry())
			{
				++m_NumRetries;
				Send();
			}
		}
	}

	unsigned GetElapsedTimeSinceStart() const
	{
		return m_StopWatchSinceStart.GetElapsedMilliseconds();
	}

private:
	netConnectionManager* m_CxnMgr;
	netP2pCrypt::Key m_RemotePeerKey;
	netAddress m_DestAddr;
	unsigned m_TransactionId;
	unsigned m_MaxRetries;
	unsigned m_RetryDelayMs;
	netStopWatch m_StopWatchSinceStart;

	// could use netFrame::MAX_BYTE_SIZEOF_PAYLOAD here but since we
	// know our max message sizes, this saves a lot of memory
	u8 m_MsgBuf[netKeyExchangeSessionOffer::MAX_EXPORT_SIZE_IN_BYTES];

	unsigned m_SizeOfData;
	const void* m_UserData;

	unsigned m_NumRetries;
	netTimeout m_ExpirationTimer;
	netRetryTimer m_ResendTimer;
	netKeyExchangeLogger* m_Logger;
	bool m_IsCancelled : 1;
};

template<unsigned MAX_TRANSACTIONS>
class netKeyExchangeTransactions
{
public:

	netKeyExchangeTransactions(netKeyExchangeLogger* logger)
		: m_Logger(logger)
	{
		Clear();
	}

	void Clear()
	{
		m_Transactions.Reset();
	}

	netKeyExchangeTransaction* AddTransaction(const netKeyExchangeTransaction& transaction)
	{
		if(transaction.IsValid() && netVerify(m_Transactions.IsFull() == false))
		{
			m_Transactions.Push(transaction);
			return &m_Transactions.Top();
		}
		return NULL;
	}

	unsigned GetCount() const
	{
		return m_Transactions.GetCount();
	}

	const netKeyExchangeTransaction& operator[](unsigned index) const
	{
		return m_Transactions[index];
	}

	const netKeyExchangeTransaction* FindByTransactionId(unsigned id) const
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetTransactionId() == id)
			{
				return &m_Transactions[i];
			}
		}

		return NULL;
	}

	void RemoveTransaction(unsigned transactionId)
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetTransactionId() == transactionId)
			{
				m_Transactions.Delete(i);
			}
		}
	}

	void RemoveTransactionsByUserData(void* userData)
	{
		// fixed array doesn't like us removing elements while we're iterating

		bool deleted = false;
		do
		{
			deleted = false;
			for(unsigned i = 0; i < GetCount(); ++i)
			{
				if(m_Transactions[i].GetUserData() == userData)
				{
					deleted = true;
					m_Transactions.Delete(i);
					break;
				}
			}
		} while(deleted);
	}

	void CancelTransactionsByUserData(void* userData)
	{
		// Note: there can be more than one active transaction with the same user data

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				m_Transactions[i].Cancel();
			}
		}
	}

	void CancelAll()
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			m_Transactions[i].Cancel();
		}
	}

	bool DoesTransactionExist(void* userData)
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				return true;
			}
		}

		return false;
	}

	/*
		returns true if at least one transaction exists with this user data and
		all transactions with this user data are timed out (does not include
		transactions that were canceled and have expired)
	*/
	bool AreAllTransactionsTimedOut(void* userData)
	{
		// Note: there can be more than one active transaction with the same user data

		bool timedOut = true;
		bool exists = false;

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				exists = true;

				if(m_Transactions[i].IsTimedOut() == false)
				{
					timedOut = false;
					break;
				}
			}
		}

		return exists && timedOut;
	}

	/*
		returns true if at least one transaction exists with this user data and
		all transactions with this user data are expired.
	*/
	bool AreAllTransactionsExpired(void* userData)
	{
		// Note: there can be more than one active transaction with the same user data

		bool expired = true;
		bool exists = false;

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				exists = true;

				if(m_Transactions[i].IsExpired() == false)
				{
					expired = false;
					break;
				}
			}
		}

		return exists && expired;
	}

	/*
		returns true if at least one transaction exists with this user data and
		all transactions with this user data are expired but not cancelled.
	*/
	bool AreAllTransactionsExpiredButNotCancelled(void* userData)
	{
		// Note: there can be more than one active transaction with the same user data

		bool expiredButNotCancelled = true;
		bool exists = false;

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				exists = true;

				/*
					Expired		Cancelled	ExpiredButNotCancelled
						  0				0						 0
						  0				1						 0
						  1				0						 1
						  1				1						 0
				*/

				if((m_Transactions[i].IsExpired() == false) || (m_Transactions[i].IsCancelled()))
				{
					expiredButNotCancelled = false;
					break;
				}
			}
		}

		return exists && expiredButNotCancelled;
	}

	void RemoveExpiredTransactions()
	{
		// fixed array doesn't like us removing elements while we're iterating

		bool deleted = false;
		do
		{
			deleted = false;
			for(unsigned i = 0; i < GetCount(); ++i)
			{
				const netKeyExchangeTransaction& t = m_Transactions[i];
				if(t.IsExpired())
				{
					netKeyExchangeDebug("Transaction %u expired after %u ms. Cancelled: %s, TimedOut: %s",
					                    t.GetTransactionId(),
					                    t.GetElapsedTimeSinceStart(),
					                    t.IsCancelled() ? "true" : "false",
					                    t.IsTimedOut() ? "true" : "false");

					deleted = true;
					m_Transactions.Delete(i);
					break;
				}
			}
		}
		while(deleted);
	}

	void Update()
	{
		for(unsigned i = 0; i < (unsigned)m_Transactions.GetCount(); ++i)
		{
			m_Transactions[i].Update();
		}
	}

private:
	atFixedArray<netKeyExchangeTransaction, MAX_TRANSACTIONS> m_Transactions;
	netKeyExchangeLogger* m_Logger;
};

class netKeyExchangeTransactor
{
public:
	netKeyExchangeTransactor(netKeyExchangeLogger* logger)
		: m_Transactions(logger)
		, m_Logger(logger)
	{
		this->Clear();
	}

	void Clear()
	{
		m_CxnMgr = NULL;
		m_Transactions.Clear();
	}

	bool Init(netConnectionManager* cxnMgr)
	{
		this->Clear();
		m_CxnMgr = cxnMgr;
		return netVerify(m_CxnMgr != NULL);
	}

	void Update()
	{
		m_Transactions.Update();
	}

	template<typename T>
	bool SendRequest(const unsigned transactionId,
	                 const netP2pCrypt::Key& remotePeerKey,
	                 const netAddress& addr,
	                 const T& msg,
	                 const unsigned maxRetries,
	                 const unsigned retryDelay,
	                 const void* userData)
	{
		u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
		unsigned size;
		return msg.Export(buf, sizeof(buf), &size)
		       && this->SendRequest(transactionId, remotePeerKey, addr, buf, size, maxRetries, retryDelay, userData);
	}

	unsigned NextId()
	{
		unsigned id = sysInterlockedIncrement(&s_NextId);

		while(KX_INVALID_TRANSACTION_ID == id)
		{
			id = sysInterlockedIncrement(&s_NextId);
		}

		return id;
	}

	const netKeyExchangeTransaction* FindTransactionById(unsigned transactionId)
	{
		return m_Transactions.FindByTransactionId(transactionId);
	}

	void RemoveTransaction(unsigned transactionId)
	{
		m_Transactions.RemoveTransaction(transactionId);
	}

	void RemoveTransactionsByUserData(void* userData)
	{
		m_Transactions.RemoveTransactionsByUserData(userData);
	}

	void CancelTransactionsByUserData(void* userData)
	{
		m_Transactions.CancelTransactionsByUserData(userData);
	}

	void CancelAll()
	{
		m_Transactions.CancelAll();
	}

	bool AreAllTransactionsTimedOut(void* userData)
	{
		return m_Transactions.AreAllTransactionsTimedOut(userData);
	}

	bool AreAllTransactionsExpired(void* userData)
	{
		return m_Transactions.AreAllTransactionsExpired(userData);
	}

	bool DoesTransactionExist(void* userData)
	{
		return m_Transactions.DoesTransactionExist(userData);
	}

	bool AreAllTransactionsExpiredButNotCancelled(void* userData)
	{
		return m_Transactions.AreAllTransactionsExpiredButNotCancelled(userData);
	}

	void RemoveExpiredTransactions()
	{
		m_Transactions.RemoveExpiredTransactions();
	}

private:
	static unsigned s_NextId;

	bool SendRequest(const unsigned transactionId,
	                 const netP2pCrypt::Key& remotePeerKey,
	                 const netAddress& addr,
	                 const void* data,
	                 const unsigned sizeofData,
	                 const unsigned maxRetries,
	                 const unsigned retryDelay,
	                 const void* userData)
	{
		rtry
		{
			netKeyExchangeTransaction* tx = m_Transactions.AddTransaction(netKeyExchangeTransaction(m_Logger,
																			m_CxnMgr,
																			transactionId,
																			remotePeerKey,
																			addr,
																			maxRetries,
																			retryDelay,
																			data,
																			sizeofData,
																			userData));

			rverify(tx != NULL, catchall,);
			rverify(tx->Start(), catchall,);

			return true;
		}
		rcatchall
		{
		}

		return false;
	}

	netConnectionManager* m_CxnMgr;
	netKeyExchangeTransactions<KX_MAX_ACTIVE_TRANSACTIONS_PER_SESSION> m_Transactions;
	netKeyExchangeLogger* m_Logger;
};

// We send our transaction id in messages so the remote peer can send it back
// to us to correlate which transaction the response is for.
unsigned netKeyExchangeTransactor::s_NextId = KX_INVALID_TRANSACTION_ID;

//////////////////////////////////////////////////////////////////////////
//  netKeyExchangeSession
//////////////////////////////////////////////////////////////////////////
class netKeyExchangeSession
{
public:
	netKeyExchangeSession();
	~netKeyExchangeSession();

	bool Init(const unsigned tunnelRequestId,
				const unsigned sessionId,
				netConnectionManager* cxnMgr,
				const netPeerAddress& peerAddr,
				const netAddress& addr, 
				const netAddress& relayAddr);
	bool StartKeyAgreement(const netKeyExchangeSessionOffer* msg);
	bool StartKeyAgreement(const netKeyExchangeSessionAnswer* msg);
	void UpdateKeyAgreement();
	void Update();
	void Clear();
	int FindFreeNetStatusIndex();
	bool Attach(const unsigned tunnelRequestId, netStatus* status);
	bool StartAsOfferer(const netAddress& addr, const bool connectingToSessionHost, netStatus* status);
	bool CanReceiveOffer(const netKeyExchangeSessionOffer* msg);
	bool ReceiveOffer(const netKeyExchangeSessionOffer* msg, const netAddress& sender);
	void ReceiveAnswer(const netKeyExchangeSessionAnswer* msg, const netAddress& sender);

	//PURPOSE
	//  Returns true if this session is pending.
	bool Pending() const;

	//PURPOSE
	//  Returns true if this session is in use.
	bool IsActive() const;

	//PURPOSE
	//  Returns true if this session is complete
	//  and it successfully obtained a valid netAddress for the remote
	//  peer.
	bool Succeeded() const;

	//PURPOSE
	//  Cancels the given status object if it is attached to the current session.
	void SetCanceled(netStatus* status);

	//PURPOSE
	//  Cancels all status objects that are attached to the current session.
	void SetCanceled();

	//PURPOSE
	//  Returns the session ID.
	unsigned GetSessionId() const;

	//PURPOSE
	//  Returns the session ID of the remote peer.
	unsigned GetDestSessionId() const;

	//PURPOSE
	//  Returns the peer address of the remote peer.
	const netPeerAddress& GetRemotePeerAddress()
	{
		netAssert(m_RemotePeerAddr.IsValid());
		return m_RemotePeerAddr;
	}

	//PURPOSE
	//  Returns the address of the remote peer.
	const netAddress& GetRemoteAddress()
	{
		netAssert(m_RemoteAddr.IsValid());
		return m_RemoteAddr;
	}

	//PURPOSE
	//  Returns true if the given status object is attached to this session.
	bool IsStatusAttached(netStatus* status) const
	{
		if(status == NULL)
		{
			return false;
		}

		for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
		{
			if(m_Status[i] == status)
			{
				return true;
			}
		}

		return false;
	}

	inlist_node<netKeyExchangeSession> m_ListLink;

private:

	// to handle multiple simultaneous key exchanges with the same peer,
	// have the key exchange session accept multiple status objects instead
	// of starting new sessions for each.
	const static unsigned KX_MAX_ATTACHED_REQUESTS = 10;

	enum AgentRole
	{
		ROLE_INVALID,

		/*
			Controlling Agent: The agent that is responsible for resolving any
			decision making conflicts. In any session, one agent is always
			controlling. The other is the controlled agent.
		*/
		ROLE_CONTROLLING,

		/*
			Controlled Agent: An agent that waits for the controlling agent
			to resolve any decision making conflicts.
		*/
		ROLE_CONTROLLED,
	};

	enum CompletionType
	{
		COMPLETE_FAILED,
		COMPLETE_SUCCEEDED,
	};

	bool IsControlling();
	bool IsControlled();
	bool SendOffer();
	void ResolveRoleConflict(const netKeyExchangeSessionOffer* msg, const netAddress& sender);
	bool SendAnswer(unsigned sourceSessionId, unsigned destSessionId, unsigned transactionId, const u8* encryptedCek, unsigned sizeOfEncryptedCek, const netAddress& sender);

	unsigned GetElapsedTime()
	{
		return m_StopWatch.GetElapsedMilliseconds();
	}

#if !__NO_OUTPUT
	const char* FailReasonToString(const FailReason failReason) const;
#endif

	void Complete(const CompletionType completionType, const FailReason failReason);

	enum SessionState
	{
		STATE_INACTIVE = -1,
		STATE_PENDING,
		STATE_SUCCEEDED,
		STATE_FAILED
	};

	enum OfferStatus
	{
		OFFER_STATUS_UNATTEMPTED = 0,
		OFFER_STATUS_FAILED_TO_SEND = 1,
		OFFER_STATUS_SENT = 2,
		OFFER_STATUS_SEND_TIMED_OUT = 3,
	};

	OUTPUT_ONLY(unsigned m_TunnelRequestId);
	unsigned m_SessionId;
	unsigned m_DestSessionId;
	unsigned m_RemoteTransactionId;
	AgentRole m_Role;
	u32 m_RoleTieBreaker;
	unsigned m_OfferTransactionToken;
	netConnectionManager* m_CxnMgr;
	netAddress m_RemoteAddr;
	netAddress m_RemoteRelayAddr;
	netPeerAddress m_LocalPeerAddr;
	netPeerAddress m_RemotePeerAddr;
	netAddress m_NetAddr;
	u8 m_NumOffersSent;
	u8 m_NumOffersReceived;
	u8 m_NumAnswersSent;
	u8 m_NumAnswersReceived;
	bool m_HaveRemotePublicKey : 1;
	bool m_HaveLocalPublicKey : 1;
	bool m_HaveAgreeKey : 1;
	bool m_ConnectingToSessionHost : 1;
	bool m_LocalPeerIsHost : 1;
	bool m_HadRoleConflict : 1;
	bool m_Canceled : 1;

	DiffieHellmanWorkItem m_WorkItem;

	netStopWatch m_StopWatch;
	netTimeout m_TerminationTimer;

	netKeyExchangeTransactor m_Transactor;
	netKeyExchangeLogger m_LoggerInstance;
	netKeyExchangeLogger* m_Logger;
	SessionState m_State;
	netStatus* m_Status[KX_MAX_ATTACHED_REQUESTS];
};

netKeyExchangeSession::netKeyExchangeSession()
	: m_LoggerInstance(OUTPUT_ONLY(&m_TunnelRequestId, )&m_SessionId, &m_StopWatch)
	, m_Logger(&m_LoggerInstance)
	, m_Transactor(&m_LoggerInstance)
	, m_State(STATE_INACTIVE)
{
	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		m_Status[i] = NULL;
	}

	this->Clear();
}

netKeyExchangeSession::~netKeyExchangeSession()
{
	this->Clear();
}

bool
netKeyExchangeSession::Pending() const
{
	return (STATE_PENDING == m_State);
}

bool
netKeyExchangeSession::IsActive() const
{
	return (STATE_INACTIVE != m_State);
}

bool
netKeyExchangeSession::Succeeded() const
{
	return (STATE_SUCCEEDED == m_State);
}

void
netKeyExchangeSession::SetCanceled()
{
	netKeyExchangeDebug3("SetCanceled :: Marking this key exchange session as canceled. "
	                     "Canceled key exchange sessions continue to completion to avoid failures on the remote peer.");

	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		if(m_Status[i])
		{
			m_Status[i]->SetCanceled();
		}

		m_Status[i] = NULL;
	}

	m_Canceled = true;
}

void
netKeyExchangeSession::SetCanceled(netStatus* status)
{
	if(status == NULL)
	{
		return;
	}

	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		if(m_Status[i] == status)
		{
			netKeyExchangeDebug3("SetCanceled :: Detaching");

			m_Status[i]->SetCanceled();
			m_Status[i] = NULL;
		}
	}
}

unsigned
netKeyExchangeSession::GetSessionId() const
{
	return m_SessionId;
}

unsigned
netKeyExchangeSession::GetDestSessionId() const
{
	return m_DestSessionId;
}

bool 
netKeyExchangeSession::StartKeyAgreement(const netKeyExchangeSessionOffer* msg)
{
	rtry
	{
		netKeyExchangeDebug3("Starting key agreement from offer.");

		rverify(!m_WorkItem.Pending(), catchall, netKeyExchangeError("DiffieHellmanWorkItem is already pending"));
		rverify(m_WorkItem.Configure(m_Logger, msg->m_PublicKey), catchall, netKeyExchangeError("DiffieHellmanWorkItem::Configure failed"));
		rverify(s_KxThreadPool.QueueWork(&m_WorkItem), catchall, netKeyExchangeError("DiffieHellmanWorkItem failed to queue"));

		netKeyExchangeDebug3("DiffieHellmanWorkItem queued with id %u. "
							 "Thread pool: num threads: %" SIZETFMT "u, "
							 "num worked items: queued: %" SIZETFMT "u, active: %" SIZETFMT "u.",
							 m_WorkItem.GetId(),
							 s_KxThreadPool.NumThreads(),
							 s_KxThreadPool.NumQueuedWorkItems(),
							 s_KxThreadPool.NumActiveWorkItems());

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netKeyExchangeSession::StartKeyAgreement(const netKeyExchangeSessionAnswer* msg)
{
	rtry
	{
		netKeyExchangeDebug3("Starting key agreement from answer.");

		rverify(!m_WorkItem.Pending(), catchall, netKeyExchangeError("DiffieHellmanWorkItem is already pending"));
		rverify(m_WorkItem.Configure(m_Logger, msg->m_PublicKey, msg->m_EncryptedCek, msg->m_SizeOfEncryptedCek), catchall, netKeyExchangeError("DiffieHellmanWorkItem::Configure failed"));
		rverify(s_KxThreadPool.QueueWork(&m_WorkItem), catchall, netKeyExchangeError("DiffieHellmanWorkItem failed to queue"));

		netKeyExchangeDebug3("DiffieHellmanWorkItem queued with id %u. "
							 "Thread pool: num threads: %" SIZETFMT "u, "
							 "num worked items: queued: %" SIZETFMT "u, active: %" SIZETFMT "u.",
							 m_WorkItem.GetId(),
							 s_KxThreadPool.NumThreads(),
							 s_KxThreadPool.NumQueuedWorkItems(),
							 s_KxThreadPool.NumActiveWorkItems());

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
netKeyExchangeSession::UpdateKeyAgreement()
{
	if(this->Pending())
	{
		if(m_WorkItem.Finished() || m_WorkItem.WasCanceled())
		{
			netKeyExchangeDebug3("DiffieHellmanWorkItem %s.", m_WorkItem.Finished() ? "finished" : "canceled");
			const netP2pCrypt::Key& p2pKey = m_WorkItem.GetCek();
			if(p2pKey.IsValid())
			{
				netKeyExchange::OnKeyReceived(m_RemotePeerAddr.GetPeerId(), m_RemoteAddr, m_RemoteRelayAddr, p2pKey);

				if(m_WorkItem.GetAcceptRemoteCek() == false)
				{
					unsigned sizeOfEncryptedCek = 0;
					const u8* encryptedCek = m_WorkItem.GetEncryptedCek(sizeOfEncryptedCek);

					SendAnswer(GetSessionId(), GetDestSessionId(), m_RemoteTransactionId, encryptedCek, sizeOfEncryptedCek, m_RemoteAddr);
				}

				Complete(COMPLETE_SUCCEEDED, FAIL_REASON_NONE);
			}
			else
			{
				const FailReason failReason = m_WorkItem.GetFailReason();
				if(failReason != FAIL_REASON_NONE)
				{
					Complete(COMPLETE_FAILED, failReason);
				}
				else
				{
					Complete(COMPLETE_FAILED, FAIL_REASON_ASYNC_TASK_FAILED);
				}
			}
		}
		else
		{
#if !__NO_OUTPUT
			if(m_WorkItem.Pending())
			{
				netKeyExchangeDebug3("DiffieHellmanWorkItem state: %s.", m_WorkItem.Queued() ? "queued" : m_WorkItem.Active() ? "active" : "unknown");
			}
#endif
		}
	}
}

void
netKeyExchangeSession::Update()
{
	UpdateKeyAgreement();

	m_Transactor.Update();

	if(m_Transactor.AreAllTransactionsExpiredButNotCancelled(&m_OfferTransactionToken))
	{
		if(Pending())
		{
			netKeyExchangeDebug1("Offer transaction timed out.");
			Complete(COMPLETE_FAILED, FAIL_REASON_OFFER_TIMED_OUT);
		}
	}

	m_Transactor.RemoveExpiredTransactions();

	m_TerminationTimer.Update();
	if(m_TerminationTimer.IsTimedOut())
	{
		// we're done
		if(Pending())
		{
			netKeyExchangeDebug("Emergency termination timer has expired, key exchange session finished.");
			Complete(COMPLETE_FAILED, FAIL_REASON_EMERGENCY_TIMER);
		}
		else
		{
			netKeyExchangeDebug("Termination timer has expired, key exchange session finished.");
			if(!m_WorkItem.Pending())
			{
				Clear();
			}
		}
	}

	// wait for the work item to finish before clearing
	if(!Pending() && !Succeeded())
	{
		if(m_WorkItem.Pending())
		{
			netKeyExchangeDebug2("Waiting for the work item to complete before clearing. Current state: %s",
								m_WorkItem.Queued() ? "queued" : m_WorkItem.Active() ? "active" : "unknown");
			s_KxThreadPool.CancelWork(m_WorkItem.GetId());
		}
		else
		{
			Clear();
		}
	}
}

void
netKeyExchangeSession::ReceiveAnswer(const netKeyExchangeSessionAnswer* msg, const netAddress& sender)
{
	m_NumAnswersReceived++;
	
	netKeyExchangeDebug("Received answer from " NET_ADDR_FMT ". TransactionId:%u, RemoteSessionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId, msg->m_SourceSessionId);
			
	const netAddress& srcRelayAddr = msg->m_SourcePeerAddr.GetRelayAddress();
	netDebug2("Sender's relay address from netKeyExchangeSessionAnswer: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(srcRelayAddr));

	if(srcRelayAddr.IsValid())
	{
		netAssert(srcRelayAddr.GetProxyAddress().IsValid());
		netAssert(srcRelayAddr.GetRelayToken().IsValid());
		netAssert(srcRelayAddr.GetTargetAddress().IsValid());
	}

	netAddress associatedAddr;
	netAddress relayAddr;
	if(sender.IsRelayServerAddr())
	{
		netAssert(srcRelayAddr.IsRelayServerAddr());

		netAssert(srcRelayAddr.GetProxyAddress() == sender.GetProxyAddress());

		if(sender.GetRelayToken().IsValid())
		{
			if(netVerify(srcRelayAddr.GetProxyAddress() == sender.GetProxyAddress()) &&
				netVerify(srcRelayAddr.GetRelayToken() == sender.GetRelayToken()) &&
				netVerify(srcRelayAddr.GetTargetAddress().IsValid()))
			{
				// the key should be associated with non-tokenized relay address only
				associatedAddr.Init(srcRelayAddr.GetProxyAddress(), netRelayToken::INVALID_RELAY_TOKEN, srcRelayAddr.GetTargetAddress());
			}
		}
		else
		{
			netAssert(srcRelayAddr.GetTargetAddress() == sender.GetTargetAddress());
			associatedAddr = sender;
		}

		relayAddr = associatedAddr;
	}
	else
	{
		associatedAddr = sender;
		relayAddr.Init(srcRelayAddr.GetProxyAddress(), netRelayToken::INVALID_RELAY_TOKEN, srcRelayAddr.GetTargetAddress());
	}

	if(!associatedAddr.IsValid())
	{
		return;
	}

	m_RemoteRelayAddr = relayAddr;
	if(m_RemoteAddr.IsRelayServerAddr())
	{
		m_RemoteAddr = relayAddr;
	}

	// find the transaction id that sent the request that generated this response
	const netKeyExchangeTransaction* transaction = m_Transactor.FindTransactionById(msg->m_TransactionId);
	if(transaction == NULL)
	{
		netKeyExchangeDebug3("Ignoring answer since we don't have a transaction with id %u. "
		                     "We might have already processed an answer for this transaction and "
		                     "the peer sent an answer to an offer retry, or the transaction might "
		                     "have timed out.", msg->m_TransactionId);
		return;
	}

	// retrieve the request that generated this response
	netKeyExchangeSessionOffer offer;
	if(!netVerify(offer.Import(transaction->GetMessageData(), transaction->GetSizeOfData())))
	{
		netKeyExchangeDebug3("Ignoring answer since importing the associated offer failed.");
		m_Transactor.RemoveTransaction(msg->m_TransactionId);
		return;
	}

	//netAssert(m_DestSessionId == KX_INVALID_SESSION_ID || (m_DestSessionId == msg->m_SourceSessionId));
	m_DestSessionId = msg->m_SourceSessionId;
	netAssert(m_DestSessionId != KX_INVALID_SESSION_ID);

	m_Transactor.RemoveTransaction(msg->m_TransactionId);

	// cancel any other offer transactions we have in progress
	m_Transactor.CancelTransactionsByUserData(&m_OfferTransactionToken);

	if(msg->m_SenderHasControllingRole)
	{
		netKeyExchangeDebug3("Sender is telling us he received our offer but he has the controlling role.");
	}
	else if(!m_HaveRemotePublicKey && !m_WorkItem.Pending())
	{
		m_HaveRemotePublicKey = true;
		if(!StartKeyAgreement(msg))
		{
			Complete(COMPLETE_FAILED, FAIL_REASON_COULD_NOT_START_KEY_AGREEMENT);
		}
	}
	else
	{
		netKeyExchangeDebug3("Answer is for a session already in progress");
	}
}

bool
netKeyExchangeSession::SendAnswer(unsigned sourceSessionId, unsigned destSessionId, unsigned transactionId, const u8* encryptedCek, unsigned sizeOfEncryptedCek, const netAddress& sender)
{
	rtry
	{
		const netP2pCrypt::PublicKey& publicKey = netP2pCrypt::GetLocalPublicKey();

		netKeyExchangeSessionAnswer msg(sourceSessionId, destSessionId, transactionId, m_LocalPeerAddr, m_Role == ROLE_CONTROLLING, publicKey, encryptedCek, sizeOfEncryptedCek);

		netKeyExchangeDebug("Sending answer to:" NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(sender), transactionId);

#if KX_TEST_PACKET_LOSS
		if(((unsigned)s_KxRng.GetInt()) % 2 == 0)
		{
			return true;
		}
#endif

		rcheck(m_CxnMgr->IsInitialized(), catchall, netWarning("m_CxnMgr->IsInitialized() is false"));

		netP2pCryptContext context;
		context.Init(m_RemotePeerAddr.GetPeerKey(), NET_P2P_CRYPT_TUNNELING_PACKET);

		rverify(m_CxnMgr->SendTunnelingPacket(sender,
												msg,
												context),
				catchall,);

		m_NumAnswersSent++;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netKeyExchangeSession::CanReceiveOffer(const netKeyExchangeSessionOffer* msg)
{
	// We need to determine whether this offer is from the same remote peer with whom we're negotiating.
	bool sameRemotePeer = m_RemotePeerAddr.IsValid() &&	(msg->m_SourcePeerAddr.GetPeerId() == m_RemotePeerAddr.GetPeerId());

#if !__NO_OUTPUT
	if(sameRemotePeer && ((m_DestSessionId != KX_INVALID_SESSION_ID) && (m_DestSessionId != msg->m_SourceSessionId)))
	{
		netKeyExchangeDebug("The remote peer is exchanging multiple keys with us simultaneously. Will fork a new session.");
	}
#endif

	return sameRemotePeer && ((m_DestSessionId == KX_INVALID_SESSION_ID) || (m_DestSessionId == msg->m_SourceSessionId));
}

void
netKeyExchangeSession::ResolveRoleConflict(const netKeyExchangeSessionOffer* msg, const netAddress& UNUSED_PARAM(sender))
{
	if(m_Role == ROLE_CONTROLLING)
	{
		m_HadRoleConflict = true;
		netKeyExchangeDebug3("We started as the offerer and we received an offer from our remote peer. Resolving role conflict..."
		                     "My tie breaker:0x%08x, their tie breaker:0x%08x", m_RoleTieBreaker, msg->m_RoleTieBreaker);

		if(m_RoleTieBreaker < msg->m_RoleTieBreaker)
		{
			netKeyExchangeDebug3("Switching to the controlled role.");
			m_Role = ROLE_CONTROLLED;
		}
		else if(m_RoleTieBreaker == msg->m_RoleTieBreaker)
		{
			// in the off chance both sides chose the same random tie breaker
			netKeyExchangeDebug3("We both chose the same random tie breaker. Resolving...");

			if(m_LocalPeerAddr.GetPeerId() < m_RemotePeerAddr.GetPeerId())
			{
				netKeyExchangeDebug3("Switching to the controlled role");
				m_Role = ROLE_CONTROLLED;
			}
			else
			{
				netKeyExchangeDebug3("Staying in the controlling role");
			}
		}
		else
		{
			netKeyExchangeDebug3("Staying in the controlling role");
		}
	}
	else
	{
		if(m_Role == ROLE_INVALID)
		{
			netKeyExchangeDebug3("Assuming the controlled role.");
		}
		else
		{
			netKeyExchangeDebug3("Remaining in the controlled role.");
		}

		m_Role = ROLE_CONTROLLED;
	}
}

bool
netKeyExchangeSession::ReceiveOffer(const netKeyExchangeSessionOffer* msg, const netAddress& sender)
{
	FailReason failReason = FAIL_REASON_RECEIVE_OFFER;

	rtry
	{
		m_NumOffersReceived++;

		netKeyExchangeDebug("Received offer from " NET_ADDR_FMT ". RemoteTransactionId:%u. RemoteSessionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId, msg->m_SourceSessionId);

		// by this point, the sender should be a plaintext address (not a tokenized relay address)
		rverifyall(sender.IsValid());
		rverifyall(!sender.IsRelayServerAddr() || (sender.GetTargetAddress().IsValid() && !sender.GetRelayToken().IsValid()));

		bool remoteAddrChanged = false;
		if(sender.IsRelayServerAddr())
		{
			m_RemoteRelayAddr = sender;
			if(m_RemoteAddr.IsRelayServerAddr() && (m_RemoteAddr != sender))
			{
				if(!m_RemoteAddr.GetRelayToken().IsValid())
				{
					remoteAddrChanged = true;
				}

				m_RemoteAddr = sender;
			}
			else if(netKeyExchange::sm_AllowOfferResendViaRelay && m_RemoteAddr.IsDirectAddr() && (m_RemoteAddr != sender))
			{
				netKeyExchangeDebug("Received offer via relay server while remote address was direct");
				remoteAddrChanged = true;
				m_RemoteAddr = sender;
			}
		}

		// remote peer tells us if he thinks we're the host of a multiplayer session
		m_LocalPeerIsHost = msg->m_ConnectingToSessionHost;

		if(m_LocalPeerIsHost)
		{
			netKeyExchangeDebug2("The remote peer is telling us we are the host of a multiplayer session");
		}

		/*
			We need to decide who should have the controlling role.
		*/
		ResolveRoleConflict(msg, sender);

		// netAssert(m_DestSessionId == KX_INVALID_SESSION_ID || (m_DestSessionId == msg->m_SourceSessionId));
		m_DestSessionId = msg->m_SourceSessionId;
		netAssert(m_DestSessionId != KX_INVALID_SESSION_ID);

		if(m_Role == ROLE_CONTROLLED)
		{
			// cancel any offer transactions we have in progress
			m_Transactor.CancelTransactionsByUserData(&m_OfferTransactionToken);

			if(!m_HaveRemotePublicKey && !m_WorkItem.Pending())
			{
				m_HaveRemotePublicKey = true;
				m_RemoteTransactionId = msg->m_TransactionId;
				rverify(StartKeyAgreement(msg), catchall, failReason = FAIL_REASON_COULD_NOT_START_KEY_AGREEMENT);
			}
			else
			{
				netKeyExchangeDebug3("Offer is for a session already in progress");

				if((Pending() || Succeeded()) && !m_WorkItem.Pending())
				{
					unsigned sizeOfEncryptedCek = 0;
					const u8* encryptedCek = m_WorkItem.GetEncryptedCek(sizeOfEncryptedCek);

					rverify(SendAnswer(GetSessionId(), GetDestSessionId(), msg->m_TransactionId, encryptedCek, sizeOfEncryptedCek, sender), catchall, );
				}
			}
		}
		else
		{
			rverify(SendAnswer(GetSessionId(), GetDestSessionId(), msg->m_TransactionId, NULL, 0, sender), catchall, );

			if(remoteAddrChanged)
			{
				if(m_Transactor.DoesTransactionExist(&m_OfferTransactionToken))
				{
					netKeyExchangeDebug("Remote address changed. Removing existing offer transactions and restarting with new remote address.");

					m_Transactor.RemoveTransactionsByUserData(&m_OfferTransactionToken);

					rverify(SendOffer(), catchall, netKeyExchangeError("Failed to start new offer transaction after remote address changed."));
				}
			}
		}

		return true;
	}
	rcatchall
	{
		Complete(COMPLETE_FAILED, failReason);
		return false;
	}
}

bool
netKeyExchangeSession::SendOffer()
{
	rtry
	{
		const netP2pCrypt::PublicKey& publicKey = netP2pCrypt::GetLocalPublicKey();

		unsigned txId = m_Transactor.NextId();
		netKeyExchangeSessionOffer msg(GetSessionId(), txId, m_LocalPeerAddr, m_RoleTieBreaker, m_ConnectingToSessionHost, publicKey);
		rverify(m_Transactor.SendRequest(txId, m_RemotePeerAddr.GetPeerKey(), m_RemoteAddr, msg, netKeyExchange::sm_MaxRetriesPerTransaction, netKeyExchange::sm_TransactionRetryIntervalMs, &m_OfferTransactionToken), catchall,);

		m_NumOffersSent++;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netKeyExchangeSession::StartAsOfferer(const netAddress& addr,
                                      const bool connectingToSessionHost,
                                      netStatus* status)
{
	FailReason failReason = FAIL_REASON_START_AS_OFFERER;
	rtry
	{
		rverify(status, catchall, failReason = FAIL_REASON_START_AS_OFFERER_STATUS);
		status->SetPending();

		rverify(addr.IsValid(), catchall, failReason = FAIL_REASON_START_AS_OFFERER_NET_ADDR);

		netAssert(m_Role == ROLE_INVALID);
		m_Role = ROLE_CONTROLLING;
		netKeyExchangeDebug3("We're the offerer so we have the controlling role.");

		m_ConnectingToSessionHost = connectingToSessionHost;

		if(m_ConnectingToSessionHost)
		{
			netKeyExchangeDebug3("We're exchanging keys with the host of a multiplayer session.");
		}
		else
		{
			netKeyExchangeDebug3("We're exchanging keys with a non-host peer.");
		}

		int index = FindFreeNetStatusIndex();
		rverify(((index >= 0) && (index < KX_MAX_ATTACHED_REQUESTS)), catchall,
		netKeyExchangeDebug("Can't start session as offerer since all status objects are in use."));

		rverify((m_Status[index] == NULL), catchall,);

		m_Status[index] = status;

		rverify(SendOffer(), catchall, );

		return true;
	}
	rcatchall
	{
		Complete(COMPLETE_FAILED, failReason);
		return false;
	}
}

int
netKeyExchangeSession::FindFreeNetStatusIndex()
{
	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		if(m_Status[i] == NULL)
		{
			return i;
		}
	}

	return -1;
}

bool
netKeyExchangeSession::Attach(const unsigned OUTPUT_ONLY(tunnelRequestId), netStatus* status)
{
	rtry
	{
#if !__NO_OUTPUT
		if(m_TunnelRequestId == KX_INVALID_TUNNEL_REQUEST_ID)
		{
			m_TunnelRequestId = tunnelRequestId;
		}
#endif

		netKeyExchangeDebug("ExchangeKeys was called while we were already exchanging keys with "
							 "with this remote peer. Attaching tunnel request %u to active session.", tunnelRequestId);

		rverify(status, catchall,);

		if(Pending())
		{
			netKeyExchangeDebug("The active session is still in the pending state. Will wait for it to complete.");

			int index = FindFreeNetStatusIndex();
			rverify(((index >= 0) && (index < KX_MAX_ATTACHED_REQUESTS)), catchall,
				netKeyExchangeDebug("Can't attach because max status objects are already assigned to this session."));

			rverify((m_Status[index] == NULL), catchall,);

			status->SetPending();
			m_Status[index] = status;
		}
		else
		{
			// don't attach if we don't have a key for this peer - it might have been deleted while the session was still active

			netP2pCrypt::Key key;
			rcheck(netKeyExchange::GetP2pKey(this->GetRemotePeerAddress().GetPeerId(), key), catchall,
					netKeyExchangeDebug("Not attaching to existing session since we don't know about this peer"));

			if(Succeeded())
			{
				netKeyExchangeDebug("The active session has already succeeded.");
				status->ForceSucceeded();
			}
			else
			{
				netKeyExchangeDebug("The active session has already failed.");
				status->ForceFailed();
			}
		}

		return true;
	}
	rcatchall
	{
		netKeyExchangeDebug("Failed to attach to active session. Will start a new session.");
		return false;
	}
}

bool
netKeyExchangeSession::IsControlling()
{
	netAssert(m_Role == ROLE_CONTROLLING ||
	          m_Role == ROLE_CONTROLLED);

	return m_Role == ROLE_CONTROLLING;
}

bool
netKeyExchangeSession::IsControlled()
{
	netAssert(m_Role == ROLE_CONTROLLING ||
	          m_Role == ROLE_CONTROLLED);

	return m_Role == ROLE_CONTROLLED;
}

void
netKeyExchangeSession::Clear()
{
	if(this->Pending())
	{
		this->SetCanceled();
	}

#if __ASSERT
	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		netAssert(!m_Status[i]);
	}
#endif

	OUTPUT_ONLY(m_TunnelRequestId = KX_INVALID_TUNNEL_REQUEST_ID);
	m_SessionId = KX_INVALID_SESSION_ID;
	m_DestSessionId = KX_INVALID_SESSION_ID;
	m_RemoteTransactionId = KX_INVALID_TRANSACTION_ID;
	m_Role = ROLE_INVALID;
	m_RoleTieBreaker = 0;
	m_OfferTransactionToken = 0;
	m_CxnMgr = NULL;
	m_RemoteAddr.Clear();
	m_RemoteRelayAddr.Clear();
	m_LocalPeerAddr.Clear();
	m_RemotePeerAddr.Clear();
	m_NetAddr.Clear();
	m_ConnectingToSessionHost = false;
	m_LocalPeerIsHost = false;
	m_HadRoleConflict = false;
	m_NumOffersSent = 0;
	m_NumOffersReceived = 0;
	m_NumAnswersSent = 0;
	m_NumAnswersReceived = 0;
	m_HaveRemotePublicKey = false;
	m_HaveLocalPublicKey = false;
	m_HaveAgreeKey = false;

	if(m_WorkItem.Pending())
	{
		s_KxThreadPool.CancelWork(m_WorkItem.GetId());
	}
	netAssert(!m_WorkItem.Pending());
	m_WorkItem = DiffieHellmanWorkItem();

	m_StopWatch = netStopWatch();
	m_TerminationTimer = netTimeout();
	m_Transactor.Clear();
	m_Canceled = false;
	m_State = STATE_INACTIVE;

	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		m_Status[i] = NULL;
	}
}

bool
netKeyExchangeSession::Init(const unsigned OUTPUT_ONLY(tunnelRequestId),
							const unsigned sessionId,
                            netConnectionManager* cxnMgr,
                            const netPeerAddress& peerAddr,
							const netAddress& addr,
							const netAddress& relayAddr)
{
	rtry
	{
		this->Clear();

		OUTPUT_ONLY(m_TunnelRequestId = tunnelRequestId);
		m_SessionId = sessionId;

		netKeyExchangeDebug1("We're exchanging keys with %s over address " NET_ADDR_FMT, peerAddr.ToString(), NET_ADDR_FOR_PRINTF(addr));

		m_RoleTieBreaker = (unsigned)s_KxRng.GetInt();

#if !__NO_OUTPUT
		static bool sizeOfSpamOnce = true;
		if(sizeOfSpamOnce)
		{
			sizeOfSpamOnce = false;
			netKeyExchangeDebug1("SessionId:%u", m_SessionId);
			netKeyExchangeDebug3("sizeof(netKeyExchangeSession):%" SIZETFMT "u", sizeof(netKeyExchangeSession));
			netKeyExchangeDebug3("sizeof(m_Transactor):%" SIZETFMT "u", sizeof(m_Transactor));

			netKeyExchangeDebug3("netKeyExchangeSessionOffer::MAX_EXPORT_SIZE_IN_BYTES: %u", netKeyExchangeSessionOffer::MAX_EXPORT_SIZE_IN_BYTES);
			netKeyExchangeDebug3("netKeyExchangeSessionAnswer::MAX_EXPORT_SIZE_IN_BYTES: %u", netKeyExchangeSessionAnswer::MAX_EXPORT_SIZE_IN_BYTES);
		}
#endif

		rverify(cxnMgr, catchall,);
		rverify(peerAddr.IsValid(), catchall,);
		rverify(peerAddr.GetGamerHandle().IsSupportedGamerHandle(), catchall, netKeyExchangeError("Unsupported gamerhandle."));
		rverify(addr.IsValid(), catchall, );
		rverify(addr.IsDirectAddr() || addr.IsRelayServerAddr(), catchall, netError("Cannot perform key exchange via peer relay."));
		
		rverify(netPeerAddress::GetLocalPeerAddress(rlPresence::GetActingUserIndex(), &m_LocalPeerAddr), catchall,);

		m_CxnMgr = cxnMgr;
		m_Transactor.Init(m_CxnMgr);
		m_RemotePeerAddr = peerAddr;
		m_RemoteAddr = addr;
		m_RemoteRelayAddr = relayAddr;

		m_StopWatch = netStopWatch();
		m_StopWatch.Start();
		m_TerminationTimer.InitMilliseconds(netKeyExchange::sm_MaxSessionTimeMs);

		m_State = STATE_PENDING;

		return true;
	}
	rcatchall
	{

	}

	return false;
}

#if !__NO_OUTPUT
const char*
netKeyExchangeSession::FailReasonToString(const FailReason failReason) const
{
#define KX_FAIL_REASON_CASE(x) case x: return (&#x[strlen("FAIL_REASON_")]); break;
	switch(failReason)
	{
		KX_FAIL_REASON_CASE(FAIL_REASON_NONE);
		KX_FAIL_REASON_CASE(FAIL_REASON_EMERGENCY_TIMER);
		KX_FAIL_REASON_CASE(FAIL_REASON_COULD_NOT_SEND_OFFER);
		KX_FAIL_REASON_CASE(FAIL_REASON_OFFER_TIMED_OUT);
		KX_FAIL_REASON_CASE(FAIL_REASON_CANCELLED);
		KX_FAIL_REASON_CASE(FAIL_REASON_START_AS_OFFERER);
		KX_FAIL_REASON_CASE(FAIL_REASON_RECEIVE_OFFER);
		KX_FAIL_REASON_CASE(FAIL_REASON_RCV_OFFER_SEND_ANSWER);
		KX_FAIL_REASON_CASE(FAIL_REASON_START_AS_OFFERER_STATUS);
		KX_FAIL_REASON_CASE(FAIL_REASON_START_AS_OFFERER_NET_ADDR);
		KX_FAIL_REASON_CASE(FAIL_REASON_SIMULATED_FAIL);
		KX_FAIL_REASON_CASE(FAIL_REASON_ASYNC_TASK_FAILED);
		KX_FAIL_REASON_CASE(FAIL_REASON_COULD_NOT_START_KEY_AGREEMENT);
		KX_FAIL_REASON_CASE(FAIL_REASON_GEN_P2P_KEY);
		KX_FAIL_REASON_CASE(FAIL_REASON_GEN_RANDOM_KEY);
		KX_FAIL_REASON_CASE(FAIL_REASON_ENCRYPT_KEY);
		KX_FAIL_REASON_CASE(FAIL_REASON_DECRYPT_KEY);
	}
#undef KX_FAIL_REASON_CASE

	netAssertf(false, "netKeyExchangeSession::FailReasonToString :: Unhandled fail reason");
	return "UNKNOWN FAIL REASON";
}
#endif

void
netKeyExchangeSession::Complete(const CompletionType completionType, const FailReason failReason)
{
	if(!netVerify(this->Pending()))
	{
		return;
	}

#if !__NO_OUTPUT
	switch(completionType)
	{
	case COMPLETE_SUCCEEDED:
		netKeyExchangeDebug1("Key Exchange SUCCEEDED after %u ms", this->GetElapsedTime());
		break;
	case COMPLETE_FAILED:
		netKeyExchangeError("Key Exchange FAILED after %u ms with fail reason: %s",
							 this->GetElapsedTime(), FailReasonToString(failReason));
		break;
	}
#endif

	{
		if(COMPLETE_SUCCEEDED == completionType)
		{
			m_State = STATE_SUCCEEDED;

			netKeyExchange::m_NumSuccessfulSessions++;

			for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
			{
				if(m_Status[i])
				{
					m_Status[i]->SetSucceeded();
				}
			}
		}
		else
		{
			m_State = STATE_FAILED;

			netKeyExchange::m_NumFailedSessions++;

			for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
			{
				if(m_Status[i])
				{
					m_Status[i]->SetFailed(failReason);
				}
			}
		}

		for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
		{
			m_Status[i] = NULL;
		}
	}

#if !__NO_OUTPUT
	unsigned totalSessions = netKeyExchange::m_NumFailedSessions + netKeyExchange::m_NumSuccessfulSessions;
	float failRate = ((float)netKeyExchange::m_NumFailedSessions / (float)totalSessions) * 100.0f;
	netDebug("Num Failed Sessions: %u, Num Successful Sessions: %u, Total Sessions: %u, Local Fail Rate: %.2f", netKeyExchange::m_NumFailedSessions, netKeyExchange::m_NumSuccessfulSessions, totalSessions, failRate);
#endif

	if(COMPLETE_SUCCEEDED == completionType)
	{
		netKeyExchangeDebug3("Setting termination timer for %u ms", KX_TERMINATION_TIME_MS);
		m_TerminationTimer.InitMilliseconds(KX_TERMINATION_TIME_MS);
	}
	else
	{
		if(!m_WorkItem.Pending())
		{
			Clear();
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//  netKeyExchange
//////////////////////////////////////////////////////////////////////////

netKeyExchangeSession m_KxSessions[KX_MAX_ACTIVE_SESSIONS];
typedef inlist<netKeyExchangeSession, &netKeyExchangeSession::m_ListLink> KxSessionList;
KxSessionList m_KxActiveSessions;
KxSessionList m_KxFreeSessions;

sysCriticalSectionToken netKeyExchange::m_Cs;

netConnectionManager* netKeyExchange::m_CxnMgr = NULL;
netConnectionManager::Delegate netKeyExchange::m_CxnEventDelegate;
bool netKeyExchange::m_Initialized = false;
unsigned netKeyExchange::m_LastKeyDeletionTime = 0;
unsigned netKeyExchange::m_PeakConcurrentSessions = 0;
unsigned netKeyExchange::m_TotalSessions = 0;
unsigned netKeyExchange::m_NumFailedSessions = 0;
unsigned netKeyExchange::m_NumSuccessfulSessions = 0;

// We send our session id in messages so the remote peer can send it back
// to us to correlate which session the message is for.
unsigned netKeyExchange::s_NextId = KX_INVALID_SESSION_ID;

sysCriticalSectionToken netKeyExchange::m_CsKeyList;

class netKeyExchangeSecurityContext
{
public:
	netKeyExchangeSecurityContext()
	{
		Clear();
	}

	~netKeyExchangeSecurityContext()
	{
		Clear();
	}

	void Clear()
	{
		m_Key.Clear();
	}

	void Init(const netP2pCrypt::Key& key)
	{
		Clear();
		m_Key = key;
	}

	netP2pCrypt::Key m_Key;
};

class netKeyExchangeEntry
{
public:
	netKeyExchangeEntry()
	{
		Clear();
	}
	~netKeyExchangeEntry()
	{
		Clear();
	}

	inlist_node<netKeyExchangeEntry> m_ListLink;

	void Clear()
	{
		m_PeerId.Clear();
		m_RelayAddr.Clear();
		m_DirectAddr.Clear();
		m_LastAccessTime = 0;
		m_Context.Clear();
	}

	void Init(const netPeerId& peerId, const netAddress& addr, const netAddress& relayAddr)
	{
		Clear();

		m_PeerId = peerId;
		SetAddr(addr);

		if(relayAddr.IsRelayServerAddr())
		{
			SetAddr(relayAddr);
		}

		Access();

		netDebug3("Initialized key:");
		Dump();
	}

	void Init(const netPeerId& peerId, const netAddress& addr, const netAddress& relayAddr, const netP2pCrypt::Key& key)
	{
		Clear();

		m_PeerId = peerId;
		SetAddr(addr);

		if(relayAddr.IsRelayServerAddr())
		{
			SetAddr(relayAddr);
		}

		m_Context.Init(key);
		Access();

		netDebug3("Initialized key:");
		Dump();
	}

	void Update(const netAddress& addr, const netAddress& relayAddr, const netP2pCrypt::Key& key)
	{
		SetAddr(addr);

		if(relayAddr.IsRelayServerAddr())
		{
			SetAddr(relayAddr);
		}

		m_Context.Init(key);
		Access();

		netDebug3("Updated key:");
		Dump();
	}

	void Update(const netAddress& addr, const netAddress& relayAddr)
	{
		bool changed = SetAddr(addr);

		if(relayAddr.IsRelayServerAddr())
		{
			changed |= SetAddr(relayAddr);
		}

		Access();

		if(changed)
		{
			netDebug3("Updated key:");
			Dump();
		}
	}

	unsigned GetLastAccessTime()
	{
		return m_LastAccessTime;
	}

	const netSocketAddress& GetDirectAddr() const
	{
		return m_DirectAddr;
	}

	const netAddress& GetRelayAddr() const
	{
		return m_RelayAddr;
	}

	const netPeerId& GetPeerId() const
	{
		return m_PeerId;
	}	

	bool IsMatch(const netAddress& addr) const
	{
		if(addr.IsDirectAddr())
		{
			return m_DirectAddr.IsValid() && (m_DirectAddr == addr.GetTargetAddress());
		}
		else if(addr.IsRelayServerAddr())
		{
			// addr might have a relay token as well as a real address so we can't
			// simply compare m_RelayAddr == addr. Keys are always associated with real
			// addresses so we only need to compare plaintext components here.
			return addr.GetTargetAddress().IsValid() &&
					m_RelayAddr.IsRelayServerAddr() &&
					(addr.GetProxyAddress() == m_RelayAddr.GetProxyAddress()) &&
					(addr.GetTargetAddress() == m_RelayAddr.GetTargetAddress());
		}
		else if(netVerify(addr.IsPeerRelayAddr()))
		{
			return m_PeerId == addr.GetTargetPeerId();
		}

		return false;
	}

	const netKeyExchangeSecurityContext& GetSecurityContext()
	{
		Access();
		return m_Context;
	}

	void Dump()
	{
#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		netDebug3("    peerId: %s, directAddr: " NET_ADDR_FMT ", relayAddr: " NET_ADDR_FMT ", hasKey : %s",
					m_PeerId.ToHexString(peerIdHexString),
					NET_ADDR_FOR_PRINTF(m_DirectAddr),
					NET_ADDR_FOR_PRINTF(m_RelayAddr),
					m_Context.m_Key.IsValid() ? "true" : "false");
#endif
	}

private:
	void Access()
	{
		m_LastAccessTime = sysTimer::GetSystemMsTime();
	}

	bool SetAddr(const netAddress& addr)
	{
		bool changed = false;

		if(addr.IsDirectAddr())
		{
			m_DirectAddr = addr.GetTargetAddress();
			changed = true;
		}
		else if(addr.IsRelayServerAddr())
		{
			if(addr.GetTargetAddress().IsValid())
			{
				m_RelayAddr = addr;
				changed = true;
			}
		}
		else if(netVerify(addr.IsPeerRelayAddr()))
		{
			// peer relay addresses contain the peer id, which never changes.
			netAssert(addr.GetTargetPeerId() == m_PeerId);
		}

		return changed;
	}

	netPeerId m_PeerId;
	netAddress m_RelayAddr;
	netSocketAddress m_DirectAddr;
	unsigned m_LastAccessTime;
	netKeyExchangeSecurityContext m_Context;
};

netKeyExchangeEntry m_KeyEntry[KX_MAX_ACTIVE_KEYS];
typedef inlist<netKeyExchangeEntry, &netKeyExchangeEntry::m_ListLink> netKeyExchangeList;
netKeyExchangeList m_ActiveKeys;
netKeyExchangeList m_FreeKeys;

bool netKeyExchange::InitThreadPool()
{
	rtry
	{
		rverifyall(s_KxThreadPool.Init());

		// logging truncates thread name to "<%s>" with 16 characters, excluding the "[RAGE] " prefix
		for (unsigned i = 0; i < KX_THREAD_POOL_NUM_THREADS; ++i)
		{
			char name[32];
			formatf(name, "[RAGE] netKxThrPool %u", i + 1);
#if USE_PROFILER & 0
			s_KxThreads[i].ProfileThread(name);
#endif
			rverifyall(s_KxThreadPool.AddThread(&s_KxThreads[i], name, (i & 1) ? NET_THREADPOOL_CPU_2 : NET_THREADPOOL_CPU_1));
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void netKeyExchange::ShutdownThreadPool()
{
	s_KxThreadPool.Shutdown();
}

bool
netKeyExchange::Init(netConnectionManager* cxnMgr)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(!m_Initialized, catchall,);
		rverify(cxnMgr != NULL, catchall,);

		netDebug3("netKeyExchange::Init(): sizeof(m_KxSessions):%" SIZETFMT "u", sizeof(m_KxSessions));
		netDebug3("netKeyExchange::Init(): sizeof(m_KeyEntry):%" SIZETFMT "u", sizeof(m_KeyEntry));

		InitThreadPool();

		for(unsigned i = 0; i < KX_MAX_ACTIVE_SESSIONS; ++i)
		{
			m_KxFreeSessions.push_back(&m_KxSessions[i]);
		}

		for(unsigned i = 0; i < KX_MAX_ACTIVE_KEYS; ++i)
		{
			m_FreeKeys.push_back(&m_KeyEntry[i]);
		}

		s_KxRng.SetRandomSeed();

		m_CxnMgr = cxnMgr;

		unsigned channelId = NET_TUNNELER_CHANNEL_ID;

		m_CxnEventDelegate.Bind(&netKeyExchange::OnCxnEvent);
		m_CxnMgr->AddChannelDelegate(&m_CxnEventDelegate, channelId);

		m_Initialized = true;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
netKeyExchange::Shutdown()
{
	SYS_CS_SYNC(m_Cs);

	if(m_Initialized)
	{
		if(m_CxnMgr)
		{
			if(m_CxnEventDelegate.IsRegistered())
			{
				m_CxnMgr->RemoveChannelDelegate(&m_CxnEventDelegate);
			}
			m_CxnMgr = NULL;
		}

		{
			KxSessionList::iterator it = m_KxActiveSessions.begin();
			KxSessionList::const_iterator stop = m_KxActiveSessions.end();
			for(; stop != it; ++it)
			{
				netKeyExchangeSession* session = *it;
				if(session->IsActive())
				{
					session->Clear();
				}
			}
		}

		{
			netKeyExchangeList::iterator it = m_ActiveKeys.begin();
			netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
			for(; stop != it; ++it)
			{
				netKeyExchangeEntry* entry = *it;
				if(entry)
				{
					entry->Clear();
				}
			}
		}

		// Reset the lists
		m_KxFreeSessions.clear();
		m_KxActiveSessions.clear();
		m_FreeKeys.clear();
		m_ActiveKeys.clear();

		ShutdownThreadPool();

		m_Initialized = false;
	}
}

void
netKeyExchange::Update()
{
	PROFILE

	SYS_CS_SYNC(m_Cs);

	if(m_Initialized)
	{
		UpdateSessions();
		DeleteExpiredKeys();
	}
}

void
netKeyExchange::UpdateSessions()
{
	PROFILE

	KxSessionList::iterator it = m_KxActiveSessions.begin();
	KxSessionList::iterator next = it;
	KxSessionList::const_iterator stop = m_KxActiveSessions.end();
	for(++next; stop != it; it = next, ++next)
	{
		netKeyExchangeSession* session = *it;

		if(session->IsActive())
		{
			session->Update();
		}
		else
		{
			FreeSession(session);
		}
	}
}

void
netKeyExchange::FreeSession(netKeyExchangeSession* session)
{
	if(netVerify(session))
	{
		m_KxActiveSessions.erase(session);
		m_KxFreeSessions.push_back(session);

		netDebug3("Removed inactive session and added to free list. "
			"Num Active Sessions: %" SIZETFMT "u, Num Free Sessions: %" SIZETFMT "u",
			m_KxActiveSessions.size(), m_KxFreeSessions.size());

		netAssert((m_KxActiveSessions.size() + m_KxFreeSessions.size()) == KX_MAX_ACTIVE_SESSIONS);
	}
}

bool
netKeyExchange::OnPeerDiscovered(const netPeerId& peerId, const netAddress& addr, const netAddress& relayAddr)
{
	SYS_CS_SYNC(m_CsKeyList);

	rtry
	{
		rverify(m_Initialized, catchall,);

		unsigned leastRecentlyUsedTime = 0;
		netKeyExchangeEntry* leastRecentlyUsedEntry = NULL;

		netKeyExchangeList::iterator it = m_ActiveKeys.begin();
		netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
		for(; stop != it; ++it)
		{
			netKeyExchangeEntry* entry = *it;
			if(entry->GetPeerId() == peerId)
			{
				// if we have an entry matching newAddr that is NOT this entry, we need
				// to delete it otherwise we'll have two keys with the same address
				netKeyExchangeEntry* entry2 = FindMatchingEntry(addr);
				if((entry2 != nullptr) && (entry2 != entry))
				{
					netDebug3("OnPeerDiscovered : already have entry with addr, deleting");
					FreeKey(entry2);
				}

				entry->Update(addr, relayAddr);
				return true;
			}

			if((leastRecentlyUsedTime == 0) || (entry->GetLastAccessTime() < leastRecentlyUsedTime))
			{
				leastRecentlyUsedTime = entry->GetLastAccessTime();
				leastRecentlyUsedEntry = entry;
			}
		}

		it = m_ActiveKeys.begin();
		stop = m_ActiveKeys.end();
		for(; stop != it; ++it)
		{
			netKeyExchangeEntry* entry = *it;
			if(entry->IsMatch(addr))
			{
				netDebug("OnPeerDiscovered : found key with same address but different peer id, overwriting entry.");
				entry->Init(peerId, addr, relayAddr);
				return true;
			}
		}

		if(!netVerify(m_FreeKeys.empty() == false))
		{
			// evict the least recently used entry
			rverify(leastRecentlyUsedEntry, catchall, netError("We have run out of free entries, and failed to find the least recently accessed entry"));

			bool canEvict = sysTimer::HasElapsedIntervalMs(leastRecentlyUsedEntry->GetLastAccessTime(), KX_MIN_TIME_BEFORE_EVICTION_MS);
			rverify(canEvict, catchall, netError("We have run out of free entries, and we have no evictable entries."));

			netWarning("We have run out of free entries. Evicting the fleast recently accessed entry.");
			leastRecentlyUsedEntry->Init(peerId, addr, relayAddr);
		}
		else
		{
			netKeyExchangeEntry* entry = m_FreeKeys.front();
			rverify(entry, catchall,);
			m_FreeKeys.pop_front();
			entry->Init(peerId, addr, relayAddr);
			m_ActiveKeys.push_back(entry);

			netDebug3("Allocated entry. Num Active Entries: %" SIZETFMT "u, Num Free Entries: %" SIZETFMT "u, ", m_ActiveKeys.size(), m_FreeKeys.size());

			netAssert((m_ActiveKeys.size() + m_FreeKeys.size()) == KX_MAX_ACTIVE_KEYS);
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
netKeyExchange::OnKeyReceived(const netPeerId& peerId, const netAddress& addr, const netAddress& relayAddr, const netP2pCrypt::Key& key)
{
	SYS_CS_SYNC(m_CsKeyList);

	rtry
	{
		rverify(m_Initialized, catchall,);

		unsigned leastRecentlyUsedTime = 0;
		netKeyExchangeEntry* leastRecentlyUsedEntry = NULL;

		unsigned curTime = sysTimer::GetSystemMsTime();

		netKeyExchangeList::iterator it = m_ActiveKeys.begin();
		netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
		for(; stop != it; ++it)
		{
			netKeyExchangeEntry* entry = *it;
			if(entry->GetPeerId() == peerId)
			{
				netDebug3("OnKeyReceived - already have entry for " NET_ADDR_FMT ". Updating address and key.", NET_ADDR_FOR_PRINTF(addr));

				// if we have an entry matching newAddr that is NOT this entry, we need
				// to delete it otherwise we'll have two keys with the same address
				netKeyExchangeEntry* entry2 = FindMatchingEntry(addr);
				if((entry2 != nullptr) && (entry2 != entry))
				{
					netDebug3("OnKeyReceived : already have entry with addr, deleting");
					FreeKey(entry2);
				}

				entry->Update(addr, relayAddr, key);
				return;
			}

			if((leastRecentlyUsedTime == 0) || (entry->GetLastAccessTime() < leastRecentlyUsedTime))
			{
				leastRecentlyUsedTime = entry->GetLastAccessTime();
				leastRecentlyUsedEntry = entry;
			}
		}

		it = m_ActiveKeys.begin();
		stop = m_ActiveKeys.end();
		for(; stop != it; ++it)
		{
			netKeyExchangeEntry* entry = *it;
			if(entry->IsMatch(addr))
			{
				netDebug("OnKeyReceived : found key with same address but different peer id, overwriting entry.");
				entry->Init(peerId, addr, relayAddr, key);
				return;
			}
		}

		if(!netVerify(m_FreeKeys.empty() == false))
		{
			// evict the least recently used key
			rverify(leastRecentlyUsedEntry, catchall, netError("We have run out of free keys, and failed to find the least recently accessed key"));
			bool canEvict = (curTime - leastRecentlyUsedEntry->GetLastAccessTime()) > KX_MIN_TIME_BEFORE_EVICTION_MS;
			rverify(canEvict, catchall, netError("We have run out of free keys, and we have no evictable keys."));

			netWarning("We have run out of free keys. Evicting the least recently accessed key.");
			leastRecentlyUsedEntry->Init(peerId, addr, relayAddr, key);
		}
		else
		{
			netKeyExchangeEntry* entry = m_FreeKeys.front();
			rverify(entry, catchall,);
			m_FreeKeys.pop_front();
			entry->Init(peerId, addr, relayAddr, key);
			m_ActiveKeys.push_back(entry);

			netDebug3("Allocated key. Num Active Entries: %" SIZETFMT "u, Num Free Entries: %" SIZETFMT "u, ", m_ActiveKeys.size(), m_FreeKeys.size());

			netAssert((m_ActiveKeys.size() + m_FreeKeys.size()) == KX_MAX_ACTIVE_KEYS);
		}
	}
	rcatchall
	{
	}
}

bool
netKeyExchange::FreeKey(netKeyExchangeEntry* entry)
{
	if(entry == nullptr)
	{
		return false;
	}

	SYS_CS_SYNC(m_CsKeyList);

	netDebug3("Deleting key:");
	entry->Dump();

	entry->Clear();
	m_ActiveKeys.erase(entry);
	m_FreeKeys.push_back(entry);

	netDebug3("Removed inactive key and added to free list. "
			    "Num Active Entries: %" SIZETFMT "u, Num Free Entries: %" SIZETFMT "u",
			    m_ActiveKeys.size(), m_FreeKeys.size());

	return true;
}

bool
netKeyExchange::FreeKey(const netAddress& addr)
{
	SYS_CS_SYNC(m_CsKeyList);

	netDebug3("FreeKey for " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr));

	netKeyExchangeList::iterator it = m_ActiveKeys.begin();
	netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
	for(; stop != it; ++it)
	{
		netKeyExchangeEntry* entry = *it;
		if(entry->IsMatch(addr))
		{
			return FreeKey(entry);
		}
	}

	return true;
}

bool
netKeyExchange::GetP2pKey(const netAddress& addr, netP2pCrypt::Key& key)
{
	SYS_CS_SYNC(m_CsKeyList);

	netKeyExchangeList::iterator it = m_ActiveKeys.begin();
	netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
	for(; stop != it; ++it)
	{
		netKeyExchangeEntry* entry = *it;
		if(entry->IsMatch(addr))
		{
			key = entry->GetSecurityContext().m_Key;
			return key.IsValid();
		}
	}

	return false;
}

bool
netKeyExchange::GetP2pKey(const netPeerId& peerId, netP2pCrypt::Key& key)
{
	SYS_CS_SYNC(m_CsKeyList);

	netKeyExchangeList::iterator it = m_ActiveKeys.begin();
	netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
	for(; stop != it; ++it)
	{
		netKeyExchangeEntry* entry = *it;
		if(entry->GetPeerId() == peerId)
		{
			key = entry->GetSecurityContext().m_Key;
			return key.IsValid();
		}
	}

	return false;
}

bool
netKeyExchange::GetRelayAddr(const netPeerId& peerId, netAddress& relayAddr)
{
	SYS_CS_SYNC(m_CsKeyList);

	netKeyExchangeList::iterator it = m_ActiveKeys.begin();
	netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
	for(; stop != it; ++it)
	{
		netKeyExchangeEntry* entry = *it;
		if(entry->GetPeerId() == peerId)
		{
			relayAddr = entry->GetRelayAddr();
			return relayAddr.IsRelayServerAddr();
		}
	}

	return false;
}

void
netKeyExchange::SetTransactionRetryIntervalMs(const unsigned transactionRetryIntervalMs)
{
	if(sm_TransactionRetryIntervalMs != transactionRetryIntervalMs)
	{
		netDebug("SetTransactionRetryIntervalMs :: %u", transactionRetryIntervalMs);
		sm_TransactionRetryIntervalMs = transactionRetryIntervalMs;
	}
}

void
netKeyExchange::SetMaxRetriesPerTransaction(const unsigned maxRetriesPerTransaction)
{
	if(sm_MaxRetriesPerTransaction != maxRetriesPerTransaction)
	{
		netDebug("SetMaxRetriesPerTransaction :: %u", maxRetriesPerTransaction);
		sm_MaxRetriesPerTransaction = maxRetriesPerTransaction;
	}
}

void
netKeyExchange::SetMaxSessionTimeMs(const unsigned maxSessionTimeMs)
{
	if(sm_MaxSessionTimeMs != maxSessionTimeMs)
	{
		netDebug("SetMaxSessionTimeMs :: %u", maxSessionTimeMs);
		sm_MaxSessionTimeMs = maxSessionTimeMs;
	}
}

void
netKeyExchange::SetAllowOfferResendViaRelay(const bool allowOfferResendViaRelay)
{
	if(sm_AllowOfferResendViaRelay != allowOfferResendViaRelay)
	{
		netDebug("SetAllowOfferResendViaRelay :: %s", allowOfferResendViaRelay ? "true" : "false");
		sm_AllowOfferResendViaRelay = allowOfferResendViaRelay;
	}
}

void
netKeyExchange::DeleteExpiredKeys()
{
	PROFILE

	// we run this periodically, not every frame.
	unsigned curTime = sysTimer::GetSystemMsTime();
	unsigned elapsed = curTime - m_LastKeyDeletionTime;
	if(elapsed < KX_KEY_PURGE_INTERVAL_MS)
	{
		return;
	}

	m_LastKeyDeletionTime = curTime;

	SYS_CS_SYNC(m_CsKeyList);	

	netKeyExchangeList::iterator it = m_ActiveKeys.begin();
	netKeyExchangeList::iterator next = it;
	netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
	for(++next; stop != it; it = next, ++next)
	{
		netKeyExchangeEntry* entry = *it;
		unsigned lastAccessTime = entry->GetLastAccessTime();
		netAssert(lastAccessTime > 0);

		unsigned elapsedTime = curTime - lastAccessTime;
		if(elapsedTime > KX_KEY_EXPIRATION_TIME_MS)
		{
			// only if we don't still have an endpoint with this peer id. This
			// prevents keys from being deleted when, for example, -notimeouts
			// is enabled, or when inactive endpoints are not being removed.
			if(!m_CxnMgr->HasActiveEndpointWithPeerId(entry->GetPeerId()))
			{
				netDebug3("DeleteExpiredKeys - deleting key unused for %ums", elapsedTime);
				FreeKey(entry);
			}
		}
	}
}

netKeyExchangeEntry* 
netKeyExchange::FindMatchingEntry(const netAddress& addr)
{
	SYS_CS_SYNC(m_CsKeyList);

	netKeyExchangeList::iterator it = m_ActiveKeys.begin();
	netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
	for(; stop != it; ++it)
	{
		netKeyExchangeEntry* entry = *it;
		if(entry->IsMatch(addr))
		{
			return entry;
		}
	}

	return nullptr;
}

bool
netKeyExchange::UpdateP2pKey(const netPeerId& peerId, const netAddress& newAddr, const netAddress& relayAddr)
{
	SYS_CS_SYNC(m_CsKeyList);

	if(!netVerify(peerId.IsValid()))
	{
		return false;
	}

	netKeyExchangeList::iterator it = m_ActiveKeys.begin();
	netKeyExchangeList::const_iterator stop = m_ActiveKeys.end();
	for(; stop != it; ++it)
	{
		netKeyExchangeEntry* entry = *it;
		if(entry->GetPeerId() == peerId)
		{
			// if we have an entry matching newAddr that is NOT this entry, we need
			// to delete it otherwise we'll have two keys with the same address
			netKeyExchangeEntry* entry2 = FindMatchingEntry(newAddr);
			if((entry2 != nullptr) && (entry2 != entry))
			{
				netDebug3("UpdateP2pKey : already have entry with newAddr, deleting");
				FreeKey(entry2);
			}

			entry->Update(newAddr, relayAddr);
			return true;
		}
	}

	it = m_ActiveKeys.begin();
	stop = m_ActiveKeys.end();
	for(; stop != it; ++it)
	{
		netKeyExchangeEntry* entry = *it;
		if(entry->IsMatch(newAddr))
		{
			netDebug("OnKeyReceived : found key with same address but different peer id, overwriting entry.");
			entry->Init(peerId, newAddr, relayAddr);
			return true;
		}
	}

#if !__NO_OUTPUT
	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	netDebug3("UpdateP2pKey no active key found for peer id %s. Creating new entry without a key.", peerId.ToHexString(peerIdHexString));
#endif

	OnPeerDiscovered(peerId, newAddr, relayAddr);

	return false;
}

bool
netKeyExchange::ExchangeKeys(const netPeerAddress& peerAddr,
                             const netAddress& addr,
							 const netAddress& relayAddr,
                             const bool connectingToSessionHost,
							 const unsigned tunnelRequestId,
                             netStatus* status)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall,);
		rverify(peerAddr.IsValid(), catchall,);
		rverify(addr.IsValid(), catchall,);
		rverify(status, catchall,);

		OnPeerDiscovered(peerAddr.GetPeerId(), addr, relayAddr);

		/*
			if there is already an active session with this peer, then attach to the active session.
		*/
		bool newSession = true;

		KxSessionList::iterator it = m_KxActiveSessions.begin();
		KxSessionList::const_iterator stop = m_KxActiveSessions.end();
		for(; stop != it; ++it)
		{
			netKeyExchangeSession* session = *it;
			if(session->IsActive())
			{
				const netPeerId& remotePeerId = session->GetRemotePeerAddress().GetPeerId();
				if(remotePeerId.IsValid() && (remotePeerId == peerAddr.GetPeerId()))
				{
#if !__NO_OUTPUT
					char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
					char peerIdHexString2[netPeerId::TO_HEX_STRING_BUFFER_SIZE];

					netDebug("Attaching: peerId: %s, remotePeerId: %s, session:%u", peerAddr.GetPeerId().ToHexString(peerIdHexString), remotePeerId.ToHexString(peerIdHexString2), session->GetSessionId());
#endif
					if(session->Attach(tunnelRequestId, status))
					{
						newSession = false;
						break;
					}
				}
			}
		}

		if(newSession)
		{
			netKeyExchangeSession* session = AllocateSession();
			rverify(session, catchall,);
			rverify(session->Init(tunnelRequestId, GetNextSessionId(), m_CxnMgr, peerAddr, addr, relayAddr), catchall,);
			rcheck(session->StartAsOfferer(addr, connectingToSessionHost, status), catchall, netDebug("ExchangeKeys(): StartAsOfferer() failed"));
		}

		return true;
	}
	rcatchall
	{
		if(status)
		{
			if(status->Pending())
			{
				status->SetFailed();
			}
			else
			{
				status->ForceFailed();
			}
		}
	}

	return false;
}

void
netKeyExchange::CancelKeyExchange(netStatus* status)
{
	/*
		Note: don't put this function in a critical section.
		We can be updating an existing session on one thread and
		it could be sending a packet which enters the connection
		manager's critical section. The connection manager
		could receive a packet that ends up calling this function
		and we deadlock.
	*/

	rtry
	{
		// find the session(s) with this status object and set them to canceled

		rverify(m_Initialized, catchall,);
		rverify(status, catchall,);

		KxSessionList::iterator it = m_KxActiveSessions.begin();
		KxSessionList::const_iterator stop = m_KxActiveSessions.end();
		for(; stop != it; ++it)
		{
			netKeyExchangeSession* session = *it;

			if(session->IsActive() && (session->IsStatusAttached(status)))
			{
				session->SetCanceled(status);
			}
		}
	}
	rcatchall
	{
	}
}

unsigned
netKeyExchange::GetNextSessionId()
{
	SYS_CS_SYNC(m_Cs);

	unsigned id = sysInterlockedIncrement(&s_NextId);

	while(KX_INVALID_SESSION_ID == id)
	{
		id = sysInterlockedIncrement(&s_NextId);
	}

	return id;
}

netKeyExchangeSession*
netKeyExchange::AllocateSession()
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall,);
		rverify(m_KxFreeSessions.empty() == false, catchall,);

		netKeyExchangeSession* session = m_KxFreeSessions.front();
		rverify(session, catchall,);
		m_KxFreeSessions.pop_front();
		m_KxActiveSessions.push_back(session);

		m_TotalSessions++;
		unsigned numConcurrentSessions = (unsigned)m_KxActiveSessions.size();
		if(numConcurrentSessions > m_PeakConcurrentSessions)
		{
			m_PeakConcurrentSessions = numConcurrentSessions;
		}

		netDebug3("Allocated session. Num Active Sessions: %" SIZETFMT "u, Num Free Sessions: %" SIZETFMT "u, "
		"Peak Concurrent Sessions: %u, Total Sessions: %u",
		m_KxActiveSessions.size(), m_KxFreeSessions.size(),
		m_PeakConcurrentSessions, m_TotalSessions);

		netAssert((m_KxActiveSessions.size() + m_KxFreeSessions.size()) == KX_MAX_ACTIVE_SESSIONS);

		return session;
	}
	rcatchall
	{
	}

	return NULL;
}

netKeyExchangeSession*
netKeyExchange::FindSessionById(const unsigned id)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall,);

		KxSessionList::iterator it = m_KxActiveSessions.begin();
		KxSessionList::const_iterator stop = m_KxActiveSessions.end();
		for(; stop != it; ++it)
		{
			netKeyExchangeSession* session = *it;
			if(session->IsActive() && (session->GetSessionId() == id))
			{
				return session;
			}
		}
	}
	rcatchall
	{
	}

	return NULL;
}

void
netKeyExchange::ReceiveOffer(const netKeyExchangeSessionOffer* msg, const netAddress& sender)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall,);
		rverify(msg, catchall, );
		rverify(msg->m_SourcePeerAddr.IsValid(), catchall,);
		rverify(sender.IsValid(), catchall,);

		const netAddress& srcRelayAddr = msg->m_SourcePeerAddr.GetRelayAddress();
		netDebug2("Sender's relay address from netKeyExchangeSessionOffer: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(srcRelayAddr));

		if(srcRelayAddr.IsValid())
		{
			netAssert(srcRelayAddr.GetProxyAddress().IsValid());
			netAssert(srcRelayAddr.GetRelayToken().IsValid());
			netAssert(srcRelayAddr.GetTargetAddress().IsValid());
		}

		netAddress associatedAddr;
		netAddress relayAddr;
		if(sender.IsRelayServerAddr())
		{
			netAssert(srcRelayAddr.IsRelayServerAddr());

			netAssert(srcRelayAddr.GetProxyAddress() == sender.GetProxyAddress());

			if(sender.GetRelayToken().IsValid())
			{
				if(netVerify(srcRelayAddr.GetProxyAddress() == sender.GetProxyAddress()) &&
					netVerify(srcRelayAddr.GetRelayToken() == sender.GetRelayToken()) &&
					netVerify(srcRelayAddr.GetTargetAddress().IsValid()))
				{
					// the key should be associated with non-tokenized relay address only
					associatedAddr.Init(srcRelayAddr.GetProxyAddress(), netRelayToken::INVALID_RELAY_TOKEN, srcRelayAddr.GetTargetAddress());
				}
			}
			else
			{
				netAssert(srcRelayAddr.GetTargetAddress() == sender.GetTargetAddress());
				associatedAddr = sender;
			}

			relayAddr = associatedAddr;
		}
		else
		{
			associatedAddr = sender;
			relayAddr.Init(srcRelayAddr.GetProxyAddress(), netRelayToken::INVALID_RELAY_TOKEN, srcRelayAddr.GetTargetAddress());
		}

		rverify(associatedAddr.IsValid(), catchall, );

		OnPeerDiscovered(msg->m_SourcePeerAddr.GetPeerId(), associatedAddr, relayAddr);

		/*
			go through our list of active sessions and see if any of them have this remote peer
			address and remote session id since this could be an offer sent to an existing
			session. We don't want to end up creating a new session in that case.
		*/
		netKeyExchangeSession* session = NULL;

		KxSessionList::iterator it = m_KxActiveSessions.begin();
		KxSessionList::const_iterator stop = m_KxActiveSessions.end();
		for(; stop != it; ++it)
		{
			netKeyExchangeSession* curSession = *it;
			if(curSession->IsActive() && curSession->CanReceiveOffer(msg))
			{
				session = curSession;
				break;
			}
		}

		if(session == NULL)
		{
			session = AllocateSession();
			rverify(session, catchall,);
			rverify(session->Init(KX_INVALID_TUNNEL_REQUEST_ID, GetNextSessionId(), m_CxnMgr, msg->m_SourcePeerAddr, associatedAddr, relayAddr), catchall,);
		}

		rcheck(session->ReceiveOffer(msg, associatedAddr), catchall,);
	}
	rcatchall
	{
	}
}

void
netKeyExchange::ReceiveAnswer(const netKeyExchangeSessionAnswer* msg, const netAddress& sender)
{
	SYS_CS_SYNC(m_Cs);

	netKeyExchangeSession* session = FindSessionById(msg->m_DestSessionId);

	if(session)
	{
		session->ReceiveAnswer(msg, sender);
	}
	else
	{
		netDebug2("Received answer from " NET_ADDR_FMT " for unknown session with id %u:", NET_ADDR_FOR_PRINTF(sender), msg->m_DestSessionId);
	}
}

void
netKeyExchange::OnCxnEvent(netConnectionManager* UNUSED_PARAM(cxnMgr), const netEvent* evt)
{
	if(!m_Initialized)
	{
		return;
	}

	unsigned msgId;
	const netEventFrameReceived* fr = evt->m_FrameReceived;

	if(NET_EVENT_FRAME_RECEIVED == evt->GetId()
	        && netMessage::GetId(&msgId,
	                             fr->m_Payload,
	                             fr->m_SizeofPayload))
	{
#if !__NO_OUTPUT
		// test cases where we can't receive key exchange messages from direct and/or relay server addresses
		if(PARAM_netkxignoredirectmsgs.Get() && fr && fr->m_Sender.IsDirectAddr())
		{
			return;
		}

		if(PARAM_netkxignoreindirectmsgs.Get() && fr && fr->m_Sender.IsRelayServerAddr())
		{
			return;
		}
#endif

		if(netKeyExchangeSessionOffer::MSG_ID() == msgId)
		{
			netKeyExchangeSessionOffer msg;
			if(msg.Import(fr->m_Payload,
			              fr->m_SizeofPayload))
			{
				ReceiveOffer(&msg, fr->m_Sender);
			}
		}
		else if(netKeyExchangeSessionAnswer::MSG_ID() == msgId)
		{
			netKeyExchangeSessionAnswer msg;
			if(msg.Import(fr->m_Payload,
			              fr->m_SizeofPayload))
			{
				ReceiveAnswer(&msg, fr->m_Sender);
			}
		}
	}
}

}   //namespace rage
