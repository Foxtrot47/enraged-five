// 
// net/netsocket_orbis.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_SCE

#include "netsocket.h"

#include "diag/seh.h"
#include "netdiag.h"
#include "nethardware.h"
#include "system/memops.h"
#include "system/timer.h"

#include <libnetctl.h>
#include <netinet/in.h>
#include <np/np_common.h> // for SCE_NP_PORT
#include <sys/socket.h>
#include <errno.h>

#define NET_SET_SKTERR(se, e)   while((se)){*(se) = (e);break;}

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, socket_orbis)
#undef __rage_channel
#define __rage_channel ragenet_socket_orbis

netSocketError
netSocket::GetLastSocketError()
{
    netSocketError err = NET_SOCKERR_NONE;

    const int lasterror = errno;

    switch(lasterror)
    {
        case 0:
            break;
        case SCE_NET_ENETDOWN:
            err = NET_SOCKERR_NETWORK_DOWN;
            break;
        case SCE_NET_ENOBUFS:
            err = NET_SOCKERR_NO_BUFFERS;
            break;
        case SCE_NET_EMSGSIZE:
            err = NET_SOCKERR_MESSAGE_TOO_LONG;
            break;
        case SCE_NET_EADDRNOTAVAIL:     //Invalid address was specified
        case SCE_NET_ENETUNREACH:       //Destination is unreachable
        case SCE_NET_EHOSTDOWN:         //Other end is down and unreachable
        case SCE_NET_EHOSTUNREACH:      //Network unreachable
            err = NET_SOCKERR_HOST_UNREACHABLE;
            break;
        case SCE_NET_ECONNRESET:
            err = NET_SOCKERR_CONNECTION_RESET;
            break;
        case SCE_NET_EWOULDBLOCK:
            err = NET_SOCKERR_WOULD_BLOCK;
            break;
        case SCE_NET_EINPROGRESS:
            err = NET_SOCKERR_IN_PROGRESS;
            break;
		case SCE_NET_EINACTIVEDISABLED:
			err = NET_SOCKERR_INACTIVEDISABLED;
			netHardware::NotifyIpReleaseSocketError();
			break;
        default:
            netError("errno = %d", lasterror);
            err = NET_SOCKERR_UNKNOWN;
            break;
    }

    return err;
}

unsigned
netSocket::SizeofHeader()
{
    static const unsigned SIZEOF_IP_HEADER  = 20;
    static const unsigned SIZEOF_UDP_HEADER = 8;

    return SIZEOF_IP_HEADER + SIZEOF_UDP_HEADER;
}

unsigned
netSocket::SizeofPacket(const unsigned sizeofPayload)
{
    return this->SizeofHeader() + sizeofPayload;
}

//protected:

bool
netSocket::NativeGetMyAddress(netSocketAddress* addr) const
{
    bool success = false;

    addr->Clear();

    rtry
    {
        int skt = this->GetRawSocket();

        rverify(skt >= 0, catchall,);

        unsigned port = 0;

        if(NET_PROTO_UDP == this->GetProtocol())
        {
            //sceNetGetSockInfo() can get the port, but can't get the IP.
            //sceNetCtlGetInfo() can get the IP, but not the port.
            //So, we use both together.

            struct SceNetSockInfo info;
            sysMemSet(&info, 0, sizeof(info));

			rcheck(0 <= sceNetGetSockInfo(skt, &info, 1, 0),
                   catchall,
                   netError("[%u] netSocket::UpdateMyAddress(): sceNetGetSockInfo failed", m_Id));

            port = net_ntohs(info.local_port);
            //Can't get the IP here - it's zero.
        }
        else
        {
            SceNetSockaddrIn sinp2p = {0};
            socklen_t sin_len = sizeof(sinp2p);

            rverify(0 == getsockname(skt, (sockaddr*) &sinp2p, &sin_len),
                    catchall,
                    netError("[%u] Error calling getsockname", m_Id));

            port = net_ntohs(sinp2p.sin_vport);
            //Can't get the IP here - it's zero.
        }

        netIpAddress ip;
        rcheck(netHardware::GetLocalIpAddress(&ip),
                catchall,
                netError("[%u] Error retrieving local IP address", m_Id));

        addr->Init(ip, port);
        success = true;
    }
    rcatchall
    {
    }

    return success;
}

void
netSocket::NativeUpdate()
{
#if __BANK
	if(CanSendReceive())
	{
		const unsigned curTime = sysTimer::GetSystemMsTime();
		const unsigned elaspedTime = curTime - m_LastDiagnosticCheckTime;
		if(elaspedTime >= 1000)
		{
			m_LastDiagnosticCheckTime = curTime;
			SceNetSockInfo info;

			const int skt = this->GetRawSocket();
			if(sceNetGetSockInfo(skt, &info, 1, 0) >= 0)
			{
				const float warnPct = NET_SOCKET_BUFFER_WARN_PCT / 100.0f;

				const bool warn = (info.recv_queue_length > (info.recv_buffer_size * warnPct)) ||
								  (info.send_queue_length > (info.send_buffer_size * warnPct));

				if(warn)
				{
					netWarning("[%u] tx_buf: %d/%d, rx_buf: %d/%d, tx_bps: %d, rx_bps: %d, max_tx_bps: %d, max_rx_bps: %d",
								m_Id, info.send_queue_length, info.send_buffer_size, info.recv_queue_length, info.recv_buffer_size,
								info.tx_bps, info.rx_bps, info.max_tx_bps, info.max_rx_bps);
				}
			}
		}
	}
#endif
}

void
netSocket::NativeShutdown()
{
    this->NativeUnbind();
}

bool
netSocket::NativeBind()
{
    SYS_CS_SYNC(m_Cs);

    netAssert(netHardware::IsAvailable());
    netAssert(m_Socket < 0);
    netAssert(NET_PROTO_UDP == this->GetProtocol());

	netDebug("[%u] Binding socket on port %d...", m_Id, m_Port);

    rtry
    {
        m_Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

        rcheck(m_Socket >= 0, catchall, netError("[%u] socket() failed", m_Id));

        //Bind the socket to the requested port and whatever address
        //is given to us from the OS.

        //Bind to INADDR_ANY (i.e. any IP).
        sockaddr_in addr		= {0};
        addr.sin_family			= AF_INET;
        addr.sin_addr.s_addr	= INADDR_ANY;
        addr.sin_port			= net_htons(m_Port);

		if(0 != bind(m_Socket,
					(const sockaddr*) &addr,
					sizeof(addr)))
		{
			netError("[%u] Failed to bind on port %u (port probably already in use). Will attempt port 0 (ephemeral port) (LastError: %d)", m_Id, m_Port, netSocket::GetLastSocketError());

			addr.sin_port = net_htons(0);

			rcheck(0 == bind(m_Socket,
							(const sockaddr*) &addr,
							sizeof(addr)),
					catchall,
					netError("[%u] Failed to bind on port 0 (LastError: %d)", m_Id, netSocket::GetLastSocketError()));
		}

        rverify(this->NativeGetMyAddress(&m_Addr), catchall,);

        m_Port = m_Addr.GetPort();

		netDebug("[%u] I am at " NET_ADDR_FMT " with port: %u", m_Id, NET_ADDR_FOR_PRINTF(m_Addr), m_Port);

        //Enable broadcasting
        int broadcast = 1;

        rcheck(0 == setsockopt(m_Socket, SOL_SOCKET, SCE_NET_SO_BROADCAST, &broadcast, sizeof(broadcast)),
                catchall,
                netError("[%u] Failed to set socket options (broadcast)", m_Id));

        m_CanBroadcast = broadcast ? true : false;

		//Set the desired send/receive buffer sizes. Note: if this fails,
		// we fall back to default values and do not return an error.
		this->SetSendReceiveBufferSizes(m_DesiredSendBufferSize, m_DesiredReceiveBufferSize);

		//Set the blocking mode
        rverify(this->NativeSetBlocking(),
                 catchall,
                 netError("[%u] Failed to set socket blocking mode", m_Id));
    }
    rcatchall
    {
        if(m_Socket >= 0)
        {
			sceNetSocketClose(m_Socket);
        }

        m_Socket = -1;
    }

    return (m_Socket >= 0);
}

void
netSocket::NativeUnbind()
{
    SYS_CS_SYNC(m_Cs);

	netDebug("[%u] Unbinding socket from port %d...", m_Id, m_Port);

	// this socket was requested without preference for a port, reset the port to 0. 
	// on PS4, we can attempt to re-bind within the 
	if(m_DesiredPort == 0)
	{
		netDebug("[%u] Reassigning port back to desired / unspecified port of 0", m_Id);
		m_Port = 0;
	}

    if(m_Socket >= 0)
    {
        const int skt = m_Socket;
        m_Socket = -1;

		sceNetSocketClose(skt);
    }
}

bool
netSocket::NativeSend(const netSocketAddress& address,
                     const void* buffer,
                     const unsigned bufferSize,
                     netSocketError* sktErr)
{
    int skt = this->GetRawSocket();

    netAssert(skt >= 0);
    netAssert(bufferSize <= MAX_BUFFER_SIZE);

    netSocketError socketErr = NET_SOCKERR_NONE;

    if(address == m_Addr)
    {
        netWarning("Why am I sending a message to myself???");
    }
	
	sockaddr_in sin = {0};
	address.ToSockAddr((sockaddr*)&sin, sizeof(sin));

    int result = sendto(skt,
                        (const char*) buffer,
                        bufferSize,
                        0,
                        (const struct sockaddr*) &sin,
                        sizeof(sin));

    if(result < 0)
    {
		//Ignore SCE_NET_EWOULDBLOCK
		if(errno != SCE_NET_EWOULDBLOCK)
        {
            socketErr = netSocket::GetLastSocketError();
            netError("[%u] Error sending to [" NET_ADDR_FMT "]: %s",
						m_Id,
                        NET_ADDR_FOR_PRINTF(address),
                        netSocketErrorString(socketErr));
        }
    }

    NET_SET_SKTERR(sktErr, socketErr);

    return (NET_SOCKERR_NONE == socketErr);
}

int
netSocket::NativeReceive(netSocketAddress* sender,
                        void* buffer,
                        const int bufferSize,
                        netSocketError* sktErr)
{
    int skt = this->GetRawSocket();

    netAssert(skt >= 0);

    netSocketError socketErr = NET_SOCKERR_NONE;

    int count = 0;

    sockaddr_in sin = {0};
    socklen_t fromlen = sizeof(sin);

    count = recvfrom(skt,
                        (char*) buffer,
                        bufferSize,
                        0,
                        (sockaddr*) &sin,
                        &fromlen);

    if((0 == count) 
        || (count < 0 && ((errno == SCE_NET_EWOULDBLOCK) || (errno == SCE_NET_EAGAIN))))
    {
        count = 0;
    }
    else if(count < 0)
    {
        socketErr = netSocket::GetLastSocketError();
        netError("[%u] Error receiving: %s", m_Id, netSocketErrorString(socketErr));
    }

    if (NET_SOCKERR_NONE == socketErr
         || NET_SOCKERR_CONNECTION_RESET == socketErr)
    {
        sender->Init(netIpAddress(netIpV4Address(net_ntohl(sin.sin_addr.s_addr))), net_ntohs(sin.sin_port));

        //Ignore messages from ourself
        if(*sender == m_Addr)
        {
            count =  0;
        }
    }
    else
    {
        sender->Clear();
    }

    NET_SET_SKTERR(sktErr, socketErr);

    return count;
}

bool
netSocket::NativeSetBlocking()
{
    bool success = false;

    int skt = this->GetRawSocket();

    if(netVerify(skt >= 0))
    {
        int nonblock = this->IsBlocking() ? 0 : 1;

        success =
            netVerify(0 == setsockopt(skt, SOL_SOCKET, SCE_NET_SO_NBIO, &nonblock, sizeof(nonblock)));

        if(!success)
        {
             netError("[%u] Failed to set socket blocking mode", m_Id);
        }
    }

    return success;
}

bool
netSocket::NativeGetSendReceiveBufferSizes(unsigned& sendBufferSize,
										   unsigned& receiveBufferSize) const
{
	rtry
	{
		int sendbuff = 0;
		socklen_t optlen = sizeof(sendbuff);
		int res = getsockopt(m_Socket, SOL_SOCKET, SCE_NET_SO_SNDBUF, (char*)&sendbuff, &optlen);
		rverify(res == 0, catchall, netError("[%u] Failed to get send buffer size (LastError: %d)", m_Id, netSocket::GetLastSocketError()));

		int rcvbuff = 0;
		optlen = sizeof(rcvbuff);
		res = getsockopt(m_Socket, SOL_SOCKET, SCE_NET_SO_RCVBUF, (char*)&rcvbuff, &optlen);
		rverify(res == 0, catchall, netError("[%u] Failed to get receive buffer size (LastError: %d)", m_Id, netSocket::GetLastSocketError()));

		sendBufferSize = (unsigned)sendbuff;
		receiveBufferSize = (unsigned)rcvbuff;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netSocket::NativeSetSendReceiveBufferSizes(const unsigned sendBufferSize,
										   const unsigned receiveBufferSize)
{
	rtry
	{
		ptrdiff_t skt = this->GetRawSocket();

		rverify(skt >= 0, catchall, );

		int sendBuff = (int)sendBufferSize;
		int res = setsockopt(m_Socket, SOL_SOCKET, SO_SNDBUF, (char*)&sendBuff, sizeof(sendBuff));
		rverify(res == 0, catchall, netError("[%u] Failed to set send buffer size (LastError: %d)", m_Id, netSocket::GetLastSocketError()));

		int rcvBuff = (int)receiveBufferSize;
		res = setsockopt(m_Socket, SOL_SOCKET, SO_RCVBUF, (char*)&rcvBuff, sizeof(rcvBuff));
		rverify(res == 0, catchall, netError("[%u] Failed to set receive buffer size (LastError: %d)", m_Id, netSocket::GetLastSocketError()));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netSocket::NativeGetReceiveBufferQueueLength(unsigned& receiveBufferQueueLength) const
{
	receiveBufferQueueLength = 0;

	const int skt = this->GetRawSocket();

	if(skt >= 0)
	{
		SceNetSockInfo info;
		if(sceNetGetSockInfo(skt, &info, 1, 0) >= 0)
		{
			receiveBufferQueueLength = info.recv_queue_length;
			return true;
		}
	}

	return false;
}

void
netSocket::NativeGetPortRangeForRandomSelection(unsigned short& lowerBound, unsigned short& upperBound)
{
	// see document "Net Library Overview", section "System Reserved Port Numbers"
	lowerBound = 32768;
	upperBound = 39999;
}

void
netSocket::NativeGetPortRangeForP2pConnections(unsigned short& lowerBound, unsigned short& upperBound)
{
	lowerBound = 0;
	upperBound = 65535;
}

bool
netSocket::IsReceivePending() const
{
    bool isPending = false;

    if(this->CanSendReceive())
    {
        int skt = this->GetRawSocket();

        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(skt, &fds);

        struct timeval tv;
        tv.tv_sec   = 0;
        tv.tv_usec  = 0;

	    int ret = select(FD_SETSIZE, &fds, NULL, NULL, &tv);

        if (ret < 0)
        {            
            netError("[%u] select failed", m_Id);
        }
        else if (ret > 0)
        {
            isPending = true;
        }
    }

    return isPending;
}

}   //namespace rage

#endif // RSG_SCE
