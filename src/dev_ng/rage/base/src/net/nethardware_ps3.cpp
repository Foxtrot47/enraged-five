// 
// net/nethardware_ps3.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_PS3

#include "nethardware.h"
#include "netdiag.h"
#include "netsocket.h"
#include "file/winsock.h"
#include "grcore/setup.h"

#include "system/ipc.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/timer.h"

#include <arpa/inet.h>
#include <netex/net.h>
#include <netex/libnetctl.h>
#include <netinet/in.h>
#include <np.h>
#include <sys/socket.h>
#include <sysutil/sysutil_common.h>

#ifdef __SNC__
#pragma diag_suppress 552
#endif

#pragma comment(lib,"sysutil_stub")

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(ragenet, hardware)
#undef __rage_channel
#define __rage_channel ragenet_hardware

int s_CellNetCtlState = -1;
int s_CellNetCtlHandlerId = -1;

//Helper function defined in winsock.cpp
const char* netGetCellNetErrorString(const int code);

//Handler for connection state transitions, as reported by OS.
void NetCtlHandler(int /*prev_state*/,
                     int new_state,
                     int /*event*/,
                     int /*error_code*/,
                     void* /*arg*/)
{
    s_CellNetCtlState = new_state;

    switch(new_state)
    {
    case CELL_NET_CTL_STATE_Disconnected:
        netDebug2("Hardware connection state changed to Disconnected");
        break;

    case CELL_NET_CTL_STATE_Connecting:
        netDebug2("Hardware connection state changed to Connecting");
        break;

    case CELL_NET_CTL_STATE_IPObtaining:
        netDebug2("Hardware connection state changed to IPObtaining");
        break;

    case CELL_NET_CTL_STATE_IPObtained:
        netDebug2("Hardware connection state changed to IPObtained");
        break;

    default:
	    netError("Unknown connection state");
    }
}

static unsigned s_NextCellNetCtlGetInfoTime = 0;

bool
netHardware::IsLinkConnected()
{
    bool connected = false;

    SYS_CS_SYNC(sm_Cs);

    //cellNetCtlGetInfo is expensive (.5 ms!) so only call it periodically.

    //Interval between calls to cellNetCtlGetInfo
    static const unsigned CTL_GET_INFO_INTERVAL = 1000;    //milliseconds
    static CellNetCtlInfo s_LastInfo;
    static int s_LastError = CELL_NET_CTL_ERROR_NET_NOT_CONNECTED;

    const unsigned curTime = sysTimer::GetSystemMsTime() | 0x01;

    if(0 == s_NextCellNetCtlGetInfoTime
        || int(curTime - s_NextCellNetCtlGetInfoTime) >= 0)
    {
        s_LastError = cellNetCtlGetInfo(CELL_NET_CTL_INFO_LINK, &s_LastInfo);

        if(s_LastError >= 0)
        {
            s_NextCellNetCtlGetInfoTime = (curTime + CTL_GET_INFO_INTERVAL) | 0x01;
        }
        else
        {
            s_NextCellNetCtlGetInfoTime = 0;
            netError("Error getting link connection state:0x%08x %s",
                        s_LastError,
                        netGetCellNetErrorString(s_LastError));
        }
    }

    if(s_LastError >= 0)
    {
        u8 mac[6];
        netIpAddress ip;
        connected = CELL_NET_CTL_LINK_CONNECTED == s_LastInfo.link
                    && netHardware::GetMacAddress(mac)
                    && netHardware::GetLocalIpAddress(&ip);
    }
    else
    {
    }

    return connected;
}

//private:

bool
netHardware::NativeInit()
{
    s_CellNetCtlHandlerId = s_CellNetCtlState = -1;
    return true;
}

void
netHardware::NativeShutdown()
{
    NativeStopNetwork();
    s_CellNetCtlHandlerId = s_CellNetCtlState = -1;
}

bool
netHardware::NativeStartNetwork()
{
    SYS_CS_SYNC(sm_Cs);

    bool success = false;

    s_NextCellNetCtlGetInfoTime = 0;

    if(netVerify(STATE_STOPPED == sm_State)
        && IsLinkConnected())
    {
        sm_State = STATE_STARTING;

        s_CellNetCtlHandlerId = s_CellNetCtlState = -1;
        int errCode;

        InitWinSock();

        const int RESOLVER_RETRANSMIT_DELAY_SECS = 1;
        const int MAX_RESOLVER_RETRIES = 3;

        //Do an immediate check for CELL_NET_CTL_STATE_IPObtained, because
        //sometimes the PS3 can startup initialized (at least in development).
        if(0 != (errCode = cellNetCtlGetState(&s_CellNetCtlState)))
        {
            netError("Error calling cellNetCtlGetState():0x%08x %s",
                        errCode,
                        netGetCellNetErrorString(errCode));
        }
        //The PS3 network startup goes through multiple states, and we can't be
        //considered "available" until we are in the IPObtained state.
        //We register a callback to detect the transitions.
        else if(0 != (errCode = cellNetCtlAddHandler(NetCtlHandler, NULL, &s_CellNetCtlHandlerId)))
        {
	        netError("Error calling cellNetCtlAddHandler():0x%08x %s",
                        errCode,
                        netGetCellNetErrorString(errCode));
        }
        else if(0 != (errCode = sys_net_set_resolver_configurations(RESOLVER_RETRANSMIT_DELAY_SECS, MAX_RESOLVER_RETRIES, 0)))
        {
	        netError("Error calling sys_net_set_resolver_configurations():0x%08x %s",
                      errCode,
                      netGetCellNetErrorString(errCode));
        }
        else
        {
            success = true;
        }

        if(!success)
        {
            NativeStopNetwork();
        }
    }

	return success;
}

bool
netHardware::NativeStopNetwork()
{
    SYS_CS_SYNC(sm_Cs);

    if(STATE_STOPPED != sm_State)
    {
        int errCode;

        if(0 != (errCode = cellNetCtlDelHandler(s_CellNetCtlHandlerId)))
        {
		    netError("Error calling cellNetCtlDelHandler():0x%08x %s",
                    errCode,
                    netGetCellNetErrorString(errCode));
        }
        else
        {
            ShutdownWinSock();
        }

        s_CellNetCtlHandlerId = s_CellNetCtlState = -1;

        sm_NetInfo.Clear();

        sm_State = STATE_STOPPED;
    }

	return true;
}

void
netHardware::NativeUpdate()
{
    SYS_CS_SYNC(sm_Cs);

    cellSysutilCheckCallback();

    if (rage::grcSetupInstance && rage::grcSetupInstance->WantExit())
        while (1)
            sysIpcSleep(1000);

    if(STATE_STOPPED != sm_State && !IsLinkConnected())
    {
        netWarning("Cable disconnected!");
        NativeStopNetwork();
    }
    else if(STATE_STOPPED == sm_State && IsLinkConnected())
    {
        NativeStartNetwork();
    }
    else if(STATE_STARTING == sm_State
            && CELL_NET_CTL_STATE_IPObtained == s_CellNetCtlState)
    {
        sm_State = STATE_AVAILABLE;
    }
}

netNatType
netHardware::NativeGetNatType()
{
    netNatType natType = NET_NAT_UNKNOWN;

    CellNetCtlNatInfo natInfo = {0};
    natInfo.size = sizeof(natInfo);

    const int err = cellNetCtlGetNatInfo(&natInfo);

    if(err >= 0)
    {
        if(CELL_NET_CTL_NATINFO_STUN_UNCHECKED == natInfo.stun_status)
        {
            netWarning("Error getting nat type (CELL_NET_CTL_NATINFO_STUN_UNCHECKED)");
        }
        else if(CELL_NET_CTL_NATINFO_STUN_FAILED == natInfo.stun_status)
        {
            netWarning("Error getting nat type (CELL_NET_CTL_NATINFO_STUN_FAILED)");
        }
        else if(CELL_NET_CTL_NATINFO_STUN_OK == natInfo.stun_status)
        {
            switch(natInfo.nat_type)
            {
            case CELL_NET_CTL_NATINFO_NAT_TYPE_1: natType = NET_NAT_OPEN; break;
            case CELL_NET_CTL_NATINFO_NAT_TYPE_2: natType = NET_NAT_MODERATE; break;
            case CELL_NET_CTL_NATINFO_NAT_TYPE_3: natType = NET_NAT_STRICT; break;
            default: natType = NET_NAT_UNKNOWN; break;
            }
        }
    }
    else
    {
        netError("Error calling cellNetCtlGetNatInfo():0x%08x %s",
                    err,
                    netGetCellNetErrorString(err));
    }

    return natType;
}

bool
netHardware::NativeGetMacAddress( u8 (&mac)[6])
{
    bool success = false;
    CellNetCtlInfo cell_info;
    int err = cellNetCtlGetInfo(CELL_NET_CTL_INFO_ETHER_ADDR, &cell_info);

    if(err < 0)
    {
        if(CELL_NET_CTL_ERROR_NOT_AVAIL != err)
        {
            netError("Error calling cellNetCtlGetInfo(): %08x", err);
        }
    }
    else
    {
        CompileTimeAssert(sizeof(mac) == CELL_NET_CTL_ETHER_ADDR_LEN);
        sysMemCpy(mac, cell_info.ether_addr.data, CELL_NET_CTL_ETHER_ADDR_LEN);

        success = true;
    }

    return success;
}

bool
netHardware::NativeGetLocalIpAddress(netIpAddress* ip)
{
    bool success = false;
    CellNetCtlInfo ctl_info;
    struct in_addr temp_addr;

    int err = cellNetCtlGetInfo(CELL_NET_CTL_INFO_IP_ADDRESS, &ctl_info);

    if(err < 0)
    {
        if(CELL_NET_CTL_ERROR_NOT_AVAIL != err)
        {
            netError("Error calling cellNetCtlGetInfo(CELL_NET_CTL_INFO_IP_ADDRESS):0x%08x", err);
        }
    }
    else if((err = inet_pton(AF_INET, ctl_info.ip_address, &temp_addr)) < 0)
    {
        netError("Error calling inet_pton():0x%08x", err);
    }
    else
    {
        *ip = netIpAddress(netIpV4Address(ntohl(temp_addr.s_addr)));
        success = ip->IsValid();
    }

    return success;
}

static void NpSignalingHandler(u32 /*ctxId*/,
                                u32 /*subjectId*/,
                                int /*eventId*/,
                                int /*errorCode*/,
                                void* /*arg*/)
{
}

bool
netHardware::NativeGetPublicIpAddress(netIpAddress* ip)
{
    bool success = false;
    SceNpId npId;
    SceNpSignalingNetInfo sigInfo;
    u32 ctxId;
    int err;
    
    sigInfo.size = sizeof(sigInfo);

    if(0 > (err = sceNpManagerGetNpId(&npId)))
    {
        //Not online yet;
    }
    else if(0 > (err = sceNpSignalingCreateCtx(&npId,
                                                NpSignalingHandler,
                                                NULL,
                                                &ctxId)))
    {
        netError("sceNpSignalingCreateCtx failed:0x%08x", err);
    }
    else if(0 > (err = sceNpSignalingGetLocalNetInfo(ctxId, &sigInfo)))
    {
        netError("sceNpSignalingGetLocalNetInfo failed:0x%08x", err);
    }
    else
    {
        sceNpSignalingDestroyCtx(ctxId);
        *ip = netIpAddress(netIpV4Address(ntohl(sigInfo.mapped_addr.s_addr)));
        success = ip->IsValid();
    }

    return success;
}

}   //namespace rage

#endif  //RSG_PS3
