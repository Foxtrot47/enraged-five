// 
// net/netsocket_ps3.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_PS3

#include "netsocket.h"

#include "diag/seh.h"
#include "netdiag.h"
#include "nethardware.h"

#include <errno.h>
#include <arpa/inet.h>
#include <netex/errno.h>
#include <netex/net.h>
#include <netinet/in.h>     //NOTE: Must come before sockinfo.h, because it doesn't include it itself.
#include <netex/sockinfo.h>
#include <netex/libnetctl.h>
#include <np/common.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <system/memory.h>
#include <system/nelem.h>

#define NET_SET_SKTERR(se, e)   while((se)){*(se) = (e);break;}

namespace rage
{

const char* netCellGetErrorString(const int err);

netSocketError
netSocket::GetLastSocketError()
{
    netSocketError err = NET_SOCKERR_NONE;

    const int lasterror = sys_net_errno;

    switch(lasterror)
    {
        case 0:
            break;
        case SYS_NET_ENETDOWN:
            err = NET_SOCKERR_NETWORK_DOWN;
            break;
        case SYS_NET_ENOBUFS:
            err = NET_SOCKERR_NO_BUFFERS;
            break;
        case SYS_NET_EMSGSIZE:
            err = NET_SOCKERR_MESSAGE_TOO_LONG;
            break;
        case SYS_NET_EADDRNOTAVAIL:     //Invalid address was specified
        case SYS_NET_ENETUNREACH:       //Destination is unreachable
        case SYS_NET_EHOSTDOWN:         //Other end is down and unreachable
        case SYS_NET_EHOSTUNREACH:      //Network unreachable
            err = NET_SOCKERR_HOST_UNREACHABLE;
            break;
        case SYS_NET_ECONNRESET:
            err = NET_SOCKERR_CONNECTION_RESET;
            break;
        case SYS_NET_EWOULDBLOCK:
            err = NET_SOCKERR_WOULD_BLOCK;
            break;
        case SYS_NET_EINPROGRESS:
            err = NET_SOCKERR_IN_PROGRESS;
            break;
        default:
            netError("sys_net_errno = %d", lasterror);
            err = NET_SOCKERR_UNKNOWN;
            break;
    }

    return err;
}

unsigned
netSocket::SizeofHeader()
{
    static const unsigned SIZEOF_IP_HEADER          = 20;
    static const unsigned SIZEOF_UDP_HEADER         = 8;
    static const unsigned SIZEOF_UDPP2P_HEADER      = 8;
    static const unsigned SIZEOF_UDPP2P_CRYPTO      = 8;
    static const unsigned SIZEOF_UDPP2P_SIGNATURE   = 8;

    return SIZEOF_IP_HEADER + SIZEOF_UDP_HEADER +
        ((this->GetProtocol() == NET_PROTO_UDPP2P)
        ? (SIZEOF_UDPP2P_HEADER + SIZEOF_UDPP2P_CRYPTO + SIZEOF_UDPP2P_SIGNATURE)
        : 0);
}

void
netSocket::SetCryptoEnabled(const bool enabled)
{
    if(NET_PROTO_UDPP2P == this->GetProtocol())
    {
        if(enabled != m_CryptoEnabled)
        {
            if(m_Socket < 0)
            {
                // if the socket is not bound yet set the crypto flag to the
                // specified value so the correct encryption mode is set when
                // the socket is bound
                m_CryptoEnabled = enabled;
            }
            else
            {
                int optval = enabled ? 1 : 0;
                if(!netVerify(0 == setsockopt(m_Socket,
                                                SOL_SOCKET,
                                                SO_USECRYPTO,
                                                &optval,
                                                sizeof(optval))))
                {
                    netError("setsockopt(SO_USECRYPTO) failed (0x%08x)\n",
                                sys_net_errno);
                }
                else
                {
                    optval = enabled ? 1 : 0;
                    if(!netVerify(0 == setsockopt(m_Socket,
                                                    SOL_SOCKET,
                                                    SO_USESIGNATURE,
                                                    &optval,
                                                    sizeof(optval))))
                    {
                        netError("setsockopt(SO_USESIGNATURE) failed (0x%08x)\n",
                                    sys_net_errno);
                    }
                    else
                    {
                        netDebug2("Encryption/authentication %s on socket",
                                    enabled ? "enabled" : "disabled");
                        m_CryptoEnabled = enabled;
                    }
                }
            }
        }
    }
    else
    {
        netWarning("Crypto cannot be enabled on non-UDPP2P sockets");
    }
}

bool
netSocket::IsCryptoEnabled() const
{
    return m_CryptoEnabled && (this->GetProtocol() == NET_PROTO_UDPP2P);
}

unsigned
netSocket::SizeofPacket(const unsigned sizeofPayload)
{
    return this->SizeofHeader() + sizeofPayload;
}

//protected:

bool
netSocket::NativeGetMyAddress(netSocketAddress* addr) const
{
    bool success = false;

    addr->Clear();

    rtry
    {
        int skt = this->GetRawSocket();

        rverify(skt >= 0, catchall,);

        unsigned port = 0;

        if(NET_PROTO_UDP == this->GetProtocol())
        {
            //sys_net_get_sockinfo() can get the port, but can't get the IP.
            //cellNetCltGetInfo() can get the IP, but not the port.
            //So, we use both together.

            struct sys_net_sockinfo info;
            sysMemSet(&info, 0, sizeof(info));

            rcheck(0 <= sys_net_get_sockinfo(skt, &info, 1),
                   catchall,
                   netError("netSocket::UpdateMyAddress(): sys_net_get_sockinfo failed"));

            port = net_ntohs(info.local_port);
            //Can't get the IP here - it's zero.
        }
        else
        {
            sockaddr_in_p2p sinp2p = {0};
            socklen_t sin_len = sizeof(sinp2p);

            rverify(0 == getsockname(skt, (sockaddr*) &sinp2p, &sin_len),
                    catchall,
                    netError("Error calling getsockname"));

            port = net_ntohs(sinp2p.sin_vport);
            //Can't get the IP here - it's zero.
        }

        netIpAddress ip;
        rcheck(netHardware::GetLocalIpAddress(&ip),
                catchall,
                netError("Error retrieving local IP address"));

        addr->Init(ip, port);
        success = true;
    }
    rcatchall
    {
    }

    return success;
}

void
netSocket::NativeShutdown()
{
    this->NativeUnbind();
}

bool
netSocket::NativeBind()
{
    SYS_CS_SYNC(m_Cs);

    netAssert(netHardware::IsAvailable());
    netAssert(m_Socket < 0);
    netAssert((NET_PROTO_UDP == GetProtocol()) || (NET_PROTO_UDPP2P == GetProtocol()));

    netDebug("Binding socket on port %d...", m_Port);

    rtry
    {
        //Create the socket used for all communication        
        if (NET_PROTO_UDP == GetProtocol())
        {
            m_Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

            rcheck(m_Socket >= 0, catchall, netError("socket() failed"));

            //Bind the socket to the requested port and whatever address
            //is given to us from the OS.

            //Bind to INADDR_ANY (i.e. any IP).
            sockaddr_in addr		= {0};
            addr.sin_family			= AF_INET;
            addr.sin_addr.s_addr	= INADDR_ANY;
            addr.sin_port			= net_htons(m_Port);

            int result = bind(m_Socket, (const sockaddr*) &addr, sizeof(addr));

            rcheck(result == 0, 
                   catchall, 
                   netError("Failed to bind UDP on port %d (0x%08x:%s)",
                                m_Port,
                                result,
                                netCellGetErrorString(result)));
        }
        else if (NET_PROTO_UDPP2P == GetProtocol())
        {
            m_Socket = socket(AF_INET, SOCK_DGRAM_P2P, IPPROTO_IP);

            rcheck(m_Socket >= 0, catchall, netError("socket() failed"));

            //Ensure the current crypto state is applied to the
            //socket parameters.
            m_CryptoEnabled = !m_CryptoEnabled;
            this->SetCryptoEnabled(!m_CryptoEnabled);

            //Bind to INADDR_ANY (i.e. any IP).
            sockaddr_in_p2p sinp2p  = {0};
	        sinp2p.sin_family       = AF_INET;
            sinp2p.sin_addr.s_addr  = INADDR_ANY;
            sinp2p.sin_port         = net_htons(SCE_NP_PORT);
            sinp2p.sin_vport        = net_htons(m_Port); //Virtual port

            int result = bind(m_Socket, (const sockaddr*) &sinp2p, sizeof(sinp2p));

            rcheck(result == 0, 
                   catchall, 
                   netError("Failed to bind UDPP2P on port %d (0x%08x:%s)",
                                m_Port,
                                result,
                                netCellGetErrorString(result)));
        }
        else
        {
            rthrow(catchall, netError("Unhandled protocol"));
        }

        rverify(this->NativeGetMyAddress(&m_Addr), catchall,);

        m_Port = m_Addr.GetPort();

        netDebug2("I am at " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(m_Addr));

        //Enable broadcasting
        int broadcast = 1;

        rcheck(0 == setsockopt(m_Socket, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)),
                catchall,
                netError("Failed to set socket options (broadcast)"));

        m_CanBroadcast = broadcast ? true : false;

        //Set the blocking mode
        rverify(this->NativeSetBlocking(),
                 catchall,
                 netError("Failed to set socket blocking mode"));
    }
    rcatchall
    {
        if(m_Socket >= 0)
        {
            socketclose(m_Socket);
        }

        m_Socket = -1;
    }

    return (m_Socket >= 0);
}

void
netSocket::NativeUnbind()
{
    SYS_CS_SYNC(m_Cs);

    netDebug("Unbinding socket from port %d...", m_Port);

    if(m_Socket >= 0)
    {
        const int skt = m_Socket;
        m_Socket = -1;
        socketclose(skt);
    }
}

bool
netSocket::NativeSend(const netSocketAddress& address,
                     const void* buffer,
                     const unsigned bufferSize,
                     netSocketError* sktErr)
{
    int skt = this->GetRawSocket();

    netAssert(skt >= 0);
    netAssert(bufferSize <= MAX_BUFFER_SIZE);

    netSocketError socketErr = NET_SOCKERR_NONE;

    if(address == m_Addr)
    {
        netWarning("Why am I sending a message to myself???");
    }

    int result = 0;

    if(NET_PROTO_UDP == GetProtocol())
    {
        sockaddr_in sin = {0};

        sin.sin_addr.s_addr = net_htonl(address.GetIp().ToV4().ToU32());
        sin.sin_port = net_htons(address.GetPort());
        sin.sin_family = AF_INET;

        result = sendto(skt,
                         (const char*) buffer,
                         bufferSize,
                         0,
                         (const struct sockaddr*) &sin,
                         sizeof(sin));
    }
    else if(NET_PROTO_UDPP2P == GetProtocol())
    {
        struct sockaddr_in_p2p sinp2p = {0};

        sinp2p.sin_family       = AF_INET;
        sinp2p.sin_addr.s_addr  = net_htonl(address.GetIp().ToV4().ToU32());
        sinp2p.sin_port         = net_htons(address.GetPort());
        sinp2p.sin_vport        = net_htons(this->GetPort());

        result = sendto(skt, 
                         (const char*) buffer,
                         bufferSize,
                         (m_CryptoEnabled ? (MSG_USECRYPTO | MSG_USESIGNATURE) : 0),
                         (struct sockaddr *)&sinp2p, 
                         sizeof(sinp2p));
    }

    if(result < 0)
    {
        //Ignore SYS_NET_ERROR_EWOULDBLOCK
        if(sys_net_errno != SYS_NET_ERROR_EWOULDBLOCK)
        {
            socketErr = netSocket::GetLastSocketError();
            netError("Error sending to [" NET_ADDR_FMT "]: %s",
                        NET_ADDR_FOR_PRINTF(address),
                        netSocketErrorString(socketErr));
        }
    }

    NET_SET_SKTERR(sktErr, socketErr);

    return (NET_SOCKERR_NONE == socketErr);
}

int
netSocket::NativeReceive(netSocketAddress* sender,
                        void* buffer,
                        const int bufferSize,
                        netSocketError* sktErr)
{
    int skt = this->GetRawSocket();

    netAssert(skt >= 0);

    netSocketError socketErr = NET_SOCKERR_NONE;

    int count = 0;

    sockaddr_in sin = {0};
    if (NET_PROTO_UDP == GetProtocol())
    {
        socklen_t fromlen = sizeof(sin);

        count = recvfrom(skt,
                         (char*) buffer,
                         bufferSize,
                         0,
                         (sockaddr*) &sin,
                         &fromlen);
    }
    else if (NET_PROTO_UDPP2P == GetProtocol())
    {
    	struct sockaddr_in_p2p sinp2p;
        socklen_t sinp2plen = sizeof(sinp2p);

        count = recvfrom(skt,
                          (char*) buffer,
                          bufferSize,
                          0,
                          (struct sockaddr*)&sinp2p,
                          &sinp2plen);

        sin.sin_addr = sinp2p.sin_addr;
        sin.sin_port = sinp2p.sin_port;
    }

    if((0 == count) 
        || (count < 0 && ((sys_net_errno == SYS_NET_EWOULDBLOCK) || (sys_net_errno == SYS_NET_EAGAIN))))
    {
        count = 0;
    }
    else if(count < 0)
    {
        socketErr = netSocket::GetLastSocketError();
        netError("Error receiving: %s", netSocketErrorString(socketErr));
    }

    if (NET_SOCKERR_NONE == socketErr
         || NET_SOCKERR_CONNECTION_RESET == socketErr)
    {
        sender->Init(netIpAddress(netIpV4Address(net_ntohl(sin.sin_addr.s_addr))), net_ntohs(sin.sin_port));

        //Ignore messages from ourself
        if(*sender == m_Addr)
        {
            count =  0;
        }
    }
    else
    {
        sender->Clear();
    }

    NET_SET_SKTERR(sktErr, socketErr);

    return count;
}

bool
netSocket::NativeSetBlocking()
{
    bool success = false;

    int skt = this->GetRawSocket();

    if(netVerify(skt >= 0))
    {
        int nonblock = this->IsBlocking() ? 0 : 1;

        success =
            netVerify(0 == setsockopt(skt, SOL_SOCKET, SO_NBIO, &nonblock, sizeof(nonblock)));

        if(!success)
        {
             netError("Failed to set socket blocking mode");
        }
    }

    return success;
}

bool
netSocket::IsReceivePending() const
{
    bool isPending = false;

    if(this->CanSendReceive())
    {
        int skt = this->GetRawSocket();

        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(skt, &fds);

        struct timeval tv;
        tv.tv_sec   = 0;
        tv.tv_usec  = 0;

	    int ret = socketselect(FD_SETSIZE, &fds, NULL, NULL, &tv);

        if (ret < 0)
        {            
            netError("socketselect failed");
        }
        else if (ret > 0)
        {
            isPending = true;
        }
    }

    return isPending;
}

#if !__NO_OUTPUT
const char* netCellGetErrorString(const int err)
#else
const char* netCellGetErrorString(const int /*err*/)
#endif  //__NO_OUTPUT
{
#if !__NO_OUTPUT
#define CELLERR_CASE(a) case a: return #a;

    switch(err)
    {
        CELLERR_CASE(SYS_NET_ERROR_EPERM);
        CELLERR_CASE(SYS_NET_ERROR_ENOENT);
        CELLERR_CASE(SYS_NET_ERROR_ESRCH);
        CELLERR_CASE(SYS_NET_ERROR_EINTR);
        CELLERR_CASE(SYS_NET_ERROR_EIO);
        CELLERR_CASE(SYS_NET_ERROR_ENXIO);
        CELLERR_CASE(SYS_NET_ERROR_E2BIG);
        CELLERR_CASE(SYS_NET_ERROR_ENOEXEC);
        CELLERR_CASE(SYS_NET_ERROR_EBADF);
        CELLERR_CASE(SYS_NET_ERROR_ECHILD);
        CELLERR_CASE(SYS_NET_ERROR_EDEADLK);
        CELLERR_CASE(SYS_NET_ERROR_ENOMEM);
        CELLERR_CASE(SYS_NET_ERROR_EACCES);
        CELLERR_CASE(SYS_NET_ERROR_EFAULT);
        CELLERR_CASE(SYS_NET_ERROR_ENOTBLK);
        CELLERR_CASE(SYS_NET_ERROR_EBUSY);
        CELLERR_CASE(SYS_NET_ERROR_EEXIST);
        CELLERR_CASE(SYS_NET_ERROR_EXDEV);
        CELLERR_CASE(SYS_NET_ERROR_ENODEV);
        CELLERR_CASE(SYS_NET_ERROR_ENOTDIR);
        CELLERR_CASE(SYS_NET_ERROR_EISDIR);
        CELLERR_CASE(SYS_NET_ERROR_EINVAL);
        CELLERR_CASE(SYS_NET_ERROR_ENFILE);
        CELLERR_CASE(SYS_NET_ERROR_EMFILE);
        CELLERR_CASE(SYS_NET_ERROR_ENOTTY);
        CELLERR_CASE(SYS_NET_ERROR_ETXTBSY);
        CELLERR_CASE(SYS_NET_ERROR_EFBIG);
        CELLERR_CASE(SYS_NET_ERROR_ENOSPC);
        CELLERR_CASE(SYS_NET_ERROR_ESPIPE);
        CELLERR_CASE(SYS_NET_ERROR_EROFS);
        CELLERR_CASE(SYS_NET_ERROR_EMLINK);
        CELLERR_CASE(SYS_NET_ERROR_EPIPE);
        CELLERR_CASE(SYS_NET_ERROR_EDOM);
        CELLERR_CASE(SYS_NET_ERROR_ERANGE);
        //CELLERR_CASE(SYS_NET_ERROR_EAGAIN);   duplicate of SYS_NET_ERROR_EWOULDBLOCK
        CELLERR_CASE(SYS_NET_ERROR_EWOULDBLOCK);
        CELLERR_CASE(SYS_NET_ERROR_EINPROGRESS);
        CELLERR_CASE(SYS_NET_ERROR_EALREADY);
        CELLERR_CASE(SYS_NET_ERROR_ENOTSOCK);
        CELLERR_CASE(SYS_NET_ERROR_EDESTADDRREQ);
        CELLERR_CASE(SYS_NET_ERROR_EMSGSIZE);
        CELLERR_CASE(SYS_NET_ERROR_EPROTOTYPE);
        CELLERR_CASE(SYS_NET_ERROR_ENOPROTOOPT);
        CELLERR_CASE(SYS_NET_ERROR_EPROTONOSUPPORT);
        CELLERR_CASE(SYS_NET_ERROR_ESOCKTNOSUPPORT);
        CELLERR_CASE(SYS_NET_ERROR_EOPNOTSUPP);
        CELLERR_CASE(SYS_NET_ERROR_EPFNOSUPPORT);
        CELLERR_CASE(SYS_NET_ERROR_EAFNOSUPPORT);
        CELLERR_CASE(SYS_NET_ERROR_EADDRINUSE);
        CELLERR_CASE(SYS_NET_ERROR_EADDRNOTAVAIL);
        CELLERR_CASE(SYS_NET_ERROR_ENETDOWN);
        CELLERR_CASE(SYS_NET_ERROR_ENETUNREACH);
        CELLERR_CASE(SYS_NET_ERROR_ENETRESET);
        CELLERR_CASE(SYS_NET_ERROR_ECONNABORTED);
        CELLERR_CASE(SYS_NET_ERROR_ECONNRESET);
        CELLERR_CASE(SYS_NET_ERROR_ENOBUFS);
        CELLERR_CASE(SYS_NET_ERROR_EISCONN);
        CELLERR_CASE(SYS_NET_ERROR_ENOTCONN);
        CELLERR_CASE(SYS_NET_ERROR_ESHUTDOWN);
        CELLERR_CASE(SYS_NET_ERROR_ETOOMANYREFS);
        CELLERR_CASE(SYS_NET_ERROR_ETIMEDOUT);
        CELLERR_CASE(SYS_NET_ERROR_ECONNREFUSED);
        CELLERR_CASE(SYS_NET_ERROR_ELOOP);
        CELLERR_CASE(SYS_NET_ERROR_ENAMETOOLONG);
        CELLERR_CASE(SYS_NET_ERROR_EHOSTDOWN);
        CELLERR_CASE(SYS_NET_ERROR_EHOSTUNREACH);
        CELLERR_CASE(SYS_NET_ERROR_ENOTEMPTY);
        CELLERR_CASE(SYS_NET_ERROR_EPROCLIM);
        CELLERR_CASE(SYS_NET_ERROR_EUSERS);
        CELLERR_CASE(SYS_NET_ERROR_EDQUOT);
        CELLERR_CASE(SYS_NET_ERROR_ESTALE);
        CELLERR_CASE(SYS_NET_ERROR_EREMOTE);
        CELLERR_CASE(SYS_NET_ERROR_EBADRPC);
        CELLERR_CASE(SYS_NET_ERROR_ERPCMISMATCH);
        CELLERR_CASE(SYS_NET_ERROR_EPROGUNAVAIL);
        CELLERR_CASE(SYS_NET_ERROR_EPROGMISMATCH);
        CELLERR_CASE(SYS_NET_ERROR_EPROCUNAVAIL);
        CELLERR_CASE(SYS_NET_ERROR_ENOLCK);
        CELLERR_CASE(SYS_NET_ERROR_ENOSYS);
        CELLERR_CASE(SYS_NET_ERROR_EFTYPE);
        CELLERR_CASE(SYS_NET_ERROR_EAUTH);
        CELLERR_CASE(SYS_NET_ERROR_ENEEDAUTH);
        CELLERR_CASE(SYS_NET_ERROR_EIDRM);
        CELLERR_CASE(SYS_NET_ERROR_ENOMSG);
        CELLERR_CASE(SYS_NET_ERROR_EOVERFLOW);
        CELLERR_CASE(SYS_NET_ERROR_EILSEQ);
        CELLERR_CASE(SYS_NET_ERROR_ENOTSUP);
        CELLERR_CASE(SYS_NET_ERROR_ECANCELED);
        CELLERR_CASE(SYS_NET_ERROR_EBADMSG);
        CELLERR_CASE(SYS_NET_ERROR_ENODATA);
        CELLERR_CASE(SYS_NET_ERROR_ENOSR);
        CELLERR_CASE(SYS_NET_ERROR_ENOSTR);
        CELLERR_CASE(SYS_NET_ERROR_ETIME);
    }
#endif  //__NO_OUTPUT

    return "Unknown cell network error";
}

}   //namespace rage

#endif  //RSG_PS3
