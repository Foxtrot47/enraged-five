// 
// net/nethardware_pc.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if !defined(_WIN32_WINNT)
#define _WIN32_WINNT	0x502
#endif

#include "file/file_config.h"
#if (RSG_PC || RSG_DURANGO)

#include "nethardware.h"
#include "natdetector.h"
#include "netdiag.h"
#include "netsocket.h"
#include "file/winsock.h"

#if RSG_DURANGO
#include "net/durango/xblnet.h"
#endif

#pragma comment(lib, "Rpcrt4.lib")

#define STRICT
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma warning(pop)

namespace rage
{

RAGE_DECLARE_CHANNEL(ragenet_hardware);
#undef __rage_channel
#define __rage_channel ragenet_hardware

static netIpAddress sm_PublicIp;

bool 
netHardware::IsLinkConnected()
{
#if RSG_DURANGO
	return netXbl::IsLinkConnected();
#else
	return true;
#endif
}

bool
netHardware::NativeStartNetwork()
{
	SYS_CS_SYNC(sm_Cs);

	bool success = false;

	if(netVerify(STATE_STOPPED == sm_State)
		&& IsLinkConnected())
	{
		sm_State = STATE_STARTING;

		InitWinSock();

		success = true;
	}
	
	return success;
}

bool
netHardware::NativeStopNetwork()
{
	SYS_CS_SYNC(sm_Cs);

	if(STATE_STOPPED != sm_State)
	{
		ShutdownWinSock();
		sm_State = STATE_STOPPED;
	}

	sm_NetInfo.Clear();

	return true;
}

void
netHardware::NativeUpdate()
{
	SYS_CS_SYNC(sm_Cs);

	if(STATE_STOPPED != sm_State && !IsLinkConnected())
	{
		netWarning("Cable disconnected!");
		NativeStopNetwork();
	}
	else if(STATE_STOPPED == sm_State && IsLinkConnected())
	{
		NativeStartNetwork();
	}
	else if(STATE_STARTING == sm_State)
	{
		sm_State = STATE_AVAILABLE;
	}
}

//private:

bool
netHardware::NativeInit()
{
#if RSG_DURANGO
	return netXbl::Init();
#else
    return true;
#endif
}

void
netHardware::NativeShutdown()
{
    NativeStopNetwork();

#if RSG_DURANGO
	netXbl::Shutdown();
#endif
}

netNatType
netHardware::NativeGetNatType()
{
	return netNatDetector::GetLocalNatType();
}

bool
netHardware::NativeGetMacAddress( u8 (&mac)[6])
{
	bool success = false;

#if RSG_DURANGO
	// MS has no plans to expose the MAC address on Durango as of November 2013:
	// https://forums.xboxlive.com/AnswerPage.aspx?qid=436b2b41-5ff0-45cf-a7e1-516bd9eb88a3&tgt=1
	memset(mac, 0, sizeof(mac));
#else
	UUID uuid;
	if(RPC_S_OK == UuidCreateSequential(&uuid)) // Ask OS to create UUID
	{
		for (int i=2; i<8; i++)  // Bytes 2 through 7 inclusive are MAC address
		{
			mac[i - 2] = uuid.Data4[i];
		}

		success = true;
	}
#endif
	return success;
}

bool
netHardware::NativeGetLocalIpAddress(netIpAddress* ip)
{
	netAssert(ip);

	bool success = false;
	ip->Clear();

	ADDRINFO* addrInfoList = NULL;
	ADDRINFO* addrInfo = NULL;

	// Note: on Windows, host names (often the PC name) can contain unicode characters. 
	// The standard gethostname() will succeed, but give back strings with "?" on those
	// systems, and GetHostNameW() doesn't exist on Windows 7 and below.
	// Pass an empty string to getaddrinfo(), which is a special case on Windows:
	// "If the pNodeName parameter contains an empty string, all registered addresses on
	// the local computer are returned."

	char myname[NET_MAX_HOSTNAME_CHARS] = {0};
	int res = gethostname(myname, sizeof(myname));

	if(!netVerify(res == 0))
	{
		netError("gethostname failed (returned %d). Host name: %s. LastError: %d. "
				 "Attempting empty hostname...", res, myname, WSAGetLastError());
		memset(myname, 0, sizeof(myname));
	}

	ADDRINFO hints;
	memset(&hints, 0, sizeof (hints));
#if IPv4_IPv6_DUAL_STACK
	hints.ai_family = AF_INET6;			// IPv6 address family
	hints.ai_flags = AI_V4MAPPED;		// if no IPv6 address exists, ask for IPv4-mapped IPv6 addresses
#else
	hints.ai_family = AF_INET;			// IPv4 address family
	hints.ai_flags = 0;
#endif

	// DNS lookup to map our host name to an IP
	res = getaddrinfo(myname, NULL, &hints, &addrInfoList);
	if(!netVerify(res == 0))
	{
		netError("getaddrinfo failed (returned %d) with host name: %s. LastError: %d. "
				 "Attempting empty hostname...", res, myname, WSAGetLastError());
		memset(myname, 0, sizeof(myname));
		res = getaddrinfo(myname, NULL, &hints, &addrInfoList);
		if(!netVerify(res == 0))
		{
			netError("getaddrinfo failed (returned %d) with host name: %s. LastError: %d.",
					 res, myname, WSAGetLastError());
		}
	}

	if(res == 0)
	{
		// use the first address in the list
		addrInfo = addrInfoList;

		if(addrInfo != NULL)
		{
			success = ip->Init(addrInfo->ai_addr) && netVerify(ip->IsValid());
		}

#if !__NO_OUTPUT
		netDebug3("Addresses available:");
		for(ADDRINFO* ai = addrInfoList; ai != NULL; ai = ai->ai_next)
		{
#if IPv4_IPv6_DUAL_STACK
			netAssert(ai->ai_family == AF_INET6);
#else
			netAssert(ai->ai_family == AF_INET);
#endif
			netIpAddress addr(ai->ai_addr);
			netDebug3("    " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr));
		}
#endif
	}
	
	if(addrInfoList)
	{
		freeaddrinfo(addrInfoList);
		addrInfoList = NULL;
	}

	return success;
}

void 
netHardware::SetPublicIpAddress(const netIpAddress& ip)
{
	sm_PublicIp = ip;
}

bool
netHardware::NativeGetPublicIpAddress(netIpAddress* ip)
{
	*ip = sm_PublicIp;
	bool success = ip->IsValid();
	return success;
}

bool
netHardware::NativeGetMtu(u32& mtu)
{
	mtu = 0;
	return false;
}

}   //namespace rage

#endif
