// 
// net/event.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_EVENT_H
#define NET_EVENT_H

#include "atl/atfunctor.h"
#include "data/autoid.h"
#include "netaddress.h"
#include "netdiag.h"
#include "netsequence.h"
#include "system/criticalsection.h"
#include "system/memory.h"

namespace rage
{

class sysMemAllocator;
template<typename T> class netEventQueue;

//PURPOSE
//  Base class for all connection event classes.
class netEventBase
{
    template<typename T> friend class netEventQueue;

public:

    netEventBase(sysMemAllocator* allocator);

    virtual ~netEventBase();

    //PURPOSE
    //  Disposes/deallocates the event.  Do not use the event
    //  instance after calling this function.
    virtual void Dispose();

    //PURPOSE
    //  Returns the event time stamp.
    unsigned GetTimeStamp() const;

    //PURPOSE
    //  Returns true if this event is queued.
    bool IsQueued() const;

	//PURPOSE
	//  Returns true if this event was filtered out by RemoveEvents()
	//  but could not be removed due to outstanding references.
	//  We use this to avoid dispatching events for terminated connections
	//  without having to enter the CxnMgr's critical section during
	//  dispatch to check whether the connection has been terminated.
	bool IsFiltered() const;

protected:

    unsigned m_TimeStamp;

    sysMemAllocator* const m_Allocator;

private:

    netEventBase(const netEventBase& that);
    netEventBase& operator=(const netEventBase& that);

    const void* m_Queue;
    netEventBase* m_Next;
    netEventBase* m_Prev;

	u8 m_Filtered;
    mutable int m_RefCount;

    volatile unsigned m_Disposing;
};

//Net event ids.
enum
{
    //A connection was requested from a remote endpoint.
    NET_EVENT_CONNECTION_REQUESTED,
    //A connection was established.
    NET_EVENT_CONNECTION_ESTABLISHED,
	//There was an error on the connection.
    NET_EVENT_CONNECTION_ERROR,
    //A connection was closed.
    NET_EVENT_CONNECTION_CLOSED,
    //A frame of data was received.
    NET_EVENT_FRAME_RECEIVED,
    //An ack for a previously sent frame was received.
    NET_EVENT_ACK_RECEIVED,
    //Bandwidth limit was exceeded.
    NET_EVENT_BANDWIDTH_EXCEEDED,
    //Ran out of memory.
    NET_EVENT_OUT_OF_MEMORY
};

#define NET_EVENT_COMMON_DECL(name)\
    static unsigned EVENT_ID() { return name::GetAutoId(); }\
    virtual unsigned GetId() const { return name::GetAutoId(); }

#define NET_EVENT_DECL(name, id)\
    AUTOID_DECL_ID(name, rage::netEvent, id)\
    NET_EVENT_COMMON_DECL(name)

// endpoint/connection related events
class netEventConnectionRequested;
class netEventConnectionEstablished;
class netEventConnectionError;
class netEventConnectionClosed;
class netEventFrameReceived;
class netEventAckReceived;
class netEventBandwidthExceeded;
class netEventOutOfMemory;

//PURPOSE
//  Base class for all connection event classes.
class netEvent : public netEventBase
{
    friend class netEventQueue<netEvent>;

public:

    AUTOID_DECL_ROOT(netEvent);
    NET_EVENT_COMMON_DECL(netEvent);

	netEvent(const int cxnId,
			 const EndpointId epId,
			 const unsigned channelId,
			 sysMemAllocator* allocator);

    virtual ~netEvent();

	//Local connection id.
	int m_CxnId;
	//Channel id.
	unsigned m_ChannelId;

	EndpointId GetEndpointId() const
	{
		netAssert(NET_IS_VALID_ENDPOINT_ID(m_EndpointId) || (m_CxnId < 0));
		return m_EndpointId;
	}
	
    //PURPOSE
    //  Convenient access to the concrete event instance.
    union
    {
        netEvent*								m_Event;
		netEventConnectionRequested*			m_CxnRequested;
        netEventConnectionEstablished*			m_CxnEstablished;
		netEventConnectionError*				m_CxnError;
        netEventConnectionClosed*				m_CxnClosed;
		netEventFrameReceived*					m_FrameReceived;
		netEventAckReceived*					m_AckReceived;
		netEventBandwidthExceeded*				m_BandwidthExceeded;
		netEventOutOfMemory*					m_OutOfMemory;
    };

protected:

	EndpointId m_EndpointId;

private:

    netEvent(const netEvent& that);
    netEvent& operator=(const netEvent& that);
};

//PURPOSE
//  Dispatched when a connection is established.
class netEventConnectionEstablished : public netEvent
{
public:
	NET_EVENT_DECL(netEventConnectionEstablished, NET_EVENT_CONNECTION_ESTABLISHED);

	netEventConnectionEstablished(const int cxnId, const EndpointId epId, const unsigned channelId, sysMemAllocator* allocator)
		: netEvent(cxnId, epId, channelId, allocator)
	{
	}
};

//PURPOSE
//  Dispatched upon receiving a frame of data.
//  A negative connection id implies this frame was sent out
//  of band, i.e. outside of any connection.  In that case the
//  sequence number and flags are meaningless and should be ignored.
class netEventConnectionRequested : public netEvent
{
public:
	NET_EVENT_DECL(netEventConnectionRequested, NET_EVENT_CONNECTION_REQUESTED);

	netEventConnectionRequested(const netAddress& sender,
								const ChannelId channelId,
								const void* payload,
								const unsigned sizeofPayload,
								const netSequence seq,
								sysMemAllocator* allocator)
		: netEvent(NET_INVALID_CXN_ID, NET_INVALID_ENDPOINT_ID, channelId, allocator)
		, m_Sender(sender)
		, m_SizeofPayload(sizeofPayload)
		, m_Payload(payload)
		, m_Sequence(seq)
	{
	}

	virtual ~netEventConnectionRequested()
	{
		if(m_Payload && m_Allocator && (void*) (this + 1) != m_Payload)
		{
			m_Allocator->Free(m_Payload);
		}
	}

	virtual unsigned GetPayloadSize() const { return m_SizeofPayload; }

	//Address of sender.
	netAddress m_Sender;
	//Size in bytes of the payload.
	unsigned m_SizeofPayload;
	//Frame payload.
	const void* m_Payload;
	//Sequence number of frame.
	netSequence m_Sequence;
};

//PURPOSE
//  Dispatched when there is an error on the connection.  In most cases the
//  app should close the connection with the NET_CLOSE_IMMEDIATELY close type.
//
//  In the case of a connection denied by a remote peer, the error code
//  will be CXNERR_TERMINATED and the m_Data member will contain the data
//  passed to the DenyConnection() function.
class netEventConnectionError : public netEvent
{
public:
	NET_EVENT_DECL(netEventConnectionError, NET_EVENT_CONNECTION_ERROR);

	enum ErrorCode
	{
		//Connection timed out
		CXNERR_TIMED_OUT,
		//There was a send error
		CXNERR_SEND_ERROR,
		//The connection was terminated by the remote peer.
		CXNERR_TERMINATED,
	};

	enum
	{
		MAX_SIZEOF_DATA     = 128,
	};

	netEventConnectionError(const int cxnId,
							const EndpointId epId,
							const ChannelId channelId,
							const void* data,
							const unsigned sizeofData,
							const ErrorCode errorCode,
							const int errorContext,
							sysMemAllocator* allocator)
		: netEvent(cxnId, epId, channelId, allocator)
		, m_Code(errorCode)
		, m_ErrorContext(errorContext)
		, m_SizeofData(sizeofData)
	{
		netAssert(!data || sizeofData > 0);
		netAssert(sizeofData <= MAX_SIZEOF_DATA);
		if(sizeofData)
		{
			sysMemCpy(m_Data, data, sizeofData);
		}
	}

	bool IsTimeout() const{return CXNERR_TIMED_OUT == m_Code;}
	bool IsSendError() const{return CXNERR_SEND_ERROR == m_Code;}
	bool IsTerm() const{return CXNERR_TERMINATED == m_Code;}

	union
	{
		int m_Code;
		ErrorCode m_ErrorCode;
	};

	int m_ErrorContext;
	u8 m_Data[MAX_SIZEOF_DATA];
	unsigned m_SizeofData;
};

//PURPOSE
//  Dispatched when a connection is closed.  When this event is received
//  the connection can no longer send or receive frames on the connection.
class netEventConnectionClosed : public netEvent
{
public:
	NET_EVENT_DECL(netEventConnectionClosed, NET_EVENT_CONNECTION_CLOSED);

	netEventConnectionClosed(const int cxnId,
							 const EndpointId epId,
							 const ChannelId channelId,
							 sysMemAllocator* allocator)
		: netEvent(cxnId, epId, channelId, allocator)
	{
	}
};

//PURPOSE
//  Dispatched upon receiving a frame of data.
//  A negative connection id implies this frame was sent out
//  of band, i.e. outside of any connection.  In that case the
//  sequence number and flags are meaningless and should be ignored.
class netEventFrameReceived : public netEvent
{
public:
	NET_EVENT_DECL(netEventFrameReceived, NET_EVENT_FRAME_RECEIVED);

	netEventFrameReceived(const netAddress& sender,
						  const EndpointId epId,
						  const int cxnId,
						  const unsigned channelId,
						  const void* payload,
						  const unsigned sizeofPayload,
						  const netSequence seq,
						  const unsigned flags,
						  sysMemAllocator* allocator)
	  : netEvent(cxnId, epId, channelId, allocator)
		, m_Sender(sender)
		, m_SizeofPayload(sizeofPayload)
		, m_Payload(payload)
		, m_Flags(flags)
		, m_Sequence(seq)
	{
	}

	virtual ~netEventFrameReceived()
	{
		if(m_Payload && m_Allocator && (void*) (this + 1) != m_Payload)
		{
			m_Allocator->Free(m_Payload);
		}
	}

	virtual unsigned GetPayloadSize() const { return m_SizeofPayload; }

	//Address of sender.
	netAddress m_Sender;
	//Size in bytes of the payload.
	unsigned m_SizeofPayload;
	//Frame payload.
	const void* m_Payload;
	//Bit flags from netSendFlags enum.
	unsigned m_Flags;
	//Sequence number of frame.
	netSequence m_Sequence;
};

//PURPOSE
//  Dispatched when a connection receives an ACK for a previously sent frame.
class netEventAckReceived : public netEvent
{
public:
	NET_EVENT_DECL(netEventAckReceived, NET_EVENT_ACK_RECEIVED);

	netEventAckReceived(const int cxnId,
						const EndpointId epId,
						const ChannelId channelId,
						const netSequence ack,
						sysMemAllocator* allocator)
		: netEvent(cxnId, epId, channelId, allocator)
		, m_Ack(ack)
	{
	}

	//Sequence number of the frame that was ACKed.
	netSequence m_Ack;
};

//PURPOSE
//  Dispatched when bandwidth is exceeded on a channel.
class netEventBandwidthExceeded : public netEvent
{
public:
	NET_EVENT_DECL(netEventBandwidthExceeded, NET_EVENT_BANDWIDTH_EXCEEDED);

	netEventBandwidthExceeded(const EndpointId epId,
							  const ChannelId channelId,
							  sysMemAllocator* allocator)
		: netEvent(NET_INVALID_CXN_ID, epId, channelId, allocator)
	{
	}
};

//PURPOSE
//  Dispatched when when the heap provided to the connection manager
//  is exhausted.  This is a fatal error - all connections will henceforth
//  be out of sync.
class netEventOutOfMemory : public netEvent
{
public:
	NET_EVENT_DECL(netEventOutOfMemory, NET_EVENT_OUT_OF_MEMORY);

	netEventOutOfMemory(const int cxnId,
						const EndpointId epId,
						const unsigned channelId,
						const bool isFatal,
						sysMemAllocator* allocator)
		: netEvent(cxnId, epId, channelId, allocator)
		, m_isFatal(isFatal)
	{
	}

	bool m_isFatal;
};

template<typename T>
class netEventConsumer
{
    friend class netEventQueue<T>;
public:
    netEventConsumer();

    ~netEventConsumer();

    //After calling NextEvent() DO NOT reference previous events.
    T* NextEvent();
	bool IsSubscribed() { return m_Queue != NULL; }
	unsigned GetNumEvents() const;

private:

    netEventQueue<T>* m_Queue;
    netEventBase* m_Event;

    netEventConsumer<T>* m_Next;
};

//PURPOSE
//  Simple queue used to queue events.  Intended for use only
//  by netConnection and netConnectionManager.
template<typename T>
class netEventQueue
{
public:

    netEventQueue();

    ~netEventQueue();

    void ClearEvents();

    bool Subscribe(netEventConsumer<T>* consumer);
    void Unsubscribe(netEventConsumer<T>* consumer);
    void UnsubscribeAll();

    //DO NOT attempt to reference the event after it's been queued.
    //If there are no consumers to the queue the event could have
    //been disposed.
    void QueueEvent(T* e,
                    const unsigned timeStamp);

    //Release e and return the event after e.
    T* NextEvent(netEventConsumer<T>* consumer);

    void RemoveEvents(bool (*filter)(const T* event));

	//PURPOSE
	//PARAMS
	//  filter   - functor to select events to remove
	//  filterIfNotRemoved - events cannot be removed if they are still
	//						 referenced. Set this to true to mark events
	//						 that cannot be removed as 'filtered'. 
	//						 Filtered events are not dispatched.
	typedef atFunctor1<bool, const T*> RemoveEventsFilter;
	void RemoveEvents(RemoveEventsFilter& filter, bool filterIfNotRemoved);

	unsigned GetNumEvents() const;

#if !__NO_OUTPUT
	void Dump();
	bool HasEvents();
#endif

private:

	void UnsubscribeInternal(netEventConsumer<T>* consumer);
	T* NextEventInternal(netEventConsumer<T>* consumer);

    int Ref(netEventBase* e);
    int Unref(netEventBase* e);

    bool IsReferencedByConsumer(const netEventBase* e) const;

    void CollectGarbage();

    void Remove(netEventBase* e);

    netEventBase m_Head;
    int m_NumEvents;
    netEventConsumer<T>* m_Consumers;
    int m_NumConsumers;
    mutable sysCriticalSectionToken m_Cs;

    bool m_CollectingGarbage    : 1;
};

//////////////////////////////////////////////////////////////////////////
//  netEventConsumer
//////////////////////////////////////////////////////////////////////////
template<typename T>
netEventConsumer<T>::netEventConsumer()
    : m_Queue(NULL)
    , m_Event(NULL)
    , m_Next(NULL)
{
}

template<typename T>
netEventConsumer<T>::~netEventConsumer()
{
    if(m_Queue)
    {
        m_Queue->Unsubscribe(this);
    }

    netAssert(!m_Queue);
    netAssert(!m_Event);
    netAssert(!m_Next);
}

template<typename T>
T*
netEventConsumer<T>::NextEvent()
{
    if(netVerify(m_Queue))
    {
        return m_Queue->NextEvent(this);
    }

    return NULL;
}

template<typename T>
unsigned 
netEventConsumer<T>::GetNumEvents() const
{
    if(netVerify(m_Queue))
    {
        return m_Queue->GetNumEvents();
    }

    return 0;
}

//////////////////////////////////////////////////////////////////////////
//  netEventQueue
//////////////////////////////////////////////////////////////////////////
template<typename T>
netEventQueue<T>::netEventQueue()
: m_Head(nullptr)
, m_NumEvents(0)
, m_Consumers(NULL)
, m_NumConsumers(0)
, m_CollectingGarbage(false)
{
    m_Head.m_Next = m_Head.m_Prev = &m_Head;
    m_Head.m_Queue = this;
}

template<typename T>
netEventQueue<T>::~netEventQueue()
{
	SYS_CS_SYNC(m_Cs);

    while(m_Consumers)
    {
        this->UnsubscribeInternal(m_Consumers);
    }

    this->CollectGarbage();

    //We shouldn't have any events in the queue at this point.
    netAssert(&m_Head == m_Head.m_Next);

    //Prevent asserts in the netEvent destructor
    m_Head.m_Next = m_Head.m_Prev = NULL;
    m_Head.m_Queue = NULL;
}

template<typename T>
void
netEventQueue<T>::ClearEvents()
{
    SYS_CS_SYNC(m_Cs);

    //Move all consumers to the end of the queue
    for(netEventConsumer<T>* c = m_Consumers; c; c = c->m_Next)
    {
        while(NextEventInternal(c));
    }

    this->CollectGarbage();

    //We shouldn't have any events in the queue at this point.
    netAssert(&m_Head == m_Head.m_Next);
}

template<typename T>
bool
netEventQueue<T>::Subscribe(netEventConsumer<T>* consumer)
{
    SYS_CS_SYNC(m_Cs);

    if(netVerify(!consumer->m_Queue))
    {
        consumer->m_Next = m_Consumers;
        m_Consumers = consumer;
        consumer->m_Queue = this;
        consumer->m_Event = NULL;
        ++m_NumConsumers;

        return true;
    }

    return false;
}

template<typename T>
void
netEventQueue<T>::UnsubscribeInternal(netEventConsumer<T>* consumer)
{
    if(netVerify(this == consumer->m_Queue))
    {
        netEventConsumer<T>** next = &m_Consumers;
        netEventConsumer<T>* c;
        for(c = m_Consumers; c && c != consumer; next = &c->m_Next, c = c->m_Next)
        {
        }

        if(netVerify(c))
        {
            //Unref all events not yet consumed by this consumer
            while(NextEventInternal(c))
            {
            }

            netAssert(!c->m_Event);

            *next = c->m_Next;
            c->m_Next = NULL;
            c->m_Queue = NULL;
            c->m_Event = NULL;
            --m_NumConsumers;
        }

        this->CollectGarbage();
    }
}

template<typename T>
void
netEventQueue<T>::Unsubscribe(netEventConsumer<T>* consumer)
{
	SYS_CS_SYNC(m_Cs);
	UnsubscribeInternal(consumer);
}

template<typename T>
void
netEventQueue<T>::UnsubscribeAll()
{
    SYS_CS_SYNC(m_Cs);
    while(m_Consumers)
    {
        this->UnsubscribeInternal(m_Consumers);
    }
}

template<typename T>
void
netEventQueue<T>::QueueEvent(T* e, const unsigned timeStamp)
{
    SYS_CS_SYNC(m_Cs);

    this->CollectGarbage();

    if(netVerify(!e->m_Queue) && netVerify(0 == e->m_RefCount))
    {
        e->m_TimeStamp = timeStamp;

        //Put the event at the end of the queue
        e->m_Prev = m_Head.m_Prev;
        e->m_Next = &m_Head;
        e->m_Prev->m_Next = e->m_Next->m_Prev = e;
        e->m_Queue = this;
        e->m_RefCount = m_NumConsumers;
        ++m_NumEvents;

        //Fix up all consumers that are new or have run out of events.
        //Next time they call NextEvent() they'll get the event
        //just queued.
        for(netEventConsumer<T>* c = m_Consumers; c; c = c->m_Next)
        {
            if(!c->m_Event)
            {
                c->m_Event = e->m_Prev;
                if(&m_Head != e->m_Prev)
                {
                    Ref(e->m_Prev);
                }
            }
        }
    }
}

template<typename T>
T*
netEventQueue<T>::NextEventInternal(netEventConsumer<T>* consumer)
{
    if(netVerify(this == consumer->m_Queue))
    {
        netEventBase* tmp = consumer->m_Event;

        if(consumer->m_Event)
        {
            consumer->m_Event = consumer->m_Event->m_Next;

            if(&m_Head == consumer->m_Event)
            {
                //Reached the end of the queue
                consumer->m_Event = NULL;
            }
        }

        if(tmp && &m_Head != tmp)
        {
            this->Unref(tmp);
        }
    }

    this->CollectGarbage();

    netAssert(!consumer->m_Event || consumer->m_Event->m_RefCount > 0);

    return static_cast<T*>(consumer->m_Event);
}

template<typename T>
T*
netEventQueue<T>::NextEvent(netEventConsumer<T>* consumer)
{
	SYS_CS_SYNC(m_Cs);
	return NextEventInternal(consumer);
}

template<typename T>
void
netEventQueue<T>::RemoveEvents(bool (*filter)(const T* event))
{
    SYS_CS_SYNC(m_Cs);

    for(netEventBase* e = m_Head.m_Next; &m_Head != e; e = e->m_Next)
    {
        if(this->IsReferencedByConsumer(e))
        {
            continue;
        }

		if(filter(static_cast<T*>(e)))
		{
			netEventBase* tmp = e->m_Prev;
			this->Remove(e);
			e->m_RefCount = 0;
			e->Dispose();
			e = tmp;
		}
    }
}

template<typename T>
void
netEventQueue<T>::RemoveEvents(RemoveEventsFilter& filter, bool filterIfNotRemoved)
{
    SYS_CS_SYNC(m_Cs);

    for(netEventBase* e = m_Head.m_Next; &m_Head != e; e = e->m_Next)
    {
		if(!filter(static_cast<T*>(e)))
		{
			continue;
		}

        if(this->IsReferencedByConsumer(e))
        {
			// can't remove this event, so mark it as filtered to avoid dispatching it
			e->m_Filtered = filterIfNotRemoved ? 1 : 0;
            continue;
        }
        
        netEventBase* tmp = e->m_Prev;
        this->Remove(e);
        e->m_RefCount = 0;
        e->Dispose();
        e = tmp;
    }
}
 
template<typename T>
unsigned
netEventQueue<T>::GetNumEvents() const
{
	return (unsigned)m_NumEvents;
}

//private:

template<typename T>
int
netEventQueue<T>::Ref(netEventBase* e)
{
    if(netVerify(this == e->m_Queue)
        && netVerify(&m_Head != e))
    {
        netAssert(e->m_RefCount >= 0);
        ++e->m_RefCount;
    }

    return e->m_RefCount;
}

template<typename T>
int
netEventQueue<T>::Unref(netEventBase* e)
{
    if(netVerify(this == e->m_Queue)
        && netVerify(&m_Head != e))
    {
        netAssert(e->m_RefCount > 0);
        --e->m_RefCount;
        netAssert(e->m_RefCount > 0 || !IsReferencedByConsumer(e));
    }

    return e->m_RefCount;
}

template<typename T>
bool
netEventQueue<T>::IsReferencedByConsumer(const netEventBase* e) const
{
    for(const netEventConsumer<T>* c = m_Consumers; c; c = c->m_Next)
    {
        if(c->m_Event == e)
        {
            return true;
        }
    }

    return false;
}

template<typename T>
void
netEventQueue<T>::CollectGarbage()
{
    if(m_CollectingGarbage)
    {
        return;
    }

    m_CollectingGarbage = true;

    netEventBase* e = m_Head.m_Next;

    while(&m_Head != e && e->m_RefCount < 1)
    {
        netEventBase* tmp = e->m_Next;
        this->Remove(e);
        e->Dispose();
        e = tmp;
    }

    m_CollectingGarbage = false;
}

template<typename T>
void
netEventQueue<T>::Remove(netEventBase* e)
{
    if(netVerify(this == e->m_Queue))
    {
        netAssert(!IsReferencedByConsumer(e));

        e->m_Next->m_Prev = e->m_Prev;
        e->m_Prev->m_Next = e->m_Next;
        e->m_Prev = e->m_Next = NULL;
        e->m_Queue = NULL;
        netAssert(m_NumEvents > 0);
        --m_NumEvents;
    }
}

#if !__NO_OUTPUT
template<typename T>
void
netEventQueue<T>::Dump()
{
	SYS_CS_SYNC(m_Cs);

	for(netEventBase* e = m_Head.m_Next; &m_Head != e; e = e->m_Next)
	{
		netEvent* ne = static_cast<netEvent*>(e);
		const char* name = AutoIdRegistrar<netEvent>::GetNameFromId(ne->GetId());
		netDebug(" %s[%d:%08x]:%u", 
			name ? name : "UNKNOWN EVENT TYPE",
			ne->m_ChannelId, 
			ne->m_CxnId, 
			ne->GetId() == NET_EVENT_FRAME_RECEIVED ? ne->m_FrameReceived->m_SizeofPayload : 0);
	}
}

template<typename T>
bool 
netEventQueue<T>::HasEvents()
{
	return !(&m_Head == m_Head.m_Next);
}
#endif

}   //namespace rage

#endif  //NET_EVENT_H
