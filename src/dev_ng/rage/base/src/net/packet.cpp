// 
// net/packet.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "packet.h"
#include "net/netdiag.h"
#include "system/memops.h"
#include "system/param.h"
#include "system/timer.h"

#include <time.h>

namespace rage
{

static bool s_IsNetPacketRngInitialized = false;
static netRandom s_netPacketRng;

PARAM(netrelaynocrypto, "Disables mixing of relay packets with random byte - all packets are secured using P2PCrypt");

///////////////////////////////////////////////////////////////////////////////
// netRelayPacket
///////////////////////////////////////////////////////////////////////////////

netRelayPacket::netRelayPacket()
{
    //Make sure we don't initialize the RNG until the PARAM
    //system is initialized because the user could have specified
    //via command line param a fixed seed for all RNGs.
    if(!s_IsNetPacketRngInitialized)
    {
		s_netPacketRng.SetRandomSeed();
        s_IsNetPacketRngInitialized = true;
    }
}

void
netRelayPacket::ResetPingHeader(const unsigned sizeofPayload)
{
    netAssert(GetSizeofHeader() == SIZEOF_PING_HEADER);

    netPacket* pkt = (netPacket*)this;
    pkt->ResetHeader(sizeofPayload + SIZEOF_PING_HEADER-netPacket::BYTE_SIZEOF_HEADER, NET_PACKETFLAG_RELAY);
    m_Data[OFFSETOF_VERSION] = PROTOCOL_VERSION;
    
    if(PARAM_netrelaynocrypto.Get())
    {
        m_Data[OFFSETOF_MIXER] = 0x00;
    }
    else
    {
        m_Data[OFFSETOF_MIXER] = u8(s_netPacketRng.GetInt() & 0xFF);
    }

    m_Data[OFFSETOF_TYPE] = (u8)NET_RELAYPACKET_PING;
}

void
netRelayPacket::ResetPing2Header(const unsigned sizeofPayload)
{
    netAssert(GetSizeofHeader() == SIZEOF_PING2_HEADER);

    netPacket* pkt = (netPacket*)this;
    pkt->ResetHeader(sizeofPayload + SIZEOF_PING2_HEADER-netPacket::BYTE_SIZEOF_HEADER, NET_PACKETFLAG_RELAY | NET_PACKETFLAG_SECURE_ADDR);
    m_Data[OFFSETOF_VERSION] = PROTOCOL_VERSION;
    
    if(PARAM_netrelaynocrypto.Get())
    {
        m_Data[OFFSETOF_MIXER] = 0x00;
    }
    else
    {
        m_Data[OFFSETOF_MIXER] = u8(s_netPacketRng.GetInt() & 0xFF);
    }

    m_Data[OFFSETOF_TYPE] = (u8)NET_RELAYPACKET_PING2;
}

void
netRelayPacket::ResetAltPortPingHeader(const unsigned sizeofPayload)
{
    netAssert(GetSizeofHeader() == SIZEOF_PING_HEADER);

    netPacket* pkt = (netPacket*)this;
    pkt->ResetHeader(sizeofPayload + SIZEOF_PING_HEADER-netPacket::BYTE_SIZEOF_HEADER, NET_PACKETFLAG_RELAY | NET_PACKETFLAG_ALT_SEND_PORT);
    m_Data[OFFSETOF_VERSION] = PROTOCOL_VERSION;
    
    if(PARAM_netrelaynocrypto.Get())
    {
        m_Data[OFFSETOF_MIXER] = 0x00;
    }
    else
    {
        m_Data[OFFSETOF_MIXER] = u8(s_netPacketRng.GetInt() & 0xFF);
    }

    m_Data[OFFSETOF_TYPE] = (u8)NET_RELAYPACKET_PING;
}

void
netRelayPacket::ResetP2pHeader(const unsigned sizeofPayload,
							   const netAddress& dstRelayAddress,
                               const netAddress& srcRelayAddress,
							   const netRelayToken& srcRelayToken)
{
    netAssert(GetSizeofHeader() == SIZEOF_P2P_HEADER);

	netAssert(dstRelayAddress.IsRelayServerAddr());
    netAssert(srcRelayAddress.IsRelayServerAddr());

	const netSocketAddress& dstTargetAddr = dstRelayAddress.GetTargetAddress();
	const netRelayToken& dstRelayToken = dstRelayAddress.GetRelayToken();

	// always use the target's real IP/port if we have it,
	// otherwise use the target's token which requires more work on the server
	const bool useRelayToken = !dstTargetAddr.IsValid() && netVerify(dstRelayToken.IsValid());
	unsigned flags = NET_PACKETFLAG_RELAY;
	if(useRelayToken)
	{
		flags |= NET_PACKETFLAG_SECURE_ADDR;
	}

    netPacket* pkt = (netPacket*)this;
    pkt->ResetHeader(sizeofPayload + SIZEOF_P2P_HEADER-netPacket::BYTE_SIZEOF_HEADER, flags);

    m_Data[OFFSETOF_VERSION] = PROTOCOL_VERSION;
    
    if(PARAM_netrelaynocrypto.Get())
    {
        m_Data[OFFSETOF_MIXER] = 0x00;
    }
    else
    {
        m_Data[OFFSETOF_MIXER] = u8(s_netPacketRng.GetInt() & 0xFF);
    }

    const netIpAddress& srcProxyIp = srcRelayAddress.GetProxyAddress().GetIp();
    const u16 srcProxyPort = srcRelayAddress.GetProxyAddress().GetPort();
    const netIpAddress& srcTargetIp = srcRelayAddress.GetTargetAddress().GetIp();
    const u16 srctargetPort = srcRelayAddress.GetTargetAddress().GetPort();
	const unsigned srcProxyIpV4 = srcProxyIp.ToV4().ToU32();
	const unsigned srcTargetIpV4 = srcTargetIp.ToV4().ToU32();

	// [IPv4 specific]
	m_Data[OFFSETOF_TYPE]               = (u8)NET_RELAYPACKET_P2P_FORWARD;
	m_Data[OFFSETOF_SRC_PROXY_IP+0]     = u8(srcProxyIpV4 >> 24);
	m_Data[OFFSETOF_SRC_PROXY_IP+1]     = u8(srcProxyIpV4 >> 16);
	m_Data[OFFSETOF_SRC_PROXY_IP+2]     = u8(srcProxyIpV4 >> 8);
	m_Data[OFFSETOF_SRC_PROXY_IP+3]     = u8(srcProxyIpV4 >> 0);
	m_Data[OFFSETOF_SRC_PROXY_PORT+0]   = u8(srcProxyPort >> 8);
	m_Data[OFFSETOF_SRC_PROXY_PORT+1]   = u8(srcProxyPort >> 0);

	if(useRelayToken)
	{
		netAssert(srcRelayToken.IsValid());
		NET_ASSERTS_ONLY(const bool success =) dstRelayToken.Export(&m_Data[OFFSETOF_DST_SECURE_TOKEN], SIZEOF_SECURE_TOKEN) &&
											   srcRelayToken.Export(&m_Data[OFFSETOF_SRC_SECURE_TOKEN], SIZEOF_SECURE_TOKEN);

		netAssert(success);
	}
	else if(netVerify(dstTargetAddr.IsValid()))
	{
		// TODO: NS - IPv4 specific
		unsigned dstIpV4 = dstTargetAddr.GetIp().ToV4().ToU32();
		u16 dstPort = dstTargetAddr.GetPort();

		m_Data[OFFSETOF_DST_IP+0]           = u8(dstIpV4 >> 24);
		m_Data[OFFSETOF_DST_IP+1]           = u8(dstIpV4 >> 16);
		m_Data[OFFSETOF_DST_IP+2]           = u8(dstIpV4 >> 8);
		m_Data[OFFSETOF_DST_IP+3]           = u8(dstIpV4 >> 0);
		m_Data[OFFSETOF_DST_PORT+0]         = u8(dstPort >> 8);
		m_Data[OFFSETOF_DST_PORT+1]         = u8(dstPort >> 0);

		m_Data[OFFSETOF_SRC_TARGET_IP+0]    = u8(srcTargetIpV4 >> 24);
		m_Data[OFFSETOF_SRC_TARGET_IP+1]    = u8(srcTargetIpV4 >> 16);
		m_Data[OFFSETOF_SRC_TARGET_IP+2]    = u8(srcTargetIpV4 >> 8);
		m_Data[OFFSETOF_SRC_TARGET_IP+3]    = u8(srcTargetIpV4 >> 0);
		m_Data[OFFSETOF_SRC_TARGET_PORT+0]  = u8(srctargetPort >> 8);
		m_Data[OFFSETOF_SRC_TARGET_PORT+1]  = u8(srctargetPort >> 0);
	}
}

void
netRelayPacket::Mix()
{
    const u8 mixer = m_Data[OFFSETOF_MIXER];
    const unsigned size = this->GetSize();

    //Don't mix the size - it's needed by Xbox low level networking.
    for(int i = SIZEOF_SIZE; i < (int)size; ++i)
    {
        m_Data[i] ^= mixer;
    }

    //Restore the mixer;
    m_Data[OFFSETOF_MIXER] = mixer;
}

void
netRelayPacket::Unmix()
{
    this->Mix();
}

netIpAddress
netRelayPacket::GetDstIp() const
{
	// [IPv4 specific]

	// ping2/pong2 put the address after the secure token
	const bool shifted = (GetType() == NET_RELAYPACKET_PONG2) ||
						 (GetType() == NET_RELAYPACKET_PING2);

	const unsigned offset = shifted ?
							OFFSETOF_DST_PLAIN_IP :
							OFFSETOF_DST_IP;

	unsigned dstIpV4 = (m_Data[offset+0] << 24)
						| (m_Data[offset+1] << 16)
						| (m_Data[offset+2] << 8)
						| (m_Data[offset+3] << 0);

	return netIpAddress(netIpV4Address(dstIpV4));
}

u16
netRelayPacket::GetDstPort() const
{
	// ping2/pong2 put the address after the secure token
	const bool shifted = (GetType() == NET_RELAYPACKET_PONG2) ||
						 (GetType() == NET_RELAYPACKET_PING2);

	const unsigned offset = shifted ?
							OFFSETOF_DST_PLAIN_PORT :
							OFFSETOF_DST_PORT;

    return (m_Data[offset+0] << 8)
            | (m_Data[offset+1] << 0);
}

netSocketAddress 
netRelayPacket::GetDstAddr() const
{
	return netSocketAddress(GetDstIp(), GetDstPort());
}

netRelayToken
netRelayPacket::GetDstRelayToken() const
{
	netRelayToken token;
	if(token.Import(&m_Data[OFFSETOF_DST_SECURE_TOKEN], SIZEOF_SECURE_TOKEN) && netVerify(token.IsValid()))
	{
		return token;
	}

	return netRelayToken::INVALID_RELAY_TOKEN;
}

bool
netRelayPacket::GetSrcRelayAddress(netAddress* addr) const
{
    if(this->IsP2pForward() || this->IsP2pTerminus())
    {
		// [IPv4 specific]

        const unsigned srcProxyIp       = (m_Data[OFFSETOF_SRC_PROXY_IP+0] << 24)
                                        | (m_Data[OFFSETOF_SRC_PROXY_IP+1] << 16)
                                        | (m_Data[OFFSETOF_SRC_PROXY_IP+2] << 8)
                                        | (m_Data[OFFSETOF_SRC_PROXY_IP+3] << 0);
        const unsigned srcProxyPort     = (m_Data[OFFSETOF_SRC_PROXY_PORT+0] << 8)
                                        | (m_Data[OFFSETOF_SRC_PROXY_PORT+1] << 0);

		netSocketAddress proxyAddr(netIpAddress(netIpV4Address(srcProxyIp)), (u16)srcProxyPort);

		const bool useToken = 0 != (this->GetFlags() & NET_PACKETFLAG_SECURE_ADDR);

		if(useToken)
		{
			netRelayToken relayToken;
			if(!netVerify(relayToken.Import(&m_Data[OFFSETOF_SRC_SECURE_TOKEN], SIZEOF_SECURE_TOKEN)) && netVerify(relayToken.IsValid()))
			{
				return false;
			}

			addr->Init(proxyAddr, relayToken, netSocketAddress::INVALID_ADDRESS);
		}
		else
		{
			const unsigned srcTargetIp      = (m_Data[OFFSETOF_SRC_TARGET_IP+0] << 24)
											| (m_Data[OFFSETOF_SRC_TARGET_IP+1] << 16)
											| (m_Data[OFFSETOF_SRC_TARGET_IP+2] << 8)
											| (m_Data[OFFSETOF_SRC_TARGET_IP+3] << 0);
			const unsigned srcTargetPort    = (m_Data[OFFSETOF_SRC_TARGET_PORT+0] << 8)
											| (m_Data[OFFSETOF_SRC_TARGET_PORT+1] << 0);

			netSocketAddress targetAddr(netIpAddress(netIpV4Address(srcTargetIp)), (u16)srcTargetPort);

			addr->Init(proxyAddr, netRelayToken::INVALID_RELAY_TOKEN, targetAddr);
		}

        return addr->IsRelayServerAddr();
    }

    return false;
}

netRelayPacketType
netRelayPacket::GetType() const
{
    return (netRelayPacketType) m_Data[OFFSETOF_TYPE];
}

unsigned
netRelayPacket::GetSize() const
{
    return ((const netPacket*)this)->GetSize();
}

unsigned
netRelayPacket::GetSizeofHeader() const
{
    switch(GetType())
    {
    case NET_RELAYPACKET_PING:
    case NET_RELAYPACKET_PONG:
    case NET_RELAYPACKET_MESSAGE:
        return SIZEOF_PING_HEADER;
	case NET_RELAYPACKET_PING2:
	case NET_RELAYPACKET_PONG2:
		return SIZEOF_PING2_HEADER;
	case NET_RELAYPACKET_P2P_FORWARD:
    case NET_RELAYPACKET_P2P_TERMINUS:
        return SIZEOF_P2P_HEADER;
    case NET_RELAYPACKET_NUM_TYPES:
		netAssert(false);
		return 0;
    }

    return 0;
}

unsigned
netRelayPacket::GetSizeofPayload() const
{
    const unsigned sizeofHeader = GetSizeofHeader();
    return sizeofHeader ? this->GetSize() - sizeofHeader : 0;
}

unsigned
netRelayPacket::GetFlags() const
{
    return ((const netPacket*)this)->GetFlags();
}

unsigned
netRelayPacket::GetVersion() const
{
    return m_Data[OFFSETOF_VERSION];
}

bool
netRelayPacket::IsRelayPacket() const
{
    return 0 != (this->GetFlags() & NET_PACKETFLAG_RELAY);
}

bool
netRelayPacket::IsPing() const
{
    return this->IsRelayPacket() && this->GetType() == NET_RELAYPACKET_PING;
}

bool
netRelayPacket::IsPing2() const
{
	return this->IsRelayPacket() && this->GetType() == NET_RELAYPACKET_PING2;
}

bool
netRelayPacket::IsPong() const
{
    return this->IsRelayPacket() && this->GetType() == NET_RELAYPACKET_PONG;
}

bool
netRelayPacket::IsPong2() const
{
    return this->IsRelayPacket() && this->GetType() == NET_RELAYPACKET_PONG2;
}

bool
netRelayPacket::IsP2pForward() const
{
    return this->IsRelayPacket() && this->GetType() == NET_RELAYPACKET_P2P_FORWARD;
}

bool
netRelayPacket::IsP2pTerminus() const
{
    return this->IsRelayPacket() && this->GetType() == NET_RELAYPACKET_P2P_TERMINUS;
}

bool
netRelayPacket::IsMessage() const
{
    return this->IsRelayPacket() && this->GetType() == NET_RELAYPACKET_MESSAGE;
}

void*
netRelayPacket::GetPayload()
{
    return &m_Data[GetSizeofHeader()];
}

const void*
netRelayPacket::GetPayload() const
{
    return &m_Data[GetSizeofHeader()];
}

netPacket*
netRelayPacket::GetPacket()
{
    return (netPacket*)GetPayload();
}

const netPacket*
netRelayPacket::GetPacket() const
{
    return (const netPacket*)GetPayload();
}

///////////////////////////////////////////////////////////////////////////////
// netRelayPingPacket
///////////////////////////////////////////////////////////////////////////////

netRelayPingPacket::netRelayPingPacket()
{
    ResetHeader();
}

void
netRelayPingPacket::ResetHeader()
{
    sysMemSet(this, 0, SIZEOF_PING_HEADER);
    m_Data[OFFSETOF_TYPE] = NET_RELAYPACKET_PING;
}

///////////////////////////////////////////////////////////////////////////////
// netRelayPing2Packet
///////////////////////////////////////////////////////////////////////////////
netRelayPing2Packet::netRelayPing2Packet()
{
    ResetHeader();
}

void
netRelayPing2Packet::ResetHeader()
{
    sysMemSet(this, 0, SIZEOF_PING2_HEADER);
    m_Data[OFFSETOF_TYPE] = NET_RELAYPACKET_PING2;
}

///////////////////////////////////////////////////////////////////////////////
// netRelayAltPortPingPacket
///////////////////////////////////////////////////////////////////////////////

netRelayAltPortPingPacket::netRelayAltPortPingPacket()
{
	ResetHeader();
}

void
netRelayAltPortPingPacket::ResetHeader()
{
	sysMemSet(this, 0, SIZEOF_PING_HEADER);
	m_Data[OFFSETOF_TYPE] = NET_RELAYPACKET_PING;
}

///////////////////////////////////////////////////////////////////////////////
// netRelayP2pPacket
///////////////////////////////////////////////////////////////////////////////

netRelayP2pPacket::netRelayP2pPacket()
{
    ResetHeader();
}

void
netRelayP2pPacket::ResetHeader()
{
    sysMemSet(this, 0, SIZEOF_PING_HEADER);
    m_Data[OFFSETOF_TYPE] = NET_RELAYPACKET_P2P_FORWARD;
}

///////////////////////////////////////////////////////////////////////////////
// netPeerRelayPacket
///////////////////////////////////////////////////////////////////////////////

netPeerRelayPacket::netPeerRelayPacket()
{
	sysMemSet(this, 0, SIZEOF_P2P_HEADER);
	m_Data[OFFSETOF_TYPE] = NET_RELAYPACKET_P2P_FORWARD;
}

void
netPeerRelayPacket::ResetP2pForwardHeader(const unsigned sizeofPayload,
										  const netPeerId& srcPeerId,
										  const netPeerId& destPeerId)
{
    netAssert(GetSizeofHeader() == SIZEOF_P2P_HEADER);

    netAssert(srcPeerId.IsValid());
	netAssert(destPeerId.IsValid());

    netPacket* pkt = (netPacket*)this;
    pkt->ResetHeader(sizeofPayload + SIZEOF_P2P_HEADER - netPacket::BYTE_SIZEOF_HEADER, NET_PACKETFLAG_RELAY);

    m_Data[OFFSETOF_VERSION] = PROTOCOL_VERSION;
	m_Data[OFFSETOF_TYPE] = (u8)NET_RELAYPACKET_P2P_FORWARD;

	srcPeerId.Export(&m_Data[OFFSETOF_SRC_PEER_ID], SIZEOF_PEER_ID);
	destPeerId.Export(&m_Data[OFFSETOF_DST_PEER_ID], SIZEOF_PEER_ID);
}

void
netPeerRelayPacket::ResetP2pTerminusHeader()
{
	m_Data[OFFSETOF_TYPE] = (u8)NET_RELAYPACKET_P2P_TERMINUS;
}

netPeerId
netPeerRelayPacket::GetSrcPeerId() const
{
	netPeerId peerId;
	peerId.Import(&m_Data[OFFSETOF_SRC_PEER_ID], SIZEOF_PEER_ID);
	return peerId;
}

netPeerId
netPeerRelayPacket::GetDstPeerId() const
{
	netPeerId peerId;
	peerId.Import(&m_Data[OFFSETOF_DST_PEER_ID], SIZEOF_PEER_ID);
	return peerId;
}

netRelayPacketType
netPeerRelayPacket::GetType() const
{
    return (netRelayPacketType) m_Data[OFFSETOF_TYPE];
}

unsigned
netPeerRelayPacket::GetSize() const
{
    return ((const netPacket*)this)->GetSize();
}

unsigned
netPeerRelayPacket::GetSizeofHeader() const
{
    switch(GetType())
    {
    case NET_RELAYPACKET_P2P_FORWARD:
    case NET_RELAYPACKET_P2P_TERMINUS:
        return SIZEOF_P2P_HEADER;
    default:
        netAssert(false);
    }

    return 0;
}

unsigned
netPeerRelayPacket::GetSizeofPayload() const
{
    const unsigned sizeofHeader = GetSizeofHeader();
    return sizeofHeader ? this->GetSize() - sizeofHeader : 0;
}

unsigned
netPeerRelayPacket::GetFlags() const
{
    return ((const netPacket*)this)->GetFlags();
}

unsigned
netPeerRelayPacket::GetVersion() const
{
    return m_Data[OFFSETOF_VERSION];
}

bool
netPeerRelayPacket::IsRelayPacket() const
{
    return 0 != (this->GetFlags() & NET_PACKETFLAG_RELAY);
}

bool
netPeerRelayPacket::IsP2pForward() const
{
    return this->IsRelayPacket() && this->GetType() == NET_RELAYPACKET_P2P_FORWARD;
}

bool
netPeerRelayPacket::IsP2pTerminus() const
{
    return this->IsRelayPacket() && this->GetType() == NET_RELAYPACKET_P2P_TERMINUS;
}

void*
netPeerRelayPacket::GetPayload()
{
    return &m_Data[GetSizeofHeader()];
}

const void*
netPeerRelayPacket::GetPayload() const
{
    return &m_Data[GetSizeofHeader()];
}

netPacket*
netPeerRelayPacket::GetPacket()
{
    return (netPacket*)GetPayload();
}

const netPacket*
netPeerRelayPacket::GetPacket() const
{
    return (const netPacket*)GetPayload();
}

///////////////////////////////////////////////////////////////////////////////
// netPacketStorage
///////////////////////////////////////////////////////////////////////////////

netPacketStorage::netPacketStorage()
{

}

netPacket*
netPacketStorage::GetPacket()
{
    return (netPacket*)(m_Data + OFFSETOF_PACKET);
}

const netPacket*
netPacketStorage::GetPacket() const
{
	return (const netPacket*)(m_Data + OFFSETOF_PACKET);
}

netRelayPacket*
netPacketStorage::GetRelayPacket()
{
	return (netRelayPacket*)(m_Data + OFFSETOF_RELAY_PACKET);
}

const netRelayPacket*
netPacketStorage::GetRelayPacket() const
{
	return (const netRelayPacket*)(m_Data + OFFSETOF_RELAY_PACKET);
}

netPeerRelayPacket*
netPacketStorage::GetPeerRelayPacket()
{
	return (netPeerRelayPacket*)(m_Data + OFFSETOF_PEER_RELAY_PACKET);
}

const netPeerRelayPacket*
netPacketStorage::GetPeerRelayPacket() const
{
	return (const netPeerRelayPacket*)(m_Data + OFFSETOF_PEER_RELAY_PACKET);
}

///////////////////////////////////////////////////////////////////////////////
// netPacket
///////////////////////////////////////////////////////////////////////////////

void
netPacket::ResetHeader(const unsigned sizeofPayload,
                        const unsigned flags)
{
    datBitBuffer::WriteUnsigned(m_Data,
                                 sizeofPayload + BYTE_SIZEOF_HEADER,
                                 BIT_SIZEOF_SIZE,
                                 BIT_OFFSETOF_SIZE);
    datBitBuffer::WriteUnsigned(m_Data,
                                 flags,
                                 BIT_SIZEOF_FLAGS,
                                 BIT_OFFSETOF_FLAGS);
}

unsigned
netPacket::GetSize() const
{
    unsigned size;
    datBitBuffer::ReadUnsigned(m_Data,
                                size,
                                BIT_SIZEOF_SIZE,
                                BIT_OFFSETOF_SIZE);
    return size;
}

unsigned
netPacket::GetSizeofPayload() const
{
    return this->GetSize() - BYTE_SIZEOF_HEADER;
}

unsigned
netPacket::GetFlags() const
{
    unsigned flags;
    datBitBuffer::ReadUnsigned(m_Data,
                                flags,
                                BIT_SIZEOF_FLAGS,
                                BIT_OFFSETOF_FLAGS);
    return flags;
}

bool
netPacket::IsCompressed() const
{
    return 0 != (this->GetFlags() & NET_PACKETFLAG_COMPRESSED);
}

bool
netPacket::IsRelayPacket() const
{
    return 0 != (this->GetFlags() & NET_PACKETFLAG_RELAY);
}

void*
netPacket::GetPayload()
{
    return m_Data + BYTE_SIZEOF_HEADER;
}

const void*
netPacket::GetPayload() const
{
    return m_Data + BYTE_SIZEOF_HEADER;
}

//protected:

netPacket::netPacket()
{
}

///////////////////////////////////////////////////////////////////////////////
// netBundle
///////////////////////////////////////////////////////////////////////////////

void
netBundle::ResetHeader(const unsigned sizeofPayload,
                        const unsigned channelId,
                        const netSequence firstUnreliableSeq,
                        const netSequence expectedSeq,
                        const unsigned ackBits,
                        const unsigned flags)
{
    datBitBuffer::WriteUnsigned(m_Data,
                                 sizeofPayload + BYTE_SIZEOF_HEADER,
                                 BIT_SIZEOF_SIZE,
                                 BIT_OFFSETOF_SIZE);
    datBitBuffer::WriteUnsigned(m_Data,
                                 flags,
                                 BIT_SIZEOF_FLAGS,
                                 BIT_OFFSETOF_FLAGS);
    datBitBuffer::WriteUnsigned(m_Data,
                                 channelId,
                                 BIT_SIZEOF_CHANNEL_ID,
                                 BIT_OFFSETOF_CHANNEL_ID);
    datBitBuffer::WriteUnsigned(m_Data,
                                 firstUnreliableSeq,
                                 BIT_SIZEOF_FIRST_SEQ,
                                 BIT_OFFSETOF_FIRST_SEQ);
    datBitBuffer::WriteUnsigned(m_Data,
                                 expectedSeq,
                                 BIT_SIZEOF_ACK,
                                 BIT_OFFSETOF_ACK);
    datBitBuffer::WriteUnsigned(m_Data,
                                 ackBits,
                                 BIT_SIZEOF_UO_ACK_BITS,
                                 BIT_OFFSETOF_UO_ACK_BITS);
}

unsigned
netBundle::GetSize() const
{
    unsigned size;
    datBitBuffer::ReadUnsigned(m_Data,
                                size,
                                BIT_SIZEOF_SIZE,
                                BIT_OFFSETOF_SIZE);
    return size;
}

unsigned
netBundle::GetSizeofPayload() const
{
    return this->GetSize() - BYTE_SIZEOF_HEADER;
}

unsigned
netBundle::GetFlags() const
{
    unsigned flags;
    datBitBuffer::ReadUnsigned(m_Data,
                                flags,
                                BIT_SIZEOF_FLAGS,
                                BIT_OFFSETOF_FLAGS);
    return flags;
}

unsigned
netBundle::GetChannelId() const
{
    unsigned channelId;
    datBitBuffer::ReadUnsigned(m_Data,
                                channelId,
                                BIT_SIZEOF_CHANNEL_ID,
                                BIT_OFFSETOF_CHANNEL_ID);
    return channelId;
}

netSequence
netBundle::GetFirstUnreliableSeq() const
{
    unsigned seq;
    datBitBuffer::ReadUnsigned(m_Data,
                                seq,
                                BIT_SIZEOF_FIRST_SEQ,
                                BIT_OFFSETOF_FIRST_SEQ);
    return netSequence(seq);
}

netSequence
netBundle::GetExpectedSeq() const
{
    unsigned ack;
    datBitBuffer::ReadUnsigned(m_Data,
                                ack,
                                BIT_SIZEOF_ACK,
                                BIT_OFFSETOF_ACK);
    return netSequence(ack);
}

unsigned
netBundle::GetUoAckBits() const
{
    unsigned bits;
    datBitBuffer::ReadUnsigned(m_Data,
                                bits,
                                BIT_SIZEOF_UO_ACK_BITS,
                                BIT_OFFSETOF_UO_ACK_BITS);
    return bits;
}

bool
netBundle::HasSyn() const
{
    return (0 != (this->GetFlags() & FLAG_SYN));
}

bool
netBundle::HasAck() const
{
    return (0 != (this->GetFlags() & FLAG_ACK));
}

bool
netBundle::HasTerm() const
{
    return (0 != (this->GetFlags() & FLAG_TERM));
}

void*
netBundle::GetPayload()
{
    return m_Data + BYTE_SIZEOF_HEADER;
}

const void*
netBundle::GetPayload() const
{
    return m_Data + BYTE_SIZEOF_HEADER;
}

///////////////////////////////////////////////////////////////////////////////
// netFrame
///////////////////////////////////////////////////////////////////////////////

netFrame::netFrame()
{
    datBitBuffer::WriteUnsigned(m_Data,
                                 0,
                                 BIT_SIZEOF_SIZE,
                                 BIT_OFFSETOF_SIZE);
}

void
netFrame::ResetHeader(const unsigned sizeofPayload,
                        const netSequence seq,
                        const netSequence seqDep,
                        const unsigned sendFlags)
{
    const unsigned size = (NET_SEND_RELIABLE & sendFlags)
                        ? sizeofPayload + BYTE_SIZEOF_RHEADER
                        : sizeofPayload + BYTE_SIZEOF_UHEADER;

    datBitBuffer::WriteUnsigned(m_Data,
                                 size,
                                 BIT_SIZEOF_SIZE,
                                 BIT_OFFSETOF_SIZE);
    datBitBuffer::WriteUnsigned(m_Data,
                                 sendFlags,
                                 BIT_SIZEOF_SEND_FLAGS,
                                 BIT_OFFSETOF_SEND_FLAGS);
    datBitBuffer::WriteUnsigned(m_Data,
                                 seqDep,
                                 BIT_SIZEOF_SEQ,
                                 BIT_OFFSETOF_SEQ_DEP);

	if((NET_SEND_RELIABLE & sendFlags))
	{
		datBitBuffer::WriteUnsigned(m_Data,
									 seq,
									 BIT_SIZEOF_SEQ,
									 BIT_OFFSETOF_SEQ);
	}

	CompileTimeAssert(BIT_SIZEOF_SIZE + BIT_SIZEOF_SEND_FLAGS + BIT_SIZEOF_SEQ <= BIT_SIZEOF_UHEADER);
	CompileTimeAssert(BIT_SIZEOF_SIZE + BIT_SIZEOF_SEND_FLAGS + BIT_SIZEOF_SEQ + BIT_SIZEOF_SEQ <= BIT_SIZEOF_RHEADER);
}

unsigned
netFrame::GetSize() const
{
    unsigned size;
    datBitBuffer::ReadUnsigned(m_Data,
                                size,
                                BIT_SIZEOF_SIZE,
                                BIT_OFFSETOF_SIZE);
    return size;
}

unsigned
netFrame::GetSizeofHeader() const
{
    return this->IsReliable() ? BYTE_SIZEOF_RHEADER : BYTE_SIZEOF_UHEADER;
}

unsigned
netFrame::GetSizeofPayload() const
{
    return this->GetSize() - this->GetSizeofHeader();
}

netSequence
netFrame::GetSeq() const
{
    //Only reliable frames have sequence numbers.  Sequence numbers for
    //unreliable frames are offsets from the initial sequence in the
    //bundle.
    netAssert(this->IsReliable());

    unsigned seq;
    datBitBuffer::ReadUnsigned(m_Data,
                                seq,
                                BIT_SIZEOF_SEQ,
                                BIT_OFFSETOF_SEQ);
    return netSequence(seq);
}

netSequence
netFrame::GetSeqDep() const
{
    unsigned seq;
    datBitBuffer::ReadUnsigned(m_Data,
                                seq,
                                BIT_SIZEOF_SEQ,
                                BIT_OFFSETOF_SEQ_DEP);
    return netSequence(seq);
}

unsigned
netFrame::GetSendFlags() const
{
    unsigned flags;
    datBitBuffer::ReadUnsigned(m_Data,
                                flags,
                                BIT_SIZEOF_SEND_FLAGS,
                                BIT_OFFSETOF_SEND_FLAGS);
    return flags;
}

void*
netFrame::GetPayload()
{
    netAssert(this->GetSize() <= MAX_BYTE_SIZEOF_FRAME
            && this->IsReliable() 
                ? this->GetSize() >= BYTE_SIZEOF_RHEADER
                : this->GetSize() >= BYTE_SIZEOF_UHEADER);

    return m_Data + this->GetSizeofHeader();
}

const void*
netFrame::GetPayload() const
{
    netAssert(this->GetSize() <= MAX_BYTE_SIZEOF_FRAME
            && this->IsReliable() 
                ? this->GetSize() >= BYTE_SIZEOF_RHEADER
                : this->GetSize() >= BYTE_SIZEOF_UHEADER);

    return m_Data + this->GetSizeofHeader();
}

bool
netFrame::IsReliable() const
{
    return 0 != (this->GetSendFlags() & NET_SEND_RELIABLE);
}

bool
netFrame::Validate(const void* buf, const unsigned sizeofBuf)
{
    bool isValid = false;
    if(sizeofBuf >= BYTE_SIZEOF_UHEADER)
    {
        const netFrame* frame = (const netFrame*) buf;
        const unsigned fsize = frame->GetSize();
        const unsigned fpSize = frame->GetSizeofPayload();

        isValid =
            //Ensure the frame size is within limits
            fsize <= sizeofBuf
            && fsize <= MAX_BYTE_SIZEOF_FRAME
            //Ensure the frame payload size is within limits
            && fpSize <= MAX_BYTE_SIZEOF_PAYLOAD
            && fsize == fpSize + frame->GetSizeofHeader();
    }

    return isValid;
}

///////////////////////////////////////////////////////////////////////////////
//  netOutFrame
///////////////////////////////////////////////////////////////////////////////

netOutFrame::netOutFrame(netFrame* frame)
    : m_Frame(frame)
    , m_LastSendTime(0)
	, m_SendCount(0)
	, m_SendFrame(0)
	, m_PackFrame(0)
    , m_Seq(0)
    , m_NotifyAck(false)
	, m_QueueTime(0)
{
}

void
netOutFrame::Reset(const unsigned sizeofPayload,
                            const netSequence seq,
                            const netSequence seqDep,
                            const unsigned sendFlags,
                            const bool notifyAck)
{
    m_Frame->ResetHeader(sizeofPayload,
                        seq,
                        seqDep,
                        sendFlags & NET_SEND_FLAG_HEADER_MASK);
    m_NotifyAck = notifyAck;
    m_Seq = seq;
}

unsigned
netOutFrame::GetSize() const
{
    return m_Frame->GetSize();
}

unsigned
netOutFrame::GetSizeofPayload() const
{
    return m_Frame->GetSizeofPayload();
}

netSequence
netOutFrame::GetSeq() const
{
    return m_Seq;
}

netSequence
netOutFrame::GetSeqDep() const
{
    return m_Frame->GetSeqDep();
}

unsigned
netOutFrame::GetSendFlags() const
{
    return m_Frame->GetSendFlags();
}

bool
netOutFrame::IsReliable() const
{
    return m_Frame->IsReliable();
}

void*
netOutFrame::GetPayload()
{
    return m_Frame->GetPayload();
}

const void*
netOutFrame::GetPayload() const
{
    return m_Frame->GetPayload();
}

}   //namespace rage
