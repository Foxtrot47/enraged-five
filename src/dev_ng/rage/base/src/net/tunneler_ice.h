// 
// net/tunneler_ice.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_TUNNELER_ICE_H
#define NET_TUNNELER_ICE_H

#include "atl/inlist.h"
#include "connectionmanager.h"
#include "netaddress.h"
#include "peeraddress.h"
#include "status.h"
#include "system/criticalsection.h"

namespace rage
{

class netSocket;
class netIceSession;
class netIceSessionOffer;
class netIceSessionAnswer;
class netIceSessionPing;
class netIceSessionPong;
class netIceSessionRelayRouteCheck;
class netIceSessionPortOpener;
class netIceSessionDebugInfo;

//PURPOSE
//  Manages request to open communication tunnels to remote peers.
class netIceTunneler
{
	friend class netIceSession;
public:
    //PARAMS
	//  cxnMgr		- Connection manager used for sending receiving packets.
    //  skt         - Socket used to communicate with peers.
    static bool Init(netConnectionManager* cxnMgr);

    static void Shutdown();

	//PURPOSE
	//  Call this on a regular interval to update pending tunnel requests.
	static void Update();

    //PURPOSE
    //  Opens a tunnel to a remote peer.
    //PARAMS
    //  peerAddr    - Peer address for the remote peer.
    //  addr        - On successful completion, contains the network address of the peer.
	//  relayAddr	- On successful completion, contains the relay address of the peer if
	//				  the remote peer was successfully reached via the relay or we learned
	//				  of a different relay address than was stored in the peer address.
	//  bilateral	- Set to true if it is expected that the remote peer will be attempting a connection
	//				  to the local peer at the same time. This is only used as hint during tunneling.
    //  status      - Optional.  Can be polled for completion.
    //RETURNS
    //  True on successful queuing of the request.
    //NOTES
    //  addr and status must remain valid for the duration of the
    //  asynchronous request.  They should not be stack variables.
	//  NAT traversal can be initiated using a secure relay address obtained from
	//  presence/matchmaking. However, once tunneling is complete, it is assumed
	//  that we can know the remote player's real address. After that point we only 
	//  use plaintext addresses to minimize relay server CPU usage.
    static bool OpenTunnel(const netPeerAddress& peerAddr,
						   netAddress* addr,
						   netAddress* relayAddr,
						   const bool bilateral,
						   u64 sessionToken,
						   u64 sessionId,
						   const unsigned tunnelRequestId,
						   netStatus* status);

    //PURPOSE
    //  Close the tunnel associated with the given address.
    //  Further sends to that address will not arrive.
    static bool CloseTunnel(const netAddress& addr);

    //PURPOSE
    //  Cancels a pending tunnel request.
	//	This is not an asynchronous function. The status object
	//  is used to look up and cancel the internal tunnel request.
	//  Pass the same status object that was passed to OpenTunnel().
    static void CancelRequest(netStatus* status);

	//PURPOSE
	//  Returns true if there is an active session with this peer.
	static bool HasActiveSession(const netPeerAddress& peerAddr);

	//PURPOSE
	//  Returns whether the local peer is failing NAT traversal
	//  more than the average peer should.
	static bool ShouldShowConnectivityTroubleshooting();

	//PURPOSE
	//  Returns the number of failed NAT traversals.
	static unsigned GetNumFailedNatTraversals();

	//PURPOSE
	//  Returns the number of successful NAT traversals.
	static unsigned GetNumSuccessfulNatTraversals();

	enum
	{
		DEFAULT_NUM_FAILS_BEFORE_WARNING = 15,
		DEFAULT_NAT_FAIL_RATE_BEFORE_WARNING = 50,
		DEFAULT_ROLE_TIE_BREAKER_STRATEGY = 0, // RTBS_RANDOM
		DEFAULT_SYMMETRIC_SOCKET_MODE = 3, // PP_SYM_AND_NON_SYMMETRIC
	};

	static void SetNumNatFailsBeforeWarning(const unsigned numNatFailsBeforeWarning); 
	static void SetNatFailRateBeforeWarning(const unsigned natFailRateBeforeWarning); 
	static void SetAllowSymmetricInference(const bool allowSymmetricInference); 
	static void SetSymmetricSocketMode(const unsigned symmetricSocketMode);
	static void SetAllowCanonicalCandidate(const bool allowCanonicalCandidate); 
	static void SetAllowSameHostCandidate(const bool allowSameHostCandidate);
	static void SetAllowCrossRelayCheck(const bool allowCrossRelayCheck); 
	static void SetAllowCrossRelayCheckDirectOfferTimedOut(const bool allowCrossRelayCheck); 
	static void SetAllowCrossRelayCheckRemoteStrict(const bool allowCrossRelayCheck); 
	static void SetAllowCrossRelayCheckLocalHost(const bool allowCrossRelayCheck); 
	static void SetAllowRelayRouteCheck(const bool allowRelayRouteCheck);
	static void SetAllowPeerReflexiveList(const bool allowPeerReflexiveList); 
	static void SetAllowPresenceQuery(const bool allowPresenceQuery); 
	static void SetAllowPeerReflexiveHighestPlusOne(const bool peerReflexiveHighestPlusOne);
	static void SetAllowIceTunnelerTelemetry(const bool allowIceTunnelerTelemetry);
	static void SetAllowLocalRelayRefresh(const bool allowLocalRelayRefresh);
	static void SetPeerReflexiveTimeoutSec(const unsigned peerReflexiveTimeoutSec);
	static void SetMaxFailedSymmetricInferenceTests(const unsigned maxFailedSymmetricInferenceTests);
	static void SetQuickConnectTimeoutMs(const unsigned quickConnectTimeoutMs);
	static void SetRoleTieBreakerStrategy(const unsigned roleTieBreakerStrategy);
	static void SetAllowDirectOffers(const bool allowDirectOffers);
	static void SetAllowDirectAnswers(const bool allowDirectAnswers);
	static void SetAllowReservedCandidates(const bool allowReservedCandidates);
	static void SetAllowReservedRfc1918Candidates(const bool allowReservedRfc1918Candidates);
	static void SetRequireRelaySuccessForQuickConnect(const bool requireRelaySuccessForQuickConnect);

private:
	static void GatherCandidates();
	static void OnCxnEvent(netConnectionManager* UNUSED_PARAM(cxnMgr), const netEvent* evt);
	static void ReceiveOffer(const netIceSessionOffer* msg, const netAddress& sender);
	static void ReceiveAnswer(const netIceSessionAnswer* msg, const netAddress& sender);
	static void ReceivePing(const netIceSessionPing* msg, const netAddress& sender);
	static void ReceivePong(const netIceSessionPong* msg, const netAddress& sender);
	static void ReceiveRelayRouteCheck(const netIceSessionRelayRouteCheck* msg, const netAddress& sender);
	static void ReceivePortOpener(const netIceSessionPortOpener* msg, const netAddress& sender);
	static void ReceiveDebugInfo(const netIceSessionDebugInfo* msg, const netAddress& sender);
	static netIceSession* FindSessionById(const unsigned id);
	static unsigned GetNextSessionId();
	static netIceSession* AllocateSession();
	static void SendTerminatePong(const netIceSessionPing* msg, const netAddress& sender);
	static unsigned GetNumConcurrentSessions() {return m_NumConcurrentSessions;}
	static unsigned GetPeakConcurrentSessions() {return m_PeakConcurrentSessions;}
	static unsigned GetTotalSessions() {return m_TotalSessions;}
	static bool AddPeerReflexiveAddress(const netAddress& senderAddr, const netSocketAddress& addr);
	static unsigned GetPeerReflexiveAddresses(netSocketAddress* addrs, unsigned maxAddrs);

	static unsigned s_NextId;
	static sysCriticalSectionToken m_Cs;
	static sysCriticalSectionToken m_CsTunnelList;	
	static netConnectionManager* m_CxnMgr;
	static netConnectionManager::Delegate m_CxnEventDelegate;
	static bool m_Initialized;
	static unsigned m_NumConcurrentSessions;
	static unsigned m_PeakConcurrentSessions;
	static unsigned m_TotalSessions;
	static unsigned m_NumFailedSessions;
	static unsigned m_NumSuccessfulSessions;

	static netSocketAddress sm_LastRelayPingResult;
	static unsigned sm_NumFailsBeforeWarning;
	static unsigned sm_FailRateBeforeWarning;
	static bool sm_AllowCanonicalCandidate;
	static bool sm_AllowSameHostCandidate;
	static bool sm_AllowSymmetricInference;
	static unsigned sm_SymmetricSocketMode;
	static bool sm_AllowCrossRelayCheck;
	static bool sm_AllowRelayRouteCheck;
	static bool sm_AllowCrossRelayCheckDirectOfferTimedOut;
	static bool sm_AllowCrossRelayCheckRemoteStrict;
	static bool sm_AllowCrossRelayCheckLocalHost;
	static bool sm_AllowPeerReflexiveList;
	static bool sm_AllowPresenceQuery;
	static bool sm_PeerReflexiveHighestPlusOne;
	static bool sm_AllowIceTunnelerTelemetry;
	static bool sm_AllowLocalRelayRefresh;
	static unsigned sm_PeerReflexiveTimeoutSec;
	static unsigned sm_MaxFailedSymmetricInferenceTests;
	static unsigned sm_QuickConnectTimeoutMs;
	static unsigned sm_RoleTieBreakerStrategy;
	static bool sm_AllowDirectOffers;
	static bool sm_AllowDirectAnswers;
	static bool sm_AllowReservedCandidates;
	static bool sm_AllowReservedRfc1918Candidates;
	static bool sm_RequireRelaySuccessForQuickConnect;

	struct PeerReflexiveCandidate
	{
		netIpAddress m_SenderIp;
		netSocketAddress m_Addr;
		unsigned m_NumHits;
		u64 m_Timestamp;
	};
	static const unsigned MAX_PEER_REFLEXIVE_CANDIDATES = 10;
	typedef atFixedArray<PeerReflexiveCandidate, MAX_PEER_REFLEXIVE_CANDIDATES> PeerReflexiveCandidates;
	static PeerReflexiveCandidates m_PeerReflexiveCandidates;
};

}   //namespace rage

#endif  //NET_TUNNELER_ICE_H
