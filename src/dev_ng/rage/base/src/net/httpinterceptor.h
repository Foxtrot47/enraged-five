// 
// net/httpinterceptor.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_HTTP_INTERCEPTOR_H 
#define NET_HTTP_INTERCEPTOR_H 

#define  ENABLE_HTTP_INTERCEPTOR __BANK

#if ENABLE_HTTP_INTERCEPTOR

#include "http.h"

namespace rage
{

class sysMemAllocator;
class netStatus;
class netHttpRequest;
class netHttpFilter;
struct netHttpInterceptRule;

//PURPOSE
//  Intercepts http requests and generates responses based on
//  a set of rules configured via XML. Data is not sent across
//  the network when a request is intercepted. This is useful for 
//  injecting responses to specific requests (eg. for failure testing).
//  RAG widgets allow individual rules to be enabled/disabled at runtime.
class netHttpInterceptor : public netHttpFilter
{
public:
    netHttpInterceptor(netHttpInterceptRule* rule,
					   netHttpFilter* filter);
    virtual ~netHttpInterceptor();

    virtual bool CanFilterRequest() const;
    virtual bool CanFilterResponse() const;
	virtual u32 DelayMs() const;
	virtual bool CanIntercept() const;
	
    virtual bool FilterRequest(const u8* data,
                               const unsigned dataLen,
                               const bool finalCall,
                               datGrowBuffer& output);

    virtual bool FilterResponse(const u8* data,
                                const unsigned dataLen,
                                const bool allDataReceived,
                                fiHandle& outputHandle,
                                const fiDevice* outputDevice,
                                unsigned* numProcessed,
                                bool* hasDataPending);

    virtual const char* GetUserAgentString() const;

    virtual bool ProcessRequestHeader(class netHttpRequest* request);

    virtual bool ProcessResponseHeader(const int statusCode, const char* header);

	virtual void Receive(void* buf,
						 const unsigned bufSize,
						 unsigned* bytesReceived);

    virtual bool AllowSucceeded(class netHttpRequest* request);
    virtual void DisableHttpDump(bool bDisabled);
private:
	netHttpFilter* m_Filter;
	netHttpInterceptRule* m_Rule;
	const fiDevice *m_Device;
	fiHandle m_hFile;
	mutable u32 m_DelayStartTime;
};

//PURPOSE
//  Class that parses the rules file, sets up RAG widgets to
//  enable/disable individual rules at runtime, and creates/destroys
//  interceptors.
class netHttpInterception
{
public:
	static netHttpFilter* CreateInterceptor(netHttpRequest* request,
												 netHttpFilter* filter);
	static void DestroyInterceptor(netHttpInterceptor* interceptor);

	static bool Init();
	static void AddWidgets();
	static void Shutdown();
	static bool EnableRuleByName(const char* name);

private:
	static netHttpInterceptRule* MatchRule(const char* uri);
};

} // namespace rage

#endif // ENABLE_HTTP_INTERCEPTOR
#endif // NET_HTTP_INTERCEPTOR_H 
