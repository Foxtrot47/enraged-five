// 
// net/netratelimit.h 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_RATE_LIMIT_H
#define NET_RATE_LIMIT_H

#include "diag/stats.h"
#include "net/netdiag.h"
#include "net/netutil.h"

#define USE_TOKEN_STATS (RSG_BANK && __STATS && 0)
#if USE_TOKEN_STATS
#define TOKEN_STATS_ONLY(...) __VA_ARGS__
#else
#define TOKEN_STATS_ONLY(...)
#endif

namespace rage
{

enum netRateLimitFlags : u8
{
    NET_RATELIMITFLAG_BLOCK,
    NET_RATELIMITFLAG_TELEMETRY,

    //count, add other flags above
    NET_RATELIMITFLAG_COUNT
};

enum class netRateLimitType : u8
{
	Millisecond,
    Second,
    Minute,
    Hour,
    Invalid,
    Count
};

//PURPOSE
// Scheduling algorithm.
// 
// The token bucket algorithm is based on an analogy of a fixed capacity bucket 
//   into which tokens, normally representing a unit of bytes or a single packet 
//   of predetermined size, are added at a fixed rate. 
//  
//  When a packet is to be checked for conformance to the defined limits, 
//   the bucket is inspected to see if it contains sufficient tokens at that time. 
//  
//  If so, the appropriate number of tokens, e.g. equivalent to the length 
//   of the packet in bytes, are removed ("cashed in"), and the packet is 
//   passed, e.g., for transmission. 
//  
struct netTokenBucketStats
{
	static const unsigned MAX_BUCKET_NAME_SIZE = 128;

	explicit netTokenBucketStats(const float tokens, const float fillrate OUTPUT_ONLY(, const char* bucketName))
		: m_tokens(tokens)
		, m_capacity(tokens)
		, m_fillrate(fillrate)
		, m_lasttimefill(0)
		, m_lastconsume(0)
		, m_avgTokenPerMin(0.0f)
		, m_timepassed(0)
		, m_consumed(0.0f)
	{
		OUTPUT_ONLY(safecpy(m_bucketname, bucketName));
	}

	//Current number of available tokens.
	float m_tokens;

	//Bucket max capacity.
	float m_capacity;

	//New tokens are added at the rate of r bytes/sec.
	float m_fillrate;

	//Time of last refill.
	u32  m_lasttimefill;

	//Most recent time when consume has been successfully called
	u32 m_lastconsume;

	// Debug name of the bucket. Used for logging
	OUTPUT_ONLY(char m_bucketname[MAX_BUCKET_NAME_SIZE]);

	//Average number of tokens consumed per minute.
	float m_avgTokenPerMin;

	//The sum of times passed between consumes (aka the total time since the 1st consume)
	u32 m_timepassed;

	//The total of consumed tokens
	float m_consumed;
};

BANK_ONLY(class TokenBucketProfiler;)

class netTokenBucket
{
	friend class TokenBucketProfiler;

public:

	//PARAMS
	//   tokens    -> Capacity of the token bucket.
	//   fillrate  -> New tokens are added at the rate of r bytes/sec.
	netTokenBucket()
		: m_stats(0.0f, 0.0f OUTPUT_ONLY(, ""))
#if USE_TOKEN_STATS
		, m_profiler(nullptr)
#endif // USE_TOKEN_STATS
	{

	}

	//PARAMS
	//   tokens    -> Capacity of the token bucket.
	//   fillrate  -> New tokens are added at the rate of r bytes/sec.
	netTokenBucket(const float tokens, const float fillrate
#if USE_TOKEN_STATS
		, TokenBucketProfiler* profiler
#endif // TOKEN_STATS
		OUTPUT_ONLY(, const char* bucketName)
	);

	//PURPOSE
	//  Deals with refilling the bucket.
	void RefillTokens();

	//PURPOSE
	//  Consume x tokens from the bucket.
	bool Consume(const float tokens);

	//PURPOSE
	//  Check if we can consume x tokens from the bucket.
	bool CanConsume(const float tokens);

	//PURPOSE
	//  Consume x tokens from the bucket.
	void Reset(const float tokens, const float fillrate);

	//PURPOSE
	//  Get the estimate time until given number of token is ready.
	float WaitTime(float tokens);

	//PURPOSE
	//  Returns the bucket capacity.
	float GetCapacity() const { return m_stats.m_capacity; }

	//PURPOSE
	//  Returns the bucket fill rate.
	float GetFillRate() const { return m_stats.m_fillrate; }

	float GetTokens() const { return m_stats.m_tokens; }

	float GetAvgTokenPerMinute() const { return m_stats.m_avgTokenPerMin; }
	float GetConsumed() const { return m_stats.m_consumed; }

	//PURPOSE
	//  Returns the most recent time when consume has been successfully called
	u32 GetLastConsumeTime() const { return m_stats.m_lastconsume; }

	OUTPUT_ONLY(const char* GetName() const { return m_stats.m_bucketname; });

	const netTokenBucketStats& GetStats() const { return m_stats; }

private:

	netTokenBucketStats m_stats;

#if USE_TOKEN_STATS
	// A pointer to a profiler can be set to display on screen infos about the bucket
	TokenBucketProfiler* m_profiler;
#endif // USE_TOKEN_STATS
};

#if USE_TOKEN_STATS
class TokenBucketProfiler
{
public:

	TokenBucketProfiler(const char* bucketName);

	void Update(const netTokenBucket& bucket);

private:

	pfPage			PFPAGE_TokenBucket;
	pfGroup			PFGROUP_TokenBuketG;
	pfPageLink		PFPAGELINK_TokenBuket;
	pfValueT<float> PFVALUE_Capacity;
	pfValueT<float> PFVALUE_Tokens;
	pfValueT<float> PFVALUE_AvgTokensPerMinute;
};
#endif // USE_TOKEN_STATS

struct netRateLimitIntervalLimit
{
    netRateLimitIntervalLimit()
        : m_Type(netRateLimitType::Invalid)
        , m_MaxRequests(0)
        , m_Interval(0)
    {}

	netRateLimitIntervalLimit(const unsigned maxRequests, const unsigned interval, const netRateLimitType type OUTPUT_ONLY(, const char* ruleName))
	{
		Init(maxRequests, interval, type OUTPUT_ONLY(, ruleName));
	}

    void Init(const unsigned maxRequests, const unsigned interval, const netRateLimitType type OUTPUT_ONLY(, const char* ruleName))
    {
        switch (type)
        {
		case rage::netRateLimitType::Millisecond:
			m_Interval = interval;
			break;
		case rage::netRateLimitType::Second:
			m_Interval = interval * 1000;
            break;
        case rage::netRateLimitType::Minute:
			m_Interval = (interval * 1000) * 60;
            break;
        case rage::netRateLimitType::Hour:
			m_Interval = (((interval * 1000) * 60) * 60);
            break;
        case rage::netRateLimitType::Invalid:
        case rage::netRateLimitType::Count:
            netAssertf(false, "Invalid rate limit type passed");
            break;
        }

		m_TokenBucket = netTokenBucket((float)maxRequests, ((float)maxRequests / ((float)m_Interval / 1000.0f)) TOKEN_STATS_ONLY(, nullptr) OUTPUT_ONLY(, ruleName));
    }

    netTokenBucket m_TokenBucket;
    netRateLimitType m_Type;
    unsigned m_MaxRequests;
    unsigned m_Interval;
};

}   //namespace rage

#endif  //NET_RATE_LIMIT_H
