// 
// net/time.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "time.h"

#include "net.h"
#include "net/netdiag.h"
#include "system/interlocked.h"
#include "system/timer.h"

// maximum value of timestep allowed - accounts for invalid jumps
static const unsigned MAX_TIMESTEP = 5000;

namespace rage
{

//////////////////////////////////////////////////////////////////////////
//  netStopWatch
//////////////////////////////////////////////////////////////////////////
static inline unsigned CurTime()
{
    return sysTimer::GetSystemMsTime() | 0x01;
}

netStopWatch::netStopWatch()
: m_StartTime(0)
, m_Elapsed(0)
, m_IsRunning(false)
{
}

netStopWatch::netStopWatch(const bool start)
{
    if(start)
    {
        this->Start();
    }
}

void
netStopWatch::Start()
{
    //If start time is zero then we've never started.
    //After starting the LSB of m_StartTime will always be 1.
    if(0 == m_StartTime)
    {
        Reset();
    }

    m_IsRunning = true;
}

void
netStopWatch::Stop()
{
    if(m_IsRunning)
    {
        m_Elapsed = CurTime() - m_StartTime;
    }

    m_IsRunning = false;
}

void
netStopWatch::Reset()
{
    Stop();
    m_StartTime = CurTime();
    m_Elapsed = 0;
}

void
netStopWatch::Restart()
{
    Reset();
    Start();
}

bool
netStopWatch::IsRunning() const
{
    return m_IsRunning;
}

unsigned
netStopWatch::GetElapsedMilliseconds() const
{
    if(m_IsRunning)
    {
        m_Elapsed = CurTime() - m_StartTime;
    }

    return m_Elapsed;
}

//////////////////////////////////////////////////////////////////////////
//  netTimeStep
//////////////////////////////////////////////////////////////////////////
netTimeStep::netTimeStep()
{
    this->Init(0);
}

void
netTimeStep::Init(const unsigned curTime)
{
    m_CurTime = curTime;
    m_TimeStep = 0;
}

void
netTimeStep::SetTime(const unsigned curTime)
{
    const unsigned adjTime = curTime | 0x01;

    const unsigned oldTime = sysInterlockedExchange(&m_CurTime, adjTime);

    if(oldTime)
    {
        unsigned timeStep = (adjTime - oldTime);

		if(adjTime < oldTime)
		{
			timeStep = adjTime + (~0u - oldTime + 1);
			netDebug("netTimeStep :: Time has wrapped. Cur: %u, Old: %u, Step: %ums", adjTime, oldTime, timeStep);
		}

		if(timeStep > MAX_TIMESTEP)
		{
			netDebug("netTimeStep :: Timestep %ums is greater than max %ums. Capping", timeStep, MAX_TIMESTEP);
			timeStep = MAX_TIMESTEP;
		}

        sysInterlockedExchange(&m_TimeStep, timeStep);
        Assert((int) m_TimeStep >= 0);
    }
}

void
netTimeStep::AddTime(const unsigned deltaTime)
{
    this->SetTime(m_CurTime + deltaTime);
}

unsigned
netTimeStep::GetCurrent() const
{
    return m_CurTime;
}

int
netTimeStep::GetTimeStep() const
{
    return (int) m_TimeStep;
}

bool
netTimeStep::HasTimeStep() const
{
    return (0 != m_CurTime);
}

//////////////////////////////////////////////////////////////////////////
//  netTimeout
//////////////////////////////////////////////////////////////////////////
netTimeout::netTimeout()
{
    this->Clear();
}

void
netTimeout::InitMilliseconds(const int timeoutMs)
{
    if(netVerify(timeoutMs > 0))
    {
        m_Timeout = timeoutMs;
        this->Reset();
    }
}

void
netTimeout::InitSeconds(const int timeoutSeconds)
{
    this->InitMilliseconds(timeoutSeconds * 1000);
}

void
netTimeout::Clear()
{
    m_CurTime = 0;
	m_LongFrameThresholdMs = 0;
	m_Timeout = 0;
    m_Countdown = 0;
}

void
netTimeout::Reset()
{
    m_CurTime = sysTimer::GetSystemMsTime() | 0x01;
    m_Countdown = m_Timeout;
}

void
netTimeout::SetLongFrameThreshold(const unsigned longFrameThresholdMs)
{
	m_LongFrameThresholdMs = longFrameThresholdMs;
}

void
netTimeout::Update()
{
	if(m_LongFrameThresholdMs > 0)
	{
		const unsigned msSinceTimeoutUpdate = this->GetMillisecondsSinceLastUpdate();
		if(msSinceTimeoutUpdate >= m_LongFrameThresholdMs)
		{
			netWarning("Frame time was %u milliseconds - adjusting timeout...", msSinceTimeoutUpdate);
			this->ExtendMilliseconds(msSinceTimeoutUpdate);
		}
	}

    if(!this->IsTimedOut() && m_CurTime)
    {
        const unsigned adjTime = sysTimer::GetSystemMsTime() | 0x01;
        const int delta = int(adjTime - m_CurTime);
        m_Countdown -= delta;
        m_CurTime = adjTime;
    }
}

void
netTimeout::ExtendMilliseconds(const int ms)
{
    if(!this->IsTimedOut() && m_CurTime)
    {
        if(netVerify(ms > 0)
            //Check for overflow
            && netVerify(m_Countdown+ms > m_Countdown))
        {
            m_Countdown = m_Countdown + ms;
        }
    }
}

void
netTimeout::ExtendSeconds(const int seconds)
{
    ExtendMilliseconds(seconds*1000);
}

unsigned
netTimeout::GetTimeoutIntervalMilliseconds() const
{
    return m_Timeout;
}

unsigned
netTimeout::GetTimeoutIntervalSeconds() const
{
    return m_Timeout / 1000;
}

unsigned
netTimeout::GetMillisecondsUntilTimeout() const
{
    return m_Countdown > 0 ? m_Countdown : 0;
}

unsigned
netTimeout::GetSecondsUntilTimeout() const
{
    return this->GetMillisecondsUntilTimeout() / 1000;
}

unsigned
netTimeout::GetMillisecondsSinceLastUpdate() const
{
    if(!this->IsTimedOut() && m_CurTime)
    {
        const unsigned adjTime = sysTimer::GetSystemMsTime() | 0x01;
        return (adjTime - m_CurTime);
    }

    return 0;
}

unsigned
netTimeout::GetSecondsSinceLastUpdate() const
{
    return this->GetMillisecondsSinceLastUpdate() / 1000;
}

/*bool
netTimeout::IsActive() const
{
    return 0 != m_CurTime;
}*/

bool
netTimeout::IsRunning() const
{
    return (0 != m_CurTime) && !IsTimedOut();
}

void
netTimeout::ForceTimeout()
{
    m_Countdown = 0;
    if(0 == m_CurTime)
    {
        //Make sure ForceTimeout() causes IsTimedOut() to return true.
        m_Timeout = m_CurTime = 1;
    }
}

bool
netTimeout::IsTimedOut() const
{
    return m_CurTime && (m_Countdown <= 0);
}

//////////////////////////////////////////////////////////////////////////
//  netRetryTimer
//////////////////////////////////////////////////////////////////////////
netRetryTimer::netRetryTimer()
: m_MinRetryIntervalMs(0)
, m_MaxRetryIntervalMs(0)
, m_RetryIntervalRangeMs(0)
, m_CurRetryIntervalMs(0)
, m_RetryTimeoutMs(0)
, m_RangeScale(RANGE_SCALE)
, m_Stopped(false)
{
}

netRetryTimer::~netRetryTimer()
{
    this->Shutdown();
}

bool
netRetryTimer::InitMilliseconds(const int minRetryIntervalMs,
                                const int maxRetryIntervalMs)
{
    bool success = false;

    if(netVerify(minRetryIntervalMs >= 0)
        && netVerify(minRetryIntervalMs <= maxRetryIntervalMs))
    {
        m_TimeStep.Init(sysTimer::GetSystemMsTime());
        m_MinRetryIntervalMs = minRetryIntervalMs;
        m_MaxRetryIntervalMs = maxRetryIntervalMs;
        m_RetryIntervalRangeMs = maxRetryIntervalMs - minRetryIntervalMs;
        m_Stopped = false;

        this->RestoreRange();
        this->Reset();

        success = true;
    }

    return success;
}

bool
netRetryTimer::InitSeconds(const int minRetryIntervalSecs,
                            const int maxRetryIntervalSecs)
{
    return this->InitMilliseconds(minRetryIntervalSecs * 1000,
                                maxRetryIntervalSecs * 1000);
}

bool
netRetryTimer::InitMilliseconds(const int retryIntervalMs)
{
    return this->InitMilliseconds(retryIntervalMs, retryIntervalMs);
}

bool
netRetryTimer::InitSeconds(const int retryIntervalSecs)
{
    return this->InitMilliseconds(retryIntervalSecs * 1000);
}

void
netRetryTimer::Shutdown()
{
    m_TimeStep.Init(0);
    m_MinRetryIntervalMs = m_MaxRetryIntervalMs = 0;
    m_RetryIntervalRangeMs = 0;
    m_RetryTimeoutMs = 0;
    m_RangeScale = RANGE_SCALE;
    m_Stopped = false;
}

void
netRetryTimer::Reset()
{
    Assert(this->GetMinRetryIntervalMilliseconds() >= 0);
    Assert(this->GetMinRetryIntervalMilliseconds() <= this->GetMaxRetryIntervalMilliseconds());

    if(m_RetryIntervalRangeMs)
    {
        netRandom rng((int) sysTimer::GetTicks());

        m_CurRetryIntervalMs =
            m_RetryTimeoutMs = rng.GetRanged(this->GetMinRetryIntervalMilliseconds(),
                                            this->GetMaxRetryIntervalMilliseconds());
    }
    else
    {
        m_CurRetryIntervalMs =
            m_RetryTimeoutMs = this->GetMinRetryIntervalMilliseconds();
    }

    Assert(m_RetryTimeoutMs >= this->GetMinRetryIntervalMilliseconds());
}

void
netRetryTimer::ResetIntervalMilliseconds(const int minRetryIntervalMs,
                                        const int maxRetryIntervalMs)
{
    if(netVerify(minRetryIntervalMs >= 0)
        && netVerify(minRetryIntervalMs <= maxRetryIntervalMs))
    {
        m_MinRetryIntervalMs = minRetryIntervalMs;
        m_MaxRetryIntervalMs = maxRetryIntervalMs;
        m_RetryIntervalRangeMs = maxRetryIntervalMs - minRetryIntervalMs;

        this->RestoreRange();
        this->Reset();
    }
}

void
netRetryTimer::ResetIntervalSeconds(const int minRetryIntervalSecs,
                                    const int maxRetryIntervalSecs)
{
    this->ResetIntervalMilliseconds(minRetryIntervalSecs * 1000,
                                    maxRetryIntervalSecs * 1000);
}

void
netRetryTimer::ResetIntervalMilliseconds(const int retryIntervalMs)
{
    this->ResetIntervalMilliseconds(retryIntervalMs, retryIntervalMs);
}

void
netRetryTimer::ResetIntervalSeconds(const int retryIntervalSecs)
{
    this->ResetIntervalMilliseconds(retryIntervalSecs * 1000);
}

void
netRetryTimer::ScaleRange(const unsigned scaleTimes100)
{
    m_RangeScale = unsigned(u64(m_RangeScale * scaleTimes100) / RANGE_SCALE);
}

void
netRetryTimer::ScaleRange(const unsigned scaleTimes100,
                            const unsigned upperLimitIntervalMs)
{
    this->ScaleRange(scaleTimes100);
    const unsigned newMaxIntervalMs = GetMaxRetryIntervalMilliseconds();

    if(newMaxIntervalMs > upperLimitIntervalMs)
    {
        m_RangeScale = unsigned(u64(upperLimitIntervalMs * RANGE_SCALE) / m_MaxRetryIntervalMs);
    }
}

void
netRetryTimer::RestoreRange()
{
    m_RangeScale = RANGE_SCALE;
}

void
netRetryTimer::Start()
{
    m_Stopped = false;
}

void
netRetryTimer::Stop()
{
    m_Stopped = true;
}

void
netRetryTimer::Restart()
{
    Reset(); Start();
}

void
netRetryTimer::Update()
{
    m_TimeStep.SetTime(sysTimer::GetSystemMsTime());

    if(!m_Stopped && m_RetryTimeoutMs > 0)
    {
        m_RetryTimeoutMs -= m_TimeStep.GetTimeStep();
    }
}

int
netRetryTimer::GetCurrentRetryIntervalMilliseconds() const
{
    return m_CurRetryIntervalMs;
}

int
netRetryTimer::GetCurrentRetryIntervalSeconds() const
{
    return GetCurrentRetryIntervalMilliseconds() / 1000;
}

int
netRetryTimer::GetMinRetryIntervalMilliseconds() const
{
    //If min/max are equal then scale the entire interval.
    //E.g. if min/max are 10 seconds and scale is 2 then min/max become 20 seconds.
    //Otherwise, if min is 10 and max is 15 and scale is 2 the min stays 10 but max
    //becomes 15.
    return m_RetryIntervalRangeMs
            ? m_MinRetryIntervalMs
            : (u64((u64)m_MinRetryIntervalMs * (u64)m_RangeScale) / RANGE_SCALE);
}

int
netRetryTimer::GetMinRetryIntervalSeconds() const
{
    return GetMinRetryIntervalMilliseconds() / 1000;
}

int
netRetryTimer::GetMaxRetryIntervalMilliseconds() const
{
     return GetMinRetryIntervalMilliseconds() + (u64((u64)m_RetryIntervalRangeMs * (u64)m_RangeScale) / RANGE_SCALE);
}

int
netRetryTimer::GetMaxRetryIntervalSeconds() const
{
    return GetMaxRetryIntervalMilliseconds() / 1000;
}

int
netRetryTimer::GetMillisecondsUntilRetry() const
{
    return (m_RetryTimeoutMs >= 0) ? m_RetryTimeoutMs : 0;
}

int
netRetryTimer::GetSecondsUntilRetry() const
{
    return GetMillisecondsUntilRetry() / 1000;
}

void
netRetryTimer::ForceRetry()
{
    m_RetryTimeoutMs = 0;
}

bool
netRetryTimer::IsTimeToRetry() const
{
    return m_RetryTimeoutMs <= 0;
}

bool
netRetryTimer::IsStopped() const
{
    return m_Stopped;
}

////////////////////////////////////////////////////////////////
// netRetryAndBackoffTimer
////////////////////////////////////////////////////////////////
void netRetryAndBackoffTimer::Init(unsigned firstRetryMs, unsigned initialBackoffMs, unsigned holdingBackoffMs)
{
	m_Intervals[0] = firstRetryMs;
	m_Intervals[1] = initialBackoffMs;
	m_Intervals[2] = holdingBackoffMs;
	m_CurrentIndex = 0;
	m_bInitialized = true;
}

void netRetryAndBackoffTimer::InitSeconds(unsigned firstRetry, unsigned initialBackoff, unsigned holdingBackoff)
{
	Init(firstRetry * 1000, initialBackoff * 1000, holdingBackoff * 1000);
}

void netRetryAndBackoffTimer::Increment()
{
	Assert(m_bInitialized);
	if (m_CurrentIndex < NUM_INTERVALS - 1)
	{
		m_CurrentIndex++;
	}

	m_StartTime = sysTimer::GetSystemMsTime();
}

void netRetryAndBackoffTimer::Reset()
{
	Assert(m_bInitialized);
	m_CurrentIndex = 0;
	m_StartTime = 0;
}

bool netRetryAndBackoffTimer::IsReadyToRetry()
{
	Assert(m_bInitialized);
	return sysTimer::HasElapsedIntervalMs(m_StartTime, GetCurrentInterval());
}


} // namespace rage
