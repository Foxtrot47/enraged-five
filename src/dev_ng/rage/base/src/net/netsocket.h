// 
// net/netsocket.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_NETSOCKET_H
#define NET_NETSOCKET_H

#include <stddef.h>

#include "net.h"
#include "bandwidth.h"
#include "netaddress.h"
#include "atl/inlist.h"
#include "system/criticalsection.h"

#define FAKE_DROP_LATENCY __BANK

#if FAKE_DROP_LATENCY
#include "atl/pool.h"
#endif

#if RSG_ANDROID || RSG_LINUX || RSG_NX
#include <sys/select.h>
#else // RSG_ANDROID
extern "C"
{
struct fd_set;
}
#endif // RSG_ANDROID || RSG_LINUX

extern "C"
{
	struct timeval;
}

namespace rage
{

class netHardware;

/*
PURPOSE
    netSocket provides an interface for sending and receiving packets,
    independent of the underlying hardware. netHardware maintains an array
    of netSockets, and allocates them with CreateSocket().

    After creating a socket call Bind() to bind the socket to a local port.
    Bind() will fail until the network hardware is available.  To determine
    if the hardware is available, call netHardware::IsAvailable().

    If the network hardware ever becomes unavailable all active sockets
    will become unusable.  A socket that has become unusable will never again
    be usable, and the only is to destroy it by calling
    netHardware::DestroySocket().
*/

enum netSocketBlockingType
{
    NET_SOCKET_BLOCKING_INVALID = -1,
    NET_SOCKET_BLOCKING,
    NET_SOCKET_NONBLOCKING
};

enum netAddressFamily
{
    NET_ADDRESS_FAMILY_IPV4,
    
#if IPv4_IPv6_DUAL_STACK
    NET_ADDRESS_FAMILY_IPV6,
    NET_DEFAULT_ADDRESS_FAMILY = NET_ADDRESS_FAMILY_IPV6,
#else
    NET_DEFAULT_ADDRESS_FAMILY = NET_ADDRESS_FAMILY_IPV4,
#endif
};

#if RSG_WIN32
// Note: on Windows SOCKET is unsigned (it's a handle to a kernel object rather than a file descriptor)
// From WinSock2.h - to avoid having to include Windows headers in this header
//typedef UINT_PTR SOCKET;
#if RSG_CPU_X64
typedef unsigned __int64 netSocketFd;
#else
typedef unsigned __int32 netSocketFd;
#endif
#define NET_INVALID_SOCKET_FD (INVALID_SOCKET)
#define NET_IS_VALID_SOCKET_FD(fd) (fd != NET_INVALID_SOCKET_FD)
#else
typedef int netSocketFd;
#define NET_INVALID_SOCKET_FD (-1)
#define NET_IS_VALID_SOCKET_FD(fd) (fd >= 0)
#endif

class netSocket
{
    friend class netHardware;

public:

#if FAKE_DROP_LATENCY
	void static InitFakeDrop();
	void static InitFakeLatency();
	void static InitFakeBandwidthLimit();
	void static ShutdownFakeLatency();
#endif

    enum
    {
        MAX_BUFFER_SIZE     = 1024
    };

	static const unsigned INVALID_SOCKET_ID = ~0u;

    netSocket();
    
    ~netSocket();

    //PURPOSE
    //  Returns true if the socket has been created via netHardware::CreateSocket().
    //  This does not imply the socket is ready to send/receive.
    //  Call CanSendReceive() to determine that.
    bool IsCreated() const;

    //PURPOSE
    //  Destroys the socket.
    void Destroy();

    //PURPOSE
    //  Sends a datagram to the specified destination address.
    //  Returns true on success, or false if there's an error.
    //PARAMS
    //  destination     - Destination address
    //  data            - Buffer to send.  Must be <= MAX_BUFFER_SIZE bytes.
    //  length          - Number of bytes in the buffer.
    //                    Must be <= MAX_BUFFER_SIZE bytes.
    //  sktErr          - Optional - receives error code on error.
    bool Send(const netSocketAddress& destination,
               const void *data,
               const unsigned length,
               netSocketError* sktErr = 0);

    //PURPOSE
    //  Broadcasts the packet on the local subnet.
    //PARAMS
    //  data        - Buffer to send.  Must be <= MAX_BUFFER_SIZE bytes.
    //  port        - Port to which we'll broadcast.
    //  length      - Number of bytes in the buffer.
    //                Must be <= MAX_BUFFER_SIZE bytes.
    //  sktErr      - Optional - receives error code on error.
    bool Broadcast(const unsigned short port,
                    const void *data,
                    const unsigned length,
                    netSocketError* sktErr = 0);

    //PURPOSE
    //  Receives a pending datagram (if any) and fills 
    //  'sender' with the address of the sender.
    //PARAMS
    //  sender      - Filled with sender's address.
    //  buffer      - Buffer that is filled with the datagram.
    //                Must be <= MAX_BUFFER_SIZE bytes.
    //  bufferSize  - Number of bytes in buffer.  Must be <= MAX_BUFFER_SIZE.
    //  sktErr      - Optional - receives error code on error.
    //RETURNS
    //  Returns 0 if no data is available.
    //  Returns -1 if there's an error.
    int Receive(netSocketAddress* sender,
                void *buffer,
                const int bufferSize,
                netSocketError* sktErr = 0);

    //PURPOSE
    //  Returns true if there is data pending in the socket's receive queue.
    bool IsReceivePending() const;

	//PURPOSE
	//  Returns the size in bytes of the socket's receive buffer and the number of bytes queued.
	bool GetReceiveBufferInfo(unsigned& receiveBufferQueueLength, unsigned& receiveBufferSize) const;

    //PURPOSE
    //  Returns true if it's okay to send and receive on this socket.
    //  False usually means the socket has not been bound to a port or
    //  the hardware isn't available.
    bool CanSendReceive() const;

	//PURPOSE
	//  Returns true if the socket is ready to send without blocking.
	//  This will return false if the send buffer is full.
	bool IsReadyToSend() const;

    //PURPOSE
    //  Returns true if this socket can be used to broadcast packets.
    bool CanBroadcast() const;

    //PURPOSE
    //  Returns raw handle of the socket used for sending and receiving, or
    //  -1 if not available.
    ptrdiff_t GetRawSocket() const;

    //PURPOSE
    //  Returns the protocol used by the socket.
    netProtocol GetProtocol() const;

    //PURPOSE
    //  Returns the port to which the socket is bound.
    unsigned short GetPort() const;

    //PURPOSE
    //  Returns true if the socket is in blocking mode.
    bool IsBlocking() const;

    //PURPOSE
    //  Returns the network address of the socket, which includes an IP address
    //  and a port number.
    const netSocketAddress& GetAddress() const;

    //PURPOSE
    //  Registers a packet recorder with the socket.  For each packet
    //  sent/received the recorder will be passed the destination/source
    //  address, the payload buffer, the number of bytes in the payload,
    //  and the time the packet was sent/received.
    void RegisterPacketRecorder(netPacketRecorder* recorder);

    //PURPOSE
    //  Unregisters a bandwidth recorder from the socket.
    void UnregisterPacketRecorder(netPacketRecorder* recorder);

	//PURPOSE
	//  Sets whether a socket select() must be performed before sending.
	static void SetRequireSelectOnSend(const bool requireSelectOnSend);

	//PURPOSE
	//  Returns the number of times a socket's send buffer was full when
	//  attempting to send a packet. This gives one measure of how often 
	//  the player's network capacity is exceeded.
	static unsigned GetNumSocketSendBufferOverflows();

	//PURPOSE
	//  Same as GetNumSocketSendBufferOverflows() but resets the count to
	//  0 after returning the current count.
	static unsigned ResetNumSocketSendBufferOverflows();

#if FAKE_DROP_LATENCY
    struct FakeDropSettings
    {
        FakeDropSettings()
            : m_BaselineDropPcnt(0)
            , m_MaxExtraBurstDropPcnt(0)
            , m_MinTimeBetweenBursts(0)
            , m_MaxTimeBetweenBursts(0)
            , m_MinBurstDuration(0)
            , m_MaxBurstDuration(0)
            , m_RandomSeed(0)
        {
        }

        //Baseline drop percentage that is always present.
        unsigned m_BaselineDropPcnt;
        //Range of extra drop % added to the baseline during a drop burst.
        //The actual % added to the baseline will be a random value
        //from zero to m_MaxExtraDropPcnt
        unsigned m_MaxExtraBurstDropPcnt;
        //Min time between drop bursts.
        unsigned m_MinTimeBetweenBursts;
        //Max time between drop bursts.
        unsigned m_MaxTimeBetweenBursts;
        //Min duration of a drop burst.
        unsigned m_MinBurstDuration;
        //Max duration of a drop burst.
        unsigned m_MaxBurstDuration;

        //Used to seed the random number generator for fake drops.
        int m_RandomSeed;
    };

    struct FakeLatencySettings
    {
        FakeLatencySettings()
            : m_BaselineLatency(0)
			, m_LatencyRange(0)
            , m_MaxExtraBurstLatency(0)
            , m_MinTimeBetweenBursts(0)
            , m_MaxTimeBetweenBursts(0)
            , m_MinBurstDuration(0)
            , m_MaxBurstDuration(0)
            , m_RandomSeed(0)
        {
        }

        //Baseline latency that is always present, in milliseconds.
        unsigned m_BaselineLatency;
		//The latency range (how much latency can be randomly added to m_BaselineLatency), in milliseconds.
		unsigned m_LatencyRange;
		//Range of extra latency added to the baseline during a latency burst.
        //The actual latency added to the baseline will be a random value
        //from zero to m_MaxExtraBurstLatency
        unsigned m_MaxExtraBurstLatency;
        //Min time between latency bursts.
        unsigned m_MinTimeBetweenBursts;
        //Max time between latency bursts.
        unsigned m_MaxTimeBetweenBursts;
        //Min duration of a latency burst.
        unsigned m_MinBurstDuration;
        //Max duration of a latency burst.
        unsigned m_MaxBurstDuration;

        //Used to seed the random number generator for fake latency.
        int m_RandomSeed;
    };
	
	//PURPOSE
	//  Set fake drop parameters to use as defaults when creating new sockets.
	//  Updating all extant sockets to use the global fake drop parameters will have 
	//  to be done by hand, using netHardware's list of active sockets.
	static void SetGlobalFakeDropSettings(const FakeDropSettings& fakeDropSettings);

	// PURPOSE
	// Retrieves the fake drop parameters to be used as defaults when creating new sockets.
	static void GetGlobalFakeDropSettings(FakeDropSettings& fakeDropSettingsOut);

    // PURPOSE
    // Get/set the uplink disconnected flag. Applies to all connections, TCP and UDP
    // In case of UDP all incoming and outgoing packets will be discarded.
    // For TCP we stop calling receive and send so it's similar to not having any pending
    // incoming data and to have a full TCP buffer when trying to send.
    // Connections can still be opened so it's not quite perfect. In addition this won't
    // block any Sony or Microsoft network calls.
    static void SetFakeUplinkDisconnection(const bool uplinkDisconencted);
    static bool GetFakeUplinkDisconnection();

    //PURPOSE
    //  Sets parameters for generating fake packet drops.
    void SetFakeDropSettings(const FakeDropSettings& fakeDropSettingsOut);

    //PURPOSE
    //  Retrieves the parameters for generating fake packet drops.
    void GetFakeDropSettings(FakeDropSettings& fakeDropSettingsOut) const;

	//PURPOSE
	//  Returns true if the -netfakelatency parameter is present.
	static bool IsFakeDropEnabled();

    //PURPOSE
    //  Returns true if the socket, based on the values in the FakeDrop
    //  object, may potentially fake packet drops.
    bool CanFakeDrop() const;

    //PURPOSE
    //  Returns true if the next packet received should be dropped.
    bool ComputeFakeDrop();

	//PURPOSE
	//  Set fake latency parameters to use as defaults when creating new sockets.
	//  Updating all extant sockets to use the global fake latency parameters will have 
	//  to be done by hand, using netHardware's list of active sockets.
	static void SetGlobalFakeLatencySettings(const FakeLatencySettings& fakeLatencySettings);

	// PURPOSE
	// Retrieves the fake latency parameters to be used as defaults when creating new sockets.
	static void GetGlobalFakeLatencySettings(FakeLatencySettings& output);

    //PURPOSE
    //  Sets parameters for generating fake latency.
    void SetFakeLatencySettings(const FakeLatencySettings& fakeLatency);

    //PURPOSE
    //  Retrieves the parameters for generating fake latency.
    void GetFakeLatencySettings(FakeLatencySettings& fakeLatency) const;

	//PURPOSE
	//  Returns true if the -netfakelatency parameter is present.
	static bool IsFakeLatencyEnabled();

    //PURPOSE
    //  Returns true if the socket, based on the values in the FakeLatency
    //  object, may potentially fake latency.
    bool CanFakeLatency() const;

    //PURPOSE
    //  Returns the extra latency that should be added on to the next packet
    //  received.
    unsigned ComputeFakeLatency();

	//PURPOSE
	//  Sets an artificial inbound bandwidth limit to use as the default when new netSockets are created.
	//PARAMS
	//  bytesPerSec     - Maximum bandwidth in bytes per second.
	//                    Pass zero to disable the limit.
	static void SetGlobalFakeInboundBandwidthLimit(const unsigned bytesPerSec) { sm_InboundBytesPerSecGlobal = bytesPerSec; }

	//PURPOSE
	//  Returns the value of the default fake inbound bandwidth limit in bytes/sec.
	static unsigned GetGlobalFakeInboundBandwidthLimit() { return sm_InboundBytesPerSecGlobal; }

	//PURPOSE
	//  Sets an artificial outbound bandwidth limit to use as the default when new netSockets are created.
	//PARAMS
	//  bytesPerSec     - Maximum bandwidth in bytes per second.
	//                    Pass zero to disable the limit.
	static void SetGlobalFakeOutboundBandwidthLimit(const unsigned bytesPerSec) { sm_OutboundBytesPerSecGlobal = bytesPerSec; }

	//PURPOSE
	//  Returns the value of the default fake inbound bandwidth limit in bytes/sec.
	static unsigned GetGlobalFakeOutboundBandwidthLimit() { return sm_OutboundBytesPerSecGlobal; }

    //PURPOSE
    //  Sets an artificial inbound bandwidth limit.
    //PARAMS
    //  bytesPerSec     - Maximum bandwidth in bytes per second.
    //                    Pass zero to disable the limit.
    void SetFakeInboundBandwidthLimit(const unsigned bytesPerSec);

    //PURPOSE
    //  Returns the value of the fake inbound bandwidth limit in bytes/sec.
    unsigned GetFakeInboundBandwidthLimit() const;

    //PURPOSE
    //  Sets an artificial outbound bandwidth limit.
    //PARAMS
    //  bytesPerSec     - Maximum bandwidth in bytes per second.
    //                    Pass zero to disable the limit.
    void SetFakeOutboundBandwidthLimit(const unsigned bytesPerSec);

    //PURPOSE
    //  Returns the value of the fake outbound bandwidth limit in bytes/sec.
    unsigned GetFakeOutboundBandwidthLimit() const;

	//PURPOSE
	//  Returns the maximum length of time that can be safely waited (i.e. by calling select or sleep)
	//  by the caller. -1 indicates no packet, 0 indicates a packet is ready to go immediately.
	int GetNextHeldPacketWaitTime();

	//PURPOSE
	//  Returns true if there is a held packet ready to be received.
	bool IsHeldPacketReady();

#endif  //FAKE_DROP_LATENCY

    //PURPOSE
    //  Computes the size of the header for transport layer packets (UDP+IP).
    //RETURNS
    //  Size of transport layer header.
    unsigned SizeofHeader();

    //PURPOSE
    //  Computes the size of the packet with a given payload size
    //  Includes the transport layer (UDP+IP) header.
    //PARAMS
    //  sizeofPayload   - Size of packet payload in bytes.
    //RETURNS
    //  Size of transport layer packet.
    unsigned SizeofPacket(const unsigned sizeofPayload);

    static int Select(const int nfds,
                    fd_set* readfds,
                    fd_set* writefds,
                    fd_set* exceptfds,
                    struct timeval* timeout);

    static netSocketError GetLastSocketError();

	//PURPOSE
	//  Returns the id of the socket.
	unsigned GetId() const { return m_Id; }

	//PURPOSE
	//  Gives a range of unreserved port numbers suitable for random selection.
	//  Will prefer to give the widest unreserved range within [32768, 65535].
	//  This is important for certain NAT traversal features where it is preferred
	//  to use a port that does not have an active mapping on the local NAT.
	//  Using port 0 (ephemeral) can often choose a port that was used
	//  by a recently closed socket, and may therefore still have an active mapping
	//  on the local NAT. Note that some OS's support a socket option that requests
	//  that a random port be selected when port 0 is specified. On those platforms
	//  this function will return 0-0 as the range and the socket option will be used.
	static void GetPortRangeForRandomSelection(unsigned short& lowerBound, unsigned short& upperBound);

	//PURPOSE
	//  Gives a range of port numbers suitable for P2P connections.
	//  Certain platforms (e.g. Stadia) require P2P connections to use ports
	//  in a range specified by the platform.
	static void GetPortRangeForP2pConnections(unsigned short& lowerBound, unsigned short& upperBound);

private:

#if RSG_BANK
	// output a warning if socket buffers are backed up beyond this percentage
	static const unsigned NET_SOCKET_BUFFER_WARN_PCT = 80;
#endif

	//PURPOSE
	//  Generates a unique socket id.
	unsigned NextId() const;

    //PURPOSE
    //  Clears the socket to its initial state.
    void SetInitialValues();
    
    //PURPOSE
    //  This gets called by netHardware when the hardware becomes available.
    //  It creates the actual socket descriptor and fills in m_Addr.
    bool Init(const unsigned short desiredPort,
              const netProtocol proto,
              const netSocketBlockingType blockingType,
              const netAddressFamily addressFamily,
			  const unsigned sendBufferSize,
			  const unsigned receiveBufferSize);
    
    //PURPOSE
    //  This gets called by netHardware when the hardware is either being shut
    //  down or has become unavailable.  It closes m_Socket and sets it to -1.
    void Shutdown();

    //PURPOSE
    //  Called on a regular interval by netHardware.
    void Update();

    //PURPOSE
    //  Called for all outbound packets.  Iterates over list of packet
    //  recorders and calls RecordOutboundPacket() on each.
    void RecordOutboundPacket(const netSocketAddress& addr,
                              const void* bytes,
                              const unsigned numBytes);

    //PURPOSE
    //  Called for all inbound packets.  Iterates over list of packet
    //  recorders and calls RecordInboundPacket() on each.
    void RecordInboundPacket(const netSocketAddress& addr,
		const void* bytes,
		const unsigned numBytes);

    //PURPOSE
    //  Set the desired send and receive buffers for the socket.
    //PARAMS
	//  sendBufferSize - the desired size, in bytes, of the send buffer
	//  receiveBufferSize - the desired size, in bytes, of the receive buffer
    //RETURNS
    //  True on success.
	bool SetSendReceiveBufferSizes(const unsigned sendBufferSize,
								   const unsigned receiveBufferSize);

    //Native functions implement platform-specific functionality.
	void NativeUpdate();
    void NativeShutdown();
    bool NativeGetMyAddress(netSocketAddress* addr) const;
    bool NativeBind();
    void NativeUnbind();
    bool NativeSend(const netSocketAddress& destination,
                    const void *data,
                    const unsigned length,
                    netSocketError* sktErr);
    int NativeReceive(netSocketAddress* sender,
                      void *buffer,
                      const int bufferSize,
                      netSocketError* sktErr);
    bool NativeSetBlocking();
	bool NativeGetSendReceiveBufferSizes(unsigned& sendBufferSize,
										 unsigned& receiveBufferSize) const;
	bool NativeSetSendReceiveBufferSizes(const unsigned sendBufferSize,
										 const unsigned receiveBufferSize);
	bool NativeGetReceiveBufferQueueLength(unsigned& receiveBufferQueueLength) const;
	static void NativeGetPortRangeForRandomSelection(unsigned short& lowerBound,
													 unsigned short& upperBound);
	static void NativeGetPortRangeForP2pConnections(unsigned short& lowerBound,
													unsigned short& upperBound);

    //Disallow copying/assigning
    netSocket(const netSocket&);
    netSocket& operator=(const netSocket&);

	unsigned m_Id;

    netProtocol m_Proto;

    //Native socket handle.
    ptrdiff_t m_Socket;

    //Link for use by netHardware
    inlist_node<netSocket> m_ListLink;

    //Used to synchronize access to most socket members.
    mutable sysCriticalSectionToken m_Cs;

    //List of registered packet recorders.
    typedef inlist<netPacketRecorder, &netPacketRecorder::m_ListLink> PktRecorderList;
    PktRecorderList m_PktRecorders;

    //Used to synchronize access to the collection of packet recorders.
    sysCriticalSectionToken m_CsPktRec;

    //Network address to which we're bound.
    netSocketAddress m_Addr;

    netSocketBlockingType m_BlockingType;
    netAddressFamily m_AddressFamily;

    //Network port to which we're bound.
    unsigned short m_Port;
	unsigned short m_DesiredPort; 

	unsigned m_DesiredSendBufferSize;
	unsigned m_DesiredReceiveBufferSize;

	static bool sm_RequireSelectOnSend;
	static unsigned sm_NumSocketSendBufferOverflows;

#if RSG_BANK
	unsigned m_LastDiagnosticCheckTime;
#endif

#if FAKE_DROP_LATENCY

    void UpdateBandwidthLimiters();

    struct HoldPacket
    {
        HoldPacket()
            : m_ActualReceiveTime(0)
			, m_ReceiveTime(0)
            , m_Size(-1)
            , m_Error(NET_SOCKERR_NONE)
        {
        }

		unsigned m_ActualReceiveTime;
        unsigned m_ReceiveTime;
        int m_Size;
        netSocketAddress m_Sender;
        netSocketError m_Error;
        inlist_node<HoldPacket> m_ListLink;

        u8 m_Data[MAX_BUFFER_SIZE];
    };

    enum
    {
        //Maximum number of hold packets that can be held to simulate
        //latency.
        MAX_HOLD_PACKETS    = 1000
    };

    typedef inlist<HoldPacket, &HoldPacket::m_ListLink> HoldQueue;

    HoldQueue m_HoldQueue;

    static FakeDropSettings sm_FakeDropSettingsGlobal;

    // Fake an uplink disconnection. Keeping this separate from sm_FakeDropSettingsGlobal so we can dynamically toggle it
    static bool sm_FakeUplinkDisconnection;

    FakeDropSettings m_FakeDropSettings;
    unsigned m_DropBurstStartTime;
    unsigned m_DropBurstEndTime;
    unsigned m_CurrentDropPcnt;

	static FakeLatencySettings sm_FakeLatencySettingsGlobal;

    FakeLatencySettings m_FakeLatencySettings;
    unsigned m_LatencyBurstStartTime;
    unsigned m_LatencyBurstEndTime;

    netRandom m_RandDrop;
    netRandom m_RandLag;

    static u8* sm_HpPile;
    static atPool<HoldPacket>* sm_HpPool;

	static unsigned sm_InboundBytesPerSecGlobal;
	static unsigned sm_OutboundBytesPerSecGlobal;

	unsigned m_InboundBytesPerSec;
	unsigned m_OutboundBytesPerSec;
	
    int m_InBoundAccum;
    int m_OutBoundAccum;
    unsigned m_LastBwUpdateTime;
#endif  //FAKE_DROP_LATENCY

    bool m_IsInitialized            : 1;
    bool m_CanBroadcast             : 1;
    bool m_ProcessingPktRecorders   : 1;
    bool m_IsCreated                : 1;
};

}   // namespace rage

#endif  //NET_NETSOCKET_H
