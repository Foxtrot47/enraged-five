//
// net/natpcp.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#include "natpcp.h"
#include "pcp_rockstar_custom.h"

#include "diag/output.h"
#include "diag/seh.h"
#include "netdiag.h"
#include "nethardware.h"
#include "netsocket.h"
#include "packet.h"
#include "status.h"
#include "system/nelem.h"
#include "system/threadpool.h"
#include "system/timer.h"
#include "rline/ros/rlros.h"

#if RSG_PC || RSG_DURANGO
#define STRICT
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#pragma warning(pop)
#endif

#include <time.h>

// NET_ENABLE_PCP is defined in pcp_rockstar_custom.h

#if NET_ENABLE_PCP
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4996)  // 'WSAAddressToStringA': Use WSAAddressToStringW() instead or define _WINSOCK_DEPRECATED_NO_WARNINGS to disable deprecated API warnings
#endif
// libpcp includes
#include "pcp.h"
#include "pcp_client_db.h"
#include "default_config.h"
#include "unp.h"
#include "pcp_gettimeofday.h"

#if RSG_DURANGO
#include "net/durango/xblgetgatewayaddress.h"
#endif

#ifdef _MSC_VER
#pragma warning(pop)
#endif
#endif

#define ENABLE_LIBPCP_OUTPUT (NET_ENABLE_PCP && !__NO_OUTPUT)

/*
For information on:
- Port Control Protocol (PCP), see RFC 6887 (https://tools.ietf.org/html/rfc6887)
- PCP's predecessor, Port Mapping Protocol (NAT-PMP), see RFC 6886 (https://tools.ietf.org/html/rfc6886)
- pcp-upnp-igd-interworking function, see RFC 6970 (https://tools.ietf.org/html/rfc6970)

Our PCP and NAT-PMP client implementation uses libpcp: https://github.com/libpcp/pcp

Useful routers for testing purposes:
Routers capable of running Tomato by Shibby 1.38 or later (http://tomato.groov.pl/)
	--> uses miniupnpd 2.0 or later, which supports PCP, NAT-PMP, UPnP 1.0 and 1.1.
	--> also allows user-configuration of the UDP session timeout.
	--> tested on a Linksys E2500 v3
Apple Airport Extreme supports PCP, earlier Apple routers support NAT-PMP.
Many routers support UPnP v1.0.
*/

namespace rage
{

// available in shipped exe
NOSTRIP_PARAM(netnopcp, "Don't use PCP or NAT-PMP protocols");

RAGE_DEFINE_SUBCHANNEL(ragenet, natpcp)
#undef __rage_channel
#define __rage_channel ragenet_natpcp

bool netNatPcp::sm_Initialized = false;
bool netNatPcp::sm_CanBegin = false;							// we wait for tunables before starting
bool netNatPcp::sm_AllowPcpAddressReservation = true;			// if false, we don't use PCP or NAT-PMP protocols
bool netNatPcp::sm_ReserveRelayMappedAddr = true;				// if true, we attempt to reserve/forward the relay mapped port. If false, we reserve the game's canonical socket port.
unsigned netNatPcp::sm_DeviceDiscoveryTimeoutMs = (10 * 1000);	// the amount of time to wait for a response to a device discovery query
unsigned netNatPcp::sm_PcpRefreshTimeMs = (20 * 60 * 1000);		// the amount of time in ms to wait before refreshing the PCP port reservation
unsigned netNatPcp::sm_MaxSequentialFailedRefreshes = 3;		// the max number of refresh failures before halting refreshes
unsigned netNatPcp::sm_PcpRefreshStartTime = 0;					// timestamp of the last successful port mapping command, used to time port mapping refreshes
unsigned netNatPcp::sm_PcpLeaseDurationSec = 3600;				// port map lease duration (in seconds). The spec recommends no more than 1 hour (3600 seconds).
netNatPcpInfo::State netNatPcp::m_DeleteState = netNatPcpInfo::NAT_PCP_UNATTEMPTED;
netNatPcpInfo netNatPcp::sm_PcpInfo;
sysCriticalSectionToken netNatPcp::m_Cs;

#if NET_ENABLE_PCP

extern sysThreadPool netThreadPool;

//////////////////////////////////////////////////////////////////////////
//  natPcpWorkItem
//////////////////////////////////////////////////////////////////////////

class natPcpWorkItem : public sysThreadPool::WorkItem
{
public:

	natPcpWorkItem()
	: m_Status(NULL)
	, m_Success(false)
	, m_RefreshMapping(false)
	, m_DeleteMapping(false)
	, m_StartTime(0)
	, m_Info(NULL)
	, m_Ctx(NULL)
	, m_Flow(NULL)
	{

	}

	bool Configure(bool deleteMapping, bool refreshMapping, netNatPcpInfo& info, netStatus* status)
	{
		m_DeleteMapping = deleteMapping;
		m_RefreshMapping = refreshMapping;
		m_Info = &info;
		m_Status = status;
		m_Success = false;
		m_StartTime = 0;

		if(m_Status)
		{
			if(m_Status->Pending())
			{
				m_Status->SetCanceled();
				m_Status->Reset();
			}

			m_Status->SetPending();
		}

		return true;
	}

	virtual void DoWork()
	{
		if(m_DeleteMapping)
		{
			DeletePortMapping();
		}
		else if(m_RefreshMapping)
		{
			RefreshPortMapping();
		}
		else
		{
			AddPortMapping();
		}
	}

	bool Succeeded() const
	{
		return m_Success;
	}

private:

	void AddPortMapping()
	{
		pcp_flow_info_t* flowInfo = NULL;

		rtry
		{
#if RSG_DURANGO
			// attempt to get the default gateway IP before doing any work.
			// This comes from a custom Microsoft-supplied DLL and is not part of the standard XDK.
			// If the DLL doesn't exist, gracefully fail. NAT-PMP / PCP will be disabled.
			HMODULE hDll = LoadLibraryW(L"GetGatewayAddress.dll");
			rcheck(hDll, taskfailed, netDebug("Failed to load GetGatewayAddress.dll - PCP / NAT-PMP will be disabled."));

			GETIPV4GATEWAYADDRESSPROC func = (GETIPV4GATEWAYADDRESSPROC)GetProcAddress(hDll, "GetIpv4GatewayAddress");

			// free the library even if GetProcAddress fails
			IN_ADDR addr = {0};
			HRESULT hr = E_FAIL;
			if(func)
			{
				hr = func(&addr);
			}

			FreeLibrary(hDll);

			rverify(func, taskfailed, netError("GetIpv4GatewayAddress not found"));
			rverify(SUCCEEDED(hr), taskfailed, netError("GetIpv4GatewayAddress returned hr=0x%08x", hr));

			netIpV4Address ipv4 = netIpV4Address(rage::net_ntohl(addr.s_addr));
			rverify(ipv4.IsValid(), taskfailed, netError("GetIpv4GatewayAddress returned an invalid default gateway IP"));
			netDebug("Default gateway IP: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(ipv4));

			sockaddr_in saddr = {0};
			saddr.sin_family = AF_INET;
			saddr.sin_addr.s_addr = addr.s_addr;
#endif

			m_StartTime = sysTimer::GetSystemMsTime();
			m_Info->SetReservedAddress(netSocketAddress());

			rcheck(!WasCanceled(), taskcancelled, netDebug("natPcpWorkItem was cancelled. Bailing."));

			netIpAddress localIp;
			rverify(netHardware::GetLocalIpAddress(&localIp), taskfailed, netError("Could not get local IP address"));
			rverify(localIp.IsValid(), taskfailed, netError("Local IP address is invalid"));

			// try to reserve the relay mapped port. This will make relayed connections more stable
			// (less chance of the NAT closing the port), and during NAT traversal, it's one of the
			// primary candidate ports we try.
			u16 requestedPort = 0;

 			if(netNatPcp::GetReserveRelayMappedAddress() && netRelay::GetMyRelayAddress().IsRelayServerAddr())
 			{
 				requestedPort = netRelay::GetMyRelayAddress().GetTargetAddress().GetPort();
 			}

			if(requestedPort == 0)
			{
				// if we're not reserving the relay mapped port, then reserve the canonical port.
				// During NAT traversal, we always try the game's canonical port as a candidate.
				requestedPort = netRelay::GetRequestedPort();
			}
			
			rcheck(requestedPort > 0, taskfailed, netError("invalid requestedPort"));
			
			u16 locallyBoundPort = 0;
			if(netRelay::GetSocket() && (netRelay::GetSocket()->GetPort() > 0))
			{
				locallyBoundPort = netRelay::GetSocket()->GetPort();
			}
			else
			{
				locallyBoundPort = requestedPort;
			}

			rcheck(locallyBoundPort > 0, taskfailed, netError("invalid locallyBoundPort"));

			m_Ctx = pcp_init(ENABLE_AUTODISCOVERY, NULL);
			rverify(m_Ctx, taskfailed, netError("pcp_init returned NULL"));

#if RSG_DURANGO
			pcp_add_server(m_Ctx, (sockaddr*)&saddr, PCP_MAX_SUPPORTED_VERSION);
#endif

			sockaddr_storage source_addr = {0};
			netSocketAddress sourceAddr(localIp, locallyBoundPort);
 			rverify(sourceAddr.ToSockAddr((sockaddr*)&source_addr, sizeof(source_addr)), taskfailed, netError("Failed to convert netSocketAddress to a sockaddr_storage"));

			sockaddr_storage requested_addr = {0};
			netSocketAddress requestedAddr(netIpAddress(), requestedPort); // default to reserving any IP address
			if(netRelay::GetMyRelayAddress().IsRelayServerAddr())
			{
				// if we have a valid relay address, attempt to reserve the same IP and port the relay observed
				requestedAddr.Init(netRelay::GetMyRelayAddress().GetTargetAddress().GetIp(), requestedPort);
			}
			rverify(requestedAddr.ToSockAddr((sockaddr*)&requested_addr, sizeof(requested_addr)), taskfailed, netError("Failed to convert netSocketAddress to a sockaddr_storage"));

			m_Info->SetRequestedAddress(requestedAddr);

			netDebug("PCP: Requesting map: " NET_ADDR_FMT " -> " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(requestedAddr), NET_ADDR_FOR_PRINTF(sourceAddr));			 

			m_Flow = pcp_new_flow(m_Ctx,
								  (struct sockaddr*)&source_addr,
								  NULL,	// destination address. Pass NULL for PCP MAP opcode (non-NULL would be a PEER opcode).
								  (struct sockaddr*)&requested_addr,
								  IPPROTO_UDP,
								  netNatPcp::GetPcpLeaseDurationSec(),
								  m_Ctx);

			rcheck(m_Flow, taskfailed, netError("pcp_new_flow returned NULL"));
			rverify(m_Flow->ctx, taskfailed, netError("m_Flow->ctx is NULL"));

			pcp_fstate_e pcpResult = pcp_wait(m_Flow, netNatPcp::GetDeviceDiscoveryTimeoutMs(), 0);

		    size_t cnt = 0;
			flowInfo = pcp_flow_get_info(m_Flow, &cnt);
			rverify(flowInfo, taskfailed, netError("pcp_flow_get_info returned NULL"));

			m_Info->SetPcpNewFlowResultCode(flowInfo->pcp_result_code);

			rcheck(pcpResult == pcp_state_succeeded, taskfailed, );
			rverify(flowInfo->result == pcp_state_succeeded, taskfailed, netError("flowInfo->result is not pcp_state_succeeded"));
			rverify(cnt > 0, taskfailed, netError("m_Flow count is 0"));

			pcp_server* server = get_pcp_server(m_Flow->ctx, m_Flow->pcp_server_indx);
			s16 version = -1;
			if(server)
			{
				version = (s16)server->pcp_version;
			}

			if(version >= 1)
			{
				m_Info->SetReserveMethod(netNatPcpInfo::NAT_PCP_METHOD_PCP);
			}
			else if(version == 0)
			{
				m_Info->SetReserveMethod(netNatPcpInfo::NAT_PCP_METHOD_PMP);
			}
			else
			{
				m_Info->SetReserveMethod(netNatPcpInfo::NAT_PCP_METHOD_NONE);
			}

#if IPv4_IPv6_DUAL_STACK
			netIpAddress reservedIp(netIpV6Address(&flowInfo->ext_ip));
#else
			// PCP always uses IPv6 addresses, but if IPv6 support is disabled, we only support IPv4-mapped IPv6 addresses
			rverify(IN6_IS_ADDR_V4MAPPED(&flowInfo->ext_ip), taskfailed, netError("Unexpected IPv6 address reserved"));

			// the last 4 bytes of an IPv4-mapped IPv6 address contains the IPv4 address
			u32 ipV4 = 0;
			sysMemCpy(&ipV4, &flowInfo->ext_ip.u.Byte[12], 4);
			netIpAddress reservedIp(netIpV4Address(net_ntohl(ipV4)));
#endif

			if(!reservedIp.IsValid())
			{
				reservedIp = netRelay::GetMyRelayAddress().GetTargetAddress().GetIp();
			}

			u16 reservedPort = net_ntohs(flowInfo->ext_port);

			netSocketAddress reservedAddr(reservedIp, reservedPort);

			rverify(reservedAddr.IsValid(), taskfailed, netError("Reserved address is invalid"));

			m_Info->SetReservedAddress(reservedAddr);
			
			rthrow(tasksucceeded, netDebug2("PCP: Succeeded. Reserved Address: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_Info->GetReservedAddress())));
		}
		rcatch(taskcancelled)
		{
			// we still clean up if we're canceled (below)
			if(m_Status)
			{
				m_Status->SetCanceled();
			}
		}
		rcatch(taskfailed)
		{
			if(m_Status)
			{
				m_Status->SetFailed();
			}
		}
		rcatch(tasksucceeded)
		{
			m_Success = true;

			if(m_Status)
			{
				m_Status->SetSucceeded();
			}
		}

		if(m_Status)
		{
			if(m_Status->Pending())
			{
				m_Status->SetFailed();
			}
		}

		if(flowInfo)
		{
			free(flowInfo);
			flowInfo = NULL;
		}

		if(!m_Success && m_Ctx)
		{
			pcp_terminate(m_Ctx, 1);
			m_Ctx = NULL;
		}

		m_Status = NULL;

		m_Info->SetPortReservationTimeMs(sysTimer::GetSystemMsTime() - m_StartTime);
	}

	void RefreshPortMapping()
	{
		rtry
		{
			rcheck(!WasCanceled(), taskcancelled, netDebug("natPcpWorkItem was cancelled. Bailing."));
			rverify(m_Ctx, taskfailed, netError("Attempting to refresh port mapping when m_Ctx is NULL"));
			rverify(m_Flow, taskfailed, netError("Attempting to refresh a NULL PCP flow"));

			pcp_flow_set_lifetime(m_Flow, netNatPcp::GetPcpLeaseDurationSec());
			pcp_fstate_e pcpResult = pcp_wait(m_Flow, netNatPcp::GetDeviceDiscoveryTimeoutMs(), 0);
			rcheck(pcpResult == pcp_state_succeeded, taskfailed, netDebug("PCP: Port mapping refresh failed."));
			
			rthrow(tasksucceeded, netDebug("PCP: Port mapping refreshed successfully."));
		}
		rcatch(taskcancelled)
		{
			// we still clean up if we're canceled (below)
			if(m_Status)
			{
				m_Status->SetCanceled();
			}
		}
		rcatch(taskfailed)
		{
			u16 numFailed = m_Info->GetNumFailedRefreshes();
			m_Info->SetNumFailedRefreshes(numFailed + 1);

			numFailed = m_Info->GetNumSequentialFailedRefreshes();
			m_Info->SetNumSequentialFailedRefreshes(numFailed + 1);

			if(m_Status)
			{
				m_Status->SetFailed();
			}
		}
		rcatch(tasksucceeded)
		{
			u16 numSuccessful = m_Info->GetNumSuccessfulRefreshes();
			m_Info->SetNumSuccessfulRefreshes(numSuccessful + 1);
			m_Info->SetNumSequentialFailedRefreshes(0);

			m_Success = true;

			if(m_Status)
			{
				m_Status->SetSucceeded();
			}
		}

		if(m_Status)
		{
			if(m_Status->Pending())
			{
				m_Status->SetFailed();
			}
		}

		m_Status = NULL;
	}

	void DeletePortMapping()
	{
		rtry
		{
			rcheck(!WasCanceled(), taskcancelled, netDebug("natPcpWorkItem was cancelled. Bailing."));
			rverify(m_Ctx, taskfailed, netError("Attempting to delete PCP flows on a NULL PCP context"));
			rverify(m_Flow, taskfailed, netError("Attempting to delete NULL PCP flow"));

			pcp_terminate(m_Ctx, 1);
			m_Ctx = NULL;
			m_Flow = NULL;

			m_Info->SetReservedAddress(netSocketAddress());

			rthrow(tasksucceeded, netDebug("PCP: Port mapping deleted successfully."));
		}
		rcatch(taskcancelled)
		{
			// we still clean up if we're canceled (below)
			if(m_Status)
			{
				m_Status->SetCanceled();
			}
		}
		rcatch(taskfailed)
		{
			if(m_Status)
			{
				m_Status->SetFailed();
			}
		}
		rcatch(tasksucceeded)
		{
			m_Success = true;

			if(m_Status)
			{
				m_Status->SetSucceeded();
			}
		}

		if(m_Status)
		{
			if(m_Status->Pending())
			{
				m_Status->SetFailed();
			}
		}

		m_Status = NULL;
	}

	netStatus* m_Status;
	bool m_Success;
	bool m_RefreshMapping;
	bool m_DeleteMapping;
	unsigned m_StartTime;
	netNatPcpInfo* m_Info;
	pcp_ctx_t* m_Ctx;
	pcp_flow_t* m_Flow;
};

//////////////////////////////////////////////////////////////////////////
//  netNatPcp
//////////////////////////////////////////////////////////////////////////

#if ENABLE_LIBPCP_OUTPUT
static void LibPcpCustomLoggingFunc(pcp_loglvl_e mode, const char *msg)
{
	char outMsg[4096];

	// skip over libpcp's filename, line number, function name preamble
	const char* toSkip = "\n     ";
	const char* beginMsg = strstr(msg, toSkip);
	if(beginMsg)
	{
		beginMsg += strlen(toSkip);
	}

	if((beginMsg == NULL) || (beginMsg[0] == '\0'))
	{
		return;
	}

	formatf(outMsg, "%s", beginMsg);

    switch(mode)
	{
        case PCP_LOGLVL_ERR:
        case PCP_LOGLVL_PERR:
			netError("%s", outMsg);
			break;
        case PCP_LOGLVL_WARN:
            netWarning("%s", outMsg);
            break;
        case PCP_LOGLVL_INFO:
            netDebug("%s", outMsg);
            break;
        case PCP_LOGLVL_DEBUG:
			// verbose
			//netDebug3("%s", outMsg);
            break;
		case PCP_LOGLVL_NONE:
			break;
    }
}
#endif // ENABLE_LIBPCP_OUTPUT

natPcpWorkItem s_natPcpWorkItem;
#endif // NET_ENABLE_PCP

bool
netNatPcp::Init()
{
	SYS_CS_SYNC(m_Cs);

#if ENABLE_LIBPCP_OUTPUT
	// override libpcp's default logging function
	pcp_set_loggerfn(LibPcpCustomLoggingFunc);
#endif

	sm_Initialized = true;
	return true;
}

void
netNatPcp::Shutdown()
{
	SYS_CS_SYNC(m_Cs);

	sm_Initialized = false;
}

bool 
netNatPcp::IsInitialized()
{
	return sm_Initialized;
}

bool 
netNatPcp::IsInitialWorkCompleted()
{
#if NET_ENABLE_PCP
	netNatPcpInfo::State pcpState = sm_PcpInfo.GetState();
	bool completed = (pcpState == netNatPcpInfo::NAT_PCP_SUCCEEDED) || (pcpState == netNatPcpInfo::NAT_PCP_FAILED) || sm_PcpInfo.GetReservedAddress().IsValid();
	bool blocked = PARAM_netnopcp.Get() || (sm_CanBegin && !sm_AllowPcpAddressReservation);
	return completed || blocked;
#else
	return true;
#endif
}

void
netNatPcp::Update()
{
#if NET_ENABLE_PCP
	bool natPcpAllowed = sm_Initialized && sm_CanBegin && sm_AllowPcpAddressReservation && !PARAM_netnopcp.Get();

	if(!natPcpAllowed)
	{
		return;
	}

	bool isAnyOnline = rlRos::IsAnyOnline();
	static bool deletePortMapping = false;
	deletePortMapping = deletePortMapping || ((!isAnyOnline || !netRelay::GetMyRelayAddress().IsRelayServerAddr()) && (sm_PcpInfo.GetReservedAddress().IsValid()));

	if(deletePortMapping)
	{
		sm_PcpInfo.SetState(netNatPcpInfo::NAT_PCP_UNATTEMPTED);
		sm_PcpRefreshStartTime = 0;

		switch(m_DeleteState)
		{
		case rage::netNatPcpInfo::NAT_PCP_UNATTEMPTED:
			if(s_natPcpWorkItem.Pending())
			{
				netThreadPool.CancelWork(s_natPcpWorkItem.GetId());
			}
			else
			{
				bool configured = s_natPcpWorkItem.Configure(true, false, sm_PcpInfo, NULL);
				if(configured && netThreadPool.QueueWork(&s_natPcpWorkItem))
				{
					m_DeleteState = netNatPcpInfo::NAT_PCP_IN_PROGRESS;
				}
				else
				{
					netError("Failed to queue PCP work item");
					deletePortMapping = false;
					m_DeleteState = netNatPcpInfo::NAT_PCP_UNATTEMPTED;
				}
			}
			break;
		case rage::netNatPcpInfo::NAT_PCP_IN_PROGRESS:
			if(s_natPcpWorkItem.Finished() || s_natPcpWorkItem.WasCanceled())
			{
				sm_PcpInfo.Clear();
				deletePortMapping = false;
				m_DeleteState = netNatPcpInfo::NAT_PCP_UNATTEMPTED;
			}
			break;
		default:
			break;
		}

		return;
	}

	switch(sm_PcpInfo.GetState())
	{
	case netNatPcpInfo::NAT_PCP_UNATTEMPTED:
		// wait until we have our mapped address from the relay
		if(netRelay::GetMyRelayAddress().IsRelayServerAddr() && !s_natPcpWorkItem.Pending())
		{
			bool refreshPortReservation = sm_PcpRefreshStartTime > 0;
			bool deletePortReservation = false;
			
			bool configured = s_natPcpWorkItem.Configure(deletePortReservation, refreshPortReservation, sm_PcpInfo, NULL);
			if(configured && netThreadPool.QueueWork(&s_natPcpWorkItem))
			{
				sm_PcpInfo.SetState(netNatPcpInfo::NAT_PCP_IN_PROGRESS);
			}
			else
			{
				netError("Failed to queue PCP work item");
				sm_PcpInfo.SetState(netNatPcpInfo::NAT_PCP_FAILED);
			}
		}
		break;
	case netNatPcpInfo::NAT_PCP_IN_PROGRESS:
		if(s_natPcpWorkItem.Finished() || s_natPcpWorkItem.WasCanceled())
		{
			// if we fail to refresh, then we set to succeeded and try again later
			if(s_natPcpWorkItem.Succeeded() || (sm_PcpRefreshStartTime > 0))
			{
				sm_PcpInfo.SetState(netNatPcpInfo::NAT_PCP_SUCCEEDED);
				sm_PcpRefreshStartTime = sysTimer::GetSystemMsTime() | 1;
			}
			else
			{
				sm_PcpInfo.SetState(netNatPcpInfo::NAT_PCP_FAILED);
			}
		}
		break;
	case netNatPcpInfo::NAT_PCP_SUCCEEDED:
		// We need to refresh the lease periodically.
		if((sm_PcpRefreshTimeMs > 0) && (sm_PcpRefreshStartTime > 0))
		{
			if(sysTimer::HasElapsedIntervalMs(sm_PcpRefreshStartTime, sm_PcpRefreshTimeMs))
			{
				if(sm_PcpInfo.GetNumSequentialFailedRefreshes() < sm_MaxSequentialFailedRefreshes)
				{
					sm_PcpInfo.SetState(netNatPcpInfo::NAT_PCP_UNATTEMPTED);
				}
				else
				{
					sm_PcpInfo.SetState(netNatPcpInfo::NAT_PCP_FAILED);
				}
			}
		}
		break;
	case netNatPcpInfo::NAT_PCP_FAILED:
	case netNatPcpInfo::NAT_PCP_NUM_STATES:
		break;
	}
#endif
}

void
netNatPcp::DeletePcpAddressReservation(netStatus* status)
{
	// note: this should only be called at shutdown

#if NET_ENABLE_PCP
	// set state to failed so the main update loop doesn't
	// start a refresh during/after we've deleted the mapping
	sm_PcpInfo.SetState(netNatPcpInfo::NAT_PCP_FAILED);

	u32 waitStartTime = sysTimer::GetSystemMsTime();
	while(s_natPcpWorkItem.Pending())
	{
		netThreadPool.CancelWork(s_natPcpWorkItem.GetId());

		if((sysTimer::GetSystemMsTime() - waitStartTime) > 10000)
		{
			if(status && !status->Pending())
			{
				status->ForceFailed();
			}
			return;
		}
	}

	if(sm_PcpInfo.GetReservedAddress().IsValid())
	{
		bool configured = s_natPcpWorkItem.Configure(true, false, sm_PcpInfo, status);
		if(configured)
		{
			netThreadPool.QueueWork(&s_natPcpWorkItem);
		}
	}
#endif

	// if we're not pending by this point, then we're done
	if(status && !status->Pending())
	{
		status->ForceSucceeded();
	}
}

void 
netNatPcp::SetAllowPcpAddressReservation(const bool allowPcpAddressReservation)
{
	if(sm_AllowPcpAddressReservation != allowPcpAddressReservation)
	{
		// once any system disables address resservation, don't allow it to be re-enabled by another system
		if(allowPcpAddressReservation == false)
		{
			netDebug("SetAllowPcpAddressReservation :: %s", allowPcpAddressReservation ? "true" : "false");
			sm_AllowPcpAddressReservation = allowPcpAddressReservation;
		}
	}
}

void
netNatPcp::SetReserveRelayMappedAddress(const bool reserveRelayMappedAddr)
{
	if(sm_ReserveRelayMappedAddr != reserveRelayMappedAddr)
	{
		netDebug("SetReserveRelayMappedAddress :: %s", reserveRelayMappedAddr ? "true" : "false");
		sm_ReserveRelayMappedAddr = reserveRelayMappedAddr;
	}
}

bool
netNatPcp::GetReserveRelayMappedAddress()
{
	return sm_ReserveRelayMappedAddr;
}

void 
netNatPcp::SetPcpRefreshTimeMs(const unsigned pcpRefreshTimeMs)
{
	if(sm_PcpRefreshTimeMs != pcpRefreshTimeMs)
	{
		netDebug("SetPcpRefreshTimeMs :: %u", pcpRefreshTimeMs);
		sm_PcpRefreshTimeMs = pcpRefreshTimeMs;
	}
}

void netNatPcp::SetTunablesReceived()
{
	sm_CanBegin = true;
}

unsigned
netNatPcp::GetPcpLeaseDurationSec()
{
	return sm_PcpLeaseDurationSec;
}

unsigned
netNatPcp::GetDeviceDiscoveryTimeoutMs()
{
	return sm_DeviceDiscoveryTimeoutMs;
}

#if !__NO_OUTPUT
void 
netNatPcpInfo::DumpInfo() const
{
	bool rfc1918 = m_ReservedAddr.GetIp().IsValid() && m_ReservedAddr.GetIp().IsV4() && m_ReservedAddr.GetIp().ToV4().IsRfc1918();
	bool altReservation = (m_State == NAT_PCP_SUCCEEDED) && (m_ReservedAddr != m_RequestedAddr);

	netDebug("          PCP: %s", (m_State == NAT_PCP_SUCCEEDED) ? "Enabled, Address Reserved" : ((m_State == NAT_PCP_FAILED) ? "Disabled/Failed" : "Not Performed"));
	netDebug("              Requested Address: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_RequestedAddr));
	netDebug("              Reserved Address: " NET_ADDR_FMT "%s%s", NET_ADDR_FOR_PRINTF(m_ReservedAddr),
								altReservation ? " (different than requested)" : "",
								rfc1918 ? " (rfc1918) - multiple levels of NAT detected" : "");
	netDebug("              Reserve Method: %s", GetReserveMethodString(m_ReserveMethod));
	netDebug("              Elapsed Time: %u ms", m_PortReservationTimeMs);
	netDebug("              PCP New Flow Result Code: %u", m_PcpNewFlowResultCode);
	netDebug("              Num Successful Refreshes: %u", m_NumSuccessfulRefreshes);
	netDebug("              Num Failed Refreshes: %u", m_NumFailedRefreshes);
}
#endif

bool
netNatPcpInfo::Import(const void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/)
{
	bool success = false;

    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

	success = bb.SerUns(m_State, datBitsNeeded<NAT_PCP_NUM_STATES>::COUNT)
				&& bb.SerUser(m_RequestedAddr)
				&& bb.SerUser(m_ReservedAddr)
				&& bb.SerUns(m_ReserveMethod, datBitsNeeded<NAT_PCP_NUM_METHODS>::COUNT)
				&& bb.SerUns(m_PortReservationTimeMs, sizeof(m_PortReservationTimeMs) << 3)
				&& bb.SerUns(m_PcpNewFlowResultCode, sizeof(m_PcpNewFlowResultCode) << 3)
				&& bb.SerUns(m_NumSuccessfulRefreshes, sizeof(m_NumSuccessfulRefreshes) << 3)
				&& bb.SerUns(m_NumFailedRefreshes, sizeof(m_NumFailedRefreshes) << 3)
				&& bb.SerUns(m_NumSequentialFailedRefreshes, sizeof(m_NumSequentialFailedRefreshes) << 3);

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}
    return success;
}

bool
netNatPcpInfo::Export( void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/ ) const
{
    bool success = false;

    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);
	
	success = bb.SerUns(m_State, datBitsNeeded<NAT_PCP_NUM_STATES>::COUNT)
				&& bb.SerUser(m_RequestedAddr)
				&& bb.SerUser(m_ReservedAddr)
				&& bb.SerUns(m_ReserveMethod, datBitsNeeded<NAT_PCP_NUM_METHODS>::COUNT)
				&& bb.SerUns(m_PortReservationTimeMs, sizeof(m_PortReservationTimeMs) << 3)
				&& bb.SerUns(m_PcpNewFlowResultCode, sizeof(m_PcpNewFlowResultCode) << 3)
				&& bb.SerUns(m_NumSuccessfulRefreshes, sizeof(m_NumSuccessfulRefreshes) << 3)
				&& bb.SerUns(m_NumFailedRefreshes, sizeof(m_NumFailedRefreshes) << 3)
				&& bb.SerUns(m_NumSequentialFailedRefreshes, sizeof(m_NumSequentialFailedRefreshes) << 3);

	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}
    return success;
}

} // namespace rage
