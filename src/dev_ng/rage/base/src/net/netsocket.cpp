// 
// net/netsocket.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "netsocket.h"

#include "diag/seh.h"
#include "netdiag.h"
#include "nethardware.h"
#include "math/random.h"
#include "profile/rocky.h"
#include "system/memory.h"
#include "system/new.h"
#include "system/timer.h"

#if RSG_NP
#include <sys/select.h>
#elif RSG_PC || RSG_DURANGO
#define STRICT
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#pragma warning(pop)
#endif //RSG_NP

#if RSG_LINUX
#include <limits.h>
#endif

//Convenience macro used to set "optional" (possibly null)
//socket error parameter.
#define NET_SET_SKTERR(se, e)   while((se)){*(se) = (e);break;}

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, socket)
#undef __rage_channel
#define __rage_channel ragenet_socket

bool netSocket::sm_RequireSelectOnSend = true;
unsigned netSocket::sm_NumSocketSendBufferOverflows = 0;

#if FAKE_DROP_LATENCY

PARAM(netfakedrop, "Enable dropping of packets and set the percentage (0-100) of inbound packets to drop. E.g. to drop 10% of packets: -netfakedrop=10");
PARAM(netfakelatency, "Enable fake latency and set how many milliseconds of latency to add to inbound packets. E.g. -netfakelatency=200. Specify a range to enable variable latency and packet reordering. E.g. -netfakelatency=100-400");
PARAM(netfakebandwidthlimitup, "Set initial value for the fake upload bandwidth limit in bytes per second. E.g. -netfakebandwidthlimitup=1024");
PARAM(netfakebandwidthlimitdown, "Set initial value for the fake download bandwidth limit in bytes per second. E.g. -netfakebandwidthlimitup=1024");

u8* netSocket::sm_HpPile = nullptr;
atPool<netSocket::HoldPacket>* netSocket::sm_HpPool = nullptr;

netSocket::FakeLatencySettings netSocket::sm_FakeLatencySettingsGlobal;
netSocket::FakeDropSettings    netSocket::sm_FakeDropSettingsGlobal;
bool                           netSocket::sm_FakeUplinkDisconnection = false;
unsigned                       netSocket::sm_InboundBytesPerSecGlobal = 0;
unsigned                       netSocket::sm_OutboundBytesPerSecGlobal = 0;

bool netSocket::IsFakeDropEnabled()
{
	return PARAM_netfakedrop.Get();
}

bool netSocket::IsFakeLatencyEnabled()
{
	return PARAM_netfakelatency.Get();
}

void netSocket::InitFakeDrop()
{
	if (IsFakeDropEnabled())
	{
		netDebug1("Fake Drop: Enabled with -%s", PARAM_netfakedrop.GetName());

		unsigned fakeDropPct;
		if (PARAM_netfakedrop.Get(fakeDropPct))
		{
			sm_FakeDropSettingsGlobal.m_BaselineDropPcnt = fakeDropPct;
			netDebug1("Fake Drop: Initialised baseline fake drop percent for all netSockets to %u with -%s",
				      fakeDropPct, PARAM_netfakedrop.GetName());
		}
	}
	else
	{
		netSocket::FakeDropSettings defaultFakeDropSettings;
		netSocket::SetGlobalFakeDropSettings(defaultFakeDropSettings);
	}
}

void netSocket::InitFakeLatency()
{
	if(IsFakeLatencyEnabled())
	{
		{
			sysMemAutoUseDebugMemory memory;

			static const unsigned HOLD_PACKET_BUFFER_SIZE = MAX_HOLD_PACKETS * sizeof(HoldPacket);
			sm_HpPile = rage_new u8[HOLD_PACKET_BUFFER_SIZE];
			sysMemSet(sm_HpPile, 0, HOLD_PACKET_BUFFER_SIZE);
			sm_HpPool = rage_new atPool<HoldPacket>(sm_HpPile, HOLD_PACKET_BUFFER_SIZE);
		}

		netDebug1("Fake Latency: Enabled with -%s", PARAM_netfakelatency.GetName());

		const char* fakeLatency = NULL;
		if(PARAM_netfakelatency.Get(fakeLatency) && fakeLatency)
		{
			unsigned fakeLatencyMinMs = 0;
			unsigned fakeLatencyMaxMs = 0;

			if(sscanf(fakeLatency, "%u-%u", &fakeLatencyMinMs, &fakeLatencyMaxMs) == 2)
			{
				netAssert(fakeLatencyMinMs <= fakeLatencyMaxMs);
			}
			else if(sscanf(fakeLatency, "%u", &fakeLatencyMinMs) == 1)
			{
				fakeLatencyMaxMs = fakeLatencyMinMs;
			}
			
			sm_FakeLatencySettingsGlobal.m_BaselineLatency = fakeLatencyMinMs;
			sm_FakeLatencySettingsGlobal.m_LatencyRange = fakeLatencyMaxMs - fakeLatencyMinMs;
			netDebug1("Fake Latency: Initialised baseline fake latency for all netSockets to %ums-%ums with -%s",
					fakeLatencyMinMs, fakeLatencyMaxMs, PARAM_netfakelatency.GetName());
		}
	}
	else
	{
		netSocket::FakeLatencySettings defaultFakeLatencySettings;
		netSocket::SetGlobalFakeLatencySettings(defaultFakeLatencySettings);
	}
}

void netSocket::InitFakeBandwidthLimit()
{
	netSocket::SetGlobalFakeInboundBandwidthLimit(0);
	netSocket::SetGlobalFakeOutboundBandwidthLimit(0);

	unsigned limit = 0;
	if (PARAM_netfakebandwidthlimitup.Get(limit) && limit != 0)
	{
		sm_OutboundBytesPerSecGlobal = limit;

		netDebug1("Fake Bandwidth Limit: Initialised upload limit for all netSockets to %u bytes per second with -%s",
			limit, PARAM_netfakebandwidthlimitup.GetName());
	}

	if (PARAM_netfakebandwidthlimitdown.Get(limit) && limit != 0)
	{
		sm_InboundBytesPerSecGlobal = limit;

		netDebug1("Fake Bandwidth Limit: Initialised download limit for all netSockets to %u bytes per second with -%s",
			limit, PARAM_netfakebandwidthlimitdown.GetName());
	}
}

void netSocket::ShutdownFakeLatency()
{
	sysMemAutoUseDebugMemory memory;
	
	if(sm_HpPool)
	{
		delete sm_HpPool;
		sm_HpPool = nullptr;
	}
	
	if(sm_HpPile)
	{
		delete[] sm_HpPile;
		sm_HpPile = nullptr;
	}
}

#endif //FAKE_DROP_LATENCY

unsigned
netSocket::NextId() const
{
	static unsigned s_NextId = 0;

	unsigned id = sysInterlockedIncrement(&s_NextId);

	while(id == INVALID_SOCKET_ID)
	{
		id = sysInterlockedIncrement(&s_NextId);
	}

	return id;
}

netSocket::netSocket()
    : m_IsInitialized(false)
    , m_IsCreated(false)
	, m_Id(INVALID_SOCKET_ID)

{
    this->SetInitialValues();
}

netSocket::~netSocket()
{
    netAssertf(!m_IsCreated , "Forgot to call netHardware::DestroySocket()");
    //this->Shutdown();
}

bool
netSocket::IsCreated() const
{
    return m_IsCreated;
}

void
netSocket::Destroy()
{
    if(m_IsCreated)
    {
        netHardware::DestroySocket(this);
    }
}

bool
netSocket::Send(const netSocketAddress& destination,
                const void *data,
                const unsigned length,
                netSocketError* sktErr)
{
	PROFILE

    bool success = false;

	const unsigned SKT_SEND_TIME_WARN_THREADSHOLD_MS = 2 * 1000;
	const unsigned curTime = sysTimer::GetSystemMsTime();

    if(length > MAX_BUFFER_SIZE)
    {
        NET_SET_SKTERR(sktErr, NET_SOCKERR_MESSAGE_TOO_LONG);
    }
    else if(m_Socket < 0)
    {
        NET_SET_SKTERR(sktErr, NET_SOCKERR_INVALID_SOCKET);
    }
	else if(!IsReadyToSend())
	{
		netWarning("[%u] [" NET_ADDR_FMT "] Cannot send due to netSocket::Select(). May be exceeding outgoing bandwidth.", m_Id, NET_ADDR_FOR_PRINTF(destination));

		sysInterlockedIncrement(&sm_NumSocketSendBufferOverflows);

		// for now, don't report an error. In the future, we may want to return an error and have the caller throttle.
		NET_SET_SKTERR(sktErr, NET_SOCKERR_NONE);
		success = true;
	}
    else
    {
		netDebug3("[%u] [" NET_ADDR_FMT "](%d)->", m_Id, NET_ADDR_FOR_PRINTF(destination), length);

#if FAKE_DROP_LATENCY
        unsigned len = length;

        {
            SYS_CS_SYNC(m_Cs);

            //If we exceed the outbound bandwidth limit then drop the packet.
            if(m_OutboundBytesPerSec)
            {
                const int pktLen = this->SizeofPacket(len);
                if(m_OutBoundAccum + (pktLen * 1000) >= (int) m_OutboundBytesPerSec * 1000)
                {
                    len = 0;
                }
                else
                {
                    m_OutBoundAccum += (pktLen * 1000);
                }
            }

            if (sm_FakeUplinkDisconnection)
            {
                len = 0;
            }
        }

        if(0 == len)
        {
            success = true;
        }
        else
#endif //FAKE_DROP_LATENCY
        if(this->NativeSend(destination, data, length, sktErr))
        {
            NET_SET_SKTERR(sktErr, NET_SOCKERR_NONE);
            success = true;
            this->RecordOutboundPacket(destination, data, length);
        }
    }
		
	const unsigned elapsed = sysTimer::GetSystemMsTime() - curTime;
	if(elapsed >= SKT_SEND_TIME_WARN_THREADSHOLD_MS)
	{
		netWarning("[%u] Send took %u ms", m_Id, elapsed);
	}

    return success;
}

bool
netSocket::Broadcast(const unsigned short port,
                    const void *data,
                    const unsigned length,
                    netSocketError* sktErr)
{
    const netSocketAddress addr(netIpAddress(netIpV4Address::GetBroadcastAddress()), port);
    return netVerify(this->CanBroadcast())
            && this->Send(addr, data, length, sktErr);
}

bool 
netSocket::GetReceiveBufferInfo(unsigned& receiveBufferQueueLength, unsigned& receiveBufferSize) const
{
	unsigned sendBufferSize = 0;
	if(NativeGetSendReceiveBufferSizes(sendBufferSize, receiveBufferSize))
	{
		return NativeGetReceiveBufferQueueLength(receiveBufferQueueLength);
	}

	return false;
}

int
netSocket::Receive(netSocketAddress* sender,
                    void *buffer,
                    const int bufferSize,
                    netSocketError* sktErr)
{
	PROFILE

	const unsigned SKT_RECEIVE_TIME_WARN_THREADSHOLD_MS = 2 * 1000;
	const unsigned curTime = sysTimer::GetSystemMsTime();

    int len = 0;

    if(m_Socket < 0)
    {
        len = -1;
        NET_SET_SKTERR(sktErr, NET_SOCKERR_INVALID_SOCKET);
    }
    else
    {
        NET_SET_SKTERR(sktErr, NET_SOCKERR_NONE);

#if FAKE_DROP_LATENCY
		{
			SYS_CS_SYNC(m_Cs);
			
			if(!m_HoldQueue.empty())
			{
				HoldPacket* hp = *m_HoldQueue.begin();

				const unsigned curTime = sysTimer::GetSystemMsTime();

				if(int(hp->m_ReceiveTime - curTime) <= 0)
				{
					// The front of the hold queue is ready. Try to receive it.

					netSocketError recvErr;

					const int size =
						hp->m_Size > bufferSize ? bufferSize : hp->m_Size;

					if(size > 0)
					{
						sysMemCpy(buffer, hp->m_Data, size);
					}

					*sender = hp->m_Sender;

					if(hp->m_Size > bufferSize)
					{
						recvErr = NET_SOCKERR_MESSAGE_TOO_LONG;
						len = -1;
					}
					else
					{
						recvErr = hp->m_Error;
						len = (NET_SOCKERR_NONE == hp->m_Error) ? hp->m_Size : -1;
					}

					m_HoldQueue.erase(hp);

					netDebug3("[%u] [" NET_ADDR_FMT "](%d)<-(Fake Latency) held for: %d ms",
						m_Id,
						NET_ADDR_FOR_PRINTF(*sender),
						len,
						(int)(curTime - hp->m_ActualReceiveTime));

					hp->~HoldPacket();
					sm_HpPool->Delete(hp);

					NET_SET_SKTERR(sktErr, recvErr);
				}
			}
		}

		// Only poll the socket if we received nothing from the hold queue.
		if (len == 0)
#endif
		{
			netSocketError recvErr;
			len = this->NativeReceive(sender, buffer, bufferSize, &recvErr);

#if !__NO_OUTPUT
			if(len > 0)
			{
				unsigned rcvQueueLength = 0;
				netDebug3("[%u] [" NET_ADDR_FMT "](%d)<-,ql:%u", m_Id, NET_ADDR_FOR_PRINTF(*sender), len, NativeGetReceiveBufferQueueLength(rcvQueueLength) ? rcvQueueLength : 0);
			}
#endif

#if FAKE_DROP_LATENCY
			const unsigned latency = this->ComputeFakeLatency();
			const bool dropPacket = this->ComputeFakeDrop();

			if(dropPacket)
			{
				if(len > 0)
				{
					netDebug2("[%u] Fake Drop: Dropped a packet of size %d from [" NET_ADDR_FMT "]",
							  m_Id,
							  len,
							  NET_ADDR_FOR_PRINTF(*sender));

					recvErr = NET_SOCKERR_NONE;
					len = 0;
				}
			}
			else if(latency > 0)
			{
				SYS_CS_SYNC(m_Cs);

				const unsigned curTime = sysTimer::GetSystemMsTime();

				if(sm_HpPool->IsFull())
				{
					netWarning("Fake Latency: Hold packet pool is full - can't simulate latency");
				}
				else if(0 != len)
				{
					if(len < 0 && NET_SOCKERR_MESSAGE_TOO_LONG == recvErr)
					{
						recvErr = NET_SOCKERR_NONE;
						len = bufferSize;
					}

					HoldPacket* hp = sm_HpPool->New();

					netAssertf(hp, "Fake Latency: Hold packet pool is exhausted");

					new (hp) HoldPacket;

					hp->m_ActualReceiveTime = curTime;
					hp->m_ReceiveTime = curTime + latency;
					hp->m_Size = len;
					hp->m_Sender = *sender;
					hp->m_Error = recvErr;

					if(len > 0)
					{
						sysMemCpy(hp->m_Data, buffer, len);
					}
					
					// variable latency can reorder packets
					bool inserted = false;
					HoldQueue::iterator it = m_HoldQueue.begin();
					HoldQueue::const_iterator stop = m_HoldQueue.end();
					for(; stop != it; ++it)
					{
						const HoldPacket* h = *it;
						if(hp->m_ReceiveTime < h->m_ReceiveTime)
						{
							m_HoldQueue.insert(it, hp);
							inserted = true;
							break;
						}
					}

					if(!inserted)
					{
						m_HoldQueue.push_back(hp);
					}

#if __ASSERT
					unsigned lastRecvTime = 0;
					it = m_HoldQueue.begin();
					stop = m_HoldQueue.end();
					for(; stop != it; ++it)
					{
						const HoldPacket* h = *it;
						netAssert(h->m_ReceiveTime >= lastRecvTime);
						lastRecvTime = h->m_ReceiveTime;
					}
#endif

					recvErr = NET_SOCKERR_NONE;
					len = 0;
				}

				NET_SET_SKTERR(sktErr, recvErr);
			}
#endif  //FAKE_DROP_LATENCY
		}
    }

#if FAKE_DROP_LATENCY
    //If we exceed the inbound bandwidth limit then drop the packet.
    if(len > 0 && m_InboundBytesPerSec)
    {
        const int pktLen = this->SizeofPacket(len);
        if(m_InBoundAccum + (pktLen * 1000) >= (int) m_InboundBytesPerSec * 1000)
        {
            len = 0;
        }
        else
        {
            m_InBoundAccum += (pktLen * 1000);
        }
    }
#endif  //FAKE_DROP_LATENCY

    if(len > 0)
    {
        this->RecordInboundPacket(*sender, buffer, len);
	}

	const unsigned elapsed = sysTimer::GetSystemMsTime() - curTime;
	if(elapsed >= SKT_RECEIVE_TIME_WARN_THREADSHOLD_MS)
	{
		netWarning("[%u] Receive took %u ms", m_Id, elapsed);
	}

	return len;
}

bool
netSocket::CanSendReceive() const
{
    return m_Socket >= 0;
}

void
netSocket::SetRequireSelectOnSend(const bool requireSelectOnSend)
{
	if(sm_RequireSelectOnSend != requireSelectOnSend)
	{
		netDebug("SetRequireSelectOnSend :: %s", requireSelectOnSend ? "true" : "false");
		sm_RequireSelectOnSend = requireSelectOnSend;
	}
}

unsigned 
netSocket::GetNumSocketSendBufferOverflows()
{
	return sm_NumSocketSendBufferOverflows;
}

unsigned 
netSocket::ResetNumSocketSendBufferOverflows()
{
	return sysInterlockedExchange(&sm_NumSocketSendBufferOverflows, 0);
}

bool 
netSocket::IsReadyToSend() const
{
	rtry
	{
		// if this is disabled (e.g. via tunable) then default to original behaviour
		// where a socket select() is not required before sending on the socket.
		if(!sm_RequireSelectOnSend)
		{
			return true;
		}

		rcheckall(CanSendReceive());

		fd_set fdsw;
		FD_ZERO(&fdsw);

		const netSocketFd sktFd = (netSocketFd)m_Socket;
		rcheckall(sktFd >= 0);

		FD_SET(sktFd, &fdsw);

		// do not block
		timeval selectTimeout;
		selectTimeout.tv_sec = 0;
		selectTimeout.tv_usec = 0;

		const int numSkts = netSocket::Select(int(sktFd + 1), nullptr, &fdsw, nullptr, &selectTimeout);
		rcheckall((numSkts > 0) && FD_ISSET(sktFd, &fdsw));

		return true;
	}
	rcatchall
	{

	}

	return false;
}

bool
netSocket::CanBroadcast() const
{
    return m_CanBroadcast && this->CanSendReceive();
}

ptrdiff_t
netSocket::GetRawSocket() const
{
    return m_Socket;
}

netProtocol
netSocket::GetProtocol() const
{
    return m_Proto;
}

unsigned short
netSocket::GetPort() const
{
    return m_Port;
}

bool
netSocket::IsBlocking() const
{
    return (NET_SOCKET_BLOCKING == m_BlockingType);
}

const netSocketAddress&
netSocket::GetAddress() const
{
    return m_Addr;
}

void
netSocket::RegisterPacketRecorder(netPacketRecorder* recorder)
{
    SYS_CS_SYNC(m_CsPktRec);

    if(netVerify(!recorder->m_Owner))
    {
        //If this fires that means a recorder's RecordOutboundPacket()
        //and/or RecordInboundPacket() function resulted in an attempt to
        //register a recorder.
        netAssert(!m_ProcessingPktRecorders);

        recorder->m_Owner = this;
        m_PktRecorders.push_back(recorder);
    }
}

void
netSocket::UnregisterPacketRecorder(netPacketRecorder* recorder)
{
    SYS_CS_SYNC(m_CsPktRec);

    if(netVerify(this == recorder->m_Owner))
    {
        //If this fires that means a recorder's RecordOutboundPacket()
        //and/or RecordInboundPacket() function resulted in an attempt to
        //unregister a recorder.
        netAssert(!m_ProcessingPktRecorders);

        recorder->m_Owner = 0;
        m_PktRecorders.erase(recorder);
    }
}

#if FAKE_DROP_LATENCY

void
netSocket::SetFakeDropSettings(const FakeDropSettings& fakeDropSettings)
{
    SYS_CS_SYNC(m_Cs);

    const bool wasFakingDrop = this->CanFakeDrop();

    netAssert(fakeDropSettings.m_BaselineDropPcnt + fakeDropSettings.m_MaxExtraBurstDropPcnt <= 100);
    netAssert(fakeDropSettings.m_MinTimeBetweenBursts <= fakeDropSettings.m_MaxTimeBetweenBursts);
    netAssert(fakeDropSettings.m_MinBurstDuration <= fakeDropSettings.m_MaxBurstDuration);

	if (fakeDropSettings.m_BaselineDropPcnt != m_FakeDropSettings.m_BaselineDropPcnt)
	{
		netDebug2("[%u] [" NET_ADDR_FMT "] Fake Drop: Baseline drop %% set to %u",
			      m_Id,
			      NET_ADDR_FOR_PRINTF(m_Addr),
				  fakeDropSettings.m_BaselineDropPcnt);
	}

    m_FakeDropSettings = fakeDropSettings;

	if(fakeDropSettings.m_RandomSeed == 0)
	{
		m_RandDrop.SetRandomSeed();
	}
	else
	{
		m_RandDrop.Reset(fakeDropSettings.m_RandomSeed);
	}

    if(this->CanFakeDrop() && !wasFakingDrop)
    {
        //Start a drop burst now.
        m_DropBurstStartTime = sysTimer::GetSystemMsTime();

		netDebug2("[%u] [" NET_ADDR_FMT "] Fake Drop: Enabled for this netSocket",
				  m_Id, 
				  NET_ADDR_FOR_PRINTF(m_Addr));
    }
	else if (wasFakingDrop && !this->CanFakeDrop())
	{
		netDebug2("[%u] [" NET_ADDR_FMT "] Fake Drop: Disabled for this netSocket",
			m_Id,
			NET_ADDR_FOR_PRINTF(m_Addr));
	}
}

void
netSocket::GetFakeDropSettings(FakeDropSettings& fakeDropSettingsOut) const
{
	SYS_CS_SYNC(m_Cs);

    fakeDropSettingsOut = m_FakeDropSettings;
}

bool
netSocket::CanFakeDrop() const
{
    const unsigned totalDropPcnt =
        m_FakeDropSettings.m_BaselineDropPcnt + m_FakeDropSettings.m_MaxExtraBurstDropPcnt;

	return totalDropPcnt > 0 && totalDropPcnt <= 100 &&
           m_FakeDropSettings.m_MinBurstDuration <= m_FakeDropSettings.m_MaxBurstDuration &&
           m_FakeDropSettings.m_MinTimeBetweenBursts <= m_FakeDropSettings.m_MaxTimeBetweenBursts;
}

bool
netSocket::ComputeFakeDrop()
{
    bool droppit = false;

    if(this->CanFakeDrop())
    {
        if(m_FakeDropSettings.m_MaxBurstDuration > 0)
        {
            const unsigned curTime = sysTimer::GetSystemMsTime();

            if(int(m_DropBurstStartTime - curTime) <= 0)
            {
                //Start a new burst

                const unsigned duration =
                    (unsigned) m_RandDrop.GetRanged(int(m_FakeDropSettings.m_MinBurstDuration),
                                                       int(m_FakeDropSettings.m_MaxBurstDuration));

                //When the current burst will end
                m_DropBurstEndTime = curTime + duration;

                //When the next burst will begin
                m_DropBurstStartTime = m_DropBurstEndTime +
                    m_RandDrop.GetRanged(int(m_FakeDropSettings.m_MinTimeBetweenBursts),
                                          int(m_FakeDropSettings.m_MaxTimeBetweenBursts));

                m_CurrentDropPcnt =
                    m_FakeDropSettings.m_BaselineDropPcnt + m_FakeDropSettings.m_MaxExtraBurstDropPcnt;

				netDebug2("[%u] [" NET_ADDR_FMT "] Fake Drop: Starting drop burst - duration:%d, drop:%d%%",
							m_Id,
                            NET_ADDR_FOR_PRINTF(m_Addr),
                            duration,
                            m_CurrentDropPcnt);
            }
            else if(int(m_DropBurstEndTime - curTime) <= 0)
            {
                //End the current burst
                m_CurrentDropPcnt = m_FakeDropSettings.m_BaselineDropPcnt;
            }
        }
        else
        {
            m_CurrentDropPcnt = m_FakeDropSettings.m_BaselineDropPcnt;
        }

        if(m_CurrentDropPcnt)
        {
            const int r = m_RandDrop.GetInt() % 100;

            if(r < (int) m_CurrentDropPcnt)
            {
                droppit = true;
            }
        }
    }

    return droppit || sm_FakeUplinkDisconnection;
}

void
netSocket::GetFakeLatencySettings(FakeLatencySettings& input) const
{
	SYS_CS_SYNC(m_Cs);

	input = m_FakeLatencySettings;
}

void
netSocket::SetFakeLatencySettings(const FakeLatencySettings& fakeLatency)
{
    SYS_CS_SYNC(m_Cs);

    const bool wasFakingLatency = this->CanFakeLatency();

    netAssert(fakeLatency.m_MinTimeBetweenBursts <= fakeLatency.m_MaxTimeBetweenBursts);
    netAssert(fakeLatency.m_MinBurstDuration <= fakeLatency.m_MaxBurstDuration);

	if ((fakeLatency.m_BaselineLatency != m_FakeLatencySettings.m_BaselineLatency) ||
		(fakeLatency.m_LatencyRange != m_FakeLatencySettings.m_LatencyRange))
	{
		netDebug2("[%u] [" NET_ADDR_FMT "] Fake Latency: Baseline set to %ums-%ums",
			      m_Id,
			      NET_ADDR_FOR_PRINTF(m_Addr),
			      fakeLatency.m_BaselineLatency,
				  fakeLatency.m_BaselineLatency + fakeLatency.m_LatencyRange);
	}

    m_FakeLatencySettings = fakeLatency;
	
	if(fakeLatency.m_RandomSeed == 0)
	{
		m_RandLag.SetRandomSeed();
	}
	else
	{
		m_RandLag.Reset(fakeLatency.m_RandomSeed);
	}

    if(this->CanFakeLatency() && !wasFakingLatency)
    {
        //Start a latency burst now.
        m_LatencyBurstStartTime = sysTimer::GetSystemMsTime();

		netDebug2("[%u] [" NET_ADDR_FMT "] Fake Latency: Enabled for this netSocket",
			      m_Id,
			      NET_ADDR_FOR_PRINTF(m_Addr));
    }
	else if (!this->CanFakeLatency() && wasFakingLatency)
	{
		netDebug2("[%u] [" NET_ADDR_FMT "] Fake Latency: Disabled for this netSocket",
			      m_Id,
			      NET_ADDR_FOR_PRINTF(m_Addr));
	}
}

void
netSocket::GetGlobalFakeDropSettings(FakeDropSettings& fakeDropSettingsOut)
{
	fakeDropSettingsOut = sm_FakeDropSettingsGlobal;
}

void
netSocket::SetGlobalFakeDropSettings(const FakeDropSettings& fakeDropSettings)
{
	sm_FakeDropSettingsGlobal = fakeDropSettings;
}

void
netSocket::GetGlobalFakeLatencySettings(FakeLatencySettings& fakeLatencySettingsOut)
{
	fakeLatencySettingsOut = sm_FakeLatencySettingsGlobal;
}

void
netSocket::SetGlobalFakeLatencySettings(const FakeLatencySettings& fakeLatencySettings) 
{
	sm_FakeLatencySettingsGlobal = fakeLatencySettings;
}

void
netSocket::SetFakeUplinkDisconnection(const bool uplinkDisconencted)
{
    sm_FakeUplinkDisconnection = uplinkDisconencted;
}

bool
netSocket::GetFakeUplinkDisconnection()
{
    return sm_FakeUplinkDisconnection;
}

bool
netSocket::CanFakeLatency() const
{
    SYS_CS_SYNC(m_Cs);

    return sm_HpPool &&
		   ((m_FakeLatencySettings.m_BaselineLatency + m_FakeLatencySettings.m_LatencyRange + m_FakeLatencySettings.m_MaxExtraBurstLatency) > 0) &&
           (m_FakeLatencySettings.m_MinBurstDuration <= m_FakeLatencySettings.m_MaxBurstDuration) &&
           (m_FakeLatencySettings.m_MinTimeBetweenBursts <= m_FakeLatencySettings.m_MaxTimeBetweenBursts);
}

unsigned
netSocket::ComputeFakeLatency()
{
    SYS_CS_SYNC(m_Cs);

    unsigned latency = 0;

    if(this->CanFakeLatency())
    {
        if(m_FakeLatencySettings.m_MaxBurstDuration > 0)
        {
            const unsigned curTime = sysTimer::GetSystemMsTime();

            if(int(m_LatencyBurstStartTime - curTime) <= 0)
            {
                //Start a new burst

                const unsigned duration =
                    (unsigned) m_RandLag.GetRanged(int(m_FakeLatencySettings.m_MinBurstDuration),
                                                      int(m_FakeLatencySettings.m_MaxBurstDuration));

                //When the current burst will end
                m_LatencyBurstEndTime = curTime + duration;

                //When the next burst will begin
                m_LatencyBurstStartTime = m_LatencyBurstEndTime +
                    m_RandLag.GetRanged(int(m_FakeLatencySettings.m_MinTimeBetweenBursts),
                                         int(m_FakeLatencySettings.m_MaxTimeBetweenBursts));
#if !__NO_OUTPUT
                const unsigned range =
                    m_FakeLatencySettings.m_BaselineLatency + m_FakeLatencySettings.m_MaxExtraBurstLatency;
#endif // __NO_OUTPUT

				netDebug2("[%u] [" NET_ADDR_FMT "] Fake Latency: starting latency burst - duration:%d, range:%d",
						  m_Id,
						  NET_ADDR_FOR_PRINTF(m_Addr),
						  duration,
						  range);
            }

            //Are we in a burst?
            if(int(m_LatencyBurstEndTime - curTime) > 0)
            {
                if(m_FakeLatencySettings.m_MaxExtraBurstLatency > 0)
                {
                    latency = unsigned(m_RandLag.GetInt() % m_FakeLatencySettings.m_MaxExtraBurstLatency);
                }
            }
        }

		unsigned min = m_FakeLatencySettings.m_BaselineLatency;
		unsigned max = min + m_FakeLatencySettings.m_LatencyRange;
		latency += m_RandLag.GetRanged(min, max);
    }

    return latency;
}

void
netSocket::SetFakeInboundBandwidthLimit(const unsigned bytesPerSec)
{
    if(netVerify(bytesPerSec <= 1024*1024))
    {
        m_InboundBytesPerSec = bytesPerSec;
        m_InBoundAccum = 0;
    }
}

unsigned
netSocket::GetFakeInboundBandwidthLimit() const
{
    return m_InboundBytesPerSec;
}

void
netSocket::SetFakeOutboundBandwidthLimit(const unsigned bytesPerSec)
{
    if(netVerify(bytesPerSec <= 1024*1024))
    {
        m_OutboundBytesPerSec = bytesPerSec;
        m_OutBoundAccum = 0;
    }
}

unsigned
netSocket::GetFakeOutboundBandwidthLimit() const
{
    return m_OutboundBytesPerSec;
}

int netSocket::GetNextHeldPacketWaitTime()
{
	SYS_CS_SYNC(m_Cs);

	if (m_HoldQueue.empty())
	{
		return -1;
	}

	HoldPacket* hp = *m_HoldQueue.begin();

	netAssert(hp);
	
	const unsigned curTime = sysTimer::GetSystemMsTime();
	
	// Otherwise, we return time until Receive() should be called to not delay the packet more than intended
	// These are unsigned values so they will wrap appropriately, but cast to an int AFTER the subtraction
	return rage::Max(static_cast<int>(hp->m_ReceiveTime - curTime), 0);
}

#endif  //FAKE_DROP_LATENCY

int
netSocket::Select(const int nfds,
                    fd_set* readfds,
                    fd_set* writefds,
                    fd_set* exceptfds,
                    struct timeval* timeout)
{
	// nfds is one more than the highest socket fd being selected in any of the fd sets.
	// A socket fd of 0 is legal, so the  highest+1 must be > 0.
	netAssert(nfds > 0);

#if FAKE_DROP_LATENCY
	fd_set origReadfds;
	timeval fakeTimeout = {0};

	if(readfds && IsFakeLatencyEnabled())
	{
		// reduce the timeout to the minimum simulated receive time of all sockets being selected

		origReadfds = *readfds;

		int maxWaitTime = timeout ? ((int)timeout->tv_sec * 1000) + ((int)timeout->tv_usec / 1000) : INT_MAX;

		if(timeout)
		{
			fakeTimeout = *timeout;
		}
		else
		{
			fakeTimeout.tv_sec = (time_t)INT_MAX;
		}

		timeout = &fakeTimeout;

		for(netSocket* skt : netHardware::GetActiveSockets())
		{
			const netSocketFd sktFd = (netSocketFd)skt->GetRawSocket();
			
			if((sktFd >= 0) && FD_ISSET(sktFd, readfds))
			{
				const int nextHeldPacketMs = skt->GetNextHeldPacketWaitTime();

				if((nextHeldPacketMs >= 0) && (nextHeldPacketMs < maxWaitTime))
				{
					timeout->tv_sec = nextHeldPacketMs / 1000;
					timeout->tv_usec = (nextHeldPacketMs % 1000) * 1000;
					maxWaitTime = nextHeldPacketMs;
				}
			}
		}
	}
#endif

#if RSG_ORBIS
    int result = select(nfds, readfds, writefds, exceptfds, timeout);

	// other versions of select return SOCKET_ERROR (-1) in case of error,
	// socketselect() returns any negative number. Fix the result to behave
	// consistently across platforms
	if(result < 0)
	{
		result = -1;
	}
#else
    int result = ::select(nfds, readfds, writefds, exceptfds, timeout);
#endif

#if FAKE_DROP_LATENCY
	if(readfds && IsFakeLatencyEnabled())
	{
		// re-add selected sockets that have packets waiting in their simulated latency queue

		for(netSocket* skt : netHardware::GetActiveSockets())
		{
			const netSocketFd sktFd = (netSocketFd)skt->GetRawSocket();
			if((sktFd >= 0) && FD_ISSET(sktFd, &origReadfds) && skt->IsHeldPacketReady())
			{
				if(!FD_ISSET(sktFd, readfds))
				{
					FD_SET(sktFd, readfds);

					if(result < 0)
					{
						result = 0;
					}

					result++;
				}
			}
		}
	}
#endif

    return result;
}

void
netSocket::GetPortRangeForRandomSelection(unsigned short& lowerBound, unsigned short& upperBound)
{
	NativeGetPortRangeForRandomSelection(lowerBound, upperBound);
}

void
netSocket::GetPortRangeForP2pConnections(unsigned short& lowerBound, unsigned short& upperBound)
{
	NativeGetPortRangeForP2pConnections(lowerBound, upperBound);
}

//private:

void
netSocket::SetInitialValues()
{
    netAssert(!m_IsInitialized);

    //***DON'T set m_IsCreated here***.

    m_Proto = NET_PROTO_INVALID;
    m_BlockingType = NET_SOCKET_BLOCKING_INVALID;
    m_AddressFamily = NET_DEFAULT_ADDRESS_FAMILY;
    m_Socket = -1;
    m_IsInitialized = false;
    m_CanBroadcast = false;
    m_ProcessingPktRecorders = false;
	m_DesiredSendBufferSize = 0;
	m_DesiredReceiveBufferSize = 0;

    m_Addr.Clear();
    m_Port = 0;

#if FAKE_DROP_LATENCY
	{
		SYS_CS_SYNC(m_Cs);

		while(!m_HoldQueue.empty())
		{
			HoldPacket* hp = m_HoldQueue.front();
			m_HoldQueue.pop_front();

			hp->~HoldPacket();

			sm_HpPool->Delete(hp);
		}
	}

	m_FakeDropSettings = sm_FakeDropSettingsGlobal;

	m_FakeLatencySettings = sm_FakeLatencySettingsGlobal;

	m_InboundBytesPerSec  = sm_InboundBytesPerSecGlobal;
	m_OutboundBytesPerSec = sm_OutboundBytesPerSecGlobal;

	m_InBoundAccum = m_OutBoundAccum = 0;
	m_LastBwUpdateTime = 0;
#endif  //FAKE_DROP_LATENCY
}

bool
netSocket::Init(const unsigned short desiredPort,
                const netProtocol proto,
                const netSocketBlockingType blockingType,
                const netAddressFamily addressFamily,
				const unsigned sendBufferSize,
				const unsigned receiveBufferSize)
{
    netAssert(!m_IsInitialized);
    netAssert(m_IsCreated);

	m_Id = NextId();

	this->SetInitialValues();

    m_Port = desiredPort;
	m_DesiredPort = desiredPort;
	m_Proto = proto;
    m_BlockingType = blockingType;
    m_AddressFamily = addressFamily;
    m_DesiredSendBufferSize = sendBufferSize;
	m_DesiredReceiveBufferSize = receiveBufferSize;

	BANK_ONLY(m_LastDiagnosticCheckTime = 0;)

#if FAKE_DROP_LATENCY
    const unsigned curTime = sysTimer::GetSystemMsTime();

    m_DropBurstStartTime = m_DropBurstEndTime = curTime;
    m_CurrentDropPcnt = 0;

    m_LatencyBurstStartTime = m_LatencyBurstEndTime = curTime;
#endif  //FAKE_DROP_LATENCY

	netDebug("[%u] Initialising with desired port %d...", m_Id, m_DesiredPort);

    m_IsInitialized = true;

    return m_IsInitialized;
}

void
netSocket::Shutdown()
{
	netDebug("[%u] Shutting down socket on port %d...", m_Id, m_Port);

    {
        SYS_CS_SYNC(m_CsPktRec);
        while(!m_PktRecorders.empty())
        {
            this->UnregisterPacketRecorder(*m_PktRecorders.begin());
        }
    }

    if(m_IsInitialized)
    {
        this->NativeShutdown();

        m_IsInitialized = false;

        this->SetInitialValues();
    }
}

void
netSocket::Update()
{
    SYS_CS_SYNC(m_Cs);

	if(!m_PktRecorders.empty())
	{
		const unsigned curTime = sysTimer::GetSystemMsTime();

		PktRecorderList::iterator it = m_PktRecorders.begin();
		PktRecorderList::const_iterator stop = m_PktRecorders.end();
		for(; stop != it; ++it)
		{
			(*it)->Update(curTime);
		}
	}

	NativeUpdate();

#if FAKE_DROP_LATENCY
    this->UpdateBandwidthLimiters();
#endif  //FAKE_DROP_LATENCY
}

void
netSocket::RecordOutboundPacket(const netSocketAddress& addr,
                                const void* bytes,
                                const unsigned numBytes)
{
    SYS_CS_SYNC(m_Cs);

    PktRecorderList::iterator it = m_PktRecorders.begin();
    PktRecorderList::const_iterator stop = m_PktRecorders.end();
    for(; stop != it; ++it)
    {
        (*it)->RecordOutboundPacket(addr, bytes, numBytes);
    }
}

void
netSocket::RecordInboundPacket(const netSocketAddress& addr,
                                const void* bytes,
                                const unsigned numBytes)
{
    SYS_CS_SYNC(m_Cs);

    PktRecorderList::iterator it = m_PktRecorders.begin();
    PktRecorderList::const_iterator stop = m_PktRecorders.end();
    for(; stop != it; ++it)
    {
        (*it)->RecordInboundPacket(addr, bytes, numBytes);
    }
}

bool
netSocket::SetSendReceiveBufferSizes(const unsigned sendBufferSize,
									 const unsigned receiveBufferSize)
{
	rtry
	{
		// if either size is 0, we use the current/default value for that buffer size.
		// if both are 0, do nothing
		if((sendBufferSize == 0) && (receiveBufferSize == 0))
		{
			return true;
		}

		unsigned curSendBufferSize = 0;
		unsigned curReceiveBufferSize = 0;
		rverify(NativeGetSendReceiveBufferSizes(curSendBufferSize, curReceiveBufferSize), catchall, );
		netDebug("[%u] Send/receive buffer sizes in bytes before setting: %u/%u", m_Id, curSendBufferSize, curReceiveBufferSize);

		unsigned sendSize = sendBufferSize;
		unsigned receiveSize = receiveBufferSize;

		if(sendBufferSize == 0)
		{
			sendSize = curSendBufferSize;
		}

		if(receiveBufferSize == 0)
		{
			receiveSize = curReceiveBufferSize;
		}

		rverify(NativeSetSendReceiveBufferSizes(sendSize, receiveSize), catchall, );

#if !__NO_OUTPUT
		rverify(NativeGetSendReceiveBufferSizes(curSendBufferSize, curReceiveBufferSize), catchall, );
		netDebug("[%u] Send/receive buffer sizes changed to: %u/%u", m_Id, curSendBufferSize, curReceiveBufferSize);
#endif

		return true;
	}
	rcatchall
	{
		return false;
	}
}

#if FAKE_DROP_LATENCY
void
netSocket::UpdateBandwidthLimiters()
{
    const unsigned curTime = sysTimer::GetSystemMsTime() | 0x01;
    unsigned deltaMs =
        m_LastBwUpdateTime ? (curTime - m_LastBwUpdateTime) : 0;
    m_LastBwUpdateTime = curTime;

    while(deltaMs >= 1000)
    {
        if(m_InBoundAccum > 0)
        {
            m_InBoundAccum -= m_InboundBytesPerSec * 1000;
            if(m_InBoundAccum < 0)
            {
                m_InBoundAccum = 0;
            }
        }

        if(m_OutBoundAccum > 0)
        {
            m_OutBoundAccum -= m_OutboundBytesPerSec * 1000;
            if(m_OutBoundAccum < 0)
            {
                m_OutBoundAccum = 0;
            }
        }

        deltaMs -= 1000;
    }

    if(deltaMs > 0)
    {
        if(m_InBoundAccum > 0)
        {
            m_InBoundAccum -= m_InboundBytesPerSec * deltaMs;
            if(m_InBoundAccum < 0)
            {
                m_InBoundAccum = 0;
            }
        }

        if(m_OutBoundAccum > 0)
        {
            m_OutBoundAccum -= m_OutboundBytesPerSec * deltaMs;
            if(m_OutBoundAccum < 0)
            {
                m_OutBoundAccum = 0;
            }
        }
    }
}

bool netSocket::IsHeldPacketReady()
{
	HoldPacket* hp = nullptr;
	
	unsigned receiveTime = 0;

	{
		SYS_CS_SYNC(m_Cs);

		if(m_HoldQueue.empty())
		{
			return false;
		}

		hp = *m_HoldQueue.begin();
		receiveTime = hp->m_ReceiveTime;
	}
	
	const unsigned curTime = sysTimer::GetSystemMsTime();

	return int(receiveTime - curTime) <= 0;
}
#endif  //FAKE_DROP_LATENCY

}   //namespace rage

