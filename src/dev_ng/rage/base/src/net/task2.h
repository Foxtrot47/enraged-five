// 
// net/task2.h
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_TASK2_H
#define NET_TASK2_H

#include "atl/inlist.h"
#include "net/netdiag.h"
#include "net/status.h"
#include "profile/rocky.h"
#include "system/ipc.h"
#include "system/lambda.h"
#include "system/threadpool.h"
#include "system/timer.h"
#include "system/tls.h"

#include <cstdint>

//This file implements the netTask2 parallel tasking system.
//
//netTask2 is intended to run tasks in a single threaded environment.
//As such, netTask2 is not thread safe.
//
//A task is a unit of work represented by a C++ lambda function.
//The lambda will be called periodically to update the state of
//the task.
//
//As long as the lambda returns netTaskState::Pending it will
//continue to be called.
//
//If a lambda needs to maintain state between invocations the state should
//be "captured" when the lambda is created.
//
//When the task is complete the lambda should return netTaskState::Succeeded,
//netTaskState::Failed or netTaskState::Canceled.
//
//If a cancelation is requested the lambda should return netTaskState::Canceled
//as soon as possible.  Some tasks might need to continue running even
//after a cancelation is requested.  When they finally complete they should
//return netTaskState::Canceled.
//
//All running tasks are updated when the static function netTask2::Update()
//is called.
//
//Continuations can be added to any task.  A continuation is scheduled to
//run as soon as its antecedent task completes.
//
//If a continuation is added to task that has already completed the continuation
//will be added as a child task to the completed task's parent.
//
//IOW, a continuation added to a completed tasks will be immediately scheduled
//to run.
//
//If a cancelation is requested for an antecedent task all of its continuations
//will also receive cancelation requests.
//
//Canceled continuations will not actually complete until they have a chance to
//run, which is only after the canceled antecedent task completes.
//
//Tasks can have child tasks.  When a task has one or more child tasks the
//parent task will "sleep" until the child tasks complete.  IOW, the parent
//task's lambda will not be called until all children (and children's children,
//and children's continuations) have completed.
//
//It is not permitted to add a child task to a parent task that has already
//completed.
//
//If a cancellation is requested for a parent task all of its child tasks
//will also receive cancellation requests.
//
//Canceled parent tasks will not actually complete until they have a chance to
//run, which is only after all canceled child tasks complete.
//
//Task grouping can be achieved by creating a dummy parent task and then
//adding children.  The dummy parent should do nothing more than return
//netTaskState::Succeeded.  When all child tasks complete the parent task
//will complete.

#define NETTASK2_POOL_PROFILING (__BANK && 1)

namespace rage
{

// This macro can be placed within a netTaskFunction to quickly
// auto-cancel it, provided no cancellation cleanup is required.
#define AUTO_CANCEL()                                       \
	if(netTask2::IsCancelRequested())						\
	{														\
		netTask2Debug("Cancelling...")						\
		return netTaskState::Canceled;						\
	}														

//Return value from a task's OnUpdate function.
enum class netTaskState
{
    None,
    Pending,
    Succeeded,
	Failed,
    Canceled
};

// The thread that a task should be executed on. Either the Main thread,
// or a worker thread in the netThreadPool
enum class netTaskThread
{
	Main,
	ThreadPool
};

RAGE_DECLARE_SUBCHANNEL(ragenet, task2)

#if !__NO_OUTPUT

// Helper macro for declaring a task, either on the main thread or on the thread pool. Pass in the 
// task name and the tag of the diagChannel you wish to use.
#if USE_PROFILER_NORMAL
#define TASK2_DECL(name,tag)  netTaskThread::Main, #name, &Channel_##tag, PROFILER_LAMBDA_DESCRIPTION(#name)
#define TASK2_DECLT(name,tag) netTaskThread::ThreadPool, #name, &Channel_##tag, PROFILER_LAMBDA_DESCRIPTION(#name)
#else
#define TASK2_DECL(name,tag)  netTaskThread::Main, #name, &Channel_##tag
#define TASK2_DECLT(name,tag) netTaskThread::ThreadPool, #name, &Channel_##tag
#endif

// Defined in rl.h
extern u64 rlGetPosixTime(bool forceSync);

// Logging helper. Accepts a diagChannel, severity and task details like task name, task id, parent task id, etc.
#define netTask2LogfHelper(channel,taskName,taskId,parentId,severity,fmt,...)\
	diagLogfHelper(*channel, severity, "[%" I64FMT "u] %s[%u,%u]: " fmt, rlGetPosixTime(false), taskName, taskId, parentId, ##__VA_ARGS__);

//Use this version when calling from within a netTaskFunction
#define netTask2Debug(fmt, ...)     netTask2LogfHelper(netTask2::GetDiagChannel(), netTask2::GetTaskName(), netTask2::GetTaskId(), netTask2::GetParentTaskId(), DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netTask2Debug1(fmt, ...)    netTask2LogfHelper(netTask2::GetDiagChannel(), netTask2::GetTaskName(), netTask2::GetTaskId(), netTask2::GetParentTaskId(), DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netTask2Debug2(fmt, ...)    netTask2LogfHelper(netTask2::GetDiagChannel(), netTask2::GetTaskName(), netTask2::GetTaskId(), netTask2::GetParentTaskId(), DIAG_SEVERITY_DEBUG2, fmt, ##__VA_ARGS__);
#define netTask2Debug3(fmt, ...)    netTask2LogfHelper(netTask2::GetDiagChannel(), netTask2::GetTaskName(), netTask2::GetTaskId(), netTask2::GetParentTaskId(), DIAG_SEVERITY_DEBUG3, fmt, ##__VA_ARGS__);
#define netTask2Warning(fmt, ...)   netTask2LogfHelper(netTask2::GetDiagChannel(), netTask2::GetTaskName(), netTask2::GetTaskId(), netTask2::GetParentTaskId(), DIAG_SEVERITY_WARNING, fmt, ##__VA_ARGS__);
#define netTask2Error(fmt, ...)     netTask2LogfHelper(netTask2::GetDiagChannel(), netTask2::GetTaskName(), netTask2::GetTaskId(), netTask2::GetParentTaskId(), DIAG_SEVERITY_ERROR, fmt, ##__VA_ARGS__);

//Use this version from within the netTask2 code base when specifying a netTaskImpl directly.
#define netTask2ImplDebug(task, fmt, ...)	 netTask2LogfHelper(task->GetDiagChannel(), task->GetTaskName(), task->GetTaskId(), task->GetParentTaskId(), DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netTask2ImplDebug1(task, fmt, ...)	 netTask2LogfHelper(task->GetDiagChannel(), task->GetTaskName(), task->GetTaskId(), task->GetParentTaskId(), DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netTask2ImplDebug2(task, fmt, ...)	 netTask2LogfHelper(task->GetDiagChannel(), task->GetTaskName(), task->GetTaskId(), task->GetParentTaskId(), DIAG_SEVERITY_DEBUG2, fmt, ##__VA_ARGS__);
#define netTask2ImplDebug3(task, fmt, ...)	 netTask2LogfHelper(task->GetDiagChannel(), task->GetTaskName(), task->GetTaskId(), task->GetParentTaskId(), DIAG_SEVERITY_DEBUG3, fmt, ##__VA_ARGS__);
#define netTask2ImplWarning(task, fmt, ...)	 netTask2LogfHelper(task->GetDiagChannel(), task->GetTaskName(), task->GetTaskId(), task->GetParentTaskId(), DIAG_SEVERITY_WARNING, fmt, ##__VA_ARGS__);
#define netTask2ImplError(task, fmt, ...)	 netTask2LogfHelper(task->GetDiagChannel(), task->GetTaskName(), task->GetTaskId(), task->GetParentTaskId(), DIAG_SEVERITY_ERROR, fmt, ##__VA_ARGS__);

#else

// Helper macro for declaring a task, either on the main thread or on the thread pool. Pass in the 
// task name and the tag of the diagChannel you wish to use.
#define TASK2_DECL(name,tag)  netTaskThread::Main
#define TASK2_DECLT(name,tag) netTaskThread::ThreadPool

//Use this version when calling from within a netTaskFunction
#define netTask2Debug(fmt, ...)   
#define netTask2Debug1(fmt, ...)  
#define netTask2Debug2(fmt, ...)  
#define netTask2Debug3(fmt, ...)  
#define netTask2Warning(fmt, ...) 
#define netTask2Error(fmt, ...)   

//Use this version from within the netTask2 code base when specifying a netTaskImpl directly.
#define netTask2ImplDebug(task, fmt, ...)
#define netTask2ImplDebug1(task, fmt, ...)
#define netTask2ImplDebug2(task, fmt, ...)
#define netTask2ImplDebug3(task, fmt, ...)
#define netTask2ImplWarning(task, fmt, ...)
#define netTask2ImplError(task, fmt, ...)

#endif // !__NO_OUTPUT

// A netTaskFunction is a lambda function that represents a unit of work. It must be declared using , which
// will include logging parameters on !__NO_OUTPUT builds, and a parameter-less function for NO_OUTPUT builds
typedef LambdaCopy<netTaskState ()> netTaskFunction;

// Forward declaration of the netTask implementation
class netTaskImpl;

//PURPOSE
//  Used as a link in a linked list of netTaskImpl.
class netTaskLink
{
public:

    explicit netTaskLink(netTaskImpl* task)
        : m_Task(task)
    {
    }

    inlist_node<netTaskLink> m_Link;

    inlist_node<netTaskLink> m_WaitersLink;

    netTaskImpl* m_Task;
};

// PURPOSE
//	Thread Pool Work item for Net Tasks requiring blocking calls.
//	Performs the work normally done by a netTask within a worker in
//	the net Thread Pool. Is owned by a netTaskImpl.
class netTaskWorkItem : public sysThreadPool::WorkItem
{
	friend class netTask2;

public:

	netTaskWorkItem()
		: m_Owner(NULL)
		, m_Result(netTaskState::None)
		, m_SetupTime(0)
	{
	}

	template<typename T>
	void Setup(netTaskImpl* owner, const T& func)
	{
		m_Owner = owner;
		m_WorkFunction = func;
		m_SetupTime = sysTimer::GetSystemMsTime();
	}

	// PURPOSE
	//	Return the Success/Fail/Cancel properties of the work item
	virtual bool HasSuccessResult() const { return m_Result == netTaskState::Succeeded; }
	virtual bool HasFailResult() const { return m_Result == netTaskState::Failed; }
	virtual bool HasCancelResult() const { return m_Result == netTaskState::Canceled; }

	// PURPOSE
	//	Returns the time since the task was created
	unsigned TimeSinceCreation() { return sysTimer::GetSystemMsTime() - m_SetupTime; }

private:

	virtual void DoWork();
	virtual void OnFinishedCanceled() {m_Result = netTaskState::Canceled;}
	
	netTaskFunction m_WorkFunction;
	netTaskImpl* m_Owner;
	netTaskState m_Result;
	unsigned m_SetupTime;
};


//PURPOSE
//  Implementation of a task object.
//  netTaskImpl is ref counted.
//  netTask2 contains a shared instance of netTaskImpl.
class netTaskImpl
{
    friend class netTask2;
	friend class netTaskWorkItem;

public:

    netTaskImpl();

    ~netTaskImpl();

private:

    netTaskImpl(const netTaskImpl& that);
    netTaskImpl& operator=(const netTaskImpl& that);

	// PURPOSE
	//	Resets the task, clearing its work item and no-output values
	void Reset();

    //PURPOSE
    //  Returns true if the task has completed.
    bool IsComplete() const;

	//PURPOSE
	//	Returns true if the task has succeeded, failed, 
	//  or canceled depending on the request.
	bool IsSucceeded() const;
	bool IsFailed() const;
	bool IsCanceled() const;

#if !__NO_OUTPUT
	// PURPOSE
	//	Returns the diag channel, task name, task id, or profiler description
	const diagChannel* GetDiagChannel() const;
	const char* GetTaskName() const;
	unsigned GetTaskId() const;
	unsigned GetParentTaskId() const;
#if USE_PROFILER_NORMAL
	const Profiler::EventDescription* GetTaskDescription() const;
#endif

	// PURPOSE
	//	Returns the time since creation, time pending or time executing.
	unsigned TimeSinceCreation() const;
	unsigned TimePending() const;
	unsigned TimeExecuting() const;
#endif

	// PURPOSE
	//	Sets the task into a pending state, as well as its
	//	netStatus pointer if it has one.
	void SetPending();

    //PURPOSE
    //  Requests cancellation of the task, the task's child tasks,
    //  and the tasks continuations.
    //
    //  NOTE: This is only a *request*.
    //  Tasks will continue running, even after a cancelation
    //  request, until they return IsComplete/Canceled.
    void Cancel();

	//PURPOSE
	//  Requests cancelation of the task if its netStatus matches
	//	the given status object. Otherwise, forwards the status
	//	on to its children to look for a match.
	//
	//  NOTE: This is only a *request*.
	//  Tasks will continue running, even after a cancelation
	//  request, until they return IsComplete/Canceled. Tasks on
	//  the thread pool will not return Cancelled until the work
	//	has completed.
	void CancelByStatus(netStatus* status);

	// PURPOSE
	//	Detaches a netStatus from a task.
	void DetachTask(netStatus* status);

    //PURPOSE
    //  Adds 1 to the ref count.
    void Ref();

    //PURPOSE
    //  Subtracts 1 from the ref count.  If the ref count reaches
    //  zero the netTaskImpl object is reclaimed.
    void Unref();

    //PURPOSE
    //  Adds a child task.
    //  The parent task will sleep until all child tasks,
    //  child of child tasks, and child task continuations
    //  complete.
    void AddChild(netTaskImpl* task);

    //PURPOSE
    //  Adds a continuation.
    //  Continuations will run when the antecedent task completes.
    void AddContinuation(netTaskImpl* task);

    //PURPOSE
    //  Adds a reference to a task that is waiting on this task.
    //  When this task completes it will "unwait" all of its waiters.
    void AddWaiter(netTaskImpl* task);

	netTaskWorkItem* m_AsyncWorkItem;

    netTaskFunction m_OnUpdate;

    netTaskState m_State;

	netStatus* m_Status;

    netTaskImpl* m_Parent;

    netTaskLink m_Link;

    inlist<netTaskLink, &netTaskLink::m_Link> m_Continuations;
    inlist<netTaskLink, &netTaskLink::m_Link> m_Children;
    inlist<netTaskLink, &netTaskLink::m_WaitersLink> m_Waiters;

#if !__NO_OUTPUT
	static const u8 MAX_TASK_NAME_BUF_SIZE = 24;
	char m_TaskName[MAX_TASK_NAME_BUF_SIZE];
	diagChannel* m_Channel;
	unsigned m_TaskId;
	unsigned m_StartTime;
	unsigned m_QueueTime;
#if USE_PROFILER_NORMAL
	Profiler::EventDescription* m_ProfilerDescription;
#endif
#endif

    int m_RefCount;

    bool m_DeleteOnRelease	: 1;

    bool m_CancelRequested	: 1;

    bool m_Wait1Child       : 1;    //If true then run after at least one child completes

    bool m_Waiting          : 1;    //True if waiting on another task, i.e. we're in the tasks m_Waiters collection.
};

class netTask2
{
    friend netTaskImpl;
	friend netTaskWorkItem;

public:

	static const size_t TASK_POOL_SIZE = 256;
	static const size_t WORK_ITEM_POOL_SIZE = 64;

    netTask2();

    ~netTask2();

    netTask2(const netTask2& that);

    netTask2& operator=(const netTask2& that);

	// PURPOSE
	//	Returns the thread pool to be used for netTasks
	static sysThreadPool* GetThreadPool();

	// PURPOSE
	//	Returns true if the task is valid.
	bool IsInitialized() const;

    //PURPOSE
    //  Returns true if the task has completed.
	//	Could be Succeeded, Failed or Canceled
    bool IsComplete() const;

	//PURPOSE
	//	Returns true if the task is pending, succeeded, 
	//  failed, or canceled depending on the request.
	bool IsPending() const;
	bool IsSucceeded() const;
	bool IsFailed() const;
	bool IsCanceled() const;

    //PURPOSE
    //  Schedules a continuation.
    //  Continuations will run when the antecedent task completes.
    template<typename Func>
    netTask2 Then(const Func& onUpdate, netStatus* status, const netTaskThread taskThread OUTPUT_ONLY(,const char* taskName, diagChannel* taskChannel PROFILER_NORMAL_ONLY(, Profiler::EventDescription* profilerDescription = nullptr)))
    {
        if(!netVerify(m_TaskImpl)) return netTask2();

		netTask2 task = netTask2::CreateTask(onUpdate, status, taskThread OUTPUT_ONLY(,taskName, taskChannel PROFILER_NORMAL_ONLY(, profilerDescription)));

        m_TaskImpl->AddContinuation(task.m_TaskImpl);

        return task;
    }

	// 'Then' overload that defaults to an anonymous task on the main thread with the specified status object.
	template<typename Func>
	netTask2 Then(const Func& onUpdate, netStatus* status)
	{
		return Then(onUpdate, status, TASK2_DECL(AnonymousTask,ragenet_task2));
	}

	// 'Then' overload that defaults to a no-status, anonymous task on the main thread.
	template<typename Func>
	netTask2 Then(const Func& onUpdate)
	{
		return Then(onUpdate, NULL, TASK2_DECL(AnonymousTask,ragenet_task2));
	}

    //PURPOSE
    //  Adds a child task.
    //  The parent task will sleep until all child tasks,
    //  child of child tasks, and child task continuations
    //  complete.
    void AddChildTask(netTask2 task);

    //PURPOSE
    //  Requests cancelation of the task, the task's child tasks,
    //  and the tasks continuations.
    //
    //  NOTE: This is only a *request*.
    //  Tasks will continue running, even after a cancelation
    //  request, until they return IsComplete/Canceled.
    void Cancel();

	// PURPOSE
	//	Cancels a task that owns the corresponding netStatus
	//  NOTE: This is only a *request*.
	//  Tasks will continue running, even after a cancelation
	//  request, until they return IsComplete/Canceled.
	static void CancelTask(netStatus* status);

	// PURPOSE
	//	Detaches a netStatus from a task.
	static void DetachTask(netStatus* status);

    //PURPOSE
    //  Schedules a task to run immediately
    static void ScheduleTask(netTask2 task);

    //PURPOSE
    //  Puts the current task to sleep until the awaited task completes.
    //
    //  Fails if there is no currently running task.
    static void Wait(const netTask2& task);

    //PURPOSE
    //  Schedules a task to run when all dependency tasks complete.
    template<typename Func>
    static netTask2 WhenAll(netTask2* tasks, const size_t numTasks, const Func& onUpdate)
    {
        netTask2 master = netTask2::CreateTask(onUpdate);

        for(int i = 0; i < (int)numTasks; ++i)
        {
            netTask2 waiter = netTask2::CreateTask([]()
            {
                return netTaskState::Succeeded;
			}, nullptr, TASK2_DECL(WhenAllTask, ragenet_task2));

            tasks[i].m_TaskImpl->AddWaiter(waiter.m_TaskImpl);

            master.AddChildTask(waiter);
        }

        ScheduleTask(master);

        return master;
    }

    //PURPOSE
    //  Schedules a task to run when at least one dependency tasks completes.
    template<typename Func>
    static netTask2 WhenAny(netTask2* tasks, const size_t numTasks, const Func& onUpdate)
    {
        netTask2 waiter = WhenAll(tasks, numTasks, []()
		{
			return netTaskState::Succeeded;
		});

        waiter.m_TaskImpl->m_Wait1Child = true;

        return waiter.Then(onUpdate);
    }

	// PURPOSE
	//  Creates a task to run with the netFaskFunction 
	template<typename Func>
	static bool CreateThreadPoolTask(netTaskImpl* parentTask, const Func& onUpdate)
	{
		if (!netVerify(parentTask))
			return false;

		netTaskImpl* task = parentTask;

		// If the ThreadPool is specified, we need to allocate a work item for the task.
		netTaskWorkItem* workItem = netTask2::CreateWorkItem(task, onUpdate);
		if (!netVerify(workItem))
		{
			return false;
		}

		// Assign the work item
		task->m_AsyncWorkItem = workItem;

		// Get current thread pool for tasks (defaults to netThreadPool)
		sysThreadPool* threadPool = GetThreadPool();

		// As the netTaskFunction has been given to the work item for execution, we must create our
		// own netTaskFunction that monitors the work item.
		task->m_OnUpdate = netTaskFunction([task, threadPool]()
		{
			// If the task has requested a cancel, attempt to cancel the work item.
			// If the work item checks its cancel status (NOT A GUARANTEE), it can
			// early out. However, don't return out of this function until the 
			// work item has completed as it could be accessing memory owned by the task.
			if(netTask2::IsCancelRequested())
			{
				threadPool->CancelWork(task->m_AsyncWorkItem->GetId());
			}

			// If a task is canceled while in queue, we can early out in a canceled state
			if(task->m_AsyncWorkItem->Queued() && task->m_AsyncWorkItem->WasCanceled())
			{
				return netTaskState::Canceled;
			}
			else if(task->m_AsyncWorkItem->Pending()) // queued and not canceled, or it's active
			{
				return netTaskState::Pending;
			}

			// If the underlying work item has finished, we can return any of our Complete values
			//	(success, failed, canceled). If a task is canceled, it may actually complete
			//	all of its work successfully, but still return in a canceled state.
			if (task->m_AsyncWorkItem->Finished())
			{
				// Extract the results of the operation. A cancelled task should have a cancelled result,
				//	a task that failed should return a failed result, and any task ran to completion without
				//  a cancel or failure should return success.
				if (task->m_AsyncWorkItem->HasCancelResult())
					return netTaskState::Canceled;
				else if (task->m_AsyncWorkItem->HasFailResult())
					return netTaskState::Failed;
				else
					return netTaskState::Succeeded;
			}

			// Attempt to queue the work item. If the thread pool is full, and cannot queue the work, simply return Pending
			//	until we reach a timeout. 
			if (!threadPool->QueueWork(task->m_AsyncWorkItem))
			{
				// TODO: Custom timeouts?
				if (task->m_AsyncWorkItem->TimeSinceCreation() > THREADPOOL_QUEUE_TIMEOUT_MS)
				{
					netTask2ImplDebug(task, "ABORTED - no threads available in pool (%ums executing, %ums in queue, %ums total)", task->TimeExecuting(), task->TimePending(), task->TimeSinceCreation());
					return netTaskState::Failed;
				}
				else
				{
					return netTaskState::Pending;
				}
			}

			return netTaskState::Pending;	
		});

		return true;
	}

    //PURPOSE
    //  Creates a task but does not schedule it.
    template<typename Func>
    static netTask2 CreateTask(const Func& onUpdate, netStatus* status = NULL, const netTaskThread taskThread = netTaskThread::Main OUTPUT_ONLY(,const char* taskName = "AnonymousTask", diagChannel* taskChannel = NULL PROFILER_NORMAL_ONLY(, Profiler::EventDescription* profilerDescription = nullptr)))
	{
		netTaskImpl* task = AllocTask();
		if (!netVerify(task))
		{
			return netTask2();
		}

		task->m_Status = status;
		task->m_DeleteOnRelease = true;

#if !__NO_OUTPUT
		task->m_TaskId = NextId();
		task->m_Channel = taskChannel;
		safecpy(task->m_TaskName, taskName);
		task->m_QueueTime = sysTimer::GetSystemMsTime();

#if USE_PROFILER_NORMAL
		task->m_ProfilerDescription = profilerDescription;
#endif

		netTask2ImplDebug(task, "Scheduling...");
#endif

		if (taskThread == netTaskThread::Main)
		{
			task->m_OnUpdate = onUpdate;
		}
		else if (taskThread == netTaskThread::ThreadPool)
		{
			if (!netVerify(CreateThreadPoolTask(task, onUpdate)))
				return netTask2();
		}

		return netTask2(task);
	}

	// PURPOSE
	//	Allocates a netTaskWorkItem to handle the given function
	//	in a work item in the current netTask2 thread pool.
	template<typename Func>
	static netTaskWorkItem* CreateWorkItem(netTaskImpl* parent, const Func& func)
	{
		netTaskWorkItem* workItem = netTask2::AllocWorkItem();
		if (workItem)
		{
			workItem->Setup(parent, func);
		}
		return workItem;
	}

    //PURPOSE
    //  Creates and schedules a task to run immediately.
    template<typename Func>
    static netTask2 ScheduleTask(const Func& onUpdate, netStatus* status, const netTaskThread taskThread OUTPUT_ONLY(,const char* taskName, diagChannel* taskChannel PROFILER_NORMAL_ONLY(, Profiler::EventDescription* profilerDescription = nullptr)))
	{
		netTask2 task = CreateTask(onUpdate, status, taskThread OUTPUT_ONLY(,taskName, taskChannel PROFILER_NORMAL_ONLY(, profilerDescription)));

		ScheduleTask(task);

		return task;
	}

	// 'ScheduleTask' overload that defaults to an anoynmous task on the main thread.
	template<typename Func>
	static netTask2 ScheduleTask(const Func& onUpdate, netStatus* status)
	{
		return ScheduleTask(onUpdate, status, TASK2_DECL(AnonymousTask,ragenet_task2));
	}

	// 'ScheduleTask' overload that defaults to a no-status, anonymous task on the main thread.
	template<typename Func>
	static netTask2 ScheduleTask(const Func& onUpdate)
	{
		return ScheduleTask(onUpdate, NULL, TASK2_DECL(AnonymousTask,ragenet_task2));
	}

    //PURPOSE
    //  Creates and adds a child task to the currently running task.
    //  The currently running task is the task whose OnUpdate function
    //  is currently running.
    //  Fails if there is no currently running task.
    template<typename Func>
    static netTask2 ScheduleChildTask(const Func& onUpdate, netStatus* status, const netTaskThread taskThread OUTPUT_ONLY(,const char* taskName, diagChannel* taskChannel PROFILER_NORMAL_ONLY(, Profiler::EventDescription* profilerDescription)))
	{
		// Child tasks must be created on the main thread. If you find yourself in a situation where a blocking, thread-pool
		// task needs to create a sub-unit of work, the task could probably be written as a main thread task with two
		// distinct separate blocking calls on the thread pool. We can potentially conquer this with a queue or liberal
		// use of critical sections, but no valid requirements of multiple threadpool tasks have arisen yet.
		if (!netVerifyf(sysIpcGetCurrentThreadId() == sm_ThreadId, 
			"netTask2 tasks must be created from the main thread. Ensure that you are using TASK2_DECL instead of TASK2_DECLT in your parent task and that the parent task is also non-blocking."))
		{
			return netTask2();
		}

		if(!netVerify(sm_CurTask))
		{
			return netTask2();
		}

		netTask2 task = CreateTask(onUpdate, status, taskThread OUTPUT_ONLY(,taskName, taskChannel PROFILER_NORMAL_ONLY(, profilerDescription)));

		sm_CurTask->AddChild(task.m_TaskImpl);

		return task;
	}

	// 'ScheduleChildTask' overload that defaults to an anonymous task on the main thread with the specified netStatus.
	template<typename Func>
	static netTask2 ScheduleChildTask(const Func& onUpdate, netStatus* status)
	{
		return ScheduleChildTask(onUpdate, status, NULL, TASK2_DECL(AnonymousTask,ragenet_task2));
	}

	// 'ScheduleChildTask' overload that defaults to a no-status, an anonymous task on the main thread.
	template<typename Func>
	static netTask2 ScheduleChildTask(const Func& onUpdate)
	{
		return ScheduleChildTask(onUpdate, NULL, TASK2_DECL(AnonymousTask,ragenet_task2));
	}

	// PURPOSE
	//	Call once on startup
	static bool Init();

    //PURPOSE
    //  Call periodically to invoke OnUpdate for all scheduled tasks.
    static void Update();

    //PURPOSE
    //  Returns true if the currently running task on the current thread has been requested to cancel.
    static bool IsCancelRequested();

	//PURPOSE
	//  Generates a new task ID.
	static unsigned NextId();

#if !__NO_OUTPUT
	//PURPOSE
	//  Returns the task id of the current running task on the current thread.
	static int GetTaskId();

	//PURPOSE
	//  Returns the task id of the parent of the current running task on the current thread.
	static int GetParentTaskId();

	//PURPOSE
	//  Returns the task name of the current running task on the current thread.
	static const char* GetTaskName();

#if USE_PROFILER_NORMAL
	//PURPOSE
	//  Returns profiler description of the current running task on the current thread.
	static const Profiler::EventDescription* GetTaskDescription();
#endif

	//PURPOSE
	//  Returns the diag channel of the current running task on the current thread.
	static const diagChannel* GetDiagChannel();
#endif

	// PURPOSE
	//	Runs the netTask2 unit tests
	// Can probably be removed and replaced with a unit testing framework soon.
	static void TestTask();

	static void SetThreadPool(sysThreadPool * tp) { sm_threadPool = tp;}
private:

	static const int THREADPOOL_QUEUE_TIMEOUT_MS = (30 * 1000);

    explicit netTask2(netTaskImpl* taskImpl);

    static netTaskImpl* AllocTask();
    static void FreeTask(netTaskImpl* task);

	static netTaskWorkItem* AllocWorkItem();
	static void FreeWorkItem(netTaskWorkItem* workItem);

    static void UpdateTask(netTaskImpl* task);
   
    static netTaskImpl& GetRootTask();

	netTaskImpl* m_TaskImpl;

	static sysIpcCurrentThreadId sm_ThreadId;

	static DECLARE_THREAD_PTR(netTaskImpl, sm_CurTask);

	static DECLARE_THREAD_PTR(sysThreadPool, sm_threadPool);

#if NETTASK2_POOL_PROFILING
	static void UpdatePoolUsage();

	static float m_TaskPoolMaxUsage;
	static float m_WorkerPoolMaxUsage;
#endif
};

class netTask2AutoUseThreadPool
{
public:
	netTask2AutoUseThreadPool (sysThreadPool* newPool)
	{
		m_PrevPool = netTask2::GetThreadPool();
		netTask2::SetThreadPool(newPool);
	}

	~ netTask2AutoUseThreadPool ()
	{
		netTask2::SetThreadPool(m_PrevPool);
	}

private:

	sysThreadPool* m_PrevPool;
};

}   //namespace rage

#endif  //NET_TASK2_H