// 
// net/net.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_NET_H
#define NET_NET_H

#include "file/file_config.h"
#include "system/bit.h"
#include "net/netdiag.h"

#if RSG_LINUX || RSG_IOS || RSG_ANDROID
#include <netdb.h>
typedef addrinfo ADDRINFO;
#endif

namespace rage
{

#ifndef MAX
#define	MAX(a,b) (((a)>(b))?(a):(b))
#endif

#ifndef __FINAL_NETWORK_LOGGING
#define __FINAL_NETWORK_LOGGING __FINAL_LOGGING
#endif

// Track bandwidth usage
#ifdef DISABLE_BANDWIDTH_TRACKING
    #define ENABLE_BANDWIDTH_TRACKING 0
#else
    #if __DEV || __BANK || (__FINAL && __FINAL_NETWORK_LOGGING)
        #define ENABLE_BANDWIDTH_TRACKING 1
    #else
        #define ENABLE_BANDWIDTH_TRACKING 0
    #endif
#endif

#if ENABLE_BANDWIDTH_TRACKING
    #define	BANDWIDTH_TRACKING_ONLY(...) __VA_ARGS__
#else
    #define	BANDWIDTH_TRACKING_ONLY(...)
#endif // ENABLE_BANDWIDTH_TRACKING

#define ENABLE_BANDWIDTH_TELEMETRY 1

#define NET_RAGE_TRACK(x)    RAGE_TRACK(net);RAGE_TRACK(x)

// GTA V does not support cross-platform multiplayer
#define NET_CROSS_PLATFORM_MP (0)

// we support IPv4/IPv6 dual stack networking on certain platforms.
#define IPv4_IPv6_DUAL_STACK (RSG_IOS)

#if IPv4_IPv6_DUAL_STACK
#define IPv4_IPv6_DUAL_STACK_ONLY(...) __VA_ARGS__
#define IPv4_IPv6_DUAL_STACK_SWITCH(x,y) x
#else
#define IPv4_IPv6_DUAL_STACK_ONLY(...) 
#define IPv4_IPv6_DUAL_STACK_SWITCH(x,y) y
#endif

// server optimization - disable NAT detection on platforms that don't need it.
#define NET_DISABLE_NAT_DETECTION (__RGSC_DLL || RSG_LAUNCHER || RSG_IOS || RSG_ANDROID)

// server optimization - disable relay server discovery and pinging on platforms that don't need it.
#define NET_DISABLE_RELAY_SERVER (__RGSC_DLL || RSG_LAUNCHER || RSG_IOS || RSG_ANDROID)

//PURPOSE
//  This define controls whether to support DTLS for encrypting
//  traffic sent/received over the relay/presence servers.
#define SUPPORT_DTLS (RSG_PC || RSG_DURANGO || RSG_SCE || RSG_IOS || RSG_ANDROID || RSG_LINUX || RSG_NX)

#if SUPPORT_DTLS
	/*
		Hash size: 20 bytes (160 bits)
		+ RECORD_HEADER_SZ (5 bytes)
		+ DTLS_RECORD_EXTRA (8 bytes)
		+ 16 byte initialization vector (IV)
		+ 1 pad byte
		+ up to 16 bytes more padding
		= up to 66 bytes
	*/
#define MAX_SIZEOF_DTLS_OVERHEAD 66
#endif

/*
	1 byte for flags
	4 bytes for random IV seed
	HMAC: 64-bits (8 bytes)
	Up to 16 bytes of padding.
	============================
	29 bytes of overhead
*/
#define MAX_SIZEOF_P2P_CRYPT_OVERHEAD (1 + 4 + 8 + 16)

#if SUPPORT_DTLS
#define MAX_SIZEOF_CRYPTO_OVERHEAD MAX(MAX_SIZEOF_DTLS_OVERHEAD, MAX_SIZEOF_P2P_CRYPT_OVERHEAD)

// * 2 due to double-encryption when relaying packets through other peers
#define MAX_SIZEOF_DOUBLE_P2P_CRYPT_OVERHEAD (MAX_SIZEOF_P2P_CRYPT_OVERHEAD * 2)
#define MAX_SIZEOF_DOUBLE_CRYPTO_OVERHEAD MAX(MAX_SIZEOF_DTLS_OVERHEAD, MAX_SIZEOF_DOUBLE_P2P_CRYPT_OVERHEAD)
#else
#define MAX_SIZEOF_CRYPTO_OVERHEAD (MAX_SIZEOF_P2P_CRYPT_OVERHEAD)
#endif

// On consoles, calls to rlPresence functions can be serviced by rlScPresence functions.
// On PC, calls to rlPresence get serviced by the Social Club DLL, which internally calls rlScPresence.
#if !defined SC_PRESENCE
#define SC_PRESENCE __RGSC_DLL || !RSG_PC
#endif

#if SC_PRESENCE
#define SC_PRESENCE_ONLY(x) x
#else
#define SC_PRESENCE_ONLY(x)
#endif

#define	MAX_HOSTNAME_BUF_SIZE (128)
#define MAX_URI_PATH_BUF_SIZE (512)
#define MAX_QUERY_STRING_BUF_SIZE (512)

#define CRLF "\r\n"

//PURPOSE
//  These macros are used to make packet signatures.  Packet
//  signatures are a sequence of bytes at a known offset in
//  a packet that can be used to identify and filter packets.
//
//NOTES
//  The values of the macro parameters should be < 32.
//
//EXAMPLES
//  static const unsigned SIG = NET_MAKE_SIG3('F'-'A', 'O'-'A', 'O'-'A');
//
#define NET_MAKE_SIG1(a)            (a)
#define NET_MAKE_SIG2(a, b)         ((NET_MAKE_SIG1(a)<<5) | NET_MAKE_SIG1(b))
#define NET_MAKE_SIG3(a, b, c)      ((NET_MAKE_SIG2(a,b)<<5) | NET_MAKE_SIG1(c))
#define NET_MAKE_SIG4(a, b, c, d)   ((NET_MAKE_SIG3(a,b,c)<<5) | NET_MAKE_SIG1(d))

// 0 is considered an invalid endpointId (along with NET_INVALID_ENDPOINT_ID) so we can catch
// common errors with systems initializing endpointIds to 0 then trying using them.
typedef unsigned EndpointId; 
static const EndpointId NET_INVALID_ENDPOINT_ID = ~0u; 
#define NET_IS_VALID_ENDPOINT_ID(i) (((i) > 0) && ((i) != NET_INVALID_ENDPOINT_ID))

typedef unsigned ChannelId; 
static const ChannelId NET_INVALID_CHANNEL_ID = ~0u; 
#define NET_IS_VALID_CHANNEL_ID(i) ((i) <= NET_MAX_CHANNEL_ID)

static const int NET_INVALID_CXN_ID = -1;

//PURPOSE
//  Parameter type to netConnectionManager::CloseConnection().
enum netCloseType
{
    //Decrement the connection's ref count and if it hits zero,
    //close the connection gracefully.  The connection will remain in
    //the PENDING_CLOSE state until all reliable frames have been
    //acknowledged and until all ACKs have been sent.
    NET_CLOSE_GRACEFULLY,

    //Decrement the connection's ref count and if it hits zero,
    //close the connection immediately without regard to pending
    //frames or ACKs.
    NET_CLOSE_IMMEDIATELY,

    //Same as close immediate, except ignore the connection's ref
    //count and just close it.
    NET_CLOSE_FINALLY,
};

//PURPOSE
//  Flags passed to netConnectionManager::Send().
enum netSendFlags
{
    // ** FLAGS SENT IN OUR PACKET HEADER 

    NET_SEND_RELIABLE           = BIT0,  //< send reliably
    NET_SEND_NUM_HEADER_FLAGS   = 1,
    NET_SEND_FLAG_HEADER_MASK   = (1 << NET_SEND_NUM_HEADER_FLAGS) - 1,

    // ** FLAGS USED LOCALLY (NOT SENT WITH PACKET)

    NET_LOCAL_SHIFT             = 24,

    // if this flag is present the frame will be sent immediately
    NET_SEND_IMMEDIATE          = BIT0 << NET_LOCAL_SHIFT,

    // if this flag is present, the buffer is interpreted as a netOutFrame
    // pre-allocated using the connection manager's allocator. It will be
    // sent without making a copy of the buffer, thus saving CPU time.
    // Once it is passed to the connection manager, the connection manager
    // takes ownership of the buffer and frees it when needed.
    NET_SEND_OUT_FRAME          = BIT1 << NET_LOCAL_SHIFT,
};

enum
{
	//Pre-allocated channels
	NET_TUNNELER_CHANNEL_ID = 14,
	NET_RESERVED_CHANNEL_ID = 15,

    //Maximum number of channels.
    NET_MAX_CHANNELS = 16,
	NET_MAX_APP_CHANNELS = 14,

	NET_MAX_APP_CHANNEL_ID = NET_MAX_APP_CHANNELS - 1,

	//Maximum value for channel ids.
    NET_MAX_CHANNEL_ID = NET_MAX_CHANNELS - 1,

    //Maximum number of chars in a host name
    NET_MAX_HOSTNAME_CHARS = 256
};

enum netProtocol
{
    NET_PROTO_INVALID   = -1,        
    NET_PROTO_UDP,      //User datagram protocol
};

//PURPOSE
//  Error codes related to netSocket.
enum netSocketError
{
    NET_SOCKERR_NONE,               // no error (m_Error is set to this if the function succeeds)
    NET_SOCKERR_NETWORK_DOWN,       // ENETDOWN - network shut down, probably due to buffer overflow or memory failure
    NET_SOCKERR_NO_BUFFERS,         // ENOBUFS - outgoing buffers full
    NET_SOCKERR_MESSAGE_TOO_LONG,   // WSAEMSGSIZE - message size was too long
    NET_SOCKERR_HOST_UNREACHABLE,   // WSAEHOSTUNREACH - Xbox NAT negotiation failure
    NET_SOCKERR_CONNECTION_RESET,   // WSAECONNRESET - stop trying to send to somebody who stopped listening
    NET_SOCKERR_WOULD_BLOCK,        // WSAEWOULDBLOCK - operation on a nonblocking socket could not be completed immediately
    NET_SOCKERR_IN_PROGRESS,
    NET_SOCKERR_INVALID_SOCKET,     // Socket is invalid.
	NET_SOCKERR_INACTIVEDISABLED,	// Network disconnection occurred because of an IP address release
	NET_SOCKERR_INVALID_ARG,		// WSAEINVAL - Some invalid argument was supplied
	NET_SOCKERR_BROKEN_CONNECTION,	// EPIPE / ENOTCONN - iOS specific when SIGPIPE is disabled, either can be returned by send()/recv()
	NET_SOCKERR_UNKNOWN             // some other error or I'm too lazy to set the error correctly
};

//PURPOSE
//  NAT types.
//  NOTE: This enum must match the NAT type values on the session server.
enum netNatType
{
    NET_NAT_UNKNOWN,
    NET_NAT_OPEN,
    NET_NAT_MODERATE,
    NET_NAT_STRICT,

    NET_NAT_NUM_TYPES
};

//TODO: Rename to TcpConnectReason
// What system is opening the socket?
enum ConnectReason {
    CR_INVALID,
    CR_ELASTIC,
    CR_CDN_UPLOAD_PHOTO,
    CR_CDN_UPLOAD_VIDEO,
    CR_ORBIS_MEDIA_DECODER,
    CR_CLOUD,
    CR_TELEMETRY_UNUSED,
    CR_STATS_UNUSED,
    CR_ENTITLEMENT,
    CR_FACEBOOK,
    CR_SC_CREATE_AUTH_TOKEN,
    CR_SC_CREATE_LINK_TOKEN,
    CR_ROS_CREATE_TICKET,
    CR_ROS_CREDENTIALS_CHANGING,
    CR_ROS_GET_GEOLOCATION_INFO,
    CR_ROS_GET_NAT_DISCOVERY_SERVERS,
    CR_SOCIAL_CLUB,
    CR_UGC,
    CR_CLOUD_SAVE,
    CR_ACHIEVEMENTS,
    CR_MATCHMAKING,
    CR_FRIENDS,
    CR_CREW,
    CR_CONDUCTOR_ROUTE,
    CR_PRESENCE,
    CR_INBOX,
    CR_YOUTUBE,
    CR_SAVE_MIGRATION,
    CR_FEED,
    CR_REGISTER_PUSH_NOTIFICATION_DEVICE,
    CR_GAME_SERVER,
    CR_ITEM_DATABASE,
    CR_POSSE,
    CR_TEST,
    CR_BUGSTAR,
    CR_GOOGLE_ANALYTICS,
    CR_DAILY_OBJECTIVES,
    CR_SP_ACTION_PROXY,
    CR_SESSION_SERVER,
    CR_CASH_INVENTORY_SERVER,
    CR_BOUNTY_SERVER,
    CR_ANTI_CHEAT_SERVER,
    CR_MINIGAME_SERVER,
    CR_MESSAGE_DISTRIBUTION_SERVER,
    CR_COUNT
};

#if RSG_DURANGO || RSG_ORBIS
#if !defined(NET_THREADPOOL_CPU_1)
#define NET_THREADPOOL_CPU_1 1
#endif

#if !defined(NET_THREADPOOL_CPU_2)
#define NET_THREADPOOL_CPU_2 2
#endif
#else // RSG_DURANGO || RSG_ORBIS
#if !defined(NET_THREADPOOL_CPU_1)
#define NET_THREADPOOL_CPU_1 0
#endif

#if !defined(NET_THREADPOOL_CPU_2)
#define NET_THREADPOOL_CPU_2 0
#endif
#endif // RSG_DURANGO || RSG_ORBIS

// PURPOSE
//	Defines the size of the net thread pool
#ifndef NET_THREAD_POOL_NUM_THREADS
#define NET_THREAD_POOL_NUM_THREADS (4)
#endif

class sysMemAllocator;
class netSocketManager;

//PURPOSE
//  Initializes the networking system
//PARAMS
//  allocator   - Allocator to use for general memory allocation.
//                Used mostly for allocating memory for inbound/outbound packets.
//  socketManager - a socket manager that will be used for p2p and relay/presence traffic.
bool netInit(sysMemAllocator* allocator, netSocketManager* socketManager);

//PURPOSE
//  Updates the networking system.  Call once per frame.
void netUpdate();

//PURPOSE
//  Shuts down the networking system.
void netShutdown();

// PURPOSE
//	Initializes or shuts down the networking thread pool.
bool netThreadPoolInit();
void netThreadPoolShutdown();

//PURPOSE
//  Use this instead of mthRandom because mthRandom has a bug
//  where the first number in the sequence of random numbers
//  has a very poor distribution.
//  netRandom uses the same algorithm as mthRandom except
//  it throws out the first number in the sequence.
class netRandom
{
public:

    netRandom();

    explicit netRandom(const int seed);

    void Reset(const int seed);

	//PURPOSE
	//  Sets the seed using crypto RNG.
	void SetRandomSeed();

    void SetFullSeed(const u64 seed);

    int GetInt();

	// PURPOSE: Returns a random float, 0.0f <= return < 1.0f
	// RETURNS: a random float
	float GetFloat();

	int GetRanged(const int m, const int M);

	// Cryptographically random - slow. For better performance,
	// use an RNG initialized with SetRandomSeed.
	static int GetIntCrypto();
	static int GetRangedCrypto(const int m, const int M);

protected:

	// See the internals of this function for why the first results are discarded.
	void DiscardFirst();

	u32 m_Seed[2];
	bool m_bDiscardFirst;
};

}   //namespace rage

#endif  //NET_H
