// 
// net/tcp.h
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_TCP_H
#define NET_TCP_H

#include "atl/inlist.h"
#include "netaddress.h"
#include "netsocket.h"
#include "status.h"
#include "rline/rl.h"
#include "system/criticalsection.h"
#include "system/threadpool.h"
#include "rline/rl.h" // for RL_FORCE_STAGE_ENVIRONMENT: todo - remove this dependency from net -> rline

#include <stddef.h>

#define ENABLE_TCP_PROXY (RSG_OUTPUT || RL_FORCE_STAGE_ENVIRONMENT)
#define ENABLE_TCP_PROXY_ARGS (ENABLE_TCP_PROXY && !RSG_FINAL)

#if ENABLE_TCP_PROXY
#define ENABLE_TCP_PROXY_ONLY(...) __VA_ARGS__
#else
#define ENABLE_TCP_PROXY_ONLY(...)
#endif

#if ENABLE_TCP_PROXY_ARGS
#define ENABLE_TCP_PROXY_ARGS_ONLY(...) __VA_ARGS__
#else
#define ENABLE_TCP_PROXY_ARGS_ONLY(...)
#endif

#define NET_TCP_SOCKET_ID_INVALID (-1)

struct WOLFSSL_CTX;
struct WOLFSSL;

namespace rage
{
class netTcp;

typedef ::WOLFSSL_CTX SSL_CTX;

enum netTcpWaitType
{
    NETTCP_WAIT_READ,
    NETTCP_WAIT_WRITE
};

enum netTcpResult
{
    NETTCP_RESULT_CANCELED      = -1,
    NETTCP_RESULT_ERROR         = -2,
    NETTCP_RESULT_TIMED_OUT     = -3,
    NETTCP_RESULT_DISCONNECTED  = -4,
    NETTCP_RESULT_OK            = 0
};

//PURPOSE
//  Represents an asynchronous TCP operation (connect/send/receive).
//  Instances of netTcpAsync are passed to ConnectAsync(), SendBufferAsync(),
//  and ReceiveBufferAsync().  The instance passed to those functions can be
//  polled for completion.
class netTcpAsyncOp
{
    friend class netTcp;
    friend class netHttpRequest;

public:

    netTcpAsyncOp();

    ~netTcpAsyncOp();

    //PURPOSE
    //  Returns true if the operation is pending.
    bool Pending() const;

    //PURPOSE
    //  Returns true if the operation succeeded.
    bool Succeeded() const;

    //PURPOSE
    //  If Pending() returns false, returns the result code of a
    //  completed operation.
    //  If Pending() returns true, returns an undefined value.
    int GetResultCode() const;

	//PURPOSE
	//	Returns the operation error cod.e
	//	Equal to wolfSSL_get_error() for SSL request, or LastError() otherwise.
	int GetLastError() const;

private:

    enum OpType
    {
        OP_INVALID      = -1,
        OP_CONNECT,
        OP_SSL_CONNECT,
        OP_RECV_BUFFER,
        OP_SEND_BUFFER,

        OP_NUM_TYPES
    };

    void Clear();

    bool ResetForConnect(const netSocketFd sktFd,
                        const netSocketAddress& addr,
                        ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                        int* callerSktId,
						const char* contextStr,
                        const unsigned timeoutSecs,
                        const ConnectReason socketSource);

    bool SslResetForConnect(SSL_CTX* sslCtx,
                            const netSocketFd sktFd,
                            const netSocketAddress& addr,
                            ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                            const char* domainName,
                            int* callerSktId,
							const char* contextStr,
                            const unsigned timeoutSecs,
                            const ConnectReason socketSource);

    bool ResetForRecvBuffer(const int sktId,
                           void* buf,
                           const unsigned bufSize,
                           const unsigned timeoutSecs);

    bool ResetForSendBuffer(const int sktId,
                           const void* buf,
                           const unsigned bufSize,
                           const unsigned timeoutSecs);

    void Complete(const netStatus::StatusCode statusCode,
                   netTcpResult result,
				   int lastError = 0);

    OpType m_Type;
    int m_SktId;
	int m_LastError;

    enum ConnectState
    {
        STATE_SOCKET_CONNECT,    //Connecting the low level socket
#if ENABLE_TCP_PROXY
        STATE_SEND_PROXY_REQUEST,
        STATE_RECV_PROXY_RESPONSE,
#endif //ENABLE_TCP_PROXY
        STATE_SSL_CONNECT        //SSL handshake, key exchange, etc.
    };

	enum SslSocketOp
	{
		SSL_SOCKET_OP_READ,
		SSL_SOCKET_OP_WRITE,
	};

    union
    {
        struct
        {
            u8 m_AddrBuf[sizeof(netSocketAddress)];
            netSocketAddress* m_Addr;
#if ENABLE_TCP_PROXY
            u8 m_ProxyAddrBuf[sizeof(netSocketAddress)];
            netSocketAddress* m_ProxyAddr;
#endif //ENABLE_TCP_PROXY
            int* m_CallerSktId;
            ConnectReason m_ConnectReason;
            ConnectState m_State;
        } m_Connect;

        struct
        {
            u8 m_AddrBuf[sizeof(netSocketAddress)];
            netSocketAddress* m_Addr;
#if ENABLE_TCP_PROXY
            u8 m_ProxyAddrBuf[sizeof(netSocketAddress)];
            netSocketAddress* m_ProxyAddr;
#endif //ENABLE_TCP_PROXY
            int* m_CallerSktId;
            ConnectState m_State;
			SslSocketOp m_NextSocketOp;
            SSL_CTX* m_SslCtx;
            const char* m_DomainName;
            ConnectReason m_ConnectReason;
        } m_SslConnect;

        struct
        {
            void* m_Buf;
            unsigned m_SizeofBuf;
            unsigned m_NumRcvd;
        } m_RcvBuf;

        struct
        {
            const void* m_Buf;
            unsigned m_SizeofBuf;
            unsigned m_NumSent;
        } m_SndBuf;
    } m_Data;

    int m_TimeoutMs;
    netStatus m_MyStatus;

    unsigned m_Id;

    inlist_node<netTcpAsyncOp> m_ListLink;

#if !__NO_OUTPUT
    static const char* GetOpTypeString(const OpType opType);
#endif
};

//PURPOSE
//  Abstracts TCP operations.
//  The int "socket" parameter passed to functions is not an actual socket.
//  Attempts to use it in send(), recv(), etc., will fail.
class netTcp
{
    friend class netTcpAsyncOp;
    friend struct netTcpSslLoggingAutoContext;

public:
	
	//PURPOSE
	//	Give access to the threadpool defined in the definition file
	static sysThreadPool *GetThreadPool();

	static bool Init();
	static void Shutdown();

    //PURPOSE
    //  Connect asynchronously.  The caller should monitor asyncOp to
    //  determine when the operation is complete.
    //PARAMS
    //  addr        - Remote endpoint to which a connection will
    //                be established.
    //  proxyAddr   - Optional proxy address over which an HTTP tunnel
    //                will be created. A connection will be established
    //                with proxyAddr, the proxy will then connect to
    //                addr and traffic will flow through the tunnel.
    //  sktId       - Upon successful return, will contain the value
    //                of the socket.  Until the operation completes the
    //                the socket cannot be used for reading/writing
    //                and should not be closed.
	//	contextStr	- Optional context string used in debug output.
    //  timeoutSecs - Seconds that must elapsed before the operation
    //                times out.
    //  op          - Used to reference the operation until it completes.
    //NOTES
    //  Do not deallocate the memory used for skt or op
    //  while the operation is pending.
    static bool ConnectAsync(const netSocketAddress& addr,
                            ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                            int* sktId,
							const char* contextStr,
                            const unsigned timeoutSecs,
                            const ConnectReason connectReason,
                            netTcpAsyncOp* op);

    //PURPOSE
    //  Connect synchronously. Don't use this unless you really need
    //  a blocking operation. Please note that the function might take
    //  slightly longer than specified in timeoutSecs.
    //PARAMS
    //  See ConnectAsync for the parameters
    static bool Connect(const netSocketAddress& addr,
                            ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                            int* sktId,
                            const char* contextStr,
                            const unsigned timeoutSecs,
                            ConnectReason connectReason);

    //PURPOSE
    //  Connect asynchronously via SSL.
    //PARAMS
    //  sslCtx      - The SSL_CTX to control SSL behavior
    //                (e.g. CipherSuites, certification validation)
    //  addr        - Remote endpoint to which a connection will
    //                be established.
    //  proxyAddr   - Optional proxy address over which an HTTPS tunnel
    //				  will be created. A connection will be established
    //				  with proxyAddr, the proxy will then connect to
    //				  addr and traffic will flow through the tunnel.
    //  domainName  - Null-terminated domain name to check against
    //                that stored in the server's certificate. Must
    //                remain valid until canceling the async op.
    //  sktId       - Upon successful return, will contain the value
    //                of the socket.  Until the operation completes the
    //                the socket cannot be used for reading/writing
    //                and should not be closed.
	//	contextStr	- Optional context string used in debug output.
    //  timeoutSecs - Seconds that must elapsed before the operation
    //                times out.
    //  op          - Used to reference the operation until it completes.
    //NOTES
    //  Do not deallocate the memory used for skt or op
    //  while the operation is pending.
    static bool SslConnectAsync(SSL_CTX *sslCtx,
                                const netSocketAddress& addr,
								ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                                const char* domainName,
                                int* sktId,
								const char* contextStr,
                                const unsigned timeoutSecs,
                                const ConnectReason connectReason,
                                netTcpAsyncOp* op);

    //PURPOSE
    //  Connect synchronously via SSL. Don't use this unless you really
    //  need a blocking operation. Please note that the function might
    //  take slightly longer than specified in timeoutSecs.
    //PARAMS
    //  See SslConnectAsync for the parameters
    static bool SslConnect(SSL_CTX *sslCtx,
                           const netSocketAddress& addr,
                           ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr,)
                           const char* domainName,
                           int* sktId,
                           const char* contextStr,
                           const unsigned timeoutSecs,
                           ConnectReason connectReason);

    //PURPOSE
    //  Shuts down and closes a socket.
    static void Close(const int sktId);

    //PURPOSE
    //  Non-blocking send that writes up to specified amount.  On return
    //  bytesSent will contain the number of bytes actually sent, which
    //  might be less than the size of the buffer.
    //PARAMS
    //  sktId           Connected socket on which to send.
    //  buf             Buffer to send.
    //  bufSize         Size of buffer.
    //  bytesSent       (out) Number of bytes sent
	//	lastError		(out) Optional - Error code returned by LastError() or SSL_get_error
    static netTcpResult Send(const int sktId,
                            const void* buf,
                            const unsigned bufSize,
							unsigned* bytesSent,
							int* lastError = NULL);

    //PURPOSE
    //  Non-blocking receive that reads up to specified amount.  On return
    //  bytesReceived will contain the number of bytes actually received,
    //  which might be less than the size of the buffer.
    //PARAMS
    //  sktId           Connected socket on which to receive.
    //  buf             Buffer to hold received data.
    //  bufSize         Size of buffer.
    //  bytesReceived   (out) Number of bytes read
	//	lastError		(out) Optional - Error code returned by LastError() or SSL_get_error
    static netTcpResult Receive(const int sktId,
                                void* buf,
                                const unsigned bufSize,
                                unsigned* bytesReceived,
								int* lastError = NULL);

    //PURPOSE
    //  Blocking send.  Does not return until entire buffer is sent,
    //  or failure/timeout.
    //PARAMS
    //  sktId           Connected socket on which to send.
    //  buf             Buffer to send.
    //  bufSize         Size of buffer.
    //  timeoutSecs     Seconds to wait for data before timing out
    static netTcpResult SendBuffer(const int sktId,
                                   const void* buf, 
                                   const unsigned bufSize,
                                   const unsigned timeoutSecs);

    //PURPOSE
    //  Blocking receive that does not return until entire buffer is
    //  filled, or failure/timeout.
    //PARAMS
    //  sktId           Connected socket on which to receive.
    //  buf             Buffer to hold received data.
    //  bufSize         Size of buffer.
    //  timeoutSecs     Seconds to wait for data before timing out
    //RETURNS
    //  True if successfully received bufSize bytes, false otherwise.
    static netTcpResult ReceiveBuffer(const int sktId,
                                      void* buf, 
                                      const unsigned bufSize,
                                      const unsigned timeoutSecs);

    //PURPOSE
    //  Send asynchronously.  The caller should monitor asyncOp to
    //  determine when the operation is complete.
    //  The operation is complete when bufSize bytes have been sent.
    //PARAMS
    //  sktId       - Connected socket on which to send.
    //  buf         - Bytes to send.
    //  bufSize     - Number of bytes to send.
    //  timeoutSecs - Seconds that must elapsed before the operation
    //                times out.
    //  op          - Used to reference the operation until it completes.
    //NOTES
    //  Do not deallocate the memory used for buf or op
    //  while the operation is pending.
    static bool SendBufferAsync(const int sktId,
                               const void* buf,
                               const unsigned bufSize,
                               const unsigned timeoutSecs,
                               netTcpAsyncOp* op);

    //PURPOSE
    //  Receive asynchronously.  The caller should monitor asyncOp to
    //  determine when the operation is complete.
    //  The operation is complete when bufSize bytes have been received.
    //PARAMS
    //  sktId       - Connected socket on which to receive.
    //  buf         - Destination buffer.
    //  bufSize     - Number of bytes to receive.
    //  timeoutSecs - Seconds that must elapsed before the operation
    //                times out.
    //  op          - Used to reference the operation until it completes.
    //NOTES
    //  Do not deallocate the memory used for buf or op
    //  while the operation is pending.
    static bool ReceiveBufferAsync(const int sktId,
                                   void* buf,
                                   const unsigned bufSize,
                                   const unsigned timeoutSecs,
                                   netTcpAsyncOp* op);

    //PURPOSE
    //  Cancels an asynchronous operation.
    static void CancelAsync(netTcpAsyncOp* op);

	//PURPOSE
	//  Retrieves the default, secure SSL_CTX.
	//NOTES
	//  The caller is expected to load its trusted roots.
	//  This ctx needs to be owned by tcp.cpp so that it is only freed in netTcp::Shutdown(),
	//  where we can be sure there are no more in-flight ops that are still referencing it,
	//  and after which no new ops can be queued.
	static SSL_CTX* GetSecureSslCtx();

    //PURPOSE
    //  Retrieves the default, insecure SSL_CTX.
    //  This should generally not be used, as it
    //  is insecure and does not authenticate the server,
    //  and is subject to mitm attacks.
    //  Primarily only used for internal development services,
    //  or communication with external parties using CAs
    //  that we don't have pinned on the client.
    static SSL_CTX* GetInsecureSslCtx();

#if !RSG_FINAL
	//PURPOSE
	//  Retrieves the default, debug SSL_CTX.
	//  This is the same as GetInsecureSslCtx(), but uses
	//  the debug allocator.
	static SSL_CTX* GetDebugSslCtx();
#endif

    static u32 GetBytesSent(const int sktId);
    static u32 GetBytesSentOnWire(const int sktId);

#if !__NO_OUTPUT
	static char* GetSocketContextStr(const int sktId);

	static const char *GetConnectReasonName(const ConnectReason connectReason);
    static void PrintSocketInfo(const netSocketFd sktFd);
#endif // !__NO_OUTPUT

#if ENABLE_BANDWIDTH_TRACKING
    static u32 GetTotalBytesSentByConnectReason(ConnectReason connectReason);
    static u32 GetTotalBytesSentOnWireByConnectReason(ConnectReason connectReason);
    static u32 GetTotalBytesReceivedByConnectReason(ConnectReason connectReason);
#endif

#if ENABLE_BANDWIDTH_TELEMETRY
    static u32 GetTotalBytesSent();
    static u32 GetTotalBytesSentOnWire();
    static u32 GetTotalBytesReceived();
#endif

#if RSG_BANK
    static void AddWidgets();
#endif

    //PURPOSE
    //  Starts the worker thread.
    static void StartWorker(const int cpuAffinity);

private:
	typedef inlist<netTcpAsyncOp, &netTcpAsyncOp::m_ListLink> AsyncOpList;

	enum UpdateOpsResult
	{
		UPDATE_OPS_DONE,
		UPDATE_OPS_CONTINUE,
		UPDATE_OPS_YIELD,
	};

	static UpdateOpsResult UpdateOps();
    static void Update();

	static netSocketAddress GetSocketAddr(const netSocketFd sktFd);

	IPv4_IPv6_DUAL_STACK_ONLY(static bool SetSockOptIpV6Only(netSocketFd* sktFd, const bool forceIpV6Only));
	static bool SetSockOptBlocking(netSocketFd* sktFd, const netSocketBlockingType blockingType);
	static bool SetSockOptAbortiveClose(netSocketFd* sktFd, const bool enabledAbortiveClose);
	static bool SetSockOptDisableSigpipe(netSocketFd* sktFd);
	static bool SetSockOptDisableNagle(netSocketFd* sktFd);
	static bool SetSockOptRcvBufferSize(netSocketFd* sktFd, const unsigned rcvBufSizeBytes);
	static int GetSockOptRcvBufferSize(netSocketFd* sktFd);

#if !__NO_OUTPUT
	static void DumpSslConnectionInfo(WOLFSSL* ssl, const netTcpAsyncOp* op, const int result);
    static void TcpSslLogCb(const int logLevel, const char* const NOTFINAL_ONLY(logMessage));
#endif

	//PURPOSE
	static bool CreateSocket(netSocketFd* sktFd,
							 const char* contextStr);

	//PURPOSE
    static bool ConnectFd(const netSocketAddress& addr,
                        netSocketFd* sktFd,
						const char* contextStr);

    //PURPOSE
    //  Common setup for SslConnect and SslConnectAsync
    static bool SslConnectCommon(SSL_CTX *sslCtx,
                              const netSocketAddress& addr,
                              ENABLE_TCP_PROXY_ONLY(const netSocketAddress& proxyAddr, )
                              const char* domainName,
                              int* sktId,
                              const char* contextStr,
                              const unsigned timeoutSecs,
                              netSocketFd& sktFd);

    //PURPOSE
    //  Blocks until at least one socket is ready for either reading or writing.
    //PARAMS
    //  sktFds          Array of raw sockets to wait on (not socket infos).
    //  ready           Upon completion will contain true for ready sockets.
    //  except          Upon completion will contain true for sockets with errors.
    //  numSkts         Number of sockets to wait on.
    //  waitType        Type of operation to wait for.
    //  timeoutSecs     Seconds to wait before giving up.
    //RETURNS
    //  True on success.
    //NOTES
    //  Before checking "ready" be sure to check "except", as both could be
    //  set to true when a connection attempt is being made.
    static bool WaitForSocketFds(const netSocketFd* sktFds,
                              bool* ready,
                              bool* except,
                              const int numSkts,
                              const netTcpWaitType waitType,
                              const unsigned timeoutSecs);

    //Closes a raw socket.
    static void CloseFd(const netSocketFd sktFd);

    static bool QueueOp(netTcpAsyncOp* asyncOp);

    static void CompleteOp(const unsigned opId,
                            const netStatus::StatusCode statusCode,
                            netTcpResult result,
							int lastError = 0);

    //  Moves the pending ops onto the main op list.
    static void MovePendingOps();

#if RSG_LINUX
	static void IgnoreSigPipe();
#endif

	//PURPOSE
	//  Shuts down the worker thread.
	static void ShutdownWorker();

    static bool WakeupWorker();

    static void WorkerUpdate(void* p);

    //Functions for managing socket infos.
    static int AllocSocketInfo(const char* contextStr, ConnectReason connectReason);
	static void CancelOpsBySocketId(const int sktId, AsyncOpList& list);
    static void FreeSocketInfo(const int sktId);
    static struct netTcpSocketInfo* GetSocketInfo(const int sktId);
    static bool IsValidSocketInfo(const int sktId);
	static netSocketFd GetSocketFd(const int sktId);

#if ENABLE_TCP_PROXY
    // Format the header string sent to the proxy as handshake
    static void FormatProxyHeader(char(&buffer)[512], const netSocketAddress& addr, const char* domainName);

    static bool SetupProxyRoute(const netSocketAddress& addr,
                                const char* domainName,
                                int* sktId,
                                const char* contextStr,
								netSocketFd sktFd,
                                s64 timeoutMs);
#endif

    static AsyncOpList sm_OpList;
    static AsyncOpList sm_PendingOpList; // Operations recently queued while the worker was busy
	static bool sm_Initialized;
};

class netWolfSslHelper
{
public:
    static void Init();
    
    typedef void(*netWolfSslLoggingCb)(const int logLevel, const char* const logMessage);
    static void SetSslLoggingCb(netWolfSslLoggingCb callback);
    static netWolfSslLoggingCb GetSslLoggingCb();
    static void SslLogCb(const int logLevel, const char* const logMessage);
};

}; //namespace rage

#endif //NET_TCP_H
