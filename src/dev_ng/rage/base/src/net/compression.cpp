//
// net/compression.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#include "compression.h"

#include "bandwidth.h"
#include "connectionmanager.h"
#include "netdiag.h"
#include "nethardware.h"
#include "netsocket.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "file/asset.h"
#include "file/file_config.h"
#include "file/limits.h"
#include "file/stream.h"
#include "rline/rlpresence.h"
#include "system/criticalsection.h"
#include "system/timer.h"

// Oodle Network is a commercial compression product from RAD Game Tools that is built specifically for UDP game traffic.
// This requires a training phase to generate a dictionary (offline) from a large, representative sample of packets.
// See rage\base\tools\cli\netCompressionTrainer.
// If no such dictionary is supplied, compression is disabled.
#if (__RGSC_DLL) || RSG_LAUNCHER || RSG_IOS || RSG_ANDROID || RSG_NX || RSG_P || (GTA_VERSION != 0)
#define NET_USE_OODLE (0)
#else
#define NET_USE_OODLE (1)
#endif

#if NET_USE_OODLE
#include "oodle2.h"
#if RSG_ANDROID
#pragma comment(lib, "liboo2coreandroid64.a")
#elif RSG_DURANGO
#pragma comment(lib, "oo2core_xboxone.lib")
#elif RSG_IOS
#pragma comment(lib, "liboo2coreios.2.5.0.a")
#elif RSG_ORBIS
#pragma comment(lib, "liboo2coreps4.a")
#elif RSG_PC
#pragma comment(lib, "oo2core_win64.lib")
#elif !RSG_LINUX && !RSG_NX
#error Unknown Platform
#endif
#else
#include "data/compress.h"
#endif

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, compression)
#undef __rage_channel
#define __rage_channel ragenet_compression

#define NET_COMPRESSION_PROFILING (!__NO_OUTPUT)
#if NET_COMPRESSION_PROFILING
// how frequently to output compression stats to tty/log
static const unsigned NET_COMPRESSION_PROFILING_FLUSH_INTERVAL_MS = (60 * 1000);
#endif
	
#if NET_COMPRESSION_TRAINING
// how frequently to flush the packet streams when collecting packets
static const unsigned NET_COMPRESSION_TRAINING_FLUSH_INTERVAL_MS = (20 * 1000);
NOSTRIP_PARAM(netCompressionTraining, "[network] Collects uncompressed packets to files for training the compression dictionary. "
											"You can supply a path to a folder (eg. -netCompressionTraining=x:\\nct\\). "
											"If no folder is specifed, the files will be stored in the current working directory", "Disabled", "All", "");
XPARAM(processinstance);

//////////////////////////////////////////////////////////////////////////
// netPacketStreamRecorder
//PURPOSE
//  Records packets to an fiStream.
//////////////////////////////////////////////////////////////////////////
class netPacketStreamRecorder : public netPacketRecorder
{
public:

    netPacketStreamRecorder();
	virtual ~netPacketStreamRecorder();

	//PURPOSE
	//PARAMS
	//  path	- the fiStream will be created at this path, overwriting if it already exists.
	virtual bool Init(const char* opath, const char* ipath);

	//PURPOSE
	//  Flushes and closes the stream.
	virtual void Shutdown();

	virtual void SampleData(const unsigned curTimeMs);
	virtual void Update(const unsigned curTimeMs);
	virtual void Flush();

    //PURPOSE
    //  Implements abstract function defined in netPacketRecorder
    //  to record the packet to a stream.
    //PARAMS
    //  addr        - ignored.
    //  bytes       - ignored.
    //  numBytes    - Number of bytes.
    virtual void RecordOutboundPacket(const netSocketAddress& addr,
                                        const void* bytes,
                                        const unsigned numBytes);

    //PURPOSE
	//  Implements abstract function defined in netPacketRecorder
	//  to record the packet to a stream.
    //PARAMS
    //  addr        - ignored.
    //  bytes       - ignored.
    //  numBytes    - Number of bytes.
    virtual void RecordInboundPacket(const netSocketAddress& addr,
                                        const void* bytes,
                                        const unsigned numBytes);

private:
	char m_OutboundFilePath[RAGE_MAX_PATH];
	char m_InboundFilePath[RAGE_MAX_PATH];
	fiStream* m_OutboundStream;
	fiStream* m_InboundStream;
	mutable sysCriticalSectionToken m_Cs;
};

netPacketStreamRecorder::netPacketStreamRecorder()
: m_OutboundStream(NULL)
, m_InboundStream(NULL)
{
	m_InboundFilePath[0] = '\0';
	m_OutboundFilePath[0] = '\0';
}

netPacketStreamRecorder::~netPacketStreamRecorder()
{
	Shutdown();
}

bool
netPacketStreamRecorder::Init(const char* opath, const char* ipath)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		if(opath)
		{
			rverify(m_OutboundStream == NULL, catchall, );
			safecpy(m_OutboundFilePath, opath);
		}

		if(ipath)
		{
			rverify(m_InboundStream == NULL, catchall, );
			safecpy(m_InboundFilePath, ipath);
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
netPacketStreamRecorder::Shutdown()
{
	SYS_CS_SYNC(m_Cs);

	if(m_OutboundStream)
	{
		m_OutboundStream->Flush();
		m_OutboundStream->Close();
		m_OutboundStream = NULL;
	}

	if(m_InboundStream)
	{
		m_InboundStream->Flush();
		m_InboundStream->Close();
		m_InboundStream = NULL;
	}
}

void 
netPacketStreamRecorder::SampleData(const unsigned /*curTimeMs*/)
{

}

void 
netPacketStreamRecorder::Update(const unsigned /*curTimeMs*/)
{

}

void 
netPacketStreamRecorder::Flush()
{
	SYS_CS_SYNC(m_Cs);

	if(m_OutboundStream)
	{
		m_OutboundStream->Flush();
	}

	if(m_InboundStream)
	{
		m_InboundStream->Flush();
	}
}

void
netPacketStreamRecorder::RecordOutboundPacket(const netSocketAddress& /*addr*/,
												const void* bytes,
												const unsigned numBytes)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rcheck(bytes && (numBytes > 0), catchall, );

		if((m_OutboundStream == NULL) && (m_OutboundFilePath[0] != '\0'))
		{
			m_OutboundStream = fiStream::Create(m_OutboundFilePath);
			rverify(m_OutboundStream, catchall, );

			unsigned numChannels = 1;
			rverify(m_OutboundStream->Write(&numChannels, sizeof(unsigned)) == sizeof(unsigned), catchall, );
		}

		rcheck(m_OutboundStream, catchall, );

		unsigned channelIndex = 0;
		rverify(m_OutboundStream->Write(&channelIndex, sizeof(unsigned)) == sizeof(unsigned), catchall, );
		rverify(m_OutboundStream->Write(&numBytes, sizeof(unsigned)) == sizeof(unsigned), catchall, );
		rverify(m_OutboundStream->Write(bytes, (int)numBytes) == (int)numBytes, catchall, );
	}
	rcatchall
	{

	}
}

void
netPacketStreamRecorder::RecordInboundPacket(const netSocketAddress& /*addr*/,
                                            const void* bytes,
                                            const unsigned numBytes)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rcheck(bytes && (numBytes > 0), catchall, );

		if((m_InboundStream == NULL) && (m_InboundFilePath[0] != '\0'))
		{
			m_InboundStream = fiStream::Create(m_InboundFilePath);
			rverify(m_InboundStream, catchall, );

			unsigned numChannels = 1;
			rverify(m_InboundStream->Write(&numChannels, sizeof(unsigned)) == sizeof(unsigned), catchall, );
		}

		rcheck(m_InboundStream, catchall, );

		unsigned channelIndex = 0;
		rverify(m_InboundStream->Write(&channelIndex, sizeof(unsigned)) == sizeof(unsigned), catchall, );
		rverify(m_InboundStream->Write(&numBytes, sizeof(unsigned)) == sizeof(unsigned), catchall, );
		rverify(m_InboundStream->Write(bytes, (int)numBytes) == (int)numBytes, catchall, );
	}
	rcatchall
	{

	}
}

//////////////////////////////////////////////////////////////////////////
// netCompressionTrainer
//////////////////////////////////////////////////////////////////////////
class netCompressionTrainer
{
public:
	netCompressionTrainer();
	~netCompressionTrainer();
	bool Init(netConnectionManager* m_CxnMgr);
	void Shutdown();
	void Update();
	bool IsInitialized() const;
	bool IsCompressionTrainingEnabled() const;
	void RecordOutboundPacket(const u8* bytes, const unsigned numBytes);
	void RecordInboundPacket(const u8* bytes, const unsigned numBytes);

private:
	netConnectionManager* m_CxnMgr;
	netPacketStreamRecorder m_CompressionTrainingRecorder[NET_MAX_CHANNELS];
	netPacketStreamRecorder m_CompressionTrainingRecorderAll;
	bool m_CompressionTrainingEnabled;
	unsigned m_LastFlushTime;
	bool m_Initialized;
};

netCompressionTrainer::netCompressionTrainer()
: m_CxnMgr(NULL)
, m_LastFlushTime(0)
, m_Initialized(false)
, m_CompressionTrainingEnabled(false)
{

}

netCompressionTrainer::~netCompressionTrainer()
{
	Shutdown();
}

bool
netCompressionTrainer::Init(netConnectionManager* cxnMgr)
{
	rtry
	{
		rverify(!m_Initialized, catchall,);
		rverify(cxnMgr, catchall,);

		m_CxnMgr = cxnMgr;

		const char* folder = NULL;
		if(PARAM_netCompressionTraining.Get(folder))
		{
			netIpAddress ip;
			netHardware::GetLocalIpAddress(&ip);
			char ipStr[netIpAddress::MAX_STRING_BUF_SIZE];
			ip.Format(ipStr);
			char opath[RAGE_MAX_PATH];
			char ipath[RAGE_MAX_PATH];

			u32 processInstance = 0;
#if RSG_PC
			// our socket isn't necessarily bound to a port yet, so use -processinstance
			// to differentiate multiple instances on the same PC instead.
			PARAM_processinstance.Get(processInstance);
#endif
			
			if(folder && (folder[0] != '\0'))
			{
				ASSET.CreateLeadingPath(folder);

				formatf(opath, "%s\\net_ostream_all_%s_%u.log", folder, ipStr, processInstance);
				formatf(ipath, "%s\\net_istream_all_%s_%u.log", folder, ipStr, processInstance);
			}
			else
			{
				formatf(opath, "net_ostream_all_%s_%u.log", ipStr, processInstance);
				formatf(ipath, "net_istream_all_%s_%u.log", ipStr, processInstance);
			}

			rverify(m_CompressionTrainingRecorderAll.Init(opath, ipath), catchall, );

			for(int i = 0; i < NET_MAX_CHANNELS; ++i)
			{
				if(folder && (folder[0] != '\0'))
				{
					formatf(opath, "%s\\net_ostream_%d_%s_%u.log", folder, i, ipStr, processInstance);
					formatf(ipath, "%s\\net_istream_%d_%s_%u.log", folder, i, ipStr, processInstance);
				}
				else
				{
					formatf(opath, "net_ostream_%d_%s_%u.log", i, ipStr, processInstance);
					formatf(ipath, "net_istream_%d_%s_%u.log", i, ipStr, processInstance);
				}

				rverify(m_CompressionTrainingRecorder[i].Init(opath, ipath), catchall, );
			}

			// make sure all packet recorders were successfully initialized before registering any of them
			for(int i = 0; i < NET_MAX_CHANNELS; ++i)
			{
				m_CxnMgr->RegisterChannelPacketRecorder(i, &m_CompressionTrainingRecorder[i]);
			}

			m_CompressionTrainingEnabled = true;

			netDebug("Network compression training is enabled. Packets streams will be written to '%s'", folder);
		}

		m_Initialized = true;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
netCompressionTrainer::Shutdown()
{
	if(IsCompressionTrainingEnabled())
	{
		for(int i = 0; i < NET_MAX_CHANNELS; ++i)
		{
			m_CxnMgr->UnregisterChannelPacketRecorder(i, &m_CompressionTrainingRecorder[i]);
		}
	}

	for(int i = 0; i < NET_MAX_CHANNELS; ++i)
	{
		m_CompressionTrainingRecorder[i].Shutdown();
	}

	m_CxnMgr = NULL;
	m_CompressionTrainingEnabled = false;
	m_Initialized = false;
}

void 
netCompressionTrainer::Update()
{
	if(IsCompressionTrainingEnabled())
	{
		unsigned elapsed = sysTimer::GetSystemMsTime() - m_LastFlushTime;
		if(elapsed >= NET_COMPRESSION_TRAINING_FLUSH_INTERVAL_MS)
		{
			for(int i = 0; i < NET_MAX_CHANNELS; ++i)
			{
				m_CompressionTrainingRecorder[i].Flush();
			}

			m_CompressionTrainingRecorderAll.Flush();

			m_LastFlushTime = sysTimer::GetSystemMsTime();
		}
	}
}

bool
netCompressionTrainer::IsInitialized() const
{
	return m_Initialized;
}

bool
netCompressionTrainer::IsCompressionTrainingEnabled() const
{
	return m_CompressionTrainingEnabled;
}

void
netCompressionTrainer::RecordOutboundPacket(const u8* bytes, const unsigned numBytes)
{
	if(IsCompressionTrainingEnabled())
	{
		m_CompressionTrainingRecorderAll.RecordOutboundPacket(netSocketAddress::INVALID_ADDRESS, bytes, numBytes);
	}
}

void
netCompressionTrainer::RecordInboundPacket(const u8* bytes, const unsigned numBytes)
{
	if(IsCompressionTrainingEnabled())
	{
		m_CompressionTrainingRecorderAll.RecordInboundPacket(netSocketAddress::INVALID_ADDRESS, bytes, numBytes);
	}
}
#endif

#if NET_COMPRESSION_PROFILING
//////////////////////////////////////////////////////////////////////////
// netCompressionProfiler
//////////////////////////////////////////////////////////////////////////
struct netCompressionProfiler
{
	int totalBytesUncompressed;
	int totalBytesCompressed;
	int totalErrors;
	int totalCompressiblePackets;
	int totalIncompressiblePackets;
	utimer_t startTime;
	utimer_t totalCompressTime;
	utimer_t startTimeDecompress;
	utimer_t totalDecompressTime;
	unsigned lastCompressionStatsDumpTime;

	netCompressionProfiler()
	{
		totalBytesUncompressed = 0;
		totalBytesCompressed = 0;
		totalErrors = 0;
		startTime = 0;
		totalCompressTime = 0;
		totalCompressiblePackets = 0;
		totalIncompressiblePackets = 0;
		startTimeDecompress = 0;
		totalDecompressTime = 0;
		lastCompressionStatsDumpTime = 0;
	}

	void Update()
	{
		unsigned elapsedTimeSinceLastStatsDump = sysTimer::GetSystemMsTime() - lastCompressionStatsDumpTime;

		if(elapsedTimeSinceLastStatsDump >= NET_COMPRESSION_PROFILING_FLUSH_INTERVAL_MS)
		{
			PrintStats();
			lastCompressionStatsDumpTime = sysTimer::GetSystemMsTime();
		}
	}

	void StartCompress()
	{
		startTime = sysTimer::GetTicks();
	}

	void EndCompress(const unsigned uncompressedSize, const unsigned compressedSize, const bool success)
	{
		totalCompressTime += (sysTimer::GetTicks() - startTime);

		if(success && (compressedSize < uncompressedSize))
		{
			totalCompressiblePackets++;
			totalBytesCompressed += compressedSize;
		}
		else
		{
			totalIncompressiblePackets++;

			// if compressed size is no better, we send the uncompressed packet
			totalBytesCompressed += uncompressedSize;
		}

		totalBytesUncompressed += uncompressedSize;

		totalErrors += success ? 0 : 1;
	}

	void StartDecompress()
	{
		startTimeDecompress = sysTimer::GetTicks();
	}

	void EndDecompress()
	{
		totalDecompressTime += (sysTimer::GetTicks() - startTimeDecompress);
	}

	void PrintStats()
	{
		if(totalBytesUncompressed == 0)
		{
			return;
		}

		int totalPackets = totalCompressiblePackets + totalIncompressiblePackets;
		float compressTime = sysTimer::GetTicksToMicroseconds() * totalCompressTime;
		float compressTimePerPacket = (totalPackets > 0) ? (compressTime / 1000.0f / totalPackets) : 0;
		float decompressTime = sysTimer::GetTicksToMicroseconds() * totalDecompressTime;
		float decompressTimePerPacket = (totalPackets > 0) ? (decompressTime / 1000.0f / totalPackets) : 0;
		float compressionRatio = (float)totalBytesCompressed / (float)totalBytesUncompressed;
		float percentCompressiblePackets = ((float)totalCompressiblePackets / (float)totalPackets) * 100.0f;

		netDebug("compression ratio: %.2f%%, "
				 "avg compression time per packet: (%.4f ms), "
				 "avg decompression time per packet: (%.4f ms), "
				 "total packets: %d, "
				 "total compressible packets: %d, "
				 "total incompressible packets: %d, "
				 "percent compressible packets: %f%%, "
				 "total errs: %d, "
				 "total bytes uncompressed: %d, "
				 "total bytes compressed: %d, "
				 "total bytes saved: %d, "
				 "total compression time: (%.2f ms), "
				 "total decompression time: (%.2f ms)",
				 compressionRatio * 100.0f,
				 compressTimePerPacket,
				 decompressTimePerPacket,
				 totalPackets,
				 totalCompressiblePackets,
				 totalIncompressiblePackets,
				 percentCompressiblePackets,
				 totalErrors,
				 totalBytesUncompressed,
				 totalBytesCompressed,
				 totalBytesUncompressed - totalBytesCompressed,
				 compressTime / 1000.0f,
				 decompressTime / 1000.0f);
	}
};

static netCompressionProfiler sm_Profiler;
#endif

//////////////////////////////////////////////////////////////////////////
// netCompressionState
//////////////////////////////////////////////////////////////////////////
#if NET_USE_OODLE
class netCompressionState
{
public:
	netCompressionState(sysMemAllocator* allocator)
	: m_Allocator(allocator)
	, dictionary(NULL)
	, shared(NULL)
	, state(NULL)
	{
		Clear();
	}

	void Free(void* ptr) const
	{
		if(ptr)
		{
			if(m_Allocator)
			{
				if(netVerify(m_Allocator->IsValidPointer(ptr)))
				{
					m_Allocator->Free(ptr);
				}
			}
			else
			{
				delete[] (u8*)ptr;
			}
		}
	}

	void Clear()
	{
		if(dictionary)
		{
			Free(dictionary);
			dictionary = NULL;
		}

		if(shared)
		{
			Free(shared);
			shared = NULL;
		}

		if(state)
		{
			Free(state);
			state = NULL;
		}
	}

	~netCompressionState()
	{
		Clear();
	}
	
	sysMemAllocator* m_Allocator;
	void* dictionary;
	OodleNetwork1UDP_State* state;
	OodleNetwork1_Shared* shared;
};
#endif

//////////////////////////////////////////////////////////////////////////
// netCompression
//////////////////////////////////////////////////////////////////////////
netCompression::netCompression()
: m_Allocator(NULL)
, m_State(NULL)
#if NET_COMPRESSION_TRAINING
, m_Trainer(NULL)
#endif
, m_TotalBytesCompressed(0)
, m_TotalBytesUncompressed(0)
, m_Initialized(false)
{

}

bool
netCompression::Init(sysMemAllocator* allocator, netConnectionManager* cxnMgr, const char* dictionaryFilename)
{
	rtry
	{
		rverify(!m_Initialized, catchall,);
		rverify(cxnMgr, catchall,);

		m_Allocator = allocator;

#if NET_COMPRESSION_TRAINING
		m_Trainer = (netCompressionTrainer*)Allocate(sizeof(netCompressionTrainer));
		if(m_Trainer)
		{
			new(m_Trainer) netCompressionTrainer;
			m_Trainer->Init(cxnMgr);
		}
#endif

#if NET_USE_OODLE
		m_State = LoadDictionary(dictionaryFilename);
#else
		(void)dictionaryFilename;
#endif

		m_Initialized = true;

		return true;
	}
	rcatchall
	{
		Shutdown();
		return false;
	}
}

void
netCompression::Shutdown()
{
#if	NET_COMPRESSION_TRAINING
	if(m_Trainer)
	{
		m_Trainer->~netCompressionTrainer();
		Free(m_Trainer);
		m_Trainer = NULL;
	}
#endif
	
#if NET_USE_OODLE
	if(m_State)
	{
		m_State->~netCompressionState();
		Free(m_State);
 		m_State = NULL;
	}
#endif

	m_Initialized = false;
}

void netCompression::Update()
{
#if NET_COMPRESSION_TRAINING
	if(m_Trainer)
	{
		m_Trainer->Update();
	}
#endif

#if NET_COMPRESSION_PROFILING
	sm_Profiler.Update();
#endif
}

bool
netCompression::IsInitialized() const
{
	return m_Initialized;
}

void*
netCompression::Allocate(const unsigned numBytes) const
{
    if(m_Allocator)
    {
#if __FINAL
		const char* fileName = NULL;
		int lineNum = 0;
#else
		const char* fileName = __FILE__;
		int lineNum = __LINE__;
#endif

		return m_Allocator->TryLoggedAllocate(numBytes, sizeof(void*), 0, fileName, lineNum);
    }
    else
    {
        return rage_new u8[numBytes];
    }
}

void
netCompression::Free(void* ptr) const
{
    if(ptr)
    {
        if(m_Allocator)
        {
			if(netVerify(m_Allocator->IsValidPointer(ptr)))
			{
				m_Allocator->Free(ptr);
			}
        }
        else
        {
            delete[] (u8*)ptr;
        }
    }
}

#if NET_USE_OODLE

struct netCompressionDictionaryHeader
{
#define ON1_MAGIC 0x11235801
	u32 magic;
	u32 compressor;
	u32 ht_bits;
	u32 dic_size;
	u32 oodle_major_version;
	u32 dic_complen;
	u32 statecompacted_size;
	u32 statecompacted_complen;
};

bool
netCompression::LoadDictionaryHeader(netCompressionDictionaryHeader* pHeader, const void* from_memory)
{
	rtry
	{
		const u32 * from_ptr = (const u32 *)from_memory;
		pHeader->magic = *(from_ptr);
		from_ptr++;
		pHeader->compressor = *(from_ptr);
		from_ptr++;
		pHeader->ht_bits = *(from_ptr);
		from_ptr++;
		pHeader->dic_size = *(from_ptr);
		from_ptr++;
		pHeader->oodle_major_version = *(from_ptr);
		from_ptr++;
		pHeader->dic_complen = *(from_ptr);
		from_ptr++;
		pHeader->statecompacted_size = *(from_ptr);
		from_ptr++;
		pHeader->statecompacted_complen = *(from_ptr);
		from_ptr++;
		rverify((((u8 *)from_ptr) - ((u8 *)from_memory)) == sizeof(netCompressionDictionaryHeader), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netCompression::LoadDictionaryFromFileData(netCompressionState* pCompressor, const void* fileData, int fileSize, int& memUsed)
{
	rtry
	{
		netCompressionDictionaryHeader header;
		rverify(LoadDictionaryHeader(&header, fileData), catchall, );
		rverify(header.magic == ON1_MAGIC, catchall, );

		/*
			file contents:
			header
			compressed dictionary
			compressed state (compacted)
		*/

		/*
			Steps:
			1. read header
			2. read compressed dictionary
			3. decompress dictionary into memory
			4. deallocate compressed dictionary
			5. read compressed compacted state
			6. decompress compacted state
			7. deallocate compressed compacted state
			8. uncompact compacted state
			9. deallocate decompressed compacted state
			10. allocate shared data (hashtable?)
		*/

		S32 ht_bits = header.ht_bits;
		int dic_size = header.dic_size;

		int dic_complen = header.dic_complen;
		int statecompacted_size = header.statecompacted_size;
		int statecompacted_complen = header.statecompacted_complen;

		rverify(dic_size >= dic_complen, catchall, );
		rverify(statecompacted_size >= statecompacted_complen, catchall, );
		rverify(statecompacted_size > 0 && statecompacted_size < OodleNetwork1UDP_StateCompacted_MaxSize(), catchall, );
		rverify(fileSize == (S64)sizeof(netCompressionDictionaryHeader) + dic_complen + statecompacted_complen, catchall, );

		// decompress dictionary and statecompacted
		pCompressor->dictionary = Allocate(dic_size);
		rverify(pCompressor->dictionary, catchall, );
		memUsed += dic_size;

		const void * dic_comp_ptr = (const u8 *)(fileData) + sizeof(netCompressionDictionaryHeader);

		if(header.compressor == (U32)OodleLZ_Compressor_Invalid)
		{
			rverify(dic_complen == dic_size, catchall, );
			memcpy(pCompressor->dictionary, dic_comp_ptr, dic_size);
		}
		else
		{
			int decomp_dic_size = (int)OodleLZ_Decompress(dic_comp_ptr, dic_complen, pCompressor->dictionary, dic_size);
			rverify(decomp_dic_size == dic_size, catchall, );
		}

		// using rage_new for temporary scratch memory (deallocated below)
		OodleNetwork1UDP_StateCompacted* compacted = (OodleNetwork1UDP_StateCompacted*) rage_new u8[statecompacted_size];
		rverify(compacted, catchall, );

		void * statecompacted_comp_ptr = (u8 *)dic_comp_ptr + dic_complen;

		if(header.compressor == (U32)OodleLZ_Compressor_Invalid)
		{
			rverify(statecompacted_size == statecompacted_complen, catchall, );
			memcpy(compacted, statecompacted_comp_ptr, statecompacted_size);
		}
		else
		{
			int decomp_statecompacted_size = (int)OodleLZ_Decompress(statecompacted_comp_ptr, statecompacted_complen, compacted, statecompacted_size);
			rverify(decomp_statecompacted_size == statecompacted_size, catchall, );
		}

		// uncompact the "Compacted" state into a usable state
		int state_size = (int)OodleNetwork1UDP_State_Size();
		pCompressor->state = (OodleNetwork1UDP_State *) Allocate(state_size);
		rverify(pCompressor->state, catchall, );
		memUsed += state_size;

		OodleNetwork1UDP_State_Uncompact(pCompressor->state, compacted);

		// deallocate scratch memory
		delete[] (u8*)compacted;
		compacted = NULL;

		// fill out shared from the dictionary
		int shared_size = (int)OodleNetwork1_Shared_Size(header.ht_bits);
		pCompressor->shared = (OodleNetwork1_Shared *) Allocate(shared_size);
		rverify(pCompressor->shared, catchall, );
		memUsed += shared_size;

		OodleNetwork1_Shared_SetWindow(pCompressor->shared, ht_bits, pCompressor->dictionary, (S32)dic_size);

		return true;
	}
	rcatchall
	{
		return false;
	}	
}

netCompressionState*
netCompression::LoadDictionary(const char* fileName)
{
	u8* fileData = NULL;
	rtry
	{
		rcheck(NET_USE_OODLE, catchall, );
		rcheck(fileName && (fileName[0] != '\0'), catchall, );

#if NET_COMPRESSION_PROFILING
		utimer_t startTime = sysTimer::GetTicks();
#endif

		fiStream* m_Stream = fiStream::Open(fileName);
		rcheck(m_Stream, catchall, );

		int fileSize = m_Stream->Size();
		rverify(fileSize > 0, catchall, );

		netDebug2("Loading dictionary with compressed size of %d bytes.", fileSize);

		// using rage_new for temporary scratch memory (deallocated below)
		fileData = (u8*)rage_new u8[fileSize + 1];
		rverify(fileData, catchall, );
		rverify(m_Stream->Read(fileData, (int)fileSize) == fileSize, catchall, );
		m_Stream->Close();

		m_State = (netCompressionState*)Allocate(sizeof(netCompressionState));
		rverify(m_State, catchall, );
		new(m_State) netCompressionState(m_Allocator);

		int memUsed = 0;
		rcheck(LoadDictionaryFromFileData(m_State, fileData, fileSize, memUsed), catchall, );

#if NET_COMPRESSION_PROFILING
		utimer_t elapsed = sysTimer::GetTicks() - startTime;
		netDebug2("Loading the dictionary from '%s' took %.2f ms. Memory used: %.2f MB.", fileName, (sysTimer::GetTicksToMicroseconds() * elapsed) / 1000.0f, (float)memUsed / 1024.0f / 1024.0f);
#endif
	}
	rcatchall
	{
		if(m_State)
		{
			m_State->~netCompressionState();
			Free(m_State);
			m_State = NULL;
		}

		netDebug("Failed to load the dictionary from '%s'. Network packets will be sent uncompressed.", fileName);
	}

	if(fileData)
	{
		// free scratch memory
		delete[] fileData;
		fileData = NULL;
	}

	return m_State;
}

static const int c_packetsize_modbits = 2;
static const int c_packetsize_immediate = 256 - (1 << c_packetsize_modbits); // = 252 when modbits = 2
static const int c_packetsize_max = c_packetsize_immediate + (1 << (c_packetsize_modbits + 8)); // = 1276 when modbits = 2
CompileTimeAssert(c_packetsize_max >= netSocket::MAX_BUFFER_SIZE);
#endif

void 
netCompression::Report(const unsigned uncompressedSize, const unsigned compressedSize, const bool success) const
{
	if(success && (compressedSize < uncompressedSize))
	{
		m_TotalBytesCompressed += compressedSize;
	}
	else
	{
		// if compressed size is no better, we send the uncompressed packet
		m_TotalBytesCompressed += uncompressedSize;
	}

	m_TotalBytesUncompressed += uncompressedSize;
}

bool
netCompression::Compress(const u8* source, const unsigned sizeOfSource, u8* dest, unsigned& sizeOfDest) const
{
#if NET_COMPRESSION_TRAINING
	if(m_Trainer)
	{
		m_Trainer->RecordOutboundPacket(source, sizeOfSource);
	}
#endif

#if NET_COMPRESSION_PROFILING
	sm_Profiler.StartCompress();
#endif

	bool success = false;

	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(source != NULL, catchall, );
		rverify(dest != NULL, catchall, );
		rverify(sizeOfSource > 0, catchall, );
		rverify(sizeOfDest >= sizeOfSource, catchall, );

#if NET_USE_OODLE
		rcheck(m_State, catchall, );

		// from the Oodle 2.3 docs:
		//     OodleNetwork1UDP_Encode will return a cmpSize <= sizeOfSource, because OodleNetwork1 won't 
		//     send packets that expand under compression (it just sends them uncompressed) - however it
		//     may write further than that during the compression attempt. The dest buffer must be
		//     allocated to at least OodleNetwork1_CompressedBufferSizeNeeded bytes.
		//
		// Note that if this requirement isn't met, we avoid compression and send the packet uncompresssed.
		SINTa requiredDestAllocSize = OodleNetwork1_CompressedBufferSizeNeeded(sizeOfSource);
		rverify(sizeOfDest >= requiredDestAllocSize, catchall,
				netWarning("netCompression::Compress: dest buffer too small. sizeOfSource: %d, sizeOfDest: %u",
						sizeOfSource, sizeOfDest));

		// compress
		SINTa cmpSize = OodleNetwork1UDP_Encode(m_State->state, m_State->shared, source, sizeOfSource, dest);

		rverify(cmpSize > 0, catchall,);
		rcheck(cmpSize < sizeOfSource, catchall,);

		// encode the delta between uncompressed vs compressed size
		unsigned delta = sizeOfSource - (unsigned)cmpSize;
		rverify(delta < c_packetsize_max, catchall, );

		u8* destPtr = (u8*)dest + cmpSize;
		unsigned numSizeBytes;

		if(delta < c_packetsize_immediate)
		{
			numSizeBytes = 1;
			rverify(cmpSize <= (sizeOfDest - numSizeBytes), catchall, netError("not enough room for delta"));

			*destPtr = (u8)delta;
		}
		else
		{
			numSizeBytes = 2;
			rverify(cmpSize <= (sizeOfDest - numSizeBytes), catchall, netError("not enough room for delta"));

			delta -= c_packetsize_immediate;
			u8 lo = (u8) delta;
			u8 hi = (u8)(delta >> 8);
			rverify(hi < (256 - c_packetsize_immediate), catchall, );
			*destPtr++ = lo;
			*destPtr = c_packetsize_immediate + hi;
		}

		sizeOfDest = (unsigned)cmpSize + numSizeBytes;
		rcheck(sizeOfDest < sizeOfSource, catchall,);

		success = true;
#else
		const unsigned len = datCompress(dest, sizeOfDest, source, sizeOfSource);
		rcheck((len > 0) && (len < sizeOfSource), catchall, );
		sizeOfDest = len;
		success = true;
#endif
	}
	rcatchall
	{
		success = false;
	}

#if NET_COMPRESSION_PROFILING
	sm_Profiler.EndCompress(sizeOfSource, sizeOfDest, success);
#endif

	Report(sizeOfSource, sizeOfDest, success);

	return success;
}

bool
netCompression::Decompress(const u8* source, const unsigned sizeOfSource, u8* dest, unsigned& sizeOfDest) const
{
#if NET_COMPRESSION_PROFILING
	sm_Profiler.StartDecompress();
#endif

	bool success = false;

	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(source != NULL, catchall, );
		rverify(dest != NULL, catchall, );
		rverify(sizeOfSource > 0, catchall, );
		rverify(sizeOfDest >= sizeOfSource, catchall, );

#if NET_USE_OODLE
		rcheck(m_State, catchall, );

		// get the uncompressed packet size
		const u8* srcPtr = (const u8*)source + sizeOfSource - 1;

		unsigned numSizeBytes;
		int decompressedSize;

		u8 first = *srcPtr;

		if(first < c_packetsize_immediate)
		{
			numSizeBytes = 1;

			decompressedSize = first + sizeOfSource - numSizeBytes;
		}
		else
		{
			numSizeBytes = 2;

			int hi = first - c_packetsize_immediate;
			int lo = *(srcPtr - 1);
			decompressedSize = (hi << 8) + lo + c_packetsize_immediate + sizeOfSource - numSizeBytes;
		}

		// decompress
		int compressedSize = sizeOfSource - numSizeBytes;
		rverify(decompressedSize >= compressedSize, catchall, );
		rverify(OodleNetwork1UDP_Decode(m_State->state, m_State->shared, source, compressedSize, dest, decompressedSize), catchall,);

		sizeOfDest = decompressedSize;
		success = true;
#else
		const unsigned len = datDecompress(dest, sizeOfDest, source, sizeOfSource);
		rcheck(len > 0, catchall, );
		sizeOfDest = len;
		success = true;
#endif
	}
	rcatchall
	{
		sizeOfDest = 0;
		success = false;
	}

#if NET_COMPRESSION_PROFILING
	sm_Profiler.EndDecompress();
#endif

#if NET_COMPRESSION_TRAINING
	if(m_Trainer && success)
	{
		// this is not a great place to collect inbound packets because the connection manager will only send
		// a packet to Decompress() if the remote peer was able to compress it. We won't be collecting any
		// incompressible packets here. Packet training currently only uses outbound packets. We're only
		// collecting some inbound packets to test different training streams.
		m_Trainer->RecordInboundPacket(dest, sizeOfDest);
	}
#endif

	return success;
}

}   //namespace rage
