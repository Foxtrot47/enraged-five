// 
// net/connectionmanager.cpp 
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved. 
// 

#include "connectionmanager.h"
#include "message.h"
#include "netdiag.h"
#include "netfuncprofiler.h"
#include "resolver.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/netsocketmanager.h"
#include "net/timesync.h"
#include "profile/rocky.h"
#include "rline/rlpresence.h"
#include "rline/rltelemetry.h"
#include "string/string.h"
#include "string/stringutil.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/stack.h"
#include "system/timer.h"

#if RSG_BANK
#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "net/task.h"
#include "rline/rltask.h"
#endif

//CAUTION!!!
//
//When doing math on sequence numbers be sure to use appropriate casting.
//
//netSequence a = 0;
//netSequence b = 65535;
//bool areEqual = (b == (a - 1));               //INCORRECT!
//
//bool areEqual2 = (b == netSequence(a - 1));   //CORRECT!
//

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, cxnmgr)
#undef __rage_channel
#define __rage_channel ragenet_cxnmgr

//extern bool g_RequestSysutilExit;

#define NET_WORKER_THREAD_STACK_SIZE 48*1024

// endpoints should only use plaintext addresses after tunneling completes
#define NET_ASSERT_PLAINTEXT_ADDRS(addr, relayAddr)																			\
	netAssert(!addr.IsRelayServerAddr() || (!addr.GetRelayToken().IsValid() && addr.GetTargetAddress().IsValid()));				\
	netAssert(!relayAddr.IsValid() || (!relayAddr.GetRelayToken().IsValid() && relayAddr.GetTargetAddress().IsValid()));

PARAM(netDisableAcquireCritSecForFullMainUpdate, "When updating main, disable acquiring connection manager critical section for full update");
PARAM(netBadQosThresholdMs, "If a connection quality is poor for this amount of time, we start reporting a bad connection quality");

#if !__FINAL
PARAM(netEndpointInactivityMs, "Set default inactivity timeout for endpoints");
PARAM(netEndpointCxnlessMs, "Set default connection-less timeout for endpoints");
PARAM(netEndpointRemovalDisabled, "Set whether endpoints are removed on inactivity");
#endif

#if !__NO_OUTPUT
PARAM(netCxnLockWaitThreshold, "Sets threshold at which we'll log that we waited for the connection manager lock");
PARAM(netCxnLockHoldThreshold, "Sets threshold at which we'll log that we held onto the connection manager lock");
PARAM(netCxnLockLongWaitThreshold, "Sets threshold at which we log 'True' for a *long* wait - this is just for searching purposes");
PARAM(netCxnLockLongHoldThreshold, "Sets threshold at which we log 'True' for a *long* hold - this is just for searching purposes");
PARAM(netCxnLockOnlyOutsideLock, "Sets whether we only consider the outside lock");
PARAM(netCxnLockPrintMainStackTrace, "Sets whether we print our main thread stack for a long hold");
PARAM(netCxnEstablishmentStackTraces, "Enables stack traces for connection establishment functions");
#endif

struct MsgCxnRelayAddrChanged
{
	NET_MESSAGE_DECL(MsgCxnRelayAddrChanged, MSG_CXN_RELAY_ADDR_CHANGED);

	MsgCxnRelayAddrChanged() : m_bNoReassign(false) {}

	void Reset(const netAddress& relayAddr, const netAddress& prevRelayAddr)
	{
		m_RelayAddr = relayAddr;
		m_PrevRelayAddr = prevRelayAddr;
		m_bNoReassign = false;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerUser(msg.m_RelayAddr)
			&& bb.SerUser(msg.m_PrevRelayAddr)
			&& bb.SerBool(msg.m_bNoReassign);
	}

	netAddress m_RelayAddr;
	netAddress m_PrevRelayAddr;
	bool m_bNoReassign; 
};

struct MsgCxnRequestRemoteTimeout
{
	NET_MESSAGE_DECL(MsgCxnRequestRemoteTimeout, MSG_CXN_REQUEST_REMOTE_TIMEOUT);

	MsgCxnRequestRemoteTimeout()
	: m_ChannelId(NET_INVALID_CHANNEL_ID)
	, m_TimeoutReason(netConnection::CXN_TIMEOUT_REASON_NONE)
	{
	}

	MsgCxnRequestRemoteTimeout(const ChannelId channelId, const netConnection::TimeoutReason timeoutReason)
	: m_ChannelId(channelId)
	, m_TimeoutReason(timeoutReason)
	{
	}

	static const unsigned SIZEOF_CHANNELID = datBitsNeeded<NET_MAX_CHANNELS>::COUNT;

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerUns(msg.m_ChannelId, SIZEOF_CHANNELID) &&
			   bb.SerInt(msg.m_TimeoutReason, 1 + datBitsNeeded<netConnection::CXN_TIMEOUT_NUM_REASONS>::COUNT);
	}

	ChannelId m_ChannelId;
	netConnection::TimeoutReason m_TimeoutReason;
};

NET_MESSAGE_IMPL(MsgCxnRelayAddrChanged);
NET_MESSAGE_IMPL(MsgCxnRequestRemoteTimeout);

#if !__NO_OUTPUT
///////////////////////////////////////////////////////////////////////////////
// netConnectionManager::netCriticalSectionToken
///////////////////////////////////////////////////////////////////////////////

// tracking variables for the threads that access our lock
static size_t s_SendThreadId = 0;
static size_t s_MainThreadId = 0;

// tracking the thread that currently holds the lock
static const size_t UNASSIGNED_LOCK_ID = 0;
static size_t s_LockThreadId = UNASSIGNED_LOCK_ID;
static unsigned s_LockThreadAcquired = 0;

// whether we're currently updating netConnectionManager::Update
static bool s_InMainUpdate = false;
static bool s_PrintingStackTrace = false;

// settings
static unsigned s_CxnLockWaitThreshold = netConnectionManager::DEFAULT_CXN_LOCK_WAIT_THRESHOLD;
static unsigned s_CxnLockHoldThreshold = netConnectionManager::DEFAULT_CXN_LOCK_HOLD_THRESHOLD;
static unsigned s_CxnLockLongWaitThreshold = netConnectionManager::DEFAULT_CXN_LOCK_LONG_WAIT_THRESHOLD;
static unsigned s_CxnLockLongHoldThreshold = netConnectionManager::DEFAULT_CXN_LOCK_LONG_HOLD_THRESHOLD;
static bool s_CxnLockOnlyOutsideLock = netConnectionManager::DEFAULT_CXN_LOCK_ONLY_OUTSIDE_LOCK;
static bool s_CxnLockPrintMainStackTrace = netConnectionManager::DEFAULT_CXN_LOCK_PRINT_MAIN_STACK_TRACE;

const char* GetThreadName(const size_t threadId)
{
	if(s_SendThreadId > 0 && threadId == s_SendThreadId)
		return "SendThread";
	else if(s_MainThreadId > 0 && threadId == s_MainThreadId)
		return "MainThread";
	else if(threadId == 0)
		return "Invalid";
	else
		return "Unknown";
}

netConnectionManager::netCriticalSectionToken::netCriticalSectionToken()
	: m_LockCount(0)
	, m_WaitCount(0)
	, m_WaitThrash(0)
	, m_LockThrash(0)
	, m_UnlockThrash(0)
{
}

void
netConnectionManager::netCriticalSectionToken::Lock()
{
	// capture variables
	unsigned lockCountOnEntry = m_LockCount;
	unsigned lockThrashOnEntry = m_LockThrash;
	unsigned unlockThrashOnEntry = m_UnlockThrash;
	unsigned waitCountOnEntry = m_WaitCount;
	size_t lockThreadId = (size_t)(s_LockThreadId);

	++m_WaitCount;
	++m_WaitThrash;

	// capture after so that our own wait isn't counted
	unsigned waitThrashOnEntry = m_WaitThrash;

	// capture time before and after our lock
	unsigned enterTime = sysTimer::GetSystemMsTime();
	{
		m_Token.Lock();
	}
	unsigned lockTime = sysTimer::GetSystemMsTime();

	// decrease waits, increase locks
	--m_WaitCount;
	++m_LockCount;
	
	// only when this is newly acquired
	if(m_LockCount == 1 || !s_CxnLockOnlyOutsideLock)
	{
		s_LockThreadId = (size_t)(sysIpcGetCurrentThreadId());
		s_LockThreadAcquired = lockTime;

		// only when above threshold and something held the lock when we arrived 
		unsigned waitTime = s_LockThreadAcquired - enterTime;
		if((waitTime >= s_CxnLockWaitThreshold) && ((lockCountOnEntry > 0) || (s_CxnLockWaitThreshold == 0) || (waitTime >= s_CxnLockLongWaitThreshold)))
		{
			netDebug3("CxnMgrLock :: Lock acquired by %s:[%" SIZETFMT "x] - Waited: %ums, LongWait: %s, WaitCount: %u, WaitThrash: %u, WaitCountOnEntry: %u, WaitedOn: [%" SIZETFMT "x], LockCount: %u, LockCountOnEntry: %u, LockThrash: %u, UnlockThrash: %u", 
					  GetThreadName(s_LockThreadId), 
					  s_LockThreadId,
					  waitTime, 
					  (waitTime >= s_CxnLockLongWaitThreshold) ? "True" : "False",
					  m_WaitCount, 
					  m_WaitThrash - waitThrashOnEntry,
					  waitCountOnEntry,
					  lockThreadId,
					  m_LockCount,
					  lockCountOnEntry,
					  m_LockThrash - lockThrashOnEntry,
					  m_UnlockThrash - unlockThrashOnEntry);
		}
	}

	// increment after so that our own lock isn't counted
	++m_LockThrash;
}

void
netConnectionManager::netCriticalSectionToken::Unlock()
{
	netAssert(m_LockCount);
	--m_LockCount;
	++m_UnlockThrash;

	// only when all locks are released
	if(m_LockCount == 0 || !s_CxnLockOnlyOutsideLock)
	{
		unsigned holdTime = sysTimer::GetSystemMsTime() - s_LockThreadAcquired;
		if(holdTime >= s_CxnLockHoldThreshold)
		{
			netDebug3("CxnMgrLock :: Lock released by %s:[%" SIZETFMT "x] - Held: %ums, LongHold: %s, WaitCount: %u, LockCount: %u", 
					  GetThreadName(s_LockThreadId), 
					  (size_t)(s_LockThreadId), 
					  holdTime, 
					  (holdTime >= s_CxnLockLongHoldThreshold) ? "True" : "False",
					  m_WaitCount, 
					  m_LockCount);

			// optionally stack dump from main thread
			if((s_LockThreadId == s_MainThreadId) && (holdTime > 1) && !s_InMainUpdate && s_CxnLockPrintMainStackTrace && !s_PrintingStackTrace)
			{
				s_PrintingStackTrace = true;
				netDebug3("CxnMgrLock :: Dumping MainThread stack");
				sysStack::PrintStackTrace();
				s_PrintingStackTrace = false;
			}
		}

		// we only release when we release the lock fully
		if(m_LockCount == 0)
		{
			s_LockThreadId = UNASSIGNED_LOCK_ID;
		}
	}
	m_Token.Unlock();
}

bool
netConnectionManager::netCriticalSectionToken::IsLocked() const
{
	return (m_LockCount > 0);
}
#endif  //!__NO_OUTPUT

///////////////////////////////////////////////////////////////////////////////
//  Cxn
///////////////////////////////////////////////////////////////////////////////
unsigned Cxn::sm_PendingCloseMaxTime = Cxn::DEFAULT_PENDING_CLOSE_MAX_TIME;

Cxn::Cxn()
    : m_Refs(0)
    , m_Status(0)
    , m_CxnClosedEvent(NET_INVALID_CXN_ID, NET_INVALID_ENDPOINT_ID, NET_INVALID_CHANNEL_ID, NULL)
    , m_OutOfMemEvent(NET_INVALID_CXN_ID, NET_INVALID_ENDPOINT_ID, NET_INVALID_CHANNEL_ID, false, NULL)
    , m_PendingClose(false)
	, m_PendingCloseTime(0)
{
}

void
Cxn::SetPendingCloseMaxTime(const unsigned maxTime)
{
	if(sm_PendingCloseMaxTime != maxTime)
	{
		netDebug("SetPendingCloseMaxTime - Setting to %u", maxTime);
		sm_PendingCloseMaxTime = maxTime;
	}
}

bool
Cxn::Init(netConnectionManager* cxnMgr,
          netEventQueue<netEvent>* eventQ,
          sysMemAllocator* allocator)
{
    netAssert(NULL == m_EpOwner);
    netAssert(0 == m_Refs);

    m_PendingClose = false;
	m_PendingCloseTime = 0;
    return this->netConnection::Init(cxnMgr, eventQ, allocator);
}

void
Cxn::Shutdown()
{
    netAssert(NULL == m_EpOwner);
    netAssert(0 == m_Refs);
    this->netConnection::Shutdown();
}

bool
Cxn::Open(netEndpoint* ep, const int cxnId, const unsigned channelId)
{
    netAssert(NULL == m_EpOwner);
    netAssert(0 == m_Refs);
    netAssert(!m_Status);
    netAssert(!m_PendingClose);
	netAssert(m_PendingCloseTime == 0);

	if(this->netConnection::Open(ep, cxnId, channelId))
    {
		netDebug("%08x.%d[" NET_ADDR_FMT "]: Open", this->GetId(), this->GetChannelId(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
		return true;
    }
	else
	{
		netWarning("%08x.%d[" NET_ADDR_FMT "]: Open Failed", this->GetId(), this->GetChannelId(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
	}

    return false;
}

void
Cxn::Close()
{
    if(this->IsOpen())
    {
		netDebug("%08x.%d[" NET_ADDR_FMT "]: Close", this->GetId(), this->GetChannelId(), m_EpOwner ? NET_ADDR_FOR_PRINTF(m_EpOwner->GetAddress()) : "");
		netAssert(0 == m_Refs);
        m_PendingClose = true;
		m_PendingCloseTime = sysTimer::GetSystemMsTime();
    }
}

void
Cxn::Destroy()
{
    netAssert(NULL == m_EpOwner);
    netAssert(0 == m_Refs);
    netAssert(!m_Status);

	netDebug("%08x.%d: Destroy", this->GetId(), this->GetChannelId());

    m_PendingClose = false;
	m_PendingCloseTime = 0;
    this->netConnection::Close();
}

bool
Cxn::CanDestroy(OUTPUT_ONLY(bool addLogging)) const
{
    bool canDestroyGracefully = false;
    
    if(!this->NeedToAck())
    {
        if(!this->GetUnAckedCount())
        {
			canDestroyGracefully = true;
        }
        //Sometimes for debug purposes we set policies so connections
        //never time out.  When a connection has been closed, however,
        //and it still has pending un-ACKed messages, we want to make sure
        //it eventually gets destroyed.
        else if(this->IsPendingClose()
                && (0 == this->GetPolicies().m_TimeoutInterval)
                && this->GetOldestResendCount() > netConnectionPolicies::PENDING_CLOSE_MAX_SEND_COUNT)
        {
			canDestroyGracefully = true;
        }
    }

	bool canDestroy = canDestroyGracefully || (this->GetTimeoutCount() > 0) || this->IsOutOfMemory();
	if(!canDestroy)
	{
		canDestroy = this->IsPendingCloseTimeExpired();
	}

#if !__NO_OUTPUT
	if(addLogging)
	{
		netDebug("%08x.%d[" NET_ADDR_FMT "]: CanDestroy - CanDestroy: %s, Gracefully: %s, NeedToAck: %s, GetUnAckedCount: %u, PendingClose: %s, TimedOut: %s, OutOfMem: %s, PendingCloseTimeExpired: %s", 
				 this->GetId(), 
				 this->GetChannelId(), 
				 NET_ADDR_FOR_PRINTF(m_EpOwner->GetAddress()),
				 canDestroy ? "True" : "False",
				 canDestroyGracefully ? "True" : "False",
				 this->NeedToAck() ? "True" : "False",
				 this->GetUnAckedCount(),
				 this->IsPendingClose() ? "True" : "False",
				 (this->GetTimeoutCount() > 0) ? "True" : "False",
				 this->IsOutOfMemory() ? "True" : "False",
				 this->IsPendingCloseTimeExpired() ? "True" : "False"
				 );
	}
#endif

    return canDestroy;
}

bool
Cxn::IsClosed() const
{
    return this->GetState() == STATE_CLOSED;
}

bool
Cxn::IsPendingOpen() const
{
    return this->GetState() < STATE_ESTABLISHED;
}

bool
Cxn::IsOpen() const
{
    return !m_PendingClose && this->GetState() == STATE_ESTABLISHED;
}

bool
Cxn::IsPendingClose() const
{
    return m_PendingClose;
}

bool
Cxn::IsPendingCloseTimeExpired() const
{
	return (sm_PendingCloseMaxTime > 0) && (sysTimer::GetSystemMsTime() - m_PendingCloseTime) > sm_PendingCloseMaxTime;
}

///////////////////////////////////////////////////////////////////////////////
//  netEndpoint
///////////////////////////////////////////////////////////////////////////////

unsigned netEndpoint::sm_InactivityTimeoutMs = netEndpoint::DEFAULT_INACTIVITY_TIMEOUT_MS;
unsigned netEndpoint::sm_CxnlessTimeoutMs = netEndpoint::DEFAULT_CXNLESS_TIMEOUT_MS;
unsigned netEndpoint::sm_BadQosThresholdMs = netEndpoint::DEFAULT_BAD_QOS_THRESHOLD_MS;

netEndpoint::netEndpoint()
: m_TunnelRqst(NULL)
, m_Status(NULL)
, m_LastReceiveTime(0)
, m_LastAddressAssignmentTime(0)
, m_OutOfMemEvent(NET_INVALID_CXN_ID, NET_INVALID_ENDPOINT_ID, NET_INVALID_CHANNEL_ID, false, NULL)
, m_Id(NET_INVALID_ENDPOINT_ID)
, m_EventQ(nullptr)
{
	for(int i = 0; i < NET_MAX_CHANNELS; ++i)
	{
		m_Cxns[i] = NULL;
	}

    this->Clear();

	m_Id = NextId();

#if NET_CXNMGR_COLLECT_STATS
	m_Stats.m_EpOwner = this;
#endif
}

netEndpoint::~netEndpoint()
{
    this->Clear();
}

unsigned
netEndpoint::NextId() const
{
	static unsigned s_NextId = 0;

	unsigned id = sysInterlockedIncrement(&s_NextId);

	while(!NET_IS_VALID_ENDPOINT_ID(id))
	{
		id = sysInterlockedIncrement(&s_NextId);
	}

	return id;
}

bool
netEndpoint::CanReassignOnRelayPacket() const
{
	return (m_LastAddressAssignmentTime == 0) || sysTimer::HasElapsedIntervalMs(m_LastAddressAssignmentTime, DEFAULT_ADDR_HOLD_TIME_MS);
}

void
netEndpoint::Clear()
{
    this->PurgeQueues();

#if __ASSERT
	for(int i = 0; i < NET_MAX_CHANNELS; ++i)
	{
		netAssert(!m_Cxns[i]);
	}
	netAssert(m_CxnList.empty());
    netAssert(m_CxnlessQ.empty());
    netAssert(!m_TunnelRqst || !m_TunnelRqst->Pending());
	netAssert(!m_MyStatus.Pending());
	netAssert(!m_Status || !m_Status->Pending());
#endif  //__ASSERT

	m_TunnelRqst = NULL;
	m_MyStatus.Reset();
	m_Status = NULL;
	m_RelayAddr.Clear();
	m_PeerAddr.Clear();
	m_ByEndpointIdLink.m_key = NET_INVALID_ENDPOINT_ID;
	m_ByPeerIdLink.m_key.Clear();
	m_ByAddrLink.m_key.Clear();
	m_PackTimeout = 0;
	m_SendInterval = netConnectionManager::DEFAULT_SEND_INTERVAL_MS;
    m_Timeout = sm_InactivityTimeoutMs;
	m_LastTime = 0;
	m_LastReceiveTime = 0;
	m_CreatedTime = 0;
	m_LastAddressAssignmentTime = 0;

	if(m_EventQ && m_OutOfMemEvent.IsQueued())
	{
		// remove the pre-allocated m_OutOfMemEvent from the event queue here, otherwise the event queue
		// will have a pointer to an out-of-scope event, and will crash when accessing it.
		struct Filter
		{
			Filter(const netEvent* e) : m_Event(e) {}

			bool Apply(const netEvent* e)
			{
				bool remove = e == m_Event;

#if !__NO_OUTPUT
				if(remove)
				{
					netDebug("[%d] netEndpoint::Clear :: removing pre-allocated out of memory event", e->GetEndpointId());
				}
#endif
				return remove;
			}

		private:
			const netEvent* m_Event;
		};

		Filter f(&m_OutOfMemEvent);
		netEventQueue<netEvent>::RemoveEventsFilter filter;
		filter.Reset<Filter, &Filter::Apply>(&f);
		m_EventQ->RemoveEvents(filter, true);
	}

	m_EventQ = nullptr;
	m_Id = NET_INVALID_ENDPOINT_ID;
	m_HopCount = 0;

#if NET_CXNMGR_COLLECT_STATS
	m_Stats.Clear();
#endif
}

EndpointId
netEndpoint::GetId() const
{
	// endpoints always have a valid EndpointId, but only active endpoints
	// will be added to the m_EndpointsByEndpointId map.
	netAssert((m_ByEndpointIdLink.m_key == m_Id) ||
			  (m_ByEndpointIdLink.m_key == NET_INVALID_ENDPOINT_ID));
	netAssert(NET_IS_VALID_ENDPOINT_ID(m_Id));

	return m_Id;
}

const netAddress& 
netEndpoint::GetAddress() const
{
    return m_ByAddrLink.m_key;
}

const netPeerAddress& 
netEndpoint::GetPeerAddress() const
{
    return m_PeerAddr;
}

const netPeerId&
netEndpoint::GetPeerId() const
{
	return m_ByPeerIdLink.m_key;
}

u8 netEndpoint::GetHopCount() const
{
	return m_HopCount;
}

void netEndpoint::SetHopCount(const u8 hopCount)
{
	m_HopCount = hopCount;
}

void
netEndpoint::PurgeQueues()
{
    while(!m_CxnlessQ.empty())
    {
        CxnlessBundle* cb = m_CxnlessQ.front();
        m_CxnlessQ.pop_front();

        sysMemAllocator* allocator = cb->m_Allocator;
        cb->m_Bundle->~netBundle();
        allocator->Free(cb->m_Bundle);
        cb->~CxnlessBundle();
        allocator->Free(cb);
    }
}

#if NET_CXNMGR_COLLECT_STATS
netEndpoint::Statistics&
netEndpoint::GetStats()
{
	return m_Stats;
}
#endif

///////////////////////////////////////////////////////////////////////////////
// netConnectionManager::Worker
///////////////////////////////////////////////////////////////////////////////

netConnectionManager::Worker::Worker()
    : m_CxnMgr(0)
    , m_SendWaitThread(0)
    , m_SendSema(0)
    , m_SendThreadHandle(sysIpcThreadIdInvalid)
    , m_SendThreadId(sysIpcCurrentThreadIdInvalid)
    , m_SendDone(true)
{
}

bool
netConnectionManager::Worker::Init(netConnectionManager* cxnMgr,
                                    const int cpuAffinity)
{
    bool success = false;

    rtry
    {
        rverify(!m_CxnMgr,catchall,);

        netAssert(sysIpcThreadIdInvalid == m_SendThreadHandle);
        netAssert(sysIpcCurrentThreadIdInvalid == m_SendThreadId);
        netAssert(!m_SendWaitThread);
        netAssert(!m_SendSema);
        netAssert(m_SendDone);

        m_SendWaitThread = sysIpcCreateSema(0);
        rverify(m_SendWaitThread,catchall,netError("Error creating semaphore"));

        m_SendSema = sysIpcCreateSema(0);
        rverify(m_SendSema,catchall,netError("Error creating semaphore"));

        m_SendDone = false;

        m_CxnMgr = cxnMgr;

        m_SendThreadHandle =
            sysIpcCreateThread(&Worker::Send,
                                this,
                                NET_WORKER_THREAD_STACK_SIZE,
                                PRIO_NORMAL,
                                "[RAGENET] Send",
                                cpuAffinity, 
								"RageNetSend");

        rverify(sysIpcThreadIdInvalid != m_SendThreadHandle,
                catchall,
                netError("Error creating Update thread"));

        //Wait for the thread to start.
        sysIpcWaitSema(m_SendWaitThread);

        success = true;
    }
    rcatchall
    {
        this->Shutdown();
    }

    return success;
}

void
netConnectionManager::Worker::Shutdown()
{
    if(sysIpcThreadIdInvalid != m_SendThreadHandle)
    {
        if(!m_SendDone)
        {
            m_SendDone = true;
            //Wait for the thread to complete.
            sysIpcWaitSema(m_SendWaitThread);
            sysIpcWaitThreadExit(m_SendThreadHandle);
            sysIpcDeleteSema(m_SendWaitThread);
        }
    }

    m_CxnMgr = 0;
    m_SendWaitThread = NULL;
    m_SendThreadHandle = sysIpcThreadIdInvalid;
    m_SendThreadId = sysIpcCurrentThreadIdInvalid;
}

//private:

void
netConnectionManager::Worker::Send(void* p)
{
	PROFILER_THREAD("RageNetSend", 1);

    Worker* worker = (Worker*) p;
    netConnectionManager* cxnMgr = worker->m_CxnMgr;

	//Memory: store the old allocator, and set the allocator to the cxn mgr allocator
	sysMemAllocator& oldAllocator = sysMemAllocator::GetCurrent();
	sysMemAllocator::SetCurrent(*(cxnMgr->GetAllocator()));

    worker->m_SendThreadId = sysIpcGetCurrentThreadId();
#if !__NO_OUTPUT
	s_SendThreadId = (size_t)(worker->m_SendThreadId);
#endif

    //Signal the caller that we've started
    sysIpcSignalSema(worker->m_SendWaitThread);

    while(!worker->m_SendDone)
    {
#if !__NO_OUTPUT
		unsigned nTopTime = sysTimer::GetSystemMsTime();
		static unsigned nBottomTime = 0;
		const unsigned nElapsedTime = (nBottomTime > 0) ? (nTopTime - nBottomTime) : 0;

		// log when the time between updates is more than LONG_FRAME_TIME ms.
		// this will also fire for 'known' stalls such breakpoints, adding a bug 
		// or warping the player but it's only a log entry
		static const unsigned STALL_THRESHOLD = 500;
		if(nElapsedTime > STALL_THRESHOLD)
			netDebug1("NetStallDetect :: SendThread - Stall of %ums (Current: %ums, Last: %ums)", nElapsedTime, nTopTime, nBottomTime);
#endif 

		worker->m_SendTimeStep.SetTime(sysTimer::GetSystemMsTime());
		cxnMgr->Send();

		for(unsigned chId = 0; chId < NET_MAX_CHANNELS; ++chId)
		{
			cxnMgr->m_Channels[chId].Update(worker->m_SendTimeStep);
		}

        unsigned waitMs = cxnMgr->m_MinSendInterval;
        if(waitMs > 250)
        {
            waitMs = 250;
        }
        else if(waitMs < 10)
        {
            waitMs = 10;
        }

#if !__NO_OUTPUT
		nBottomTime = sysTimer::GetSystemMsTime();
		const unsigned nSendTime = (nBottomTime - nTopTime);
		if(nSendTime > STALL_THRESHOLD)
			netWarning("NetStallDetect :: SendThread - Send time of %ums (Top: %ums, Bottom: %ums)", nSendTime, nTopTime, nBottomTime);
#endif

        sysIpcWaitSemaTimed(worker->m_SendSema, waitMs);
    }

    //Signal the caller that we've stopped
    sysIpcSignalSema(worker->m_SendWaitThread);

	//Memory: reset to the old allocator before leaving
	sysMemAllocator::SetCurrent(oldAllocator);
}

///////////////////////////////////////////////////////////////////////////////
// netConnectionManager::Delegate
///////////////////////////////////////////////////////////////////////////////

netConnectionManager::Delegate::Delegate()
    : m_Owner(NULL)
    , m_ChannelId(NET_INVALID_CHANNEL_ID)
	, m_ChannelProc(0)
{
}

netConnectionManager::Delegate::~Delegate()
{
    netAssertf(!m_Owner , "Forgot to unregister a delegate");
}

void
netConnectionManager::Delegate::Unregister()
{
    if(m_Owner)
    {
        m_Owner->RemoveChannelDelegate(this);
        netAssert(NET_INVALID_CHANNEL_ID == m_ChannelId);
    }
}

bool
netConnectionManager::Delegate::IsRegistered() const
{
    return NULL != m_Owner;
}

///////////////////////////////////////////////////////////////////////////////
// netConnectionManager::Channel
///////////////////////////////////////////////////////////////////////////////

netConnectionManager::Channel::Channel()
    : m_ChannelId(NET_INVALID_CHANNEL_ID)
    //Multiply by 1000 because we use milliseconds when computing
    //bandwidth used.
    , m_OutboundLimiter(netChannelPolicies::DEFAULT_OUTBOUND_BANDWIDTH_LIMIT * 1000)
    , m_Proc(1)
    , m_BandwidthExceeded(false)
{
}

void
netConnectionManager::Channel::Init(const unsigned channelId)
{
    netAssert(m_PktRecorders.empty());
    netAssert(m_Dlgts.empty());

    m_ChannelId = channelId;
    m_Policies = netChannelPolicies();
    m_OutboundLimiter = netChannelPolicies::DEFAULT_OUTBOUND_BANDWIDTH_LIMIT * 1000;
    m_Proc = 1;
    m_BandwidthExceeded = false;
}

void
netConnectionManager::Channel::Shutdown()
{
    //The connection manager should have removed all of our delegates.
    netAssert(m_Dlgts.empty());

    m_ChannelId = NET_INVALID_CHANNEL_ID;
    m_PktRecorders.clear();
}

void
netConnectionManager::Channel::Update(const netTimeStep& timeStep)
{
    netAssert(NET_INVALID_CHANNEL_ID != m_ChannelId);

    if(!m_PktRecorders.empty())
    {
		PktRecorderList::iterator it = m_PktRecorders.begin();
		PktRecorderList::const_iterator stop = m_PktRecorders.end();
        for(; stop != it; ++it)
        {
            (*it)->Update(timeStep.GetCurrent());
        }
    }

    if(timeStep.HasTimeStep())
    {
        int delta = timeStep.GetTimeStep();
        netAssert(delta >= 0);

        //Multiply by 1000 because we use milliseconds when computing
        //bandwidth used.
        const int scaledLimit = m_Policies.m_OutboundBandwidthLimit * 1000;

        //Clamp delta time.
        if(delta > 1000){delta = 1000;}

        m_OutboundLimiter += delta * m_Policies.m_OutboundBandwidthLimit;

        if(m_OutboundLimiter > scaledLimit)
        {
            m_OutboundLimiter = scaledLimit;
        }
    }
    else
    {
        m_OutboundLimiter = m_Policies.m_OutboundBandwidthLimit * 1000;
    }
}

void
netConnectionManager::Channel::RecordOutbound(const netAddress& addr,
                                                const void* bytes,
                                                const unsigned numBytes)
{
    netAssert(NET_INVALID_CHANNEL_ID != m_ChannelId);

    if(!m_PktRecorders.empty())
    {
		PktRecorderList::iterator it = m_PktRecorders.begin();
		PktRecorderList::const_iterator stop = m_PktRecorders.end();
        for(; stop != it; ++it)
        {
			const netSocketAddress& sockAddr = addr.IsPeerRelayAddr() ?
												addr.GetProxyAddress() : addr.GetTargetAddress();
            (*it)->RecordOutboundPacket(sockAddr, bytes, numBytes);
        }
    }

    if(m_Policies.m_OutboundBandwidthLimit)
    {
        //Multiply by 1000 because we use milliseconds when computing
        //bandwidth used.
        m_OutboundLimiter -= numBytes * 1000;
    }
}

void
netConnectionManager::Channel::RecordInbound(const netAddress& addr,
                                                const void* bytes,
                                                const unsigned numBytes)
{
    netAssert(NET_INVALID_CHANNEL_ID != m_ChannelId);

    if(!m_PktRecorders.empty())
    {
		PktRecorderList::iterator it = m_PktRecorders.begin();
		PktRecorderList::const_iterator stop = m_PktRecorders.end();
        for(; stop != it; ++it)
        {
			const netSocketAddress& sockAddr = addr.IsPeerRelayAddr() ?
												addr.GetProxyAddress() : addr.GetTargetAddress();
			(*it)->RecordInboundPacket(sockAddr, bytes, numBytes);
        }
    }
}

void
netConnectionManager::Channel::AddDelegate(Delegate* dlgt)
{
    netAssert(NET_INVALID_CHANNEL_ID != m_ChannelId);

    netAssert(!dlgt->m_Owner);
    netAssert(NET_INVALID_CHANNEL_ID == dlgt->m_ChannelId);

	ChannelDlgtList::iterator it = std::find(m_Dlgts.begin(), m_Dlgts.end(), dlgt);
	if(netVerify(m_Dlgts.end() == it))
	{
		m_Dlgts.push_back(dlgt);
	}
    dlgt->m_ChannelProc = 0;
    dlgt->m_ChannelId = m_ChannelId;
}

void
netConnectionManager::Channel::RemoveDelegate(Delegate* dlgt)
{
    netAssert(NET_INVALID_CHANNEL_ID != m_ChannelId);

    if(dlgt->m_Owner)
    {
        netAssert(NET_INVALID_CHANNEL_ID != dlgt->m_ChannelId);

		ChannelDlgtList::iterator it = std::find(m_Dlgts.begin(), m_Dlgts.end(), dlgt);
		if(netVerify(m_Dlgts.end() != it))
		{
			m_Dlgts.erase(dlgt);
		}
        dlgt->m_ChannelProc = 0;
        dlgt->m_ChannelId = NET_INVALID_CHANNEL_ID;
    }
}

void
netConnectionManager::Channel::DispatchEvent(netConnectionManager* cxnMgr,
											 netEvent* e)
{
	PROFILE

    const size_t numDlgts = m_Dlgts.size();

    //This code may look a bit strange, but when implemented
    //this way it can handle cases where the callback results
    //in removal of delegates, including the delegate that's
    //currently being called

    for(size_t i = 0; i < numDlgts && !m_Dlgts.empty(); ++i)
    {
        Delegate* dlgt = m_Dlgts.front();
        if(m_Proc == dlgt->m_ChannelProc)
        {
			netDebug("dispatching channeled event - hit break: m_Proc: %u, dlgt->m_ChannelProc: %u", m_Proc, dlgt->m_ChannelProc);
            break;
        }

        m_Dlgts.pop_front();
        m_Dlgts.push_back(dlgt);
        dlgt->m_ChannelProc = m_Proc;
        dlgt->Invoke(cxnMgr, e);
    }

    while(0 == ++m_Proc){}
}

///////////////////////////////////////////////////////////////////////////////
// netConnectionManager
///////////////////////////////////////////////////////////////////////////////

netConnectionManager* g_CxnMgr;
bool netConnectionManager::sm_bAcquireCritSecForFullMainUpdate = false;
ServiceDelegate netConnectionManager::sm_ServiceDelegate;
bool netConnectionManager::sm_EnableDelegate = false;
bool netConnectionManager::sm_MetricEnabled = false;
bool netConnectionManager::sm_CancelTunnelsMatchingActiveEndpoints = false;
bool netConnectionManager::sm_EnableOverallQos = true; 

netConnectionManager::netConnectionManager()
    : m_SocketManager(nullptr)
    , m_Allocator(nullptr)
    , m_SendInterval(DEFAULT_SEND_INTERVAL_MS)
    , m_MinSendInterval(DEFAULT_SEND_INTERVAL_MS)
    , m_NumRelayEndpoints(0)
    , m_OutOfMemoryCallback(nullptr)
    , m_IsInitialized(false)
	, m_IsMemoryTracking(false)
    , m_EnableCompression(false)
    , m_IsMT(false)
    , m_DispatchTimedOut(false)
#if !__FINAL
	, m_DisableInactiveEndpointRemoval(false)
#endif //!__FINAL
	, m_LastReceiveTime(0)
	, m_SendFrame(0)
	, m_PackFrame(0)
	, m_ReceiveFrame(0)
	, m_UnpackFrame(0)
	, m_LastPackFrameSent(0)
	, m_RecvPackFrameMark(0)
	, m_Router(this)
{
	m_SocketDelegatePacketReceived.Bind(this, &netConnectionManager::OnSocketEventPacketReceived);
	m_SocketDelegateReceiveThreadTicked.Bind(this, &netConnectionManager::OnSocketEventReceiveThreadTicked);

	m_RelayDelegatePacketReceived.Bind(this, &netConnectionManager::OnRelayEventPacketReceived);
	m_RelayDelegateAddressChanged.Bind(this, &netConnectionManager::OnRelayEventAddressChanged);

    m_IsListening.Reset();
}

netConnectionManager::~netConnectionManager()
{
    this->Shutdown();
}

void 
netConnectionManager::SetEndpointInactivityTimeoutMs(const unsigned nInactivityTimeoutMs)
{
	if(netEndpoint::sm_InactivityTimeoutMs != nInactivityTimeoutMs)
	{
		netDebug("SetEndpointInactivityTimeoutMs :: %u", nInactivityTimeoutMs);
		netEndpoint::sm_InactivityTimeoutMs = nInactivityTimeoutMs;
	}
}

void 
netConnectionManager::SetEndpointCxnlessTimeoutMs(const unsigned nCxnlessTimeoutMs)
{
	if(netEndpoint::sm_CxnlessTimeoutMs != nCxnlessTimeoutMs)
	{
		netDebug("SetEndpointCxnlessTimeoutMs :: %u", nCxnlessTimeoutMs);
		netEndpoint::sm_CxnlessTimeoutMs = nCxnlessTimeoutMs;
	}
}

void
netConnectionManager::SetBadQosThresholdMs(const unsigned badQosThresholdMs)
{
	if(netEndpoint::sm_BadQosThresholdMs != badQosThresholdMs)
	{
		netDebug("SetBadQosThresholdMs :: %u", badQosThresholdMs);
		netEndpoint::sm_BadQosThresholdMs = badQosThresholdMs;
	}
}

void
netConnectionManager::SetEnableOverallQos(const bool enableOverallQos)
{
	if(sm_EnableOverallQos != enableOverallQos)
	{
		netDebug("SetEnableOverallQos :: %s", LogBool(enableOverallQos));
		sm_EnableOverallQos = enableOverallQos;
	}
}

void
netConnectionManager::MetricConfigChanged()
{
	// Ideally we'd do without this function, but polling is currently too slow
	// IsDisabled only checks if telemetry and this metric is enabled, it ignores any sampling settings
	netIpAddress addr;
	netEndpoint::Statistics s;
	rlStandardMetrics::P2pConnectionReport m(addr, addr, addr, addr, s);
	sm_MetricEnabled = !m.IsDisabled();
}

bool 
netConnectionManager::SetPoolSizeHints(const unsigned nEpHint, const unsigned nCxnHint)
{
	// at least one of these should be > 0
	netAssertf(nEpHint || nCxnHint, "SetPoolSizeHints :: At least one of endpoint / cxn hint must be > 0");

	// validate endpoint size
	if(!netVerifyf(nEpHint <= MAX_ENDPOINTS_IN_POOL, "SetPoolSizeHints :: Too many endpoints. Max: %u, Supplied: %u", nEpHint, MAX_ENDPOINTS_IN_POOL))
	{
		netError("SetPoolSizeHints :: Too many endpoints. Max: %u, Supplied: %u", nEpHint, MAX_ENDPOINTS_IN_POOL);
		return false;
	}

	// validate connection size
	if(!netVerifyf(nCxnHint <= MAX_CXNS_IN_POOL, "SetPoolSizeHints :: Too many connections. Max: %u, Supplied: %u", nCxnHint, MAX_CXNS_IN_POOL))
	{
		netError("SetPoolSizeHints :: Too many connections. Max: %u, Supplied: %u", nCxnHint, MAX_CXNS_IN_POOL);
		return false;
	}

	unsigned nCurrentEps = m_EpPile.Size();
	unsigned nCurrentCxns = m_CxnPile.Size();

	if(nEpHint > 0)
	{
		if(nEpHint > nCurrentEps)
		{
			// allocate new endpoints
			if(!netVerifyf(m_EpPile.Resize(nEpHint), "SetPoolSizeHints :: Failed to allocate new endpoint capacity of %u (Current: %u)", nEpHint, nCurrentEps))
			{
				netError("SetPoolSizeHints :: Failed to allocate new endpoint capacity of %u (Current: %u)", nEpHint, nCurrentEps);
				return false;
			}

			// push new allocations into our pool
			for(unsigned i = nCurrentEps; i < nEpHint; ++i)
			{
				m_EpPool.push_back(&m_EpPile[i]);
			}
		}
	}

	if(nCxnHint > 0)
	{
		if(nCxnHint > nCurrentCxns)
		{
			// allocate new connections
			if(!netVerifyf(m_CxnPile.Resize(nCxnHint), "SetPoolSizeHints :: Failed to allocate new connection capacity of %u (Current: %u)", nCxnHint, nCurrentCxns))
			{
				netError("SetPoolSizeHints :: Failed to allocate new connection capacity of %u (Current: %u)", nCxnHint, nCurrentCxns);
				return false;
			}

			// push new allocations into our pool
			for(unsigned i = nCurrentCxns; i < nCxnHint; ++i)
			{
				// initialise connection
				if(!netVerifyf(m_CxnPile[i].Init(this, &m_EventQ, m_Allocator), "SetPoolSizeHints :: Error initializing connection: %u", i))
				{
					netError("SetPoolSizeHints :: Error initializing connection: %u", i);
				}

				m_CxnPool.push_back(&m_CxnPile[i]);
			}
		}
	}

#if !__NO_OUTPUT
	if((nCurrentEps != nEpHint) || (nCurrentCxns != nCxnHint))
		netDebug("SetPoolSizeHints :: Endpoints - Was: %u, Now: %u, Connections - Was: %u, Now: %u", nCurrentEps, nEpHint, nCurrentCxns, nCxnHint);
#endif

	return true;
}

bool
netConnectionManager::Init(sysMemAllocator* allocator,
						   const int maxEpsHint,
						   const int maxCxnsHint,
						   netSocketManager* socketManager,
                           const int cpuAffinity,
						   sysMemAllocator* compressionAllocator,
						   const char* dictionaryFilename,
						   netOutOfMemoryCallabck oomCallback)
{
    netDebug("Init");

	netAssert(maxEpsHint > 0 && maxEpsHint <= MAX_ENDPOINTS_IN_POOL);
	netAssert(maxCxnsHint > 0 && maxCxnsHint <= MAX_CXNS_IN_POOL);
	netAssert(socketManager);

    bool success = false;

    rtry
    {
        rverify(!this->IsInitialized(), catchall, netError("Already initialized"));

        rverify(NULL == g_CxnMgr, catchall, netError("Already initialized"));

        g_CxnMgr = this;

        netAssert(m_EpPool.empty());
		netAssert(m_CxnPool.empty());
        netAssert(m_ActiveEndpoints.empty());
		netAssert(m_EndpointsByEndpointId.empty());
        netAssert(m_EndpointsByAddr.empty());
		netAssert(m_EndpointsByPeerId.empty());
		netAssert(m_PendingEndpoints.empty());

		if (m_IsMemoryTracking)
		{
			allocator->BeginLayer();
		}

        AutoIdInit();

		m_Rng.SetRandomSeed();

        m_SocketManager = socketManager;
        m_Allocator = allocator;
        m_IsListening.Reset();
        m_EnableCompression = DEFAULT_COMPRESSION_ENABLED;
        m_SendInterval = m_MinSendInterval = DEFAULT_SEND_INTERVAL_MS;
        m_DispatchTimedOut = false;
        m_OutOfMemoryCallback = oomCallback;
#if !__FINAL
		m_DisableInactiveEndpointRemoval = PARAM_netEndpointRemovalDisabled.Get();
		PARAM_netEndpointInactivityMs.Get(netEndpoint::sm_InactivityTimeoutMs);
		PARAM_netEndpointCxnlessMs.Get(netEndpoint::sm_CxnlessTimeoutMs);
#endif //!__FINAL

#if !__NO_OUTPUT
		PARAM_netCxnLockWaitThreshold.Get(s_CxnLockWaitThreshold);
		PARAM_netCxnLockHoldThreshold.Get(s_CxnLockHoldThreshold);
		PARAM_netCxnLockLongWaitThreshold.Get(s_CxnLockLongWaitThreshold);
		PARAM_netCxnLockLongHoldThreshold.Get(s_CxnLockLongHoldThreshold);

		int param; 
		if(PARAM_netCxnLockOnlyOutsideLock.Get(param))
		{
			s_CxnLockOnlyOutsideLock = (param == 1); 
		}
		if(PARAM_netCxnLockPrintMainStackTrace.Get(param))
		{
			s_CxnLockPrintMainStackTrace = (param == 1); 
		}
#endif

        m_TimeStep.Init(0);
        m_LastReceiveTime = 0;
        m_LastMainThreadUpdateTime = 0;
		m_LastReceiveTimeoutCheckTime = 0;

		m_EpPile.Init(m_Allocator);
		m_CxnPile.Init(m_Allocator);

        rverify(m_EpPile.Resize(maxEpsHint),
                catchall,
                netError("Error allocating endpoints"));

		for(int i = 0; i < maxEpsHint; ++i)
		{
			m_EpPool.push_back(&m_EpPile[i]);
		}

        rverify(m_CxnPile.Resize(maxCxnsHint),
                catchall,
                netError("Error allocating connections"));

        for(int i = 0; i < maxCxnsHint; ++i)
        {
            rverify(m_CxnPile[i].Init(this, &m_EventQ, allocator),
                    catchall,
                    netError("Error initializing connection:%d", i));                                   

            m_CxnPool.push_back(&m_CxnPile[i]);
        }

		for(int i = 0; i < NET_MAX_CHANNELS; ++i)
        {
            m_Channels[i].Init(i);
        }

        m_NumRelayEndpoints = 0;

        //This is temporary until we can remove the delegate interface
        //for delivering events.  m_EventConsumer pulls events from the
        //queue and pushes them to delegates.
        m_EventQ.Subscribe(&m_EventConsumer);

        //Set the initialized flag before starting the worker.
        m_IsInitialized = true;

		m_Compression.Init(compressionAllocator, this, dictionaryFilename);

		// must be done after initialized is true because we set up delegates in the tunneler
        rverify(m_Tunneler.Init(this),
                catchall,
                netError("Error initializing tunneler"));

		m_Router.Init(m_Allocator);

        if(cpuAffinity >= 0)
        {
            m_IsMT = true;

            rverify(m_Worker.Init(this, cpuAffinity),
                    catchall,
                    netError("Error starting the Worker thread"));
        }

		m_SocketManager->AddConcurrentDelegate(NET_SOCKET_EVENT_PACKET_RECEIVED, &m_SocketDelegatePacketReceived);
		m_SocketManager->AddConcurrentDelegate(NET_SOCKET_EVENT_RECEIVE_THREAD_TICKED, &m_SocketDelegateReceiveThreadTicked);
        netRelay::AddConcurrentDelegate(NET_RELAYEVENT_PACKET_RECEIVED, &m_RelayDelegatePacketReceived);
		netRelay::AddConcurrentDelegate(NET_RELAYEVENT_ADDRESS_CHANGED, &m_RelayDelegateAddressChanged);
		if (!sm_ServiceDelegate.IsBound())
		{
			sm_ServiceDelegate.Bind(&netConnectionManager::OnSysOnServiceEvent);
			g_SysService.AddDelegate(&sm_ServiceDelegate);
		}
		sm_EnableDelegate = true;
		
		netRelay::StartRelay();

		success = true;
    }
    rcatchall
    {
        this->Shutdown();
    }

    return success;
}

void
netConnectionManager::Shutdown()
{
    netDebug("Shutting down connection manager...");

    if(this->IsInitialized())
    {
        netRelay::StopRelay();

		sm_EnableDelegate = false;

        netRelay::RemoveConcurrentDelegate(NET_RELAYEVENT_ADDRESS_CHANGED, &m_RelayDelegateAddressChanged);
		netRelay::RemoveConcurrentDelegate(NET_RELAYEVENT_PACKET_RECEIVED, &m_RelayDelegatePacketReceived);

		m_SocketManager->RemoveConcurrentDelegate(NET_SOCKET_EVENT_RECEIVE_THREAD_TICKED, &m_SocketDelegateReceiveThreadTicked);
		m_SocketManager->RemoveConcurrentDelegate(NET_SOCKET_EVENT_PACKET_RECEIVED, &m_SocketDelegatePacketReceived);

		g_SysService.RemoveDelegate(&sm_ServiceDelegate);

        if(m_IsMT)
        {
            m_Worker.Shutdown();
            m_IsMT = false;
        }

		m_Router.Shutdown();

        //Clear the channels, being careful not to interleave
        //critical sections to avoid deadlock.
        for(int i = 0; i < NET_MAX_CHANNELS; ++i)
        {
            {
                NET_CS_SYNC(m_CsDlgt);

                while(!m_Channels[i].m_Dlgts.empty())
                {
                    this->RemoveChannelDelegate(*m_Channels[i].m_Dlgts.begin());
                }
            }

            {
                NET_CS_SYNC(m_Cs);
                m_Channels[i].Shutdown();
            }
        }

        //Must come after worker shutdown to avoid deadlock.
        NET_CS_SYNC(m_Cs);

		m_Compression.Shutdown();

		//Close remaining connections.
		while(!m_ActiveEndpoints.empty())
		{
			netEndpoint* ep = m_ActiveEndpoints.front();
			const size_t count = ep->m_CxnList.size();

			if(count)
			{
				for(int i = 0; i < (int)count; ++i)
				{
					netAssert(ep->m_CxnList.size() == count - i);
					this->CloseConnection(*ep->m_CxnList.begin(), NET_CLOSE_FINALLY);
				}

				//KB - The endpoint is no longer destroyed when the last
				//connection is closed.  Instead a timeout is set and
				//when the timeout expires the endpoint is destroyed.
				//IOW, when shutting down we can't wait for the timeout so
				//we need to explicitly call DestroyEndpoint().

				/*//The endpoint should have been removed from
				//the active endpoints list.
				netAssert(m_ActiveEndpoints.empty()
				|| *m_ActiveEndpoints.begin() != ep);*/
			}
			//else
			//{
			this->DestroyEndpoint(ep);
			//}
		}

		netAssert(m_NumRelayEndpoints == 0);
		m_NumRelayEndpoints = 0;

        //Clear pending endpoints
        while(!m_PendingEndpoints.empty())
        {
            netEndpoint* ep = m_PendingEndpoints.front();
            this->FreeEndpoint(ep);
        }
        netAssert(m_ActiveEndpoints.empty());
		netAssert(m_EndpointsByEndpointId.empty());
        netAssert(m_EndpointsByAddr.empty());
		netAssert(m_EndpointsByPeerId.empty());
		netAssert(m_PendingEndpoints.empty());

		m_CxnPool.clear();
		netAssert(m_CxnPool.empty());

        m_EpPool.clear();
        netAssert(m_EpPool.empty());

        m_EventQ.UnsubscribeAll();
        m_EventQ.ClearEvents();

		m_EpPile.Shutdown();
		m_CxnPile.Shutdown();

        AutoIdShutdown();

		m_Tunneler.Shutdown();

		if (m_IsMemoryTracking)
		{
			NET_ASSERTS_ONLY(int numLeaks = )m_Allocator->EndLayer("netConnectionManager", NULL);
			netAssertf(numLeaks == 0, "netConnectionManager::Shutdown :: num leaks: %d", numLeaks);
		}

        m_IsListening.Reset();
        m_EnableCompression = DEFAULT_COMPRESSION_ENABLED;
        m_SendInterval = m_MinSendInterval = DEFAULT_SEND_INTERVAL_MS;
        m_DispatchTimedOut = false;
#if !__FINAL
		m_DisableInactiveEndpointRemoval = false;
#endif //!__FINAL
        m_LastReceiveTime = 0;
        m_LastMainThreadUpdateTime = 0;
		m_LastReceiveTimeoutCheckTime = 0;

        m_SocketManager = nullptr;
		m_Allocator = nullptr;
        g_CxnMgr = NULL;

        m_IsInitialized = false;
    }
}

void netConnectionManager::SetMemoryTrackingEnabled(bool bEnabled)
{
	netAssert(!m_IsInitialized);
	m_IsMemoryTracking = bEnabled;
}

bool
netConnectionManager::IsUsingRelay() const
{
    return (m_NumRelayEndpoints > 0);
}

netSocketManager*
netConnectionManager::GetSocketManager() const
{
    return m_SocketManager;
}

void
netConnectionManager::StartListening(const unsigned channelId)
{
    netDebug2("Start listening for connections on channel:%d", channelId);
    netAssert(this->IsInitialized());
    m_IsListening.Set(channelId);
}

void
netConnectionManager::StopListening(const unsigned channelId)
{
    netDebug2("Stop listening for connections on channel:%d", channelId);
    netAssert(this->IsInitialized());
    m_IsListening.Clear(channelId);
}

bool
netConnectionManager::IsListening(const unsigned channelId) const
{
    netAssert(this->IsInitialized());
    return m_IsListening.IsSet(channelId);
}

bool 
netConnectionManager::OpenTunnel(const netPeerAddress& peerAddr,
                                const netTunnelDesc& tunnelDesc,
                                netTunnelRequest* tunnelRqst,
                                netStatus* status)
{
    netAssert(this->IsInitialized());
    netAssert(NET_TUNNELTYPE_LAN == tunnelDesc.m_TunnelType
            || NET_TUNNELTYPE_ONLINE == tunnelDesc.m_TunnelType);

	if(!netVerifyf(peerAddr.IsValid(), "Invalid peerAddr"))
	{
		return false;
	}

	if(!netVerifyf(status, "Invalid status object"))
    {
        return false;
    }

	if(!netVerifyf(tunnelRqst, "Invalid tunnelRqst object"))
	{
		return false;
	}

    NET_CS_SYNC(m_Cs);

    bool success = false;

    //NOTE:  We don't check for duplicate endpoints until the
    //tunnel request created by calling OpenTunnel() has completed.
    //See UpdateTunnelRequests().

    netEndpoint* ep = this->AllocEndpoint();

    if(ep)
    {
		ep->m_PeerAddr = peerAddr;
        ep->m_RelayAddr = peerAddr.GetRelayAddress();
        ep->m_Status = status;
        ep->m_Status->SetPending();

        success = m_Tunneler.OpenTunnel(peerAddr,
                                        tunnelDesc,
                                        tunnelRqst,
                                        &ep->m_MyStatus);

        if(success)
        {
            ep->m_TunnelRqst = tunnelRqst;

            m_PendingEndpoints.push_back(ep);

            //If this request is pending (it might have already succeeded), check if we already 
            //have an active endpoint with the given relay address. 
            //This will override the request, completing it successfully immediately. This prevents
            //code in UpdateTunnelRequests() from cancelling the in-flight request in the next frame
            if(sm_CancelTunnelsMatchingActiveEndpoints && ep->m_TunnelRqst->Pending())
            {
                if(ep->m_RelayAddr.IsValid() && this->HasActiveEndpoint(ep->m_RelayAddr))
                {
					netDebug("OpenTunnel :: Existing endpoint with relay address [" NET_ADDR_FMT "]. Overriding request", NET_ADDR_FOR_PRINTF(ep->m_RelayAddr));
                    m_Tunneler.OverrideRequest(ep->m_TunnelRqst, ep->m_RelayAddr);
                }
            }

			// if we already have an endpoint with this peerId, reset the timeout so that we don't
			// inadvertently close this prior to completing the tunnel request
			netEndpoint* activeEp = GetActiveEndpointByPeerId(peerAddr.GetPeerId());
			if(activeEp && activeEp->m_Timeout < static_cast<int>(netEndpoint::sm_CxnlessTimeoutMs))
			{
				netDebug("OpenTunnel :: Have active endpoint with this peer id, resetting timeout");
				activeEp->m_Timeout = netEndpoint::sm_CxnlessTimeoutMs;
			}
        }
        else
        {
			netError("OpenTunnel :: Failed to open tunnel");
			ep->m_Status->SetFailed();
            this->FreeEndpoint(ep);
        }
    }

    return success;
}

void
netConnectionManager::CancelTunnelRequest(netTunnelRequest* tunnelRqst)
{
    netAssert(this->IsInitialized());

	netDebug("CancelTunnelRequest :: Cancelling tunnel request - Pending: %s", tunnelRqst->Pending() ? "True" : "False");

	// note: it's possible that the tunnelRqst has completed but we still have it in our pending endpoints list
	// (eg. if the tunnel request completed, then someone called CancelTunnelRequest() before we've processed the completed tunnel request.
	// We need to set the caller's status object to completed and don't use their status pointer after this function completes.
    NET_CS_SYNC(m_Cs);

    EndpointList::iterator it = m_PendingEndpoints.begin();
    EndpointList::const_iterator stop = m_PendingEndpoints.end();
    for(; stop != it; ++it)
    {
        netEndpoint* ep = *it;
        if(ep->m_TunnelRqst == tunnelRqst)
        {
            //Freeing the endpoint cancels the tunnel request
            this->FreeEndpoint(ep);
            break;
        }
    }

    netAssert(!tunnelRqst->Pending());
}

void 
netConnectionManager::RemoveCachedRelayAddress(const netAddress& useAddr)
{
    m_Tunneler.RemoveCachedRelayAddress(useAddr);
}

int
netConnectionManager::AcceptConnection(const netPeerAddress& peerAddr,
									   const netAddress& addr,
									   const ChannelId channelId,
									   const netSequence seq,
									   netStatus* status)
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    netDebug2("*.%d[" NET_ADDR_FMT "]: Accepting connection...",
                channelId,
                NET_ADDR_FOR_PRINTF(addr));
	
#if !__NO_OUTPUT
	if(PARAM_netCxnEstablishmentStackTraces.Get())
	{
		sysStack::PrintStackTrace();
	}
#endif

	bool success = false;

	if(status){status->SetPending();}
		
	Cxn* cxn = 0;

    rtry
    {
        rverify(channelId <= NET_MAX_CHANNEL_ID,
                catchall,
                netError("Invalid channel id:%d", channelId));

		netEndpoint* ep = nullptr;

		netEndpointResolution epResolution = ResolveNewEndpointRequest(peerAddr, peerAddr.GetPeerId(), addr, peerAddr.GetRelayAddress(), &ep OUTPUT_ONLY(, "AcceptConnection"));
		if(epResolution == netEndpointResolution::Resolution_CreateNew)
		{
			ep = this->CreateEndpoint(peerAddr, addr, peerAddr.GetRelayAddress());
		}

		// if we found an existing endpoint, ResolveNewEndpointRequest should have assigned it
		rverify(ep, catchall, netError("AcceptConnection :: Failed to create endpoint!"));
        
		cxn = this->GetCxnByChannelId(ep->GetId(), channelId);

        if(!cxn)
        {
            cxn = this->CreateConnection(ep, channelId);

            rverify(cxn, catchall, netError("Error creating connection"));

            rverify(cxn->Synchronize(seq + 1),
                    catchall,
                    netError("Error synchronizing connection"));
        }
        else
        {
            netDebug2("A connection to:" NET_ADDR_FMT " with channel id:%d exists - adding to ref count",
                    NET_ADDR_FOR_PRINTF(addr), channelId);

            netAssert(cxn->m_Refs > 0);
        }

		// manage status
		if(status)
		{
            if(0 == cxn->m_Refs)
            {
                netAssert(!cxn->m_Status);
                cxn->m_Status = status;
            }
            else
            {
                netAssert(cxn->m_Refs > 0);

                //FIXME (KB) - Need to allow multiple refs to a pending
                //connection.  We should be keeping track of all the status
                //objects so they can all be set to Succeeded when the
                //connection is established, but that will require some
                //re-architecting.
                netAssert(!cxn->m_Status);
                status->SetSucceeded();
            }
		}

		++cxn->m_Refs;

		netDebug("AcceptConnection :: RefCount: %d", cxn->m_Refs);

		success = true;
	}
	rcatchall
	{
		if(cxn && 0 == cxn->m_Refs)
		{
			//Add to refs so CloseConnection() doesn't assert.
			++cxn->m_Refs;
			this->CloseConnection(cxn, NET_CLOSE_FINALLY);
			cxn = 0;
		}
	}

	if(!success && status)
	{
		status->SetFailed();
	}
	
	return success ? cxn->GetId() : NET_INVALID_CXN_ID;
}

bool
netConnectionManager::DenyConnection(const netAddress& addr,
									const ChannelId channelId,
                                    const void* data,
                                    const unsigned sizeofData)
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    netAssert(NULL == data || sizeofData > 0);
    netAssert(sizeofData <= netEventConnectionError::MAX_SIZEOF_DATA);
	netAssertf(!this->GetCxnByChannelId(addr, channelId)
		, "Can't deny a connection that already exists");
	CompileTimeAssert((unsigned)netEventConnectionError::MAX_SIZEOF_DATA <= (unsigned)netBundle::MAX_BYTE_SIZEOF_PAYLOAD);

	netDebug2("DenyConnection");

#if !__NO_OUTPUT
	if(PARAM_netCxnEstablishmentStackTraces.Get())
	{
		sysStack::PrintStackTrace();
	}
#endif
		
	netPacketStorage pktStorage;
	netPacket* pkt = pktStorage.GetPacket();
	netBundle* bndl = (netBundle*) pkt->GetPayload();
	bndl->ResetHeader(sizeofData, channelId, 0, 0, 0, netBundle::FLAG_TERM);
	if(sizeofData)
	{
		sysMemCpy(bndl->GetPayload(), data, sizeofData);
	}
	pkt->ResetHeader(bndl->GetSize(), 0);

	this->SendPacket(addr, &pktStorage, 0, -1, NULL);

	return true;
}

bool
netConnectionManager::CloseConnection(const int cxnId,
                                        const netCloseType closeType)
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    Cxn* cxn;
    return cxnId >= 0
            && 0 != (cxn = this->GetCxnById(cxnId))
            && this->CloseConnection(cxn, closeType);
}

bool 
netConnectionManager::CloseConnection(Cxn* cxn, const netCloseType closeType)
{
	netAssert(this->IsInitialized());
	NET_CS_SYNC(m_Cs);

	rtry
	{
		rverifyall(cxn);

	    netDebug("%08x.%d[" NET_ADDR_FMT "]: Closing connection %s (%d refs)",
                cxn->GetId(),
                cxn->GetChannelId(),
                cxn->m_EpOwner ? NET_ADDR_FOR_PRINTF(cxn->m_EpOwner->GetAddress()) : "",
                closeType == NET_CLOSE_GRACEFULLY ? "GRACEFULLY" : "IMMEDIATELY",
                cxn->m_Refs);

#if !__NO_OUTPUT
		if(PARAM_netCxnEstablishmentStackTraces.Get())
		{
			sysStack::PrintStackTrace();
		}
#endif

		bool success = false;
		bool destroyIt = false;

		if(cxn->IsOpen())
		{
			netAssert(cxn->m_Refs > 0);

			if(NET_CLOSE_FINALLY == closeType)
			{
				cxn->m_Refs = 0;
			}
			else
			{
				--cxn->m_Refs;
			}

			if(0 == cxn->m_Refs)
			{
				cxn->Close();

				if(NET_CLOSE_IMMEDIATELY == closeType
					|| NET_CLOSE_FINALLY == closeType
					|| cxn->CanDestroy(OUTPUT_ONLY(true)))
				{
					destroyIt = true;
				}
				else
				{
					netAssert(NET_CLOSE_GRACEFULLY == closeType);
				}

				netEndpoint* ep = cxn->m_EpOwner;
				netAssert(ep);

				//Remove it from the channel array so it
				//can no longer receive.  It's still in the EP's connection
				//list so it can continue to send messages that are
				//already in the send queue.
				ep->m_Cxns[cxn->GetChannelId()] = NULL;
			}

			success = true;
		}
		else if(cxn->IsPendingOpen())
		{
			netAssert(cxn->m_Refs > 0);

			if(NET_CLOSE_FINALLY == closeType)
			{
				cxn->m_Refs = 0;
			}
			else
			{
				--cxn->m_Refs;
			}

			if(0 == cxn->m_Refs)
			{
				//FIXME (KB) - at some point, when we can have a multiple
				//refs to a pending connection, we'll need to set each status
				//object (one per ref) to failed.
				//See the FIXME in OpenConnection().
				if(cxn->m_Status)
				{
					cxn->m_Status->SetFailed();
					cxn->m_Status = 0;
				}

				netEndpoint* ep = cxn->m_EpOwner;
				netAssert(ep);

				//Remove it from the channel array so DestroyConnection() doesn't
				//assert.
				ep->m_Cxns[cxn->GetChannelId()] = NULL;

				destroyIt = true;
			}

			success = true;
		}
		else if(cxn->IsPendingClose())
		{
			netAssert(NET_CLOSE_IMMEDIATELY == closeType
					|| NET_CLOSE_FINALLY == closeType);
			netAssert(0 == cxn->m_Refs);

			destroyIt = true;

			success = true;
		}
		else
		{
			netAssert(cxn->IsClosed());
			success = true;
		}

		if(destroyIt)
		{
			const size_t requiredBytes = sizeof(netEventConnectionClosed);
			netEventConnectionClosed* e = (netEventConnectionClosed*) m_Allocator->RAGE_LOG_ALLOCATE(requiredBytes, 0);

			if(netVerify(e))
			{
				new(e) netEventConnectionClosed(cxn->GetId(),
												cxn->m_EpOwner ? cxn->m_EpOwner->GetId() : NET_INVALID_ENDPOINT_ID,
												cxn->GetChannelId(),
												m_Allocator);

				this->QueueEvent(e);
			}
			else
			{
				//We have the pre-allocated event, unless we encounter the edge case described below, default to non-fatal
				bool isFatal = false;

				//Plan B - use a pre-allocated connection closed event.
				//We only do this as a last resort because there are edge cases where we close
				//the connection, queue the pre-allocated event, the connection object gets
				//reused for a new connection, then gets closed again while the event
				//is still on the event queue.  If that happens we wouldn't be able to
				//re-queue the pre-allocated event.
				if(netVerify(!cxn->m_CxnClosedEvent.IsQueued()))
				{
					cxn->m_CxnClosedEvent.~netEventConnectionClosed();
					new (&cxn->m_CxnClosedEvent) netEventConnectionClosed(cxn->GetId(),
																		  cxn->m_EpOwner ? cxn->m_EpOwner->GetId() : NET_INVALID_ENDPOINT_ID,
																		  cxn->GetChannelId(),
																		  NULL);
					this->QueueEvent(&cxn->m_CxnClosedEvent);
				}
				else
				{
					//If this is a different connection than the queued event, we've lost someone to an edge case
					if((cxn->m_CxnClosedEvent.m_CxnId != cxn->GetId()) || (cxn->m_CxnClosedEvent.m_ChannelId != cxn->GetChannelId()))
					{
						netError("Pre-allocated netEventConnectionClosed event on Cxn:%08x.%d for prior connection Cxn:%08x.%d",
								 cxn->GetId(),
								 cxn->GetChannelId(),
								 cxn->m_CxnClosedEvent.m_CxnId,
								 cxn->m_CxnClosedEvent.m_ChannelId);

						isFatal = true; 
					}
				}

				netError("Out of memory allocating netEventConnectionClosed (%dB) on Cxn:%08x.%d. Queued: %s, Fatal: %s, Used: %d, Available: %d, Largest Block: %d", 
						 static_cast<int>(sizeof(netEventConnectionClosed)), 
						 cxn->GetId(), 
						 cxn->GetChannelId(),
						 cxn->m_CxnClosedEvent.IsQueued() ? "True" : "False",
						 isFatal ? "True" : "False",
						 static_cast<int>(m_Allocator->GetMemoryUsed(-1)),
						 static_cast<int>(m_Allocator->GetMemoryAvailable()),
						 static_cast<int>(m_Allocator->GetLargestAvailableBlock()));

				this->QueueOutOfMemory(cxn, isFatal, requiredBytes);
			}

	        this->DestroyConnection(cxn);
		}

		return success;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netConnectionManager::DestroyConnection(Cxn* cxn)
{
	netAssert(this->IsInitialized());
		
	NET_CS_SYNC(m_Cs);

	rtry
	{
		rverifyall(cxn);

		netDebug("%08x.%d[" NET_ADDR_FMT "]: Destroying connection",
					cxn->GetId(),
					cxn->GetChannelId(),
					cxn->m_EpOwner ? NET_ADDR_FOR_PRINTF(cxn->m_EpOwner->GetAddress()) : "");

		netAssert(0 == cxn->m_Refs);

	    if(cxn->IsPendingClose() || cxn->IsPendingOpen() || cxn->IsOpen())
		{
			netEndpoint* ep = cxn->m_EpOwner;
			netAssert(ep);
			netAssert(!ep->m_CxnList.empty());
			//The connection should have been removed from the channel array.
			netAssert(ep->m_Cxns[cxn->GetChannelId()] != cxn);

			cxn->UpdateStats();

			ep->m_CxnList.erase(cxn);
			cxn->m_EpOwner = NULL;

			//NOTE(RDT): Made it so endpoints destroy only via timeout, so that
			//           certain session-switching situations (like that found by
			//           RDR's posse system) don't result in a cnxless endpoint
			//           being closed while another session is being joined.
			//           This isn't bulletproof, but makes these kinds of timing
			//           issues less likely.
			if(ep->m_CxnList.empty())
			{
				//this->DestroyEndpoint(ep);
				//not needed to reset m_LastTime
				ep->m_Timeout = netEndpoint::sm_CxnlessTimeoutMs;
				netDebug("Last connection on endpoint: " NET_ADDR_FMT " closed, timeout set to %ums",
						 NET_ADDR_FOR_PRINTF(ep->GetAddress()),
						 ep->m_Timeout);

				m_Router.RemoveEndpoint(ep->GetPeerId(), ep->GetId());
			}

			netAssert(!cxn->m_Status);

			// remove events queued for this connection here. This allows us to avoid
			// checking for closed connections every time we dispatch an event (and
			// avoids taking the critical section lock when dispatching).

			// set up an event filter
			struct Filter
			{
				Filter(const int cxnId) : m_CxnId(cxnId) {}

				bool Apply(const netEvent* e)
				{
					// we still want to dispatch NET_EVENT_ACK_RECEIVED and NET_EVENT_CONNECTION_CLOSED events.
					// note that out-of-band events are not associated with a connection and so are always dispatched.
					const bool remove = (e->m_CxnId >= 0) &&
										(e->m_CxnId == m_CxnId) &&
										(NET_EVENT_ACK_RECEIVED != e->GetId()) &&
										(NET_EVENT_CONNECTION_CLOSED != e->GetId());

#if !__NO_OUTPUT
					if(remove)
					{
						netDebug("%08x.%d netConnectionManager::DestroyConnection :: removing event id %u", e->m_CxnId, e->m_ChannelId, e->GetId());
					}
#endif

					return remove;
				}

			private:
				const int m_CxnId;
			};

			Filter f(cxn->GetId());
			netEventQueue<netEvent>::RemoveEventsFilter filter;
			filter.Reset<Filter, &Filter::Apply>(&f);
			m_EventQ.RemoveEvents(filter, true);

			cxn->Destroy();
		}
		else
		{
			netAssert(cxn->IsClosed());
		}

		m_CxnPool.push_back(cxn);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

unsigned
netConnectionManager::GetNumTimeoutPending()
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	unsigned numTimeoutPending = 0;

	EndpointList::iterator itEp = m_ActiveEndpoints.begin();
	EndpointList::const_iterator stopEp = m_ActiveEndpoints.end();
	for(; stopEp != itEp; ++itEp)
	{
		netEndpoint::CxnList::iterator itCxn = (*itEp)->m_CxnList.begin();
		netEndpoint::CxnList::const_iterator stopCxn = (*itEp)->m_CxnList.end();

		for(; stopCxn != itCxn; ++itCxn)
		{
			Cxn* cxn = *itCxn;
			if(cxn->IsTimedOut())
			{
				numTimeoutPending++;
			}
		}
	}

	return numTimeoutPending;
}

bool
netConnectionManager::HasActiveEndpoint(const netAddress& addr) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    return (NULL != this->GetActiveEndpointByAddr(addr));
}

bool 
netConnectionManager::HasActiveEndpointWithRelayAddr(const netAddress& addr) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

    EndpointList::const_iterator it = m_ActiveEndpoints.begin();
    EndpointList::const_iterator stop = m_ActiveEndpoints.end();
    for(; stop != it; ++it)
    {
        if((*it)->m_RelayAddr == addr)
        {
            return true; 
        }
    }

    //Not found
    return false;
}

bool
netConnectionManager::HasActiveEndpointWithPeerId(const netPeerId& peerId) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	return (NULL != this->GetActiveEndpointByPeerId(peerId));
}

bool 
netConnectionManager::GetAddrByPeerId(const netPeerId& peerId,
										netAddress* addr,
										netAddress* relayAddr,
										unsigned* deltaLastReceiveTimeMs) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetActiveEndpointByPeerId(peerId);

	if(ep)
	{
		if(addr)
		{
			*addr = ep->GetAddress();
		}

		if(relayAddr)
		{
			*relayAddr = ep->m_RelayAddr;
		}

		if(deltaLastReceiveTimeMs)
		{
			*deltaLastReceiveTimeMs = sysTimer::GetSystemMsTime() - ep->m_LastReceiveTime;
		}

		return true;
	}

	return false;
}

bool 
netConnectionManager::GetAddrByPeerId(const netPeerId& peerId,
									  netAddress* addr) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetActiveEndpointByPeerId(peerId);

	if(ep)
	{
		if(addr)
		{
			*addr = ep->GetAddress();
		}

		return true;
	}

	return false;
}

#if __BANK
EndpointId 
netConnectionManager::GetEndpointIdByGamerHandle(const rlGamerHandle& gamerHandle) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetActiveEndpointByGamerHandle(gamerHandle);

	if(ep)
	{
		return ep->GetId();
	}

	return NET_INVALID_ENDPOINT_ID;
}
#endif

void
netConnectionManager::SetGlobalSendInterval(const unsigned sendInterval)
{
    netAssert(this->IsInitialized());

    netAssert(int(sendInterval) >= 0);

    //FIXME (KB) - make this a per-channel policy.
    m_SendInterval = m_MinSendInterval = sendInterval;

    NET_CS_SYNC(m_Cs);

    // Set this for all active and pending endpoints.
    EndpointList::iterator itEp = m_ActiveEndpoints.begin();
    EndpointList::const_iterator stopEp = m_ActiveEndpoints.end();
    for(; stopEp != itEp; ++itEp)
    {
        (*itEp)->m_SendInterval = m_SendInterval;
    }

    itEp = m_PendingEndpoints.begin();
    stopEp = m_PendingEndpoints.end();
    for(; stopEp != itEp; ++itEp)
    {
        (*itEp)->m_SendInterval = m_SendInterval;
    }
}

unsigned
netConnectionManager::GetGlobalSendInterval() const
{
    netAssert(this->IsInitialized());

    return m_SendInterval;
}

void
netConnectionManager::SetCompressionEnabled(const bool enabled)
{
    netAssert(this->IsInitialized());

    m_EnableCompression = enabled;
}

bool
netConnectionManager::IsCompressionEnabled() const
{
    netAssert(this->IsInitialized());

    return m_EnableCompression;
}

bool netConnectionManager::IsCompressionEnabled(const ChannelId channelId) const
{
	return m_Channels[channelId].m_Policies.m_CompressionEnabled;
}

void
netConnectionManager::SetCancelTunnelsMatchingActiveEndpoints(const bool cancelTunnelsMatchingActiveEndpoints)
{
	if(sm_CancelTunnelsMatchingActiveEndpoints != cancelTunnelsMatchingActiveEndpoints)
	{
		netDebug("SetCancelTunnelsMatchingActiveEndpoints :: %s", cancelTunnelsMatchingActiveEndpoints ? "true" : "false");
		sm_CancelTunnelsMatchingActiveEndpoints = cancelTunnelsMatchingActiveEndpoints;
	}
}

bool 
netConnectionManager::SetConnectionPolicies(const netConnectionPolicies& policies)
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	EndpointList::iterator itEp = m_ActiveEndpoints.begin();
	EndpointList::const_iterator stopEp = m_ActiveEndpoints.end();
	for(; stopEp != itEp; ++itEp)
	{
		netEndpoint::CxnList::iterator itCxn = (*itEp)->m_CxnList.begin();
		netEndpoint::CxnList::const_iterator stopCxn = (*itEp)->m_CxnList.end();

		for(; stopCxn != itCxn; ++itCxn)
		{
			Cxn* cxn = *itCxn;
			if(!cxn->IsClosed() && !cxn->IsPendingClose())
			{
				cxn->SetPolicies(policies);
			}
		}
	}

	return true;
}

void
netConnectionManager::SetChannelPolicies(const unsigned channelId,
                                        const netChannelPolicies& policies)
{
    netAssert(this->IsInitialized());

    netAssert(channelId <= NET_MAX_CHANNEL_ID);
    netAssertf(policies.m_OutboundBandwidthLimit <= netChannelPolicies::MAX_OUTBOUND_BANDWIDTH_LIMIT
            , "Outbound bandwidth limit is too large");

    NET_CS_SYNC(m_Cs);

    netDebug2("Setting policies on channel:%d :", channelId);
    netDebug2("  Outbound bandwidth limit:     %d (was %d)",
                policies.m_OutboundBandwidthLimit,
                m_Channels[channelId].m_Policies.m_OutboundBandwidthLimit);
	netDebug2("  Compression Enabled:     %s (was %s)",
				policies.m_CompressionEnabled ? "true" : "false",
				m_Channels[channelId].m_Policies.m_CompressionEnabled ? "true" : "false");
	netDebug2("  Always Maintain Keepalive:     %s (was %s)",
				policies.m_AlwaysMaintainKeepAlive ? "true" : "false",
				m_Channels[channelId].m_Policies.m_AlwaysMaintainKeepAlive ? "true" : "false");

    m_Channels[channelId].m_Policies = policies;

    m_Channels[channelId].m_OutboundLimiter = policies.m_OutboundBandwidthLimit * 1000;
}

void
netConnectionManager::GetChannelPolicies(const unsigned channelId,
                                        netChannelPolicies* policies) const
{
    netAssert(this->IsInitialized());

    netAssert(channelId <= NET_MAX_CHANNEL_ID);

    NET_CS_SYNC(m_Cs);

    *policies = m_Channels[channelId].m_Policies;
}

void
netConnectionManager::SetDefaultConnectionPolicies(const unsigned channelId,
                                                const netConnectionPolicies& policies)
{
    netAssert(this->IsInitialized());

    netAssert(channelId <= NET_MAX_CHANNEL_ID);

    NET_CS_SYNC(m_Cs);

    m_Channels[channelId].m_CxnPolicies = policies;
}

void
netConnectionManager::SetDefaultConnectionPolicies(const netConnectionPolicies& policies)
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    for(unsigned i = 0; i < NET_MAX_CHANNELS; ++i)
    {
        this->SetDefaultConnectionPolicies(i, policies);
    }

	netDebug("SetDefaultConnectionPolicies :: Timeout: %u, KeepAliveInterval: %u, ResendInterval: %u, MaxSendCount: %u",
			 policies.m_TimeoutInterval, 
			 policies.m_KeepaliveInterval, 
			 policies.m_ResendInterval, 
			 policies.m_MaxSendCount); 
}

void
netConnectionManager::GetDefaultConnectionPolicies(const unsigned channelId,
                                                    netConnectionPolicies* policies) const
{
    netAssert(this->IsInitialized());

    netAssert(channelId <= NET_MAX_CHANNEL_ID);

    NET_CS_SYNC(m_Cs);

    *policies = m_Channels[channelId].m_CxnPolicies;
}

void
netConnectionManager::SetP2pKeySalt(const netP2pCrypt::HmacKeySalt& p2pKeySalt)
{
	netP2pCrypt::SetHmacKeyAndSalt(p2pKeySalt);
}

#if !__FINAL
void
netConnectionManager::SetDisableInactiveEndpointRemoval(const bool disable)
{
	if(m_DisableInactiveEndpointRemoval != disable)
	{
		netDebug("SetDisableInactiveEndpointRemoval :: %s -> %s", m_DisableInactiveEndpointRemoval ? "True" : "False", disable ? "True" : "False");
		m_DisableInactiveEndpointRemoval = disable;
	}
}
#endif //!__FINAL

void
netConnectionManager::RegisterChannelPacketRecorder(const unsigned channelId,
                                                    netPacketRecorder* recorder)
{
    netAssert(this->IsInitialized());

    netAssert(channelId <= NET_MAX_CHANNEL_ID);

    NET_CS_SYNC(m_Cs);

    m_Channels[channelId].m_PktRecorders.push_back(recorder);
}

void
netConnectionManager::UnregisterChannelPacketRecorder(const unsigned channelId,
                                                        netPacketRecorder* recorder)
{
    netAssert(this->IsInitialized());

    netAssert(channelId <= NET_MAX_CHANNEL_ID);

    NET_CS_SYNC(m_Cs);
	
	if(!m_Channels[channelId].m_PktRecorders.empty())
	{
		m_Channels[channelId].m_PktRecorders.erase(recorder);
	}
}

bool
netConnectionManager::Send(const int cxnId,
                            const void* bytes,
                            const unsigned numBytes,
                            const unsigned sendFlags,
                            netSequence* frameSeq)
{
	PROFILE

    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    bool success = false;

    rtry
    {
        Cxn* cxn = this->GetCxnById(cxnId);

        // closed connections retain the connection Id
        rverify(cxn,
                catchall,
                netError("Send :: Invalid CxnId: %08x", cxnId));

        // being open or pending open is a requirement to getting data out
        rcheck(cxn->IsOpen() || cxn->IsPendingOpen(),
                catchall,
                netWarning("Send :: Connection in invalid state, CxnId: %08x", cxnId));

		if((sendFlags & NET_SEND_RELIABLE)
			|| !this->CheckExceededBandwidth(cxn->GetChannelId()))
		{
			success = cxn->QueueFrame(bytes, numBytes, sendFlags, frameSeq);

			if(!success && (sendFlags & NET_SEND_RELIABLE))
			{
				//Try to reclaim some mem and send again.
				this->ReclaimMem();

				success = cxn->QueueFrame(bytes, numBytes, sendFlags, frameSeq);

				//We really want to know if we failed to send a reliable frame.
				netAssert(success);
			}

			if(success && (sendFlags & NET_SEND_IMMEDIATE))
			{
				netEndpoint* ep = cxn->m_EpOwner;
				netAssert(ep);

				ep->m_PackTimeout = 0;
				if(m_Worker.m_SendSema)
				{
					sysIpcSignalSema(m_Worker.m_SendSema);
				}
			}
        }
    }
    rcatchall
    {
    }

	// once the game sends us a pre-allocated out frame, we own it. If we fail to queue it, free it here
	if(!success)
	{
		if(sendFlags & NET_SEND_OUT_FRAME)
		{
			netOutFrame* outfrm = (netOutFrame*)bytes;
			if(outfrm)
			{
				this->FreeOutFrame(outfrm);
				outfrm = NULL;
			}
		}
	}

    //Always return true for unreliable frames.
    return success || !(sendFlags & NET_SEND_RELIABLE);
}

bool
netConnectionManager::SendOutOfBand(const netAddress& addr,
                                    const unsigned channelId,
                                    const void* bytes,
                                    const unsigned numBytes,
                                    const unsigned sendFlags)
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    return this->SendConnectionless(addr, channelId, bytes, numBytes, sendFlags);
}

void
netConnectionManager::SendRemoteTimeoutRequest(const EndpointId epId, const ChannelId channelId, const netConnection::TimeoutReason timeoutReason)
{
	const netEndpoint* ep = this->GetEndpointById(epId);
	if(!ep)
	{
		return;
	}

	netDebug("*.%d[" NET_ADDR_FMT "]: Sending MsgCxnRequestRemoteTimeout", channelId, NET_ADDR_FOR_PRINTF(ep->GetAddress()));

	// initialize message
	MsgCxnRequestRemoteTimeout msg(channelId, timeoutReason);

	// always send directly to the socket and avoid trying to queue. This message is send during a stall,
	// when we are most likely to run out of memory with backed up queues and we won't have memory to
	// queue the frame. Also, on suspension, we get terminated after the suspension callback so there
	// is no time to process the queue after the callback completes.
	this->SendReservedChannelPacket(ep->GetAddress(), msg, nullptr);
}

#if 0
void
netConnectionManager::SendRemoteTimeoutRequestToAllEndpoints(const netConnection::TimeoutReason timeoutReason)
{
    NET_CS_SYNC(m_Cs);

	// TODO: NS - could send these out of band (cxnless?)

    EndpointList::iterator itEp = m_ActiveEndpoints.begin();
    EndpointList::const_iterator itEpStop = m_ActiveEndpoints.end();
    for (; itEpStop != itEp; ++itEp)
    {
        netEndpoint* ep = *itEp;

        if (ep)
        {
			// this->SendRemoteTimeoutRequest(ep->GetId(), timeoutReason);
        }
    }
}
#endif

const netAddress&
netConnectionManager::GetAddress(const EndpointId epId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    const netEndpoint* ep = this->GetEndpointById(epId);

    return ep ? ep->GetAddress() : netAddress::INVALID_ADDRESS;
}

const netAddress&
netConnectionManager::GetAddress(const netPeerId& peerId) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetActiveEndpointByPeerId(peerId);

	return ep ? ep->GetAddress() : netAddress::INVALID_ADDRESS;
}

const netAddress&
netConnectionManager::GetRelayAddress(const EndpointId epId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetEndpointById(epId);
	return ep ? ep->m_RelayAddr : netAddress::INVALID_ADDRESS;
}

const netAddress&
netConnectionManager::GetRelayAddress(const netPeerId& peerId) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetActiveEndpointByPeerId(peerId);
	return ep ? ep->m_RelayAddr : netAddress::INVALID_ADDRESS;
}

const netPeerId&
netConnectionManager::GetPeerId(const EndpointId epId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetEndpointById(epId);
	return ep ? ep->GetPeerId() : netPeerId::INVALID_PEER_ID;
}

const netPeerAddress&
netConnectionManager::GetPeerAddress(const EndpointId epId) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetEndpointById(epId);
	return ep ? ep->GetPeerAddress() : netPeerAddress::INVALID_PEER_ADDRESS;
}

u8
netConnectionManager::GetHopCount(const EndpointId epId) const
{
	NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetEndpointById(epId);
	return ep ? ep->GetHopCount() : 0;
}

unsigned 
netConnectionManager::GetNumForwardedConnections() const
{
	return m_Router.GetNumForwardedConnections();
}

netEndpoint::EndpointQoS
netConnectionManager::GetQoS(const EndpointId epId, netEndpoint::EndpointQosReason& reason)
{
	NET_CS_SYNC(m_Cs);

	reason = netEndpoint::EndpointQosReason::QOS_REASON_UNKNOWN;

	// Note that these are heuristics. They can provide a hint about the
	// apparent quality of the connection between two peers.

	// can be configured via tunable. If configured to 0
	// then this system is disabled and we always report QOS_GOOD.
	u32 thresholdMs = netEndpoint::sm_BadQosThresholdMs;

#if !RSG_FINAL
	// override if command line is enabled
	PARAM_netBadQosThresholdMs.Get(thresholdMs);
#endif

	if(thresholdMs == 0)
	{
		return netEndpoint::EndpointQoS::QOS_GOOD;
	}

	const netEndpoint* ep = this->GetEndpointById(epId);

	if(ep)
	{
		// if we haven't received anything from this ep for more than the threshold
		const u32 curTime = sysTimer::GetSystemMsTime();
		if((ep->m_LastReceiveTime > 0) &&
			(curTime > ep->m_LastReceiveTime) &&
			((curTime - ep->m_LastReceiveTime) > thresholdMs))
		{
			netDebug1("GetQoS :: time since last receive: %u ms is greater than the threshold: %u", curTime - ep->m_LastReceiveTime, thresholdMs);
			reason = netEndpoint::EndpointQosReason::QOS_REASON_LONG_TIME_SINCE_LAST_RECEIVE;
			return netEndpoint::EndpointQoS::QOS_BAD;
		}

		// if the socket's receive buffer is backed up
		netSocketManager* sktMgr = GetSocketManager();
		if(sktMgr)
		{
			unsigned rcvBufQueueLen = 0;
			unsigned rcvBufSize = 0;
			if(sktMgr->GetReceiveBufferInfo(ep->GetAddress(), rcvBufQueueLen, rcvBufSize))
			{
				const float rcvBufQueuePct = (rcvBufSize > 0) ? ((float)rcvBufQueueLen / (float)rcvBufSize) : 0.0f;
				const float rcvBufPctThreshold = netEndpoint::DEFAULT_BAD_QOS_RCV_QUEUE_PCT / 100.0f;
				if(rcvBufQueuePct >= rcvBufPctThreshold)
				{
					netDebug1("GetQoS :: receive buffer %u / %u bytes is greater than the threshold: %d%%", rcvBufQueueLen, rcvBufSize, netEndpoint::DEFAULT_BAD_QOS_RCV_QUEUE_PCT);
					reason = netEndpoint::EndpointQosReason::QOS_REASON_SOCKET_RCV_QUEUE;
					return netEndpoint::EndpointQoS::QOS_BAD;
				}
			}
		}
		
		CxnList::const_iterator it = ep->m_CxnList.begin();
		CxnList::const_iterator stop = ep->m_CxnList.end();
		for(; stop != it; ++it)
		{
			const Cxn* cxn = (*it);
			if(cxn->IsOpen())
			{
				// if the cxn is timed out
				if(cxn->IsTimedOut())
				{
					reason = netEndpoint::EndpointQosReason::QOS_REASON_CXN_TIMED_OUT;
					return netEndpoint::EndpointQoS::QOS_BAD;
				}

				// if the cxn is out of memory
				if(cxn->IsOutOfMemory())
				{
					reason = netEndpoint::EndpointQosReason::QOS_REASON_CXN_MEMORY;
					return netEndpoint::EndpointQoS::QOS_BAD;
				}

				// if a reliable frame has been unacked for longer than the threshold
				if(cxn->GetAgeOfOldestUnackedFrame() > thresholdMs)
				{
					netDebug1("GetQoS :: age of oldest unacked frame: %u ms is greater than the threshold: %u", cxn->GetAgeOfOldestUnackedFrame(), thresholdMs);
					reason = netEndpoint::EndpointQosReason::QOS_REASON_OLD_UNACKED_FRAME;
					return netEndpoint::EndpointQoS::QOS_BAD;
				}

				// if RTT has been above the specified limit for more than the threshold and
				// the most recent ack was received fairly recently and was still above the limit.
				// For example, if our RTT has been above 500ms for 4+ seconds AND we have continued
				// to observe high a RTT in the last 2 seconds, consider the QoS to be poor.
				// We want to avoid reporting a poor QoS if we haven't received an ack for a long
				// time, since we won't know the current RTT (they are based on acks to reliable frames).
				const u32 startTime = cxn->GetRttOverThresholdStartTime();
				if(startTime > 0)
				{
					if((curTime - startTime) > thresholdMs)
					{
						const u32 lastTime = cxn->GetRttOverThresholdLastTime();
						if((curTime - lastTime) < (thresholdMs / 2))
						{
							netDebug1("GetQoS :: time RTT has been over the warn limit: %u ms is greater than the threshold: %u", curTime - startTime, thresholdMs);
							reason = netEndpoint::EndpointQosReason::QOS_REASON_HIGH_RTT;
							return netEndpoint::EndpointQoS::QOS_BAD;
						}
					}
				}
			}
		}
	}

	return netEndpoint::EndpointQoS::QOS_GOOD;
}

netConnectionManager::OverallQoS netConnectionManager::GetOverallQoS(netConnectionManager::OverallQosReason& reason) const
{
	NET_CS_SYNC(m_Cs);

	reason = netConnectionManager::OverallQosReason::QOS_REASON_UNKNOWN;

	// Note that these are heuristics. They can provide a hint about the
	// connection manager's overall health.

	// if disabled via tunable then always report QOS_GOOD.
	if(!sm_EnableOverallQos)
	{
		return netConnectionManager::OverallQoS::QOS_GOOD;
	}

	// if any channel is exceeding bandwidth
	for(unsigned chId = 0; chId < NET_MAX_CHANNELS; ++chId)
	{
		if(m_Channels[chId].m_BandwidthExceeded)
		{
			netDebug1("GetOverallQoS :: bandwidth exceeded on channel %u", chId);
			reason = netConnectionManager::OverallQosReason::QOS_REASON_BANDWIDTH;
			return netConnectionManager::OverallQoS::QOS_BAD;
		}
	}

	// if we're low on memory
	if(m_Allocator)
	{
		const size_t heapSize = m_Allocator->GetHeapSize();
		if(heapSize > 0)
		{
			const size_t memAvail = m_Allocator->GetMemoryAvailable();
			const float pctAvail = (float)memAvail / (float)heapSize;
			const float pctThreshold = DEFAULT_BAD_QOS_MEM_AVAILABLE_PCT / 100.0f;
			if((pctThreshold > 0.0f) && (pctAvail <= pctThreshold))
			{
				netDebug1("GetOverallQoS :: available memory: %" SIZETFMT "u bytes is below threshold of %d%%", memAvail, DEFAULT_BAD_QOS_MEM_AVAILABLE_PCT);
				reason = netConnectionManager::OverallQosReason::QOS_REASON_MEMORY;
				return netConnectionManager::OverallQoS::QOS_BAD;
			}
		}
	}

	return netConnectionManager::OverallQoS::QOS_GOOD;
}

#if NET_CXNMGR_COLLECT_STATS
const netEndpoint::Statistics 
netConnectionManager::GetStats(const EndpointId epId)
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	netEndpoint::Statistics snapshot;

	netEndpoint* ep = this->GetEndpointById(epId);
	if(!ep)
	{
		return snapshot;
	}

	snapshot = ep->GetStats();
	return snapshot;
}
#endif

unsigned
netConnectionManager::GetUnAckedCount(const int cxnId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    const Cxn* cxn = this->GetCxnById(cxnId);

    return cxn ? cxn->GetUnAckedCount() : 0;
}

unsigned
netConnectionManager::GetOldestResendCount(const int cxnId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    const Cxn* cxn = this->GetCxnById(cxnId);

    return cxn ? cxn->GetOldestResendCount() : 0;
}

netSequence
netConnectionManager::GetSequenceOfOldestResendCount(const int cxnId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    const Cxn* cxn = this->GetCxnById(cxnId);

    return cxn ? cxn->GetSequenceOfOldestResendCount() : 0;
}

unsigned
netConnectionManager::GetNewQueueLength(const int cxnId) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const Cxn* cxn = this->GetCxnById(cxnId);

	return cxn ? cxn->GetNewQueueLength() : 0;
}

unsigned
netConnectionManager::GetResendQueueLength(const int cxnId) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const Cxn* cxn = this->GetCxnById(cxnId);

	return cxn ? cxn->GetResendQueueLength() : 0;
}

unsigned
netConnectionManager::GetAgeOfOldestUnackedFrame(const EndpointId epId) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = this->GetEndpointById(epId);
	if(!ep)
	{
		return 0;
	}

	unsigned ageOfOldestUnackedFrame = 0;
	CxnList::const_iterator it = ep->m_CxnList.begin();
	CxnList::const_iterator stop = ep->m_CxnList.end();
	for(; stop != it; ++it)
	{
		const Cxn* cxn = (*it);
		if(cxn->IsOpen())
		{
			ageOfOldestUnackedFrame = rage::Max(ageOfOldestUnackedFrame, cxn->GetAgeOfOldestUnackedFrame());
		}
	}

	return ageOfOldestUnackedFrame;
}

bool
netConnectionManager::ResetPackTimeout(const EndpointId epId)
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	netEndpoint* ep = this->GetEndpointById(epId);
	if(!ep)
	{
		return false;
	}

	ep->m_PackTimeout = ep->m_SendInterval;

	return true;
}

void
netConnectionManager::AddChannelDelegate(Delegate* dlgt, const ChannelId channelId)
{
    netAssert(this->IsInitialized());

	netAssert(dlgt->IsBound());

    NET_CS_SYNC(m_CsDlgt);

	if(netVerify(NET_IS_VALID_CHANNEL_ID(channelId)))
	{
		netAssert(NULL == dlgt->m_Owner);

		m_Channels[channelId].AddDelegate(dlgt);
		//this->AddDelegate(dlgt);

		dlgt->m_Owner = this;
	}
}

void
netConnectionManager::RemoveChannelDelegate(Delegate* dlgt)
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_CsDlgt);

    if(netVerify(NET_IS_VALID_CHANNEL_ID(dlgt->m_ChannelId)))
	{
		netAssert(this == dlgt->m_Owner);

		m_Channels[dlgt->m_ChannelId].RemoveDelegate(dlgt);
		dlgt->m_Owner = NULL;
	}
}

bool
netConnectionManager::Subscribe(netEventConsumer<netEvent>* consumer)
{
    if(netVerify(IsInitialized()))
    {
        return m_EventQ.Subscribe(consumer);
    }

    return false;
}

bool
netConnectionManager::Unsubscribe(netEventConsumer<netEvent>* consumer)
{
    if(netVerify(IsInitialized()))
    {
        m_EventQ.Unsubscribe(consumer);
        return true;
    }

    return false;
}

unsigned
netConnectionManager::GetActiveConnectionCount() const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

	unsigned count = 0;
	EndpointList::const_iterator it = m_ActiveEndpoints.begin();
	EndpointList::const_iterator stop = m_ActiveEndpoints.end();
	for(; stop != it; ++it)
	{
		count += (unsigned)(*it)->m_CxnList.size();
	}

	return count;
}

unsigned
netConnectionManager::Pack(netAddress* recipient,
                            void* buf,
                            const unsigned sizeofBuf)
{
	PROFILE

    netAssert(this->IsInitialized());

#if !__NO_OUTPUT
	unsigned nTopTime = sysTimer::GetSystemMsTime();
	static unsigned nBottomTime = 0;
	static unsigned nSendFrame = m_SendFrame; 
	const unsigned nElapsedTime = (nBottomTime > 0) ? (nTopTime - nBottomTime) : 0;

	// log when the time between updates is more than LONG_FRAME_TIME ms.
	// this will also fire for 'known' stalls such breakpoints, adding a bug 
	// or warping the player but it's only a log entry
	static const unsigned STALL_THRESHOLD = 500;
	if(nElapsedTime > STALL_THRESHOLD && (nSendFrame == m_SendFrame))
	{
		netDebug1("NetStallDetect :: Pack - Stall of %ums (Current: %ums, Last: %ums)", nElapsedTime, nTopTime, nBottomTime);
	}

	nSendFrame = m_SendFrame;

	// take a snapshot of the time prior to requesting the lock
	const unsigned nPreLockTimestamp = sysTimer::GetSystemMsTime();
#endif 

	NET_CS_SYNC(m_Cs);

#if !__NO_OUTPUT
	// take a snapshot of the time after acquiring the lock
	const unsigned nPostLockTimestamp = sysTimer::GetSystemMsTime();
#endif 

    unsigned numBytes = 0;

    if(netVerify(sizeofBuf > netPacketStorage::MAX_SIZEOF_HEADER
                                + netPacket::BYTE_SIZEOF_HEADER
                                + netBundle::BYTE_SIZEOF_HEADER
								+ MAX_SIZEOF_CRYPTO_OVERHEAD))
    {
        //Process endpoints

		// TODO: NS - change encrypted -> compressible, plaintext -> incompressible
        netPacketStorage* storagePkt = (netPacketStorage*) buf;
        netPacket* pkt = storagePkt->GetPacket();
        u8* encryptedBase = (u8*) pkt->GetPayload();
        u8* encrypted = encryptedBase;
        u8 plaintextBase[netPacket::MAX_BYTE_SIZEOF_PAYLOAD];
        u8* plaintext = plaintextBase;
        unsigned capacity = netPacket::MAX_BYTE_SIZEOF_PAYLOAD;

		int epCount = (int) m_ActiveEndpoints.size();

		//Iterate over endpoints until we get a payload. Each endpoint represents a single remote peer.
		while(epCount-- && !numBytes)
		{
			netEndpoint* ep = m_ActiveEndpoints.front();
			
			if(!netVerifyf(ep, "Invalid endpoint for pack."))
			{
				break; 
			}

			const unsigned currentTime = sysTimer::GetSystemMsTime();
			const unsigned timeStep = (ep->m_LastTime > 0) ? (currentTime - ep->m_LastTime) : 0;
			ep->m_LastTime = currentTime;

			//If the endpoint has been inactive for too long, then free it.
            //If we didn't do this, a remote host that goes down would never be
            //able to reconnect after we've established a tunnel for them.
			NOTFINAL_ONLY(if(!m_DisableInactiveEndpointRemoval))
			{
				OUTPUT_ONLY(int prevTimeout = ep->m_Timeout);

				ep->m_Timeout -= timeStep;
				
#if !__NO_OUTPUT
				// log out when we cross a second barrier, but only if it's at least 1 second after a reset to reduce spam
				if((prevTimeout / 1000) != (ep->m_Timeout / 1000) && (ep->m_Timeout < int(netEndpoint::sm_InactivityTimeoutMs - 1000)))
				{
					netDebug3("netEndpoint: " NET_ADDR_FMT " will time out in %dms. TimeStep: %dms", NET_ADDR_FOR_PRINTF(ep->GetAddress()), ep->m_Timeout, timeStep);
				}
#endif			

				if(ep->m_Timeout <= 0)
                {
					netDebug("Destroying inactive endpoint: " NET_ADDR_FMT " (%s). Timestep: %dms", NET_ADDR_FOR_PRINTF(ep->GetAddress()), ep->GetPeerAddress().ToString(), timeStep);
					const size_t count = ep->m_CxnList.size();

					for(int i = 0; i < (int) count; ++i)
					{
						netAssert(ep->m_CxnList.size() == count - i);
						this->CloseConnection(*ep->m_CxnList.begin(), NET_CLOSE_FINALLY);
					}
					this->DestroyEndpoint(ep);
					continue;
                }
            }

            //For fairness move the endpoint to the back of the queue.
            m_ActiveEndpoints.pop_front();
            m_ActiveEndpoints.push_back(ep);

            //Is it time to pack data for this endpoint?
            ep->m_PackTimeout -= timeStep;
            if(ep->m_PackTimeout > 0)
            {
                netDebug3("Skipping pack for endpoint: " NET_ADDR_FMT ". Pack timeout: %d",
                    NET_ADDR_FOR_PRINTF(ep->GetAddress()),
                    ep->m_PackTimeout);
				continue;
            }

			//Track which channels we sent data on
			bool hasSentDataOnChannel[NET_MAX_CHANNELS] = {};

            if(!ep->m_CxnList.empty())
            {
                int cxnCount = (int) ep->m_CxnList.size();

                while(cxnCount-- && capacity >= netBundle::BYTE_SIZEOF_HEADER)
                {
                    Cxn* cxn = ep->m_CxnList.front();

                    //For fairness move the cxn to the back of the queue.
                    ep->m_CxnList.pop_front();
                    ep->m_CxnList.push_back(cxn);

                    netAssert(!cxn->IsClosed());

					if(cxn->IsPendingClose() && cxn->CanDestroy())
					{
						netDebug("%08x.%d[" NET_ADDR_FMT "]: Closing connection from Pack", cxn->GetId(), cxn->GetChannelId(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
						this->CloseConnection(cxn, NET_CLOSE_IMMEDIATELY);
						continue;
					}

                    if(cxn->IsOutOfMemory())
                    {
                        netWarning("%08x.%d[" NET_ADDR_FMT "]: Skipping pack due to Out of memory!", cxn->GetId(), cxn->GetChannelId(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
						this->QueueOutOfMemory(cxn, true, cxn->GetOutOfMemoryRequiredBytes());
                        continue;
                    }

                    if(cxn->IsTimedOut())
                    {
                        m_DispatchTimedOut = true;
					}

#if __BANK
					if(cxn->IsForcedTimeout())
					{
						netDebug("%08x.%d[" NET_ADDR_FMT "]: Skipping pack due to Forced time out!", cxn->GetId(), cxn->GetChannelId(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
						continue;
					}
#endif

                    const unsigned channelId = cxn->GetChannelId();
                    unsigned packLen = 0;
                    u8** payload;

                    if(m_Channels[channelId].m_Policies.m_CompressionEnabled)
                    {
                        payload = &encrypted;
                    }
                    else
                    {
                        payload = &plaintext;
                    }

                    //Check for excessive bandwidth
                    if(this->CheckExceededBandwidth(channelId))
                    {
						cxn->DropOutboundUnreliableFrames();

                        if(cxn->IsPendingOpen())
                        {
                            //We've exceeded our bandwidth but we're trying to
                            //establish the connection, so send anyway.

                            //FIXME(KB) - this could be very detrimental because
                            //we're continuing to use up bandwidth we don't have.
                            //It will now take longer to recover the bandwidth
                            //overrun.

                            //FIXME (KB) - try telling the connection to only send one frame.
							netWarning("%08x.%d[" NET_ADDR_FMT "]: Packing pending-open cxn exceeding bandwidth!", cxn->GetId(), cxn->GetChannelId(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
                            packLen = cxn->Pack(*payload, capacity);
                        }
                        else if(cxn->NeedToAck() || cxn->TimeForKeepalive())
                        {
                            //We've exceeded our bandwidth but we need to ACK
                            //or send a keepalive, so just send the bundle header.
							netWarning("%08x.%d[" NET_ADDR_FMT "]: Packing only bundle header due to bandwidth!", cxn->GetId(), cxn->GetChannelId(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
							packLen = cxn->Pack(*payload, netBundle::BYTE_SIZEOF_HEADER);
                        }
                        else
                        {
							netWarning("%08x.%d[" NET_ADDR_FMT "]: Skipping pack due to bandwidth!", cxn->GetId(), cxn->GetChannelId(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
							continue;
                        }
                    }
                    else
                    {
						//Pack up frames that need to be sent.
                        packLen = cxn->Pack(*payload, capacity);
                    }

                    netAssert(capacity >= packLen);

                    //Call the packet recorders for the channel.
                    m_Channels[channelId].RecordOutbound(ep->GetAddress(), *payload, packLen);

                    *payload += packLen;
                    capacity -= packLen;
                    numBytes += packLen;

					//Track whether we sent data on this channel
					hasSentDataOnChannel[cxn->GetChannelId()] = (packLen > 0);
                }
            }
			
			// reset the keep alive if we sent anything via a connection
			const bool resetKeepAlive = (numBytes > 0);

            //Pack up connectionless frames destined for this address.
            //Note the Pack() function called here checks for bandwidth overruns.
            unsigned numEncryptedBytes = 0;
            unsigned numPlaintextBytes = 0;
            const unsigned cxnlessLen =
                this->PackConnectionless(ep->GetAddress(),
                                        &ep->m_CxnlessQ,
                                        encrypted,
                                        &numEncryptedBytes,
                                        plaintext,
                                        &numPlaintextBytes,
                                        capacity);

            netAssert((numEncryptedBytes + numPlaintextBytes) == cxnlessLen);
            netAssert(cxnlessLen <= capacity);

            encrypted += numEncryptedBytes;
            plaintext += numPlaintextBytes;
            capacity -= cxnlessLen;
            numBytes += cxnlessLen;

            // only reset the pack timeout for the endpoint once we have no more data left to pack but
            // still space to pack more - this allows data to coalesce into bigger packets within the
            // send interval without limiting the number of packets that can be sent each send frame
            if(numBytes == 0)
            {
#if !__NO_OUTPUT
                if(!ep->m_CxnList.empty())
                {
                    Cxn *cxn = ep->m_CxnList.front();

                    while(cxn)
                    {
						bool forceTimeout = false;

#if __BANK
						forceTimeout = cxn->IsForcedTimeout();
#endif

                        if(!m_Channels[cxn->GetChannelId()].m_BandwidthExceeded && cxn->IsOpen() && cxn->GetNewQueueLength()!=0 && !cxn->IsTimedOut() && !forceTimeout)
                        {
                            netAssert(!"No bytes packed but new packets queued!");
                            netWarning("%08x.%d[" NET_ADDR_FMT "]: No bytes packed but new packets queued!", cxn->GetId(), cxn->GetChannelId(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
                        }

                        cxn = cxn->m_ListLink.m_next;
                    }
                }
#endif // !__NO_OUTPUT

                ep->m_PackTimeout = (int) ep->m_SendInterval;
            }

            pkt->ResetHeader(ptrdiff_t_to_int(encrypted - encryptedBase), 0);

            if(numBytes > 0)
            {
                netAssert(storagePkt->GetPacket() == pkt);

                numBytes += netPacket::BYTE_SIZEOF_HEADER;

                ep->m_Timeout = netEndpoint::sm_InactivityTimeoutMs;
				
				netDebug3("Resetting timeout for endpoint: " NET_ADDR_FMT " for pack. Resetting Keep Alives: %s",
					NET_ADDR_FOR_PRINTF(ep->GetAddress()),
					resetKeepAlive ? "True" : "False");

                *recipient = ep->GetAddress();

                //Reset keepalive timers for all connections on this endpoint.
                if(!ep->m_CxnList.empty())
                {
                    CxnList::iterator it = ep->m_CxnList.begin();
                    CxnList::const_iterator stop = ep->m_CxnList.end();
                    for(; stop != it; ++it)
                    {
						Cxn* cxn = (*it);
						if(hasSentDataOnChannel[cxn->GetChannelId()] || (resetKeepAlive && !m_Channels[cxn->GetChannelId()].m_Policies.m_AlwaysMaintainKeepAlive))
						{
							cxn->ResetKeepalive();
						}
                    }
                }
			}
        }

        //Append the plaintext to the encrypted.
        const unsigned sizeofPlaintext = ptrdiff_t_to_int(plaintext - plaintextBase);

        if(sizeofPlaintext > 0)
        {
            netAssert(encrypted + sizeofPlaintext <= &((u8*)buf)[sizeofBuf - 1]);
            sysMemCpy(encrypted, plaintextBase, sizeofPlaintext);
        }
	}

#if !__NO_OUTPUT
	nBottomTime = sysTimer::GetSystemMsTime();
	const unsigned nSendTime = (nBottomTime - nTopTime);
	if(nSendTime > STALL_THRESHOLD)
		netWarning("NetStallDetect :: Pack - Send time of %ums (Top: %ums, Bottom: %ums), LockWait: %u", nSendTime, nTopTime, nBottomTime, nPostLockTimestamp - nPreLockTimestamp);
#endif

    return numBytes;
}

bool 
netConnectionManager::IsTunnelingPacket(netEndpoint* UNUSED_PARAM(ep),
										const void* buf,
										const unsigned sizeofBuf)
{
	// for security purposes, we want to return true if and only if this
	// buffer contains a tunneling packet, and nothing else.

	// Note: the cxnRelayAddrChangedMsg msg is also sent as a 'tunneling packet' but on the reserved channel

	rtry
	{
		rverify(buf, catchall, );
		rverify(sizeofBuf > 0, catchall, );

		const netPacket* pkt = (netPacket*) buf;

		const u8* eob = &((u8*)buf)[sizeofBuf];         //End of buffer
		const u8* eop = &((u8*)pkt)[pkt->GetSize()];    //End of packet
		const unsigned sizeofPlaintext = ptrdiff_t_to_int(eob - eop); //plaintext bytes start after encrypted bytes
		rcheck(sizeofPlaintext == 0, catchall, netError("IsTunnelingPacket :: sizeofPlaintext: %u, expected 0", sizeofPlaintext)); // make sure there is no plaintext segment
		rcheck((NET_PACKETFLAG_COMPRESSED & pkt->GetFlags()) == 0, catchall, netError("IsTunnelingPacket :: packet should not be compressed"));

		const u8* p = (u8*)pkt->GetPayload();
		const u8* eobndl = p + pkt->GetSizeofPayload();

		const netBundle* b = (const netBundle*) p;
		const unsigned bufSize = unsigned(eobndl - p);
		rcheck(bufSize >= netBundle::BYTE_SIZEOF_HEADER, catchall, netError("IsTunnelingPacket :: bufSize: %u < netBundle::BYTE_SIZEOF_HEADER", bufSize));
		rcheck(bufSize <= netBundle::MAX_BYTE_SIZEOF_BUNDLE, catchall, netError("IsTunnelingPacket :: bufSize: %u > netBundle::MAX_BYTE_SIZEOF_BUNDLE", bufSize));
		rcheck(b->GetSize() == bufSize, catchall, netError("IsTunnelingPacket :: bufSize: %u != b->GetSize(): %u", bufSize, b->GetSize())); // make sure there is only 1 bundle
		rcheck((b->GetChannelId() == NET_TUNNELER_CHANNEL_ID) || (b->GetChannelId() == NET_RESERVED_CHANNEL_ID), catchall, netError("IsTunnelingPacket :: packet arrived over incorrect channel. b->GetChannelId(): %d", b->GetChannelId()));
		rcheck(netBundle::FLAG_OUT_OF_BAND & b->GetFlags(), catchall, netError("IsTunnelingPacket :: packet was not sent out-of-band")); // tunneling packets are sent connectionless
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netConnectionManager::HandlePeerRelayForwardPacket(const netSocketAddress& OUTPUT_ONLY(sender),
													netPeerRelayPacket* relayPkt)
{
	PROFILE

	rtry
	{
		netDebug2("Received a P2P_FORWARD from [" NET_ADDR_FMT "]", NET_ADDR_FOR_PRINTF(sender));

		// packets to be forwarded are sent immediately. They have already waited a pack interval at the
		// source peer, and incur additional latency as they route through each hop to the destination.

		rverifyall(m_SocketManager);
		netSocketError sktErr = NET_SOCKERR_NONE;
		
		netAddress nextHopAddr;
		bool success = m_Router.PrepareToForward(relayPkt, &nextHopAddr);
		rcheck(success && nextHopAddr.IsValid(), catchall, netWarning("HandlePeerRelayForwardPacket :: PrepareToForward failed, dropping packet"));

		const unsigned sizeofRelayPkt = relayPkt->GetSize();
		netAssert(sizeofRelayPkt == (relayPkt->GetSizeofPayload() + netPeerRelayPacket::SIZEOF_P2P_HEADER));

		// encrypt entire relay packet (including header) with the next hop peer's key
		u8 data[netSocket::MAX_BUFFER_SIZE];
		unsigned int sizeOfRelayData = sizeof(data);
		success = P2pEncrypt(nextHopAddr, relayPkt, sizeofRelayPkt, data, sizeOfRelayData, nullptr);
		rverify(success, catchall, netError("Error encrypting data for p2p terminus [" NET_ADDR_FMT "]: P2pEncrypt failed", NET_ADDR_FOR_PRINTF(nextHopAddr)))

		const netSocketAddress& dstAddr = nextHopAddr.GetTargetAddress();
		netAssert(dstAddr.IsValid());

		if(!m_SocketManager->Send(dstAddr, data, sizeOfRelayData, &sktErr))
		{
			//Ignore SOCKERR_CONNECTION_RESET.
			if(NET_SOCKERR_CONNECTION_RESET == sktErr)
			{
				sktErr = NET_SOCKERR_NONE;
			}
		}

		// Note: the packets we forward are encrypted, so we cannot determine the number of frames in the packet or which channels they're on.
		// Because of this, forwarded packets are not accounted for when calculating bandwidth limits, recording outbound packets, or p2pcxn stats.

		return true;
	}
	rcatchall
	{

	}

	return false;
}

bool
netConnectionManager::HandlePeerRelayTerminusPacket(const netSocketAddress& sender,
													netPeerRelayPacket* relayPkt)
{
    rtry
    {
		netDebug2("Received a P2P_TERMINUS from [" NET_ADDR_FMT "]", NET_ADDR_FOR_PRINTF(sender));

        rverify(relayPkt->GetType() == NET_RELAYPACKET_P2P_TERMINUS, catchall,);

		netAddress reverseNextHopAddr;
		bool success = m_Router.PrepareToReceiveTerminus(relayPkt, sender, &reverseNextHopAddr);
		rcheck(success && reverseNextHopAddr.IsValid(), catchall, netWarning("HandlePeerRelayTerminusPacket :: PrepareToReceiveTerminus failed, dropping packet"));

		void* payload = relayPkt->GetPayload();
		unsigned sizeofPayload = relayPkt->GetSizeofPayload();

		u8* buf = (u8*)payload;
		unsigned sizeofBuf = sizeofPayload;
		bool allowRelayOverride = false;
		rcheck(P2pDecrypt(reverseNextHopAddr,
							&buf,
							sizeofBuf,
							allowRelayOverride),
							catchall,
							netWarning("HandlePeerRelayTerminusPacket :: P2pDecrypt failed, dropping packet"));

		this->Unpack(reverseNextHopAddr, buf, sizeofBuf, allowRelayOverride);

		return true;
    }
    rcatchall
    {
		return false;
    }
}

bool
netConnectionManager::HandlePeerRelayPacket(const netSocketAddress& from,
											   void* pktBuf,
											   const int len)
{
    rtry
    {
        netDebug3("[" NET_ADDR_FMT "]:PKT<-,sz:%d",
                    NET_ADDR_FOR_PRINTF(from),
                    len);

        rverify(IsInitialized(),catchall,);

        rcheck(len > 0,catchall,);
		
		netPeerRelayPacket* relayPkt = static_cast<netPeerRelayPacket*>(pktBuf);

        rcheck(len >= netPeerRelayPacket::SIZEOF_P2P_HEADER
                && len <= (int)netPeerRelayPacket::MAX_SIZEOF_RELAY_PACKET,
                catchall,
                netDebug2("Packet size:%d is outside valid range:%d-%d",
                            len,
							netPeerRelayPacket::SIZEOF_P2P_HEADER,
                            (int)netPeerRelayPacket::MAX_SIZEOF_RELAY_PACKET));

        //GetSize() can return less than len if it contains plaintext.
        //The plain text portion of the packet comes after the encrypted portion.
        //See Xbox networking docs on the VDP packet format.
        rcheck((int)relayPkt->GetSize() <= len,
                catchall,
                netDebug2("Dropping packet with invalid size:%d - should be:%d.",
                            relayPkt->GetSize(),
                            len));

        rverify(relayPkt->IsRelayPacket(),
                catchall,
                netDebug2("Received unknown packet type"));

        rcheck(relayPkt->GetVersion() == netPeerRelayPacket::PROTOCOL_VERSION,
                catchall,
                netDebug2("Incompatible protocol version %d, expected %d",
                            relayPkt->GetVersion(),
							netPeerRelayPacket::PROTOCOL_VERSION));

        if(relayPkt->IsP2pForward())
        {
			// we're an intermediate peer that will forward a packet to another peer.
			HandlePeerRelayForwardPacket(from, relayPkt);
        }
        else if(relayPkt->IsP2pTerminus())
        {
			// we're the final recipient of a peer relayed P2P packet.
			HandlePeerRelayTerminusPacket(from, relayPkt);
        }
        else
        {
            netDebug2("Invalid relay packet received from [" NET_ADDR_FMT "]",
                    NET_ADDR_FOR_PRINTF(from));
        }

		return true;
    }
    rcatchall
    {

    }

	return false;
}

void
netConnectionManager::HandleReceivedPacket(const netAddress& sender,
											const void* encodedBuf,
											const unsigned sizeofEncodedBuf)
{
	PROFILE

    rtry
	{
		u8* buf = (u8*)encodedBuf;
		unsigned sizeofBuf = sizeofEncodedBuf;

		// if the packet is encrypted, decrypt it using the key agreed upon
		// between the local peer and the sending peer
		const bool encryptDirect = true;
		const bool encryptRelay = !netRelay::IsUsingSecureRelays();
		const bool isRelay = sender.IsRelayServerAddr();
		const bool useP2pCrypt = (encryptRelay && isRelay) || (encryptDirect && !isRelay);
		bool allowRelayOverride = false;

		if(useP2pCrypt)
		{
			rcheck(P2pDecrypt(sender,
							  &buf,
							  sizeofBuf,
							  allowRelayOverride),
							  catchall,
							  netWarning("HandleReceivedPacket :: P2pDecrypt failed, dropping packet"));
		}

		const netPacket* pkt = (const netPacket*)buf;

		const unsigned pktSize = pkt->GetSize();

		//GetSize() can return less than sizeofBuf if it contains plaintext.
		//The plaintext portion of the packet comes after the encrypted portion.
		rcheck(pktSize <= sizeofBuf,
				catchall,
				netDebug2("Dropping invalid packet with invalid size:%d - should be:%d.",
							pktSize,
							sizeofBuf));

		if(sender.IsRelayServerAddr())
		{
			// from relay server - relay header has already been removed by relay.cpp
			this->Unpack(sender, buf, sizeofBuf, allowRelayOverride);
		}
		else if(pkt->IsRelayPacket())
		{
			// from peer relay network - either p2p forward or p2p terminus
			HandlePeerRelayPacket(sender.GetTargetAddress(), buf, sizeofBuf);
		}
		else
		{
			// from directly connected peer (non-relayed) - no relay header
			this->Unpack(sender, buf, sizeofBuf, allowRelayOverride);
		}
	}
	rcatchall
	{

	}
}

void
netConnectionManager::Unpack(const netAddress& sender,
                            const void* buf,
                            const unsigned sizeofBuf,
							const bool allowRelayOverride)
{
	PROFILE

    netAssert(this->IsInitialized());

#if !__NO_OUTPUT
	unsigned nTopTime = sysTimer::GetSystemMsTime();
	static unsigned nBottomTime = 0;
	static unsigned nRecvFrame = m_ReceiveFrame; 
	const unsigned nElapsedTime = (nBottomTime > 0) ? (nTopTime - nBottomTime) : 0;

	// log when the time between updates is more than LONG_FRAME_TIME ms.
	// this will also fire for 'known' stalls such breakpoints, adding a bug 
	// or warping the player but it's only a log entry
	static const unsigned STALL_THRESHOLD = 500;
	if(nElapsedTime > STALL_THRESHOLD && (nRecvFrame == m_ReceiveFrame))
		netDebug1("NetStallDetect :: Unpack - Stall of %ums (Current: %ums, Last: %ums)", nElapsedTime, nTopTime, nBottomTime);

	nRecvFrame = m_ReceiveFrame;

	unsigned nPreLockTimestamp = 0;
	unsigned nPostLockTimestamp = 0;
#endif 

    rtry
    {
		const netPacket* pkt = (const netPacket*) buf;

        rcheck(sizeofBuf >= netPacket::BYTE_SIZEOF_HEADER
                && sizeofBuf <= netPacket::MAX_BYTE_SIZEOF_PACKET,
                catchall,
                netDebug2("Packet size:%d is outside valid range:%d-%d",
                            sizeofBuf,
                            netPacket::BYTE_SIZEOF_HEADER,
                            netPacket::MAX_BYTE_SIZEOF_PACKET));

		const unsigned pktSize = pkt->GetSize();

        //GetSize() can return less than sizeofBuf if it contains plaintext.
        //The plaintext portion of the packet comes after the encrypted portion.
        rcheck(pktSize <= sizeofBuf,
                catchall,
                netDebug2("Dropping invalid packet with invalid size:%d - should be:%d.",
                            pktSize,
                            sizeofBuf));
	
         rcheck(!pkt->IsRelayPacket(),
                 catchall,
                 netDebug("We shouldn't be receiving relay packets!!!"));

        const u8* eob = &((u8*)buf)[sizeofBuf];         //End of buffer
        const u8* eop = &((u8*)pkt)[pktSize];			//End of packet
        //Size of plaintext data is the size of the data after
        //the encrypted portion of the packet.
        const unsigned sizeofPlaintext = ptrdiff_t_to_int(eob - eop); //plaintext bytes start after encrypted bytes

        u8 tmpBuf[netPacket::MAX_BYTE_SIZEOF_PACKET];

        const unsigned pktFlags = pkt->GetFlags();

        //Decompress the packet if necessary.
        if(NET_PACKETFLAG_COMPRESSED & pktFlags)
        {
            pkt = this->DecompressPacket(pkt, tmpBuf, sizeof(tmpBuf));
            rcheck(pkt, catchall, netError("Error decompressing packet"));
        }

#if !__NO_OUTPUT
		// take a snapshot of the time prior to requesting the lock
		nPreLockTimestamp = sysTimer::GetSystemMsTime();
#endif 

		// Note: we are purposely entering the critical section *after* packet decryption and
		// decompression, which do not access endpoints or other shared memory. This allows
		// us to perform those heavy operations without blocking the main/send threads.
		NET_CS_SYNC(m_Cs);

#if !__NO_OUTPUT
		// take a snapshot of the time after acquiring the lock
		nPostLockTimestamp = sysTimer::GetSystemMsTime();
#endif 

		// get the endpoint associated with this sender address
        netEndpoint* ep = this->GetActiveEndpointByAddr(sender);

        if(!ep && sender.IsRelayServerAddr())
        {
            //We don't have an active endpoint for this sender.

            //We could be in the midst of tunneling to the sender so
            //check the pending list for a matching endpoint.
            //Note that pending endpoints don't yet have a direct
            //address to the remote peer, so we'll be checking the
            //relay addresses for a match.
            if((ep = this->GetPendingEndpointByAddr(sender)) != NULL)
            {
				if(allowRelayOverride)
				{
					netDebug("Received relay packet from [" NET_ADDR_FMT "] for pending endpoint - promoting to active endpoint",
						NET_ADDR_FOR_PRINTF(sender));
					m_Tunneler.OverrideRequest(ep->m_TunnelRqst, sender);
				}
            }
			else if((ep = this->GetActiveEndpointByRelayAddr(sender)) != NULL)
			{
				if(allowRelayOverride && ep->CanReassignOnRelayPacket())
				{
					//The sender could have switched to using the relay,
					//which would mean we no longer recognize the sender address.
					//In that case search the active endpoints for one
					//with a matching relay address.  If found switch the endpoint
					//to use the relay.
					netDebug("Received relay packet from [" NET_ADDR_FMT "] for existing endpoint [" NET_ADDR_FMT "] - switching endpoint to relay",
						NET_ADDR_FOR_PRINTF(sender), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
					this->ReassignEndpoint(ep, sender, ep->m_RelayAddr);
				}
				else
				{
					netDebug("Received relay packet from [" NET_ADDR_FMT "] for existing endpoint [" NET_ADDR_FMT "] - can't reassign due to disallowing relay override",
						NET_ADDR_FOR_PRINTF(sender), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
				}
			}
        }

        bool resetEndpointTimeout = false;
		bool resetConnectionTimeouts = false;

        struct Payload
        {
            const u8* p;
            const u8* eob;
        };

        Payload payloads[2] = {{0}};

        //Encrypted
        if(pkt)
        {
            payloads[0].p = (const u8*) pkt->GetPayload();
            payloads[0].eob = payloads[0].p + pkt->GetSizeofPayload();
        }

        //Plaintext
        if(sizeofPlaintext)
        {
            payloads[1].p = eop;    //plaintext bytes start after encrypted bytes
            payloads[1].eob = payloads[1].p + sizeofPlaintext;
        }

        for(int ip = 0; ip < 2; ++ip)
        {
            const u8* p = payloads[ip].p;
            const u8* eob = payloads[ip].eob;

            for(unsigned bsize = 0; p < eob; p += bsize)
            {
                const netBundle* b = (const netBundle*) p;

                const unsigned bufSize = unsigned(eob - p);

                if(bufSize < netBundle::BYTE_SIZEOF_HEADER)
                {
                    netDebug2("Buffer size:%d is too small to be a bundle",
                                bufSize);

                    break;
                }

                bsize = b->GetSize();

                if(bsize > bufSize)
                {
                     netDebug2("*[" NET_ADDR_FMT "]: Bundle size:%d exceeds buffer size:%u",
                                NET_ADDR_FOR_PRINTF(sender),
                                bsize,
                                unsigned(eob - p));
                     break;
                }

                if(bsize < netBundle::BYTE_SIZEOF_HEADER
                    || bsize > netBundle::MAX_BYTE_SIZEOF_BUNDLE)
                {
                    netDebug2("Bundle size:%d is outside valid range:%d-%d",
                                bsize,
                                netBundle::BYTE_SIZEOF_HEADER,
                                netBundle::MAX_BYTE_SIZEOF_BUNDLE);

                    break;
                }

                const unsigned channelId = b->GetChannelId();
                if(channelId > NET_MAX_CHANNEL_ID)
                {
                    netDebug2("Invalid channel id:%d in bundle", channelId);
                    break;
                }

                //Call the packet recorders for the channel.
                m_Channels[channelId].RecordInbound(sender, b, bsize);

				if(!(netBundle::FLAG_OUT_OF_BAND & b->GetFlags()))
				{
					Cxn* cxn = ep ? ep->m_Cxns[channelId] : NULL;

					if(!cxn)
					{
						const netFrame* frame = (const netFrame*) b->GetPayload();

						if(!this->IsListening(channelId))
						{
							netDebug2("*.%d[" NET_ADDR_FMT "]: Dropping packet for invalid connection.",
								channelId,
								NET_ADDR_FOR_PRINTF(sender));

							if(b->HasSyn())
							{
								netDebug2("    Did you forget to call StartListening()?");
							}
						}
						else if(!b->HasSyn())
						{
							netDebug2("*.%d[" NET_ADDR_FMT "]: Dropping non-SYN packet for invalid connection.",
								channelId,
								NET_ADDR_FOR_PRINTF(sender));
						}
						else if(b->GetSizeofPayload() < netFrame::BYTE_SIZEOF_RHEADER)
						{
							netDebug2("*.%d[" NET_ADDR_FMT "]: Dropping ill-formed SYN packet.",
								channelId,
								NET_ADDR_FOR_PRINTF(sender));
						}
						else if(!netFrame::Validate(frame, frame->GetSize()))
						{
							netDebug2("*.%d[" NET_ADDR_FMT "]: Dropping invalid frame from SYN packet.",
								channelId,
								NET_ADDR_FOR_PRINTF(sender));
						}
						else if(netVerify(this->QueueCxnRequested(sender,
							b->GetChannelId(),
							frame)))
						{
							resetEndpointTimeout = true;
							resetConnectionTimeouts = true;
						}
					}
                    else if(cxn->IsClosed())
                    {
                        netDebug2("*.%d[" NET_ADDR_FMT "]: Dropping bundle received on closed connection",
                                    channelId,
                                    NET_ADDR_FOR_PRINTF(sender));
                    }
                    else if(b->HasTerm())
                    {
						OUTPUT_ONLY(const netFrame* frame = (const netFrame*) b->GetPayload();)

						netDebug2("%08x.%d[" NET_ADDR_FMT "]: Connection terminated by remote peer :: [%u/%u] FRM(%s)[%s]<-,sz:%d",
									cxn->GetId(),
									channelId,
									NET_ADDR_FOR_PRINTF(sender),
									this->GetReceiveFrame(),
									this->GetUnpackFrame(),
									frame->IsReliable() ? "R" : "U",
									netMessage::IsMessage(frame->GetPayload(), frame->GetSizeofPayload())
									? netMessage::GetName(frame->GetPayload(), frame->GetSizeofPayload())
									: "",
									frame->GetSize());

                        const size_t requiredBytes = sizeof(netEventConnectionError);
                        netEventConnectionError* e =
                            (netEventConnectionError*) m_Allocator->RAGE_LOG_ALLOCATE(requiredBytes, 0);

                        if(netVerify(e))
                        {
                            new(e) netEventConnectionError(cxn->GetId(),
															cxn->m_EpOwner ? cxn->m_EpOwner->GetId() : NET_INVALID_ENDPOINT_ID,
                                                            channelId,
                                                            b->GetSizeofPayload() ? b->GetPayload() : NULL,
                                                            b->GetSizeofPayload(),
															netEventConnectionError::CXNERR_TERMINATED,
															0,
                                                            m_Allocator);
                            this->QueueEvent(e);
                        }
                        else
                        {
                            netError("Out of memory allocating netEventConnectionError (CXNERR_TERMINATED) (%dB) on Cxn:%08x.%d. Used: %d, Available: %d, Largest Block: %d", 
                                     static_cast<int>(sizeof(netEventConnectionError)), 
                                     cxn->GetId(), 
                                     channelId,
                                     static_cast<int>(m_Allocator->GetMemoryUsed(-1)),
                                     static_cast<int>(m_Allocator->GetMemoryAvailable()),
                                     static_cast<int>(m_Allocator->GetLargestAvailableBlock()));

                            this->QueueOutOfMemory(cxn, true, requiredBytes);
                        }
                    }
#if __BANK
					else if(cxn->IsForcedTimeout())
					{
						netDebug2("%08x.%d[" NET_ADDR_FMT "]: Dropping bundle received due to forced time out",
								  cxn->GetId(),
								  channelId,
								  NET_ADDR_FOR_PRINTF(sender));
					}
#endif
                    else if(cxn->Unpack(p, bsize) != bsize)
                    {
                         netError("%08x.%d[" NET_ADDR_FMT "]: Error unpacking bundle",
                                    cxn->GetId(),
                                    channelId,
                                    NET_ADDR_FOR_PRINTF(sender));
                    }
                    else
                    {
						netDebug3("%08x.%d[" NET_ADDR_FMT "]:[Bundle]<-,sz:%d",
								  cxn->GetId(),
								  channelId,
								  NET_ADDR_FOR_PRINTF(sender),
								  bsize);

                        resetEndpointTimeout = true;
						resetConnectionTimeouts = true;
					}
                }
				else if(bsize > netBundle::BYTE_SIZEOF_HEADER)
				{
					//Out of band.

					const unsigned size = b->GetSizeofPayload();

					if(size <= netFrame::MAX_BYTE_SIZEOF_FRAME)
					{
						netDebug3("*.%d[" NET_ADDR_FMT "]:[%s]<-,sz:%d",
							b->GetChannelId(),
							NET_ADDR_FOR_PRINTF(sender),
							netMessage::IsMessage(b->GetPayload(), size) ? netMessage::GetName(b->GetPayload(), size) : "",
							size);

						if(size > 0)
						{
							// check if this is a relay changed message
							if((b->GetChannelId() == NET_RESERVED_CHANNEL_ID) && HandleBundle(sender, b->GetPayload(), size))
							{
								// if the bundle was handled, we don't dispatch it to the game
							}
							else
							{
								this->QueueOobFrameReceived(ep ? ep->GetId() : NET_INVALID_ENDPOINT_ID,
														    b->GetChannelId(),
															sender,
														    b->GetPayload(),
														    size);
							}
                        }

                        resetEndpointTimeout = true;
                    }
                    else
                    {
                        netError("*.%d[" NET_ADDR_FMT "]: Error unpacking bundle - invalid frame",
                                    b->GetChannelId(),
                                    NET_ADDR_FOR_PRINTF(sender));
                    }
                }
            }
        }

		//Reset timeout timers for all connections on this endpoint.
		if(ep && resetEndpointTimeout)
		{
			ep->m_Timeout = netEndpoint::sm_InactivityTimeoutMs;

			netDebug3("Resetting timeout for endpoint: " NET_ADDR_FMT " for unpack. Resetting connection timeouts: %s",
				NET_ADDR_FOR_PRINTF(ep->GetAddress()),
				resetConnectionTimeouts ? "True" : "False");

			//Update the last receive time
			const unsigned curTime = sysTimer::GetSystemMsTime();

#if NET_CXNMGR_COLLECT_STATS
			const unsigned elapsedTime = (ep->m_LastReceiveTime > 0) ? (curTime - ep->m_LastReceiveTime) : 0;
			netEndpoint::Statistics& stats = ep->GetStats();
			stats.m_MaxDeltaReceiveTime = Max(stats.m_MaxDeltaReceiveTime, elapsedTime);
#endif

			m_LastReceiveTime = curTime;
			ep->m_LastReceiveTime = m_LastReceiveTime;
			
			bool isAnyCxnOpen = false;

			CxnList::iterator it = ep->m_CxnList.begin();
			CxnList::const_iterator stop = ep->m_CxnList.end();
			for(; stop != it; ++it)
			{
				Cxn* cxn = *it;
				isAnyCxnOpen = isAnyCxnOpen || cxn->IsOpen();

				if(resetConnectionTimeouts)
				{
					cxn->ResetTimeoutTimer();
				}
			}

			if(isAnyCxnOpen)
			{					
				m_Router.UpdateLastReceiveTime(ep->GetPeerId(), sender, ep->m_LastReceiveTime);
			}
        }
    }
    rcatchall
    {
    }

#if !__NO_OUTPUT
	nBottomTime = sysTimer::GetSystemMsTime();
	const unsigned nRecvTime = (nBottomTime - nTopTime);
	if(nRecvTime > STALL_THRESHOLD)
		netWarning("NetStallDetect :: Unpack - Receive time of %ums (Top: %ums, Bottom: %ums), LockWait: %u", nRecvTime, nTopTime, nBottomTime, nPostLockTimestamp - nPreLockTimestamp);
#endif
}

void
netConnectionManager::Update(const unsigned UNUSED_PARAM(curTime))
{
	PROFILE

    if(m_IsInitialized)
    {
        netAssert(m_IsMT || sysIpcCurrentThreadIdInvalid == m_Worker.m_SendThreadId);

#if !__NO_OUTPUT
		s_InMainUpdate = true; 
		s_MainThreadId = (size_t)(sysIpcGetCurrentThreadId());
#endif

		m_LastMainThreadUpdateTime = sysTimer::GetSystemMsTime();
		m_TimeStep.SetTime(m_LastMainThreadUpdateTime);

        if(!m_IsMT)
        {
            this->Send();

            for(unsigned chId = 0; chId < NET_MAX_CHANNELS; ++chId)
            {
                m_Channels[chId].Update(m_TimeStep);
            }
        }

		m_Compression.Update();

		// prevent send / receive threads from interjecting when we update
		// Note: we no longer need to take the critical section lock when we dispatch
		// so running the other thread(s) while dispatching should not be an issue.
		if(sm_bAcquireCritSecForFullMainUpdate NOTFINAL_ONLY(&& !PARAM_netDisableAcquireCritSecForFullMainUpdate.Get()))
		{
			NPROFILE(NET_CS_SYNC(m_Cs));
			NPROFILE(this->Dispatch());
			NPROFILE(this->UpdateTunnelRequests());
		}
		else
		{
			NPROFILE(this->Dispatch());
			NPROFILE(this->UpdateTunnelRequests());
		}

#if !__NO_OUTPUT
		s_InMainUpdate = false;
#endif // !__NO_OUTPUT

		if(m_Allocator)
		{
			sysMemAllocator *current = m_Allocator->GetAllocator(MEMTYPE_GAME_VIRTUAL);

			size_t curLowWaterMark = current->GetLowWaterMark(false);
			if(curLowWaterMark < m_MemoryWatermark.LowWatermark)
			{
				m_MemoryWatermark.LowWatermark = curLowWaterMark;
				m_MemoryWatermark.HeapSize = current->GetHeapSize();
				m_MemoryWatermark.MemoryAvailable = current->GetMemoryAvailable();
				m_MemoryWatermark.LargestBlock = current->GetLargestAvailableBlock();
				m_MemoryWatermark.Fragmentation = current->GetFragmentation();
				m_MemoryWatermark.Dirty = true;

				OUTPUT_ONLY(size_t memUsed = m_MemoryWatermark.HeapSize - curLowWaterMark);
				netDebug2("cxnMgr memory low water mark: %" SIZETFMT "u", m_MemoryWatermark.LowWatermark);
				netDebug2("cxnMgr max memory used: %" SIZETFMT "u (%.2f%%)", memUsed, ((float)memUsed / (float)m_MemoryWatermark.HeapSize) * 100.0f);
				netDebug2("cxnMgr memory available: %" SIZETFMT "u", m_MemoryWatermark.MemoryAvailable);
				netDebug2("cxnMgr largest free block: %" SIZETFMT "u", m_MemoryWatermark.LargestBlock);
				netDebug2("cxnMgr memory fragmentation: %u%%", (u32)m_MemoryWatermark.Fragmentation);
			}
		}
    }
}

static bool UnreliableFrameFilter(const netEvent* e)
{
    return (e->GetId() == NET_EVENT_FRAME_RECEIVED)
        && !(e->m_FrameReceived->m_Flags & NET_SEND_RELIABLE);
}

void
netConnectionManager::ReclaimMem()
{
	PROFILE

    NET_CS_SYNC(m_Cs);

	netDebug("Reclaiming memory");

    EndpointsByAddr::iterator itEp = m_EndpointsByAddr.begin();
    EndpointsByAddr::const_iterator stopEp = m_EndpointsByAddr.end();

    for(; stopEp != itEp; ++itEp)
    {
		netEndpoint* ep = itEp->second;
		CxnList::iterator itCxn = ep->m_CxnList.begin();
		CxnList::const_iterator stopCxn = ep->m_CxnList.end();

		for(; stopCxn != itCxn; ++itCxn)
		{
			(*itCxn)->ReclaimMem();
		}

		ep->PurgeQueues();
    }

    //Discard unreliable frames that have already been queued.
    m_EventQ.RemoveEvents(UnreliableFrameFilter);
}

#if !__NO_OUTPUT
void 
DumpEndpoint(netEndpoint* ep, const unsigned epIndex)
{
	netDebug("Active netEndpoint [%u], Address: " NET_ADDR_FMT ", Relay Address: " NET_ADDR_FMT ", Peer Address: %s, NumCxns: %u",
			 epIndex,
			 NET_ADDR_FOR_PRINTF(ep->GetAddress()),
			 NET_ADDR_FOR_PRINTF(ep->m_RelayAddr),
			 ep->GetPeerAddress().ToString(),
			 static_cast<unsigned>(ep->m_CxnList.size()));

	netEndpoint::CxnList::iterator itCxn = ep->m_CxnList.begin();
	netEndpoint::CxnList::const_iterator stopCxn = ep->m_CxnList.end();

	for(unsigned cxnIndex = 0; stopCxn != itCxn; ++itCxn, ++cxnIndex)
	{
		netDebug("Connection [%u], CxnId: 0x%08x, ChannelID: %u", cxnIndex, (*itCxn)->GetId(), (*itCxn)->GetChannelId()); 
		(*itCxn)->DumpQueues();
	}

	if(!ep->m_CxnlessQ.empty())
	{
		netDebug("Connectionless Queue");
		netEndpoint::CxnlessQueue::iterator it = ep->m_CxnlessQ.begin();
		netEndpoint::CxnlessQueue::const_iterator stop = ep->m_CxnlessQ.end();
		for(; stop != it; ++it)
		{
			const CxnlessBundle* bundle = *it;
			netDebug(" *.%d[" NET_ADDR_FMT "]:[%s]->,sz:%d",
				bundle->m_Bundle->GetChannelId(),
				NET_ADDR_FOR_PRINTF(ep->GetAddress()),
				netMessage::IsMessage(bundle->m_Bundle->GetPayload(), bundle->m_Bundle->GetSizeofPayload())
				? netMessage::GetName(bundle->m_Bundle->GetPayload(), bundle->m_Bundle->GetSizeofPayload())
				: "",
				bundle->m_Bundle->GetSize());
		}
	}
}
#endif

void
netConnectionManager::DumpStats()
{
#if !__NO_OUTPUT
    NET_CS_SYNC(m_Cs);

    netDebug("****Dumping Stats****");

	// allocator stats
	netDebug("Allocator: Base: %p, Size: %u, Used: %u, Available: %u, LargestBlock: %u, Fragmentation: %u", 
			 m_Allocator->GetHeapBase(),
			 static_cast<int>(m_Allocator->GetHeapSize()),
			 static_cast<int>(m_Allocator->GetMemoryUsed(-1)),
			 static_cast<int>(m_Allocator->GetMemoryAvailable()),
			 static_cast<int>(m_Allocator->GetLargestAvailableBlock()),
			 static_cast<int>(m_Allocator->GetFragmentation()));

	// endpoint stats
	netDebug("Num Endpoints in Pile: %u", m_EpPile.Size());
	netDebug("Num Endpoints in Pool: %u", static_cast<unsigned>(m_EpPool.size()));
	netDebug("Num Active Endpoints: %u", static_cast<unsigned>(m_ActiveEndpoints.size()));
	netDebug("Num Pending Endpoints: %u", static_cast<unsigned>(m_PendingEndpoints.size()));
	netDebug("Size of netEndpoint: %u", static_cast<unsigned>(sizeof(netEndpoint)));

	// for completeness, lets log out addresses to see how these contribute to fragmentation
	unsigned numEps = m_EpPile.Size();
	for(unsigned i = 0; i < numEps; i++)
		netDebug("netEndpoint %u: Address: %p", i, &m_EpPile[i]);

	// connection stats
	netDebug("Num Connections in Pile: %u", m_CxnPile.Size());
	netDebug("Num Connections in Pool: %u", static_cast<unsigned>(m_CxnPool.size()));
	netDebug("Size of Connection: %u", static_cast<unsigned>(sizeof(Cxn)));

	// for completeness, lets log out addresses to see how these contribute to fragmentation
	unsigned numCxns = m_CxnPile.Size();
	for(unsigned i = 0; i < numCxns; i++)
		netDebug("Connection %u: Address: %p", i, &m_CxnPile[i]);

	if(!m_ActiveEndpoints.empty())
	{
		netDebug("Active Endpoints:");
		EndpointList::iterator itEp = m_ActiveEndpoints.begin();
		EndpointList::const_iterator stopEp = m_ActiveEndpoints.end();
		for(unsigned epIndex = 0; stopEp != itEp; ++itEp, ++epIndex)
		{
			DumpEndpoint(*itEp, epIndex);
		}
	}

	if(!m_PendingEndpoints.empty())
	{
		netDebug("Pending Endpoints:");
		EndpointList::iterator itEp = m_PendingEndpoints.begin();
		EndpointList::const_iterator stopEp = m_PendingEndpoints.end();
		for(unsigned epIndex = 0; stopEp != itEp; ++itEp, ++epIndex)
		{
			DumpEndpoint(*itEp, epIndex);
		}
	}

	if(m_EventQ.HasEvents())
	{
		netDebug("Event Queue:");
		m_EventQ.Dump();
	}
#endif  //__NO_OUTPUT
}

unsigned
netConnectionManager::GetDeltaLastReceiveTime() const
{
    return sysTimer::GetSystemMsTime() - m_LastReceiveTime;
}

unsigned
netConnectionManager::GetDeltaLastReceiveTime(const EndpointId endpointId) const
{
	NET_CS_SYNC(m_Cs);

    const netEndpoint* pEndpoint = GetEndpointById(endpointId);
    if(!pEndpoint)
	{
		return 0;
	}

    return sysTimer::GetSystemMsTime() - pEndpoint->m_LastReceiveTime;
}

unsigned 
netConnectionManager::GetLastAddressAssignmentTime(const EndpointId endpointId) const
{
	NET_CS_SYNC(m_Cs);

	const netEndpoint* ep = GetEndpointById(endpointId);
	if(ep == nullptr)
	{
		return 0;
	}

	return ep->m_LastAddressAssignmentTime;
}

unsigned
netConnectionManager::GetSendFrame() const
{
	return m_SendFrame;
}

unsigned
netConnectionManager::GetPackFrame() const
{
	return m_PackFrame;
}

unsigned
netConnectionManager::GetReceiveFrame() const
{
	return m_ReceiveFrame;
}

unsigned
netConnectionManager::GetUnpackFrame() const
{
	return m_UnpackFrame;
}

unsigned
netConnectionManager::GetLastPackFrameSent() const
{
	return m_LastPackFrameSent;
}

unsigned
netConnectionManager::GetRecvPackFrameMark() const
{
	return m_RecvPackFrameMark;
}

unsigned
netConnectionManager::GetDeltaLastMainThreadUpdateTime() const
{
	unsigned main = m_LastMainThreadUpdateTime; 
	unsigned current = sysTimer::GetSystemMsTime();
	return (current > main) ? (current - main) : 0;
}

bool
netConnectionManager::IsInitialized() const
{
    return m_IsInitialized;
}

//private:

bool
netConnectionManager::SendConnectionless(const netAddress& addr,
                                        const unsigned channelId,
                                        const void* bytes,
                                        const unsigned numBytes,
                                        const unsigned sendFlags)
{
    netAssert(this->IsInitialized());
    netAssert(m_Cs.IsLocked());
	netAssert((sendFlags == 0) || (sendFlags == NET_SEND_IMMEDIATE));

    bool success = false;

    if(netVerify(numBytes <= netFrame::MAX_BYTE_SIZEOF_PAYLOAD))
    {
        netDebug3("*.%d[" NET_ADDR_FMT "]:[%s]->,sz:%d",
                    channelId,
                    NET_ADDR_FOR_PRINTF(addr),
                    netMessage::IsMessage(bytes, numBytes)
                    ? netMessage::GetName(bytes, numBytes)
                    : "",
                    numBytes);

        if(!this->CheckExceededBandwidth(channelId))
        {
            netEndpoint* ep = this->GetActiveEndpointByAddr(addr);
            if(ep)
            {
                CxnlessBundle* cb =
                    (CxnlessBundle*) m_Allocator->RAGE_LOG_ALLOCATE(sizeof(CxnlessBundle), 0);

                if(cb)
                {
                    const unsigned sizeofBndl = sizeof(netBundle) + numBytes;
                    netBundle* bndl =
                        (netBundle*) m_Allocator->RAGE_LOG_ALLOCATE(sizeofBndl, 0);

                    if(bndl)
                    {
                        new(cb) CxnlessBundle(bndl, m_Allocator);
                        bndl->ResetHeader(numBytes,
                                            channelId,
                                            0,
                                            0,
                                            0,
                                            netBundle::FLAG_OUT_OF_BAND);
                        sysMemCpy(bndl->GetPayload(), bytes, numBytes);

                        ep->m_CxnlessQ.push_back(cb);

                        if(sendFlags & NET_SEND_IMMEDIATE)
                        {
                            ep->m_PackTimeout = 0;
                        }

                        success = true;
                    }
                    else
                    {
                        netWarning("Out of memory - not critical but alarming"); 
                        m_Allocator->Free(cb);
                    }
                }
                else
                {
                    netWarning("Out of memory - not critical but alarming"); 
                }
            }
            else
            {
                netPacketStorage pktStorage;
                netPacket* pkt = pktStorage.GetPacket();
                netBundle* bndl = (netBundle*) pkt->GetPayload();

                bndl->ResetHeader(numBytes,
                                    channelId,
                                    0,
                                    0,
                                    0,
                                    netBundle::FLAG_OUT_OF_BAND);
                sysMemCpy(bndl->GetPayload(), bytes, numBytes);

                unsigned sizeofEncrypted, sizeofPlaintext;
                if(m_Channels[channelId].m_Policies.m_CompressionEnabled)
                {
                    sizeofEncrypted = bndl->GetSize();
                    sizeofPlaintext = 0;
                }
                else
                {
                    sizeofEncrypted = 0;
                    sizeofPlaintext = bndl->GetSize();
                }

                pkt->ResetHeader(sizeofEncrypted, 0);

                m_Channels[channelId].RecordOutbound(addr, bndl, bndl->GetSize());

                success = (NET_SOCKERR_NONE == this->SendPacket(addr, &pktStorage, sizeofPlaintext, -1, NULL));
            }
        }
    }

    return success;
}

bool
netConnectionManager::SendTunnelingPacket(const netAddress& addr,
										  const void* bytes,
										  const unsigned numBytes,
										  const netP2pCryptContext& p2pCryptContext)
{
    netAssert(this->IsInitialized());

	// always want tunneling packets to be sent immediately, and on their own,
	// not coalesced with other frames/bundles since we use a different key
	// to encrypt tunneling packets vs non-tunneling packets.

	bool send = false;
	netPacketStorage pktStorage;

    if(netVerify(numBytes <= netFrame::MAX_BYTE_SIZEOF_PAYLOAD))
    {
		NET_CS_SYNC(m_Cs);

		unsigned channelId = NET_TUNNELER_CHANNEL_ID;

        netDebug3("*.%d[" NET_ADDR_FMT "]:[%s]->,sz:%d",
                    channelId,
                    NET_ADDR_FOR_PRINTF(addr),
                    netMessage::IsMessage(bytes, numBytes)
                    ? netMessage::GetName(bytes, numBytes)
                    : "",
                    numBytes);

        if(!this->CheckExceededBandwidth(channelId))
        {
            netPacket* pkt = pktStorage.GetPacket();
            netBundle* bndl = (netBundle*) pkt->GetPayload();

            bndl->ResetHeader(numBytes,
                                channelId,
                                0,
                                0,
                                0,
                                netBundle::FLAG_OUT_OF_BAND);
            sysMemCpy(bndl->GetPayload(), bytes, numBytes);

            unsigned sizeofEncrypted = bndl->GetSize();

            pkt->ResetHeader(sizeofEncrypted, 0);

            m_Channels[channelId].RecordOutbound(addr, bndl, bndl->GetSize());

			send = true;
        }
    }

	// minor optimization - SendPacket (and encryption) can be outside of the CxnMgr's critical section
	if(send)
	{
		return (NET_SOCKERR_NONE == this->SendPacket(addr, &pktStorage, 0, -1, &p2pCryptContext));
	}

    return false;
}

bool
netConnectionManager::SendReservedChannelPacket(const netAddress& addr,
												const void* bytes,
												const unsigned numBytes,
												const netP2pCryptContext* p2pCryptContext)
{
    netAssert(this->IsInitialized());

	// always want tunneling packets to be sent immediately, and on their own,
	// not coalesced with other frames/bundles since we use a different key
	// to encrypt tunneling packets vs non-tunneling packets.

    bool send = false;
	netPacketStorage pktStorage;

    if(netVerify(numBytes <= netFrame::MAX_BYTE_SIZEOF_PAYLOAD))
    {
		NET_CS_SYNC(m_Cs);

		unsigned channelId = NET_RESERVED_CHANNEL_ID;

        netDebug3("*.%d[" NET_ADDR_FMT "]:[%s]->,sz:%d",
                    channelId,
                    NET_ADDR_FOR_PRINTF(addr),
                    netMessage::IsMessage(bytes, numBytes)
                    ? netMessage::GetName(bytes, numBytes)
                    : "",
                    numBytes);

        if(!this->CheckExceededBandwidth(channelId))
        {
            netPacket* pkt = pktStorage.GetPacket();
            netBundle* bndl = (netBundle*) pkt->GetPayload();

            bndl->ResetHeader(numBytes,
                                channelId,
                                0,
                                0,
                                0,
                                netBundle::FLAG_OUT_OF_BAND);
            sysMemCpy(bndl->GetPayload(), bytes, numBytes);

            unsigned sizeofEncrypted = bndl->GetSize();

            pkt->ResetHeader(sizeofEncrypted, 0);

            m_Channels[channelId].RecordOutbound(addr, bndl, bndl->GetSize());

			send = true;
        }
    }

	// minor optimization - SendPacket (and encryption) can be outside of the CxnMgr's critical section
	if(send)
	{
		return (NET_SOCKERR_NONE == this->SendPacket(addr, &pktStorage, 0, -1, p2pCryptContext));
	}

    return false;
}

unsigned
netConnectionManager::PackConnectionless(const netAddress& addr,
                                        netEndpoint::CxnlessQueue* queue,
                                        void* encrypted,
                                        unsigned* numEncryptedBytes,
                                        void* plaintext,
                                        unsigned* numPlaintextBytes,
                                        const unsigned maxBytes)
{
	PROFILE

    netAssert(this->IsInitialized());
    netAssert(m_Cs.IsLocked());

    unsigned capacity = maxBytes;
    u8* tmpEncrypted = (u8*) encrypted;
    u8* tmpPlaintext = (u8*) plaintext;
    *numEncryptedBytes = *numPlaintextBytes = 0;

    //Pack up connectionless bundles that have been queued
    //to the endpoint.  These are bundles that were sent
    //"out-of-band".
    while(!queue->empty() && capacity >= netBundle::BYTE_SIZEOF_HEADER)
    {
        CxnlessBundle* cb = queue->front();

        const unsigned sizeofBndl = cb->m_Bundle->GetSize();

        if(sizeofBndl <= capacity)
        {
            const unsigned channelId = cb->m_Bundle->GetChannelId();
            netAssert(channelId < NET_MAX_CHANNELS);

            //Check for excessive bandwidth
            if(!this->CheckExceededBandwidth(channelId))
            {
                u8** payload;
                unsigned* numBytes;

                if(m_Channels[channelId].m_Policies.m_CompressionEnabled)
                {
                    payload = &tmpEncrypted;
                    numBytes = numEncryptedBytes;
                }
                else
                {
                    payload = &tmpPlaintext;
                    numBytes = numPlaintextBytes;
                }

                sysMemCpy(*payload, cb->m_Bundle, sizeofBndl);

                //Call the packet recorders for the channel.
                m_Channels[channelId].RecordOutbound(addr, cb->m_Bundle, sizeofBndl);

                *payload += sizeofBndl;
                capacity -= sizeofBndl;
                *numBytes += sizeofBndl;
            }

            queue->pop_front();

            sysMemAllocator* allocator = cb->m_Allocator;
            cb->m_Bundle->~netBundle();
            allocator->Free(cb->m_Bundle);
            cb->~CxnlessBundle();
            allocator->Free(cb);
        }
        else
        {
            break;
        }
    }

    return *numEncryptedBytes + *numPlaintextBytes;
}

void*
netConnectionManager::Alloc(const unsigned size)
{
    return m_Allocator->RAGE_LOG_ALLOCATE(size, 0);
}

void*
netConnectionManager::AllocCritical(const unsigned size)
{
    void* p = m_Allocator->RAGE_LOG_ALLOCATE(size, 0);

    if(!p)
    {
		this->ReclaimMem();

        p = m_Allocator->RAGE_LOG_ALLOCATE(size, 0);

        if(!p)
        {
			netError("AllocCritical :: Out of memory allocating %uB - Used: %d, Available: %d, Largest Block: %d", 
				size,
				static_cast<int>(m_Allocator->GetMemoryUsed(-1)),
				static_cast<int>(m_Allocator->GetMemoryAvailable()),
				static_cast<int>(m_Allocator->GetLargestAvailableBlock()));
        }
    }

    return p;
}

netOutFrame*
netConnectionManager::AllocOutFrame(const unsigned sizeofPayload,
									 const unsigned sendFlags)
{
    bool isReliable = (sendFlags & NET_SEND_RELIABLE);
    netOutFrame* outfrm =
        (netOutFrame*) (isReliable
                    ? this->AllocCritical(sizeof(netOutFrame))
                    : this->Alloc(sizeof(netOutFrame)));

    if(outfrm)
    {
        const unsigned sizeofFrame = sizeof(netFrame) + sizeofPayload;
        netFrame* frame =
            (netFrame*) (isReliable
                        ? this->AllocCritical(sizeofFrame)
                        : this->Alloc(sizeofFrame));

        if(frame)
        {
            new(frame) netFrame;
            new(outfrm) netOutFrame(frame);

			outfrm->Reset(sizeofPayload,
				0,
				0,
				sendFlags,
				false);
        }
        else
        {
            m_Allocator->Free(outfrm);
            outfrm = 0;
        }
    }

    return outfrm;
}

void
netConnectionManager::FreeOutFrame(netOutFrame* outfrm)
{
	if(outfrm)
	{
		outfrm->m_Frame->~netFrame();
		outfrm->~netOutFrame();

		if(m_Allocator)
		{
			m_Allocator->Free(outfrm->m_Frame);
			m_Allocator->Free(outfrm);
		}
	}
}

bool
netConnectionManager::Send()
{
	PROFILE

    netAssert(this->IsInitialized());

    if(!m_SocketManager)
    {
    	return false;
    }

	// TODO: NS - because this is updated on the send thread, it will only be updated
	// once per pack interval. Could use a separate network update thread instead.
	m_Router.Update();

    // we break out of the loop when we have nothing to pack during this send frame
    while(true)
	{
    	netPacketStorage storageBuf;
    	netAddress recip;

    	unsigned size = this->Pack(&recip, &storageBuf, netPacketStorage::MAX_SIZEOF_PACKET);
    	if(size == 0)
    	{
    		break;
    	}

    	netPacketStorage storageBufCompressed;
		netPacketStorage* storagePkt = &storageBuf;
    	netPacket* pkt = storagePkt->GetPacket();
    	const unsigned sizeofEncrypted = pkt->GetSize();
    	const unsigned sizeofPlaintext = size - sizeofEncrypted;
		int bytesSavedByCompression = -1;

    	netAssert(size <= netPacket::MAX_BYTE_SIZEOF_PACKET);
    	netAssert(size >= sizeofEncrypted);
    	netAssert(size > sizeofPlaintext);

    	// payload size can be 0 if the packet only contains plaintext data, we don't compress plaintext data
    	if(this->IsCompressionEnabled() && (pkt->GetSizeofPayload() > 0))
    	{
			netPacketStorage* storagePktCompressed = &storageBufCompressed;
    		pkt = this->CompressPacket(pkt, storagePktCompressed->GetPacket(), netPacketStorage::MAX_SIZEOF_PACKET - netPacketStorage::MAX_SIZEOF_HEADER);
    		if(pkt->IsCompressed())
    		{
    			netAssert(storagePktCompressed->GetPacket() == pkt);
    			netAssert(pkt->GetSize() < sizeofEncrypted);

				bytesSavedByCompression = sizeofEncrypted - pkt->GetSize();
				if(!netVerify(bytesSavedByCompression > 0))
				{
					bytesSavedByCompression = 0;
				}

    			//Copy the plaintext portion of the original (uncompressed) packet to the new packet.
    			if(sizeofPlaintext)
    			{
    				const u8* plaintext = (const u8*)storagePkt->GetPacket();
    				plaintext += sizeofEncrypted;
    				u8* compressBuf = (u8*)pkt;
    				sysMemCpy(&compressBuf[pkt->GetSize()], plaintext, sizeofPlaintext);
    			}

				storagePkt = storagePktCompressed;
    		}
			else
			{
				bytesSavedByCompression = 0;
			}
    	}

    	netAssert(storagePkt->GetPacket() == pkt);
		
    	const netSocketError sktErr = this->SendPacket(recip, storagePkt, sizeofPlaintext, bytesSavedByCompression, NULL);

    	// track the last pack frame sent
    	m_LastPackFrameSent = m_PackFrame;

    	if(NET_SOCKERR_NONE != sktErr)
    	{
    		NET_CS_SYNC(m_Cs);

    		//terminate the connection on this ep if possible,
    		//otherwise send a connection error event.
    		netEndpoint* ep = this->GetActiveEndpointByAddr(recip);

			if(ep && !ep->m_CxnList.empty())
    		{
#if NET_CXNMGR_COLLECT_STATS
				ep->GetStats().m_NumSendErrors++;
#endif

				CxnList::iterator it = ep->m_CxnList.begin();
				CxnList::const_iterator stop = ep->m_CxnList.end();

				for(; stop != it; ++it)
				{
					Cxn* cxn = *it;
					
					if(cxn->IsPendingClose())
					{
						this->CloseConnection(cxn, NET_CLOSE_IMMEDIATELY);
					}
    				else
    				{
    					switch(sktErr)
    					{
    						// let these through, the connection will time out if these persist
    						case NET_SOCKERR_WOULD_BLOCK:
    						case NET_SOCKERR_INVALID_ARG:
    						case NET_SOCKERR_NO_BUFFERS:
    						{
    							netWarning("Ignoring socket error %s sending to [" NET_ADDR_FMT "]", netSocketErrorString(sktErr), NET_ADDR_FOR_PRINTF(recip));
    						}
    						break;

    						// generate a send error event
    						case NET_SOCKERR_MESSAGE_TOO_LONG:
    						case NET_SOCKERR_INVALID_SOCKET:
    						case NET_SOCKERR_NETWORK_DOWN:
    						case NET_SOCKERR_INACTIVEDISABLED:
    						case NET_SOCKERR_CONNECTION_RESET:
    						case NET_SOCKERR_HOST_UNREACHABLE:
    						default:
    						{
								const size_t requiredBytes = sizeof(netEventConnectionError);
    							netEventConnectionError* e =
    								(netEventConnectionError*) m_Allocator->RAGE_LOG_ALLOCATE(requiredBytes, 0);

    							if(netVerify(e))
    							{
    								new(e) netEventConnectionError(cxn->GetId(),
																	ep->GetId(),
																	cxn->GetChannelId(),
																	NULL,
																	0,
    																netEventConnectionError::CXNERR_SEND_ERROR,
																	sktErr,
    																m_Allocator);

    								this->QueueEvent(e);
    							}
    							else
    							{
									netError("Out of memory allocating netEventConnectionError (CXNERR_SEND_ERROR) (%dB) on Cxn:%08x.%d. Used: %d, Available: %d, Largest Block: %d", 
											 static_cast<int>(sizeof(netEventConnectionError)), 
											 cxn->GetId(), 
											 cxn->GetChannelId(),
											 static_cast<int>(m_Allocator->GetMemoryUsed(-1)),
											 static_cast<int>(m_Allocator->GetMemoryAvailable()),
											 static_cast<int>(m_Allocator->GetLargestAvailableBlock()));

    								//This event will keep firing until closed or we recover.
    								this->QueueOutOfMemory(cxn, false, requiredBytes);
    							}
							}
    						break;
    					}
    				}
				}
    		}
    	}
    }

	// track send frame
	m_SendFrame++;

    return true;
}

void
netConnectionManager::CheckForReceiveTimeouts()
{
	const unsigned curTime = sysTimer::GetSystemMsTime();
	const unsigned elaspedTime = curTime - m_LastReceiveTimeoutCheckTime;
	const unsigned CHECK_FOR_RECEIVE_TIMEOUTS_INTERVAL_MS = 500;
	if(elaspedTime >= CHECK_FOR_RECEIVE_TIMEOUTS_INTERVAL_MS)
	{
		m_LastReceiveTimeoutCheckTime = curTime;

		NET_CS_SYNC(m_Cs);

		EndpointList::iterator itEp = m_ActiveEndpoints.begin();
		EndpointList::const_iterator itEpStop = m_ActiveEndpoints.end();
		for(; itEpStop != itEp; ++itEp)
		{
			netEndpoint* ep = *itEp;
			if(ep && !ep->m_CxnList.empty())
			{
				netEndpoint::CxnList::iterator it = ep->m_CxnList.begin();
				netEndpoint::CxnList::const_iterator stop = ep->m_CxnList.end();

				for(; stop != it; ++it)
				{
					Cxn* cxn = *it;

					if(cxn->TimeForTimeout())
					{
						netWarning("%08x.%d[" NET_ADDR_FMT "]: QueueTimeout due to no KeepAlives",
							cxn->GetId(),
							cxn->GetChannelId(),
							NET_ADDR_FOR_PRINTF(ep->GetAddress()));

						netVerify(cxn->QueueTimeout(netConnection::CXN_TIMEOUT_REASON_NO_KEEPALIVES));

						cxn->ResetTimeoutTimer();
					}
				}
			}
		}
	}
}

#if NET_PROFILER
static bool s_DidEventRootProfilerPush = false; 
#endif
#define NPROFILE_EVENT(x)  NPROFILE_COND(x, s_DidEventRootProfilerPush);
#define NPROFILE_EVENT_BLOCK(x)  NPROFILE_BLOCK_COND(x, s_DidEventRootProfilerPush);

void
netConnectionManager::Dispatch()
{
	PROFILE

#if NET_PROFILER
	// cap the number of events we consider for profiling (we'll blow the pool otherwise)
	static const unsigned MAX_PROFILER_EVENTS = 50; 
#endif

	// don't dispatch events that get queued while dispatching. This limits the number of events we dispatch per
	// call, which avoids long/infinite dispatch loops when events are continuously being queued by the receive thread.
	const unsigned maxEvents = m_EventConsumer.GetNumEvents();
	unsigned numEvents = 0;

    for(netEvent* e = m_EventConsumer.NextEvent(); e; e = m_EventConsumer.NextEvent())
    {
		netAssert(e->m_CxnId >= 0 || NET_INVALID_CXN_ID == e->m_CxnId);

        // scope block used for profiler push object - DO NOT REMOVE
        {
#if NET_PROFILER
			char szEvent[255];
				formatf(szEvent, "%s[%d:%08x]:%u%s[%s]:%u", e->GetAutoIdNameFromId(e->GetId()), 
												 e->m_ChannelId, 
												 e->m_CxnId, 
												 e->GetId() == NET_EVENT_FRAME_RECEIVED ? e->m_FrameReceived->m_Sequence : 0, 
												 e->GetId() == NET_EVENT_FRAME_RECEIVED ? (((e->m_FrameReceived->m_Flags & NET_SEND_RELIABLE) != 0) ? "R" : "U") : "",
												 e->GetId() == NET_EVENT_FRAME_RECEIVED ? (netMessage::IsMessage(e->m_FrameReceived->m_Payload, e->m_FrameReceived->m_SizeofPayload) ? netMessage::GetName(e->m_FrameReceived->m_Payload, e->m_FrameReceived->m_SizeofPayload) : "") : "",
                                                 numEvents);
			netFunctionProfiler::AutoProfilerPush autoProfilerPush(netFunctionProfiler::GetProfiler(), szEvent, netFunctionProfiler::GetProfiler().GetSubProfilerCountForTop() < MAX_PROFILER_EVENTS, false);
            s_DidEventRootProfilerPush = autoProfilerPush.DidPush();
#endif
			const unsigned eid = e->GetId();

			if(NET_EVENT_CONNECTION_ESTABLISHED == eid)
			{
                NPROFILE_EVENT(NET_CS_SYNC(m_Cs));

				netDebug("Dispatching NET_EVENT_CONNECTION_ESTABLISHED");

				netEndpoint* ep = GetEndpointById(e->GetEndpointId());

				if(netVerify(ep))
				{
					m_Router.AddEndpoint(ep->GetId());

#if NET_CXNMGR_COLLECT_STATS
					ep->GetStats().m_NumCxnsOpened++;
#endif
				}

                Cxn* cxn = this->GetCxnById(e->m_CxnId);
				netStatus* status = cxn->m_Status;
				if(status)
				{
					cxn->m_Status = 0;
					status->SetSucceeded();
				}
			}
			else if(NET_EVENT_CONNECTION_ERROR == eid)
			{
                NPROFILE_EVENT(NET_CS_SYNC(m_Cs));

                Cxn* cxn = this->GetCxnById(e->m_CxnId);
				netStatus* status = cxn->m_Status;
				if(status)
				{
					netAssert(cxn->IsPendingOpen());
					cxn->m_Status = 0;
					status->SetFailed();
				}
			}

            NPROFILE_EVENT(this->DispatchEvent(e));

			++numEvents;

			if(numEvents >= maxEvents)
			{
				break;
			}
		}
    }

    if(m_DispatchTimedOut)
    {
        //Check for connections that have timed out
        //and dispatch timeout error events.

        //Collect the events in this array and dispatch them
        //outside of the crit section to avoid deadlocks.
        //We only dispatch a maximum of 20 events per frame.
        //On the next frame we'll get any we missed.
        const int MAX_CXN_ERROR_EVENTS  = 20;
        u8 eventBuf[MAX_CXN_ERROR_EVENTS*sizeof(netEventConnectionError)];
        netEventConnectionError* cxnErrorEvents = (netEventConnectionError*)eventBuf;
        int numEvents = 0;

        //Begin crit section
        {
            NET_CS_SYNC(m_Cs);

            EndpointList::iterator itEp = m_ActiveEndpoints.begin();
            EndpointList::const_iterator itEpStop = m_ActiveEndpoints.end();
            for(; itEpStop != itEp && numEvents < MAX_CXN_ERROR_EVENTS; ++itEp)
            {
                netEndpoint* ep = *itEp;

				if(ep)
				{
					netEndpoint::CxnList::iterator itCxn = ep->m_CxnList.begin();
					netEndpoint::CxnList::const_iterator itCxnStop = ep->m_CxnList.end();
					for(; itCxnStop != itCxn && numEvents < MAX_CXN_ERROR_EVENTS; ++itCxn)
					{
						Cxn* cxn = *itCxn;

						netConnection::TimeoutReason timeoutReason;
						const bool isTimedOut = cxn->IsTimedOut(&timeoutReason);

						if(!isTimedOut)
						{
							continue;
						}

						new (&cxnErrorEvents[numEvents])
							netEventConnectionError(cxn->GetId(),
													ep->GetId(),
													cxn->GetChannelId(),
													NULL,
													0,
													netEventConnectionError::CXNERR_TIMED_OUT,
													timeoutReason,
													NULL);

						++numEvents;

						cxn->ClearTimeout();

#if NET_CXNMGR_COLLECT_STATS
						ep->GetStats().m_NumTimedOutEvents++;
						if(ep->GetStats().m_TimedOutReason == netConnection::CXN_TIMEOUT_REASON_NONE)
						{
							ep->GetStats().m_TimedOutReason = timeoutReason;
						}
#endif
					}
                }
            }
        }
        //End crit section

        //Dispatch the events we collected in the crit section
        for(int i = 0; i < (int)numEvents; ++i)
        {
            this->DispatchEvent(&cxnErrorEvents[i]);
        }

        m_DispatchTimedOut = false;
    }
}

void
netConnectionManager::UpdateTunnelRequests()
{
	PROFILE

    NPROFILE(NET_CS_SYNC(m_Cs));

	NPROFILE(this->m_Tunneler.Update());

    EndpointList::iterator it = m_PendingEndpoints.begin();
    EndpointList::iterator next = it;
    EndpointList::const_iterator stop = m_PendingEndpoints.end();

    for(++next; stop != it; it = next, ++next)
    {
        netEndpoint* ep = *it;

        netAssert(ep->m_Status);

        if(ep->m_MyStatus.Pending())
        {
            if(sm_CancelTunnelsMatchingActiveEndpoints && ep->m_RelayAddr.IsValid() && this->HasActiveEndpoint(ep->m_RelayAddr) && !ep->m_TunnelRqst->IsExchangingKeys())
            {
                //This can also occur if we receive a packet from the
                //remote peer via a packet relay before our tunnel request
                //completes.
                //FreeEndpoint will cancel the status
				netDebug("UpdateTunnelRequests :: Detected in flight tunnel request with relay address matching existing endpoint [" NET_ADDR_FMT "].", NET_ADDR_FOR_PRINTF(ep->m_RelayAddr));
				this->FreeEndpoint(ep);
            }
        }
        else if(ep->m_MyStatus.Succeeded())
        {
			const netPeerAddress& peerAddr = ep->m_TunnelRqst->GetPeerAddress();
			const netPeerId& peerId = peerAddr.GetPeerId();
			const netAddress& addrOrig = ep->m_TunnelRqst->GetNetAddress();
            const netAddress& relayAddrOrig = ep->m_TunnelRqst->GetRelayAddress();

			netAddress addr = StripRelayToken(addrOrig);
			netAddress relayAddr = StripRelayToken(relayAddrOrig);
			NET_ASSERT_PLAINTEXT_ADDRS(addr, relayAddr);

			netAssert(peerId.IsValid());
			netAssert(addr.IsValid());
			netAssert(!NET_IS_VALID_ENDPOINT_ID(ep->m_TunnelRqst->GetEndpointId()));

			netEndpoint* existingEp = nullptr;
			const netEndpointResolution epResolution = ResolveNewEndpointRequest(peerAddr, peerId, addr, relayAddr, &existingEp OUTPUT_ONLY(, "UpdateTunnelRequests"));
			switch(epResolution)
			{
			case netEndpointResolution::Resolution_Reconnect_SameDetails:
			case netEndpointResolution::Resolution_Reconnect_NewPeerId:
			case netEndpointResolution::Resolution_Reconnect_NewAddress:
			case netEndpointResolution::Resolution_Reconnect_SwitchedToRelay:
				{
					// all of these are expected to supply an existing endpoint
					if(!netVerify(existingEp != nullptr))
					{
						break;
					}

					if(epResolution == netEndpointResolution::Resolution_Reconnect_SwitchedToRelay)
					{
						// extra step just for this case - switch the address to the existing endpoint
						ep->m_TunnelRqst->OverrideNetAddress(existingEp->GetAddress(), relayAddr);
					}

					ep->m_TunnelRqst->SetEndpointId(existingEp->GetId());

					if(ep->m_Status)
					{
						ep->m_Status->SetSucceeded();
					}

					this->FreeEndpoint(ep);
				}
				break;

			case netEndpointResolution::Resolution_Failed:
				{
					if(ep->m_Status)
					{
						ep->m_Status->SetFailed();
					}

					this->FreeEndpoint(ep);
				}
				break;

			case netEndpointResolution::Resolution_CreateNew:
				{
					netVerify(this->ActivatePendingEndpoint(ep, peerAddr, addr, relayAddr));
				}
				break;
			}
        }
        else
        {
            if(ep->m_Status)
            {
                ep->m_Status->SetFailed(ep->m_MyStatus.GetResultCode());
            }
            
            //Tunnel request failed
            this->FreeEndpoint(ep);
        }
    }
}

netAddress
netConnectionManager::StripRelayToken(const netAddress& addr)
{
	// Secure relay addresses are resolved to their plaintext address after tunneling completes.
	// However, the address passed in can come from the peerAddr, which can contain both a relay
	// token and plaintext address. Clear the token here.
	
	if(addr.IsRelayServerAddr() && netVerify(addr.GetTargetAddress().IsValid()) && addr.GetRelayToken().IsValid())
	{
		netAddress plaintextRelayAddr;
		plaintextRelayAddr.Init(addr.GetProxyAddress(), netRelayToken::INVALID_RELAY_TOKEN, addr.GetTargetAddress());
		netDebug("StripRelayToken :: [" NET_ADDR_FMT "] -> [" NET_ADDR_FMT "]", NET_ADDR_FOR_PRINTF(addr), NET_ADDR_FOR_PRINTF(plaintextRelayAddr));
		return plaintextRelayAddr;
	}

	return addr;
}

netConnectionManager::netEndpointResolution
netConnectionManager::ResolveNewEndpointRequest(
	const netPeerAddress& peerAddr,
	const netPeerId& peerId,
	const netAddress& addrOrig,
	const netAddress& relayAddrOrig,
	netEndpoint** existingEp_Out
	OUTPUT_ONLY(, const char* callingFunction))
{
	netAssert(peerId.IsValid());
	netAssert(addrOrig.IsValid());

	netAddress addr = StripRelayToken(addrOrig);
	netAddress relayAddr = StripRelayToken(relayAddrOrig);
	NET_ASSERT_PLAINTEXT_ADDRS(addr, relayAddr);

	netEndpoint* epSameAddr = this->GetActiveEndpointByAddr(addr);
	netEndpoint* epSamePeerId = this->GetActiveEndpointByPeerId(peerId);
	const bool sameEp = (epSameAddr && epSamePeerId) && (epSameAddr == epSamePeerId);

	netEndpointResolution epResolution = netEndpointResolution::Resolution_CreateNew;
	netEndpoint* existingEp = nullptr;

	if((epSameAddr && !epSamePeerId) || (epSameAddr && epSamePeerId && sameEp))
	{
		// Case 2 or 5: We already have an active endpoint with this address. 
		netDebug("ResolveNewEndpointRequest :: %s :: Request succeeded but we already have an active endpoint with address [" NET_ADDR_FMT "].", callingFunction, NET_ADDR_FOR_PRINTF(addr));

		// This can occur if we're in the listening state (see StartListening()) 
		// and we receive a packet from a peer to which we're also trying to connect.
		existingEp = epSameAddr;

		netDebug("ResolveNewEndpointRequest :: %s :: Resetting endpoint timeout", callingFunction);
		existingEp->m_Timeout = netEndpoint::sm_InactivityTimeoutMs;

		if(peerId != existingEp->GetPeerId())
		{
			// the peer could have relaunched the game, changing
			// their netPeerId but keeping the same addr.
			netDebug("ResolveNewEndpointRequest :: %s :: Existing endpoint has different peerId. Updating to new peerId.", callingFunction);
			m_EndpointsByPeerId.erase(existingEp);
			m_EndpointsByPeerId.insert(peerId, existingEp);
		}
		existingEp->m_PeerAddr = peerAddr;
		existingEp->m_RelayAddr = relayAddr;

		epResolution = (epSameAddr && !epSamePeerId) ? netEndpointResolution::Resolution_Reconnect_NewPeerId : netEndpointResolution::Resolution_Reconnect_SameDetails;
	}
	else if(!epSameAddr && epSamePeerId)
	{
		// Case 3: We already have an active endpoint with this peerId, but with a different address.

#if !__NO_OUTPUT
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		netDebug("ResolveNewEndpointRequest ::  %s :: Tunnel request succeeded but we already have an active endpoint with peer id [%s]", callingFunction, peerId.ToHexString(peerIdHexString));
#endif

		existingEp = epSamePeerId;

		netDebug("ResolveNewEndpointRequest :: %s :: Tunnel Request - Direct: " NET_ADDR_FMT ", Relay:" NET_ADDR_FMT ", Existing endpoint - Direct: " NET_ADDR_FMT ", Relay:" NET_ADDR_FMT "",
			callingFunction,
			NET_ADDR_FOR_PRINTF(addr),
			NET_ADDR_FOR_PRINTF(relayAddr),
			NET_ADDR_FOR_PRINTF(existingEp->GetAddress()),
			NET_ADDR_FOR_PRINTF(existingEp->m_RelayAddr));

		if(existingEp->GetAddress().IsPeerRelayAddr() && addr.IsRelayServerAddr())
		{
			netDebug("ResolveNewEndpointRequest :: %s :: Existing connection to %s is via peer relay. Not reassigning to relay server.", callingFunction, peerId.ToHexString(peerIdHexString));
		}
		else
		{
			this->ReassignEndpoint(existingEp, addr, relayAddr);
		}

		netDebug("ResolveNewEndpointRequest :: %s :: Resetting existing endpoint timeout", callingFunction);
		existingEp->m_Timeout = netEndpoint::sm_InactivityTimeoutMs;
		existingEp->m_PeerAddr = peerAddr;
		existingEp->m_RelayAddr = relayAddr;

		epResolution = netEndpointResolution::Resolution_Reconnect_NewAddress;
	}
	else if(epSameAddr && epSamePeerId && !sameEp)
	{
		// Case 4: We already have one endpoint with the same address, and a different endpoint with the same peerId.

		netDebug("ResolveNewEndpointRequest :: %s :: We already have one endpoint with the same address, and a different endpoint with the same peerId. "
			"[" NET_ADDR_FMT "]. Failing request.", callingFunction, NET_ADDR_FOR_PRINTF(addr));

		epResolution = netEndpointResolution::Resolution_Failed;
	}
	else if(relayAddr.IsValid() &&
		relayAddr == addr &&
		this->HasActiveEndpointWithRelayAddr(relayAddr) &&
		!this->HasActiveEndpoint(relayAddr))
	{
		netWarning("ResolveNewEndpointRequest :: %s :: Directly connected endpoint found with relay address [" NET_ADDR_FMT "]. Failing request.", callingFunction, NET_ADDR_FOR_PRINTF(addr));

		// This can occur if we fail a direct connection to a host to whom we already have 
		// an existing direct connection. The tunnel request will route via relay. In this case,
		// we do not want to action this as we have existing established direct connections. 
		existingEp = GetActiveEndpointByRelayAddr(relayAddr);
		netAssert(existingEp);

		//Remove the address from the cache
		RemoveCachedRelayAddress(relayAddr);

		epResolution = netEndpointResolution::Resolution_Reconnect_SwitchedToRelay;
	}
	else
	{
		// Case 1 - no conflicting addr or peerId
		netDebug("ResolveNewEndpointRequest :: %s :: New endpoint with address [" NET_ADDR_FMT "].", callingFunction, NET_ADDR_FOR_PRINTF(addr));
		epResolution = netEndpointResolution::Resolution_CreateNew;
	}

	if(existingEp_Out != nullptr && existingEp != nullptr)
	{
		netDebug("ResolveNewEndpointRequest :: %s :: Returning existing endpoint %u[" NET_ADDR_FMT "]", callingFunction, existingEp->GetId(), NET_ADDR_FOR_PRINTF(existingEp->GetAddress()));
		*existingEp_Out = existingEp;
	}

	return epResolution; 
}

netEndpoint* 
netConnectionManager::CreateEndpoint(const netPeerAddress& peerAddr,
									const netAddress& addrOrig,
                                    const netAddress& relayAddrOrig)
{
    NET_CS_SYNC(m_Cs);

    netEndpoint* ep = NULL;
	std::pair<EndpointsByEndpointId::iterator, bool> byEndpointIdResult;
	byEndpointIdResult.second = false;
	std::pair<EndpointsByAddr::iterator, bool> byAddrResult; 
	byAddrResult.second = false; 
	std::pair<EndpointsByPeerId::iterator, bool> byPeerIdResult;
	byPeerIdResult.second = false;

	const netPeerId& peerId = peerAddr.GetPeerId();

#if !__NO_OUTPUT
	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	peerId.ToHexString(peerIdHexString);
#endif

	rtry
	{
		// the relay address can come from the peerAddr, which can contain
		// both a relay token and plaintext address. Clear the token here.
		netAddress addr = StripRelayToken(addrOrig);
		netAddress relayAddr = StripRelayToken(relayAddrOrig);
		NET_ASSERT_PLAINTEXT_ADDRS(addr, relayAddr);

		rverify(addr.IsValid(), catchall, netError("CreateEndpoint: Invalid Addr: " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(addr)));
		rverify(peerId.IsValid(), catchall, netError("CreateEndpoint: Invalid PeerId: %s", peerIdHexString));
		rverify(peerAddr.IsValid(), catchall, netError("CreateEndpoint: Invalid PeerAddr: %s", peerAddr.ToString()));
		rverify(!this->GetActiveEndpointByAddr(addr), catchall, netError("CreateEndpoint: Found existing netEndpoint with Addr: " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(addr)));
		rverify(!this->GetActiveEndpointByPeerId(peerId), catchall, netError("CreateEndpoint: Found existing netEndpoint with PeerId: %s", peerIdHexString));

#if !__NO_OUTPUT
		for(EndpointsByAddr::iterator itEp = m_EndpointsByAddr.begin(); m_EndpointsByAddr.end() != itEp; ++itEp)
		{
			rverify(itEp->second->GetAddress() != addr, catchall, netError("CreateEndpoint: Iterator - Found existing netEndpoint with Addr: " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(addr)));
		}

		for(EndpointsByPeerId::iterator itEp = m_EndpointsByPeerId.begin(); m_EndpointsByPeerId.end() != itEp; ++itEp)
		{
			rverify(itEp->second->GetPeerId() != peerId, catchall, netError("CreateEndpoint: Iterator - Found existing netEndpoint with PeerId: %s", peerIdHexString));
		}
#endif
		
		ep = this->AllocEndpoint();
		rverify(ep, catchall, netError("CreateEndpoint: Failed to allocate netEndpoint!"));

		ep->m_CreatedTime = sysTimer::GetSystemMsTime();
		ep->m_LastAddressAssignmentTime = ep->m_CreatedTime;

		// keep result when adding to our inmap trackers
		byEndpointIdResult = m_EndpointsByEndpointId.insert(ep->GetId(), ep);
		byAddrResult = m_EndpointsByAddr.insert(addr, ep);
		byPeerIdResult = m_EndpointsByPeerId.insert(peerId, ep);

		rverify(byEndpointIdResult.second, catchall, netError("CreateEndpoint: Failed to add EndpointId to map: %u", ep->GetId()));
		rverify(byAddrResult.second, catchall, netError("CreateEndpoint: Failed to add Addr to map: " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(addr)));
		rverify(byPeerIdResult.second, catchall, netError("CreateEndpoint: Failed to add PeerId to map: %s", peerIdHexString));

		m_ActiveEndpoints.push_back(ep);
		
		netDebug("CreateEndpoint: [%02u-%p] Addr: " NET_ADDR_FMT ", Relay: " NET_ADDR_FMT ", PeerAddr: %s, Size: %u / %u / %u, NumEndpoints: %u, EndpointCapacity: %u",
				 ep->m_Id,
				 ep,
				 NET_ADDR_FOR_PRINTF(addr),
				 NET_ADDR_FOR_PRINTF(relayAddr),
				 peerAddr.ToString(),
				 static_cast<unsigned>(m_EndpointsByEndpointId.size()),
				 static_cast<unsigned>(m_EndpointsByAddr.size()),
				 static_cast<unsigned>(m_EndpointsByPeerId.size()),
				 static_cast<unsigned>(m_ActiveEndpoints.size()),
				 m_EpPile.Size());

		// we need to assign relay from the direct address if we're connected via relay
		if(addr.IsRelayServerAddr())
		{
			++m_NumRelayEndpoints;
			ep->m_RelayAddr = addr;
			ep->SetHopCount(2);

			netDebug("CreateEndpoint: Main address is via relay - assigned:" NET_ADDR_FMT " as relay address",
				NET_ADDR_FOR_PRINTF(ep->m_RelayAddr));
		}
		else
		{
			ep->m_RelayAddr = relayAddr;
			ep->SetHopCount(1);
		}

		ep->m_PeerAddr = peerAddr;
	}
	rcatchall
	{
		// we need to revert 
		if(byEndpointIdResult.second)
		{
			m_EndpointsByEndpointId.erase(ep);
		}

		if(byAddrResult.second)
		{
			m_EndpointsByAddr.erase(ep);
		}

		if(byPeerIdResult.second)
		{
			m_EndpointsByPeerId.erase(ep);
		}

		if(ep)
		{
			// remove endpoint if it was allocated
			FreeEndpoint(ep);
		}

		return NULL;
	}

	return ep;
}

bool
netConnectionManager::ReassignEndpoint(netEndpoint* ep,
                                        const netAddress& addrOrig,
                                        const netAddress& relayAddrOrig)
{
	netAssert(this->IsInitialized());
	netAssert(m_Cs.IsLocked());

	netAddress addr = StripRelayToken(addrOrig);
	netAddress relayAddr = StripRelayToken(relayAddrOrig);
	NET_ASSERT_PLAINTEXT_ADDRS(addr, relayAddr);

	const unsigned curTime = sysTimer::GetSystemMsTime();

    netDebug("Reassigning endpoint [" NET_ADDR_FMT "] to [" NET_ADDR_FMT "] (from %s to %s). Time on last address: %u ms.",
            NET_ADDR_FOR_PRINTF(ep->GetAddress()),
            NET_ADDR_FOR_PRINTF(addr),
			ep->GetAddress().GetTypeString(),
			addr.GetTypeString(),
			curTime - ep->m_LastAddressAssignmentTime);

	const netEndpoint* existingEp = GetActiveEndpointByAddr(addr);
	if(!netVerifyf(!existingEp || (existingEp == ep),
		"ReassignEndpoint :: already have an endpoint with this addr. Cannot have multiple endpoints with the same address."))
	{
		return false;
	}

#if NET_CXNMGR_COLLECT_STATS
	if(ep->GetAddress().IsDirectAddr() && !addr.IsDirectAddr())
	{
		// reassigning a direct route to a non-direct route
		ep->GetStats().m_NumDirectRouteReassignments++;
	}
#endif

    if(ep->GetAddress().IsRelayServerAddr())
    {
        --m_NumRelayEndpoints;

		if(!addr.IsRelayServerAddr())
		{
			// assigning from relay server to direct or peer relay, get rid of relay cache entry
			RemoveCachedRelayAddress(ep->GetAddress());
		}
    }

	m_Tunneler.UpdateP2pKey(ep->GetPeerId(), addr, relayAddr);

    m_EndpointsByAddr.erase(ep);
    m_EndpointsByAddr.insert(addr, ep);
    ep->m_RelayAddr = relayAddr;

	ep->m_LastAddressAssignmentTime = curTime;

	m_Router.UpdateEndpoint(ep->GetPeerId(), ep->m_Id);

	if(addr.IsDirectAddr())
	{
		ep->SetHopCount(1);
	}
	else if(addr.IsRelayServerAddr())
	{
		ep->SetHopCount(2);
	}
	else
	{
		u8 hopCount = m_Router.GetHopCount(ep->GetId());
		netAssert(hopCount >= 2);
		ep->SetHopCount(hopCount);
	}

    if(addr.IsRelayServerAddr())
    {
        ++m_NumRelayEndpoints;
    }

    return true;
}

void
netConnectionManager::UpdateEndpointAddress(const netPeerId& peerId,
											const netAddress& addrOrig,
											const netAddress& relayAddrOrig)
{
    NET_CS_SYNC(m_Cs);

	netAddress addr = StripRelayToken(addrOrig);
	netAddress relayAddr = StripRelayToken(relayAddrOrig);
	NET_ASSERT_PLAINTEXT_ADDRS(addr, relayAddr);

#if !__NO_OUTPUT
	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
	netDebug("UpdateEndpointAddress :: Received updated address (" NET_ADDR_FMT ") for peer id: %s", NET_ADDR_FOR_PRINTF(addr), peerId.ToHexString(peerIdHexString));
#endif

	bool found = false;
	netEndpoint* ep = GetActiveEndpointByPeerId(peerId);
	if(ep)
	{	
		found = true;
		netDebug2("UpdateEndpointAddress :: ep was already active");

		if(relayAddr.IsRelayServerAddr())
		{
			ep->m_RelayAddr = relayAddr;
		}

		const netAddress& curAddr = ep->GetAddress();
		if(addr.IsValid() && (curAddr != addr))
		{
			this->ReassignEndpoint(ep, addr, ep->m_RelayAddr);
		}
	}

	// there can be more than one *pending* endpoint with the same peer id
	EndpointList::iterator it = m_PendingEndpoints.begin();
	EndpointList::const_iterator stop = m_PendingEndpoints.end();
	for(; stop != it; ++it)
	{
		netEndpoint* ep = *it;

		if(ep->m_TunnelRqst)
		{
			if(ep->m_TunnelRqst->GetPeerAddress().GetPeerId() == peerId)
			{
				found = true;
				netDebug2("UpdateEndpointAddress :: ep was in pending list");

				if(relayAddr.IsRelayServerAddr())
				{
					ep->m_RelayAddr = relayAddr;
				}

				if(addr.IsDirectAddr())
				{
					// we need to store the direct address so that when the pending
					// endpoint is activated, it will succeed with a direct address.
					ep->m_TunnelRqst->SetQuickConnectDirectAddr(addr);
				}
			}
		}
	}

	if(!found)
	{
		netDebug2("UpdateEndpointAddress :: no ep was found for the given peer id");
	}

	// we don't necessarily have an existing endpoint when we discover an address for
	// a destination peer. For example, when the remote peer sends the local peer an
	// unsolicited connection request. Create or update a p2p key with this address
	// so any future connection requests from the new address can be decrypted.
	m_Tunneler.UpdateP2pKey(peerId, addr, relayAddr);
}

void
netConnectionManager::DestroyEndpoint(netEndpoint* ep)
{
    if(!netVerify(ep))
	{
		return;
	}

    NET_CS_SYNC(m_Cs);

	netAssert(ep->m_CxnList.empty());

    if(ep->GetAddress().IsRelayServerAddr())
    {
        --m_NumRelayEndpoints;
        netAssert(m_NumRelayEndpoints >= 0);
    }

    m_Tunneler.CloseTunnel(ep->GetAddress());

	// we can have a different key for direct and relay addresses. Free them both.
	if(ep->m_RelayAddr.IsRelayServerAddr() && (ep->GetAddress() != ep->m_RelayAddr))
	{
		m_Tunneler.CloseTunnel(ep->m_RelayAddr);
	}

	const bool eraseAddr = ep->GetAddress().IsValid() && GetActiveEndpointByAddr(ep->GetAddress());
	if(netVerify(eraseAddr))
	{
		m_EndpointsByAddr.erase(ep);
	}
#if !__NO_OUTPUT
	else
	{
		netError("DestroyEndpoint: Failed to erase from address map, Addr: " NET_ADDR_FMT ", Valid: %s, HasActiveEp: %s", 
				 NET_ADDR_FOR_PRINTF(ep->GetAddress()),
				 ep->GetAddress().IsValid() ? "True" : "False",
				 GetActiveEndpointByAddr(ep->GetAddress()) ? "True" : "False");

		EndpointsByAddr::iterator itEp = m_EndpointsByAddr.begin();
		EndpointsByAddr::const_iterator stopEp = m_EndpointsByAddr.end();

		for(unsigned i = 0; stopEp != itEp; ++itEp, ++i)
		{
			netDebug("DestroyEndpoint: Dumping Map - Index: %u, Id: %u, AddrKey: " NET_ADDR_FMT ", AddrEp: " NET_ADDR_FMT ", Match: %s", 
					  i,
					  itEp->second->m_Id,
					  NET_ADDR_FOR_PRINTF(itEp->first),
					  NET_ADDR_FOR_PRINTF(itEp->second->GetAddress()),
					  itEp->first == ep->GetAddress() ? "True" : "False");
		}
	}
#endif
	
	const bool erasePeerId = ep->GetPeerId().IsValid() && GetActiveEndpointByPeerId(ep->GetPeerId());
	if(netVerify(erasePeerId))
	{
		m_EndpointsByPeerId.erase(ep);
	}
#if !__NO_OUTPUT
	else
	{
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];

		netError("DestroyEndpoint: Failed to erase from peer id map, netPeerId: %s, Valid: %s, HasActiveEp: %s", 
				 ep->GetPeerId().ToHexString(peerIdHexString),
				 ep->GetPeerId().IsValid() ? "True" : "False",
				 GetActiveEndpointByPeerId(ep->GetPeerId()) ? "True" : "False");

		EndpointsByPeerId::iterator itEp = m_EndpointsByPeerId.begin();
		EndpointsByPeerId::const_iterator stopEp = m_EndpointsByPeerId.end();

		for(unsigned i = 0; stopEp != itEp; ++itEp, ++i)
		{
			netDebug("DestroyEndpoint: Dumping Map - Index: %u, Id: %u, AddrKey: %s, AddrEp: %s, Match: %s", 
					 i,
					 itEp->second->m_Id,
					 itEp->first.ToHexString(peerIdHexString),
					 itEp->second->GetPeerId().ToHexString(peerIdHexString),
					 itEp->first == ep->GetPeerId() ? "True" : "False");
		}
	}
#endif

	// check if it's an active endpoint first. It won't be in the EndpointId map if it's a pending endpoint.
	const bool eraseEndpointId = GetEndpointById(ep->GetId()) != nullptr;
	if(eraseEndpointId)
	{
		m_EndpointsByEndpointId.erase(ep);
	}

	m_ActiveEndpoints.erase(ep);

	netDebug("DestroyEndpoint: [%02u-%p] Addr: " NET_ADDR_FMT ", Relay: " NET_ADDR_FMT ", PeerAddr: %s, Uptime: %u, Erased: %s / %s, Size: %u / %u, NumEndpoints: %u",
			  ep->m_Id, 
			  ep,
			  NET_ADDR_FOR_PRINTF(ep->GetAddress()),
			  NET_ADDR_FOR_PRINTF(ep->m_RelayAddr),
			  ep->GetPeerAddress().ToString(),
			  (ep->m_CreatedTime > 0) ? sysTimer::GetSystemMsTime() - ep->m_CreatedTime : 0,
			  eraseAddr ? "True" : "False",
			  erasePeerId ? "True" : "False",
			  static_cast<unsigned>(m_EndpointsByAddr.size()),
			  static_cast<unsigned>(m_EndpointsByPeerId.size()),
			  static_cast<unsigned>(m_ActiveEndpoints.size()));

    this->FreeEndpoint(ep);
}

netEndpoint*
netConnectionManager::ActivatePendingEndpoint(netEndpoint* ep,
											const netPeerAddress& peerAddr,
                                            const netAddress& addrOrig,
                                            const netAddress& relayAddrOrig)
{
    netAssert(m_Cs.IsLocked());

	netAddress addr = StripRelayToken(addrOrig);
	netAddress relayAddr = StripRelayToken(relayAddrOrig);
	NET_ASSERT_PLAINTEXT_ADDRS(addr, relayAddr);

    netAssert(!this->HasActiveEndpoint(addr));
    netAssert(!this->HasActiveEndpoint(relayAddr));

    netEndpoint* newEp = NULL;

#if __ASSERT
    EndpointList::iterator it =
        std::find(m_PendingEndpoints.begin(), m_PendingEndpoints.end(), ep);
    netAssert(m_PendingEndpoints.end() != it);
#endif

    //Make sure this is a pending endpoint
    if(netVerify(ep->m_TunnelRqst))
    {
        //Copy the addresses before we free the endpoint
		const netPeerAddress tmpPeerAddr = peerAddr;
		const netAddress tmpAddr = addr.IsValid() ? addr : relayAddr;
        const netAddress tmpRelayAddr = relayAddr;

		m_PendingEndpoints.erase(ep);

		newEp = this->CreateEndpoint(tmpPeerAddr, tmpAddr, tmpRelayAddr);
		if(netVerify(newEp))
		{
			ep->m_TunnelRqst->SetEndpointId(newEp->GetId());

			if(ep->m_Status)
			{
				ep->m_Status->SetSucceeded();
			}
		}
		else if(ep->m_Status)
		{
			ep->m_Status->SetFailed();
		}

		ep->m_TunnelRqst = NULL;

		//Put the endpoint back in the free pool and
        //call CreateEndpoint.
        this->FreeEndpoint(ep);
    }

    return newEp;
}

netEndpoint* 
netConnectionManager::AllocEndpoint()
{
    NET_CS_SYNC(m_Cs);
    
    netEndpoint* ep = NULL;
    
    //Allocate a new endpoint, growing the endpoint pool if necessary.
    if(m_EpPool.empty())
    {
        const unsigned numEps = m_EpPile.Size();
        if(!netVerify(m_EpPile.Resize(numEps + 1)))
        {
            netError("AllocEndpoint: Could not allocate a new endpoint!");
            return 0;
        }

        ep = &m_EpPile[numEps];
    }
    else
    {
        ep = m_EpPool.front();
        m_EpPool.pop_front();
    }

	netAssert(!m_PendingEndpoints.is_present_unsafe(ep));
	netAssert(!m_ActiveEndpoints.is_present_unsafe(ep));
	netAssert(!m_EpPool.is_present_unsafe(ep));

	netAssert(ep->m_CxnList.empty());
	netAssert(ep->m_CxnlessQ.empty());
	netAssert(0 == ep->m_PackTimeout);
	
	// re-constructs the endpoint and assigns a new id
	new (ep) netEndpoint();
    ep->m_Timeout = netEndpoint::sm_InactivityTimeoutMs;
	ep->m_LastTime = 0;
    ep->m_SendInterval = m_SendInterval;
	ep->m_CreatedTime = 0;
	ep->m_EventQ = &m_EventQ;
#if NET_CXNMGR_COLLECT_STATS
	ep->GetStats().m_CxnStartTime = rlGetPosixTime();
#endif

    return ep;
}

void
netConnectionManager::FreeEndpoint(netEndpoint* ep)
{
	if(!netVerify(ep))
	{
		return;
	}

    NET_CS_SYNC(m_Cs);

	netDebug2("FreeEndpoint: " NET_ADDR_FMT " - Tunnel Request: %s", NET_ADDR_FOR_PRINTF(ep->GetAddress()), ep->m_TunnelRqst ? "True" : "False");

#if NET_CXNMGR_COLLECT_STATS
	ep->GetStats().Finalize();

	if(ep->GetStats().HaveStats())
	{
		ep->GetStats().DumpStats();
		ep->GetStats().SendTelemetry();
	}
#endif

	if(ep->m_TunnelRqst)
    {
        //This is a pending endpoint - remove it from the pending list.

        if(ep->m_TunnelRqst->Pending())
        {
            m_Tunneler.CancelRequest(ep->m_TunnelRqst);
        }

        if(ep->m_Status && ep->m_Status->Pending())
        {
            ep->m_Status->SetCanceled();
        }
        
        m_PendingEndpoints.erase(ep);
    }

	// trying to track down url:bugstar:1983294
	// check to make sure this endpoint is not linked into any list
	// is_present_unsafe() is a quick (constant time) check to see if we're linked into a list
	if(!netVerify(!m_PendingEndpoints.is_present_unsafe(ep) && !m_ActiveEndpoints.is_present_unsafe(ep)))
	{
		EndpointList::iterator it = std::find(m_PendingEndpoints.begin(), m_PendingEndpoints.end(), ep);
		if(!netVerifyf(m_PendingEndpoints.end() == it, "Freeing an endpoint that shouldn't have been in the pending list"))
		{
			m_PendingEndpoints.erase(it);
		}
		else
		{
			it = std::find(m_ActiveEndpoints.begin(), m_ActiveEndpoints.end(), ep);
			if(!netVerifyf(m_ActiveEndpoints.end() == it, "Freeing an endpoint that shouldn't have been in the active list"))
			{
				m_ActiveEndpoints.erase(it);
			}
		}
	}

    ep->Clear();
    m_EpPool.push_back(ep);
}

#if __BANK
void
netConnectionManager::ForceTimeOutAllConnections()
{
	netDebug("ForceTimeOutAllConnections");

	NET_CS_SYNC(m_Cs);

	EndpointList::iterator itEp = m_ActiveEndpoints.begin();
	EndpointList::const_iterator stopEp = m_ActiveEndpoints.end();
	for(; stopEp != itEp; ++itEp)
	{
		netEndpoint::CxnList::iterator itCxn = (*itEp)->m_CxnList.begin();
		netEndpoint::CxnList::const_iterator stopCxn = (*itEp)->m_CxnList.end();

		for(; stopCxn != itCxn; ++itCxn)
		{
			Cxn* cxn = *itCxn;
			cxn->ForceTimeout();
		}
	}
}

void 
netConnectionManager::ForceOutOfMemory()
{
	netDebug("ForceOutOfMemory");

	NET_CS_SYNC(m_Cs);


	EndpointList::iterator itEp = m_ActiveEndpoints.begin();
	EndpointList::const_iterator stopEp = m_ActiveEndpoints.end();
	for(; stopEp != itEp; ++itEp)
	{
		netEndpoint::CxnList::iterator itCxn = (*itEp)->m_CxnList.begin();
		netEndpoint::CxnList::const_iterator stopCxn = (*itEp)->m_CxnList.end();

		for(; stopCxn != itCxn; ++itCxn)
		{
			Cxn* cxn = *itCxn;
			cxn->QueueOutOfMemory(333000333); //generic magic number so we know it's a debug call
		}
	}
}

void 
netConnectionManager::ForceCloseConnectionlessEndpoints()
{
	netDebug("ForceCloseConnectionlessEndpoints");

	NET_CS_SYNC(m_Cs);

	EndpointList::iterator itEp = m_ActiveEndpoints.begin();
	EndpointList::const_iterator stopEp = m_ActiveEndpoints.end();
	for(; stopEp != itEp; ++itEp)
	{
		netEndpoint* ep = (*itEp);
		if(ep->m_CxnList.empty())
		{
			m_Tunneler.CloseTunnel(ep->GetAddress());
		}
	}
}
#endif

void
netConnectionManager::GetMaxEndpointStats(const ChannelId channelId, unsigned& unacked, unsigned& oldestResend, unsigned& newQueueLength, unsigned& resendQueueLength, unsigned& ageOfOldestUnackedFrame) const
{
	NET_CS_SYNC(m_Cs);

	if(netVerify(channelId <= NET_MAX_CHANNEL_ID))
	{
		EndpointList::const_iterator itEp = m_ActiveEndpoints.begin();
		EndpointList::const_iterator stopEp = m_ActiveEndpoints.end();

		for (; stopEp != itEp; ++itEp)
		{
			const netEndpoint* ep = (*itEp);
			if(ep)
			{
				Cxn* cxn = ep ? ep->m_Cxns[channelId] : NULL;
				if(cxn)
				{
					if(cxn->IsClosed())
					{
						continue;
					}

					unacked = rage::Max(cxn->GetUnAckedCount(), unacked);
					oldestResend = rage::Max(cxn->GetOldestResendCount(), oldestResend);
					newQueueLength = rage::Max(cxn->GetNewQueueLength(), newQueueLength);
					resendQueueLength = rage::Max(cxn->GetResendQueueLength(), resendQueueLength);
					ageOfOldestUnackedFrame = rage::Max(cxn->GetAgeOfOldestUnackedFrame(), ageOfOldestUnackedFrame);
				}
			}
		}
	}
}

int 
netConnectionManager::EstablishConnection(const EndpointId epId,
										  const ChannelId channelId,
										  const void* bytes,
										  const unsigned numBytes,
										  netStatus* status)
{
	NET_CS_SYNC(m_Cs);

	bool success = false;

	Cxn* cxn = 0;

	rtry
	{
		rverify(IsInitialized(), catchall, netError("EstablishConnection :: Not initialised!"));
		rverify(status == nullptr || !status->Pending(), catchall, netError("EstablishConnection :: Invalid status object!"));
		rverify(NET_IS_VALID_ENDPOINT_ID(epId), catchall, netError("EstablishConnection :: Invalid endpointId %u", epId));
		rverify(NET_IS_VALID_CHANNEL_ID(channelId), catchall, netError("EstablishConnection :: Invalid channelId %u", channelId));

		// find endpoint
		netEndpoint* ep = GetEndpointById(epId);
		
		rverify(ep != nullptr, catchall, netError("EstablishConnection :: Endpoint %d does not exist", epId));

		netDebug2("%u.%u[" NET_ADDR_FMT "]: Establishing connection...", epId, channelId, NET_ADDR_FOR_PRINTF(ep->GetAddress()));

#if !__NO_OUTPUT
		if(PARAM_netCxnEstablishmentStackTraces.Get())
		{
			sysStack::PrintStackTrace();
		}
#endif

        cxn = this->GetCxnByChannelId(epId, channelId);

        if(!cxn)
        {
            cxn = this->CreateConnection(ep, channelId);

            rverify(cxn, catchall, netError("Error creating connection"));
        }
        else
        {
            netDebug2("A connection to:" NET_ADDR_FMT " with channel id:%d exists - adding to ref count",
                    NET_ADDR_FOR_PRINTF(ep->GetAddress()), channelId);

            netAssert(cxn->m_Refs > 0);
        }

		rcheck(cxn->QueueFrame(bytes, numBytes, NET_SEND_RELIABLE, NULL), catchall, netError("EstablishConnection :: Error queuing frame!"));

		// manage status
		if(status)
		{
			status->SetPending();

			if(0 == cxn->m_Refs)
			{
				netAssert(!cxn->m_Status);
				cxn->m_Status = status;
			}
			else
			{
				// already established, succeed the status
				netDebug("%u.%u[" NET_ADDR_FMT "]: EstablishConnection :: already established, succeeding immediately.", epId, channelId, NET_ADDR_FOR_PRINTF(ep->GetAddress()));

				netAssert(cxn->m_Refs > 0);

				//FIXME (KB) - Need to allow multiple refs to a pending
				//connection.  We should be keeping track of all the status
				//objects so they can all be set to Succeeded when the
				//connection is established, but that will require some
				//re-architecting.
				netAssert(!cxn->m_Status);
				status->SetSucceeded();
			}
		}

		++cxn->m_Refs;
		netDebug("%u.%u[" NET_ADDR_FMT "]: EstablishConnection :: RefCount: %u", epId, channelId, NET_ADDR_FOR_PRINTF(ep->GetAddress()), cxn->m_Refs);

		ep->m_Timeout = netEndpoint::sm_InactivityTimeoutMs;

		success = true;
	}
	rcatchall
	{
		if(cxn && 0 == cxn->m_Refs)
		{
			//Add to refs so CloseConnection() doesn't assert.
			++cxn->m_Refs;
			this->CloseConnection(cxn, NET_CLOSE_FINALLY);
			cxn = 0;
		}
	}

	if(!success && status)
	{
		status->SetFailed();
	}

	return success ? cxn->GetId() : NET_INVALID_CXN_ID;
}

int
netConnectionManager::OpenConnection(const netPeerAddress& peerAddr,
									 const netAddress& addr,
									 const ChannelId channelId,
									 const void* bytes,
									 const unsigned numBytes,
									 netStatus* status)
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	netDebug2("*.%u[" NET_ADDR_FMT "]: Opening connection via address...", channelId, NET_ADDR_FOR_PRINTF(addr));

#if !__NO_OUTPUT
	if(PARAM_netCxnEstablishmentStackTraces.Get())
	{
		sysStack::PrintStackTrace();
	}
#endif

	rtry
	{
		netEndpoint* ep = this->GetActiveEndpointByPeerId(peerAddr.GetPeerId());
		if(ep)
		{
			netDebug("OpenConnection :: Already have an endpoint with this peer id %s. Using existing endpoint with address " NET_ADDR_FMT, peerAddr.ToString(), NET_ADDR_FOR_PRINTF(ep->GetAddress()));
		}
		else
		{
			ep = this->CreateEndpoint(peerAddr, addr, peerAddr.GetRelayAddress());
		}
		
		rverify(ep, catchall, netError("OpenConnection :: Failed to create endpoint!"));

		return this->EstablishConnection(ep->GetId(), channelId, bytes, numBytes, status);
	}
	rcatchall
	{
		if(status)
		{
			status->SetFailed();
		}
		return false;
	}
}

int 
netConnectionManager::GetConnectionId(const EndpointId epId,
									  const unsigned channelId) const
{
	NET_CS_SYNC(m_Cs);

	const Cxn* cxn = this->GetCxnByChannelId(epId, channelId);

	return cxn ? cxn->GetId() : NET_INVALID_CXN_ID;
}

int
netConnectionManager::GetConnectionId(const netAddress& addr,
                                    const unsigned channelId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    const Cxn* cxn = this->GetCxnByChannelId(addr, channelId);

    return cxn ? cxn->GetId() : NET_INVALID_CXN_ID;
}

EndpointId
netConnectionManager::GetEndpointId(const int cxnId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

	if(cxnId >= 0)
	{
		const Cxn* cxn = this->GetCxnById(cxnId);

		const netEndpoint* ep = cxn ? cxn->m_EpOwner : nullptr;

		if(ep)
		{
			return ep->GetId();
		}
	}

	return NET_INVALID_ENDPOINT_ID;
}

unsigned
netConnectionManager::GetChannelId(const int cxnId) const
{
	netAssert(this->IsInitialized());

	NET_CS_SYNC(m_Cs);

	const Cxn* cxn = this->GetCxnById(cxnId);

	return cxn ? cxn->GetChannelId() : NET_INVALID_CHANNEL_ID;
}

int
netConnectionManager::GetAnyConnectionWithEndpointId(const EndpointId epId) const
{
	for(int i = 0; i <= NET_MAX_CHANNEL_ID; ++i)
	{
		int connectionId = GetConnectionId(epId, i);
		if(connectionId != NET_INVALID_CXN_ID)
		{
			return connectionId;
		}
	}

	return NET_INVALID_CXN_ID;
}

bool
netConnectionManager::IsOpen(const int cxnId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    const Cxn* cxn = this->GetCxnById(cxnId);
    return cxn && cxn->IsOpen();
}

bool
netConnectionManager::IsClosed(const int cxnId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    const Cxn* cxn = this->GetCxnById(cxnId);
    return !cxn || cxn->IsClosed();
}

bool
netConnectionManager::IsPendingOpen(const int cxnId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    const Cxn* cxn = this->GetCxnById(cxnId);
    return cxn && cxn->IsPendingOpen();
}

bool
netConnectionManager::IsPendingClose(const int cxnId) const
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    const Cxn* cxn = this->GetCxnById(cxnId);
    return cxn && cxn->IsPendingClose();
}

Cxn*
netConnectionManager::CreateConnection(netEndpoint* ep,
									   const unsigned channelId)
{
    netAssert(this->IsInitialized());

    NET_CS_SYNC(m_Cs);

    Cxn* cxn = 0;

    rtry
    {
		rverify(ep, catchall, netError("CreateConnection :: ep is null"));

		netDebug("*.%d[" NET_ADDR_FMT "]: Creating connection on endpointId %u...",
				channelId,
				NET_ADDR_FOR_PRINTF(ep->GetAddress()),
				ep->GetId());

        netAssert(!ep->m_Cxns[channelId]);

        if(m_CxnPool.empty())
        {
            const unsigned numCxns = m_CxnPile.Size();
            rverify(m_CxnPile.Resize(numCxns + 1),
                    catchall,
                    netError("Could not allocate a new connection"));

			rverify(m_CxnPile[numCxns].Init(this, &m_EventQ, m_Allocator),
				catchall,
				netError("Error initializing connection:%d", numCxns));

            cxn = &m_CxnPile[numCxns];
        }
        else
        {
            cxn = m_CxnPool.front();

            //Remove the connection from the inactive list.
            m_CxnPool.pop_front();
        }

        //Put the connection in the PENDING_CONNECTION state.
        netAssert(m_CxnPile.Size() <= 0xFFFF);

        //Each time a connection is pulled from the pool it will have a different id than the 
        //last time it was used.  This helps prevent using a stale id to access a new connection.
		//This previously used a random number between 0 and 0x7FFF but a collision occurred to 
		//the same player that previously held the same connection. This won't happen with a 
		//0x7FFF ranged counter.
		static int s_ConnectionsAllocated = 0;
        const int cxnId = ((++s_ConnectionsAllocated % 0x7FFF) << 16) | m_CxnPile.IndexOf(cxn);
		
		netDebug("*.%d[" NET_ADDR_FMT "]: Allocating Id of %d [from %d / %d]...",
			channelId,
			NET_ADDR_FOR_PRINTF(ep->GetAddress()),
			cxnId,
			s_ConnectionsAllocated,
			m_CxnPile.IndexOf(cxn));

        rcheck(cxn->Open(ep, cxnId, channelId),
                catchall,
                netError("Error creating connection"));

        netAssert(cxn->IsPendingOpen());
        netAssert(!cxn->m_Status);

        //Add the connection to the endpoint representing the remote peer.
        ep->m_Cxns[channelId] = cxn;
        ep->m_CxnList.push_back(cxn);
        ep->m_Timeout = netEndpoint::sm_InactivityTimeoutMs;

        //Set the connection's default policies.
        cxn->SetPolicies(m_Channels[channelId].m_CxnPolicies);

        //Close any lingering connections to the same endpoint on the
        //same channel.  This is done *after* adding the new connection
        //to the endpoint so the endpoint won't be destroyed by destroying
        //what is perhaps the last connection on its list.
        CxnList::iterator it = ep->m_CxnList.begin();
        CxnList::iterator next = it;
        CxnList::const_iterator stop = ep->m_CxnList.end();

        for(++next; stop != it; it = next, ++next)
        {
            Cxn* tmp = *it;
            if(cxn != tmp && tmp->GetChannelId() == channelId)
            {
                netAssert(0 == tmp->m_Refs);
                this->CloseConnection(tmp, NET_CLOSE_IMMEDIATELY);
            }
        }

        netDebug("%08x.%d[" NET_ADDR_FMT "]: Created connection",
                    cxn->GetId(),
                    channelId,
                    NET_ADDR_FOR_PRINTF(ep->GetAddress()));
    }
    rcatchall
    {
        if(cxn)
        {
            this->DestroyConnection(cxn);
            cxn = 0;
        }
    }

    return cxn;
}

netEndpoint*
netConnectionManager::GetEndpointById(const EndpointId epId)
{
	NET_CS_SYNC(m_Cs);

	netAssert(this->IsInitialized());

	if(!netVerify(NET_IS_VALID_ENDPOINT_ID(epId)))
	{
		return nullptr;
	}

	EndpointsByEndpointId::iterator it = m_EndpointsByEndpointId.find(epId);

	return (m_EndpointsByEndpointId.end() != it) ? it->second : nullptr;
}

const netEndpoint*
netConnectionManager::GetEndpointById(const EndpointId epId) const
{
	return const_cast<netConnectionManager*>(this)->GetEndpointById(epId);
}

netEndpoint*
netConnectionManager::GetActiveEndpointByAddr(const netAddress& addr)
{
    netAssert(m_Cs.IsLocked());

	if(addr.IsPeerRelayAddr())
	{
		// this is a peer relay addr - we may know the final destination endpoint by a different address.
		return this->GetActiveEndpointByPeerId(addr.GetTargetPeerId());
	}

    EndpointsByAddr::iterator it = m_EndpointsByAddr.find(addr);

    return (m_EndpointsByAddr.end() != it) ? it->second : NULL;
}

const netEndpoint*
netConnectionManager::GetActiveEndpointByAddr(const netAddress& addr) const
{
    return const_cast<netConnectionManager*>(this)->GetActiveEndpointByAddr(addr);
}

netEndpoint*
netConnectionManager::GetActiveEndpointByPeerId(const netPeerId& peerId)
{
	netAssert(m_Cs.IsLocked());
	netAssert(peerId.IsValid());

	EndpointsByPeerId::iterator it = m_EndpointsByPeerId.find(peerId);

	return (m_EndpointsByPeerId.end() != it) ? it->second : NULL;;
}

const netEndpoint*
netConnectionManager::GetActiveEndpointByPeerId(const netPeerId& peerId) const
{
	return const_cast<netConnectionManager*>(this)->GetActiveEndpointByPeerId(peerId);
}

netEndpoint*
netConnectionManager::GetActiveEndpointByRelayAddr(const netAddress& addr)
{
	netAssert(m_Cs.IsLocked());

	EndpointList::iterator it = m_ActiveEndpoints.begin();
	EndpointList::const_iterator stop = m_ActiveEndpoints.end();
	for(; stop != it; ++it)
	{
		netEndpoint* ep = *it;
		if(ep->m_RelayAddr == addr)
		{
			return ep;
		}
	}

	// not found
	return nullptr;
}

#if __BANK
const netEndpoint* 
netConnectionManager::GetActiveEndpointByGamerHandle(const rlGamerHandle& gamerHandle) const
{
	netAssert(m_Cs.IsLocked());

	if(gamerHandle.IsValid())
	{
		EndpointList::const_iterator it = m_ActiveEndpoints.begin();
		EndpointList::const_iterator stop = m_ActiveEndpoints.end();
		for(; stop != it; ++it)
		{
			const netEndpoint* ep = *it;
			if(ep->m_PeerAddr.GetGamerHandle() == gamerHandle)
			{
				return ep;
			}
		}
	}

	// not found
	return nullptr;
}
#endif

netEndpoint*
netConnectionManager::GetPendingEndpointByAddr(const netAddress& addr)
{
    netAssert(m_Cs.IsLocked());

    EndpointList::iterator it = m_PendingEndpoints.begin();
    EndpointList::const_iterator stop = m_PendingEndpoints.end();
    for(; stop != it; ++it)
    {
        netEndpoint* ep = *it;
        const netAddress& netAddr = ep->GetAddress();

		// using addr.IsEquivalent() here because the pending endpoint may have the
		// 'commplete' relay address for the endpoint, but addr may only contain the
		// plaintext portion of the relay address. These two adresses aren't equal
		// but they are equivalent.
        if((netAddr.IsValid() && addr.IsEquivalent(netAddr))
            || (ep->m_RelayAddr.IsRelayServerAddr() && (addr.IsEquivalent(ep->m_RelayAddr)))
			|| (ep->m_TunnelRqst && (addr.IsEquivalent(ep->m_TunnelRqst->GetRelayAddress()))))
        {
            return ep;
        }
    }

	// not found
	return nullptr;
}

Cxn*
netConnectionManager::GetCxnById(const int id)
{
    netAssert(m_Cs.IsLocked());

    const int realId = id & 0xFFFF;
    Cxn* cxn = (id >= 0 && unsigned(realId) < m_CxnPile.Size()) ? &m_CxnPile[realId] : 0;

    return (cxn && cxn->GetId() == id) ? cxn : NULL;
}

const Cxn*
netConnectionManager::GetCxnById(const int id) const
{
    return const_cast<netConnectionManager*>(this)->GetCxnById(id);
}

Cxn*
netConnectionManager::GetCxnByChannelId(const netAddress& addr,
                                        const unsigned channelId)
{
    netAssert(m_Cs.IsLocked());

    Cxn* cxn = NULL;

    if(netVerify(channelId <= NET_MAX_CHANNEL_ID))
    {
        netEndpoint* ep = this->GetActiveEndpointByAddr(addr);
        cxn = ep ? ep->m_Cxns[channelId] : NULL;
    }

    return cxn;
}

const Cxn*
netConnectionManager::GetCxnByChannelId(const netAddress& addr,
                                        const unsigned channelId) const
{
    return const_cast<netConnectionManager*>(this)->GetCxnByChannelId(addr, channelId);
}

Cxn*
netConnectionManager::GetCxnByChannelId(const EndpointId epId,
                                        const unsigned channelId)
{
    netAssert(m_Cs.IsLocked());

    Cxn* cxn = NULL;

    if(netVerify(channelId <= NET_MAX_CHANNEL_ID))
    {
        netEndpoint* ep = this->GetEndpointById(epId);
        cxn = ep ? ep->m_Cxns[channelId] : NULL;
    }

    return cxn;
}

const Cxn*
netConnectionManager::GetCxnByChannelId(const EndpointId epId,
                                        const unsigned channelId) const
{
    return const_cast<netConnectionManager*>(this)->GetCxnByChannelId(epId, channelId);
}

netPacket*
netConnectionManager::CompressPacket(netPacket* pkt,
                                    void* buf,
                                    const unsigned sizeofBuf) const
{
	PROFILE

    netAssert(!pkt->IsCompressed());

    const unsigned sizeofPl = pkt->GetSizeofPayload();
	netAssert(sizeofPl > 0);

    u8* cbuf = ((u8*) buf) + netPacket::BYTE_SIZEOF_HEADER;
    unsigned sizeofDest = sizeofBuf - netPacket::BYTE_SIZEOF_HEADER;

	// sizeofDest is used as an [in/out] param. On out, will contain the number of bytes in the compressed buffer.
	bool success = m_Compression.Compress((const u8*) pkt->GetPayload(), sizeofPl, cbuf, sizeofDest);

    if(success && (sizeofDest < sizeofPl))
    {
        netPacket* tmpPkt = (netPacket*) buf;
        tmpPkt->ResetHeader(sizeofDest, pkt->GetFlags() | NET_PACKETFLAG_COMPRESSED);
		pkt = tmpPkt;
    }

    return pkt;
}

const netPacket*
netConnectionManager::DecompressPacket(const netPacket* pkt,
                                        void* buf,
                                        const unsigned sizeofBuf) const
{
	PROFILE

    netAssert(pkt->IsCompressed());

    const unsigned sizeofPl = pkt->GetSizeofPayload();
    u8* dcbuf = ((u8*) buf) + netPacket::BYTE_SIZEOF_HEADER;
    unsigned sizeofDest = sizeofBuf - netPacket::BYTE_SIZEOF_HEADER;

	// sizeofDest is used as an [in/out] param. On out, will contain the number of bytes in the decompressed buffer.
	bool success = m_Compression.Decompress((const u8*) pkt->GetPayload(), sizeofPl, dcbuf, sizeofDest);

	if(success)
	{
		netAssert(sizeofDest > 0);
        netPacket* tmpPkt = (netPacket*) buf;
        tmpPkt->ResetHeader(sizeofDest, pkt->GetFlags() & ~NET_PACKETFLAG_COMPRESSED);
        pkt = tmpPkt;
    }
	else
	{
		netError("Failed to decompress packet of size:%d", pkt->GetSize());
		pkt = NULL;
	}

    return pkt;
}

bool 
netConnectionManager::P2pEncrypt(const netAddress& addr,
								 const void* data,
								 const unsigned int sizeOfData,
								 u8* encodedData,
								 unsigned int& sizeOfEncodedData,
								 const netP2pCryptContext* p2pCryptContext)
{
	PROFILE

	rtry
	{
		rverify(addr.IsValid(), catchall, );
		rverify(data, catchall, );
		rverify(sizeOfData > 0, catchall, );
		rverify(encodedData, catchall, );
		rverify(sizeOfEncodedData >= (sizeOfData + MAX_SIZEOF_P2P_CRYPT_OVERHEAD), catchall, );

		u8 flags = p2pCryptContext ? p2pCryptContext->GetFlags() : 0;

		//netDebug3("P2pEncrypting %u bytes for " NET_ADDR_FMT "%s", sizeOfData, NET_ADDR_FOR_PRINTF(addr), (flags & NET_P2P_CRYPT_TUNNELING_PACKET) ? " (tunneling packet)" : "");

		const unsigned sizeOfFlags = 1;
		sizeOfEncodedData -= sizeOfFlags;

		encodedData[0] = flags;

		netP2pCrypt::Key key;

		if(p2pCryptContext)
		{
			key = p2pCryptContext->GetKey();
		}
		else
		{
			// get the key that we previously negotiated with this peer
			rcheck(m_Tunneler.GetP2pKey(addr, key), noEncryptionKey, );
		}

		rverify(netP2pCrypt::Encrypt(key, (const u8*)data, sizeOfData, &encodedData[sizeOfFlags], sizeOfEncodedData), encryptFailed,);

		sizeOfEncodedData += sizeOfFlags;

		return true;
	}
	rcatch(noEncryptionKey)
	{
#if NET_CXNMGR_COLLECT_STATS
		NET_CS_SYNC(m_Cs);
		netEndpoint* ep = this->GetActiveEndpointByAddr(addr);
		if(ep)
		{
			// don't report encryption errors for fake addresses coming from snConnectToPeerTask::Update()
			bool isFakeIp = false;
			if(addr.IsDirectAddr() && addr.GetTargetAddress().GetIp().IsV4())
			{
				isFakeIp = addr.GetTargetAddress().GetIp().ToV4().GetOctet(0) == 0x7F;
			}

			if(!isFakeIp)
			{
				netError("No p2p encryption key for " NET_ADDR_FMT ". Can't encrypt.", NET_ADDR_FOR_PRINTF(addr));
				ep->GetStats().m_NumNoEncryptionKey++;
			}
		}
#endif

		return false;
	}
	rcatch(encryptFailed)
	{
#if NET_CXNMGR_COLLECT_STATS
		NET_CS_SYNC(m_Cs);
		netEndpoint* ep = this->GetActiveEndpointByAddr(addr);
		if(ep)
		{
			ep->GetStats().m_NumFailedEncryptions++;
		}
#endif

		return false;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netConnectionManager::P2pDecrypt(const netAddress& addr,
								 u8** buf,
								 unsigned int& sizeOfBuf,
								 bool& allowRelayOverride)
{
	PROFILE

	rtry
	{
		rverify(addr.IsValid(), catchall, );
		rverify(buf, catchall, );
		rverify(*buf, catchall, );
		rcheck(sizeOfBuf >= MAX_SIZEOF_P2P_CRYPT_OVERHEAD, catchall, netDebug("P2pDecrypt :: packet is too small"));
		
		u8 flags = (*buf)[0];
		//netDebug3("P2pDecrypting %u bytes from " NET_ADDR_FMT "%s", sizeOfBuf, NET_ADDR_FOR_PRINTF(addr), (flags & NET_P2P_CRYPT_TUNNELING_PACKET) ? " (tunneling packet)" : "");

		unsigned sizeOfFlags = 1;
		sizeOfBuf -= sizeOfFlags;
		*buf += sizeOfFlags;

		netP2pCrypt::Key key;
		if(flags & NET_P2P_CRYPT_TUNNELING_PACKET)
		{
			key = netP2pCrypt::GetLocalPeerKey();
		}
		else
		{
			// get the key that we previously negotiated with this peer
			rcheck(m_Tunneler.GetP2pKey(addr, key), catchall, netDebug2("No p2p decryption key for encrypted packet from " NET_ADDR_FMT ". Can't decrypt.", NET_ADDR_FOR_PRINTF(addr)));
		}

		rcheck(netP2pCrypt::Decrypt(key, buf, sizeOfBuf), decryptFailed, netWarning("P2pDecrypt :: netP2pCrypt::Decrypt failed - this usually happens when we try to decrypt using the wrong key."));
		
		if(flags & NET_P2P_CRYPT_TUNNELING_PACKET)
		{
			// only tunneling/reserved channel packets can be encrypted using only our local peer key
			rcheck(IsTunnelingPacket(NULL, *buf, sizeOfBuf), decryptFailed, netError("P2pDecrypt: expected tunneling packet"));
			allowRelayOverride = false;
		}
		else
		{
			allowRelayOverride = true;
		}

		return true;
	}
	rcatch(decryptFailed)
	{
#if NET_CXNMGR_COLLECT_STATS
		NET_CS_SYNC(m_Cs);
		netEndpoint* ep = this->GetActiveEndpointByAddr(addr);

		if(!ep && addr.IsRelayServerAddr())
		{
			ep = this->GetActiveEndpointByRelayAddr(addr);
		}

		if(ep)
		{
			ep->GetStats().m_NumFailedDecryptions++;
		}
#endif

		return false;
	}
	rcatchall
	{
		return false;
	}
}

#if NET_CXNMGR_COLLECT_STATS
void
netConnectionManager::AddSentPacketStats(const netAddress& addr,
										 const unsigned sizeofPkt,
										 const int bytesSavedByCompression,
										 const unsigned sendSize)
{
	// don't take the critical section if we're not going to send to telemetry or output to log/tty
#if __NO_OUTPUT
	if(sm_MetricEnabled)
#endif
	{
		NET_CS_SYNC(m_Cs);
		
		netEndpoint* ep = this->GetActiveEndpointByAddr(addr);

		if(ep)
		{
			unsigned sizeWithProtoHeaders = sendSize + ((m_SocketManager && m_SocketManager->GetMainSocket()) ? m_SocketManager->GetMainSocket()->SizeofHeader() : 0);

			netEndpoint::Statistics& stats = ep->GetStats();
			stats.m_BytesSent += sizeWithProtoHeaders;
			stats.m_PacketsSent++;

			if(bytesSavedByCompression >= 0)
			{
				// these two stats are to measure the compression ratio of the bytes actually 
				// sent to the compressor to determine the compression ratio it achieves.
				stats.m_BytesCompressed += sizeofPkt - netPacket::BYTE_SIZEOF_HEADER;
				stats.m_BytesUncompressed += sizeofPkt - netPacket::BYTE_SIZEOF_HEADER + bytesSavedByCompression;
			}

			if(addr.IsRelayServerAddr())
			{
				stats.m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_RELAY_SERVER] += sizeWithProtoHeaders;
				stats.m_PacketsSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_RELAY_SERVER]++;
			}
			else if(addr.IsPeerRelayAddr())
			{
				const unsigned hopCount = ep->GetHopCount();

				unsigned offset = 0;
				if(hopCount >= 4)
				{
					offset = (unsigned)netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_MORE_HOPS;
				}
				else if(netVerify(hopCount >= 2))
				{
					offset = (unsigned)netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_2_HOPS + (hopCount - 2);
				}

				if((offset >= netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_2_HOPS) &&
					(offset <= netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_MORE_HOPS))
				{
					stats.m_BytesSentByRoute[offset] += sizeWithProtoHeaders;
					stats.m_PacketsSentByRoute[offset]++;
				}
			}
			else
			{
				if(netVerify(addr.IsDirectAddr()))
				{
					stats.m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_DIRECT] += sizeWithProtoHeaders;
					stats.m_PacketsSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_DIRECT]++;
				}
			}
		}
	}
}
#endif

netSocketError
netConnectionManager::SendPacket(const netAddress& addr,
								netPacketStorage* pktStorage,
                                const unsigned sizeofPlaintext,
								const int NET_CXNMGR_COLLECT_STATS_ONLY(bytesSavedByCompression),
								const netP2pCryptContext* p2pCryptContext)
{
	PROFILE

    netSocketError sktErr = NET_SOCKERR_NONE;

	if(addr.IsPeerRelayAddr())
	{
		const netPacket* pkt = pktStorage->GetPacket();
		const unsigned sizeofPkt = pkt->GetSize() + sizeofPlaintext;

		// encrypt peer relay packet payload (i.e. the netPacket) with the final destination peer's key
        netDebug3("*.*[" NET_ADDR_FMT "]:PKT->,sz(e/p):%d/%d",
                    NET_ADDR_FOR_PRINTF(addr),
                    sizeofPkt + netPeerRelayPacket::SIZEOF_P2P_HEADER,
                    sizeofPlaintext);

		netPeerRelayPacket relayPktSecure;
		unsigned int sizeOfData = netPeerRelayPacket::MAX_SIZEOF_RELAY_PACKET - netPeerRelayPacket::SIZEOF_P2P_HEADER;
		if(P2pEncrypt(addr, pkt, sizeofPkt, (u8*)relayPktSecure.GetPayload(), sizeOfData, p2pCryptContext) == false)
		{
			netError("Error encrypting data for peer relay packet to [" NET_ADDR_FMT "]: P2pEncrypt failed", NET_ADDR_FOR_PRINTF(addr));

			// if encryption fails, return no error in case it's a temporary error.
			// The connection will time out if these persist.
			return NET_SOCKERR_NONE;
		}

		// fill out the relay packet header with forward flag set
		m_Router.ResetP2pPacketHeader(&relayPktSecure, addr.GetTargetPeerId(), sizeOfData);

		netAddress nextHopAddr;
		bool success = m_Router.PrepareToForward(&relayPktSecure, &nextHopAddr);
		if(!success || !nextHopAddr.IsValid())
		{
			netWarning("SendPacket :: PrepareToForward failed, dropping packet");
			return NET_SOCKERR_NONE;
		}

		// encrypt entire relay packet (including header) with the next hop peer's key
		const unsigned sizeofRelayPkt = relayPktSecure.GetSize();
		netAssert(sizeofRelayPkt == (sizeOfData + netPeerRelayPacket::SIZEOF_P2P_HEADER));

		u8 data[netSocket::MAX_BUFFER_SIZE];
		unsigned int sizeOfRelayData = sizeof(data);
		if(P2pEncrypt(nextHopAddr, &relayPktSecure, sizeofRelayPkt, data, sizeOfRelayData, nullptr) == false)
		{
			netError("Error encrypting data for p2p relayer [" NET_ADDR_FMT "]: P2pEncrypt failed", NET_ADDR_FOR_PRINTF(nextHopAddr));
			return NET_SOCKERR_NONE;
		}

		if(m_SocketManager)
		{
			const netSocketAddress& dstAddr = nextHopAddr.GetTargetAddress();
			netAssert(dstAddr.IsValid());

			if(!m_SocketManager->Send(dstAddr, data, sizeOfRelayData, &sktErr))
			{
				//Ignore SOCKERR_CONNECTION_RESET.
				if(NET_SOCKERR_CONNECTION_RESET == sktErr)
				{
					sktErr = NET_SOCKERR_NONE;
				}
			}

#if NET_CXNMGR_COLLECT_STATS
 			this->AddSentPacketStats(addr, pkt->GetSize(), bytesSavedByCompression, sizeOfRelayData);
 #endif
		}
	}
    else if(addr.IsRelayServerAddr())
    {
		const netPacket* pkt = pktStorage->GetPacket();
		const unsigned sizeofPkt = pkt->GetSize() + sizeofPlaintext;

        netDebug3("*.*[" NET_ADDR_FMT "]:PKT->,sz(e/p):%d/%d",
                    NET_ADDR_FOR_PRINTF(addr),
                    sizeofPkt + netRelayPacket::SIZEOF_P2P_HEADER,
                    sizeofPlaintext);

		if(!netRelay::IsUsingSecureRelays())
		{
			netRelayP2pPacket relayPktSecure;
			unsigned int sizeOfData = netRelayPacket::MAX_SIZEOF_RELAY_PACKET - netRelayPacket::SIZEOF_P2P_HEADER;
			if(P2pEncrypt(addr, pkt, sizeofPkt, (u8*)relayPktSecure.GetPayload(), sizeOfData, p2pCryptContext) == false)
			{
				netError("Error sending to [" NET_ADDR_FMT "]: P2pEncrypt failed", NET_ADDR_FOR_PRINTF(addr));
				return NET_SOCKERR_NONE;
			}

			sktErr = netRelay::Send(addr, &relayPktSecure, sizeOfData);
#if NET_CXNMGR_COLLECT_STATS
			this->AddSentPacketStats(addr, pkt->GetSize(), bytesSavedByCompression, sizeOfData + netRelayPacket::SIZEOF_P2P_HEADER);
#endif
		}
		else
		{
			netRelayP2pPacket* relayPkt = (netRelayP2pPacket*)pktStorage->GetRelayPacket();
			relayPkt->ResetHeader();
			sktErr = netRelay::Send(addr, relayPkt, sizeofPkt);
#if NET_CXNMGR_COLLECT_STATS
			this->AddSentPacketStats(addr, pkt->GetSize(), bytesSavedByCompression, sizeofPkt + netRelayPacket::SIZEOF_P2P_HEADER);
#endif
		}
    }
    else
    {
        const netPacket* pkt = pktStorage->GetPacket();
        const unsigned sizeofPkt = pkt->GetSize() + sizeofPlaintext;

        netDebug3("*.*[" NET_ADDR_FMT "]:PKT->,sz(e/p):%d/%d",
                    NET_ADDR_FOR_PRINTF(addr),
                    sizeofPkt,
                    sizeofPlaintext);

        if(!netVerify(sizeofPkt <= netPacket::MAX_BYTE_SIZEOF_PACKET))
        {
            sktErr = NET_SOCKERR_MESSAGE_TOO_LONG;
        }
        else if(!netVerify(sizeofPkt >= netPacket::BYTE_SIZEOF_HEADER))
        {
            sktErr = NET_SOCKERR_UNKNOWN;
        }
        else
        {
			u8 data[netSocket::MAX_BUFFER_SIZE];
			unsigned int sizeOfData = sizeof(data);
			if(P2pEncrypt(addr, pkt, sizeofPkt, data, sizeOfData, p2pCryptContext) == false)
			{
				netError("Error sending to [" NET_ADDR_FMT "]: P2pEncrypt failed", NET_ADDR_FOR_PRINTF(addr));
				return NET_SOCKERR_NONE;
			}

            if(m_SocketManager)
            {
				const netSocketAddress& dstAddr = addr.GetTargetAddress();
				netAssert(dstAddr.IsValid());

                if(!m_SocketManager->Send(dstAddr, data, sizeOfData, &sktErr))
                {
                    //Ignore SOCKERR_CONNECTION_RESET.
                    if(NET_SOCKERR_CONNECTION_RESET == sktErr)
                    {
                        sktErr = NET_SOCKERR_NONE;
                    }
                }

#if NET_CXNMGR_COLLECT_STATS
				this->AddSentPacketStats(addr, pkt->GetSize(), bytesSavedByCompression, sizeOfData);
#endif
            }
        }
    }

	if(NET_SOCKERR_NONE != sktErr)
	{
		netError("Error sending to [" NET_ADDR_FMT "]: %s(%d)",
				 NET_ADDR_FOR_PRINTF(addr),
				 netSocketErrorString(sktErr),
				 sktErr);
	}

    return sktErr;
}

bool
netConnectionManager::QueueCxnRequested(const netAddress& sender,
										const ChannelId channelId,
                                        const netFrame* frame)
{
    bool success = false;

    netEventConnectionRequested* rqst =
        (netEventConnectionRequested*) m_Allocator->RAGE_LOG_ALLOCATE(sizeof(netEventConnectionRequested), 0);

	netDebug2("*.%d[" NET_ADDR_FMT "]: QueueCxnRequested :: [%u/%u] FRM(%s)[%s]<-,ch:%u,sq:%d,sc: ,sd:%d,sz:%d",
				channelId,
				NET_ADDR_FOR_PRINTF(sender),
				this->GetReceiveFrame(),
				this->GetUnpackFrame(),
				frame->IsReliable() ? "R" : "U",
				netMessage::IsMessage(frame->GetPayload(), frame->GetSizeofPayload())
				? netMessage::GetName(frame->GetPayload(), frame->GetSizeofPayload())
				: "",
				channelId,
				frame->GetSeq(),
				frame->GetSeqDep(),
				frame->GetSize());

    if(rqst)
    {
        const unsigned sizeofPayload = frame->GetSizeofPayload();
        u8* payload = NULL;
        
        if(sizeofPayload)
        {
            payload = (u8*) m_Allocator->RAGE_LOG_ALLOCATE(sizeofPayload, 0);

            if(payload)
            {
                sysMemCpy(payload, frame->GetPayload(), sizeofPayload);
            }
            else
            {
                netWarning("Out of memory - not critical but alarming"); 
                m_Allocator->Free(rqst);
                rqst = NULL;
            }
        }

        if(rqst)
        {
            new(rqst) netEventConnectionRequested(sender,
												channelId,
                                                payload,
                                                sizeofPayload,
                                                frame->GetSeq(),
                                                m_Allocator);
            this->QueueEvent(rqst);

            success = true;
        }
    }
    else
    {
        netWarning("Out of memory - not critical but alarming"); 
    }

    return success;
}

bool
netConnectionManager::QueueOobFrameReceived(const EndpointId epId,
                                            const unsigned channelId,
											const netAddress& sender,
											const void* payload,
                                            const unsigned sizeofPayload)
{
    netAssert(sizeofPayload > 0);

    bool success = false;

    netEventFrameReceived* fr =
        (netEventFrameReceived*) m_Allocator->RAGE_LOG_ALLOCATE(sizeof(netEventFrameReceived), 0);

    if(fr)
    {
        void* newPayload = m_Allocator->RAGE_LOG_ALLOCATE(sizeofPayload, 0);

        if(newPayload)
        {
            sysMemCpy(newPayload, payload, sizeofPayload);
            new(fr) netEventFrameReceived(sender,
											epId,
											NET_INVALID_CXN_ID,
                                            channelId,
                                            newPayload,
                                            sizeofPayload,
                                            0,
                                            0,
                                            m_Allocator);
            this->QueueEvent(fr);

            success = true;
        }
        else
        {
            netWarning("Out of memory - not critical but alarming"); 
            m_Allocator->Free(fr);
        }
    }
    else
    {
        netWarning("Out of memory - not critical but alarming"); 
    }

    return success;
}

void
netConnectionManager::QueueEvent(netEvent* e)
{
    NET_CS_SYNC(m_Cs);
    m_EventQ.QueueEvent(e, netTimeSync::GetLocalTime(true));
}

void
netConnectionManager::DispatchEvent(netEvent* e)
{
	PROFILE

    //There are cases where we might have queued duplicate connection
    //requests from the same sender (e.g. if we took too long to respond).
    //If we've already accepted the connection then don't dispatch
    //duplicate requests.
    if(NET_EVENT_CONNECTION_REQUESTED == e->GetId()
        && this->GetConnectionId(e->m_CxnRequested->m_Sender,
                                    e->m_CxnRequested->m_ChannelId) >= 0)
    {
        //Already have the connection - don't dispatch the event.
		netDebug("DispatchEvent :: not dispatching duplicate NET_EVENT_CONNECTION_REQUESTED event for channel:%u [" NET_ADDR_FMT "]", e->m_CxnRequested->m_ChannelId, NET_ADDR_FOR_PRINTF(e->m_CxnRequested->m_Sender));
    }
	else if(e->IsFiltered())
	{
		// filtered means an attempt was made to remove the event but it had refs, don't dispatch
		netDebug("DispatchEvent :: not dispatching filtered event with id %u for epId: %u", e->GetId(), e->GetEndpointId());
	}
	else
    {
        NPROFILE_EVENT(NET_CS_SYNC(m_CsDlgt));

		if(netVerify(NET_IS_VALID_CHANNEL_ID(e->m_ChannelId)))
		{
			NPROFILE_EVENT(m_Channels[e->m_ChannelId].DispatchEvent(this, e));
		}
    }
}

bool
netConnectionManager::CheckExceededBandwidth(const unsigned channelId)
{
    netAssert(NET_IS_VALID_CHANNEL_ID(channelId));
    netAssert(m_Cs.IsLocked());

    Channel& channel = m_Channels[channelId];

    if(channel.m_OutboundLimiter < 0)
    {
        if(!channel.m_BandwidthExceeded)
        {
            netWarning("Bandwidth exceeded on channel:%d! Outbound bandwidth limit: %d. Outbound Limiter: %d",
					channelId, channel.m_Policies.m_OutboundBandwidthLimit, channel.m_OutboundLimiter);

            netEventBandwidthExceeded* e =
                (netEventBandwidthExceeded*) m_Allocator->RAGE_LOG_ALLOCATE(sizeof(netEventBandwidthExceeded), 0);

            if(netVerify(e))
            {
                new(e) netEventBandwidthExceeded(NET_INVALID_ENDPOINT_ID, channelId, m_Allocator);
                this->QueueEvent(e);
            }
            else
            {
                netWarning("Out of memory - not critical but alarming"); 
            }

            channel.m_BandwidthExceeded = true;
        }
    }
    else
    {
		if(channel.m_BandwidthExceeded)
		{
			netDebug("Bandwidth no longer exceeded on channel:%d. Outbound bandwidth limit: %d. Outbound Limiter: %d",
				channelId, channel.m_Policies.m_OutboundBandwidthLimit, channel.m_OutboundLimiter);
		}
        channel.m_BandwidthExceeded = false;
    }

    return channel.m_BandwidthExceeded;
}

void
netConnectionManager::QueueOutOfMemory(Cxn* cxn, bool isFatal, size_t attemptedAllocation)
{
    NET_CS_SYNC(m_Cs);

	netError("Queuing netEventOutOfMemory on Cxn:%08x.%d. Fatal: %s, Queued: %s, Used: %d, Available: %d, Largest Block: %d, Requested: %u", 
		cxn->GetId(), 
		cxn->GetChannelId(),
		isFatal ? "True" : "False",
		cxn->m_OutOfMemEvent.IsQueued() ? "True" : "False",
		static_cast<int>(m_Allocator->GetMemoryUsed(-1)),
		static_cast<int>(m_Allocator->GetMemoryAvailable()),
		static_cast<int>(m_Allocator->GetLargestAvailableBlock()),
		static_cast<unsigned>(attemptedAllocation));

	if (m_OutOfMemoryCallback)
	{
		m_OutOfMemoryCallback(cxn->m_EpOwner ? cxn->m_EpOwner->GetId() : NET_INVALID_ENDPOINT_ID, isFatal, static_cast<unsigned>(attemptedAllocation));
	}

	if(!cxn->m_OutOfMemEvent.IsQueued())
    {
#if NET_CXNMGR_COLLECT_STATS
		if(cxn->m_EpOwner)
		{
			cxn->m_EpOwner->GetStats().m_NumOutOfMemoryEvents++;
		}
#endif
		cxn->m_OutOfMemEvent.~netEventOutOfMemory();
		new (&cxn->m_OutOfMemEvent) netEventOutOfMemory(cxn->m_EpOwner ? cxn->m_EpOwner->GetId() : NET_INVALID_ENDPOINT_ID,
														cxn->GetId(),
														cxn->GetChannelId(),
														isFatal,
														NULL);
		this->QueueEvent(&cxn->m_OutOfMemEvent);
    }
}

void
netConnectionManager::OnSocketEventPacketReceived(const netSocketEvent& event)
{
	// p2p packet sent to us by another peer. It may be a direct, forward, or terminus packet.
    if(netVerify(NET_SOCKET_EVENT_PACKET_RECEIVED == event.m_EventType))
    {
        const netSocketEventPacketReceived& pktRecvd = (const netSocketEventPacketReceived&) event;
        this->HandleReceivedPacket(pktRecvd.m_Sender, pktRecvd.m_Payload, pktRecvd.m_Length);

		// track the last pack frame sent when we received
		m_RecvPackFrameMark = m_LastPackFrameSent;
    }
}

void
netConnectionManager::OnRelayEventPacketReceived(const netRelayEvent& event)
{
	// p2p terminus packet relayed to us by our relay server
    if(netVerify(NET_RELAYEVENT_PACKET_RECEIVED == event.m_EventType))
    {
        const netRelayEventPacketReceived& pktRecvd = (const netRelayEventPacketReceived&) event;
		this->HandleReceivedPacket(pktRecvd.m_Sender, pktRecvd.m_Payload, pktRecvd.m_Length);

		// track the last pack frame sent when we received
		m_RecvPackFrameMark = m_LastPackFrameSent;
    }
}

void
netConnectionManager::SendRelayAddressChangedMsg(const netPeerAddress& peerAddr, const netAddress& addr, const MsgCxnRelayAddrChanged* msg)
{
	if((msg == NULL) || !addr.IsValid())
	{
		return;
	}

	bool useP2PCrypt = addr.IsRelayServerAddr() ? !netRelay::IsUsingSecureRelays() : true;

	if(useP2PCrypt)
	{
		// the remote peer won't recognize our address if we've switched to/from relay,
		// so he won't be able to look up the key to decrypt messages we send to him.
		// We need to send the packet encrypted with the remote peer's key instead of the full
		// p2p key so he can decrypt it. Only packets sent on the tunneling or reserved channel
		// will be accepted without a full p2p key.
		const netP2pCrypt::Key& remotePeerKey = peerAddr.GetPeerKey();

		netP2pCryptContext context;
		context.Init(remotePeerKey, NET_P2P_CRYPT_TUNNELING_PACKET);
		this->SendReservedChannelPacket(addr, *msg, &context);
	}
	else
	{
		this->SendOutOfBand(addr, NET_RESERVED_CHANNEL_ID, *msg, NET_SEND_IMMEDIATE);
	}
}

void
netConnectionManager::OnRelayEventAddressChanged(const netRelayEvent& event)
{
	if(netVerify(NET_RELAYEVENT_ADDRESS_CHANGED == event.m_EventType))
	{
		NET_CS_SYNC(m_Cs);

		const netRelayEventAddressChanged& addrChanged = (const netRelayEventAddressChanged&) event;
		
		MsgCxnRelayAddrChanged msg; 
		msg.Reset(addrChanged.m_Address, addrChanged.m_PrevAddress);

		{
			EndpointList::iterator it = m_PendingEndpoints.begin();
			EndpointList::const_iterator stop = m_PendingEndpoints.end();
			for(; stop != it; ++it)
			{
				// default to reassign
				msg.m_bNoReassign = false;

				netEndpoint* ep = *it;

				const netPeerAddress& peerAddr = (ep->m_TunnelRqst && ep->m_TunnelRqst->GetPeerAddress().IsValid()) ?
													ep->m_TunnelRqst->GetPeerAddress() : 
													ep->GetPeerAddress();

				if(ep->GetAddress().IsValid())
				{
					this->SendRelayAddressChangedMsg(peerAddr, ep->GetAddress(), &msg);
				}
				else if(ep->m_RelayAddr.IsValid())
				{
					// if we are communicating on relay, ep->GetAddress() still would have been valid
					// we are sending via relay just to update the remote player copy of the new relay address
					// we don't want to reassign the connection to relay, so flag this
					msg.m_bNoReassign = true;
					this->SendRelayAddressChangedMsg(peerAddr, ep->m_RelayAddr, &msg);
				}
				else
				{
#if !__NO_OUTPUT
					// shouldn't get here
					if(ep->m_TunnelRqst && ep->m_TunnelRqst->GetPeerAddress().IsValid())
					{
						char paBuf[netPeerAddress::TO_STRING_BUFFER_SIZE];
						if(netVerifyf(ep->m_TunnelRqst->GetPeerAddress().ToString(paBuf), "OnRelayEventAddressChanged :: Error getting peer address"))
						{
							netDebug("OnRelayEventAddressChanged :: Invalid address and relay address for %s", paBuf);
						}
					}
#endif
				}
			}
		}

		{
			// reset, all active endpoints should have a valid address
			msg.m_bNoReassign = false;

			EndpointList::iterator it = m_ActiveEndpoints.begin();
			EndpointList::const_iterator stop = m_ActiveEndpoints.end();
			for(; stop != it; ++it)
			{
				netEndpoint* ep = *it;
				if(netVerify(ep->GetAddress().IsValid()))
				{
					this->SendRelayAddressChangedMsg(ep->GetPeerAddress(), ep->GetAddress(), &msg);
				}
			}
		}
	}
}

void
netConnectionManager::OnSocketEventReceiveThreadTicked(const netSocketEvent& event)
{
	if(netVerify(NET_SOCKET_EVENT_RECEIVE_THREAD_TICKED == event.m_EventType))
	{
		// we need to tick this on the relay thread as that's where we receive packets
		CheckForReceiveTimeouts();

		// track receive frame
		m_ReceiveFrame++;
	}
}

void netConnectionManager::HandleSuspension()
{
	PROFILE

#if RSG_DURANGO && 0
    // track how long this call takes - on XBO, we get a small window to clean up before we're suspended. 
    OUTPUT_ONLY(const unsigned curTime = sysTimer::GetSystemMsTime());

    // a suspend will cause us to be disconnected from XBL on resuming so request a remote timeout 
    // so that remote clients don't need to discover this for themselves
    SendRemoteTimeoutRequestToAllEndpoints(netConnection::CXN_TIMEOUT_REASON_REMOTE_SUSPENDED);
    netDebug("HandleSuspension :: Sending timeout request to all connected peers, Time: %ums", sysTimer::GetSystemMsTime() - curTime);
#endif
}

void netConnectionManager::OnSysOnServiceEvent(sysServiceEvent* evnt)
{
	if (!g_CxnMgr || !sm_EnableDelegate)
	{
		return;
	}

	if(evnt != NULL)
	{
		switch(evnt->GetType())
		{
			case sysServiceEvent::SUSPEND_IMMEDIATE:
			{
				g_CxnMgr->HandleSuspension();
				break;
			}
			default:
			{
				break;
			}
		}
	}
}

bool
netConnectionManager::HandleBundle(const netAddress& sender,
								   const void* payload,
								   const unsigned sizeofPayload)
{
	if(!netMessage::IsMessage(payload, sizeofPayload))
	{
		return false;
	}

	unsigned msgId;
	if(!netMessage::GetId(&msgId, payload, sizeofPayload))
	{
		netWarning("Failed to get message Id");
		return false;
	}

	if(msgId == MsgCxnRelayAddrChanged::MSG_ID())
	{
		NET_CS_SYNC(m_Cs);

		// verify message contents
		MsgCxnRelayAddrChanged relayMsg;
		if(netVerifyf(relayMsg.Import(payload, sizeofPayload), "Failed to import MsgCxnRelayAddrChanged!"))
		{
			netEndpoint* ep = NULL;

			// log contents
			netDebug("Received MsgCxnRelayAddrChanged from [" NET_ADDR_FMT "]. Addr: [" NET_ADDR_FMT "], Prev: [" NET_ADDR_FMT "], Reassign: %s",
					  NET_ADDR_FOR_PRINTF(sender),
					  NET_ADDR_FOR_PRINTF(relayMsg.m_RelayAddr),
					  NET_ADDR_FOR_PRINTF(relayMsg.m_PrevRelayAddr),
					  relayMsg.m_bNoReassign ? "False" : "True");

			// if this has come from a relay address
			if(sender.IsRelayServerAddr())
			{
				// validate, if the sender is logged as the previous address - reject this
				if(sender == relayMsg.m_PrevRelayAddr)
				{
					netDebug("Received MsgCxnRelayAddrChanged but sender address is existing relay address!");
					return true;
				}

				// same as the relay checks at the top of this function - see comments there
				if((ep = this->GetPendingEndpointByAddr(relayMsg.m_PrevRelayAddr)) != NULL)
				{
					if(relayMsg.m_bNoReassign)
					{
						// just switch the address, this is still a direct connection
						netDebug("Received MsgCxnRelayAddrChanged for pending endpoint. No reassign but switching address");
						ep->m_RelayAddr = relayMsg.m_RelayAddr;
					}
					else
					{
						// use the new address that this message was sent from
						netDebug("Received MsgCxnRelayAddrChanged for pending endpoint. Switching address and promoting to active endpoint");
						ep->m_RelayAddr = sender;
						m_Tunneler.OverrideRequest(ep->m_TunnelRqst, sender);
					}
				}
				// check both main and relay address, we need to reassign from either
				else if((ep = this->GetActiveEndpointByAddr(relayMsg.m_PrevRelayAddr)) != NULL ||
						(ep = this->GetActiveEndpointByRelayAddr(relayMsg.m_PrevRelayAddr)) != NULL)
				{
					// make sure we don't reassign to an already active endpoint
					if((this->GetActiveEndpointByAddr(sender) == NULL) && (this->GetActiveEndpointByRelayAddr(sender) == NULL))
					{
						netDebug("Received MsgCxnRelayAddrChanged for active endpoint. Reassigning to new relay address");
						this->ReassignEndpoint(ep, sender, sender);
					}
					else
					{
						// we don't want two endpoints with the same address
						netError("Received MsgCxnRelayAddrChanged for active endpoint. Already have an endpoint with this address!");
					}
				}
				else
				{
					netDebug("Unhandled MsgCxnRelayAddrChanged from relay address");
				}
			}
			// look for this relay address on any active endpoint
			else if((ep = this->GetActiveEndpointByAddr(sender)) != NULL)
			{
				// just assign the relay address directly, it's not pending or the main address on an active endpoint
				netDebug("Received MsgCxnRelayAddrChanged for existing endpoint. Assigning new relay address");

				// inform any systems that store the relay address
				if(ep->m_RelayAddr != relayMsg.m_RelayAddr) 
				{
					ep->m_RelayAddr = relayMsg.m_RelayAddr;
					m_Tunneler.UpdateP2pKey(ep->GetPeerId(), sender, relayMsg.m_RelayAddr);
				}
			}
			else
			{
				netDebug("Unhandled MsgCxnRelayAddrChanged");
			}
		}

		return true;
	}
	else if(msgId == MsgCxnRequestRemoteTimeout::MSG_ID())
	{
		NET_CS_SYNC(m_Cs);

		// verify message contents
		MsgCxnRequestRemoteTimeout timeoutMsg;
		if(netVerifyf(timeoutMsg.Import(payload, sizeofPayload), "Failed to import MsgCxnRequestRemoteTimeout!"))
		{
			// log contents
			netDebug("Received MsgCxnRequestRemoteTimeout from [" NET_ADDR_FMT "], Channel: %u, Reason: %d", NET_ADDR_FOR_PRINTF(sender), timeoutMsg.m_ChannelId, timeoutMsg.m_TimeoutReason);
				
			netEndpoint* ep = this->GetActiveEndpointByAddr(sender);
			if(ep)
			{
				CxnList::iterator it = ep->m_CxnList.begin();
				CxnList::const_iterator stop = ep->m_CxnList.end();

				for(; stop != it; ++it)
				{
					Cxn* cxn = *it;
					if(cxn->GetChannelId() == timeoutMsg.m_ChannelId)
					{
						netWarning("%08x.%d[" NET_ADDR_FMT "]: QueueTimeout due to no MsgCxnRequestRemoteTimeout",
								 cxn->GetId(),
								 cxn->GetChannelId(),
								 NET_ADDR_FOR_PRINTF(ep->GetAddress()));

						netVerify(cxn->QueueTimeout(timeoutMsg.m_TimeoutReason));
						break;
					}
				}
			}
		}

		return true;
	}

	return false;
}

#if NET_CXNMGR_COLLECT_STATS
const float netEndpoint::Statistics::STATS_EMA_ALPHA = 1.0f / 100.0f;

netEndpoint::Statistics::Statistics()
: m_Rtt(STATS_EMA_ALPHA)
{
	Clear();
}

netEndpoint::Statistics::~Statistics()
{
	Clear();
}

void netEndpoint::Statistics::Clear()
{
	m_EpOwner = nullptr;
	m_BytesSent = 0;
	m_PacketsSent = 0;

	for(unsigned i = 0; i < NUM_ROUTE_TYPES; ++i)
	{
		m_BytesSentByRoute[i] = 0;
		m_PacketsSentByRoute[i] = 0;
	}

	m_CxnStartTime = 0;
	m_CxnEndTime = 0;
	m_CxnDuration = 0;
	m_NumCxnsOpened = 0;
	m_BytesUncompressed = 0;
	m_BytesCompressed = 0;
	m_FramesSent = 0;
	m_FramesSentReliably = 0;
	m_FramesResent = 0;
	m_FramesDroppedDuplicate = 0;
	m_FramesDroppedLateUnreliable = 0;
	m_FramesDroppedWndOverflow = 0;
	m_FramesRcvdInOrder = 0;
	m_FramesRcvdOutOfOrder = 0;
	m_NumOutOfMemoryEvents = 0;
	m_NumFailedAllocs = 0;
	m_NumTimedOutEvents = 0;
	m_NumSendErrors = 0;
	m_NumFailedEncryptions = 0;
	m_NumNoEncryptionKey = 0;
	m_NumFailedDecryptions = 0;
	m_NumDirectRouteReassignments = 0;
	m_TimedOutReason = netConnection::CXN_TIMEOUT_REASON_NONE;
	m_RouteType = ROUTE_TYPE_INVALID;
	m_Rtt.Clear();
	m_Rtt.SetAlpha(STATS_EMA_ALPHA);
	m_MaxDeltaReceiveTime = 0;
}

bool netEndpoint::Statistics::HaveStats()
{
	return (m_BytesSent > 0) && (m_CxnDuration > 0) && (m_NumCxnsOpened > 0);
}

#if !__NO_OUTPUT
const char* netEndpoint::Statistics::RouteTypeToString(const RouteType type) const
{
	switch(type)
	{
		case ROUTE_TYPE_INVALID: return "INVALID"; break;
		case ROUTE_TYPE_DIRECT: return "Direct"; break;
		case ROUTE_TYPE_RELAY_SERVER: return "Relay Server"; break;
		case ROUTE_TYPE_PEER_RELAY_2_HOPS: return "Peer Relay: 2 hops"; break;
		case ROUTE_TYPE_PEER_RELAY_3_HOPS: return "Peer Relay: 3 hops"; break;
		case ROUTE_TYPE_PEER_RELAY_MORE_HOPS: return "Peer Relay: 4+ hops"; break;
		case NUM_ROUTE_TYPES: return "INVALID"; break;
	}

	netAssertf(false, "Unknown Route Type: %d", (int)type);
	return "**UNKNOWN ROUTE TYPE**";
}
#endif

void netEndpoint::Statistics::Finalize()
{
	m_CxnEndTime = rlGetPosixTime();

	netAssert((m_CxnStartTime > 0) && (m_CxnStartTime <= m_CxnEndTime));

	m_CxnDuration = (u32)(m_CxnEndTime - m_CxnStartTime);

	const unsigned bytesSentDirect = m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_DIRECT];
	const unsigned bytesSentRelayServer = m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_RELAY_SERVER];
	const unsigned bytesSentPeerRelay = m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_2_HOPS] +
									m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_3_HOPS] +
									m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_MORE_HOPS];

	// we can switch back and forth between relay, peer relay, and direct.
	// Choose the route over which the most data was sent.
	const bool viaRelayServer = (bytesSentRelayServer >= bytesSentDirect) && (bytesSentRelayServer >= bytesSentPeerRelay);
	const bool viaPeerRelay = !viaRelayServer && (bytesSentPeerRelay >= bytesSentDirect);

	if(viaRelayServer)
	{
		m_RouteType = netEndpoint::Statistics::ROUTE_TYPE_RELAY_SERVER;
	}
	else if(viaPeerRelay)
	{
		const unsigned bytesSent2Hops = m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_2_HOPS];
		const unsigned bytesSent3Hops = m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_3_HOPS];
		const unsigned bytesSentMoreHops = m_BytesSentByRoute[netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_MORE_HOPS];
		const bool routedVia2Hops = (bytesSent2Hops >= bytesSent3Hops) && (bytesSent2Hops >= bytesSentMoreHops);
		const bool routedVia3Hops = !routedVia2Hops && (bytesSent3Hops >= bytesSentMoreHops);

		if(routedVia2Hops)
		{
			m_RouteType = netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_2_HOPS;
		}
		else if(routedVia3Hops)
		{
			m_RouteType = netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_3_HOPS;
		}
		else
		{
			m_RouteType = netEndpoint::Statistics::ROUTE_TYPE_PEER_RELAY_MORE_HOPS;
		}
	}
	else
	{
		m_RouteType = netEndpoint::Statistics::ROUTE_TYPE_DIRECT;
	}
}

void netEndpoint::Statistics::DumpStats()
{
#if !__NO_OUTPUT

#define NET_CXN_STATS_AVG(x, y) ((y) ? ((x) / (y)) : 0)
#define NET_CXN_STATS_PCT(x, y) (((float)(y) > 0.0f) ? ((float)(x) / (float)(y)) * 100.0f : 0.0f)

	unsigned bytesSent = 0;
	unsigned packetsSent = 0;
	for(unsigned i = 0; i < NUM_ROUTE_TYPES; ++i)
	{
		bytesSent += m_BytesSentByRoute[i];
		packetsSent += m_PacketsSentByRoute[i];
	}

	netAssert(bytesSent == m_BytesSent);
	netAssert(packetsSent == m_PacketsSent);
	netAssert((m_RouteType > ROUTE_TYPE_INVALID) && (m_RouteType < NUM_ROUTE_TYPES));

	unsigned bytesSentPeerRelay = m_BytesSentByRoute[ROUTE_TYPE_PEER_RELAY_2_HOPS] +
								  m_BytesSentByRoute[ROUTE_TYPE_PEER_RELAY_3_HOPS] +
								  m_BytesSentByRoute[ROUTE_TYPE_PEER_RELAY_MORE_HOPS];

	unsigned packetsSentPeerRelay = m_PacketsSentByRoute[ROUTE_TYPE_PEER_RELAY_2_HOPS] + 
									m_PacketsSentByRoute[ROUTE_TYPE_PEER_RELAY_3_HOPS] +
									m_PacketsSentByRoute[ROUTE_TYPE_PEER_RELAY_MORE_HOPS];

	netDebug1("[%u] Connection Statistics: "
				"Duration: %u seconds, "
				"Route: %s, "
				"Out of memory events: %u, "
				"Failed allocs: %u, "
				"Timed out events: %u, "
				"Send errs: %u, "
				"Failed encryptions: %u",
				m_EpOwner->GetId(),
				m_CxnDuration,
				RouteTypeToString(m_RouteType),
				m_NumOutOfMemoryEvents,
				m_NumFailedAllocs,
				m_NumTimedOutEvents,
				m_NumSendErrors,
				m_NumNoEncryptionKey + m_NumFailedEncryptions);

	netDebug1("[%u] Packet Statistics:\n"
				"Bytes Sent: %u (Direct: %u (%.0f%%), Relay: %u (%.0f%%), Peer Relay: %u (%.0f%%))\n"
				"Packets Sent: %u (Direct: %u, Relay: %u, Peer Relay: %u)\n"
				"Frames Sent: %u (Reliably: %u, Resent: %u)\n"
				"Frames Dropped: %u (Duplicate: %u, Late Unreliable: %u, Wnd Overflow: %u)\n"
				"Frames Rcvd: %u (In Order: %u, Out Of Order: %u)\n",
				m_EpOwner->GetId(),
				m_BytesSent,
				m_BytesSentByRoute[ROUTE_TYPE_DIRECT],
				NET_CXN_STATS_PCT(m_BytesSentByRoute[ROUTE_TYPE_DIRECT], m_BytesSent),
				m_BytesSentByRoute[ROUTE_TYPE_RELAY_SERVER],
				NET_CXN_STATS_PCT(m_BytesSentByRoute[ROUTE_TYPE_RELAY_SERVER], m_BytesSent),
				bytesSentPeerRelay,
				NET_CXN_STATS_PCT(bytesSentPeerRelay, m_BytesSent),
				m_PacketsSent,
				m_PacketsSentByRoute[ROUTE_TYPE_DIRECT],
				m_PacketsSentByRoute[ROUTE_TYPE_RELAY_SERVER],
				packetsSentPeerRelay,
				m_FramesSent,
				m_FramesSentReliably,
				m_FramesResent,
				m_FramesDroppedDuplicate + m_FramesDroppedLateUnreliable + m_FramesDroppedWndOverflow,
				m_FramesDroppedDuplicate,
				m_FramesDroppedLateUnreliable,
				m_FramesDroppedWndOverflow,
				m_FramesRcvdInOrder + m_FramesRcvdOutOfOrder,
				m_FramesRcvdInOrder,
				m_FramesRcvdOutOfOrder);

	netDebug1("[%u] Bandwidth Statistics:\n"
				"Avg Bytes/sec: %u\n"
				"Avg Packets/sec: %u\n"
				"Avg Bytes/Packet: %u\n"
				"Avg Bytes/Frame: %u\n"
				"Avg Frames/Packet: %.1f\n"
				"Avg Reliable RTT: %u ms",
				m_EpOwner->GetId(),
				NET_CXN_STATS_AVG(m_BytesSent, m_CxnDuration),
				NET_CXN_STATS_AVG(m_PacketsSent, m_CxnDuration),
				NET_CXN_STATS_AVG(m_BytesSent, m_PacketsSent),
				NET_CXN_STATS_AVG(m_BytesSent, m_FramesSent),
				NET_CXN_STATS_AVG((float)m_FramesSent, (float)m_PacketsSent),
				m_Rtt.Get());

	u32 bytesSaved = m_BytesUncompressed - m_BytesCompressed;
	float compressionRatio = NET_CXN_STATS_PCT(m_BytesCompressed, m_BytesUncompressed);
	float overallCompressionRatio = NET_CXN_STATS_PCT(m_BytesSent, m_BytesSent + bytesSaved);

	netDebug1("[%u] Compression Statistics: "
			"Compression ratio: %.2f%%, "
			"Ratio incl bytes not sent to compressor: %.2f%%",
			m_EpOwner->GetId(),
			compressionRatio,
			overallCompressionRatio);
#undef NET_CXN_STATS_AVG
#undef NET_CXN_STATS_PCT
#endif
}

void netEndpoint::Statistics::SendTelemetry()
{
	netIpAddress lpub;
	netIpAddress lrly;
	netIpAddress rpub;
	netIpAddress rrly;

	netPeerAddress peerAddr;
	if(netPeerAddress::GetLocalPeerAddress(rlPresence::GetActingUserIndex(), &peerAddr))
	{
		lpub = peerAddr.GetPublicAddress().GetIp();

		if(peerAddr.GetRelayAddress().IsRelayServerAddr())
		{
			lrly = peerAddr.GetRelayAddress().GetProxyAddress().GetIp();
		}
	}

	if(m_EpOwner->m_RelayAddr.IsRelayServerAddr())
	{
		rpub = m_EpOwner->m_RelayAddr.GetTargetAddress().GetIp();
		rrly = m_EpOwner->m_RelayAddr.GetProxyAddress().GetIp();
	}
	else
	{
		// this can be a private address if on the same LAN
		const netAddress& addr = m_EpOwner->GetAddress();
		const netSocketAddress& sockAddr = addr.IsPeerRelayAddr() ?
											addr.GetProxyAddress() : addr.GetTargetAddress();

		rpub = sockAddr.GetIp();
	}
	
	rlStandardMetrics::P2pConnectionReport metric(lpub, lrly, rpub, rrly, *this);
	rlTelemetry::Write(rlPresence::GetActingUserIndex(), metric);
}

#endif

}   //namespace rage
