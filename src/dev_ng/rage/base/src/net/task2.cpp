// 
// net/task2.cpp 
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#include "task.h"
#include "task2.h"
#include "netdiag.h"
#include "profile/rocky.h"
#include "system/threadpool.h"
#include "system/timer.h"
#include "pool.h"
#include "tcp.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, task2);
#undef __rage_channel
#define __rage_channel ragenet_task2

INITIALIZE_THREAD_PTR(netTaskImpl, netTask2::sm_CurTask, nullptr);
INITIALIZE_THREAD_PTR(sysThreadPool, netTask2::sm_threadPool, nullptr);

sysIpcCurrentThreadId netTask2::sm_ThreadId = sysIpcCurrentThreadIdInvalid;

static sysCriticalSectionToken s_TaskChildCs;
static sysCriticalSectionToken s_TaskPoolCs;
static sysCriticalSectionToken s_WorkerPoolCs;

static netPool<netTaskImpl, netTask2::TASK_POOL_SIZE>& GetPool()
{
    static netPool<netTaskImpl, netTask2::TASK_POOL_SIZE> s_TaskPool;
    return s_TaskPool;
}

static netPool<netTaskWorkItem, netTask2::WORK_ITEM_POOL_SIZE>& GetWorkItemPool()
{
	static netPool<netTaskWorkItem, netTask2::WORK_ITEM_POOL_SIZE> s_WorkItemPool;
	return s_WorkItemPool;
}

extern sysThreadPool netThreadPool;

static unsigned volatile s_NetTask2NextTaskId = 0;

#if NETTASK2_POOL_PROFILING
float netTask2::m_TaskPoolMaxUsage = 0;
float netTask2::m_WorkerPoolMaxUsage = 0;
#endif

netTask2::netTask2()
    : m_TaskImpl(nullptr)
{
}

netTask2::~netTask2()
{
    if(m_TaskImpl)
    {
        m_TaskImpl->Unref();
		m_TaskImpl = nullptr;
    }
}

netTask2::netTask2(const netTask2& that)
    : m_TaskImpl(that.m_TaskImpl)
{
    if(m_TaskImpl)
    {
        m_TaskImpl->Ref();
    }
}

netTask2&
netTask2::operator=(const netTask2& that)
{
    if(&that == this) return *this;

    if(m_TaskImpl)
    {
        m_TaskImpl->Unref();
		m_TaskImpl = nullptr;
    }

    m_TaskImpl = that.m_TaskImpl;

    if(m_TaskImpl)
    {
        m_TaskImpl->Ref();
    }

    return *this;
}

sysThreadPool* 
netTask2::GetThreadPool()
{
	// Safety net in case this gets called
	// before the netTask2::Init() [which gets called from netInit()]
	if(sm_threadPool == NULL)
		return &netThreadPool;
	return sm_threadPool;
}

bool
netTask2::IsInitialized() const
{
	return m_TaskImpl != nullptr;
}

bool
netTask2::IsComplete() const
{
    if (netVerify(IsInitialized()))
	{
		return m_TaskImpl->IsComplete();
	}

	return false;
}

bool
netTask2::IsPending() const
{
	if (!IsInitialized())
		return false;

	return !m_TaskImpl->IsComplete();
}

bool 
netTask2::IsSucceeded() const
{
	if (netVerify(IsInitialized()))
	{
		return m_TaskImpl->IsSucceeded();
	}

	return false;
}

bool
netTask2::IsFailed() const
{
	if (netVerify(IsInitialized()))
	{
		return m_TaskImpl->IsFailed();
	}

	return false;
}

bool
netTask2::IsCanceled() const
{
	if (netVerify(IsInitialized()))
	{
		return m_TaskImpl->IsCanceled();
	}

	return false;
}

void
netTask2::AddChildTask(netTask2 task)
{
    if (!netVerify(IsInitialized()))
		return;

    if (!netVerify(task.IsInitialized()))
		return;

    if (!netVerify(!task.m_TaskImpl->m_Parent))
		return;

    m_TaskImpl->AddChild(task.m_TaskImpl);
}

void
netTask2::Cancel()
{
    netAssert(IsInitialized());
    m_TaskImpl->Cancel();
}

void
netTask2::CancelTask(netStatus* status)
{
	netTaskImpl& rootTask = GetRootTask();
	rootTask.CancelByStatus(status);
}

void
netTask2::DetachTask(netStatus* status)
{
	netTaskImpl& rootTask = GetRootTask();
	rootTask.DetachTask(status);
}

void
netTask2::ScheduleTask(netTask2 task)
{
	GetRootTask().AddChild(task.m_TaskImpl);
}

void
netTask2::Wait(const netTask2& task)
{
	// Potential TODO:
	//	Add support for allowing a threadpool task to wait for another task
	//	to complete. For now, threadpool tasks should only represent a single
	//	unit of work due to their blocking nature. We only have 2 threads in the
	//	netThreadPool after all.
	if (!netVerify(sysIpcGetCurrentThreadId() == sm_ThreadId))
		return;

    if(!netVerify(sm_CurTask)) return;

    if(!netVerify(task.m_TaskImpl)) return;

    if(!netVerify(!sm_CurTask->m_Waiting)) return;

    if(task.IsComplete()) return;

    task.m_TaskImpl->AddWaiter(sm_CurTask);
}

bool
netTask2::Init()
{
	sm_ThreadId = sysIpcGetCurrentThreadId();

	netDebug3("sizeof netTaskImpl = %" SIZETFMT "u (total: %" SIZETFMT "u)", sizeof(netTaskImpl), sizeof(netTaskImpl) * GetPool().capacity());
	netDebug3("sizeof netTaskWorkItem = %" SIZETFMT "u (total: %" SIZETFMT "u)", sizeof(netTaskWorkItem), sizeof(netTaskWorkItem) * GetWorkItemPool().capacity());

	// social club sdk - no TLS
	sm_CurTask = nullptr;
	sm_threadPool = &netThreadPool;
	return true;
}

void
netTask2::Update()
{
	PROFILE

    UpdateTask(&GetRootTask());

#if NETTASK2_POOL_PROFILING
	UpdatePoolUsage();
#endif // NETTASK2_POOL_PROFILING
}

#if NETTASK2_POOL_PROFILING
void
netTask2::UpdatePoolUsage()
{
	float poolUsage, workerUsage;

	{
		SYS_CS_SYNC(s_TaskPoolCs);
		poolUsage = (float)GetPool().size() / (float)GetPool().capacity() * 100;
	}
	
	{
		SYS_CS_SYNC(s_WorkerPoolCs);
		workerUsage = (float)GetWorkItemPool().size() / (float)GetWorkItemPool().capacity() * 100;
	}	

	if (poolUsage > m_TaskPoolMaxUsage)
	{
		m_TaskPoolMaxUsage = poolUsage;

		// output every time the task pool max usage increases by 20%
		static float lastPoolOutputUsage = 0;
		if (m_TaskPoolMaxUsage > lastPoolOutputUsage * 1.2)
		{
			netDebug3("NetTask2 max pool usage: %" SIZETFMT "d (%.2f%%)", GetPool().size(), m_TaskPoolMaxUsage);
			lastPoolOutputUsage = m_TaskPoolMaxUsage;
		}
	}

	if (workerUsage > m_WorkerPoolMaxUsage)
	{
		m_WorkerPoolMaxUsage = workerUsage;

		// output every time the work item pool max usage increases by 20%
		static float lastPoolOutputUsage = 0;
		if (m_WorkerPoolMaxUsage > lastPoolOutputUsage * 1.2)
		{
			netDebug3("NetTask2 max work item pool usage: %" SIZETFMT "d (%.2f%%)", GetPool().size(), m_WorkerPoolMaxUsage);
			lastPoolOutputUsage = m_WorkerPoolMaxUsage;
		}
	}
}
#endif // NETTASK2_POOL_PROFILING

bool
netTask2::IsCancelRequested()
{
    if (netVerifyf(sm_CurTask != nullptr, "IsCancelRequested can only be called on the netTask2::Update loop or netTaskWorkItem::DoWork function"))
	{
		return sm_CurTask->m_CancelRequested;
	}

	return false;
}

#if !__NO_OUTPUT

int 
netTask2::GetTaskId()
{
	if (netVerifyf(sm_CurTask != nullptr, "GetTaskId can only be called on the netTask2::Update loop or netTaskWorkItem::DoWork function"))
	{
		return sm_CurTask->GetTaskId();
	}

	return 0;
}

int
netTask2::GetParentTaskId()
{
	if (netVerifyf(sm_CurTask != nullptr, "GetParentId can only be called on the netTask2::Update loop or netTaskWorkItem::DoWork function"))
	{
		return sm_CurTask->GetParentTaskId();
	}

	return 0;
}


const char*
netTask2::GetTaskName()
{
	if (netVerifyf(sm_CurTask != nullptr, "GetTaskName can only be called on the netTask2::Update loop or netTaskWorkItem::DoWork function"))
	{
		return sm_CurTask->GetTaskName();
	}

	return "";
}

#if USE_PROFILER_NORMAL
const Profiler::EventDescription*
netTask2::GetTaskDescription()
{
	if(netVerifyf(sm_CurTask != nullptr, "GetTaskDescription can only be called on the netTask2::Update loop or netTaskWorkItem::DoWork function"))
	{
		return sm_CurTask->GetTaskDescription();
	}

	return nullptr;
}
#endif

const diagChannel*
netTask2::GetDiagChannel()
{
	if (netVerifyf(sm_CurTask != nullptr, "GetDiagChannel can only be called on the netTask2::Update loop or netTaskWorkItem::DoWork function"))
	{
		return sm_CurTask->GetDiagChannel();
	}

	return &Channel_ragenet_task2;
}

#endif // ! __NO_OUTPUT

//private:

netTaskImpl*
netTask2::AllocTask()
{
	SYS_CS_SYNC(s_TaskPoolCs);

    //TODO - issue warnings when task pool is low
    return GetPool().alloc();
}

void
netTask2::FreeTask(netTaskImpl* task)
{
	if (task->m_AsyncWorkItem != nullptr)
	{
		netTask2::FreeWorkItem(task->m_AsyncWorkItem);
		task->m_AsyncWorkItem = nullptr;
	}

	SYS_CS_SYNC(s_TaskPoolCs);
    GetPool().free(task);
}

netTaskWorkItem* 
netTask2::AllocWorkItem()
{
	SYS_CS_SYNC(s_WorkerPoolCs);
	return GetWorkItemPool().alloc();
}

void
netTask2::FreeWorkItem(netTaskWorkItem* workItem)
{
	SYS_CS_SYNC(s_WorkerPoolCs);
	GetWorkItemPool().free(workItem);
}

unsigned netTask2::NextId()
{
	return sysInterlockedIncrement(&s_NetTask2NextTaskId) - 1;
}

void
netTask2::UpdateTask(netTaskImpl* task)
{
	PROFILE

    netAssert(netTaskState::Pending == task->m_State);

	SYS_CS_SYNC(s_TaskChildCs);

#if USE_PROFILER_NORMAL
	if(task->GetTaskDescription())
	{
		PROFILER_CUSTOM_EVENT(task->GetTaskDescription());
	}
#endif

    //If there are child tasks then run all the child tasks
    //and skip running the parent task until all child tasks are complete.
    inlist<netTaskLink, &netTaskLink::m_Link>::iterator it = task->m_Children.begin();
    inlist<netTaskLink, &netTaskLink::m_Link>::const_iterator stop = task->m_Children.end();
    inlist<netTaskLink, &netTaskLink::m_Link>::iterator next = it;

    bool childCompleted = false;

    for(++next; stop != it; it = next, ++next)
    {
        netTaskImpl* child = it->m_Task;

        netAssert(task == child->m_Parent);

        UpdateTask(child);

        if(child->IsComplete())
        {
            task->m_Children.erase(it);
            child->m_Parent = nullptr;
            child->Unref();

            childCompleted = true;
        }
    }

    if(!task->m_Children.empty() && childCompleted && task->m_Wait1Child)
    {
        while(!task->m_Children.empty())
        {
            netTaskImpl* child = task->m_Children.front()->m_Task;
            task->m_Children.pop_front();
            child->m_Parent = nullptr;
            const netTaskState oldState = child->m_State;
            child->m_State = netTaskState::None;
            GetRootTask().AddChild(child);
            child->m_State = oldState;
            child->Unref();
        }

        task->m_Wait1Child = false;
    }

    //Run the parent task only if it's not waiting on another task and
    //no child tasks remain.
    if(!task->m_Waiting && task->m_Children.empty())
    {
		netAssertf(sm_CurTask == nullptr, "Multiple tasks running their update logic on the same thread simultaneously");
        sm_CurTask = task;
        task->m_State = task->m_OnUpdate();

		if (task->m_Status)
		{
			switch(task->m_State)
			{
			case netTaskState::Canceled:
				task->m_Status->SetCanceled();
				break;
			case netTaskState::Succeeded:
				task->m_Status->SetSucceeded();
				break;
			case netTaskState::Failed:
				task->m_Status->SetFailed();
				break;
			default:
				break;
			}
		}

        sm_CurTask = nullptr;
    }

    netAssert(netTaskState::None != task->m_State);

    if(!task->IsComplete())
    {
        return;
    }

#if !__NO_OUTPUT
	if (task->IsSucceeded())
	{
		netTask2ImplDebug(task, "SUCCEEDED (%ums executing, %ums in queue, %ums total)", task->TimeExecuting(), task->TimePending(), task->TimeSinceCreation());
	}
	else if (task->IsFailed())
	{
		netTask2ImplDebug(task, "FAILED (%ums executing, %ums in queue, %ums total)", task->TimeExecuting(), task->TimePending(), task->TimeSinceCreation());
	}
	else if (task->IsCanceled())
	{
		netTask2ImplDebug(task, "CANCELLED (%ums executing, %ums in queue, %ums total)", task->TimeExecuting(), task->TimePending(), task->TimeSinceCreation());
	}
	else
	{
		netTask2ImplDebug(task, "FINISHED (%ums executing, %ums in queue, %ums total)", task->TimeExecuting(), task->TimePending(), task->TimeSinceCreation());
	}
#endif

    for(auto waiter : task->m_Waiters)
    {
        waiter->m_Task->m_Waiting = false;
        waiter->m_Task->Unref();
    }

    task->m_Waiters.clear();

    //Antecedent task is done, now schedule continuations.

    //Move continuations to the parent's list of children.
    //Implies the parent won't run again until all of its children's
    //(and children's children's) continuations have completed.

    while(!task->m_Continuations.empty())
    {
        netTaskImpl* continuation = task->m_Continuations.front()->m_Task;
        task->m_Continuations.pop_front();

        netAssert(netTaskState::Pending == continuation->m_State);
        netAssert(task == continuation->m_Parent);
        netAssert(nullptr != task->m_Parent);

        const netTaskState oldState = continuation->m_State;
        continuation->m_State = netTaskState::None;
        continuation->m_Parent = nullptr;
        task->m_Parent->AddChild(continuation);
        continuation->m_State = oldState;

        continuation->Unref();
    }
}

netTask2::netTask2(netTaskImpl* taskImpl)
    : m_TaskImpl(taskImpl)
{
    if(m_TaskImpl)
    {
        m_TaskImpl->Ref();
    }
}

static netTaskImpl* s_pRootTask = nullptr;

netTaskImpl&
netTask2::GetRootTask()
{
    if(!s_pRootTask)
    {
        static netTaskImpl s_RootTask;
        s_RootTask.m_OnUpdate = []()
        {
            return netTaskState::Pending;
        };

		s_RootTask.SetPending();
        s_pRootTask = &s_RootTask;
    }

    return *s_pRootTask;
}

///////////////////////////////////////////////////////////////////////////////
// netTaskWorkItem
///////////////////////////////////////////////////////////////////////////////
void netTaskWorkItem::DoWork()
{
	PROFILER_CATEGORY("netTaskWorkItem", Profiler::CategoryColor::Network);

	netAssertf(netTask2::sm_CurTask == nullptr, "Multiple tasks running their update logic on the same thread simultaneously");
	netTask2::sm_CurTask = m_Owner;

	m_Result = netTaskState::Pending;

	// Run until the state is no longer pending
	while (m_Result == netTaskState::Pending)
	{		
		// If we're already canceled, we can skip doing the work and return canceled early.
		if (m_Owner->IsCanceled() || WasCanceled())
		{
			m_Result = netTaskState::Canceled;
		}
		else
		{
			// Get the result from the current work function
			m_Result = m_WorkFunction();
		}
	}

	netTask2::sm_CurTask = nullptr;
}

///////////////////////////////////////////////////////////////////////////////
//  netTaskImpl
///////////////////////////////////////////////////////////////////////////////
netTaskImpl::netTaskImpl()
	: m_State(netTaskState::None)
	, m_Parent(nullptr)
	, m_Link(this)
	, m_RefCount(0)
	, m_DeleteOnRelease(false)
	, m_CancelRequested(false)
	, m_Wait1Child(false)
	, m_Waiting(false)
	, m_AsyncWorkItem(nullptr)
	, m_Status(NULL)
{
#if !__NO_OUTPUT
	m_StartTime = 0;
	m_TaskId = 0;
	m_TaskName[0] = '\0';
	m_Channel = NULL;
	m_QueueTime = 0;
#if USE_PROFILER_NORMAL
	m_ProfilerDescription = nullptr;
#endif
#endif
}

netTaskImpl::~netTaskImpl()
{
	if (m_AsyncWorkItem != nullptr)
	{
		netTask2::FreeWorkItem(m_AsyncWorkItem);
		m_AsyncWorkItem = nullptr;
	}

    netAssert(0 == m_RefCount);
}

void
netTaskImpl::AddChild(netTaskImpl* task)
{
    if(!netVerify(task)) return;

    if(!netVerify(netTaskState::None == task->m_State)) return;

    if(!netVerify(nullptr == task->m_Parent)) return;

    if(!netVerify(netTaskState::None == m_State || netTaskState::Pending == m_State)) return;

    task->m_Parent = this;
    task->SetPending();
    task->Ref();

#if !__NO_OUTPUT
	netTask2ImplDebug(task, "Starting...");
	task->m_StartTime = sysTimer::GetSystemMsTime();
#endif

	SYS_CS_SYNC(s_TaskChildCs);
    m_Children.push_back(&task->m_Link);
}

void
netTaskImpl::AddContinuation(netTaskImpl* task)
{
    if(!netVerify(task)) return;

    if(!netVerify(netTaskState::None == task->m_State)) return;

    if(!netVerify(nullptr == task->m_Parent)) return;

    //If the antecedent task is already complete
    //the continuation should be scheduled immediately.
    //Find a parent task that has not completed and
    //schedule the continuation as a child of that task.
    if(IsComplete())
    {
        netTaskImpl* parent = m_Parent;
        while(parent && parent->IsComplete())
        {
            parent = parent->m_Parent;
        }

        netAssert(parent);

        //The parent will not complete until the continuation
        //task is complete.  This would have also been the case
        //if we could have added the continuation to the (completed)
        //target task - the ancestor task would have waited for
        //both the antecedent and the continuation to complete.
        parent->AddChild(task);
    }
    else
    {
    	task->m_Parent = this;
    	task->SetPending();
        task->Ref();

        m_Continuations.push_back(&task->m_Link);
    }
}

void
netTaskImpl::AddWaiter(netTaskImpl* task)
{
    if(!netVerify(task)) return;

    if(!netVerify(!IsComplete())) return;

    if(!netVerify(!task->m_Waiting)) return;

    task->Ref();
    task->m_Waiting = true;
    m_Waiters.push_back(&task->m_Link);
}

bool
netTaskImpl::IsComplete() const
{
    netAssert(netTaskState::None != m_State);
    return netTaskState::Pending != m_State;
}

bool
netTaskImpl::IsSucceeded() const
{
	netAssert(netTaskState::None != m_State);
	return netTaskState::Succeeded == m_State;
}

bool
netTaskImpl::IsFailed() const
{
	netAssert(netTaskState::None != m_State);
	return netTaskState::Failed == m_State;
}

bool
netTaskImpl::IsCanceled() const
{
	netAssert(netTaskState::None != m_State);
	return netTaskState::Canceled == m_State;
}


#if !__NO_OUTPUT

unsigned 
netTaskImpl::TimeSinceCreation() const
{
	return sysTimer::GetSystemMsTime() - m_QueueTime;
}

unsigned
netTaskImpl::TimePending() const
{
	if(m_StartTime > 0)
	{
		return m_StartTime - m_QueueTime;
	}
	else
	{
		return sysTimer::GetSystemMsTime() - m_QueueTime;
	}
}

unsigned
netTaskImpl::TimeExecuting() const
{
	return sysTimer::GetSystemMsTime() - m_StartTime;
}

const diagChannel* 
netTaskImpl::GetDiagChannel() const
{
	if (m_Channel)
	{
		return m_Channel;
	}
	else
	{
		return &Channel_ragenet_task2;
	}
}

const char*
netTaskImpl::GetTaskName() const
{
	return m_TaskName;
}

#if USE_PROFILER_NORMAL
const Profiler::EventDescription* 
netTaskImpl::GetTaskDescription() const
{
	return m_ProfilerDescription;
}
#endif

unsigned
netTaskImpl::GetTaskId() const
{
	return m_TaskId;
}

unsigned 
netTaskImpl::GetParentTaskId() const
{
	return m_Parent ? m_Parent->GetTaskId() : 0;
}

#endif // !__NO_OUTPUT

void 
netTaskImpl::SetPending()
{
	m_State = netTaskState::Pending;
	if (m_Status && !m_Status->Pending())
	{
		m_Status->SetPending();
	}
}

void
netTaskImpl::Cancel()
{
    if(m_CancelRequested) return;

    m_CancelRequested = true;

    //Cancel all continuations
    for(auto link : m_Continuations)
    {
        link->m_Task->Cancel();
    }

    //Cancel all children..
	SYS_CS_SYNC(s_TaskChildCs);
    for(auto link : m_Children)
    {
        link->m_Task->Cancel();
    }
}

void netTaskImpl::CancelByStatus(netStatus* status)
{
	// If the given status matches our own
	if (status == m_Status && status != nullptr)
	{
		Cancel();
		return;
	}

	// Otherwise, cancel any children tasks that match the status.
	SYS_CS_SYNC(s_TaskChildCs);
	for(auto link : m_Children)
	{
		if (link->m_Task)
		{
			link->m_Task->CancelByStatus(status);
		}
	}

	//Cancel all continuations
	for(auto link : m_Continuations)
	{
		if (link->m_Task)
		{
			link->m_Task->CancelByStatus(status);
		}
	}
}

void netTaskImpl::DetachTask(netStatus* status)
{
	if (status == m_Status)
	{
		m_Status = nullptr;
		return;
	}

	// Otherwise, detach any children tasks that match the status.
	SYS_CS_SYNC(s_TaskChildCs);
	for (auto link : m_Children)
	{
		if (link->m_Task)
		{
			link->m_Task->DetachTask(status);
		}
	}

	//Detach all continuations
	for (auto link : m_Continuations)
	{
		if (link->m_Task)
		{
			link->m_Task->DetachTask(status);
		}
	}
}


void
netTaskImpl::Ref()
{
    netAssert(m_RefCount >= 0);

    ++m_RefCount;
}

void
netTaskImpl::Unref()
{
    netAssert(m_RefCount > 0);

    if(--m_RefCount == 0 && m_DeleteOnRelease)
    {
        netTask2::FreeTask(this);
    }
}

///////////////////////////////////////////////////////////////////////////////
//  Tests
///////////////////////////////////////////////////////////////////////////////
static void TestChild();
static void TestContinuation();
static void TestGroup();
static void TestWait();
static void TestWhenAll();
static void TestWhenAny();
static void TestCancelWithChild();
static void TestCancelWithContinuation();
static void TestCancelWithGroup();
static void TestThreadPool();
static void TestThreadPoolWait();
static void TestNetStatus();
static void TestLogging();
static void TestComposite();
static void TestComposite2();

void netTask2::TestTask()
{
	TestChild();
	TestContinuation();
	TestGroup();
	TestWait();
	TestWhenAll();
	TestWhenAny();
	TestCancelWithChild();
	TestCancelWithContinuation();
	TestCancelWithGroup();
	TestThreadPool();
	TestThreadPoolWait();
	TestNetStatus();
	TestLogging();

    TestComposite();
    TestComposite2();
}

static void TestChild()
{
    int state = 0;

    //Start a task that will have a child task.
    //The parent task will not continue until the child task completes.

    auto parentTask = netTask2::ScheduleTask([&state]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        int childState;

        switch(state)
        {
        case 0:
            state = 1;
            childState = 0;

            //Create a child task.
            //We'll go to sleep until the child task completes.

            //Declaring the lambda "mutable" allows us to modify captured state.
            //The captured variable "childState" becomes similar to a member
            //variable for the lambda.  It maintains state between lambda invocations.
            netTask2::ScheduleChildTask([childState]() mutable
            {
                if(netTask2::IsCancelRequested())
                {
                    return netTaskState::Canceled;
                }

                while(childState < 5)
                {
                    ++childState;
                    return netTaskState::Pending;
                }

                return netTaskState::Succeeded;
            }, NULL, TASK2_DECL(TestChild, ragenet_task2));

            return netTaskState::Pending;
            break;
        case 1:
            //If we reach this state then the child task completed.
            ++state;
            return netTaskState::Pending;
            break;
        case 2:
            ++state;
            return netTaskState::Pending;
            break;
        case 3:
            ++state;
            return netTaskState::Succeeded;
            break;
        }

        return netTaskState::Succeeded;
    });

    while(!parentTask.IsComplete())
    {
        netTask2::Update();
    }

    netAssert(4 == state);
}

static void TestContinuation()
{
    int x = 0, y = 0, z = 0;

    //Create task A.

    auto A = netTask2::ScheduleTask([&x]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        while(x < 10)
        {
            ++x;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestContinuationA, ragenet_task2));

    //Add a continuation to task A, then add a continuation to that.
    //C will refer to the third task in the chain.
    //The continuation between A and C is unnamed.
    //A -> ___ -> C.
    //When C completes then both A and the unnamed tasks will have completed.

    auto C =

    //Unnamed continuation
    A.Then([&y]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        while(y < 15)
        {
            ++y;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestContinuationB, ragenet_task2))
    //Continuation that's assigned to C.
    .Then([&z]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        while(z < 20)
        {
            ++z;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestContinuationC, ragenet_task2));

    while(!C.IsComplete())
    {
        netTask2::Update();
    }

    netAssert(A.IsComplete());
    netAssert(10 == x);
    netAssert(15 == y);
    netAssert(20 == z);
}

static void TestGroup()
{
    int x = 0, y = 0, z = 0;

    //Create a dummy task as the grouper.
    //The dummy task won't run until all child tasks
    //complete, at which time the dummy task will complete.
    auto G = netTask2::CreateTask([]()
    {
        return netTaskState::Succeeded;
    });

    //Create child tasks A, B, C.

    auto A = netTask2::CreateTask([&x]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        while(x < 10)
        {
            ++x;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestGroupA, ragenet_task2));

    auto B = netTask2::CreateTask([&y]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        while(y < 15)
        {
            ++y;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestGroupB, ragenet_task2));

    auto C = netTask2::CreateTask([&z]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        while(z < 20)
        {
            ++z;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestGroupC, ragenet_task2));

    //Add the child tasks to the group (parent) task.
    G.AddChildTask(A);
    G.AddChildTask(B);
    G.AddChildTask(C);

    //Schedule the group task to run.
    netTask2::ScheduleTask(G);

    //Run until G completes, implying A, B, and C have also competed.
    while(!G.IsComplete())
    {
        netTask2::Update();
    }

    netAssert(A.IsComplete());
    netAssert(B.IsComplete());
    netAssert(C.IsComplete());
    netAssert(10 == x);
    netAssert(15 == y);
    netAssert(20 == z);
}

static void TestWait()
{
    int state = 0;

    //Start a task that will wait for another task.
    //The waiter task will not continue until the awaited task completes.

    auto waiterTask = netTask2::ScheduleTask([&state]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        int awaitedState;

        switch(state)
        {
        case 0:
            state = 1;
            awaitedState = 0;

            //Create a task that will be awaited.
            //We'll go to sleep until the awaited task completes.
            {
                auto awaitedTask = netTask2::ScheduleTask([awaitedState]() mutable
                {
                    if(netTask2::IsCancelRequested())
                    {
                        return netTaskState::Canceled;
                    }

                    while(awaitedState < 5)
                    {
                        ++awaitedState;
                        return netTaskState::Pending;
                    }

                    return netTaskState::Succeeded;
                }, NULL, TASK2_DECL(TestWaitA, ragenet_task2));

                netTask2::Wait(awaitedTask);
            }

            return netTaskState::Pending;
            break;
        default:
            ++state;
            break;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestWaitB, ragenet_task2));

    while(!waiterTask.IsComplete())
    {
        netTask2::Update();
    }

    netAssert(2 == state);
}

sysCriticalSectionToken s_TestCs;
static void TestWhenAll()
{
    const int numTasks = 10;
    netTask2 tasks[numTasks];
    int states[numTasks] = {0};

    for(int i = 0; i < numTasks; ++i)
    {
        states[i] = 0;

        tasks[i] = netTask2::ScheduleTask([&states, i]()
        {
			SYS_CS_SYNC(s_TestCs);

            if(netTask2::IsCancelRequested())
            {
                return netTaskState::Canceled;
            }

            if(++states[i] == (i+1)*10)
            {
                return netTaskState::Succeeded;
            }

            return netTaskState::Pending;
        }, NULL, TASK2_DECLT(TestWhenAllA, ragenet_task2));
    }

    bool waiterRan = false;

    auto waiterFunc = [&waiterRan]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        waiterRan = true;

        return netTaskState::Succeeded;
    };

	netTask2 waiterTask = netTask2::WhenAll(tasks, numTasks, waiterFunc);

    //Test canceling the waiter.  The waitables should not have been canceled.
    waiterTask.Cancel();

    //The waiter won't realize it's been canceled until it has a chance to run,
    //which is only after all waitables have completed.
    while(!waiterTask.IsComplete())
    {
        netTask2::Update();
    }

    //The waiter was canceled before it could complete.
    netAssert(!waiterRan);

	// Run the waiters to completion
	bool allComplete = false;
	while(!allComplete)
	{
		netTask2::Update();

		allComplete = true;
		for (int i = 0; i < numTasks; i++)
		{
			if (!tasks[i].IsComplete())
				allComplete = false;
		}
	}

    //The waitables should have run to completion.
    for(int i = 0; i < numTasks; ++i)
    {
        netAssert(tasks[i].IsComplete());
        netAssert(states[i] == (i+1)*10);
    }

    //Recreate the waitable tasks.
    for(int i = 0; i < numTasks; ++i)
    {
        states[i] = 0;

        tasks[i] = netTask2::ScheduleTask([&states, i]()
        {
            if(netTask2::IsCancelRequested())
            {
                return netTaskState::Canceled;
            }

            if(++states[i] == (i+1)*10)
            {
                return netTaskState::Succeeded;
            }

            return netTaskState::Pending;
        }, NULL, TASK2_DECL(TestWhenAllB, ragenet_task2));
    }

    //Recreate the waiter and let it run to completion
    waiterTask = netTask2::WhenAll(tasks, numTasks, waiterFunc);

    while(!waiterTask.IsComplete())
    {
        netTask2::Update();
    }

    //The waiter should have run to completion.
    netAssert(waiterRan);

    //The waitables should have all run to completion.
    for(int i = 0; i < numTasks; ++i)
    {
        netAssert(tasks[i].IsComplete());
        netAssert(states[i] == (i+1)*10);
    }
}

static void TestWhenAny()
{
    const int numTasks = 10;
    netTask2 tasks[numTasks];
    int states[numTasks] = {0};
    for(int i = 0; i < numTasks; ++i)
    {
        tasks[i] = netTask2::ScheduleTask([&states, i]()
        {
            if(netTask2::IsCancelRequested())
            {
                return netTaskState::Canceled;
            }

            if(++states[i] == (i+1)*10)
            {
                return netTaskState::Succeeded;
            }

            return netTaskState::Pending;
        }, NULL, TASK2_DECL(TestWhenAnyA, ragenet_task2));
    }

    bool waiterRan = false;

    auto waiterFunc = [&waiterRan]()
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        waiterRan = true;

        return netTaskState::Succeeded;
    };

    netTask2 waiterTask = netTask2::WhenAny(tasks, numTasks, waiterFunc);

    //Test canceling the waiter.  The waitables should not have been canceled.
    waiterTask.Cancel();

    //The waiter won't realize it's been canceled until it has a chance to run,
    //which is only after one of the waitables has completed.
    while(!waiterTask.IsComplete())
    {
        netTask2::Update();
    }

    //The waiter was canceled before it could complete.
    netAssert(!waiterRan);

    //Task 0 completed, the rest didn't
    netAssert(tasks[0].IsComplete());
    netAssert(states[0] == 10);

    for(int i = 1; i < numTasks; ++i)
    {
        netAssert(!tasks[i].IsComplete());
        int expected = 10;
        //One extra iteration was run before the waiter realized it was canceled.
        expected += 1;
        netAssert(states[i] == expected);
    }

    //Let the waitable tasks run to completion.
    bool complete = false;
    while(!complete)
    {
        netTask2::Update();
        complete = true;
        for(int i = 0; i < numTasks && complete; ++i)
        {
            complete = complete && tasks[i].IsComplete();
        }
    }

    for(int i = 0; i < numTasks; ++i)
    {
        netAssert(tasks[i].IsComplete());
        NET_ASSERTS_ONLY(int expected = (i+1)*10);
        netAssert(states[i] == expected);
    }

    //Recreate the waitable tasks.
    for(int i = 0; i < numTasks; ++i)
    {
        states[i] = 0;

        tasks[i] = netTask2::ScheduleTask([&states, i]()
        {
            if(netTask2::IsCancelRequested())
            {
                return netTaskState::Canceled;
            }

            if(++states[i] == (i+1)*10)
            {
                return netTaskState::Succeeded;
            }

            return netTaskState::Pending;
        }, NULL, TASK2_DECL(TestWhenAnyB, ragenet_task2));
    }

    //Recreate the waiter and let it run to completion
    waiterTask = netTask2::WhenAny(tasks, numTasks, waiterFunc);

    while(!waiterTask.IsComplete())
    {
        netTask2::Update();
    }

    //The waiter should have run to completion.
    netAssert(waiterRan);

    //Task 0 completed, the rest didn't
    netAssert(tasks[0].IsComplete());
    netAssert(states[0] == 10);

    for(int i = 1; i < numTasks; ++i)
    {
        netAssert(!tasks[i].IsComplete());
        int expected = 10;
        //One extra iteration was run before the waiter completed.
        expected += 1;
        netAssert(states[i] == expected);
    }

    //Let the waitable tasks run to completion.
    complete = false;
    while(!complete)
    {
        netTask2::Update();
        complete = true;
        for(int i = 0; i < numTasks && complete; ++i)
        {
            complete = complete && tasks[i].IsComplete();
        }
    }

    for(int i = 0; i < numTasks; ++i)
    {
        netAssert(tasks[i].IsComplete());
        NET_ASSERTS_ONLY(int expected = (i+1)*10);
        netAssert(states[i] == expected);
    }
}

static void TestCancelWithChild()
{
    //Start a task with a child task.
    //Cancel the parent task.  The child task
    //should also be canceled.

    int state = 0;
    int x = 0, y = 0;
    bool parentCanceled = false, childCanceled = false;

    auto parentTask = netTask2::ScheduleTask([&state, &x, &y, &parentCanceled, &childCanceled]()
    {
        if(netTask2::IsCancelRequested())
        {
            parentCanceled = true;
            return netTaskState::Canceled;
        }

        switch(state)
        {
        case 0:
            state = 1;

            netTask2::ScheduleChildTask([&y, &childCanceled]()
            {
                if(netTask2::IsCancelRequested())
                {
                    childCanceled = true;
                    return netTaskState::Canceled;
                }

                while(y < 10)
                {
                    ++y;
                    return netTaskState::Pending;
                }

                return netTaskState::Succeeded;
            }, NULL, TASK2_DECL(TestCancelChild, ragenet_task2));

            return netTaskState::Pending;
            break;
        case 1:
            //If we reach this state then the child task completed.
            while(x < 10)
            {
                ++x;
                return netTaskState::Pending;
            }
            ++state;
            break;
        case 2:
            break;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestCancelParent, ragenet_task2));

    //After 5 iterations cancel the parent.
    //The child should also be canceled as a result.
    while(!parentTask.IsComplete())
    {
        for(int i = 0; i < 5; ++i)
        {
            netTask2::Update();
        }

        parentTask.Cancel();
    }

    netAssert(parentCanceled);
    netAssert(childCanceled);
    netAssert(1 == state);
    netAssert(0 == x);
    netAssert(4 == y);  //Child was canceled before y reached 10
}

static void TestCancelWithContinuation()
{
    int x = 0, y = 0, z = 0;

    //Start a task and add continuations.
    //Cancel the antecedent task.
    //The continuations should also have been canceled.

    bool canceledA = false, canceledB = false, canceledC = false;

    auto A = netTask2::ScheduleTask([&x, &canceledA]()
    {
        if(netTask2::IsCancelRequested())
        {
            canceledA = true;
            return netTaskState::Canceled;
        }

        while(x < 10)
        {
            ++x;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestCancelA, ragenet_task2));

    auto B = A.Then([&y, &canceledB]()
    {
        if(netTask2::IsCancelRequested())
        {
            canceledB = true;
            return netTaskState::Canceled;
        }

        while(y < 15)
        {
            ++y;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestCancelB, ragenet_task2));

    auto C = B.Then([&z, &canceledC]()
    {
        if(netTask2::IsCancelRequested())
        {
            canceledC = true;
            return netTaskState::Canceled;
        }

        while(z < 20)
        {
            ++z;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestCancelC, ragenet_task2));

    //After 5 iterations cancel A.
    //B and C should never run, except to recognize they've been canceled
    while(!A.IsComplete())
    {
        for(int i = 0; i < 5; ++i)
        {
            netTask2::Update();
        }

        A.Cancel();
    }

    netAssert(A.IsComplete());
    netAssert(B.IsComplete());
    netAssert(C.IsComplete());
    netAssert(canceledA);
    netAssert(canceledB);
    netAssert(canceledC);
    netAssert(5 == x);
    netAssert(0 == y);
    netAssert(0 == z);
}

static void TestCancelWithGroup()
{
    int x = 0, y = 0, z = 0;
    bool canceledA = false, canceledB = false, canceledC = false;

    //Create a group of tasks and cancel the group.
    //All tasks in the group should also be canceled.

    auto G = netTask2::CreateTask([]()
    {
        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestCancelGroup, ragenet_task2));

    auto A = netTask2::CreateTask([&x, &canceledA]()
    {
        if(netTask2::IsCancelRequested())
        {
            canceledA = true;
            return netTaskState::Canceled;
        }

        while(x < 10)
        {
            ++x;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestCancelGroupA, ragenet_task2));

    auto B = netTask2::CreateTask([&y, &canceledB]()
    {
        if(netTask2::IsCancelRequested())
        {
            canceledB = true;
            return netTaskState::Canceled;
        }

        while(y < 15)
        {
            ++y;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestCancelGroupB, ragenet_task2));

    auto C = netTask2::CreateTask([&z, &canceledC]()
    {
        if(netTask2::IsCancelRequested())
        {
            canceledC = true;
            return netTaskState::Canceled;
        }

        while(z < 20)
        {
            ++z;
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    }, NULL, TASK2_DECL(TestCancelGroupC, ragenet_task2));

    G.AddChildTask(A);
    G.AddChildTask(B);
    G.AddChildTask(C);

    netTask2::ScheduleTask(G);

    //After 5 iterations cancel G.
    //A, B, and C should have also run for 5 iterations.
    while(!G.IsComplete())
    {
        for(int i = 0; i < 5; ++i)
        {
            netTask2::Update();
        }

        G.Cancel();
    }

    netAssert(G.IsComplete());
    netAssert(A.IsComplete());
    netAssert(B.IsComplete());
    netAssert(C.IsComplete());
    netAssert(canceledA);
    netAssert(canceledB);
    netAssert(canceledC);
    netAssert(5 == x);
    netAssert(5 == y);
    netAssert(5 == z);
}

static void TestThreadPool()
{
	static const unsigned TASK_DURATION = 1000;
	const unsigned CANCEL_TIME = TASK_DURATION / 2;

	netStatus sa, sb, sc;

	auto blockingTask = netTask2::ScheduleTask([]()
	{
		sysIpcSleep(TASK_DURATION);
		return netTaskState::Succeeded;
	}, &sa, TASK2_DECLT(ThreadPoolTask1,ragenet_task2))
	.Then([]()
	{
		sysIpcSleep(TASK_DURATION);
		return netTaskState::Succeeded;
	}, &sc, TASK2_DECL(ThreadPoolTask2,ragenet_task2));

	auto blockingTaskCancel = netTask2::ScheduleTask([]()
	{
		AUTO_CANCEL();
		sysIpcSleep(TASK_DURATION);
		netTask2Debug("BlockingTask Sleep #1 completed.");
		
		AUTO_CANCEL();
		sysIpcSleep(TASK_DURATION);
		netTask2Debug("BlockingTask Sleep #2 completed.");

		return netTaskState::Succeeded;
	}, &sb, TASK2_DECLT(ThreadPoolTask3,ragenet_task2));

	u32 startTime = sysTimer::GetSystemMsTime();
	
	// Wait for both tasks to complete
	while (sb.Pending() || sa.Pending() || sc.Pending())
	{
		netTask2::Update();

		// Cancel the second task half-way through.
		if (sysTimer::HasElapsedIntervalMs(startTime, CANCEL_TIME))
		{
			blockingTaskCancel.Cancel();
		}
	}

	// the first task should have succeeded, the second was canceled
	netAssert(blockingTask.IsSucceeded());
	netAssert(blockingTaskCancel.IsCanceled());
}

static void TestThreadPoolWait()
{
	int state = 0;

	//Start a task that will wait for another task.
	//The waiter task will not continue until the awaited task completes.

	auto waiterTask = netTask2::ScheduleTask([&state]()
	{
		if(netTask2::IsCancelRequested())
		{
			return netTaskState::Canceled;
		}

		int awaitedState;

		switch(state)
		{
		case 0:
			state = 1;
			awaitedState = 0;

			//Create a task that will be awaited.
			//We'll go to sleep until the awaited task completes.
			{
				auto awaitedTask = netTask2::ScheduleTask([awaitedState]() mutable
				{
					if(netTask2::IsCancelRequested())
					{
						return netTaskState::Canceled;
					}

					while(awaitedState < 3)
					{
						++awaitedState;
						sysIpcSleep(100);
						return netTaskState::Pending;
					}

					return netTaskState::Succeeded;
				}, NULL, TASK2_DECLT(ThreadPoolWaited, ragenet_task2));

				netTask2::Wait(awaitedTask);
			}

			return netTaskState::Pending;
			break;
		default:
			++state;
			break;
		}

		return netTaskState::Succeeded;
	}, NULL, TASK2_DECL(ThreadPoolWaiter, ragenet_task2));

	while(!waiterTask.IsComplete())
	{
		netTask2::Update();
	}

	netAssert(2 == state);
}

void TestNetStatus()
{
	netStatus sa, sb, sc, sd;

	auto ta = netTask2::ScheduleTask([]()
	{
		return netTaskState::Succeeded;
	}, &sa)
	.Then([]()
	{
		return netTaskState::Succeeded;
	}, &sb);

	auto tb = netTask2::ScheduleTask([]()
	{
		return netTaskState::Failed;
	}, &sc);

	auto tc = netTask2::ScheduleTask([]()
	{
		return netTaskState::Canceled;
	}, &sd);

	while(sa.Pending() || sb.Pending() || sc.Pending() || sd.Pending())
	{
		netTask2::Update();
	}

	netAssert(sa.Succeeded());
	netAssert(sb.Succeeded());
	netAssert(sc.Failed());
	netAssert(sd.Canceled());
}

void TestLogging()
{
	netStatus status;

	auto a = netTask2::ScheduleTask([]()
	{
		netTask2Debug("Channeled Task Output");
		return netTaskState::Succeeded;
	}, &status, TASK2_DECL(TestLogging,ragenet_task2));

	while(status.Pending())
	{
		netTask2::Update();
	}

	netAssert(a.IsSucceeded() && status.Succeeded());
}

static void TestComposite()
{
    int state = 0, x = 0;

    auto compositeTask = netTask2::ScheduleTask([&state, &x]() mutable
    {
        if(netTask2::IsCancelRequested())
        {
            return netTaskState::Canceled;
        }

        switch(state)
        {
        case 0:
            netTask2::ScheduleChildTask([&x]()
            {
                while(x++ < 3)
                {
                    return netTaskState::Pending;
                }

                return netTaskState::Succeeded;
            });
            ++state;
            break;
        case 1:
            netAssert(4 == x);
            netTask2::ScheduleChildTask([&x]()
            {
                while(x++ < 6)
                {
                    return netTaskState::Pending;
                }

                return netTaskState::Succeeded;
            });
            ++state;
            break;
        case 2:
            netAssert(7 == x);
            netTask2::ScheduleChildTask([&x]()
            {
                while(x++ < 9)
                {
                    return netTaskState::Pending;
                }

                return netTaskState::Succeeded;
            });
            ++state;
            break;
        case 3:
            netAssert(10 == x);
            return netTaskState::Succeeded;
            break;
        }

        return netTaskState::Pending;
    });

    while(!compositeTask.IsComplete())
    {
        netTask2::Update();
    }

    netAssert(3 == state);
    netAssert(10 == x);
}

static void TestComposite2()
{
    int x = 0;

    auto subtask1 = netTask2::ScheduleTask([&x]()
    {
        while(x++ < 3)
        {
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    });

    auto subtask2 = subtask1.Then([&x, subtask1]()
    {
        if(subtask1.IsFailed()) return netTaskState::Failed;

        while(x++ < 6)
        {
            return netTaskState::Pending;
        }

        return netTaskState::Failed;
    });

    auto subtask3 = subtask2.Then([&x, subtask2]()
    {
        if(subtask2.IsFailed()) return netTaskState::Failed;

        while(x++ < 9)
        {
            return netTaskState::Pending;
        }

        return netTaskState::Succeeded;
    });

    auto compositeTask = subtask3.Then([subtask3]()
    {
        if(subtask3.IsFailed()) return netTaskState::Failed;

        return netTaskState::Succeeded;
    });

    while(!compositeTask.IsComplete())
    {
        netTask2::Update();
    }

    netAssert(compositeTask.IsFailed());
    netAssert(7 == x);
}

}   //namespace rage
