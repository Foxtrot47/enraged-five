// 
// net/nethardware_orbis.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#if RSG_SCE

#include "nethardware.h"
#include "natdetector.h"
#include "netdiag.h"
#include "netsocket.h"
#include "diag/seh.h"
#include "file/winsock.h"

#include "system/ipc.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/timer.h"

#include <arpa/inet.h>
#include <libnetctl.h>
#include <libsysmodule.h>
#include <netinet/in.h>
#include <np.h>
#include <sys/socket.h>

namespace rage
{
static netIpAddress sm_PublicIp;

RAGE_DECLARE_SUBCHANNEL(ragenet, hardware)
#undef __rage_channel
#define __rage_channel ragenet_hardware

extern SceUserServiceUserId g_initialUserId;

//Helper function defined in winsock.cpp
const char* netGetSceNetErrorString(const int code);

class netSceNetWorker
{

public:

	static const unsigned THREAD_STACK_SIZE = 4 * 1024; 
	static const unsigned CHECK_CONNECTED_INTERVAL = 1 * 1000; 
	static const unsigned MTU_QUERY_INTERVAL_MS = 1 * 5000;
	static const unsigned DEVICE_QUERY_INTERVAL_MS = 1 * 60000;
	static const unsigned SCE_NET_WORKER_SLEEP_INTERVAL = 30; 

	netSceNetWorker()
		: m_ThreadHandle(sysIpcThreadIdInvalid)
		, m_WaitThreadSema(0)
		, m_Finished(false)
		, m_LastLinkConnectedCheck(0)
		, m_LastMtuQueryTime(0)
		, m_LastDeviceQueryTime(0)
		, m_IsConnected(false)
		, m_Mtu(-1)
		, m_Device(REACHABILITY_NONE)
		, m_SceNetCtlHandlerId(-1)
		, m_SceNetCtlState(-1)
	{

	}

	bool Init()
	{
		bool success = false; 

		rtry
		{
			// make sure we haven't already created this thread
			rverify(m_ThreadHandle == sysIpcThreadIdInvalid, alreadystarted,);
		
			// set control flag
			m_Finished = false;

			m_WaitThreadSema = sysIpcCreateSema(0);
			rverify(m_WaitThreadSema, catchall,);

			m_ThreadHandle = sysIpcCreateThread(&netSceNetWorker::ThreadFunc,
												this,
												THREAD_STACK_SIZE,
												PRIO_NORMAL,
												"[net] SceNet",
												-1, 
												"SceNetWorker");

			rverify(m_ThreadHandle != sysIpcThreadIdInvalid, catchall,);

			// wait for the thread to start
			sysIpcWaitSema(m_WaitThreadSema);

			// all good
			success = true; 
		}
		rcatch(alreadystarted)
		{
			// don't clean up here
		}
		rcatchall
		{
			if(m_WaitThreadSema)
			{
				sysIpcDeleteSema(m_WaitThreadSema);
				m_WaitThreadSema = 0;
			}
		}

		return success; 
	}

	void Shutdown()
	{
		if(m_ThreadHandle == sysIpcThreadIdInvalid)
			return;

		if(!m_Finished)
		{
			m_Finished = true;

			// wait for the thread to complete.
			sysIpcWaitSema(m_WaitThreadSema);
			sysIpcWaitThreadExit(m_ThreadHandle);
			sysIpcDeleteSema(m_WaitThreadSema);
			m_ThreadHandle = sysIpcThreadIdInvalid;
		}
	}

	void QueryLink()
	{
		if(sysTimer::HasElapsedIntervalMs(this->m_LastLinkConnectedCheck, CHECK_CONNECTED_INTERVAL))
		{
			// we populate this structure with a call to sceNetCtlGetInfo
			SceNetCtlInfo sceNetInfo;

			// a negative result indicates an error
			const int result = sceNetCtlGetInfo(SCE_NET_CTL_INFO_LINK, &sceNetInfo);
			if(result < 0)
			{
				netError("QueryLink :: Error calling sceNetCtlGetInfo(SCE_NET_CTL_INFO_LINK) - 0x%08x %s", result, netGetSceNetErrorString(result));
			}

			// we need a positive result (SCE_NET_CTL_LINK_CONNECTED) and 
			// the local IP and MAC address to be valid
			u8 mac[6];
			netIpAddress ip;
			bool isConnected = sceNetInfo.link == SCE_NET_CTL_LINK_CONNECTED &&
				netHardware::GetMacAddress(mac) &&
				netHardware::GetLocalIpAddress(&ip);

			// update connected state
			sysInterlockedExchange((unsigned int*)&this->m_IsConnected, (unsigned int)isConnected);

			// update checked time
			this->m_LastLinkConnectedCheck = sysTimer::GetSystemMsTime();
		}
	}

	void QueryMtu()
	{
		if(sysTimer::HasElapsedIntervalMs(this->m_LastMtuQueryTime, MTU_QUERY_INTERVAL_MS))
		{
			SceNetCtlInfo ctl_info;
			const int result = sceNetCtlGetInfo(SCE_NET_CTL_INFO_MTU, &ctl_info);
			if(result < 0)
			{
				// if an error occurs, retain the last known value
				netError("QueryMtu :: Error calling sceNetCtlGetInfo(SCE_NET_CTL_INFO_MTU) - 0x%08x %s", result, netGetSceNetErrorString(result));
			}
			else
			{
				sysInterlockedExchange((unsigned int*)&this->m_Mtu, (unsigned int)ctl_info.mtu);
			}

			// update checked time
			this->m_LastMtuQueryTime = sysTimer::GetSystemMsTime();
		}
	}

	void QueryDevice()
	{
		if(sysTimer::HasElapsedIntervalMs(this->m_LastDeviceQueryTime, DEVICE_QUERY_INTERVAL_MS))
		{
			SceNetCtlInfo ctl_info;
			const int result = sceNetCtlGetInfo(SCE_NET_CTL_INFO_DEVICE, &ctl_info);
			if(result < 0)
			{
				// if an error occurs, retain the last known value
				netError("QueryDevice :: Error calling sceNetCtlGetInfo(SCE_NET_CTL_INFO_DEVICE) - 0x%08x %s", result, netGetSceNetErrorString(result));
			}
			else
			{
				netHardwareReachability device = REACHABILITY_NONE;

				if(ctl_info.device == SCE_NET_CTL_DEVICE_WIRED)
				{
					device = REACHABILITY_VIA_WIRED;
				}
				else if(ctl_info.device == SCE_NET_CTL_DEVICE_WIRELESS)
				{
					device = REACHABILITY_VIA_WIFI;
				}

				sysInterlockedExchange((unsigned int*)&this->m_Device, (unsigned int)device);
			}

			// update checked time
			this->m_LastDeviceQueryTime = sysTimer::GetSystemMsTime();
		}
	}

	static void ThreadFunc(void* p)
	{
		netSceNetWorker* worker = (netSceNetWorker*)p;

		// signal that we've started
		sysIpcSignalSema(worker->m_WaitThreadSema);

		// stay here until something indicates we need to finish
		while(!worker->m_Finished)
		{
			{
				SYS_CS_SYNC(worker->m_Cs);

				// this needs pumped regularly
				sceNetCtlCheckCallback();

				worker->QueryLink();
				worker->QueryMtu();
				worker->QueryDevice();
			}

			// sleep for given interval
			sysIpcSleep(SCE_NET_WORKER_SLEEP_INTERVAL);
		}

		// signal that we've finished
		sysIpcSignalSema(worker->m_WaitThreadSema);
	}

	// handler for connection state transitions, as reported by OS.
	static void NetCtlHandler(int event_type, void* p)
	{
		netSceNetWorker* worker = (netSceNetWorker*)p;

		// update tracked state
		sysInterlockedExchange((unsigned int*)&worker->m_SceNetCtlState, (unsigned int)event_type);

		switch (event_type)
		{
		case SCE_NET_CTL_STATE_DISCONNECTED:
			{
				netDebug2("Hardware connection state changed to Disconnected");
				sysInterlockedExchange((unsigned int*)&worker->m_IsConnected, 0); // clear connected flag

				int result = 0;
				int ret = sceNetCtlGetResult(event_type, &result);
				if (ret == 0)
				{
					switch (result)
					{
					case SCE_NET_CTL_ERROR_NETWORK_DISABLED:
						netDebug1("Disconnected - Network disabled due to settings");
						break;
					case SCE_NET_CTL_ERROR_DISCONNECT_REQ:
						netDebug1("Disconnected - Network disconnected due to disconnection request");
						break;
					case SCE_NET_CTL_ERROR_ETHERNET_PLUGOUT:
						netDebug1("Disconnected - Ethernet cable was removed");
						break;
					case SCE_NET_CTL_ERROR_WIFI_DEAUTHED:
						netDebug1("Disconnected from access point");
						break;
					case SCE_NET_CTL_ERROR_WIFI_BEACON_LOST:
						netDebug1("Disconnected - Can no longer receive beacon from access point");
						break;
					default:
						netDebug1("Disconnected - unknown reason %d", result);
						break;
					}
				}
			}
			break;

		case SCE_NET_CTL_STATE_CONNECTING:
			netDebug2("Hardware connection state changed to Connecting");
			sysInterlockedExchange((unsigned int*)&worker->m_IsConnected, 0); // clear connected flag
			break;

		case SCE_NET_CTL_STATE_IPOBTAINING:
			netDebug2("Hardware connection state changed to IPObtaining");
			sysInterlockedExchange((unsigned int*)&worker->m_IsConnected, 0); // clear connected flag
			break;

		case SCE_NET_CTL_STATE_IPOBTAINED:
			netDebug2("Hardware connection state changed to IPObtained");
			// m_IsConnected to be set in update loop once IP address and MAC are available
			break;

		default:
			netError("Unknown connection state");
			break;
		}
	}

	bool StartNetwork()
	{
		SYS_CS_SYNC(m_Cs);
		
		netAssert(m_SceNetCtlHandlerId == -1);
		netAssert(m_SceNetCtlState == -1);

		int result;
		
		// do an immediate check for SCE_NET_CTL_STATE_IPOBTAINED, because
		// sometimes Orbis can startup initialized (at least in development).
		int sceNetCtlState;
		result = sceNetCtlGetState(&sceNetCtlState);
		if(result != SCE_OK)
		{
			netError("Error calling sceNetCtlGetState():0x%08x %s", result, netGetSceNetErrorString(result));
			return false; 
		}
		
		// update tracked state
		sysInterlockedExchange((unsigned int*)&m_SceNetCtlState, (unsigned int)sceNetCtlState);

		// Orbis network startup goes through multiple states, and we can't be
		// considered "available" until we are in the SCE_NET_CTL_STATE_IPOBTAINED state.
		// We register a callback to detect the transitions.
		result = sceNetCtlRegisterCallback(NetCtlHandler, this, &m_SceNetCtlHandlerId);
		if(result != SCE_OK)
		{
			netError("Error calling sceNetCtlRegisterCallback():0x%08x %s", result, netGetSceNetErrorString(result));
			return false; 
		}

		return true;
	}

	bool StopNetwork()
	{
		SYS_CS_SYNC(m_Cs);

		netAssert(m_SceNetCtlHandlerId != -1);
		
		int result = sceNetCtlUnregisterCallback(m_SceNetCtlHandlerId);
		if(result != SCE_OK)
		{
			netError("Error calling sceNetCtlUnregisterCallback():0x%08x %s", result, netGetSceNetErrorString(result));
			return false; 
		}

		m_SceNetCtlHandlerId = -1; 
		m_SceNetCtlState = -1; 

		return true;
	}

	bool IsConnected()
	{
		return m_IsConnected;
	}

	bool GetMtu(u32& mtu)
	{
		mtu = m_Mtu;
		return m_Mtu >= 0;
	}

	bool GetDevice(netHardwareReachability& device)
	{
		device = m_Device;
		return device != REACHABILITY_NONE;
	}

	int GetSceNetCtlState()
	{
		return m_SceNetCtlState;
	}

private:

	sysIpcThreadId			m_ThreadHandle;
	sysIpcSema				m_WaitThreadSema;
	bool					m_Finished; 
	sysCriticalSectionToken m_Cs;
	int						m_SceNetCtlHandlerId;
	int						m_SceNetCtlState;
	unsigned				m_LastLinkConnectedCheck;
	unsigned				m_LastMtuQueryTime;
	unsigned				m_LastDeviceQueryTime;
	bool					m_IsConnected;
	int						m_Mtu;
	netHardwareReachability	m_Device;
};

static netSceNetWorker s_NetSceNetWorker;

bool
netHardware::IsLinkConnected()
{
    return s_NetSceNetWorker.IsConnected();
}

#if !__NO_OUTPUT
void netHardware::DumpNetIfConfig()
{
	sceNetShowIfconfig();
}

void netHardware::GetNetStatisticsInfo(rlSceNetStatisticsInfo& info)
{
	SceNetStatisticsInfo sceInfo;
	sceNetGetStatisticsInfo(&sceInfo, 0);

	// populate game structure
	info.m_KernelMemFreeSize = sceInfo.kernel_mem_free_size;
	info.m_KernelMemFreeMin = sceInfo.kernel_mem_free_min;
	info.m_PacketCount = sceInfo.packet_count;
	info.m_PacketQosCount = sceInfo.packet_qos_count;
	info.m_LibNetFreeSize = sceInfo.libnet_mem_free_size;
	info.m_LibNetFreeMin = sceInfo.libnet_mem_free_min;
}
#endif

//private:

bool
netHardware::NativeInit()
{
	s_NetSceNetWorker.Init();

	return true;
}

void
netHardware::NativeShutdown()
{
	s_NetSceNetWorker.Shutdown();

    NativeStopNetwork();

#if RSG_ORBIS && !RSG_P
	sceNetTerm();
#endif
}

bool
netHardware::NativeStartNetwork()
{
    SYS_CS_SYNC(sm_Cs);

    bool success = false;

    if(netVerify(STATE_STOPPED == sm_State) && IsLinkConnected())
    {
        sm_State = STATE_STARTING;

        InitWinSock();

		// this registers callbacks and initialises our states
        if(s_NetSceNetWorker.StartNetwork())
        {
	        success = true;
        }

        if(!success)
        {
            NativeStopNetwork();
        }
    }

	return success;
}

bool
netHardware::NativeStopNetwork()
{
    SYS_CS_SYNC(sm_Cs);

    if(STATE_STOPPED != sm_State)
    {
        if(s_NetSceNetWorker.StopNetwork())
        {
            ShutdownWinSock();
        }

        sm_NetInfo.Clear();

        sm_State = STATE_STOPPED;
    }

	return true;
}

void
netHardware::NativeUpdate()
{
    SYS_CS_SYNC(sm_Cs);

    if(STATE_STOPPED != sm_State && !IsLinkConnected())
    {
        netWarning("Cable disconnected! Stopping...");
        NativeStopNetwork();
    }
	else if (STATE_STOPPED != sm_State && HasIpReleaseSocketError())
	{
		netWarning("Socket error identified as due to IP Release, stopping...");
		ConsumeIpReleaseSocketError();
		NativeStopNetwork();
	}
    else if(STATE_STOPPED == sm_State && IsLinkConnected())
    {
        NativeStartNetwork();
    }
    else if(STATE_STARTING == sm_State && SCE_NET_CTL_STATE_IPOBTAINED == s_NetSceNetWorker.GetSceNetCtlState())
    {
        sm_State = STATE_AVAILABLE;
    }
}

netNatType
netHardware::NativeGetNatType()
{
	return netNatDetector::GetLocalNatType();
}

bool
netHardware::NativeGetMacAddress( u8 (&mac)[6])
{
    bool success = false;
    SceNetCtlInfo netCtlInfo;
    int err = sceNetCtlGetInfo(SCE_NET_CTL_INFO_ETHER_ADDR, &netCtlInfo);

    if(err < 0)
    {
		if (SCE_NET_CTL_ERROR_NOT_AVAIL != err && SCE_NET_CTL_ERROR_NOT_CONNECTED != err)
        {
            netError("Error calling sceNetCtlGetInfo(): %08x", err);
        }
    }
    else
    {
        CompileTimeAssert(sizeof(mac) == SCE_NET_ETHER_ADDR_LEN);
        sysMemCpy(mac, netCtlInfo.ether_addr.data, SCE_NET_ETHER_ADDR_LEN);

        success = true;
    }

    return success;
}

bool
netHardware::NativeGetLocalIpAddress(netIpAddress* ip)
{
    bool success = false;
    SceNetCtlInfo ctl_info;
    struct in_addr temp_addr;

    int err = sceNetCtlGetInfo(SCE_NET_CTL_INFO_IP_ADDRESS, &ctl_info);

    if(err < 0)
    {
        if(SCE_NET_CTL_ERROR_NOT_AVAIL != err)
        {
            netError("Error calling sceNetCtlGetInfo(SCE_NET_CTL_INFO_IP_ADDRESS):0x%08x", err);
        }
    }
	else if((err = sceNetInetPton(AF_INET, ctl_info.ip_address, &temp_addr)) < 0)
    {
        netError("Error calling inet_pton():0x%08x", err);
    }
    else
    {
        *ip = netIpAddress(netIpV4Address(ntohl(temp_addr.s_addr)));
        success = (ip->IsValid());
    }

    return success;
}

bool
netHardware::NativeGetPublicIpAddress(netIpAddress* ip)
{
	*ip = sm_PublicIp;
	bool success = ip->IsValid();
	return success;
}

void 
netHardware::SetPublicIpAddress(const netIpAddress& ip)
{
	netDebug("SetPublicIpAddress - " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(ip));
	sm_PublicIp = ip;
}

bool 
netHardware::NativeGetMtu(u32& mtu)
{
	return s_NetSceNetWorker.GetMtu(mtu);
}

netHardwareReachability
netHardware::GetReachability()
{
	netHardwareReachability device = REACHABILITY_NONE;
	s_NetSceNetWorker.GetDevice(device);
	return device;
}

}   //namespace rage

#endif  //RSG_SCE
