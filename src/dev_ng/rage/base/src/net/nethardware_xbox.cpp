// 
// net/nethardware_xbox.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "file/file_config.h"

#if __XENON || __GFWL

#include "nethardware.h"
#include "netdiag.h"
#include "netsocket.h"
#include "file/winsock.h"
#include "system/memops.h"

#include "system/xtl.h"
#if __XENON
#include <winsockx.h>
#else
#include <winsock2.h>
#include <winxnet.h>
#endif

#if __XENON
# if !__OPTIMIZED
    #pragma comment(lib, "xonlined.lib")
# else
    #pragma comment(lib, "xonline.lib")
# endif //__OPTIMIZED
#endif

namespace rage
{

RAGE_DECLARE_SUBCHANNEL(ragenet, hardware)
#undef __rage_channel
#define __rage_channel ragenet_hardware

#if __GFWL

static u16 s_SystemLinkPort = 0;

void 
netHardware::SetSystemLinkPort(const u16 port)
{
    s_SystemLinkPort = port;
}

#endif

bool
netHardware::IsLinkConnected()
{
    u8 mac[6];
    netIpAddress ip;
    return XNetGetEthernetLinkStatus() != 0
            && netHardware::GetMacAddress(mac)
            && netHardware::GetLocalIpAddress(&ip);
}

//private:

bool
netHardware::NativeInit()
{
    return true;
}

void
netHardware::NativeShutdown()
{
    NativeStopNetwork();
}

bool
netHardware::NativeStartNetwork()
{
    SYS_CS_SYNC(sm_Cs);

    bool success = false;

    if(netVerify(STATE_STOPPED == sm_State)
        && IsLinkConnected())
    {
        sm_State = STATE_STARTING;

#if __GFWL
        netAssert(s_SystemLinkPort != 0);
        XNetSetSystemLinkPort(s_SystemLinkPort);
#endif

        InitWinSock();

        success = true;
    }
    
    return success;
}

bool
netHardware::NativeStopNetwork()
{
    SYS_CS_SYNC(sm_Cs);

    if(STATE_STOPPED != sm_State)
    {
        ShutdownWinSock();
        sm_State = STATE_STOPPED;
    }

    sm_NetInfo.Clear();

    return true;
}

void
netHardware::NativeUpdate()
{
    SYS_CS_SYNC(sm_Cs);

    if(STATE_STOPPED != sm_State && !IsLinkConnected())
    {
        netWarning("Cable disconnected!");
        NativeStopNetwork();
    }
    else if(STATE_STOPPED == sm_State && IsLinkConnected())
    {
        NativeStartNetwork();
    }
    else if(STATE_STARTING == sm_State)
    {
        sm_State = STATE_AVAILABLE;
    }
}

netNatType
netHardware::NativeGetNatType()
{
    switch(XOnlineGetNatType())
    {
    case XONLINE_NAT_OPEN: return NET_NAT_OPEN;
    case XONLINE_NAT_MODERATE: return NET_NAT_MODERATE;
    case XONLINE_NAT_STRICT: return NET_NAT_STRICT;
    default: return NET_NAT_UNKNOWN;
    }
}

bool
netHardware::NativeGetMacAddress(u8 (&mac)[6])
{
    bool success = false;
    XNADDR xnaddr;
    const DWORD dw = XNetGetTitleXnAddr(&xnaddr);

    if(XNET_GET_XNADDR_PENDING == dw)
    {
        netWarning("XNetGetTitleXnAddr() returned XNET_GET_XNADDR_PENDING");
    }
    else if(XNET_GET_XNADDR_ETHERNET & dw)
    {
        success = true;
    }
    else if((XNET_GET_XNADDR_NONE | XNET_GET_XNADDR_TROUBLESHOOT) & dw)
    {
        netError("Error calling XNetGetTitleXnAddr()");
    }
    else
    {
        success = true;
    }

    if(success)
    {
        CompileTimeAssert(sizeof(mac) == sizeof(xnaddr.abEnet));
        sysMemCpy(mac, xnaddr.abEnet, sizeof(mac));
    }

    return success;
}

bool
netHardware::NativeGetLocalIpAddress(netIpAddress* ip)
{
    bool success = false;
    XNADDR xnaddr;
    const DWORD dw = XNetGetTitleXnAddr(&xnaddr);

    if(XNET_GET_XNADDR_PENDING == dw)
    {
        netWarning("XNetGetTitleXnAddr() returned XNET_GET_XNADDR_PENDING");
    }
    else if((XNET_GET_XNADDR_NONE | XNET_GET_XNADDR_TROUBLESHOOT) & dw)
    {
        netError("Error calling XNetGetTitleXnAddr()");
    }
    else
    {
        *ip = netIpAddress(netIpV4Address(ntohl(xnaddr.ina.s_addr)));
        success = ip->IsValid();
    }

    return success;
}

bool
netHardware::NativeGetPublicIpAddress(netIpAddress* ip)
{
    bool success = false;
    XNADDR xnaddr;
    const DWORD dw = XNetGetTitleXnAddr(&xnaddr);

    if(XNET_GET_XNADDR_PENDING == dw)
    {
        netWarning("XNetGetTitleXnAddr() returned XNET_GET_XNADDR_PENDING");
    }
    else if((XNET_GET_XNADDR_NONE | XNET_GET_XNADDR_TROUBLESHOOT) & dw)
    {
        netError("Error calling XNetGetTitleXnAddr()");
    }
    else
    {
        *ip = netIpAddress(netIpV4Address(ntohl(xnaddr.inaOnline.s_addr)));
        success = ip->IsValid();
    }

    return success;
}

}   //namespace rage

#endif

