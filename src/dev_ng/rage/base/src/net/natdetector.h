//
// net/natdetector.h
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//

#ifndef NET_NAT_DETECTOR_H
#define NET_NAT_DETECTOR_H

#include "data/autoid.h"
#include "system/criticalsection.h"
#include "net.h"
#include "netaddress.h"
#include "netdiag.h"
#include "nethardware.h"
#include "netsocket.h"
#include "relay.h"
#include "status.h"

namespace rage
{

/*
Sample of the information we gather about the NAT we're behind:

NAT Detection Report
    NAT Detected: true
    NAT Type: Strict
    Port Preserved: false
    Port Mapping Method: Endpoint Dependent
    Filtering Mode: Port and Address Restricted
    UPnP: Enabled, Port Forwarded
    UDP Port Binding Timeout Detection: Succeeded
    UDP Port Binding Timeout: 90 seconds
    Port Allocation Strategy: Contiguous with port increment of 1.
*/

enum netNatEventType
{   
	NET_NAT_EVENT_NAT_INFO_CHANGED,
};

#define NET_NAT_EVENT_COMMON_DECL( name )\
	static unsigned EVENT_ID() { return name::GetAutoId(); }\
	virtual unsigned GetId() const { return name::GetAutoId(); }

#define NET_NAT_EVENT_DECL( name, id )\
	AUTOID_DECL_ID( name, rage::netNatEvent, id )\
	NET_NAT_EVENT_COMMON_DECL( name )

class netNatEvent
{
public:
	AUTOID_DECL_ROOT(netNatEvent);

	NET_NAT_EVENT_COMMON_DECL(netNatEvent);

	netNatEvent() {}
	virtual ~netNatEvent() {}
};

class netNatEventNatInfoChanged : public netNatEvent
{
public:
	NET_NAT_EVENT_DECL(netNatEventNatInfoChanged, NET_NAT_EVENT_NAT_INFO_CHANGED);

	netNatEventNatInfoChanged()
	{

	}
};

enum netNatTypeDiscoveryState
{
	NAT_TYPE_STATE_WAIT_FOR_ROS_ONLINE,
	NAT_TYPE_STATE_GET_SERVER_LIST,
	NAT_TYPE_STATE_GETTING_SERVER_LIST,
	NAT_TYPE_STATE_HAVE_SERVER_LIST,
	NAT_TYPE_STATE_WAIT_FOR_PORT_MAPPER,
	NAT_TYPE_STATE_WAITING_FOR_PORT_MAPPER,
	NAT_TYPE_STATE_DISCOVERING_NAT_TYPE,
	NAT_TYPE_STATE_MEASURE_BINDING_TIMEOUT,
	NAT_TYPE_STATE_MEASURING_BINDING_TIMEOUT,
	NAT_TYPE_STATE_SUCCEEDED,
	NAT_TYPE_STATE_FAILED,
};

enum netNatPortMappingMethod
{
	NET_NAT_PMM_UNKNOWN,
	NET_NAT_PMM_ENDPOINT_INDEPENDENT,
	NET_NAT_PMM_ENDPOINT_DEPENDENT,
	NET_NAT_PMM_NUM_MAPPING_METHODS
};

enum netNatFilteringMode
{
	NET_NAT_FM_UNKNOWN,
	NET_NAT_FM_OPEN, // endpoint independent or address restricted filtering
	NET_NAT_FM_PORT_AND_ADDRESS_RESTRICTED,
	NET_NAT_FM_NUM_FILTERING_MODES
};

enum netNatPortAllocationStrategy
{
	NET_NAT_PAS_UNKNOWN,
	NET_NAT_PAS_PORT_PRESERVING,
	NET_NAT_PAS_PORT_CONTIGUOUS,
	NET_NAT_PAS_PORT_RANDOM,
	NET_NAT_PAS_NUM_ALLOCATION_STRATEGIES,
};

enum netNatUdpTimeoutState
{
	NAT_UDP_TIMEOUT_UNATTEMPTED,
	NAT_UDP_TIMEOUT_IN_PROGRESS,
	NAT_UDP_TIMEOUT_SUCCEEDED,
	NAT_UDP_TIMEOUT_FAILED,
	NAT_UDP_TIMEOUT_NUM_STATES,
};

//PURPOSE
// Error codes reported to telemetry.
enum netNatTelemetryErrorCode
{
	NET_NAT_TELEMETRY_ERR_NONE = 0,
	NET_NAT_TELEMETRY_ERR_INCOMPLETE = 1,
	NET_NAT_TELEMETRY_ERR_NO_PRIVATE_IP = 2,
	NET_NAT_TELEMETRY_ERR_NO_PRIVATE_PORT = 3,
	NET_NAT_TELEMETRY_ERR_NO_RELAY_ADDR = 4,
	NET_NAT_TELEMETRY_ERR_NO_MAPPED_ADDR = 5,
};

//PURPOSE
// Encapsulates information collected about the NAT we're running behind.
class netNatInfo
{
public:
	
	static const unsigned MAX_EXPORTED_SIZE_IN_BITS =  (netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES << 3) +			// m_RelayMappedAddr
													   1 +																// m_NatDetected
													   datBitsNeeded<NET_NAT_NUM_TYPES>::COUNT +						// m_NatType
													   datBitsNeeded<NET_NAT_PMM_NUM_MAPPING_METHODS>::COUNT +			// m_PortMappingMethod
													   datBitsNeeded<NET_NAT_FM_NUM_FILTERING_MODES>::COUNT +			// m_FilteringMode
													   datBitsNeeded<NET_NAT_PAS_NUM_ALLOCATION_STRATEGIES>::COUNT +	// m_PortAllocationStrategy
													   (sizeof(u8) << 3) +												// m_PortIncrement
													   datBitsNeeded<NAT_UDP_TIMEOUT_NUM_STATES>::COUNT +				// m_UdpTimeoutState
													   (sizeof(u16) << 3) +												// m_UdpTimeoutSec
													   (sizeof(u32) << 3) +												// m_NumRelayAddrChanges
													   (sizeof(u32) << 3);												// m_NumRelayMappedAddrChanges
															
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((MAX_EXPORTED_SIZE_IN_BITS + 7) & ~7) >> 3;						// round up to nearest multiple of 8 then divide by 8

	netNatInfo()
	{
		Clear();
	}

	void Clear();

	bool Init(bool natDetected,
			  netNatType natType,
			  netNatPortMappingMethod portMappingMethod,
			  netNatFilteringMode filteringMode,
			  netNatPortAllocationStrategy portAllocationStrategy,
			  u8 portIncrement,
			  netNatUdpTimeoutState udpTimeoutState,
			  u16 udpTimeoutSec);

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

	bool IsNatDetected() const {return m_NatDetected;}
	netNatType GetNatType() const {return m_NatType;}
	netNatPortMappingMethod GetPortMappingMethod() const {return m_PortMappingMethod;}
	netNatFilteringMode GetFilteringMode() const {return m_FilteringMode;}
	netNatPortAllocationStrategy GetPortAllocationStrategy() const {return m_PortAllocationStrategy;}
	u8 GetPortIncrement() const {return m_PortIncrement;}
	netNatUdpTimeoutState GetUdpTimeoutState() const {return m_UdpTimeoutState;}
	void SetUdpTimeoutState(netNatUdpTimeoutState udpTimeoutState) {if(m_UdpTimeoutState != udpTimeoutState){m_UdpTimeoutState = udpTimeoutState; SetDirty();}}
	void SetUdpTimeoutSec(const u16 udpTimeoutSec) {if(m_UdpTimeoutSec != udpTimeoutSec){m_UdpTimeoutSec = udpTimeoutSec; SetDirty();}}
	u16 GetUdpTimeoutSec() const {return m_UdpTimeoutSec;}
	void SetNonDeterministic() {m_NonDeterministic = true; SetDirty();}
	bool IsNonDeterministic() const {return m_NonDeterministic;}
	void UpdateRelayMappedAddr();

#if !__NO_OUTPUT
	void DumpInfo() const;
#endif

	void SetDirty();

private:
	netSocketAddress m_RelayMappedAddr;
	bool m_NatDetected;
	netNatType m_NatType;
	netNatPortMappingMethod m_PortMappingMethod;
	netNatFilteringMode m_FilteringMode;
	netNatPortAllocationStrategy m_PortAllocationStrategy;
	u8 m_PortIncrement;
	netNatUdpTimeoutState m_UdpTimeoutState;
	u16 m_UdpTimeoutSec;
	u32 m_NumRelayAddrChanges;
	u32 m_NumRelayMappedAddrChanges;
	bool m_NonDeterministic;
};

//PURPOSE
// Discovers which public port our NAT maps to our private port
// when we send a packet to a NAT type discovery server.
class netNatPortMapper
{
public:
	static const unsigned MIN_NAT_TYPE_DISCOVERY_SERVERS = 2;
	static const unsigned MAX_NAT_TYPE_DISCOVERY_SERVERS = 3;

	static const unsigned MAX_ADDRESSES = MAX_NAT_TYPE_DISCOVERY_SERVERS;

	netNatPortMapper();
	~netNatPortMapper();

	//PURPOSE
	//  If skt is nullptr, a new socket is created.
	bool Init(netSocket* skt,
			  const unsigned minAddrs,
	          const unsigned maxAddrs);

	//PURPOSE
	//  Creates a socket.
	//  establishUdpStream - if true, it will send 2 pings to the relay
	//  since some routers require two packets sent and two packets
	//  received before it detects a UDP stream and keeps the port
	//  open for the full UDP session timeout.
	bool Init(const unsigned minAddrs,
			  const unsigned maxAddrs,
			  const bool establishUdpStream);

	//PURPOSE
	//  Clears this port mapper instance, destroys socket if it's created.
	void Clear();

	//PURPOSE
	//  Call this on a regular interval.
	void Update();

	//PURPOSE
	//  Returns true when the port mapper is ready to send.
	bool IsReadyToSend() const;

	//PURPOSE
	//  Returns the number of servers available. This will be in [0, numAddrs], where numAddrs is the value passed to Init().
	unsigned GetNumServersAvailable() const;

	//PURPOSE
	//  Returns the server address at the specified index. Index must be in [0, numAddrs].
	netSocketAddress GetServerAddress(const unsigned index) const;

	//PURPOSE
	//  Starts an async operation to discover the port mapping.
	//  Note: the packet is sent before the function returns to ensure we send a packet through the NAT to create a port mapping immediately.
	//PARAMS
	//  skt			  - The socket over which to send to packet. The socket must be ready to send on.
	//  serverAddr    - Address of the NAT type discovery server to ping.
	//  mappedAddr    - On successful completion, contains the address that the server returned.
	//  status		  - Optional. Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the request.
	bool MapPort(const unsigned index,
	             netSocketAddress& mappedAddr,
	             netStatus* status);

	//PURPOSE
	// Same as MapPort() but relay pongs are sent from the server's alternate port to test for Port and Address retricted filtering.
	bool MapAltPort(const unsigned index,
					netSocketAddress& mappedAddr,
					netStatus* status);

	//PURPOSE
	//  Returns the socket.
	netSocket& GetSocket();

	//PURPOSE
	//  Returns the address of the socket. Only valid after IsReadyToSend() returns true.
	const netSocketAddress& GetSocketAddress() const;

	//PURPOSE
	//  Cancels a pending request.
	void Cancel();

private:
	void UpdateOne(const unsigned i);
	void UpdateOneAltPort(const unsigned i);

	bool GetNatTypeDiscoveryServers();
	bool SendPing(const unsigned index);
	bool SendPingAltPort(const unsigned index);

	bool ProcessSocket();
	bool OnReceive(const netSocketAddress& from,
	               void* pktBuf,
	               const int len);

	netSocket m_Skt;
	netSocket* m_SktPtr;
	netSocketAddress m_ServerAddrs[MAX_ADDRESSES];

	unsigned m_NextSendTime[MAX_ADDRESSES];
	unsigned m_NumRetries[MAX_ADDRESSES];
	netSocketAddress* m_MappedAddrs[MAX_ADDRESSES];
	netStatus* m_Status[MAX_ADDRESSES];

	unsigned m_NextSendTimeAltPort[MAX_ADDRESSES];
	unsigned m_NumRetriesAltPort[MAX_ADDRESSES];
	netSocketAddress* m_MappedAddrsAltPort[MAX_ADDRESSES];
	netStatus* m_StatusAltPort[MAX_ADDRESSES];

	unsigned m_NumAddrs;
	unsigned m_MinAddrs;
	unsigned m_MaxAddrs;
	unsigned m_MaxRetries;
	unsigned m_NumPongsReceived;
	bool m_EstablishUdpStream;

	bool m_Initialized;
};

//PURPOSE
// Discovers which public port our NAT maps to our private port when we
// send a packet to a NAT type discovery server over the shared socket.
class netNatSharedSocketPortMapper
{
public:

	static const unsigned MAX_ADDRESSES = 4;

	netNatSharedSocketPortMapper();
	~netNatSharedSocketPortMapper();

	//PURPOSE
	//  Initializes the shared socket port mapper.
	bool Init(const unsigned minAddrs,
			  const unsigned maxAddrs);

	//PURPOSE
	//  Initializes the shared socket port mapper with a specific server address.
	bool Init(const netSocketAddress& addr);

	void Clear();

	//PURPOSE
	//  Call this on a regular interval.
	void Update();

	//PURPOSE
	//  Returns true when the port mapper is ready to send.
	bool IsReadyToSend() const;

	//PURPOSE
	//  Returns the number of servers available. This will be in [0, numAddrs], where numAddrs is the value passed to Init().
	unsigned GetNumServersAvailable() const;

	//PURPOSE
	//  Returns the server address at the specified index. Index must be in [0, numAddrs].
	netSocketAddress GetServerAddress(const unsigned index) const;

	//PURPOSE
	//  Starts an async operation to discover the port mapping.
	//  Note: the packet is sent before the function returns to ensure we send a packet through the NAT to create a port mapping immediately.
	//PARAMS
	//  mappedAddr    - On successful completion, contains the address that the server returned.
	//  status		  - Optional. Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the request.
	bool MapPort(const unsigned index,
				 netSocketAddress& mappedAddr,
				 netStatus* status);

	//PURPOSE
	//  Returns the address of the socket. Only valid after IsReadyToSend() returns true.
	const netSocketAddress& GetSocketAddress() const;

	//PURPOSE
	//  Cancels a pending request.
	void Cancel();

private:
	bool GetNatTypeDiscoveryServers();
	void UpdateOne(const unsigned i);
	bool SendPing(const unsigned index);
	void OnRelayEventReceivePong(const netRelayEvent& event);

	netRelay::Delegate m_RelayDelegateReceivePong;
	netSocketAddress m_ServerAddrs[MAX_ADDRESSES];

	sysCriticalSectionToken m_Cs;

	unsigned m_NextSendTime[MAX_ADDRESSES];
	unsigned m_NumRetries[MAX_ADDRESSES];
	netSocketAddress* m_MappedAddrs[MAX_ADDRESSES];
	netStatus* m_Status[MAX_ADDRESSES];

	unsigned m_NumAddrs;
	unsigned m_MinAddrs;
	unsigned m_MaxAddrs;
	unsigned m_MaxRetries;

	bool m_Initialized;
};

//PURPOSE
// Discovers which public port our NAT maps to our private port
// when we send a packet to a NAT type discovery server.
// This version can send via unique socket or shared socket.
class netNatCombinedPortMapper
{
public:
	netNatCombinedPortMapper();
	~netNatCombinedPortMapper();

	//PURPOSE
	//  Initializes the combined port mapper.
	//  If sharedSocket = true, then the behaviour is identical
	//  to a netNatSharedSocketPortMapper. If sharedSocket = false,
	//  the behaviour is identical to a netNatPortMapper.
	bool Init(const unsigned minAddrs,
	          const unsigned maxAddrs, 
			  const bool sharedSocket);

	//PURPOSE
	//  Clears this port mapper instance, destroys socket if it's created.
	void Clear();

	//PURPOSE
	//  Call this on a regular interval.
	void Update();

	//PURPOSE
	//  Returns true when the port mapper is ready to send.
	bool IsReadyToSend() const;

	//PURPOSE
	//  Returns the number of servers available. This will be in [0, numAddrs], where numAddrs is the value passed to Init().
	unsigned GetNumServersAvailable() const;

	//PURPOSE
	//  Returns the server address at the specified index. Index must be in [0, numAddrs].
	netSocketAddress GetServerAddress(const unsigned index) const;

	//PURPOSE
	//  Starts an async operation to discover the port mapping.
	//  Note: the packet is sent before the function returns to ensure we send a packet through the NAT to create a port mapping immediately.
	//PARAMS
	//  serverAddr    - Address of the NAT type discovery server to ping.
	//  mappedAddr    - On successful completion, contains the address that the server returned.
	//  status		  - Optional. Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the request.
	bool MapPort(const unsigned index,
	             netSocketAddress& mappedAddr,
	             netStatus* status);

	//PURPOSE
	// Same as MapPort() but relay pongs are sent from the server's alternate port to test for Port and Address retricted filtering.
	bool MapAltPort(const unsigned index,
					netSocketAddress& mappedAddr,
					netStatus* status);

	//PURPOSE
	//  Returns the socket.
	netSocket& GetSocket();

	//PURPOSE
	//  Returns the address of the socket. Only valid after IsReadyToSend() returns true.
	const netSocketAddress& GetSocketAddress() const;

	//PURPOSE
	//  Cancels a pending request.
	void Cancel();

private:
	netNatPortMapper m_PortMapper;
	netNatSharedSocketPortMapper m_SharedSocketPortMapper;

	bool m_SharedSocket;
	bool m_Initialized;
};

//PURPOSE
// Determines whether the NAT keeps a port open for a specified duration 
// when no data is sent out of the port or received from the port.
// This is used by the netNatPortTimeoutMeasurer (below).
class netNatPortTimeoutChecker
{
public:

	netNatPortTimeoutChecker();
	~netNatPortTimeoutChecker();

	bool SendPing();

	//PURPOSE
	bool Init(const unsigned timeoutToCheck);

	//PURPOSE
	//  Clears this instance.
	void Clear();

	//PURPOSE
	//  Call this on a regular interval.
	void Update();

	//PURPOSE
	//  Returns true if still pending.
	bool Pending();

	//PURPOSE
	//  Returns true if not pending and it has succeeded.
	bool Succeeded();

	//PURPOSE
	//  Cancels a pending request.
	void Cancel();

	unsigned GetTimeoutToCheckSeconds() const {return m_TimeoutToCheckMs / 1000;}
	bool ReceivedResponse() const {return m_RecievedResponse;}

private:
	bool SendTimeoutCheckMessage();
	bool ProcessSocket(netSocket& rcvSkt);
	bool OnReceive(const netSocketAddress& from,
	               void* pktBuf,
	               const int len);

	enum State
	{
		STATE_WAITING_FOR_SOCKETS,
		STATE_WAITING_FOR_MAPPED_PORT,
		STATE_WAITING_TO_SEND,
		STATE_WAITING_FOR_RESPONSE,
		STATE_COMPLETED,
		STATE_ERROR
	};
	netNatPortMapper m_PortMapper;
	netSocket m_SendSkt;
	State m_State;
	netSocketAddress m_MappedAddr;
	netStatus m_MappedAddrStatus;
	netAddress m_Addr;
	unsigned m_TimeoutToCheckMs;
	unsigned m_PingSendTime;
	unsigned m_CheckMessageSendTime;
	bool m_RecievedResponse;

	bool m_Initialized;
};

//PURPOSE
// Uses a series of netNatPortTimeoutCheckers with different check times
// to determine how long the NAT keeps a port binding open when no data
// is sent out of the port or received from the port.
class netNatPortTimeoutMeasurer
{
public:

	netNatPortTimeoutMeasurer();
	~netNatPortTimeoutMeasurer();

	//PURPOSE
	bool Init();

	//PURPOSE
	//  Clears this instance, destroys the receive socket passed to init.
	void Clear();

	//PURPOSE
	//  Call this on a regular interval.
	void Update();

	//PURPOSE
	//  Returns true if still pending.
	bool Pending();

	//PURPOSE
	//  Returns true if not pending and it has succeeded.
	bool Succeeded();

	//PURPOSE
	//  Returns the UDP timeout discovered so far.
	//  Can be called while timeout discovery is in progress. May continue to increase
	//  until the operation succeeds or fails.
	unsigned GetUdpTimeoutSec();

	//PURPOSE
	//  Cancels a pending request.
	void Cancel();

private:

	enum State
	{
		STATE_CHECKING,
		STATE_COMPLETED,
		STATE_ERROR
	};

	static const unsigned sm_NumIntervals = 4;
	static const unsigned sm_Intervals[sm_NumIntervals];
	netNatPortTimeoutChecker m_PortTimeoutChecker[sm_NumIntervals];
	State m_State;

	bool m_Initialized;
};

//PURPOSE
//  Detects NAT type + other information about the NAT and sets up
//  port forwarding if UPnP is available.
class netNatDetector
{
	typedef atDelegator<void(const netNatEvent& event)> Delegator;

public:
	typedef Delegator::Delegate Delegate;

	static bool Init();
	static void Shutdown();
	static void Update();

	//PURPOSE
	//  Add/remove a delegate that will be called with event notifications.
	static void AddDelegate(netNatDetector::Delegate* dlgt);
	static void RemoveDelegate(netNatDetector::Delegate* dlgt);
	static void DispatchEvent(netNatEvent* e);

	static netNatType GetLocalNatType();
	static const netNatInfo& GetLocalNatInfo();
	
	static bool HasNatTypeDiscoveryFailed();
	static bool HasNatTypeDiscoveryCompleted();
	static bool IsReadyForMultiplayer();
	static void SetNonDeterministic();

	static netNatTypeDiscoveryState GetNatDiscoveryState();

#if __RGSC_DLL
	// the Social Club DLL gets the info from the game, and then sets its own local copy here
	static void SetLocalNatInfo(const netNatInfo& info);
#endif
	static void SetAllowUdpTimeoutMeasurement(const bool allowUdpTimeoutMeasurement); 
	static void SetAllowAdjustableUdpSendInterval(const bool allowAdjustableUdpSendInterval); 
	static void SetAllowNatInfoTelemetry(const bool allowNatInfoTelemetry); 
	static bool GetAllowAdjustableUdpSendInterval() {return sm_AllowAdjustableUdpSendInterval;}

	static void UpdateRgsc();

private:
	static void SetNatDiscoveryState(netNatTypeDiscoveryState state);
	static void UpdateNatTypeDiscovery();
	static void UpdateReport();

	static bool m_Initialized;
	static netNatType sm_LocalNatType;
	static netNatInfo sm_LocalNatInfo;
	static bool sm_AllowUdpTimeoutMeasurement;
	static bool sm_AllowAdjustableUdpSendInterval;
	static bool sm_AllowNatInfoTelemetry;
	static netNatDetector::Delegator sm_Delegator;
	static sysCriticalSectionToken m_Cs;
};

}   //namespace rage

#endif  //NET_NAT_DETECTOR_H
