// 
// net/netsequence.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_NETSEQUENCE_H
#define NET_NETSEQUENCE_H

namespace rage
{

//This file contains convenience functions for comparing "sequence" numbers.
//Sequence numbers are simply unsigned numbers.
//Note that the sense of comparison between two sequence numbers will
//change if the difference between them is greater or equal to 2^x
//where x = b-1, b is the number of bits in a sequence number.
//For example, given 16 bit numbers a and b, a = 3 and b = 32770, then
//netSeqGt(b, a) is true.  Adding one to b, making it 32771, makes
//netSeqGt(b, a) false.

typedef u16 netSequence;

namespace netsequence_detail
{

template<typename T> class SignedType
{
public:

    typedef T Type;
};

template<>
class SignedType<u64>
{
public:

    typedef s64 Type;
};

template<>
class SignedType<u32>
{
public:

    typedef s32 Type;
};

template<>
class SignedType<u16>
{
public:

    typedef s16 Type;
};

template<>
class SignedType<u8>
{
public:

    typedef s8 Type;
};

}   //namespace netsequence_detail

//PURPOSE
//  Convenience functions for comparing sequence numbers
//NOTES:
//  The sense of comparison between two sequence numbers will
//  change if the difference between them is greater or equal to 2^x
//  where x = b-1, b is the number of bits in a sequence number.
//  See comments at the top of this file.
template<typename T>
inline
typename netsequence_detail::SignedType<T>::Type
netSeqDiff(const T a, const T b)
{
    return typename netsequence_detail::SignedType<T>::Type(a - b);
}

template<typename T>
inline
bool
netSeqLt(const T a, const T b)
{
    return netSeqDiff(a, b) <0;
}

template<typename T>
inline
bool
netSeqLe(const T a, const T b)
{
    return netSeqDiff(a, b) <= 0;
}

template<typename T>
inline
bool
netSeqGt(const T a, const T b)
{
    return netSeqDiff(a, b)> 0;
}

template<typename T>
inline
bool
netSeqGe(const T a, const T b)
{
    return netSeqDiff(a, b)>= 0;
}

//PURPOSE
//  Sequence number manager.
class netSequencer
{
public:

    netSequencer()
    {
        this->Reset();
    }

    void Reset()
    {
        m_SeqSent = 0;
        //m_SeqExpected is initialized to 1 because the first
        //message any connection sends is m_SeqSent + 1.
        m_SeqExpected = 1;
        m_AckSent = 0;
        m_AckReceived = 0;
    }

    //PURPOSE
    //  Returns the next sequence number of the next outgoing message.
    netSequence GetNextSeq() const
    {
        return m_SeqSent + 1;
    }

    //PURPOSE
    //  Sets the sequence number for the next expected message.
    void SetSeqExpected(const netSequence expected)
    {
        m_SeqExpected = expected;
    }

    //PURPOSE
    //  Returns the sequence number of the next expected message.
    netSequence GetSeqExpected() const
    {
        return m_SeqExpected;
    }

    //PURPOSE
    //  Sets the sequence number of the last acked message.
    void SetAckSent(const netSequence ackSent)
    {
        m_AckSent = ackSent;
    }

    //PURPOSE
    //  Returns the sequence number of the last acked message.
    netSequence GetAckSent() const
    {
        return m_AckSent;
    }

    //PURPOSE
    //  Sets the sequence of number of the last message
    //  for which we've received an ack.
    void SetAckReceived(const netSequence ackReceived)
    {
        m_AckReceived = ackReceived;
    }

    //PURPOSE
    //  Returns the sequence of number of the last message
    //  for which we've received an ack.
    netSequence GetAckReceived() const
    {
        return m_AckReceived;
    }

    //PURPOSE
    //  Returns true if we need to send an ACK.
    bool NeedToAck() const
    {
        return netSeqDiff(this->GetSeqExpected(), this->GetAckSent())> 1;
    }

    //PURPOSE
    //  Returns the sequence number that we need to ack
    netSequence GetNextAck() const
    {
        return this->GetSeqExpected() - 1;
    }

    //PURPOSE
    //  Generates the sequence number for the next message to be sent.
    netSequence GenerateNextSeq()
    {
        return ++m_SeqSent;
    }

private:

    //Sequence number of last message sent.
    netSequence m_SeqSent;

    //Sequence number of next expected message.
    netSequence m_SeqExpected;

    //Sequence number of last ACKed message.
    netSequence m_AckSent;

    //Highest sequence number that has been ACKed.
    netSequence m_AckReceived;
};

}   //namespace rage

#endif  //NET_NETSEQUENCE_H
