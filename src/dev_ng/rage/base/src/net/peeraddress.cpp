// 
// net/peeraddress.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 
#include "peeraddress.h"
#include "data/base64.h"
#include "data/bitbuffer.h"
#include "diag/seh.h"
#include "netdiag.h"
#include "net/nethardware.h"
#include "net/relay.h"
#include "rline/rlpresence.h"
#include "string/stringutil.h"
#include <string.h>

#if RSG_LINUX
#if (YETI_VERSION >= YETI_MAKE_VERSION(1, 33, 0))
#include <ggp/gamelet_config.h>
#include <ggp/multiplayer.h>
#endif
#endif

namespace rage
{
#if RSG_PC && !__NO_OUTPUT
	XPARAM(processinstance);
#endif

CompileTimeAssert(netPeerAddress::TO_STRING_BUFFER_SIZE <= RLSC_PRESENCE_STRING_MAX_SIZE);

const netPeerAddress netPeerAddress::INVALID_PEER_ADDRESS;
extern sysMemAllocator* g_rlAllocator;
extern netSocket* g_rlSkt;

//////////////////////////////////////////////////////////////////////////
//  netPeerAddress
//////////////////////////////////////////////////////////////////////////

bool
netPeerAddress::HasNetwork()
{
    bool hasNetwork = false;

    if(g_rlSkt
        && g_rlSkt->CanSendReceive()
        && g_rlSkt->GetAddress().GetIp().IsValid())
    {
        hasNetwork = true;
    }

    return hasNetwork;
}

#if NET_PEER_ADDR_CANDIDATES
void 
netPeerAddress::GatherLocalP2pCandidates()
{
	rtry
	{
		rcheckall(g_rlSkt && g_rlSkt->CanSendReceive())
		rcheckall(g_rlSkt->GetAddress().GetPort() != 0)
		rverifyall(g_rlAllocator);

#if RSG_LINUX
#if (YETI_VERSION >= YETI_MAKE_VERSION(1, 33, 0))
	// Note about GGP: gamelets in the Google datacentre communicate with each
	// another via the address returned via GetExternalConnectionAddress().
	// There is no guarantee that this address will be the same address as seen
	// from the relay server, so we will need to consider this as a separate
	// candidate address for P2P communications during NAT traversal.
	//
	// GGP implements a simple form of Network Address Translation (NAT) where
	// an internal IP address is mapped to an external IP address,
	// but ports are mapped 1:1, so the public port will be the same as 
	// the private port.

	// GGP APIs alloc/free memory, often when the wrong allocator is set. Use the rline allocator to avoid a crash.
	sysMemAutoUseAllocator allocator(*g_rlAllocator);

	ggp::GameletConfig gc = ggp::GetGameletConfig();
	if(gc.machine == ggp::GameletMachine::kCloud)
	{
		ggp::Status status;
		ggp::multiplayer::Address ggpAddr;
		const bool ggpSuccess = ggp::multiplayer::GetExternalConnectionAddress(&ggpAddr, &status);
		const u16 port = g_rlSkt->GetAddress().GetPort();

		if(ggpSuccess && status.Ok())
		{
			if(ggpAddr.types & kGgpMultiplayerAddressType_IpV4)
			{
				if(m_P2pCandidates.m_NumCandidates < COUNTOF(m_P2pCandidates.m_P2pCandidate))
				{
					netSocketAddress& addr = m_P2pCandidates.m_P2pCandidate[m_P2pCandidates.m_NumCandidates].m_Addr;
					if(addr.Init(netIpAddress(netIpV4Address(net_ntohl(ggpAddr.address.v4))), port) && netVerify(addr.IsValid()))
					{
						++m_P2pCandidates.m_NumCandidates;
					}
				}
			}
			
			if(ggpAddr.types & kGgpMultiplayerAddressType_IpV6)
			{
#if IPv4_IPv6_DUAL_STACK
				if(m_P2pCandidates.m_NumCandidates < COUNTOF(m_P2pCandidates.m_P2pCandidate))
				{
					netSocketAddress& addr = m_P2pCandidates.m_P2pCandidate[m_P2pCandidates.m_NumCandidates].m_Addr;
					if(addr.Init(netIpAddress(netIpV6Address(ggpAddr.address.v6_addr8)), port) && netVerify(addr.IsValid()))
					{
						++m_P2pCandidates.m_NumCandidates;
					}
				}
#else
				netDebug2("netPeerAddress::GatherLocalP2pCandidates :: GetExternalConnectionAddress() returned an IPv6 address. IPv4_IPv6_DUAL_STACK is disabled, ignoring.");
#endif
			}
		}
	}
#endif
#endif
	}
	rcatchall
	{

	}
}
#endif

bool
netPeerAddress::GetLocalPeerAddress(const int localGamerIndex, netPeerAddress* addr)
{
    bool success = false;

	// Note: anything that causes a change to the local peer address must be raised as an
	// event and handled in rlPresence.cpp so that the updated peer address can propagate
	// to all interested parties (presence, session server, connected peers, etc.).

    addr->Clear();

	netPeerId::GetLocalPeerId(addr->m_PeerId);

	// the relay address contains both our public address as seen by the relay 
	// server, and the address of the relay server to which we are connected.
	// Note: the target address is cleared when advertising to 
	// presence/matchmaking, leaving only the relay token.
	const netAddress& relayAddr = netRelay::GetMyRelayAddress();
	const netRelayToken& relayToken = netRelay::GetMyRelayToken();
	if(relayAddr.IsRelayServerAddr())
	{
		addr->m_RelayServerAddress = relayAddr.GetProxyAddress();
		addr->m_RelayToken = relayToken;
		addr->m_PublicAddress = relayAddr.GetTargetAddress();
	}
	else
	{
		// if the relay address is invalid, attempt to get the public IP. Remote
		// peers might be able to make a direct connection to us using just the 
		// public IP and the socket's bound port (for NAT's that preserve the port).
		netIpAddress publicIp;
		if(netHardware::GetPublicIpAddress(&publicIp) && publicIp.IsValid())
		{
			addr->m_PublicAddress.Init(publicIp, 0);
		}
	}

	if(g_rlSkt && g_rlSkt->CanSendReceive())
	{
		addr->m_PrivateAddress = g_rlSkt->GetAddress();
		addr->m_NatType = netHardware::GetNatType();

		success = true;
	}

#if NET_PEER_ADDR_CANDIDATES
	addr->GatherLocalP2pCandidates();
#endif

    if(!success)
    {
        addr->Clear();
    }
	else
	{
		// Note: this will only be valid if we're signed in
		addr->m_GamerHandle.Clear();

		if(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
		{
			if(!rlPresence::GetGamerHandle(localGamerIndex, &addr->m_GamerHandle))
			{
				addr->m_GamerHandle.Clear();
			}
		}

		addr->m_Key = netP2pCrypt::GetLocalPeerKey();
	}

    return success;
}

netPeerAddress::netPeerAddress()
{
    this->Clear();
}

void
netPeerAddress::Clear()
{
	ClearKey();

	m_GamerHandle.Clear();
	m_RelayServerAddress.Clear();
	m_RelayToken.Clear();
	m_PublicAddress.Clear();
	m_PrivateAddress.Clear();
#if NET_PEER_ADDR_CANDIDATES
	m_P2pCandidates.Clear();
#endif
	m_NatType = NET_NAT_UNKNOWN;

	OUTPUT_ONLY(m_Str[0] = '\0');
}

void 
netPeerAddress::ClearNonAdvertisableData()
{
	// don't advertise the player's public address to presence/matchmaking
	// only the tokenized relay address will be advertised.
	m_PublicAddress.Clear();
}

bool
netPeerAddress::IsValid() const
{
	return m_PrivateAddress.IsValid();
}

bool
netPeerAddress::IsLocal() const
{
	// first check the netPeerId which is generated at game startup, but can change between launches
	if(this->GetPeerId().IsLocal())
	{
		return true;
	}

	// next check the gamerhandle - the local peer can discover sessions they've previously uploaded
	// to presence from a previous launch of the game, which will have a different netPeerId
	bool isLocal = false;

	if(this->IsValid())
	{
		for(unsigned i = 0; (i < RL_MAX_LOCAL_GAMERS) && !isLocal; i++)
		{
			if(rlPresence::IsSignedIn(i))
			{
				rlGamerHandle gh;
				if(rlPresence::GetGamerHandle(i, &gh))
				{
					isLocal = gh.IsValid() && (this->GetGamerHandle() == gh);
				}
			}
		}
	}

	return isLocal;
}

bool
netPeerAddress::IsRemote() const
{
    return this->IsValid() && !this->IsLocal();
}

bool
netPeerAddress::ExportInternal(void* buf, const unsigned sizeofBuf, const bool ORBIS_ONLY(forNpPresence), unsigned* size) const
{
	rtry
	{
		datExportBuffer bb;
		bb.SetReadWriteBytes(buf, sizeofBuf);

		rverify(bb.SerUser(m_PeerId), catchall, );

#if RSG_ORBIS
		if(forNpPresence)
		{
			// to keep the session info struct under 128 bytes, the struct serialized
			// to NP Presnce will only have the accountId, not the onlineId
			const rlSceNpAccountId accountId = m_GamerHandle.GetNpAccountId();
			rverify(bb.SerUns(accountId, 64), catchall, );
		}
		else
		{
			rverify(bb.SerUser(m_GamerHandle), catchall, );
		}
#else
		rverify(bb.SerUser(m_GamerHandle), catchall, );
#endif

		rverify(bb.SerUser(m_Key), catchall, );
		rverify(bb.SerUser(m_RelayServerAddress), catchall, );

#if RSG_ORBIS
		if(!forNpPresence)
#endif
		{
			rverify(bb.SerUser(m_PublicAddress), catchall, );
		}
		rverify(bb.SerUser(m_PrivateAddress), catchall, );
		rverify(bb.SerUns(m_NatType, datBitsNeeded<NET_NAT_NUM_TYPES>::COUNT), catchall, );
#if NET_PEER_ADDR_CANDIDATES
		rverify(bb.SerUser(m_P2pCandidates), catchall, );
#endif
		rverify(bb.SerUser(m_RelayToken), catchall, );

		netAssert(bb.GetNumBytesWritten() <= ORBIS_ONLY(forNpPresence ? NP_PRESENCE_DATA_MAX_EXPORTED_SIZE_IN_BYTES :) MAX_EXPORTED_SIZE_IN_BYTES);

		if(size){*size = bb.GetNumBytesWritten();}

		return true;
	}
	rcatchall
	{
		if(size){*size = 0;}
		return false;
	}
}

bool
netPeerAddress::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
	return ExportInternal(buf, sizeofBuf, false, size);
}

bool
netPeerAddress::ImportInternal(const void* buf, const unsigned sizeofBuf, const bool ORBIS_ONLY(fromNpPresence), unsigned* size)
{
	rtry
	{
		datImportBuffer bb;
		bb.SetReadOnlyBytes(buf, sizeofBuf);

		rcheck(bb.SerUser(m_PeerId), catchall, );

#if RSG_ORBIS
		if(fromNpPresence)
		{
			rlSceNpAccountId accountId;
			rcheck(bb.SerUns(accountId, 64), catchall, );
			m_GamerHandle.ResetNp(g_rlNp.GetEnvironment(), accountId, nullptr);
		}
		else
		{
			rcheck(bb.SerUser(m_GamerHandle), catchall, );
		}
#else
		rcheck(bb.SerUser(m_GamerHandle), catchall, );
#endif

		rcheck(bb.SerUser(m_Key), catchall, );
		rcheck(bb.SerUser(m_RelayServerAddress), catchall, );

#if RSG_ORBIS
		if(fromNpPresence)
		{
			m_PublicAddress.Clear();
		}
		else
#endif
		{
			rcheck(bb.SerUser(m_PublicAddress), catchall, );
		}
		rcheck(bb.SerUser(m_PrivateAddress), catchall, );
		rcheck(bb.SerUns(m_NatType, datBitsNeeded<NET_NAT_NUM_TYPES>::COUNT), catchall, );

#if NET_PEER_ADDR_CANDIDATES
		rcheck(bb.SerUser(m_P2pCandidates), catchall, );
#endif

		rcheck(bb.SerUser(m_RelayToken), catchall, );

		if(size){*size = bb.GetNumBytesRead();}

		return true;
	}
	rcatchall
	{
		if(size){*size = 0;}
		return false;
	}
}

bool
netPeerAddress::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
	return ImportInternal(buf, sizeofBuf, false, size);
}

#if RSG_ORBIS
bool 
netPeerAddress::ExportForNpPresence(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
	return ExportInternal(buf, sizeofBuf, true, size);
}

bool 
netPeerAddress::ImportFromNpPresence(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
	return ImportInternal(buf, sizeofBuf, true, size);
}
#endif

const char*
netPeerAddress::ToString(char* buf,
						 const unsigned bufSize,
						 unsigned* size) const
{
    if(netVerifyf(bufSize >= TO_STRING_BUFFER_SIZE, "Destination buffer is too small"))
    {
        u8 exportBuf[TO_STRING_BUFFER_SIZE] = {0};
        unsigned exportSize;
        if(netVerifyf(this->Export(exportBuf, sizeof(exportBuf), &exportSize),
                    "Error exporting netPeerAddress")
            && netVerifyf(datBase64::GetMaxEncodedSize(exportSize) <= bufSize,
                        "Destination buffer is too small")
            && netVerifyf(datBase64::Encode(exportBuf, exportSize, buf, bufSize, size),
                        "Error encoding netPeerAddress"))
        {
            return buf;
        }
    }

    return NULL;
}

bool
netPeerAddress::FromString(const char* buf, unsigned* size)
{
    u8 decodeBuf[TO_STRING_BUFFER_SIZE] = {0};
    unsigned decodeSize;
    unsigned importSize;
    const bool success =
		!StringNullOrEmpty(buf)
        && netVerifyf(datBase64::GetMaxDecodedSize(buf) <= sizeof(decodeBuf),
                    "Destination buffer is too small")
        && netVerifyf(datBase64::Decode(buf, sizeof(decodeBuf), decodeBuf, &decodeSize),
                    "Error decoding netPeerAddress")
        && netVerifyf(this->Import(decodeBuf, decodeSize, &importSize),
                    "Error importing netPeerAddress");

    if(size)
    {
        *size = success ? ustrlen(buf) : 0;
    }

    return success;
}

#if !__NO_OUTPUT
const char*
netPeerAddress::ToString() const
{
	CompileTimeAssert(sizeof(m_Str) >= (netPeerId::TO_HEX_STRING_BUFFER_SIZE + RL_MAX_GAMER_HANDLE_CHARS + (netSocketAddress::MAX_STRING_BUF_SIZE * 3) + 12 /*separators + slush*/));
	char handleString[RL_MAX_GAMER_HANDLE_CHARS] = "invalid";
	if(m_GamerHandle.IsValid())
	{
		m_GamerHandle.ToString(handleString, RL_MAX_GAMER_HANDLE_CHARS);
	}

	char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE] = "invalid";
	if(m_PeerId.IsValid())
	{
		m_PeerId.ToHexString(peerIdHexString);
	}

	formatf(m_Str, "%s:%s:" NET_ADDR_FMT ":" NET_ADDR_FMT "/" NET_ADDR_FMT ":%d", peerIdHexString, handleString, NET_ADDR_FOR_PRINTF(m_PrivateAddress), NET_ADDR_FOR_PRINTF(m_RelayServerAddress), NET_ADDR_FOR_PRINTF(m_PublicAddress), m_NatType);
	return m_Str[0] ? m_Str : ToString(m_Str, sizeof(m_Str));
}
#endif

const netAddress
netPeerAddress::GetRelayAddress() const
{
	return netAddress(m_RelayServerAddress, m_RelayToken, m_PublicAddress);
}

const netSocketAddress&
netPeerAddress::GetRelayServerAddress() const
{
	return m_RelayServerAddress;
}

const netSocketAddress&
netPeerAddress::GetPublicAddress() const
{
	return m_PublicAddress;
}

const netSocketAddress&   
netPeerAddress::GetPrivateAddress() const
{
	return m_PrivateAddress;
}

bool
netPeerAddress::GetP2pCandidateAddress(const unsigned index, netSocketAddress& addr, netCandidateAddrType& type) const
{
#if NET_PEER_ADDR_CANDIDATES
	if(index < m_P2pCandidates.m_NumCandidates)
	{
		addr = m_P2pCandidates.m_P2pCandidate[index].m_Addr;
		type = m_P2pCandidates.m_P2pCandidate[index].m_Type;
		return true;
	}
#else
	(void)index;
#endif
	
	addr.Clear();
	type = netCandidateAddrType::ADDR_TYPE_INVALID;

	return false;
}

unsigned 
netPeerAddress::GetNumP2pCandidates() const
{
#if NET_PEER_ADDR_CANDIDATES
	return m_P2pCandidates.m_NumCandidates;
#else
	return 0;
#endif
}

netNatType 
netPeerAddress::GetNatType() const
{
	return m_NatType;
}

const netPeerId&
netPeerAddress::GetPeerId() const
{
	return m_PeerId;
}

const rlGamerHandle&
netPeerAddress::GetGamerHandle() const
{
	return m_GamerHandle;
}

const netP2pCrypt::Key&
netPeerAddress::GetPeerKey() const
{
	return m_Key;
}

void
netPeerAddress::ClearKey()
{
	m_Key.Clear();
}

#if NET_PEER_ADDR_CANDIDATES
//////////////////////////////////////////////////////////////////////////
//  P2pCandidateAddrs
//////////////////////////////////////////////////////////////////////////

netPeerAddress::P2pCandidateAddrs::P2pCandidateAddrs()
: m_NumCandidates(0)
{

}

void netPeerAddress::P2pCandidateAddrs::Clear()
{
	for(unsigned i = 0; i < COUNTOF(m_P2pCandidate); ++i)
	{
		m_P2pCandidate[i].Clear();
	}

	m_NumCandidates = 0;
}

bool netPeerAddress::P2pCandidateAddrs::Export(void* buf, const unsigned sizeofBuf, unsigned* size) const
{
	rtry
	{
		datExportBuffer bb;
		bb.SetReadWriteBytes(buf, sizeofBuf);

		rverifyall(bb.SerArraySize(m_NumCandidates, m_P2pCandidate ASSERT_ONLY(, "m_P2pCandidate")))
		for(unsigned i = 0; (i < m_NumCandidates) && netVerify(i < COUNTOF(m_P2pCandidate)); ++i)
		{
			rverifyall(bb.SerUser(m_P2pCandidate[i]));
		}

		netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

		if(size) { *size = bb.GetNumBytesWritten(); }

		return true;
	}
	rcatchall
	{
		if(size) {*size = 0;}
		return false;
	}
}

bool netPeerAddress::P2pCandidateAddrs::Import(const void* buf, const unsigned sizeofBuf, unsigned* size)
{
	rtry
	{
		datImportBuffer bb;
		bb.SetReadOnlyBytes(buf, sizeofBuf);

		rverifyall(bb.SerArraySize(m_NumCandidates, m_P2pCandidate ASSERT_ONLY(, "m_P2pCandidate")))
		for(unsigned i = 0; (i < m_NumCandidates) && netVerify(i < COUNTOF(m_P2pCandidate)); ++i)
		{
			rverifyall(bb.SerUser(m_P2pCandidate[i]));
		}

		if(size){*size = bb.GetNumBytesRead();}

		return true;
	}
	rcatchall
	{
		if(size){*size = 0;}
		return false;
	}
}

//////////////////////////////////////////////////////////////////////////
//  P2pCandidateAddr
//////////////////////////////////////////////////////////////////////////

void netPeerAddress::P2pCandidateAddr::Clear()
{
	m_Type = ADDR_TYPE_INVALID;
	m_Addr.Clear();
}

bool netPeerAddress::P2pCandidateAddr::Export(void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/) const
{
	rtry
	{
		datExportBuffer bb;
		bb.SetReadWriteBytes(buf, sizeofBuf);

		rverifyall(bb.SerUser(m_Addr));
		rverify(bb.SerUns(m_Type, datBitsNeeded<ADDR_TYPE_NUM_TYPES>::COUNT), catchall, );

		netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

		if(size) { *size = bb.GetNumBytesWritten(); }

		return true;
	}
	rcatchall
	{
		if(size) {*size = 0;}
		return false;
	}
}

bool netPeerAddress::P2pCandidateAddr::Import(const void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/)
{
	rtry
	{
		datImportBuffer bb;
		bb.SetReadOnlyBytes(buf, sizeofBuf);

		rverifyall(bb.SerUser(m_Addr));
		rverify(bb.SerUns(m_Type, datBitsNeeded<ADDR_TYPE_NUM_TYPES>::COUNT), catchall, );

		if(size){*size = bb.GetNumBytesRead();}

		return true;
	}
	rcatchall
	{
		if(size){*size = 0;}
		return false;
	}
}
#endif

}   //namespace rage
