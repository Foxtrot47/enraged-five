// 
// net/status.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "status.h"
#include "system/nelem.h"

namespace rage
{


#if !__NO_OUTPUT
const char *netStatus::GetStatusCodeString( const StatusCode statusCode )
{
    static const char* s_StatusCodeString[] = 
    {
        "NET_STATUS_NONE",
        "NET_STATUS_PENDING",
        "NET_STATUS_FAILED",
        "NET_STATUS_SUCCEEDED",
        "NET_STATUS_CANCELED",
    };
    CompileTimeAssert(COUNTOF(s_StatusCodeString) == NET_STATUS_COUNT);

    return s_StatusCodeString[statusCode];
}
#endif

}   //namespace rage
