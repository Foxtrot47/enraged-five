// 
// net/tunneler_ice.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#if defined(RSG_LEAN_CLIENT)
#define ICE_TUNNELER_LEAN (1)
#else
#define ICE_TUNNELER_LEAN (!__OPTIMIZED)
#endif

// TODO: NS - move firewall code to a new file
#if ICE_TUNNELER_LEAN
#define WINDOWS_FIREWALL_API 0
#else
#define WINDOWS_FIREWALL_API (RSG_PC)
#endif

#include "tunneler_ice.h"

#include "atl/array.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/message.h"
#include "net/natdetector.h"
#include "net/natpcp.h"
#include "net/natupnp.h"
#include "net/net.h"
#include "net/netdiag.h"
#include "net/nethardware.h"
#include "net/netsocket.h"
#include "net/task.h"
#include "profile/rocky.h"
#include "rline/rl.h"
#include "rline/rlpc.h"
#include "rline/rlpresence.h"
#include "rline/rltelemetry.h"
#include "rline/rltitleid.h"
#include "rline/ros/rlros.h"
#include "rline/scpresence/rlscpresence.h"
#include "string/stringhash.h"
#include "system/nelem.h"
#include "system/timemgr.h"

#include <time.h>

#if WINDOWS_FIREWALL_API
#include "system/xtl.h"
#include <crtdbg.h>
#include <netfw.h>
#include <objbase.h>
#include <oleauto.h>
#include <stdio.h>

#pragma comment( lib, "ole32.lib" )
#pragma comment( lib, "oleaut32.lib" )
#endif

/*
Based on RFC 5245
Interactive Connectivity Establishment (ICE):
A Protocol for Network Address Translator (NAT) Traversal for
Offer/Answer Protocols
https://tools.ietf.org/html/rfc5245
*/

namespace rage
{

#if defined(RSG_LEAN_CLIENT)
#define ICE_PARAM NOSTRIP_PC_PARAM
#else
#define ICE_PARAM PARAM
#endif

#if !__FINAL || defined(RSG_LEAN_CLIENT)
PARAM(neticeforcefail, "Don't start NAT traversal, instead force a failure after a specified number of milliseconds (eg. -neticeforcefail=1000)");
ICE_PARAM(neticefailprobability, "Force a NAT traversal failure based on a [0%, 100%] probability (eg. -neticefailprobability=50)");
ICE_PARAM(neticefailbasedonnatcombo, "Force a NAT traversal failure based on NAT type combination (i.e. strict-strict, strict-moderate will fail)");
PARAM(neticefailgamers, "Force a NAT traversal failure based on a comma-separated list of gamerhandles (eg. -neticefailgamers=\"SC 280471,SC 280921\")");
PARAM(neticesucceedgamers, "Force a NAT traversal failure to gamers NOT listed in a comma-separated list of gamerhandles (eg. -neticesucceedgamers=\"SC 280471,SC 280921\")");
PARAM(neticeforcedelay, "Delays NAT traversal for a specified number of milliseconds to simulate long-running NAT traversals (eg. -neticeforcedelay=10000, or a random delay in a range, eg. -neticeforcedelay=1000-8000");
PARAM(neticenodirectoffers, "Only send indirect (relay assisted) offers");
PARAM(netIceIgnoreDirectMsgs, "Ignores ICE messages that are received from non-relayed addresses");
PARAM(netIceIgnoreIndirectMsgs, "Ignores ICE messages that are received via relay server");
PARAM(netIceQuickConnectTimeoutMs, "Sets the QuickConnect timeout in milliseconds.");
PARAM(neticetestwrongrelayaddr, "Pretends we don't have the remote peer's correct relay address, so we need to discover it during NAT traversal");
XPARAM(netforceuserelay);
ICE_PARAM(neticeforcesymmetricsocket, "Always use the symmetric socket for all NAT traversals");
PARAM(neticesymsocketlocal, "Map the symmetric socket to the local LAN address");
XPARAM(netrealworld);
#endif

NOSTRIP_FINAL_LOGGING_PARAM(neticesenddebuginfo, "Tell the remote peer to send debug info to the local peer after each NAT traversal");

#if __FINAL_LOGGING
NOSTRIP_XPARAM(netforceuserelay);
#endif

extern const rlTitleId* g_rlTitleId;

RAGE_DEFINE_SUBCHANNEL(ragenet, tunneler_ice)
#undef __rage_channel
#define __rage_channel ragenet_tunneler_ice

#if !__NO_OUTPUT
#define netIceLogfHelper(logger,severity,fmt,...)\
	diagLogfHelper(Channel_ragenet_tunneler_ice, severity, "[%u] [S:%u T:%u]: " fmt,\
	(logger) ? (logger)->GetTunnelRequestId() : 0, (logger) ? (logger)->GetSessionId() : 0, (logger) ? (logger)->GetElapsedTime() : 0, ##__VA_ARGS__);

//Use this version when calling from outside the session.
#define netIceDebugPtr(logger, fmt, ...)     netIceLogfHelper(logger,DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netIceDebug1Ptr(logger, fmt, ...)    netIceLogfHelper(logger,DIAG_SEVERITY_DEBUG1, fmt, ##__VA_ARGS__);
#define netIceDebug2Ptr(logger, fmt, ...)    netIceLogfHelper(logger,DIAG_SEVERITY_DEBUG2, fmt, ##__VA_ARGS__);
#define netIceDebug3Ptr(logger, fmt, ...)    netIceLogfHelper(logger,DIAG_SEVERITY_DEBUG3, fmt, ##__VA_ARGS__);
#define netIceWarningPtr(logger, fmt, ...)   netIceLogfHelper(logger,DIAG_SEVERITY_WARNING, fmt, ##__VA_ARGS__);
#define netIceErrorPtr(logger, fmt, ...)     netIceLogfHelper(logger,DIAG_SEVERITY_ERROR, fmt, ##__VA_ARGS__);

#else
#define netIceDebugPtr(logger, fmt, ...)
#define netIceDebug1Ptr(logger, fmt, ...)
#define netIceDebug2Ptr(logger, fmt, ...)
#define netIceDebug3Ptr(logger, fmt, ...)
#define netIceWarningPtr(logger, fmt, ...)
#define netIceErrorPtr(logger, fmt, ...)
#endif

//Use this version when calling from within the session (most common case).
#define netIceDebug(fmt, ...)      netIceDebugPtr(this->m_Logger,fmt,##__VA_ARGS__)
#define netIceDebug1(fmt, ...)     netIceDebug1Ptr(this->m_Logger,fmt,##__VA_ARGS__)
#define netIceDebug2(fmt, ...)     netIceDebug2Ptr(this->m_Logger,fmt,##__VA_ARGS__)
#define netIceDebug3(fmt, ...)     netIceDebug3Ptr(this->m_Logger,fmt,##__VA_ARGS__)
#define netIceWarning(fmt, ...)    netIceWarningPtr(this->m_Logger,fmt,##__VA_ARGS__)
#define netIceError(fmt, ...)      netIceErrorPtr(this->m_Logger,fmt,##__VA_ARGS__)

/*
	The maximum number of tunnel requests we can have active simultaneously.
	Must be >= MAX_NUM_PLAYERS.
*/
static const unsigned MAX_ACTIVE_ICE_SESSIONS = 64;

#if ICE_TUNNELER_LEAN
static const unsigned MAX_CANDIDATES_PER_PEER = 5;
static const unsigned MAX_CANDIDATE_PAIRS = 8;
static const unsigned MAX_TRIGGERED_CHECKS_QUEUED_PER_SESSION = MAX_CANDIDATE_PAIRS;
static const unsigned MAX_ACTIVE_TRANSACTIONS_PER_SESSION = 10;
static const unsigned MAX_TRANSACTIONS_PER_CANDIDATE_PAIR = 3;
static const unsigned MAX_BLACKLISTED_ADDRESSES_PER_SESSION = 1;
#else
static const unsigned MAX_CANDIDATES_PER_PEER = 10;
static const unsigned MAX_CANDIDATE_PAIRS = 25;
static const unsigned MAX_TRIGGERED_CHECKS_QUEUED_PER_SESSION = MAX_CANDIDATE_PAIRS;
static const unsigned MAX_ACTIVE_TRANSACTIONS_PER_SESSION = 30;
static const unsigned MAX_TRANSACTIONS_PER_CANDIDATE_PAIR = 3;
static const unsigned MAX_BLACKLISTED_ADDRESSES_PER_SESSION = 3;
#endif

/*
	How often to start a new connectivity check transaction.
	This corresponds to the 'Ta' value in the ICE specification.
	Note that transaction retries use TRANSACTION_RETRY_INTERVAL_MS.
*/
static const unsigned CHECK_INTERVAL_MS = 20;

/*
	Delay the checklist if we're on a Strict NAT. We want the remote
	peer to send to our canonical candidate first, to avoid creating a
	(random) mapping on the NAT towards the other player. If the local
	NAT has port forwarding set up, then once we receive a packet
	from our canonical port, the NAT will use that port as its mapping.
*/
static const unsigned CHECK_START_DELAY_STRICT_NAT_MS = 2000;

/*
	How often we resend offer packets or connectivity check pings per remote peer.
	This corresponds to the 'RTO' retry timer value in the ICE specification.
*/
static const unsigned TRANSACTION_RETRY_INTERVAL_MS = 500;

/*
	How often we resend relayed offer packets per remote peer.
	This corresponds to the 'RTO' retry timer value in the ICE specification.
*/
static const unsigned TRANSACTION_RELAY_RETRY_INTERVAL_MS = 500;

/*
	The maximum number of times to retry sending offers and pings.
*/
static const unsigned MAX_RETRIES_PER_TRANSACTION = 8;

/*
	The maximum amount of time a session can take before canceling it.
	We do 2 sets of offers, the first set is sent directly to the peer, 
	the second set is sent over the relay.
*/
static const unsigned MAX_ICE_SESSION_TME_MS = (20 * 1000);

/*
	Make sure tunnel requests can't last longer than 20 seconds
	(somewhat arbitrary but if it takes longer than that it probably isn't working).
*/
CompileTimeAssert(MAX_ICE_SESSION_TME_MS <= (20 * 1000));

/*
	Transactions wait for a response even after the transaction
	has used up all its retries.
*/
static const unsigned MIN_TRANSACTION_TTL_MS = 8 * 1000;

/*
	8.3.  Freeing Candidates
	Once ICE processing has reached the Completed state for all peers 
	for media streams using those candidates, the agent SHOULD wait an 
	additional three seconds, and then it MAY cease responding to 
	checks or generating triggered checks on that candidate.

	[NS] we wait longer than 3 seconds in case of game stalls, etc.
*/
static const unsigned TERMINATION_TIME_MS = 8 * 1000;

/*
	If we've sent an indirect offer but haven't received a response
	within this amount of time, then we refresh our local relay
	mapping to ensure our NAT hasn't closed the port to our relay.
*/
static const unsigned LOCAL_RELAY_REFRESH_TIMEOUT_MS = 1500;

static const unsigned NET_INVALID_TUNNEL_REQUEST_ID = 0;
static const unsigned NET_INVALID_ICE_SESSION_ID = 0;
static const unsigned NET_INVALID_ICE_TRANSACTION_ID = 0;

static netRandom s_IceRng;

#if !__NO_OUTPUT

static unsigned s_TotalRemoteFails = 0;
static unsigned s_TotalRemoteSuccess = 0;

#if GTA_VERSION
__THREAD char s_TempToStringBuf[320];
#define OBJ_TO_STRING(x) ((x).ToString(s_TempToStringBuf, sizeof(s_TempToStringBuf)))
#else
struct TempToStringStruct
{
    char m_String[320];
};
    
DEFINE_THREAD_VAR(TempToStringStruct, s_TempToStringBuf);
#define OBJ_TO_STRING(x) ((x).ToString(((TempToStringStruct&)s_TempToStringBuf).m_String, sizeof(((TempToStringStruct&)s_TempToStringBuf).m_String)))
#endif

//////////////////////////////////////////////////////////////////////////
//  netIceLogger
//////////////////////////////////////////////////////////////////////////
class netIceLogger
{
public:

	netIceLogger(netIceLogger* logger)
	{
		this->Clear();

		if(logger)
		{
			m_TunnelRequestId = logger->m_TunnelRequestId;
			m_SessionId = logger->m_SessionId;
			m_StopWatch = logger->m_StopWatch;
		}
	}

	netIceLogger(unsigned* tunnelRequestId, unsigned* sessionId, netStopWatch* stopWatch)
	{
		this->Clear();

		m_TunnelRequestId = tunnelRequestId;
		m_SessionId = sessionId;
		m_StopWatch = stopWatch;
	}

	void Clear()
	{
		m_TunnelRequestId = NULL;
		m_SessionId = NULL;
		m_StopWatch = NULL;
	}

	unsigned GetTunnelRequestId()
	{
		if(m_TunnelRequestId)
		{
			return *m_TunnelRequestId;
		}
		return 0;
	}

	unsigned GetSessionId()
	{
		if(m_SessionId)
		{
			return *m_SessionId;
		}
		return 0;
	}

	unsigned GetElapsedTime()
	{
		if(m_StopWatch)
		{
			return m_StopWatch->GetElapsedMilliseconds();
		}
		return 0;
	}

private:
	unsigned* m_TunnelRequestId;
	unsigned* m_SessionId;
	netStopWatch* m_StopWatch;
};
#else
class netIceLogger
{
public:

	netIceLogger(netIceLogger* UNUSED_PARAM(logger))
	{
	}

	netIceLogger(unsigned* UNUSED_PARAM(sessionId), netStopWatch* UNUSED_PARAM(stopWatch))
	{
	}
};
#endif

/*
Candidate: A transport address that is a potential point of contact
for receipt of media.  Candidates also have properties -- their
type (host, server reflexive, peer reflexive, or relayed),
priority, foundation, and base.
*/
class Candidate
{
public:
	enum CandidateType
	{
		INVALID_CANDIDATE = -1,

		/*
			Host Candidate:  A candidate obtained by binding to a specific port
			from an IP address on the host.  This includes IP addresses on
			physical interfaces and logical ones, such as ones obtained
			through Virtual Private Networks (VPNs) and Realm Specific IP
			(RSIP) [RFC3102] (which lives at the operating system level).
		*/
		HOST_CANDIDATE,

		/*
			Server Reflexive Candidate:  A candidate whose IP address and port
			are a binding allocated by a NAT for an agent when it sent a
			packet through the NAT to a server.  Server reflexive candidates
			can be learned by STUN servers using the Binding request, or TURN
			servers, which provides both a relayed and server reflexive
			candidate.
		*/
		SERVER_REFLEXIVE_CANDIDATE,

		/*
			Peer Reflexive Candidate:  A candidate whose IP address and port are
			a binding allocated by a NAT for an agent when it sent a STUN
			Binding request through the NAT to its peer.
		*/
		PEER_REFLEXIVE_CANDIDATE,

		/*
			[NS] Custom extension to support Stricts connecting with Moderates.
			Symmetric Inference Candidate:  A candidate obtained by inferring
			which port the NAT opened when sending a 'port opener' packet to a
			remote peer. The inference is made by first pinging one or 
			more relay servers to see which ports each of them reported,
			while also sending a packet to the remote peer within a few
			milliseconds of the packets sent to the relays. By analyzing
			the ports reported by the relays we can infer which port was
			opened by the NAT when sending a packet to the remote peer.
			This works when the Symmetric NAT uses contiguous port allocation.
		*/
		SYMMETRIC_INFERENCE_CANDIDATE,

		/*
			[NS] Many types of NATs/networks benefit from using a shared socket
			for all NAT traversals. However, some NATs/networks don't
			work well with a shared socket approach. For example, a
			port-preserving symmetric NAT will use the same public port as the
			socket's private port for the *first* endpoint it sends to
			(and only the first endpoint) so the mapped port is predictable,
			but only for the first endpoint to which it sends. For these types
			of NATs, we attempt to establish a connection over a unique socket,
			which is only used to connect two peers together, and does not
			combine traffic to/from other peers. This allows, for example, two
			port-preserving symmetric NATs to make a direct connection. Some
			non-symmetric, but non-deterministic NATs also benefit from this
			separate socket technique.
		*/
		SYMMETRIC_SOCKET_CANDIDATE,

		/*
			[NS] Canonical Candidate: A candidate whose IP is the same as the
			server reflexive IP, and whose port is the game's standard binding
			port. Peers who have forwarded this standard port from their router
			to their private host address will be able to communicate via this
			standard port, even if they're behind a random port allocating
			symmetric NAT without UPnP/NAT-PMP/PCP/port forwarding enabled.
		*/
		CANONICAL_CANDIDATE,

		NUM_CANDIDATE_TYPES
	};

	Candidate()
#if !__NO_OUTPUT
	: m_Logger(NULL)
#endif
	{
		this->Clear();
	}

 	Candidate(netIceLogger* OUTPUT_ONLY(logger))
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
 	{
 		this->Clear();
 	}

	Candidate(netIceLogger* OUTPUT_ONLY(logger), CandidateType type, const netSocketAddress& transportAddress)
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		this->Reset(type, transportAddress);
	}

	void Clear()
	{
		SetType(INVALID_CANDIDATE);
		m_TransportAddress.Clear();
	}

	const netSocketAddress& GetTransportAddress() const
	{
		return m_TransportAddress;
	}

	CandidateType GetType() const
	{
		return m_Type;
	}

	bool IsValid() const
	{
		return (m_Type != INVALID_CANDIDATE) && m_TransportAddress.IsValid();
	}

	bool operator==(const Candidate& rhs) const
	{
		return (m_Type == rhs.m_Type) && (m_TransportAddress == rhs.m_TransportAddress);
	}

#if !__NO_OUTPUT
	static const char* CandidateTypeToString(const CandidateType type)
	{
		switch(type)
		{
			case INVALID_CANDIDATE: return "INVALID_CANDIDATE"; break;
			case HOST_CANDIDATE: return "HOST_CANDIDATE"; break;
			case SERVER_REFLEXIVE_CANDIDATE: return "SERVER_REFLEXIVE_CANDIDATE"; break;
			case PEER_REFLEXIVE_CANDIDATE: return "PEER_REFLEXIVE_CANDIDATE"; break;
			case SYMMETRIC_INFERENCE_CANDIDATE: return "SYMMETRIC_INFERENCE_CANDIDATE"; break;
			case SYMMETRIC_SOCKET_CANDIDATE: return "SYMMETRIC_SOCKET_CANDIDATE"; break;
			case CANONICAL_CANDIDATE: return "CANONICAL_CANDIDATE"; break;
			case NUM_CANDIDATE_TYPES: break;
		}

		netAssertf(false, "Unknown Candidate Type: %d", (int)type);
		return "**UNKNOWN CANDIATE TYPE**";
	}

	static const unsigned TO_STRING_BUFFER_SIZE = 128;

	const char* ToString(char* buf, const unsigned sizeOfBuf) const
	{
		return formatf_sized(buf, sizeOfBuf,
					   "[Type: %s, Transport Address: " NET_ADDR_FMT "]",
					   CandidateTypeToString(m_Type),
					   NET_ADDR_FOR_PRINTF(m_TransportAddress));
	}
#endif

private:
	void Reset(CandidateType type, const netSocketAddress& transportAddress)
	{
		this->Clear();
		SetType(type);
		m_TransportAddress = transportAddress;
	}

	void SetType(CandidateType type)
	{
		m_Type = type;
	}

	netSocketAddress m_TransportAddress;
	CandidateType m_Type;
	netIceLogger* m_Logger;
};

/*
Local Candidate:  A candidate that an agent has obtained and included
in an offer or answer it sent.
*/
class LocalCandidate : public Candidate
{
public:
	LocalCandidate() {};
	LocalCandidate(netIceLogger* logger) : Candidate(logger) {};
	LocalCandidate(netIceLogger* logger, CandidateType type, const netSocketAddress& transportAddress)
		: Candidate(logger, type, transportAddress) {}
};

/*
Remote Candidate:  A candidate that an agent received in an offer or
answer from its peer.
*/
class RemoteCandidate : public Candidate
{
public:
	RemoteCandidate() {};
	RemoteCandidate(netIceLogger* logger) : Candidate(logger) {};
	RemoteCandidate(netIceLogger* logger, CandidateType type, const netSocketAddress& transportAddress)
		: Candidate(logger, type, transportAddress) {}
};

/*
	Candidate List:  A list of Candidates.
*/
template<typename T, unsigned MAX_CANDIDATES>
class CandidateList
{
public:
	CandidateList(netIceLogger* OUTPUT_ONLY(logger))
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		Clear();
	}

	void Clear()
	{
		m_Candidates.Reset();
	}

	T* AddCandidate(const T& candidate)
	{
		if(netVerify(m_Candidates.IsFull() == false) &&
			candidate.IsValid() &&
			(FindByTransportAddress(candidate.GetTransportAddress()) == nullptr))
		{
			m_Candidates.Push(candidate);
			return &m_Candidates.Top();
		}
		return NULL;
	}

	unsigned GetCount() const
	{
		return m_Candidates.GetCount();
	}

	const T& operator[](unsigned index) const
	{
		return m_Candidates[index];
	}

	const T* FindByTransportAddress(const netSocketAddress& transportAddress) const
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Candidates[i].GetTransportAddress() == transportAddress)
			{
				return &m_Candidates[i];
			}
		}

		return NULL;
	}

private:
	atFixedArray<T, MAX_CANDIDATES> m_Candidates;

protected:
	netIceLogger* m_Logger;
};

/*
	A list of Local Candidates.
*/
class LocalCandidates : public CandidateList<LocalCandidate, MAX_CANDIDATES_PER_PEER>
{
public:
	LocalCandidates(netIceLogger* logger) : CandidateList<LocalCandidate, MAX_CANDIDATES_PER_PEER>(logger) {}

	LocalCandidate* AddCandidate(const LocalCandidate& candidate)
	{
		netIceDebug3("Adding local candidate: %s", OBJ_TO_STRING(candidate));
		return CandidateList<LocalCandidate, MAX_CANDIDATES_PER_PEER>::AddCandidate(candidate);
	}
};

/*
	A list of Remote Candidates.
*/
class RemoteCandidates : public CandidateList<RemoteCandidate, MAX_CANDIDATES_PER_PEER>
{
public:
	RemoteCandidates(netIceLogger* logger) :  CandidateList<RemoteCandidate, MAX_CANDIDATES_PER_PEER>(logger) {}

	RemoteCandidate* AddCandidate(const RemoteCandidate& candidate)
	{
		netIceDebug3("Adding remote candidate: %s", OBJ_TO_STRING(candidate));
		return CandidateList<RemoteCandidate, MAX_CANDIDATES_PER_PEER>::AddCandidate(candidate);
	}
};

/*
	Candidate Pair:  A pairing containing a local candidate and a remote candidate.
*/
class CandidatePair
{
public:
	enum PairState
	{
		/*
			A check has not been performed for this pair, and can be
			performed as soon as it is the highest-priority Waiting pair on
			the check list.
		*/
		STATE_WAITING,

		/*
			A check has been sent for this pair, but the
			transaction is in progress.
		*/
		STATE_IN_PROGRESS,

		/*
			A check for this pair was already done and produced a
			successful result.
		*/
		STATE_SUCCEEDED,

		/*
			A check for this pair was already done and failed, either
			never producing any response or producing an unrecoverable failure
			response.
		*/
		STATE_FAILED,
	};

	CandidatePair()
#if !__NO_OUTPUT
	: m_Logger(NULL)
#endif
	{
		this->Clear();
	}

	CandidatePair(netIceLogger* OUTPUT_ONLY(logger))
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		this->Clear();
	}

	CandidatePair(netIceLogger* OUTPUT_ONLY(logger), const LocalCandidate& local, const RemoteCandidate& remote)
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		this->Reset(local, remote);
	}

	PairState GetState() const
	{
		return m_State;
	}

	void SetState(PairState state)
	{
		if(m_State != state)
		{
			netIceDebug3("Changing state of pair from %s to %s: %s", PairStateToString(m_State), PairStateToString(state), OBJ_TO_STRING(*this));
			m_State = state;
		}
	}

	void IncreaseTransactionCount()
	{
		m_TransactionCount++;
	}

	u32 GetTransactionCount()
	{
		return m_TransactionCount;
	}

	/*
		Valid:  If a candidate pair has its valid flag set, it
				has been validated by a successful STUN transaction
				and may be nominated by ICE for sending and receiving media.
	*/
 	bool IsValid() const
 	{
 		return m_IsValid;
 	}

	void SetValid()
	{
		m_IsValid = true;
	}

	/*
		Nominated:  If a valid candidate pair has its nominated flag set, it
					means that it may be selected by ICE for sending and receiving
					media.
	*/
	bool IsNominated() const
	{
		return m_IsNominated;
	}

	void SetNominated()
	{
		netIceDebug3("Nominating pair: %s", OBJ_TO_STRING(*this));
		m_IsNominated = true;
	}

	bool ShouldNominateOnSuccess() const
	{
		return m_SetNominatedOnSuccess;
	}

	void SetNominatedOnSuccess()
	{
		m_SetNominatedOnSuccess = true;
	}

	bool ShouldUseCandidate() const
	{
		return m_UseCandidate;
	}

	void SetUseCandidate()
	{
		m_UseCandidate = true;
	}

	const LocalCandidate& GetLocalCandidate() const
	{
		return m_Local;
	}

	const RemoteCandidate& GetRemoteCandidate() const
	{
		return m_Remote;
	}

	bool operator==(const CandidatePair& rhs) const
	{
		return (m_Local == rhs.m_Local) && (m_Remote == rhs.m_Remote);
	}

#if !__NO_OUTPUT
	static const char* PairStateToString(PairState state)
	{
		switch(state)
		{
			case STATE_WAITING: return "STATE_WAITING"; break;
			case STATE_IN_PROGRESS: return "STATE_IN_PROGRESS"; break;
			case STATE_SUCCEEDED: return "STATE_SUCCEEDED"; break;
			case STATE_FAILED: return "STATE_FAILED"; break;
		}

		netAssertf(false, "Unknown State: %d", (int)state);
		return "**UNKNOWN STATE**";
	}

	static const unsigned TO_STRING_BUFFER_SIZE = 320;

	const char* ToString(char* buf, const unsigned sizeOfBuf) const
	{
		char local[LocalCandidate::TO_STRING_BUFFER_SIZE];
		char remote[RemoteCandidate::TO_STRING_BUFFER_SIZE];
		m_Local.ToString(local, sizeof(local));
		m_Remote.ToString(remote, sizeof(remote));

		return formatf_sized(buf, sizeOfBuf,
					   "[Local: %s, "
					   "Remote: %s, "
					   "State: %s, "
					   "IsValid: %s, "
					   "UseCandidate: %s, "
					   "SetNominatedOnSuccess: %s, "
					   "IsNominated: %s]",
					   local,
					   remote,
					   PairStateToString(m_State),
					   m_IsValid ? "true" : "false",
					   m_UseCandidate ? "true" : "false",
					   m_SetNominatedOnSuccess ? "true" : "false",
					   m_IsNominated ? "true" : "false");
	}
#endif

private:
	void Clear()
	{
		m_Local.Clear();
		m_Remote.Clear();
		m_State = STATE_WAITING;
		m_IsValid = false;
		m_IsNominated = false;
		m_SetNominatedOnSuccess = false;
		m_UseCandidate = false;
		m_TransactionCount = 0;
	}

	void Reset(const LocalCandidate& local, const RemoteCandidate& remote)
	{
		this->Clear();
		m_Local = local;
		m_Remote = remote;
	}

	LocalCandidate m_Local;
	RemoteCandidate m_Remote;
	PairState m_State;
	netIceLogger* m_Logger;
	u8 m_TransactionCount;
	u8 m_IsValid : 1;
	u8 m_IsNominated : 1;
	u8 m_SetNominatedOnSuccess : 1;
	u8 m_UseCandidate : 1;
};

/*
Check List:  An ordered set of candidate pairs that an agent will use
to generate checks.
*/
class CheckList
{
public:

	enum CheckListState
	{
		/*
			In this state, ICE checks are still in progress for this media stream.
		*/
		STATE_RUNNING,

		/*
			In this state, ICE checks have produced nominated pairs
			for each component of the media stream.  Consequently, 
			ICE has succeeded and media can be sent.
		*/
		STATE_COMPLETED,

		/*
			In this state, the ICE checks have not completed
			successfully for this media stream.
		*/
		STATE_FAILED,
	};

	CheckList(netIceLogger* OUTPUT_ONLY(logger))
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		Clear();
	}

	void Clear()
	{
		m_State = STATE_RUNNING;
		m_CandidatePairs.Reset();
	}

	CheckListState GetState() const
	{
		return m_State;
	}

	void SetState(CheckListState state)
	{
		netIceDebug3("Changing state of checklist from %s to %s", CheckListStateToString(m_State), CheckListStateToString(state));

		m_State = state;
	}

	unsigned GetCount() const
	{
		return m_CandidatePairs.GetCount();
	}

	bool AddCandidatePair(CandidatePair* pair)
	{
		netIceDebug3("Adding candidate pair to checklist: %s", OBJ_TO_STRING(*pair));

		if(!netVerify(FindMatchingPair(*pair) == NULL))
		{
			netIceDebug3("Matching candidate pair is already on Check List!: %s", OBJ_TO_STRING(*pair));
			return false;
		}

		if(netVerify(m_CandidatePairs.IsFull() == false))
		{
			// appends to end of the array
			m_CandidatePairs.Push(pair);
			return true;
		}

		return false;
	}

	CandidatePair* GetCandidatePair(unsigned index)
	{
		if(netVerify(index < GetCount()))
		{
			return m_CandidatePairs[index];
		}
		return NULL;
	}

	CandidatePair* GetNextWaitingPair()
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			CandidatePair* pair = GetCandidatePair(i);
			if(pair->GetState() == CandidatePair::STATE_WAITING)
			{
				return pair;
			}
		}
		return NULL;
	}
	
	CandidatePair* FindMatchingPair(const CandidatePair& pair)
	{		
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			CandidatePair* listPair = GetCandidatePair(i);
			if(*listPair == pair)
			{
				return listPair;
			}
		}

		return NULL;
	}

	bool Contains(const CandidatePair* pair)
	{		
		if(!netVerify(pair != NULL))
		{
			return false;
		}

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			CandidatePair* listPair = GetCandidatePair(i);
			if(listPair == pair)
			{
				return true;
			}
		}

		return false;
	}

	void FailPairsWithTargetAddr(const netSocketAddress& target)
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			CandidatePair* pair = GetCandidatePair(i);

			if((pair->GetRemoteCandidate().GetTransportAddress() == target) &&
				(pair->GetState() != CandidatePair::STATE_SUCCEEDED))
			{
				pair->SetState(CandidatePair::STATE_FAILED);
			}
		}
	}

	void RemoveAllWaitingPairs()
	{
		// fixed array doesn't like us removing elements while we're iterating

		bool deleted = false;
		do 
		{
			deleted = false;
			for(unsigned i = 0; i < GetCount(); ++i)
			{
				CandidatePair* listPair = GetCandidatePair(i);

				if(listPair->GetState() == CandidatePair::STATE_WAITING)
				{
					deleted = true;
					m_CandidatePairs.Delete(i);
					break;
				}
			}
		} while (deleted);
	}

private:
#if !__NO_OUTPUT
	static const char* CheckListStateToString(CheckListState state)
	{
		switch(state)
		{
		case STATE_RUNNING: return "STATE_RUNNING"; break;
		case STATE_COMPLETED: return "STATE_COMPLETED"; break;
		case STATE_FAILED: return "STATE_FAILED"; break;
		}

		netAssertf(false, "Unknown State: %d", (int)state);
		return "**UNKNOWN STATE**";
	}
#endif

	atFixedArray<CandidatePair*, MAX_CANDIDATE_PAIRS> m_CandidatePairs;
	CheckListState m_State;

#if !__NO_OUTPUT
	netIceLogger* m_Logger;
#endif
};

/*
Triggered Check:  A connectivity check generated as a consequence of
the receipt of a connectivity check from the peer.

The agent maintains a FIFO queue, called the triggered check queue,
which contains candidate pairs for which checks are to be sent at
the next available opportunity.
*/
class TriggeredCheckQueue
{
public:

	TriggeredCheckQueue(netIceLogger* OUTPUT_ONLY(logger))
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		Clear();
	}

	void Clear()
	{
		m_CandidatePairs.Reset();
	}

	// adds an item to the end of the queue
	bool Push(CandidatePair* pair)
	{
		netIceDebug3("Enqueing candidate pair to the triggered queue: %s", OBJ_TO_STRING(*pair));

		if(!netVerify(FindMatchingPair(*pair) == NULL))
		{
			netIceDebug3("Matching candidate pair is already on Triggered Check Queue!: %s", OBJ_TO_STRING(*pair));
			return false;
		}

		if(netVerify(m_CandidatePairs.IsFull() == false))
		{
			m_CandidatePairs.Insert(0) = pair;
			return true;
		}

		return false;
	}

	CandidatePair* FindMatchingPair(const CandidatePair& pair)
	{		
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			CandidatePair* listPair = m_CandidatePairs[i];
			if(*listPair == pair)
			{
				return listPair;
			}
		}

		return NULL;
	}

	void RemovePairsWithTargetAddr(const netSocketAddress& target)
	{
		// fixed array doesn't like us removing elements while we're iterating

		bool deleted = false;
		do
		{
			deleted = false;
			for (unsigned i = 0; i < GetCount(); ++i)
			{
				CandidatePair* pair = m_CandidatePairs[i];
				if(pair->GetRemoteCandidate().GetTransportAddress() == target)
				{
					deleted = true;
					netIceDebug3("Removing candidate pair from the triggered queue: %s", OBJ_TO_STRING(*pair));
					m_CandidatePairs.Delete(i);
					break;
				}
			}
		} while (deleted);
	}


	// removes the first item from the queue and returns it
	CandidatePair* GetNext()
	{
		if(GetCount() > 0)
		{
			return m_CandidatePairs.Pop();
		}

		return NULL;
	}

private:
	unsigned GetCount() const
	{
		return m_CandidatePairs.GetCount();
	}

	atFixedArray<CandidatePair*, MAX_TRIGGERED_CHECKS_QUEUED_PER_SESSION> m_CandidatePairs;

#if !__NO_OUTPUT
	netIceLogger* m_Logger;
#endif
};

/*
Valid List:  An ordered set of candidate pairs for a media stream
that have been validated by a successful STUN transaction.
*/
class ValidList
{
public:

	ValidList(netIceLogger* OUTPUT_ONLY(logger))
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		Clear();
	}

	void Clear()
	{
		m_CandidatePairs.Reset();
	}

	unsigned GetCount() const
	{
		return m_CandidatePairs.GetCount();
	}

	bool AddValidPair(CandidatePair* pair)
	{
		netAssert(pair->IsValid());
		netIceDebug3("Adding candidate pair to valid list: %s", OBJ_TO_STRING(*pair));

		if(!netVerify(FindMatchingPair(*pair) == NULL))
		{
			netIceDebug3("Matching candidate pair is already on Check List!: %s", OBJ_TO_STRING(*pair));
			return false;
		}

		if(netVerify(m_CandidatePairs.IsFull() == false))
		{
			// appends to end of the array
			m_CandidatePairs.Push(pair);
			return true;
		}

		return false;
	}

	CandidatePair* GetValidPair(unsigned index)
	{
		if(netVerify(index < GetCount()))
		{
			return m_CandidatePairs[index];
		}
		return NULL;
	}

	const CandidatePair* GetValidPair(unsigned index) const
	{
		if(netVerify(index < GetCount()))
		{
			return m_CandidatePairs[index];
		}
		return NULL;
	}

	CandidatePair* FindMatchingPair(const CandidatePair& pair)
	{		
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			CandidatePair* listPair = GetValidPair(i);
			if(*listPair == pair)
			{
				return listPair;
			}
		}

		return NULL;
	}

private:
	atFixedArray<CandidatePair*, MAX_CANDIDATE_PAIRS> m_CandidatePairs;

#if !__NO_OUTPUT
	netIceLogger* m_Logger;
#endif
};

/*
Discovered Pairs:  The complete set of candidate pairs we've discovered during an ICE session.
The Check List, Triggered Check Queue, and Valid List contain pointers to the candidate pairs in this list.
There may be pairs in this list that are never added to the check list.
*/
class DiscoveredPairs
{
public:

	DiscoveredPairs(netIceLogger* OUTPUT_ONLY(logger))
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		Clear();
	}

	void Clear()
	{
		m_CandidatePairs.Reset();
	}

	unsigned GetCount() const
	{
		return m_CandidatePairs.GetCount();
	}

	CandidatePair* AddCandidatePair(const CandidatePair& pair)
	{
		netIceDebug3("Adding candidate pair to discovered pairs list: %s", OBJ_TO_STRING(pair));

		if(!netVerify(FindMatchingPair(pair) == NULL))
		{
			netIceDebug3("Matching candidate pair is already on Discovered Pairs list!: %s", OBJ_TO_STRING(pair));
			return NULL;
		}

		if(netVerify(m_CandidatePairs.IsFull() == false))
		{
			// appends to end of the array
			m_CandidatePairs.Push(pair);
			return &m_CandidatePairs.Top();
		}

		return NULL;
	}

	CandidatePair* GetCandidatePair(unsigned index)
	{
		if(netVerify(index < GetCount()))
		{
			return &m_CandidatePairs[index];
		}
		return NULL;
	}

	CandidatePair* FindMatchingPair(const CandidatePair& pair)
	{		
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			CandidatePair* listPair = GetCandidatePair(i);
			if(*listPair == pair)
			{
				return listPair;
			}
		}

		return NULL;
	}

	bool Contains(const CandidatePair* pair)
	{		
		if(!netVerify(pair != NULL))
		{
			return false;
		}

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			CandidatePair* listPair = GetCandidatePair(i);
			if(listPair == pair)
			{
				return true;
			}
		}

		return false;
	}

private:
	atFixedArray<CandidatePair, MAX_CANDIDATE_PAIRS> m_CandidatePairs;

#if !__NO_OUTPUT
	netIceLogger* m_Logger;
#endif
};

/*
	Blacklist: not officially part of ICE. Maintains a list of addresses that are
	blocked from sending/receiving during an ICE session. These are used to fix 
	issues with some symmetric NATs that have endpoint independent filtering.
	We might ping a port, which their firewall lets through. Then they send a pong
	back over a different port, since it's a symmetric NAT. When we ping that port,
	we may end up opening another port if we are also symmetric, which they add as a 
	peer reflexive candidate. When they then send a response, it can use yet 
	another port, and so on until both peers fill up their remote candidate
	list to capacity, and none of them are usable. This also wastes resources on
	the NATs as they open many unusable ports towards each other.
*/
class Blacklist
{
public:

	Blacklist(netIceLogger* OUTPUT_ONLY(logger))
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		Clear();
	}

	void Clear()
	{
		m_Addresses.Reset();
	}

	unsigned GetCount() const
	{
		return m_Addresses.GetCount();
	}

	bool IsFull() const
	{
		return m_Addresses.IsFull();
	}

	bool AddAddress(const netSocketAddress& addr)
	{
		netAssert(addr.IsValid());

		if(IsBlacklisted(addr))
		{
			return false;
		}

		netIceDebug3("Adding address to blacklist: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr));

		if(m_Addresses.IsFull())
		{
			netIceDebug3("Blacklist is full.");
		}
		else
		{
			// appends to end of the array
			m_Addresses.Push(addr);
			return true;
		}

		return false;
	}

	bool IsBlacklisted(const netSocketAddress& addr)
	{		
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Addresses[i] == addr)
			{
				return true;
			}
		}

		return false;
	}

private:
	atFixedArray<netSocketAddress, MAX_BLACKLISTED_ADDRESSES_PER_SESSION> m_Addresses;
#if !__NO_OUTPUT
	netIceLogger* m_Logger;
#endif
};

/*
	SymmetricSocket: state that tracks the SYMMETRIC_SOCKET_CANDIDATE candidate
*/
class SymmetricSocket
{
public:

	enum State
	{
		// be careful of the order, we do >= comparisons on the state, and we
		// send these values to telemetry.
		STATE_UNATTEMPTED = 0,
		STATE_WAITING_FOR_SOCKET,
		STATE_FAILED_TO_CREATE,
		STATE_WAITING_FOR_PORT_MAPPER,
		STATE_MAPPING_PORT,
		STATE_ASSUME_PORT_PRESERVING,
		STATE_MAP_SUCCEEDED,
		STATE_MAP_FAILED,
		STATE_READY_FOR_ASSOCIATION,
		STATE_ASSOCIATED,
		STATE_VALID_CANDIDATE,
		STATE_NOMINATED_CANDIDATE,
		NUM_STATES,
	};

	static const unsigned MAX_EXPORTED_SIZE_IN_BITS = datBitsNeeded<NUM_STATES>::COUNT +					// m_State
													  (netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES << 3); 	// m_MappedAddr

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((MAX_EXPORTED_SIZE_IN_BITS + 7) & ~7) >> 3;		// round up to nearest multiple of 8 then divide by 8

	// when to allow symmetric sockets (can be set via tunable)
	enum Condition
	{
		NEVER = 0,								// never allow symmetric sockets
		PORT_PRESERVING_SYMMETRIC = 1,			// only when local NAT is a port-preserving symmetric
		PP_SYM_AND_NON_DETERMINISTIC = 2,		// when local NAT is port-preserving symmetric or flagged non-deterministic 
		PP_SYM_AND_NON_SYMMETRIC = 3,			// when local NAT is port-preserving symmetric or has any non-symmetric behaviour
		ALWAYS = 4,								// always allow
	};

	SymmetricSocket() { Clear(); }
	~SymmetricSocket() { Clear(); }

	void Clear()
	{
		m_State = STATE_UNATTEMPTED;
		m_PortMapper.Clear();
		m_MappedAddr.Clear();
		m_RemoteMappedAddr.Clear();
		m_SocketId = netSocket::INVALID_SOCKET_ID;

		if(m_Status.Pending())
		{
			m_Status.SetCanceled();
		}
		m_Status.Reset();
		m_Finished = false;
	}

	bool Export(void* buf,
				const unsigned sizeofBuf,
				unsigned* size = 0) const
	{
		rtry
		{
			datExportBuffer bb;
			bb.SetReadWriteBytes(buf, sizeofBuf);

			rverify(bb.SerUns(m_State, datBitsNeeded<NUM_STATES>::COUNT), catchall, );

			if(m_State >= STATE_READY_FOR_ASSOCIATION)
			{
				rverify(bb.SerUser(m_MappedAddr), catchall, );
			}
		
			netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

			if(size){*size = bb.GetNumBytesWritten();}

			return true;
		}
		rcatchall
		{
			if(size){*size = 0;}
			return false;
		}

	}

	bool Import(const void* buf,
				const unsigned sizeofBuf,
				unsigned* size = 0)
	{
		rtry
		{
			Clear();

			datImportBuffer bb;
			bb.SetReadOnlyBytes(buf, sizeofBuf);
			
			rverify(bb.SerUns(m_State, datBitsNeeded<NUM_STATES>::COUNT), catchall, );

			if(m_State >= STATE_READY_FOR_ASSOCIATION)
			{
				rverify(bb.SerUser(m_MappedAddr), catchall, );
			}

			if(size){*size = bb.GetNumBytesRead();}

			return true;
		}
		rcatchall
		{
			if(size){*size = 0;}
			return false;
		}
	}

	SymmetricSocket& operator=(const SymmetricSocket& other)
	{
		// copy the data, but not the port mapper, since it contains objects that can't be copied.
		// We only need to copy the data so it can be exported in a netIceSessionOffer.

		m_State = other.m_State;
		m_MappedAddr = other.m_MappedAddr;
		m_RemoteMappedAddr = other.m_RemoteMappedAddr;
		m_SocketId = other.m_SocketId;

		return *this;
	}

	bool IsAssociated() const
	{
		return m_State >= STATE_ASSOCIATED;
	}
	
	static unsigned sm_NumNominated;

	State m_State;
	netNatPortMapper m_PortMapper;
	netSocketAddress m_MappedAddr;
	netSocketAddress m_RemoteMappedAddr;
	unsigned m_SocketId;
	bool m_Finished;
	netStatus m_Status;
};

unsigned SymmetricSocket::sm_NumNominated = 0;

/*
	SymmetricInference: state that tracks the SYMMETRIC_INFERENCE_CANDIDATE candidate
*/
class SymmetricInference
{
public:

	enum State
	{
		SYM_STATE_UNATTEMPTED = 0,
		SYM_STATE_WAITING_FOR_PORT_MAPPER,
		SYM_STATE_DISCOVERING,
		SYM_STATE_SUCCEEDED,
		SYM_STATE_FAILED,
		NUM_STATES,
	};

	static const unsigned MAX_EXPORTED_SIZE_IN_BITS = datBitsNeeded<NUM_STATES>::COUNT +		// m_State
													  (sizeof(u16) << 3) +						// m_SymmetricInferredPort
													  ((sizeof(u16) * 2) << 3) +				// m_SymmetricMappedAddrs (ports only)
													  (sizeof(u16) << 3);						// m_PortMapperPort
	
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((MAX_EXPORTED_SIZE_IN_BITS + 7) & ~7) >> 3;		// round up to nearest multiple of 8 then divide by 8

	SymmetricInference() { Clear(); }
	~SymmetricInference() { Clear(); }

	void Clear()
	{
		m_State = SYM_STATE_UNATTEMPTED;
		m_SymmetricInferredPort = 0;

		for(unsigned i = 0; i < COUNTOF(m_RemoteSymmetricInferredPort); ++i)
		{
			m_RemoteSymmetricInferredPort[i] = 0;
		}

		for(unsigned i = 0; i < COUNTOF(m_SymmetricMappedAddrs); ++i)
		{
			m_SymmetricMappedAddrs[i].Clear();
			if(m_SymmetricMappedAddrStatus[i].Pending())
			{
				m_SymmetricMappedAddrStatus[i].SetCanceled();
			}
			m_SymmetricMappedAddrStatus[i].Reset();
		}

		m_PortMapperPort = 0;
		m_PortMapper.Clear();
	}

	bool Export(void* buf,
				const unsigned sizeofBuf,
				unsigned* size = 0) const
	{
		rtry
		{
			datExportBuffer bb;
			bb.SetReadWriteBytes(buf, sizeofBuf);

			rverify(bb.SerUns(m_State, datBitsNeeded<NUM_STATES>::COUNT), catchall, );

			if(m_State == SYM_STATE_SUCCEEDED)
			{
				rverify(bb.SerUns(m_SymmetricInferredPort, sizeof(m_SymmetricInferredPort) << 3), catchall, );

				// the remote peer only sends the mapped ports, not the full address
				for(unsigned i = 0; i < COUNTOF(m_SymmetricMappedAddrs); ++i)
				{
					u16 port = m_SymmetricMappedAddrs[i].GetPort();
					rverify(bb.SerUns(port, sizeof(u16) << 3), catchall, );
				}

				rverify(bb.SerUns(m_PortMapperPort, sizeof(m_PortMapperPort) << 3), catchall, );
			}
		
			netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

			if(size){*size = bb.GetNumBytesWritten();}

			return true;
		}
		rcatchall
		{
			if(size){*size = 0;}
			return false;
		}
	}

	bool Import(const void* buf,
				const unsigned sizeofBuf,
				unsigned* size = 0)
	{
		rtry
		{
			Clear();

			datImportBuffer bb;
			bb.SetReadOnlyBytes(buf, sizeofBuf);
			
			rverify(bb.SerUns(m_State, datBitsNeeded<NUM_STATES>::COUNT), catchall, );

			if(m_State == SYM_STATE_SUCCEEDED)
			{
				rverify(bb.SerUns(m_SymmetricInferredPort, sizeof(m_SymmetricInferredPort) << 3), catchall, );

				// the remote peer only sends the mapped ports, not the full address
				for(unsigned i = 0; i < COUNTOF(m_SymmetricMappedAddrs); ++i)
				{
					u16 port = 0;
					rverify(bb.SerUns(port, sizeof(u16) << 3), catchall, );
					m_SymmetricMappedAddrs[i].Init(netIpAddress(), port);
				}

				rverify(bb.SerUns(m_PortMapperPort, sizeof(m_PortMapperPort) << 3), catchall, );
			}

			if(size){*size = bb.GetNumBytesRead();}

			return true;
		}
		rcatchall
		{
			if(size){*size = 0;}
			return false;
		}
	}

	SymmetricInference& operator=(const SymmetricInference& other)
	{
		// copy the data, but not the port mapper, since it contains objects that can't be copied.
		// We only need to copy the data so it can be exported in a netIceSessionOffer.

		m_State = other.m_State;
		m_SymmetricInferredPort = other.m_SymmetricInferredPort;

		for(unsigned i = 0; i < COUNTOF(m_RemoteSymmetricInferredPort); ++i)
		{
			m_RemoteSymmetricInferredPort[i] = other.m_RemoteSymmetricInferredPort[i];
		}

		for(unsigned i = 0; i < COUNTOF(m_SymmetricMappedAddrs); ++i)
		{
			m_SymmetricMappedAddrs[i] = other.m_SymmetricMappedAddrs[i];
		}

		m_PortMapperPort = other.m_PortMapperPort;

		return *this;
	}

	State m_State;
	u16 m_SymmetricInferredPort;
	u16 m_RemoteSymmetricInferredPort[2];
	netSocketAddress m_SymmetricMappedAddrs[2];
	netStatus m_SymmetricMappedAddrStatus[2];
	u16 m_PortMapperPort;
	netNatCombinedPortMapper m_PortMapper;
};

//////////////////////////////////////////////////////////////////////////
//  netIceSessionOffer
//////////////////////////////////////////////////////////////////////////
class netIceSessionOffer
{
	friend class netIceTunneler;

public:

	static const unsigned CURRENT_VERSION = 3;
	static const unsigned MAX_CANDIDATES_PER_OFFER = 6;

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													 sizeof(u8) +										// m_Version
													 sizeof(unsigned) +									// m_SourceSessionId
													 sizeof(unsigned) +									// m_TransactionId
													 netPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES +		// m_SourcePeerAddr
													 netPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES +		// m_DestPeerAddr
													 sizeof(u8) +										// m_NumCandidates
													 (MAX_CANDIDATES_PER_OFFER *						// m_Candidates
													 netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES) + 
													 sizeof(u64) +										// m_RoleTieBreaker
													 1 +												// m_Bilateral
													 1 +												// m_SendDebugInfo
													 1 +												// m_NonDeterministic
													 SymmetricInference::MAX_EXPORTED_SIZE_IN_BYTES +	// m_SymmetricInference
													 SymmetricSocket::MAX_EXPORTED_SIZE_IN_BYTES +		// m_SymmetricSocket
													 sizeof(u64) +										// m_GameSessionToken
													 sizeof(u64);										// m_GameSessionId
	
	NET_MESSAGE_DECL(netIceSessionOffer, NET_ICE_SESSION_OFFER);

	netIceSessionOffer()
	{

	}

	netIceSessionOffer(const unsigned sourceSessionId,
					   const unsigned transactionId,
					   const netPeerAddress& sourcePeerAddr,
					   const netPeerAddress& destPeerAddr,
					   const netSocketAddress* candidates,
					   const unsigned numCandidates,
					   const u64 roleTieBreaker,
					   const bool bilateral,
					   const bool sendDebugInfo,
					   const bool nonDeterministic,
					   const SymmetricInference& symmetricInference,
					   const SymmetricSocket& symmetricSocket,
					   const u64 gameSessionToken,
					   const u64 gameSessionId)
	{
		netAssert(sourceSessionId != NET_INVALID_ICE_SESSION_ID);

		m_Version = CURRENT_VERSION;
		m_SourceSessionId = sourceSessionId;
		m_TransactionId = transactionId;
		m_SourcePeerAddr = sourcePeerAddr;
		m_DestPeerAddr = destPeerAddr;		
		m_DestPeerAddr.ClearKey();
		m_RoleTieBreaker = roleTieBreaker;
		m_Bilateral = bilateral;
		m_SendDebugInfo = sendDebugInfo;
		m_NonDeterministic = nonDeterministic;

#if __FINAL && !__FINAL_LOGGING
		m_SendDebugInfo = false;
#endif
		m_SymmetricInference = symmetricInference;
		m_SymmetricSocket = symmetricSocket;

		m_GameSessionToken = gameSessionToken;
		m_GameSessionId = gameSessionId;

		if(numCandidates >= COUNTOF(m_Candidates))
		{
			m_NumCandidates = COUNTOF(m_Candidates);
		}
		else
		{
			m_NumCandidates = (u8)numCandidates;
		}

		for(unsigned i = 0; i < m_NumCandidates; ++i)
		{
			m_Candidates[i] = candidates[i];
		}		
	}

	NET_MESSAGE_SER(bb, msg)
	{
		bool success = false;
		if(bb.SerUns(msg.m_Version, sizeof(msg.m_Version) << 3)
			&& (msg.m_Version == CURRENT_VERSION)
			&& bb.SerUns(msg.m_SourceSessionId, sizeof(msg.m_SourceSessionId) << 3)
			&& bb.SerUns(msg.m_TransactionId, sizeof(msg.m_TransactionId) << 3)
			&& bb.SerUser(msg.m_SourcePeerAddr)
			&& bb.SerUser(msg.m_DestPeerAddr)
			&& bb.SerUns(msg.m_RoleTieBreaker, sizeof(msg.m_RoleTieBreaker) << 3)
			&& bb.SerBool(msg.m_Bilateral)
			&& bb.SerBool(msg.m_SendDebugInfo)
			&& bb.SerBool(msg.m_NonDeterministic)
			&& bb.SerUser(msg.m_SymmetricInference)
			&& bb.SerUser(msg.m_SymmetricSocket)
			&& bb.SerUns(msg.m_GameSessionToken, sizeof(msg.m_GameSessionToken) << 3)
			&& bb.SerUns(msg.m_GameSessionId, sizeof(msg.m_GameSessionId) << 3)
			&& bb.SerArraySize(msg.m_NumCandidates, msg.m_Candidates ASSERT_ONLY(, "m_NumCandidates")))
		{
			for(unsigned i = 0; (i < msg.m_NumCandidates) && (i < COUNTOF(msg.m_Candidates)); ++i)
			{
				bb.SerUser(msg.m_Candidates[i]);
			}

			success = true;
		}

		return success;
	}

//private:
	u8 m_Version;
	unsigned m_SourceSessionId;
	unsigned m_TransactionId;
	netPeerAddress m_SourcePeerAddr;
	netPeerAddress m_DestPeerAddr;
	u8 m_NumCandidates;
	netSocketAddress m_Candidates[MAX_CANDIDATES_PER_OFFER];
	u64 m_RoleTieBreaker;
	bool m_Bilateral;
	bool m_SendDebugInfo;
	bool m_NonDeterministic;
	SymmetricInference m_SymmetricInference;
	SymmetricSocket m_SymmetricSocket;
	u64 m_GameSessionToken;
	u64 m_GameSessionId;
};

NET_MESSAGE_IMPL(netIceSessionOffer);

//////////////////////////////////////////////////////////////////////////
//  netIceSessionAnswer
//////////////////////////////////////////////////////////////////////////
class netIceSessionAnswer
{
	friend class netIceTunneler;

public:

	static const unsigned CURRENT_VERSION = 2;
	static const unsigned MAX_CANDIDATES_PER_ANSWER = 6;

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													 sizeof(u8) +									// m_Version
													 sizeof(unsigned) +								// m_SourceSessionId
													 sizeof(unsigned) +								// m_DestSessionId
													 sizeof(unsigned) +								// m_TransactionId
													 netPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES +	// m_SourcePeerAddr
													 sizeof(u8) +									// m_NumCandidates
													 (MAX_CANDIDATES_PER_ANSWER *					// m_Candidates
													 netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES) + 
													 1 +											// m_SenderHasControllingRole
													 1 +											// m_SendDebugInfo
													 1;												// m_NonDeterministic

	NET_MESSAGE_DECL(netIceSessionAnswer, NET_ICE_SESSION_ANSWER);

	netIceSessionAnswer()
	{

	}

	netIceSessionAnswer(const unsigned sourceSessionId,
						const unsigned destSessionId,
						const unsigned transactionId,
						const netPeerAddress& peerAddr,
						const netSocketAddress* candidates,
						const unsigned numCandidates,
						const bool senderHasControllingRole,
						const bool sendDebugInfo,
						const bool nonDeterministic)
	{
		netAssert(sourceSessionId != NET_INVALID_ICE_SESSION_ID);
		netAssert(destSessionId != NET_INVALID_ICE_SESSION_ID);

		m_Version = CURRENT_VERSION;
		m_SourceSessionId = sourceSessionId;
		m_DestSessionId = destSessionId;
		m_TransactionId = transactionId;
		m_SourcePeerAddr = peerAddr;
		m_SourcePeerAddr.ClearKey();
		m_SenderHasControllingRole = senderHasControllingRole;
		m_SendDebugInfo = sendDebugInfo;
		m_NonDeterministic = nonDeterministic;

#if __FINAL && !__FINAL_LOGGING
		m_SendDebugInfo = false;
#endif

		if(numCandidates >= COUNTOF(m_Candidates))
		{
			m_NumCandidates = COUNTOF(m_Candidates);
		}
		else
		{
			m_NumCandidates = (u8)numCandidates;
		}

		for(unsigned i = 0; i < m_NumCandidates; ++i)
		{
			m_Candidates[i] = candidates[i];
		}		
	}

	NET_MESSAGE_SER(bb, msg)
	{
		bool success = false;

		if(bb.SerUns(msg.m_Version, sizeof(msg.m_Version) << 3)
			&& (msg.m_Version == CURRENT_VERSION)
			&& bb.SerUns(msg.m_SourceSessionId, sizeof(msg.m_SourceSessionId) << 3)
			&& bb.SerUns(msg.m_DestSessionId, sizeof(msg.m_DestSessionId) << 3)
			&& bb.SerUns(msg.m_TransactionId, sizeof(msg.m_TransactionId) << 3)
			&& bb.SerUser(msg.m_SourcePeerAddr)
			&& bb.SerBool(msg.m_SenderHasControllingRole)
			&& bb.SerBool(msg.m_SendDebugInfo)
			&& bb.SerBool(msg.m_NonDeterministic)
			&& bb.SerArraySize(msg.m_NumCandidates, msg.m_Candidates ASSERT_ONLY(, "m_NumCandidates")))
		{
			for(unsigned i = 0; (i < msg.m_NumCandidates) && (i < COUNTOF(msg.m_Candidates)); ++i)
			{
				bb.SerUser(msg.m_Candidates[i]);
			}

			success = true;
		}

		return success;
	}

//private:
	u8 m_Version;
	unsigned m_SourceSessionId;
	unsigned m_DestSessionId;
	unsigned m_TransactionId;
	netPeerAddress m_SourcePeerAddr;
	u8 m_NumCandidates;
	netSocketAddress m_Candidates[MAX_CANDIDATES_PER_ANSWER];
	bool m_SenderHasControllingRole;
	bool m_SendDebugInfo;
	bool m_NonDeterministic;
};

NET_MESSAGE_IMPL(netIceSessionAnswer);

//////////////////////////////////////////////////////////////////////////
//  netIceSessionPing
/*
	Check, Connectivity Check, STUN Check:  A STUN Binding request
	transaction for the purposes of verifying connectivity.  A check
	is sent from the local candidate to the remote candidate of a
	candidate pair.
*/
//////////////////////////////////////////////////////////////////////////
class netIceSessionPing
{
	friend class netIceTunneler;

public:

	NET_MESSAGE_DECL(netIceSessionPing, NET_ICE_SESSION_PING);

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													 netPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES +	// m_SourcePeerAddr
													 netPeerAddress::MAX_EXPORTED_SIZE_IN_BYTES +	// m_DestPeerAddr
													 sizeof(unsigned) +								// m_SourceSessionId
													 sizeof(unsigned) +								// m_DestSessionId
													 sizeof(unsigned) +								// m_TransactionId
													 1 +											// m_ViaSymmetricSocket
													 1;												// m_UseCandidate

	netIceSessionPing()
	{

	}

	netIceSessionPing(const netPeerAddress& sourcePeerAddr,
					  const netPeerAddress& destPeerAddr,
					  const unsigned sourceSessionId,
					  const unsigned destSessionId,
					  const unsigned transactionId,
					  const bool viaSymmetricSocket,
					  const bool useCandidate)
	{
		netAssert(sourceSessionId != NET_INVALID_ICE_SESSION_ID);
		netAssert(destSessionId != NET_INVALID_ICE_SESSION_ID);
		m_SourcePeerAddr = sourcePeerAddr;
		m_DestPeerAddr = destPeerAddr;
		m_DestPeerAddr.ClearKey();
		m_SourceSessionId = sourceSessionId;
		m_DestSessionId = destSessionId;
		m_TransactionId = transactionId;
		m_ViaSymmetricSocket = viaSymmetricSocket;
		m_UseCandidate = useCandidate;
	}

	NET_MESSAGE_SER(bb, msg)
	{
        bool success = false;

        if(bb.SerUser(msg.m_SourcePeerAddr)
			&& bb.SerUser(msg.m_DestPeerAddr)
			&& bb.SerUns(msg.m_SourceSessionId, sizeof(msg.m_SourceSessionId) << 3)
			&& bb.SerUns(msg.m_DestSessionId, sizeof(msg.m_DestSessionId) << 3)
			&& bb.SerUns(msg.m_TransactionId, sizeof(msg.m_TransactionId) << 3)
			&& bb.SerBool(msg.m_ViaSymmetricSocket)
			&& bb.SerBool(msg.m_UseCandidate))
        {
            success = true;
        }

        return success;
	}

//private:
	netPeerAddress m_SourcePeerAddr;
	netPeerAddress m_DestPeerAddr;
	unsigned m_SourceSessionId;
	unsigned m_DestSessionId;
	unsigned m_TransactionId;
	bool m_ViaSymmetricSocket;
	bool m_UseCandidate;
};

NET_MESSAGE_IMPL(netIceSessionPing);

//////////////////////////////////////////////////////////////////////////
//  netIceSessionPong
//////////////////////////////////////////////////////////////////////////
class netIceSessionPong
{
	friend class netIceTunneler;

public:
	NET_MESSAGE_DECL(netIceSessionPong, NET_ICE_SESSION_PONG);

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													 sizeof(unsigned) +								// m_DestSessionId
													 sizeof(unsigned) +								// m_TransactionId
													 netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES + // m_ObfuscatedMappedAddr
													 1 +											// m_ViaSymmetricSocket
													 1;												// m_Terminate

	netIceSessionPong()
	{

	}

	netIceSessionPong(const unsigned destSessionId,
					  const unsigned transactionId,
					  const netSocketAddress& mappedAddr,
					  const bool viaSymmetricSocket,
					  const bool terminate)
	{
		netAssert(destSessionId != NET_INVALID_ICE_SESSION_ID);

		m_DestSessionId = destSessionId;
		m_TransactionId = transactionId;
		m_ObfuscatedMappedAddr = netIceSessionPong::ObfuscateMappedAddress(mappedAddr);
		m_ViaSymmetricSocket = viaSymmetricSocket;
		m_Terminate = terminate;
	}

	static netSocketAddress ObfuscateMappedAddress(const netSocketAddress& mappedAddr)
	{
		/*
		From RFC 5389 - Session Traversal Utilities for NAT (STUN)
		Deployment experience found that some NATs
		rewrite the 32-bit binary payloads containing the NAT's public IP
		address, such as STUN's MAPPED-ADDRESS attribute, in the well-meaning
		but misguided attempt at providing a generic ALG function.
		*/

		// Note: We support IPv6, and obfuscation of IPv4 addresses is
		// no longer necessary, since we encrypt packets.

		return mappedAddr;
	}

	static netSocketAddress DeObfuscateMappedAddress(const netSocketAddress& obfuscatedMappedAddr)
	{
		return ObfuscateMappedAddress(obfuscatedMappedAddr);
	}

	NET_MESSAGE_SER(bb, msg)
	{
        bool success = false;

		if(bb.SerUns(msg.m_DestSessionId, sizeof(msg.m_DestSessionId) << 3)
			&& bb.SerUns(msg.m_TransactionId, sizeof(msg.m_TransactionId) << 3)
			&& bb.SerUser(msg.m_ObfuscatedMappedAddr)
			&& bb.SerBool(msg.m_ViaSymmetricSocket)
			&& bb.SerBool(msg.m_Terminate))
        {
            success = true;
        }

        return success;
	}

//private:
	unsigned m_DestSessionId;
	unsigned m_TransactionId;
	netSocketAddress m_ObfuscatedMappedAddr;
	bool m_ViaSymmetricSocket;
	bool m_Terminate;
};

NET_MESSAGE_IMPL(netIceSessionPong);

//////////////////////////////////////////////////////////////////////////
//  netIceSessionRouteCheck
//////////////////////////////////////////////////////////////////////////
class netIceSessionRelayRouteCheck
{
	friend class netIceTunneler;

public:

	enum Flags
	{
		RELAY_ROUTE_CHECK_FLAGS_NONE = 0,
		RELAY_ROUTE_CHECK_FLAGS_REQUEST = BIT0,
		RELAY_ROUTE_CHECK_FLAGS_RESPONSE = BIT1,
		RELAY_ROUTE_CHECK_NUM_FLAGS = 2,
	};

	CompileTimeAssert(RELAY_ROUTE_CHECK_NUM_FLAGS < (sizeof(u8) << 3));

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													   sizeof(unsigned) +								// m_SourceSessionId
													   sizeof(unsigned) +								// m_DestSessionId
													   sizeof(u8);										// m_Flags

	NET_MESSAGE_DECL(netIceSessionRelayRouteCheck, NET_ICE_SESSION_RELAY_ROUTE_CHECK);

	netIceSessionRelayRouteCheck()
		: m_Flags(RELAY_ROUTE_CHECK_FLAGS_NONE)
		, m_SourceSessionId(NET_INVALID_ICE_SESSION_ID)
		, m_DestSessionId(NET_INVALID_ICE_SESSION_ID)
	{

	}

	netIceSessionRelayRouteCheck(const unsigned sourceSessionId,
								 const unsigned destSessionId,
								 const u8 flags)
		: m_SourceSessionId(sourceSessionId)
		, m_DestSessionId(destSessionId)
		, m_Flags(flags)
	{
	}

	bool IsRequest() const
	{
		return (m_Flags & RELAY_ROUTE_CHECK_FLAGS_REQUEST) != 0;
	}

	bool IsResponse() const
	{
		return (m_Flags & RELAY_ROUTE_CHECK_FLAGS_RESPONSE) != 0;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerUns(msg.m_SourceSessionId, sizeof(msg.m_SourceSessionId) << 3)
			&& bb.SerUns(msg.m_DestSessionId, sizeof(msg.m_DestSessionId) << 3)
			&& bb.SerUns(msg.m_Flags, sizeof(msg.m_Flags) << 3);
	}

	unsigned m_SourceSessionId;
	unsigned m_DestSessionId;
	u8 m_Flags;
};

NET_MESSAGE_IMPL(netIceSessionRelayRouteCheck);

//////////////////////////////////////////////////////////////////////////
//  netIceSessionPortOpener
//////////////////////////////////////////////////////////////////////////
class netIceSessionPortOpener
{
	friend class netIceTunneler;

public:

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER;

	NET_MESSAGE_DECL(netIceSessionPortOpener, NET_ICE_SESSION_PORT_OPENER);

	netIceSessionPortOpener()
	{

	}

	NET_MESSAGE_SER(/*bb*/, /*msg*/)
	{
		return true;
	}

//private:
};

NET_MESSAGE_IMPL(netIceSessionPortOpener);

//////////////////////////////////////////////////////////////////////////
//  netIceSessionDebugInfo
//////////////////////////////////////////////////////////////////////////
class netIceSessionDebugInfo
{
	friend class netIceTunneler;

public:

	static const unsigned MAX_CANDIDATES = 10;
	static const unsigned MAX_METRIC_BUF_SIZE = 512;

	static const unsigned MAX_EXPORT_SIZE_IN_BYTES = netMessage::MAX_BYTE_SIZEOF_HEADER +
													 sizeof(unsigned) +													// m_SourceSessionId
													 sizeof(unsigned) +													// m_DestSessionId
													 netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES +						// m_PrivateAddr
													 netAddress::MAX_EXPORTED_SIZE_IN_BYTES +							// m_RelayAddr
													 datMaxBytesNeededForString<MAX_METRIC_BUF_SIZE>::COUNT +			// m_Metric
													 sizeof(u8) +														// m_NumCandidates
													 (MAX_CANDIDATES *													// m_Candidates
													 netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES) + 
													 (MAX_CANDIDATES * sizeof(u8)) +									// m_CandidateHits
													 (MAX_CANDIDATES * sizeof(u32)) +									// m_CandidateTimestamps
													 netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES +						// m_CrossRelayAddr
													 sizeof(u16) +														// m_NumFailedSessions
													 sizeof(u16) +														// m_NumSuccessfulSessions
													 sizeof(unsigned) +													// m_Regioncode
													 datMaxBytesNeededForString<RLSC_MAX_COUNTRY_CODE_CHARS>::COUNT +	// m_CountryCode
													 netNatInfo::MAX_EXPORTED_SIZE_IN_BYTES + 							// m_NatInfo
													 netNatUpNpInfo::MAX_EXPORTED_SIZE_IN_BYTES + 						// m_NatUpNpInfo
													 netNatPcpInfo::MAX_EXPORTED_SIZE_IN_BYTES;							// m_NatPcpInfo

// TODO: NS - the exported size is too large when IPv6 addresses are used. Will need to reduce the size of this message further.
// This message won't get sent to/from the companion app, so for now I'm ignoring this compile time assert on mobile.
#if !RSG_MOBILE && 0
	CompileTimeAssert(MAX_EXPORT_SIZE_IN_BYTES < netRelayPacket::MAX_SIZEOF_P2P_PAYLOAD);
#endif

	NET_MESSAGE_DECL(netIceSessionDebugInfo, NET_ICE_SESSION_DEBUG_INFO);

	netIceSessionDebugInfo()
	{

	}

	netIceSessionDebugInfo(const unsigned sourceSessionId,
						const unsigned destSessionId,
						const netPeerAddress& peerAddr,
						const char* metrics,
						const netSocketAddress* candidates,
						const unsigned* candidateHits,
						const u64* candidateTimestamps,
						const unsigned numCandidates,
						const netSocketAddress& crossRelayAddr,
						const unsigned numFailedSessions,
						const unsigned numSuccessfulSessions,
						const unsigned regionCode,
						const char* countryCode,
						const netNatInfo& natInfo, 
						const netNatUpNpInfo& natUpNpInfo,
						const netNatPcpInfo& natPcpInfo,
						const unsigned numSymmetricSocketNominations)
	{
		netAssert(sourceSessionId != NET_INVALID_ICE_SESSION_ID);

		m_SourceSessionId = sourceSessionId;
		m_DestSessionId = destSessionId;
		m_PrivateAddr = peerAddr.GetPrivateAddress();
		m_RelayAddr = peerAddr.GetRelayAddress();
		m_CrossRelayAddr = crossRelayAddr;
		safecpy(m_Metrics, metrics);
		m_NumFailedSessions = (u16)numFailedSessions;
		m_NumSuccessfulSessions = (u16)numSuccessfulSessions;
		m_RegionCode = regionCode;
		safecpy(m_CountryCode, countryCode);
		m_NatInfo = natInfo;
		m_NatUpNpInfo = natUpNpInfo;
		m_NatPcpInfo = natPcpInfo;
		m_NumSymmetricSocketNominations = numSymmetricSocketNominations;
		m_NumCandidates = (u8)Min(numCandidates, (unsigned)COUNTOF(m_Candidates));

		for(unsigned i = 0; (i < m_NumCandidates) && (i < COUNTOF(m_Candidates)); ++i)
		{
			m_Candidates[i] = candidates[i];
			m_CandidateHits[i] = candidateHits[i];
			m_CandidateTimestamps[i] = candidateTimestamps[i];
		}		
	}

	NET_MESSAGE_SER(bb, msg)
	{
		bool success = false;

		if(bb.SerUns(msg.m_SourceSessionId, sizeof(msg.m_SourceSessionId) << 3)
			&& bb.SerUns(msg.m_DestSessionId, sizeof(msg.m_DestSessionId) << 3)
			&& bb.SerUser(msg.m_PrivateAddr)
			&& bb.SerUser(msg.m_RelayAddr)
			&& bb.SerStr(msg.m_Metrics, sizeof(msg.m_Metrics))
			&& bb.SerUser(msg.m_CrossRelayAddr)
			&& bb.SerUns(msg.m_NumFailedSessions, sizeof(msg.m_NumFailedSessions) << 3)
			&& bb.SerUns(msg.m_NumSuccessfulSessions, sizeof(msg.m_NumSuccessfulSessions) << 3)
			&& bb.SerUns(msg.m_RegionCode, sizeof(msg.m_RegionCode) << 3)
			&& bb.SerStr(msg.m_CountryCode, RLSC_MAX_COUNTRY_CODE_CHARS)
			&& bb.SerUser(msg.m_NatInfo)
			&& bb.SerUser(msg.m_NatUpNpInfo)
			&& bb.SerUser(msg.m_NatPcpInfo)
			&& bb.SerUns(msg.m_NumSymmetricSocketNominations, sizeof(msg.m_NumSymmetricSocketNominations) << 3)
			&& bb.SerArraySize(msg.m_NumCandidates, msg.m_Candidates ASSERT_ONLY(, "m_NumCandidates")))
		{
			success = true;

			for(unsigned i = 0; i < msg.m_NumCandidates; ++i)
			{
				success = success && bb.SerUser(msg.m_Candidates[i])
				 && bb.SerUns(msg.m_CandidateHits[i], sizeof(u8) << 3)
				 && bb.SerUns(msg.m_CandidateTimestamps[i], sizeof(u32) << 3);
			}
		}

		return success;
	}

#if !__NO_OUTPUT
	void DumpInfo() const
	{
		netDebug3("\tDebug info received for ICE session %d. Remote sessionId %d.", m_DestSessionId, m_SourceSessionId);

		netDebug3("\tm_RemotePeerAddress:" NET_ADDR_FMT "/" NET_ADDR_FMT,
				NET_ADDR_FOR_PRINTF(m_PrivateAddr),
				NET_ADDR_FOR_PRINTF(m_RelayAddr.GetTargetAddress()));

		netDebug3("\tRemote Relay:" NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_RelayAddr.GetProxyAddress()));

		netDebug3("\tRemote Telemetry: %s", m_Metrics);

		netDebug3("\tCross Relay Mapped Address: " NET_ADDR_FMT "%s", NET_ADDR_FOR_PRINTF(m_CrossRelayAddr), (m_CrossRelayAddr.IsValid() && (m_CrossRelayAddr != m_RelayAddr.GetTargetAddress())) ? " (non-deterministic)" : "");

		netDebug3("\tNum Symmetric Socket Nominations: %u", m_NumSymmetricSocketNominations);

		unsigned peerReflexiveListSize = 0;
		bool reusedPorts = false;
		for(unsigned i = 0; i < COUNTOF(m_Candidates); ++i)
		{
			if(m_Candidates[i].IsValid())
			{
				peerReflexiveListSize++;
				if(m_CandidateHits[i] > 1)
				{
					reusedPorts = true;
				}
			}
		}

		if(peerReflexiveListSize == 0)
		{
			netDebug3("\tPeer Reflexive List (empty)");
		}
		else
		{
			netDebug3("\tPeer Reflexive List (num valid entries: %u, re-used ports: %s):", peerReflexiveListSize, reusedPorts ? "true" : "false");
		}

		for(unsigned i = 0; i < COUNTOF(m_Candidates); ++i)
		{
			netDebug3("\t\t%u: " NET_ADDR_FMT ", NumHits: %u, Timestamp: %" I64FMT "u", i, NET_ADDR_FOR_PRINTF(m_Candidates[i]), m_CandidateHits[i], m_CandidateTimestamps[i]);
		}

		m_NatInfo.DumpInfo();
		m_NatPcpInfo.DumpInfo();
		m_NatUpNpInfo.DumpInfo();

		unsigned totalSessions = m_NumFailedSessions + m_NumSuccessfulSessions;
		float failRate = ((float)m_NumFailedSessions / (float)totalSessions) * 100.0f;

		netDebug3("\tNum Failed Sessions: %u, Num Successful Sessions: %u, Total Sessions: %u, Remote Fail Rate: %.2f", m_NumFailedSessions, m_NumSuccessfulSessions, totalSessions, failRate);
		netDebug3("\tRegion Code: %u, Country Code: %s", m_RegionCode, m_CountryCode);

		s_TotalRemoteFails += m_NumFailedSessions;
		s_TotalRemoteSuccess += m_NumSuccessfulSessions;
		unsigned totalRemoteSessions = s_TotalRemoteFails + s_TotalRemoteSuccess;
		float overallFailRate = ((float)s_TotalRemoteFails / (float)totalRemoteSessions) * 100.0f;
		netDebug3("\tOverall: Num Failed Sessions: %u, Num Successful Sessions: %u, Total Sessions: %u, Overall Fail Rate: %.2f", s_TotalRemoteFails, s_TotalRemoteSuccess, totalRemoteSessions, overallFailRate);
	}
#endif

//private:
	unsigned m_SourceSessionId;
	unsigned m_DestSessionId;
	netSocketAddress m_PrivateAddr;
	netAddress m_RelayAddr;
	char m_Metrics[MAX_METRIC_BUF_SIZE];
	u8 m_NumCandidates;
	netSocketAddress m_Candidates[MAX_CANDIDATES];
	unsigned m_CandidateHits[MAX_CANDIDATES];
	u64 m_CandidateTimestamps[MAX_CANDIDATES];
	netSocketAddress m_CrossRelayAddr;
	u16 m_NumFailedSessions;
	u16 m_NumSuccessfulSessions;
	unsigned m_RegionCode;
	char m_CountryCode[RLSC_MAX_COUNTRY_CODE_CHARS];
	netNatInfo m_NatInfo;
	netNatPcpInfo m_NatPcpInfo;
	netNatUpNpInfo m_NatUpNpInfo;
	unsigned m_NumSymmetricSocketNominations;
};

NET_MESSAGE_IMPL(netIceSessionDebugInfo);

//////////////////////////////////////////////////////////////////////////
//  netIceTransaction
//////////////////////////////////////////////////////////////////////////
class netIceTransaction
{
public:
	netIceTransaction()
#if !__NO_OUTPUT
	: m_Logger(NULL)
#endif
	{
		this->Clear();
	}

	netIceTransaction(netIceLogger* OUTPUT_ONLY(logger),
					  netConnectionManager* cxnMgr,
					  const unsigned transactionId,
					  const netP2pCrypt::Key& remotePeerKey,
					  const netAddress& destAddr,
					  unsigned maxRetries,
					  unsigned retryDelay,
					  const void* data,
					  const unsigned sizeofData,
					  const void* userData)
	{
		rtry
		{
			this->Clear();

			rverify(cxnMgr != NULL, catchall, );
			rverify(transactionId != NET_INVALID_ICE_TRANSACTION_ID, catchall, );
			rverify(destAddr.IsValid(), catchall, );
			rverify(maxRetries < 50, catchall, );
			rverify(retryDelay >= 20, catchall, );
			rverify(data != NULL, catchall, );
			rverify(sizeofData > 0, catchall, );
			rverify(sizeofData < sizeof(m_MsgBuf), catchall, );

#if !__NO_OUTPUT
			m_Logger = logger;
#endif
			m_CxnMgr = cxnMgr;
			m_TransactionId = transactionId;
			m_RemotePeerKey = remotePeerKey;
			m_DestAddr = destAddr;
			m_MaxRetries = maxRetries;
			m_RetryDelayMs = retryDelay;
			sysMemCpy(m_MsgBuf, data, sizeofData);
			m_SizeOfData = sizeofData;
			m_UserData = userData;
		}
		rcatchall
		{
			this->Clear();
		}
	}

	bool IsValid() const
	{
		return (m_CxnMgr != NULL) && (m_TransactionId != NET_INVALID_ICE_TRANSACTION_ID);
	}

	void Clear()
	{
		m_CxnMgr = NULL;
		m_RemotePeerKey.Clear();
		m_DestAddr.Clear();
		m_TransactionId = NET_INVALID_ICE_TRANSACTION_ID;
		m_MaxRetries = 0;
		m_RetryDelayMs = 0;
		memset(m_MsgBuf, 0, sizeof(m_MsgBuf));
		m_SizeOfData = 0;
		m_UserData = NULL;

		m_NumRetries = 0;
		m_ResendTimer = netRetryTimer();
		m_ExpirationTimer = netTimeout();
		m_StopWatchSinceStart = netStopWatch();
		m_IsCancelled = false;
#if !__NO_OUTPUT
		m_Logger = NULL;
#endif
	}

	~netIceTransaction()
	{
	}

	const netAddress& GetDestAddr() const
	{
		return m_DestAddr;
	}

	unsigned GetTransactionId() const
	{
		return m_TransactionId;
	}

	unsigned GetMaxRetries() const
	{
		return m_MaxRetries;
	}

	const u8* GetMessageData() const
	{
		return m_MsgBuf;
	}

	unsigned GetSizeOfData() const
	{
		return m_SizeOfData;
	}

	const void* GetUserData() const
	{
		return m_UserData;
	}

	unsigned GetNumRetries() const
	{
		return m_NumRetries;
	}

	bool Start()
	{
		m_StopWatchSinceStart.Restart();
		return Send();
	}

	bool Send()
	{
		rtry
		{	
			netIceDebug1("Sending %s to:" NET_ADDR_FMT ". TransactionId:%u (attempt %u of %u)",
					  netMessage::GetName(this->GetMessageData(), this->GetSizeOfData()),
					  NET_ADDR_FOR_PRINTF(this->GetDestAddr()),
					  m_TransactionId,
					  m_NumRetries + 1,
					  m_MaxRetries + 1);

			m_ResendTimer.InitMilliseconds(m_RetryDelayMs);

			if(m_ExpirationTimer.IsRunning() == false)
			{
				m_ExpirationTimer.InitMilliseconds(Max(m_RetryDelayMs * (m_MaxRetries + 1), MIN_TRANSACTION_TTL_MS));
			}

			netP2pCryptContext context;
			context.Init(m_RemotePeerKey, NET_P2P_CRYPT_TUNNELING_PACKET);

			rcheck(m_CxnMgr->IsInitialized(), catchall, netWarning("m_CxnMgr->IsInitialized() is false"));
			rverify(m_CxnMgr->SendTunnelingPacket(this->GetDestAddr(),
												  this->GetMessageData(),
												  this->GetSizeOfData(),
												  context), catchall, netWarning("Sending message failed"));
			
			return true;
		}
		rcatchall
		{
		}

		return false;
	}

	/*
		7.2.1.4.
		Cancellation means that the agent
		will not retransmit the request, will not treat the lack of
		response to be a failure, but will wait the duration of the
		transaction timeout for a response
	*/
	void Cancel()
	{
		m_IsCancelled = true;
	}

	bool IsCancelled() const
	{
		return m_IsCancelled;
	}

	/*
		A timed out transaction is one that has sent all of its packets including retries.
		However, the transaction still remains active in the timed out state so we can
		process responses that may arrive late.
	*/
	bool IsTimedOut() const
	{
		// if we've sent our last retry and it would be time to retry again, we've timed out
		return (m_NumRetries >= m_MaxRetries) && m_ResendTimer.IsTimeToRetry();
	}

	/*
		An expired transaction is one that has sent all of its packets and waited its
		maximum duration for a response. Cancelled transactions can expire sooner.
	*/
	bool IsExpired() const
	{
		if(m_IsCancelled)
		{
			// a cancelled transaction won't send any more retries, so IsTimedOut() won't return true.
			return IsTimedOut() || m_ExpirationTimer.IsTimedOut() || (m_ExpirationTimer.IsRunning() == false);
		}

		// if we're timed out and not canceled then we're expired
		// (but let it hang around waiting for a response for a minimum duration in case the remote peer has stalled)
		return IsTimedOut() && (m_ExpirationTimer.IsTimedOut() || (m_ExpirationTimer.IsRunning() == false));
	}

	void Update()
	{
		m_ResendTimer.Update();
		m_ExpirationTimer.Update();

		if(m_IsCancelled == false)
		{
			if((m_NumRetries < m_MaxRetries) && m_ResendTimer.IsTimeToRetry())
			{
				++m_NumRetries;
				Send();
			}
		}
	}

	unsigned GetElapsedTimeSinceStart() const
	{
		return m_StopWatchSinceStart.GetElapsedMilliseconds();
	}

private:
	netConnectionManager* m_CxnMgr;
	netP2pCrypt::Key m_RemotePeerKey;
	netAddress m_DestAddr;
	unsigned m_TransactionId;
	unsigned m_MaxRetries;
	unsigned m_RetryDelayMs;
	netStopWatch m_StopWatchSinceStart;

	// could use netFrame::MAX_BYTE_SIZEOF_PAYLOAD here but since we 
	// know our max message sizes, this saves a lot of memory
	u8 m_MsgBuf[(netIceSessionPing::MAX_EXPORT_SIZE_IN_BYTES > netIceSessionOffer::MAX_EXPORT_SIZE_IN_BYTES) ?
				 netIceSessionPing::MAX_EXPORT_SIZE_IN_BYTES : 
				 netIceSessionOffer::MAX_EXPORT_SIZE_IN_BYTES];

	unsigned m_SizeOfData;
	const void* m_UserData;

	unsigned m_NumRetries;
	netTimeout m_ExpirationTimer;
	netRetryTimer m_ResendTimer;
#if !__NO_OUTPUT
	netIceLogger* m_Logger;
#endif
	bool m_IsCancelled : 1;
};

template<unsigned MAX_TRANSACTIONS>
class netIceTransactions
{
public:

	netIceTransactions(netIceLogger* OUTPUT_ONLY(logger))
#if !__NO_OUTPUT
	: m_Logger(logger)
#endif
	{
		Clear();
	}

	void Clear()
	{
		m_Transactions.Reset();
	}

	netIceTransaction* AddTransaction(const netIceTransaction& transaction)
	{
		if(transaction.IsValid() && netVerify(m_Transactions.IsFull() == false))
		{
			m_Transactions.Push(transaction);
			return &m_Transactions.Top();
		}
		return NULL;
	}

	unsigned GetCount() const
	{
		return m_Transactions.GetCount();
	}

	const netIceTransaction& operator[](unsigned index) const
	{
		return m_Transactions[index];
	}

	const netIceTransaction* FindByTransactionId(unsigned id) const
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetTransactionId() == id)
			{
				return &m_Transactions[i];
			}
		}

		return NULL;
	}

	void RemoveTransaction(unsigned transactionId)
	{
		// fixed array doesn't like us removing elements while we're iterating

		bool deleted = false;
		do 
		{
			deleted = false;
			for(unsigned i = 0; i < GetCount(); ++i)
			{
				if(m_Transactions[i].GetTransactionId() == transactionId)
				{
					deleted = true;
					m_Transactions.Delete(i);
					break;
				}
			}
		} while (deleted);
	}

	void RemoveTransactionsByUserData(void* userData)
	{
		// fixed array doesn't like us removing elements while we're iterating

		bool deleted = false;
		do 
		{
			deleted = false;
			for(unsigned i = 0; i < GetCount(); ++i)
			{
				if(m_Transactions[i].GetUserData() == userData)
				{
					deleted = true;
					m_Transactions.Delete(i);
					break;
				}
			}
		} while (deleted);
	}

	void RemoveTransactionsByAddress(const netSocketAddress& addr)
	{
		// fixed array doesn't like us removing elements while we're iterating

		bool deleted = false;
		do 
		{
			deleted = false;
			for(unsigned i = 0; i < GetCount(); ++i)
			{
				if(m_Transactions[i].GetDestAddr().IsDirectAddr() &&
					m_Transactions[i].GetDestAddr().GetTargetAddress() == addr)
				{
					deleted = true;
					m_Transactions.Delete(i);
					break;
				}
			}
		} while (deleted);
	}

	void CancelTransactionsByUserData(void* userData)
	{
		// Note: there can be more than one active transaction with the same user data

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				m_Transactions[i].Cancel();
			}
		}
	}

	void CancelAll()
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			m_Transactions[i].Cancel();
		}
	}

	bool DoesTransactionExist(void* userData)
	{
		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				return true;
			}
		}

		return false;
	}

	/*
		returns true if at least one transaction exists with this user data and
		all transactions with this user data are timed out (does not include
		transactions that were canceled and have expired)
	*/
	bool AreAllTransactionsTimedOut(void* userData)
	{
		// Note: there can be more than one active transaction with the same user data

		bool timedOut = true;
		bool exists = false;

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				exists = true;

				if(m_Transactions[i].IsTimedOut() == false)
				{
					timedOut = false;
					break;
				}
			}
		}

		return exists && timedOut;
	}

	/*
		returns true if at least one transaction exists with this user data and
		all transactions with this user data are expired.
	*/
	bool AreAllTransactionsExpired(void* userData)
	{
		// Note: there can be more than one active transaction with the same user data

		bool expired = true;
		bool exists = false;

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				exists = true;

				if(m_Transactions[i].IsExpired() == false)
				{
					expired = false;
					break;
				}
			}
		}

		return exists && expired;
	}

	/*
		returns true if at least one transaction exists with this user data and
		all transactions with this user data are expired but not cancelled.
	*/
	bool AreAllTransactionsExpiredButNotCancelled(void* userData)
	{
		// Note: there can be more than one active transaction with the same user data

		bool expiredButNotCancelled = true;
		bool exists = false;

		for(unsigned i = 0; i < GetCount(); ++i)
		{
			if(m_Transactions[i].GetUserData() == userData)
			{
				exists = true;

				/*
					Expired		Cancelled	ExpiredButNotCancelled
						  0				0						 0
						  0				1						 0
						  1				0						 1
						  1				1						 0
				*/

				if((m_Transactions[i].IsExpired() == false) || (m_Transactions[i].IsCancelled()))
				{
					expiredButNotCancelled = false;
					break;
				}
			}
		}

		return exists && expiredButNotCancelled;
	}

	void RemoveExpiredTransactions()
	{
		// fixed array doesn't like us removing elements while we're iterating

		bool deleted = false;
		do 
		{
			deleted = false;
			for(unsigned i = 0; i < GetCount(); ++i)
			{
				const netIceTransaction& t = m_Transactions[i];
				if(t.IsExpired())
				{
					netIceDebug("Transaction %u expired after %u ms. Cancelled: %s, TimedOut: %s",
								t.GetTransactionId(),
								t.GetElapsedTimeSinceStart(),
								t.IsCancelled() ? "true" : "false",
								t.IsTimedOut() ? "true" : "false");

					deleted = true;
					m_Transactions.Delete(i);
					break;
				}
			}
		} while (deleted);
	}

	void Update()
	{
		for(unsigned i = 0; i < (unsigned)m_Transactions.GetCount(); ++i)
		{
			m_Transactions[i].Update();
		}
	}

private:
	atFixedArray<netIceTransaction, MAX_TRANSACTIONS> m_Transactions;
	netIceLogger* m_Logger;
};

class netIceTransactor
{
public:
	netIceTransactor(netIceLogger* logger)
	: m_Transactions(logger)
#if !__NO_OUTPUT
	, m_Logger(logger)
#endif
	{
		this->Clear();
	}

	void Clear()
	{
		m_CxnMgr = NULL;
		m_Transactions.Clear();
	}

	bool Init(netConnectionManager* cxnMgr)
	{
		this->Clear();
		m_CxnMgr = cxnMgr;
		return netVerify(m_CxnMgr != NULL);
	}

	void Update()
	{
		m_Transactions.Update();
	}

    template<typename T>
    bool SendRequest(const unsigned transactionId,
					 const netP2pCrypt::Key& remotePeerKey,
					 const netAddress& addr,
					 const T& msg,
					 const unsigned maxRetries,
					 const unsigned retryDelay,
					 const void* userData)
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
                && this->SendRequest(transactionId, remotePeerKey, addr, buf, size, maxRetries, retryDelay, userData);
    }

	unsigned NextId()
	{
		unsigned id = sysInterlockedIncrement(&s_NextId);

		while(NET_INVALID_ICE_TRANSACTION_ID == id)
		{
			id = sysInterlockedIncrement(&s_NextId);
		}

		return id;
	}

	const netIceTransaction* FindTransactionById(unsigned transactionId)
	{
		return m_Transactions.FindByTransactionId(transactionId);
	}

	void RemoveTransaction(unsigned transactionId)
	{
		m_Transactions.RemoveTransaction(transactionId);
	}

	void RemoveTransactionsByUserData(void* userData)
	{
		m_Transactions.RemoveTransactionsByUserData(userData);
	}

	void RemoveTransactionsByAddress(const netSocketAddress& addr)
	{
		m_Transactions.RemoveTransactionsByAddress(addr);
	}

	void CancelTransactionsByUserData(void* userData)
	{
		m_Transactions.CancelTransactionsByUserData(userData);
	}

	void CancelAll()
	{
		m_Transactions.CancelAll();
	}

	bool AreAllTransactionsTimedOut(void* userData)
	{
		return m_Transactions.AreAllTransactionsTimedOut(userData);
	}

	bool AreAllTransactionsExpired(void* userData)
	{
		return m_Transactions.AreAllTransactionsExpired(userData);
	}

	bool AreAllTransactionsExpiredButNotCancelled(void* userData)
	{
		return m_Transactions.AreAllTransactionsExpiredButNotCancelled(userData);
	}
	
	bool DoesTransactionExist(void* userData)
	{
		return m_Transactions.DoesTransactionExist(userData);
	}
	
	void RemoveExpiredTransactions()
	{
		m_Transactions.RemoveExpiredTransactions();
	}

	unsigned GetCount() const
	{
		return m_Transactions.GetCount();
	}

private:
	static unsigned s_NextId;

    bool SendRequest(const unsigned transactionId,
					 const netP2pCrypt::Key& remotePeerKey,
					 const netAddress& addr,
					 const void* data,
					 const unsigned sizeofData,
					 const unsigned maxRetries,
					 const unsigned retryDelay,
					 const void* userData)
	{
		rtry
		{
			netIceTransaction* tx = m_Transactions.AddTransaction(netIceTransaction(m_Logger,
																					m_CxnMgr,
																					transactionId,
																					remotePeerKey,
																					addr,
																					maxRetries,
																					retryDelay,
																					data,
																					sizeofData,
																					userData));

			rverify(tx != NULL, catchall, );
			rverify(tx->Start(), catchall, );

			return true;
		}
		rcatchall
		{
		}

		return false;
	}

	netConnectionManager* m_CxnMgr;
	netIceTransactions<MAX_ACTIVE_TRANSACTIONS_PER_SESSION> m_Transactions;
	netIceLogger* m_Logger;
};

// We send our transaction id in messages so the remote peer can send it back 
// to us to correlate which transaction the response is for.
unsigned netIceTransactor::s_NextId = NET_INVALID_ICE_TRANSACTION_ID;

//////////////////////////////////////////////////////////////////////////
//  netIceSession
//////////////////////////////////////////////////////////////////////////

//PURPOSE
// Represents a NAT traversal / NAT negotiation between two peers.
// ICE sessions are managed by the ICE tunneler. Multiple sessions can be
// active at the same time, between different pairs of peers.
class netIceSession
{
public:
    netIceSession();
    ~netIceSession();

	bool Init(const unsigned tunnelRequestId,
			  const unsigned sessionId,
			  netConnectionManager* cxnMgr,
			  const netPeerAddress& peerAddr,
			  const u64 gameSessionToken,
			  const u64 gameSessionId);
	void DevInit();
	void Update();
	void Clear();
	bool Attach(netAddress* netAddr, netAddress* relayAddr, const unsigned tunnelRequestId, netStatus* status);
	bool StartAsOfferer(netAddress* netAddr, netAddress* relayAddr, const bool bilateral, netStatus* status);
	bool CanReceiveOffer(const netIceSessionOffer* msg, const netAddress& sender);
	bool ReceiveOffer(const netIceSessionOffer* msg, const netAddress& sender);
	void ReceiveAnswer(const netIceSessionAnswer* msg, const netAddress& sender);
	void ReceivePing(const netIceSessionPing* msg, const netAddress& sender);
	void ReceivePong(const netIceSessionPong* msg, const netAddress& sender);
	void ReceiveRelayRouteCheck(const netIceSessionRelayRouteCheck* msg, const netAddress& sender);
	void ReceiveDebugInfo(const netIceSessionDebugInfo* msg, const netAddress& sender);

    //PURPOSE
    //  Returns true if this session is pending.
    bool Pending() const;

	//PURPOSE
	//  Returns true if this session is in use.
	bool IsActive() const;

    //PURPOSE
    //  Returns true if this session is complete
    //  and it successfully obtained a valid netAddress for the remote
    //  peer.
    bool Succeeded() const;

	//PURPOSE
	//  Cancels the given status object if it is attached to the current session.
	void SetCanceled(netStatus* status);

	//PURPOSE
	//  Cancels all status objects that are attached to the current session.
	void SetCanceled();

    //PURPOSE
    //  Returns the session ID.
    unsigned GetSessionId() const;

	//PURPOSE
	//  Returns the session ID of the remote peer.
	unsigned GetDestSessionId() const;

	//PURPOSE
	//  Returns the peer address of the remote peer.
	const netPeerAddress& GetRemotePeerAddress() const
	{
		netAssert(m_RemotePeerAddr.IsValid());
		return m_RemotePeerAddr;
	}

	//PURPOSE
	//  Returns true if the given status object is attached to this session.
	bool IsStatusAttached(netStatus* status) const
	{
		if(status == NULL)
		{
			return false;
		}

		for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
		{
			if(m_Status[i] == status)
			{
				return true;
			}
		}

		return false;
	}

	inlist_node<netIceSession> m_ListLink;

private:

	// Support applications that open multiple tunnels to the same player at the same time.
	// Have the ICE session accept multiple status objects instead of starting new sessions
	// for each.
	const static unsigned MAX_ATTACHED_TUNNEL_REQUESTS = 10;

	enum AgentRole
	{
		ROLE_INVALID,

		/*
			Controlling Agent:  The ICE agent that is responsible for selecting
			the final choice of candidate pairs and signaling them through
			STUN and an updated offer, if needed.  In any session, one agent
			is always controlling.  The other is the controlled agent.
		*/
		ROLE_CONTROLLING,

		/*
			Controlled Agent:  An ICE agent that waits for the controlling agent
			to select the final choice of candidate pairs.
		*/
		ROLE_CONTROLLED,
	};

	enum CompletionType
	{
		COMPLETE_FAILED,
		COMPLETE_SUCCEEDED,
	};

	enum FailReason
	{
		// only add new reasons to the end, and don't re-order/renumber existing
		// reasons. These match values in our telemetry reporting system.
		FAIL_REASON_NONE = 0,
		FAIL_REASON_EMERGENCY_TIMER = 1,
		FAIL_REASON_COULD_NOT_SEND_INDIRECT_OFFER = 2,
		FAIL_REASON_INDIRECT_OFFER_TIMED_OUT = 3,
		FAIL_REASON_NO_VALID_PAIRS = 4,
		FAIL_REASON_CANCELLED = 5,
		FAIL_REASON_START_AS_OFFERER = 6,
		FAIL_REASON_RECEIVE_OFFER = 7,
		FAIL_REASON_RCV_OFFER_GLC = 8,
		FAIL_REASON_RCV_OFFER_GRC = 9,
		FAIL_REASON_RCV_OFFER_CHECKLIST = 10,
		FAIL_REASON_RCV_OFFER_SEND_ANSWER = 11,
		FAIL_REASON_START_AS_OFFERER_STATUS = 12,
		FAIL_REASON_START_AS_OFFERER_NET_ADDR = 13,
		FAIL_REASON_START_AS_OFFERER_GLC = 14,
		FAIL_REASON_START_AS_OFFERER_GRC = 15,
		FAIL_REASON_START_AS_OFFERER_CHECKLIST = 16,
		FAIL_REASON_COULD_NOT_SEND_DIRECT_OR_INDIRECT_OFFERS = 17,
		FAIL_REASON_SIMULATED_FAIL = 18,
		FAIL_REASON_REMOTE_TERMINATED = 19,
		FAIL_REASON_PORT_PRESERVING_SYMMETRIC = 20,
		FAIL_REASON_PORT_RANDOM_SYMMETRIC = 21,
		FAIL_REASON_QUICK_CONNECT = 22,
		FAIL_REASON_NON_DETERMINISTIC = 23,
		FAIL_REASON_INDIRECT_OFFER_NO_LOCAL_RELAY_ADDR = 24,
		FAIL_REASON_INDIRECT_OFFER_NO_LOCAL_RELAY_MAPPED_ADDR = 25,
		FAIL_REASON_INDIRECT_OFFER_NO_REMOTE_RELAY = 26,
	};

	void InitRoleTieBreaker();
	int FindFreeNetStatusIndex() const;
	bool IsLocalStrict() const;
	bool IndirectOfferTimedOut();
	bool PassedOfferAnswerExchange() const;

	bool NeedsPresenceQuery() const;
	bool StartPresenceQuery();
	bool IsPresenceQueryInProgress() const;
	void UpdatePresenceQuery();
	void CancelPresenceQuery();

	bool NeedsSymmetricInference() const;
	bool StartSymmetricInference();
	bool IsSymmetricInferenceInProgress() const;
	void UpdateSymmetricInference();
	void CancelSymmetricInference();

	bool NeedsSymmetricSocket(const bool directOfferReceived) const;
	bool ChooseSymmetricSocketParams(u16& localPort);
	bool StartSymmetricSocket();
	bool IsSymmetricSocketInProgress() const;
	void UpdateSymmetricSocket();
	void CancelSymmetricSocket();

	void AddRemoteSymmetricSocketCandidates();
	void AddRemoteSymmetricInferenceCandidates();
	bool AddRemoteCandidateToChecklist(const Candidate::CandidateType type, const netSocketAddress &addr);
	bool AddRemoteCandidates(const netIceSessionOffer* msg, const netAddress& sender);
	bool AddRemoteCandidates(const netIceSessionAnswer* msg, const netAddress& sender);

	bool NeedLocalRelayRefresh() const;
	void RefreshLocalRelay();

	bool NeedsCrossRelayCandidateCheck() const;
	bool StartCrossRelayCandidateCheck();
	bool IsCrossRelayCandidateCheckInProgress() const;
	void UpdateCrossRelayCandidateCheck();
	void CancelCrossRelayCandidateCheck();

	void SendRelayRouteCheckRequest();
	void SendRelayRouteCheckResponse(const netIceSessionRelayRouteCheck* msg, const netAddress& sender);

	void UpdateUnmatchedPublicIpState(const netSocketAddress& senderAddr, const bool nominated);

	bool SendPortOpenerMessage(const netSocketAddress& addr) const;

	bool IsControlling() const;
	bool IsControlled() const;
	unsigned GenerateImplicitTieBreaker();
	bool SendInitialOffers();
	void ResolveRoleConflict(const netIceSessionOffer* msg, const netAddress& sender);
	bool CanSendViaRelay(FailReason& failReason) const;
	bool SendDirectOffers();
	bool SendIndirectOffer(FailReason& failReason);
	bool SendAnswer(unsigned sourceSessionId, unsigned destSessionId, unsigned transactionId, const netAddress& sender);
	void SendPing(CandidatePair* candidatePair, bool useCandidate);
	void SendPong(unsigned sessionId, unsigned transactionId, CandidatePair* candidatePair, const netSocketAddress& mappedAddr);

	bool GatherLocalCandidates();
	bool GatherRemoteCandidates();
	unsigned GetAdditionalLocalCandidates(netSocketAddress* addrs, unsigned maxAddrs);
	bool CreateCheckList();
	
	bool SendDebugInfo(const char* metrics);
	
	void Detach(const FailReason failReason);

	unsigned GetElapsedTime() const
	{
		return m_StopWatch.GetElapsedMilliseconds();
	}

#if !__NO_OUTPUT
	const char* FailReasonToString(const FailReason failReason) const;
#endif

	void NominatePair(CandidatePair *pair);
	bool ShouldDelayReport(const CompletionType completionType);
	void Report(const CompletionType completionType, const FailReason failReason);
	void Complete(const CompletionType completionType, const FailReason failReason, const CandidatePair* nominatedPair);
	void QuickConnect();

    enum SessionState
    {
        STATE_INACTIVE = -1,
        STATE_PENDING,
        STATE_SUCCEEDED,
        STATE_FAILED
    };

	enum OfferStatus
	{
		OFFER_STATUS_UNATTEMPTED = 0,
		OFFER_STATUS_FAILED_TO_SEND = 1,
		OFFER_STATUS_SENT = 2,
		OFFER_STATUS_SEND_TIMED_OUT = 3,
	};

	enum PresenceQueryState
	{
		PRESENCE_QUERY_STATE_UNATTEMPTED = 0,
		PRESENCE_QUERY_STATE_IN_PROGRESS,
		PRESENCE_QUERY_STATE_SUCCEEDED,
		PRESENCE_QUERY_STATE_FAILED,
	};

	enum LocalRelayRefreshState
	{
		LOCAL_RELAY_REFRESH_STATE_UNATTEMPTED = 0,
		LOCAL_RELAY_REFRESH_STATE_COMPLETED,
	};

	enum CrossRelayCheckState
	{
		CROSS_RELAY_STATE_UNATTEMPTED = 0,
		CROSS_RELAY_STATE_WAITING_FOR_PORT_MAPPER,
		CROSS_RELAY_STATE_DISCOVERING,
		CROSS_RELAY_STATE_SUCCEEDED,
		CROSS_RELAY_STATE_FAILED,
	};

	enum RelayRouteCheckState
	{
		RELAY_ROUTE_CHECK_STATE_UNATTEMPTED = 0,
		RELAY_ROUTE_CHECK_STATE_REQUEST_SENT,
		RELAY_ROUTE_CHECK_STATE_REPLY_RECEIVED,
		RELAY_ROUTE_CHECK_STATE_FAILED,
	};

	enum PresenceAttribIndices
	{
		ATTR_INDEX_PEER_ADDRESS,
		ATTR_MAX_PRESENCE_ATTRIBUTES
	};

	enum RoleTieBreakerStrategy
	{
		RTBS_RANDOM = 0,
		RTBS_IMPLICIT = 1,
		RTBS_STRICTEST_WINS = 2,
		RTBS_LEAST_STRICT_WINS = 3,
	};

	enum UnmatchedPublicIpState
	{
		UNMATCHED_PUBLIC_IP_STATE_NONE = 0,
		UNMATCHED_PUBLIC_IP_STATE_RECEIVED,
		UNMATCHED_PUBLIC_IP_STATE_NOMINATED,
	};

	// connection manager
	netConnectionManager* m_CxnMgr;

	// session identifiers
	OUTPUT_ONLY(unsigned m_TunnelRequestId);
	unsigned m_SessionId;
	unsigned m_DestSessionId;
	u64 m_GameSessionToken;
	u64 m_GameSessionId;

	// role management
	AgentRole m_Role;
	u64 m_RoleTieBreaker;

	// offer transaction state
	unsigned m_DirectOffersToken;
	unsigned m_IndirectOffersToken;
	unsigned m_LastIndirectOfferSentTime;
	OfferStatus m_IndirectOfferStatus;
	OfferStatus m_DirectOfferStatus;

	// addresses
	netPeerAddress m_LocalPeerAddr;
	netPeerAddress m_RemotePeerAddr;
	netAddress m_RemoteRelayAddr;
	netAddress m_NetAddr;
	Candidate::CandidateType m_LocalCandidateType;
	Candidate::CandidateType m_RemoteCandidateType;

	// message counts
	unsigned m_NumOffersSent;
	unsigned m_NumOffersReceived;
	unsigned m_NumAnswersSent;
	unsigned m_NumAnswersReceived;
	unsigned m_NumPingsSent;
	unsigned m_NumPingsReceived;
	unsigned m_NumPongsSent;
	unsigned m_NumPongsReceived;

	// ICE core structures
	netRetryTimer m_ChecklistTimer;
	netStopWatch m_StopWatch;
	netTimeout m_TerminationTimer;
	LocalCandidates m_LocalCandidates;
	RemoteCandidates m_RemoteCandidates;
	CheckList m_CheckList;
	TriggeredCheckQueue m_TriggeredQueue;
	ValidList m_ValidList;
	DiscoveredPairs m_DiscoveredPairs;
	Blacklist m_Blacklist;
	netIceTransactor m_Transactor;

	// local relay refresh state
	LocalRelayRefreshState m_LocalRelayRefreshState;

	// cross relay check state
	CrossRelayCheckState m_CrossRelayCheckState;
	netSocketAddress m_CrossRelayMappedAddrs[1];
	netStatus m_CrossRelayMappedAddrStatus[1];	
	netNatSharedSocketPortMapper m_SharedSocketPortMapper;

	// relay route check state
	RelayRouteCheckState m_RelayRouteCheckState;

	// presence query state
	PresenceQueryState m_PresenceQueryState;
	netStatus m_PresenceQueryStatus;
	rlScPresenceAttribute m_PresenceAttrs[ATTR_MAX_PRESENCE_ATTRIBUTES];

	// unmatched public IP state
	UnmatchedPublicIpState m_UnmatchedPublicIpState;

	// symmetric inference state
	SymmetricInference m_SymmetricInference;

	// symmetric socket state
	SymmetricSocket m_SymmetricSocket;

	// logging state
	netIceLogger m_LoggerInstance;
	netIceLogger* m_Logger;

	// simulation state
#if !__FINAL || defined(RSG_LEAN_CLIENT)
	bool m_ForceFail;
	u32 m_ForceFailTimeoutMs;
	u32 m_ForceDelayMs;
#endif

	// ICE session state
    SessionState m_State;
	SessionState m_RemoteState;
	FailReason m_FailReason;

	u8 m_RelayAssisted : 1;
	u8 m_Bilateral : 1;
	u8 m_HadRoleConflict : 1;
	u8 m_SentInitialOffers : 1;
	u8 m_RelayCommunicationSucceeded : 1;
	u8 m_RandomPortSymmetric : 1;
	u8 m_PortPreservingSymmetric : 1;
	u8 m_RemoteIsNonDeterministic : 1;
	u8 m_Canceled : 1;
	u8 m_SendDebugInfo : 1;
	u8 m_SendTelemetry : 1;

	// session results - pointers to caller-owned data
	netAddress* m_NetAddrPtr[MAX_ATTACHED_TUNNEL_REQUESTS];
	netAddress* m_RelayAddrPtr[MAX_ATTACHED_TUNNEL_REQUESTS];
	netStatus* m_Status[MAX_ATTACHED_TUNNEL_REQUESTS];
};

netIceSession::netIceSession()
: m_LoggerInstance(OUTPUT_ONLY(&m_TunnelRequestId,) &m_SessionId, &m_StopWatch)
, m_Logger(&m_LoggerInstance)
, m_LocalCandidates(&m_LoggerInstance)
, m_RemoteCandidates(&m_LoggerInstance)
, m_CheckList(&m_LoggerInstance)
, m_TriggeredQueue(&m_LoggerInstance)
, m_ValidList(&m_LoggerInstance)
, m_DiscoveredPairs(&m_LoggerInstance)
, m_Blacklist(&m_LoggerInstance)
, m_Transactor(&m_LoggerInstance)
, m_State(STATE_INACTIVE)
, m_RemoteState(STATE_INACTIVE)
{
	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		m_Status[i] = NULL;
	}

    this->Clear();
}

netIceSession::~netIceSession()
{
    this->Clear();
}

bool
netIceSession::Pending() const
{
    return (STATE_PENDING == m_State);
}

bool
netIceSession::IsActive() const
{
	return (STATE_INACTIVE != m_State);
}

bool
netIceSession::Succeeded() const
{
    return (STATE_SUCCEEDED == m_State);
}

void 
netIceSession::SetCanceled()
{
	/*
		The game tells us to cancel a lot of our in-flight NAT traversals. Each cancellation is recorded as a NAT traversal failure,
		and the remote peer won't know that we've canceled. He will eventually report a failure as well and fall back to relay.
		So (most) cancellations result in 2 NAT failures and 1 relay usage hit. Instead, allow NAT traversal to internally continue
		to completion even if they're canceled. This should increase NAT traversal success rate and reduce relay usage.
	*/

	netIceDebug3("SetCanceled :: Marking this ICE session as canceled. "
				 "Canceled ICE sessions continue to completion to avoid failures on the remote peer.");

	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		if(m_Status[i])
		{
			m_Status[i]->SetCanceled();
		}

		m_Status[i] = NULL;
		m_NetAddrPtr[i] = NULL;
		m_RelayAddrPtr[i] = NULL;
	}
	
	m_Canceled = true;
}

void
netIceSession::SetCanceled(netStatus* status)
{
	if(status == NULL)
	{
		return;
	}

	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		if(m_Status[i] == status)
		{
			netIceDebug3("SetCanceled :: Detaching");

			m_Status[i]->SetCanceled();

			m_Status[i] = NULL;
			m_NetAddrPtr[i] = NULL;
			m_RelayAddrPtr[i] = NULL;
		}
	}
}

unsigned
netIceSession::GetSessionId() const
{
    return m_SessionId;
}

unsigned
netIceSession::GetDestSessionId() const
{
	return m_DestSessionId;
}

void
netIceSession::Update()
{
#if !__FINAL || defined(RSG_LEAN_CLIENT)
	if(m_ForceFail)
	{
		if(this->GetElapsedTime() >= m_ForceFailTimeoutMs)
		{
			Complete(COMPLETE_FAILED, FAIL_REASON_SIMULATED_FAIL, NULL);
		}
		return;
	}
#endif

	
	if((netIceTunneler::sm_QuickConnectTimeoutMs > 0) &&
		this->Pending() &&
		(this->GetElapsedTime() >= netIceTunneler::sm_QuickConnectTimeoutMs))
	{
		FailReason failReason;
		const bool canUseQuickConnect = CanSendViaRelay(failReason) && (m_RelayCommunicationSucceeded || !netIceTunneler::sm_RequireRelaySuccessForQuickConnect);
		if(canUseQuickConnect)
		{
			QuickConnect();
		}
	}

#if !__FINAL || defined(RSG_LEAN_CLIENT)
	if((m_ForceDelayMs > 0) && (this->GetElapsedTime() < m_ForceDelayMs))
	{
		return;
	}
#endif

	m_TerminationTimer.Update();
	if(m_TerminationTimer.IsTimedOut())
	{
		// we're done
		if(Pending())
		{
			netIceDebug2("Emergency termination timer has expired, ice session finished.");
			Complete(COMPLETE_FAILED, FAIL_REASON_EMERGENCY_TIMER, NULL);
			Clear();
			return;
		}
		else
		{
			netIceDebug2("Termination timer has expired, ice session finished.");
			const CompletionType completionType = Succeeded() ? COMPLETE_SUCCEEDED : COMPLETE_FAILED;
			if(ShouldDelayReport(completionType))
			{
				netIceDebug3("Sending delayed report");
				Report(completionType, m_FailReason);
			}
			Clear();
			return;
		}
	}

	UpdateSymmetricInference();
	UpdateSymmetricSocket();
	UpdateCrossRelayCandidateCheck();
	UpdatePresenceQuery();
	
	m_ChecklistTimer.Update();

	if(m_ChecklistTimer.IsTimeToRetry())
	{
		/*
			5.8.  Scheduling Checks
			When the timer fires, the agent removes the top pair
			from the triggered check queue, performs a connectivity check on that
			pair, and sets the state of the candidate pair to In-Progress.  If
			there are no pairs in the triggered check queue, an ordinary check is
			sent.
		*/
		CandidatePair* candidatePair = NULL;
		do 
		{
			candidatePair = m_TriggeredQueue.GetNext();
		} while ((candidatePair != NULL) && (candidatePair->GetState() != CandidatePair::STATE_WAITING));

		if(candidatePair)
		{
			netIceDebug3("Taking a candidate pair from the triggered queue to perform a triggered check: %s. ", OBJ_TO_STRING(*candidatePair))
		}

		if(!candidatePair && m_CheckList.GetState() == CheckList::STATE_RUNNING)
		{
			/*
				When the timer fires and there is no triggered check to be sent, the
				agent MUST choose an ordinary check as follows:

				o  Find the highest-priority pair in that check list that is in the
				Waiting state.
			*/
			candidatePair = m_CheckList.GetNextWaitingPair();

			if(candidatePair)
			{
				netIceDebug3("Taking a candidate pair from the check list to perform an ordinary check: %s. ", OBJ_TO_STRING(*candidatePair));
			}
		}

		if(candidatePair)
		{
			/*
				if we have a candidate pair, send a connectivity check from the local
				candidate of that pair to the remote candidate of that pair, causing
				its state to transition to In-Progress.
			*/
			SendPing(candidatePair, IsControlling() ? candidatePair->ShouldUseCandidate() : false);
			m_ChecklistTimer.InitMilliseconds(CHECK_INTERVAL_MS);
		}
		else
		{
			/*
			if there is no such pair, terminate the timer for that check list
			*/
			
			// NOTE: we don't actually stop the timer here since receiving a ping
			// can put a failed pair back into the waiting state and we need to
			// continue processing it.
		}
	}

	m_Transactor.Update();

	// if we sent direct offers and they've all timed out, switch to indirect offer.
	// Note that we're using TimedOut state here, which means we will still accept
	// responses to direct offers even while we're sending indirect offers.
	if((m_DirectOfferStatus != OFFER_STATUS_SEND_TIMED_OUT) &&
		m_Transactor.AreAllTransactionsTimedOut(&m_DirectOffersToken))
	{
		m_DirectOfferStatus = OFFER_STATUS_SEND_TIMED_OUT;

		if(PassedOfferAnswerExchange())
		{
			netIceDebug1("Direct offer transactions timed out, but we're passed the offer/answer exchange.");
		}
		else
		{
			netIceDebug1("Direct offer transactions timed out.");

			if(IsSymmetricSocketInProgress() || NeedsSymmetricSocket(false))
			{
				if(IsSymmetricSocketInProgress())
				{
					netIceDebug1("Symmetric socket operation in progress.");
				}
				else if(StartSymmetricSocket() == false)
				{
					FailReason failReason;
					if(SendIndirectOffer(failReason) == false)
					{
						netIceDebug1("Failed to send indirect offer.");
						Complete(COMPLETE_FAILED, failReason, NULL);
						return;
					}
				}
			}
			else if(NeedsCrossRelayCandidateCheck())
			{
				if(StartCrossRelayCandidateCheck() == false)
				{
					FailReason failReason;
					if(SendIndirectOffer(failReason) == false)
					{
						netIceDebug1("Failed to send indirect offer.");
						Complete(COMPLETE_FAILED, failReason, NULL);
						return;
					}
				}
			}
			else
			{
				FailReason failReason;
				if(SendIndirectOffer(failReason) == false)
				{
					netIceDebug1("Failed to send indirect offer.");
					Complete(COMPLETE_FAILED, failReason, NULL);
					return;
				}
			}
		}
	}

	// if we sent indirect offers and they've timed out, we may not have the remote peer's
	// up-to-date relay address. We'll look it up from presence and try again. Note that
	// we're using TimedOut state here, which means we will still accept responses to
	// indirect offers while we're querying presence. If the presence query fails, or if
	// it confirms we have the correct relay address for the remote peer, we'll let the
	// existing indirect offer continue until it has fully expired.
	if(NeedsPresenceQuery() &&
		m_Transactor.AreAllTransactionsTimedOut(&m_IndirectOffersToken))
	{
		netIceDebug1("Indirect offer transactions timed out.");

		if(StartPresenceQuery() == false)
		{
			netIceDebug1("Failed to start presence query.");
		}
	}
	
	if(m_Transactor.AreAllTransactionsExpiredButNotCancelled(&m_IndirectOffersToken))
	{
		if(IsPresenceQueryInProgress() == false)
		{
			netIceDebug1("Indirect offer transactions expired.");

			m_IndirectOfferStatus = OFFER_STATUS_SEND_TIMED_OUT;

			if(IndirectOfferTimedOut())
			{
				return;
			}
		}
	}

	// go through the list of transactions for each candidate pair.
	for(unsigned i = 0; i < m_CheckList.GetCount(); ++i)
	{
		CandidatePair* pair = m_CheckList.GetCandidatePair(i);

		// if all transactions are expired for this pair, then set the pair to failed
		if(m_Transactor.AreAllTransactionsExpiredButNotCancelled(pair))
		{
			netIceDebug3("All transactions for pair have expired: %s.", OBJ_TO_STRING(*pair));

			if(pair->GetState() != CandidatePair::STATE_SUCCEEDED)
			{
				pair->SetState(CandidatePair::STATE_FAILED);
			}
		}
	}

	m_Transactor.RemoveExpiredTransactions();

	if(NeedLocalRelayRefresh())
	{
		RefreshLocalRelay();
	}

	/*
	7.1.3.3.  Check List and Timer State Updates

	Regardless of whether the check was successful or failed, the
	completion of the transaction may require updating of check list and
	timer states. If all of the pairs in the check list are now either in
	the Failed or Succeeded state:

	o  If there is not a pair in the valid list for each component of the
	media stream, the state of the check list is set to Failed.
	*/

	if(m_CheckList.GetState() == CheckList::STATE_RUNNING)
	{
		bool allPairsDone = m_CheckList.GetCount() > 0;
		for(unsigned i = 0; i < m_CheckList.GetCount(); ++i)
		{
			CandidatePair* pair = m_CheckList.GetCandidatePair(i);
			if(pair->GetState() != CandidatePair::STATE_FAILED && pair->GetState() != CandidatePair::STATE_SUCCEEDED)
			{
				allPairsDone = false;
				break;
			}
		}

		if(allPairsDone)
		{
			if(m_ValidList.GetCount() == 0)
			{
				// if we got this far and still haven't tried a symmetric socket, then try it now.
				// We need to wait until it's created, and while sending the offer, and then wait for a reply.
				bool isSymmetricSocketInProgress = IsSymmetricSocketInProgress() || (NeedsSymmetricSocket(false) && StartSymmetricSocket());
				bool isSymmetricSocketReady = m_SymmetricSocket.m_State == SymmetricSocket::STATE_READY_FOR_ASSOCIATION;
				bool transctionsInProgress = m_Transactor.GetCount() > 0;
				if(!isSymmetricSocketInProgress && !isSymmetricSocketReady && !transctionsInProgress)
				{
					netIceDebug2("All of our checks are done but we have no valid pairs.");
					m_CheckList.SetState(CheckList::STATE_FAILED);

					// collect info on when we fail due to different Strict NAT behaviors
					if(netNatDetector::GetLocalNatInfo().IsNonDeterministic() || m_RemoteIsNonDeterministic)
					{
						Complete(COMPLETE_FAILED, FAIL_REASON_NON_DETERMINISTIC, NULL);
					}
					else if(m_PortPreservingSymmetric)
					{
						Complete(COMPLETE_FAILED, FAIL_REASON_PORT_PRESERVING_SYMMETRIC, NULL);
					}
					else if(m_RandomPortSymmetric)
					{
						Complete(COMPLETE_FAILED, FAIL_REASON_PORT_RANDOM_SYMMETRIC, NULL);
					}
					else if((m_LocalPeerAddr.GetNatType() == NET_NAT_STRICT) || (m_RemotePeerAddr.GetNatType() == NET_NAT_STRICT))
					{
						// if either side is Strict, record failure as random port symmetric
						Complete(COMPLETE_FAILED, FAIL_REASON_PORT_RANDOM_SYMMETRIC, NULL);
					}
					else
					{
						Complete(COMPLETE_FAILED, FAIL_REASON_NO_VALID_PAIRS, NULL);
					}

					return;
				}
			}
		}
	}
}

void
netIceSession::NominatePair(CandidatePair *pair)
{
	pair->SetNominated();

	if(m_CheckList.GetState() == CheckList::STATE_RUNNING)
	{
		/*
			8.1.2.  Updating States
			o  If there is at least one nominated pair in the valid list for a
			media stream and the state of the check list is Running:

			*  The agent MUST remove all Waiting and Frozen pairs in the check
			list and triggered check queue for the same component as the
			nominated pairs for that media stream.

			*  If an In-Progress pair in the check list is for the same
			component as a nominated pair, the agent SHOULD cease
			retransmissions for its check if its pair priority is lower
			than the lowest-priority nominated pair for that component.
		*/
		m_CheckList.RemoveAllWaitingPairs();
		m_TriggeredQueue.Clear();
		m_Transactor.CancelAll();

		/*
			8.1.2.  Updating States
			Once there is at least one nominated pair in the valid list for
			every component of at least one media stream and the state of the
			check list is Running:

			*  The agent MUST change the state of processing for its check
			list for that media stream to Completed.
		*/
		m_CheckList.SetState(CheckList::STATE_COMPLETED);
		
		if(pair->GetLocalCandidate().GetType() == Candidate::SYMMETRIC_SOCKET_CANDIDATE)
		{
			m_SymmetricSocket.m_State = SymmetricSocket::STATE_NOMINATED_CANDIDATE;
			SymmetricSocket::sm_NumNominated++;
		}

		Complete(COMPLETE_SUCCEEDED, FAIL_REASON_NONE, pair);
	}
}

void
netIceSession::ReceiveAnswer(const netIceSessionAnswer* msg, const netAddress& sender)
{
#if !__FINAL || defined(RSG_LEAN_CLIENT)
	if(m_ForceFail)
	{
		return;
	}

	if(PARAM_neticeforcesymmetricsocket.Get())
	{
		if(sender.GetTargetAddress() == m_RemotePeerAddr.GetPrivateAddress())
		{
			netIceDebug2("-neticeforcesymmetricsocket is enabled, ignoring answer from " NET_ADDR_FMT ". TransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);
			return;
		}
	}
#endif

	m_NumAnswersReceived++;

	netIceDebug1("Received answer from " NET_ADDR_FMT ". TransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);

	if(sender.IsDirectAddr() && m_Blacklist.IsBlacklisted(sender.GetTargetAddress()))
	{
		netIceDebug2("Sender is blacklisted, discarding answer.");
		return;
	}

	if(sender.IsRelayServerAddr())
	{
		m_RelayAssisted = true;
		m_RelayCommunicationSucceeded = true;
	}	

#if !__NO_OUTPUT
	if(m_RemoteRelayAddr != msg->m_SourcePeerAddr.GetRelayAddress())
	{
		netIceDebug("Relay address from netIceSessionAnswer: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_SourcePeerAddr.GetRelayAddress()));

		if(m_RemoteRelayAddr.GetProxyAddress().IsValid() && m_RemoteRelayAddr.GetRelayToken().IsValid() && !m_RemoteRelayAddr.GetTargetAddress().IsValid())
		{
			netIceDebug("Exchanging secure relay address: " NET_ADDR_FMT " with real address: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_RemoteRelayAddr), NET_ADDR_FOR_PRINTF(msg->m_SourcePeerAddr.GetRelayAddress()));
		}
	}
#endif

	m_RemoteRelayAddr = msg->m_SourcePeerAddr.GetRelayAddress();

	AddRemoteCandidates(msg, sender);

	if(msg->m_SendDebugInfo)
	{
		m_SendDebugInfo = true;
	}

	if(msg->m_NonDeterministic)
	{
		m_RemoteIsNonDeterministic = true;
		netIceDebug2("The remote peer is behind a non-deterministic NAT");
	}

	// find the transaction id that sent the request that generated this response
	const netIceTransaction* transaction = m_Transactor.FindTransactionById(msg->m_TransactionId);
	if(transaction == NULL)
	{
		netIceDebug3("Ignoring answer since we don't have a transaction with id %u. "
					"We might have already processed an answer for this transaction and "
					"the peer sent an answer to an offer retry, or the transaction might "
					"have timed out.", msg->m_TransactionId);
		return;
	}

	// retrieve the request that generated this response
	netIceSessionOffer offer;
	if(!netVerify(offer.Import(transaction->GetMessageData(), transaction->GetSizeOfData())))
	{
		netIceDebug3("Ignoring answer since importing the associated offer failed.");
		m_Transactor.RemoveTransaction(msg->m_TransactionId);
		return;
	}

	//netAssert(m_DestSessionId == NET_INVALID_ICE_SESSION_ID || (m_DestSessionId == msg->m_SourceSessionId));

	if(m_DestSessionId == NET_INVALID_ICE_SESSION_ID)
	{
		netIceDebug3("Setting remote session ID: %u", msg->m_SourceSessionId);
	}
	else if(m_DestSessionId != msg->m_SourceSessionId)
	{
		netIceWarning("Changing remote session ID: %u", msg->m_SourceSessionId);
	}

	m_DestSessionId = msg->m_SourceSessionId;
	netAssert(m_DestSessionId != NET_INVALID_ICE_SESSION_ID);

	SendRelayRouteCheckRequest();

	m_Transactor.RemoveTransaction(msg->m_TransactionId);

	// cancel any other offer transactions we have in progress
	m_Transactor.CancelTransactionsByUserData(&m_DirectOffersToken);
	m_Transactor.CancelTransactionsByUserData(&m_IndirectOffersToken);

	if(msg->m_SenderHasControllingRole)
	{
		netIceDebug1("Sender is telling us he received our offer but he has the "
						"controlling role. We'll wait for his offer to arrive.");
	}
	else if(m_ChecklistTimer.IsStopped())
	{
		m_ChecklistTimer.InitMilliseconds((IsLocalStrict() && (m_RemotePeerAddr.GetNatType() != NET_NAT_STRICT)) ? CHECK_START_DELAY_STRICT_NAT_MS : CHECK_INTERVAL_MS);

		if(!IsLocalStrict())
		{
			m_ChecklistTimer.ForceRetry();

			// call update here so the agent performs a connectivity 
			// check the moment the offer/answer exchange has been done
			Update();
		}
	}
	else
	{
		netIceDebug3("Answer is for a session already in progress");
	}
}

void
netIceSession::ReceivePing(const netIceSessionPing* msg, const netAddress& sender)
{
#if !__FINAL || defined(RSG_LEAN_CLIENT)
	if(m_ForceFail)
	{
		return;
	}

	if(PARAM_neticeforcesymmetricsocket.Get())
	{
		if(sender.GetTargetAddress() == m_RemotePeerAddr.GetPrivateAddress())
		{
			netIceDebug2("-neticeforcesymmetricsocket is enabled, ignoring ping from:" NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);
			return;
		}
	}
#endif

	m_NumPingsReceived++;

	netIceDebug1("Received ping from:" NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);

	if(sender.IsDirectAddr() && m_Blacklist.IsBlacklisted(sender.GetTargetAddress()))
	{
		netIceDebug2("Sender is blacklisted, discarding ping.");
		return;
	}

	UpdateUnmatchedPublicIpState(sender.GetTargetAddress(), false);

#if !__NO_OUTPUT
	if(msg->m_DestPeerAddr.GetPrivateAddress() != m_LocalPeerAddr.GetPrivateAddress())
	{
		netIceDebug3("Remote peer thinks my private address is: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_DestPeerAddr.GetPrivateAddress()));
	}

	if(msg->m_DestPeerAddr.GetPublicAddress() != m_LocalPeerAddr.GetPublicAddress())
	{
		netIceDebug3("Remote peer thinks my public address is: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_DestPeerAddr.GetPublicAddress()));
	}

	if(msg->m_DestPeerAddr.GetRelayAddress() != m_LocalPeerAddr.GetRelayAddress())
	{
		netIceDebug3("Remote peer thinks my relay address is: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_DestPeerAddr.GetRelayAddress()));
	}
#endif

	/*
		If the source transport address of the request does not match any
		existing remote candidates, it represents a new peer reflexive remote
		candidate.
	*/
	const RemoteCandidate* remoteCandidate = m_RemoteCandidates.FindByTransportAddress(sender.GetTargetAddress());
	
	if(!remoteCandidate)
	{
		// if there are blacklisted addresses, it likely means that peer reflexive candidates will not
		// work, and can lead to runaway port allocations on the NAT. See comments for the Blacklist class.
		if(m_Blacklist.IsFull())
		{
			netIceDebug3("Remote candidate for " NET_ADDR_FMT " not found. "
						"Not adding a remote peer reflexive candidate due to active blacklist.",
						NET_ADDR_FOR_PRINTF(sender));
			return;
		}

		/*
			This candidate is added to the list of remote candidates.  However,
			the agent does not pair this candidate with any local candidates.
		*/
		netIceDebug3("Remote candidate for " NET_ADDR_FMT " not found. "
				  "Adding a remote peer reflexive candidate.",
				  NET_ADDR_FOR_PRINTF(sender));

		RemoteCandidate peerReflexiveCandidate(m_Logger, Candidate::PEER_REFLEXIVE_CANDIDATE, sender.GetTargetAddress());
		remoteCandidate = m_RemoteCandidates.AddCandidate(peerReflexiveCandidate);
		if(remoteCandidate == NULL)
		{
			netIceError("ReceivePing(): Failed to add to remote candidates list.");
			return;
		}
	}

	/*
		7.2.1.4.  Triggered Checks

		Next, the agent constructs a pair whose local candidate is equal to
		the transport address on which the STUN request was received, and a
		remote candidate equal to the source transport address where the
		request came from (which may be the peer reflexive remote candidate
		that was just learned).  The local candidate will either be a host
		candidate (for cases where the request was not received through a
		relay) or a relayed candidate (for cases where it is received through
		a relay).  The local candidate can never be a server reflexive
		candidate.
	*/

	const LocalCandidate* localCandidate = nullptr;
	if(msg->m_ViaSymmetricSocket)
	{
		if(m_SymmetricSocket.IsAssociated())
		{
			const netSocketAddress& socketAddr = m_CxnMgr->GetSocketManager()->GetSymmetricSocketAddress(m_SymmetricSocket.m_SocketId);
			if(netVerify(socketAddr.IsValid()))
			{
				localCandidate = m_LocalCandidates.FindByTransportAddress(socketAddr);
			}
		}

		if(!localCandidate)
		{
			netIceDebug3("Ignoring ping from remote peer's symmetric socket since we haven't associated ours yet.");
			return;
		}
	}

	if(!localCandidate)
	{
		localCandidate = m_LocalCandidates.FindByTransportAddress(m_LocalPeerAddr.GetPrivateAddress());
		if(!netVerify(localCandidate && (localCandidate->GetType() == Candidate::HOST_CANDIDATE)))
		{
			netIceError("ReceivePing(): Local Host Candidate not found. Could be a ping from a previous ICE session.");
			return;
		}
	}

	if(!localCandidate || !remoteCandidate)
	{
		return;
	}

	CandidatePair pair(m_Logger, *localCandidate, *remoteCandidate);

	/*
		This pair is then looked up in the check list.
	*/
	CandidatePair* foundPair = m_CheckList.FindMatchingPair(pair);

	if(foundPair)
	{
		netIceDebug3("Found matching pair on our checklist: %s", OBJ_TO_STRING(*foundPair));
		netAssert(m_DiscoveredPairs.Contains(foundPair));

		/*
			If the pair is already on the check list:
			*  If the state of that pair is Waiting, a check for
			that pair is enqueued into the triggered check queue if not
			already present.
		*/
		if(foundPair->GetState() == CandidatePair::STATE_WAITING)
		{
			if(m_TriggeredQueue.FindMatchingPair(*foundPair) == NULL)
			{
				m_TriggeredQueue.Push(foundPair);
			}
		}
		else if(foundPair->GetState() == CandidatePair::STATE_IN_PROGRESS)
		{
			/*
				*  If the state of that pair is In-Progress, the agent cancels the
				in-progress transaction.  Cancellation means that the agent
				will not retransmit the request, will not treat the lack of
				response to be a failure, but will wait the duration of the
				transaction timeout for a response.  In addition, the agent
				MUST create a new connectivity check for that pair
				(representing a new STUN Binding request transaction) by
				enqueueing the pair in the triggered check queue.  The state of
				the pair is then changed to Waiting.
				[NS] - however, if we continue to receive pings but the remote
				peer doesn't receive our pongs, we end up continuously testing
				this pair until the emergency timer goes off. To mitigate this,
				allow a max number of transactions per pair.
			*/
			if(foundPair->GetTransactionCount() < MAX_TRANSACTIONS_PER_CANDIDATE_PAIR)
			{
				m_Transactor.CancelTransactionsByUserData(foundPair);
				foundPair->SetState(CandidatePair::STATE_WAITING);
				m_TriggeredQueue.Push(foundPair);
			}
			else
			{
				netIceDebug3("Not enqueing candidate pair to the triggered queue because we've already had at least %u transactions for pair %s.", MAX_TRANSACTIONS_PER_CANDIDATE_PAIR, OBJ_TO_STRING(*foundPair));
			}
		}
		else if(foundPair->GetState() == CandidatePair::STATE_FAILED)
		{
			/*
				*  If the state of the pair is Failed, it is changed to Waiting
				and the agent MUST create a new connectivity check for that
				pair (representing a new STUN Binding request transaction), by
				enqueueing the pair in the triggered check queue.
			*/
			foundPair->SetState(CandidatePair::STATE_WAITING);
			m_TriggeredQueue.Push(foundPair);
		}
		else if(foundPair->GetState() == CandidatePair::STATE_SUCCEEDED)
		{
			/*
			*  If the state of that pair is Succeeded, nothing further is done.
			*/
		}
	}
	else
	{
		/*
			If the pair is not already on the check list:
			*  The pair is inserted into the check list based on its priority.
			*  Its state is set to Waiting.
			*  The pair is enqueued into the triggered check queue.
		*/
		netIceDebug3("No matching pair found on our checklist.");

		foundPair = m_DiscoveredPairs.FindMatchingPair(pair);
		if(foundPair == NULL)
		{
			foundPair = m_DiscoveredPairs.AddCandidatePair(pair);
		}

		if(foundPair)
		{
			foundPair->SetState(CandidatePair::STATE_WAITING);
			m_CheckList.AddCandidatePair(foundPair);
			m_TriggeredQueue.Push(foundPair);
		}
	}

	if(foundPair == NULL)
	{
		netIceError("ReceivePing: foundPair is null");
		return;
	}

	/*
		7.2.1.5.  Updating the Nominated Flag
		If the Binding request received by the agent had the USE-CANDIDATE attribute
		set, and the agent is in the controlled role, the agent looks at the state
		of the pair computed in Section 7.2.1.4:
	*/
	if(IsControlled() && msg->m_UseCandidate)
	{
		netIceDebug3("Ping has 'USE-CANDIDATE' flag and we're in the CONTROLLED role.");

		if(foundPair->GetState() == CandidatePair::STATE_SUCCEEDED)
		{
			/*
				If the state of this pair is Succeeded, it means that the check
				generated by this pair produced a successful response.  This would
				have caused the agent to construct a valid pair when that success
				response was received (see Section 7.1.3.2.2).  The agent now sets
				the nominated flag in the valid pair to true.
			*/

			netIceDebug3("This pair already succeeded earlier, so nominating this pair.");

			/*
				[NS] The foundPair is not necessarily on our valid list since it's the
				check that *generated* the valid pair that got set to succeeded.
				For example, the valid list might contain a Server Reflexive -> Server Reflexive
				pair, which would never be on our checklist since we can't send directly
				from our Server Reflexive address. In this example, the check that *generated* the 
				valid pair would be a Host Candidate -> Server Reflexive pair.
			*/

			NominatePair(foundPair);
		}
		else
		{
			/*
				If the state of this pair is In-Progress, if its check produces a
				successful result, the resulting valid pair has its nominated flag
				set when the response arrives.  This may end ICE processing for
				this media stream when it arrives; see Section 8.
			*/

			// [NS] we always capture the use-candidate flag. If we're still
			// in the waiting state we still want to nominate this pair on success.

			netIceDebug3("This pair is waiting or in progress, "
						 "setting its 'nominate on success' flag.");

			foundPair->SetNominatedOnSuccess();
		}
	}

	SendPong(msg->m_SourceSessionId, msg->m_TransactionId, foundPair, sender.GetTargetAddress());
}

void
netIceSession::ReceivePong(const netIceSessionPong* msg, const netAddress& sender)
{
#if !__FINAL || defined(RSG_LEAN_CLIENT)
	if(m_ForceFail)
	{
		return;
	}

	if(PARAM_neticeforcesymmetricsocket.Get())
	{
		if(sender.GetTargetAddress() == m_RemotePeerAddr.GetPrivateAddress())
		{
			netIceDebug2("-neticeforcesymmetricsocket is enabled, ignoring pong from:" NET_ADDR_FMT " TransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);
			return;
		}
	}
#endif

	m_NumPongsReceived++;

	netIceDebug1("Received pong from:" NET_ADDR_FMT " TransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);

	if(sender.IsDirectAddr() && m_Blacklist.IsBlacklisted(sender.GetTargetAddress()))
	{
		netIceDebug2("Sender is blacklisted, discarding pong.");
		return;
	}

	UpdateUnmatchedPublicIpState(sender.GetTargetAddress(), false);

	netSocketAddress mappedAddr = netIceSessionPong::DeObfuscateMappedAddress(msg->m_ObfuscatedMappedAddr);
	netIceDebug3("Our mapped address as seen from the peer:" NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(mappedAddr));

	/*
		7.1.3.  Processing the Response

		When a Binding response is received, it is correlated to its Binding
		request using the transaction ID, as defined in [RFC5389], which then
		ties it to the candidate pair for which the Binding request was sent.
	*/

	// find the transaction id that sent the request that generated this response
	const netIceTransaction* transaction = m_Transactor.FindTransactionById(msg->m_TransactionId);
	if(transaction == NULL)
	{
		netIceDebug3("Ignoring pong since we don't have a transaction with id %u. "
						"We might have already processed a pong for this transaction and "
						"the peer sent a pong to a ping retry, or the transaction might "
						"have timed out.", msg->m_TransactionId);
		return;
	}

	if(msg->m_Terminate)
	{
		netIceDebug2("Received terminate pong from:" NET_ADDR_FMT " TransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);
		Complete(COMPLETE_FAILED, FAIL_REASON_REMOTE_TERMINATED, NULL);
		return;
	}

	// retrieve the request that generated this response
	netIceSessionPing request;
	if(!netVerify(request.Import(transaction->GetMessageData(), transaction->GetSizeOfData())))
	{
		netIceDebug3("Ignoring pong since importing the associated request failed.");
		m_Transactor.RemoveTransaction(msg->m_TransactionId);
		return;
	}

	// the transaction contains the candidate pair for which we sent the request
	CandidatePair* candidatePair = (CandidatePair*)transaction->GetUserData();
	if(netVerify(candidatePair) && (netVerify(m_DiscoveredPairs.Contains(candidatePair)) == false))
	{
		netIceDebug3("Ignoring pong since our discovered pairs list doesn't contain the associated candidate pair.");
		m_Transactor.RemoveTransaction(msg->m_TransactionId);
		return;
	}

	m_Transactor.RemoveTransaction(msg->m_TransactionId);

	netIceDebug3("Found transaction associated with this pong:%s", OBJ_TO_STRING(*candidatePair));

	/*
		7.1.3.1.  Failure Cases
		The agent MUST check that the source IP address and port of the
		response equal the destination IP address and port to which the
		Binding request was sent, and that the destination IP address and
		port of the response match the source IP address and port from which
		the Binding request was sent.  In other words, the source and
		destination transport addresses in the request and responses are
		symmetric.  If they are not symmetric, the agent sets the state of
		the pair to Failed.
	*/

	if(sender.GetTargetAddress() != candidatePair->GetRemoteCandidate().GetTransportAddress())
	{
		netIceDebug3("The source IP address and port of the response does not equal the destination "
			"IP address and port to which the request was sent. Sender:" NET_ADDR_FMT", Expected:" NET_ADDR_FMT,
			NET_ADDR_FOR_PRINTF(sender), NET_ADDR_FOR_PRINTF(candidatePair->GetRemoteCandidate().GetTransportAddress()));

		if(candidatePair->GetState() != CandidatePair::STATE_SUCCEEDED)
		{
			candidatePair->SetState(CandidatePair::STATE_FAILED);
		}

		if(sender.IsDirectAddr())
		{
			// add the expected address to the blacklist to avoid symmetric NATs from continuously opening new ports
			// toward one another for each ping/pong sent. Observed when connecting two symmetric NATs with endpoint 
			// independent filtering. Also cancel any current/future transactions in progress to this address.

			const netSocketAddress& badAddr = candidatePair->GetRemoteCandidate().GetTransportAddress();
			m_Blacklist.AddAddress(badAddr);
			m_Transactor.RemoveTransactionsByAddress(badAddr);
			m_TriggeredQueue.RemovePairsWithTargetAddr(badAddr);
			m_CheckList.FailPairsWithTargetAddr(badAddr);
		}

		return;
	}
	
	if(netVerify(sender.IsValid()) == false)
	{
		return;
	}

	/*
		The agent checks the mapped address from the STUN response.  If the
		transport address does not match any of the local candidates that the
		agent knows about, the mapped address represents a new candidate -- a
		peer reflexive candidate.
	*/
	const LocalCandidate* localCandidate = m_LocalCandidates.FindByTransportAddress(mappedAddr);

	if(localCandidate)
	{
		netIceDebug3("The mapped ip/port seen by our peer matches the ip/port from which we sent the request "
				  "(i.e. we didn't open a different port when we sent the request)");
	}
	else
	{
		/*
			This peer reflexive candidate is then added to the list of local
			candidates. However, the peer reflexive candidate is not paired 
			with other remote candidates. This is not necessary; a valid
			pair will be generated from it momentarily.
		*/

		netIceDebug3("The mapped ip/port seen by our peer (" NET_ADDR_FMT ") doesn't match any of our local candidates. "
				  "It looks like we're behind an Endpoint Dependent Mapping NAT or there is a different NAT between "
				  "us and the remote peer than between us and the relay server. "
				  "Adding a local peer reflexive candidate.",
				  NET_ADDR_FOR_PRINTF(mappedAddr));

		LocalCandidate peerReflexiveCandidate(m_Logger, Candidate::PEER_REFLEXIVE_CANDIDATE, mappedAddr);
		localCandidate = m_LocalCandidates.AddCandidate(peerReflexiveCandidate);

		netIceTunneler::AddPeerReflexiveAddress(sender, mappedAddr);
	}

	if(localCandidate == NULL)
	{
		netIceError("ReceivePong: localCandidate is null");
		return;
	}

	/*
		7.1.3.2.2.  Constructing a Valid Pair

		The agent constructs a candidate pair whose local candidate equals
		the mapped address of the response, and whose remote candidate equals
		the destination address to which the request was sent.  This is
		called a valid pair, since it has been validated by a STUN
		connectivity check. 
	*/
	netAssert(localCandidate->GetTransportAddress() == mappedAddr);
	RemoteCandidate remoteCandidate = candidatePair->GetRemoteCandidate();
	CandidatePair* validPair = m_DiscoveredPairs.FindMatchingPair(CandidatePair(m_Logger, *localCandidate, remoteCandidate));
	if(validPair == NULL)
	{
		validPair = m_DiscoveredPairs.AddCandidatePair(CandidatePair(m_Logger, *localCandidate, remoteCandidate));

		if(validPair == NULL)
		{
			netIceError("ReceivePong: failed to add valid pair to discovered pairs list");
			return;
		}
	}

	validPair->SetValid();

	if(validPair->GetLocalCandidate().GetType() == Candidate::SYMMETRIC_SOCKET_CANDIDATE)
	{
		m_SymmetricSocket.m_State = SymmetricSocket::STATE_VALID_CANDIDATE;
	}

	// the pair is then added to the VALID LIST
	if(m_ValidList.FindMatchingPair(*validPair) == NULL)
	{
		m_ValidList.AddValidPair(validPair);
	}

	/*
		The agent sets the state of the pair that *generated* the check to
		Succeeded.  Note that, the pair which *generated* the check may be
		different than the valid pair.
	*/
	candidatePair->SetState(CandidatePair::STATE_SUCCEEDED);

	/*
		7.1.3.2.4.  Updating the Nominated Flag

		If the agent was a controlling agent, and it had included a USE-
		CANDIDATE attribute in the Binding request, the valid pair generated
		from that check has its nominated flag set to true.  This flag
		indicates that this valid pair should be used for media if it is the
		highest-priority one amongst those whose nominated flag is set.  This
		may conclude ICE processing for this media stream or all media
		streams; see Section 8.
	*/
	if(IsControlling() && request.m_UseCandidate)
	{
		netIceDebug3("We're the controlling agent and we had included the USE-CANDIDATE "
				  "attribute in the ping that generated this pong, so nominating pair");

		NominatePair(validPair);
	}

	/*
		If the agent is the controlled agent, the response may be the result
		of a triggered check that was sent in response to a request that
		itself had the USE-CANDIDATE attribute.  This case is described in
		Section 7.2.1.5, and may now result in setting the nominated flag for
		the pair learned from the original request.
	*/
	if(IsControlled() && candidatePair->ShouldNominateOnSuccess())
	{
		netIceDebug3("We're the controlled agent and we received the USE-CANDIDATE "
				  "attribute in a previous ping for this pair, so nominating pair");

		NominatePair(validPair);
	}

	if(IsControlling() && (Pending()))
	{
		if(validPair->ShouldUseCandidate() == false)
		{
			/*
				8.1.1.1.  Regular Nomination
				When the controlling agent selects the valid pair, it repeats the
				check that produced this valid pair (by enqueuing the pair that
				generated the check into the triggered check queue), this time with
				the USE-CANDIDATE attribute. 
			*/
			// [NS] we just select the first valid pair we get
			if(m_ValidList.GetCount() == 1)
			{
				netIceDebug3("We're the controlling agent and we now have a valid pair. "
						  "Setting the USE-CANDIDATE attribute on this pair and adding to triggered "
						  "queue:%s", OBJ_TO_STRING(*validPair));

				validPair->SetUseCandidate();
				validPair->SetState(CandidatePair::STATE_WAITING);
				if(m_TriggeredQueue.FindMatchingPair(*validPair) == NULL)
				{
					m_TriggeredQueue.Push(validPair);
				}
			}
		}
		else
		{
			if(m_TriggeredQueue.FindMatchingPair(*validPair) != NULL)
			{
				validPair->SetState(CandidatePair::STATE_WAITING);
			}
		}
	}
}

void 
netIceSession::ReceiveRelayRouteCheck(const netIceSessionRelayRouteCheck* msg, const netAddress& sender)
{
	rtry
	{
		rcheckall(netIceTunneler::sm_AllowRelayRouteCheck);

		netIceDebug1("Received relay route check %s from:" NET_ADDR_FMT, msg->IsRequest() ? "request" : "response", NET_ADDR_FOR_PRINTF(sender));

		if(msg->IsRequest())
		{
			SendRelayRouteCheckResponse(msg, sender);
		}
		else if(msg->IsResponse())
		{
			rverifyall(m_RelayRouteCheckState == RELAY_ROUTE_CHECK_STATE_REQUEST_SENT);
			m_RelayRouteCheckState = RELAY_ROUTE_CHECK_STATE_REPLY_RECEIVED;
		}
	}
	rcatchall
	{
	}
}

void
netIceSession::SendPing(CandidatePair* candidatePair, bool useCandidate)
{
	rtry
	{
		rverifyall(candidatePair);

		const netSocketAddress& destSockAddr = candidatePair->GetRemoteCandidate().GetTransportAddress();
		if(m_Blacklist.IsBlacklisted(destSockAddr))
		{
			netIceDebug2("Address " NET_ADDR_FMT " is blacklisted, not sending ping.", NET_ADDR_FOR_PRINTF(destSockAddr));
			return;
		}

		bool viaSymmetricSocket = m_SymmetricSocket.IsAssociated() && (candidatePair->GetRemoteCandidate().GetTransportAddress() == m_SymmetricSocket.m_RemoteMappedAddr);

		unsigned txId = m_Transactor.NextId();
		netIceSessionPing msg(m_LocalPeerAddr, m_RemotePeerAddr, GetSessionId(), GetDestSessionId(), txId, viaSymmetricSocket, useCandidate);
		netAddress destAddr(destSockAddr);
		rverify(m_Transactor.SendRequest(txId, m_RemotePeerAddr.GetPeerKey(), destAddr, msg, MAX_RETRIES_PER_TRANSACTION, TRANSACTION_RETRY_INTERVAL_MS, candidatePair), catchall, );

		m_NumPingsSent++;

		candidatePair->IncreaseTransactionCount();
		candidatePair->SetState(CandidatePair::STATE_IN_PROGRESS);
	}
	rcatchall
	{

	}
}

void
netIceSession::SendPong(unsigned destSessionId, unsigned transactionId, CandidatePair* NET_ASSERTS_ONLY(candidatePair), const netSocketAddress& mappedAddr)
{
	if(m_Blacklist.IsBlacklisted(mappedAddr))
	{
		netIceDebug2("Address " NET_ADDR_FMT " is blacklisted, not sending pong.", NET_ADDR_FOR_PRINTF(mappedAddr));
		return;
	}

	bool viaSymmetricSocket = m_SymmetricSocket.IsAssociated() && (mappedAddr == m_SymmetricSocket.m_RemoteMappedAddr);

	netIceSessionPong msg(destSessionId, transactionId, mappedAddr, viaSymmetricSocket, false);

	netAddress destAddr(mappedAddr);
	netIceDebug1("Sending pong to:" NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(destAddr), transactionId);

	netAssert(mappedAddr == candidatePair->GetRemoteCandidate().GetTransportAddress());

	netP2pCryptContext context;
	context.Init(m_RemotePeerAddr.GetPeerKey(), NET_P2P_CRYPT_TUNNELING_PACKET);

	if(m_CxnMgr->IsInitialized() &&
		m_CxnMgr->SendTunnelingPacket(destAddr,
									  msg,
									  context))
	{
		m_NumPongsSent++;
	}
}

void 
netIceSession::ReceiveDebugInfo(const netIceSessionDebugInfo* OUTPUT_ONLY(msg), const netAddress& OUTPUT_ONLY(sender))
{
#if !__NO_OUTPUT
	netIceDebug2("Received debug info message from " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(sender));

	bool remoteSucceeded = strstr(msg->m_Metrics, "\"S\":true") != NULL; 
	m_RemoteState = (remoteSucceeded) ? STATE_SUCCEEDED : STATE_FAILED;

	if(!Pending())
	{
		if(remoteSucceeded && !Succeeded())
		{
			netIceDebug2("\tRemote session succeeded. Local session failed. One way success.");
		}
		else if(!remoteSucceeded && Succeeded())
		{
			netIceDebug2("\tRemote session failed. Local session succeeded. One way success.");
		}
	}
	else
	{
		netIceDebug2("\tRemote session failed. Local session still in progress.");
	}

	msg->DumpInfo();
#endif
}

void
netIceSession::Clear()
{
	if(this->Pending())
	{
		this->SetCanceled();
	}

#if __ASSERT
	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		netAssert(!m_Status[i]);
	}
#endif

	CancelPresenceQuery();
	CancelSymmetricInference();
	CancelSymmetricSocket();
	CancelCrossRelayCandidateCheck();

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
	// if we're not going to use our symmetric socket, then destroy it now.
	if(m_CxnMgr && (m_SymmetricSocket.m_SocketId != netSocket::INVALID_SOCKET_ID))
	{
		if(!this->Succeeded() || (m_NetAddr.GetTargetAddress() != m_SymmetricSocket.m_RemoteMappedAddr))
		{
			m_CxnMgr->GetSocketManager()->DestroySymmetricSocket(m_SymmetricSocket.m_SocketId);
		}
	}
#endif

	OUTPUT_ONLY(m_TunnelRequestId = NET_INVALID_TUNNEL_REQUEST_ID);
	m_SessionId = NET_INVALID_ICE_SESSION_ID;
	m_DestSessionId = NET_INVALID_ICE_SESSION_ID;
	m_GameSessionToken = 0;
	m_GameSessionId = 0;
	m_Role = ROLE_INVALID;
	m_RoleTieBreaker = 0;
	m_DirectOffersToken = 0;
	m_IndirectOffersToken = 0;
	m_CxnMgr = NULL;
	m_LocalPeerAddr.Clear();
	m_RemotePeerAddr.Clear();
	m_RemoteRelayAddr.Clear();
	m_LocalCandidateType = Candidate::INVALID_CANDIDATE;
	m_RemoteCandidateType = Candidate::INVALID_CANDIDATE;
	m_NetAddr.Clear();
	m_NumOffersSent = 0;
	m_NumOffersReceived = 0;
	m_NumAnswersSent = 0;
	m_NumAnswersReceived = 0;
	m_NumPingsSent = 0;
	m_NumPingsReceived = 0;
	m_NumPongsSent = 0;
	m_NumPongsReceived = 0;
	m_LastIndirectOfferSentTime = 0;
	m_IndirectOfferStatus = OFFER_STATUS_UNATTEMPTED;
	m_DirectOfferStatus = OFFER_STATUS_UNATTEMPTED;
	m_ChecklistTimer = netRetryTimer();
	m_StopWatch = netStopWatch();
	m_TerminationTimer = netTimeout();
	m_LocalCandidates.Clear();
	m_RemoteCandidates.Clear();
	m_CheckList.Clear();
	m_TriggeredQueue.Clear();
	m_ValidList.Clear();
	m_DiscoveredPairs.Clear();
	m_Blacklist.Clear();
	m_Transactor.Clear();
	m_SharedSocketPortMapper.Clear();
	m_LocalRelayRefreshState = LOCAL_RELAY_REFRESH_STATE_UNATTEMPTED;
	m_CrossRelayCheckState = CROSS_RELAY_STATE_UNATTEMPTED;
	m_RelayRouteCheckState = RELAY_ROUTE_CHECK_STATE_UNATTEMPTED;
	m_PresenceQueryState = PRESENCE_QUERY_STATE_UNATTEMPTED;
	if(m_PresenceQueryStatus.Pending())
	{
		rlPresence::CancelQuery(&m_PresenceQueryStatus);
	}
	m_PresenceQueryStatus.Reset();
	for(unsigned i = 0; i < COUNTOF(m_PresenceAttrs); ++i)
	{
		m_PresenceAttrs[i].Clear();
	}

	m_UnmatchedPublicIpState = UNMATCHED_PUBLIC_IP_STATE_NONE;
	m_SymmetricInference.Clear();
	m_SymmetricSocket.Clear();

	for(unsigned i = 0; i < COUNTOF(m_CrossRelayMappedAddrs); ++i)
	{
		m_CrossRelayMappedAddrs[i].Clear();
		if(m_CrossRelayMappedAddrStatus[i].Pending())
		{
			m_CrossRelayMappedAddrStatus[i].SetCanceled();
		}
		m_CrossRelayMappedAddrStatus[i].Reset();
	}

#if !__FINAL || defined(RSG_LEAN_CLIENT)
	m_ForceFail = false;
	m_ForceFailTimeoutMs = 0;
	m_ForceDelayMs = 0;
#endif

	m_State = STATE_INACTIVE;
	m_RemoteState = STATE_INACTIVE;
	m_FailReason = FAIL_REASON_NONE;

	m_RelayAssisted = false;
	m_Bilateral = false;
	m_HadRoleConflict = false;
	m_SentInitialOffers = false;
	m_RelayCommunicationSucceeded = false;
	m_RandomPortSymmetric = false;
	m_PortPreservingSymmetric = false;
	m_RemoteIsNonDeterministic = false;
	m_Canceled = false;
	m_SendDebugInfo = false;
	m_SendTelemetry = false;

	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		m_RelayAddrPtr[i] = NULL;
		m_NetAddrPtr[i] = NULL;
		m_Status[i] = NULL;
	}

#if !__NO_OUTPUT
	if(PARAM_neticesenddebuginfo.Get())
	{
		// tell the remote peer to send us debugging info after each ICE session
		m_SendDebugInfo = true;
	}
#endif
}

bool
netIceSession::GatherLocalCandidates()
{
	rtry
	{
		/*
			gather local candidates
			this is simplified because our local peer address already contains our host candidate
			(private ip/port) and server reflexive candidate (public ip/port as seen by the relay server).
		*/
		rverify(m_LocalPeerAddr.GetPrivateAddress().IsValid(), catchall, );

		LocalCandidate localHostCandidate(m_Logger, Candidate::HOST_CANDIDATE, m_LocalPeerAddr.GetPrivateAddress());
		rverify(m_LocalCandidates.AddCandidate(localHostCandidate), catchall, );

		/*
			4.1.3.  Eliminating Redundant Candidates

			Frequently, a server reflexive candidate and a host candidate will
			be redundant when the agent is not behind a NAT.
		*/
		if(m_LocalPeerAddr.GetPublicAddress().IsValid() && (m_LocalPeerAddr.GetPrivateAddress() != m_LocalPeerAddr.GetPublicAddress()))
		{
			LocalCandidate localServerReflexiveCandidate(m_Logger, Candidate::SERVER_REFLEXIVE_CANDIDATE, m_LocalPeerAddr.GetPublicAddress());
			m_LocalCandidates.AddCandidate(localServerReflexiveCandidate);
		}
		
		const unsigned numCandidates = m_LocalPeerAddr.GetNumP2pCandidates();
		for(unsigned i = 0; i < numCandidates; ++i)
		{
			netSocketAddress candidateAddr;
			netCandidateAddrType candidateAddrType;
			if(m_LocalPeerAddr.GetP2pCandidateAddress(i, candidateAddr, candidateAddrType))
			{
				LocalCandidate localCandidate(m_Logger, Candidate::SERVER_REFLEXIVE_CANDIDATE, candidateAddr);
				m_LocalCandidates.AddCandidate(localCandidate);
			}
		}

		return true;
	}
	rcatchall
	{

	}

	return false;
}

bool
netIceSession::GatherRemoteCandidates()
{
	rtry
	{
		/*
			gather remote candidates
			this is simplified because the peer address we got from matchmaking/invite/presence already contains
			the remote host candidate (his private ip/port) and remote server reflexive candidate
			(public ip/port as seen by the relay server)
		*/
		rverify(m_RemotePeerAddr.GetPrivateAddress().IsValid(), catchall, );

		RemoteCandidate remoteHostCandidate(m_Logger, Candidate::HOST_CANDIDATE, m_RemotePeerAddr.GetPrivateAddress());
		rverify(m_RemoteCandidates.AddCandidate(remoteHostCandidate), catchall, );

		if(m_RemoteRelayAddr.GetTargetAddress().IsValid() && (m_RemotePeerAddr.GetPrivateAddress() != m_RemoteRelayAddr.GetTargetAddress()))
		{
			// the host and server reflexive candidates can be the same if the peer is not behind a NAT
			RemoteCandidate remoteServerReflexiveCandidate(m_Logger, Candidate::SERVER_REFLEXIVE_CANDIDATE, m_RemoteRelayAddr.GetTargetAddress());
			m_RemoteCandidates.AddCandidate(remoteServerReflexiveCandidate);
		}

		const unsigned numCandidates = m_RemotePeerAddr.GetNumP2pCandidates();
		for(unsigned i = 0; i < numCandidates; ++i)
		{
			netSocketAddress candidateAddr;
			netCandidateAddrType candidateAddrType;
			if(m_RemotePeerAddr.GetP2pCandidateAddress(i, candidateAddr, candidateAddrType))
			{
				RemoteCandidate remoteCandidate(m_Logger, Candidate::SERVER_REFLEXIVE_CANDIDATE, candidateAddr);
				m_RemoteCandidates.AddCandidate(remoteCandidate);
			}
		}

		return true;
	}
	rcatchall
	{

	}

	return false;
}

unsigned
netIceSession::GetAdditionalLocalCandidates(netSocketAddress* candidates, unsigned maxAddrs)
{
	if((maxAddrs == 0) || (candidates == NULL))
	{
		return 0;
	}

	unsigned numCandidates = 0;

	// reserved candidates are addresses reserved via protocols such as UPnP, PCP, or NAT-PMP
	if(netIceTunneler::sm_AllowReservedCandidates)
	{
		if(numCandidates < maxAddrs)
		{
			// address reserved via PCP or NAT-PMP
			const netNatPcpInfo& natPcpInfo = netNatPcp::GetPcpInfo();
			const netSocketAddress& reservedAddr = natPcpInfo.GetReservedAddress();
			if(reservedAddr.IsValid() && reservedAddr.GetIp().IsV4() && (m_LocalCandidates.FindByTransportAddress(reservedAddr) == NULL))
			{
				// only allow private IPs if the tunable is enabled
				if(netIceTunneler::sm_AllowReservedRfc1918Candidates || !reservedAddr.GetIp().ToV4().IsRfc1918())
				{
					candidates[numCandidates++] = reservedAddr;
				}
			}
		}

		if(numCandidates < maxAddrs)
		{
			// address reserved via UPnP
			const netNatUpNpInfo& natUpNpInfo = netNatUpNp::GetUpNpInfo();
			u16 reservedPort = natUpNpInfo.GetReservedPort();
			if(reservedPort > 0)
			{
				netSocketAddress reservedAddr(natUpNpInfo.GetExternalIpAddress(), reservedPort);
				if(reservedAddr.IsValid() && reservedAddr.GetIp().IsV4() && (m_LocalCandidates.FindByTransportAddress(reservedAddr) == NULL))
				{
					// only allow private IPs if the tunable is enabled
					if(netIceTunneler::sm_AllowReservedRfc1918Candidates || !reservedAddr.GetIp().ToV4().IsRfc1918())
					{
						candidates[numCandidates++] = reservedAddr;
					}
				}

				if(numCandidates < maxAddrs)
				{
					netSocketAddress reservedAddr2(m_LocalPeerAddr.GetPublicAddress().GetIp(), reservedPort);
					if(reservedAddr2.IsValid() && reservedAddr2.GetIp().IsV4() && (reservedAddr2 != reservedAddr) && (m_LocalCandidates.FindByTransportAddress(reservedAddr2) == NULL))
					{
						if(netIceTunneler::sm_AllowReservedRfc1918Candidates || !reservedAddr2.GetIp().ToV4().IsRfc1918())
						{
							candidates[numCandidates++] = reservedAddr2;
						}
					}
				}
			}
		}
	}

	// cross relay address - the address our NAT mapped to our shared socket when we pinged the remote peer's relay server
	if(numCandidates < maxAddrs)
	{
		if(((m_CrossRelayCheckState == CROSS_RELAY_STATE_SUCCEEDED) && m_CrossRelayMappedAddrs[0].IsValid()) || netIceTunneler::sm_LastRelayPingResult.IsValid())
		{
			netSocketAddress relayMappedAddr;
			if(m_CrossRelayMappedAddrs[0].IsValid())
			{
				relayMappedAddr = m_CrossRelayMappedAddrs[0];
			}
			else if(netIceTunneler::sm_LastRelayPingResult.IsValid())
			{
				relayMappedAddr = netIceTunneler::sm_LastRelayPingResult;
			}

			if(relayMappedAddr.IsValid() && (m_LocalCandidates.FindByTransportAddress(relayMappedAddr) == NULL))
			{
				candidates[numCandidates++] = relayMappedAddr;
			}
		}
	}

	// peer reflexive addresses - addresses that other remote peers have seen from our packets
	if((numCandidates < maxAddrs) && (m_LocalPeerAddr.GetNatType() != NET_NAT_STRICT))
	{
		numCandidates += netIceTunneler::GetPeerReflexiveAddresses(&candidates[numCandidates], maxAddrs - numCandidates);
	}

	return numCandidates;
}

bool
netIceSession::SendDirectOffers()
{
	rtry
	{
#if !__FINAL
		if(PARAM_neticenodirectoffers.Get())
		{
			return false;
		}
#endif

		rcheck(netIceTunneler::sm_AllowDirectOffers, catchall, netIceDebug3("Direct offers are disabled via tunable"));

		/*
			Major optimization that differs from ICE. We don't always need to use the relay to
			tell the other peer to start NAT negotiating with us. When both The local and remote
			peers start NAT negotiating with each other at the same time, a connection can often
			succeed without any relay assistance.

			Connecting 32 players together would normally require pow(32,2) = 1024 relay-assisted
			ICE sessions, with each looking up relay addresses and relaying offer packets.
			With this optimization, we only send indirect offers when connecting unilaterally.

			*Except when a peer cannot be reached via its host candidate or server reflexive
			address. For example if a peer is behind an endpoint dependent mapping NAT (e.g.
			a symmetric NAT) or if there is a different NAT between the peer and the relay than
			between the two peers in the negotiation, or some other non-deterministic behaviour.
			These are relatively rare cases, and will be resolved by using the relay to initiate
			the negotiation. If *both* peers in the negotiation are behind such a topology, NAT
			negotiation will fail and we'll fall back to using the relay for all communications
			between these two peers.

			Also, there are several (albeit uncommon) cases where sending the offer directly 
			will work even when we're connecting to the remote peer unilaterally:
			1. When the remote peer is not behind a NAT (i.e. directly connected to the internet).
			2. When both sides are behind the same NAT (e.g. on the same LAN or VPN)
			3. When the remote peer is behind a NAT that does endpoint independent mapping with
			  endpoint independent filtering (aka full cone/promiscuous NAT), the port that the
			  remote peer's NAT has open with the relay server can be used to send him packets
			  from any other endpoint, so the offer will go through.
			4. When the two sides have recently opened a tunnel to each other (i.e. within
			  the last minute or two), the NAT mapping will still be open.

			We don't try to detect whether any of these conditions are met, we just try to 
			send the offer directly, and if it fails we send the offer over the relay.
		*/

		/*
			Avoid sending offers through a Strict NAT to avoid creating a (random) mapping on the NAT towards the other
			player. This is to improve NAT traversal success rate when the Strict NAT has port forwarding or UPnP
			enabled. Once our offer is sent via the relay, the remote player will send a ping directly to
			our 'canonical' port, and the Strict NAT's port forwarding rule will kick in, ensuring that further
			packets to the remote peer will use the canonical port instead of a random, unpredictable port.
			We still attempt to send the offer directly to the host candidate however, so we don't need the relay
			to initiate NAT traversal when the two endpoints are on the same LAN.
		*/
		bool onlyToHostCandidate = IsLocalStrict();

		for(unsigned i = 0; i < m_RemoteCandidates.GetCount(); ++i)
		{
			const RemoteCandidate& remoteCandidate = m_RemoteCandidates[i];

			if(onlyToHostCandidate && (remoteCandidate.GetType() != Candidate::HOST_CANDIDATE))
			{
				continue;
			}

			// the remote host candidate can be a public address if they're not behind a NAT
			const netIpAddress& ip = remoteCandidate.GetTransportAddress().GetIp();
			if(onlyToHostCandidate && ip.IsV4() && !ip.ToV4().IsRfc1918())
			{
				continue;
			}

			// offers are only sent via the main socket, not via symmetric sockets
			if(remoteCandidate.GetType() == Candidate::SYMMETRIC_SOCKET_CANDIDATE)
			{
				continue;
			}

			// make sure we're not sending to ourselves
			if((remoteCandidate.GetType() == Candidate::HOST_CANDIDATE) &&
				(m_LocalPeerAddr.GetPrivateAddress() == remoteCandidate.GetTransportAddress()))
			{
				netIceDebug3("The remote peer has the same private address as us. "
							 "Skip sending direct offer to their private address since "
							 "we would be sending a packet to ourselves.");
				continue;
			}

			unsigned txId = m_Transactor.NextId();

			netSocketAddress candidates[netIceSessionOffer::MAX_CANDIDATES_PER_OFFER];
			unsigned numCandidates = GetAdditionalLocalCandidates(candidates, COUNTOF(candidates));

			netPeerAddress localPeerAddr;
			if(!netPeerAddress::GetLocalPeerAddress(rlPresence::GetActingUserIndex(), &localPeerAddr))
			{
				localPeerAddr = m_LocalPeerAddr;
			}

			netIceSessionOffer msg(GetSessionId(),
									txId,
									localPeerAddr,
									m_RemotePeerAddr,
									candidates,
									numCandidates,
									m_RoleTieBreaker,
									m_Bilateral,
									m_SendDebugInfo,
									netNatDetector::GetLocalNatInfo().IsNonDeterministic(),
									m_SymmetricInference,
									m_SymmetricSocket,
									m_GameSessionToken,
									m_GameSessionId);

			/*
				it's less likely that a direct offer will succeed when connecting unilaterally since
				the remote peer is not sending packets to us at the same time. Direct offers are
				also less likely to succeed if either side is on a Strict NAT, so we speed up the
				test by retrying half as many times as normal. Note that this also opens a NAT
				mapping towards the remote peer which is helpful since it gives the remote peer
				a path over which to send answer packets directly even if the offer is sent via
				relay (see comments in SendAnswer()).
			*/
			bool eitherIsStrict = IsLocalStrict() || (m_RemotePeerAddr.GetNatType() == NET_NAT_STRICT);
			unsigned dividend = (!m_Bilateral || eitherIsStrict) ? 2 : 1;

			netAddress destAddr(remoteCandidate.GetTransportAddress());
			if(m_Transactor.SendRequest(txId, m_RemotePeerAddr.GetPeerKey(), destAddr, msg, MAX_RETRIES_PER_TRANSACTION / dividend, TRANSACTION_RETRY_INTERVAL_MS, &m_DirectOffersToken))
			{
				m_NumOffersSent++;
			}
		}

		rcheck(m_Transactor.DoesTransactionExist(&m_DirectOffersToken), catchall, );

		m_DirectOfferStatus = OFFER_STATUS_SENT;

		return true;
	}
	rcatchall
	{

	}

	m_DirectOfferStatus = OFFER_STATUS_FAILED_TO_SEND;

	return false;
}

bool
netIceSession::CanSendViaRelay(FailReason& failReason) const
{
	rtry
	{
		failReason = FAIL_REASON_NONE;

		rcheck(netRelay::GetRelayServerAddress().IsValid(), catchall,
				failReason = FAIL_REASON_INDIRECT_OFFER_NO_LOCAL_RELAY_ADDR;
				netIceWarning("We don't have a valid relay server address. Relay server may be down."));

		rcheck(netRelay::GetMyRelayAddress().IsRelayServerAddr(), catchall,
				failReason = FAIL_REASON_INDIRECT_OFFER_NO_LOCAL_RELAY_MAPPED_ADDR;
				netIceWarning("We don't have a valid relay address. Our relay server may not be responding to pings."));

		rcheck(m_RemoteRelayAddr.IsRelayServerAddr(), catchall,
				failReason = FAIL_REASON_INDIRECT_OFFER_NO_REMOTE_RELAY;
				netIceWarning("Remote peer's address isn't a valid relay address. Their relay server may not be responding to pings."));

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netIceSession::SendIndirectOffer(FailReason& failReason)
{
	rtry
	{
		/*
			optimized out the relay/mapped address lookup by having the relay address
			included in the peer address. This significantly reduces server load by not
			having to execute the NetAddrByPeerAddr query for every NAT traversal.
		*/

		failReason = FAIL_REASON_NONE;

		if(m_CheckList.GetCount() == 0)
		{
			rverify(GatherLocalCandidates(), catchall, );
			rverify(GatherRemoteCandidates(), catchall, );
			rverify(CreateCheckList(), catchall, );
			AddRemoteSymmetricSocketCandidates();
			AddRemoteSymmetricInferenceCandidates();
		}

		rcheckall(CanSendViaRelay(failReason));

		m_RelayAssisted = true;
		unsigned txId = m_Transactor.NextId();

		netSocketAddress candidates[netIceSessionOffer::MAX_CANDIDATES_PER_OFFER];
		unsigned numCandidates = GetAdditionalLocalCandidates(candidates, COUNTOF(candidates));

		netPeerAddress localPeerAddr;
		if(!netPeerAddress::GetLocalPeerAddress(rlPresence::GetActingUserIndex(), &localPeerAddr))
		{
			localPeerAddr = m_LocalPeerAddr;
		}

		netIceSessionOffer msg(GetSessionId(),
								txId,
								localPeerAddr,
								m_RemotePeerAddr,
								candidates,
								numCandidates,
								m_RoleTieBreaker,
								m_Bilateral,
								m_SendDebugInfo,
								netNatDetector::GetLocalNatInfo().IsNonDeterministic(),
								m_SymmetricInference,
								m_SymmetricSocket,
								m_GameSessionToken,
								m_GameSessionId);

		rverify(m_Transactor.SendRequest(txId, m_RemotePeerAddr.GetPeerKey(), m_RemoteRelayAddr, msg, MAX_RETRIES_PER_TRANSACTION, TRANSACTION_RELAY_RETRY_INTERVAL_MS, &m_IndirectOffersToken), catchall, );

		m_NumOffersSent++;

		m_LastIndirectOfferSentTime = sysTimer::GetSystemMsTime();
		m_IndirectOfferStatus = OFFER_STATUS_SENT;

		return true;
	}
	rcatchall
	{

	}

	m_IndirectOfferStatus = OFFER_STATUS_FAILED_TO_SEND;

	if(failReason == FAIL_REASON_NONE)
	{
		failReason = FAIL_REASON_COULD_NOT_SEND_INDIRECT_OFFER;
	}

	return false;
}

bool
netIceSession::SendAnswer(unsigned sourceSessionId, unsigned destSessionId, unsigned transactionId, const netAddress& sender)
{
	rtry
	{
		netSocketAddress candidates[netIceSessionAnswer::MAX_CANDIDATES_PER_ANSWER];
		unsigned numCandidates = GetAdditionalLocalCandidates(candidates, COUNTOF(candidates));

		netIceSessionAnswer msg(sourceSessionId,
								destSessionId,
								transactionId,
								m_LocalPeerAddr,
								candidates,
								numCandidates,
								m_Role == ROLE_CONTROLLING,
								m_SendDebugInfo,
								netNatDetector::GetLocalNatInfo().IsNonDeterministic());

		netIceDebug1("Sending answer to:" NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(sender), transactionId);

		rcheck(m_CxnMgr->IsInitialized(), catchall, netWarning("m_CxnMgr->IsInitialized() is false"));

		netP2pCryptContext context;
		context.Init(m_RemotePeerAddr.GetPeerKey(), NET_P2P_CRYPT_TUNNELING_PACKET);

		if(!m_CxnMgr->SendTunnelingPacket(sender, msg, context))
		{
			netIceError("Failed to send answer to:" NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(sender), transactionId);
		}

		// if we received the offer over the relay, we send the answer back over the relay.
		// But we've found that the answer often doesn't get received (unreliable relay).
		// So we also try to send the answer directly to them. This trick works because
		// the remote peer will have first attempted to send the offer directly to us,
		// opening a NAT mapping, which we should be able to send to if they're not
		// behind a symmetric NAT.
		if(netIceTunneler::sm_AllowDirectAnswers && sender.IsRelayServerAddr() && !IsLocalStrict() && !IsSymmetricInferenceInProgress())
		{
			for(unsigned i = 0; i < m_RemoteCandidates.GetCount(); ++i)
			{
				const RemoteCandidate& remoteCandidate = m_RemoteCandidates[i];

				if(remoteCandidate.GetType() == Candidate::SYMMETRIC_SOCKET_CANDIDATE)
				{
					continue;
				}

				if((remoteCandidate.GetType() == Candidate::HOST_CANDIDATE) &&
					(m_LocalPeerAddr.GetPrivateAddress() == remoteCandidate.GetTransportAddress()))
				{
					netIceDebug3("The remote peer has the same private address as us. "
								 "Skip sending answer to their private address since "
								 "we would be sending a packet to ourselves.");

					continue;
				}

				netAddress destAddr(remoteCandidate.GetTransportAddress());
				netIceDebug1("Sending answer to:" NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(destAddr), transactionId);
				if(!m_CxnMgr->SendTunnelingPacket(destAddr, msg, context))
				{
					netIceError("Failed to send answer to:" NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(destAddr), transactionId);
				}
			}
		}

		m_NumAnswersSent++;

		return true;
	}
	rcatchall
	{
	}

	return false;
}

bool
netIceSession::SendDebugInfo(const char* metrics)
{
	rtry
	{
		// we don't want to send debug info from an output-enabled testing build to a retail final build.
		rcheck(!__FINAL_LOGGING, catchall, );
		rcheck(m_RemoteRelayAddr.IsValid(), catchall, );
		rcheck(netRelay::GetMyRelayAddress().IsValid(), catchall, );
		netSocketAddress candidates[netIceTunneler::MAX_PEER_REFLEXIVE_CANDIDATES];
		unsigned numHits[netIceTunneler::MAX_PEER_REFLEXIVE_CANDIDATES];
		u64 timestamps[netIceTunneler::MAX_PEER_REFLEXIVE_CANDIDATES];

		for(unsigned i = 0; i < netIceTunneler::MAX_PEER_REFLEXIVE_CANDIDATES; ++i)
		{
			candidates[i] = netIceTunneler::m_PeerReflexiveCandidates[i].m_Addr;
			numHits[i] = netIceTunneler::m_PeerReflexiveCandidates[i].m_NumHits;
			timestamps[i] = netIceTunneler::m_PeerReflexiveCandidates[i].m_Timestamp;
		}

		rlRosGeoLocInfo geolocInfo;
		rlRos::GetGeoLocInfo(&geolocInfo);

		netIceSessionDebugInfo msg(GetSessionId(),
									GetDestSessionId(),
									m_LocalPeerAddr,
									metrics,
									candidates,
									numHits,
									timestamps,
									netIceTunneler::MAX_PEER_REFLEXIVE_CANDIDATES,
									m_CrossRelayMappedAddrs[0],
									netIceTunneler::m_NumFailedSessions,
									netIceTunneler::m_NumSuccessfulSessions,
									geolocInfo.m_RegionCode,
									geolocInfo.m_CountryCode,
									netNatDetector::GetLocalNatInfo(),
									netNatUpNp::GetUpNpInfo(),
									netNatPcp::GetPcpInfo(),
									SymmetricSocket::sm_NumNominated);

		rcheck(m_CxnMgr->IsInitialized(), catchall, netWarning("m_CxnMgr->IsInitialized() is false"));

		netP2pCryptContext context;
		context.Init(m_RemotePeerAddr.GetPeerKey(), NET_P2P_CRYPT_TUNNELING_PACKET);

		// debug info is sent via relay to make sure we receive it even if NAT traversal fails
		rverify(m_CxnMgr->SendTunnelingPacket(m_RemoteRelayAddr,
											  msg,
											  context),
				catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netIceSession::CreateCheckList()
{
	rtry
	{
		netIceDebug3("Creating checklist");

		const LocalCandidate* localHostCandidate = NULL;
		for(unsigned i = 0; i < m_LocalCandidates.GetCount(); ++i)
		{
			if(m_LocalCandidates[i].GetType() == Candidate::HOST_CANDIDATE)
			{
				localHostCandidate = &m_LocalCandidates[i];
				break;
			}
		}
		rverify(localHostCandidate, catchall, );

		/*
			gather remote candidates
			this is simplified because the peer address we got from matchmaking/invite/presence already contains
			the remote host candidate (private ip/port) and remote server reflexive candidate
			(public ip/port as seen by the relay server)
		*/
		RemoteCandidate remoteHostCandidate(m_Logger, Candidate::HOST_CANDIDATE, m_RemotePeerAddr.GetPrivateAddress());

		/*
			if the remote peer has the same private address as us, then he's behind a different NAT.
			Don't try sending packets to that private address since we'd be sending packets to ourselves.
			Note that if the remote peer address has the same IP as us but a different port, it could
			be another instance of the executable running on the same PC, which we support for testing.
		*/
		/*
			Note that if we have the same public IP as the remote peer, it doesn't necessarily mean
			we can communicate via our private addresses. Consider multi-level NAT topologies, where
			two peers A and B are behind different NATs Na and Nb, but both Na and Nb are themselves
			behind a common NAT Nc. Some corporations and ISPs use this topology. If Nc supports
			hairpinning (and it likely will in such a topology), A and B can communicate by sending
			packets to each other's public address. This can also work on simpler topologies where
			only one NAT is involved but A and B are on different subnets of the private network.
		*/
		if(localHostCandidate->GetTransportAddress() != remoteHostCandidate.GetTransportAddress())
		{
			CandidatePair* pair = m_DiscoveredPairs.FindMatchingPair(CandidatePair(m_Logger, *localHostCandidate, remoteHostCandidate));
			if(pair == NULL)
			{
				pair = m_DiscoveredPairs.AddCandidatePair(CandidatePair(m_Logger, *localHostCandidate, remoteHostCandidate));
			}

			if(pair)
			{
				m_CheckList.AddCandidatePair(pair);
			}
		}

		if(m_RemoteRelayAddr.GetTargetAddress().IsValid() && (m_RemotePeerAddr.GetPrivateAddress() != m_RemoteRelayAddr.GetTargetAddress()))
		{
			// the host and server reflexive candidates can be the same if the peer is not behind a NAT
			RemoteCandidate remoteServerReflexiveCandidate(m_Logger, Candidate::SERVER_REFLEXIVE_CANDIDATE, m_RemoteRelayAddr.GetTargetAddress());

			CandidatePair* pair = m_DiscoveredPairs.FindMatchingPair(CandidatePair(m_Logger, *localHostCandidate, remoteServerReflexiveCandidate));
			if(pair == NULL)
			{
				pair = m_DiscoveredPairs.AddCandidatePair(CandidatePair(m_Logger, *localHostCandidate, remoteServerReflexiveCandidate));
			}

			if(pair)
			{
				m_CheckList.AddCandidatePair(pair);
			}
		}

		const unsigned numCandidates = m_RemotePeerAddr.GetNumP2pCandidates();
		for(unsigned i = 0; i < numCandidates; ++i)
		{
			netSocketAddress candidateAddr;
			netCandidateAddrType candidateAddrType;
			if(m_RemotePeerAddr.GetP2pCandidateAddress(i, candidateAddr, candidateAddrType))
			{
				AddRemoteCandidateToChecklist(Candidate::SERVER_REFLEXIVE_CANDIDATE, candidateAddr);
			}
		}

		if(netIceTunneler::sm_AllowSameHostCandidate && (m_CheckList.GetCount() == 0) && (localHostCandidate->GetTransportAddress() == remoteHostCandidate.GetTransportAddress()))
		{
			CandidatePair* pair = m_DiscoveredPairs.FindMatchingPair(CandidatePair(m_Logger, *localHostCandidate, remoteHostCandidate));
			if(pair == NULL)
			{
				pair = m_DiscoveredPairs.AddCandidatePair(CandidatePair(m_Logger, *localHostCandidate, remoteHostCandidate));
			}

			if(pair)
			{
				m_CheckList.AddCandidatePair(pair);
			}
		}

		rverify(m_CheckList.GetCount() > 0, catchall, );

		if(netIceTunneler::sm_AllowCanonicalCandidate)
		{
			netSocketAddress canonicalAddr(m_RemoteRelayAddr.GetTargetAddress().GetIp(), netRelay::GetRequestedPort());
			AddRemoteCandidateToChecklist(Candidate::CANONICAL_CANDIDATE, canonicalAddr);
		}

		// put the first candidate pair into the waiting state
		m_CheckList.GetCandidatePair(0)->SetState(CandidatePair::STATE_WAITING);

		return true;
	}
	rcatchall
	{
	}

	return false;
}

bool
netIceSession::IsLocalStrict() const
{
	return (m_LocalPeerAddr.GetNatType() == NET_NAT_STRICT) || netNatDetector::GetLocalNatInfo().IsNonDeterministic();
}

bool
netIceSession::PassedOfferAnswerExchange() const
{
	return (m_NumAnswersReceived > 0) ||
			(m_NumPingsReceived > 0) ||
			(m_NumPongsReceived > 0) ||
			(m_NumPingsSent > 0) ||
			(m_NumPongsSent > 0);
}

bool
netIceSession::IndirectOfferTimedOut()
{
	// we only want to fail the ICE session if we haven't made any progress
	if((m_NumAnswersReceived == 0) &&
		(m_NumPingsReceived == 0) &&
		(m_NumPongsReceived == 0) &&
		(m_NumPingsSent == 0) &&
		(m_NumPongsSent == 0) &&
		(m_Role == ROLE_CONTROLLING))
	{
		Complete(COMPLETE_FAILED, FAIL_REASON_INDIRECT_OFFER_TIMED_OUT, NULL);
		return true;
	}
	
	netIceDebug3("IndirectOfferTimedOut :: not failing the session due to current progress");

	return false;
}

bool
netIceSession::CanReceiveOffer(const netIceSessionOffer* msg, const netAddress& UNUSED_PARAM(sender))
{
	// We need to determine whether this offer is from the same remote peer with whom we're negotiating.
	const bool sameRemotePeer = m_RemotePeerAddr.IsValid() && (msg->m_SourcePeerAddr.GetPeerId() == m_RemotePeerAddr.GetPeerId());

#if !__NO_OUTPUT
	if(sameRemotePeer && ((m_DestSessionId != NET_INVALID_ICE_SESSION_ID) && (m_DestSessionId != msg->m_SourceSessionId)))
	{
		netIceDebug("The remote peer is opening multiple tunnels to us simultaneously. Will fork a new session.");
	}
#endif

	return sameRemotePeer && ((m_DestSessionId == NET_INVALID_ICE_SESSION_ID) || (m_DestSessionId == msg->m_SourceSessionId));
}

void
netIceSession::ResolveRoleConflict(const netIceSessionOffer* msg, const netAddress& sender)
{
	// we need to take the controlling role if we need to infer our port, create a symmetric socket, or if we need to do a cross-relay check.
	// if we receive an offer over the relay, then the direct offer timed out on the remote peer, so do a cross-relay check first.
	// if the remote peer is on a Strict NAT, they will always send an offer indirectly, so we may not want to do a cross-relay check
	// in that case (to reduce server load). We have a tunable to enable/disable the cross-relay check in that case.

	// we can attempt a symmetric socket even after we're pass the offer/answer exchange.
	bool isSymmetricSocketInProgress = IsSymmetricSocketInProgress() || (NeedsSymmetricSocket(!sender.IsRelayServerAddr()) && StartSymmetricSocket());
	bool needControllingRole = false;
	if(!PassedOfferAnswerExchange())
	{
		bool isSymmetricInferenceInProgress = IsSymmetricInferenceInProgress() || (NeedsSymmetricInference() && StartSymmetricInference());

		bool remotePeerIsStrict = m_RemotePeerAddr.GetNatType() == NET_NAT_STRICT;
		bool directOfferTimedOut = sender.IsRelayServerAddr();
		bool allowCrossRelayCheck = netIceTunneler::sm_AllowCrossRelayCheckDirectOfferTimedOut && directOfferTimedOut;

		if(allowCrossRelayCheck && remotePeerIsStrict)
		{
			allowCrossRelayCheck = netIceTunneler::sm_AllowCrossRelayCheckRemoteStrict;
		}

		bool isCrossRelayCandidateCheckInProgress = (IsCrossRelayCandidateCheckInProgress() || (allowCrossRelayCheck && NeedsCrossRelayCandidateCheck() && StartCrossRelayCandidateCheck()));

		needControllingRole = isSymmetricInferenceInProgress || isSymmetricSocketInProgress || isCrossRelayCandidateCheckInProgress;
	}

	if(needControllingRole)
	{
		// we will tell the remote peer to sit tight until we have the results.
		if(m_Role != ROLE_CONTROLLING)
		{
			netIceDebug3("Need the controlling role. Gaining the controlling role.");
		}
		else
		{
			netIceDebug3("Need the controlling role. Staying in the controlling role.");
		}
		m_Role = ROLE_CONTROLLING;
		m_HadRoleConflict = true;
		m_RoleTieBreaker = msg->m_RoleTieBreaker + 1;
	}
	else if(m_Role == ROLE_CONTROLLING)
	{
		m_HadRoleConflict = true;
		netIceDebug3("We started as the offerer and we received an offer from our remote peer. Resolving role conflict..."
					 "My tie breaker:0x%016" I64FMT "x, their tie breaker:0x%016" I64FMT "x", m_RoleTieBreaker, msg->m_RoleTieBreaker);

		if(m_RoleTieBreaker < msg->m_RoleTieBreaker)
		{
			netIceDebug3("Switching to the controlled role.");
			m_Role = ROLE_CONTROLLED;
		}
		else if(m_RoleTieBreaker == msg->m_RoleTieBreaker)
		{
			// in the off chance both sides chose the same random tie breaker
			netIceDebug3("We both chose the same random tie breaker. Resolving...");

			if(m_LocalPeerAddr.GetPeerId() < m_RemotePeerAddr.GetPeerId())
			{
				netIceDebug3("Switching to the controlled role");
				m_Role = ROLE_CONTROLLED;
			}
			else
			{
				netIceDebug3("Staying in the controlling role");
			}
		}
		else
		{
			netIceDebug3("Staying in the controlling role");
		}
	}
	else
	{
		if(m_Role == ROLE_INVALID)
		{
			netIceDebug3("Assuming the controlled role.");
		}
		else
		{
			netIceDebug3("Remaining in the controlled role.");
		}

		m_Role = ROLE_CONTROLLED;
	}
}

bool
netIceSession::ReceiveOffer(const netIceSessionOffer* msg, const netAddress& sender)
{
	FailReason failReason = FAIL_REASON_RECEIVE_OFFER;

	rtry
	{
#if !__FINAL || defined(RSG_LEAN_CLIENT)
		if(m_ForceFail)
		{
			return false;
		}

		if(PARAM_neticeforcesymmetricsocket.Get())
		{
			if(sender.GetTargetAddress() == m_RemotePeerAddr.GetPrivateAddress())
			{
				netIceDebug2("-neticeforcesymmetricsocket is enabled, ignoring offer from " NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);
				return false;
			}
		}
#endif

		m_NumOffersReceived++;

		netIceDebug1("Received offer from " NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);

		if(sender.IsDirectAddr() && m_Blacklist.IsBlacklisted(sender.GetTargetAddress()))
		{
			netIceDebug2("Sender is blacklisted, discarding offer.");
			return false;
		}

#if !__NO_OUTPUT
		if(msg->m_DestPeerAddr.GetPrivateAddress() != m_LocalPeerAddr.GetPrivateAddress())
		{
			netIceDebug3("Remote peer thinks my private address is: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_DestPeerAddr.GetPrivateAddress()));
		}

		if(msg->m_DestPeerAddr.GetPublicAddress() != m_LocalPeerAddr.GetPublicAddress())
		{
			netIceDebug3("Remote peer thinks my public address is: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_DestPeerAddr.GetPublicAddress()));
		}

		if(msg->m_DestPeerAddr.GetRelayAddress() != m_LocalPeerAddr.GetRelayAddress())
		{
			netIceDebug3("Remote peer thinks my relay address is: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_DestPeerAddr.GetRelayAddress()));
		}

		if(msg->m_SourcePeerAddr.GetPublicAddress() != m_RemoteRelayAddr.GetTargetAddress())
		{
			netIceDebug("Remote peer changed public address from " NET_ADDR_FMT " to " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_RemoteRelayAddr.GetTargetAddress()), NET_ADDR_FOR_PRINTF(msg->m_SourcePeerAddr.GetPublicAddress()));
		}

		if(msg->m_SourcePeerAddr.GetRelayAddress().GetProxyAddress() != m_RemoteRelayAddr.GetProxyAddress())
		{
			netIceDebug("Remote peer changed relay address from " NET_ADDR_FMT " to " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_RemoteRelayAddr.GetProxyAddress()), NET_ADDR_FOR_PRINTF(msg->m_SourcePeerAddr.GetRelayAddress().GetProxyAddress()));
		}

		if(msg->m_SourcePeerAddr.GetPrivateAddress() != m_RemotePeerAddr.GetPrivateAddress())
		{
			netIceDebug("Remote peer changed private address from " NET_ADDR_FMT " to " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_RemotePeerAddr.GetPrivateAddress()), NET_ADDR_FOR_PRINTF(msg->m_SourcePeerAddr.GetPrivateAddress()));
		}
#endif

		if(sender.IsRelayServerAddr())
		{
			m_RelayAssisted = true;
			m_RelayCommunicationSucceeded = true;
		}

#if !__NO_OUTPUT
		if(m_RemoteRelayAddr != msg->m_SourcePeerAddr.GetRelayAddress())
		{
			netIceDebug("Relay address from netIceSessionOffer: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_SourcePeerAddr.GetRelayAddress()));
		
			if(m_RemoteRelayAddr.GetProxyAddress().IsValid() && m_RemoteRelayAddr.GetRelayToken().IsValid() && !m_RemoteRelayAddr.GetTargetAddress().IsValid())
			{
				netIceDebug("Exchanging secure relay address: " NET_ADDR_FMT " with real address: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_RemoteRelayAddr), NET_ADDR_FOR_PRINTF(msg->m_SourcePeerAddr.GetRelayAddress()));
			}
		}
#endif

		const netAddress& relayAddr = msg->m_SourcePeerAddr.GetRelayAddress();

		bool remoteRelayAddrChanged = false;
		if((m_RemoteRelayAddr.GetProxyAddress() != relayAddr.GetProxyAddress()) ||
			(m_RemoteRelayAddr.IsRelayServerAddr() && relayAddr.IsRelayServerAddr() && (m_RemoteRelayAddr.GetRelayToken() != relayAddr.GetRelayToken())) ||
			((m_RemoteRelayAddr.GetTargetAddress().IsValid() && relayAddr.GetTargetAddress().IsValid()) &&
			(m_RemoteRelayAddr.GetTargetAddress() != relayAddr.GetTargetAddress())))
		{
			remoteRelayAddrChanged = true;
		}

		m_RemoteRelayAddr = relayAddr;

		if(msg->m_SendDebugInfo)
		{
			m_SendDebugInfo = true;
		}

		if(msg->m_NonDeterministic)
		{
			m_RemoteIsNonDeterministic = true;
			netIceDebug2("The remote peer is behind a non-deterministic NAT");
		}

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
		// handle remote symmetric socket data
		if(msg->m_SymmetricSocket.m_MappedAddr.IsValid())
		{
			m_SymmetricSocket.m_RemoteMappedAddr = msg->m_SymmetricSocket.m_MappedAddr;

			netIceDebug1("ReceiveOffer :: Remote peer created a symmetric socket with mapped address: " NET_ADDR_FMT,
						NET_ADDR_FOR_PRINTF(msg->m_SymmetricSocket.m_MappedAddr));

			// check if we already have a symmetric socket
			if(m_SymmetricSocket.m_State == SymmetricSocket::STATE_READY_FOR_ASSOCIATION)
			{
				netIceDebug1("ReceiveOffer :: Our symmetric socket is ready for association. Associating now.");
				bool isAssociated = m_CxnMgr->GetSocketManager()->AssociateSymmetricSocket(m_SymmetricSocket.m_SocketId, m_SymmetricSocket.m_RemoteMappedAddr);
				if(isAssociated)
				{
					m_SymmetricSocket.m_State = SymmetricSocket::STATE_ASSOCIATED;
				}
			}
		}
#endif

		// handle Symmetric Inference data
		if((msg->m_SymmetricInference.m_SymmetricMappedAddrs[0].GetPort() != 0) ||
			(msg->m_SymmetricInference.m_SymmetricMappedAddrs[1].GetPort() != 0) ||
			(msg->m_SymmetricInference.m_SymmetricInferredPort != 0) ||
			(msg->m_SymmetricInference.m_PortMapperPort != 0))
		{
			const u16 port0 = msg->m_SymmetricInference.m_SymmetricMappedAddrs[0].GetPort();
			const u16 port1 = msg->m_SymmetricInference.m_SymmetricMappedAddrs[1].GetPort();

			u16 lowerBound = port0;
			u16 upperBound = port1;

			if(lowerBound > upperBound)
			{
				lowerBound = port1;
				upperBound = port0;
			}

			int difference = upperBound - lowerBound;

			bool symmetric = difference != 0;
			bool portPreserving = symmetric && ((lowerBound == msg->m_SymmetricInference.m_PortMapperPort) || (upperBound == msg->m_SymmetricInference.m_PortMapperPort));
			bool portContiguous = symmetric && (!portPreserving && (difference <= 4));
			bool randomPort = symmetric && (!portContiguous && !portPreserving);

#if !__NO_OUTPUT
			char portAllocationStrategy[32] = {0};
			if(!symmetric)
			{
				safecpy(portAllocationStrategy, "Not-Symmetric");
			}
			else if(portPreserving)
			{
				safecpy(portAllocationStrategy, "Port-Preserving");
			}
			else if(portContiguous)
			{
				safecpy(portAllocationStrategy, "Port-Contiguous");
			}
			else
			{
				safecpy(portAllocationStrategy, "Port-Random");
			}

			netIceDebug3("Remote symmetric inference result: %s, Port Mapper private Port: %d, Mapped Port 0: %d, Mapped Port 1: %d, Diff: %d, Symmetric inferred port: %d",
						portAllocationStrategy, msg->m_SymmetricInference.m_PortMapperPort, port0, port1, port1 - port0, msg->m_SymmetricInference.m_SymmetricInferredPort);
#endif

			/*
				Testing in public sessions, I found a potential improvement for the Symmetric Inference technique, which is used to connect
				a Moderate to a Strict when the Strict uses a contiguous port allocation strategy. It sometimes infers the next port
				incorrectly (off by one) if the test packets leave the NAT out-of-order. When we get the upper and lower bound for the ports
				the remote peer is using, if the difference is 1, then use lower bound - 1 and upper bound + 1 as candidates.
			*/
			m_SymmetricInference.m_RemoteSymmetricInferredPort[0] = msg->m_SymmetricInference.m_SymmetricInferredPort;
			m_SymmetricInference.m_RemoteSymmetricInferredPort[1] = 0;
			if((msg->m_SymmetricInference.m_SymmetricInferredPort == 0) && portContiguous && (difference == 1))
			{
				m_SymmetricInference.m_RemoteSymmetricInferredPort[0] = lowerBound - 1;
				m_SymmetricInference.m_RemoteSymmetricInferredPort[1] = upperBound + 1;
			}

			/*
				If symmetric inference was performed on a non-Strict NAT, use the relay mapped port(s) as candidates. Symmetric inference can
				discover that a non-deterministic NAT has turned symmetric and infer the port if it is using a contiguous port allocation,
				or it can discover which port is currently open on a non-symmetric, but non-deterministic NAT.
			*/
			if(!portContiguous && (m_RemotePeerAddr.GetNatType() != NET_NAT_STRICT))
			{
				m_SymmetricInference.m_RemoteSymmetricInferredPort[0] = lowerBound;

				if(lowerBound != upperBound)
				{
					m_SymmetricInference.m_RemoteSymmetricInferredPort[1] = upperBound;
				}
			}

			m_PortPreservingSymmetric = m_PortPreservingSymmetric || portPreserving;
			m_RandomPortSymmetric = m_RandomPortSymmetric || randomPort;
		}

		/*
			We need to decide who should have the controlling role.
		*/
		ResolveRoleConflict(msg, sender);

		//netAssert(m_DestSessionId == NET_INVALID_ICE_SESSION_ID || (m_DestSessionId == msg->m_SourceSessionId));

		if(m_DestSessionId == NET_INVALID_ICE_SESSION_ID)
		{
			netIceDebug3("Setting remote session ID: %u", msg->m_SourceSessionId);
		}
		else if(m_DestSessionId != msg->m_SourceSessionId)
		{
			netIceWarning("Changing remote session ID: %u", msg->m_SourceSessionId);
		}

		m_DestSessionId = msg->m_SourceSessionId;
		netAssert(m_DestSessionId != NET_INVALID_ICE_SESSION_ID);

		SendRelayRouteCheckRequest();

		if(m_Role == ROLE_CONTROLLED)
		{
			// if we created a symmetric socket and we received an offer from the remote peer and
			// they haven't attempted to create their symmetric socket, then discard this offer.
			// Otherwise they will think we're ready to start the pinging phase.
			// Once they receive our offer notifying them of our symmetric socket, they will
			// send another offer with their symmetric socket info.
			if((msg->m_SymmetricSocket.m_State == SymmetricSocket::STATE_UNATTEMPTED)
				&& (m_SymmetricSocket.m_State == SymmetricSocket::STATE_READY_FOR_ASSOCIATION)
				&& ((m_Transactor.DoesTransactionExist(&m_IndirectOffersToken)) ||
				(m_Transactor.DoesTransactionExist(&m_DirectOffersToken))))
			{
				netIceDebug1("ReceiveOffer :: dropping netIceSessionOffer - waiting for remote peer to attempt symmetric socket");
				return true;
			}
			
			if(remoteRelayAddrChanged)
			{
				if(m_Transactor.DoesTransactionExist(&m_IndirectOffersToken))
				{
					netIceDebug("Relay address changed. Removing existing indirect offer transactions.");
					m_Transactor.RemoveTransactionsByUserData(&m_IndirectOffersToken);
				}
			}

			if(m_ChecklistTimer.IsStopped())
			{
				/*
					5.  Receiving the Initial Offer

					When an agent receives an initial offer, it will check if the offerer
					supports ICE, determine its own role, gather candidates, prioritize
					them, choose default candidates, encode and send an answer, and for
					full implementations, form the check lists and begin connectivity
					checks.
				*/
				if(m_CheckList.GetCount() == 0)
				{
					// if we started as the offerer, we already created our checklist
					rverify(GatherLocalCandidates(), catchall, failReason = FAIL_REASON_RCV_OFFER_GLC);
					rverify(GatherRemoteCandidates(), catchall, failReason = FAIL_REASON_RCV_OFFER_GRC);
					rverify(CreateCheckList(), catchall, failReason = FAIL_REASON_RCV_OFFER_CHECKLIST);
				}
				AddRemoteSymmetricSocketCandidates();
				AddRemoteSymmetricInferenceCandidates();
				AddRemoteCandidates(msg, sender);
				rverify(SendAnswer(GetSessionId(), GetDestSessionId(), msg->m_TransactionId, sender), catchall, failReason = FAIL_REASON_RCV_OFFER_SEND_ANSWER);

				m_ChecklistTimer.InitMilliseconds((IsLocalStrict() && (m_RemotePeerAddr.GetNatType() != NET_NAT_STRICT)) ? CHECK_START_DELAY_STRICT_NAT_MS : CHECK_INTERVAL_MS);

				if(!IsLocalStrict())
				{
					m_ChecklistTimer.ForceRetry();

					// call update here so the agent performs a connectivity 
					// check the moment the offer/answer exchange has been done
					Update();
				}
			}
			else
			{
				netIceDebug3("Offer is for a session already in progress");

				AddRemoteSymmetricSocketCandidates();
				AddRemoteSymmetricInferenceCandidates();
				AddRemoteCandidates(msg, sender);
				rverify(SendAnswer(GetSessionId(), GetDestSessionId(), msg->m_TransactionId, sender), catchall, failReason = FAIL_REASON_RCV_OFFER_SEND_ANSWER);
			}
		}
		else
		{
			AddRemoteSymmetricSocketCandidates();
			AddRemoteSymmetricInferenceCandidates();
			AddRemoteCandidates(msg, sender);
			rverify(SendAnswer(GetSessionId(), GetDestSessionId(), msg->m_TransactionId, sender), catchall, failReason = FAIL_REASON_RCV_OFFER_SEND_ANSWER);

			if(remoteRelayAddrChanged)
			{
				if(m_Transactor.DoesTransactionExist(&m_IndirectOffersToken))
				{
					netIceDebug("Relay address changed. Removing existing indirect offer transactions and restarting with new remote relay address.");

					m_Transactor.RemoveTransactionsByUserData(&m_IndirectOffersToken);

					FailReason failReason;
					if(SendIndirectOffer(failReason) == false)
					{
						netIceDebug("Failed to start new indirect offer transaction after remote relay address changed.");
					}
				}
			}
		}

		return true;
	}
	rcatchall
	{
		Complete(COMPLETE_FAILED, failReason, NULL);
	}

	return false;
}

bool
netIceSession::SendInitialOffers()
{
	FailReason failReason = FAIL_REASON_COULD_NOT_SEND_DIRECT_OR_INDIRECT_OFFERS;

	rtry
	{
		rverify(m_SentInitialOffers == false, catchall, );

		m_SentInitialOffers = true;

		if(m_CheckList.GetCount() == 0)
		{
			/*
			4.  Sending the Initial Offer

			In order to send the initial offer in an offer/answer exchange, an
			agent must (1) gather candidates, (2) prioritize them, (3) eliminate
			redundant candidates, (4) choose default candidates, and then (5)
			formulate and send the SDP offer.
			*/
			rverify(GatherLocalCandidates(), catchall, failReason = FAIL_REASON_START_AS_OFFERER_GLC);

			/*
			[NS] since we already know the remote candidates from the peer address,
			we gather remote candidates and create the checklist at session start
			time so we can more easily handle cases where we receive pings from 
			the remote peer before receiving the answer to our offer.
			*/
			rverify(GatherRemoteCandidates(), catchall, failReason = FAIL_REASON_START_AS_OFFERER_GRC);
			rverify(CreateCheckList(), catchall, failReason = FAIL_REASON_START_AS_OFFERER_CHECKLIST);
		}

		AddRemoteSymmetricSocketCandidates();
		AddRemoteSymmetricInferenceCandidates();
		
		if(SendDirectOffers() == false)
		{
			netIceDebug1("Failed to send direct offers.");

			FailReason failReason;
			if(SendIndirectOffer(failReason) == false)
			{
				// ignore the returned failReason, use a specific error indicating the direct offer failed too.
				netIceDebug1("Failed to send indirect offer.");
				Complete(COMPLETE_FAILED, FAIL_REASON_COULD_NOT_SEND_DIRECT_OR_INDIRECT_OFFERS, NULL);
				return false;
			}
		}

		return true;
	}
	rcatchall
	{
		Complete(COMPLETE_FAILED, failReason, NULL);
		return false;
	}
}

bool
netIceSession::NeedsPresenceQuery() const
{
	if(netIceTunneler::sm_AllowPresenceQuery &&
		(m_RelayCommunicationSucceeded == false) &&
		(m_PresenceQueryState == PRESENCE_QUERY_STATE_UNATTEMPTED) &&
		(m_RemotePeerAddr.GetGamerHandle().IsNativeGamerHandle()))
	{
		return true;
	}

	return false;
}

bool
netIceSession::StartPresenceQuery()
{
	if(NeedsPresenceQuery())
	{
		rtry
		{
			netIceDebug1("Starting presence query...");
			rcheck(m_RemotePeerAddr.GetGamerHandle().IsValid(), catchall, );

			CompileTimeAssert(COUNTOF(m_PresenceAttrs) == ATTR_MAX_PRESENCE_ATTRIBUTES);
			m_PresenceAttrs[ATTR_INDEX_PEER_ADDRESS].Reset(rlScAttributeId::PeerAddress.Name, "");

			rverify(rlPresence::GetAttributesForGamer(rlPresence::GetActingUserIndex(), m_RemotePeerAddr.GetGamerHandle(), m_PresenceAttrs, COUNTOF(m_PresenceAttrs), &m_PresenceQueryStatus), catchall, );

			m_PresenceQueryState = PRESENCE_QUERY_STATE_IN_PROGRESS;

			return true;
		}
		rcatchall
		{
			m_PresenceQueryState = PRESENCE_QUERY_STATE_FAILED;
		}
	}

	return false;
}

bool
netIceSession::IsPresenceQueryInProgress() const
{
	switch(m_PresenceQueryState)
	{
	case PRESENCE_QUERY_STATE_IN_PROGRESS:
		return true;

	case PRESENCE_QUERY_STATE_UNATTEMPTED:
	case PRESENCE_QUERY_STATE_SUCCEEDED:
	case PRESENCE_QUERY_STATE_FAILED:
		return false;
	}

	return false;
}

void 
netIceSession::UpdatePresenceQuery()
{
	switch(m_PresenceQueryState)
	{
	case PRESENCE_QUERY_STATE_UNATTEMPTED:
		break;
	case PRESENCE_QUERY_STATE_IN_PROGRESS:
		if(m_RelayCommunicationSucceeded)
		{
			netIceDebug1("Relay communication succeeded. Cancelling the in-progress presence query.");

			if(m_PresenceQueryStatus.Pending())
			{
				rlPresence::CancelQuery(&m_PresenceQueryStatus);
			}
			m_PresenceQueryState = PRESENCE_QUERY_STATE_FAILED;
			return;
		}
		else if(m_PresenceQueryStatus.Succeeded())
		{
			netPeerAddress peerAddr;

			if(m_PresenceAttrs[ATTR_INDEX_PEER_ADDRESS].Type == RLSC_PRESTYPE_STRING)
			{
				char szPeerAddr[netPeerAddress::TO_STRING_BUFFER_SIZE] = {0};
				if(m_PresenceAttrs[ATTR_INDEX_PEER_ADDRESS].GetValue(szPeerAddr))
				{
					if(!peerAddr.FromString(szPeerAddr))
					{
						peerAddr.Clear();
					}
				}
			}

			netIceDebug1("Presence returned netPeerAddress: %s", peerAddr.ToString());
			
			// the relay address from the presence attribute will only contain the relay token, not the real ip/port
			const netAddress& relayAddress = peerAddr.GetRelayAddress();

			if(relayAddress.IsRelayServerAddr())
			{
				netIceDebug1("Presence query returned: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(relayAddress));
				netAssert(relayAddress.GetRelayToken().IsValid() && !relayAddress.GetTargetAddress().IsValid());

				m_PresenceQueryState = PRESENCE_QUERY_STATE_SUCCEEDED;

				AddRemoteCandidateToChecklist(Candidate::SERVER_REFLEXIVE_CANDIDATE, relayAddress.GetTargetAddress());

				if(relayAddress.GetProxyAddress() != m_RemoteRelayAddr.GetProxyAddress() || relayAddress.GetRelayToken() != m_RemoteRelayAddr.GetRelayToken())
				{
					netIceDebug1("Presence query returned a different relay address, retrying NAT traversal.");

					m_RemoteRelayAddr = relayAddress;

					// cancel existing indirect offer transactions
					m_Transactor.CancelTransactionsByUserData(&m_IndirectOffersToken);

					FailReason failReason;
					if(SendIndirectOffer(failReason) == false)
					{
						netIceDebug1("Failed to send indirect offer.");
						IndirectOfferTimedOut();
						return;
					}
				}
				else
				{
					netIceDebug1("Presence query confirmed we had the remote peer's up-to-date relay address.");

					// if there is still an indirect offer transaction in progress, let it continue.
					if(m_Transactor.DoesTransactionExist(&m_IndirectOffersToken) == false)
					{
						IndirectOfferTimedOut();
						return;
					}
				}
			}
			else
			{
				m_PresenceQueryState = PRESENCE_QUERY_STATE_FAILED;
				netIceDebug1("Presence query returned no relay address. Remote peer is probably offline.");
				if(m_Transactor.DoesTransactionExist(&m_IndirectOffersToken) == false)
				{
					IndirectOfferTimedOut();
					return;
				}
			}
		}
		else if(m_PresenceQueryStatus.Canceled())
		{
			m_PresenceQueryState = PRESENCE_QUERY_STATE_FAILED;
			netIceDebug1("Presence query cancelled.");
		}
		else if(!m_PresenceQueryStatus.Pending())
		{
			m_PresenceQueryState = PRESENCE_QUERY_STATE_FAILED;
			netIceDebug1("Failed to query presence.");

			// if there is still an indirect offer transaction in progress, let it continue.
			if(m_Transactor.DoesTransactionExist(&m_IndirectOffersToken) == false)
			{
				IndirectOfferTimedOut();
				return;
			}
		}
		break;

	case PRESENCE_QUERY_STATE_SUCCEEDED:
		break;
	case PRESENCE_QUERY_STATE_FAILED:
		break;
	}
}

void
netIceSession::CancelPresenceQuery()
{
	if(m_PresenceQueryStatus.Pending())
	{
		rlPresence::CancelQuery(&m_PresenceQueryStatus);
	}

	m_PresenceQueryState = PRESENCE_QUERY_STATE_FAILED;
}

bool
netIceSession::NeedLocalRelayRefresh() const
{
	// If we've sent an indirect offer but haven't received a response *via relay*
	// within a set amount of time, then we refresh our local relay mapping to 
	// ensure our NAT hasn't closed the port to our relay. This can happen if we
	// haven't sent it any packets within the current UDP session timeout,
	// which can change over time on some NATs. Additional checks here are to 
	// make sure we only refresh when necessary.
 	if(netIceTunneler::sm_AllowLocalRelayRefresh &&
		(m_RelayCommunicationSucceeded == false) &&
 		(m_LocalRelayRefreshState == LOCAL_RELAY_REFRESH_STATE_UNATTEMPTED) &&
		(m_LastIndirectOfferSentTime > 0) &&
		((sysTimer::GetSystemMsTime() - m_LastIndirectOfferSentTime) > LOCAL_RELAY_REFRESH_TIMEOUT_MS) &&
 		(netRelay::GetMsSinceLastRelayPing() >= (netRelay::GetMinSupportedPortBindingTimeoutSec() * 1000)))
 	{
 		return true;
 	}

	return false;
}

void
netIceSession::RefreshLocalRelay()
{
	if(NeedLocalRelayRefresh())
	{
		netIceDebug3("Refreshing local relay port mapping. Time since last local relay ping: %ums.", netRelay::GetMsSinceLastRelayPing());
 		netRelay::ForceRefreshRelayMapping();
 		m_LocalRelayRefreshState = LOCAL_RELAY_REFRESH_STATE_COMPLETED;
	}
}

bool
netIceSession::NeedsCrossRelayCandidateCheck() const
{
	FailReason failReason;
	if(netIceTunneler::sm_AllowCrossRelayCheck &&
		!IsLocalStrict() &&
		CanSendViaRelay(failReason) &&
		(m_CrossRelayCheckState == CROSS_RELAY_STATE_UNATTEMPTED) &&
		(m_IndirectOfferStatus == OFFER_STATUS_UNATTEMPTED))
	{
		return true;
	}

	return false;
}

bool
netIceSession::StartCrossRelayCandidateCheck()
{
	if(netIceTunneler::sm_AllowCrossRelayCheck &&
		(m_CrossRelayCheckState == CROSS_RELAY_STATE_UNATTEMPTED))
	{
		if(m_SharedSocketPortMapper.Init(m_RemoteRelayAddr.GetProxyAddress()))
		{
			netIceDebug3("Starting Cross Relay Check");

			m_CrossRelayCheckState = CROSS_RELAY_STATE_WAITING_FOR_PORT_MAPPER;
			return true;
		}

		m_SharedSocketPortMapper.Clear();
		m_CrossRelayCheckState = CROSS_RELAY_STATE_FAILED;
	}

	netIceDebug3("StartCrossRelayCandidateCheck failed");

	return false;
}

bool
netIceSession::IsCrossRelayCandidateCheckInProgress() const
{
	switch(m_CrossRelayCheckState)
	{
	case CROSS_RELAY_STATE_WAITING_FOR_PORT_MAPPER:
	case CROSS_RELAY_STATE_DISCOVERING:
		return true;

	case CROSS_RELAY_STATE_UNATTEMPTED:
	case CROSS_RELAY_STATE_SUCCEEDED:
	case CROSS_RELAY_STATE_FAILED:
		return false;
	}

	return false;
}

void
netIceSession::UpdateCrossRelayCandidateCheck()
{
	m_SharedSocketPortMapper.Update();

	switch(m_CrossRelayCheckState)
	{
	case CROSS_RELAY_STATE_UNATTEMPTED:
		break;
	case CROSS_RELAY_STATE_WAITING_FOR_PORT_MAPPER:
		if(m_SharedSocketPortMapper.IsReadyToSend())
		{
			if(m_SharedSocketPortMapper.GetNumServersAvailable() == 1)
			{
				m_SharedSocketPortMapper.MapPort(0, m_CrossRelayMappedAddrs[0], &m_CrossRelayMappedAddrStatus[0]);
			}

			m_CrossRelayCheckState = CROSS_RELAY_STATE_DISCOVERING;
		}
		break;
	case CROSS_RELAY_STATE_DISCOVERING:
		{
			bool pending = false;
			unsigned numSucceeded = 0;
			for(unsigned i = 0; i < m_SharedSocketPortMapper.GetNumServersAvailable(); ++i)
			{
				if(m_CrossRelayMappedAddrStatus[i].Pending())
				{
					pending = true;
					break;
				}
				else if(m_CrossRelayMappedAddrStatus[i].Succeeded())
				{
					numSucceeded++;
				}
			}

			if(!pending)
			{
				if(numSucceeded == 0)
				{
					netIceDebug2("Cross relay check failed to get anything back from the relays.");

					m_SharedSocketPortMapper.Clear();
					m_CrossRelayCheckState = CROSS_RELAY_STATE_FAILED;
				}
				else
				{
					for(unsigned i = 0; i < m_SharedSocketPortMapper.GetNumServersAvailable(); ++i)
					{
						if(m_CrossRelayMappedAddrStatus[i].Succeeded() && m_CrossRelayMappedAddrs[i].IsValid())
						{
							netIceDebug2("NAT port mapping %d of %d: " NET_ADDR_FMT, i + 1, numSucceeded, NET_ADDR_FOR_PRINTF(m_CrossRelayMappedAddrs[i]));
							netIceTunneler::sm_LastRelayPingResult = m_CrossRelayMappedAddrs[i];

							if(m_LocalPeerAddr.GetPublicAddress().IsValid() && (m_CrossRelayMappedAddrs[i] != m_LocalPeerAddr.GetPublicAddress()) && (m_LocalPeerAddr.GetNatType() != NET_NAT_STRICT))
							{
								netNatDetector::SetNonDeterministic();
							}
						}
					}
					
					m_SharedSocketPortMapper.Clear();
					m_CrossRelayCheckState = CROSS_RELAY_STATE_SUCCEEDED;
				}
			}
		}
		break;
	case CROSS_RELAY_STATE_SUCCEEDED:
	case CROSS_RELAY_STATE_FAILED:
		if(m_IndirectOfferStatus == OFFER_STATUS_UNATTEMPTED && Pending())
		{
			FailReason failReason;
			if(SendIndirectOffer(failReason) == false)
			{
				netIceDebug1("Failed to send indirect offer.");
				Complete(COMPLETE_FAILED, failReason, NULL);
				return;
			}
		}
		break;
	}
}

void
netIceSession::CancelCrossRelayCandidateCheck()
{
	m_SharedSocketPortMapper.Clear();
	m_CrossRelayCheckState = CROSS_RELAY_STATE_FAILED;
}

void
netIceSession::SendRelayRouteCheckRequest()
{
	rtry
	{
		rcheck(netIceTunneler::sm_AllowRelayRouteCheck, noerr, );
		rcheck(m_RelayRouteCheckState == RELAY_ROUTE_CHECK_STATE_UNATTEMPTED, noerr, );
		rcheck(m_CxnMgr && m_CxnMgr->IsInitialized(), noerr, );
		rverifyall(GetDestSessionId() != NET_INVALID_ICE_SESSION_ID);

		// this is only used for telemetry - skip it if the NAT metric won't be sent
		rcheck(m_SendTelemetry, noerr, );
		
		netIceDebug2("Sending relay route check request to:" NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_RemoteRelayAddr));

		FailReason relayFailReason;
		rcheck(CanSendViaRelay(relayFailReason), catchall, );

		netIceSessionRelayRouteCheck msg(GetSessionId(), GetDestSessionId(), netIceSessionRelayRouteCheck::RELAY_ROUTE_CHECK_FLAGS_REQUEST);

		netP2pCryptContext context;
		context.Init(m_RemotePeerAddr.GetPeerKey(), NET_P2P_CRYPT_TUNNELING_PACKET);

		rverify(m_CxnMgr->SendTunnelingPacket(m_RemoteRelayAddr,
											  msg,
											  context),
				catchall, );

		m_RelayRouteCheckState = RELAY_ROUTE_CHECK_STATE_REQUEST_SENT;
	}
	rcatch(noerr)
	{

	}
	rcatchall
	{
		m_RelayRouteCheckState = RELAY_ROUTE_CHECK_STATE_FAILED;
	}
}

void
netIceSession::SendRelayRouteCheckResponse(const netIceSessionRelayRouteCheck* requestMsg, const netAddress& sender)
{
	rtry
	{
		rcheck(netIceTunneler::sm_AllowRelayRouteCheck, noerr, );
		rverifyall(sender.IsRelayServerAddr())
		rcheck(m_CxnMgr && m_CxnMgr->IsInitialized(), noerr, );

		netIceDebug2("Sending relay route check reply to:" NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(sender));

		netIceSessionRelayRouteCheck msg(GetSessionId(), requestMsg->m_SourceSessionId, netIceSessionRelayRouteCheck::RELAY_ROUTE_CHECK_FLAGS_RESPONSE);

		netP2pCryptContext context;
		context.Init(m_RemotePeerAddr.GetPeerKey(), NET_P2P_CRYPT_TUNNELING_PACKET);

		rverify(m_CxnMgr->SendTunnelingPacket(m_RemoteRelayAddr,
											  msg,
											  context),
				catchall, );

	}
	rcatch(noerr)
	{

	}
	rcatchall
	{

	}
}

bool
netIceSession::NeedsSymmetricInference() const
{
	if(netIceTunneler::sm_AllowSymmetricInference && 
		(m_SymmetricInference.m_State == SymmetricInference::SYM_STATE_UNATTEMPTED))
	{
		if(m_LocalPeerAddr.GetNatType() == NET_NAT_STRICT)
		{
			// symmetric inference only works with symmetric NATs that do port-contiguous allocation.
			const netNatInfo& natInfo = netNatDetector::GetLocalNatInfo();
			return natInfo.GetPortAllocationStrategy() == NET_NAT_PAS_PORT_CONTIGUOUS;
		}
		else if((m_LocalPeerAddr.GetNatType() != NET_NAT_STRICT) &&
				(netIceTunneler::sm_MaxFailedSymmetricInferenceTests > 0))
		{
			/*
			Some Moderate NATs turn contiguous symmetric under some condition. Example:
			Peer Reflexive List (num valid entries: 10, re-used ports: false):
			0: 62.78.55.40:1548, NumHits: 1, Timestamp: 1460062125
			1: 62.78.55.40:1546, NumHits: 1, Timestamp: 1460062124
			2: 62.78.55.40:1544, NumHits: 1, Timestamp: 1460062099
			3: 62.78.55.40:1542, NumHits: 1, Timestamp: 1460062082
			4: 62.78.55.40:1539, NumHits: 1, Timestamp: 1460062074

			Some Moderate NATs change ports over time and re-use them, as in:
			0: 82.4.254.57:1432, NumHits: 29, Timestamp: 1446844409
			1: 82.4.254.57:1431, NumHits: 32, Timestamp: 1446844409
			2: 82.4.254.57:1430, NumHits: 48, Timestamp: 1446844409
			3: 82.4.254.57:1429, NumHits: 48, Timestamp: 1446842986
			4: 82.4.254.57:1428, NumHits: 33, Timestamp: 1446841753

			And some Moderate NATs that turn symmetric become unpredictable:
			Peer Reflexive List (num valid entries: 10, re-used ports: false):
			0: 182.253.150.113:2928, NumHits: 1, Timestamp: 1460058931
			1: 182.253.150.113:3326, NumHits: 1, Timestamp: 1460058830
			2: 182.253.150.113:2422, NumHits: 1, Timestamp: 1460058712
			3: 182.253.150.113:2634, NumHits: 1, Timestamp: 1460058521
			4: 182.253.150.113:2848, NumHits: 1, Timestamp: 1460058512

			If we have several non-expired peer reflexive candidates, then perform a symmetric inference
			test to see if the NAT has turned symmetric with contiguous port allocation.
			*/

			if(netNatDetector::GetLocalNatInfo().IsNonDeterministic())
			{
				return true;
			}

			netSocketAddress candidates[netIceTunneler::MAX_PEER_REFLEXIVE_CANDIDATES];
			unsigned numCandidates = netIceTunneler::GetPeerReflexiveAddresses(candidates, COUNTOF(candidates));
			if(numCandidates >= netIceTunneler::MAX_PEER_REFLEXIVE_CANDIDATES)
			{
				// we have the max number of valid peer reflexive candidates. This is a good indication
				// that we're behind a non-deterministic NAT. Let's do a test.
				return true;
			}
		}
	}

	return false;
}

bool
netIceSession::StartSymmetricInference()
{
	if(netIceTunneler::sm_AllowSymmetricInference &&
		(m_SymmetricInference.m_State == SymmetricInference::SYM_STATE_UNATTEMPTED))
	{
		// Strict NAT's do symmetric inference over a different socket, all other NAT
		// types do symmetric inference over the shared socket. This is to handle Moderate
		// NAT that turn symmetric under load. Only the shared socket seems to turn symmetric;
		// opening a different socket makes it look Moderate on the new socket so it's not helpful.
		const bool sharedSocket = m_LocalPeerAddr.GetNatType() != NET_NAT_STRICT;
		if(m_SymmetricInference.m_PortMapper.Init(1, 2, sharedSocket))
		{
			m_SymmetricInference.m_State = SymmetricInference::SYM_STATE_WAITING_FOR_PORT_MAPPER;
			netIceDebug3("Starting Symmetric Inference");
			return true;
		}

		m_SymmetricInference.m_PortMapper.Clear();
		m_SymmetricInference.m_State = SymmetricInference::SYM_STATE_FAILED;
	}

	netIceDebug("StartSymmetricInference failed");

	return false;
}

bool
netIceSession::IsSymmetricInferenceInProgress() const
{
	switch(m_SymmetricInference.m_State)
	{
	case SymmetricInference::SYM_STATE_WAITING_FOR_PORT_MAPPER:
	case SymmetricInference::SYM_STATE_DISCOVERING:
		return true;

	case SymmetricInference::SYM_STATE_UNATTEMPTED:
	case SymmetricInference::SYM_STATE_SUCCEEDED:
	case SymmetricInference::SYM_STATE_FAILED:
	case SymmetricInference::NUM_STATES:
		return false;
	}

	return false;
}

void
netIceSession::UpdateSymmetricInference()
{
	m_SymmetricInference.m_PortMapper.Update();

	switch(m_SymmetricInference.m_State)
	{
	case SymmetricInference::SYM_STATE_UNATTEMPTED:
		break;
	case SymmetricInference::SYM_STATE_WAITING_FOR_PORT_MAPPER:
		if(m_SymmetricInference.m_PortMapper.IsReadyToSend())
		{
			// Note: this is carefully designed to send all pings in the shortest
			// amount of time possible (i.e. all within the same millisecond). Any
			// amount of time between pings would allow other NAT traversals and
			// other devices on the same network to interfere with our port inference
			// test. Make sure each ping is sent to the socket before exiting this function.
			if(m_SymmetricInference.m_PortMapper.GetNumServersAvailable() == 1)
			{
				// we'll have trouble if we're behind a port-preserving symmetric NAT
				m_SymmetricInference.m_PortMapper.MapPort(0, m_SymmetricInference.m_SymmetricMappedAddrs[0], &m_SymmetricInference.m_SymmetricMappedAddrStatus[0]);
				SendPortOpenerMessage(m_RemoteRelayAddr.GetTargetAddress());
			}
			else if(m_SymmetricInference.m_PortMapper.GetNumServersAvailable() >= 2)
			{
				m_SymmetricInference.m_PortMapper.MapPort(0, m_SymmetricInference.m_SymmetricMappedAddrs[0], &m_SymmetricInference.m_SymmetricMappedAddrStatus[0]);
				SendPortOpenerMessage(m_RemoteRelayAddr.GetTargetAddress());
				m_SymmetricInference.m_PortMapper.MapPort(1, m_SymmetricInference.m_SymmetricMappedAddrs[1], &m_SymmetricInference.m_SymmetricMappedAddrStatus[1]);
			}

			m_SymmetricInference.m_State = SymmetricInference::SYM_STATE_DISCOVERING;
		}
		break;
	case SymmetricInference::SYM_STATE_DISCOVERING:
		{
			bool pending = false;
			unsigned numSucceeded = 0;
			for(unsigned i = 0; i < m_SymmetricInference.m_PortMapper.GetNumServersAvailable(); ++i)
			{
				if(m_SymmetricInference.m_SymmetricMappedAddrStatus[i].Pending())
				{
					pending = true;
					break;
				}
				else if(m_SymmetricInference.m_SymmetricMappedAddrStatus[i].Succeeded())
				{
					numSucceeded++;
				}
			}

			if(!pending)
			{
				if(numSucceeded == 0)
				{
					netIceDebug2("Symmetric inference failed to get anything back from the relays.");

					m_SymmetricInference.m_PortMapper.Clear();
					m_SymmetricInference.m_State = SymmetricInference::SYM_STATE_FAILED;
				}
				else
				{
					netIceDebug2("NAT port mapper socket bound to address: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_SymmetricInference.m_PortMapper.GetSocketAddress()));

					for(unsigned i = 0; i < m_SymmetricInference.m_PortMapper.GetNumServersAvailable(); ++i)
					{
						if(m_SymmetricInference.m_SymmetricMappedAddrStatus[i].Succeeded() && m_SymmetricInference.m_SymmetricMappedAddrs[i].IsValid())
						{
							netIceDebug2("NAT port mapping %d of %d: " NET_ADDR_FMT, i + 1, numSucceeded, NET_ADDR_FOR_PRINTF(m_SymmetricInference.m_SymmetricMappedAddrs[i]));

							if(m_LocalPeerAddr.GetPublicAddress().IsValid() && (m_SymmetricInference.m_SymmetricMappedAddrs[i] != m_LocalPeerAddr.GetPublicAddress()) && (m_LocalPeerAddr.GetNatType() != NET_NAT_STRICT))
							{
								netNatDetector::SetNonDeterministic();
							}
						}
					}

					u16 portIncrement = 1;
					m_SymmetricInference.m_SymmetricInferredPort = 0;
					m_SymmetricInference.m_PortMapperPort = m_SymmetricInference.m_PortMapper.GetSocketAddress().GetPort();

					// Note: the order in which the packets get sent to the socket is not necessarily the same order in 
					// which they reach the NAT. Analyze the results in a way that doesn't depend on the order.

					if(m_SymmetricInference.m_SymmetricMappedAddrStatus[0].Succeeded() && m_SymmetricInference.m_SymmetricMappedAddrStatus[1].Succeeded())
					{
						// both succeeded
						u16 port0 = m_SymmetricInference.m_SymmetricMappedAddrs[0].GetPort();
						u16 port1 = m_SymmetricInference.m_SymmetricMappedAddrs[1].GetPort();

						u16 lowerBound = port0;
						u16 upperBound = port1;

						if(lowerBound > upperBound)
						{
							lowerBound = port1;
							upperBound = port0;
						}

						int difference = upperBound - lowerBound;

						if((lowerBound != upperBound) && (m_LocalPeerAddr.GetNatType() != NET_NAT_STRICT))
						{
							netNatDetector::SetNonDeterministic();
						}

						// if either of the ports match our locally bound port, the NAT preserved the local port
						// if we're not port-contiguous and not port-preserving, the NAT is doing random port allocation
						bool portPreserving = (lowerBound == m_SymmetricInference.m_PortMapperPort) || (upperBound == m_SymmetricInference.m_PortMapperPort);
						bool portContiguous = !portPreserving && (difference <= 4);
						bool randomPort = !portContiguous && !portPreserving;

						m_PortPreservingSymmetric = m_PortPreservingSymmetric || portPreserving;
						m_RandomPortSymmetric = m_RandomPortSymmetric || randomPort;

						netIceDebug2("Both port mappings succeeded. Difference = %d.", difference);

						if(difference <= 1)
						{
							netIceDebug2("We didn't open a new port when sending to the remote peer.");
							m_SymmetricInference.m_SymmetricInferredPort = 0;
						}
						else if(difference <= 4)
						{
							u16 midpoint = upperBound - ((upperBound - lowerBound) / 2);
							m_SymmetricInference.m_SymmetricInferredPort = midpoint;
						}
						else
						{
							// upper and lower bound are too far apart, we're not behind a port-contiguous symmetric

							if(upperBound == m_SymmetricInference.m_PortMapperPort)
							{
								m_SymmetricInference.m_SymmetricInferredPort = lowerBound + portIncrement;
							}
							else
							{
								m_SymmetricInference.m_SymmetricInferredPort = upperBound - portIncrement;
							}							
						}
					}
					else if(m_SymmetricInference.m_SymmetricMappedAddrStatus[0].Succeeded())
					{
						// only lower bound succeeded
						netIceDebug2("Only the lower bound port mapping succeeded.");

						u16 lowerBound = m_SymmetricInference.m_SymmetricMappedAddrs[0].GetPort();
						m_PortPreservingSymmetric = m_PortPreservingSymmetric || (lowerBound == m_SymmetricInference.m_PortMapperPort);

						m_SymmetricInference.m_SymmetricInferredPort = lowerBound + portIncrement;
					}
					else if(m_SymmetricInference.m_SymmetricMappedAddrStatus[1].Succeeded())
					{
						// only upper bound succeeded
						netIceDebug2("Only the upper bound port mapping succeeded.");

						u16 upperBound = m_SymmetricInference.m_SymmetricMappedAddrs[1].GetPort();
						m_PortPreservingSymmetric = m_PortPreservingSymmetric || (upperBound == m_SymmetricInference.m_PortMapperPort);

						m_SymmetricInference.m_SymmetricInferredPort = upperBound - portIncrement;
					}

					if(m_SymmetricInference.m_SymmetricInferredPort > 0)
					{
						netIceDebug2("Inferring that the NAT opened port %u when sending a packet to the remote peer.", m_SymmetricInference.m_SymmetricInferredPort);
					}

					m_SymmetricInference.m_PortMapper.Clear();
					m_SymmetricInference.m_State = SymmetricInference::SYM_STATE_SUCCEEDED;
				}
			}
		}
		break;
	case SymmetricInference::SYM_STATE_SUCCEEDED:
	case SymmetricInference::SYM_STATE_FAILED:
		if((m_SentInitialOffers == false) && Pending() && !IsSymmetricSocketInProgress())
		{
			SendInitialOffers();
		}
		break;
	case SymmetricInference::NUM_STATES:
		break;
	}
}

void 
netIceSession::CancelSymmetricInference()
{
	m_SymmetricInference.m_PortMapper.Clear();
	m_SymmetricInference.m_State = SymmetricInference::SYM_STATE_FAILED;
}

void netIceSession::AddRemoteSymmetricSocketCandidates()
{
	if(m_SymmetricSocket.IsAssociated())
	{
		AddRemoteCandidateToChecklist(Candidate::SYMMETRIC_SOCKET_CANDIDATE, m_SymmetricSocket.m_RemoteMappedAddr);
	}
}

void netIceSession::AddRemoteSymmetricInferenceCandidates()
{
	if(m_CheckList.GetCount() != 0)
	{
		for(unsigned i = 0; i < COUNTOF(m_SymmetricInference.m_RemoteSymmetricInferredPort); ++i)
		{
			const u16 inferredPort = m_SymmetricInference.m_RemoteSymmetricInferredPort[i];
			if(inferredPort != 0)
			{
				netSocketAddress inferredAddress;
				inferredAddress.Init(m_RemoteRelayAddr.GetTargetAddress().GetIp(), inferredPort);
				const RemoteCandidate* remoteCandidate = m_RemoteCandidates.FindByTransportAddress(inferredAddress);
				if(!remoteCandidate)
				{
					/*
					This candidate is added to the list of remote candidates.
					*/
					netIceDebug3("Remote candidate for " NET_ADDR_FMT " not found. "
						"Adding a remote symmetric inference candidate.",
						NET_ADDR_FOR_PRINTF(inferredAddress));

					RemoteCandidate inferredCandidate(m_Logger, Candidate::SYMMETRIC_INFERENCE_CANDIDATE, inferredAddress);
					remoteCandidate = m_RemoteCandidates.AddCandidate(inferredCandidate);
					if(netVerify(remoteCandidate))
					{
						const LocalCandidate* localCandidate = m_LocalCandidates.FindByTransportAddress(m_LocalPeerAddr.GetPrivateAddress());
						if(netVerify(localCandidate && localCandidate->GetType() == Candidate::HOST_CANDIDATE))
						{
							CandidatePair pair(m_Logger, *localCandidate, *remoteCandidate);

							/*
							This pair is then added to check list if it's not already there.
							*/
							CandidatePair* foundPair = m_CheckList.FindMatchingPair(pair);

							if(foundPair == NULL)
							{
								foundPair = m_DiscoveredPairs.FindMatchingPair(pair);
								if(foundPair == NULL)
								{
									foundPair = m_DiscoveredPairs.AddCandidatePair(pair);
								}

								if(foundPair)
								{
									foundPair->SetState(CandidatePair::STATE_WAITING);
									m_CheckList.AddCandidatePair(foundPair);
								}
							}
						}
					}
				}
			}
		}
	}
}

bool
netIceSession::NeedsSymmetricSocket(const bool directOfferReceived) const
{
#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
	FailReason failReason;
	if((netIceTunneler::sm_SymmetricSocketMode != SymmetricSocket::Condition::NEVER) &&
		(m_SymmetricSocket.m_State == SymmetricSocket::STATE_UNATTEMPTED) &&		
		CanSendViaRelay(failReason))
	{
#if !__FINAL || defined(RSG_LEAN_CLIENT)
		if(PARAM_neticeforcesymmetricsocket.Get())
		{
			netIceDebug3("-neticeforcesymmetricsocket is enabled");
			return true;
		}
#endif

		if(m_SymmetricSocket.m_RemoteMappedAddr.IsValid())
		{
			// the remote peer opened a symmetric socket, we need to open ours
			netIceDebug3("Remote peer opened a symmetric socket with mapped address " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_SymmetricSocket.m_RemoteMappedAddr));
			return true;
		}
		else if(directOfferReceived)
		{
			return false;
		}
		else if(netIceTunneler::sm_SymmetricSocketMode == SymmetricSocket::Condition::ALWAYS)
		{
			return true;
		}
		else if(m_LocalPeerAddr.GetNatType() == NET_NAT_STRICT)
		{
			// this system is designed to support port-preserving symmetric NATs
			const netNatInfo& natInfo = netNatDetector::GetLocalNatInfo();
			return natInfo.GetPortAllocationStrategy() == NET_NAT_PAS_PORT_PRESERVING;
		}
		else if(m_LocalPeerAddr.GetNatType() != NET_NAT_STRICT)
		{
			if(netIceTunneler::sm_SymmetricSocketMode == SymmetricSocket::Condition::PP_SYM_AND_NON_SYMMETRIC)
			{
				return true;
			}
			else if(netIceTunneler::sm_SymmetricSocketMode == SymmetricSocket::Condition::PP_SYM_AND_NON_DETERMINISTIC)
			{
				// this system can also work for some non-symmetric, non-deterministic NATs
				return netNatDetector::GetLocalNatInfo().IsNonDeterministic();
			}
		}
	}
#endif

	return false;
}

bool
netIceSession::ChooseSymmetricSocketParams(u16& localPort)
{
	// We don't use port 0 here because we don't want to re-use a port we've used previously (even from a
	// previous launch in the last 15 minutes). Ideally we want to use a port that doesn't have a port
	// mapping on our NAT.
	unsigned short lowerBound = 0;
	unsigned short upperBound = 0;
	netSocket::GetPortRangeForRandomSelection(lowerBound, upperBound);
	localPort = (u16)s_IceRng.GetRanged(lowerBound, upperBound);

#if 0
	// TODO: NS - bonus: if relay comms fail, then we could send offer direct from
	// symmetric socket based on an implicitly agreed port

#if RSG_PC && !__NO_OUTPUT
	if(PARAM_processinstance.Get())
	{
		// multiple instances on the same device can't bind multiple sockets to the same port
		localPort = (u16)s_IceRng.GetRanged(32768, 65535);
	}
	else
#endif
	{
		// implicitly agree on a port between 32768 to 65535 without communication, based
		// on information known by both peers. Port range chosen according to RFC 5780.
		const netP2pCrypt::Key& localPeerKey = m_LocalPeerAddr.GetPeerKey();
		const netP2pCrypt::Key& remotePeerKey = m_RemotePeerAddr.GetPeerKey();

		netAssert(localPeerKey.IsValid());
		netAssert(remotePeerKey.IsValid());

		int result = memcmp(localPeerKey.GetRawKeyPtr(), remotePeerKey.GetRawKeyPtr(), netP2pCrypt::Key::KEY_LENGTH_IN_BYTES);

		const netP2pCrypt::Key* key1 = &localPeerKey;
		const netP2pCrypt::Key* key2 = &remotePeerKey;

		if(result > 0)
		{
			key2 = &localPeerKey;
			key1 = &remotePeerKey;
		}

		u32 hash = 0;
		hash = atDataHash((char*)key1->GetRawKeyPtr(), netP2pCrypt::Key::KEY_LENGTH_IN_BYTES, hash);
		hash = atDataHash((char*)key2->GetRawKeyPtr(), netP2pCrypt::Key::KEY_LENGTH_IN_BYTES, hash);
		hash = (hash & 0x00007FFF) + 0x8000;
		
		localPort = (u16)hash;
	}
#endif
		
	return true;
}

bool
netIceSession::StartSymmetricSocket()
{
	rtry
	{	
		netIceDebug3("Starting Symmetric Socket");

		rverifyall(SKT_MGR_ALLOW_SYMMETRIC_SOCKETS);
		rcheckall(netIceTunneler::sm_SymmetricSocketMode != SymmetricSocket::Condition::NEVER);
		rcheckall(m_SymmetricSocket.m_State == SymmetricSocket::STATE_UNATTEMPTED);
		rcheckall(m_CxnMgr->IsInitialized());

		u16 localPort = 0;
		rverifyall(ChooseSymmetricSocketParams(localPort));

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
		rcheckall(m_CxnMgr->GetSocketManager()->CreateSymmetricSocket(localPort, &m_SymmetricSocket.m_SocketId, &m_SymmetricSocket.m_Status));
#endif

		m_SymmetricSocket.m_State = SymmetricSocket::STATE_WAITING_FOR_SOCKET;

		return true;
	}
	rcatchall
	{
		netIceDebug3("StartSymmetricSocket failed");
		m_SymmetricSocket.m_State = SymmetricSocket::STATE_FAILED_TO_CREATE;
		return false;
	}
}

bool
netIceSession::IsSymmetricSocketInProgress() const
{
	switch(m_SymmetricSocket.m_State)
	{
	case SymmetricSocket::STATE_WAITING_FOR_SOCKET:
	case SymmetricSocket::STATE_WAITING_FOR_PORT_MAPPER:
	case SymmetricSocket::STATE_MAPPING_PORT:
	case SymmetricSocket::STATE_ASSUME_PORT_PRESERVING:
	case SymmetricSocket::STATE_MAP_SUCCEEDED:
	case SymmetricSocket::STATE_MAP_FAILED:
		return true;

	case SymmetricSocket::STATE_UNATTEMPTED:
	case SymmetricSocket::STATE_READY_FOR_ASSOCIATION:
	case SymmetricSocket::STATE_FAILED_TO_CREATE:
	case SymmetricSocket::STATE_ASSOCIATED:
	case SymmetricSocket::STATE_VALID_CANDIDATE:
	case SymmetricSocket::STATE_NOMINATED_CANDIDATE:
	case SymmetricSocket::NUM_STATES:
		return false;
	}

	return false;
}

void
netIceSession::UpdateSymmetricSocket()
{
#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
	if(m_SymmetricSocket.m_Finished || (m_SymmetricSocket.m_State == SymmetricSocket::STATE_UNATTEMPTED))
	{
		return;
	}

	const netSocket* skt = m_CxnMgr->GetSocketManager()->GetSymmetricSocket(m_SymmetricSocket.m_SocketId);
	if(skt)
	{
		m_SymmetricSocket.m_PortMapper.Update();
	}

	switch (m_SymmetricSocket.m_State)
	{
	case SymmetricSocket::STATE_UNATTEMPTED:
		break;
	case SymmetricSocket::STATE_WAITING_FOR_SOCKET:
		{
			if(!m_SymmetricSocket.m_Status.Pending())
			{
				if(m_SymmetricSocket.m_Status.Succeeded())
				{
					// Next, we need to determine, or predict, which port the NAT will map to
					// our symmetric socket. The mapped address will be sent to the remote peer
					// as a candidate address in our offer. The remote peer will do the same,
					// and tell us which address their NAT mapped to their symmetric socket.
					// Once both sides know which address is mapped to each other's symmetric
					// sockets, we should be able to make a direct connection to each other.

					const netNatInfo& natInfo = netNatDetector::GetLocalNatInfo();
					const bool portPreserving = natInfo.GetPortAllocationStrategy() == NET_NAT_PAS_PORT_PRESERVING;

					if(portPreserving ||
						m_LocalPeerAddr.GetNatType() == NET_NAT_STRICT ||
						m_LocalPeerAddr.GetNatType() == NET_NAT_UNKNOWN)
					{
						// if we're on a port-preserving symmetric NAT, it means the NAT 
						// will use the same public port as the socket's private port for the
						// *first* endpoint we send to (and only the first endpoint).
						// We only get one shot to figure out which address is the most likely
						// to succeed. Once a packet is sent from the symmetric socket, the
						// port-preservation attribute is used up. Packets sent to any other
						// endpoint would use a random, unpredictable port. Don't send any packet
						// out of our symmetric socket until the remote peer tells us their 
						// symmetric socket mapped address.
						m_SymmetricSocket.m_State = SymmetricSocket::STATE_ASSUME_PORT_PRESERVING;
					}
					else
					{
						// we're on a non-symmetric or non-port-preserving NAT.
						// Because it's not symmetric, it should be safe to send to a couple of
						// of different addresses without the NAT changing ports - even 
						// non-deterministic NATs will often use the same port for at least 2
						// endpoints. We ping a relay server to determine the socket's mapped port.

						netSocket* skt = m_CxnMgr->GetSocketManager()->GetSymmetricSocket(m_SymmetricSocket.m_SocketId);
						if(netVerify(skt) &&
							m_SymmetricSocket.m_PortMapper.Init(skt, 1, 1) &&
							(m_SymmetricSocket.m_PortMapper.GetNumServersAvailable() > 0))
						{
							m_SymmetricSocket.m_State = SymmetricSocket::STATE_WAITING_FOR_PORT_MAPPER;
						}
						else
						{
							// let's hope our NAT is port-preserving
							m_SymmetricSocket.m_State = SymmetricSocket::STATE_ASSUME_PORT_PRESERVING;
						}
					}
				}
				else
				{
					m_SymmetricSocket.m_State = SymmetricSocket::STATE_FAILED_TO_CREATE;
				}
			}
		}
		break;
	case SymmetricSocket::STATE_WAITING_FOR_PORT_MAPPER:
		if(skt && m_SymmetricSocket.m_PortMapper.IsReadyToSend())
		{
			if(m_SymmetricSocket.m_PortMapper.MapPort(0, m_SymmetricSocket.m_MappedAddr, &m_SymmetricSocket.m_Status))
			{
				m_SymmetricSocket.m_State = SymmetricSocket::STATE_MAPPING_PORT;
			}
			else
			{
				m_SymmetricSocket.m_State = SymmetricSocket::STATE_ASSUME_PORT_PRESERVING;
			}
		}
		break;
	case SymmetricSocket::STATE_MAPPING_PORT:
		{
			if(!m_SymmetricSocket.m_Status.Pending())
			{
				if(m_SymmetricSocket.m_Status.Succeeded() &&
					netVerify(m_SymmetricSocket.m_MappedAddr.IsValid()))
				{
#if !__FINAL || defined(RSG_LEAN_CLIENT)
					if(PARAM_neticesymsocketlocal.Get() || (m_LocalPeerAddr.GetPublicAddress().GetIp() == m_RemotePeerAddr.GetPublicAddress().GetIp()))
					{
						// for testing symmetric sockets on a LAN or multiple instances on the same PC.
						const netSocketAddress& socketAddr = m_CxnMgr->GetSocketManager()->GetSymmetricSocketAddress(m_SymmetricSocket.m_SocketId);
						m_SymmetricSocket.m_MappedAddr.Init(m_LocalPeerAddr.GetPrivateAddress().GetIp(), socketAddr.GetPort());
						m_SymmetricSocket.m_State = SymmetricSocket::STATE_MAP_SUCCEEDED;
					}
					else
#endif
					{
						m_SymmetricSocket.m_State = SymmetricSocket::STATE_MAP_SUCCEEDED;
					}
				}
				else
				{
					m_SymmetricSocket.m_State = SymmetricSocket::STATE_ASSUME_PORT_PRESERVING;
				}
			}
		}
		break;
	case SymmetricSocket::STATE_ASSUME_PORT_PRESERVING:
		{
			const netSocketAddress& socketAddr = m_CxnMgr->GetSocketManager()->GetSymmetricSocketAddress(m_SymmetricSocket.m_SocketId);
			if(netVerify(socketAddr.IsValid()))
			{
#if !__FINAL || defined(RSG_LEAN_CLIENT)
				if(PARAM_neticesymsocketlocal.Get() || (m_LocalPeerAddr.GetPublicAddress().GetIp() == m_RemotePeerAddr.GetPublicAddress().GetIp()))
				{
					// for testing symmetric sockets on a LAN or multiple instances on the same PC.
					m_SymmetricSocket.m_MappedAddr.Init(m_LocalPeerAddr.GetPrivateAddress().GetIp(), socketAddr.GetPort());
				}
				else
#endif
				{
					// if the NAT preserves the port, the mapped address is predictable
					m_SymmetricSocket.m_MappedAddr.Init(m_LocalPeerAddr.GetPublicAddress().GetIp(), socketAddr.GetPort());
				}

				m_SymmetricSocket.m_State = SymmetricSocket::STATE_MAP_SUCCEEDED;
			}
			else
			{
				m_SymmetricSocket.m_State = SymmetricSocket::STATE_MAP_FAILED;
			}
		}
		break;
	case SymmetricSocket::STATE_MAP_SUCCEEDED:
	case SymmetricSocket::STATE_MAP_FAILED:
	case SymmetricSocket::STATE_FAILED_TO_CREATE:
		if(Pending())
		{
			m_SymmetricSocket.m_PortMapper.Clear();

			if(m_SymmetricSocket.m_State == SymmetricSocket::STATE_MAP_SUCCEEDED)
			{
#if !__NO_OUTPUT
				const netSocketAddress& socketAddr = m_CxnMgr->GetSocketManager()->GetSymmetricSocketAddress(m_SymmetricSocket.m_SocketId);
				netIceDebug("UpdateSymmetricSocket :: Succeeded. Socket address: " NET_ADDR_FMT ", Mapped Address: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(socketAddr), NET_ADDR_FOR_PRINTF(m_SymmetricSocket.m_MappedAddr));
#endif

				m_SymmetricSocket.m_State = SymmetricSocket::STATE_READY_FOR_ASSOCIATION;

				if(m_SymmetricSocket.m_RemoteMappedAddr.IsValid())
				{
					// the remote peer already created a symmetric socket and we know
					// its mapped address. Activate it. All future packets sent to
					// m_SymmetricSocket.m_RemoteMappedAddr will use the symmetric socket.

					netIceDebug1("UpdateSymmetricSocket :: Remote peer already created a symmetric socket. Associating now.");

					bool isAssociated = m_CxnMgr->GetSocketManager()->AssociateSymmetricSocket(m_SymmetricSocket.m_SocketId, m_SymmetricSocket.m_RemoteMappedAddr);
					if(isAssociated)
					{
						m_SymmetricSocket.m_State = SymmetricSocket::STATE_ASSOCIATED;
					}
				}
			}
			else
			{
				// don't change the state, we want to send the state at which it failed to telemetry
				m_SymmetricSocket.m_Finished = true;
				netIceDebug("UpdateSymmetricSocket :: Failed");
			}

			// regardless of whether we succeeded or failed, we need to send our indirect offer
			// cancel existing indirect offer transactions
			m_Transactor.CancelTransactionsByUserData(&m_IndirectOffersToken);

			FailReason failReason;
			if(SendIndirectOffer(failReason) == false)
			{
				netIceDebug1("UpdateSymmetricSocket :: Failed to send indirect offer.");
			}
		}
		break;
	default:
		break;
	}
#endif
}

void 
netIceSession::CancelSymmetricSocket()
{
	// don't change the state, we want to send the state at which it failed to telemetry
	m_SymmetricSocket.m_Finished = true;
	m_SymmetricSocket.m_PortMapper.Clear();
}

bool netIceSession::AddRemoteCandidateToChecklist(const Candidate::CandidateType type, const netSocketAddress &addr)
{
	rtry
	{
		rcheckall(addr.IsValid());
		rcheckall(m_CheckList.GetCount() != 0);

		const LocalCandidate* localCandidate = nullptr;
		if(type == Candidate::SYMMETRIC_SOCKET_CANDIDATE)
		{
			rcheckall(SKT_MGR_ALLOW_SYMMETRIC_SOCKETS);

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
			// add the mapped address first. This is not paired with a remote candidate.
			if(m_SymmetricSocket.m_MappedAddr.IsValid())
			{
				localCandidate = m_LocalCandidates.FindByTransportAddress(m_SymmetricSocket.m_MappedAddr);
				if(!localCandidate)
				{
					LocalCandidate symmetricSocketCandidate(m_Logger, Candidate::SYMMETRIC_SOCKET_CANDIDATE, m_SymmetricSocket.m_MappedAddr);
					m_LocalCandidates.AddCandidate(symmetricSocketCandidate);
				}
			}

			// next add the private socket address. This is paired with the given remote candidate.
			// Note that the localAddr can be the same as the above m_SymmetricSocket.m_MappedAddr if there is no NAT.
			const netSocketAddress& localAddr = m_CxnMgr->GetSocketManager()->GetSymmetricSocketAddress(m_SymmetricSocket.m_SocketId);
			rverifyall(localAddr.IsValid());

			localCandidate = m_LocalCandidates.FindByTransportAddress(localAddr);
			if(!localCandidate)
			{
				LocalCandidate symmetricSocketCandidate(m_Logger, Candidate::SYMMETRIC_SOCKET_CANDIDATE, localAddr);
				localCandidate = m_LocalCandidates.AddCandidate(symmetricSocketCandidate);
			}

			rverifyall(localCandidate);
			rverifyall(localCandidate->GetType() == Candidate::SYMMETRIC_SOCKET_CANDIDATE);
#endif
		}
		else
		{
			localCandidate = m_LocalCandidates.FindByTransportAddress(m_LocalPeerAddr.GetPrivateAddress());
			rverifyall(localCandidate);
			rverifyall(localCandidate->GetType() == Candidate::HOST_CANDIDATE);
		}

		const RemoteCandidate* remoteCandidate = m_RemoteCandidates.FindByTransportAddress(addr);
		if(!remoteCandidate)
		{
			netIceDebug3("Remote candidate for " NET_ADDR_FMT " not found. "
							"Adding to remote candidates list.",
							NET_ADDR_FOR_PRINTF(addr));

			RemoteCandidate candidate(m_Logger, type, addr);
			remoteCandidate = m_RemoteCandidates.AddCandidate(candidate);
		}

		rverifyall(remoteCandidate);

		CandidatePair pair(m_Logger, *localCandidate, *remoteCandidate);

		/*
		This pair is then added to check list if it's not already there.
		*/
		CandidatePair* foundPair = m_CheckList.FindMatchingPair(pair);

		if(foundPair == NULL)
		{
			foundPair = m_DiscoveredPairs.FindMatchingPair(pair);
			if(foundPair == NULL)
			{
				foundPair = m_DiscoveredPairs.AddCandidatePair(pair);
			}

			if(foundPair)
			{
				foundPair->SetState(CandidatePair::STATE_WAITING);
				m_CheckList.AddCandidatePair(foundPair);
			}
		}
		
		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool netIceSession::AddRemoteCandidates(const netIceSessionOffer* msg, const netAddress& sender)
{
	if(m_CheckList.GetCount() != 0)
	{
		AddRemoteCandidateToChecklist(Candidate::HOST_CANDIDATE, msg->m_SourcePeerAddr.GetPrivateAddress());
		AddRemoteCandidateToChecklist(Candidate::SERVER_REFLEXIVE_CANDIDATE, msg->m_SourcePeerAddr.GetPublicAddress());
		AddRemoteCandidateToChecklist(Candidate::PEER_REFLEXIVE_CANDIDATE, sender.GetTargetAddress());

 		if(m_SymmetricSocket.IsAssociated())
 		{
 			AddRemoteCandidateToChecklist(Candidate::SYMMETRIC_SOCKET_CANDIDATE, m_SymmetricSocket.m_RemoteMappedAddr);
 		}

		for(unsigned i = 0; (i < netIceSessionOffer::MAX_CANDIDATES_PER_OFFER) && (i < msg->m_NumCandidates); ++i)
		{
			if(msg->m_Candidates[i].IsValid())
			{
				netIceDebug3("Offer contains additional candidates: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_Candidates[i]));
				AddRemoteCandidateToChecklist(Candidate::SERVER_REFLEXIVE_CANDIDATE, msg->m_Candidates[i]);
			}
		}
	}

	return true;
}

bool netIceSession::AddRemoteCandidates(const netIceSessionAnswer* msg, const netAddress& sender)
{
	if(m_CheckList.GetCount() != 0)
	{
		AddRemoteCandidateToChecklist(Candidate::HOST_CANDIDATE, msg->m_SourcePeerAddr.GetPrivateAddress());
		AddRemoteCandidateToChecklist(Candidate::SERVER_REFLEXIVE_CANDIDATE, msg->m_SourcePeerAddr.GetPublicAddress());
		AddRemoteCandidateToChecklist(Candidate::PEER_REFLEXIVE_CANDIDATE, sender.GetTargetAddress());

		if(m_SymmetricSocket.IsAssociated())
		{
			AddRemoteCandidateToChecklist(Candidate::SYMMETRIC_SOCKET_CANDIDATE, m_SymmetricSocket.m_RemoteMappedAddr);
		}

		for(unsigned i = 0; (i < netIceSessionAnswer::MAX_CANDIDATES_PER_ANSWER) && (i < msg->m_NumCandidates); ++i)
		{
			if(msg->m_Candidates[i].IsValid())
			{
				netIceDebug3("Answer contains additional candidates: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(msg->m_Candidates[i]));
				AddRemoteCandidateToChecklist(Candidate::SERVER_REFLEXIVE_CANDIDATE, msg->m_Candidates[i]);
			}
		}
	}

	return true;
}

void
netIceSession::UpdateUnmatchedPublicIpState(const netSocketAddress& senderAddr, const bool nominated)
{
	const netIpAddress& remoteIp = m_RemotePeerAddr.GetPublicAddress().GetIp();
	const netIpAddress& senderIp = senderAddr.GetIp();

	// collect telemetry on how often a remote peer sends a P2P message over a publicly 
	// routable (non-RFC 1918) IP that's different than the public IP seen by our servers.
	if(remoteIp.IsValid() && senderIp.IsValid() &&
		senderIp.IsV4() && !senderIp.ToV4().IsRfc1918() &&
		(senderIp != remoteIp))
	{
		if(m_UnmatchedPublicIpState != UNMATCHED_PUBLIC_IP_STATE_NOMINATED)
		{
			m_UnmatchedPublicIpState = nominated ?
									   UNMATCHED_PUBLIC_IP_STATE_NOMINATED :
									   UNMATCHED_PUBLIC_IP_STATE_RECEIVED;
		}
	}
}

bool
netIceSession::SendPortOpenerMessage(const netSocketAddress& addr) const
{
	rtry
	{
		netIceSessionPortOpener msg;

		netIceDebug2("Sending port opener to:" NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(m_RemoteRelayAddr.GetTargetAddress()));

		rcheck(m_CxnMgr->IsInitialized(), catchall, netWarning("m_CxnMgr->IsInitialized() is false"));

		netP2pCryptContext context;
		context.Init(m_RemotePeerAddr.GetPeerKey(), NET_P2P_CRYPT_TUNNELING_PACKET);

		netAddress destAddr(addr);
		rcheck(destAddr.IsValid(), catchall, netWarning("SendPortOpenerMessage :: destAddr is invalid"))
		rverify(m_CxnMgr->SendTunnelingPacket(destAddr,
											  msg,
											  context),
				catchall, );

		return true;
	}
	rcatchall
	{
	}

	return false;
}

void
netIceSession::InitRoleTieBreaker()
{
	netIceDebug3("Using role tie breaker strategy: %u", netIceTunneler::sm_RoleTieBreakerStrategy);

	if(netIceTunneler::sm_RoleTieBreakerStrategy == RTBS_IMPLICIT)
	{
		// Strategy 1: generate a tie breaker using known data on both sides
		m_RoleTieBreaker = GenerateImplicitTieBreaker();
		netIceDebug3("Generated implicit tie breaker: %" I64FMT "u", m_RoleTieBreaker);
	}
	else if((netIceTunneler::sm_RoleTieBreakerStrategy == RTBS_STRICTEST_WINS) || (netIceTunneler::sm_RoleTieBreakerStrategy == RTBS_LEAST_STRICT_WINS))
	{
		// Strategy 2: strictest NAT type has the controlling role
		// Strategy 3: least-strict NAT type has the controlling role
		m_RoleTieBreaker = (unsigned)s_IceRng.GetInt();

		netNatType localNatType = m_LocalPeerAddr.GetNatType();
		netNatType remoteNatType = m_RemotePeerAddr.GetNatType();

		int natTypeDifference = 0;

		if((localNatType != NET_NAT_UNKNOWN) && (remoteNatType != NET_NAT_UNKNOWN))
		{
			natTypeDifference = (int)localNatType - (int)remoteNatType;
		}

		CompileTimeAssert((NET_NAT_STRICT > NET_NAT_MODERATE) && (NET_NAT_MODERATE > NET_NAT_OPEN));

		if(((natTypeDifference > 0) && (netIceTunneler::sm_RoleTieBreakerStrategy == RTBS_STRICTEST_WINS)) ||
			((natTypeDifference < 0) && (netIceTunneler::sm_RoleTieBreakerStrategy == RTBS_LEAST_STRICT_WINS)))
		{
			unsigned rnd = (unsigned)s_IceRng.GetInt();
			m_RoleTieBreaker = m_RoleTieBreaker | (((u64)rnd) << 32);
		}
	}
	else
	{
		// Default strategy: use random tie breaker
		m_RoleTieBreaker = (unsigned)s_IceRng.GetInt();

		if(IsLocalStrict())
		{
			netIceDebug3("We are behind a Symmetric NAT. Prefer the controlling role.");

			/*
				Optimization: if we detect that we're behind a Symmetric NAT, we would prefer to 
				have the controlling role. That way, when we send a direct offer to the other peer,
				the other peer will receive it if they're behind an Open NAT (No NAT, Full Cone, or
				Restricted Cone) and we won't need to use the relay to initiate NAT traversal.
				If the other peer is behind a Symmetric or Moderate (Port Restricted Cone) NAT,
				the direct offer won't go through, and we'll fall back to the relay.
				Without this optimization we would need to use the relay to initiate NAT traversal
				whenever a peer with a Symmetric NAT was the offerer.
			*/
			unsigned rnd = (unsigned)s_IceRng.GetInt();
			m_RoleTieBreaker = m_RoleTieBreaker | (((u64)rnd) << 32);
		}
	}
}

bool
netIceSession::StartAsOfferer(netAddress* netAddr,
							  netAddress* relayAddr,
							  const bool bilateral,
							  netStatus* status)
{
	FailReason failReason = FAIL_REASON_START_AS_OFFERER;
	rtry
	{
		rverify(status, catchall, failReason = FAIL_REASON_START_AS_OFFERER_STATUS);
		status->SetPending();

		rverify(netAddr, catchall, failReason = FAIL_REASON_START_AS_OFFERER_NET_ADDR);

		netAssert(m_Role == ROLE_INVALID);
		m_Role = ROLE_CONTROLLING;
		netIceDebug3("We're the offerer so we have the controlling role.");
		
		// we init our role tie breaker if we start as the controller. If the remote
		// peer also starts as the controller, the peer with the lower tie breaker
		// will switch to the controlled role. Note that if we don't start as the
		// offerer, we assume the controlled role, and our role tie breaker will
		// remain at 0 so we don't retake the controlling role if we end up
		// sending an offer. This avoids some role reversal issues.
		InitRoleTieBreaker();

		m_Bilateral = bilateral;

		if(m_Bilateral)
		{
			netIceDebug3("We're connecting bilaterally.");
		}
		else
		{
			netIceDebug3("We're connecting unilaterally.");
		}

		int index = FindFreeNetStatusIndex();
		rverify(((index >= 0) && (index < MAX_ATTACHED_TUNNEL_REQUESTS)), catchall,
				netIceDebug3("Can't start session as offerer since all status objects are in use."));

		rverify((m_Status[index] == NULL) && (m_NetAddrPtr[index] == NULL) && (m_RelayAddrPtr[index] == NULL), catchall, );

		m_Status[index] = status;
		m_NetAddrPtr[index] = netAddr;
		m_RelayAddrPtr[index] = relayAddr;

		if(NeedsSymmetricInference())
		{
			StartSymmetricInference();
		}

		if(!IsSymmetricInferenceInProgress())
		{
			rverify(SendInitialOffers(), catchall, );
		}

		return true;
	}
	rcatchall
	{
		Complete(COMPLETE_FAILED, failReason, NULL);
	}

	return false;	
}

int
netIceSession::FindFreeNetStatusIndex() const
{
	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		if((m_Status[i] == NULL) && (m_NetAddrPtr[i] == NULL) && (m_RelayAddrPtr[i] == NULL))
		{
			return i;
		}
	}

	return -1;
}

bool
netIceSession::Attach(netAddress* netAddr,
					  netAddress* relayAddr,
					  const unsigned OUTPUT_ONLY(tunnelRequestId),
					  netStatus* status)
{
	rtry
	{
#if !__NO_OUTPUT
		if(m_TunnelRequestId == NET_INVALID_TUNNEL_REQUEST_ID)
		{
			m_TunnelRequestId = tunnelRequestId;
		}
#endif

		netIceDebug3("OpenTunnel was called after we had already started an ICE "
					 "session with this remote peer. Attaching tunnel request %u to active session.", tunnelRequestId);

		rverify(status, catchall, );
		rverify(netAddr, catchall, );

		int index = FindFreeNetStatusIndex();
		rverify(((index >= 0) && (index < MAX_ATTACHED_TUNNEL_REQUESTS)), catchall,
			netIceDebug3("Can't attach because max status objects are already assigned to this ICE session."));

		rverify((m_Status[index] == NULL) && (m_NetAddrPtr[index] == NULL) && (m_RelayAddrPtr[index] == NULL), catchall, );

		m_NetAddrPtr[index] = netAddr;
		m_RelayAddrPtr[index] = relayAddr;

		status->SetPending();
		m_Status[index] = status;

		if(Pending())
		{
			netIceDebug3("The active session is still in the pending state. "
						 "Will wait for it to complete.");
		}
		else
		{
			netIceDebug3("The active session has already %s.", Succeeded() ? "succeeded" : "failed");

			// run detach logic, which will either succeed or fail the request
			// based on the state of the current ICE session
			Detach(m_FailReason);
		}

		return true;
	}
	rcatchall
	{

	}

	netIceDebug3("Failed to attach to active session. Will start a new session.");

	return false;	
}

bool
netIceSession::IsControlling() const
{
	netAssert(m_Role == ROLE_CONTROLLING ||
		   m_Role == ROLE_CONTROLLED);

	return m_Role == ROLE_CONTROLLING;
}

bool
netIceSession::IsControlled() const
{
	netAssert(m_Role == ROLE_CONTROLLING ||
		   m_Role == ROLE_CONTROLLED);

	return m_Role == ROLE_CONTROLLED;
}

unsigned 
netIceSession::GenerateImplicitTieBreaker()
{
	netAssert(m_RemotePeerAddr.IsValid() && m_LocalPeerAddr.IsValid());
	netAssert(m_RemotePeerAddr.GetPeerKey().IsValid());
	netAssert(m_LocalPeerAddr.GetPeerKey().IsValid());

	const netP2pCrypt::Key& localPeerKey = m_LocalPeerAddr.GetPeerKey();
	const netP2pCrypt::Key& remotePeerKey = m_RemotePeerAddr.GetPeerKey();

	int result = memcmp(localPeerKey.GetRawKeyPtr(), remotePeerKey.GetRawKeyPtr(), netP2pCrypt::Key::KEY_LENGTH_IN_BYTES);

	if(result == 0)
	{
		// same peer key

		if(m_LocalPeerAddr.GetPeerId() < m_RemotePeerAddr.GetPeerId())
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}
	else if(result < 0)
	{
		return 1;
	}
	else
	{
		return 2;
	}
}

void
netIceSession::DevInit()
{
#if !__NO_OUTPUT
	static bool sizeOfSpamOnce = true;
	if(sizeOfSpamOnce)
	{
		sizeOfSpamOnce = false;
		netIceDebug1("SessionId:%u", m_SessionId);
		netIceDebug3("sizeof(netIceSession):%" SIZETFMT "u", sizeof(netIceSession));
		netIceDebug3("sizeof(m_LocalCandidates):%" SIZETFMT "u", sizeof(m_LocalCandidates));
		netIceDebug3("sizeof(m_RemoteCandidates):%" SIZETFMT "u", sizeof(m_RemoteCandidates));
		netIceDebug3("sizeof(m_CheckList):%" SIZETFMT "u", sizeof(m_CheckList));
		netIceDebug3("sizeof(m_TriggeredQueue):%" SIZETFMT "u", sizeof(m_TriggeredQueue));
		netIceDebug3("sizeof(m_ValidList):%" SIZETFMT "u", sizeof(m_ValidList));
		netIceDebug3("sizeof(m_DiscoveredPairs):%" SIZETFMT "u", sizeof(m_DiscoveredPairs));
		netIceDebug3("sizeof(m_Blacklist):%" SIZETFMT "u", sizeof(m_Blacklist));
		netIceDebug3("sizeof(m_Transactor):%" SIZETFMT "u", sizeof(m_Transactor));

		netIceDebug3("netIceSessionOffer::MAX_EXPORT_SIZE_IN_BYTES: %u", netIceSessionOffer::MAX_EXPORT_SIZE_IN_BYTES);
		netIceDebug3("netIceSessionAnswer::MAX_EXPORT_SIZE_IN_BYTES: %u", netIceSessionAnswer::MAX_EXPORT_SIZE_IN_BYTES);
		netIceDebug3("netIceSessionPing::MAX_EXPORT_SIZE_IN_BYTES: %u", netIceSessionPing::MAX_EXPORT_SIZE_IN_BYTES);
		netIceDebug3("netIceSessionPong::MAX_EXPORT_SIZE_IN_BYTES: %u", netIceSessionPong::MAX_EXPORT_SIZE_IN_BYTES);
		netIceDebug3("netIceSessionDebugInfo::MAX_EXPORT_SIZE_IN_BYTES: %u", netIceSessionDebugInfo::MAX_EXPORT_SIZE_IN_BYTES);
	}

	if((m_LocalPeerAddr.GetPublicAddress().GetIp().IsValid() == false) || (m_LocalPeerAddr.GetPublicAddress().GetPort() == 0))
	{
		netIceDebug("We don't have a valid public address. Relay may not be responding to pings.");
	}
#endif

#if !__FINAL || defined(RSG_LEAN_CLIENT)
	if(PARAM_neticefailbasedonnatcombo.Get())
	{
		netNatType localNatType = netHardware::GetNatType();
		netNatType remoteNatType = m_RemotePeerAddr.GetNatType();
		unsigned numStricts = 0;
		unsigned numModerates = 0;
		if(localNatType == NET_NAT_STRICT)
		{
			numStricts++;
		}
		else if(localNatType == NET_NAT_MODERATE)
		{
			numModerates++;
		}

		if(remoteNatType == NET_NAT_STRICT)
		{
			numStricts++;
		}
		else if(remoteNatType == NET_NAT_MODERATE)
		{
			numModerates++;
		}

		m_ForceFail = (numStricts == 2) || (numStricts == 1 && numModerates == 1);

		if(m_ForceFail)
		{
			netIceDebug1("Force failing due to NAT type combination and -neticefailbasedonnatcombo");
		}
	}

	u32 agreedHash = 0;
	bool needAgreedHash = PARAM_neticefailprobability.Get() || PARAM_neticeforcedelay.Get();
	if(needAgreedHash)
	{
		// have both sides agree on a number without communicating
		const netPeerId& localPeerId = m_LocalPeerAddr.GetPeerId();
		const netPeerId& remotePeerId = m_RemotePeerAddr.GetPeerId();
		const netPeerId lowerPeerId = Min(localPeerId, remotePeerId);
		const netPeerId higherPeerId = Max(localPeerId, remotePeerId);
		netAssert((lowerPeerId < higherPeerId) && (lowerPeerId != higherPeerId));

		agreedHash = atDataHash((char*)&lowerPeerId, sizeof(u64), agreedHash);
		agreedHash = atDataHash((char*)&higherPeerId, sizeof(u64), agreedHash);
	}

	unsigned failProbability = 0;
	if(PARAM_neticefailprobability.Get(failProbability))
	{
		netAssert(agreedHash != 0);
		netRandom rng;
		rng.SetFullSeed((u64)agreedHash);

		if(rng.GetFloat() < (failProbability / 100.0f))
		{
			// only simulate the failure if both sides have a valid relay address,
			// since this command line is often used to test p2p relaying which requires
			// falling back to the relay server first.
			if(m_LocalPeerAddr.GetRelayAddress().IsRelayServerAddr() && m_RemotePeerAddr.GetRelayAddress().IsRelayServerAddr())
			{
				netIceDebug1("-%s is enabled, simulating failure.", PARAM_neticefailprobability.GetName());
				m_ForceFail = true;
			}
			else
			{
				netIceDebug1("-%s is enabled, but cannot simulate a failure because one or both sides don't have a valid relay address.", PARAM_neticefailprobability.GetName());
			}				
		}
		else
		{
			netIceDebug1("-%s is enabled, but rolled a saving throw.", PARAM_neticefailprobability.GetName());
		}
	}

	if(PARAM_neticeforcefail.Get(m_ForceFailTimeoutMs) || PARAM_neticeforcefail.Get() || PARAM_netforceuserelay.Get())
	{
		m_ForceFail = true;
	}

	const char* failGamers = nullptr;
	if(PARAM_neticefailgamers.Get(failGamers))
	{
		// strtok modifies the string, make a copy
		char* copy = StringDuplicate(failGamers);
		char *ghStr = strtok((char*)copy, ",");
		while(ghStr != NULL)
		{
			rlGamerHandle gh;
			if(netVerify(gh.FromString(ghStr) && gh.IsValid()) && (gh == m_RemotePeerAddr.GetGamerHandle()))
			{
				m_ForceFail = true;
				break;
			}
			ghStr = strtok(NULL, ",");
		}
		StringFree(copy);
	}

	const char* succeedGamers = nullptr;
	if(PARAM_neticesucceedgamers.Get(succeedGamers))
	{
		m_ForceFail = true;

		// strtok modifies the string, make a copy
		char* copy = StringDuplicate(succeedGamers);
		char *ghStr = strtok((char*)copy, ",");
		while(ghStr != NULL)
		{
			rlGamerHandle gh;
			if(netVerify(gh.FromString(ghStr) && gh.IsValid()) && (gh == m_RemotePeerAddr.GetGamerHandle()))
			{
				m_ForceFail = false;
				break;
			}
			ghStr = strtok(NULL, ",");
		}
		StringFree(copy);

		char ghBuf[RL_MAX_GAMER_HANDLE_CHARS];
		if(m_ForceFail)
		{
			netIceDebug1("-neticesucceedgamers is enabled, %s not in list, failing.", m_RemotePeerAddr.GetGamerHandle().ToString(ghBuf));
		}
		else
		{
			netIceDebug1("-neticesucceedgamers is enabled, %s is in list, not failing.", m_RemotePeerAddr.GetGamerHandle().ToString(ghBuf));
		}
	}

	const char* forceDelay = NULL;
	if(PARAM_neticeforcedelay.Get(forceDelay) && forceDelay)
	{
		unsigned forceDelayMinMs = 0;
		unsigned forceDelayMaxMs = 0;

		if(sscanf(forceDelay, "%u-%u", &forceDelayMinMs, &forceDelayMaxMs) == 2)
		{
			netAssert(forceDelayMinMs <= forceDelayMaxMs);
			netAssert(agreedHash != 0);
			netRandom rng;
			rng.SetFullSeed((u64)agreedHash);
			m_ForceDelayMs = rng.GetRanged(forceDelayMinMs, forceDelayMaxMs);
		}
		else if(sscanf(forceDelay, "%u", &forceDelayMinMs) == 1)
		{
			m_ForceDelayMs = forceDelayMinMs;
		}

		netIceDebug1("-neticeforcedelay is enabled. Delaying this ICE session for %d ms.", m_ForceDelayMs);
	}

	if(PARAM_neticetestwrongrelayaddr.Get())
	{
		netIceDebug("-neticetestwrongrelayaddr is enabled.");

		// test case where we don't have the remote peer's correct relay address
		netSocketAddress proxyAddr = m_RemoteRelayAddr.GetProxyAddress();
		netSocketAddress targetAddr = m_RemoteRelayAddr.GetTargetAddress();
		u8 tokenBuf[netRelayToken::MAX_TOKEN_BUF_SIZE] = {1};
		netRelayToken relayToken(tokenBuf);
		targetAddr.Init(targetAddr.GetIp(), targetAddr.GetPort() + 1);
		m_RemoteRelayAddr.Init(proxyAddr, relayToken, targetAddr);
	}
#endif
}

bool
netIceSession::Init(const unsigned OUTPUT_ONLY(tunnelRequestId),
					const unsigned sessionId,
					netConnectionManager* cxnMgr,
					const netPeerAddress& peerAddr,
					const u64 gameSessionToken,
					const u64 gameSessionId)
{
	rtry
	{
		this->Clear();

		rverify(cxnMgr, catchall, );
		rverify(peerAddr.IsValid(), catchall, );

		OUTPUT_ONLY(m_TunnelRequestId = tunnelRequestId);
		m_SessionId = sessionId;
		m_GameSessionToken = gameSessionToken;
		m_GameSessionId = gameSessionId;
		m_SendTelemetry = rlTelemetry::ShouldSendMetric(rlStandardMetrics::NatTraversal::MetricId(), rlStandardMetrics::NatTraversal::LogChannel(), rlStandardMetrics::NatTraversal::SamplingMode()  OUTPUT_ONLY(, "netIceSession"));

		rverify(peerAddr.GetGamerHandle().IsSupportedGamerHandle(), catchall, netIceError("Unsupported gamerhandle."));

		rverify(netPeerAddress::GetLocalPeerAddress(rlPresence::GetActingUserIndex(), &m_LocalPeerAddr), catchall, );
		rverify(m_LocalPeerAddr.GetPrivateAddress().IsValid(), catchall, );

		/*
			if the relay is down, we won't have a server reflexive address but might still
			be able to connect using only host candidates if the two peers are behind
			the same NAT (eg. both on a corporate or home network) or both are not
			behind NATs.
		*/

		m_CxnMgr = cxnMgr;
		m_Transactor.Init(m_CxnMgr);
		m_RemotePeerAddr = peerAddr;
		m_RemoteRelayAddr = m_RemotePeerAddr.GetRelayAddress();

		m_StopWatch = netStopWatch();
		m_StopWatch.Start();
		m_ChecklistTimer.InitMilliseconds((IsLocalStrict() && (m_RemotePeerAddr.GetNatType() != NET_NAT_STRICT)) ? CHECK_START_DELAY_STRICT_NAT_MS : CHECK_INTERVAL_MS);
		m_TerminationTimer.InitMilliseconds(MAX_ICE_SESSION_TME_MS);
		m_ChecklistTimer.Stop();

		netIceDebug1("Local Peer Address: %s", m_LocalPeerAddr.ToString());
		netIceDebug1("Remote Peer Address: %s", m_RemotePeerAddr.ToString());

		netIceDebug1("Local Relay:" NET_ADDR_FMT", Remote Relay:" NET_ADDR_FMT,
					 NET_ADDR_FOR_PRINTF(m_LocalPeerAddr.GetRelayAddress().GetProxyAddress()),
					 NET_ADDR_FOR_PRINTF(m_RemoteRelayAddr.GetProxyAddress()));

		netIceDebug1("Local NAT Type: %s, Remote NAT Type: %s",
					 netHardware::GetNatTypeString(m_LocalPeerAddr.GetNatType()),
					 netHardware::GetNatTypeString(m_RemotePeerAddr.GetNatType()));

		DevInit();

		m_State = STATE_PENDING;

		return true;
	}
	rcatchall
	{

	}

	return false;
}

#if !__NO_OUTPUT
const char*
netIceSession::FailReasonToString(const FailReason failReason) const
{
#define ICE_FAIL_REASON_CASE(x) case x: return (&#x[strlen("FAIL_REASON_")]); break;
	switch(failReason)
	{
		ICE_FAIL_REASON_CASE(FAIL_REASON_NONE);
		ICE_FAIL_REASON_CASE(FAIL_REASON_EMERGENCY_TIMER);
		ICE_FAIL_REASON_CASE(FAIL_REASON_COULD_NOT_SEND_INDIRECT_OFFER);
		ICE_FAIL_REASON_CASE(FAIL_REASON_INDIRECT_OFFER_TIMED_OUT);
		ICE_FAIL_REASON_CASE(FAIL_REASON_NO_VALID_PAIRS);
		ICE_FAIL_REASON_CASE(FAIL_REASON_CANCELLED);
		ICE_FAIL_REASON_CASE(FAIL_REASON_START_AS_OFFERER);
		ICE_FAIL_REASON_CASE(FAIL_REASON_RECEIVE_OFFER);
		ICE_FAIL_REASON_CASE(FAIL_REASON_RCV_OFFER_GLC);
		ICE_FAIL_REASON_CASE(FAIL_REASON_RCV_OFFER_GRC);
		ICE_FAIL_REASON_CASE(FAIL_REASON_RCV_OFFER_CHECKLIST);
		ICE_FAIL_REASON_CASE(FAIL_REASON_RCV_OFFER_SEND_ANSWER);
		ICE_FAIL_REASON_CASE(FAIL_REASON_START_AS_OFFERER_STATUS);
		ICE_FAIL_REASON_CASE(FAIL_REASON_START_AS_OFFERER_NET_ADDR);
		ICE_FAIL_REASON_CASE(FAIL_REASON_START_AS_OFFERER_GLC);
		ICE_FAIL_REASON_CASE(FAIL_REASON_START_AS_OFFERER_GRC);
		ICE_FAIL_REASON_CASE(FAIL_REASON_START_AS_OFFERER_CHECKLIST);
		ICE_FAIL_REASON_CASE(FAIL_REASON_COULD_NOT_SEND_DIRECT_OR_INDIRECT_OFFERS);
		ICE_FAIL_REASON_CASE(FAIL_REASON_SIMULATED_FAIL);
		ICE_FAIL_REASON_CASE(FAIL_REASON_REMOTE_TERMINATED);
		ICE_FAIL_REASON_CASE(FAIL_REASON_PORT_PRESERVING_SYMMETRIC);
		ICE_FAIL_REASON_CASE(FAIL_REASON_PORT_RANDOM_SYMMETRIC);
		ICE_FAIL_REASON_CASE(FAIL_REASON_QUICK_CONNECT);
		ICE_FAIL_REASON_CASE(FAIL_REASON_NON_DETERMINISTIC);
		ICE_FAIL_REASON_CASE(FAIL_REASON_INDIRECT_OFFER_NO_LOCAL_RELAY_ADDR);
		ICE_FAIL_REASON_CASE(FAIL_REASON_INDIRECT_OFFER_NO_LOCAL_RELAY_MAPPED_ADDR);
		ICE_FAIL_REASON_CASE(FAIL_REASON_INDIRECT_OFFER_NO_REMOTE_RELAY);
	}
#undef ICE_FAIL_REASON_CASE

	netAssertf(false, "netIceSession::FailReasonToString :: Unhandled fail reason");
	return "UNKNOWN FAIL REASON";
}
#endif

bool
netIceSession::ShouldDelayReport(const CompletionType completionType)
{
	// the relay route check can complete after nominating a candidate. 
	// Delay sending the telemetry until the termination timer expires.
	return (completionType == COMPLETE_SUCCEEDED) &&
			netIceTunneler::sm_AllowRelayRouteCheck;
}

void
netIceSession::Report(const CompletionType completionType, const FailReason failReason)
{
	switch(completionType)
	{
	case COMPLETE_SUCCEEDED:
		{
			netAssert(m_NetAddr.IsValid());

			netIceTunneler::m_NumSuccessfulSessions++;
			netIceDebug1("ICE Tunnel request SUCCEEDED with address [" NET_ADDR_FMT "] after %u ms",
						 NET_ADDR_FOR_PRINTF(m_NetAddr), this->GetElapsedTime());
		}
		break;
	case COMPLETE_FAILED:
		{
			netIceTunneler::m_NumFailedSessions++;
			netIceDebug1("ICE Tunnel request FAILED after %u ms with fail reason: %s",
						 this->GetElapsedTime(), FailReasonToString(failReason));
		}
		break;
	}

	char lpriv[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};
	char lpub[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};
	char lrly[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};

	char rpriv[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};
	char rpub[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};
	char rrly[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};

	m_LocalPeerAddr.GetPrivateAddress().Format(lpriv, true);
	m_LocalPeerAddr.GetPublicAddress().Format(lpub, true);
	m_LocalPeerAddr.GetRelayAddress().GetProxyAddress().Format(lrly, true);
	m_RemotePeerAddr.GetPrivateAddress().Format(rpriv, true);
	m_RemoteRelayAddr.GetTargetAddress().Format(rpub, true);
	m_RemoteRelayAddr.GetProxyAddress().Format(rrly, true);

	netNatType localNatType = m_LocalPeerAddr.GetNatType();
	netNatType remoteNatType = m_RemotePeerAddr.GetNatType();

	// record telemetry
	rlStandardMetrics::NatTraversal metric(COMPLETE_SUCCEEDED == completionType,
										   (unsigned)failReason,
										   m_Canceled,
										   netIceTunneler::GetNumConcurrentSessions(),
										   netIceTunneler::GetPeakConcurrentSessions(),
										   netIceTunneler::GetTotalSessions(),
										   m_LocalCandidateType,
										   m_RemoteCandidateType,
										   lpriv,
										   lpub,
										   lrly,
										   rpriv,
										   rpub,
										   rrly,
										   this->GetElapsedTime(),
										   !m_Bilateral,
										   m_Role == ROLE_CONTROLLING,
										   m_HadRoleConflict,
										   m_RelayAssisted,
										   m_DirectOfferStatus,
										   m_IndirectOfferStatus,
										   m_SymmetricSocket.m_State,
										   m_NumOffersSent,
										   m_NumOffersReceived,
										   m_NumAnswersSent,
										   m_NumAnswersReceived,
										   m_NumPingsSent,
										   m_NumPingsReceived,
										   m_NumPongsSent,
										   m_NumPongsReceived,
										   m_GameSessionToken,
										   m_GameSessionId,
										   localNatType,
										   remoteNatType,
										   m_UnmatchedPublicIpState,
										   m_RelayRouteCheckState);
	
	if(netIceTunneler::sm_AllowIceTunnelerTelemetry && m_SendTelemetry)
	{
		// this metric is also sent in the debug info message (below).
		// The tunable only enables/disables sending to telemetry.
		rlTelemetry::Write(rlPresence::GetActingUserIndex(), metric);
	}

#if !__NO_OUTPUT
	char metricBuf[2048] = {0};
	RsonWriter rw(metricBuf, RSON_FORMAT_JSON);
	metric.Write(&rw);
	netIceDebug3("Writing telemetry: %s", rw.ToString());

	bool remoteSucceeded = m_RemoteState == STATE_SUCCEEDED; 

	if((m_RemoteState == STATE_SUCCEEDED) || (m_RemoteState == STATE_FAILED))
	{
		if(remoteSucceeded && !Succeeded())
		{
			netIceDebug2("Remote session succeeded. Local session failed. One way success.");
		}
		else if(!remoteSucceeded && Succeeded())
		{
			netIceDebug2("Remote session failed. Local session succeeded. One way success.");
		}
	}
	else
	{
		netIceDebug2("Remote session still in progress.");
	}
#endif

#if !__NO_OUTPUT
	unsigned totalSessions = netIceTunneler::m_NumFailedSessions + netIceTunneler::m_NumSuccessfulSessions;
	float failRate = ((float)netIceTunneler::m_NumFailedSessions / (float)totalSessions) * 100.0f;
	netDebug("Num Failed Sessions: %u, Num Successful Sessions: %u, Total Sessions: %u, Local Fail Rate: %.2f",
			 netIceTunneler::m_NumFailedSessions, netIceTunneler::m_NumSuccessfulSessions, totalSessions, failRate);
#endif

	if(m_SendDebugInfo)
	{
		char metricBuf[2048] = {0};
		RsonWriter rw(metricBuf, RSON_FORMAT_JSON);
		metric.Write(&rw);
		SendDebugInfo(metricBuf);
	}

#if RSG_PC
	// if we're on a Strict NAT and failing the majority of our NAT traversals, tell the SCUI
	// to display additional info in the Network Information UI on how to resolve (includes link to support article)

	if(m_LocalPeerAddr.GetNatType() == NET_NAT_STRICT)
	{
#if GTA_VERSION
		if(g_rlPc.GetNetworkInterface())
		{
			g_rlPc.GetNetworkInterface()->SetShowNatTypeWarning(netIceTunneler::ShouldShowConnectivityTroubleshooting());
		}
#else
		if(g_rlPc.GetNetworkInterface().IsValid())
		{
			g_rlPc.GetNetworkInterface().SetShowNatTypeWarning(netIceTunneler::ShouldShowConnectivityTroubleshooting());
		}
#endif
	}
#endif
}

void 
netIceSession::Detach(const FailReason failReason)
{
	for(unsigned i = 0; i < COUNTOF(m_Status); ++i)
	{
		if(m_NetAddrPtr[i])
		{
			if(m_State == STATE_SUCCEEDED)
			{
				*(m_NetAddrPtr[i]) = m_NetAddr;
			}
			else
			{
				m_NetAddrPtr[i]->Clear();
			}

			m_NetAddrPtr[i] = NULL;
		}

		if(m_RelayAddrPtr[i])
		{
			if(m_RemoteRelayAddr.IsRelayServerAddr() &&
				m_RemoteRelayAddr.GetTargetAddress().IsValid() &&
#if !__FINAL
				!PARAM_neticetestwrongrelayaddr.Get() &&
#endif
				(m_RelayCommunicationSucceeded ||
				!m_RemotePeerAddr.GetRelayAddress().IsRelayServerAddr() ||
				(m_RemoteRelayAddr != m_RemotePeerAddr.GetRelayAddress())))
			{
				m_RelayAddrPtr[i]->Init(m_RemoteRelayAddr.GetProxyAddress(), netRelayToken::INVALID_RELAY_TOKEN, m_RemoteRelayAddr.GetTargetAddress());
				netIceDebug3("Notifying the caller that the remote peer's relay address is " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(*m_RelayAddrPtr[i]));
			}

			m_RelayAddrPtr[i] = NULL;
		}

		if(m_Status[i])
		{
			if(m_State == STATE_SUCCEEDED)
			{
				m_Status[i]->SetSucceeded();
			}
			else
			{
#if !__NO_OUTPUT
				if(failReason == FAIL_REASON_QUICK_CONNECT)
				{
					netIceDebug3("Detaching for QuickConnect. Not actually a failure, will continue to search for a direct path.");
				}
#endif
				m_Status[i]->SetFailed(failReason);
			}

			m_Status[i] = NULL;
		}
	}
}

void
netIceSession::QuickConnect()
{
	// if we've hit our tunable QuickConnect threshold, then tell the caller we've failed to find a
	// direct path in time. The game will then start communicating via relay address if possible.
	// The ICE session continues to search for a direct path and reassigns the CxnMgr's endpoint
	// to the direct address if it finds one.

	Detach(FAIL_REASON_QUICK_CONNECT);
}

void
netIceSession::Complete(const CompletionType completionType, const FailReason failReason, const CandidatePair* nominatedPair)
{
	if(!this->Pending())
	{
		return;
	}

	m_FailReason = failReason;

	if(COMPLETE_SUCCEEDED == completionType)
    {
		netAssert(nominatedPair && nominatedPair->IsNominated() && nominatedPair->GetRemoteCandidate().IsValid());
		m_NetAddr.Init(nominatedPair->GetRemoteCandidate().GetTransportAddress());

        m_State = STATE_SUCCEEDED;

 		if(m_CxnMgr)
 		{
 			m_CxnMgr->UpdateEndpointAddress(m_RemotePeerAddr.GetPeerId(), m_NetAddr, m_RelayCommunicationSucceeded ? m_RemoteRelayAddr : netAddress::INVALID_ADDRESS);
 		}
    }
    else
    {
		m_State = STATE_FAILED;
    }

	CancelPresenceQuery();
	CancelSymmetricInference();
	CancelSymmetricSocket();
	CancelCrossRelayCandidateCheck();

	Detach(failReason);

	if(nominatedPair)
	{
		m_LocalCandidateType = nominatedPair->GetLocalCandidate().GetType();
		m_RemoteCandidateType = nominatedPair->GetRemoteCandidate().GetType(),

		UpdateUnmatchedPublicIpState(nominatedPair->GetRemoteCandidate().GetTransportAddress(), true);
	}

	// if we're not delaying the report, send it now.
	// Otherwise send it when the termination timer expires.
	if(!ShouldDelayReport(completionType))
	{
		Report(completionType, failReason);
	}

	/*
		8.3.  Freeing Candidates
		Once ICE processing has reached the Completed state for all peers 
		for media streams using those candidates, the agent SHOULD wait an 
		additional three seconds, and then it MAY cease responding to 
		checks or generating triggered checks on that candidate.
	*/
	if(COMPLETE_SUCCEEDED == completionType)
	{
		netIceDebug3("Setting termination timer for %u ms", TERMINATION_TIME_MS);
		m_TerminationTimer.InitMilliseconds(TERMINATION_TIME_MS);
	}
	else
	{
		Clear();
	}
}

//////////////////////////////////////////////////////////////////////////
//  netIceTunneler
//////////////////////////////////////////////////////////////////////////

netIceSession m_Sessions[MAX_ACTIVE_ICE_SESSIONS];
typedef inlist<netIceSession, &netIceSession::m_ListLink> SessionList;
SessionList m_ActiveSessions;
SessionList m_FreeSessions;

sysCriticalSectionToken netIceTunneler::m_Cs;

netConnectionManager* netIceTunneler::m_CxnMgr;
netConnectionManager::Delegate netIceTunneler::m_CxnEventDelegate;
bool netIceTunneler::m_Initialized = false;
unsigned netIceTunneler::m_NumConcurrentSessions = 0;
unsigned netIceTunneler::m_PeakConcurrentSessions = 0;
unsigned netIceTunneler::m_TotalSessions = 0;
unsigned netIceTunneler::m_NumFailedSessions = 0;
unsigned netIceTunneler::m_NumSuccessfulSessions = 0;

netSocketAddress netIceTunneler::sm_LastRelayPingResult;
unsigned netIceTunneler::sm_NumFailsBeforeWarning = DEFAULT_NUM_FAILS_BEFORE_WARNING; 
unsigned netIceTunneler::sm_FailRateBeforeWarning = DEFAULT_NAT_FAIL_RATE_BEFORE_WARNING; 
bool netIceTunneler::sm_AllowSymmetricInference = true;
unsigned netIceTunneler::sm_SymmetricSocketMode = SymmetricSocket::Condition::NEVER;
bool netIceTunneler::sm_AllowCanonicalCandidate = true; 
bool netIceTunneler::sm_AllowSameHostCandidate = true; 
bool netIceTunneler::sm_AllowCrossRelayCheck = true; 
bool netIceTunneler::sm_AllowCrossRelayCheckDirectOfferTimedOut = false;
bool netIceTunneler::sm_AllowCrossRelayCheckRemoteStrict = true;
bool netIceTunneler::sm_AllowCrossRelayCheckLocalHost = false;
bool netIceTunneler::sm_AllowRelayRouteCheck = true;
bool netIceTunneler::sm_AllowPeerReflexiveList = true; 
bool netIceTunneler::sm_AllowPresenceQuery = true; 
bool netIceTunneler::sm_PeerReflexiveHighestPlusOne = true; 
bool netIceTunneler::sm_AllowIceTunnelerTelemetry = true; 
bool netIceTunneler::sm_AllowLocalRelayRefresh = true;
unsigned netIceTunneler::sm_PeerReflexiveTimeoutSec = 15 * 60; 
unsigned netIceTunneler::sm_MaxFailedSymmetricInferenceTests = 50; // currently used as a bool, anything > 0 enables symmetric inference for non-Strict NATs
unsigned netIceTunneler::sm_QuickConnectTimeoutMs = 6000; // 0 = QuickConnect is disabled
unsigned netIceTunneler::sm_RoleTieBreakerStrategy = 0;
bool netIceTunneler::sm_AllowDirectOffers = true;
bool netIceTunneler::sm_AllowDirectAnswers = true;
bool netIceTunneler::sm_AllowReservedCandidates = true;
bool netIceTunneler::sm_AllowReservedRfc1918Candidates = true;
bool netIceTunneler::sm_RequireRelaySuccessForQuickConnect = false;

netIceTunneler::PeerReflexiveCandidates netIceTunneler::m_PeerReflexiveCandidates;

// We send our session id in messages so the remote peer can send it back 
// to us to correlate which session the message is for.
unsigned netIceTunneler::s_NextId = NET_INVALID_ICE_SESSION_ID;

void 
netIceTunneler::SetNumNatFailsBeforeWarning(const unsigned numNatFailsBeforeWarning)
{
	if(sm_NumFailsBeforeWarning != numNatFailsBeforeWarning)
	{
		netDebug("SetNumNatFailsBeforeWarning :: %d", numNatFailsBeforeWarning);
		sm_NumFailsBeforeWarning = numNatFailsBeforeWarning;
	}
}

void 
netIceTunneler::SetNatFailRateBeforeWarning(const unsigned failRateBeforeWarning)
{
	if(sm_FailRateBeforeWarning != failRateBeforeWarning)
	{
		netDebug("SetNatFailRateBeforeWarning :: %d", failRateBeforeWarning);
		sm_FailRateBeforeWarning = failRateBeforeWarning;
	}
}

void 
netIceTunneler::SetAllowSymmetricInference(const bool allowSymmetricInference)
{
	if(sm_AllowSymmetricInference != allowSymmetricInference)
	{
		netDebug("SetAllowSymmetricInference :: %s", allowSymmetricInference ? "true" : "false");
		sm_AllowSymmetricInference = allowSymmetricInference;
	}
}

void 
netIceTunneler::SetSymmetricSocketMode(const unsigned symmetricSocketMode)
{
	if(sm_SymmetricSocketMode != symmetricSocketMode)
	{
		netDebug("SetSymmetricSocketMode :: %u", symmetricSocketMode);
		sm_SymmetricSocketMode = symmetricSocketMode;
	}
}

void 
netIceTunneler::SetAllowCanonicalCandidate(const bool allowCanonicalCandidate)
{
	if(sm_AllowCanonicalCandidate != allowCanonicalCandidate)
	{
		netDebug("SetAllowCanonicalCandidate :: %s", allowCanonicalCandidate ? "true" : "false");
		sm_AllowCanonicalCandidate = allowCanonicalCandidate;
	}
}

void 
netIceTunneler::SetAllowSameHostCandidate(const bool allowSameHostCandidate)
{
	if(sm_AllowSameHostCandidate != allowSameHostCandidate)
	{
		netDebug("SetAllowSameHostCandidate :: %s", allowSameHostCandidate ? "true" : "false");
		sm_AllowSameHostCandidate = allowSameHostCandidate;
	}
}

void 
netIceTunneler::SetAllowCrossRelayCheck(const bool allowCrossRelayCheck)
{
	if(sm_AllowCrossRelayCheck != allowCrossRelayCheck)
	{
		netDebug("SetAllowCrossRelayCheck :: %s", allowCrossRelayCheck ? "true" : "false");
		sm_AllowCrossRelayCheck = allowCrossRelayCheck;
	}
}

void
netIceTunneler::SetAllowRelayRouteCheck(const bool allowRelayRouteCheck)
{
	if(sm_AllowRelayRouteCheck != allowRelayRouteCheck)
	{
		netDebug("SetAllowRelayRouteCheck :: %s", allowRelayRouteCheck ? "true" : "false");
		sm_AllowRelayRouteCheck = allowRelayRouteCheck;
	}
}

void 
netIceTunneler::SetAllowCrossRelayCheckDirectOfferTimedOut(const bool allowCrossRelayCheck)
{
	if(sm_AllowCrossRelayCheckDirectOfferTimedOut != allowCrossRelayCheck)
	{
		netDebug("SetAllowCrossRelayCheckDirectOfferTimedOut :: %s", allowCrossRelayCheck ? "true" : "false");
		sm_AllowCrossRelayCheckDirectOfferTimedOut = allowCrossRelayCheck;
	}
}

void 
netIceTunneler::SetAllowCrossRelayCheckRemoteStrict(const bool allowCrossRelayCheck)
{
	if(sm_AllowCrossRelayCheckRemoteStrict != allowCrossRelayCheck)
	{
		netDebug("SetAllowCrossRelayCheckRemoteStrict :: %s", allowCrossRelayCheck ? "true" : "false");
		sm_AllowCrossRelayCheckRemoteStrict = allowCrossRelayCheck;
	}
}

void 
netIceTunneler::SetAllowCrossRelayCheckLocalHost(const bool allowCrossRelayCheck)
{
	if(sm_AllowCrossRelayCheckLocalHost != allowCrossRelayCheck)
	{
		netDebug("SetAllowCrossRelayCheckLocalHost :: %s", allowCrossRelayCheck ? "true" : "false");
		sm_AllowCrossRelayCheckLocalHost = allowCrossRelayCheck;
	}
}

void 
netIceTunneler::SetAllowPeerReflexiveList(const bool allowPeerReflexiveList)
{
	if(sm_AllowPeerReflexiveList != allowPeerReflexiveList)
	{
		netDebug("SetAllowPeerReflexiveList :: %s", allowPeerReflexiveList ? "true" : "false");
		sm_AllowPeerReflexiveList = allowPeerReflexiveList;
	}
}

void 
netIceTunneler::SetAllowPresenceQuery(const bool allowPresenceQuery)
{
	if(sm_AllowPresenceQuery != allowPresenceQuery)
	{
		netDebug("SetAllowPresenceQuery :: %s", allowPresenceQuery ? "true" : "false");
		sm_AllowPresenceQuery = allowPresenceQuery;
	}
}

void 
netIceTunneler::SetAllowPeerReflexiveHighestPlusOne(const bool peerReflexiveHighestPlusOne)
{
	if(sm_PeerReflexiveHighestPlusOne != peerReflexiveHighestPlusOne)
	{
		netDebug("SetAllowPeerReflexiveHighestPlusOne :: %s", peerReflexiveHighestPlusOne ? "true" : "false");
		sm_PeerReflexiveHighestPlusOne = peerReflexiveHighestPlusOne;
	}
}

void 
netIceTunneler::SetAllowIceTunnelerTelemetry(const bool allowIceTunnelerTelemetry)
{
	if(sm_AllowIceTunnelerTelemetry != allowIceTunnelerTelemetry)
	{
		netDebug("SetAllowIceTunnelerTelemetry :: %s", allowIceTunnelerTelemetry ? "true" : "false");
		sm_AllowIceTunnelerTelemetry = allowIceTunnelerTelemetry;
	}
}

void 
netIceTunneler::SetAllowLocalRelayRefresh(const bool allowLocalRelayRefresh)
{
	if(sm_AllowLocalRelayRefresh != allowLocalRelayRefresh)
	{
		netDebug("SetAllowLocalRelayRefresh :: %s", allowLocalRelayRefresh ? "true" : "false");
		sm_AllowLocalRelayRefresh = allowLocalRelayRefresh;
	}
}

void 
netIceTunneler::SetPeerReflexiveTimeoutSec(const unsigned peerReflexiveTimeoutSec)
{
	if(sm_PeerReflexiveTimeoutSec != peerReflexiveTimeoutSec)
	{
		netDebug("SetPeerReflexiveTimeoutSec :: %d", peerReflexiveTimeoutSec);
		sm_PeerReflexiveTimeoutSec = peerReflexiveTimeoutSec;
	}
}

void 
netIceTunneler::SetMaxFailedSymmetricInferenceTests(const unsigned maxFailedSymmetricInferenceTests)
{
	if(sm_MaxFailedSymmetricInferenceTests != maxFailedSymmetricInferenceTests)
	{
		netDebug("SetMaxFailedSymmetricInferenceTests :: %u", maxFailedSymmetricInferenceTests);
		sm_MaxFailedSymmetricInferenceTests = maxFailedSymmetricInferenceTests;
	}
}

void 
netIceTunneler::SetQuickConnectTimeoutMs(const unsigned quickConnectTimeout)
{
	if(sm_QuickConnectTimeoutMs != quickConnectTimeout)
	{
		netDebug("SetQuickConnectTimeoutMs :: %u", quickConnectTimeout);
		sm_QuickConnectTimeoutMs = quickConnectTimeout;
	}
}

void 
netIceTunneler::SetRoleTieBreakerStrategy(const unsigned roleTieBreakerStrategy)
{
	if(sm_RoleTieBreakerStrategy != roleTieBreakerStrategy)
	{
		netDebug("SetRoleTieBreakerStrategy :: %u", roleTieBreakerStrategy);
		sm_RoleTieBreakerStrategy = roleTieBreakerStrategy;
	}
}

void 
netIceTunneler::SetAllowDirectOffers(const bool allowDirectOffers)
{
	if(sm_AllowDirectOffers != allowDirectOffers)
	{
		netDebug("SetAllowDirectOffers :: %s", allowDirectOffers ? "true" : "false");
		sm_AllowDirectOffers = allowDirectOffers;
	}
}

void 
netIceTunneler::SetAllowDirectAnswers(const bool allowDirectAnswers)
{
	if(sm_AllowDirectAnswers != allowDirectAnswers)
	{
		netDebug("SetAllowDirectAnswers :: %s", allowDirectAnswers ? "true" : "false");
		sm_AllowDirectAnswers = allowDirectAnswers;
	}
}

void 
netIceTunneler::SetAllowReservedCandidates(const bool allowReservedCandidates)
{
	if(sm_AllowReservedCandidates != allowReservedCandidates)
	{
		netDebug("SetAllowReservedCandidates :: %s", allowReservedCandidates ? "true" : "false");
		sm_AllowReservedCandidates = allowReservedCandidates;
	}
}

void 
netIceTunneler::SetAllowReservedRfc1918Candidates(const bool allowReservedRfc1918Candidates)
{
	if(sm_AllowReservedRfc1918Candidates != allowReservedRfc1918Candidates)
	{
		netDebug("SetAllowReservedRfc1918Candidates :: %s", allowReservedRfc1918Candidates ? "true" : "false");
		sm_AllowReservedRfc1918Candidates = allowReservedRfc1918Candidates;
	}
}

void
netIceTunneler::SetRequireRelaySuccessForQuickConnect(const bool requireRelaySuccessForQuickConnect)
{
	if(sm_RequireRelaySuccessForQuickConnect != requireRelaySuccessForQuickConnect)
	{
		netDebug("SetRequireRelaySuccessForQuickConnect :: %s", requireRelaySuccessForQuickConnect ? "true" : "false");
		sm_RequireRelaySuccessForQuickConnect = requireRelaySuccessForQuickConnect;
	}
}

#if WINDOWS_FIREWALL_API
/*
	From https://msdn.microsoft.com/en-us/library/aa364726(v=VS.85).aspx
*/
HRESULT WindowsFirewallInitialize(OUT INetFwProfile** fwProfile)
{
	HRESULT hr = S_OK;
	INetFwMgr* fwMgr = NULL;
	INetFwPolicy* fwPolicy = NULL;

	netAssert(fwProfile != NULL);

	*fwProfile = NULL;

	// Create an instance of the firewall settings manager.
	hr = CoCreateInstance(
	         __uuidof(NetFwMgr),
	         NULL,
	         CLSCTX_INPROC_SERVER,
	         __uuidof(INetFwMgr),
	         (void**)&fwMgr
	     );
	if(FAILED(hr))
	{
		netDebug2("CoCreateInstance failed: 0x%08lx", hr);
		goto error;
	}

	// Retrieve the local firewall policy.
	hr = fwMgr->get_LocalPolicy(&fwPolicy);
	if(FAILED(hr))
	{
		netDebug2("get_LocalPolicy failed: 0x%08lx", hr);
		goto error;
	}

	// Retrieve the firewall profile currently in effect.
	hr = fwPolicy->get_CurrentProfile(fwProfile);
	if(FAILED(hr))
	{
		netDebug2("get_CurrentProfile failed: 0x%08lx", hr);
		goto error;
	}

error:

	// Release the local firewall policy.
	if(fwPolicy != NULL)
	{
		fwPolicy->Release();
	}

	// Release the firewall settings manager.
	if(fwMgr != NULL)
	{
		fwMgr->Release();
	}

	return hr;
}

void WindowsFirewallCleanup(IN INetFwProfile* fwProfile)
{
	// Release the firewall profile.
	if(fwProfile != NULL)
	{
		fwProfile->Release();
	}
}

HRESULT WindowsFirewallAppIsEnabled(
    IN INetFwProfile* fwProfile,
    IN const wchar_t* fwProcessImageFileName,
    OUT BOOL* fwAppEnabled
)
{
	HRESULT hr = S_OK;
	BSTR fwBstrProcessImageFileName = NULL;
	VARIANT_BOOL fwEnabled;
	INetFwAuthorizedApplication* fwApp = NULL;
	INetFwAuthorizedApplications* fwApps = NULL;

	netAssert(fwProfile != NULL);
	netAssert(fwProcessImageFileName != NULL);
	netAssert(fwAppEnabled != NULL);

	*fwAppEnabled = FALSE;

	// Retrieve the authorized application collection.
	hr = fwProfile->get_AuthorizedApplications(&fwApps);
	if(FAILED(hr))
	{
		netDebug2("get_AuthorizedApplications failed: 0x%08lx", hr);
		goto error;
	}

	// Allocate a BSTR for the process image file name.
	fwBstrProcessImageFileName = SysAllocString(fwProcessImageFileName);
	if(fwBstrProcessImageFileName == NULL)
	{
		hr = E_OUTOFMEMORY;
		netDebug2("SysAllocString failed: 0x%08lx", hr);
		goto error;
	}

	// Attempt to retrieve the authorized application.
	hr = fwApps->Item(fwBstrProcessImageFileName, &fwApp);
	if(SUCCEEDED(hr))
	{
		// Find out if the authorized application is enabled.
		hr = fwApp->get_Enabled(&fwEnabled);
		if(FAILED(hr))
		{
			netDebug2("get_Enabled failed: 0x%08lx", hr);
			goto error;
		}

		if(fwEnabled != VARIANT_FALSE)
		{
			// The authorized application is enabled.
			*fwAppEnabled = TRUE;

			netDebug2(
			    "Authorized application %lS is enabled in the firewall.",
			    fwProcessImageFileName
			);
		}
		else
		{
			netDebug2(
			    "Authorized application %lS is disabled in the firewall.",
			    fwProcessImageFileName
			);
		}
	}
	else
	{
		// The authorized application was not in the collection.
		hr = S_OK;

		netDebug2(
		    "Authorized application %lS is disabled in the firewall.",
		    fwProcessImageFileName
		);
	}

error:

	// Free the BSTR.
	SysFreeString(fwBstrProcessImageFileName);

	// Release the authorized application instance.
	if(fwApp != NULL)
	{
		fwApp->Release();
	}

	// Release the authorized application collection.
	if(fwApps != NULL)
	{
		fwApps->Release();
	}

	return hr;
}

HRESULT WindowsFirewallAddApp(
    IN INetFwProfile* fwProfile,
    IN const wchar_t* fwProcessImageFileName,
    IN const wchar_t* fwName
)
{
	HRESULT hr = S_OK;
	BOOL fwAppEnabled;
	BSTR fwBstrName = NULL;
	BSTR fwBstrProcessImageFileName = NULL;
	INetFwAuthorizedApplication* fwApp = NULL;
	INetFwAuthorizedApplications* fwApps = NULL;

	netAssert(fwProfile != NULL);
	netAssert(fwProcessImageFileName != NULL);
	netAssert(fwName != NULL);

	// First check to see if the application is already authorized.
	hr = WindowsFirewallAppIsEnabled(
	         fwProfile,
	         fwProcessImageFileName,
	         &fwAppEnabled
	     );
	if(FAILED(hr))
	{
		netDebug2("WindowsFirewallAppIsEnabled failed: 0x%08lx", hr);
		goto error;
	}

	// Only add the application if it isn't already authorized.
	if(!fwAppEnabled)
	{
		// Retrieve the authorized application collection.
		hr = fwProfile->get_AuthorizedApplications(&fwApps);
		if(FAILED(hr))
		{
			netDebug2("get_AuthorizedApplications failed: 0x%08lx", hr);
			goto error;
		}

		// Create an instance of an authorized application.
		hr = CoCreateInstance(
		         __uuidof(NetFwAuthorizedApplication),
		         NULL,
		         CLSCTX_INPROC_SERVER,
		         __uuidof(INetFwAuthorizedApplication),
		         (void**)&fwApp
		     );
		if(FAILED(hr))
		{
			netDebug2("CoCreateInstance failed: 0x%08lx", hr);
			goto error;
		}

		// Allocate a BSTR for the process image file name.
		fwBstrProcessImageFileName = SysAllocString(fwProcessImageFileName);
		if(fwBstrProcessImageFileName == NULL)
		{
			hr = E_OUTOFMEMORY;
			netDebug2("SysAllocString failed: 0x%08lx", hr);
			goto error;
		}

		// Set the process image file name.
		hr = fwApp->put_ProcessImageFileName(fwBstrProcessImageFileName);
		if(FAILED(hr))
		{
			netDebug2("put_ProcessImageFileName failed: 0x%08lx", hr);
			goto error;
		}

		// Allocate a BSTR for the application friendly name.
		fwBstrName = SysAllocString(fwName);
		if(SysStringLen(fwBstrName) == 0)
		{
			hr = E_OUTOFMEMORY;
			netDebug2("SysAllocString failed: 0x%08lx", hr);
			goto error;
		}

		// Set the application friendly name.
		hr = fwApp->put_Name(fwBstrName);
		if(FAILED(hr))
		{
			netDebug2("put_Name failed: 0x%08lx", hr);
			goto error;
		}

		// Add the application to the collection.
		hr = fwApps->Add(fwApp);
		if(FAILED(hr))
		{
			netDebug2("Add failed: 0x%08lx", hr);
			goto error;
		}

		netDebug2(
		    "Windows Firewall - added rule for application '%lS'.",
		    fwProcessImageFileName
		);
	}

error:

	// Free the BSTRs.
	SysFreeString(fwBstrName);
	SysFreeString(fwBstrProcessImageFileName);

	// Release the authorized application instance.
	if(fwApp != NULL)
	{
		fwApp->Release();
	}

	// Release the authorized application collection.
	if(fwApps != NULL)
	{
		fwApps->Release();
	}

	return hr;
}

void AddApplicationToWindowsFirewall()
{
	HRESULT hr = S_OK;
	HRESULT comInit = E_FAIL;
	INetFwProfile* fwProfile = NULL;

	// Initialize COM.
	comInit = CoInitialize(NULL);

	// Ignore RPC_E_CHANGED_MODE; this just means that COM has already been
	// initialized with a different mode. Since we don't care what the mode is,
	// we'll just use the existing mode.
	if(comInit != RPC_E_CHANGED_MODE)
	{
		hr = comInit;
		if(FAILED(hr))
		{
			netError("CoInitialize failed: 0x%08lx", hr);
			goto error;
		}
	}

	// Retrieve the firewall profile currently in effect.
	hr = WindowsFirewallInitialize(&fwProfile);
	if(FAILED(hr) || (fwProfile == NULL))
	{
		netDebug2("WindowsFirewallInitialize failed: 0x%08lx", hr);
		goto error;
	}

	wchar_t fileName[MAX_PATH] = {0};
	bool success = GetModuleFileNameW(NULL, fileName, COUNTOF(fileName)) != 0;

	if(!success)
	{
		netDebug2("GetModuleFileNameW failed");
		goto error;
	}

	if(g_rlTitleId)
	{
		const char* titleName = g_rlTitleId->m_RosTitleId.GetTitleDirectoryName();
		wchar_t titleNameW[RLROS_MAX_TITLE_DIRECTORY_SIZE] = {0};
		MultiByteToWideChar(CP_ACP, 0, titleName, -1, titleNameW, COUNTOF(titleNameW));

		hr = WindowsFirewallAddApp(fwProfile, fileName, titleNameW);
	}

	if(FAILED(hr))
	{
		netDebug2("WindowsFirewallAddApp failed: 0x%08lx", hr);
		goto error;
	}

error:

	// Release the firewall profile.
	WindowsFirewallCleanup(fwProfile);
}
#endif

bool
netIceTunneler::Init(netConnectionManager* cxnMgr)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(!m_Initialized, catchall,);
		rverify(cxnMgr != NULL, catchall,);

		s_IceRng.SetRandomSeed();

#if WINDOWS_FIREWALL_API
		AddApplicationToWindowsFirewall();
#endif

		for(unsigned i = 0; i < MAX_ACTIVE_ICE_SESSIONS; ++i)
		{
			m_FreeSessions.push_back(&m_Sessions[i]);
		}

		for(unsigned i = 0; i < MAX_PEER_REFLEXIVE_CANDIDATES; ++i)
		{
			PeerReflexiveCandidate candidate;
			candidate.m_Addr.Clear();
			candidate.m_NumHits = 0;
			candidate.m_Timestamp = 0;
			m_PeerReflexiveCandidates.Push(candidate);
		}

		netDebug3("netIceTunneler::Init(): sizeof(m_Sessions):%" SIZETFMT "u", sizeof(m_Sessions));
				
		m_CxnMgr = cxnMgr;

		unsigned channelId = NET_TUNNELER_CHANNEL_ID;

		m_CxnEventDelegate.Bind(&netIceTunneler::OnCxnEvent);
		m_CxnMgr->AddChannelDelegate(&m_CxnEventDelegate, channelId);

#if !RSG_FINAL
		if(PARAM_netrealworld.Get())
		{
			if(netNatDetector::GetLocalNatInfo().GetUdpTimeoutState() != netNatUdpTimeoutState::NAT_UDP_TIMEOUT_FAILED)
			{
				const unsigned desiredFailRate = 30;
				netDebug1("-%s is enabled, setting -%s=%u.", PARAM_netrealworld.GetName(), PARAM_neticefailprobability.GetName(), desiredFailRate);

				static char szDesiredFailRate[4];
				formatf(szDesiredFailRate, "%u", desiredFailRate);

				// set NAT traversal fail probability
				if(!PARAM_neticefailprobability.Get())
				{
					PARAM_neticefailprobability.Set(szDesiredFailRate);
				}

				unsigned failProbability = 0;
				if(!PARAM_neticefailprobability.Get(failProbability) || (failProbability != desiredFailRate))
				{
					netDebug1("Failed to set -%s=%u.", PARAM_neticefailprobability.GetName(), desiredFailRate);
				}
			}
			else
			{
				// don't simulate NAT traversal failures if we don't know the UDP session timeout. If the firewall times out
				// UDP sessions in less than 60 seconds and we're pinging the relay every 60 seconds (the default), peers
				// using the relay server behind this firewall will experience P2P disconnections.
				netDebug1("Unknown UDP session timeout. Ignoring -%s.", PARAM_netrealworld.GetName());
			}
		}

		unsigned quickConnectTimeoutMs = sm_QuickConnectTimeoutMs;
		if(PARAM_netIceQuickConnectTimeoutMs.Get(quickConnectTimeoutMs))
		{
			if(quickConnectTimeoutMs > (60 * 1000))
			{
				quickConnectTimeoutMs = s_IceRng.GetRanged(1, 3000);
			}

			SetQuickConnectTimeoutMs(quickConnectTimeoutMs);
		}
#endif

		m_Initialized = true;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
netIceTunneler::Shutdown()
{
    SYS_CS_SYNC(m_Cs);

    if(m_Initialized)
    {
		if(m_CxnMgr)
		{
			if(m_CxnEventDelegate.IsRegistered())
			{
				m_CxnMgr->RemoveChannelDelegate(&m_CxnEventDelegate);
			}
			m_CxnMgr = NULL;
		}

		SessionList::iterator it = m_ActiveSessions.begin();
		SessionList::const_iterator stop = m_ActiveSessions.end();
		for(; stop != it; ++it)
		{
			netIceSession* session = *it;
			if(session->IsActive())
			{
				session->Clear();
			}
		}

		// clear the lists
		m_ActiveSessions.clear();
		m_FreeSessions.clear();
		m_PeerReflexiveCandidates.clear();

        m_Initialized = false;
    }
}

void
netIceTunneler::Update()
{
	SYS_CS_SYNC(m_Cs);

	if(m_Initialized)
	{
		SessionList::iterator it = m_ActiveSessions.begin();
		SessionList::const_iterator stop = m_ActiveSessions.end();
		for(; stop != it; ++it)
		{
			netIceSession* session = *it;
			if(session->IsActive())
			{
				session->Update();
			}
		}

		// in list doesn't like us removing elements while we're iterating
		bool deleted = false;
		do 
		{
			deleted = false;
			SessionList::iterator it = m_ActiveSessions.begin();
			SessionList::const_iterator stop = m_ActiveSessions.end();
			for(; stop != it; ++it)
			{
				netIceSession* session = *it;
				if(session->IsActive() == false)
				{
					deleted = true;
					m_ActiveSessions.erase(session);
					m_FreeSessions.push_back(session);

					netDebug3("Removed inactive session and added to free list. "
							  "Num Active Sessions: %" SIZETFMT "u, Num Free Sessions: %" SIZETFMT "u",
							  m_ActiveSessions.size(), m_FreeSessions.size());

					m_NumConcurrentSessions = (unsigned)m_ActiveSessions.size();

					break;
				}
			}
		} while (deleted);

		netAssert((m_ActiveSessions.size() + m_FreeSessions.size()) == MAX_ACTIVE_ICE_SESSIONS);
	}
}

bool
netIceTunneler::OpenTunnel(const netPeerAddress& peerAddr,
						   netAddress* addr,
						   netAddress* relayAddr,
						   const bool bilateral,
						   const u64 sessionToken,
						   const u64 sessionId,
						   const unsigned tunnelRequestId,
						   netStatus* status)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(peerAddr.IsValid(), catchall, );
		rverify(status, catchall, );

		/*
			if there is already an active ICE session with this peer, then attach to the active ICE session.
		*/
		rverify(m_Initialized, catchall, );
		bool newSession = true;

		SessionList::iterator it = m_ActiveSessions.begin();
		SessionList::const_iterator stop = m_ActiveSessions.end();
		for(; stop != it; ++it)
		{
			netIceSession* session = *it;

			if(session->IsActive() &&
				(session->GetRemotePeerAddress().GetGamerHandle() == peerAddr.GetGamerHandle()) &&
				(session->GetRemotePeerAddress().GetPeerKey() == peerAddr.GetPeerKey()))
			{
				if(session->Attach(addr, relayAddr, tunnelRequestId, status))
				{
					newSession = false;
					break;
				}
			}
		}

		if(newSession)
		{
			netIceSession* session = AllocateSession();
			rverify(session, catchall, );
			rverify(session->Init(tunnelRequestId, GetNextSessionId(), m_CxnMgr, peerAddr, sessionToken, sessionId), catchall, );
			rcheck(session->StartAsOfferer(addr, relayAddr, bilateral, status), catchall, netDebug("netIceTunneler::OpenTunnel(): StartAsOfferer() failed"));
		}

		return true;
	}
	rcatchall
	{
		if(status)
		{
			if(status->Pending())
			{
				status->SetFailed();
			}
			else
			{
				status->ForceFailed();
			}
		}
	}

	return false;
}

bool
netIceTunneler::CloseTunnel(const netAddress& addr)
{
	rtry
	{
		rverifyall(addr.IsValid());
		rcheckall(addr.IsDirectAddr());

		rverifyall(m_CxnMgr && m_CxnMgr->IsInitialized());

		netSocketManager* sktMgr = m_CxnMgr->GetSocketManager();
		rverifyall(sktMgr && sktMgr->IsInitialized());

		// if a symmetric socket has been associated with this address, it will be destroyed.
		sktMgr->DestroySymmetricSocket(addr.GetTargetAddress());
	}
	rcatchall
	{
		
	}

	return true;
}

void
netIceTunneler::CancelRequest(netStatus* status)
{
	/*
		Note: don't put this function in a critical section.
		We can be updating an existing session on one thread and
		it could be sending a packet which enters the connection
		manager's critical section. The connection manager
		could receive a packet that tells it to override a tunnel
		request, which in turn calls this function and we deadlock.
	*/

	rtry
	{
		// find the session(s) with this status object and set them to canceled

		rverify(m_Initialized, catchall, );
		rverify(status, catchall, );

		SessionList::iterator it = m_ActiveSessions.begin();
		SessionList::const_iterator stop = m_ActiveSessions.end();
		for(; stop != it; ++it)
		{
			netIceSession* session = *it;

			if(session->IsActive() && (session->IsStatusAttached(status)))
			{
				session->SetCanceled(status);
			}
		}
	}
	rcatchall
	{
	}
}

bool
netIceTunneler::HasActiveSession(const netPeerAddress& peerAddr)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(peerAddr.IsValid(), catchall, );

		SessionList::iterator it = m_ActiveSessions.begin();
		SessionList::const_iterator stop = m_ActiveSessions.end();
		for(; stop != it; ++it)
		{
			netIceSession* session = *it;

			if(session->IsActive() &&
				(session->GetRemotePeerAddress().GetGamerHandle() == peerAddr.GetGamerHandle()) &&
				(session->GetRemotePeerAddress().GetPeerKey() == peerAddr.GetPeerKey()))
			{
				return true;
			}
		}
	}
	rcatchall
	{

	}

	return false;
}

bool 
netIceTunneler::ShouldShowConnectivityTroubleshooting()
{
	unsigned totalSessionsCompleted = m_NumFailedSessions + m_NumSuccessfulSessions;
	float failRate = (float)m_NumFailedSessions / (float)totalSessionsCompleted;
	float failRateBeforeWarning = (float)sm_FailRateBeforeWarning / 100.0f;

	return (m_NumFailedSessions >= sm_NumFailsBeforeWarning) && (failRate >= failRateBeforeWarning);
}

unsigned 
netIceTunneler::GetNumFailedNatTraversals()
{
	return m_NumFailedSessions;
}

unsigned 
netIceTunneler::GetNumSuccessfulNatTraversals()
{
	return m_NumSuccessfulSessions;
}

bool 
netIceTunneler::AddPeerReflexiveAddress(const netAddress& senderAddr, const netSocketAddress& addr)
{
	if(!sm_AllowPeerReflexiveList)
	{
		return false;
	}

	netIpAddress senderIp = senderAddr.GetTargetAddress().GetIp();

	bool found = false;
	for(unsigned i = 0; i < MAX_PEER_REFLEXIVE_CANDIDATES; ++i)
	{
		if(m_PeerReflexiveCandidates[i].m_Addr == addr)
		{
			// filter out reconnects if it's the same remote peer each time.
			// We want to know if we're re-using the port for multiple remote endpoints or
			// using a different port for every remote endpoint (some moderate NATs turn symmetric under certain conditions)
			if(m_PeerReflexiveCandidates[i].m_SenderIp != senderIp)
			{
				m_PeerReflexiveCandidates[i].m_NumHits++;
				m_PeerReflexiveCandidates[i].m_Timestamp = rlGetPosixTime();
			}
			found = true;
			break;
		}
	}

	if(!found)
	{
		// try to find an unused slot
		for(unsigned i = 0; i < MAX_PEER_REFLEXIVE_CANDIDATES; ++i)
		{
			if(!m_PeerReflexiveCandidates[i].m_Addr.IsValid())
			{
				m_PeerReflexiveCandidates[i].m_SenderIp = senderIp;
				m_PeerReflexiveCandidates[i].m_Addr = addr;
				m_PeerReflexiveCandidates[i].m_NumHits = 1;
				m_PeerReflexiveCandidates[i].m_Timestamp = rlGetPosixTime();
				found = true;
				break;
			}
		}
	}

	if(!found)
	{
		// evict the one with the least recent timestamp
		m_PeerReflexiveCandidates[MAX_PEER_REFLEXIVE_CANDIDATES - 1].m_SenderIp = senderIp;
		m_PeerReflexiveCandidates[MAX_PEER_REFLEXIVE_CANDIDATES - 1].m_Addr = addr;
		m_PeerReflexiveCandidates[MAX_PEER_REFLEXIVE_CANDIDATES - 1].m_NumHits = 1;
		m_PeerReflexiveCandidates[MAX_PEER_REFLEXIVE_CANDIDATES - 1].m_Timestamp = rlGetPosixTime();
	}

	// sort candidate by most recent - the candidate that worked most recently is most
	// likely to be our currently 'active' address
	struct SortPredicate
	{
		static int Sort(const PeerReflexiveCandidate* a, const PeerReflexiveCandidate* b)
		{
			// where timestamps are the same for multiple entries, sort descending by port.
			// Most NATs that change ports use monotonically increasing port numbers, so the highest port is often the best candidate.
			if(b->m_Timestamp == a->m_Timestamp)
			{
				return (b->m_Addr.GetPort() < a->m_Addr.GetPort()) ? -1 : 1;
			}

			return (b->m_Timestamp < a->m_Timestamp) ? -1 : 1;
		}
	};

	m_PeerReflexiveCandidates.QSort(0, -1, SortPredicate::Sort);

	return true;
}

unsigned  
netIceTunneler::GetPeerReflexiveAddresses(netSocketAddress* addrs, unsigned maxAddrs)
{
	if((addrs == NULL) || (maxAddrs == 0))
	{
		return 0;
	}

	unsigned numAddrs = 0;

	if(sm_PeerReflexiveHighestPlusOne == false)
	{
		--maxAddrs;
	}

	if(sm_AllowPeerReflexiveList)
	{
		bool oddPorts = false;
		bool evenPorts = false;
		unsigned numValidAddrs = 0;

		for(unsigned i = 0; (i < MAX_PEER_REFLEXIVE_CANDIDATES) && (numAddrs < maxAddrs); ++i)
		{
			if(m_PeerReflexiveCandidates[i].m_Addr.IsValid())
			{
				numValidAddrs++;
				if((m_PeerReflexiveCandidates[i].m_Addr.GetPort() % 2) == 0)
				{
					evenPorts = true;
				}
				else
				{
					oddPorts = true;
				}

				// ignore peer reflexive addresses that we haven't seen for a long time.
				// We're only interested in which ports we are actively using.
				const u64 PEER_REFLEXIVE_ADDR_TIMEOUT_SEC = sm_PeerReflexiveTimeoutSec;
				u64 curTime = rlGetPosixTime();
				if((curTime - m_PeerReflexiveCandidates[i].m_Timestamp) < (PEER_REFLEXIVE_ADDR_TIMEOUT_SEC))
				{
					addrs[numAddrs] = m_PeerReflexiveCandidates[i].m_Addr;
					numAddrs++;
				}
			}
		}

		/*
			Some Moderate NATs change ports over time. Example:
			0: 82.4.254.57:1432, NumHits: 29, Timestamp: 1446844409
			1: 82.4.254.57:1431, NumHits: 32, Timestamp: 1446844409
			2: 82.4.254.57:1430, NumHits: 48, Timestamp: 1446844409
			3: 82.4.254.57:1429, NumHits: 48, Timestamp: 1446842986
			4: 82.4.254.57:1428, NumHits: 33, Timestamp: 1446841753

			Clearly the NAT is incrementing the port by 1 under some condition, even though it re-uses
			ports for many endpoints.

			Theory:
			The NAT has a fixed sized number of slots to record IP's it's communicating with for each port.
			Once it reaches the max number of IP's for that port, it has no room in that port's array to
			store new endpoints, so it opens a new port. Other NATs likely allocate these arrays dynamically.

			Heuristic:
			In the above sample, we can see the latest port that a remote peer saw us use was 1432.
			We tell newly connecting peers to try port 1432, but in case we are just on the edge
			of opening a new port, we also tell the new peer to try port 1433. It's usually the Open
			NATs that we connect to that allow us to discover that we're opening new ports, so until
			we connect to another Open, we won't know we're using 1433 yet. Any Moderates trying to
			connect to us before realizing we're using port 1433 would fail. To try to help the process
			along, we predict highest port + 1 as a candidate for new peers to try.
		*/
		if(sm_PeerReflexiveHighestPlusOne && (numAddrs > 0))
		{
			netSocketAddress addrHighestPort;
			for(unsigned i = 0; i < numAddrs; ++i)
			{
				if(addrs[i].GetPort() > addrHighestPort.GetPort())
				{
					addrHighestPort = addrs[i];
				}
			}

			// some NATs only assign even ports, or only assign odd ports, so increment by 2 in that case
			short portIncrement = 2;
			if((oddPorts && evenPorts) || (numValidAddrs <= 2))
			{
				portIncrement = 1;
			}

			// increment highest port by portIncrement (predicting the next port we'll open)
			addrHighestPort.Init(addrHighestPort.GetIp(), addrHighestPort.GetPort() + portIncrement);

			if(addrHighestPort.IsValid())
			{
				if(numAddrs < maxAddrs)
				{
					addrs[numAddrs] = addrHighestPort;
					numAddrs++;
				}
				else
				{
					// overwrite the last one
					addrs[numAddrs - 1] = addrHighestPort;
				}
			}
		}
	}

	return numAddrs;
}

unsigned
netIceTunneler::GetNextSessionId()
{
	SYS_CS_SYNC(m_Cs);

	unsigned id = sysInterlockedIncrement(&s_NextId);

	while(NET_INVALID_ICE_SESSION_ID == id)
	{
		id = sysInterlockedIncrement(&s_NextId);
	}

	return id;
}

netIceSession*
netIceTunneler::AllocateSession()
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(m_FreeSessions.empty() == false, catchall, );

		netIceSession* session = m_FreeSessions.front();
		rverify(session, catchall, );
		m_FreeSessions.pop_front();
		m_ActiveSessions.push_back(session);

		m_TotalSessions++;
		m_NumConcurrentSessions = (unsigned)m_ActiveSessions.size();
		if(m_NumConcurrentSessions > m_PeakConcurrentSessions)
		{
			m_PeakConcurrentSessions = m_NumConcurrentSessions;
		}

		netDebug3("Allocated session. Num Active Sessions: %" SIZETFMT "u, Num Free Sessions: %" SIZETFMT "u, "
				  "Num Concurrent Sessions: %u, Peak Concurrent Sessions: %u, Total Sessions: %u",
				  m_ActiveSessions.size(), m_FreeSessions.size(), m_NumConcurrentSessions,
				  m_PeakConcurrentSessions, m_TotalSessions);

		netAssert((m_ActiveSessions.size() + m_FreeSessions.size()) == MAX_ACTIVE_ICE_SESSIONS);

		return session;
	}
	rcatchall
	{
	}

	return NULL;
}

netIceSession*
netIceTunneler::FindSessionById(const unsigned id)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall, );

		SessionList::iterator it = m_ActiveSessions.begin();
		SessionList::const_iterator stop = m_ActiveSessions.end();
		for(; stop != it; ++it)
		{
			netIceSession* session = *it;
			if(session->IsActive() && (session->GetSessionId() == id))
			{
				return session;
			}
		}
	}
	rcatchall
	{
	}

	return NULL;
}

void
netIceTunneler::ReceiveOffer(const netIceSessionOffer* msg, const netAddress& sender)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(msg->m_SourcePeerAddr.IsValid(), catchall, );
		rverify(sender.IsValid(), catchall, );

		/*
			go through our list of active sessions and see if any of them have this remote peer
			address and remote session id since this could be an offer sent to an existing
			session. We don't want to end up creating a new session in that case.
		*/
		netIceSession* session = NULL;

		SessionList::iterator it = m_ActiveSessions.begin();
		SessionList::const_iterator stop = m_ActiveSessions.end();
		for(; stop != it; ++it)
		{
			netIceSession* curSession = *it;
			if(curSession->IsActive() && curSession->CanReceiveOffer(msg, sender))
			{
				session = curSession;
				break;
			}
		}

		if(session == NULL)
		{
			session = AllocateSession();
			rverify(session, catchall, );
			rverify(session->Init(NET_INVALID_TUNNEL_REQUEST_ID, GetNextSessionId(), m_CxnMgr, msg->m_SourcePeerAddr, msg->m_GameSessionToken, msg->m_GameSessionId), catchall, );
		}

		rcheck(session->ReceiveOffer(msg, sender), catchall, );
	}
	rcatchall
	{
	}
}

void
netIceTunneler::ReceiveAnswer(const netIceSessionAnswer* msg, const netAddress& sender)
{
	SYS_CS_SYNC(m_Cs);

	netIceSession* session = FindSessionById(msg->m_DestSessionId);

	if(session)
	{
		session->ReceiveAnswer(msg, sender);
	}
	else
	{
		netDebug1("Received answer from " NET_ADDR_FMT " for unknown session with id %u:", NET_ADDR_FOR_PRINTF(sender), msg->m_DestSessionId);
	}
}

void
netIceTunneler::SendTerminatePong(const netIceSessionPing* msg, const netAddress& sender)
{
	SYS_CS_SYNC(m_Cs);

	if(m_CxnMgr && m_CxnMgr->IsInitialized())
	{
		netIceSessionPong pongMsg(msg->m_SourceSessionId, msg->m_TransactionId, netSocketAddress::INVALID_ADDRESS, false, true);

		netDebug2("Sending terminate pong to:" NET_ADDR_FMT ". RemoteTransactionId:%u", NET_ADDR_FOR_PRINTF(sender), msg->m_TransactionId);

		netP2pCryptContext context;
		context.Init(msg->m_SourcePeerAddr.GetPeerKey(), NET_P2P_CRYPT_TUNNELING_PACKET);

		m_CxnMgr->SendTunnelingPacket(sender,
									  pongMsg,
									  context);
	}
}

void
netIceTunneler::ReceivePing(const netIceSessionPing* msg, const netAddress& sender)
{
	SYS_CS_SYNC(m_Cs);

	netIceSession* session = FindSessionById(msg->m_DestSessionId);

	if(session)
	{
		session->ReceivePing(msg, sender);
	}
	else
	{
		netDebug1("Received ping from " NET_ADDR_FMT " for unknown session with id %u:", NET_ADDR_FOR_PRINTF(sender), msg->m_DestSessionId);
		SendTerminatePong(msg, sender);
	}
}

void
netIceTunneler::ReceivePong(const netIceSessionPong* msg, const netAddress& sender)
{
	SYS_CS_SYNC(m_Cs);

	netIceSession* session = FindSessionById(msg->m_DestSessionId);

	if(session)
	{
		session->ReceivePong(msg, sender);
	}
	else
	{
		netDebug1("Received pong from " NET_ADDR_FMT " for unknown session with id %u:", NET_ADDR_FOR_PRINTF(sender), msg->m_DestSessionId);
	}
}

void
netIceTunneler::ReceiveRelayRouteCheck(const netIceSessionRelayRouteCheck* msg, const netAddress& sender)
{
	SYS_CS_SYNC(m_Cs);

	netIceSession* session = FindSessionById(msg->m_DestSessionId);

	if(session)
	{
		session->ReceiveRelayRouteCheck(msg, sender);
	}
	else
	{
		netDebug1("Received relay route check %s from " NET_ADDR_FMT " for unknown session with id %u:", msg->IsRequest() ? "request" : "response", NET_ADDR_FOR_PRINTF(sender), msg->m_DestSessionId);
	}
}

void
netIceTunneler::ReceivePortOpener(const netIceSessionPortOpener* UNUSED_PARAM(msg), const netAddress& OUTPUT_ONLY(sender))
{
	netDebug2("Received port opener message from " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(sender));
}

void
netIceTunneler::ReceiveDebugInfo(const netIceSessionDebugInfo* OUTPUT_ONLY(msg), const netAddress& OUTPUT_ONLY(sender))
{
#if !__NO_OUTPUT
	SYS_CS_SYNC(m_Cs);

	netIceSession* session = FindSessionById(msg->m_DestSessionId);

	if(session)
	{
		session->ReceiveDebugInfo(msg, sender);
	}
	else
	{
		netDebug2("[S:%u T:-] Received debug info message from " NET_ADDR_FMT, msg->m_DestSessionId, NET_ADDR_FOR_PRINTF(sender));
		msg->DumpInfo();
	}
#endif
}

void
netIceTunneler::OnCxnEvent(netConnectionManager* UNUSED_PARAM(cxnMgr), const netEvent* evt)
{
	PROFILE;

	if(!m_Initialized)
	{
		return;
	}

#if __FINAL_LOGGING
	if(PARAM_netforceuserelay.Get())
	{
		return;
	}
#endif

    if(NET_EVENT_FRAME_RECEIVED == evt->GetId())
    {
		const netEventFrameReceived* fr = evt->m_FrameReceived;

#if !__FINAL
		if(PARAM_netIceIgnoreDirectMsgs.Get())
		{
			if(fr && fr->m_Sender.IsDirectAddr())
			{
				return;
			}
		}

		if(PARAM_netIceIgnoreIndirectMsgs.Get())
		{
			if(fr && !fr->m_Sender.IsDirectAddr())
			{
				return;
			}
		}
#endif

		unsigned msgId;

		if(netMessage::GetId(&msgId, fr->m_Payload, fr->m_SizeofPayload))
		{
			if(netIceSessionOffer::MSG_ID() == msgId)
			{
				netIceSessionOffer msg;
				if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
				{
					ReceiveOffer(&msg, fr->m_Sender);
				}
			}
			else if(netIceSessionAnswer::MSG_ID() == msgId)
			{
				netIceSessionAnswer msg;
				if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
				{
					ReceiveAnswer(&msg, fr->m_Sender);
				}
			}
			else if(netIceSessionPing::MSG_ID() == msgId)
			{
				netIceSessionPing msg;
				if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
				{
					ReceivePing(&msg, fr->m_Sender);
				}
			}
			else if(netIceSessionPong::MSG_ID() == msgId)
			{
				netIceSessionPong msg;
				if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
				{
					ReceivePong(&msg, fr->m_Sender);
				}
			}
			else if(netIceSessionRelayRouteCheck::MSG_ID() == msgId)
			{
				netIceSessionRelayRouteCheck msg;
				if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
				{
					ReceiveRelayRouteCheck(&msg, fr->m_Sender);
				}
			}
			else if(netIceSessionPortOpener::MSG_ID() == msgId)
			{
				netIceSessionPortOpener msg;
				if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
				{
					ReceivePortOpener(&msg, fr->m_Sender);
				}
			}
#if !__NO_OUTPUT
			else if(netIceSessionDebugInfo::MSG_ID() == msgId)
			{
				netIceSessionDebugInfo msg;
				if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
				{
					ReceiveDebugInfo(&msg, fr->m_Sender);
				}
			}
#endif
		}
	}
}

}   //namespace rage
