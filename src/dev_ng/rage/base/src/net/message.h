// 
// net/message.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_MESSAGE_H
#define NET_MESSAGE_H

#include "data/autoid.h"
#include "data/bitbuffer.h"
#include "packet.h"
#include "messageids.h"
#include "netaddress.h"
#include "netsequence.h"
#include "netdiag.h"

#ifdef IS_COMBINED
# ifdef NET_MESSAGE_COMMON_DECL
#  undef NET_MESSAGE_COMMON_DECL
#  undef NET_MESSAGE_DECL
#  undef NET_MESSAGE_DECL_ID
#  undef NET_MESSAGE_IMPL
# endif  //NET_MESSAGE_COMMON_DECL
#endif  //IS_COMBINED

#define NET_MESSAGE_EXPORT_COMMON(func)\
    template<typename T>\
    bool Export(T& bb) const\
    {\
        return rage::netMessage::WriteHeader(this->GetMsgId(), bb)\
                && this->func(bb, *this);\
    }\
    bool Export(void* buf, const unsigned sizeofBuf, unsigned* size = 0) const\
    {\
        rage::datExportBuffer bb;\
        bb.SetReadWriteBytes(buf, sizeofBuf);\
        const bool success = this->Export(bb);\
        if(size){*size = success ? bb.GetNumBytesWritten() : 0;}\
        return success;\
    }

#define NET_MESSAGE_IMPORT_COMMON(func)\
    template<typename T>\
    bool Import(T& bb)\
    {\
        unsigned msgId = 0;\
        return rage::netMessage::ReadHeader(&msgId, bb)\
                && this->GetMsgId() == msgId\
                && this->func(bb, *this);\
    }\
    bool Import(const void* buf, const unsigned sizeofBuf, unsigned* size = 0)\
    {\
        rage::datImportBuffer bb;\
        bb.SetReadOnlyBytes(buf, sizeofBuf);\
        const bool success = this->Import(bb);\
        if(size){*size = success ? bb.GetNumBytesRead() : 0;}\
        return success\
                /*Make sure we read all the bits in the msg.*/\
                && (bb.GetNumBytesRead() == (int) sizeofBuf);\
    }

#define NET_MESSAGE_IMPORT_STUB_COMMON(func)\
	template<typename T>\
	bool ImportStub(T& bb)\
	{\
		unsigned msgId = 0;\
		return rage::netMessage::ReadHeader(&msgId, bb)\
				&& this->func(bb, *this);\
	}\
	bool ImportStub(const void* buf, const unsigned sizeofBuf, unsigned* size = 0)\
	{\
		rage::datImportBuffer bb;\
		bb.SetReadOnlyBytes(buf, sizeofBuf);\
		const bool success = this->ImportStub(bb);\
		if(size){*size = success ? bb.GetNumBytesRead() : 0;}\
			return success\
					/*Make sure we read all the bits in the msg.*/\
					&& (bb.GetNumBytesRead() < (int) sizeofBuf);\
	}

#if __NO_OUTPUT
// security: remove strings from Final builds
#define NET_MESSAGE_DECL_COMMON(name)\
	static unsigned MSG_ID() { return name::GetAutoId(); }\
	unsigned GetMsgId() const { return name::GetAutoId(); }\
	NET_MESSAGE_EXPORT_COMMON(SerMsgPayload)\
	NET_MESSAGE_IMPORT_COMMON(SerMsgPayload)
#else
#define NET_MESSAGE_DECL_COMMON(name)\
	static unsigned MSG_ID() { return name::GetAutoId(); }\
	static const char* MSG_NAME() { return #name; }\
	unsigned GetMsgId() const { return name::GetAutoId(); }\
	const char* GetMsgName() const { return #name; }\
	NET_MESSAGE_EXPORT_COMMON(SerMsgPayload)\
	NET_MESSAGE_IMPORT_COMMON(SerMsgPayload)
#endif // __NO_OUTPUT

#define NET_MESSAGE_IMPORT_STUB_DECL() NET_MESSAGE_IMPORT_STUB_COMMON(SerMsgStub)

//PURPOSE
//  Use this variant if a predefined message id is to be used.
//PARAMS
//  name        - Name of the message.
//  id          - Predefined message id.
#define NET_MESSAGE_DECL(name, id)\
    AUTOID_DECL_ID(name, rage::netMessage, id)\
    NET_MESSAGE_DECL_COMMON(name)

//PURPOSE
//  Export/import macro.
//PARAMS
//  bb      - Bit buffer.
//  msg     - The message to be exported/imported.
#define NET_MESSAGE_SER(bb, msg)\
    template<typename T, typename U>\
    static bool SerMsgPayload(T& bb, U& msg)

//PURPOSE
//  Export/import macro.
//PARAMS
//  bb      - Bit buffer.
//  msg     - The message to be exported/imported.
#define NET_MESSAGE_IMPORT_STUB(bb, msg)\
	template<typename T>\
	static bool SerMsgStub(const rage::datImportBuffer& bb, T& msg)

//PURPOSE
//  Export macro.
//PARAMS
//  bb      - Bit buffer.
//  msg     - The message to be exported.
#define NET_MESSAGE_EXPORT(bb, msg)\
    template<typename T>\
    static bool SerMsgPayload(rage::datExportBuffer& bb, T& msg)

//PURPOSE
//  Import macro.
//PARAMS
//  bb      - Bit buffer.
//  msg     - The message to be imported.
#define NET_MESSAGE_IMPORT(bb, msg)\
    template<typename T>\
    static bool SerMsgPayload(const rage::datImportBuffer& bb, T& msg)

//PURPOSE
//  Call the base class serialization function
//PARAMS
//  base    - Name of base class
//  bb      - Bit buffer.
//  msg     - The message to be exported/imported.
#define NET_MESSAGE_SER_BASE(base, bb, msg)\
    base::SerMsgPayload(bb, msg)

//PURPOSE
//  Place this macro in the implementation (.cpp) file of network messages.
//PARAMS
//  classname   - Classname of the message.
//  name        - Name of the message.
#define NET_MESSAGE_IMPL AUTOID_IMPL

/*
    Example of using the NET_MESSAGE_* macros:

    .h file
    -------

    class netMsgFoo
    {
    public:
        NET_MESSAGE_DECL(netMsgFoo, NET_MSG_FOO);
    };

    .cpp file
    ---------

    NET_MESSAGE_IMPL(netMsgFoo, netMsgFoo);
*/

namespace rage
{

//PURPOSE
//  netMessage is never instantiated but is necessary in order
//  to implement export and import of app-specific message classes.
//
//  Serialization of a net message is implemented by embedding
//  NET_MESSAGE_SER in the net message class declaration.
//  The macro implements the following member functions:
//
//  bool Export(void* buf, const unsigned sizeofBuf, unsigned* size) const;
//
//  bool Import(const void* buf, const unsigned sizeofBuf);
//
//  template<typename T, typename U> static bool SerMsg(T& bb, U& msg);
//
//  Following is an example declaration of an app-specific net message
//  using the NET_MESSAGE_SER macro:
//
//  struct MyMsg
//  {
//      NET_MESSAGE_DECL(MyMsg, MY_MSG);
//
//      NET_MESSAGE_SER(bb, msg)
//      {
//          return bb.SerInt(msg.m_Health, 13)
//                 && bb.SerStr(msg.m_Name, sizeof(msg.m_Name));
//      }
//      int m_Health;
//      char m_Name[64];
//  };
//
//  Note that the same code is used to both export and import
//  message data.  For this reason care must be taken when writing
//  serialization code.  For example, the following will result in
//  undefined behavior:
//
//  struct AddGamerMsg
//  {
//      NET_MESSAGE_DECL(AddGamerMsg, ADD_GAMER_MSG);
//
//      NET_MESSAGE_SER(bb, msg)
//      {
//          bool success = true;
//          for(int i = 0; i < msg.m_NumGamers && success; ++i)
//          {
//              success = bb.SerBytes(msg.&m_Gamers[i], sizeof(msg.m_Gamers[i]));
//          }
//          return success && bb.SerInt(msg.m_NumGamers);
//      }
//      int m_NumGamers;
//      Gamer m_Gamers[MAX_GAMERS];
//  };
//
//  The above code is fine for export, but for import the results are
//  undefined because the array is read before its size is known.  A
//  better implementation is as follows (note the validation of the
//  array size before reading the array):
//
//      NET_MESSAGE_SER(bb, msg)
//      {
//          bool success = bb.SerInt(msg.m_NumGamers)
//                          && msg.m_NumGamers >= 0
//                          && msg.m_NumGamers < MAX_GAMERS;
//          for(int i = 0; i < msg.m_NumGamers && success; ++i)
//          {
//              success == bb.SerBytes(msg.&m_Gamers[i], sizeof(msg.m_Gamers[i]));
//          }
//          return success;
//      }


class netMessage
{
    //Signature to identify messages from other types of data.
    static const unsigned MSG_SIG = NET_MAKE_SIG3('M'-'A', 'S'-'A', 'G'-'A');

    enum Flags
    {
        //If this flag is set the message id is 16 bits, otherwise
        //it's 8 bits.
        MSG_FLAG_16BIT_ID   = 0x01,

        MSG_NUM_FLAGS       = 1,
    };

public:

    AUTOID_DECL_ROOT(netMessage);
    NET_MESSAGE_DECL_COMMON(netMessage);

    enum
    {
        BIT_SIZEOF_SIG      = datBitsNeeded<MSG_SIG>::COUNT,
        BIT_SIZEOF_FLAGS    = MSG_NUM_FLAGS,
        MAX_BIT_SIZEOF_ID   = 16,

        BIT_OFFSETOF_SIG    = 0,
        BIT_OFFSETOF_FLAGS  = BIT_OFFSETOF_SIG + BIT_SIZEOF_SIG,
        BIT_OFFSETOF_ID     = BIT_OFFSETOF_FLAGS + BIT_SIZEOF_FLAGS,

        MAX_BIT_SIZEOF_HEADER       = BIT_OFFSETOF_ID + MAX_BIT_SIZEOF_ID,

        MAX_BYTE_SIZEOF_HEADER      = (MAX_BIT_SIZEOF_HEADER + 7) >> 3,

        MAX_BIT_OFFSETOF_PAYLOAD    = MAX_BYTE_SIZEOF_HEADER << 3,

        MAX_BYTE_SIZEOF_MESSAGE = netFrame::MAX_BYTE_SIZEOF_PAYLOAD,
        MAX_BYTE_SIZEOF_PAYLOAD = MAX_BYTE_SIZEOF_MESSAGE - MAX_BYTE_SIZEOF_HEADER,
    };

    static void InitClass()
    {
        //Initialize the Auto Id system.  This is the system that
        //automatically generates message ids for network messages.
        AutoIdInit();
    }

    static void ShutdownClass()
    {
        AutoIdShutdown();
    }

    //PURPOSE
    //  Writes a message header to the buffer.
    //RETURNS
    //  True on success.
    template<typename T>
    static bool WriteHeader(const unsigned msgId, T& bb)
    {
        bool success = bb.WriteUns(MSG_SIG, BIT_SIZEOF_SIG);

        if(success)
        {
            if(msgId <= 0xFF)
            {
                success = bb.WriteUns(0, BIT_SIZEOF_FLAGS)
                            && bb.WriteUns(msgId, 8);
            }
            else
            {
                success = bb.WriteUns(MSG_FLAG_16BIT_ID, BIT_SIZEOF_FLAGS)
                            && bb.WriteUns(msgId, 16);
            }
        }

        return success;
    }

    //PURPOSE
    //  Writes a message header to the buffer.
    //RETURNS
    //  Number of bytes written to the buffer.
    static unsigned WriteHeader(const unsigned msgId,
                                void* buf,
                                const unsigned sizeofBuf)
    {
        datBitBuffer bb;

        bb.SetReadWriteBytes(buf, sizeofBuf);

        return (netVerify(sizeofBuf >= rage::netMessage::MAX_BYTE_SIZEOF_HEADER)
            && netMessage::WriteHeader(msgId, bb))
            ? bb.GetNumBytesWritten()
            : 0;
    }

    //PURPOSE
    //  Reads a message header from the buffer.
    //RETURNS
    //  True on success.
    template<typename T>
    static unsigned ReadHeader(unsigned* msgId, const T& bb)
    {
        bool success = false;
        unsigned sig, flags;

        if(bb.CanReadBits(BIT_SIZEOF_SIG + BIT_SIZEOF_FLAGS)
            && bb.ReadUns(sig, BIT_SIZEOF_SIG)
            && MSG_SIG == sig
            && bb.ReadUns(flags, BIT_SIZEOF_FLAGS))
        {
            if(!(flags & MSG_FLAG_16BIT_ID))
            {
                success = bb.CanReadBits(8) && bb.ReadUns(*msgId, 8);
            }
            else
            {
                success = bb.CanReadBits(16) && bb.ReadUns(*msgId, 16);
            }
        }
        else
        {
            *msgId = ~0u;
        }

        return success;
    }

    //PURPOSE
    //  Returns the net message header size
    static unsigned GetHeaderSize(const unsigned msgId)
    {
        unsigned size = BIT_SIZEOF_SIG + BIT_SIZEOF_FLAGS;

        if(msgId <= 0xFF)
        {
            size += 8;
        }
        else
        {
            size += 16;
        }

        return size;
    }

    //PURPOSE
    //  Reads a message header from the buffer.
    //RETURNS
    //  Number of bytes read from the buffer, or zero if the buffer is
    //  not a message.
    static unsigned ReadHeader(unsigned* msgId,
                                const void* buf,
                                const unsigned sizeofBuf)
    {
        datBitBuffer bb;

        bb.SetReadOnlyBytes(buf, sizeofBuf);

        return netMessage::ReadHeader(msgId, bb) ? bb.GetNumBytesRead() : 0;
    }

    //PURPOSE
    //  Returns true if the data contained in the buffer is a message.
    static bool IsMessage(const void* buf,
                        const unsigned sizeofBuf)
    {
        unsigned sig = ~unsigned(MSG_SIG);

        if(sizeofBuf >= ((BIT_SIZEOF_SIG + 7) >> 3))
        {
            datBitBuffer::ReadUnsigned(buf, sig, BIT_SIZEOF_SIG, BIT_OFFSETOF_SIG);
        }

        return MSG_SIG == sig;
    }

    //PURPOSE
    //  Extracts a message id from a buffer.
    //NOTES
    //  Returns false if the buffer does not contain a message.
    static bool GetId(unsigned* id,
                        const void* buf,
                        const unsigned sizeofBuf)
    {
        return 0 != netMessage::ReadHeader(id, buf, sizeofBuf);
    }

	// security - the names don't exist in the shipping exe
#if !__NO_OUTPUT
    //PURPOSE
    //  Returns the name of the message contained in the buffer,
    //  or NULL if the buffer doesn't contain a message.
    static const char* GetName(const void* buf,
                                const unsigned sizeofBuf)
    {
        unsigned id;
        return netMessage::GetId(&id, buf, sizeofBuf)
                ? AutoIdRegistrar<netMessage>::GetNameFromId(id)
                : NULL;
    }

    //PURPOSE
    //  Returns the name of the message for the given message id.
    static const char* GetName(const unsigned msgId)
    {
        return AutoIdRegistrar<netMessage>::GetNameFromId(msgId);
    }
#endif

    //PURPOSE
    //  Returns the number of registered message ids.
    static unsigned GetMessageCount()
    {
        return AutoIdRegistrar<netMessage>::GetIdCount();
    }

private:

    NET_MESSAGE_SER(/*bb*/,/*msg*/)
    {
        return true;
    }

    netMessage();
    ~netMessage();
};

}   //namespace rage

#endif  //NET_MESSAGE_H
