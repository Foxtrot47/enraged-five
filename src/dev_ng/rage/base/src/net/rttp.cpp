// 
// net/rttp.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "rttp.h"
#include "net.h"
#include "tcp.h"
#include "data/bitbuffer.h"
#include "diag/channel.h"
#include "diag/seh.h"
#include "diag/tracker.h"
#include "net/netdiag.h"
#include "system/memory.h"
#include "system/timer.h"

#include <time.h>

namespace rage
{

RAGE_DECLARE_CHANNEL(ragenet);
RAGE_DEFINE_SUBCHANNEL(ragenet, rttp)
#undef __rage_channel
#define __rage_channel ragenet_rttp

//////////////////////////////////////////////////////////////////////////
// class netRttpHeader
//////////////////////////////////////////////////////////////////////////
netRttpHeader::netRttpHeader()
{
    Clear();
}

void
netRttpHeader::Clear()
{
    m_Signature = 0;
    m_TxId = RTTP_INVALID_TXID;
    m_ContentLength = 0;
    m_Mixer = 0;
    m_Flags = 0;

    netAssert(!IsValid());
}

void 
netRttpHeader::ResetForKeepAlive(const u32 signature)
{
    Clear();

    m_Mixer = netRttpHeader::CreateMixer();
    m_Signature = signature;
    m_Flags = RTTP_HEADERFLAG_KEEPALIVE;

    netAssert(IsValid());
}

void 
netRttpHeader::ResetForPush(const u32 signature, 
                            const u32 contentLen,
                            const u8 flags)
{
    Clear();

    m_Mixer = netRttpHeader::CreateMixer();
    m_Signature = signature;
    m_ContentLength = contentLen;
    m_Flags = flags;

    netAssert(IsValid());
}

void 
netRttpHeader::ResetForTransaction(const u32 signature, 
                                    const u32 contentLen,
                                    const u16 txId,
                                    const u8 flags)
{
    Clear();

    m_Mixer = netRttpHeader::CreateMixer();
    m_Signature = signature;
    m_TxId = txId;
    m_ContentLength = contentLen;
    m_Flags = flags;

    netAssert(IsValid());
}

bool 
netRttpHeader::IsValid() const
{
    return SIGNATURE == m_Signature && (m_ContentLength <= MAX_CONTENT_LENGTH);
}

bool
netRttpHeader::IsTransaction() const
{
    return (RTTP_INVALID_TXID != m_TxId);
}

bool
netRttpHeader::IsStream() const
{
    return 0 != (RTTP_HEADERFLAG_STREAM & m_Flags);
}

u32
netRttpHeader::GetSignature() const
{
    return m_Signature;
}

u8
netRttpHeader::GetMixer() const
{
    return m_Mixer;
}

u8
netRttpHeader::GetFlags() const
{
    return m_Flags;
}

u16
netRttpHeader::GetTransactionId() const
{
    return m_TxId;
}

u32
netRttpHeader::GetContentLength() const
{
    return m_ContentLength;
}

bool
netRttpHeader::Export(void* buf,
                      const unsigned sizeofBuf,
                      unsigned* size) const
{
    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);

    bool success = false;

    rtry
    {
        rcheck(bb.WriteUns(m_Mixer, BIT_SIZEOF_MIXER), catchall,);
        for(int i = BIT_SIZEOF_SIGNATURE - 8; i >= 0; i -= 8)
        {
            rcheck(bb.WriteUns(m_Mixer ^ ((m_Signature >> i) & 0xFF), 8), catchall, );
        }
        for(int i = BIT_SIZEOF_FLAGS - 8; i >= 0; i -= 8)
        {
            rcheck(bb.WriteUns(m_Mixer ^ ((m_Flags >> i) & 0xFF), 8), catchall, );
        }
        for(int i = BIT_SIZEOF_TXID - 8; i >= 0; i -= 8)
        {
            rcheck(bb.WriteUns(m_Mixer ^ ((m_TxId >> i) & 0xFF), 8), catchall, );
        }
        for(int i = BIT_SIZEOF_CONTENT_LEN - 8; i >= 0; i -= 8)
        {
            rcheck(bb.WriteUns(m_Mixer ^ ((m_ContentLength >> i) & 0xFF), 8), catchall, );
        }

        success = true;        
    }
    rcatchall
    {
    }

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}

    return success;
}

bool 
netRttpHeader::Import(const void* buf,
                      const unsigned sizeofBuf,
                      unsigned* size)
{
    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

    bool success = false;

    Clear();

    u8 u;

    rtry
    {
        rcheck(bb.ReadUns(m_Mixer, BIT_SIZEOF_MIXER), catchall,);
        for(int i = 0; i < BIT_SIZEOF_SIGNATURE; i += 8)
        {
            rcheck(bb.ReadUns(u, 8), catchall,);
            m_Signature = (m_Signature << 8) | (u ^ m_Mixer);
        }
        for(int i = 0; i < BIT_SIZEOF_FLAGS; i += 8)
        {
            rcheck(bb.ReadUns(u, 8), catchall,);
            m_Flags = (m_Flags << 8) | (u ^ m_Mixer);
        }
        for(int i = 0; i < BIT_SIZEOF_TXID; i += 8)
        {
            rcheck(bb.ReadUns(u, 8), catchall,);
            m_TxId = (m_TxId << 8) | (u ^ m_Mixer);
        }
        for(int i = 0; i < BIT_SIZEOF_CONTENT_LEN; i += 8)
        {
            rcheck(bb.ReadUns(u, 8), catchall,);
            m_ContentLength = (m_ContentLength << 8) | (u ^ m_Mixer);
        }

        success = IsValid();
    }
    rcatchall
    {
    }

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}

    return success;
}

//private:

sysCriticalSectionToken s_MixerCs;

u8
netRttpHeader::CreateMixer()
{
    SYS_CS_SYNC(s_MixerCs);

    static time_t timer;
    static netRandom s_Rng((int) time(&timer));

    return (u8) s_Rng.GetInt();
}

//////////////////////////////////////////////////////////////////////////
//  netRttpMessage
//////////////////////////////////////////////////////////////////////////
netRttpMessage::netRttpMessage()
: m_ContentLen(0)
, m_Content(NULL)
, m_TxId(RTTP_INVALID_TXID)
{
}

void
netRttpMessage::Clear()
{
    m_ContentLen = 0;
    m_Content = NULL;
    m_TxId = RTTP_INVALID_TXID;
}

void 
netRttpMessage::ResetForKeepAlive()
{
    this->Clear();
}

void 
netRttpMessage::Reset(const void* content,
                    const u32 contentLen,
                    const u16 txId)
{
    this->Clear();
    m_ContentLen = contentLen;
    m_Content = (const u8*) content;
    m_TxId = txId;
}

const void*
netRttpMessage::GetContent() const
{
    return m_Content;
}

unsigned
netRttpMessage::GetContentLength() const
{
    return m_ContentLen;
}

u16
netRttpMessage::GetTransactionId() const
{
    return m_TxId;
}

bool
netRttpMessage::IsTransaction() const
{
    return RTTP_INVALID_TXID != this->GetTransactionId();
}

//////////////////////////////////////////////////////////////////////////
//  netRttpRequest
//////////////////////////////////////////////////////////////////////////
netRttpRequest::netRttpRequest()
: m_Allocator(NULL)
, m_ResponseContent(NULL)
, m_TimeoutSecs(0)
, m_TimeLeftMs(0)
{
}

netRttpRequest::~netRttpRequest()
{
    this->Dispose();
}

void
netRttpRequest::Dispose()
{
    netAssert(!Pending());

    if(m_ResponseContent && m_Allocator)
    {
        m_Allocator->Free(m_ResponseContent);
        m_ResponseContent = NULL;
    }
    m_Allocator = NULL;
    m_TimeoutSecs = 0;
    m_TimeoutSecs = 0;

    m_RequestMsg.Clear();
    m_ResponseMsg.Clear();

    m_Status.Reset();
}

bool
netRttpRequest::Pending() const
{
    return m_Status.Pending();
}

bool
netRttpRequest::Succeeded() const
{
    return m_Status.Succeeded();
}

const netRttpMessage&
netRttpRequest::GetRequestMsg() const
{
    return m_RequestMsg;
}

const netRttpMessage&
netRttpRequest::GetResponseMsg() const
{
    netAssert(Succeeded());
    return m_ResponseMsg;
}

u16
netRttpRequest::GetTransactionId() const
{
    return m_RequestMsg.GetTransactionId();
}

bool
netRttpRequest::IsTransaction() const
{
    return m_RequestMsg.IsTransaction();
}

//private:

void
netRttpRequest::ResetRequest(const void* content,
                            const u32 contentLen,
                            const u16 txId,
                            const unsigned timeoutSecs)
{
    netAssert(timeoutSecs);

    this->Dispose();

    m_RequestMsg.Reset(content, contentLen, txId);
    m_TimeoutSecs = timeoutSecs;
    m_TimeLeftMs = timeoutSecs * 1000;

    m_Status.SetPending();
}

void
netRttpRequest::SetResponse(sysMemAllocator* allocator,
                            const void* content,
                            const u32 contentLen)
{
    netAssert(allocator);
    netAssert(RTTP_INVALID_TXID != m_RequestMsg.GetTransactionId());
    netAssert(!m_Allocator);
    netAssert(!m_ResponseContent);

    m_Allocator = allocator;
    m_ResponseContent = content;
    m_ResponseMsg.Reset(content, contentLen, m_RequestMsg.GetTransactionId());
}

void
netRttpRequest::Finish(const bool success)
{
    success ? m_Status.SetSucceeded() : m_Status.SetFailed();
}

//Negative numbers for window bits imply raw deflate format (RFC 1951),
//which is what's understood by .NET's System.IO.Compression.DeflateStream.
#define __DEFLATE_WINDOW_BITS   -9  /* window size of 512 bytes */
#define __DEFLATE_MEM_LEVEL     1

#define __ABS_DEFLATE_WINDOW_BITS \
    (((__DEFLATE_WINDOW_BITS) < 0) ? -(__DEFLATE_WINDOW_BITS) : (__DEFLATE_WINDOW_BITS))

#define __DEFLATE_MEM_REQUIRED\
    ((1 << (__ABS_DEFLATE_WINDOW_BITS+2)) +  (1 << (__DEFLATE_MEM_LEVEL+9)))

#define __INFLATE_MEM_REQUIRED\
    (1 << __ABS_DEFLATE_WINDOW_BITS+2)

//////////////////////////////////////////////////////////////////////////
//  netRttpStream
//////////////////////////////////////////////////////////////////////////

static void* rttpZAlloc(void* /*opaque*/, unsigned items, unsigned size)
{
    NET_RAGE_TRACK(rttp);
    RAGE_TRACK(zlib);
    return sysMemAllocator::GetCurrent().TryAllocate(items*size, 0);
}

static void rttpZFree(void* /*opaque*/, void* ptr)
{
    sysMemAllocator::GetCurrent().Free(ptr);
}

netRttpStream::netRttpStream()
: m_State(STATE_INITIAL)
, m_StreamType(STREAMTYPE_INVALID)
, m_NumConsumed(0)
, m_NumProduced(0)
{
    sysMemSet(&m_ZlibStrm, 0, sizeof(m_ZlibStrm));
}

netRttpStream::~netRttpStream()
{
    this->Clear();
}

void
netRttpStream::Clear()
{
    if(STREAMTYPE_ENCODE == m_StreamType)
    {
		NET_ASSERTS_ONLY(const int zErr =) deflateEnd(&m_ZlibStrm);
        netAssert(Z_OK == zErr || Z_DATA_ERROR == zErr);
    }
    else if(STREAMTYPE_DECODE == m_StreamType)
    {
        (void)netVerify(Z_OK == inflateEnd(&m_ZlibStrm));
    }

    m_ZlibStrm.avail_in = m_ZlibStrm.avail_out = 0;
    m_ZlibStrm.next_in = m_ZlibStrm.next_out = NULL;
    m_ZlibStrm.zalloc = rttpZAlloc;
    m_ZlibStrm.zfree = rttpZFree;

    m_Header.Clear();
    m_NumConsumed = m_NumProduced = 0;
    m_StreamType = STREAMTYPE_INVALID;
    m_State = STATE_INITIAL;
}

bool
netRttpStream::BeginEncode(const unsigned contentLen,
                            const u16 txId)
{
    this->Clear();

    bool success = netVerify(contentLen <= netRttpHeader::MAX_CONTENT_LENGTH);

    if(success)
    {
        success = netVerify(Z_OK == deflateInit2(&m_ZlibStrm,
                                                    9,
                                                    Z_DEFLATED,
                                                    __DEFLATE_WINDOW_BITS,
                                                    __DEFLATE_MEM_LEVEL,
                                                    Z_DEFAULT_STRATEGY));
    }

    if(success)
    {
        m_StreamType = STREAMTYPE_ENCODE;
        m_State = STATE_ENCODING_HEADER;

        unsigned flags = 0;
        
        if(0 == contentLen)
        {
            flags |= RTTP_HEADERFLAG_STREAM;
        }

        if(RTTP_INVALID_TXID == txId)
        {
            m_Header.ResetForPush(netRttpHeader::SIGNATURE,
                                contentLen,
                                (u8) flags);
        }
        else
        {
            m_Header.ResetForTransaction(netRttpHeader::SIGNATURE,
                                        contentLen,
                                        txId,
                                        (u8) flags);
        }
    }

    return success;
}

bool
netRttpStream::BeginEncode(const u16 txId)
{
    return this->BeginEncode(0, txId);
}

bool
netRttpStream::BeginDecode()
{
    bool success = false;

    this->Clear();

    success = netVerify(Z_OK == inflateInit2(&m_ZlibStrm, __DEFLATE_WINDOW_BITS));

    if(success)
    {
        m_StreamType = STREAMTYPE_DECODE;
        m_State = STATE_DECODING_HEADER;
    }

    return success;
}

void
netRttpStream::Encode(const void* src,
                    const unsigned srcLen,
                    void* dst,
                    const unsigned dstLen,
                    unsigned* numConsumed,
                    unsigned* numProduced)
{
    netAssert(this->IsEncoding());
    netAssert(src || 0 == srcLen);
    netAssert(dst);
    netAssert(dstLen >= netRttpHeader::BYTE_SIZEOF_HEADER);

    *numConsumed = *numProduced = 0;

    if(STATE_ENCODING_HEADER == m_State)
    {
        if(netVerify(m_Header.Export(dst, dstLen, numProduced)))
        {
            m_State = STATE_ENCODING;
        }
        else
        {
            m_State = STATE_ERROR;
        }
    }

    if(STATE_ENCODING == m_State && dstLen > *numProduced)
    {
        m_ZlibStrm.avail_in = srcLen;
        m_ZlibStrm.next_in = (Bytef*) src;

        const unsigned availOut = dstLen - *numProduced;
        u8* zlibDst = ((u8*) dst) + *numProduced;
        m_ZlibStrm.avail_out = availOut;
        m_ZlibStrm.next_out = (Bytef*) zlibDst;

        int zflush = Z_NO_FLUSH;

        if(0 == srcLen)
        {
            //Zero srcLen implies the caller has no more data to encode.
            zflush = Z_FINISH;
        }
        else if(m_Header.GetContentLength())
        {
            netAssert(!m_Header.IsStream());

            //If we're about to consume the entire contents then
            //set the flush type to Z_FINISH.
            const unsigned totalConsumed = srcLen + m_NumConsumed;

            netAssert(totalConsumed <= m_Header.GetContentLength());

            if(totalConsumed == m_Header.GetContentLength())
            {
                zflush = Z_FINISH;
            }
        }

        const int zlibErr = deflate(&m_ZlibStrm, zflush);

        if(Z_STREAM_END == zlibErr)
        {
            m_State = STATE_COMPLETE;
        }
        else if(Z_BUF_ERROR == zlibErr)
        {
            //No progress possible due to insufficient buffer.
        }
        else if(zlibErr < Z_OK)
        {
            m_State = STATE_ERROR;
        }

        const unsigned zlibProduced = availOut - m_ZlibStrm.avail_out;
        *numConsumed += srcLen - m_ZlibStrm.avail_in;
        *numProduced += zlibProduced;

        //Make sure we don't encode more than the length of the content.
        if(!netVerify(m_Header.IsStream()
                        || (m_NumConsumed + *numConsumed) <= m_Header.GetContentLength()))
        {
            m_State = STATE_ERROR;
        }

        //Make sure that when srcLen == 0 (indicating no more content) that
        //we've encoded exactly the size of the content buffer..
        if(!netVerify(srcLen > 0
                        || m_Header.IsStream()
                        || m_Header.GetContentLength() == this->GetNumConsumed()))
        {
            m_State = STATE_ERROR;
        }

        const unsigned mixer = m_Header.GetMixer();
        for(int i = 0; i < (int) zlibProduced; ++i)
        {
            zlibDst[i] ^= mixer;
        }
    }

    m_NumConsumed += *numConsumed;
    m_NumProduced += *numProduced;
}

void
netRttpStream::Decode(const void* src,
                        const unsigned srcLen,
                        void* dst,
                        const unsigned dstLen,
                        unsigned* numConsumed,
                        unsigned* numProduced)
{
    netAssert(this->IsDecoding());
    netAssert(src);
    netAssert(srcLen > 0);
    netAssert(dst || STATE_DECODING_HEADER == m_State);
    netAssert(dstLen > 0 || STATE_DECODING_HEADER == m_State);

    *numConsumed = *numProduced = 0;

    if(STATE_DECODING_HEADER == m_State)
    {
        netAssert(m_NumConsumed < netRttpHeader::BYTE_SIZEOF_HEADER);
        unsigned numToCopy = netRttpHeader::BYTE_SIZEOF_HEADER - m_NumConsumed;

        if(numToCopy > srcLen)
        {
            numToCopy = srcLen;
        }

        for(int i = 0; i < (int) numToCopy; ++i, ++*numConsumed)
        {
            m_HeaderBuf[m_NumConsumed + i] = ((const u8*)src)[i];
        }

        netAssert((m_NumConsumed + *numConsumed) <= netRttpHeader::BYTE_SIZEOF_HEADER);
        if(netRttpHeader::BYTE_SIZEOF_HEADER == (m_NumConsumed + *numConsumed))
        {
            if(netVerify(m_Header.Import(m_HeaderBuf, netRttpHeader::BYTE_SIZEOF_HEADER, NULL)))
            {
                m_State = STATE_DECODING;
            }
            else
            {
                m_State = STATE_ERROR;
            }
        }
    }

    if(STATE_DECODING == m_State && srcLen > *numConsumed)
    {
        unsigned availIn = srcLen - *numConsumed;
        const u8* nextIn = ((const u8*) src) + *numConsumed;
        unsigned availOut = dstLen - *numProduced;
        u8* nextOut = ((u8*) dst) + *numProduced;
        const unsigned mixer = m_Header.GetMixer();

        int zlibErr = Z_OK;
        while(availOut > 0 && availIn > 0 && Z_OK == zlibErr)
        {
            u8 unMixBuf[128];
            unsigned numUnmixed;
            for(numUnmixed = 0; numUnmixed < sizeof(unMixBuf) && numUnmixed < availIn; ++numUnmixed)
            {
                unMixBuf[numUnmixed] = u8(nextIn[numUnmixed] ^ mixer);
            }

            m_ZlibStrm.avail_in = numUnmixed;
            m_ZlibStrm.next_in = (Bytef*) unMixBuf;

            m_ZlibStrm.avail_out = availOut;
            m_ZlibStrm.next_out = (Bytef*) nextOut;

            zlibErr = inflate(&m_ZlibStrm, Z_SYNC_FLUSH);

            if(Z_STREAM_END == zlibErr)
            {
                m_State = STATE_COMPLETE;
            }
            else if(Z_BUF_ERROR == zlibErr)
            {
                //No progress possible due to insufficient buffer.
            }
            else if(zlibErr < Z_OK)
            {
                m_State = STATE_ERROR;
            }

            const unsigned tmpNumConsumed = numUnmixed - m_ZlibStrm.avail_in;
            netAssert(tmpNumConsumed <= availIn);

            *numConsumed += tmpNumConsumed;
            *numProduced += availOut - m_ZlibStrm.avail_out;

            availIn -= tmpNumConsumed;
            nextIn += tmpNumConsumed;
            availOut = m_ZlibStrm.avail_out;
            nextOut = m_ZlibStrm.next_out;
        }
    }

    m_NumConsumed += *numConsumed;
    m_NumProduced += *numProduced;
}

const netRttpHeader&
netRttpStream::GetHeader() const
{
    return m_Header;
}

unsigned
netRttpStream::GetNumConsumed() const
{
    return m_NumConsumed;
}

unsigned
netRttpStream::GetNumProduced() const
{
    return m_NumProduced;
}

bool
netRttpStream::IsEncoding() const
{
    return STATE_ENCODING_HEADER == m_State
            || STATE_ENCODING == m_State;
}

bool
netRttpStream::IsDecoding() const
{
    return STATE_DECODING_HEADER == m_State
            || STATE_DECODING == m_State;
}

bool
netRttpStream::IsComplete() const
{
    return STATE_COMPLETE == m_State;
}

bool
netRttpStream::HasError() const
{
    return STATE_ERROR == m_State;
}

//////////////////////////////////////////////////////////////////////////
// netRttp
//////////////////////////////////////////////////////////////////////////
netRttp::netRttp()
: m_Initialized(false)
, m_CurSndRqst(NULL)
, m_CurRcvRqst(NULL)
, m_Skt(-1)
, m_KeepAliveMs(0)
, m_KeepAliveAccumMs(0)
, m_Allocator(NULL)
{
    ResetSend();
    ResetReceive();
}

netRttp::~netRttp()
{
    Shutdown();
}

bool
netRttp::Init(sysMemAllocator* allocator,
              const int skt, 
              PushDelegate pushDlgt,
              const unsigned keepAliveMs)
{
    netAssert(allocator);
    netAssert(skt >= 0);

    bool success = false;

    if(netVerify(!m_Initialized))
    {
        netAssert(-1 == m_Skt);
        netAssert(!m_PushDlgt.IsBound());
        netAssert(0 == m_KeepAliveMs);
        netAssert(m_SendingRequests.empty());
        netAssert(m_ReceivingRequests.empty());
        netAssert(!m_CurSndRqst);
        netAssert(!m_CurRcvRqst);
        netAssert(0 == m_KeepAliveAccumMs);
        netAssert(!m_Allocator);

        m_Allocator = allocator;
        m_Skt = skt;
        m_PushDlgt = pushDlgt;
        m_KeepAliveMs = keepAliveMs;

        m_Initialized = success = true;
    }

    return success;
}

void
netRttp::Shutdown()
{
    if(m_Initialized)
    {
        m_Skt = -1;

        ResetSend();
        ResetReceive();

        netAssert(!m_CurSndRqst);
        netAssert(!m_CurRcvRqst);

        while(!m_SendingRequests.empty())
        {
            RequestList::iterator iter = m_SendingRequests.begin();
            netRttpRequest* rqst = *iter;
            m_SendingRequests.erase(iter);
            rqst->Finish(false);
        }

        while(!m_ReceivingRequests.empty())
        {
            RequestList::iterator iter = m_ReceivingRequests.begin();
            netRttpRequest* rqst = *iter;
            m_ReceivingRequests.erase(iter);
            rqst->Finish(false);
        }

        m_PushDlgt.Unbind();

        m_Allocator = NULL;

        m_Initialized = false;
    }
}

bool
netRttp::IsValid() const
{
    return m_Skt >= 0;
}

void
netRttp::Update()
{
    m_TimeStep.SetTime(sysTimer::GetSystemMsTime());

    if(IsValid())
    {
        this->UpdateSending();
        this->UpdateReceiving();
        this->CheckForTimeouts();
    }
}

static volatile unsigned s_NextRttpTxId = RTTP_INVALID_TXID;

bool
netRttp::SendRequest(const void* content,
                     const unsigned contentLen,
                     netRttpRequest* rqst,
                     const unsigned timeoutSecs)
{
    netAssert(IsValid());
    netAssert(timeoutSecs > 0);

    bool success = false;

    if(netVerify(!rqst->Pending()))
    {
        if(0 == contentLen)
        {
            netWarning("Attempt to send RTTP request with no content");
        }
        else
        {
            //TODO: Consider moving TXID assignment out of here, that way netRttp can deal
            //      with pure netRttpRequests without knowing details.
            u16 txId = RTTP_INVALID_TXID;

            do 
            {
                txId = (u16) (sysInterlockedIncrement(&s_NextRttpTxId) & 0xFFFF);
            } while(RTTP_INVALID_TXID == txId);

            rqst->ResetRequest(content,
                                contentLen,
                                txId,
                                timeoutSecs);

            m_SendingRequests.push_back(rqst);
            success = true;
        }
    }

    return success;
}

bool
netRttp::SendMsg(const void* content,
                 const unsigned contentLen,
                 netRttpRequest* rqst,
                 const unsigned timeoutSecs)
{
    netAssert(IsValid());
    netAssert(timeoutSecs > 0);

    bool success = false;

    if(netVerify(!rqst->Pending()))
    {
        if(0 == contentLen)
        {
            netWarning("Attempt to send RTTP message with no content");
        }
        else
        {
            rqst->ResetRequest(content,
                                contentLen,
                                RTTP_INVALID_TXID,
                                timeoutSecs);

            m_SendingRequests.push_back(rqst);
            success = true;
        }
    }

    return success;
}

#if __DEV

void
netRttp::TestStream()
{
    netRttpStream rttpStrm;

    netRandom rng;
    const int rngSeed = 21501009;//(int) sysTimer::GetSystemMsTime();
    rng.Reset(rngSeed);

    char str[337];

    for(int i = 0; i < sizeof(str) - 1; ++i)
    {
        str[i] = (char) rng.GetRanged('A', 'z');
    }

    str[sizeof(str)-1] = '\0';

    rtry
    {
        for(int attempts = 0; attempts < 8; ++attempts)
        {
            u8 encBuf[(sizeof(str) + netRttpHeader::BYTE_SIZEOF_HEADER) * 3];
            unsigned encBufConsumed = 0;

            for(int i = 0; i < 3; ++i)
            {
                rverify(rttpStrm.BeginEncode(123),catchall,);

                u8 tmpBuf[37];
                unsigned tmpBufConsumed = 0;

                int strConsumed = 0;

                while(strConsumed < sizeof(str))
                {
                    int len = 0;
                    while(len < 1 && strConsumed < sizeof(str))
                    {
                        len = rng.GetRanged(strConsumed, sizeof(str)) - strConsumed;
                    }

                    netAssert(strConsumed < sizeof(str) || len == 0);

                    while(len > 0)
                    {
                        unsigned tmpConsumed, tmpProduced;
                        rttpStrm.Encode(&str[strConsumed],
                                        len,
                                        &tmpBuf[tmpBufConsumed],
                                        sizeof(tmpBuf) - tmpBufConsumed,
                                        &tmpConsumed,
                                        &tmpProduced);

                        strConsumed += tmpConsumed;
                        len -= (int) tmpConsumed;
                        tmpBufConsumed += tmpProduced;

                        if(0 == tmpConsumed)
                        {
                            netAssert(tmpBufConsumed > 0);
                            sysMemCpy(&encBuf[encBufConsumed], tmpBuf, tmpBufConsumed);
                            encBufConsumed += tmpBufConsumed;
                            tmpBufConsumed = 0;
                        }
                    }

                    rverify(strConsumed <= sizeof(str), catchall,);
                }

                while(rttpStrm.IsEncoding())
                {
                    unsigned tmpConsumed, tmpProduced;
                    rttpStrm.Encode(NULL,
                                    0,
                                    &tmpBuf[tmpBufConsumed],
                                    sizeof(tmpBuf) - tmpBufConsumed,
                                    &tmpConsumed,
                                    &tmpProduced);

                    netAssert(!rttpStrm.HasError());

                    tmpBufConsumed += tmpProduced;

                    netAssert(tmpBufConsumed > 0);

                    sysMemCpy(&encBuf[encBufConsumed], tmpBuf, tmpBufConsumed);
                    encBufConsumed += tmpBufConsumed;
                    tmpBufConsumed = 0;
                }
            }

            encBufConsumed = 0;

            u8 decBuf[sizeof(str) * 3];
            unsigned decBufConsumed = 0;

            for(int i = 0; i < 3; ++i)
            {
                rverify(rttpStrm.BeginDecode(),catchall,);

                do
                {
                    u8 tmpBuf[13];
                    unsigned numConsumed, numProduced;
                    rttpStrm.Decode(&encBuf[encBufConsumed],
                                    sizeof(encBuf) - encBufConsumed,
                                    tmpBuf,
                                    sizeof(tmpBuf),
                                    &numConsumed,
                                    &numProduced);

                    encBufConsumed += numConsumed;

                    if(numProduced)
                    {
                        netAssert(decBufConsumed + numProduced <= sizeof(decBuf));
                        sysMemCpy(&decBuf[decBufConsumed], tmpBuf, numProduced);
                        decBufConsumed += numProduced;
                    }
                }
                while(rttpStrm.IsDecoding());

                rverify(!rttpStrm.HasError(),catchall,);

                rverify(!strcmp((const char*) decBuf, str), catchall, );
            }
        }
    }
    rcatchall
    {
    }
}

#endif  //__DEV

//private:

void
netRttp::SendKeepalive()
{
    if(!m_KeepaliveTcpOp.Pending())
    {
        netRttpHeader header;
        header.ResetForKeepAlive(netRttpHeader::SIGNATURE);

        if(netVerify(header.Export(m_KeepaliveBuf, sizeof(m_KeepaliveBuf), NULL)))
        {
            netTcp::SendBufferAsync(m_Skt,
                                    m_KeepaliveBuf,
                                    sizeof(m_KeepaliveBuf),
                                    3,
                                    &m_KeepaliveTcpOp);

            netDebug("Keepalive sent");

            m_KeepAliveAccumMs = 0;
        }
    }
}

void 
netRttp::ResetSend()
{
    if(m_SendTcpOp.Pending())
    {
        netTcp::CancelAsync(&m_SendTcpOp);
    }

    m_SndStrm.Clear();
    m_KeepAliveAccumMs = 0;

    if(m_CurSndRqst)
    {
        netAssert(m_SendingRequests.front() == m_CurSndRqst);
        m_SendingRequests.pop_front();
        if(m_CurSndRqst->Pending())
        {
            m_CurSndRqst->Finish(false);
        }

        m_CurSndRqst = NULL;
    }
}


void
netRttp::ResetReceive()
{
    if(m_CurRcvRqst)
    {
        if(&m_PushedRqst != m_CurRcvRqst)
        {
            m_ReceivingRequests.erase(m_CurRcvRqst);
            if(m_CurRcvRqst->Pending())
            {
                m_CurRcvRqst->Finish(false);
            }
        }
        else
        {
            m_CurRcvRqst->Dispose();
        }

        m_CurRcvRqst = NULL;
    }

    m_RcvStrm.BeginDecode();
}

void 
netRttp::UpdateSending()
{
    if(this->IsValid())
    {
        //If we've finished sending the current request then remove
        //it from the send queue.
        //If it's a transaction put it on the receive queue in preparation
        //for receiving the response.
        //If it's a message then just finish it up.
        if(m_CurSndRqst && !m_SendTcpOp.Pending())
        {
            netAssert(m_CurSndRqst->Pending());

            if(!m_SendTcpOp.Succeeded())
            {
                this->ResetSend();
            }
            else if(m_SndStrm.IsComplete())
            {
                if(m_CurSndRqst->IsTransaction())
                {
                    m_SendingRequests.erase(m_CurSndRqst);
                    m_ReceivingRequests.push_back(m_CurSndRqst);
                    m_CurSndRqst = NULL;
                }
                else
                {
                    //Not a transaction - don't need to wait for a response.
                    m_CurSndRqst->Finish(true);
                }

                this->ResetSend();
            }
        }

        //Send requests
        if(!m_CurSndRqst && !m_SendingRequests.empty())
        {
            m_CurSndRqst = m_SendingRequests.front();

            //Begin a new rttp message.

            if(!m_SndStrm.BeginEncode(m_CurSndRqst->GetRequestMsg().GetContentLength(),
                                        m_CurSndRqst->GetTransactionId()))
            {
                netError("Failed to write RTTP header");
                m_CurSndRqst->Finish(false);
                this->ResetSend();  //Sets m_CurSndRqst to NULL.
            }
        }

        if(m_CurSndRqst)
        {
            this->SendRequest();
            m_KeepAliveAccumMs = 0;
        }
        //Send a keepalive if necessary
        else if(m_KeepAliveMs)
        {
            m_KeepAliveAccumMs += m_TimeStep.GetTimeStep();

            if(m_KeepAliveAccumMs >= m_KeepAliveMs)
            {
                this->SendKeepalive();
            }
        }
    }
}

void
netRttp::SendRequest()
{
    netAssert(this->IsValid());
    netAssert(!m_SendTcpOp.Pending());

    if(netVerify(m_CurSndRqst))
    {
        netAssert(m_CurSndRqst->Pending());
        netAssert(m_SendingRequests.front() == m_CurSndRqst);

        const unsigned sizeofMsg = m_CurSndRqst->GetRequestMsg().GetContentLength();

        //Shouldn't permit sending messages with zero bytes.
        netAssert(sizeofMsg > 0);

        const u8* content = (const u8*) m_CurSndRqst->GetRequestMsg().GetContent();
        unsigned numConsumed = m_SndStrm.GetNumConsumed(), numProduced = 0;
        netAssert(numConsumed <= sizeofMsg);

        if(m_SndStrm.IsEncoding())
        {
            m_SndStrm.Encode(&content[numConsumed],
                            sizeofMsg - numConsumed,
                            m_SndBuf,
                            sizeof(m_SndBuf),
                            &numConsumed,
                            &numProduced);

            if(m_SndStrm.HasError())
            {
                netError("Failed to write RTTP content");
                m_CurSndRqst->Finish(false);
                this->ResetSend();
            }
            else
            {
                netAssert(numConsumed > 0 || m_SndStrm.GetNumConsumed() == sizeofMsg);

                if(numProduced > 0)
                {
                    if(!netTcp::SendBufferAsync(m_Skt,
                                                m_SndBuf,
                                                numProduced,
                                                m_CurSndRqst->m_TimeoutSecs,
                                                &m_SendTcpOp))
                    {
                        netError("Failed to send RTTP content");
                        m_CurSndRqst->Finish(false);
                        this->ResetSend();
                    }
                }
            }
        }
    }
}

void 
netRttp::UpdateReceiving()
{
    netAssert(m_RcvStrm.IsDecoding());

    rtry
    {
        rverify(this->IsValid(), catchall,);

        unsigned numRead = 0;

        const netTcpResult result =
            netTcp::Receive(m_Skt,
                            m_RcvBuf,
                            sizeof(m_RcvBuf),
                            &numRead);

        rcheck(NETTCP_RESULT_ERROR != result,SocketError,);

        unsigned totalConsumed = 0;

        //Loop until we consume all bytes received.
        while(totalConsumed < numRead)
        {
            unsigned numConsumed, numProduced;

            const netRttpHeader& header = m_RcvStrm.GetHeader();

            if(!header.IsValid())
            {
                netAssert(!m_CurRcvRqst);

                m_RcvStrm.Decode(&m_RcvBuf[totalConsumed],
                                numRead - totalConsumed,
                                NULL,
                                0,
                                &numConsumed,
                                &numProduced);
                totalConsumed += numConsumed;

                rverify(!m_RcvStrm.HasError(),
                        catchall,
                        netError("Error decoding RTTP"));

                //We need to have consumed something
                rverify(numConsumed > 0,catchall,)

                if(header.IsValid())
                {
                    const u16 txId = header.GetTransactionId();

                    if(txId != RTTP_INVALID_TXID)
                    {
                        //Find request in receiving queue matching tx ID.
                        m_CurRcvRqst = GetReceivingRequest(txId, false);
                    }
                    else
                    {
                        m_CurRcvRqst = &m_PushedRqst;
                    }

                    //The request might have been canceled.
                    //If so m_CurRcvRqst will be null and we don't
                    //need to allocate a content buffer.
                    if(m_CurRcvRqst)
                    {
                        //For now we need to know the size of the content.
                        //Therefore we don't handle streams.
                        netAssert(!header.IsStream());

                        const unsigned contentLen = header.GetContentLength();

                        rverify(contentLen > 0,
                                catchall,
                                netError("Zero RTTP content length"));

                        //Allocate a buffer large enough to hold the
                        //entire contents of the message.
                        NET_RAGE_TRACK(rttp);
                        u8* content = (u8*)m_Allocator->RAGE_LOG_ALLOCATE(contentLen, 0);

                        rverify(content,
                                catchall,
                                netError("Failed to allocate content buffer (%u bytes) on request (txid=%u)",
                                        contentLen,
                                        txId));

                        m_CurRcvRqst->SetResponse(m_Allocator,
                                                content, 
                                                contentLen);
                    }
                }
            }

            if(m_CurRcvRqst)
            {
                netAssert(header.IsValid());
                //We don't handle streams for now.
                netAssert(!header.IsStream());
                netAssert(m_RcvStrm.IsDecoding());
                
                u8* content = (u8*) m_CurRcvRqst->GetResponseMsg().GetContent();
                const unsigned contentLen = header.GetContentLength();
                const unsigned contentOffset = m_RcvStrm.GetNumProduced();

                netAssert(m_RcvStrm.GetNumProduced() <= contentLen);
                m_RcvStrm.Decode(&m_RcvBuf[totalConsumed],
                                numRead - totalConsumed,
                                &content[contentOffset],
                                contentLen - contentOffset,
                                &numConsumed,
                                &numProduced);
                totalConsumed += numConsumed;

                rverify(!m_RcvStrm.HasError(),
                        catchall,
                        netError("Error decoding RTTP"));

                //We need to have either consumed or produced something
                //or we'll land in an infinite loop.
                rverify(numConsumed > 0 || numProduced > 0,catchall,)

                if(m_RcvStrm.IsComplete())
                {
                    if(&m_PushedRqst == m_CurRcvRqst)
                    {
                        if(m_PushDlgt.IsBound())
                        {
                            //This is a pushed message.  Dispatch it
                            //to the app.
                            m_PushDlgt(m_CurRcvRqst->GetResponseMsg());
                        }
                    }
                    else
                    {
                        m_CurRcvRqst->Finish(true);
                    }

                    //Prepare to read the next message.
                    this->ResetReceive();
                }
            }
            else if(header.IsValid())
            {
                netAssert(m_RcvStrm.IsDecoding());

                //The request was likely canceled, but we still need
                //to consume the bytes received.
                u8 buf[128];
                m_RcvStrm.Decode(&m_RcvBuf[totalConsumed],
                                numRead - totalConsumed,
                                buf,
                                sizeof(buf),
                                &numConsumed,
                                &numProduced);
                totalConsumed += numConsumed;

                rverify(!m_RcvStrm.HasError(),
                        catchall,
                        netError("Error decoding RTTP"));

                //We need to have either consumed or produced something
                //or we'll land in an infinite loop.
                rverify(numConsumed > 0 || numProduced > 0,catchall,)

                if(m_RcvStrm.IsComplete())
                {
                    //Prepare to read the next message.
                    this->ResetReceive();
                }
            }
        }
    }
    rcatch(SocketError)
    {
        this->Shutdown();
    }
    rcatchall
    {
        this->ResetReceive();
        netAssert(!m_CurRcvRqst);
    }
}

void
netRttp::CheckForTimeouts()
{
    RequestList::iterator it = m_SendingRequests.begin();
    RequestList::iterator next = it;
    RequestList::const_iterator stop = m_SendingRequests.end();

    for(++next; stop != it; it = next)
    {
        netRttpRequest* rqst = *it;
        rqst->m_TimeLeftMs -= m_TimeStep.GetTimeStep();

        if(rqst->m_TimeLeftMs < 0)
        {
            netDebug("Request timed out while sending");

            if(m_CurSndRqst == rqst)
            {
                this->ResetSend();
            }
            else
            {
                rqst->Finish(false);
                m_SendingRequests.erase(it);
            }
        }
    }

    it = m_ReceivingRequests.begin();
    next = it;
    stop = m_ReceivingRequests.end();

    for(++next; stop != it; it = next)
    {
        netRttpRequest* rqst = *it;
        rqst->m_TimeLeftMs -= m_TimeStep.GetTimeStep();

        if(rqst->m_TimeLeftMs < 0)
        {
            netDebug("Request timed out while receiving response");

            rqst->Finish(false);
            m_ReceivingRequests.erase(it);
        }
    }
}

netRttpRequest*
netRttp::GetReceivingRequest(const u16 txid, const bool remove)
{
    netRttpRequest* rqst = NULL;

    if(!m_ReceivingRequests.empty())
    {
        RequestList::iterator iter = m_ReceivingRequests.begin();
        RequestList::iterator endIter = m_ReceivingRequests.end();

        while(iter != endIter && !rqst)
        {
            netRttpRequest* r = *iter;

            if(r->GetTransactionId() == txid)
            {
                if(remove)
                {
                    m_ReceivingRequests.erase(iter);
                }

                rqst = r;
            }

            ++iter;
        }
    }

    return rqst;
}

}   //namespace rage
