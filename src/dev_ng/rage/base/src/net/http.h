// 
// net/http.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_HTTP_H 
#define NET_HTTP_H 

#include "netaddress.h"
#include "tcp.h"
#include "time.h"
#include "atl/inlist.h"
#include "string/stringbuilder.h"
#include "diag/channel.h"
#include "file/device.h"
#include "file/file_config.h"
#include "system/uri.h"

#if RSG_DURANGO
#include "durango/xblhttp.h"
#endif

namespace rage
{

class sysMemAllocator;
class netStatus;
class netHttpRequest;

enum netHttpOptions
{
    //Default/empty options
    NET_HTTP_OPTIONS_NONE = 0x0,

    //Dumps contents of requests and responses
    NET_HTTP_DUMP  = 0x01,

	//Force the request to be sent with non-chunked encoding
	NET_HTTP_FORCE_TRANSFER_ENCODING_NORMAL  = 0x02,

	//Use Rage's custom http lib instead of the platform lib (eg. IXHR2 on Xbox One)
	//This is useful if you want to be able to hit URLs in development without
	//being signed into XBL (eg. Bugstar urls)
	NET_HTTP_USE_RAGE_HTTP = 0x04,

	//The request's query string will be maintained through redirects. If the redirect
	//URI from the server includes a different query string, we append the original 
	//query string to the server's.
	NET_HTTP_MAINTAIN_QUERY_ON_REDIRECT = 0x20,
};

enum netHttpVerb
{
    NET_HTTP_VERB_INVALID,
    NET_HTTP_VERB_GET,
    NET_HTTP_VERB_POST,
	NET_HTTP_VERB_PUT,
    NET_HTTP_VERB_DELETE,
	NET_HTTP_VERB_HEAD,

    NET_HTTP_VERBS_COUNT
};

enum netHttpStatusCodes
{
    NET_HTTPSTATUS_CONTINUE                         = 100,
    NET_HTTPSTATUS_SWITCHING_PROTOCOLS              = 101,
    NET_HTTPSTATUS_OK                               = 200,
    NET_HTTPSTATUS_CREATED                          = 201,
    NET_HTTPSTATUS_ACCEPTED                         = 202,
    NET_HTTPSTATUS_NON_AUTHORITATIVE_INFORMATION    = 203,
    NET_HTTPSTATUS_NO_CONTENT                       = 204,
    NET_HTTPSTATUS_RESET_CONTENT                    = 205,
    NET_HTTPSTATUS_PARTIAL_CONTENT                  = 206,
    NET_HTTPSTATUS_MULTIPLE_CHOICES                 = 300,
    NET_HTTPSTATUS_MOVED_PERMANENTLY                = 301,
    NET_HTTPSTATUS_FOUND                            = 302,
    NET_HTTPSTATUS_SEE_OTHER                        = 303,
    NET_HTTPSTATUS_NOT_MODIFIED                     = 304,
    NET_HTTPSTATUS_USE_PROXY                        = 305,
    NET_HTTPSTATUS_UNUSED                           = 306,
    NET_HTTPSTATUS_TEMPORARY_REDIRECT               = 307,
	NET_HTTPSTATUS_RESUME_INCOMPLETE				= 308,
    NET_HTTPSTATUS_BAD_REQUEST                      = 400,
    NET_HTTPSTATUS_UNAUTHORIZED                     = 401,
    NET_HTTPSTATUS_PAYMENT_REQUIRED                 = 402,
    NET_HTTPSTATUS_FORBIDDEN                        = 403,
    NET_HTTPSTATUS_NOT_FOUND                        = 404,
    NET_HTTPSTATUS_METHOD_NOT_ALLOWED               = 405,
    NET_HTTPSTATUS_NOT_ACCEPTABLE                   = 406,
    NET_HTTPSTATUS_PROXY_AUTHENTICATION_REQUIRED    = 407,
    NET_HTTPSTATUS_REQUEST_TIMEOUT                  = 408,
    NET_HTTPSTATUS_CONFLICT                         = 409,
    NET_HTTPSTATUS_GONE                             = 410,
    NET_HTTPSTATUS_LENGTH_REQUIRED                  = 411,
    NET_HTTPSTATUS_PRECONDITION_FAILED              = 412,
    NET_HTTPSTATUS_REQUEST_ENTITY_TOO_LARGE         = 413,
    NET_HTTPSTATUS_REQUEST_URI_TOO_LONG             = 414,
    NET_HTTPSTATUS_UNSUPPORTED_MEDIA_TYPE           = 415,
    NET_HTTPSTATUS_REQUEST_RANGE_NOT_SATISFIABLE    = 416,
	NET_HTTPSTATUS_EXPECTATION_FAILED               = 417,
    NET_HTTPSTATUS_INTERNAL_SERVER_ERROR            = 500,
    NET_HTTPSTATUS_NOT_IMPLEMENTED                  = 501,
    NET_HTTPSTATUS_BAD_GATEWAY                      = 502,
    NET_HTTPSTATUS_SERVICE_UNAVAILABLE              = 503,
    NET_HTTPSTATUS_GATEWAY_TIMEOUT                  = 504,
    NET_HTTPSTATUS_HTTP_VERSION_NOT_SUPPORTED       = 505,

	//Rockstar made up Error codes.
	NET_HTTPSTATUS_CANT_BUFFER_RESPONSE             = 600,
    NET_HTTPSTATUS_OUT_OF_MEMORY                    = 601
};

enum netHttpAbortReason
{
	/* Used by Telemetry to track HTTP failures.
		New Abort Reasons should be added to end. */
	NET_HTTPABORT_NONE										= 0,
	NET_HTTPABORT_COMMIT_FAILED								= 1,
	NET_HTTPABORT_CANCELED									= 2,
	NET_HTTPABORT_REQUEST_TIMED_OUT							= 3,
	NET_HTTPABORT_RESOLVE_HOST_FAILED						= 4,
	NET_HTTPABORT_RESOLVING_HOST_FAILED						= 5,
	NET_HTTPABORT_CONNECT_HOST_FAILED						= 6,
	NET_HTTPABORT_CONNECTING_HOST_FAILED					= 7,
	NET_HTTPABORT_SEND_HEADER_FAILED						= 8,
	NET_HTTPABORT_RECEIVE_HEADER_FAILED						= 9,
	NET_HTTPABORT_PROCESS_RESPONSE_HEADER_FAILED			= 10,
	NET_HTTPABORT_RESPONSE_NO_LOCATION_LENGTH				= 11,
	NET_HTTPABORT_RESPONSE_REDIRECT_FAILED					= 12,
	NET_HTTPABORT_RESPONSE_GET_LOCATION_FAILED				= 13,
	NET_HTTPABORT_RESPONSE_UNHANDLED_TRANSFER_ENCODING		= 14,
	NET_HTTPABORT_RESPONSE_PARSE_CONTENT_LENGTH_FAILED		= 15,
	NET_HTTPABORT_READ_BODY_FAILED							= 16,
	NET_HTTPABORT_CONSUME_BODY_FAILED						= 17,
	NET_HTTPABORT_CHUNK_OPERATION_FAILED					= 18,
	NET_HTTPABORT_ALLOCATE_CHUNK_FAILED						= 19,
	NET_HTTPABORT_SEND_CHUNK_FAILED							= 20,
	NET_HTTPABORT_XBL_RECEIVE_HEADER_FAILED					= 21,
	NET_HTTPABORT_XBL_RECEIVE_BODY_FAILED					= 22,
	NET_HTTPABORT_PROCESS_BOUNCE_CONTENT_FAILED				= 23,
	NET_HTTPABORT_READ_BODY_ONCE_FAILED						= 24,
	NET_HTTPABORT_XBOX_AUTH_TOKEN_FAILED					= 25,
};


//PURPOSE
//  Class uses to modify HTTP request and/or response data.
//  For requests, this filters data just about to be sent.
//  For responses, this filters data as its pulled off the wire.
//  Responses can also be generated by this filter without
//  any data going to/from a server.
class netHttpFilter
{
    friend class netHttpRequest;

public:
    netHttpFilter() {};
    virtual ~netHttpFilter() {};

    //PURPOSE
    //  Returns true depending on what filter can do.
    virtual bool CanFilterRequest() const = 0;
    virtual bool CanFilterResponse() const = 0;

	//PURPOSE
	//  Returns the amount of latency in ms to add to the request.
	virtual u32 DelayMs() const = 0;

	//PURPOSE
	//  Returns true if this filter will intercept the request
	//  and provide a custom response via Receive().
	//  Note: no data is sent to the server if this returns true.
	virtual bool CanIntercept() const = 0;
	
    //PURPOSE
    //  Processes outgoing request data, and writes result to output buffer.
    //  Derived filters are allowed to buffer data, but must write out all
    //  filtered data if endOfData is true.
    //PARAMS
    //  data                - Data to filter.  This may be NULL.
    //  dataLen             - Length of data to filter.  Will be 0 if data is NULL.
    //  finalCall           - If true, this is the last call for this request.
    //                        The filter should flush anything it has buffered, and
    //                        write any post-body data, like a hash digest.
    //  output              - Buffer to output filtered data to
    virtual bool FilterRequest(const u8* data,
                               const unsigned dataLen,
                               const bool finalCall,
                               datGrowBuffer& output) = 0;

    //PURPOSE
    //  Processes incoming response data, and writes any resulting data
    //  to the output device.  It is not required that each call write to
    //  the output device; if necessary, derived classes may wait until 
    //  more data is queued up.
    //PARAMS
    //  data                - Data to filter, or NULL if no more data
    //  dataLen             - Length of data, or 0 if data is NULL
    //  allDataReceived     - True if all response data has been received
    //  outputHandle        - Device handle to output filtered data to
    //  outputDevice        - Devie to output filtered data to
    //  numProcessed        - Number of bytes of data consumed by filter.  If the
    //                        filter doesn't yet have enough data to work with, it 
    //                        may return zero.
    //  hasDataPending      - If true, the filter has some data still pending
    //                        either filtering or delivery to the output device.
    //                        Caller should continue calling FilterResponse until
    //                        this returns false.
    virtual bool FilterResponse(const u8* data,
                                const unsigned dataLen,
                                const bool allDataReceived,
                                fiHandle& outputHandle,
                                const fiDevice* outputDevice,
                                unsigned* numProcessed,
                                bool* hasDataPending) = 0;

    //PURPOSE
    //  Returns user-agent string to add to request, or NULL if none.
    virtual const char* GetUserAgentString() const = 0;

    //PURPOSE
    //  Called before the request header is sent, allowing the filter to
    //  add any additional headers.
    //  Returns false if there was an error
    virtual bool ProcessRequestHeader(class netHttpRequest* request) = 0;

    //PURPOSE
    //  Called after entire response header has been read, allowing the filter
    //  to make decisions based on its contents.  Header values may be parsed
    //  with netHttpRequest methods like GetHeaderValue() and GetCookieValue().
    //  Should return false if there is an error and the request should be aborted
    virtual bool ProcessResponseHeader(const int statusCode, const char* header) = 0;

	//PURPOSE
	//  Responds to the http request with a custom response, without any
	//  data going to/from the server.
	//  Note: Return true in CanIntercept() to intercept a request.
	//PARAMS
	//  buf             Buffer to hold received data.
	//  bufSize         Size of buffer.
	//  bytesReceived   (out) Number of bytes read
	virtual void Receive(void* buf,
						 const unsigned bufSize,
						 unsigned* bytesReceived) = 0;

    //PURPOSE
    //  Upon successful completion of the http request, allows the filter
    //  to make the final determination on whether or not the task should
    //  succeed.
    //  Return false to override the successful completion
    virtual bool AllowSucceeded(class netHttpRequest* request) = 0;

    virtual void DisableHttpDump(bool /*bDisabled*/) = 0;

protected:
    //Dumps filtered content delivered to output device,
    //just like netHttpRequest does for unfiltered data.
    void DumpHttp(const char* label,
                  const char* str,
                  const unsigned strLen) const;

private:

    const netHttpRequest* m_HttpRequest;
};

//PURPOSE
//  Represents a HTTP request.
//  Use netHttpRequest to send a request and wait for a response.
//  Call Update() at least once per frame.
class netHttpRequest
{
    friend class netHttpFilter;

public:

    static const int DEFAULT_FLOAT_PRECISION    = 6;

    static const unsigned short HTTP_PORT       = 80;
    static const unsigned short HTTPS_PORT      = 443;

    // Note: Currently this needs to be at least 1044 since rlRosHttpFilter relies
    // on us buffering data until there is enough to decrypt, and it uses a 1044
    // byte block size. 
    static const unsigned DEFAULT_BOUNCE_BUFFER_MAX_LENGTH  = 1044;

	static const unsigned CONTEXT_STRING_BUF_SIZE = 48;

	// PURPOSE
	//	Extract debugging information
	struct DebugInfo
	{
		DebugInfo() { Clear(); }

		void Clear()
		{
			m_SendState = 0;
			m_RecvState = 0;
			m_InContentBytesRcvd = 0;
			m_OutContentBytesSent = 0;
			m_OutChunkBytesSent = 0;
			m_Committed = false;
			m_HttpStatusCode = 0;
			m_AbortReason = (int)NET_HTTPABORT_NONE;
			m_ChunkAllocationError = false;
			m_TcpResult = 0;
			m_TcpError = 0;
		}

		int m_SendState; // enum SendState
		int m_RecvState; // enum RecvState
		int m_HttpStatusCode;
		unsigned m_InContentBytesRcvd;
		unsigned m_OutContentBytesSent;
		unsigned m_OutChunkBytesSent;
		bool m_Committed;
		int m_AbortReason;
		bool m_ChunkAllocationError;
		int m_TcpResult;
		int m_TcpError;
	};

    netHttpRequest();
    ~netHttpRequest();

    //PURPOSE
    //  Initialize the HTTP request object.
    //PARAMS
    //  allocator   - Optional.  Used to allocate memory.  If NULL the global
    //                allocator will be used.
    //  sslCtx      - Optional. The ssl context to use for requests over ssl.
    //                May be null if ssl won't be used, or null if set later
    //                through SetSslContext
    void Init(sysMemAllocator* allocator, SSL_CTX* sslCtx);

    //PURPOSE
    //  Initialize the HTTP request object with an optional bounce buffer.
    //  The bounce buffer is used to receive data from the TCP socket
    //  associated with the request.  The larger the bounce buffer, the
    //  faster data can be downloaded.
    //  The default bounce buffer length is 256 bytes, which can result in
    //  rather slow downloads for large amounts of data.
    //PARAMS
    //  allocator   - Optional.  Used to allocate memory.  If NULL the global
    //                allocator will be used.
    //  sslCtx      - Optional. The ssl context to use for requests over ssl.
    //                May be null if ssl won't be used, or null if set later
    //                through SetSslContext
    //  bounceBuffer    - Optional. Buffer to which content is received from a
    //                    TCP socket.  If NULL the default bounce buffer will
    //                    be used.
    //  maxBounceBufferLen  - Maximum length of bounce buffer.
    //NOTES
    //  ****DO NOT deallocate the bounce buffer without also calling Init()****
    //  The netHttpRequest instance keeps a reference to the bounce buffer
    //  until the next call to Init().
    void Init(sysMemAllocator* allocator,
            SSL_CTX* sslCtx,
            void* bounceBuffer,
            const unsigned maxBounceBufferLen);

    //PURPOSE
    //  Clear the request and free memory.  Should not be called while a
    //  request is pending.
    //NOTES
    //  Options are not cleared.
    void Clear();

    //PURPOSE
    //  Sets HTTP options (see netHttpOptions).
    //PARAMS
    //  options     - Bit combination of netHttpOptions.
    void SetOptions(const unsigned options);

	//PURPOSE
	//  Sets byte ranges for a specific transfer.
	//PARAMS
	//  byteRangeLow     - Byte range "from".
	//  byteRangeHigh	 - Byte range "to"
	//NOTES
	//  Used primarily for grabbing only a certain section of a file.
	//  Going to be used to create a resumable download.
	void SetByteRange(unsigned byteRangeLow, unsigned byteRangeHigh);

    //PURPOSE
    //  Returns current options (see netHttpOptions).
    unsigned GetOptions() const;

    //PURPOSE
    //  Begins a GET request.  When ready to send the request call Commit().
    //PARAMS
    //  uri         - URI of resource, including http://.
    //  proxyAddr   - If non-null overrides global proxy address for this request.
    //                The port part of the address is optional.  If it's zero
    //                then 80 will be used for HTTP and 443 will be used for HTTPS.
    //  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
    //  responseBuffer  - Destination buffer for response.  If NULL an internal buffer
    //                will be used.
    //  contextStr  - Optional context string used in debug output.
    //  status      - Poll for completion.
    bool BeginGet(const char* uri,
                  const netSocketAddress* proxyAddr,
                  const unsigned timeoutSecs,
                  datGrowBuffer* responseBuffer,
                  const char* contextStr,
                  netHttpFilter* filter,
                  netStatus* status);

    //PURPOSE
    //  Begins a GET request.  When ready to send the request call Commit().
    //PARAMS
    //  uri         - URI of destination host, including http://.
    //  proxyAddr   - If non-null overrides global proxy address for this request.
    //                The port part of the address is optional.  If it's zero
    //                then 80 will be used for HTTP and 443 will be used for HTTPS.
    //  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
    //  responseDevice  - Device to which response is written.
    //  responseHandle  - Device handle to which response is written.
    //  contextStr  - Optional context string used in debug output.
    //  status      - Poll for completion.
    bool BeginGet(const char* uri,
                  const netSocketAddress* proxyAddr,
                  const unsigned timeoutSecs,
                  const fiDevice* responseDevice,
                  fiHandle responseHandle,
                  const char* contextStr,
                  netHttpFilter* filter,
                  netStatus* status);

    //PURPOSE
    //  Begins a GET request.  When ready to send the request call Commit().
    // The response must be polled/read with ReadBody
    //PARAMS
    //  uri         - URI of destination host, including http://.
    //  proxyAddr   - If non-null overrides global proxy address for this request.
    //                The port part of the address is optional.  If it's zero
    //                then 80 will be used for HTTP and 443 will be used for HTTPS.
    //  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
    //  contextStr  - Optional context string used in debug output.
    //  status      - Poll for completion.
    bool BeginGet(const char* uri,
                  const netSocketAddress* proxyAddr,
                  const unsigned timeoutSecs,
                  const char* contextStr,
                  netHttpFilter* filter,
                  netStatus* status);

    //PURPOSE
    //  Begins a form POST request.
    //  The content type will be application/x-www-form-urlencoded.
    //  When ready to send the request call Commit().
    //PARAMS
    //  uri         - URI of resource, including http://.
    //  proxyAddr   - If non-null overrides global proxy address for this request.
    //                The port part of the address is optional.  If it's zero
    //                then 80 will be used for HTTP and 443 will be used for HTTPS.
    //  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
    //  responseBuffer  - Destination buffer for response.  If NULL an internal buffer
    //                will be used.
    //  contextStr  - Optional context string used in debug output.
    //  status      - Poll for completion.
    bool BeginPost(const char* uri,
                   const netSocketAddress* proxyAddr,
                   const unsigned timeoutSecs,
                   datGrowBuffer* responseBuffer,
                   const char* contextStr,
                   netHttpFilter* filter,
                   netStatus* status);

    //PURPOSE
    //  Begins a form POST request.
    //  The content type will be application/x-www-form-urlencoded.
    //  When ready to send the request call Commit().
    //PARAMS
    //  uri         - URI of destination host, including http://.
    //  proxyAddr   - If non-null overrides global proxy address for this request.
    //                The port part of the address is optional.  If it's zero
    //                then 80 will be used for HTTP and 443 will be used for HTTPS.
    //  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
    //  responseDevice  - Device to which response is written.
    //  responseHandle  - Device handle to which response is written.
    //  contextStr  - Optional context string used in debug output.
    //  status      - Poll for completion.
    bool BeginPost(const char* uri,
                   const netSocketAddress* proxyAddr,
                   const unsigned timeoutSecs,
                   const fiDevice* responseDevice,
                   fiHandle responseHandle,
                   const char* contextStr,
                   netHttpFilter* filter,
                   netStatus* status);

    //PURPOSE
    //  Begins a form POST request.
    //  The content type will be application/x-www-form-urlencoded.
    //  When ready to send the request call Commit().
    //  The response must be polled/read with ReadBody
    //PARAMS
    //  uri         - URI of destination host, including http://.
    //  proxyAddr   - If non-null overrides global proxy address for this request.
    //                The port part of the address is optional.  If it's zero
    //                then 80 will be used for HTTP and 443 will be used for HTTPS.
    //  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
    //  contextStr  - Optional context string used in debug output.
    //  status      - Poll for completion.
    bool BeginPost(const char* uri,
                   const netSocketAddress* proxyAddr,
                   const unsigned timeoutSecs,
                   const char* contextStr,
                   netHttpFilter* filter,
                   netStatus* status);

	//PURPOSE
	//  Begins a form PUT request.
	//  The content type will be application/x-www-form-urlencoded.
	//  When ready to send the request call Commit().
	//PARAMS
	//  uri         - URI of resource, including http://.
	//  proxyAddr   - If non-null overrides global proxy address for this request.
	//                The port part of the address is optional.  If it's zero
	//                then 80 will be used for HTTP and 443 will be used for HTTPS.
	//  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
	//  responseBuffer  - Destination buffer for response.  If NULL an internal buffer
	//                will be used.
	//  contextStr  - Optional context string used in debug output.
	//  status      - Poll for completion.
	bool BeginPut(const char* uri,
					const netSocketAddress* proxyAddr,
					const unsigned timeoutSecs,
					datGrowBuffer* responseBuffer,
					const char* contextStr,
					netHttpFilter* filter,
					netStatus* status);

	//PURPOSE
	//  Begins a form PUT request.
	//  The content type will be application/x-www-form-urlencoded.
	//  When ready to send the request call Commit().
	//PARAMS
	//  uri         - URI of destination host, including http://.
	//  proxyAddr   - If non-null overrides global proxy address for this request.
	//                The port part of the address is optional.  If it's zero
	//                then 80 will be used for HTTP and 443 will be used for HTTPS.
	//  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
	//  responseDevice  - Device to which response is written.
	//  responseHandle  - Device handle to which response is written.
	//  contextStr  - Optional context string used in debug output.
	//  status      - Poll for completion.
	bool BeginPut(const char* uri,
					const netSocketAddress* proxyAddr,
					const unsigned timeoutSecs,
					const fiDevice* responseDevice,
					fiHandle responseHandle,
					const char* contextStr,
					netHttpFilter* filter,
					netStatus* status);

	//PURPOSE
	//  Begins a form PUT request.
	//  The content type will be application/x-www-form-urlencoded.
	//  When ready to send the request call Commit().
	//  The response must be polled/read with ReadBody
	//PARAMS
	//  uri         - URI of destination host, including http://.
	//  proxyAddr   - If non-null overrides global proxy address for this request.
	//                The port part of the address is optional.  If it's zero
	//                then 80 will be used for HTTP and 443 will be used for HTTPS.
	//  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
	//  contextStr  - Optional context string used in debug output.
	//  status      - Poll for completion.
	bool BeginPut(const char* uri,
					const netSocketAddress* proxyAddr,
					const unsigned timeoutSecs,
					const char* contextStr,
					netHttpFilter* filter,
					netStatus* status);

    //PURPOSE
    //  Begins a HTTP DELETE request.
    //  When ready to send the request call Commit().
    //PARAMS
    //  uri         - URI of resource, including http://.
    //  proxyAddr   - If non-null overrides global proxy address for this request.
    //                The port part of the address is optional.  If it's zero
    //                then 80 will be used for HTTP and 443 will be used for HTTPS.
    //  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
    //  responseBuffer  - Destination buffer for response.  If NULL an internal buffer
    //                will be used.
    //  contextStr  - Optional context string used in debug output.
    //  status      - Poll for completion.
    bool BeginDelete(const char* uri,
                     const netSocketAddress* proxyAddr,
                     const unsigned timeoutSecs,
                     datGrowBuffer* responseBuffer,
                     const char* contextStr,
                     netHttpFilter* filter,
                     netStatus* status);

    //PURPOSE
    //  Begins a HTTP DELETE request.
    //  When ready to send the request call Commit().
    //PARAMS
    //  uri         - URI of destination host, including http://.
    //  proxyAddr   - If non-null overrides global proxy address for this request.
    //                The port part of the address is optional.  If it's zero
    //                then 80 will be used for HTTP and 443 will be used for HTTPS.
    //  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
    //  responseDevice  - Device to which response is written.
    //  responseHandle  - Device handle to which response is written.
    //  contextStr  - Optional context string used in debug output.
    //  status      - Poll for completion.
    bool BeginDelete(const char* uri,
                     const netSocketAddress* proxyAddr,
                     const unsigned timeoutSecs,
                     const fiDevice* responseDevice,
                     fiHandle responseHandle,
                     const char* contextStr,
                     netHttpFilter* filter,
                     netStatus* status);

	//PURPOSE
	//  Begins a HTTP HEAD request.
	//  When ready to send the request call Commit().
	//PARAMS
	//  uri         - URI of destination host, including http://.
	//  proxyAddr   - If non-null overrides global proxy address for this request.
	//                The port part of the address is optional.  If it's zero
	//                then 80 will be used for HTTP and 443 will be used for HTTPS.
	//  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
	//  contextStr  - Optional context string used in debug output.
	//  status      - Poll for completion.
	bool BeginHead(const char* uri,
				const netSocketAddress* proxyAddr,
				const unsigned timeoutSecs,
				const char* contextStr,
				netStatus* status);

    //PURPOSE
	//  Begins a HTTP HEAD request using the specified filter.
	//  When ready to send the request call Commit().
	//PARAMS
	//  uri         - URI of destination host, including http://.
	//  proxyAddr   - If non-null overrides global proxy address for this request.
	//                The port part of the address is optional.  If it's zero
	//                then 80 will be used for HTTP and 443 will be used for HTTPS.
	//  timeoutSecs - Timeout in seconds.  If zero use the default timeout.
	//  contextStr  - Optional context string used in debug output.
    //  filter      - Optional filter to use to filter the request/response
	//  status      - Poll for completion.
    bool BeginHead(const char* uri,
				const netSocketAddress* proxyAddr,
				const unsigned timeoutSecs,
				const char* contextStr,
                netHttpFilter* filter,
				netStatus* status);

    //PURPOSE
    //  Returns the HTTP verb for the current request.
    netHttpVerb GetHttpVerb() const;

    //PURPOSE
    //  Returns the textual HTTP verb for the current request
    const char* GetHttpVerbString() const;

    //PURPOSE
    //  Commits (sends) the request.
    bool Commit();

    //PURPOSE
    //  Returns true if we have a proxy address for the current request.
    bool HaveProxyAddress() const;

	//PURPOSE
	//  Returns the proxy adddress for the current request (if any).
	netSocketAddress GetProxyAddress() const;

	//PURPOSE
	//  If this is set to true, the request will ignore the proxy.
	void SetIgnoreProxy(bool ignoreProxy);

	//PURPOSE
	//  Returns true if we will create an HTTPS tunnel for this request.
	bool UseHttpsTunnel() const;

    //PURPOSE
    //  Returns true if request has specified header.
    bool HasRequestHeader(const char* name) const;

	//PURPOSE
	//  Removes a request header, e.g. Keep-Alive: 115.
	//NOTES
	//  Attempts to call this after adding data to the content section
	//  of the request, or after calling Commit() will fail.
	bool RemoveRequestHeader(const char* name);

    //PURPOSE
    //  Adds request headers.
    //PARAMS
    //  headers - NULL terminated string of HTTP headers, each
    //            header separated by CRLF.
    //NOTES
    //  The string must be in HTTP header format.
    //  Example:
    //      {header-name}: {header-value}CRLF{header-name}: {header-value}CRLF{header-name}: {header-value}CRLF
    bool AddRequestHeaders(const char* headers);

    //PURPOSE
    //  Adds a request header, e.g. Keep-Alive: 115.
    //NOTES
    //  Attempts to call this after adding data to the content section
    //  of the request, or after calling Commit() will fail.
    //
    //  A "Content-Length" header is automatically added to the request if
    //  any AddFormParam() or AppendContent() function is called.
    bool AddRequestHeaderValue(const char* name,
                                const char* value);

	//PURPOSE
	//  Adds a request header, e.g. Keep-Alive: 115.
	//NOTES
	//  Attempts to call this after adding data to the content section
	//  of the request, or after calling Commit() will fail.
	//
	//  A "Content-Length" header is automatically added to the request if
	//  any AddFormParam() or AppendContent() function is called.
	bool AddRequestHeaderValue(const char* name,
							   const char* value,
							   const unsigned valLen);
		
    //PURPOSE
    //  Add basic authentication credentials to HTTP header.
    //  username    - Plaintext username to be used for basic authentication.
    //  password    - Plaintext password to be used for basic authentication.
    //NOTES
    //  Attempts to call this after adding data to the content section
    //  of the request, or after calling Commit() will fail.
    bool AddBasicAuth(const char* username,
                        const char* password);
						
    //PURPOSE
    //  Appends to the user agent string.
    bool AppendUserAgentString(const char* userAgentString);

    //PURPOSE
    //  Adds a parameter to the form request.
    //PARAMS
    //  name    - Name of the parameter.
    //  value   - String
    //  valLen  - Number of chars in the string
    //NOTES
    //  It value is NULL then "<name>=" is appended to the request.
    //  The value can then be built up using AppendContent().
    //  This is useful for streaming out content.
    //
    //  The string is URL-encoded prior to writing.
    //  A "Content-Length" header is automatically added to the request.
    bool AddFormParam(const char* name, const char* value, const unsigned valLen);

    //PURPOSE
    //  Adds a parameter to the query string.
    //PARAMS
    //  name    - Name of the parameter.
    //  value   - String
    //  valLen  - Number of chars in the string
    //NOTES
    //  The string is URL-encoded prior to writing.
    bool AddQueryParam(const char* name, const char* value, const unsigned valLen);

    //PURPOSE
    //  Adds a parameter to the form request.
    //PARAMS
    //  name    - Name of the parameter.
    //  value   - Optional: Null-terminated string
    //NOTES
    //  It value is NULL then "<name>=" is appended to the request.
    //  The value can then be built up using AppendContent().
    //  This is useful for streaming out content.
    //
    //  The string is URL-encoded prior to writing.
    //  A "Content-Length" header is automatically added to the request.
    bool AddFormParam(const char* name, const char* value);

    //PURPOSE
    //  Adds an int parameter to the form request.
    //PARAMS
    //  name    - Name of the parameter.
    //  value   - Parameter value.
    //NOTES
    //  A "Content-Length" header is automatically added to the request.
    bool AddIntFormParam(const char* name, const s64 value);

    //PURPOSE
    //  Adds an unsigned parameter to the form request.
    //PARAMS
    //  name    - Name of the parameter.
    //  value   - Parameter value.
    //NOTES
    //  A "Content-Length" header is automatically added to the request.
    bool AddUnsFormParam(const char* name, const u64 value);

    //PURPOSE
    //  Adds a double param to the form request.
    //PARAMS
    //  name    - Name of the parameter.
    //  value   - Parameter value.
    //NOTES
    //  A "Content-Length" header is automatically added to the request.
    bool AddDoubleFormParam(const char* name,
                        const double value,
                        const int precision = DEFAULT_FLOAT_PRECISION);

    //PURPOSE
    //  Adds a binary param as a base64-encoded string to the form request.
    //PARAMS
    //  name    - Name of the parameter.
    //  value   - Binary data.
    //  len     - Number of bytes of binary data.
    //NOTES
    //  A "Content-Length" header is automatically added to the request.
    bool AddBinaryFormParam(const char* name,
                            const void* value,
                            const unsigned len);

    //PURPOSE
    //  Adds a comma separated list of values to the form request.
    //  Call BeginListFormParam to begin the list, passing the name of the list.
    //  For each value in the list call AddListFormParam*().
    //  The list is ended when another named parameter is added (e.g. AddStringFormParam())
    //  or when the request is Committed.
    //NOTES
    //  A "Content-Length" header is automatically added to the request.
    bool BeginListFormParam(const char* name);
    bool AddListFormParamValue(const char* value);
    bool AddListFormParamIntValue(const s64 value);
    bool AddListFormParamUnsValue(const u64 value);
    bool AddListFormParamDoubleValue(const double value,
                                const int precision = DEFAULT_FLOAT_PRECISION);

    //PURPOSE
    //  Returns the number of form parameters added.
    unsigned GetFormParameterCount() const;

    //PURPOSE
    //  Returns the number of query parameters added.
    unsigned GetQueryParameterCount() const;

    //PURPOSE
    //  URL-encodes then appends data to the content section of the request.
    //PARAMS
    //  data    - Data buffer.
    //  len     - Length of buffer.
    //NOTES
    //  The data is URL-encoded prior to writing.
    //  A "Content-Length" header is automatically added to the request.
    bool AppendContentUrlEncoded(const void* data,
                                const unsigned len);

    //PURPOSE
    //  Appends data to the content section of the request.
    //PARAMS
    //  data    - Data buffer.
    //  len     - Length of buffer.
    //NOTES
    //  The data is *NOT* URL-encoded prior to writing.
    //  A "Content-Length" header is automatically added to the request.
    bool AppendContent(const void* data,
                        const unsigned len);

    //PURPOSE
    //  Appends data to the content section of the request.
    //  The data is not copied so the data buffer must not be freed until
    //  the HTTP request completes.
    //PARAMS
    //  data    - Data buffer.
    //  len     - Length of buffer.
    //NOTES
    //  The data is *NOT* URL-encoded prior to writing.
    //  A "Content-Length" header is automatically added to the request.
    //********
    //  Data queued by this function WILL NOT be filtered through m_Filter->FilterRequest.
    //  IOW, it WILL NOT BE ENCRYPTED.
    //  We assume it's been pre-filtered.
    bool AppendContentZeroCopy(const void* data,
                                const unsigned len);

    //PURPOSE
    //  Retrieves the URI for the request.
    const char* GetUri() const;

    //PURPOSE
    //  Returns the host part of the URI.
    const char* GetHost() const;

    //PURPOSE
    //  Returns the path part of the URI.
    const char* GetPath() const;

    //PURPOSE
    //  Returns true while the request is pending.
    bool Pending() const;

    //PURPOSE
    //  Returns true if the status code is 2xx.
    bool Succeeded() const;

    //PURPOSE
    //  Cancels a pending request.
    void Cancel(netHttpStatusCodes statusCode = NET_HTTPSTATUS_BAD_REQUEST);

    //PURPOSE
    //  Updates a pending request.  Should be called once per frame.
    void Update();

    //PURPOSE
    //  Return the response status code.
    int GetStatusCode() const;

    //PURPOSE
    //  Return the response status code in string form.
    bool GetStatusCodeString(char* buf,
                                const unsigned bufLen) const;

    //PURPOSE
    //  Returns true if we've received a complete response header.
    bool HaveResponseHeader() const;

    //PURPOSE
    //  Retrieves the HTTP response header.
    //  Returns NULL until a complete header has been received.
    const char* GetResponseHeader() const;

    //PURPOSE
    //  Retrieves the value of an item in the HTTP response header.
    //PARAMS
    //  fieldName   - Name of HTTP header field.
    //  value       - Destination buffer for header value.
    //                Pass NULL to get the length of the header value.
    //  valLen      - IN: Max chars in value buffer.
    //                OUT: Length of header value.
    //NOTES
    //  If a complete header has not been received the function returns
    //  false and sets valLen to zero.
    //  If the value buffer is too short to contain the header value or
    //  the value buffer is NULL the function returns false and sets valLen
    //  to the required length.
    bool GetResponseHeaderValue(const char* fieldName,
                                char* value,
                                unsigned* valLen) const;

    //PURPOSE
    //  Parses a HTTP header for the requested cookie and returns its value.
    //NOTE
    //  This is provided in case the header contains multiple cookie values,
    //  in which case GetResponseHeaderValue would only return the first.
    //  If we only got the first one, adding cookies to requests might break
    //  previously deployed code.
    static bool GetCookieValue(const char* header,
                               const char* cookieName,
                               char* value,
                               unsigned* valLen);

    //PURPOSE
    //  Parses a HTTP header for the requested field and returns it in value.
    //NOTES
    //  Callers can determine the length of the field value by first calling
    //  the method with a NULL value, allocating their buffer, and then calling
    //  again with the allocated value buffer.
    static bool GetHeaderValue(const char* header,
                               const char* fieldName,
                               char* value,
                               unsigned* valLen);

    //PURPOSE
    //  Returns the value in the Content-Length response header.
	//NOTES
	//  There is no guarantee that a response header will provide a Content-Length.
	//  Different platforms may behave differently with regards to Content-Length
	//  (example: Xbox One will often use chunked encoding and omit this header).
	//  Callers can determine the length of the response using
	//  GetResponseContentNumBytesReceived() after the request has succeeded.
    unsigned GetResponseContentLength() const;

    //PURPOSE
    //  Returns the number of content bytes received so far.
    unsigned GetResponseContentNumBytesReceived() const;

    //PURPOSE
    //  Sets the SSL context to use for this request
    void SetSslContext(WOLFSSL_CTX* sslCtx);

	// PURPOSE
	//	Extracts debug information from the request.
	void GetDebugInfo(DebugInfo& info);

	//PURPOSE
   //  Returns true if the proxy has been set with an arg like -nethttpproxy
   //  Returns false if the proxy has been set with an arg like -fiddler
   //  which 'only' sets a proxy as side effect for that command
   //  Returns false if no proxy has been set.
	static bool IsUsingExplicitProxyAddress();

    //PURPOSE
    //  Returns true if the string requires URL encoding.
    //  str     - The unencoded string.
    //  len     - Number of characters to encode.
    //  excludedChars   - Null terminated string comprising characters
    //                    that are excluded from encoding.
    //                    Can be NULL.
    static bool NeedsUrlEncoding(const char* str,
                                const unsigned len,
                                const char* excludedChars);

    //PURPOSE
    //  Returns the length of the string if it were to be URL encoded.
    //PARAMS
    //  str     - The unencoded string.
    //  len     - Number of characters to encode.
    //  excludedChars   - Null terminated string comprising characters
    //                    that are excluded from encoding.
    //                    Can be NULL.
    static unsigned GetUrlEncodedLength(const char* str,
                                        const unsigned len,
                                        const char* excludedChars);

    //PURPOSE
    //  URL encodes a character.
    //PARAMS
    //  dst     - Destination string
    //  dstLen  - In: Length of destination buffer.
    //            Out: Length of encoded string.
    //  c       - Source character
    //  excludedChars   - Null terminated string comprising characters
    //                    that are excluded from encoding.
    //                    Can be NULL.
    //RETURNS
    //  True on success.
    static bool UrlEncode(char* dst,
                            unsigned* dstLen,
                            const int c,
                            const char* excludedChars);

    //PURPOSE
    //  URL encodes a string.
    //PARAMS
    //  dst         - Destination buffer
    //  dstLen      - In: Length of destination buffer.
    //                Out: Length of encoded string.
    //  src         - Source string
    //  srcLen      - Length of source string
    //  numConsumed - Number of source chars consumed.
    //  excludedChars   - Null terminated string comprising characters
    //                    that are excluded from encoding.
    //                    Can be NULL.
    //RETURNS
    //  True on success.
    static bool UrlEncode(char* dst,
                            unsigned* dstLen,
                            const char* src,
                            const unsigned srcLen,
                            unsigned* numConsumed,
                            const char* excludedChars);

    //PURPOSE
    //  Decodes a URL encoded string.
    //PARAMS
    //  dst         - Destination buffer
    //  dstLen      - In: Length of destination buffer.
    //                Out: Length of encoded string.
    //  src         - Source string
    //  srcLen      - Length of source string
    //  numConsumed - Number of source chars consumed.
    //RETURNS
    //  True on success.
    static bool UrlDecode(char* dst,
                            unsigned* dstLen,
                            const char* src,
                            const unsigned srcLen,
                            unsigned* numConsumed);

    //PURPOSE
    //  Creates a date string in RFC1123 format:
    //      Sun, 06 Nov 1994 08:49:37 GMT
    //PARAMS
    //  posixTime   - Time in posix format
    //  buf         - Destination buffer
    //  sizeofBuf   - Size of destination buffer
    static const char* MakeDateString(const u64 posixTime,
                                        char* buf,
                                        const unsigned sizeofBuf);

    template<int SIZE>
    static const char* MakeDateString(const u64 posixTime,
                                        char (&buf)[SIZE])
    {
        return MakeDateString(posixTime, buf, SIZE);
    }

    //PURPOSE
    //  Parses a date string and returns an integer representing Posix time.
    //  Assumes the date string represents the date/time in GMT/UTC.
    //PARAMS
    //  dateString  - Date in RFC1123, RFC850, or asctime format.
    //EXAMPLE
    //  RFC1123: Sun, 06 Nov 1994 08:49:37 GMT
    //  RFC850:  Sunday, 06-Nov-94 08:49:37 GMT
    //  asctime: Sun Nov  6 08:49:37 1994
    static u64 ParseDateString(const char* dateString);
	
	//PURPOSE
	//  Parses a date string and returns an integer representing Posix time.
	//  Assumes the date string represents the date/time in GMT/UTC.
	//PARAMS
	//  dateString  - Date in RFC3339
	//EXAMPLE
	//  RFC3339: 
	static u64 ParseDateStringRFC3339(const char* dateString);

#if __ASSERT
    //For unit testing
    static int TestUrlEncoding();
#endif

#if !__FINAL
	//PURPOSE
	//  Allows setting of simulated fail rate for http operations
	static void SetSimulatedFailPct(const char* failRate);
#endif

private:

	// the request id is added to the user-provided context string
	static const unsigned INTERNAL_CONTEXT_STRING_BUF_SIZE = CONTEXT_STRING_BUF_SIZE + 16;

    enum TransferEncoding
    {
        TRANSFER_ENCODING_NORMAL,
        TRANSFER_ENCODING_CHUNKED,
    };

    enum UriScheme
    {
        URISCHEME_INVALID,
        URISCHEME_HTTP,
        URISCHEME_HTTPS,
    };

    static const char* HTTP_VERBS[NET_HTTP_VERBS_COUNT];

    struct Chunk
    {
        datGrowBuffer m_Data;
        netTcpAsyncOp m_TcpOp;
		netStatus m_Status; // when not using m_TcpOp, such as when IXHR2 is used.
        inlist_node<Chunk> m_ListLink;
    };

    //Clear only the data used to send the request, e.g. the URI,
    //query string, etc.
    void ClearRequest();

    bool Begin(const netHttpVerb verb,
               const char* uri,
               const netSocketAddress* proxyAddr,
               const unsigned timeoutSecs,
               const fiDevice* responseDevice,
               const fiHandle responseHandle,
               const char* contextStr,
               netHttpFilter* filter,
               netStatus* status);

    //Build the HTTP header and start resolving the host.
    bool StartRequest(const char* uri);

    //Called in response to a 3xx status code.
    //Can only be used for GET requests.
    bool Redirect(const char* uri);

    //PURPOSE
    //  Resolve the host in the URL of the request.
    bool ResolveHost(netStatus* status);

    //PURPOSE
    //  Connect to the destination host/proxy.
    bool Connect();

    //PURPOSE
    //  Parse the URI into it's components:
    //      HTTP scheme
    //      Host
    //      Port
    //      Path
    //      Query string
    bool ParseUri(const char* uri);

    //PURPOSE
    //  Create a HTTP header.
    bool MakeHeader(datGrowBuffer* header);

    bool QueueRawData(const void* data,
                    const unsigned dataLen);

    bool QueueRawDataZeroCopy(const void* data,
                            const unsigned dataLen);

    bool QueueUrlEncodedData(const void* data,
                            const unsigned dataLen);

    //PURPOSE
    //  Send the HTTP header.
    bool SendHeader();

    //PURPOSE
    //  Returns true if Commit() has been called.
    bool IsCommitted() const;

    //PURPOSE
    //  Processes outbound chunks.
	void OnFinishedChunk();
    void ProcessOutboundChunks();

    //PURPOSE
    //  Send a chunk.
    bool SendChunk(Chunk* chunk, const bool termChunk);

    //PURPOSE
    //  Processes bounce content contained in the specified buffer. This means
    //  filtering it if there is a filter, or just sending it to the output
    //  device otherwise.
    bool ProcessBounceContent(u8* buffer,
                              const unsigned receivedDataLen,
                              fiHandle responseHandle,
                              const fiDevice& responseDevice,
                              const bool allContentReceived,
                              unsigned* const amountConsumed,
                              bool* allContentConsumed = NULL);

#if !__NO_OUTPUT
	bool IsBinaryResponse();

	//PURPOSE
	//  Return a context string that gets passed to lower-level systems
	//  to make logging more useful.
	const char* GetContextString(char* ctx, const unsigned bufSize) const;
#endif

public:
    //PURPOSE
    //  Polls the response body, which can be used for requests that don't use a response device
    //  to which to write the response to. This accepts both a temporary buffer used to read the
    //  response and an output buffer to which to write the response to. The larger the read buffer,
    //  the more data can be processed and received in a single call.
    //PARAMS
    //  readBuffer          - Used for temporary storage when processing the response stream
    //  readBufferSize      - Size of the read buffer
    //  writeBuffer         - Buffer to write the response to
    //  writerBufferSize    - Size of the write buffer.
    //  amountWritten       - (out) holds the amount written to the write buffer
    bool ReadBody(u8* readBuffer, const unsigned readBufferSize, u8* writeBuffer, const unsigned writeBufferSize, unsigned* const amountWritten);

    //PURPOSE
    //  Overload of ReadBody that uses an fiDevice instead of a buffer
    bool ReadBody(u8* readBuffer, const unsigned readBufferSize, fiHandle responseHandle, const fiDevice& responseDevice);

	//PURPOSE
	// Access the number of bytes sent, set immediately after data is queued.
	unsigned  GetOutContentBytesSent() const {return m_OutContentBytesSent;}

	// PURPOSE
	//	Access the number of bytes sent, set after a chunk is uploaded.
	unsigned  GetOutChunksBytesSent() const {return m_OutChunksBytesSent; }

	//Returns true if the request can run
	bool CanRun() const;

    // PURPOSE
    //  Sets if the request should be url encoded
    void SetUrlEncode(bool bUrlEncode);

private:
    //PURPOSE
    //  Mainly for Xhttp, which calls ReadBodyOnce multiple times from ReadBody
    //  out_amountRecvdFromSocket receives the amount received from the socket (i.e. xhttp)
    //  out_amountRecvd receives the total amount received (i.e. from socket and our bounce buffer)
    //  out_amountConsumed receives the total amount of the received data that was consumed
    bool ReadBodyOnce(u8* buffer, const unsigned bufferSize, unsigned* const out_amountRecvd, unsigned* const out_amountRecvdFromSocket, unsigned* const out_amountConsumed, fiHandle responseHandle, const fiDevice& responseDevice);

    //PURPOSE
    //  Receive the response header.
    bool ReceiveHeader();

    //PURPOSE
    //  Receive the response body into the specified buffer without actually consuming it. 
    //  Writes the amount received into amountReceived. 
    //  Must also pass in whether or not received data from the body is being buffered elsewhere
    //  (this controls how socket recv errors are handled)
    bool ReceiveBody(u8* buffer, const unsigned bufferSize, const bool haveBufferedBody, unsigned* const out_amountReceived);

    //PURPOSE
    //  Consumes body content from the specified buffer, calling ProcessBodyContent
    //  as necessary to handle processing it.
    bool ConsumeBody(u8* buffer, const unsigned bufferSize, unsigned* const out_amountConsumed, fiHandle responseHandle, const fiDevice& responseDevice);

    //PURPOSE
    //  Closes the IO device to which we're writing response data.
    void CloseResponseDevice();

    void SetSucceeded();

    //PURPOSE
    //  Abort the current request
    void Abort(const netStatus::StatusCode statusCode, const netHttpStatusCodes httpStatusCode, const netHttpAbortReason abortReason);

    //PURPOSE
    //  Trims any junk off the front of a header.
    void TrimJunk(atStringBuilder* header) const;

    void DumpHttp(const char* label,
                const char* str,
                const unsigned strLen) const;

    //PURPOSE
    //  Parses a HTTP header for the requested field, and returns true if
    //  it exists.
    static bool HasHeaderValue(const char* header,
                                const char* fieldName);

    Chunk* NewChunk();
    void DeleteChunk(Chunk* chunk);
	void DeleteInFlightChunk();

#if RSG_DURANGO
	void DeleteAllInFlightChunks();
#endif

    char* Allocate(const unsigned numChars) const;

    void Free(char* chars) const;

    //Links/unlinks from a linked list of requests
    void Enqueue();
    void Dequeue();

	//PURPOSE
	//  Clears/frees the cached Uri
	void ClearCachedEncodedUri();

    enum SendState
    {
        SENDSTATE_NONE,
        SENDSTATE_WAIT_UNTIL_READY,
        SENDSTATE_RESOLVE_HOST,
        SENDSTATE_RESOLVING_HOST,
        SENDSTATE_CONNECT,
        SENDSTATE_CONNECTING,
#if RSG_DURANGO
		SENDSTATE_GET_AUTH_TOKEN,
		SENDSTATE_GETTING_AUTH_TOKEN,
#endif
		SENDSTATE_SEND_HEADER,
        SENDSTATE_SENDING_CONTENT,
        SENDSTATE_SENDING_TERM_CHUNK,
        SENDSTATE_SUCCEEDED,
        SENDSTATE_ERROR
    };

    enum RecvState
    {
        RECVSTATE_NONE,
        RECVSTATE_RECEIVING_HEADER,
        RECVSTATE_RECEIVED_HEADER,
        RECVSTATE_RECEIVING_NORMAL_CONTENT,
        RECVSTATE_RECEIVING_CHUNKED_CONTENT,
        RECVSTATE_FINISH_CONSUMING_CONTENT,
        RECVSTATE_SUCCEEDED,
        RECVSTATE_ERROR
    };

    //Prevent copying
    netHttpRequest(const netHttpRequest&);
    netHttpRequest& operator=(const netHttpRequest&);

    Chunk* m_CurChunk;

    typedef inlist<Chunk, &Chunk::m_ListLink> ChunkList;

    ChunkList m_QueuedChunks;
    Chunk* m_InFlightChunk;
#if RSG_DURANGO
	ChunkList m_InFlightChunks;
#endif

    datGrowBuffer m_InBuf;
    atStringBuilder m_InHeader;
    atStringBuilder m_OutHeader;

    netHttpFilter* m_Filter;

    fiHandle m_ResponseHandle;
    const fiDevice* m_ResponseDevice;

    char m_DefaultBounceBuf[DEFAULT_BOUNCE_BUFFER_MAX_LENGTH];
    char* m_BounceBuf;
    unsigned m_BounceBufMaxLen;
    unsigned m_BounceBufLen;        //Number of bytes currently in the bounce buffer

    unsigned m_RequestId;

    netHttpVerb m_HttpVerb;         //POST, GET, etc.
    mutable int m_HttpStatusCode;   //200, 404, etc.
    UriScheme m_UriScheme;          //URI scheme (https or http)
    char* m_RawUri;                 //URI buffer to hold host, path, query string
    const char* m_RawHost;          //Raw destination host - points to substring of m_RawUri
    const char* m_RawPath;          //Raw destination path - points to substring of m_RawUri
    mutable char* m_EncodedUri;     //URL encoded URI
    atStringBuilder m_QueryString;  //Query string
    atStringBuilder m_UserAgentString;  //User agent string
    unsigned m_InContentLen;        //Length of response content as specified in the Content-Length header
    unsigned m_InContentBytesRcvd;  //Inbound content bytes received so far
    unsigned m_OutContentBytesSent; //Outbound content bytes sent so far
	unsigned m_OutChunksBytesSent; //Byte total of uploaded chunks so far
    netSocketAddress m_ProxyAddr;   //Proxy address used for request
	bool m_IgnoreProxy;
    netTimeout m_Timeout;           //Used to generate timeouts
    int m_RedirectCount;            //Number of redirects for this request
    int m_Skt;                      //Network socket
    SendState m_SendState;
    RecvState m_RecvState;
    sysMemAllocator* m_Allocator;
    unsigned m_Options;

	unsigned m_ByteRangeLow;			// Low byte range to download;
	unsigned m_ByteRangeHigh;			// High byte range to download

    unsigned m_NumFormParams;       //Number of parameters written
    unsigned m_NumQueryParams;      //Number of query string parameters written

    int m_NumListParamValues;       //Number of values added to a comma separated list parameter.

    netIpAddress m_HostIp;          //Destination host IP
    unsigned short m_HostPort;      //Destination host port in host byte order
    netTcpAsyncOp m_ConnectTcpOp;

	// Diagnostics
	netHttpAbortReason m_AbortReason;
	bool m_ChunkAllocationError;
	netTcpResult m_TcpOpResult;
	int m_TcpOpError;

    TransferEncoding m_OutboundTransferEncoding;

    netStatus* m_Status;
    netStatus m_MyStatus;
    netStatus m_HostResolutionStatus;

    //For managing a linked list of requests.
    netHttpRequest* m_Next;
    netHttpRequest* m_Prev;

    WOLFSSL_CTX* m_SslCtx;

#if RSG_DURANGO
	netXblHttpRequest m_XblHttpRequest;
	bool m_AllContentReceived;
	netStatus m_AuthTokenStatus;
	netXblHttp::netXblRelyingPartyToken m_AuthToken;
#endif

#if !__FINAL
    //Used when -nohttpsometimes is present.
    //Represents the percentage of time we should simulate failures.
    int m_SimulateFailPct;
#endif

#if !__NO_OUTPUT
    char m_ContextStr[INTERNAL_CONTEXT_STRING_BUF_SIZE];
#endif

    bool m_Initialized          : 1;
    bool m_Committed            : 1;
    bool m_OwnResponseDevice    : 1;
    bool m_bUrlEncode           : 1;

#if RSG_DURANGO
	//If true then use rlXblHttp on Durango
	bool m_UseXblHttp           : 1;
#endif
};

class netHttp
{
public:
	// PURPOSE
	// If true, all requests must use HTTPS/SSL. Non-HTTPS requests will be rejected.
    static void SetRequireHttps(const bool requireHttps);
    static bool AllowNonHttpsRequest();
};

#if ENABLE_TCP_PROXY
struct netHttpProxy
{
public:
	static const unsigned MAX_HOST_URL_LEN = 128;
	static const unsigned MAX_PROXIES = 8;
	static const unsigned MAX_WHITELISTS = 8;

	netHttpProxy();

	//PURPOSE
	//  Set the proxy data
	bool Set(const char* urlWildcard, const char* proxy);

	//PURPOSE
	//  True if any of the wildcard proxy redirections matches the url
	//  url can be null in which case a match means the '*' has been used.
	bool MatchesUrl(const char* url) const;

    //PURPOSE
    //  Parses the proxies from the args
    static void InitProxies();

    //PURPOSE
    //  Retrieve a proxy for the specified url.
    // when url is null a match means the '*' has been used.
    static bool GetProxyAddress(const char* url, const char*& addrStr);
    static bool GetProxyAddress(const char* url, netSocketAddress& addr);

    //PURPOSE
    //  Whitelist as in we always ignore the proxy for these
    static void AddProxyWhitelist(const char* urlWildcard);

#if ENABLE_TCP_PROXY_ARGS
	//PURPOSE
	//  Adds a proxy mapping. Does not do any check to see if it already exists
	//  The order of addition also dictates the order of lookup.
	//  So if for example the first element is urlWildcard='*' then this will win
	//  over any later added value.
	static void AddProxyAddress(const char* urlWildcard, const char* proxyAddress);
#endif //ENABLE_TCP_PROXY_ARGS

public:
	char m_urlWildcard[MAX_HOST_URL_LEN];
	char m_proxy[netSocketAddress::MAX_STRING_BUF_SIZE];

#if ENABLE_TCP_PROXY_ARGS
	static netHttpProxy sm_proxies[MAX_PROXIES];
	static unsigned sm_numProxies;
#endif

	static netHttpProxy sm_whitelist[MAX_WHITELISTS];
	static unsigned sm_numWhitelists;
};
#endif //ENABLE_TCP_PROXY

} // namespace rage

#endif // NET_HTTP_H 
