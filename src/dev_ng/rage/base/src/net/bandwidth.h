// 
// net/bandwidth.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_BANDWIDTH_H
#define NET_BANDWIDTH_H

#include "atl/inlist.h"

#include "net.h"

namespace rage
{

class netSocketAddress;
class netSocket;
class sysMemAllocator;

//PURPOSE
//  Abstract interface to record information about inbound and outbound packets.
//  This interface can be used in many contexts.  For netSocket it is used to
//  record all packets flowing through the Send() and Receive() interfaces to,
//  for example, calculate average outbound/inbound bandwidth.
class netPacketRecorder
{
    friend class netSocket;
public:

    netPacketRecorder();

    virtual ~netPacketRecorder();

    //PURPOSE
    //  This function can be called to sample the data currently collected
    //  by the packet recorder manually. This is useful to ensure that all recorders
    //  are sampled at the same point in time for consistent results across multiple recorders.
    //PARAMS
    //  curTimeMs   - Current time in milliseconds.
    //NOTES
    //  Don't call SampleData() for recorders registered with a socket.
    //  Socket packet recorders always use automatic sampling.
    virtual void SampleData(const unsigned curTimeMs) = 0;

    //PURPOSE
    //  This should be called on a regular interval to keep things
    //  consistent when no packets are being sent or received.
    //PARAMS
    //  curTimeMs   - Current time in milliseconds.
    //NOTES
    //  Don't call Update() for recorders registered with a socket.
    //  The socket will do this.
    virtual void Update(const unsigned curTimeMs) = 0;

    //PURPOSE
    //  Abstract function for recording outbound packets.
    //PARAMS
    //  addr        - Network address of recipient.
    //  bytes       - Packet payload.
    //  numBytes    - Number of bytes in the payload.
    virtual void RecordOutboundPacket(const netSocketAddress& addr,
                                        const void* bytes,
                                        const unsigned numBytes) = 0;

    //PURPOSE
    //  Abstract function for recording inbound packets.
    //PARAMS
    //  addr        - Network address of sender.
    //  bytes       - Packet payload.
    //  numBytes    - Number of bytes in the payload.
    virtual void RecordInboundPacket(const netSocketAddress& addr,
                                        const void* bytes,
                                        const unsigned numBytes) = 0;

    //Exposed in the public interface so any system can keep
    //an intrusive list of recorders.
    inlist_node<netPacketRecorder> m_ListLink;

protected:
    //If we're registered with a socket this will point to the socket.
    netSocket* m_Owner;
};

//PURPOSE
//  Implementation of netPacketRecorder used to calculate average
//  outbound/inbound bandwidth.
//NOTES
//  Bandwidth recorder can be used in just about any context where
//  a flow rate is desired.
//
//  Additionally, because the addr and bytes parameters are ignored,
//  the numBytes parameter does not necessarily need to be a byte
//  count.  For example, if you're more interested in the bit rate
//  then pass in the number of bits as the numBytes parameter.  If
//  you're more interested in the kilobyte rate then pass in the number
//  of kilobytes as the numBytes parameter.
class netBandwidthRecorder : public netPacketRecorder
{
public:

    enum
    {
        DEFAULT_SAMPLE_INTERVAL_MS  = 1000
    };

    netBandwidthRecorder();

    //PURPOSE
    //  This function can be called to sample the data currently collected
    //  by the packet recorder manually. This is useful to ensure that all recorders
    //  are sampled at the same point in time for consistent results across multiple recorders.
    //PARAMS
    //  curTimeMs   - Current time in milliseconds.
    //NOTES
    //  Don't call SampleData() for recorders registered with a socket.
    //  Socket packet recorders always use automatic sampling. 
    //  If you want to use manual sampling you should
    //  set the sampling interval to 0 (via SetSampleInterval())
    virtual void SampleData(const unsigned curTimeMs);

    //PURPOSE
    //  This should be called on a regular interval to keep the rate
    //  consistent when no packets are being sent or received.
    virtual void Update(const unsigned curTimeMs);

    //PURPOSE
    //  Sets the millisecond interval over which samples are collected.
    void SetSampleInterval(const unsigned interval);

    //PURPOSE
    //  Returns the millisecond interval over which samples are collected.
    unsigned GetSampleInterval() const;

    //PURPOSE
    //  Implements abstract function defined in netPacketRecorder
    //  to calculate outbound bandwidth.
    //PARAMS
    //  addr        - ignored.
    //  bytes       - ignored.
    //  numBytes    - Number of bytes.
    //NOTES
    //  If the recorder is registered with a socket it will add
    //  the size of a UDP header to each packet recorded.
    //
    //  If not registered with a socket it will simply record the
    //  size passed to it.
    //
    //  For general purpose use (i.e. in contexts other than recording
    //  outbound packets from a socket) the value passed in for numBytes
    //  can actually be any unit (e.g. instead of a byte count it could
    //  be a bit count).
    virtual void RecordOutboundPacket(const netSocketAddress& /*addr*/,
                                        const void* /*bytes*/,
                                        const unsigned numBytes);

    //PURPOSE
    //  Implements abstract function defined in netPacketRecorder
    //  to calculate inbound bandwidth.
    //PARAMS
    //  addr        - ignored.
    //  bytes       - ignored.
    //  numBytes    - Number of bytes.
    //NOTES
    //  If the recorder is registered with a socket it will add
    //  the size of a UDP header to each packet recorded.
    //
    //  If not registered with a socket it will simply record the
    //  size passed to it.
    //
    //  For general purpose use (i.e. in contexts other than recording
    //  outbound packets from a socket) the value passed in for numBytes
    //  can actually be any unit (e.g. in stead of a byte count it could
    //  be a bit count).
    virtual void RecordInboundPacket(const netSocketAddress& /*addr*/,
                                        const void* /*bytes*/,
                                        const unsigned numBytes);

    //PURPOSE
    //  Returns average number of outbound bytes per second.
    unsigned GetOutboundBandwidth() const;

    //PURPOSE
    //  Returns average number of inbound bytes per second.
    unsigned GetInboundBandwidth() const;

    //PURPOSE
    //  Returns average number of outbound packets per second.
    unsigned GetOutboundPacketFreq() const;

    //PURPOSE
    //  Returns average number of inbound packets per second.
    unsigned GetInboundPacketFreq() const;

	// PURPOSE
	//	Returns the total number of inbound/outbound accumulated bytes.
	unsigned GetOutboundAccumulatedBytes() const;
	unsigned GetInboundAccmulatedBytes() const;

private:

	void CalculateBandwidth(const unsigned curTimeMs);

    unsigned m_SampleInterval;

    unsigned m_InBw;
    unsigned m_OutBw;
    unsigned m_InPkt;
    unsigned m_OutPkt;

    unsigned m_InBwAccum;
    unsigned m_OutBwAccum;
    unsigned m_InPktAccum;
    unsigned m_OutPktAccum;

    unsigned m_LastUpdateTime;
};

enum netMessageDisposition
{
    MSGDISP_OUT_OF_BAND     = 0,
    MSGDISP_RELIABLE,
    MSGDISP_UNRELIABLE,

    MSGDISP_NUM_DISPOSITIONS
};

//PURPOSE
//  Used in netMessageHistogram
class netMessageCounter
{
public:

    unsigned m_MessageId;
    unsigned m_Count[MSGDISP_NUM_DISPOSITIONS];
    unsigned m_NumBytes[MSGDISP_NUM_DISPOSITIONS];
    unsigned m_CountAccum;
    unsigned m_NumBytesAccum;
    float m_Frequency;
    float m_KilobitsPerSec;

    unsigned TotalCount() const
    { 
        unsigned total = 0; 
        for(int i = 0; i < MSGDISP_NUM_DISPOSITIONS; ++i)
            total += m_Count[i];
        return total;
    }

    unsigned TotalNumBytes() const
    { 
        unsigned total = 0; 
        for(int i = 0; i < MSGDISP_NUM_DISPOSITIONS; ++i)
            total += m_NumBytes[i];
        return total;
    }
};

//PURPOSE
//  Records message counts categorized by message type so that
//  a histogram can be built of inbound and outbound message
//  statistics.
//NOTES
//  netMessageHistogram recorders can only parse buffers that contain
//  netBundle, so they should only be registered as channel recorders
//  (netConnectionManager::RegisterChannelPacketRecorder()), not as
//  socket recorders.
class netMessageHistogram : public netPacketRecorder
{
public:

    netMessageHistogram();

    virtual ~netMessageHistogram();

    //PURPOSE
    //  Initialize the recorder.
    //PARAMS
    //  allocator   - Used to allocate memory for the histograms.
    bool Init(sysMemAllocator* allocator);

    //PURPOSE
    //  Releases resources
    void Shutdown();

    //PURPOSE
    //  Resets message counts to zero.
    void Reset();

    //PURPOSE
    //  Required from netPacketRecorder, but does nothing in this
    //  implementation.
    virtual void SampleData(const unsigned curTimeMs);

    //PURPOSE
    //  Required from netPacketRecorder, but does nothing in this
    //  implementation.
    virtual void Update(const unsigned curTimeMs);

    //PURPOSE
    //  Implements abstract function defined in netPacketRecorder
    //  to record outbound messages.
    //PARAMS
    //  addr        - ignored.
    //  bytes       - Parsed for netMessages.
    //  numBytes    - Number of bytes.
    virtual void RecordOutboundPacket(const netSocketAddress& /*addr*/,
                                        const void* bytes,
                                        const unsigned numBytes);

    //PURPOSE
    //  Implements abstract function defined in netPacketRecorder
    //  to record inbound messages.
    //PARAMS
    //  addr        - ignored.
    //  bytes       - Parsed for netMessages.
    //  numBytes    - Number of bytes.
    virtual void RecordInboundPacket(const netSocketAddress& /*addr*/,
                                        const void* bytes,
                                        const unsigned numBytes);

    //PURPOSE
    //  Retrieves current counts for outbound messages.
    //PARAMS
    //  counters    - Receives a pointer to the array of counters.
    //RETURNS
    //  Number of counters.
    unsigned GetOutCounts(const netMessageCounter** counters) const;

    //PURPOSE
    //  Retrieves current counts for inbound messages.
    //PARAMS
    //  counters    - Receives a pointer to the array of counters.
    //RETURNS
    //  Number of counters.
    unsigned GetInCounts(const netMessageCounter** counters) const;

    //PURPOSE
    //  Builds a CSV formatted string with a row for each message type
    //  that contains the number of outbound messages, and the total
    //  size in bytes of all outbound messages of that type.
    //PARAMS
    //  buf     - Destination string buffer
    //  bufLen  - Number of chars in the destination buffer
    //RETURNS
    //  True if all counters were added to the string.
    //  False if the buffer was too small.
    bool GetOutCsv(char* buf,
                    const unsigned bufLen) const;

    //PURPOSE
    //  Builds a CSV formatted string with a row for each message type
    //  that contains the number of inbound messages, and the total
    //  size in bytes of all inbound messages of that type.
    //PARAMS
    //  buf     - Destination string buffer
    //  bufLen  - Number of chars in the destination buffer
    //RETURNS
    //  True if all counters were added to the string.
    //  False if the buffer was too small.
    bool GetInCsv(char* buf,
                    const unsigned bufLen) const;

private:

    static void RecordMessages(const void* bytes,
                                const unsigned numBytes,
                                netMessageCounter* msgCounts,
                                unsigned* numIds);

    static void ParsePayload(const void* bytes,
                            const unsigned numBytes,
                            netMessageCounter* msgCounts,
                            unsigned* numIds,
                            const netMessageDisposition msgDisp);

    static bool GetCsv(char* buf,
                        const unsigned bufLen,
                        netMessageCounter* msgCounts,
                        const unsigned numIds);

    sysMemAllocator* m_Allocator;

    netMessageCounter* m_MsgsIn;
    netMessageCounter* m_MsgsOut;

    unsigned m_NumIn;
    unsigned m_NumOut;

    //rdr2 specific - wants per second values
    unsigned m_SampleInterval;
    unsigned m_LastUpdateTime;

};

//PURPOSE
//  Holds the result for a bespoke upload and/or download bandwidth test
class netBandwidthTestResult
{
public:
    netBandwidthTestResult();
    netBandwidthTestResult(const unsigned source, const u64 testTime);

    bool IsValid() const;

public:
    u64 m_TestTime; // The posix time the test was started
    s64 m_UploadBps; // bits per second
    s64 m_DownloadBps; // bits per second
    int m_ResultCode; // An optional result value in case of errors or partial results
    unsigned m_Source;
};


}   // namespace rage

#endif  //NET_BANDWIDTH_H
