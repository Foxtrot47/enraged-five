// 
// net/netsocketmanager.cpp
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#include "netsocketmanager.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "diag/channel.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "net/netdiag.h"
#include "net/nethardware.h"
#include "net/packet.h"
#include "net/relay.h"
#include "net/status.h"
#include "profile/rocky.h"
#include "system/param.h"
#include "system/timer.h"

#if RSG_PC || RSG_DURANGO
#define STRICT
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#pragma warning(pop)
#elif RSG_ORBIS
#include <sys/select.h>
#endif //RSG_PC || RSG_DURANGO

#if RSG_ANDROID
// The android OS doesn't suspend threads when the application is suspended, allowing
// SKTMGR Receive thread to keep ticking on the background wasting battery
// The event is signaled when the app is resumed
extern  rage::sysIpcEvent                g_SktMgrEvent;
extern  rage::sysAtomic<bool>            g_AppSuspended;
#endif // RSG_ANDROID

namespace rage
{

RAGE_DECLARE_CHANNEL(ragenet);
RAGE_DEFINE_SUBCHANNEL(ragenet, sktmgr)
#undef __rage_channel
#define __rage_channel ragenet_sktmgr

#define NET_SET_SKTERR(se, e)   while((se)){*(se) = (e);break;}

#define SKT_MGR_INVALID_SKT_ID (netSocket::INVALID_SOCKET_ID)
#define SKT_MGR_IS_SOCKET_ID_VALID(i) (i != SKT_MGR_INVALID_SKT_ID)

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
//////////////////////////////////////////////////////////////////////////
//  netSocketManager::Endpoint
//////////////////////////////////////////////////////////////////////////

netSocketManager::Endpoint::Endpoint()
: m_Status(nullptr)
{
	Clear();
}

netSocketManager::Endpoint::~Endpoint()
{
	Clear();
}

void netSocketManager::Endpoint::Clear()
{
	m_ByIdLink.m_key = SKT_MGR_INVALID_SKT_ID;
	m_ByAddrLink.m_key.Clear();

	if(m_Skt.IsCreated())
	{
		netHardware::DestroySocket(&m_Skt);
	}

	m_Timeout.Clear();
	m_IsPendingDestroy = false;

	if(m_Status)
	{
		if(m_Status->Pending())
		{
			m_Status->SetFailed();
		}
		else
		{
			m_Status->ForceFailed();
		}

		m_Status = nullptr;
	}
}

void netSocketManager::Endpoint::Update()
{
	m_Timeout.Update();
}

unsigned netSocketManager::Endpoint::GetId() const
{
	return m_ByIdLink.m_key;
}

const netSocketAddress& netSocketManager::Endpoint::GetAddr() const
{
	return m_ByAddrLink.m_key;
}

netSocket* netSocketManager::Endpoint::GetSocket()
{
	return &m_Skt;
}

const netSocket* netSocketManager::Endpoint::GetSocket() const
{
	return &m_Skt;
}

netStatus* netSocketManager::Endpoint::GetStatus()
{
	return m_Status;
}

void netSocketManager::Endpoint::SetStatus(netStatus* status)
{
	m_Status = status;
}

void netSocketManager::Endpoint::SetTimeout(const int timeoutMs)
{
	m_Timeout.InitMilliseconds(timeoutMs);
}

void netSocketManager::Endpoint::ResetTimeout()
{
	m_Timeout.Reset();
}

bool netSocketManager::Endpoint::IsTimedOut() const
{
	return m_Timeout.IsTimedOut();
}

void netSocketManager::Endpoint::SetPendingDestroy()
{
	m_IsPendingDestroy = true;
}

bool netSocketManager::Endpoint::IsPendingDestroy() const
{
	return m_IsPendingDestroy;
}

//////////////////////////////////////////////////////////////////////////
//  netSktMgrRcvBuffer
//////////////////////////////////////////////////////////////////////////
template<unsigned BUFFER_SIZE, unsigned MAX_ELEMENTS>
class netSktMgrRcvBuffer
{
public:

	struct Element
	{
		netSocketAddress m_Sender;
		void* m_Buffer;
		u16 m_Size;
	};

	netSktMgrRcvBuffer(sysMemAllocator* allocator)
	: m_Allocator(nullptr)
	, m_Buffer(nullptr)
	{
		Clear();
		m_Allocator = allocator;

		if(netVerify(m_Allocator))
		{
			m_Buffer = (u8*)m_Allocator->RAGE_LOG_ALLOCATE(BUFFER_SIZE, 0);
			if(m_Buffer)
			{
				m_CurPosition = m_Buffer;
			}
		}
	}

	~netSktMgrRcvBuffer()
	{
		Clear();
	}

	void Clear()
	{
		m_CurPosition = nullptr;

		m_NumElements = 0;
		sysMemSet(m_Elements, 0, sizeof(m_Elements));

		if(m_Buffer && m_Allocator)
		{
			m_Allocator->Free(m_Buffer);
			m_Buffer = nullptr;
		}

		m_Allocator = nullptr;
	}

	Element* ReserveElement(const unsigned maxSize)
	{
		const unsigned available = GetAvailableMemory();
		if((available >= maxSize) && (m_NumElements < MAX_ELEMENTS))
		{
			Element* e = &m_Elements[m_NumElements];
			e->m_Buffer = m_CurPosition;
			return e;
		}

		return nullptr;
	}

	unsigned GetAvailableMemory() const
	{
		const int available = ptrdiff_t_to_int(m_Buffer + BUFFER_SIZE - m_CurPosition);
		return netVerify(available > 0) ? (unsigned)available : 0;
	}

	void CommitElement(Element* e)
	{
		netAssert(e->m_Size > 0);
		netAssert(e->m_Size <= GetAvailableMemory());
		netAssert(m_NumElements < MAX_ELEMENTS);
		m_CurPosition += e->m_Size;
		++m_NumElements;
	}

	void ClearElements()
	{
		m_CurPosition = m_Buffer;
		m_NumElements = 0;
	}

	unsigned GetNumElements() const
	{
		return m_NumElements;
	}

	const Element* GetElement(const unsigned index) const 
	{
		if(netVerify(index < MAX_ELEMENTS))
		{
			return &m_Elements[index];
		}

		return nullptr;
	}

private:
	sysMemAllocator* m_Allocator;
	u8* m_Buffer;
	u8* m_CurPosition;
	unsigned m_NumElements;
	Element m_Elements[MAX_ELEMENTS];
};
#endif

//////////////////////////////////////////////////////////////////////////
//  netSocketManager
//////////////////////////////////////////////////////////////////////////

netSocketManager::netSocketManager()
{
	Clear();
}

netSocketManager::~netSocketManager()
{
	Shutdown();
}

void
netSocketManager::Clear()
{
	m_Initialized = false;
	m_RequestedPort = 0;
	m_RcvThreadHandle = sysIpcThreadIdInvalid;
	m_RcvThreadId = sysIpcCurrentThreadIdInvalid;
	m_Allocator = nullptr;
	m_RcvThreadExit = false;
	m_State = STATE_CREATE_SOCKET;
	m_CreateSocketRetryTimer.Shutdown();
}

bool
netSocketManager::Init(sysMemAllocator* allocator, const int cpuAffinity, const u16 mainSocketPort)
{
	bool success = false;

    if(netVerify(!IsInitialized()))
    {
		// override the port if the platform requires binding to a port in a specified range for P2P connections
		u16 portLowerBound = 0;
		u16 portUpperBound = 0;
		netSocket::GetPortRangeForP2pConnections(portLowerBound, portUpperBound);
		if((mainSocketPort < portLowerBound) || (mainSocketPort > portUpperBound))
		{
			m_RequestedPort = portLowerBound;
		}		
		else
		{
			m_RequestedPort = mainSocketPort;
		}

		m_Allocator = allocator;

        m_CreateSocketRetryTimer.InitSeconds(CREATE_SOCKET_MIN_RETRY_INTERVAL_SEC,
											 CREATE_SOCKET_MAX_RETRY_INTERVAL_SEC);
        m_CreateSocketRetryTimer.ForceRetry();

        netDebug("Running in multi-threaded mode");

        m_RcvThreadExit = false;

        //The semaphores on which the threads wait before they start.
        m_RcvThreadSema = sysIpcCreateSema(0);

        if(m_RcvThreadSema)
        {
            m_RcvThreadHandle =
                sysIpcCreateThread(&netSocketManager::ProcessSockets,
                                    this,
									RECEIVE_THREAD_STACK_SIZE,
                                    PRIO_NORMAL,
                                    "[SKTMGR] Receive",
                                    cpuAffinity, 
									"RageNetRecv");

            if(sysIpcThreadIdInvalid != m_RcvThreadHandle)
            {
                m_Initialized = true;
                success = true;
            }
            else
            {
                ShutdownThreads();
                netError("Error starting worker threads");
            }
        }
        else
        {
            success = m_Initialized = true;
        }

        if(!success)
        {
            m_Allocator = nullptr;
        }

		// initial state
		SetState(STATE_CREATE_SOCKET);

		AddWidgets();
    }

	return success;
}

void
netSocketManager::Shutdown()
{
    if(IsInitialized())
    {
        ShutdownThreads();

        SYS_CS_SYNC(m_MainCs);

        for(int i = 0; i < NET_SOCKET_EVENT_NUM_EVENTS; ++i)
        {
            m_ConcurrentDelegators[i].Clear();
        }

		Clear();
    }
}

bool
netSocketManager::IsInitialized() const
{
    return m_Initialized;
}

bool 
netSocketManager::CanSendReceive() const
{
	return m_MainSkt.CanSendReceive();
}

void netSocketManager::AddWidgets()
{
#if RSG_BANK && !__RGSC_DLL
	bkBank *pBank = BANKMGR.FindBank("Network");

	if(!pBank)
	{
		pBank = &BANKMGR.CreateBank("Network");
	}

	if (!pBank) { return; }

	pBank->PushGroup("Socket Manager", false);

	// add widgets here

	pBank->PopGroup();
#endif
}

#if !__NO_OUTPUT
const char*
netSocketManager::GetStateString(const State state)
{
	switch(state)
	{
	case STATE_CREATE_SOCKET: return "STATE_CREATE_SOCKET"; break;
	case STATE_RUNNING: return "STATE_RUNNING"; break;
	}

	return "Unknown State";
}
#endif

void
netSocketManager::SetState(const State state)
{
	if(m_State != state)
	{
		netDebug1("SetState :: New: %s, Was: %s", GetStateString(state), GetStateString(m_State));
		m_State = state;
	}
}

bool
netSocketManager::AddConcurrentDelegate(netSocketEventType eventType, Delegate* dlgt)
{
    SYS_CS_SYNC(m_ConcurrentDlgtCs);

    if(netVerify(IsInitialized())
        && netVerify((int)eventType >= 0)
        && netVerify(eventType < NET_SOCKET_EVENT_NUM_EVENTS))
    {
        m_ConcurrentDelegators[eventType].AddDelegate(dlgt);
        return true;
    }

    return false;
}

bool
netSocketManager::RemoveConcurrentDelegate(netSocketEventType eventType, Delegate* dlgt)
{
    SYS_CS_SYNC(m_ConcurrentDlgtCs);

    if(netVerify(IsInitialized())
        && netVerify((int)eventType >= 0)
        && netVerify(eventType < NET_SOCKET_EVENT_NUM_EVENTS)
        && netVerifyf(m_ConcurrentDelegators[eventType].HasDelegate(dlgt),
                    "Delegate isn't registered for concurrent events of type: %d", eventType))
    {
        m_ConcurrentDelegators[eventType].RemoveDelegate(dlgt);
        return true;
    }

    return false;
}

void
netSocketManager::Update()
{
	PROFILE

    if(!IsInitialized())
    {
        return;
    }

    switch(m_State)
    {
	case STATE_CREATE_SOCKET:
		{
			SYS_CS_SYNC(m_MainCs);

			m_CreateSocketRetryTimer.Update();

			if(netHardware::IsAvailable()
				&& m_CreateSocketRetryTimer.IsTimeToRetry())
			{
				netDebug("Creating main socket...");

				m_CreateSocketRetryTimer.Restart();

				const netSocketBlockingType blocking = NET_SOCKET_BLOCKING;

				if(netVerifyf(netHardware::CreateSocket(&m_MainSkt,
								m_RequestedPort,
								NET_PROTO_UDP,
								blocking,
                                NET_DEFAULT_ADDRESS_FAMILY,
								MAIN_SOCKET_SEND_BUFFER_SIZE_BYTES,
								MAIN_SOCKET_RCV_BUFFER_SIZE_BYTES),
								"Error creating main socket"))
				{
					sysIpcSignalSema(m_RcvThreadSema);
					SetState(STATE_RUNNING);
				}
			}
		}
		break;
    case STATE_RUNNING:
		if(!netHardware::IsAvailable())
		{
			SYS_CS_SYNC(m_MainCs);

			netDebug("Hardware unavailable");
			DestroySockets();
			m_CreateSocketRetryTimer.ForceRetry();
			SetState(STATE_CREATE_SOCKET);
		}
        break;
	default:
		break;
    }
}

netSocket*
netSocketManager::GetMainSocket()
{
	return &m_MainSkt;
}

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
bool 
netSocketManager::CreateSymmetricSocket(const u16 port, unsigned* socketId, netStatus* status)
{
	netSocketManager::Endpoint* ep = nullptr;

	std::pair<EndpointsById::iterator, bool> byIdResult;
	byIdResult.second = false;

	rtry
	{
		SYS_CS_SYNC(m_MainCs);

		rverifyall(socketId != nullptr);
		rverifyall((status == nullptr) || !status->Pending());

		*socketId = SKT_MGR_INVALID_SKT_ID;

		rcheck(m_EndpointsById.size() < MAX_ENDPOINTS,
				catchall,
				netWarning("CreateSymmetricSocket :: we already have the max number of symmetric sockets"));

		const netSocketBlockingType blocking = NET_SOCKET_BLOCKING;
		
		ep = (netSocketManager::Endpoint*)m_Allocator->RAGE_LOG_ALLOCATE(sizeof(netSocketManager::Endpoint), 0);
		rcheck(ep, catchall, netWarning("Could not allocate memory for a netSocketManager::Endpoint"));
		new (ep) netSocketManager::Endpoint();
	
		rverifyall(netHardware::CreateSocket(ep->GetSocket(),
											port,
											NET_PROTO_UDP,
											blocking,
                                            NET_DEFAULT_ADDRESS_FAMILY,
											SYMMETRIC_SOCKET_SEND_BUFFER_SIZE_BYTES,
											SYMMETRIC_SOCKET_RCV_BUFFER_SIZE_BYTES));

		unsigned id = ep->GetSocket()->GetId();
		byIdResult = m_EndpointsById.insert(id, ep);
		rverify(byIdResult.second, catchall,
				netError("[%u] CreateSymmetricSocket: Failed to add endpoint to map", id));

		ep->SetTimeout(Endpoint::DEFAULT_TIMEOUT_MS);

		*socketId = id;

		netDebug("[%u] Created endpoint", id);

		if(status)
		{
			if(ep->GetSocket()->CanSendReceive())
			{
				status->ForceSucceeded();
			}
			else
			{
				status->SetPending();
				ep->SetStatus(status);
			}
		}

		return true;
	}
	rcatchall
	{
		if(byIdResult.second)
		{
			m_EndpointsById.erase(ep);
		}

		if(ep)
		{
			ep->~Endpoint();
			m_Allocator->Free(ep);
			ep = nullptr;
		}

		if(status)
		{
			if(status->Pending())
			{
				status->SetFailed();
			}
			else
			{
				status->ForceFailed();
			}
		}

		return false;
	}
}

bool 
netSocketManager::AssociateSymmetricSocket(const unsigned socketId, const netSocketAddress& addr)
{
	netSocketManager::Endpoint* ep = nullptr;

	std::pair<EndpointsByAddr::iterator, bool> byAddrResult;
	byAddrResult.second = false;

	rtry
	{
		SYS_CS_SYNC(m_MainCs);

		rverifyall(addr.IsValid());

		rverify(!GetEndpointByAddr(addr),
				catchall,
				netError("AssociateSymmetricSocket :: a symmetric socket is already assocated with address " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr)));

		ep = GetEndpointById(socketId);

		rverify(ep,
				catchall,
				netError("AssociateSymmetricSocket :: no symmetric socket exists with id %u", socketId));

		rverify(!ep->GetAddr().IsValid(),
			catchall,
			netError("AssociateSymmetricSocket :: socket %d is already associated with address " NET_ADDR_FMT, socketId, NET_ADDR_FOR_PRINTF(ep->GetAddr())))

		byAddrResult = m_EndpointsByAddr.insert(addr, ep);
		rverify(byAddrResult.second, catchall,
				netError("AssociateSymmetricSocket: Failed to add endpoint to map: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr)));

		netDebug("[%u] Associated socket with destination address " NET_ADDR_FMT, socketId, NET_ADDR_FOR_PRINTF(addr));

		return true;
	}
	rcatchall
	{
		if(byAddrResult.second)
		{
			m_EndpointsByAddr.erase(ep);
		}

		if(ep)
		{
			ep->m_ByAddrLink.m_key.Clear();
		}

		return false;
	}
}

void 
netSocketManager::DestroySymmetricSocket(const unsigned socketId)
{
	rtry
	{
		SYS_CS_SYNC(m_MainCs);

		rverifyall(SKT_MGR_IS_SOCKET_ID_VALID(socketId));

		Endpoint* ep = GetEndpointById(socketId);
		rcheck(ep, catchall,
			   netDebug("DestroySymmetricSocket :: no endpoint exists with id %u",
						socketId));

#if !__NO_OUTPUT
		if(ep->GetAddr().IsValid())
		{
			netDebug("[%u] Endpoint with address " NET_ADDR_FMT " marked for destruction.",
					ep->GetId(), NET_ADDR_FOR_PRINTF(ep->GetAddr()));
		}
		else
		{
			netDebug("[%u] Endpoint marked for destruction (unassociated).", ep->GetId());
		}
#endif

		ep->SetPendingDestroy();
	}
	rcatchall
	{

	}
}

void 
netSocketManager::DestroySymmetricSocket(const netSocketAddress& addr)
{
	rtry
	{
		SYS_CS_SYNC(m_MainCs);

		rcheckall(addr.IsValid());

		Endpoint* epByAddr = GetEndpointByAddr(addr);
		rcheckall(epByAddr);

		DestroySymmetricSocket(epByAddr->GetId());
	}
	rcatchall
	{

	}
}

void 
netSocketManager::DestroyEndpoint(Endpoint* ep)
{
	rtry
	{
		SYS_CS_SYNC(m_MainCs);

		rverifyall(ep);

		m_EndpointsById.erase(ep);

		Endpoint* epByAddr = GetEndpointByAddr(ep->GetAddr());
		if(epByAddr)
		{
			netAssert(epByAddr == ep);
			m_EndpointsByAddr.erase(epByAddr);
		}

#if !__NO_OUTPUT
		if(epByAddr)
		{
			netDebug("[%u] Destroyed endpoint associated with address " NET_ADDR_FMT, ep->GetId(), NET_ADDR_FOR_PRINTF(ep->GetAddr()));
		}
		else
		{
			netDebug("[%u] Destroyed endpoint (unassociated)", ep->GetId());
		}
#endif

		ep->~Endpoint();
		m_Allocator->Free(ep);
		ep = nullptr;
	}
	rcatchall
	{

	}
}

bool 
netSocketManager::IsUsingSymmetricSocket(const netSocketAddress& addr)
{
	rtry
	{
		SYS_CS_SYNC(m_MainCs);

		rcheckall(addr.IsValid());

		Endpoint* epByAddr = GetEndpointByAddr(addr);
		return epByAddr != nullptr;
	}
	rcatchall
	{
		return false;
	}
}

const netSocketAddress&
netSocketManager::GetSymmetricSocketAddress(const unsigned socketId)
{
	rtry
	{
		SYS_CS_SYNC(m_MainCs);

		rverifyall(SKT_MGR_IS_SOCKET_ID_VALID(socketId));

		Endpoint* ep = GetEndpointById(socketId);
		rcheck(ep, catchall,
			   netDebug("GetSymmetricSocketAddress :: no endpoint exists with id %u",
						socketId));

		return ep->GetSocket()->GetAddress();
	}
	rcatchall
	{
		return netSocketAddress::INVALID_ADDRESS;
	}
}

netSocket*
netSocketManager::GetSymmetricSocket(const unsigned socketId)
{
	rtry
	{
		SYS_CS_SYNC(m_MainCs);

		rverifyall(SKT_MGR_IS_SOCKET_ID_VALID(socketId));

		Endpoint* ep = GetEndpointById(socketId);
		rcheck(ep, catchall,
			   netDebug("GetSymmetricSocket :: no endpoint exists with id %u",
						socketId));

		return ep->GetSocket();
	}
	rcatchall
	{
		return nullptr;
	}
}

#endif

u16 
netSocketManager::GetRequestedPort() const
{
	return m_RequestedPort;
}

sysIpcCurrentThreadId
netSocketManager::GetReceiveThreadId() const
{
	return m_RcvThreadId;
}

unsigned
netSocketManager::GetNumSocketSendBufferOverflows() const
{
	return netSocket::GetNumSocketSendBufferOverflows();
}

unsigned
netSocketManager::ResetNumSocketSendBufferOverflows()
{
	return netSocket::ResetNumSocketSendBufferOverflows();
}

bool 
netSocketManager::GetReceiveBufferInfo(const netAddress& addr, unsigned& rcvBufQueueLen, unsigned& rcvBufSize) const
{
    SYS_CS_SYNC(m_MainCs);

	rcvBufQueueLen = 0;
	rcvBufSize = 0;

	rtry
	{
		rverify(IsInitialized(),
				catchall,
				netError("Send :: netSocketManager not initialized"));

		rverify(addr.IsValid(),
				catchall,
				netError("Send :: [" NET_ADDR_FMT "] is not a valid address", NET_ADDR_FOR_PRINTF(addr)));
		
		const netSocket* skt = &m_MainSkt;

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
		const netSocketAddress& socketAddr = addr.IsDirectAddr() ? addr.GetTargetAddress() :
											(addr.IsPeerRelayAddr() ? addr.GetProxyAddress() :
											netSocketAddress::INVALID_ADDRESS);

		if(socketAddr.IsValid())
		{
			// retrieve the symmetric socket associated with this endpoint, if any
			const netSocketManager::Endpoint* ep = GetEndpointByAddr(socketAddr);
			if(ep)
			{
				skt = ep->GetSocket();
			}
		}
#endif

		if(!skt->CanSendReceive())
		{
			return false;
		}

		return skt->GetReceiveBufferInfo(rcvBufQueueLen, rcvBufSize);
	}
	rcatchall
	{
		return false;
	}
}

bool
netSocketManager::Send(const netSocketAddress& addr,
						const void *data,
						const unsigned size,
						netSocketError* sktErr)
{
    SYS_CS_SYNC(m_MainCs);

	NET_SET_SKTERR(sktErr, NET_SOCKERR_UNKNOWN);

	rtry
	{
		rverify(IsInitialized(),
				catchall,
				netError("Send :: netSocketManager not initialized"));

		rverify(addr.IsValid(),
				catchall,
				netError("Send :: [" NET_ADDR_FMT "] is not a valid address", NET_ADDR_FOR_PRINTF(addr)));
		
		rverify(data,
				catchall,
				netError("data is null"));

		rverify(size > 0,
				catchall,
				netError("Send :: No payload"));

		if(!netVerify(size <= netSocket::MAX_BUFFER_SIZE))
		{
			netError("Send :: Message too long");
			NET_SET_SKTERR(sktErr, NET_SOCKERR_MESSAGE_TOO_LONG);
			return false;
		}
		else
		{
			netSocket* skt = &m_MainSkt;

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
			// retrieve the symmetric socket associated with this endpoint, if any
			netSocketManager::Endpoint* ep = GetEndpointByAddr(addr);
			if(ep)
			{
				skt = ep->GetSocket();
				ep->ResetTimeout();
			}
#endif

			if(!skt->CanSendReceive())
			{
				netError("Send :: Can't send on socket");
				NET_SET_SKTERR(sktErr, NET_SOCKERR_INVALID_SOCKET);
				return false;
			}

			return skt->Send(addr, data, size, sktErr);
		}
	}
	rcatchall
	{
		return false;
	}
}

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
netSocketManager::Endpoint*
netSocketManager::GetEndpointByAddr(const netSocketAddress& addr)
{
	SYS_CS_SYNC(m_MainCs);

	if(!addr.IsValid())
	{
		return nullptr;
	}

	if(m_EndpointsByAddr.empty())
	{
		return nullptr;
	}

    EndpointsByAddr::iterator it = m_EndpointsByAddr.find(addr);

    return (m_EndpointsByAddr.end() != it) ? it->second : nullptr;
}

const netSocketManager::Endpoint*
netSocketManager::GetEndpointByAddr(const netSocketAddress& addr) const
{
	return const_cast<netSocketManager*>(this)->GetEndpointByAddr(addr);
}

netSocketManager::Endpoint*
netSocketManager::GetEndpointById(const unsigned socketId)
{
	SYS_CS_SYNC(m_MainCs);

	if(!netVerify(SKT_MGR_IS_SOCKET_ID_VALID(socketId)))
	{
		return nullptr;
	}

	if(m_EndpointsById.empty())
	{
		return nullptr;
	}

    EndpointsById::iterator it = m_EndpointsById.find(socketId);

    return (m_EndpointsById.end() != it) ? it->second : nullptr;
}
#endif

void netSocketManager::DestroySockets()
{
	if(m_MainSkt.IsCreated())
	{
		netHardware::DestroySocket(&m_MainSkt);
	}

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
	{
		SYS_CS_SYNC(m_MainCs);

		EndpointsById::iterator it = m_EndpointsById.begin();
		EndpointsById::iterator next = it;
		EndpointsById::const_iterator stop = m_EndpointsById.end();

		for(++next; stop != it; it = next, ++next)
		{
			netSocketManager::Endpoint* ep = it->second;
			DestroySymmetricSocket(ep->GetId());
		}

		netAssert(m_EndpointsById.empty());
		netAssert(m_EndpointsByAddr.empty());
	}
#endif
}

void
netSocketManager::ShutdownThreads()
{
    //Signal the threads to stop
    m_RcvThreadExit = true;

    ///Destroy the sockets to unblock the threads.
	DestroySockets();

    if(m_RcvThreadHandle != sysIpcThreadIdInvalid)
    {
        //Unblock the thread
        sysIpcSignalSema(m_RcvThreadSema);
        //Wait for it to stop
        sysIpcWaitThreadExit(m_RcvThreadHandle);
        m_RcvThreadHandle = sysIpcThreadIdInvalid;
    }

    m_RcvThreadExit = false;

    if(m_RcvThreadSema)
    {
        sysIpcDeleteSema(m_RcvThreadSema);
        m_RcvThreadSema = nullptr;
    }
}

void
netSocketManager::ProcessSockets(void* arg)
{
	PROFILER_THREAD("RageNetSktMgr", 1);

	netSocketManager* sktMgr = (netSocketManager*)arg;

	sktMgr->m_RcvThreadId = sysIpcGetCurrentThreadId();

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
 	netSktMgrRcvBuffer<SYMMETRIC_SOCKET_RCV_QUEUE_SIZE_BYTES, MAX_ENDPOINTS> rcvBuf(sktMgr->m_Allocator);
 	typedef netSktMgrRcvBuffer<SYMMETRIC_SOCKET_RCV_QUEUE_SIZE_BYTES, MAX_ENDPOINTS>::Element QueuedPkt;
#endif

    while(!sktMgr->m_RcvThreadExit)
    {
        //Wait for the main thread to signal us we have a valid socket.
		sysIpcWaitSemaTimed(sktMgr->m_RcvThreadSema, 60*1000);

        while(sktMgr->m_MainSkt.CanSendReceive())
        {
#if RSG_ANDROID
			if (g_AppSuspended)
            {
			    sysIpcWaitEvent(g_SktMgrEvent);
            }
#endif // RSG_ANDROID

            rtry
            {
                rverify(sktMgr->IsInitialized(),catchall,);

				fd_set fdsr, fdsw, fdse;

				FD_ZERO(&fdsr);
				FD_ZERO(&fdsw);
				FD_ZERO(&fdse);

				netSocketFd sktFd = (netSocketFd)sktMgr->m_MainSkt.GetRawSocket();
				rverify(sktFd >= 0, catchall, );
				netSocketFd highest = sktFd;
				FD_SET(sktFd, &fdsr);

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
				{
					SYS_CS_SYNC(sktMgr->m_MainCs);

					EndpointsById::iterator it = sktMgr->m_EndpointsById.begin();
					EndpointsById::iterator next = it;
					EndpointsById::const_iterator stop = sktMgr->m_EndpointsById.end();

                    for(++next; stop != it; it = next, ++next)
                    {
						netSocketManager::Endpoint* ep = it->second;

						ep->Update();

						if(ep->IsPendingDestroy() || ep->IsTimedOut())
						{
#if !__NO_OUTPUT
							if(ep->GetAddr().IsValid())
							{
								netDebug("[%u] Endpoint with address " NET_ADDR_FMT " %s.",
									ep->GetId(),
									NET_ADDR_FOR_PRINTF(ep->GetAddr()),
									ep->IsPendingDestroy() ? "pending destruction" : "timed out");
							}
							else
							{
								netDebug("[%u] Endpoint %s (unassociated).", ep->GetId(),
									ep->IsPendingDestroy() ? "pending destruction" : "timed out");
							}
#endif

							sktMgr->DestroyEndpoint(ep);
							continue;
						}

						if(ep->GetSocket()->CanSendReceive())
						{
							if(ep->GetStatus() && ep->GetStatus()->Pending())
							{
								ep->GetStatus()->SetSucceeded();
								ep->SetStatus(nullptr);
							}

							if(ep->GetAddr().IsValid())
							{
								// the ep should be in the address map
								netAssert(sktMgr->GetEndpointByAddr(ep->GetAddr()));

								sktFd = (netSocketFd)ep->GetSocket()->GetRawSocket();
								if(sktFd >= 0)
								{
									FD_SET(sktFd, &fdsr);
									FD_SET(sktFd, &fdse);

									if(sktFd > highest)
									{
										highest = sktFd;
									}
								}
							}
						}
					}
				}
#endif

				rcheck(highest >= 0, catchall, );

				// Only wait for a second on select so that we will tick and detect timeouts.
				// Note: on some platforms (e.g. Linux/Android), select() can modify the timer to
				// indicate the amount of time waited or amount of time left. Reset the timeout
				// on each call, otherwise it drops to 0 (no waiting).
				timeval selectTimeout;
				selectTimeout.tv_sec = 1;
				selectTimeout.tv_usec = 0;

				// critical section is unlocked while we select
                int numSkts = netSocket::Select(int(highest+1), &fdsr, &fdsw, &fdse, &selectTimeout);

                if(numSkts < 0)
                {
					// we might have been shutdown
					if(sktMgr->m_RcvThreadExit || !sktMgr->m_MainSkt.CanSendReceive())
					{
						continue;
					}

                    const int lastError = netSocket::GetLastSocketError();
                    if(NET_SOCKERR_WOULD_BLOCK != lastError && NET_SOCKERR_IN_PROGRESS != lastError)
                    {
                         netError("Select Error %d", lastError);
                    }
                }

                // we want to run this at least once per second, so a timeout has been added to Select.
				{
					SYS_CS_SYNC(sktMgr->m_ConcurrentDlgtCs);

					if (sktMgr->m_ConcurrentDelegators[NET_SOCKET_EVENT_RECEIVE_THREAD_TICKED].HasDelegates())
					{
						//Concurrent delegates can handle the event at any time, from
						//any thread, so just send it now.
						const netSocketEventReceiveThreadTicked e(sktMgr->m_Allocator);
						sktMgr->m_ConcurrentDelegators[NET_SOCKET_EVENT_RECEIVE_THREAD_TICKED].Dispatch(e);
					}
				}

				if(numSkts <= 0)
				{
					continue;
				}

				// one extra byte so we can null-terminate messages
				const unsigned maxLen = netSocket::MAX_BUFFER_SIZE + 1;

				sktFd = (netSocketFd)sktMgr->m_MainSkt.GetRawSocket();

				if((sktFd >= 0) && FD_ISSET(sktFd, &fdsr))
				{
					u8 pktBuf[maxLen + 1];
					netSocketAddress from;
					const int len = sktMgr->m_MainSkt.Receive(&from, pktBuf, sizeof(pktBuf) - 1);
					sktMgr->OnReceive(SOCKET_SOURCE_MAIN_SOCKET, from, pktBuf, len);
					--numSkts;
				}

#if SKT_MGR_ALLOW_SYMMETRIC_SOCKETS
				if(numSkts > 0)
				{
					// we cannot call skMgr->OnReceive() inside the critical section due to deadlocks.
					// Queue up packets inside the critical section and dispatch after exiting it.
					rcvBuf.ClearElements();

					{
						SYS_CS_SYNC(sktMgr->m_MainCs);

						EndpointsByAddr::iterator it = sktMgr->m_EndpointsByAddr.begin();
						EndpointsByAddr::const_iterator stop = sktMgr->m_EndpointsByAddr.end();

						for(; stop != it; ++it)
						{
							netSocketManager::Endpoint* ep = it->second;

							if(ep->IsPendingDestroy() || ep->IsTimedOut())
							{
								continue;
							}

							sktFd = (netSocketFd)ep->GetSocket()->GetRawSocket();
							if((sktFd >= 0) && FD_ISSET(sktFd, &fdsr))
							{
								CompileTimeAssert(SYMMETRIC_SOCKET_RCV_QUEUE_SIZE_BYTES >= maxLen);

								QueuedPkt* pkt = rcvBuf.ReserveElement(maxLen);
								if(!pkt)
								{
									// out of space in the receive queue, we'll receive it on the next iteration
									break;
								}

								const int len = ep->GetSocket()->Receive(&pkt->m_Sender, pkt->m_Buffer, maxLen - 1);

								if(len > 0)
								{
									pkt->m_Size = (u16)len;
									rcvBuf.CommitElement(pkt);
								}
							}
						}
					}
					
					// sktMgr->OnReceive must be called outside of the above critical section
					const unsigned numQueued = rcvBuf.GetNumElements();
					for(unsigned i = 0; i < numQueued; ++i)
					{
						const QueuedPkt* pkt = rcvBuf.GetElement(i);
						if(pkt)
						{
							sktMgr->OnReceive(SOCKET_SOURCE_SYMMETRIC_SOCKET, pkt->m_Sender, pkt->m_Buffer, pkt->m_Size);
						}
					}
				}
#endif
            }
            rcatchall
            {
            }
        }
    }

	rcvBuf.Clear();
}

void
netSocketManager::OnReceiveNonRelayPacket(const netSocketAddress& from,
										  const void* pktBuf,
										  const int len)
{
    const netAddress sender(from);
    const void* payload = pktBuf;
    const unsigned sizeofPayload = len;

    {
        SYS_CS_SYNC(m_ConcurrentDlgtCs);

        if(m_ConcurrentDelegators[NET_SOCKET_EVENT_PACKET_RECEIVED].HasDelegates())
        {
            //Concurrent delegates can handle the event at any time, from
            //any thread, so just send it now.
            const netSocketEventPacketReceived e(sender, payload, sizeofPayload, nullptr);
            m_ConcurrentDelegators[NET_SOCKET_EVENT_PACKET_RECEIVED].Dispatch(e);
        }
    }
}

void
netSocketManager::OnReceive(const netSocketManagerSource sourceSocket,
							const netSocketAddress& from,
							void* data,
							const int len)
{
	if(len > 0)
	{
		const bool consumed = (sourceSocket == SOCKET_SOURCE_MAIN_SOCKET) && netRelay::Accept(from, data, len);
		if(!consumed)
		{
			OnReceiveNonRelayPacket(from, data, len);
		}
	}
}

}   //namespace rage
