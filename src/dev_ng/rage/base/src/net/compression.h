// 
// net/compression.h 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_COMPRESSION_H
#define NET_COMPRESSION_H

#include "net.h"

// it's possible to train the dictionary using __FINAL_LOGGING builds
#define NET_COMPRESSION_TRAINING (!__NO_OUTPUT)

namespace rage
{

class netConnectionManager;
class netCompressionState;
class sysMemAllocator;
struct netCompressionDictionaryHeader;
class netCompressionTrainer;

//PURPOSE
//  P2P network packet compression.
//  UDP traffic requires a different compression strategy than most data streams. We can't use a
//  streaming compressor due to packet loss/reordering/resending, and block compression doesn't
//  get us much since most packets are tiny. We were seeing < 10% savings with block compressors
//  (zlib/lz4/datCompress). Instead we use a technique that uses block compression with a
//  precomputed static dictionary. This works by first collecting a large packet sample from QA
//  playing the game in every game mode available, and then training the dictionary offline using
//  tools specifically designed for small UDP game packets. The resulting dictionary is then
//  loaded into memory by all multiplayer participants. With this technique we save 40%-50%
//  of p2p and relay server bandwidth.
class netCompression
{
	friend class netConnectionManager;

public:
	static void SetTelemetrySendIntervalMs(const unsigned telemetrySendIntervalMs);

private:
	netCompression();

    //PARAMS
	//  allocator			- Optional. Used to allocate memory. If NULL the global
	//						  allocator will be used.
	//  cxnMgr				- The connection manager that owns this netCompression object.
	//  dictionaryFilename	- The path to the precomputed compression dictionary.
	//						  If NULL or file does not exist, then packets will be
	//						  sent uncompressed.
	bool Init(sysMemAllocator* allocator, netConnectionManager* cxnMgr, const char* dictionaryFilename);

	//PURPOSE
	//  Shuts down the class.
	void Shutdown();

	//PURPOSE
	//  Call every frame.
	void Update();

	//PURPOSE
	//  Returns true if Init() has been called successfully.
	bool IsInitialized() const;

	//PURPOSE
	//  Compresses a p2p network packet using the precomputed dictionary.
	//PARAMS
	//  source			- The data to compress.
	//  sizeOfSource	- Number of bytes in the source buffer.
	//  dest			- [out] Buffer to fill with compressed data.
	//  sizeofDest		- [in/out] On [in], number of bytes available in the dest buffer.
	//					  Must be >= sizeOfSource.
	//					  On [out], contains the size in bytes of the resulting compressed dest buffer.
	//					  Note: The final compressed size is guaranteed to be <= sizeOfSource
	//					  (no expansion), but the compression algorithm may temporarily write more
	//					  bytes than sizeOfSource during its compression attempt.
	//					  This function is safe in that it won't write more than sizeofDest, but it
	//					  will avoid compression if sizeofDest isn't large enough.
	//					  As a heuristic, make sizeofDest at least 16 bytes larger than sizeOfSource.
	//RETURNS
	//  True on success. If false, compression failed. Send the packet uncompressed.
	bool Compress(const u8* source, const unsigned sizeOfSource, u8* dest, unsigned& sizeofDest) const;

	//PURPOSE
	//  Decompresses a p2p network packet using the precomputed dictionary.
	//PARAMS
	//  source			- The data to decompress.
	//  sizeOfSource	- Number of bytes in the source buffer.
	//  dest			- [out] Buffer to fill with decompressed data.
	//  sizeofDest		- [in/out] On [in], number of bytes available in the dest buffer.
	//					  Must be large enough to contain the max size of a packet payload.
	//					  On [out], contains the number of bytes written to the decompressed dest buffer.
	//RETURNS
	//  True on success. If false, decompression failed. Discard the packet.
	bool Decompress(const u8* source, const unsigned sizeOfSource, u8* dest, unsigned& sizeofDest) const;

private:
	bool LoadDictionaryHeader(netCompressionDictionaryHeader* pHeader, const void* from_memory);
	bool LoadDictionaryFromFileData(netCompressionState* pCompressor, const void* fileData, int fileSize, int& memUsed);
	netCompressionState* LoadDictionary(const char* fileName);
	void Report(const unsigned uncompressedSize, const unsigned compressedSize, const bool success) const;
	void* Allocate(const unsigned numBytes) const;
	void Free(void* ptr) const;

	sysMemAllocator* m_Allocator;
	netCompressionState* m_State;
	netCompressionTrainer* m_Trainer;
	mutable s64 m_TotalBytesCompressed;
	mutable s64 m_TotalBytesUncompressed;	
	bool m_Initialized;
};

} // namespace rage

#endif  // NET_COMPRESSION_H
