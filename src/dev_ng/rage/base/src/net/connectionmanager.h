// 
// net/connectionmanager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_CONNECTIONMGR_H
#define NET_CONNECTIONMGR_H

#include "atl/bitset.h"
#include "atl/delegate.h"
#include "atl/inlist.h"
#include "atl/inmap.h"
#include "bandwidth.h"
#include "compression.h"
#include "connection.h"
#include "connectionrouter.h"
#include "crypto.h"
#include "event.h"
#include "netdiag.h"
#include "netsequence.h"
#include "netsocketmanager.h"
#include "netutil.h"
#include "relay.h"
#include "peeraddress.h"
#include "status.h"
#include "time.h"
#include "tunneler.h"
#include "rline/rlgamerhandle.h"
#include "system/criticalsection.h"
#include "system/service.h"

#define NET_CXNMGR_COLLECT_STATS (1)
#if NET_CXNMGR_COLLECT_STATS
#	define NET_CXNMGR_COLLECT_STATS_ONLY(...)    __VA_ARGS__
#else
#	define NET_CXNMGR_COLLECT_STATS_ONLY(...)
#endif

namespace rage
{

class netChannel;
class netEndpoint;
class netConnectionManager;
class netMemoryStats;
struct MsgCxnRelayAddrChanged;

typedef void(*netOutOfMemoryCallabck)(const EndpointId endpointId, const bool isFatal, const unsigned attemptedAllocation);
typedef inlist<netPacketRecorder, &netPacketRecorder::m_ListLink> PktRecorderList;

//PURPOSE
//  Connection info.
class Cxn : public netConnection
{
	friend class rage::netConnectionManager;
	friend class rage::netEndpoint;

public:

	enum 
	{
		DEFAULT_PENDING_CLOSE_MAX_TIME = 1000,
	};

	Cxn();

	static void SetPendingCloseMaxTime(const unsigned maxTime); 

private:

	bool Init(netConnectionManager* cxnMgr,
		netEventQueue<netEvent>* eventQ,
		sysMemAllocator* allocator);

	void Shutdown();

	bool Open(netEndpoint* ep,
		const int cxnId,
		const unsigned channelId);

	void Close();

	void Destroy();

	bool CanDestroy(OUTPUT_ONLY(bool addLogging = false)) const;

	bool IsClosed() const;
	bool IsPendingOpen() const;
	bool IsOpen() const;
	bool IsPendingClose() const;
	bool IsPendingCloseTimeExpired() const;

	inlist_node<Cxn> m_ListLink;

	//Ref count
	int m_Refs;

	//Pointer to a status object owned by the caller of OpenConnection.
	netStatus* m_Status;

	//Pre-allocated connection closed event.
	netEventConnectionClosed m_CxnClosedEvent;

	//Pre-allocated out of memory event.
	netEventOutOfMemory m_OutOfMemEvent;

	//If this connection is pending being closed and when this was set
	bool m_PendingClose : 1;
	unsigned m_PendingCloseTime; 

	static unsigned sm_PendingCloseMaxTime;
};

//Connectionless bundle.
class CxnlessBundle
{
public:

    CxnlessBundle(netBundle* bndl,
                  sysMemAllocator* allocator)
        : m_Bundle(bndl)
        , m_Allocator(allocator)
    {
    }

    netBundle* m_Bundle;
    sysMemAllocator* m_Allocator;
    inlist_node<CxnlessBundle> m_ListLink;
};

//PURPOSE
//  An endpoint is a collection of channels that all have the same destination address.
class netEndpoint
{
public:
#if NET_CXNMGR_COLLECT_STATS
	struct Statistics
	{
		// how much weight to apply to new samples when calculating 
		// exponential moving averages
		static const float STATS_EMA_ALPHA;

		enum RouteType
		{
			ROUTE_TYPE_INVALID = -1,
			ROUTE_TYPE_DIRECT = 0,
			ROUTE_TYPE_RELAY_SERVER = 1,
			ROUTE_TYPE_PEER_RELAY_2_HOPS = 2,
			ROUTE_TYPE_PEER_RELAY_3_HOPS = 3,
			ROUTE_TYPE_PEER_RELAY_MORE_HOPS = 4, // 4+ hops
			NUM_ROUTE_TYPES
		};

		Statistics();
		~Statistics();
		void Clear();
		bool HaveStats();
		void Finalize();
		void DumpStats();
		void SendTelemetry();
#if !__NO_OUTPUT
		const char* RouteTypeToString(const RouteType type) const;
#endif

		netEndpoint* m_EpOwner;

		// health
		u64 m_CxnStartTime;
		u64 m_CxnEndTime;
		u32 m_CxnDuration;
		u32 m_NumCxnsOpened;
		netMovingAverage<u32> m_Rtt;
		u32 m_MaxDeltaReceiveTime;

		// anomalies
		u32 m_NumOutOfMemoryEvents;
		u32 m_NumFailedAllocs;
		u32 m_NumTimedOutEvents;
		u32 m_NumSendErrors;
		u32 m_NumFailedEncryptions;
		u32 m_NumNoEncryptionKey;
		u32 m_NumFailedDecryptions;
		u32 m_NumDirectRouteReassignments;

		netConnection::TimeoutReason m_TimedOutReason;

		// route
		RouteType m_RouteType;

		// bandwidth
		u32 m_BytesSent;
		u32 m_PacketsSent;
		u32 m_BytesSentByRoute[NUM_ROUTE_TYPES];
		u32 m_PacketsSentByRoute[NUM_ROUTE_TYPES];

		u32 m_FramesSent;
		u32 m_FramesSentReliably;
		u32 m_FramesResent;
		u32 m_FramesDroppedDuplicate;
		u32 m_FramesDroppedLateUnreliable;
		u32 m_FramesDroppedWndOverflow;
		u32 m_FramesRcvdInOrder;
		u32 m_FramesRcvdOutOfOrder;

		// compression
		u32 m_BytesUncompressed;
		u32 m_BytesCompressed;
	};
#endif

	enum EndpointQoS
	{
		QOS_GOOD = 0,
		QOS_BAD = 1
	};

	enum EndpointQosReason
	{
		QOS_REASON_UNKNOWN = 0,
		QOS_REASON_CXN_TIMED_OUT = 1,
		QOS_REASON_LONG_TIME_SINCE_LAST_RECEIVE = 2,
		QOS_REASON_OLD_UNACKED_FRAME = 3,
		QOS_REASON_HIGH_RTT = 4,
		QOS_REASON_SOCKET_RCV_QUEUE = 5,
		QOS_REASON_CXN_MEMORY = 6,
	};

    netEndpoint();
    ~netEndpoint();

	EndpointId GetId() const;

    void Clear();

    void PurgeQueues();

    const netAddress& GetAddress() const;
	const netPeerAddress& GetPeerAddress() const;
	const netPeerId& GetPeerId() const;

	//PURPOSE
	//  Returns the number of hops along the route to this endpoint.
	//  Direct connection = 1 hop, relay server = 2 hops, peer relay >= 2 hops.
	u8 GetHopCount() const;
	void SetHopCount(const u8 hopCount);

	bool CanReassignOnRelayPacket() const;

#if NET_CXNMGR_COLLECT_STATS
	Statistics& GetStats();
#endif

    typedef inlist<CxnlessBundle, &CxnlessBundle::m_ListLink> CxnlessQueue;
	
	//Connections that can send (i.e. open and pending open connections).
	Cxn* m_Cxns[NET_MAX_CHANNELS];

	//Connections that can receive (i.e. open, pending open, and
	//pending close connections).
	typedef inlist<Cxn, &Cxn::m_ListLink> CxnList;
	CxnList m_CxnList;

	//Connectionless frames bound for this endpoint.
    CxnlessQueue m_CxnlessQ;

    //A pending tunnel request.  Will be NULL when no request is pending.
    netTunnelRequest* m_TunnelRqst;

    //Internal status object to pass to the netTunnelRequest and
    //Pointer to a status object owned by the caller of OpenTunnel.
    netStatus m_MyStatus;
    netStatus* m_Status;

    //Address to use if our connection fails or we can't
    //tunnel to the remote endpoint.
    netAddress m_RelayAddr;
	netPeerAddress m_PeerAddr;
	
	inmap_node<EndpointId, netEndpoint> m_ByEndpointIdLink;
	inmap_node<netPeerId, netEndpoint> m_ByPeerIdLink;
	inmap_node<netAddress, netEndpoint> m_ByAddrLink;
    inlist_node<netEndpoint> m_ListLink;

    //When this timeout reaches zero, pack and send packets.
    int m_PackTimeout;
	// Send interval per endpoint - allows bandwidth to be adjusted for each endpoint rather than globally.
	int m_SendInterval;

    enum 
    { 
        DEFAULT_INACTIVITY_TIMEOUT_MS   = 60 * 1000,
        DEFAULT_CXNLESS_TIMEOUT_MS      = 30 * 1000,
		DEFAULT_ADDR_HOLD_TIME_MS		= (3 * 1000),
		DEFAULT_BAD_QOS_THRESHOLD_MS	= (4 * 1000),
		DEFAULT_BAD_QOS_RCV_QUEUE_PCT	= (80),
    };

    //Timeout that expires if no packets are sent/received during the
    //inactivity timeout interval, or no connections are on the endpoint
    //during the cxnless timeout interval.
    int m_Timeout;
	unsigned int m_LastTime;

	static unsigned sm_InactivityTimeoutMs;
	static unsigned sm_CxnlessTimeoutMs;
	static unsigned sm_BadQosThresholdMs;

	unsigned m_LastReceiveTime;
	unsigned m_CreatedTime;

	//The last time we assigned or reassigned this endpoint's address.
	unsigned m_LastAddressAssignmentTime;

	netEventQueue<netEvent>* m_EventQ;
	netEventOutOfMemory m_OutOfMemEvent;

	EndpointId m_Id;

	// see comments on GetHopCount();
	u8 m_HopCount;

#if NET_CXNMGR_COLLECT_STATS
	Statistics m_Stats;
#endif

private:
	unsigned NextId() const;
};

//PURPOSE
//  Manages network connections.
//
//  Typically an app will initialize an instance of netConnectionManager
//  (see Init()) and then register one or more delegates.  The delegates
//  will receive netEvents dispatched by the connection manager.
//
//  Once a connection manager is initialized it can be used to open
//  connections to remote peers, and to send and receive data to and
//  from remote peers.
//
//  To run the connection manager in multi-threaded (MT) mode pass a
//  non-negative value for the cpu affinity to Init().  In MT mode
//  sends/receives will occur on a separate thread, while event
//  dispatches will occur on the thread calling Update().
class netConnectionManager
{
    class netChannel;

public:

	enum
	{
		//NOTE: Don't increase these values.  It's used to ensure
		//that a connection index is never greater than 0xFFFF.
		MAX_ENDPOINTS_IN_POOL = 0xFFFF,
		MAX_CXNS_IN_POOL = 0xFFFF,
		ENDPOINT_ALLOC_STEP = 4,
	};

	enum DefaultPolicies
	{
		DEFAULT_COMPRESSION_ENABLED = true,
		DEFAULT_SEND_INTERVAL_MS = 0,
		DEFAULT_BAD_QOS_MEM_AVAILABLE_PCT = 10,

#if !__NO_OUTPUT
		DEFAULT_CXN_LOCK_WAIT_THRESHOLD = 3,
		DEFAULT_CXN_LOCK_HOLD_THRESHOLD = 3,
		DEFAULT_CXN_LOCK_LONG_WAIT_THRESHOLD = 5,
		DEFAULT_CXN_LOCK_LONG_HOLD_THRESHOLD = 5,
		DEFAULT_CXN_LOCK_ONLY_OUTSIDE_LOCK = true,
		DEFAULT_CXN_LOCK_PRINT_MAIN_STACK_TRACE = false,
#endif
	};

	enum OverallQoS
	{
		QOS_GOOD = 0,
		QOS_BAD = 1
	};

	enum OverallQosReason
	{
		QOS_REASON_UNKNOWN = 0,
		QOS_REASON_MEMORY = 1,
		QOS_REASON_BANDWIDTH = 2,
	};

    //PURPOSE
    //  Declare the delegate type.
    //NOTES
    //  The signature for a delegate is:
    //
    //  void OnEvent(netConnectionManager* cxnMgr, const netEvent* event);
    //
    //  cxnMgr  - The connection manager dispatching the event.
    //  event   - The event.
    class Delegate : public atDelegate<void (netConnectionManager*, const netEvent*)>
    {
        friend class netConnectionManager;
        friend class Channel;

    public:

         Delegate();
        ~Delegate();

        void Unregister();

        bool IsRegistered() const;

    private:
		// marking as non-copyable to prevent hard-to-find bugs where the Delegate
		// is registered with one address, but removed with another address.
		// e.g. when deleting objects from an atFixedArray, it moves elements
		// down. a[2].dlgt might be registered with the connection manager, then
		// a[1] is deleted. a[2] is copied into a[1], and &a[1].dlgt != &a[2].dlgt.
		// When RemoveDelegate is then called with &a[1].dlgt, it's not found
		// in the list of registered delegates.
		NON_COPYABLE(Delegate);

        netConnectionManager* m_Owner;
        ChannelId m_ChannelId;
		unsigned m_ChannelProc;

        inlist_node<Delegate> m_ListLink;
	};

#define NET_CS_SYNC(cs) netConnectionManager::netCriticalSection __cssync##__LINE__(cs)

#if !__NO_OUTPUT
    //Used for debugging critical sections, and to make sure
    //our crit section token is locked at appropriate times.
    struct netCriticalSectionToken
    {
        netCriticalSectionToken();

        void Lock();

        void Unlock();

        bool IsLocked() const;

        sysCriticalSectionToken m_Token;
        unsigned m_LockCount;
		unsigned m_LockThrash;
		unsigned m_UnlockThrash;
		unsigned m_WaitCount;
		unsigned m_WaitThrash;
    };

    struct netCriticalSection
    {
        explicit netCriticalSection(netCriticalSectionToken& token)
            : m_Token(token)
        {
            m_Token.Lock();
        }

        ~netCriticalSection()
        {
            m_Token.Unlock();
        }

        netCriticalSectionToken& m_Token;
    };

#else

    typedef sysCriticalSectionToken netCriticalSectionToken;
    typedef sysCriticalSection netCriticalSection;

#endif  //!__NO_OUTPUT


    netConnectionManager();

    virtual ~netConnectionManager();

	// tunables
	static void SetAcquireCritSecForFullMainUpdate(const bool bAcquireCritSecForFullMainUpdate) { sm_bAcquireCritSecForFullMainUpdate = bAcquireCritSecForFullMainUpdate; }
	static bool GetAcquireCritSecForFullMainUpdate() { return sm_bAcquireCritSecForFullMainUpdate; }
	static void SetEndpointInactivityTimeoutMs(const unsigned nInactivityTimeoutMs);
	static void SetEndpointCxnlessTimeoutMs(const unsigned nCxnlessTimeoutMs);
	static void SetBadQosThresholdMs(const unsigned badQosThresholdMs);
	static void SetEnableOverallQos(const bool enableOverallQos);
	static void MetricConfigChanged();
	bool SetPoolSizeHints(const unsigned nEpHint, const unsigned nCxnHint);

    //FIXME (KB) - Provide a heuristic for computing the heap size
    //             for the allocator.

	//PURPOSE
	//  Initialize the connection manager without a socket.  The app must call Pack()/Unpack() to retrieve/deliver packets 
	//	from/to the connection manager.
	//PARAMS
	//  allocator           - Used for general memory allocation.  Must be capable of allocating memory for the given
	//                        number of connections, as well as for various event and frame queues.
	//  maxEpsHint          - Hint of the maximum number of endpoints that can be open simultaneously				
	//  maxCxnsHint         - Hint of the maximum number of connections that can be open simultaneously.
	//  socketManager       - The socket manager over which to send/receive packets.
	//  cpuAffinity         - CPU affinity for send/receive thread.  If -1 then send/receive will occur within the thread
	//                        that calls Update().
	// compressionAllocator - The allocator to use for loading the compression dictionary.
	// dictionaryFilename	- The path to the precomputed compression dictionary.
	//						  If NULL or file does not exist, then packets will be sent uncompressed.
	//  oomCallback         - Callback called immediately on the thread where the allocation attempt failed. Can be null.
	//NOTES
	//  To run the connection manager in multi-threaded (MT) mode pass a non-negative value for the cpu affinity.  
	//  In MT mode sends/receives will occur on a separate thread, while event dispatches will occur on the thread 
	//  calling Update().
    bool Init(sysMemAllocator* allocator,
			  const int maxEpsHint,
			  const int maxCxnsHint,
			  netSocketManager* socketManager,
              const int cpuAffinity,
			  sysMemAllocator* compressionAllocator,
			  const char* dictionaryFilename,
			  netOutOfMemoryCallabck oomCallback);

    //PURPOSE
    //  Shuts down the connection manager.
    void Shutdown();

	// PURPOSE
	//	Enables memory tracking, must be called before the manager is initialized.
	void SetMemoryTrackingEnabled(bool bEnabled);

	//PURPOSE
	//  Sends and receives connection data.
	//  This should be called often (i.e. at least 15Hz).
	//PARAMS
	//  curTime     - Current time in milliseconds.
	//NOTES
	//  If initialized as multi-threaded (see Init()) then curTime
	//  is ignored except when Update() called from the worker thread.
	//  This ensures we keep a consistent time.
	void Update(const unsigned curTime);

    //PURPOSE
    //  Returns true any open endpoints are using the relay server.
    bool IsUsingRelay() const;

    //PURPOSE
    //  Returns the socket manager used for sending/receiving data.
    netSocketManager* GetSocketManager() const;

    //PURPOSE
    //  Start listening for unsolicited connections on the given channel.
    //  The app will be notified of unsolicited connection requests
    //  with a netEventConnectionRequested event.  To accept an unsolicited
    //  connection call AcceptConnection().
    //PARAMS
    //  channelId   - Channel on which to listen.
    //NOTES
    //  Call StopListening() to stop listening for unsolicited connections.
    //  StopListening() must be called as many times as StartListening() in
    //  order to disable listening.
    void StartListening(const ChannelId channelId);

    //PURPOSE
    //  Stop listening for unsolicited connections on the given channel.
    //PARAMS
    //  channelId   - Channel on which to stop listening.
    //NOTES
    //  StopListening() must be called as many times as StartListening() in
    //  order to disable listening.
    void StopListening(const ChannelId channelId);

    //PURPOSE
    //  Returns true if listening for unsolicited connection requests
    //  on the given channel.
    //PARAMS
    //  channelId   - Channel to query.
    bool IsListening(const ChannelId channelId) const;

	//PURPOSE
	//  Opens a tunnel to a remote peer.
	//PARAMS
	//  peerAddr		- Peer address for the remote peer.
	//  tunnelDesc		- Tunnel description struct (see netTunnelDesc)
	//  tunnelRqst		- Tunnel request.  On successful completion call
	//					  GetEndpoinId() to retrieve the endpointId of the
	//					  peer.
	//  status			- Optional. Can be polled for completion.
	//RETURNS
	//  True on successful queuing of the request.
	//NOTES
	//  The netTunnelRequest and netStatus objects must stay valid until the
	//  asynchronous request completes.  They should not be stack variables.
	//
	//  A tunnel comprises a NAT traversal and security key
	//  exchange.  Tunnels are necessary when peers are behind routers
	//  and/or encrypted messages are being transmitted.
	//
	//  The peer address of the remote peer is typically discovered via
	//  a third party advertising or matchmaking service. Example:
	//  Social Club presence services, or matchmaking/session server.
	//
	//  When a tunnel is successfully opened, the network endpointId of
	//  the remote peer can be retrieved from the netTunnelRequest
	//  object. Note that the underlying netAddress that is discovered
	//  during tunneling can change or be reassigned from relay to direct
	//  or vice versa. It is recommended to use the endpointId and pass
	//  it to the connection manager, which handles address reassignments.
	//
	//  A successfully opened tunnel will be kept open as long as
	//  network traffic passes through it.  If there is no network
	//  traffic the tunnel will timeout after several seconds.  Further
	//  attempts to send/receive packets through the tunnel will fail.
	bool OpenTunnel(const netPeerAddress& peerAddr,
					const netTunnelDesc& tunnelDesc,
					netTunnelRequest* tunnelRqst,
					netStatus* status);

    //PURPOSE
    //  Cancels a pending tunnel request.
    void CancelTunnelRequest(netTunnelRequest* tunnelRqst);

    //PURPOSE
    //  Removes a cached relay address should it exist. 
    void RemoveCachedRelayAddress(const netAddress& useAddr);

	//PURPOSE
	//  Establishes mutual two-way communication
	//PARAMS
	//  epId		- The id of the endpoint on which to establish
	//					the connection (e.g. from OpenTunnel()).
	//  channelId   - The channel over which to send optional data.
	//					The value of the channel id must be less than or
	//					equal to NET_MAX_CHANNEL_ID.
	//  bytes       - Data to be queued (optional).
	//  numBytes    - Number of bytes to be queued.
	//  status      - Optional status object that can be polled
	//					for completion of the operation.
	int EstablishConnection(const EndpointId epId,
							 const ChannelId channelId,
							 const void* bytes,
							 const unsigned numBytes,
							 netStatus* status);

	//PURPOSE
	//  Synonym for EstablishConnection for GTA V.
	int OpenConnection(const EndpointId epId,
							 const ChannelId channelId,
							 const void* bytes,
							 const unsigned numBytes,
							 netStatus* status)
	{
		return EstablishConnection(epId, channelId, bytes, numBytes, status);
	}

	//PURPOSE
	//  For the fake address logic in snConnectToPeerTask::Update()
	int OpenConnection(const netPeerAddress& peerAddr,
							 const netAddress& addr,
							 const ChannelId channelId,
							 const void* bytes,
							 const unsigned numBytes,
							 netStatus* status);

	//PURPOSE
	//  Returns the id of any connection to the given endpoint,
	//  or -1 if no connection exists.
	//PARAMS
	//  epID        - EndpointId of remote peer.
	int GetAnyConnectionWithEndpointId(const EndpointId epId) const;

	//PURPOSE
	//  Returns the id of the connection to the given endpointId and with the
	//  given channel id, or -1 if the connection does not exist.
	//PARAMS
	//  epId        - Endpoint id of remote peer.
	//  channelId   - Channel id.
	int GetConnectionId(const EndpointId epId,
						const unsigned channelId) const;

    //PURPOSE
    //  Returns the id of the connection to the given address and with the
    //  given channel id, or -1 if the connection does not exist.
    //PARAMS
    //  addr        - Address of remote peer.
    //  channelId   - Channel id.
	int GetConnectionId(const netAddress& addr,
						const unsigned channelId) const;

	//PURPOSE
	//  Returns the endpointId id for the connection with the given id.
	//  Returns NET_INVALID_ENDPOINT_ID for invalid connection ids.
	EndpointId GetEndpointId(const int cxnId) const;

	//PURPOSE
	//  Returns the channel id for the connection with the given id.
	//  Returns NET_INVALID_CHANNEL_ID for invalid connection ids.
	unsigned GetChannelId(const int cxnId) const;

	//PURPOSE
	//  Returns true if the connection with the given id is open.
	bool IsOpen(const int cxnId) const;

	//PURPOSE
	//  Returns true if the connection with the given id is closed.
	bool IsClosed(const int cxnId) const;

	//PURPOSE
	//  Returns true if the connection with the given id is pending open.
	bool IsPendingOpen(const int cxnId) const;

	//PURPOSE
	//  Returns true if the connection with the given id is pending close.
	bool IsPendingClose(const int cxnId) const;

    //PURPOSE
    //  Accept an unsolicited connection request.
    //PARAMS
	//  peerAddr        - Peer address of remote endpoint.
	//  addr            - Network address of remote endpoint.
	//  channelId       - Id of channel on which connection was requested.
	//                    The value of the channel id must be less than or
	//                    equal to NET_MAX_CHANNEL_ID.
	//  seq             - Sequence number of connection request.
    //  status          - Optional status object that can be polled
    //                    for completion of the operation.
    //NOTES
    //  The addr and seq arguments should be obtained
    //  from an earlier-received netEventConnectionRequested event.
	int AcceptConnection(const netPeerAddress& peerAddr,
							const netAddress& addr,
							const ChannelId channelId,
							const netSequence seq,
							netStatus* status);

    //PURPOSE
    //  Deny an unsolicited connection request.
    //PARAMS
    //  addr            - Network address of remote endpoint.
    //  data            - Optional data sent to the peer requesting the
    //                    connection.  Typically this contains the reason
    //                    for the denial.  Can be NULL.
    //  sizeofData      - Number of bytes in the data buffer.
    //NOTES
    //  The addr argument should be obtained
    //  from an earlier-received netEventConnectionRequested event.
    bool DenyConnection(const netAddress& addr,
						const ChannelId channelId,
						const void* data,
                        const unsigned sizeofData);


	template<typename T>
	bool DenyConnection(const netAddress& addr,
						const ChannelId channelId,
						const T& msg)
	{
		u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
		unsigned size;
		return msg.Export(buf, sizeof(buf), &size)
			&& this->DenyConnection(addr, channelId, buf, size);
	}

	//PURPOSE
	//  Decrements the ref count of a connection and closes it when the
	//  ref count reaches zero.
	//PARAMS
	//  cxnId       - Id of the connection to close.
	//  closeType   - How to close the connection.
	//RETURNS
	//  True on success.
	//NOTES
	//  If the close type is GRACEFULLY, and the ref count reaches zero,
	//  the connection will enter the PENDING_CLOSE state until it has
	//  successfully sent all queued frames and pending ACKs, at which
	//  time it will enter the CLOSED state.
	//
	//  If the close type is IMMEDIATELY, and the ref count reaches zero,
	//  the connection will immediately enter the CLOSED state without
	//  sending queued frames or ACKs.
	//
	//  If there is more than one reference to the connection then it
	//  not be closed until the ref count reaches zero.
	//
	//  If the close type is FINALLY then the connection is closed
	//  regardless of the ref count.
	//
	//  When the connection is actually closed (the ref count reaches zero)
	//  a netEventConnectionClosed event will be dispatched.
    bool CloseConnection(const int cxnId,
                          const netCloseType closeType);

	//PURPOSE
	//  Gets the number of pending timeouts
	unsigned GetNumTimeoutPending();

    //PURPOSE
    //  Returns true if we have an endpoint with the given address.
    bool HasActiveEndpoint(const netAddress& addr) const;
	
    //PURPOSE
    //  Returns true if we have an endpoint with the given relay address.
    bool HasActiveEndpointWithRelayAddr(const netAddress& addr) const;

	//PURPOSE
	//  Returns true if we have an endpoint with the given peer id.
	bool HasActiveEndpointWithPeerId(const netPeerId& peerId) const;

	//PURPOSE
	//  Returns true if we have an endpoint with the given peer id,
	//  and gives back the net address, relay address, and last received
	//  time of that endpoint.
	bool GetAddrByPeerId(const netPeerId& peerId,
						   netAddress* addr,
						   netAddress* relayAddr,
						   unsigned* deltaLastReceiveTimeMs) const;

	//PURPOSE
	//  Returns true if we have an endpoint with the given peer id,
	//  and gives back the net address.
	bool GetAddrByPeerId(const netPeerId& peerId, netAddress* addr) const;

#if __BANK
	//PURPOSE
	//  Returns the endpoint id associated with the specified gamer handle.
	EndpointId GetEndpointIdByGamerHandle(const rlGamerHandle& gamerHandle) const;
#endif

	//PURPOSE
	//  Informs the Connection Manager that a new route has been
	//  found for the specified peer.
	void UpdateEndpointAddress(const netPeerId& peerId,
								const netAddress& addr,
								const netAddress& relayAddr);

    //PURPOSE
    //  Sets the global send interval.
    //PARAMS
    //  sendInterval    - Millisecond send interval
    //NOTES
    //  For each endpoint (remote peer) there is a countdown timer
    //  since the last send.  When that timer reaches zero all data
    //  destined for the endpoint is packed up and sent as a network
    //  packet.
    //
    //  Longer send intervals mean more data will be packed into each
    //  packet (reducing header overhead), but result in higher latencies.
    //
    //  The default send interval is zero.
    //
    //  If the send interval is non-zero and a frame needs to be sent
    //  immediately, pass the NET_SEND_IMMEDIATE flag to
    //  netConnectionManager::Send()
    void SetGlobalSendInterval(const unsigned sendInterval);

    //PURPOSE
    //  Returns the global send interval.
    unsigned GetGlobalSendInterval() const;

    //PURPOSE
    //  Enable/disable compression on outbound packets.
    //NOTES
    //  Instead of a per channel, or per connection policy, this
    //  is a global setting because there might be frames from
    //  multiple channels contained in a single packet.
    void SetCompressionEnabled(const bool enabled);

    //PURPOSE
    //  Returns true if packet compression is enabled.
    bool IsCompressionEnabled() const;

	//PURPOSE
	//  Returns true if packet compression is enabled for the specified channel.
	bool IsCompressionEnabled(const ChannelId channelId) const;

	//PURPOSE
	// enable/disable logic that cancels pending tunnel requests that match active endpoints -
	// leaving code in place for now but disabled by default.
	static void SetCancelTunnelsMatchingActiveEndpoints(const bool cancelTunnelsMatchingActiveEndpoints);

    //PURPOSE
    //  Sets the policies on the given channel.  All existing and new
    //  connections on the given channel will use the new policies.
    //PARAMS
    //  channelId       - Id of channel.
    //  polices         - Channel policies.
    void SetChannelPolicies(const unsigned channelId,
                            const netChannelPolicies& policies);

    //PURPOSE
    //  Retrieves the policies for the given channel.
    //PARAMS
    //  channelId       - Id of channel.
    //  polices         - Channel policies.
    void GetChannelPolicies(const unsigned channelId,
                            netChannelPolicies* policies) const;

    //PURPOSE
    //  Sets the default policies for all new connections on the given
    //  channel.  Policies on existing connections are not changed.
    //PARAMS
    //  channelId   - Id of channel for which default policies will be set.
    //  policies    - Default policies used for the channel.
    void SetDefaultConnectionPolicies(const unsigned channelId,
                                    const netConnectionPolicies& policies);

    //PURPOSE
    //  Sets the default policies for all new connections on all channels.
    //  Policies on existing connections are not changed.
    //PARAMS
    //  policies    - Default policies used for all channels.
    void SetDefaultConnectionPolicies(const netConnectionPolicies& policies);

    //PURPOSE
    //  Retrieves the default policies for the given channel.
    //PARAMS
    //  channelId   - Id of channel for which default policies will be retrieved.
    //  policies    - On return will contain default policies for the channel.
    void GetDefaultConnectionPolicies(const unsigned channelId,
                                    netConnectionPolicies* policies) const;
	
    //PURPOSE
    //  Sets the policies for the given connection.
    //PARAMS
    //  cxnId       - Id of connection.
    //  policies    - The policies.
    //RETURNS
    //  True on success.
    bool SetConnectionPolicies(const int cxnId,
                            const netConnectionPolicies& policies);

    //PURPOSE
    //  Sets the policies for all existing connections.
    //PARAMS
    //  policies    - The policies.
    //RETURNS
    //  True on success.
    bool SetConnectionPolicies(const netConnectionPolicies& policies);

    //PURPOSE
    //  Retrieves the policies for the given connection.
    //PARAMS
    //  cxnId       - Id of connection.
    //  policies    - The policies.
    //RETURNS
    //  True on success.
    bool GetConnectionPolicies(const int cxnId,
                            netConnectionPolicies* policies) const;

	//PURPOSE
	//	Sets the P2P key and salt which is used in addition to
	//  the secret key agreed upon during P2P key exchange.
	void SetP2pKeySalt(const netP2pCrypt::HmacKeySalt& p2pKeySalt);

    //PURPOSE
#if !__FINAL
    //PURPOSE
    //  Disables removal of inactive end points (for debugging purposes as game connections
    //  can be dropped even if the connection policies specify no timeouts if one machine is paused
    //  for too long (i.e. on a debug breakpoint)
	void SetDisableInactiveEndpointRemoval(const bool disable);
#endif //!__FINAL

    //PURPOSE
    //  Registers a packet recorder to a channel.  It will record all
    //  inbound/outbound bundles for all connections on the channel.
    //PARAMS
    //  channelId   - Channel id.
    //  recorder    - Packet recorder.
    //NOTES
    //  The recorder's Record*() methods are called every time
    //  a bundle is sent or received.
    //
    //  Do not call Update() on a recorder registered with a
    //  channel.  netConnectionManager will do that.
    //
    //  Register a netBandwidthRecorder to measure inbound/outbound
    //  bandwidth usage on a channel.
    //
    //  Outbound bundles are recorded prior to compression, and
    //  inbound bundles are recorded post decompression.  Bandwidth
    //  recorders, therefore, will not have an accurate measure of the
    //  actual bandwidth used by the channel.
    void RegisterChannelPacketRecorder(const ChannelId channelId,
                                       netPacketRecorder* recorder);

    //PURPOSE
    //  Unregisters a packet recorder from a channel.
    //PARAMS
    //  channelId   - Channel id.
    //  recorder    - Packet recorder.
    void UnregisterChannelPacketRecorder(const ChannelId channelId,
                                         netPacketRecorder* recorder);

	//PURPOSE
	//  Allocates memory for a netOutFrame with a payload of a
	//  specified size. The purpose of this being publicly
	//  exposed is to allow external systems to allocate a
	//  buffer, fill it in, and send it to a remote peer
	//  without the connection manager needing to copy the
	//  buffer to another buffer before sending it, thus
	//  saving CPU time. Once the netOutFrame is filled in, pass it
	//  to the connection manager via Send() with the
	//  NET_SEND_OUT_FRAME flag enabled. The connection manager
	//  takes ownership of the buffer and will destroy it when needed.
	//PARAMS
	//  sizeofPayload   - The size in bytes of the payload.
	//  sendFlags		- Bits from netSendFlags enum.
	netOutFrame* AllocOutFrame(const unsigned sizeofPayload,
							   const unsigned sendFlags);

	//PURPOSE
	//  Frees a netOutFrame allocated with AllocOutFrame().
	//  This should only be called if the netOutFrame was allocated
	//  but never sent to the connection manager via Send().
	void FreeOutFrame(netOutFrame* outfrm);

    //PURPOSE
    //  Queues a frame of data onto the the connection's outbound queue.
    //  A non-NULL seq will return with the sequence number of the
    //  queued frame.
    //PARAMS
    //  channelId   - Id of connection on which to send.
    //  bytes       - Data to be queued.
    //  numBytes    - Number of bytes to be queued.
    //  sendFlags   - Bits from netSendFlags enum.
    //  frameSeq    - Optional.  Set to sequence number of queued frame.
    //RETURNS
    //  True on success.
    //NOTES
    //  If frameSeq is not NULL it will receive the sequence number of
    //  the frame that is sent.  If the frame is sent reliably, and frameSeq
    //  is non-NULL, a NET_EVENT_ACK_RECEIVED event will be dispatched when
    //  the frame is ACKed.
    //
    //  In low memory situations outbound unreliable frames will be dropped.
    //
    //  Reliable frames won't be dropped until an attempt is made to reclaim
    //  memory by freeing already-queued unreliable frames.
    //
    //  In both cases, if there is not enough memory to queue the frame,
    //  Send() will return false.
    bool Send(const int cxnId,
				const void* bytes,
				const unsigned numBytes,
				const unsigned sendFlags,
				netSequence* frameSeq);

    template<typename T>
    bool Send(const int cxnId,
                const T& msg,
                const unsigned sendFlags,
                netSequence* frameSeq)
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
            && this->Send(cxnId, buf, size, sendFlags, frameSeq);
    }

    //PURPOSE
    //  Sends out of band data, i.e. outside of any connection.
    //PARAMS
    //  addr        - Network address of recipient.
    //  channelId   - Id of channel on which to send.
    //  bytes       - Data to be sent.
    //  numBytes    - Number of bytes to be sent.
    //  sendFlags   - Bits from netSendFlags enum.  All flags are ignored
    //                except NET_SEND_IMMEDIATE.
    //RETURNS
    //  True on success.
    //NOTES
    //  In low memory situations out-of-band frames will be dropped.
    bool SendOutOfBand(const netAddress& addr,
                        const ChannelId channelId,
                        const void* bytes,
                        const unsigned numBytes,
                        const unsigned sendFlags);

    template<typename T>
    bool SendOutOfBand(const netAddress& addr,
                        const ChannelId channelId,
                        const T& msg,
                        const unsigned sendFlags)
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
            && this->SendOutOfBand(addr, channelId, buf, size, sendFlags);
    }

    bool SendOutOfBand(const EndpointId endpointId,
                        const ChannelId channelId,
                        const void* bytes,
                        const unsigned numBytes,
                        const unsigned sendFlags)
	{
		const netAddress& addr = this->GetAddress(endpointId);
		if(addr.IsValid())
		{
			return SendOutOfBand(addr, channelId, bytes, numBytes, sendFlags);
		}

		return false;
	}

    template<typename T>
    bool SendOutOfBand(const EndpointId endpointId,
                        const ChannelId channelId,
                        const T& msg,
                        const unsigned sendFlags)
    {
		const netAddress& addr = this->GetAddress(endpointId);
		if(addr.IsValid())
		{
			return SendOutOfBand(addr, channelId, msg, sendFlags);
		}

		return false;
	}

	//PURPOSE
	//  Sends a tunneling packet over the tunneling channel.
    bool SendTunnelingPacket(const netAddress& addr,
							 const void* bytes,
							 const unsigned numBytes,
							 const netP2pCryptContext& p2pCryptContext);

    template<typename T>
    bool SendTunnelingPacket(const netAddress& addr,
							 const T& msg,
							 const netP2pCryptContext& p2pCryptContext)
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
            && this->SendTunnelingPacket(addr, buf, size, p2pCryptContext);
    }

	//PURPOSE
	//  Sends a packet over the reserved channel. These are sent the same way
	//  as tunneling packets, which are only encrypted using the remote peer's
	//  key. This is so we can send relay address changed messages, where the
	//  remote peer wouldn't otherwise know how to decrypt the packet since
	//  it will be coming from a new address.
    bool SendReservedChannelPacket(const netAddress& addr,
									const void* bytes,
									const unsigned numBytes,
									const netP2pCryptContext* p2pCryptContext);

    template<typename T>
    bool SendReservedChannelPacket(const netAddress& addr,
									const T& msg,
									const netP2pCryptContext* p2pCryptContext)
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
            && this->SendReservedChannelPacket(addr, buf, size, p2pCryptContext);
    }

	//PURPOSE
	//  Sends a timeout request packet 
	//	The result will be that we are timed on on this connection Id from the remote side
	//PARAMS
	//  epId          - Id of the remote endpoint to which to send.
	//  channelId     - channelId that timed out.
	//  timeoutReason - Specific cause of the timeout.
	void SendRemoteTimeoutRequest(const EndpointId epId, const ChannelId channelId, const netConnection::TimeoutReason timeoutReason);

#if 0
    void SendRemoteTimeoutRequestToAllEndpoints(const netConnection::TimeoutReason timeoutReason);
#endif

	//PURPOSE
	//  Returns the network address of the endpoint with the given id.
	//  For invalid ids the address will be invalid.
	const netAddress& GetAddress(const EndpointId epId) const;
	const netAddress& GetAddress(const netPeerId& peerId) const;

	//PURPOSE
	//  Returns the relay address of the endpoint with the given id.
	//  For invalid endpoint ids the address will be invalid.
	const netAddress& GetRelayAddress(const EndpointId epId) const;
	const netAddress& GetRelayAddress(const netPeerId& peerId) const;

	//PURPOSE
	//  Returns the peer id of the endpoint with the given id.
	//  For invalid endpoint ids the peerId will be invalid.
	const netPeerId& GetPeerId(const EndpointId epId) const;

	//PURPOSE
	//  Returns the peer address of the endpoint with the given id.
	//  For invalid endpoint ids the peer address will be invalid.
	const netPeerAddress& GetPeerAddress(const EndpointId epId) const;

	//PURPOSE
	//  Returns the number of hops along the route to the specified peer.
	//  Direct connection = 1 hop, relay server = 2 hops, peer relay >= 2 hops.
	u8 GetHopCount(const EndpointId epId) const;

	//PURPOSE
	// Returns the number of connections currently being relayed by the local peer.
	//NOTES
	// In this case, a connection is considered to be unilateral. If the local peer B is relaying 
	// from A -> B -> C and from C-> B -> A, the local peer would be relaying two connections.
	unsigned GetNumForwardedConnections() const;

#if NET_CXNMGR_COLLECT_STATS
	//PURPOSE
	//  Returns a snapshot of the current stats for a given endpoint.
	const netEndpoint::Statistics GetStats(const EndpointId epId);
#endif

    //PURPOSE
    //  Returns number of reliable frames on the channel that have not been ACKed
    unsigned GetUnAckedCount(const int cxnId) const;

    //PURPOSE
    //  Returns the resend count of the oldest un-ACKed frame on the channel
    //  Returns zero if there are no un-ACKed frames
    unsigned GetOldestResendCount(const int cxnId) const;

    //PURPOSE
    //  Returns the sequence number of the oldest un-ACKed frame on the channel
    //  Returns zero if there are no un-ACKed frames
    netSequence GetSequenceOfOldestResendCount(const int cxnId) const;

    //PURPOSE
    //  Returns the current length of the new message send queue for the 
	//  specified connection.
    unsigned GetNewQueueLength(const int cxnId) const;

	//PURPOSE
	//  Returns the current length of the resend queue for the specified
	//  connection.
	unsigned GetResendQueueLength(const int cxnId) const;

	//PURPOSE
	//  Returns the age of the oldest unacked frame across all channels, in milliseconds.
	//  Returns zero if there are no un-ACKed frames
	unsigned GetAgeOfOldestUnackedFrame(const EndpointId epId) const;

	//PURPOSE
	//  Returns a measure of the connection quality of the specified endpoint.
	//  epId        - The ID of the endpoint whose current QoS will be obtained.
	//  reason		- When QOS_BAD is returned, this gives the reason for the bad QoS.
	netEndpoint::EndpointQoS GetQoS(const EndpointId epId, netEndpoint::EndpointQosReason& reason);

	//PURPOSE
	//  Returns a measure of the overall quality of service of the local peer.
	//  reason		- When QOS_BAD is returned, this gives the reason for the bad QoS.
	OverallQoS GetOverallQoS(OverallQosReason& reason) const;

	//PURPOSE
	//  Resets the pack timeout for the specified endpoint to its send interval.
	//  This is used by unit tests to control when a packet is sent.
	bool ResetPackTimeout(const EndpointId epId);

	//PURPOSE
	//  Returns true if the specified channel has exceeded its bandwidth;.
	bool CheckExceededBandwidth(const ChannelId channelId);

	//PURPOSE
	//  Adds a delegate to the collection of delegates that will be called
	//  with connection events.
	//PARAMS
	//  dlgt        - The delegate.
	//  channelId   - Id of the channel for which the delegate will receive
	//                events.
	//NOTES
	//  The channel id acts as a filter.  Only events related to channels
	//  with a matching channel id will be delivered to the delegate.
	void AddChannelDelegate(Delegate* dlgt, const ChannelId channelId);

	//PURPOSE
	//  Removes a channel delegate.
	void RemoveChannelDelegate(Delegate* Delegate);

    //PURPOSE
    //  Adds a network event consumer.
    //  Periodically call NextEvent() on the consumre to retrieve
    //  network events.
    bool Subscribe(netEventConsumer<netEvent>* consumer);

    //PURPOSE
    //  Removes a network event consumer.
    bool Unsubscribe(netEventConsumer<netEvent>* consumer);

    //PURPOSE
    //  Returns number of active connections.  An active connection is a
    //  connection that is not closed, i.e. open, pending open, or pending
    //  close.
    unsigned GetActiveConnectionCount() const;

    //PURPOSE
    //  Packs up a single packet's worth of data.
    //PARAMS
    //  recipient   - On return contains the address of the packet recipient.
    //  buf         - The buffer that will contain the packet.
    //  sizeofBuf   - Maximum size of the buffer.
    //RETURNS
    //  Total number of bytes packed into the buffer.  This total
    //  includes encrypted and plaintext, but if the buffer is cast to
    //  a netPacket and queried for it's size it will only return
    //  the size of the encrypted portion.  This is to accommodate
    //  Xenon's VDP protocol.
    unsigned Pack(netAddress* recipient,
                    void* buf,
                    const unsigned sizeofBuf);

    //PURPOSE
    //  Unpacks a single packet's worth of data.
    //PARAMS
    //  sender      - Packet sender.
    //  buf         - The packet.
    //  sizeofBuf   - Number of bytes in the packet.
	//  allowRelayOverride - Allow switching the receiving endpoint to an
	//						 endpoint that uses the relay server
    void Unpack(const netAddress& sender,
                const void* buf,
                const unsigned sizeofBuf,
				const bool allowRelayOverride);

    //PURPOSE
    //  Reclaims non-critical memory, e.g. memory allocated for
    //  unreliable messages.
    void ReclaimMem();

    //PURPOSE
    //  Dumps contents of outbound/inbound/event queues to debug output.
    void DumpStats();

    //PURPOSE
    //  Provide a way to retrieve the current delta time from the last received time.
    //  This can be used to protect the game against lag-hacking (standbying).
    unsigned GetDeltaLastReceiveTime() const;
	unsigned GetDeltaLastReceiveTime(const EndpointId endpointId) const;

	//PURPOSE
	//  Returns the time at which the given endpoint's address was last assigned/changed.
	unsigned GetLastAddressAssignmentTime(const EndpointId endpointId) const;

	//PURPOSE
	//  Access to send / receive frame counts
	unsigned GetSendFrame() const;
	unsigned GetPackFrame() const;
	unsigned GetReceiveFrame() const;
	unsigned GetUnpackFrame() const;
	unsigned GetLastPackFrameSent() const;
	unsigned GetRecvPackFrameMark() const;

	//PURPOSE
	//  Provide a way to retrieve the current delta time from the last main thread update time.
	//  This can be used to protect the game against main thread suspensions.
	unsigned GetDeltaLastMainThreadUpdateTime() const;
	bool HasValidLastMainThreadUpdateTime() const { return m_LastMainThreadUpdateTime > 0; }

    //PURPOSE
    //  Returns true if Init() has been called.
    bool IsInitialized() const;

	//PURPOSE
	//  Returns the allocator that is set within the connection manager, used by the worker to set the allocators appropriately underneath
	sysMemAllocator* GetAllocator() { return m_Allocator; }

	//PURPOSE
	//  Returns the CxnMgr's critical section token, for subsystems that need to share the same token.
	netCriticalSectionToken& GetCriticalSectionToken() { return m_Cs; }

	// PURPOSE
	//	Returns the CxnMgr's memory watermark
	sysMemoryWatermark& GetMemoryWatermark() { return m_MemoryWatermark; }

	//PURPOSE
	//  Returns the immediate callback called when an oom happens
	netOutOfMemoryCallabck& GetOutOfMemoryCallback() { return m_OutOfMemoryCallback; }

#if __BANK
	void ForceTimeOutAllConnections();
	void ForceOutOfMemory();
	void ForceCloseConnectionlessEndpoints();
#endif

	void GetMaxEndpointStats(const ChannelId channelId, unsigned& unacked, unsigned& oldestResend, unsigned& newQueueLength, unsigned& resendQueueLength, unsigned& ageOfOldestUnackedFrame) const;

private:
#if RSG_BANK
	void BankEstablishConnection(const bool wantAccept);
	void BankListenForConnections();
	void BankStopListeningForConnections();
	void BankTerminateConnection();
	void BankSendMessage();
	void BankOpenTunnel();
	void AddWidgets();
#endif

	bool IsTunnelingPacket(netEndpoint* ep,
						   const void* buf,
						   const unsigned sizeofBuf);

	bool HandlePeerRelayForwardPacket(const netSocketAddress& sender, netPeerRelayPacket* relayPkt);
	bool HandlePeerRelayTerminusPacket(const netSocketAddress& sender, netPeerRelayPacket* relayPkt);
	bool HandlePeerRelayPacket(const netSocketAddress& from, void* pktBuf, const int len);
	void HandleReceivedPacket(const netAddress& sender, const void* encodedBuf, const unsigned sizeofEncodedBuf);
	bool P2pEncrypt(const netAddress& addr,
					const void* data,
					const unsigned int sizeOfData,
					u8* encodedData,
					unsigned int& sizeOfEncodedData,					
					const netP2pCryptContext* p2pCryptContext);

	bool P2pDecrypt(const netAddress& addr,
					u8** buf,
					unsigned int& sizeOfBuf,
					bool& allowRelayOverride);

    bool SendConnectionless(const netAddress& addr,
                            const ChannelId channelId,
                            const void* bytes,
                            const unsigned numBytes,
                            const unsigned sendFlags);

    //PURPOSE
    //  Packs up connectionless data.
    //PARAMS
    //  encrypted   - The buffer that will contain the packet's encrypted bytes.
    //  numEncryptedBytes   - On return the number of encrypted bytes.
    //  plaintext   - The buffer that will contain the packet's plaintext bytes.
    //  numPlaintextBytes   - On return the number of plaintext bytes.
    //  maxBytes    - Maximum number of bytes (encrypted or plaintext)
    //                that should be packed.
    //RETURNS
    //  Total number of bytes packed.
    unsigned PackConnectionless(const netAddress& addr,
                                netEndpoint::CxnlessQueue* queue,
                                void* encrypted,
                                unsigned* numEncryptedBytes,
                                void* plaintext,
                                unsigned* numPlaintextBytes,
                                const unsigned maxBytes);

	//PURPOSE
	//  Allocates memory from the connection manager's allocator.
	//  Returns NULL if the allocation could not be satisfied.
	void* Alloc(const unsigned size);

	//PURPOSE
	//  Allocates memory from the connection manager's allocator.
	//  Attempts to reclaim memory if we're low on memory.
	//  Returns NULL if the allocation could not be satisfied.
	void* AllocCritical(const unsigned size);

    //PURPOSE
    //  Collect all queued frames from all connections into packets and
    //  send them to the appropriate remote peers.
    bool Send();

	void CheckForReceiveTimeouts();

    //PURPOSE
    //  Dispatches queued connection events to delegates registered
    //  with the connection manager.
    void Dispatch();

    //PURPOSE
    //  Update pending tunnel requests.
    void UpdateTunnelRequests();

    netEndpoint* CreateEndpoint(const netPeerAddress& peerAddr,
										 const netAddress& addr,
                                         const netAddress& relayAddr);

	/*
		Endpoints are mapped by netAddress and netPeerId. If a new endpoint succeeds that would
		conflict with an existing endpoint, we need to resolve the conflict in a reasonable manner.

		Case	Have Addr	Have PeerId		Same Ep		Conflict Resolution
		----	---------	-----------		--------	-------------------
		1		0			0				-			No conflict. Create new ep.
		2		1			0				-			Player may have relaunched with the same address. Use existing ep, updated with new info.
		3		0			1				-			Player may have reconnected with a different address. Reassign existing ep to new address.
		4		1			1				0			Unknown resolution. Fail tunnel request.
		5		1			1				1			Player reconnected with the same address and peerId. Use existing ep.
	*/
	enum netEndpointResolution
	{
		Resolution_CreateNew,
		Resolution_Reconnect_SameDetails,
		Resolution_Reconnect_NewPeerId,
		Resolution_Reconnect_NewAddress,
		Resolution_Reconnect_SwitchedToRelay,
		Resolution_Failed,
	};

	netEndpointResolution ResolveNewEndpointRequest(
		const netPeerAddress& peerAddr,
		const netPeerId& peerId,
		const netAddress& addr,
		const netAddress& relayAddr,
		netEndpoint** existingEp_Out
		OUTPUT_ONLY(, const char* callingFunction));

	netAddress StripRelayToken(const netAddress& addrOrig);

	Cxn* CreateConnection(netEndpoint* ep, const unsigned channelId);

    //PURPOSE
    //  Reassigns new addresses to the endpoint.
    //  The state of the endpoint remains unchanged.
    bool ReassignEndpoint(netEndpoint* ep,
                            const netAddress& addr,
                            const netAddress& relayAddr);

    void DestroyEndpoint(netEndpoint* ep);

	bool CloseConnection(Cxn* cxn,
						 const netCloseType closeType);

	bool DestroyConnection(Cxn* cxn);

    //PURPOSE
    //  Moves an endpoint from the pending list to the active list and
    //  cancels the pending tunnel request.
    //  This is used when we're tunneling to a remote peer and the receive
    //  a packet from that peer.  At that point we don't need to continue
    //  tunneling.
    netEndpoint* ActivatePendingEndpoint(netEndpoint* ep,
													const netPeerAddress& peerAddr,
                                                    const netAddress& addr,
                                                    const netAddress& relayAddr);

    netEndpoint* AllocEndpoint();
    void FreeEndpoint(netEndpoint* ep);

	//Retrieves an endpoint by id
	netEndpoint* GetEndpointById(const EndpointId epId);
	const netEndpoint* GetEndpointById(const EndpointId epId) const;

    //Retrieves an active endpoint by address
    netEndpoint* GetActiveEndpointByAddr(const netAddress& addr);
    const netEndpoint* GetActiveEndpointByAddr(const netAddress& addr) const;

	//Retrieves an active endpoint by netPeerId
	netEndpoint* GetActiveEndpointByPeerId(const netPeerId& peerId);
	const netEndpoint* GetActiveEndpointByPeerId(const netPeerId& peerId) const;

	//Retrieves an active endpoint by relay address
	netEndpoint* GetActiveEndpointByRelayAddr(const netAddress& addr);

#if __BANK
	//Retrieves an active endpoint by gamer handle
	const netEndpoint* GetActiveEndpointByGamerHandle(const rlGamerHandle& gamerHandle) const;
#endif

    //Retrieves a pending endpoint by address.
    //A pending endpoint is one which is in the midst of tunneling
    //to the remote peer.  It's possible for the remote peer to send
    //us a packet while we're still tunneling.
    //The endpoint is found by matching the passed-in address with either the
    //endpoint's direct address (which is likely invalid due to an incomplete
    //tunnel request) or the endpoint's relay address.
    netEndpoint* GetPendingEndpointByAddr(const netAddress& addr);

	Cxn* GetCxnById(const int id);
	const Cxn* GetCxnById(const int id) const;
	Cxn* GetCxnByChannelId(const netAddress& addr, const unsigned channelId);
	const Cxn* GetCxnByChannelId(const netAddress& addr, const unsigned channelId) const;
	Cxn* GetCxnByChannelId(const EndpointId epId, const unsigned channelId);
	const Cxn* GetCxnByChannelId(const EndpointId epId, const unsigned channelId) const;

	netPacket* CompressPacket(netPacket* pkt,
                                void* buf,
                                const unsigned sizeofBuf) const;

    const netPacket* DecompressPacket(const netPacket* pkt,
                                    void* buf,
                                    const unsigned sizeofBuf) const;

#if NET_CXNMGR_COLLECT_STATS
	void AddSentPacketStats(const netAddress& addr,
							const unsigned sizeofPkt,
							const int bytesSavedByCompression,
							const unsigned size);
#endif

    netSocketError SendPacket(const netAddress& addr,
                                netPacketStorage* storagePkt,
                                const unsigned sizeofPlaintext,
								const int bytesSavedByCompression,
								const netP2pCryptContext* p2pCryptContext);

	bool QueueCxnRequested(const netAddress& sender,
							const ChannelId channelId,
                            const netFrame* frame);

    bool QueueOobFrameReceived(const EndpointId epId,
                                const ChannelId channelId,
								const netAddress& sender,
								const void* payload,
                                const unsigned sizeofPayload);

    void QueueEvent(netEvent* e);

	void DispatchEvent(netEvent* e);

    //This is only called when there is a fatal mem allocation error.
    void QueueOutOfMemory(Cxn* cxn, bool isFatal, size_t attemptedAllocation);

	void SendRelayAddressChangedMsg(const netPeerAddress& peerAddr, const netAddress& addr, const MsgCxnRelayAddrChanged* msg);

	void OnSocketEventPacketReceived(const netSocketEvent& event);
	void OnSocketEventReceiveThreadTicked(const netSocketEvent& event);

	void OnRelayEventPacketReceived(const netRelayEvent& event);
	void OnRelayEventAddressChanged(const netRelayEvent& event);
	static void OnSysOnServiceEvent(sysServiceEvent* evnt);

	bool HandleBundle(const netAddress& sender,
					  const void* payload,
					  const unsigned sizeofPayload);

	void HandleSuspension();

	typedef netEndpoint::CxnList CxnList;
	typedef netEndpoint::CxnlessQueue CxnlessQueue;
	typedef inlist<netEndpoint, &netEndpoint::m_ListLink> EndpointList;
	typedef inmap<EndpointId, netEndpoint, &netEndpoint::m_ByEndpointIdLink> EndpointsByEndpointId;
    typedef inmap<netAddress, netEndpoint, &netEndpoint::m_ByAddrLink> EndpointsByAddr;
	typedef inmap<netPeerId, netEndpoint, &netEndpoint::m_ByPeerIdLink> EndpointsByPeerId;
	typedef inlist<Delegate, &Delegate::m_ListLink> ChannelDlgtList;

    //PURPOSE
    //  Performs sends in a separate thread.
    class Worker
    {
    public:
        Worker();

        bool Init(netConnectionManager* cxnMgr,
                  const int cpuAffinity);

        void Shutdown();

        static void Send(void* p);

        netConnectionManager* m_CxnMgr;
        sysIpcSema m_SendWaitThread;
        sysIpcSema m_SendSema;
        sysIpcThreadId m_SendThreadHandle;
        sysIpcCurrentThreadId m_SendThreadId;

        bool m_SendDone : 1;

		netTimeStep m_SendTimeStep;
    };

    class Channel
    {
    public:

        Channel();

        void Init(const ChannelId channelId);

        void Shutdown();

        void Update(const netTimeStep& timeStep);

        void RecordOutbound(const netAddress& addr,
                            const void* bytes,
                            const unsigned numBytes);

        void RecordInbound(const netAddress& addr,
                            const void* bytes,
                            const unsigned numBytes);

        void AddDelegate(Delegate* dlgt);

        void RemoveDelegate(Delegate* dlgt);

        void DispatchEvent(netConnectionManager* cxnMgr, netEvent* e);

        ChannelId m_ChannelId;

        //Used to limit outbound bandwidth.  When negative, don't send
        //any frames.
        //
        //Bandwidth limiters use milliseconds to compute bandwidth used.  But
        //the bandwidth limit policy is in bytes/second.  Therefore all
        //calculations involved in limiting bandwidth are scaled by 1000.
        int m_OutboundLimiter;

        //Channel policies
        netChannelPolicies m_Policies;

		//Default policies.  Given to new connections as they are opened.
		netConnectionPolicies m_CxnPolicies;

        //Packet recorders registered on channels.
		PktRecorderList m_PktRecorders;

        //List of delegates registered with the channel
        ChannelDlgtList m_Dlgts;

        unsigned m_Proc;

        //Set when bandwidth is exceeded, cleared when bandwidth is
        //within limits.
        bool m_BandwidthExceeded    : 1;
    };

	static bool	sm_bAcquireCritSecForFullMainUpdate;
	static bool sm_EnableDelegate;
	static bool sm_MetricEnabled;
	static bool sm_CancelTunnelsMatchingActiveEndpoints;
	static bool sm_EnableOverallQos;

    netSocketManager* m_SocketManager;

    sysMemAllocator* m_Allocator;

    //Pile of endpoints
    netDynArray<netEndpoint, ENDPOINT_ALLOC_STEP, MAX_ENDPOINTS_IN_POOL> m_EpPile;

    //Pool of endpoints.
    EndpointList m_EpPool;

    //List of active endpoints.  These are endpoints on which
    //packets have recently been transmitted.
    EndpointList m_ActiveEndpoints;

	//Active endpoints indexed by endpoint id.
	EndpointsByEndpointId m_EndpointsByEndpointId;

	//Active endpoints indexed by network address.
	EndpointsByAddr m_EndpointsByAddr;

	//Active endpoints indexed by peer id.
	EndpointsByPeerId m_EndpointsByPeerId;

    //List of endpoints that are pending open tunnels.
    EndpointList m_PendingEndpoints;

	//Pile of connections
	netDynArray<Cxn, 4, MAX_CXNS_IN_POOL> m_CxnPile;

	//Pool of available (closed) connections.
	CxnList m_CxnPool;

	// Finds routes to remote peers through the p2p mesh
	netConnectionRouter m_Router;

	//FIXME (KB) - find a solution for deadlocks.
    //Crit section used to protect access to the delegate collection.
    //We use a separate crit section here to help avoid deadlocks,
    //though there is still the potential for a deadlock if a delegate
    //is invoked on a locked object in one thread while that object tries
    //to add/remove delegates in another thread.
    netCriticalSectionToken m_CsDlgt;

    netEventQueue<netEvent> m_EventQ;

    //Used temporarily to bridge between the old callback/delegate
    //mechanism, and the new producer/consumer mechanism.  The delegate
    //interface should be considered deprecated.
    //m_EventConsumer pulls events from the queue and pushes them to delegates.
    netEventConsumer<netEvent> m_EventConsumer;

    Channel m_Channels[NET_MAX_CHANNELS];

    //Sends on a separate thread.
    Worker m_Worker;

    mutable netCriticalSectionToken m_Cs;

    //Global send interval.
    unsigned m_SendInterval;
    //Minimum send interval of all endpoints
    unsigned m_MinSendInterval;

    //Used to open tunnels to remote peers.  Performs
    //NAT negotiation and key exchange.
    netTunneler m_Tunneler;

	netCompression m_Compression;

	//Callback registered with netSocketManager to call us when a
	//packet is received.
	netSocketManager::Delegate m_SocketDelegatePacketReceived;
	netSocketManager::Delegate m_SocketDelegateReceiveThreadTicked;

    //Callback registered with netRelay to call us when a
    //relay packet is received.
	netRelay::Delegate m_RelayDelegatePacketReceived;
	netRelay::Delegate m_RelayDelegateAddressChanged;

	//Callback registered with service to call us with
	//PLM status changes.
	static ServiceDelegate sm_ServiceDelegate;
	
    //Number of endpoints using the relay server.
    int m_NumRelayEndpoints;

    //Bit array tracking channels on which we're listening for
    //unsolicited connections.
    atFixedBitSet<NET_MAX_CHANNELS> m_IsListening;

	// memory watermarking
	sysMemoryWatermark m_MemoryWatermark;

	// out of memory callback
	netOutOfMemoryCallabck m_OutOfMemoryCallback;

#if RSG_BANK
	char m_BankGamerHandle[RL_MAX_GAMER_HANDLE_CHARS];
	char m_BankChannels[32];
#endif

	netRandom m_Rng;
	netTimeStep m_TimeStep;
	unsigned m_SendFrame;
	unsigned m_PackFrame;
	unsigned m_ReceiveFrame;
	unsigned m_UnpackFrame;
	unsigned m_LastPackFrameSent; 
	unsigned m_RecvPackFrameMark; 

	//The last time data was received from any connections in timestep time.
	unsigned m_LastReceiveTime;

	//The last time the main thread called Update().
	//Used to detect a main thread hang so we can stop sending keepalives when we're not alive.
	unsigned m_LastMainThreadUpdateTime;

	//The last time we checked for receive timeouts
	unsigned m_LastReceiveTimeoutCheckTime;

    bool m_IsInitialized                    : 1;
    //True if packet compression is enabled (set to DEFAULT_COMPRESSION_ENABLED
    //in Init()).
    bool m_EnableCompression                : 1;
    //True if multi-threaded.
    bool m_IsMT                             : 1;
    //If true one of the connections timed out; dispatch a netEventConnectionError
    bool m_DispatchTimedOut                 : 1;
	// If enabled, the allocator will keep track of a memory layer for the connection mgr.
	bool m_IsMemoryTracking					: 1;
#if !__FINAL
	//If true in-active endpoints will no be removed (intended for debugging purposes)
	bool m_DisableInactiveEndpointRemoval   : 1;
#endif // !__FINAL

    netConnectionManager(const netConnectionManager&);
    netConnectionManager& operator=(const netConnectionManager&);
};

extern netConnectionManager* g_CxnMgr;

}   //namespace rage

#endif  //NET_CONNECTIONMGR_H
