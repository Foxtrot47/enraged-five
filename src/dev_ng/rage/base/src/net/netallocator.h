//
// net/netallocator.h
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#ifndef NET_MEM_ALLOCATOR_H
#define NET_MEM_ALLOCATOR_H

#include "net.h"
#include "system/fixedallocator.h"
#include "system/memory.h"
#include "system/simpleallocator.h"

namespace rage
{

//PURPOSE
//  Memory allocator for the connection manager.
//  This allocator is optimized for lots of small allocations <= 1024 bytes (eg. netFrames, netPackets, etc.)
//  It reduces external fragmentation by using a fixed allocator for small allocations, and a simple allocator
//  for larger allocations. The tradeoff is that we waste some memory due to allocating fixed size chunks
//  >= the requested allocation size. Because frames/packets are typically short-lived, this wasted memory
//  is quickly recovered as chunks are put back in the pool after packets are sent/acked.
class netMemAllocator : public sysMemAllocator
{
	friend class netAllocationProfiler;
public:
	// sysMemFixedAllocator supports up to 255 chunks in each bucket.
	const static unsigned MAX_FIXED_BUCKETS = 12;
	const static unsigned MAX_FIXED_CHUNKS_PER_BUCKET = 255;
	enum AllocReason
	{
		ALLOC_OTHER = 0,
		ALLOC_CXN_MGR_OUTFRM_UNRELIABLE,
		ALLOC_CXN_MGR_OUTFRM_RELIABLE,
		ALLOC_CXN_OUTFRM_UNRELIABLE,
		ALLOC_CXN_OUTFRM_RELIABLE,
		ALLOC_CXN_INFRM,
		ALLOC_CXN_MGR_EVENT,
		ALLOC_CXNLESS_BUNDLE,
		ALLOC_OOB_FRM_RCVD,
	};

	// PURPOSE:	Set up the allocator with the specified fixed size buckets.
	// PARAMS:	
	//  heap				- Pointer to memory we should manage.
	//	heapSize			- The number of bytes in the heap.
	//	heapId				- Passed to the sub allocators.
	//	numFixedBuckets		- number of buckets the allocator should support.
	//	fixedChunkSizes		- an array of bucket sizes.
	//	fixedChunkCounts	- the number of chunks in each bucket.
	// NOTE:
	//  the total of all size*counts must be < heapSize.
	//  If numFixedBuckets = 0, then the fixed allocator is disabled.
	netMemAllocator(void* heap,
	                const int heapSize,
	                const int heapId,
	                const int numFixedBuckets,
	                const size_t fixedChunkSizes[],
	                const u8 fixedChunkCounts[]);

	virtual ~netMemAllocator();

	// These methods are derived from sysMemAllocator
	SYS_MEM_VIRTUAL bool SetQuitOnFail(const bool quitOnFail);
	SYS_MEM_VIRTUAL void* Allocate(size_t size, AllocReason reason, size_t align, int heapIndex = 0);
	SYS_MEM_VIRTUAL void* Allocate(size_t size, size_t align, int heapIndex = 0);
	SYS_MEM_VIRTUAL void* TryAllocate(size_t size, size_t align, int heapIndex);
	SYS_MEM_VIRTUAL void Free(const void *ptr);
	SYS_MEM_VIRTUAL void Resize(const void* ptr, size_t newSmallerSize);
	SYS_MEM_VIRTUAL sysMemAllocator* GetPointerOwner(const void *ptr);
	SYS_MEM_VIRTUAL size_t GetMemoryUsed(int bucket = -1);
	SYS_MEM_VIRTUAL size_t GetMemoryAvailable();
	SYS_MEM_VIRTUAL size_t GetLargestAvailableBlock();
	SYS_MEM_VIRTUAL size_t GetLowWaterMark(bool reset);
	SYS_MEM_VIRTUAL void SanityCheck();
	SYS_MEM_VIRTUAL bool IsValidPointer(const void * ptr) const;
	SYS_MEM_VIRTUAL void BeginLayer();
	SYS_MEM_VIRTUAL int EndLayer(const char *layerName,const char *leakfile);
	SYS_MEM_VIRTUAL size_t GetSizeWithOverhead(const void *ptr) const;
	SYS_MEM_VIRTUAL size_t GetHeapSize() const;
	SYS_MEM_VIRTUAL void *GetHeapBase() const;
	SYS_MEM_VIRTUAL size_t GetFragmentation();

	size_t GetMemoryAvailableFixed();
	size_t GetMemoryAvailableSimple();
	size_t GetLargestAvailableBlockFixed();
	size_t GetLargestAvailableBlockSimple();
	u8 GetNumBucketAllocations(const unsigned bucket);

	size_t GetFixedHeapMisses() const;
	size_t GetFailedAllocations() const;
	float GetEfficiency() const; // range from 0 to 100
	
	void PrintBasicMemoryUsage();
	void PrintMemoryUsage();

private:
	typedef sysMemFixedAllocator netMemFixedAllocator;
	sysCriticalSectionToken m_AllocCs;
	void* m_FixedAllocatorHeap;
	unsigned m_FixedHeapSize;
	sysMemSimpleAllocator m_SimpleAllocator;
	netMemFixedAllocator* m_FixedAllocator;
	unsigned m_MaxFixedChunkSize;
#if !__NO_OUTPUT
	size_t m_LeastMemoryAvailable;
#endif
	bool m_QuitOnFailure : 1;
};

} // namespace rage

#endif  // NET_MEM_ALLOCATOR_H
