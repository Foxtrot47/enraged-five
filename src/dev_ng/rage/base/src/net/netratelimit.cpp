// ================================================================================================================
// netTokenBucket
// ================================================================================================================

#include "net/netratelimit.h"

#include "diag/seh.h"
#include "net/netdiag.h"
#include "system/timer.h"

namespace rage
{

netTokenBucket::netTokenBucket(const float tokens, const float fillrate
#if USE_TOKEN_STATS
	, TokenBucketProfiler* profiler
#endif // USE_TOKEN_STATS
	OUTPUT_ONLY(, const char* bucketName)
)
	: m_stats(tokens, fillrate OUTPUT_ONLY(, bucketName))
#if USE_TOKEN_STATS
	, m_profiler(profiler)
#endif // USE_TOKEN_STATS
{

}

void netTokenBucket::RefillTokens()
{
	// current time in milliseconds.
	const u32 currtime = sysTimer::GetSystemMsTime();

	// time passed since last refill.
	const u32 timepassed = currtime - m_stats.m_lasttimefill;

	m_stats.m_lasttimefill = currtime;

	// the number of tokens to add every S milliseconds.
	const float tokens = (m_stats.m_fillrate * timepassed);

#if RSG_OUTPUT
	if(tokens > 0)
	{
		netDebug("TokenBucket[%s]:: Refill Tokens :: Adding: %f (%f * %d), Current: %f, NewTotal: %f",
			m_stats.m_bucketname,
			tokens,
			m_stats.m_fillrate,
			timepassed,
			m_stats.m_tokens,
			m_stats.m_tokens + tokens > m_stats.m_capacity ? m_stats.m_capacity : m_stats.m_tokens + tokens);
	}
#endif

	// add the tokens to our bucket.
	m_stats.m_tokens += tokens;

	// make sure we haven't went over the capacity.
	if(m_stats.m_tokens > m_stats.m_capacity)
		m_stats.m_tokens = m_stats.m_capacity;

#if USE_TOKEN_STATS
	if (m_profiler) m_profiler->Update(*this);
#endif //USE_TOKEN_STATS
}

bool netTokenBucket::Consume(const float tokens)
{
	RefillTokens();

	if(tokens <= m_stats.m_tokens)
	{
		const u32 currtime = sysTimer::GetSystemMsTime();

		m_stats.m_consumed += tokens;
		m_stats.m_timepassed += m_stats.m_lastconsume > 0 ? (currtime - m_stats.m_lastconsume) : 0;
		m_stats.m_avgTokenPerMin = (m_stats.m_timepassed > 0) ? ((m_stats.m_consumed - 1.0f) / (static_cast<float>(m_stats.m_timepassed) / 1000.0f)) * 60.0f : 0.0f;

#if USE_TOKEN_STATS
		if (m_profiler) m_profiler->Update(*this);
#endif // USE_TOKEN_STATS

		m_stats.m_lastconsume = currtime;
		m_stats.m_tokens -= tokens;

		netDebug("TokenBucket[%s]:: Consume Tokens :: %f, Remaining: %f", m_stats.m_bucketname, tokens, m_stats.m_tokens);

		return true;
	}

	netError("TokenBucket[%s]:: Failed to Consume Tokens :: %f", m_stats.m_bucketname, tokens);

	return false;
}

bool netTokenBucket::CanConsume(const float tokens)
{
	RefillTokens();

	const bool canConsume = (tokens <= m_stats.m_tokens);

#if RSG_OUTPUT
	if(!canConsume)
	{
		netWarning("TokenBucket[%s]:: CanConsume '%f' tokens - Failed, current available tokens are '%f'.", m_stats.m_bucketname, tokens, m_stats.m_tokens);
	}
#endif // RSG_OUTPUT

	return canConsume;
}

void netTokenBucket::Reset(const float tokens, const float fillrate)
{
	m_stats.m_tokens = tokens;
	m_stats.m_capacity = tokens;
	m_stats.m_fillrate = fillrate;
	m_stats.m_lasttimefill = sysTimer::GetSystemMsTime();
	m_stats.m_lastconsume = 0;

	// m_timepassed and m_consumed are not reset here as originally they'd been static variables and they're only used for logging

#if USE_TOKEN_STATS
	if (m_profiler) m_profiler->Update(*this);
#endif // USE_TOKEN_STATS
}

float netTokenBucket::WaitTime(float tokens)
{
	RefillTokens();

	if(m_stats.m_tokens >= tokens)
		return 0;

	float needed = tokens - m_stats.m_tokens;
	float estimate = (1000 * needed) / m_stats.m_fillrate;

	netDebug("TokenBucket[%s]:: WaitTime: '%lf'", m_stats.m_bucketname, estimate);

	return estimate;
}

#if USE_TOKEN_STATS

TokenBucketProfiler::TokenBucketProfiler(const char* bucketName)
	: PFPAGE_TokenBucket(bucketName)
	, PFGROUP_TokenBuketG(bucketName)
	, PFPAGELINK_TokenBuket(&PFPAGE_TokenBucket, &PFGROUP_TokenBuketG)
	, PFVALUE_Capacity("Capacity", PFGROUP_TokenBuketG, true)
	, PFVALUE_Tokens("Tokens", PFGROUP_TokenBuketG, true)
	, PFVALUE_AvgTokensPerMinute("AvgTokensPerMinute", PFGROUP_TokenBuketG, true)
{

}

void TokenBucketProfiler::Update(const netTokenBucket& bucket)
{
	PFVALUE_Capacity.Set(bucket.m_stats.m_capacity);
	PFVALUE_Tokens.Set(bucket.m_stats.m_tokens);
	PFVALUE_AvgTokensPerMinute.Set(bucket.GetAvgTokenPerMinute());
}

#endif // USE_TOKEN_STATS
}