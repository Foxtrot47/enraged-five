// 
// net/httprequesttracker.cpp 
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved. 
// 

#if !__NO_OUTPUT

#include "httprequesttracker.h"

#include "diag/seh.h"
#include "parser/manager.h"
#include "rline/rldiag.h"
#include "string/stringutil.h"
#include "system/param.h"
#include "system/timer.h"
#include "netdiag.h"

namespace rage
{

PARAM(netHttpRequestTrackerDisable, "Disable the HTTP request tracker altogether.");
PARAM(netHttpRequestTrackerRules,  "Specify rules .xml file for HTTP request tracker. -netHttpRequestTrackerRules=rules.xml");

class netTokenBucket_RequestTracker
{
public:
	netTokenBucket_RequestTracker() :
		m_nTokens(0),
		m_nTokenLimit(0),
		m_nMsSinceTokenIncrement(0),
		m_nTokenRate(0.0f)
	{
	}

	// Returns false if the bucket has run out of tokens.
	bool RemoveToken()
	{
		if (m_nTokens == 0)
		{
			return false;
		}

		m_nTokens--;

		return true;
	}

	// Call once per frame.
	void Update(const unsigned nElapsedMs) 
	{
		unsigned tokenTimeMs = (unsigned)(1000.0f / m_nTokenRate);

		m_nMsSinceTokenIncrement += nElapsedMs;
		if (m_nMsSinceTokenIncrement >= tokenTimeMs)
		{
			m_nMsSinceTokenIncrement -= tokenTimeMs;

			m_nTokens = Min((m_nTokens + 1), m_nTokenLimit);
		}
	}

	void Refill()
	{
		m_nTokens = m_nTokenLimit;
		m_nMsSinceTokenIncrement = 0;
	}

	unsigned GetTokenCount() const { return m_nTokens; }
	unsigned GetTokenLimit() const { return m_nTokenLimit; }

	// Set the maximum number of tokens that can be in the bucket.
	void SetTokenLimit(const unsigned nTokenLimit) { m_nTokenLimit = nTokenLimit; }

	// Get the rate at which tokens are automatically added to the bucket in tokens per second.
	float GetTokenRate() const { return m_nTokenRate; }
	// Set the rate at which tokens are automatically added to the bucket in tokens per second.
	void SetTokenRate(const float nTokensPerSecond) { m_nTokenRate = nTokensPerSecond; }

private:
	unsigned m_nTokens;
	unsigned m_nTokenLimit;
	unsigned m_nMsSinceTokenIncrement;
	float m_nTokenRate;
};

struct netHttpRequestTrackerRule
{
	bool Init(const char* name, const char* match, const unsigned bucketSize, const float bucketTokenRate)
	{
		safecpy(m_Name, name);
		safecpy(m_Match, match);
		m_Bucket.SetTokenLimit(bucketSize);
		m_Bucket.SetTokenRate(bucketTokenRate);
		return true;
	}

	char m_Name[64];
	char m_Match[128];
	netTokenBucket_RequestTracker m_Bucket;
	inlist_node<netHttpRequestTrackerRule> m_ListLink;
};

typedef inlist<netHttpRequestTrackerRule, &netHttpRequestTrackerRule::m_ListLink> netHttpRequestTrackerRuleList;
netHttpRequestTrackerRuleList s_RequestTrackerRules;

unsigned netHttpRequestTracker::sm_nPrevTime = 0;

netHttpRequestTracker::Delegator netHttpRequestTracker::sm_Delegator;

bool netHttpRequestTracker::Init()
{
	if (PARAM_netHttpRequestTrackerDisable.Get()) {
		netDebug("HttpRequestTracker is disabled by -netHttpRequestTrackerDisable");
		return true;
	}

	netDebug("HttpRequestTracker::Init");

	const char* filename = NULL;
	if(!PARAM_netHttpRequestTrackerRules.Get(filename))
	{
		filename = "common:/Data/Debug/HttpRequestTrackerRules.xml";
	}

	s_RequestTrackerRules.clear();

	bool result = LoadRules(filename);
	if (!result)
	{
		netDebug("HttpRequestTracker::Init Failed");
		return false;
	}

	sm_nPrevTime = 0;

	netDebug("HttpRequestTracker::Init Succeeded");

	return true;
}

bool netHttpRequestTracker::LoadRules(const char* sRulesFileXML)
{
	/* Example rules file
	<?xml version="1.0" encoding="utf-8"?>
	<Rules>
		<Rule Name="Telemetry/SubmitCompressed" Match="*Telemetry.asmx/SubmitCompressed" BucketSize="5" BucketTokenRate="0.1065" />
	</Rules>
	*/

	bool success = false;

	INIT_PARSER;

	parTree* tree = NULL;

	rtry
	{
		tree = PARSER.LoadTree(sRulesFileXML, "xml");

		rverify(tree && tree->GetRoot()
				,catchall
				,rlError("File '%s' is missing", sRulesFileXML));

		parTreeNode* pRoot = tree->GetRoot();
		rverify(pRoot, catchall, );

		parTreeNode* node = pRoot;

		for(unsigned i = 0; i < (unsigned)node->FindNumChildren(); i++)
		{
			parTreeNode* pNode = node->FindChildWithIndex(i);
			rverify(pNode, catchall, );

			if(stricmp(pNode->GetElement().GetName(), "Rule") != 0)
			{
				continue;
			}

			const parAttribute *attr = pNode->GetElement().FindAttributeAnyCase("Name");
			rverify(attr, catchall, );
			const char* name = attr->GetStringValue();
			rverify(name, catchall, );

			
			attr = pNode->GetElement().FindAttributeAnyCase("Match");
			rverify(attr, catchall, );
			const char* match = attr->GetStringValue();
			rverify(match, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("BucketSize");
			u32 bucketSize = 0;
			if(attr)
			{
				const char* szBucketSize = attr->GetStringValue();
				rverify((szBucketSize != NULL) && (sscanf(szBucketSize, "%u", &bucketSize) == 1), catchall, );
			}

			attr = pNode->GetElement().FindAttributeAnyCase("BucketTokenRate");
			float tokenRate = 0.0f;
			if(attr)
			{
				const char* szTokenRate = attr->GetStringValue();
				rverify((szTokenRate != NULL) && (sscanf(szTokenRate, "%f", &tokenRate) == 1), catchall, );
			}

			netHttpRequestTrackerRule* rule = rage_new netHttpRequestTrackerRule;
			rverify(rule, catchall, );

			bool valid = rule->Init(name, match, bucketSize, tokenRate);

			if(valid)
			{
				s_RequestTrackerRules.push_back(rule);
			}
			else
			{
				delete rule;
			}
		}

		//AddWidgets();

		success = true;
	}
	rcatchall
	{
	}

	if(tree)
	{
		delete tree;
		tree = NULL;
	}

	SHUTDOWN_PARSER;

	return success;
}

void netHttpRequestTracker::Update()
{
	if (PARAM_netHttpRequestTrackerDisable.Get()) {
		return;
	}

	const unsigned time = sysTimer::GetSystemMsTime();
	const unsigned elapsedTime = time - sm_nPrevTime;
	sm_nPrevTime = time;

	netHttpRequestTrackerRuleList::iterator it = s_RequestTrackerRules.begin();
	netHttpRequestTrackerRuleList::iterator stop = s_RequestTrackerRules.end();
	for(; stop != it; ++it)
	{
		netHttpRequestTrackerRule* rule = (*it);

		// Update the rule's token bucket.
		rule->m_Bucket.Update(elapsedTime);
	}
}

void netHttpRequestTracker::HandleRequest(const char* uri)
{
	if (PARAM_netHttpRequestTrackerDisable.Get()) {
		return;
	}

	netHttpRequestTrackerRuleList::iterator it = s_RequestTrackerRules.begin();
	netHttpRequestTrackerRuleList::iterator stop = s_RequestTrackerRules.end();
	for(; stop != it; ++it)
	{
		netHttpRequestTrackerRule* rule = (*it);

		if (StringWildcardCompare(rule->m_Match, uri, true) == 0)
		{
			// Remove a token from the bucket. 
			if (!rule->m_Bucket.RemoveToken())
			{
				// If this isn't possible, do a thing!
				DispatchDelegate(rule->m_Name,
								 rule->m_Match, 
								 rule->m_Bucket.GetTokenLimit(), 
								 rule->m_Bucket.GetTokenRate());
				
				// Refill the bucket to prevent loads of spam.
				rule->m_Bucket.Refill();
			}
		}
	}
}

void netHttpRequestTracker::AddDelegate(Delegate* dlgt)
{
	sm_Delegator.AddDelegate(dlgt);
}

void netHttpRequestTracker::RemoveDelegate(Delegate* dlgt)
{
	sm_Delegator.RemoveDelegate(dlgt);
}

void netHttpRequestTracker::DispatchDelegate(const char* sRuleName, const char* sRuleMatch, const unsigned nTokenLimit, const float nTokenRate)
{
	sm_Delegator.Dispatch(sRuleName, sRuleMatch, nTokenLimit, nTokenRate);
}

} // namespace rage

#endif // !__NO_OUTPUT
