// 
// net/peercomplainer.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_PEERCOMPLAINER_H
#define NET_PEERCOMPLAINER_H


#include "atl/array.h"
#include "atl/delegate.h"
#include "net/connectionmanager.h"

//TODO:
// - consider templatizing the peer id type, for apps that don't need 64-bits
//	- alternative: Policies specify how many bits are used in the id
//	- then the app could use much smaller synchronized values to conserve bandwidth

namespace rage
{

class netPeerComplainer
{
public:

    //PURPOSE
    //  An array of boot candidates is passed back to the game
    //  via the boot delegate.  The candidate array is sorted in
    //  descending order of complaints.  Candidates with the most
    //  complaints appear first in the array.
    struct BootCandidate
    {
        BootCandidate()
            : m_PeerId(0)
            , m_NumComplaints(0)
			, m_HasPrevious(false)
			, m_Booted(false)
        {
        }

        BootCandidate(const u64 peerId,
                        const unsigned numComplaints)
            : m_PeerId(peerId)
			, m_NumComplaints(numComplaints)
			, m_HasPrevious(false)
			, m_Booted(false)
        {
        }

        u64 m_PeerId;
        unsigned m_NumComplaints;
		bool m_HasPrevious;

		//Set by the delegate
		bool m_Booted;

        bool operator==(const BootCandidate& that) const
        {
            return m_PeerId == that.m_PeerId;
        }
    };

	//Type for the callback used when the server has received complaints
    //for peers.
    //The callback is called with an array of boot candidates sorted in
    //descending order of complaints.  Candidates with the most complaints
    //appear first in the array.
	typedef atDelegate< void (netPeerComplainer*, BootCandidate*, const unsigned) > BootDelegate;

    enum
    {
        MAX_PEERS			= 64,
		MAX_FRIENDLY_NAME	= 32,
	};

    //PURPOSE
    //  Peer manager policies.
	struct Policies
	{
        enum
        {
            DEFAULT_BOOT_DELAY      = 10 * 1000,
            DEFAULT_RESEND_INTERVAL = 1000
        };

        Policies()
            : m_BootDelay(DEFAULT_BOOT_DELAY)
            , m_ComplaintResendInterval(DEFAULT_RESEND_INTERVAL)
            , m_ChannelId(NET_INVALID_CHANNEL_ID)
        {
        }

		//After receiving an initial complaint, how long the server will wait
		//before deciding to boot. This allows complaints to come in from
		//other peers before a decision is made.
		unsigned m_BootDelay;

		//How often to resend registered complaints. Resending compensates for
		//dropped packets and host migration.
		unsigned m_ComplaintResendInterval;

		//Channel used for network communication. Complaint messages will be dropped if 
		//sent on a channel which is exceeding it's bandwidth limit. Thus, it would be wise 
		//to pick a channel which is unlikely to exceed bandwidth, or to designate a channel
		//just for complaints.
		unsigned m_ChannelId;
	};

	 netPeerComplainer();
	~netPeerComplainer();

	//PURPOSE
	//  Initialize the complaint system when entering a session.
	//PARAMS
    //  myPeerId        - Peer id of the local peer.
	//  cxnMgr			- Connection manager used for communication.
	//  serverEndpointId - Network endpointId of the server to which we'll
	//					  send complaints.  Pass NET_INVALID_ENDPOINT_ID if server is local.
	//  appBootHandler	- Delegate called by complainer when a peer should be booted.
	//  policies		- Policies used by complainer, See above.
	//NOTES
	//	Init should be called whenever entering a new session, once the
	//  the endpointId of the server is known. Shutdown should be called
	//  before calling Init again.
	void Init(const u64 myPeerId,
        netConnectionManager* pCxnMgr, 
		const EndpointId serverEndpointId,
		const BootDelegate& appBootHandler,
		const Policies& policies,
		const u64 token);

	//PURPOSE
	//  Shutdown the complaint system when exiting a session.
	//NOTES
	//	Shutdown should be called when leaving a session, and before
	//  calling Init again.
	void Shutdown();

    //PURPOSE
    //  Adds a peer to the session.
    //NOTES
    //  If the server receives a complaint from a peer that is not
    //  in the session the complaint will be ignored.
    bool AddPeer(const u64 peerId
#if !__NO_OUTPUT
		, const char* friendlyName
#endif
		);

    //PURPOSE
    //  Removes a peer from the session.
    //NOTES
    //  If the server receives a complaint from a peer, or for a peer,
    //  that is not in the session the complaint will be ignored.
    void RemovePeer(const u64 peerId);

    //PURPOSE
    //  Returns true if the peer id is registered.
    bool HavePeer(const u64 peerId) const;

	//PURPOSE
	//  Register a complaint about another peer (clients only).
	//PARAMS
	//  otherPeerId		- App specific unique id of the other peer. The id of each peer
	//					  must be a unique synchronized value within the session.
	//NOTES
	//	Complaint is sent to the server immediately after initial registration, 
	//  and then periodically resent (see Policies) until the complaint is
	//  unregistered by the app.
    //
    //  Complaints can only be sent for peers that have been registered using
    //  AddPeer().
	void RegisterComplaint(const u64 otherPeerId);

	//PURPOSE
	//  Unregister a complaint about another peer (clients only).
	//PARAMS
	//  otherPeerId		- App specific unique id of the other peer. The id of each peer
	//					  must be a unique synchronized value within the session.
	//NOTES
	//	Call once the complaint has been resolved to prevent complaints from
	//  being resent to the server.
	void UnregisterComplaint(const u64 otherPeerId);

	//PURPOSE
	//  Check if a complaint has been locally registered for the given peer (clients only).
	//PARAMS
	//  otherPeerId		- App specific id of the other peer.
	bool IsComplaintRegistered(const u64 otherPeerId) const;

	//PURPOSE
	//	Update peer complainer system.
	//NOTES
	//	Must be called periodically on clients and servers.	Ideally, once per frame 
	//	to ensure timely resending of complaints (on clients), and for timely boot decisions
	//	(on servers).
	void Update();

	//PURPOSE
	//	Check if system has been initialized.
	//NOTES
	//	Many of these api calls will assert if the system isn't initialized.
	bool IsInitialized() const { return m_bInitialized; }

	//PURPOSE
	//	Check if server is local.
	//NOTES
	//	Servers shouldn't need to register complaints.
	bool IsServer() const { return m_bServer; }

	//PURPOSE
	//	Migrate the server to a different remote machine
	//PARAMS
	//  serverEndpointId - Network endpointId of the server to which we'll
	//					  send complaints.  NULL if server is local.
	//NOTES
	//	Call after a host migration or whenever a new server is desired.
	//	The server is responsible for collating complaints and deciding
	//	who and when to boot.
	//
	//	Clients will immediately resend their complaints to the new server.
	void MigrateServer(const EndpointId serverEndpointId, const u64 token);

	//PURPOSE
	//  Sets the peer complainer policies
	//PARAMS
	//  policies		- Policies used by complainer, See above.
	void SetPolicies(const Policies& policies);

	//PURPOSE
	//  Returns server's endpointId
	EndpointId GetServerEndpointId() { return m_ServerEndpointId; }

	//PURPOSE
	//  Returns session token
	u64 GetToken() { return m_Token; }

private:

	void OnCxnEvent(netConnectionManager*, const netEvent*);

	void ReceiveComplaints(const u64 complainerId,
                            const u64* complaineeIds,
                            const unsigned numComplaints);

	void SendComplaintMessage(u64 complaineeId);

	void ResendComplaints();

	struct Complaint
	{
        Complaint()
            : m_PeerIdA(u64(-1))
            , m_PeerIdB(u64(-1))
            , m_AAgainstB(false)
            , m_BAgainstA(false)
			, m_TimeReceived(0)
        {
        }

		void Clear()
		{
			m_PeerIdA = u64(-1);
			m_PeerIdB = u64(-1);
			m_AAgainstB = false;
			m_BAgainstA = false;
			m_TimeReceived = 0;
		}

        void Update(const u64 complainer, const u64 complainee)
        {
            if(u64(-1) == m_PeerIdA)
            {
                netAssert(u64(-1) == m_PeerIdB);

                m_PeerIdA = complainer;
                m_PeerIdB = complainee;
                m_AAgainstB = true;
                m_BAgainstA = false;
            }
            else if(complainer == m_PeerIdA)
            {
                m_AAgainstB = true;
            }
            else if(complainer == m_PeerIdB)
            {
                m_BAgainstA = true;
            }
        }

		bool operator==(const Complaint& other) const
		{
			return
                (other.m_PeerIdA == m_PeerIdA && other.m_PeerIdB == m_PeerIdB)
                || (other.m_PeerIdA == m_PeerIdB && other.m_PeerIdB == m_PeerIdA);
		}

		u64 m_PeerIdA;
		u64 m_PeerIdB;

        bool m_AAgainstB    : 1;    //A complained about B
        bool m_BAgainstA    : 1;    //B complained about A

		unsigned m_TimeReceived;
	};

	void CheckComplaints();
	void ProcessComplaint(const Complaint& complaintToProcess);

#if !__NO_OUTPUT
	const char* GetFriendlyName(u64 peerId) const;
#endif

	struct Peer
	{
		Peer()
			 : m_PeerId(u64(-1))
		{
#if !__NO_OUTPUT
			m_FriendlyName[0] = '\0';
#endif
		}

		Peer(u64 peerId)
			: m_PeerId(peerId)
		{
#if !__NO_OUTPUT
			m_FriendlyName[0] = '\0';
#endif
		}

		void Init(u64 peerId
#if !__NO_OUTPUT
			, const char* friendlyName
#endif
			)
		{
			m_PeerId = peerId;
#if !__NO_OUTPUT
			safecpy(m_FriendlyName, friendlyName);
#endif
		}

		bool operator==(const Peer& other) const
		{
			return (other.m_PeerId == m_PeerId);
		}

		u64 m_PeerId;
#if !__NO_OUTPUT
		char m_FriendlyName[MAX_FRIENDLY_NAME];
#endif
	};

	static unsigned sm_InitId; 

    u64 m_MyPeerId;
	EndpointId m_ServerEndpointId;
	netConnectionManager* m_pCxnMgr;
	netConnectionManager::Delegate m_CxnEventDelegate;
	BootDelegate m_BootDelegate;
    atFixedArray<Peer, MAX_PEERS> m_PeerIds;				//peer ids -- ids of peers in the session.
	atFixedArray<Complaint, MAX_PEERS> m_Complaints;		//server -- list of complaints from others
	atFixedArray<u64, MAX_PEERS> m_RegisteredComplainees;   //client -- list of peers with which local peer has registered complaint
	atFixedArray<u64, MAX_PEERS> m_PeerHasPrevious;		    //peer has previous -- ids of peers with previous complaints against them
	unsigned m_TimeToResendComplaints;						//client -- if non-zero, the sysTime when we need to resend our list of complaints to the host
	Policies m_Policies;
	u64 m_Token;
	unsigned m_InitId;

    bool m_bInitialized     : 1;
    bool m_bServer          : 1;
};

}
#endif //NET_PEERCOMPLAINER_H
