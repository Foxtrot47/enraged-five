// 
// net/crypto.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_CRYPTO_H
#define NET_CRYPTO_H

#include "net.h"
#include "data/bitbuffer.h"
#include "data/sha1.h"

struct ecc_key;

namespace rage
{

// BCrypt is available (and hardware accelerated) on Xbox One and PC (Windows Vista+).
// It is also the only TCR compliant crypto library that Microsoft approves on Xbox One.
// We use wolfSSL for all other platforms. Sony's libSecure library on PS4 SDK 3.0
// was about 2x slower than wolfSSL 3.0, especially during HMAC calculation.
// See perforce history for libSecure implementation if needed.
#define NET_CRYPTO_USE_BCRYPT (RSG_DURANGO || RSG_PC)

//PURPOSE
//  Implements RC4 encryption.
class Rc4Context
{
public:
    Rc4Context();

    bool IsValid() const;
    void Clear();
    void Reset(const u8* key, const unsigned size);
    void Encrypt(u8* data, const unsigned size);

    //PURPOSE
    //  Convenience method that encrypts a buffer with specified key.
    //  This can't be used for streamed data, but is useful for encrypting
    //  or decrypting single buffers.
    static void Encrypt(const u8* key, const unsigned keySize,
                        u8* data, const unsigned dataSize)
    {
        Rc4Context rc4;
        rc4.Reset(key, keySize);
        rc4.Encrypt(data, dataSize);
    }

private:
    u8 m_X;
    u8 m_Y;
    u8 m_State[256];
    bool m_IsValid;
};

//PURPOSE
//  A wrapper for wolfcrypt's RSA implementation to make working it easier
//  Keys should be in DER encoded ASN.1 format
class Rsa
{
public:
    //PURPOSE
    //  Encrypts the input buffer with the public key
    //  and stores the result in the supplied output
    //  buffer.
    static bool Encrypt(const u8* key, 
                        const unsigned keyLen, 
                        const u8* const inBuf,
                        const unsigned inBufLen,
                        u8* outBuf,
                        const unsigned outBufSize,
                        unsigned *outLen);

    //PURPOSE
    //  Decrypts an input buffer (in place) with the private key
    //  Note: This is in-place because openssl doesn't take a
    //  user-supplied buffer to decrypt into, and the decrypted
    //  message is guaranteed to be the same size or smaller than
    //  the encrypted message
    static bool Decrypt(const u8* key,
                        const unsigned keyLen,
                        u8* inBuf,
                        const unsigned inBufLen,
                        u8** outBuf,
                        unsigned *outLen);

    //PURPOSE
    //  Encrypts an input buffer with the private key
    //  and stores the result in the supplied output buffer
    static bool PrivateEncrypt(const u8* key, 
                               const unsigned keyLen, 
                               const u8* const inBuf,
                               const unsigned inBufLen,
                               u8* outBuf,
                               const unsigned outBufSize,
                               unsigned *outLen);

    //PURPOSE
    //  Decrypts an input buffer (in place) with the public key
    static bool PublicDecrypt(const u8* key,
                              const unsigned keyLen,
                              u8* inBuf,
                              const unsigned inBufLen,
                              u8** outBuf,
                              unsigned *outLen);
    
    //PURPOSE
    //  PKCS1 signature with Sha1 hash
    static bool SignSha1(const u8* key,
                         const unsigned keyLen,
                         const u8 (&hash)[Sha1::SHA1_DIGEST_LENGTH],
                         u8* outBuf,
                         const unsigned outBufSize,
                         unsigned *outLen);


    //PURPOSE
    //  Verifies a PKCS1 signature with Sha1 hash
    static bool VerifySha1(const u8* key,
                           const unsigned keyLen,
                           u8* inBuf,
                           const unsigned inBufLen,
                           const u8 (&hash)[Sha1::SHA1_DIGEST_LENGTH]);
};

class EcDsa
{
public:
	//PURPOSE
	//  Signs data using ECC private key with Sha256 hash
	//NOTE
	// N-bit ECC key produces a 2N-bit signature
	// e.g. 256-bit key -> 512-bit (64 byte) signature
	static bool SignSha256(ecc_key* key,
							const u8* data,
							const unsigned dataSize,
							u8* outSignature,
							unsigned& signatureSize);

	//PURPOSE
	//  Verifies a signature using ECC public key with Sha256 hash
	static bool VerifySha256(ecc_key* key,
							 const u8* data,
							 const unsigned dataSize,
							 const u8* signature,
							 const unsigned signatureSize);

};

class netCrypto
{
public:
	static bool Init();
	static void Shutdown();

	NET_ASSERTS_ONLY(static void CheckWolfSslSizes());

	//PURPOSE
	//  Generates cryptographically random bytes.
	static bool GenerateRandomBytes(u8* outBytes, const unsigned numBytes);

	//PURPOSE
	//  Generates non-cryptographically random bytes. Faster, but don't use if crypto RNG is required.
	static void GenerateRandomBytesWeak(u8* outBytes, const unsigned numBytes);

#if !__NO_OUTPUT
	//PURPOSE
	//  Converts an ASN.1 formatted date/timestamp to human readable string.
	static char* AsnDateTimeToString(const u8* asnDateTime, char* out, unsigned outLen);
#endif

	//PURPOSE
	//  Implements DiffieHellman key agreement.
	class DiffieHellman
	{
	public:
		static const unsigned MAX_PRIME_SIZE_IN_BITS = 1024;
		static const unsigned MAX_PRIME_SIZE_IN_BYTES = MAX_PRIME_SIZE_IN_BITS >> 3;
		static const unsigned MAX_GENERATOR_SIZE_IN_BYTES = MAX_PRIME_SIZE_IN_BYTES;
		static const unsigned MAX_PUBLIC_KEY_LENGTH_IN_BYTES = MAX_PRIME_SIZE_IN_BYTES;
		static const unsigned MAX_PRIVATE_KEY_LENGTH_IN_BYTES = MAX_PUBLIC_KEY_LENGTH_IN_BYTES;
		static const unsigned MAX_AGREE_KEY_LENGTH_IN_BYTES = MAX_PUBLIC_KEY_LENGTH_IN_BYTES;

		DiffieHellman();
		~DiffieHellman();

		//PURPOSE
		//  Initializes the DH state with prime and generator.
		//NOTE
		//  Calling InitDefault1024BitGroup() will initialize with a known 1024-bit group.
		bool Init(const u8* prime,
					const unsigned sizeOfPrime,
					const u8* generator,
					const unsigned sizeOfGenerator);

		//PURPOSE
		//  Initializes the DH state with a standard 1024-bit group.
		//NOTE
		//  Public key will be 128 bytes.
		bool InitDefault1024BitGroup();

		//PURPOSE
		//  Clears state and frees memory.
		void Clear();

		//PURPOSE
		//  Generates the public/private key pair.
		bool GenerateKeyPair();

		//PURPOSE
		//  Retrieves the public key.
		//PARAMS
		//  publicKey         - [out] Destination buffer that will hold the public key.
		//  sizeOfPublicKey   - [in/out] On [in] contains the size of the publicKey buffer.
		//						This must be >= the size of the group used to initialize the class.
		//						On [out], contains the actual length of the public key in bytes.
		bool ExportPublicKey(u8* publicKey,
							 unsigned* sizeOfPublicKey);

		//PURPOSE
		//  Given the public key of the remote peer, generates the mutually-agreed key.
		//PARAMS
		//  otherPublicKey			- [in] The remote peer's public key.
		//  sizeOfOtherPublicKey	- contains the size of the otherPublicKey in bytes.
		//  agreeKey				- [out] The mutually-agreed key.
		//  sizeOfAgreeKey			- [in/out] On [in] contains the size of the agreeKey buffer.
		//							  This must be >= the size of the prime used to initialize the class.
		//							  On [out], contains the actual length of the agreeKey in bytes.
		bool Agree(const u8* otherPublicKey,
					const unsigned sizeOfOtherPublicKey,
					u8* agreeKey,
					unsigned* sizeOfAgreeKey);
	private:

		// we can't include wolfSSL headers in this header, so we'll use a Cheshire cat.
		struct Impl;
		Impl* m_pImpl;

		bool m_Initialized;
		bool m_GeneratedKeyPair;
	};

private:
	static netRandom sm_WeakRng;
	static bool sm_Initialized;
};

class netP2pCrypt
{
public:

	/*
		1 byte for flags
		4 bytes for random IV seed
		HMAC: 64-bits (8 bytes)
		Up to 16 bytes of padding.
		============================
		29 bytes of overhead
	*/
	static const unsigned MAX_SIZEOF_OVERHEAD_IN_BYTES = (1 + 4 + 8 + 16);

	class Key
	{
	public:
		static const unsigned KEY_LENGTH_IN_BITS = 256;
		static const unsigned KEY_LENGTH_IN_BYTES = KEY_LENGTH_IN_BITS >> 3;
		static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = KEY_LENGTH_IN_BYTES;

		Key();
		~Key();

		//PURPOSE
		//  Exports data in a platform/endian independent format.
		//PARAMS
		//  buf         - Destination buffer.
		//  sizeofBuf   - Size of destination buffer.
		//  size        - Set to number of bytes exported.
		//RETURNS
		//  True on success.
		bool Export(void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0) const;

		//PURPOSE
		//  Imports data exported with Export().
		//  buf         - Source buffer.
		//  sizeofBuf   - Size of source buffer.
		//  size        - Set to number of bytes imported.
		//RETURNS
		//  True on success.
		bool Import(const void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0);

		void ResetKey(const u8* key, const unsigned keyLengthInBytes);
		void CombineKeys(const netP2pCrypt::Key& key1, const netP2pCrypt::Key& key2);
		void Clear();
		bool IsValid() const;
		void GetRawKey(u8 (&rawKey)[KEY_LENGTH_IN_BYTES]) const;
		const u8* GetRawKeyPtr() const;
		bool operator==(const netP2pCrypt::Key& that) const;

	private:
		u8 m_RawKey[KEY_LENGTH_IN_BYTES];
		bool m_IsValid;
	};

	class PublicKey
	{
	public:
		static const unsigned KEY_LENGTH_IN_BYTES = netCrypto::DiffieHellman::MAX_PUBLIC_KEY_LENGTH_IN_BYTES;
		static const unsigned KEY_LENGTH_IN_BITS = KEY_LENGTH_IN_BYTES << 3;
		static const unsigned MAX_EXPORTED_SIZE_IN_BITS = datBitsNeeded<KEY_LENGTH_IN_BYTES>::COUNT + KEY_LENGTH_IN_BITS;
		static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((MAX_EXPORTED_SIZE_IN_BITS + 7) & ~7) >> 3;				// round up to nearest multiple of 8 then divide by 8

		PublicKey();
		~PublicKey();

		//PURPOSE
		//  Exports data in a platform/endian independent format.
		//PARAMS
		//  buf         - Destination buffer.
		//  sizeofBuf   - Size of destination buffer.
		//  size        - Set to number of bytes exported.
		//RETURNS
		//  True on success.
		bool Export(void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0) const;

		//PURPOSE
		//  Imports data exported with Export().
		//  buf         - Source buffer.
		//  sizeofBuf   - Size of source buffer.
		//  size        - Set to number of bytes imported.
		//RETURNS
		//  True on success.
		bool Import(const void* buf,
					const unsigned sizeofBuf,
					unsigned* size = 0);

		void ResetKey(const u8* key, const unsigned keyLengthInBytes);
		void Clear();
		bool IsValid() const;
		void GetRawKey(u8 (&rawKey)[KEY_LENGTH_IN_BYTES], unsigned* keyLengthInBytes) const;
		bool operator==(const netP2pCrypt::PublicKey& that) const;

	private:
		u8 m_RawKey[KEY_LENGTH_IN_BYTES];
		unsigned m_KeyLengthInBytes;
		bool m_IsValid;
	};

	static const unsigned HMAC_KEY_MAX_LEN = 32;
	static const unsigned HMAC_SALT_MAX_LEN = 32;

	typedef u8 HmacKeySalt[HMAC_KEY_MAX_LEN + HMAC_SALT_MAX_LEN];

	static bool Init();
	static void Shutdown();

	//PURPOSE
	//  Sets the key and salt used during Hmac generation.
	static void SetHmacKeyAndSalt(const u8* key, const unsigned keyLen, const u8* salt, const unsigned saltLen);
	static void SetHmacKeyAndSalt(const netP2pCrypt::HmacKeySalt& p2pKeySalt);

	//PURPOSE
	//  Returns the random key generated for the local peer.
	static const netP2pCrypt::Key& GetLocalPeerKey();

	//PURPOSE
	//  Returns the public key generated for the local peer.
	static const netP2pCrypt::PublicKey& GetLocalPublicKey();

	//PURPOSE
	//  Generates a p2p key from a remote peer's Diffie-Hellman public key.
	static bool GenerateP2pKey(const netP2pCrypt::PublicKey& remotePublicKey, netP2pCrypt::Key& outKey);

	//PURPOSE
	//  Generates a random key.
	static bool GenerateRandomKey(Key& key);

	//PURPOSE
	//  Generates cryptographically random bytes.
	static bool GenerateRandomBytes(u8* outBytes, const unsigned numBytes);

	//PURPOSE
	//  Encrypts data. The encoded data will be larger than the data passed in.
	//  key					- The key to use to encrypt the data.
	//  data				- The data to encrypt.
	//  sizeOfData			- The length in bytes of the data to encrypt.
	//  encodedData			- Receives the encoded data. The size of this buffer must be
	//						  at least (sizeOfData + MAX_SIZEOF_P2P_CRYPT_OVERHEAD - 1)
	//  sizeOfEncodedData	- The length in bytes of the encoded data buffer.
	//NOTES
	//  The encoded data will contain the encrypted data, plus an HMAC that is used
	//  by Decrypt() to verify the data has not been altered.
	//RETURNS
	//  True on successful encoding, false otherwise.
	static bool Encrypt(const netP2pCrypt::Key& key,
						const u8* data,
						const unsigned sizeOfData,
						u8* encodedData,
						unsigned& sizeOfEncodedData);

	//PURPOSE
	//  Decrypts data in-place that was encrypted with Encrypt().
	//  key			- The key to use to decrypt the data. Must be the same key that was used to encrypt the data.
	//  buf         - [in/out] Buffer containing the encoded buffer output by Encrypt(). Receives the decrypted plaintext.
	//  sizeofBuf   - [in/out] Size of the encodedData. Receives the length of the plaintext after decrypting.
	//RETURNS
	//  True on successful decryption and HMAC verification, false otherwise.
	static bool Decrypt(const netP2pCrypt::Key& key,
						u8** buf,
						unsigned& sizeOfBuf);

private:
	static const unsigned P2P_CRYPT_HMAC_LENGTH_IN_BYTES = (256 >> 3); // 256-bit hash
	static const unsigned P2P_CRYPT_TRUNCATED_HMAC_LENGTH_IN_BYTES = (64 >> 3); // 64-bit truncated hash

	static bool GenerateHmac(const u8* data,
								const unsigned sizeOfData,
								const u8* secret,
								const unsigned sizeOfSecret,
								const u8* salt,
								const unsigned sizeOfSalt,
								u8* digest);

	static u8 sm_HmacKey[HMAC_KEY_MAX_LEN];
	static u8 sm_HmacSalt[HMAC_SALT_MAX_LEN];
	static unsigned sm_HmacKeyLen;
	static unsigned sm_HmacSaltLen;
	static netP2pCrypt::Key sm_LocalPeerKey;
	static netP2pCrypt::PublicKey sm_LocalPublicKey;
	static netCrypto::DiffieHellman sm_DiffieHellman;
	static bool sm_Initialized;
};

//PURPOSE
//  Contains security context for P2P Crypt
class netP2pCryptContext
{
public:
	netP2pCryptContext();
	~netP2pCryptContext();
	void Init(const netP2pCrypt::Key& key, const u8 flags);
	void Clear();
	bool IsValid();
	const netP2pCrypt::Key& GetKey() const;
	u8 GetFlags() const;
private:
	netP2pCrypt::Key m_Key;
	u8 m_Flags;
	bool m_IsValid;
};

}   //namespace rage

#endif  //NET_CRYPTO_H
