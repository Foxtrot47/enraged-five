// 
// net/net.cpp
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#include "crypto.h"
#include "diag/seh.h"
#include "httpinterceptor.h"
#include "natdetector.h"
#include "net.h"
#include "nethardware.h"
#include "resolver.h"
#include "task.h"
#include "task2.h"
#include "relay.h"
#include "netsocket.h"
#include "netsocketmanager.h"
#include "tcp.h"
#include "profile/rocky.h"
#include "system/interlocked.h"
#include "system/threadpool.h"

namespace rage
{
// This command line can evolve over time to add/remove simulations that cause more real-world networking conditions.
// Examples may include packet loss/latency, NAT traversal failures, etc.
PARAM(netrealworld, "Allows networking systems to enable simulation of real world networking conditions.", "Disabled", "All", "All");

#undef __rage_channel
#define __rage_channel ragenet

#if RSG_PC && __FINAL && !__FINAL_LOGGING
#ifdef _CPPRTTI
#error "Security - RTTI should not be enabled for the RageNet project. If the RageNet project has been regenerated, projgen may have re-enabled RTTI."
#endif
#endif

static volatile unsigned s_NetInitCount = 0;
sysThreadPool::Thread s_NetThreads[NET_THREAD_POOL_NUM_THREADS];
sysThreadPool netThreadPool;

bool netInit(sysMemAllocator* allocator, netSocketManager* socketManager)
{
    if(1 == sysInterlockedIncrement(&s_NetInitCount))
    {
		netVerifyf(netThreadPoolInit(), "Error initializing netThreadPool");

#if !RSG_FINAL
		if(PARAM_netrealworld.Get())
		{
			netDebug("-%s is enabled", PARAM_netrealworld.GetName());
		}
#endif

#if FAKE_DROP_LATENCY
		netSocket::InitFakeLatency();
		netSocket::InitFakeDrop();
		netSocket::InitFakeBandwidthLimit();
#endif

		netVerifyf(netTcp::Init(), "Error initializing netTcp");

#if !RSG_NX
		netVerifyf(netCrypto::Init(), "Error initializing netCrypto");
#endif
        netVerifyf(netHardware::Init(), "Error initializing netHardware");
        netVerifyf(netRelay::Init(allocator, socketManager), "Error initializing netRelay");

#if !GTA_VERSION
		netVerifyf(netResolver::Init(), "Error initializing netResolver");
#endif
		netVerifyf(netNatDetector::Init(), "Error initializing netNatDetector");

#if ENABLE_HTTP_INTERCEPTOR
		netVerifyf(netHttpInterception::Init(), "Error initializing netHttpInterceptor");
#endif

		netVerifyf(netTask::Init(allocator), "Error initializing netTask");

		netVerifyf(netTask2::Init(), "Error initializing netTask2");
    }

    return true;
}

void netUpdate()
{
	PROFILE
    if(sysInterlockedRead(&s_NetInitCount))
    {
        netHardware::Update();
        netRelay::Update();

#if !GTA_VERSION 
		netResolver::Update();
#endif
		netNatDetector::Update();

        netTask::Update();
		netTask2::Update();
    }
}

void netShutdown()
{
    if(0 == sysInterlockedDecrement(&s_NetInitCount))
    {
		netDebug("netShutdown");

        netTask::Shutdown();

#if ENABLE_HTTP_INTERCEPTOR
		netHttpInterception::Shutdown();
#endif

		netRelay::Shutdown();
		netNatDetector::Shutdown();
#if !GTA_VERSION 
		netResolver::Shutdown();
#endif
        netHardware::Shutdown();
		netCrypto::Shutdown();
		netTcp::Shutdown();

#if FAKE_DROP_LATENCY
		netSocket::ShutdownFakeLatency();
#endif

		netThreadPoolShutdown();
    }
}

bool netThreadPoolInit()
{
	rtry
	{
		rverifyall(netThreadPool.Init());

		// logging truncates thread name to "<%s>" with 16 characters, excluding the "[RAGE] " prefix
		for (unsigned i = 0; i < NET_THREAD_POOL_NUM_THREADS; ++i)
		{
			char name[32];
			formatf(name, "[RAGE] netThrPool %u", i + 1);
#if USE_PROFILER & 0
			s_NetThreads[i].ProfileThread(name);
#endif
			rverifyall(netThreadPool.AddThread(&s_NetThreads[i], name, (i & 1) ? NET_THREADPOOL_CPU_2 : NET_THREADPOOL_CPU_1));
		}

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void netThreadPoolShutdown()
{
	netThreadPool.Shutdown();
}

///////////////////////////////////////////////////////////////////////////////
// netRandom
///////////////////////////////////////////////////////////////////////////////
netRandom::netRandom()
	: m_bDiscardFirst(true)
{
    Reset(1);
}

netRandom::netRandom(const int seed)
	: m_bDiscardFirst(true)
{
    Reset(seed);
}

void
netRandom::Reset(const int seed)
{
	const unsigned useed = ( unsigned ) seed;
	m_Seed[0] = useed + !useed;   // Make sure zero doesn't happen
	m_Seed[1] = ( ( useed << 16 ) | ( useed >> 16 ) ) ^ useed;

	// This algorithm gives a poor distribution for the first number in the sequence
	// when there is low variance between seeds.
	DiscardFirst();
}

void
netRandom::SetRandomSeed()
{
	netCrypto::GenerateRandomBytes((u8*)m_Seed, sizeof(m_Seed));

	// This algorithm gives a poor distribution for the first number in the sequence
	// when there is low variance between seeds.
	DiscardFirst();
}

void
netRandom::SetFullSeed(const u64 seed)
{ 
	m_Seed[0] = u32(seed >> 32);
	m_Seed[1] = u32(seed);

	// This algorithm gives a poor distribution for the first number in the sequence
	// when there is low variance between seeds.
	DiscardFirst();
}

int
netRandom::GetInt()
{
	u64 temp = (u64)1557985959 * m_Seed[0] + m_Seed[1];
    m_Seed[0] = u32( temp & ~0u );
    m_Seed[1] = u32( ( temp >> 32 ) & ~0u );
    // All 32 bits are random but it would break too much code
    // if the sign bit was left in.
    return m_Seed[0] & ( ~0u >> 1 );
}

int
netRandom::GetIntCrypto()
{
	int result = 0;
	netCrypto::GenerateRandomBytes((u8*)&result, sizeof(result));

	// All 32 bits are random but it would break too much code
	// if the sign bit was left in.
	return result & (~0u >> 1);
}

float 
netRandom::GetFloat()
{
	// Mask lower 23 bits, multiply by 1/2**23.
	return (GetInt() & ((1 << 23) - 1)) * 0.00000011920928955078125f;
}

int
netRandom::GetRanged(const int m, const int M)
{
    return((GetInt()%(M-m+1))+m);
}

int
netRandom::GetRangedCrypto(const int m, const int M)
{
	return((GetIntCrypto() % (M - m + 1)) + m);
}

void
netRandom::DiscardFirst()
{
	// This random number generator gives a poor distribution on the first call to GetInt() across multiple RNG's using similar seeds. 
	// Consider this example: 100,000 machines boot up around the same time and a code system creates a netRng and seeds it with time(NULL). 
	// Let's say this system needs to randomly connect to one of size servers (0-5). The following results are real world values of these 
	// 'random' selections, without and with discarding the first result.
	//	
	//				Don't Discard				Discard First
	//				0 - 11474					0 - 16678
	//				1 - 21849					0 -	16648
	//				2 - 11497					0 -	16648
	//				3 - 21844					0 -	16674
	//				4 - 11493					0 -	16672
	//				5 - 21843					0 -	16680
	// 
	//	You can see that without discarding the first GetInt() result, servers 1, 3, 5, etc are chosen twice as often as the rest. The 
	//	expectation of a normally distributed system here is 100,000 / 6 = ~16667. We see the first distribution has each result being
	//	~31% deviated from this expected value, whereas the results of the second test are evenly distributed (max deviation of 0.001%).
	//	Note: This could also be resolved by not seeding with time() and instead using crypto random seed (SetRandomSeed).
	if (m_bDiscardFirst)
	{
		GetInt();
	}
}

}   //namespace rage
