// 
// net/transaction.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "transaction.h"
#include "netdiag.h"

#include "profile/rocky.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, transaction)
#undef __rage_channel
#define __rage_channel ragenet_transaction

///////////////////////////////////////////////////////////////////////////////
//  netTransaction
///////////////////////////////////////////////////////////////////////////////

unsigned
netTransaction::InitRequest(void* buf,
                            const unsigned sizeofBuf,
                            unsigned* txId)
{
    const unsigned tmpId = netTransaction::NextId();
    if(txId)
    {
        *txId = tmpId;
    }
    return netTransaction::WriteHeader(SIG_RQST, tmpId, buf, sizeofBuf);
}

unsigned
netTransaction::InitRequest(void* buf,
                            const unsigned sizeofBuf,
                            const unsigned txId)
{
    netAssert(NET_INVALID_TRANSACTION_ID != txId);
    return netTransaction::WriteHeader(SIG_RQST, txId, buf, sizeofBuf);
}

unsigned
netTransaction::InitResponse(void* buf,
                            const unsigned sizeofBuf,
                            const unsigned txId)
{
    netAssert(NET_INVALID_TRANSACTION_ID != txId);
    return netTransaction::WriteHeader(SIG_RESP, txId, buf, sizeofBuf);
}

bool
netTransaction::IsTransaction(const void* buf,
                                const unsigned sizeofBuf)
{
    unsigned sig, txId;
    return netTransaction::ReadHeader(&sig, &txId, buf, sizeofBuf) == BYTE_SIZEOF_HEADER;
}

bool
netTransaction::IsRequest(const void* buf,
                            const unsigned sizeofBuf)
{
    unsigned sig, txId;
    return netTransaction::ReadHeader(&sig, &txId, buf, sizeofBuf) == BYTE_SIZEOF_HEADER
            && SIG_RQST == sig;
}

bool
netTransaction::IsResponse(const void* buf,
                            const unsigned sizeofBuf)
{
    unsigned sig, txId;
    return netTransaction::ReadHeader(&sig, &txId, buf, sizeofBuf) == BYTE_SIZEOF_HEADER
            && SIG_RESP == sig;
}

const void*
netTransaction::GetPayload(const void* buf,
                            const unsigned sizeofBuf,
                            unsigned* sizeofPayload)
{
    const void* payload = 0;
    if(netTransaction::IsTransaction(buf, sizeofBuf)
        && sizeofBuf > BYTE_SIZEOF_HEADER)
    {
        payload = ((const u8*)buf) + BYTE_SIZEOF_HEADER;
        *sizeofPayload = sizeofBuf - BYTE_SIZEOF_HEADER;
    }
    return payload;
}

netTransactionType
netTransaction::GetId(const void* buf,
                    const unsigned sizeofBuf,
                    unsigned* txId)
{
    netTransactionType type = NET_TXTYPE_INVALID;
    unsigned sig;

    if(netTransaction::ReadHeader(&sig, txId, buf, sizeofBuf))
    {
        type = (sig == SIG_RQST) ? NET_TXTYPE_REQUEST : NET_TXTYPE_RESPONSE;
    }
    return type;
}

//private:

static unsigned s_NetTransactionNextId;

unsigned
netTransaction::NextId()
{
    unsigned id = sysInterlockedIncrement(&s_NetTransactionNextId);

    while(NET_INVALID_TRANSACTION_ID == id)
    {
        id = sysInterlockedIncrement(&s_NetTransactionNextId);
    }

    return id;
}

unsigned
netTransaction::ReadHeader(unsigned* sig,
                        unsigned* txId,
                        const void* buf,
                        const unsigned sizeofBuf)
{
    unsigned size = 0;
    *sig = SIG_INVALID;
    if(sizeofBuf > BYTE_SIZEOF_HEADER)
    {
        datBitBuffer::ReadUnsigned(buf,
                                    *sig,
                                    BIT_SIZEOF_SIG,
                                    BIT_OFFSETOF_SIG);
        datBitBuffer::ReadUnsigned(buf,
                                    *txId,
                                    BIT_SIZEOF_TXID,
                                    BIT_OFFSETOF_TXID);
        size = (SIG_RQST == *sig || SIG_RESP == *sig) ? BYTE_SIZEOF_HEADER : 0;
    }
    return size;
}

unsigned
netTransaction::WriteHeader(const unsigned sig,
                            const unsigned txId,
                            void* buf,
                            const unsigned sizeofBuf)
{
    unsigned size = 0;
    if(sizeofBuf > BYTE_SIZEOF_HEADER
        && netVerify(SIG_RQST == sig || SIG_RESP == sig))
    {
        datBitBuffer::WriteUnsigned(buf,
                                    sig,
                                    BIT_SIZEOF_SIG,
                                    BIT_OFFSETOF_SIG);
        datBitBuffer::WriteUnsigned(buf,
                                    txId,
                                    BIT_SIZEOF_TXID,
                                    BIT_OFFSETOF_TXID);
        size = BYTE_SIZEOF_HEADER;
    }
    return size;
}

///////////////////////////////////////////////////////////////////////////////
//  netRequestHandler
///////////////////////////////////////////////////////////////////////////////

netRequestHandler::netRequestHandler()
    : m_Owner(0)
{
    m_CxnDlgt.Bind(this, &netRequestHandler::OnCxnEvent);
}

netRequestHandler::~netRequestHandler()
{
    if(m_Owner)
    {
        m_Owner->RemoveRequestHandler(this);
    }
}

//private:
void
netRequestHandler::OnCxnEvent(netConnectionManager* /*cxnMgr*/,
                            const netEvent* nevt)
{
    if(NET_EVENT_FRAME_RECEIVED == nevt->GetId())
    {
        const netEventFrameReceived* fr = nevt->m_FrameReceived;
        unsigned txId;
        if(NET_TXTYPE_REQUEST == netTransaction::GetId(fr->m_Payload,
                                                        fr->m_SizeofPayload,
                                                        &txId))
        {
            netRequest rqst;
            rqst.m_TxInfo.Reset(txId, fr->m_Sender, fr->GetEndpointId(), fr->m_ChannelId, fr->m_CxnId);
            rqst.m_Data = netTransaction::GetPayload(fr->m_Payload,
                                                    fr->m_SizeofPayload,
                                                    &rqst.m_SizeofData);

            this->Invoke(m_Owner, this, &rqst);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//  netResponseHandler
///////////////////////////////////////////////////////////////////////////////

netResponseHandler::netResponseHandler()
    : m_Owner(0)
    , m_TxId(NET_INVALID_TRANSACTION_ID)
	, m_CxnId(-1)
	, m_EndpointId(NET_INVALID_ENDPOINT_ID)
	, m_ChannelId(NET_INVALID_CHANNEL_ID)
    , m_Timeout(-1)
    , m_Result(-1)
    , m_Seq(0)
{
    m_CxnDlgt.Bind(this, &netResponseHandler::OnCxnEvent);
}

netResponseHandler::~netResponseHandler()
{
    if(m_Owner)
    {
        this->Cancel();
    }
}

bool
netResponseHandler::Answered() const
{
    return netResponse::REQUEST_ANSWERED == m_Result;
}

bool
netResponseHandler::Failed() const
{
    return netResponse::REQUEST_FAILED == m_Result;
}

bool
netResponseHandler::TimedOut() const
{
    return netResponse::REQUEST_TIMED_OUT == m_Result;
}

bool
netResponseHandler::Canceled() const
{
    return netResponse::REQUEST_CANCELED == m_Result;
}

bool
netResponseHandler::Pending() const
{
    return (0 != m_Owner);
}

void
netResponseHandler::Cancel()
{
    if(m_Owner)
    {
        netTransactor* owner = m_Owner;
        m_Owner->AbortRequest(this);
        netAssert(!m_Owner);
        netResponse resp;
        resp.m_TxInfo.Reset(m_TxId, m_Addr, m_EndpointId, NET_INVALID_CHANNEL_ID, m_CxnId);
        m_Result = resp.m_Code = netResponse::REQUEST_CANCELED;
        this->Invoke(owner, this, &resp);
    }
}

//private:

void
netResponseHandler::Fail()
{
    netAssert(m_Owner);
    netTransactor* owner = m_Owner;
    this->Abort();
    netResponse resp;
    resp.m_TxInfo.Reset(m_TxId, m_Addr, m_EndpointId, NET_INVALID_CHANNEL_ID, m_CxnId);
    m_Result = resp.m_Code = netResponse::REQUEST_FAILED;
    this->Invoke(owner, this, &resp);
}

void
netResponseHandler::Timeout()
{
    netAssert(m_Owner);
    netTransactor* owner = m_Owner;
    this->Abort();
    netResponse resp;
    resp.m_TxInfo.Reset(m_TxId, m_Addr, m_EndpointId, NET_INVALID_CHANNEL_ID, m_CxnId);
    m_Result = resp.m_Code = netResponse::REQUEST_TIMED_OUT;
    this->Invoke(owner, this, &resp);
}

void
netResponseHandler::Abort()
{
    if(m_Owner)
    {
        m_Owner->AbortRequest(this);
        netAssert(!m_Owner);
    }
}

void
netResponseHandler::OnCxnEvent(netConnectionManager* /*cxnMgr*/,
                                const netEvent* nevt)
{
    if(nevt->m_CxnId == m_CxnId)
    {
        const unsigned eid = nevt->GetId();

        if(NET_EVENT_ACK_RECEIVED == eid)
        {
            if(nevt->m_AckReceived->m_Ack == m_Seq)
            {
                netResponse resp;
                resp.m_Code = netResponse::REQUEST_RECEIVED;
                this->Invoke(m_Owner, this, &resp);
            }
        }
        else if(NET_EVENT_FRAME_RECEIVED == eid)
        {
			// Note: when a response is received, there might not be an endpoint created for the remote sender.
			// In that case, the address of the sender must match the address to which the request was sent.
			// If the frame is associated with an endpoint and the address doesn't match, we check the
			// endpoint ids, so we can handle the case where the address/route has changed.
            const netEventFrameReceived* fr = nevt->m_FrameReceived;
            unsigned txId;
            if(NET_TXTYPE_RESPONSE == netTransaction::GetId(fr->m_Payload,
                                                            fr->m_SizeofPayload,
                                                            &txId)
                && m_TxId == txId
                && ((m_Addr == fr->m_Sender) || (NET_IS_VALID_ENDPOINT_ID(m_EndpointId) && (m_EndpointId == fr->GetEndpointId()))))
            {
                netTransactor* owner = m_Owner;
                this->Abort();
                netResponse resp;
                m_Result = resp.m_Code = netResponse::REQUEST_ANSWERED;
                resp.m_TxInfo.Reset(txId, fr->m_Sender, fr->GetEndpointId(), fr->m_ChannelId, fr->m_CxnId);
                resp.m_Data = netTransaction::GetPayload(fr->m_Payload, fr->m_SizeofPayload, &resp.m_SizeofData);
                this->Invoke(owner, this, &resp);
            }
        }
    	else if(NET_EVENT_CONNECTION_ERROR == eid)
    	{
			netAssert(NET_IS_VALID_ENDPOINT_ID(m_EndpointId));
			if(nevt->GetEndpointId() == m_EndpointId)
			{
				if(nevt->m_CxnError->IsTimeout())
				{
					this->Timeout();
				}
				else
				{
					this->Fail();
				}
			}
    	}
    	else if(NET_EVENT_CONNECTION_CLOSED == eid)
    	{
			netAssert(NET_IS_VALID_ENDPOINT_ID(m_EndpointId));
			if(nevt->GetEndpointId() == m_EndpointId)
			{
				this->Fail();
			}
    	}
    }
}

///////////////////////////////////////////////////////////////////////////////
//  netTransactor
///////////////////////////////////////////////////////////////////////////////

netTransactor::netTransactor()
    : m_CxnMgr(0)
{
}

netTransactor::~netTransactor()
{
    this->Shutdown();
}

void
netTransactor::Init(netConnectionManager* cxnMgr)
{
    if(netVerifyf(!m_CxnMgr, "Already initialized"))
    {
        netAssert(m_RqstHandlers.empty());
        netAssert(m_RespHandlers.empty());

        m_CxnMgr = cxnMgr;
    }
}

void
netTransactor::Shutdown()
{
    if(m_CxnMgr)
    {
        while(!m_RqstHandlers.empty())
        {
            this->RemoveRequestHandler(*m_RqstHandlers.begin());
        }

        while(!m_RespHandlers.empty())
        {
            (*m_RespHandlers.begin())->Cancel();
        }

        m_CxnMgr = 0;
    }
}

bool
netTransactor::IsInitialized() const
{
    return (NULL != m_CxnMgr);
}

void
netTransactor::Update(const unsigned timeStep)
{
    netAssert(m_CxnMgr);

    RespHandlerList::iterator it = m_RespHandlers.begin();
    RespHandlerList::iterator next = it;
    RespHandlerList::const_iterator stop = m_RespHandlers.end();

    for(++next; stop != it; it = next, ++next)
    {
        netResponseHandler* h = *it;
        if(h->m_Timeout)
        {
            h->m_Timeout -= (int) timeStep;
            if(h->m_Timeout <= 0)
            {
                h->Timeout();
            }
        }
    }
}

void
netTransactor::AddRequestHandler(netRequestHandler* handler,
                                const unsigned channelId)
{
    if(netVerifyf(!handler->m_Owner, "Handler already registered")
        && netVerifyf(m_CxnMgr, "Invalid connection manager pointer"))
    {
        m_CxnMgr->AddChannelDelegate(&handler->m_CxnDlgt, channelId);
        m_RqstHandlers.push_back(handler);
        handler->m_Owner = this;
    }
}

void
netTransactor::RemoveRequestHandler(netRequestHandler* handler)
{
    //Silently allow passing of non-registered handler, i.e. m_Owner == NULL.
    if(handler->m_Owner)
    {
        if(netVerifyf(this == handler->m_Owner, "Handler not registered to this transactor")
            && netVerifyf(m_CxnMgr, "Invalid connection manager pointer"))
        {
            handler->m_Owner = 0;
            if(handler->m_CxnDlgt.IsRegistered())
            {
                m_CxnMgr->RemoveChannelDelegate(&handler->m_CxnDlgt);
            }
            m_RqstHandlers.erase(handler);
        }
    }
}

bool
netTransactor::SendRequest(const int cxnId,
							unsigned* txId,
                            const void* data,
                            const unsigned sizeofData,
                            const int timeout,
                            netResponseHandler* handler)
{
    if(netVerifyf(m_CxnMgr, "Invalid connection manager pointer"))
    {
        netDebug2("%08x.%d[]:TXRQST[%s]->",
                    cxnId,
                    m_CxnMgr->GetChannelId(cxnId),
                    netMessage::IsMessage(data, sizeofData)
                        ? netMessage::GetName(data, sizeofData)
                        : "");

		u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
		unsigned tmpTxId;
		const unsigned sizeofHdr =
			netTransaction::InitRequest(buf, sizeof(buf), &tmpTxId);
		if(handler){handler->m_TxId = tmpTxId;}
		if(txId){*txId = tmpTxId;}

        return sizeofHdr
                && (sizeofHdr + sizeofData <= sizeof(buf))
                && sysMemCpy(&buf[sizeofHdr], data, sizeofData)
                && this->SendRqstBuf(cxnId,
                                    buf,
                                    sizeofHdr + sizeofData,
                                    timeout,
                                    handler);
    }

    return false;
}

bool
netTransactor::SendRequestOutOfBand(const EndpointId endpointId,
									const unsigned channelId,
									unsigned* txId,
									const void* data,
									const unsigned sizeofData,
									const unsigned timeout,
									netResponseHandler* handler)
{
	netDebug2("%u.%d[]:TXRQST[%s]->",
				endpointId,
				channelId,
				netMessage::IsMessage(data, sizeofData)
				? netMessage::GetName(data, sizeofData)
				: "");

    u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
    unsigned tmpTxId;
    const unsigned sizeofHdr =
        netTransaction::InitRequest(buf, sizeof(buf), &tmpTxId);
    if(handler){handler->m_TxId = tmpTxId;}
    if(txId){*txId = tmpTxId;}

    return sizeofHdr
            && (sizeofHdr + sizeofData <= sizeof(buf))
            && sysMemCpy(&buf[sizeofHdr], data, sizeofData)
            && this->SendRqstBufOutOfBand(endpointId,
										channelId,
										buf,
										sizeofHdr + sizeofData,
										timeout,
										handler);
}

bool
netTransactor::SendRequestOutOfBand(const netAddress& addr,
									const unsigned channelId,
									unsigned* txId,
									const void* data,
									const unsigned sizeofData,
									const unsigned timeout,
									netResponseHandler* handler)
{
    netDebug2("*.%d[" NET_ADDR_FMT "]:TXRQST[%s]->",
                channelId,
                NET_ADDR_FOR_PRINTF(addr),
                netMessage::IsMessage(data, sizeofData)
                    ? netMessage::GetName(data, sizeofData)
                    : "");

    u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
    unsigned tmpTxId;
    const unsigned sizeofHdr =
        netTransaction::InitRequest(buf, sizeof(buf), &tmpTxId);
    if(handler){handler->m_TxId = tmpTxId;}
    if(txId){*txId = tmpTxId;}

    return sizeofHdr
            && (sizeofHdr + sizeofData <= sizeof(buf))
            && sysMemCpy(&buf[sizeofHdr], data, sizeofData)
            && this->SendRqstBufOutOfBand(addr,
                                channelId,
                                buf,
                                sizeofHdr + sizeofData,
                                timeout,
                                handler);
}

bool
netTransactor::ResendRequestOutOfBand(const EndpointId endpointId,
									 const unsigned channelId,
									 const unsigned txId,
									 const void* data,
									 const unsigned sizeofData,
									 const unsigned timeout,
									 netResponseHandler* handler)
{
	netDebug2("%u.%d:TXRQST[%s]->",
			  endpointId,
		      channelId,
		      netMessage::IsMessage(data, sizeofData) 
                  ? netMessage::GetName(data, sizeofData) : "");

	u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
	const unsigned sizeofHdr =
		netTransaction::InitRequest(buf, sizeof(buf), txId);
	if(handler){handler->m_TxId = txId;}

	return sizeofHdr
		&& (sizeofHdr + sizeofData <= sizeof(buf))
		&& sysMemCpy(&buf[sizeofHdr], data, sizeofData)
		&& this->SendRqstBufOutOfBand(endpointId,
									 channelId,
									 buf,
									 sizeofHdr + sizeofData,
									 timeout,
									 handler);
}

bool
netTransactor::ResendRequestOutOfBand(const netAddress& addr,
									const unsigned channelId,
									const unsigned txId,
									const void* data,
									const unsigned sizeofData,
									const unsigned timeout,
									netResponseHandler* handler)
{
	netDebug2("*.%d[" NET_ADDR_FMT "]:TXRQST[%s]->",
		      channelId,
		      NET_ADDR_FOR_PRINTF(addr),
		      netMessage::IsMessage(data, sizeofData) 
                  ? netMessage::GetName(data, sizeofData) : "");

	u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
	const unsigned sizeofHdr =
		netTransaction::InitRequest(buf, sizeof(buf), txId);
	if(handler){handler->m_TxId = txId;}

	return sizeofHdr
		&& (sizeofHdr + sizeofData <= sizeof(buf))
		&& sysMemCpy(&buf[sizeofHdr], data, sizeofData)
		&& this->SendRqstBufOutOfBand(addr,
		                     channelId,
		                     buf,
		                     sizeofHdr + sizeofData,
		                     timeout,
		                     handler);
}

void
netTransactor::CancelRequest(netResponseHandler* handler)
{
    handler->Cancel();
}

bool
netTransactor::SendResponse(const netTransactionInfo& txInfo,
                            const void* data,
                            const unsigned sizeofData)
{
	netDebug2("%08x.%d[" NET_ADDR_FMT "]:TXRESP[%s]->",
                txInfo.m_CxnId,
                txInfo.m_ChannelId,
                NET_ADDR_FOR_PRINTF(txInfo.m_Addr),
                netMessage::IsMessage(data, sizeofData)
                    ? netMessage::GetName(data, sizeofData)
                    : "");

    u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
    const unsigned sizeofHdr =
        netTransaction::InitResponse(buf, sizeof(buf), txInfo.m_TxId);

    return sizeofHdr
            && (sizeofHdr + sizeofData <= sizeof(buf))
            && sysMemCpy(&buf[sizeofHdr], data, sizeofData)
            && this->SendRespBuf(txInfo, buf, sizeofHdr + sizeofData);
}

//private:

void
netTransactor::AbortRequest(netResponseHandler* handler)
{
    if(netVerifyf(this == handler->m_Owner, "Handler not registered to this transactor")
        && netVerifyf(m_CxnMgr, "Invalid connection manager pointer"))
    {
        handler->m_Owner = 0;
        m_CxnMgr->RemoveChannelDelegate(&handler->m_CxnDlgt);
        m_RespHandlers.erase(handler);
    }
}

bool
netTransactor::SendRqstBuf(const int cxnId,
                            const void* buf,
                            const unsigned sizeofBuf,
                            const int timeout,
                            netResponseHandler* handler)
{
    bool success = false;

    if(netVerify(!handler || !handler->m_Owner)
        && netVerifyf(m_CxnMgr, "Invalid connection manager pointer"))
    {
        netAssert(timeout >= 0);

		const unsigned endpointId = m_CxnMgr->GetEndpointId(cxnId);
        const unsigned chId = m_CxnMgr->GetChannelId(cxnId);

        if(netVerifyf(NET_INVALID_CHANNEL_ID != chId,
                    "Invalid channel ID:%d for connection:%08x ", chId, cxnId))
        {
            if(handler)
            {
                handler->m_Owner = this;
                handler->m_EndpointId = endpointId;
				handler->m_ChannelId = chId;
				handler->m_CxnId = cxnId;
                handler->m_Addr = m_CxnMgr->GetAddress(endpointId);
                handler->m_Timeout = timeout;
                handler->m_Result = -1;
                m_CxnMgr->AddChannelDelegate(&handler->m_CxnDlgt, chId);
                m_RespHandlers.push_back(handler);

                if(m_CxnMgr->Send(cxnId, buf, sizeofBuf, NET_SEND_RELIABLE, &handler->m_Seq))
                {
                    success = true;
                }
                else
                {
                    handler->Abort();
                }
            }
            else if(m_CxnMgr->Send(cxnId, buf, sizeofBuf, NET_SEND_RELIABLE, NULL))
            {
                success = true;
            }
        }
    }

    return success;
}

bool
netTransactor::SendRqstBufOutOfBand(const EndpointId endpointId,
									const unsigned channelId,
									const void* buf,
									const unsigned sizeofBuf,
									const int timeout,
									netResponseHandler* handler)
{
    bool success = false;

    if(netVerifyf(!handler->m_Owner, "Handler already registered")
        && netVerifyf(m_CxnMgr, "Invalid connection manager pointer"))
    {
        netAssert(timeout >= 0);

        if(handler)
        {
            handler->m_Owner = this;
            m_CxnMgr->AddChannelDelegate(&handler->m_CxnDlgt, channelId);
            m_RespHandlers.push_back(handler);

            if(m_CxnMgr->SendOutOfBand(endpointId, channelId, buf, sizeofBuf, 0))
            {
				handler->m_CxnId = -1;
				handler->m_EndpointId = endpointId;
				handler->m_ChannelId = channelId;
				handler->m_Addr = m_CxnMgr->GetAddress(endpointId);
                handler->m_Timeout = timeout;
                handler->m_Result = -1;
                success = true;
            }
            else
            {
                handler->Abort();
            }
        }
        else if(m_CxnMgr->SendOutOfBand(endpointId, channelId, buf, sizeofBuf, 0))
        {
            success = true;
        }
    }

    return success;
}

bool
netTransactor::SendRqstBufOutOfBand(const netAddress& addr,
									const unsigned channelId,
									const void* buf,
									const unsigned sizeofBuf,
									const int timeout,
									netResponseHandler* handler)
{
    bool success = false;

    if(netVerifyf(!handler->m_Owner, "Handler already registered")
        && netVerifyf(m_CxnMgr, "Invalid connection manager pointer"))
    {
        netAssert(timeout >= 0);

        if(handler)
        {
            handler->m_Owner = this;
            m_CxnMgr->AddChannelDelegate(&handler->m_CxnDlgt, channelId);
            m_RespHandlers.push_back(handler);

            if(m_CxnMgr->SendOutOfBand(addr, channelId, buf, sizeofBuf, 0))
            {
				handler->m_CxnId = -1;
				handler->m_EndpointId = NET_INVALID_ENDPOINT_ID;
                handler->m_ChannelId = channelId;
                handler->m_Addr = addr;
                handler->m_Timeout = timeout;
                handler->m_Result = -1;
                success = true;
            }
            else
            {
                handler->Abort();
            }
        }
        else if(m_CxnMgr->SendOutOfBand(addr, channelId, buf, sizeofBuf, 0))
        {
            success = true;
        }
    }

    return success;
}

bool
netTransactor::SendRespBuf(const netTransactionInfo& txInfo,
                            const void* buf,
                            const unsigned sizeofBuf)
{
    bool success = false;

	if(netVerifyf(m_CxnMgr, "Invalid connection manager pointer"))
    {
		if(txInfo.m_CxnId >= 0)
		{
	        success = m_CxnMgr->IsOpen(txInfo.m_CxnId)
    	            && m_CxnMgr->Send(txInfo.m_CxnId,
        	                        buf,
            	                    sizeofBuf,
                	                NET_SEND_RELIABLE,
                    	            NULL);

		}
		else
		{
			if(NET_IS_VALID_ENDPOINT_ID(txInfo.m_EndpointId))
			{
				success = m_CxnMgr->SendOutOfBand(txInfo.m_EndpointId,
												txInfo.m_ChannelId,
												buf,
												sizeofBuf,
												0);
			}
			else
			{
				success = m_CxnMgr->SendOutOfBand(txInfo.m_Addr,
												txInfo.m_ChannelId,
												buf,
												sizeofBuf,
												0);
			}
		}
    }

	if(!success)
	{
		netWarning("SendRespBuf :: failed to send response buffer.");
	}

    return success;
}

}   //namespace rage

///////////////////////////////////////////////////////////////////////////////
// Unit tests
///////////////////////////////////////////////////////////////////////////////

#ifndef QA_ASSERT_ON_FAIL
#define QA_ASSERT_ON_FAIL   0
#endif
#include "qa/qa.h"

#if 0 && __QA

#include "net/nethardware.h"
#include "net/message.h"
#include "system/simpleallocator.h"

//#define QA_MAKE_RAND_SEED   1060657561
#define QA_MAKE_RAND_SEED   int(sysTimer::GetSystemMsTime())

//#define QA_PRINT(a)    Displayf a
#define QA_PRINT(a)

#ifndef QA_NET_DEBUG_LEVEL
#define QA_NET_DEBUG_LEVEL  0
#endif

//Waits until data is available on the socket.
//This macro is used before any function call where we expect
//to receive data on the socket.
#define WAIT_FOR_SOCKET(s)    while(!s->IsReceivePending());

using namespace rage;

static const unsigned QA_NET_CHANNEL_ID = 13;

class qa_NrFooMsg
{
public:

    qa_NrFooMsg()
        : m_Id(456)
    {
        safecpy(m_Str, "Foo", sizeof(m_Str));
    }

    NET_MESSAGE_DECL(qa_NrFooMsg, QA_NRFOOMSG);

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerInt(msg.m_Id, 32)
                && bb.SerStr(msg.m_Str, sizeof(msg.m_Str));
    }

    int m_Id;
    char m_Str[32];
};

class qa_NrBarMsg
{
public:

    qa_NrBarMsg()
        : m_Id(456)
    {
        safecpy(m_Str, "Bar", sizeof(m_Str));
    }

    NET_MESSAGE_DECL(qa_NrBarMsg, QA_NRBARMSG);

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerInt(msg.m_Id, 32)
                && bb.SerStr(msg.m_Str, sizeof(msg.m_Str));
    }

    int m_Id;
    char m_Str[32];
};

NET_MESSAGE_IMPL(qa_NrFooMsg);
NET_MESSAGE_IMPL(qa_NrBarMsg);

class qa_netRequest : public qaItem
{
public:

    static const int MAX_CXNS       = 8;

    qa_netRequest()
        : m_SocketA(0)
        , m_SocketB(0)
        , m_AllocatorA(m_HeapA, sizeof(m_HeapA))
        , m_AllocatorB(m_HeapB, sizeof(m_HeapB))
    {
    }

    void Init()
    {
        static bool s_IsInitialized = false;
        if(!s_IsInitialized)
        {
#if defined(QA_NET_DEBUG_LEVEL)
            netDebugSetLevel(QA_NET_DEBUG_LEVEL);
#endif

            s_IsInitialized = true;
        }

        AutoIdInit();
        netHardware::StartNetwork();
        m_SocketA = netHardware::CreateSocket(0, NET_PROTO_UDP);
        m_SocketB = netHardware::CreateSocket(0, NET_PROTO_UDP);

        //Pass 1 as the hint to number of cxns to test cxn allocation.
        m_CxnMgrA.Init(&m_AllocatorA, 1, m_SocketA, -1);
        m_CxnMgrB.Init(&m_AllocatorB, 1, m_SocketB, -1);

        m_DlgtA.Bind(this, &qa_netRequest::OnCxnEvent);
        m_DlgtB.Bind(this, &qa_netRequest::OnCxnEvent);

        m_CxnMgrA.AddDelegate(&m_DlgtA, QA_NET_CHANNEL_ID);
        m_CxnMgrB.AddDelegate(&m_DlgtB, QA_NET_CHANNEL_ID);

        m_Requester.Init(&m_CxnMgrA, QA_NET_CHANNEL_ID, 0);
        m_Responder.Init(&m_CxnMgrB, QA_NET_CHANNEL_ID);
        m_Responder.Bind(this, &qa_netRequest::OnRequestEvent);

        m_ReceivedRqst = m_ReceivedResp = false;
    }

    void Shutdown()
    {
        m_Requester.Shutdown();
        m_Responder.Shutdown();
        m_Responder.Unbind();

        m_CxnMgrA.RemoveChannelDelegate(&m_DlgtA);
        m_CxnMgrB.RemoveChannelDelegate(&m_DlgtB);

        m_CxnMgrB.Shutdown();
        m_CxnMgrA.Shutdown();

        if(m_SocketA)
        {
            netHardware::DestroySocket(m_SocketA);
            m_SocketA = 0;
        }

        if(m_SocketB)
        {
            netHardware::DestroySocket(m_SocketB);
            m_SocketB = 0;
        }
        
        netHardware::StopNetwork();
        AutoIdShutdown();
    }

	void Update(qaResult& result);

    void OnResponseEvent(netRequester::Delegate* /*dlgt*/,
                        const netResponseEvent* evt)
    {
        qa_NrBarMsg barMsg;

        if(barMsg.Import(evt->m_Data, evt->m_SizeofData))
        {
            m_ReceivedResp = true;
        }
    }

    void OnRequestEvent(netResponder* /*responder*/,
                        const netRequestEvent* evt)
    {
        qa_NrFooMsg fooMsg;

        if(fooMsg.Import(evt->m_Data, evt->m_SizeofData))
        {
            m_TxInfo = evt->m_TxInfo;
            m_ReceivedRqst = true;
        }
    }

    void OnCxnEvent(netConnectionManager* /*cxnMgr*/,
                    const netEvent* /*evt*/)
    {
    }

    netRequester m_Requester;
    netResponder m_Responder;

    netSocket* m_SocketA;
    netSocket* m_SocketB;
    netConnectionManager m_CxnMgrA, m_CxnMgrB;
    netConnectionManager::Delegate m_DlgtA, m_DlgtB;
    u8 m_HeapA[64 * 1024];
    u8 m_HeapB[64 * 1024];
    sysMemSimpleAllocator m_AllocatorA;
    sysMemSimpleAllocator m_AllocatorB;
    netTransactionInfo m_TxInfo;

    bool m_ReceivedRqst;
    bool m_ReceivedResp;
};

void
qa_netRequest::Update(qaResult& result)
{
    static const int UPTATE_INTERVAL    = 500;

    netRequester::Delegate respDlgt;
    respDlgt.Bind(this, &qa_netRequest::OnResponseEvent);

    unsigned curTime = 0;

    netStatus cxnStatus;

    const int cxnIdA =
        m_CxnMgrA.OpenConnection(m_SocketB->GetAddress(), QA_NET_CHANNEL_ID, NULL, 0, &cxnStatus);

    m_CxnMgrB.OpenConnection(m_SocketA->GetAddress(), QA_NET_CHANNEL_ID, NULL, 0, NULL);

    //A: Send cxn request to B
    m_CxnMgrA.Update(curTime);

    //B: Receive and process A's cxn request.
    WAIT_FOR_SOCKET(m_SocketB);
    m_CxnMgrB.Update(curTime);
    curTime += 100;

    //A: Receive and process B's cxn accept.
    WAIT_FOR_SOCKET(m_SocketA);
    m_CxnMgrA.Update(curTime);
    curTime += UPTATE_INTERVAL;

    QA_CHECK(cxnStatus.Succeeded());

    qa_NrFooMsg fooMsg;
    QA_CHECK(m_Requester.Send(cxnIdA, fooMsg, 0, &respDlgt));
    //A: Send request to B
    m_CxnMgrA.Update(curTime);
    m_Requester.Update(curTime);
    curTime += UPTATE_INTERVAL;

    QA_CHECK(respDlgt.Pending());
    QA_CHECK(!respDlgt.Acked());
    QA_CHECK(!respDlgt.Answered());

    //B: Receive process A's request.
    WAIT_FOR_SOCKET(m_SocketB);
    m_CxnMgrB.Update(curTime);
    curTime += UPTATE_INTERVAL;

    QA_CHECK(respDlgt.Pending());
    QA_CHECK(!respDlgt.Acked());
    QA_CHECK(!respDlgt.Answered());

    QA_CHECK(m_ReceivedRqst);

    //A: Receive and process B's ACK.
    WAIT_FOR_SOCKET(m_SocketA);
    m_CxnMgrA.Update(curTime);
    m_Requester.Update(curTime);
    curTime += UPTATE_INTERVAL;

    QA_CHECK(respDlgt.Pending());
    QA_CHECK(respDlgt.Acked());
    QA_CHECK(!respDlgt.Answered());

    qa_NrBarMsg barMsg;
    netVerify(m_Responder.Send(m_TxInfo, barMsg));

    //B: Send the response
    m_CxnMgrB.Update(curTime);
    curTime += UPTATE_INTERVAL;

    //A: Receive and process B's response.
    WAIT_FOR_SOCKET(m_SocketA);
    m_CxnMgrA.Update(curTime);
    m_Requester.Update(curTime);
    curTime += UPTATE_INTERVAL;

    QA_CHECK(m_ReceivedResp);

    QA_CHECK(!respDlgt.Pending());
    QA_CHECK(respDlgt.Acked());
    QA_CHECK(respDlgt.Answered());

    ////////Test cancellation.

    QA_CHECK(m_Requester.Send(cxnIdA, fooMsg, 0, &respDlgt));
    //A: Send request to B
    m_CxnMgrA.Update(curTime);
    m_Requester.Update(curTime);
    curTime += UPTATE_INTERVAL;

    QA_CHECK(respDlgt.Pending());
    QA_CHECK(!respDlgt.Canceled());

    respDlgt.Cancel();

    QA_CHECK(!respDlgt.Pending());
    QA_CHECK(respDlgt.Canceled());

    ////////Test timeouts.

    QA_CHECK(m_Requester.Send(cxnIdA, fooMsg, UPTATE_INTERVAL * 3, &respDlgt));
    //A: Send request to B
    m_CxnMgrA.Update(curTime);
    m_Requester.Update(curTime);
    curTime += UPTATE_INTERVAL;

    QA_CHECK(respDlgt.Pending());
    QA_CHECK(!respDlgt.TimedOut());

    m_Requester.Update(curTime);
    curTime += UPTATE_INTERVAL;

    QA_CHECK(respDlgt.Pending());
    QA_CHECK(!respDlgt.TimedOut());

    m_Requester.Update(curTime);
    curTime += UPTATE_INTERVAL;

    QA_CHECK(!respDlgt.Pending());
    QA_CHECK(respDlgt.TimedOut());

    TST_PASS;
}

//QA_ITEM_FAMILY(qa_netRequest, (), ());

//QA_ITEM(qa_netRequest, (), qaResult::PASS_OR_FAIL);

#endif  //__QA
