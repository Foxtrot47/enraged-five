// 
// net/netrelaytoken.h 
// 
// Copyright (C) 1999-2020 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_RELAY_TOKEN_H
#define NET_RELAY_TOKEN_H

namespace rage
{

class netRelayToken
{
public:

	static const unsigned MAX_TOKEN_BUF_SIZE = 6;
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = MAX_TOKEN_BUF_SIZE;
	
	// 2 hex chars per byte + 2 for "0x", + 1 for null terminator
	static const unsigned TO_HEX_STRING_BUFFER_SIZE = (MAX_TOKEN_BUF_SIZE * 2) + 2 + 1;

	static const netRelayToken INVALID_RELAY_TOKEN;

	netRelayToken();
    netRelayToken(const u8 (&tokenBuf)[MAX_TOKEN_BUF_SIZE]);

    //PURPOSE
    //  Invalidates the token.
    void Clear();

    //PURPOSE
    //  Returns true if the token is valid.
    bool IsValid() const;

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    bool operator==(const netRelayToken& that) const;
    bool operator!=(const netRelayToken& that) const;

    //PURPOSE
    //  Implemented for the benefit of containers like std::map,
	//  and templated versions of Min/Max.
    bool operator<(const netRelayToken& that) const;
	bool operator>(const netRelayToken& that) const;

    //PURPOSE
    //  Creates a null-terminated string representing the token.
    //PARAMS
    //  dst         - Destination char array.
    //  dstLen      - Number of chars in the array.
    const char* ToHexString(char* dst,
                        const unsigned dstLen) const;
    template<int SIZE>
    const char* ToHexString(char (&buf)[SIZE]) const
    {
        return ToHexString(buf, SIZE);
    }

private:
    u8 m_Token[MAX_TOKEN_BUF_SIZE];
};

}   //namespace rage

#endif  //NET_RELAY_TOKEN_H
