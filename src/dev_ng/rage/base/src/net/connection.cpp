// 
// net/connection.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "connection.h"

#include "connectionmanager.h"

#include "message.h"
#include "diag/seh.h"
#include "net/netdiag.h"
#include "profile/rocky.h"
#include "string/string.h"
#include "string/stringutil.h"
#include "system/timer.h"
#include "time.h"

//CAUTION!!!
//
//When doing math on sequence numbers be sure to use appropriate casting.
//
//netSequence a = 0;
//netSequence b = 65535;
//bool areEqual = (b == (a - 1));               //INCORRECT!
//
//bool areEqual2 = (b == netSequence(a - 1));   //CORRECT!
//

//A word about frame dependencies.
//
//netConnection implements a single stream for both reliable and unreliable
//frames.  In other words, reliable and unreliable frames share the same
//sequence space.
//
//For example, assume the following frame sequence, where "r" indicates a
//frame sent reliably.
//
//13r 14 15 16 17r 18 19r 20
//
//Because of packet loss and lag the receiver might receive the frames
//in the following order:
//
//16 17r 14 13r 19r 20
//
//To be able to deliver the frames to the app the receiver must wait until
//all gaps in the sequence are filled.  This, of course, may never happen
//because some frames are sent unreliably and can be lost (e.g. frame 15).
//
//Additionally, frame 13r was received after frame 17r.  Somehow the receiver
//needs to know not to deliver 17r until it receives 13r.  If frame 13 were
//unreliable this would not be the case (the receiver could simply assume
//frame 13 was lost and deliver frame 17r).
//In order for the receiver to know it needs to hold 17r until it receives
//13r, 17r must contain dependency information - i.e. 17r depends on 13r.
//
//Every frame sent (both unreliable and reliable) contains a dependency - a
//prior frame which must be received before the current frame can be dispatched
//to the app.

#include "string/string.h"
#include "netdiag.h"
#include "system/bootmgr.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, cxn)
#undef __rage_channel
#define __rage_channel ragenet_cxn

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt)\
    "%08x.%d[" NET_ADDR_FMT "]: " fmt, m_Id, m_ChannelId, m_EpOwner ? NET_ADDR_FOR_PRINTF(m_EpOwner->GetAddress()) : ""

#if !__NO_OUTPUT
//Show warnings when we fail to allocate mem for inbound/outbound frames.
//But limit the amount of spew so it doesn't fill up the TTY.

static bool s_EnableMemWarnings = true;
static volatile u32 s_FrmMemSpewLimiter = 0;

#define frmMemWarning(inout, seq)\
    while(s_EnableMemWarnings && !sysHangDetected) \
    {\
        if(0 == (sysInterlockedAdd(&s_FrmMemSpewLimiter, 1) % 10))\
        {\
            netWarning("Failure allocating memory for %s frame [sq: %d]", inout, seq);\
        }\
        break;\
    }

#define frmMemResetWarningSpew()\
    ((void)sysInterlockedExchange(&s_FrmMemSpewLimiter, 0))

#else

#define frmMemWarning(inout, seq)
#define frmMemResetWarningSpew()

#endif  //!__NO_OUTPUT

//Compute the bit of a sequence number.
//NOTE:  seq *must* be larger than expectedSeq,
//i.e. netSeqGt(seq, expectedSeq) must be true.
inline
static
unsigned AckBit(const netSequence seq, const netSequence expectedSeq)
{
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT RAGE_LOG_FMT_DEFAULT

    if(!netVerify(netSeqGt(seq, expectedSeq)))
		netError("Invalid Ack Seq, Seq: %d, Expected: %d", seq, expectedSeq);

	if(!netVerify(netSeqDiff(seq, expectedSeq) <= netBundle::BIT_SIZEOF_UO_ACK_BITS))
		netError("Ack Seq Out of Range, Seq: %d, Expected: %d, Diff: %d, MaxDiff: %d", seq, expectedSeq, netSeqDiff(seq, expectedSeq), netBundle::BIT_SIZEOF_UO_ACK_BITS);

    return unsigned(1 << (netSeqDiff(seq, expectedSeq) - 1));

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt)\
	"%08x.%d[" NET_ADDR_FMT "]: " fmt, m_Id, m_ChannelId,  m_EpOwner ? NET_ADDR_FOR_PRINTF(m_EpOwner->GetAddress()) : ""
}

#if !__NO_OUTPUT
static const char* MakeAckStr(char* buf,
                            const int sizeofBuf,
                            const netSequence expectedSeq,
                            const unsigned ackBits)
{
    formatf_sized(buf, sizeofBuf, "%d", netSequence(expectedSeq - 1));
    netSequence seq = netSequence(expectedSeq + 1);
    unsigned bits = ackBits;
    for(; bits; ++seq, bits >>= 1)
    {
        if(bits & 0x01)
        {
            safecatf_sized(buf, sizeofBuf, " %d", seq);
        }
    }

    return buf;
}
#endif  //__NO_OUTPUT

///////////////////////////////////////////////////////////////////////////////
//  netConnection
///////////////////////////////////////////////////////////////////////////////

unsigned netConnection::sm_RttWarningThresholdMs = DEFAULT_RTT_WARNING_THRESHOLD_MS;

netConnection::netConnection()
    : m_Id(-1)
	, m_EpOwner(NULL)
    , m_ChannelId(NET_INVALID_CHANNEL_ID)
    , m_Allocator(NULL)
    , m_State(STATE_CLOSED)
    , m_LastKeepaliveTime(0)
    , m_LastReceiveTime(0)
    , m_TimeoutCount(0)
	, m_RttOverThresholdStartTime(0)
	, m_RttOverThresholdLastTime(0)
    , m_CxnMgr(NULL)
    , m_EventQ(NULL)
    , m_RcvdBits(0)
    , m_OutboundSeq(0)
    , m_InboundSeq(0)
    , m_Initialized(false)
    , m_NeedToAck(false)
    , m_IsTimedOut(false)
	, m_IsOutOfMemory(false)
	, m_TimeoutReason(CXN_TIMEOUT_REASON_NONE)
	, m_IsOutOfMemoryRequiredBytes(0)
	, m_NumFailedAllocs(0)
#if __BANK
	, m_IsForcedTimeOut(false)
#endif
	, m_RegisteredMainThreadHang(false)
	, m_WasMainThreadDead(false)
#if !__NO_OUTPUT
	, m_nMaxMainThreadDelta(0)
#endif
{
    CompileTimeAssert((sizeof(m_RcvdBits) << 3) >= RCV_WIN);
}

netConnection::~netConnection()
{
    this->Shutdown();
}

bool
netConnection::Init(netConnectionManager* cxnMgr,
                    netEventQueue<netEvent>* eventQ,
                    sysMemAllocator* allocator)
{
    bool success = false;

    if(netVerify(!m_Initialized))
    {
        m_Id = -1;
        m_Allocator = allocator;
        m_CxnMgr = cxnMgr;
        m_EventQ = eventQ;
		m_Initialized = true;

        success = true;
    }

    return success;
}

void
netConnection::Shutdown()
{
    if(m_Initialized)
    {
		// TODO: NS - make sure all members are cleared here
        this->Close();

        m_Id = -1;
		m_EpOwner = NULL;
		m_ChannelId = NET_INVALID_CHANNEL_ID;

		m_LastReceiveTime = 0;
		m_LastKeepaliveTime = 0;
		m_TimeoutCount = 0;
		m_RttOverThresholdStartTime = 0;
		m_RttOverThresholdLastTime = 0;
		m_IsTimedOut = false;
		m_IsOutOfMemory = false;
#if __BANK
		m_IsForcedTimeOut = false;
#endif
		m_TimeoutReason = CXN_TIMEOUT_REASON_NONE;
		m_RegisteredMainThreadHang = false;
		m_WasMainThreadDead = false;
        m_Allocator = NULL;
        m_CxnMgr = NULL;
        m_EventQ = NULL;
		m_RcvdBits = 0;
		m_OutboundSeq = 0;
		m_InboundSeq = 0;
	    m_NeedToAck = false;
#if !__NO_OUTPUT
		m_nMaxMainThreadDelta = 0;
#endif
		m_IsOutOfMemoryRequiredBytes = 0;
		m_NumFailedAllocs = 0;

		m_Initialized = false;
    }
}

bool
netConnection::Open(netEndpoint* epOwner,
                    const int cxnId,
                    const unsigned channelId)
{
    netAssert(m_Initialized);
    netAssert(STATE_CLOSED == m_State);
    netAssert(epOwner != NULL);
    netAssert(cxnId >= 0);
    netAssert(channelId <= NET_MAX_CHANNEL_ID);

    m_EpOwner = epOwner;
    m_Id = cxnId;
    m_ChannelId = channelId;
    this->Reset();

    return true;
}

bool
netConnection::Synchronize(const netSequence nextInboundSeq)
{
    bool success = false;

    if(netVerify(STATE_LISTEN == m_State || STATE_SYN_SENT == m_State))
    {
        m_InboundSeq = nextInboundSeq;
        //Assume we're synchronizing on a SYN, which is always sent reliably.
        m_LastReliableRcvd = netSequence(nextInboundSeq - 1);

        netDebug2("Synchronized inbound sequence:%d, transitioning to SYN_RECEIVED",
                    netSequence(nextInboundSeq - 1));

        if(STATE_LISTEN == m_State)
        {
            //Send an empty frame (which will send a SYN)
            this->QueueFrame(NULL, 0, NET_SEND_RELIABLE, NULL);
        }

        this->SetState(STATE_SYN_RECEIVED);
        success = true;
    }

    return success;
}

void netConnection::UpdateStats()
{
#if NET_CXNMGR_COLLECT_STATS
	if(m_EpOwner)
	{
		netEndpoint::Statistics& stats = m_EpOwner->GetStats();
		stats.m_NumFailedAllocs += m_NumFailedAllocs;
		m_NumFailedAllocs = 0;
	}
#endif
}

void
netConnection::Close()
{
    netAssert(m_Initialized);

    if(m_State != STATE_CLOSED)
    {
        this->Reset();
        m_ChannelId = NET_INVALID_CHANNEL_ID;
        this->SetState(STATE_CLOSED);
		m_EpOwner = NULL;
    }
}

void
netConnection::Reset()
{
    netAssert(m_Initialized);
    netAssert(m_ChannelId <= NET_MAX_CHANNEL_ID);

    while(!m_ResendQ.empty())
    {
        OutFrameQ::iterator it = m_ResendQ.begin();
        OutFrame* outfrm = *it;
        m_ResendQ.erase(it);
        netVerify(m_Unacked.erase(outfrm->GetSeq()));
        this->FreeOutFrame(outfrm);
    }

    while(!m_NewQ.empty())
    {
        OutFrameQ::iterator it = m_NewQ.begin();
        OutFrame* outfrm = *it;
        m_NewQ.erase(it);
        if(outfrm->IsReliable())
        {
            netVerify(m_Unacked.erase(outfrm->GetSeq()));
        }
        this->FreeOutFrame(outfrm);
    }

    while(!m_OoRcvd.empty())
    {
        InFramesBySeq::iterator it = m_OoRcvd.begin();
        InFrame* infrm = it->second;
        m_OoRcvd.erase(it);
        infrm->Dispose();
    }

    netAssert(m_Unacked.empty());

    //Don't use SetState() here!!!
    m_State = STATE_LISTEN;
    m_LastKeepaliveTime = m_LastReceiveTime = 0;
    m_TimeoutCount = 0;
	m_TimeoutReason = CXN_TIMEOUT_REASON_NONE;
	m_RttOverThresholdStartTime = 0;
	m_RttOverThresholdLastTime = 0;
	m_Policies = netConnectionPolicies();
    m_RcvdBits = 0;
    //Choose a random initial sequence number.
    netRandom r(sysTimer::GetSystemMsTime() ^ (unsigned) (size_t) this);
    static const int SHIFT  = (sizeof(netSequence) << 3) / 2;
    static const int LOMASK = (1u << SHIFT) - 1;
    if(0 == m_OutboundSeq)
    {
        //Ensure the very first sequence number is far from zero.
        m_OutboundSeq = netSequence(r.GetRanged(0, 0xFF00) + 0xFF);
    }
    else
    {
        //Ensure new sequence numbers are far away from old sequence numbers
        m_OutboundSeq = netSequence((m_OutboundSeq << SHIFT) | (r.GetInt() & LOMASK));
    }
    if(0 != m_InboundSeq)
    {
        //Ensure new sequence numbers are far away from old sequence numbers
        m_InboundSeq = netSequence((m_InboundSeq << SHIFT) | (r.GetInt() & LOMASK));
    }

    m_LastReliableRcvd = netSequence(r.GetInt());
    m_LastReliableSent = netSequence(r.GetInt());

    m_NeedToAck = false;
    m_IsTimedOut = false;
	m_IsOutOfMemory = false;
	m_IsOutOfMemoryRequiredBytes = 0;
	m_NumFailedAllocs = 0;
	m_RegisteredMainThreadHang = false;
	m_WasMainThreadDead = false;

#if __BANK
	m_IsForcedTimeOut = false; 
#endif

#if !__NO_OUTPUT
	m_nMaxMainThreadDelta = 0;
#endif
}

netConnection::State
netConnection::GetState() const
{
    return m_State;
}

int
netConnection::GetId() const
{
    return m_Id;
}

unsigned
netConnection::GetChannelId() const
{
    return m_ChannelId;
}

const netConnectionPolicies&
netConnection::GetPolicies() const
{
    return m_Policies;
}

void
netConnection::SetPolicies(const netConnectionPolicies& policies)
{
    netAssert(policies.m_ResendInterval > 0);
    netAssert(policies.m_MaxSendCount > 0);

    netDebug2("Setting connection policies:");
    netDebug2("  Resend interval:      %d (was %d)",
                policies.m_ResendInterval,
                m_Policies.m_ResendInterval);
    netDebug2("  Max send count:       %d (was %d)",
                policies.m_MaxSendCount,
                m_Policies.m_MaxSendCount);
    netDebug2("  Keepalive interval:   %d (was %d)",
                policies.m_KeepaliveInterval,
                m_Policies.m_KeepaliveInterval);
    netDebug2("  Timeout interval:     %d (was %d)",
                policies.m_TimeoutInterval,
                m_Policies.m_TimeoutInterval);
    netDebug2("  Debug spew:           %s (was %s)",
                policies.m_EnableDebugSpew ? "enabled" : "disabled",
                m_Policies.m_EnableDebugSpew ? "enabled" : "disabled");

    if(policies.m_MaxSendCount < m_Policies.m_MaxSendCount)
    {
        netDebug2("New connection policies reduced the max send count - resetting send counts on all un-ACKed frames");

        OutFrameQ::iterator it = m_ResendQ.begin();
        OutFrameQ::const_iterator stop = m_ResendQ.end();

        for(; stop != it; ++it)
        {
            (*it)->m_SendCount = 1;
        }
    }

    if(policies.m_TimeoutInterval < m_Policies.m_TimeoutInterval)
    {
        netDebug2("New connection policies reduced the timeout interval - resetting timeout");

        m_LastReceiveTime = 0;
    }

    if(policies.m_KeepaliveInterval < m_Policies.m_KeepaliveInterval)
    {
        netDebug2("New connection policies reduced the keepalive interval - adjusting keepalive");

        //This will cause a keepalive to be sent immediately.
		m_LastKeepaliveTime = sysTimer::GetSystemMsTime() - m_Policies.m_KeepaliveInterval;
	}

    m_Policies = policies;

    if(!m_Policies.m_KeepaliveInterval)
    {
        m_LastKeepaliveTime = 0;
    }

    if(!m_Policies.m_TimeoutInterval)
    {
        m_LastReceiveTime = 0;
    }
}

void
netConnection::SetRttWarningThresholdMs(const unsigned rttWarningThresholdMs)
{
	if(sm_RttWarningThresholdMs != rttWarningThresholdMs)
	{
		sm_RttWarningThresholdMs = rttWarningThresholdMs;
	}
}

netSequence
netConnection::GetNextOutboundSequence() const
{
    return m_OutboundSeq;
}

netSequence
netConnection::GetNextInboundSequence() const
{
    return m_InboundSeq;
}

unsigned
netConnection::GetAckBits() const
{
    return m_RcvdBits;
}

bool
netConnection::QueueFrame(const void* bytes,
                          const unsigned numBytes,
                          const unsigned sendFlags,
                          netSequence* frameSeq)
{
    netAssert(m_Initialized);
    netAssert(m_State <= STATE_ESTABLISHED);

    bool success = false;

    OutFrame* outfrm = 0;

    rtry
    {
        rcheck(!IsOutOfMemory(),
                catchall,
                netError("Can't send - out of memory!!!"));

        rverify(/*numBytes >= 0 && */ numBytes <= netFrame::MAX_BYTE_SIZEOF_PAYLOAD,
                catchall,
                netError("Invalid send size:%d", numBytes));

        /*netDebug2("SND(%s)[%s]->,sz:%d,sq:%d,sc:%d,sd:%d",
                    (sendFlags & NET_SEND_RELIABLE) ? "R" : "U",
                    netMessage::IsMessage(bytes, numBytes)
                    ? netMessage::GetName(bytes, numBytes)
                    : "",
                    numBytes,
                    m_OutboundSeq,
                    0,
                    m_LastReliableSent);*/

        //Are we synchronizing sequence numbers?
        const bool isSyn = (STATE_LISTEN == m_State);

		if(sendFlags & NET_SEND_OUT_FRAME)
		{
			outfrm = (netOutFrame*)bytes;
		}
		else
		{
			outfrm = this->AllocOutFrame(numBytes, sendFlags);
		}

        rcheck(outfrm, catchall, frmMemWarning("outbound", m_OutboundSeq));

        //Reset the warning spew limiter.
        frmMemResetWarningSpew();

        //This frame will depend on the most recently sent reliable frame.
        //The receiver will not process this frame until it has received
        //the frame on which it depends.  The special case is for
        //SYN frames - they depend on a fake sequence number that
        //is one less than their own sequence number.  This is directly
        //coupled to how Synchronize() initializes expected sequence
        //numbers and last reliable received.
        outfrm->Reset(numBytes,
                    m_OutboundSeq,
                    isSyn ? netSequence(m_OutboundSeq - 1) : m_LastReliableSent,
                    sendFlags | (isSyn ? NET_SEND_RELIABLE : 0),
                    NULL != frameSeq);

		outfrm->m_QueueTime = sysTimer::GetSystemMsTime();

		if(!(sendFlags & NET_SEND_OUT_FRAME))
		{
			sysMemCpy(outfrm->GetPayload(), bytes, numBytes);
		}

        if(!isSyn)
        {
            m_NewQ.push_back(outfrm);
        }
        else
        {
            //SYN frames go directly on the resend queue.
            netAssert(m_NewQ.empty());
            netAssert(m_ResendQ.empty());

            netDebug2("Sending SYN:%d, transitioning to SYN_SENT",
                        m_OutboundSeq);

            m_ResendQ.push_back(outfrm);
            this->SetState(STATE_SYN_SENT);
        }

        //Reliable frames go in the collection of un-ACKed frames.
        if(outfrm->IsReliable())
        {
            m_Unacked.insert(OutFramesBySeq::value_type(m_OutboundSeq, outfrm));
            m_LastReliableSent = m_OutboundSeq;
        }

        if(frameSeq)
        {
            *frameSeq = m_OutboundSeq;
        }

        ++m_OutboundSeq;

		//FIXME(KB) - We have very old reliable frames in our
		//resend queue.  The connection should timeout permanently.
		netAssert(m_Unacked.find(m_OutboundSeq) == m_Unacked.end());

        success = true;
    }
    rcatchall
    {
        if(outfrm && !(sendFlags & NET_SEND_OUT_FRAME))
        {
            this->FreeOutFrame(outfrm);
            outfrm = 0;
        }
    }

    return success;
}

unsigned
netConnection::Pack(void* buf, const unsigned maxLen)
{
	netAssert(maxLen >= netBundle::BYTE_SIZEOF_HEADER);

    const unsigned curTime = sysTimer::GetSystemMsTime();

    netBundle* bundle = (netBundle*) buf;
    const u8* eob = ((u8*) buf) + maxLen;
    u8* p = (u8*) bundle->GetPayload();

    int numBytes = 0;
    int numFrames = 0;
	unsigned framesResent = 0;
	unsigned framesSentReliably = 0;

    bool timedout = false;

    const bool isEstablished = (STATE_ESTABLISHED == m_State);

	//Start packing from the resend queue first until we hit a reliable
	//frame that isn't ready to be resent, then start packing
	//from the new queue. This avoids a problem where the new queue
	//could starve out the resend queue if there is a constant influx of
	//new frames. If we're not yet established then only send from
	//the resend queue, which should contain our SYN frame.

	OutFrameQ* queues[2] =
	{
		&m_ResendQ,
		isEstablished ? &m_NewQ : 0
	};

    //Sequence number of first unreliable frame to be packed into the bundle.
    netSequence firstSeq = 0;
    bool haveFirstSeq = false;

    //Continue packing frames until we run out of space or we reach
    //a frame that's not ready for resend.
    //Note that frames in the resend queue are in re-send order, not
    //necessarily sequence order.
    for(int i = 0; i < 2; ++i)
    {
        OutFrameQ* curQ = queues[i];
		
        if(!curQ){continue;}

        OUTPUT_ONLY(int index = 0);

		int count = int(curQ->size());

		while(count--)
		{
			netAssert(p <= eob);

			OutFrame* outfrm = curQ->front();
			
            //Make sure frames with non-zero send count are in the
            //unacked collection.
            netAssert(0 == outfrm->m_SendCount
                    || m_Unacked.find(outfrm->GetSeq()) != m_Unacked.end());

            netAssert(0 == outfrm->m_SendCount
                    || int(curTime - outfrm->m_LastSendTime) >= 0);

           // check if this is a resend...
			if(outfrm->m_SendCount > 0)
			{
				// if it's not time to resend, break 
				// any further packets in this queue (the resend queue) will have a m_LastSendTime that
				// is less than this packet so it's safe to break here
				if((curTime - outfrm->m_LastSendTime) < m_Policies.m_ResendInterval)
					break; 

				// do not allow resends if the receive thread hasn't processed since we packed this frame
				if(m_CxnMgr && (m_CxnMgr->GetRecvPackFrameMark() < outfrm->m_PackFrame))
				{
					// log this so we can track this...
					netDebug2("[%u/%u] SKIP_RESEND(%s)[%s]->,ch:%u,sq:%d,sc:%d,sd:%d,rp:%u,pk:%u",
							  m_CxnMgr ? m_CxnMgr->GetSendFrame() : 0,
							  m_CxnMgr ? m_CxnMgr->GetPackFrame() : 0,
							  outfrm->IsReliable() ? "R" : "U",
							  netMessage::IsMessage(outfrm->GetPayload(), outfrm->GetSizeofPayload()) ? netMessage::GetName(outfrm->GetPayload(), outfrm->GetSizeofPayload()) : "",
							  this->m_ChannelId,
							  outfrm->GetSeq(),
							  outfrm->m_SendCount,
							  outfrm->GetSeqDep(),
							  m_CxnMgr ? m_CxnMgr->GetRecvPackFrameMark() : 0,
							  outfrm->m_PackFrame);

						break;
				}
			}
            
			//Will it fit?
			const unsigned fsize = outfrm->GetSize();
			if(fsize > unsigned(eob - p))
			{
				break;
			}

			{
				++outfrm->m_SendCount;
				outfrm->m_SendFrame = m_CxnMgr ? m_CxnMgr->GetSendFrame() : 0;
				outfrm->m_PackFrame = m_CxnMgr ? m_CxnMgr->GetPackFrame() : 0;

				//Did we exceed the maximum send count?
				const unsigned timeoutCount = this->GetTimeoutCount(outfrm);
				if(timeoutCount)
				{
					netDebug2("frame:%d send count:%d exceeds maximum:%d",
						outfrm->GetSeq(),
						outfrm->m_SendCount,
						m_Policies.m_MaxSendCount);

					if(1 == timeoutCount)
					{
						//m_TimeoutCount counts the number of queued
						//frames that have timed out, so increment only
						//the first time a frame times out.
						++m_TimeoutCount;
					}

					if(!timedout)
					{
						if(1 == m_TimeoutCount)
						{
							netWarning("QueueTimeout due to un-ACKed messages");
						}

						TimeoutReason timeoutReason = CXN_TIMEOUT_REASON_UNACKED_FRAMES;

						if(m_CxnMgr && m_EpOwner && (m_Policies.m_TimeoutInterval > 0))
						{
							// if we haven't received anything for the majority of the p2p timeout, then the
							// timeout reason is really due to nothing received at all

							const unsigned timeSinceLastReceive = m_CxnMgr->GetDeltaLastReceiveTime(m_EpOwner->GetId());

							const float P2P_TIMEOUT_PCT = 0.75f;
							if(timeSinceLastReceive > (m_Policies.m_TimeoutInterval * P2P_TIMEOUT_PCT))
							{
								// nothing received
								timeoutReason = CXN_TIMEOUT_REASON_NO_KEEPALIVES;
							}
						}

						timedout = this->QueueTimeout(timeoutReason);
					}
				}

				netDebug2("[%u/%u][ql:%u,%u,qi:%d] FRM(%s)[%s]->,ch:%u,age:%u,sz:%d,sq:%d,sc:%d,sd:%d",
						  m_CxnMgr ? m_CxnMgr->GetSendFrame() : 0,
						  m_CxnMgr ? m_CxnMgr->GetPackFrame() : 0,
						  GetNewQueueLength(),
						  GetResendQueueLength(),
						  index,
						  outfrm->IsReliable() ? "R" : "U",
						  netMessage::IsMessage(outfrm->GetPayload(), outfrm->GetSizeofPayload())
						  ? netMessage::GetName(outfrm->GetPayload(), outfrm->GetSizeofPayload())
						  : "",
						  this->m_ChannelId,
						  curTime - outfrm->m_QueueTime,
						  fsize,
						  outfrm->GetSeq(),
						  outfrm->m_SendCount,
						  outfrm->GetSeqDep());

				sysMemCpy(p, outfrm->m_Frame, fsize);
				p += fsize;

				++numFrames;
				
				curQ->pop_front();

				if(outfrm->IsReliable())
				{
					framesSentReliably++;

					if(outfrm->m_SendCount > 1)
					{
						framesResent++;
					}

					outfrm->m_LastSendTime = curTime;

					//Move to the back of the re-send queue.
					m_ResendQ.push_back(outfrm);
				}
				else
				{
					//Unreliables should not be in the unacked frames.
					netAssertf(m_Unacked.count(outfrm->GetSeq()) == 0, "Unreliable frame [sq: %d] found in unacked queue", outfrm->GetSeq());

					if(!haveFirstSeq)
					{
						firstSeq = outfrm->GetSeq();
						haveFirstSeq = true;
					}

					//Unreliables are chucked after the first send.
					this->FreeOutFrame(outfrm);
				}
			}

			OUTPUT_ONLY(++index);
        }
    }

	const bool timeForKeepalive = (0 == numFrames) && this->TimeForKeepalive();

	// detect if the main thread has hung, which would prevent us from dispatching messages.
	// To make sure we send our remote timeout request before remote peers time us out on their end,
	// we detect that the main thread has hung if it has been unresponsive for > 90% of the P2P timeout.
	// This is to ensure we attribute the associated peer complaint telemetry to the stall/hang.
	const float P2P_TIMEOUT_PCT_TO_DETECT_HANG = 0.9f;
	const unsigned nMainThreadDelta = m_CxnMgr ? m_CxnMgr->GetDeltaLastMainThreadUpdateTime() : 0;
	const bool bIsMainThreadDead = (m_Policies.m_TimeoutInterval > 0) &&
									m_CxnMgr &&
									m_CxnMgr->HasValidLastMainThreadUpdateTime() &&
									(nMainThreadDelta > (m_Policies.m_TimeoutInterval * P2P_TIMEOUT_PCT_TO_DETECT_HANG));
	
	// if the main thread is dead, 
	if(bIsMainThreadDead)
	{
		if(!m_RegisteredMainThreadHang)
		{
			netError("NetStallDetect :: Main thread stall of (at least) %ums (Timeout: %ums)", nMainThreadDelta, m_Policies.m_TimeoutInterval);
			netError("NetStallDetect :: QueueTimeout due to stall");

			// send a remote timeout request
			if(this->m_EpOwner)
			{
				m_CxnMgr->SendRemoteTimeoutRequest(this->m_EpOwner->GetId(), this->GetChannelId(), CXN_TIMEOUT_REASON_REMOTE_STALL);
			}

			// ... and queue a timeout on this connection
			this->QueueTimeout(CXN_TIMEOUT_REASON_LOCAL_STALL);

			// flag that we acknowledged this
			m_RegisteredMainThreadHang = true;
		}
#if !__NO_OUTPUT
		m_nMaxMainThreadDelta = nMainThreadDelta;
#endif
	}
	else 
	{
		// check if we've recovered
		m_RegisteredMainThreadHang = false;
		if(m_WasMainThreadDead)
		{
			netError("NetStallDetect :: Main thread recovered. Stall time: %ums", m_nMaxMainThreadDelta);
		}

		if(numFrames > 0 || m_NeedToAck || timeForKeepalive)
		{
			m_NeedToAck = false;

			//Send a bundle only if we're ESTABLISHED.  If we're not
			//ESTABLISHED then only if the bundle contains a frame.  Don't
			//send a bundle that is only an ACK (i.e. with no frames) if
			//we're not ESTABLISHED.  During the sync phase bundles
			//must contain at least one frame so the remote peer can use it
			//to determine our initial sequence number.
			if(STATE_ESTABLISHED == m_State || numFrames > 0)
			{
	#if !__NO_OUTPUT
				{
					char buf[256];

					netDebug2("[%u/%u][ql:%u,%u] %s->,sz:%d,ib:%d,rb:%d,sq:%s",
							  m_CxnMgr ? m_CxnMgr->GetSendFrame() : 0,
							  m_CxnMgr ? m_CxnMgr->GetPackFrame() : 0,
							  GetNewQueueLength(),
							  GetResendQueueLength(),
							  timeForKeepalive ? "KEEPALIVE/ACK" : "ACK",
							  0,
							  m_InboundSeq,
							  m_RcvdBits,
							  MakeAckStr(buf, sizeof(buf), m_InboundSeq, m_RcvdBits));
				}
	#endif  //__NO_OUTPUT

				unsigned flags = 0;

				if(m_State < STATE_ESTABLISHED)
				{
					flags |= netBundle::FLAG_SYN;
				}

				if(m_State >= STATE_SYN_RECEIVED)
				{
					flags |= netBundle::FLAG_ACK;
				}

				bundle->ResetHeader(ptrdiff_t_to_int(p - (u8*) bundle->GetPayload()),
									m_ChannelId,
									firstSeq,
									m_InboundSeq,
									m_RcvdBits,
									flags);

				//All sends are essentially keepalives.
				this->ResetKeepalive();

				numBytes = ptrdiff_t_to_int(p - (u8*) buf);
			}
		}
    }

	// capture state from this frame
	m_WasMainThreadDead = bIsMainThreadDead;

#if NET_CXNMGR_COLLECT_STATS
	if(netVerify(m_EpOwner))
	{
		netEndpoint::Statistics& stats = m_EpOwner->GetStats();
		stats.m_FramesSent += numFrames;
		stats.m_FramesSentReliably += framesSentReliably;
		stats.m_FramesResent += framesResent;
	}
#endif

    return numBytes;
}

unsigned
netConnection::Unpack(const void* buf, const unsigned len)
{
    netAssert(m_Initialized);

    const u8* p = (const u8*) buf;
    const u8* eob = p + len;
    OUTPUT_ONLY(bool success = false);

    rtry
    {
		u32 framesRcvdInOrder = 0;
		u32 framesRcvdOutOfOrder = 0;
		u32 framesDroppedDuplicate = 0;
		u32 framesDroppedLateUnreliable = 0;
		u32 framesDroppedWndOverflow = 0;

        rcheck(!IsOutOfMemory(),
                catchall,
                netError("Can't unpack - out of memory!!!"));

        rverify(len >= netBundle::BYTE_SIZEOF_HEADER
                && len <= netBundle::MAX_BYTE_SIZEOF_BUNDLE,
                 catchall,
                 netDebug2("Bundle size:%d is outside valid range:%d-%d",
                            len,
                            netBundle::BYTE_SIZEOF_HEADER,
                            netBundle::MAX_BYTE_SIZEOF_BUNDLE));

        const netBundle* bundle = (const netBundle*) p;

        rcheck(bundle->GetSize() == len,
                catchall,
                netDebug2("Bundle size:%d is different from buffer size:%d",
                            bundle->GetSize(),
                            len));

        const netSequence sendersExpectedSeq = bundle->GetExpectedSeq();
        netSequence unreliableSeq = bundle->GetFirstUnreliableSeq();
        const bool hasAck = bundle->HasAck();

        //Check that the sender is expecting the same sequence numbers
        //that we're sending.
        rcheck(!hasAck || netSeqLe(sendersExpectedSeq, m_OutboundSeq),
                catchall,
                netDebug2("Received bundle from different sequence space (Expected: %u, Outbound: %u", sendersExpectedSeq, m_OutboundSeq));

        //With each packet we receive we can reset the timeout.
        this->ResetTimeoutTimer();

        this->ProcessBundleHeader(*bundle);

        //Ignore everything until we receive a SYN.
        rcheck(m_State >= STATE_SYN_RECEIVED,
                catchall,
                netDebug2("Received bundle before receiving SYN"));

        p = (const u8*) bundle->GetPayload();

        unsigned fsize = 0;
        unsigned numFrames = 0;

#if __ASSERT && 0
        //Make sure each bit in m_RcvdBits has a corresponding
        //message in m_OoRcvd.
        {
            unsigned bits = m_RcvdBits;
            netSequence checkSeq = m_InboundSeq + 1;
            unsigned bitCount = 0, relCount = 0;
            for(; bits; bits >>= 1, ++checkSeq)
            {
                if(bits & 0x01)
                {
                    ++bitCount;
                    netAssertf(m_OoRcvd.count(checkSeq) == 1, "Invalid out of order received count: %d", static_cast<int>(m_OoRcvd.count(checkSeq)));
                }

                InFramesBySeq::iterator it = m_OoRcvd.find(checkSeq);
                if(m_OoRcvd.end() != it && it->second->m_IsReliable)
                {
                    ++relCount;
                }
            }
            netAssert(bitCount == relCount, "Unpack :: BitCount [%u] != RelCount [%u]");
        }
#endif  //__ASSERT

        for(; p < eob; p += fsize, ++numFrames)
        {
            const netFrame* f = (const netFrame*) p;

            rcheck(netFrame::Validate(f, ptrdiff_t_to_int(eob - p)),
                    catchall,
                    netDebug2("Invalid frame"));

            fsize = f->GetSize();

            const bool isReliable = f->IsReliable();
            const netSequence seq = isReliable ? f->GetSeq() : unreliableSeq;
            const netSequence seqDep = f->GetSeqDep();

            //Unreliable frames should always be packed into
            //a bundle in strict sequence order.  Reliable
            //frames can come in any order due to resends.
            netAssertf(isReliable || seq == unreliableSeq, "Invalid unreliable sequence: Seq: %d, Expected: %d", seq, unreliableSeq);
			
            if(seq == unreliableSeq)
			{
				++unreliableSeq;
			}

            netDebug2("[%u/%u] FRM(%s)[%s]<-,sq:%d,sc: ,sd:%d,sz:%d,lr:%d",
						m_CxnMgr ? m_CxnMgr->GetReceiveFrame() : 0,
						m_CxnMgr ? m_CxnMgr->GetUnpackFrame() : 0,
						isReliable ? "R" : "U",
                        netMessage::IsMessage(f->GetPayload(), f->GetSizeofPayload())
                        ? netMessage::GetName(f->GetPayload(), f->GetSizeofPayload())
                        : "",
                        seq,
                        seqDep,
                        f->GetSize(),
						m_LastReliableRcvd);

            if(seqDep == m_LastReliableRcvd && netSeqGe(seq, m_InboundSeq))
            {
                //The frame was received in order.
				framesRcvdInOrder++;

                if(!this->QueueFrameEvent(f, seq))
                {

                }
                else if(isReliable)
                {
                    //Follow the chain of dependencies, removing
                    //frames from the Out of Order queue.

                    m_RcvdBits >>= netSeqDiff(netSequence(seq + 1), m_InboundSeq);

                    m_InboundSeq = netSequence(seq + 1);
                    m_LastReliableRcvd = seq;

                    InFramesBySeq::iterator it = m_OoRcvd.begin();
                    InFramesBySeq::iterator next = it;
                    InFramesBySeq::const_iterator stop = m_OoRcvd.end();

                    for(++next; stop != it; it = next, ++next)
                    {
                        InFrame* infrm = it->second;
                        if(infrm->m_SeqDep == m_LastReliableRcvd)
                        {
                            if(infrm->m_IsReliable)
                            {
                                m_LastReliableRcvd = it->first;
                            }

                            const netSequence curSeq = infrm->m_Sequence;

                            netAssert(netSeqLe(m_InboundSeq, curSeq));

                            m_RcvdBits >>= netSeqDiff(netSequence(curSeq + 1), m_InboundSeq);

                            m_InboundSeq = netSequence(curSeq + 1);

							netDebug3("[%u/%u] DEQ(%s)<-sq:%d,sd:%d,rb:%d,lr:%d",
									  m_CxnMgr ? m_CxnMgr->GetReceiveFrame() : 0,
									  m_CxnMgr ? m_CxnMgr->GetUnpackFrame() : 0,
									  infrm->m_IsReliable ? "R" : "U",
									  infrm->m_Sequence,
									  infrm->m_SeqDep,
									  m_RcvdBits,
									  m_LastReliableRcvd);

                            m_OoRcvd.erase(it);
                            this->QueueEvent(infrm);
                        }
                        else
                        {
                            break;
                        }
                    }

                    m_NeedToAck = true;
                }
                else
                {
                    const netSequence newInbound = netSequence(seq + 1);
                    const netSequence diff = netSeqDiff(newInbound, m_InboundSeq);
                    if(diff >= RCV_WIN)
                    {
                        m_RcvdBits = 0;
                    }
                    else
                    {
                        m_RcvdBits >>= diff;
                    }

                    m_InboundSeq = newInbound;

                    netAssert(m_OoRcvd.empty()
                            || netSeqGt(m_OoRcvd.begin()->second->m_Sequence, m_InboundSeq));
                }
            }
            //To compare performance with a windowless technique,
            //comment out the remainder of the else's.
            else if(netSeqGt(seq, m_InboundSeq)
                    && netSeqDiff(seq, m_InboundSeq) <= RCV_WIN)
            {
                //The frame was received out of order.
				framesRcvdOutOfOrder++;

				netDebug3("The frame was received out of order");

                netAssert(seq != m_InboundSeq);

				bool duplicate = false;
                if(!this->QueueOutOfOrder(f, seq, duplicate))
                {

                }
                else if(isReliable)
                {
                    //Add a bit to the received bits.
                    m_RcvdBits |= AckBit(seq, m_InboundSeq);

                    m_NeedToAck = true;
                }

				if(duplicate)
				{
					netDebug2("[%u/%u] DROP(%s)<-,ch:%u,sq:%d,sc: ,sd:%d,sz:%d,%s",
								m_CxnMgr ? m_CxnMgr->GetReceiveFrame() : 0,
								m_CxnMgr ? m_CxnMgr->GetUnpackFrame() : 0,
								isReliable ? "R" : "U",
								this->m_ChannelId,
								seq,
								seqDep,
								f->GetSize(),
								"duplicate out-of-order frame");

					framesDroppedDuplicate++;
				}
            }
            else
            {
                netDebug2("[%u/%u] DROP(%s)<-,ch:%u,sq:%d,sc: ,sd:%d,sz:%d,%s",
						  m_CxnMgr ? m_CxnMgr->GetReceiveFrame() : 0,
						  m_CxnMgr ? m_CxnMgr->GetUnpackFrame() : 0,
						  isReliable ? "R" : "U",
						  this->m_ChannelId,
						  seq,
						  seqDep,
						  f->GetSize(),
						  netSeqLe(seq, m_InboundSeq) ? (isReliable ? "duplicate reliable" : "late unreliable") : "window overflow");

				if(netSeqLe(seq, m_InboundSeq))
				{
					if(isReliable)
					{
						framesDroppedDuplicate++;
					}
					else
					{
						framesDroppedLateUnreliable++;
					}
				}
				else
				{
					framesDroppedWndOverflow++;
				}				

                //Perhaps he didn't get our last ACK so ACK it again.
                m_NeedToAck = m_NeedToAck || isReliable;
            }
        }

#if NET_CXNMGR_COLLECT_STATS
		if(netVerify(m_EpOwner))
		{
			netEndpoint::Statistics& stats = m_EpOwner->GetStats();
			stats.m_FramesRcvdInOrder += framesRcvdInOrder;
			stats.m_FramesRcvdOutOfOrder += framesRcvdOutOfOrder;
			stats.m_FramesDroppedDuplicate += framesDroppedDuplicate;
			stats.m_FramesDroppedLateUnreliable += framesDroppedLateUnreliable;
			stats.m_FramesDroppedWndOverflow += framesDroppedWndOverflow;
		}
#endif

        OUTPUT_ONLY(success = true);
    }
    rcatchall
    {
    }

    netAssert(!success || len == unsigned(p - (const u8*) buf));

    return ptrdiff_t_to_int(p - (const u8*) buf);
}

unsigned
netConnection::GetUnAckedCount() const
{
    return (unsigned) m_Unacked.size();
}

unsigned
netConnection::GetOldestResendCount() const
{
    return !m_Unacked.empty()
            ? m_Unacked.begin()->second->m_SendCount
            : 0;
}

netSequence
netConnection::GetSequenceOfOldestResendCount() const
{
    return !m_Unacked.empty()
            ? m_Unacked.begin()->first
            : 0;
}

unsigned
netConnection::GetQueueTimeOfOldestUnackedFrame() const
{
    return !m_Unacked.empty()
            ? m_Unacked.begin()->second->m_QueueTime
            : 0;
}

unsigned
netConnection::GetAgeOfOldestUnackedFrame() const
{
	if(!m_Unacked.empty())
	{
		const unsigned curTime = sysTimer::GetSystemMsTime();
		const unsigned queueTime = m_Unacked.begin()->second->m_QueueTime;

		// handle time wrap-around
		if(curTime > queueTime)
		{
			return curTime - queueTime;
		}
	}

	return 0;
}

unsigned
netConnection::GetNewQueueLength() const
{
    return static_cast<unsigned>(m_NewQ.size());
}

unsigned
netConnection::GetResendQueueLength() const
{
	return static_cast<unsigned>(m_ResendQ.size());
}

unsigned
netConnection::GetTimeoutCount() const
{
    return m_TimeoutCount;
}

unsigned
netConnection::GetRttOverThresholdStartTime() const
{
	return m_RttOverThresholdStartTime;
}

unsigned
netConnection::GetRttOverThresholdLastTime() const
{
	return m_RttOverThresholdLastTime;
}

bool
netConnection::NeedToAck() const
{
    return m_NeedToAck;
}

bool
netConnection::TimeForKeepalive() const
{
    //A value of zero for m_LastKeepaliveTime indicates we haven't
    //yet called ResetKeepalive().
    return m_Policies.m_KeepaliveInterval
            && (STATE_ESTABLISHED == m_State)
            && m_LastKeepaliveTime
            && (sysTimer::GetSystemMsTime() - m_LastKeepaliveTime) > m_Policies.m_KeepaliveInterval;
}

void
netConnection::ResetKeepalive()
{
    if(m_Policies.m_KeepaliveInterval)
    {
        m_LastKeepaliveTime = sysTimer::GetSystemMsTime();
    }
}

bool
netConnection::IsTimedOut() const
{
    return m_IsTimedOut;
}

bool
netConnection::IsTimedOut(TimeoutReason* timeoutReason) const
{
	if(m_IsTimedOut)
	{
		if(timeoutReason)
		{
			*timeoutReason = m_TimeoutReason;
		}
		return true;
	}

	if(timeoutReason)
	{
		*timeoutReason = CXN_TIMEOUT_REASON_NONE;
	}

	return false;
}

void
netConnection::ClearTimeout()
{
    m_IsTimedOut = false;
	m_TimeoutReason = CXN_TIMEOUT_REASON_NONE;
}

bool
netConnection::TimeForTimeout()
{
	//A value of zero for m_LastReceiveTime indicates we haven't
	//yet called ResetTimeoutTimer().
	return m_Policies.m_TimeoutInterval
		&& m_LastReceiveTime
		&& (sysTimer::GetSystemMsTime() - m_LastReceiveTime) > m_Policies.m_TimeoutInterval;
}

bool
netConnection::QueueTimeout(const TimeoutReason timeoutReason)
{
	netAssert((timeoutReason > CXN_TIMEOUT_REASON_NONE) && (timeoutReason < CXN_TIMEOUT_NUM_REASONS));

	m_IsTimedOut = true;

	// attribute the timeout to the first reason given
	if(m_TimeoutReason == CXN_TIMEOUT_REASON_NONE)
	{
		m_TimeoutReason = timeoutReason;
	}
	return true;
}

#if __BANK
void 
netConnection::ForceTimeout()
{
	m_IsForcedTimeOut = true; 
	this->QueueTimeout(CXN_TIMEOUT_REASON_FORCED);
}

bool 
netConnection::IsForcedTimeout()
{
	return m_IsForcedTimeOut;
}
#endif

bool
netConnection::QueueOutOfMemory(const unsigned requiredBytes)
{
	m_IsOutOfMemory = true; 
	m_IsOutOfMemoryRequiredBytes = rage::Max(requiredBytes, m_IsOutOfMemoryRequiredBytes);

	// If we can we trigger this callback immediately so to have the most accurate stats
	// It might be called again in the CxnMgr but that won't cause any issues.
	if (m_CxnMgr && m_CxnMgr->GetOutOfMemoryCallback())
	{
		m_CxnMgr->GetOutOfMemoryCallback()(m_EpOwner ? m_EpOwner->GetId() : NET_INVALID_ENDPOINT_ID, true, requiredBytes);
	}

	return true; 
}

bool 
netConnection::IsOutOfMemory() const
{
	return m_IsOutOfMemory;
}

unsigned
netConnection::GetOutOfMemoryRequiredBytes() const
{
	return m_IsOutOfMemoryRequiredBytes;
}

void
netConnection::ResetTimeoutTimer()
{
    if(m_Policies.m_TimeoutInterval)
    {
        m_LastReceiveTime = sysTimer::GetSystemMsTime();
    }
}

void
netConnection::DropOutboundUnreliableFrames()
{
	netDebug3("netConnection::DropOutboundUnreliableFrames");

    OutFrameQ::iterator itOut = m_NewQ.begin();
    OutFrameQ::iterator nextOut = itOut;
    OutFrameQ::const_iterator stopOut = m_NewQ.end();
    for(++nextOut; stopOut != itOut; itOut = nextOut, ++nextOut)
    {
        OutFrame* f = *itOut;
        if(!f->IsReliable())
        {
            m_NewQ.erase(itOut);
            this->FreeOutFrame(f);
        }
    }
}

void
netConnection::ReclaimMem()
{
    this->DropOutboundUnreliableFrames();

    //Drop inbound unreliable frames.
    InFramesBySeq::iterator itOo = m_OoRcvd.begin();
    InFramesBySeq::iterator nextOo = itOo;
    InFramesBySeq::const_iterator stopOo = m_OoRcvd.end();
    for(++nextOo; stopOo != itOo; itOo = nextOo, ++nextOo)
    {
        InFrame* f = itOo->second;
        if(!f->m_IsReliable)
        {
			netDebug3("netConnection::ReclaimMem :: erasing OoRcvd frame sq:%d", f->m_Sequence);

            m_OoRcvd.erase(itOo);
            f->Dispose();
        }
    }
}

void
netConnection::ReclaimAllMem()
{
    this->ReclaimMem();

    while(!m_ResendQ.empty())
    {
        OutFrame* outfrm = m_ResendQ.front();

        m_ResendQ.pop_front();
        m_Unacked.erase(outfrm);

        this->FreeOutFrame(outfrm);
    }

    netAssert(m_Unacked.empty());

    while(!m_NewQ.empty())
    {
        OutFrame* outfrm = m_NewQ.front();

        m_NewQ.pop_front();

        this->FreeOutFrame(outfrm);
    }

    while(!m_OoRcvd.empty())
    {
        InFrame* infrm = m_OoRcvd.begin()->second;
        m_OoRcvd.erase(infrm);
        infrm->Dispose();
    }
}

void
netConnection::DumpQueues()
{
#if !__NO_OUTPUT

	if(!m_NewQ.empty() || !m_ResendQ.empty() || !m_OoRcvd.empty())
	{
		netDebug("Message Queues:");
	}

	const unsigned curTime = sysTimer::GetSystemMsTime();

    if(!m_NewQ.empty())
    {
        netDebug(" New Outbound Messages:");
        OutFrameQ::iterator it = m_NewQ.begin();
        OutFrameQ::const_iterator stop = m_NewQ.end();
        for(; stop != it; ++it)
        {
            const OutFrame* outfrm = *it;
            netDebug("   [%u/%u] FRM(%s)[%s]->,sz:%d,sq:%d,age:%u,sc:%d,sd:%d",
						outfrm->m_SendFrame,
						outfrm->m_PackFrame,
						outfrm->IsReliable() ? "R" : "U",
                        netMessage::IsMessage(outfrm->GetPayload(), outfrm->GetSizeofPayload())
                        ? netMessage::GetName(outfrm->GetPayload(), outfrm->GetSizeofPayload())
                        : "",
                        outfrm->GetSize(),
                        outfrm->GetSeq(),
						curTime - outfrm->m_QueueTime,
                        outfrm->m_SendCount,
						outfrm->GetSeqDep());
        }
    }

    if(!m_ResendQ.empty())
    {
        netDebug(" Resend Queue:");
        OutFrameQ::iterator it = m_ResendQ.begin();
        OutFrameQ::const_iterator stop = m_ResendQ.end();
        for(; stop != it; ++it)
        {
            const OutFrame* outfrm = *it;
            netDebug("   [%u/%u] FRM(%s)[%s]->,sz:%d,sq:%d,age:%u,sc:%d,sd:%d",
						outfrm->m_SendFrame,
						outfrm->m_PackFrame,
						outfrm->IsReliable() ? "R" : "U",
                        netMessage::IsMessage(outfrm->GetPayload(), outfrm->GetSizeofPayload())
                        ? netMessage::GetName(outfrm->GetPayload(), outfrm->GetSizeofPayload())
                        : "",
                        outfrm->GetSize(),
                        outfrm->GetSeq(),
						curTime - outfrm->m_QueueTime,
                        outfrm->m_SendCount,
                        outfrm->GetSeqDep());
        }
    }

    if(!m_OoRcvd.empty())
    {
        netDebug(" Received Out of Order:");
        InFramesBySeq::iterator it = m_OoRcvd.begin();
        InFramesBySeq::const_iterator stop = m_OoRcvd.end();
        for(; stop != it; ++it)
        {
            const InFrame* infrm = it->second;

            netDebug("  FRM(%s)[%s]<-,sz:%d,sq:%d",
                        infrm->m_IsReliable ? "R" : "U",
                        netMessage::IsMessage(infrm->m_Payload, infrm->m_SizeofPayload)
                        ? netMessage::GetName(infrm->m_Payload, infrm->m_SizeofPayload)
                        : "",
                        infrm->m_SizeofPayload,
                        infrm->m_Sequence);
        }
    }
#endif  //__NO_OUTPUT
}

//private:

void
netConnection::SetState(const State state)
{
    netAssert(STATE_LISTEN == state || state > m_State);
	m_State = state;
}

void
netConnection::QueueEvent(netEvent* e)
{
    netAssert(e);
    if(e){ m_EventQ->QueueEvent(e, sysTimer::GetSystemMsTime()); }
}

bool
netConnection::QueueFrameEvent(const netFrame* f, const netSequence seq)
{
    netAssert(m_State <= STATE_ESTABLISHED);

    bool success = true;
    InFrame* infrm = 0;

    if(f->GetSizeofPayload())
    {
        if(0 != (infrm = this->AllocInFrame(f, seq)))
        {
            this->QueueEvent(infrm);
            //Reset the warning spew limiter.
            frmMemResetWarningSpew();
        }
        else
        {
            frmMemWarning("inbound", seq);
            success = false;
        }
    }

    return success;
}

bool
netConnection::QueueOutOfOrder(const netFrame* f, const netSequence seq, bool& wasDuplicate)
{
    netAssert(m_State <= STATE_ESTABLISHED);

    bool success = true;
	wasDuplicate = false;
    InFrame* infrm = 0;

    if(f->GetSizeofPayload())
    {
        if(0 != (infrm = this->AllocInFrame(f, seq)))
        {
			netDebug3("Queuing OoRcvd frame sq:%d", seq);

            auto result = m_OoRcvd.insert(InFramesBySeq::value_type(seq, infrm));

			if(result.second == false)
			{
				netDebug3("   Already have OoRcvd frame sq:%d, discarding.", seq);
				wasDuplicate = true;
				infrm->Dispose();
			}

            //Reset the warning spew limiter.
            frmMemResetWarningSpew();
        }
        else
        {
            frmMemWarning("inbound", seq);
            success = false;
        }
    }

    return success;
}

void
netConnection::ProcessBundleHeader(const netBundle& bundle)
{
    netAssert(m_Initialized);

    const bool hasSyn = bundle.HasSyn();
    const bool hasAck = bundle.HasAck();

    if(STATE_LISTEN == m_State)
    {
        if(hasSyn
            //We shouldn't be getting an ACK at this point.
            && !hasAck
            && netVerify(bundle.GetSizeofPayload() >= netFrame::BYTE_SIZEOF_RHEADER))
        {
            const netFrame* f = (const netFrame*) bundle.GetPayload();

            netAssert(f->GetSeq() == netSequence(f->GetSeqDep() + 1));

            netDebug2("Received SYN:%d,sd:%d in state:LISTEN, synchronizing...",
                        f->GetSeq(), f->GetSeqDep());

            this->Synchronize(f->GetSeq());
        }
    }
    else if(STATE_SYN_SENT == m_State)
    {
        if(hasSyn
            && netVerify(bundle.GetSizeofPayload() >= netFrame::BYTE_SIZEOF_RHEADER))
        {
            bool acceptIt = true;
            if(hasAck)
            {
                //Make sure it's ACKing our SYN.
                acceptIt = (m_ResendQ.size() > 0) && (*m_ResendQ.begin())->GetSeq() == netSequence(bundle.GetExpectedSeq() - 1);
            }

            if(acceptIt)
            {
                const netFrame* f = (const netFrame*) bundle.GetPayload();

                netAssert(f->GetSeq() == netSequence(f->GetSeqDep() + 1));

                netDebug2("Received SYN:%d,sd:%d in state:SYN_SENT, synchronizing...",
                            f->GetSeq(), f->GetSeqDep());

                this->Synchronize(f->GetSeq());
            }
        }
    }

    if(STATE_SYN_RECEIVED == m_State && hasAck && (m_ResendQ.size() > 0))
    {
		//The only thing in the resend queue should be the SYN frame we sent.
		netAssert(m_ResendQ.size() == 1);

        const OutFrame* mySyn = *m_ResendQ.begin();
        const netSequence mySynSeq = mySyn->GetSeq();
        const netSequence expectedSeq = bundle.GetExpectedSeq();

        if(netSeqLt(mySynSeq, expectedSeq))
        //if(mySynSeq == netSequence(expectedSeq - 1))
        {
            netDebug2("Received ACK for SYN:%d in state:SYN_RECEIVED, transitioning to ESTABLISHED",
                        mySynSeq);

            netEventConnectionEstablished* e =
                (netEventConnectionEstablished*) this->AllocCritical(sizeof(netEventConnectionEstablished));

            if(netVerify(e))
            {
                new(e) netEventConnectionEstablished(m_Id,
													m_EpOwner ? m_EpOwner->GetId() : NET_INVALID_ENDPOINT_ID,
                                                    m_ChannelId,
                                                    m_Allocator);

                this->SetState(STATE_ESTABLISHED);
                this->QueueEvent(e);
            }
            else
            {
                netError("Out of memory allocating netEventConnectionEstablished");
            }
        }
    }

    if(STATE_ESTABLISHED == m_State && hasAck)
    {
        //Process the ACK
        const netSequence expectedSeq = bundle.GetExpectedSeq();
        const unsigned ackBits = bundle.GetUoAckBits();

        OutFramesBySeq::iterator it = m_Unacked.begin();
        OutFramesBySeq::iterator itNext = it;
        OutFramesBySeq::const_iterator stop = m_Unacked.end();

        //Remove all frames with sequences <= the ack or that
        //correspond to bits in the ack bits.
        for(++itNext; stop != it; it = itNext, ++itNext)
        {
            OutFrame* outfrm = it->second;
            const netSequence seq = outfrm->GetSeq();
            const unsigned seqDiff = netSeqDiff(seq, expectedSeq);

            if(netSeqGt(seq, expectedSeq) && seqDiff > RCV_WIN)
            {
                //Sequence is outside the ACKer's receive window.
				break;
            }

            //A frame is ACKed if its sequence is less than the next
            //sequence expected by the ACKer or its bit is set in the ack bits.
            const bool isAcked =
                netSeqLt(seq, expectedSeq)
                || (netSeqGt(seq, expectedSeq)
                    && (ackBits & AckBit(seq, expectedSeq)));

            if(isAcked)
            {
                netDebug2("ACK(%s)<-,sq:%d,age:%u",
                            outfrm->IsReliable() ? "R" : "U",
                            seq,
							sysTimer::GetSystemMsTime() - outfrm->m_QueueTime);

                netAssert(outfrm->m_SendCount > 0);

#if NET_CXNMGR_COLLECT_STATS
				if(netVerify(m_EpOwner))
				{
					netEndpoint::Statistics& stats = m_EpOwner->GetStats();

					if(outfrm->IsReliable())
					{
						const unsigned curTime = sysTimer::GetSystemMsTime();

						// make sure time hasn't wrapped around
						if((outfrm->m_QueueTime > 0) && (curTime > outfrm->m_QueueTime))
						{
							const u32 rtt = curTime - outfrm->m_QueueTime;

							// get rid of impossibly large RTTs (possibly due to long stalls)
							if(rtt < (60 * 1000))
							{
								if((sm_RttWarningThresholdMs > 0) && (rtt > sm_RttWarningThresholdMs))
								{
									if(m_RttOverThresholdStartTime == 0)
									{
										m_RttOverThresholdStartTime = curTime;
									}

									m_RttOverThresholdLastTime = curTime;
								}
								else
								{
									m_RttOverThresholdStartTime = 0;
									m_RttOverThresholdLastTime = 0;
								}

								stats.m_Rtt.Update(rtt);
							}
						}
					}
				}
#endif

                if(this->GetTimeoutCount(outfrm))
                {
                    --m_TimeoutCount;
                    netAssert(m_TimeoutCount >= 0);
                }

                m_ResendQ.erase(outfrm);
                m_Unacked.erase(it);

                const bool notifyAck = outfrm->m_NotifyAck;

                this->FreeOutFrame(outfrm);

                if(notifyAck)
                {
                    netEventAckReceived* e =
                        (netEventAckReceived*) this->AllocCritical(sizeof(netEventAckReceived));

                    if(netVerify(e))
                    {
                        new(e) netEventAckReceived(m_Id,
													m_EpOwner ? m_EpOwner->GetId() : NET_INVALID_ENDPOINT_ID,
                                                    m_ChannelId,
                                                    seq,
                                                    m_Allocator);
                        this->QueueEvent(e);
                    }
                    else
                    {
                        netError("Out of memory allocating netEventAckReceived");
                    }
                }
            }
        }
    }
}

unsigned
netConnection::GetTimeoutCount(const OutFrame* outfrm) const
{
    return outfrm->m_SendCount > m_Policies.m_MaxSendCount ?
        outfrm->m_SendCount - m_Policies.m_MaxSendCount :
        0;
}

void*
netConnection::Alloc(const unsigned size)
{
	void* ptr = m_Allocator->RAGE_LOG_ALLOCATE(size, 0);

	if(!ptr)
	{
		sysInterlockedAdd(&m_NumFailedAllocs, 1);
	}

    return ptr;
}

void*
netConnection::AllocCritical(const unsigned size)
{
    void* p = m_Allocator->RAGE_LOG_ALLOCATE(size, 0);

    if(!p)
    {
		sysInterlockedAdd(&m_NumFailedAllocs, 1);

        if(m_CxnMgr)
        {
            m_CxnMgr->ReclaimMem();
        }
        else
        {
            this->ReclaimMem();
        }

        p = m_Allocator->RAGE_LOG_ALLOCATE(size, 0);

        if(!p)
        {
			sysInterlockedAdd(&m_NumFailedAllocs, 1);

			netError("AllocCritical :: Out of memory allocating %uB - Used: %d, Available: %d, Largest Block: %d", 
				size,
				static_cast<int>(m_Allocator->GetMemoryUsed(-1)),
				static_cast<int>(m_Allocator->GetMemoryAvailable()),
				static_cast<int>(m_Allocator->GetLargestAvailableBlock()));
			this->QueueOutOfMemory(size);
        }
    }

    return p;
}

netConnection::InFrame*
netConnection::AllocInFrame(const netFrame* f, const netSequence seq)
{
    netAssert((f->GetSize() >= netFrame::BYTE_SIZEOF_RHEADER) || (f->GetSize() >= netFrame::BYTE_SIZEOF_UHEADER));
    InFrame* infrm = 0;

    infrm = (InFrame*) this->Alloc(sizeof(InFrame));

    if(infrm)
    {
        const unsigned sizeofPayload = f->GetSizeofPayload();
        void* payload = this->Alloc(sizeofPayload);
        if(payload)
        {
            sysMemCpy(payload, f->GetPayload(), sizeofPayload);
            new(infrm) InFrame(this,
								m_EpOwner ? m_EpOwner->GetAddress() : netAddress::INVALID_ADDRESS,
								m_EpOwner ? m_EpOwner->GetId() : NET_INVALID_ENDPOINT_ID,
								payload,
								f,
								seq,
								m_Allocator);
        }
        else
        {
            m_Allocator->Free(infrm);
            infrm = 0;
        }
    }

    return infrm;
}

netConnection::OutFrame*
netConnection::AllocOutFrame(const unsigned sizeofPayload,
                            const unsigned sendFlags)
{
    bool isReliable = (sendFlags & NET_SEND_RELIABLE);
    OutFrame* outfrm =
        (OutFrame*) (isReliable
                    ? this->AllocCritical(sizeof(OutFrame))
                    : this->Alloc(sizeof(OutFrame)));

    if(outfrm)
    {
        const unsigned sizeofFrame =  sizeof(netFrame) + sizeofPayload;
        netFrame* frame =
            (netFrame*) (isReliable
                        ? this->AllocCritical(sizeofFrame)
                        : this->Alloc(sizeofFrame));

        if(frame)
        {
            new(frame) netFrame;
            new(outfrm) OutFrame(frame);
        }
        else
        {
            m_Allocator->Free(outfrm);
            outfrm = 0;
        }
    }

    return outfrm;
}

void
netConnection::FreeOutFrame(OutFrame* outfrm)
{
    outfrm->m_Frame->~netFrame();
    outfrm->~OutFrame();
    m_Allocator->Free(outfrm->m_Frame);
    m_Allocator->Free(outfrm);
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT

}   //namespace rage

///////////////////////////////////////////////////////////////////////////////
// Unit tests
///////////////////////////////////////////////////////////////////////////////

#ifndef QA_ASSERT_ON_FAIL
#define QA_ASSERT_ON_FAIL   0
#endif
#include "qa/qa.h"

#if __QA

#include <list>
#include "diag/output.h"
#include "system/simpleallocator.h"

//#define QA_MAKE_RAND_SEED   106029516
#define QA_MAKE_RAND_SEED   int(sysTimer::GetSystemMsTime())

#ifndef QA_NET_DEBUG_LEVEL
#define QA_NET_DEBUG_LEVEL  DIAG_SEVERITY_FATAL
#endif

using namespace rage;

class qa_ConnectionTimeout : public qaItem
{
public:

    void Init()
    {
        static bool s_IsInitialized = false;
        if(!s_IsInitialized)
        {
#if defined(QA_NET_DEBUG_LEVEL) && !defined(__NO_OUTPUT)
            Channel_ragenet.TtyLevel = QA_NET_DEBUG_LEVEL;
#endif

            s_IsInitialized = true;
        }
    }

    void Shutdown()
    {
    }

	void Update(qaResult& result);
};

class qa_CxnData
{
public:

    qa_CxnData()
    {
        m_EventQ.Subscribe(&m_EventConsumer);
    }

    void ProcessEvents()
    {
        for(netEvent* e = m_EventConsumer.NextEvent(); e; e = m_EventConsumer.NextEvent())
        {
            if(NET_EVENT_FRAME_RECEIVED == e->GetId())
            {
                this->RecordReceive(e->m_FrameReceived->m_Sequence);
            }
        }
    }

    void RecordSend(const netSequence seq)
    {
        m_SentList.push_back(seq);
    }

    void RecordReceive(const netSequence seq)
    {
        m_ReceivedList.push_back(seq);
    }

    netEventQueue<netEvent> m_EventQ;
    netEventConsumer<netEvent> m_EventConsumer;

    typedef std::list<netSequence> SequenceList;

    SequenceList m_SentList;
    SequenceList m_ReceivedList;
};

class qa_ConnectionMem : public qaItem
{
public:

    qa_ConnectionMem()
    {
    }

    void Init()
    {
        static bool s_IsInitialized = false;
        if(!s_IsInitialized)
        {
#if defined(QA_NET_DEBUG_LEVEL) && !defined(__NO_OUTPUT)
            Channel_ragenet.TtyLevel = QA_NET_DEBUG_LEVEL;
            //Prevent tons of debug spew about running out of memory.
            s_EnableMemWarnings = false;
#endif

            s_IsInitialized = true;
        }

#if !__NO_OUTPUT
        //We expect alot of warnings in this test so disable warnings
        //to avoid a ton of debug spew.
        m_OldOutputMask = diagOutput::GetOutputMask();
        diagOutput::SetOutputMask(m_OldOutputMask & ~(diagOutput::OUTPUT_WARNINGS|diagOutput::OUTPUT_DISPLAYS));
#endif
    }

    void Shutdown()
    {
#if !__NO_OUTPUT
        diagOutput::SetOutputMask(m_OldOutputMask);
#endif // !__NO_OUTPUT
        s_EnableMemWarnings = true;
    }

	void Update(qaResult& result);

    unsigned m_OldOutputMask;
};

class qa_Connection : public qaItem
{
public:

    static const unsigned TIME_INTERVAL     = 10;

    qa_Connection()
        : m_Heap(0)
        , m_Allocator(0)
    {
    }

    void Init(const int lossPct, const int numMsgs)
    {
        static bool s_IsInitialized = false;
        if(!s_IsInitialized)
        {
            const int seed = QA_MAKE_RAND_SEED;

            QALog("qa_Connection: Using random seed:%u", seed);

            sm_Rand.Reset(seed);

#if defined(QA_NET_DEBUG_LEVEL)
            OUTPUT_ONLY(Channel_ragenet.TtyLevel = QA_NET_DEBUG_LEVEL);
#endif

            s_IsInitialized = true;
        }

        netAssert(!m_Heap && !m_Allocator);

        static const int SIZEOF_HEAP    = 128 * 1024;
        m_Heap = rage_new u8[SIZEOF_HEAP];
        m_Allocator = rage_new sysMemSimpleAllocator(m_Heap, SIZEOF_HEAP,sysMemSimpleAllocator::HEAP_NET);

        m_LossPct = lossPct;
        m_NumMsgs = numMsgs;
    }

    void Shutdown()
    {
        delete m_Allocator;
        delete [] m_Heap;

        m_Allocator = 0;
        m_Heap = 0;
    }

	void Update(qaResult& result);

    int m_LossPct;
    int m_NumMsgs;

    u8* m_Heap;
    sysMemSimpleAllocator* m_Allocator;

    static netRandom sm_Rand;
};

netRandom qa_Connection::sm_Rand;

void
qa_ConnectionTimeout::Update(qaResult& result)
{
    netConnection cxn;
    netConnectionPolicies policies;
    u8 snd[64];
    static const int SIZEOF_HEAP    = 64 * 1024;
    u8 heap[SIZEOF_HEAP];
    sysMemSimpleAllocator allocator(heap, SIZEOF_HEAP, sysMemSimpleAllocator::HEAP_NET);

    allocator.BeginLayer();

    netEventQueue<netEvent> eq;
    netEventConsumer<netEvent> evtConsumer;
    netTimeStep timeStep;

    eq.Subscribe(&evtConsumer);

    cxn.Init(NULL, &eq, &timeStep, &timeStep, &allocator);
    cxn.Open(netAddress(netSocketAddress("1.2.3.4:5")), 0, 0);
    cxn.SetPolicies(policies);

    bool timedOut = false;

    QA_CHECK(cxn.QueueFrame("Hola", sizeof("Hola"), NET_SEND_RELIABLE, NULL));

    for(int i = 0; i <= (int) cxn.GetPolicies().m_MaxSendCount; ++i)
    {
        QA_CHECK(cxn.GetTimeoutCount() == 0);
        cxn.Pack(snd, sizeof(snd));
        timeStep.AddTime(cxn.GetPolicies().m_ResendInterval);

        for(netEvent* e = evtConsumer.NextEvent(); e; e = evtConsumer.NextEvent())
        {
            if(NET_EVENT_CONNECTION_ERROR == e->GetId()
                && e->m_CxnError->IsTimeout())
            {
                timedOut = true;
            }
        }

        QA_CHECK(!timedOut || i == (int) cxn.GetPolicies().m_MaxSendCount);
    }

    QA_CHECK(timedOut);
    QA_CHECK(cxn.GetTimeoutCount() > 0);

    cxn.Shutdown();

    //Make sure we're not leaking mem
    QA_CHECK(0 == allocator.EndLayer("qa_ConnectionTimeout", NULL));

    TST_PASS;
}

void
qa_ConnectionMem::Update(qaResult& result)
{
    static const unsigned TIME_INTERVAL     = 10;

    //Use a small heap so we run out of mem
    static const int SIZEOF_HEAP    = netPacket::MAX_BYTE_SIZEOF_PACKET * 2;
    u8 heaps[2][SIZEOF_HEAP];
    sysMemSimpleAllocator allocator0(heaps[0], SIZEOF_HEAP, sysMemSimpleAllocator::HEAP_NET);
    sysMemSimpleAllocator allocator1(heaps[1], SIZEOF_HEAP, sysMemSimpleAllocator::HEAP_NET);
    sysMemSimpleAllocator* allocators[] =
    {
        &allocator0, &allocator1
    };

    static const int NUM_CXNS = 2;
    netConnection cxns[NUM_CXNS];
    netEventQueue<netEvent> queues[NUM_CXNS];
    netEventConsumer<netEvent> evtConsumers[NUM_CXNS];
    netTimeStep timeStep;
    netConnectionPolicies policies;

    //Prevent debug spew from timeouts.
    policies.m_MaxSendCount = ~0x0u;

    for(int i = 0; i < NUM_CXNS; ++i)
    {
        allocators[i]->BeginLayer();
        allocators[i]->SetQuitOnFail(false);
        cxns[i].Init(NULL, &queues[i], &timeStep, &timeStep, allocators[i]);
        cxns[i].Open(netAddress(netSocketAddress("1.2.3.4:5")), 0, i);
        cxns[i].SetPolicies(policies);
        queues[i].Subscribe(&evtConsumers[i]);
    }

    unsigned size;

    for(int i = 0; i < NUM_CXNS; ++i)
    {
        cxns[i].SetPolicies(policies);
    }

    u8 bundle[netBundle::MAX_BYTE_SIZEOF_BUNDLE];

    //Get the connections connected
    cxns[0].QueueFrame(NULL, 0, NET_SEND_RELIABLE, NULL);
    cxns[1].QueueFrame(NULL, 0, NET_SEND_RELIABLE, NULL);
    size = cxns[0].Pack(bundle, sizeof(bundle));
    QA_CHECK(!size || size == cxns[1].Unpack(bundle, size));
    size = cxns[1].Pack(bundle, sizeof(bundle));
    QA_CHECK(!size || size == cxns[0].Unpack(bundle, size));
    size = cxns[0].Pack(bundle, sizeof(bundle));
    QA_CHECK(!size || size == cxns[1].Unpack(bundle, size));

    for(int i = 0; i < 2; ++i)
    {
        for(netEvent* e = evtConsumers[i].NextEvent(); e; e = evtConsumers[i].NextEvent())
        {
        }
    }

    //We're going to send NUM_ITERATIONS reliable messages.
    //Between each reliable we're going to send a bunch of unreliables
    //to exhaust the heap.  We expect that the unreliables will be dropped
    //and the reliables will be delivered.

    //Counts the number of reliables received.
    int recvCount = 0;

    static const int NUM_ITERATIONS     = 1000;

    for(int i = 0; i < NUM_ITERATIONS; ++i, timeStep.AddTime(TIME_INTERVAL))
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];

        //Send a bunch of unreliables to exhaust mem
        while(cxns[0].QueueFrame(buf, sizeof(buf), 0, NULL)){}

        //Send a reliable and make sure it gets there.
        cxns[0].QueueFrame("Hola", sizeof("Hola"), NET_SEND_RELIABLE, NULL);

        size = cxns[0].Pack(bundle, sizeof(bundle));
        QA_CHECK(!size || size == cxns[1].Unpack(bundle, size));

        for(netEvent* e = evtConsumers[0].NextEvent(); e; e = evtConsumers[0].NextEvent())
        {
        }

        for(netEvent* e = evtConsumers[1].NextEvent(); e; e = evtConsumers[1].NextEvent())
        {
            if(e->GetId() == NET_EVENT_FRAME_RECEIVED
                && (e->m_FrameReceived->m_Flags & NET_SEND_RELIABLE))
            {
                ++recvCount;
            }
        }

        //Send ACKs to ch0.
        size = cxns[1].Pack(bundle, sizeof(bundle));
        QA_CHECK(!size || size == cxns[0].Unpack(bundle, size));
    }

    //Keep packing/unpacking until we've emptied the outbound queue.
    while(cxns[0].GetUnAckedCount())
    {
        size = cxns[0].Pack(bundle, sizeof(bundle));
        QA_CHECK(!size || size == cxns[1].Unpack(bundle, size));
        size = cxns[1].Pack(bundle, sizeof(bundle));
        QA_CHECK(!size || size == cxns[0].Unpack(bundle, size));

        for(netEvent* e = evtConsumers[0].NextEvent(); e; e = evtConsumers[0].NextEvent())
        {
        }

        for(netEvent* e = evtConsumers[1].NextEvent(); e; e = evtConsumers[1].NextEvent())
        {
            if(e->GetId() == NET_EVENT_FRAME_RECEIVED
                && (e->m_FrameReceived->m_Flags & NET_SEND_RELIABLE))
            {
                ++recvCount;
            }
        }

        timeStep.AddTime(TIME_INTERVAL);
    }

    //Make sure we received all the reliables.
    QA_CHECK(NUM_ITERATIONS == recvCount);

    for(int i = 0; i < NUM_CXNS; ++i)
    {
        cxns[i].Shutdown();
        //Make sure we're not leaking mem
        QA_CHECK(0 == allocators[i]->EndLayer("qa_ConnectionMem", NULL));
    }

    TST_PASS;
}

void
qa_Connection::Update(qaResult& result)
{
    m_Allocator->BeginLayer();

    static const int NUM_CXNS = 2;
    netConnection cxns[NUM_CXNS];
    qa_CxnData cxnData[NUM_CXNS];
    netTimeStep timeStep;
    netConnectionPolicies policies;
    u8 snd[64];

    //Prevent debug spew from timeouts.
    policies.m_MaxSendCount = ~0x0u;

    for(int i = 0; i < NUM_CXNS; ++i)
    {
        cxns[i].Init(NULL, &cxnData[i].m_EventQ, &timeStep, &timeStep, m_Allocator);
        cxns[i].Open(netAddress(netSocketAddress("1.2.3.4:5")), 0, i);
        cxns[i].SetPolicies(policies);
    }

    unsigned size;
    netSequence seq;

    for(int i = 0; i < m_NumMsgs; ++i, timeStep.AddTime(TIME_INTERVAL))
    {
        const bool isReliable = (0 == (sm_Rand.GetInt() % 3));

        const unsigned flags =
            (isReliable ? NET_SEND_RELIABLE : 0);

        QA_CHECK(cxns[0].QueueFrame("Hola", sizeof("Hola"), flags, &seq));

        cxnData[0].RecordSend(seq);

        size = cxns[0].Pack(snd, sizeof(snd));
        cxnData[0].ProcessEvents();

        //Randomly drop packets.
        if(size && (sm_Rand.GetInt() % 100) >= m_LossPct)
        {
            QA_CHECK(size == cxns[1].Unpack(snd, size));
            cxnData[1].ProcessEvents();
        }

        //Send ACKs to ch0.
        size = cxns[1].Pack(snd, sizeof(snd));
        cxnData[1].ProcessEvents();
        QA_CHECK(!size || size == cxns[0].Unpack(snd, size));
        cxnData[0].ProcessEvents();
    }

    //Continue packing and sending messages queued to ch0 until its outbound
    //queue is empty.
    for(; cxns[0].GetUnAckedCount() > 0; timeStep.AddTime(TIME_INTERVAL))
    {
        for(size = cxns[0].Pack(snd, sizeof(snd));
             size;
             size = cxns[0].Pack(snd, sizeof(snd)))
        {
            cxnData[0].ProcessEvents();

            //Randomly drop packets.
            if((sm_Rand.GetInt() % 100) >= m_LossPct)
            {
                QA_CHECK(size == cxns[1].Unpack(snd, size));
                cxnData[1].ProcessEvents();
            }
        }

        //Send ACKs to ch0.
        size = cxns[1].Pack(snd, sizeof(snd));
        cxnData[1].ProcessEvents();
        QA_CHECK(!size || size == cxns[0].Unpack(snd, size));
        cxnData[0].ProcessEvents();

        //sysIpcSleep(0);
    }

    //Ensure that we sent at least as many as we received.
    QA_CHECK(cxnData[0].m_SentList.size() >= cxnData[1].m_ReceivedList.size());

    //Ensure we received all the reliable messages in the proper order.
    qa_CxnData::SequenceList::iterator it0 = cxnData[0].m_SentList.begin();
    qa_CxnData::SequenceList::iterator it1 = cxnData[1].m_ReceivedList.begin();
    int numMatches = 0;

    for(; cxnData[1].m_ReceivedList.end() != it1; ++it1)
    {
        const netSequence seq1 = *it1;

        for(; cxnData[0].m_SentList.end() != it0; ++it0)
        {
            const netSequence seq0 = *it0;

            if(seq1 == seq0)
            {
                ++numMatches;
                break;
            }
        }
    }

    for(int i = 0; i < NUM_CXNS; ++i)
    {
        cxns[i].Shutdown();
    }

    QA_CHECK(numMatches == (int) cxnData[1].m_ReceivedList.size());

    //Make sure we're not leaking mem
    QA_CHECK(0 == m_Allocator->EndLayer("qa_Connection", NULL));

    const double expectedTotalTime =
        (100.0 / (100.0 - m_LossPct)) * m_NumMsgs * TIME_INTERVAL;

    QALog("qa_Connection: Total time/Expected: %d/%f\n",
           timeStep.GetCurrent(), expectedTotalTime);

    TST_PASS;
}

QA_ITEM_FAMILY(qa_ConnectionTimeout, (), ());
QA_ITEM_FAMILY(qa_ConnectionMem, (), ());
QA_ITEM_FAMILY(qa_Connection, (const int lossPct, const int numMsgs), (lossPct, numMsgs));

QA_ITEM_FAST(qa_ConnectionTimeout, (), qaResult::PASS_OR_FAIL);

QA_ITEM_FAST(qa_ConnectionMem, (), qaResult::PASS_OR_FAIL);

QA_ITEM_FAST(qa_Connection, (10, 1000), qaResult::PASS_OR_FAIL);
QA_ITEM_FAST(qa_Connection, (25, 1000), qaResult::PASS_OR_FAIL);
QA_ITEM_FAST(qa_Connection, (30, 1000), qaResult::PASS_OR_FAIL);
QA_ITEM_FAST(qa_Connection, (50, 1000), qaResult::PASS_OR_FAIL);
QA_ITEM(qa_Connection, (90, 1000), qaResult::PASS_OR_FAIL);

#endif  //__QA
