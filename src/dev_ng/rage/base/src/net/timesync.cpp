// 
// net/timesync.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "timesync.h"

#include "math/random.h"
#include "netdiag.h"
#include "system/timer.h"

/*
    Network time is synchronized by pinging a time server, the address
    of which is configured in Start().  When a sending a ping the
    client's local time is recorded in the ping message.

    When a time server receives a ping it immediately responds with a pong
    message that includes the client's original time stamp, as well as the
    server's local time.

    When the client receives the pong it computes the round trip time of the
    exchange and uses that with the server's local time at the time the pong
    was sent to estimate the current time on the server.

    roundTripTime = clientLocalTime - pingSendTime
    serverLocalTime = pongSendTime + (roundTripTime / 2)
    timeOffset = serverLocalTime - clientLocalTime

    Henceforth server local time can be estimated by adding timeOffset to
    the client's local time.

    These values are periodically refreshed by sending more pings.
*/

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, timesync)
#undef __rage_channel
#define __rage_channel ragenet_timesync

//PURPOSE
//  Network message used to synchronize time.
class netTimeSyncMsg
{
    friend class netTimeSync;

public:

    NET_MESSAGE_DECL(netTimeSyncMsg, NET_TIME_SYNC_MSG);

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerUns(msg.m_Type, datBitsNeeded<NUM_TYPES>::COUNT)
                && bb.SerUns(msg.m_Seq, 32)
				&& bb.SerUns(msg.m_Token, 32)
                && bb.SerUns(msg.m_PingSendTime, 32)
                && (msg.m_Type != TYPE_PONG || bb.SerUns(msg.m_PingRecvTime, 32));
    }

private:

    enum Type
    {
        TYPE_INVALID    = -1,
        TYPE_PING,
        TYPE_PONG,

        NUM_TYPES,
    };

    unsigned m_Type;
    unsigned m_Seq;
    unsigned m_Token;
    //Time ping was sent
    unsigned m_PingSendTime;
    //Time ping was received - present only in PONG messages.
    unsigned m_PingRecvTime;
};

NET_MESSAGE_IMPL(netTimeSyncMsg);

bool netTimeSync::ms_bUseToken = true; 

netTimeSync::netTimeSync()
    : m_CxnMgr(nullptr)
	, m_ServerEndpointId(NET_INVALID_ENDPOINT_ID)
    , m_ServerTimeOffset(0)
    , m_NextPingTime(0)
    , m_InitialPingInterval(DEFAULT_INITIAL_PING_INTERVAL)
    , m_FinalPingInterval(DEFAULT_FINAL_PING_INTERVAL)
    , m_PingInterval(DEFAULT_INITIAL_PING_INTERVAL)
    , m_AverageRTT(0)
    , m_NumRejectedPongs(0)
    , m_SentSeq(0)
    , m_RcvdSeq(0)
    , m_Flags(0)
    , m_ChannelId(NET_INVALID_CHANNEL_ID)
	, m_LastStableTime(0)
    , m_Token(0)
    , m_HasSynched(false)
    , m_HasStableTime(false)
{
    m_Dlgt.Bind(this, &netTimeSync::OnNetEvent);
}

netTimeSync::~netTimeSync()
{
    this->Stop();
}

bool
netTimeSync::Start(netConnectionManager* cxnMgr,
                    const unsigned flags,
                    const EndpointId endpointId,
                    const unsigned  timeToken,
                    const unsigned* initialServerTime,
                    const unsigned channelId,
                    const unsigned initialPingInterval,
                    const unsigned finalPingInterval)
{
    netAssert((FLAG_SERVER | FLAG_CLIENT) & flags);
    netAssert(!(FLAG_CLIENT & flags) || (NET_IS_VALID_ENDPOINT_ID(endpointId)));
    netAssert(!(FLAG_SERVER & flags) || (timeToken != 0));
    netAssert(channelId <= NET_MAX_CHANNEL_ID);
    netAssert(initialPingInterval > 0
            && finalPingInterval >= initialPingInterval);
	netAssert(finalPingInterval - initialPingInterval >= 1000);

    bool success = false;

    if(netVerify(!m_CxnMgr))
    {
        netDebug1("netTimeSync :: Starting time synchronization as %s", (FLAG_CLIENT & flags) ? "client" : "server");

        m_CxnMgr = cxnMgr;
        
        if(FLAG_CLIENT & flags)
		{
			m_ServerEndpointId = endpointId;
		}

        m_Token = timeToken;
		netDebug1("netTimeSync :: Setting time token: %u", m_Token);
        
        m_ServerTimeOffset = initialServerTime ? *initialServerTime - this->GetLocalTime() : 0;
        m_NextPingTime = 0;
        m_InitialPingInterval = initialPingInterval;
        m_FinalPingInterval = finalPingInterval;
        m_PingInterval = initialPingInterval;
        m_AverageRTT = 0;
        m_NumRejectedPongs = 0;
        m_SentSeq = m_RcvdSeq = 0;
        m_Flags = flags;
        m_ChannelId = channelId;
        m_HasSynched = !(FLAG_CLIENT & flags) || (NULL != initialServerTime);
		m_LastStableTime = 0;
        m_HasStableTime = false;
		m_IsPaused = false;
        cxnMgr->AddChannelDelegate(&m_Dlgt, m_ChannelId);
        success = true;
    }

    return success;
}

bool
netTimeSync::Restart(const unsigned flags,
                     const unsigned  *serverTimeToken,
                     const EndpointId endpointId)
{
	netAssert((FLAG_SERVER | FLAG_CLIENT) & flags);
	netAssert(!(FLAG_CLIENT & flags) || (NET_IS_VALID_ENDPOINT_ID(endpointId)));

    bool success = false;

#if !__NO_OUTPUT
    if(NET_IS_VALID_ENDPOINT_ID(endpointId))
    {
		if(m_CxnMgr)
		{
			netDebug2("netTimeSync :: Restarting time synchronization with server:" NET_ADDR_FMT "",
						NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(endpointId)));
		}
    }
    else
    {
		netDebug1("netTimeSync :: Restarting time synchronization - I'm the server");
    }
#endif

    if(netVerify(m_CxnMgr))
    {
        if(FLAG_CLIENT & flags)
		{
			m_ServerEndpointId = endpointId;
			
			// start syncing with the new server ASAP.
            m_PingInterval = m_InitialPingInterval;
			m_NextPingTime = 0;
            m_AverageRTT = 0;
            m_NumRejectedPongs = 0;
			m_Token = 0;
            netDebug2("netTimeSync :: Resetting token: %u", m_Token);
		}
		else
		{
			m_HasSynched = true;

			// new token
			m_Token = serverTimeToken ? *serverTimeToken : GenerateToken();
            netDebug2("netTimeSync :: Generated token: %u", m_Token);
		}

		m_LastStableTime = 0;
        m_HasStableTime = false;
        m_Flags = flags;
        m_IsPaused = false;
        success = true;
    }

    return success;
}

bool
netTimeSync::Stop()
{
    netDebug1("netTimeSync :: Stopping time synchronization");

    if(m_CxnMgr)
    {
        m_CxnMgr->RemoveChannelDelegate(&m_Dlgt);
        m_CxnMgr = nullptr;
    }

    m_ServerTimeOffset = 0;
    m_NextPingTime = 0;
    m_PingInterval = m_InitialPingInterval;
    m_AverageRTT = 0;
    m_NumRejectedPongs = 0;
    m_SentSeq = m_RcvdSeq = 0;
    m_Flags = 0;
    m_ChannelId = NET_INVALID_CHANNEL_ID;
    m_HasSynched = m_HasStableTime = false;
	m_LastStableTime = 0;
	m_IsPaused = false;
	m_Token = 0;

    return true;
}

void
netTimeSync::Pause()
{
	m_IsPaused = true;
}

void
netTimeSync::Unpause()
{
	m_IsPaused = false;
}

void
netTimeSync::Update()
{
    if(netVerify(m_CxnMgr))
    {
        if(m_Flags & FLAG_CLIENT)
        {
			// if paused, don't send ping messages
			if(!m_IsPaused)
			{
				const unsigned localTime = this->GetLocalTime();
				if(!m_NextPingTime || int(localTime - m_NextPingTime) >= 0)
				{
					netTimeSyncMsg ping;

					ping.m_Type = netTimeSyncMsg::TYPE_PING;
					ping.m_PingSendTime = localTime;
					ping.m_Seq = ++m_SentSeq;

					m_CxnMgr->SendOutOfBand(m_ServerEndpointId, m_ChannelId, ping, NET_SEND_IMMEDIATE);		
	               
					//Make sure next ping time is never zero.
					m_NextPingTime = (localTime + m_PingInterval) | 0x01;

					netDebug1("netTimeSync :: Sending time sync ping to:" NET_ADDR_FMT ", Time: %u, Seq: %u, Next: %u",
							  NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(m_ServerEndpointId)), localTime, ping.m_Seq, m_NextPingTime);
				}
			}
        }
    }
}

unsigned
netTimeSync::GetTime() const
{
	unsigned currentTime = this->GetLocalTime();
    if(m_HasSynched)
    {
        currentTime = this->LocalTimeToServerTime(currentTime);
        
		// prevent time from going backward
        if(m_HasStableTime && int(currentTime - m_LastStableTime) < 0)
        {
#if !__NO_OUTPUT
			// prevent multiple output messages within the same frame
			static unsigned s_loggedTime = 0; 
			if(currentTime != s_loggedTime)
			{
                netWarning("netTimeSync :: Time went backwards. Diff: %u, Stable: %u, Current: %u, Local: %u, ServerOffset: %u", (m_LastStableTime - currentTime), m_LastStableTime, currentTime, this->GetLocalTime(), m_ServerTimeOffset);
				s_loggedTime = currentTime;
			}
#endif	
			currentTime = m_LastStableTime;
        }
        else
        {
			m_LastStableTime = currentTime;
            m_HasStableTime = true;
        }
    }
    return currentTime;
}

unsigned
netTimeSync::GetLocalTime()
{
#if RSG_PC
    return sysTimer::GetSystemMsTime();
#else
	static rage::utimer_t SystemTicksAtInit = sysTimer::GetTicks();
	return static_cast<unsigned>((sysTimer::GetTicks() - SystemTicksAtInit) * sysTimer::GetTicksToMilliseconds());
#endif // RSG_PC
}

unsigned
netTimeSync::GetLocalTime(const bool UNUSED_PARAM(bUseServerSharedTime))
{
	// stub to make integrations from RDR easier
	return GetLocalTime();
}

unsigned
netTimeSync::GenerateToken()
{
    mthRandom random(static_cast<int>(sysTimer::GetSystemMsTime()));
    return static_cast<unsigned>(random.GetInt());
}

bool
netTimeSync::HasStarted() const
{
	return (m_CxnMgr != NULL);
}

bool
netTimeSync::HasSynched() const
{
    return m_HasSynched && netVerify(m_CxnMgr);
}

bool
netTimeSync::IsServer() const
{
	return (m_Flags & FLAG_SERVER) != 0;
}

bool
netTimeSync::IsClient() const
{
	return (m_Flags & FLAG_CLIENT) != 0;
}

void 
netTimeSync::SetUsingToken(const bool bUseToken)
{
	netDebug1("SetUsingToken :: Setting to %s", bUseToken ? "True" : "False");
	ms_bUseToken = bUseToken;
}

#if !__NO_OUTPUT
void 
netTimeSync::Dump()
{
	netDebug("netTimeSync::Dump - Time: %u", this->GetTime());
	netDebug("netTimeSync::Dump - Flags: %d", m_Flags);
	netDebug("netTimeSync::Dump - Synched: %s", m_HasSynched ? "True" : "False");
	netDebug("netTimeSync::Dump - HasStableTime: %s", m_HasStableTime ? "True" : "False");
	netDebug("netTimeSync::Dump - StableTime: %d", m_LastStableTime);
	netDebug("netTimeSync::Dump - Paused: %s", m_IsPaused ? "True" : "False");
	netDebug("netTimeSync::Dump - LocalTime: %u", this->GetLocalTime());
	netDebug("netTimeSync::Dump - ServerOffset: %u", m_ServerTimeOffset);
	netDebug("netTimeSync::Dump - Token: %u", m_Token);
	netDebug("netTimeSync::Dump - AverageRTT: %u", this->GetAverageRTT());
}
#endif

//private:

unsigned
netTimeSync::LocalTimeToServerTime(const unsigned localTime) const
{
    return m_ServerTimeOffset + localTime;
}

void
netTimeSync::OnNetEvent(netConnectionManager* cxnMgr,
                        const netEvent* evt)
{
	// ignore incoming events when paused
	if(m_IsPaused)
		return;

    netTimeSyncMsg tsMsg;
    //Prevent "may be used uninitialized" warnings from gcc.
    tsMsg.m_PingRecvTime = 0;

    if(NET_EVENT_FRAME_RECEIVED == evt->GetId() && 
       tsMsg.Import(evt->m_FrameReceived->m_Payload, evt->m_FrameReceived->m_SizeofPayload))
    {
		netDebug1("netTimeSync :: Received netTimeSyncMsg. Type: %d, My Flags: %d", tsMsg.m_Type, m_Flags);

        if(netTimeSyncMsg::TYPE_PING == tsMsg.m_Type &&
           (m_Flags & FLAG_SERVER))
        {
            netTimeSyncMsg pong;

            pong.m_Type = netTimeSyncMsg::TYPE_PONG;
            pong.m_Seq = tsMsg.m_Seq;
			pong.m_Token = m_Token;
            pong.m_PingSendTime = tsMsg.m_PingSendTime;

            //Send the server time (aka global time), not our local
            //time, back to the client.  That handles the cases where:
            //1.  We started as a client and now we're the server.
            //2.  We're both a client and a server.
            //
            //To clarify, once we've established a global time we need
            //to maintain it even when we go from being a client to
            //being a server.
            //
            //Note that if we started as a server and never were a client
            //then the server time will be the same as our local time (the
            //server time offset will be zero).
            //
            //Use the event time stamp (not our current local time) because
            //that's the closest estimate to when the ping was received.
            pong.m_PingRecvTime = this->LocalTimeToServerTime(evt->GetTimeStamp());

			netDebug1("netTimeSync :: Processing ping - sending pong to:" NET_ADDR_FMT ", Seq: %u, PingSend: %u, PingRecv: %u",
					  NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender), pong.m_Seq, pong.m_PingSendTime, pong.m_PingRecvTime);

            cxnMgr->SendOutOfBand(evt->m_FrameReceived->m_Sender,
                                  m_ChannelId,
                                  pong,
                                  NET_SEND_IMMEDIATE);
        }
        else if(netTimeSyncMsg::TYPE_PONG == tsMsg.m_Type
                && (m_Flags & FLAG_CLIENT)
                && int(tsMsg.m_Seq - m_RcvdSeq) > 0
                && int(evt->GetTimeStamp() - tsMsg.m_PingSendTime) > 0
                && evt->m_FrameReceived->GetEndpointId() == m_ServerEndpointId)
        {
            //The local time at which we received the pong.
            const unsigned rttime = evt->GetTimeStamp() - tsMsg.m_PingSendTime;

            netDebug2("netTimeSync :: Processing pong from:" NET_ADDR_FMT " - RTT: %u",
                      NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender),
                      rttime);

			//Estimate the current local time on the server by taking the
			//server's local time at which it received the ping and adding
			//1/2 the round trip time.
			//Offset is difference between our local time and the server's local time.
			const unsigned serverTime = tsMsg.m_PingRecvTime + (rttime / 2);
			const unsigned serverTimeOffset = serverTime - evt->GetTimeStamp();
			
#if !__NO_OUTPUT
			const unsigned absDiff = (serverTimeOffset > m_ServerTimeOffset) ? (serverTimeOffset - m_ServerTimeOffset) : (m_ServerTimeOffset - serverTimeOffset);
			static const unsigned s_nMaximumServerTimeDeviation = netConnectionPolicies::DEFAULT_TIMEOUT_INTERVAL;
			if(m_HasSynched && s_nMaximumServerTimeDeviation > 0 && (absDiff > s_nMaximumServerTimeDeviation))
				netWarning("netTimeSync :: Rejecting - ServerTimeOffset has deviated too far (%u, %u)", serverTimeOffset, m_ServerTimeOffset);
#endif

			//Only accept if the server token has not been set or if the token is equal to what we know, or the token more recent
			if(ms_bUseToken && !(m_Token == 0 || (m_Token == tsMsg.m_Token)))
			{
                netWarning("netTimeSync :: Rejecting - Unexpected token (Received:%u, Expected:%u)", tsMsg.m_Token, m_Token);
			}
			else if(rttime > MAX_ACCEPTED_PONG_RTT && (m_NumRejectedPongs < MAX_REJECTIONS))
            {
                netWarning("netTimeSync :: Dropping pong due to large RTT (RTT: %u, Max: %u, Rejected: %u)", rttime, MAX_ACCEPTED_PONG_RTT, m_NumRejectedPongs);

                //Force a ping message to be sent next update
                m_NextPingTime = 0;
                m_NumRejectedPongs++;
            }
            else
            {
                if(m_AverageRTT == 0)
                {
                    m_AverageRTT = rttime;
                }

                if((rttime > ALWAYS_ACCEPTED_PONG_RTT) && ((rttime / m_AverageRTT) >= MAX_PONG_RTT_DEV_MULT) && (m_NumRejectedPongs < MAX_REJECTIONS))
                {
                    netWarning("netTimeSync :: Rejecting - Large deviation from average RTT (RTT: %u, Avg: %u, Max: %u, Rejected: %u)", rttime, m_AverageRTT, MAX_PONG_RTT_DEV_MULT, m_NumRejectedPongs);

                    //Force a ping message to be sent next update
                    m_NextPingTime = 0;
                    m_NumRejectedPongs++;
                }
                else
                {
                    m_RcvdSeq = tsMsg.m_Seq;
					m_ServerTimeOffset = serverTimeOffset;
					m_Token = tsMsg.m_Token;

                    if(m_PingInterval < m_FinalPingInterval)
                    {
                        m_PingInterval = m_PingInterval * 2;

                        if(m_PingInterval > m_FinalPingInterval)
                        {
                            m_PingInterval = m_FinalPingInterval;
                        }
                    }

                    //Reset this 
                    m_NumRejectedPongs = 0;

                    if(!m_HasSynched)
                    {
                        m_LastStableTime = this->LocalTimeToServerTime(this->GetLocalTime());
                    }

					netDebug1("netTimeSync :: Handled pong. Seq: %u, PingInterval: %u, HasSynched: %s, LastStableTime: %u, ServerTimeOffset: %u, ServerTime: %u, Token: %u",
						       tsMsg.m_Seq,
							   m_PingInterval,
							   m_HasSynched ? "T" : "F",
							   m_LastStableTime,
							   m_ServerTimeOffset,
							   serverTime,
							   m_Token);

					m_HasSynched = m_HasStableTime = true;
                }

                m_AverageRTT = (m_AverageRTT + rttime) / 2;
            }
        }
        else
        {
            netWarning("netTimeSync :: Unhandled netTimeSyncMsg from: " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));
        }
    }
}

}   //namespace rage
