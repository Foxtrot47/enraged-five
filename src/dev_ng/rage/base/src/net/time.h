// 
// net/time.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_TIME_H 
#define NET_TIME_H 

namespace rage
{

//PURPOSE
//  Implements a simple millisecond stopwatch
class netStopWatch
{
public:

    netStopWatch();

    //PURPOSE
    //  Pass true to start the stopwatch
    explicit netStopWatch(const bool start);

    //PURPOSE
    //  Starts the stopwatch.
    void Start();

    //PURPOSE
    //  Stops the stopwatch.
    void Stop();

    //PURPOSE
    //  Resets the stopwatch to its initial state
    void Reset();

    //PURPOSE
    //  Resets and then starts the stopwatch
    void Restart();

    //PURPOSE
    //  Returns true if the stopwatch is running
    bool IsRunning() const;

    //PURPOSE
    //  Returns time elapsed since Start() was called.
    //  Calling Stop() freezes the elapsed time.
    unsigned GetElapsedMilliseconds() const;

private:

    unsigned m_StartTime;
    mutable unsigned m_Elapsed;
    bool m_IsRunning  : 1;
};

//PURPOSE
//  Computes time steps.
class netTimeStep
{
public:

    netTimeStep();

    //PURPOSE
    //  Initialize with the current time.
    //NOTES
    void Init(const unsigned curTime);

    //PURPOSE
    //  Sets the current time.
    //PARAMS
    //  curTime - Current time.
    //NOTES
    //  curTime must be larger than the prior value passed to SetTime().
    //  Values of curTime that generate negative time steps are not permitted.
    //
    //  Wrap is handled as long as the difference between two consecutive time
    //  values is less than 2^(n-2), where n is the bit size of unsigned.
    void SetTime(const unsigned curTime);

    //PURPOSE
    //  Adds a delta to the current time.
    //PARAMS
    //  deltaTime   - Time delta.
    //NOTES
    //  deltaTime must be less than 2^(n-2), where n is the bit size of unsigned.
    void AddTime(const unsigned deltaTime);

    //PURPOSE
    //  Returns the current time.
    unsigned GetCurrent() const;

    //PURPOSE
    //  Returns the current time step, which is the difference between the
    //  current time and the previous time.
    int GetTimeStep() const;

    //PURPOSE
    //  Returns true if a time step has been computed.  A time step won't
    //  be computed until the second invocation of SetTime()/AddTime() after
    //  an invocation of Clear().
    bool HasTimeStep() const;

private:

    volatile unsigned m_CurTime;
    volatile unsigned m_TimeStep;
};

//PURPOSE
//  Computes time-outs.
class netTimeout
{
public:

	// the default amount of time in milliseconds for a frame to be considered
	// long or stalled. Can be passed to SetLongFrameThreshold();
	static const unsigned DEFAULT_LONG_FRAME_THRESHOLD_MS = (3 * 1000);

    netTimeout();

    //PURPOSE
    //  Starts the timer with the given number of milliseconds.
    //  After calling InitMilliseconds() IsTimedOut() will return false.
    void InitMilliseconds(const int timeoutMs);

    //PURPOSE
    //  Starts the timer with the given number of seconds.
    //  After calling InitSeconds() IsTimedOut() will return false.
    void InitSeconds(const int timeoutSeconds);

    //PURPOSE
    //  Stops and clears the timer.
    //  After calling Clear() IsTimedOut() will return false.
    void Clear();

    //PURPOSE
    //  Resets the timer and begins the countdown.
    //  After calling Reset() IsTimedOut() will return false.
    void Reset();

    //PURPOSE
    //  Updates the timeout.  Must be called at least once per frame.
    void Update();

    //PURPOSE
    //  Extend the timeout by the given number of milliseconds.
    //  No effect if IsTimedOut() already returns true.
    void ExtendMilliseconds(const int ms);

    //PURPOSE
    //  Extend the timeout by the given number of seconds
    //  No effect if IsTimedOut() already returns true.
    void ExtendSeconds(const int seconds);

    //PURPOSE
    //  Returns the timeout interval in milliseconds.
    unsigned GetTimeoutIntervalMilliseconds() const;

    //PURPOSE
    //  Returns the timeout interval in seconds.
    unsigned GetTimeoutIntervalSeconds() const;

    //PURPOSE
    //  Returns the number of milliseconds until a timeout occurs.
    unsigned GetMillisecondsUntilTimeout() const;

    //PURPOSE
    //  Returns the number of seconds until a timeout occurs.
    //NOTES
    //  The value returned is GetTimeUntilTimeout() / 1000, implying
    //  it's always rounded down.
    unsigned GetSecondsUntilTimeout() const;

    //PURPOSE
    //  Returns the number of milliseconds since the last call to Update()
    unsigned GetMillisecondsSinceLastUpdate() const;

    //PURPOSE
    //  Returns the number of seconds since the last call to Update()
    //NOTES
    //  The value returned is GetMillisecondsSinceLastUpdate() / 1000, implying
    //  it's always rounded down.
    unsigned GetSecondsSinceLastUpdate() const;

	//PURPOSE
	//  Allows the timeout to adjust for long frame times/stalls.
	//  If a frame takes too long then adjust the timeout by the frame duration.
	//  This compensates for cases where the game is stuck on an assert or
	//  breakpoint for too long.
	//PARAMS
	//  longFrameThresholdMs - the amount of time in milliseconds for
	//						   a frame to be considered long or stalled.
	//						   Set to 0 to disable long frame compensation.
	void SetLongFrameThreshold(const unsigned longFrameThresholdMs);

    //PURPOSE
    //  Returns true if currently tracking a timeout.
    //  This will return false if one of the Init*() functions
    //  has not been called, or if Clear() has been called.
    //bool IsActive() const;

    //PURPOSE
    //  Returns true if currently counting down.
    bool IsRunning() const;

    //PURPOSE
    //  Forces a timeout to occur now.
    void ForceTimeout();

    //PURPOSE
    //  Returns true if a timeout has occurred.
    //NOTES
    //  IsTimedOut() will continue returning true until Reset() is called.
    bool IsTimedOut() const;

private:

    unsigned m_CurTime;
	unsigned m_LongFrameThresholdMs;
    int m_Timeout;
    int m_Countdown;
};

//PURPOSE
//  Used to compute random retry intervals within a min/max range.
//  This is useful in client/server applications where the client wants
//  to avoid connecting to the server simultaneously with 1000's of other
//  clients, especially after a server outage.
//  Each call to Reset() randomly selects a retry interval that's within
//  the min/max range passed to Init().  Clients using netRetryTimer
//  to time connection retries should be evenly distributed in their
//  retry attempts, thus evening out the load on the server.
class netRetryTimer
{
public:

    netRetryTimer();

    ~netRetryTimer();

    //PURPOSE
    //  Initialize the retry timer with the min/max retry intervals
    //  and call Start().
    //PARAMS
    //  minRetryIntervalMs  - Minimum retry interval in milliseconds.
    //  maxRetryIntervalMs  - Maximum retry interval in milliseconds.
    //NOTES
    //  The actual retry interval will be a random value between min/max
    //  and will be recalculated each time Reset() is called.
    //  If minRetryIntervalMs and maxRetryIntervalMs are equal then there
    //  will be no randomness to the retry interval.
    bool InitMilliseconds(const int minRetryIntervalMs,
                            const int maxRetryIntervalMs);

    //PURPOSE
    //  Initialize the retry timer with the min/max retry intervals
    //  and call Start().
    //PARAMS
    //  minRetryIntervalSecs    - Minimum retry interval in seconds.
    //  maxRetryIntervalSecs    - Maximum retry interval in seconds.
    //NOTES
    //  The actual retry interval will be a random value between min/max
    //  and will be recalculated each time Reset() is called.
    //  If minRetryIntervalSecs and maxRetryIntervalSecs are equal then there
    //  will be no randomness to the retry interval.
    bool InitSeconds(const int minRetryIntervalSecs,
                        const int maxRetryIntervalSecs);

    //PURPOSE
    //  Initialize the retry timer with a constant retry interval
    //  and call Start().
    //PARAMS
    //  retryIntervalMs - Retry interval in milliseconds.
    //NOTES
    //  Using this method is equivalent to calling Init() with the same
    //  value for min/max interval. There will be no randomness to the
    //  retry interval.
    bool InitMilliseconds(const int retryIntervalMs);

    //PURPOSE
    //  Initialize the retry timer with a constant retry interval
    //  and call Start().
    //PARAMS
    //  retryIntervalSecs   - Retry interval in seconds.
    //NOTES
    //  Using this method is equivalent to calling InitSeconds() with the same
    //  value for min/max interval. There will be no randomness to the
    //  retry interval.
    bool InitSeconds(const int retryIntervalSecs);

    //PURPOSE
    //  Shuts down the retry timer.
    void Shutdown();

    //PURPOSE
    //  Resets the retry timer and selects a new random retry timeout
    //  within the interval range passed to Init().
    //  The running state of the timer is not affected, i.e stopped/running.
    void Reset();

    //PURPOSE
    //  Resets the retry timer and sets a new min/max retry range.
    //NOTES
    //  ResetInterval*() differs from Init*() in that ResetInterval*() does
    //  not reset the actual timer, it only resets the retry interval.
    //  The running state of the timer is not affected, i.e stopped/running.
    //
    //  The actual retry interval will be a random value between min/max
    //  and will be recalculated each time Reset() is called.
    //
    //  If minRetryIntervalMs and maxRetryIntervalMs are equal then there
    //  will be no randomness to the retry interval.
    void ResetIntervalMilliseconds(const int minRetryIntervalMs,
                                    const int maxRetryIntervalMs);

    //PURPOSE
    //  Resets the retry timer and sets a new min/max retry range.
    //NOTES
    //  ResetInterval*() differs from Init*() in that ResetInterval*() does
    //  not reset the actual timer, it only resets the retry interval.
    //  The running state of the timer is not affected, i.e stopped/running.
    //
    //  The actual retry interval will be a random value between min/max
    //  and will be recalculated each time Reset() is called.
    //  If minRetryIntervalSecs and maxRetryIntervalSecs are equal then there
    //  will be no randomness to the retry interval.
    void ResetIntervalSeconds(const int minRetryIntervalSecs,
                            const int maxRetryIntervalSecs);

    //PURPOSE
    //  Resets the retry timer and sets a constant retry interval.
    //NOTES
    //  ResetInterval*() differs from Init*() in that ResetInterval*() does
    //  not reset the actual timer, it only resets the retry interval.
    //  The running state of the timer is not affected, i.e stopped/running.
    //
    //  Using this method is equivalent to calling ResetIntervalMilliseconds()
    //  with the same value for min/max interval. There will be no randomness
    //  to the retry interval.
    void ResetIntervalMilliseconds(const int retryIntervalMs);

    //PURPOSE
    //  Resets the retry timer and sets a constant retry interval.
    //NOTES
    //  ResetInterval*() differs from Init*() in that ResetInterval*() does
    //  not reset the actual timer, it only resets the retry interval.
    //  The running state of the timer is not affected, i.e stopped/running.
    //
    //  Using this method is equivalent to calling ResetIntervalSeconds()
    //  with the same value for min/max interval. There will be no randomness
    //  to the retry interval.
    void ResetIntervalSeconds(const int retryIntervalSecs);

    //PURPOSE
    //  Scales the retry interval range (max - min).  After calling ScaleRange()
    //  retry intervals will be in the range min + ((max - min) * (scaleTimes100/100)).
    //  For example, to expand the range by 75% (1.75 times), call:
    //      ScaleRange(175);
    //  To reduce the range by 50% (.5 times) call:
    //      ScaleRange(50);
    //NOTES
    //  This function is typically used when a server outage is suspected
    //  and the client wants back off the retry frequency.  The minimum
    //  retry interval remains in tact, but the range of intervals is expanded.
    //
    //  For intervals where the range is zero, i.e. the min and max interval are equal,
    //  the entire interval is scaled.  For example, if min/max are 60 seconds and the
    //  range is scaled by two then min/max will be 120 seconds.
    void ScaleRange(const unsigned scaleTimes100);

    //PURPOSE
    //  Scales the retry interval range and caps the upper limit of the interval.
    void ScaleRange(const unsigned scaleTimes100,
                    const unsigned upperLimitIntervalMs);

    //PURPOSE
    //  Restores the retry interval range specified in the call to Init().
    //NOTES
    //  Use this function after calling ScaleInterval() to restore the
    //  original interval.
    void RestoreRange();

    //PURPOSE
    //  Starts the countdown timer.  The countdown timer is started implicitly
    //  in Init().
    void Start();

    //PURPOSE
    //  Stops the countdown timer.
    void Stop();

    //PURPOSE
    //  Restarts the countdown timer.  Same as calling Reset() then Start().
    void Restart();

    //PURPOSE
    //  Updates the countdown timer.  This should be called once per frame.
    void Update();

    //PURPOSE
    //  Returns the value of the current retry interval in milliseconds.
    int GetCurrentRetryIntervalMilliseconds() const;

    //PURPOSE
    //  Returns the value of the current retry interval in seconds.
    int GetCurrentRetryIntervalSeconds() const;

    //PURPOSE
    //  Returns the value of the minimum retry interval in milliseconds.
    int GetMinRetryIntervalMilliseconds() const;

    //PURPOSE
    //  Returns the value of the minimum retry interval in seconds.
    int GetMinRetryIntervalSeconds() const;

    //PURPOSE
    //  Returns the value of the maximum retry interval in milliseconds.
    int GetMaxRetryIntervalMilliseconds() const;

    //PURPOSE
    //  Returns the value of the maximum retry interval in seconds.
    int GetMaxRetryIntervalSeconds() const;

    //PURPOSE
    //  Returns the number of milliseconds until a retry should be attempted.
    int GetMillisecondsUntilRetry() const;

    //PURPOSE
    //  Returns the number of seconds until a retry should be attempted.
    int GetSecondsUntilRetry() const;

    //PURPOSE
    //  Force the timer to time out such that IsTimeToRetry() will return true.
    void ForceRetry();

    //PURPOSE
    //  Returns true if a retry should be attempted.
    //NOTES
    //  IsTimeToRetry() will continue returning true until Reset() is called.
    bool IsTimeToRetry() const;

    //PURPOSE
    //  Returns true if the retry timer has been stopped.
    bool IsStopped() const;

private:

    static const unsigned RANGE_SCALE    = 100;

    netTimeStep m_TimeStep;

    int m_MinRetryIntervalMs;   //Minimum retry interval
    int m_MaxRetryIntervalMs;   //Maximum retry interval
    int m_RetryIntervalRangeMs; //Interval range (max - min)
    int m_CurRetryIntervalMs;   
    int m_RetryTimeoutMs;
    unsigned m_RangeScale;

    bool m_Stopped  : 1;
};

/* PURPOSE
	Used for retrying functionality with a simple reliable and non-random backoff timer. This is particularly useful for basic
	functionality that we want to retry immediately, try again in a few seconds, and then go to a holding pattern with a long retry.
	This allows game functionality to quick attempt a retry to avoid bailing out of important multiplayer functionality, but then
	prevents DDOS from refreshing at that quick interval. A little easier and more predictable to use than netRetryTimer.
	Users pass in an initial retry timer, a second retry timer, a final interval.
---------------------------------------------------------------------------------------------------
USAGE:

	netRetryAndBackoffTimer backoffTimer;
	
	void Init()
	{
		// Initial backoff of 1 seconds, then 15, and finally enter a holding pattern of 60 seconds
		backoffTimer.Init(1, 15, 60);
	}

	void Update()
	{
		if (backoffTimer.IsReadyToRetry())
		{
			if (DoWork())
			{
				backoffTimer.Reset();
			}
			else
			{
				// Action failed,
				backoffTimer.Increment();
			}
		}
	}

*/
class netRetryAndBackoffTimer
{
public:

	// PURPOSE
	//	Initializes the backoff timer with an initial retry value, the first backoff timer, and holding backoff.
	void Init(unsigned retryMs, unsigned initialBackoffMs, unsigned holdingBackoffMs);

	// PURPOSE
	//	Calls InitMs internally after converting to milliseconds.
	void InitSeconds(unsigned retry, unsigned initialBackoff, unsigned holdingBackoff);

	// PURPOSE
	//	Attempts to increment the current interval, will not exceed the holding position
	void Increment();

	// PURPOSE
	//	Resets the start time and current interval
	void Reset();

	// PURPOSE
	//	Returns TRUE if the timer is ready to retry
	bool IsReadyToRetry();

	// PURPOSE
	//	Get current interval 
	unsigned GetCurrentInterval() { return m_Intervals[m_CurrentIndex]; }

private:
	static const unsigned NUM_INTERVALS = 3;
	bool m_bInitialized;
	unsigned m_Intervals[NUM_INTERVALS];
	unsigned m_StartTime;
	unsigned m_CurrentIndex;
};

} // namespace rage

#endif // NET_TIME_H 
