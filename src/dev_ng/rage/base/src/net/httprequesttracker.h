// 
// net/httprequesttracker.h 
// 
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved. 
// 

#if !__NO_OUTPUT

#ifndef NET_HTTP_REQUEST_TRACKER_H 
#define NET_HTTP_REQUEST_TRACKER_H 

#include "atl/delegate.h"

namespace rage
{

// Tracks HTTP request counts and calls a callback if there are too many in rapid succession.
// The HTTP requests to look out for, and properties for determining if they are too frequent,
// are defined in an XML file.
class netHttpRequestTracker 
{
	typedef atDelegator<void (const char* sRuleName, 
		                      const char* sRuleMatch, 
							  const unsigned nTokenLimit, 
							  const float nTokenRate)> Delegator;

public:
	typedef Delegator::Delegate Delegate;

	static bool Init();

	static void Update();

	// Checks whether the given URI matches with one of the rules and dispatches the delegate if it does.
	static void HandleRequest(const char* uri);

	static void AddDelegate(Delegate* dlgt);
	static void RemoveDelegate(Delegate* dlgt);

private:
	static bool LoadRules(const char* sRulesFileXML);

	static void DispatchDelegate(const char* sRuleName, 
		                         const char* sRuleMatch,
								 const unsigned nTokenLimit, 
								 const float nTokenRate);
	
	static unsigned sm_nPrevTime;

	static Delegator sm_Delegator;
};

} // namespace rage

#endif // NET_HTTP_REQUEST_TRACKER_H 

#endif // !__NO_OUTPUT
