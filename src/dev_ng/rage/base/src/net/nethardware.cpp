// 
// net/nethardware.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "nethardware.h"

#include "atl/uuid.h"
#include "atl/guid.h"
#include "diag/channel.h"
#include "file/tcpip.h"
#include "netdiag.h"
#include "profile/rocky.h"
#include "system/memops.h"
#include "system/param.h"

#if RSG_ORBIS
#include <np.h>
#include <libnetctl.h>
#include "rline/rlnp.h"			//For rlNp::IsOnline
#include "rline/rlnpcommon.h"
#elif RSG_PC
#include "rline/ros/rlros.h"	//For rlRos::IsOnline()
#elif RSG_LINUX
#include "rline/ggp/rlggp.h"	// For IsOnline()
#elif RSG_DURANGO
#include "rline/durango/rlxbl_interface.h" //For IsOnline()
#elif RSG_NX
#include "rline/rl.h"
#include "rline/ros/rlros.h"
#endif //RSG_ORBIS

#if RSG_BANK
#include "bank/bkmgr.h"
#include "bank/bank.h"
#endif // RSG_BNK

PARAM(lan, "[network] Use LAN sessions instead of online sessions");

namespace rage
{

RAGE_DECLARE_CHANNEL(ragenet);
RAGE_DEFINE_SUBCHANNEL(ragenet, hardware)
#undef __rage_channel
#define __rage_channel ragenet_hardware

AUTOID_IMPL(netHardwareEvent);
AUTOID_IMPL(netHardwareEventNetworkAvailabilityChanged);
AUTOID_IMPL(netHardwareEventSocketBindingChanged);
AUTOID_IMPL(netHardwareEventReachabilityChanged);

netHardware::State netHardware::sm_State = STATE_STOPPED;
netHardware::SocketList netHardware::sm_ActiveSockets;
sysCriticalSectionToken netHardware::sm_Cs;
netHardware::NetworkInfo netHardware::sm_NetInfo;
netHardware::Delegator netHardware::sm_Delegator;
static bool s_netHwIsOnline = false;
static bool s_netHwInitialized = false;
#if !__FINAL
static bool s_netHwInitializedForDebug = false;
#endif
bool netHardware::sm_bIpReleaseSocketError = false;

bool
netHardware::Init()
{
#if !__FINAL
	if(s_netHwInitializedForDebug)
	{
		// flag that our initialization is now not debug (so that we can catch double initialization as normal)
		s_netHwInitializedForDebug = false;
		return true; 
	}
#endif

    bool success = false;
    if(netVerify(!s_netHwInitialized))
    {
        sm_State = STATE_STOPPED;
        success = NativeInit();
        s_netHwInitialized = success;
    }

	// Initialize the Mac address being used for GUID generation
	rage::UuidUtils::uuid_address_t mac;
	if (GetMacAddress(mac.eaddr))
	{
		rage::UuidUtils::encryptMacAddress(&mac);
		rage::UuidUtils::uuid_set_address(&mac);
	}

#if RSG_BANK
	CreateBankWidget();
#endif // RSG_BANK

    return success;
}

#if !__FINAL
bool
netHardware::InitDebug()
{
	if(!s_netHwInitialized)
	{
		s_netHwInitializedForDebug = true; 
		return Init();
	}
	return true;
}
#endif

void
netHardware::Shutdown()
{
    if(s_netHwInitialized)
    {
        while(!sm_ActiveSockets.empty())
        {
            netHardware::DestroySocket(*sm_ActiveSockets.begin());
        }

        NativeStopNetwork();

        NativeShutdown();

        s_netHwInitialized = false;
		s_netHwIsOnline = false;
		sm_bIpReleaseSocketError = false;

#if !__FINAL
		s_netHwInitializedForDebug = false;
#endif
    }
}

netNatType
netHardware::GetNatType()
{
    if(NET_NAT_UNKNOWN == sm_NetInfo.m_NatType)
    {
        GetNetworkInfo();
    }

    return sm_NetInfo.m_NatType;
}

#if !__NO_OUTPUT
const char*
netHardware::GetNatTypeString(netNatType natType)
{
	switch(natType)
	{
	case NET_NAT_OPEN: return "Open"; break;
	case NET_NAT_MODERATE: return "Moderate"; break;
	case NET_NAT_STRICT: return "Strict"; break;
	default: return "Unknown NAT Type"; break;
	}
}
#endif

bool
netHardware::GetMacAddress(u8 (&mac)[6])
{
    bool success = false;
    if(!(NetworkInfo::NETINFO_MAC_ADDR & sm_NetInfo.m_Flags))
    {
        GetNetworkInfo();
    }
    
	
    if(NetworkInfo::NETINFO_MAC_ADDR & sm_NetInfo.m_Flags)
    {
		
        sysMemCpy(mac, sm_NetInfo.m_MacAddress, sizeof(mac));
        success = true;
		
    }

    return success;
}

bool
netHardware::GetLocalIpAddress(netIpAddress* ip)
{
    bool success = false;
    if(!(NetworkInfo::NETINFO_LOCAL_IP & sm_NetInfo.m_Flags))
    {
        GetNetworkInfo();
    }
    
    if(NetworkInfo::NETINFO_LOCAL_IP & sm_NetInfo.m_Flags)
    {
        *ip = sm_NetInfo.m_LocalIp;
        success = true;
    }

    return success;
}

bool
netHardware::GetPublicIpAddress(netIpAddress* ip)
{
    bool success = false;
    if(!(NetworkInfo::NETINFO_PUBLIC_IP & sm_NetInfo.m_Flags))
    {
        GetNetworkInfo();
    }
    
    if(NetworkInfo::NETINFO_PUBLIC_IP & sm_NetInfo.m_Flags)
    {
        *ip = sm_NetInfo.m_PublicIp;
        success = true;
    }

    return success;
}

bool
netHardware::GetMtu(u32& mtu)
{
	return NativeGetMtu(mtu);
}

bool
netHardware::IsOnline()
{
    return s_netHwIsOnline;
}

bool
netHardware::IsAvailable()
{
    return s_netHwInitialized && sm_State == STATE_AVAILABLE;
}

bool
netHardware::IsInitialized()
{
	return s_netHwInitialized;
}

void
netHardware::Update()
{
	PROFILE

    SYS_CS_SYNC(sm_Cs);

    if(s_netHwInitialized)
    {
        const unsigned oldState = sm_State;

        NativeUpdate();

        if(STATE_AVAILABLE != oldState && STATE_AVAILABLE == sm_State)
        {
            netDebug2("Network has become available.");

            SocketList::iterator it = sm_ActiveSockets.begin();
            SocketList::const_iterator stop = sm_ActiveSockets.end();
            for(; stop != it; ++it)
            {
				netSocket* skt = *it;
                if(skt->NativeBind())
				{
					netHardwareEventSocketBindingChanged e(skt);
					DispatchEvent(&e);
				}
            }

			netHardwareEventNetworkAvailabilityChanged e(true);
			DispatchEvent(&e);
        }
        else if(STATE_STOPPED != oldState && STATE_STOPPED == sm_State)
        {
            netDebug2("Network has become unavailable.");

            SocketList::iterator it = sm_ActiveSockets.begin();
            SocketList::const_iterator stop = sm_ActiveSockets.end();
            for(; stop != it; ++it)
            {
				netSocket* skt = *it;
                skt->NativeUnbind();

				netHardwareEventSocketBindingChanged e(skt);
				DispatchEvent(&e);
            }

			netHardwareEventNetworkAvailabilityChanged e(false);
			DispatchEvent(&e);
        }

        SocketList::iterator it = sm_ActiveSockets.begin();
        SocketList::const_iterator stop = sm_ActiveSockets.end();
        for(; stop != it; ++it)
        {
            (*it)->Update();
        }

		bool isOnline = false;

#if RSG_NP
		for (int i = 0; i < RL_MAX_LOCAL_GAMERS && !isOnline; ++i)
			isOnline = g_rlNp.IsOnline(i);
#elif __RGSC_DLL
		isOnline = netHardware::IsLinkConnected();
#elif RSG_LINUX
		isOnline = g_rlGgp.IsOnline();
#elif RL_SC_PLATFORMS
        for(int i = 0; i < RL_MAX_LOCAL_GAMERS && !isOnline; ++i)
			isOnline = rlRos::IsOnline(i);
#elif RSG_DURANGO
		isOnline = g_rlXbl.GetPresenceManager()->IsOnline();
#endif

		SetOnlineState(isOnline);
    }
}

void
netHardware::AddDelegate(netHardware::Delegate* dlgt)
{
	sm_Delegator.AddDelegate(dlgt);
}

void
netHardware::RemoveDelegate(netHardware::Delegate* dlgt)
{
	sm_Delegator.RemoveDelegate(dlgt);
}

void
netHardware::DispatchEvent(netHardwareEvent* e)
{
	sm_Delegator.Dispatch(*e);
}

#if !__NO_OUTPUT
static const char* GetProtocolString(const netProtocol proto)
{
    if(NET_PROTO_UDP == proto)
    {
        return "UDP";
    }
    else
    {
        return "UNKNOWN";
    }
}
#endif  //__NO_OUTPUT

bool
netHardware::CreateSocket(netSocket* socket,
                            const unsigned short port,
                            const netProtocol proto,
                            const netSocketBlockingType blockingType,
                            const netAddressFamily addressFamily /*= NET_DEFAULT_ADDRESS_FAMILY  */,
							const unsigned sendBufferSize /*= 0*/,
							const unsigned receiveBufferSize /*= 0*/)
{
    SYS_CS_SYNC(sm_Cs);

    bool success = true;

    netDebug("Creating %s %s protocol socket on port %d...",
            (NET_SOCKET_BLOCKING == blockingType) ? "BLOCKING" : "NON-BLOCKING",
            GetProtocolString(proto),
            port);

    if(netVerify(s_netHwInitialized) && netVerify(!socket->m_IsCreated))
    {
        sm_ActiveSockets.push_back(socket);
        socket->m_IsCreated = true;

        socket->Init(port, proto, blockingType, addressFamily, sendBufferSize, receiveBufferSize);

        if(IsAvailable())
        {
			if(socket->NativeBind())
			{
				netHardwareEventSocketBindingChanged e(socket);
				DispatchEvent(&e);
			}
			else
            {
                DestroySocket(socket);
                success = false;
            }
        }
    }
    
    return success;
}

void
netHardware::DestroySocket(netSocket* socket)
{
    SYS_CS_SYNC(sm_Cs);

    netAssert(socket);

    if(netVerify(s_netHwInitialized) && socket->m_IsCreated)
    {
        netDebug("Destroying socket on port %d...", socket->GetPort());

        sm_ActiveSockets.erase(socket);
        socket->Shutdown();
        socket->m_IsCreated = false;
    }
}

//private:

void
netHardware::SetOnlineState(const bool isOnline)
{
    const bool wasOnline = s_netHwIsOnline;

    s_netHwIsOnline = isOnline;

    if(!s_netHwIsOnline && wasOnline)
    {
        sm_NetInfo.SetOffline();
    }

#if !__NO_OUTPUT
    if(wasOnline != isOnline)
    {
        if(isOnline)
        {
            netDebug("Came online...");
        }
        else
        {
            netDebug("Went offline...");
        }
    }
#endif  //__NO_OUTPUT
}

void
netHardware::GetNetworkInfo()
{
	
    SYS_CS_SYNC(sm_Cs);

	
    if(!(sm_NetInfo.m_Flags & NetworkInfo::NETINFO_MAC_ADDR))
    {
        if(NativeGetMacAddress(sm_NetInfo.m_MacAddress))
        {
            sm_NetInfo.m_Flags |= NetworkInfo::NETINFO_MAC_ADDR;
        }
    }

	
    if(!(sm_NetInfo.m_Flags & NetworkInfo::NETINFO_LOCAL_IP))
    {
        if(NativeGetLocalIpAddress(&sm_NetInfo.m_LocalIp))
        {
            sm_NetInfo.m_Flags |= NetworkInfo::NETINFO_LOCAL_IP;

#if !__NO_OUTPUT
			// Push this IP addr down to the rage\base level too
			fiDeviceTcpIp::SetLocalIpAddrName(sm_NetInfo.m_LocalIp.ToString());
#endif
        }
    }

    if(!(sm_NetInfo.m_Flags & NetworkInfo::NETINFO_PUBLIC_IP))
    {
        if(NativeGetPublicIpAddress(&sm_NetInfo.m_PublicIp))
        {
            sm_NetInfo.m_Flags |= NetworkInfo::NETINFO_PUBLIC_IP;
        }
    }
	

	//Assume we can't get our NAT type until we know our public IP.
	//Unless we're using -lan.. then assume no firewalls
	if (PARAM_lan.Get())
	{
		sm_NetInfo.m_NatType = NET_NAT_OPEN;
	}
    else if(NET_NAT_UNKNOWN == sm_NetInfo.m_NatType
        && (sm_NetInfo.m_Flags & NetworkInfo::NETINFO_PUBLIC_IP))
    {
        sm_NetInfo.m_NatType = NativeGetNatType();
    }
	
}

void netHardware::NotifyIpReleaseSocketError()
{
	sm_bIpReleaseSocketError = true;
}

bool netHardware::HasIpReleaseSocketError()
{
	return sm_bIpReleaseSocketError;
}

void netHardware::ConsumeIpReleaseSocketError()
{
	sm_bIpReleaseSocketError = false;
}

#if RSG_BANK

static netHardwareReachability s_NetHardwareReachability = REACHABILITY_VIA_WIFI;

void netHardwareSetReachability(netHardwareReachability reachability)
{
	s_NetHardwareReachability = reachability;

	// dispatch an event with the new reachability
	netHardwareEventReachabilityChanged evt(s_NetHardwareReachability);
	netHardware::DispatchEvent(&evt);
}

void netHardware::CreateBankWidget()
{
	bkBank& bank = BANKMGR.CreateBank("netHardware");
	{
		bank.AddButton("Set Reachability to WiFi", datCallback(CFA1(netHardwareSetReachability), CallbackData(REACHABILITY_VIA_WIFI)));
		bank.AddButton("Set Reachability to Cellular", datCallback(CFA1(netHardwareSetReachability), CallbackData(REACHABILITY_VIA_WWAN)));
		bank.AddButton("Set Reachability to Wired", datCallback(CFA1(netHardwareSetReachability), CallbackData(REACHABILITY_VIA_WIRED)));
		bank.AddButton("Set Reachability to NONE", datCallback(CFA1(netHardwareSetReachability), CallbackData(REACHABILITY_NONE)));
	}
}

#endif // RSG_BANK
#if RSG_PC || RSG_DURANGO
// We don't have code for reachability on PC, so this just uses the widget value
netHardwareReachability netHardware::GetReachability()
{
#if RSG_BANK
	return s_NetHardwareReachability;
#else
	return REACHABILITY_VIA_WIFI;
#endif
}
#endif // RSG_PC

}   //namespace rage
