// 
// net/stream.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "stream.h"

#include "profile/rocky.h"

namespace rage
{

///////////////////////////////////////////////////////////////////////////////
//  netStreamFragment
///////////////////////////////////////////////////////////////////////////////

static unsigned s_NextId;

unsigned
netStreamFragment::NextId()
{
    unsigned id = sysInterlockedIncrement(&s_NextId);

    while(NET_INVALID_STREAM_ID == id)
    {
        id = sysInterlockedIncrement(&s_NextId);
    }

    return id;
}

bool
netStreamFragment::ReadHeader(const void* buf,
                            const unsigned sizeofBuf,
                            unsigned* streamId,
                            unsigned* streamSize)
{
    bool success = false;
    unsigned sig = SIG_INVALID;
    if(sizeofBuf > BYTE_SIZEOF_HEADER)
    {
        datBitBuffer::ReadUnsigned(buf,
                                    sig,
                                    BIT_SIZEOF_SIG,
                                    BIT_OFFSETOF_SIG);
        datBitBuffer::ReadUnsigned(buf,
                                    *streamId,
                                    BIT_SIZEOF_STREAMID,
                                    BIT_OFFSETOF_STREAMID);
        datBitBuffer::ReadUnsigned(buf,
                                    *streamSize,
                                    BIT_SIZEOF_STREAM_SIZE,
                                    BIT_OFFSETOF_STREAM_SIZE);

		//BE CAREFUL IF CHANGING THIS SYNTAX!!!
		if (SIG_STRM == sig)
			success = true;

		//Original syntax for the line above was:
		//
		//	success = (SIG_STRM == sig);
		//
		//This was changed in June '07 due to an apparent compiler bug in optimized Xenon builds
		//which resulted in success always being false. It may be possible to revert to the 
		//original syntax with future Xenon compilers.

        //Make sure that if it's a valid stream it has a valid stream id.
        netAssert(!success || NET_INVALID_STREAM_ID != *streamId);
    }

    return success;
}

unsigned
netStreamFragment::WriteHeader(void* buf,
                                const unsigned sizeofBuf,
                                const unsigned streamId,
                                const unsigned streamSize)
{
    netAssert(NET_INVALID_STREAM_ID != streamId);

    unsigned size = 0;
    if(netVerify(sizeofBuf > BYTE_SIZEOF_HEADER))
    {
        datBitBuffer::WriteUnsigned(buf,
                                    SIG_STRM,
                                    BIT_SIZEOF_SIG,
                                    BIT_OFFSETOF_SIG);
        datBitBuffer::WriteUnsigned(buf,
                                    streamId,
                                    BIT_SIZEOF_STREAMID,
                                    BIT_OFFSETOF_STREAMID);
        datBitBuffer::WriteUnsigned(buf,
                                    streamSize,
                                    BIT_SIZEOF_STREAM_SIZE,
                                    BIT_OFFSETOF_STREAM_SIZE);
        size = BYTE_SIZEOF_HEADER;
    }
    return size;
}

bool
netStreamFragment::IsFragment(const void* buf,
                            const unsigned sizeofBuf)
{
    unsigned id, streamLen;
    return netStreamFragment::ReadHeader(buf, sizeofBuf, &id, &streamLen);
}

const void*
netStreamFragment::GetPayload(const void* buf,
                            const unsigned sizeofBuf,
                            unsigned* sizeofPayload)
{
    const void* payload = 0;
    if(netStreamFragment::IsFragment(buf, sizeofBuf)
        && sizeofBuf > BYTE_SIZEOF_HEADER)
    {
        payload = ((const u8*)buf) + BYTE_SIZEOF_HEADER;
        *sizeofPayload = sizeofBuf - BYTE_SIZEOF_HEADER;
    }
    return payload;
}

///////////////////////////////////////////////////////////////////////////////
//  netStream
///////////////////////////////////////////////////////////////////////////////

netStream::netStream()
    : m_CxnMgr(0)
    , m_State(STATE_NONE)
    , m_SndPtr(0)
    , m_EndBuf(0)
    , m_SizeofBuf(0)
    , m_StreamId(NET_INVALID_STREAM_ID)
    , m_CxnId(-1)
{
    m_CxnDlgt.Bind(this, &netStream::OnCxnEvent);
}

netStream::~netStream()
{
    if(this->Pending())
    {
        this->SetFailed();
    }
}

bool
netStream::PrepareToSend(unsigned* streamId)
{
    bool success = false;

    if(netVerify(!this->Pending()))
    {
        m_State = STATE_PREPARED;
        *streamId = m_StreamId = netStreamFragment::NextId();
        success = true;
    }

    return success;
}

bool
netStream::Send(netConnectionManager* cxnMgr,
                const int cxnId,
                const void* buf,
                const unsigned sizeofBuf)
{
    netAssert(!m_EndBuf && !m_SndPtr && !m_SizeofBuf);
    netAssert(buf && sizeofBuf);
    netAssert(NET_INVALID_STREAM_ID != m_StreamId);

    bool success = false;

    //In the current implementation we avoid flooding the
    //connection buffers by waiting for the ACK for each
    //fragment sent before sending another fragment.

    if(netVerify(STATE_PREPARED == m_State))
    {
        //Retrieve the channel on which we'll listen for ACKs.
        const unsigned chId = cxnMgr->GetChannelId(cxnId);

        if(NET_INVALID_CHANNEL_ID != chId)
        {
            m_SndPtr = (const u8*) buf;
            m_EndBuf = m_SndPtr + sizeofBuf;
            m_SizeofBuf = sizeofBuf;
            m_CxnMgr = cxnMgr;
            m_CxnId = cxnId;
            this->SetSending();
            cxnMgr->AddChannelDelegate(&m_CxnDlgt, chId);
            if(this->SendFrag())
            {
                success = true;
            }
            else
            {
                this->SetFailed();
            }
        }
    }

    return success;
}

bool
netStream::Receive(netConnectionManager* cxnMgr,
                    const int cxnId,
                    const unsigned streamId,
                    void* buf,
                    const unsigned sizeofBuf)
{
    netAssert(!m_EndBuf && !m_RcvPtr && !m_SizeofBuf);
    netAssert(buf && sizeofBuf);

    bool success = false;

    if(netVerify(!this->Pending()))
    {
        //Retrieve the channel on which we'll listen for frames.
        const unsigned chId = cxnMgr->GetChannelId(cxnId);

        if(NET_INVALID_CHANNEL_ID != chId)
        {
            m_StreamId = streamId;
            m_RcvPtr = (u8*) buf;
            m_EndBuf = m_RcvPtr + sizeofBuf;
            m_SizeofBuf = sizeofBuf;
            m_CxnMgr = cxnMgr;
            m_CxnId = cxnId;
            this->SetReceiving();
            cxnMgr->AddChannelDelegate(&m_CxnDlgt, chId);

            success = true;
        }
    }

    return success;
}

bool
netStream::Sending() const
{
    return STATE_SENDING == m_State;
}

bool
netStream::Receiving() const
{
    return STATE_RECEIVING == m_State;
}

bool
netStream::Pending() const
{
    return this->Sending() || this->Receiving();
}

bool
netStream::Succeeded() const
{
    return STATE_SUCCEEDED == m_State;
}

bool
netStream::Failed() const
{
    return STATE_FAILED == m_State;
}

bool
netStream::Complete() const
{
    return this->Succeeded() || this->Failed();
}

//private:

void
netStream::SetSending()
{
    netAssert(!this->Pending());
    m_State = STATE_SENDING;
}

void
netStream::SetReceiving()
{
    netAssert(!this->Pending());
    m_State = STATE_RECEIVING;
}

void
netStream::SetSucceeded()
{
    netAssert(this->Pending());
    this->Abort();
    m_State = STATE_SUCCEEDED;
}

void
netStream::SetFailed()
{
    netAssert(this->Pending());
    this->Abort();
    m_State = STATE_FAILED;
}

void
netStream::Abort()
{
    m_CxnDlgt.Unregister();
    m_CxnMgr = 0;
    m_SndPtr = m_EndBuf = m_RcvPtr = 0;
    m_SizeofBuf = 0;
    m_StreamId = NET_INVALID_STREAM_ID;
    m_CxnId = -1;
}

bool
netStream::SendFrag()
{
    netAssert(this->Sending());
    netAssert(m_SndPtr && m_EndBuf);
    netAssert(m_CxnMgr);

    bool success = false;

    u8 frag[netStreamFragment::MAX_BYTE_SIZEOF_FRAGMENT];
    const unsigned sizeofRemaining = ptrdiff_t_to_int(m_EndBuf - m_SndPtr);
    unsigned sizeofFrag;

    if(sizeofRemaining > netStreamFragment::MAX_BYTE_SIZEOF_PAYLOAD)
    {
        sizeofFrag = netStreamFragment::MAX_BYTE_SIZEOF_PAYLOAD;
    }
    else
    {
        sizeofFrag = sizeofRemaining;
    }

    if(netVerify(netStreamFragment::BYTE_SIZEOF_HEADER ==
                    netStreamFragment::WriteHeader(frag,
                                                    sizeof(frag),
                                                    m_StreamId,
                                                    m_SizeofBuf)))
    {
        sysMemCpy(&frag[netStreamFragment::BYTE_SIZEOF_HEADER],
                    m_SndPtr,
                    sizeofFrag);

        if(m_CxnMgr->Send(m_CxnId,
                        frag,
                        sizeofFrag + netStreamFragment::BYTE_SIZEOF_HEADER,
                        NET_SEND_RELIABLE,
                        &m_Seq))
        {
            m_SndPtr += sizeofFrag;
            netAssert(m_SndPtr <= m_EndBuf);
            success = true;
        }
    }

    return success;
}

bool
netStream::RcvFrag(const void* frag,
                    const unsigned sizeofFrag)
{
    netAssert(this->Receiving());

    bool success = false;

    if(netVerify(sizeofFrag <= unsigned(m_EndBuf - m_RcvPtr)))
    {
        sysMemCpy(m_RcvPtr, frag, sizeofFrag);
        m_RcvPtr += sizeofFrag;
        netAssert(m_RcvPtr <= m_EndBuf);
        success = true;
    }

    return success;
}

void
netStream::OnCxnEvent(netConnectionManager* /*cxnMgr*/,
                    const netEvent* nevt)
{
    //We're only concerned about events occurring on the connection
    //on which we're streaming.

    if(nevt->m_CxnId == m_CxnId)
    {
        const unsigned eid = nevt->GetId();
        if(NET_EVENT_ACK_RECEIVED == eid)
        {
            if(this->Sending() && nevt->m_AckReceived->m_Ack == m_Seq)
            {
                //Received the ACK for the last fragment we sent.
                //Either we're finished or we need to send another fragment.

                if(m_SndPtr == m_EndBuf)
                {
                    this->SetSucceeded();
                }
                else if(!this->SendFrag())
                {
                    this->SetFailed();
                }
            }
        }
        else if(NET_EVENT_FRAME_RECEIVED == eid)
        {
            const netEventFrameReceived* fr = nevt->m_FrameReceived;
            unsigned streamId, streamLen;
            if(this->Receiving()
                && netStreamFragment::ReadHeader(fr->m_Payload,
                                                fr->m_SizeofPayload,
                                                &streamId,
                                                &streamLen)
                && m_StreamId == streamId)
            {
                //Each fragment contains the total length of the stream,
                //which we use to validate the fragment.

                if(netVerify(streamLen == m_SizeofBuf))
                {
                    unsigned sizeofPl = 0;
                    const void* pld =
                        netStreamFragment::GetPayload(fr->m_Payload,
                                                        fr->m_SizeofPayload,
                                                        &sizeofPl);

                    if(!this->RcvFrag(pld, sizeofPl))
                    {
                        this->SetFailed();
                    }
                    else if(m_RcvPtr == m_EndBuf)
                    {
                        this->SetSucceeded();
                    }
                }
                else
                {
                    this->SetFailed();
                }
            }
        }
        else if(NET_EVENT_CONNECTION_CLOSED == eid
                || NET_EVENT_CONNECTION_ERROR == eid)
        {
            this->SetFailed();
        }
    }
}

}   //namespace rage
