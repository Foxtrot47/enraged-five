// 
// net/tunneler.cpp 
// 
// Copyright (C) Rockstar Games.  All Rights Reserved. 
// 

#include "tunneler.h"
#include "diag/seh.h"
#include "keyexchange.h"
#include "netdiag.h"
#include "nethardware.h"
#include "netsocket.h"
#include "profile/rocky.h"
#include "tunneler_ice.h"
#include "rline/rlpresence.h"
#include "rline/rltelemetry.h"
#include "system/nelem.h"
#include "system/timemgr.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, tunneler)
#undef __rage_channel
#define __rage_channel ragenet_tunneler

NOSTRIP_FINAL_LOGGING_PARAM(netforceuserelay, "Force the packet relay to be used");
PARAM(netlogtelemetry, "Logs out telemetry content");
PARAM(nettestwrongrelayaddrfrompeeraddr, "");

bool netTunneler::sm_AllowNetTunnelerTelemetry = true;
bool netTunneler::sm_AllowKeyExchangeRetry = true;

static const unsigned ACTIVE_ENDPOINT_BYPASS_NAT_TRAVERSAL_TIMEOUT_MS = (15 * 1000);

/////////////////////////////////////////////////////////////////////////////////////
//  netTunnelRelayCache
//	Caches relay addresses to reduce the number of queries to the presence server.
//  Also, if we have a relay address cached, we bypass NAT traversal and go straight
//  to relay. Don't cache unless we previously fell back to relay for the same peer.
/////////////////////////////////////////////////////////////////////////////////////
class netTunnelRelayCache
{
public:
	netTunnelRelayCache()
	{
		Clear();
	}

	void Init()
	{
		Clear();
	}

	void Shutdown()
	{
		Clear();
	}

	void Clear()
	{
		for(int i = 0; i < RELAY_ADDR_CACHE_LEN; ++i)
		{
			m_RelayAddressCache[i].Clear();
		}
	}
	
	void AddEntry(const netPeerId& peerId, const netAddress& relayAddr)
	{
		if(netVerify(relayAddr.IsRelayServerAddr()))
		{
			// update entry if it exists
			RelayCacheEntry* entry = NULL;
			for(int i = 0; i < RELAY_ADDR_CACHE_LEN; ++i)
			{
				if(m_RelayAddressCache[i].m_PeerId == peerId)
				{
					entry = &m_RelayAddressCache[i];
					break;
				}
			}

			if(!entry)
			{
				// search for an unused slot
				for(int i = 0; i < RELAY_ADDR_CACHE_LEN; ++i)
				{
					if(!m_RelayAddressCache[i].m_Addr.IsRelayServerAddr())
					{
						entry = &m_RelayAddressCache[i];
						break;
					}
				}
			}

			if(entry)
			{
				entry->m_PeerId = peerId;
				entry->m_Addr = relayAddr;
				entry->m_Expiry = sysTimer::GetSystemMsTime() + (RELAY_CACHE_ENTRY_TTL_SEC * 1000);
			}
		}
	}

	bool RemoveEntry(const netAddress& addr)
	{
		bool foundAddress = false;

		// clear all matching entries
		for(int i = 0; i < RELAY_ADDR_CACHE_LEN; ++i)
		{
			if(m_RelayAddressCache[i].m_Addr.IsRelayServerAddr()
				&& m_RelayAddressCache[i].m_Addr == addr)
			{
				netDebug3("Removing cache entry %d: " NET_ADDR_FMT,
					i,
					NET_ADDR_FOR_PRINTF(m_RelayAddressCache[i].m_Addr));
				m_RelayAddressCache[i].Clear();
				foundAddress = true;
			}
		}

		return foundAddress;
	}

	void Update()
	{
		// clear all expired entries
		const unsigned curTime = sysTimer::GetSystemMsTime();
		for(int i = 0; i < RELAY_ADDR_CACHE_LEN; ++i)
		{
			if(m_RelayAddressCache[i].m_Addr.IsRelayServerAddr()
				&& int(curTime - m_RelayAddressCache[i].m_Expiry) >= 0)
			{
				netDebug3("Expired cache entry %d: " NET_ADDR_FMT,
					i,
					NET_ADDR_FOR_PRINTF(m_RelayAddressCache[i].m_Addr));
				m_RelayAddressCache[i].Clear();
			}
		}
	}

	bool GetCachedRelayAddress(const netPeerId& peerId, netAddress* relayAddr) const
	{
		for(int i = 0; i < RELAY_ADDR_CACHE_LEN; ++i)
		{
			if(m_RelayAddressCache[i].m_PeerId == peerId
				&& m_RelayAddressCache[i].m_Addr.IsRelayServerAddr())
			{
				*relayAddr = m_RelayAddressCache[i].m_Addr;
				return true;
			}
		}

		return false;
	}

private:
	static const unsigned RELAY_ADDR_CACHE_LEN      = 64;
	static const unsigned RELAY_CACHE_ENTRY_TTL_SEC = 60;

	class RelayCacheEntry
	{
	public:

		RelayCacheEntry()
		{
			Clear();
		}

		void Clear()
		{
			m_PeerId.Clear();
			m_Addr.Clear();
			m_Expiry = 0;
		}

		netPeerId m_PeerId;
		netAddress m_Addr;
		unsigned m_Expiry;
	};

	RelayCacheEntry m_RelayAddressCache[RELAY_ADDR_CACHE_LEN];
};

static netTunnelRelayCache s_RelayAddressCache;

///////////////////////////////////////////////////////////////////////////////
//  netTunnelRequest
///////////////////////////////////////////////////////////////////////////////
static volatile unsigned s_NextTunnelId = 0;

netTunnelRequest::netTunnelRequest()
    : m_Id(0)
    , m_Status(NULL)
    , m_Tunneler(NULL)
    , m_State(STATE_NONE)
{
    this->Clear();
}

netTunnelRequest::~netTunnelRequest()
{
	netAssert((m_Status == NULL) || (m_Status->Pending() == false));

    this->Clear();
}

bool
netTunnelRequest::Pending() const
{
    return (STATE_NONE != m_State)
			&& (STATE_SUCCEEDED != m_State)
			&& (STATE_FAILED != m_State);
}

bool
netTunnelRequest::Succeeded() const
{
    return (STATE_SUCCEEDED == m_State);
}

void
netTunnelRequest::Cancel()
{
    if(this->Pending() && netVerify(m_Tunneler))
    {
        m_Tunneler->CancelRequest(this);
    }
}

const EndpointId
netTunnelRequest::GetEndpointId() const
{
	return m_EndpointId;
}

void netTunnelRequest::SetEndpointId(const EndpointId endpointId)
{
	netAssert(NET_IS_VALID_ENDPOINT_ID(endpointId));

	m_EndpointId = endpointId;
}

const netAddress&
netTunnelRequest::GetNetAddress() const
{
    netAssert(this->Succeeded());

    return m_NetAddr;
}

void 
netTunnelRequest::OverrideNetAddress(const netAddress& netAddr, const netAddress& relayAddr)
{
	if(netVerify(Succeeded()))
	{
		netDebug("[%u]: OverrideNetAddress - Assigning [" NET_ADDR_FMT "]", m_Id, NET_ADDR_FOR_PRINTF(netAddr));

		netKeyExchange::UpdateP2pKey(this->m_PeerAddr.GetPeerId(), netAddr, relayAddr);
		m_NetAddr = netAddr;
		m_RelayAddr = relayAddr;
	}
}

void 
netTunnelRequest::SetQuickConnectDirectAddr(const netAddress& netAddr)
{
	netDebug("[%u]: StoreQuickConnectDirectAddr - Setting to " NET_ADDR_FMT "", m_Id, NET_ADDR_FOR_PRINTF(netAddr));
	m_QuickConnectDirectAddr = netAddr;
}

const netAddress
netTunnelRequest::GetRelayAddress() const
{
	// only return plaintext address - not tokenized
	netAddress relayAddr(m_RelayAddr.GetProxyAddress(), netRelayToken::INVALID_RELAY_TOKEN, m_RelayAddr.GetTargetAddress());

	if(!relayAddr.IsRelayServerAddr())
	{
		if(m_NetAddr.IsRelayServerAddr() &&
			m_NetAddr.GetTargetAddress().IsValid())
		{
			relayAddr = m_NetAddr;
		}
		else if(m_PeerAddr.GetRelayAddress().IsRelayServerAddr() &&
			m_PeerAddr.GetRelayAddress().GetTargetAddress().IsValid())
		{
			relayAddr = m_PeerAddr.GetRelayAddress();
		}
	}

	if(!relayAddr.IsRelayServerAddr())
	{
		relayAddr.Clear();
	}

	return relayAddr;
}

const netAddress
netTunnelRequest::GetRelayAddressInternal() const
{
	// this is allowed to return a tokenized address for use during tunneling
	if(!m_RelayAddr.IsRelayServerAddr())
	{
		if(m_NetAddr.IsRelayServerAddr())
		{
			return m_NetAddr;
		}
		else if(m_PeerAddr.GetRelayAddress().IsRelayServerAddr())
		{
			return m_PeerAddr.GetRelayAddress();
		}
	}

	return m_RelayAddr;
}

const netPeerAddress&
netTunnelRequest::GetPeerAddress() const
{
    return m_PeerAddr;
}

unsigned
netTunnelRequest::GetId() const
{
    return m_Id;
}

bool 
netTunnelRequest::IsExchangingKeys() const
{
	return (m_State == STATE_EXCHANGE_KEYS) || (m_State == STATE_EXCHANGING_KEYS);
}

//private:

void
netTunnelRequest::Clear()
{
    netAssert(!m_Status);
    netAssert(!this->Pending());
    netAssert(!m_Tunneler);

    if(this->Pending())
    {
        this->Cancel();
    }

	if(m_RelayConnectionStatus.Pending())
	{
		netRelay::CancelConnect(&m_RelayConnectionStatus);
	}

	if(m_RelayDiscoveryStatus.Pending())
	{
		rlPresence::CancelQuery(&m_RelayDiscoveryStatus);
	}

	if(m_KeyExchangeStatus.Pending())
	{
		netKeyExchange::CancelKeyExchange(&m_KeyExchangeStatus);
	}

	m_Id = 0;
	m_State = STATE_NONE;
	m_Status = NULL;

	m_EndpointId = NET_INVALID_ENDPOINT_ID;
	m_PeerAddr.Clear();
	m_QuickConnectDirectAddr.Clear();
	m_NetAddr.Clear();
	m_RelayAddr.Clear();
	m_PrevRelayAddress.Clear();
	m_TunnelDesc.Clear();
	m_PresenceQueryState = STATE_PQ_UNATTEMPTED;
	m_ForcePresenceQuery = false;

	m_Tunneler = NULL;
	
	m_DirectTunnelingStatus.Reset();
	m_RelayDiscoveryStatus.Reset();
	m_RelayConnectionStatus.Reset();
	m_KeyExchangeStatus.Reset();

	m_DirectResultCode = 0;
	m_KeyExchangeResultCode = 0;
	m_TimeStarted = 0;
	m_TimeStateStarted = 0;
}

void
netTunnelRequest::Reset(const netPeerAddress& peerAddr,
						const netTunnelDesc& tunnelDesc,
						netStatus* status)
{
    netAssert(status);

    this->Clear();

    m_Id = sysInterlockedIncrement(&s_NextTunnelId);
    m_PeerAddr = peerAddr;
	m_TunnelDesc = tunnelDesc;
    m_Status = status;
    m_Status->SetPending();
}

bool
netTunnelRequest::DiscoverRelayAddress()
{
	netAssert(!m_RelayDiscoveryStatus.Pending());
	m_RelayDiscoveryStatus.Reset();

	if(m_ForcePresenceQuery)
	{
		netDebug("[%u]: Forcing presence query", GetId());
		m_RelayAddr.Clear();
		
		if(QueryRelayAddress())
		{
			return true;
		}
	}
	else if(m_RelayAddr.IsRelayServerAddr())
	{
		netDebug("[%u]: The relay address is already valid for the tunnel", GetId());
		m_RelayDiscoveryStatus.ForceSucceeded();
		return true;
	}
	else if(s_RelayAddressCache.GetCachedRelayAddress(m_PeerAddr.GetPeerId(), &m_RelayAddr))
	{
		netDebug("[%u]: Using cached relay address:" NET_ADDR_FMT, GetId(), NET_ADDR_FOR_PRINTF(m_RelayAddr));
		m_RelayDiscoveryStatus.ForceSucceeded();
		return true;
	}
	else if(m_PeerAddr.GetRelayAddress().IsRelayServerAddr())
	{
		m_RelayAddr = m_PeerAddr.GetRelayAddress();
		netDebug("[%u]: Using relay address from peer address:" NET_ADDR_FMT, GetId(), NET_ADDR_FOR_PRINTF(m_RelayAddr));

#if !__NO_OUTPUT
		if(PARAM_nettestwrongrelayaddrfrompeeraddr.Get())
		{
			// test case where the peer address doesn't have the remote peer's up-to-date relay address
			netSocketAddress proxyAddr = m_RelayAddr.GetProxyAddress();
			netSocketAddress targetAddr = m_RelayAddr.GetTargetAddress();
			u8 tokenBuf[netRelayToken::MAX_TOKEN_BUF_SIZE] = {1};
			netRelayToken relayToken(tokenBuf);
			targetAddr.Init(targetAddr.GetIp(), targetAddr.GetPort() + 1);
			m_RelayAddr.Init(proxyAddr, relayToken, targetAddr);

			netDebug("[%u]: -nettestwrongrelayaddrfrompeeraddr is enabled. Using incorrect relay address from peer address:" NET_ADDR_FMT, GetId(), NET_ADDR_FOR_PRINTF(m_RelayAddr));
		}
#endif

		m_RelayDiscoveryStatus.ForceSucceeded();
		return true;
	}
	else if(QueryRelayAddress())
	{
		return true;
	}

	if(m_RelayDiscoveryStatus.Pending())
	{
		m_RelayDiscoveryStatus.SetFailed(RELAY_ERROR_DISCOVERY_FAILED);
	}
	else
	{
		m_RelayDiscoveryStatus.ForceFailed(RELAY_ERROR_DISCOVERY_FAILED);
	}

	return false;
}

bool
netTunnelRequest::QueryRelayAddress()
{
    netAssert(!m_RelayAddr.IsRelayServerAddr());
	netAssert(m_PresenceQueryState == STATE_PQ_UNATTEMPTED);
    //Resorting to querying a relay address means we've
    //failed to tunnel by other means.  This implies we
    //should already have a valid status object.
    netAssert(m_Status);

    m_RelayAddr.Clear();

    netDebug("[%u]: Querying relay address from presence...", GetId());

	rtry
	{
		rcheck(m_PeerAddr.GetGamerHandle().IsValid(), catchall, );
		rcheck(m_PeerAddr.GetGamerHandle().IsNativeGamerHandle(), noerr, netDebug2("Skipping presence lookup for non-native gamer."));

		CompileTimeAssert(COUNTOF(m_PresenceAttrs) == ATTR_MAX_PRESENCE_ATTRIBUTES);
		m_PresenceAttrs[ATTR_INDEX_PEER_ADDRESS].Reset(rlScAttributeId::PeerAddress.Name, "");

		rverify(rlPresence::GetAttributesForGamer(rlPresence::GetActingUserIndex(),
													m_PeerAddr.GetGamerHandle(),
													m_PresenceAttrs,
													COUNTOF(m_PresenceAttrs),
													&m_RelayDiscoveryStatus),
				catchall, );

		m_PresenceQueryState = STATE_PQ_PENDING;
		return true;
	}
	rcatch(noerr)
	{
		m_PresenceQueryState = STATE_PQ_FAILED_TO_INITIATE;
		return false;
	}
	rcatchall
	{
		rlError("[%u] Failed to query relay address from presence", GetId());
		m_PresenceQueryState = STATE_PQ_FAILED_TO_INITIATE;
		return false;
	}
}

void
netTunnelRequest::UpdateRelayAddressDiscovery()
{
	netAssert(m_State == STATE_DISCOVERING_RELAY_ADDRESS);

	if(m_RelayAddr.IsValid())
	{
		// we may have retrieved the relay address from the cache
		netAssert(m_RelayDiscoveryStatus.Succeeded());
		return;
	}

	if(m_RelayDiscoveryStatus.Succeeded())
	{
		m_PresenceQueryState = STATE_PQ_SUCCEEDED;

		netPeerAddress peerAddr;

		if(m_PresenceAttrs[ATTR_INDEX_PEER_ADDRESS].Type == RLSC_PRESTYPE_STRING)
		{
			char szPeerAddr[netPeerAddress::TO_STRING_BUFFER_SIZE] = {0};
			if(m_PresenceAttrs[ATTR_INDEX_PEER_ADDRESS].GetValue(szPeerAddr))
			{
				if(!peerAddr.FromString(szPeerAddr))
				{
					peerAddr.Clear();
				}
			}
		}

		netDebug("[%u]: Presence returned netPeerAddress: %s", GetId(), peerAddr.ToString());

		// the relay address from the presence attribute will only contain the relay token, not the real ip/port
		const netAddress& relayAddress = peerAddr.GetRelayAddress();

		if(relayAddress.IsRelayServerAddr())
		{
			if((m_PeerAddr.GetPeerId().IsValid()) &&
				(peerAddr.GetPeerId() != m_PeerAddr.GetPeerId()) &&
				m_PeerAddr.GetRelayAddress().IsRelayServerAddr())
			{
				// out-of-date presence attribute - we'll use the relay address from the supplied peer address
				netDebug("[%u]: Presence returned out-of-date peer address (peerIds do not match): %s, compared to supplied peer address: %s", GetId(), peerAddr.ToString(), m_PeerAddr.ToString());
				m_RelayDiscoveryStatus.Reset();
				m_RelayDiscoveryStatus.ForceFailed(RELAY_ERROR_OUT_OF_DATE_PRES_ATTR);
				m_PresenceQueryState = STATE_PQ_OUT_OF_DATE;
			}
			else
			{
				m_PresenceQueryState = STATE_PQ_SUCCEEDED;

				m_RelayAddr = relayAddress;

				netDebug("[%u]: Presence returned relay address:" NET_ADDR_FMT, GetId(), NET_ADDR_FOR_PRINTF(m_RelayAddr));

				// we add the relay address to the cache when we get a valid relay address from presence.
				// The purpose of the cache is to reduce the number of queries to the presence server.
				// Even if the remote peer isn't reachable from this relay address, we avoid looking it
				// up again next time since we'll likely get the same result.
				s_RelayAddressCache.AddEntry(m_PeerAddr.GetPeerId(), m_RelayAddr);
			}
		}
		else
		{
			netError("[%u]: Presence returned an invalid relay address", GetId());
			m_PresenceQueryState = peerAddr.GetPeerId().IsValid() ? STATE_PQ_NO_RELAY_ADDR : STATE_PQ_NO_PEER_ADDR;
			int failReason = RELAY_ERROR_INVALID_FIELD;
			if(!relayAddress.GetProxyAddress().GetIp().IsValid())
			{
				failReason = RELAY_ERROR_NO_RELAY_IP_FIELD;
			}
			else if(!relayAddress.GetProxyAddress().IsValid())
			{
				failReason = RELAY_ERROR_NO_RELAY_PORT_FIELD;
			}
			else if(!relayAddress.GetRelayToken().IsValid())
			{
				failReason = RELAY_ERROR_NO_MAPPED_IP_FIELD;
			}

			m_RelayDiscoveryStatus.Reset();
			m_RelayDiscoveryStatus.ForceFailed(failReason);
		}
	}
	else if(!m_RelayDiscoveryStatus.Pending())
	{
		netError("[%u]: Failed to retrieve relay address from presence", GetId());
		m_RelayDiscoveryStatus.Reset();
		m_RelayDiscoveryStatus.ForceFailed(RELAY_ERROR_NO_ADDRESS_RECORDS);

		if(m_RelayDiscoveryStatus.Canceled())
		{
			m_PresenceQueryState = STATE_PQ_CANCELED;
		}
		else
		{
			m_PresenceQueryState = STATE_PQ_FAILED;
		}
	}

	if(m_RelayDiscoveryStatus.Failed() && m_PeerAddr.GetRelayAddress().IsRelayServerAddr())
	{
		m_RelayAddr = m_PeerAddr.GetRelayAddress();
		netDebug("[%u]: Using relay address from peer address:" NET_ADDR_FMT, GetId(), NET_ADDR_FOR_PRINTF(m_RelayAddr));
		m_RelayDiscoveryStatus.Reset();
		m_RelayDiscoveryStatus.ForceSucceeded();
	}
}

bool
netTunnelRequest::ConnectToRelay()
{
	return netRelay::Connect(m_RelayAddr.GetProxyAddress(), netRelay::IsUsingSecureRelays(), &m_RelayConnectionStatus);
}

void
netTunnelRequest::UpdateConnectingToRelay()
{
	netAssert(m_State == STATE_CONNECTING_TO_RELAY);

	if(m_RelayConnectionStatus.Succeeded())
	{
		netDebug("[%u]: Connected to relay at: " NET_ADDR_FMT, GetId(), NET_ADDR_FOR_PRINTF(m_RelayAddr));
		m_NetAddr = m_RelayAddr;
	}
	else if(!m_RelayConnectionStatus.Pending())
	{
		netDebug("[%u]: Failed to connect to relay at: " NET_ADDR_FMT, GetId(), NET_ADDR_FOR_PRINTF(m_RelayAddr));
		m_RelayAddr.Clear();
		m_RelayConnectionStatus.Reset();
		m_RelayConnectionStatus.ForceFailed(RELAY_ERROR_CONNECTION_FAILED);
	}
}

bool
netTunnelRequest::ExchangeKeys()
{
	// Note: even if we already have a key for the remote peer, we still need to do a key exchange with them in case they've closed their end of the tunnel and don't have a key.

	rtry
	{
		rverify(!m_KeyExchangeStatus.Pending(), catchall, netError("[%u]: Key exchange is already in progress", this->GetId()));

		// m_NetAddr must be valid by this point - it gets set after NAT traversal succeeds, or when we've connected to a relay.
		rverify(m_NetAddr.IsValid(), catchall, netError("[%u]: Cannot exchange keys because we don't have a valid address", this->GetId()));

		rverify(netKeyExchange::ExchangeKeys(GetPeerAddress(), m_NetAddr, GetRelayAddress(), !m_TunnelDesc.m_Bilateral, m_Id, &m_KeyExchangeStatus), catchall, netError("[%u]: ExchangedKeys() failed", this->GetId()));

		return true;
	}
	rcatchall
	{
		if(m_KeyExchangeStatus.Pending())
		{
			m_KeyExchangeStatus.SetFailed(KX_ERROR_FAILED_TO_INITIATE);
		}
		else
		{
			m_KeyExchangeStatus.ForceFailed(KX_ERROR_FAILED_TO_INITIATE);
		}

		return false;
	}
}

void
netTunnelRequest::UpdateKeyExchange()
{
	rtry
	{
		rverify(m_State == STATE_EXCHANGING_KEYS, catchall, );
	}
	rcatchall
	{
		if(m_KeyExchangeStatus.Pending())
		{
			m_KeyExchangeStatus.SetFailed(KX_ERROR_FAILED_TO_INITIATE);
		}
		else
		{
			m_KeyExchangeStatus.ForceFailed(KX_ERROR_FAILED_TO_INITIATE);
		}
	}

	if(m_KeyExchangeStatus.Succeeded())
	{
		netDebug("[%u]: Key exchange succeeded", GetId());
	}
	else if(!m_KeyExchangeStatus.Pending())
	{
		m_KeyExchangeResultCode = m_KeyExchangeStatus.GetResultCode();
		netError("[%u]: Key exchange failed. Result Code: %d", GetId(), m_KeyExchangeStatus.GetResultCode());
		m_RelayAddr.Clear();

		m_KeyExchangeStatus.Reset();
		m_KeyExchangeStatus.ForceFailed(m_NetAddr.IsDirectAddr() ? KX_ERROR_FAILED_DIRECT : KX_ERROR_FAILED_RELAY);
	}
}

void
netTunnelRequest::SendTelemetry(CompletionType completionType, const int resultCode)
{
	if(netTunneler::sm_AllowNetTunnelerTelemetry == false)
	{
		return;
	}

	// convert remote IP
	char lpub[netSocketAddress::MAX_STRING_BUF_SIZE] = { 0 };
	char lrly[netSocketAddress::MAX_STRING_BUF_SIZE] = { 0 };
	char rpub[netSocketAddress::MAX_STRING_BUF_SIZE] = { 0 };
	char rrly[netSocketAddress::MAX_STRING_BUF_SIZE] = { 0 };

	netPeerAddress localPeerAddr;
	if(netPeerAddress::GetLocalPeerAddress(rlPresence::GetActingUserIndex(), &localPeerAddr))
	{
		localPeerAddr.GetPublicAddress().Format(lpub, true);
		localPeerAddr.GetRelayAddress().GetProxyAddress().Format(lrly, true);
	}

	m_NetAddr.GetTargetAddress().FormatAttemptIpV4(rpub, netSocketAddress::MAX_STRING_BUF_SIZE, false);

	if(m_RelayAddr.IsRelayServerAddr())
	{
		m_RelayAddr.GetProxyAddress().FormatAttemptIpV4(rrly, netSocketAddress::MAX_STRING_BUF_SIZE, false);
	}
	else if(m_PeerAddr.GetRelayAddress().IsRelayServerAddr())
	{
		m_PeerAddr.GetRelayAddress().GetProxyAddress().FormatAttemptIpV4(rrly, netSocketAddress::MAX_STRING_BUF_SIZE, false);
	}

	// telemetry
	rlStandardMetrics::TunnelerResult metric(static_cast<int>(completionType),
												m_NetAddr.IsRelayServerAddr(),
												(m_DirectResultCode != 0),
												true,
												m_KeyExchangeResultCode,
												m_DirectResultCode, 
												resultCode,
												m_TunnelDesc.m_TunnelReason, 
												m_TunnelDesc.m_UniqueId,
												sysTimer::GetSystemMsTime() - m_TimeStarted,
												rpub,
												rrly,
												lpub,
												lrly,
												static_cast<int>(m_PresenceQueryState));

	rlTelemetry::Write(rlPresence::GetActingUserIndex(), metric);

#if !__NO_OUTPUT
	if(PARAM_netlogtelemetry.Get())
	{
		char metricBuf[2048] = {0};
		RsonWriter rw(metricBuf, RSON_FORMAT_JSON);
		metric.Write(&rw);
		netDebug3("[%u] Writing telemetry: %s", GetId(), rw.ToString());
	}
#endif
}

void
netTunnelRequest::Complete(CompletionType completionType, const int resultCode)
{
	// Case 1: if we succeeded with a relay address, but NAT traversal has found a direct address
	// (while performing key exchange via the relay for example), swap to direct address.
	// Case 2: during key exchange, it's possible that another NAT traversal session was
	// taking place (for example, if one side skipped NAT traversal but the other side
	// didn't) and the latest NAT traversal session selected a different address than
	// the the address over which key exchanged occurred. To resync, succeed the tunnel
	// request with the latest negotiated address.
	if((completionType == COMPLETE_SUCCEEDED) &&
		m_QuickConnectDirectAddr.IsValid() &&
		(m_QuickConnectDirectAddr != m_NetAddr))
	{
		netDebug("[%u]: Swapping to QuickConnect direct address [" NET_ADDR_FMT "]", this->GetId(), NET_ADDR_FOR_PRINTF(m_QuickConnectDirectAddr));

		if(m_NetAddr.IsRelayServerAddr())
		{
			m_Tunneler->RemoveCachedRelayAddress(m_NetAddr);
		}

		m_Tunneler->UpdateP2pKey(m_PeerAddr.GetPeerId(), m_QuickConnectDirectAddr, GetRelayAddress());
		m_NetAddr = m_QuickConnectDirectAddr;
		netAssert(m_NetAddr.IsRelayServerAddr() == false);
	}

#if !__NO_OUTPUT
    switch(completionType)
    {
    case COMPLETE_SUCCEEDED:
        netAssert(m_NetAddr.IsValid());
        netDebug("[%u]: Tunnel request SUCCEEDED with address [" NET_ADDR_FMT "]", this->GetId(), NET_ADDR_FOR_PRINTF(m_NetAddr));
        break;
    case COMPLETE_FAILED:
		if (resultCode == NET_TUNNELER_CANCELLED)
		{
			netWarning("[%u]: Tunnel request CANCELLED with code: 0x%08x", this->GetId(), resultCode);
		}
		else
		{
			netError("[%u]: Tunnel request FAILED with code: 0x%08x", this->GetId(), resultCode);
		}
        
        break;
    }
#endif

	if(netVerify(m_Tunneler))
	{
		m_Tunneler->RemoveRequest(this);
	}

	if(COMPLETE_SUCCEEDED == completionType)
	{
		// if we didn't explicitly vet the relay address, use the relay address stored in the peer address
		if(!m_RelayAddr.IsRelayServerAddr() && m_PeerAddr.GetRelayAddress().IsRelayServerAddr())
		{
			m_RelayAddr = m_PeerAddr.GetRelayAddress();
		}
	}

	// if we're still holding the remote peer's tokenized relay address, clear it.
	// we don't create endpoints with tokenized addresses.
	if(m_RelayAddr.IsRelayServerAddr() && m_RelayAddr.GetRelayToken().IsValid())
	{
		m_RelayAddr.Clear();
	}

	if(m_Status)
	{
		if(COMPLETE_SUCCEEDED == completionType)
		{
			netAssert(m_NetAddr.IsValid());
			netAssert(!m_NetAddr.IsRelayServerAddr() || !m_NetAddr.GetRelayToken().IsValid());
			m_Status->SetSucceeded();
		}
		else
		{
			m_Status->SetFailed(resultCode);
		}

		m_Status = NULL;
	}

	SendTelemetry(completionType, resultCode);
}

#if !__NO_OUTPUT
const char*
netTunnelRequest::GetStateName(State state)
{
#define TUNNEL_STATE_CASE(x) case x: return #x; break;
	switch(state)
	{
		TUNNEL_STATE_CASE(STATE_NONE);
		TUNNEL_STATE_CASE(STATE_TUNNEL_DIRECT);
		TUNNEL_STATE_CASE(STATE_TUNNELING_DIRECT);
		TUNNEL_STATE_CASE(STATE_DISCOVER_RELAY_ADDRESS);
		TUNNEL_STATE_CASE(STATE_DISCOVERING_RELAY_ADDRESS);
		TUNNEL_STATE_CASE(STATE_CONNECT_TO_RELAY);
		TUNNEL_STATE_CASE(STATE_CONNECTING_TO_RELAY);
		TUNNEL_STATE_CASE(STATE_EXCHANGE_KEYS);
		TUNNEL_STATE_CASE(STATE_EXCHANGING_KEYS);
		TUNNEL_STATE_CASE(STATE_SUCCEEDED);
		TUNNEL_STATE_CASE(STATE_FAILED);
	}
#undef TUNNEL_STATE_CASE

	netAssertf(false, "netTunnelRequest::GetStateName :: UNKNOWN STATE");
	return "UNKNOWN STATE";
}
#endif

void netTunnelRequest::SetState(State state, const int resultCode /*= NET_TUNNELER_NO_ERROR*/)
{
	if(state != m_State)
	{
		unsigned curTime = sysTimer::GetSystemMsTime();

		netDebug("[%u]: SetState :: %s -> %s, Code: 0x%08x, Time: %u ms",
					GetId(),
					GetStateName(m_State),
					GetStateName(state),
					resultCode,
					(m_State == STATE_NONE) ? 0 : (curTime - m_TimeStateStarted));

		// mark when we entered this state
		m_TimeStateStarted = curTime;

		// assign state
		m_State = state; 
	}

	if(m_State == STATE_SUCCEEDED)
	{
		Complete(netTunnelRequest::COMPLETE_SUCCEEDED, resultCode);
	}
	else if(m_State == STATE_FAILED)
	{
		Complete(netTunnelRequest::COMPLETE_FAILED, resultCode);
	}
}

///////////////////////////////////////////////////////////////////////////////
//  netTunneler
///////////////////////////////////////////////////////////////////////////////

netTunneler::netTunneler()
: m_CxnMgr(NULL)
, m_Initialized(false)
{

}

netTunneler::~netTunneler()
{
    this->Shutdown();
}

bool
netTunneler::Init(netConnectionManager* cxnMgr)
{
	rtry
	{
		rverify(!m_Initialized, catchall, );
		rverify(cxnMgr, catchall, );

		netAssert(m_Requests.empty());

		m_CxnMgr = cxnMgr;

		s_RelayAddressCache.Init();

		rverify(netKeyExchange::Init(cxnMgr), catchall, );

		rverify(this->NativeInit(cxnMgr), catchall, );

		m_Initialized = true;
			
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void
netTunneler::Shutdown()
{
    SYS_CS_SYNC(m_Cs);

    if(m_Initialized)
    {
        this->CancelAllRequests();

		s_RelayAddressCache.Shutdown();

		netKeyExchange::Shutdown();

        this->NativeShutdown();

		m_CxnMgr = NULL;

        m_Initialized = false;
    }
}

void
netTunneler::SelectStartState(netTunnelRequest* rqst)
{
	unsigned lastReceiveTimeMs = 0;

	netAddress netAddr;
	netAddress relayAddr;

	if(PARAM_netforceuserelay.Get())
	{
		netDebug("[%u]: -netforceuserelay is on - using relay addresses only", rqst->GetId());
		rqst->SetState(netTunnelRequest::STATE_DISCOVER_RELAY_ADDRESS);
    }
    else if(m_CxnMgr->GetAddrByPeerId(rqst->GetPeerAddress().GetPeerId(), &netAddr, &relayAddr, &lastReceiveTimeMs)
			&& ((lastReceiveTimeMs <= ACTIVE_ENDPOINT_BYPASS_NAT_TRAVERSAL_TIMEOUT_MS) || netAddr.IsRelayServerAddr())
			&& !NativeHasActiveSession(rqst->GetPeerAddress()))
    {
		rqst->m_NetAddr = netAddr;
		rqst->m_RelayAddr = relayAddr;

		// We skip NAT traversal if we already have an active endpoint to this peer and we've received a packet
		// from that peer in the last few seconds (typically before the NAT would close the port on the remote end).
		// For example, when we have an existing connection created from a different session, avoid NAT traversing again
		// since this (when it fails) can cause long delays and trigger timeouts when joining sessions.
		netDebug("[%u]: We already have an active endpoint with peeraddr %s via " NET_ADDR_FMT ". Skipping NAT traversal.",
				rqst->GetId(), rqst->GetPeerAddress().ToString(), NET_ADDR_FOR_PRINTF(rqst->m_NetAddr));
		
		// we still need to connect to relay (in case we've disconnected from it recently), and go through key exchange,
		// in case the remote peer has recently closed their end of the tunnel and freed their key.
		if(rqst->m_NetAddr.IsRelayServerAddr())
		{
			// connect to relay (eg. if we're using secure relays). We do a key exchange after connecting to relay.
			rqst->SetState(netTunnelRequest::STATE_CONNECT_TO_RELAY);
		}
		else if(rqst->m_NetAddr.IsPeerRelayAddr())
		{
			// we are connected to the remote peer via peer relay. Exchange keys via relay server. 
			// Exchanging keys via peer relay is not currently supported.
			netDebug("[%u]: Existing route is via peer relay. Exchanging keys via relay server.", rqst->GetId());
			rqst->m_NetAddr.Clear();
			rqst->SetState(netTunnelRequest::STATE_DISCOVER_RELAY_ADDRESS);
		}
		else
		{
			netAssert(rqst->m_NetAddr.IsDirectAddr());
			rqst->SetState(netTunnelRequest::STATE_EXCHANGE_KEYS);
		}
	}
	else
	{
		rqst->SetState(netTunnelRequest::STATE_TUNNEL_DIRECT);
    }

	netAssert(rqst->Pending());
}

bool
netTunneler::OpenTunnel(const netPeerAddress& peerAddr,
						const netTunnelDesc& tunnelDesc,
						netTunnelRequest* rqst,
						netStatus* status)
{
    SYS_CS_SYNC(m_Cs);

	rtry
	{
		rverify(m_Initialized, catchall, );
		rverify(peerAddr.IsValid(), catchall, );
		rverify((tunnelDesc.m_TunnelType == NET_TUNNELTYPE_LAN) || (tunnelDesc.m_TunnelType == NET_TUNNELTYPE_ONLINE), catchall, );
		rverify(rqst, catchall, );
		rverify(!rqst->Pending(), catchall, );
		rverify(status, catchall, netError("Invalid status object"));

		rverify(peerAddr.GetGamerHandle().IsSupportedGamerHandle(), catchall, netError("Unsupported gamerhandle."));

		rqst->Reset(peerAddr, tunnelDesc, status);
		this->AddRequest(rqst);

		netDebug("[%u]: Opening %s tunnel",
				rqst->GetId(),
				(NET_TUNNELTYPE_LAN == tunnelDesc.m_TunnelType) ? "LAN" : "ONLINE");

		netDebug("[%u]: Remote Peer Address: %s", rqst->GetId(), peerAddr.ToString());

		this->SelectStartState(rqst);

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netTunneler::CloseTunnel(const netAddress& addr)
{
    SYS_CS_SYNC(m_Cs);

    netDebug("Closing tunnel to:" NET_ADDR_FMT "...", NET_ADDR_FOR_PRINTF(addr));

	netVerify(netKeyExchange::FreeKey(addr));

    return this->NativeCloseTunnel(addr);
}

void
netTunneler::CancelRequest(netTunnelRequest* rqst)
{
    SYS_CS_SYNC(m_Cs);

	this->NativeCancelRequest(rqst);

	if(rqst->m_RelayDiscoveryStatus.Pending())
	{
		rlPresence::CancelQuery(&rqst->m_RelayDiscoveryStatus);
	}

	if(rqst->m_RelayConnectionStatus.Pending())
	{
		netRelay::CancelConnect(&rqst->m_RelayConnectionStatus);
	}

	if(rqst->m_KeyExchangeStatus.Pending())
	{
		netKeyExchange::CancelKeyExchange(&rqst->m_KeyExchangeStatus);
	}

	if(rqst->Pending())
    {
		netDebug("[%u]: Canceling pending tunnel request...", rqst->GetId());

		if(rqst->m_Status)
		{
			rqst->m_Status->SetCanceled();
			rqst->m_Status = NULL;
		}

		// fail the tunnel request (this also calls through to remove the request)
		rqst->SetState(netTunnelRequest::STATE_FAILED, NET_TUNNELER_CANCELLED);
    }
}

void
netTunneler::OverrideRequest(netTunnelRequest* rqst, const netAddress& useAddr)
{
    SYS_CS_SYNC(m_Cs);

	this->NativeCancelRequest(rqst);

    if(rqst->Pending())
    {
        netDebug("[%u]: OverrideRequest - Assigning [" NET_ADDR_FMT "]", rqst->GetId(), NET_ADDR_FOR_PRINTF(useAddr));

		netP2pCrypt::Key key;
		if(this->GetP2pKey(rqst->GetPeerAddress().GetPeerId(), key))
		{
			netAddress relayAddr = useAddr.IsRelayServerAddr() ? useAddr : rqst->GetRelayAddress();
			this->UpdateP2pKey(rqst->GetPeerAddress().GetPeerId(), useAddr, relayAddr);
			rqst->m_NetAddr = useAddr;

			// clear the QuickConnect direct address since we're being overridden.
			rqst->m_QuickConnectDirectAddr.Clear();

			rqst->SetState(netTunnelRequest::STATE_SUCCEEDED);
		}
		else
		{
			OUTPUT_ONLY(char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE]);
			netDebug("[%u]: OverrideRequest - No P2P key for peerId: %s. Rejecting override.", rqst->GetId(), rqst->GetPeerAddress().GetPeerId().ToHexString(peerIdHexString));
		}
    }
}

void 
netTunneler::UpdateP2pKey(const netPeerId& peerId, const netAddress& newAddr, const netAddress& relayAddr)
{
	SYS_CS_SYNC(m_Cs);

	netKeyExchange::UpdateP2pKey(peerId, newAddr, relayAddr);
}

void
netTunneler::UpdateRequest(netTunnelRequest* rqst)
{
	switch(rqst->m_State)
	{
	case netTunnelRequest::STATE_TUNNEL_DIRECT:
		if(this->NativeOpenTunnel(rqst))
		{
			rqst->SetState(netTunnelRequest::STATE_TUNNELING_DIRECT);
		}
		else
		{
			netDebug("[%u]: Failed to open direct tunnel", rqst->GetId());
			rqst->SetState(netTunnelRequest::STATE_DISCOVER_RELAY_ADDRESS);
		}
		break;
	case netTunnelRequest::STATE_TUNNELING_DIRECT:
		this->NativeUpdateRequest(rqst);

		if(rqst->m_DirectTunnelingStatus.Succeeded())
		{
			rqst->SetState(netTunnelRequest::STATE_EXCHANGE_KEYS);
		}
		else if(!rqst->m_DirectTunnelingStatus.Pending())
		{
			rqst->m_DirectResultCode = rqst->m_DirectTunnelingStatus.GetResultCode();
			netDebug("[%u]: Failed to open direct tunnel. Result Code: %d", rqst->GetId(), rqst->m_DirectResultCode);
			rqst->SetState(netTunnelRequest::STATE_DISCOVER_RELAY_ADDRESS);
		}
		break;
	case netTunnelRequest::STATE_DISCOVER_RELAY_ADDRESS:
		if(rqst->DiscoverRelayAddress())
		{
			rqst->SetState(netTunnelRequest::STATE_DISCOVERING_RELAY_ADDRESS);
		}
		else
		{
			rqst->SetState(netTunnelRequest::STATE_FAILED, RELAY_ERROR_DISCOVERY_FAILED);
		}
		break;
	case netTunnelRequest::STATE_DISCOVERING_RELAY_ADDRESS:
		rqst->UpdateRelayAddressDiscovery();

		if(rqst->m_RelayDiscoveryStatus.Succeeded())
		{
			netAssert(rqst->m_RelayAddr.IsRelayServerAddr());

			// if we already attempted to tunnel via this relay address, don't try again
			if(rqst->m_PrevRelayAddress.IsRelayServerAddr() &&
				(rqst->m_PrevRelayAddress.IsEquivalent(rqst->m_RelayAddr)))
			{
				rqst->SetState(netTunnelRequest::STATE_FAILED, rqst->m_KeyExchangeStatus.GetResultCode());
			}
			else
			{
				rqst->m_PrevRelayAddress = rqst->m_RelayAddr;
				rqst->SetState(netTunnelRequest::STATE_CONNECT_TO_RELAY);
			}
		}
		else if(!rqst->m_RelayDiscoveryStatus.Pending())
		{
			netAssert(!rqst->m_RelayAddr.IsValid());
			rqst->SetState(netTunnelRequest::STATE_FAILED, rqst->m_RelayDiscoveryStatus.GetResultCode());
		}
		break;
	case netTunnelRequest::STATE_CONNECT_TO_RELAY:
		if(rqst->ConnectToRelay())
		{
			rqst->SetState(netTunnelRequest::STATE_CONNECTING_TO_RELAY);
		}
		else
		{
			rqst->SetState(netTunnelRequest::STATE_FAILED, RELAY_ERROR_CONNECT_FAILED);
		}
		break;
	case netTunnelRequest::STATE_CONNECTING_TO_RELAY:
		rqst->UpdateConnectingToRelay();

		if(rqst->m_RelayConnectionStatus.Succeeded())
		{
			rqst->SetState(netTunnelRequest::STATE_EXCHANGE_KEYS);
		}
		else if(!rqst->m_RelayConnectionStatus.Pending())
		{
			rqst->SetState(netTunnelRequest::STATE_FAILED, rqst->m_RelayConnectionStatus.GetResultCode());
		}
		break;
	case netTunnelRequest::STATE_EXCHANGE_KEYS:
		if(rqst->ExchangeKeys())
		{
			rqst->SetState(netTunnelRequest::STATE_EXCHANGING_KEYS);
		}
		else
		{
			rqst->SetState(netTunnelRequest::STATE_FAILED, KX_ERROR_FAILED_TO_INITIATE);
		}
		break;
	case netTunnelRequest::STATE_EXCHANGING_KEYS:
		rqst->UpdateKeyExchange();

		if(rqst->m_KeyExchangeStatus.Succeeded())
		{
			netAddress finalRelayAddress;
			if(netKeyExchange::GetRelayAddr(rqst->GetPeerAddress().GetPeerId(), finalRelayAddress) &&
				finalRelayAddress.IsRelayServerAddr() &&
				finalRelayAddress.GetTargetAddress().IsValid())
			{
#if !__NO_OUTPUT
				if(rqst->m_RelayAddr.GetProxyAddress().IsValid() && rqst->m_RelayAddr.GetRelayToken().IsValid() && !rqst->m_RelayAddr.GetTargetAddress().IsValid())
				{
					netDebug("[%u]: Exchanging secure relay address: " NET_ADDR_FMT " with real address: " NET_ADDR_FMT, rqst->GetId(), NET_ADDR_FOR_PRINTF(rqst->m_RelayAddr), NET_ADDR_FOR_PRINTF(finalRelayAddress));
				}
#endif

				rqst->m_RelayAddr.Init(finalRelayAddress.GetProxyAddress(), netRelayToken::INVALID_RELAY_TOKEN, finalRelayAddress.GetTargetAddress());

				// if key exchange succeeded via relay address and discovered a different relay address
				// for the remote peer, update to that relay address
				if(rqst->m_NetAddr.IsRelayServerAddr() && (rqst->m_NetAddr != rqst->m_RelayAddr))
				{
					netDebug("[%u]: Key exchanged succeeded and found an updated relay address: " NET_ADDR_FMT, rqst->GetId(), NET_ADDR_FOR_PRINTF(rqst->m_RelayAddr));
					rqst->m_NetAddr = rqst->m_RelayAddr;
				}
			}

			rqst->SetState(netTunnelRequest::STATE_SUCCEEDED);
		}
		else if(!rqst->m_KeyExchangeStatus.Pending())
		{
			// case 1: key exchange failed via direct address. Look up the remote peer's relay address
			// via presence, and try key exchange via relay server.
			// case 2: key exchange failed via relay server. Look up the remote peer's relay address
			// via presence, and try key exchange via relay server again if the address is different.
			if(sm_AllowKeyExchangeRetry && (rqst->m_PresenceQueryState == netTunnelRequest::STATE_PQ_UNATTEMPTED))
			{
				if(rqst->m_NetAddr.IsDirectAddr())
				{
					netDebug("[%u]: Key exchanged failed via direct address, clearing the QuickConnect address", rqst->GetId());
					rqst->m_QuickConnectDirectAddr.Clear();
				}

				rqst->m_ForcePresenceQuery = true;
				rqst->SetState(netTunnelRequest::STATE_DISCOVER_RELAY_ADDRESS);
			}
			else
			{
				rqst->SetState(netTunnelRequest::STATE_FAILED, rqst->m_KeyExchangeStatus.GetResultCode());
			}
		}
		break;
	case netTunnelRequest::STATE_SUCCEEDED:		
		break;
	case netTunnelRequest::STATE_FAILED:
		break;
	case netTunnelRequest::STATE_NONE:
		netAssertf(false, "netTunneler::UpdateRequest :: STATE_NONE");
		break;
	}
}

void
netTunneler::Update()
{
	PROFILE

    SYS_CS_SYNC(m_Cs);

	netKeyExchange::Update();

	s_RelayAddressCache.Update();

	NativeUpdate();

    Requests::iterator it = m_Requests.begin();
    Requests::iterator next = it;
    Requests::const_iterator stop = m_Requests.end();

    for(++next; stop != it; it = next, ++next)
    {
        netTunnelRequest* rqst = *it;
        netAssert(rqst->Pending());
		UpdateRequest(rqst);
	}
}

void
netTunneler::SetAllowKeyExchangeRetry(const bool allowKeyExchangeRetry)
{
	if(sm_AllowKeyExchangeRetry != allowKeyExchangeRetry)
	{
		netDebug("SetAllowKeyExchangeRetry :: %s", allowKeyExchangeRetry ? "true" : "false");
		sm_AllowKeyExchangeRetry = allowKeyExchangeRetry;
	}
}

//private:

void
netTunneler::CancelAllRequests()
{
	SYS_CS_SYNC(m_Cs);

	netDebug("CancelAllRequests");

	while(!m_Requests.empty())
	{
		netTunnelRequest* rqst = m_Requests.front();
		netAssert(rqst->Pending());

		//Calling CancelRequest() removes the request from the queue.
		this->CancelRequest(rqst);
	}

	//CancelRequest() should have removed all requests from the queue.
	netAssert(m_Requests.empty());
}

void
netTunneler::AddRequest(netTunnelRequest* rqst)
{
    SYS_CS_SYNC(m_Cs);

    if(netVerify(!rqst->m_Tunneler))
    {
        netAssert(!rqst->Pending());

        rqst->m_Tunneler = this;
		rqst->m_TimeStarted = sysTimer::GetSystemMsTime();

        m_Requests.push_back(rqst);
    }
}

void
netTunneler::RemoveRequest(netTunnelRequest* rqst)
{
    SYS_CS_SYNC(m_Cs);

    if(netVerify(this == rqst->m_Tunneler))
    {
		netDebug("[%u]: RemoveRequest. Time: %u ms", rqst->GetId(), sysTimer::GetSystemMsTime() - rqst->m_TimeStarted);

        m_Requests.erase(rqst);
        rqst->m_Tunneler = NULL;
    }
}

bool 
netTunneler::RemoveCachedRelayAddress(const netAddress& addr)
{
	SYS_CS_SYNC(m_Cs);

	return s_RelayAddressCache.RemoveEntry(addr);
}

bool
netTunneler::GetP2pKey(const netAddress& addr, netP2pCrypt::Key& key)
{
	bool haveKey = netKeyExchange::GetP2pKey(addr, key);

#if !__NO_OUTPUT
	if(!haveKey)
	{
		netDebug3("netTunneler::GetP2pKey no active key found for " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr));
	}
#endif

	return haveKey;
}

bool
netTunneler::GetP2pKey(const netPeerId& peerId, netP2pCrypt::Key& key)
{
	bool haveKey = netKeyExchange::GetP2pKey(peerId, key);

#if !__NO_OUTPUT
	if(!haveKey)
	{
		char peerIdHexString[netPeerId::TO_HEX_STRING_BUFFER_SIZE];
		netDebug3("netTunneler::GetP2pKey no active key found for peer id %s", peerId.ToHexString(peerIdHexString));
	}
#endif

	return haveKey;
}

// this isn't in the header due to recursive header includes
static netIceTunneler m_IceTunneler;

bool
netTunneler::NativeInit(netConnectionManager* cxnMgr)
{
	return m_IceTunneler.Init(cxnMgr);
}

void
netTunneler::NativeShutdown()
{
	SYS_CS_SYNC(m_Cs);

	m_IceTunneler.Shutdown();
}

bool
netTunneler::NativeOpenTunnel(netTunnelRequest* rqst)
{
	SYS_CS_SYNC(m_Cs);
	return m_IceTunneler.OpenTunnel(rqst->m_PeerAddr,
									&rqst->m_NetAddr,
									&rqst->m_RelayAddr,
									rqst->m_TunnelDesc.m_Bilateral,
									rqst->m_TunnelDesc.m_UniqueId,
									0,
									rqst->m_Id,
									&rqst->m_DirectTunnelingStatus);
}

bool
netTunneler::NativeCloseTunnel(const netAddress& addr)
{
	return m_IceTunneler.CloseTunnel(addr);
}

void
netTunneler::NativeCancelRequest(netTunnelRequest* rqst)
{
	m_IceTunneler.CancelRequest(&rqst->m_DirectTunnelingStatus);
}

bool
netTunneler::NativeHasActiveSession(const netPeerAddress& peerAddr)
{
	return m_IceTunneler.HasActiveSession(peerAddr);
}

void
netTunneler::NativeUpdateRequest(netTunnelRequest* UNUSED_PARAM(rqst))
{

}

void
netTunneler::NativeUpdate()
{
	SYS_CS_SYNC(m_Cs);

	m_IceTunneler.Update();
}

}   //namespace rage
