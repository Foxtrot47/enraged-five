// 
// net/lag.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_LAG_H
#define NET_LAG_H

#include "connectionmanager.h"
#include "message.h"

/*
    netLag is used to estimate the lag between peers.

    To begin lag estimation call Start() with the address of the
    peer to which a lag estimation is desired and the interval at
    which that peer should be pinged to obtain lag samples.

    Once started an instance of netLag will answer pings from any
    other peer.
*/

namespace rage
{

class netLag;

//PURPOSE
//  Network message used to synchronize time.
class netLagPingMsg
{
    friend class netLag;

public:

    NET_MESSAGE_DECL(netLagPingMsg, NET_LAG_PING_MSG);

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerUns(msg.m_Type, datBitsNeeded<NUM_TYPES>::COUNT)
                && bb.SerUns(msg.m_Timestamp, 32);
    }

private:

    enum Type
    {
        TYPE_INVALID    = -1,
        TYPE_PING,
        TYPE_PONG,

        NUM_TYPES,
    };

    unsigned m_Type;
    unsigned m_Timestamp;
};

class netLag
{
public:

    enum
    {
        //Default ping interval in milliseconds.
        DEFAULT_PING_INTERVAL   = 2 * 1000,
    };

    netLag();

    virtual ~netLag();

    //PURPOSE
    //  Start estimating lag.
    //PARAMS
    //  cxnMgr      - Connection manager used for communication.
    //  peerAddr    - Network address of the peer to which we'll
    //                compute lag.
    //  channelId   - Channel on which ping messages will be
    //                sent/received.  This should be a channel that
    //                is not used by any other system.
    //  pingInterval    - Interval at which pings will be sent.
    bool Start(netConnectionManager* cxnMgr,
                const netAddress& peerAddr,
                const unsigned channelId,
                const unsigned pingInterval);

    //PURPOSE
    //  Stop managing time.
    bool Stop();

    //PURPOSE
    //  Update the current state.  This should be called as often as
    //  possible (at least once per frame).
    void Update();

    //PURPOSE
    //  Returns the current local time.
    //  Default behavior is to return the result of sysTimer::GetSystemMsTime().
    virtual unsigned GetLocalTime() const;

    //PURPOSE
    //  Returns an estimate of round trip time.
    unsigned GetAvgRtTime() const;

    //PURPOSE
    //  Returns the address of the peer to which we're computing lag.
    const netAddress& GetPeerAddress();

    //PURPOSE
    //  Returns true if there are samples available to compute lag.
    bool HasSamples() const;

private:

    void OnNetEvent(netConnectionManager* cxnMgr,
                    const netEvent* evt);

    netConnectionManager* m_CxnMgr;
    netAddress m_PeerAddr;

    netConnectionManager::Delegate m_Dlgt;

    //Next time we'll send a ping.
    unsigned m_NextPingTime;

    unsigned m_PingInterval;

    unsigned m_ChannelId;

    enum
    {
        NUM_RTT_SAMPLES     = 8
    };

    unsigned m_Rtt[NUM_RTT_SAMPLES];    //Most recent round trip times
    unsigned m_RttTop;                  //Index into m_Rtt
    unsigned m_RttTotal;                //Total of most recent rtt
    unsigned m_RttCount;                //Number of rtt
};

}   //namespace rage

#endif  //NET_TIMESYNC_H
