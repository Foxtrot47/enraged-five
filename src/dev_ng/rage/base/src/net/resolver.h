// 
// net/resolver.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_RESOLVER_H 
#define NET_RESOLVER_H 

namespace rage
{

class netStatus;
class netIpAddress;

class netResolver
{
public:

    static bool ResolveHost(const char* hostname,
                            netIpAddress* ip,
                            netStatus* status);

    static void Cancel(netStatus* status);
};

} // namespace rage

#endif // NET_RESOLVER_H 
