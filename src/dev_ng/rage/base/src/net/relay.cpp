// 
// net/relay.cpp
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#include "relay.h"

#include "diag/channel.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "natdetector.h"
#include "netdiag.h"
#include "nethardware.h"
#include "netsocketmanager.h"
#include "packet.h"
#include "profile/rocky.h"
#include "resolver.h"
#include "status.h"
#include "time.h"
#include "rline/rlpresence.h"
#include "rline/ros/rlros.h"
#include "rline/rltelemetry.h"
#include "rline/rltitleid.h"
#include "string/stringutil.h"
#include "system/param.h"
#include "system/timer.h"

#include <time.h>

#if RSG_BANK
#include "bank/bank.h"
#include "bank/bkmgr.h"
#endif

#if RSG_PC || RSG_DURANGO
#define STRICT
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#pragma warning(pop)
#endif //RSG_PC || RSG_DURANGO

#if SUPPORT_DTLS
#include "wolfssl/ssl.h"
#include "wolfssl/error-ssl.h"

#if defined(DEBUG_WOLFSSL)
// Note: enabling this sets the SslLogCb for all wolfSSL sessions, including those originating from tcp.cpp.
#define DTLS_DEBUGGING (0)
#else
#define DTLS_DEBUGGING (0)
#endif

#if DTLS_DEBUGGING
#include "wolfssl/wolfcrypt/logging.h"
#endif
#endif

#ifndef MAX
#define	MAX(a,b) (((a)>(b))?(a):(b))
#endif

namespace rage
{

RAGE_DECLARE_CHANNEL(ragenet);
RAGE_DEFINE_SUBCHANNEL(ragenet, relay)
#undef __rage_channel
#define __rage_channel ragenet_relay

PARAM(netrelayserveraddr, "Sets the full address of the relay server: <x.x.x.x:port>.");
PARAM(netrelaysecure, "Determine whether to use secure protocol when communicating with the relay.");
PARAM(netpresenceserveraddr, "Sets the full address the of presence server: <x.x.x.x:port> or <hostname:port>.");
PARAM(netpresencesecure, "Determine whether to use secure protocol when communicating with the presence server.");
PARAM(netrelaydisable, "Disables discovery of the relay server");
PARAM(netpresenceserverdisable, "Disables discovery of the presence server");
PARAM(netrelaypinginterval, "Number of seconds between pinging the relay server.");
PARAM(netrelaychangeaddr, "Change relay servers periodically. Tests relayed p2p connections when our relay address changes.");
PARAM(netrelayignorepongs, "Ignores pongs from the relay server to help test fault tolerance.");
PARAM(netrelayignorep2pterminus, "Ignores P2P terminus packets that are received from the relay server.");
PARAM(netpresenceignorepongs, "Ignores pongs from the presence server to help test fault tolerance.");

//Used to select a random relay server
static netRandom netRelayRNG((int) time(NULL));

//Ping interval starts off at the default interval when we first go online.
//As soon as we get a pong, we set the ping interval to the minimum supported UDP port binding timeout.
//As the NAT detector determines that the NAT keeps the port open longer, we increase the time between pings.
static const unsigned NET_MIN_PORT_BINDING_TIMEOUT_SEC			= 15;	// minimum supported UDP port binding timeout
static const unsigned NET_DEFAULT_PING_INTERVAL_SEC				= 60;
static const unsigned NET_DEFAULT_PING_INTERVAL_ADJUSTMENT_MS	= 5000;	// ping is sent slightly early to catch UDP timeout edges and to allow time for resends (eg. if the port closes in exactly 60 seconds, we send at 55 seconds).
static const unsigned NET_RELAY_MAXIMUM_PING_INTERVAL_SEC		= 60;
static const unsigned NET_PRESENCE_MAXIMUM_PING_INTERVAL_SEC	= 60;	// the presence ping is used to refresh the player's presence record.
static const unsigned NET_RELAY_RETRY_SCALE						= 110;	//1.1
static const unsigned NET_DEFAULT_RELAY_PING_RETRY_MS			= 2000;	// how long to wait for a pong before resending the first unacked ping.
static const float NET_RELAY_RETRY_BACKOFF_MULTIPLIER			= 1.5f; // relay ping retries back off by multiplying the ping retry time by NET_RELAY_RETRY_BACKOFF_MULTIPLIER for each consecutive unacked ping.
static const unsigned NET_MAX_TIME_TO_DETECT_PING_INTERVAL_SEC  = (3 * 60);
static const unsigned NET_MAX_TIME_UNTIL_READY_FOR_MP_SEC		= 30;	

#if RSG_OUTPUT
// don't assume these ports will be used outside of development
enum RelayPresenceDefaultPorts : u16
{
	NET_PRESENCE_DEFAULT_PORT = 61455,
	NET_RELAY_DEFAULT_PORT = 61456,
	NET_PRESENCE_DEFAULT_PORT_SECURE = 61457,
	NET_RELAY_DEFAULT_PORT_SECURE = 61458,
};
#endif

//Maximum number of un-acked pings before we try connecting to
//a different relay server.
static const unsigned NET_RELAY_SERVER_MAX_UNACKED_PINGS = 4;

//Put in the header of presence keepalives.
static const char PRESENCE_VERSION_STRING[]                 = {"PRES1"};

enum PresenceServerDiscoveryState
{
	PRES_STATE_WAIT_FOR_ROS_ONLINE,
	PRES_STATE_BEGIN_DISCOVERY,
	PRES_STATE_DISCOVERING,
	PRES_STATE_HAVE_ADDRESS
};

static PresenceServerDiscoveryState s_PresenceServerState = PRES_STATE_WAIT_FOR_ROS_ONLINE;

static unsigned s_PingIntervalAdjustmentMs = NET_DEFAULT_PING_INTERVAL_ADJUSTMENT_MS;
static unsigned s_TunableRelayPingIntervalMs = 0;	// 0 means no tunable value has been set
static unsigned s_PingRetryMs = NET_DEFAULT_RELAY_PING_RETRY_MS; // 0 means disable fast retries (retry at the regular ping interval)
static bool s_AllowMultiplayerWithoutRelayServer = false; // by default, we need a relay server connection before allowing entry into multiplayer
static unsigned s_RelayTelemetryIntervalMs = 1000 * 60 * 10; // by default, we send telemetry every 10 mins

static netIpAddress s_PresenceServerIp;
static u16 s_PresenceServerPort;

#if RSG_BANK
bool netRelay::sm_bIgnorePresencePongs = false;
bool netRelay::sm_bIgnoreRelayPongs = false;
#endif

#if !__NO_OUTPUT
static netTimeout s_RelayAddrChangeTimer;
#endif

#if SUPPORT_DTLS
// Our pinned CAs to verify our relay/presence server certificates against. 
static const char s_ServerCertificate[] = 
	"-----BEGIN CERTIFICATE-----\n"
	"MIIC7zCCAdegAwIBAgIJAJqoUEaKbdKaMA0GCSqGSIb3DQEBBQUAMA0xCzAJBgNV\n"
	"BAYTAlVTMCAXDTE0MDkxMjIxMjcwOFoYDzIwNTkwNzIyMjEyNzA4WjANMQswCQYD\n"
	"VQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALSm5zyAOGZF\n"
	"iXq5Sb8qzKcg7IEe5SVrpZFygrYKfeHsKHxa0jHUdh4iBsHAKi3Ld8VHsf4F2Vu5\n"
	"b27rfCfYW1bL7Hk4GG/kEYkoYQGpfykn4+xxx2LKtk3Uw7lcaHIbbne8tCIEzfAT\n"
	"pu2h2LXq/zJnJcFChEKq3w6YPhTe21C7tNP2m7IsWCewYClRMRvk+hL2V4erYZHu\n"
	"LRiWvGYZjKfA60AXwqjOFO+YBfsnbjUlftkS4/TtjPglct7MnXGOMvlgKlT45sqE\n"
	"iYbunL8bUZYFBZtKvi9R59uXyNFzZvNM53nIoOe8fWGO5I6iGk2tP7HvcVrnMS4G\n"
	"QpInf3LlUMUCAwEAAaNQME4wHQYDVR0OBBYEFPuaxRHiYZyXCKVZ90WKuf5od2vX\n"
	"MB8GA1UdIwQYMBaAFPuaxRHiYZyXCKVZ90WKuf5od2vXMAwGA1UdEwQFMAMBAf8w\n"
	"DQYJKoZIhvcNAQEFBQADggEBAD7CHXIcDgeOPUHyNBq8WFYYj8PZdz6AKPy6I/oZ\n"
	"K7UcfVaQ0uYRv2Yt6IGU1frXHXTGgLkBH+3YgCb37Phyh531N8OARrrtVLfKXpll\n"
	"oPBmcAxnnw+g3pAyVJCQRbRrvk/h8kKVSKdH/o4LjTnPXZLXw9RNBPhhvqNiwONM\n"
	"74CITGXNS1GqGs/RoC0xxBabYqZfwgxcmpbcyafMjpScSR49s8Redksa6o+TCp5b\n"
	"57dK1+h+ZYR0sS90/Qgz4PxNReh00E8Ltn5FHbJLd/EWET/udNtLC1ZzBaz4jaYz\n"
	"py2Wxj/fAujhGUa0T0mzv4EAVMYJdkIb6ID8pjl88cFTH74=\n"
	"-----END CERTIFICATE-----\n";

static const unsigned NET_RELAY_DTLS_CXN_TIMEOUT_SEC				= 60 * 3 + 10;	// DTLS connections time out if we don't receive a packet for this amount of time
static const unsigned NET_RELAY_DTLS_CONNECT_TIMEOUT_SEC			= 7;		// amount of time to allow for the DTLS connection/handshake
static const unsigned NET_RELAY_DTLS_CONNECT_RESEND_INTERVAL_MS		= 1000;		// packet resend interval during DTLS connections and disconnections
static const unsigned NET_RELAY_DTLS_MIN_RECONNECT_TIMEOUT_SEC		= 4;		// minimum amount of time to wait until retrying a failed DTLS connection/handshake (min backoff time), this gets doubled on each reconnection attempt
static const unsigned NET_RELAY_DTLS_MAX_RECONNECT_TIMEOUT_SEC		= 60 * 10;	// maximum amount of time to wait until retrying a failed DTLS connection/handshake (max backoff time)

// when DTLS_VERIFY_PEER is set to 1, we verify the relay/presence server's
// certificate and domain before allowing connections to it. This will
// prevent connections to imposter servers.
#define DTLS_VERIFY_PEER 1

namespace netRelayGlobals {

static WOLFSSL_CTX* s_RelaySslCtx = NULL;

} // namespace netRelayGlobals

class netDtlsEndpoint
{
public:
	
	enum SslState
	{
		STATE_NOT_CONNECTED = 0,
		STATE_CONNECTING,
		STATE_CONNECTED,
		STATE_DISCONNECTING,
	};

	netDtlsEndpoint()
	: m_Ssl(NULL)
	{
		Clear();
	}

	~netDtlsEndpoint()
	{
		Clear();
	}

	void Clear()
	{
		m_Timeout.Clear();
		m_ResendTimeout.Clear();
		m_Addr.Clear();

		if(m_Ssl)
		{
			wolfSSL_free(m_Ssl);
			m_Ssl = NULL;
		}
		
		m_Connect = false;
		m_SslState = STATE_NOT_CONNECTED;

		for(int i = 0; i < m_ConnectStatus.GetCount(); ++i)
		{
			if(m_ConnectStatus[i] && m_ConnectStatus[i]->Pending())
			{
				m_ConnectStatus[i]->SetFailed();
			}
		}
		m_ConnectStatus.clear();		
	}

	bool ResetForReconnect()
	{
		rtry
		{
			m_Timeout.Clear();
			m_ResendTimeout.Clear();

			if(m_Ssl)
			{
				wolfSSL_free(m_Ssl);
				m_Ssl = NULL;
			}

			m_Ssl = wolfSSL_new(netRelayGlobals::s_RelaySslCtx);
			rverify(m_Ssl != NULL, catchall, netError("Could not allocate SSL object"));
			m_SslState = STATE_NOT_CONNECTED;

			// don't clear the m_ConnectStatus here

			return true;
		}
		rcatchall
		{
			
		}

		return false;
	}

	netSocketAddress m_Addr;
	WOLFSSL* m_Ssl;
	netTimeout m_Timeout;
	netTimeout m_ResendTimeout;

	SslState m_SslState;
	bool m_Connect;
	atArray<netStatus*> m_ConnectStatus;
	inlist_node<netDtlsEndpoint> m_ListLink;
};

// we no longer use DTLS relays, only the presence server(s) use DTLS. We only connect to one presence server
// at a time, but we may need to switch between presence servers if one or more are not responding.
static const unsigned MAX_DTLS_ENDPOINTS = 4;
static netDtlsEndpoint s_EndpointsPool[MAX_DTLS_ENDPOINTS];
typedef inlist<netDtlsEndpoint, &netDtlsEndpoint::m_ListLink> EndpointList;
static EndpointList s_ActiveEndpoints;
static EndpointList s_FreeEndpoints;

static int s_RcvdPktLen = 0;
//One extra byte so we can null-terminate messages
static u8* s_RcvdPktBuf = nullptr;
#if USE_NET_ASSERTS
static netSocketAddress s_RcvdPktSender;
#endif

#endif

#define NET_SET_SKTERR(se, e)   while((se)){*(se) = (e);break;}

static bool s_RelayInitialized = false;

//True if the relay is running
static bool s_RelayStarted = false;
static unsigned s_RelayRunningStartTime = 0;

static sysCriticalSectionToken s_MainCs;

//Protects access to concurrent delegates.
static sysCriticalSectionToken s_ConcurrentDlgtCs;

//My address relative to the relay.
static netAddress s_MyRelayAddr;
static netAddress s_MyLastRelayAddr;
static netRelayToken s_MyRelayToken;

//My address relative to the presence.
static netSocketAddress s_MyPresenceAddr;

//Contains a list of relay servers
static rlRosGeoLocInfo s_RelayGeolocInfo;

//Contains a list of presence servers
static rlRosPresenceInfo s_PresenceServerList;

//Relay/Presence Telemetry
class netRelayPresenceStats : public rlMetric
{
#if GTA_VERSION
	RL_DECLARE_METRIC(RELAY_PRESENCE, rlTelemetryPolicies::DEFAULT_RAGE_LOG_CHANNEL, 3);
#else
	RL_DECLARE_METRIC(RELAY_PRESENCE, rlTelemetryPolicies::DEFAULT_RAGE_METRIC_GROUP, METRIC_QUEUE_HIGH_VOLUME);
#endif

public:
	netRelayPresenceStats()
	{
		ResetStats();
		m_SendTimer.Stop();
	}

	void ResetStats()
	{
		m_NumPresencePings = 0;
		m_NumPresenceConnectionAttempts = 0;
		m_NumFailedPresenceConnections = 0;
		m_NumPresenceDisconnections = 0;
		m_NumPresencePongs = 0;
		m_NumPresenceMessagesReceived = 0;
		m_NumFailedP2pPresenceMessages = 0;
		m_NumSucceededP2pPresenceMessages = 0;
		m_NumRelayPings = 0;
		m_NumRelayConnectionAttempts = 0;
		m_NumFailedRelayConnections = 0;
		m_NumRelayDisconnections = 0;
		m_NumRelayAddrChanges = 0;
		m_NumRelayMappedAddrChanges = 0;
		m_NumRelayPongs = 0;
		m_FailedRelayServerAddr.Clear();
		m_FailedPresenceServerAddr.Clear();
	}

	void IncNumPresenceConnectionAttempts() { ++m_NumPresenceConnectionAttempts; }
	void IncNumFailedPresenceConnections() { ++m_NumFailedPresenceConnections; }
	void IncNumPresenceDisconnections() { ++m_NumPresenceDisconnections; }
	void IncTotalNumPresencePings() { ++m_NumPresencePings; }
	void IncNumPresncePongs() { ++m_NumPresencePongs; }
	void IncNumRelayConnectionAttempts() { ++m_NumRelayConnectionAttempts; }
	void IncNumFailedRelayConnections() { ++m_NumFailedRelayConnections; }
	void IncNumRelayDisconnections() { ++m_NumRelayDisconnections; }
	void IncTotalNumRelayPings() { ++m_NumRelayPings; }
	void IncNumRelayPongs() { ++m_NumRelayPongs; }
	void IncNumRelayAddrChanges() { ++m_NumRelayAddrChanges; }
	void IncNumRelayMappedAddrChanges() { ++m_NumRelayMappedAddrChanges; }
	void SetFailedRelayServerAddr(const netSocketAddress& addr) { m_FailedRelayServerAddr = addr; }
	void SetFailedPresenceServerAddr(const netSocketAddress& addr) { m_FailedPresenceServerAddr = addr; }
	void IncNumPresenceMessagesReceived() { ++m_NumPresenceMessagesReceived; }
	
	u16 GetNumRelayAddrChanges() const { return m_NumRelayAddrChanges; }
	u16 GetNumRelayMappedAddrChanges() const { return m_NumRelayMappedAddrChanges; }

	void IncP2pPresenceMessageStats(const bool succeeded)
	{
		if(succeeded)
		{
			++m_NumSucceededP2pPresenceMessages;
		}
		else
		{
			++m_NumFailedP2pPresenceMessages;
		}
	}

	bool Write(RsonWriter* rw) const
	{
		char failedRelayServerAddr[netSocketAddress::MAX_STRING_BUF_SIZE] = { 0 };
		if(m_FailedRelayServerAddr.IsValid())
		{
			m_FailedRelayServerAddr.Format(failedRelayServerAddr, true);
		}

		char failedPresenceServerAddr[netSocketAddress::MAX_STRING_BUF_SIZE] = { 0 };
		if(m_FailedPresenceServerAddr.IsValid())
		{
			m_FailedPresenceServerAddr.Format(failedPresenceServerAddr, true);
		}

		char lpub[netSocketAddress::MAX_STRING_BUF_SIZE] = { 0 };

		netPeerAddress localPeerAddr;
		if(netPeerAddress::GetLocalPeerAddress(rlPresence::GetActingUserIndex(), &localPeerAddr))
		{
			localPeerAddr.GetPublicAddress().Format(lpub, true);
		}

		bool success =  rw->WriteUns("PCA", m_NumPresenceConnectionAttempts)
						&& rw->WriteUns("FPC", m_NumFailedPresenceConnections)
						&& rw->WriteUns("PD", m_NumPresenceDisconnections)
						&& rw->WriteUns("PPI", m_NumPresencePings)
						&& rw->WriteUns("PPO", m_NumPresencePongs)
						&& rw->WriteUns("RCA", m_NumRelayConnectionAttempts)
						&& rw->WriteUns("FRC", m_NumFailedRelayConnections)
						&& rw->WriteUns("RD", m_NumRelayDisconnections)
						&& rw->WriteUns("RPI", m_NumRelayPings)
						&& rw->WriteUns("RPO", m_NumRelayPongs)
						&& rw->WriteUns("RAC", m_NumRelayAddrChanges)
						&& rw->WriteUns("RMC", m_NumRelayMappedAddrChanges)
						&& rw->WriteUns("PM", m_NumPresenceMessagesReceived)
						&& rw->WriteUns("SPP", m_NumSucceededP2pPresenceMessages)
						&& rw->WriteUns("FPP", m_NumFailedP2pPresenceMessages)
						&& rw->WriteUns("NAT", netNatDetector::GetLocalNatInfo().GetNatType())
						&& rw->WriteUns("UTT", netNatDetector::GetLocalNatInfo().GetUdpTimeoutSec())
						&& rw->WriteString("FRA", failedRelayServerAddr)
						&& rw->WriteString("FPA", failedPresenceServerAddr)
						&& rw->WriteString("LPUB", lpub);

		return success;
	}

	netRetryTimer& GetSendTimer()
	{
		return m_SendTimer;
	}

private:
	u16 m_NumPresenceConnectionAttempts;
	u16 m_NumFailedPresenceConnections;
	u16 m_NumPresenceDisconnections;
	u16 m_NumPresencePings;
	u16 m_NumPresencePongs;
	unsigned m_NumPresenceMessagesReceived;
	unsigned m_NumFailedP2pPresenceMessages;
	unsigned m_NumSucceededP2pPresenceMessages;
	u16 m_NumRelayConnectionAttempts;
	u16 m_NumFailedRelayConnections;
	u16 m_NumRelayDisconnections;
	u16 m_NumRelayPings;
	u16 m_NumRelayPongs;
	u16 m_NumRelayAddrChanges;
	u16 m_NumRelayMappedAddrChanges;
	netSocketAddress m_FailedRelayServerAddr;
	netSocketAddress m_FailedPresenceServerAddr;

	netRetryTimer m_SendTimer;
};

netRelayPresenceStats s_RelayPresenceStats;

class ServerInfo
{
public:

    netSocketAddress m_ServerAddr;

	netStatus m_ConnectStatus;

    netRetryTimer m_NextPing;
	netRetryTimer m_NextSecureConnect;
	unsigned m_LastPingTimeMs;
	unsigned m_LastPongTimeMs;
	unsigned m_LastTerminusTimeMs;
	unsigned m_NextPingTimeMs;

    // We count the number of unacked pings.
	// For the Relay server, we restart if it's too many.
    // When restarting we pick a different relay server.
    // Presence servers are not geo located.
    unsigned m_NumUnAckedPings;

	unsigned m_NumConsecutivePingRetries;
	unsigned m_PingRetryMs;

	netRetryTimer m_DiscoveryRetryTimer;
    netStatus m_DiscoveryStatus;
	
    ServerInfo()
    {
        ClearAddr();
        m_DiscoveryRetryTimer.InitSeconds(60);
		m_NextSecureConnect.InitMilliseconds(NET_RELAY_DTLS_MIN_RECONNECT_TIMEOUT_SEC * 1000);
		m_NextSecureConnect.ForceRetry();
		m_LastPingTimeMs = 0;
		m_LastPongTimeMs = 0;
		m_LastTerminusTimeMs = 0;
		m_NextPingTimeMs = 0;
		m_NumConsecutivePingRetries = 0;
		m_PingRetryMs = 0;
    }

    void ClearAddr()
    {
		if(m_ConnectStatus.Pending())
		{
			netRelay::CancelConnect(&m_ConnectStatus);
		}

		m_ConnectStatus.Reset();
        m_ServerAddr.Clear();
        m_NextPing.Reset();
		m_NextPing.Stop();
        m_NumUnAckedPings = 0;
		m_NextPingTimeMs = 0;

		// Don't clear m_NumConsecutivePingRetries here.
		// It gets preserved even when switching servers, so if
		// many/all of our relays are down, we gradually back off.
		// m_NumConsecutivePingRetries = 0;
    }

    bool HasValidAddress() const
    {
        return m_ServerAddr.IsValid();
    }

    bool GetServerAddress(netSocketAddress* addr)
    {
        const netSocketAddress tmp = m_ServerAddr;
        if(tmp.IsValid())
        {
            *addr = tmp;
            return true;
        }

        return false;
    }
};

static ServerInfo s_RelayServerInfo;
static ServerInfo s_LastRelayServerInfo;
static ServerInfo s_PresenceServerInfo;

//Callbacks listening for receipt of packets.
netRelay::Delegator netRelay::sm_Delegators[NET_RELAYEVENT_NUM_EVENTS];             //non-thread safe callbacks
netRelay::Delegator netRelay::sm_ConcurrentDelegators[NET_RELAYEVENT_NUM_EVENTS];   //thread safe callbacks

static netEventQueue<netRelayEvent> s_EventQ;
static netEventConsumer<netRelayEvent> s_EventConsumer;

//Used to allocate queued packets.
static sysMemAllocator* s_Allocator = NULL;

static netSocketManager* s_SocketManager = NULL;

enum RelayState
{
	RELAY_STATE_WAIT_FOR_SOCKET_MGR,
	RELAY_STATE_WAIT_FOR_ONLINE,
	RELAY_STATE_RUNNING,
	RELAY_STATE_NUM,
};

#if !__NO_OUTPUT
const char* s_RelayState[RELAY_STATE_NUM] =
{
	"RELAY_STATE_WAIT_FOR_SOCKET_MGR",
	"RELAY_STATE_WAIT_FOR_ONLINE",
	"RELAY_STATE_RUNNING"
};
CompileTimeAssert(COUNTOF(s_RelayState) == RELAY_STATE_NUM);
#endif

static RelayState s_State;
void SetRelayState(RelayState state)
{
	if (state == RELAY_STATE_WAIT_FOR_SOCKET_MGR || state == RELAY_STATE_WAIT_FOR_ONLINE)
	{
		s_MyRelayAddr.Clear();
		s_MyPresenceAddr.Clear();
	}

	if(s_State != state)
	{
		netDebug1("SetRelayState :: New: %s, Was: %s", s_RelayState[state], s_RelayState[s_State]);
		s_State = state;
	}
}

//////////////////////////////////////////////////////////////////////////
//  netRelay
//////////////////////////////////////////////////////////////////////////
bool
netRelay::Init(sysMemAllocator* allocator, netSocketManager* socketManager)
{
	bool success = false;

#if SUPPORT_DTLS
	netDebug3("sizeof(s_EndpointsPool):%" SIZETFMT "u", sizeof(s_EndpointsPool));
#endif

    if(netVerify(!IsInitialized()))
    {
		netRelayRNG.SetRandomSeed();

		s_Allocator = allocator;
		s_SocketManager = socketManager;

        //Post events to our own subscriber
        //which will be used to deliver them to the
        //app (via delegates) in the main thread.
        s_EventQ.Subscribe(&s_EventConsumer);

        s_RelayServerInfo.ClearAddr();
        s_RelayServerInfo.m_DiscoveryRetryTimer.ForceRetry();
        s_PresenceServerInfo.ClearAddr();
        s_PresenceServerInfo.m_DiscoveryRetryTimer.ForceRetry();

		// initial state
		SetRelayState(RELAY_STATE_WAIT_FOR_SOCKET_MGR);

		AddWidgets();

		s_RelayInitialized = true;
		success = true;
    }

	return success;
}

void
netRelay::Shutdown()
{
    if(IsInitialized())
    {
        SYS_CS_SYNC(s_MainCs);

        StopRelay();

#if SUPPORT_DTLS
		// InitEndpoints sends a shutdown packet to all endpoints we're connected to
		InitEndpoints();
		FreeSslCtx();
#endif

        for(int i = 0; i < NET_RELAYEVENT_NUM_EVENTS; ++i)
        {
            sm_Delegators[i].Clear();
            sm_ConcurrentDelegators[i].Clear();
        }

        s_MyRelayAddr.Clear();
		s_MyLastRelayAddr.Clear();
		s_MyRelayToken.Clear();
        s_MyPresenceAddr.Clear();
        s_RelayServerInfo.ClearAddr();
        s_PresenceServerInfo.ClearAddr();
		
        s_EventQ.Unsubscribe(&s_EventConsumer);
        s_EventQ.ClearEvents();

        s_Allocator = NULL;
		s_SocketManager = NULL;

        s_RelayInitialized = false;
    }
}

bool
netRelay::IsInitialized()
{
    return s_RelayInitialized;
}

void netRelay::AddWidgets()
{
#if RSG_BANK && !__RGSC_DLL
	bkBank *pBank = BANKMGR.FindBank("Network");

	if(!pBank)
	{
		pBank = &BANKMGR.CreateBank("Network");
	}

	if (!pBank) { return; }

	pBank->PushGroup("Relay", false);

	pBank->AddToggle("Ignore Presence Pongs", &sm_bIgnorePresencePongs, NullCallback);
	pBank->AddToggle("Ignore Relay Pongs", &sm_bIgnoreRelayPongs, NullCallback);

	pBank->PopGroup();
#endif
}

bool
netRelay::StartRelay()
{
    netDebug("Starting relay...");

    if(netVerify(IsInitialized()))
    {
        SYS_CS_SYNC(s_MainCs);

        if(!s_RelayStarted)
        {
            s_MyRelayAddr.Clear();
			s_MyRelayToken.Clear();
            s_RelayServerInfo.ClearAddr();
            s_RelayServerInfo.m_DiscoveryRetryTimer.ForceRetry();
            s_RelayStarted = true;
        }

        return true;
    }

    return false;
}

bool
netRelay::StopRelay()
{
    netDebug("Stopping relay...");

    SYS_CS_SYNC(s_MainCs);

	s_MyRelayAddr.Clear();
	s_MyRelayToken.Clear();
    s_RelayServerInfo.ClearAddr();
    s_RelayServerInfo.m_DiscoveryRetryTimer.ForceRetry();
    s_RelayStarted = false;

    return true;
}

bool
netRelay::IsRelayRunning()
{
    return s_RelayStarted;
}

bool
netRelay::AddDelegate(netRelayEventType eventType, Delegate* dlgt)
{
    SYS_CS_SYNC(s_MainCs);

    if(netVerify(IsInitialized())
        && netVerify((int)eventType >= 0)
        && netVerify(eventType < NET_RELAYEVENT_NUM_EVENTS))
    {
        sm_Delegators[eventType].AddDelegate(dlgt);
        return true;
    }

    return false;
}

bool
netRelay::AddConcurrentDelegate(netRelayEventType eventType, Delegate* dlgt)
{
    SYS_CS_SYNC(s_ConcurrentDlgtCs);

    if(netVerify(IsInitialized())
        && netVerify((int)eventType >= 0)
        && netVerify(eventType < NET_RELAYEVENT_NUM_EVENTS))
    {
        sm_ConcurrentDelegators[eventType].AddDelegate(dlgt);
        return true;
    }

    return false;
}

bool
netRelay::RemoveDelegate(netRelayEventType eventType, Delegate* dlgt)
{
    SYS_CS_SYNC(s_MainCs);

    if(netVerify(IsInitialized())
        && netVerify((int)eventType >= 0)
        && netVerify(eventType < NET_RELAYEVENT_NUM_EVENTS)
        && netVerifyf(sm_Delegators[eventType].HasDelegate(dlgt),
                    "Delegate isn't registered for non-concurrent events of type: %d", eventType))
    {
        sm_Delegators[eventType].RemoveDelegate(dlgt);
        return true;
    }

    return false;
}

bool
netRelay::RemoveConcurrentDelegate(netRelayEventType eventType, Delegate* dlgt)
{
    SYS_CS_SYNC(s_ConcurrentDlgtCs);

    if(netVerify(IsInitialized())
        && netVerify((int)eventType >= 0)
        && netVerify(eventType < NET_RELAYEVENT_NUM_EVENTS)
        && netVerifyf(sm_ConcurrentDelegators[eventType].HasDelegate(dlgt),
                    "Delegate isn't registered for concurrent events of type: %d", eventType))
    {
        sm_ConcurrentDelegators[eventType].RemoveDelegate(dlgt);
        return true;
    }

    return false;
}

#if SUPPORT_DTLS
void
netRelay::UpdateEndpoints()
{
	PROFILE

	SYS_CS_SYNC(s_MainCs);

	if(s_ActiveEndpoints.empty())
	{
		return;
	}

	EndpointList::iterator it = s_ActiveEndpoints.begin();
	EndpointList::iterator next = it;
	EndpointList::const_iterator stop = s_ActiveEndpoints.end();

	for(++next; stop != it; it = next, ++next)
	{
		netDtlsEndpoint* ep = *it;

		ep->m_Timeout.Update();

		if((ep->m_SslState != netDtlsEndpoint::STATE_CONNECTED))
		{
			ep->m_ResendTimeout.Update();
			if(ep->m_ResendTimeout.IsTimedOut())
			{
				netDebug("DTLS timeout: %d ms. Got timeout for endpoint " NET_ADDR_FMT ". Will resend last packet.", NET_RELAY_DTLS_CONNECT_RESEND_INTERVAL_MS, NET_ADDR_FOR_PRINTF(ep->m_Addr));
				int result = wolfSSL_dtls_got_timeout(ep->m_Ssl);
				if(result < 0)
				{
					netError("wolfSSL_dtls_got_timeout returned %d", result);
				}
				ep->m_ResendTimeout.InitMilliseconds(NET_RELAY_DTLS_CONNECT_RESEND_INTERVAL_MS);
			}
// 			else
// 			{
// 				if(ep->m_ResendTimeout.IsRunning())
// 				{
// 					netDebug3("netRelay connecting to: " NET_ADDR_FMT ". Will resend last connection packet in %d ms", NET_ADDR_FOR_PRINTF(ep->m_Addr), ep->m_ResendTimeout.GetMillisecondsUntilTimeout());
// 				}
// 			}
		}
		else
		{
			ep->m_ResendTimeout.Clear();
		}

		if(ep->m_Timeout.IsTimedOut())
		{
			const bool presenceServer = ep->m_Addr == s_PresenceServerInfo.m_ServerAddr;
				
			if(ep->m_SslState == netDtlsEndpoint::STATE_CONNECTING)
			{
				netDebug("secure endpoint " NET_ADDR_FMT " timed out due to lack of packets received. Failed to connect to server.", NET_ADDR_FOR_PRINTF(ep->m_Addr));
				if(presenceServer)
				{
					s_RelayPresenceStats.SetFailedPresenceServerAddr(ep->m_Addr);
					s_RelayPresenceStats.IncNumFailedPresenceConnections();
				}
				else
				{
					s_RelayPresenceStats.SetFailedRelayServerAddr(ep->m_Addr);
					s_RelayPresenceStats.IncNumFailedRelayConnections();
				}
			}
			else if(ep->m_SslState == netDtlsEndpoint::STATE_CONNECTED)
			{
				if(presenceServer)
				{
					netError("Lost connection to the presence server.");
					s_RelayPresenceStats.IncNumPresenceDisconnections();
				}
				else
				{
					s_RelayPresenceStats.IncNumRelayDisconnections();
				}
			}

			bool shouldFreeEndpoint = false;

			if((ep->m_SslState != netDtlsEndpoint::STATE_CONNECTED))
			{
				netDebug("secure endpoint " NET_ADDR_FMT " timed out due to lack of packets received. Freeing unused endpoint.", NET_ADDR_FOR_PRINTF(ep->m_Addr));
				shouldFreeEndpoint = true;
			}
			else
			{
				if(ep->m_SslState != netDtlsEndpoint::STATE_DISCONNECTING)
				{
					ep->m_SslState = netDtlsEndpoint::STATE_DISCONNECTING;
					ep->m_Timeout.InitSeconds(NET_RELAY_DTLS_CONNECT_TIMEOUT_SEC);
				}

				int result = wolfSSL_shutdown(ep->m_Ssl);

				if(SSL_SUCCESS == result)
				{
					netDebug2("netRelay disconnected from " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(ep->m_Addr));
					if(ep->m_Connect)
					{
						// handle the case where someone called Connect() while we were disconnecting
						ep->ResetForReconnect();
					}
					else
					{
						shouldFreeEndpoint = true;
					}
				}
				else if(SSL_SHUTDOWN_NOT_DONE == result)
				{
					// we need to continue to call SSL_shutdown()
					netAssert(ep->m_SslState == netDtlsEndpoint::STATE_DISCONNECTING);
				}
				else
				{
					int error = wolfSSL_get_error(ep->m_Ssl, result);
					if((SSL_ERROR_WANT_READ != error) && (SSL_ERROR_WANT_WRITE != error))
					{
						OUTPUT_ONLY(char buf[128];)
						netError("Error shutting down SSL connection: %s (%d)", wolfSSL_ERR_error_string(error, buf), error);
						shouldFreeEndpoint = true;
					}
					else
					{
						netAssert(ep->m_SslState == netDtlsEndpoint::STATE_DISCONNECTING);
					}
				}
			}

			if(shouldFreeEndpoint)
			{
				netSocketAddress relayAddr;
				s_RelayServerInfo.GetServerAddress(&relayAddr);
				if(ep->m_Addr == relayAddr)
				{
					if(s_RelayServerInfo.m_ConnectStatus.Succeeded())
					{
						// we just disconnected from the relay server, attempt to reconnect to the same server first
						s_RelayServerInfo.m_ConnectStatus.Reset();
						s_RelayServerInfo.m_NextSecureConnect.InitMilliseconds(NET_RELAY_DTLS_MIN_RECONNECT_TIMEOUT_SEC * 1000);
					}
				}

				netSocketAddress presenceAddr;
				s_PresenceServerInfo.GetServerAddress(&presenceAddr);
				if(ep->m_Addr == presenceAddr)
				{
					if(s_PresenceServerInfo.m_ConnectStatus.Succeeded())
					{
						// we just disconnected from the presence server, attempt to reconnect to the same server first
						s_PresenceServerInfo.m_ConnectStatus.Reset();
						s_PresenceServerInfo.m_NextSecureConnect.InitMilliseconds(NET_RELAY_DTLS_MIN_RECONNECT_TIMEOUT_SEC * 1000);
					}
				}

				FreeEndpoint(ep);
			}

#if FAKE_DROP_LATENCY
			// When we detect a timeout and GetFakeUplinkDisconnection
			// is enabled we start to fake an IsOffline too.
			if (netSocket::GetFakeUplinkDisconnection())
			{
				rlPresence::FakeOfflineDueToUplink(true);
			}
#endif
		}
		else if((ep->m_SslState == netDtlsEndpoint::STATE_NOT_CONNECTED)
				|| (ep->m_SslState == netDtlsEndpoint::STATE_CONNECTING))
		{
#if __ASSERT
			for(int i = 0; i < ep->m_ConnectStatus.GetCount(); ++i)
			{
				if(ep->m_ConnectStatus[i])
				{
					netAssert(ep->m_ConnectStatus[i]->Pending());
				}
			}
#endif

			if(ep->m_SslState == netDtlsEndpoint::STATE_NOT_CONNECTED)
			{
				ep->m_SslState = netDtlsEndpoint::STATE_CONNECTING;
				ep->m_Timeout.InitSeconds(NET_RELAY_DTLS_CONNECT_TIMEOUT_SEC);
			}

			int result = wolfSSL_connect(ep->m_Ssl);

			if(SSL_SUCCESS == result)
			{
				netDebug2("netRelay connected to " NET_ADDR_FMT " (handshake took %u ms)", NET_ADDR_FOR_PRINTF(ep->m_Addr),
						  (NET_RELAY_DTLS_CONNECT_TIMEOUT_SEC * 1000) - ep->m_Timeout.GetMillisecondsUntilTimeout());

				ep->m_SslState = netDtlsEndpoint::STATE_CONNECTED;
				ep->m_Connect = false;

				for(int i = 0; i < ep->m_ConnectStatus.GetCount(); ++i)
				{
					if(ep->m_ConnectStatus[i])
					{
						ep->m_ConnectStatus[i]->SetSucceeded();
						ep->m_ConnectStatus[i] = NULL;
					}
				}
				ep->m_ConnectStatus.DeleteMatches(NULL);

				ep->m_Timeout.InitSeconds(NET_RELAY_DTLS_CXN_TIMEOUT_SEC);
			}
			else
			{
				int error = wolfSSL_get_error(ep->m_Ssl, result);
				if((SSL_ERROR_WANT_READ != error) && (SSL_ERROR_WANT_WRITE != error))
				{
					OUTPUT_ONLY(char buf[128];)
					netError("Error creating SSL connection: %s (%d)", wolfSSL_ERR_error_string(error, buf), error);

					for(int i = 0; i < ep->m_ConnectStatus.GetCount(); ++i)
					{
						if(ep->m_ConnectStatus[i])
						{
							ep->m_ConnectStatus[i]->SetFailed();
							ep->m_ConnectStatus[i] = NULL;
						}
					}
					ep->m_ConnectStatus.DeleteMatches(NULL);

					FreeEndpoint(ep);
				}
				else
				{
					netAssert(ep->m_SslState == netDtlsEndpoint::STATE_CONNECTING);

#if __ASSERT
					for(int i = 0; i < ep->m_ConnectStatus.GetCount(); ++i)
					{
						if(ep->m_ConnectStatus[i])
						{
							netAssert(ep->m_ConnectStatus[i]->Pending());
						}
					}
#endif
				}
			}
		}
	}
}
#endif

void
netRelay::Update()
{
	PROFILE

    SYS_CS_SYNC(s_MainCs);

    if(!IsInitialized())
    {
        return;
    }

#if SUPPORT_DTLS
	UpdateEndpoints();
#endif

    netRelayEvent* event;
    while(NULL != (event = s_EventConsumer.NextEvent()))
    {
        sm_Delegators[event->m_EventType].Dispatch(*event);
    }

	s_RelayServerInfo.m_NextSecureConnect.Update();
	s_PresenceServerInfo.m_NextSecureConnect.Update();

	bool isAnyOnline = rlRos::IsAnyOnline();
	bool canSendReceive = s_SocketManager && s_SocketManager->CanSendReceive();

    if(canSendReceive && isAnyOnline)
    {
        if(!s_RelayServerInfo.HasValidAddress()
            && s_RelayStarted
            && FetchRelayServerAddr())
        {
            netDebug("Got relay server address: " NET_ADDR_FMT,
                    NET_ADDR_FOR_PRINTF(s_RelayServerInfo.m_ServerAddr));

			s_RelayServerInfo.m_NextPing.InitSeconds(NET_DEFAULT_PING_INTERVAL_SEC);
			s_RelayServerInfo.m_NextPing.Stop();
		}

		if(s_RelayServerInfo.HasValidAddress())
		{
			if(s_RelayServerInfo.m_ConnectStatus.None()
				&& s_RelayServerInfo.m_NextSecureConnect.IsTimeToRetry())
			{
				s_RelayPresenceStats.IncNumRelayConnectionAttempts();
				Connect(s_RelayServerInfo.m_ServerAddr, IsUsingSecureRelays(), &s_RelayServerInfo.m_ConnectStatus);
			}
			else if(s_RelayServerInfo.m_ConnectStatus.Succeeded()
					&& s_RelayServerInfo.m_NextPing.IsStopped())
			{
				//Ping immediately once we've connected to the relay
				s_RelayServerInfo.m_NextPing.Start();
				s_RelayServerInfo.m_NextPing.ForceRetry();
			}
		}

#if SC_PRESENCE
        if(!s_PresenceServerInfo.HasValidAddress())
        {
            UpdatePresenceServerDiscovery();

            if(s_PresenceServerInfo.HasValidAddress())
            {
                netDebug("Got presence server address: " NET_ADDR_FMT,
                        NET_ADDR_FOR_PRINTF(s_PresenceServerInfo.m_ServerAddr));

 				s_PresenceServerInfo.m_NextPing.InitSeconds(NET_DEFAULT_PING_INTERVAL_SEC);
 				s_PresenceServerInfo.m_NextPing.Stop();
            }
        }

		if(s_PresenceServerInfo.HasValidAddress())
		{
			if(s_PresenceServerInfo.m_ConnectStatus.None()
				&& s_PresenceServerInfo.m_NextSecureConnect.IsTimeToRetry())
			{
				s_RelayPresenceStats.IncNumPresenceConnectionAttempts();
				Connect(s_PresenceServerInfo.m_ServerAddr, IsUsingSecurePresence(), &s_PresenceServerInfo.m_ConnectStatus);
			}
			else if(s_PresenceServerInfo.m_ConnectStatus.Succeeded()
				&& s_PresenceServerInfo.m_NextPing.IsStopped())
			{
				//Ping immediately once we've connected to the presence server
				s_PresenceServerInfo.m_NextPing.Start();
				s_PresenceServerInfo.m_NextPing.ForceRetry();
			}
		}
#endif
	}

	UpdateTelemetry();

    switch(s_State)
    {
	case RELAY_STATE_WAIT_FOR_SOCKET_MGR:
		if(canSendReceive)
		{
			netDebug("Socket is ready. Waiting for online....");
			SetRelayState(RELAY_STATE_WAIT_FOR_ONLINE);
		}
		break;
    case RELAY_STATE_WAIT_FOR_ONLINE:
        if(canSendReceive && isAnyOnline)
        {
#if !__NO_OUTPUT
			s_RelayAddrChangeTimer.InitMilliseconds(netRelayRNG.GetRanged(15 * 1000, 25 * 1000));
#endif

			s_MyRelayAddr.Clear();
			s_MyRelayToken.Clear();
            s_MyPresenceAddr.Clear();

			s_RelayRunningStartTime = sysTimer::GetSystemMsTime();
			SetRelayState(RELAY_STATE_RUNNING);
        }
        break;
    case RELAY_STATE_RUNNING:
        {
			static bool s_IsAnyOnline = false;
			const bool wasAnyOnline = s_IsAnyOnline;
			const bool wentOffline = wasAnyOnline && !isAnyOnline;
			s_IsAnyOnline = isAnyOnline;
			bool relayConnected = !wentOffline;
			bool presConnected = !wentOffline;

#if !__NO_OUTPUT
			if(PARAM_netrelaychangeaddr.Get())
			{
				s_RelayAddrChangeTimer.Update();
				if(s_RelayAddrChangeTimer.IsTimedOut())
				{
					s_RelayServerInfo.m_NumUnAckedPings = NET_RELAY_SERVER_MAX_UNACKED_PINGS + 1;
					s_RelayAddrChangeTimer.InitMilliseconds(netRelayRNG.GetRanged(15 * 1000, 25 * 1000));
				}						
			}
#endif

            if(s_RelayServerInfo.m_NumUnAckedPings > NET_RELAY_SERVER_MAX_UNACKED_PINGS)
            {
				netDebug("Number of un-acked relay pings exceeds the maximum of %d - restarting relay...",
                        NET_RELAY_SERVER_MAX_UNACKED_PINGS);

				s_RelayPresenceStats.SetFailedRelayServerAddr(s_RelayServerInfo.m_ServerAddr);
				s_RelayPresenceStats.IncNumFailedRelayConnections();

                //Try connecting to a different relay
                relayConnected = false;
            }

			if(s_RelayServerInfo.m_ConnectStatus.Failed())
			{
				// double the amount of time between retries each fail
				s_RelayServerInfo.m_NextSecureConnect.ScaleRange(200);
				s_RelayServerInfo.m_NextSecureConnect.Reset();
				if(s_RelayServerInfo.m_NextSecureConnect.GetMillisecondsUntilRetry() > (NET_RELAY_DTLS_MAX_RECONNECT_TIMEOUT_SEC * 1000))
				{
					s_RelayServerInfo.m_NextSecureConnect.InitMilliseconds(NET_RELAY_DTLS_MAX_RECONNECT_TIMEOUT_SEC * 1000);
					s_RelayServerInfo.m_NextSecureConnect.Reset();
				}
				netDebug("Connecting to the relay failed - will retry in %u ms...", s_RelayServerInfo.m_NextSecureConnect.GetMillisecondsUntilRetry());

				//Try connecting to a different relay
				relayConnected = false;
			}

			if(wentOffline || !canSendReceive || !isAnyOnline)
			{
				s_RelayGeolocInfo.Clear();
				s_RelayServerInfo.m_NextSecureConnect.InitMilliseconds(NET_RELAY_DTLS_MIN_RECONNECT_TIMEOUT_SEC * 1000);
				s_RelayServerInfo.m_NextSecureConnect.ForceRetry();

				s_PresenceServerList.Clear();
				s_PresenceServerInfo.m_NextSecureConnect.InitMilliseconds(NET_RELAY_DTLS_MIN_RECONNECT_TIMEOUT_SEC * 1000);
				s_PresenceServerInfo.m_NextSecureConnect.ForceRetry();
			}

			if(s_PresenceServerInfo.m_ConnectStatus.Failed())
			{
				// double the amount of time between retries each fail
				s_PresenceServerInfo.m_NextSecureConnect.ScaleRange(200);
				s_PresenceServerInfo.m_NextSecureConnect.Reset();
				if(s_PresenceServerInfo.m_NextSecureConnect.GetMillisecondsUntilRetry() > (NET_RELAY_DTLS_MAX_RECONNECT_TIMEOUT_SEC * 1000))
				{
					s_PresenceServerInfo.m_NextSecureConnect.InitMilliseconds(NET_RELAY_DTLS_MAX_RECONNECT_TIMEOUT_SEC * 1000);
					s_PresenceServerInfo.m_NextSecureConnect.Reset();
				}
				netDebug("Connecting to the presence server failed - will retry in %u ms...", s_PresenceServerInfo.m_NextSecureConnect.GetMillisecondsUntilRetry());

				//Try reconnecting to the presence server
				presConnected = false;
			}

            if(!canSendReceive || !isAnyOnline)
            {
                netDebug("Went offline...");
#if SUPPORT_DTLS
				// InitEndpoints sends a shutdown packet to all endpoints we're connected to
				InitEndpoints();
#endif
				SetRelayState(RELAY_STATE_WAIT_FOR_ONLINE);
                relayConnected = presConnected = false;
            }

			if(!canSendReceive)
			{
				netDebug("Hardware unavailable");
				SetRelayState(RELAY_STATE_WAIT_FOR_SOCKET_MGR);
			}

            if(!relayConnected)
            {
				s_LastRelayServerInfo = s_RelayServerInfo;
				netDebug("Relay disconnected - reconnecting...");
				//Restart the relay.
				StopRelay();
				StartRelay();
            }
            
#if SC_PRESENCE
            if(!presConnected)
            {
                netDebug("Presence disconnected - reconnecting...");
                s_PresenceServerInfo.ClearAddr();
                //Restart the presence pings.
                s_PresenceServerInfo.m_DiscoveryRetryTimer.ForceRetry();
            }
#endif

            if(relayConnected || presConnected)
            {
				netRelay::UpdatePing();
            }
        }
        break;
	default:
			break;
    }
}

unsigned
netRelay::GetMinSupportedPortBindingTimeoutSec()
{
	return NET_MIN_PORT_BINDING_TIMEOUT_SEC;
}

unsigned
netRelay::GetMsSinceLastRelayPing()
{
	return (s_RelayServerInfo.m_LastPingTimeMs != 0) ?
			sysTimer::GetSystemMsTime() - s_RelayServerInfo.m_LastPingTimeMs :
			0;
}

unsigned
netRelay::GetMsSinceLastRelayPong()
{
	return (s_RelayServerInfo.m_LastPongTimeMs != 0) ?
			sysTimer::GetSystemMsTime() - s_RelayServerInfo.m_LastPongTimeMs :
			0;
}

unsigned
netRelay::GetMsSinceLastRelayTerminus()
{
	return (s_RelayServerInfo.m_LastTerminusTimeMs != 0) ?
			sysTimer::GetSystemMsTime() - s_RelayServerInfo.m_LastTerminusTimeMs :
			0;
}

unsigned
netRelay::GetMsSinceLastPresencePing()
{
	return (s_PresenceServerInfo.m_LastPingTimeMs != 0) ?
			sysTimer::GetSystemMsTime() - s_PresenceServerInfo.m_LastPingTimeMs :
			0;
}

void
netRelay::ForceRefreshRelayMapping()
{
	if(s_RelayStarted
		&& (s_RelayServerInfo.m_NextPing.IsStopped() == false)
		&& (s_RelayServerInfo.m_LastPingTimeMs > 0)
		&& (netRelay::GetMsSinceLastRelayPing() >= (netRelay::GetMinSupportedPortBindingTimeoutSec() * 1000)))
	{
		s_RelayServerInfo.m_NextPing.ForceRetry();
	}
}

unsigned
netRelay::GetPortBindingTimeoutSec()
{
	//Ping interval starts off small when we first go online and gradually
	//increases.  As soon as we get a pong we set the ping interval to the minimum supported UDP port binding timeout.
	//As the NAT detector determines that the NAT keeps the port open longer, we increase the time between pings.
	unsigned portBindingTimeoutSec = NET_DEFAULT_PING_INTERVAL_SEC;

	if(netNatDetector::GetAllowAdjustableUdpSendInterval())
	{
		portBindingTimeoutSec = NET_MIN_PORT_BINDING_TIMEOUT_SEC;

		const netNatInfo& natInfo = netNatDetector::GetLocalNatInfo();
		unsigned timeSinceStarted = sysTimer::GetSystemMsTime() - s_RelayRunningStartTime;

		if(natInfo.GetUdpTimeoutState() == NAT_UDP_TIMEOUT_SUCCEEDED)
		{
			portBindingTimeoutSec = natInfo.GetUdpTimeoutSec();
			if(portBindingTimeoutSec == 0)
			{
				portBindingTimeoutSec = NET_DEFAULT_PING_INTERVAL_SEC;
			}
		}
		else if(natInfo.GetUdpTimeoutState() == NAT_UDP_TIMEOUT_FAILED)
		{
			// if we could not determine the UDP session timeout, fall back
			// to the highest supported ping frequency. This will send more 
			// pings to the relay server from a small percentage of players
			// in exchange for improved p2p connectivity.
			portBindingTimeoutSec = NET_MIN_PORT_BINDING_TIMEOUT_SEC;
		}
		else if(timeSinceStarted > (NET_MAX_TIME_TO_DETECT_PING_INTERVAL_SEC * 1000))
		{
			portBindingTimeoutSec = NET_MIN_PORT_BINDING_TIMEOUT_SEC;
		}
		else if(natInfo.GetUdpTimeoutState() == NAT_UDP_TIMEOUT_IN_PROGRESS)
		{
			if(natInfo.GetUdpTimeoutSec() > NET_MIN_PORT_BINDING_TIMEOUT_SEC)
			{
				portBindingTimeoutSec = natInfo.GetUdpTimeoutSec();
			}
		}
	}

	return portBindingTimeoutSec;
}

unsigned
netRelay::GetRelayPingIntervalMs()
{
	// check for command line override
    int pingInterval;
    if(PARAM_netrelaypinginterval.Get(pingInterval))
    {
        return pingInterval * 1000;
    }

	// check for tunable override
	if(s_TunableRelayPingIntervalMs > 0)
	{
		// don't allow extremely high-frequency pinging
		return Max(s_TunableRelayPingIntervalMs, (NET_MIN_PORT_BINDING_TIMEOUT_SEC * 1000) / 2);
	}

	unsigned portBindingTimeoutSec = GetPortBindingTimeoutSec();
	unsigned portBindingTimeoutMs = (portBindingTimeoutSec * 1000) - s_PingIntervalAdjustmentMs;
	return Clamp(portBindingTimeoutMs, NET_MIN_PORT_BINDING_TIMEOUT_SEC * 1000, NET_RELAY_MAXIMUM_PING_INTERVAL_SEC * 1000);
}

unsigned
netRelay::GetPresencePingIntervalMs()
{
	int pingInterval;
	if(PARAM_netrelaypinginterval.Get(pingInterval))
	{
		return pingInterval * 1000;
	}
	
	unsigned portBindingTimeoutSec = GetPortBindingTimeoutSec();
	unsigned portBindingTimeoutMs = (portBindingTimeoutSec * 1000) - s_PingIntervalAdjustmentMs;
	return Clamp(portBindingTimeoutMs, NET_MIN_PORT_BINDING_TIMEOUT_SEC * 1000, (unsigned)NET_PRESENCE_MAXIMUM_PING_INTERVAL_SEC * 1000);
}

unsigned 
netRelay::GetNumRelayAddrChanges()
{
	return s_RelayPresenceStats.GetNumRelayAddrChanges();
}

unsigned 
netRelay::GetNumRelayMappedAddrChanges()
{
	return s_RelayPresenceStats.GetNumRelayMappedAddrChanges();
}

netSocket*
netRelay::GetSocket()
{
	return s_SocketManager->GetMainSocket();
}

u16
netRelay::GetRequestedPort()
{
	return s_SocketManager->GetRequestedPort();
}

const netAddress&
netRelay::GetMyRelayAddress()
{
    SYS_CS_SYNC(s_MainCs);
    return netVerify(IsInitialized()) ? s_MyRelayAddr : netAddress::INVALID_ADDRESS;
}

const netRelayToken& 
netRelay::GetMyRelayToken()
{
	return s_MyRelayToken;
}

const netSocketAddress&
netRelay::GetMyPresenceAddress()
{
    SYS_CS_SYNC(s_MainCs);
    return netVerify(IsInitialized()) ? s_MyPresenceAddr : netSocketAddress::INVALID_ADDRESS;
}

const netSocketAddress&
netRelay::GetRelayServerAddress()
{
    SYS_CS_SYNC(s_MainCs);
    return netVerify(IsInitialized()) ? s_RelayServerInfo.m_ServerAddr : netSocketAddress::INVALID_ADDRESS;
}

const netSocketAddress&
netRelay::GetPresenceServerAddress()
{
	SYS_CS_SYNC(s_MainCs);
	return netVerify(IsInitialized()) ? s_PresenceServerInfo.m_ServerAddr : netSocketAddress::INVALID_ADDRESS;
}

#if SUPPORT_DTLS && DTLS_DEBUGGING
static void SslLogCb(const int logLevel, const char *const OUTPUT_ONLY(logMessage))
{
	// disable spurious output. Code -323 is WANT_READ.
	if ((strstr(logMessage, "-323") == nullptr) &&
		(strstr(logMessage, "Embed Receive error") == nullptr) &&
		(strstr(logMessage, "Would block") == nullptr))
	{
		if (ERROR_LOG == logLevel)
		{
			netError("wolfSSL: %s", logMessage);
		}
		else if (INFO_LOG == logLevel
			|| OTHER_LOG == logLevel)
		{
			netDebug("wolfSSL: %s", logMessage);
		}
	}
}
#endif

#if SUPPORT_DTLS
int netRelay::SslReceiveCb(WOLFSSL* /*ssl*/, char* buffer, int NET_ASSERTS_ONLY(bufferSize), void* NET_ASSERTS_ONLY(ctx))
{
	SYS_CS_SYNC(s_MainCs);
	
	// When data arrives from a DTLS relay or presence server, it is dispatched to netRelay::Accept().
	// netRelay::Accept() calls SSL_read on the receive thread, which in turn calls this function to acquire the data.
	// We push data to SSL rather than having it pull data directly from the socket, since we mux multiple data
	// streams onto the same socket.

	// When connecting to a DTLS server, the SSL library calls this function from the main thread to check for
	// pending data. In that case, we say there is no pending data and return. When connection handshake packets
	// arrive, we push them to the SSL library in the same manner as all other DTLS packets - on the receive thread.

	if((s_RcvdPktLen <= 0) ||
		(s_SocketManager == nullptr) ||
		(netRelay::GetCurrentThreadId() != s_SocketManager->GetReceiveThreadId()))
	{
		return WOLFSSL_CBIO_ERR_WANT_READ;
	}

#if USE_NET_ASSERTS
	netDtlsEndpoint* ep = (netDtlsEndpoint*)ctx;
	netAssert(ep);
	netAssert(s_RcvdPktSender == ep->m_Addr);
#endif

	const int recvd = s_RcvdPktLen;
	netAssert(s_RcvdPktLen <= bufferSize);
	sysMemCpy(buffer, s_RcvdPktBuf, s_RcvdPktLen);
	s_RcvdPktLen = 0;

	return recvd;
}
#endif

bool
netRelay::Accept(const netSocketAddress& sender,
					void *buffer,
					const int bufferSize)
{
	PROFILE

	bool consumed = false;
	void* finalBuffer = nullptr;
	int finalBufferSize = 0;
	u8 decryptedBuf[netSocket::MAX_BUFFER_SIZE + 1];

	if(bufferSize > 0)
	{
		SYS_CS_SYNC(s_MainCs);

		// Relay server, presence server, and direct p2p packets all arrive from the same socket.
		// We need to determine whether this incoming packet is from a relay or presence server,
		// and furthermore, whether the packet is DTLS encrypted.

		// a relay packet is any packet that was sent from a relay server, including p2p terminus packets.
		// We receive packets from our connected relay server, our connected presence server,
		// and NAT traversal can send pings and receive pongs from any (non-DTLS) relay server.
		bool isRelayPacket = IsRelayAddress(sender);
		bool isPresencePacket = !isRelayPacket && IsPresenceAddress(sender);
		bool isRelayPong = !isRelayPacket && !isPresencePacket && IsRelayPong(buffer, bufferSize);
		bool isSecure = (isRelayPacket && IsUsingSecureRelays()) || (isPresencePacket && IsUsingSecurePresence());
		netDtlsEndpoint* endpoint = nullptr;

		if(!isSecure && IsUsingSecureRelays())
		{
			// IsRelayAddress() only checks whether the packet is from our local relay.
			// The one time we get packets from a different DTLS relay is when we're connecting to a
			// remote peer's relay using DTLS. Check whether a DTLS endpoint exists for this sender.
			endpoint = GetEndpointByAddr(sender);
			isRelayPacket = isSecure = endpoint != nullptr;
		}

		if(!isRelayPacket && !isPresencePacket && !isRelayPong)
		{
			// the packet is not from a relay or presence server
			return false;
		}

		consumed = true;

		if(isSecure)
		{
			if(!endpoint)
			{
				endpoint = GetCreateEndpoint(sender);
			}

			if(!endpoint)
			{
				return consumed;
			}

			if(endpoint->m_SslState == netDtlsEndpoint::STATE_CONNECTED)
			{
#if RSG_BANK
				// If presence packet, ensure we're not ignoring presence pongs
				// If relay packet, ensure we're not ignoring relay pongs.
				// If neither relay or presence
				if((isPresencePacket && !sm_bIgnorePresencePongs) || (isRelayPacket && !sm_bIgnoreRelayPongs) || (!isRelayPacket && !isPresencePacket))
#endif
				{
					// check if we've timed out here since a long stall (eg. an netAssert) could cause us to time out
					// and when we start processing packets again, we may end up processing packet that had 
					// arrived prior to the stall, but the server will have already timed out the connection
					endpoint->m_Timeout.Update();
					if(endpoint->m_Timeout.IsTimedOut() == false)
					{
						endpoint->m_Timeout.InitSeconds(NET_RELAY_DTLS_CXN_TIMEOUT_SEC);
					}
				}
			}

			if((endpoint->m_SslState != netDtlsEndpoint::STATE_CONNECTED))
			{
				endpoint->m_ResendTimeout.Update();
				if(endpoint->m_ResendTimeout.IsTimedOut() == false)
				{
					netDebug3("ReceiveData: resetting resend timeout for " NET_ADDR_FMT " to %d ms", NET_ADDR_FOR_PRINTF(endpoint->m_Addr), NET_RELAY_DTLS_CONNECT_RESEND_INTERVAL_MS);
					endpoint->m_ResendTimeout.InitMilliseconds(NET_RELAY_DTLS_CONNECT_RESEND_INTERVAL_MS);
				}
			}

			// Copy the pointer to the encrypted buffer. SSL_read will call our
			// SSL receive callback, from which we will pass this encrypted buffer.
			s_RcvdPktLen = bufferSize;
			s_RcvdPktBuf = (u8*)buffer;

#if USE_NET_ASSERTS
			s_RcvdPktSender = sender;
#endif

			finalBuffer = decryptedBuf;

			{
				PROFILER_EVENT("SSL_read");
				finalBufferSize = wolfSSL_read(endpoint->m_Ssl, decryptedBuf, sizeof(decryptedBuf) - 1);
			}
		}
		else
		{
			finalBuffer = buffer;
			finalBufferSize = bufferSize;
		}
	}

	// OnReceive is purposely outside of the above critical section
	if(finalBufferSize > 0)
	{
		OnReceive(sender, finalBuffer, finalBufferSize);
	}

	return consumed;
}

#if SUPPORT_DTLS
int
netRelay::SslSendCb(WOLFSSL* /*ssl*/, char *data, int length, void *ctx)
{
	SYS_CS_SYNC(s_MainCs);

	netDtlsEndpoint* ep = (netDtlsEndpoint*)ctx;
	netAssert(ep);

	netSocketError sktErr = NET_SOCKERR_UNKNOWN;

	bool success = s_SocketManager->Send(ep->m_Addr, data, length, &sktErr);

	if((ep->m_SslState != netDtlsEndpoint::STATE_CONNECTED))
	{
		ep->m_ResendTimeout.Update();
		if(ep->m_ResendTimeout.IsTimedOut() == false)
		{
			netDebug3("SslSendCb: resetting resend timeout for " NET_ADDR_FMT " to %d ms", NET_ADDR_FOR_PRINTF(ep->m_Addr), NET_RELAY_DTLS_CONNECT_RESEND_INTERVAL_MS);
			ep->m_ResendTimeout.InitMilliseconds(NET_RELAY_DTLS_CONNECT_RESEND_INTERVAL_MS);
		}
	}

	if(success)
	{
		sktErr = NET_SOCKERR_NONE;
		return length;
	}

	if(sktErr == NET_SOCKERR_WOULD_BLOCK)
	{
		return WOLFSSL_CBIO_ERR_WANT_WRITE;
	}
	else if(sktErr == NET_SOCKERR_CONNECTION_RESET)
	{
		return WOLFSSL_CBIO_ERR_CONN_RST;
	}
	else
	{
		return WOLFSSL_CBIO_ERR_GENERAL;
	}
}
#endif

bool
netRelay::SendData(const netSocketAddress& destination,
					const bool isSecure,
					const void *data,
					const unsigned length,
					netSocketError* sktErr)
{
	PROFILE

	if(data == NULL)
	{
		NET_SET_SKTERR(sktErr, NET_SOCKERR_UNKNOWN);
		return false;
	}

#if SUPPORT_DTLS
	if(isSecure)
	{
		SYS_CS_SYNC(s_MainCs);

		NET_SET_SKTERR(sktErr, NET_SOCKERR_NONE);

		netDtlsEndpoint* endpoint = GetCreateEndpoint(destination);
		if((endpoint == NULL) || (endpoint->m_Ssl == NULL))
		{
			NET_SET_SKTERR(sktErr, NET_SOCKERR_NO_BUFFERS);
			return false;
		}

		if(endpoint->m_SslState != netDtlsEndpoint::STATE_CONNECTED)
		{
			// there are cases where the game sends a packet to a relay server
			// before connecting to it. For example, if a player A sends a join
			// request to us over our relay server R1, we immediately reply back
			// with a response to his relay server R2, which we may not be
			// connected to yet. We don't have an opportunity to connect
			// to R2 in advance.
			netDebug("Sending to secure endpoint " NET_ADDR_FMT " we haven't yet connected to", NET_ADDR_FOR_PRINTF(endpoint->m_Addr));

			Connect(destination, isSecure, NULL);

			// returning true here even though we didn't send the packet
			// because the connection manager will close a connection if it gets
			// an error back.
			return true;
		}

		const int len = wolfSSL_write(endpoint->m_Ssl, data, length);
		if(len != (int)length)
		{
			int sslErr = wolfSSL_get_error(endpoint->m_Ssl, 0);
			if(sslErr == SSL_ERROR_WANT_READ || sslErr == SSL_ERROR_WANT_WRITE)
			{
				NET_SET_SKTERR(sktErr, NET_SOCKERR_WOULD_BLOCK);
			}
			else
			{
				NET_SET_SKTERR(sktErr, NET_SOCKERR_UNKNOWN);
			}

			return false;
		}

		return true;
	}
	else
	{
		return s_SocketManager->Send(destination, data, length, sktErr);
	}
#else
	(void)isSecure;
	return s_SocketManager->Send(destination, data, length, sktErr);
#endif
}

#if SUPPORT_DTLS
bool
netRelay::InitSslCtx()
{
	SYS_CS_SYNC(s_MainCs);

	if(netRelayGlobals::s_RelaySslCtx == NULL)
	{
		wolfSSL_Init();

		// the relay/presence server doesn't support DTLS v1.2
		if((netRelayGlobals::s_RelaySslCtx = wolfSSL_CTX_new(wolfDTLSv1_client_method())) == NULL)
		{
			netError("Error creating SSL context");
			return false;
		}

		//wolfSSL_CTX_set_cipher_list(netRelayGlobals::s_RelaySslCtx, "DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:AES256-SHA256");
		
#if DTLS_VERIFY_PEER
		//Verify peer will require the server to give us a certificate
		//and will automatically fail the connection if it isn't signed
		//by any of our CAs loaded below.
		//Additionally the domain we're connecting to is validated against
		//the domain in the cert via wolfSSL_check_domain_name.
		wolfSSL_CTX_set_verify(netRelayGlobals::s_RelaySslCtx, SSL_VERIFY_PEER, NULL);

		//Load our CAs.
		const int loadCAResult = wolfSSL_CTX_load_verify_buffer(netRelayGlobals::s_RelaySslCtx, (const unsigned char*)s_ServerCertificate, (long)sizeof(s_ServerCertificate) - 1, SSL_FILETYPE_PEM);

		if(loadCAResult != SSL_SUCCESS)
		{
			netError("Error loading ros CA buffer: %d", loadCAResult);
			return false;
		}
#else
		wolfSSL_CTX_set_verify(netRelayGlobals::s_RelaySslCtx, SSL_VERIFY_NONE, 0);
#endif

 		wolfSSL_SetIORecv(netRelayGlobals::s_RelaySslCtx, SslReceiveCb);
 		wolfSSL_SetIOSend(netRelayGlobals::s_RelaySslCtx, SslSendCb);

#if DTLS_DEBUGGING
		wolfSSL_SetLoggingCb(SslLogCb);
		wolfSSL_Debugging_ON();
#endif

		InitEndpoints();
	}

	return true;
}

void
netRelay::FreeSslCtx()
{
	SYS_CS_SYNC(s_MainCs);

	if(netRelayGlobals::s_RelaySslCtx == NULL)
	{
		wolfSSL_CTX_free(netRelayGlobals::s_RelaySslCtx);
	}
}

void  
netRelay::InitEndpoints()
{
	SYS_CS_SYNC(s_MainCs);

	s_FreeEndpoints.clear();
	s_ActiveEndpoints.clear();

	for(unsigned i = 0; i < MAX_DTLS_ENDPOINTS; ++i)
	{
		if(s_EndpointsPool[i].m_Ssl)
		{
			if(s_EndpointsPool[i].m_SslState == netDtlsEndpoint::STATE_CONNECTED)
			{
				// try to send a graceful shutdown without waiting
				wolfSSL_shutdown(s_EndpointsPool[i].m_Ssl);
			}

			wolfSSL_free(s_EndpointsPool[i].m_Ssl);
			s_EndpointsPool[i].m_Ssl = NULL;
		}
		s_EndpointsPool[i].Clear();
		s_FreeEndpoints.push_back(&s_EndpointsPool[i]);
	}
}

netDtlsEndpoint* 
netRelay::AllocateEndpoint(const netSocketAddress& addr)
{
	SYS_CS_SYNC(s_MainCs);

	bool success = false;
	netDtlsEndpoint* ep = NULL;
	rtry
	{
		netDebug3("Allocating secure endpoint [" NET_ADDR_FMT "]", NET_ADDR_FOR_PRINTF(addr));

#if DTLS_VERIFY_PEER
		rcheck(g_rlTitleId, catchall, netWarning("Cannot create endpoint - g_rlTitleId is nullptr"));

		const char* env = g_rlTitleId->m_RosTitleId.GetEnvironmentName();
		rcheck(env, catchall, netWarning("Cannot create endpoint - env is nullptr"));

		const char* baseDomainName = g_rlTitleId->m_RosTitleId.GetRosBaseDomainName();
		rcheck(baseDomainName, catchall, netWarning("Cannot create endpoint - baseDomainName is nullptr"));
#endif

		rverify(InitSslCtx(), catchall, );

		rverify(s_FreeEndpoints.empty() == false, catchall, netError("Out of free endpoints"));

		ep = s_FreeEndpoints.front();
		rverify(ep, catchall, );
		s_FreeEndpoints.pop_front();
		s_ActiveEndpoints.push_back(ep);

		netDebug3("Allocated secure endpoint. Num Active endpoints: %" SIZETFMT "u, Num Free Endpoints: %" SIZETFMT "u", s_ActiveEndpoints.size(), s_FreeEndpoints.size());
		netAssert((s_ActiveEndpoints.size() + s_FreeEndpoints.size()) == MAX_DTLS_ENDPOINTS);

		ep->m_Ssl = wolfSSL_new(netRelayGlobals::s_RelaySslCtx);
		rverify(ep->m_Ssl != NULL, catchall, netError("Could not allocate SSL object"));

#if DTLS_VERIFY_PEER
		char domainName[256] = {0};
		formatf(domainName, "%s.relay.%s", env, baseDomainName);
		wolfSSL_check_domain_name(ep->m_Ssl, domainName);
#endif

		ep->m_Addr = addr;

		wolfSSL_SetIOWriteCtx(ep->m_Ssl, (void*)ep);
		wolfSSL_SetIOReadCtx(ep->m_Ssl, (void*)ep);
		wolfSSL_dtls_set_using_nonblock(ep->m_Ssl, 1);

		success = true;
	}
	rcatchall
	{
	}

	if((success == false) && (ep != NULL))
	{
		FreeEndpoint(ep);
		ep = NULL;
	}

	return ep;
}

void  
netRelay::FreeEndpoint(netDtlsEndpoint* ep)
{
	SYS_CS_SYNC(s_MainCs);

	if(!netVerify(ep))
	{
		return;
	}

	ep->Clear();
	s_ActiveEndpoints.erase(ep);
	s_FreeEndpoints.push_back(ep);
	netAssert((s_ActiveEndpoints.size() + s_FreeEndpoints.size()) == MAX_DTLS_ENDPOINTS);

	netDebug3("Removed secure endpoint and added to free list. "
			 "Num Active Endpoints: %" SIZETFMT "u, Num Free Endpoints: %" SIZETFMT "u",
			 s_ActiveEndpoints.size(), s_FreeEndpoints.size());
}

netDtlsEndpoint*
netRelay::GetEndpointByAddr(const netSocketAddress& addr)
{
	SYS_CS_SYNC(s_MainCs);

	EndpointList::iterator it = s_ActiveEndpoints.begin();
	EndpointList::const_iterator stop = s_ActiveEndpoints.end();
	for(; stop != it; ++it)
	{
		netDtlsEndpoint* ep = *it;
		if(ep->m_Addr == addr)
		{
			return ep;
		}
	}

	return NULL;
}

netDtlsEndpoint*
netRelay::GetCreateEndpoint(const netSocketAddress& addr)
{
	SYS_CS_SYNC(s_MainCs);

	netDtlsEndpoint* ep = GetEndpointByAddr(addr);
	if(ep)
	{
		return ep;
	}

	return AllocateEndpoint(addr);
}
#endif

bool
netRelay::IsUsingSecureRelays()
{
#if !__NO_OUTPUT
	if(PARAM_netrelayserveraddr.Get())
	{
		return PARAM_netrelaysecure.Get();
	}
#endif

	return s_RelayGeolocInfo.m_IsSecure;
}

bool
netRelay::IsUsingSecurePresence()
{
#if RL_FORCE_STAGE_ENVIRONMENT
    // this is only required for stage-cert
    if(rlIsForcedEnvironment())
    {
        return true;
    }
#endif

#if !__NO_OUTPUT
	if(PARAM_netpresenceserveraddr.Get())
	{
		return PARAM_netpresencesecure.Get();
	}
#endif

	return s_PresenceServerList.m_IsSecure;
}

bool
netRelay::IsRelayAddress(const netSocketAddress& addr)
{
	return addr.IsValid() &&
			((s_RelayServerInfo.m_ServerAddr == addr) ||
			(s_LastRelayServerInfo.m_ServerAddr == addr));
}

bool
netRelay::IsPresenceAddress(const netSocketAddress& addr)
{
	return addr.IsValid() &&
			(s_PresenceServerInfo.m_ServerAddr == addr);
}

bool 
netRelay::IsRelayPong(const void* pktBuf,
					  const int len)
{
	rtry
	{
		// pongs are smaller than any p2p packet, so most of the time, this check
		// is all that's needed to prune out non-pong packets
		rcheck(len == netRelayPacket::SIZEOF_PING_HEADER ||
			   len == netRelayPacket::SIZEOF_PING2_HEADER, catchall,);

		u8 cpyBuf[MAX(netRelayPacket::SIZEOF_PING_HEADER, netRelayPacket::SIZEOF_PING2_HEADER)];
		memcpy(cpyBuf, pktBuf, len);

		netRelayPacket* relayPkt = static_cast<netRelayPacket*>((void*)cpyBuf);
		rcheck((int)relayPkt->GetSize() == len,	catchall,);
        relayPkt->Unmix();
        rcheck(relayPkt->GetVersion() == netRelayPacket::PROTOCOL_VERSION, catchall,);
        rcheck(relayPkt->IsPong() || relayPkt->IsPong2(), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool 
netRelay::IsConnectedToRelay()
{
	SYS_CS_SYNC(s_MainCs);
	if (!s_RelayServerInfo.HasValidAddress())
	{
		return false;
	}

	return IsUsingSecureRelays() ? s_RelayServerInfo.m_ConnectStatus.Succeeded() : true;
}

bool
netRelay::IsReadyForMultiplayerTimedOut()
{
	return ((s_State == RELAY_STATE_RUNNING) &&
			((sysTimer::GetSystemMsTime() - s_RelayRunningStartTime) > (NET_MAX_TIME_UNTIL_READY_FOR_MP_SEC * 1000)));
}

bool 
netRelay::IsReadyForMultiplayer()
{
	// if s_AllowMultiplayerWithoutRelayServer == false, don't allow entry to muliplayer without a relay server connection.
	// if s_AllowMultiplayerWithoutRelayServer == true, prefer to connect to relay before entering multiplayer but don't
	// block access to multiplayer if we fail to connect.

	const bool allowMultiplayerWithoutRelayServer = s_AllowMultiplayerWithoutRelayServer && IsReadyForMultiplayerTimedOut();

	return NET_DISABLE_RELAY_SERVER ||
			((s_SocketManager && s_SocketManager->CanSendReceive()) &&
			(s_MyRelayAddr.IsRelayServerAddr() || allowMultiplayerWithoutRelayServer));
}

bool
netRelay::IsConnectedToPresence()
{
	SYS_CS_SYNC(s_MainCs);
	if (!s_PresenceServerInfo.HasValidAddress())
	{
		return false;
	}

	return IsUsingSecurePresence() ? s_PresenceServerInfo.m_ConnectStatus.Succeeded() : true;
}

unsigned
netRelay::GetNumUnackedPresencePings()
{
	return s_PresenceServerInfo.m_NumUnAckedPings;
}

bool 
netRelay::Connect(const netSocketAddress& addr, const bool isSecure, netStatus* status)
{
	PROFILE

	SYS_CS_SYNC(s_MainCs);

	rtry
	{
		rverify((status == NULL) || !status->Pending(), catchall, );
		rverify(addr.IsValid(), catchall, );

#if SUPPORT_DTLS
		if(isSecure)
		{
			netDtlsEndpoint* ep = GetCreateEndpoint(addr);
			rverify(ep, catchall, );

			if(ep->m_SslState == netDtlsEndpoint::STATE_CONNECTED)
			{
				netDebug("netRelay already connected to: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr));

				if(status)
				{
					status->ForceSucceeded();
				}
			}
			else
			{
				netDebug("netRelay connecting to: " NET_ADDR_FMT ". Current state is %d", NET_ADDR_FOR_PRINTF(addr), ep->m_SslState);

				// queues up another connect if we're currently disconnecting
				ep->m_Connect = true;

				if(status)
				{
					if(netVerify(ep->m_ConnectStatus.Find(status) < 0))
					{
						// multiple callers can listen for connection status
						ep->m_ConnectStatus.PushAndGrow(status);
						status->SetPending();

						netDebug("netRelay added status object to endpoint: " NET_ADDR_FMT ". Status count is now %d", NET_ADDR_FOR_PRINTF(addr), ep->m_ConnectStatus.GetCount());
					}
				}
			}
		}
		else
		{
			netDebug("netRelay connected to: " NET_ADDR_FMT ". Not using DTLS so we're already 'connected'.", NET_ADDR_FOR_PRINTF(addr));

			if(status)
			{
				status->ForceSucceeded();
			}
		}
#else
		(void)isSecure;
		netDebug("netRelay connected to: " NET_ADDR_FMT ". Not using DTLS so we're already 'connected'.", NET_ADDR_FOR_PRINTF(addr));

		if(status)
		{
			status->ForceSucceeded();
		}
#endif

		return true;
	}
	rcatchall
	{

	}

	if(status)
	{
		if(status->Pending())
		{
			status->SetFailed();
		}
		else
		{
			status->ForceFailed();
		}
	}

	netDebug("netRelay::Connect() returning false. Addr: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(addr));

	return false;
}

void
netRelay::CancelConnect(netStatus* status)
{
	SYS_CS_SYNC(s_MainCs);

	if(status == NULL)
	{
		return;
	}

#if SUPPORT_DTLS
	EndpointList::iterator it = s_ActiveEndpoints.begin();
	EndpointList::iterator next = it;
	EndpointList::const_iterator stop = s_ActiveEndpoints.end();

	for(++next; stop != it; it = next, ++next)
	{
		netDtlsEndpoint* ep = *it;

		int index = ep->m_ConnectStatus.Find(status);
		if(index >= 0)
		{
			netAssert(ep->m_ConnectStatus[index] == status);

			// we allow the connection to continue but unhook the
			// caller's status object so we don't write to it later
			status->SetCanceled();
			ep->m_ConnectStatus.Delete(index);
			return;
		}
	}
#endif
}

unsigned 
netRelay::GetMaxPacketSize()
{
	return IsUsingSecureRelays() ?
			(netRelayPacket::MAX_SIZEOF_RELAY_PACKET - MAX_SIZEOF_DTLS_OVERHEAD) : 
			(netRelayPacket::MAX_SIZEOF_RELAY_PACKET);
}

netSocketError
netRelay::Send(const netAddress& addr,
                netRelayPacket* relayPkt,
                const unsigned sizeofPayload)
{
	PROFILE

    SYS_CS_SYNC(s_MainCs);

    netSocketError sktErr = NET_SOCKERR_UNKNOWN;

	rtry
	{
		rverify(IsInitialized(),
				catchall,
				netError("Send :: netRelay not initialized"));

		rverify(addr.IsRelayServerAddr(),
				catchall,
				netError("Send :: [" NET_ADDR_FMT "] is not a valid relay address", NET_ADDR_FOR_PRINTF(addr)));
		
		rverify(sizeofPayload > 0,
				catchall,
				netError("Send :: No payload"));

		// skip when local relay address not valid, we will be in the process of attempting
		// to reconnect and don't want to drop connections in the interim
		// normal timeout rules will apply here (unacked sends / no data received)
		if(!s_MyRelayAddr.IsRelayServerAddr())
		{
			netWarning("Send :: Invalid local relay address, ignoring");
			return NET_SOCKERR_NONE;
		}

		if(!s_SocketManager->CanSendReceive())
		{
			netError("Send :: Can't send on socket");
			return NET_SOCKERR_INVALID_SOCKET;
		}

		relayPkt->ResetP2pHeader(sizeofPayload,
								 addr,
								 s_MyRelayAddr,
								 s_MyRelayToken);

		const unsigned sizeofPkt = relayPkt->GetSize();
				
		if(!netVerify(sizeofPkt <= GetMaxPacketSize()))
		{
			netError("Send :: Message too long");
			sktErr = NET_SOCKERR_MESSAGE_TOO_LONG;
		}
		else
		{
			netDebug3("[" NET_ADDR_FMT "]:PKT->,sz:%d",
					  NET_ADDR_FOR_PRINTF(addr),
					  relayPkt->GetSize());

			relayPkt->Mix();

			const netSocketAddress& dstAddr = addr.GetProxyAddress();

			if(!SendData(dstAddr, IsUsingSecureRelays(), relayPkt, sizeofPkt, &sktErr))
			{
				//Ignore SOCKERR_CONNECTION_RESET.
				if(NET_SOCKERR_CONNECTION_RESET == sktErr)
				{
					sktErr = NET_SOCKERR_NONE;
				}
			}
		}
	}
	rcatchall
	{

	}

    return sktErr;
}

//private:

bool
netRelay::FetchRelayServerAddr()
{
    if(PARAM_netrelaydisable.Get())
    {
        return false;
    }

    SYS_CS_SYNC(s_MainCs);

	if(!rlRos::IsAnyOnline())
	{
		return false;
	}

	netSocketAddress oldRelayAddr = s_LastRelayServerInfo.m_ServerAddr;

    s_RelayServerInfo.ClearAddr();

    netSocketAddress addr;
    const char* addrStr;
    if(PARAM_netrelayserveraddr.Get(addrStr)
        && netVerifyf(addr.Init(addrStr), "Invalid relay server address"))
    {
        netDebug("FetchRelayServerAddr :: Relay server address is from the command line");
#if RSG_OUTPUT
		if(addr.GetPort() == 0)
		{
			const u16 port = PARAM_netrelaysecure.Get() ? NET_RELAY_DEFAULT_PORT_SECURE : NET_RELAY_DEFAULT_PORT;
			netWarning("FetchRelayServerAddr :: Relay server address is from the command line but missing the port. Using default port %u.", port);
			addr.Init(addr.GetIp(), port);
		}
#endif
        s_RelayServerInfo.m_ServerAddr = addr;
    }
#if RL_FORCE_STAGE_ENVIRONMENT
#ifdef RL_FORCED_RELAY_ADDRESS // not defined for all stage-environments
    else if(rlIsForcedEnvironment() && addr.Init(RL_FORCED_RELAY_ADDRESS))
    {
        netDebug("FetchRelayServerAddr :: Relay server address is from RL_FORCED_RELAY_ADDRESS");
        s_RelayServerInfo.m_ServerAddr = addr;
    }
#endif
#endif
    else if(s_RelayGeolocInfo.m_NumRelayAddrs > 0
            || (rlRos::GetGeoLocInfo(&s_RelayGeolocInfo)
            && netVerifyf(s_RelayGeolocInfo.m_NumRelayAddrs > 0, "No relay servers in geoloc info")))
    {
        netDebug("FetchRelayServerAddr :: Relay server address is from geolocation data");

        //Pick a random relay server and make sure it's a different server than the one we already had
		unsigned numIterations = 0;
		unsigned maxIterations = 20;
		netSocketAddress relayAddr;

		while(true)
		{
			++numIterations;
			if(numIterations > maxIterations)
			{
				break;
			}

			const int idx = netRelayRNG.GetInt() % s_RelayGeolocInfo.m_NumRelayAddrs;

			relayAddr = s_RelayGeolocInfo.m_RelayAddrs[idx];
			if(!relayAddr.IsValid())
			{
				// invalid address, try again
				continue;
			}

			if(!oldRelayAddr.IsValid())
			{
				// we didn't have a previous relay address
				break;
			}

			if(s_RelayGeolocInfo.m_NumRelayAddrs <= 1)
			{
				// we only have one server to choose
				break;
			}

			if(relayAddr == oldRelayAddr)
			{
				// we chose the same server, try again
				continue;
			}

			break;
		}

        if(netVerifyf(relayAddr.IsValid(), "Invalid relay server address"))
        {
            s_RelayServerInfo.m_ServerAddr = relayAddr;
        }
    }

    return s_RelayServerInfo.HasValidAddress();
}

#if SC_PRESENCE
bool
netRelay::FetchPresenceServerAddr(char (&hostname)[MAX_HOSTNAME_BUF_SIZE], u16& port)
{
	if(PARAM_netpresenceserverdisable.Get())
	{
		return false;
	}

    SYS_CS_SYNC(s_MainCs);
	
	hostname[0] = '\0';
	port = 0;

    s_PresenceServerInfo.ClearAddr();

    const char* addrStr = nullptr;
    if(PARAM_netpresenceserveraddr.Get(addrStr) RL_FORCE_STAGE_ENVIRONMENT_ONLY(|| rlIsForcedEnvironment()))
    {
#if RL_FORCE_STAGE_ENVIRONMENT
        if(rlIsForcedEnvironment())
        {
			addrStr = RL_FORCED_PRESENCE_ADDRESS;
			netDebug("FetchPresenceServerAddr :: Presence server address is from forced environment: %s", addrStr);
        }
		else
#endif
		netDebug("FetchPresenceServerAddr :: Presence server address is from the command line: -%s=%s", PARAM_netpresenceserveraddr.GetName(), addrStr);
        rtry
		{
			// hostname + port
			char serverStr[MAX_HOSTNAME_BUF_SIZE + 32];
			safecpy(serverStr, addrStr);

			char* colon = strrchr(serverStr, ':');
			rverify(colon, catchall, netError("Failed to find the ':' in the -netpresenceserveraddr command line"));

			u32 u32Port = 0;
			rverify((sscanf(colon, ":%u", &u32Port) == 1) && (u32Port < 65536), catchall, netError("Failed to find the port in the -netpresenceserveraddr command line"));
			port = (u16)u32Port;

			*colon = '\0';

			safecpy(hostname, serverStr);

			netDebug("Presence server address is from the command line");
		}
		rcatchall
		{
			hostname[0] = '\0';
			port = 0;
		}
    }
	else if(s_PresenceServerList.m_NumPresenceAddrs > 0
		|| (rlRos::GetPresenceInfo(&s_PresenceServerList)
		&& netVerifyf(s_PresenceServerList.m_NumPresenceAddrs > 0, "No presence servers in list")))
	{
		netDebug("Presence server address is from SCS service");

		//Pick a random presence server
		const int idx = netRelayRNG.GetInt() % s_PresenceServerList.m_NumPresenceAddrs;
		
		(void)netVerifyf(s_PresenceServerList.GetPresenceServer(idx, hostname, port), "Invalid presence server address");
	}

    return (hostname[0] != '\0') && (port > 0);
}

void
netRelay::UpdatePresenceServerDiscovery()
{
	PROFILE

    s_PresenceServerInfo.m_DiscoveryRetryTimer.Update();

	bool isAnyOnline = false;
	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		if (rlRos::IsOnline(i) && rlRos::HasPrivilege(i, RLROS_PRIVILEGEID_PRESENCE_WRITE))
		{
			isAnyOnline = true;
			break;
		}
	}
	
	if(!isAnyOnline)
	{
		if(s_PresenceServerInfo.m_DiscoveryStatus.Pending())
		{
			netResolver::Cancel(&s_PresenceServerInfo.m_DiscoveryStatus);
		}
		s_PresenceServerInfo.ClearAddr();
		s_PresenceServerList.Clear();
		s_PresenceServerState = PRES_STATE_WAIT_FOR_ROS_ONLINE;
	}

    switch(s_PresenceServerState)
    {
	case PRES_STATE_WAIT_FOR_ROS_ONLINE:
		// wait until we have our CreateTicketResponse before connecting to presence
		// since it contains the flag that determines whether or not we connect securely.
		if(isAnyOnline)
		{
			s_PresenceServerState = PRES_STATE_BEGIN_DISCOVERY;
		}
		break;
    case PRES_STATE_BEGIN_DISCOVERY:
        if(s_PresenceServerInfo.HasValidAddress())
        {
            s_PresenceServerState = PRES_STATE_HAVE_ADDRESS;
        }
        else if(s_PresenceServerInfo.m_DiscoveryRetryTimer.IsTimeToRetry())
        {
            //If we haven't yet discovered what environment we're running in
            //(dev, cert, prod, etc.) then we won't have a valid domain name.

			char hostname[MAX_HOSTNAME_BUF_SIZE] = {0};			
            if(FetchPresenceServerAddr(hostname, s_PresenceServerPort))
            {
                netDebug("Discovering presence server address...");
#if !__NO_OUTPUT
				char contextStr[64];
				formatf(contextStr, "[Presence Server]");
#elif !GTA_VERSION
				const char* contextStr = nullptr;
#endif
				
                if(netResolver::ResolveHost(hostname,
                                            &s_PresenceServerIp,
#if !GTA_VERSION
											contextStr,
#endif
                                            &s_PresenceServerInfo.m_DiscoveryStatus))
                {
                    s_PresenceServerState = PRES_STATE_DISCOVERING;
                }
                else
                {
                    netError("Error resolving host: \"%s\"", hostname);
                }
            }

            if(PRES_STATE_DISCOVERING == s_PresenceServerState)
            {
                s_PresenceServerInfo.m_DiscoveryRetryTimer.Restart();
            }
        }
        break;
    case PRES_STATE_DISCOVERING:
        if(s_PresenceServerInfo.m_DiscoveryStatus.Succeeded())
        {
            s_PresenceServerInfo.m_ServerAddr.Init(s_PresenceServerIp, s_PresenceServerPort);

            netDebug("Discovered presence server address: " NET_ADDR_FMT,
                        NET_ADDR_FOR_PRINTF(s_PresenceServerInfo.m_ServerAddr));

            s_PresenceServerState = PRES_STATE_HAVE_ADDRESS;
        }
        else if(!s_PresenceServerInfo.m_DiscoveryStatus.Pending())
        {
            netError("Failed to discover presence server address");
            s_PresenceServerState = PRES_STATE_WAIT_FOR_ROS_ONLINE;
        }
        break;
    case PRES_STATE_HAVE_ADDRESS:
        if(!s_PresenceServerInfo.m_ServerAddr.IsValid())
        {
            s_PresenceServerState = PRES_STATE_WAIT_FOR_ROS_ONLINE;
        }
        break;
    }
}
#endif

void
netRelay::UpdatePing()
{
	PROFILE

    SYS_CS_SYNC(s_MainCs);

    s_RelayServerInfo.m_NextPing.Update();
    s_PresenceServerInfo.m_NextPing.Update();

    if(s_SocketManager->CanSendReceive())
    {
        netSocketAddress presServerAddr;

        //Ping the presence server
        if(s_PresenceServerInfo.m_NextPing.IsTimeToRetry()
            && s_PresenceServerInfo.GetServerAddress(&presServerAddr)
			&& GetSocket()->IsReadyToSend())
        {
            netRelayPingPacket pkt;

            netDebug2("Presence PING to [" NET_ADDR_FMT "]",
                    NET_ADDR_FOR_PRINTF(presServerAddr));

			const unsigned curTime = sysTimer::GetSystemMsTime();
			s_PresenceServerInfo.m_LastPingTimeMs = curTime;

			//The next ping will be sent X seconds after the last ping was sent (not since the last pong was received).
			//This ensures the NAT will be kept open even if the RTT is high, or if a (short) stall occurs between
			//sending a ping and receiving a pong.
			s_PresenceServerInfo.m_NextPingTimeMs = curTime + GetPresencePingIntervalMs();

            //For each signed in gamer add their gamer handle and rockstar ID
            //to the keepalive.
            //The payload looks like:
            //  PRES1,<title name>,<platform name>,<gamer handle>,<rockstar ID>,<gamer handle>,<rockstar ID>
            int credCount = 0;
            char* p = (char*)pkt.GetPayload();
            const char* eob = p + netRelayPacket::MAX_SIZEOF_PING_PAYLOAD;
            *p = '\0';

            for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
            {
                const rlRosCredentials& cred = rlRos::GetCredentials(i);
                if(!cred.IsValid())
                {
                    continue;
                }
                rlGamerHandle gh;
#if __RGSC_DLL
				// in the SC DLL, we have ROS creds before a gamer handle so this can return false for a few frames
                if(!rlPresence::GetGamerHandle(i, &gh))
#else
				if(!netVerify(rlPresence::GetGamerHandle(i, &gh)))
#endif
                {
                    continue;
                }

                if(0 == credCount)
                {
                    //Header
                    formatf_sized(p,
                            int(eob-p),
                            "%s,%s,%s",
                            PRESENCE_VERSION_STRING,
                            g_rlTitleId->m_RosTitleId.GetTitleName(),
                            g_rlTitleId->m_RosTitleId.GetPlatformName());

                    p += strlen(p);
                }

                char ghStr[RL_MAX_GAMER_HANDLE_CHARS];
                formatf_sized(p,
                        int(eob-p),
                        ",%s,%" I64FMT "d",
                        gh.ToString(ghStr),
                        cred.GetRockstarId());

                p += strlen(p);

                ++credCount;
            }

            const unsigned sizeofPayload = istrlen((char*)pkt.GetPayload());

            pkt.ResetPingHeader(sizeofPayload);
            pkt.Mix();

			SendData(presServerAddr, IsUsingSecurePresence(), &pkt, pkt.GetSize());

			++s_PresenceServerInfo.m_NumConsecutivePingRetries;
            s_PresenceServerInfo.m_NextPing.ScaleRange(NET_RELAY_RETRY_SCALE,
                                                        GetPresencePingIntervalMs());
            s_PresenceServerInfo.m_NextPing.Restart();

			++s_PresenceServerInfo.m_NumUnAckedPings;
			s_RelayPresenceStats.IncTotalNumPresencePings();

#if FAKE_DROP_LATENCY
            // When the GetFakeUplinkDisconnection is enabled and some time has passed we start to fake an IsOffline too.
            if (netSocket::GetFakeUplinkDisconnection() && s_PresenceServerInfo.m_NumUnAckedPings > 1 && 
                (sysTimer::GetSystemMsTime() - s_PresenceServerInfo.m_LastPongTimeMs) > 20000)
            {
                rlPresence::FakeOfflineDueToUplink(true);
            }
#endif

#if !__NO_OUTPUT
			if(s_PresenceServerInfo.m_NumUnAckedPings > 1)
			{
				netWarning("Unacked presence pings: %u", s_PresenceServerInfo.m_NumUnAckedPings - 1);
			}
#endif
        }

        netSocketAddress relayServerAddr;

        //Ping the relay server
        if(s_RelayStarted
            && s_RelayServerInfo.m_NextPing.IsTimeToRetry()
            && s_RelayServerInfo.GetServerAddress(&relayServerAddr)
			&& GetSocket()->IsReadyToSend())
        {
			const unsigned relayPingIntervalMs = GetRelayPingIntervalMs();

            //Send the ping only if the relay server is different from the
            //presence server.
            if(relayServerAddr != presServerAddr)
            {
                netRelayPing2Packet pkt;

#if !__NO_OUTPUT
				if(s_RelayServerInfo.m_NumConsecutivePingRetries == 0)
				{
					netDebug2("Relay PING to [" NET_ADDR_FMT "]",
						NET_ADDR_FOR_PRINTF(relayServerAddr));
				}
				else
				{
					netWarning("Relay PING to [" NET_ADDR_FMT "]. Retry: %u",
						NET_ADDR_FOR_PRINTF(relayServerAddr),
						s_RelayServerInfo.m_NumConsecutivePingRetries);
				}
#endif

                pkt.ResetPing2Header(0);
                pkt.Mix();

				const unsigned curTime = sysTimer::GetSystemMsTime();
				s_RelayServerInfo.m_LastPingTimeMs = curTime;

				//The next ping will be sent X seconds after the last ping was sent (not since the last pong was received).
				//This ensures the NAT will be kept open even if the RTT is high, or if a (short) stall occurs between
				//sending a ping and receiving a pong.
				s_RelayServerInfo.m_NextPingTimeMs = curTime + relayPingIntervalMs;

                SendData(relayServerAddr, IsUsingSecureRelays(), &pkt, pkt.GetSize());
            }
            else
            {
                netDebug3("Skipping relay PING - relay server is same as presence server");

            }

			// set up retry timer in case this ping goes unacked.
			// if fast retries are enabled (i.e. s_PingRetryMs > 0), then the first few retries will be sent quickly
			// (to maintain the connection to the relay server and the p2p connections that rely on it), but back
			// off gradually to avoid flooding the relay server if it has gone down. The retry interval backs off
			// until the time between retries reaches the normal ping interval.
			if(s_PingRetryMs > 0)
			{
				if(s_RelayServerInfo.m_NumConsecutivePingRetries == 0)
				{
					s_RelayServerInfo.m_PingRetryMs = s_PingRetryMs;
				}
				else
				{
					s_RelayServerInfo.m_PingRetryMs = static_cast<unsigned>(s_RelayServerInfo.m_PingRetryMs * NET_RELAY_RETRY_BACKOFF_MULTIPLIER);
				}
			}
			else
			{
				// retries are disabled
				s_RelayServerInfo.m_PingRetryMs = relayPingIntervalMs;
			}
			
			s_RelayServerInfo.m_PingRetryMs = Clamp(s_RelayServerInfo.m_PingRetryMs, s_PingRetryMs, relayPingIntervalMs);

			const unsigned timeUntilPingRetry = s_RelayServerInfo.m_PingRetryMs;

			// randomize retry timing slightly to avoid having everyone retry at the same time
			s_RelayServerInfo.m_NextPing.InitMilliseconds(static_cast<int>(timeUntilPingRetry / (NET_RELAY_RETRY_SCALE / 100.f)), timeUntilPingRetry);

			++s_RelayServerInfo.m_NumConsecutivePingRetries;

#if !__NO_OUTPUT
			if(s_RelayServerInfo.m_NumConsecutivePingRetries > 1)
			{
				netDebug2("Will retry relay ping in %u ms", s_RelayServerInfo.m_NextPing.GetMillisecondsUntilRetry());
			}
#endif

			++s_RelayServerInfo.m_NumUnAckedPings;
			s_RelayPresenceStats.IncTotalNumRelayPings();

#if !__NO_OUTPUT
			if(s_RelayServerInfo.m_NumUnAckedPings > 1)
			{
				netWarning("Unacked relay pings: %u", s_RelayServerInfo.m_NumUnAckedPings - 1);
			}
#endif
        }
    }
}

bool 
netRelay::SendPing(const netSocketAddress& addr)
{
	netRelayPingPacket pkt;

	pkt.ResetPingHeader(0);
	pkt.Mix();

	return SendData(addr, IsUsingSecureRelays(), &pkt, pkt.GetSize());
}

sysIpcCurrentThreadId
netRelay::GetCurrentThreadId()
{
#if RSG_PC && __RGSC_DLL
	// don't use sysIpcGetCurrentThreadId here. The 32-bit version of the socialclub.dll 
	// doesn't support thread local variables due to a limitation in earlier versions of Windows.
	// Instead, call the Windows native API directly instead of using Rage's TLS cached value.
	return (sysIpcCurrentThreadId) GetCurrentThreadId();
#else
	return sysIpcGetCurrentThreadId();
#endif
}

void
netRelay::OnReceive(const netSocketAddress& from,
					void* pktBuf,
					const int len)
{
	PROFILE

    rtry
    {
        netDebug3("[" NET_ADDR_FMT "]:PKT<-,sz:%d",
                    NET_ADDR_FOR_PRINTF(from),
                    len);

        rcheck(IsInitialized(),catchall,);

        rcheck(len > 0,catchall,);

		// during development make sure the ports are in the range we expect, but
		// these can change later, so don't assume this range outside of development.
		netAssert(from.GetPort() >= 61455 && from.GetPort() <= 61458);
		
		netRelayPacket* relayPkt = static_cast<netRelayPacket*>(pktBuf);

        rcheck(len >= netRelayPacket::MIN_SIZEOF_HEADER
                && len <= (int)GetMaxPacketSize(),
                catchall,
                netDebug2("Packet size:%d is outside valid range:%d-%d",
                            len,
                            netRelayPacket::MIN_SIZEOF_HEADER,
                            (int)GetMaxPacketSize()));

        //GetSize() can return less than len if it contains plaintext.
        //The plain text portion of the packet comes after the encrypted portion.
        //See Xbox networking docs on the VDP packet format.
        rcheck((int)relayPkt->GetSize() <= len,
                catchall,
                netDebug2("Dropping packet with invalid size:%d - should be:%d.",
                            relayPkt->GetSize(),
                            len));

        relayPkt->Unmix();

        rcheck(relayPkt->IsRelayPacket(),
                catchall,
                netDebug2("Received unknown packet type"));

        rcheck(relayPkt->GetVersion() == netRelayPacket::PROTOCOL_VERSION,
                catchall,
                netDebug2("Incompatible protocol version %d, expected %d",
                            relayPkt->GetVersion(),
                            netRelayPacket::PROTOCOL_VERSION));

        //Who we expect to be sending us packets.

        netSocketAddress relayAddr, presenceAddr;
        s_RelayServerInfo.GetServerAddress(&relayAddr);
        s_PresenceServerInfo.GetServerAddress(&presenceAddr);

        if(relayPkt->IsPing() || relayPkt->IsPing2())
        {
            netDebug2("Why are we receiving a PING from [" NET_ADDR_FMT "]?",
                    NET_ADDR_FOR_PRINTF(from));
        }
        else if(relayPkt->IsP2pForward())
        {
            netDebug2("Why are we receiving a P2P_FORWARD from [" NET_ADDR_FMT "]?",
                    NET_ADDR_FOR_PRINTF(from));
        }
        else if(relayPkt->IsPong() || relayPkt->IsPong2())
        {
			//This is an answer to a PING we sent
			const netSocketAddress reflexiveAddr = relayPkt->GetDstAddr();

			{
				SYS_CS_SYNC(s_MainCs);
				if(sm_Delegators[NET_RELAYEVENT_RELAY_PONG_RECEIVED].HasDelegates())
				{
					//Non-concurrent delegates can handle the message only from
					//the main thread, so queue it for later delivery.
					netRelayEventRelayPongReceived* e = (netRelayEventRelayPongReceived*)s_Allocator->RAGE_LOG_ALLOCATE(sizeof(netRelayEventRelayPongReceived), 0);

					if(netVerifyf(e, "Error allocating relay event"))
					{
						new (e) netRelayEventRelayPongReceived(netAddress(from, netRelayToken::INVALID_RELAY_TOKEN, reflexiveAddr), s_Allocator);
						s_EventQ.QueueEvent(e, 0);
					}
				}
			}

			{
				SYS_CS_SYNC(s_ConcurrentDlgtCs);

				if(sm_ConcurrentDelegators[NET_RELAYEVENT_RELAY_PONG_RECEIVED].HasDelegates())
				{
					//Concurrent delegates can handle the event at any time, from any
					//thread, so just send it now. Note that this must be dispatched 
					//outside of the s_MainCs critical section to avoid deadlocks.
					const netRelayEventRelayPongReceived e(netAddress(from, netRelayToken::INVALID_RELAY_TOKEN, reflexiveAddr), NULL);
					sm_ConcurrentDelegators[NET_RELAYEVENT_RELAY_PONG_RECEIVED].Dispatch(e);
				}
			}

            if(relayAddr == presenceAddr && relayAddr == from)
            {
                netDebug2("Relay/presence PONG from [" NET_ADDR_FMT "]",
                            NET_ADDR_FOR_PRINTF(from));

                HandleRelayPong(relayPkt);
                HandlePresencePong(relayPkt);
            }
            else if(relayAddr == from)
            {
                netDebug2("Relay PONG from [" NET_ADDR_FMT "]. RTT: %u ms.",
                            NET_ADDR_FOR_PRINTF(from),
							GetMsSinceLastRelayPing());

                HandleRelayPong(relayPkt);
            }
            else if(presenceAddr == from)
            {
                netDebug2("Presence PONG from [" NET_ADDR_FMT "]. RTT: %u ms.",
                            NET_ADDR_FOR_PRINTF(from),
							GetMsSinceLastPresencePing());

                HandlePresencePong(relayPkt);
            }
        }
        else if(relayPkt->IsMessage())
        {
            netDebug2("Received message from [" NET_ADDR_FMT "]",
                        NET_ADDR_FOR_PRINTF(from));

            if(presenceAddr == from)
            {
                //This is an answer to a PING we sent
                HandleMessage(relayPkt);
            }
            else
            {
                netDebug("Unknown source: " NET_ADDR_FMT ", expected: " NET_ADDR_FMT,
                        NET_ADDR_FOR_PRINTF(from),
                        NET_ADDR_FOR_PRINTF(presenceAddr));
            }
        }
        else if(relayPkt->IsP2pTerminus())
        {
            if(relayAddr == from)
            {
                //We're the final recipient of a P2P packet.
                HandleTerminus(relayPkt);
            }
            else
            {
#if !__NO_OUTPUT
				if(s_LastRelayServerInfo.m_ServerAddr == from)
				{
					netWarning("Received relay TERMINUS from [" NET_ADDR_FMT "] (our previous relay address) - dropping packet",
						NET_ADDR_FOR_PRINTF(from));
				}
				else
                {
					netDebug("Unknown source: " NET_ADDR_FMT ", expected: " NET_ADDR_FMT,
							NET_ADDR_FOR_PRINTF(from),
							NET_ADDR_FOR_PRINTF(relayAddr));
                }
#endif
            }
        }
        else
        {
            netDebug2("Invalid relay packet received from [" NET_ADDR_FMT "]",
                    NET_ADDR_FOR_PRINTF(from));
        }
    }
    rcatchall
    {
    }
}

void
netRelay::HandleTerminus(netRelayPacket* relayPkt)
{
	PROFILE

	if(PARAM_netrelayignorep2pterminus.Get())
	{
		return;
	}

    if(s_RelayStarted)
    {
        bool success = false;

        netAddress sender;
        void* payload = NULL;
        unsigned sizeofPayload = 0;

        rtry
        {
            SYS_CS_SYNC(s_MainCs);

            rverify(relayPkt->GetType() == NET_RELAYPACKET_P2P_TERMINUS,catchall,);

            rverify(relayPkt->GetSrcRelayAddress(&sender),
                    catchall,
                    netError("Error retrieving source relay address"));

            netDebug3("Received relay TERMINUS from [" NET_ADDR_FMT "]",
                    NET_ADDR_FOR_PRINTF(sender));

			netDtlsEndpoint* ep = GetEndpointByAddr(sender.GetProxyAddress());
			if(ep)
			{
				if(ep->m_SslState == netDtlsEndpoint::STATE_CONNECTED)
				{
					ep->m_Timeout.Update();
					if(ep->m_Timeout.IsTimedOut() == false)
					{
						ep->m_Timeout.InitSeconds(NET_RELAY_DTLS_CXN_TIMEOUT_SEC);
					}
				}
			}

            payload = relayPkt->GetPayload();
            sizeofPayload = relayPkt->GetSizeofPayload();

			s_RelayServerInfo.m_LastTerminusTimeMs = sysTimer::GetSystemMsTime();

            if(sm_Delegators[NET_RELAYEVENT_PACKET_RECEIVED].HasDelegates())
            {
                //Non-concurrent delegates can handle the message only from
                //the main thread, so queue it for later delivery.
                const int evtSize = sizeof(netRelayEventPacketReceived) + sizeofPayload;
                netRelayEventPacketReceived* e =
                    (netRelayEventPacketReceived*)s_Allocator->RAGE_LOG_ALLOCATE(evtSize, 0);

                if(netVerifyf(e, "Error allocating relay event"))
                {
                    void* payloadDst = &e[1];
                    sysMemCpy(payloadDst, payload, sizeofPayload);
                    new (e) netRelayEventPacketReceived(sender, payloadDst, sizeofPayload, s_Allocator);
                    s_EventQ.QueueEvent(e, 0);
                }
            }

            success = true;
        }
        rcatchall
        {
        }

        if(success)
        {
            SYS_CS_SYNC(s_ConcurrentDlgtCs);

            if(sm_ConcurrentDelegators[NET_RELAYEVENT_PACKET_RECEIVED].HasDelegates())
            {
				//Concurrent delegates can handle the event at any time, from any
				//thread, so just send it now. Note that this must be dispatched 
				//outside of the s_MainCs critical section to avoid deadlocks.
                const netRelayEventPacketReceived e(sender, payload, sizeofPayload, NULL);
                sm_ConcurrentDelegators[NET_RELAYEVENT_PACKET_RECEIVED].Dispatch(e);
            }
        }
    }
}

void
netRelay::HandleRelayPong(netRelayPacket* relayPkt)
{
	PROFILE

	if(PARAM_netrelayignorepongs.Get())
	{
		return;
	}

#if RSG_BANK
	if (sm_bIgnoreRelayPongs)
		return;
#endif

	bool relayAddrObtained = false;
	bool relayAddrChanged = false;
	netRelayEventAddressObtained evtRelayEventAddressObtained;
	netRelayEventAddressChanged evtRelayEventAddressChanged;

    if(s_RelayStarted)
    {
        SYS_CS_SYNC(s_MainCs);

        netAssert(relayPkt->GetType() == NET_RELAYPACKET_PONG ||
				  relayPkt->GetType() == NET_RELAYPACKET_PONG2);

		if(relayPkt->GetType() != NET_RELAYPACKET_PONG2)
		{
			// only accept pong2 - NAT detection and ICE can send ping1 messages,
			// don't allow a pong1 through here since it won't have a token.
			return;
		}

		s_RelayPresenceStats.IncNumRelayPongs();
		s_RelayServerInfo.m_LastPongTimeMs = sysTimer::GetSystemMsTime();

        const netSocketAddress myTargetAddr = relayPkt->GetDstAddr();
		const netRelayToken myRelayToken = relayPkt->GetDstRelayToken();
        netAssert(s_RelayServerInfo.m_ServerAddr.IsValid());

        if(!s_MyRelayAddr.GetTargetAddress().IsValid())
        {
            s_MyRelayAddr.Init(s_RelayServerInfo.m_ServerAddr, netRelayToken::INVALID_RELAY_TOKEN, myTargetAddr);

			if(myRelayToken.IsValid())
			{
				s_MyRelayToken = myRelayToken;
			}

			OUTPUT_ONLY(char relayTokenHexString[netRelayToken::TO_HEX_STRING_BUFFER_SIZE];)
            netDebug("My relay address: [" NET_ADDR_FMT "]. Token: %s.",
                    NET_ADDR_FOR_PRINTF(s_MyRelayAddr),
					s_MyRelayToken.ToHexString(relayTokenHexString));

#if !__NO_OUTPUT
			// assert if the relay mapped address is a private address. Indicates a configuration issue,
			// or someone using a presence address as a relay address on the command line.
			const char* addrStr;
			PARAM_netrelayserveraddr.Get(addrStr);
			netAssertf(!s_MyRelayAddr.GetTargetAddress().GetIp().IsV4() ||
					   !s_MyRelayAddr.GetTargetAddress().GetIp().ToV4().IsRfc1918(),
						"Relay mapped address is a private address: [" NET_ADDR_FMT "]. -netrelayserveraddr: %s",
						NET_ADDR_FOR_PRINTF(s_MyRelayAddr),
						PARAM_netrelayserveraddr.Get() ? addrStr : "not enabled");
#endif

			// dispatched to concurrent delegator outside of s_MainCs critical section to avoid deadlocks
			relayAddrObtained = true;
			new (&evtRelayEventAddressObtained) netRelayEventAddressObtained(s_MyRelayAddr, NULL);

			{
				SYS_CS_SYNC(s_MainCs);
				if(sm_Delegators[NET_RELAYEVENT_ADDRESS_OBTAINED].HasDelegates())
				{
					//Non-concurrent delegates can handle the message only from
					//the main thread, so queue it for later delivery.
					netRelayEventAddressObtained* e = (netRelayEventAddressObtained*)s_Allocator->RAGE_LOG_ALLOCATE(sizeof(netRelayEventAddressObtained), 0);

					if(netVerifyf(e, "Error allocating relay event"))
					{
						new (e) netRelayEventAddressObtained(s_MyRelayAddr, s_Allocator);
						s_EventQ.QueueEvent(e, 0);
					}
				}
			}
        }
        else if((s_MyRelayAddr.GetTargetAddress() != myTargetAddr) || 
				(s_MyRelayToken != myRelayToken))
        {
            const netAddress newRelayAddr(s_RelayServerInfo.m_ServerAddr, netRelayToken::INVALID_RELAY_TOKEN, myTargetAddr);			

			// dispatched to concurrent delegator outside of s_MainCs critical section to avoid deadlocks
			relayAddrChanged = true;
			new (&evtRelayEventAddressChanged) netRelayEventAddressChanged(newRelayAddr, s_MyRelayAddr, NULL);

			{
				SYS_CS_SYNC(s_MainCs);
				if(sm_Delegators[NET_RELAYEVENT_ADDRESS_CHANGED].HasDelegates())
				{
					//Non-concurrent delegates can handle the message only from
					//the main thread, so queue it for later delivery.
					netRelayEventAddressChanged* e = (netRelayEventAddressChanged*)s_Allocator->RAGE_LOG_ALLOCATE(sizeof(netRelayEventAddressChanged), 0);

					if(netVerifyf(e, "Error allocating relay event"))
					{
						new (e) netRelayEventAddressChanged(newRelayAddr, s_MyRelayAddr, s_Allocator);
						s_EventQ.QueueEvent(e, 0);
					}
				}
			}

#if !__NO_OUTPUT
			if(s_MyRelayAddr.GetTargetAddress() != myTargetAddr)
			{
				netWarning("My relay address changed from [" NET_ADDR_FMT "] to [" NET_ADDR_FMT "]!!!",
							NET_ADDR_FOR_PRINTF(s_MyRelayAddr),
							NET_ADDR_FOR_PRINTF(newRelayAddr));
			}

			if(s_MyRelayToken != myRelayToken)
			{
				char hexString1[netRelayToken::TO_HEX_STRING_BUFFER_SIZE];
				char hexString2[netRelayToken::TO_HEX_STRING_BUFFER_SIZE];
				netWarning("My relay token changed from [%s] to [%s]!!!",
							s_MyRelayToken.ToHexString(hexString1),
							myRelayToken.ToHexString(hexString2));
			}
#endif

			s_RelayPresenceStats.IncNumRelayMappedAddrChanges();

            s_MyRelayAddr = newRelayAddr;

			if(myRelayToken.IsValid())
			{
				s_MyRelayToken = myRelayToken;
			}
        }

		if(s_MyRelayAddr.IsValid() && (s_MyRelayAddr != s_MyLastRelayAddr))
		{
			if(s_MyLastRelayAddr.IsValid())
			{
				// dispatched to concurrent delegator outside of s_MainCs critical section to avoid deadlocks
				relayAddrChanged = true;
				new (&evtRelayEventAddressChanged) netRelayEventAddressChanged(s_MyRelayAddr, s_MyLastRelayAddr, NULL);

				{
					SYS_CS_SYNC(s_MainCs);
					if(sm_Delegators[NET_RELAYEVENT_ADDRESS_CHANGED].HasDelegates())
					{
						//Non-concurrent delegates can handle the message only from
						//the main thread, so queue it for later delivery.
						netRelayEventAddressChanged* e = (netRelayEventAddressChanged*)s_Allocator->RAGE_LOG_ALLOCATE(sizeof(netRelayEventAddressChanged), 0);

						if(netVerifyf(e, "Error allocating relay event"))
						{
							new (e) netRelayEventAddressChanged(s_MyRelayAddr, s_MyLastRelayAddr, s_Allocator);
							s_EventQ.QueueEvent(e, 0);
						}
					}
				}

				netWarning("My relay address changed from [" NET_ADDR_FMT "] to [" NET_ADDR_FMT "]!!!",
					NET_ADDR_FOR_PRINTF(s_MyLastRelayAddr),
					NET_ADDR_FOR_PRINTF(s_MyRelayAddr));

				s_RelayPresenceStats.IncNumRelayAddrChanges();
			}

			s_MyLastRelayAddr = s_MyRelayAddr;
		}

        //Ping every X seconds to keep the NAT open.
		const unsigned curTime = sysTimer::GetSystemMsTime();
		const unsigned timeUntilNextPingMs = Min(GetRelayPingIntervalMs(),
												(s_RelayServerInfo.m_NextPingTimeMs > curTime) ?
												(s_RelayServerInfo.m_NextPingTimeMs - curTime) :
												(NET_MIN_PORT_BINDING_TIMEOUT_SEC * 1000));

		netDebug3("Next relay ping will be in %u ms", timeUntilNextPingMs);
		s_RelayServerInfo.m_NextPing.InitMilliseconds(timeUntilNextPingMs);

		s_RelayServerInfo.m_NumConsecutivePingRetries = 0;
        s_RelayServerInfo.m_NumUnAckedPings = 0;
    }

	// dispatched outside of s_MainCs critical section to avoid deadlocks
	if(relayAddrObtained)
	{
		SYS_CS_SYNC(s_ConcurrentDlgtCs);

		if(sm_ConcurrentDelegators[NET_RELAYEVENT_ADDRESS_OBTAINED].HasDelegates())
		{
			sm_ConcurrentDelegators[NET_RELAYEVENT_ADDRESS_OBTAINED].Dispatch(evtRelayEventAddressObtained);
		}
	}

	if(relayAddrChanged)
	{
		SYS_CS_SYNC(s_ConcurrentDlgtCs);

		if(sm_ConcurrentDelegators[NET_RELAYEVENT_ADDRESS_CHANGED].HasDelegates())
		{
			sm_ConcurrentDelegators[NET_RELAYEVENT_ADDRESS_CHANGED].Dispatch(evtRelayEventAddressChanged);
		}
	}
}

void
netRelay::HandlePresencePong(netRelayPacket* relayPkt)
{
	PROFILE

    SYS_CS_SYNC(s_MainCs);

	if(PARAM_netpresenceignorepongs.Get())
	{
		return;
	}

#if RSG_BANK
	if (sm_bIgnorePresencePongs)
		return;
#endif
	
    netAssert(relayPkt->GetType() == NET_RELAYPACKET_PONG ||
			  relayPkt->GetType() == NET_RELAYPACKET_PONG2);

	s_RelayPresenceStats.IncNumPresncePongs();

	const netSocketAddress myPresenceAddress = relayPkt->GetDstAddr();
	if(!s_PresenceServerInfo.m_ServerAddr.IsValid())
	{
		netWarning("s_PresenceServerInfo server addr \"%s\" is not valid ",
                    s_PresenceServerInfo.m_ServerAddr.ToString());
	}

    if(s_MyPresenceAddr != myPresenceAddress)
    {
        s_MyPresenceAddr = myPresenceAddress;
        netDebug("My presence address: [" NET_ADDR_FMT "]",
                NET_ADDR_FOR_PRINTF(s_MyPresenceAddr));
    }

	//Ping every X seconds to keep the NAT open, keep our presence record alive, and
    //to check for presence messages.
	const unsigned curTime = sysTimer::GetSystemMsTime();
	const unsigned timeUntilNextPingMs = Min(GetPresencePingIntervalMs(),
											(s_PresenceServerInfo.m_NextPingTimeMs > curTime) ?
											(s_PresenceServerInfo.m_NextPingTimeMs - curTime) :
											(NET_MIN_PORT_BINDING_TIMEOUT_SEC * 1000));

	netDebug3("Next presence ping will be in %u ms", timeUntilNextPingMs);
    s_PresenceServerInfo.m_NextPing.InitMilliseconds(timeUntilNextPingMs);

    const unsigned sizeofPayload = relayPkt->GetSizeofPayload();

    //Does the PONG contain a message?
    if(sizeofPayload > 0)
    {
        HandleMessage(relayPkt);
    }

	s_PresenceServerInfo.m_NumConsecutivePingRetries = 0;
	s_PresenceServerInfo.m_NumUnAckedPings = 0;
}

void
netRelay::HandleMessage(netRelayPacket* relayPkt)
{
	PROFILE

	s_RelayPresenceStats.IncNumPresenceMessagesReceived();

    netAssert(relayPkt->GetType() == NET_RELAYPACKET_MESSAGE
            || (((relayPkt->GetType() == NET_RELAYPACKET_PONG) ||
				(relayPkt->GetType() == NET_RELAYPACKET_PONG2)) && relayPkt->GetSizeofPayload() > 0));

    char* payload = (char*)relayPkt->GetPayload();
    const unsigned sizeofPayload = relayPkt->GetSizeofPayload();

    netAssert(sizeofPayload > 0);

#if !__NO_OUTPUT
    char msg[1024];
    unsigned msgLen = sizeof(msg)-1;
    if(msgLen > sizeofPayload) msgLen = sizeofPayload;
    sysMemCpy(msg, payload, msgLen);
    msg[msgLen] = '\0';
    netDebug3("Received MESSAGE: %s", msg);
#endif

    {
        SYS_CS_SYNC(s_MainCs);

        if(sm_Delegators[NET_RELAYEVENT_MESSAGE_RECEIVED].HasDelegates())
        {
            //Non-concurrent delegates can handle the message only from
            //the main thread, so queue it for later delivery.

            //Extra byte for null terminator.
            const int evtSize = sizeof(netRelayEventMessageReceived) + sizeofPayload + 1;
            netRelayEventMessageReceived* e =
                (netRelayEventMessageReceived*)s_Allocator->RAGE_LOG_ALLOCATE(evtSize, 0);

            if(netVerifyf(e, "Error allocating relay event"))
            {
                char* msg = (char*)&e[1];
                sysMemCpy(msg, payload, sizeofPayload);
                msg[sizeofPayload] = '\0';
                new (e) netRelayEventMessageReceived(msg, sizeofPayload, s_Allocator);
                s_EventQ.QueueEvent(e, 0);
            }
        }
    }

    {
        SYS_CS_SYNC(s_ConcurrentDlgtCs);

        if(sm_ConcurrentDelegators[NET_RELAYEVENT_MESSAGE_RECEIVED].HasDelegates())
        {
			//Concurrent delegates can handle the event at any time, from any
			//thread, so just send it now. Note that this must be dispatched 
			//outside of the s_MainCs critical section to avoid deadlocks.
            const netRelayEventMessageReceived e((const char*)payload, sizeofPayload, NULL);
            sm_ConcurrentDelegators[NET_RELAYEVENT_MESSAGE_RECEIVED].Dispatch(e);
        }
    }
}

void netRelay::IncP2pPresenceMessageStats(const bool succeeded)
{
	s_RelayPresenceStats.IncP2pPresenceMessageStats(succeeded);
}

void netRelay::SetPingIntervalAdjustmentMs(const unsigned pingIntervalAdjustmentMs)
{
	if(s_PingIntervalAdjustmentMs != pingIntervalAdjustmentMs)
	{
		netDebug("SetPingIntervalAdjustmentMs :: %u", pingIntervalAdjustmentMs);
		s_PingIntervalAdjustmentMs = pingIntervalAdjustmentMs;
	}
}

void netRelay::SetRelayPingIntervalMs(const unsigned relayPingIntervalMs)
{
	if(s_TunableRelayPingIntervalMs != relayPingIntervalMs)
	{
		netDebug("SetRelayPingIntervalMs :: %u", relayPingIntervalMs);
		s_TunableRelayPingIntervalMs = relayPingIntervalMs;
	}
}

void netRelay::SetRelayPingRetryMs(const unsigned relayPingRetryMs)
{
	if(s_PingRetryMs != relayPingRetryMs)
	{
		netDebug("SetRelayPingRetryMs :: %u", relayPingRetryMs);
		s_PingRetryMs = relayPingRetryMs;
	}
}

void netRelay::SetAllowMultiplayerWithoutRelayServer(const bool allowMultiplayerWithoutRelayServer)
{
	if(s_AllowMultiplayerWithoutRelayServer != allowMultiplayerWithoutRelayServer)
	{
		netDebug("SetAllowMultiplayerWithoutRelayServer :: %s", LogBool(allowMultiplayerWithoutRelayServer));
		s_AllowMultiplayerWithoutRelayServer = allowMultiplayerWithoutRelayServer;
	}
}

void 
netRelay::SetRelayTelemetrySendIntervalMs(const unsigned relayTelemetrySendIntervalMs)
{
	if(s_RelayTelemetryIntervalMs != relayTelemetrySendIntervalMs)
	{
		netDebug("SetRelayTelemetrySendIntervalMs :: %d", relayTelemetrySendIntervalMs);
		s_RelayTelemetryIntervalMs = relayTelemetrySendIntervalMs;
	}
}

void
netRelay::UpdateTelemetry()
{
#if GTA_VERSION
	// we don't have a telemetry config on GTA V - the send interval is controlled via tunable instead.
	const u32 sendIntervalMs = s_RelayTelemetryIntervalMs;
#else
	const u32 sendIntervalMs = s_RelayPresenceStats.GetSendIntervalMs();
#endif

	if(sendIntervalMs == 0)
	{
		return;
	}

	netRetryTimer& timer = s_RelayPresenceStats.GetSendTimer();
	timer.Update();
	if(timer.IsStopped())
	{
		timer.InitMilliseconds(sendIntervalMs);
	}

	if(!timer.IsTimeToRetry())
	{
		return;
	}

	timer.Restart();	

	// send it, also returns true if discarded
	if(rlTelemetry::Write(rlPresence::GetActingUserIndex(), s_RelayPresenceStats))
	{
#if !__NO_OUTPUT
		char metricBuf[2048] = {0};
		RsonWriter rw(metricBuf, RSON_FORMAT_JSON);
		s_RelayPresenceStats.Write(&rw);
		netDebug3("Writing telemetry: %s", rw.ToString());
#endif
	}
	
	s_RelayPresenceStats.ResetStats();
}

}   //namespace rage
