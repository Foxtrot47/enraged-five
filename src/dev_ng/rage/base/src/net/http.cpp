// 
// net/http.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "http.h"

#include "data/base64.h"
#include "diag/channel.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "httprequesttracker.h"
#include "httpinterceptor.h"
#include "net.h"
#include "netdiag.h"
#include "resolver.h"
#include "string/string.h"
#include "string/stringutil.h"
#include "system/memory.h"
#include "system/nelem.h"

#include <stdio.h>
#include <time.h>
#include <algorithm>

#if __ASSERT
//For unit testing
#include "system/timer.h"
#endif

#if __WIN32
#include "system/xtl.h"
#if RSG_DURANGO
#include <winsock2.h>	// for INADDR_ANY
#endif
#elif RSG_NP
#include <netinet/in.h>
#endif

#include "bank/bank.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"

PF_PAGE(Http, "HTTP");
PF_GROUP(HttpMain);
PF_LINK(Http, HttpMain);
PF_TIMER(ReceiveBodyCost, HttpMain);
PF_TIMER(ReadBodyCost, HttpMain);

namespace rage
{

NOSTRIP_FINAL_LOGGING_PARAM(nethttpproxy, "Address of proxy to use for HTTP requests: <x.x.x.x:port>.  Port is optional.");
NOSTRIP_FINAL_LOGGING_PARAM(nethttpsnoproxy, "Ignore proxy for HTTPS requests");
PARAM(nethttpstunnel, "[http] Use the proxy address to tunnel HTTPS requests");
PARAM(nethttpserveraddr, "Address of server to use for HTTP requests: <x.x.x.x:port>.  Port is optional.");
PARAM(nethttpdumpbinary, "If present HTTP requests/responses will dump binary content");
PARAM(nethttptimeout, "HTTP request timeout in seconds.  Overrides application defined timeout.");
PARAM(nethttpuseragent, "HTTP user agent string.");
NOSTRIP_FINAL_LOGGING_PARAM(nethttpdump, "If present HTTP requests/responses (for everything except Bugstar) will be dumped to debug output");
PARAM(nethttpdumpbugstar, "If present, HTTP requests/responses for Bugstar will be dumped to debug output");
PARAM(nethttpallownonhttps, "[http] Allow non-HTTPS requests", "Disabled", "All", "All");

#if RSG_DURANGO
PARAM(nethttpnoxhttp, "If present don't use IXHR2 on Xbox One");
#endif

#if !__FINAL
PARAM(nohttpsometimes, "Debug setting to cause http to intermittently fail.  -nohttpsometimes=<failure percentage>");
PARAM(simulatedFailStatusCode, "If we trigger a debug failure through a simulated fail, use this error code");
//Used for simulating failures
static bool s_IsNetHttpRngInitialized = false;
static netRandom s_netHttpRng;
#endif

#if ENABLE_TCP_PROXY_ARGS
XPARAM(fiddler);
#endif

RAGE_DECLARE_CHANNEL(ragenet);
RAGE_DEFINE_SUBCHANNEL(ragenet, http)
#undef __rage_channel
#define __rage_channel ragenet_http

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "[reqid:%d]%s: " fmt, m_RequestId, m_ContextStr

#if !__NO_OUTPUT
#define GetHttpContextString() (GetContextString(Alloca(char, INTERNAL_CONTEXT_STRING_BUF_SIZE), INTERNAL_CONTEXT_STRING_BUF_SIZE))
#else
#define GetHttpContextString() (nullptr)
#endif

#if defined(CRLF)
#undef CRLF
#endif

#if defined(CR)
#undef CR
#endif

#if defined(LF)
#undef LF
#endif

#define CR '\r'
#define LF '\n'
#define CRLF "\r\n"

//Fudge factor for timeouts.
//When calling async functions we add additional seconds
//so HTTP detects the timeout before the async functions detect it.
//That way we can report back a HTTP status code of 408 "Request Timeout".
#define TIMEOUT_FUDGE_AMOUNT_SECONDS    1

//If a frame takes longer than this (e.g. due to an assert
//or breakpoint) we compensate by adjusting the timeout.
#define TIMEOUT_COMPENSATION_THRESHOLD_SECONDS  5

//Maximum number of in-flight requests.  If this is exceeded further requests
//will wait in the queue.
#define MAX_IN_FLIGHT_REQUESTS  8

#define ALLOW_HTTP_PROXY (!__FINAL || __FINAL_LOGGING)

//Critical section to allow http requests from different threads
static sysCriticalSectionToken s_Cs;

//Queue of in-flight requests.
static netHttpRequest s_NetHttpRequestQueue;

//Number of requests in the queue.
static int s_NetHttpRequestQueueLength;

//if true, all requests must use HTTPS/SSL. Non-HTTPS requests will be rejected.
static bool s_NetHttpRequireHttps = false;

//////////////////////////////////////////////////////////////////////////
//  netHttpFilter
//////////////////////////////////////////////////////////////////////////
//protected:

void
netHttpFilter::DumpHttp(const char* label,
                          const char* str,
                          const unsigned strLen) const
{
    m_HttpRequest->DumpHttp(label, str, strLen);
}

//////////////////////////////////////////////////////////////////////////
//  netHttpRequest
//////////////////////////////////////////////////////////////////////////

//Maximum outbound chunk size is 4/5 times the grow increment of a growbuffer.
//In most cases this will result in the chunk buffer being sent before
//it needs to be grown, as growing the buffer causes the buffer to be copied.
#define MAX_OUTBOUND_CHUNK_SIZE (datGrowBuffer::DEFAULT_GROW_INCR * 4 / 5)

//Maximum number of times a request can be redirected.
#define MAX_REDIRECT_COUNT  10

const char* netHttpRequest::HTTP_VERBS[] =
{
    "INVALID", "GET", "POST", "PUT", "DELETE", "HEAD"
};

static bool IsHttpUri(const char* uri)
{
    return (strstr(uri, "http://") == uri);
}

static bool IsHttpsUri(const char* uri)
{
    return (strstr(uri, "https://") == uri);
}

static unsigned s_NextRequestId;

netHttpRequest::netHttpRequest()
    : m_CurChunk(NULL)
    , m_InFlightChunk(NULL)
    , m_Filter(NULL)
    , m_ResponseHandle(fiHandleInvalid)
    , m_ResponseDevice(NULL)
    , m_BounceBuf(m_DefaultBounceBuf)
    , m_BounceBufMaxLen(sizeof(m_DefaultBounceBuf))
    , m_BounceBufLen(0)
    , m_RequestId(0)
    , m_HttpVerb(NET_HTTP_VERB_INVALID)
    , m_HttpStatusCode(-1)
    , m_UriScheme(URISCHEME_INVALID)
    , m_RawUri(NULL)
    , m_RawHost(NULL)
    , m_RawPath(NULL)
    , m_EncodedUri(NULL)
    , m_InContentLen(0)
    , m_InContentBytesRcvd(0)
    , m_OutContentBytesSent(0)
	, m_OutChunksBytesSent(0)
	, m_IgnoreProxy(false)
    , m_RedirectCount(0)
    , m_Skt(-1)
    , m_SendState(SENDSTATE_NONE)
    , m_RecvState(RECVSTATE_NONE)
    , m_Allocator(NULL)
    , m_Options(0)
	, m_ByteRangeLow(0)
	, m_ByteRangeHigh(0)
    , m_NumFormParams(0)
    , m_NumQueryParams(0)
    , m_NumListParamValues(-1)
    , m_HostIp()
    , m_HostPort(0)
    , m_OutboundTransferEncoding(TRANSFER_ENCODING_NORMAL)
    , m_Status(NULL)
    , m_Initialized(false)
    , m_Committed(false)
	, m_ChunkAllocationError(false)
	, m_AbortReason(NET_HTTPABORT_NONE)
	, m_TcpOpResult(NETTCP_RESULT_OK)
	, m_TcpOpError(0)
    , m_OwnResponseDevice(false)
    , m_bUrlEncode(true)
    , m_SslCtx(NULL)
{
    CompileTimeAssert(COUNTOF(HTTP_VERBS) == NET_HTTP_VERBS_COUNT);

    OUTPUT_ONLY(m_ContextStr[0] = '\0');

#if RSG_DURANGO
	m_UseXblHttp = !PARAM_nethttpnoxhttp.Get();
	m_AllContentReceived = false;
#endif

#if !__FINAL
    m_SimulateFailPct = 0;

    //Make sure we don't initialize the RNG until the PARAM
    //system is initialized because the user could have specified
    //via command line param a fixed seed for all RNGs.
    if(!s_IsNetHttpRngInitialized)
    {
        s_netHttpRng.Reset((int)time(NULL));
        s_IsNetHttpRngInitialized = true;
    }
#endif

    m_Next = m_Prev = this;
}

netHttpRequest::~netHttpRequest()
{
    this->Clear();
}

void
netHttpRequest::Init(sysMemAllocator* allocator, SSL_CTX* sslCtx)
{
    this->Init(allocator, sslCtx, NULL, 0);
}

void
netHttpRequest::Init(sysMemAllocator* allocator,
                    SSL_CTX* sslCtx,
                    void* bounceBuffer,
                    const unsigned maxBounceBufferLen)
{
    this->Clear();
    m_Allocator = allocator;
#if RSG_DURANGO
	m_XblHttpRequest.Init(m_Allocator);
#endif
    m_InBuf.Init(allocator, datGrowBuffer::NULL_TERMINATE);
    m_InHeader.Init(allocator, 0);
    m_OutHeader.Init(allocator, 0);
	m_QueryString.Init(allocator, 0);
	m_UserAgentString.Init(allocator, 0);
    SetSslContext(sslCtx);
    m_BounceBuf = (char*)(bounceBuffer ? bounceBuffer : m_DefaultBounceBuf);
    m_BounceBufMaxLen = bounceBuffer ? maxBounceBufferLen : sizeof(m_DefaultBounceBuf);
    m_Options = 0;
    m_Initialized = true;
}

void
netHttpRequest::Clear()
{

    netAssert(!this->Pending());

    if(this->Pending())
    {
        this->Cancel();
    }

    netAssert(SENDSTATE_SUCCEEDED == m_SendState
            || SENDSTATE_NONE == m_SendState
            || SENDSTATE_ERROR == m_SendState);

    netAssert(RECVSTATE_SUCCEEDED == m_RecvState
            || RECVSTATE_NONE == m_RecvState
            || RECVSTATE_ERROR == m_RecvState);

    this->CloseResponseDevice();

    netAssert(m_QueuedChunks.empty());
#if RSG_DURANGO
	netAssert(m_InFlightChunks.empty());
#endif
    netAssert(!m_InFlightChunk);
    netAssert(!m_ResponseDevice);
    netAssert(fiHandleInvalid == m_ResponseHandle);

    if(m_RawUri)
    {
        this->Free(m_RawUri);
        m_RawUri = NULL;
        m_RawHost = NULL;
        m_RawPath = NULL;
    }

	ClearCachedEncodedUri();

    if(m_CurChunk)
    {
        this->DeleteChunk(m_CurChunk);
        m_CurChunk = NULL;
    }

    if(m_Skt >= 0)
    {
        netTcp::Close(m_Skt);
        m_Skt = -1;
    }

    if(m_Filter)
    {
        m_Filter->m_HttpRequest = NULL;
    }

    m_InBuf.Clear();
    m_InHeader.Clear();
    m_OutHeader.Clear();
    m_QueryString.Clear();
    m_UserAgentString.Clear();
    m_Filter = NULL;
    m_BounceBufLen = 0;
    m_RequestId = 0;
    m_HttpVerb = NET_HTTP_VERB_INVALID;
    m_HttpStatusCode = -1;
    m_UriScheme = URISCHEME_INVALID;
    m_InContentLen = 0;
    m_InContentBytesRcvd = 0;
    m_OutContentBytesSent = 0;
	m_OutChunksBytesSent = 0;
    m_ProxyAddr.Clear();
    m_Timeout.Clear();
    m_RedirectCount = 0;
    m_SendState = SENDSTATE_NONE;
    m_RecvState = RECVSTATE_NONE;
    m_NumFormParams = 0;
    m_NumListParamValues = -1;
    m_NumQueryParams = 0;
    m_HostIp.Clear();
    m_HostPort = 0;
    m_OutboundTransferEncoding = TRANSFER_ENCODING_NORMAL;

    //** DON'T set the m_Allocator to NULL or clear m_Options
    //** DON'T set m_BounceBufMaxLen to zero
    //** DON'T set m_BounceBuf to NULL

    m_Committed = false;
    m_OwnResponseDevice = false;
	m_ChunkAllocationError = false;
	m_AbortReason = NET_HTTPABORT_NONE;
	m_TcpOpResult = NETTCP_RESULT_OK;
	m_TcpOpError = 0;

	SetByteRange(0, 0);

#if RSG_DURANGO
	m_XblHttpRequest.Clear();
#endif

    if(m_Status)
    {
        m_Status->SetFailed();
        m_Status = NULL;
    }
}

void
netHttpRequest::SetOptions(const unsigned options)
{
    m_Options = options;

#if RSG_DURANGO
	if(m_Options & NET_HTTP_USE_RAGE_HTTP)
	{
		m_UseXblHttp = false;
	}
#endif
}

unsigned
netHttpRequest::GetOptions() const
{
    return m_Options;
}

void
netHttpRequest::SetByteRange(unsigned byteRangeLow, unsigned byteRangeHigh)
{
	m_ByteRangeLow = byteRangeLow;
	m_ByteRangeHigh = byteRangeHigh;
}

bool
netHttpRequest::BeginGet(const char* uri,
                         const netSocketAddress* proxyAddr,
                         const unsigned timeoutSecs,
                         datGrowBuffer* responseBuffer,
                         const char* contextStr,
                         netHttpFilter* filter,
                         netStatus* status)
{
    if(!responseBuffer)
    {
        responseBuffer = &m_InBuf;
    }

    if(this->Begin(NET_HTTP_VERB_GET,
                   uri,
                   proxyAddr,
                   timeoutSecs,
                   responseBuffer->GetFiDevice(),
                   responseBuffer->GetFiHandle(),
                   contextStr,
                   filter,
                   status))
    {
        m_OwnResponseDevice = (responseBuffer == &m_InBuf);
        return true;
    }

    return false;
}

bool
netHttpRequest::BeginGet(const char* uri,
                         const netSocketAddress* proxyAddr,
                         const unsigned timeoutSecs,
                         const fiDevice* responseDevice,
                         const fiHandle responseHandle,
                         const char* contextStr,
                         netHttpFilter* filter,
                         netStatus* status)
{
    return this->Begin(NET_HTTP_VERB_GET,
                       uri,
                       proxyAddr,
                       timeoutSecs,
                       responseDevice,
                       responseHandle,
                       contextStr,
                       filter,
                       status);
}

bool
netHttpRequest::BeginGet(const char* uri,
                         const netSocketAddress* proxyAddr,
                         const unsigned timeoutSecs,
                         const char* contextStr,
                         netHttpFilter* filter,
                         netStatus* status)
{
    return this->Begin(NET_HTTP_VERB_GET,
                       uri,
                       proxyAddr,
                       timeoutSecs,
                       NULL,
                       fiHandleInvalid,
                       contextStr,
                       filter,
                       status);
}

bool
netHttpRequest::BeginPost(const char* uri,
                          const netSocketAddress* proxyAddr,
                          const unsigned timeoutSecs,
                          datGrowBuffer* responseBuffer,
                          const char* contextStr,
                          netHttpFilter* filter,
                          netStatus* status)
{
    if(!responseBuffer)
    {
        responseBuffer = &m_InBuf;
    }

    if(this->Begin(NET_HTTP_VERB_POST,
                   uri,
                   proxyAddr,
                   timeoutSecs,
                   responseBuffer->GetFiDevice(),
                   responseBuffer->GetFiHandle(),
                   contextStr,
                   filter,
                   status))
    {
        m_OwnResponseDevice = (responseBuffer == &m_InBuf);
        return true;
    }

    return false;
}

bool
netHttpRequest::BeginPost(const char* uri,
                          const netSocketAddress* proxyAddr,
                          const unsigned timeoutSecs,
                          const fiDevice* responseDevice,
                          const fiHandle responseHandle,
                          const char* contextStr,
                          netHttpFilter* filter,
                          netStatus* status)
{
    return this->Begin(NET_HTTP_VERB_POST,
                       uri,
                       proxyAddr,
                       timeoutSecs,
                       responseDevice,
                       responseHandle,
                       contextStr,
                       filter,
                       status);
}

bool
netHttpRequest::BeginPost(const char* uri,
                          const netSocketAddress* proxyAddr,
                          const unsigned timeoutSecs,
                          const char* contextStr,
                          netHttpFilter* filter,
                          netStatus* status)
{
    return this->Begin(NET_HTTP_VERB_POST,
                       uri,
                       proxyAddr,
                       timeoutSecs,
                       NULL,
                       fiHandleInvalid,
                       contextStr,
                       filter,
                       status);
}

bool
netHttpRequest::BeginPut(const char* uri,
						const netSocketAddress* proxyAddr,
						const unsigned timeoutSecs,
						datGrowBuffer* responseBuffer,
						const char* contextStr,
						netHttpFilter* filter,
						netStatus* status)
{
	if(!responseBuffer)
	{
		responseBuffer = &m_InBuf;
	}

	if(this->Begin(NET_HTTP_VERB_PUT,
		uri,
		proxyAddr,
		timeoutSecs,
		responseBuffer->GetFiDevice(),
		responseBuffer->GetFiHandle(),
		contextStr,
		filter,
		status))
	{
		m_OwnResponseDevice = (responseBuffer == &m_InBuf);
		return true;
	}

	return false;
}

bool
netHttpRequest::BeginPut(const char* uri,
						const netSocketAddress* proxyAddr,
						const unsigned timeoutSecs,
						const fiDevice* responseDevice,
						const fiHandle responseHandle,
						const char* contextStr,
						netHttpFilter* filter,
						netStatus* status)
{
	return this->Begin(NET_HTTP_VERB_PUT,
		uri,
		proxyAddr,
		timeoutSecs,
		responseDevice,
		responseHandle,
		contextStr,
		filter,
		status);
}

bool
netHttpRequest::BeginPut(const char* uri,
						const netSocketAddress* proxyAddr,
						const unsigned timeoutSecs,
						const char* contextStr,
						netHttpFilter* filter,
						netStatus* status)
{
	return this->Begin(NET_HTTP_VERB_PUT,
		uri,
		proxyAddr,
		timeoutSecs,
		NULL,
		fiHandleInvalid,
		contextStr,
		filter,
		status);
}

bool
netHttpRequest::BeginDelete(const char* uri,
                            const netSocketAddress* proxyAddr,
                            const unsigned timeoutSecs,
                            datGrowBuffer* responseBuffer,
                            const char* contextStr,
                            netHttpFilter* filter,
                            netStatus* status)
{
    if(!responseBuffer)
    {
        responseBuffer = &m_InBuf;
    }

    if(this->Begin(NET_HTTP_VERB_DELETE,
                   uri,
                   proxyAddr,
                   timeoutSecs,
                   responseBuffer->GetFiDevice(),
                   responseBuffer->GetFiHandle(),
                   contextStr,
                   filter,
                   status))
    {
        m_OwnResponseDevice = (responseBuffer == &m_InBuf);
        return true;
    }

    return false;
}

bool
netHttpRequest::BeginDelete(const char* uri,
                            const netSocketAddress* proxyAddr,
                            const unsigned timeoutSecs,
                            const fiDevice* responseDevice,
                            const fiHandle responseHandle,
                            const char* contextStr,
                            netHttpFilter* filter,
                            netStatus* status)
{
    return this->Begin(NET_HTTP_VERB_DELETE,
                       uri,
                       proxyAddr,
                       timeoutSecs,
                       responseDevice,
                       responseHandle,
                       contextStr,
                       filter,
                       status);
}

bool
netHttpRequest::BeginHead(const char* uri,
							const netSocketAddress* proxyAddr,
							const unsigned timeoutSecs,
							const char* contextStr,
							netStatus* status)
{
	return this->Begin(NET_HTTP_VERB_HEAD,
		uri,
		proxyAddr,
		timeoutSecs,
		NULL,
		fiHandleInvalid,
		contextStr,
		0, //filter
		status);
}

bool
netHttpRequest::BeginHead(const char* uri,
							const netSocketAddress* proxyAddr,
							const unsigned timeoutSecs,
							const char* contextStr,
                            netHttpFilter* filter,
							netStatus* status)
{
	return this->Begin(NET_HTTP_VERB_HEAD,
		uri,
		proxyAddr,
		timeoutSecs,
		NULL,
		fiHandleInvalid,
		contextStr,
		filter,
		status);
}

netHttpVerb
netHttpRequest::GetHttpVerb() const
{
    return m_HttpVerb;
}

const char*
netHttpRequest::GetHttpVerbString() const
{
    return HTTP_VERBS[m_HttpVerb];
}

bool
netHttpRequest::Commit()
{
    netDebug2("Committing request to '%s'...", this->GetUri());

    rtry
    {
        rverify(NET_HTTP_VERB_POST == m_HttpVerb
                || NET_HTTP_VERB_GET == m_HttpVerb
                || NET_HTTP_VERB_DELETE == m_HttpVerb
				|| NET_HTTP_VERB_HEAD == m_HttpVerb
				|| NET_HTTP_VERB_PUT == m_HttpVerb,
                catchall,
                netError("Can't commit HTTP request - Unknown HTTP verb: %d", m_HttpVerb));

        rverify(!IsCommitted(), catchall, netError("Can't commit HTTP request - Already committed"));

        rverify(m_SendState > SENDSTATE_NONE && m_SendState < SENDSTATE_SENDING_TERM_CHUNK,
                catchall,
                netError("Can't commit HTTP request - invalid state: %d", m_SendState));

        rverify(m_CurChunk, catchall, netError("Can't commit HTTP request - NULL chunk"));

        //Allow request filter to add data after request body (like a hash)
        if(m_Filter && m_Filter->CanFilterRequest())
        {
            rverify(m_Filter->FilterRequest(0, 0, true, m_CurChunk->m_Data),
                    catchall,
                    netError("Error filtering request"));
        }

        //Queue the final chunk
        if(m_CurChunk->m_Data.Length())
        {
            m_QueuedChunks.push_back(m_CurChunk);
        }
        else
        {
            this->DeleteChunk(m_CurChunk);
        }

        m_CurChunk = NULL;
        m_NumListParamValues = -1;
        m_Committed = true;

        return true;
    }
    rcatchall
    {
    }

    this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_PRECONDITION_FAILED, NET_HTTPABORT_COMMIT_FAILED);

    return false;
}

bool
netHttpRequest::HaveProxyAddress() const
{
#if ALLOW_HTTP_PROXY
	bool disableProxyOnHttps = (URISCHEME_HTTPS == m_UriScheme) && PARAM_nethttpsnoproxy.Get();

    //Don't call m_ProxyAddr.IsValid().
    //Only check for a valid IP.  We permit a port value of zero, which
    //means use the best guess (80 for HTTP, 443 for HTTPS).
    return !m_IgnoreProxy && m_ProxyAddr.GetIp().IsValid() && !disableProxyOnHttps;
#else
	return false;
#endif
}

netSocketAddress 
netHttpRequest::GetProxyAddress() const
{
	netSocketAddress addr;

	if(this->HaveProxyAddress())
	{
		if(m_ProxyAddr.GetPort())
		{
			addr = m_ProxyAddr;
		}
		else
		{
			addr.Init(m_ProxyAddr.GetIp(), m_HostPort);
		}

		//If port is zero then use 80 for HTTP and 443 for HTTPS.
		if(!addr.GetPort())
		{
			const bool useHttps = (URISCHEME_HTTPS == m_UriScheme);
			const unsigned short port = useHttps ? HTTPS_PORT : HTTP_PORT;
			addr.Init(addr.GetIp(), port);
		}
	}

	return addr;
}

void 
netHttpRequest::SetIgnoreProxy(bool ignoreProxy)
{
	m_IgnoreProxy = ignoreProxy;
	m_ProxyAddr.Clear();
}

bool 
netHttpRequest::UseHttpsTunnel() const
{
#if ENABLE_TCP_PROXY
	const bool useHttps = (URISCHEME_HTTPS == m_UriScheme);
	return useHttps && HaveProxyAddress() && PARAM_nethttpstunnel.Get();
#else
	return false;
#endif
}

bool
netHttpRequest::HasRequestHeader(const char* name) const
{
    return HasHeaderValue(m_OutHeader.ToString(), name);
}

bool
netHttpRequest::RemoveRequestHeader(const char* name)
{
    if(netVerify(m_SendState > SENDSTATE_NONE)
        && netVerifyf(m_SendState <= SENDSTATE_SEND_HEADER,
                    "Can't remove a request header because the header has already been sent"))
    {
		const char* headers = m_OutHeader.ToString();

		if(HasHeaderValue(headers, name))
		{
			const char* line = stristr(headers, name);
			if(line)
			{
				const char* eol = strstr(line, CRLF);
				if(eol)
				{
					eol = eol + strlen(CRLF);
					m_OutHeader.Remove(ptrdiff_t_to_int(line - headers), ptrdiff_t_to_int(eol - line));
					return true;
				}
			}
		}
    }

    return false;
}

bool
netHttpRequest::AddRequestHeaders(const char* headers)
{
    if(netVerify(m_SendState > SENDSTATE_NONE)
        && netVerifyf(m_SendState <= SENDSTATE_SEND_HEADER,
                    "Can't add add a request header because the header has already been sent" )
        && netVerify(headers)
        && netVerify(headers[0] != '\0'))
    {
        netDebug("Headers: %s", headers);
        return netVerifyf(m_OutHeader.AppendOrFail(headers),
                        "Error adding HTTP header: %s", headers);
    }

    return false;
}

bool
netHttpRequest::AddRequestHeaderValue(const char* name, const char* value)
{

    bool success = false;
    if(netVerify(m_SendState > SENDSTATE_NONE)
        && netVerifyf(m_SendState <= SENDSTATE_SEND_HEADER,
                    "Can't add add a request header because the header has already been sent" )
        && netVerify(name)
        && netVerify(name[0] != '\0')
        && netVerify(value)
        && netVerify(value[0] != '\0')
        && netVerifyf(!HasHeaderValue(m_OutHeader.ToString(), name),
                        "Already have request header '%s'", name))
    {
        netDebug("Header: %s : %s", name, value);
		unsigned valBufLen = ustrlen(value) + 1;
		char* valBuf = (char*)Alloca(char, valBufLen);
		char buf[1024];
        success = netVerifyf(m_OutHeader.AppendOrFail(StringTrim(buf, name, sizeof(buf)))
                    && m_OutHeader.AppendOrFail(": ")
                    && m_OutHeader.AppendOrFail(StringTrim(valBuf, value, valBufLen))
                    && m_OutHeader.AppendOrFail(CRLF),
                    "Error adding HTTP header: %s", name);
    }

    return success;
}

bool
netHttpRequest::AddRequestHeaderValue(const char* name, const char* value, const unsigned valLen)
{
    bool success = false;
    if(netVerify(m_SendState > SENDSTATE_NONE)
        && netVerifyf(m_SendState <= SENDSTATE_SEND_HEADER,
                    "Can't add add a request header because the header has already been sent" )
        && netVerify(name)
        && netVerify(name[0] != '\0')
        && netVerify(value)
		&& netVerify(valLen > 0)
        && netVerifyf(!HasHeaderValue(m_OutHeader.ToString(), name),
                        "Already have request header '%s'", name))
    {
        netDebug("Header: %s : %s", name, value);
        char buf[1024];
        success = netVerifyf(m_OutHeader.AppendOrFail(StringTrim(buf, name, sizeof(buf)))
                    && m_OutHeader.AppendOrFail(": ")
                    && m_OutHeader.AppendOrFail(value, valLen)
                    && m_OutHeader.AppendOrFail(CRLF),
                    "Error adding HTTP header: %s", name);
    }

    return success;
}

bool
netHttpRequest::AddBasicAuth(const char* username, const char* password)
{
    char baStr[128];
    char baStr64[128] = {"Basic "};
    formatf(baStr, "%s:%s", username, password);
    const int baLen = istrlen(baStr);
    const int dstLen = istrlen(baStr64);

    return netVerifyf(datBase64::Encode((const u8*) baStr,
                                        baLen,
                                        baStr64 + dstLen,
                                        sizeof(baStr64) - dstLen,
                                        NULL),
                        "Error encoding data to base64")
            && netVerifyf(this->AddRequestHeaderValue("Authorization", baStr64),
                        "Error adding 'Authorization' header");
}

bool
netHttpRequest::AppendUserAgentString(const char* userAgentString)
{
    bool success = true;

    if (userAgentString == nullptr || userAgentString[0] == '\0')
    {
        return true;
    }

    if(m_UserAgentString.Length() > 0)
    {
        success = success && m_UserAgentString.Append(' ') > 0;
    }

    success = success && (m_UserAgentString.Append(userAgentString) > 0);

    if(success)
    {
        netDebug("User agent: %s", m_UserAgentString.ToString());
    }

    return success;
}

static void FormatInt(const s64 value, char* buf, int maxLen)
{
    formatf(buf, maxLen, "%" I64FMT "d", value);
}

static void FormatUns(const u64 value, char* buf, int maxLen)
{
    formatf(buf, maxLen, "%" I64FMT "u", value);
}

static void FormatDouble(const double value,
                        const int precision,
                        char* buf,
                        int maxLen)
{
    formatf(buf, maxLen, "%#.*g", precision, value);
}

bool
netHttpRequest::AddFormParam(const char* name,
                                const char* value,
                                const unsigned valLen)
{
    netDebug("Param (%s): %s=%s", this->GetPath(), name, value?value:"(null)");

    bool success = false;

    rtry
    {
        //End the current list param, if any.
        m_NumListParamValues = -1;
		
        rverify((NET_HTTP_VERB_POST == m_HttpVerb) || (NET_HTTP_VERB_GET == m_HttpVerb) || (NET_HTTP_VERB_PUT == m_HttpVerb),catchall,);
        rverify(!this->IsCommitted(),catchall,);
        rverify(name,catchall,);
        rverify(strlen(name),catchall,);

        char tmp[2048];
        formatf(tmp, "%s%s=", (m_NumFormParams ? "&" : ""), name);
        success = netVerify(this->QueueRawData(tmp, istrlen(tmp)));
        if(success && value && valLen)
        {
            success = this->QueueUrlEncodedData(value, valLen);
        }

        ++m_NumFormParams;

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool
netHttpRequest::AddQueryParam(const char* name,
                                const char* value,
                                const unsigned valLen)
{
    netDebug("Param (%s): %s=%s", this->GetPath(), name, value);

    //End the current list param, if any.
    m_NumListParamValues = -1;

    bool success = false;
    if(netVerify(!this->IsCommitted())
        && netVerify(name)
        && netVerify(strlen(name)))
    {
		// clear the cached URI if there is one since query params are added to the URI
		ClearCachedEncodedUri();

        char tmp[1024];
        formatf(tmp, "%s%s=", (m_NumQueryParams ? "&" : "?"), name);
        success = netVerifyf(m_QueryString.AppendOrFail(tmp),
                            "Error adding query param: %s", name);
        if(success && value && valLen)
        {
            const char* src = (const char*) value;
            const char* eos = src + valLen;

            while(src < eos && success)
            {
                char dst[2048];
                unsigned dstLen = COUNTOF(dst);
                unsigned numConsumed;

                if (this->m_bUrlEncode)
                {
                    if (netHttpRequest::UrlEncode(dst, &dstLen, src, ptrdiff_t_to_int(eos - src), &numConsumed, NULL))
                    {
                        if (netVerifyf(m_QueryString.AppendOrFail(dst, dstLen),
                            "Error adding UrlEncoded query param: %s", name))
                        {
                            src += numConsumed;
                        }
                        else
                        {
                            success = false;
                        }
                    }
                }
                else // Just copy, UrlEncode is disabled
                {
                    // Make sure value len does not exceed the dstLen
                    if (dstLen < valLen)
                    {
                        netError("Param %s value length %d exceeds destination: %d", name, valLen, dstLen);
                        success = false;
                    }
                    else
                    {
                        safecpy(dst, src, dstLen);
                        if (netVerifyf(m_QueryString.AppendOrFail(dst, istrlen(dst)),
                            "Error adding non-UrlEncoded query param: %s", name))
                        {
                            src += dstLen;
                        }
                        else
                        {
                            success = false;
                        }
                    }
                }
            }
        }
    }

    if(success){++m_NumQueryParams;}

    return success;
}

bool
netHttpRequest::AddFormParam(const char* name, const char* value)
{
    return this->AddFormParam(name, value, istrlen(value));
}

bool
netHttpRequest::AddIntFormParam(const char* name, const s64 value)
{
    char tmp[64];
    FormatInt(value, tmp, sizeof(tmp));
    return this->AddFormParam(name, tmp, istrlen(tmp));
}

bool
netHttpRequest::AddUnsFormParam(const char* name, const u64 value)
{
    char tmp[64];
    FormatUns(value, tmp, sizeof(tmp));
    return this->AddFormParam(name, tmp, istrlen(tmp));
}

bool
netHttpRequest::AddDoubleFormParam(const char* name,
                                const double value,
                                const int precision)
{
    char tmp[64];
    FormatDouble(value, precision, tmp, sizeof(tmp));
    return this->AddFormParam(name, tmp, istrlen(tmp));
}

bool
netHttpRequest::AddBinaryFormParam(const char* name,
                                    const void* value,
                                    const unsigned len)
{
    bool success = false;
    char* buf = 0;

    rtry
    {
        rverify(value,catchall,);
        rverify(len,catchall,);

        unsigned maxSize =
            datBase64::GetMaxEncodedSize(len);

        buf = (char*)this->Allocate(maxSize);
        rcheck(buf, catchall, netError("Failed to allocate buffer for <%s> (%u bytes, %u bytes encoded)", name, len, maxSize));

        if(maxSize)
        {
            unsigned charsUsed = 0;
            rcheck(datBase64::Encode((const u8*) value, len, buf, maxSize, &charsUsed), 
                   catchall, 
                   netError("Failed to encode data as base-64 string"));
        }

        success = AddFormParam(name, buf);
    }
    rcatchall
    {        
    }

    if(buf)
    {
        this->Free(buf);
        buf = 0;
    }

    return success;
}

bool
netHttpRequest::BeginListFormParam(const char* name)
{
    bool success = false;

    if(this->AddFormParam(name, NULL, 0))
    {
        netDebug2("List param (%s): %s", this->GetPath(), name);

        m_NumListParamValues = 0;
        success = true;
    }

    return success;
}

bool
netHttpRequest::AddListFormParamValue(const char* value)
{
    netDebug("    List value: %s", value);

    bool success = false;
    if(netVerify(m_NumListParamValues >= 0))
    {
        char tmp[1024];
        formatf(tmp, "%s%s", m_NumListParamValues ? "," : "", value);
        success = this->QueueUrlEncodedData(tmp, istrlen(tmp));
        if(success){++m_NumListParamValues;}
    }

    return success;
}

bool
netHttpRequest::AddListFormParamIntValue(const s64 value)
{
    char tmp[64];
    FormatInt(value, tmp, sizeof(tmp));
    return this->AddListFormParamValue(tmp);
}

bool
netHttpRequest::AddListFormParamUnsValue(const u64 value)
{
    char tmp[64];
    FormatUns(value, tmp, sizeof(tmp));
    return this->AddListFormParamValue(tmp);
}

bool
netHttpRequest::AddListFormParamDoubleValue(const double value, const int precision)
{
    char tmp[64];
    FormatDouble(value, precision, tmp, sizeof(tmp));
    return this->AddListFormParamValue(tmp);
}

unsigned
netHttpRequest::GetFormParameterCount() const
{
    return m_NumFormParams;
}

unsigned
netHttpRequest::GetQueryParameterCount() const
{
    return m_NumQueryParams;
}

bool
netHttpRequest::AppendContentUrlEncoded(const void* data, const unsigned len)
{
#if !__NO_OUTPUT
    const char* p = (const char*) data;
    const char* eob = p + len;
    size_t lenRemaining = len;
    char buf[1024];
    const size_t sizeofBuf = sizeof(buf)-1;
    for(; p < eob; p += sizeofBuf, lenRemaining -= sizeofBuf)
    {
        const size_t cplen = sizeofBuf < lenRemaining ? sizeofBuf : lenRemaining;
        sysMemCpy(buf, p, cplen);
        buf[cplen] = '\0';

        netDebug("    Data: %s", buf);
    }
#endif  //__NO_OUTPUT

    netAssert(len);
    if(netVerify(NET_HTTP_VERB_POST == m_HttpVerb || NET_HTTP_VERB_PUT == m_HttpVerb)
        && netVerify(!this->IsCommitted()))
    {
        return this->QueueUrlEncodedData(data, len);
    }

    return false;
}

bool
netHttpRequest::AppendContent(const void* data, const unsigned len)
{
    netAssert(len);
    if(netVerify(NET_HTTP_VERB_POST == m_HttpVerb || NET_HTTP_VERB_PUT == m_HttpVerb)
        && netVerify(!this->IsCommitted()))
    {
        return this->QueueRawData(data, len);
    }

    return false;
}

bool
netHttpRequest::AppendContentZeroCopy(const void* data, const unsigned len)
{
    netAssert(len);
    if(netVerify(NET_HTTP_VERB_POST == m_HttpVerb || NET_HTTP_VERB_PUT == m_HttpVerb)
        && netVerify(!this->IsCommitted()))
    {
        return this->QueueRawDataZeroCopy(data, len);
    }

    return false;
}

const char*
netHttpRequest::GetUri() const
{
    if(m_RawUri)
    {
        if(!m_EncodedUri)
        {
            const char* schemeStr = (URISCHEME_HTTPS == m_UriScheme) ? "https" : "http";
            char hostPortStr[64] = {""};
            if(m_HostPort)
            {
                formatf(hostPortStr, ":%d", m_HostPort);
            }

            const unsigned encPathLen = GetUrlEncodedLength(m_RawPath, istrlen(m_RawPath), "/");

            const unsigned uriLen =
                istrlen(schemeStr)
                + istrlen("://")
                + istrlen(m_RawHost)
                + istrlen(hostPortStr)
                + istrlen("/")
                + encPathLen
                + m_QueryString.Length()
                + 1;

            m_EncodedUri = (char*) this->Allocate(uriLen);

            if(m_EncodedUri)
            {
                formatf(m_EncodedUri, uriLen, "%s://%s%s",
                        schemeStr,
                        GetHost(),
                        hostPortStr);
                
                unsigned encLen = uriLen - istrlen(m_EncodedUri);
                unsigned numConsumed;
                netVerify(UrlEncode(&m_EncodedUri[strlen(m_EncodedUri)], &encLen, m_RawPath, istrlen(m_RawPath), &numConsumed, "/"));
                netAssert(encPathLen == encLen);
                safecat(m_EncodedUri, m_QueryString.ToString(), uriLen);
            }
        }
    }

    return m_EncodedUri ? m_EncodedUri : "";
}

const char*
netHttpRequest::GetHost() const
{
    return m_RawHost;
}

const char*
netHttpRequest::GetPath() const
{
    return m_RawPath;
}

bool
netHttpRequest::Pending() const
{
    if(SENDSTATE_NONE == m_SendState && RECVSTATE_NONE == m_RecvState)
    {
        return false;
    }

    if(SENDSTATE_SUCCEEDED == m_SendState && RECVSTATE_SUCCEEDED == m_RecvState)
    {
        return false;
    }

    if(SENDSTATE_ERROR == m_SendState || RECVSTATE_ERROR == m_RecvState)
    {
        return false;
    }

    return true;
}

bool
netHttpRequest::Succeeded() const
{
    if(SENDSTATE_SUCCEEDED == m_SendState && RECVSTATE_SUCCEEDED == m_RecvState)
    {
        const int statusCode = this->GetStatusCode();
        return (statusCode >= 200 && statusCode < 300)
                //304 means "not modified".  Sent in response to a request containing
                //the "If-Modified-Since" header.
                || NET_HTTPSTATUS_NOT_MODIFIED == statusCode;
    }

    return false;
}

void
netHttpRequest::Cancel(netHttpStatusCodes statusCode)
{
    netDebug("Canceling request to: %s", GetUri());
    this->Abort(netStatus::NET_STATUS_CANCELED, statusCode, NET_HTTPABORT_CANCELED);
}

void
netHttpRequest::Update()
{
#if !__FINAL
    if(m_SimulateFailPct)
    {
        netDebug("Simulating failure %d%% of the time...", m_SimulateFailPct);
        if(s_netHttpRng.GetRanged(1, 100) <= m_SimulateFailPct)
        {
            netDebug("  Failure simulated!");

            //Allow status code override
            int statusCode;
            if(!PARAM_simulatedFailStatusCode.Get(statusCode))
            {
                statusCode = NET_HTTPSTATUS_BAD_REQUEST;
            }

            this->Cancel(static_cast<netHttpStatusCodes>(statusCode));
        }
        else
        {
            netDebug("  Failure averted!");
        }

        m_SimulateFailPct = 0;
    }
#endif

    if(!this->Pending())
    {
        return;
    }

    //If a frame takes too long then adjust the timeout by the
    //frame duration.  This compensates for cases where the game
    //is stuck on an assert or breakpoint for too long.
    const unsigned msSinceTimeoutUpdate = m_Timeout.GetMillisecondsSinceLastUpdate();
    if(msSinceTimeoutUpdate >= TIMEOUT_COMPENSATION_THRESHOLD_SECONDS*1000)
    {
        netWarning("Frame time was %u milliseconds - adjusting timeout...", msSinceTimeoutUpdate);
        m_Timeout.ExtendMilliseconds(msSinceTimeoutUpdate);
    }

    m_Timeout.Update();

    if(m_Timeout.IsTimedOut())
    {
        netDebug("HTTP request to '%s' timed out after %d seconds",
                GetUri(),
                m_Timeout.GetTimeoutIntervalSeconds());
        if(m_BounceBufLen > 0 && NET_HTTP_VERB_GET == m_HttpVerb)
        {
            netWarning("  Timeout could be due to insufficient space in destination buffer");
        }

        this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_REQUEST_TIMEOUT, NET_HTTPABORT_REQUEST_TIMED_OUT);
    }

    if(!this->Pending())
    {
        return;
    }

    //Check if we're ready to run prior to entering the main
    //switch statement.  In the best case this could save 2 frames
    //of delay in issuing the request.  One frame for checking
    //SENDSTATE_WAIT_UNTIL_READY and one state for checking
    //SENDSTATE_WAIT_FOR_XHTTP
    switch((int)m_SendState)
    {
    case SENDSTATE_WAIT_UNTIL_READY:
        if(this->CanRun())
        {
            netDebug("Starting (queue length %d)...", s_NetHttpRequestQueueLength);
            {
#if RSG_DURANGO
				if(m_UseXblHttp)
				{
					// IXHR2 resolves the address internally
					m_SendState = SENDSTATE_CONNECT;
				}
				else if(HasRequestHeader(netXblHttp::GetAuthActorHeaderName()))
				{
					// if we're not using IXHR2, then we need to add the authorization
					// header manually if the higher-level code has requested it. 
					m_SendState = SENDSTATE_GET_AUTH_TOKEN;
				}
				else
				{
					m_SendState = SENDSTATE_RESOLVE_HOST;
				}					
#else
                m_SendState = SENDSTATE_RESOLVE_HOST;
#endif
            }
        }
        break;

#if RSG_DURANGO
	case SENDSTATE_GET_AUTH_TOKEN:
		netAssert(m_UseXblHttp == false);

		// To get the auth token, we need the full body of the request first,
		// so wait until we're committed.
		if(this->IsCommitted())
		{
			// if we fail to get the auth token, send the request anyway
			// and let the server decide if it wants to accept it
			m_SendState = SENDSTATE_RESOLVE_HOST;

			char xblUserHash[64] = {0};
			unsigned valLen = sizeof(xblUserHash);
			if(GetHeaderValue(m_OutHeader.ToString(), netXblHttp::GetAuthActorHeaderName(), xblUserHash, &valLen))
			{
				datGrowBuffer m_Body;
				m_Body.Init(m_Allocator, 0);
				RemoveRequestHeader(netXblHttp::GetAuthActorHeaderName());

				unsigned totalSize = 0;
				bool success = true;
				for (ChunkList::const_iterator it = m_QueuedChunks.begin(); it != m_QueuedChunks.end(); ++it)
				{
					totalSize += (*it)->m_Data.Length();
					success = m_Body.AppendOrFail((*it)->m_Data.GetBuffer(), (*it)->m_Data.Length());
					if(!success)
					{
						break;
					}
				}
				
				if(success && netXblHttp::GetAuthToken(GetHttpVerbString(),
													   GetUri(),
													   m_OutHeader.ToString(),
													   (u8*)m_Body.GetBuffer(),
													   m_Body.Length(),
													   xblUserHash,
													   &m_AuthToken,
													   &m_AuthTokenStatus))
				{
					m_SendState = SENDSTATE_GETTING_AUTH_TOKEN;
				}
				else
				{
					this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_XBOX_AUTH_TOKEN_FAILED);
				}
			}
		}
		break;
	case SENDSTATE_GETTING_AUTH_TOKEN:
		if(m_AuthTokenStatus.Succeeded())
		{
			if(m_AuthToken.m_Token && (m_AuthToken.m_Token[0] != '\0'))
			{
				AddRequestHeaderValue("Authorization", m_AuthToken.m_Token);
			}

			if(m_AuthToken.m_Signature && (m_AuthToken.m_Signature[0] != '\0'))
			{
				AddRequestHeaderValue("Signature", m_AuthToken.m_Signature);
			}

			netXblHttp::FreeAuthToken(&m_AuthToken);

			m_SendState = SENDSTATE_RESOLVE_HOST;
		}
		else if(!m_AuthTokenStatus.Pending())
		{
			// If we need an auth token but we failed to get it we fail the request
			netError("Error getting xbox auth token");
			this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_XBOX_AUTH_TOKEN_FAILED);
		}
		break;
#endif
    }

    switch(m_SendState)
    {
    case SENDSTATE_NONE:
    case SENDSTATE_WAIT_UNTIL_READY:
        break;
    case SENDSTATE_RESOLVE_HOST:
        if(netVerify(this->ResolveHost(&m_HostResolutionStatus)))
        {
            m_SendState = SENDSTATE_RESOLVING_HOST;
        }
        else
        {
            netError("Error resolving host");
            this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_RESOLVE_HOST_FAILED);
        }
        break;
    case SENDSTATE_RESOLVING_HOST:
        if(!m_HostResolutionStatus.Pending())
        {
			if(m_HostResolutionStatus.Succeeded())
			{
				m_SendState = SENDSTATE_CONNECT;
			}
			else
			{
				netError("Error resolving host");
				this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_RESOLVING_HOST_FAILED);
			}
        }
        break;
    case SENDSTATE_CONNECT:
#if ENABLE_HTTP_INTERCEPTOR
		if(m_Filter && (m_Filter->DelayMs() > 0))
		{
			// injecting latency
			netDebug2("Filter injecting latency: %ums remaining before sending request...", m_Filter->DelayMs());
		}
		else
#endif
        if(netVerify(this->Connect()))
        {
#if RSG_DURANGO
			if(m_UseXblHttp)
			{
				m_SendState = SENDSTATE_SEND_HEADER;
			}
			else
#endif
            {
                m_SendState = SENDSTATE_CONNECTING;
            }
        }
        else
        {
            netError("Error connecting to host");
            this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_CONNECT_HOST_FAILED);
        }
        break;
    case SENDSTATE_CONNECTING:
#if ENABLE_HTTP_INTERCEPTOR
		if(m_Filter && m_Filter->CanIntercept())
		{
			m_SendState = SENDSTATE_SEND_HEADER;
		}
		else
#endif
		if(!m_ConnectTcpOp.Pending())
		{
			if(m_ConnectTcpOp.Succeeded())
			{
				m_SendState = SENDSTATE_SEND_HEADER;
			}
			else
			{
				m_TcpOpResult = (netTcpResult)m_ConnectTcpOp.GetResultCode();
				m_TcpOpError = m_ConnectTcpOp.m_LastError;
				netError("Error connecting to host");
				this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_CONNECTING_HOST_FAILED);
			}
		}
        break;

#if RSG_DURANGO
	case SENDSTATE_GET_AUTH_TOKEN:
	case SENDSTATE_GETTING_AUTH_TOKEN:
		break;
#endif

    case SENDSTATE_SEND_HEADER:
        //Wait until we've queued a chunk and/or called Commit()
        //before we send the header.
        if(!m_QueuedChunks.empty() || this->IsCommitted())
        {
            //If we're committed then send it no matter what
            bool sendIt = this->IsCommitted();

            if(!sendIt)
            {
                //Not committed.  Are we allowed to send with chunked encoding?
                if(!(m_Options & NET_HTTP_FORCE_TRANSFER_ENCODING_NORMAL))
                {
                    //If we're POSTing then start sending.
                    //The transfer encoding will be chunked.
                    sendIt = (NET_HTTP_VERB_POST == m_HttpVerb || NET_HTTP_VERB_PUT == m_HttpVerb);
                }
                else
                {
                    //Not allowed to send chunked, so don't
                    //send until we've committed, i.e. until we
                    //know the full size of the data.
                }
            }

            if(sendIt)
            {
                if(this->SendHeader())
                {
					m_SendState = SENDSTATE_SENDING_CONTENT;
					m_RecvState = RECVSTATE_RECEIVING_HEADER;
                }
                else
                {
                    netError("Error sending header");
                    this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_SEND_HEADER_FAILED);
                }
            }
        }
        break;
    case SENDSTATE_SENDING_CONTENT:
    case SENDSTATE_SENDING_TERM_CHUNK:
        ProcessOutboundChunks();
        break;
    case SENDSTATE_SUCCEEDED:
    case SENDSTATE_ERROR:
        break;
    }

    switch(m_RecvState)
    {
    case RECVSTATE_NONE:
        break;
    case RECVSTATE_RECEIVING_HEADER:
        if(!this->ReceiveHeader())
        {
            this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_RECEIVE_HEADER_FAILED);
        }
        else if(HaveResponseHeader())
        {
            m_RecvState = RECVSTATE_RECEIVED_HEADER;
        }
        break;
    case RECVSTATE_RECEIVED_HEADER:
        this->DumpHttp("Inbound header", 
                        this->GetResponseHeader(),
                        istrlen(this->GetResponseHeader()));

#if !__NO_OUTPUT
        char statusCode[32];
        this->GetStatusCodeString(statusCode, COUNTOF(statusCode));
        netDebug2("Received status code:%s", statusCode);
#endif  //__NO_OUTPUT

        //Allow the filter to respond to the header
        if (m_Filter)
        {
            if (!m_Filter->ProcessResponseHeader(this->GetStatusCode(), this->GetResponseHeader()))
            {
                netError("Filter failed to process response header");
                this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_PROCESS_RESPONSE_HEADER_FAILED);
            }
        }

        //304 means "not modified".  Sent in response to a request containing
        //the "If-Modified-Since" header.
        if(this->GetStatusCode() != NET_HTTPSTATUS_NOT_MODIFIED
            && this->GetStatusCode() >= 300
            && this->GetStatusCode() < 400
            && (NET_HTTP_VERB_GET == m_HttpVerb || NET_HTTP_VERB_HEAD == m_HttpVerb))
        {
            netDebug2("Redirected...");

            //Delete previous header chunk as we will attempt to send another
            if(m_InFlightChunk)
            {
				DeleteInFlightChunk();
                m_InFlightChunk = NULL;
            }

            unsigned locLen = 0;
            this->GetResponseHeaderValue("Location", NULL, &locLen);
            if(0 == locLen)
            {
                netError("No 'Location:' header found in response");
                this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_RESPONSE_NO_LOCATION_LENGTH);
            }
            else
            {
                ++locLen;
                char* location = Alloca(char, locLen);
                if(this->GetResponseHeaderValue("Location", location, &locLen))
                {
                    if(!this->Redirect(location))
                    {
                        netError("Error redirecting to '%s'", location);
                        this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_RESPONSE_REDIRECT_FAILED);
                    }
                }
                else
                {
                    netError("No 'Location:' header found in response");
                    this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_RESPONSE_GET_LOCATION_FAILED);
                }
            }
        }
        else
        {
            char val[1024];
            unsigned valLen = COUNTOF(val);

            TransferEncoding transferEncoding = TRANSFER_ENCODING_NORMAL;

            if(this->GetResponseHeaderValue("Transfer-Encoding", val, &valLen))
            {
                if(0 == strcasecmp("chunked", val))
                {
                    netDebug2("Response is chunked");
                    transferEncoding = TRANSFER_ENCODING_CHUNKED;
                }
                else if(0 != strcasecmp("identity", val))
                {
                    netError("Unhandled transfer encoding: '%s'", val);
                    this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_RESPONSE_UNHANDLED_TRANSFER_ENCODING);
                }
            }

            if(this->Pending()) //No errors and still pending?
            {
                valLen = COUNTOF(val);

                //304 means "not modified".  Sent in response to a request containing
                //the "If-Modified-Since" header.
                if(NET_HTTPSTATUS_NOT_MODIFIED == this->GetStatusCode())
                {
                    netDebug2("Not modified");
                    SetSucceeded();
                }
                else if(TRANSFER_ENCODING_CHUNKED == transferEncoding)
                {
                    m_RecvState = RECVSTATE_RECEIVING_CHUNKED_CONTENT;
					m_InContentBytesRcvd = 0;
                }
                else if(!this->GetResponseHeaderValue("Content-Length", val, &valLen))
                {
#if RSG_DURANGO
					if(m_UseXblHttp)
					{
						// if IXMLHttpRequest2 receives compressed (eg. gzip or deflate encoded) data, it seems
						// to decompress the stream and send it to us without providing a Content-Length header.
						// Switch to chunked receive which processes all the data IXMLHttpRequest2 sends us. 
						transferEncoding = TRANSFER_ENCODING_CHUNKED;
						m_RecvState = RECVSTATE_RECEIVING_CHUNKED_CONTENT;
						m_InContentBytesRcvd = 0;
					}
					else
#endif
					{
						netDebug2("No content length");
						SetSucceeded();
					}
                }
                else if(1 == sscanf(val, "%u", &m_InContentLen))
                {
                    netDebug2("Content length:%u", m_InContentLen);
                    if(NET_HTTP_VERB_HEAD == m_HttpVerb)
                    {
                        SetSucceeded();
                    }
                    else if(m_InContentLen > 0)
                    {
                        m_RecvState = RECVSTATE_RECEIVING_NORMAL_CONTENT;
                    }
                    else
                    {
                        SetSucceeded();
                    }
                }
                else
                {
                    netError("Error parsing Content-Length header item");
                    this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_RESPONSE_PARSE_CONTENT_LENGTH_FAILED);
                }
            }

            netAssert(RECVSTATE_RECEIVING_NORMAL_CONTENT == m_RecvState
                    || RECVSTATE_RECEIVING_CHUNKED_CONTENT == m_RecvState
                    || RECVSTATE_SUCCEEDED == m_RecvState
                    || RECVSTATE_ERROR == m_RecvState);
        }
        break;
    case RECVSTATE_RECEIVING_NORMAL_CONTENT:
    case RECVSTATE_RECEIVING_CHUNKED_CONTENT:
        // If a response device was supplied, we automatically read the response
        // body into it, using our bounce buffer. Otherwise callers are responsible for 
        // polling ReadBody themselves
        if (m_ResponseHandle != fiHandleInvalid &&
            m_ResponseDevice != NULL)
        {
            if (!ReadBody((u8*)m_BounceBuf,
                          m_BounceBufMaxLen,
                          m_ResponseHandle,
                          *m_ResponseDevice))
            {
                netError("Error receiving HTTP content");
                this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_READ_BODY_FAILED);
            }
        }
        break;
    case RECVSTATE_FINISH_CONSUMING_CONTENT:
        {
            // If a response device was supplied, we automatically read the response
            // body into it, using our bounce buffer. Otherwise callers are responsible for 
            // polling ReadBody themselves
            if (m_ResponseHandle != fiHandleInvalid &&
                m_ResponseDevice != NULL)
            {
                // Don't receive any body, just finish consuming it
                unsigned amountConsumed = 0;
                if (!ConsumeBody((u8*)m_BounceBuf,
                                 m_BounceBufLen,
                                 &amountConsumed,
                                 m_ResponseHandle,
                                 *m_ResponseDevice))
                {
                    netError("Error consuming HTTP content");
                    this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_CONSUME_BODY_FAILED);
                }
                else
                {
                    datGrowBuffer::Remove(m_BounceBuf, &m_BounceBufLen, 0, amountConsumed);
                }
            }
        }
        break;
    case RECVSTATE_SUCCEEDED:
    case RECVSTATE_ERROR:
        break;
    }
}

int
netHttpRequest::GetStatusCode() const
{
    if(m_HttpStatusCode < 0)
    {
        char buf[32];
        int tmpCode;

        if(this->GetStatusCodeString(buf, COUNTOF(buf))
            && 1 == sscanf(buf, "%d", &tmpCode))
        {
            m_HttpStatusCode = tmpCode;
        }
    }

    return m_HttpStatusCode;
}

bool
netHttpRequest::GetStatusCodeString(char* buf,
                                    const unsigned bufLen) const
{
    bool success = false;

    *buf = '\0';

    //Do we have a full header?
    if(this->GetResponseHeader())
    {
        const char* header = m_InHeader.ToString();
        const char* eol = strstr(header, CRLF);
        if(eol)
        {
            //Status code comes after the first space.
            const char* statusCode = strchr(header, ' ');

            if(statusCode)
            {
                //Skip extra spaces.
                while(statusCode < eol && isspace(*statusCode))
                {
                    ++statusCode;
                }

                if(statusCode < eol)
                {
                    const char* src = statusCode;
                    char* dst = buf;
                    const char* eod = dst + bufLen - 1;

                    while(src < eol && dst < eod && !isspace(*src))
                    {
                        *dst++ = *src++;
                    }

                    *dst = '\0';
                    success = true;
                }
            }
        }
    }

    return success;
}


bool
netHttpRequest::HaveResponseHeader() const
{
    return m_InHeader.EndsWith(CRLF CRLF);
}

const char*
netHttpRequest::GetResponseHeader() const
{
    return HaveResponseHeader() ? m_InHeader.ToString() : NULL;
}

bool
netHttpRequest::GetResponseHeaderValue(const char* fieldName,
                                char* value,
                                unsigned* valLen) const
{
    bool success = false;

    rtry
    {
        if(!netVerify(this->GetResponseHeader()))
        {
            *valLen = 0;
            rthrow(catchall,);
        }

        const char* header = m_InHeader.ToString();
        success = netHttpRequest::GetHeaderValue(header, fieldName, value, valLen);
    }
    rcatchall
    {
        *valLen = 0;
    }

    return success;
}

unsigned
netHttpRequest::GetResponseContentLength() const
{
	if(HaveResponseHeader())
	{
		return m_InContentLen;
	}

	netWarning("netHttpRequest::GetResponseContentLength :: HaveResponseHeader is false, returning 0.");

	return 0;
}

unsigned
netHttpRequest::GetResponseContentNumBytesReceived() const
{
    return HaveResponseHeader() ? m_InContentBytesRcvd : 0;
}

bool
netHttpRequest::IsUsingExplicitProxyAddress()
{
#if RL_USE_FORCED_HTTP_PROXY
    if(rlIsForcedEnvironment())
    {
        return true;
    }
#endif

#if ENABLE_TCP_PROXY_ARGS
    if(PARAM_fiddler.Get())
    {
        return false;
    }

    // This check is not completely correct at this point but whichever
    // way we go there will be cases with false positives or negatives
    // With this check as soon as there's a global proxy wildcard '*'
    // we treat it as explicit proxy
    const char* addrStr = nullptr;
    if(netHttpProxy::GetProxyAddress(nullptr, addrStr))
    {
        return true;
    }
#endif //ENABLE_TCP_PROXY_ARGS

    return false;
}

static inline bool netHttpNeedsEncoding(const int c, const char* excludedChars)
{
    //http://tools.ietf.org/html/rfc3986#section-2.3
    bool legal =
        (c >= '0' && c <= '9')
            || (c >= 'a' && c <= 'z')
            || (c >= 'A' && c <= 'Z')
            || '_' == c || '-' == c || '.' == c || '~' == c;

    if(!legal && excludedChars)
    {
        //If it's in the excluded chars then it's considered legal.
        legal = (NULL != strchr(excludedChars, c));
    }

    return !legal;
}

bool
netHttpRequest::NeedsUrlEncoding(const char* str,
                                const unsigned len,
                                const char* excludedChars)
{
    for(int i = 0; i < (int) len; ++i)
    {
        if(netHttpNeedsEncoding(str[i], excludedChars))
        {
            return true;
        }
    }

    return false;
}

unsigned
netHttpRequest::GetUrlEncodedLength(const char* str,
                                    const unsigned len,
                                    const char* excludedChars)
{
    unsigned encLen = 0;

    for(int i = 0; i < (int) len; ++i)
    {
        if(' ' == str[i])
        {
            encLen += 1;    //encoded with '+'
        }
        else if(netHttpNeedsEncoding(str[i], excludedChars))
        {
            encLen += 3;
        }
        else
        {
            encLen += 1;
        }
    }

    return encLen;
}

static const char HTTP_HEX_DIGITS[] = "0123456789abcdef";

bool
netHttpRequest::UrlEncode(char* dst,
                            unsigned* dstLen,
                            const int c,
                            const char* excludedChars)
{
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT RAGE_LOG_FMT_DEFAULT
	netAssert(c >= 0);
#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "[reqid:%d]%s: " fmt, m_RequestId, m_ContextStr

    if(netVerify(*dstLen >= 4))
    {
        if(' ' == c)
        {
            dst[0] = '+';
            dst[1] = '\0';
            *dstLen = 1;
        }
        else if(netHttpNeedsEncoding(c, excludedChars))
        {
            dst[0] = '%';
            dst[1] = HTTP_HEX_DIGITS[c/16];
            dst[2] = HTTP_HEX_DIGITS[c%16];
            dst[3] = '\0';
            *dstLen = 3;
        }
        else
        {
            dst[0] = (char) c;
            dst[1] = '\0';
            *dstLen = 1;
        }

        return true;
    }

    return false;
}

bool
netHttpRequest::UrlEncode(char* dst,
                        unsigned* dstLen,
                        const char* src,
                        const unsigned srcLen,
                        unsigned* numConsumed,
                        const char* excludedChars)
{
    char* p = dst;
    const char* eod = dst + *dstLen;
    *dstLen = 0;
    *numConsumed = 0;

    for(int i = 0; i < (int) srcLen; ++i)
    {
        char encoded[4];
        unsigned encodedLen = COUNTOF(encoded);

        UrlEncode(encoded, &encodedLen, (unsigned char) src[i], excludedChars);

        if(p + encodedLen >= eod)
        {
            break;
        }

        for(int i = 0; i < (int) encodedLen; ++i, ++p, ++*dstLen)
        {
            *p = encoded[i];
        }
        *p = '\0';
        ++*numConsumed;
    }

    return true;
}

bool
netHttpRequest::UrlDecode(char* dst,
                        unsigned* dstLen,
                        const char* src,
                        const unsigned srcLen,
                        unsigned* numConsumed)
{
    //Hack to support the overridden debug output macros
    //See RAGE_LOGF_HELPER defined at top of file.
    OUTPUT_ONLY(static const char* m_ContextStr = "");
    OUTPUT_ONLY(static const unsigned m_RequestId = 0);

    bool success = true;
    char* p = dst;
    const char* eod = dst + *dstLen - 1;
    *dstLen = 0;
    *numConsumed = 0;

    for(int i = 0; i < (int) srcLen && p < eod; ++i, ++p, ++*numConsumed)
    {
        const int c = (unsigned char) src[i];

        if('%' == c && i < (int)srcLen - 2)
        {
            int tmp;
            if(1 == sscanf(&src[i], "%%%02x", &tmp))
            {
                *p = (char) tmp;
                i += 2;
            }
            else
            {
                netDebug2("Invalid URL encoding");
                success = false;
            }
        }
        else
        {
            *p = (char) c;
        }
    }

    if(success)
    {
        *dstLen = ptrdiff_t_to_int(p - dst);
        dst[*dstLen] = '\0';
    }
    else
    {
        *dstLen = 0;
        dst[0] = '\0';
    }

    return success;
}

const char*
netHttpRequest::MakeDateString(const u64 posixTime,
                                char* buf,
                                const unsigned sizeofBuf)
{
    struct tm timeinfo;
    time_t t = (time_t)posixTime;
    timeinfo = *gmtime(&t);
    if(0 == strftime(buf, sizeofBuf-1, "%a, %d %b %Y %H:%M:%S GMT", &timeinfo))
    {
        buf[0] = '\0';
    }

    return buf;
}

static const char* MONTH_NAMES[] =
{
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

u64 
GetPosixTimeFromDateTime(struct tm dateTime)
{
	u64 posixTime = mktime(&dateTime);

	//mktime interprets the time as local time, whereas we want
	//UTC time, so compute the tz offset and subtract it from local time.
	time_t t0 = time(NULL);
#if RSG_PROSPERO
	struct tm timeinfo = *gmtime_s(&t0, &timeinfo);
#else
	struct tm timeinfo = *gmtime(&t0);
#endif
	time_t t1 = mktime(&timeinfo);
	posixTime -= (t1 - t0);

	return posixTime;
}

u64
netHttpRequest::ParseDateString(const char* dateString)
{
    //Day, DD Mon YYYY hh:mm:ss GMT
    static const char* rfc1123 = "%s %d %s %d %d:%d:%d GMT";
    //Day, DD-Mon-YY hh:mm:ss GMT
    //static const char* rfc850 = "%s %d-%s-%d %d:%d:%d GMT";
    //Day Mon D hh:mm:ss YYYY
    static const char* asctm = "%s %s %d %d:%d:%d %d";
	//yyyy-MM-dd HH:mm:ssZ ==> we remove the '-' and 'Z'
	static const char* unisortable = "%d %d %d %d:%d:%d";

    char wkDay[32], month[32];
    int D, Y;
	int M = 0;
    int h, m, s;
	
	bool isRFC850 = false;
	bool isUniversalSortable = false;

    u64 posixTime = 0;

    char val[1024];
    unsigned valLen = COUNTOF(val);

    safecpy(val, dateString);

    /*static bool TESTME = false;
    if(!TESTME)
    {
        static const char* testRfc1123 = "Sun, 06 Nov 1994 08:49:37 GMT";
        static const char* testRfc850 = "Sunday, 06-Nov-94 08:49:37 GMT";
        static const char* testAsctime = "Sun Nov  6 08:49:37 1994";

        FOO = true;
        u64 tmp = ParseDateTime(testRfc1123);
        time_t t = (time_t) tmp;
        const char* a = ctime(&t);
        tmp = ParseDateTime(testRfc850);
        t = (time_t) tmp;
        a = ctime(&t);
        tmp = ParseDateTime(testAsctime);
        t = (time_t) tmp;
        a = ctime(&t);

        TESTME = false;
    }*/

    //Remove '-' from rfc850 format.  It then essentially becomes rfc1123 format,
    //except with a 2 digit year.
    if(strchr(val, '-'))
    {
        std::replace(val, &val[valLen], '-', ' ');
		if (strlen(val) == 20 && (val[19] == 'z' || val[19] == 'Z'))
		{
			// remove the Z
			val[19] = '\0';
			isUniversalSortable = true;
		}
		else
		{
			isRFC850 = true;
		}
    }

    if(7 == sscanf(val, rfc1123, wkDay, &D, month, &Y, &h, &m, &s)
        || 7 == sscanf(val, asctm, wkDay, month, &D, &h, &m, &s, &Y)
		||	6 == sscanf(val, unisortable, &Y, &M, &D, &h, &m, &s))
    {
        if(isRFC850)
        {
            if(Y < 70)
            {
                Y += 2000;
            }
            else
            {
                Y += 1900;
            }
        }

        struct tm dateTime = {0};

        dateTime.tm_hour = h;
        dateTime.tm_min = m;
        dateTime.tm_sec = s;
        dateTime.tm_mday = D;
        dateTime.tm_year = Y - 1900;

		if (isUniversalSortable)
		{
			dateTime.tm_mon = M-1; // M is 1-12, tm_mon is 0-11
		}
		else
		{
			for(int i = 0; i < COUNTOF(MONTH_NAMES); ++i)
			{
				if(0 == strnicmp(MONTH_NAMES[i], month, 3))
				{
					dateTime.tm_mon = i;
					break;
				}
			}
		}

		posixTime = GetPosixTimeFromDateTime(dateTime);
	}

	return posixTime;
}

inline int ParseInt(const char* value)
{
	return atoi(value);
}

u64 netHttpRequest::ParseDateStringRFC3339(const char* dateString)
{
	/*
	static const char* utcFormat = "%04d-%02d-%02dT%02d:%02d:%02d.%dZ";
	static const char* timeZoneFormatNg = "%04d-%02d-%02dT%d:%02d:%02d.%d-%02d:%02d";
	static const char* timeZoneFormatPv = "%04d-%02d-%02dT%d:%02d:%02d.%d+%02d:%02d";
	*/

	// assumes a strict format in terms of number of characters
	struct tm dateTime = { 0 };
	dateTime.tm_year = ParseInt(&dateString[0]);
	dateTime.tm_mon = ParseInt(&dateString[5]);
	dateTime.tm_mday = ParseInt(&dateString[8]);
	dateTime.tm_hour = ParseInt(&dateString[11]);
	dateTime.tm_min = ParseInt(&dateString[14]);
	dateTime.tm_sec = ParseInt(&dateString[17]);
	// ms not relevant for posix but would be ParseInt(&dateString[20])

	// check for invalid / unset value
	if(dateTime.tm_year == 1)
	{
		return 0;
	}

	// setup offsets
	dateTime.tm_year -= 1900;	// year is offset from 1900
	dateTime.tm_mon -= 1;		// month indexes from 0 to 11 
	dateTime.tm_hour -= 1;		// hour indexes from 0 to 23

	// check what timezone format this uses, UTC is indicated by Z
	const char* tzCheck = &dateString[20];
	const char* tzMarker = strchr(tzCheck, 'Z');
	if(tzMarker == nullptr)
	{
		int tzMult = 0;

		// time zone
		tzMarker = strchr(tzCheck, '-');
		if(tzMarker != nullptr)
			tzMult = 1;
		else
		{
			tzMarker = strchr(tzCheck, '+');
			if(tzMarker != nullptr)
				tzMult = -1;
		}

		if(tzMult != 0)
		{
			dateTime.tm_hour += (ParseInt(&tzMarker[1]) * tzMult);
			dateTime.tm_min += (ParseInt(&tzMarker[4]) * tzMult);
		}
	}

	return GetPosixTimeFromDateTime(dateTime);
}

#if __ASSERT && 0
static const int TEST_URL_ENCODING = netHttpRequest::TestUrlEncoding();

int
netHttpRequest::TestUrlEncoding()
{
    char bufA[512], bufB[sizeof(bufA)+1];
    char dstBuf[3*sizeof(bufA)];
    netRandom rng(1);
    unsigned lenA, lenB, numConsumed;

    for(int i = 0; i < 1000; ++i)
    {
        for(int j = 0; j < sizeof(bufA); ++j)
        {
            bufA[j] = (char)(rng.GetInt() & 0xFF);
        }

        lenA = sizeof(dstBuf);
        netAssert(netHttpRequest::UrlEncode(dstBuf, &lenA, bufA, sizeof(bufA), &numConsumed));
        netAssert(numConsumed == sizeof(bufA));
        lenB = sizeof(bufB);
        netAssert(netHttpRequest::UrlDecode(bufB, &lenB, dstBuf, lenA, &numConsumed));
        netAssert(numConsumed == lenA);
        netAssert(!memcmp(bufA, bufB, sizeof(bufA)));
    }

    return 1;
}
#endif

#if !__FINAL
void 
netHttpRequest::SetSimulatedFailPct(const char* failRate)
{
	PARAM_nohttpsometimes.Set(failRate);
}
#endif

//private:

bool
netHttpRequest::Begin(const netHttpVerb verb,
                      const char* uri,
                      const netSocketAddress* proxyAddr,
                      const unsigned timeoutSecs,
                      const fiDevice* responseDevice,
                      const fiHandle responseHandle,
                      const char* OUTPUT_ONLY(contextStr),
                      netHttpFilter* filter,
                      netStatus* status)
{

    bool success = false;

    this->Clear();

    m_RequestId = ++s_NextRequestId;

#if !__NO_OUTPUT
    if(contextStr)
    {
        safecpy(m_ContextStr, contextStr, CONTEXT_STRING_BUF_SIZE);

		if(strlen(m_ContextStr) != strlen(contextStr))
		{
			netWarning("Context string %s will be truncated to %s", contextStr, m_ContextStr);
		}
    }
    else
    {
        m_ContextStr[0] = '\0';
    }
#endif

    m_HttpVerb = verb;

    netDebug("Begin %s to '%s'...", HTTP_VERBS[m_HttpVerb], uri);

#if !__NO_OUTPUT
	netHttpRequestTracker::HandleRequest(uri);
#endif

    unsigned toSecs;
    if(!PARAM_nethttptimeout.Get(toSecs))
    {
        toSecs = timeoutSecs;
    }

    netDebug("Timeout: %d seconds", toSecs);

    status->SetPending();

    rtry
    {
        rverify(m_Initialized,
                catchall,
                netError("HTTP request has not been initialized"));
        rverify(!m_CurChunk,catchall,)
        rverify(SENDSTATE_SUCCEEDED == m_SendState
                || SENDSTATE_NONE == m_SendState
                || SENDSTATE_ERROR == m_SendState,catchall,);
        rverify(RECVSTATE_SUCCEEDED == m_RecvState
                || RECVSTATE_NONE == m_RecvState
                || RECVSTATE_ERROR == m_RecvState,catchall,);

		if(!m_IgnoreProxy)
		{
			//Non-null proxyAddr overrides everything.
			if(proxyAddr)
			{
				m_ProxyAddr = *proxyAddr;
			}
#if ENABLE_TCP_PROXY
			else if(netHttpProxy::GetProxyAddress(uri, m_ProxyAddr))
			{
				netDebug2("Overriding proxy address with value from netHttpProxy: " NET_ADDR_FMT "",
					NET_ADDR_FOR_PRINTF(m_ProxyAddr));
			}
#endif //ENABLE_TCP_PROXY
		}

        // If a response device/handle aren't supplied, then we're expecting the caller to poll
        // ReadBody
        //rverify((responseDevice && fiHandleInvalid != responseHandle)
        //        || NET_HTTP_VERB_HEAD == verb,
        //        catchall,);

        m_Filter = filter;
        if(m_Filter)
        {
            m_Filter->m_HttpRequest = this;
        }

        m_ResponseDevice = responseDevice;
        m_ResponseHandle = responseHandle;

        m_Timeout.InitSeconds(toSecs);

        rverify(this->StartRequest(uri),
                catchall,
                netError("Error starting request"));

        //Give the filter an opportunity to append to the user-agent string.
        //The filter may need to do this even if it doesn't filter the
        //request or response.
        if (m_Filter && m_Filter->GetUserAgentString())
        {
            rverify(this->AppendUserAgentString(m_Filter->GetUserAgentString()),
                catchall,
                netError("Failed to append user agent string [%s]", m_Filter->GetUserAgentString()));
        }

        const char* useragent;
        if(PARAM_nethttpuseragent.Get(useragent))
        {
            this->AppendUserAgentString(useragent);
        }

        //Allow the flter to modify any other request headers
        if (m_Filter)
        {
            rverify(m_Filter->ProcessRequestHeader(this),
                    catchall,
                    netError("Filter failed to process request headers"));
        }

        m_Status = status;

        //Add to the run queue
        Enqueue();

        netDebug("Request %u queued at position %d",
                m_RequestId,
                s_NetHttpRequestQueueLength-1);

        if(s_NetHttpRequestQueueLength > MAX_IN_FLIGHT_REQUESTS)
        {
            netWarning("%d in-flight requests exceeds maximum of %d - request ID %u will be queued",
                        s_NetHttpRequestQueueLength,
                        MAX_IN_FLIGHT_REQUESTS,
                        m_RequestId);
        }

        success = true;
    }
    rcatchall
    {
        this->Clear();
        status->SetFailed();
    }

    return success;
}

bool
netHttpRequest::StartRequest(const char* uri)
{
    bool success = false;

    m_QueryString.Clear();
    m_HttpStatusCode = -1;
    m_InHeader.Clear();
    m_UriScheme = URISCHEME_INVALID;

    if(m_RawUri)
    {
        this->Free(m_RawUri);
        m_RawUri = NULL;
        m_RawHost = NULL;
        m_RawPath = NULL;
    }

	ClearCachedEncodedUri();

    if(m_CurChunk)
    {
        this->DeleteChunk(m_CurChunk);
        m_CurChunk = NULL;
    }

    if(m_Skt >= 0)
    {
        netTcp::Close(m_Skt);
        m_Skt = -1;
    }

    m_InContentLen = 0;
    m_InContentBytesRcvd = 0;
    m_OutContentBytesSent = 0;
	m_OutChunksBytesSent = 0;
    m_NumQueryParams = 0;
    m_HostIp.Clear();
    m_HostPort = 0;
    m_Committed = false;
	m_ChunkAllocationError = false;
	m_AbortReason = NET_HTTPABORT_NONE;
	m_TcpOpResult = NETTCP_RESULT_OK;
	m_TcpOpError = 0;

#if RSG_DURANGO
	m_AllContentReceived = false;
#endif

    rtry
    {
        rverify(m_Initialized,
                catchall,
                netError("HTTP request has not been initialized"));

        rverify(!m_CurChunk,catchall,netError("Valid chunk found when it should be NULL"));

        if(this->HaveProxyAddress())
        {
            netDebug("Using proxy: " NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(m_ProxyAddr));
        }

        m_CurChunk = this->NewChunk();

        rverify(m_CurChunk,catchall,netError("Error allocating chunk"));

        rverify(this->ParseUri(uri),catchall,netError("Error in ParseUri"));

        rverify((URISCHEME_HTTPS == m_UriScheme) || netHttp::AllowNonHttpsRequest(),
                catchall,
                netError("Non-SSL HTTP requests are prohibited. Rejecting request to '%s'", uri));

#if ENABLE_HTTP_INTERCEPTOR
		m_Filter = netHttpInterception::CreateInterceptor(this, m_Filter);
		if(m_Filter && m_Filter->CanIntercept())
		{
#if RSG_DURANGO
			m_UseXblHttp = false;
#endif
		}
#endif

#if RSG_DURANGO
		if(m_UseXblHttp && HaveProxyAddress())
		{
			m_UseXblHttp = false;
		}

		netDebug("Using IXHR2: %s", m_UseXblHttp ? "YES" : "NO");
#endif

        m_SendState = SENDSTATE_WAIT_UNTIL_READY;
        m_RecvState = RECVSTATE_NONE;
        m_Timeout.Reset();

#if !__FINAL
        if(!PARAM_nohttpsometimes.Get(m_SimulateFailPct))
        {
            m_SimulateFailPct = 0;
        }
#endif

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool
netHttpRequest::Redirect(const char* uri)
{
    netDebug("Redirecting to '%s'...", uri);

	rtry
	{
		++m_RedirectCount;

		rverify(RECVSTATE_RECEIVED_HEADER == m_RecvState, catchall, );
		rverify(this->Pending(),
				catchall,
				netError("Can't redirect - no request pending"));
		rverify(NET_HTTP_VERB_GET == m_HttpVerb || NET_HTTP_VERB_HEAD == m_HttpVerb,
				catchall,
				netError("Can only redirect a GET or HEAD request"));
		rverify(m_RedirectCount <= MAX_REDIRECT_COUNT,
				catchall,
				netError("Maximum of %d redirects has been exceeded", MAX_REDIRECT_COUNT));

		unsigned urlLen = istrlen(uri);
		rverify(urlLen < sysUri::MAX_STRING_BUF_SIZE, catchall, netError("Redirect URL too long"));

		char uriEncoded[sysUri::MAX_STRING_BUF_SIZE];
		safecpy(uriEncoded, uri);

		bool maintainQueryString = ((m_Options & NET_HTTP_MAINTAIN_QUERY_ON_REDIRECT) != 0) && (m_QueryString.Length() > 0);
		if(maintainQueryString)
		{
			netDebug2("Maintaining query string on redirect due to enabled option.");

			// some servers will maintain the query string on redirects, or even redirect using a
			// different query string. If the redirect URI contains a different query string, merge with ours.
			// Note that the merge is a simple append, so duplicate query params are possible.
			sysUri uriInfo;
			netVerifyf(sysUri::Parse(uriEncoded, uriInfo, true), "Error parsing url: \"%s\"", uriEncoded);

			if(uriInfo.m_QueryString[0] != '\0')
			{
				if((strcmp(uriInfo.m_QueryString, m_QueryString.ToString()) != 0))
				{
					netDebug2("Redirect URI contains a different query string. Combining query strings.");

					// +1 to skip over the '?' to get to the key=value pairs
					const char* queryKvps = m_QueryString.ToString() + 1;
					safecatf(uriEncoded, "&%s", queryKvps);
				}
			}
			else
			{
				safecat(uriEncoded, m_QueryString.ToString());
			}

			urlLen = istrlen(uriEncoded);
			m_QueryString.Clear();
		}		

		char uriDecoded[sysUri::MAX_STRING_BUF_SIZE];
		unsigned uriDecodeSize = sizeof(uriDecoded);
		unsigned numConsumed;
		rverify(UrlDecode(uriDecoded, &uriDecodeSize, uriEncoded, urlLen, &numConsumed),
			catchall,
			netError("Failed to URL decode the redirect uri"));

		rverify(this->StartRequest(uriDecoded), catchall, );

		rverify(this->Commit(), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool
netHttpRequest::ResolveHost(netStatus* status)
{
    bool success = false;

    if(netVerify(!status->Pending()))
    {
		// if we're using https tunneling, we need to resolve the host so that we 
		// can tell the proxy the IP of the web server to which to connect.
        if(this->HaveProxyAddress() && !UseHttpsTunnel())
        {
            //No host resolution necessary because we're using a proxy
            status->SetPending();
            status->SetSucceeded();
            success = true;
        }
        else
        {
            netDebug2("Resolving host: '%s'...", m_RawHost);
            if(netResolver::ResolveHost(m_RawHost, &m_HostIp, status))
            {
                success = true;
            }
            else
            {
                netError("Error calling netResolver::ResolveHost()");
                status->SetPending();
                status->SetFailed();
            }
        }
    }

    return success;
}

bool
netHttpRequest::Connect()
{
    bool success = false;

    netSocketAddress addr;

	const bool useHttps = (URISCHEME_HTTPS == m_UriScheme);

    if(!useHttps && !netHttp::AllowNonHttpsRequest())
    {
        return false;
    }

	bool useHttpsTunnel = UseHttpsTunnel();

	netSocketAddress proxyAddr = this->GetProxyAddress();

    if(proxyAddr.IsValid() && !useHttpsTunnel)
    {
		addr = proxyAddr;
    }
    else
    {
        addr.Init(m_HostIp, m_HostPort);

		//If port is zero then use 80 for HTTP and 443 for HTTPS.
		if(!addr.GetPort())
		{
			const unsigned short port = useHttps ? HTTPS_PORT : HTTP_PORT;
			addr.Init(addr.GetIp(), port);
		}
	}

	if(!useHttpsTunnel)
	{
		proxyAddr.Clear();
	}

#if ENABLE_HTTP_INTERCEPTOR
	if(m_Filter && m_Filter->CanIntercept())
	{
		netDebug2("Filter intercepting Connect()...");
		success = true;
	}
	else
#endif
#if RSG_DURANGO
	if(m_UseXblHttp)
	{
		success = m_XblHttpRequest.Open(HTTP_VERBS[m_HttpVerb], GetUri());
	}
	else
#endif
    if(netVerify(addr.IsValid()))
    {
        netDebug2("Connecting to:" NET_ADDR_FMT "", NET_ADDR_FOR_PRINTF(addr));

        if(useHttps)
        {
            netDebug2("Connecting via SSL...");
            //Use our ssl context if configured
            if(netVerifyf(m_SslCtx != nullptr, "You must set an SslCtx before starting an https request"))
            {
                //Pass our host (if any) to the connect so that the host domain
                //will be verified against the one returned in the certificate
                success = netTcp::SslConnectAsync(m_SslCtx,
                                                addr,
												ENABLE_TCP_PROXY_ONLY(proxyAddr,)
                                                m_RawHost,
                                                &m_Skt,
                                                GetHttpContextString(),
                                                m_Timeout.GetSecondsUntilTimeout() + TIMEOUT_FUDGE_AMOUNT_SECONDS,
                                                CR_INVALID,
                                                &m_ConnectTcpOp);
            }
        }
        else
        {
            success = netTcp::ConnectAsync(addr,
                                            ENABLE_TCP_PROXY_ONLY(proxyAddr,)
                                            &m_Skt,
                                            GetHttpContextString(),
                                            m_Timeout.GetSecondsUntilTimeout() + TIMEOUT_FUDGE_AMOUNT_SECONDS,
                                            CR_INVALID,
                                            &m_ConnectTcpOp);
        }
    }

    return success;
}

bool
netHttpRequest::ParseUri(const char* uri)
{
    bool success = false;

    rtry
    {
        netAssert(!m_RawUri);
        netAssert(URISCHEME_INVALID == m_UriScheme);
        netAssert(!m_RawHost);
        netAssert(!m_RawPath);
        netAssert(m_QueryString.Length() == 0);

        //Add two to the length because we add an extra
        //slash before the path (see path construction below).
        const unsigned urlLen = istrlen(uri);
        const unsigned uriBufLen = urlLen + 2;
        m_RawUri = (char*) this->Allocate(uriBufLen);
        rcheck(m_RawUri,catchall,netError("Failed to allocate RawUri buffer"));
        safecpy(m_RawUri, uri, uriBufLen);

        StringTrim(m_RawUri, uri, uriBufLen);

        rverify(IsHttpUri(m_RawUri) || IsHttpsUri(m_RawUri),
                catchall,
                netError("URI must begin with 'http://' or 'https://'"));

        char* host = strstr(m_RawUri, "://");
        rverify(host,catchall,);
        *host = '\0';   //Terminate the URI scheme
        host += 3;

        char* path = strchr(host, '/');
        char* qstring = NULL;

        if(path)
        {
            //We need a "/" at the beginning of the path, but
            //we also need to terminate the host string.
            //This loop shifts the path component forward
            //in the uri buffer, and is the reason we needed
            //to allocate an extra char above.
            char* p = path+strlen(path)+1;
            netAssert(p == &m_RawUri[uriBufLen-1]);
            for(; p > path; --p)
            {
                *p = p[-1];
            }

            *path++ = '\0';     //Terminate the host string

            qstring = strchr(path, '?');
            if(qstring)
            {
                *qstring++ = '\0';  //Terminate the path string
            }
        }

        //Parse the port part of the host name.
        char* portStr = strchr(host, ':');
        if(portStr)
        {
            *portStr++ = '\0';  //Terminate the host string
            m_HostPort = (unsigned short) atoi(portStr);
        }
        else
        {
            m_HostPort = 0;
        }

        m_RawHost = host;
        m_RawPath = path ? path : "";

        //Parse the query string
        if(qstring)
        {
            char* p = qstring;
            while(p)
            {
                char* name = p;
                p = strchr(p, '&');
                if(p)
                {
                    *p++ = '\0';
                }

                char* val = strchr(name, '=');
                rverify(val, catchall, netError("Invalid query string in %s", uri));
                *val++ = '\0';
                rverify(this->AddQueryParam(name, val, istrlen(val)),
                        catchall,
                        netError("Error adding query param %s", name));

            }
        }

        m_UriScheme = !strcmp(m_RawUri, "https") ? URISCHEME_HTTPS : URISCHEME_HTTP;

        success = true;
    }
    rcatchall
    {
		if(m_RawUri)
		{
			this->Free(m_RawUri);
			m_RawUri = NULL;
			m_RawHost = NULL;
			m_RawPath = NULL;
		}
    }

    return success;
}

bool
netHttpRequest::MakeHeader(datGrowBuffer* header)
{
    bool success = false;

    rtry
    {
        rverify(SENDSTATE_SEND_HEADER == m_SendState,catchall,);

        char tmp[2048];
        {
            const char FMT_START_LINE[] = {"%s %s%s HTTP/1.1" CRLF};
            const char FMT_HOST[] = {"Host: %s" CRLF};
			const char FMT_NON_DEFAULT_HOST[] = {"Host: %s:%d" CRLF};
            //const char FMT_SOAP_ACTION[] = {"SOAPAction: \"%s\"" CRLF};

            netAssert(m_RawUri);
            netAssert(URISCHEME_INVALID != m_UriScheme);
            netAssert(m_RawHost);
            netAssert(m_RawPath);

            if(this->HaveProxyAddress())
            {
                //Put the entire URI in the header.
                formatf(tmp,
                        FMT_START_LINE,
                        HTTP_VERBS[m_HttpVerb],
                        GetUri(),
                        "");
                netAssert(strlen(tmp) == strlen(FMT_START_LINE)
                                    + strlen(HTTP_VERBS[m_HttpVerb])
                                    + strlen(GetUri())
                                    - strlen("%s")
                                    - strlen("%s")
                                    - strlen("%s"));
            }
            else
            {
                //Put only the path part of the URI in the header.
                const char* path = this->GetPath();
                formatf(tmp, FMT_START_LINE, HTTP_VERBS[m_HttpVerb], path, m_QueryString.ToString());
                netAssert(strlen(tmp) == strlen(FMT_START_LINE)
                                    + strlen(HTTP_VERBS[m_HttpVerb])
                                    + strlen(path)
                                    + m_QueryString.Length()
                                    - strlen("%s")
                                    - strlen("%s")
                                    - strlen("%s"));
            }

            rverify(header->AppendOrFail(tmp, istrlen(tmp)),
                    catchall,
                    netError("Error building HTTP header"));

			//append the port number to the host header if the port isn't 80
			//the default port for host headers is 80 and if the port is anything other than that
			//it must be included in the header as host:port http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.23
			//port 443 is being skipped for now, but may need to be added later if it's required for proxies, etc.
			if(m_HostPort != HTTP_PORT && m_HostPort != HTTPS_PORT && m_HostPort != 0)
			{
				formatf(tmp, FMT_NON_DEFAULT_HOST, m_RawHost, m_HostPort);
			}
			else
			{
                formatf(tmp, FMT_HOST, m_RawHost);
				netAssert(strlen(tmp) == strlen(FMT_HOST) + strlen(m_RawHost) - strlen("%s"));
			}
            
            rverify(header->AppendOrFail(tmp, istrlen(tmp)),
                    catchall,
                    netError("Error adding 'Host' header"));
        }

        m_OutboundTransferEncoding = TRANSFER_ENCODING_NORMAL;

        if(NET_HTTP_VERB_POST == m_HttpVerb || NET_HTTP_VERB_PUT == m_HttpVerb)
        {
			if(m_Options & NET_HTTP_FORCE_TRANSFER_ENCODING_NORMAL)
			{
				netAssert(this->IsCommitted());
			}
			else
			{
				if(this->IsCommitted())
				{
					//If Commit() has been called and we have more than 1 queued
					//chunk then send with chunked transfer encoding.
					if(m_QueuedChunks.size() > 1)
					{
						m_OutboundTransferEncoding = TRANSFER_ENCODING_CHUNKED;
					}
				}
				else
				{
					//We wouldn't be sending the header unless we've queued
					//a chunk.  In addition Commit() hasn't been called
					//so we assume we're going to send more chunks.
					//Because we have more than one chunk the transfer
					//encoding will be chunked.
					netAssert(m_QueuedChunks.size() >= 1);

					m_OutboundTransferEncoding = TRANSFER_ENCODING_CHUNKED;
				}
			}

            if(TRANSFER_ENCODING_CHUNKED == m_OutboundTransferEncoding)
            {
#if RSG_DURANGO
				// IXHR2 doesn't support chunked encoding
				if(!m_UseXblHttp)
#endif
				{
					rverify(this->AddRequestHeaderValue("Transfer-Encoding", "chunked"),
							catchall,
							netError("Error adding 'Transfer-Encoding' header"));
				}
            }
            else
            {
                {
                    //We should only have one chunk, unless we forced
                    //normal encoding with multiple chunks, in which
                    //case we need to compute the length of all chunks
                    netAssert(m_QueuedChunks.size() <= 1 || 
                           m_Options & NET_HTTP_FORCE_TRANSFER_ENCODING_NORMAL);

                    unsigned totalSize = 0;
                    for (ChunkList::const_iterator it = m_QueuedChunks.begin(); it != m_QueuedChunks.end(); ++it)
                    {
                        totalSize += (*it)->m_Data.Length();
                    }

                    formatf(tmp, "%d", totalSize);
                    rverify(this->AddRequestHeaderValue("Content-Length", tmp),
                            catchall,
                            netError("Error adding Content-Length header"));
                }
            }
        }

        {
            if(m_UserAgentString.Length() > 0 && !HasRequestHeader("User-Agent"))
            {
                rverify(this->AddRequestHeaderValue("User-Agent",
                                                    m_UserAgentString.ToString()),
                        catchall,
                        netError("Error adding 'User-Agent' header"));
            }
        }

        rverify(header->AppendOrFail(m_OutHeader.ToString(), m_OutHeader.Length()),
                catchall,
                netError("Error adding HTTP header to request"));

        //header->Append("Expect: 100-continue" CRLF);

        {
            //Add the final CRLF.
            rverify(header->AppendOrFail(CRLF, istrlen(CRLF)),
                    catchall,
                    netError("Error appending CRLF to header"));
        }

        success = true;
    }
    rcatchall
    {
    }

    return success;
}

bool
netHttpRequest::QueueRawData(const void* data, const unsigned dataLen)
{
    netAssert(dataLen);

    rtry
    {
        rverify(m_CurChunk, catchall, );
        rverify(NET_HTTP_VERB_POST == m_HttpVerb || NET_HTTP_VERB_GET == m_HttpVerb || NET_HTTP_VERB_PUT == m_HttpVerb, catchall, );
        rverify(!this->IsCommitted(), catchall, );

        unsigned startLen = m_CurChunk->m_Data.Length();

        //If we have a request filter, have it append filtered data to the chunk.
        //Otherwise, just append the data unchanged.
        if (m_Filter && m_Filter->CanFilterRequest())
        {
            rverify(m_Filter->FilterRequest((const u8*)data, dataLen, false, m_CurChunk->m_Data),
                    catchall,
                    netError("Error filtering request data"));
        }
        else
        {
            rverify(m_CurChunk->m_Data.AppendOrFail(data, dataLen),
                    catchall,
                    netError("Error appending data to chunk"));
        }

        m_OutContentBytesSent += (m_CurChunk->m_Data.Length() - startLen);

        //Start a new chunk if this one has grown too large.
        if(m_CurChunk->m_Data.Length() >= MAX_OUTBOUND_CHUNK_SIZE)
        {
            m_QueuedChunks.push_back(m_CurChunk);
            m_CurChunk = this->NewChunk();            
            rverify(m_CurChunk, catchall, netError("Error allocating chunk"));
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
netHttpRequest::QueueRawDataZeroCopy(const void* data, const unsigned dataLen)
{
    //*** NOTE ***
    //  Data queued by this function will not be filtered through m_Filter->FilterRequest.
    //  IOW, it will not be encrypted.
    //  We assume it's been pre-filtered.

    netAssert(dataLen);

    rtry
    {
        rverify(m_CurChunk, catchall, );
        rverify(NET_HTTP_VERB_POST == m_HttpVerb || NET_HTTP_VERB_GET == m_HttpVerb || NET_HTTP_VERB_PUT == m_HttpVerb, catchall, );
        rverify(!this->IsCommitted(), catchall, );

        if(m_CurChunk->m_Data.Length())
        {
            //Queue the current chunk and start a new one
            m_QueuedChunks.push_back(m_CurChunk);
            m_CurChunk = this->NewChunk();            
            rverify(m_CurChunk, catchall, netError("Error allocating chunk"));
        }

        //Create a readonly chunk
        m_CurChunk->m_Data.InitReadOnly(data, dataLen);

        //Queue the readonly chunk.
        m_QueuedChunks.push_back(m_CurChunk);

        //Create a new chunk ready to receive more data.
        m_CurChunk = this->NewChunk();            
        rverify(m_CurChunk, catchall, netError("Error allocating chunk"));

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
netHttpRequest::QueueUrlEncodedData(const void* data, const unsigned dataLen)
{
    netAssert(dataLen);

    bool success = false;

    if((!netHttpRequest::NeedsUrlEncoding((const char*) data, dataLen, NULL)) || (!this->m_bUrlEncode))
    {
        success = this->QueueRawData(data, dataLen);
    }
    else if(netVerify(m_CurChunk))
    {
        const char* src = (const char*) data;
        const char* eos = src + dataLen;

        success = true;

        while(src < eos && success)
        {
            char dst[4096];
            unsigned dstLen = COUNTOF(dst);
            unsigned numConsumed;

            if(netHttpRequest::UrlEncode(dst, &dstLen, src, ptrdiff_t_to_int(eos - src), &numConsumed, NULL))
            {
                success = this->QueueRawData(dst, dstLen);
                src += numConsumed;
            }
        }
    }

    return success;
}

bool
netHttpRequest::SendHeader()
{
    netDebug2("Sending HTTP header...");

    Chunk* headerChunk = NULL;

    m_InHeader.Clear();

    rtry
    {
        rverify(SENDSTATE_SEND_HEADER == m_SendState,catchall,);

        //If we're POSTing we should be sending a header *only* if
        //we've queued a chunk, or if Commit() has been called with
        //no chunks queued.
        rverify((NET_HTTP_VERB_POST != m_HttpVerb && NET_HTTP_VERB_PUT != m_HttpVerb)
                || !m_QueuedChunks.empty()
                || this->IsCommitted(),catchall,);

        headerChunk = this->NewChunk();

        rverify(headerChunk,
                catchall,
                netError("Error allocating chunk for HTTP header"));

		if (m_ByteRangeHigh > 0 && (m_ByteRangeLow < m_ByteRangeHigh))
		{
			char byteRangeString[128];
			formatf(byteRangeString, "bytes=%u-%u", m_ByteRangeLow, m_ByteRangeHigh);
			if (!this->AddRequestHeaderValue("Range", byteRangeString ))
				netError("Could not add byte range to request header");
		}

        rverify(this->MakeHeader(&headerChunk->m_Data),
                catchall,
                netError("Error constructing HTTP header."));

        this->DumpHttp("Outbound header", 
                       (const char *)headerChunk->m_Data.GetBuffer(),
                       headerChunk->m_Data.Length());

#if ENABLE_HTTP_INTERCEPTOR
		if(m_Filter && m_Filter->CanIntercept())
		{
			netDebug2("Filter intercepting SendHeader()...");
		}
		else
#endif
#if RSG_DURANGO
		if(m_UseXblHttp)
		{
			rverify(m_XblHttpRequest.SetRequestHeader((const char*)headerChunk->m_Data.GetBuffer(),
													   headerChunk->m_Data.Length()),
					catchall,
					netError("Error setting request header"));

			if(m_QueuedChunks.size() == 0)
			{
				// no body to send
				rcheck(m_XblHttpRequest.Commit(),
						catchall,
						netError("Error sending content"));
			}
		}
		else
#endif
        {
            rcheck(netTcp::SendBufferAsync(m_Skt,
                                           headerChunk->m_Data.GetBuffer(),
                                           headerChunk->m_Data.Length(),
                                           m_Timeout.GetSecondsUntilTimeout() + TIMEOUT_FUDGE_AMOUNT_SECONDS,
                                           &headerChunk->m_TcpOp),
                    catchall,
                    netError("Error sending header"));
        }

        netAssert(!m_InFlightChunk);
        m_InFlightChunk = headerChunk;

        return true;
    }
    rcatchall
    {
    }

    if(headerChunk)
    {
        this->DeleteChunk(headerChunk);
    }

    return false;
}

bool
netHttpRequest::IsCommitted() const
{
    return m_Committed;
}

void netHttpRequest::OnFinishedChunk()
{
	if(m_InFlightChunk)
	{
		// Add the completed chunk to our total chunked bytes sent.
		m_OutChunksBytesSent += m_InFlightChunk->m_Data.Length();

		//Reset the timeout
		m_Timeout.Reset();
		this->DeleteChunk(m_InFlightChunk);
		m_InFlightChunk = NULL;

		// clear the tcp operation error flag
		m_TcpOpResult = NETTCP_RESULT_OK;
		m_TcpOpError = 0;
	}
}

void
netHttpRequest::ProcessOutboundChunks()
{
    netAssert(SENDSTATE_SENDING_CONTENT == m_SendState
            || SENDSTATE_SENDING_TERM_CHUNK == m_SendState);

	bool readyToProceed = true;

#if ENABLE_HTTP_INTERCEPTOR
	if(m_Filter && m_Filter->CanIntercept())
	{
		OnFinishedChunk();
	}
	else
#endif
#if RSG_DURANGO
	if(m_UseXblHttp)
	{
		/*
			On Xbox One, we cannot stream data to their native HTTP library since the entire
			request must be in memory before sending it. We used to copy all of the in-flight
			chunks into a contiguous growbuffer in netXblHttpRequest. However, if the request
			is large, we run out of memory and the request fails.

			Instead, we share the list of chunks between netHttpRequest and netXblHttpRequest
			so the copy doesn't need to be made. All chunks need to be kept "in-flight" at
			once, unlike on other platforms where we only have one in-flight chunk at a time.
			This complicates the already complicated code but uses far less memory.
		*/

        if(m_InFlightChunk)
        {
            //Reset the timeout
			m_Timeout.Reset();
			DeleteInFlightChunk();
            m_InFlightChunk = NULL;
        }

		while(!m_InFlightChunks.empty())
		{
			Chunk* chunk = m_InFlightChunks.front();
			if(chunk->m_Status.Pending())
			{
				break;
			}
			else
			{
				m_InFlightChunks.pop_front();
				this->DeleteChunk(chunk);
			}
		}

		bool allChunksQueued = (SENDSTATE_SENDING_TERM_CHUNK == m_SendState)
								|| (m_QueuedChunks.empty()
								&& TRANSFER_ENCODING_NORMAL == m_OutboundTransferEncoding);

		readyToProceed = !allChunksQueued || m_InFlightChunks.empty();
	}
	else 
#endif
	if(m_InFlightChunk && !m_InFlightChunk->m_TcpOp.Pending())
    {
        if(m_InFlightChunk->m_TcpOp.Succeeded())
        {
			OnFinishedChunk();
        }
        else
        {
			m_TcpOpResult = (netTcpResult)m_InFlightChunk->m_TcpOp.GetResultCode();
			m_TcpOpError = m_InFlightChunk->m_TcpOp.m_LastError;
            netError("Error sending content");
            this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_CHUNK_OPERATION_FAILED);
        }
    }

    if(this->Pending() && (!m_InFlightChunk && readyToProceed))
    {
        if(SENDSTATE_SENDING_TERM_CHUNK == m_SendState)
        {
            m_SendState = SENDSTATE_SUCCEEDED;
        }
        else if(m_QueuedChunks.empty()
                && TRANSFER_ENCODING_NORMAL == m_OutboundTransferEncoding)
        {
            m_SendState = SENDSTATE_SUCCEEDED;
        }
        else
        {
            Chunk* chunk = NULL;

            if(!m_QueuedChunks.empty())
            {
                chunk = m_QueuedChunks.front();
                m_QueuedChunks.pop_front();
            }
            else if(TRANSFER_ENCODING_CHUNKED == m_OutboundTransferEncoding && IsCommitted())
            {
                //Last chunk.
                chunk = NewChunk();
                if(!chunk)
                {
                    netError("Error allocating chunk");
                    this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_ALLOCATE_CHUNK_FAILED);
                }

                m_SendState = SENDSTATE_SENDING_TERM_CHUNK;
            }

            if(chunk)
            {
                if(this->SendChunk(chunk, (SENDSTATE_SENDING_TERM_CHUNK == m_SendState)))
                {
                    m_InFlightChunk = chunk;
                }
                else
                {
                    this->DeleteChunk(chunk);
                    this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_SEND_CHUNK_FAILED);
                }
            }
        }
    }
}

bool
netHttpRequest::SendChunk(Chunk* chunk, const bool termChunk)
{
    netDebug2("Sending chunk of size:%d...", chunk->m_Data.Length());

    rtry
    {
        rverify(this->Pending(),catchall,);
        rverify(m_SendState > SENDSTATE_SEND_HEADER,catchall,);

        rverify(chunk,catchall,);

        rverify(chunk->m_Data.Length() > 0 || termChunk,
                catchall,
                netError("Empty chunk"));

		// Durango's IXHR2 doesn't support chunked encoding
		if(DURANGO_ONLY(!m_UseXblHttp &&) TRANSFER_ENCODING_CHUNKED == m_OutboundTransferEncoding)
        {
            //Prepend the length of the chunk
            char tmp[64];
            formatf(tmp, "%x" CRLF, chunk->m_Data.Length());
            rverify(chunk->m_Data.PrependOrFail(tmp, istrlen(tmp)),
                    catchall,
                    netError("Error prepending chunk length to chunk"));

            //Append CRLF to the chunk
            rverify(chunk->m_Data.AppendOrFail(CRLF, istrlen(CRLF)),
                    catchall,
                    netError("Error appending CRLF to chunk"));
        }

#if !__NO_OUTPUT
        if (!m_Filter || !m_Filter->CanFilterRequest())
        {
            bool dumpIt = true;

            if(m_RawPath)
            {
                const char* ext = strrchr(m_RawPath, '.');
                if(ext)
                {
                    //Don't dump binary content
                    if(!PARAM_nethttpdumpbinary.Get() && 
						(  !stricmp(ext, ".jpg")
                        || !stricmp(ext, ".jpeg")
                        || !stricmp(ext, ".png")
                        || !stricmp(ext, ".dds")))
                    {
                        dumpIt = false;
                    }
                }
            }

            if(dumpIt)
            {
                if(!chunk->m_Data.IsReadOnly())
                {
                    this->DumpHttp("Outbound content", 
                                   (const char*)chunk->m_Data.GetBuffer(),
                                   chunk->m_Data.Length());
                }
                else
                {
                    //The readonly check is a hack.  The readonly feature was implemented
                    //specifically for uploading large video files, which are binary, and
                    //for which we don't want to dump outbound content.
                    const char msg[] = {"Binary data - not dumping"};
                    this->DumpHttp("Outbound content",  msg, sizeof(msg));
                }
            }
        }
#endif

#if ENABLE_HTTP_INTERCEPTOR
		if(m_Filter && m_Filter->CanIntercept())
		{
			netDebug2("Filter intercepting SendChunk()...");
		}
		else
#endif
#if RSG_DURANGO
		if(m_UseXblHttp)
		{

			rcheck(m_XblHttpRequest.QueueChunk((const u8*)chunk->m_Data.GetBuffer(),
											   chunk->m_Data.Length(),
											   &chunk->m_Status),
                    catchall,
                    netError("Error sending content"));

			m_InFlightChunks.push_back(chunk);

			bool finished = termChunk
							|| (m_QueuedChunks.empty()
							&& TRANSFER_ENCODING_NORMAL == m_OutboundTransferEncoding);

			if(finished)
			{
				rcheck(m_XblHttpRequest.Commit(),
						catchall,
						netError("Error sending content"));
			}
		}
		else
#endif
        {
            rcheck(netTcp::SendBufferAsync(m_Skt,
                                           chunk->m_Data.GetBuffer(),
                                           chunk->m_Data.Length(),
                                           m_Timeout.GetSecondsUntilTimeout() + TIMEOUT_FUDGE_AMOUNT_SECONDS,
                                           &chunk->m_TcpOp),
                    catchall,
                    netError("Error sending content"));
        }

        netAssert(!m_InFlightChunk);
        m_InFlightChunk = chunk;

        return true;
    }
    rcatchall
    {
    }

    return false;
}

static char* FindCrLf(char* str, const unsigned len)
{
    for(int i = 0; i < (int)len - 1; ++i)
    {
        if(CR == str[i] && LF == str[i+1])
        {
            return &str[i];
        }
    }

    return NULL;
}

bool 
netHttpRequest::ProcessBounceContent(u8* buffer,
                                   const unsigned len,
                                   fiHandle responseHandle,
                                   const fiDevice& responseDevice,
                                   const bool allContentReceived,
                                   unsigned* const amountConsumed,
                                   bool* allContentConsumed/* = NULL*/)
{
    netAssert(amountConsumed != NULL);

    if (allContentConsumed) *allContentConsumed = false;

    if(m_Filter && m_Filter->CanFilterResponse())
    {
#if !__NO_OUTPUT
        if(!PARAM_nethttpdumpbinary.Get() && IsBinaryResponse())
        {
            m_Filter->DisableHttpDump(true);
        }
#endif

        //Filter and consume the content
        bool hasDataPending = false;
        if (!m_Filter->FilterResponse((const u8*)buffer, 
                                      len, 
                                      allContentReceived,
                                      responseHandle,
                                      &responseDevice,
                                      amountConsumed,
                                      &hasDataPending))
        {
            netError("FilterResponse failed (len=%d  allContentReceived=%s)",
                     len, allContentReceived ? "TRUE" : "FALSE");
            return false;
        }

        if (allContentConsumed) *allContentConsumed = (*amountConsumed == len) && !hasDataPending;
    }
    else
    {
        if (len)
        {
            //Otherwise immediately deliver the content to our response device
            *amountConsumed = responseDevice.Write(responseHandle, (const u8*)buffer, len);            

#if !__NO_OUTPUT
            bool dumpIt = true;

            if(!PARAM_nethttpdumpbinary.Get() && IsBinaryResponse())
            {
                //Don't dump binary content
                dumpIt = false;
            }

            if(dumpIt)
            {
                DumpHttp("Inbound content", (const char*)buffer, *amountConsumed);
            }
#endif
		}

        if (allContentConsumed) *allContentConsumed = (*amountConsumed == len);
    }

    return true;
}

#if !__NO_OUTPUT

const char* netHttpRequest::GetContextString(char* ctx, const unsigned bufSize) const
{
	return formatf_sized(ctx, bufSize, "[reqid:%d]%s", m_RequestId, m_ContextStr);
}

bool
netHttpRequest::IsBinaryResponse()
{
    if(m_RawPath)
    {
        const char* ext = strrchr(m_RawPath, '.');
        if(ext)
        {
            //Don't dump binary content
            if(!stricmp(ext, ".jpg")
                || !stricmp(ext, ".jpeg")
                || !stricmp(ext, ".png")
                || !stricmp(ext, ".dds")
                || !stricmp(ext, ".dll")
                || !stricmp(ext, ".rpf")
                || !stricmp(ext, ".exe")
                || !stricmp(ext, ".dat")
                || !stricmp(ext, ".bin"))
            {
                return true;
            }
        }
    }

    char val[256];
    unsigned valLen = COUNTOF(val);
    if(this->GetResponseHeaderValue("Content-Type", val, &valLen))
    {
        // there are a lot more binary types. We could reverse the logic
        // and whitelist certain types instead (text, xml, html, json).
        if((stristr(val, "binary") != nullptr) ||
            (stristr(val, "octet-stream") != nullptr) ||
            (stristr(val, "image") != nullptr))
        {
            return true;
        }
    }

    return false;
}
#endif

bool
netHttpRequest::ReceiveHeader()
{
    rtry
    {
#if RSG_DURANGO
		if(m_UseXblHttp)
		{
			if(m_XblHttpRequest.HasError())
			{
				netError("Error receiving: 0x%x", m_XblHttpRequest.GetErrorCode());
				this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_XBL_RECEIVE_HEADER_FAILED);
			}
            else if(m_XblHttpRequest.HaveHeaders())
            {
                unsigned headerLen;
                rverify(m_XblHttpRequest.GetResponseHeaderLength(&headerLen),
                        catchall,
                        netError("Error getting response header length"));

                m_InHeader.Clear();
                char* header = (char*)Alloca(char, headerLen);
                rverify(m_XblHttpRequest.GetResponseHeader(header, &headerLen),
                        catchall,
                        netError("Error getting response header"));

                rverify(m_InHeader.AppendOrFail(header)
                        && (m_InHeader.EndsWith(CRLF CRLF) || m_InHeader.Append(CRLF CRLF)),    //Trailing CRLF CRLF indicates we have a complete header
                        catchall,
                        netError("Error copying to response header"));

				m_Timeout.Reset(); //Reset the timeout

				//If this was a serious error, unhook the filter 
				//so the response is passed through unchanged.
				if ((this->GetStatusCode() >= 400) && m_Filter)
				{
					m_Filter = 0;
				}
            }
		}
		else
#endif
        {
            unsigned numRcvd = 0;

            if(m_BounceBufLen < m_BounceBufMaxLen)
            {
#if ENABLE_HTTP_INTERCEPTOR
				if(m_Filter && m_Filter->CanIntercept())
				{
					m_Filter->Receive(&m_BounceBuf[m_BounceBufLen],
									  m_BounceBufMaxLen - m_BounceBufLen,
									  &numRcvd);
				}
				else
#endif
				{
					netTcpResult result =
						netTcp::Receive(m_Skt,
										&m_BounceBuf[m_BounceBufLen],
										m_BounceBufMaxLen - m_BounceBufLen,
										&numRcvd, &m_TcpOpError);

					if(NETTCP_RESULT_DISCONNECTED == result && m_BounceBufLen > 0)
					{
						//The connection was closed but we still have data to process.
						result = NETTCP_RESULT_OK;
					}

					rcheck(NETTCP_RESULT_OK == result,
							catchall,
							netError("Error receiving on socket:0x%08x (0x%08x)", m_Skt, result));
				}

                if (numRcvd)
                {
                    m_BounceBufLen += numRcvd;
                    m_Timeout.Reset(); //Reset the timeout

                    //netDebug3("Received %d bytes into bounce buffer (%d now)", numRcvd, m_BounceBufLen);
                }
            }

            if(m_BounceBufLen > 0)
            {
                //Check for CRLF CRLF to indicate the end of the header

                const char* eob = &m_BounceBuf[m_BounceBufLen];
                int numConsumed = 0;
                for(const char* p = m_BounceBuf; p < eob && 0 == numConsumed; ++p)
                {
                    switch(eob-p)
                    {
                    case 1:
                        if(CR == p[0])
                        {
                            //Leave the CR in the buffer
                            numConsumed = m_InHeader.Append(m_BounceBuf, ptrdiff_t_to_int(p-m_BounceBuf));
                        }
                        break;
                    case 2:
                        if(CR == p[0] && LF == p[1])
                        {
                            //Leave the CRLF in the buffer
                            numConsumed = m_InHeader.Append(m_BounceBuf, ptrdiff_t_to_int(p-m_BounceBuf));
                        }
                        break;
                    case 3:
                        if(CR == p[0]
                            && LF == p[1]
                            && CR == p[2])
                        {
                            //Leave the CRLF CR in the buffer
                            numConsumed = m_InHeader.Append(m_BounceBuf, ptrdiff_t_to_int(p-m_BounceBuf));
                        }
                        break;
                    default:
                        if(CR == p[0]
                            && LF == p[1]
                            && CR == p[2]
                            && LF == p[3])
                        {
                            //Consume everything including the CRLF CRLF
                            numConsumed = m_InHeader.Append(m_BounceBuf, ptrdiff_t_to_int((p+4)-m_BounceBuf));
                        }
                    }
                }

                if(0 == numConsumed && m_BounceBufLen > 0)
                {
                    //No CR or LF in the buffer - just consume the whole thing
                    numConsumed = m_InHeader.Append(m_BounceBuf, m_BounceBufLen);
                }

                rverify(numConsumed >= 0 && numConsumed <= (int)m_BounceBufLen,
                        catchall,
                        netError("Error appending to response header buffer"));

                if(numConsumed > 0)
                {
                    if(numConsumed < (int)m_BounceBufLen)
                    {
                        memmove(m_BounceBuf, &m_BounceBuf[numConsumed], m_BounceBufLen-numConsumed);
                    }

                    m_BounceBufLen -= numConsumed;
                }

                if(!m_InHeader.StartsWith("HTTP/1."))
                {
                    this->TrimJunk(&m_InHeader);
                }

                //Did we reach the end of the header?
                if(m_InHeader.EndsWith(CRLF CRLF) && m_InHeader.StartsWith("HTTP/1."))
                {
                    if(NET_HTTPSTATUS_CONTINUE == this->GetStatusCode())
                    {
                        //Wait for the next header.
                        m_InHeader.Clear();
                    }
                    //We do not detach the filter if there was a serious error
                    //so that it may still handle any special processing of the
                    //response.
                }
            }
        }

        return true;
    }
    rcatchall
    {
    }

    return false;
}

bool 
netHttpRequest::ReceiveBody(u8* buffer, const unsigned bufferSize, const bool haveBufferedData, unsigned* const out_amountReceived)
{
    PF_FUNC(ReceiveBodyCost);

    netAssert(out_amountReceived != NULL);

    rtry
    {
        *out_amountReceived = 0;
        unsigned numRcvd = 0;

        if(bufferSize > 0)
        {
            // Sanity check, if the buffer we're being passed is our bounce buffer, then it should
            // point to the first available spot in the buffer
            rverify(buffer == (u8*)&m_BounceBuf[m_BounceBufLen] ||
                    buffer >= (u8*)&m_BounceBuf[m_BounceBufMaxLen] ||
                    &buffer[bufferSize] <= (u8*)m_BounceBuf,
                    catchall,);

#if ENABLE_HTTP_INTERCEPTOR
			if(m_Filter && m_Filter->CanIntercept())
			{
				m_Filter->Receive(buffer,
								  bufferSize,
								  &numRcvd);
			}
			else
#endif
#if RSG_DURANGO
			if(m_UseXblHttp)
			{
				if(m_XblHttpRequest.HasError())
				{
					netError("Error receiving body: 0x%x", m_XblHttpRequest.GetErrorCode());
					this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_XBL_RECEIVE_BODY_FAILED);
				}
				else if((RECVSTATE_RECEIVING_NORMAL_CONTENT == m_RecvState
					|| RECVSTATE_RECEIVING_CHUNKED_CONTENT == m_RecvState))
				{
					rverify(m_XblHttpRequest.ReadData((u8*)buffer,
													  bufferSize,
													  &numRcvd,
													  &m_AllContentReceived),
								catchall,
								netError("Error receiving response"));

					if((numRcvd == 0) && m_AllContentReceived)
					{
						netDebug3("ReceiveBody :: read 0 bytes and finished receiving the response.");
						m_InContentLen = m_InContentBytesRcvd;
						m_RecvState = RECVSTATE_FINISH_CONSUMING_CONTENT;
					}
				}
			}
			else
#endif
            {
                netTcpResult result =
                    netTcp::Receive(m_Skt,
                                    buffer,
                                    bufferSize,
                                    &numRcvd);

                // Don't treat the other end closing the connection as fatal
                // if we still have body data being buffered externally
                // Don't think this is actually necessary, since we should
                // be in the RECVSTATE_FINISH_CONSUMING_CONTENT state if
                // this is the case
                if(NETTCP_RESULT_DISCONNECTED == result && haveBufferedData)
                {
                    //The connection was closed but we still have data to process.
                    result = NETTCP_RESULT_OK;
                }

                rcheck(NETTCP_RESULT_OK == result,
                        catchall,
                        netError("Error receiving on socket:0x%08x (0x%08x)", m_Skt, result));
            }

            if (numRcvd)
            {
                *out_amountReceived = numRcvd;
                m_Timeout.Reset(); //Reset the timeout

                //netDebug3("Received %d bytes into bounce buffer (%d now)", numRcvd, m_BounceBufLen);
            }
        }

        return true;
    }
    rcatchall
    {
    }

    return false;
}

bool
netHttpRequest::ConsumeBody(u8* buffer, const unsigned bufferSize, unsigned* const out_amountConsumed, fiHandle responseHandle, const fiDevice& responseDevice)
{
    rtry
    {
        netAssert(out_amountConsumed != NULL);

        // Sanity check, if the buffer we're being passed is our bounce buffer, then it should
        // be the beginning of our bounce buffer
        rverify(buffer == (u8*)m_BounceBuf ||
                buffer >= (u8*)&m_BounceBuf[m_BounceBufMaxLen] ||
                &buffer[bufferSize] <= (u8*)m_BounceBuf,
                catchall,);

        *out_amountConsumed = 0;

        u8* readBuffer = buffer;
        unsigned readBufferSize = bufferSize;

        if (RECVSTATE_RECEIVING_NORMAL_CONTENT == m_RecvState ||
            RECVSTATE_RECEIVING_CHUNKED_CONTENT == m_RecvState)
        {
            if(0 == readBufferSize)
            {
                //No processing to be done
            }
            else if(RECVSTATE_RECEIVING_NORMAL_CONTENT == m_RecvState)
            {
                // Make sure the amount we're receiving won't push us over what is expected
                netAssert(m_InContentBytesRcvd + readBufferSize <= m_InContentLen);
                //netDebug3("Have received %d of %d content bytes", m_InContentBytesRcvd, m_InContentLen);

                // Compute whether or not all content has been received based on what we've received
                // so far and what we're receiving right now
                bool allContentReceived = (m_InContentLen == m_InContentBytesRcvd + readBufferSize);

                //Process and consume content we've received
                rverify(ProcessBounceContent(readBuffer,
                                             readBufferSize,
                                             responseHandle,
                                             responseDevice,
                                             allContentReceived,
                                             out_amountConsumed),
                        catchall,
                        netError("Failed to ProcessBounceContent"));

                //Update number of content bytes we've received based on what was consumed
                m_InContentBytesRcvd += *out_amountConsumed;

                if (allContentReceived)
                {
                    //netDebug3("Received all content, switching to SENDSTATE_FINISH_CONSUMING_CONTENT");
                    m_RecvState = RECVSTATE_FINISH_CONSUMING_CONTENT;
                }
            }
            else if(RECVSTATE_RECEIVING_CHUNKED_CONTENT == m_RecvState)
            {
#if RSG_DURANGO
				if(m_UseXblHttp)
				{
					// IXMLHttpRequest2 automatically unchunks data as it streams in

					//Process and consume content we've received
					rverify(ProcessBounceContent(readBuffer,
												 readBufferSize,
												 responseHandle,
												 responseDevice,
												 m_AllContentReceived,
												 out_amountConsumed),
												 catchall,
												 netError("Failed to ProcessBounceContent"));

					//Update number of content bytes we've received based on what was consumed
					m_InContentBytesRcvd += *out_amountConsumed;

					if (m_AllContentReceived)
					{
						m_InContentLen = m_InContentBytesRcvd;
						//netDebug3("Received all content, switching to SENDSTATE_FINISH_CONSUMING_CONTENT");
						m_RecvState = RECVSTATE_FINISH_CONSUMING_CONTENT;
					}
				}
				else
#endif
				{
					if(m_InContentBytesRcvd == m_InContentLen && readBufferSize > 2)
					{
						//If this is not the first chunk then the first two
						//chars should be the CRLF that trails the previous
						//chunk.
						netAssert(0 == m_InContentLen
							|| (char*)readBuffer == FindCrLf((char*)readBuffer, readBufferSize));

						//The chunk header starts at the end of the previous chunk,
						//which was terminated by a CRLF.
						char* chunkHeader = (m_InContentLen > 0)
								? (char*)&readBuffer[2]   //skip the CRLF at the end of the previous chunk
								: (char*)readBuffer;

						//Find the end of the chunk header
						//Don't use strstr() here because the chunkheader is
						//not null terminated.
						char* crlf = FindCrLf(chunkHeader, ptrdiff_t_to_int((char*)&readBuffer[readBufferSize] - chunkHeader));

						if(crlf)
						{
							//Null terminate the chunk header
							*crlf = '\0';

							unsigned chunkSize;
							if(sscanf(chunkHeader, "%x" CRLF, &chunkSize)
								|| sscanf(chunkHeader, "%X" CRLF, &chunkSize))
							{
								m_InContentLen += chunkSize;

								if(0 == chunkSize)
								{
									//End of transmission
									m_RecvState = RECVSTATE_FINISH_CONSUMING_CONTENT;

									crlf += 2;  //The last CRLF at the end of the body
								}
							}

							//Remove the chunk header from the read buffer
							const int amountToRemove = ptrdiff_t_to_int(crlf - (char*)readBuffer) + 2;
							*out_amountConsumed += amountToRemove;
							readBufferSize -= amountToRemove;
							readBuffer += amountToRemove;
						}
					}

					if(m_InContentLen > 0)
					{
						// Compute how much of the buffer we should read, basically need to calculate the remaining
						// chunk size from above...
						unsigned numNewBytes = m_InContentLen - m_InContentBytesRcvd;
						if(numNewBytes > readBufferSize)
						{
							numNewBytes = readBufferSize;
						}

						if(numNewBytes > 0)
						{
							unsigned amountProcessed = 0;
							rverify(ProcessBounceContent(readBuffer,
														 numNewBytes,
														 responseHandle,
														 responseDevice,
														 false,
														 &amountProcessed),
									catchall, 
									netError("Failed to ProcessBounceContent for %d chunk bytes", numNewBytes));

							*out_amountConsumed += amountProcessed;
							m_InContentBytesRcvd += amountProcessed;
						}
					}
				}
            }
        }
        else if (RECVSTATE_FINISH_CONSUMING_CONTENT == m_RecvState)
        {
            unsigned amountProcessed = 0;
            bool allContentConsumed;

            if (!ProcessBounceContent((u8*)readBuffer,
                                      readBufferSize,
                                      responseHandle,
                                      responseDevice,
                                      true,
                                      &amountProcessed,
                                      &allContentConsumed))
            {
                netError("Failed to ProcessBounceContent for %d bytes in RECVSTATE_FINISH_CONSUMING_CONTENT", readBufferSize);
                this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_PROCESS_BOUNCE_CONTENT_FAILED);
            }
            else
            {
                *out_amountConsumed += amountProcessed;

				//Update number of content bytes we've received based on what was consumed
				m_InContentBytesRcvd += amountProcessed;

                if (allContentConsumed)
                {
#if RSG_DURANGO
					if(m_UseXblHttp)
					{
						m_InContentLen = m_InContentBytesRcvd;
					}
#endif
                    SetSucceeded();
                }
            }
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
netHttpRequest::ReadBody(u8* readBuffer, const unsigned readBufferSize, u8* writeBuffer, const unsigned writeBufferSize, unsigned* const amountWritten)
{
    rtry
    {
        //Wrap the write buffer with an fiDevice since that's what we use internally
        char memFileName[RAGE_MAX_PATH];
        const fiDevice& outputDevice = fiDeviceMemory::GetInstance();
        fiDevice::MakeMemoryFileName(memFileName, RAGE_MAX_PATH, writeBuffer, writeBufferSize, false, "netHttpRequest::ReadBody");
        fiHandle outputHandle = outputDevice.Create(memFileName);

        rverify(outputHandle != fiHandleInvalid, catchall, );
		
        bool readResult = ReadBody(readBuffer, readBufferSize, outputHandle, outputDevice);
        outputDevice.Flush(outputHandle);

        if (amountWritten)
        {
            const int devicePos = outputDevice.Seek(outputHandle, 0, seekCur);
            *amountWritten = devicePos >= 0 ? devicePos : 0;
        }

        outputDevice.Close(outputHandle);

        return readResult;
    }
    rcatchall
    {
        if (amountWritten)
        {
            *amountWritten = 0;
        }

        return false;
    }
}

bool
netHttpRequest::ReadBody(u8* buffer, const unsigned bufferSize, fiHandle responseHandle, const fiDevice& responseDevice)
{
    rtry
    {
        PF_FUNC(ReadBodyCost);

        // Sanity check, if the buffer we're being passed is our bounce buffer, then it should
        // be the beginning of our bounce buffer
        rverify(buffer == (u8*)m_BounceBuf ||
                buffer >= (u8*)&m_BounceBuf[m_BounceBufMaxLen] ||
                &buffer[bufferSize] <= (u8*)m_BounceBuf,
                catchall,);

		unsigned amountRecvd;
		unsigned amountRecvdFromSocket;
		unsigned amountConsumed;
		return ReadBodyOnce(buffer, bufferSize, &amountRecvd, &amountRecvdFromSocket, &amountConsumed, responseHandle, responseDevice);
    }
    rcatchall
    {
    }

    return false;
}

bool 
netHttpRequest::ReadBodyOnce(u8* buffer, const unsigned bufferSize, unsigned* const out_amountRecvd, unsigned* const out_amountRecvdFromSocket, unsigned* const out_amountConsumed, fiHandle responseHandle, const fiDevice& responseDevice)
{
    netAssert(out_amountRecvd != NULL);
    netAssert(out_amountRecvdFromSocket != NULL);
    netAssert(out_amountConsumed != NULL);

    *out_amountRecvd = 0;
    *out_amountRecvdFromSocket = 0;
    *out_amountConsumed = 0;

    rtry
    {
        // Sanity check, if the buffer we're being passed is our bounce buffer, then it should
        // point to the beginning of the bounce buffer
        rverify(buffer == (u8*)m_BounceBuf ||
                buffer >= (u8*)&m_BounceBuf[m_BounceBufMaxLen] ||
                &buffer[bufferSize] <= (u8*)m_BounceBuf,
                catchall,);

        rverify(responseHandle != fiHandleInvalid,
                catchall,
                netError("Invalid handle in ReadBody"));

        if (RECVSTATE_RECEIVING_NORMAL_CONTENT == m_RecvState || 
            RECVSTATE_RECEIVING_CHUNKED_CONTENT == m_RecvState ||
            RECVSTATE_FINISH_CONSUMING_CONTENT == m_RecvState)
        {
            u8* recvBuffer = buffer;
            unsigned recvBufferSize = bufferSize;

            // Receive anything that's already in our bounce buffer into our recvBuffer first
            const unsigned amountRecvdFromBounceBuf = Min<unsigned>(recvBufferSize, m_BounceBufLen);
            if (amountRecvdFromBounceBuf > 0)
            {
                // Skip if this is our bounce buffer
                if (buffer != (u8*)m_BounceBuf)
                {
                    sysMemCpy(recvBuffer, m_BounceBuf, amountRecvdFromBounceBuf);
                    datGrowBuffer::Remove(m_BounceBuf, &m_BounceBufLen, 0, amountRecvdFromBounceBuf);
                }
                recvBuffer += amountRecvdFromBounceBuf;
                recvBufferSize -= amountRecvdFromBounceBuf;
            }
        
            // Now read from our socket if there is data left to read
            unsigned amountRecvdFromSocket = 0;
            if (RECVSTATE_FINISH_CONSUMING_CONTENT != m_RecvState)
            {
                rcheck(ReceiveBody(recvBuffer, recvBufferSize, amountRecvdFromBounceBuf > 0, &amountRecvdFromSocket),
                       catchall,
                       netError("Failed to receive body"));

                *out_amountRecvdFromSocket = amountRecvdFromSocket;

                // If using our bounce buffer, update its length
                if (buffer == (u8*)m_BounceBuf)
                {
                    m_BounceBufLen += amountRecvdFromSocket;

                    // Shouldn't be possible to overflow
                    netAssert(m_BounceBufLen <= m_BounceBufMaxLen);
                }
            }

            // Consume the data that we just received
            // Even if we didn't receive anything, we still need to call Consume since filters
            // may have data buffered, or we need to finish consuming content
            unsigned amountConsumed = 0;
            // It's possible we received more than we were supposed to, so clamp ourselves to our
            // receive buffer size again. Should only happen with Xhttp
            const unsigned amountRecvd = Min<unsigned>(amountRecvdFromBounceBuf + amountRecvdFromSocket, bufferSize);
            *out_amountRecvd = amountRecvd;
            rcheck(ConsumeBody(buffer, amountRecvd, &amountConsumed, responseHandle, responseDevice),
                   catchall,
                   netError("Failed to consume body"));

            *out_amountConsumed = amountConsumed;

            // If this is our bounce buffer, we need to remove the consumed elements
            if (buffer == (u8*)m_BounceBuf)
            {
                datGrowBuffer::Remove(m_BounceBuf, &m_BounceBufLen, 0, amountConsumed);
            }
            else
            {
                //buffer != (u8*)m_BounceBuf
                // If we have some buffered data left that couldn't be consumed, buffer it back in our bounce buffer
                // so that it doesn't get lost
                // I.e. this happens if the filter doesn't have enough data to decrypt yet
                // If it doesn't fit, then it's fatal since we're losing data
                const unsigned amountRemaining = amountRecvd - amountConsumed;
                rverify(amountRemaining <= m_BounceBufMaxLen - m_BounceBufLen,
                        catchall,
                        netError("Not enough room to buffer unconsumed data"));

                if (amountRemaining > 0)
                {
                    // We need to insert it into the beginning of our bounce buffer
                    // So shift everything currently in the bounce buffer back to make room for it
                    memmove(&m_BounceBuf[amountRemaining], m_BounceBuf, m_BounceBufLen);
                    sysMemCpy(m_BounceBuf, &buffer[amountConsumed], amountRemaining);
                    m_BounceBufLen += amountRemaining;
                }
            }
        }

        return true;
    }
    rcatchall
    {
        this->Abort(netStatus::NET_STATUS_FAILED, NET_HTTPSTATUS_BAD_REQUEST, NET_HTTPABORT_READ_BODY_ONCE_FAILED);
    }

    return false;
}

void
netHttpRequest::CloseResponseDevice()
{
    if(m_ResponseDevice)
    {
        m_ResponseDevice->Flush(m_ResponseHandle);
        if(m_OwnResponseDevice)
        {
            m_ResponseDevice->Close(m_ResponseHandle);
        }
        m_ResponseHandle = fiHandleInvalid;
        m_ResponseDevice = NULL;
        m_OwnResponseDevice = false;
    }
}

void
netHttpRequest::SetSucceeded()
{
    netDebug("Finished receiving HTTP content for '%s'", GetUri());

	netAssert((m_InContentLen == m_InContentBytesRcvd) || (NET_HTTP_VERB_GET != m_HttpVerb && NET_HTTP_VERB_POST != m_HttpVerb));

    if(m_Skt > -1)
    {
        netTcp::Close(m_Skt);
        m_Skt = -1;
    }

    this->CloseResponseDevice();

#if RSG_DURANGO
	if(m_UseXblHttp)
	{
		m_XblHttpRequest.Clear();
		DeleteAllInFlightChunks();
	}
#endif

    //At this point the sending thread might not have updated
    //the status of the in-flight chunk.  However we've already
    //received a complete reply from the host so we know all in-flight
    //chunks have been delivered.  To keep things rolling we just cancel
    //any that are still "pending".
    if(m_InFlightChunk)
    {
        netDebug2("Canceling in-flight chunks that are still lingering");
        netTcp::CancelAsync(&m_InFlightChunk->m_TcpOp);
		DeleteInFlightChunk();
        m_InFlightChunk = NULL;
    }
	// Same as above, clear the pending chunks
	while(!m_QueuedChunks.empty())
	{
		Chunk* chunk = m_QueuedChunks.front();
		m_QueuedChunks.pop_front();
		this->DeleteChunk(chunk);
	}

    //Allow our filter to override successfully completing
    if (m_Filter
        && !m_Filter->AllowSucceeded(this))
    {
        m_HttpStatusCode = NET_HTTPSTATUS_BAD_REQUEST;
        m_Status->SetStatus(netStatus::NET_STATUS_FAILED, m_HttpStatusCode);

    }
    else
    {
        m_Status->SetSucceeded(GetStatusCode());
    }

    m_Status = NULL;
    m_SendState = SENDSTATE_SUCCEEDED;
    m_RecvState = RECVSTATE_SUCCEEDED;

    //Remove from the run queue
    Dequeue();
}

void
netHttpRequest::Abort(const netStatus::StatusCode statusCode, const netHttpStatusCodes httpStatusCode, const netHttpAbortReason abortReason)
{
	// Set the reason for the abort.
	m_AbortReason = abortReason;

    const bool wasPending = this->Pending();

    if(m_HostResolutionStatus.Pending())
    {
		netResolver::Cancel(&m_HostResolutionStatus);
    }

#if RSG_DURANGO
	if(m_AuthTokenStatus.Pending())
	{
		netXblHttp::CancelAuthTokenRequest(&m_AuthTokenStatus);
		netXblHttp::FreeAuthToken(&m_AuthToken);
	}

	if(m_UseXblHttp)
	{
		m_XblHttpRequest.Cancel();
		m_XblHttpRequest.Clear();
	}
	else
#endif
    if(m_ConnectTcpOp.Pending())
    {
        netTcp::CancelAsync(&m_ConnectTcpOp);
    }

#if RSG_DURANGO
	if(m_UseXblHttp)
	{
		DeleteAllInFlightChunks();
	}
#endif

    if(m_InFlightChunk)
    {
		DeleteInFlightChunk();
        m_InFlightChunk = NULL;
    }

    while(!m_QueuedChunks.empty())
    {
        Chunk* chunk = m_QueuedChunks.front();
        m_QueuedChunks.pop_front();
        this->DeleteChunk(chunk);
    }

    if(m_CurChunk)
    {
        this->DeleteChunk(m_CurChunk);
        m_CurChunk = NULL;
    }

    if(-1 != m_Skt)
    {
        netTcp::Close(m_Skt);
        m_Skt = -1;
    }

    if(m_Status)
    {
        m_HttpStatusCode = httpStatusCode;
        m_Status->SetStatus(statusCode, httpStatusCode);
        m_Status = NULL;
    }

    if(wasPending)
    {
        netDebug("Aborted HTTP request to: '%s'", GetUri());

        m_SendState = SENDSTATE_ERROR;
        m_RecvState = RECVSTATE_ERROR;
    }

    //Remove from the run queue
    Dequeue();
}

void
netHttpRequest::TrimJunk(atStringBuilder* sb) const
{
    const char* buf = sb->ToString();
    const char* header = buf ? strstr(buf, "HTTP/1.1") : NULL;

    if(buf && !header)
    {
        header = strstr(buf, "HTTP/1.0");
    }

    if(buf && !header)
    {
        //No header found.  Check for a partial header at the end
        //of the buffer.

        const int bufLen = sb->Length();
        const char* p = buf + bufLen - 1;

        if(bufLen >= 1 && !strcmp(&p[0], "H")){header = &p[0];}
        else if(bufLen >= 2 && !strcmp(&p[-1], "HT")){header = &p[-1];}
        else if(bufLen >= 3 && !strcmp(&p[-2], "HTT")){header = &p[-2];}
        else if(bufLen >= 4 && !strcmp(&p[-3], "HTTP")){header = &p[-3];}
        else if(bufLen >= 5 && !strcmp(&p[-4], "HTTP/")){header = &p[-4];}
        else if(bufLen >= 6 && !strcmp(&p[-5], "HTTP/1")){header = &p[-5];}
        else if(bufLen >= 7 && !strcmp(&p[-6], "HTTP/1.")){header = &p[-6];}
        else
        {
            header = p + 1;
        }
    }

    if(header && buf != header)
    {
        const int sizeofJunk = ptrdiff_t_to_int(header - buf);
        sb->Remove(0, sizeofJunk);
    }
}

void
netHttpRequest::DumpHttp(const char* OUTPUT_ONLY(label),
                        const char* OUTPUT_ONLY(str),
                        const unsigned OUTPUT_ONLY(strLen)) const
{
#if !__NO_OUTPUT
    if(Channel_ragenet_http.TtyLevel >= DIAG_SEVERITY_DEBUG3
        || (m_Options & NET_HTTP_DUMP)
        || (PARAM_nethttpdump.Get() && !strstr(m_RawHost, "bugstar"))
		|| (PARAM_nethttpdumpbugstar.Get() && strstr(m_RawHost, "bugstar")) )
    {
        netDebug1("%s:\n", label);
        diagLoggedPrintLn(str, strLen);
    }
#endif  //__NO_OUTPUT
}

bool
netHttpRequest::HasHeaderValue(const char* header, const char* fieldName)
{
    bool success = false;

    const int nameLen = istrlen(fieldName);

    //End of header
    const char* eoh = header + strlen(header);
    const char* line = header;
    const char* nextLine = eoh;

    for(; !success && line < eoh; line = nextLine)
    {
        //End of line
        const char* eol = strstr(line, CRLF);
        if(!eol)
        {
            //Bail
            nextLine = eoh;
            continue;
        }

        nextLine = eol + 2;

        //End of field
        const char* eof = strchr(line, ':');

        if(!eof){continue;}

        if((eof - line) != nameLen)
        {
            continue;
        }

        if(0 != strnicmp(fieldName, line, nameLen))
        {
            continue;
        }

        success = true;
    }

    return success;
}

bool
netHttpRequest::GetHeaderValue(const char* header,
                                const char* fieldName,
                                char* value,
                                unsigned* valLen)
{
    bool success = false;

    rtry
    {
        const char* tmpVal = NULL;
        const int nameLen = istrlen(fieldName);

        //End of initial line
        const char* eol = strstr(header, CRLF);

        while(!tmpVal && eol)
        {
            const char* line = eol + 2;
            eol = strstr(line, CRLF);
            if(!eol){continue;}

            //End of field
            const char* eof = strchr(line, ':');

            if(!eof){continue;}

            if((eof - line) != nameLen)
            {
                continue;
            }

            if(0 != strnicmp(fieldName, line, nameLen))
            {
                continue;
            }

            //Skip spaces
            tmpVal = eof + 1;
            while(tmpVal < eol && isspace(*tmpVal))
            {
                ++tmpVal;
            }
        }

        rcheck(tmpVal && eol && tmpVal < eol,catchall,);

        const unsigned tmpValLen = ptrdiff_t_to_int(eol - tmpVal) + 1;
		if (value != NULL)
		{
			if(netVerify(tmpValLen < *valLen))
			{
				// We're copying the tmpValLen since we only want to copy the characters 
				// in the header. While this isn't quite correct for safecpy, we're protected
				// by the assert above and changing this would break other code.
				safecpy(value, tmpVal, tmpValLen);
				success = true;
			}
		}

        *valLen = tmpValLen;
    }
    rcatchall
    {
        *valLen = 0;
    }

    return success;
}

bool 
netHttpRequest::GetCookieValue(const char* header,
                               const char* cookieName,
                               char* value,
                               unsigned* valLen)
{
    bool success = false;

    rtry
    {
        const char* fieldName = "Set-Cookie";
        const char* fieldVal = NULL;

        const int cookieNameLen = istrlen(cookieName);
        const int fieldNameLen = istrlen(fieldName);

        //End of initial line
        const char* eol = strstr(header, CRLF);

        while(!fieldVal && eol)
        {
            const char* line = eol + 2;
            eol = strstr(line, CRLF);
            if(!eol) { continue; }

            //If this is a field, find end of its name
            const char* eof = strchr(line, ':');
            if(!eof)
            {
                continue;
            }

            //Does it match our field name?
            if((eof - line) != fieldNameLen)
            {
                continue;
            }
            else if(0 != strncmp(fieldName, line, fieldNameLen))
            {
                continue;
            }

            //Skip spaces to get to the field value
            fieldVal = eof + 1;
            while(fieldVal < eol && isspace(*fieldVal))
            {
                ++fieldVal;
            }

            //Does the field value start with our cookie name?
            const char* endOfName = strchr(fieldVal, '=');
            rverify(endOfName != NULL, catchall, );

            int curCookieNameLen = (int)(endOfName - fieldVal);

            if ((cookieNameLen != curCookieNameLen) 
                || strncmp(fieldVal, cookieName, cookieNameLen))
            {
                fieldVal = 0;
                continue;
            }

            //Copy from the end of name to the end of value (or eol, whichever comes first)
            const char* startOfValue = endOfName + 1;
            const char* endOfValue = strchr(startOfValue, ';');
            if (endOfValue == NULL) endOfValue = eol;

            const unsigned tmpvalueLen = ptrdiff_t_to_int(endOfValue - startOfValue) + 1;

            if(value && (tmpvalueLen < *valLen))
            {
                safecpy(value, startOfValue, tmpvalueLen);
                success = true;
            }

            *valLen = tmpvalueLen;
            break;
        }
    }
    rcatchall
    {
        *valLen = 0;
    }

    return success;
}

netHttpRequest::Chunk*
netHttpRequest::NewChunk()
{
    Chunk* chunk = (Chunk*) this->Allocate(sizeof(Chunk));
    if(chunk)
    {
        new (chunk) Chunk();
        chunk->m_Data.Init(m_Allocator, 0);
    }

	m_ChunkAllocationError = (chunk == NULL);
    return chunk;
}

void
netHttpRequest::DeleteChunk(Chunk* chunk)
{
#if RSG_DURANGO
	if(m_UseXblHttp)
	{
		// if the chunk is not managed by the list of in-flight chunks, then delete it now,
		// otherwise we'll delete it later when processing the list.
		if(chunk)
		{
			for(ChunkList::const_iterator it = m_InFlightChunks.begin(); it != m_InFlightChunks.end(); ++it)
			{
				const Chunk* inFlightChunk = *it;
				if(chunk == inFlightChunk)
				{
					// chunk will be deleted when we process the in-flight chunks list
					return;
				}
			}			
		}
	}
#endif

    if(chunk)
    {
        if(chunk->m_TcpOp.Pending())
        {
            netTcp::CancelAsync(&chunk->m_TcpOp);
        }
        chunk->~Chunk();
        this->Free((char*) chunk);
    }
}

void
netHttpRequest::DeleteInFlightChunk()
{
	if(m_InFlightChunk)
	{
		this->DeleteChunk(m_InFlightChunk);
		m_InFlightChunk = nullptr;
	}
}

#if RSG_DURANGO
void
netHttpRequest::DeleteAllInFlightChunks()
{
	if(m_InFlightChunk)
	{
		DeleteInFlightChunk();
		m_InFlightChunk = NULL;
	}

	while(!m_InFlightChunks.empty())
	{
		Chunk* chunk = m_InFlightChunks.front();
		m_InFlightChunks.pop_front();
		this->DeleteChunk(chunk);
	}
}
#endif

char*
netHttpRequest::Allocate(const unsigned numChars) const
{
    if(m_Allocator)
    {
        return (char*) m_Allocator->TryLoggedAllocate(numChars, sizeof(void*), 0, __FILE__, __LINE__);
    }
    else
    {
        return rage_new char[numChars];
    }
}

void
netHttpRequest::Free(char* chars) const
{
    if(chars)
    {
        if(m_Allocator)
        {
            m_Allocator->Free(chars);
        }
        else
        {
            delete[] chars;
        }
    }
}

bool
netHttpRequest::CanRun() const
{
	SYS_CS_SYNC(s_Cs);

    netAssert(this != m_Next);

    const netHttpRequest* rqst = s_NetHttpRequestQueue.m_Next;

    for(int i = 0; i < MAX_IN_FLIGHT_REQUESTS; ++i, rqst = rqst->m_Next)
    {
        if(this == rqst)
        {
            return true;
        }
    }

    return false;
}

void netHttpRequest::SetUrlEncode(bool bUrlEncode)
{
    m_bUrlEncode = bUrlEncode;
}

void
netHttpRequest::ClearCachedEncodedUri()
{
	if(m_EncodedUri)
	{
		this->Free(m_EncodedUri);
		m_EncodedUri = NULL;
	}
}

void
netHttpRequest::Enqueue()
{
	SYS_CS_SYNC(s_Cs);

    if(netVerifyf(this == m_Next, "Already linked"))
    {
        //Sanity check - make sure the queue length is zero when no
        //requests are queued.
        netAssert((&s_NetHttpRequestQueue == s_NetHttpRequestQueue.m_Next && 0 == s_NetHttpRequestQueueLength)
                || (&s_NetHttpRequestQueue != s_NetHttpRequestQueue.m_Next && s_NetHttpRequestQueueLength > 0));

        m_Prev = s_NetHttpRequestQueue.m_Prev;
        m_Next = &s_NetHttpRequestQueue;
        m_Prev->m_Next = m_Next->m_Prev = this;

        ++s_NetHttpRequestQueueLength;
    }
}

void
netHttpRequest::Dequeue()
{
	SYS_CS_SYNC(s_Cs);

    if(this != m_Next)
    {
        m_Next->m_Prev = m_Prev;
        m_Prev->m_Next = m_Next;
        m_Next = m_Prev = this;

        --s_NetHttpRequestQueueLength;

        netAssert(s_NetHttpRequestQueueLength >= 0);
    }
}

void
netHttpRequest::SetSslContext(WOLFSSL_CTX* sslCtx)
{
    m_SslCtx = sslCtx;
}

void
netHttpRequest::GetDebugInfo(DebugInfo& info)
{
	info.m_Committed = m_Committed;
	info.m_InContentBytesRcvd = m_InContentBytesRcvd;
	info.m_OutContentBytesSent = m_OutContentBytesSent;
	info.m_OutChunkBytesSent = m_OutChunksBytesSent;
	info.m_RecvState = m_RecvState;
	info.m_SendState = m_SendState;
	info.m_HttpStatusCode = m_HttpStatusCode;
	info.m_AbortReason = m_AbortReason;
	info.m_ChunkAllocationError = m_ChunkAllocationError;
	info.m_TcpResult = m_TcpOpResult;
	info.m_TcpError = m_TcpOpError;
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT

void netHttp::SetRequireHttps(const bool requireHttps)
{
    if(s_NetHttpRequireHttps != requireHttps)
    {
        netDebug("SetRequireHttps :: %s", LogBool(requireHttps));
        s_NetHttpRequireHttps = requireHttps;
    }
}

bool netHttp::AllowNonHttpsRequest()
{
    return !s_NetHttpRequireHttps || PARAM_nethttpallownonhttps.Get();
}

#if ENABLE_TCP_PROXY

#if ENABLE_TCP_PROXY_ARGS
netHttpProxy netHttpProxy::sm_proxies[MAX_PROXIES];
unsigned netHttpProxy::sm_numProxies = 0;
#endif

netHttpProxy netHttpProxy::sm_whitelist[MAX_WHITELISTS];
unsigned netHttpProxy::sm_numWhitelists = 0;

netHttpProxy::netHttpProxy()
{
	memset(m_urlWildcard, 0, sizeof(m_urlWildcard));
	memset(m_proxy, 0, sizeof(m_proxy));
}

bool netHttpProxy::GetProxyAddress(const char* url, const char*& addrStr)
{
#if RL_USE_FORCED_HTTP_PROXY || ENABLE_TCP_PROXY_ARGS
	sysUri uri;
	const char* host = url;
	const char* start = url ? strstr(url, "://") : nullptr;

	bool fail = false;

	if(start)
	{
		if(!sysUri::Parse(url, uri))
		{
			netWarning("GetProxyAddress -Failed to parse url [%s] for a proxy check", url);
			fail = true;
		}
		else
		{
			host = uri.m_Host;
		}
	}

	// The white list also wins when RL_USE_FORCED_HTTP_PROXY is used so we check this first
	if(!fail)
	{
		for(unsigned i = 0; i < sm_numWhitelists; ++i)
		{
			if(sm_whitelist[i].MatchesUrl(host))
			{
				netDebug3("GetProxyAddress - Whitelisted url [%s] won't be proxied. Match on [%s]", url, sm_whitelist[i].m_urlWildcard);
				return false;
			}
		}
	}

#if RL_USE_FORCED_HTTP_PROXY
	if(rlIsForcedEnvironment())
	{
		addrStr = RL_FORCED_PROXY_ADDRESS;
		return true;
	}
#endif

#if ENABLE_TCP_PROXY_ARGS
	// If args are enabled and we have a valid url, check if it's in our proxy list
	if(fail)
	{
		return false;
	}

	for(unsigned i = 0; i < sm_numProxies; ++i)
	{
		if(sm_proxies[i].MatchesUrl(host))
		{
			addrStr = sm_proxies[i].m_proxy;

			netDebug3("GetProxyAddress - Proxy for [%s] is [%s]. Match on [%s]", url, addrStr, sm_proxies[i].m_urlWildcard);
			return true;
		}
	}
#endif //ENABLE_TCP_PROXY_ARGS

	netDebug3("GetProxyAddress - No proxy found for [%s]", url);
#else //RL_USE_FORCED_HTTP_PROXY || ENABLE_TCP_PROXY_ARGS
	(void)url;
	(void)addrStr;
#endif

	return false;
}

bool netHttpProxy::GetProxyAddress(const char* url, netSocketAddress& addr)
{
	const char* addrStr = nullptr;

	if(GetProxyAddress(url, addrStr) && addrStr)
	{
		return addr.Init(addrStr);
	}

	return false;
}

bool netHttpProxy::Set(const char* urlWildcard, const char* proxy)
{
	rtry
	{
		rverify(urlWildcard != nullptr, catchall, netError("The wildcard is null"));
		rverify(proxy != nullptr, catchall, netError("The proxy is null"));

		safecpy(m_urlWildcard, urlWildcard);
		safecpy(m_proxy, proxy);

		return true;
	}
	rcatchall
	{

	}

	return false;
}

bool netHttpProxy::MatchesUrl(const char* url) const
{
	if(StringNullOrEmpty(url))
	{
		// An emtpy url means we look for the * wildcard.
		return m_urlWildcard[0] == '*' && m_urlWildcard[1] == 0;
	}

	return StringWildcardCompare(m_urlWildcard, url, true) == 0;
}

void netHttpProxy::AddProxyWhitelist(const char* urlWildcard)
{
	if(!netVerifyf(sm_numWhitelists < netHttpProxy::MAX_WHITELISTS, "Too many whitelists. Increase buffer"))
	{
		return;
	}

	if(sm_whitelist[sm_numWhitelists].Set(urlWildcard, ""))
	{
		netDebug1("netHttpProxy %u - Proxy whitelist added with wildcard[%s]", sm_numWhitelists, urlWildcard);
		++sm_numWhitelists;
	}
}

#if ENABLE_TCP_PROXY_ARGS
void netHttpProxy::AddProxyAddress(const char* urlWildcard, const char* proxyAddress)
{
	if(!netVerifyf(sm_numProxies < netHttpProxy::MAX_PROXIES, "Too many proxies. Increase buffer"))
	{
		return;
	}

	if(sm_proxies[sm_numProxies].Set(urlWildcard, proxyAddress))
	{
		netDisplay("netHttpProxy %u - Proxy[%s] added with wildcard[%s]", sm_numProxies, proxyAddress, urlWildcard);
		++sm_numProxies;
	}
}
#endif //ENABLE_TCP_PROXY_ARGS

void netHttpProxy::InitProxies()
{
#if ENABLE_TCP_PROXY_ARGS
	const char* list[netHttpProxy::MAX_PROXIES] = { 0 };

	char buffer[netHttpProxy::MAX_PROXIES * (netHttpProxy::MAX_HOST_URL_LEN + netSocketAddress::MAX_STRING_BUF_SIZE + 4)] = { 0 };

	const int numValues = PARAM_nethttpproxy.GetArray(list, netHttpProxy::MAX_PROXIES, buffer, static_cast<int>(sizeof(buffer)));

	for(int i = 0; i < numValues; ++i)
	{
		const char* val = list[i];
		const char* hasSplit = strchr(list[i], '=');

		if(hasSplit)
		{
			size_t pos = hasSplit - buffer;
			buffer[pos] = 0;
			AddProxyAddress(val, hasSplit + 1);
		}
		else
		{
			AddProxyAddress("*", val);
		}
	}
#endif //ENABLE_TCP_PROXY_ARGS
}
#endif //ENABLE_TCP_PROXY

} // namespace rage
