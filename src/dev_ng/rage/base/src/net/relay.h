// 
// net/relay.h
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_RELAY_H 
#define NET_RELAY_H 

#include "net.h"
#include "event.h"
#include "packet.h"
#include "atl/delegate.h"

struct WOLFSSL;

namespace rage
{

class netSocket;
class netSocketAddress;
class netAddress;
class rlRosEvent;
class netStatus;
class netSocketManager;

#if SUPPORT_DTLS
class netDtlsEndpoint;
#endif

enum netRelayEventType
{
    NET_RELAYEVENT_INVALID  = -1,
    NET_RELAYEVENT_ADDRESS_OBTAINED,
	NET_RELAYEVENT_ADDRESS_CHANGED,
	NET_RELAYEVENT_PACKET_RECEIVED,
    NET_RELAYEVENT_MESSAGE_RECEIVED,
	NET_RELAYEVENT_RELAY_PONG_RECEIVED,

    NET_RELAYEVENT_NUM_EVENTS
};

class netRelayEvent : public netEventBase
{
public:

    netRelayEvent(netRelayEventType eventType,
                    sysMemAllocator* allocator)
    : netEventBase(allocator)
    , m_EventType(eventType)
    {
    }

    netRelayEventType m_EventType;
};

class netRelayEventRelayPongReceived : public netRelayEvent
{
public:

    netRelayEventRelayPongReceived(const netAddress& addr,
                                sysMemAllocator* allocator)
        : netRelayEvent(NET_RELAYEVENT_RELAY_PONG_RECEIVED, allocator)
        , m_Address(addr)
    {
    }

    netAddress m_Address;
};

class netRelayEventAddressObtained : public netRelayEvent
{
public:
	netRelayEventAddressObtained()
		: netRelayEvent(NET_RELAYEVENT_ADDRESS_OBTAINED, NULL)
	{

	}

    netRelayEventAddressObtained(const netAddress& addr,
                                sysMemAllocator* allocator)
        : netRelayEvent(NET_RELAYEVENT_ADDRESS_OBTAINED, allocator)
        , m_Address(addr)
    {
    }

    netAddress m_Address;
};

class netRelayEventAddressChanged : public netRelayEvent
{
public:

	netRelayEventAddressChanged()
		: netRelayEvent(NET_RELAYEVENT_ADDRESS_CHANGED, NULL)
	{

	}

	netRelayEventAddressChanged(const netAddress& addr,
		const netAddress& prevAddr,
		sysMemAllocator* allocator)
		: netRelayEvent(NET_RELAYEVENT_ADDRESS_CHANGED, allocator)
		, m_Address(addr)
		, m_PrevAddress(prevAddr)
	{
	}

	netAddress m_Address;
	netAddress m_PrevAddress;
};

class netRelayEventPacketReceived : public netRelayEvent
{
public:

    netRelayEventPacketReceived(const netAddress& sender,
                                const void* payload,
                                const unsigned len,
                                sysMemAllocator* allocator)
        : netRelayEvent(NET_RELAYEVENT_PACKET_RECEIVED, allocator)
        , m_Sender(sender)
        , m_Payload(payload)
        , m_Length(len)
    {
    }

    netAddress m_Sender;
    const void* m_Payload;
    unsigned m_Length;
};

class netRelayEventMessageReceived : public netRelayEvent
{
public:

    netRelayEventMessageReceived(const char* message,
                                const unsigned len,
                                sysMemAllocator* allocator)
        : netRelayEvent(NET_RELAYEVENT_MESSAGE_RECEIVED, allocator)
        , m_Message(message)
        , m_Length(len)
    {
    }

    const char* m_Message;
    unsigned m_Length;
};

//PURPOSE
//  Communicates with the relay server to relay packets between peers.
//  Receives real-time messages and notification of events from ROS
//  systems.
class netRelay
{
    typedef atDelegator<void (const netRelayEvent& event)> Delegator;

public:

    typedef Delegator::Delegate Delegate;

    //PURPOSE
    //  Initializes netRelay
    //PARAMS
    //  allocator       - Used to allocate memory for storing packets
    //                    prior to dispatching them to interested delegates.
	//  socketManager	- a socket manager that will be used for p2p and relay/presence traffic.
    static bool Init(sysMemAllocator* allocator,
					netSocketManager* socketManager);

    //PURPOSE
    //  Shuts down netRelay
    static void Shutdown();

    //PURPOSE
    //  Returns true if Init() has been called successfully.
    static bool IsInitialized();

	// PURPOSE
	// Add Bank Widgets
	static void AddWidgets();

    //PURPOSE
    //  Starts communication with the relay server.
    //  This is called when starting a network game.
    //NOTES
    //  This doesn't start communication with the presence server.
    //  That starts immediately after calling Init().
    static bool StartRelay();

    //PURPOSE
    //  Stops communication with the relay server.
    //NOTES
    //  This doesn't stop communication with the presence server.
    //  That stops after calling Shutdown().
    static bool StopRelay();

    //PURPOSE
    //  Returns true when the relay server is running.
    static bool IsRelayRunning();

    //PURPOSE
    //  Registers a delegate (callback) to be called when events of type
    //  eventType are dispatched.
    //PARAMS
    //  eventType   - Event type to listen for.
    //  dlgt        - Callback to call when packets of type packetType
    //                are received.
    static bool AddDelegate(netRelayEventType eventType,
                            Delegate* dlgt);

    //PURPOSE
    //  Same as AddDelegate() except that the delegate is assumed to be thread
    //  safe and will likely be called from a background thread.
    static bool AddConcurrentDelegate(netRelayEventType eventType,
                                        Delegate* dlgt);

    //PURPOSE
    //  Unregisters a delegate that was registered with AddDelegate().
    static bool RemoveDelegate(netRelayEventType eventType,
                                Delegate* dlgt);

    //PURPOSE
    //  Unregisters a delegate that was registered with AddConcurrentDelegate().
    static bool RemoveConcurrentDelegate(netRelayEventType eventType,
                                        Delegate* dlgt);

    //PURPOSE
    //  Call at least once per frame to process relay packets.
    static void Update();

	//PURPOSE
	//  Returns the socket over which relay packets are send and received.
	static netSocket* GetSocket();

	//PURPOSE
	//  Returns the port the game requested for the socket. Note: this
	//  can be different than the port to which the socket was bound.
	static u16 GetRequestedPort();

    //PURPOSE
    //  Returns my address relative to the relay server.
    //  This is passed to other peers in case they need to use the
    //  relay to communicate with us.
    static const netAddress& GetMyRelayAddress();
	
	//PURPOSE
	//  Returns the local peer's relay token, returned in relay pongs.
	//  This is passed to other peers in case they need to use the
	//  relay to communicate with us.
	//NOTE
	//  This is an opaque token that our local relay server uses to map to
	//  our real address. The token is meaningless to the client and is
	//  only passed back to the relay server which knows how to convert it to
	//  (or look up) the real address. As an example, the token might be an
	//  encrypted version of our IP/port as seen by the relay server, or an
	//  ID used to look up the real address from state stored on the server.
	//  Either the real address (if known) or the token can be used to
	//  communicate with another peer via the relay server. Only the token is
	//  advertised to presence/matchmaking. This is to ensure players' real
	//  IPs/ports are not advertised when possible.
	static const netRelayToken& GetMyRelayToken();

    //PURPOSE
    //  Returns my address relative to the presence server, in host byte order.
    static const netSocketAddress& GetMyPresenceAddress();

    //PURPOSE
    //  Returns the address of the relay server.
    static const netSocketAddress& GetRelayServerAddress();

	//PURPOSE
	//  Returns the address of the presence server.
	static const netSocketAddress& GetPresenceServerAddress();
	
	//PURPOSE
	//  Returns true if we are using a secure protocol for relay connections.
	static bool IsUsingSecureRelays();

	//PURPOSE
	//  Returns true if we are using a secure protocol for presence connection.
	static bool IsUsingSecurePresence();

	//PURPOSE
	//  Returns true if we are connected to our relay server.
	static bool IsConnectedToRelay();

	//PURPOSE
	//  Returns true if we timed out waiting for a relay connection.
	static bool IsReadyForMultiplayerTimedOut();

	//PURPOSE
	//  Returns true if we are ready to enter multiplayer.
	static bool IsReadyForMultiplayer();

	//PURPOSE
	//  Returns the relay ping interval in milliseconds.
	//  This is not necessarily a constant.
	static unsigned GetRelayPingIntervalMs();

	//PURPOSE
	//  Returns the presence ping interval in milliseconds.
	//  This is not necessarily a constant.
	static unsigned GetPresencePingIntervalMs();

	//PURPOSE
	//  Returns the number of times our relay address has changed.
	static unsigned GetNumRelayAddrChanges();

	//PURPOSE
	//  Returns the number of times our relay mapped address has changed.
	static unsigned GetNumRelayMappedAddrChanges();

	//PURPOSE
	//  Returns true if we are connected to our presence server.
	// NOTE
	//  Not available on PC since presence goes through the RGSC DLL.
	static bool IsConnectedToPresence();
	static unsigned GetNumUnackedPresencePings();
	
	//PURPOSE
	//  Starts an async operation to connect to the specified relay server.
	//  Returns true if the async operation was started successfully.
	//PARAMS
	//  relayAddr	- The relay address to connect to.
	//  status		- Poll for completion.
	static bool Connect(const netSocketAddress& relayAddr, const bool isSecure, netStatus* status);
	
	//PURPOSE
	//  Cancels an in-progress Connect().
	static void CancelConnect(netStatus* status);

    //PURPOSE
    //  Sends a packet to a recipient via the relay server.
    //PARAMS
    //  addr    - Recipient address, by way of the relay server.
    //            addr.IsRelayServerAddr() must return true.
    static netSocketError Send(const netAddress& addr,
                                netRelayPacket* relayPkt,
                                const unsigned sizeofPayload);

    //PURPOSE
    //  Sends a ping to a specified relay server. You can
	//  receive the pong by listening for the
	//  NET_RELAYEVENT_RELAY_PONG_RECEIVED event.
    //PARAMS
    //  addr    - Address of the relay server to ping.
    static bool SendPing(const netSocketAddress& addr);

	//PURPOSE
	//  Returns the minimum supported port binding timeout.
	static unsigned GetMinSupportedPortBindingTimeoutSec();

	//PURPOSE
	//  Returns the time in ms since the last relay ping.
	static unsigned GetMsSinceLastRelayPing();

	//PURPOSE
	//  Returns the time in ms since the last relay pong.
	static unsigned GetMsSinceLastRelayPong();

	//PURPOSE
	//  Returns the time in ms since the last relay terminus.
	static unsigned GetMsSinceLastRelayTerminus();

	//PURPOSE
	//  Returns the time in ms since the last presence ping.
	static unsigned GetMsSinceLastPresencePing();

	//PURPOSE
	//  Forces the relay ping timer to retry immediately.
	static void ForceRefreshRelayMapping();

	//PURPOSE
	//  Receives a datagram from a socket.
	//  Returns true if the datagram was consumed (i.e. it was
	//  from a relay or presence server), or false if the datagram
	//  is not from a relay or presence server.
	static bool Accept(const netSocketAddress& from,
						void *buffer,
						const int bufferSize);

	//PURPOSE
	//  Increments the number of failed/succeeded p2p presence
	//  messages. This data gets sent to telemetry.
	static void IncP2pPresenceMessageStats(const bool succeeded);

	static void SetPingIntervalAdjustmentMs(const unsigned pingIntervalAdjustmentMs);

	//PURPOSE
	//  Sets the interval between relay pings in milliseconds.
	static void SetRelayPingIntervalMs(const unsigned relayPingIntervalMs);

	//PURPOSE
	//  Set how long to wait for a relay pong before resending the first unacked relay ping.
	//  Retries back off by multiplying this value by the number of consecutive unacked pings.
	static void SetRelayPingRetryMs(const unsigned relayPingRetryMs);

	//PURPOSE
	//  Set whether to allow entry into multiplayer when a relay server connection cannot be established.
	static void SetAllowMultiplayerWithoutRelayServer(const bool allowMultiplayerWithoutRelayServer);

	//PURPOSE
	//  Set the interval at which relay/presence telemetry is sent, in milliseconds
	static void SetRelayTelemetrySendIntervalMs(const unsigned relayTelemetrySendIntervalMs);

private:
#if SUPPORT_DTLS
	static bool InitSslCtx();
	static void FreeSslCtx();
	static void InitEndpoints();
	static void UpdateEndpoints();
	static netDtlsEndpoint* AllocateEndpoint(const netSocketAddress& addr);
	static void FreeEndpoint(netDtlsEndpoint* ep);
	static netDtlsEndpoint* GetEndpointByAddr(const netSocketAddress& addr);
	static netDtlsEndpoint* GetCreateEndpoint(const netSocketAddress& addr);
	static int SslReceiveCb(WOLFSSL* ssl, char* buffer, int bufferSize, void* ctx);
	static int SslSendCb(WOLFSSL* ssl, char* data, int length, void* ctx);
#endif

	static unsigned GetMaxPacketSize();

	static bool IsRelayAddress(const netSocketAddress& addr);
	static bool IsPresenceAddress(const netSocketAddress& addr);
	static bool IsRelayPong(const void* pktBuf, const int len);

	static bool SendData(const netSocketAddress& destination,
						 const bool isSecure,
						 const void *data,
						 const unsigned length,
						 netSocketError* sktErr = 0);

	static void OnReceive(const netSocketAddress& from,
						void* pktBuf,
						const int len);

    static bool FetchRelayServerAddr();

#if SC_PRESENCE
    static bool FetchPresenceServerAddr(char (&hostname)[MAX_HOSTNAME_BUF_SIZE], u16& port);
    static void UpdatePresenceServerDiscovery();
#endif

    static void UpdatePing();

	static unsigned GetPortBindingTimeoutSec();

	static sysIpcCurrentThreadId GetCurrentThreadId();

    static void HandleTerminus(netRelayPacket* relayPkt);

    static void HandleRelayPong(netRelayPacket* relayPkt);

    static void HandlePresencePong(netRelayPacket* relayPkt);

    static void HandleMessage(netRelayPacket* relayPkt);

	static void UpdateTelemetry();

    static netRelay::Delegator sm_Delegators[NET_RELAYEVENT_NUM_EVENTS];

    static netRelay::Delegator sm_ConcurrentDelegators[NET_RELAYEVENT_NUM_EVENTS];

#if RSG_BANK
	static bool sm_bIgnorePresencePongs;
	static bool sm_bIgnoreRelayPongs;
#endif
};

} // namespace rage

#endif // NET_RELAY_H 
