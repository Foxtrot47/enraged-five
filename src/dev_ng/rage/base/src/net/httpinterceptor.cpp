// 
// net/httpinterceptor.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "httpinterceptor.h"

#if ENABLE_HTTP_INTERCEPTOR

#include "diag/seh.h"
#include "parser/manager.h"
#include "rline/rldiag.h"
#include "string/stringutil.h"
#include "system/param.h"
#include "system/timer.h"

#if __BANK
#include "bank/bkmgr.h"
#include "bank/bank.h"
#endif

namespace rage
{

PARAM(nethttprules, "Enable http interception and specify rules xml file. -nethttprules=rules.xml");

///////////////////////////////////////////////////////////////////////////////
//  netHttpInterceptRule
///////////////////////////////////////////////////////////////////////////////
struct netHttpInterceptRule
{
	bool Init(const char* name,
			  const char* match,
			  u32 delayMs,
			  const char* responseFile, 
			  bool enabled)
	{
		safecpy(m_Name, name);
		safecpy(m_Match, match);
		safecpy(m_ResponseFile, responseFile);
		m_DelayMs = delayMs;
		m_Enabled = enabled;
		return true;
	}

	char m_Match[128];
	char m_ResponseFile[128];
	char m_Name[20];
	u32 m_DelayMs;
	bool m_Enabled;
	inlist_node<netHttpInterceptRule> m_ListLink;
};

typedef inlist<netHttpInterceptRule, &netHttpInterceptRule::m_ListLink> netHttpInterceptRules;
netHttpInterceptRules s_Rules;

///////////////////////////////////////////////////////////////////////////////
//  netHttpInterception
///////////////////////////////////////////////////////////////////////////////

bool 
netHttpInterception::EnableRuleByName(const char* name)
{
	bool rulefound = false;

	if (name)
	{
		netHttpInterceptRules::iterator it = s_Rules.begin();
		netHttpInterceptRules::iterator stop = s_Rules.end();
		for(; stop != it && !rulefound; ++it)
		{
			netHttpInterceptRule* rule = (*it);
			if (rule)
			{
				if (stricmp(name, rule->m_Name) == 0)
				{
					rulefound = true;
					rule->m_Enabled = true;
					break;
				}
			}
		}
	}

	return rulefound;
}

netHttpInterceptRule* 
netHttpInterception::MatchRule(const char* uri)
{
	netHttpInterceptRules::iterator it = s_Rules.begin();
	netHttpInterceptRules::iterator stop = s_Rules.end();
	for(; stop != it; ++it)
	{
		netHttpInterceptRule* rule = (*it);

		if(rule->m_Enabled)
		{
			// false means it's a match
			if(StringWildcardCompare((*it)->m_Match, uri, true) == false)
			{
				return *it;
			}
		}
	}

	return NULL;
}

netHttpFilter*
netHttpInterception::CreateInterceptor(netHttpRequest* request,
									   netHttpFilter* filter)
{
	// if the uri matches a rule, then create an interceptor
	netHttpInterceptRule* rule = MatchRule(request->GetUri());
	if(rule)
	{
		return rage_new netHttpInterceptor(rule, filter);
	}

	return filter;
}

void
netHttpInterception::DestroyInterceptor(netHttpInterceptor* interceptor)
{
	if(interceptor)
	{
		delete interceptor;
	}
}

bool 
netHttpInterception::Init()
{
	const char* filename = NULL;
	if(!PARAM_nethttprules.Get(filename))
	{
		return true;
	}

	/* Example rules file
		<?xml version="1.0" encoding="utf-8"?>
		<Rules>
			<Rule Name="Rule 1" Match="https://dev.cloud.rockstargames.com/" ResponseFile="404_NotFound.dat" EnabledAtStartup="false" />
			<Rule Name="Rule 2" Match="https://dev.ros.rockstargames.com/gta5/11/gameservices/auth.asmx/" ResponseFile"404_NotFound.dat" EnabledAtStartup="false" />
		</Rules>
	*/

	bool success = false;

	INIT_PARSER;

	parTree* tree = NULL;

	rtry
	{
		tree = PARSER.LoadTree(filename, "xml");

		rverify(tree && tree->GetRoot()
				,catchall
				,rlError("File '%s' is missing", filename));

		parTreeNode* pRoot = tree->GetRoot();
		rverify(pRoot, catchall, );

		parTreeNode* node = pRoot;

		for(unsigned i = 0; i < (unsigned)node->FindNumChildren(); i++)
		{
			parTreeNode* pNode = node->FindChildWithIndex(i);
			rverify(pNode, catchall, );

			if(stricmp(pNode->GetElement().GetName(), "Rule") != 0)
			{
				continue;
			}


			const parAttribute *attr = pNode->GetElement().FindAttributeAnyCase("Name");
			rverify(attr, catchall, );
			const char* name = attr->GetStringValue();
			rverify(name, catchall, );

			
			attr = pNode->GetElement().FindAttributeAnyCase("Match");
			rverify(attr, catchall, );
			const char* match = attr->GetStringValue();
			rverify(match, catchall, );

			attr = pNode->GetElement().FindAttributeAnyCase("ResponseFile");
			const char* responseFile = NULL;
			if(attr)
			{
				responseFile = attr->GetStringValue();
			}

			attr = pNode->GetElement().FindAttributeAnyCase("DelayMs");
			u32 delayMs = 0;
			if(attr)
			{
				const char* szDelayMs = attr->GetStringValue();
				rverify((szDelayMs != NULL) && (sscanf(szDelayMs, "%u", &delayMs) == 1), catchall, );
			}

			attr = pNode->GetElement().FindAttributeAnyCase("EnabledAtStartup");
			rverify(attr, catchall, );
			const char* enableAtStartup = attr->GetStringValue();
			rverify(enableAtStartup, catchall, );

			netHttpInterceptRule* rule = rage_new netHttpInterceptRule;
			rverify(rule, catchall, );

			bool valid = rule->Init(name, match, delayMs, responseFile, stricmp(enableAtStartup, "true") == 0);

			if(valid)
			{
				s_Rules.push_back(rule);
			}
			else
			{
				delete rule;
			}
		}

		AddWidgets();

		success = true;
	}
	rcatchall
	{
	}

	if(tree)
	{
		delete tree;
		tree = NULL;
	}

	SHUTDOWN_PARSER;

	return success;
}

void
netHttpInterception::AddWidgets()
{
#if __BANK
	if(!bkManager::IsEnabled())
	{
		return;
	}

	bkBank *pBank = BANKMGR.FindBank("Network");

	if(!pBank)
	{
		pBank = &BANKMGR.CreateBank("Network");
	}

	pBank->PushGroup("HTTP Interception", false);

	netHttpInterceptRules::iterator it = s_Rules.begin();
	netHttpInterceptRules::iterator stop = s_Rules.end();
	for(; stop != it; ++it)
	{
		netHttpInterceptRule* rule = (*it);

		pBank->AddToggle(rule->m_Name, &rule->m_Enabled, NullCallback, rule->m_Match);
	}

	pBank->PopGroup();
#endif
}

void
netHttpInterception::Shutdown()
{

}

///////////////////////////////////////////////////////////////////////////////
//  netHttpInterceptor
///////////////////////////////////////////////////////////////////////////////

netHttpInterceptor::netHttpInterceptor(netHttpInterceptRule* rule,
									   netHttpFilter* filter)
: m_Filter(filter)
, m_Rule(rule)
, m_DelayStartTime(0)
, m_Device(NULL)
, m_hFile(fiHandleInvalid)
{
	if(m_Rule->m_ResponseFile[0] != '\0')
	{
		m_Device = fiDevice::GetDevice(m_Rule->m_ResponseFile, true);
		if(m_Device)
		{
			m_hFile = m_Device->Open(m_Rule->m_ResponseFile, true);
		}
	}
}

netHttpInterceptor::~netHttpInterceptor()
{
	if(m_Device && fiIsValidHandle(m_hFile))
	{
		m_Device->Close(m_hFile);
		m_hFile = fiHandleInvalid;
		m_Device = NULL;
	}
}

bool 
netHttpInterceptor::CanFilterRequest() const
{
	if(m_Filter && (CanIntercept() == false))
	{
		return m_Filter->CanFilterRequest();
	}
	return false;
}

bool 
netHttpInterceptor::CanFilterResponse() const
{
	if(m_Filter && (CanIntercept() == false))
	{
		return m_Filter->CanFilterResponse();
	}
	return false;
}

u32 
netHttpInterceptor::DelayMs() const
{
	// the first time this is called, we start the timer.
	if(m_Rule && (m_Rule->m_DelayMs > 0))
	{
		if(m_DelayStartTime == 0)
		{
			m_DelayStartTime = sysTimer::GetSystemMsTime();
		}

		u32 elapsed = sysTimer::GetSystemMsTime() - m_DelayStartTime;
		if(elapsed < m_Rule->m_DelayMs)
		{
			return m_Rule->m_DelayMs - elapsed;
		}
	}

	return 0;
}

bool 
netHttpInterceptor::CanIntercept() const
{
	return (m_Rule != NULL) && (m_Rule->m_ResponseFile[0] != '\0');
}

bool 
netHttpInterceptor::FilterRequest(const u8* data,
								  const unsigned dataLen,
								  const bool finalCall,
								  datGrowBuffer& output)
{
	if(m_Filter && (CanIntercept() == false))
	{
		return m_Filter->FilterRequest(data,
									   dataLen,
									   finalCall,
									   output);
	}
	return false;
}

bool 
netHttpInterceptor::FilterResponse(const u8* data,
								   const unsigned dataLen,
								   const bool allDataReceived,
								   fiHandle& outputHandle,
								   const fiDevice* outputDevice,
								   unsigned* numProcessed,
								   bool* hasDataPending)
{
	if(m_Filter && (CanIntercept() == false))
	{
		return m_Filter->FilterResponse(data,
										dataLen,
										allDataReceived,
										outputHandle,
										outputDevice,
										numProcessed,
										hasDataPending);
	}
	return false;
}

const char* 
netHttpInterceptor::GetUserAgentString() const
{
	if(m_Filter && (CanIntercept() == false))
	{
		return m_Filter->GetUserAgentString();
	}
	return NULL;
}

bool 
netHttpInterceptor::ProcessRequestHeader(netHttpRequest* request)
{
    if(m_Filter && (CanIntercept() == false))
	{
		return m_Filter->ProcessRequestHeader(request);
	}
    return true;
}

bool 
netHttpInterceptor::ProcessResponseHeader(const int statusCode,
										  const char* header)
{
	if(m_Filter && (CanIntercept() == false))
	{
		return m_Filter->ProcessResponseHeader(statusCode, header);
	}
    return true;
}

bool
netHttpInterceptor::AllowSucceeded(class netHttpRequest* request)
{
    if(m_Filter && (CanIntercept() == false))
	{
		return m_Filter->AllowSucceeded(request);
	}
    return true;
}

void
netHttpInterceptor::DisableHttpDump(bool bDisabled)
{
	if (m_Filter)
	{
		m_Filter->DisableHttpDump(bDisabled);
	}
}

void 
netHttpInterceptor::Receive(void* buf,
							const unsigned bufSize,
							unsigned* bytesReceived)
{
	*bytesReceived = 0;

	rtry
	{
		rverify(m_Device && fiIsValidHandle(m_hFile)
			,catchall
			,rlError("Http Interceptor Failed to open the response file '%s'.", m_Rule ? m_Rule->m_ResponseFile : "null rule response file."));

		*bytesReceived = m_Device->Read(m_hFile, buf, bufSize);	
	}
	rcatchall
	{

	}
}

} // namespace rage

#endif // ENABLE_HTTP_INTERCEPTOR
