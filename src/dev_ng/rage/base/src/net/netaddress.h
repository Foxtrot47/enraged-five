// 
// net/netaddress.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_NETADDRESS_H
#define NET_NETADDRESS_H

#include "system/endian.h"
#include "data/bitbuffer.h"
#include "net.h"
#include "netpeerid.h"
#include "netrelaytoken.h"

#define IPV6_ADDRESSES (IPv4_IPv6_DUAL_STACK)

struct sockaddr;
struct in_addr;
struct in6_addr;

namespace rage {

	class RsonWriter;

#if !__NO_OUTPUT
//Formats netIpAddress for printf-like functions. Use as follows:
//printf("This is a netIpAddress " NET_IP_FMT, NET_IP_FOR_PRINTF(ip));
// Note: use netIpAddress::Format() in all other cases. 
#define NET_IP_FMT "%s"
#define NET_IP_FOR_PRINTF(addr) (addr).ToString()

//Formats a netAddress for printf.  Use as follows:
//printf("This is a netAddress " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(address));
// Note: use netAddress::Format() or netSocketAddress::Format() in all other cases.
#define NET_ADDR_FMT "%s"
#define NET_ADDR_FOR_PRINTF(addr) (addr).ToString()
#endif

//Functions to transform between network and host byte order.
//Network byte order is big endian.
inline u32 net_htonl(const u32 u) { return sysEndian::NtoB(u); }
inline u32 net_ntohl(const u32 u) { return sysEndian::BtoN(u); }
inline u16 net_htons(const u16 u) { return sysEndian::NtoB(u); }
inline u16 net_ntohs(const u16 u) { return sysEndian::BtoN(u); }

// implements an IPv4 address
class netIpV4Address
{
public:
	// max length of a string representation of an IPv4 address, including null-terminator
	static const unsigned MAX_STRING_BUF_SIZE = 16;

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = sizeof(u32);	// m_Ip

	//PURPOSE
	// Default constructor.
	netIpV4Address();

	//PURPOSE
	//  Construct an address from an unsigned int in host byte order.
	explicit netIpV4Address(u32 ip);

	//PURPOSE
	//  Construct a netIpV4Address from a sockaddr*.
	//NOTES
	//  saddr->sa_family must be AF_INET.
	explicit netIpV4Address(const sockaddr* saddr);

	//PURPOSE
	//  Construct a netIpV4Address from a in_addr.
	explicit netIpV4Address(const in_addr* addr4);

	//PURPOSE
	//  Copy constructor.
	netIpV4Address(const netIpV4Address& other);

	//PURPOSE
	//  Assign from another netIpV4Address.
	netIpV4Address& operator=(const netIpV4Address& other);

	//PURPOSE
	//  Initialize a netIpV4Address from an unsigned int in host byte order.
	bool Init(u32 ip);

	//PURPOSE
	//  Initialize a netIpV4Address from a sockaddr*.
	//NOTES
	//  saddr->sa_family must be AF_INET.
	bool Init(const sockaddr* saddr);

	//PURPOSE
	//  Initialize a netIpV4Address from a in_addr.
	bool Init(const in_addr* addr4);

	//PURPOSE
	//  Clears the IP to an invalid IP.
	void Clear();

	//PURPOSE
	//  Returns true if this netIpV4Address represents a valid IPv4 address.
	bool IsValid() const;

	//PURPOSE
	//  Returns the IP as an unsigned 32-bit value in host byte order.
	u32 ToU32() const;

	//PURPOSE
	//  Returns the IP as an unsigned 32-bit value in network byte order (always big endian).
	u32 ToU32Nbo() const;

	//PURPOSE
	//  Returns the IP as an unsigned 32-bit value in litte-endian byte order.
	u32 ToU32Le() const;

	//PURPOSE
	//  Returns true if this IP is an RFC1918 IP (private address space).
	bool IsRfc1918() const;

	//PURPOSE
	//  Obtain an address object that represents the loopback address.
	static netIpV4Address GetLoopbackAddress();

	//PURPOSE
	//  Obtain an address object that represents the broadcast address.
	static netIpV4Address GetBroadcastAddress();

	//PURPOSE
	//  Compare addresses.
	bool operator==(const netIpV4Address& rhs) const;
	bool operator!=(const netIpV4Address& rhs) const;
	bool operator<(const netIpV4Address& rhs) const;
	bool operator>(const netIpV4Address& rhs) const;
	bool operator<=(const netIpV4Address& rhs) const;
	bool operator>=(const netIpV4Address& rhs) const;
	
    //PURPOSE
    //  Formats the address into a string representation.
    //PARAMS
    //  dest        - Destination char buffer.
    //  sizeofDest  - Size in bytes of dest buffer. Must be at least MAX_STRING_BUF_SIZE.
    //NOTES
    //  Formats the string like so: "%u.%u.%u.%u"
    const char* Format(char* dest,
					   const unsigned sizeofDest) const;
    template<int SIZE>
    const char* Format(char (&dest)[SIZE]) const
    {
		CompileTimeAssert(SIZE >= MAX_STRING_BUF_SIZE);
        return Format(dest, SIZE);
    }

	//PURPOSE
	//  Initializes an address from a string representation in dotted-decimal format.
	bool FromString(const char* addr, unsigned* size = NULL);

#if !__NO_OUTPUT
	//PURPOSE
	//  Convenience function to return a string representation of the IP address
	//  without having to supply a buffer. Used in calls to printf()-like functions.
	//  Use Format() and supply a buffer in all other cases.
	const char* ToString() const;
#endif

	//PURPOSE
    //  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
	//PARAMS
	//  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

	unsigned GetOctet(const int i) const;

private:
	static const u32 NET_INADDR_ANY			= 0x00000000;
	static const u32 NET_INADDR_LOOPBACK	= 0x7F000001;
	static const u32 NET_INADDR_NONE		= 0xFFFFFFFF;
	static const u32 NET_INADDR_BROADCAST	= 0xFFFFFFFF;

	u32 m_Ip;

#if !__NO_OUTPUT
	mutable char m_Str[MAX_STRING_BUF_SIZE];
#endif
};

#if IPV6_ADDRESSES
// implements an IPv6 address
class netIpV6Address
{
public:
	// max length of a string representation of an IPv6 address, including null-terminator
	// IPv6 addresses can be 39 chars, eg. fe80:38bd:c1e7:9ff0:90c0:758f:c083:3c27
	// IPv4-mapped IPv6 addresses can be 45 chars, eg. 0000:0000:0000:0000:0000:FFFF:123.123.123.123
	// the literal IP is surrounded by square brackets (a standard representation to support the inclusion of a port number after the IP)
	static const unsigned MAX_STRING_BUF_SIZE = 45 + 2 + 1; // + 2 for the surrounding square brackets, + 1 for null terminator
	static const unsigned SIZE_OF_IP_IN_BYTES = 16; // 128-bit IP
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = SIZE_OF_IP_IN_BYTES;

	//PURPOSE
	//  Default constructor.
	netIpV6Address();

	//PURPOSE
	//  Construct a netIpV6Address from an array of bytes.
	explicit netIpV6Address(const u8 (&ip)[SIZE_OF_IP_IN_BYTES]);

	//PURPOSE
	//  Construct a netIpV6Address from a sockaddr*.
	//NOTES
	//  saddr->sa_family must be AF_INET or AF_INET6.
	explicit netIpV6Address(const sockaddr* saddr);

	//PURPOSE
	//  Construct a netIpV6Address from a in6_addr.
	explicit netIpV6Address(const in6_addr* addr6);

	//PURPOSE
	//  Copy constructor.
	netIpV6Address(const netIpV6Address& other);

	//PURPOSE
	//  Construct a netIpV6Address from a netIpV4Address.
	netIpV6Address(const netIpV4Address& other);

	//PURPOSE
	//  Assign from another netIpV6Address.
	netIpV6Address& operator=(const netIpV6Address& other);

	//PURPOSE
	//  Assign from an netIpV4Address.
	netIpV6Address& operator=(const netIpV4Address& other);

	//PURPOSE
	//  Init a netIpV6Address from an array of bytes.
	bool Init(const u8(&ip)[SIZE_OF_IP_IN_BYTES]);

	//PURPOSE
	//  Construct a netIpV6Address from a sockaddr*.
	//NOTES
	//  saddr->sa_family must be AF_INET or AF_INET6.
	bool Init(const sockaddr* saddr);

	//PURPOSE
	//  Initialize an netIpV6Address from an in6_addr.
	bool Init(const in6_addr* addr6);

	//PURPOSE
	//  Initialize an netIpV6Address from a netIpV4Address.
	bool Init(const netIpV4Address& ipV4Address);

	//PURPOSE
	//  Clears the IP to an invalid IP.
	void Clear();

	//PURPOSE
	//  Returns true if this netIpV6Address represents a valid IPv6 address.
	bool IsValid() const;

	//PURPOSE
	//  Returns true if this netIpV6Address contains an IPv4-mapped IPv6 address
	//  Note: invalid addresses will always return false.
	bool IsIpV4Mapped() const;

	//PURPOSE
	//  Returns true if this netIpV6Address is using Teredo protocol
	//  Note: invalid addresses will always return false.
	bool IsTeredo() const;

	//PURPOSE
	//  Returns true if this IPv6 can be converted to an IPv4 address.
	bool IsV4() const;

	//PURPOSE
	//  If this is an IPv6 address than can be converted to an IPv4 address,
	//  returns the converted IPv4 address. Only valid when IsV4() is true.
	netIpV4Address ToV4() const;

	//PURPOSE
	//  Returns the raw IP as an array of bytes.
	const u8 (&ToBytes() const)[SIZE_OF_IP_IN_BYTES];

	//PURPOSE
    //  Formats the address into a string representation.
    //PARAMS
    //  dest        - Destination char buffer.
    //  sizeofDest  - Size in bytes of dest buffer. Must be at least MAX_STRING_BUF_SIZE.
    //NOTES
    //  Example string representation: "fe80:38bd:c1e7:9ff0:90c0:758f:c083:3c27"
    const char* Format(char* dest,
					   const unsigned sizeofDest) const;
    template<int SIZE>
    const char* Format(char (&dest)[SIZE]) const
    {
		CompileTimeAssert(SIZE >= MAX_STRING_BUF_SIZE);
        return Format(dest, SIZE);
    }

#if !__NO_OUTPUT
	//PURPOSE
	//  Formats the address into a string representation, using IPv4 representations when possible.
	//PARAMS
	//  dest        - Destination char buffer.
	//  sizeofDest  - Size in bytes of dest buffer.
	//  includePort - True to include the port in resulting string.
	const char* FormatAttemptIpV4(char* dest, const unsigned sizeofDest) const;
	template<int SIZE>
	const char* FormatAttemptIpV4(char(&dest)[SIZE]) const
	{
		CompileTimeAssert(SIZE >= MAX_STRING_BUF_SIZE);
		return FormatAttemptIpV4(dest, SIZE);
	}
#endif

	//PURPOSE
	//  Initializes an address from a string representation in Internet Standard Format.
	//  Example: [fe80:38bd:c1e7:9ff0:90c0:758f:c083:3c27]
	bool FromString(const char* addr, unsigned* size = NULL);

#if !__NO_OUTPUT
	//PURPOSE
	//  Convenience function to return a string representation of the IP address
	//  without having to supply a buffer. Used in calls to printf()-like functions.
	//  Use Format() and supply a buffer in all other cases.
	const char* ToString() const;
#endif

	//PURPOSE
    //  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
	//PARAMS
	//  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

	// compare two addresses
	bool operator==(const netIpV6Address& rhs) const;
	bool operator!=(const netIpV6Address& rhs) const;
	bool operator<(const netIpV6Address& rhs) const;
	bool operator>(const netIpV6Address& rhs) const;
	bool operator<=(const netIpV6Address& rhs) const;
	bool operator>=(const netIpV6Address& rhs) const;
	
private:
	u8 m_Ip[SIZE_OF_IP_IN_BYTES];

#if !__NO_OUTPUT
	mutable char m_Str[MAX_STRING_BUF_SIZE];
#endif
};
#endif

// implements a version-independent IP address.
class netIpAddress
{
public:
	// max length of a string representation of a netIpAddress, including null-terminator
#if IPV6_ADDRESSES
	static const unsigned MAX_STRING_BUF_SIZE = netIpV6Address::MAX_STRING_BUF_SIZE;
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((NET_CROSS_PLATFORM_MP) ? 1 : 0) + // flag that indicates whether we exported from an IPv6 platform
													   netIpV6Address::MAX_EXPORTED_SIZE_IN_BYTES;
#else
	static const unsigned MAX_STRING_BUF_SIZE = netIpV4Address::MAX_STRING_BUF_SIZE;
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = ((NET_CROSS_PLATFORM_MP) ? 1 : 0) + // flag that indicates whether we exported from an IPv6 platform
													   netIpV4Address::MAX_EXPORTED_SIZE_IN_BYTES;
#endif

	//PURPOSE
	//  Default constructor.
	netIpAddress();

	//PURPOSE
	//  Construct a netIpAddress from an IPv4 netIpAddress.
	explicit netIpAddress(const netIpV4Address& ipv4_address);

#if IPV6_ADDRESSES
	//PURPOSE
	//  Construct a netIpAddress from an IPv6 netIpAddress.
	explicit netIpAddress(const netIpV6Address& ipv6_address);
#endif

	//PURPOSE
	//  Construct a netIpAddress from a sockaddr*.
	//NOTES
	//  saddr->sa_family must be AF_INET or AF_INET6.
	explicit netIpAddress(const sockaddr* saddr);

	//PURPOSE
	//  Copy constructor.
	netIpAddress(const netIpAddress& other);

	//PURPOSE
	//  Assign from another netIpAddress.
	netIpAddress& operator=(const netIpAddress& other);

	//PURPOSE
	//  Assign from an IPv4 netIpAddress.
	netIpAddress& operator=(const netIpV4Address& ipv4_address);

#if IPV6_ADDRESSES
	//PURPOSE
	//  Assign from an IPv6 netIpAddress.
	netIpAddress& operator=(const netIpV6Address& ipv6_address);
#endif

	//PURPOSE
	//  Compare addresses.
	bool operator==(const netIpAddress& rhs) const;
	bool operator!=(const netIpAddress& rhs) const;
	bool operator<(const netIpAddress& rhs) const;

	//PURPOSE
	//  Initialize a netIpAddress from a sockaddr*.
	//NOTES
	//  saddr->sa_family must be AF_INET or AF_INET6.
	bool Init(const sockaddr* saddr);

	//PURPOSE
	//  Clears the IP to an invalid IP.
	void Clear();

	//PURPOSE
	//  Returns true if this netIpAddress represents a valid IP address.
	bool IsValid() const;

	//PURPOSE
	//  Returns true if this is an IPv4 address or if it is an IPv6 
	//  address that can be converted to an IPv4 address with ToV4().
	bool IsV4() const;

	//PURPOSE
	//  Get the underlying netIpV4Address, or if this is an IPv6
	//  address than can be converted to an IPv4 address, returns
	//  the converted IPv4 address. Only valid when IsV4() is true.
	netIpV4Address ToV4() const;

#if IPV6_ADDRESSES
	//PURPOSE
	//  Get the underlying netIpV6Address.
	netIpV6Address ToV6() const;
#endif

	//PURPOSE
    //  Formats the address into a string representation.
    //PARAMS
    //  dest        - Destination char buffer.
    //  sizeofDest  - Size in bytes of dest buffer. Must be at least MAX_STRING_BUF_SIZE.
    //NOTES
    //  Formats the string using the Format() function of the underlying IP address class.
    const char* Format(char* dest,
					   const unsigned sizeofDest) const;
    template<int SIZE>
    const char* Format(char (&dest)[SIZE]) const
    {
		CompileTimeAssert(SIZE >= MAX_STRING_BUF_SIZE);
        return Format(dest, SIZE);
    }

	//PURPOSE
	//  Formats the address into a string representation, using IPv4 representations when possible.
	//PARAMS
	//  dest        - Destination char buffer.
	//  sizeofDest  - Size in bytes of dest buffer.
	//  includePort - True to include the port in resulting string.
	const char* FormatAttemptIpV4(char* dest, const unsigned sizeofDest) const;
	template<int SIZE>
	const char* FormatAttemptIpV4(char(&dest)[SIZE]) const
	{
		CompileTimeAssert(SIZE >= MAX_STRING_BUF_SIZE);
		return FormatAttemptIpV4(dest, SIZE);
	}

	//PURPOSE
	//  Initializes an address from a string representation.
	bool FromString(const char* addr, unsigned* size = NULL);

#if !__NO_OUTPUT
	//PURPOSE
	//  Convenience function to return a string representation of the IP address
	//  without having to supply a buffer. Used in calls to printf()-like functions.
	//  Use Format() and supply a buffer in all other cases.
	const char* ToString() const;
#endif

	//PURPOSE
	//  Writes the IP address to an RSON writer (used for telemetry).
	//  An IPv4 address is written as a 32-bit int in little-endian order.
	//  An IPv6 address is written as 0 for now (will need updating when we support IPv6)
	bool WriteRson(const char* name, RsonWriter* rw) const;

	//PURPOSE
    //  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
	//PARAMS
	//  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

private:

#if IPV6_ADDRESSES
	netIpV6Address m_IpAddress;
#else
	netIpV4Address m_IpAddress;
#endif
};

// implements a version-independent socket address - the combination of an IP address and a port number
class netSocketAddress
{
public:
	// max length of a string representation of a netSocketAddress without port, including null-terminator
	static const unsigned MAX_STRING_BUF_SIZE_WITHOUT_PORT = netIpAddress::MAX_STRING_BUF_SIZE;

	// max length of a string representation of a netSocketAddress including port and null-terminator
	static const unsigned MAX_STRING_BUF_SIZE = MAX_STRING_BUF_SIZE_WITHOUT_PORT + 6; // + 6 for colon and port (eg. ":54321")

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = netIpAddress::MAX_EXPORTED_SIZE_IN_BYTES +	// m_Ip
													   sizeof(u16);									// m_Port

    static const netSocketAddress INVALID_ADDRESS;

    netSocketAddress();

    netSocketAddress(const netIpAddress& ip,
					 const unsigned short port);

    //PURPOSE
    //  Constructs an address from a string in the form x.x.x.x:port for IPv4 or [ip]:port for IPv6
	explicit netSocketAddress(const char* addr);

	//PURPOSE
	//  Construct a netSocketAddress from a sockaddr*.
	//NOTES
	//  saddr->sa_family must be AF_INET or AF_INET6.
	netSocketAddress(const sockaddr* saddr);

    //PURPOSE
    //  Initializes the address.
    //PARAMS
    //  ip      - ip address.
    //  port    - Port number (between 0 and 0xFFFF).
    //RETURNS
    //  True on success.
    bool Init(const netIpAddress& ip,
			  const unsigned short port);

    //PURPOSE
    //  Initializes an address from a string in the form x.x.x.x:port for IPv4 or [ip]:port for IPv6
    bool Init(const char* addr);

	//PURPOSE
	//  Initialize a netSocketAddress from a sockaddr*.
	//NOTES
	//  saddr->sa_family must be AF_INET or AF_INET6.
	bool Init(const sockaddr* saddr);

    //PURPOSE
    //  Clears the IP to an invalid IP and sets the port to 0.
    void Clear();

    //PURPOSE
    //  Returns the ip address.
    netIpAddress GetIp() const;

    //PURPOSE
    //  Returns the address's port (between 0 and 65535)
    unsigned short GetPort() const;

    //PURPOSE
    //  Returns true if the address represents a valid socket address.
    bool IsValid() const;

	//PURPOSE
	//  Fills in a sockaddr structure with the IP and port of this netSocketAddress.
	//PARAMS
	//  dest        - Destination sockaddr struct to be filled out.
	//				  Must be a sockaddr_in6, or sockaddr_storage when IPV6_ADDRESSES
	//				  is enabled. Must be a sockaddr_in, or sockaddr_storage when 
	//				  IPV6_ADDRESSES is not enabled.
	//				  sockaddr_storage is recommended on platforms that support it,
	//				  since it is address-family agnostic and is large enough to hold
	//				  an IPv4 or IPv6 address.
	//  sizeofDest  - Size in bytes of dest buffer.
	bool ToSockAddr(sockaddr* dest, const unsigned sizeofDest) const;

    //PURPOSE
    //  Formats the address into a string representation.
    //PARAMS
    //  dest        - Destination char buffer.
    //  sizeofDest  - Size in bytes of dest buffer.
    //  includePort - True to include the port in resulting string.
    //NOTES
    //  for IPv4, this formats the string like so: "%u.%u.%u.%u"
    //  If includePort is true the format is: "%u.%u.%u.%u:%u"
	//  for IPv6, this formats the string in Internet Standard Format, with square brackets.
	//  Example: [fe80:38bd:c1e7:9ff0:90c0:758f:c083:3c27]
    const char* Format(char* dest,
                        const unsigned sizeofDest,
                        const bool includePort) const;
    template<int SIZE>
    const char* Format(char (&dest)[SIZE],
                        const bool includePort) const
    {
		// if includePort is false, we really only need SIZE >= MAX_STRING_BUF_SIZE_WITHOUT_PORT
		// but I want to catch any potential errors at compile-time
		CompileTimeAssert(SIZE >= MAX_STRING_BUF_SIZE);
        return Format(dest, SIZE, includePort);
    }

    const char* Format(char* dest,
                        const unsigned sizeofDest) const
    {
        return Format(dest, sizeofDest, true);
    }
    template<int SIZE>
    const char* Format(char (&dest)[SIZE]) const
    {
        return Format(dest, true);
    }

	//PURPOSE
	//  Formats the address into a IpV4 string representation if the underlying address is v4 or v4 mapped or Teredo - v6 otherwise
	//PARAMS
	//  dest        - Destination char buffer.
	//  sizeofDest  - Size in bytes of dest buffer.
	//  includePort - True to include the port in resulting string.
	const char* FormatAttemptIpV4(char* dest, const unsigned sizeofDest, const bool includePort) const;
    template<int SIZE>
    const char* FormatAttemptIpV4(char (&dest)[SIZE],
									const bool includePort) const
    {
		// if includePort is false, we really only need SIZE >= MAX_STRING_BUF_SIZE_WITHOUT_PORT
		// but I want to catch any potential errors at compile-time
		CompileTimeAssert(SIZE >= MAX_STRING_BUF_SIZE);
        return FormatAttemptIpV4(dest, SIZE, includePort);
    }

    //PURPOSE
    //  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
	//PARAMS
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    //PURPOSE
    //  Used to sort instances of netSocketAddress and/or to use netSocketAddress
    //  as the key in a hash table or binary tree.
    bool operator<(const netSocketAddress& rhs) const;

    bool operator==(const netSocketAddress& rhs) const;
    bool operator!=(const netSocketAddress& rhs) const;

#if !__NO_OUTPUT
	//PURPOSE
	//  Convenience function to return a string representation of the socket address
	//  without having to supply a buffer. Used in calls to printf()-like functions.
	//  Use Format() and supply a buffer in all other cases.
    const char* ToString() const;
#endif

private:
	netIpAddress m_Ip;
	u16 m_Port;

#if !__NO_OUTPUT
    mutable char m_Str[MAX_STRING_BUF_SIZE];
#endif
};

//PURPOSE
//  Represents a peer's address relative to a relay server or a peer relay
class netAddress
{
public:

	// max length of a string representation of a netAddress, including null-terminator
	// TODO: NS - add const for each type and use compile-time MAX for both string size and exported size
	//  - using simpler version for the ETU 
	static const unsigned MAX_STRING_BUF_SIZE = (((netSocketAddress::MAX_STRING_BUF_SIZE - 1) * 2) +
													netRelayToken::TO_HEX_STRING_BUFFER_SIZE) + 2 + 1; // + 2 for the "/" separators, + 1 for null-terminator

	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = sizeof(u8) +											// m_Type
													   netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES +	// m_ProxyAddr
													   netSocketAddress::MAX_EXPORTED_SIZE_IN_BYTES +	// m_TargetAddr
													   netRelayToken::MAX_EXPORTED_SIZE_IN_BYTES;		// m_RelayToken

    static const netAddress INVALID_ADDRESS;

	enum Type : u8
	{
		TYPE_INVALID,
		TYPE_DIRECT,
		TYPE_RELAY_SERVER,
		TYPE_PEER_RELAY,

		NUM_TYPES
	};

    netAddress();
	netAddress(const netAddress & rhs);

    netAddress(const netIpAddress& targetIp,
			   const unsigned short targetPort);

    explicit netAddress(const netSocketAddress& targetAddr);

    netAddress(const netSocketAddress& proxyAddr,
			   const netRelayToken& relayToken,
			   const netSocketAddress& targetAddr);

	netAddress(const netSocketAddress& proxyAddr,
			   const netPeerId& targetPeerId);

    bool Init(const netIpAddress& targetIp,
			  const unsigned short targetPort);

    bool Init(const netSocketAddress& targetAddr);

    bool Init(const netSocketAddress& proxyAddr,
			  const netRelayToken& relayToken,
			  const netSocketAddress& targetAddr);

    bool Init(const netSocketAddress& proxyAddr,
			  const netPeerId& targetPeerId);

    void Clear();

    //PURPOSE
    //  Returns the relay server or next hop peer relay address.
    const netSocketAddress& GetProxyAddress() const;

    //PURPOSE
    //  Returns the peer's address relative to the relay server.
	//  Only applicable if type != PEER_RELAY.
    const netSocketAddress& GetTargetAddress() const;

	//PURPOSE
	//  Returns the relay token.
	const netRelayToken& GetRelayToken() const;

	//PURPOSE
	//  Returns the netPeerId of the target.
	//  Only applicable if type == PEER_RELAY.
	const netPeerId& GetTargetPeerId() const;

    //PURPOSE
    //  Returns true if this is a valid address.
    bool IsValid() const;

	//PURPOSE
	//  Returns true if this is a direct address.
	bool IsDirectAddr() const;

	//PURPOSE
	//  Returns true if this is a peer relay address.
	bool IsPeerRelayAddr() const;

	//PURPOSE
    //  Returns true if this is a relay server address.
    bool IsRelayServerAddr() const;

	//PURPOSE
	//  Returns true if this address is equal to addr, or they both address the same relay server target.
	//  For example, a "plaintext" relay address (e.g. 192.81.241.233:61456/139.138.232.5:5555) or a
	//  "tokenized" relay address (e.g. 192.81.241.233:61456/0xb3f59ed79ad6) is equivalent to its combined
	//  "complete" relay address (e.g. 192.81.241.233:61456/0xb3f59ed79ad6/139.138.232.5:5555). All three
	//  of these example addresses address the same peer.
	bool IsEquivalent(const netAddress& addr) const;

	//PURPOSE
	//  Returns the type of this address.
	Type GetType() const {return m_Type;}

	//PURPOSE
	//  Formats the address into a string representation.
	//PARAMS
	//  dest        - Destination char buffer.
	//  sizeofDest  - Size in bytes of dest buffer.
	const char* Format(char* dest, const unsigned sizeofDest) const;

	template<int SIZE>
	const char* Format(char(&dest)[SIZE]) const
	{
		CompileTimeAssert(SIZE >= MAX_STRING_BUF_SIZE);
		return Format(dest, SIZE);
	}

#if !__NO_OUTPUT
	//PURPOSE
	//  Formats the address into a string representation, using IPv4 representations when possible.
	//PARAMS
	//  dest        - Destination char buffer.
	//  sizeofDest  - Size in bytes of dest buffer.
	//  includePort - True to include the port in resulting string.
	const char* FormatAttemptIpV4(char* dest, const unsigned sizeofDest) const;
	template<int SIZE>
	const char* FormatAttemptIpV4(char(&dest)[SIZE]) const
	{
		CompileTimeAssert(SIZE >= MAX_STRING_BUF_SIZE);
		return FormatAttemptIpV4(dest, SIZE);
	}
#endif

    //PURPOSE
    //  Serializes
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Deserializes
	//PARAMS
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    //PURPOSE
    //  Used to sort instances of netAddress and/or to use netAddress
    //  as the key in a hash table or binary tree.
    bool operator<(const netAddress& rhs) const;

    bool operator==(const netAddress& rhs) const;
    bool operator!=(const netAddress& rhs) const;	

	netAddress& operator=(const netAddress& other);

#if !__NO_OUTPUT
	//PURPOSE
	//  Convenience function to return a string representation of the netAddress
	//  without having to supply a buffer. Used in calls to printf()-like functions.
    const char* ToString() const;
	const char* GetTypeString() const;
#endif

private:
    netSocketAddress m_ProxyAddr;
	netSocketAddress m_TargetAddr;
	netPeerId m_TargetPeerId;
	netRelayToken m_RelayToken;

	Type m_Type;

	NET_ASSERTS_ONLY(bool SanityCheck() const;)
	OUTPUT_ONLY(mutable char m_Str[MAX_STRING_BUF_SIZE];)
};

}   // namespace rage

#endif  //NET_NETADDRESS_H
