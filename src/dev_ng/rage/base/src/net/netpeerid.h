// 
// net/netpeerid.h 
// 
// Copyright (C) 1999-2017 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_PEERID_H
#define NET_PEERID_H

#include "system/criticalsection.h"

namespace rage
{

class netPeerId
{
public:

	static const u64 NET_INVALID_PEER_ID = ~u64(0);
	static const unsigned MAX_EXPORTED_SIZE_IN_BYTES = sizeof(u64);
	static const unsigned TO_HEX_STRING_BUFFER_SIZE = 19;

	//PURPOSE
	//  Retrieves the net peer ID for the local machine.
	//  Note: this is not the same value as in rlPeerInfo.
	//PARAMS
	//  id          - Local peer ID (out)
	//RETURNS
	//  True on success.
	//NOTES
	//  The peer id should *not* be saved to persistent storage.  It will
	//  change with each reboot.
	static bool GetLocalPeerId(netPeerId& peerId);

	static const netPeerId INVALID_PEER_ID;

    netPeerId();

    //PURPOSE
    //  Invalidates the peer id.
    void Clear();

    //PURPOSE
    //  Returns true if the peer id is valid.
    bool IsValid() const;

	//PURPOSE
	//  Returns true if the peer id represents the local peer.
	bool IsLocal() const;

	//PURPOSE
	//  Returns the internal u64 peer id for backward compatibility
	//  with systems that use rlPeerId as u64.
	u64 GetU64() const;

    //PURPOSE
    //  Exports data in a platform/endian independent format.
    //PARAMS
    //  buf         - Destination buffer.
    //  sizeofBuf   - Size of destination buffer.
    //  size        - Set to number of bytes exported.
    //RETURNS
    //  True on success.
    bool Export(void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0) const;

    //PURPOSE
    //  Imports data exported with Export().
    //  buf         - Source buffer.
    //  sizeofBuf   - Size of source buffer.
    //  size        - Set to number of bytes imported.
    //RETURNS
    //  True on success.
    bool Import(const void* buf,
                const unsigned sizeofBuf,
                unsigned* size = 0);

    bool operator==(const netPeerId& that) const;
    bool operator!=(const netPeerId& that) const;

    //PURPOSE
    //  Implemented for the benefit of containers like std::map,
	//  and templated versions of Min/Max.
    bool operator<(const netPeerId& that) const;
	bool operator>(const netPeerId& that) const;

    //PURPOSE
    //  Creates a null-terminated string representing the peer id.
    //PARAMS
    //  dst         - Destination char array.
    //  dstLen      - Number of chars in the array.
    const char* ToHexString(char* dst,
                        const unsigned dstLen) const;
    template<int SIZE>
    const char* ToHexString(char (&buf)[SIZE]) const
    {
        return ToHexString(buf, SIZE);
    }

private:
	static sysCriticalSectionToken sm_LocalPeerIdCs;
	static u64 sm_LocalPeerId;

    u64 m_Id;
};

}   //namespace rage

#endif  //NET_PEERID_H
