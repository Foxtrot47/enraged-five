//
// net/natdetector.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#include "natdetector.h"
#include "diag/output.h"
#include "diag/seh.h"
#include "natpcp.h"
#include "natupnp.h"
#include "netdiag.h"
#include "nethardware.h"
#include "netsocket.h"
#include "packet.h"
#include "system/nelem.h"
#include "system/threadpool.h"
#include "system/timer.h"
#include "rline/rltelemetry.h"
#include "rline/ros/rlros.h"
#include "rline/rlpc.h"
#include "rline/rlpresence.h"

#include "status.h"

#if RSG_PC || RSG_DURANGO
#define STRICT
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#pragma warning(pop)
#endif

#if RSG_ANDROID
#include <sys/select.h>
#endif

#include <time.h>

namespace rage
{

#if defined(RSG_LEAN_CLIENT)
#define NAT_DETECTION_PARAM NOSTRIP_PC_PARAM
#else
#define NAT_DETECTION_PARAM PARAM
#endif

NAT_DETECTION_PARAM(netnattype, "Bypasses NAT type detection and sets the local NAT type. Example -netnattype=Open, -netnattype=Moderate, -netnattype=Strict, -netnattype=Unknown, -netnattype=Random");

RAGE_DEFINE_SUBCHANNEL(ragenet, natdetection)
#undef __rage_channel
#define __rage_channel ragenet_natdetection

AUTOID_IMPL(netNatEvent);
AUTOID_IMPL(netNatEventNatInfoChanged);

/*
	How often we resend packets.
*/
static const unsigned ND_TRANSACTION_RETRY_INTERVAL_MS = 750;

/*
	The maximum number of times to retry sending packets.
*/
static const unsigned ND_MAX_RETRIES_PER_TRANSACTION = 3;

/*
	The maximum amount of time to wait for a packet to arrive over the relay.
*/
static const unsigned ND_RELAY_PACKET_TIMEOUT_MS = 5000;

/*
	The maximum amount of time to wait before sending the telemetry.
	If we can't connect to the relay, etc. we still want to send the telemetry.
*/
const unsigned MAX_REPORT_WAIT_TIME_MS = 10 * 60 * 1000;

bool netNatDetector::m_Initialized = false;
bool netNatDetector::sm_AllowUdpTimeoutMeasurement = true;
bool netNatDetector::sm_AllowAdjustableUdpSendInterval = true;
bool netNatDetector::sm_AllowNatInfoTelemetry = true;

netNatType netNatDetector::sm_LocalNatType = NET_NAT_UNKNOWN;
netNatInfo netNatDetector::sm_LocalNatInfo;
#if NET_DISABLE_NAT_DETECTION
static netNatTypeDiscoveryState s_NatTypeDiscoveryState = NAT_TYPE_STATE_FAILED;
#else
static netNatTypeDiscoveryState s_NatTypeDiscoveryState = NAT_TYPE_STATE_WAIT_FOR_ROS_ONLINE;
#endif
static netSocketAddress s_MappedAddrs[netNatPortMapper::MAX_NAT_TYPE_DISCOVERY_SERVERS];
static netStatus s_MappedAddrStatus[netNatPortMapper::MAX_NAT_TYPE_DISCOVERY_SERVERS];
static netSocketAddress s_MappedAddrsAltPort[1];
static netStatus s_MappedAddrStatusAltPort[1];
static rlRosGeoLocInfo s_NatTypeServerList;
static netTimeout s_NatDiscoveryTimeout;
static netTimeout s_NatReportTimeout;
static netRandom s_NatTypeRng;
netNatDetector::Delegator netNatDetector::sm_Delegator;
sysCriticalSectionToken netNatDetector::m_Cs;

//////////////////////////////////////////////////////////////////////////
//  netNatInfo
//////////////////////////////////////////////////////////////////////////

void netNatInfo::Clear()
{
	m_RelayMappedAddr.Clear();
	m_NatDetected = false;
	m_NatType = NET_NAT_UNKNOWN;
	m_PortMappingMethod = NET_NAT_PMM_UNKNOWN;
	m_FilteringMode = NET_NAT_FM_UNKNOWN;
	m_PortAllocationStrategy = NET_NAT_PAS_UNKNOWN;
	m_PortIncrement = 0;
	m_UdpTimeoutState = NAT_UDP_TIMEOUT_UNATTEMPTED;
	m_UdpTimeoutSec = 0;
	m_NumRelayAddrChanges = 0;
	m_NumRelayMappedAddrChanges = 0;
	m_NonDeterministic = false;
}

bool netNatInfo::Import(const void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/)
{
	bool success = false;

    datImportBuffer bb;
    bb.SetReadOnlyBytes(buf, sizeofBuf);

	success =  bb.SerUser(m_RelayMappedAddr)
				&& bb.SerBool(m_NatDetected)
				&& bb.SerUns(m_NatType, datBitsNeeded<NET_NAT_NUM_TYPES>::COUNT)
				&& bb.SerUns(m_PortMappingMethod, datBitsNeeded<NET_NAT_PMM_NUM_MAPPING_METHODS>::COUNT)
				&& bb.SerUns(m_FilteringMode, datBitsNeeded<NET_NAT_FM_NUM_FILTERING_MODES>::COUNT)
				&& bb.SerUns(m_PortAllocationStrategy, datBitsNeeded<NET_NAT_PAS_NUM_ALLOCATION_STRATEGIES>::COUNT)
				&& bb.SerUns(m_PortIncrement, sizeof(m_PortIncrement) << 3)
				&& bb.SerUns(m_UdpTimeoutState, datBitsNeeded<NAT_UDP_TIMEOUT_NUM_STATES>::COUNT)
				&& bb.SerUns(m_UdpTimeoutSec, sizeof(m_UdpTimeoutSec) << 3)
				&& bb.SerUns(m_NumRelayAddrChanges, sizeof(unsigned) << 3)
				&& bb.SerUns(m_NumRelayMappedAddrChanges, sizeof(unsigned) << 3)
				&& bb.SerBool(m_NonDeterministic);

    if(size){*size = success ? bb.GetNumBytesRead() : 0;}
    return success;
}

bool netNatInfo::Export( void* buf, const unsigned sizeofBuf, unsigned* size /*= 0*/ ) const
{
    bool success = false;

    datExportBuffer bb;
    bb.SetReadWriteBytes(buf, sizeofBuf);
	
	unsigned numRelayAddrChanges = netRelay::GetNumRelayAddrChanges();
	unsigned numRelayMappedAddrChanges = netRelay::GetNumRelayMappedAddrChanges();

	success = bb.SerUser(netRelay::GetMyRelayAddress().GetTargetAddress())
				&& bb.SerBool(m_NatDetected)
				&& bb.SerUns(m_NatType, datBitsNeeded<NET_NAT_NUM_TYPES>::COUNT)
				&& bb.SerUns(m_PortMappingMethod, datBitsNeeded<NET_NAT_PMM_NUM_MAPPING_METHODS>::COUNT)
				&& bb.SerUns(m_FilteringMode, datBitsNeeded<NET_NAT_FM_NUM_FILTERING_MODES>::COUNT)
				&& bb.SerUns(m_PortAllocationStrategy, datBitsNeeded<NET_NAT_PAS_NUM_ALLOCATION_STRATEGIES>::COUNT)
				&& bb.SerUns(m_PortIncrement, sizeof(m_PortIncrement) << 3)
				&& bb.SerUns(m_UdpTimeoutState, datBitsNeeded<NAT_UDP_TIMEOUT_NUM_STATES>::COUNT)
				&& bb.SerUns(m_UdpTimeoutSec, sizeof(m_UdpTimeoutSec) << 3)
				&& bb.SerUns(numRelayAddrChanges, sizeof(unsigned) << 3)
				&& bb.SerUns(numRelayMappedAddrChanges, sizeof(unsigned) << 3)
				&& bb.SerBool(m_NonDeterministic);
	
	netAssert(bb.GetNumBytesWritten() <= MAX_EXPORTED_SIZE_IN_BYTES);

    if(size){*size = success ? bb.GetNumBytesWritten() : 0;}
    return success;
}

bool netNatInfo::Init(bool natDetected,
					  netNatType natType,
					  netNatPortMappingMethod portMappingMethod,
					  netNatFilteringMode filteringMode,
					  netNatPortAllocationStrategy portAllocationStrategy,
					  u8 portIncrement,
					  netNatUdpTimeoutState udpTimeoutState,
					  u16 udpTimeoutSec)
{
	m_RelayMappedAddr = netRelay::GetMyRelayAddress().GetTargetAddress();
	m_NatDetected = natDetected;
	m_NatType = natType;
	m_PortMappingMethod = portMappingMethod;
	m_FilteringMode = filteringMode;
	m_PortAllocationStrategy = portAllocationStrategy;
	m_PortIncrement = portIncrement;
	m_UdpTimeoutState = udpTimeoutState;
	m_UdpTimeoutSec = udpTimeoutSec;

	SetDirty();

	return true;
}

void netNatInfo::UpdateRelayMappedAddr()
{
	m_RelayMappedAddr = netRelay::GetMyRelayAddress().GetTargetAddress();
	SetDirty();
}

#if !__NO_OUTPUT
void
netNatInfo::DumpInfo() const
{
	netDebug("NAT Detection Report");
	netDebug("    Relay Mapped Address: %s", m_RelayMappedAddr.ToString());
	netDebug("    NAT Detected: %s", m_NatDetected ? "true" : "false");
	netDebug("    NAT Type: %s%s", netHardware::GetNatTypeString(m_NatType), m_NonDeterministic ? " (Non-Deterministic)" : "");
	netDebug("    Port Preserved: %s", (m_PortAllocationStrategy == NET_NAT_PAS_PORT_PRESERVING) ? "true" : "false");
	netDebug("    Port Mapping Method: %s", (m_PortMappingMethod == NET_NAT_PMM_ENDPOINT_DEPENDENT) ? "Endpoint Dependent" : "Endpoint Independent");
	netDebug("    Filtering Mode: %s", (m_FilteringMode == NET_NAT_FM_PORT_AND_ADDRESS_RESTRICTED) ? "Port and Address Restricted" : "Endpoint Independent or Address Restricted");

	if(m_NatType == NET_NAT_STRICT)
	{
		if(m_PortAllocationStrategy == NET_NAT_PAS_PORT_PRESERVING)
		{
			netDebug("    Port Allocation Strategy: Port-Preserving");
		}
		else if(m_PortAllocationStrategy == NET_NAT_PAS_PORT_CONTIGUOUS)
		{
			netDebug("    Port Allocation Strategy: Contiguous with port increment of %d", m_PortIncrement);
		}
		else
		{
			netDebug("    Port Allocation Strategy: Non-Deterministic");
		}
	}

	netDebug("    UDP Port Binding Timeout Detection: %s", (m_UdpTimeoutState == NAT_UDP_TIMEOUT_SUCCEEDED) ? "Succeeded" : ((m_UdpTimeoutState == NAT_UDP_TIMEOUT_FAILED) ? "Failed" : ((m_UdpTimeoutState == NAT_UDP_TIMEOUT_IN_PROGRESS) ? "In Progress" : "Not Performed")));
	if(m_UdpTimeoutState == NAT_UDP_TIMEOUT_SUCCEEDED)
	{
		netDebug("    UDP Port Binding Timeout: %u seconds", m_UdpTimeoutSec);
	}

	netDebug("    Num Relay Addr Changes: %u", m_NumRelayAddrChanges);
	netDebug("    Num Relay Mapped Addr Changes: %u", m_NumRelayMappedAddrChanges);
}
#endif

void
netNatInfo::SetDirty()
{
	netNatDetector::UpdateRgsc();

	netNatEventNatInfoChanged e;
	netNatDetector::DispatchEvent(&e);
}

//////////////////////////////////////////////////////////////////////////
//  netNatPortMapper
//////////////////////////////////////////////////////////////////////////

netNatPortMapper::netNatPortMapper()
{
	m_Initialized = false;

	for(unsigned i = 0; i < MAX_ADDRESSES; ++i)
	{
		m_MappedAddrs[i] = NULL;
		m_Status[i] = NULL;

		m_MappedAddrsAltPort[i] = NULL;
		m_StatusAltPort[i] = NULL;
	}

	m_SktPtr = nullptr;
	this->Clear();
}

netNatPortMapper::~netNatPortMapper()
{
	m_SktPtr = nullptr;
	this->Clear();
}

bool netNatPortMapper::Init(const unsigned minAddrs,
							const unsigned maxAddrs,
							const bool establishUdpStream)
{
	bool success = Init(nullptr, minAddrs, maxAddrs);
	if(success)
	{
		m_EstablishUdpStream = establishUdpStream;
	}
	return success;
}

bool netNatPortMapper::Init(netSocket* skt,
							const unsigned minAddrs,
							const unsigned maxAddrs)
{
	rtry
	{
		Clear();

		m_MaxRetries = ND_MAX_RETRIES_PER_TRANSACTION;

		rverifyall((skt == nullptr) || skt->IsCreated());
		rverify(minAddrs > 0, catchall, );
		rverify((maxAddrs >= minAddrs) && (maxAddrs <= MAX_ADDRESSES), catchall, );
		m_MinAddrs = minAddrs;
		m_MaxAddrs = maxAddrs;

		rcheck(GetNatTypeDiscoveryServers(), catchall, netWarning("GetNatTypeDiscoveryServers() failed"));

		rverify(netHardware::IsOnline(), catchall, netError("netNatPortMapper::Init - netHardware is not online"));
		rverify(netHardware::IsAvailable(), catchall, netError("netNatPortMapper::Init - netHardware is not available"));

		m_SktPtr = skt;

		if(!skt)
		{
			// no socket was supplied, we'll create one here
			m_SktPtr = &m_Skt;

			// We don't use port 0 here because we don't want to re-use a port we've used previously (even from a
			// previous launch in the last 15 minutes). Ideally we want to use a port that doesn't have a port
			// mapping on our NAT.
			unsigned short lowerBound = 0;
			unsigned short upperBound = 0;
			netSocket::GetPortRangeForRandomSelection(lowerBound, upperBound);

			u16 port = (u16)s_NatTypeRng.GetRanged(lowerBound, upperBound);
			rverify(netHardware::CreateSocket(m_SktPtr, port, NET_PROTO_UDP, NET_SOCKET_NONBLOCKING), catchall, netError("Error creating NAT port mapping socket"));
		}

		rlDebug("netNatPortMapper :: socket id %u", m_SktPtr->GetId());

		m_Initialized = true;

		return true;
	}
	rcatchall
	{

	}

	return false;
}

bool netNatPortMapper::GetNatTypeDiscoveryServers()
{
	bool success = false;
	rtry
	{
		rverify(rlRos::GetGeoLocInfo(&s_NatTypeServerList), catchall, netError("No NAT discovery servers"));
		rcheck(s_NatTypeServerList.m_NumRelayAddrs >= (int)m_MinAddrs, catchall, netWarning("Not enough NAT type discovery servers available"));

		m_NumAddrs = (s_NatTypeServerList.m_NumRelayAddrs >= (int)m_MaxAddrs) ? m_MaxAddrs : s_NatTypeServerList.m_NumRelayAddrs;

		// choose N random servers, making sure they're all at different IPs
		for(unsigned i = 0; i < m_NumAddrs; ++i)
		{
			unsigned numIterations = 0;
			unsigned maxIterations = 1000;

			while(true)
			{
				++numIterations;
				if(numIterations > maxIterations)
				{
					netWarning("Too many iterations to choose random NAT discovery servers");
					break;
				}

				const int idx = s_NatTypeRng.GetInt() % s_NatTypeServerList.m_NumRelayAddrs;
				const netSocketAddress& addr = s_NatTypeServerList.m_RelayAddrs[idx];
				if(!addr.IsValid())
				{
					continue;
				}

				bool duplicate = false;
				for(unsigned j = 0; j < i; ++j)
				{
					if(addr.GetIp() == m_ServerAddrs[j].GetIp())
					{
						duplicate = true;
						break;
					}
				}

				if(duplicate)
				{
					continue;
				}

				m_ServerAddrs[i] = addr;
				break;
			}
		}

		bool valid = true;
		for(unsigned i = 0; i < m_NumAddrs; ++i)
		{
			if(!m_ServerAddrs[i].IsValid())
			{
				valid = false;
				break;
			}
		}

#if !__NO_OUTPUT
		if(valid)
		{
			for(unsigned i = 0; i < m_NumAddrs; ++i)
			{
				netDebug("NAT type discovery server address %d of %d: " NET_ADDR_FMT, i + 1, m_NumAddrs, NET_ADDR_FOR_PRINTF(m_ServerAddrs[i]));
			}
		}
#endif

		success = valid;
	}
	rcatchall
	{

	}

	return success;
}

void netNatPortMapper::Clear()
{
	// only destroy the socket if we created it, do not destroy the socket
	// if the caller supplied it (i.e. don't destroy m_SktPtr here).
	if(m_Skt.IsCreated())
	{
		netHardware::DestroySocket(&m_Skt);
	}

	m_SktPtr = nullptr;

	m_MinAddrs = 0;
	m_MaxAddrs = 0;
	m_NumAddrs = 0;
	m_MaxRetries = 0;
	m_NumPongsReceived = 0;
	m_EstablishUdpStream = false;

	for(unsigned i = 0; i < MAX_ADDRESSES; ++i)
	{
		m_NextSendTime[i] = 0;
		m_NumRetries[i] = 0;
		m_ServerAddrs[i].Clear();
		m_MappedAddrs[i] = NULL;

		if(m_Status[i] && m_Status[i]->Pending())
		{
			m_Status[i]->SetCanceled();
		}

		m_Status[i] = NULL;

		// alt port
		m_NextSendTimeAltPort[i] = 0;
		m_NumRetriesAltPort[i] = 0;
		m_MappedAddrsAltPort[i] = NULL;

		if(m_StatusAltPort[i] && m_StatusAltPort[i]->Pending())
		{
			m_StatusAltPort[i]->SetCanceled();
		}

		m_StatusAltPort[i] = NULL;
	}

	m_Initialized = false;
}

unsigned netNatPortMapper::GetNumServersAvailable() const
{
	return m_NumAddrs;
}

netSocketAddress netNatPortMapper::GetServerAddress(const unsigned index) const
{
	rtry
	{
		rverify(index < m_NumAddrs, catchall, );
		return m_ServerAddrs[index];
	}
	rcatchall
	{
		netSocketAddress addr;
		return addr;
	}
}

netSocket& netNatPortMapper::GetSocket()
{
	netAssert(m_SktPtr);
	if(m_SktPtr)
	{
		return *m_SktPtr;
	}
	else
	{
		return m_Skt;
	}
}

const netSocketAddress& netNatPortMapper::GetSocketAddress() const
{
	return m_SktPtr->GetAddress();
}

bool netNatPortMapper::IsReadyToSend() const
{
	return m_SktPtr && m_SktPtr->CanSendReceive();
}

void netNatPortMapper::UpdateOne(const unsigned i)
{
	if((m_Status[i] == NULL) || !m_Status[i]->Pending())
	{
		return;
	}

	const unsigned curTime = sysTimer::GetSystemMsTime();

	if(!m_NextSendTime[i] || int(curTime - m_NextSendTime[i]) >= 0)
	{
		if(m_NumRetries[i] > m_MaxRetries)
		{
			if(m_Status[i])
			{
				m_Status[i]->SetFailed();
				return;
			}
		}

		SendPing(i);

		m_NumRetries[i]++;

		m_NextSendTime[i] = (curTime + ND_TRANSACTION_RETRY_INTERVAL_MS) | 0x01;
	}
}

void netNatPortMapper::UpdateOneAltPort(const unsigned i)
{
	if((m_StatusAltPort[i] == NULL) || !m_StatusAltPort[i]->Pending())
	{
		return;
	}

	const unsigned curTime = sysTimer::GetSystemMsTime();

	if(!m_NextSendTimeAltPort[i] || int(curTime - m_NextSendTimeAltPort[i]) >= 0)
	{
		if(m_NumRetriesAltPort[i] > m_MaxRetries)
		{
			if(m_StatusAltPort[i])
			{
				m_StatusAltPort[i]->SetFailed();
				return;
			}
		}

		SendPingAltPort(i);

		m_NumRetriesAltPort[i]++;

		m_NextSendTimeAltPort[i] = (curTime + ND_TRANSACTION_RETRY_INTERVAL_MS) | 0x01;
	}
}

void netNatPortMapper::Update()
{
	rtry
	{
		if(!m_Initialized)
		{
			return;
		}

		if(IsReadyToSend())
		{
			rverifyall(ProcessSocket());

			for(unsigned i = 0; i < m_NumAddrs; ++i)
			{
				UpdateOne(i);
				UpdateOneAltPort(i);
			}
		}
	}
	rcatchall
	{
		Clear();
	}
}

bool netNatPortMapper::MapPort(const unsigned index,
							   netSocketAddress& mappedAddr,
							   netStatus* status)
{
	bool success = false;

	rtry
	{
		rverify(IsReadyToSend(), catchall, );
		rverify(index < m_NumAddrs, catchall, );
		rverify(status, catchall, );
		rverify(!status->Pending(), catchall, );
		rverify(m_Status[index] == NULL, catchall, );

		status->SetPending();
		m_Status[index] = status;

		mappedAddr.Clear();
		m_MappedAddrs[index] = &mappedAddr;

		UpdateOne(index);

		success = true;
	}
	rcatchall
	{
		if(status)
		{
			if(status->Pending())
			{
				status->SetFailed();
			}
			else
			{
				status->ForceFailed();
			}
		}
	}

	return success;
}

bool netNatPortMapper::MapAltPort(const unsigned index,
								  netSocketAddress& mappedAddr,
								  netStatus* status)
{
	bool success = false;

	rtry
	{
		rverify(IsReadyToSend(), catchall, );
		rverify(index < m_NumAddrs, catchall, );
		rverify(status, catchall, );
		rverify(!status->Pending(), catchall, );
		rverify(m_StatusAltPort[index] == NULL, catchall, );

		status->SetPending();
		m_StatusAltPort[index] = status;

		mappedAddr.Clear();
		m_MappedAddrsAltPort[index] = &mappedAddr;

		UpdateOneAltPort(index);

		success = true;
	}
	rcatchall
	{
		if(status)
		{
			if(status->Pending())
			{
				status->SetFailed();
			}
			else
			{
				status->ForceFailed();
			}
		}
	}

	return success;
}

void netNatPortMapper::Cancel()
{
	Clear();
}

bool netNatPortMapper::SendPing(const unsigned index)
{
	bool success = false;
	rtry
	{
		// reset pongs received when pinging to discard previous results on retries
		m_NumPongsReceived = 0;

		netSocketAddress& addr = m_ServerAddrs[index];

		netDebug2("Sending NAT check ping to:" NET_ADDR_FMT " (attempt %u of %u)",
				  NET_ADDR_FOR_PRINTF(addr),
				  m_NumRetries[index] + 1,
				  m_MaxRetries + 1);

		rverify(m_SktPtr->CanSendReceive(), catchall, );
		
		netRelayPingPacket pkt;

		pkt.ResetPingHeader(0);
		pkt.Mix();

		netSocketError sktErr = NET_SOCKERR_UNKNOWN;
		rverify(m_SktPtr->Send(addr, &pkt, pkt.GetSize(), &sktErr), catchall, );

		if(m_EstablishUdpStream)
		{
			//  Many routers have two different UDP timeouts. One that is in effect
			//  after the first packet is sent to a new endpoint, but before a packet
			//  is received from that endpoint. This is called the UNREPLIED state.
			//  If a packet is not received within X seconds of the initial packet
			//  being sent, the port times out. If a 'UDP stream' is detected
			//  (aka a reply packet is received), then the connection enters the
			//  'ASSURED' state. The port is only closed after no packets are sent
			//  or received after the full UDP timeout interval, which we attempt
			//  to measure using the netNatPortTimeoutMeasurer. Note that some routers
			//  refresh on send, some on receive, and some on both send and receive.
			//  See comments in netNatPortTimeoutMeasurer::Init().

			//  However, some routers require two packets sent and two packets
			//  received before it enters the 'ASSURED' state. It remains in the
			//  'UNREPLIED' state if it receives only one packet back and closes the
			//  port faster. This behavior was observed using a Linksys WRT54G router
			//  with DD-WRT Firmware v24-sp2 (08/07/10) micro installed.
			//  To make sure these types of routers enter the 'ASSURED' state, we
			//  send 2 ping packets out and get 2 pongs back.
			rverify(m_SktPtr->Send(addr, &pkt, pkt.GetSize(), &sktErr), catchall, );
		}

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool netNatPortMapper::SendPingAltPort(const unsigned index)
{
	bool success = false;
	rtry
	{
		netSocketAddress& addr = m_ServerAddrs[index];

		netDebug2("Sending Alt Port NAT check ping to:" NET_ADDR_FMT " (attempt %u of %u)",
				  NET_ADDR_FOR_PRINTF(addr),
				  m_NumRetriesAltPort[index] + 1,
				  m_MaxRetries + 1);

		rverify(m_SktPtr->CanSendReceive(), catchall, );
		
		netRelayAltPortPingPacket pkt;

		pkt.ResetAltPortPingHeader(0);
		pkt.Mix();

		netSocketError sktErr = NET_SOCKERR_UNKNOWN;
		rverify(m_SktPtr->Send(addr, &pkt, pkt.GetSize(), &sktErr), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool netNatPortMapper::ProcessSocket()
{
    rtry
    {
		if(!m_SktPtr->CanSendReceive())
		{
			// not ready yet
			return true;
		}
	
        fd_set fdsr, fdsw, fdse;

        FD_ZERO(&fdsr);
        FD_ZERO(&fdsw);
        FD_ZERO(&fdse);
        const netSocketFd sktFd = (netSocketFd)m_SktPtr->GetRawSocket();
		rcheck(sktFd >= 0, catchall, );
        FD_SET(sktFd, &fdsr);

		// do not block
		timeval t;
		t.tv_sec  = 0;
		t.tv_usec = 0;

        const int numSkts = netSocket::Select(int(sktFd+1), &fdsr, &fdsw, &fdse, &t);

		if(numSkts < 0)
		{
			const int lastError = netSocket::GetLastSocketError();
			if(NET_SOCKERR_WOULD_BLOCK != lastError && NET_SOCKERR_IN_PROGRESS != lastError)
			{
				netError("Error %d on socket: 0x%08x", lastError, (int)sktFd);
				return false;
			}
			return true;
		}

        if((numSkts == 0) || !FD_ISSET(sktFd, &fdsr))
        {
			return true;
        }

		u8 pktBuf[netRelayPacket::MAX_SIZEOF_RELAY_PACKET+1];
		netSocketAddress from;
		const int len = m_SktPtr->Receive(&from, pktBuf, sizeof(pktBuf) - 1);

		if(len < 0)
		{
			const int lastError = netSocket::GetLastSocketError();
			if(NET_SOCKERR_WOULD_BLOCK != lastError && NET_SOCKERR_IN_PROGRESS != lastError)
			{
				netError("Error %d on socket: 0x%08x", lastError, (int)sktFd);
				return false;
			}
		}
		else if(len > 0)
		{
			// not checking the return value - if our socket receives an invalid packet (from a LAN
			// broadcast for example), we discard it and continue. See url:bugstar:6927197.
			OnReceive(from, pktBuf, len);
		}

		return true;
    }
    rcatchall
    {
    }

	return false;
}

bool
netNatPortMapper::OnReceive(const netSocketAddress& from,
							void* pktBuf,
							const int len)
{
    rtry
    {
		rcheck(len > 0,catchall,);

		netDebug3("netNatPortMapper [" NET_ADDR_FMT "]:PKT<-,sz:%d",
                    NET_ADDR_FOR_PRINTF(from),
                    len);

        netRelayPacket* relayPkt = static_cast<netRelayPacket*>(pktBuf);

        rcheck(len >= netRelayPacket::MIN_SIZEOF_HEADER
                && len <= netRelayPacket::MAX_SIZEOF_RELAY_PACKET,
                catchall,
                netDebug2("Packet size:%d is outside valid range:%d-%d",
                            len,
                            netRelayPacket::MIN_SIZEOF_HEADER,
                            netRelayPacket::MAX_SIZEOF_RELAY_PACKET));

        //GetSize() can return less than len if it contains plaintext.
        rcheck((int)relayPkt->GetSize() <= len,
                catchall,
                netDebug2("Dropping packet with invalid size:%d - should be:%d.",
                            relayPkt->GetSize(),
                            len));

        relayPkt->Unmix();

        rcheck(relayPkt->IsRelayPacket(),
                catchall,
                netDebug2("Received unknown packet type"));

        rcheck(relayPkt->GetVersion() == netRelayPacket::PROTOCOL_VERSION,
                catchall,
                netDebug2("Incompatible protocol version %d, expected %d",
                            relayPkt->GetVersion(),
                            netRelayPacket::PROTOCOL_VERSION));

		rverify(relayPkt->IsPong(),
				catchall,
				netError("Received an invalid packet type from [" NET_ADDR_FMT "]", NET_ADDR_FOR_PRINTF(from));)

        //This is a PONG in response to a PING we sent
		int index = -1;
		bool altPort = false;
		for(unsigned i = 0; i < m_NumAddrs; ++i)
		{
			if(m_ServerAddrs[i] == from)
			{
				index = i;
				altPort = false;
				break;
			}
			else if((m_ServerAddrs[i].GetIp() == from.GetIp()) && (m_ServerAddrs[i].GetPort() != from.GetPort()))
			{
				index = i;
				altPort = true;
				break;
			}
		}

		rverify(index >= 0,
				catchall,
				netError("NAT Check PONG from unknown source [" NET_ADDR_FMT "]", NET_ADDR_FOR_PRINTF(from));)

		netStatus* status = NULL;
		netSocketAddress* mappedAddr = NULL;

		if(altPort)
		{
			status = m_StatusAltPort[index];
			mappedAddr = m_MappedAddrsAltPort[index];
		}
		else
		{
			status = m_Status[index];
			mappedAddr = m_MappedAddrs[index];
		}

		const netSocketAddress myTargetAddr(relayPkt->GetDstIp(), relayPkt->GetDstPort());

		netDebug3("%sNAT Check PONG %d from [" NET_ADDR_FMT "]. Mapped Address: [" NET_ADDR_FMT "]", altPort ? "Alt Port " : "", index + 1, NET_ADDR_FOR_PRINTF(from), NET_ADDR_FOR_PRINTF(myTargetAddr));

		if(status && status->Pending())
		{
			if(mappedAddr)
			{
				if(netVerify(myTargetAddr.IsValid()))
				{
					(*mappedAddr) = myTargetAddr;
					++m_NumPongsReceived;

					if(m_EstablishUdpStream)
					{
						if(m_NumPongsReceived >= 2)
						{
							status->SetSucceeded();
						}
					}
					else
					{
						status->SetSucceeded();
					}
				}
				else
				{
					mappedAddr->Clear();
					status->SetFailed();
				}
			}
			else
			{
				status->SetFailed();
			}
		}

		return true;
    }
    rcatchall
    {
		return false;
    }
}

//////////////////////////////////////////////////////////////////////////
//  netNatSharedSocketPortMapper
//////////////////////////////////////////////////////////////////////////

netNatSharedSocketPortMapper::netNatSharedSocketPortMapper()
{
	m_Initialized = false;
	
	for(unsigned i = 0; i < MAX_ADDRESSES; ++i)
	{
		m_MappedAddrs[i] = NULL;
		m_Status[i] = NULL;
	}

	this->Clear();
}

netNatSharedSocketPortMapper::~netNatSharedSocketPortMapper()
{
	this->Clear();
}

bool netNatSharedSocketPortMapper::Init(const unsigned minAddrs,
										const unsigned maxAddrs)
{
	SYS_CS_SYNC(m_Cs);

	rtry
	{
		Clear();

		rverify(netHardware::IsOnline(), catchall, netError("netNatSharedSocketPortMapper::Init - netHardware is not online"));
		rverify(netHardware::IsAvailable(), catchall, netError("netNatSharedSocketPortMapper::Init - netHardware is not available"));

		m_MaxRetries = ND_MAX_RETRIES_PER_TRANSACTION;

		rverify(minAddrs > 0, catchall, );
		rverify((maxAddrs >= minAddrs) && (maxAddrs <= MAX_ADDRESSES), catchall, );
		m_MinAddrs = minAddrs;
		m_MaxAddrs = maxAddrs;

		rverify(GetNatTypeDiscoveryServers(), catchall, );

		m_RelayDelegateReceivePong.Bind(this, &netNatSharedSocketPortMapper::OnRelayEventReceivePong);

		// note: this can't use AddConcurrentDelegate() due to a deadlock caused by a circular critical section dependency between the
		// relay and connection manager. See url:bugstar:2475670.
		// If we want to get pongs from the relay thread, could register a concurrent delegate at the class level (static function)
		// add the pong to a list, then have each port mapper instance check the list during update to retrieve the pong.
		// Could also cache the results for 15 seconds.
		netRelay::AddDelegate(NET_RELAYEVENT_RELAY_PONG_RECEIVED, &m_RelayDelegateReceivePong);

		m_Initialized = true;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool netNatSharedSocketPortMapper::Init(const netSocketAddress& addr)
{
	SYS_CS_SYNC(m_Cs);

	bool success = false;

	rtry
	{
		rverify(addr.IsValid(), catchall, );
		rverify(netHardware::IsOnline(), catchall, netError("netNatSharedSocketPortMapper::Init - netHardware is not online"));
		rverify(netHardware::IsAvailable(), catchall, netError("netNatSharedSocketPortMapper::Init - netHardware is not available"));

		Clear();

		m_MaxRetries = ND_MAX_RETRIES_PER_TRANSACTION;

		m_MinAddrs = 1;
		m_MaxAddrs = 1;
		m_NumAddrs = 1;
		m_ServerAddrs[0] = addr;

		m_RelayDelegateReceivePong.Bind(this, &netNatSharedSocketPortMapper::OnRelayEventReceivePong);

		// note: this can't use AddConcurrentDelegate() due to a deadlock caused by a circular critical section dependency between the
		// relay and connection manager. See url:bugstar:2475670.
		// If we want to get pongs from the relay thread, could register a concurrent delegate at the class level (static function)
		// add the pong to a list, then have each port mapper instance check the list during update to retrieve the pong.
		// Could also cache the results for 15 seconds.
		netRelay::AddDelegate(NET_RELAYEVENT_RELAY_PONG_RECEIVED, &m_RelayDelegateReceivePong);

		m_Initialized = true;

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

bool netNatSharedSocketPortMapper::GetNatTypeDiscoveryServers()
{
	bool success = false;
	rtry
	{
		rverify(rlRos::GetGeoLocInfo(&s_NatTypeServerList), catchall, netError("No NAT discovery servers"));
		rcheck(s_NatTypeServerList.m_NumRelayAddrs >= (int)m_MinAddrs, catchall, netWarning("Not enough NAT type discovery servers available"));

		m_NumAddrs = (s_NatTypeServerList.m_NumRelayAddrs >= (int)m_MaxAddrs) ? m_MaxAddrs : s_NatTypeServerList.m_NumRelayAddrs;

		// choose N random servers, making sure they're all at different IPs
		for(unsigned i = 0; i < m_NumAddrs; ++i)
		{
			unsigned numIterations = 0;
			unsigned maxIterations = 1000;

			while(true)
			{
				++numIterations;
				if(numIterations > maxIterations)
				{
					netWarning("Too many iterations to choose random NAT discovery servers");
					break;
				}

				const int idx = s_NatTypeRng.GetInt() % s_NatTypeServerList.m_NumRelayAddrs;
				const netSocketAddress& addr = s_NatTypeServerList.m_RelayAddrs[idx];
				if(!addr.IsValid())
				{
					continue;
				}

				bool duplicate = false;
				for(unsigned j = 0; j < i; ++j)
				{
					if(addr.GetIp() == m_ServerAddrs[j].GetIp())
					{
						duplicate = true;
						break;
					}
				}

				if(duplicate)
				{
					continue;
				}

				m_ServerAddrs[i] = addr;
				break;
			}
		}

		bool valid = true;
		for(unsigned i = 0; i < m_NumAddrs; ++i)
		{
			if(!m_ServerAddrs[i].IsValid())
			{
				valid = false;
				break;
			}
		}

#if !__NO_OUTPUT
		if(valid)
		{
			for(unsigned i = 0; i < m_NumAddrs; ++i)
			{
				netDebug("NAT type discovery server address %d of %d: " NET_ADDR_FMT, i + 1, m_NumAddrs, NET_ADDR_FOR_PRINTF(m_ServerAddrs[i]));
			}
		}
#endif

		success = valid;
	}
	rcatchall
	{

	}

	return success;
}

void netNatSharedSocketPortMapper::Clear()
{
	SYS_CS_SYNC(m_Cs);

	if(m_Initialized)
	{
		m_Initialized = false;
		netRelay::RemoveDelegate(NET_RELAYEVENT_RELAY_PONG_RECEIVED, &m_RelayDelegateReceivePong);
	}

	m_MinAddrs = 0;
	m_MaxAddrs = 0;
	m_NumAddrs = 0;
	m_MaxRetries = 0;

	for(unsigned i = 0; i < MAX_ADDRESSES; ++i)
	{
		m_NextSendTime[i] = 0;
		m_NumRetries[i] = 0;
		m_ServerAddrs[i].Clear();
		m_MappedAddrs[i] = NULL;

		if(m_Status[i] && m_Status[i]->Pending())
		{
			m_Status[i]->SetCanceled();
		}

		m_Status[i] = NULL;
	}
}

unsigned netNatSharedSocketPortMapper::GetNumServersAvailable() const
{
	return m_NumAddrs;
}

netSocketAddress netNatSharedSocketPortMapper::GetServerAddress(const unsigned index) const
{
	rtry
	{
		rverify(index < m_NumAddrs, catchall, );
		return m_ServerAddrs[index];
	}
	rcatchall
	{
		netSocketAddress addr;
		return addr;
	}
}

const netSocketAddress& netNatSharedSocketPortMapper::GetSocketAddress() const
{
	return netRelay::GetSocket()->GetAddress();
}

bool netNatSharedSocketPortMapper::IsReadyToSend() const
{
	return netRelay::GetSocket()->CanSendReceive();
}

void netNatSharedSocketPortMapper::UpdateOne(const unsigned i)
{
	SYS_CS_SYNC(m_Cs);

	if(!m_Initialized)
	{
		return;
	}

	if((m_Status[i] == NULL) || !m_Status[i]->Pending())
	{
		return;
	}

	const unsigned curTime = sysTimer::GetSystemMsTime();

	if(!m_NextSendTime[i] || int(curTime - m_NextSendTime[i]) >= 0)
	{
		if(m_NumRetries[i] > m_MaxRetries)
		{
			if(m_Status[i])
			{
				m_Status[i]->SetFailed();
				return;
			}
		}

		SendPing(i);

		m_NumRetries[i]++;

		m_NextSendTime[i] = (curTime + ND_TRANSACTION_RETRY_INTERVAL_MS) | 0x01;
	}
}

void netNatSharedSocketPortMapper::Update()
{
	SYS_CS_SYNC(m_Cs);

	if(!m_Initialized)
	{
		return;
	}

	if(IsReadyToSend())
	{
		for(unsigned i = 0; i < m_NumAddrs; ++i)
		{
			UpdateOne(i);
		}
	}
}

bool netNatSharedSocketPortMapper::MapPort(const unsigned index,
											netSocketAddress& mappedAddr,
											netStatus* status)
{
	SYS_CS_SYNC(m_Cs);

	bool success = false;

	rtry
	{
		rverify(IsReadyToSend(), catchall, );
		rverify(index < m_NumAddrs, catchall, );
		rverify(status, catchall, );
		rverify(!status->Pending(), catchall, );
		rverify(m_Status[index] == NULL, catchall, );

		status->SetPending();
		m_Status[index] = status;

		mappedAddr.Clear();
		m_MappedAddrs[index] = &mappedAddr;

		UpdateOne(index);

		success = true;
	}
	rcatchall
	{
		if(status)
		{
			if(status->Pending())
			{
				status->SetFailed();
			}
			else
			{
				status->ForceFailed();
			}
		}
	}

	return success;
}

void netNatSharedSocketPortMapper::Cancel()
{
	SYS_CS_SYNC(m_Cs);

	Clear();
}

bool netNatSharedSocketPortMapper::SendPing(const unsigned index)
{
	SYS_CS_SYNC(m_Cs);

	bool success = false;
	rtry
	{
		netSocketAddress& addr = m_ServerAddrs[index];

		netDebug2("Sending NAT check ping to:" NET_ADDR_FMT " (attempt %u of %u)",
				  NET_ADDR_FOR_PRINTF(addr),
				  m_NumRetries[index] + 1,
				  m_MaxRetries + 1);

		rverify(IsReadyToSend(), catchall, );
		
		rverify(netRelay::SendPing(addr), catchall, );

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void netNatSharedSocketPortMapper::OnRelayEventReceivePong(const netRelayEvent& event)
{
	SYS_CS_SYNC(m_Cs);

    rtry
    {
		rverify(NET_RELAYEVENT_RELAY_PONG_RECEIVED == event.m_EventType, catchall, );

		const netRelayEventRelayPongReceived& relayPongEvent = (const netRelayEventRelayPongReceived&) event;
		const netSocketAddress from = relayPongEvent.m_Address.GetProxyAddress();
		const netSocketAddress myTargetAddr = relayPongEvent.m_Address.GetTargetAddress();
		rverify(from.IsValid(), catchall, );

		int index = -1;
		for(unsigned i = 0; i < m_NumAddrs; ++i)
		{
			if(m_ServerAddrs[i] == from)
			{
				index = i;
				break;
			}
		}

		if(index >= 0)
        {
			netStatus* status = NULL;
			netSocketAddress* mappedAddr = NULL;

			status = m_Status[index];
			mappedAddr = m_MappedAddrs[index];

			if(status && status->Pending())
			{
				netDebug3("NAT Check PONG %d from [" NET_ADDR_FMT "]. Mapped Address: [" NET_ADDR_FMT "]", index + 1, NET_ADDR_FOR_PRINTF(from), NET_ADDR_FOR_PRINTF(myTargetAddr));

				if(mappedAddr)
				{
					if(myTargetAddr.IsValid())
					{
						(*mappedAddr) = myTargetAddr;
						status->SetSucceeded();
					}
					else
					{
						mappedAddr->Clear();
						status->SetFailed();
					}
				}
				else
				{
					status->SetFailed();
				}
			}
        }
    }
    rcatchall
    {
    }
}

//////////////////////////////////////////////////////////////////////////
//  netNatCombinedPortMapper
//////////////////////////////////////////////////////////////////////////

netNatCombinedPortMapper::netNatCombinedPortMapper()
{
	this->Clear();
}

netNatCombinedPortMapper::~netNatCombinedPortMapper()
{
	this->Clear();
}

bool netNatCombinedPortMapper::Init(const unsigned minAddrs,
									const unsigned maxAddrs,
									const bool sharedSocket)
{
	rtry
	{
		Clear();

		m_SharedSocket = sharedSocket;

		if(m_SharedSocket)
		{
			rverify(m_SharedSocketPortMapper.Init(minAddrs, maxAddrs), catchall,  );
		}
		else
		{
			rverify(m_PortMapper.Init(nullptr, minAddrs, maxAddrs), catchall, );
		}

		m_Initialized = true;

		return true;
	}
	rcatchall
	{
		return false;
	}
}

void netNatCombinedPortMapper::Clear()
{
	m_PortMapper.Clear();
	m_SharedSocketPortMapper.Clear();
	m_SharedSocket = false;
	m_Initialized = false;
}

unsigned netNatCombinedPortMapper::GetNumServersAvailable() const
{
	if(m_SharedSocket)
	{
		return m_SharedSocketPortMapper.GetNumServersAvailable();
	}
	else
	{
		return m_PortMapper.GetNumServersAvailable();
	}
}

netSocketAddress netNatCombinedPortMapper::GetServerAddress(const unsigned index) const
{
	if(m_SharedSocket)
	{
		return m_SharedSocketPortMapper.GetServerAddress(index);
	}
	else
	{
		return m_PortMapper.GetServerAddress(index);
	}
}

const netSocketAddress& netNatCombinedPortMapper::GetSocketAddress() const
{
	if(m_SharedSocket)
	{
		return m_SharedSocketPortMapper.GetSocketAddress();
	}
	else
	{
		return m_PortMapper.GetSocketAddress();
	}
}

bool netNatCombinedPortMapper::IsReadyToSend() const
{
	if(m_SharedSocket)
	{
		return m_SharedSocketPortMapper.IsReadyToSend();
	}
	else
	{
		return m_PortMapper.IsReadyToSend();
	}
}

void netNatCombinedPortMapper::Update()
{
	if(!m_Initialized)
	{
		return;
	}

	if(m_SharedSocket)
	{
		m_SharedSocketPortMapper.Update();
	}
	else
	{
		m_PortMapper.Update();
	}
}

bool netNatCombinedPortMapper::MapPort(const unsigned index,
									   netSocketAddress& mappedAddr,
									   netStatus* status)
{
	if(m_SharedSocket)
	{
		return m_SharedSocketPortMapper.MapPort(index, mappedAddr, status);
	}
	else
	{
		return m_PortMapper.MapPort(index, mappedAddr, status);
	}
}

void netNatCombinedPortMapper::Cancel()
{
	Clear();
}

//////////////////////////////////////////////////////////////////////////
//  netNatPortTimeoutChecker
//////////////////////////////////////////////////////////////////////////

netNatPortTimeoutChecker::netNatPortTimeoutChecker()
{
	m_Initialized = false;

	this->Clear();
}

netNatPortTimeoutChecker::~netNatPortTimeoutChecker()
{
	this->Clear();
}

bool netNatPortTimeoutChecker::Init(const unsigned timeoutToCheckSeconds)
{
	rtry
	{
		Clear();

		rverify(timeoutToCheckSeconds > 0, catchall, netError("netNatPortTimeoutChecker::Init - timeoutToCheckSeconds == 0"));

		m_TimeoutToCheckMs = timeoutToCheckSeconds * 1000;

		rverify(netHardware::IsOnline(), catchall, netError("netNatPortTimeoutChecker::Init - netHardware is not online"));
		rverify(netHardware::IsAvailable(), catchall, netError("netNatPortTimeoutChecker::Init - netHardware is not available"));

		// use any port for sending to the relay
		u16 port = 0;
		rverify(netHardware::CreateSocket(&m_SendSkt, port, NET_PROTO_UDP, NET_SOCKET_NONBLOCKING), catchall, netError("Error creating port timeout check socket"));

		rlDebug("netNatPortTimeoutChecker :: socket id %u", m_SendSkt.GetId());

		rverify(m_PortMapper.Init(1, 1, true), catchall, netError("netNatPortTimeoutChecker::Init - failed to init port mapper"));

		m_Initialized = true;

		return true;
	}
	rcatchall
	{
		Clear();
		return false;
	}
}

void netNatPortTimeoutChecker::Clear()
{
	m_PortMapper.Clear();

	if(m_SendSkt.IsCreated())
	{
		netHardware::DestroySocket(&m_SendSkt);
	}

	m_State = STATE_WAITING_FOR_SOCKETS;
	m_MappedAddr.Clear();
	if(m_MappedAddrStatus.Pending())
	{
		m_MappedAddrStatus.SetCanceled();
	}
	m_MappedAddrStatus.Reset();
	m_Addr.Clear();
	m_TimeoutToCheckMs = 0;
	m_PingSendTime = 0;
	m_CheckMessageSendTime = 0;
	m_RecievedResponse = false;

	m_Initialized = false;
}

void netNatPortTimeoutChecker::Update()
{
	if(!m_Initialized)
	{
		return;
	}

	switch(m_State)
	{
	case STATE_WAITING_FOR_SOCKETS:
		if(m_SendSkt.CanSendReceive() && m_PortMapper.IsReadyToSend())
		{
			if(m_PortMapper.MapPort(0, m_MappedAddr, &m_MappedAddrStatus))
			{
				m_State = STATE_WAITING_FOR_MAPPED_PORT;
			}
			else
			{
				m_State = STATE_ERROR;
			}
		}
		break;
	case STATE_WAITING_FOR_MAPPED_PORT:
		{
			m_PortMapper.Update();

			if(m_PingSendTime == 0)
			{
				m_PingSendTime = sysTimer::GetSystemMsTime();
			}

			if(!m_MappedAddrStatus.Pending())
			{
				if(m_MappedAddrStatus.Succeeded())
				{
					// we send a message to ourselves via the relay using this address
					m_Addr.Init(m_PortMapper.GetServerAddress(0), netRelayToken::INVALID_RELAY_TOKEN, m_MappedAddr);
					m_State = STATE_WAITING_TO_SEND;
				}
				else
				{
					m_State = STATE_ERROR;
				}
			}
		}
		break;
	case STATE_WAITING_TO_SEND:
		{
			unsigned curTime = sysTimer::GetSystemMsTime();
			unsigned timeSincePingSent = curTime - m_PingSendTime;

			// ping is sent this many ms earlier to catch UDP timeout edges
			// (eg. if the port closes in exactly 60 seconds, we send at 59.5 seconds).
			// The relay/presence code pings slightly earlier as well.
			const static unsigned PING_CHECK_SLUSH_MS = 500;
			if(timeSincePingSent >= (m_TimeoutToCheckMs - PING_CHECK_SLUSH_MS))
			{
				// we're ready to test whether the port is still mapped on the NAT
				if(SendTimeoutCheckMessage())
				{
					m_CheckMessageSendTime = sysTimer::GetSystemMsTime();
					m_State = STATE_WAITING_FOR_RESPONSE;
				}
				else
				{
					m_State = STATE_ERROR;
				}
			}
		}
		break;
	case STATE_WAITING_FOR_RESPONSE:
		{
			if(ProcessSocket(m_PortMapper.GetSocket()))
			{
				if(m_RecievedResponse)
				{
					m_State = STATE_COMPLETED;
				}
				else
				{
					unsigned curTime = sysTimer::GetSystemMsTime();
					unsigned timeSinceCheckMessageSent = curTime - m_CheckMessageSendTime;

					if(timeSinceCheckMessageSent >= ND_RELAY_PACKET_TIMEOUT_MS)
					{
						m_State = STATE_COMPLETED;
					}
				}
			}
			else
			{
				m_State = STATE_ERROR;
			}
		}
		break;
	case STATE_ERROR:
		break;
	case STATE_COMPLETED:
		break;
	}
}

bool netNatPortTimeoutChecker::Pending()
{
	return (m_State != STATE_COMPLETED) && (m_State != STATE_ERROR);
}

bool netNatPortTimeoutChecker::Succeeded()
{
	return (m_State == STATE_COMPLETED);
}

void netNatPortTimeoutChecker::Cancel()
{
	Clear();
}

bool netNatPortTimeoutChecker::SendTimeoutCheckMessage()
{
	rtry
	{
		struct MeasuringPing
		{
			MeasuringPing(int id)
			{
				m_Id = id;
			}
			int m_Id;
		};

		MeasuringPing ping(0);
		unsigned numBytes = sizeof(ping.m_Id);

		netRelayP2pPacket relayPkt;
		netPacket* pkt = relayPkt.GetPacket();
		netBundle* bndl = (netBundle*) pkt->GetPayload();

		// channel is ignored
		bndl->ResetHeader(numBytes, 1, 0, 0, 0, netBundle::FLAG_OUT_OF_BAND);
		sysMemCpy(bndl->GetPayload(), &ping.m_Id, numBytes);

		unsigned sizeofEncrypted = bndl->GetSize();

		pkt->ResetHeader(sizeofEncrypted, 0);

		const unsigned sizeofPayload = relayPkt.GetPacket()->GetSize();

		rverify(sizeofPayload > 0, catchall, netError("Send :: No payload"));

		rverify(m_Addr.IsRelayServerAddr(), catchall, netError("Send :: [" NET_ADDR_FMT "] is not a valid relay address", NET_ADDR_FOR_PRINTF(m_Addr)));
		
		if(!m_SendSkt.CanSendReceive())
		{
			netError("Send :: Can't send on socket");
			return false;
		}

		netAddress srcAddr;
		srcAddr.Init(m_Addr.GetProxyAddress(), netRelayToken::INVALID_RELAY_TOKEN, m_MappedAddr);

		relayPkt.ResetP2pHeader(sizeofPayload,
								 m_Addr,
								 srcAddr,
								 netRelayToken::INVALID_RELAY_TOKEN);

		netDebug3("Sending port timeout check to ourselves over the relay: [" NET_ADDR_FMT "]:PKT->,sz:%d",
					NET_ADDR_FOR_PRINTF(m_Addr),
					relayPkt.GetSize());

		relayPkt.Mix();

		const netSocketAddress& dstAddr = m_Addr.GetProxyAddress();

		netSocketError sktErr = NET_SOCKERR_UNKNOWN;
		rcheck(m_SendSkt.Send(dstAddr, &relayPkt, relayPkt.GetSize(), &sktErr), catchall, );

		return true;
	}
	rcatchall
	{
		return false;
	}
}

bool netNatPortTimeoutChecker::ProcessSocket(netSocket& rcvSkt)
{
    rtry
    {
		rcheck(rcvSkt.CanSendReceive(), catchall, );

        fd_set fdsr, fdsw, fdse;

        FD_ZERO(&fdsr);
        FD_ZERO(&fdsw);
        FD_ZERO(&fdse);
        const netSocketFd sktFd = (netSocketFd)rcvSkt.GetRawSocket();
		rcheck(sktFd >= 0, catchall, );
        FD_SET(sktFd, &fdsr);

		// do not block
		timeval t;
		t.tv_sec  = 0;
		t.tv_usec = 0;

        const int numSkts = netSocket::Select(int(sktFd+1), &fdsr, &fdsw, &fdse, &t);

        if(numSkts < 0)
        {
            const int lastError = netSocket::GetLastSocketError();
            if(NET_SOCKERR_WOULD_BLOCK != lastError && NET_SOCKERR_IN_PROGRESS != lastError)
            {
                netError("Error %d on socket: 0x%08x", lastError, (int)sktFd);
				return false;
            }
            return true;
        }

		if((numSkts == 0) || !FD_ISSET(sktFd, &fdsr))
        {
            return true;
        }

		u8 pktBuf[netRelayPacket::MAX_SIZEOF_RELAY_PACKET+1];
		netSocketAddress from;
		const int len = rcvSkt.Receive(&from, pktBuf, sizeof(pktBuf) - 1);

		if(len < 0)
		{
			const int lastError = netSocket::GetLastSocketError();
			if(NET_SOCKERR_WOULD_BLOCK != lastError && NET_SOCKERR_IN_PROGRESS != lastError)
			{
				netError("Error %d on socket: 0x%08x", lastError, (int)sktFd);
				return false;
			}
		}
		else if(len > 0)
		{
			// not checking the return value - if our socket receives an invalid packet (from a LAN
			// broadcast for example), we discard it and continue. See url:bugstar:6927197.
			OnReceive(from, pktBuf, len);
		}

		return true;
    }
    rcatchall
    {
		return false;
    }
}

bool
netNatPortTimeoutChecker::OnReceive(const netSocketAddress& from,
									 void* pktBuf,
									 const int len)
{
    rtry
    {
		rcheck(len > 0,catchall,);

		netDebug3("netNatPortTimeoutChecker [" NET_ADDR_FMT "]:PKT<-,sz:%d",
                    NET_ADDR_FOR_PRINTF(from),
                    len);

        netRelayPacket* relayPkt = static_cast<netRelayPacket*>(pktBuf);

        rcheck(len >= netRelayPacket::MIN_SIZEOF_HEADER
                && len <= netRelayPacket::MAX_SIZEOF_RELAY_PACKET,
                catchall,
                netDebug2("Packet size:%d is outside valid range:%d-%d",
                            len,
                            netRelayPacket::MIN_SIZEOF_HEADER,
                            netRelayPacket::MAX_SIZEOF_RELAY_PACKET));

        //GetSize() can return less than len if it contains plaintext.
        rcheck((int)relayPkt->GetSize() <= len,
                catchall,
                netDebug2("Dropping packet with invalid size:%d - should be:%d.",
                            relayPkt->GetSize(),
                            len));

        relayPkt->Unmix();

        rcheck(relayPkt->IsRelayPacket(),
                catchall,
                netDebug2("Received unknown packet type"));

        rcheck(relayPkt->GetVersion() == netRelayPacket::PROTOCOL_VERSION,
                catchall,
                netDebug2("Incompatible protocol version %d, expected %d",
                            relayPkt->GetVersion(),
                            netRelayPacket::PROTOCOL_VERSION));

		if(relayPkt->IsPong())
		{
			// ignore any pongs we receive from repeated pings we sent earlier
			netWarning("netNatPortTimeoutChecker ignoring relay pong from [" NET_ADDR_FMT "]", NET_ADDR_FOR_PRINTF(from));
			return true;
		}

		rverify(relayPkt->IsP2pTerminus(),
				catchall,
				netError("Received an invalid packet type from [" NET_ADDR_FMT "]", NET_ADDR_FOR_PRINTF(from)));

		rverify(from == m_Addr.GetProxyAddress(),
				catchall,
				netError("netNatPortTimeoutChecker received packet from unexpected sender [" NET_ADDR_FMT "]. Expected [" NET_ADDR_FMT "]", NET_ADDR_FOR_PRINTF(from), NET_ADDR_FOR_PRINTF(m_Addr)));

		m_RecievedResponse = true;
		netDebug3("NAT port binding stays open for at least %u seconds", m_TimeoutToCheckMs / 1000);

		return true;
    }
    rcatchall
    {
		return false;
    }
}

//////////////////////////////////////////////////////////////////////////
//  netNatPortTimeoutMeasurer
//////////////////////////////////////////////////////////////////////////

// We mainly care about UDP port bindings timeouts of 15, 30, 60, and 90 seconds. More intervals
// give us more information, but each interval requires 3 packets to be sent to the relay.
// The time it takes to detect the port timeout is Max(sm_Intervals[]). Note that the NAT
// might keep the port open longer than Max(sm_Intervals[]), but we're really only concerned
// about NATs that have shorter than average timeouts. RFC 4787 requires a timeout of at
// least 120 seconds to be BEHAVE compliant but many NATs are not compliant.
const unsigned netNatPortTimeoutMeasurer::sm_Intervals[] = {15, 30, 60, 90};
//const unsigned netNatPortTimeoutMeasurer::sm_Intervals[] = {15, 28, 29, 30, 31, 32, 58, 59, 60,
//															  61, 62, 88, 89, 90, 91, 92, 93, 115,
//															  116, 117, 118, 119, 120, 121, 122,
//															  123, 124, 125, 130, 150};

netNatPortTimeoutMeasurer::netNatPortTimeoutMeasurer()
{
	m_Initialized = false;

	this->Clear();
}

netNatPortTimeoutMeasurer::~netNatPortTimeoutMeasurer()
{
	this->Clear();
}

bool netNatPortTimeoutMeasurer::Init()
{
	/*
		Method:
		Open socket A, ping relay server (opening a port binding on the NAT) and get mapped address.
		Open socket B, send a packet over the relay to socket A after X seconds.

		If we receive the packet sent to ourselves, then we know the NAT kept the port
		open for X seconds. By doing this with different values of X, we can
		determine how long the NAT keeps the port open.

		The reason we need to send a packet to ourselves over the relay instead
		of simply pinging the relay server again from socket A after X seconds is that
		many NATs will preserve the port on both the initial ping and subsequent pings
		so we will end up with the same mapped address even if the NAT had closed
		the port between pings.

		Also note that some NATs keep the mapping active (i.e. refresh the timer value)
		when a packet goes from the internal side of the NAT to the external side of
		the NAT (Outbound Refresh). Some NATs keep the mapping active when a packet goes
		from the external side of the NAT to the internal side of the NAT (Inbound Refresh).

		Attempting to detect the timeout interval by frequently sending packets to
		socket A until we stop receiving wouldn't work for NATs that have inbound refresh.

		The method used here ensures that we don't send to, or receive from, the 
		port before verifying that it's still open after X seconds have elapsed.
	*/

	rtry
	{
		Clear();

		rverify(netHardware::IsOnline(), catchall, netError("netNatPortTimeoutMeasurer::Init - netHardware is not online"));
		rverify(netHardware::IsAvailable(), catchall, netError("netNatPortTimeoutMeasurer::Init - netHardware is not available"));

		for(unsigned i = 0; i < COUNTOF(m_PortTimeoutChecker); ++i)
		{
			rverify(m_PortTimeoutChecker[i].Init(sm_Intervals[i]), catchall, netError("netNatPortTimeoutMeasurer::Init - failed to init a port timeout checker"));
		}

		m_Initialized = true;

		return true;
	}
	rcatchall
	{
		Clear();
		return false;
	}
}

void netNatPortTimeoutMeasurer::Clear()
{
	for(unsigned i = 0; i < COUNTOF(m_PortTimeoutChecker); ++i)
	{
		m_PortTimeoutChecker[i].Clear();
	}
	m_State = STATE_CHECKING;
	m_Initialized = false;
}

void netNatPortTimeoutMeasurer::Update()
{
	if(!m_Initialized)
	{
		return;
	}

	for(unsigned i = 0; i < COUNTOF(m_PortTimeoutChecker); ++i)
	{
		m_PortTimeoutChecker[i].Update();
	}

	switch(m_State)
	{
	case STATE_CHECKING:
		{
			bool pending = false;
			unsigned numSucceeded = 0;

			for(unsigned i = 0; i < COUNTOF(m_PortTimeoutChecker); ++i)
			{
				if(m_PortTimeoutChecker[i].Pending())
				{
					pending = true;
					break;
				}
				else if(m_PortTimeoutChecker[i].Succeeded())
				{
					numSucceeded++;
				}
			}

			if(!pending)
			{
				if(numSucceeded == COUNTOF(m_PortTimeoutChecker))
				{
					if(this->GetUdpTimeoutSec() > 0)
					{
						m_State = STATE_COMPLETED;
					}
					else
					{
						// if none of them received a response, then we failed to measure the timeout
						m_State = STATE_ERROR;
					}
				}
				else
				{
					netWarning("At least one port timeout checker failed.");
					m_State = STATE_ERROR;
				}
			}
		}
		break;
	case STATE_ERROR:
		break;
	case STATE_COMPLETED:
		break;
	}
}

bool netNatPortTimeoutMeasurer::Pending()
{
	return (m_State != STATE_COMPLETED) && (m_State != STATE_ERROR);
}

bool netNatPortTimeoutMeasurer::Succeeded()
{
	return (m_State == STATE_COMPLETED);
}

unsigned netNatPortTimeoutMeasurer::GetUdpTimeoutSec()
{
	unsigned udpTimeoutSec = 0;

	for(unsigned i = 0; i < COUNTOF(m_PortTimeoutChecker); ++i)
	{
		if(m_PortTimeoutChecker[i].ReceivedResponse())
		{
			unsigned timeoutToCheckSeconds = m_PortTimeoutChecker[i].GetTimeoutToCheckSeconds();
			if(timeoutToCheckSeconds > udpTimeoutSec)
			{
				udpTimeoutSec = timeoutToCheckSeconds;
			}
		}			
	}

	return udpTimeoutSec;
}

void netNatPortTimeoutMeasurer::Cancel()
{
	Clear();
}

//////////////////////////////////////////////////////////////////////////
//  netNatDetector
//////////////////////////////////////////////////////////////////////////

netNatPortMapper s_PortMapper;
netNatPortTimeoutMeasurer s_PortTimoutMeasurer;

bool
netNatDetector::Init()
{
	SYS_CS_SYNC(m_Cs);
		
	s_NatTypeRng.SetRandomSeed();
	
	netNatPcp::Init();

	m_Initialized = true;
	return true;
}

void
netNatDetector::Shutdown()
{
	SYS_CS_SYNC(m_Cs);

	if(m_Initialized)
	{
		netNatUpNp::Shutdown();
		netNatPcp::Shutdown();
		s_NatTypeDiscoveryState = NAT_TYPE_STATE_WAIT_FOR_ROS_ONLINE;
		m_Initialized = false;
	}
}

void
netNatDetector::UpdateRgsc()
{
#if RSG_PC && !__RGSC_DLL
#if GTA_VERSION
	if(g_rlPc.GetNetworkInterface())
#else
	if(g_rlPc.GetNetworkInterface().IsValid())
#endif // GTA_VERSION
	{
		rgsc::NetworkInfo info;
		info.SetAllowAdjustablePingInterval(netNatDetector::GetAllowAdjustableUdpSendInterval());
		info.SetNatDetected(sm_LocalNatInfo.IsNatDetected());

		rgsc::NetworkInfo::NatDetectionState state;
		if(s_NatTypeDiscoveryState == NAT_TYPE_STATE_SUCCEEDED || (sm_LocalNatInfo.GetNatType() != NET_NAT_UNKNOWN))
		{
			state = rgsc::NetworkInfo::NAT_NDS_SUCCEEDED;
		}
		else if(s_NatTypeDiscoveryState == NAT_TYPE_STATE_FAILED)
		{
			state = rgsc::NetworkInfo::NAT_NDS_FAILED;
		}
		else if(s_NatTypeDiscoveryState > NAT_TYPE_STATE_WAIT_FOR_ROS_ONLINE)
		{
			state = rgsc::NetworkInfo::NAT_NDS_IN_PROGRESS;
		}
		else
		{
			state = rgsc::NetworkInfo::NAT_NDS_UNATTEMPTED;
		}

		info.SetNatDetectionState(state);
		info.SetNatFilteringMode((rgsc::NetworkInfo::NatFilteringMode)sm_LocalNatInfo.GetFilteringMode());
		info.SetNatPortAllocationStrategy((rgsc::NetworkInfo::NatPortAllocationStrategy)sm_LocalNatInfo.GetPortAllocationStrategy());
		info.SetNatPortMappingMethod((rgsc::NetworkInfo::NatPortMappingMethod)sm_LocalNatInfo.GetPortMappingMethod());
		info.SetNatType((rgsc::NetworkInfo::NatType) sm_LocalNatInfo.GetNatType());
		info.SetPortIncrement(sm_LocalNatInfo.GetPortIncrement());

		char privateAddr[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};
		char publicAddr[netSocketAddress::MAX_STRING_BUF_SIZE] = {0};

		if(netRelay::GetSocket() && netRelay::GetSocket()->GetAddress().IsValid())
		{
			netRelay::GetSocket()->GetAddress().Format(privateAddr, true);
		}
		else
		{
			netIpAddress privateIpAddress;
			if(netHardware::GetLocalIpAddress(&privateIpAddress))
			{
				privateIpAddress.Format(privateAddr);
			}
		}

		if(netRelay::GetSocket() && netRelay::GetMyRelayAddress().GetTargetAddress().IsValid())
		{
			netRelay::GetMyRelayAddress().GetTargetAddress().Format(publicAddr, true);
		}
		else
		{
			netIpAddress publicIpAddress;
			if(netHardware::GetPublicIpAddress(&publicIpAddress))
			{
				publicIpAddress.Format(publicAddr);
			}
		}

		info.SetPrivateAddress(privateAddr);
		info.SetPublicAddress(publicAddr);
		info.SetUdpTimeoutSec(sm_LocalNatInfo.GetUdpTimeoutSec());
		info.SetUdpTimeoutState((rgsc::NetworkInfo::UdpTimeoutState)sm_LocalNatInfo.GetUdpTimeoutState());
		info.SetuPnPState((rgsc::NetworkInfo::UpNpState) netNatUpNp::GetUpNpInfo().GetState());

		/*
		SCUI Rules for PCP
		If PCP State is:
		-  "0" Don't show the PCP information at all.
		-  "1" Show "NAT-PMP / PCP: Detecting..."
		-  "2" Show "NAT-PMP / PCP: Enabled"
		-  "3" Show "NAT-PMP / PCP: Disabled"
		-  "4" Show "NAT-PMP: Enabled"
		-  "5" Show "PCP (Port Control Protocol): Enabled"
		*/

		netNatPcpInfo::State pcpState = netNatPcp::GetPcpInfo().GetState();
		netNatPcpInfo::ReserveMethod pcpMethod = netNatPcp::GetPcpInfo().GetReserveMethod();

		switch (pcpState)
		{
		case rage::netNatPcpInfo::NAT_PCP_UNATTEMPTED:
			{
				info.SetPcpState(rgsc::NetworkInfo::NAT_PCP_UNATTEMPTED);
			}
			break;
		case rage::netNatPcpInfo::NAT_PCP_IN_PROGRESS:
			{
				info.SetPcpState(rgsc::NetworkInfo::NAT_PCP_IN_PROGRESS);
			}
			break;
		case rage::netNatPcpInfo::NAT_PCP_SUCCEEDED:
			{
				switch (pcpMethod)
				{
				case rage::netNatPcpInfo::NAT_PCP_METHOD_NONE:
					info.SetPcpState(rgsc::NetworkInfo::NAT_PCP_SUCCEEDED);
					break;
				case rage::netNatPcpInfo::NAT_PCP_METHOD_PMP:
					info.SetPcpState(rgsc::NetworkInfo::NAT_PCP_METHOD_PMP);
					break;
				case rage::netNatPcpInfo::NAT_PCP_METHOD_PCP:
					info.SetPcpState(rgsc::NetworkInfo::NAT_PCP_METHOD_PCP);
					break;
				case rage::netNatPcpInfo::NAT_PCP_NUM_METHODS:
					break;
				}
			}
			break;
		case rage::netNatPcpInfo::NAT_PCP_FAILED:
			{
				if(netNatUpNp::GetUpNpInfo().GetState() == netNatUpNpInfo::NAT_UPNP_SUCCEEDED)
				{
					// don't show that PCP failed if UPnP succeeded
					info.SetPcpState(rgsc::NetworkInfo::NAT_PCP_UNATTEMPTED);
				}
				else
				{
					info.SetPcpState(rgsc::NetworkInfo::NAT_PCP_FAILED);
				}
			}
			break;
		case rage::netNatPcpInfo::NAT_PCP_NUM_STATES:
			break;
		}
		
#if GTA_VERSION
		if(g_rlPc.GetNetworkInterface())
		{
			g_rlPc.GetNetworkInterface()->SetNetworkInfo(&info);
		}
#else
		if(g_rlPc.GetNetworkInterface().IsValid())
		{
			g_rlPc.GetNetworkInterface().SetNetworkInfo(&info);
		}
#endif // GTA_VERSION
	}
#endif // #if RSG_PC && !__RGSC_DLL
}

void
netNatDetector::UpdateReport()
{
	// only allow one telemetry report per game launch
	static bool s_ReportComplete = false;

	if(s_ReportComplete)
	{
		return;
	}

	const bool uPnPcompleted = netNatUpNp::IsInitialWorkCompleted();
	const bool pcpCompleted = netNatPcp::IsInitialWorkCompleted();
	const bool natDiscoveryCompleted = (s_NatTypeDiscoveryState == NAT_TYPE_STATE_SUCCEEDED) || (s_NatTypeDiscoveryState == NAT_TYPE_STATE_FAILED);

	const bool completed = uPnPcompleted && pcpCompleted && natDiscoveryCompleted && netRelay::GetMyRelayAddress().IsRelayServerAddr();

	s_NatReportTimeout.Update();

	if(completed || s_NatReportTimeout.IsTimedOut())
	{
		sm_LocalNatInfo.UpdateRelayMappedAddr();

		UpdateRgsc();

#if !__NO_OUTPUT
		sm_LocalNatInfo.DumpInfo();
		netNatPcp::GetPcpInfo().DumpInfo();
		netNatUpNp::GetUpNpInfo().DumpInfo();
#endif

		netNatTelemetryErrorCode errCode = NET_NAT_TELEMETRY_ERR_NONE;

		if(!netRelay::GetSocket() || !netRelay::GetSocket()->GetAddress().IsValid())
		{
			netIpAddress privateIpAddress;
			if(!netHardware::GetLocalIpAddress(&privateIpAddress) || !privateIpAddress.IsValid())
			{
				errCode = NET_NAT_TELEMETRY_ERR_NO_PRIVATE_IP;
			}
			else
			{
				errCode = NET_NAT_TELEMETRY_ERR_NO_PRIVATE_PORT;
			}
		}
		else if(!netRelay::GetMyRelayAddress().IsRelayServerAddr())
		{
			if(!netRelay::GetRelayServerAddress().IsValid())
			{
				errCode = NET_NAT_TELEMETRY_ERR_NO_RELAY_ADDR;
			}
			else
			{
				errCode = NET_NAT_TELEMETRY_ERR_NO_MAPPED_ADDR;
			}
		}
		else if(!completed)
		{
			errCode = NET_NAT_TELEMETRY_ERR_INCOMPLETE;
		}

		if(netNatDetector::sm_AllowNatInfoTelemetry)
		{
			u32 mtu = 0;
			if(!netHardware::GetMtu(mtu))
			{
				mtu = 0;
			}

			rlStandardMetrics::NatInfoMetric metric(NAT_TYPE_STATE_SUCCEEDED == s_NatTypeDiscoveryState,
													sm_LocalNatInfo,
													netNatPcp::GetPcpInfo(),
													netNatUpNp::GetUpNpInfo(),
													(int)netHardware::GetNatType(),
													(int)errCode,
													mtu,
													netHardware::GetReachability());
			rlTelemetry::Write(rlPresence::GetActingUserIndex(), metric);

#if !__NO_OUTPUT
			char natInfoJson[1024] = {0};
			RsonWriter rw(natInfoJson, RSON_FORMAT_JSON);
			metric.Write(&rw);
			netDebug3("NAT Info Telemetry: %s", rw.ToString());
#endif
		}

		s_ReportComplete = true;
	}
}

void
netNatDetector::Update()
{
#if NET_DISABLE_NAT_DETECTION
	return;
#else
	SYS_CS_SYNC(m_Cs);

	if(m_Initialized)
	{
		UpdateNatTypeDiscovery();
		netNatPcp::Update();

		if(netNatPcp::IsInitialWorkCompleted() && !netNatUpNp::IsInitialized())
		{
			// we only fallback to UPnP if PCP failed to reserve an address
			if(netNatPcp::GetPcpInfo().GetReservedAddress().IsValid())
			{
				netNatUpNp::SetAllowUpNpPortForwarding(false);
			}
			netNatUpNp::Init();
		}

		netNatUpNp::Update();
		UpdateReport();
	}
#endif
}

void
netNatDetector::AddDelegate(netNatDetector::Delegate* dlgt)
{
	sm_Delegator.AddDelegate(dlgt);
}

void
netNatDetector::RemoveDelegate(netNatDetector::Delegate* dlgt)
{
	sm_Delegator.RemoveDelegate(dlgt);
}

void
netNatDetector::DispatchEvent(netNatEvent* e)
{
	sm_Delegator.Dispatch(*e);
}

netNatType
netNatDetector::GetLocalNatType()
{
	return sm_LocalNatType;
}

const netNatInfo&
netNatDetector::GetLocalNatInfo()
{
	return sm_LocalNatInfo;
}

#if __RGSC_DLL
void 
netNatDetector::SetLocalNatInfo(const netNatInfo& info)
{
	sm_LocalNatInfo = info;
}
#endif

void 
netNatDetector::SetAllowUdpTimeoutMeasurement(const bool allowUdpTimeoutMeasurement)
{
	if(sm_AllowUdpTimeoutMeasurement != allowUdpTimeoutMeasurement)
	{
		netDebug("SetAllowUdpTimeoutMeasurement :: %s", allowUdpTimeoutMeasurement ? "true" : "false");
		sm_AllowUdpTimeoutMeasurement = allowUdpTimeoutMeasurement;
	}
}

void 
netNatDetector::SetAllowAdjustableUdpSendInterval(const bool allowAdjustableUdpSendInterval)
{
	if(sm_AllowAdjustableUdpSendInterval != allowAdjustableUdpSendInterval)
	{
		netDebug("SetAllowAdjustableUdpSendInterval :: %s", allowAdjustableUdpSendInterval ? "true" : "false");
		sm_AllowAdjustableUdpSendInterval = allowAdjustableUdpSendInterval;
	}
}

void 
netNatDetector::SetAllowNatInfoTelemetry(const bool allowNatInfoTelemetry)
{
	if(sm_AllowNatInfoTelemetry != allowNatInfoTelemetry)
	{
		netDebug("SetAllowNatInfoTelemetry :: %s", allowNatInfoTelemetry ? "true" : "false");
		sm_AllowNatInfoTelemetry = allowNatInfoTelemetry;
	}
}

netNatTypeDiscoveryState
netNatDetector::GetNatDiscoveryState()
{
	return s_NatTypeDiscoveryState;
}

void 
netNatDetector::SetNatDiscoveryState(netNatTypeDiscoveryState state)
{
	if(state == NAT_TYPE_STATE_FAILED)
	{
		sm_LocalNatType = NET_NAT_UNKNOWN;
		sm_LocalNatInfo.SetUdpTimeoutState(NAT_UDP_TIMEOUT_FAILED);
	}

	bool stateChanged = s_NatTypeDiscoveryState != state;
	s_NatTypeDiscoveryState = state;

	if(stateChanged)
	{
		sm_LocalNatInfo.SetDirty();
	}
}

bool
netNatDetector::HasNatTypeDiscoveryFailed()
{
	return s_NatTypeDiscoveryState == NAT_TYPE_STATE_FAILED;
}

bool
netNatDetector::HasNatTypeDiscoveryCompleted()
{
	return HasNatTypeDiscoveryFailed() || (s_NatTypeDiscoveryState > NAT_TYPE_STATE_DISCOVERING_NAT_TYPE);
}

bool
netNatDetector::IsReadyForMultiplayer()
{
	// prefer to have our NAT type before entering multiplayer, but don't block multiplayer if detection fails.
	return netNatDetector::HasNatTypeDiscoveryCompleted();
}

void 
netNatDetector::SetNonDeterministic()
{
	if(s_NatTypeDiscoveryState == NAT_TYPE_STATE_SUCCEEDED)
	{
		sm_LocalNatInfo.SetNonDeterministic();
	}
}

void
netNatDetector::UpdateNatTypeDiscovery()
{
	// TODO: NS - use SignedOnilne/SignedOffline/SignedOut events instead of doing this every frame
	bool isAnyOnline = rlRos::IsAnyOnline();

	if(!isAnyOnline)
	{
		sm_LocalNatType = NET_NAT_UNKNOWN;
		sm_LocalNatInfo.Clear();
		s_NatTypeServerList.Clear();
		s_PortMapper.Clear();
		s_PortTimoutMeasurer.Clear();
		s_NatDiscoveryTimeout.Clear();
		s_NatReportTimeout.Clear();
		SetNatDiscoveryState(NAT_TYPE_STATE_WAIT_FOR_ROS_ONLINE);
	}

	switch(s_NatTypeDiscoveryState)
	{
	case NAT_TYPE_STATE_WAIT_FOR_ROS_ONLINE:
		// wait until we have our CreateTicketResponse before using server resources
		if(isAnyOnline)
		{
#if RSG_PC && !__RGSC_DLL
			// send the 'adjustable UDP send interval' tunable to the DLL
			rgsc::NetworkInfo info;
			info.SetAllowAdjustablePingInterval(sm_AllowAdjustableUdpSendInterval);
#if GTA_VERSION
			if(g_rlPc.GetNetworkInterface())
			{
				g_rlPc.GetNetworkInterface()->SetNetworkInfo(&info);
			}
#else
			if(g_rlPc.GetNetworkInterface().IsValid())
			{
				g_rlPc.GetNetworkInterface().SetNetworkInfo(&info);
			}
#endif // GTA_VERSION
#endif // #if RSG_PC && !__RGSC_DLL

			s_NatReportTimeout.Clear();
			s_NatReportTimeout.InitMilliseconds(MAX_REPORT_WAIT_TIME_MS);

#if RSG_GGP
			// GGP guarantees specific NAT/firewall behaviours. Skip detection since it would
			// require binding to ports in a wider range than available by the platform.
			sm_LocalNatType = NET_NAT_MODERATE;
			sm_LocalNatInfo.Init(true,
								 sm_LocalNatType,
								 NET_NAT_PMM_ENDPOINT_INDEPENDENT,
								 NET_NAT_FM_PORT_AND_ADDRESS_RESTRICTED,
								 NET_NAT_PAS_PORT_PRESERVING,
								 (u8)0,
								 NAT_UDP_TIMEOUT_SUCCEEDED,
								 60);
			SetNatDiscoveryState(NAT_TYPE_STATE_SUCCEEDED);
#else
			SetNatDiscoveryState(NAT_TYPE_STATE_GET_SERVER_LIST);
#endif
		}
		break;
	case NAT_TYPE_STATE_GET_SERVER_LIST:
		{
			const unsigned MAX_WAIT_TIME_MS = 30 * 1000;
			s_NatDiscoveryTimeout.Clear();
			s_NatDiscoveryTimeout.InitMilliseconds(MAX_WAIT_TIME_MS);
			s_NatDiscoveryTimeout.SetLongFrameThreshold(netTimeout::DEFAULT_LONG_FRAME_THRESHOLD_MS);
			s_NatDiscoveryTimeout.Update();
			SetNatDiscoveryState(NAT_TYPE_STATE_GETTING_SERVER_LIST);
		}
		break;
	case NAT_TYPE_STATE_GETTING_SERVER_LIST:
		{
			s_NatDiscoveryTimeout.Update();

			if(rlRos::GetGeoLocInfo(&s_NatTypeServerList))
			{
				SetNatDiscoveryState(NAT_TYPE_STATE_HAVE_SERVER_LIST);
			}
			else if(s_NatDiscoveryTimeout.IsTimedOut())
			{
				netDebug("rlRos::GetNatDiscoveryServers() timed out.");
				SetNatDiscoveryState(NAT_TYPE_STATE_FAILED);
			}
		}
		break;
	case NAT_TYPE_STATE_HAVE_SERVER_LIST:

#if !RSG_FINAL || defined(RSG_LEAN_CLIENT)
		if(PARAM_netnattype.Get())
		{
			netDebug("-netnattype is enabled, setting NAT type via command line");
			const char* fakeNatType = NULL;
			PARAM_netnattype.Get(fakeNatType);

			if(stricmp(fakeNatType, "Open") == 0)
			{
				sm_LocalNatType = NET_NAT_OPEN;
			}
			else if(stricmp(fakeNatType, "Moderate") == 0)
			{
				sm_LocalNatType = NET_NAT_MODERATE;
			}
			else if(stricmp(fakeNatType, "Strict") == 0)
			{
				sm_LocalNatType = NET_NAT_STRICT;
			}
			else if(stricmp(fakeNatType, "Random") == 0)
			{
				sm_LocalNatType = (netNatType)s_NatTypeRng.GetRanged(NET_NAT_OPEN, NET_NAT_STRICT);
			}
			else
			{
				sm_LocalNatType = NET_NAT_UNKNOWN;
			}

			sm_LocalNatInfo.Init(true,
						sm_LocalNatType,
						(sm_LocalNatType == NET_NAT_STRICT) ? NET_NAT_PMM_ENDPOINT_DEPENDENT : NET_NAT_PMM_ENDPOINT_INDEPENDENT,
						(sm_LocalNatType == NET_NAT_OPEN) ? NET_NAT_FM_OPEN : NET_NAT_FM_PORT_AND_ADDRESS_RESTRICTED,
						NET_NAT_PAS_PORT_RANDOM,
						(u8)0,
						NAT_UDP_TIMEOUT_UNATTEMPTED,
						0);

			SetNatDiscoveryState(NAT_TYPE_STATE_MEASURE_BINDING_TIMEOUT);

		}
		else
#endif

		if(s_PortMapper.Init(nullptr, netNatPortMapper::MIN_NAT_TYPE_DISCOVERY_SERVERS, netNatPortMapper::MAX_NAT_TYPE_DISCOVERY_SERVERS))
		{
			SetNatDiscoveryState(NAT_TYPE_STATE_WAIT_FOR_PORT_MAPPER);
		}
		else
		{
			s_PortMapper.Clear();
			SetNatDiscoveryState(NAT_TYPE_STATE_FAILED);
		}
		break;
	case NAT_TYPE_STATE_WAIT_FOR_PORT_MAPPER:
		{
			const unsigned MAX_WAIT_TIME_MS = 10 * 1000;
			s_NatDiscoveryTimeout.Clear();
			s_NatDiscoveryTimeout.InitMilliseconds(MAX_WAIT_TIME_MS);
			s_NatDiscoveryTimeout.SetLongFrameThreshold(netTimeout::DEFAULT_LONG_FRAME_THRESHOLD_MS);
			s_NatDiscoveryTimeout.Update();
			SetNatDiscoveryState(NAT_TYPE_STATE_WAITING_FOR_PORT_MAPPER);
		} 
		break;
	case NAT_TYPE_STATE_WAITING_FOR_PORT_MAPPER:
		{
			s_NatDiscoveryTimeout.Update();

			if(s_PortMapper.IsReadyToSend())
			{
				for(unsigned i = 0; i < s_PortMapper.GetNumServersAvailable(); ++i)
				{
					s_PortMapper.MapPort(i, s_MappedAddrs[i], &s_MappedAddrStatus[i]);
				}

				// only need one alt port test
				s_PortMapper.MapAltPort(0, s_MappedAddrsAltPort[0], &s_MappedAddrStatusAltPort[0]);

				SetNatDiscoveryState(NAT_TYPE_STATE_DISCOVERING_NAT_TYPE);
			}
			else if(s_NatDiscoveryTimeout.IsTimedOut())
			{
				netDebug("s_PortMapper.IsReadyToSend() timed out.");
				s_PortMapper.Clear();
				SetNatDiscoveryState(NAT_TYPE_STATE_FAILED);
			}
		}
		break;
	case NAT_TYPE_STATE_DISCOVERING_NAT_TYPE:
		{
			s_PortMapper.Update();

			bool pending = false;
			unsigned numSucceeded = 0;

			if(s_MappedAddrStatusAltPort[0].Pending())
			{
				pending = true;
			}

			for(unsigned i = 0; i < s_PortMapper.GetNumServersAvailable(); ++i)
			{
				if(s_MappedAddrStatus[i].Pending())
				{
					pending = true;
					break;
				}
				else if(s_MappedAddrStatus[i].Succeeded())
				{
					numSucceeded++;
				}
			}

			if(!pending)
			{
				if((numSucceeded < netNatPortMapper::MIN_NAT_TYPE_DISCOVERY_SERVERS))
				{
					netDebug("NAT type discovery failed. Possibly UDP blocked.");

					s_PortMapper.Clear();
					SetNatDiscoveryState(NAT_TYPE_STATE_FAILED);
				}
				else
				{
					bool natDetected = false;
					bool portPreserved = false;
					bool endpointDependentMapping = false;
					int portIncrement = 0;
					bool portContiguous = false;
					bool portRestrictedFiltering = s_MappedAddrStatusAltPort[0].Failed();

					netDebug("NAT port mapper socket bound to address: " NET_ADDR_FMT, NET_ADDR_FOR_PRINTF(s_PortMapper.GetSocketAddress()));

					// Note: the order in which the packets get sent to the socket is not necessarily the same order in 
					// which they reach the NAT. Analyze the results in a way that doesn't depend on the order.
					netSocketAddress tmpAddr;
					for(unsigned i = 0; i < s_PortMapper.GetNumServersAvailable(); ++i)
					{
						netDebug("NAT port mapping %d of %d: " NET_ADDR_FMT, i + 1, s_PortMapper.GetNumServersAvailable(), NET_ADDR_FOR_PRINTF(s_MappedAddrs[i]));

						if(s_MappedAddrs[i].IsValid())
						{
							// if all mapped addresses are the same, then the NAT does endpoint 
							// independent mapping. Otherwise it's endpoint dependent.
							if(!tmpAddr.IsValid())
							{
								tmpAddr = s_MappedAddrs[i];
							}
							else if(tmpAddr != s_MappedAddrs[i])
							{
								endpointDependentMapping = true;
							}

							// if any mapped address is different than the private address,
							// then we're behind a NAT.
							if(s_MappedAddrs[i] != s_PortMapper.GetSocketAddress())
							{
								natDetected = true;
							}

							// if any of the mapped addresses have the same port as the
							// private address, then we're behind a port-preserving NAT.
							if(s_MappedAddrs[i].GetPort() == s_PortMapper.GetSocketAddress().GetPort())
							{
								portPreserved = true;
							}
						}
					}

					if(portRestrictedFiltering)
					{
						netDebug("We didn't receive a pong from the relay server's alternate port. The NAT appears to do port and address restricted filtering.");
					}
					else
					{
						netDebug("Received a pong from the relay server's alternate port. The NAT does not appear to do port and address restricted filtering.");
					}

					if(natDetected)
					{
						netDebug("Private IP and port are different than the public IP and port. Looks like we're behind a NAT.");
						if(portPreserved)
						{
							netDebug("Private port is the same as the public port. Looks like we're behind a port-preserving NAT.");
						}
						else
						{
							netDebug("Private port is always different than the public port. We may be behind a non-port-preserving NAT or something else has the preserved port open.");
						}

						if(!endpointDependentMapping)
						{
							netDebug("NAT reused the same ports for different endpoints. We appear to be behind an Endpoint Independent Mapping NAT (non-symmetric).");

							if(portRestrictedFiltering)
							{
								sm_LocalNatType = NET_NAT_MODERATE;
							}
							else
							{
								sm_LocalNatType = NET_NAT_OPEN;
							}
						}
						else
						{
							netDebug("NAT opened different ports for different endpoints. We appear to be behind a Strict/Symmetric/Endpoint Dependent Mapping NAT.");
							sm_LocalNatType = NET_NAT_STRICT;

							u16 port1 = s_MappedAddrs[0].GetPort();
							u16 port2 = s_MappedAddrs[1].GetPort();
							u16 port3 = s_MappedAddrs[2].GetPort();

							int difference12 = abs(port1 - port2);
							int difference13 = abs(port1 - port3);
							int difference23 = abs(port2 - port3);

							if((difference12 == 1) ||
							   (difference13 == 1) ||
							   (difference23 == 1)) 
							{
								portIncrement = 1;
							}
							else if((difference12 == 2) ||
									(difference13 == 2) ||
									(difference23 == 2))
							{
								portIncrement = 2;
							}

							portContiguous = (portIncrement == 1) || (portIncrement == 2);

							if(portContiguous)
							{
								netDebug("Symmetric NAT appears to have a contiguous port allocation strategy with a port increment of %d.", portIncrement);
							}
							else
							{
								netDebug("Symmetric NAT appears to have a random/unpredictable port allocation strategy.");
							}
						}
					}
					else
					{
						netDebug("Private IP and port are the same as the public IP and port. Looks like we're not behind a NAT.");

						if(portRestrictedFiltering)
						{
							// probably firewalled even though there is no NAT
							sm_LocalNatType = NET_NAT_MODERATE;
						}
						else
						{
							sm_LocalNatType = NET_NAT_OPEN;
						}
					}

					netNatType natType = sm_LocalNatType;

					netNatPortAllocationStrategy portAllocationStrategy = NET_NAT_PAS_UNKNOWN;
					if(portPreserved)
					{
						portAllocationStrategy = NET_NAT_PAS_PORT_PRESERVING;
					}
					else if((natType == NET_NAT_STRICT) && portContiguous)
					{
						portAllocationStrategy = NET_NAT_PAS_PORT_CONTIGUOUS;
					}
					else
					{
						portAllocationStrategy = NET_NAT_PAS_PORT_RANDOM;
					}

					sm_LocalNatInfo.Init(natDetected,
										 sm_LocalNatType,
										 (sm_LocalNatType == NET_NAT_STRICT) ? NET_NAT_PMM_ENDPOINT_DEPENDENT : NET_NAT_PMM_ENDPOINT_INDEPENDENT,
										 portRestrictedFiltering ? NET_NAT_FM_PORT_AND_ADDRESS_RESTRICTED : NET_NAT_FM_OPEN,
										 portAllocationStrategy,
										 (u8)portIncrement,
										 NAT_UDP_TIMEOUT_UNATTEMPTED,
										 0);

					s_PortMapper.Clear();

					SetNatDiscoveryState(NAT_TYPE_STATE_MEASURE_BINDING_TIMEOUT);
				}
			}
		}
		break;
	case NAT_TYPE_STATE_MEASURE_BINDING_TIMEOUT:
		{
			if(!netNatDetector::sm_AllowUdpTimeoutMeasurement || !s_PortTimoutMeasurer.Init())
			{
				s_PortTimoutMeasurer.Clear();
				sm_LocalNatInfo.SetUdpTimeoutState(NAT_UDP_TIMEOUT_FAILED);
				SetNatDiscoveryState(NAT_TYPE_STATE_SUCCEEDED);
			}
			else
			{
				sm_LocalNatInfo.SetUdpTimeoutState(NAT_UDP_TIMEOUT_IN_PROGRESS);
				SetNatDiscoveryState(NAT_TYPE_STATE_MEASURING_BINDING_TIMEOUT);
			}
		}
		break;
	case NAT_TYPE_STATE_MEASURING_BINDING_TIMEOUT:
		{
			s_PortTimoutMeasurer.Update();
			unsigned sendInterval = s_PortTimoutMeasurer.GetUdpTimeoutSec();
			sm_LocalNatInfo.SetUdpTimeoutSec((u16)sendInterval);

			if(!s_PortTimoutMeasurer.Pending())
			{
				if(s_PortTimoutMeasurer.Succeeded())
				{
					sm_LocalNatInfo.SetUdpTimeoutState(NAT_UDP_TIMEOUT_SUCCEEDED);
				}
				else
				{
					netDebug3("Failed to measure NAT port binding timeout.");
					sm_LocalNatInfo.SetUdpTimeoutState(NAT_UDP_TIMEOUT_FAILED);
				}

				s_PortTimoutMeasurer.Clear();
				SetNatDiscoveryState(NAT_TYPE_STATE_SUCCEEDED);				
			}
		}
		break;
	case NAT_TYPE_STATE_SUCCEEDED:
		break;
	case NAT_TYPE_STATE_FAILED:
		break;
	}
}

} // namespace rage
