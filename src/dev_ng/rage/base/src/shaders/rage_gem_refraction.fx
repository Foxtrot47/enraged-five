#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_samplers.fxh"

float4 gemColor = {0.0f, 0.7f, 0.0f, 0.7f};

struct vertexInput 
{
    float4 pos				: POSITION;
    float3 normal 			: NORMAL;
    float2 uv				: TEXCOORD0;
};

struct vertexOutput  
{
   DECLARE_POSITION( Pos)
   float4 worldPos:	TEXCOORD0;
   float3 normal:	TEXCOORD1;
   float3 viewVec:	TEXCOORD2;
   float2 uv:		TEXCOORD4;
};

float3 ComputeLightColor(float3 pos, float3 normal, int index)
{
	float3 lightColor;
	{
		float4 pointLight;
		pointLight.xyz = (gLightPosDir[index].xyz - pos);
		pointLight.w = length(pointLight.xyz);
		if (pointLight.w)
			pointLight.xyz /= pointLight.w;
			
		float4 dirLight;
		dirLight.xyz = -gLightPosDir[index].xyz;
		dirLight.w = 0.0;

		float4 lightPosDir = lerp(pointLight, dirLight, gLightType[index]);
		
		float3 L = lightPosDir.xyz;

		// Compute falloff term
		float falloff = 1.0 - min((lightPosDir.w / gLightPosDir[index].w), 1.0);
		float lightIntensity = gLightColor[index].w;
		if ( lightIntensity < 0.f ) 
		{
			falloff *= -lightIntensity;		// Allow oversaturation
		}
		else 
		{
			falloff = min(falloff * lightIntensity, 1.0);
		}

		float3 light = max(dot(normal,L), 0.0);

		// Sum up color contribution of light sources
		lightColor = gLightColor[index].rgb * light.y * falloff;
	}
	return lightColor;
}

vertexOutput VS_Gem( vertexInput IN ) 
{	
	vertexOutput OUT;
	OUT.Pos = mul(IN.pos, gWorldViewProj);
	OUT.worldPos = mul(IN.pos, gWorld);
	OUT.normal = normalize(IN.normal);//mul(IN.normal, (float3x3)gWorldInverseTranspose);
	OUT.viewVec = (float3)-mul(IN.pos, gWorldView);
	OUT.uv = IN.uv;

	return OUT;
}

float4 PS_Gem( vertexOutput IN ) : COLOR
{
	float3 light0 = ComputeLightColor(IN.worldPos.xyz, normalize(IN.normal), 0);
	float3 lightColor = light0;
	
	#if __SHADERMODEL >= 30
	float3 light1 = ComputeLightColor(IN.worldPos.xyz, normalize(IN.normal), 1);
	float3 light2 = ComputeLightColor(IN.worldPos.xyz, normalize(IN.normal), 2);		
	lightColor += light1 + light2;
	#endif
	
	return saturate(float4(lightColor, 1) * gemColor);
}

technique draw
{
	pass p0 
	{       
		CullMode = None; 
		VertexShader = compile VERTEXSHADER VS_Gem();
		PixelShader  = compile PIXELSHADER PS_Gem();
	}
}

technique drawskinned
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_Gem();
		PixelShader  = compile PIXELSHADER PS_Gem();
	}
}
