#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_gi.fxh"

BeginSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
	string UIName="Diffuse Texture";
ContinueSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
EndSampler;

BeginSampler(sampler2D,ProbeTexture,ProbeSampler,ProbeTex)
	string UIName="Probes Texture";
ContinueSampler(sampler2D,ProbeTexture,ProbeSampler,ProbeTex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
//	MipFilter = POINT;
//	MinFilter = POINT;
//	MagFilter = POINT;
EndSampler;

BeginSampler(sampler2D,SH_R_0_Texture,SH_R_0_Sampler,SH_R_0_Tex)
	string UIName="SH_R_0_ Texture";
ContinueSampler(sampler2D,SH_R_0_Texture,SH_R_0_Sampler,SH_R_0_Tex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
//	MipFilter = LINEAR;
//	MinFilter = LINEAR;
//	MagFilter = LINEAR;
	MIN_POINT_MAG_POINT_MIP_POINT;
EndSampler;

BeginSampler(sampler2D,SH_G_0_Texture,SH_G_0_Sampler,SH_G_0_Tex)
	string UIName="SH_G_0_ Texture";
ContinueSampler(sampler2D,SH_G_0_Texture,SH_G_0_Sampler,SH_G_0_Tex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
//	MipFilter = LINEAR;
//	MinFilter = LINEAR;
//	MagFilter = LINEAR;
	MIN_POINT_MAG_POINT_MIP_POINT;
EndSampler;

BeginSampler(sampler2D,SH_B_0_Texture,SH_B_0_Sampler,SH_B_0_Tex)
	string UIName="SH_B_0_ Texture";
ContinueSampler(sampler2D,SH_B_0_Texture,SH_B_0_Sampler,SH_B_0_Tex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
//	MipFilter = LINEAR;
//	MinFilter = LINEAR;
//	MagFilter = LINEAR;
	MIN_POINT_MAG_POINT_MIP_POINT;
EndSampler;

BeginSampler(samplerCUBE,SH_Cube_Texture,SH_Cube_Sampler,SH_Cube_Tex)
	string UIName="SH_Cube Texture";
ContinueSampler(samplerCUBE,SH_Cube_Texture,SH_Cube_Sampler,SH_Cube_Tex)
EndSampler;

BeginSampler(sampler2D, SHTexture, SHSampler, SHTex)
	string	UIName = "SH Texture";
ContinueSampler(sampler2D, SHTexture, SHSampler, SHTex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_POINT_MAG_POINT_MIP_POINT;
EndSampler;

BeginConstantBufferDX10(rage_debug1)
SHARED bool debugShader : debugShader REGISTER(b3);
EndConstantBufferDX10(rage_debug1)

struct inputVertex 
{
	float3 pos	: POSITION;
	float2 tc	: TEXCOORD0;
    float3 normal		: NORMAL;
};

struct inputVertexSkinned
{
	float3 pos	: POSITION;
	float4 weight		  : BLENDWEIGHT;
	index4 blendindices	  : BLENDINDICES;
	float2 tc	: TEXCOORD0;
    float3 normal		: NORMAL;
};

struct outputVertex 
{
	DECLARE_POSITION( pos )
	float2 tc	: TEXCOORD0;
	float3 norm : TEXCOORD1;
	float3 position : TEXCOORD2;
};

//------------------------------------------------------------------------------
// Unskinned vertex shader
//------------------------------------------------------------------------------
outputVertex VS_Transform(inputVertex IN) 
{
	outputVertex OUT;
	OUT.pos = mul(float4(IN.pos,1), gWorldViewProj);
	OUT.tc = IN.tc;
	OUT.norm = mul(IN.normal, (float3x3)gWorld);
	OUT.position = mul(float4(IN.pos,1), gWorld).xyz;
	return OUT;
}//VS_Transform

//------------------------------------------------------------------------------
// Skinned vertex shader
//------------------------------------------------------------------------------
outputVertex VS_TransformSkin(inputVertexSkinned IN) 
{
	outputVertex OUT;
	float3 skinPos = rageSkinTransform(IN.pos, ComputeSkinMtx( IN.blendindices, IN.weight ));
	// Finish transform
	OUT.pos = mul(float4(skinPos,1), gWorldViewProj);
	if (debugShader) {
		OUT.tc.x = IN.tc.y;
		OUT.tc.y = IN.tc.x;
	}
	else
		OUT.tc = IN.tc;
	OUT.norm = mul(IN.normal, (float3x3)gWorld);
	OUT.position = mul(float4(skinPos,1), gWorld).xyz;
	return OUT;
}//VS_TransformSkin

//------------------------------------------------------------------------------
// Lookup from the cube map for the nearest sample
//------------------------------------------------------------------------------
float4 GetLightingColor(float3 norm, float3 position)
{
	// Initial tex coords
	float3 tx = giGetCubeMapTexFace(norm);

	// Convert texture coordinates into 0-cubemapsiz
	float2 texSize = {texWidth, texHeight};
	tx.xy = tx.xy*cubeSize;
	tx.xy = tx.xy/texSize;

	// Figure out texture offset
	float2 offset = giGetCubeFaceOffset(position, tx.z);

	// Sample from the "cubemap" texture
	float4 oColor = tex2D(ProbeSampler, tx.xy+offset);

	// All done
	return oColor;
}//GetLightingColor

//------------------------------------------------------------------------------
// Compute indirect lighting from spherical harmonics
//------------------------------------------------------------------------------
float3 GetLightingIndirect(float3 norm, float3 pos)
{
	float3 indirect = 0;

	// Right now only one sample...
	//float2 shCoord = giConvertFromPosToSHTex(pos);
	float3 sPos = giConvertFromPositionToSample(pos);
	
	// Lower Z bound
	float3 lPos = sPos;
	lPos.z = floor(lPos.z);
	float3 nPos = saturate(lPos/float3(samplesWidth-1, samplesHeight-1, samplesDepth-1));
	float2 shCoordL;
	shCoordL.x = nPos.x;
	shCoordL.y = nPos.y/(samplesDepth);
	shCoordL.y += (lPos.z/(samplesDepth));

	// Upper Z bound
	float3 uPos = sPos;
	uPos.z = ceil(uPos.z);
	nPos = saturate(uPos/float3(samplesWidth-1, samplesHeight-1, samplesDepth-1));
	float2 shCoordU;
	shCoordU.x = nPos.x;
	shCoordU.y = nPos.y/(samplesDepth);
	shCoordU.y += (uPos.z/(samplesDepth));

	// lerp factor
	float zLerp = frac(sPos.z);

	//[unroll]
	//for (int i = 0; i < /*SH_COEFF_VECTORS*/ 1; i++)
	{
		// Get the SH coefficients from this point in space
		//float4 cr = ShVolumeR[i].Sample(volumeFilter, shCoord);
		//float4 cr = tex2D(SH_R_0_Sampler, shCoord);
		float4 cr = lerp(tex2D(SH_R_0_Sampler, shCoordL),
						 tex2D(SH_R_0_Sampler, shCoordU),
						 zLerp);
		//float4 cg = ShVolumeG[i].Sample(volumeFilter, shCoord);
		//float4 cg = tex2D(SH_G_0_Sampler, shCoord);
		float4 cg = lerp(tex2D(SH_G_0_Sampler, shCoordL),
						 tex2D(SH_G_0_Sampler, shCoordU),
						 zLerp);
		//float4 cb = ShVolumeB[i].Sample(volumeFilter, shCoord);
		//float4 cb = tex2D(SH_B_0_Sampler, shCoord);
		float4 cb = lerp(tex2D(SH_B_0_Sampler, shCoordL),
						 tex2D(SH_B_0_Sampler, shCoordU),
						 zLerp);

		// Get the SH transfer function coefficients for the normal direction
		//float4 sh = ShCube[i].Sample(volumeFilter, normal);
		float3 nrm = normalize(-norm);
		float4 sh = texCUBE(SH_Cube_Sampler, nrm);

		// The indirect lighting is the integral over the environment sphere
		// which is a dot-product in SH space.
		indirect.r += dot(cr, sh);
		indirect.g += dot(cg, sh);
		indirect.b += dot(cb, sh);
	}

	return indirect;
}//GetLightingIndirect

//------------------------------------------------------------------------------
// Get the color from a probe. Takes the cube face, the pre-computed offset
// (use GetCubeFaceOffset) and the 0-N texture coordinates of the face where
// N is in pixels
//------------------------------------------------------------------------------
float3 SHGetProbeColor(float face, float2 offset, float2 tx)
{
	// Convert 0-1 tex coords per face to 0-1 for texture
	float2 texSize = {texWidth, texHeight};
	float2 texC = tx/texSize;

	// Read the texture
	float3 oColor = tex2D(ProbeSampler, texC + offset).xyz;
	return oColor;
}//SHGetProbeColor

//------------------------------------------------------------------------------
// Debugging function
//------------------------------------------------------------------------------
float4 GetSHDebugColor(float3 norm, float3 position)
{
	// Figure out which face and it's texture coordinates
	float3 tx = giGetCubeMapTexFace(norm);
	tx.xy *= cubeSize; // This is what we use in sh_conversion.fx 0-7 coordinates

	// Get our 2D tex coord offset into the flattened 2D cube texture
	float3 pos;
	//pos.x = clamp(round(shX), 0, samplesWidth-1);
	//pos.y = clamp(round(shY), 0, samplesHeight-1);
	//pos.z = clamp(round(shZ), 0, samplesDepth-1);
	pos = position;
	float2 offset = giGetCubeFaceOffset(pos, tx.z);

	float3 l = SHGetProbeColor(tx.z, offset, tx.xy);

	float2 tex = giConvertSHCubeCoord(tx.xy, tx.z);
	float4 ya = tex2D(SHSampler, tex);

	float4 cOut;
	//cOut.rgb = norm*0.5+0.5;
	//cOut.rgb = tx/cubesize;
	//cOut.rg = tex.xy;
	//cOut.b = 0;
	cOut.rgb = l;
	//cOut.rgb = ya;
	//cOut.rgb = l.r*ya;
	//cOut.rgb = l.g*ya;
	//cOut.rgb = l.b*ya;
	cOut.a = 1.0;
	return cOut;
}//GetSHDebugColor

//------------------------------------------------------------------------------
// main pixel shader
//------------------------------------------------------------------------------
float4 PS_Textured(outputVertex IN) : COLOR
{
	float4 tex1 = tex2D(TextureSampler, IN.tc.xy);
	float4 lighting;

	//lighting = GetLightingColor(normalize(IN.norm), IN.position);

	lighting.rgb = GetLightingIndirect(normalize(IN.norm), IN.position);
	lighting.a = 1.0f;

	lighting = lerp(lighting, GetSHDebugColor(normalize(IN.norm), IN.position), shShowCube);

	//float3 nPos = (IN.position - bbMin) / (bbMax - bbMin);
	//lighting.rgb = nPos;
	//lighting.a = 1.0;

	//float4 cOut = tex1*(0.75 + (lighting*giFactor));
	//float4 cOut = lerp(tex1, lighting, giFactor);
	float4 cOut = lighting;
	return cOut;
}

technique draw
{
	pass p0 
	{        
	    AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_Textured() CGC_FLAGS("-po OutColorPrec=fp16") ;
	}
}

technique drawskinned
{
	pass p0 
	{        
	    AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = CW;

		VertexShader = compile VERTEXSHADER VS_TransformSkin();
		PixelShader  = compile PIXELSHADER PS_Textured() CGC_FLAGS("-po OutColorPrec=fp16");
	}
}
