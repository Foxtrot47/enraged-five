//-----------------------------------------------------
// Eye shader
//
// Future Improvements/Bugs
// - move all vectors into tangent space so that the reference 
//   space system in the pixel shader is tangent space

// Get the pong globals
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(11);

#include "../shaderlib/rage_diffuse_sampler.fxh"

struct vertexOutputComposite 
{
    DECLARE_POSITION( hPosition)
    float2 texCoordDiffuse  : TEXCOORD0;
};


//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
#if USE_OFFSETS
vertexOutputComposite VS_CompositeLashes(rageSkinVertexInputBump IN, rageSkinVertexInputBumpOffset IN2, uniform bool bUseOffsets) 
#else
vertexOutputComposite VS_CompositeLashes(rageSkinVertexInputBump IN) 
#endif
{
    float3 Position, Normal, Tangent;

    vertexOutputComposite OUT = (vertexOutputComposite)0;
    
    Position = IN.pos;
    Normal   = IN.normal;
    Tangent  = IN.tangent.xyz;
    
#if USE_OFFSETS
    if(bUseOffsets != 0.0f)
    {
        Position += IN2.pos;
        Normal   += IN2.normal;
        Tangent  += IN2.tangent;
    }
#endif
    
    float4 Plocal = float4(Position, 1.0);
    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);
	float3 pos = rageSkinTransform(IN.pos, boneMtx);

    OUT.hPosition = mul(float4(pos,1), gWorldViewProj);
    OUT.texCoordDiffuse = IN.texCoord0;

    return OUT;
}

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS_CompositeLighting( vertexOutputComposite IN ) : COLOR
{
    // color map
    return tex2D( DiffuseSampler, IN.texCoordDiffuse );
}

float4 PS_CompositeLightingSilhouette( vertexOutputComposite IN ) : COLOR
{ 
	return float4(gLightAmbient.rgb,1.0);
}

#if USE_OFFSETS
#define OFFSET_ENABLE false
#else
#define OFFSET_ENABLE 
#endif

technique drawskinned
{
    pass p0 
    {
		CullMode = NONE;
		ZWriteEnable = false;
		
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

		SrcBlend = ONE;
		DestBlend = INVSRCALPHA;        
        VertexShader = compile VERTEXSHADER VS_CompositeLashes(OFFSET_ENABLE);
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}

technique silhouette_drawskinned
{
    pass p0 
    {
        AlphaBlendEnable = false;
        
		SrcBlend = ONE;
		DestBlend = ZERO;

        VertexShader = compile VERTEXSHADER VS_CompositeLashes(OFFSET_ENABLE);
        PixelShader  = compile PIXELSHADER PS_CompositeLightingSilhouette();
    }
}
