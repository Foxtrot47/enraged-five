REM @echo off
@setlocal
set TARGETS=psn fxl_final win32_30 win32_40
if NOT "%SCE_ORBIS_SDK_DIR%"=="" set TARGETS=%TARGETS% orbis

for /f %%i in (x:\rage\assets\tune\shaders\lib\preload.list) do (
  echo ----- %%i ------
  for %%j in (%TARGETS%) do (
    call x:\gta5\tools\script\coding\shaders\makeshader.bat -platform %%j %%i
    if errorlevel 1 exit/b
  )
)
@echo ALL TARGETS SUCCEEDED
