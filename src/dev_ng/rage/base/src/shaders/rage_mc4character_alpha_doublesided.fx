#define USE_ALPHA_BLENDING (1)
#include "../shaderlib/rage_mc4character_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(2);

technique draw
{
	pass	p0
	{
		AlphaBlendEnable = false;
#if __XENON
		HighPrecisionBlendEnable = true; 
#endif
		CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS();
		PixelShader  = compile PIXELSHADER PSAlphaDoubleSided();
	}
}

technique	drawskinned
{
	pass	p0
	{
		AlphaBlendEnable = true;
		ZWriteEnable = false;
#if __XENON
		HighPrecisionBlendEnable = true; 
#endif
		CullMode = NONE;

		VertexShader = compile VERTEXSHADER VSSkinned();
		PixelShader = compile PIXELSHADER PSAlphaDoubleSided();
	}
	
	pass	p1
	{
		AlphaBlendEnable = true;
		ZWriteEnable = true;
#if __XENON
		HighPrecisionBlendEnable = true; 
#endif
		CullMode = NONE;
		AlphaRef = 2;

		VertexShader = compile VERTEXSHADER VSSkinned();
		PixelShader = compile PIXELSHADER PSAlphaDoubleSided();
	}
}
