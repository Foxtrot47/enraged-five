//-----------------------------------------------------
// Rage Diffuse
//

// Get the pong globals
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_utility.fxh"
#include "../shaderlib/rage_shadowmap_common.fxh"
#include "../shaderlib/rage_shadow.fxh"
#include "../shaderlib/rage_fastLight.fxh"
#include "../shaderlib/rage_parallax.fxh"
#include "../shaderlib/rage_heighttricks.fxh"

BeginSampler(sampler2D, HeightTexture, HeightSampler, HeightTex)
    string UIName = "Hieght Texture";
    string TextureOutputFormats = "PC=L8";
ContinueSampler(sampler2D,HeightTexture, HeightSampler, HeightTex)
		AddressU = WRAP;
		AddressV = WRAP;
		AddressW = WRAP; 
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR; 
		LOD_BIAS = 0;
EndSampler;




BeginSampler(sampler2D,ColorTexture,ColorSampler,ColorTex)
    string UIName = "Color Texture";
ContinueSampler(sampler2D,ColorTexture,ColorSampler,ColorTex)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;


struct VertexOutput 
{
    DECLARE_POSITION( Pos )
    float4 WorldPos	  : TEXCOORD0;
    float2 Tex		  : TEXCOORD1;
    float3 Normal      : TEXCOORD2;
    float3 Tangent     : TEXCOORD3;
    float3 Binormal    : TEXCOORD4;
    float3 vcolor	   : TEXCOORD5;
    float3 ViewTS      : TEXCOORD6;
};


//float SpecularAmount;




//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS( rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    float3 pos = IN.pos;

    // output position value
    OUT.Pos = mul(float4( IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    float4 WorldPos = float4(pos, 1.0);
    
    // create tangent space system
	OUT.Normal = normalize(mul(IN.normal, (float3x3)gWorld));
    OUT.Tangent =  normalize(mul(IN.tangent.xyz, (float3x3)gWorld));
    OUT.Binormal = normalize(cross(OUT.Tangent, OUT.Normal));    
    
    // output texture coordinate
    OUT.Tex = IN.texCoord0;
    OUT.WorldPos = mul(float4( IN.pos, 1.0), gWorld );;
    OUT.vcolor = IN.diffuse.xyz;
    
     	OUT.ViewTS = CalcViewTS( OUT.Tangent.xyz, OUT.Binormal.xyz, OUT.Normal.xyz, OUT.WorldPos.xyz);

  
    return OUT;
}

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VSSkinned(rageSkinVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);
	float3 pos = rageSkinTransform(IN.pos, boneMtx);

    // output position value
    OUT.Pos = mul(float4(pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    float4 WorldPos = float4(pos, 1.0);
    
    // create tangent space system
	OUT.Normal = normalize(mul(IN.normal, (float3x3)boneMtx));
    OUT.Tangent = normalize(mul(IN.tangent.xyz, (float3x3)boneMtx));
    OUT.Binormal = normalize(cross(OUT.Tangent, OUT.Normal));    
    
    // output texture coordinate
    OUT.Tex = IN.texCoord1;
    OUT.WorldPos = mul(float4( IN.pos, 1.0), gWorld );;
    
   	OUT.ViewTS = CalcViewTS( OUT.Tangent.xyz, OUT.Binormal.xyz, OUT.Normal.xyz, OUT.WorldPos.xyz);


    return OUT;
}


//--------------------------- general lighting stuff

float3 HemiSphereDiffuseAmbient( float3 normal )
{
	float3 sky = float3( 0.7f , 0.9f, 1.0f ) * 0.7f;
	float3 ground = float3( 0.8, 0.8, 0.6 ) * 0.6f;
	return lerp( ground, sky, saturate( normal.y * 0.5 + 0.5) ); 
}

//-------------------------------------------
void CalculateLighting( float3 Normal, float3 pos, out float3 diffuse, out float3 spec,  float4 shad )
{
	// do sunlight and moon light
	// add point light
	float3 diff2 = rageComputeLightColor( pos, Normal, 1) ;
	float3 diff3 = rageComputeLightColor( pos, Normal, 2) * shad.y ;
	
	diffuse =rageComputeLightColor( pos, Normal, 0) * shad.x + ( diff2 + diff3 ) ;
	
	rageLightOutput lightData = rageComputeLightData( pos, 0);
	float3 L = lightData.lightPosDir.xyz;
	
	float3 view = normalize( pos  - gViewInverse[3].xyz);
	float3 H = normalize( -view + L  );
	spec =  pow( saturate( dot( Normal, H ) ), 32.0f ) * gLightColor[0].xyz * shad.x;// * occ;
	
}	


//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS( VertexOutput IN ) : COLOR
{
	float3 Snorm;
	float3 diffuseTex;
	float4 surfVal;
		
	diffuseTex = tex2D( ColorSampler, IN.Tex  ).xyz;	
	//IN.ViewTS = CalcViewTS( normalize(IN.Tangent.xyz), normalize(IN.Binormal.xyz), normalize(IN.Normal.xyz), IN.WorldPos.xyz);

	IN.Tex = ModifyTexParallax( IN.Tex.xy, IN.ViewTS.xy, HeightSampler);
	float h= HeightGetInfo( IN.Tex, HeightSampler, Snorm, surfVal );
	
	Snorm = normalize(Snorm);
	float3 Normal = normalize( normalize( IN.Tangent ) * Snorm.x + normalize( IN.Binormal) * Snorm.y + normalize( IN.Normal )* Snorm.z );	
	
	float3 diffuse;
	float3 spec;
	float4 shad = 1.0f;//shadow( vpos );
	
	
	rageLightOutput lightData = rageComputeLightData( IN.WorldPos.xyz, 0);
	float3 L = lightData.lightPosDir.xyz;

	shad.x *= HeightCalculateApproxSelfShadowing( saturate( dot( IN.Normal, L )), surfVal.xyz );
	
	float AO = IN.vcolor.x * surfVal.x;
	
	CalculateLighting( Normal, IN.WorldPos.xyz, diffuse, spec,  shad );
	diffuse += HemiSphereDiffuseAmbient( Normal) *  AO ;
	diffuseTex = 0.82f;
	
	float SpecularAmount = 0.3f;
	
	return float4( diffuse * diffuseTex+ spec * surfVal.y * SpecularAmount , 1.0f);
}

technique drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;    
         ZENABLE = true;
       ZWRITEENABLE = true;
         CULLMODE = CW;
        VertexShader = compile VERTEXSHADER VSSkinned(); //VSSkinned();
        PixelShader  = compile PIXELSHADER PS();
    }
}


technique draw
{
    pass p0 
    {
        ZENABLE = true;
       ZWRITEENABLE = true;
         CULLMODE = CW;
      ALPHATESTENABLE = false;
      ALPHABLENDENABLE = false;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}
