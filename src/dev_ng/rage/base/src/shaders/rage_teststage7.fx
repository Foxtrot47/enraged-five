#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_samplers.fxh"

//------------------------------------
float diffuseScalar0 : TextureBlend0
<
	string UIName = "Texture Blend0";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 0.2;
float diffuseScalar1 : TextureBlend1
<
	string UIName = "Texture Blend1";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 0.2;
float diffuseScalar2 : TextureBlend2
<
	string UIName = "Texture Blend2";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 0.2;
float diffuseScalar3 : TextureBlend3
<
	string UIName = "Texture Blend3";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 0.2;
float diffuseScalar4 : TextureBlend4
<
	string UIName = "Texture Blend4";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 0.2;
float diffuseScalar5 : TextureBlend5
<
	string UIName = "Texture Blend5";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = 0.2;




//------------------------------------

struct vertexInput {
	// This covers the default rage vertex format (non-skinned)
    float3 pos			: POSITION0;
	float3 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD1;
    float2 texCoord2	: TEXCOORD2;
    float2 texCoord3	: TEXCOORD3;
    float2 texCoord4	: TEXCOORD4;
    float2 texCoord5	: TEXCOORD5;
};


struct vertexOutput {
    DECLARE_POSITION( worldPos)
    float2 texCoord0        : TEXCOORD0;
	float2 texCoord1		: TEXCOORD1;
	float2 texCoord2		: TEXCOORD2;
	float2 texCoord3		: TEXCOORD3;
	float2 texCoord4		: TEXCOORD4;
	float2 texCoord5		: TEXCOORD5;
	float3 color0			: COLOR0;
};

//------------------------------------

// ******************************
//	DRAW METHODS
// ******************************
vertexOutput VS_Transform(vertexInput IN)
{
    vertexOutput OUT;

    // Write out final position & texture coords
    OUT.worldPos =  mul(float4(IN.pos,1), gWorldViewProj);
    OUT.texCoord0 = IN.texCoord0;
    OUT.texCoord1 = IN.texCoord1;
    OUT.texCoord2 = IN.texCoord2;
    OUT.texCoord3 = IN.texCoord3;
    OUT.texCoord4 = IN.texCoord4;
    OUT.texCoord5 = IN.texCoord5;
    OUT.color0 = IN.diffuse;
    return OUT;
}


BeginSampler(sampler,diffuseTexture,TextureSampler,DiffuseTex)
	string UIName = "Texture Map 0";
ContinueSampler(sampler,diffuseTexture,TextureSampler,DiffuseTex)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

BeginSampler(sampler,diffuseTexture1,TextureSampler1,DiffuseTex1)
	string UIName = "Texture Map 1";
ContinueSampler(sampler,diffuseTexture1,TextureSampler1,DiffuseTex1)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

BeginSampler(sampler,diffuseTexture2,TextureSampler2,DiffuseTex2)
	string UIName = "Texture Map 2";
ContinueSampler(sampler,diffuseTexture2,TextureSampler2,DiffuseTex2)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

BeginSampler(sampler,diffuseTexture3,TextureSampler3,DiffuseTex3)
	string UIName = "Texture Map 3";
ContinueSampler(sampler,diffuseTexture3,TextureSampler3,DiffuseTex3)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

BeginSampler(sampler,diffuseTexture4,TextureSampler4,DiffuseTex4)
	string UIName = "Texture Map 4";
ContinueSampler(sampler,diffuseTexture4,TextureSampler4,DiffuseTex4)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

BeginSampler(sampler,diffuseTexture5,TextureSampler5,DiffuseTex5)
	string UIName = "Texture Map 5";
ContinueSampler(sampler,diffuseTexture5,TextureSampler5,DiffuseTex5)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

/* ====== PIXEL SHADERS =========== */
//-----------------------------------
float4 PS_Textured( vertexOutput IN): COLOR
{
	float3 color = tex2D(TextureSampler, IN.texCoord0.xy).rgb * diffuseScalar0;
	color += tex2D(TextureSampler1, IN.texCoord1.xy).rgb * diffuseScalar1;
	color += tex2D(TextureSampler2, IN.texCoord2.xy).rgb * diffuseScalar2;
	color += tex2D(TextureSampler3, IN.texCoord3.xy).rgb * diffuseScalar3;
	color += tex2D(TextureSampler4, IN.texCoord4.xy).rgb * diffuseScalar4;
	color += tex2D(TextureSampler5, IN.texCoord5.xy).rgb * diffuseScalar5;
	return float4(color, 1.0);
}

//-----------------------------------

// ===============================
// Lit (default) techniques
// ===============================
technique draw
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER VS_Transform();
        PixelShader  = compile PIXELSHADER PS_Textured();
    }
}
