// Configure the megashder
#define USE_SPECULAR
#define IGNORE_SPECULAR_MAP
#define NO_SKINNING

#include "../../src/shaderlib/rage_megashader.fxh"
#include "../../src/shaderlib/instancebuffer.fxh"

#if __XENON
float4 instancing_params REGISTER2( vs, c63 );
#endif

CBSHARED BeginConstantBufferPagedDX10(rage_instmtx,PASTE2(b,INSTANCE_CONSTANT_BUFFER_SLOT))
shared 
#if __PSSL
row_major
#endif
float3x4 gWorldInstanceMatrix[MAX_INSTANCES_PER_DRAW] REGISTER2(vs, PASTE2(c,INSTANCE_SHADER_CONSTANT_SLOT));
EndConstantBufferDX10(rage_instmtx)

#pragma dcl position normal diffuse texcoord0

#if NORMAL_MAP
#error "change DCL to include tangent0 flag too"
#endif

#if __XENON
vertexOutput VS_TransformInst(int nIndex : INDEX)
#elif __SHADERMODEL >= 40
vertexOutput VS_TransformInst(rageVertexInputBump IN,uint nInstIndex : SV_InstanceID)
#else
vertexOutput VS_TransformInst(rageVertexInputBump IN)
#endif
{
	vertexOutput OUT;
    
#if __XENON
    // Compute the instance index
    int nNumIndicesPerInstance = instancing_params.x;
    int nInstIndex = ( nIndex + 0.5 ) / nNumIndicesPerInstance;
    int nMeshIndex = nIndex - nInstIndex * nNumIndicesPerInstance;

    // Fetch the mesh vertex data
	float4 inPos;
	float4 inNrm;
	float4 inCpv;
	float4 inTex;
    asm
    {
        vfetch inPos, nMeshIndex, position0;
        vfetch inNrm,  nMeshIndex, normal0;
		vfetch inCpv, nMeshIndex, color0;
		vfetch inTex, nMeshIndex, texcoord0;
    };
#else

#if __SHADERMODEL < 40
	const int nInstIndex = 0;
#endif
    float4 inPos = float4(IN.pos,1);
    float3 inNrm = IN.normal;
    float4 inCpv = IN.diffuse;
    float2 inTex = IN.texCoord0;
#endif

	rtsProcessVertLightingUnskinned( float3(inPos.xyz), 
							inNrm, 
#if NORMAL_MAP
							inTan,
#endif
							inCpv, 
							gWorld, 
							OUT.worldEyePos,
							OUT.worldNormal,
#if NORMAL_MAP
							OUT.worldBinormal,
							OUT.worldTangent,
#endif // NORMAL_MAP
							OUT.color0 
						);

    // Transform position into worldspace using the instance world matrix
    float3 vPos = mul(gWorldInstanceMatrix[nInstIndex], inPos);
   
    // Transform using the view/projection matrix to get the final screenspace coordinates
    OUT.pos =  mul(float4(vPos,1), gWorldViewProj);
	OUT.color0 = inCpv;
#if SPEC_MAP
    SPECULAR_VS_OUTPUT = SPECULAR_VS_INPUT;
#endif	// SPEC_MAP
	OUT.texCoord.xy = inTex;

	return OUT;
}

float4 PS_Debug(vertexOutput IN) : COLOR0
{
	return float4(1.0f, 1.0f, 1.0f, 1.0f);
}

technique draw
{
	pass p0 
	{        
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		VertexShader = compile VERTEXSHADER VS_TransformInst();
		PixelShader  = compile PIXELSHADER PS_Textured();
	}
}
technique drawskinned
{
	pass p0 
	{        
		AlphaRef = 100;
		AlphaBlendEnable = false;
		AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformInst();
		PixelShader  = compile PIXELSHADER PS_Textured();
	}
}
