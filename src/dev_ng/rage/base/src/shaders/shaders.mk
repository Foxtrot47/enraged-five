SHELL = $(WINDIR)\system32\cmd.exe

ifeq 	($(PLATFORM),psn)
SHADERDIR = psn
SHADEREXT = cgx
else
ifeq 	($(PLATFORM),xenon)
SHADERDIR = fxl_final
SHADEREXT = fxc
else
SHADERDIR	= win32_30
SHADERDIR_30ATI9 = win32_30_atidx9
SHADERDIR_30ATI10 = win32_30_atidx10
SHADERDIR_30NV9 = win32_30_nvdx9
SHADERDIR_30NV10 = win32_30_nvdx10
SHADERDIR_40ATI10 = win32_40_atidx10
SHADERDIR_40NV10 = win32_40_nvdx10
SHADEREXT = fxc
endif
endif

ifeq    ($(RAGE_ASSET_ROOT),)
badEnv:
	@echo $$(RAGE_ASSET_ROOT) env variable is not defined.
	@exit 1
endif

RAGE_DIRECTORY = $(subst \,/,$(RAGE_DIR))

MAKEDEP_PATH = $(subst \,/,$(RS_TOOLSROOT)\bin\coding\makedep.exe)

MAKESHADER_PATH = $(subst \,/,$(RS_TOOLSROOT)\script\coding\shaders\makeshader.bat)

SHADERPATH = $(subst \,/,$(RAGE_ASSET_ROOT)\tune\shaders\lib\)

SHADERPATHDOS = $(subst /,\,$(SHADERPATH))

all: alltargets

clean:
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR)/*.$(SHADEREXT))
	if exist $(SHADERDIR)\*.d del $(SHADERDIR)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI9)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI9)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30ATI9)\*.d del $(SHADERDIR_30ATI9)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30ATI10)\*.d del $(SHADERDIR_30ATI10)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV9)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV9)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30NV9)\*.d del $(SHADERDIR_30NV9)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30NV10)\*.d del $(SHADERDIR_30NV10)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_40ATI10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_40ATI10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_40ATI10)\*.d del $(SHADERDIR_40ATI10)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_40NV10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_40NV10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_40NV10)\*.d del $(SHADERDIR_40NV10)\*.d

# FLAGS = -useATGCompiler
FLAGS = -noPerformanceDump -quiet

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_bump.$(SHADEREXT)

-include $(SHADERDIR)/rage_bump.d

$(SHADERPATH)$(SHADERDIR)/rage_bump.$(SHADEREXT): rage_bump.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_bump.$(SHADEREXT) rage_bump.fx $(SHADERDIR)/rage_bump.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_bump.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_bump.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump.$(SHADEREXT): rage_bump.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump.$(SHADEREXT) rage_bump.fx $(SHADERDIR_30ATI9)/rage_bump.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_bump.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_bump.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump.$(SHADEREXT): rage_bump.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump.$(SHADEREXT) rage_bump.fx $(SHADERDIR_30ATI10)/rage_bump.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_bump.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_bump.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump.$(SHADEREXT): rage_bump.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump.$(SHADEREXT) rage_bump.fx $(SHADERDIR_30NV9)/rage_bump.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_bump.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_bump.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump.$(SHADEREXT): rage_bump.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump.$(SHADEREXT) rage_bump.fx $(SHADERDIR_30NV10)/rage_bump.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_bump.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_bump.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump.$(SHADEREXT): rage_bump.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump.$(SHADEREXT) rage_bump.fx $(SHADERDIR_40ATI10)/rage_bump.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_bump.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_bump.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump.$(SHADEREXT): rage_bump.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump.$(SHADEREXT) rage_bump.fx $(SHADERDIR_40NV10)/rage_bump.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_bump.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_bump_spec.$(SHADEREXT)

-include $(SHADERDIR)/rage_bump_spec.d

$(SHADERPATH)$(SHADERDIR)/rage_bump_spec.$(SHADEREXT): rage_bump_spec.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_bump_spec.$(SHADEREXT) rage_bump_spec.fx $(SHADERDIR)/rage_bump_spec.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_bump_spec.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_bump_spec.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec.$(SHADEREXT): rage_bump_spec.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec.$(SHADEREXT) rage_bump_spec.fx $(SHADERDIR_30ATI9)/rage_bump_spec.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_bump_spec.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_bump_spec.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec.$(SHADEREXT): rage_bump_spec.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec.$(SHADEREXT) rage_bump_spec.fx $(SHADERDIR_30ATI10)/rage_bump_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_bump_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_bump_spec.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec.$(SHADEREXT): rage_bump_spec.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec.$(SHADEREXT) rage_bump_spec.fx $(SHADERDIR_30NV9)/rage_bump_spec.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_bump_spec.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_bump_spec.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec.$(SHADEREXT): rage_bump_spec.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec.$(SHADEREXT) rage_bump_spec.fx $(SHADERDIR_30NV10)/rage_bump_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_bump_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_bump_spec.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec.$(SHADEREXT): rage_bump_spec.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec.$(SHADEREXT) rage_bump_spec.fx $(SHADERDIR_40ATI10)/rage_bump_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_bump_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_bump_spec.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec.$(SHADEREXT): rage_bump_spec.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec.$(SHADEREXT) rage_bump_spec.fx $(SHADERDIR_40NV10)/rage_bump_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_bump_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_2.$(SHADEREXT)

-include $(SHADERDIR)/rage_bump_spec_2.d

$(SHADERPATH)$(SHADERDIR)/rage_bump_spec_2.$(SHADEREXT): rage_bump_spec_2.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_2.$(SHADEREXT) rage_bump_spec_2.fx $(SHADERDIR)/rage_bump_spec_2.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_bump_spec_2.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_2.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_bump_spec_2.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_2.$(SHADEREXT): rage_bump_spec_2.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_2.$(SHADEREXT) rage_bump_spec_2.fx $(SHADERDIR_30ATI9)/rage_bump_spec_2.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_bump_spec_2.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_2.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_bump_spec_2.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_2.$(SHADEREXT): rage_bump_spec_2.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_2.$(SHADEREXT) rage_bump_spec_2.fx $(SHADERDIR_30ATI10)/rage_bump_spec_2.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_bump_spec_2.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_2.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_2.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_bump_spec_2.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_2.$(SHADEREXT): rage_bump_spec_2.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_2.$(SHADEREXT) rage_bump_spec_2.fx $(SHADERDIR_30NV9)/rage_bump_spec_2.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_bump_spec_2.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_2.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_bump_spec_2.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_2.$(SHADEREXT): rage_bump_spec_2.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_2.$(SHADEREXT) rage_bump_spec_2.fx $(SHADERDIR_30NV10)/rage_bump_spec_2.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_bump_spec_2.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_2.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_2.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_bump_spec_2.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_2.$(SHADEREXT): rage_bump_spec_2.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_2.$(SHADEREXT) rage_bump_spec_2.fx $(SHADERDIR_40ATI10)/rage_bump_spec_2.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_bump_spec_2.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_2.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_2.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_bump_spec_2.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_2.$(SHADEREXT): rage_bump_spec_2.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_2.$(SHADEREXT) rage_bump_spec_2.fx $(SHADERDIR_40NV10)/rage_bump_spec_2.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_bump_spec_2.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_2.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_alpha.$(SHADEREXT)

-include $(SHADERDIR)/rage_bump_spec_alpha.d

$(SHADERPATH)$(SHADERDIR)/rage_bump_spec_alpha.$(SHADEREXT): rage_bump_spec_alpha.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_alpha.$(SHADEREXT) rage_bump_spec_alpha.fx $(SHADERDIR)/rage_bump_spec_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_bump_spec_alpha.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_bump_spec_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_alpha.$(SHADEREXT): rage_bump_spec_alpha.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_alpha.$(SHADEREXT) rage_bump_spec_alpha.fx $(SHADERDIR_30ATI9)/rage_bump_spec_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_bump_spec_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_bump_spec_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_alpha.$(SHADEREXT): rage_bump_spec_alpha.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_alpha.$(SHADEREXT) rage_bump_spec_alpha.fx $(SHADERDIR_30ATI10)/rage_bump_spec_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_bump_spec_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_bump_spec_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_alpha.$(SHADEREXT): rage_bump_spec_alpha.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_alpha.$(SHADEREXT) rage_bump_spec_alpha.fx $(SHADERDIR_30NV9)/rage_bump_spec_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_bump_spec_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_bump_spec_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_alpha.$(SHADEREXT): rage_bump_spec_alpha.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_alpha.$(SHADEREXT) rage_bump_spec_alpha.fx $(SHADERDIR_30NV10)/rage_bump_spec_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_bump_spec_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_alpha.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_bump_spec_alpha.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_alpha.$(SHADEREXT): rage_bump_spec_alpha.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_alpha.$(SHADEREXT) rage_bump_spec_alpha.fx $(SHADERDIR_40ATI10)/rage_bump_spec_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_bump_spec_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_alpha.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_bump_spec_alpha.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_alpha.$(SHADEREXT): rage_bump_spec_alpha.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_alpha.$(SHADEREXT) rage_bump_spec_alpha.fx $(SHADERDIR_40NV10)/rage_bump_spec_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_bump_spec_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_alpha.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_dirt.$(SHADEREXT)

-include $(SHADERDIR)/rage_bump_spec_dirt.d

$(SHADERPATH)$(SHADERDIR)/rage_bump_spec_dirt.$(SHADEREXT): rage_bump_spec_dirt.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_dirt.$(SHADEREXT) rage_bump_spec_dirt.fx $(SHADERDIR)/rage_bump_spec_dirt.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_bump_spec_dirt.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_dirt.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_bump_spec_dirt.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_dirt.$(SHADEREXT): rage_bump_spec_dirt.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_dirt.$(SHADEREXT) rage_bump_spec_dirt.fx $(SHADERDIR_30ATI9)/rage_bump_spec_dirt.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_bump_spec_dirt.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_dirt.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_bump_spec_dirt.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_dirt.$(SHADEREXT): rage_bump_spec_dirt.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_dirt.$(SHADEREXT) rage_bump_spec_dirt.fx $(SHADERDIR_30ATI10)/rage_bump_spec_dirt.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_bump_spec_dirt.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_dirt.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_dirt.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_bump_spec_dirt.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_dirt.$(SHADEREXT): rage_bump_spec_dirt.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_dirt.$(SHADEREXT) rage_bump_spec_dirt.fx $(SHADERDIR_30NV9)/rage_bump_spec_dirt.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_bump_spec_dirt.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_dirt.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_bump_spec_dirt.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_dirt.$(SHADEREXT): rage_bump_spec_dirt.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_dirt.$(SHADEREXT) rage_bump_spec_dirt.fx $(SHADERDIR_30NV10)/rage_bump_spec_dirt.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_bump_spec_dirt.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_dirt.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_dirt.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_bump_spec_dirt.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_dirt.$(SHADEREXT): rage_bump_spec_dirt.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_dirt.$(SHADEREXT) rage_bump_spec_dirt.fx $(SHADERDIR_40ATI10)/rage_bump_spec_dirt.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_bump_spec_dirt.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_dirt.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_dirt.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_bump_spec_dirt.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_dirt.$(SHADEREXT): rage_bump_spec_dirt.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_dirt.$(SHADEREXT) rage_bump_spec_dirt.fx $(SHADERDIR_40NV10)/rage_bump_spec_dirt.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_bump_spec_dirt.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_dirt.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflect.$(SHADEREXT)

-include $(SHADERDIR)/rage_bump_spec_reflect.d

$(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflect.$(SHADEREXT): rage_bump_spec_reflect.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflect.$(SHADEREXT) rage_bump_spec_reflect.fx $(SHADERDIR)/rage_bump_spec_reflect.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_bump_spec_reflect.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflect.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_bump_spec_reflect.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflect.$(SHADEREXT): rage_bump_spec_reflect.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflect.$(SHADEREXT) rage_bump_spec_reflect.fx $(SHADERDIR_30ATI9)/rage_bump_spec_reflect.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_bump_spec_reflect.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflect.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_bump_spec_reflect.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflect.$(SHADEREXT): rage_bump_spec_reflect.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflect.$(SHADEREXT) rage_bump_spec_reflect.fx $(SHADERDIR_30ATI10)/rage_bump_spec_reflect.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_bump_spec_reflect.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflect.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflect.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_bump_spec_reflect.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflect.$(SHADEREXT): rage_bump_spec_reflect.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflect.$(SHADEREXT) rage_bump_spec_reflect.fx $(SHADERDIR_30NV9)/rage_bump_spec_reflect.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_bump_spec_reflect.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflect.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_bump_spec_reflect.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflect.$(SHADEREXT): rage_bump_spec_reflect.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflect.$(SHADEREXT) rage_bump_spec_reflect.fx $(SHADERDIR_30NV10)/rage_bump_spec_reflect.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_bump_spec_reflect.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflect.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflect.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_bump_spec_reflect.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflect.$(SHADEREXT): rage_bump_spec_reflect.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflect.$(SHADEREXT) rage_bump_spec_reflect.fx $(SHADERDIR_40ATI10)/rage_bump_spec_reflect.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_bump_spec_reflect.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflect.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflect.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_bump_spec_reflect.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflect.$(SHADEREXT): rage_bump_spec_reflect.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflect.$(SHADEREXT) rage_bump_spec_reflect.fx $(SHADERDIR_40NV10)/rage_bump_spec_reflect.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_bump_spec_reflect.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflect.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR)/rage_bump_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflectcube.$(SHADEREXT): rage_bump_spec_reflectcube.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflectcube.$(SHADEREXT) rage_bump_spec_reflectcube.fx $(SHADERDIR)/rage_bump_spec_reflectcube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_bump_spec_reflectcube.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube.$(SHADEREXT): rage_bump_spec_reflectcube.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube.$(SHADEREXT) rage_bump_spec_reflectcube.fx $(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_bump_spec_reflectcube.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube.$(SHADEREXT): rage_bump_spec_reflectcube.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube.$(SHADEREXT) rage_bump_spec_reflectcube.fx $(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_bump_spec_reflectcube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_bump_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflectcube.$(SHADEREXT): rage_bump_spec_reflectcube.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflectcube.$(SHADEREXT) rage_bump_spec_reflectcube.fx $(SHADERDIR_30NV9)/rage_bump_spec_reflectcube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_bump_spec_reflectcube.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_bump_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflectcube.$(SHADEREXT): rage_bump_spec_reflectcube.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflectcube.$(SHADEREXT) rage_bump_spec_reflectcube.fx $(SHADERDIR_30NV10)/rage_bump_spec_reflectcube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_bump_spec_reflectcube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflectcube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube.$(SHADEREXT): rage_bump_spec_reflectcube.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube.$(SHADEREXT) rage_bump_spec_reflectcube.fx $(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_bump_spec_reflectcube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_bump_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflectcube.$(SHADEREXT): rage_bump_spec_reflectcube.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflectcube.$(SHADEREXT) rage_bump_spec_reflectcube.fx $(SHADERDIR_40NV10)/rage_bump_spec_reflectcube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_bump_spec_reflectcube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflectcube.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

-include $(SHADERDIR)/rage_bump_spec_reflectcube_alpha.d

$(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT): rage_bump_spec_reflectcube_alpha.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT) rage_bump_spec_reflectcube_alpha.fx $(SHADERDIR)/rage_bump_spec_reflectcube_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_bump_spec_reflectcube_alpha.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT): rage_bump_spec_reflectcube_alpha.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT) rage_bump_spec_reflectcube_alpha.fx $(SHADERDIR_30ATI9)/rage_bump_spec_reflectcube_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_bump_spec_reflectcube_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT): rage_bump_spec_reflectcube_alpha.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT) rage_bump_spec_reflectcube_alpha.fx $(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_bump_spec_reflectcube_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_bump_spec_reflectcube_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT): rage_bump_spec_reflectcube_alpha.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT) rage_bump_spec_reflectcube_alpha.fx $(SHADERDIR_30NV9)/rage_bump_spec_reflectcube_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_bump_spec_reflectcube_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_bump_spec_reflectcube_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT): rage_bump_spec_reflectcube_alpha.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT) rage_bump_spec_reflectcube_alpha.fx $(SHADERDIR_30NV10)/rage_bump_spec_reflectcube_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_bump_spec_reflectcube_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube_alpha.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT): rage_bump_spec_reflectcube_alpha.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT) rage_bump_spec_reflectcube_alpha.fx $(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_bump_spec_reflectcube_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_bump_spec_reflectcube_alpha.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT): rage_bump_spec_reflectcube_alpha.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT) rage_bump_spec_reflectcube_alpha.fx $(SHADERDIR_40NV10)/rage_bump_spec_reflectcube_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_bump_spec_reflectcube_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflectcube_alpha.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR)/rage_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflect_alpha.$(SHADEREXT): rage_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_bump_spec_reflect_alpha.$(SHADEREXT) rage_bump_spec_reflect_alpha.fx $(SHADERDIR)/rage_bump_spec_reflect_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_bump_spec_reflect_alpha.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflect_alpha.$(SHADEREXT): rage_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_bump_spec_reflect_alpha.$(SHADEREXT) rage_bump_spec_reflect_alpha.fx $(SHADERDIR_30ATI9)/rage_bump_spec_reflect_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_bump_spec_reflect_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflect_alpha.$(SHADEREXT): rage_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflect_alpha.$(SHADEREXT) rage_bump_spec_reflect_alpha.fx $(SHADERDIR_30ATI10)/rage_bump_spec_reflect_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_bump_spec_reflect_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflect_alpha.$(SHADEREXT): rage_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_bump_spec_reflect_alpha.$(SHADEREXT) rage_bump_spec_reflect_alpha.fx $(SHADERDIR_30NV9)/rage_bump_spec_reflect_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_bump_spec_reflect_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflect_alpha.$(SHADEREXT): rage_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflect_alpha.$(SHADEREXT) rage_bump_spec_reflect_alpha.fx $(SHADERDIR_30NV10)/rage_bump_spec_reflect_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_bump_spec_reflect_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflect_alpha.$(SHADEREXT): rage_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflect_alpha.$(SHADEREXT) rage_bump_spec_reflect_alpha.fx $(SHADERDIR_40ATI10)/rage_bump_spec_reflect_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_bump_spec_reflect_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflect_alpha.$(SHADEREXT): rage_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflect_alpha.$(SHADEREXT) rage_bump_spec_reflect_alpha.fx $(SHADERDIR_40NV10)/rage_bump_spec_reflect_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_bump_spec_reflect_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_bump_spec_reflect_alpha.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_breakableglass.$(SHADEREXT)

-include $(SHADERDIR)/rage_breakableglass.d

$(SHADERPATH)$(SHADERDIR)/rage_breakableglass.$(SHADEREXT): rage_breakableglass.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_breakableglass.$(SHADEREXT) rage_breakableglass.fx $(SHADERDIR)/rage_breakableglass.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_breakableglass.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_breakableglass.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_breakableglass.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_breakableglass.$(SHADEREXT): rage_breakableglass.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_breakableglass.$(SHADEREXT) rage_breakableglass.fx $(SHADERDIR_30ATI9)/rage_breakableglass.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_breakableglass.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_breakableglass.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_breakableglass.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_breakableglass.$(SHADEREXT): rage_breakableglass.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_breakableglass.$(SHADEREXT) rage_breakableglass.fx $(SHADERDIR_30ATI10)/rage_breakableglass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_breakableglass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_breakableglass.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_breakableglass.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_breakableglass.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_breakableglass.$(SHADEREXT): rage_breakableglass.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_breakableglass.$(SHADEREXT) rage_breakableglass.fx $(SHADERDIR_30NV9)/rage_breakableglass.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_breakableglass.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_breakableglass.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_breakableglass.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_breakableglass.$(SHADEREXT): rage_breakableglass.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_breakableglass.$(SHADEREXT) rage_breakableglass.fx $(SHADERDIR_30NV10)/rage_breakableglass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_breakableglass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_breakableglass.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_breakableglass.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_breakableglass.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_breakableglass.$(SHADEREXT): rage_breakableglass.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_breakableglass.$(SHADEREXT) rage_breakableglass.fx $(SHADERDIR_40ATI10)/rage_breakableglass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_breakableglass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_breakableglass.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_breakableglass.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_breakableglass.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_breakableglass.$(SHADEREXT): rage_breakableglass.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_breakableglass.$(SHADEREXT) rage_breakableglass.fx $(SHADERDIR_40NV10)/rage_breakableglass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_breakableglass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_breakableglass.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_cloth.$(SHADEREXT)

-include $(SHADERDIR)/rage_cloth.d

$(SHADERPATH)$(SHADERDIR)/rage_cloth.$(SHADEREXT): rage_cloth.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_cloth.$(SHADEREXT) rage_cloth.fx $(SHADERDIR)/rage_cloth.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_cloth.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_cloth.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_cloth.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_cloth.$(SHADEREXT): rage_cloth.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_cloth.$(SHADEREXT) rage_cloth.fx $(SHADERDIR_30ATI9)/rage_cloth.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_cloth.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cloth.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_cloth.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cloth.$(SHADEREXT): rage_cloth.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cloth.$(SHADEREXT) rage_cloth.fx $(SHADERDIR_30ATI10)/rage_cloth.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_cloth.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cloth.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_cloth.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_cloth.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_cloth.$(SHADEREXT): rage_cloth.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_cloth.$(SHADEREXT) rage_cloth.fx $(SHADERDIR_30NV9)/rage_cloth.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_cloth.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_cloth.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_cloth.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_cloth.$(SHADEREXT): rage_cloth.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_cloth.$(SHADEREXT) rage_cloth.fx $(SHADERDIR_30NV10)/rage_cloth.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_cloth.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_cloth.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cloth.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_cloth.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cloth.$(SHADEREXT): rage_cloth.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cloth.$(SHADEREXT) rage_cloth.fx $(SHADERDIR_40ATI10)/rage_cloth.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_cloth.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cloth.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_cloth.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_cloth.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_cloth.$(SHADEREXT): rage_cloth.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_cloth.$(SHADEREXT) rage_cloth.fx $(SHADERDIR_40NV10)/rage_cloth.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_cloth.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_cloth.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_cloth_dynamic.$(SHADEREXT)

-include $(SHADERDIR)/rage_cloth_dynamic.d

$(SHADERPATH)$(SHADERDIR)/rage_cloth_dynamic.$(SHADEREXT): rage_cloth_dynamic.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_cloth_dynamic.$(SHADEREXT) rage_cloth_dynamic.fx $(SHADERDIR)/rage_cloth_dynamic.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_cloth_dynamic.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_cloth_dynamic.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_cloth_dynamic.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_cloth_dynamic.$(SHADEREXT): rage_cloth_dynamic.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_cloth_dynamic.$(SHADEREXT) rage_cloth_dynamic.fx $(SHADERDIR_30ATI9)/rage_cloth_dynamic.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_cloth_dynamic.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cloth_dynamic.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_cloth_dynamic.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cloth_dynamic.$(SHADEREXT): rage_cloth_dynamic.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cloth_dynamic.$(SHADEREXT) rage_cloth_dynamic.fx $(SHADERDIR_30ATI10)/rage_cloth_dynamic.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_cloth_dynamic.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cloth_dynamic.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_cloth_dynamic.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_cloth_dynamic.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_cloth_dynamic.$(SHADEREXT): rage_cloth_dynamic.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_cloth_dynamic.$(SHADEREXT) rage_cloth_dynamic.fx $(SHADERDIR_30NV9)/rage_cloth_dynamic.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_cloth_dynamic.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_cloth_dynamic.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_cloth_dynamic.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_cloth_dynamic.$(SHADEREXT): rage_cloth_dynamic.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_cloth_dynamic.$(SHADEREXT) rage_cloth_dynamic.fx $(SHADERDIR_30NV10)/rage_cloth_dynamic.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_cloth_dynamic.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_cloth_dynamic.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cloth_dynamic.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_cloth_dynamic.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cloth_dynamic.$(SHADEREXT): rage_cloth_dynamic.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cloth_dynamic.$(SHADEREXT) rage_cloth_dynamic.fx $(SHADERDIR_40ATI10)/rage_cloth_dynamic.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_cloth_dynamic.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cloth_dynamic.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_cloth_dynamic.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_cloth_dynamic.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_cloth_dynamic.$(SHADEREXT): rage_cloth_dynamic.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_cloth_dynamic.$(SHADEREXT) rage_cloth_dynamic.fx $(SHADERDIR_40NV10)/rage_cloth_dynamic.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_cloth_dynamic.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_cloth_dynamic.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_crowd.$(SHADEREXT)

-include $(SHADERDIR)/rage_crowd.d

$(SHADERPATH)$(SHADERDIR)/rage_crowd.$(SHADEREXT): rage_crowd.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_crowd.$(SHADEREXT) rage_crowd.fx $(SHADERDIR)/rage_crowd.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_crowd.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_crowd.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_crowd.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_crowd.$(SHADEREXT): rage_crowd.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_crowd.$(SHADEREXT) rage_crowd.fx $(SHADERDIR_30ATI9)/rage_crowd.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_crowd.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_crowd.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_crowd.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_crowd.$(SHADEREXT): rage_crowd.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_crowd.$(SHADEREXT) rage_crowd.fx $(SHADERDIR_30ATI10)/rage_crowd.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_crowd.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_crowd.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_crowd.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_crowd.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_crowd.$(SHADEREXT): rage_crowd.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_crowd.$(SHADEREXT) rage_crowd.fx $(SHADERDIR_30NV9)/rage_crowd.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_crowd.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_crowd.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_crowd.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_crowd.$(SHADEREXT): rage_crowd.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_crowd.$(SHADEREXT) rage_crowd.fx $(SHADERDIR_30NV10)/rage_crowd.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_crowd.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_crowd.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_crowd.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_crowd.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_crowd.$(SHADEREXT): rage_crowd.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_crowd.$(SHADEREXT) rage_crowd.fx $(SHADERDIR_40ATI10)/rage_crowd.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_crowd.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_crowd.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_crowd.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_crowd.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_crowd.$(SHADEREXT): rage_crowd.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_crowd.$(SHADEREXT) rage_crowd.fx $(SHADERDIR_40NV10)/rage_crowd.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_crowd.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_crowd.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_cube.$(SHADEREXT)

-include $(SHADERDIR)/rage_cube.d

$(SHADERPATH)$(SHADERDIR)/rage_cube.$(SHADEREXT): rage_cube.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_cube.$(SHADEREXT) rage_cube.fx $(SHADERDIR)/rage_cube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_cube.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_cube.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_cube.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_cube.$(SHADEREXT): rage_cube.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_cube.$(SHADEREXT) rage_cube.fx $(SHADERDIR_30ATI9)/rage_cube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_cube.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cube.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_cube.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cube.$(SHADEREXT): rage_cube.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cube.$(SHADEREXT) rage_cube.fx $(SHADERDIR_30ATI10)/rage_cube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_cube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_cube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_cube.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_cube.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_cube.$(SHADEREXT): rage_cube.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_cube.$(SHADEREXT) rage_cube.fx $(SHADERDIR_30NV9)/rage_cube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_cube.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_cube.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_cube.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_cube.$(SHADEREXT): rage_cube.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_cube.$(SHADEREXT) rage_cube.fx $(SHADERDIR_30NV10)/rage_cube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_cube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_cube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cube.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_cube.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cube.$(SHADEREXT): rage_cube.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cube.$(SHADEREXT) rage_cube.fx $(SHADERDIR_40ATI10)/rage_cube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_cube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_cube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_cube.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_cube.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_cube.$(SHADEREXT): rage_cube.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_cube.$(SHADEREXT) rage_cube.fx $(SHADERDIR_40NV10)/rage_cube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_cube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_cube.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_diffuse.$(SHADEREXT)

-include $(SHADERDIR)/rage_diffuse.d

$(SHADERPATH)$(SHADERDIR)/rage_diffuse.$(SHADEREXT): rage_diffuse.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_diffuse.$(SHADEREXT) rage_diffuse.fx $(SHADERDIR)/rage_diffuse.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_diffuse.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_diffuse.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse.$(SHADEREXT): rage_diffuse.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse.$(SHADEREXT) rage_diffuse.fx $(SHADERDIR_30ATI9)/rage_diffuse.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_diffuse.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_diffuse.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse.$(SHADEREXT): rage_diffuse.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse.$(SHADEREXT) rage_diffuse.fx $(SHADERDIR_30ATI10)/rage_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_diffuse.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse.$(SHADEREXT): rage_diffuse.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse.$(SHADEREXT) rage_diffuse.fx $(SHADERDIR_30NV9)/rage_diffuse.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_diffuse.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_diffuse.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse.$(SHADEREXT): rage_diffuse.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse.$(SHADEREXT) rage_diffuse.fx $(SHADERDIR_30NV10)/rage_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_diffuse.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse.$(SHADEREXT): rage_diffuse.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse.$(SHADEREXT) rage_diffuse.fx $(SHADERDIR_40ATI10)/rage_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_diffuse.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse.$(SHADEREXT): rage_diffuse.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse.$(SHADEREXT) rage_diffuse.fx $(SHADERDIR_40NV10)/rage_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_diffuse_alpha.$(SHADEREXT)

-include $(SHADERDIR)/rage_diffuse_alpha.d

$(SHADERPATH)$(SHADERDIR)/rage_diffuse_alpha.$(SHADEREXT): rage_diffuse_alpha.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_diffuse_alpha.$(SHADEREXT) rage_diffuse_alpha.fx $(SHADERDIR)/rage_diffuse_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_diffuse_alpha.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_diffuse_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_alpha.$(SHADEREXT): rage_diffuse_alpha.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_alpha.$(SHADEREXT) rage_diffuse_alpha.fx $(SHADERDIR_30ATI9)/rage_diffuse_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_diffuse_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_diffuse_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_alpha.$(SHADEREXT): rage_diffuse_alpha.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_alpha.$(SHADEREXT) rage_diffuse_alpha.fx $(SHADERDIR_30ATI10)/rage_diffuse_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_diffuse_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_diffuse_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_alpha.$(SHADEREXT): rage_diffuse_alpha.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_alpha.$(SHADEREXT) rage_diffuse_alpha.fx $(SHADERDIR_30NV9)/rage_diffuse_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_diffuse_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_diffuse_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_alpha.$(SHADEREXT): rage_diffuse_alpha.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_alpha.$(SHADEREXT) rage_diffuse_alpha.fx $(SHADERDIR_30NV10)/rage_diffuse_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_diffuse_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_alpha.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_diffuse_alpha.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_alpha.$(SHADEREXT): rage_diffuse_alpha.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_alpha.$(SHADEREXT) rage_diffuse_alpha.fx $(SHADERDIR_40ATI10)/rage_diffuse_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_diffuse_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_alpha.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_diffuse_alpha.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_alpha.$(SHADEREXT): rage_diffuse_alpha.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_alpha.$(SHADEREXT) rage_diffuse_alpha.fx $(SHADERDIR_40NV10)/rage_diffuse_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_diffuse_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_alpha.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_diffuse_blendshapes.$(SHADEREXT)

-include $(SHADERDIR)/rage_diffuse_blendshapes.d

$(SHADERPATH)$(SHADERDIR)/rage_diffuse_blendshapes.$(SHADEREXT): rage_diffuse_blendshapes.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_diffuse_blendshapes.$(SHADEREXT) rage_diffuse_blendshapes.fx $(SHADERDIR)/rage_diffuse_blendshapes.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_diffuse_blendshapes.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_blendshapes.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_diffuse_blendshapes.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_blendshapes.$(SHADEREXT): rage_diffuse_blendshapes.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_blendshapes.$(SHADEREXT) rage_diffuse_blendshapes.fx $(SHADERDIR_30ATI9)/rage_diffuse_blendshapes.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_diffuse_blendshapes.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_blendshapes.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_diffuse_blendshapes.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_blendshapes.$(SHADEREXT): rage_diffuse_blendshapes.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_blendshapes.$(SHADEREXT) rage_diffuse_blendshapes.fx $(SHADERDIR_30ATI10)/rage_diffuse_blendshapes.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_diffuse_blendshapes.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_blendshapes.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_blendshapes.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_diffuse_blendshapes.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_blendshapes.$(SHADEREXT): rage_diffuse_blendshapes.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_blendshapes.$(SHADEREXT) rage_diffuse_blendshapes.fx $(SHADERDIR_30NV9)/rage_diffuse_blendshapes.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_diffuse_blendshapes.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_blendshapes.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_diffuse_blendshapes.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_blendshapes.$(SHADEREXT): rage_diffuse_blendshapes.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_blendshapes.$(SHADEREXT) rage_diffuse_blendshapes.fx $(SHADERDIR_30NV10)/rage_diffuse_blendshapes.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_diffuse_blendshapes.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_blendshapes.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_blendshapes.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_diffuse_blendshapes.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_blendshapes.$(SHADEREXT): rage_diffuse_blendshapes.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_blendshapes.$(SHADEREXT) rage_diffuse_blendshapes.fx $(SHADERDIR_40ATI10)/rage_diffuse_blendshapes.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_diffuse_blendshapes.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_blendshapes.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_blendshapes.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_diffuse_blendshapes.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_blendshapes.$(SHADEREXT): rage_diffuse_blendshapes.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_blendshapes.$(SHADEREXT) rage_diffuse_blendshapes.fx $(SHADERDIR_40NV10)/rage_diffuse_blendshapes.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_diffuse_blendshapes.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_blendshapes.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_diffuse_ppp.$(SHADEREXT)

-include $(SHADERDIR)/rage_diffuse_ppp.d

$(SHADERPATH)$(SHADERDIR)/rage_diffuse_ppp.$(SHADEREXT): rage_diffuse_ppp.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_diffuse_ppp.$(SHADEREXT) rage_diffuse_ppp.fx $(SHADERDIR)/rage_diffuse_ppp.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_diffuse_ppp.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_ppp.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_diffuse_ppp.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_ppp.$(SHADEREXT): rage_diffuse_ppp.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_ppp.$(SHADEREXT) rage_diffuse_ppp.fx $(SHADERDIR_30ATI9)/rage_diffuse_ppp.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_diffuse_ppp.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_ppp.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_diffuse_ppp.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_ppp.$(SHADEREXT): rage_diffuse_ppp.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_ppp.$(SHADEREXT) rage_diffuse_ppp.fx $(SHADERDIR_30ATI10)/rage_diffuse_ppp.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_diffuse_ppp.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_ppp.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_ppp.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_diffuse_ppp.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_ppp.$(SHADEREXT): rage_diffuse_ppp.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_ppp.$(SHADEREXT) rage_diffuse_ppp.fx $(SHADERDIR_30NV9)/rage_diffuse_ppp.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_diffuse_ppp.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_ppp.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_diffuse_ppp.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_ppp.$(SHADEREXT): rage_diffuse_ppp.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_ppp.$(SHADEREXT) rage_diffuse_ppp.fx $(SHADERDIR_30NV10)/rage_diffuse_ppp.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_diffuse_ppp.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_ppp.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_ppp.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_diffuse_ppp.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_ppp.$(SHADEREXT): rage_diffuse_ppp.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_ppp.$(SHADEREXT) rage_diffuse_ppp.fx $(SHADERDIR_40ATI10)/rage_diffuse_ppp.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_diffuse_ppp.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_ppp.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_ppp.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_diffuse_ppp.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_ppp.$(SHADEREXT): rage_diffuse_ppp.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_ppp.$(SHADEREXT) rage_diffuse_ppp.fx $(SHADERDIR_40NV10)/rage_diffuse_ppp.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_diffuse_ppp.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_ppp.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_diffuse_shadow.$(SHADEREXT)

-include $(SHADERDIR)/rage_diffuse_shadow.d

$(SHADERPATH)$(SHADERDIR)/rage_diffuse_shadow.$(SHADEREXT): rage_diffuse_shadow.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_diffuse_shadow.$(SHADEREXT) rage_diffuse_shadow.fx $(SHADERDIR)/rage_diffuse_shadow.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_diffuse_shadow.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_shadow.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_diffuse_shadow.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_shadow.$(SHADEREXT): rage_diffuse_shadow.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_shadow.$(SHADEREXT) rage_diffuse_shadow.fx $(SHADERDIR_30ATI9)/rage_diffuse_shadow.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_diffuse_shadow.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_shadow.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_diffuse_shadow.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_shadow.$(SHADEREXT): rage_diffuse_shadow.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_shadow.$(SHADEREXT) rage_diffuse_shadow.fx $(SHADERDIR_30ATI10)/rage_diffuse_shadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_diffuse_shadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_shadow.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_shadow.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_diffuse_shadow.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_shadow.$(SHADEREXT): rage_diffuse_shadow.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_shadow.$(SHADEREXT) rage_diffuse_shadow.fx $(SHADERDIR_30NV9)/rage_diffuse_shadow.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_diffuse_shadow.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_shadow.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_diffuse_shadow.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_shadow.$(SHADEREXT): rage_diffuse_shadow.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_shadow.$(SHADEREXT) rage_diffuse_shadow.fx $(SHADERDIR_30NV10)/rage_diffuse_shadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_diffuse_shadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_shadow.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_shadow.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_diffuse_shadow.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_shadow.$(SHADEREXT): rage_diffuse_shadow.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_shadow.$(SHADEREXT) rage_diffuse_shadow.fx $(SHADERDIR_40ATI10)/rage_diffuse_shadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_diffuse_shadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_shadow.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_shadow.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_diffuse_shadow.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_shadow.$(SHADEREXT): rage_diffuse_shadow.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_shadow.$(SHADEREXT) rage_diffuse_shadow.fx $(SHADERDIR_40NV10)/rage_diffuse_shadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_diffuse_shadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_shadow.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_edgefilter.$(SHADEREXT)

-include $(SHADERDIR)/rage_edgefilter.d

$(SHADERPATH)$(SHADERDIR)/rage_edgefilter.$(SHADEREXT): rage_edgefilter.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_edgefilter.$(SHADEREXT) rage_edgefilter.fx $(SHADERDIR)/rage_edgefilter.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_edgefilter.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_edgefilter.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_edgefilter.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_edgefilter.$(SHADEREXT): rage_edgefilter.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_edgefilter.$(SHADEREXT) rage_edgefilter.fx $(SHADERDIR_30ATI9)/rage_edgefilter.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_edgefilter.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_edgefilter.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_edgefilter.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_edgefilter.$(SHADEREXT): rage_edgefilter.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_edgefilter.$(SHADEREXT) rage_edgefilter.fx $(SHADERDIR_30ATI10)/rage_edgefilter.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_edgefilter.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_edgefilter.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_edgefilter.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_edgefilter.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_edgefilter.$(SHADEREXT): rage_edgefilter.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_edgefilter.$(SHADEREXT) rage_edgefilter.fx $(SHADERDIR_30NV9)/rage_edgefilter.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_edgefilter.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_edgefilter.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_edgefilter.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_edgefilter.$(SHADEREXT): rage_edgefilter.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_edgefilter.$(SHADEREXT) rage_edgefilter.fx $(SHADERDIR_30NV10)/rage_edgefilter.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_edgefilter.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_edgefilter.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_edgefilter.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_edgefilter.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_edgefilter.$(SHADEREXT): rage_edgefilter.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_edgefilter.$(SHADEREXT) rage_edgefilter.fx $(SHADERDIR_40ATI10)/rage_edgefilter.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_edgefilter.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_edgefilter.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_edgefilter.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_edgefilter.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_edgefilter.$(SHADEREXT): rage_edgefilter.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_edgefilter.$(SHADEREXT) rage_edgefilter.fx $(SHADERDIR_40NV10)/rage_edgefilter.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_edgefilter.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_edgefilter.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_emissive.$(SHADEREXT)

-include $(SHADERDIR)/rage_emissive.d

$(SHADERPATH)$(SHADERDIR)/rage_emissive.$(SHADEREXT): rage_emissive.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_emissive.$(SHADEREXT) rage_emissive.fx $(SHADERDIR)/rage_emissive.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_emissive.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_emissive.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_emissive.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_emissive.$(SHADEREXT): rage_emissive.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_emissive.$(SHADEREXT) rage_emissive.fx $(SHADERDIR_30ATI9)/rage_emissive.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_emissive.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_emissive.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_emissive.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_emissive.$(SHADEREXT): rage_emissive.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_emissive.$(SHADEREXT) rage_emissive.fx $(SHADERDIR_30ATI10)/rage_emissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_emissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_emissive.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_emissive.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_emissive.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_emissive.$(SHADEREXT): rage_emissive.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_emissive.$(SHADEREXT) rage_emissive.fx $(SHADERDIR_30NV9)/rage_emissive.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_emissive.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_emissive.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_emissive.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_emissive.$(SHADEREXT): rage_emissive.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_emissive.$(SHADEREXT) rage_emissive.fx $(SHADERDIR_30NV10)/rage_emissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_emissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_emissive.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_emissive.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_emissive.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_emissive.$(SHADEREXT): rage_emissive.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_emissive.$(SHADEREXT) rage_emissive.fx $(SHADERDIR_40ATI10)/rage_emissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_emissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_emissive.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_emissive.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_emissive.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_emissive.$(SHADEREXT): rage_emissive.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_emissive.$(SHADEREXT) rage_emissive.fx $(SHADERDIR_40NV10)/rage_emissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_emissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_emissive.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_eye.$(SHADEREXT)

-include $(SHADERDIR)/rage_eye.d

$(SHADERPATH)$(SHADERDIR)/rage_eye.$(SHADEREXT): rage_eye.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_eye.$(SHADEREXT) rage_eye.fx $(SHADERDIR)/rage_eye.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_eye.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_eye.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_eye.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_eye.$(SHADEREXT): rage_eye.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_eye.$(SHADEREXT) rage_eye.fx $(SHADERDIR_30ATI9)/rage_eye.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_eye.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_eye.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_eye.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_eye.$(SHADEREXT): rage_eye.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_eye.$(SHADEREXT) rage_eye.fx $(SHADERDIR_30ATI10)/rage_eye.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_eye.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_eye.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_eye.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_eye.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_eye.$(SHADEREXT): rage_eye.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_eye.$(SHADEREXT) rage_eye.fx $(SHADERDIR_30NV9)/rage_eye.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_eye.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_eye.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_eye.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_eye.$(SHADEREXT): rage_eye.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_eye.$(SHADEREXT) rage_eye.fx $(SHADERDIR_30NV10)/rage_eye.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_eye.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_eye.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_eye.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_eye.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_eye.$(SHADEREXT): rage_eye.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_eye.$(SHADEREXT) rage_eye.fx $(SHADERDIR_40ATI10)/rage_eye.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_eye.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_eye.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_eye.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_eye.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_eye.$(SHADEREXT): rage_eye.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_eye.$(SHADEREXT) rage_eye.fx $(SHADERDIR_40NV10)/rage_eye.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_eye.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_eye.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_fins.$(SHADEREXT)

-include $(SHADERDIR)/rage_fins.d

$(SHADERPATH)$(SHADERDIR)/rage_fins.$(SHADEREXT): rage_fins.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_fins.$(SHADEREXT) rage_fins.fx $(SHADERDIR)/rage_fins.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_fins.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_fins.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_fins.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_fins.$(SHADEREXT): rage_fins.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_fins.$(SHADEREXT) rage_fins.fx $(SHADERDIR_30ATI9)/rage_fins.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_fins.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_fins.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_fins.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_fins.$(SHADEREXT): rage_fins.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_fins.$(SHADEREXT) rage_fins.fx $(SHADERDIR_30ATI10)/rage_fins.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_fins.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_fins.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_fins.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_fins.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_fins.$(SHADEREXT): rage_fins.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_fins.$(SHADEREXT) rage_fins.fx $(SHADERDIR_30NV9)/rage_fins.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_fins.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_fins.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_fins.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_fins.$(SHADEREXT): rage_fins.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_fins.$(SHADEREXT) rage_fins.fx $(SHADERDIR_30NV10)/rage_fins.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_fins.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_fins.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_fins.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_fins.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_fins.$(SHADEREXT): rage_fins.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_fins.$(SHADEREXT) rage_fins.fx $(SHADERDIR_40ATI10)/rage_fins.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_fins.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_fins.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_fins.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_fins.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_fins.$(SHADEREXT): rage_fins.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_fins.$(SHADEREXT) rage_fins.fx $(SHADERDIR_40NV10)/rage_fins.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_fins.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_fins.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_gem.$(SHADEREXT)

-include $(SHADERDIR)/rage_gem.d

$(SHADERPATH)$(SHADERDIR)/rage_gem.$(SHADEREXT): rage_gem.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_gem.$(SHADEREXT) rage_gem.fx $(SHADERDIR)/rage_gem.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_gem.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gem.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_gem.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gem.$(SHADEREXT): rage_gem.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gem.$(SHADEREXT) rage_gem.fx $(SHADERDIR_30ATI9)/rage_gem.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_gem.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gem.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_gem.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gem.$(SHADEREXT): rage_gem.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gem.$(SHADEREXT) rage_gem.fx $(SHADERDIR_30ATI10)/rage_gem.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_gem.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gem.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gem.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_gem.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_gem.$(SHADEREXT): rage_gem.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gem.$(SHADEREXT) rage_gem.fx $(SHADERDIR_30NV9)/rage_gem.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_gem.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gem.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_gem.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_gem.$(SHADEREXT): rage_gem.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gem.$(SHADEREXT) rage_gem.fx $(SHADERDIR_30NV10)/rage_gem.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_gem.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gem.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gem.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_gem.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gem.$(SHADEREXT): rage_gem.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gem.$(SHADEREXT) rage_gem.fx $(SHADERDIR_40ATI10)/rage_gem.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_gem.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gem.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gem.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_gem.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_gem.$(SHADEREXT): rage_gem.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gem.$(SHADEREXT) rage_gem.fx $(SHADERDIR_40NV10)/rage_gem.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_gem.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gem.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_gem_refraction.$(SHADEREXT)

-include $(SHADERDIR)/rage_gem_refraction.d

$(SHADERPATH)$(SHADERDIR)/rage_gem_refraction.$(SHADEREXT): rage_gem_refraction.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_gem_refraction.$(SHADEREXT) rage_gem_refraction.fx $(SHADERDIR)/rage_gem_refraction.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_gem_refraction.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gem_refraction.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_gem_refraction.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gem_refraction.$(SHADEREXT): rage_gem_refraction.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gem_refraction.$(SHADEREXT) rage_gem_refraction.fx $(SHADERDIR_30ATI9)/rage_gem_refraction.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_gem_refraction.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gem_refraction.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_gem_refraction.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gem_refraction.$(SHADEREXT): rage_gem_refraction.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gem_refraction.$(SHADEREXT) rage_gem_refraction.fx $(SHADERDIR_30ATI10)/rage_gem_refraction.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_gem_refraction.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gem_refraction.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gem_refraction.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_gem_refraction.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_gem_refraction.$(SHADEREXT): rage_gem_refraction.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gem_refraction.$(SHADEREXT) rage_gem_refraction.fx $(SHADERDIR_30NV9)/rage_gem_refraction.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_gem_refraction.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gem_refraction.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_gem_refraction.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_gem_refraction.$(SHADEREXT): rage_gem_refraction.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gem_refraction.$(SHADEREXT) rage_gem_refraction.fx $(SHADERDIR_30NV10)/rage_gem_refraction.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_gem_refraction.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gem_refraction.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gem_refraction.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_gem_refraction.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gem_refraction.$(SHADEREXT): rage_gem_refraction.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gem_refraction.$(SHADEREXT) rage_gem_refraction.fx $(SHADERDIR_40ATI10)/rage_gem_refraction.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_gem_refraction.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gem_refraction.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gem_refraction.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_gem_refraction.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_gem_refraction.$(SHADEREXT): rage_gem_refraction.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gem_refraction.$(SHADEREXT) rage_gem_refraction.fx $(SHADERDIR_40NV10)/rage_gem_refraction.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_gem_refraction.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gem_refraction.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_gi_diffuse.$(SHADEREXT)

-include $(SHADERDIR)/rage_gi_diffuse.d

$(SHADERPATH)$(SHADERDIR)/rage_gi_diffuse.$(SHADEREXT): rage_gi_diffuse.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_gi_diffuse.$(SHADEREXT) rage_gi_diffuse.fx $(SHADERDIR)/rage_gi_diffuse.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_gi_diffuse.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_gi_diffuse.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_diffuse.$(SHADEREXT): rage_gi_diffuse.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_diffuse.$(SHADEREXT) rage_gi_diffuse.fx $(SHADERDIR_30ATI9)/rage_gi_diffuse.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_gi_diffuse.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_gi_diffuse.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_diffuse.$(SHADEREXT): rage_gi_diffuse.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_diffuse.$(SHADEREXT) rage_gi_diffuse.fx $(SHADERDIR_30ATI10)/rage_gi_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_gi_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_diffuse.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_gi_diffuse.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_diffuse.$(SHADEREXT): rage_gi_diffuse.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_diffuse.$(SHADEREXT) rage_gi_diffuse.fx $(SHADERDIR_30NV9)/rage_gi_diffuse.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_gi_diffuse.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_gi_diffuse.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_diffuse.$(SHADEREXT): rage_gi_diffuse.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_diffuse.$(SHADEREXT) rage_gi_diffuse.fx $(SHADERDIR_30NV10)/rage_gi_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_gi_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_diffuse.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_diffuse.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_gi_diffuse.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_diffuse.$(SHADEREXT): rage_gi_diffuse.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_diffuse.$(SHADEREXT) rage_gi_diffuse.fx $(SHADERDIR_40ATI10)/rage_gi_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_gi_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_diffuse.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_diffuse.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_gi_diffuse.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_diffuse.$(SHADEREXT): rage_gi_diffuse.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_diffuse.$(SHADEREXT) rage_gi_diffuse.fx $(SHADERDIR_40NV10)/rage_gi_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_gi_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_diffuse.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_gi_sh_conversion.$(SHADEREXT)

-include $(SHADERDIR)/rage_gi_sh_conversion.d

$(SHADERPATH)$(SHADERDIR)/rage_gi_sh_conversion.$(SHADEREXT): rage_gi_sh_conversion.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_gi_sh_conversion.$(SHADEREXT) rage_gi_sh_conversion.fx $(SHADERDIR)/rage_gi_sh_conversion.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_gi_sh_conversion.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_sh_conversion.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_gi_sh_conversion.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_sh_conversion.$(SHADEREXT): rage_gi_sh_conversion.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_sh_conversion.$(SHADEREXT) rage_gi_sh_conversion.fx $(SHADERDIR_30ATI9)/rage_gi_sh_conversion.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_gi_sh_conversion.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_sh_conversion.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_gi_sh_conversion.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_sh_conversion.$(SHADEREXT): rage_gi_sh_conversion.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_sh_conversion.$(SHADEREXT) rage_gi_sh_conversion.fx $(SHADERDIR_30ATI10)/rage_gi_sh_conversion.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_gi_sh_conversion.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_sh_conversion.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_sh_conversion.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_gi_sh_conversion.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_sh_conversion.$(SHADEREXT): rage_gi_sh_conversion.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_sh_conversion.$(SHADEREXT) rage_gi_sh_conversion.fx $(SHADERDIR_30NV9)/rage_gi_sh_conversion.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_gi_sh_conversion.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_sh_conversion.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_gi_sh_conversion.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_sh_conversion.$(SHADEREXT): rage_gi_sh_conversion.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_sh_conversion.$(SHADEREXT) rage_gi_sh_conversion.fx $(SHADERDIR_30NV10)/rage_gi_sh_conversion.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_gi_sh_conversion.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_sh_conversion.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_sh_conversion.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_gi_sh_conversion.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_sh_conversion.$(SHADEREXT): rage_gi_sh_conversion.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_sh_conversion.$(SHADEREXT) rage_gi_sh_conversion.fx $(SHADERDIR_40ATI10)/rage_gi_sh_conversion.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_gi_sh_conversion.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_sh_conversion.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_sh_conversion.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_gi_sh_conversion.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_sh_conversion.$(SHADEREXT): rage_gi_sh_conversion.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_sh_conversion.$(SHADEREXT) rage_gi_sh_conversion.fx $(SHADERDIR_40NV10)/rage_gi_sh_conversion.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_gi_sh_conversion.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_sh_conversion.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_gi_sphere.$(SHADEREXT)

-include $(SHADERDIR)/rage_gi_sphere.d

$(SHADERPATH)$(SHADERDIR)/rage_gi_sphere.$(SHADEREXT): rage_gi_sphere.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_gi_sphere.$(SHADEREXT) rage_gi_sphere.fx $(SHADERDIR)/rage_gi_sphere.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_gi_sphere.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_sphere.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_gi_sphere.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_sphere.$(SHADEREXT): rage_gi_sphere.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_sphere.$(SHADEREXT) rage_gi_sphere.fx $(SHADERDIR_30ATI9)/rage_gi_sphere.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_gi_sphere.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_sphere.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_gi_sphere.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_sphere.$(SHADEREXT): rage_gi_sphere.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_sphere.$(SHADEREXT) rage_gi_sphere.fx $(SHADERDIR_30ATI10)/rage_gi_sphere.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_gi_sphere.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_sphere.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_sphere.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_gi_sphere.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_sphere.$(SHADEREXT): rage_gi_sphere.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_sphere.$(SHADEREXT) rage_gi_sphere.fx $(SHADERDIR_30NV9)/rage_gi_sphere.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_gi_sphere.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_sphere.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_gi_sphere.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_sphere.$(SHADEREXT): rage_gi_sphere.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_sphere.$(SHADEREXT) rage_gi_sphere.fx $(SHADERDIR_30NV10)/rage_gi_sphere.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_gi_sphere.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_sphere.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_sphere.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_gi_sphere.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_sphere.$(SHADEREXT): rage_gi_sphere.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_sphere.$(SHADEREXT) rage_gi_sphere.fx $(SHADERDIR_40ATI10)/rage_gi_sphere.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_gi_sphere.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_sphere.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_sphere.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_gi_sphere.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_sphere.$(SHADEREXT): rage_gi_sphere.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_sphere.$(SHADEREXT) rage_gi_sphere.fx $(SHADERDIR_40NV10)/rage_gi_sphere.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_gi_sphere.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_sphere.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_gi_blit_textures.$(SHADEREXT)

-include $(SHADERDIR)/rage_gi_blit_textures.d

$(SHADERPATH)$(SHADERDIR)/rage_gi_blit_textures.$(SHADEREXT): rage_gi_blit_textures.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_gi_blit_textures.$(SHADEREXT) rage_gi_blit_textures.fx $(SHADERDIR)/rage_gi_blit_textures.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_gi_blit_textures.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_blit_textures.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_gi_blit_textures.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_blit_textures.$(SHADEREXT): rage_gi_blit_textures.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gi_blit_textures.$(SHADEREXT) rage_gi_blit_textures.fx $(SHADERDIR_30ATI9)/rage_gi_blit_textures.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_gi_blit_textures.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_blit_textures.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_gi_blit_textures.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_blit_textures.$(SHADEREXT): rage_gi_blit_textures.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_blit_textures.$(SHADEREXT) rage_gi_blit_textures.fx $(SHADERDIR_30ATI10)/rage_gi_blit_textures.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_gi_blit_textures.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gi_blit_textures.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_blit_textures.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_gi_blit_textures.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_blit_textures.$(SHADEREXT): rage_gi_blit_textures.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gi_blit_textures.$(SHADEREXT) rage_gi_blit_textures.fx $(SHADERDIR_30NV9)/rage_gi_blit_textures.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_gi_blit_textures.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_blit_textures.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_gi_blit_textures.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_blit_textures.$(SHADEREXT): rage_gi_blit_textures.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_blit_textures.$(SHADEREXT) rage_gi_blit_textures.fx $(SHADERDIR_30NV10)/rage_gi_blit_textures.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_gi_blit_textures.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gi_blit_textures.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_blit_textures.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_gi_blit_textures.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_blit_textures.$(SHADEREXT): rage_gi_blit_textures.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_blit_textures.$(SHADEREXT) rage_gi_blit_textures.fx $(SHADERDIR_40ATI10)/rage_gi_blit_textures.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_gi_blit_textures.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gi_blit_textures.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_blit_textures.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_gi_blit_textures.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_blit_textures.$(SHADEREXT): rage_gi_blit_textures.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_blit_textures.$(SHADEREXT) rage_gi_blit_textures.fx $(SHADERDIR_40NV10)/rage_gi_blit_textures.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_gi_blit_textures.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gi_blit_textures.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_glass.$(SHADEREXT)

-include $(SHADERDIR)/rage_glass.d

$(SHADERPATH)$(SHADERDIR)/rage_glass.$(SHADEREXT): rage_glass.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_glass.$(SHADEREXT) rage_glass.fx $(SHADERDIR)/rage_glass.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_glass.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_glass.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_glass.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_glass.$(SHADEREXT): rage_glass.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_glass.$(SHADEREXT) rage_glass.fx $(SHADERDIR_30ATI9)/rage_glass.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_glass.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_glass.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_glass.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_glass.$(SHADEREXT): rage_glass.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_glass.$(SHADEREXT) rage_glass.fx $(SHADERDIR_30ATI10)/rage_glass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_glass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_glass.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_glass.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_glass.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_glass.$(SHADEREXT): rage_glass.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_glass.$(SHADEREXT) rage_glass.fx $(SHADERDIR_30NV9)/rage_glass.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_glass.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_glass.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_glass.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_glass.$(SHADEREXT): rage_glass.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_glass.$(SHADEREXT) rage_glass.fx $(SHADERDIR_30NV10)/rage_glass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_glass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_glass.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_glass.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_glass.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_glass.$(SHADEREXT): rage_glass.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_glass.$(SHADEREXT) rage_glass.fx $(SHADERDIR_40ATI10)/rage_glass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_glass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_glass.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_glass.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_glass.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_glass.$(SHADEREXT): rage_glass.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_glass.$(SHADEREXT) rage_glass.fx $(SHADERDIR_40NV10)/rage_glass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_glass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_glass.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_glass_cube.$(SHADEREXT)

-include $(SHADERDIR)/rage_glass_cube.d

$(SHADERPATH)$(SHADERDIR)/rage_glass_cube.$(SHADEREXT): rage_glass_cube.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_glass_cube.$(SHADEREXT) rage_glass_cube.fx $(SHADERDIR)/rage_glass_cube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_glass_cube.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_glass_cube.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_glass_cube.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_glass_cube.$(SHADEREXT): rage_glass_cube.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_glass_cube.$(SHADEREXT) rage_glass_cube.fx $(SHADERDIR_30ATI9)/rage_glass_cube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_glass_cube.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_glass_cube.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_glass_cube.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_glass_cube.$(SHADEREXT): rage_glass_cube.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_glass_cube.$(SHADEREXT) rage_glass_cube.fx $(SHADERDIR_30ATI10)/rage_glass_cube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_glass_cube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_glass_cube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_glass_cube.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_glass_cube.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_glass_cube.$(SHADEREXT): rage_glass_cube.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_glass_cube.$(SHADEREXT) rage_glass_cube.fx $(SHADERDIR_30NV9)/rage_glass_cube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_glass_cube.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_glass_cube.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_glass_cube.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_glass_cube.$(SHADEREXT): rage_glass_cube.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_glass_cube.$(SHADEREXT) rage_glass_cube.fx $(SHADERDIR_30NV10)/rage_glass_cube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_glass_cube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_glass_cube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_glass_cube.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_glass_cube.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_glass_cube.$(SHADEREXT): rage_glass_cube.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_glass_cube.$(SHADEREXT) rage_glass_cube.fx $(SHADERDIR_40ATI10)/rage_glass_cube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_glass_cube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_glass_cube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_glass_cube.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_glass_cube.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_glass_cube.$(SHADEREXT): rage_glass_cube.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_glass_cube.$(SHADEREXT) rage_glass_cube.fx $(SHADERDIR_40NV10)/rage_glass_cube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_glass_cube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_glass_cube.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_gtagrass.$(SHADEREXT)

-include $(SHADERDIR)/rage_gtagrass.d

$(SHADERPATH)$(SHADERDIR)/rage_gtagrass.$(SHADEREXT): rage_gtagrass.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_gtagrass.$(SHADEREXT) rage_gtagrass.fx $(SHADERDIR)/rage_gtagrass.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_gtagrass.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gtagrass.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_gtagrass.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gtagrass.$(SHADEREXT): rage_gtagrass.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_gtagrass.$(SHADEREXT) rage_gtagrass.fx $(SHADERDIR_30ATI9)/rage_gtagrass.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_gtagrass.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gtagrass.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_gtagrass.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gtagrass.$(SHADEREXT): rage_gtagrass.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gtagrass.$(SHADEREXT) rage_gtagrass.fx $(SHADERDIR_30ATI10)/rage_gtagrass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_gtagrass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_gtagrass.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gtagrass.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_gtagrass.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_gtagrass.$(SHADEREXT): rage_gtagrass.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_gtagrass.$(SHADEREXT) rage_gtagrass.fx $(SHADERDIR_30NV9)/rage_gtagrass.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_gtagrass.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gtagrass.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_gtagrass.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_gtagrass.$(SHADEREXT): rage_gtagrass.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gtagrass.$(SHADEREXT) rage_gtagrass.fx $(SHADERDIR_30NV10)/rage_gtagrass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_gtagrass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_gtagrass.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gtagrass.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_gtagrass.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gtagrass.$(SHADEREXT): rage_gtagrass.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gtagrass.$(SHADEREXT) rage_gtagrass.fx $(SHADERDIR_40ATI10)/rage_gtagrass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_gtagrass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_gtagrass.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gtagrass.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_gtagrass.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_gtagrass.$(SHADEREXT): rage_gtagrass.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gtagrass.$(SHADEREXT) rage_gtagrass.fx $(SHADERDIR_40NV10)/rage_gtagrass.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_gtagrass.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_gtagrass.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_im.$(SHADEREXT)

-include $(SHADERDIR)/rage_im.d

$(SHADERPATH)$(SHADERDIR)/rage_im.$(SHADEREXT): rage_im.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_im.$(SHADEREXT) rage_im.fx $(SHADERDIR)/rage_im.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_im.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_im.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_im.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_im.$(SHADEREXT): rage_im.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_im.$(SHADEREXT) rage_im.fx $(SHADERDIR_30ATI9)/rage_im.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_im.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_im.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_im.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_im.$(SHADEREXT): rage_im.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_im.$(SHADEREXT) rage_im.fx $(SHADERDIR_30ATI10)/rage_im.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_im.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_im.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_im.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_im.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_im.$(SHADEREXT): rage_im.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_im.$(SHADEREXT) rage_im.fx $(SHADERDIR_30NV9)/rage_im.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_im.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_im.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_im.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_im.$(SHADEREXT): rage_im.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_im.$(SHADEREXT) rage_im.fx $(SHADERDIR_30NV10)/rage_im.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_im.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_im.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_im.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_im.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_im.$(SHADEREXT): rage_im.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_im.$(SHADEREXT) rage_im.fx $(SHADERDIR_40ATI10)/rage_im.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_im.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_im.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_im.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_im.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_im.$(SHADEREXT): rage_im.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_im.$(SHADEREXT) rage_im.fx $(SHADERDIR_40NV10)/rage_im.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_im.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_im.$(SHADEREXT)

endif


TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_lashes.$(SHADEREXT)

-include $(SHADERDIR)/rage_lashes.d

$(SHADERPATH)$(SHADERDIR)/rage_lashes.$(SHADEREXT): rage_lashes.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_lashes.$(SHADEREXT) rage_lashes.fx $(SHADERDIR)/rage_lashes.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_lashes.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_lashes.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_lashes.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_lashes.$(SHADEREXT): rage_lashes.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_lashes.$(SHADEREXT) rage_lashes.fx $(SHADERDIR_30ATI9)/rage_lashes.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_lashes.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_lashes.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_lashes.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_lashes.$(SHADEREXT): rage_lashes.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_lashes.$(SHADEREXT) rage_lashes.fx $(SHADERDIR_30ATI10)/rage_lashes.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_lashes.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_lashes.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_lashes.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_lashes.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_lashes.$(SHADEREXT): rage_lashes.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_lashes.$(SHADEREXT) rage_lashes.fx $(SHADERDIR_30NV9)/rage_lashes.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_lashes.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_lashes.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_lashes.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_lashes.$(SHADEREXT): rage_lashes.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_lashes.$(SHADEREXT) rage_lashes.fx $(SHADERDIR_30NV10)/rage_lashes.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_lashes.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_lashes.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_lashes.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_lashes.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_lashes.$(SHADEREXT): rage_lashes.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_lashes.$(SHADEREXT) rage_lashes.fx $(SHADERDIR_40ATI10)/rage_lashes.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_lashes.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_lashes.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_lashes.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_lashes.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_lashes.$(SHADEREXT): rage_lashes.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_lashes.$(SHADEREXT) rage_lashes.fx $(SHADERDIR_40NV10)/rage_lashes.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_lashes.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_lashes.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_layer_2.$(SHADEREXT)

-include $(SHADERDIR)/rage_layer_2.d

$(SHADERPATH)$(SHADERDIR)/rage_layer_2.$(SHADEREXT): rage_layer_2.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_layer_2.$(SHADEREXT) rage_layer_2.fx $(SHADERDIR)/rage_layer_2.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_layer_2.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_layer_2.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_layer_2.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_layer_2.$(SHADEREXT): rage_layer_2.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_layer_2.$(SHADEREXT) rage_layer_2.fx $(SHADERDIR_30ATI9)/rage_layer_2.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_layer_2.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_layer_2.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_layer_2.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_layer_2.$(SHADEREXT): rage_layer_2.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_layer_2.$(SHADEREXT) rage_layer_2.fx $(SHADERDIR_30ATI10)/rage_layer_2.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_layer_2.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_layer_2.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_layer_2.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_layer_2.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_layer_2.$(SHADEREXT): rage_layer_2.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_layer_2.$(SHADEREXT) rage_layer_2.fx $(SHADERDIR_30NV9)/rage_layer_2.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_layer_2.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_layer_2.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_layer_2.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_layer_2.$(SHADEREXT): rage_layer_2.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_layer_2.$(SHADEREXT) rage_layer_2.fx $(SHADERDIR_30NV10)/rage_layer_2.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_layer_2.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_layer_2.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_layer_2.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_layer_2.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_layer_2.$(SHADEREXT): rage_layer_2.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_layer_2.$(SHADEREXT) rage_layer_2.fx $(SHADERDIR_40ATI10)/rage_layer_2.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_layer_2.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_layer_2.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_layer_2.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_layer_2.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_layer_2.$(SHADEREXT): rage_layer_2.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_layer_2.$(SHADEREXT) rage_layer_2.fx $(SHADERDIR_40NV10)/rage_layer_2.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_layer_2.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_layer_2.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha.$(SHADEREXT)

-include $(SHADERDIR)/rage_mc4character_alpha.d

$(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha.$(SHADEREXT): rage_mc4character_alpha.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha.$(SHADEREXT) rage_mc4character_alpha.fx $(SHADERDIR)/rage_mc4character_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mc4character_alpha.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mc4character_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha.$(SHADEREXT): rage_mc4character_alpha.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha.$(SHADEREXT) rage_mc4character_alpha.fx $(SHADERDIR_30ATI9)/rage_mc4character_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mc4character_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mc4character_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha.$(SHADEREXT): rage_mc4character_alpha.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha.$(SHADEREXT) rage_mc4character_alpha.fx $(SHADERDIR_30ATI10)/rage_mc4character_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mc4character_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mc4character_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha.$(SHADEREXT): rage_mc4character_alpha.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha.$(SHADEREXT) rage_mc4character_alpha.fx $(SHADERDIR_30NV9)/rage_mc4character_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mc4character_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mc4character_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha.$(SHADEREXT): rage_mc4character_alpha.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha.$(SHADEREXT) rage_mc4character_alpha.fx $(SHADERDIR_30NV10)/rage_mc4character_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mc4character_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mc4character_alpha.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha.$(SHADEREXT): rage_mc4character_alpha.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha.$(SHADEREXT) rage_mc4character_alpha.fx $(SHADERDIR_40ATI10)/rage_mc4character_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mc4character_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mc4character_alpha.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha.$(SHADEREXT): rage_mc4character_alpha.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha.$(SHADEREXT) rage_mc4character_alpha.fx $(SHADERDIR_40NV10)/rage_mc4character_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mc4character_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

-include $(SHADERDIR)/rage_mc4character_alpha_doublesided.d

$(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha_doublesided.$(SHADEREXT): rage_mc4character_alpha_doublesided.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha_doublesided.$(SHADEREXT) rage_mc4character_alpha_doublesided.fx $(SHADERDIR)/rage_mc4character_alpha_doublesided.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mc4character_alpha_doublesided.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mc4character_alpha_doublesided.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha_doublesided.$(SHADEREXT): rage_mc4character_alpha_doublesided.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha_doublesided.$(SHADEREXT) rage_mc4character_alpha_doublesided.fx $(SHADERDIR_30ATI9)/rage_mc4character_alpha_doublesided.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mc4character_alpha_doublesided.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mc4character_alpha_doublesided.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_doublesided.$(SHADEREXT): rage_mc4character_alpha_doublesided.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_doublesided.$(SHADEREXT) rage_mc4character_alpha_doublesided.fx $(SHADERDIR_30ATI10)/rage_mc4character_alpha_doublesided.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mc4character_alpha_doublesided.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mc4character_alpha_doublesided.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha_doublesided.$(SHADEREXT): rage_mc4character_alpha_doublesided.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha_doublesided.$(SHADEREXT) rage_mc4character_alpha_doublesided.fx $(SHADERDIR_30NV9)/rage_mc4character_alpha_doublesided.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mc4character_alpha_doublesided.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mc4character_alpha_doublesided.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_doublesided.$(SHADEREXT): rage_mc4character_alpha_doublesided.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_doublesided.$(SHADEREXT) rage_mc4character_alpha_doublesided.fx $(SHADERDIR_30NV10)/rage_mc4character_alpha_doublesided.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mc4character_alpha_doublesided.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mc4character_alpha_doublesided.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_doublesided.$(SHADEREXT): rage_mc4character_alpha_doublesided.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_doublesided.$(SHADEREXT) rage_mc4character_alpha_doublesided.fx $(SHADERDIR_40ATI10)/rage_mc4character_alpha_doublesided.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mc4character_alpha_doublesided.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mc4character_alpha_doublesided.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_doublesided.$(SHADEREXT): rage_mc4character_alpha_doublesided.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_doublesided.$(SHADEREXT) rage_mc4character_alpha_doublesided.fx $(SHADERDIR_40NV10)/rage_mc4character_alpha_doublesided.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mc4character_alpha_doublesided.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_doublesided.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

-include $(SHADERDIR)/rage_mc4character_alpha_normalmap.d

$(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha_normalmap.$(SHADEREXT): rage_mc4character_alpha_normalmap.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha_normalmap.$(SHADEREXT) rage_mc4character_alpha_normalmap.fx $(SHADERDIR)/rage_mc4character_alpha_normalmap.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mc4character_alpha_normalmap.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap.$(SHADEREXT): rage_mc4character_alpha_normalmap.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap.$(SHADEREXT) rage_mc4character_alpha_normalmap.fx $(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mc4character_alpha_normalmap.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap.$(SHADEREXT): rage_mc4character_alpha_normalmap.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap.$(SHADEREXT) rage_mc4character_alpha_normalmap.fx $(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mc4character_alpha_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap.$(SHADEREXT): rage_mc4character_alpha_normalmap.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap.$(SHADEREXT) rage_mc4character_alpha_normalmap.fx $(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mc4character_alpha_normalmap.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap.$(SHADEREXT): rage_mc4character_alpha_normalmap.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap.$(SHADEREXT) rage_mc4character_alpha_normalmap.fx $(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mc4character_alpha_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap.$(SHADEREXT): rage_mc4character_alpha_normalmap.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap.$(SHADEREXT) rage_mc4character_alpha_normalmap.fx $(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mc4character_alpha_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap.$(SHADEREXT): rage_mc4character_alpha_normalmap.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap.$(SHADEREXT) rage_mc4character_alpha_normalmap.fx $(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mc4character_alpha_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

-include $(SHADERDIR)/rage_mc4character_alpha_normalmap_doublesided.d

$(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT): rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT) rage_mc4character_alpha_normalmap_doublesided.fx $(SHADERDIR)/rage_mc4character_alpha_normalmap_doublesided.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mc4character_alpha_normalmap_doublesided.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap_doublesided.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT): rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT) rage_mc4character_alpha_normalmap_doublesided.fx $(SHADERDIR_30ATI9)/rage_mc4character_alpha_normalmap_doublesided.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mc4character_alpha_normalmap_doublesided.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap_doublesided.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT): rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT) rage_mc4character_alpha_normalmap_doublesided.fx $(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap_doublesided.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap_doublesided.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT): rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT) rage_mc4character_alpha_normalmap_doublesided.fx $(SHADERDIR_30NV9)/rage_mc4character_alpha_normalmap_doublesided.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mc4character_alpha_normalmap_doublesided.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap_doublesided.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT): rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT) rage_mc4character_alpha_normalmap_doublesided.fx $(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap_doublesided.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap_doublesided.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT): rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT) rage_mc4character_alpha_normalmap_doublesided.fx $(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap_doublesided.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap_doublesided.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT): rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT) rage_mc4character_alpha_normalmap_doublesided.fx $(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap_doublesided.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mc4character_alpha_normalmap_doublesided.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_alpha_normalmap_doublesided.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mc4character_hair_blendshape.$(SHADEREXT)

-include $(SHADERDIR)/rage_mc4character_hair_blendshape.d

$(SHADERPATH)$(SHADERDIR)/rage_mc4character_hair_blendshape.$(SHADEREXT): rage_mc4character_hair_blendshape.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mc4character_hair_blendshape.$(SHADEREXT) rage_mc4character_hair_blendshape.fx $(SHADERDIR)/rage_mc4character_hair_blendshape.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mc4character_hair_blendshape.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_hair_blendshape.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mc4character_hair_blendshape.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_hair_blendshape.$(SHADEREXT): rage_mc4character_hair_blendshape.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_hair_blendshape.$(SHADEREXT) rage_mc4character_hair_blendshape.fx $(SHADERDIR_30ATI9)/rage_mc4character_hair_blendshape.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mc4character_hair_blendshape.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_hair_blendshape.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mc4character_hair_blendshape.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_hair_blendshape.$(SHADEREXT): rage_mc4character_hair_blendshape.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_hair_blendshape.$(SHADEREXT) rage_mc4character_hair_blendshape.fx $(SHADERDIR_30ATI10)/rage_mc4character_hair_blendshape.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mc4character_hair_blendshape.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_hair_blendshape.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_hair_blendshape.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mc4character_hair_blendshape.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_hair_blendshape.$(SHADEREXT): rage_mc4character_hair_blendshape.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_hair_blendshape.$(SHADEREXT) rage_mc4character_hair_blendshape.fx $(SHADERDIR_30NV9)/rage_mc4character_hair_blendshape.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mc4character_hair_blendshape.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_hair_blendshape.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mc4character_hair_blendshape.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_hair_blendshape.$(SHADEREXT): rage_mc4character_hair_blendshape.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_hair_blendshape.$(SHADEREXT) rage_mc4character_hair_blendshape.fx $(SHADERDIR_30NV10)/rage_mc4character_hair_blendshape.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mc4character_hair_blendshape.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_hair_blendshape.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_hair_blendshape.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mc4character_hair_blendshape.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_hair_blendshape.$(SHADEREXT): rage_mc4character_hair_blendshape.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_hair_blendshape.$(SHADEREXT) rage_mc4character_hair_blendshape.fx $(SHADERDIR_40ATI10)/rage_mc4character_hair_blendshape.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mc4character_hair_blendshape.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_hair_blendshape.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_hair_blendshape.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mc4character_hair_blendshape.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_hair_blendshape.$(SHADEREXT): rage_mc4character_hair_blendshape.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_hair_blendshape.$(SHADEREXT) rage_mc4character_hair_blendshape.fx $(SHADERDIR_40NV10)/rage_mc4character_hair_blendshape.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mc4character_hair_blendshape.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_hair_blendshape.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mc4character_normalmap.$(SHADEREXT)

-include $(SHADERDIR)/rage_mc4character_normalmap.d

$(SHADERPATH)$(SHADERDIR)/rage_mc4character_normalmap.$(SHADEREXT): rage_mc4character_normalmap.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mc4character_normalmap.$(SHADEREXT) rage_mc4character_normalmap.fx $(SHADERDIR)/rage_mc4character_normalmap.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mc4character_normalmap.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mc4character_normalmap.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_normalmap.$(SHADEREXT): rage_mc4character_normalmap.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_normalmap.$(SHADEREXT) rage_mc4character_normalmap.fx $(SHADERDIR_30ATI9)/rage_mc4character_normalmap.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mc4character_normalmap.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mc4character_normalmap.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_normalmap.$(SHADEREXT): rage_mc4character_normalmap.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_normalmap.$(SHADEREXT) rage_mc4character_normalmap.fx $(SHADERDIR_30ATI10)/rage_mc4character_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mc4character_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_normalmap.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mc4character_normalmap.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_normalmap.$(SHADEREXT): rage_mc4character_normalmap.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_normalmap.$(SHADEREXT) rage_mc4character_normalmap.fx $(SHADERDIR_30NV9)/rage_mc4character_normalmap.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mc4character_normalmap.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mc4character_normalmap.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_normalmap.$(SHADEREXT): rage_mc4character_normalmap.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_normalmap.$(SHADEREXT) rage_mc4character_normalmap.fx $(SHADERDIR_30NV10)/rage_mc4character_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mc4character_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_normalmap.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_normalmap.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mc4character_normalmap.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_normalmap.$(SHADEREXT): rage_mc4character_normalmap.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_normalmap.$(SHADEREXT) rage_mc4character_normalmap.fx $(SHADERDIR_40ATI10)/rage_mc4character_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mc4character_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_normalmap.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_normalmap.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mc4character_normalmap.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_normalmap.$(SHADEREXT): rage_mc4character_normalmap.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_normalmap.$(SHADEREXT) rage_mc4character_normalmap.fx $(SHADERDIR_40NV10)/rage_mc4character_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mc4character_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_normalmap.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mc4character_skin_blendshape.$(SHADEREXT)

-include $(SHADERDIR)/rage_mc4character_skin_blendshape.d

$(SHADERPATH)$(SHADERDIR)/rage_mc4character_skin_blendshape.$(SHADEREXT): rage_mc4character_skin_blendshape.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mc4character_skin_blendshape.$(SHADEREXT) rage_mc4character_skin_blendshape.fx $(SHADERDIR)/rage_mc4character_skin_blendshape.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mc4character_skin_blendshape.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape.$(SHADEREXT): rage_mc4character_skin_blendshape.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape.$(SHADEREXT) rage_mc4character_skin_blendshape.fx $(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mc4character_skin_blendshape.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape.$(SHADEREXT): rage_mc4character_skin_blendshape.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape.$(SHADEREXT) rage_mc4character_skin_blendshape.fx $(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mc4character_skin_blendshape.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape.$(SHADEREXT): rage_mc4character_skin_blendshape.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape.$(SHADEREXT) rage_mc4character_skin_blendshape.fx $(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mc4character_skin_blendshape.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape.$(SHADEREXT): rage_mc4character_skin_blendshape.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape.$(SHADEREXT) rage_mc4character_skin_blendshape.fx $(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mc4character_skin_blendshape.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape.$(SHADEREXT): rage_mc4character_skin_blendshape.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape.$(SHADEREXT) rage_mc4character_skin_blendshape.fx $(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mc4character_skin_blendshape.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape.$(SHADEREXT): rage_mc4character_skin_blendshape.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape.$(SHADEREXT) rage_mc4character_skin_blendshape.fx $(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mc4character_skin_blendshape.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

-include $(SHADERDIR)/rage_mc4character_skin_blendshape_normalmap.d

$(SHADERPATH)$(SHADERDIR)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT): rage_mc4character_skin_blendshape_normalmap.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT) rage_mc4character_skin_blendshape_normalmap.fx $(SHADERDIR)/rage_mc4character_skin_blendshape_normalmap.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mc4character_skin_blendshape_normalmap.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape_normalmap.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT): rage_mc4character_skin_blendshape_normalmap.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT) rage_mc4character_skin_blendshape_normalmap.fx $(SHADERDIR_30ATI9)/rage_mc4character_skin_blendshape_normalmap.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mc4character_skin_blendshape_normalmap.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape_normalmap.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT): rage_mc4character_skin_blendshape_normalmap.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT) rage_mc4character_skin_blendshape_normalmap.fx $(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mc4character_skin_blendshape_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape_normalmap.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT): rage_mc4character_skin_blendshape_normalmap.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT) rage_mc4character_skin_blendshape_normalmap.fx $(SHADERDIR_30NV9)/rage_mc4character_skin_blendshape_normalmap.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mc4character_skin_blendshape_normalmap.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape_normalmap.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT): rage_mc4character_skin_blendshape_normalmap.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT) rage_mc4character_skin_blendshape_normalmap.fx $(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mc4character_skin_blendshape_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape_normalmap.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT): rage_mc4character_skin_blendshape_normalmap.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT) rage_mc4character_skin_blendshape_normalmap.fx $(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mc4character_skin_blendshape_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape_normalmap.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT): rage_mc4character_skin_blendshape_normalmap.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT) rage_mc4character_skin_blendshape_normalmap.fx $(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape_normalmap.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mc4character_skin_blendshape_normalmap.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mc4character_skin_blendshape_normalmap.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_memexport.$(SHADEREXT)

-include $(SHADERDIR)/rage_memexport.d

$(SHADERPATH)$(SHADERDIR)/rage_memexport.$(SHADEREXT): rage_memexport.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_memexport.$(SHADEREXT) rage_memexport.fx $(SHADERDIR)/rage_memexport.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_memexport.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_memexport.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_memexport.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_memexport.$(SHADEREXT): rage_memexport.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_memexport.$(SHADEREXT) rage_memexport.fx $(SHADERDIR_30ATI9)/rage_memexport.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_memexport.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_memexport.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_memexport.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_memexport.$(SHADEREXT): rage_memexport.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_memexport.$(SHADEREXT) rage_memexport.fx $(SHADERDIR_30ATI10)/rage_memexport.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_memexport.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_memexport.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_memexport.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_memexport.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_memexport.$(SHADEREXT): rage_memexport.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_memexport.$(SHADEREXT) rage_memexport.fx $(SHADERDIR_30NV9)/rage_memexport.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_memexport.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_memexport.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_memexport.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_memexport.$(SHADEREXT): rage_memexport.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_memexport.$(SHADEREXT) rage_memexport.fx $(SHADERDIR_30NV10)/rage_memexport.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_memexport.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_memexport.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_memexport.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_memexport.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_memexport.$(SHADEREXT): rage_memexport.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_memexport.$(SHADEREXT) rage_memexport.fx $(SHADERDIR_40ATI10)/rage_memexport.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_memexport.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_memexport.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_memexport.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_memexport.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_memexport.$(SHADEREXT): rage_memexport.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_memexport.$(SHADEREXT) rage_memexport.fx $(SHADERDIR_40NV10)/rage_memexport.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_memexport.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_memexport.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mk_demo.$(SHADEREXT)

-include $(SHADERDIR)/rage_mk_demo.d

$(SHADERPATH)$(SHADERDIR)/rage_mk_demo.$(SHADEREXT): rage_mk_demo.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mk_demo.$(SHADEREXT) rage_mk_demo.fx $(SHADERDIR)/rage_mk_demo.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mk_demo.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mk_demo.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mk_demo.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mk_demo.$(SHADEREXT): rage_mk_demo.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mk_demo.$(SHADEREXT) rage_mk_demo.fx $(SHADERDIR_30ATI9)/rage_mk_demo.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mk_demo.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mk_demo.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mk_demo.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mk_demo.$(SHADEREXT): rage_mk_demo.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mk_demo.$(SHADEREXT) rage_mk_demo.fx $(SHADERDIR_30ATI10)/rage_mk_demo.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mk_demo.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mk_demo.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mk_demo.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mk_demo.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mk_demo.$(SHADEREXT): rage_mk_demo.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mk_demo.$(SHADEREXT) rage_mk_demo.fx $(SHADERDIR_30NV9)/rage_mk_demo.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mk_demo.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mk_demo.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mk_demo.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mk_demo.$(SHADEREXT): rage_mk_demo.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mk_demo.$(SHADEREXT) rage_mk_demo.fx $(SHADERDIR_30NV10)/rage_mk_demo.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mk_demo.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mk_demo.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mk_demo.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mk_demo.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mk_demo.$(SHADEREXT): rage_mk_demo.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mk_demo.$(SHADEREXT) rage_mk_demo.fx $(SHADERDIR_40ATI10)/rage_mk_demo.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mk_demo.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mk_demo.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mk_demo.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mk_demo.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mk_demo.$(SHADEREXT): rage_mk_demo.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mk_demo.$(SHADEREXT) rage_mk_demo.fx $(SHADERDIR_40NV10)/rage_mk_demo.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mk_demo.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mk_demo.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_nodraw.$(SHADEREXT)

-include $(SHADERDIR)/rage_nodraw.d

$(SHADERPATH)$(SHADERDIR)/rage_nodraw.$(SHADEREXT): rage_nodraw.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_nodraw.$(SHADEREXT) rage_nodraw.fx $(SHADERDIR)/rage_nodraw.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_nodraw.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_nodraw.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_nodraw.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_nodraw.$(SHADEREXT): rage_nodraw.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_nodraw.$(SHADEREXT) rage_nodraw.fx $(SHADERDIR_30ATI9)/rage_nodraw.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_nodraw.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_nodraw.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_nodraw.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_nodraw.$(SHADEREXT): rage_nodraw.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_nodraw.$(SHADEREXT) rage_nodraw.fx $(SHADERDIR_30ATI10)/rage_nodraw.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_nodraw.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_nodraw.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_nodraw.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_nodraw.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_nodraw.$(SHADEREXT): rage_nodraw.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_nodraw.$(SHADEREXT) rage_nodraw.fx $(SHADERDIR_30NV9)/rage_nodraw.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_nodraw.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_nodraw.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_nodraw.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_nodraw.$(SHADEREXT): rage_nodraw.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_nodraw.$(SHADEREXT) rage_nodraw.fx $(SHADERDIR_30NV10)/rage_nodraw.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_nodraw.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_nodraw.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_nodraw.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_nodraw.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_nodraw.$(SHADEREXT): rage_nodraw.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_nodraw.$(SHADEREXT) rage_nodraw.fx $(SHADERDIR_40ATI10)/rage_nodraw.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_nodraw.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_nodraw.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_nodraw.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_nodraw.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_nodraw.$(SHADEREXT): rage_nodraw.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_nodraw.$(SHADEREXT) rage_nodraw.fx $(SHADERDIR_40NV10)/rage_nodraw.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_nodraw.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_nodraw.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_parallax.$(SHADEREXT)

-include $(SHADERDIR)/rage_parallax.d

$(SHADERPATH)$(SHADERDIR)/rage_parallax.$(SHADEREXT): rage_parallax.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_parallax.$(SHADEREXT) rage_parallax.fx $(SHADERDIR)/rage_parallax.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_parallax.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_parallax.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_parallax.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_parallax.$(SHADEREXT): rage_parallax.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_parallax.$(SHADEREXT) rage_parallax.fx $(SHADERDIR_30ATI9)/rage_parallax.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_parallax.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_parallax.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_parallax.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_parallax.$(SHADEREXT): rage_parallax.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_parallax.$(SHADEREXT) rage_parallax.fx $(SHADERDIR_30ATI10)/rage_parallax.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_parallax.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_parallax.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_parallax.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_parallax.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_parallax.$(SHADEREXT): rage_parallax.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_parallax.$(SHADEREXT) rage_parallax.fx $(SHADERDIR_30NV9)/rage_parallax.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_parallax.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_parallax.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_parallax.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_parallax.$(SHADEREXT): rage_parallax.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_parallax.$(SHADEREXT) rage_parallax.fx $(SHADERDIR_30NV10)/rage_parallax.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_parallax.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_parallax.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_parallax.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_parallax.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_parallax.$(SHADEREXT): rage_parallax.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_parallax.$(SHADEREXT) rage_parallax.fx $(SHADERDIR_40ATI10)/rage_parallax.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_parallax.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_parallax.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_parallax.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_parallax.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_parallax.$(SHADEREXT): rage_parallax.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_parallax.$(SHADEREXT) rage_parallax.fx $(SHADERDIR_40NV10)/rage_parallax.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_parallax.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_parallax.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_parallax_old.$(SHADEREXT)

-include $(SHADERDIR)/rage_parallax_old.d

$(SHADERPATH)$(SHADERDIR)/rage_parallax_old.$(SHADEREXT): rage_parallax_old.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_parallax_old.$(SHADEREXT) rage_parallax_old.fx $(SHADERDIR)/rage_parallax_old.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_parallax_old.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_parallax_old.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_parallax_old.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_parallax_old.$(SHADEREXT): rage_parallax_old.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_parallax_old.$(SHADEREXT) rage_parallax_old.fx $(SHADERDIR_30ATI9)/rage_parallax_old.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_parallax_old.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_parallax_old.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_parallax_old.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_parallax_old.$(SHADEREXT): rage_parallax_old.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_parallax_old.$(SHADEREXT) rage_parallax_old.fx $(SHADERDIR_30ATI10)/rage_parallax_old.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_parallax_old.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_parallax_old.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_parallax_old.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_parallax_old.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_parallax_old.$(SHADEREXT): rage_parallax_old.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_parallax_old.$(SHADEREXT) rage_parallax_old.fx $(SHADERDIR_30NV9)/rage_parallax_old.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_parallax_old.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_parallax_old.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_parallax_old.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_parallax_old.$(SHADEREXT): rage_parallax_old.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_parallax_old.$(SHADEREXT) rage_parallax_old.fx $(SHADERDIR_30NV10)/rage_parallax_old.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_parallax_old.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_parallax_old.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_parallax_old.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_parallax_old.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_parallax_old.$(SHADEREXT): rage_parallax_old.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_parallax_old.$(SHADEREXT) rage_parallax_old.fx $(SHADERDIR_40ATI10)/rage_parallax_old.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_parallax_old.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_parallax_old.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_parallax_old.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_parallax_old.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_parallax_old.$(SHADEREXT): rage_parallax_old.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_parallax_old.$(SHADEREXT) rage_parallax_old.fx $(SHADERDIR_40NV10)/rage_parallax_old.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_parallax_old.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_parallax_old.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_rdr2_skin_landbaron.$(SHADEREXT)

-include $(SHADERDIR)/rage_rdr2_skin_landbaron.d

$(SHADERPATH)$(SHADERDIR)/rage_rdr2_skin_landbaron.$(SHADEREXT): rage_rdr2_skin_landbaron.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_rdr2_skin_landbaron.$(SHADEREXT) rage_rdr2_skin_landbaron.fx $(SHADERDIR)/rage_rdr2_skin_landbaron.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_rdr2_skin_landbaron.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_rdr2_skin_landbaron.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_rdr2_skin_landbaron.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_rdr2_skin_landbaron.$(SHADEREXT): rage_rdr2_skin_landbaron.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_rdr2_skin_landbaron.$(SHADEREXT) rage_rdr2_skin_landbaron.fx $(SHADERDIR_30ATI9)/rage_rdr2_skin_landbaron.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_rdr2_skin_landbaron.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_rdr2_skin_landbaron.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_rdr2_skin_landbaron.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_rdr2_skin_landbaron.$(SHADEREXT): rage_rdr2_skin_landbaron.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_rdr2_skin_landbaron.$(SHADEREXT) rage_rdr2_skin_landbaron.fx $(SHADERDIR_30ATI10)/rage_rdr2_skin_landbaron.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_rdr2_skin_landbaron.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_rdr2_skin_landbaron.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_rdr2_skin_landbaron.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_rdr2_skin_landbaron.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_rdr2_skin_landbaron.$(SHADEREXT): rage_rdr2_skin_landbaron.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_rdr2_skin_landbaron.$(SHADEREXT) rage_rdr2_skin_landbaron.fx $(SHADERDIR_30NV9)/rage_rdr2_skin_landbaron.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_rdr2_skin_landbaron.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_rdr2_skin_landbaron.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_rdr2_skin_landbaron.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_rdr2_skin_landbaron.$(SHADEREXT): rage_rdr2_skin_landbaron.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_rdr2_skin_landbaron.$(SHADEREXT) rage_rdr2_skin_landbaron.fx $(SHADERDIR_30NV10)/rage_rdr2_skin_landbaron.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_rdr2_skin_landbaron.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_rdr2_skin_landbaron.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_rdr2_skin_landbaron.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_rdr2_skin_landbaron.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_rdr2_skin_landbaron.$(SHADEREXT): rage_rdr2_skin_landbaron.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_rdr2_skin_landbaron.$(SHADEREXT) rage_rdr2_skin_landbaron.fx $(SHADERDIR_40ATI10)/rage_rdr2_skin_landbaron.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_rdr2_skin_landbaron.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_rdr2_skin_landbaron.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_rdr2_skin_landbaron.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_rdr2_skin_landbaron.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_rdr2_skin_landbaron.$(SHADEREXT): rage_rdr2_skin_landbaron.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_rdr2_skin_landbaron.$(SHADEREXT) rage_rdr2_skin_landbaron.fx $(SHADERDIR_40NV10)/rage_rdr2_skin_landbaron.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_rdr2_skin_landbaron.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_rdr2_skin_landbaron.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_roughSurface.$(SHADEREXT)

-include $(SHADERDIR)/rage_roughSurface.d

$(SHADERPATH)$(SHADERDIR)/rage_roughSurface.$(SHADEREXT): rage_roughSurface.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_roughSurface.$(SHADEREXT) rage_roughSurface.fx $(SHADERDIR)/rage_roughSurface.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_roughSurface.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_roughSurface.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_roughSurface.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_roughSurface.$(SHADEREXT): rage_roughSurface.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_roughSurface.$(SHADEREXT) rage_roughSurface.fx $(SHADERDIR_30ATI9)/rage_roughSurface.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_roughSurface.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_roughSurface.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_roughSurface.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_roughSurface.$(SHADEREXT): rage_roughSurface.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_roughSurface.$(SHADEREXT) rage_roughSurface.fx $(SHADERDIR_30ATI10)/rage_roughSurface.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_roughSurface.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_roughSurface.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_roughSurface.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_roughSurface.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_roughSurface.$(SHADEREXT): rage_roughSurface.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_roughSurface.$(SHADEREXT) rage_roughSurface.fx $(SHADERDIR_30NV9)/rage_roughSurface.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_roughSurface.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_roughSurface.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_roughSurface.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_roughSurface.$(SHADEREXT): rage_roughSurface.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_roughSurface.$(SHADEREXT) rage_roughSurface.fx $(SHADERDIR_30NV10)/rage_roughSurface.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_roughSurface.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_roughSurface.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_roughSurface.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_roughSurface.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_roughSurface.$(SHADEREXT): rage_roughSurface.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_roughSurface.$(SHADEREXT) rage_roughSurface.fx $(SHADERDIR_40ATI10)/rage_roughSurface.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_roughSurface.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_roughSurface.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_roughSurface.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_roughSurface.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_roughSurface.$(SHADEREXT): rage_roughSurface.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_roughSurface.$(SHADEREXT) rage_roughSurface.fx $(SHADERDIR_40NV10)/rage_roughSurface.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_roughSurface.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_roughSurface.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_shadow_bump_spec.$(SHADEREXT)

-include $(SHADERDIR)/rage_shadow_bump_spec.d

$(SHADERPATH)$(SHADERDIR)/rage_shadow_bump_spec.$(SHADEREXT): rage_shadow_bump_spec.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_shadow_bump_spec.$(SHADEREXT) rage_shadow_bump_spec.fx $(SHADERDIR)/rage_shadow_bump_spec.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_shadow_bump_spec.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_shadow_bump_spec.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_bump_spec.$(SHADEREXT): rage_shadow_bump_spec.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_bump_spec.$(SHADEREXT) rage_shadow_bump_spec.fx $(SHADERDIR_30ATI9)/rage_shadow_bump_spec.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_shadow_bump_spec.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_shadow_bump_spec.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_bump_spec.$(SHADEREXT): rage_shadow_bump_spec.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_bump_spec.$(SHADEREXT) rage_shadow_bump_spec.fx $(SHADERDIR_30ATI10)/rage_shadow_bump_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_shadow_bump_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_bump_spec.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_shadow_bump_spec.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_bump_spec.$(SHADEREXT): rage_shadow_bump_spec.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_bump_spec.$(SHADEREXT) rage_shadow_bump_spec.fx $(SHADERDIR_30NV9)/rage_shadow_bump_spec.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_shadow_bump_spec.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_shadow_bump_spec.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_bump_spec.$(SHADEREXT): rage_shadow_bump_spec.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_bump_spec.$(SHADEREXT) rage_shadow_bump_spec.fx $(SHADERDIR_30NV10)/rage_shadow_bump_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_shadow_bump_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_bump_spec.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_shadow_bump_spec.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_bump_spec.$(SHADEREXT): rage_shadow_bump_spec.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_bump_spec.$(SHADEREXT) rage_shadow_bump_spec.fx $(SHADERDIR_40ATI10)/rage_shadow_bump_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_shadow_bump_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_bump_spec.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_bump_spec.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_shadow_bump_spec.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_bump_spec.$(SHADEREXT): rage_shadow_bump_spec.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_bump_spec.$(SHADEREXT) rage_shadow_bump_spec.fx $(SHADERDIR_40NV10)/rage_shadow_bump_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_shadow_bump_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_bump_spec.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR)/rage_shadow_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT): rage_shadow_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT) rage_shadow_bump_spec_reflect_alpha.fx $(SHADERDIR)/rage_shadow_bump_spec_reflect_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_shadow_bump_spec_reflect_alpha.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_shadow_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT): rage_shadow_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT) rage_shadow_bump_spec_reflect_alpha.fx $(SHADERDIR_30ATI9)/rage_shadow_bump_spec_reflect_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_shadow_bump_spec_reflect_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_shadow_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT): rage_shadow_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT) rage_shadow_bump_spec_reflect_alpha.fx $(SHADERDIR_30ATI10)/rage_shadow_bump_spec_reflect_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_shadow_bump_spec_reflect_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_shadow_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT): rage_shadow_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT) rage_shadow_bump_spec_reflect_alpha.fx $(SHADERDIR_30NV9)/rage_shadow_bump_spec_reflect_alpha.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_shadow_bump_spec_reflect_alpha.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_shadow_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT): rage_shadow_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT) rage_shadow_bump_spec_reflect_alpha.fx $(SHADERDIR_30NV10)/rage_shadow_bump_spec_reflect_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_shadow_bump_spec_reflect_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_shadow_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT): rage_shadow_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT) rage_shadow_bump_spec_reflect_alpha.fx $(SHADERDIR_40ATI10)/rage_shadow_bump_spec_reflect_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_shadow_bump_spec_reflect_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_shadow_bump_spec_reflect_alpha.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT): rage_shadow_bump_spec_reflect_alpha.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT) rage_shadow_bump_spec_reflect_alpha.fx $(SHADERDIR_40NV10)/rage_shadow_bump_spec_reflect_alpha.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_shadow_bump_spec_reflect_alpha.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_bump_spec_reflect_alpha.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_shadow_decal.$(SHADEREXT)

-include $(SHADERDIR)/rage_shadow_decal.d

$(SHADERPATH)$(SHADERDIR)/rage_shadow_decal.$(SHADEREXT): rage_shadow_decal.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_shadow_decal.$(SHADEREXT) rage_shadow_decal.fx $(SHADERDIR)/rage_shadow_decal.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_shadow_decal.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_decal.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_shadow_decal.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_decal.$(SHADEREXT): rage_shadow_decal.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_decal.$(SHADEREXT) rage_shadow_decal.fx $(SHADERDIR_30ATI9)/rage_shadow_decal.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_shadow_decal.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_decal.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_shadow_decal.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_decal.$(SHADEREXT): rage_shadow_decal.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_decal.$(SHADEREXT) rage_shadow_decal.fx $(SHADERDIR_30ATI10)/rage_shadow_decal.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_shadow_decal.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_decal.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_decal.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_shadow_decal.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_decal.$(SHADEREXT): rage_shadow_decal.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_decal.$(SHADEREXT) rage_shadow_decal.fx $(SHADERDIR_30NV9)/rage_shadow_decal.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_shadow_decal.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_decal.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_shadow_decal.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_decal.$(SHADEREXT): rage_shadow_decal.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_decal.$(SHADEREXT) rage_shadow_decal.fx $(SHADERDIR_30NV10)/rage_shadow_decal.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_shadow_decal.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_decal.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_decal.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_shadow_decal.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_decal.$(SHADEREXT): rage_shadow_decal.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_decal.$(SHADEREXT) rage_shadow_decal.fx $(SHADERDIR_40ATI10)/rage_shadow_decal.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_shadow_decal.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_decal.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_decal.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_shadow_decal.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_decal.$(SHADEREXT): rage_shadow_decal.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_decal.$(SHADEREXT) rage_shadow_decal.fx $(SHADERDIR_40NV10)/rage_shadow_decal.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_shadow_decal.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_decal.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_shadow_default.$(SHADEREXT)

-include $(SHADERDIR)/rage_shadow_default.d

$(SHADERPATH)$(SHADERDIR)/rage_shadow_default.$(SHADEREXT): rage_shadow_default.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_shadow_default.$(SHADEREXT) rage_shadow_default.fx $(SHADERDIR)/rage_shadow_default.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_shadow_default.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_default.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_shadow_default.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_default.$(SHADEREXT): rage_shadow_default.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_default.$(SHADEREXT) rage_shadow_default.fx $(SHADERDIR_30ATI9)/rage_shadow_default.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_shadow_default.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_default.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_shadow_default.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_default.$(SHADEREXT): rage_shadow_default.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_default.$(SHADEREXT) rage_shadow_default.fx $(SHADERDIR_30ATI10)/rage_shadow_default.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_shadow_default.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_default.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_default.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_shadow_default.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_default.$(SHADEREXT): rage_shadow_default.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_default.$(SHADEREXT) rage_shadow_default.fx $(SHADERDIR_30NV9)/rage_shadow_default.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_shadow_default.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_default.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_shadow_default.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_default.$(SHADEREXT): rage_shadow_default.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_default.$(SHADEREXT) rage_shadow_default.fx $(SHADERDIR_30NV10)/rage_shadow_default.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_shadow_default.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_default.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_default.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_shadow_default.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_default.$(SHADEREXT): rage_shadow_default.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_default.$(SHADEREXT) rage_shadow_default.fx $(SHADERDIR_40ATI10)/rage_shadow_default.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_shadow_default.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_default.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_default.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_shadow_default.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_default.$(SHADEREXT): rage_shadow_default.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_default.$(SHADEREXT) rage_shadow_default.fx $(SHADERDIR_40NV10)/rage_shadow_default.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_shadow_default.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_default.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_shadow_emissive.$(SHADEREXT)

-include $(SHADERDIR)/rage_shadow_emissive.d

$(SHADERPATH)$(SHADERDIR)/rage_shadow_emissive.$(SHADEREXT): rage_shadow_emissive.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_shadow_emissive.$(SHADEREXT) rage_shadow_emissive.fx $(SHADERDIR)/rage_shadow_emissive.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_shadow_emissive.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_emissive.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_shadow_emissive.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_emissive.$(SHADEREXT): rage_shadow_emissive.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shadow_emissive.$(SHADEREXT) rage_shadow_emissive.fx $(SHADERDIR_30ATI9)/rage_shadow_emissive.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_shadow_emissive.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_emissive.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_shadow_emissive.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_emissive.$(SHADEREXT): rage_shadow_emissive.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_emissive.$(SHADEREXT) rage_shadow_emissive.fx $(SHADERDIR_30ATI10)/rage_shadow_emissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_shadow_emissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shadow_emissive.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_emissive.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_shadow_emissive.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_emissive.$(SHADEREXT): rage_shadow_emissive.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shadow_emissive.$(SHADEREXT) rage_shadow_emissive.fx $(SHADERDIR_30NV9)/rage_shadow_emissive.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_shadow_emissive.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_emissive.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_shadow_emissive.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_emissive.$(SHADEREXT): rage_shadow_emissive.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_emissive.$(SHADEREXT) rage_shadow_emissive.fx $(SHADERDIR_30NV10)/rage_shadow_emissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_shadow_emissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shadow_emissive.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_emissive.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_shadow_emissive.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_emissive.$(SHADEREXT): rage_shadow_emissive.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_emissive.$(SHADEREXT) rage_shadow_emissive.fx $(SHADERDIR_40ATI10)/rage_shadow_emissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_shadow_emissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shadow_emissive.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_emissive.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_shadow_emissive.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_emissive.$(SHADEREXT): rage_shadow_emissive.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_emissive.$(SHADEREXT) rage_shadow_emissive.fx $(SHADERDIR_40NV10)/rage_shadow_emissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_shadow_emissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shadow_emissive.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_shells.$(SHADEREXT)

-include $(SHADERDIR)/rage_shells.d

$(SHADERPATH)$(SHADERDIR)/rage_shells.$(SHADEREXT): rage_shells.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_shells.$(SHADEREXT) rage_shells.fx $(SHADERDIR)/rage_shells.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_shells.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shells.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_shells.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shells.$(SHADEREXT): rage_shells.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_shells.$(SHADEREXT) rage_shells.fx $(SHADERDIR_30ATI9)/rage_shells.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_shells.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shells.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_shells.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shells.$(SHADEREXT): rage_shells.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shells.$(SHADEREXT) rage_shells.fx $(SHADERDIR_30ATI10)/rage_shells.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_shells.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_shells.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shells.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_shells.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_shells.$(SHADEREXT): rage_shells.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_shells.$(SHADEREXT) rage_shells.fx $(SHADERDIR_30NV9)/rage_shells.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_shells.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shells.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_shells.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_shells.$(SHADEREXT): rage_shells.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shells.$(SHADEREXT) rage_shells.fx $(SHADERDIR_30NV10)/rage_shells.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_shells.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_shells.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shells.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_shells.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shells.$(SHADEREXT): rage_shells.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shells.$(SHADEREXT) rage_shells.fx $(SHADERDIR_40ATI10)/rage_shells.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_shells.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_shells.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shells.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_shells.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_shells.$(SHADEREXT): rage_shells.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shells.$(SHADEREXT) rage_shells.fx $(SHADERDIR_40NV10)/rage_shells.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_shells.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_shells.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_simple.$(SHADEREXT)

-include $(SHADERDIR)/rage_simple.d

$(SHADERPATH)$(SHADERDIR)/rage_simple.$(SHADEREXT): rage_simple.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_simple.$(SHADEREXT) rage_simple.fx $(SHADERDIR)/rage_simple.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_simple.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_simple.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_simple.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_simple.$(SHADEREXT): rage_simple.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_simple.$(SHADEREXT) rage_simple.fx $(SHADERDIR_30ATI9)/rage_simple.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_simple.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_simple.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_simple.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_simple.$(SHADEREXT): rage_simple.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_simple.$(SHADEREXT) rage_simple.fx $(SHADERDIR_30ATI10)/rage_simple.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_simple.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_simple.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_simple.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_simple.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_simple.$(SHADEREXT): rage_simple.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_simple.$(SHADEREXT) rage_simple.fx $(SHADERDIR_30NV9)/rage_simple.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_simple.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_simple.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_simple.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_simple.$(SHADEREXT): rage_simple.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_simple.$(SHADEREXT) rage_simple.fx $(SHADERDIR_30NV10)/rage_simple.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_simple.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_simple.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_simple.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_simple.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_simple.$(SHADEREXT): rage_simple.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_simple.$(SHADEREXT) rage_simple.fx $(SHADERDIR_40ATI10)/rage_simple.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_simple.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_simple.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_simple.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_simple.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_simple.$(SHADEREXT): rage_simple.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_simple.$(SHADEREXT) rage_simple.fx $(SHADERDIR_40NV10)/rage_simple.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_simple.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_simple.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_skin.$(SHADEREXT)

-include $(SHADERDIR)/rage_skin.d

$(SHADERPATH)$(SHADERDIR)/rage_skin.$(SHADEREXT): rage_skin.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_skin.$(SHADEREXT) rage_skin.fx $(SHADERDIR)/rage_skin.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_skin.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_skin.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_skin.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_skin.$(SHADEREXT): rage_skin.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_skin.$(SHADEREXT) rage_skin.fx $(SHADERDIR_30ATI9)/rage_skin.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_skin.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_skin.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin.$(SHADEREXT): rage_skin.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin.$(SHADEREXT) rage_skin.fx $(SHADERDIR_30ATI10)/rage_skin.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_skin.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_skin.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_skin.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_skin.$(SHADEREXT): rage_skin.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_skin.$(SHADEREXT) rage_skin.fx $(SHADERDIR_30NV9)/rage_skin.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_skin.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_skin.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin.$(SHADEREXT): rage_skin.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin.$(SHADEREXT) rage_skin.fx $(SHADERDIR_30NV10)/rage_skin.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_skin.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_skin.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin.$(SHADEREXT): rage_skin.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin.$(SHADEREXT) rage_skin.fx $(SHADERDIR_40ATI10)/rage_skin.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_skin.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_skin.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin.$(SHADEREXT): rage_skin.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin.$(SHADEREXT) rage_skin.fx $(SHADERDIR_40NV10)/rage_skin.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_skin.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_skin_common.$(SHADEREXT)

-include $(SHADERDIR)/rage_skin_common.d

$(SHADERPATH)$(SHADERDIR)/rage_skin_common.$(SHADEREXT): rage_skin_common.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_skin_common.$(SHADEREXT) rage_skin_common.fx $(SHADERDIR)/rage_skin_common.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_skin_common.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_skin_common.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_skin_common.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_skin_common.$(SHADEREXT): rage_skin_common.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_skin_common.$(SHADEREXT) rage_skin_common.fx $(SHADERDIR_30ATI9)/rage_skin_common.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_skin_common.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin_common.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_skin_common.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin_common.$(SHADEREXT): rage_skin_common.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin_common.$(SHADEREXT) rage_skin_common.fx $(SHADERDIR_30ATI10)/rage_skin_common.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_skin_common.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin_common.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_skin_common.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_skin_common.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_skin_common.$(SHADEREXT): rage_skin_common.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_skin_common.$(SHADEREXT) rage_skin_common.fx $(SHADERDIR_30NV9)/rage_skin_common.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_skin_common.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin_common.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_skin_common.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin_common.$(SHADEREXT): rage_skin_common.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin_common.$(SHADEREXT) rage_skin_common.fx $(SHADERDIR_30NV10)/rage_skin_common.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_skin_common.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin_common.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin_common.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_skin_common.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin_common.$(SHADEREXT): rage_skin_common.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin_common.$(SHADEREXT) rage_skin_common.fx $(SHADERDIR_40ATI10)/rage_skin_common.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_skin_common.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin_common.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin_common.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_skin_common.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin_common.$(SHADEREXT): rage_skin_common.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin_common.$(SHADEREXT) rage_skin_common.fx $(SHADERDIR_40NV10)/rage_skin_common.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_skin_common.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin_common.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_skin_no_z_write.$(SHADEREXT)

-include $(SHADERDIR)/rage_skin_no_z_write.d

$(SHADERPATH)$(SHADERDIR)/rage_skin_no_z_write.$(SHADEREXT): rage_skin_no_z_write.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_skin_no_z_write.$(SHADEREXT) rage_skin_no_z_write.fx $(SHADERDIR)/rage_skin_no_z_write.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_skin_no_z_write.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_skin_no_z_write.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_skin_no_z_write.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_skin_no_z_write.$(SHADEREXT): rage_skin_no_z_write.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_skin_no_z_write.$(SHADEREXT) rage_skin_no_z_write.fx $(SHADERDIR_30ATI9)/rage_skin_no_z_write.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_skin_no_z_write.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin_no_z_write.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_skin_no_z_write.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin_no_z_write.$(SHADEREXT): rage_skin_no_z_write.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin_no_z_write.$(SHADEREXT) rage_skin_no_z_write.fx $(SHADERDIR_30ATI10)/rage_skin_no_z_write.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_skin_no_z_write.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_skin_no_z_write.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_skin_no_z_write.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_skin_no_z_write.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_skin_no_z_write.$(SHADEREXT): rage_skin_no_z_write.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_skin_no_z_write.$(SHADEREXT) rage_skin_no_z_write.fx $(SHADERDIR_30NV9)/rage_skin_no_z_write.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_skin_no_z_write.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin_no_z_write.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_skin_no_z_write.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin_no_z_write.$(SHADEREXT): rage_skin_no_z_write.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin_no_z_write.$(SHADEREXT) rage_skin_no_z_write.fx $(SHADERDIR_30NV10)/rage_skin_no_z_write.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_skin_no_z_write.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_skin_no_z_write.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin_no_z_write.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_skin_no_z_write.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin_no_z_write.$(SHADEREXT): rage_skin_no_z_write.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin_no_z_write.$(SHADEREXT) rage_skin_no_z_write.fx $(SHADERDIR_40ATI10)/rage_skin_no_z_write.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_skin_no_z_write.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_skin_no_z_write.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin_no_z_write.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_skin_no_z_write.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin_no_z_write.$(SHADEREXT): rage_skin_no_z_write.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin_no_z_write.$(SHADEREXT) rage_skin_no_z_write.fx $(SHADERDIR_40NV10)/rage_skin_no_z_write.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_skin_no_z_write.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_skin_no_z_write.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_spec.$(SHADEREXT)

-include $(SHADERDIR)/rage_spec.d

$(SHADERPATH)$(SHADERDIR)/rage_spec.$(SHADEREXT): rage_spec.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_spec.$(SHADEREXT) rage_spec.fx $(SHADERDIR)/rage_spec.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_spec.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spec.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_spec.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spec.$(SHADEREXT): rage_spec.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spec.$(SHADEREXT) rage_spec.fx $(SHADERDIR_30ATI9)/rage_spec.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_spec.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spec.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_spec.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spec.$(SHADEREXT): rage_spec.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spec.$(SHADEREXT) rage_spec.fx $(SHADERDIR_30ATI10)/rage_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spec.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_spec.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_spec.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_spec.$(SHADEREXT): rage_spec.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_spec.$(SHADEREXT) rage_spec.fx $(SHADERDIR_30NV9)/rage_spec.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_spec.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spec.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_spec.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_spec.$(SHADEREXT): rage_spec.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spec.$(SHADEREXT) rage_spec.fx $(SHADERDIR_30NV10)/rage_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spec.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spec.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_spec.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spec.$(SHADEREXT): rage_spec.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spec.$(SHADEREXT) rage_spec.fx $(SHADERDIR_40ATI10)/rage_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spec.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spec.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_spec.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_spec.$(SHADEREXT): rage_spec.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spec.$(SHADEREXT) rage_spec.fx $(SHADERDIR_40NV10)/rage_spec.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_spec.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spec.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR)/rage_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR)/rage_spec_reflectcube.$(SHADEREXT): rage_spec_reflectcube.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_spec_reflectcube.$(SHADEREXT) rage_spec_reflectcube.fx $(SHADERDIR)/rage_spec_reflectcube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_spec_reflectcube.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spec_reflectcube.$(SHADEREXT): rage_spec_reflectcube.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spec_reflectcube.$(SHADEREXT) rage_spec_reflectcube.fx $(SHADERDIR_30ATI9)/rage_spec_reflectcube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_spec_reflectcube.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spec_reflectcube.$(SHADEREXT): rage_spec_reflectcube.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spec_reflectcube.$(SHADEREXT) rage_spec_reflectcube.fx $(SHADERDIR_30ATI10)/rage_spec_reflectcube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_spec_reflectcube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spec_reflectcube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_spec_reflectcube.$(SHADEREXT): rage_spec_reflectcube.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_spec_reflectcube.$(SHADEREXT) rage_spec_reflectcube.fx $(SHADERDIR_30NV9)/rage_spec_reflectcube.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_spec_reflectcube.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_spec_reflectcube.$(SHADEREXT): rage_spec_reflectcube.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spec_reflectcube.$(SHADEREXT) rage_spec_reflectcube.fx $(SHADERDIR_30NV10)/rage_spec_reflectcube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_spec_reflectcube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spec_reflectcube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spec_reflectcube.$(SHADEREXT): rage_spec_reflectcube.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spec_reflectcube.$(SHADEREXT) rage_spec_reflectcube.fx $(SHADERDIR_40ATI10)/rage_spec_reflectcube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_spec_reflectcube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spec_reflectcube.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spec_reflectcube.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_spec_reflectcube.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_spec_reflectcube.$(SHADEREXT): rage_spec_reflectcube.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spec_reflectcube.$(SHADEREXT) rage_spec_reflectcube.fx $(SHADERDIR_40NV10)/rage_spec_reflectcube.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_spec_reflectcube.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spec_reflectcube.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_teststage7.$(SHADEREXT)

-include $(SHADERDIR)/rage_teststage7.d

$(SHADERPATH)$(SHADERDIR)/rage_teststage7.$(SHADEREXT): rage_teststage7.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_teststage7.$(SHADEREXT) rage_teststage7.fx $(SHADERDIR)/rage_teststage7.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_teststage7.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_teststage7.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_teststage7.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_teststage7.$(SHADEREXT): rage_teststage7.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_teststage7.$(SHADEREXT) rage_teststage7.fx $(SHADERDIR_30ATI9)/rage_teststage7.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_teststage7.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_teststage7.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_teststage7.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_teststage7.$(SHADEREXT): rage_teststage7.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_teststage7.$(SHADEREXT) rage_teststage7.fx $(SHADERDIR_30ATI10)/rage_teststage7.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_teststage7.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_teststage7.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_teststage7.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_teststage7.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_teststage7.$(SHADEREXT): rage_teststage7.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_teststage7.$(SHADEREXT) rage_teststage7.fx $(SHADERDIR_30NV9)/rage_teststage7.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_teststage7.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_teststage7.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_teststage7.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_teststage7.$(SHADEREXT): rage_teststage7.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_teststage7.$(SHADEREXT) rage_teststage7.fx $(SHADERDIR_30NV10)/rage_teststage7.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_teststage7.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_teststage7.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_teststage7.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_teststage7.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_teststage7.$(SHADEREXT): rage_teststage7.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_teststage7.$(SHADEREXT) rage_teststage7.fx $(SHADERDIR_40ATI10)/rage_teststage7.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_teststage7.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_teststage7.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_teststage7.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_teststage7.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_teststage7.$(SHADEREXT): rage_teststage7.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_teststage7.$(SHADEREXT) rage_teststage7.fx $(SHADERDIR_40NV10)/rage_teststage7.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_teststage7.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_teststage7.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_terrain.$(SHADEREXT)

-include $(SHADERDIR)/rage_terrain.d

$(SHADERPATH)$(SHADERDIR)/rage_terrain.$(SHADEREXT): rage_terrain.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_terrain.$(SHADEREXT) rage_terrain.fx $(SHADERDIR)/rage_terrain.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_terrain.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_terrain.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_terrain.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_terrain.$(SHADEREXT): rage_terrain.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_terrain.$(SHADEREXT) rage_terrain.fx $(SHADERDIR_30ATI9)/rage_terrain.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_terrain.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_terrain.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_terrain.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_terrain.$(SHADEREXT): rage_terrain.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_terrain.$(SHADEREXT) rage_terrain.fx $(SHADERDIR_30ATI10)/rage_terrain.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_terrain.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_terrain.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_terrain.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_terrain.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_terrain.$(SHADEREXT): rage_terrain.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_terrain.$(SHADEREXT) rage_terrain.fx $(SHADERDIR_30NV9)/rage_terrain.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_terrain.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_terrain.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_terrain.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_terrain.$(SHADEREXT): rage_terrain.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_terrain.$(SHADEREXT) rage_terrain.fx $(SHADERDIR_30NV10)/rage_terrain.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_terrain.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_terrain.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_terrain.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_terrain.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_terrain.$(SHADEREXT): rage_terrain.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_terrain.$(SHADEREXT) rage_terrain.fx $(SHADERDIR_40ATI10)/rage_terrain.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_terrain.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_terrain.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_terrain.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_terrain.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_terrain.$(SHADEREXT): rage_terrain.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_terrain.$(SHADEREXT) rage_terrain.fx $(SHADERDIR_40NV10)/rage_terrain.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_terrain.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_terrain.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_verletperturbation.$(SHADEREXT)

-include $(SHADERDIR)/rage_verletperturbation.d

$(SHADERPATH)$(SHADERDIR)/rage_verletperturbation.$(SHADEREXT): rage_verletperturbation.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_verletperturbation.$(SHADEREXT) rage_verletperturbation.fx $(SHADERDIR)/rage_verletperturbation.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_verletperturbation.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_verletperturbation.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_verletperturbation.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_verletperturbation.$(SHADEREXT): rage_verletperturbation.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_verletperturbation.$(SHADEREXT) rage_verletperturbation.fx $(SHADERDIR_30ATI9)/rage_verletperturbation.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_verletperturbation.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletperturbation.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_verletperturbation.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletperturbation.$(SHADEREXT): rage_verletperturbation.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletperturbation.$(SHADEREXT) rage_verletperturbation.fx $(SHADERDIR_30ATI10)/rage_verletperturbation.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_verletperturbation.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletperturbation.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_verletperturbation.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_verletperturbation.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_verletperturbation.$(SHADEREXT): rage_verletperturbation.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_verletperturbation.$(SHADEREXT) rage_verletperturbation.fx $(SHADERDIR_30NV9)/rage_verletperturbation.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_verletperturbation.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletperturbation.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_verletperturbation.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletperturbation.$(SHADEREXT): rage_verletperturbation.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletperturbation.$(SHADEREXT) rage_verletperturbation.fx $(SHADERDIR_30NV10)/rage_verletperturbation.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_verletperturbation.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletperturbation.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletperturbation.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_verletperturbation.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletperturbation.$(SHADEREXT): rage_verletperturbation.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletperturbation.$(SHADEREXT) rage_verletperturbation.fx $(SHADERDIR_40ATI10)/rage_verletperturbation.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_verletperturbation.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletperturbation.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletperturbation.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_verletperturbation.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletperturbation.$(SHADEREXT): rage_verletperturbation.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletperturbation.$(SHADEREXT) rage_verletperturbation.fx $(SHADERDIR_40NV10)/rage_verletperturbation.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_verletperturbation.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletperturbation.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_verletsimulation.$(SHADEREXT)

-include $(SHADERDIR)/rage_verletsimulation.d

$(SHADERPATH)$(SHADERDIR)/rage_verletsimulation.$(SHADEREXT): rage_verletsimulation.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_verletsimulation.$(SHADEREXT) rage_verletsimulation.fx $(SHADERDIR)/rage_verletsimulation.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_verletsimulation.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_verletsimulation.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_verletsimulation.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_verletsimulation.$(SHADEREXT): rage_verletsimulation.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_verletsimulation.$(SHADEREXT) rage_verletsimulation.fx $(SHADERDIR_30ATI9)/rage_verletsimulation.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_verletsimulation.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletsimulation.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_verletsimulation.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletsimulation.$(SHADEREXT): rage_verletsimulation.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletsimulation.$(SHADEREXT) rage_verletsimulation.fx $(SHADERDIR_30ATI10)/rage_verletsimulation.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_verletsimulation.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletsimulation.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_verletsimulation.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_verletsimulation.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_verletsimulation.$(SHADEREXT): rage_verletsimulation.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_verletsimulation.$(SHADEREXT) rage_verletsimulation.fx $(SHADERDIR_30NV9)/rage_verletsimulation.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_verletsimulation.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletsimulation.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_verletsimulation.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletsimulation.$(SHADEREXT): rage_verletsimulation.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletsimulation.$(SHADEREXT) rage_verletsimulation.fx $(SHADERDIR_30NV10)/rage_verletsimulation.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_verletsimulation.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletsimulation.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletsimulation.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_verletsimulation.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletsimulation.$(SHADEREXT): rage_verletsimulation.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletsimulation.$(SHADEREXT) rage_verletsimulation.fx $(SHADERDIR_40ATI10)/rage_verletsimulation.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_verletsimulation.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletsimulation.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletsimulation.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_verletsimulation.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletsimulation.$(SHADEREXT): rage_verletsimulation.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletsimulation.$(SHADEREXT) rage_verletsimulation.fx $(SHADERDIR_40NV10)/rage_verletsimulation.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_verletsimulation.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletsimulation.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_verletwater.$(SHADEREXT)

-include $(SHADERDIR)/rage_verletwater.d

$(SHADERPATH)$(SHADERDIR)/rage_verletwater.$(SHADEREXT): rage_verletwater.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_verletwater.$(SHADEREXT) rage_verletwater.fx $(SHADERDIR)/rage_verletwater.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_verletwater.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_verletwater.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_verletwater.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_verletwater.$(SHADEREXT): rage_verletwater.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_verletwater.$(SHADEREXT) rage_verletwater.fx $(SHADERDIR_30ATI9)/rage_verletwater.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_verletwater.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletwater.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_verletwater.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletwater.$(SHADEREXT): rage_verletwater.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletwater.$(SHADEREXT) rage_verletwater.fx $(SHADERDIR_30ATI10)/rage_verletwater.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_verletwater.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_verletwater.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_verletwater.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_verletwater.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_verletwater.$(SHADEREXT): rage_verletwater.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_verletwater.$(SHADEREXT) rage_verletwater.fx $(SHADERDIR_30NV9)/rage_verletwater.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_verletwater.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletwater.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_verletwater.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletwater.$(SHADEREXT): rage_verletwater.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletwater.$(SHADEREXT) rage_verletwater.fx $(SHADERDIR_30NV10)/rage_verletwater.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_verletwater.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_verletwater.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletwater.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_verletwater.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletwater.$(SHADEREXT): rage_verletwater.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletwater.$(SHADEREXT) rage_verletwater.fx $(SHADERDIR_40ATI10)/rage_verletwater.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_verletwater.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_verletwater.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletwater.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_verletwater.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletwater.$(SHADEREXT): rage_verletwater.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletwater.$(SHADEREXT) rage_verletwater.fx $(SHADERDIR_40NV10)/rage_verletwater.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_verletwater.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_verletwater.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_rmptfx_diffuse.$(SHADEREXT)

-include $(SHADERDIR)/rage_rmptfx_diffuse.d

$(SHADERPATH)$(SHADERDIR)/rage_rmptfx_diffuse.$(SHADEREXT): rage_rmptfx_diffuse.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_rmptfx_diffuse.$(SHADEREXT) rage_rmptfx_diffuse.fx $(SHADERDIR)/rage_rmptfx_diffuse.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_rmptfx_diffuse.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_rmptfx_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_rmptfx_diffuse.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_rmptfx_diffuse.$(SHADEREXT): rage_rmptfx_diffuse.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_rmptfx_diffuse.$(SHADEREXT) rage_rmptfx_diffuse.fx $(SHADERDIR_30ATI9)/rage_rmptfx_diffuse.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_rmptfx_diffuse.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_rmptfx_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_rmptfx_diffuse.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_rmptfx_diffuse.$(SHADEREXT): rage_rmptfx_diffuse.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_rmptfx_diffuse.$(SHADEREXT) rage_rmptfx_diffuse.fx $(SHADERDIR_30ATI10)/rage_rmptfx_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_rmptfx_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_rmptfx_diffuse.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_rmptfx_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_rmptfx_diffuse.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_rmptfx_diffuse.$(SHADEREXT): rage_rmptfx_diffuse.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_rmptfx_diffuse.$(SHADEREXT) rage_rmptfx_diffuse.fx $(SHADERDIR_30NV9)/rage_rmptfx_diffuse.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_rmptfx_diffuse.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_rmptfx_diffuse.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_rmptfx_diffuse.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_rmptfx_diffuse.$(SHADEREXT): rage_rmptfx_diffuse.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_rmptfx_diffuse.$(SHADEREXT) rage_rmptfx_diffuse.fx $(SHADERDIR_30NV10)/rage_rmptfx_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_rmptfx_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_rmptfx_diffuse.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_rmptfx_diffuse.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_rmptfx_diffuse.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_rmptfx_diffuse.$(SHADEREXT): rage_rmptfx_diffuse.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_rmptfx_diffuse.$(SHADEREXT) rage_rmptfx_diffuse.fx $(SHADERDIR_40ATI10)/rage_rmptfx_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_rmptfx_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_rmptfx_diffuse.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_rmptfx_diffuse.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_rmptfx_diffuse.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_rmptfx_diffuse.$(SHADEREXT): rage_rmptfx_diffuse.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_rmptfx_diffuse.$(SHADEREXT) rage_rmptfx_diffuse.fx $(SHADERDIR_40NV10)/rage_rmptfx_diffuse.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_rmptfx_diffuse.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_rmptfx_diffuse.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rmptfx_blood.$(SHADEREXT)

-include $(SHADERDIR)/rmptfx_blood.d

$(SHADERPATH)$(SHADERDIR)/rmptfx_blood.$(SHADEREXT): rmptfx_blood.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rmptfx_blood.$(SHADEREXT) rmptfx_blood.fx $(SHADERDIR)/rmptfx_blood.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rmptfx_blood.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rmptfx_blood.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rmptfx_blood.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rmptfx_blood.$(SHADEREXT): rmptfx_blood.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rmptfx_blood.$(SHADEREXT) rmptfx_blood.fx $(SHADERDIR_30ATI9)/rmptfx_blood.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rmptfx_blood.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rmptfx_blood.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rmptfx_blood.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rmptfx_blood.$(SHADEREXT): rmptfx_blood.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rmptfx_blood.$(SHADEREXT) rmptfx_blood.fx $(SHADERDIR_30ATI10)/rmptfx_blood.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rmptfx_blood.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rmptfx_blood.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rmptfx_blood.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rmptfx_blood.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rmptfx_blood.$(SHADEREXT): rmptfx_blood.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rmptfx_blood.$(SHADEREXT) rmptfx_blood.fx $(SHADERDIR_30NV9)/rmptfx_blood.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rmptfx_blood.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rmptfx_blood.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rmptfx_blood.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rmptfx_blood.$(SHADEREXT): rmptfx_blood.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rmptfx_blood.$(SHADEREXT) rmptfx_blood.fx $(SHADERDIR_30NV10)/rmptfx_blood.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rmptfx_blood.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rmptfx_blood.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rmptfx_blood.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rmptfx_blood.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rmptfx_blood.$(SHADEREXT): rmptfx_blood.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rmptfx_blood.$(SHADEREXT) rmptfx_blood.fx $(SHADERDIR_40ATI10)/rmptfx_blood.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rmptfx_blood.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rmptfx_blood.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rmptfx_blood.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rmptfx_blood.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rmptfx_blood.$(SHADEREXT): rmptfx_blood.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rmptfx_blood.$(SHADEREXT) rmptfx_blood.fx $(SHADERDIR_40NV10)/rmptfx_blood.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rmptfx_blood.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rmptfx_blood.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rmptfx_pvolume.$(SHADEREXT)

-include $(SHADERDIR)/rmptfx_pvolume.d

$(SHADERPATH)$(SHADERDIR)/rmptfx_pvolume.$(SHADEREXT): rmptfx_pvolume.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rmptfx_pvolume.$(SHADEREXT) rmptfx_pvolume.fx $(SHADERDIR)/rmptfx_pvolume.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rmptfx_pvolume.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rmptfx_pvolume.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rmptfx_pvolume.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rmptfx_pvolume.$(SHADEREXT): rmptfx_pvolume.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rmptfx_pvolume.$(SHADEREXT) rmptfx_pvolume.fx $(SHADERDIR_30ATI9)/rmptfx_pvolume.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rmptfx_pvolume.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rmptfx_pvolume.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rmptfx_pvolume.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rmptfx_pvolume.$(SHADEREXT): rmptfx_pvolume.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rmptfx_pvolume.$(SHADEREXT) rmptfx_pvolume.fx $(SHADERDIR_30ATI10)/rmptfx_pvolume.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rmptfx_pvolume.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rmptfx_pvolume.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rmptfx_pvolume.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rmptfx_pvolume.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rmptfx_pvolume.$(SHADEREXT): rmptfx_pvolume.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rmptfx_pvolume.$(SHADEREXT) rmptfx_pvolume.fx $(SHADERDIR_30NV9)/rmptfx_pvolume.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rmptfx_pvolume.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rmptfx_pvolume.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rmptfx_pvolume.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rmptfx_pvolume.$(SHADEREXT): rmptfx_pvolume.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rmptfx_pvolume.$(SHADEREXT) rmptfx_pvolume.fx $(SHADERDIR_30NV10)/rmptfx_pvolume.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rmptfx_pvolume.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rmptfx_pvolume.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rmptfx_pvolume.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rmptfx_pvolume.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rmptfx_pvolume.$(SHADEREXT): rmptfx_pvolume.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rmptfx_pvolume.$(SHADEREXT) rmptfx_pvolume.fx $(SHADERDIR_40ATI10)/rmptfx_pvolume.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rmptfx_pvolume.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rmptfx_pvolume.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rmptfx_pvolume.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rmptfx_pvolume.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rmptfx_pvolume.$(SHADEREXT): rmptfx_pvolume.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rmptfx_pvolume.$(SHADEREXT) rmptfx_pvolume.fx $(SHADERDIR_40NV10)/rmptfx_pvolume.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rmptfx_pvolume.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rmptfx_pvolume.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_damage.$(SHADEREXT)

-include $(SHADERDIR)/rage_damage.d

$(SHADERPATH)$(SHADERDIR)/rage_damage.$(SHADEREXT): rage_damage.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_damage.$(SHADEREXT) rage_damage.fx $(SHADERDIR)/rage_damage.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_damage.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_damage.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_damage.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_damage.$(SHADEREXT): rage_damage.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_damage.$(SHADEREXT) rage_damage.fx $(SHADERDIR_30ATI9)/rage_damage.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_damage.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_damage.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_damage.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_damage.$(SHADEREXT): rage_damage.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_damage.$(SHADEREXT) rage_damage.fx $(SHADERDIR_30ATI10)/rage_damage.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_damage.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_damage.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_damage.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_damage.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_damage.$(SHADEREXT): rage_damage.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_damage.$(SHADEREXT) rage_damage.fx $(SHADERDIR_30NV9)/rage_damage.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_damage.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_damage.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_damage.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_damage.$(SHADEREXT): rage_damage.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_damage.$(SHADEREXT) rage_damage.fx $(SHADERDIR_30NV10)/rage_damage.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_damage.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_damage.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_damage.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_damage.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_damage.$(SHADEREXT): rage_damage.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_damage.$(SHADEREXT) rage_damage.fx $(SHADERDIR_40ATI10)/rage_damage.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_damage.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_damage.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_damage.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_damage.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_damage.$(SHADEREXT): rage_damage.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_damage.$(SHADEREXT) rage_damage.fx $(SHADERDIR_40NV10)/rage_damage.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_damage.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_damage.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_diffuse_instance.$(SHADEREXT)

-include $(SHADERDIR)/rage_diffuse_instance.d

$(SHADERPATH)$(SHADERDIR)/rage_diffuse_instance.$(SHADEREXT): rage_diffuse_instance.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_diffuse_instance.$(SHADEREXT) rage_diffuse_instance.fx $(SHADERDIR)/rage_diffuse_instance.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_diffuse_instance.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_instance.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_diffuse_instance.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_instance.$(SHADEREXT): rage_diffuse_instance.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_diffuse_instance.$(SHADEREXT) rage_diffuse_instance.fx $(SHADERDIR_30ATI9)/rage_diffuse_instance.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_diffuse_instance.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_instance.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_diffuse_instance.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_instance.$(SHADEREXT): rage_diffuse_instance.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_instance.$(SHADEREXT) rage_diffuse_instance.fx $(SHADERDIR_30ATI10)/rage_diffuse_instance.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_diffuse_instance.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_diffuse_instance.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_instance.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_diffuse_instance.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_instance.$(SHADEREXT): rage_diffuse_instance.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_diffuse_instance.$(SHADEREXT) rage_diffuse_instance.fx $(SHADERDIR_30NV9)/rage_diffuse_instance.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_diffuse_instance.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_instance.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_diffuse_instance.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_instance.$(SHADEREXT): rage_diffuse_instance.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_instance.$(SHADEREXT) rage_diffuse_instance.fx $(SHADERDIR_30NV10)/rage_diffuse_instance.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_diffuse_instance.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_diffuse_instance.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_instance.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_diffuse_instance.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_instance.$(SHADEREXT): rage_diffuse_instance.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_instance.$(SHADEREXT) rage_diffuse_instance.fx $(SHADERDIR_40ATI10)/rage_diffuse_instance.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_diffuse_instance.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_diffuse_instance.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_instance.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_diffuse_instance.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_instance.$(SHADEREXT): rage_diffuse_instance.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_instance.$(SHADEREXT) rage_diffuse_instance.fx $(SHADERDIR_40NV10)/rage_diffuse_instance.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_diffuse_instance.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_diffuse_instance.$(SHADEREXT)

endif



TARGETS += $(SHADERPATH)preload.list

$(SHADERPATH)preload.list: preload.list
	if not exist $(SHADERPATHDOS)* mkdir $(SHADERPATHDOS)
	if exist $(SHADERPATHDOS)preload.list del /f $(SHADERPATHDOS)preload.list
	type preload.list > $(SHADERPATHDOS)preload.list

alltargets: $(TARGETS)
	echo DONE > $(SHADERDIR).dummy
