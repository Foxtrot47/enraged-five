#include "rage_skin_common.fx"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(4);

technique drawskinned
{
    pass p0 
    {
        AlphaBlendEnable = false;

		SrcBlend = ONE;
		DestBlend = ZERO;
		
        VertexShader = compile VERTEXSHADER VS_CompositeSkin();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}

technique silhouette_drawskinned
{
    pass p0 
    {
        AlphaBlendEnable = false;

		SrcBlend = ONE;
		DestBlend = ZERO;
		
        VertexShader = compile VERTEXSHADER VS_CompositeSkin();
        PixelShader  = compile PIXELSHADER PS_CompositeLightingSilhouette();
    }
}
