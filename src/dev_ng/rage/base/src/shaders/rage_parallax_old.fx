#ifndef __RAGE_TOOLSET
#define __RAGE_TOOLSET

#define RAGE_ENABLE_OFFSET
#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"

//---------------------- WIDGETS

float bumpiness : Bumpiness
<
	string UIWidget = "slider";
	float UIMin = 0.0;
	float UIMax = 200.0;
	float UIStep = .01;
	string UIName = "Bumpiness";
> = 1.0;

float specularFactor : Specular
<
	string UIName = "Specular Falloff";
	float UIMin = 0.0;
	float UIMax = 10000.0;
	float UIStep = 0.1;
> = 100.0;

float specularColorFactor : SpecularColor
<
	string UIName = "Specular Intensity";
	float UIMin = 0.0;
	float UIMax = 10000.0;
	float UIStep = 0.1;
> = 1.0;


struct vertexOutput {
    DECLARE_POSITION( pos)
    float2 texCoord					: TEXCOORD0;
    float3 lightVec					: TEXCOORD4;
    float3 viewVec					: TEXCOORD5;
	float4 color0					: COLOR0;
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//=====================================================================================================================
/* ====== VERTEX SHADERS =========== */
//=====================================================================================================================
#if USE_OFFSETS
vertexOutput VS_Transform(rageVertexInputBump IN, rageVertexOffsetBump INOFFSET)
#else
vertexOutput VS_Transform(rageVertexInputBump IN)
#endif // USE_OFFSETS
{
    vertexOutput OUT;
    
    float3 inPos = IN.pos;
    float3 inNrm = IN.normal;
    float3 Tangent  = IN.tangent.xyz;
    float4 inCpv = IN.diffuse;

#if USE_OFFSETS
    if( gbOffsetEnable )
	{
		inPos += INOFFSET.pos;
		inNrm += INOFFSET.normal;
		inCpv += INOFFSET.diffuse;
        Tangent  += INOFFSET.tangent;
	}
#endif // USE_OFFSETS

	float3 pos = mul(float4(inPos, 1), (float4x3)gWorld);

	// Store position in eyespace
	float3 worldEyePos = gViewInverse[3].xyz - pos;

	// Transform normal by "transpose" matrix
	float3 worldNormal = normalize(mul(float4(inNrm, 1), (float4x3)gWorld));;

    OUT.color0 = inCpv;


    // Write out final position & texture coords
    OUT.pos =  mul(float4(inPos,1), gWorldViewProj);
    
    OUT.texCoord = IN.texCoord0;

    // tangent space matrix
//    float3 worldTangent = normalize(mul(float4(Tangent, 1), (float4x3)gWorld));
//    float3 worldBinormal = normalize(cross(OUT.worldTangent, OUT.worldNormal));

	float3 binormal = normalize(cross(Tangent, inNrm));

	OUT.lightVec.x = dot(gLightPosDir[0].xyz, Tangent);
	OUT.lightVec.y = dot(gLightPosDir[0].xyz, binormal);
	OUT.lightVec.z = dot(gLightPosDir[0].xyz, inNrm);

	OUT.viewVec.x = dot(worldEyePos, Tangent);
	OUT.viewVec.y = dot(worldEyePos, binormal);
	OUT.viewVec.z = dot(worldEyePos, inNrm);

	return OUT;
}

#if USE_OFFSETS
vertexOutput VS_TransformSkin(rageSkinVertexInputBump IN, rageSkinVertexOffsetBump INOFFSET) 
#else
vertexOutput VS_TransformSkin(rageSkinVertexInputBump IN) 
#endif // USE_OFFSETS
{
    vertexOutput OUT;
    
    float3 inPos = IN.pos;
    float3 inNrm = IN.normal;
    float3 Tangent  = IN.tangent.xyz;

#if USE_OFFSETS
    if( gbOffsetEnable )
	{
		inPos += INOFFSET.pos;
		inNrm += INOFFSET.normal;
        Tangent  += INOFFSET.tangent;
	}
#endif // USE_OFFSETS
	
	int4 IndexVector = D3DCOLORtoUBYTE4(IN.blendindices);
#if SKINNING
    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);
#else
	rageSkinMtx boneMtx = (rageSkinMtx) gWorld;
#endif

	float3 bpos = rageSkinTransform(inPos, boneMtx);

	// Store position in eyespace
	float3 worldEyePos = gViewInverse[3].xyz - bpos;

	// Transform normal by "transpose" matrix
	float3 worldNormal = normalize(rageSkinRotate(inNrm, boneMtx));

    OUT.color0 = float4(1,1,1,1);


    // Write out final position & texture coords
	// Transform position by bone matrix
#if SKINNING
	float3 pos = mul(float4(inPos,1), boneMtx);
#else
	float3 pos = inPos;
#endif

    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
    // NOTE: These 3 statements may resolve to the exact same code -- rely on HLSL to strip worthless code
    OUT.texCoord = IN.texCoord0;

    // tangent space matrix
//    OUT.worldTangent = normalize(mul(float4(Tangent, 1), boneMtx));
//    OUT.worldBinormal = normalize(cross(OUT.worldTangent, OUT.worldNormal));

	float3 binormal = normalize(cross(Tangent, inNrm));

	OUT.lightVec.x = dot(gLightPosDir[0].xyz, Tangent);
	OUT.lightVec.y = dot(gLightPosDir[0].xyz, binormal);
	OUT.lightVec.z = dot(gLightPosDir[0].xyz, inNrm);

	OUT.viewVec.x = dot(worldEyePos, Tangent);
	OUT.viewVec.y = dot(worldEyePos, binormal);
	OUT.viewVec.z = dot(worldEyePos, inNrm);

	return OUT;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeginSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
	string UIName="Diffuse Texture";
ContinueSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

BeginSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
	string UIName="Bump Texture";
	string UIHint="normalmap";
ContinueSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;


//=====================================================================================================================
/* ====== PIXEL SHADERS =========== */
//=====================================================================================================================

float4 PS_Textured( vertexOutput IN): COLOR
{
	float3 lightVec = normalize(IN.lightVec);
	float3 viewVec  = normalize(IN.viewVec);

	float parallax = 0.035;

	float height = tex2D(TextureSampler, IN.texCoord).a;
	float offset = parallax * (2.0 * height - 1.0);
	float2 parallaxTexCoord = IN.texCoord + offset * viewVec.xy;

	float4 base = float4((float3)tex2D(TextureSampler, parallaxTexCoord),1);
	float3 bump = normalize(tex2D(TextureSampler, parallaxTexCoord).xyz * 2.0 - 1.0);

	// Standard Phong lighting
	float diffuse = saturate(dot(lightVec, bump));
	float specular = pow(saturate(dot(reflect(-viewVec, bump), lightVec)), 12.0);

	float4 lighting = (diffuse * base + 0.0 * specular);
	float4 ambient = IN.color0 * base;

	return lighting;// + ambient;
}


// ===================================================================================================
// Default techniques
// ===================================================================================================

technique draw
{
	pass p0 
	{        
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_Textured();
	}
}
technique drawskinned
{
	pass p0 
	{        
		AlphaRef = 100;
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		VertexShader = compile VERTEXSHADER VS_TransformSkin();
		PixelShader  = compile PIXELSHADER PS_Textured();
	}
}


#endif	// __RAGE_TOOLSET
