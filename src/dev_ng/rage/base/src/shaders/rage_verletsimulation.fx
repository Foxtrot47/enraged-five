//**************************************************************//
//  RAGE Verlet Simulation shader
//
//  David Serafim 21 Oct 2005
//**************************************************************//

#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(14);


BeginSampler(sampler2D,dampeningTexture,DampeningSampler,DampeningTex)
    string UIName = "Dampening Texture";
ContinueSampler(sampler2D,dampeningTexture,DampeningSampler,DampeningTex)
#if !__FXL 
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
#endif
EndSampler;

BeginSampler(sampler2D,simulationTexture,SimulationSampler,SimulationTex)
    string UIName = "Simulation Texture";
ContinueSampler(sampler2D,simulationTexture,SimulationSampler,SimulationTex)
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_POINT_MAG_POINT_MIP_POINT;
	LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D,prevSimulationTexture,PrevSimulationSampler,PrevSimulationTex)
    string UIName = "Previous Simulation Texture";
ContinueSampler(sampler2D,prevSimulationTexture,PrevSimulationSampler,PrevSimulationTex)
	#if 1
		AddressU  = CLAMP;        
		AddressV  = CLAMP;
		AddressW  = CLAMP;
		MIN_POINT_MAG_POINT_MIP_POINT;
		LOD_BIAS = 0;
	#endif
EndSampler;


const float SimulationTexWidth : SimulationTexWidth = 512;
const float SimulationTexHeight : SimulationTexHeight = 512;
const float WaveSpeed : WaveSpeed = .1;



struct VS_OUTPUT
{
   DECLARE_POSITION( Pos)
   float2 TexCoord:   TEXCOORD0;
};


#pragma dcl position

VS_OUTPUT vs_main(float4 inPos: POSITION0)
{
	VS_OUTPUT OUT;

	OUT.Pos = float4(inPos.xy*2-1,0,1);
	OUT.TexCoord.x = inPos.x;
	OUT.TexCoord.y = 1-inPos.y;

	return OUT;
}


float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	const float oneovertwo = 128.f / 255.f;

	float2 tex = IN.TexCoord;

	float height = tex2D(SimulationSampler,tex).r;
	float prevHeight = tex2D(PrevSimulationSampler,tex).r;
	float hleft = tex2D(SimulationSampler,tex + float2(-1/SimulationTexWidth,0)).r;
	float hright = tex2D(SimulationSampler,tex + float2(1/SimulationTexWidth,0)).r;
	float htop = tex2D(SimulationSampler,tex + float2(0,-1/SimulationTexHeight)).r;
	float hbott = tex2D(SimulationSampler,tex + float2(0,1/SimulationTexHeight)).r;

	float dampening = saturate(tex2D(DampeningSampler,tex).r + 0);

	float accel = dampening * WaveSpeed*3 * ( (hleft + hright + htop + hbott) - 4 * height );

	float newHeight = height + (height - prevHeight) * 0.96;		// vertical velocity term
	newHeight -= (height - oneovertwo) * 0.001;		// spring effect
	newHeight += accel/8;			// propagation

//	newHeight = height * 1.99 - prevHeight * 0.99 + (accel/2) * 0.01;
//	newHeight *= dampening;

	return saturate(newHeight);
}


//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
      ZWRITEENABLE = false;
      ZENABLE = false;
      CULLMODE = NONE;
      ALPHABLENDENABLE = false;

      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

