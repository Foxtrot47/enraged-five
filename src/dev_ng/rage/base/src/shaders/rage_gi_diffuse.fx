#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_gi.fxh"

BeginSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
	string UIName="Diffuse Texture";
	string MaxRGBMap="diffuseColor";
ContinueSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
EndSampler;

BeginSampler(sampler2D,bumpTexture,bumpSampler,bumpTex)
	string UIName="Bump Texture";
	string MaxRGBMap="bump";
ContinueSampler(sampler2D,bumpTexture,bumpSampler,bumpTex)
EndSampler;


BeginSampler(sampler2D,specTexture,specSampler, specTex)
	string UIName="Spec Texture";
	string MaxRGBMap="specularColor";
ContinueSampler(sampler2D,specTexture,specSampler,specTex)
EndSampler;

BeginSampler(sampler2D,ProbesTexture,ProbesSampler,ProbesTex)
	string UIName="Probes Texture";
	string MaxRGBMap="filterColor";
ContinueSampler(sampler2D,ProbesTexture,ProbesSampler,ProbesTex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
//	MipFilter = POINT;
//	MinFilter = POINT;
//	MagFilter = POINT;
EndSampler;

BeginSampler(sampler2D,SH_R_0_Texture,SH_R_0_Sampler,SH_R_0_Tex)
	string UIName="SH_R_0_ Texture";
	string MaxRGBMap="reflection";
ContinueSampler(sampler2D,SH_R_0_Texture,SH_R_0_Sampler,SH_R_0_Tex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
//	MipFilter = POINT;
//	MinFilter = POINT;
//	MagFilter = POINT;
EndSampler;

BeginSampler(sampler2D,SH_G_0_Texture,SH_G_0_Sampler,SH_G_0_Tex)
	string UIName="SH_G_0_ Texture";
	string MaxRGBMap="refraction";
ContinueSampler(sampler2D,SH_G_0_Texture,SH_G_0_Sampler,SH_G_0_Tex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
//	MipFilter = POINT;
//	MinFilter = POINT;
//	MagFilter = POINT;
EndSampler;

BeginSampler(sampler2D,SH_B_0_Texture,SH_B_0_Sampler,SH_B_0_Tex)
	string UIName="SH_B_0_ Texture";
	string MaxRGBMap="displacement";
ContinueSampler(sampler2D,SH_B_0_Texture,SH_B_0_Sampler,SH_B_0_Tex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

BeginSampler(samplerCUBE,SH_Cube_Texture,SH_Cube_Sampler,SH_Cube_Tex)
	string UIName="SH_Cube Texture";
	string MaxRGBMap="selfIllumination";
ContinueSampler(samplerCUBE,SH_Cube_Texture,SH_Cube_Sampler,SH_Cube_Tex)
EndSampler;

BeginConstantBufferDX10(rage_debug)
SHARED bool debugShader : debugShader REGISTER(b3);
EndConstantBufferDX10(gDebug)

	float bumpiness : Bumpiness
	<
		string UIWidget = "slider";
		float UIMin = 0.0;
		float UIMax = 200.0;
		float UIStep = .01;
		string UIName = "Bumpiness";
	> = 2.0;

	float specularFactor : Specular
	<
		string UIName = "Specular Falloff";
		float UIMin = 0.0;
		float UIMax = 10000.0;
		float UIStep = 0.1;
	> = 100.0;

	float specularColorFactor : SpecularColor
	<
		string UIName = "Specular Intensity";
		float UIMin = 0.0;
		float UIMax = 10000.0;
		float UIStep = 0.1;
	> = 1.0;

struct inputVertex 
{
	DECLARE_POSITION( pos )
	float2 tc	: TEXCOORD0;
    float3 normal		: NORMAL;
	float4 tangent		: TANGENT0;
	float4 diffuse		: COLOR0;
};

struct inputVertexSkinned
{
	float3 pos	: POSITION;
	float4 weight		  : BLENDWEIGHT;
	index4 blendindices	  : BLENDINDICES;
	float2 tc	: TEXCOORD0;
    float3 normal		: NORMAL;
	float4 tangent		: TANGENT0;
	float4 diffuse		: COLOR0;
};

struct outputVertex 
{
	DECLARE_POSITION( pos)
	float3 position : TEXCOORD0;
	float2 tc	    : TEXCOORD1;
	float3 view     : TEXCOORD2;
	float3 norm     : TEXCOORD3;
	float3 tangent  : TEXCOORD4;
	float3 binormal : TEXCOORD5;
	float4 diffuse  : COLOR0;
};

outputVertex VS_Transform(inputVertex IN) 
{
	outputVertex OUT;
	OUT.pos = mul(IN.pos, gWorldViewProj);
	OUT.tc = IN.tc;
	OUT.norm = mul(IN.normal, (float3x3)gWorld);
	OUT.position = mul(IN.pos, gWorld).xyz;

	OUT.view = gViewInverse[3].xyz - OUT.position.xyz;

	float3 binorm = rageComputeBinormal(IN.normal, IN.tangent);
	OUT.tangent  = normalize(mul(IN.tangent.xyz, (float3x3)gWorld));
	OUT.binormal = normalize(mul(binorm, (float3x3)gWorld));

	OUT.diffuse = IN.diffuse;

	return OUT;
}

outputVertex VS_TransformSkin(inputVertexSkinned IN) 
{
	outputVertex OUT;
	float3 skinPos = rageSkinTransform(IN.pos, ComputeSkinMtx( IN.blendindices, IN.weight ));
	// Finish transform
	OUT.pos = mul(float4(skinPos,1), gWorldViewProj);
	if (debugShader) {
		OUT.tc.x = IN.tc.y;
		OUT.tc.y = IN.tc.x;
	}
	else
		OUT.tc = IN.tc;
	OUT.norm = mul(IN.normal, (float3x3)gWorld);
	OUT.position = mul(float4(skinPos,1), gWorld).xyz;

	OUT.view = gViewInverse[3].xyz - OUT.position.xyz;

	float3 binorm = rageComputeBinormal(IN.normal, IN.tangent);
	OUT.tangent  = normalize(mul(IN.tangent.xyz, (float3x3)gWorld));
	OUT.binormal = normalize(mul(binorm, (float3x3)gWorld));

	OUT.diffuse = IN.diffuse;

	return OUT;
}

//------------------------------------------------------------------------------
// Compute indirect lighting from spherical harmonics
//------------------------------------------------------------------------------
float3 GetLightingIndirect(float3 norm, float3 pos)
{
	float3 indirect = 0;

	// Right now only one sample...
	//float2 shCoord = giConvertFromPosToSHTex(pos);
	float3 sPos = giConvertFromPositionToSample(pos);
#define USE_LERP
#ifdef USE_LERP
	// Lower Z bound
	float3 lPos = sPos;
	lPos.z = floor(lPos.z);
	float3 nPos = saturate(lPos/float3(samplesWidth-1, samplesHeight-1, samplesDepth-1));
	float2 shCoordL;
	shCoordL.x = nPos.x;
	shCoordL.y = nPos.y/(samplesDepth);
	shCoordL.y += (lPos.z/(samplesDepth));

	// Upper Z bound
	float3 uPos = sPos;
	uPos.z = ceil(uPos.z);
	nPos = saturate(uPos/float3(samplesWidth-1, samplesHeight-1, samplesDepth-1));
	float2 shCoordU;
	shCoordU.x = nPos.x;
	shCoordU.y = nPos.y/(samplesDepth);
	shCoordU.y += (uPos.z/(samplesDepth));

	// lerp factor
	float zLerp = frac(sPos.z);
#else
	float3 nPos = round(sPos);
	nPos = saturate(nPos/float3(samplesWidth-1, samplesHeight-1, samplesDepth-1));
	float2 shCoord;
	shCoord.x = nPos.x;
	shCoord.y = nPos.y/(samplesDepth);
	shCoord.y += (nPos.z/(samplesDepth));
#endif

	//[unroll]
	//for (int i = 0; i < /*SH_COEFF_VECTORS*/ 1; i++)
	{
		// Get the SH coefficients from this point in space
#ifdef USE_LERP
		float4 cr = lerp(tex2D(SH_R_0_Sampler, shCoordL),
						 tex2D(SH_R_0_Sampler, shCoordU),
						 zLerp);
		float4 cg = lerp(tex2D(SH_G_0_Sampler, shCoordL),
						 tex2D(SH_G_0_Sampler, shCoordU),
						 zLerp);
		float4 cb = lerp(tex2D(SH_B_0_Sampler, shCoordL),
						 tex2D(SH_B_0_Sampler, shCoordU),
						 zLerp);
#else
		float4 cr = tex2D(SH_R_0_Sampler, shCoord);
		float4 cg = tex2D(SH_G_0_Sampler, shCoord);
		float4 cb = tex2D(SH_B_0_Sampler, shCoord);
#endif

		//float4 cb = ShVolumeB[i].Sample(volumeFilter, shCoord);

		// Get the SH transfer function coefficients for the normal direction
		//float4 sh = ShCube[i].Sample(volumeFilter, normal);
		float3 nrm = normalize(-norm);
		float4 sh = texCUBE(SH_Cube_Sampler, nrm);

		// The indirect lighting is the integral over the environment sphere
		// which is a dot-product in SH space.
		indirect.r += dot(cr, sh);
		indirect.g += dot(cg, sh);
		indirect.b += dot(cb, sh);
	}

	return indirect;
}//GetLightingIndirect

//------------------------------------------------------------------------------
// main pixel shader
//------------------------------------------------------------------------------
float4 PS_Textured(outputVertex IN) : COLOR
{
	float4 tex1 = tex2D(TextureSampler, IN.tc.xy);
	float4 bump = tex2D(bumpSampler, IN.tc.xy);
	float4 specularColor = tex2D(specSampler, IN.tc.xy)*specularColorFactor;
	float specStrength = specularColor.w * specularFactor;

    float3 bumpN = (bump.xyz - 0.5) * bumpiness;
    float3 N = normalize(IN.norm + (bumpN.x * IN.tangent) + (bumpN.y * IN.binormal) );
	//float3 N = normalize(IN.norm);

	float3 indirect = GetLightingIndirect(N, IN.position)*giFactor;

	//float3 direct = ComputeLight(IN.position, N);
	// Figure out light vector/distance
	float3 L = lightPos - IN.position;
	//float3 L = IN.view;
	float lDistSq = dot(L, L);
	float lDist = sqrt(lDistSq);
	L = L/lDist;

	// Now lambert lighting
	float NdotL = saturate(dot(N, L));
	float atten = lightAtten/lDistSq;
	float3 direct = NdotL*atten*lightColor.rgb;

	// Head light
	direct += (saturate(dot(N, normalize(IN.view)))*(headLightAtten/dot(IN.view, IN.view))*headLightColor.xyz);

	// Specular
    float3 E = normalize(IN.view);
    float3 V = normalize(IN.view);
	float3 R = normalize(reflect(-E, N));
	float3 H = normalize(L + E);
	//float3 specular = 0;
	float3 specular = pow(saturate(dot(N,H)), specStrength)*atten*lightColor.rgb;
	//float specIndirect = (GetLightingIndirect(R, IN.position)*giFactor);
	//specular += specIndirect;
	specular *= specularColor.xyz;
	//specIndirect *= specularColor;

	// Emissive
	float3 emissive = emissiveFactor + ((emissiveFactor-0.1)*emissiveMult);
	//float3 emissive = 0;

	// Compute lighting
	float4 lighting;
	lighting.rgb = ((indirect + direct + emissive)*tex1.xyz + specular)*IN.diffuse.xyz;
	lighting.a = 1.0f;

	// Output
	float4 cOut = lighting;
	cOut.rgb = lerp(cOut.rgb, indirect /*+ specIndirect*/, shShowIndirect);
	//cOut.rgb = N*0.5+0.5;
	//cOut.rgb = indirect*giFactor + emissive;
	//cOut.rgb = saturate(dot(N,V));
	//cOut.rgb = saturate(lightAtten/pow(length(IN.view),2));
	//cOut.rgb = tex1.a;
	return cOut;
}

technique draw
{
	pass p0 
	{        
	    AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = none;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_Textured() CGC_FLAGS("-po OutColorPrec=fp16") ;
	}
}

technique drawskinned
{
	pass p0 
	{        
	    AlphaTestEnable = false;
        AlphaBlendEnable = false;
        CullMode = none;

		VertexShader = compile VERTEXSHADER VS_TransformSkin();
		PixelShader  = compile PIXELSHADER PS_Textured() CGC_FLAGS("-po OutColorPrec=fp16");
	}
}


