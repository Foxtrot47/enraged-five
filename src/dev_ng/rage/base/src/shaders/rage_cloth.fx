
// Cloth shader
//
// Future Improvements/Bugs
// - move all vectors into tangent space so that the reference 
//   space system in the pixel shader is tangent space

#define USE_SPECULAR 1
#define USE_NORMAL_MAP 1
#define USE_DIFFUSE_DETAIL_MAP 1
#define USE_SHADOW_MAP 1
#define USE_GROUND_MAP 1
#define USE_SKINNING 1
#define CULL_MODE	CW


// Get the pong globals
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(4);

#include "../shaderlib/rage_cloth_common.fxh"
