
#ifndef __RMPTFX_LITSPRITE_FX
#define __RMPTFX_LITSPRITE_FX

#ifndef __RAGE_RMPTFX_COMMON_FXH
	#include "../shaderlib/rmptfx_common.fxh"
#endif	

#pragma dcl position normal diffuse texcoord0 texcoord1 texcoord2 texcoord3 texcoord4

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4x4	CameraInverse:CameraInverse REGISTER(c19);				// Inverse of camera

// NORMAL MAP		///////////////////////////////////////////////////////////////////////////////////////
BeginSampler(sampler2D,NormalMapTex,NormalMapTexSampler,NormalMapTex)
string UIName = "Normal Map";
ContinueSampler(sampler2D,NormalMapTex,NormalMapTexSampler,NormalMapTex)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
    AddressU  = CLAMP;        
    AddressV  = CLAMP;
EndSampler;

float3 LightColor : LightColor
<
	string UIName = "Light Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = float3(0.782f,0.7925f,0.06f);

float3 LightDir : LightDir
<
	float UIMin = -1.0;
	float UIMax = 1.0; 
	float UIStep = 0.01;
	string UIName = "Light Direction";
> = float3(0.697181f,-0.716895f,0.0f);

float3 CalculateSpecular( float3 normal, float3 eyePos, float specStrength)
{
	float3 H = normalize(LightDir + eyePos);
	float finalSpecular = pow(saturate(dot(normal, H)), specStrength);
	
	return LightColor.xyz * finalSpecular;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Output Structure
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct rmptfxPS {
	rmptfxPSBase BASE;
    float4 camSpaceLight	: TEXCOORD5;					// lightdir in camspace, softparticle precalc
    float4 screenPosRad		: TEXCOORD6;					// screenPositionX, screenPositionY, depth, radius
    float3 eye				: TEXCOORD7;
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 PS_Draw( rmptfxPS IN ) : COLOR
{
	float3 normal = ((tex2D(NormalMapTexSampler, IN.BASE.normalTexCoord.xy)*2.0f)-1.0f).xyz;	
	
	float4 texColor;
	rage_rmptfx_ComputeDiffusePixelColor(texColor,IN.BASE);
	float4 finalColor = texColor * IN.BASE.color;

	finalColor.xyz *= saturate( dot(normal.xyz, IN.camSpaceLight.xyz) );

	normal = normalize(mul(normal, (float3x3)gViewInverse)); // Normal must be in world space!
	
	// Put some highlights on the blood
	float3 E = normalize(IN.eye);
	float3 N = normalize(normal);
	float3 Intensity = 2.0f * (IN.BASE.color.w * IN.BASE.color.w);
	finalColor.xyz += Intensity * CalculateSpecular(N, E, 135.0f);
	finalColor.w = texColor.w;

	
	//return float4(normal.xyz,texColor.w);
	return finalColor;
	//return float4(1.0f,0.0f,0.0f,1.0f);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmptfxPS VS_Transform(rmptfxVS IN)
{
rmptfxPS OUT;
	OUT.BASE.color = IN.color;
	OUT.BASE.texCoord = IN.texCoord;
	OUT.BASE.pos = mul(float4(IN.center.xyz,1), gWorldViewProj);

	OUT.BASE.pos += rageComputeBillboard(IN.billboard.xyz);
	
	// Calculate light direction in camera space
	float3 dir = -normalize(LightDir);
	OUT.camSpaceLight = float4(mul(dir,float3x3(CameraInverse[0].xyz,CameraInverse[1].xyz,CameraInverse[2].xyz)),0);
	
	// "unrotate" light direction to compensate for sprite rotation of normal map
	if(IN.billboard.z!=0)
	{
		float sinTheta, cosTheta;
		sincos(-IN.billboard.z, sinTheta, cosTheta);	
		OUT.camSpaceLight.xy = float2(OUT.camSpaceLight.x * cosTheta - OUT.camSpaceLight.y * sinTheta, 
									OUT.camSpaceLight.x * sinTheta + OUT.camSpaceLight.y * cosTheta);
	}

	OUT.screenPosRad = float4(0,0,0,0);		
	OUT.eye = (gViewInverse[3].xyz - IN.center.xyz);	

	POSTPROCESS_VS(OUT.BASE,IN);
	
	return OUT;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	technique draw
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}
	technique drawskinned
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}
	technique unlit_draw
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}
	technique unlit_drawskinned
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}
#endif 

