echo Shaders with errors: > error.lst
for /f %%f in (preload.list) do (
	call %RS_TOOLSROOT%\script\coding\shaders\makeshader.bat %%f
	if ERRORLEVEL 1 echo ERROR ON %%f . . . >> error.lst
)

type error.lst
