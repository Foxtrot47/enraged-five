#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_samplers.fxh"

float4 gemColor : gemColor
<
	string UIName = "gemColor";
	float UIMin = 0.0f;
	float UIMax = 1.0f;
	float UIStep = 0.001f;
> = {0.0f, 0.7f, 0.0f, 0.7f};

float ir : IR
<
	string UIName = "IR";
	float UIMin = 0.0f;
	float UIMax = 10.0f;
	float UIStep = 0.01f;
> = 1.0f;

float fSpecTerm : SpecTerm
<
	string UIName = "Specular";
	float UIMin = 0.0f;
	float UIMax = 128.0f;
	float UIStep = 0.1f;
> = 6.00f;

BeginSampler(samplerCUBE, refractionMap, RefractionMapSampler, RefractionMap)
    string UIName = "Refraction Map";
ContinueSampler(samplerCUBE, refractionMap, RefractionMapSampler, RefractionMap)
EndSampler;

struct vertexInput 
{
    float4 pos				: POSITION;
    float3 normal 			: NORMAL;
    float2 uv				: TEXCOORD0;
};

struct vertexOutput  
{
   DECLARE_POSITION( Pos )
   float3 worldPos:	TEXCOORD0;
   float3 normal:	TEXCOORD1;
   float3 viewVec:	TEXCOORD2;
   float2 uv:		TEXCOORD4;
};

float4 ComputeLightColor(float3 pos, float3 normal, float3 view, int index)
{
	float4 lightColor = float4(gLightColor[index].xyz, 1);
	if( gLightType[index] == LT_POINT )
	{
		// Point Light		
		float3 lightDir = gLightPosDir[index].xyz - pos;
		float3 lightDirOverRange = lightDir * gLightPosDir[index].w;  // w is stored as 1/range
	
		float falloff = gLightColor[index].w - gLightColor[index].w*saturate(dot(lightDirOverRange, lightDirOverRange));

		float3 light = max(dot(normal,normalize(lightDir)), 0.0);
		lightColor.xyz = lightColor.xyz * light * falloff;	
		lightColor.w = pow(saturate(dot(reflect(-view, normal), lightDir)), fSpecTerm);
	}
	else
	{
		// Directional Light
		float3 lightDir = normalize(-gLightPosDir[index].xyz);
		
		float3 ndotl = dot(lightDir, normal);
		lightDir = max(ndotl, 0.0f);
		
		lightColor.xyz = lightDir * lightColor.xyz;
		lightColor.w = pow(saturate(dot(reflect(-view, normal), lightDir)), fSpecTerm);
	}	
	return lightColor;
}

vertexOutput VS_Gem( vertexInput IN ) 
{	
	vertexOutput OUT;
	OUT.Pos = mul(IN.pos, gWorldViewProj);
	OUT.worldPos = mul(IN.pos, gWorld).xyz;
	OUT.normal = mul(IN.normal, (float3x3)gWorld);
	OUT.viewVec = (float3)-mul(IN.pos, gWorldView);
	OUT.uv = IN.uv;

	return OUT;
}

float4 PS_Gem( vertexOutput IN, uniform float fColorScale )
{
	float3 vView = normalize(IN.viewVec);
	float3 vNrm = normalize(IN.normal);
	
	// Compute Light Color
	float4 light0 = ComputeLightColor(IN.worldPos, vNrm, vView, 0);
#if !__WIN32PC
	float4 light1 = ComputeLightColor(IN.worldPos, vNrm, vView, 1);
	float4 light2 = ComputeLightColor(IN.worldPos, vNrm, vView, 2);
	float4 lightColor = light0 + light1 + light2;
	float specular = min(light0.w + light1.w + light2.w, 0.5f) * fColorScale;
#else
	float4 lightColor = light0;
	float specular = min(light0.w, 0.5f) * fColorScale;
#endif
	
	// Lookup the refraction color
	float4 refractionColor = texCUBE(RefractionMapSampler, refract(vView, vNrm, ir));

	return saturate(refractionColor + (float4(lightColor.xyz, 1) * gemColor * fColorScale) + specular);
}

float4 PS_Gem_1( vertexOutput IN ) : COLOR
{
	return PS_Gem(IN, 1.0f);
}

float4 PS_Gem_05( vertexOutput IN ) : COLOR
{
	return PS_Gem(IN, 0.5f);
}

technique draw
{
	pass p0 
	{
		AlphaBlendEnable = false;
		DestBlend = InvSrcAlpha;
		CullMode = CCW;
		VertexShader = compile VERTEXSHADER VS_Gem();
		PixelShader = compile PIXELSHADER PS_Gem_1();
	}
	
	pass p1
	{
		AlphaBlendEnable = true;
		//DestBlend = DestColor;  Commented til dave fixes the parser for ps3
		BlendOp = Add;
		CullMode = CW;
		VertexShader = compile VERTEXSHADER VS_Gem();
		PixelShader  = compile PIXELSHADER PS_Gem_05();
	}

}

technique drawskinned
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_Gem();
		PixelShader  = compile PIXELSHADER PS_Gem_1();
	}

}
