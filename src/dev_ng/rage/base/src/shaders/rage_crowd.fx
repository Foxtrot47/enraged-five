#if !__PS3
#pragma pack_matrix (row_major)
#endif

#include "../shaderlib/rage_defines.fxh"

// These two lines guarantee that the constants rage uses internally for
// immediate mode support don't get thrashed by any shaders.
float4 gRageVertexConstants[23] REGISTER2(vs,c0);	
float4 gRagePixelConstants[1] REGISTER2(ps,c23);

float4x4 gCrowdWorld REGISTER2(vs,c32);
float4x4 gCrowdViewProj REGISTER2(vs,c36);
const float4x4 gCrowdBones[24] REGISTER2(vs,c40);

#include "../shaderlib/rage_samplers.fxh"

#define MAX_LIGHTS 4

// Need to match grcore/light.h
#define LT_DIR 0
#define LT_POINT 1
#define LT_SPOT 2
#define LT_COUNT 3

#define USE_SKIN 1

#define ACTIVE_LIGHTS 4  // MAX LIGHT IS 3 to keep the interface the same, but we don't really want 3 lights...

float4 gLightPosDir[MAX_LIGHTS] REGISTER2(vs, c136);
float4 gLightDir[MAX_LIGHTS] REGISTER2(vs, c140);
float4 gLightColor[MAX_LIGHTS] REGISTER2(vs, c144);
float gLightType[MAX_LIGHTS] REGISTER2(vs, c148);
float4 gAmbient REGISTER2(vs, c152) =  {0.0f, 0.0f, 0.0f, 0.0f};

// Put no state changes here, make sure we just hoist it out of the loop instead
BeginSampler(sampler3D,diffuseTexture,DiffuseSampler,DiffuseTex)
    string UIName = "Diffuse Texture";
ContinueSampler(sampler3D,diffuseTexture,DiffuseSampler,DiffuseTex)
EndSampler;

struct rageSkinVertexInput {
    float3 pos				: POSITION;
	float4 blendindices		: BLENDINDICES;
    float3 normal 			: NORMAL;
    float2 texCoord0		: TEXCOORD0;
};

struct vertexOutputUnlit {
    DECLARE_POSITION( pos)
    float4 color					: COLOR0;
    float3 texCoord					: TEXCOORD0;
};


float3 ComputeLightColor(float3 pos, float3 normal, int index)
{
	float3 lightColor;
	{
		float4 pointLight;
		pointLight.xyz = (gLightPosDir[index].xyz - pos);
		pointLight.w = length(pointLight.xyz);
		if (pointLight.w)
			pointLight.xyz /= pointLight.w;
			
		float4 dirLight;
		dirLight.xyz = -gLightPosDir[index].xyz;
		dirLight.w = 0.0;

		float4 lightPosDir = lerp(pointLight, dirLight, gLightType[index]);
		
		float3 L = lightPosDir.xyz;

		// Compute falloff term
		float falloff = 1.0 - min((lightPosDir.w / gLightPosDir[index].w), 1.0);
		float lightIntensity = gLightColor[index].w;
		if ( lightIntensity < 0.f ) 
		{
			falloff *= -lightIntensity;		// Allow oversaturation
		}
		else 
		{
			falloff = min(falloff * lightIntensity, 1.0);
		}

		float3 light = max(dot(normal,L), 0.0);

		// Sum up color contribution of light sources
		lightColor = gLightColor[index].rgb * light.y * falloff;
	}
	return lightColor;
}

vertexOutputUnlit VS_TransformSkinUnlit(rageSkinVertexInput IN) 
{
	vertexOutputUnlit OUT;

#if !__PS3	
	float4x4 crowdWorld = gCrowdWorld;

	float textureZ = gCrowdWorld._m03;
	crowdWorld._m03 = 0.0f;

#if USE_SKIN
	int4 IndexVector = D3DCOLORtoUBYTE4(IN.blendindices);  // we should pack this value in the pos.w
    float4x4 boneMtx = gCrowdBones[IndexVector.z];
    float3 pos = mul(float4(IN.pos.xyz,1),boneMtx).xyz;       // Transform position by bone matrix
#else
    float3 pos = IN.pos;
#endif
 
	pos = mul(float4(pos, 1), gCrowdWorld).xyz;
       
#if USE_SKIN
    float3 norm = mul(float4(IN.normal, 1), boneMtx).xyz;
#else
	float3 norm = float(IN.normal);
#endif
    norm = normalize(mul(norm, (float3x3)crowdWorld));

	float3 lightColor = gAmbient.rgb;
	lightColor += ComputeLightColor( pos, norm, 0);
//	lightColor += ComputeLightColor( pos, norm, 1);
//	lightColor += ComputeLightColor( pos, norm, 2);

    // Write out final position & texture coords
    OUT.pos = mul(float4(pos,1), gCrowdViewProj);
    OUT.color = float4(lightColor, 1);
    OUT.texCoord = float3(IN.texCoord0.xy, textureZ);
#else
	OUT.pos = float4(0.0f, 0.0f, 0.0f, 0.0f);
	OUT.color = float4(0.0f, 0.0f, 0.0f, 0.0f);
	OUT.texCoord = float3(0.0f, 0.0f, 0.0f);
#endif
    return OUT;
}


float4 PS_TexturedUnlit( vertexOutputUnlit IN ) : COLOR
{
	float4 texColor = float4(tex3D(DiffuseSampler, IN.texCoord.xyz).rgb,1);
	float4 finalColor = IN.color * texColor;    
    return saturate(finalColor);
}


technique draw
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_TransformSkinUnlit();
		PixelShader  = compile PIXELSHADER PS_TexturedUnlit();

	}
}

technique drawskinned
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_TransformSkinUnlit();
		PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
	}

}

// ===============================
// Unlit techniques
// ===============================
technique unlit_draw
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_TransformSkinUnlit();
		PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
	}
}

technique unlit_drawskinned
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_TransformSkinUnlit();
		PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
	}
}
