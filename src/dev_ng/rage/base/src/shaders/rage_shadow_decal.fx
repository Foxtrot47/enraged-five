#include "../shaderlib/rage_drawbucket.fxh"

RAGE_DRAWBUCKET(1);

// Configure the megashder
// Enable shadows
#define USE_SHADOW_MAP 1
// TODO: Slide verts along normal based on angle & distance to camera
#define USE_DEFAULT_TECHNIQUES

#include "../shaderlib/rage_megashader.fxh"
