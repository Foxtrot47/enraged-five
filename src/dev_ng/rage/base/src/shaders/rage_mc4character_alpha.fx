#define USE_ALPHA_BLENDING (1)
#include "../shaderlib/rage_mc4character_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(2);

#pragma dcl position normal diffuse texcoord0

technique draw
{
	pass	p0
	{
		AlphaBlendEnable = false;
#if __XENON
		HighPrecisionBlendEnable = true; 
#endif

		VertexShader = compile VERTEXSHADER VS();
		PixelShader  = compile PIXELSHADER PSAlpha();
	}
}

technique	drawskinned
{
	pass	p0
	{
		AlphaBlendEnable = true;
		ZWriteEnable = false;
#if __XENON
		HighPrecisionBlendEnable = true; 
#endif

		VertexShader = compile VERTEXSHADER VSSkinned();
		PixelShader = compile PIXELSHADER PSAlpha();
	}
	
	pass	p1
	{
		AlphaBlendEnable = true;
		ZWriteEnable = true;
#if __XENON
		HighPrecisionBlendEnable = true; 
#endif
		AlphaRef = 2;

		VertexShader = compile VERTEXSHADER VSSkinned();
		PixelShader = compile PIXELSHADER PSAlpha();
	}
}
