
#ifndef __RMPTFX_LITSPRITE_FX
#define __RMPTFX_LITSPRITE_FX

#ifndef __RAGE_RMPTFX_COMMON_FXH
	#include "../shaderlib/rmptfx_common.fxh"
#endif	

#pragma dcl position normal diffuse texcoord0 texcoord1 texcoord2 texcoord3 texcoord4

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4x4	gMyInverse : MyInverse;
// NORMAL MAP		///////////////////////////////////////////////////////////////////////////////////////
BeginSampler(sampler2D,NormalMapTex,NormalMapTexSampler,NormalMapTex)
string UIName = "Normal Map";
ContinueSampler(sampler2D,NormalMapTex,NormalMapTexSampler,NormalMapTex)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
    AddressU  = CLAMP;        
    AddressV  = CLAMP;
EndSampler;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Output Structure
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct rmptfxPS {
	rmptfxPSBase BASE;
    float3 worldpixelpos	: TEXCOORD5;
};

float Dist(float3 A, float3 B)
{
	float dx = A.x-B.x; 
	float dy = A.y-B.y; 
	float dz = A.z-B.z; 
	return sqrt(dx*dx + dy*dy + dz*dz);
}

float GetIntensity(float t, float3 pos)
{
	float3 a;
	float sty = sin(t+pos.y);
	float cty = cos(t+pos.y);
	
	a.x=sty;
	a.y=pos.y;
	a.z=cty;

	float3 b;
	b.x=0.5*cty;
	b.y=pos.y;
	b.z=0.5*sty;

	float dist1 =Dist(a,pos);
	float dist2 =Dist(b,pos);
	
	float dist = dist1;
	if(dist2<dist) dist = dist2;
	return 1.0-dist;
	

//	float3 a;
//	a.y=pos.y;
//	a.x=pos.y*sin(t)+pos.x;
//	a.z=pos.y*cos(t)+pos.z;
//	float d1= Dist(a,pos)*1;
//	a.x=cos(pos.y+t)+pos.x;
//	a.z=sin(pos.y+t)+pos.z;
//	float d2= Dist(a,pos)*1;
//	float d =d1;
//	if(d2<d) d= d2;
	//float d =(d1+d2)/2;
	//float2 value(d1/200,d2/200);
//	float value =d;
//	return value;	
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float4 PS_Draw( rmptfxPS IN ) : COLOR
{
	float4 texColor = tex2D(DiffuseTexSampler,IN.BASE.texCoord.xy);
	//IN.BASE.custom1.x is hacked hijack of the hybridadd variable
	float4 color = texColor * IN.BASE.color*GetIntensity(IN.BASE.custom1.x,IN.worldpixelpos);
	
	
	//POSTPROCESS_PS(color,IN.BASE);
	//return float4(IN.worldpixelpos.x,IN.worldpixelpos.y,IN.worldpixelpos.z,texColor.w);
	return color;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmptfxPS VS_Transform(rmptfxVS IN)
{
	rmptfxPS OUT;
	OUT.BASE.color = IN.color;
	OUT.BASE.texCoord = IN.texCoord;
	OUT.BASE.pos = mul(float4(IN.center.xyz,1), gWorldViewProj);
	OUT.worldpixelpos = IN.center.xyz;

	OUT.BASE.pos += rageComputeBillboard(IN.billboard.xyz);
	//this needs fixing (must get vert back into worldspace
	OUT.worldpixelpos = mul(float4(OUT.BASE.pos.xyz,1),gMyInverse).xyz;
	//OUT.worldpixelpos += rageComputeBillboard(IN.billboard.xyz);
	//OUT.worldpixelpos.z -= MyInverse[3].z;  //This is wrong -need to figure it out
	 //OUT.worldpixelpos.z = mul(float4(OUT.worldpixelpos.z,1,1,1),gViewInverse);
	//OUT.worldpixelpos = mul(float4(OUT.worldpixelpos,1), MyInverse);
	
	POSTPROCESS_VS(OUT.BASE,IN);

	return OUT;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	technique draw
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}
	technique drawskinned
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}
	technique unlit_draw
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}
	technique unlit_drawskinned
	{
		pass p0 
		{        
			VertexShader = compile VERTEXSHADER VS_Transform();
			PixelShader  = compile PIXELSHADER PS_Draw();
		}
	}
#endif 

