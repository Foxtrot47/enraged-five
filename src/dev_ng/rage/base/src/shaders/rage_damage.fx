string description = "Default per pixel lit fx shader used by rmcore"; 


#include "../../../../rage/base/src/shaderlib/rage_common.fxh"
#include "../../../../rage/base/src/shaderlib/rage_samplers.fxh"
#include "../../../../rage/base/src/shaderlib/rage_skin.fxh"

float3 materialDiffuse : MaterialDiffuse
<
    string UIWidget = "slider";
    float UIMin = 0.0;
    float UIMax = 200.0;
    float UIStep = .01;
    string UIName = "diffuse color";
> = { 1.0, 1.0, 1.0 };

// Normalized damage -- set to 1.0 for full damage, 0.0 for no damage
float damage : Damage = 0.0;

//------------------------------------

struct vertexOutput {
    DECLARE_POSITION(pos)
    float2 texCoord         : TEXCOORD0;
	float4 lightDistDir0	: TEXCOORD1;
	float4 lightDistDir1	: TEXCOORD2;
	float4 lightDistDir2	: TEXCOORD3;
	float3 worldPos			: TEXCOORD4;
	float3 worldEyePos		: TEXCOORD5;
	float3 worldNormal		: TEXCOORD6;
	float3 color0			: COLOR0;
};

struct vertexOutputUnlit {
    DECLARE_POSITION(pos)
    float2 texCoord         : TEXCOORD0;
	float3 color0			: COLOR0;
};

//------------------------------------

#pragma dcl position texcoord0

// ******************************
//	DRAW METHODS
// ******************************
vertexOutputUnlit VS_TransformUnlit(rageVertexInput IN)
{
    vertexOutputUnlit OUT;
    // Write out final position & texture coords
    OUT.pos =  mul(float4(IN.pos,1), gWorldViewProj);
    OUT.texCoord = IN.texCoord0;
    OUT.color0 = float3(1.0,1.0,1.0);
    return OUT;
}

// ******************************
//	DRAWSKINNED METHODS
// ******************************
vertexOutputUnlit VS_TransformSkinUnlit(rageSkinVertexInput IN) 
{
    vertexOutputUnlit OUT;
	rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
	// Transform position by bone matrix
	float3 pos = rageSkinTransform(IN.pos, boneMtx);

    // Write out final position & texture coords
    OUT.pos =  mul(float4(pos,1), gWorldViewProj);
    OUT.texCoord = IN.texCoord0;
    OUT.color0 = float3(1.0,1.0,1.0);
    return OUT;
}


//------------------------------------
BeginSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
	string UIName = "Diffuse Texture";
ContinueSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
EndSampler;

BeginSampler(sampler2D,damageTexture,DamageSampler,DamageTex)
	string UIName = "Damage Texture";
ContinueSampler(sampler2D,damageTexture,DamageSampler,DamageTex)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
EndSampler;

/* ====== PIXEL SHADERS =========== */
//-----------------------------------
float4 PS_TexturedUnlit( vertexOutputUnlit IN ) : COLOR
{
	float3 color = IN.color0;
	color *= materialDiffuse;

    float4 undamaged = tex2D(TextureSampler, IN.texCoord.xy) * (1.0 - damage);
    float4 damaged = tex2D(DamageSampler, IN.texCoord.xy) * damage;
    
    // Sum it all up
    return (float4(color,1.0) * (undamaged + damaged));
}

//-----------------------------------

technique draw
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER VS_TransformUnlit();
        PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
    }
}
technique drawskinned
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER VS_TransformSkinUnlit();
        PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
    }
}

technique unlit_draw
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER VS_TransformUnlit();
        PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
    }
}
technique unlit_drawskinned
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER VS_TransformSkinUnlit();
        PixelShader  = compile PIXELSHADER PS_TexturedUnlit();
    }
}

