//-----------------------------------------------------
// Rage Diffuse
//

// Get the pong globals
#include "../shaderlib/rage_megashader.fxh"
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
//#include "../shaderlib/rage_dof.fxh"

#if __PS3
shared float4 gDofParams REGISTER(c24);
shared float3 gDofProjData REGISTER(c25);
#else
// gDofParams[0] - Focal Plane Dist
// gDofParams[2] - Far Focus Offset
// gDofParams[3] - Far Focus Clamp
BeginConstantBufferDX10(rage_dofparams)
float4 gDofParams : DOF_PARAMS REGISTER(c24);
float3 gDofProjData : DOF_PROJ REGISTER(c25);
EndConstantBufferDX10(rage_dofparams)
#endif



#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(4);

struct vertexOutputComposite 
{
    DECLARE_POSITION( hPosition )
    float2 texCoordDiffuse  : TEXCOORD0;
    float depth				: TEXCOORD2;
};


//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN) 
{
    float3 Position, Normal, Tangent;

    vertexOutputComposite OUT = (vertexOutputComposite)0;

    OUT.hPosition = mul(float4(IN.pos, 1.0), gWorldViewProj);
    OUT.texCoordDiffuse = IN.texCoord0;
        
    // compute camera depth
    OUT.depth = OUT.hPosition.z;

    return OUT;
}


//
// compute depth value in worldviewproj space
// this code is PC only now
//
float ComputeDepthBlur (float depth)
{
/*
	// transform the depth value back into world space (so we can interpolate the DOF there)
	depth = gDofProjData[1]/(gDofProjData[0]- depth);

	// subtract the focal plane...
	depth = depth - gDofParams[0];

	float f=0;

	if (depth > 0)
	{
		// scale depth value between focal distance and far blur [0 to 1] range
		f = depth/gDofParams[2];
		f = saturate(f);
	}
*/
// gDofParams[0] - Focal Plane Dist
// gDofParams[2] - Far Focus Offset
// gDofParams[3] - Far Focus Clamp
	float f=0;
	/*
   if (depth < d_focus)
   {
      // scale depth value between near blur distance and focal distance to [-1, 0] range
      f = (depth - d_focus)/(d_focus - d_near);
   }
   else
   {
      // scale depth value between focal distance and far blur
      // distance to [0, 1] range
      f = (depth - d_focus)/(d_far - d_focus);

      // clamp the far blur to a maximum blurriness
      f = clamp (f, 0, far_clamp);
   }	
   */
   
   if (depth < gDofParams.x)
   {
      // scale depth value between near blur distance and focal distance to [-1, 0] range
      f = (depth - gDofParams.x)/(gDofParams.x - gDofParams.y);
   }
   else
   {
      // scale depth value between focal distance and far blur
      // distance to [0, 1] range
      f = (depth - gDofParams.x)/(gDofParams.z - gDofParams.x);

      // clamp the far blur to a maximum blurriness
      f = clamp (f, 0, gDofParams.w);
    }

	return f * 0.5 + 0.5;
}
//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS_CompositeLighting( vertexOutputComposite IN ) : COLOR
{
    // color map
    float4 Color = tex2D( TextureSampler, IN.texCoordDiffuse );
    
    return float4(Color.xyz, ComputeDepthBlur(IN.depth));
}

technique drawskinned
{
    pass p0 
    {/*
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = CULL_BACK;
        ZWriteEnable = true;
        ZEnable = true;
        */
        VertexShader = compile VERTEXSHADER VS_CompositeSkin();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}
