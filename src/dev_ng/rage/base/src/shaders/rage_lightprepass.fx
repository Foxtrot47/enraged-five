
#define DISABLE_RAGE_LIGHTING

#include "../../../../rage/base/src/shaderlib/rage_common.fxh"

// TexelSize is in here
#include "../../../../rage/base/src/shaderlib/rage_xplatformtexturefetchmacros.fxh"
#include "../../../../rage/base/src/shaderlib/rage_shadowmap_common.fxh"
#include "rdr2_lighting.fxh"

#define GEOMETRYLIGHTS

// floating-point depth buffer support on the 360 needs -fpz
#if __PSN
#define FLOAT half
#define FLOAT2 half2
#define FLOAT3 half3
#define FLOAT4 half4
#define FLOAT3x3 half3x3
#else
#define FLOAT float
#define FLOAT2 float2
#define FLOAT3 float3
#define FLOAT4 float4
#define FLOAT3x3 float3x3
#endif

float PointLightOneOverRangeSq;
/*
// The light buffer sampler, used by color shaders (TODO: Move to rdr2_lighting.fxh)
BeginSharedSampler(sampler, LightBufferTexture, LightBufferSampler, LightBufferTex, s11)
ContinueSharedSampler(sampler, LightBufferTexture, LightBufferSampler, LightBufferTex, s11)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIPFILTER = NONE;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
EndSharedSampler;
*/
BeginSampler(sampler2D,DepthMap,DepthMapSampler,DepthMap)
ContinueSampler(sampler2D,DepthMap,DepthMapSampler,DepthMap)
   MinFilter = POINT;
   MagFilter = POINT;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

// fallback default samplers
BeginSampler(sampler2D,RenderMap,RenderMapSampler,RenderMap)
ContinueSampler(sampler2D,RenderMap,RenderMapSampler,RenderMap)
   MinFilter = LINEAR;
   MagFilter = LINEAR;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

/*
// The normal buffer sampler, used by the light shaders.
BeginSharedSampler(sampler, NormalBufferTexture, NormalBufferSampler, NormalBufferTex, s12)
ContinueSharedSampler(sampler, NormalBufferTexture, NormalBufferSampler, NormalBufferTex, s12)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIPFILTER = NONE;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
EndSharedSampler;
*/

//#define OPTIMIZEDPOINTLIGHTS



FLOAT3 NearFarClipPlaneQ;

#if OPTIMIZEDPOINTLIGHTS
	float2 TanHalfFOVAspect;
	float4x4 InverseViewProj;
#endif

float4x4 WorldViewProjInverse;

float4 PointLightSphere;
float3 PointLightColor;
float3 AttenuationConstants;

float3 SpotLightDirection;

float3 ViewDirection;
float2 OuterConeInnerCone;

struct SimpleVertexInput
{
    float3 pos			: POSITION;
};

// special struct for screen space blits
struct VertexOutput
{
	// This is used for the simple vertex and pixel shader routines
    DECLARE_POSITION(pos)
#ifndef OPTIMIZEDPOINTLIGHTS
    float4 texCoord0	: TEXCOORD0;
#else
    float4 vPos	: TEXCOORD0;
#endif    
#ifdef GEOMETRYLIGHTS
#ifndef OPTIMIZEDPOINTLIGHTS
    float4 texCoord1	: TEXCOORD1;
#else    
    float4 vEyeRay	: TEXCOORD1;
#endif    
#endif    
};

#ifdef OPTIMIZEDPOINTLIGHTS
float4 ConvertToVPos( float4 p )
{
	return float4( 0.5 * ( float2(p.x + p.w, p.w - p.y) + p.w * TexelSize.xy), p.zw);
}

float3 CreateEyeRay( float4 p )
{	
	// construct a ray in view space
	// TanHalfFOVAspect.x == TanHalfFOV * AspectRatio
	// TanHalfFOVAspect.y == TanHalfFOV
	float3 ViewSpaceRay = float3( p.x * TanHalfFOVAspect.x, p.y * TanHalfFOVAspect.y, p.w);

	// transform from view space to world space
	return mul( ViewSpaceRay, InverseViewProj); 
}
#endif


#ifdef GEOMETRYLIGHTS

VertexOutput VS_PointLightVolume(SimpleVertexInput IN)
{
 	VertexOutput OUT =(VertexOutput)0;

    OUT.pos = mul(float4(IN.pos,1), gWorldViewProj);
    return OUT;
}

// -------------------------------------------------------------
//
//
// -------------------------------------------------------------
VertexOutput VS_PointLight(rageVertexInput IN)
{
 	VertexOutput OUT =(VertexOutput)0;
#ifndef OPTIMIZEDPOINTLIGHTS
	OUT.pos = FLOAT4(IN.pos, 1.0f);

	// calculate world-space x and y value for depth re-construction
	FLOAT2 halfscres = TexelSize.zw * 0.5f;
	
	//
	// in texCoord0.zw this gives us a value range of 
	// -1 * 512 + 512 .. 1 * 512 + 512 and then 0..1024
	//
    OUT.texCoord0 = float4(IN.texCoord0.x, IN.texCoord0.y, 
						   IN.pos.x * halfscres.x + halfscres.x,
						 - IN.pos.y * halfscres.y + halfscres.y);

#else						 
	OUT.pos = FLOAT4(IN.pos, 1.0f);
							 
    OUT.vPos = ConvertToVPos( OUT.pos );
    OUT.vEyeRay.xyz = CreateEyeRay( OUT.pos );						 
#endif						 
    return OUT;
}

float CalculateAttenuation( float3 WorldPosition, float3 AttenuationC, float3 LightPosition)
{
	float3 dist = LightPosition - WorldPosition;
	float d = dot(dist, dist);
	return pow(saturate((PointLightSphere.w - d) / PointLightSphere.w), AttenuationC.z);
}

FLOAT4 PS_PointLight(VertexOutput IN) : COLOR
{
	//
	// reconstruct world space position
	//
#ifndef OPTIMIZEDPOINTLIGHTS

#if __XENON || __WIN32PC || __PSSL
	float gCurrDepth = tex2D(DepthMapSampler, IN.texCoord0.xy).x;
#elif __PSN
	float gCurrDepth = texDepth2D(DepthMapSampler, IN.texCoord0.xy);
#endif	
	
	FLOAT2 PositionXY = IN.texCoord0.zw;
	
	FLOAT3 screenPos;
#if __XENON || __WIN32PC || __PSSL
	// we have a float depth buffer here .. so invert it
	screenPos = FLOAT3(PositionXY, 1.0f - gCurrDepth);
#elif __PSN
	screenPos = FLOAT3(PositionXY, gCurrDepth);
#endif	

	FLOAT4 worldPos4 = mul(FLOAT4(screenPos, 1.0f), WorldViewProjInverse);
	worldPos4.xyz /= worldPos4.w;
	
	// find light direction
	FLOAT3 LightDir = (PointLightSphere.xyz - worldPos4.xyz);
#else

#if __XENON || __WIN32PC || __PSSL
	float gCurrDepth = tex2Dproj(DepthMapSampler, IN.vPos).x;
#elif __PSN
	float gCurrDepth = texDepth2Dproj(DepthMapSampler, IN.vPos);
#endif	

    IN.vEyeRay.xyz /= IN.vEyeRay.z;
    
#if __XENON  || __WIN32PC || __PSSL
    float3 PixelPos   = IN.vEyeRay.xyz * (1.0f - gCurrDepth) + gViewInverse[3].xyz;	
#elif __PSN
    float3 PixelPos   = IN.vEyeRay.xyz * (gCurrDepth) + gViewInverse[3].xyz;	
#endif
	FLOAT3 LightDir = (PointLightSphere.xyz - PixelPos);

#endif

//return float4(PixelPos, 1.0f);

	//
	// read normal map data
	//
#ifndef OPTIMIZEDPOINTLIGHTS
	FLOAT4 ShadowCollector = tex2D(ShadowCollectorSampler, IN.texCoord0.xy);
#else
	FLOAT4 ShadowCollector = tex2Dproj(ShadowCollectorSampler, IN.vPos);
#endif	
	FLOAT3 Normal = ShadowCollector.yzw * 2.0f - 1.0f;
	
//	return float4(Normal, 1.0f);

#ifndef OPTIMIZEDPOINTLIGHTS
	float Attenuation = CalculateAttenuation( worldPos4.xyz, AttenuationConstants, PointLightSphere.xyz);
#else	
	float Attenuation = CalculateAttenuation( PixelPos, AttenuationConstants, PointLightSphere.xyz);
#endif

//	float distance = dot(LightDir, LightDir);
//	float Attenuation = saturate( ( distance * -PointLightOneOverRangeSq ) + 1 );

//	FLOAT Attenuation = saturate(1 - dot(LightDir / PointLightSphere.w, LightDir / PointLightSphere.w));
	LightDir = normalize(LightDir);
	
// 	return dot(LightDir, Normal);
	
	
	// get N.H
#ifndef OPTIMIZEDPOINTLIGHTS
	float NH = dot(Normal, normalize(LightDir + (gViewInverse[3].xyz - worldPos4.xyz))); 
#else
	float NH = dot(Normal, normalize(LightDir + (gViewInverse[3].xyz - PixelPos))); 
#endif
	
	// now get specular
	// just pick a specular power value until we implement one
	float Specular = pow(NH, 20.0f);
	
	// now get diffuse
	float Diffuse = saturate(dot(LightDir, (Normal))) * Attenuation;
//	Diffuse = saturate( Diffuse * rsqrt(distance) );	
	
	// store the magic sauce in the light buffer
	return float4(PointLightColor.x * Diffuse, 
					PointLightColor.y * Diffuse, 
					PointLightColor.z * Diffuse, 
					// Diffuse is here considered as the self-shadowing term					
					Specular * Diffuse);
}


VertexOutput VS_SpotLightVolume(SimpleVertexInput IN)
{
 	VertexOutput OUT =(VertexOutput)0;

    OUT.pos = mul(float4(IN.pos,1), gWorldViewProj);
    return OUT;
}

#ifndef OPTIMIZEDPOINTLIGHTS
VertexOutput VS_SpotLight(rageVertexInput IN)
{
 	VertexOutput OUT =(VertexOutput)0;
	OUT.pos = FLOAT4(IN.pos, 1.0f);

	// calculate world-space x and y value for depth re-construction
	FLOAT2 halfscres = TexelSize.zw * 0.5f;

	//
	// in texCoord0.zw this gives us a value range of 
	// -1 * 512 + 512 .. 1 * 512 + 512 and then 0..1024
	//
    OUT.texCoord0 = float4(IN.texCoord0.x, IN.texCoord0.y, 
						   IN.pos.x * halfscres.x + halfscres.x,
						 - IN.pos.y * halfscres.y + halfscres.y);
						 
    return OUT;
}
#endif

float linstep(float lower, float upper, float s)
{
	return saturate((s - lower) / (upper - lower));
}

float SStep(float lower, float upper, float a)
{
	float x = saturate((a - lower) / (upper - lower));

	return saturate(x * x * (3 - 2 * x));
}

#ifndef OPTIMIZEDPOINTLIGHTS
FLOAT4 PS_SpotLight(VertexOutput IN) : COLOR
{
	//
	// reconstruct world space position
	//
#if __XENON || __WIN32PC || __PSSL
	float gCurrDepth = tex2D(DepthMapSampler, IN.texCoord0.xy).x;
#elif __PSN
	float gCurrDepth = texDepth2D(DepthMapSampler, IN.texCoord0.xy);
#endif	
	
	FLOAT2 PositionXY = IN.texCoord0.zw;
	
	FLOAT3 screenPos;
#if __XENON || __WIN32PC || __PSSL
	// we have a float depth buffer here .. so invert it
	screenPos = FLOAT3(PositionXY, 1.0f - gCurrDepth);
#elif __PSN
	screenPos = FLOAT3(PositionXY, gCurrDepth);
#endif	

	FLOAT4 worldPos4 = mul(FLOAT4(screenPos, 1.0f), WorldViewProjInverse);
	worldPos4.xyz /= worldPos4.w;
	
	// find light direction
	FLOAT3 LightDir = (PointLightSphere.xyz - worldPos4.xyz);
	
	//
	// read normal map data
	//
	FLOAT4 ShadowCollector = tex2D(ShadowCollectorSampler, IN.texCoord0.xy);
	FLOAT3 Normal = ShadowCollector.yzw * 2.0f - 1.0f;

	float distance = dot(LightDir, LightDir);
//	float Attenuation = saturate( ( distance * -PointLightOneOverRangeSq ) + 1 );
	float Attenuation = CalculateAttenuation( worldPos4.xyz, AttenuationConstants, PointLightSphere.xyz);

//	FLOAT Attenuation = saturate(1 - dot(LightDir / PointLightSphere.w, LightDir / PointLightSphere.w));
	LightDir = normalize(LightDir);
	
	// get angle between current light beam and the spotlight direction
	float CosAngle = dot(SpotLightDirection, LightDir);

	// CosOuter and CosInner are user defined
	float SpotFactor = smoothstep(OuterConeInnerCone.x, OuterConeInnerCone.y, CosAngle);
	//float SpotFactor = linstep(OuterConeInnerCone.x, OuterConeInnerCone.y, CosAngle);
	
	// get N.H
	float NH = dot(Normal, normalize(LightDir + (gViewInverse[3].xyz - worldPos4.xyz))); 
//		float NH = dot(Normal, normalize(LightDir + ViewDirection));
	
	// now get specular
	// just pick a specular power value until we implement one
	float Specular = pow(NH, 20.0f);
	
	// now get diffuse
	float Diffuse = saturate(dot(LightDir, (Normal))) * SpotFactor * Attenuation;
//	Diffuse = saturate( Diffuse * rsqrt(distance) ) * Attenuation;	
	
	// store the magic sauce in the light buffer
	return float4(PointLightColor.x * Diffuse, 
					PointLightColor.y * Diffuse, 
					PointLightColor.z * Diffuse, 
					// Diffuse is here considered as the self-shadowing term					
					Specular * Diffuse);
}
#endif

#else

// -------------------------------------------------------------
//
//
// -------------------------------------------------------------
VertexOutput VS_PointLight(rageVertexInput IN)
{
 	VertexOutput OUT =(VertexOutput)0;
	OUT.pos = FLOAT4(IN.pos, 1.0f);

	// calculate world-space x and y value for depth re-construction
	FLOAT2 halfscres = TexelSize.zw * 0.5f;
	
	//
	// in texCoord0.zw this gives us a value range of 
	// -1 * 512 + 512 .. 1 * 512 + 512 and then 0..1024
	//
    OUT.texCoord0 = float4(IN.texCoord0.x, IN.texCoord0.y, 
						   IN.pos.x * halfscres.x + halfscres.x,
						 - IN.pos.y * halfscres.y + halfscres.y);
						 
    return OUT;
}

/*
float Attenuation( float3 WorldPosition, float3 AttenuationC, float3 LightPosition)
{
	float d = distance(WorldPosition, LightPosition);
	return 1.0f / (AttenuationC.x + AttenuationC.y * d + AttenuationC.z * d * d);
}
*/

FLOAT4 PS_PointLight(VertexOutput IN) : COLOR
{
	//
	// reconstruct world space position
	//
#if __XENON || __WIN32PC || __PSSL
	float gCurrDepth = tex2D(DepthMapSampler, IN.texCoord0.xy).x;
#elif __PSN
	float gCurrDepth = texDepth2D(DepthMapSampler, IN.texCoord0.xy);
#endif	
	
	FLOAT2 PositionXY = IN.texCoord0.zw;
	
	FLOAT3 screenPos;
#if __XENON || __WIN32PC || __PSSL
	// we have a float depth buffer here .. so invert it
	screenPos = FLOAT3(PositionXY, 1.0f - gCurrDepth);
#elif __PSN
	screenPos = FLOAT3(PositionXY, gCurrDepth);
#endif	

	FLOAT4 worldPos4 = mul(FLOAT4(screenPos, 1.0f), WorldViewProjInverse);
	worldPos4.xyz /= worldPos4.w;
	
	// find light direction
	FLOAT3 LightDir = (PointLightSphere.xyz - worldPos4.xyz);
	
	if (length(LightDir) < PointLightSphere.w)
	{
		//
		// read normal map data
		//
		FLOAT4 ShadowCollector = tex2D(ShadowCollectorSampler, IN.texCoord0.xy);
		FLOAT3 Normal = ShadowCollector.yzw * 2.0f - 1.0f;

		float distance = dot(LightDir, LightDir);
		float Attenuation = saturate( ( distance * -PointLightOneOverRangeSq ) + 1 );

	//	FLOAT Attenuation = saturate(1 - dot(LightDir / PointLightSphere.w, LightDir / PointLightSphere.w));
		LightDir = normalize(LightDir);
		
		// get N.H
		float NH = dot(Normal, normalize(LightDir + (gViewInverse[3].xyz - worldPos4.xyz))); 
//		float NH = dot(Normal, normalize(LightDir + ViewDirection));
		
		// now get specular
		// just pick a specular power value until we implement one
		float Specular = pow(NH, 20.0f);
		
		// now get diffuse
		float Diffuse = dot(LightDir, (Normal)) * Attenuation;
		Diffuse = saturate( Diffuse * rsqrt(distance) );	
		
		// store the magic sauce in the light buffer
		return float4(PointLightColor.x * Diffuse, 
						PointLightColor.y * Diffuse, 
						PointLightColor.z * Diffuse, 
						// Diffuse is here considered as the self-shadowing term					
						Specular * Diffuse);
	}
	else
	  return 0.0f; 
/*
	float Diffuse = dot(LightDir, (Normal)) * Attenuation(worldPos4.xyz, AttenuationConstants, PointLightSphere.xyz);
*/					
}

VertexOutput VS_SpotLight(rageVertexInput IN)
{
 	VertexOutput OUT =(VertexOutput)0;
	OUT.pos = FLOAT4(IN.pos, 1.0f);

	// calculate world-space x and y value for depth re-construction
	FLOAT2 halfscres = TexelSize.zw * 0.5f;

	//
	// in texCoord0.zw this gives us a value range of 
	// -1 * 512 + 512 .. 1 * 512 + 512 and then 0..1024
	//
    OUT.texCoord0 = float4(IN.texCoord0.x, IN.texCoord0.y, 
						   IN.pos.x * halfscres.x + halfscres.x,
						 - IN.pos.y * halfscres.y + halfscres.y);
						 
    return OUT;
}


float linstep(float lower, float upper, float s)
{
	return saturate((s - lower) / (upper - lower));
}

float SStep(float lower, float upper, float a)
{
	float x = saturate((a - lower) / (upper - lower));

	return saturate(x * x * (3 - 2 * x));
}


FLOAT4 PS_SpotLight(VertexOutput IN) : COLOR
{
	//
	// reconstruct world space position
	//
#if __XENON || __WIN32PC || __PSSL
	float gCurrDepth = tex2D(DepthMapSampler, IN.texCoord0.xy).x;
#elif __PSN
	float gCurrDepth = texDepth2D(DepthMapSampler, IN.texCoord0.xy);
#endif	
	
	FLOAT2 PositionXY = IN.texCoord0.zw;
	
	FLOAT3 screenPos;
#if __XENON || __WIN32PC || __PSSL
	// we have a float depth buffer here .. so invert it
	screenPos = FLOAT3(PositionXY, 1.0f - gCurrDepth);
#elif __PSN
	screenPos = FLOAT3(PositionXY, gCurrDepth);
#endif	

	FLOAT4 worldPos4 = mul(FLOAT4(screenPos, 1.0f), WorldViewProjInverse);
	worldPos4.xyz /= worldPos4.w;
	
	// find light direction
	FLOAT3 LightDir = (PointLightSphere.xyz - worldPos4.xyz);
	
	if (length(LightDir) < PointLightSphere.w)
	{
		//
		// read normal map data
		//
		FLOAT4 ShadowCollector = tex2D(ShadowCollectorSampler, IN.texCoord0.xy);
		FLOAT3 Normal = ShadowCollector.yzw * 2.0f - 1.0f;

		float distance = dot(LightDir, LightDir);
		float Attenuation = saturate( ( distance * -PointLightOneOverRangeSq ) + 1 );

	//	FLOAT Attenuation = saturate(1 - dot(LightDir / PointLightSphere.w, LightDir / PointLightSphere.w));
		LightDir = normalize(LightDir);
		
		// get angle between current light beam and the spotlight direction
		float CosAngle = dot(SpotLightDirection, LightDir);
	
		// CosOuter and CosInner are user defined
		float SpotFactor = smoothstep(OuterConeInnerCone.x, OuterConeInnerCone.y, CosAngle);
		//float SpotFactor = linstep(OuterConeInnerCone.x, OuterConeInnerCone.y, CosAngle);
		
		// get N.H
		float NH = dot(Normal, normalize(LightDir + (gViewInverse[3].xyz - worldPos4.xyz))); 
//		float NH = dot(Normal, normalize(LightDir + ViewDirection));
		
		// now get specular
		// just pick a specular power value until we implement one
		float Specular = pow(NH, 20.0f);
		
		// now get diffuse
		float Diffuse = dot(LightDir, (Normal)) * SpotFactor;
		Diffuse = saturate( Diffuse * rsqrt(distance) ) * Attenuation;	
		
		// store the magic sauce in the light buffer
		return float4(PointLightColor.x * Diffuse, 
						PointLightColor.y * Diffuse, 
						PointLightColor.z * Diffuse, 
						// Diffuse is here considered as the self-shadowing term					
						Specular * Diffuse);
	}
	else
	  return 0.0f; 
}
#endif

#ifndef OPTIMIZEDPOINTLIGHTS
VertexOutput VS_DirectionalLight(rageVertexInput IN)
{
 	VertexOutput OUT =(VertexOutput)0;
	OUT.pos = FLOAT4(IN.pos, 1.0f);

	// calculate world-space x and y value for depth re-construction
	FLOAT2 halfscres = TexelSize.zw * 0.5f;

	//
	// in texCoord0.zw this gives us a value range of 
	// -1 * 512 + 512 .. 1 * 512 + 512 and then 0..1024
	//
    OUT.texCoord0 = float4(IN.texCoord0.x, IN.texCoord0.y, 
						   IN.pos.x * halfscres.x + halfscres.x,
						 - IN.pos.y * halfscres.y + halfscres.y);
						 
    return OUT;
}


FLOAT4 PS_DirectionalLight(VertexOutput IN) : COLOR
{
	//
	// read normal map data
	//
	FLOAT4 ShadowCollector = tex2D(ShadowCollectorSampler, IN.texCoord0.xy);
	FLOAT3 Normal = ShadowCollector.yzw * 2.0f - 1.0f;
	
	//
	// reconstruct world space position
	//
#if __XENON || __WIN32PC || __PSSL
	float gCurrDepth = tex2D(DepthMapSampler, IN.texCoord0.xy).x;
#elif __PSN
	float gCurrDepth = texDepth2D(DepthMapSampler, IN.texCoord0.xy);
#endif	
	
	FLOAT2 PositionXY = IN.texCoord0.zw;
	
	FLOAT3 screenPos;
#if __XENON || __WIN32PC || __PSSL
	// we have a float depth buffer here .. so invert it
	screenPos = FLOAT3(PositionXY, 1.0f - gCurrDepth);
#elif __PSN
	screenPos = FLOAT3(PositionXY, gCurrDepth);
#endif	

	FLOAT4 worldPos4 = mul(FLOAT4(screenPos, 1.0f), WorldViewProjInverse);
	worldPos4.xyz /= worldPos4.w;
	
	// diffuse directional light contribution
	float NL = saturate(dot(gPrimaryDirectionalDir, Normal));

	float4 Return;
	Return.xyz = gPrimaryDirectionalColor.rgb * NL;
	
	// get N.H
	float NH = dot(Normal, normalize(gPrimaryDirectionalDir + (gViewInverse[3].xyz - worldPos4.xyz))); 
		
	// now get specular
	// just pick a specular power value until we implement one
	Return.w = pow(NH, 20.0f) * NL;
	
	return Return * ShadowCollector.x;

//	return float4(Return.xyz, 0.0f);
}
#endif


// -------------------------------------------------------------
// PSCopyRT
// - plain copy
// -------------------------------------------------------------

float4 PS_NULL( rageVertexOutputPassThrough IN ) : COLOR
{
	// only xbox supports NULL shaders so here we have an empty shader to emulate that for the win32/PS3
	return float4(0,0,0,0);
}

float4 PSCopyRT( rageVertexOutputPassThrough IN): COLOR
{
     return tex2D(RenderMapSampler, IN.texCoord0);
}

float4 PSCopyRTSPEC( rageVertexOutputPassThrough IN): COLOR
{
     return float4(tex2D(RenderMapSampler, IN.texCoord0).aaa, 1.0f);
}

technique PointLight
{
    pass P0
    {
		VertexShader = compile vs_3_0 VS_PointLight();
		PixelShader = compile ps_3_0 PS_PointLight();

#ifdef GEOMETRYLIGHTS
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
		AlphaBlendEnable = True;
		SrcBlend = One;
		DestBlend = One;
		ZEnable = true;
		ZWriteEnable = false;	
#else
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
		AlphaBlendEnable = True;
		SrcBlend = One;
		DestBlend = One;
		AlphaTestEnable = false;
		AlphaRef = 0;
		CullMode = CCW;
		ZEnable = True;
		ZWriteEnable = False;	
#endif	
		}
}

#ifdef GEOMETRYLIGHTS
technique PointLightVolume
{
    pass P0
    {
		CullMode = NONE;
		ColorWriteEnable = 0;
		AlphaBlendEnable = false;
		AlphaTestEnable = false;
		ZEnable = True;
		ZWriteEnable = False;	
		VertexShader = compile vs_3_0 VS_PointLightVolume();
		#if __XENON
			PixelShader  = NULL;
		#else
			PixelShader  = compile ps_3_0 PS_NULL();
		#endif
	}
}

technique SpotLightVolume
{
    pass P0
    {
		CullMode = NONE;
		ColorWriteEnable = 0;
		AlphaBlendEnable = false;
		AlphaTestEnable = false;
		ZEnable = True;
		ZWriteEnable = false;	
		VertexShader = compile vs_3_0 VS_SpotLightVolume();
		#if __XENON
			PixelShader  = NULL;
		#else
			PixelShader  = compile ps_3_0 PS_NULL();
		#endif
	}
}
#endif	

#ifndef OPTIMIZEDPOINTLIGHTS
technique DirectionalLight
{
    pass P0
    {
		VertexShader = compile vs_3_0 VS_DirectionalLight();
		PixelShader = compile ps_3_0 PS_DirectionalLight();
		AlphaBlendEnable = True;
		AlphaTestEnable = false;
		SrcBlend = One;
		DestBlend = One;

		ZEnable = false;
		ZWriteEnable = False;	
		}
}
#endif

technique SpotLight
{
    pass P0
    {
#ifndef OPTIMIZEDPOINTLIGHTS
		VertexShader = compile vs_3_0 VS_SpotLight();
		PixelShader = compile ps_3_0 PS_SpotLight();
#else
		VertexShader = compile vs_3_0 VS_PointLight();
		PixelShader = compile ps_3_0 PS_PointLight();
#endif		
		
#ifdef GEOMETRYLIGHTS
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
		AlphaBlendEnable = True;
		SrcBlend = One;
		DestBlend = One;
		ZEnable = true;
		ZWriteEnable = false;	
#else
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
		AlphaBlendEnable = True;
		SrcBlend = One;
		DestBlend = One;
		AlphaTestEnable = false;
		AlphaRef = 0;
		CullMode = CCW;
		ZEnable = True;
		ZWriteEnable = False;	
#endif	
	}
}



technique CopyRT
{

    pass P0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = NONE;
        ZWriteEnable = false;
        ZEnable = false;
        VertexShader = compile vs_3_0 VS_ragePassThroughNoXform();
        PixelShader  = compile ps_3_0 PSCopyRT();
    }
}

technique CopyRTSPEC
{

    pass P0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = NONE;
        ZWriteEnable = false;
        ZEnable = false;
        VertexShader = compile vs_3_0 VS_ragePassThroughNoXform();
        PixelShader  = compile ps_3_0 PSCopyRTSPEC();
    }
}
