#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"

#include "../shaderlib/rage_ambient.fxh"
#include "../shaderlib/rage_diffuse_sampler.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(7);

float4x4 lightProjMtx : LightMtx0;				// Projection matrix used for the shadow casting light


struct vertexOutputComposite {
    DECLARE_POSITION( hPosition)
    float2 texCoordDiffuse	: TEXCOORD0;
    float2 texCoordNoise	: TEXCOORD2;
	float4 lightDistDir0	: TEXCOORD1;
	//float4 lightDistDir1	: TEXCOORD2;
	float4 vColor			: TEXCOORD7;
	
	float3 worldPos			: TEXCOORD4;
	float3 worldEyePos		: TEXCOORD5;
	float3 worldNormal		: TEXCOORD6;
	float3 worldTangent		: TEXCOORD3;
	float3 worldBinormal	: COLOR1;
};

BeginConstantBufferDX10(rage_combing)
float useCombingData = false;
EndConstantBufferDX10(rage_combing)

float combScale <
	float UIMin = 0.0;
    float UIMax = 10.0;
    float UIStep = 0.001;
	string UIWidget = "combing scale";
    string UIName =  "combing scale";
> = 0.572f;

float4 hairColor1 <
	float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
	string UIWidget = "hair color 1";
    string UIName =  "hair color 1";
> = {0.0f, 0.0f, 0.0f, 1.0f};

float4 hairColor2 <
	float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
	string UIWidget = "hair color 2";
    string UIName =  "hair color 2";
> = {1.0f, 1.0f, 1.0f, 1.0f};

float noiseUVScale
<
	float UIMin = 0.0;
    float UIMax = 100.0;
    float UIStep = 0.01;
	string UIWidget = "uv scale";
    string UIName =  "uv sclae";
> = 1.0f;

float diffuseExponent
<
	string UIWidget = "diffuse exponent";
	float UIMin = 0.0;
	float UIMax = 10000.0;
	float UIStep = 0.1;
    string UIName =  "diffuse exponent";
> = 1.0f;

float LayerAlpha = 1.0f;
float LayerScale : LayerScale = 0.0f;
float LayerLightMod = 0.0f;
float TexCoordShift = 0.0f;
float VolLayer = 0.5f;

float specExp1
<
	string UIName = "Primary Highlight Exponent";
	float UIMin = 0.0;
	float UIMax = 10000.0;
	float UIStep = 0.1;
> = 40.0f;

float specExp2
<
	string UIName = "Secondary Highlight Exponent";
	float UIMin = 0.0;
	float UIMax = 10000.0;
	float UIStep = 0.1;
> = 40.0f;

float specOffset1
<
	string UIName = "Primary Highlight Offset";
	float UIMin = -100.0;
	float UIMax = 100.0;
	float UIStep = 0.001;
> = 0.0f;

float specOffset2
<
	string UIName = "Secondary Highlight Offset";
	float UIMin = -100.0;
	float UIMax = 100.0;
	float UIStep = 0.001;
> = 0.0f;

float4 specColor1
<
	string UIName = "Primary Specular Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = { 0.25f, 0.25f, 0.25f, 0.25f };

float4 specColor2
<
	string UIName = "Secondary Specular Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = { 0.25f,0.25f,0.25f,0.25f };

BeginSampler(sampler3D,noiseTexture,NoiseTextureSampler,NoiseTex)
	string UIName = "Short hair noise tex";
	string UIHint = "James fix this";
ContinueSampler(sampler3D,noiseTexture,NoiseTextureSampler,NoiseTex)
	AddressU = WRAP;
	AddressV = WRAP;
	AddressW = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
    LOD_BIAS = 0;	
EndSampler;


struct hairCombInfo {
#if __PS3
	float4 pos            : POSITION;
#else
	float4 pos            : POSITION1;
#endif
};

vertexOutputComposite VS_Shells_Unskinned(rageSkinVertexInputBump IN, hairCombInfo COMB) 
{
	vertexOutputComposite OUT;
	
	float3 localPos = IN.pos;
	localPos += IN.normal * LayerScale;
	
	float3 worldPos = mul(float4(localPos,1), gWorld).xyz;
	OUT.hPosition = mul(float4(localPos,1.0), gWorldViewProj);
	float3 N = normalize(mul(IN.normal, (float3x3)gWorld));
	

	float3 worldEyePos = gViewInverse[3].xyz;
	
    float3 E = normalize(worldEyePos - worldPos); //eye vector
    float3 T = normalize(mul(IN.tangent.xyz, (float3x3)gWorld));

    OUT.worldTangent = T;
	OUT.worldBinormal = normalize(cross(N,T)) * 0.5f + 0.5f;
	OUT.worldNormal = N;

	OUT.texCoordDiffuse = IN.texCoord0;// * 0.5f;
	OUT.texCoordNoise = IN.texCoord1;// * 40.0f;


	// Compute the light info, store the distance to light in w component
	float4 lightDistDir = rageComputeLightData(worldPos, 0).lightPosDir;
	
	float EDotN = dot(E,N);
	float LDotN = dot(lightDistDir.xyz, N);

	OUT.vColor.xyz = 0.2f;
	OUT.vColor.w = LayerAlpha;
	OUT.worldEyePos = worldEyePos;

	OUT.worldPos = worldPos;
	
	OUT.lightDistDir0 = lightDistDir;

	return OUT;
}



vertexOutputComposite VS_Shells(rageSkinVertexInputBump IN, hairCombInfo COMB, uniform float useComb) 
{
	vertexOutputComposite OUT;
    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);

	float3 localPos = IN.pos;
	if(useComb)
		localPos += COMB.pos.xyz * LayerScale * combScale;
	else
		localPos += IN.normal * LayerScale;

	float3 pos = rageSkinTransform(localPos, boneMtx);
	OUT.hPosition = mul(float4(pos,1.0), gWorldViewProj);
	float3 N = normalize(mul(IN.normal, (float3x3)boneMtx));
	

	float3 worldEyePos = gViewInverse[3].xyz;
	
    float3 E = normalize(worldEyePos - pos); //eye vector
    float3 T = normalize(mul(IN.tangent.xyz, (float3x3)boneMtx));

    OUT.worldTangent = T;
	OUT.worldBinormal = normalize(cross(N,T)) * 0.5f + 0.5f;
	OUT.worldNormal = N;

	OUT.texCoordDiffuse = IN.texCoord0 * noiseUVScale;
	OUT.texCoordNoise = IN.texCoord1;// * 40.0f;


	// Compute the light info, store the distance to light in w component
	float4 lightDistDir = rageComputeLightData(pos, 0).lightPosDir;
	
	float EDotN = dot(E,N);
	float LDotN = dot(lightDistDir.xyz, N);

	//OUT.vColor.xyz = 1.0f;
	//wrap lighting for diffuse color
	//float wrapLightingCoef = lerp(wrapLightingAmount, 1.0f, LDotN);
	//OUT.vColor.xyz = clamp(wrapLightingCoef, 0.0f, 1.0f);
	OUT.vColor.xyz = 0.2f;
	OUT.vColor.w = LayerAlpha;
	OUT.worldEyePos = worldEyePos;
	OUT.worldPos = pos;
	
	OUT.lightDistDir0 = lightDistDir;

	return OUT;
}

float3 ShiftTangent(float3 tan, float3 norm, float3 shift)
{
	float3 shiftTan = tan + shift * norm;
	return normalize(shiftTan);
}

float StrandSpecular(float3 T, float3 H, float3 L, float exp)
{
	float v = dot(T,H);
	float v2 = v * v;

	float dotTL = dot(T, L);
	float dirAtten = smoothstep(-1.0f, 0.0f, dotTL);
	return saturate(pow(1.0f - v2, exp / 2) * dirAtten);

	//if(dot(T,L) < 0.0f)
	//	return 0.0f;
	//else
	//	return pow(1.0f - v2, exp / 2);
}



float StrandSpecular2(float3 T, float3 V, float3 L, float3 exp)
{
	float3 H = V;//normalize(L + V);
	float dotTH = dot(T, H);
	float sinTH = sqrt(1.0f - dotTH*dotTH);
	//float dirAtten = smoothstep(-1.0f, 0.0f, dotTH);
	return /*dirAtten * */pow(sinTH, exp.x).x;
}

float4 PS_Shells( vertexOutputComposite IN , uniform bool writeFullAlpha, uniform bool directColor)
{
#if 1//__XENON

	float4 texColor;
	float4 diffuseColor = tex2D( DiffuseSampler, IN.texCoordNoise );
	float3 texCoord3d = float3(IN.texCoordDiffuse.xy, VolLayer);

	float3 L = normalize(IN.lightDistDir0.xyz);
	float3 N = normalize(IN.worldNormal);
	float3 E = normalize(IN.worldEyePos - IN.worldPos);
	float3 T = normalize(IN.worldTangent);
	float3 B = normalize((IN.worldBinormal*2) - 1);
	float3 H = normalize(L + -E);

	texColor = tex3D(NoiseTextureSampler, texCoord3d);
	texColor.a *= diffuseColor.a;
	float4 color;
	float3 bumpedT;

	if(directColor)
	{
		bumpedT = N;
	}
	else
	{
		texColor.xyz = texColor.xyz * 2.0f - 1.0f;
		bumpedT = normalize((N * texColor.z) + (texColor.x * T) + (texColor.y * B));
	}

	float4 Ka = CalcAmbient(N);//gLightAmbient;
	float4 Kd = gLightColor[0]; 
	float Pd = diffuseExponent;

	float nDotL = dot(N, L);
	float selfShadow = -clamp(nDotL, -1.0f, 0.0f);

	float u = dot(bumpedT,L);
	float u2 = u * u;
	float diffuseCoef = ragePow(1.0f - u2, Pd / 2);

	diffuseCoef = saturate(diffuseCoef - selfShadow);

	float specCoef1 = StrandSpecular2(bumpedT, H, L, specExp1);
	specCoef1 = specCoef1 - selfShadow;

	color.rgb = diffuseColor.rgb;
	if(directColor)
		color.rgb *= lerp(hairColor1.xyz, hairColor2.xyz, texColor.x);
	else
		color.rgb *= hairColor1.xyz;

	color.a = texColor.a;
	color.rgb *= Ka.xyz /* * LayerLightMod */+ (Kd.xyz * diffuseCoef);// + Ks * specCoef;
	color.rgb += specColor1.xyz * specCoef1;

	if(writeFullAlpha)
		color.a = 1.0f;
	else
		color.a *= IN.vColor.w;

	return color;

#else
	return float4(0.0f, 1.0f, 1.0f, 1.0f);
#endif
}

vertexOutputComposite VS_Shells_Combed(rageSkinVertexInputBump IN, hairCombInfo COMB) 
{
	return VS_Shells(IN,COMB,useCombingData);
}

float4 PS_Shells_ff( vertexOutputComposite IN) : COLOR
{
	return PS_Shells(IN, false, false);
}

float4 PS_Shells_tf( vertexOutputComposite IN) : COLOR
{
	return PS_Shells(IN, true, false);
}

technique draw
{
    pass p0 
    {
		CullMode = CULL_BACK;
		//CullMode = NONE;
		AlphaBlendEnable = true;
		
		SrcBlend = SRCALPHA;
		DestBlend = INVSRCALPHA;

		ZWriteEnable = false;
		VertexShader = compile VERTEXSHADER VS_Shells_Unskinned();
		PixelShader  = compile PIXELSHADER PS_Shells_ff();
    }
}


#if 0
technique drawskinned
{
    pass p0 
    {
		ZWriteEnable = false;

		VertexShader = compile VERTEXSHADER VS_Shells_Combed();
		PixelShader  = compile PIXELSHADER PS_Shells_ff();
    }
}

technique shellsFinalLayer_drawskinned
{
    pass p0 
    {
		ColorWriteEnable = ALPHA;
		ZWriteEnable = false;
		
		VertexShader = compile VERTEXSHADER VS_Shells_Combed();
		PixelShader  = compile PIXELSHADER PS_Shells_tf();
    }
}
#endif






