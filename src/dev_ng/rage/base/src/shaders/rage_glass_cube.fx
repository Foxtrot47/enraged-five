//**************************************************************//
//  RAGE Glass Cube shader
//
//  David Serafim 30 Sept 2005
//**************************************************************//

#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(14);


BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
    string UIName = "Diffuse Texture";
ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
	#if 1
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

BeginSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
    string UIName = "Bump Texture";
ContinueSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
	#if 1 
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

BeginSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
    string UIName = "Environment Texture";
ContinueSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
	#if !__FXL
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

BeginSampler(samplerCUBE,refractionTexture,RefractionSampler,RefractionTex )
    string UIName = "Refraction Texture";
ContinueSampler(samplerCUBE,refractionTexture,RefractionSampler,RefractionTex )
	#if !__FXL
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;



float ReflectiveIntensity : ReflectiveIntensity
<
    string UIName = "Intensity Environment";
    float UIMin = 0.0;
    float UIMax = 3.0;
    float UIStep = 0.05;
> = 0.5;

float DiffuseIntensity : DiffuseIntensity
<
    string UIName = "Diffuse Intensity";
    float UIMin = 0.0;
    float UIMax = 1.5;
    float UIStep = 0.05;
> = 0.15;

float SpecularIntensity : SpecularIntensity
<
    string UIName = "Specular Intensity";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.5;

float SpecExponent : SpecularExp
<
    string UIName = "Specular Exponent";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 5.0;
> = 250.0;

float RefractionIndex : RefractionIndex
<
   string UIName = "Refraction Index";
   string UIWidget = "Numeric";
   float UIMin = -1.00;
   float UIMax = 1.00;
> = 1.00;



struct VS_OUTPUT 
{
   DECLARE_POSITION( Pos)
   float2 TexCoord:   TEXCOORD0;
   float3 Normal:   TEXCOORD1;
   float3 Bump:   TEXCOORD2;
   float3 View:   TEXCOORD3;
   float4 ScrPos:   TEXCOORD4;
};

#pragma dcl position normal texcoord0 tangent

VS_OUTPUT vs_main(float4 inPos: POSITION, float3 inNormal: NORMAL,
                  float2 inTxr: TEXCOORD0, float3 inTangent: TANGENT)
{
	VS_OUTPUT OUT;

	OUT.Pos = mul(inPos, gWorldViewProj);
	OUT.TexCoord = inTxr;

	float3 pos = (float3) mul(inPos, gWorld);
	float3 viewVec = (gViewInverse[3].xyz - pos);

//	float3 norm = normalize(mul(inNormal, gWorld));

	OUT.Normal = inNormal;
	OUT.View = -viewVec;
	OUT.ScrPos = OUT.Pos;

	viewVec = normalize(viewVec);

	OUT.Bump.x = dot(viewVec, inTangent);
	float3 binormal = cross(inTangent,inNormal);
	OUT.Bump.y = dot(viewVec, binormal);
	OUT.Bump.z = dot(viewVec, inNormal);

	return OUT;
}

// because our Fx->Cg translator doesn't understand functions...
const float R0 = ((1.0 - 1.14) * (1.0 - 1.14)) / ((1.0 + 1.14) * (1.0 + 1.14));
//const float R0 = pow(1.0 - 1.14, 2.0) / pow(1.0 + 1.14, 2.0);

float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	float3 view = normalize(IN.View);
	float3 normal = normalize(IN.Normal);
	float3 bumpMap = (float3) tex2D(BumpSampler, IN.TexCoord) * 2 - 1;
	float3 bump = normalize(IN.Bump);
	float bumpamount = saturate(dot(bump, bumpMap)) * RefractionIndex + 1;

	// Snell's law
	// sin(theta_i)
	float cosine = dot(view,normal);
	float sine = sqrt(1 - cosine * cosine);
	// sin(theta_r)
	float sine2 = bumpamount * sine;
	float cosine2 = sqrt(1 - sine2 * sine2);
	// avoid wrapping on TIR critical angle
	if ( sine2 > 1.0 )
	{
		cosine2 = -cosine2;
		sine2 = 2.0 - sine2;
	}

	float3 x = -normal;
	float3 y = normalize(cross(cross(-view, normal), normal));
	float3 refr = normalize(x * cosine2 + y * sine2);

	float3 refl = view - 2 * cosine2 * normal;

	float diffbump = bumpamount * 0.2 - 0.2;
	float4 diff = tex2D(DiffuseSampler,IN.TexCoord+diffbump);
	diff.a = DiffuseIntensity * (1-diff.a);		// for alpha texture where white = see thru

	// fresnel reflection
	float fresnel = max(cosine, -cosine);
	fresnel = (R0 + (1.0 - R0) * pow(1.0 - fresnel, 8));
	fresnel = saturate(fresnel*2*ReflectiveIntensity);

	float4 col = lerp(texCUBE(RefractionSampler,refr),texCUBE(EnvironmentSampler,refl),fresnel);
	col = col * (1-diff.a) + diff*diff.a;
#if 1		// output for testing only
//	col = texCUBE(RefractionSampler,refr);
//	col = float4(refr,1);
	col = float4(refl,1);
#endif

	return col;
}


//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
      ZENABLE = true;
      CULLMODE = CW;
      ALPHABLENDENABLE = true;

      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

