//
// 
// first version was a major pain to create
// because in case of cloth we have to fetch the normal all the time
//
//
//
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_samplers.fxh"

#pragma dcl position texcoord0 normal tangent0

BeginSampler(sampler2D,DiffuseTex,DiffuseSampler,DiffuseTex)
string UIName="Diffuse Texture";
ContinueSampler(sampler2D,DiffuseTex,DiffuseSampler,DiffuseTex)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;

	float specularFactor : Specular
	<
		string UIName = "Specular Falloff";
		float UIMin = 0.0;
		float UIMax = 10000.0;
		float UIStep = 0.1;
	> = 100.0;

struct VertexOutput 
{
    DECLARE_POSITION(hPosition)
    float4 Normal			: TEXCOORD0;
    float2 texcoord			: TEXCOORD1;
    float4 worldPos			: TEXCOORD2;
    float3 worldEyePos		: TEXCOORD3;
    float3 Tangent			: TEXCOORD4;
};

VertexOutput VS_Composite(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;
	
	// this is actually complete nonsense
	// but it is necessary because -only in case of cloth- we 
	// always have to fetch the normal ... so we 
	// have to pretend that we do something with it otherwise the 
	// HLSL compiler strips it off
	OUT.Normal= mul( float4(IN.normal, 1.0f), gWorld);	
	
	OUT.texcoord = 	IN.texCoord0.xy;		
   
    OUT.hPosition =  mul(float4(IN.pos, 1.0f), gWorldViewProj);
    OUT.worldPos = mul(float4(IN.pos, 1.0f), gWorld);
    
    // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - OUT.worldPos.xyz).xyz;
    
    // we need to fetch the tangent in case of cloth ... not very useful now,
    // but probably in the future for per-pixel lighting
	OUT.Tangent = (IN.tangent.xyz);

    return OUT;
}

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS_CompositeLighting( VertexOutput IN) : COLOR
{
	float4 Color = tex2D(DiffuseSampler, IN.texcoord.xy);
	
    float4 LightDir = rageComputeLightData(IN.worldPos.xyz, 0).lightPosDir;
    float3 L0 = normalize(LightDir.xyz);

	float NL = saturate(dot(L0, normalize(IN.Normal.xyz)));
	
    // self shadow terms 
    float SelfShadow = NL * 4.0;
    
    // viewer vector in world space
    float3 E = normalize(IN.worldEyePos);

    // specular highlights
    float3 R = 2 * NL * IN.Normal.xyz - L0;
    float Specular0 = saturate(pow(saturate(dot(R,E)), 25.0));
	
	return Color + SelfShadow * (Color * NL + Color * Specular0);
}


technique draw
{
    pass p0 
    {
        AlphaTestEnable = true;
        AlphaBlendEnable = true;
        CullMode = NONE;
        VertexShader = compile VERTEXSHADER VS_Composite();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}
