#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_diffuse_sampler.fxh"

struct VS_Input
{
	float3		pos	: POSITION0;
	float2		uv	: TEXCOORD0;
};

struct VS_Output
{
	DECLARE_POSITION(pos )
	float2		uv	: TEXCOORD0;
};

VS_Output VS_Transform(VS_Input IN)
{
	VS_Output OUT;
	OUT.pos = mul(float4(IN.pos, 1), gWorldViewProj);
	OUT.uv = IN.uv;
	
	return OUT;
}

float4 PS_EdgeFilter(VS_Output IN) : COLOR
{
	float3 texel = tex2D(DiffuseSampler, IN.uv).rgb;
	
	float len = length(float3(ddx(texel) + ddy(texel)));
	
	// increase edge sensitivity
	len = len * 6;   
   
	// Write out the edge value
	return float4(len, len, len, 1.0f);
}

technique draw
{
	pass p0
	{
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Transform();
		
		#if	__SHADERMODEL >= 30
        PixelShader  = compile PIXELSHADER PS_EdgeFilter();
        #else
        PixelShader = NULL;
        #endif
	}	
}

