//-----------------------------------------------------
// Skin shader for rage - adapted from pong
//
// Based on age_skin_technique2.fx2
//
// Last changes by Wolfgang F. Engel: May 25th, 2005
// 

// Get the globals
#define USE_SPECULAR 1
#define USE_NORMAL_MAP 1
#define USE_SHADOW_MAP 1
#define USE_GROUND_MAP 1


#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_ambient.fxh"
#include "../shaderlib/rage_diffuse_sampler.fxh"
#include "../shaderlib/rage_bump_sampler.fxh"
#include "../shaderlib/rage_spec_sampler.fxh"

float sweatiness : sweatiness = 0.0f;

// color that is interpolated with the light color reflected by the floor/table
float3 RoofColor = float3(0.0, 0.0, 0.0);



float useOldSubsurface
<
    string UIName = "Use Old Subsurface";
    float UIMin = 0;
    float UIMax = 1;
    float UIStep = 1;
> = 1;

float3 diffuseLightExp
<
	string UIName = "diffuse lighting exp";
    float UIMin = 0.001;
    float UIMax = 10000.0;
    float UIStep = 0.01;
> = {2.0f, 2.0f, 2.0f};

float wrapAmount
<
	string UIName = "diffuse lighting wrap";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 0.1;
> = 0.3f;

float specularExponentHi : SpecularExpHi
<
    string UIName = "Specular Exponent Hi";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 0.1;
> = 20.0;

float specularExponentLow : SpecularExpLow
<
    string UIName = "Specular Exponent Low";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 0.1;
> = 8.0;

float specularColorFactorHi : SpecularColorHi
<
    string UIName = "Specular Intensity Hi";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 0.01;
> = 0.42;

float specularColorFactorLow : SpecularColorLow
<
    string UIName = "Specular Intensity Low";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 0.01;
> = 0.21;

float SpecFresnelExp : SpecFresnelExp
<
    string UIName = "fresnel exp";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 0.1;
> = 2.0;

float fresnelLow : fresnelLow
<
    string UIName = "fresnel low val";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 0.01;
> = 0.5;

float fresnelHi : fresnelHi
<
    string UIName = "fresnel hi val";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 0.01;
> = 1.0;

float DiffuseIntensity : DiffuseIntensity
<
    string UIName = "Diffuse Intensity";
    float UIMin = 0.0;
    float UIMax = 1.5;
    float UIStep = 0.05;
> = 1.0;

float4 SubsurfColor
<
    string UIWidget = "Sub-Surface Color";
    float UIMin = 0.;
    float UIMax = 1.;
    float UIStep = 0.001;
    string UIName = "Sub-Surface Color";
> = {0.86667f, 0.6235f, 0.53333f, 1.0f};

float RollOff
<
    string UIWidget = "color";
    float UIMin = 0.0f;
    float UIMax = 1.0f;
    float UIStep = 0.01f;
    string UIName = "Rolloff Range";
> = 0.15;

BeginSampler(samplerCUBE,skinCube,skinCubeSampler,SkinCubeTexture)
    string ResourceName = "__charcubemap";
ContinueSampler(samplerCUBE,skinCube,skinCubeSampler,SkinCubeTexture)

	AddressU = CLAMP;
    AddressV = CLAMP;
    AddressW = CLAMP;

	#if !__FXL 
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

// intensity of reflections from floor/table
float InterReflectInt : InterReflectIntensity
<
	string UIName = "Reflection from floor/roof Intensity";
	float UIMin = 0.0;
	float UIMax = 1.5;
	float UIStep = 0.01;
> = 0.3;

// fall-off from light bouncing from floor/table
float FalloffRange : FalloffIntensity
<
	string UIName = "Fall-off Bouncing light from floor";
	float UIMin = 0.0;
	float UIMax = 1.5;
	float UIStep = 0.01;
> = 0.8;

BeginSampler(sampler2D,GroundTex,GroundSampler,GroundTex)
    string ResourceName = "__groundmap";
ContinueSampler(sampler2D,GroundTex,GroundSampler,GroundTex)
#if !__FXL 
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
#if __SHADERMODE >= 40
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
#else
	MIPFILTER = LINEAR;
	MINFILTER = MIN_FILTER;
	MAGFILTER = LINEAR;
#endif
	LOD_BIAS = 0;
#else
	#if MIN_FILER != Linear		//don't reset if default
#if __SHADERMODEL >= 40
		MIN_LINEAR_MAG_POINT_MIP_POINT;
#else
		MINFILTER = MIN_FILTER;
#endif
	#endif
#endif
EndSampler;



float IBLCubeBias
<
    string UIWidget = "IBL cube bias";
    float UIMin = -10.0f;
    float UIMax = 10.0f;
    float UIStep = 0.1f;
    string UIName = "IBL cube bias";
> = 0.0;

float specMapBiasHiNormal
<
	string UIWidget = "spec map bias high normal";
    float UIMin = 0.0f;
    float UIMax = 7.0f;
    float UIStep = 0.1f;
    string UIName = "spec map bias high normal";
> = 5.0;

float specMapBiasLowNormal
<
	string UIWidget = "spec map bias low normal";
    float UIMin = 0.0f;
    float UIMax = 7.0f;
    float UIStep = 0.1f;
    string UIName = "spec map bias low normal";
> = 4.1;


float specMapBiasHiSweaty
<
	string UIWidget = "spec map bias high sweaty";
    float UIMin = 0.0f;
    float UIMax = 7.0f;
    float UIStep = 0.1f;
    string UIName = "spec map bias high sweaty";
> = 4.2; //want this to be 3.3 after sweatiness interp is done

float specMapBiasLowSweaty
<
	string UIWidget = "spec map bias low sweaty";
    float UIMin = 0.0f;
    float UIMax = 7.0f;
    float UIStep = 0.1f;
    string UIName = "spec map bias low sweaty";
> = 7.0;

float specMapIntensityNormal
<
	float UIMin = 0.0f;
    float UIMax = 1.0f;
    float UIStep = 0.01f;
    string UIName = "intensity normal";
> = 0.4f;

float specMapIntensitySweaty
<
	float UIMin = 0.0f;
    float UIMax = 1.0f;
    float UIStep = 0.01f;
    string UIName = "intensity sweaty";
> = 0.45f;


float3 diffuseExp; //per color channel


struct vertexOutputComposite 
{
    DECLARE_POSITION( hPosition )
    float2 texCoordDiffuse		: TEXCOORD0;
    float4 shadowmap_pixelPos	: TEXCOORD4;
    float4 lightDistDir0		: TEXCOORD1;
    float4 lightDistDir1		: TEXCOORD2;
    float3 worldPos				: TEXCOORD7;
    float3 worldEyePos			: TEXCOORD5;
    float3 worldNormal			: TEXCOORD6;
    float3 worldTangent			: TEXCOORD3;
    float3 worldBinormal		: COLOR0;
    float4 lightDistDir2		: COLOR1;
};

struct pixelInComposite 
{
	DECLARE_POSITION_PSIN( hPosition )
    float2 texCoordDiffuse		: TEXCOORD0;
    float4 shadowmap_pixelPos	: TEXCOORD4;
    float4 lightDistDir0		: TEXCOORD1;
    float4 lightDistDir1		: TEXCOORD2;
    float3 worldPos				: TEXCOORD7;
    float3 worldEyePos			: TEXCOORD5;
    float3 worldNormal			: TEXCOORD6;
    float3 worldTangent			: TEXCOORD3;
    float3 worldBinormal		: COLOR0;
    float4 lightDistDir2		: COLOR1;
};

//------------------------------------


/* ====== VERTEX SHADERS =========== */
#if USE_OFFSETS
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN, rageSkinVertexInputBumpOffset IN2) 
#else
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN) 
#endif
{
    float3 Position, Normal, Tangent;

    vertexOutputComposite OUT = (vertexOutputComposite)0;

        Position = IN.pos.xyz;
        Normal = IN.normal.xyz;
        Tangent = IN.tangent.xyz;
    
#if USE_OFFSETS
	if (gbOffsetEnable)
	{
		Position += IN2.pos.xyz;
		Normal   += IN2.normal.xyz;
		Tangent  += IN2.tangent.xyz;
	}
#endif

    float4 Plocal = float4(Position, 1.0);
    float3x4 boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
    float3 pos = rageSkinTransform(Position, boneMtx);
    //float3 Norm = normalize(mul(float4(Normal,1), gWorld));

    OUT.worldPos = mul(float4(pos,1), gWorld).xyz;

	float4 screenPos = mul(float4(pos,1), gWorldViewProj);
    OUT.hPosition = screenPos;
    OUT.texCoordDiffuse = IN.texCoord0;

    // tangent space matrix
    OUT.worldNormal = mul(Normal, (float3x3)boneMtx);
    OUT.worldTangent = mul(Tangent, (float3x3)boneMtx);
    OUT.worldBinormal = cross(OUT.worldTangent, OUT.worldNormal);
    OUT.worldBinormal *= IN.tangent.w;
    OUT.worldNormal = normalize(OUT.worldNormal);
    OUT.worldTangent = normalize(OUT.worldTangent);
    OUT.worldBinormal = normalize(OUT.worldBinormal);
    OUT.worldBinormal = float3(1.f,0,0);
    OUT.worldNormal = float3(1.f,0,0);

    // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - pos);

    // Compute the light direction info, store the falloff in w component
    OUT.lightDistDir0 = rageComputeLightData(pos, 0).lightPosDir;
    OUT.lightDistDir1 = rageComputeLightData(pos, 1).lightPosDir;
    OUT.lightDistDir2 = rageComputeLightData(pos, 2).lightPosDir;

    OUT.shadowmap_pixelPos =  float4(OUT.worldPos, 1);

    return OUT;
}

float2 InterpSpecCoefs(float t)
{
    float2 ret;
    ret.x = lerp(specularExponentLow, specularExponentHi, t);
    ret.y = lerp(specularColorFactorLow, specularColorFactorHi, t);
    return ret;
}

float ComputeSkinSpecular(float3 lightDir, float3 E, float3 N, float specStrength)
{
    float3 L = lightDir;
    float3 H = normalize(L + E);
    float NdotH = saturate(dot(N,H));
    return pow(NdotH, specStrength);
}

float4 PS_CompositeLightingSilhouette( pixelInComposite IN ) : COLOR
{ 
	return float4(gLightAmbient.rgb,1.0);
}

float4 PS_CompositeLighting( pixelInComposite IN ) : COLOR
{
	// color map
	float4 color = tex2D( DiffuseSampler, IN.texCoordDiffuse );

	// normal map
	//float3 bumpN = 2 * tex2D(BumpSampler, IN.texCoordDiffuse.xy).xyz - 1;
	float4 bumpN = tex2D(BumpSampler, IN.texCoordDiffuse.xy) * 2 - 1;

	// reconstruct normal
	float3 Normal;
	Normal.xy = bumpN.wy;
	Normal.z = sqrt(1 - Normal.x * Normal.x - Normal.y * Normal.y);
	bumpN.xyz = Normal;

	// compute specular color
	float4 specMaskMapColor = tex2D(SpecSampler, IN.texCoordDiffuse.xy);
	float SpecularMap = specMaskMapColor.x;
	float normalInterp = specMaskMapColor.x;
	float sweatInterp = specMaskMapColor.y;
	float interp = lerp(normalInterp, sweatInterp, sweatiness);

	// compute specular power value + specular color
	float2 ic = InterpSpecCoefs(interp);
	float4 specColor = ic.yyyy;
	float specExponent = ic.x;

	// transforming N to worldspace ?
	float3 N = normalize((IN.worldNormal * bumpN.z) + (bumpN.x * IN.worldTangent) + (bumpN.y * (2*IN.worldBinormal-1)));
	//float3 NSweat = normalize((IN.worldNormal*bumpSweat.z) + (bumpSweat.x * IN.worldTangent) + (bumpSweat.y * IN.worldBinormal));

	float4 niceAmb = CalcAmbient(N);
	//float4 normalBias = float4(N.x, N.y, N.z, IBLCubeBias);
	//float4 niceAmb = texCUBElod(skinCubeSampler, normalBias);


	float3 L0 = normalize(IN.lightDistDir0.xyz);
	float3 L1 = normalize(IN.lightDistDir1.xyz);
	float3 L2 = normalize(2*IN.lightDistDir2.xyz-1);

	float NL;
	float NL1;
	float NL2;

	NL = saturate((dot(N, L0) + wrapAmount) / (1.0f + wrapAmount));
	NL1 = saturate((dot(N, L1) + wrapAmount) / (1.0f + wrapAmount));
	NL2 = saturate((dot(N, L2) + wrapAmount) / (1.0f + wrapAmount));

	float3 NL0adj = float3(NL,NL,NL);
	float3 NL1adj = float3(NL1,NL1,NL1);
	float3 NL2adj = float3(NL2,NL2,NL2);

	NL0adj = pow(NL0adj, diffuseLightExp);
	NL1adj = pow(NL1adj, diffuseLightExp);
	NL2adj = pow(NL2adj, diffuseLightExp);

	float4 NLDiff = float4(IN.lightDistDir0.w * NL0adj * gLightColor[0].rgb + 
		IN.lightDistDir1.w * NL1adj * gLightColor[1].rgb + 
		IN.lightDistDir2.w * NL2adj * gLightColor[2].rgb, 1);    


	float SubLamb = saturate(smoothstep(-RollOff, 1.0, NLDiff.x) - smoothstep(0.0, 1.0, NLDiff.x)).x;
	float4 SubSurface = SubLamb * SubsurfColor;

	float4 Diffuse = NLDiff * color;// + SubSurface;
	
	if(useOldSubsurface == 1.0f)
		Diffuse += SubSurface;

	float3 E = normalize(IN.worldEyePos);

	//
	// reflections from the floor
	//
	float2 texCoord = IN.worldPos.xz;

	// hack alert :-) ... this is hardcoded .. not good if the player can move any farther than this
	// both players can move in the range -2.5..2.5 in x and in the range -3.78..3.78 in z direction
	// player one moves in between -3.78..-1.95 in z direction
	// player two moves in between 1.95..3.78 in z direction
	texCoord.x = texCoord.x / 2.5;
	texCoord.y = texCoord.y / 3.78;

    float3 GroundColor = tex2D(GroundSampler, 0.5 * texCoord + 0.5).xyz;

	// just use a vector that points up
	float3 UpVector = float3(0.0, 1.0, 0.0);

	// compute bouncing color
	float4 BounceColor = float4(lerp(GroundColor, RoofColor, dot(UpVector, IN.worldNormal) + 0.5),1);

	// falloff for bouncing light from the floor/table
	float Falloff = 1 - dot(UpVector * FalloffRange, UpVector * FalloffRange);

	// compute bouncing light from floor/table
	float4 Bounce = 0.0f;//clamp(BounceColor * Falloff * InterReflectInt, 0.0f, 1.0f);    

	{
		float fresnel = 1.0f - dot(E, N);
		fresnel = ragePow(fresnel, SpecFresnelExp);
		fresnel = lerp(fresnelLow, fresnelHi, fresnel);
		// specular for light 1
		float light0 = ComputeSkinSpecular(IN.lightDistDir0.xyz, E, N, specExponent);

		// specular for light 2
		float light1 = ComputeSkinSpecular(IN.lightDistDir1.xyz, E, N, specExponent);

		// specular for light 3
		float light2 = ComputeSkinSpecular(2 * IN.lightDistDir2.xyz - 1, E, N, specExponent);

		float3 cubeReflect = reflect(-E, N);
		float hiVal = lerp(specMapBiasHiNormal, specMapBiasHiSweaty, sweatiness);
		float lowVal = lerp(specMapBiasLowNormal, specMapBiasLowSweaty, sweatiness);
		float4 normalBias = float4(cubeReflect, lerp(lowVal, hiVal, interp));
		float4 specMap = texCUBElod(skinCubeSampler, normalBias);
		float intensity = lerp(specMapIntensityNormal, specMapIntensitySweaty, sweatiness);
		specMap.xyz *= specMap.w * intensity;// * ShadowMapFactor;

		// specular part of the two attenuated lights
		float3 specular = (gLightColor[0].rgb * IN.lightDistDir0.w * light0) + (gLightColor[1].rgb * IN.lightDistDir1.w * light1) + (gLightColor[2].rgb * IN.lightDistDir2.w * light2) + specMap.xyz;
		specular *= specColor.rgb * fresnel;
		//specular += sweatVal + (sweatReflectColor * sweatFresnel);


		float4 light = niceAmb + Bounce + (DiffuseIntensity * Diffuse);
		float4 finalSpec = float4(specular + (specMap.xyz), 1.0f);
		float4 resColor = light + finalSpec;
		return float4(resColor.xyz, 0.5f);
    }
}


technique draw
{
	pass p0
	{
	    VertexShader = compile VERTEXSHADER VS_CompositeSkin();
		PixelShader = compile PIXELSHADER PS_CompositeLighting();
	}
}
