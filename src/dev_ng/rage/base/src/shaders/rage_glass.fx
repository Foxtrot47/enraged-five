//**************************************************************//
//  RAGE Glass shader
//
//  David Serafim 30 Sept 2005
//**************************************************************//

#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(15);


BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
    string UIName = "Diffuse Texture";
ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
	#if 1
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

BeginSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
    string UIName = "Bump Texture";
ContinueSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
	#if 1
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

BeginSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
    string UIName = "Environment Texture";
ContinueSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
	#if !__FXL
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;

BeginSampler(sampler2D,refractionTexture,RefractionSampler,RefractionTex )
    string UIName = "Refraction Texture";
ContinueSampler(sampler2D,refractionTexture,RefractionSampler,RefractionTex )
	#if 1
		AddressU  = CLAMP;        
		AddressV  = CLAMP;
		AddressW  = CLAMP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
	#endif
EndSampler;



float ReflectiveIntensity : ReflectiveIntensity
<
    string UIName = "Reflective Intensity";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.5;

float RefractionIntensity : RefractionIntensity
<
	string UIName = "Refraction Intensity";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.05;
> = 0.5;

float DiffuseIntensity : DiffuseIntensity
<
    string UIName = "Diffuse Intensity";
    float UIMin = 0.0;
    float UIMax = 1.5;
    float UIStep = 0.05;
> = 0.15;

float SpecularIntensity : SpecularIntensity
<
    string UIName = "Specular Intensity";
    float UIMin = 0.0;
    float UIMax = 3.0;
    float UIStep = 0.05;
> = 0.5;

float SpecExponent : SpecularExp
<
    string UIName = "Specular Exponent";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 5.0;
> = 250.0;

float ReflectionDistortion : ReflectionDistortion
<
	string UIName = "Reflection Distortion";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 1.00;
	float UIStep = 0.05;
> = 0.50;

float RefractionIndex : RefractionIndex
<
   string UIName = "Refraction Index";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 1.00;
	float UIStep = 0.05;
> = 1.00;

float FresnelIntensity : FresnelIntensity
<
	string UIName = "Fresnel Intensity";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 1.00;
	float UIStep = 0.05;
> = 0.50;



struct VS_OUTPUT 
{
   DECLARE_POSITION( Pos)
   float2 TexCoord:   TEXCOORD0;
   float3 worldNormal:   TEXCOORD1;
   float3 worldTangent:   TEXCOORD2;
   float3 View:   TEXCOORD3;
   float4 ScrPos:   TEXCOORD4;
   float3 worldPos:   TEXCOORD5;
   float3 worldBinormal:   TEXCOORD6;
};


VS_OUTPUT vs_main(rageVertexInputBump IN)
{
	VS_OUTPUT OUT;

	// Compute the projected position and send out the texture coordinates
	OUT.Pos = mul(float4(IN.pos,1), gWorldViewProj);
	OUT.TexCoord = IN.texCoord0;

	float3 pos = (float3) mul(float4(IN.pos,1), gWorld);
	float3 viewVec = (gViewInverse[3].xyz - pos);

	OUT.worldNormal = (float3) normalize(mul(float4(IN.normal,1), gWorld));
	OUT.View = viewVec;
	OUT.ScrPos = OUT.Pos;

	OUT.worldPos = pos;

	OUT.worldTangent = (float3) normalize(mul(IN.tangent, gWorld));
	OUT.worldBinormal = cross(OUT.worldTangent,OUT.worldNormal);

	return OUT;
}

// because our Fx->Cg translator doesn't understand functions...
const float R0 = ((1.0 - 1.14) * (1.0 - 1.14)) / ((1.0 + 1.14) * (1.0 + 1.14));
//const float R0 = pow(1.0 - 1.14, 2.0) / pow(1.0 + 1.14, 2.0);

float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	float3 view = normalize(IN.View);
	float3 normal = normalize(IN.worldNormal);
	float3 bumpN = (float3) tex2D(BumpSampler, IN.TexCoord) * 2 - 1;

	// transforming bumpN to worldspace
	bumpN = normalize(bumpN.x*IN.worldTangent + bumpN.y*IN.worldBinormal + bumpN.z*IN.worldNormal);

	float bumpamount = saturate(dot(bumpN, view));
	bumpamount *= RefractionIndex * 0.5;

	// cos(theta_i)
	float cosine = dot(view,normal);

	// refraction
	float2 refr = IN.ScrPos.xy / IN.ScrPos.w;
	refr += bumpamount * saturate(1.0/IN.ScrPos.z) + 0.0;	// z corrected refraciton
	refr.y = -refr.y;
	refr = refr * 0.5 + 0.5;

	// reflection
	float3 refl = -(view - 2 * cosine * normal);
	refl -= bumpamount * ReflectionDistortion;

	// Compute the light direction info, store the falloff in w component
	float4 LightDir0 = normalize(rageComputeLightData(IN.worldPos, 0).lightPosDir+float4(view,1));
	float4 LightDir1 = normalize(rageComputeLightData(IN.worldPos, 1).lightPosDir+float4(view,1));
	float4 LightDir2 = normalize(rageComputeLightData(IN.worldPos, 2).lightPosDir+float4(view,1));

	float3 halfvec0 = normalize((float3)LightDir0+view);
	float3 halfvec1 = normalize((float3)LightDir1+view);
	float3 halfvec2 = normalize( (float3)LightDir2+view);

	// specular highlights
	float Specular0 = saturate(pow(saturate(dot(halfvec0,bumpN)), SpecExponent));
	float Specular1 = saturate(pow(saturate(dot(halfvec1,bumpN)), SpecExponent));
	float Specular2 = saturate(pow(saturate(dot(halfvec2,bumpN)), SpecExponent));

	float4 spec = float4((LightDir0.w * Specular0 * gLightColor[0].rgb) + 
									(LightDir1.w * Specular1 * gLightColor[1].rgb) + 
									(LightDir2.w * Specular2 * gLightColor[2].rgb), 1);
	spec *= bumpamount * SpecularIntensity;

	// diffuse map
	float diffbump = bumpamount * 0.1 - 0.1;
	float4 diff = tex2D(DiffuseSampler,IN.TexCoord+diffbump);
	diff.a = DiffuseIntensity * (1-diff.a);		// for alpha texture where white = see thru

	// fresnel term
	float fresnel = max(cosine, -cosine);
	fresnel = (R0 + (1.0 - R0) * pow(1.0 - fresnel, 8));
	fresnel = saturate(fresnel*4*FresnelIntensity);

	float ratio = ReflectiveIntensity / (ReflectiveIntensity+RefractionIntensity);
	ratio = saturate(lerp(ratio,ratio*fresnel,FresnelIntensity));
	float4 col = lerp(tex2D(RefractionSampler,refr)*RefractionIntensity, texCUBE(EnvironmentSampler,refl)*ReflectiveIntensity, ratio);
	col = lerp(col,diff,diff.a) + spec;

#if 0		// output for testing only
//	col = tex2D(RefractionSampler,refr);
//	col = float4(refr,0,1);
	col = spec;
#endif
	col.a = 1;

	return col;
}


//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
      ZENABLE = true;
      CULLMODE = CW;
      ALPHABLENDENABLE = false;
//      SRCBLEND = SRCALPHA;
//      DESTBLEND = INVSRCALPHA;

      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

