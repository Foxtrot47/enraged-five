//-----------------------------------------------------
// Rage Diffuse shadow map shader
//

// Get the pong globals
#include "../shaderlib/rage_megashader.fxh"
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"

#define FOURSHADOWMAPS 1
#define ZBIAS_PER_OBJECT 1
#include "../shaderlib/rage_shadow.fxh"
#include "../shaderlib/rage_shadowmap_common.fxh"

struct VertexOutput 
{
    DECLARE_POSITION(hPosition )
    float2 texCoordDiffuse  : TEXCOORD0;
    float3 ScreenPos		: TEXCOORD1;
};


//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS_CompositeSkin(rageSkinVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);
	float3 pos = rageSkinTransform(IN.pos, boneMtx);
    OUT.hPosition = mul(float4(pos,1), gWorldViewProj);

	// screenspace position
	OUT.ScreenPos.x = (OUT.hPosition.x * 0.5f + OUT.hPosition.w * 0.5f);
	OUT.ScreenPos.y = (OUT.hPosition.w * 0.5f - OUT.hPosition.y * 0.5f);
	OUT.ScreenPos.z = OUT.hPosition.w;

    OUT.texCoordDiffuse = IN.texCoord0;
    
    return OUT;
}

VertexOutput VS_Composite(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    OUT.hPosition = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
	// screenspace position
	OUT.ScreenPos.x = (OUT.hPosition.x * 0.5f + OUT.hPosition.w * 0.5f);
	OUT.ScreenPos.y = (OUT.hPosition.w * 0.5f - OUT.hPosition.y * 0.5f);
	OUT.ScreenPos.z = OUT.hPosition.w;
	
    
    OUT.texCoordDiffuse = IN.texCoord0;

    return OUT;
}

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS_CompositeLighting( VertexOutput IN) : COLOR
{
    // color map
    float4 Color;
    float2 Tex = IN.texCoordDiffuse;
    
#if __XENON
    asm {
		tfetch2D Color, Tex, TextureSampler, MagFilter = linear, MinFilter = linear, MipFilter = linear, OffsetX = 0.0, OffsetY = 0.0
    };
#else
	Color = tex2D( TextureSampler, Tex);
#endif    
    
    // this is actually an optimization trick ... if you use tex2Dproj here, 
    // the division is done for each of the texture lookups ... so using tex2D is faster
    float4 TexCoord = 0.0f;
    TexCoord.xy = IN.ScreenPos.xy / IN.ScreenPos.z;
    
    // mip map lod level
    //TexCoord.w = 0.0f;
    
    float4 Shadow = 0.0f;
    float4 Shadow0 = 0.0f;
    float4 Shadow1 = 0.0f;
    float4 Shadow2 = 0.0f;
    float4 Shadow3 = 0.0f;
    

#if __XENON
    asm {
		tfetch2D Shadow0, TexCoord, ShadowCollectorSampler, MagFilter = linear, MinFilter = linear, MipFilter = linear, OffsetX = -1.0, OffsetY =  1.0
		tfetch2D Shadow1, TexCoord, ShadowCollectorSampler, MagFilter = linear, MinFilter = linear, MipFilter = linear, OffsetX =  1.0, OffsetY =  1.0
		tfetch2D Shadow2, TexCoord, ShadowCollectorSampler, MagFilter = linear, MinFilter = linear, MipFilter = linear, OffsetX = -1.0, OffsetY = -1.0
		tfetch2D Shadow3, TexCoord, ShadowCollectorSampler, MagFilter = linear, MinFilter = linear, MipFilter = linear, OffsetX =  1.0, OffsetY = -1.0
    };
    Shadow = (Shadow0 + Shadow1 + Shadow2 + Shadow3) * 0.25f;
/*   
#if __XENON
    asm {
		tfetch2D Shadow, TexCoord, ShadowCollectorSampler, MagFilter = linear, MinFilter = linear, MipFilter = linear, AnisoFilter = max16to1, OffsetX = 0.0, OffsetY = 0.0
    };*/
#else
	Shadow0  = tex2D( ShadowCollectorSampler, TexCoord.xy + float2( -gShadowCollectorTexelSize.x, 0 ) );
	Shadow0 += tex2D( ShadowCollectorSampler, TexCoord.xy + float2( +gShadowCollectorTexelSize.x, 0 ) );
	Shadow0 += tex2D( ShadowCollectorSampler, TexCoord.xy + float2( 0, -gShadowCollectorTexelSize.y ) );
	Shadow0 += tex2D( ShadowCollectorSampler, TexCoord.xy + float2( 0, +gShadowCollectorTexelSize.y ) );
	
    Shadow = Shadow0 * 0.25f;
#endif    

	//return Shadow;
	return float4(Color.xyz * Shadow.x, Color.w);
}


technique drawskinned
{
    pass p0 
    {
    
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        //ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = CW;
        ZWriteEnable = true;
        ZEnable = true;
         VertexShader = compile VERTEXSHADER VS_CompositeSkin();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        //ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
        CullMode = CW;
        ZWriteEnable = true;
        ZEnable = true;
         VertexShader = compile VERTEXSHADER VS_Composite();
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}
