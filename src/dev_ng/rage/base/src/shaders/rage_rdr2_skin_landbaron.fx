#define USE_NORMAL_MAP (1)
#define USE_DETAIL_MAP (1)
#define USE_ANIMATED_NORMAL_MAP (1)
#define ANIMATED_NORMAL_MAP_WRINKLE_MAP_COUNT (2)
#define ANIMATED_NORMAL_MAP_PARAM_COUNT (32)
#define DISABLE_NORMAL_OFFSETS (0)

#pragma dcl  position normal diffuse texcoord0 tangent0

#include "rage_mc4character_skin_blendshape.fx"
