// Get the age globals
#include "../shaderlib/rage_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(15);

float4 PS_NoDraw(rageVertexOutputPassThrough IN) : COLOR
{
	return float4(0.0f, 1.0f, 1.0f, 1.0f);
}

technique draw
{
	pass p0
	{
		ColorWriteEnable = 0;

		VertexShader = compile VERTEXSHADER VS_ragePassThrough();
		PixelShader = compile PIXELSHADER PS_NoDraw();
	}
}

technique drawskinned
{
	pass p0
	{
		ColorWriteEnable = 0;

		VertexShader = compile VERTEXSHADER VS_ragePassThrough();
		PixelShader = compile PIXELSHADER PS_NoDraw();
	}
}

