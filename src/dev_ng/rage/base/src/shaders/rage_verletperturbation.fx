//**************************************************************//
//  RAGE Verlet Simulation shader
//
//  David Serafim 21 Oct 2005
//**************************************************************//

#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(14);


BeginSampler(sampler2D,perturbationTexture,PerturbationSampler,PerturbationTex)
    string UIName = "Perturbation Texture";
ContinueSampler(sampler2D,perturbationTexture,PerturbationSampler,PerturbationTex)
#if !__FXL 
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
#endif
EndSampler;

BeginSampler(sampler2D,simulationTexture,SimulationSampler,SimulationTex)
    string UIName = "Simulation Texture";
ContinueSampler(sampler2D,simulationTexture,SimulationSampler,SimulationTex)
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_POINT_MAG_POINT_MIP_POINT;
	LOD_BIAS = 0;
EndSampler;



struct VS_OUTPUT
{
   DECLARE_POSITION( Pos )
   float2 TexCoord:   TEXCOORD0;
};


#pragma dcl position texcoord0

VS_OUTPUT vs_main(float4 inPos: POSITION0, float2 inTxr: TEXCOORD0)
{
	VS_OUTPUT OUT;

	OUT.Pos = float4(inPos.xy*2-1,0,1);
	OUT.TexCoord = inTxr;

	return OUT;
}


float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	float4 col = {0,0,0,1};

	// new height
	const float oneovertwo = 128.f / 255.f;
	col = tex2D(PerturbationSampler,IN.TexCoord).r * 0.5;

	return saturate(col+tex2D(SimulationSampler,IN.TexCoord).r);
}


//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
      ZWRITEENABLE = false;
      ZENABLE = false;
      CULLMODE = CW;

//      BLENDOP = ADD;
//      SRCBLEND = ONE;
//      DESTBLEND = ONE;
      ALPHABLENDENABLE = false;

      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

