//-----------------------------------------------------
// Rage Diffuse
//

// Get the pong globals
#include "../shaderlib/rage_megashader.fxh"
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"

// gDofParams[0] - Focal Plane Dist
// gDofParams[2] - Far Focus Offset
// gDofParams[3] - Far Focus Clamp
BeginConstantBufferDX10(rage_dofparams1)
float4 gDofParams : DOF_PARAMS REGISTER(c24);
float3 gDofProjData : DOF_PROJ REGISTER(c25);
EndConstantBufferDX10(rage_dofparams1)

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(4);

BeginSampler(samplerCUBE,specmapTexture,SpecSampler,SpecTex)
		string UIName="Specular Texture";
		string ResourceType = "Cube";
	ContinueSampler(samplerCUBE,specmapTexture,SpecSampler,SpecTex)
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	EndSampler;
	
BeginSampler(samplerCUBE,CubeTexture,CubeSampler, CubeTex)
		string UIName="Cube Texture";
		string ResourceType = "Cube";
	ContinueSampler(samplerCUBE,CubeTexture,CubeSampler,CubeTex)
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	EndSampler;
	

struct VertexOutput 
{
    DECLARE_POSITION( Pos)
    float4 WorldPos	  : TEXCOORD0;
    float2 Tex		  : TEXCOORD1;
    float3 Normal      : TEXCOORD2;
    float3 Tangent     : TEXCOORD3;
    float3 Binormal    : TEXCOORD4;
    float3 worldEyePos	: TEXCOORD5;
    float depth : TEXCOORD6;
};

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VSSkinned(rageSkinVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);
	float3 pos = rageSkinTransform(IN.pos, boneMtx);

    // output position value
    OUT.Pos = mul(float4(pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    float4 WorldPos = float4(pos, 1.0);
    
    // create tangent space system
	OUT.Normal = normalize(mul(IN.normal, (float3x3)boneMtx));
    OUT.Tangent = normalize(mul(IN.tangent.xyz, (float3x3)boneMtx));
    OUT.Binormal = normalize(cross(OUT.Tangent, OUT.Normal));    
    
    // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - OUT.WorldPos.xyz);
    
    // output texture coordinate
    OUT.Tex = IN.texCoord0;
    
    // compute camera depth
    OUT.depth = OUT.Pos.z;
    

    return OUT;
}

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos = mul(float4(IN.pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal = normalize(mul(IN.normal.xyz, (float3x3)gWorld));
    OUT.Tangent = normalize(mul(IN.tangent.xyz, (float3x3)gWorld));
    OUT.Binormal = normalize(cross(OUT.Tangent, OUT.Normal));    
    
    // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - OUT.WorldPos.xyz);
    
    // output texture coordinate
    OUT.Tex = IN.texCoord0;
    
    // compute camera depth
    OUT.depth = OUT.Pos.z;
    

    return OUT;
}

/*

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN) 
{
    float3 Position, Normal, Tangent;

    vertexOutputComposite OUT = (vertexOutputComposite)0;

    OUT.hPosition = mul(float4(IN.pos, 1.0), gWorldViewProj);
    OUT.texCoordDiffuse = IN.texCoord0;
        
    // compute camera depth
    OUT.depth = OUT.hPosition.z;

    return OUT;
}
*/

float ComputeDepthBlur (float depth)
{
/*
	// transform the depth value back into world space (so we can interpolate the DOF there)
	depth = gDofProjData[1]/(gDofProjData[0]- depth);

	// subtract the focal plane...
	depth = depth - gDofParams[0];

	float f=0;

	if (depth > 0)
	{
		// scale depth value between focal distance and far blur [0 to 1] range
		f = depth/gDofParams[2];
		f = saturate(f);
	}
*/
// gDofParams[0] - Focal Plane Dist
// gDofParams[2] - Far Focus Offset
// gDofParams[3] - Far Focus Clamp
	float f=0;
	/*
   if (depth < d_focus)
   {
      // scale depth value between near blur distance and focal distance to [-1, 0] range
      f = (depth - d_focus)/(d_focus - d_near);
   }
   else
   {
      // scale depth value between focal distance and far blur
      // distance to [0, 1] range
      f = (depth - d_focus)/(d_far - d_focus);

      // clamp the far blur to a maximum blurriness
      f = clamp (f, 0, far_clamp);
   }	
   */
   
   if (depth < gDofParams.x)
   {
      // scale depth value between near blur distance and focal distance to [-1, 0] range
      f = (depth - gDofParams.x)/(gDofParams.x - gDofParams.y);
   }
   else
   {
      // scale depth value between focal distance and far blur
      // distance to [0, 1] range
      f = (depth - gDofParams.x)/(gDofParams.z - gDofParams.x);

      // clamp the far blur to a maximum blurriness
      f = clamp (f, 0, gDofParams.w);
    }

	return f * 0.5 + 0.5;
}
//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS( VertexOutput IN ) : COLOR
{
    // color map
    float3 Color = tex2D( TextureSampler, IN.Tex ).xyz;
    
	float3 N = normalize(IN.Normal);

	float3 E = normalize(gViewInverse[3].xyz - IN.WorldPos.xyz); //move to vertex shader?

    float3 CubeUV = normalize( reflect( -E, N));
    float3 DiffuseCube  = texCUBE(SpecSampler, CubeUV).xyz * 0.1f;
    float3 CubeReflect  = texCUBE(CubeSampler, CubeUV).xyz * 0.02f;
    Color *= 0.10f;
    
    return float4(DiffuseCube + Color + CubeReflect, 1.0f); //ComputeDepthBlur(IN.depth));
}

technique drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;    
        VertexShader = compile VERTEXSHADER VSSkinned();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}
