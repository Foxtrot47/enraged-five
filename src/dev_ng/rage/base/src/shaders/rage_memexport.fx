#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_diffuse_sampler.fxh"
#include "../shaderlib/rage_memexport.fxh"

#if !__PS3
#include "../shaderlib/rage_skin.fxh"
#endif

struct vertexInput 
{
    float3 pos			: POSITION;
    float2 texCoord		: TEXCOORD0;
    float3 normal		: NORMAL;
};

struct skinVertexInput
{
    float3 pos			: POSITION0;
	float4 weight		: BLENDWEIGHT;
	float4 blendindices	: BLENDINDICES;
    float2 texCoord		: TEXCOORD0;
    float3 normal		: NORMAL0;
};

struct vertexOutput
 {
    DECLARE_POSITION( pos)
    float4 texCoord		: TEXCOORD0;
	float4 worldNormal	: TEXCOORD1;
	float4 worldPos		: TEXCOORD2;
	float4 lightColor	: TEXCOORD3;
};

/* bool useVertexLighting : UseVertexLighting 
<
	string UIName = "Use Vertex Lighting";
	float UIMin = 0.0f;
	float UIMax = 1.0f;
	float UIStep = 1.0f;
> = true; */

vertexOutput VS_Transform(vertexInput IN)
{
    vertexOutput OUT;
    					
	OUT.worldPos = mul(float4(IN.pos, 1), gWorld);

	float3 worldNormal = normalize(mul((float3x3)gWorld, IN.normal));
	OUT.worldNormal = float4(worldNormal, 0);
	
	float3 fLightColor = gLightAmbient.rgb;
#if	__SHADERMODEL >= 30 && !__PS3
	for( int i = 0; i < gLightCount; i++ )
	{
		fLightColor += rageComputeLightColor(OUT.worldPos.xyz, worldNormal, i);
	}
#endif
	OUT.lightColor = float4(fLightColor, 1);
   
    OUT.pos =  mul(float4(IN.pos, 1), gWorldViewProj);
    OUT.texCoord = float4(IN.texCoord, 0, 0);

	return OUT;
}

vertexOutput VS_TransformSkin(skinVertexInput IN) 
{
    vertexOutput OUT;
    
#if __PS3
	rageSkinMtx boneMtx = (rageSkinMtx)gWorld;
#else
    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);
#endif
	float3 skinnedPos = rageSkinTransform(IN.pos, boneMtx);
    float3 skinnedNrm = normalize(mul(IN.normal, (float3x3)boneMtx));
    
    float3 fLightColor = gLightAmbient.rgb;
#if	__SHADERMODEL >= 30 && !__PS3
	for( int i = 0; i < gLightCount; i++ )
	{
		fLightColor += rageComputeLightColor(skinnedPos, skinnedNrm, i);
	}
#endif
	OUT.lightColor = float4(fLightColor, 1);
    
    OUT.pos = mul(float4(skinnedPos, 1), gWorldViewProj);
    OUT.texCoord = float4(IN.texCoord, 0, 0);
    OUT.worldNormal = float4(skinnedNrm, 0);
    OUT.worldPos = mul(float4(skinnedPos, 1), gWorld);

	return OUT;
}

float4 PS_Textured(vertexOutput IN): COLOR
{
	float4 textureColor = tex2D(DiffuseSampler, IN.texCoord.xy);
	
	float3 nrm = normalize(IN.worldNormal.xyz);
	
	float3 fLightColor = IN.lightColor.xyz;
	float4 finalColor = textureColor * float4(fLightColor, 1);
	return finalColor;
}

#if __XENON
// This is the prepass export function.  This is the first pass every frame
MEM_EXPORT_PREPASS(skinVertexInput IN)
{
	// Call the normal vertex shader
	vertexOutput OUT = VS_TransformSkin(IN);
	
	// Export the normal vertex shader output
	rageMemExport5(vertIndex, OUT.pos, OUT.texCoord, OUT.worldNormal, OUT.worldPos, OUT.lightColor);
}

// Declare the funciton used for the subsequent passes.
// This macro is just a passthrough function of the vertex output parameter
MEM_EXPORT_FASTPASS(vertexOutput);

// Declare the technique using PS_Textured as the pixel shader used to draw the subsequent passes.
MEM_EXPORT_TECHNIQUE(PS_Textured);
#endif

technique draw
{
	pass p0 
	{        
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_Textured();
	}
}

technique drawskinned
{
	pass p0 
	{        
		AlphaRef = 100;
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		VertexShader = compile VERTEXSHADER VS_TransformSkin();
		PixelShader  = compile PIXELSHADER PS_Textured();
	}
}
