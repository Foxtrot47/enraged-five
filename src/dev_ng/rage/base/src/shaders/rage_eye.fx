//-----------------------------------------------------
// Eye shader
//
// Future Improvements/Bugs
// - move all vectors into tangent space so that the reference 
//   space system in the pixel shader is tangent space

#define USE_SPECULAR 1
#define USE_NORMAL_MAP
#define USE_SHADOW_MAP 1
#define USE_ENVIRONMENT_MAP2 1

// Get the pong globals
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_ambient.fxh"
#include "../shaderlib/rage_diffuse_sampler.fxh"
#include "../shaderlib/rage_bump_sampler.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(4);

float specExponent : SpecularExp
<
    string UIName = "Specular Exponent";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 5.0;
> = 250.0;

float DiffuseIntensity : DiffuseIntensity
<
    string UIName = "Diffuse Intensity";
    float UIMin = 0.0;
    float UIMax = 1.5;
    float UIStep = 0.05;
> = 0.15;

float SpecularIntensity : SpecularIntensity
<
    string UIName = "Specular Intensity";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.5;


float AmbientIntensity : AmbientIntensity
<
    string UIName = "Ambient Intensity";
    float UIMin = 0.0;
    float UIMax = 10.0;
    float UIStep = 0.05;
> = 0.8;


float ReflectiveIntensityLow
<
    string UIName = "Reflection Intensity World";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.2;

// intensity of reflections from environment
float ReflectiveIntensity2 : ReflectiveIntensity2
<
    string UIName = "Intensity Environment2";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.5;

BeginSampler(samplerCUBE,EnvironmentTex2,EnvironmentSampler2,EnvironmentTex2)
    string ResourceName = "__envcubemap2";
ContinueSampler(samplerCUBE,EnvironmentTex2,EnvironmentSampler2,EnvironmentTex2)
#if !__FXL 
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
#if __SHADERMODEL >= 40
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
#else
	MIPFILTER = LINEAR;
	MINFILTER = MIN_FILTER;
	MAGFILTER = LINEAR;
#endif
	LOD_BIAS = 0;
#else
	#if MIN_FILER != Linear		//don't reset if default
#if __SHADERMODEL >= 40
		MIN_LINEAR_MAG_POINT_MIP_POINT;
#else
		MINFILTER = MIN_FILTER;
#endif
	#endif
#endif
EndSampler;	


struct vertexOutputComposite 
{
    DECLARE_POSITION( hPosition )
    float2 texCoordDiffuse  : TEXCOORD0;
    float3 worldPos    : TEXCOORD1;
  //  float4 lightDistDir1    : TEXCOORD2;
    float4 shadowmap_pixelPos        : TEXCOORD4;
//    float3 worldPos         : TEXCOORD4;
    float3 worldEyePos      : TEXCOORD5;
    float3 worldNormal      : TEXCOORD6;
    float3 worldTangent     : TEXCOORD3;
    float3 worldBinormal    : TEXCOORD7;
    float3 Incident         : COLOR0;
};


//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
#if USE_OFFSETS 
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN, rageSkinVertexInputBumpOffset IN2, uniform bool bUseOffsets) 
#else
vertexOutputComposite VS_CompositeSkin(rageSkinVertexInputBump IN) 
#endif
{
    float3 Position, Normal, Tangent;

    vertexOutputComposite OUT = (vertexOutputComposite)0;
    
    
    Position = IN.pos;
    Normal   = IN.normal;
    Tangent  = IN.tangent.xyz;
    
#if USE_OFFSETS
    if(bUseOffsets != 0.0f)
    {
        Position += IN2.pos;
        Normal   += IN2.normal;
        Tangent  += IN2.tangent.xyz;
    }
#endif

    
    float4 Plocal = float4(Position, 1.0);
    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);
	float3 pos = rageSkinTransform(IN.pos, boneMtx);
    float3 Norm = normalize(mul(float4(Normal,1), gWorld).xyz);

    //OUT.worldPos = pos;

    OUT.hPosition = mul(float4(pos,1), gWorldViewProj);
    OUT.texCoordDiffuse = IN.texCoord0;
    
    OUT.worldPos = pos;

    // tangent space matrix
    OUT.worldNormal = normalize(mul(Normal, (float3x3)boneMtx));
    OUT.worldTangent = normalize(mul(Tangent, (float3x3)boneMtx));
    OUT.worldBinormal = normalize(cross(OUT.worldTangent, OUT.worldNormal));

    // Compute the eye vector
    OUT.worldEyePos = normalize(gViewInverse[3].xyz - pos);

    // Incident vector to fetch the cube map
    // I = 2 * N.V * N - V
    OUT.Incident = 0.5 * (2 * dot(OUT.worldNormal, OUT.worldEyePos) * 
                   OUT.worldNormal - OUT.worldEyePos) + 0.5;
                   
    OUT.shadowmap_pixelPos = OUT.hPosition;
                   

    return OUT;
}

float4 PS_CompositeLightingSilhouette( vertexOutputComposite IN ) : COLOR
{ 
	return float4(gLightAmbient.rgb,1.0);
}

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS_CompositeLighting( vertexOutputComposite IN ) : COLOR
{
    // color map
    float4 Color = tex2D( DiffuseSampler, IN.texCoordDiffuse );
    
    // normal map
    //float3 bumpN = 2 * tex2D(BumpSampler, IN.texCoordDiffuse.xy).xyz - 1;    
    float4 bumpN = tex2D(BumpSampler, IN.texCoordDiffuse.xy) * 2 - 1;

    // reconstruct normal
    float3 Normal;
    Normal.xy = bumpN.wy;
    Normal.z = sqrt(1 - Normal.x * Normal.x - Normal.y * Normal.y);
    bumpN.xyz = Normal;    
    
    // transforming N to worldspace ?
    float3 N = normalize(IN.worldNormal * bumpN.z + (bumpN.x * IN.worldTangent) + (bumpN.y * IN.worldBinormal));
    
    // Compute the light direction info, store the falloff in w component
    float4 LightDir = rageComputeLightData(IN.worldPos, 0).lightPosDir;
    float4 LightDir1 = rageComputeLightData(IN.worldPos, 1).lightPosDir;
    float4 LightDir2 = rageComputeLightData(IN.worldPos, 2).lightPosDir;

    float3 L0 = normalize(LightDir.xyz);
    float3 L1 = normalize(LightDir1.xyz);
    float3 L2 = normalize(LightDir2.xyz);

    // viewer vector in world space
    float3 E = normalize(IN.worldEyePos);
    
    // light vectors
    // should really multiply in the in the full shadow color somewhere, but our lights are just white right now anyway...
    float NL = saturate(dot(N, L0));    
    float NL1 = saturate(dot(N, L1));
    float NL2 = saturate(dot(N, L2));

    // self shadow terms 
    float SelfShadow = NL * 4.0;
    float SelfShadow1 = NL1 * 4.0;
    float SelfShadow2 = NL2 * 4.0;
   
    // specular highlights
    float3 R = 2 * NL * N - L0;
    float Specular0 = saturate(pow(saturate(dot(R,E)), specExponent));
    
    float3 R1 = 2 * NL1 * N - L1;
    float Specular1 = saturate(pow(saturate(dot(R1,E)), specExponent));
    
    float3 R2 = 2 * NL2 * N - L2;
    float Specular2 = saturate(pow(saturate(dot(R2,E)), specExponent));
    
    float4 Spec = float4(SelfShadow * (LightDir.w * Specular0 * gLightColor[0].rgb) + SelfShadow1 * (LightDir1.w * Specular1 * gLightColor[1].rgb) + SelfShadow2 * (LightDir2.w * Specular2 * gLightColor[2].rgb), 1);
    
    float4 NLDiff = float4(LightDir.w * NL * gLightColor[0].rgb + LightDir1.w * NL1 * gLightColor[1].rgb + LightDir2.w * NL2 * gLightColor[2].rgb, 1);    
    
    // reflection from cube environment map
    float4 Reflection = texCUBE(EnvironmentSampler2, (2 * IN.Incident - 1));

	float4 amb = CalcAmbient(N);

	float4 spec = (ReflectiveIntensity2 * Reflection * Reflection.w + ReflectiveIntensityLow * Reflection) * Color.a + SpecularIntensity * Spec;
	float4 diffuse = DiffuseIntensity * NLDiff + amb * AmbientIntensity;
	float4 color = Color * diffuse + spec;

    //float4 color = AmbientIntensity * Color + ReflectiveIntensity * Color.a  * Reflection + 
    //        DiffuseIntensity * Color * NLDiff + SpecularIntensity * Spec;
	color.w = 1.0f;
	return color;
}

#if USE_OFFSETS
#define OFFSET_ENABLE false
#else
#define OFFSET_ENABLE 
#endif

technique drawskinned
{
    pass p0 
    {
   		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

        VertexShader = compile VERTEXSHADER VS_CompositeSkin(OFFSET_ENABLE);
        PixelShader  = compile PIXELSHADER PS_CompositeLighting();
    }
}

technique silhouette_drawskinned
{
    pass p0 
    {
        AlphaBlendEnable = false;
        
		SrcBlend = ONE;
		DestBlend = ZERO;

        VertexShader = compile VERTEXSHADER VS_CompositeSkin(OFFSET_ENABLE);
        PixelShader  = compile PIXELSHADER PS_CompositeLightingSilhouette();
    }
}
