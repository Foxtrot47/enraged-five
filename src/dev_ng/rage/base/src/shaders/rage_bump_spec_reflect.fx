// Configure the megashder
#define USE_NORMAL_MAP
#define USE_SPECULAR
#define USE_REFLECT
#define USE_SPHERICAL_REFLECT
#define USE_DEFAULT_TECHNIQUES

#include "../shaderlib/rage_megashader.fxh"

