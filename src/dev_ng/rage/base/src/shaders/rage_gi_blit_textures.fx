#include "../../src/shaderlib/rage_common.fxh"
#include "../../src/shaderlib/rage_samplers.fxh"

struct VS_OUTPUT 
{
   DECLARE_POSITION( Pos)
   float2 TexCoord:   TEXCOORD0;
};

#pragma dcl position normal texcoord0 tangent

VS_OUTPUT vs_main(float4 inPos: POSITION, float3 inNormal: NORMAL,
                  float2 inTxr: TEXCOORD0, float3 inTangent: TANGENT)
{
	VS_OUTPUT OUT;
	OUT.Pos = inPos;
	OUT.TexCoord = inTxr;
	return OUT;
}

BeginSampler(sampler2D, ColorTexture, ColorSampler, ColorTex)
	string	UIName = "Color Texture";
	string	UvSetName = "map1";
	int		UvSetIndex = 0;
ContinueSampler(sampler2D, ColorTexture, ColorSampler, ColorTex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_POINT_MAG_POINT_MIP_POINT;
EndSampler;


float4 ps(VS_OUTPUT IN) : COLOR 
{
	float4 TextureColor = tex2D(ColorSampler,IN.TexCoord);
	return TextureColor;
}

technique draw
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps();
   }
}

// Maybe this should be it's own shader?

BeginSampler(sampler2D, Texture0, Sampler0, Tex0)
ContinueSampler(sampler2D, Texture0, Sampler0, Tex0)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MipFilter = POINT;
	MinFilter = POINT;
	MagFilter = POINT;
EndSampler;

BeginSampler(sampler2D, Texture1, Sampler1, Tex1)
ContinueSampler(sampler2D, Texture1, Sampler1, Tex1)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MipFilter = POINT;
	MinFilter = POINT;
	MagFilter = POINT;
EndSampler;

BeginSampler(sampler2D, Texture2, Sampler2, Tex2)
ContinueSampler(sampler2D, Texture2, Sampler2, Tex2)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_POINT_MAG_POINT_MIP_POINT;
EndSampler;


struct PS_OUTPUT
{
	float4 coeffsR: SV_Target0;
	float4 coeffsG: SV_Target1;
	float4 coeffsB: SV_Target2;
};

PS_OUTPUT psMRT(VS_OUTPUT IN)
{
	PS_OUTPUT OUT;
	OUT.coeffsR = tex2D(Sampler0,IN.TexCoord);
	//OUT.coeffsR.rg = IN.TexCoord;
	//OUT.coeffsR.b = 0;
	//OUT.coeffsR.a = tex2D(Sampler0,IN.TexCoord).a;
	OUT.coeffsG = tex2D(Sampler1,IN.TexCoord);
	OUT.coeffsB = tex2D(Sampler2,IN.TexCoord);
	return OUT;
}

technique drawMRT
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER psMRT();
   }
}
