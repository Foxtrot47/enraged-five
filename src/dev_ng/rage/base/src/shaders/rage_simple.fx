// This shader demonstrates the basics on how to render an unlit skinned, unskinned, or "blitted" object

//------------------------------------
#include "../shaderlib/rage_samplers.fxh"


#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"


//------------------------------------

struct vertexInput {
	// This covers the default rage vertex format (non-skinned)
    float3 pos			: POSITION;
	float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
};


struct vertexOutput {
    DECLARE_POSITION( worldPos )
    float2 texCoord0        : TEXCOORD0;
	float4 color0			: COLOR0;
};

//------------------------------------
float FPColorScale : FPColorScale = 1.0f;

// ******************************
//	DRAW METHODS
// ******************************
vertexOutput VS_Transform(vertexInput IN)
{
    vertexOutput OUT;
    // Write out final position & texture coords
    OUT.worldPos =  mul(float4(IN.pos,1), gWorldViewProj);
    OUT.color0 = IN.diffuse * FPColorScale;
    OUT.texCoord0 = IN.texCoord0;
    return OUT;
}

vertexOutput VS_TransformSkinned(rageSkinVertexInput IN)
{
    vertexOutput OUT;
    
    // Write out final position & texture coords
    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);
	OUT.worldPos = float4(rageSkinTransform(IN.pos, boneMtx), 1);
    OUT.color0 = IN.diffuse * FPColorScale;
    OUT.texCoord0 = IN.texCoord0;
    return OUT;
}


//------------------------------------
BeginSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
	string UIName = "Texture Map 0";
ContinueSampler(sampler2D,diffuseTexture,TextureSampler,DiffuseTex)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

/* ====== PIXEL SHADERS =========== */
//-----------------------------------
float4 PS_Textured( vertexOutput IN): COLOR
{
	float4 color = tex2D(TextureSampler, IN.texCoord0.xy) * IN.color0;
	return color;
}

float4 PS_Passthrough( rageVertexOutputPassThrough IN ): COLOR
{
	float4 color = tex2D(TextureSampler, IN.texCoord0.xy) * float4(IN.diffuse);
	return color;
}

//-----------------------------------

// ===============================
// Lit (default) techniques
// ===============================
technique draw
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER  VS_Transform();
        PixelShader  = compile PIXELSHADER PS_Textured();
    }
}

technique drawskinned
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER VS_TransformSkinned();
        PixelShader  = compile PIXELSHADER PS_Textured();
    }
}

technique blit_draw
{
	pass p0
	{
		VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform();
		PixelShader = compile PIXELSHADER PS_Passthrough();
	}
}
