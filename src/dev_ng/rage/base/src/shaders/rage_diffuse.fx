#pragma dcl position diffuse texcoord0 normal

// Configure the megashder
#define USE_SPECULAR
#define IGNORE_SPECULAR_MAP
#define USE_DEFAULT_TECHNIQUES

#include "../shaderlib/rage_megashader.fxh"
