#pragma strip off
#pragma dcl position diffuse specular texcoord0

#define PLAYER_COLL_RADIUS0			(0.75f)	// radius of influence
#define PLAYER_COLL_RADIUS_SQR0		(PLAYER_COLL_RADIUS0*PLAYER_COLL_RADIUS0)

#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_grass_regs.fxh"

float4	 plantColor			: plantColor0			REGISTER2(vs, GRASS_CREG_PLANTCOLOR);	// global plant color + alpha
float4	 groundColor		: groundColor0			REGISTER2(vs, GRASS_CREG_GROUNDCOLOR);	// global ground color [R | G | B | 1.0f]

BeginConstantBufferDX10(rage_campos)
float3	vecCameraPos	: vecCameraPos0 REGISTER2(vs, GRASS_CREG_CAMERAPOS)	= float3(0,0,0);	// xyz = camera pos
float3	vecPlayerPos	: vecPlayerPos0 REGISTER2(vs, GRASS_CREG_PLAYERPOS)	= float3(0,0,0);	// xyz = current player pos
EndConstantBufferDX10(rage_campos)

float4  _dimensionLOD2	: dimensionLOD20 REGISTER2(vs,GRASS_CREG_DIMENSIONLOD2)	= float4(0,0,0,0);	// xy = LOD.wh, zw=0
#define WidthLOD2		(_dimensionLOD2.x)	// LOD2: billboard width scale
#define HeightLOD2		(_dimensionLOD2.y)	// LOD2: billboard height scale

// ped collision params:
// x = coll sphere radius sqr
// y = inv of coll sphere radius sqr
float2	_vecCollParams	: vecCollParams0 REGISTER2(vs,GRASS_CREG_PLAYERCOLLPARAMS)	= float2(PLAYER_COLL_RADIUS_SQR0, 1.0f/PLAYER_COLL_RADIUS_SQR0);
#define collPedSphereRadiusSqr		(_vecCollParams.x)		// x = coll sphere radius sqr
#define collPedInvSphereRadiusSqr	(_vecCollParams.y)		// y = inv of coll sphere radius sqr

// vehicle collision params:
BeginConstantBufferDX10(rage_vehcol)
#if !(__WIN32PC && __SHADERMODEL < 40)
bool		bVehCollEnabled : bVehCollEnabled0 REGISTER(GRASS_BREG_VEHCOLLENABLED) = false;
#endif
float4		_vecVehCollB	: vecVehCollB0 REGISTER2(vs,GRASS_CREG_VEHCOLLB) = float4(0,0,0,0);	// [ segment start point B.xyz				| 0				]
float4		_vecVehCollM	: vecVehCollM0 REGISTER2(vs,GRASS_CREG_VEHCOLLM) = float4(0,0,0,0);	// [ segment vector M.xyz					| 1.0f/dot(M,M) ]
float4		_vecVehCollR	: vecVehCollR0 REGISTER2(vs,GRASS_CREG_VEHCOLLR) = float4(0,0,0,0);	// [ R	      | R*R          | 1.0f/(R*R)   | worldPosZ		]
EndConstantBufferDX10(rage_vehcol)
#define collVehVecB				(_vecVehCollB.xyz)	// start segment point B
#define collVehVecM				(_vecVehCollM.xyz)	// segment vector M
#define collVehInvDotMM			(_vecVehCollM.w)	// 1/dot(M,M)
#define collVehR				(_vecVehCollR.x)	// collision radius
#define collVehRR				(_vecVehCollR.y)	// R*R
#define collVehInvRR			(_vecVehCollR.z)	// 1/(R*R)
#define collVehGroundZ			(_vecVehCollR.w)

float4	fadeAlphaDistUmTimer	: fadeAlphaDistUmTimer0 REGISTER2(vs,GRASS_CREG_ALPHALOD0DISTUMTIMER);	// [ (f)/(f-c) | (-1.0)/(f-c) | umTimer | ? ];
#define fadeAlphaLOD0Dist0		(fadeAlphaDistUmTimer.x)	// (f)/(f-c)
#define fadeAlphaLOD0Dist1		(fadeAlphaDistUmTimer.y)	// (-1.0)/(f-c)
#define umGlobalTimer			(fadeAlphaDistUmTimer.z)	// micro-movement timer

float4	fadeAlphaLOD1Dist		: fadeAlphaLOD1Dist0	REGISTER2(vs,GRASS_CREG_ALPHALOD1DIST);	// [ (f)/(f-c) | (-1.0)/(f-c) | umTimer | ? ];
#define fadeAlpha0LOD1Dist0		(fadeAlphaLOD1Dist.x)	// (f)/(f-c)
#define fadeAlpha0LOD1Dist1		(fadeAlphaLOD1Dist.y)	// (-1.0)/(f-c)
#define fadeAlpha1LOD1Dist0		(fadeAlphaLOD1Dist.z)	// (f)/(f-c)
#define fadeAlpha1LOD1Dist1		(fadeAlphaLOD1Dist.w)	// (-1.0)/(f-c)

float4	fadeAlphaLOD2Dist		: fadeAlphaLOD2Dist0	REGISTER2(vs,GRASS_CREG_ALPHALOD2DIST);	// [ (f)/(f-c) | (-1.0)/(f-c) | umTimer | ? ];
#define fadeAlpha0LOD2Dist0		(fadeAlphaLOD2Dist.x)	// (f)/(f-c)
#define fadeAlpha0LOD2Dist1		(fadeAlphaLOD2Dist.y)	// (-1.0)/(f-c)
#define fadeAlpha1LOD2Dist0		(fadeAlphaLOD2Dist.z)	// (f)/(f-c)
#define fadeAlpha1LOD2Dist1		(fadeAlphaLOD2Dist.w)	// (-1.0)/(f-c)

// micro-movement params: [globalScaleH | globalScaleV | globalArgFreqH | globalArgFreqV ]
float4	uMovementParams			: uMovementParams0		REGISTER2(vs,GRASS_CREG_UMPARAMS)	= float4(0.05f, 0.05f, 0.2125f, 0.2125f);
#define umGlobalScaleH			(uMovementParams.x)		// micro-movement: globalScaleHorizontal
#define umGlobalScaleV			(uMovementParams.y)		// micro-movement: globalScaleVertical
#define umGlobalArgFreqH		(uMovementParams.z)		// micro-movement: globalArgFreqHorizontal
#define umGlobalArgFreqV		(uMovementParams.w)		// micro-movement: globalArgFreqVertical

float4 _fakedGrassNormal		: fakedGrassNormal0		REGISTER2(vs,GRASS_CREG_FAKEDNORMAL)
<
	int nostrip=1;
>;
#define fakedGrassNormal		(_fakedGrassNormal.xyz)

BeginSampler(sampler2D,		grassTexture,	TextureGrassSampler,	grassTexture0)
ContinueSampler(sampler2D,	grassTexture,	TextureGrassSampler,	grassTexture0)
    AddressU  = WRAP;        
    AddressV  = WRAP;
    AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;

struct vertexInputUnlit
{
	float4 pos		        : POSITION;
	float4 color0			: COLOR0;
	float4 color1			: COLOR1;
	float2 texCoord0        : TEXCOORD0;
};

struct vertexInputUnlitLOD2
{
	float2 texCoord0 : TEXCOORD0;
};

struct vertexOutputUnlit
{
    DECLARE_POSITION( pos)
	float4	color0			: TEXCOORD0;
	float4	worldNormal		: TEXCOORD1;	// worldNormal.w = UV0
	float2	texCoord0		: TEXCOORD2;
	float4	groundColor0	: TEXCOORD3;	// groundColor(RGB) + groundBlend
//	float3 collisionColor : COLOR;
};

vertexOutputUnlit VS_Transform(vertexInputUnlit IN)
{
	vertexOutputUnlit OUT = (vertexOutputUnlit)0;

    float4 vertColor	= plantColor;
	float3	vertpos		= (float3)IN.pos;
    float2	localUV0	= IN.texCoord0;

	float4 IN_Color0 = IN.color0;	// used fields: rgb 
	float4 IN_Color1 = IN.color1;	// used fields: rb

	// local micro-movements of plants:
	float umScaleH	= IN_Color0.r		* umGlobalScaleH;	// horizontal movement scale (red0:   255=max, 0=min)
	float umScaleV	= (1.0f-IN_Color0.b)* umGlobalScaleV;	// vertical movement scale	 (blue0:  0=max, 255=min)
	float umArgPhase= IN_Color0.g		* 2.0f*PI;		// phase shift               (green0: phase 0-1     )

	float3 uMovementArg			= float3(umGlobalTimer, umGlobalTimer, umGlobalTimer);
	float3 uMovementArgPhase	= float3(umArgPhase, umArgPhase, umArgPhase);
	float3 uMovementScaleXYZ	= float3(umScaleH, umScaleH, umScaleV);

	uMovementArg			*= float3(umGlobalArgFreqH, umGlobalArgFreqH, umGlobalArgFreqV);
	uMovementArg			+= uMovementArgPhase;
	float3 uMovementAdd		=  sin(uMovementArg);
	uMovementAdd			*= uMovementScaleXYZ;

	// add final micro-movement:
	vertpos.xyz				+= uMovementAdd;

	float3	worldNormal = fakedGrassNormal;		// faked normal

	// player's collision:
	if (1)
	{
		float3 vecDist3 = vertpos.xyz - vecPlayerPos.xyz;

		float distc2	= dot(vecDist3, vecDist3);
		//OUT.collisionColor = abs(distc2);
		float coll		= (collPedSphereRadiusSqr - distc2) * (collPedInvSphereRadiusSqr);
		coll = saturate(coll);

		vertpos.xy += (float2)0.5f*coll*normalize(vecDist3).xy * (IN_Color1.b);		// blue1: scale for collision (useful for flower tops, etc.)
	}

	// vehicle collision:
#if __WIN32PC && __SHADERMODEL < 40
 	if (0)
#else
	if (bVehCollEnabled && 1)
#endif
	{
		float3 B		= collVehVecB.xyz;	// vecB

		float3 M		= collVehVecM.xyz;	// vecM
		float  invDotMM = collVehInvDotMM;	// 1/dot(M,M)

		float  R		= collVehR;			// collision radius
		float  RR		= collVehRR;		// R*R
		float  invRR	= collVehInvRR;		// 1/(R*R)
		float  GroundZ	= collVehGroundZ;

		float3 P		= vertpos.xyz;

		float2 PB = P.xy-B.xy;
		float t0 = dot(M.xy, PB.xy);
		t0 *= invDotMM;
		t0 = saturate(t0);

		float2 P0 = (B.xy + t0*M.xy);	// P0 = image of P on the segment

		float2 vecDist2 = P.xy - P0.xy;
		float dist2 = dot(vecDist2, vecDist2);

		const float INNER_RADIUS = R * 0.50f; // where grass is put down
		const float INNER_RADIUS2 = INNER_RADIUS * INNER_RADIUS;

		float coll = (RR - dist2) * invRR;
		coll = saturate(coll);

#if 1
		// v1: xy + z
		vertpos.xy += 1.0f * coll * R * normalize(vecDist2).xy * (IN_Color1.b);
		vertpos.z   = lerp(vertpos.z, GroundZ, coll);
#else
		// v2: z
		vertpos.z   = lerp(vertpos.z, GroundZ, coll);
#endif

		if(dist2 < INNER_RADIUS2)
		{
			// put grass down:
			vertpos.z = GroundZ-0.25f;
		}
	}

    float3 vecDist3	= vertpos - vecCameraPos;

    // calculate alpha fading:
    float2 vecDist	= vecDist3.xy;
#if 1
	// square distance approach (fading stops too rapidly):
    float  dist2	= dot(vecDist, vecDist);
#else
	// linear distance approach:
    float  dist2	= sqrt(dot(vecDist, vecDist));
#endif
    
	/*
	   f2      dist2
	------- - -------
	 f2-c2     f2-c2
	*/
	float alphaFade = dist2 * fadeAlphaLOD0Dist1 + fadeAlphaLOD0Dist0;
	alphaFade		= min(alphaFade, 1.0f);
	alphaFade		= max(alphaFade, 0.0f);

	// final alpha:    
    float theAlpha		= vertColor.w * alphaFade;
    
    OUT.pos				= mul(float4(vertpos,1.0f),	gWorldViewProj);
    OUT.worldNormal.xyz	= worldNormal.xyz;
    OUT.worldNormal.w	= localUV0.x;
	OUT.color0			= float4(vertColor.rgb, theAlpha);
	OUT.texCoord0		= IN.texCoord0;

	float groundBlend = IN_Color1.r;		// how it should be COLOR1.r

	OUT.groundColor0	= float4(groundColor.rgb, groundBlend);

	return(OUT);
}

// simplified VS for LOD1: no umovements, collision, alphafade, etc.
vertexOutputUnlit VS_TransformLOD1(vertexInputUnlit IN)
{
	vertexOutputUnlit OUT = (vertexOutputUnlit)0;

    float4 vertColor	= plantColor;
	float3	vertpos		= (float3)IN.pos;
    float2	localUV0	= IN.texCoord0;

	float4 IN_Color0 = IN.color0;	// used fields: rgb 
	float4 IN_Color1 = IN.color1;	// used fields: rb

	float3	worldNormal = fakedGrassNormal;		// faked normal

	float3 vecDist3	= vertpos - vecCameraPos;

    // calculate alpha fading:
    float2 vecDist	= vecDist3.xy;
#if 1
	// square distance approach (fading stops too rapidly):
    float  dist2	= dot(vecDist, vecDist);
#else
	// linear distance approach:
    float  dist2	= sqrt(dot(vecDist, vecDist));
#endif
	// near alpha:
	float alphaFade0 = dist2 * fadeAlpha0LOD1Dist1 + fadeAlpha0LOD1Dist0;
	alphaFade0		= min(alphaFade0, 1.0f);
	alphaFade0		= max(alphaFade0, 0.0f);
	// far alpha:
	float alphaFade1 = dist2 * fadeAlpha1LOD1Dist1 + fadeAlpha1LOD1Dist0;
	alphaFade1		= min(alphaFade1, 1.0f);
	alphaFade1		= max(alphaFade1, 0.0f);

	float alphaFade = (1.0f-alphaFade0) * alphaFade1;

	// final alpha:    
    float theAlpha		= vertColor.w * alphaFade;

    OUT.pos				= mul(float4(vertpos,1.0f),	gWorldViewProj);
    OUT.worldNormal.xyz	= worldNormal.xyz;
    OUT.worldNormal.w	= localUV0.x;
	OUT.color0			= float4(vertColor.rgb, theAlpha);
	OUT.texCoord0		= IN.texCoord0;

	float groundBlend = min(IN_Color1.r /*+ (1.0f-alphaFade1)*/, 1.0f);

	OUT.groundColor0	= float4(groundColor.rgb, groundBlend);

    return(OUT);
}

// simplified VS for LOD2: no umovements, collision, alphafade, etc.
vertexOutputUnlit VS_TransformLOD2(vertexInputUnlitLOD2 IN)
{
	vertexOutputUnlit OUT = (vertexOutputUnlit)0;

	// reconstruct billboard vertex position from texcoord:
	float2 IN_texCoord0;
	float3 IN_pos;
	float3 IN_Color1;

	float2 uvs = IN.texCoord0.xy - float2(0.5f,0.0f); // center around x-axis only

	float radiusW = WidthLOD2;
	float radiusH = HeightLOD2;

	float3 up		= normalize( gViewInverse[0].xyz) * radiusW; 
	float3 across	= normalize(-gViewInverse[1].xyz) * radiusH;
	float3 offset	= uvs.x*up + uvs.y*across;
	offset.z += radiusH*1.0f;

	IN_pos			= offset.xyz;
	IN_texCoord0	= IN.texCoord0;
	IN_texCoord0.y	*= 0.5f; // use lower half of split texture
	IN_texCoord0.y  += 0.5f;
	IN_texCoord0.y  = min(IN_texCoord0.y, 1.0f);

	//fake: regenerate ground blending: 50% at the bottom
	IN_Color1.rgb	= float3(0.5f,0.5f,0.5f);
	IN_Color1.rgb	*= IN.texCoord0.y;

    float4 vertColor	= plantColor;
	float3	vertpos		= (float3)IN_pos;
    float2	localUV0	= IN_texCoord0;

	float3	worldNormal = fakedGrassNormal;		// faked normal

	float3 vecDist3	= vertpos - vecCameraPos;

    // calculate alpha fading:
    float2 vecDist	= vecDist3.xy;
#if 1
	// square distance approach (fading stops too rapidly):
    float  dist2	= dot(vecDist, vecDist);
#else
	// linear distance approach:
    float  dist2	= sqrt(dot(vecDist, vecDist));
#endif
	// near alpha:
	float alphaFade0 = dist2 * fadeAlpha0LOD2Dist1 + fadeAlpha0LOD2Dist0;
	alphaFade0		= min(alphaFade0, 1.0f);
	alphaFade0		= max(alphaFade0, 0.0f);
	// far alpha:
	float alphaFade1 = dist2 * fadeAlpha1LOD2Dist1 + fadeAlpha1LOD2Dist0;
	alphaFade1		= min(alphaFade1, 1.0f);
	alphaFade1		= max(alphaFade1, 0.0f);

	float alphaFade = (1.0f-alphaFade0) * alphaFade1;

	// final alpha:    
    float theAlpha		= vertColor.w * alphaFade;

    OUT.pos				= mul(float4(vertpos,1.0f),	gWorldViewProj);
    OUT.worldNormal.xyz	= worldNormal.xyz;
    OUT.worldNormal.w	= localUV0.x;
	OUT.color0			= float4(vertColor.rgb, theAlpha);
	OUT.texCoord0		= IN.texCoord0;

	float groundBlend = min(IN_Color1.r + (1.0f-alphaFade1), 1.0f);

	OUT.groundColor0	= float4(groundColor.rgb, groundBlend);

    return(OUT);
}

float4 PS_TexturedUnlit(vertexOutputUnlit IN ) : COLOR
{
//return float4(IN.collisionColor, 1);
	float4 colortex	= tex2D(TextureGrassSampler, IN.texCoord0);
	float4 color = IN.color0 * colortex;

	// modulate with ground color:
	color.rgb = lerp(color.rgb, IN.groundColor0.rgb, IN.groundColor0.w);

	return color;
}

float4 PS_Textured(vertexOutputUnlit IN) : COLOR
{
	return 0.0f.xxxx;
}

float4 PS_TexturedLOD(vertexOutputUnlit IN) : COLOR
{
	return 0.0f.xxxx;
}

struct vertexInputBlit {
    float3 pos		: POSITION;
};

struct vertexOutputBlit {
    DECLARE_POSITION( pos)
    float3 worldNormal : TEXCOORD0;
};

struct pixelInputBlit { 
	float3 worldNormal : TEXCOORD0;
};

vertexOutputBlit VS_blit(vertexInputBlit IN)
{
	vertexOutputBlit OUT;
	OUT.pos			= float4(IN.pos,1);
	OUT.worldNormal = fakedGrassNormal;		// faked normal
    return(OUT);
}

technique deferredLOD0_draw
{
	pass p0 
	{        
		VertexShader		= compile VERTEXSHADER	VS_Transform();
		PixelShader			= compile PIXELSHADER	PS_Textured();
	}
}

technique deferredLOD1_draw
{
	pass p0 
	{        
		VertexShader		= compile VERTEXSHADER	VS_TransformLOD1();
		PixelShader			= compile PIXELSHADER	PS_TexturedLOD();
	}
}

technique deferredLOD2_draw
{
	pass p0 
	{        
		VertexShader		= compile VERTEXSHADER	VS_TransformLOD2();
		PixelShader			= compile PIXELSHADER	PS_TexturedLOD();
	}
}

technique deferred_drawskinned
{
	pass p0 
	{        
		VertexShader		= compile VERTEXSHADER	VS_Transform();
		PixelShader			= compile PIXELSHADER	PS_Textured();
	}
}	

technique draw
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER	VS_Transform();
        PixelShader  = compile PIXELSHADER	PS_TexturedUnlit();
    }
}

technique unlit_draw
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER	VS_Transform();
        PixelShader  = compile PIXELSHADER	PS_TexturedUnlit();
    }
}

#pragma strip on
