// Configure the megashder
#define USE_SPECULAR
#define IGNORE_SPECULAR_MAP
#define USE_NORMAL_MAP
#define USE_DEFAULT_TECHNIQUES

#pragma dcl position diffuse texcoord0 normal tangent0

#include "../shaderlib/rage_megashader.fxh"
