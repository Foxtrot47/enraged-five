#define USE_SUBSURFACE_SCATTERING (1)
#include "../shaderlib/rage_mc4character_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(0);

#pragma dcl position normal diffuse texcoord0 tangent0

technique draw
{
	pass	p0
	{
#if __PS3
		VertexShader = compile VERTEXSHADER VS();
#else
		VertexShader = compile VERTEXSHADER VS_BlendShape();
#endif // __PS3
		PixelShader  = compile PIXELSHADER PS();
	}
}


technique	drawskinned
{
	pass	p0
	{
		VertexShader = compile VERTEXSHADER VSSkinned_BlendShape();
		PixelShader = compile PIXELSHADER PS();
	}
}

