#define USE_NORMAL_MAP (1)
#define USE_DETAIL_MAP (1)

#pragma dcl position normal diffuse texcoord0 tangent0

#include "../shaderlib/rage_mc4character_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(0);


technique draw
{
	pass	p0
	{
		VertexShader = compile VERTEXSHADER VS();
		PixelShader  = compile PIXELSHADER PS();
	}
}

technique	drawskinned
{
	pass	p0
	{
		VertexShader = compile VERTEXSHADER VSSkinned();
		PixelShader = compile PIXELSHADER PS();
	}
}
