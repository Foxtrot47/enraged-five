//**************************************************************//
//  RAGE Glass Cube shader
//
//  David Serafim 30 Sept 2005
//**************************************************************//

#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_parallax.fxh"

BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
    string UIName = "Diffuse Texture";
ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
    string UIName = "Bump Texture";
ContinueSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
EndSampler;




float AmbientIntensity : AmbientIntensity
<
    string UIName = "Ambient Intensity";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.15;

float DiffuseIntensity : DiffuseIntensity
<
    string UIName = "Diffuse Intensity";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.15;

float SpecularIntensity : SpecularIntensity
<
    string UIName = "Specular Intensity";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 0.5;

float SpecExponent : SpecularExp
<
    string UIName = "Specular Exponent";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 5.0;
> = 250.0;

float ParallaxScale : ParallaxScale
<
   string UIName = "Parallax Scale";
   float UIMin = 0.00;
   float UIMax = 0.1;
> = 1.00;

float ParallaxShadow : ParallaxShadow
<
   string UIName = "Parallax Shadow";
   float UIMin = 0.00;
   float UIMax = 1.00;
> = 1.00;


float UsePOM : UsePOM
<
   string UIName = "use POM";
   float UIMin = 0.00;
   float UIMax = 1.00;
> = 0.00;

struct VS_OUTPUT 
{
   DECLARE_POSITION( Pos)
   float2 TexCoord:   TEXCOORD0;
   float3 Normal:   TEXCOORD1;
   float3 Tangent:   TEXCOORD2;
   float3 Binormal:   TEXCOORD3;
   float3 View:   TEXCOORD4;
   float3 worldPos:   TEXCOORD5;
};


VS_OUTPUT vs_main(rageVertexInputBump IN)
{
	VS_OUTPUT OUT;

	OUT.Pos = mul(float4(IN.pos,1), gWorldViewProj);
	OUT.TexCoord = IN.texCoord0	;

	float3 pos = (float3) mul(float4(IN.pos,1), gWorld);
	float3 viewVec = normalize(gViewInverse[3].xyz - pos);

	OUT.View = viewVec;
	OUT.worldPos = pos;

	OUT.Normal = normalize(IN.normal.xyz);
	OUT.Tangent = (float3) normalize(IN.tangent.xyz);
	OUT.Binormal =  (float3) normalize(cross((float3)IN.tangent,IN.normal));
	//OUT.Tangent =  (float3) normalize(cross((float3)OUT.Binormal,OUT.Normal.xyz));
	
		
	parallaxVSOutput pInfo = CalcPOMVSInfo( OUT.Tangent.xyz, OUT.Binormal.xyz, IN.normal.xyz, pos, 0);
	// WRITE_ParallaxInfo( pInfo );

	OUT.Normal = normalize(mul(OUT.Normal, (float3x3)gWorld));
	OUT.Tangent = normalize(mul(OUT.Tangent, (float3x3)gWorld));
	OUT.Binormal = normalize(mul(OUT.Binormal, (float3x3)gWorld));

	return OUT;
}


float4 ps_main(VS_OUTPUT IN) : COLOR 
{

//return float4( dot( IN.Tangent.xyz, IN.Normal.xyz ).xxx, 1.0f);

	parallaxVSOutput pInfo;
	// READ_ParallaxInfo( pInfo );
		
	pInfo = CalcPOMVSInfo( IN.Tangent.xyz, IN.Binormal.xyz, IN.Normal.xyz, IN.worldPos, 0);
	
	//return float4( pInfo.ViewTS.xy , 0.0f, 1.0f);
	float3 view = normalize(gViewInverse[3].xyz - IN.worldPos); //normalize(IN.View);
	
	parallaxPSOutput pResult;
	parallaxPSOutput result1 = CalcPOMShadowPSInfo( IN.TexCoord.xy, pInfo.POMParallaxVec, pInfo.LightDirectionTS, BumpSampler);
	parallaxPSOutput result2 = CalcParallaxPSInfo( IN.TexCoord.xy, pInfo.ViewTS.xy, BumpSampler);
	if ( UsePOM )
		pResult = result1;
	else
		pResult = result2;
	
	/* error X4121: gradient-based operations must be moved out of flow control to prevent divergence.
	if ( UsePOM )
	{
		 pResult = CalcPOMShadowPSInfo( IN.TexCoord.xy, pInfo.POMParallaxVec, pInfo.LightDirectionTS, BumpSampler); // CalcPOMPSInfo
	}
	else
	{
		pResult = CalcParallaxPSInfo( IN.TexCoord.xy, pInfo.ViewTS.xy, BumpSampler);
	}*/
	float2 parallaxCoord = pResult.OffsetTexCoord;
	

	// color map
    float4 Color = tex2D( DiffuseSampler, parallaxCoord );
    // normal map
	float3 bumpN = normalize(tex2D(BumpSampler, parallaxCoord).xyz * 2.0 - 1.0);
	
    float3 N = normalize(IN.Normal.xyz * bumpN.zzz * 0.5 + (bumpN.xxx * IN.Tangent.xyz) + (bumpN.yyy * IN.Binormal.xyz));

//return float4( N.xyz * 0.5 + 0.5 , 1.0f );
    // Compute the light direction info, store the falloff in w component
    float4 LightDir = rageComputeLightData(IN.worldPos, 0).lightPosDir;
    float4 LightDir1 = rageComputeLightData(IN.worldPos, 1).lightPosDir;
    float4 LightDir2 = rageComputeLightData(IN.worldPos, 2).lightPosDir;

    float3 L0 = normalize(LightDir.xyz);
    float3 L1 = normalize(LightDir1.xyz);
    float3 L2 = normalize(LightDir2.xyz);

    // viewer vector in world space
    float3 E = normalize(view);
    
    // light vectors
    // should really multiply in the in the full shadow color somewhere, but our lights are just white right now anyway...
    float NL = saturate(dot(N, L0));    
    float NL1 = saturate(dot(N, L1));
    float NL2 = saturate(dot(N, L2));

    // self shadow terms 
    float SelfShadow = NL > 0.0f;
    float SelfShadow1 =  NL1  > 0.0f;
    float SelfShadow2 =  NL2 > 0.0f;
   
    // specular highlights
    float3 R = 2 * NL * N - L0;
    float Specular0 = saturate(pow(saturate(dot(R,E)), SpecExponent));
    
    float3 R1 = 2 * NL1 * N - L1;
    float Specular1 = saturate(pow(saturate(dot(R1,E)), SpecExponent));
    
    float3 R2 = 2 * NL2 * N - L2;
    float Specular2 = saturate(pow(saturate(dot(R2,E)), SpecExponent));
    
    float4 Spec = float4(SelfShadow * (LightDir.w * Specular0 * gLightColor[0].rgb) + SelfShadow1 * (LightDir1.w * Specular1 * gLightColor[1].rgb) + SelfShadow2 * (LightDir2.w * Specular2 * gLightColor[2].rgb), 1);
    
    float4 NLDiff = float4(LightDir.w * NL * gLightColor[0].rgb + LightDir1.w * NL1 * gLightColor[1].rgb + LightDir2.w * NL2 * gLightColor[2].rgb, 1);    
    
	float4 amb =0.1f;

	float shadow = saturate( pResult.ShadowVal );//saturate(height+ParallaxShadow);
	//return float4( shadow.xxx, 1.0f);
	float4 spec = SpecularIntensity * Spec * shadow;
	float4 diffuse = DiffuseIntensity * NLDiff * shadow + amb * AmbientIntensity ;
	float4 col = Color * diffuse + spec;
	
	return col;
}


//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
      ZENABLE = true;
      CULLMODE = CW;
      ALPHABLENDENABLE = false;

      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

