#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_samplers.fxh"

#if __WIN32PC 
    #if __SHADERMODEL < 20
        // Currently, all we can go down to is 1.4
        //    If we need lower, we can allow it
        #define PIXELSHADER		ps_1_4
        #define VERTEXSHADER	vs_1_1
    #elif __SHADERMODEL >= 30
		#if __SHADERMODEL >= 40
			#if __SHADERMODEL >= 41
				#if __SHADERMODEL >= 50
					#define PIXELSHADER		ps_5_0
					#define VERTEXSHADER	vs_5_0
				#else
					#define PIXELSHADER		ps_4_1
					#define VERTEXSHADER	vs_4_1
				#endif
			#else
				#define PIXELSHADER		ps_4_0
				#define VERTEXSHADER	vs_4_0
			#endif
		#else
			#define PIXELSHADER		ps_3_0
			#define VERTEXSHADER	vs_3_0
		#endif
    #else
        #define PIXELSHADER ps_2_0
        #define VERTEXSHADER vs_2_0
    #endif    // __SHADERMODEL
#elif __XENON || __PS3
    #define PIXELSHADER ps_3_0
    #define VERTEXSHADER vs_3_0
#endif

BeginSampler(samplerCUBE, cubeMap, CubeMapSampler, CubeMap)
    string UIName = "Cube Map";
ContinueSampler(samplerCUBE, cubeMap, CubeMapSampler, CubeMap)
EndSampler;

struct vertexInput 
{
    float4 pos				: POSITION;
    float3 normal 			: NORMAL;
};

struct vertexOutput  
{
   DECLARE_POSITION( pos)
   float3 worldPos:	TEXCOORD0;
   float3 normal:	TEXCOORD1;
};

vertexOutput VS_Cube( vertexInput IN ) 
{	
	vertexOutput OUT;
	OUT.pos = mul(IN.pos, gWorldViewProj);
	OUT.worldPos = (float3)mul(IN.pos, gWorld);
	OUT.normal = (float3)mul(IN.normal, (float3x3)gWorldView);

	return OUT;
}

float4 PS_Cube( vertexOutput IN ) : COLOR
{
	float4 cubeColor = texCUBE(CubeMapSampler, IN.worldPos);
	return cubeColor;
}

technique draw
{
	pass p0 
	{        
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Cube();
		PixelShader  = compile PIXELSHADER PS_Cube();
	}
}

technique drawskinned
{
	pass p0 
	{        
		VertexShader = compile VERTEXSHADER VS_Cube();
		PixelShader  = compile PIXELSHADER PS_Cube();
	}

}
