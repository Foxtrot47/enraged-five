//string description = "default breakable glass shader"; 

#include "../../../../rage/base/src/shaderlib/rage_common.fxh"
#include "../../../../rage/base/src/shaderlib/rage_samplers.fxh"
#include "../../../../rage/base/src/shaderlib/rage_skin.fxh"
#include "../../../../rage/base/src/shaderlib/rage_drawbucket.fxh"
#include "../../../../rage/base/src/shaderlib/rage_shadowmap_common.fxh"
#include "../../../../rage/base/src/shaderlib/rage_blendshadows.fxh"
#include "../../../../rage/base/src/shaderlib/rage_drawbucket.fxh"

float2 FastDirectional( float3 normal, float3 dir, float3 H , float power )  // H should be precomputed in vertex shader / offline
{
	float2 res;
	res.x = saturate( dot( normal, dir  ));
	float occ = saturate( res.x * 16.0f);
	res.y = ragePow( dot( normal, H ), power ) * occ;
	return res;
}

//------------------------------------------------------------------------------
// Artist placed textures
//------------------------------------------------------------------------------

BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
    string UIName = "Pane Diffuse";
	string MaxRGBMap = "diffuseColor";
	string MaxAMap = "opacity";
ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
	AddressU  = WRAP;  
	AddressV  = WRAP;
	AddressW  = WRAP;
	MaxAnisotropy = 4;
	MIN_ANISO_MAG_ANISO_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D,specularTexture,SpecularSampler,SpecularTex)
	string UIName = "Pane Specular Texture";
	string MaxRGBMap = "specularColor";
ContinueSampler(sampler2D,specularTexture,SpecularSampler,SpecularTex)
	AddressU = WRAP;
	AddressV = WRAP;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
EndSampler;

BeginSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
    string UIName = "Environment Texture";
	string MaxRGBMap = "reflection";
ContinueSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
	AddressU  = WRAP;   
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;

//------------------------------------------------------------------------------
// Code placed textures
//------------------------------------------------------------------------------

BeginSampler(sampler2D,decalTexture,DecalSampler,DecalTex )
ContinueSampler(sampler2D,decalTexture,DecalSampler,DecalTex )
	AddressU  = WRAP;
	AddressV  = WRAP;
	MaxAnisotropy = 4;
	MIN_ANISO_MAG_ANISO_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D,brokenDiffuseTexture,BrokenDiffuseSampler,BrokenDiffuseTex )
ContinueSampler(sampler2D,brokenDiffuseTexture,BrokenDiffuseSampler,BrokenDiffuseTex )
	AddressU  = WRAP;
	AddressV  = WRAP;
	MaxAnisotropy = 4;
	MIPFILTER = LINEAR;
	MINFILTER = LINEAR;
	MAGFILTER = ANISOTROPIC;
	LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D,brokenSpecularTexture,BrokenSpecularSampler,BrokenSpecularTex)
ContinueSampler(sampler2D,brokenSpecularTexture,BrokenSpecularSampler,BrokenSpecularTex)
	AddressU = WRAP;
	AddressV = WRAP;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
EndSampler;

BeginSampler(sampler2D,brokenBumpTexture,BrokenBumpSampler,BrokenBumpTex)
ContinueSampler(sampler2D,brokenBumpTexture,BrokenBumpSampler,BrokenBumpTex)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MaxAnisotropy = 4;
	MIN_ANISO_MAG_ANISO_MIP_LINEAR;
EndSampler;

BeginSampler(sampler2D,crackTexture,CrackSampler,CrackTex)
ContinueSampler(sampler2D,crackTexture,CrackSampler,CrackTex)
	AddressU = WRAP;
	AddressV = WRAP;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
EndSampler;

float4 BrokenColor
<
	string UIName = "BrokenColor";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = float4(61.0/255.0,66.0/255.0,67.0/255.0, 1.0);

float4 PaneColor
<
	string UIName = "PaneColor";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;
> = float4(111.0/255.0,115.0/255.0,116.0/255.0, 1.0);

// Ideally, I'd want a float3x2 here, or even a float2x2, but the shader compiler doesn't seem to support those types.
// Transform from base texture coords to crack mask texture coords, rotating and scaling as necessary
float4 CrackMatrix = float4(1.f, 0.f, 0.0f, 1.0f);
// 'w' value of 0 indicates no breakage yet
float4 CrackOffset = float4(0.f, 0.f, 0.0f, 0.0f);

float ReflectiveIntensity : ReflectiveIntensity
<
    string UIName = "Reflective Intensity";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.05;
> = 1.0;

float DiffuseIntensity : DiffuseIntensity
<
    string UIName = "Diffuse Intensity";
    float UIMin = 0.0;
    float UIMax = 100;
    float UIStep = 0.05;
> = 0.5;

float SpecularIntensity : SpecularIntensity
<
    string UIName = "Specular Intensity";
    float UIMin = 0.0;
    float UIMax = 100.0;
    float UIStep = 0.05;
> = 1.0;

float SpecExponent : SpecularExp
<
    string UIName = "Specular Exponent";
    float UIMin = 0.0;
    float UIMax = 10000.0;
    float UIStep = 5.0;
> = 50.0;

float FresnelIntensity : FresnelIntensity
<
	string UIName = "Fresnel Intensity";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 100.00;
	float UIStep = 0.05;
> = 1.00;

float FresnelExponent : FresnelExponent
<
	string UIName = "Fresnel Exponent";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 50.00;
	float UIStep = 0.05;
> = 4.00;

float OpacityIntensity : OpacityIntensity
<
   string UIName = "Opacity Intensity";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 100.00;
	float UIStep = 0.05;
> = 1.0;

float4 BrokenGlassTileScale : BrokenGlassTileScale
<
   string UIName = "Broken Glass Tile Scale";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 100.00;
	float UIStep = 0.01;
> = float4(1.0, 1.0, 0.0f, 0.0f);

// ripped out of gta_common.fxh to set draw bucket here
RAGE_DRAWBUCKET(1)

struct VS_INPUT {
    float3 pos			: POSITION;
	float4 diffuse		: COLOR0;
    float2 texCoord0	: TEXCOORD0;
    float2 texCoord1	: TEXCOORD1;
    float2 texCoord2	: TEXCOORD2;
    float3 normal		: NORMAL;
};

struct VS_OUTPUT 
{
	DECLARE_POSITION( Pos )
	float4 TexCoord:		TEXCOORD0; // zw = screen position
	float2 TexCoord1:       TEXCOORD1;
	float3 worldNormal:		TEXCOORD2;
	float3 View:			TEXCOORD3;
	float4 worldPos:		TEXCOORD4;
	float3 color:			TEXCOORD5;
	float2 decal:			TEXCOORD6;
};

//------------------------------------------------------------------------------
// Vertex shader
//------------------------------------------------------------------------------
VS_OUTPUT VSCommon(VS_INPUT IN)
{
	VS_OUTPUT OUT;

	//When a piece is displaced because of an impact, rotation/translation get applied here
	//The diffuse w is the actual index
	float3 displacePos = IN.pos.xyz;
	float3 displaceNormal = IN.normal;
	if (CrackOffset.w > 0.5)
	{
		float3x4 transform = gBoneMtx[uint(round(IN.diffuse.w*255))];
		OUT.color.xy = IN.diffuse.xy;

		// Get scale from matrix and store as fade value
		OUT.color.z = length(transform[0].xyz);
		if (0 < OUT.color.z)
		{
			// Remove scale from matrix
			transform[0].xyz /= OUT.color.z;
			transform[1].xyz /= OUT.color.z;
			transform[2].xyz /= OUT.color.z;
		}
		displacePos = rageSkinTransform(displacePos, transform);
		displaceNormal = rageSkinRotate(displaceNormal, transform);
	}
	else
	{
		// Only pass along vertex color if we have a hit location
		OUT.color = 0;
	}

	// Compute the projected position and send out the texture coordinates
	OUT.Pos = mul(float4(displacePos,1), gWorldViewProj);
	OUT.TexCoord.xy = IN.texCoord0.xy;
	OUT.TexCoord1.xy = IN.texCoord1.xy;
	OUT.decal = IN.texCoord2;
	// must ignore translation terms in normal
	OUT.worldNormal = normalize(mul(displaceNormal, (float3x3)gWorld));

	float3 pos = mul(float4(displacePos,1), gWorld).xyz;
	OUT.View = pos - gViewInverse[3].xyz;
	OUT.worldPos.xyz = pos;

	// Screen coordinates
	OUT.worldPos.w = OUT.Pos.w;
	OUT.TexCoord.z = 0.5 * (OUT.Pos.x + OUT.Pos.w); //x
	OUT.TexCoord.w = 0.5 * (-OUT.Pos.y + OUT.Pos.w); //y

	return OUT;
}

VS_OUTPUT VS_TransformLit(VS_INPUT IN)
{
	return VSCommon(IN);
}

VS_OUTPUT VS_TransformSkinnedLit(VS_INPUT IN)
{
	return VSCommon(IN);
}


// because our Fx->Cg translator doesn't understand functions...
const float R0 = ((1.0 - 1.14) * (1.0 - 1.14)) / ((1.0 + 1.14) * (1.0 + 1.14));
//const float R0 = pow(1.0 - 1.14, 2.0) / pow(1.0 + 1.14, 2.0);

//------------------------------------------------------------------------------
// Figure out our coordinates in the crack texture.
//------------------------------------------------------------------------------
float2 ComputeCrackTextureCoords(float2 in_vTexCoord)
{
	float2 crackCoord;
	crackCoord.x = in_vTexCoord.x * CrackMatrix.x + in_vTexCoord.y * CrackMatrix.y;
	crackCoord.y = in_vTexCoord.x * CrackMatrix.z + in_vTexCoord.y * CrackMatrix.w;
	crackCoord += CrackOffset.xy;
	return crackCoord;
}

//------------------------------------------------------------------------------
// Main pixel shader code with variable number of lights
//------------------------------------------------------------------------------
float4 PSCommon(VS_OUTPUT IN, int numLights)
{
	// make this an input parameter?
	float scaling = 16.0f;
	float alpha_scaling = 2.0f;

	// Compute view
	float3 view = normalize(IN.View);

	// Figure out the crack mask which is a blend between the per-vertex crack visibility
	// and an artists generated texture for this particular crack pattern.
	float2 vCrackTex = ComputeCrackTextureCoords(IN.TexCoord1);
	float crackMask = IN.color.r*tex2D(CrackSampler, vCrackTex).r;
	//float crackMask = tex2D(CrackSampler, vCrackTex).r;

	// Scale texture coordinates
	float2 vWholeTex = IN.TexCoord.xy;
	float2 vBrokenTex = BrokenGlassTileScale.xy * IN.TexCoord.xy;

	// Cracks texture is scaled differently
	if (IN.color.g > 0.5)
	{
		vBrokenTex = BrokenGlassTileScale.zw * IN.TexCoord.xy;
	}

	// Figure out a normal for the unbroken pane and the broken pane.
	// Fake a bump by distoring the normal for cracked places, we aren't 
	// computing tangent space for the peices.
	float3 vWholeNormal = normalize(IN.worldNormal);
	float3 vBrokenNormal = vWholeNormal;
	float3 vOffset = (tex2D(BrokenBumpSampler,vBrokenTex)*2-1).xyz;
	vBrokenNormal.xy += vOffset.xy;
	vBrokenNormal = normalize(vBrokenNormal);
	float3 normal = normalize(lerp(vWholeNormal, vBrokenNormal, crackMask));

	// Get base color for broken and whole pane of glass
	float4 cWhole = tex2D(DiffuseSampler,vWholeTex)*PaneColor;
	//cWhole = float4(0.0f,0.0f,0.0f,0.0f);
	float4 cBroken = tex2D(BrokenDiffuseSampler,vBrokenTex)*BrokenColor;
	float4 cDiffuse = lerp(cWhole, cBroken, crackMask);

	// Figure out specular
	float4 cSpecWhole = tex2D(SpecularSampler, vWholeTex)*PaneColor;
	float4 cSpecBroken = tex2D(BrokenSpecularSampler, vBrokenTex)*BrokenColor;
	float4 cSpecular = lerp(cSpecWhole, cSpecBroken, crackMask);

	float4 cDecal = tex2D(DecalSampler,IN.decal) * IN.color.r;
	cDiffuse = saturate(cDiffuse + cDecal);
	cSpecular = saturate(cSpecular + cDecal);

	// Cracks get some special love (in other words, only the broken textures)
	if (IN.color.g > 0.5)
	{
		cDiffuse = cBroken;
		normal = vBrokenNormal;
		cSpecular = cSpecBroken;
	}

	// Multiply the specular and diffuse colors by their respective intensities
	cDiffuse.rgb *= DiffuseIntensity;
	cSpecular.rgb *= SpecularIntensity;

	// Figure out reflection vector
	float cosine = dot(view,normal);

	// Reflection
	float3 refl = -(view - 2 * cosine * normal);

	// Sample cubemap reflections
	float3 cReflection = texCUBE(EnvironmentSampler, refl).xyz;

	float2 collectorCoord = IN.TexCoord.zw / IN.worldPos.w;
	float4 shadowTerm = 1.f; //tex2D(ShadowCollectorSampler,collectorCoord);

	// Compute lighting
//	float3 diffLight,specLight;
//	RSV_CalcLight(numLights,
//				  IN.worldPos.xyz,
//				  normal,
//				  SpecExponent,
//				  shadowTerm.xyz,
//				  diffLight,
//				  specLight);

	// Apply base lighting
	float4 OUT;
//	OUT.rgb = RSV_ApplyLights(cDiffuse.rgb,
//							  diffLight,
//							  cSpecular.rgb,
//							  specLight);

	OUT.rgba = float4(0.0f,0.0f,0.0f,0.0f);

	float3 h = normalize(normalize(gLightDir[LT_DIR].xyz)+view);
	float2 lighting = FastDirectional(normal, gLightDir[LT_DIR].xyz, h, SpecExponent);

	// not using ambient lights -- something is up with the ambient light values
	// is using alpha the right thing to do?
	OUT.rgb += cDiffuse.rgb * (lighting.x * gLightColor[LT_DIR].rgb) * scaling;// * cDiffuse.a * scaling;
	OUT.rgb += cSpecular.rgb * (lighting.y * gLightColor[LT_DIR].rgb) * scaling;// * cSpecular.a * scaling;

	// fresnel term
	float fresnel = max(cosine, -cosine);
	fresnel = (R0 + (1.0 - R0) * ragePow(1.0 - fresnel, FresnelExponent));
	fresnel = saturate(fresnel*4*FresnelIntensity);

	// Add in reflection term
	OUT.rgb += (fresnel*cReflection*ReflectiveIntensity*cSpecular.rgb) * scaling;
	//OUT.rgb += (fresnel*cReflection*ReflectiveIntensity) * scaling;

	// Alpha value from base color
	OUT.a = cDiffuse.a * OpacityIntensity;
	// Add in reflection alpha -- hack?
	OUT.a += fresnel*ReflectiveIntensity;
	if (IN.color.g > 0)
	{
		OUT.a *= IN.color.r;
	}

	OUT.a = saturate(OUT.a) * alpha_scaling * IN.color.z;

	// Done!
	return OUT;
}//ps_main

//------------------------------------------------------------------------------
// Unlit (i.e. only lit by directional light)
//------------------------------------------------------------------------------
float4 PS_Unlit(VS_OUTPUT IN) : COLOR
{
	return PSCommon(IN,0);
}

//------------------------------------------------------------------------------
// Light 2
//------------------------------------------------------------------------------

float4 PS_Lit2(VS_OUTPUT IN) : COLOR
{
	return PSCommon(IN,2);
}

//------------------------------------------------------------------------------
// Light 4
//------------------------------------------------------------------------------

float4 PS_Lit4(VS_OUTPUT IN) : COLOR
{
	return PSCommon(IN,4);
}

//------------------------------------------------------------------------------
// Light 8
//------------------------------------------------------------------------------

float4 PS_Lit8(VS_OUTPUT IN) : COLOR
{
	return PSCommon(IN,8);
}

//------------------------------------------------------------------------------
// Technique declaration
//
// The Payne lighting solution requires the following technique groups:
//  - unlit
//  - lit2
//  - lit4
//  - lit8
//
// Each technique group supports the following techniques:
//  - draw
//  - drawskinned
//
// No need to implement skinned techniques if shader is only used on non-skinned
// objects.
//------------------------------------------------------------------------------

technique unlit_draw
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = true;
        AlphaTestEnable = true;
		//ZWriteEnable = false;
        //AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformLit();
		PixelShader = compile PIXELSHADER PS_Unlit();
	}
}

technique unlit_drawskinned
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformSkinnedLit();
		PixelShader = compile PIXELSHADER PS_Unlit();
	}
}

technique draw
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = true;
        AlphaTestEnable = true;
		//ZWriteEnable = false;
        //AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformLit();
		PixelShader = compile PIXELSHADER PS_Unlit();
	}
}

technique drawskinned
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformSkinnedLit();
		PixelShader = compile PIXELSHADER PS_Unlit();
	}
}

technique lightweight0_draw
{
	pass p0 
	{      
		AlphaBlendEnable	= true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformLit();
		PixelShader = compile PIXELSHADER PS_Unlit();
	}
}

technique lightweight0_drawskinned
{
	pass p0 
	{        
		AlphaBlendEnable	= true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformSkinnedLit();
		PixelShader = compile PIXELSHADER PS_Unlit();
	}
}

technique lightweight4_draw
{
	pass p0 
	{      
		AlphaBlendEnable	= true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformLit();
		PixelShader = compile PIXELSHADER PS_Lit4();
	}
}

technique lightweight4_drawskinned
{
	pass p0 
	{        
		AlphaBlendEnable	= true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformSkinnedLit();
		PixelShader = compile PIXELSHADER PS_Lit4();
	}
}

technique lit2_draw
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformLit();
		PixelShader = compile PIXELSHADER PS_Lit2();
	}
}

technique lit2_drawskinned
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformSkinnedLit();
		PixelShader = compile PIXELSHADER PS_Lit2();
	}
}



technique lit4_draw
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformLit();
		PixelShader = compile PIXELSHADER PS_Lit4();
	}
}

technique lit4_drawskinned
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformSkinnedLit();
		PixelShader = compile PIXELSHADER PS_Lit4();
	}
}

technique lit8_draw
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformLit();
		PixelShader = compile PIXELSHADER PS_Lit8();
	}
}

technique lit8_drawskinned
{
	pass p0
	{
		AlphaBlendEnable = true;
		SrcBlend            = SrcAlpha;
		DestBlend           = InvSrcAlpha;
		BlendOp             = Add;
		ZWriteEnable = false;
        AlphaTestEnable = false;
		VertexShader = compile VERTEXSHADER VS_TransformSkinnedLit();
		PixelShader = compile PIXELSHADER PS_Lit8();
	}
}
