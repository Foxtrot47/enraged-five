#include "../../src/shaderlib/rage_common.fxh"
#include "../../src/shaderlib/rage_samplers.fxh"
#include "../../src/shaderlib/rage_gi.fxh"

// Trying to get point drawing to work properly, but leaving old method working
// or at least that's the theory.
//#define USE_BLIT_FOR_SH_CONVERSION

struct VS_OUTPUT 
{
   DECLARE_POSITION( Pos)
   float2 TexCoord:   TEXCOORD0;
};


#pragma dcl position normal texcoord0 tangent

//------------------------------------------------------------------------------
// Main vertex shader
//------------------------------------------------------------------------------
VS_OUTPUT vs_main(float4 inPos: POSITION, float3 inNormal: NORMAL,
                  float2 inTxr: TEXCOORD0, float3 inTangent: TANGENT)
{
	VS_OUTPUT OUT;
#ifdef USE_BLIT_FOR_SH_CONVERSION
	OUT.Pos = inPos;
#else
	OUT.Pos = inPos;
	OUT.Pos.xy = (inPos.xy/float2(samplesWidth, samplesHeight*samplesDepth));
	OUT.Pos.y = 1-OUT.Pos.y;
	OUT.Pos.xy = OUT.Pos.xy*2-1;
#endif
	OUT.TexCoord = inTxr;
	return OUT;
}//vs_main

BeginSampler(sampler2D, ProbeTexture, ProbeSampler, ProbeTex)
	string	UIName = "Probe Texture";
ContinueSampler(sampler2D, ProbeTexture, ProbeSampler, ProbeTex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_POINT_MAG_POINT_MIP_POINT;
EndSampler;

BeginSampler(sampler2D, SHTexture, SHSampler, SHTex)
	string	UIName = "SH Texture";
ContinueSampler(sampler2D, SHTexture, SHSampler, SHTex)
	AddressU  = CLAMP;
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_POINT_MAG_POINT_MIP_POINT;
EndSampler;

//------------------------------------------------------------------------------
// Get the color from a probe. Takes the cube face, the pre-computed offset
// (use GetCubeFaceOffset) and the 0-N texture coordinates of the face where
// N is in pixels
//------------------------------------------------------------------------------
float3 SHGetProbeColor(float face, float2 offset, float2 tx)
{
	// We might also need to flip some coordinates, but I'm not sure yet.

	// Convert 0-1 tex coords per face to 0-1 for texture
	float2 texSize = {texWidth, texHeight};
	float2 texC = tx/texSize;

	// Read the texture
	float3 oColor = tex2D(ProbeSampler, texC + offset).xyz;
	return oColor;
}//SHGetProbeColor

struct PS_OUTPUT
{
	float4 coeffsR: SV_Target0;
	float4 coeffsG: SV_Target1;
	float4 coeffsB: SV_Target2;
};

//------------------------------------------------------------------------------
// Main pixel shader
//------------------------------------------------------------------------------
PS_OUTPUT ps(VS_OUTPUT IN)
{
	float4 c0 = 0;
	float4 c1 = 0;
	float4 c2 = 0;

#ifdef USE_BLIT_FOR_SH_CONVERSION
	// First convert texture coords into which probe sample we care about.
	// Since I'm not using 3D textures at the moment, this takes some math.
	// Our texture coordinates correspond to a single probe location and the probes
	// are laid out in a 2D texture x, y*z.
	//@TODO - I should really just draw points and only update the few spherical harmonics
	//        that have changed. In that case I'd need to work from a position instead.
	float3 pos = 0;
	float tmpX = floor(IN.TexCoord.x * samplesWidth); // convert from 0-1
    pos.x = tmpX;
	float tmpY =  IN.TexCoord.y * samplesHeight * samplesDepth; // convert from 0-1
	pos.y = ((tmpY+0.00001) % samplesHeight);
	pos.z = (int)(floor(tmpY / (float)(samplesHeight)));
	pos = giConvertFromSampleToPostion(pos);
#endif

	// I should probably make cubeSize constant too
	float2 texSize;
	texSize.xy = 1.0f/(float)(cubeSize);
	for (int i = 0; i < 6; i++)
	{
		// Get our 2D tex coord offset into the flattened 2D cube texture
#ifdef USE_BLIT_FOR_SH_CONVERSION
		float2 offset = giGetCubeFaceOffset(pos, (float)(i));
#else
		float2 offset = IN.TexCoord;
#endif

		for (int y = 0; y < /*cubeSize*/8; y++)
		{
			for (int x = 0; x < /*cubeSize*/8; x++)
			{
				// Compute tex coords 0-1
				float2 tx;
				tx.x = x;
				tx.y = y;
				
				// Some faces are inverted so we need to pass through a wrapper
#ifdef USE_BLIT_FOR_SH_CONVERSION
				float3 l = SHGetProbeColor((float)(i), offset, tx);
#else
				float2 nOffset = offset;
				nOffset.y += ((i*cubeSize)/(texHeight));
				float3 l = SHGetProbeColor((float)(i), nOffset, tx);
#endif

				// Use 2D flattened cubmap that matches the probes texture
				float2 tex = giConvertSHCubeCoord(tx.xy, (float)(i));
				float4 ya = tex2D(SHSampler, tex);

				c0 += l.r * ya;
				c1 += l.g * ya;
				c2 += l.b * ya;
			}
		}
	}

	const float nf = (1.0 / (cubeSize * cubeSize));

	PS_OUTPUT OUT;
	OUT.coeffsR = c0 * nf;
	OUT.coeffsG = c1 * nf;
	OUT.coeffsB = c2 * nf;
	/*
	OUT.coeffsR.rgb = pos;
	OUT.coeffsR.a = 1;
	OUT.coeffsG.rgb = pos;
	OUT.coeffsG.a = 1;
	OUT.coeffsB.rgb = pos;
	OUT.coeffsB.a = 1;
	*/
	/*
	OUT.coeffsR.rg = IN.TexCoord.xy;
	OUT.coeffsR.ba = 1;
	OUT.coeffsG.rgb = pos;
	OUT.coeffsG.a = 1;
	float3 nPos = saturate((pos - bbMin) / (bbMax - bbMin));
	OUT.coeffsB.rgb = nPos;
	OUT.coeffsB.a = 1;
	*/
	/*
	OUT.coeffsR.rg = IN.TexCoord.xy;
	OUT.coeffsR.ba = 1;
	OUT.coeffsG.rg = IN.TexCoord.xy*float2(texWidth, texHeight);
	OUT.coeffsG.ba = 1;

	float2 nOffset = IN.TexCoord.xy;
	nOffset.y += ((2*cubeSize)/(texHeight));
	OUT.coeffsB.rg = nOffset*float2(texWidth, texHeight);
	OUT.coeffsB.ba = 1;
	*/	
	return OUT;
}//ps

//------------------------------------------------------------------------------
// Main technique
//------------------------------------------------------------------------------
technique draw
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps();
   }
}

