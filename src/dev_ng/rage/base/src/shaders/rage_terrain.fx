// This shader demonstrates the basics on how to render an unlit skinned, unskinned, or "blitted" object

//------------------------------------
#include "../shaderlib/rage_samplers.fxh"


#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"


//------------------------------------

struct vertexInput {
	// This covers the default rage vertex format (non-skinned)
    float4 pos			: POSITION;
    float4 diffuse		: TEXCOORD0;
};


struct vertexOutput {
	DECLARE_POSITION(worldPos)
    float4 color0			: COLOR0;
};


// ******************************
//	DRAW METHODS
// ******************************
vertexOutput VS_Transform(vertexInput IN)
{
    vertexOutput OUT;
    // Write out final position & texture coords
    OUT.worldPos =  mul(float4(IN.pos.xyz,1), gWorldViewProj);
    OUT.color0 = IN.diffuse;
    return OUT;
}
float4 PS_Textured( vertexOutput IN): COLOR
{
	float4 color = IN.color0;
	return color;
}
//-----------------------------------
// ===============================
// Lit (default) techniques
// ===============================
technique draw
{
    pass p0 
    {        
        VertexShader = compile VERTEXSHADER  VS_Transform();
        PixelShader  = compile PIXELSHADER PS_Textured();
    }
}
//
technique unlit_draw
{  
	pass p0 
    {        
        VertexShader = compile VERTEXSHADER  VS_Transform();
        PixelShader  = compile PIXELSHADER PS_Textured();
    }
}
