#define NO_SAMPLERS
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_skin.fxh"
#include "../shaderlib/rage_samplers.fxh"


#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(6);


float4x4 lightProjMtx : LightMtx0;				// Projection matrix used for the shadow casting light


struct vertexOutputComposite {
    DECLARE_POSITION( hPosition)
    float2 texCoordDiffuse	: TEXCOORD0;
	float4 lightDistDir0	: TEXCOORD1;
	float4 lightDistDir1	: TEXCOORD2;
	float4 vColor			: TEXCOORD7;
	
	float3 worldPos			: TEXCOORD4;
	float3 worldEyePos		: TEXCOORD5;
	float3 worldNormal		: TEXCOORD6;
	float3 worldTangent		: TEXCOORD3;
	float3 worldBinormal	: COLOR1;
};


float finSizeMod <
	float UIMin = -1.0;
    float UIMax = 1.0;
    float UIStep = 0.001;
	string UIWidget = "fin size mod";
    string UIName =  "fin size mod";
> = 0.0f;

float4 hairColor <
	float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
	string UIWidget = "hair color";
    string UIName =  "hair color";
> = {0.0f, 0.0f, 0.0f, 1.0f};


float primaryShift <
	float UIMin = -10.0;
    float UIMax = 10.0;
    float UIStep = 0.01;
	string UIWidget = "primaryShift";
    string UIName =  "primaryShift";
> = 0.1f;

float secondaryShift <
	float UIMin = -10.0;
    float UIMax = 10.0;
    float UIStep = 0.01;
	string UIWidget = "secondaryShift";
    string UIName =  "secondaryShift";
> = 0.2f;

float fresnelExp <
	float UIMin = 0.0;
    float UIMax = 100.0;
    float UIStep = 0.1;
	string UIWidget = "fresnel exp";
    string UIName =  "fresnel exp";
> = 1.0f;

float fresnelMin <
	float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
	string UIWidget = "fresnel min";
    string UIName =  "fresnel min";
> = 0.0f;

float fresnelMax <
	float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
	string UIWidget = "fresnel max";
    string UIName =  "fresnel max";
> = 1.0f;


float diffuseExponent
<
	string UIWidget = "diffuse exponent";
	float UIMin = 0.0;
	float UIMax = 10000.0;
	float UIStep = 0.1;
    string UIName =  "diffuse exponent";
> = 1.0f;

float4 diffuseColor <
	string UIWidget = "diffuseColor";
    string UIName =  "diffuseColor";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
> = {0.15f, 0.15f, 0.15f, 1.0f};

float4 specColor1 <
	string UIWidget = "specularColor1";
    string UIName =  "specularColor1";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
> = {1.0f, 1.0f, 1.0f, 1.0f};

float4 speColor2 <
	string UIWidget = "specularColor2";
    string UIName =  "specularColor2";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.01;
> = {1.0f, 0.2f, 0.2f, 1.0f};

float specExp1 <
	float UIMin = 0.0;
    float UIMax = 1000.0;
    float UIStep = 0.25;
	string UIWidget = "specExp1";
    string UIName =  "specExp1";
> = 25.0f;

float specExp2 <
	float UIMin = 0.0;
    float UIMax = 1000.0;
    float UIStep = 0.25;
	string UIWidget = "specExp2";
    string UIName =  "specExp2";
> = 25.0f;



DEFINE_SAMPLER(diffuseTexture,DiffuseTex,FinsDiffuseSampler,"Fins Diffuse Texture","diffusemap");


float CalcFresnelTerm(float3 N, float3 E, float exp, float low, float hi)
{
	float fresnel = clamp(abs(dot(E, N)), 0.0f, 1.0f);
	fresnel = pow(fresnel, exp);
	fresnel = lerp(low, hi, fresnel);
	return fresnel;
}

vertexOutputComposite VS_Fins_Unskinned(rageSkinVertexInputBump IN) 
{
	vertexOutputComposite OUT;
	float4 Plocal = float4(IN.pos.xyz, 1.0);
	
	float4 localBinorm = float4(normalize(cross(IN.tangent.xyz, IN.normal)), 1.0f);
	
	Plocal = Plocal + (localBinorm * finSizeMod * (1.0f - IN.texCoord0.y));
	float4 worldPos = mul(Plocal, gWorld);
	OUT.hPosition = mul(Plocal, gWorldViewProj);
	OUT.texCoordDiffuse = IN.texCoord0;
	OUT.texCoordDiffuse.x = IN.texCoord0.x;

	// Compute the eye vector
	float3 worldEyePos = gViewInverse[3].xyz;

	float3 N = normalize(mul(IN.normal, (float3x3)gWorld));
    float3 E = normalize(worldEyePos - worldPos.xyz); //eye vector
    float3 T = normalize(mul(IN.tangent.xyz, (float3x3)gWorld));

    OUT.worldTangent = T;
	OUT.worldBinormal = normalize(cross(N,T));
	OUT.worldNormal = N;

	float alpha = CalcFresnelTerm(N, E, fresnelExp, fresnelMin, fresnelMax);

	OUT.vColor.xyz = 1.0f;
	OUT.vColor.w = alpha; //max(0, 2 * abs(EDotN)-1);

	OUT.worldEyePos = worldEyePos;

	OUT.worldPos = worldPos.xyz;

	// Compute the light info, store the distance to light in w component
	OUT.lightDistDir0 = rageComputeLightData(worldPos.xyz, 0).lightPosDir;
	OUT.lightDistDir1 = rageComputeLightData(worldPos.xyz, 1).lightPosDir;

	return OUT;
}


vertexOutputComposite VS_Fins(rageSkinVertexInputBump IN) 
{
	vertexOutputComposite OUT;
	float4 Plocal = float4(IN.pos.xyz, 1.0);
    rageSkinMtx boneMtx = ComputeSkinMtx(IN.blendindices, IN.weight);
	float3 pos = rageSkinTransform(IN.pos, boneMtx);

	OUT.hPosition = mul(float4(pos,1.0), gWorldViewProj);
	OUT.texCoordDiffuse = IN.texCoord0;

	// Compute the eye vector
	float3 worldEyePos = gViewInverse[3].xyz;

	float3 N = normalize(mul(IN.normal, (float3x3)boneMtx));
    float3 E = normalize(worldEyePos - pos); //eye vector
    float3 T = normalize(mul(IN.tangent.xyz, (float3x3)boneMtx));

    OUT.worldTangent = T;
	OUT.worldBinormal = normalize(cross(N,T));
	OUT.worldNormal = N;


	//float3 E = normalize(worldEyePos - pos);
	float EDotN = dot(E,N);

	OUT.vColor.xyz = 1.0f;

	OUT.vColor.w = CalcFresnelTerm(N, E, fresnelExp, fresnelMin, fresnelMax);

	OUT.worldEyePos = worldEyePos;

	OUT.worldPos = pos;

	// Compute the light info, store the distance to light in w component
	OUT.lightDistDir0 = rageComputeLightData(pos, 0).lightPosDir;
	OUT.lightDistDir1 = rageComputeLightData(pos, 1).lightPosDir;

	return OUT;
}

float StrandSpecular(float3 T, float3 H, float exp)
{
	float v = dot(T,H);
	float v2 = v * v;
	return ragePow(1.0f - v2, exp / 2);
}


float4 PS_Fins( vertexOutputComposite IN, uniform bool writeFullAlpha )
{
	float3 L = IN.lightDistDir0.xyz;
	float3 N = normalize(IN.worldNormal);
	float3 E = normalize(IN.worldEyePos - IN.worldPos);
	float3 T = normalize(IN.worldTangent);
	float3 B = normalize(IN.worldBinormal);
	float3 H = normalize(L + E);

	float4 Ka = float4(0.25f, 0.25f, 0.25f, 1.0f);
	float4 Kd = float4(0.5f, 0.5f, 0.5f, 1.0f);
	float Pd = diffuseExponent;

	float u = abs(dot(B,L));

	// This negation is to work around an HLSL compiler bug in final hardware
	float u2 = -u * u;
	float diffuseCoef = ragePow(1.0f + u2, Pd * 0.5);

	float specCoef1 = StrandSpecular(B, H, specExp1);

	float4 color = tex2D( FinsDiffuseSampler, IN.texCoordDiffuse );
	color.xyz = hairColor.xyz;
	color.rgb *= Ka.rgb + (Kd.rgb * diffuseCoef);
	color.rgb += specColor1.rgb * specCoef1;

	if(writeFullAlpha)
		color.w = 1.0f;
	else
		color.w *= IN.vColor.w;
	return color;
}

float4 PS_Fins_false( vertexOutputComposite IN ) : COLOR
{
	return PS_Fins(IN, false);
}

float4 PS_Fins_true( vertexOutputComposite IN ) : COLOR
{
	return PS_Fins(IN, true);
}



technique draw
{

	pass p0 
	{
		CullMode = NONE;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SRCALPHA;
		DestBlend = INVSRCALPHA;

		VertexShader = compile VERTEXSHADER VS_Fins_Unskinned();
		PixelShader  = compile PIXELSHADER PS_Fins_false();
	}
}

#if 0
technique drawskinned
{

	pass p0 
	{
		CullMode = NONE;
		ZWriteEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

		VertexShader = compile VERTEXSHADER VS_Fins();
		PixelShader  = compile PIXELSHADER PS_Fins_false();
	}
}



technique finsPass2_drawskinned
{

	pass p0 
	{
		AlphaTestEnable = true;
		CullMode = NONE;
		ZWriteEnable = true;
		AlphaRef = 32;
		AlphaFunc = GREATER;

		AlphaBlendEnable = false;
		ColorWriteEnable = ALPHA;

		VertexShader = compile VERTEXSHADER VS_Fins();
		PixelShader  = compile PIXELSHADER PS_Fins_true();
	}
}



#endif



