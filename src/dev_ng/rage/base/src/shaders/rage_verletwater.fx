//**************************************************************//
//  RAGE Water shader
//
//  David Serafim 12 Oct 2005
//**************************************************************//

#include "../shaderlib/rage_samplers.fxh"
#include "../shaderlib/rage_common.fxh"

#include "../shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(15);

// Put vertex sampler first since we only support four.
BeginSampler(sampler2D,verletsimTexture,VerletSimSampler,VerletSimTex)
    string UIName = "Verlet Simulation Texture";
ContinueSampler(sampler2D,verletsimTexture,VerletSimSampler,VerletSimTex)
EndSampler;

BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
    string UIName = "Diffuse Texture";
ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
EndSampler;

BeginSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
    string UIName = "Bump Texture";
ContinueSampler(sampler2D,bumpTexture,BumpSampler,BumpTex)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;

BeginSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
    string UIName = "Environment Texture";
ContinueSampler(samplerCUBE,environmentTexture,EnvironmentSampler,EnvironmentTex )
#if !__FXL
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
#endif
EndSampler;

BeginSampler(sampler2D,refractionTexture,RefractionSampler,RefractionTex )
    string UIName = "Refraction Texture";
ContinueSampler(sampler2D,refractionTexture,RefractionSampler,RefractionTex )
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;


float ReflectiveIntensity : ReflectiveIntensity
<
	string UIName = "Reflection Intensity";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.05;
> = 0.5;

float RefractionIntensity : RefractionIntensity
<
	string UIName = "Refraction Intensity";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.05;
> = 0.5;

float DiffuseIntensity : DiffuseIntensity
<
	string UIName = "Diffuse Intensity";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.05;
> = 0.15;

float SpecularIntensity : SpecularIntensity
<
	string UIName = "Specular Intensity";
	float UIMin = 0.0;
	float UIMax = 2.0;
	float UIStep = 0.05;
> = 0.5;

float SpecExponent : SpecularExp
<
	string UIName = "Specular Exponent";
	float UIMin = 4.0;
	float UIMax = 10000.0;
	float UIStep = 5.0;
> = 250.0;

float ReflectionDistortion : ReflectionDistortion
<
	string UIName = "Reflection Distortion";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 1.00;
	float UIStep = 0.05;
> = 0.50;

float RefractionIndex : RefractionIndex
<
	string UIName = "Refraction Index";
	string UIWidget = "Numeric";
	float UIMin = -0.00;
	float UIMax = 1.00;
	float UIStep = 0.05;
> = 1.00;

float FresnelIntensity : FresnelIntensity
<
	string UIName = "Fresnel Intensity";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 1.00;
	float UIStep = 0.05;
> = 0.50;

const float4 WaterColor
<
	string UIName = "Water Color";
	string UIWidget = "Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = { 0.2, 0.4, 0.64, 1.00 };

float RippleAmplitude : RippleAmplitude
<
	string UIName = "Ripple Amplitude";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 10.00;
	float UIStep = 0.5;
> = 2.00;

float RippleScale : RippleScale
<
	string UIName = "Ripple Scale";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 100.00;
	float UIStep = 0.5;
> = 3.00;


const float RipplePhase : RipplePhase;



struct VS_OUTPUT
{
   float Col:       COLOR0;
   DECLARE_POSITION( Pos )
   float2 TexCoord:   TEXCOORD0;
   float3 Normal:   TEXCOORD1;
   float3 View:   TEXCOORD2;
   float4 ScrPos:   TEXCOORD3;
   float3 WorldPos:   TEXCOORD4;
};

#pragma dcl position normal texcoord0

VS_OUTPUT vs_main(float4 inPos: POSITION0, float3 inNormal: NORMAL, float2 inTxr: TEXCOORD0)
{
	VS_OUTPUT OUT;

	float4 displ = tex2Dlod(VerletSimSampler,float4(inTxr,0,0));

	float4 pos = inPos;
	pos.y += saturate(displ.r) * RippleAmplitude;
	pos = mul(pos, gWorldViewProj);

	OUT.Col = saturate(displ.r);

	OUT.Pos = pos;
	OUT.TexCoord = inTxr;

	OUT.Normal = normalize(mul(inNormal, (float3x3)gWorld));
	OUT.View = normalize(gViewInverse[3].xyz - (float3)mul(inPos, gWorld));
	OUT.ScrPos = pos;
	OUT.WorldPos = (float3) normalize(float4(0,0,1000,1) - mul(inPos, gWorld));

	return OUT;
}


// because our Fx->Cg translator doesn't understand functions...
const float R0 = ((1.0 - 1.14) * (1.0 - 1.14)) / ((1.0 + 1.14) * (1.0 + 1.14));

float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	float3 view = normalize(IN.View);
	float3 normal = normalize(IN.Normal);
	float3 verletpert = IN.Col * 0.1;

	// move ripples in multiple directions
	float3 np = float3(IN.TexCoord*RippleScale,IN.TexCoord.y);
	float3 normal0 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.02,0.05) * RipplePhase) * 2 - 1);// * rphase0;
	float3 normal1 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.1,-0.03) * RipplePhase) * 2 - 1);// * rphase1;
	float3 normal2 = (float3) (tex2D(BumpSampler, (float2)np + float2(-0.1,0.05) * RipplePhase) * 2 - 1);
	float3 normal3 = (float3) (tex2D(BumpSampler, (float2)np + float2(0.03,0.07) * RipplePhase) * 2 - 1);
	float3 ldir = (float3) normalize(IN.WorldPos);
	float3 halfvec = normalize(ldir+view);
	view.z=-view.z;
	float3 halfvec2 = normalize(ldir+view);
	float3x3 TangentSpace;
	TangentSpace[0] = float3(1,0,0);
	TangentSpace[1] = cross(TangentSpace[0],normal);
	TangentSpace[2] = normal;
	halfvec = mul(halfvec,TangentSpace);
	halfvec2 = mul(halfvec2,TangentSpace);
	ldir = mul(ldir,TangentSpace);
	float3 diffuse = (dot(ldir,normal0) + dot(ldir,normal1)) / 2;
	float3 specular = dot(halfvec,normal2) * dot(halfvec,normal3) * dot(halfvec,normal1);
	specular *= (ragePow(dot(halfvec2,normal), SpecExponent) * SpecularIntensity) * gLightColor[2].rgb;
	float3 bump = diffuse + specular;
	bump *= 1-IN.Col;

	float4 pos = IN.ScrPos;

	float2 refr = pos.xy / pos.w;
	
	float cosine = dot(view,normal);
	
	float3 zcorrectedbump = bump * saturate(1.0/IN.ScrPos.z) + 0.1;
	refr = refr + (float2)zcorrectedbump * RefractionIndex;
	refr = refr * 0.5 + 0.5;
	refr.y += 0.05;
	refr.x -= 0.05;
	refr.y = 1-refr.y;
	refr += (float2) verletpert;

	float2 difftcoord = (IN.TexCoord*6+ RipplePhase*0.1) - (float2)bump;
	float2 difftcoord2 = (IN.TexCoord*3 - RipplePhase*0.15) + (float2)bump;
	float4 diff = tex2D(DiffuseSampler,difftcoord)*tex2D(DiffuseSampler,difftcoord2)*WaterColor*DiffuseIntensity + WaterColor;

	float3 refl = -view + 2*cosine * normal - bump * ReflectionDistortion;
	
	refl += verletpert;

	// fresnel term
	float fresnel = max(cosine, -cosine);
	fresnel = (R0 + (1.0 - R0) * pow(1.0 - fresnel, 2));
	fresnel = saturate((fresnel*4+verletpert.x)*FresnelIntensity);

	float ratio = (ReflectiveIntensity / (ReflectiveIntensity+RefractionIntensity));
	ratio = saturate(lerp(ratio,ratio*fresnel,FresnelIntensity));
	float4 col = lerp(tex2D(RefractionSampler,refr)*RefractionIntensity, texCUBE(EnvironmentSampler,refl)*ReflectiveIntensity, ratio);
	col = lerp(col,col*diff,DiffuseIntensity);
	float4 diffcolor = diff*DiffuseIntensity * (float4(diffuse,1)*0.3+0.7);
	col += diffcolor + pow(diff,8);
	col += float4(specular,1);

	col.a = WaterColor.a;

	return col;
}


//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
      ZENABLE = true;
      CULLMODE = CW;
      ALPHABLENDENABLE = true;
      SRCBLEND = SRCALPHA;
      DESTBLEND = INVSRCALPHA;

      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

