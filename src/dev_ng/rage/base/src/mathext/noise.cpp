// 
// math/noise.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "noise.h"

#include "math/amath.h"
#include "vector/vector4.h"

using namespace rage;

// This needs to be two copies of a permutation of 0..255
int PerlinNoise::sm_Permutation[512] = {
		151,160,137,91,90,15,
		131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
		190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
		88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
		77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
		102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
		135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
		5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
		223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
		129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
		251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
		49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
		138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
		,
		151,160,137,91,90,15,
		131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
		190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
		88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
		77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
		102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
		135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
		5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
		223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
		129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
		251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
		49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
		138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
};

RawVector3 PerlinNoise::sm_GradientTable[16] = 
{
	{1.0f, 1.0f, 0.0f},
	{-1.0f, 1.0f, 0.0f},
	{1.0f, -1.0f, 0.0f},
	{-1.0f, -1.0f, 0.0f},

	{1.0f, 0.0f, 1.0f},
	{-1.0f, 0.0f, 1.0f},
	{1.0f, 0.0f, -1.0f},
	{-1.0f, 0.0f, -1.0f},

	{0.0f, 1.0f, 1.0f},
	{0.0f, 1.0f, -1.0f},
	{0.0f, -1.0f, 1.0f},
	{0.0f, -1.0f, -1.0f},

	{1.0f, 1.0f, 0.0f},
	{-1.0f, 1.0f, 0.0f},
	{0.0f, -1.0f, 1.0f},
	{0.0f, -1.0f, -1.0f}
};

u32 PerlinNoise::sm_Wrap = 0;

u32 PerlinNoise::SetWrap(u32 wrap)
{
	mthAssertf((wrap & (wrap -1)) == 0, "wrap must be a power of two, not %d", wrap);
	u32 oldWrap = sm_Wrap;
	sm_Wrap = wrap;
	return oldWrap;
}

float PerlinNoise::RawNoise(float x)
{
	float floorx = floorf(x);
	int xi = (int)floorx & 255;

	x -= floorx;

	float u = Fade(x);

	int A = sm_Permutation[xi];
	int B = sm_Permutation[xi + 1];

	return Lerp(u, 
		Grad(A, Vector3(x, 0.0f, 0.0f)), 
		Grad(B, Vector3(x - 1.0f, 0.0f, 0.0f)));
}

float PerlinNoise::RawNoise(Vector3::Param vOrig)
{
	Vector3 v(vOrig);
	Vector3 floors(floorf(v.x), floorf(v.y), floorf(v.z));

	u32 mask = sm_Wrap - 1;
	if (mask > 255)
		mask = 255;

	int xi = (int)floors.x;
	int yi = (int)floors.y;
	int zi = (int)floors.z;
	int xf = (xi + 1) & mask;
	xi = xi & mask;
	int yf = (yi + 1) & mask;
	yi = yi & mask;
	int zf = (zi + 1) & mask;
	zi = zi & mask;

	v.Subtract(floors);

	Vector3 lerpFactors(Fade(v));

	int A = sm_Permutation[xi];
	int AA = sm_Permutation[A + yi];
	int AB = sm_Permutation[A + yf];
	int B = sm_Permutation[xf];
	int BA = sm_Permutation[B + yi];
	int BB = sm_Permutation[B + yf];

	// Do 4 lerps at once
	Vector4 vecA(
		Grad(sm_Permutation[AA + zi], v), 
		Grad(sm_Permutation[AB + zi], v - YAXIS),
		Grad(sm_Permutation[AA + zf], v - ZAXIS),
		Grad(sm_Permutation[AB + zf], (v - YAXIS) - ZAXIS)
		);

	Vector4 vecB(
		Grad(sm_Permutation[BA + zi], v - XAXIS),
		Grad(sm_Permutation[BB + zi], (v - XAXIS) - YAXIS),
		Grad(sm_Permutation[BA + zf], (v - XAXIS) - ZAXIS),
		Grad(sm_Permutation[BB + zf], v - VEC3_IDENTITY)
	);

	Vector4 lerpOverU;
	lerpOverU.Lerp(lerpFactors.x, vecA, vecB);

	return Lerp(lerpFactors.z, 
				Lerp(lerpFactors.y,
					lerpOverU.x,
					lerpOverU.y),
				Lerp(lerpFactors.y,
					lerpOverU.z,
					lerpOverU.w));
}

float PerlinNoise::Noise(Vector3::Param vOrig, int octaves, float persistance/* =0.5f */, float separation /* = 2.0f */)
{
	Vector3 v(vOrig);
	float weight = 1.0f;
	float accumulator = 0.0f;
	u32 oldWrap = sm_Wrap;
	mthAssertf(sm_Wrap == 0 || separation == 2.0, "Tiling is only supported for separation == 2.0");
	for(int i = 0; i < octaves; i++)
	{
		accumulator += RawNoise(v) * weight;
		weight *= persistance;
		v.Scale(separation);
		sm_Wrap *= 2;
	}
	sm_Wrap = oldWrap;
	return accumulator;
}

Vector3::Return PerlinNoise::Noise3d(Vector3::Param vOrig, int octaves, float persistance/* =0.5f */, float separation /* = 2.0f */)
{
	Vector3 orig(vOrig);
	Vector3 v1(orig.x, orig.y, orig.z);
	Vector3 v2(orig.z, orig.x, orig.y);
	Vector3 v3(orig.y, orig.z, orig.x);

	return Vector3(
		Noise(v1, octaves, persistance, separation),
		Noise(v2, octaves, persistance, separation),
		Noise(v3, octaves, persistance, separation)
		);
}
