//
// mathext/hilbert.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "hilbert.h"

using namespace rage;

// This implementation for hilbert curves differs from most descriptions.
// Most descriptions use recursive subdivision to find a point on the curve,
// but for that to work you need to know how big of a curve you want to start with.
// This technique doesn't depend on knowing the final size of the curve, rather
// it treats every curve segment as being 1 unit long and grows the whole curve to 
// fill larger and large spaces.
//
// The basic algorithm we use is to look at the bits in the number, 0xXXYYYY.
// Assume the YYYY bits have already been used to find a point on a hilbert curve.
// The hilbert curve is such that all the 0xYYYY points will fall into a square
// now the XX bits will transform (through offseting, rotation or mirroring) that curve so that
// when all XX bits are combined (00, 01, 10, 11) the 4 curves are connected.
// That is, when YYYY ranges from 0000 to 1111 we get a small hilbert curve. The 4
// XXs will create 4 copies of the curve that need to be offset, rotated and mirrored
// so that the ends of each of the smaller curves join up.
// So we process the bits from bottom up. The first N bits represent a point on a smaller curve,
// the next 2 bits tell us how to transform that point so that it fits on the larger curve.
//
// We do this instead of the top-down subdivision because with the subdivision you basically
// have to carry the transformation info along the way (i.e. given 0xXXYYZZ, and processing
// ZZ we need to know what kinds of rotations and reflections have been specified by XX and YY.
// The bottom up construction doesn't need to carry information from one level to another except
// for a bit saying whether we're processing an even or odd level.
//
// We also need the property that 0x00YYYY and 0xYYYY represent the same curve,
// so that we get that continual growth without knowing the final size.
//
// It may be faster to process the curve 4 bits at a time, instead of 2. It means there will be
// 16 cases in the switch statement, but we wouldn't have to track the parity bit (firstState)
// below, so would reduce the number of tests, loops, and math.
// Could also look into using lookup tables or something (vectorized integer ops?) if necessary

void FindPointOnHilbertCurve(u32 num, u32& x, u32& y)
{
	x = 0;
	y = 0;

	u32 highbits = num;
	u32 size = 1;

	bool firstState = false;

	while(highbits)
	{
		u32 lowbits = highbits & 3; 	// go through num 2 bits at a time, 
							// each 2 bits determines a new quadrant and orientation for the X, Y
							// coords determined by the preceeding bits
		highbits = highbits >> 2;

		firstState = !firstState;

		u32 oldX = x;
		u32 oldY = y;

		// We can probably rearrange code a bit to get rid of this test. since cases 0 and 2 are the same
		// for both states.
		if (firstState)
		{
			//             ^^
			//  >  becomes ><
			//
			// i.e. the preceding lowbits have been used to form a curve represented by this shape '>'.
			// (starts at the bottom left, goes right, up, left and ends in the upper left corner)
			// The current highbits tell how to transform and offset points in that shape to join up with other
			// copies to form a continuous curve. We want to make sure that case 0 is a no-op so that the curve
			// always grows in length without affecting what came before it. I.e. 0x00N and 0xN need to represent
			// the same value. 
			// Note that now that we've done this, the general shape of the new curve is ^ 
		 	// (starts at bottom left, goes up, right, down) so we need to go to the !firstState case.

			switch(lowbits)
			{
			case 0:
				break;
			case 1:
				// Mirror X,Y and offset up
				x = oldY;
				y = oldX + size; // could use | in place of + here and below
				break;
			case 2:
				// Mirror X, Y and offset into corner
				x = oldY + size;
				y = oldX + size;
				break;
			case 3:
				// 180 degree rotation (2 reflections) for X and Y, offset right
				x = ((size-1)-oldX) + size;
				y = ((size-1)-oldY);
				break;
			}
		}
		else
		{
			//              V>
			//   ^ becomes  ^>
			//
			// See above comments for what we're doing here but note that the general shape of the new curve is > which
			// brings us back to the firstState case.

			switch(lowbits)
			{
			case 0:
				break;
			case 1:
				// Mirror X, Y, offset right
				x = oldY + size;
				y = oldX;
				break;
			case 2:
				// Mirror X, Y and offset into corner
				x = oldY + size;
				y = oldX + size;
				break;
			case 3:
				// 180 degree rotation (2 reflections) for X and Y, offset up
				x = ((size-1)-oldX);
				y = ((size-1)-oldY) + size;
				break;
			}
		}

		size <<= 1;
	}
}

#if 0
// Faster implementation below

// Maps our computed quadrant table to the hilbert quadrants for a given level
u32 s_QuadrantTable1[] = {0,1,3,2};
u32 s_QuadrantTable2[] = {0,3,1,2};

// To find the distance along a hilbert curve...
// This is essentially the inverse of the technique we use above to find the X,Y position. 
// But rather than using two bits in the index to determine how to transform X and Y, we figure out
// which quadrant (i.e. which two high bits) X and Y put us in for a given level.
// Then we transform X and Y so they are essentially back in 'quadrant 0' and repeat.
//
// A hilbert curve that's an even power of 2 will fill a square. Call the length of the side of the square N
//
// Depending on the power of 2, the curve, in rough outline could look like '>' or '^' (see above)
// The structure of the curve is such that it fills one quadrant of the square completely before moving
// on to the next. So if we order the quadrants like so (subquadrants shown in parens):
//
//         3 2  ( v > )                   1 2  ( ^ ^ )
//  '>' :  0 1  ( ^ > )             '^' : 0 3  ( > < )
//
// The length of a curve ending in quadrant Q must be Q * (N*N) + E where E is the length of the curve
// within quadrant Q
//
// To find E, we tranform quadrant Q to quadrant 0 and repeat (using 
// square size N/2 and the opposite parity)
//
// Note that we can cheat a little bit for the transformation. At each successive level we're always transforming
// Q to the lower left, so we can clear bits in X and Y to handle the offset, then just need to mirror or rotate
// as appropriate.


u32 FindDistanceAlongHilbertCurve(u32 x, u32 y)
{
	bool firstState = true;

	u32 size = 1 << 16; // start with a square 2^16 on a side (could reduce this if necessary). 

	u32 dist = 0;

	while(size > 1)
	{
		size >>= 1;

		// find the quadrant that x,y is in
		u32 quad = 0;

		firstState = !firstState;

		u32 oldX = x;
		u32 oldY = y;

		// change this into bit extraction?
		if (x >= size)	quad += 1;
		if (y >= size)	quad += 2;

		//                    2 3                                                                               3 2           1 2
		//  quadrant is now   0 1.   Depending on firstState the real distance along the curve looks like this  0 1  or this  0 3
		//  so remap the quadrant number and then the quad * (halfSize^2) will be the number of distance of the curve up to the 
		//  halfSize block we're looking at.

		//  Could math be replaced here with bitwise operations on the bits of X, Y?
		//  subtractions could def. be done by clearing high bit of x, y (clearing the 1 << size bit)
		// (folds case 1, 2 together?)

		if (firstState)
		{
			quad = s_QuadrantTable2[quad];
			switch(quad)
			{
			case 0:
				break;
			case 1:
				// offset down, mirror X, Y
				x = oldY - size;
				y = oldX;
				break;
			case 2:
				// offset down, left. Mirror x,y
				x = oldY - size;
				y = oldX - size;
				break;
			case 3:
				// offset left, 180 degree rotation (2 reflections)
				x = (size-1) - (oldX - size);
				y = (size-1) - oldY;
				break;
			}
		}
		else
		{
			quad = s_QuadrantTable1[quad];
			switch(quad)
			{
			case 0:
				break;
			case 1:
				// offset down, mirror X, Y
				x = oldY;
				y = oldX - size;
				break;
			case 2:
				// Mirror X, Y and offset into corner
				x = oldY - size;
				y = oldX - size;
				break;
			case 3:
				// offset down, 180 degree rotation (2 reflections)
				x = (size-1) - oldX;
				y = (size-1) - (oldY - size);
				break;
			}
		}
		dist += (size * size) * quad;
	}
	return dist;
}
#else

// The faster implementation of distance-along-hilbert-curve.
//
// The technique is basically the same as above, but with a few refinements.
// The most significant change is that instead of keeping track of a parity bit and
// doing something different at even and odd powers of two for size, we handle
// 2 powers of 2 at a time. That is, rather than finding out which quadrant
// the X and Y coordinates specify, we divide the space into a 4x4 grid and
// find out which of the 16 grid cells contains X,Y.
//
// The reason for doing this is that the curve is self-similar at this level. For example
// consider the curve represented by '>' (starting at the bottom left, moving around the
// square to the top left). The code above shows a single expansion of this curve. Doing
// two expansions yields:
//
//                > < v >       15 12 11 10
//                v v ^ >       14 13 8  9
//  >    -->      ^ ^ v >       1  2  7  6
//                > < ^ >       0  3  4  5
//
// This new curve begins at the lower left, and continues to the upper left, so it 
// also has a '>' shape.
//
// So the basic algorithm above is used, where we compute the distance along the curve
// to the grid cell that contains x,y. Transform x,y to the lower left corner, then
// repeat with the new grid size.
//
// We can simplify the transformation a bit too. Since we're always translating the curve
// to the lower left we can clear the high bits of X and Y.
// Also, as it happens each of the >, <, ^ and V shapes comes in only one parity, i.e. 
// there are no cases where the > means the curve starts at the upper left and moves to the
// lower left, it's always lower left to upper left.
// This means that after translation, there are only four kinds of transformation we need to
// to to map the existing grid cell to >.
// The transformations are:
//  > : identity
//  < : mirror X and Y (180 degree rotation)
//  ^ : swap X and Y (reflect along X=Y axis)
//  v : swap X and Y, mirror X and Y (reflection + rotation)
//
//  The mirror X operation is (size-1)-x. (i.e. maps (0,N) to (N,0)) 
//    As a possible optmization, we can replace this by inverting the bits in X and clearing out the high bits (> size)
//
//  could have a quick initial loop to start 'size' at the first non-zero bits.
u32 FindDistanceAlongHilbertCurve(u32 x, u32 y)
{
	// Since there are 16 grid cells and we're mapping a grid cell to its
	// distance along the curve, we can use one 64 bit literal value to hold
	// the 16 4-bit distance values
	const u64 cellDistanceTable = 0xabcf98de67215430ULL;

	// Specifies the transformation needed for each grid cell
	// 0 = identity, 1 = mirror XY, 2 = swap x,y, 3 = swap X, Y, mirror X
	// yeah this could be a u32, since we only need 2 bits per cell, but u64 is easier to read
	const u64 cellTransformTable = 0x0310023303220210ULL;

#if 0
	int size = 1 << 14; // start with a square 2^16 per size (max size.). So cell size is 2^14 per side.

	int shift = 14;
#else
	// find the smallest power of 4 greater than x or y. Since we're looking for a smallest power of 2
	// we can literally find one smaller than (x | y)
	u32 xory = x | y;

	int shift = 2;

	u32 mask = 0xfffffffc;
	while((xory & mask) != 0)
	{
		mask <<= 2;
		shift += 2;
	}
	int size = 1 << shift;

#endif

	u32 dist = 0;
#if 1
	while(size > 0)
	{
		// could we do this w/o a variable length shift? Maybe using rotate instructions to wrap high bits around
		// to be the low bits
		// Since transform is done w/ bit operations, not arithmetic, we could shift bits up instead of down.
		// i.e. 
		// xBits = x & (0xb0000000)
		// transform x as normal
		// x <<= 2

		u32 xBits = (x >> shift) & 0x3;
		u32 yBits = (y >> shift) & 0x3;

		u32 whichCell = (yBits * 4 + xBits) * 4; // *4 for 4 bits per cell (needed on the next lines)

		u32 celldist = (u32)((cellDistanceTable >> whichCell) & 0xf);
		u32 xform = (u32)((cellTransformTable >> whichCell) & 0xf);

		dist += (size*size) * celldist;

		// Could do selects instead of switch?
		u32 oldX = x;
		u32 oldY = y;
		x = (xform & 2) ? oldY : oldX;
		y = (xform & 2) ? oldX : oldY;
		x = (xform & 1) ? ~x : x;
		y = (xform & 1) ? ~y : y;
/*
		switch(xform)
		{
		case 0:
			break;
		case 1:
			x = ~x;
			y = ~y;
			break;
		case 2:
			{
				u32 t = x;
				x = y;
				y = t;
			}
			break;
		case 3:
			{
				u32 t = x;
				x = ~y;
				y = ~t;
			}
			break;
		}
		*/

		// Translate to 0,0 (0 out all the high bits, including the ones we just read)
		x = x & (0xffffffff >> shift);
		y = y & (0xffffffff >> shift);
		// size is a power of 2, so size-1 is 000...001111...111, which is just the mask we need.
//		x = x & (size-1);
//		y = y & (size-1);

		size = size >> 2;
		shift -= 2;
	}
#else
	// Same as above, but rather than shifting x and y down toward 0, shift them up to put the bits we want to 
	// work with in the high bits. This lets us do more fixed sized shifts (which are apparently faster)
	// also change some of the arithmetic to bit operations.
	// This works mostly because we're no longer doing any arithmetic on x and y, just looking at the bits
	// and swapping or flipping them as necessary.
	while(size > 0)
	{
		u32 xBits = (x & 0xb0000000);
		u32 yBits = (y & 0xb0000000);

		u32 whichCell = (y >> 26) | (x >> 28);  // want 00000yyxx00 (the final 00 because we want tableIndex * 4 (4 bits per table entry) )

		u32 celldist = (u32)((cellDistanceTable >> whichCell) & 0xf);
		u32 xform = (u32)(cellTransformTable >> whichCell); // skip the mask, do it later

		dist = (dist << 4) | celldist; // adds the adds celldist * (size*size) to dist (relies on the fact that there isn't an early exit from the loop, so dist gets shifted up the right amount)

		bool swap = xform && 1;
		bool flip = xform && 2;

		x = flip ? ~x : x;
		y = flip ? ~y : y;

		u32 oldX = x;
		u32 oldY = y;

		x = swap ? oldY : oldX;
		y = swap ? oldX : oldY;

		x <<= 2;
		y <<= 2;

		size = size >> 2;
	}
#endif



	return dist;
}

#endif
