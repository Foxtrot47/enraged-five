//
// mathext/lcp.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// LCP SOLVER using GAUSS-SEIDEL METHOD
// Initial author: Sam Buss, May 2004.

#include "lcp.h"

#include "math/amath.h"
#include "math/constants.h"


#if SPARSE_SHOOTING==1
#include "vector/vector3.h"
#endif


#define LCP_WARNING_BEEPS	0
#define LCP_LARGE_ERROR_BEEPS	0

#if LCP_WARNING_BEEPS || LCP_LARGE_ERROR_BEEPS
#include "system/xtl.h"
#endif

#define TRY_FOR_COLINEAR	0		// 0 means don't try for colinear force and velocity


// Enable this to make "S" key press cause a save of the LCP data.
#define	WRITE_LCP_TO_FILE	0
#if WRITE_LCP_TO_FILE
#include "file/asset.h"
#include "file/token.h"
#include "input/keyboard.h"
#include "input/keys.h"
#endif


// Damp all diagonal matrix elements.
#define	DAMP_LCP			1
#define DAMP_LCP_LAMBDA2	0.002f


namespace rage {

namespace LCPSolver {

const float omega = 1.9f;			// Overrelaxation constant.
#define OVERRELAX 1

#if OVERRELAX
bool g_overrelax = false;
#endif

void Transform3x3 (const float* matrix, const float* in, float* out)
{
	out[0] = matrix[0]*in[0]+matrix[1]*in[1]+matrix[2]*in[2];
	out[1] = matrix[3]*in[0]+matrix[4]*in[1]+matrix[5]*in[2];
	out[2] = matrix[6]*in[0]+matrix[7]*in[1]+matrix[8]*in[2];
}

void Transform3x3AndNegate (const float* matrix, const float* in, float* out)
{
	out[0] = -(matrix[0]*in[0]+matrix[1]*in[1]+matrix[2]*in[2]);
	out[1] = -(matrix[3]*in[0]+matrix[4]*in[1]+matrix[5]*in[2]);
	out[2] = -(matrix[6]*in[0]+matrix[7]*in[1]+matrix[8]*in[2]);
}

void SolveLcpGS (int dimension, const float* matrix, const float* vector, float* solution, float* delVector,
					float* tempArray, float tolerance, int maxIterations)
{
	DebugAssert(CheckPositiveDiagonal(dimension,matrix));

	// Initialize by setting tempQ equal to q.
	// This is equivalent to initially setting z equal to zero.
	// Any other nonnegative values could be used for z (see Cottle et al.)
	float* tempQ = &tempArray[0];
	float* to = tempQ;
	const float* from = vector;
	int i;
	for (i=dimension; i>0; i--)
	{
		(*(to++)) = (*(from++));
	}

	float tolerSq = square(tolerance);
	float bestError = 1.0e30f;
	int iterNum = 0;
	int nonConvergeCount = 0;	// Counts the number of times z,w worsen

	while (1)
	{
		// Step 1 of Cottle et al's projected Gauss-Seidel.
		// Calculate z, w and the next q value.

		// First solve the lower triangular version of the LCP.
		//    LCP(D+L,tempQ) ==> z  
		const float* mDiagPtr = matrix;
		const float* mRowPtr = matrix;
		for ( i=0; i<dimension; i++ )
		{
			float r = *(tempQ+i) + InnerProduct( i, mRowPtr, solution );
			if ( r<0.0f )
			{
				*(solution+i) = -r/(*mDiagPtr);
			}
			else
			{
				*(solution+i) = 0.0f;
			}
			mRowPtr += dimension;
			mDiagPtr += dimension+1;
		}
		// Second calculate the q value for the next LCP subproblem
		//    tempQ = q + Uz;
		const float* mSuperDiag = matrix+1;
		for ( i=0; i<dimension; i++ )
		{
			tempQ[i] = *(vector+i) + InnerProduct( dimension-i-1, mSuperDiag, solution+i+1 );
			mSuperDiag += dimension+1;
		}
		// Third calculate the w value that corresponds to z, w = M z.
		mRowPtr = matrix;
		for ( i=0; i<dimension; i++ )
		{
			delVector[i] = tempQ[i] + InnerProduct( i+1, mRowPtr, solution );
			mRowPtr += dimension;
		}
		
		// Step 2 of Cottle-Pang-Stone
		// Use Root Mean Square as measure of tolerance
		if ( ++iterNum >= maxIterations )
		{
			break;					// If number of iterations is too high
		}
		float meanErrSq = 0.0f;
		for ( i=0; i<dimension; i++ )
		{
			float thisW = delVector[i];
			float thisZ = solution[i];
			// error in component i = Min ( -w[i], Min(w[i],z[i]) )
			//   We know z[i]>0.
			if ( thisW < thisZ )
			{
				meanErrSq += thisW*thisW;
			}
			else
			{
				meanErrSq += thisZ*thisZ;
			}
		}
		if ( meanErrSq<bestError )
		{
			if ( meanErrSq<tolerSq )
			{
				break;				// If error is sufficiently small
			}

			bestError = meanErrSq;
		}
		else if (++nonConvergeCount >= 2 )
		{
			break;					// If we are not making progress!
		}
	}
}


void SolveMlcpFriction1GS (int dimension, const float* matrix, const float* vector, const int* frictionMode,
							const float* frictionCoef, float* solution, float* delVector, float* tempArray,
							float tolerance, int maxIterations)
{
	DebugAssert(CheckPositiveDiagonal(dimension,matrix));

	// Initialize by setting tempQ equal to q.
	// This is equivalent to initially setting z equal to zero.
	// Any other nonnegative values could be used for z (see Cottle et al.)
	float* tempQ = &tempArray[0];
	float* to = tempQ;
	const float* from = vector;
	int i;
	for ( i=dimension; i>0; i-- )
	{
		(*(to++)) = (*(from++));
	}

	// Set up termination decisions.
	float tolerSq = tolerance*tolerance;
	float bestError = 1.0e30f;
	int iterNum = 0;
	int nonConvergeCount = 0;	// Counts the number of times z,w worsen

	while (1)
	{
		// Step 1 of Cottle et al's projected Gauss-Seidel.
		// Calculate z, w and the next q value.

		// First solve the lower triangular version of the LCP.
		//    LCP(D+L,tempQ) ==> z  -- modified with frictional limits
		const float* mDiagPtr = matrix;
		const float* mRowPtr = matrix;
		const int* fModePtr = frictionMode;
		const float* fCoefPtr = frictionCoef;
		for ( i=0; i<dimension; i++ ) {
			float r = *(tempQ+i) + InnerProduct( i, mRowPtr, solution );

			//!me this matrix splitting really only amounts to starting the solution off with one solve of the lower diaganal first.  n'est pas?
			//!me if we pass in the solution from the last frame... don't we want to use that instead?
//			r? = (*(vector+i) + InnerProduct( dimension-i-1, mSuperDiag, (solution+i+1)^(iterNum-1) )) + InnerProduct( i, mRowPtr, solution^iterNum );


			if ( (*fModePtr) == -1 ) { // If normal force
				if ( r<0.0f ) {
					*(solution+i) = -r/(*mDiagPtr);
				}
				else {
					*(solution+i) = 0.0f;
				}
			}
			else {							// If tangential (frictional) force
				mthAssertf ( *fModePtr < i, "Frictional force is later in list its normal force");
				mthAssertf ( -1 == *(frictionMode+*fModePtr), " Only the \"2\" version of routine allows this");
				float f = -r/(*mDiagPtr);
				float fricLimit = (*(solution+(*fModePtr)))*(*fCoefPtr);	// f_n * \mu
				if ( f < -fricLimit ) {
					f = -fricLimit;
				}
				else if ( f > fricLimit ) {
					f = fricLimit;
				}
				*(solution+i) = f;
			}
			mRowPtr += dimension;
			mDiagPtr += dimension+1;
			fModePtr++;
			fCoefPtr++;
		}

		// Second calculate the q value for the next LCP subproblem
		//    tempQ = q + Uz;
		const float* mSuperDiag = matrix+1;
		for ( i=0; i<dimension; i++ ) {
			tempQ[i] = *(vector+i) + InnerProduct( dimension-i-1, mSuperDiag, solution+i+1 );
			mSuperDiag += dimension+1;
		}
		// Third calculate the w value that corresponds to z, w = M z.
		mRowPtr = matrix;
		for ( i=0; i<dimension; i++ ) {
			delVector[i] = tempQ[i] + InnerProduct( i+1, mRowPtr, solution );
			mRowPtr += dimension;
		}
		
		// Step 2 of Cottle-Pang-Stone
		// Use Root Mean Square as measure of tolerance
		if ( ++iterNum >= maxIterations ) {
			break;					// If number of iterations is too high
		}
		float meanErrSq = 0.0f;
		for ( i=0; i<dimension; i++ ) {
			float thisZ = solution[i];
			int j = *(frictionMode+i);
			float thisW = delVector[i];
			if ( j == -1 ) {			// if normal force
				// error in component i = Min ( -w[i], Min(w[i],z[i]) )
				//   We know z[i]>0.
				if ( thisW < thisZ ) {
					meanErrSq += thisW*thisW;
				}
				else {
					meanErrSq += thisZ*thisZ;
				}
			}
			else {						// if tangential force
				float fricLimit = (*(frictionCoef+i))*solution[j];
				float fabsThisZ = fabsf(thisZ);
				fricLimit -= fabsThisZ;
				if ( fricLimit<0 ) {				// If exceeds frictional limits
					meanErrSq += fricLimit*fricLimit;
				}
				if ( thisW*thisZ > 0.0f ) {
					float e = Min(fabsf(thisW),fabsThisZ);
					meanErrSq += e*e;		// Frictional force should oppose motion direction
				}
			}
		}
		if ( meanErrSq<bestError ) {
			if ( meanErrSq<tolerSq ) {
				break;				// If error is sufficiently small
			}
			bestError = meanErrSq;
		}
		else if (++nonConvergeCount >= 2 )
		{
			break;					// If we are not making progress!
		}
	}
}


int SolveMlcpFriction2GS (int dimension, const float* matrix, const float* vector, const int* frictionMode,
							const float* frictionCoef, float* solution, float* delVector, float* tempArray,
							float* tempSolution, float* tempDelVector, bool startWithTempSolution,
							float tolerance, int maxIterations)
{
	DebugAssert(CheckPositiveDiagonal(dimension,matrix));

	// Initialize by setting tempArray to the input vector.
	// This is equivalent to initially setting z equal to zero.
	// Any other nonnegative values could be used for z (see Cottle et al.)
	float* to = tempArray;
	const float* from = vector;
	int i;
	for ( i=dimension; i>0; i-- ) {
		(*(to++)) = (*(from++));
	}

	// Set up termination decisions.
	float tolerSq = square(tolerance);
	float bestError = 1.0e30f;
	float meanErrSq = 0.0f;
	float prevMeanErrSq = bestError;
	int iterNum = 0;
	int nonConvergeCount = 0;	// counts the number of times the solution gets worse
	int badDivergeCount = 0;	// counts the number of times the solution gets much worse

#if TRY_FOR_COLINEAR 
	bool firstTimeFlag = true;
#endif

	while (1)
	{
		// Step 1 of Cottle et al's projected Gauss-Seidel.
		// Calculate z, w and the next q value.

		// First solve the lower triangular version of the LCP.
		//    LCP(D+L,tempArray) ==> z  -- modified with frictional limits
		const float* mDiagPtr = matrix;
		const float* mRowPtr = matrix;
		const int* fModePtr = frictionMode;
		const float* fCoefPtr = frictionCoef;

		if (!startWithTempSolution || iterNum>0)
		{
			for ( i=0; i<dimension; i++ ) {
				float r = *(tempArray+i) + InnerProduct( i, mRowPtr, tempSolution );
				if ( (*fModePtr) == -1 ) { // If normal force
					if ( r<0.0f ) {

	#if OVERRELAX
						if( g_overrelax )
						{
							float oldSoln = *(tempSolution+i);
							float newSoln = -r/(*mDiagPtr);
							newSoln = (1.0f-omega)*oldSoln + omega*newSoln;
							*(tempSolution+i) = Max(0.0f, newSoln);
						}else
						{
							*(tempSolution+i) = -r/(*mDiagPtr);
						}
	#else	
						*(tempSolution+i) = -r/(*mDiagPtr);
	#endif
					}
					else {
						*(tempSolution+i) = 0.0f;
					}
				}
				else if (i==dimension-1 || (*(fModePtr+1))!=i ) {							
					// If a single tangential (frictional) force
					mthAssertf ( *fModePtr < i, "Frictional force is later in list its normal force");
					float f = -r/(*mDiagPtr);
	
	#if OVERRELAX
					if( g_overrelax )
					{
						float oldF = *(tempSolution+i);
						f = (1.0f-omega)*oldF + omega*f;
					}
	#endif

					float fricLimit = (*(tempSolution+(*fModePtr)))*(*fCoefPtr);	// f_n * \mu
					if ( f < -fricLimit ) {
						f = -fricLimit;
					}
					else if ( f > fricLimit ) {
						f = fricLimit;
					}
					*(tempSolution+i) = f;
				}
				else {			// If two tangential forces to be handled together

					float f1s, f2s;
					{

						mthAssertf ( *fModePtr < i, "Frictional force is later in list its normal force");
	
						float fricLimit = (*(tempSolution+(*fModePtr)))*(*fCoefPtr);	// f_n * \mu
	
						Solve2DFriction( i, r, dimension, mRowPtr, fricLimit, tempSolution, tempArray, f1s, f2s );
						*(tempSolution+i) = f1s;
						*(tempSolution+i+1) = f2s;
						i++;				// Will skip the next entry!
	
						fModePtr++;			// Increment pointers one extra
						fCoefPtr++;
						mRowPtr += dimension;
						mDiagPtr = mRowPtr + i;
					}
				}

				// Update pointers for while loop
				mRowPtr += dimension;
				mDiagPtr += dimension+1;
				fModePtr++;
				fCoefPtr++;
			}
		}	// if (!startWithTempSolution || iterNum>0)

		// Second calculate the q value for the next LCP subproblem
		// Third calculate the w value that corresponds to z, w = matrix z.
		//    tempArray = q + Uz;
		const float* mSuperDiag = matrix+1;
		mRowPtr = matrix;
		for ( i=0; i<dimension; i++ ) {
			if ( i<dimension-1 && *(frictionMode+i)!=-1 && *(frictionMode+i+1)==i ) {
				// If first of two tangential forces handled together
				tempArray[i] = *(vector+i) + InnerProduct( dimension-i-2, mSuperDiag+1, tempSolution+i+2 );
				tempDelVector[i] = tempArray[i] + InnerProduct( i+2, mRowPtr, tempSolution );
			}
			else {
				tempArray[i] = *(vector+i) + InnerProduct( dimension-i-1, mSuperDiag, tempSolution+i+1 );
				tempDelVector[i] = tempArray[i] + InnerProduct( i+1, mRowPtr, tempSolution );
			}
			mSuperDiag += dimension+1;
			mRowPtr += dimension;
		}
		
		// Step 2 of Cottle-Pang-Stone
		// Use Root Mean Square as measure of tolerance
		if ( ++iterNum >= maxIterations )
		{
			// The number of iterations exceeded the maximum. See of the error is large.
			if (meanErrSq>1.0e10f*tolerSq)
			{
			#if LCP_LARGE_ERROR_BEEPS
				MessageBeep(MB_ICONEXCLAMATION);
			#endif

			}

		#if LCP_WARNING_BEEPS
			else
			{
				MessageBeep(MB_OK);
			}
		#endif

			// Quit looking for a solution.
			break;
		}

		meanErrSq = 0.0f;
		for ( i=0; i<dimension; i++ ) {
			float thisZ = tempSolution[i];
			int j = *(frictionMode+i);
			float thisW = tempDelVector[i];
			if ( j == -1 ) {			// if normal force
				// error in component i = Min ( -w[i], Min(w[i],z[i]) )
				//   We know z[i]>0.
				if ( thisW < thisZ ) {
					meanErrSq += square(thisW);
				}
				else {
					meanErrSq += square(thisZ);
				}
			}
			else if (i==dimension-1 || (*(frictionMode+i+1))!=i )
			{	 // if single independent tangential force						
				float fricLimit = (*(frictionCoef+i))*tempSolution[j];
				float fabsThisZ = fabsf(thisZ);
				fricLimit -= fabsThisZ;
				if ( fricLimit<0.0f )
				{				// If exceeds frictional limits
					meanErrSq += square(fricLimit);
				}
				if ( thisW*thisZ > 0.0f )
				{
					float e = Min(fabsf(thisW),fabsThisZ);
					meanErrSq += square(e);		// Frictional force should oppose motion direction
				}
			}
			else
			{			// If two tangential forces to use together
				i++;				// Skip the next entry, already handled!
			}
		}
		if ( meanErrSq<bestError )
		{
			for (i=0; i<dimension; i++)
			{
				solution[i] = tempSolution[i];
				delVector[i] = tempDelVector[i];
			}

			if ( meanErrSq<tolerSq )
			{
				break;				// If error is sufficiently small
			}
			bestError = meanErrSq;
		}

		else if (meanErrSq>2.0f*prevMeanErrSq)
		{
			if (++badDivergeCount>=2)
			{
				break;
			}
		}

		else if (++nonConvergeCount >= 20 )
		{
			// The number of iterations with no progress exceeded the maximum, so quit looking for a solution.
			break;
		}

	#if TRY_FOR_COLINEAR 
		firstTimeFlag = false;
	#endif

		prevMeanErrSq = meanErrSq;

	}

	return iterNum;		
}

/*
 *	//!me need to add error metric
 */
void SolveSparseMlcpFriction2GSNoSplit (int dimension, const float* matrix, const float* vector, float* solution, 
				 const int* mode,
				 const float* friction,
				 float UNUSED_PARAM(tolerance), int maxIterations)
{
	DebugAssert(CheckPositiveDiagonal(dimension,matrix));

	// Initialize by setting tempQ equal to q.
	// This is equivalent to initially setting z equal to zero.
	// Any other nonnegative values could be used for z (see Cottle et al.)
	int i;
	
	for (i=dimension; i>0; i--)
	{
		solution[i] = 0.0f; 
	}

	int iterNum = 0;

	while (1)
	{
		// Step 1 of Cottle et al's projected Gauss-Seidel.
		// Calculate z, w and the next q value.

		// First solve the lower triangular version of the LCP.
		//    LCP(D+L,tempQ) ==> z  

		//!me actually, I'm breaking some rules here, matrix splitting has been desynched...
		//!me Don't know if this is ok.  Effectively q^n = z^n-1
		const float* mDiagPtr = matrix;
		const float* mRowPtr = matrix;
		const int* fModePtr = mode;
		const float* fCoefPtr = friction;

		for ( i=0; i<dimension; i++ ) {
			float r = *(vector+i) + InnerProduct( i, mRowPtr, solution );
			//r += InnerProduct( dimension-i-1, mRowPtr+i+1, solution+i+1 );

			if ( (*fModePtr) == -1 ) 
			{ // If normal force
				r += InnerProduct( dimension-i-1, mRowPtr+i+1, solution+i+1 );
				if ( r<0.0f ) 
				{
					*(solution+i) = -r/(*mDiagPtr);
				}
				else 
				{
					*(solution+i) = 0.0f;
				}
			}
			else if (i==dimension-1 || (*(fModePtr+1))!=i ) 
			{	
				r += InnerProduct( dimension-i-1, mRowPtr+i+1, solution+i+1 );
				// If a single tangential (frictional) force
				mthAssertf ( *fModePtr < i, "Frictional force is later in list its normal force");
				float f = -r/(*mDiagPtr);
				float fricLimit = (*(solution+(*fModePtr)))*(*fCoefPtr);	// f_n * \mu
				if ( f < -fricLimit ) {
					f = -fricLimit;
				}
				else if ( f > fricLimit ) {
					f = fricLimit;
				}
				*(solution+i) = f;
			}
			else 
			{
				r += InnerProduct( dimension-i-2, mRowPtr+i+2, solution+i+2 );
				float f1, f2;
				mthAssertf ( *fModePtr < i,  "Frictional force is later in list its normal force");
				float fricLimit = (*(solution+(*fModePtr)))*(*fCoefPtr);	// f_n * \mu

				Solve2DFriction( i, r, dimension, mRowPtr, fricLimit, solution, vector, f1, f2, true );
				*(solution+i) = f1;
				*(solution+i+1) = f2;
				i++;				// Will skip the next entry!
				fModePtr++;			// Increment pointers one extra
				fCoefPtr++;
				mRowPtr += dimension;
				mDiagPtr = mRowPtr + i;

			}


			mRowPtr += dimension;
			mDiagPtr += dimension+1;
			fModePtr++;
			fCoefPtr++;
		}

		// Step 2 of Cottle-Pang-Stone
		// Use Root Mean Square as measure of tolerance
		if ( ++iterNum >= maxIterations ) {
			break;					// If number of iterations is too high
		}

	}

	return;
}


// ***********************************************************
// SolveMlcpFriction2J
//    Solve the Modified Linear Complementarity Problem with special
//		complementary conditions that apply to frictional
//		forces.  Frictional forces are done in pairs, taking
//		into account both tangential directions at once.
//		The functionality of SolveMlcpFriction1GS is also
//		supported
//    This version uses a projected Jacobi iterative
//		method, see Cottle, Pang, Stone, chapter 5.
//
//  n - dimension of the LCP problem.
//  M - an n x n matrix, diagonal entries MUST be positive!
//		Entries in M are given in *row* order.
//  q - an column vector of length n
//  FrictionalMode - see above.  Identifies frictional forces
//	CoefFric - array of n coefficients of friction
//  z, w - also column vectors of length n (returned values)
//  tolerance - a stopping criterion (in terms of root mean
//				square error.
//  maxIterations - a backup stopping criterion.
//  There is a further stopping criterion which is
//		automatically applied if the iterations fail
//		to converge to a better solution on at least two tries.
// ***********************************************************
void CopyFloatArray (int n, const float* from, float* to)
{
	for (int index=n; index!=0; index--)
	{
		*(to++) = *(from++);
	}
}


void SolveMlcpFriction2J (int dimension, const float* matrix, const float* vector, const int* frictionMode,
							const float* frictionCoef, float* solution, float* delVector, float* tempArray,
							float* tempSolution, float* tempDelVector, float tolerance, int maxIterations)
{
	DebugAssert(CheckPositiveDiagonal(dimension,matrix));

	// Initialize by setting tempQ equal to q.
	// This is equivalent to initially setting z equal to zero.
	// Any other nonnegative values could be used for z (see Cottle et al.)
	float* tempQ = &tempArray[0];
	float* z = &tempSolution[0];
	float* w = &tempDelVector[0];
	int i;
	float* to = tempQ;
	const float* from = vector;

	CopyFloatArray(dimension,from,to);

	// Set up termination decisions.
	float tolerSq = tolerance*tolerance;
	float bestError = 1.0e30f;
	int iterNum = 0;
	int nonConvergeCount = 0;	// Counts the number of times z,w worsen
	int badDivergeCount = 0;	// Counts the number of times z,w are *much* worse

	// bool firstTimeFlag = true;
	float prevMeanErrSq = bestError;

	while (1)
	{
		// Step 1 of Cottle et al's projected Gauss-Seidel.
		// Calculate z, w and the next q value.

		// First solve the lower triangular version of the LCP.
		//    LCP(D+L,tempQ) ==> z  -- modified with frictional limits
		const float* mDiagPtr = matrix;
		const float* mRowPtr = matrix;
		const int* fModePtr = frictionMode;
		const float* fCoefPtr = frictionCoef;
		for ( i=0; i<dimension; i++ ) {
			float r = *(tempQ+i);
			if ( (*fModePtr) == -1 ) { // If normal force
				if ( r<0.0f ) {
					*(z+i) = -r/(*mDiagPtr);
				}
				else {
					*(z+i) = 0.0f;
				}
			}
			else if (i==dimension-1 || (*(fModePtr+1))!=i ) {							
				// If a single tangential (frictional) force
				mthAssertf ( *fModePtr < i, "Frictional force is later in list its normal force");
				float f = -r/(*mDiagPtr);
				float fricLimit = (*(z+(*fModePtr)))*(*fCoefPtr);	// f_n * \mu
				if ( f < -fricLimit ) {
					f = -fricLimit;
				}
				else if ( f > fricLimit ) {
					f = fricLimit;
				}
				*(z+i) = f;
			}
			else {			// If two tangential forces to be handled together
							// ( w_i, w_{i+1} )^T = ((a,0),(0,d)) * (f_{t1},f_{t2})^T + (r,s)^T.
				mthAssertf ( *fModePtr < i, "Frictional force is later in list its normal force");
				float a = *mDiagPtr;		// 2x2 diagonal:
				mDiagPtr += dimension+1;
				float d = *mDiagPtr;
				mRowPtr += dimension;
				float s = *(tempQ+i+1);
				// (w_i, w_{i+1})^T = ((a,0),(0,d))(z_i, z_{i+1})^T + (r,s)^T.
				float f1 = -r/a;		// Solve 
				float f2 = -s/d;
				float normSq = f1*f1+f2*f2;
				float fricLimit = (*(z+(*fModePtr)))*(*fCoefPtr);	// f_n * \mu
				if ( normSq > fricLimit*fricLimit ) {
#if 1
					float fadeFactor = fricLimit/sqrt(normSq);
					f1 *= fadeFactor;
					f2 *= fadeFactor;
#else
					if ( !firstTimeFlag ) {
						f1 = w[i];	// Use previous velocity direction for first force estimate
						f2 = w[i+1];
					}
					else {			
						f1 = r;		// No previous velocity, just use r, s.
						f2 = s;
					}
					float normInv = sqrt(f1*f1+f2*f2);
					normInv = (normInv>0.0) ? 1.0/normInv : normInv;	// Avoid divide by zero
					f1 *= -(normInv*fricLimit);					// Right magnitude, and
					f2 *= -(normInv*fricLimit);					//    switch signs too.
#endif
				}
				*(z+i) = f1;
				*(z+i+1) = f2;
				fModePtr++;			// Increment pointers one extra time
				fCoefPtr++;			// mRowPtr and mDiagPtr already incremented above
				i++;				// Will skip the next entry!
			}
			mRowPtr += dimension;
			mDiagPtr += dimension+1;
			fModePtr++;
			fCoefPtr++;
		}

		// Second calculate the q value for the next LCP subproblem
		// Third calculate the w value that corresponds to z, w = M z.
		//    tempQ = q + Uz;
		mDiagPtr = matrix;
		mRowPtr = matrix;
		for ( i=0; i<dimension; i++ ) {
			w[i] = *(vector+i) + InnerProduct( dimension, mRowPtr, z );
			tempQ[i] = w[i] - (*mDiagPtr)*(*(z+i));
			mDiagPtr += dimension+1;
			mRowPtr += dimension;
		}
		
		// Step 2 of Cottle-Pang-Stone
		// Use Root Mean Square as measure of tolerance
		if ( ++iterNum >= maxIterations ) {
			break;					// If number of iterations is too high
		}
		float meanErrSq = 0.0f;
		for ( i=0; i<dimension; i++ ) {
			float thisZ = z[i];
			int j = *(frictionMode+i);
			float thisW = w[i];
			if ( j == -1 ) {			// if normal force
				// error in component i = Min ( -w[i], Min(w[i],z[i]) )
				//   We know z[i]>0.
				if ( thisW < thisZ ) {
					meanErrSq += thisW*thisW;
				}
				else {
					meanErrSq += thisZ*thisZ;
				}
			}
			else if (i==dimension-1 || (*(frictionMode+i+1))!=i ) {	 // if single independent tangential force						
				float fricLimit = (*(frictionCoef+i))*z[j];
				float fabsThisZ = fabs(thisZ);
				fricLimit -= fabsThisZ;
				if ( fricLimit<0 ) {				// If exceeds frictional limits
					meanErrSq += fricLimit*fricLimit;
				}
				if ( thisW*thisZ > 0.0f ) {
					float e = Min( fabsf(thisW), fabsThisZ );
					meanErrSq += e*e;		// Frictional force should oppose motion direction
				}
			}
			else {			// If two tangential forces to use together
				//float fricLimit = (*(CoefFric+i))*z[j];
				//float f1 = thisZ;
				//float f2 = z[i+1];
				//float normSq = f1*f1+f2*f2;
				//if ( normSq > fricLimit*fricLimit ) {
				//	float overage = sqrt(normSq)-fricLimit;
				//	meanErrSq += overage*overage;
				//}
				//float w1 = thisW;
				//float w2 = w[i+1];
				//float dotFW = f1*w1 + f2*w2;
				//float noncollinearErr = 0.0;
				//float normSqW = w1*w1+w2*w2;
				//if ( dotFW < 0.0 ) {			// This is the good case
				//	if ( normSqW > 0.0 ) {
				//		noncollinearErr = normSq - dotFW*dotFW/normSqW;
				//	}
				//}
				//else {
				//	noncollinearErr = normSq;	// This is the undesirable case
				//}
				//meanErrSq += Min( normSqW, noncollinearErr );
				i++;				// Skip the next entry, already handled!
			}
		}
		if ( meanErrSq<bestError ) {
			CopyFloatArray( dimension, z, solution );
			CopyFloatArray( dimension, w, delVector );
			if ( meanErrSq<tolerSq ) {
				break;				// If error is sufficiently small
			}
			bestError = meanErrSq;
		}
		else if ( meanErrSq > 2.0f*prevMeanErrSq ) {
			if ( ++badDivergeCount >= 2 ) {
				break;
			}
		}
		else if ( ++nonConvergeCount >= 20 ) {
			break;					// If we are not making progress!
		}
		// firstTimeFlag = false;
		prevMeanErrSq = meanErrSq;
	}
}


// Invert a symmetrix 3x3 matrix represented by an array of floats.
// This only uses lower part of the matrix, and there is no check for symmetry.
void InvertSym (float* matrix)
{
	// Compute the six distinct subdeterminants
	//float sd11 = m22*m33-m32*m32;
	float sd11 = matrix[4]*matrix[8]-square(matrix[7]);
	//float sd12 = m31*m32-m21*m33;
	float sd12 = matrix[6]*matrix[7]-matrix[3]*matrix[8];
	//float sd22 = m11*m33-m31*m31;
	float sd22 = matrix[0]*matrix[8]-square(matrix[6]);
	//float sd13 = m21*m32-m31*m22;
	float sd13 = matrix[3]*matrix[7]-matrix[6]*matrix[4];
	//float sd23 = m31*m21-m11*m32;
	float sd23 = matrix[6]*matrix[3]-matrix[0]*matrix[7];
	//float sd33 = m11*m22-m21*m21;
	float sd33 = matrix[0]*matrix[4]-square(matrix[3]);

	//register float detInv = 1.0/(m11*sd11 + m21*sd12 + m31*sd13);
	register float detInv = 1.0f/(matrix[0]*sd11 + matrix[3]*sd12 + matrix[6]*sd13);

	//m11 = sd11*detInv;
	matrix[0] = sd11*detInv;
	//m12 = m21 = sd12*detInv;
	matrix[1] = matrix[3] = sd12*detInv;
	//m13 = m31 = sd13*detInv;
	matrix[2] = matrix[6] = sd13*detInv;
	//m22 = sd22*detInv;
	matrix[4] = sd22*detInv;
	//m23 = m32 = sd23*detInv;
	matrix[5] = matrix[7] = sd23*detInv;
	//m33 = sd33*detInv;
	matrix[8] = sd33*detInv;
}


void SolveMlcpFriction3GS3x3 (int dimension, const float* matrix, const float* q, const int* frictionMode,
								const float* coefFric, float* zReturn, float* diagInverse, float stopFraction,
								float tolerance, int maxIterations)
{
	// Make sure all the diagonal elements of the input matrix are positive.
	DebugAssert(CheckPositiveDiagonal(dimension,matrix));

	// Set the return values to zero. Any other nonnegative values could be used (see Cottle et al.)
	float* zPtr = zReturn;
	int i;
	for (i=0; i<dimension; i++)
	{
		(*(zPtr++)) = 0.0f;
	}

	// Setup the inverses of the diagonal matrices.
	const float* diagPtr = matrix;
	i = 0;
	while (i<dimension-2)
	{

		mthAssertf(frictionMode[i]==-1 || frictionMode[i]==-2, "FrictionMode %d invalid (should be -1 or -2)", frictionMode[i]);
		if (frictionMode[i+1]!=i)
		{
	#if __ASSERT
			for ( ; i<dimension; i++ ) {		
				mthAssertf(frictionMode[i]==-1, "Only complementarity singletons allowed");
			}
	#endif
			break;
		}
		mthAssertf(frictionMode[i+2]==i+1, "FrictionMode %d invalid, expecting %d", frictionMode[i+2], i+1);

		int num3x3 = i/3;
		int diagInvPos = num3x3*9;
		//diagInverse[num3x3].SetRow1( *diagPtr, *(diagPtr+1), *(diagPtr+2) ); 
		diagInverse[diagInvPos] = *diagPtr;
		diagInverse[diagInvPos+1] = *(diagPtr+1);
		diagInverse[diagInvPos+2] = *(diagPtr+2); 
		diagPtr += dimension;
		//diagInverse[num3x3].SetRow2( *diagPtr, *(diagPtr+1), *(diagPtr+2) ); 
		diagInverse[diagInvPos+3] = *diagPtr;
		diagInverse[diagInvPos+4] = *(diagPtr+1);
		diagInverse[diagInvPos+5] = *(diagPtr+2); 
		diagPtr += dimension;
		//diagInverse[num3x3].SetRow3( *diagPtr, *(diagPtr+1), *(diagPtr+2) ); 
		diagInverse[diagInvPos+6] = *diagPtr;
		diagInverse[diagInvPos+7] = *(diagPtr+1);
		diagInverse[diagInvPos+8] = *(diagPtr+2); 
		diagPtr += dimension+3;

	#if DAMP_LCP==1
		float trace = Max(diagInverse[diagInvPos] + diagInverse[diagInvPos+4] + diagInverse[diagInvPos+8],SMALL_FLOAT);
		diagInverse[diagInvPos] += DAMP_LCP_LAMBDA2*trace;
		diagInverse[diagInvPos+4] += DAMP_LCP_LAMBDA2*trace;
		diagInverse[diagInvPos+8] += DAMP_LCP_LAMBDA2*trace;
	#endif

		// Inverts a symmetric, non-singular matrix.
		//mthAssertf(IsSymmetric(diagInverse[num3x3]), "diagInverse wasn't symmetric" );
		InvertPositiveDefinite(&diagInverse[diagInvPos],3);
		//InvertSym(&diagInverse[diagInvPos]);
		i += 3;
	}

	// Set up termination decisions.
	float solnMagSq;
	float deltaMagSq;
	float stopFracSq = square(stopFraction);
	float nearZeroSq = square(tolerance);
	int badDivergeCount = 0;	// Counts the number of times z,w are *much* worse

	int iterNum = 0;

	// Main iteration loop.
	float* z = zReturn;
	while (true) 
	{
		solnMagSq = 0.0f;
		deltaMagSq = 0.0f;
		int i = 0;
		const float* rowPtr = matrix;
		diagPtr = matrix;
		const float* qPtr = q;
		const int* fModePtr = frictionMode;
		const float* coefPtr = coefFric+1;
		zPtr = z;
		while (i<dimension-2 && (*(fModePtr+1))!=-1)
		{
			// Handle 3x3 block
			float r[3];
			r[0] = *(qPtr++);
			r[1] = *(qPtr++);
			r[2] = *(qPtr++);
			r[0] += InnerProduct( dimension-i-3, diagPtr+3, z+i+3 );
			diagPtr += (dimension+1);
			r[1] += InnerProduct( dimension-i-3, diagPtr+2, z+i+3 );
			diagPtr += (dimension+1);
			r[2] += InnerProduct( dimension-i-3, diagPtr+1, z+i+3 );
			diagPtr += (dimension+1);
			r[0] += InnerProduct( i, rowPtr, z );
			rowPtr += dimension;
			r[1] += InnerProduct( i, rowPtr, z );
			rowPtr += dimension;
			r[2] += InnerProduct( i, rowPtr, z );
			rowPtr += dimension;
			float oldF1 = *zPtr;
			float oldF2 = *(zPtr+1);
			float oldF3 = *(zPtr+2);
			r[0] = -r[0];
			r[1] = -r[1];
			r[2] = -r[2];

			if ( (*fModePtr)==-2 ) {
				// If no frictional constraints (hence, no complementarity)
				solnMagSq += square(oldF1)+square(oldF2)+square(oldF3);
				Transform3x3(&diagInverse[i*3],r,zPtr);
				deltaMagSq += square(oldF1-*zPtr)+square(oldF2-*(zPtr+1))+square(oldF3-*(zPtr+2));
			}
			else {				// Otherwise, handle frictional constraints (complementarity)
				if (r[0]>=0.0f)
				{
					// If trying to penetrate the surface:
					solnMagSq += square(oldF1)+square(oldF2)+square(oldF3);
					Solve3x3WithFriction(&diagInverse[i*3],*coefPtr,r,zPtr);
					deltaMagSq += square(oldF1-*zPtr)+square(oldF2-*(zPtr+1))+square(oldF3-*(zPtr+2));
				}
				else
				{
					// If leaving the surface, use zero force.
					float temp = square(oldF1)+square(oldF2)+square(oldF3);
					solnMagSq += temp;
					deltaMagSq += temp;
					(*zPtr) = (*(zPtr+1)) = (*(zPtr+2)) = 0.0f;
				}
			}

			//solnMagSq += square(oldF1)+square(oldF2)+square(oldF3);
			//Solve3x3WithFriction(&diagInverse[i*3],*coefPtr,r,zPtr);	// changed "i/3" to "i*3"
			//deltaMagSq += square(oldF1-*zPtr)+square(oldF2-*(zPtr+1))+square(oldF3-*(zPtr+2));
			zPtr += 3;
			fModePtr += 3;
			coefPtr += 3;
			i += 3;
		}

		while (i<dimension)
		{
			// Handle 1Dof value (no frictional limits)
			float r = *qPtr;
			r += InnerProduct( dimension-i-1, diagPtr+1, z+i+1 );
			r += InnerProduct( i, rowPtr, z );
			float oldF = *zPtr;
			float newF = Max(-r/(*diagPtr),0.0f);
			*(zPtr++) = newF;
			solnMagSq += oldF*oldF;
			deltaMagSq += (newF-oldF)*(newF-oldF);
			qPtr++;
			rowPtr += dimension;
			diagPtr += (dimension+1);
			i++;
		}

		// Check termination conditions.
		if ( iterNum>=1 )
		{
			if ( deltaMagSq < stopFracSq*solnMagSq || (solnMagSq < nearZeroSq && deltaMagSq < nearZeroSq) )
			{
				return;		// Converged well.  Return the solution
			}
			if ( iterNum>=maxIterations )
			{
				// Did not converge so well, but ran out of iterations.
				return;
			}
			if ( deltaMagSq > 1.0f*solnMagSq )
			{
				if ( ++badDivergeCount >= 3 )
				{
					// Diverging badly. (Or converging to zero erratically.)  Give up. Return zero.
					for ( i=0; i<dimension; i++ )
					{
						*(z+i) = 0.0f;
					}

					return;
				}
			}
		}

		iterNum++;
	}
}


int Sh3x3_FindNext (const float* deltaR, const int* frictionMode, int numTriples, int numSingles, int& diagInvPos)
{
	int bestJ = -1;
	float largestRsq = 0.0f;
	const float* r = deltaR;
	int i;
	int j=0;
	int numTriplesPassed = 0;
	int bestNumTriplesPassed = 0;
	int numTriplesPlusSingles = numTriples+numSingles;
	int dimension = 3*numTriples+numSingles;
	for (i=0; i<numTriplesPlusSingles; i++)
	{
		if (j<dimension-2 && frictionMode[j+1]==j)
		{
			// Compute a vector magnitude squared.
			mthAssertf(frictionMode[j+2]==j+1, "Incorrect friction mode %d, expecting %d", frictionMode[j+2], j+1);
			float newSq = (*r)*(*r) + (*(r+1))*(*(r+1)) + (*(r+2))*(*(r+2));
			if ( newSq>largestRsq ) {
				bestJ = j;
				bestNumTriplesPassed = numTriplesPassed;
				largestRsq = newSq;
			}
			j += 3;
			r += 3;
			numTriplesPassed += 9;
		}
		else
		{
			float newSq = (*r)*(*r);
			if ( newSq>largestRsq ) {
				bestJ = j;
				largestRsq = newSq;
			}
			j++;
			r++;	
		}
	}

	diagInvPos = bestNumTriplesPassed;
	return bestJ;
}


// Store the delta values for the shooting update
//float R[MAX_DIM];
//float deltaR[MAX_DIM];
//int dimension, const float* matrix, const float* q, const int* frictionMode,
//const float* coefFric, float* zReturn, float* diagInverse, float* tempArray,
//float* delTempArray, float stopFraction, float zeroTolerance, 
//int maxIterations
void SolveMlcpFriction3Sh3x3 (int dimension, const float* matrix, const float*q, const int* frictionMode,
								const float* coefFric, float* zReturn, float* diagInverse, float* tempArray,
								float* delTempArray, float stopFraction, float zeroTolerance, int maxIterations)
{
	mthAssertf(CheckPositiveDiagonal(dimension,matrix), "Matrix should have all positive diagonal elements");

	// Initialize by  setting z equal to zero.  (Just in case.)
	//	At the same time, set the deltaR values equal to the q vector
	float* zPtr = zReturn;
	float* rPtr = tempArray;
	float* drPtr = delTempArray;
	const float* qPtr = q;
	int i;
	for ( i=0; i<dimension; i++ )
	{
		*(zPtr++) = 0.0f;
		*(rPtr++) = 0.0f;
		*(drPtr++) = *(qPtr++);
	}

	// Setup the inverses of the diagonal matrices.
	// Also get the count of 3x3 parts and 1x1 parts
	int numTriples = 0;
	int numSingles = 0;
	i = 0;
	const float * diagPtr = matrix;
	int diagInvPos = 0;
	while (i<dimension-2)
	{
		mthAssertf(frictionMode[i]==-1 || frictionMode[i]==-2, "Invalid friction mode %d (should be -1 or -2)", frictionMode[i]);
		if (frictionMode[i+1]==i)
		{
			// This is a 3D contact.
			mthAssertf(frictionMode[i+2]==i+1, "Invalid friction mode %d, expecting %d", frictionMode[i+2], i+1);
			numTriples++;
			//diagInverse[num3x3].SetRow1( *diagPtr, *(diagPtr+1), *(diagPtr+2) ); 
			diagInverse[diagInvPos] = *diagPtr;
			diagInverse[diagInvPos+1] = *(diagPtr+1);
			diagInverse[diagInvPos+2] = *(diagPtr+2);
			diagPtr += dimension;
			//diagInverse[num3x3].SetRow2( *diagPtr, *(diagPtr+1), *(diagPtr+2) ); 
			diagInverse[diagInvPos+3] = *diagPtr;
			diagInverse[diagInvPos+4] = *(diagPtr+1);
			diagInverse[diagInvPos+5] = *(diagPtr+2);
			diagPtr += dimension;
			//diagInverse[num3x3].SetRow3( *diagPtr, *(diagPtr+1), *(diagPtr+2) ); 
			diagInverse[diagInvPos+6] = *diagPtr;
			diagInverse[diagInvPos+7] = *(diagPtr+1);
			diagInverse[diagInvPos+8] = *(diagPtr+2);
			diagPtr += dimension+3;
			// Inverts a symmetric, non-singular matrix.
			// assert( IsSymmetric(diagInverse[num3x3]) );
			//diagInverse[num3x3].InvertSym();

		#if DAMP_LCP==1
			float trace = Max(diagInverse[diagInvPos] + diagInverse[diagInvPos+4] + diagInverse[diagInvPos+8],SMALL_FLOAT);
			diagInverse[diagInvPos] += DAMP_LCP_LAMBDA2*trace;
			diagInverse[diagInvPos+4] += DAMP_LCP_LAMBDA2*trace;
			diagInverse[diagInvPos+8] += DAMP_LCP_LAMBDA2*trace;
		#endif

			InvertPositiveDefinite(&diagInverse[diagInvPos],3);
			//InvertSym(&diagInverse[diagInvPos]);

			// Increase the index by 3 to the next contact.
			i += 3;
			diagInvPos += 9;
		}
		else
		{
			// This is a 1D contact (a non-constraint contact calculating pushes, or a joint degree of freedom).
			i++;
			diagPtr += dimension+1;
		}
	}

	// Set the number of 1-dimensional contacts.
	numSingles = dimension - 3*numTriples;

	// Set up termination decisions.
	float stopFracSq = square(stopFraction);
	float nearZeroSq = square(zeroTolerance);
	int badDivergeCount = 0;	// Counts the number of times z,w are *much* worse

	int iterNum = 0;
	int stopIterations = maxIterations*(numTriples+numSingles);
	float solnMagSq = 0.0f;
	float deltaMagSq = 0.0f;
	// Main iteration loop.
	while ( true ) 
	{
		// Find max value in deltaR array (max. velocity)
		int j = Sh3x3_FindNext(delTempArray,frictionMode,numTriples,numSingles,diagInvPos);
		if ( j<0 )
		{
			return;				// No updates left to make.
		}
		rPtr = &tempArray[j];
		drPtr = &delTempArray[j];
		zPtr = zReturn+j;
		deltaMagSq = 0.0f;		// THIS LINE CHANGED: MAY 9, 2005
		if ( j<dimension-1 && frictionMode[j+1]==j ) {
			// Case A: A 3x3 update
			mthAssertf(frictionMode[j+2]==j+1, "Invalid friction mode %d, expecting %d", frictionMode[j+2], j+1);
			solnMagSq = square(*rPtr) + square(*(rPtr+1)) + square(*(rPtr+2));
		//	deltaMagSq = (*(drPtr))*(*(drPtr)) + (*(drPtr+1))*(*(drPtr+1)) + (*(drPtr+2))*(*(drPtr+2));		// THIS LINE CHANGED: MAY 9, 2005
			*rPtr += *drPtr;
			*(rPtr+1) += *(drPtr+1);
			*(rPtr+2) += *(drPtr+2);
			*drPtr = *(drPtr+1) = *(drPtr+2) = 0.0f;
			float deltaZ1 = *zPtr;
			float deltaZ2 = *(zPtr+1);
			float deltaZ3 = *(zPtr+2);
			if ( frictionMode[j] == -2 ) {
				// Handle case of no complementarity conditions
		//		Vector3 f( -(*rPtr), -(*(rPtr+1)), -(*(rPtr+1)) );
				// Transform the vector (v1, v2, v3) by matrixInv;
		//		diagInverse[j*3].Transform( &f );
		//		*zPtr = f.x;
		//		*(zPtr+1) = f.y;
		//		*(zPtr+2) = f.z;
				Transform3x3AndNegate(&diagInverse[diagInvPos],rPtr,zPtr);
			}
			else {
				mthAssertf( frictionMode[j] == -1, "Invalid friction mode %d, expecting -1", frictionMode[j] );
				// Handle complementarity, including frictional force
				if ( (*rPtr)<=0 ) {
					// If trying to penetrate the surface:
					float r[3];
					r[0] = -(*rPtr);
					r[1] = -(*(rPtr+1));
					r[2] = -(*(rPtr+2));
				//	Solve3x3WithFriction(&diagInverse[j*3], coefFric[j+1], 
				//						  -(*rPtr), -(*(rPtr+1)), -(*(rPtr+1)), zPtr );
					Solve3x3WithFriction(&diagInverse[diagInvPos],coefFric[j+1],r,zPtr);
				}
				else {
					// If leaving the surface,use zero force.
					(*zPtr) = *(zPtr+1) = *(zPtr+2) = 0.0f;
				}
			}
			// Change in the Z values
			deltaZ1 = *zPtr - deltaZ1;
			deltaZ2 = *(zPtr+1) - deltaZ2;
			deltaZ3 = *(zPtr+2) - deltaZ3;
			
			// Update the other deltaR values
			const float* mPtr = matrix+j;
			rPtr = tempArray;
			drPtr = delTempArray;
			for (i=0; i<dimension; i++)
			{
				if (i!=j)
				{
					solnMagSq += square(*rPtr);
					float t = (*mPtr)*deltaZ1 + (*(mPtr+1))*deltaZ2 + (*(mPtr+2))*deltaZ3;
					*drPtr += t;
					deltaMagSq += square(*drPtr);		// THIS LINE CHANGED: MAY 9, 2005
					mPtr += dimension;
					rPtr++;
					drPtr++;
				}
				else
				{
					i += 2;
					mPtr += 3*dimension;
					rPtr += 3;
					drPtr += 3;	
				}
			}

	/*		for ( i=0; i<j; i++, mPtr+=dimension, rPtr++, drPtr++ ) {
				solnMagSq += (*rPtr)*(*rPtr);
				float t = (*mPtr)*deltaZ1 + (*(mPtr+1))*deltaZ2 + (*(mPtr+2))*deltaZ3;
				*drPtr += t;
				deltaMagSq += (*drPtr)*(*drPtr);		// THIS LINE CHANGED: MAY 9, 2005
			}
			i += 3;				// Skip next two rows to avoid self-updates
			mPtr += 3*dimension;
			rPtr += 3;
			drPtr += 3;	
			for ( ; i<dimension; i++, mPtr+=dimension, rPtr++, drPtr++ ) {
				solnMagSq += (*rPtr)*(*rPtr);
				float t = (*mPtr)*deltaZ1 + (*(mPtr+1))*deltaZ2 + (*(mPtr+2))*deltaZ3;
				*drPtr += t;
				deltaMagSq += (*drPtr)*(*drPtr);		// THIS LINE CHANGED: MAY 9, 2005
			}*/
		}
		else {
			// Case B: A 1x1 update
			solnMagSq = (*(rPtr))*(*(rPtr));
			*rPtr += *drPtr;
		//	deltaMagSq = (*(drPtr))*(*(drPtr));		// THIS LINE CHANGED: MAY 9, 2005
			*drPtr = 0.0f;
			float deltaZ = *zPtr;
			int inputMatrixIndex = (dimension+1)*j;
			if (fabsf(matrix[inputMatrixIndex])>VERY_SMALL_FLOAT)
			{
				*zPtr = -(*rPtr)/matrix[inputMatrixIndex];
				if (*zPtr<0.0f)
				{
					*zPtr = 0.0f;
				}
			}
			else
			{
				*zPtr = 0.0f;
			}

			deltaZ = *zPtr - deltaZ;

			// Update the other deltaR values
			const float* mPtr = matrix+j;
			rPtr = tempArray;
			drPtr = delTempArray;
			for ( i=0; i<dimension; i++)
			{
				if (i!=j)
				{
					solnMagSq += square(*rPtr);
					float t = (*mPtr)*deltaZ;
					*drPtr += t;
					deltaMagSq += square(*drPtr);		// THIS LINE CHANGED: MAY 9, 2005
				}

				mPtr+=dimension;
				rPtr++;
				drPtr++;
			}
		/*	for ( i=0; i<j; i++, mPtr+=dimension, rPtr++, drPtr++ ) {
				solnMagSq += (*rPtr)*(*rPtr);
				float t = (*mPtr)*deltaZ;
				*drPtr += t;
				deltaMagSq += (*drPtr)*(*drPtr);		// THIS LINE CHANGED: MAY 9, 2005
			}
			i++;
			mPtr+=dimension;
			rPtr++;
			drPtr++;
			for ( ; i<dimension; i++, mPtr+=dimension, rPtr++, drPtr++ ) {
				solnMagSq += (*rPtr)*(*rPtr);
				float t = (*mPtr)*deltaZ;
				*drPtr += t;
				deltaMagSq += (*drPtr)*(*drPtr);		// THIS LINE CHANGED: MAY 9, 2005
			}*/
		}

		// Check termination conditions.
		if ( iterNum > 0 ) {
			if ( deltaMagSq < stopFracSq*solnMagSq ||
						(solnMagSq < nearZeroSq && deltaMagSq < nearZeroSq) )
			{
				break;		// Converged well.  Return the solution
			}
			if ( iterNum>=stopIterations ) {
				// Did not converge so well, but ran out of iterations.
				break;
			}
			if ( deltaMagSq > 1.0f*solnMagSq ) {
				if ( ++badDivergeCount >= 3*dimension ) {
					// Diverging badly. (Or converging to zero erratically.)  Give up. Return zero.
					for ( i=0; i<dimension; i++ ) {
						*(zReturn+i) = 0.0f;
					}
					return;
				}
			}
		}
		iterNum++;
	}

	// One last update to bring some closure 
	//			and clean up any remaining convergence problems
	// Uses diagonal blocks only
	const int* fPtr = frictionMode;
	rPtr = tempArray;
	drPtr = delTempArray;
	zPtr = zReturn;
	const float* cfPtr = coefFric+1;
	// Nathan: Code is simplified repeat of functionality from above.  
	//		Doing about the same thing but without tracking magnitudes.
	//		Note that i here equals j/3, where j is used above.
	const float* mPtr = matrix;
	int numTriplesPlusSingles = numTriples+numSingles;
	diagInvPos = 0;
	int j = 0;
	for ( i=0; i<numTriplesPlusSingles; i++)
	{
		if (j<dimension-1 && frictionMode[j+1]==j)
		{
			mthAssertf(frictionMode[j+2]==j+1, "Invalid friction mode %d, expecting %d", frictionMode[j+2], j+1);
			*rPtr += *drPtr;
			*(rPtr+1) += *(drPtr+1);
			*(rPtr+2) += *(drPtr+2);
			if ( *fPtr == -2 ) {
				// Handle case of no complementarity conditions
			//	Vector3 f( -(*rPtr), -(*(rPtr+1)), -(*(rPtr+1)) );
			// Transform the vector (v1, v2, v3) by matrixInv;
			//	diagInverse[i].Transform( &f );
			//	*zPtr = f.x;
			//	*(zPtr+1) = f.y;
			//	*(zPtr+2) = f.z;
				Transform3x3AndNegate(&diagInverse[diagInvPos],rPtr,zPtr);
			}
			else {
				mthAssertf ( *fPtr == -1, "Invalid friction mode %d, expecing -1", *fPtr );
				// Handle complementarity, including frictional force
				if ( (*rPtr)<=0 ) {
					// If trying to penetrate the surface:
					float r[3];
					r[0] = -(*rPtr);
					r[1] = -(*(rPtr+1));
					r[2] = -(*(rPtr+2));
				//	Solve3x3WithFriction( &diagInverse[i*9], *cfPtr, 
				//						  -(*rPtr), -(*(rPtr+1)), -(*(rPtr+1)), zPtr );
					Solve3x3WithFriction( &diagInverse[diagInvPos], *cfPtr, r, zPtr );
				}
				else {
						// If leaving the surface, use zero force.
					(*zPtr) = *(zPtr+1) = *(zPtr+2) = 0.0f;
				}
			}

			fPtr += 3;
			zPtr += 3;
			rPtr += 3;
			drPtr += 3;
			cfPtr += 3;
			mPtr += 3*(dimension+1);
			diagInvPos += 9;
			j += 3;
		}
		else
		{
			*rPtr += *drPtr;
			if (fabsf(*mPtr)>VERY_SMALL_FLOAT)
			{
				*zPtr = -(*rPtr)/(*mPtr);
				if ( *zPtr < 0.0f )
				{
					*zPtr = 0.0f;
				}
			}
			else
			{
				*zPtr = 0.0f;
			}

			fPtr++;
			zPtr++;
			rPtr++;
			drPtr++;
			cfPtr++;
			mPtr += (dimension+1);
			j++;
		}
	}
}



#if SPARSE_SHOOTING==1
//
// MODIFIED LCP FRICTION SOLVER using SHOOTING METHOD
// Initial author: Sam Buss (SB), June 2004.
//			Modified: January 2005.  SB
//			March 2005.  The "3GS3x3" version. SB
//				With (a) uses 3x3 solver
//					 (b) new conventions on stopping criteria
//					 (c) no longer permits single axis for friction.
//			April-May 2005.  SB
//					 (a) Converted to a shooting method.
//					 (b) Implemented a sparse shooting method

// ***********************************************************
// Function prototypes
//  "3" = New conventions on stopping criteria.
//	"GS" = "Gauss-Seidel"
//  "J" = "Jacobian"
//  "GS3x3" - Gauss-Seidel with 3x3 blocks
//  "Sh3x3" - Shooting method with 3x3 blocks
//  "Sparse" - Means sparse update.
// ***********************************************************

// ***********************************************************
// SolveMlcpFriction3Sh3x3Sparse
//    Solve the Modified Linear Complementarity Problem with special
//		complementary conditions that apply to frictional
//		forces.  Frictional forces are done in pairs, taking
//		into account both tangential directions at once.
//    This version uses a projected iterative shooting method
//		based on splitting.
//	  The lower triangular matrix includes 3x3 blocks from the 
//		diagonal.
//	  All 3x3 items MUST come first in the array, followed
//		by all 1x1 items.  In practice, this means that 
//		contacts must have 3 components, normal and then two
//		tangential.  All contacts come first, then come all
//		1DOF values (namely, from 1Dof or 3Dof joints:  3Dof
//		joints are treated as three separate 1Dof values.)
//		Linear constraints (with out complementarity conditions)
//		may be included with the 3x3 items and must (a) come beore
//		all 1Dof items, (b) come in 3x3 versions.
//
//  n - dimension of the LCP problem.
//  M - an n x n matrix, diagonal entries MUST be positive!
//		Entries in M are given in *row* order.
//  q - an column vector of length n
//  FrictionalMode - see below.  Identifies frictional forces
//	CoefFric - array of n coefficients of friction
//  z - also a column vectors of length n (returned values)
//  stopFraction - a stopping criterion (in terms of root mean
//				square error.  At the second iteration onwards,
//				the change in the solution (as a vector) is less
//				than fraction "stopFraction" of the solution, then
//				the iteration terminates.
//	zeroTolerance.  If the magnitude of the solution is less than
//		this, it is deemed to be close enough to zero to count as zero.
//  maxIterations - a backup stopping criterion.
//  There is a further stopping criterion which is
//		automatically applied if the iterations fail
//		to converge to a better solution on at least two tries.
//
// ******************************
//
//  An array CoefFric[] gives the coefficient of friction for
//	each direction.  That is, CoefFric[i] gives the coefficient
//	of friction for the force/acceleration (or impulse/velocity)
//	of the i-th components of force/acceleration.
//
//  An array FrictionalMode[i] describes which contacts are tengential
//	frictional forces.  There are two permitted usages -- all occurences
//  the first usage MUST come before any occurance of the second usage.
//
// Usage #1:  A normal force and two tangential forces.  They must obey a
//		a circular frictional restriction.

//		FrictionalMode[i] = -1   -- this force and acceleration is *normal*
//									CoefFric[i] is irrelevant.
//		FrictionalMode[i+1] = i    -- this force and acceleration is *tangential*
//									-- it corresponds to the normal force # i
//									-- it uses CoefFric[j].
//		FrictionalMode[j+1] = i+1  -- this force and acceleration is *tangential*
//									-- it corresponds to the normal force # i
//									-- it also uses CoefFric[j].
//		The normal force and the two tangential forces are presumed to be orthogonal.
//		(The format is a little strange, but is designed to be backward
//		compatible with the "2GS" and "2J" methods.
//
// Usage #2: A single force (no frictional limits), e.g. a 1Dof joint, or one
//		component of a 3Dof joint.
//
// Usage #3: FrictionalMode[i] = -2  - First of three forces with no complementarity
//										conditions.
//			 FrictionalMode[i+1] = i;
//			 FrictionalMode[i+2] = i+1;
//
// ***********************************************************

// Store the delta values for the shooting update
#define	MAX_DIM	2048

// Holds the (negative of the) magnitudes of the deltaRsq values.
atPriorityArray<float,MAX_DIM> deltaRsq_p;

static int s_sync = 1;

// Store the inverses of diagonal 3x3 blocks
// This might be better made a member of the phConstraintGraph
// The correct size for it is to be big enouch to hold
//		(a) all inverses of 3x3 diagonal matrices
//		(b) all inverses of 1x1 diagonal entries

void SolveMlcpFriction3Sh3x3Sparse (const phConstraintGraph& theSparseMatrixGraph, const float* q, float* zReturn, 
									float* R, float* deltaR, float* diagInverse, float stopFraction, float zeroTolerance, int maxIterations)
{
	int n = theSparseMatrixGraph.m_nConstraint;		// The number of constraints/contacts
	mthAssertf( n<=MAX_DIM, "%dx%d matrix is too big, max is %d", n, n, MAX_DIM );
	float* z = zReturn;

	// Initialize by  setting z equal to zero.  (Just in case.)
	//	At the same time, set the deltaR values equal to the q vector.
	float* zPtr = z;
	float* rPtr = R;
	float* drPtr = deltaR;
	const float* qPtr = q;
	int i;
	for ( i=theSparseMatrixGraph.GetNumDofs(); i>0; i-- ) {
		*(zPtr++) = 0.0;
		*(rPtr++) = 0.0;
		*(drPtr++) = *(qPtr++);
	}
	
	// Setup the inverses of the diagonal matrices.
	for ( i=0; i<n; i++ )  {
		const phConstraintGraphEdge& thisEdge = theSparseMatrixGraph.m_edges[i];
		const float* diagJac = theSparseMatrixGraph.GetJacobianDiagonal( i );
		if ( thisEdge.m_nDof==1 ) {
			// NATHAN: This value could be stored in a "jacobian inverse" array in the phConstraintGraph
		//	diagInverse[i].m11 = 1.0/(*diagJac);	
			diagInverse[i*9] = 1.0f/(*diagJac);	
		}
		else {
			mthAssertf ( thisEdge.m_nDof==3, "Degrees of freedom should be 1 or 3, not %d", thisEdge.m_nDof );
			// NATHAN: This matrix could be stored in the same "jacobian inverse" array in the phConstraintGraph
			for (int index=0; index<9; index++)
			{
				diagInverse[i*9+index] = diagJac[index];
			}
		//	diagInverse[i].LoadByRows( diagJac ); 
			// Inverts a symmetric, non-singular matrix.
			// assert( IsSymmetric(diagInverse[num3x3]) );
			InvertPositiveDefinite(&diagInverse[i*9],3);
			//InvertSym(&diagInverse[i*9]);
		//	diagInverse[i].InvertSym();
		}
	}

	//  Initialize the priority queue of deltaR values
	//  Also, set up the solnMagSq and deltaMagSq values.
	deltaRsq_p.InitValues( n, 0.0 );
	float solnMagSq = 0.0;
	float deltaMagSq = 0.0;
	const phConstraintGraphEdge* thisEdgePtr = &(theSparseMatrixGraph.m_edges[0]);
	for ( i=0; i<n; i++, thisEdgePtr++ ) {
		float* drPtr = deltaR + thisEdgePtr->m_iX;
		float v;
		if ( thisEdgePtr->m_nDof==3 ) {
			v = (*drPtr)*(*drPtr) + (*(drPtr+1))*(*(drPtr+1)) + (*(drPtr+2))*(*(drPtr+2));
		}
		else {
			v = (*drPtr)*(*drPtr);
		}
		deltaMagSq += v;
		deltaRsq_p.LowerValue(i, -v);
	}

	// Set up termination decisions variables.
	float stopFracSq = stopFraction*stopFraction;
	float nearZeroSq = zeroTolerance*zeroTolerance;
	int badDivergeCount = 0;	// Counts the number of times z,w are *much* worse

	int iterNum = 0;

	// Main iteration loop.
	while ( true ) 
	{
		// Find max value in deltaR array (max. velocity)
		int i;
		float d = deltaRsq_p.GetHead(&i);			// Get the min value in the priority queue
		if ( d==0.0 ) {
			break;									// No more updates left to do (rare event)
		}
		deltaMagSq -= -deltaRsq_p[i];				// Adjust magnitude of delta R vector
		deltaRsq_p.RaiseValue(i, 0.0);

		const phConstraintGraphEdge& thisEdge = theSparseMatrixGraph.m_edges[i];
		int j = thisEdge.m_iX;						// Index into solution
		rPtr = &R[j];
		drPtr = &deltaR[j];
		zPtr = z+j;
		if ( thisEdge.m_nDof==3 ) {
			// Case A: A 3x3 update
			// Change in SolnMagSq;
			solnMagSq -= (*rPtr)*(*rPtr) + (*(rPtr+1))*(*(rPtr+1)) + (*(rPtr+2))*(*(rPtr+2));
			*rPtr += *drPtr;
			*(rPtr+1) += *(drPtr+1);
			*(rPtr+2) += *(drPtr+2);
			solnMagSq += (*rPtr)*(*rPtr) + (*(rPtr+1))*(*(rPtr+1)) + (*(rPtr+2))*(*(rPtr+2));
			*drPtr = *(drPtr+1) = *(drPtr+2) = 0.0;
			float deltaZ1 = *zPtr;
			float deltaZ2 = *(zPtr+1);
			float deltaZ3 = *(zPtr+2);
			if ( thisEdge.m_type==CONSTRAINT_TYPE_3DOF_NONCOMPL ) {
				// Handle case of no complementarity conditions
				Vector3 f( -(*rPtr), -(*(rPtr+1)), -(*(rPtr+2)) );
				// Transform the vector (v1, v2, v3) by matrixInv;
		//		diagInverse[i].Transform( &f );
				Vector3 fOld(f); // @@@@@@ matrix times column vector
				f.x = diagInverse[i*9+0]*fOld.x+diagInverse[i*9+1]*fOld.y+diagInverse[i*9+2]*fOld.z;
				f.y = diagInverse[i*9+3]*fOld.x+diagInverse[i*9+4]*fOld.y+diagInverse[i*9+5]*fOld.z;
				f.z = diagInverse[i*9+6]*fOld.x+diagInverse[i*9+7]*fOld.y+diagInverse[i*9+8]*fOld.z;
				*zPtr = f.x;
				*(zPtr+1) = f.y;
				*(zPtr+2) = f.z;
			}
			else {
				mthAssertf( thisEdge.m_type==CONSTRAINT_TYPE_3DOFCONTACT, "Type should be CONSTRAINT_TYPE_3DOF_NONCOMPL or CONSTRAINT_TYPE_3DOFCONTACT, got %d", thisEdge.m_type );
				// Handle complementarity, including frictional force
				if ( (*rPtr)<=0 ) {
					// If trying to penetrate the surface:
					//	thisEdge.m_k is the friction value
					float temp[3];
					temp[0] = -(*rPtr);
					temp[1] = -(*(rPtr+1));
					temp[2] = -(*(rPtr+2));
				//	Solve3x3WithFriction( &diagInverse[i*9], thisEdge.m_k, 
				//						  -(*rPtr), -(*(rPtr+1)), -(*(rPtr+2)), zPtr );
					Solve3x3WithFriction( &diagInverse[i*9], thisEdge.m_k, 
										  temp, zPtr );
				}
				else {
					// If leaving the surface,use zero force.
					(*zPtr) = *(zPtr+1) = *(zPtr+2) = 0.0;
				}
			}
			// Change in the Z values
			deltaZ1 = *zPtr - deltaZ1;
			deltaZ2 = *(zPtr+1) - deltaZ2;
			deltaZ3 = *(zPtr+2) - deltaZ3;

			// Update the adjoining deltaR values
			// For body A, then for body B, 
			//	Loop over all constraints/contacts that involve the body
			s_sync++;
			for ( int iBody=0; iBody<2; iBody++ ) {
				const phConstraintGraphNode& thisBody = theSparseMatrixGraph.m_nodes[thisEdge.m_bodies[iBody]];
				int numCons = thisBody.m_numEdges;
				for ( int k=0; k<numCons; k++ ) {
					const phConstraintGraphEdge* affectedEdgePtr=thisBody.m_edges[k];
					if ( affectedEdgePtr->m_sync==s_sync ) {
						continue;
					}
					affectedEdgePtr->m_sync = s_sync;
					int thisEdgeNum = affectedEdgePtr - &(theSparseMatrixGraph.m_edges[0]);
					if( i==thisEdgeNum ) {
						continue;			// Do not re-do self-influence
					}
					int affDOF = affectedEdgePtr->m_nDof;
					float* drPtr = deltaR + affectedEdgePtr->m_iX;
					const float* theJacP = theSparseMatrixGraph.GetJacobian(thisEdgeNum,i);
					float newDeltaRsq = 0.0;
					// Step from one row of Jacobian to the next:
					int jacRowStep = 3;	// Use n_dim for Ewert's version
					for ( int ell=0; ell<affDOF; ell++, theJacP+=jacRowStep, drPtr++ ) {
						*drPtr += (*theJacP)*deltaZ1 + (*(theJacP+1))*deltaZ2 + (*(theJacP+2))*deltaZ3;
						newDeltaRsq += (*drPtr)*(*drPtr);
					}
					deltaMagSq -= -deltaRsq_p[thisEdgeNum];
					deltaRsq_p.SetValue(thisEdgeNum, -newDeltaRsq );
					deltaMagSq += newDeltaRsq;
				}
			}

		}
		else {
			// Case B: A 1x1 update
			mthAssertf( thisEdge.m_nDof==1, "Degrees of freedom should be 1 or 3, not %d", thisEdge.m_nDof );
			mthAssertf( thisEdge.m_type==CONSTRAINT_TYPE_1DOFCONTACT, "Only CONSTRAIN_TYPE_1DOFCONTACT is valid for 1-dof edges, not %d", thisEdge.m_type );
			solnMagSq -= (*rPtr)*(*rPtr);
			*rPtr += *drPtr;
			solnMagSq += (*rPtr)*(*rPtr);
			*drPtr = 0.0;
			float deltaZ = *zPtr;
		//	*zPtr = -(*rPtr)*diagInverse[i].m11;
			*zPtr = -(*rPtr)*diagInverse[i*9];	// @@@@@@@
			if ( *zPtr <= 0.0 ) {
				*zPtr = 0.0;
			}
			deltaZ = (*zPtr) - deltaZ;

			// Update the adjoining deltaR values
			// For body A, then for body B, 
			//	Loop over all constraints/contacts that involve the body
			s_sync++;
			for ( int iBody=0; iBody<2; iBody++ ) {
				const phConstraintGraphNode& thisBody = theSparseMatrixGraph.m_nodes[thisEdge.m_bodies[iBody]];
				int numCons = thisBody.m_numEdges;
				for ( int k=0; k<numCons; k++ ) {
					const phConstraintGraphEdge* affectedEdgePtr=thisBody.m_edges[k];
					if ( affectedEdgePtr->m_sync==s_sync ) {
						break;
					}
					affectedEdgePtr->m_sync = s_sync;
					int thisEdgeNum = affectedEdgePtr - &(theSparseMatrixGraph.m_edges[0]);
					if( i==thisEdgeNum ) {
						continue;			// Do not re-do self-influence
					}
					int affDOF = affectedEdgePtr->m_nDof;
					float* drPtr = deltaR + affectedEdgePtr->m_iX;
					const float* theJacP = theSparseMatrixGraph.GetJacobian(thisEdgeNum,i);
					float newDeltaRsq = 0.0;
					// Step from one row of Jacobian to the next:
					int jacRowStep = 1;	// Use n_dim for Ewert's version
					for ( int ell=0; ell<affDOF; ell++, theJacP+=jacRowStep, drPtr++ ) {
						*drPtr += (*theJacP)*deltaZ;
						newDeltaRsq += (*drPtr)*(*drPtr);
					}
					deltaMagSq -= -deltaRsq_p[thisEdgeNum];
					deltaRsq_p.SetValue(thisEdgeNum, -newDeltaRsq );
					deltaMagSq += newDeltaRsq;
				}
			}
		}

		// Check termination conditions.
		if ( iterNum > 0 ) {
			if ( deltaMagSq < stopFracSq*solnMagSq ||
						(solnMagSq < nearZeroSq && deltaMagSq < nearZeroSq) )
			{
				break;		// Converged well.  Return the solution
			}
			if ( iterNum>=maxIterations*n ) {
				// Did not converge so well, but ran out of iterations.
				break;
			}
			if ( deltaMagSq > 1.0*solnMagSq ) {
				if ( ++badDivergeCount >= 3*n ) {
					// Diverging badly. (Or converging to zero erratically.)  Give up. Return zero.
					for ( i=0; i<n; i++ ) {
						*(z+i) = 0.0;
					}
					return;
				}
			}
		}
		iterNum++;
	}

	// One last update to bring some closure 
	//			and clean up any remaining convergence problems
	//		Uses diagonal blocks only
	// Code is simplified repeat of functionality from above.  
	//		Doing about the same thing but no propogation of updates.
	thisEdgePtr = &(theSparseMatrixGraph.m_edges[0]);
	for ( i=0; i<n; i++, thisEdgePtr++ ) {
		int j = thisEdgePtr->m_iX;						// Index into solution
		rPtr = &R[j];
		drPtr = &deltaR[j];
		zPtr = z+j;
		if ( thisEdgePtr->m_nDof==3 ) {
			// Case A: A 3x3 update
			*rPtr += *drPtr;
			*(rPtr+1) += *(drPtr+1);
			*(rPtr+2) += *(drPtr+2);
			if ( thisEdgePtr->m_type==CONSTRAINT_TYPE_3DOF_NONCOMPL ) {
				// Handle case of no complementarity conditions
				Vector3 f( -(*rPtr), -(*(rPtr+1)), -(*(rPtr+2)) );
				// Transform the vector (v1, v2, v3) by matrixInv;
			//	diagInverse[i].Transform( &f );
				Vector3 fOld(f); // @@@@@@
				f.x = diagInverse[i*9+0]*fOld.x+diagInverse[i*9+1]*fOld.y+diagInverse[i*9+2]*fOld.z;
				f.y = diagInverse[i*9+3]*fOld.x+diagInverse[i*9+4]*fOld.y+diagInverse[i*9+5]*fOld.z;
				f.z = diagInverse[i*9+6]*fOld.x+diagInverse[i*9+7]*fOld.y+diagInverse[i*9+8]*fOld.z;
				*zPtr = f.x;
				*(zPtr+1) = f.y;
				*(zPtr+2) = f.z;
			}
			else {
				// Handle complementarity, including frictional force
				if ( (*rPtr)<=0 ) {			// If trying to penetrate the surface:
					float temp[3];
					temp[0] = -(*rPtr);
					temp[1] = -(*(rPtr+1));
					temp[2] = -(*(rPtr+2));
					Solve3x3WithFriction( &diagInverse[i*9], thisEdgePtr->m_k, // @@@@@@@@
										  temp, zPtr );
				}
				else {					// If leaving the surface,use zero force.
					(*zPtr) = *(zPtr+1) = *(zPtr+2) = 0.0;
				}
			}
		}
		else {
			// Case B: A 1x1 update
			*rPtr += *drPtr;
			*zPtr = -(*rPtr)*diagInverse[i*9];//@@@.m11;
			if ( *zPtr <= 0.0 ) {
				*zPtr = 0.0;
			}
		}
	}
}
#endif // end of #if SPARSE_SHOOTING==1


void Solve2DFriction( int i, float r, int dimension, const float* mRowPtr, float fricLimit, const float* solution, const float* rhs, float& f1, float& f2, bool noTemps )
{
		// If two tangential forces to be handled together

	const float* mDiagPtr = mRowPtr + i;

	float a = *mDiagPtr;
	float b = *(mDiagPtr+1);
	mDiagPtr += dimension;
	float c = *(mDiagPtr++);
	float d = *mDiagPtr;

	//Make sure the matrix is symmetric.
	//DebugAssert(fabsf(b-c)<1.0e-3f*Max(fabsf(b),fabsf(c),1.0f));

	mRowPtr += dimension;
	float s = *(rhs+i+1) + InnerProduct(i,mRowPtr,solution);
	
	if( noTemps )
	{
		s += InnerProduct( dimension-i-2, mRowPtr+i+2, solution+i+2 );
	}

	mthAssertf(fabsf(a*d-b*c)>VERY_SMALL_FLOAT, "Determinant is zero, can't solve matrx");
	float determinantInv = 1.0f/(a*d-b*c);
	f1 = (s*b-r*d)*determinantInv;		// Solve with Cramer's rule
	f2 = (r*c-s*a)*determinantInv;


#if OVERRELAX
	if( g_overrelax )
	{
		float oldF1 = *(solution+i);
		float oldF2 = *(solution+i+1);
		f1 = (1.0f-omega)*oldF1 + omega*f1;
		f2 = (1.0f-omega)*oldF2 + omega*f2;
	}
#endif

	float normSq = square(f1)+square(f2);
	if (normSq>square(fricLimit))
	{

#if !TRY_FOR_COLINEAR
		float scaleFactor = fricLimit/sqrtf(normSq);
		f1 *= scaleFactor;
		f2 *= scaleFactor;
#else
		if (!firstTimeFlag)
		{
			f1 = tempDelVector[i];	// Use previous velocity direction for first force estimate
			f2 = tempDelVector[i+1];
		}
		else
		{			
			f1 = r;		// No previous velocity, just use r, s.
			f2 = s;
		}
		float normInv = sqrtf(square(f1)+square(f2));
		normInv = (normInv>0.0f) ? 1.0f/normInv : normInv;	// Avoid divide by zero
		f1 *= -(normInv*fricLimit);					// Right magnitude, and
		f2 *= -(normInv*fricLimit);					//    switch signs too.

		// Do single step of a Newton method correction to direction
		float epsilon = -f2*(c*f1+d*f2+s) + f1*(d*f1-c*f2) - f1*(a*f1+b*f2+r) - f2*(b*f1-a*f2);
		if (fabsf(epsilon)>VERY_SMALL_FLOAT)
		{
			epsilon = (f2*(a*f1+b*f2+r) - f1*(c*f1+d*f2+s))/epsilon;
			//float epsilon = (f2*(a*f1+b*f2+r) - f1*(c*f1+d*f2+s)) /
			//				((-f2)*(c*f1+d*f2+s)+f1*(d*f1-c*f2) - (f1*(a*f1+b*f2+r)+f2*(b*f1-a*f2)));
			// ( (3)-(1) ) / ( (2)-(4) )
			float temp = f1;
			f1 -= epsilon*f2;
			f2 += epsilon*temp;
			normInv = sqrtf(square(f1)+square(f2));
			normInv = (normInv>0.0f) ? 1.0f/normInv : normInv;	// Avoid divide by zero
			f1 *= normInv*fricLimit;
			f2 *= normInv*fricLimit;
		}
#endif		// TRY_FOR_COLINEAR

	}
}


void Solve3x3WithFriction (const float* matrixInv, float friction, const float* v, float* force)
{
	DebugAssert(v[0]>=0.0f);

	float nuSq = square(friction);

	// Transform the vector (v1, v2, v3) by matrixInv.
	float f[3];
	Transform3x3(matrixInv,v,f);

	if (f[0]>=0.0f && nuSq*square(f[0])>=square(f[1]) + square(f[2]))
	{
		// Friction limits are not exceeded.
		(*force) = f[0];
		(*(force+1)) = f[1];
		(*(force+2)) = f[2];
		return;
	}

	float a = matrixInv[4];//.m22;
	float b = matrixInv[5];//.m23;
	float c = matrixInv[7];//.m32;
	float d = matrixInv[8];//.m33;

	float s1 = matrixInv[3];//.m21;
	float s2 = matrixInv[6];//.m31;

	// Would be possible to precompute B2, B3.
	float detInv = a*d-b*c;
	detInv = (fabsf(detInv)>VERY_SMALL_FLOAT ? 1.0f/detInv : 0.0f);
	float b2 = (-d*s1 + b*s2)*detInv;
	float b3 = (c*s1 - a*s2)*detInv;
	float f2x = v[0]*(matrixInv[0]/*.m11*/ + b2*matrixInv[1]/*.m12*/ + b3*matrixInv[2]/*.m13*/);
	DebugAssert(f2x>=0.0f);			// Mathematical error if this fails. Check if matrix is positive definite.

	// Testing:
//	float testIn[3];
//	testIn[0] = 1.0f;
//	testIn[1] = b2;
//	testIn[2] = b3;
//	float test[3];
//	Transform3x3(matrixInv,testIn,test);

	// Interpolate between (r1x, 0, 0) and f=(f.x, f.y, f.z) to find a
	//	solution which matches frictional limits.
	float f3x = f[0]-f2x;			// f3 = f - f2 = ( f.x-f2x, f.y, f.z )
	float C = nuSq*square(f2x);
	float B = nuSq*f2x*f3x;
	float A = nuSq*square(f3x) - square(f[1]) - square(f[2]);
    float descrim = SqrtfSafe(square(B)-A*C);
	float t;
	if (B>0.0f)
	{
		if (A>0.0f)
		{
			t = -C/(descrim+B);
		}
		else
		{
			t = -(descrim+B)/A;
		}
	}
	else if (fabsf(descrim-B)>VERY_SMALL_FLOAT)
	{
		t = C/(descrim-B);
	}
	else
	{
		t = 0.0f;
	}

	t = Clamp(t,0.0f,1.0f);
	(*force) = (1.0f-t)*f2x + t*f[0];
	(*(force+1)) = t*f[1];
	(*(force+2)) = t*f[2];
}


float InnerProduct (int dimension, const float* vector1, const float* vector2)
{
	float innerProduct = 0.0f;
	for (int index=dimension; index>0; index-- )
	{
		innerProduct += (*(vector1++))*(*(vector2++));
	}
	return innerProduct;
}


bool CheckPositiveDiagonal (int dimension, const float* matrix) 
{
	for (int index=0; index<dimension; index++)
	{
		const float smallNegative = -1.0e-4f;
		if (*(matrix+index*dimension+index)<smallNegative)
		{
			DebugAssert(0);
			return false;
		}
	}

	return true;
}


// Checks MLCP solution for accuracy
// Returns a non-negative float indicating error level (unit-less ratio)
// A value close to zero is good.  A value near 1.0 or higher is very bad.
// PURPOSE: Mostly debugging and testing.  Might be used in a production environment to 
//		decide quality of convergence of Mlcp iterations (but should be modified
//		if used for that to precompute inverse mass equivalence).
float CheckMlcpAnswer (int dim, const float* M, const float* q, const int* fricMode, const float* z)
{
	// Find average diagonal element.  Use as (a) a scaling value and
	//	(b) a conversion factor between force (impulse) and velocity.
	//	This average value represents an approximate average inverse mass value.

	const float* diagPtr = M;
	int i;
	float massInvEquiv = 0.0f;
	for ( i=0; i<dim; i++ )
	{
		massInvEquiv += *diagPtr;
		diagPtr += dim+1;
	}
	massInvEquiv /= dim;

	// Find each w value.
	// If both z[i] and w[i] are non-zero, or if either is negative,
	//	then the complementarity condition is violated.
	//	increase the error measure correspondingly.
	float errorNumer = 0.0f;
	float errorDenom = 0.0f;
	const float* rowPtr = M;
	const float* qPtr = q;
	const float* zPtr = z;
	for ( i=0; i<dim-2; i+=3 )
	{
		if ( fricMode[i+1] == -1 )
		{
			break;
		}
		float w = InnerProduct( dim, rowPtr, z ) + *qPtr;
		float wEquiv = w*massInvEquiv;
		float thisError = Min( wEquiv, *zPtr );
		errorNumer += thisError*thisError;
		errorDenom += wEquiv*wEquiv+(*zPtr)*(*zPtr);
		rowPtr += 3*dim;
		qPtr += 3;
		zPtr += 3;
	}
	for ( ; i<dim; i++ )
	{
		float w = InnerProduct( dim, rowPtr, z ) + *qPtr;
		float wEquiv = w*massInvEquiv;
		float thisError = Min( wEquiv, *zPtr );
		errorNumer += thisError*thisError;
		errorDenom += wEquiv*wEquiv + (*zPtr)*(*zPtr);
		qPtr++;
		rowPtr += dim;
		zPtr++;
	}

	float errorRatio = errorDenom<=0.0f ? 0.0f : sqrtf(errorNumer/errorDenom);
	DebugAssert(errorRatio<1.0e-1f);
    return errorRatio;
}

}	// end of namespace LCPSolver


void LCPData::Init (int dimension)
{
	m_MaxDimension = dimension;
	m_InputMatrix = rage_new float[m_MaxDimension*m_MaxDimension];
	m_InputVector = rage_new float[m_MaxDimension];
	m_Solution = rage_new float[m_MaxDimension];
	m_DelInputVector = rage_new float[m_MaxDimension];
	m_FrictionCoef = rage_new float[m_MaxDimension];
	m_FrictionMode = rage_new int[m_MaxDimension];
	m_TempArray = rage_new float[m_MaxDimension*3];
	m_TempSolution = rage_new float[m_MaxDimension];
	m_TempDelInput = rage_new float[m_MaxDimension];
	m_Dimension = 0;
}

// temporary interface for sparse solver
void LCPData::InitSolutionVector (int dimension)
{
	m_Solution = rage_new float[dimension];
	m_FrictionMode = rage_new int[dimension];
}

void LCPData::Shutdown ()
{
	delete [] m_InputMatrix;
	delete [] m_InputVector;
	delete [] m_Solution;
	delete [] m_TempArray;
	delete [] m_TempSolution;
	delete [] m_TempDelInput;
	delete [] m_DelInputVector;
	delete [] m_FrictionCoef;
	delete [] m_FrictionMode;
}


void LCPData::SetDimension (int dimension)
{
	mthAssertf(dimension<=m_MaxDimension,"SetDimension(%i) was called but the maximum was set to %i.",dimension,m_MaxDimension);
	m_Dimension = dimension;
}


void LCPData::ZeroInputMatrix ()
{
	int numElements = m_Dimension*m_Dimension;
	int index;
	for (index=0; index<numElements; index++)
	{
		m_InputMatrix[index] = 0.0f;
	}

	for (index=0; index<m_Dimension; index++)
	{
		m_InputVector[index] = 0.0f;
		m_DelInputVector[index] = 0.0f;
		m_FrictionCoef[index] = 0.0f;
		m_Solution[index] = 0.0f;
		m_TempSolution[index] = 0.0f;
		m_TempDelInput[index] = 0.0f;
		m_FrictionMode[index] = -1;
	}

	for (index=0; index<3*m_Dimension; index++)
	{
		m_TempArray[index] = 0.0f;
	}
}

void LCPData::SetFrictionCoef (int index, float coef0, float coef1, float coef2)
{
	m_FrictionCoef[index] = coef0;
	m_FrictionCoef[index+1] = coef1;
	m_FrictionCoef[index+2] = coef2;
}


void LCPData::SetTransverseFriction (int index, float friction)
{
	m_FrictionCoef[index] = 0.0f;
	m_FrictionCoef[index+1] = friction;
	m_FrictionCoef[index+2] = friction;
}


void LCPData::SetFrictionMode (int index, bool constraint)
{
	// -1 means sliding friction, -2 means infinite friction and no separating (a constraint instead of a collision)
	m_FrictionMode[index] = constraint ? -2 : -1;
	m_FrictionMode[index+1] = index;
	m_FrictionMode[index+2] = index+1;
}


void LCPData::AddToMatrix3x3Block (int blockRow, int blockCol, const float values[3*VEC3_NUM_STORED_FLOATS])
{
	// Make sure the block of nine elements will fit in the input matrix.
	mthAssertf(m_Dimension>=3*blockRow && m_Dimension>=3*blockCol, "Can't put the 3x3 block at %d, %d in a %dx%d matrix", blockRow, blockCol, m_Dimension, m_Dimension);

	// Don't make sure that no negative diagonal elements are added. This can fail during articulated body self-collisions, in which the 3x3 diagonal block of the matrix
	// is the sum of 4 components - 2 for the inverse mass matrices at the collision points, and two for the reactions at each collision point from an impulse at the
	// other point. The cross terms can have negative diagonal elements, but the sum should never have a negative diagonal element.
//	ASSERT_ONLY(const float smallNegative = -1.0e-4f;)
//	Assert((blockRow!=blockCol) || (values[0]>smallNegative && values[VEC3_NUM_STORED_FLOATS+1]>smallNegative && values[2*VEC3_NUM_STORED_FLOATS+2]>smallNegative));

	// Find the position of the first element to set.
	int firstIndex = 3*(m_Dimension*blockCol+blockRow);

	// Set the nine elements in the 3x3 block.
	int rowIndex,colStart=firstIndex;
	for (int colIndex=0; colIndex<3; colIndex++)
	{
		const float* colVector = values+VEC3_NUM_STORED_FLOATS*colIndex;
		for (rowIndex=0; rowIndex<3; rowIndex++)
		{
			*(m_InputMatrix+colStart+rowIndex) += *(colVector+rowIndex);
		}
		colStart += m_Dimension;
	}
}


void LCPData::GetMatrix3x3Block (int blockRow, int blockCol, float values[3*VEC3_NUM_STORED_FLOATS])
{
	// Make sure the block of nine elements will fit in the input matrix.
	mthAssertf(m_Dimension>=3*blockRow && m_Dimension>=3*blockCol, "Can't get block at %d,%d in a %dx%d matrix", blockRow, blockCol, m_Dimension, m_Dimension);

	// Find the position of the first element to set.
	int firstIndex = 3*(m_Dimension*blockCol+blockRow);

	// Set the nine elements in the 3x3 block.
	int rowIndex,colStart=firstIndex;
	for (int colIndex=0; colIndex<3; colIndex++)
	{
		float* colVector = values+VEC3_NUM_STORED_FLOATS*colIndex;
		for (rowIndex=0; rowIndex<3; rowIndex++)
		{
			*(colVector+rowIndex) = *(m_InputMatrix+colStart+rowIndex);
		}
		colStart += m_Dimension;
	}
}


void LCPData::AddToMatrix3VectorRow (int rowStart, int colStart, const float values[3])
{
	// Make sure the row of three elements will fit in the input matrix.
	mthAssertf(rowStart<m_Dimension && colStart+2<m_Dimension, "Can't add a row vector at %d,%d to a %dx%d matrix", rowStart, colStart, m_Dimension, m_Dimension);

	// Make sure the row of three elements will not cover a diagonal position.
	mthAssertf(colStart+2<rowStart || colStart>rowStart, "Row vector at %d,%d would cover a diagonal element", rowStart, colStart);

	// Find the position of the first element to set.
	int firstIndex = m_Dimension*colStart+rowStart;

	// Set three consecutive elements in the same row.
	for (int colIndex=0; colIndex<3; colIndex++)
	{
		*(m_InputMatrix+firstIndex+m_Dimension*colIndex) += *(values+colIndex);
	}
}


void LCPData::GetMatrix3VectorRow (int rowStart, int colStart, float values[3])
{
	// Make sure the row of three elements will fit in the input matrix.
	mthAssertf(rowStart<m_Dimension && colStart+2<m_Dimension, "Can't get a row vector at %d,%d to a %dx%d matrix", rowStart, colStart, m_Dimension, m_Dimension);

	// Make sure the row of three elements will not cover a diagonal position.
	mthAssertf(colStart+2<rowStart || colStart>rowStart, "Row vector at %d,%d would cover a diagonal element", rowStart, colStart);

	// Find the position of the first element to set.
	int firstIndex = m_Dimension*colStart+rowStart;

	// Set three consecutive elements in the same row.
	for (int colIndex=0; colIndex<3; colIndex++)
	{
		*(values+colIndex) = *(m_InputMatrix+firstIndex+m_Dimension*colIndex);
	}
}

void LCPData::AddToMatrix3VectorCol (int rowStart, int colStart, const float values[3])
{
	// Make sure the column of three elements will fit in the input matrix.
	mthAssertf(rowStart+2<m_Dimension && colStart<m_Dimension, "Can't add a column vector at %d,%d to a %dx%d matrix", rowStart, colStart, m_Dimension, m_Dimension);

	// Make sure the column of three elements will not cover a diagonal position.
	mthAssertf(rowStart+2<colStart || rowStart>colStart, "Row vector at %d,%d would cover a diagonal element", rowStart, colStart);

	// Find the position of the first element to set.
	int firstIndex = m_Dimension*colStart+rowStart;

	// Set three consecutive elements in the same column.
	for (int rowIndex=0; rowIndex<3; rowIndex++)
	{
		*(m_InputMatrix+firstIndex+rowIndex) += *(values+rowIndex);
	}
}

void LCPData::GetMatrix3VectorCol (int rowStart, int colStart, float values[3])
{
	// Make sure the column of three elements will fit in the input matrix.
	mthAssertf(rowStart+2<m_Dimension && colStart<m_Dimension, "Can't get a column vector at %d,%d to a %dx%d matrix", rowStart, colStart, m_Dimension, m_Dimension);

	// Make sure the column of three elements will not cover a diagonal position.
	mthAssertf(rowStart+2<colStart || rowStart>colStart, "Row vector at %d,%d would cover a diagonal element", rowStart, colStart);

	// Find the position of the first element to set.
	int firstIndex = m_Dimension*colStart+rowStart;

	// Set three consecutive elements in the same column.
	for (int rowIndex=0; rowIndex<3; rowIndex++)
	{
		*(values+rowIndex) = *(m_InputMatrix+firstIndex+rowIndex);
	}
}


void LCPData::SetMatrixElement (int row, int col, float value)
{
	// Make sure no negative diagonal elements are set.
	DebugAssert(row!=col || value>-SMALL_FLOAT);

	// Set the given value in the input matrix.
	*(m_InputMatrix+m_Dimension*col+row) = value;
}


float LCPData::GetMatrixElement (int row, int col) const
{
	return *(m_InputMatrix+m_Dimension*col+row);
}


float LCPData::GetInputElement (int linearIndex) const
{
	return *(m_InputMatrix+linearIndex);
}


void LCPData::SetInputVector3Elements (int firstIndex, float element0, float element1, float element2)
{
	m_InputVector[firstIndex] = element0;
	m_InputVector[firstIndex+1] = element1;
	m_InputVector[firstIndex+2] = element2;
}


void LCPData::SolveLcpGS ()
{
	LCPSolver::SolveLcpGS(m_Dimension,m_InputMatrix,m_InputVector,m_Solution,m_TempDelInput,m_TempArray);
}


void LCPData::SolveMlcpFriction2GS ()
{
	LCPSolver::SolveMlcpFriction2GS(m_Dimension,m_InputMatrix,m_InputVector,m_FrictionMode,m_FrictionCoef,
									m_Solution,m_DelInputVector,m_TempArray,m_TempSolution,m_TempDelInput);
}


void LCPData::SolveMlcpFriction3GS3x3 ()
{
	const float stopFraction = 0.0001f;
	LCPSolver::SolveMlcpFriction3GS3x3(m_Dimension,m_InputMatrix,m_InputVector,m_FrictionMode,m_FrictionCoef,
										m_Solution,m_TempArray,stopFraction);

#if WRITE_LCP_TO_FILE
	if (ioMapper::DebugKeyPressed(KEY_S))
	{
		fiStream* stream = ASSET.Create("lcpData","txt");
		fiAsciiTokenizer token;
		token.Init("lcpData",stream);
		token.PutDelimiter("humanoid and box-shaped table parts\n");
		token.PutDelimiter("\nDimension: ");
		token.Put(m_Dimension);
		token.PutDelimiter("\n\nInput matrix:\n");
		for (int i=0; i<m_Dimension*m_Dimension; i++)
		{
			token.Put(m_InputMatrix[i]);
			token.PutDelimiter("\t");
		}
		token.PutDelimiter("\n\nInput vector:\n");
		for (int i=0; i<m_Dimension; i++)
		{
			token.Put(m_InputVector[i]);
			token.PutDelimiter("\t");
		}
		token.PutDelimiter("\n\nFriction mode:\n");
		for (int i=0; i<m_Dimension; i++)
		{
			token.Put(m_FrictionMode[i]);
			token.PutDelimiter("\t");
		}
		token.PutDelimiter("\n\nFriction coef:\n");
		for (int i=0; i<m_Dimension; i++)
		{
			token.Put(m_FrictionCoef[i]);
			token.PutDelimiter("\t");
		}
		token.PutDelimiter("\n\nSolution:\n");
		for (int i=0; i<m_Dimension; i++)
		{
			token.Put(m_Solution[i]);
			token.PutDelimiter("\t");
		}
		stream->Close();
	}
#endif

	//LCPSolver::CheckMlcpAnswer(m_Dimension,m_InputMatrix,m_InputVector,m_FrictionMode,m_Solution);
}

#if SPARSE_SHOOTING==1
void LCPData::SolveMlcpFriction3Sh3x3Sparse (const phConstraintGraph& theSparseMatrixGraph)
{
	const float stopFraction = 0.0001f;
	LCPSolver::SolveMlcpFriction3Sh3x3Sparse(theSparseMatrixGraph,m_InputVector,
										m_Solution,m_TempArray,m_TempSolution,m_TempDelInput,stopFraction);
}
#endif



void LCPData::SolveMlcpFriction3Sh3x3 (float stopFraction, float zeroTolerance, int maxIterations)
{
	LCPSolver::SolveMlcpFriction3Sh3x3(m_Dimension,m_InputMatrix,m_InputVector,m_FrictionMode,m_FrictionCoef,m_Solution,
										m_TempArray,m_TempSolution,m_TempDelInput,stopFraction,zeroTolerance,maxIterations);

#if WRITE_LCP_TO_FILE
	if (ioMapper::DebugKeyPressed(KEY_S))
	{
		fiStream* stream = ASSET.Create("lcpData","txt");
		fiAsciiTokenizer token;
		token.Init("lcpData",stream);
		token.PutDelimiter("humanoid and box-shaped table parts\n");
		token.PutDelimiter("\nDimension: ");
		token.Put(m_Dimension);
		token.PutDelimiter("\n\nInput matrix:\n");
		for (int i=0; i<m_Dimension*m_Dimension; i++)
		{
			token.Put(m_InputMatrix[i]);
			token.PutDelimiter("\t");
		}
		token.PutDelimiter("\n\nInput vector:\n");
		for (int i=0; i<m_Dimension; i++)
		{
			token.Put(m_InputVector[i]);
			token.PutDelimiter("\t");
		}
		token.PutDelimiter("\n\nFriction mode:\n");
		for (int i=0; i<m_Dimension; i++)
		{
			token.Put(m_FrictionMode[i]);
			token.PutDelimiter("\t");
		}
		token.PutDelimiter("\n\nFriction coef:\n");
		for (int i=0; i<m_Dimension; i++)
		{
			token.Put(m_FrictionCoef[i]);
			token.PutDelimiter("\t");
		}
		token.PutDelimiter("\n\nSolution:\n");
		for (int i=0; i<m_Dimension; i++)
		{
			token.Put(m_Solution[i]);
			token.PutDelimiter("\t");
		}
		stream->Close();
	}
#endif

	//LCPSolver::CheckMlcpAnswer(m_Dimension,m_InputMatrix,m_InputVector,m_FrictionMode,m_Solution);
}


void LCPData::ReduceDimensionForPushes (int numTriples, int numSingles)
{
	// Find the number of constraints in the input vector (they are triples that will not be reduced).
	int numConstraints = 0;
	for (int index=0; index<numTriples; index++)
	{
		if (m_FrictionMode[3*index]==-2)
		{
			numConstraints++;
		}
	}

	// Find and set the new dimension. The new dimension is 1 for each non-constraint contact, 3 for each constraint, and 1 for each
	// joint limit degree of freedom.
	int oldDimension = m_Dimension;
	int contactDimension = numTriples+2*numConstraints;
	int newDimension = contactDimension+numSingles;
	mthAssertf(newDimension<=oldDimension, "New dimension %d larger than current dimension %d", newDimension, oldDimension);
	if (newDimension<oldDimension)
	{
		SetDimension(newDimension);

		int newColIndex,oldColIndex=0;
		for (newColIndex=0; newColIndex<contactDimension; newColIndex++)
		{
			// Copy the contact normal elements in this column into the first rows.
			int oldRowIndex = 0;
			int newRowIndex;
			for (newRowIndex=0; newRowIndex<contactDimension; newRowIndex++)
			{
				int elementIndex = oldColIndex*oldDimension+oldRowIndex;
				float elementValue = GetInputElement(elementIndex);
				SetMatrixElement(newRowIndex,newColIndex,elementValue);
				if (m_FrictionMode[oldRowIndex]==-1)
				{
					// This is not a constraint, so skip ahead to the next contact.
					oldRowIndex += 3;
				}
				else
				{
					// This contact is a constraint, so go to its next row.
					oldRowIndex++;
				}
			}

			// Copy the joint dof elements in this row into the next columns.
			for (; newRowIndex<newDimension; newRowIndex++)
			{
				int elementIndex = oldColIndex*oldDimension+oldRowIndex;
				float elementValue = GetInputElement(elementIndex);
				SetMatrixElement(newRowIndex,newColIndex,elementValue);
				oldRowIndex++;
			}

			if (m_FrictionMode[oldColIndex]==-1)
			{
				// This is not a constraint, so skip ahead to the next contact.
				oldColIndex += 3;
			}
			else
			{
				// This contact is a constraint, so go to its next column.
				oldColIndex++;
			}
		}

		for (; newColIndex<newDimension; newColIndex++)
		{
			int oldRowIndex = 0;
			int newRowIndex;
			for (newRowIndex=0; newRowIndex<contactDimension; newRowIndex++)
			{
				int elementIndex = oldColIndex*oldDimension+oldRowIndex;
				float elementValue = GetInputElement(elementIndex);
				SetMatrixElement(newRowIndex,newColIndex,elementValue);
				if (m_FrictionMode[oldRowIndex]==-1)
				{
					// This is not a constraint, so skip ahead to the next contact.
					oldRowIndex += 3;
				}
				else
				{
					// This contact is a constraint, so go to its next row.
					oldRowIndex++;
				}
			}

			for (; newRowIndex<newDimension; newRowIndex++)
			{
				int elementIndex = oldColIndex*oldDimension+oldRowIndex;
				float elementValue = GetInputElement(elementIndex);
				SetMatrixElement(newRowIndex,newColIndex,elementValue);
				oldRowIndex++;
			}

			oldColIndex++;
		}
	}
}

} // namespace rage
			   
