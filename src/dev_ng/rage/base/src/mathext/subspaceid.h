// 
// mathext/subspaceid.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef MATHEXT_SUBSPACEIDENTIFICATION_H
#define MATHEXT_SUBSPACEIDENTIFICATION_H

#include "vector/matrixt.h"
#include "vector/vectort.h"

namespace rage
{

// PURPOSE:  Provides methods for doing subspace identification
class SubspaceIdentification
{
public:
	// PURPOSE:  Perform subspace identification using a biased N4SID algorithm.
	//           Given a sequence of inputs (where each input is m-dimensional)
	//           and a sequence of outputs (where each output is l-dimensional),
	//           solve for A, B, C, and D, given:
	//                x_(t+1) = A(x_t) + B(u_t) + e_t
	//                    y_t = C(x_t) + D(u_t) + f_t
	//           where u_t and y_t are the input and output at time t, 
	//           respectively, x_t is a hidden state of unknown dimension, and 
	//           e_t and f_t are noise terms.
	// PARAMS:  input - a matrix that holds the values of each u_t.  Each row 
	//                  corresponds to one u_t of m dimensions.
	//          output - a matrix that holds the values of each y_t.  Each row 
	//                   corresponds to one y_t of l dimensions.
	//          A - a matrix to hold the computed value of A
	//          B - a matrix to hold the computed value of B
	//          C - a matrix to hold the computed value of C
	//          D - a matrix to hold the computed value of D
	//          epsilon - any singular value of the system space that is > 
	//                    epsilon is possibly significant and is considered
	//                    when determining the system order
	// NOTE:  This version of subspace identification produces biased results
	//        unless the system proves to be entirely deterministic.  But the 
	//        is more efficient (in both space and time) then the more robust
	//        version.
	// SEE ALSO:  SubspaceIdentificationRobust
	static void SubspaceIdentificationBiased(const MatrixT<float>& input, 
		const MatrixT<float>& output, MatrixT<float>& A, MatrixT<float>& B, MatrixT<float>& C, 
		MatrixT<float>& D, float epsilon = 0.0000001f);

	// PURPOSE:  Perform subspace identification using a robust N4SID algorithm.
	//           Given a sequence of inputs (where each input is m-dimensional)
	//           and a sequence of outputs (where each output is l-dimensional),
	//           solve for A, B, C, and D, given:
	//                x_(t+1) = A(x_t) + B(u_t) + e_t
	//                    y_t = C(x_t) + D(u_t) + f_t
	//           where u_t and y_t are the input and output at time t, 
	//           respectively, x_t is a hidden state of unknown dimension, and 
	//           e_t and f_t are noise terms.
	// PARAMS:  input - a matrix that holds the values of each u_t.  Each row 
	//                  corresponds to one u_t of m dimensions.
	//          output - a matrix that holds the values of each y_t.  Each row 
	//                   corresponds to one y_t of l dimensions.
	//          A - a matrix to hold the computed value of A
	//          B - a matrix to hold the computed value of B
	//          C - a matrix to hold the computed value of C
	//          D - a matrix to hold the computed value of D
	//          forceStability - if true, the algorithm forces the system 
	//                           matrices A and C to be stable.
	//          epsilon - any singular value of the system space that is > 
	//                    epsilon is possibly significant and is considered
	//                    when determining the system order
	// NOTES:  This version of subspace identification produces robust results.  
	//			  The algorithm is less efficient (in both space and time) then
	//            the biased version.
	//         Forcing stability on a system that naturally produces stable 
	//            results can degrade the results slightly.  Forced stability 
	//            is most useful in cases where the system matrices cannot be 
	//            checked for stability after the fact or when the data is 
	//            known to be "nonlinear".
	// SEE ALSO:  SubspaceIdentificationBiased
	static void SubspaceIdentificationRobust(const MatrixT<float>& input, 
		const MatrixT<float>& output, MatrixT<float>& A, MatrixT<float>& B, MatrixT<float>& C, 
		MatrixT<float>& D, bool forceStability, float epsilon = 0.0000001f);
private:

	// Determine the hankel parameters from the input and output
	static bool DetermineHankelParameters(const MatrixT<float>& input, 
		const MatrixT<float>& output, short& numColsHankel, short& numBlockRows);

	// Compute the stacked hankel matrix from the input and output
	static void ComputeStackedHankelMatrix(const MatrixT<float>& input, 
		const MatrixT<float>& output, short numColsHankel, short numBlockRows, 
		MatrixT<float>& stackedHankel);

	// Biased Subspace Identification Subroutines
	static void CalculateStateSequencesBiased(const MatrixT<float>& input, 
		const MatrixT<float>& output, short numBlockRows, short numColsHankel, 
		MatrixT<float>& stateSequence, MatrixT<float>& stateSequenceP1,
		MatrixT<float>& R, float epsilon);
	static void ComputeSystemMatricesBiased(const MatrixT<float>& stateSequence, 
		const MatrixT<float>& stateSequenceP1, const MatrixT<float>& input, 
		const MatrixT<float>& output, short numBlockRows, MatrixT<float>& A, MatrixT<float>& B, 
		MatrixT<float>& C, MatrixT<float>& D, const MatrixT<float>& R);

	// Robust Subspace Identification Subroutines
	static void CalculateObservabilityMatrixRobust(const MatrixT<float>& input, 
		const MatrixT<float>& output, short numBlockRows, short numColsHankel,
		MatrixT<float>& R, MatrixT<float>& observe, float epsilon);
	static void ComputeSystemMatricesRobust(const MatrixT<float>& input, 
		const MatrixT<float>& output, short numBlockRows, MatrixT<float>& observe, 
		MatrixT<float>& R, MatrixT<float>& A, MatrixT<float>& B, 
		MatrixT<float>& C, MatrixT<float>& D, bool makeStable);
};

} // namespace rage

#endif // MATHEXT_SUBSPACEIDENTIFICATION_H
