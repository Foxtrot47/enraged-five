// 
// mathext/linearalgebra.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef MATHEXT_LINEARALGEBRA_H
#define MATHEXT_LINEARALGEBRA_H

#include "vector/vectort.h"
#include "vector/matrixt.h"
#include "vectormath/vectormath.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"

#include "math/amath.h"		// for InvSqrtf

// This file contains a number of misc. linear algebra algorithms (e.g. various decomposition
// methods and linear system solvers)

namespace rage
{

// PURPOSE: Computes the projection of one vector onto another
Vec2V_Out Project(Vec2V_In vec, Vec2V_In onto);
Vec3V_Out Project(Vec3V_In vec, Vec3V_In onto);
Vec4V_Out Project(Vec4V_In vec, Vec4V_In onto);

// PURPOSE: Given 3 points and a scalar value at each point, finds a 
// gradient and offset (gx, gy, gz, d) such that Dot((gx, gy, gz, d), (px, py, pz, 1)) = v for each 
// point and value pair.
// PARAMS:
//		point1, point2, point3 - Three points in space with known scalar values
//		values - A vector containing the three scalar values for point1, 2 and 3 in the x, y and z components respectively.
// RETURNS:
//		The gradient and offset as a vector4
// NOTES:
//		The gradient is chosen such that it is parallel to the plane formed by the three points.
Vec4V_Out ScalarGradient(Vec3V_In point1, Vec3V_In point2, Vec3V_In point3, Vec3V_In values);

// PURPOSE: Given 3 points in space with known scalar values, extrapolate those values out to 
// any other point in space. 
// Note that this returns Dot(ScalarGradient(), (queryPoint, 1)), so if you need to do multiple
// queries you should compute the ScalarGradient once and reuse it.
// PARAMS:
//		point1, point2, point3 - Three points in space with known scalar values
//		values - A vector containing the three scalar values for point1, 2 and 3 in the x, y and z components respectively.
//		queryPoint - The point you want an extrapolated scalar value for
// RETURNS:
//		The extrapolated scalar value
ScalarV LinearExtrapolate(Vec3V_In point1, Vec3V_In point2, Vec3V_In point3, Vec3V_In values, Vec3V_In queryPoint);


// Given a matrix a[1..m][1..n], this routine computes its singular value 
// decomposition, A = U�W�V T. The matrix U replaces a on output. The 
// diagonal matrix of singular values W is output as a vector w[1..n]. The 
// matrix V (not the transpose V T ) is output as v[1..n][1..n].
// Based on code in Numerical Recipes in C.
void SvdCmp(double **a, int m, int n, double w[], double **v, double epsilon = 1E-20);

///////// Templated Methods

// PURPOSE:  Calculate the Singular Value Decomposition (SVD) of a general 
//           MxN Matrix, A.  The SVD is A = UW(V^T), where U is an MxN 
//           Matrix, W is a diagonal matrix of the N singular values, and V
//           is an NxN Matrix.
// PARAMS:  A - the matrix to be decomposed
//          U - the matrix to hold the computed value of U
//          W - a vector to hold the computed singular values
//          V - the matrix to hold the computed value of V (not V^T)
template<class _Type> void SVD(const MatrixT<_Type>& A, MatrixT<_Type>& U, VectorT<_Type>& W, MatrixT<_Type>& V, double epsilon = 1E-20);

// PURPOSE:  Calculate the Moore-Penrose pseudoinverse of a general MxN 
//           Matrix, A.
// PARAMS:  A - the matrix to be inverted
//          result - the matrix to hold the pseudoinverse of A
//          epsilon - any singular value of A that is > epsilon is 
//                    considered nonzero
template<class _Type> void PseudoInverse(const MatrixT<_Type>& A, MatrixT<_Type>& result, 
						  _Type epsilon = (_Type) 0.00001);

// PURPOSE:  Calculate the LQ Decomposition of a general MxN Matrix, A.
//           The LQ decomposition is A = LQ, where L is a lower triangular 
//           matrix of size MxMin(M,N) and Q is an orthogonal matrix of 
//           size Min(M,N)xN.
// PARAMS:  A - the matrix to be decomposed
//          L - a matrix to hold the computed value of L
//          Q - a matrix to hold the computed value of Q
//          computeQ - specifies whether Q should be explicitly calculated.  If true,
//                     the calculated value of Q will be stored in 'Q', 
//                     otherwise, 'Q' will remain unchanged
// SEE ALSO:  QRDecomposition
template<class _Type> void LQDecomposition(const MatrixT<_Type>& A, MatrixT<_Type>& L, MatrixT<_Type>& Q, 
							bool computeQ = true);

// PURPOSE:  Calculate the QR Decomposition of a general MxN Matrix, A.  
//           The QR decomposition is A = QR, where R is an
//           upper triangular matrix of size Min(M,N)xN and Q is an 
//           orthogonal matrix of size MxMin(M,N).
// PARAMS:  A - the matrix to be decomposed
//          Q - a matrix to hold the computed value of Q
//          R - a matrix to hold the computed value of R
//          computeQ - specifies whether Q should be explicitly calculated.  If true,
//                     the calculated value of Q will be stored in 'Q', 
//                     otherwise, 'Q' will remain unchanged
// SEE ALSO:  LQDecomposition
template<class _Type> void QRDecomposition(const MatrixT<_Type>& A, MatrixT<_Type>& Q, MatrixT<_Type>& R, 
							bool computeQ = true);

// PURPOSE:  Provided with the mxn matrix A (where m >= n) and the m-vector B,
//           solve AX=B for the n-vector X using Gaussian Elimination with backsubstitution.
// PARAMS:  A - hold the value of matrix A
//          B - Starts as the value of vector B.  Upon completion, the first n 
//              elements of B hold the solution, X
// RETURNS:  true - if the linear system can be solve (i.e. A and B have matching 
//                  dimensions, A has at least as many rows as columns, and A is
//                  not singular)
//           false - otherwise.
// NOTE:  The value of A and B could be changed by this method even when the method
//        returns false.
// SEE ALSO:  GaussJordanElimination
template<class _Type> bool GaussianElimination(MatrixT<_Type>& A, VectorT<_Type>& B);

// PURPOSE:  Provided with the mxn matrix A (where m >= n) and the m-vector B,
//           solve AX=B for the n-vector X using Gaussian Elimination with backsubstitution.
// PARAMS:  A - hold the value of matrix A
//          B - Starts as the value of vector B.  Upon completion, the first n 
//              elements of B hold the solution, X
// RETURNS:  true - if A is not singular
//           false - otherwise.
// NOTE:  The value of A and B could be changed by this method even when the method
//        returns false.
// SEE ALSO:  GaussianElimination
// Based on code in Numerical Recipes in C.
template<class _Type> bool GaussJordanElimination(MatrixT<_Type>& a, VectorT<_Type>& b);

// PURPOSE:	Factorize an NxN, symmetric, positive-definite matrix A into the product of a lower
//			triangular matrix and its transpose (A=LL^T).
// PARAMS:	A - the matrix
//			n - dimension of A
// RETURNS: true - if the factorization succeeds
//			false - otherwise
// NOTE:	It's assumed that the lower triangular portion of A is stored in the lower triangle
//			of A and the computed elements of L overwrite A.
template<class _Type> bool CholeskyFactorization(MatrixT<_Type>& A, short n);

// PURPOSE:  Computes sqrt(a*a + b*b) while avoiding underflow and overflow errors
template<class _Type> _Type HypotMag(_Type a, _Type b);

// PURPOSE:  Returns abs(a) if b >= 0 and -abs(a) if b < 0.
template<class _Type> _Type MatchSign(_Type a, _Type b);

template<class T> inline void SWAP(T &a, T &b) { T dum = a; a = b; b = dum; }

/////////// Template Implementation

////////////////////////////////////////////////////////////////////////////////

template<class _Type> void SVD(const MatrixT<_Type>& A, MatrixT<_Type>& U, VectorT<_Type>& W, MatrixT<_Type>& V, double epsilon /* = 1E-20 */)
{
	// Setup
	int n = A.GetCols();
	int m = A.GetRows();
	double** a = rage_new double*[m+1];
	double* w = rage_new double[n+1];
	double** v = rage_new double*[n+1];
	
	for(short i = 1; i <= m; i++)
	{
		a[i] = rage_new double[n+1];
		for(short j = 1; j <= n; j++)
		{
			a[i][j] = (double) A[i-1][j-1];
		}
	}

	for(short i = 1; i <= n; i++)
	{
		v[i] = rage_new double[n+1];
	}

	// SVD
	SvdCmp(a, m, n, w, v, epsilon);

	// Make sure the result matrices are initiated
	U.Init(A.GetRows(), A.GetCols(), 0);
	W.Init(A.GetCols(), 0);
	V.Init(A.GetCols(), A.GetCols(), 0);

	// Transfer the results and cleanup
	for(short i = 1; i <= m; i++)
	{
		for(short j = 1; j <= n; j++)
		{
			U[i-1][j-1] = (_Type) a[i][j];
		}
		delete[] a[i];
	}

	for(short i = 1; i <= n; i++)
	{
		W[i-1] = (_Type) w[i];
		for(short j = 1; j <= n; j++)
		{
			V[i-1][j-1] = (_Type) v[i][j];
		}
		delete[] v[i];
	}

	delete[] a;
	delete[] w;
	delete[] v;
}

////////////////////////////////////////////////////////////////////////////////

template<class _Type> void PseudoInverse(const MatrixT<_Type>& A, MatrixT<_Type>& result, 
						  _Type epsilon /* = (_Type) 0.00001 */)
{
	MatrixT<_Type> U = MatrixT<_Type>(A.GetRows(), A.GetCols());
	VectorT<_Type> W = VectorT<_Type>(A.GetCols(), 0);
	MatrixT<_Type> V = MatrixT<_Type>(A.GetCols(), A.GetCols());
	
	SVD(A, U, W, V);
	
	MatrixT<_Type> WInverse = MatrixT<_Type>(); 
	WInverse.Init(W.GetSize(), W.GetSize(), 0);
	for (short i = 0; i < W.GetSize(); i++) 
	{ 
		if (abs(W[i]) > epsilon)
		{
			WInverse[i][i] = 1/W[i];
		}
	}

	// Compute V*W^(1/2)*U^(T)
	MatrixT<_Type> UTranspose;
	U.Transpose(UTranspose);
	MatrixT<_Type> intermediateResult;
	V.Multiply(WInverse, intermediateResult);
	intermediateResult.Multiply(UTranspose, result);
}

////////////////////////////////////////////////////////////////////////////////

template<class _Type> void LQDecomposition(const MatrixT<_Type>& A, MatrixT<_Type>& L, MatrixT<_Type>& Q, 
							bool computeQ /* = true */)
{
	MatrixT<_Type> ATranspose;
	A.Transpose(ATranspose);

	MatrixT<_Type> QTranspose;
	MatrixT<_Type> R;
	
	QRDecomposition(ATranspose, QTranspose, R, computeQ);

	if(computeQ) QTranspose.Transpose(Q);
	R.Transpose(L);
}

////////////////////////////////////////////////////////////////////////////////

template<class _Type> void QRDecomposition(const MatrixT<_Type>& A, MatrixT<_Type>& Q, MatrixT<_Type>& R, 
							bool computeQ /* = true */)
{
	MatrixT<_Type> compressedQR = MatrixT<_Type>(A);
	
	short numRows = compressedQR.GetRows();
	short numCols = compressedQR.GetCols();

	bool padded = false;
	if(numRows < numCols) padded = true;
    
	VectorT<_Type> diagonalR = VectorT<_Type>(numCols, 0);

	// Perform the QR Decomposition
	for(short l = 0; l < numCols; l++)
	{
		// Compute the scale of the column from the diagonal
		_Type columnScale = 0;
		for(short m = l; m < numRows; m++)
		{
			columnScale = HypotMag(columnScale, compressedQR[m][l]);
		}
		
		// Householder Reduction
		if(columnScale)
		{
			columnScale = MatchSign(columnScale, compressedQR[l][l]);

			// Scale the column from the diagonal
			for(short m = l; m < numRows; m++)
			{
				compressedQR[m][l] /= columnScale;
			}
			compressedQR[l][l] += 1.0f;

			// Store the column scale
			diagonalR[l] = -columnScale;

			for(short n = l+1; n < numCols; n++)
			{
				// Compute sum
				_Type sum = 0;
				for(short m = l; m < numRows; m++)
				{
					sum += compressedQR[m][l]*compressedQR[m][n];
				}
				sum = -sum/compressedQR[l][l];

				// Set column n from the pivot
				for(short m = l; m < numRows; m++)
				{
					compressedQR[m][n] += sum*compressedQR[m][l];
				}
			}
		}
	}

	// Fill R Matrix
	R.Init(padded ? numRows : numCols, numCols, 0);
	for(short m = 0; m < R.GetRows(); m++)
	{
		for(short n = 0; n < R.GetCols(); n++)
		{
			if(m < n)
			{
				R[m][n] = compressedQR[m][n];
			}
			else if(m == n)
			{
				R[m][n] = diagonalR[m];
			}
		}
	}

	if(computeQ)
	{
		// Fill Q Matrix
		Q.Init(numRows, padded ? numRows : numCols, 0);
		for(short l = Q.GetCols() - 1; l >= 0; l--)
		{
			// Set Diagonal Value To 1
			Q[l][l] = 1;

			for(short n = l; n < Q.GetCols(); n++)
			{
				if(compressedQR[l][l])
				{
					_Type sum = 0;
					for(short m = l; m < Q.GetRows(); m++)
					{
						sum += compressedQR[m][l]*Q[m][n];
					}
					sum = -sum/compressedQR[l][l];

					for(short m = l; m < Q.GetRows(); m++)
					{
						Q[m][n] += sum*compressedQR[m][l];
					}
				}


			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

template<class _Type> _Type HypotMag(_Type a, _Type b)
{
	_Type absa = (a >= 0) ? a : -a;
	_Type absb = (b >= 0) ? b : -b;

	if(!absa && !absb)
	{
		return 0;
	}

	if(absa > absb) 
	{
		return absa*sqrt(1+square(absb/absa));
	}
	else
	{
		return absb*sqrt(1+square(absa/absb));
	}
}

////////////////////////////////////////////////////////////////////////////////

template<class _Type> _Type MatchSign(_Type a, _Type b)
{
	_Type aa = (a >= 0)? a : -a;
	return (b >= 0) ? aa : -aa;
}

////////////////////////////////////////////////////////////////////////////////

template<class _Type> bool GaussianElimination(MatrixT<_Type>& A, VectorT<_Type>& B)
{
	if(A.GetCols() != B.GetSize())
	{
		mthErrorf("GaussianElimination - Cannot solve linear equations without matching dimensions");
		return false;
	}

	if(A.GetCols() > A.GetRows())
	{
		mthErrorf("GaussianElimination - Cannot use Gaussian Elimination to solve underdetermined system");
		return false;
	}

	// Elimination Step
	for (short i = 0; i < A.GetCols(); i++)
	{
		// Find the row that has the largest value below the diagonal in column i
		short bigRow = i;
		for(short j = i; j < A.GetRows(); j++)
		{
			if (abs(A[j][i]) > abs(A[bigRow][i]))
				bigRow = j;
		}

		// Check for singularity
		if(A[bigRow][i] == 0.0f)
		{
			mthErrorf("GaussianElimination - Cannot solve linear system with singularity");
			return false;
		}

		// Swap the row that had the biggest value with row i
		if(i != bigRow)
		{
			for (short j = i; j < A.GetCols(); j++)
			{
				_Type tempValue = A[i][j];
				A[i][j] = A[bigRow][j];
				A[bigRow][j] = tempValue;
			}

			// Don't forget to swap the vector value as well
			_Type tempValue = B[i];
			B[i] = B[bigRow];
			B[bigRow] = tempValue;
		}

		// Zero the terms below the diagonal
		for(short j = i + 1; j < A.GetRows(); j++)
		{
			_Type factor = A[j][i]/A[i][i];

			B[j] = B[j]-B[i]*factor;

			for(short k = A.GetCols() - 1; k > i; k--)
			{
				A[j][k] = A[j][k] - A[i][k]*factor;
			}

			A[j][i] = 0;
		}
	}

	// Back Substitution
	for(short i = A.GetCols() - 1; i >= 0; i--)
	{
		for(short j = A.GetCols() - 1; j > i; j--)
		{
			B[i] = B[i] - A[i][j]*B[j];
		}

		B[i] = B[i]/A[i][i];
	}

	return true;
}

// Based on code in Numerical Recipes in C.
template<class _Type> bool GaussJordanElimination(MatrixT<_Type>& a, VectorT<_Type>& b)
{
	short i, icol, irow, j, k, l, ll;
	_Type big, dum, pivinv;

	short n = a.GetRows();
	VectorT<int> indxc(n, 0), indxr(n, 0), ipiv(n, 0);

	for( j = 0; j < n; j++ )
		ipiv[j] = 0;

	icol = irow = 0;
	for( i = 0; i < n; i++ )
	{
		big=0;
		for( j = 0; j < n; j++ )
		{
			if( ipiv[j] != 1 )
			{
				for( k = 0; k < n; k++ )
				{
					if( ipiv[k] == 0 )
					{
						if( fabs(a[j][k]) >= big )
						{
							big = fabs(a[j][k]);
							irow = j;
							icol = k;
						}
					}
				}
			}
		}

		++(ipiv[icol]);
		if( irow != icol )
		{
			for( l = 0; l < n; l++ ) SWAP( a[irow][l], a[icol][l] );
			SWAP( b[irow], b[icol] );
		}

		indxr[i] = irow;
		indxc[i] = icol;
		if(a[icol][icol] == 0)
		{
			mthErrorf("GaussJordanElimination - Cannot solve linear system with singularity");
			return false;
		}

		pivinv = 1/a[icol][icol];
		a[icol][icol] = 1;
		for( l = 0; l < n; l++ ) a[icol][l] *= pivinv;
		b[icol] *= pivinv;
		for( ll = 0; ll < n; ll++ )
		{
			if( ll != icol )
			{
				dum = a[ll][icol];
				a[ll][icol]=0.0f;
				for( l = 0; l < n; l++ ) a[ll][l] -= a[icol][l]*dum;
				b[ll] -= b[icol]*dum;
			}
		}
	}

	for( l = n-1; l >= 0; l-- )
	{
		if( indxr[l] != indxc[l] )
			for( k = 0; k < n; k++ )
				SWAP( a[k][indxr[l]], a[k][indxc[l]] );
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

template<class _Type> bool CholeskyFactorization(MatrixT<_Type>& A, short n)
{
	Assert(A.GetCols() == n && A.GetRows() == n);

	short sizem1 = n - 1;
	for (short i = 0; i < n; ++i) {
		for (short j = 0; j < i; ++j) {
			for (short k = i; k <= sizem1; ++k) {
				A[k][i] -= A[i][j] * A[k][j];
			}
		}
		for (short k = 0; k < i; ++k) {
			A[k][i] = A[i][k];
		}
		_Type diagonal = A[i][i];
		if (diagonal <= 0.0) {
			return false;
		}
		_Type invsqrt = InvSqrtf(float(diagonal));
		for (short k = i; k <= sizem1; ++k) {
			A[k][i] *= invsqrt;
		}
	}
	return true;
}

} // namespace rage

#endif
