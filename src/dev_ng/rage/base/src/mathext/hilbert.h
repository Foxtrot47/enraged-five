//
// mathext/hilbert.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef MATHEXT_HILBERT_H
#define MATHEXT_HILBERT_H

void FindPointOnHilbertCurve(rage::u32 num, rage::u32& x, rage::u32& y);
rage::u32 FindDistanceAlongHilbertCurve(rage::u32 x, rage::u32 y);

#endif
