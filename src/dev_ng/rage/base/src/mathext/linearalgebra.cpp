// 
// mathext/linearalgebra.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "linearalgebra.h"
#include "math/amath.h"
#include "vectormath/classes.h"

namespace rage
{

// Numerical Recipes licensing requires this to appear in the executable
#ifndef __SNC__
static const char NRNotice[]="Copyright (c) 1986, 1992 by Numerical Recipes Software";
#endif

////////////////////////////////////////////////////////////////////////////////
Vec2V_Out Project(Vec2V_In vec, Vec2V_In onto)
{
	// proj(a,b) = ((a dot b) / | b |^2 ) * b
	ScalarV dot = Dot(vec, onto);
	ScalarV oneOvermag2onto = InvMagSquared(onto);
	return Scale(Scale(dot, oneOvermag2onto), onto);
}

Vec3V_Out Project(Vec3V_In vec, Vec3V_In onto)
{
	// proj(a,b) = ((a dot b) / | b |^2 ) * b
	ScalarV dot = Dot(vec, onto);
	ScalarV oneOvermag2onto = InvMagSquared(onto);
	return Scale(Scale(dot, oneOvermag2onto), onto);
}

Vec4V_Out Project(Vec4V_In vec, Vec4V_In onto)
{
	// proj(a,b) = ((a dot b) / | b |^2 ) * b
	ScalarV dot = Dot(vec, onto);
	ScalarV oneOvermag2onto = InvMagSquared(onto);
	return Scale(Scale(dot, oneOvermag2onto), onto);
}

Vec4V_Out ScalarGradient(Vec3V_In point1, Vec3V_In point2, Vec3V_In point3, Vec3V_In values)
{
	// Compute this by solving a system of eqns. We know that we want 
	// the eqn. listed in the PURPOSE to be true for all 3 points, so thats
	// three equations with 4 unknowns. The final constraint is that we want the gradient to be
	// parallel with the plane formed by the three points. So the plane normal dotted with
	// the gradient should be 0.
	// I.e. we want to solve this system for (g,d)
	//  [ p1x p1y p1z 1 ] [ gx ]   [ v1 ]
	//  [ p2x p2y p2z 1 ] [ gy ] = [ v2 ]
	//  [ p3x p3y p3z 1 ] [ gz ]   [ v3 ]
	//  [  nx  ny  nz 0 ] [  d ]   [  0 ]

	Vec3V oneToTwo = Subtract(point2, point1);
	Vec3V oneToThree = Subtract(point3, point1);

	Vec3V normal = Cross(oneToTwo, oneToThree); // Don't need to normalize since scale doesn't matter for the eqn above.

	// Since we're building by rows, build matrixT and transpose it
	Mat44V matrixT;
	matrixT.SetCol0(Vec4V(point1, ScalarV(V_ONE)));
	matrixT.SetCol1(Vec4V(point2, ScalarV(V_ONE)));
	matrixT.SetCol2(Vec4V(point3, ScalarV(V_ONE)));
	matrixT.SetCol3(Vec4V(normal, ScalarV(V_ZERO)));

	Mat44V matrix;
	Transpose(matrix, matrixT);

	Mat44V matrixInv;
	InvertFull(matrixInv, matrix);

	Vec4V gradientAndOffset;
	gradientAndOffset = Multiply(matrixInv, Vec4V(values, ScalarV(V_ZERO)));

	return gradientAndOffset;
}

// PURPOSE: Given 3 points in space with known scalar values, extrapolate those values out to 
// any other point in space. 
// Note that this returns ScalarGradient() * (queryPoint, 1), so if you need to do multiple
// queries you should compute the ScalarGradient once and reuse it.
ScalarV LinearExtrapolate(Vec3V_In point1, Vec3V_In point2, Vec3V_In point3, Vec3V_In values, Vec3V_In queryPoint)
{
	Vec4V sg = ScalarGradient(point1, point2, point3, values);
	return Dot(sg, Vec4V(queryPoint, ScalarV(V_ONE)));
}

////////////////////////////////////////////////////////////////////////////////

// Based on code in Numerical Recipes in C.
void SvdCmp(double **a, int m, int n, double w[], double **v, double epsilon /* = 1E-20 */)
{	
	int flag,i,its,j,jj,k,l,nm;
	l = 0;
	nm = 0;

	double anorm,c,f,g,h,s,scale,x,y,z,*rv1;
	rv1=rage_new double[n+1];
	g=scale=anorm=0.0; 
	
	// Householder reduction to bidiagonal form.
	for (i=1;i<=n;i++) 
	{
		l=i+1;
		rv1[i]=scale*g;
		g=s=scale=0.0;
		if (i <= m) 
		{
			for (k=i;k<=m;k++) scale += abs(a[k][i]);
			// Epsilon added by rheck to solve division by zero problems
			if (scale > epsilon) 
			{
				for (k=i;k<=m;k++) 
				{
					a[k][i] /= scale;
					s += a[k][i]*a[k][i];
				}
				f=a[i][i];
				g = -MatchSign(sqrt(s),f);
				h=f*g-s;
				a[i][i]=f-g;
				for (j=l;j<=n;j++) 
				{
					for (s=0.0,k=i;k<=m;k++) s += a[k][i]*a[k][j];
					f=s/h;
					for (k=i;k<=m;k++) 
					{
						a[k][j] += f*a[k][i];
					}
				}
				for (k=i;k<=m;k++) 
				{
					a[k][i] *= scale;
				}
			}
		}
		w[i]=scale *g;
		g=s=scale=0.0;
		if (i <= m && i != n) 
		{
			for (k=l;k<=n;k++) scale += abs(a[i][k]);
			// Epsilon added by rheck to solve division by zero problems
			if (scale > epsilon)
			{
				for (k=l;k<=n;k++) 
				{
					a[i][k] /= scale;
					s += a[i][k]*a[i][k];
				}
				f=a[i][l];
				g = -MatchSign(sqrt(s),f);
				h=f*g-s;
				a[i][l]=f-g;
				for (k=l;k<=n;k++) rv1[k]=a[i][k]/h;
				for (j=l;j<=m;j++) 
				{
					for (s=0.0,k=l;k<=n;k++) s += a[j][k]*a[i][k];
					for (k=l;k<=n;k++) a[j][k] += s*rv1[k];
				}
				for (k=l;k<=n;k++) a[i][k] *= scale;
			}
		}
		anorm= Max(anorm,(abs(w[i])+abs(rv1[i])));
	}
	for (i=n;i>=1;i--) 
	{ 
		// Accumulation of right-hand transformations.
		if (i < n) 
		{
			// Epsilon added by rheck to solve division by zero problems
			if (abs(g) > epsilon) 
			{
				for (j=l;j<=n;j++)
				{
					// Double division to avoid possible underflow.
					v[j][i]=(a[i][j]/a[i][l])/g;
				}
				for (j=l;j<=n;j++) 
				{
					for (s=0.0,k=l;k<=n;k++) s += a[i][k]*v[k][j];
					for (k=l;k<=n;k++) v[k][j] += s*v[k][i];
				}
			}
			for (j=l;j<=n;j++) v[i][j]=v[j][i]=0.0;
		}
		v[i][i]=1.0;
		g=rv1[i];
		l=i;
	}
	for (i = Min(m,n);i>=1;i--) 
	{ 
		// Accumulation of left-hand transformations.
		l=i+1;
		g=w[i];
		for (j=l;j<=n;j++) a[i][j]=0.0;
		// Epsilon added by rheck to solve division by zero problems
		if (abs(g) > epsilon)
		{
			g=1.0f/g;
			for (j=l;j<=n;j++) 
			{
				for (s=0.0,k=l;k<=m;k++) s += a[k][i]*a[k][j];
				f=(s/a[i][i])*g;
				for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
			}
			for (j=i;j<=m;j++) a[j][i] *= g;
		} 
		else for (j=i;j<=m;j++) a[j][i]=0.0;
		++a[i][i];
	}
	for (k=n;k>=1;k--) 
	{ 
		// Diagonalization of the bidiagonal form: Loop over singular values, 
		for (its=1;its<=30;its++) 
		{ 
			// and over allowed iterations.
			flag=1;
			for (l=k;l>=1;l--) 
			{ 
				// Test for splitting.
				nm=l-1; 
				// Note that rv1[1] is always zero.
				if ((abs((float)rv1[l])+anorm) == anorm) 
				{
					flag=0;
					break;
				}
				if ((abs((float)w[nm])+anorm) == anorm) break;
			}
			if (flag) 
			{
				c=0.0; 
				// Cancellation of rv1[l], if l > 1.
				s=1.0;
				for (i=l;i<=k;i++) 
				{
					f=s*rv1[i];
					rv1[i]=c*rv1[i];
					if ((abs(f)+anorm) == anorm) break;
					g=w[i];
					h=HypotMag(f,g);
					w[i]=h;
					h=1.0f/h;
					c=g*h;
					s = -f*h;
					for (j=1;j<=m;j++) 
					{
						y=a[j][nm];
						z=a[j][i];
						a[j][nm]=y*c+z*s;
						a[j][i]=z*c-y*s;
					}
				}
			}
			z=w[k];
			if (l == k) 
			{ 
				// Convergence.
				if (z < 0.0) 
				{ 
					// Singular value is made nonnegative.
					w[k] = -z;
					for (j=1;j<=n;j++) v[j][k] = -v[j][k];
				}
				break;
			}
			if (its == 30) 
			{
				mthWarningf("SvdCmp - no convergence in 30 svdcmp iterations");
			}
			x=w[l]; 
			// Shift from bottom 2-by-2 minor.
			nm=k-1;
			y=w[nm];
			g=rv1[nm];
			h=rv1[k];
			f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0f*h*y);
			g=HypotMag(f,1.0);
			f=((x-z)*(x+z)+h*((y/(f+MatchSign(g,f)))-h))/x;
			c=s=1.0; 
			// Next QR transformation:
			for (j=l;j<=nm;j++) 
			{
				i=j+1;
				g=rv1[i];
				y=w[i];
				h=s*g;
				g=c*g;
				z=HypotMag(f,h);
				rv1[j]=z;
				c=f/z;
				s=h/z;
				f=x*c+g*s;
				g = g*c-x*s;
				h=y*s;
				y *= c;
				for (jj=1;jj<=n;jj++) 
				{
					x=v[jj][j];
					z=v[jj][i];
					v[jj][j]=x*c+z*s;
					v[jj][i]=z*c-x*s;
				}
				z=HypotMag(f,h);
				w[j]=z; 
				// Rotation can be arbitrary if z = 0.
				// Epsilon added by rheck to solve division by zero problems
				if (abs(z) > epsilon) 
				{
					z=1.0f/z;
					c=f*z;
					s=h*z;
				}
				f=c*g+s*y;
				x=c*y-s*g;
				for (jj=1;jj<=m;jj++) 
				{
					y=a[jj][j];
					z=a[jj][i];
					a[jj][j]=y*c+z*s;
					a[jj][i]=z*c-y*s;
				}
			}
			rv1[l]=0.0;
			rv1[k]=f;
			w[k]=x;
		}
	}
	delete[] rv1;
}

} // namespace rage
