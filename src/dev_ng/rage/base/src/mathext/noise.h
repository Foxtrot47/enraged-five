// 
// math/noise.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef MATH_NOISE_H
#define MATH_NOISE_H

#include "vector/vector3.h"

namespace rage
{

	struct VECTOR_ALIGN RawVector3
	{
		float x,y,z,w;
		operator Vector3() {
			return *reinterpret_cast<Vector3*>(this);
		}
		operator Vector3() const {
			return *reinterpret_cast<const Vector3*>(this);
		}
	};

	// Perlin noise algorithm taken from http://mrl.nyu.edu/~perlin/noise/
	class PerlinNoise
	{
	public:
		// Call before calling Noise() methods to specify a tiling period for noise.
		// Returns the previous wrap value.  
		// wrap must be a power of two.
		// wrap==0 means no tiling.
		// non-zero wrap may only be used with separation == 2.0f
		static u32 SetWrap(u32 wrap);

		static float Noise(Vector3::Param v, int octaves, float persistance=0.5f, float separation=2.0f);

		// Compute 3 separate noise values by permuting the components in v
		static Vector3::Return Noise3d(Vector3::Param v, int octaves, float persistance=0.5f, float separation=2.0f);

		static float RawNoise(float x);
		static float RawNoise(float x, float y)
		{
			return RawNoise(Vector3(x, y, 0.0f));
		}
		static float RawNoise(Vector3::Param v);

	private:
		static float Fade(float t)
		{
			return t * t * t * (t * (t * 6.0f - 15.0f) + 10.0f);
		}

		static Vector3 Fade(Vector3::Param t)
		{
			Vector3 out;
			Vector3 t2, t3;
			t2.Multiply(t,t);
			t3.Multiply(t2, t);
			out.AddScaled(Vector3(-15.0f, -15.0f, -15.0f), t, 6.0f);
			out.AddScaled(Vector3(10.0f, 10.0f, 10.0f), out, t);
			out.Multiply(t3);

			return out;
		}

		static float Grad(int hash, Vector3::Param v)
		{	
//			return v.Dot(sm_GradientTable[hash % 12]);
			Vector3 vec(v);
			Vector3 randVec(sm_GradientTable[hash & 0xF]);
			return vec.Dot(randVec);
		}

		static float Grad(int hash, float f)
		{
			return f * (hash & 1 ? 1.0f : -1.0f); //rs does this work?
		}

		static int sm_Permutation[512];

		static RawVector3 sm_GradientTable[16];

		static u32 sm_Wrap;
	};

}

#endif
