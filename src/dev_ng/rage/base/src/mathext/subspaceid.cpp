// 
// mathext/subspaceid.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "subspaceid.h"
#include <stdio.h>
#include <math/amath.h>
#include "linearalgebra.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////

void SubspaceIdentification::SubspaceIdentificationBiased(const MatrixT<float>& input, 
											   const MatrixT<float>& output, 
											   MatrixT<float>& A, 
											   MatrixT<float>& B, MatrixT<float>& C, 
											   MatrixT<float>& D, 
											   float epsilon /* = 0.0000001f */)
{
	short numColsHankel;
	short numBlockRows;
	if(!DetermineHankelParameters(input, output, numColsHankel, numBlockRows)) 
		return;

	MatrixT<float> stateSequence;
	MatrixT<float> stateSequenceP1;
	MatrixT<float> R;
	CalculateStateSequencesBiased(input, output, numBlockRows, numColsHankel, 
		stateSequence, stateSequenceP1, R, epsilon);
	printf("  Finished Identifying State Sequences\n");

	ComputeSystemMatricesBiased(stateSequence, stateSequenceP1, input, output, 
		numBlockRows, A, B, C, D, R);
	printf("  Finished Computing System Matrices\n");
}

////////////////////////////////////////////////////////////////////////////////

void SubspaceIdentification::SubspaceIdentificationRobust(const MatrixT<float>& input, 
											   const MatrixT<float>& output, 
											   MatrixT<float>& A, 
											   MatrixT<float>& B, MatrixT<float>& C, 
											   MatrixT<float>& D, bool forceStability, 
											   float epsilon /* = 0.0000001f */)
{
	short numColsHankel;
	short numBlockRows;
	if(!DetermineHankelParameters(input, output, numColsHankel, numBlockRows)) 
		return;

	MatrixT<float> R;
	MatrixT<float> observe;
	CalculateObservabilityMatrixRobust(input, output, numBlockRows, 
		numColsHankel, R, observe, epsilon);
	printf("  Finished Computing Observability Matrix\n");

	ComputeSystemMatricesRobust(input, output, numBlockRows, observe, R, A, B, 
		C, D, forceStability);
	printf("  Finished Computing System Matrices\n");
}

////////////////////////////////////////////////////////////////////////////////

bool SubspaceIdentification::DetermineHankelParameters(const MatrixT<float>& input, 
												  const MatrixT<float>& output, 
												  short& numColsHankel, 
												  short& numBlockRows)
{
	// Setup
	short maxOrderPerOutput = 10;
	// i is usually equal to 2*maxorder/#outputs
	numBlockRows = 2*maxOrderPerOutput; 
	short numSamplePairs = Min(input.GetRows(), output.GetRows());
	numColsHankel = numSamplePairs - (2*numBlockRows) + 1;

	// Make sure we have enough data to do the identification
	if(numColsHankel < output.GetCols()*2*numBlockRows)
	{
		printf("Error: Not enough samples to do subspace identification ");
		printf("(%d should be >= %d)\n", numColsHankel, 
			output.GetCols()*2*numBlockRows);
		return false;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void SubspaceIdentification::ComputeStackedHankelMatrix(const MatrixT<float>& input, 
												   const MatrixT<float>& output, 
												   short numColsHankel, 
												   short numBlockRows,
												   MatrixT<float>& stackedHankel)
{
	// Some temporary matrices used for intermediate results
	MatrixT<float> temp1;
	MatrixT<float> temp2;

	// Compute the Hankel Matrices
	float jsqrt = sqrt((float) numColsHankel);
	temp1 = MatrixT<float>(input);
	temp1.Scale(1.0f/jsqrt);
	temp2 = MatrixT<float>(output);
	temp2.Scale(1.0f/jsqrt);
	MatrixT<float> inputHankel;
	ComputeBlockHankel(temp1, numBlockRows, numColsHankel, inputHankel);
	MatrixT<float> outputHankel;
	ComputeBlockHankel(temp2, numBlockRows, numColsHankel, outputHankel);

	printf("  Computed Hankel Matrices\n");

	// Stack the hankels
	Stack(inputHankel, outputHankel, stackedHankel);

	printf("  Stacked the Hankel Matrices\n");
}

////////////////////////////////////////////////////////////////////////////////

void SubspaceIdentification::CalculateStateSequencesBiased(const MatrixT<float>& input, 
												const MatrixT<float>& output, 
												short numBlockRows, 
												short numColsHankel, 
												MatrixT<float>& stateSequence, 
												MatrixT<float>& stateSequenceP1, 
												MatrixT<float>& R,
												float epsilon)
{
	// Some temporary matrices used for intermediate results
	MatrixT<float> temp1;
	MatrixT<float> temp2;
	MatrixT<float> temp3;

	// Compute the stacked hankel matrix
	MatrixT<float> stackedHankel;
	ComputeStackedHankelMatrix(input, output, numColsHankel, numBlockRows, 
		stackedHankel);

	// Determine the dimensionality of the input and output
	short numDimsInput = input.GetCols();
	short numDimsOutput = output.GetCols();

	// Determine the number of rows in each "piece" of the stacked hankel
	short numRows1 = numDimsInput*numBlockRows; // U0|i
	short numRows2 = numDimsInput; // Ui|i
	short numRows3 = numDimsInput*(numBlockRows - 1); // Ui+1|2i-1
	short numRows4 = numDimsOutput*numBlockRows; // Y0|i
	short numRows5 = numDimsOutput; // Yi|i
	short numRows6 = numDimsOutput*(numBlockRows - 1); // Yi+1|2i-1

	// Do the LQ Decomposition
	LQDecomposition(stackedHankel, temp2, temp1, false);
	temp2.GetSubMatrix(0, 
		numRows1+numRows2+numRows3+numRows4+numRows5+numRows6, 0, 
		numRows1+numRows2+numRows3+numRows4+numRows5+numRows6, R);

	printf("  Computed the LQ Decomposition\n");

	// Future Outputs
	MatrixT<float> Rf;
	R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4, numRows5+numRows6,
		0, R.GetCols(), Rf);

	// Past Data
	MatrixT<float> Rp;
	R.GetSubMatrix(0, numRows1, 0, R.GetCols(), temp1);
	R.GetSubMatrix(numRows1+numRows2+numRows3, numRows4, 0, R.GetCols(), 
		temp2);
	Stack(temp1, temp2, Rp);

	// Future Inputs
	MatrixT<float> Ru;
	R.GetSubMatrix(numRows1, numRows2+numRows3, 0, 
		numRows1+numRows2+numRows3, Ru);

	// Invert the future inputs
	MatrixT<float> RuInverse;
	PseudoInverse(Ru, RuInverse);

	// Perpendicular Future Outputs
	MatrixT<float> Rfp;
	Rf.GetSubMatrix(0, Rf.GetRows(), 0, numRows1+numRows2+numRows3, temp1);
	temp1.Multiply(RuInverse, temp3);
	temp3.Multiply(Ru, temp2);
	temp1.Subtract(temp2, temp3);
	Rf.GetSubMatrix(0, Rf.GetRows(), numRows1+numRows2+numRows3, 
		numRows4+numRows5+numRows6, temp1);
	Append(temp3, temp1, Rfp);

	// Perpendicular Past
	MatrixT<float> Rpp;
	Rp.GetSubMatrix(0, Rp.GetRows(), 0, numRows1+numRows2+numRows3, temp1);
	temp1.Multiply(RuInverse, temp3);
	temp3.Multiply(Ru, temp2);
	temp1.Subtract(temp2, temp3);
	Rp.GetSubMatrix(0, Rp.GetRows(), numRows1+numRows2+numRows3, 
		numRows4+numRows5+numRows6, temp1);
	Append(temp3, temp1, Rpp);

	// The oblique projection
	MatrixT<float> Ob;
	PseudoInverse(Rpp, temp1);
	Rfp.Multiply(temp1, temp2);
	temp2.Multiply(Rp, Ob);

	printf("  Computed Ob\n");

	// Perform extra projection of Ob on Uf perpendicular
	MatrixT<float> WOW;
	Ob.GetSubMatrix(0, Ob.GetRows(), 0, numRows1+numRows2+numRows3, temp1);
	temp1.Multiply(RuInverse, temp3);
	temp3.Multiply(Ru, temp2);
	temp1.Subtract(temp2, temp3);
	Ob.GetSubMatrix(0, Ob.GetRows(), numRows1+numRows2+numRows3, 
		numRows4+numRows5+numRows6, temp1);
	Append(temp3, temp1, WOW);

	// Do the SVD part
	MatrixT<float> U;
	VectorT<float> W;
	MatrixT<float> V;
	SVD(WOW, U, W, V);

	printf("  Did SVD for system order\n");

	// Compute the order of the system
	float sum = 0;
	for(short i = 0; i < W.GetSize(); i++)
	{
		if(W[i] > epsilon) sum += W[i];
	}
	short systemOrder = 0;
	float eigenCutOff = sum*.5f;
	sum = 0;
	for(short i = 0; i < W.GetSize(); i++)
	{
		if(sum < eigenCutOff) 
		{
			systemOrder++;
			sum += W[i];
		}
		else
		{
			break;
		}
	}

	printf("  Found system order of %d", systemOrder);
	if(systemOrder)
	{
		printf(" with highest and lowest singular values %f and %f", W[0], 
			W[systemOrder-1]);
	}
	if(systemOrder < W.GetSize() - 1)
	{
		printf(" (Passed On %f)", W[systemOrder]);
	}
	printf("\n");

	// Find Observability Matrices
	MatrixT<float> observe;
	U.GetSubMatrix(0, U.GetRows(), 0, systemOrder, observe);
	for(short i = 0; i < observe.GetRows(); i++)
	{
		for(short j = 0; j < observe.GetCols(); j++)
		{
			observe[i][j] *= sqrt(W[j]);
		}
	}
	MatrixT<float> observem1;
	observe.GetSubMatrix(0, observe.GetRows()-numDimsOutput, 0, 
		observe.GetCols(), observem1);

	printf("  Computed the observability matrices\n");

	// Future Outputs
	R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4+numRows5, numRows6,
		0, R.GetCols(), Rf);

	// Past Data
	R.GetSubMatrix(0, numRows1+numRows2, 0, R.GetCols(), temp1);
	R.GetSubMatrix(numRows1+numRows2+numRows3, numRows4+numRows5, 0,
		R.GetCols(), temp2);
	Stack(temp1, temp2, Rp);

	// Future Inputs
	R.GetSubMatrix(numRows1+numRows2, numRows3, 0, 
		numRows1+numRows2+numRows3, Ru);

	// Invert the future inputs
	PseudoInverse(Ru, RuInverse);

	// Perpendicular Future Outputs
	Rf.GetSubMatrix(0, Rf.GetRows(), 0, numRows1+numRows2+numRows3, temp1);
	temp1.Multiply(RuInverse, temp3);
	temp3.Multiply(Ru, temp2);
	temp1.Subtract(temp2, temp3);
	Rf.GetSubMatrix(0, Rf.GetRows(), numRows1+numRows2+numRows3, 
		numRows4+numRows5+numRows6, temp1);
	Append(temp3, temp1, Rfp);

	// Perpendicular Past
	Rp.GetSubMatrix(0, Rp.GetRows(), 0, numRows1+numRows2+numRows3, temp1);
	temp1.Multiply(RuInverse, temp3);
	temp3.Multiply(Ru, temp2);
	temp1.Subtract(temp2, temp3);
	Rp.GetSubMatrix(0, Rp.GetRows(), numRows1+numRows2+numRows3, 
		numRows4+numRows5+numRows6, temp1);
	Append(temp3, temp1, Rpp);

	// The oblique projection
	MatrixT<float> Obm;
	PseudoInverse(Rpp, temp1);
	Rfp.Multiply(temp1, temp2);
	temp2.Multiply(Rp, Obm);

	printf("  Computed Obm\n");
	
	// Calculate the state sequences
	PseudoInverse(observe, temp1);
	temp1.Multiply(Ob, stateSequence);

	PseudoInverse(observem1, temp1);
	temp1.Multiply(Obm, stateSequenceP1);
}

////////////////////////////////////////////////////////////////////////////////

void SubspaceIdentification::ComputeSystemMatricesBiased(
	const MatrixT<float>& stateSequence, const MatrixT<float>& stateSequenceP1, 
	const MatrixT<float>& input, const MatrixT<float>& output, short numBlockRows,
    MatrixT<float>& A, MatrixT<float>& B, MatrixT<float>& C, MatrixT<float>& D, const MatrixT<float>& R)
{
	// Determine the necessary dimensionalities
	short systemOrder = stateSequence.GetRows();
	short numDimsInput = input.GetCols();
	short numDimsOutput = output.GetCols();

	// Determine the number of rows in each "piece" of the stacked hankel
	short numRows1 = numDimsInput*numBlockRows; // U0|i
	short numRows2 = numDimsInput; // Ui|i
	short numRows3 = numDimsInput*(numBlockRows - 1); // Ui+1|2i-1
	short numRows4 = numDimsOutput*numBlockRows; // Y0|i
	short numRows5 = numDimsOutput; // Yi|i

	// Solve for the system matrices
	MatrixT<float> X1;
	MatrixT<float> temp;
	R.GetSubMatrix(numRows1, numRows2, 0, R.GetCols(), temp);
	Stack(stateSequence, temp, X1);

	MatrixT<float> X2;
	R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4, numRows5, 0, 
		R.GetCols(), temp);
	Stack(stateSequenceP1, temp, X2);

	MatrixT<float> XInverse;
	PseudoInverse(X1, XInverse);

	MatrixT<float> BigSolution;
	X2.Multiply(XInverse, BigSolution);

	// Extract A, B, C, and D
	BigSolution.GetSubMatrix(0, systemOrder, 0, systemOrder, A);
	BigSolution.GetSubMatrix(0, systemOrder, systemOrder, numDimsInput, B);
	BigSolution.GetSubMatrix(systemOrder, numDimsOutput, 0, systemOrder, 
		C);
	BigSolution.GetSubMatrix(systemOrder, numDimsOutput, systemOrder, 
		numDimsInput, D);
}

////////////////////////////////////////////////////////////////////////////////

void SubspaceIdentification::CalculateObservabilityMatrixRobust(
	const MatrixT<float>& input, const MatrixT<float>& output, short numBlockRows, 
	short numColsHankel, MatrixT<float>& R, MatrixT<float>& observe, 
	float epsilon)
{
	// Some temporary matrices used for intermediate results
	MatrixT<float> temp1;
	MatrixT<float> temp2;
	MatrixT<float> temp3;
	MatrixT<float> temp4;

	// Compute the stacked hankel matrix
	MatrixT<float> stackedHankel;
	ComputeStackedHankelMatrix(input, output, numColsHankel, numBlockRows, 
		stackedHankel);

	// Determine the dimensionality of the input and output
	short numDimsInput = input.GetCols();
	short numDimsOutput = output.GetCols();

	// Determine the number of rows in each "piece" of the stacked hankel
	short numRows1 = numDimsInput*numBlockRows; // U0|i
	short numRows2 = numDimsInput; // Ui|i
	short numRows3 = numDimsInput*(numBlockRows - 1); // Ui+1|2i-1
	short numRows4 = numDimsOutput*numBlockRows; // Y0|i
	short numRows5 = numDimsOutput; // Yi|i
	short numRows6 = numDimsOutput*(numBlockRows - 1); // Yi+1|2i-1

	// Do the LQ Decomposition
	LQDecomposition(stackedHankel, temp2, temp1, false);
	temp2.GetSubMatrix(0, 
		numRows1+numRows2+numRows3+numRows4+numRows5+numRows6, 0, 
		numRows1+numRows2+numRows3+numRows4+numRows5+numRows6, R);

	printf("  Computed LQ Decomposition\n");

	// Future Outputs
	MatrixT<float> Rf;
	R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4, numRows5+numRows6,
		0, R.GetCols(), Rf);

	// Past Data
	MatrixT<float> Rp;
	R.GetSubMatrix(0, numRows1, 0, R.GetCols(), temp1);
	R.GetSubMatrix(numRows1+numRows2+numRows3, numRows4, 0, R.GetCols(),
		temp2);
	Stack(temp1, temp2, Rp);

	// Future Inputs
	MatrixT<float> Ru;
	R.GetSubMatrix(numRows1, numRows2+numRows3, 0, 
		numRows1+numRows2+numRows3, Ru);

	// Invert the Future Inputs
	MatrixT<float> RuInverse;
	PseudoInverse(Ru, RuInverse);

	// Perpendicular Future Outputs
	MatrixT<float> Rfp;
	Rf.GetSubMatrix(0, Rf.GetRows(), 0, numRows1+numRows2+numRows3, temp1);
	temp1.Multiply(RuInverse, temp3);
	temp3.Multiply(Ru, temp2);
	temp1.Subtract(temp2, temp3);
	Rf.GetSubMatrix(0, Rf.GetRows(), numRows1+numRows2+numRows3, 
		numRows4+numRows5+numRows6, temp1);
	Append(temp3, temp1, Rfp);

	// Perpendicular Past
	MatrixT<float> Rpp;
	Rp.GetSubMatrix(0, Rp.GetRows(), 0, numRows1+numRows2+numRows3, temp1);
	temp1.Multiply(RuInverse, temp3);
	temp3.Multiply(Ru, temp2);
	temp1.Subtract(temp2, temp3);
	Rp.GetSubMatrix(0, Rp.GetRows(), numRows1+numRows2+numRows3, 
		numRows4+numRows5+numRows6, temp1);
	Append(temp3, temp1, Rpp);

	// The oblique projection
	MatrixT<float> Ob;
	PseudoInverse(Rpp, temp1);
	Rfp.Multiply(temp1, temp2);
	temp2.Multiply(Rp, Ob);

	Printf("  Computed Ob\n");

	// Perform extra projection of Ob on Uf perpendicular
	MatrixT<float> WOW;
	Ob.GetSubMatrix(0, Ob.GetRows(), 0, numRows1+numRows2+numRows3, temp1);
	temp1.Multiply(RuInverse, temp3);
	temp3.Multiply(Ru, temp2);
	temp1.Subtract(temp2, temp3);
	Ob.GetSubMatrix(0, Ob.GetRows(), numRows1+numRows2+numRows3, 
		numRows4+numRows5+numRows6, temp1);
	Append(temp3, temp1, WOW);

	// Do the SVD part
	MatrixT<float> U;
	VectorT<float> W;
	MatrixT<float> V;
	SVD(WOW, U, W, V);

	printf("  Did SVD for system order\n");

	// Compute the order of the system
	float sum = 0;
	for(short i = 0; i < W.GetSize(); i++)
	{
		if(W[i] > epsilon) sum += W[i];
	}
	short systemOrder = 0;
	float eigenCutOff = sum*.5f;
	sum = 0;
	for(short i = 0; i < W.GetSize(); i++)
	{
		if(sum < eigenCutOff) 
		{
			systemOrder++;
			sum += W[i];
		}
		else
		{
			break;
		}
	}

	printf("  Found system order of %d", systemOrder);
	if(systemOrder)
	{
		printf(" with highest and lowest singular values %f and %f", W[0], 
			W[systemOrder-1]);
	}
	if(systemOrder < W.GetSize() - 1)
	{
		printf(" (Passed On %f)", W[systemOrder]);
	}
	printf("\n");

	// Find Observability Matrix
	U.GetSubMatrix(0, U.GetRows(), 0, systemOrder, observe);
	for(short i = 0; i < observe.GetRows(); i++)
	{
		for(short j = 0; j < observe.GetCols(); j++)
		{
			observe[i][j] *= sqrt(W[j]);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void SubspaceIdentification::ComputeSystemMatricesRobust(const MatrixT<float>& input, 
											  const MatrixT<float>& output, 
											  short numBlockRows, 
											  MatrixT<float>& observe, MatrixT<float>& R, 
											  MatrixT<float>& A, MatrixT<float>& B, 
											  MatrixT<float>& C, MatrixT<float>& D, 
											  bool makeStable)
{
	// Some temporary matrices used for intermediate results
	MatrixT<float> temp1;
	MatrixT<float> temp2;
	MatrixT<float> temp3;
	MatrixT<float> temp4;

	// Determine the necessary dimensionalities
	short numDimsInput = input.GetCols();
	short numDimsOutput = output.GetCols();
	short systemOrder = observe.GetCols();

	// Determine the number of rows in each "piece" of the stacked hankel
	short numRows1 = numDimsInput*numBlockRows; // U0|i
	short numRows2 = numDimsInput; // Ui|i
	short numRows3 = numDimsInput*(numBlockRows - 1); // Ui+1|2i-1
	short numRows4 = numDimsOutput*numBlockRows; // Y0|i
	short numRows5 = numDimsOutput; // Yi|i
	short numRows6 = numDimsOutput*(numBlockRows - 1); // Yi+1|2i-1

	// Compute A and C
	if(makeStable)
	{
		// Compute observem1
		MatrixT<float> observem1;
		observe.GetSubMatrix(numDimsOutput, 
			observe.GetRows()-numDimsOutput, 0, observe.GetCols(), temp1);
		temp2.Init(numDimsOutput, observe.GetCols(), 0);
		Stack(temp1, temp2, observem1);

		// Calculate A and C
		observe.GetSubMatrix(0, numDimsOutput, 0, observe.GetCols(), C);
		PseudoInverse(observe, temp1);
		temp1.Multiply(observem1, A);

		printf("  Calculated A and C with forced stability\n");
	}
	else
	{
		// Compute observem1
		MatrixT<float> observem1;
		observe.GetSubMatrix(0, observe.GetRows()-numDimsOutput, 0, 
			observe.GetCols(), observem1);

		// Set up the least squares problem
		MatrixT<float> RHS;
		PseudoInverse(observe, temp1);
		R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4, 
			numRows5+numRows6, 0, numRows1+numRows2+numRows3+numRows4, temp2);
		temp1.Multiply(temp2, temp3);
		temp1.Init(systemOrder, numDimsOutput, 0);
		Append(temp3, temp1, temp2);
		R.GetSubMatrix(numRows1, numRows2+numRows3, 0, 
			numRows1+numRows2+numRows3+numRows4+numRows5, temp1);
		Stack(temp2, temp1, RHS);

		MatrixT<float> LHS;
		PseudoInverse(observem1, temp1);
		R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4+numRows5, 
			numRows6, 0, numRows1+numRows2+numRows3+numRows4+numRows5, temp2);
		temp1.Multiply(temp2, temp3);
		R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4, numRows5, 0, 
			numRows1+numRows2+numRows3+numRows4+numRows5, temp1);
		Stack(temp3, temp1, LHS);

		// Solve
		MatrixT<float> BigSolution;
		PseudoInverse(RHS, temp1);
		LHS.Multiply(temp1, BigSolution);

		// Extract A and C
		BigSolution.GetSubMatrix(0, systemOrder, 0, systemOrder, A);
		BigSolution.GetSubMatrix(systemOrder, numDimsOutput, 0, 
			systemOrder, C);

		printf("  Calculated A and C without forced stability\n");
	}

	// Recompute Observability Matrices
	observe = C;
	MatrixT<float> prevTerm = MatrixT<float>(C);
	for(int i = 1; i < numBlockRows; i++)
	{
		prevTerm.Multiply(A, temp1);
		prevTerm = temp1;
		Stack(observe, prevTerm, temp1);
		observe = temp1;
	}

	MatrixT<float> observem1;
	observe.GetSubMatrix(0, observe.GetRows()-numDimsOutput, 0, 
		observe.GetCols(), observem1);

	printf("  Recomputed observability matrices\n");

	// Invert Observability matrices
	MatrixT<float> observeInverse;
	PseudoInverse(observe, observeInverse);

	MatrixT<float> observem1Inverse;
	PseudoInverse(observem1, observem1Inverse);

	///// Compute B and D

	// Set up the least squares problem
	MatrixT<float> RHS;
	R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4, numRows5+numRows6,
		0, numRows1+numRows2+numRows3+numRows4, temp2);
	observeInverse.Multiply(temp2, temp3);
	temp1.Init(systemOrder, numDimsOutput, 0);
	Append(temp3, temp1, RHS);

	MatrixT<float> LHS;
	R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4+numRows5, numRows6,
		0, numRows1+numRows2+numRows3+numRows4+numRows5, temp2);
	observem1Inverse.Multiply(temp2, temp3);
	R.GetSubMatrix(numRows1+numRows2+numRows3+numRows4, numRows5, 0,
		numRows1+numRows2+numRows3+numRows4+numRows5, temp1);
	Stack(temp3, temp1, LHS);

	// Determine Residual
	MatrixT<float> P;
	Stack(A, C, temp1);
	temp1.Multiply(RHS, temp3);
	LHS.Subtract(temp3, temp1);
	temp1.GetSubMatrix(0, temp1.GetRows(), 0, numRows1+numRows2+numRows3,
		P);

	printf("  Calculated Residual\n");

	// Build the Kronecker Tensor Product
	MatrixT<float> Q;
	R.GetSubMatrix(numRows1, numRows2+numRows3, 0,
		numRows1+numRows2+numRows3, Q);

	MatrixT<float> L1;
	A.Multiply(observeInverse, L1);

	MatrixT<float> L2;
	C.Multiply(observeInverse, L2);

	MatrixT<float> M;
	temp1.Init(systemOrder, numDimsOutput, 0);
	Append(temp1, observem1Inverse, M);

	MatrixT<float> X;
	temp1.MakeIdentity(numDimsOutput);
	temp2.Init(numDimsOutput, systemOrder, 0);
	Append(temp1, temp2, temp3);
	temp1.Init(numDimsOutput*(numBlockRows-1), numDimsOutput, 0);
	Append(temp1, observem1, temp2);
	Stack(temp3, temp2, X);

	MatrixT<float> totm;
	for(short i = 0; i < numBlockRows; i++)
	{
		MatrixT<float> N;
		M.GetSubMatrix(0, M.GetRows(), i*numDimsOutput,
			(numBlockRows-i)*numDimsOutput, temp1);
		L1.GetSubMatrix(0, L1.GetRows(), i*numDimsOutput,
			(numBlockRows-i)*numDimsOutput, temp2);
		temp1.Subtract(temp2, temp3);
		temp1.Init(systemOrder, i*numDimsOutput, 0);
		Append(temp3, temp1, temp2);
		L2.GetSubMatrix(0, L2.GetRows(), i*numDimsOutput,
			(numBlockRows-i)*numDimsOutput, temp1);
		temp1.Scale(-1);
		temp3.Init(numDimsOutput, i*numDimsOutput, 0);
		Append(temp1, temp3, temp4);
		Stack(temp2, temp4, N);

		if(!i)
		{
			temp1.MakeIdentity(numDimsOutput);
			N.GetSubMatrix(systemOrder, numDimsOutput, 0, numDimsOutput,
				temp2);
			temp1.Add(temp2, temp3);
			for(short j = 0; j < numDimsOutput; j++)
			{
				for(short k = 0; k < numDimsOutput; k++)
				{
					N[systemOrder+j][k] = temp3[j][k];
				}
			}
		}

		N.Multiply(X, temp1);
		Q.GetSubMatrix(i*numDimsInput, numDimsInput, 0, Q.GetCols(),
			temp2);
		temp2.Transpose(temp3);
		ComputeKronProduct(temp3, temp1, temp2);
		if(i)
		{
			totm.Add(temp2);
		}
		else
		{
			totm = temp2;
		}
	}

	printf("  Done calculating kronecker tensor product\n");

	VectorT<float> vectorP = VectorT<float>(P.GetRows()*P.GetCols(), 0);
	for(short i = 0; i < P.GetCols(); i++)
	{
		for(short j = 0; j < P.GetRows(); j++)
		{
			vectorP[i*P.GetRows()+j] = P[j][i];
		}
	}

	// Solve
	VectorT<float> vectorBD;
	PseudoInverse(totm, temp1);
	temp1.Transform(vectorP, vectorBD);

	MatrixT<float> BD = MatrixT<float>(systemOrder+numDimsOutput, numDimsInput);
	for(short i = 0; i < numDimsInput; i++)
	{
		for(short j = 0; j < systemOrder+numDimsOutput; j++)
		{
			BD[j][i] = vectorBD[i*P.GetRows()+j];
		}
	}

	// Extract B and D
	BD.GetSubMatrix(0, numDimsOutput, 0, BD.GetCols(), D);
	BD.GetSubMatrix(numDimsOutput, systemOrder, 0, BD.GetCols(), B);
}
