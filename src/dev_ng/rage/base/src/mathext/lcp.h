//
// mathext/lcp.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef MATHEXT_LCP_H
#define MATHEXT_LCP_H

#include "vector/vector3.h"

#define SPARSE_SHOOTING	0

#if SPARSE_SHOOTING==1
/*
PURPOSE:
	atPriorityArray is an atArray with two lists of integers for ordering the array elements. One list holds
	the priority for each element, and the other list holds the index numbers of the elements in order of priority.
<FLAG Component>
*/
template <class _Type, int _MaxSize>
class atPriorityArray
{
public:
	// PURPOSE: Resets queue to be empty
	void Reset() {
		m_Size = 0;
	}

	// PURPOSE: Default constructor
	atPriorityArray() { Reset(); }

	// PURPOSE: Destructor
	~atPriorityArray() {}

	// PURPOSE: Get size of the array and priority queue.
	// RETURNS: Count of objects
	int GetSize() const { return m_Size; }

	// PURPOSE: Get the total size of the array and priority queue
	// RETURNS: Total allocated space for the array and priority queue
	int GetAllocSize() const { return _AllocSize; }

	// PURPOSE: Initialize the array of objects and the priority queue
	// PARAMS: Number of objects to initialize and initial value.
	void InitValues ( int numValues, const _Type& initialValue )
	{
		FastAssert( numValues<=_MaxSize );		// APRIL 17, 2005
		m_Size = numValues;
		for ( int i=0; i<numValues; i++ ) {
			m_Array[i] = initialValue;
			m_PQ[i] = i;
			m_PQindices[i] = i;
		}
	}

	// PURPOSE: Get the item with lowest priority value.
	// RETURNS: The value of lowest priority, and
	//			Its index in the array.
	const _Type& GetHead( int *index )
	{
		*index = m_PQ[0];
		return m_Array[*index];
	}

	// PURPOSE: Change the value of an entry in the array.
	//			Also updates the priority queue.
	void SetValue( int i, const _Type& newValue ) {
		if ( newValue < m_Array[i] ) {
			LowerValue( i, newValue );
		}
		else {
			RaiseValue(i, newValue );
		}
	}

	// PURPOSE: Change a value to one of lower (i.e., earlier) priority.
	// PARAMS: Index and new value
	void LowerValue( int i, const _Type& newValue ) {
		FastAssert( 0<=i && i<m_Size );
		FastAssert( !(m_Array[i]<newValue) );
		m_Array[i] = newValue;
		int j = m_PQindices[i];		// Index to where value is stored in the priority queue
		while ( j>0 ) {				// Loop to bubble new value upward
			int k = Parent(j);
			int m = m_PQ[k];
			if ( !(newValue < m_Array[m]) ) {
				break;
			}
			m_PQ[j] = m_PQ[k];
			m_PQindices[m_PQ[k]] = j;
			j = k;
		}
		m_PQ[j] = i;
		m_PQindices[i] = j;
	}

	// PURPOSE: Change a value to one of higher (i.e., later) priority.
	// PARAMS: Index and new value
	void RaiseValue( int i, const _Type& newValue ) {
		FastAssert( 0<=i && i<m_Size );
		FastAssert( !(newValue<m_Array[i]) );
		m_Array[i] = newValue;
		int j = m_PQindices[i];
		while ( true ) {
			int child = LeftChild(j);
			if ( child>=m_Size ) {
				break;
			}
			_Type* childValue = &m_Array[m_PQ[child]];
			if ( child+1<m_Size && m_Array[m_PQ[child+1]]<(*childValue) ) {
				childValue = &m_Array[m_PQ[child+1]];
				child = child+1;
			}
			if ( !((*childValue)<newValue) ) {
				break;
			}
			m_PQ[j] = m_PQ[child];
			m_PQindices[m_PQ[child]] = j;
			j = child;
		}
		m_PQ[j] = i;
		m_PQindices[i] = j;
	}

	// PUROPOSE: Get the array element with the given index.
	// RETURN: the array element with the given index
	const _Type& operator[](int i) const {			
		FastAssert ( i>=0 && i<_MaxSize );
		return m_Array[i];
	}

	// PURPOSE: Get a pointer to the whole array of elements.
	// RETURN: a pointer to the whole array of elements
	const _Type* GetArray() const {
		return m_Array;
	}

private:

	// Use a zero based array for the priority queue.
	static int LeftChild( int i ) { return (i<<1)+1; }
	static int Parent( int i ) { return (i-1)>>1; }

private:
	int m_Size;			// Number of items currently in the array and priority queue

	_Type m_Array[_MaxSize];		// Holds the array of values	

	// m_PQ: Holds the priority queue as a binary heap 
	//    m_PQ[i]=j means that m_Array[j] is the i-th entry in the priority queue heap.
	int m_PQ[_MaxSize];				 

	// m_PQindices[] Holds indices of items in the priority queue.
	//    m_PQindices[i] is the position of array item #i in the priority queue.
	int m_PQindices[_MaxSize];		

};

#endif // end of #if SPARSE_SHOOTING==1


/*
	PURPOSE:
		This file provides a collection of functions for setting up and solving the linear complimentarity
		problem by the Gauss-Seidel method.

	NOTES:
		Three methods are included for solving LCP problems:
		1.	SolveLcpGS solves with no friction forces.
		2.	SolveMlcpFriction1GS solves frictional constraints in one dimension only.  I.e. a "box"-like
			friction.  Would allow static friction in one direction at the same time as sliding friction
			in another direction. Allows different coefficients of friction in different directions.
		3.	SolveMlcpFriction2GS solves frictional constraints in the more-correct planar model.
			Frictional force can always oppose the direction of sliding (if not static). This function also
			supports the functionality of the "1D" function.

		Both routines with friction use the same coefficient of friction for
		both static and sliding friction (may change in the future
		extensions of the software).

		An array frictionCoef[] gives the coefficient of friction for
		each direction.  That is, CoefFric[i] gives the coefficient
		of friction for the force/acceleration (or impulse/velocity)
		of the i-th components of force/acceleration.

		An array frictionMode[i] describes which contacts are tengential
		frictional forces.  There are three main usages (usually, perhaps
		only the third usage is present).

		Usage 1: Normal force with no tangential frictional forces.
		frictionMode[i] = -1   -- this force and acceleration is *normal*
		frictionCoef[i] is irrelevant.

		Usage 2: Normal force with no tangential frictional forces.  
		Here i < j is required: usually j = i+1.
		frictionMode[i] = -1  -- this force and acceleration is *normal*
									frictionCoef[i] is irrelevant.
		frictionMode[j] = i   -- this force and acceleration is *tangential*
								-- it corresponds to the normal force # i
								-- it uses frictionCoef[j].

		Usage 3: Normal force with no tangential frictional forces.  
		Here i < j is required: usually j = i+1.
		Only supported by SolveLCPFriction2GS!
		frictionMode[i] = -1	-- this force and acceleration is *normal*
									frictionCoef[i] is irrelevant.
		frictionMode[j] = i		-- this force and acceleration is *tangential*
								-- it corresponds to the normal force # i
								-- it uses frictionCoef[j].
		frictionMode[j+1] = i	-- this force and acceleration is *tangential*
								-- it corresponds to the normal force # i
								-- it uses frictionCoef[j+1].
		The two tangential forces are presumed to be orthogonal.

		The initial author was Sam Buss, May 2004.

	<FLAG Component>
*/

namespace rage {


/*
class phConstraintGraphNode;
class phConstraintGraphEdge;

enum {
	LCP_CONSTRAINT_TYPE_3DOFCONTACT,
	LCP_CONSTRAINT_TYPE_1DOFCONTACT,
	LCP_CONSTRAINT_TYPE_3DOF_NONCOMPL,		// No complementarity conditions
	LCP_CONSTRAINT_TYPE_ETC
};


// phConstractGraphEdge
//	Holds a description of a single edge.  (Or "constraint".)
class phConstraintGraphEdge
{
public:
	phConstraintGraphEdge() { m_sync = 0; }

	// operator=() - Does this need to be defined?

public:
	int m_bodies[2];		// The two bodies connected by this constraint

	int m_nDof;				// Either 1 or 3

	int m_iX;				// Index into the solution vector (and q vector)

	int m_type;				// Constraint type (See enum above)

	float m_k;				// Frictional value, or whatever

	mutable int m_sync;		// Should be initialized to zero
};

// phConstraintGraphNode
//		- Holds a list of edges (constraints) that are incident on 
//			a given node (body).
class phConstraintGraphNode
{
public:
	// Array of pointers to edges (contacts)
	phConstraintGraphEdge** m_edges;	// Pointers
	int m_numEdges;						// Size of the array

	int m_id;							// The index of this body (use to keep track of identity)

	bool m_Fixed;						// Unused by "buss" code.
};

// The entire graph is described by the phConstraintGraph
class phConstraintGraph
{
public:
	phConstraintGraph(int maxNodes, int maxEdges) { m_nodes.Resize(maxNodes); m_edges.Resize(maxEdges); }

public:
	// Array of "nodes" that describe a body in the system
	atArray<phConstraintGraphNode> m_nodes;
	// Array of "edges", each describing a contact or constraint or joint limit
	atArray<phConstraintGraphEdge> m_edges;

	// Array of pointers to the edges belonging to a node.
	//		I.e., the contacts that apply to a body.
	// QUESTION: THIS SEEMS REDUNDANT.  IS THIS ON PURPOSE?
	//Array<phConstraintGraphEdge*> m_edgePtrs;

	// QUESTION: IS THIS THE SAME AS WHAT MICHEAL USES THIS FOR?
	int m_dim;				// The number of degrees of freedom (total for all edges)

	int m_nConstraint;		// The number of contacts/constraints

	// Lookup table giving the Jacobian that corresponds to a constraint
	//	In this case, it is contains pointers to the full array in ROW order
	atArray<const float*>  m_constraintPairToJacobian;

public:

	inline int GetNumDofs( ) const { return m_dim; }

	inline void SetJacobianIndex( int constraintIdxA, int constraintIdxB, const float* jac )
	{
		// Effect of constraint A force on contraint B velocity
		m_constraintPairToJacobian[constraintIdxA*m_nConstraint+constraintIdxB] = jac;
	}

	inline const float* GetJacobian( int constraintIdxA, int constraintIdxB ) const
	{
		return m_constraintPairToJacobian[constraintIdxA*m_nConstraint+constraintIdxB];
	}

	inline const float* GetJacobianDiagonal( int constraintIdx ) const
	{
		return m_constraintPairToJacobian[constraintIdx*(m_nConstraint+1)];
	}

};*/

#if SPARSE_SHOOTING==1
class phConstraintGraph;
#endif

namespace LCPSolver
{
	// PURPOSE: Solve a linear system of equations that include inequalities.
	// PARAMS:
	//	dimension	- the dimension of the square input matrix, the input vector and the solution vector
	//	matrix		- list of floats representing the input matrix
	//	vector		- list of floats representing the input vector
	//	solution	- list of floats representing the solution vector
	//	delVector	- list of changes in the input vector
	//	tolerance	- optional lower limit for a number to be considered non-zero when testing the solution accuracy
	//	maxIterations-optional maximum number of iterations to try before stopping with the current solution
	// NOTES:
	//	1.	This could be used by the contact manager to solve for multiple interacting contact forces with no friction.
	//		It was used during development of the contact manager, and it not currently in use.
	//	2.	This uses a projected Gauss-Seidel iterative method (Cottle, Pang, Stone, chapter 5).
	void SolveLcpGS (int dimension, const float* matrix, const float* vector, float* solution, float* delVector,
						float* tempArray, float tolerance=1.0e-5f, int maxIterations=256);

	// PURPOSE: Solve a linear system of equations that include inequalities, with special complementary
	//			conditions that apply to friction forces.
	// PARAMS:
	//	dimension	- the dimension of the square input matrix, the input vector and the solution vector
	//	matrix		- list of floats representing the input matrix
	//	vector		- list of floats representing the input vector
	//	frictionMode- list of integers to indicate whether sliding is occuring, when this is used for contact forces
	//	frictionCoef- list of friction coefficients along each dimension, when this is used for contact forces
	//	solution	- list of floats representing the solution vector
	//	delVector	- list of changes in the input vector
	//	tolerance	- optional lower limit for a number to be considered non-zero when testing the solution accuracy
	//	maxIterations-optional maximum number of iterations to try before stopping with the current solution
	// NOTES:
	//	1.	This could be used by the contact manager to solve for multiple interacting contact forces with
	//		frictional forces that are independent in each tangent direction. It was used during development
	//		of the contact manager, and it not currently in use.
	//	2.	This uses a projected Gauss-Seidel iterative method (Cottle, Pang, Stone, chapter 5).
	void SolveMlcpFriction1GS (int dimension, const float* matrix, const float* vector, const int* frictionMode,
								const float* frictionCoef, float* solution, float* delVector, float* tempArray,
								float tolerance=1.0e-5f, int maxIterations=32);

	// PURPOSE: Solve a linear system of equations.
	// PARAMS:
	//	dimension	- the dimension of the square input matrix, the input vector and the solution vector
	//	matrix		- list of floats representing the input matrix
	//	vector		- list of floats representing the input vector
	//	frictionMode- list of integers to indicate whether sliding is occuring, when this is used for contact forces
	//	frictionCoef- list of friction coefficients along each dimension, when this is used for contact forces
	//	solution	- list of floats representing the solution vector
	//	delVector	- list of changes in the input vector
	//	tolerance	- optional lower limit for a number to be considered non-zero when testing the solution accuracy
	//	maxIterations-optional maximum number of iterations to try before stopping with the current solution
	// NOTES: This is used by the contact manager to solve for multiple interacting contact forces.
	int SolveMlcpFriction2GS (int dimension, const float* matrix, const float* vector, const int* frictionMode,
								const float* frictionCoef, float* solution, float* delVector, float* tempArray,
								float* tempSolution, float* tempDelVector, bool startWithTempSolution=false,
								float tolerance=1.0e-5f, int maxIterations=32);
	
	void SolveMlcpFriction2J (int dimension, const float* matrix, const float* vector, const int* frictionMode,
								const float* frictionCoef, float* solution, float* delVector, float* tempArray,
								float* tempSolution, float* tempDelVector, float tolerance=1.0e-5f, int maxIterations=32);

	// PURPOSE:    Solve the Modified Linear Complementarity Problem with special complementary conditions that apply
	//				to frictional forces.
	// PARAMS:
	//  dimension - dimension of the LCP problem.
	//  matrix - an n x n matrix, diagonal entries MUST be positive!
	//		Entries in M are given in *row* order.
	//  q - an column vector of length n
	//  frictionMode - see below.  Identifies frictional forces
	//	coefFric - array of n coefficients of friction
	//  zReturn - also a column vectors of length n (returned values)
	//	diagInverse array needs length at least (dimension/3)*9 to make a 3x3 matrix for each contact
	//  stopFraction - a stopping criterion (in terms of root mean
	//				square error.  At the second iteration onwards,
	//				the change in the solution (as a vector) is less
	//				than fraction "stopFraction" of the solution, then
	//				the iteration terminates.
	//	zeroTolerance.  If the magnitude of the solution is less than
	//		this, it is deemed to be close enough to zero to count as zero.
	//  maxIterations - a backup stopping criterion.
	// NOTES:
	//		Frictional forces are done in pairs, taking into account both tangential directions at once.
	//		The functionality of SolveMlcpFriction1GS is also supported.
	//		This version uses a projected Gauss-Seidel iterative method, see Cottle, Pang, Stone, chapter 5.
	//		The lower triangular matrix includes 3x3 blocks from the diagonal.
	//		All 3x3 items MUST come first in the array, followed by all 1x1 items.  In practice, this means that 
	//		contacts must have 3 components, normal and then two tangential.  All contacts come first, then come all
	//		1DOF values (namely, from 1Dof or 3Dof joints:  3Dof joints are treated as three separate 1Dof values).

	//  There is a further stopping criterion which is
	//		automatically applied if the iterations fail
	//		to converge to a better solution on at least two tries.
	//
	// ******************************
	//
	//  An array CoefFric[] gives the coefficient of friction for
	//	each direction.  That is, CoefFric[i] gives the coefficient
	//	of friction for the force/acceleration (or impulse/velocity)
	//	of the i-th components of force/acceleration.
	//
	//  An array FrictionalMode[i] describes which contacts are tengential
	//	frictional forces.  There are two permitted usages -- all occurences
	//  the first usage MUST come before any occurance of the second usage.
	//
	// Usage #1:  A normal force and two tangential forces.  They must obey a
	//		a circular frictional restriction.

	//		FrictionalMode[i] = -1   -- this force and acceleration is *normal*
	//									CoefFric[i] is irrelevant.
	//		FrictionalMode[i+1] = i    -- this force and acceleration is *tangential*
	//									-- it corresponds to the normal force # i
	//									-- it uses CoefFric[j].
	//		FrictionalMode[j+1] = i+1  -- this force and acceleration is *tangential*
	//									-- it corresponds to the normal force # i
	//									-- it also uses CoefFric[j].
	//		The normal force and the two tangential forces are presumed to be orthogonal.
	//		(The format is a little strange, but is designed to be backward
	//		compatible with the "2GS" and "2J" methods.
	//
	// Usage #2: A single force (no frictional limits), e.g. a 1Dof joint, or one
	//		component of a 3Dof joint.
	void SolveMlcpFriction3GS3x3 (int dimension, const float* matrix, const float* q, const int* frictionMode,
									const float* coefFric, float* zReturn, float* diagInverse, float stopFraction,
									float tolerance=1.0e-5f, int maxIterations=16);

	// MODIFIED LCP FRICTION SOLVER using SHOOTING METHOD
	// Initial author: Sam Buss (SB), June 2004.
	//			Modified: January 2005.  SB
	//			March 2005.  The "3GS3x3" version.
	//				With (a) uses 3x3 solver
	//					 (b) new conventions on stopping criteria
	//					 (c) no longer permits single axis for friction.
	//			APril 2005.  Converted to a shooting method.
	// Function prototypes
	//  "3" = New conventions on stopping criteria.
	//	"GS" = "Gauss-Seidel"
	//  "J" = "Jacobian"
	//  "GS3x3" - Gauss-Seidel with 3x3 blocks
	//  "Sh3x3" - Shooting method with 3x3 blocks
	// SolveMlcpFriction3Sh3x3
	//    Solve the Modified Linear Complementarity Problem with special
	//		complementary conditions that apply to frictional
	//		forces.  Frictional forces are done in pairs, taking
	//		into account both tangential directions at once.
	//    This version uses a projected iterative shooting method
	//		based on splitting.
	//	  The lower triangular matrix includes 3x3 blocks from the 
	//		diagonal.
	//	  All 3x3 items MUST come first in the array, followed
	//		by all 1x1 items.  In practice, this means that 
	//		contacts must have 3 components, normal and then two
	//		tangential.  All contacts come first, then come all
	//		1DOF values (namely, from 1Dof or 3Dof joints:  3Dof
	//		joints are treated as three separate 1Dof values.)
	//		Linear constraints (with out complementarity conditions)
	//		may be included with the 3x3 items and must (a) come beore
	//		all 1Dof items, (b) come in 3x3 versions.
	//
	//  n - dimension of the LCP problem.
	//  M - an n x n matrix, diagonal entries MUST be positive!
	//		Entries in M are given in *row* order.
	//  q - an column vector of length n
	//  FrictionalMode - see below.  Identifies frictional forces
	//	CoefFric - array of n coefficients of friction
	//  z - also a column vectors of length n (returned values)
	//  stopFraction - a stopping criterion (in terms of root mean
	//				square error.  At the second iteration onwards,
	//				the change in the solution (as a vector) is less
	//				than fraction "stopFraction" of the solution, then
	//				the iteration terminates.
	//	zeroTolerance.  If the magnitude of the solution is less than
	//		this, it is deemed to be close enough to zero to count as zero.
	//  maxIterations - a backup stopping criterion.
	//  There is a further stopping criterion which is
	//		automatically applied if the iterations fail
	//		to converge to a better solution on at least two tries.
	//  An array CoefFric[] gives the coefficient of friction for
	//	each direction.  That is, CoefFric[i] gives the coefficient
	//	of friction for the force/acceleration (or impulse/velocity)
	//	of the i-th components of force/acceleration.
	//
	//  An array FrictionalMode[i] describes which contacts are tengential
	//	frictional forces.  There are two permitted usages -- all occurences
	//  the first usage MUST come before any occurance of the second usage.
	//
	// Usage #1:  A normal force and two tangential forces.  They must obey a
	//		a circular frictional restriction.

	//		FrictionalMode[i] = -1   -- this force and acceleration is *normal*
	//									CoefFric[i] is irrelevant.
	//		FrictionalMode[i+1] = i    -- this force and acceleration is *tangential*
	//									-- it corresponds to the normal force # i
	//									-- it uses CoefFric[j].
	//		FrictionalMode[j+1] = i+1  -- this force and acceleration is *tangential*
	//									-- it corresponds to the normal force # i
	//									-- it also uses CoefFric[j].
	//		The normal force and the two tangential forces are presumed to be orthogonal.
	//		(The format is a little strange, but is designed to be backward
	//		compatible with the "2GS" and "2J" methods.
	//
	// Usage #2: A single force (no frictional limits), e.g. a 1Dof joint, or one
	//		component of a 3Dof joint.
	//
	// Usage #3: FrictionalMode[i] = -2  - First of three forces with no complementarity
	//										conditions.
	//			 FrictionalMode[i+1] = i;
	//			 FrictionalMode[i+2] = i+1;
	void SolveMlcpFriction3Sh3x3 (int dimension, const float* matrix, const float* q, const int* frictionMode,
									const float* coefFric, float* zReturn, float* diagInverse, float* tempArray,
									float* delTempArray, float stopFraction, float zeroTolerance=1.0e-5f, 
									int maxIterations=4);

#if SPARSE_SHOOTING==1
	void SolveMlcpFriction3Sh3x3Sparse (const phConstraintGraph& theSparseMatrixGraph, const float* q, float* zReturn, 
										float* R, float* deltaR, float* diagInverse, float stopFraction, float zeroTolerance=1.0e-5f,
										int maxIterations=4);
#endif

	void Solve2DFriction (int i, float r, int dimension, const float* mRowPtr, float fricLimit, const float* solution,
							const float* rhs, float& f1, float& f2, bool noTemps=false);

	// PURPOSE: Solve forces with frictional limits
	// NOTES:
	//	1.	Always gives a force that gives zero normal velocity, without exceeding frictional limits.
	//	2.	The direction of the normal force is the positive x-axis.
	//
	//	3.	This fills in the given force array with (M^inverse)( r1, r2, r3 )^T  if the given conditions respect
	//		frictional limits. Otherwise returns a force vector at which the frictional limit is exactly matched
	//		and which does a reasonably good job of more-or-less minimizing tangential velocity.  (Unfortunately
	//		to do this exactly would require solving a quartic, which would be more costly than is reasonable).
	void Solve3x3WithFriction (const float* matrixInv, float friction, const float* v, float* force);

	// PURPOSE: Compute the inner (dot) product of two vectors.
	// PARAMS:
	//	dimension -	the dimension of the two input vectors
	//	vector1 -	the first input vector, defined by an array of floats
	//	vector2 -	the second input vector, defined by an array of floats
	// RETURN:	the sum of all the vector elements multiplied in matching pairs from each vector
	// NOTES: Entries are presumed to be stored consecutively in memory.
	float InnerProduct (int dimension, const float* vector1, const float* vector2);

	// PURPOSE: Checks whether the given square matrix has all positive diagonal elements.
	// PARAMS:
	//	dimension -	the dimension of the given square matrix
	//	matrix -	the square matrix to check, defined by an array of floats
	// RETURN:	true if all the diagonal elements are positive, false if not all are positive
	// NOTES:	This triggers a DebugAssert failure before returning false.
	bool CheckPositiveDiagonal (int dimension, const float* matrix);

	// Checks MLCP solution for accuracy
	// Returns a non-negative float indicating error level (unit-less ratio)
	// A value close to zero is good.  A value near 1.0 or higher is very bad.
	// PURPOSE: Mostly debugging and testing.  Might be used in a production environment to 
	//		decide quality of convergence of Mlcp iterations (but should be modified
	//		if used for that to precompute inverse mass equivalence).
	float CheckMlcpAnswer (int dim, const float* M, const float* q, const int* fricMode, const float* z);

	// PURPOSE: Invert the given matrix (represented by an array of floats) with the assumption that matrix is symmetric
	//			and positive definite.
	// PARAMS:	matrix - a 3x3 matrix represented by an array of 9 floats
	// NOTES:
	//	1.	This changes the given matrix if needed to make it positive definite before inverting it.
	//	2.	This only uses lower part of the matrix.
	//	3.	No checking is done for symmetry.
	//	4.	Positive definiteness is only partially guarded against. If the eigenvalues are all positive and not too
	//		different in magnitude, this will compute the correct inverse. If one or more eigenvalues are too close
	//		to zero, this will compute the inverse of a slightly modified matrix. A better solution would restart the
	//		calculation, but the present method is faster and should work acceptably.
	void InvertPositiveDefinite (float* matrix, const int vec3Stride);
} // namespace LCPSolver


class LCPData
{
public:
	// PURPOSE: Allocate memory for the LCP data.
	// PARAMS:
	//	dimension	- the maximum dimension of the square input matrix, the input vector and the solution vector
	// NOTES: This is not in the constructor to avoid accidental run-time memory allocation.
	void Init (int maxDimension);

	// PURPOSE: Temporary interface for creating data for output of sparse solver
	void InitSolutionVector (int dimension);

	// PURPOSE: Release the memory allocated by Init.
	void Shutdown ();

	// PURPOSE: Set the dimension of the square LCP input matrix and the LCP input vector.
	// PARAMS:
	//	dimension	- the dimension of the square input matrix, the input vector and the solution vector
	void SetDimension (int dimension);

	inline int GetDimension () const { return m_Dimension; }

	// PURPOSE: Initialize the square LCP input matrix by making all of its elements zero.
	void ZeroInputMatrix ();

	// PURPOSE: Get the maximum dimension of the square LCP input matrix and the LCP input vector.
	int GetMaxDimension () const { return m_MaxDimension; }

	// PURPOSE: Add the list of 9 elements to a 3x3 block in the input matrix.
	// PARAMS:
	//	blockRow	- the row of the block to set, counting in 3x3 blocks
	//	blockCol	- the column of the block to set, counting in 3x3 blocks
	//	values		- the list of 9 values to put in the 3x3 block in the input matrix
	void AddToMatrix3x3Block (int blockRow, int blockCol, const float values[3*VEC3_NUM_STORED_FLOATS]);

	// PURPOSE: Get the list of 9 elements in a 3x3 block in the input matrix.
	// PARAMS:
	//	blockRow	- the row of the block to get, counting in 3x3 blocks
	//	blockCol	- the column of the block to get, counting in 3x3 blocks
	//	values		- the list of 9 values to get from the 3x3 block in the input matrix
	void GetMatrix3x3Block (int blockRow, int blockCol, float values[3*VEC3_NUM_STORED_FLOATS]);

	// PURPOSE: Add the list of 3 elements to part of a row in the input matrix.
	// PARAMS:
	//	rowStart	- the row of the input matrix in which to put the first element in the given list
	//	colStart	- the column of the input matrix in which to put the first element in the given list
	//	values		- the list of 3 values to put in the input matrix
	void AddToMatrix3VectorRow (int rowStart, int colStart, const float values[3]);

	// PURPOSE: Get the list of 3 elements from part of a row in the input matrix.
	// PARAMS:
	//	rowStart	- the row of the input matrix from which to get the first element in the given list
	//	colStart	- the column of the input matrix from which to get the first element in the given list
	//	values		- the list of 3 values to get from the input matrix
	void GetMatrix3VectorRow (int rowStart, int colStart, float values[3]);

	// PURPOSE: Add the list of 3 elements to part of a column in the input matrix.
	// PARAMS:
	//	rowStart	- the row of the input matrix in which to put the first element in the given list
	//	colStart	- the column of the input matrix in which to put the first element in the given list
	//	values		- the list of 3 values to put in the input matrix
	void AddToMatrix3VectorCol (int rowStart, int colStart, const float values[3]);

	// PURPOSE: Get the list of 3 elements from part of a column in the input matrix.
	// PARAMS:
	//	rowStart	- the row of the input matrix from which to get the first element in the given list
	//	colStart	- the column of the input matrix from which to get the first element in the given list
	//	values		- the list of 3 values to get from the input matrix
	void GetMatrix3VectorCol (int rowStart, int colStart, float values[3]);

	// PURPOSE: Set a single element in the input matrix.
	// PARAMS:
	//	row		- the row of the input matrix in which to put the given element
	//	col		- the column of the input matrix in which to put the given element
	//	value	- the value to put in the input matrix
	void SetMatrixElement (int row, int col, float value);

	// PURPOSE: Get a single element from the input matrix.
	// PARAMS:
	//	row		- the row of the input matrix from which to get the element
	//	col		- the column of the input matrix from which to get the element
	// RETURN:	the value in the specified position in the input matrix
	float GetMatrixElement (int row, int col) const;

	// PURPOSE: Get a single element from the input matrix by linear index (instead of row index and column index).
	// PARAMS:
	//	linearIndex		- the linear position in the input matrix from which to get the element
	// RETURN:	the value in the specified position in the input matrix
	float GetInputElement (int linearIndex) const;

	// PURPOSE: Set a single element in the input vector.
	// PARAMS:
	//	index	- the position in the input vector in which to put the given element
	//	value	- the value to put in the input vector
	void SetInputVectorElement (int index, float value) { m_InputVector[index] = value; }

	// PURPOSE: Add to a single element in the input vector.
	// PARAMS:
	//	index	- the position in the input vector in which to add the given element
	//	value	- the value to add to the input vector
	void AddToInputVectorElement (int index, float value) { m_InputVector[index] += value; }

	// PURPOSE: Set 3 consecutive elements of the input vector.
	// PARAMS:
	//	firstIndex	- the index number of the first element to set
	//	element0	- the value of the first element
	//	element1	- the value of the second element
	//	element2	- the value of the third element
	void SetInputVector3Elements (int firstIndex, float element0, float element1, float element2);

	// PURPOSE: Set one element in the friction coefficient array.
	// PARAMS:
	//	index		- the index of the element to set in the friction coefficient array
	//	value		- the value to set in the friction coefficient array
	void SetFrictionCoefElement (int index, float value) { m_FrictionCoef[index] = value; }

	// PURPOSE: Set 3 elements in the friction coefficient array.
	// PARAMS:
	//	index		- the index of the first element to set in the friction coefficient array
	//	coef0		- the first of 3 elements to set in the friction coefficient array
	//	coef1		- the second of 3 elements to set in the friction coefficient array
	//	coef2		- the third of 3 elements to set in the friction coefficient array
	void SetFrictionCoef (int index, float coef0, float coef1, float coef2);

	// PURPOSE: Set 2 elements in the friction coefficient array.
	// PARAMS:
	//	index		- the index of the element for the normal direction
	//	friction	- the value to set in the friction coefficient array
	void SetTransverseFriction (int index, float friction);

	// PURPOSE: Set one element in the friction mode array to -1 (unused).
	// PARAMS:
	//	index		- the index of the element to set in the friction mode array
	void SetFrictionModeElement (int index) { m_FrictionMode[index] = -1; }
	int GetFrictionModeElement (int index) { return m_FrictionMode[index]; }

	// PURPOSE: Set 3 elements in the friction mode array.
	// PARAMS:
	//	index		- the index of the element for the normal direction
	//	constraint	- whether this is a constraint (no separating allowed) instead of a collision
	void SetFrictionMode (int index, bool constraint=false);

	// PURPOSE: Get an element from the solution array.
	// PARAMS:
	//	index		- the index of the element to get from the solution array
	float GetSolution (int index) const;

	// PURPOSE: Get three successive elements from the solution array.
	// PARAMS:
	//	index		- the index of the first element to get from the solution array
	void GetSolutionVector (int index, Vector3& solution) const;

	// PURPOSE: Use the LCP solver to find the solution ot the matrix equation set up in this LCPdata.
	// NOTES:	This calls LCPSolver::SolveLcpGS with data in this LCPdata as arguments.
	void SolveLcpGS ();

	// PURPOSE: Use the LCP solver to find the solution to the matrix equation set up in this LCPdata.
	// NOTES:	This calls LCPSolver::SolveMlcpFriction2GS with data in this LCPdata as arguments.
	void SolveMlcpFriction2GS ();

	// PURPOSE: Use the LCP solver to find the solution to the matrix equation set up in this LCPdata.
	// NOTES:	This calls LCPSolver::SolveMlcpFriction3GS3x3 with data in this LCPdata as arguments.
	void SolveMlcpFriction3GS3x3 ();

	// PURPOSE: Use the LCP solver to find the solution to the matrix equation set up in this LCPdata.
	// PARAMS:
	//	stopFraction -	optional stopping limit for the root mean square error
	//	zeroTolerance -	optional maximum magnitude of the solution that is close enough to zero
	//  maxIterations - optional maximum number of iterations used in the solver
	// NOTES:	This calls LCPSolver::SolveMlcpFriction3Sh3x3 with data in this LCPdata as arguments.
	void SolveMlcpFriction3Sh3x3 (float stopFraction=0.0001f, float zeroTolerance=1.0e-5f, int maxIterations=4);

#if SPARSE_SHOOTING==1
	void SolveMlcpFriction3Sh3x3Sparse (const phConstraintGraph& theSparseMatrixGraph);
#endif

	// PURPOSE:	Condense the input matrix to use only the normal component of every 3 dimensional contact.
	// PARAMS:
	//	numTriples -	the number of 3x3 diagonal blocks (one for each 3-dimensional contact)
	//	numSingles -	the number of single matrix elements (one for each joint limit degree of freedom)
	// NOTES:	This is used to change the input matrix after it has been used for find contact forces so that
	//			it can be used to find pushes.
	void ReduceDimensionForPushes (int numTriples, int numSingles);

	float* GetSolutionData() { return m_Solution; }

protected:
	float* m_InputMatrix;
	float* m_InputVector;
	float* m_DelInputVector;
	float* m_FrictionCoef;
	float* m_Solution;
	float* m_TempArray;
	float* m_TempSolution;
	float* m_TempDelInput;
	int* m_FrictionMode;
	int m_Dimension;
	int m_MaxDimension;
};

//=============================================================================
// Implementations


inline void LCPSolver::InvertPositiveDefinite (float* matrix, const int vec3Stride)
{
	// Add the diagonal elements, make sure the sum is not negative, and choose the nearly-zero limit.
	float trace = matrix[0]+matrix[vec3Stride+1]+matrix[vec3Stride*2+2];//m11+m22+m33;
	float epsilon = Max(1.0e-8f,fabsf(1.0e-5f*trace));

	// Make sure the first matrix element is not nearly zero.
	matrix[0] = Max(epsilon,matrix[0]);

	// Form a L * D * L^T representation.
	//   L = ( (1 0 0)(a 0 0)(b 0 0) * * ( (1 0 0)(0 1 0)(0 c 1) ).
	//	 D is diagonal. - d1, d2, d3 are inverses of the diagonal entries.
	float d1 = 1.0f/matrix[0];
	float a = matrix[vec3Stride]*d1;//m12*d1;
	float b = matrix[vec3Stride*2]*d1;//m13*d1;
	float u22star = matrix[vec3Stride+1]-matrix[vec3Stride]*a;//m22 - m12*a;
	float u23star = matrix[vec3Stride*2+1]-matrix[vec3Stride]*b;//m23 - m12*b;
	float u33star = matrix[vec3Stride*2+2]-matrix[vec3Stride*2]*b;//m33 - m13*b;
	//mthAssertf(u22star>-epsilon, "fails when two non-reacting bodies hit (creature and door)");
	u22star = Max(epsilon,u22star);
	float d2 = 1.0f/u22star;
	float c = u23star*d2;
	float u33starstar = u33star-u23star*c;
	//mthAssertf(u33starstar>-epsilon, "fails when two non-reacting bodies hit (creature and door)");
	u33starstar = Max(epsilon,u33starstar);
	float d3 = 1.0f/u33starstar;

	// Compute the inverse and write it back into the matrix.
	matrix[vec3Stride*2+2] = d3;//m33 = d3;
	matrix[vec3Stride*2+1] = matrix[vec3Stride+2] = -c*d3;//m23 = m32 = -c*d3;
	matrix[vec3Stride+1] = d2-c*matrix[vec3Stride*2+1];//m22 = d2 - c*m23;
	float acminusb = a*c - b;
	matrix[vec3Stride*2] = matrix[2] = acminusb*d3;//m13 = m31 = acminusb*d3;
	float ad2 = a*d2;
	matrix[vec3Stride] = matrix[1] = -c*matrix[vec3Stride*2]-ad2;//m12 = m21 = -c*m13 - ad2;
	matrix[0] = d1+a*ad2+acminusb*matrix[vec3Stride*2];//m11 = d1 + a*ad2 + acminusb*m13;
}


inline float LCPData::GetSolution (int index) const
{
	return m_Solution[index];
}


inline void LCPData::GetSolutionVector (int index, Vector3& solution) const
{
	solution.Set(m_Solution[index],m_Solution[index+1],m_Solution[index+2]);
}

} // namespace rage

#endif // MATHEXT_LCP_H
