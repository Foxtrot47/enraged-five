// 
// shaderMaterial/shaderMaterialGeoParamValue.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_H

#include "parser/macros.h"
#include "grCore/effect.h"
#include "grCore/effect_typedefs.h"
#include "string/string.h"

#include "shaderMaterialGeoTemplate.h"
//#include "shaderMaterialGeoType.h"
//#include "shaderMaterialGeoInstance.h"

//#define FORCE_GEO_BASED_MTLS

namespace rage {

class grcEffect;
typedef grcEffect::VariableInfo grmVariableInfo;
class fiTokenizer;

class shaderMaterialGeoParamValue
{
public:

	shaderMaterialGeoParamValue(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValue(const shaderMaterialGeoParamValue*	pobSourceParam)	{Copy(pobSourceParam);}
	virtual ~shaderMaterialGeoParamValue();

	// Pure virtuals
	virtual int	GetType()				const {return grcEffect::VT_NONE;}
	virtual const char * GetTypeName()	const {return grcEffect::GetTypeName(grcEffect::VT_NONE);}

	// Accessors
	const char *	GetName()		const {return m_Name.c_str();}
	int		GetSourceOfParamValue()	const;
	const shaderMaterialGeoParamDescription* GetParamDescription() const {return m_pobParamDescription;}

    virtual void Reset();

	void Copy(const shaderMaterialGeoParamValue * param);
	virtual bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	// Update functions
	virtual bool Update(const shaderMaterialGeoParamValue * )					{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const bool )											{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const float )											{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const int )												{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const char * )											{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const char *, const float )								{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const char *, const float , const float, const float )	{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const Vector2& )										{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const Vector3& )										{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const Vector4& )										{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const Matrix34& )										{AssertMsg(0,"Invalid call of the Update function"); return false;}
	virtual bool Update(const Matrix44& )										{AssertMsg(0,"Invalid call of the Update function"); return false;}

	virtual void SetDefaultValue(grcEffect & , grcEffectVar & )					{AssertMsg(0,"Invalid call of the SetDefaultValue function");}

	virtual void SetMaterialValue(Vector2 & )									{AssertMsg(0,"Invalid call of the SetMaterialValue function");}
	virtual void CopyValueIntoVector2(Vector2 & ) const								{AssertMsg(0,"Invalid call of the CopyValueIntoVector2 function");}

	// This function reads all the paramdata
	virtual bool ReadGeoParamValueData(fiTokenizer & T);

	// This function writes out all the param data to the .shadergroup format
	virtual bool WriteGeoParamValueData(fiTokenizer & T) const;

	// This function cleans up any of the parameter data
	virtual bool CleanUp() {return true;}

	bool Init(const grmVariableInfo & varInfo, grcEffect & effect);

	void SetName(const char * name)
	{
		m_Name = name;
	}

	enum LIGHTMAP_TYPES
	{
		LT_LIGHTMAP = 0,
		LT_LIGHTMAP_HDR_COLOR,
		LT_LIGHTMAP_HDR_EXP,
		LIGHTMAP_TYPE_COUNT
	};

	static const char * LIGHTMAP_TYPE_NAMES[LIGHTMAP_TYPE_COUNT];

	bool IsLightmap(const int lightmapType) const;

	virtual void	WriteAsGeoTypeMember(parTreeNode* /*pobParent*/, const char* /*pcTemplateName*/) const	{AssertMsg(0,"Invalid call to WriteAsGeoTypeMember");}

protected:
	shaderMaterialGeoParamValue(const parTreeNode*	pobGeoParamValueGeoRootNode);

	// Written data
	ConstString			m_Name;			// Semantic name

	// For backwards referencing
	const shaderMaterialGeoParamDescription*	m_pobParamDescription;
//	const shaderMaterialGeoType*		m_pobGeoType;
//	const shaderMaterialGeoInstance*	m_pobGeoInstance;
};

} // end using namespace rage

#endif
