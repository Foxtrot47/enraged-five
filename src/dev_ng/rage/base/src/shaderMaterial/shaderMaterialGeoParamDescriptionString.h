// 
// shaderMaterial/shaderMaterialGeoParamDescriptionString.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_STRING_H
#define SHADER_MATERIAL_GEO_PARAM_STRING_H

#include "parser/macros.h"
#include "string/string.h"

#include "shaderMaterialGeoParamDescription.h"

namespace rage {

class shaderMaterialGeoParamDescriptionString : public shaderMaterialGeoParamDescription
{
public:

	shaderMaterialGeoParamDescriptionString();
	virtual ~shaderMaterialGeoParamDescriptionString();

	shaderMaterialGeoParamDescriptionString(const parTreeNode*	pobGeoParamDescriptionGeoRootNode);

	// Virtual functions
	int	GetType()				const	{return grcEffect::VT_STRING;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_STRING);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase=true);
	bool IsEqual(const shaderMaterialGeoParamDescription * param) const;

	bool CleanUp();

	bool ReadGeoParamDescriptionData(fiTokenizer & T);
	bool WriteGeoParamDescriptionData(fiTokenizer & T) const;

	// Non virtual function
	const char * GetDefaultValue() const {return (const char *)m_sDefaultValue;}
	void		 SetDefaultValue(const char * szDefaultValue);

	virtual void	WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const;
	virtual void	WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// DefaultValue for this param
	ConstString m_sDefaultValue;
};

} // end namespace rage

#endif
