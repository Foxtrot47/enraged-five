// 
// shaderMaterial/shaderMaterialGeoParamValueFloat.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamValueFloat.h"

#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"
#include "vector/vector2.h"


using namespace rage;

shaderMaterialGeoParamValueFloat::shaderMaterialGeoParamValueFloat(const shaderMaterialGeoParamDescription*	pobParamDescription) : shaderMaterialGeoParamValue(pobParamDescription)
{
	Reset();
}

shaderMaterialGeoParamValueFloat::shaderMaterialGeoParamValueFloat(const shaderMaterialGeoParamValueFloat*	pobSourceParam)
:shaderMaterialGeoParamValue(pobSourceParam)
{
	Copy(pobSourceParam);
}

shaderMaterialGeoParamValueFloat::~shaderMaterialGeoParamValueFloat()
{
	Reset();
}

shaderMaterialGeoParamValueFloat::shaderMaterialGeoParamValueFloat(const parTreeNode*	pobGeoParamValueGeoRootNode)
: shaderMaterialGeoParamValue(pobGeoParamValueGeoRootNode)
{
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamValueGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "floatvalue") == 0)
		{
			float fValue = pobChildValueNode->GetElement().FindAttributeFloatValue("init", 0.0f);
			SetValue(fValue);
		}
	}
}

void shaderMaterialGeoParamValueFloat::Reset()
{
	shaderMaterialGeoParamValue::Reset();

	m_fValue = 0;
}

void shaderMaterialGeoParamValueFloat::Copy(const shaderMaterialGeoParamValueFloat * param)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamValue::Copy(param);

	m_fValue = ((shaderMaterialGeoParamValueFloat *)param)->m_fValue;
}

bool shaderMaterialGeoParamValueFloat::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!shaderMaterialGeoParamValue::IsEqual(param))
	{
		return false;
	}

	return m_fValue == ((shaderMaterialGeoParamValueFloat *)param)->m_fValue;
}

bool shaderMaterialGeoParamValueFloat::Update(const float f)
{
	if (m_fValue != f)
	{
		m_fValue = f;
		CleanUp();
		return true;
	}
	
	return false;
}

bool shaderMaterialGeoParamValueFloat::Update(const shaderMaterialGeoParamValue * param)
{
	Assert(GetType() == param->GetType());

	return Update(((shaderMaterialGeoParamValueFloat *)param)->m_fValue);
}

void shaderMaterialGeoParamValueFloat::SetDefaultValue(grcEffect & effect, grcEffectVar & var)
{
	effect.GetVar(var, m_fValue);
}

void shaderMaterialGeoParamValueFloat::SetMaterialValue(Vector2 & vValue)
{
	vValue.x = m_fValue;
	vValue.y = 0.0f;
}

bool shaderMaterialGeoParamValueFloat::CleanUp()
{
	if (m_fValue < GetParamDescription()->GetUIMin())
	{
		m_fValue = GetParamDescription()->GetUIMin();
	}

	if (m_fValue > GetParamDescription()->GetUIMax())
	{
		m_fValue = GetParamDescription()->GetUIMax();
	}

	return true;
}

bool shaderMaterialGeoParamValueFloat::ReadGeoParamValueData(fiTokenizer & T)
{
	shaderMaterialGeoParamValue::ReadGeoParamValueData(T);

	m_fValue = T.GetFloat();

	return true;
}

bool shaderMaterialGeoParamValueFloat::WriteGeoParamValueData(fiTokenizer & T) const
{
	shaderMaterialGeoParamValue::WriteGeoParamValueData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %f", GetTypeName(), m_fValue);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}


void	shaderMaterialGeoParamValueFloat::WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open string
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add floatvalue
	parTreeNode* pobFloatNode = rage_new parTreeNode();
	pobFloatNode->GetElement().SetName("floatvalue");
	pobFloatNode->GetElement().AddAttribute("value", GetValue());
	pobFloatNode->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}

void shaderMaterialGeoParamValueFloat::CopyValueIntoVector2(Vector2 & vData) const
{
	vData.x = GetValue();
	vData.y = 0.0f;
}

