// 
// shaderMaterial/shaderMaterialGeoParamDescriptionVector3.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_VECTOR3_H
#define SHADER_MATERIAL_GEO_PARAM_VECTOR3_H

#include "parser/macros.h"
#include "vector/vector3.h"

#include "shaderMaterialGeoParamDescription.h"

namespace rage {

class shaderMaterialGeoParamDescriptionVector3 : public shaderMaterialGeoParamDescription
{
public:

	shaderMaterialGeoParamDescriptionVector3();
	virtual ~shaderMaterialGeoParamDescriptionVector3();

	shaderMaterialGeoParamDescriptionVector3(const parTreeNode*	pobGeoParamDescriptionGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_VECTOR3;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_VECTOR3);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase=true);
	bool IsEqual(const shaderMaterialGeoParamDescription * param) const;

	bool ReadGeoParamDescriptionData(fiTokenizer & T);
	bool WriteGeoParamDescriptionData(fiTokenizer & T) const;

	const Vector3 & GetDefaultValue() const {return m_vDefaultValue;}
	void SetDefaultValue(const Vector3& vDefaultValue) {m_vDefaultValue.Set(vDefaultValue);}

	virtual void	WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const;
	virtual void	WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// DefaultValue for this param
	Vector3 m_vDefaultValue;
};

} // end namespace rage

#endif
