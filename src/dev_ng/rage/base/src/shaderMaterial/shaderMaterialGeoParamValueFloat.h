// 
// shaderMaterial/shaderMaterialGeoParamValueFloat.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_FLOAT_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_FLOAT_H

#include "parser/macros.h"
#include "shaderMaterialGeoParamValue.h"

namespace rage {

class shaderMaterialGeoParamValueFloat : public shaderMaterialGeoParamValue
{
public:

	shaderMaterialGeoParamValueFloat(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValueFloat(const shaderMaterialGeoParamValueFloat*	pobSourceParam);
	virtual ~shaderMaterialGeoParamValueFloat();

	shaderMaterialGeoParamValueFloat(const parTreeNode*	pobGeoParamValueGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_FLOAT;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_FLOAT);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamValueFloat * param);
	bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	bool Update(const shaderMaterialGeoParamValue * param);

	bool Update(const float f);

	bool Update(const bool b)														{return shaderMaterialGeoParamValue::Update(b);}
	bool Update(const int n)														{return shaderMaterialGeoParamValue::Update(n);}
	bool Update(const char * texture)												{return shaderMaterialGeoParamValue::Update(texture);}
	bool Update(const char * param, const float f)									{return shaderMaterialGeoParamValue::Update(param, f);}
	bool Update(const char * param, const float f0, const float f1, const float f2)	{return shaderMaterialGeoParamValue::Update(param, f0, f1, f2);}

	void SetDefaultValue(grcEffect & effect, grcEffectVar & var);

	void SetMaterialValue(Vector2 & vValue);
	void CopyValueIntoVector2(Vector2 & vData) const;

	bool CleanUp();

	bool LoadFromXML(const char * szFilename);
	bool SaveToXML(const char * szFilename) const;

	bool ReadGeoParamValueData(fiTokenizer & T);
	bool WriteGeoParamValueData(fiTokenizer & T) const;

	float GetValue() const {return m_fValue;}
	void SetValue(float fValue) {m_fValue = fValue;}

	virtual void	WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// Value for this param
	float m_fValue;
};

} // end namespace rage

#endif
