// 
// shaderMaterial/shaderMaterialGeoParamDescription.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"

#include "shaderMaterialGeoParamDescription.h"

using namespace rage;

const char * shaderMaterialGeoParamDescription::LIGHTMAP_TYPE_NAMES[] =
{
	"lightmap",
	"lightmapColorHDR",
	"lightmapExpHDR"
};

shaderMaterialGeoParamDescription::shaderMaterialGeoParamDescription()
{
}

shaderMaterialGeoParamDescription::~shaderMaterialGeoParamDescription()
{
}

void shaderMaterialGeoParamDescription::Reset()
{
	m_Name		= "None";
	m_UIName	= "None";
	m_UIWidget	= "None";
	m_UIHelp	= "None";
	m_UIHint	= "None";
	m_UIMin		= 0.0f;
	m_UIMax		= 1.0f;
	m_UIStep	= 0.01f;
	m_UIHidden  = false;
	m_UvSetIndex  = 0;
	m_UvSetName = "map1";
	m_TextureOutputFormats = "GUESS_COLOR";
	m_iNoOfMipLevels = 0;
	m_MaterialDefaultValueUvSetIndex = -1;
	m_eValueSource = GeoInstance | GeoType | GeoTemplate;
}

void shaderMaterialGeoParamDescription::Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase)
{
	if (copyBase)
	{
		m_UIMin		= param->m_UIMin;
		m_UIMax		= param->m_UIMax;
		m_UIStep	= param->m_UIStep;

		m_Name		= param->m_Name;
		m_UIName	= param->m_UIName;
		m_UIWidget	= param->m_UIWidget;
		m_UIHelp	= param->m_UIHelp;
		m_UIHint	= param->m_UIHint;
		m_UIHidden  = param->m_UIHidden;
		m_UvSetIndex= param->m_UvSetIndex;
		m_UvSetName = param->m_UvSetName;
		m_TextureOutputFormats = param->m_TextureOutputFormats;
		m_iNoOfMipLevels = param->m_iNoOfMipLevels;
		m_MaterialDefaultValueUvSetIndex = param->m_MaterialDefaultValueUvSetIndex;
		m_eValueSource = param->m_eValueSource;
	}
}

bool shaderMaterialGeoParamDescription::IsEqual(const shaderMaterialGeoParamDescription * param) const
{
	if (!param)
	{
		return false;
	}

	if (GetType() != param->GetType())
	{
		return false;
	}

	// Name is the only important parameter that needs to determine something is equal
	if (strcmp((const char *)m_Name, (const char *)param->m_Name)!=0)
	{
		return false;
	}

	if(m_eValueSource != param->m_eValueSource)
	{
		return false;
	}

	return true;
}

bool shaderMaterialGeoParamDescription::ReadGeoParamDescriptionData(fiTokenizer & T)
{	
	T.MatchToken("Value");
	return true;
}

bool shaderMaterialGeoParamDescription::WriteGeoParamDescriptionData(fiTokenizer & T) const
{
	T.StartLine();
	T.PutStr("%s", m_Name.m_String);
	T.EndLine();

	return true;
}

bool shaderMaterialGeoParamDescription::IsLightmap(const int lightmapType) const
{
	if (!m_UIHidden || GetType() != grcEffect::VT_TEXTURE)
		return false;
	
	if (stricmp((const char *)m_UIName, LIGHTMAP_TYPE_NAMES[lightmapType])==0)
	{
		return true;
	}

	return false;
}

shaderMaterialGeoParamDescription::shaderMaterialGeoParamDescription(const parTreeNode*	pobGeoParamDescriptionGeoRootNode)
{
	// Clear it all up
	Reset();

	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamDescriptionGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildDefaultValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "uihint") == 0)
		{
			const char* pcDefaultValue = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("init", "none", NULL, 0);
			SetUIHint(pcDefaultValue);
		}
		if(strcmp(pcNodeName, "uiwidget") == 0)
		{
			const char* pcDefaultValue = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("init", "none", NULL, 0);
			SetUIWidget(pcDefaultValue);
		}
		else if(strcmp(pcNodeName, "globalvar") == 0)
		{
			bool bDefaultValue = pobChildDefaultValueNode->GetElement().FindAttributeBoolValue("init", 0);
			SetValueSource(bDefaultValue ? GeoType : (GeoInstance | GeoType));
		}
		else if(strcmp(pcNodeName, "materialdatauvsetindex") == 0)
		{
			int iDefaultValue = pobChildDefaultValueNode->GetElement().FindAttributeIntValue("init", -1);
			SetDefaultValueUvSetIndex(iDefaultValue);
		}
	}
}
