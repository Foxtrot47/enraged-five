// 
// shaderMaterial/shaderMaterialGeoParamValueBool.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if 0
#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_BOOL_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_BOOL_H

#include "parser/manager.h"
#include "shaderMaterialGeoParamValue.h"

namespace rage {

class shaderMaterialGeoParamValueBool : public shaderMaterialGeoParamValue
{
public:
	shaderMaterialGeoParamValueBool(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValueBool(const shaderMaterialGeoParamValueBool*	pobSourceParam);
	virtual ~shaderMaterialGeoParamValueBool();

	shaderMaterialGeoParamValueBool(const parTreeNode*	pobGeoParamValueGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_BOOL;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_BOOL);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamValueBool * param);
	bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	bool Update(const shaderMaterialGeoParamValue * param);

	bool Update(const bool b);

	bool Update(const float f)														{return shaderMaterialGeoParamValue::Update(f);}
	bool Update(const int n)														{return shaderMaterialGeoParamValue::Update(n);}
	bool Update(const char * texture)												{return shaderMaterialGeoParamValue::Update(texture);}
	bool Update(const char * param, const float f)									{return shaderMaterialGeoParamValue::Update(param, f);}
	bool Update(const char * param, const float f0, const float f1, const float f2)	{return shaderMaterialGeoParamValue::Update(param, f0, f1, f2);}

	void SetDefaultValue(grcEffect & effect, grcEffectVar & var);

	void SetMaterialValue(Vector2 & vValue);
	void CopyValueIntoVector2(Vector2 & vData) const;

	bool ReadGeoParamValueData(fiTokenizer & T);
	bool WriteGeoParamValueData(fiTokenizer & T) const;

	bool GetValue() const {return m_bValue;}
	void SetValue(bool bValue) {m_bValue = bValue;}

	virtual void	WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:
	// Value for this param
	bool m_bValue;
};

} // end namespace rage

#endif
#endif
