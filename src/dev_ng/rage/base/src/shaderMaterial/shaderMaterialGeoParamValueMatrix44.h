// 
// shaderMaterial/shaderMaterialGeoParamValueMatrix44.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_MATRIX44_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_MATRIX44_H

#include "parser/macros.h"
#include "vector/matrix44.h"

#include "shaderMaterialGeoParamValue.h"

namespace rage {

class shaderMaterialGeoParamValueMatrix44 : public shaderMaterialGeoParamValue
{
public:

	shaderMaterialGeoParamValueMatrix44(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValueMatrix44(const shaderMaterialGeoParamValueMatrix44*	pobSourceParam);
	virtual ~shaderMaterialGeoParamValueMatrix44();

	int	GetType()				const	{return grcEffect::VT_MATRIX44;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_MATRIX44);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamValueMatrix44 * param);
	bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	bool Update(const shaderMaterialGeoParamValue * param);
	bool Update(const char * param, const float f);
	bool Update(const Matrix44& m);

	void SetDefaultValue(grcEffect & effect, grcEffectVar & var);

	bool ReadGeoParamValueData(fiTokenizer & T);
	bool WriteGeoParamValueData(fiTokenizer & T) const;

	Matrix44 GetValue() const {Matrix44 m; m.a = m_mValueA; m.b = m_mValueB; m.c = m_mValueC; m.d = m_mValueD; return m;}
	void SetValue(const Matrix44& obMatrix) 
	{
		m_mValueA = obMatrix.a; 
		m_mValueB = obMatrix.b; 
		m_mValueC = obMatrix.c; 
		m_mValueD = obMatrix.d; 
	}

	virtual void	WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// Helper functions
	bool UpdateVector(const char * param, const float f, const int len, Vector4 & v);

	// Value for this param
	Vector4 m_mValueA;
	Vector4 m_mValueB;
	Vector4 m_mValueC;
	Vector4 m_mValueD;
};

} // end namespace rage

#endif
