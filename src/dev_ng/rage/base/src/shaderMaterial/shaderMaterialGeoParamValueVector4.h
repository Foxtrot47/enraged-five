// 
// shaderMaterial/shaderMaterialGeoParamValueVector4.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_VECTOR4_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_VECTOR4_H

#include "parser/macros.h"
#include "vector/vector4.h"

#include "shaderMaterialGeoParamValue.h"

namespace rage {

class shaderMaterialGeoParamValueVector4 : public shaderMaterialGeoParamValue
{
public:

	shaderMaterialGeoParamValueVector4(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValueVector4(const shaderMaterialGeoParamValueVector4*	pobSourceParam);
	virtual ~shaderMaterialGeoParamValueVector4();

	shaderMaterialGeoParamValueVector4(const parTreeNode*	pobGeoParamValueGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_VECTOR4;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_VECTOR4);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamValueVector4 * param);
	bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	bool Update(const shaderMaterialGeoParamValue * param);
	bool Update(const char * param, const float f0, const float f1, const float f2);
	bool Update(const char * param, const float f);
	bool Update(const Vector4& obValue);

	void SetDefaultValue(grcEffect & effect, grcEffectVar & var);

	bool ReadGeoParamValueData(fiTokenizer & T);
	bool WriteGeoParamValueData(fiTokenizer & T) const;

	const Vector4 & GetValue() const {return m_vValue;}
	void SetValue(const Vector4& vValue) {m_vValue.Set(vValue);}

	virtual void	WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// Value for this param
	Vector4 m_vValue;
};

} // end namespace rage

#endif
