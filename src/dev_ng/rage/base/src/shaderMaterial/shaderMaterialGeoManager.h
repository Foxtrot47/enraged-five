// 
// shaderMaterialGeoManager/shaderMaterialGeoManager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef SHADER_MATERIAL_GEO_MANAGER_H
#define SHADER_MATERIAL_GEO_MANAGER_H

#include "atl/map.h"
#include "string/string.h"
#include "shaderMaterialGeoTemplate.h"
#include "shaderMaterialGeoType.h"
#include "shaderMaterialGeoInstance.h"
#include "grmodel/shader.h"

#define CleanFilename(a,b)	StringNormalize(a, b, sizeof(a));

namespace rage {

	class shaderMaterialGeoManager
	{
	public:
		// Singleton access
		static shaderMaterialGeoManager* GetShaderMaterialGeoManager()
		{
			if(!m_gShaderMaterialGeoManager) Initialize();
			return m_gShaderMaterialGeoManager;
		}
		static void Initialize()
		{
			if(!m_gShaderMaterialGeoManager) m_gShaderMaterialGeoManager = rage_new shaderMaterialGeoManager();
		}
		static void Shutdown()
		{
			delete m_gShaderMaterialGeoManager;
			m_gShaderMaterialGeoManager = NULL;
		}

		// Accessors

		//WARNING!!! WARNING!!! WARNING!!! WARNING!!!
		// DO NOT HOLD ONTO THESE POINTERS use filename or guid as identifier
		const shaderMaterialGeoTemplate *	CreateShaderMaterialGeoTemplateFromShader(const char* pcShaderPathAndFilename);
		const shaderMaterialGeoType *		CreateShaderMaterialGeoTypeFromTemplate(const char* pcGeoTemplatePathAndFilename, bool asNew = false);
		const shaderMaterialGeoInstance *	CreateShaderMaterialGeoInstanceFromType(const char* pcGeoTypePathAndFilename);
		const shaderMaterialGeoInstance *	CreateShaderMaterialGeoInstanceFromTemplate(const char* pcGeoTemplatePathAndFilename);
		// DO NOT HOLD ONTO THESE POINTERS use filename or guid as identifier
		//WARNING!!! WARNING!!! WARNING!!! WARNING!!!

		//WARNING!!! WARNING!!! WARNING!!! WARNING!!!
		// DO NOT HOLD ONTO THESE POINTERS use filename or guid as identifier
		const shaderMaterialGeoTemplate *	GetShaderMaterialGeoTemplate(const char* pcGeoTemplatePathAndFilename) const;
		shaderMaterialGeoType *				GetShaderMaterialGeoType(const char* pcGeoTypePathAndFilename);
		shaderMaterialGeoInstance *			GetShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename);
		shaderMaterialGeoInstance *			GetShaderMaterialGeoInstanceFromUID(const char* pcGeoInstanceUID);
		// DO NOT HOLD ONTO THESE POINTERS use filename or guid as identifier
		//WARNING!!! WARNING!!! WARNING!!! WARNING!!!

		// DO NOT HOLD ONTO THESE POINTERS use filename or guid as identifier
		//WARNING!!! WARNING!!! WARNING!!! WARNING!!!
		const shaderMaterialGeoTemplate *	LoadShaderMaterialGeoTemplate(const char* pcGeoTemplatePathAndFilename);
		const shaderMaterialGeoType *		LoadShaderMaterialGeoType(const char* pcGeoTypePathAndFilename);
		shaderMaterialGeoInstance *			LoadShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename);
		// DO NOT HOLD ONTO THESE POINTERS use filename or guid as identifier
		//WARNING!!! WARNING!!! WARNING!!! WARNING!!!

		bool ReloadShaderMaterialGeoType(const char* pcGeoTypePathAndFilename);
		bool ReloadShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename);
		
		void ReleaseShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename);
		void ReleaseShaderMaterialGeoInstanceWithGUID(const char* pcGeoInstanceUID);

		void AddRefToShaderMaterialGeoInstanceWithGUID(const char* pcGeoInstanceUID);

		void AddChildShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename, grmShader* shader); //connections used by reload
		void RemoveChildShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename, grmShader* shader);//connections used by reload

		void AddChildShaderMaterialGeoInstanceWithGUID(const char* pcGeoInstanceUID, grmShader* shader); //connections used by reload
		void RemoveChildShaderMaterialGeoInstanceWithGUID(const char* pcGeoInstanceUID, grmShader* shader);//connections used by reload

		// Utility functions
		static const atString& GetBaseGEOPath();
		static const atString& GetPathToTemplates();
		static const atString& GetPathToTypes();

	private:

		// Constructors
		shaderMaterialGeoManager();
		~shaderMaterialGeoManager();

		void AddChildShaderMaterialGeoType(const char* pcGeoTypePathAndFilename, unsigned int childHash);//connections used by reload
		void RemoveChildShaderMaterialGeoType(const char* pcGeoTypePathAndFilename, unsigned int childHash);//connections used by reload
		void updateChildren(const shaderMaterialGeoInstance* instance); //used by reload
		void updateChildren(const shaderMaterialGeoType* type); //used by reload

		void ReleaseShaderMaterialGeoTemplate(const char* pcGeoTemplatePathAndFilename);
		void ReleaseShaderMaterialGeoType(const char* pcGeoTypePathAndFilename);

		void getValidTemplateFullPath(char* _OutFullPathBuffer, const char* _FileName) const;
		void getValidTypeFullPath(char* _OutFullPathBuffer, const char* _FileName) const;

		// Singleton
		static shaderMaterialGeoManager* m_gShaderMaterialGeoManager;

		//Pretty lame: should not have to do this .... mapping so reload propagates through all children.
		atMap <unsigned int, atArray<unsigned int > > m_typeChildrenMappingByFileorGuid;
		atMap <unsigned int, atArray<grmShader* > > m_instanceChildrenMappingByFile;
		atMap <unsigned int, atArray<grmShader* > > m_instanceChildrenMappingByGUID;

		//////////////////////////////////////////////////////////////////////////
		//Geo instance
		atArray <shaderMaterialGeoInstance *>	m_apShaderMaterialGeoInstances;
		atArray <short> m_aGeoInstanceRefCounts;

		//Mappings
		atMap <unsigned int, unsigned short> m_shaderMaterialGeoInstanceMappingByFile;
		atMap <unsigned int, unsigned short> m_shaderMaterialGeoInstanceMappingByGUID;

		//unused
		//atArray <unsigned int> m_aUnusedGeoInstanceIDs;
		//Geo instance
		//////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////
		//Geo Type
		atArray <shaderMaterialGeoType *>	m_apShaderMaterialGeoTypes;
		atArray <short> m_aGeoTypeRefCounts;
		
		//Mapping
		atMap <unsigned int, unsigned short> m_shaderMaterialGeoTypeMappingByFile;
		
		//unused
		//atArray <unsigned int> m_aUnusedGeoTypeIDs;
		//Geo Type
		//////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////
		//Geo Template
		atArray <const shaderMaterialGeoTemplate *>	m_apShaderMaterialGeoTemplates;
		atArray <short> m_aGeoTemplateRefCounts;

		//Mapping
		atMap <unsigned int, unsigned short> m_shaderMaterialGeoTemplateMappingByFile;

		//unused
		//atArray <unsigned int> m_aUnusedGeoTemplateIDs;
		//
		//////////////////////////////////////////////////////////////////////////
	};

} // end namespace rage

#define SHADER_MATERIAL_GEO_MANAGER (*shaderMaterialGeoManager::GetShaderMaterialGeoManager())

#endif
