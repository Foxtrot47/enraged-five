// 
// shaderMaterial/shaderMaterialGeoParamValue.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "file/token.h"
#include "grmodel/shader.h"

#include "shaderMaterialGeoParamValue.h"

using namespace rage;

const char * shaderMaterialGeoParamValue::LIGHTMAP_TYPE_NAMES[] =
{
	"lightmap",
	"lightmapColorHDR",
	"lightmapExpHDR"
};

shaderMaterialGeoParamValue::shaderMaterialGeoParamValue(const shaderMaterialGeoParamDescription*	pobParamDescription)
{
	Reset();
	m_pobParamDescription = pobParamDescription;
}

shaderMaterialGeoParamValue::~shaderMaterialGeoParamValue()
{
	Reset();
}

int		shaderMaterialGeoParamValue::GetSourceOfParamValue()	const
{
	Assertf(m_pobParamDescription, "Unable to determine the source for a param's value without a param description");

	// Where a param is stored lives in the param description, not the param value
	
	return m_pobParamDescription->GetSourceOfParamValue();
}

void shaderMaterialGeoParamValue::Reset()
{ 
	m_Name		= "None";
}

void shaderMaterialGeoParamValue::Copy(const shaderMaterialGeoParamValue * param)
{
	m_Name		= param->m_Name;
	m_pobParamDescription = param->m_pobParamDescription;
}

bool shaderMaterialGeoParamValue::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!param)
	{
		return false;
	}

	if (GetType() != param->GetType())
	{
		return false;
	}

	// Name is the only important parameter that needs to determine something is equal
	if (strcmp((const char *)m_Name, (const char *)param->m_Name)!=0)
	{
		return false;
	}

	return true;
}

bool shaderMaterialGeoParamValue::ReadGeoParamValueData(fiTokenizer & T)
{	
	T.MatchToken("Value");
	return true;
}

bool shaderMaterialGeoParamValue::WriteGeoParamValueData(fiTokenizer & T) const
{
	T.StartLine();
	T.PutStr("%s", m_Name.m_String);
	T.EndLine();

	return true;
}

bool shaderMaterialGeoParamValue::IsLightmap(const int lightmapType) const
{
	if (stricmp((const char *)GetParamDescription()->GetUIName(), LIGHTMAP_TYPE_NAMES[lightmapType])==0)
	{
		return true;
	}

	return false;
}

shaderMaterialGeoParamValue::shaderMaterialGeoParamValue(const parTreeNode*	/*pobGeoParamValueGeoRootNode*/)
{
	// Clear it all up
	Reset();
}
