// 
// shaderMaterial/shaderMaterialGeoParamValueString.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_STRING_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_STRING_H

#include "parser/macros.h"
#include "string/string.h"

#include "shaderMaterialGeoParamValue.h"

namespace rage {

class shaderMaterialGeoParamValueString : public shaderMaterialGeoParamValue
{
public:

	shaderMaterialGeoParamValueString(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValueString(const shaderMaterialGeoParamValueString*	pobSourceParam);
	virtual ~shaderMaterialGeoParamValueString();

	shaderMaterialGeoParamValueString(const parTreeNode*	pobGeoParamValueGeoRootNode);

	// Virtual functions
	int	GetType()				const	{return grcEffect::VT_STRING;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_STRING);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamValueString * param);
	bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	bool Update(const shaderMaterialGeoParamValue * param);

	bool Update(const char * data);

	bool Update(const bool b)														{return shaderMaterialGeoParamValue::Update(b);}
	bool Update(const float f)														{return shaderMaterialGeoParamValue::Update(f);}
	bool Update(const int n)														{return shaderMaterialGeoParamValue::Update(n);}
	bool Update(const char * param, const float f)									{return shaderMaterialGeoParamValue::Update(param, f);}
	bool Update(const char * param, const float f0, const float f1, const float f2)	{return shaderMaterialGeoParamValue::Update(param, f0, f1, f2);}

	void SetDefaultValue(grcEffect & effect, grcEffectVar & var);

	bool CleanUp();

	bool ReadGeoParamValueData(fiTokenizer & T);
	bool WriteGeoParamValueData(fiTokenizer & T) const;

	// Non virtual function
	const char * GetValue() const {return (const char *)m_sValue;}
	void		 SetValue(const char * szValue);

	virtual void	WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// Value for this param
	ConstString m_sValue;
};

} // end namespace rage

#endif
