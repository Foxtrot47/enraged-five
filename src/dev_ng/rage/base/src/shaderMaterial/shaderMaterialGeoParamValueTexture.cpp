// 
// shaderMaterial/shaderMaterialGeoParamValueTexture.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamValueTexture.h"

#include "file/asset.h"
#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"

using namespace rage;

shaderMaterialGeoParamValueTexture::shaderMaterialGeoParamValueTexture(const shaderMaterialGeoParamDescription*	pobParamDescription) : shaderMaterialGeoParamValue(pobParamDescription)
{
	Reset();
}

shaderMaterialGeoParamValueTexture::shaderMaterialGeoParamValueTexture(const shaderMaterialGeoParamValueTexture*	pobSourceParam)
:shaderMaterialGeoParamValue(pobSourceParam)
{
	Copy(pobSourceParam);
}

shaderMaterialGeoParamValueTexture::~shaderMaterialGeoParamValueTexture()
{
	Reset();
}

shaderMaterialGeoParamValueTexture::shaderMaterialGeoParamValueTexture(const parTreeNode*	pobGeoParamValueGeoRootNode)
: shaderMaterialGeoParamValue(pobGeoParamValueGeoRootNode)
{
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamValueGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "filename") == 0)
		{
			const char* pcValue = pobChildValueNode->GetElement().FindAttributeStringValue("init", "none", NULL, 0);
			SetValue(pcValue);
		}
	}
}

void shaderMaterialGeoParamValueTexture::Reset()
{
	shaderMaterialGeoParamValue::Reset();
	m_sValue = "none";
}

void shaderMaterialGeoParamValueTexture::Copy(const shaderMaterialGeoParamValueTexture * param)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamValue::Copy(param);

	m_sValue = ((shaderMaterialGeoParamValueTexture *)param)->m_sValue;
}

bool shaderMaterialGeoParamValueTexture::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!shaderMaterialGeoParamValue::IsEqual(param))
	{
		return false;
	}

	return (strcmp((const char *)m_sValue, (const char *)((shaderMaterialGeoParamValueTexture *)param)->m_sValue)==0);
}

bool shaderMaterialGeoParamValueTexture::Update(const char * texture)
{
	if (strcmp((const char *)m_sValue, texture)!=0)
	{
		m_sValue = texture;
		CleanUp();
		return true;
	}

	return false;
}

bool shaderMaterialGeoParamValueTexture::Update(const shaderMaterialGeoParamValue * param)
{
	Assert(GetType() == param->GetType());

	return Update(((shaderMaterialGeoParamValueTexture *)param)->m_sValue);
}

void shaderMaterialGeoParamValueTexture::SetDefaultValue(grcEffect & , grcEffectVar & )
{
	m_sValue = "none";
}

bool shaderMaterialGeoParamValueTexture::CleanUp()
{
	if (strlen(m_sValue)==0)
	{
		m_sValue = "none";
	}
	else
	{
		char szValue[256];
		strcpy(szValue, m_sValue);
		for (u32 i=0; i<strlen(szValue); i++)
		{
			if (szValue[i]=='\\')
			{
				szValue[i] = '/';
			}
		}
		m_sValue = szValue;
	}

	return true;
}

bool shaderMaterialGeoParamValueTexture::ReadGeoParamValueData(fiTokenizer & T)
{
	shaderMaterialGeoParamValue::ReadGeoParamValueData(T);

	char szToken[128];
	T.GetToken(szToken, sizeof(szToken));
	m_sValue = szToken;

	return true;
}

bool shaderMaterialGeoParamValueTexture::WriteGeoParamValueData(fiTokenizer & T) const
{
	shaderMaterialGeoParamValue::WriteGeoParamValueData(T);

	T.StartBlock();
	{
		ConstString sTextureName;
		GetTextureName(sTextureName);

		T.StartLine();
		T.PutStr("%s %s", GetTypeName(), sTextureName.m_String);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void shaderMaterialGeoParamValueTexture::GetTextureName(ConstString & sTextureName, const char * szTextureFilename)
{
	char texName[128];
	memset(texName, 0, sizeof(texName));
	fiAssetManager::BaseName(texName, sizeof(texName), fiAssetManager::FileName(szTextureFilename));
	strcat(texName, ".dds");
	sTextureName = texName;
}

void shaderMaterialGeoParamValueTexture::GetTextureName(ConstString & sTextureName) const
{
	shaderMaterialGeoParamValueTexture::GetTextureName(sTextureName, m_sValue);
}

void shaderMaterialGeoParamValueTexture::SetValue(const char * szValue)
{
	m_sValue = szValue;
}


void	shaderMaterialGeoParamValueTexture::WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	if(GetValue())
	{
		// Open texture
		char acBuffer[256];
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName(GetName());
		pobNode->GetElement().AddAttribute("type", GetName());
		formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
		pobNode->GetElement().AddAttribute("template", acBuffer);

		// Add filename
		parTreeNode* pobFileNode = rage_new parTreeNode();
		pobFileNode->GetElement().SetName("filename");
		pobFileNode->SetData(GetValue(), StringLength(GetValue()));
		pobFileNode->AppendAsChildOf(pobNode);

		// Add to parent
		pobNode->AppendAsChildOf(pobParent);
	}
}

