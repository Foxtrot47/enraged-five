// 
// shaderMaterial/shaderMaterialGeoParamValueVector2.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamValueVector2.h"

#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"

using namespace rage;

shaderMaterialGeoParamValueVector2::shaderMaterialGeoParamValueVector2(const shaderMaterialGeoParamDescription*	pobParamDescription) : shaderMaterialGeoParamValue(pobParamDescription)
{
	Reset();
}

shaderMaterialGeoParamValueVector2::shaderMaterialGeoParamValueVector2(const shaderMaterialGeoParamValueVector2*	pobSourceParam)
:shaderMaterialGeoParamValue(pobSourceParam)
{
	Copy(pobSourceParam);
}

shaderMaterialGeoParamValueVector2::~shaderMaterialGeoParamValueVector2()
{
	Reset();
}

shaderMaterialGeoParamValueVector2::shaderMaterialGeoParamValueVector2(const parTreeNode*	pobGeoParamValueGeoRootNode)
: shaderMaterialGeoParamValue(pobGeoParamValueGeoRootNode)
{
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamValueGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "Vector2value") == 0)
		{
			Vector2 obInitVector;
			parTreeNode* pobInitNode = pobChildValueNode->FindChildWithName("init");
			Assertf(pobInitNode, "Unable to find <init> child tag in geo template file");

			obInitVector.x = pobInitNode->GetElement().FindAttributeFloatValue("x", 0.0f);
			obInitVector.y = pobInitNode->GetElement().FindAttributeFloatValue("y", 0.0f);
			SetValue(obInitVector);
		}
	}
}

void shaderMaterialGeoParamValueVector2::Reset()
{
	shaderMaterialGeoParamValue::Reset();

	m_vValue.Zero();
}

void shaderMaterialGeoParamValueVector2::Copy(const shaderMaterialGeoParamValueVector2 * param)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamValue::Copy(param);

	m_vValue = ((shaderMaterialGeoParamValueVector2 *)param)->m_vValue;
}

bool shaderMaterialGeoParamValueVector2::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!shaderMaterialGeoParamValue::IsEqual(param))
	{
		return false;
	}

	return m_vValue == ((shaderMaterialGeoParamValueVector2 *)param)->m_vValue;
}

bool shaderMaterialGeoParamValueVector2::Update(const char * param, const float f)
{
	int len = (int) strlen(param);
	if (param[len-1] == 'X')
	{
		if (m_vValue.x != f)
		{
			m_vValue.x = f;
			return true;
		}
	}
	else if (param[len-1] == 'Y')
	{
		if (m_vValue.y != f)
		{
			m_vValue.y = f;
			return true;
		}
	}

	return false;
}

bool shaderMaterialGeoParamValueVector2::Update(const shaderMaterialGeoParamValue * param)
{
	Assert(GetType() == param->GetType());

	m_vValue = ((shaderMaterialGeoParamValueVector2 *)param)->m_vValue;

	return true;
}

void shaderMaterialGeoParamValueVector2::SetDefaultValue(grcEffect & effect, grcEffectVar & var)
{
	effect.GetVar(var, m_vValue);
}

void shaderMaterialGeoParamValueVector2::SetMaterialValue(Vector2 & vValue)
{
	vValue = m_vValue;
}

bool shaderMaterialGeoParamValueVector2::ReadGeoParamValueData(fiTokenizer & T)
{
	shaderMaterialGeoParamValue::ReadGeoParamValueData(T);

	T.GetVector(m_vValue);

	return true;
}

bool shaderMaterialGeoParamValueVector2::WriteGeoParamValueData(fiTokenizer & T) const
{
	shaderMaterialGeoParamValue::WriteGeoParamValueData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %f %f", GetTypeName(), m_vValue.x, m_vValue.y);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}


void	shaderMaterialGeoParamValueVector2::WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open string
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add Vector2value
	parTreeNode* pobVector2Node = rage_new parTreeNode();
	pobVector2Node->GetElement().SetName("Vector2value");
	pobVector2Node->GetElement().AddAttribute("x", GetValue().x);
	pobVector2Node->GetElement().AddAttribute("y", GetValue().y);
	pobVector2Node->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}

bool shaderMaterialGeoParamValueVector2::Update(const Vector2& obValue)
{
	if(m_vValue == obValue)
	{
		return false;
	}
	m_vValue = obValue;
	return true;
}

void shaderMaterialGeoParamValueVector2::CopyValueIntoVector2(Vector2 & vData) const
{
	vData = m_vValue;
}

