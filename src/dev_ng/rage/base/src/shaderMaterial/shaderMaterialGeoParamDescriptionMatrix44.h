// 
// shaderMaterial/shaderMaterialGeoParamDescriptionMatrix44.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_MATRIX44_H
#define SHADER_MATERIAL_GEO_PARAM_MATRIX44_H

#include "parser/macros.h"
#include "vector/matrix44.h"

#include "shaderMaterialGeoParamDescription.h"

namespace rage {

class shaderMaterialGeoParamDescriptionMatrix44 : public shaderMaterialGeoParamDescription
{
public:

	shaderMaterialGeoParamDescriptionMatrix44();
	virtual ~shaderMaterialGeoParamDescriptionMatrix44();

	int	GetType()				const	{return grcEffect::VT_MATRIX44;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_MATRIX44);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase=true);
	bool IsEqual(const shaderMaterialGeoParamDescription * param) const;

	bool ReadGeoParamDescriptionData(fiTokenizer & T);
	bool WriteGeoParamDescriptionData(fiTokenizer & T) const;

	Matrix44 GetDefaultValue() const {Matrix44 m; m.a = m_mDefaultValueA; m.b = m_mDefaultValueB; m.c = m_mDefaultValueC; m.d = m_mDefaultValueD; return m;}
	void SetDefaultValue(const Matrix44& obMatrix) 
	{
		m_mDefaultValueA = obMatrix.a; 
		m_mDefaultValueB = obMatrix.b; 
		m_mDefaultValueC = obMatrix.c; 
		m_mDefaultValueD = obMatrix.d; 
	}

	virtual void	WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const;
	virtual void	WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// Helper functions
	bool UpdateVector(const char * param, const float f, const int len, Vector4 & v);

	// DefaultValue for this param
	Vector4 m_mDefaultValueA;
	Vector4 m_mDefaultValueB;
	Vector4 m_mDefaultValueC;
	Vector4 m_mDefaultValueD;
};

} // end namespace rage

#endif
