// 
// shaderMaterial/shaderMaterialGeoParamDescriptionTexture.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_TEXTURE_H
#define SHADER_MATERIAL_GEO_PARAM_TEXTURE_H

#include "parser/macros.h"
#include "string/string.h"

#include "shaderMaterialGeoParamDescription.h"

namespace rage {

#define DEFAULT_UV_SET_NAME		"map1"

class shaderMaterialGeoParamDescriptionTexture : public shaderMaterialGeoParamDescription
{
public:

	shaderMaterialGeoParamDescriptionTexture();
	virtual ~shaderMaterialGeoParamDescriptionTexture();

	shaderMaterialGeoParamDescriptionTexture(const parTreeNode*	pobGeoParamDescriptionGeoRootNode);

	// Virtual functions
	int	GetType()				const	{return grcEffect::VT_TEXTURE;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_TEXTURE);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase=true);
	bool IsEqual(const shaderMaterialGeoParamDescription * param) const;

	bool CleanUp();

	bool ReadGeoParamDescriptionData(fiTokenizer & T);
	bool WriteGeoParamDescriptionData(fiTokenizer & T) const;

	// Non virtual function
	static void  GetTextureName(ConstString & sTextureName, const char * szTextureFilename);
	void		 GetTextureName(ConstString & sTextureName) const;
	const char * GetDefaultValue() const {return (const char *)m_sDefaultValue;}
	void		 SetDefaultValue(const char * szDefaultValue);

	virtual void	WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const;
	virtual void	WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:
	// DefaultValue for this param
	ConstString m_sDefaultValue;
};

} // end namespace rage

#endif
