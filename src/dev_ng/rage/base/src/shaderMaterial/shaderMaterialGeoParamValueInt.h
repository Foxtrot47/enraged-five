// 
// shaderMaterial/shaderMaterialGeoParamValueInt.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if 0
#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_INT_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_INT_H

#include "parser/manager.h"
#include "shaderMaterialGeoParamValue.h"

namespace rage {

class shaderMaterialGeoParamValueInt : public shaderMaterialGeoParamValue
{
public:

	shaderMaterialGeoParamValueInt(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValueInt(const shaderMaterialGeoParamValueInt*	pobSourceParam);
	virtual ~shaderMaterialGeoParamValueInt();

	shaderMaterialGeoParamValueInt(const parTreeNode*	pobGeoParamValueGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_INT;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_INT);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamValueInt * param);
	bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	bool Update(const shaderMaterialGeoParamValue * param);

	bool Update(const int n);

	bool Update(const bool b)														{return shaderMaterialGeoParamValue::Update(b);}
	bool Update(const float f)														{return shaderMaterialGeoParamValue::Update(f);}
	bool Update(const char * texture)												{return shaderMaterialGeoParamValue::Update(texture);}
	bool Update(const char * param, const float f)									{return shaderMaterialGeoParamValue::Update(param, f);}
	bool Update(const char * param, const float f0, const float f1, const float f2)	{return shaderMaterialGeoParamValue::Update(param, f0, f1, f2);}

	void SetDefaultValue(grcEffect & effect, grcEffectVar & var);

	void SetMaterialValue(Vector2 & vValue);
	void CopyValueIntoVector2(Vector2 & vData) const;

	bool CleanUp();

	bool ReadGeoParamValueData(fiTokenizer & T);
	bool WriteGeoParamValueData(fiTokenizer & T) const;

	int GetValue() const {return m_nValue;}
	void SetValue(int nValue) {m_nValue = nValue;}

	virtual void	WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// Value for this param
	int m_nValue;
};

} // end namespace rage

#endif
#endif
