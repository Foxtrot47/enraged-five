// 
// shaderMaterial/shaderMaterialGeoParamDescriptionFloat.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_FLOAT_H
#define SHADER_MATERIAL_GEO_PARAM_FLOAT_H

#include "parser/macros.h"
#include "shaderMaterialGeoParamDescription.h"

namespace rage {

class shaderMaterialGeoParamDescriptionFloat : public shaderMaterialGeoParamDescription
{
public:

	shaderMaterialGeoParamDescriptionFloat();
	virtual ~shaderMaterialGeoParamDescriptionFloat();

	shaderMaterialGeoParamDescriptionFloat(const parTreeNode*	pobGeoParamDescriptionGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_FLOAT;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_FLOAT);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase=true);
	bool IsEqual(const shaderMaterialGeoParamDescription * param) const;

	bool CleanUp();

	bool LoadFromXML(const char * szFilename);
	bool SaveToXML(const char * szFilename) const;

	bool ReadGeoParamDescriptionData(fiTokenizer & T);
	bool WriteGeoParamDescriptionData(fiTokenizer & T) const;

	float GetDefaultValue() const {return m_fDefaultValue;}
	void SetDefaultValue(const float& fDefaultValue) {m_fDefaultValue = fDefaultValue;}

	virtual void	WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const;
	virtual void	WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// DefaultValue for this param
	float m_fDefaultValue;
};

} // end namespace rage

#endif
