// 
// shaderMaterialGeoManager/shaderMaterialGeoManager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoManager.h"

#include "file/asset.h"

#if __WIN32PC
#pragma warning(push)
#pragma warning(disable : 4668)
#include <Rpc.h>

#pragma comment(lib, "rpcrt4.lib")

#pragma warning(pop)
#endif

#define BUFFER_LEN	1024

using namespace rage;

shaderMaterialGeoManager* shaderMaterialGeoManager::m_gShaderMaterialGeoManager = NULL;

shaderMaterialGeoManager::shaderMaterialGeoManager()
{
}

shaderMaterialGeoManager::~shaderMaterialGeoManager()
{
	for(int i=0; i<m_apShaderMaterialGeoTemplates.GetCount(); i++)
	{
		if (m_apShaderMaterialGeoTemplates[i])
		{
			delete m_apShaderMaterialGeoTemplates[i];
		}
	}
	for(int i=0; i<m_apShaderMaterialGeoTypes.GetCount(); i++)
	{
		if (m_apShaderMaterialGeoTypes[i])
		{
			delete m_apShaderMaterialGeoTypes[i];
		}
	}
	for(int i=0; i<m_apShaderMaterialGeoInstances.GetCount(); i++)
	{
		if (m_apShaderMaterialGeoInstances[i])
		{
			delete m_apShaderMaterialGeoInstances[i];
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

const shaderMaterialGeoTemplate * shaderMaterialGeoManager::CreateShaderMaterialGeoTemplateFromShader(const char* pcShaderPathAndFilename)
{
	//NO caching done here because this is used for creating the geotemplate file itself and should not be used by an application other than
	//	the template generator.
	const shaderMaterialGeoTemplate* pobNewShaderMaterialGeoTemplate = NULL;
	
	shaderMaterialGeoTemplate* pobNewShaderMaterialGeoTemplateTemp = rage_new shaderMaterialGeoTemplate();
	if(!pobNewShaderMaterialGeoTemplateTemp->CreateFromShader(pcShaderPathAndFilename))
	{
		// If here, then nothing works
		// Error occured loading it, so kill it
		delete 	pobNewShaderMaterialGeoTemplateTemp;
		pobNewShaderMaterialGeoTemplateTemp = NULL;
	}
	pobNewShaderMaterialGeoTemplate = pobNewShaderMaterialGeoTemplateTemp;

	return pobNewShaderMaterialGeoTemplate;
}

const shaderMaterialGeoType * shaderMaterialGeoManager::CreateShaderMaterialGeoTypeFromTemplate(const char* pcGeoTemplatePathAndFilename, bool asNew)
{
	// First off get the template
	const shaderMaterialGeoTemplate * pobShaderMaterialGeoTemplate = LoadShaderMaterialGeoTemplate(pcGeoTemplatePathAndFilename);
	Assertf(pobShaderMaterialGeoTemplate, "Unable to find template [%s] to create a type", pcGeoTemplatePathAndFilename);

	//See if we have a default type already with the same name as the template
	const shaderMaterialGeoType * pobNewShaderMaterialGeoType = NULL;
	if (!asNew)
	{
		pobNewShaderMaterialGeoType = LoadShaderMaterialGeoType(pcGeoTemplatePathAndFilename);
	}

	//dont have one so need to create it
	if (!pobNewShaderMaterialGeoType)
	{
		shaderMaterialGeoType* pobNewShaderMaterialGeoTypeTemp = rage_new shaderMaterialGeoType();
		if(pobNewShaderMaterialGeoTypeTemp->InitialiseFromTemplate(pobShaderMaterialGeoTemplate))
		{
			m_apShaderMaterialGeoTypes.PushAndGrow(pobNewShaderMaterialGeoTypeTemp);
			m_aGeoTypeRefCounts.PushAndGrow(1);
			unsigned int hash = atStringHash(pobNewShaderMaterialGeoTypeTemp->GetTypePathAndFilename());
			//Displayf("Add: Type [%s] Ref: %d", pobNewShaderMaterialGeoTypeTemp->GetTypePathAndFilename(), 1);
			m_shaderMaterialGeoTypeMappingByFile.Insert(hash,(unsigned short)m_apShaderMaterialGeoTypes.GetCount()-1);
		}
		else
		{
			// If here, then nothing works
			// Error occured loading it, so kill it
			delete 	pobNewShaderMaterialGeoTypeTemp;
			pobNewShaderMaterialGeoTypeTemp = NULL;
		}
		pobNewShaderMaterialGeoType = pobNewShaderMaterialGeoTypeTemp;
	}

	return pobNewShaderMaterialGeoType;
}

const shaderMaterialGeoInstance *	shaderMaterialGeoManager::CreateShaderMaterialGeoInstanceFromType(const char* pcGeoTypePathAndFilename)
{
	// First off get the type
	const shaderMaterialGeoType * pobShaderMaterialGeoType = LoadShaderMaterialGeoType(pcGeoTypePathAndFilename);
	Assertf(pobShaderMaterialGeoType, "Unable to find type [%s] to create geo instance", pcGeoTypePathAndFilename);

	shaderMaterialGeoInstance* pobNewShaderMaterialGeoInstance = NULL;
	if (pobShaderMaterialGeoType)
	{
		pobNewShaderMaterialGeoInstance = rage_new shaderMaterialGeoInstance();
		if (pobNewShaderMaterialGeoInstance->ConstructFromParentType(pobShaderMaterialGeoType))
		{
			m_apShaderMaterialGeoInstances.PushAndGrow(pobNewShaderMaterialGeoInstance);
			m_aGeoInstanceRefCounts.PushAndGrow(1);

			//add to mappings
			unsigned short addedIndex = (unsigned short)m_apShaderMaterialGeoInstances.GetCount()-1;

			//NOTE: no filename specified yet so cant add it to this list, but can add it to the Guid list
			//m_shaderMaterialGeoInstanceMappingByFile.Insert(hash, addedIndex);
			unsigned int guidHash = atStringHash(pobNewShaderMaterialGeoInstance->GetUID());
			m_shaderMaterialGeoInstanceMappingByGUID.Insert(guidHash, addedIndex);
			AddChildShaderMaterialGeoType(pobNewShaderMaterialGeoInstance->GetParentTypePathAndFilename(), guidHash);
			//Displayf("Add: Instance from template [%s] Ref: %d", pobNewShaderMaterialGeoInstance->GetUID(), 1);
		}
		else
		{
			// Error occurred loading it, so kill it
			delete 	pobNewShaderMaterialGeoInstance;
			pobNewShaderMaterialGeoInstance = NULL;
		}
	}
	return pobNewShaderMaterialGeoInstance;
}

const shaderMaterialGeoInstance *	shaderMaterialGeoManager::CreateShaderMaterialGeoInstanceFromTemplate(const char* pcGeoTemplatePathAndFilename)
{
	const shaderMaterialGeoType * pobShaderMaterialGeoType = CreateShaderMaterialGeoTypeFromTemplate(pcGeoTemplatePathAndFilename);
	Assertf(pobShaderMaterialGeoType, "Unable to find type with template [%s] to create geo instance", pcGeoTemplatePathAndFilename);

	shaderMaterialGeoInstance* pobNewShaderMaterialGeoInstance = NULL;
	if (pobShaderMaterialGeoType)
	{
		pobNewShaderMaterialGeoInstance = rage_new shaderMaterialGeoInstance();
		if (pobNewShaderMaterialGeoInstance->ConstructFromParentType(pobShaderMaterialGeoType))
		{
			m_apShaderMaterialGeoInstances.PushAndGrow(pobNewShaderMaterialGeoInstance);
			m_aGeoInstanceRefCounts.PushAndGrow(1);

			//add to mappings
			unsigned short addedIndex = (unsigned short)m_apShaderMaterialGeoInstances.GetCount()-1;
			
			//NOTE: no filename specified yet so cant add it to this list, but can add it to the Guid list
			//m_shaderMaterialGeoInstanceMappingByFile.Insert(hash, addedIndex);
			unsigned int guidHash = atStringHash(pobNewShaderMaterialGeoInstance->GetUID());
			m_shaderMaterialGeoInstanceMappingByGUID.Insert(guidHash, addedIndex);
			AddChildShaderMaterialGeoType(pobNewShaderMaterialGeoInstance->GetParentTypePathAndFilename(), guidHash);
			//Displayf("Add: Instance from template [%s] Ref: %d", pobNewShaderMaterialGeoInstance->GetUID(), 1);
		}
		else
		{
			// Error occurred loading it, so kill it
			delete 	pobNewShaderMaterialGeoInstance;
			pobNewShaderMaterialGeoInstance = NULL;
		}
	}
	return pobNewShaderMaterialGeoInstance;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

const shaderMaterialGeoTemplate * shaderMaterialGeoManager::GetShaderMaterialGeoTemplate(const char* pcGeoTemplatePathAndFilename) const
{
	char acCleanGeoTemplatePathAndFilename[BUFFER_LEN];
	getValidTemplateFullPath(acCleanGeoTemplatePathAndFilename, pcGeoTemplatePathAndFilename);

	const shaderMaterialGeoTemplate* pobShaderMaterialGeoTemplate = NULL;
	unsigned int hash = atStringHash(acCleanGeoTemplatePathAndFilename);

	const unsigned short* found = m_shaderMaterialGeoTemplateMappingByFile.Access(hash);
	if (found)
	{
		unsigned short index = (*found);
		pobShaderMaterialGeoTemplate = m_apShaderMaterialGeoTemplates[index];
	}
	
	Assertf(pobShaderMaterialGeoTemplate, "GeoTemplate [%s] was not found make sure it is loaded before you try and get it.", acCleanGeoTemplatePathAndFilename);
	return pobShaderMaterialGeoTemplate;
}

shaderMaterialGeoType * shaderMaterialGeoManager::GetShaderMaterialGeoType(const char* pcGeoTypePathAndFilename)
{
	char acCleanGeoTypePathAndFilename[BUFFER_LEN];
	getValidTypeFullPath(acCleanGeoTypePathAndFilename, pcGeoTypePathAndFilename);

	shaderMaterialGeoType* pobShaderMaterialGeoType = NULL;
	unsigned int hash = atStringHash(acCleanGeoTypePathAndFilename);

	const unsigned short* found = m_shaderMaterialGeoTypeMappingByFile.Access(hash);
	if (found)
	{
		unsigned short index = (*found);
		pobShaderMaterialGeoType = m_apShaderMaterialGeoTypes[index];
	}

	Assertf(pobShaderMaterialGeoType, "GeoType [%s] was not found make sure it is loaded before you try and get it.", acCleanGeoTypePathAndFilename);
	return pobShaderMaterialGeoType;
}

shaderMaterialGeoInstance * shaderMaterialGeoManager::GetShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename)
{
	char acCleanGeoInstancePathAndFilename[BUFFER_LEN];
	CleanFilename(acCleanGeoInstancePathAndFilename, pcGeoInstancePathAndFilename);

	shaderMaterialGeoInstance* pobShaderMaterialGeoInstance = NULL;
	unsigned int hash = atStringHash(pcGeoInstancePathAndFilename);

	const unsigned short* found = m_shaderMaterialGeoInstanceMappingByFile.Access(hash);
	if (found)
	{
		unsigned short index = (*found);
		pobShaderMaterialGeoInstance = m_apShaderMaterialGeoInstances[index];
	}

	Assertf(pobShaderMaterialGeoInstance, "GeoInstance [%s] was not found make sure it is loaded before you try and get it.", pcGeoInstancePathAndFilename);
	return pobShaderMaterialGeoInstance;
}

shaderMaterialGeoInstance * shaderMaterialGeoManager::GetShaderMaterialGeoInstanceFromUID(const char* pcGeoInstanceUID)
{
	shaderMaterialGeoInstance* pobShaderMaterialGeoInstance = NULL;
	unsigned int hash = atStringHash(pcGeoInstanceUID);

	const unsigned short* found = m_shaderMaterialGeoInstanceMappingByGUID.Access(hash);
	if (found)
	{
		unsigned short index = (*found);
		pobShaderMaterialGeoInstance = m_apShaderMaterialGeoInstances[index];
	}

	return pobShaderMaterialGeoInstance;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

const shaderMaterialGeoTemplate * shaderMaterialGeoManager::LoadShaderMaterialGeoTemplate(const char* pcGeoTemplatePathAndFilename)
{
	char acCleanGeoTemplatePathAndFilename[BUFFER_LEN];
	getValidTemplateFullPath(acCleanGeoTemplatePathAndFilename, pcGeoTemplatePathAndFilename);

	const shaderMaterialGeoTemplate* pobNewShaderMaterialGeoTemplate = NULL;
	unsigned int hash = atStringHash(acCleanGeoTemplatePathAndFilename);

	const unsigned short* found = m_shaderMaterialGeoTemplateMappingByFile.Access(hash);
	if (found)
	{
		unsigned short index = (*found);

		// Bingo, found the Instance is already loaded
		m_aGeoTemplateRefCounts[index]++;
		//Displayf("Inc: Template [%s] Ref: %d", acCleanGeoTemplatePathAndFilename, m_aGeoTemplateRefCounts[index]);
		pobNewShaderMaterialGeoTemplate = m_apShaderMaterialGeoTemplates[index];
	}
	else
	{
		if(ASSET.Exists(acCleanGeoTemplatePathAndFilename, NULL))
		{
			shaderMaterialGeoTemplate* pobNewShaderMaterialGeoTemplateTemp = rage_new shaderMaterialGeoTemplate();
			if(pobNewShaderMaterialGeoTemplateTemp->LoadFromGeoTemplate(acCleanGeoTemplatePathAndFilename))
			{
				m_apShaderMaterialGeoTemplates.PushAndGrow(pobNewShaderMaterialGeoTemplateTemp);
				m_aGeoTemplateRefCounts.PushAndGrow(1);
				//Displayf("Add: Template [%s] Ref: %d", acCleanGeoTemplatePathAndFilename, 1);
				m_shaderMaterialGeoTemplateMappingByFile.Insert(hash,(unsigned short)m_apShaderMaterialGeoTemplates.GetCount()-1);
			}
			else
			{
				// If here, then nothing works
				// Error occured loading it, so kill it
				delete 	pobNewShaderMaterialGeoTemplateTemp;
				pobNewShaderMaterialGeoTemplateTemp = NULL;
			}
			pobNewShaderMaterialGeoTemplate = pobNewShaderMaterialGeoTemplateTemp;
		}
	}
	return pobNewShaderMaterialGeoTemplate;
}

const shaderMaterialGeoType * shaderMaterialGeoManager::LoadShaderMaterialGeoType(const char* pcGeoTypePathAndFilename)
{
	char acCleanGeoTypePathAndFilename[BUFFER_LEN];
	getValidTypeFullPath(acCleanGeoTypePathAndFilename, pcGeoTypePathAndFilename);

	const shaderMaterialGeoType* pobNewShaderMaterialGeoType = NULL;
	unsigned int hash = atStringHash(acCleanGeoTypePathAndFilename);

	const unsigned short* found = m_shaderMaterialGeoTypeMappingByFile.Access(hash);
	if (found)
	{
		unsigned short index = (*found);
		m_aGeoTypeRefCounts[index]++;
		//Displayf("Inc: Type [%s] Ref: %d", acCleanGeoTypePathAndFilename, m_aGeoTypeRefCounts[index]);
		pobNewShaderMaterialGeoType = m_apShaderMaterialGeoTypes[index];

		//this is pretty lame. because using load for refcounting ... would be better to have index system

		//because types can have parent types we need to load that as well
		//MAKE NOT LAME: add refs to the right things
		if (pobNewShaderMaterialGeoType->GetParentTypePathAndFilename() && strcmp(pobNewShaderMaterialGeoType->GetParentTypePathAndFilename(),""))
		{
			LoadShaderMaterialGeoType(pobNewShaderMaterialGeoType->GetParentTypePathAndFilename());
		}

		//because types are made up of templates we need to load that as well
		//MAKE NOT LAME: add refs to the right things
	}
	else
	{
		if(ASSET.Exists(acCleanGeoTypePathAndFilename, NULL))
		{
			shaderMaterialGeoType* pobNewShaderMaterialGeoTypeTemp = rage_new shaderMaterialGeoType();
			if(pobNewShaderMaterialGeoTypeTemp->LoadFromGeoType(acCleanGeoTypePathAndFilename))
			{
				m_apShaderMaterialGeoTypes.PushAndGrow(pobNewShaderMaterialGeoTypeTemp);
				m_aGeoTypeRefCounts.PushAndGrow(1);
				//Displayf("Add: Type [%s] Ref: %d", acCleanGeoTypePathAndFilename, 1);
				m_shaderMaterialGeoTypeMappingByFile.Insert(hash,(unsigned short)m_apShaderMaterialGeoTypes.GetCount()-1);

				if (pobNewShaderMaterialGeoTypeTemp->GetParentTypePathAndFilename() && strcmp(pobNewShaderMaterialGeoTypeTemp->GetParentTypePathAndFilename(),""))
				{
					AddChildShaderMaterialGeoType(pobNewShaderMaterialGeoTypeTemp->GetParentTypePathAndFilename(), hash);
				}
			}
			else
			{
				// If here, then nothing works
				// Error occured loading it, so kill it
				delete 	pobNewShaderMaterialGeoTypeTemp;
				pobNewShaderMaterialGeoTypeTemp = NULL;
			}
			pobNewShaderMaterialGeoType = pobNewShaderMaterialGeoTypeTemp;
		}
	}

	return pobNewShaderMaterialGeoType;
}

shaderMaterialGeoInstance * shaderMaterialGeoManager::LoadShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename)
{
	char acCleanGeoInstancePathAndFilename[BUFFER_LEN];
	CleanFilename(acCleanGeoInstancePathAndFilename, pcGeoInstancePathAndFilename);

	shaderMaterialGeoInstance* pobNewShaderMaterialGeoInstance = NULL;
	unsigned int hash = atStringHash(acCleanGeoInstancePathAndFilename);

	const unsigned short* found = m_shaderMaterialGeoInstanceMappingByFile.Access(hash);
	if (found)
	{
		unsigned short index = (*found);

		// Bingo, found the Instance is already loaded
		m_aGeoInstanceRefCounts[index]++;
		//Displayf("Inc: Instance [%s] Ref: %d", acCleanGeoInstancePathAndFilename, m_aGeoInstanceRefCounts[index]);
		//this is pretty lame. because using load for refcounting ... would be better to have index system
		LoadShaderMaterialGeoType(m_apShaderMaterialGeoInstances[index]->GetParentTypePathAndFilename());
		pobNewShaderMaterialGeoInstance = m_apShaderMaterialGeoInstances[index];
	}
	else
	{
		// Hmmm, not yet loaded this Instance, so load it
		pobNewShaderMaterialGeoInstance = rage_new shaderMaterialGeoInstance();
		if(pobNewShaderMaterialGeoInstance->LoadFromGeoInstance(acCleanGeoInstancePathAndFilename))
		{
			m_apShaderMaterialGeoInstances.PushAndGrow(pobNewShaderMaterialGeoInstance);
			m_aGeoInstanceRefCounts.PushAndGrow(1);
			
			//add to mappings
			unsigned short addedIndex = (unsigned short)m_apShaderMaterialGeoInstances.GetCount()-1;
			m_shaderMaterialGeoInstanceMappingByFile.Insert(hash, addedIndex);
			unsigned int guidHash = atStringHash(pobNewShaderMaterialGeoInstance->GetUID());
			m_shaderMaterialGeoInstanceMappingByGUID.Insert(guidHash, addedIndex);
			//Displayf("Add: Instance [%s] Ref: %d", acCleanGeoInstancePathAndFilename, 1);
			if (pobNewShaderMaterialGeoInstance->GetParentTypePathAndFilename() && strcmp(pobNewShaderMaterialGeoInstance->GetParentTypePathAndFilename(),""))
			{
				AddChildShaderMaterialGeoType(pobNewShaderMaterialGeoInstance->GetParentTypePathAndFilename(), hash);
			}
		}
		else
		{
			// Error occurred loading it, so kill it
			delete 	pobNewShaderMaterialGeoInstance;
			pobNewShaderMaterialGeoInstance = NULL;
		}
	}
	
	return pobNewShaderMaterialGeoInstance;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

bool shaderMaterialGeoManager::ReloadShaderMaterialGeoType(const char* pcGeoTypePathAndFilename)
{
	shaderMaterialGeoType* pobShaderMaterialGeoType = GetShaderMaterialGeoType(pcGeoTypePathAndFilename);
	Assertf(pobShaderMaterialGeoType,"Unable to get geo Type [%s] for reload", pcGeoTypePathAndFilename);

	//Kinda LAME. because we know that the load calls functions that add refs to the parent type and template

	//because types can have parent types we need to release that as well
	//MAKE NOT LAME: deref
	if (pobShaderMaterialGeoType->GetParentTypePathAndFilename() && strcmp(pobShaderMaterialGeoType->GetParentTypePathAndFilename(),""))
	{
		ReleaseShaderMaterialGeoType(pobShaderMaterialGeoType->GetParentTypePathAndFilename());
	}

	//because types are made up of templates we need to release that as well
	//MAKE NOT LAME: deref
	pobShaderMaterialGeoType->LoadFromGeoType(pcGeoTypePathAndFilename);

	if (pobShaderMaterialGeoType)
	{
		updateChildren(pobShaderMaterialGeoType);
	}

	return (pobShaderMaterialGeoType) ? true : false;
}

bool shaderMaterialGeoManager::ReloadShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename)
{
	shaderMaterialGeoInstance* pobShaderMaterialGeoInstance = GetShaderMaterialGeoInstance(pcGeoInstancePathAndFilename);
	Assertf(pobShaderMaterialGeoInstance,"Unable to get geo instance [%s] for reload", pcGeoInstancePathAndFilename);

	//Kinda LAME. because we know that the load calls functions that add refs to the parent type and template
	//Note: this function calls down the "tree" through all types and templates
	ReleaseShaderMaterialGeoType(pobShaderMaterialGeoInstance->GetParentTypePathAndFilename());
	pobShaderMaterialGeoInstance->LoadFromGeoInstance(pcGeoInstancePathAndFilename);

	if (pobShaderMaterialGeoInstance)
	{
		updateChildren(pobShaderMaterialGeoInstance);
	}

	return (pobShaderMaterialGeoInstance) ? true : false;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void shaderMaterialGeoManager::ReleaseShaderMaterialGeoTemplate(const char* pcGeoTemplatePathAndFilename)
{
	char acCleanGeoTemplatePathAndFilename[BUFFER_LEN];
	getValidTemplateFullPath(acCleanGeoTemplatePathAndFilename, pcGeoTemplatePathAndFilename);

	unsigned int hash = atStringHash(acCleanGeoTemplatePathAndFilename);

	const unsigned short* found = m_shaderMaterialGeoTemplateMappingByFile.Access(hash);
	if (found)
	{
		unsigned short index = (*found);

		m_aGeoTemplateRefCounts[index]--;
		//Displayf("Dec: Template [%s] Ref: %d", pcGeoTemplatePathAndFilename, m_aGeoTemplateRefCounts[index]);
		Assertf(m_aGeoTemplateRefCounts[index] >= 0, "Refcounting error [%s] released too much? Count: %d", acCleanGeoTemplatePathAndFilename, m_aGeoTemplateRefCounts[index]);
		if (m_aGeoTemplateRefCounts[index] == 0)
		{
			m_shaderMaterialGeoTemplateMappingByFile.Delete(hash);
			delete m_apShaderMaterialGeoTemplates[index];
			m_apShaderMaterialGeoTemplates[index] = NULL;
		}
	}
	else
	{
		Errorf("Attempting to Release [%s], but it is not in the list. Refcounting Error?", acCleanGeoTemplatePathAndFilename);
		Assertf(false, "Attempting to Release [%s], but it is not in the list. Refcounting Error?", acCleanGeoTemplatePathAndFilename);
	}
}

void shaderMaterialGeoManager::ReleaseShaderMaterialGeoType(const char* pcGeoTypePathAndFilename)
{
	char acCleanGeoTypePathAndFilename[BUFFER_LEN];
	getValidTypeFullPath(acCleanGeoTypePathAndFilename, pcGeoTypePathAndFilename);

	unsigned int hash = atStringHash(acCleanGeoTypePathAndFilename);

	const unsigned short* found = m_shaderMaterialGeoTypeMappingByFile.Access(hash);
	if (found)
	{
		unsigned short index = (*found);
		const shaderMaterialGeoType* pobShaderMaterialGeoType = m_apShaderMaterialGeoTypes[index];

		//because types can have parent types we need to Release that as well
		//MAKE NOT LAME: deref
		if (pobShaderMaterialGeoType->GetParentTypePathAndFilename() && strcmp(pobShaderMaterialGeoType->GetParentTypePathAndFilename(),""))
		{
			ReleaseShaderMaterialGeoType(pobShaderMaterialGeoType->GetParentTypePathAndFilename());
		}

		//because types are made up of templates we need to Release that as well
		//MAKE NOT LAME: deref

		m_aGeoTypeRefCounts[index]--;
		//Displayf("Dec: Type [%s] Ref: %d", pcGeoTypePathAndFilename, m_aGeoTypeRefCounts[index]);
		Assertf(m_aGeoTypeRefCounts[index] >= 0, "Refcounting error [%s] released too much? Count: %d", acCleanGeoTypePathAndFilename, m_aGeoTypeRefCounts[index]);
		if (m_aGeoTypeRefCounts[index] == 0)
		{
			if (pobShaderMaterialGeoType->GetParentTypePathAndFilename() && strcmp(pobShaderMaterialGeoType->GetParentTypePathAndFilename(),""))
			{
				RemoveChildShaderMaterialGeoType(pobShaderMaterialGeoType->GetParentTypePathAndFilename(), hash);
			}
			m_shaderMaterialGeoTypeMappingByFile.Delete(hash);
			delete m_apShaderMaterialGeoTypes[index];
			m_apShaderMaterialGeoTypes[index] = NULL;
		}
	}
	else
	{
		Errorf("Attempting to Release [%s], but it is not in the list. Refcounting Error?", acCleanGeoTypePathAndFilename);
		Assertf(false, "Attempting to Release [%s], but it is not in the list. Refcounting Error?", acCleanGeoTypePathAndFilename);
	}
}

void shaderMaterialGeoManager::ReleaseShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename)
{
	char acCleanGeoInstancePathAndFilename[BUFFER_LEN];
	CleanFilename(acCleanGeoInstancePathAndFilename, pcGeoInstancePathAndFilename);

	unsigned int hash = atStringHash(acCleanGeoInstancePathAndFilename);

	const unsigned short* found = m_shaderMaterialGeoInstanceMappingByFile.Access(hash);
	if (found)
	{
		unsigned short index = (*found);
		shaderMaterialGeoInstance* pobShaderMaterialGeoInstance = m_apShaderMaterialGeoInstances[index];

		//because instances are made up of types we need to Release that as well
		ReleaseShaderMaterialGeoType(pobShaderMaterialGeoInstance->GetParentTypePathAndFilename());

		m_aGeoInstanceRefCounts[index]--;
		//Displayf("Dec: Instance [%s] Ref: %d", pcGeoInstancePathAndFilename, m_aGeoInstanceRefCounts[index]);
		Assertf(m_aGeoInstanceRefCounts[index] >= 0, "Refcounting error [%s] released too much? Count: %d", acCleanGeoInstancePathAndFilename, m_aGeoInstanceRefCounts[index]);
		if (m_aGeoInstanceRefCounts[index] == 0)
		{
			RemoveChildShaderMaterialGeoType(pobShaderMaterialGeoInstance->GetParentTypePathAndFilename(), hash);
			m_shaderMaterialGeoInstanceMappingByFile.Delete(hash);
			unsigned int guidHash = atStringHash(pobShaderMaterialGeoInstance->GetUID());
			m_shaderMaterialGeoInstanceMappingByGUID.Delete(guidHash);
			delete m_apShaderMaterialGeoInstances[index];
			m_apShaderMaterialGeoInstances[index] = NULL;
		}
	}
	else
	{
		Errorf("Attempting to Release [%s], but it is not in the list. Refcounting Error?", acCleanGeoInstancePathAndFilename);
		Assertf(false, "Attempting to Release [%s], but it is not in the list. Refcounting Error?", acCleanGeoInstancePathAndFilename);
	}
}

void shaderMaterialGeoManager::ReleaseShaderMaterialGeoInstanceWithGUID(const char* pcGeoInstanceUID)
{
	//this is pretty lame. string as unique ID == slow compares + extra memory ... would be better to have index system
	shaderMaterialGeoInstance* instance = GetShaderMaterialGeoInstanceFromUID(pcGeoInstanceUID);
	if(instance)
	{
		//handle things that done actually have file names
		if (strcmp(instance->GetInstancePathAndFilename(), ""))
		{
			ReleaseShaderMaterialGeoInstance(instance->GetInstancePathAndFilename());
		}
		else
		{
			unsigned int hash = atStringHash(pcGeoInstanceUID);
			const unsigned short* found = m_shaderMaterialGeoInstanceMappingByGUID.Access(hash);
			if (found)
			{
				unsigned short index = (*found);
				shaderMaterialGeoInstance* pobShaderMaterialGeoInstance = m_apShaderMaterialGeoInstances[index];

				//because instances are made up of types we need to Release that as well
				ReleaseShaderMaterialGeoType(pobShaderMaterialGeoInstance->GetParentTypePathAndFilename());

				m_aGeoInstanceRefCounts[index]--;
				//Displayf("Dec: Instance [%s] Ref: %d", pcGeoInstanceUID, m_aGeoInstanceRefCounts[index]);
				Assertf(m_aGeoInstanceRefCounts[index] >= 0, "Refcounting error [%s] released too much? Count: %d", pcGeoInstanceUID, m_aGeoInstanceRefCounts[index]);
				if (m_aGeoInstanceRefCounts[index] == 0)
				{
					RemoveChildShaderMaterialGeoType(pobShaderMaterialGeoInstance->GetParentTypePathAndFilename(), hash);
					m_shaderMaterialGeoInstanceMappingByGUID.Delete(hash);
					delete m_apShaderMaterialGeoInstances[index];
					m_apShaderMaterialGeoInstances[index] = NULL;
				}
			}
			else
			{
				Errorf("Attempting to Release [%s], but it is not in the list. Refcounting Error?", pcGeoInstanceUID);
				Assertf(false, "Attempting to Release [%s], but it is not in the list. Refcounting Error?", pcGeoInstanceUID);
			}
		}
	}
}

void shaderMaterialGeoManager::AddRefToShaderMaterialGeoInstanceWithGUID(const char* pcGeoInstanceUID)
{
	//this is pretty lame. string as unique ID == slow compares + extra memory ... would be better to have index system
	shaderMaterialGeoInstance* instance = GetShaderMaterialGeoInstanceFromUID(pcGeoInstanceUID);
	if(instance)
	{
		if (strcmp(instance->GetInstancePathAndFilename(), ""))
		{
			LoadShaderMaterialGeoInstance(instance->GetInstancePathAndFilename());
		}
		else
		{
			unsigned int hash = atStringHash(pcGeoInstanceUID);
			const unsigned short* found = m_shaderMaterialGeoInstanceMappingByGUID.Access(hash);
			if (found)
			{
				unsigned short index = (*found);

				// Bingo, found the Instance is already loaded
				m_aGeoInstanceRefCounts[index]++;
				//Displayf("Inc: Instance [%s] Ref: %d", pcGeoInstanceUID, m_aGeoInstanceRefCounts[index]);
				//this is pretty lame. because using load for refcounting ... would be better to have index system
				LoadShaderMaterialGeoType(m_apShaderMaterialGeoInstances[index]->GetParentTypePathAndFilename());
			}
			else
			{
				Errorf("Attempting to Add Ref to geo instance [%s], but it is not in the list. Refcounting Error?", pcGeoInstanceUID);
				Assertf(false, "Attempting to Add Ref to geo instance [%s], but it is not in the list. Refcounting Error?", pcGeoInstanceUID);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void shaderMaterialGeoManager::AddChildShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename, grmShader* shader)
{
	unsigned int hash = atStringHash(pcGeoInstancePathAndFilename);
	atArray<grmShader* > * found = m_instanceChildrenMappingByFile.Access(hash);
	if (found)
	{
		found->PushAndGrow(shader);
	}
	else
	{
		atArray <grmShader*> temp;
		temp.PushAndGrow(shader);
		m_instanceChildrenMappingByFile.Insert(hash, temp);
	}
}

void shaderMaterialGeoManager::RemoveChildShaderMaterialGeoInstance(const char* pcGeoInstancePathAndFilename, grmShader* shader)
{
	unsigned int hash = atStringHash(pcGeoInstancePathAndFilename);
	atArray<grmShader* > * found = m_instanceChildrenMappingByFile.Access(hash);
	if (found)
	{
		found->DeleteMatches(shader);

		//no more mappings we can remove ourselves
		if (!found->GetCount())
		{
			m_instanceChildrenMappingByFile.Delete(hash);
		}
	}
	else
	{
		Errorf("Attempting to remove a child from instance mapping [%s] but it is not in the list.", pcGeoInstancePathAndFilename);
		Assertf(false, "Attempting to remove a child from instance mapping [%s] but it is not in the list.", pcGeoInstancePathAndFilename);
	}
}

void shaderMaterialGeoManager::AddChildShaderMaterialGeoInstanceWithGUID(const char* pcGeoInstanceUID, grmShader* shader)
{
	unsigned int hash = atStringHash(pcGeoInstanceUID);
	atArray<grmShader* > * found = m_instanceChildrenMappingByGUID.Access(hash);
	if (found)
	{
		found->PushAndGrow(shader);
	}
	else
	{
		atArray <grmShader*> temp;
		temp.PushAndGrow(shader);
		m_instanceChildrenMappingByGUID.Insert(hash, temp);
	}
}

void shaderMaterialGeoManager::RemoveChildShaderMaterialGeoInstanceWithGUID(const char* pcGeoInstanceUID, grmShader* shader)
{
	unsigned int hash = atStringHash(pcGeoInstanceUID);
	atArray<grmShader* > * found = m_instanceChildrenMappingByGUID.Access(hash);
	if (found)
	{
		found->DeleteMatches(shader);

		//no more mappings we can remove ourselves
		if (!found->GetCount())
		{
			m_instanceChildrenMappingByGUID.Delete(hash);
		}
	}
	else
	{
		Errorf("Attempting to remove a child from instance mapping [%s] but it is not in the list.", pcGeoInstanceUID);
		Assertf(false, "Attempting to remove a child from instance mapping [%s] but it is not in the list.", pcGeoInstanceUID);
	}
}

void shaderMaterialGeoManager::AddChildShaderMaterialGeoType(const char* pcGeoTypePathAndFilename, unsigned int childHash)
{
	unsigned int hash = atStringHash(pcGeoTypePathAndFilename);
	atArray< unsigned int > * found = m_typeChildrenMappingByFileorGuid.Access(hash);
	if (found)
	{
		found->PushAndGrow(childHash);
	}
	else
	{
		atArray < unsigned int > temp;
		temp.PushAndGrow(childHash);
		m_typeChildrenMappingByFileorGuid.Insert(hash, temp);
	}
}

void shaderMaterialGeoManager::RemoveChildShaderMaterialGeoType(const char* pcGeoTypePathAndFilename, unsigned int childHash)
{
	unsigned int hash = atStringHash(pcGeoTypePathAndFilename);
	atArray< unsigned int > * found = m_typeChildrenMappingByFileorGuid.Access(hash);
	if (found)
	{
		found->DeleteMatches(childHash);

		//no more mappings we can remove ourselves
		if (!found->GetCount())
		{
			m_typeChildrenMappingByFileorGuid.Delete(hash);
		}
	}
	else
	{
		Errorf("Attempting to remove a child from type mapping [%s] but it is not in the list.", pcGeoTypePathAndFilename);
		Assertf(false, "Attempting to remove a child from type mapping [%s] but it is not in the list.", pcGeoTypePathAndFilename);
	}
}

void shaderMaterialGeoManager::updateChildren(const shaderMaterialGeoInstance* instance)
{
	//kinda lame should not have to do this ... update all my children
	unsigned int hash = atStringHash(instance->GetInstancePathAndFilename());
	atArray <grmShader*>* found = m_instanceChildrenMappingByFile.Access(hash);
	if (found)
	{
		for(int shader = 0; shader < found->GetCount(); shader++)
		{
			Assert((*found)[shader]);
			// FIXME (*found)[shader]->CopyFromMTL((*instance));
		}
	}
	else
	{
		//try finding it through the guid
		unsigned int hash = atStringHash(instance->GetUID());
		atArray <grmShader*>* found = m_instanceChildrenMappingByGUID.Access(hash);
		if (found)
		{
			for(int shader = 0; shader < found->GetCount(); shader++)
			{
				Assert((*found)[shader]);
				// FIXME (*found)[shader]->CopyFromMTL((*instance));
			}
		}
	}
}

void shaderMaterialGeoManager::updateChildren(const shaderMaterialGeoType* parent)
{
	//kinda lame should not have to do this ... update all my children
	unsigned int hash = atStringHash(parent->GetTypePathAndFilename());
	atArray < unsigned int >* found = m_typeChildrenMappingByFileorGuid.Access(hash);
	if (found)
	{
		for(int child = 0; child < found->GetCount(); child++)
		{
			unsigned int hash = (*found)[child];
			//check for type mapping
			unsigned short* found_index = m_shaderMaterialGeoTypeMappingByFile.Access(hash);
			if (found_index)
			{
				m_apShaderMaterialGeoTypes[(*found_index)]->CopyParamValuesSelectively((*parent));
				//keep the train a rolling
				updateChildren(m_apShaderMaterialGeoTypes[(*found_index)]);
			}
			else
			{
				//check for instance mapping by file
				unsigned short* found_index = m_shaderMaterialGeoInstanceMappingByFile.Access(hash);
				if (found_index)
				{
					m_apShaderMaterialGeoInstances[(*found_index)]->CopyParamValuesSelectively((*parent));
					//keep the train a rolling
					updateChildren(m_apShaderMaterialGeoInstances[(*found_index)]);
				}
				else
				{
					//check for instance mapping by guid
					unsigned short* found_index = m_shaderMaterialGeoInstanceMappingByGUID.Access(hash);
					if (found_index)
					{
						m_apShaderMaterialGeoInstances[(*found_index)]->CopyParamValuesSelectively((*parent));
						//keep the train a rolling
						updateChildren(m_apShaderMaterialGeoInstances[(*found_index)]);
					}
					else
					{
						//checked everything ... error
						Errorf("Attempting to update children of type [%s] and was unable to find anything with hash [%d].", parent->GetTypePathAndFilename(), hash);
						Assertf(false, "Attempting to update children of type [%s] and was unable to find anything with hash [%d].", parent->GetTypePathAndFilename(), hash);
					}
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

const atString& shaderMaterialGeoManager::GetBaseGEOPath()
{
	static atString strBaseGEOPath("");
	static char attempted = 0;
	if(strBaseGEOPath == "" && !attempted)
	{
		//initModuleSettings::GetModuleSetting(atString("BaseGEOPath"), strBaseGEOPath);
		attempted = 1;
	}
	return strBaseGEOPath;
}


const atString& shaderMaterialGeoManager::GetPathToTemplates()
{
	static atString strPathToTemplate("");
	static char attempted = 0;
	if(strPathToTemplate == "" && !attempted)
	{
		//initModuleSettings::GetModuleSetting(atString("PathToTemplate"), strPathToTemplate);
		attempted = 1;
	}
	return strPathToTemplate;
}

const atString& shaderMaterialGeoManager::GetPathToTypes()
{
	static atString strPathToType("");
	static char attempted = 0;
	if(strPathToType == "" && !attempted)
	{
		//initModuleSettings::GetModuleSetting(atString("PathToType"), strPathToType);
		attempted = 1;
	}
	return strPathToType;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void shaderMaterialGeoManager::getValidTemplateFullPath(char* _OutFullPathBuffer, const char* _FileName) const
{
	Assert(_OutFullPathBuffer);
	Assert(_FileName);

	CleanFilename(_OutFullPathBuffer, _FileName);

	//////////////////////////////////////////////////////////////////////////
	//strip off any path info.
	//This seems pretty lame. but since we store the template as a file name only in the .mtlgeo and .geotype files
	//	this is the only way to ensure that the hashes used for caching are the same across all cases
	char acGeoTemplateName[BUFFER_LEN];
	strcpy(acGeoTemplateName, _FileName);
	if(strrchr(acGeoTemplateName, '/') != NULL)
	{
		strcpy(acGeoTemplateName, strrchr(acGeoTemplateName, '/') + 1);
	}
	else if(strrchr(acGeoTemplateName, '\\') != NULL)
	{
		strcpy(acGeoTemplateName, strrchr(acGeoTemplateName, '\\') + 1);
	}
	if(strrchr(acGeoTemplateName, '.') != NULL)
	{
		char* pcPosOfDot = strrchr(acGeoTemplateName, '.');
		(*pcPosOfDot) = '\0';
	}
	//////////////////////////////////////////////////////////////////////////

	char strGeoTemplatePath[256];

	safecpy(strGeoTemplatePath, GetBaseGEOPath().c_str(), sizeof(strGeoTemplatePath));
	if(strGeoTemplatePath[0] != 0)
	{
		safecat(strGeoTemplatePath, "GEOTemplates/", sizeof(strGeoTemplatePath));
		safecat(strGeoTemplatePath, acGeoTemplateName, sizeof(strGeoTemplatePath));
		safecat(strGeoTemplatePath, ".geotemplate", sizeof(strGeoTemplatePath));

		// Does it exist?
		if(ASSET.Exists(strGeoTemplatePath, NULL))
		{
			strcpy(_OutFullPathBuffer, strGeoTemplatePath);
			return;
		}
	}

	// That didn't work, so look in the template folder
	safecpy(strGeoTemplatePath, GetPathToTemplates().c_str(), sizeof(strGeoTemplatePath));
	if(strGeoTemplatePath[0] != 0)
	{
		safecat(strGeoTemplatePath, acGeoTemplateName, sizeof(strGeoTemplatePath));
		safecat(strGeoTemplatePath, ".geotemplate", sizeof(strGeoTemplatePath));

		// Does it exist?
		if(ASSET.Exists(strGeoTemplatePath, NULL))
		{
			strcpy(_OutFullPathBuffer, strGeoTemplatePath);
			return;
		}
	}

	// That didn't work either!  So just use the path I was given
	if(ASSET.Exists(_FileName, NULL))
	{
		strcpy(_OutFullPathBuffer, _FileName);
		//Throw a warning because this may cause errors if the game has to load an mtlgeo that is based on a template not in
		// a module setting based path.
		//Warningf("Did not find the .geotemplate in any module setting provided template directory using name as is. [%s]", _FileName);
	}
}

void shaderMaterialGeoManager::getValidTypeFullPath(char* _OutFullPathBuffer, const char* _FileName) const
{
	Assert(_OutFullPathBuffer);
	Assert(_FileName);

	//Displayf("Begin Find valid type full path for %s.", _FileName);

	// if the filename exists then use that. this done so you dont have to have all .geotypes stored in the same place.
	if(ASSET.Exists(_FileName, NULL))
	{
		strcpy(_OutFullPathBuffer, _FileName);
		//Displayf("Found and using %s and output %s.", _FileName, _OutFullPathBuffer);
		//Displayf("End Find valid type full path for %s.", _FileName);
		return;
	}
	else
	{
		//search for it in our known paths.

		CleanFilename(_OutFullPathBuffer, _FileName);

		//////////////////////////////////////////////////////////////////////////
		//strip off any path info.
		//This seems pretty lame. but since we store the Type as a file name only in the .mtlgeo and .geotype files
		//	this is the only way to ensure that the hashes used for caching are the same across all cases
		char acGeoTypeName[BUFFER_LEN];
		strcpy(acGeoTypeName, _FileName);
		if(strrchr(acGeoTypeName, '/') != NULL)
		{
			strcpy(acGeoTypeName, strrchr(acGeoTypeName, '/') + 1);
		}
		else if(strrchr(acGeoTypeName, '\\') != NULL)
		{
			strcpy(acGeoTypeName, strrchr(acGeoTypeName, '\\') + 1);
		}
		if(strrchr(acGeoTypeName, '.') != NULL)
		{
			char* pcPosOfDot = strrchr(acGeoTypeName, '.');
			(*pcPosOfDot) = '\0';
		}
		//////////////////////////////////////////////////////////////////////////

		atString strGeoTypePath = GetBaseGEOPath();
		//Displayf("Try path %s", strGeoTypePath.c_str());
		if(strGeoTypePath != "")
		{
			strGeoTypePath += "GEOTypes/";
			strGeoTypePath += acGeoTypeName;
			strGeoTypePath += ".geotype";

			// Does it exist?
			if(ASSET.Exists(strGeoTypePath.c_str(), NULL))
			{
				strcpy(_OutFullPathBuffer, strGeoTypePath.c_str());
				//Displayf("Found and using %s and output %s.", strGeoTypePath.c_str(), _OutFullPathBuffer);
				//Displayf("End Find valid type full path for %s.", _FileName);
				return;
			}
		}

		// That didn't work, so look in the Type folder
		strGeoTypePath = GetPathToTypes();
		//Displayf("Try path %s", strGeoTypePath.c_str());
		if(strGeoTypePath != "")
		{
			strGeoTypePath += acGeoTypeName;
			strGeoTypePath += ".geotype";

			// Does it exist?
			if(ASSET.Exists(strGeoTypePath.c_str(), NULL))
			{
				strcpy(_OutFullPathBuffer, strGeoTypePath.c_str());
				//Displayf("Found and using %s and output %s.", strGeoTypePath.c_str(), _OutFullPathBuffer);
				//Displayf("End Find valid type full path for %s.", _FileName);
				return;
			}
		}
	}

	//Displayf("Nothing copied over to _OutFullPathBuffer this is probably an issue.");
	//Displayf("End Find valid type full path for %s.", _FileName);
}
