// 
// shaderMaterialGeoInstance/shaderMaterialGeoInstance.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef SHADER_MATERIAL_GEO_Instance_H
#define SHADER_MATERIAL_GEO_Instance_H

#include "parser/macros.h"
#include "string/string.h"
#include "shaderMaterialGeoType.h"
#include "shaderMaterialGeoParamValue.h"

namespace rage {

	class shaderMaterialGeoInstance : public shaderMaterialGeoType
	{
	public:
		shaderMaterialGeoInstance();
		virtual ~shaderMaterialGeoInstance();
		virtual bool SaveTo(const char * szFilename) const;

		bool LoadFromGeoInstance(const char * szFilename);
		bool ConstructFromParentType(const shaderMaterialGeoType* pobParentShaderMaterialGeoType);

		const char* GetInstanceFilename() const
		{
			return GetTypeFilename();
		}
		const char* GetInstancePathAndFilename() const
		{
			return GetTypePathAndFilename();
		}
		const char* GetInstanceName() const
		{
			return GetTypeName();
		}

		const char* GetTemplateName() const;

		void CopyParamValuesSelectively(const shaderMaterialGeoType & obThat);
		const shaderMaterialGeoTemplate* GetTemplate() const;
	};

} // end namespace rage

#endif
