// 
// shaderMaterialGeoTemplate/shaderMaterialGeoTemplate.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamDescription.h"
// #include "shaderMaterialGeoParamDescriptionBool.h"
#include "shaderMaterialGeoParamDescriptionFloat.h"
// #include "shaderMaterialGeoParamDescriptionInt.h"
#include "shaderMaterialGeoParamDescriptionMatrix34.h"
#include "shaderMaterialGeoParamDescriptionMatrix44.h"
#include "shaderMaterialGeoParamDescriptionString.h"
#include "shaderMaterialGeoParamDescriptionTexture.h"
#include "shaderMaterialGeoParamDescriptionVector2.h"
#include "shaderMaterialGeoParamDescriptionVector3.h"
#include "shaderMaterialGeoParamDescriptionVector4.h"
#include "shaderMaterialGeoManager.h"
#include "shaderMaterialGeoTemplate.h"

#include "bank/msgbox.h"
#include "file/asset.h"
#include "file/device.h"
#include "grmodel/namedbuckets.h"
#include "grmodel/shader.h"
#include "grmodel/shaderfx.h"
#include "parser/manager.h"


#if __WIN32PC
#pragma warning(push)
#pragma warning(disable : 4668)
#include <Rpc.h>

#pragma comment(lib, "rpcrt4.lib")

#pragma warning(pop)
#endif

#define BUFFER_LEN	1024

using namespace rage;

// atBucket<shaderMaterialGeoTemplate::shaderMaterialGeoTemplateMap> shaderMaterialGeoTemplate::sm_aGeoTemplates;


shaderMaterialGeoTemplate::shaderMaterialGeoTemplate()
{
	SetShaderPathAndFilename("");
	m_iBucketNumber = 0;
}

shaderMaterialGeoTemplate::shaderMaterialGeoTemplate(const char * szFilename)
{
	SetShaderPathAndFilename("");
	m_iBucketNumber = 0;

	// Load the data
	LoadFromGeoTemplate(szFilename);
}

shaderMaterialGeoTemplate::~shaderMaterialGeoTemplate()
{
	for(int i=0; i<m_apShaderParamDescriptions.GetCount(); i++)
	{
		delete m_apShaderParamDescriptions[i];
	}
}

bool shaderMaterialGeoTemplate::CreateFromShader(const char * pcShaderFilename)
{
	grmShader *pShader = grmShaderFactory::GetInstance().Create();
	if (!pShader)
	{
		return false;
	}

	if (!pShader->Load(pcShaderFilename))
	{
		return false;
	}

	grcEffect * pTemplate = &pShader->GetInstanceData().GetBasis();
	if (!pTemplate)
	{
		return false;
	}

	if (const char *names = pTemplate->GetPropertyValue("drawBucketNamed",(const char*)0)) {
		grmNamedBuckets		bucketMask( names );
		if ( bucketMask() ) 
			SetBucketNumber (bucketMask.GetNumber()); 
	}
	else
		SetBucketNumber(pTemplate->GetPropertyValue("drawBucket", 0));

	m_apShaderParamDescriptions.clear();
#if EFFECT_PRESERVE_STRINGS
	for (int i=0; i<pShader->GetInstancedVariableCount(); i++) 
	{
		grmVariableInfo varInfo;
		pShader->GetInstancedVariableInfo(i,varInfo);
		grcEffectVar var = pTemplate->LookupVar(varInfo.m_Name);

		//A bit of a hack here to force creation of a combo box to be populated from a file
		//	this is being done because we dont have a string type in HLSL so we use this hack to
		//	get around that and allow us to create a combo box anyway.
		if (varInfo.m_UiWidget && !strcmp(varInfo.m_UiWidget,"menu"))
		{
			varInfo.m_Type = grcEffect::VT_STRING;
		}

		shaderMaterialGeoParamDescription * pobParamDescription = NULL;
		switch(varInfo.m_Type)
		{
		/* case grcEffect::VT_INT:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionInt();
				int iDefaultValue = 0;
				pTemplate->GetVar(var, iDefaultValue);
				pobParamDescription->SetDefaultValue(iDefaultValue);
				break;
			} */
		case grcEffect::VT_FLOAT:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionFloat();
				float fDefaultValue = 0.0f;
				pTemplate->GetVar(var, fDefaultValue);
				pobParamDescription->SetDefaultValue(fDefaultValue);
				break;
			}
		case grcEffect::VT_VECTOR2:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionVector2();
				Vector2 vDefaultValue;
				pTemplate->GetVar(var, vDefaultValue);
				pobParamDescription->SetDefaultValue(vDefaultValue);
				break;
			}
		case grcEffect::VT_VECTOR3:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionVector3();
				Vector3 vDefaultValue;
				pTemplate->GetVar(var, vDefaultValue);
				pobParamDescription->SetDefaultValue(vDefaultValue);
				break;
			}
		case grcEffect::VT_VECTOR4:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionVector4();
				Vector4 vDefaultValue;
				pTemplate->GetVar(var, vDefaultValue);
				pobParamDescription->SetDefaultValue(vDefaultValue);
				break;
			}
		case grcEffect::VT_TEXTURE:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionTexture();
				pobParamDescription->SetDefaultValue("");
				break;
			}
		/* case grcEffect::VT_BOOL:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionBool();
				bool bDefaultValue = true;
				pTemplate->GetVar(var, bDefaultValue);
				pobParamDescription->SetDefaultValue(bDefaultValue);
				break;
			} */
		case grcEffect::VT_MATRIX34:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionMatrix34();
				Matrix34 obDefaultValue;
				pTemplate->GetVar(var, obDefaultValue);
				pobParamDescription->SetDefaultValue(obDefaultValue);
				break;
			}
		case grcEffect::VT_MATRIX44:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionMatrix44();
				Matrix44 obDefaultValue;
				pTemplate->GetVar(var, obDefaultValue);
				pobParamDescription->SetDefaultValue(obDefaultValue);
				break;
			}
		case grcEffect::VT_STRING:
			{
				pobParamDescription = rage_new shaderMaterialGeoParamDescriptionString();
				if (varInfo.m_AssetName)
				{
					pobParamDescription->SetDefaultValue(varInfo.m_AssetName);
				}
				else
				{
					pobParamDescription->SetDefaultValue("");
				}				
				break;
			}
		default:
			AssertMsg(0 , "Invalid param type");
		}

		pobParamDescription->SetName(varInfo.m_Name);

		//////////////////////////////////////////////////////////////////////////
		// Lets fixup any data that could be bad for Maya like spaces in the UI name
		char szUiName[128];
		strcpy(szUiName, varInfo.m_UiName);

		int count = 0;
		int len = (int) strlen(szUiName);
		for (int i=0; i<len; i++)
		{
			if ( (szUiName[i] != ' ') &&
				(szUiName[i] != '-') )
			{
				szUiName[count] = szUiName[i];
				count++;
			}
		}
		szUiName[count] = 0;
		//////////////////////////////////////////////////////////////////////////

		pobParamDescription->SetUIName(szUiName);
		pobParamDescription->SetUIHint(varInfo.m_UiHint);
		pobParamDescription->SetUIWidget(varInfo.m_UiWidget);

		shaderMaterialGeoParamDescription::ValueSource source = shaderMaterialGeoParamDescription::GeoInstance;
		if (varInfo.m_GeoValueSource)
		{
			if (!strcmp(varInfo.m_GeoValueSource, "type"))
			{
				source = shaderMaterialGeoParamDescription::GeoType;
			}
			else if (!strcmp(varInfo.m_GeoValueSource, "template"))
			{
				source = shaderMaterialGeoParamDescription::GeoTemplate;
			}
		}
		else
		{
			//This is the old way and should go away at some point
			if (varInfo.m_UiHidden)
			{
				source = shaderMaterialGeoParamDescription::GeoType;
			}
		}
		pobParamDescription->SetValueSource(source);
		
		pobParamDescription->SetDefaultValueUvSetIndex(varInfo.m_MaterialDataUvSetIndex);
		pobParamDescription->SetUIMin(varInfo.m_UiMin);
		pobParamDescription->SetUIMax(varInfo.m_UiMax);
		pobParamDescription->SetUIStep(varInfo.m_UiStep);
		pobParamDescription->SetUvSetIndex(varInfo.m_UvSetIndex);
		if(varInfo.m_UvSetName)
		{
			pobParamDescription->SetUvSetName(varInfo.m_UvSetName);
		}
		if(varInfo.m_TextureOutputFormats)
		{
			pobParamDescription->SetTextureOutputFormats(varInfo.m_TextureOutputFormats);
		}

		m_apShaderParamDescriptions.PushAndGrow(pobParamDescription);
	}
#endif

	delete pShader;

	SetShaderPathAndFilename(pcShaderFilename);

	// Is there a .vertexColorMappings file for this shader?
	char acVertexColorMappingsPathAndFilename[BUFFER_LEN];
	strcpy(acVertexColorMappingsPathAndFilename, pcShaderFilename);
	strcat(acVertexColorMappingsPathAndFilename, ".vertexColorMappings");
	if(ASSET.Exists(acVertexColorMappingsPathAndFilename, NULL))
	{
		// There is a vertexColorMappings file, so parse it and set up my vertex colors
		// Load it from the stream
		parTree*		pobTree = PARSER.LoadTree(acVertexColorMappingsPathAndFilename, NULL);
		parTreeNode*	pobRootNode = pobTree->GetRoot();

		// Get the source colours
		parTreeNode*	pobSourceColoursNode = pobRootNode->FindChildWithName("rage__rsmSourceVertexColourDescriptions");
		Assertf(pobSourceColoursNode, "Unable to find <rage__rsmSourceVertexColourDescriptions> tag in vertexColorMappings file %s", acVertexColorMappingsPathAndFilename);
		bool bSuccess = PARSER.LoadObject(pobSourceColoursNode, m_obSourceVertexColourDescriptions);
		if(!bSuccess)
		{
			delete pobTree;
			return false;
		}

		// Get the Dest colours
		parTreeNode*	pobRunTimeColoursNode = pobRootNode->FindChildWithName("rage__rsmRunTimeVertexColourDescriptions");
		Assertf(pobRunTimeColoursNode, "Unable to find <rage__rsmRunTimeVertexColourDescriptions> tag in vertexColorMappings file %s", acVertexColorMappingsPathAndFilename);
		bSuccess = PARSER.LoadObject(pobRunTimeColoursNode, m_obRunTimeVertexColourDescriptions);
		if(!bSuccess)
		{
			delete pobTree;
			return false;
		}
		delete pobTree;
	}

	return true;
}

bool shaderMaterialGeoTemplate::SaveTo(const char * szFilename) const
{
	(const_cast<shaderMaterialGeoTemplate*>(this))->SetTemplatePathAndFilename(szFilename);
	// Does it end in .geotemplate?
	if(strstr(GetTemplatePathAndFilename(), ".geotemplate"))
	{
		// It is a GeoMtl, so Save it accordingly
		return SaveToGeoTemplate(szFilename);
	}
	else
	{
		Warningf("File %s has is not recongnized as a MTL template adding .geotemplate ext and attempting save.", szFilename);
		// It doesn't end in any useful extension, so add the geo one and save it as a geo
		char acGeoMtlPathAndFilename[512];
		strcpy(acGeoMtlPathAndFilename, szFilename);
		strcat(acGeoMtlPathAndFilename, ".geotemplate");
		return SaveTo(acGeoMtlPathAndFilename);
	}
}

bool shaderMaterialGeoTemplate::LoadFromGeoTemplate(fiStream * s)
{
	// Load it from the stream
	parTree*		pobTree = PARSER.LoadTree(s);
	parTreeNode*	pobRootNode = pobTree->GetRoot();

	// Get the members
	parTreeNode*	pobRootMembersNode = pobRootNode->FindChildWithName("Members");
	Assertf(pobRootMembersNode, "Unable to find <Members> tag in geo template file %s", s->GetName());

	// Load the two things I care about
	for(int i=0; i<pobRootMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildNode = pobRootMembersNode->FindChildWithIndex(i);
		// Displayf("%p", pobChildNode);

		// Get the type of node
		const char* pcTypeOfNode = pobChildNode->GetElement().GetName();
		// Displayf("%s", pcTypeOfNode);

		if(!stricmp(pcTypeOfNode, "file"))
		{
			// It is a file node, so could be one I'm interested in
			const char* pcNodeName = pobChildNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);
			// Displayf("%s", pcNodeName);

			if(!stricmp(pcNodeName, "ShaderFilename"))
			{
				// Bingo!  Found the ShaderFilename
				char acBuffer[512];
				StringNormalize(acBuffer, pobChildNode->GetElement().FindAttributeStringValue("init", "Nonsense", NULL, 0), sizeof(acBuffer));
				SetShaderPathAndFilename(acBuffer);
			}
		}
		if(!stricmp(pcTypeOfNode, "int"))
		{
			// It is a int node, so could be one I'm interested in
			const char* pcNodeName = pobChildNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);
			// Displayf("%s", pcNodeName);

			if(!stricmp(pcNodeName, "BucketNumber"))
			{
				// BucketNumber
				m_iBucketNumber = pobChildNode->GetElement().FindAttributeIntValue("init", 0);
			}
		}
	}

	// Get the template name
	// Get the GeoTemplates
	parTreeNode*	pobRootGeoTemplatesNode = pobRootNode->FindChildWithName("GeoTemplates");
	Assertf(pobRootGeoTemplatesNode, "Unable to find <GeoTemplates> tag in geo template file %s", s->GetName());

	for(int iGeoTemplateNo=0; iGeoTemplateNo<pobRootGeoTemplatesNode->FindNumChildren(); iGeoTemplateNo++)
	{
		// Get child template
		parTreeNode*	pobRootGeoTemplateNode = pobRootGeoTemplatesNode->FindChildWithIndex(iGeoTemplateNo);
		const char* pcSubGeoTemplateName = pobRootGeoTemplateNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		bool bShaderMember = (strcmp(pcSubGeoTemplateName, "ShaderMembers") == 0);

		if(bShaderMember)
		{
			// ********************************************************************
			// Get members
			// ********************************************************************
			parTreeNode*	pobMembersNode = pobRootGeoTemplateNode->FindChildWithName("Members");
			Assertf(pobMembersNode, "Unable to find <Members> tag in geo template file %s", s->GetName());

			// Create a material based on the remaining template file
			for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
			{
				parTreeNode*	pobChildNode = pobMembersNode->FindChildWithIndex(i);
				// Displayf("%p", pobChildNode);

				// Get the type of node
				const char* pcTypeOfNode = pobChildNode->GetElement().GetName();
				// Displayf("%s", pcTypeOfNode);

				// Get the name of the node
				const char* pcNodeName = pobChildNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);
				// Displayf("%s", pcNodeName);
				const char* pcUiName = pobChildNode->GetElement().FindAttributeStringValue("displayName", "Nonsense", NULL, 0);
				// Displayf("%s", pcUiName);
				
				shaderMaterialGeoParamDescription* pobParam = NULL;
				if(strcmp(pcTypeOfNode, "Matrix34") == 0)
				{
					// It is an matrix34 node, so create an int param and add it
					shaderMaterialGeoParamDescriptionMatrix34* pobMatrix34Param = rage_new shaderMaterialGeoParamDescriptionMatrix34();

					// Get initial values
					Matrix34 obInitMatrix;
					parTreeNode* pobInitNode = pobChildNode->FindChildWithName("init");
					Assertf(pobInitNode, "Unable to find <init> child tag in geo template file %s", s->GetName());

					parTreeNode* pobANode = pobInitNode->FindChildWithName("a");
					Assertf(pobANode, "Unable to find <a> child tag in geo template file %s", s->GetName());

					obInitMatrix.a.x = pobANode->GetElement().FindAttributeFloatValue("x", 0.0f);
					obInitMatrix.a.y = pobANode->GetElement().FindAttributeFloatValue("y", 0.0f);
					obInitMatrix.a.z = pobANode->GetElement().FindAttributeFloatValue("z", 0.0f);


					parTreeNode* pobBNode = pobInitNode->FindChildWithName("b");
					Assertf(pobBNode, "Unable to find <b> child tag in geo template file %s", s->GetName());

					obInitMatrix.b.x = pobBNode->GetElement().FindAttributeFloatValue("x", 0.0f);
					obInitMatrix.b.y = pobBNode->GetElement().FindAttributeFloatValue("y", 0.0f);
					obInitMatrix.b.z = pobBNode->GetElement().FindAttributeFloatValue("z", 0.0f);


					parTreeNode* pobCNode = pobInitNode->FindChildWithName("c");
					Assertf(pobCNode, "Unable to find <c> child tag in geo template file %s", s->GetName());

					obInitMatrix.c.x = pobCNode->GetElement().FindAttributeFloatValue("x", 0.0f);
					obInitMatrix.c.y = pobCNode->GetElement().FindAttributeFloatValue("y", 0.0f);
					obInitMatrix.c.z = pobCNode->GetElement().FindAttributeFloatValue("z", 0.0f);


					parTreeNode* pobDNode = pobInitNode->FindChildWithName("d");
					Assertf(pobDNode, "Unable to find <d> child tag in geo template file %s", s->GetName());

					obInitMatrix.d.x = pobDNode->GetElement().FindAttributeFloatValue("x", 0.0f);
					obInitMatrix.d.y = pobDNode->GetElement().FindAttributeFloatValue("y", 0.0f);
					obInitMatrix.d.z = pobDNode->GetElement().FindAttributeFloatValue("z", 0.0f);

					pobMatrix34Param->SetDefaultValue(obInitMatrix);
					pobParam = pobMatrix34Param;
				}
				else if(strcmp(pcTypeOfNode, "Matrix44") == 0)
				{
					// It is an matrix44 node, so create an int param and add it
					shaderMaterialGeoParamDescriptionMatrix44* pobMatrix44Param = rage_new shaderMaterialGeoParamDescriptionMatrix44();

					// Get initial values
					Matrix44 obInitMatrix;
					parTreeNode* pobInitNode = pobChildNode->FindChildWithName("init");
					Assertf(pobInitNode, "Unable to find <init> child tag in geo template file %s", s->GetName());

					parTreeNode* pobANode = pobInitNode->FindChildWithName("a");
					Assertf(pobANode, "Unable to find <a> child tag in geo template file %s", s->GetName());

					obInitMatrix.a.x = pobANode->GetElement().FindAttributeFloatValue("x", 0.0f);
					obInitMatrix.a.y = pobANode->GetElement().FindAttributeFloatValue("y", 0.0f);
					obInitMatrix.a.z = pobANode->GetElement().FindAttributeFloatValue("z", 0.0f);
					obInitMatrix.a.w = pobANode->GetElement().FindAttributeFloatValue("w", 0.0f);


					parTreeNode* pobBNode = pobInitNode->FindChildWithName("b");
					Assertf(pobBNode, "Unable to find <b> child tag in geo template file %s", s->GetName());

					obInitMatrix.b.x = pobBNode->GetElement().FindAttributeFloatValue("x", 0.0f);
					obInitMatrix.b.y = pobBNode->GetElement().FindAttributeFloatValue("y", 0.0f);
					obInitMatrix.b.z = pobBNode->GetElement().FindAttributeFloatValue("z", 0.0f);
					obInitMatrix.b.w = pobBNode->GetElement().FindAttributeFloatValue("w", 0.0f);


					parTreeNode* pobCNode = pobInitNode->FindChildWithName("c");
					Assertf(pobCNode, "Unable to find <c> child tag in geo template file %s", s->GetName());

					obInitMatrix.c.x = pobCNode->GetElement().FindAttributeFloatValue("x", 0.0f);
					obInitMatrix.c.y = pobCNode->GetElement().FindAttributeFloatValue("y", 0.0f);
					obInitMatrix.c.z = pobCNode->GetElement().FindAttributeFloatValue("z", 0.0f);
					obInitMatrix.c.w = pobCNode->GetElement().FindAttributeFloatValue("w", 0.0f);


					parTreeNode* pobDNode = pobInitNode->FindChildWithName("d");
					Assertf(pobDNode, "Unable to find <d> child tag in geo template file %s", s->GetName());

					obInitMatrix.d.x = pobDNode->GetElement().FindAttributeFloatValue("x", 0.0f);
					obInitMatrix.d.y = pobDNode->GetElement().FindAttributeFloatValue("y", 0.0f);
					obInitMatrix.d.z = pobDNode->GetElement().FindAttributeFloatValue("z", 0.0f);
					obInitMatrix.d.w = pobDNode->GetElement().FindAttributeFloatValue("w", 0.0f);

					pobMatrix44Param->SetDefaultValue(obInitMatrix);
					pobParam = pobMatrix44Param;
				}
				else if(strcmp(pcTypeOfNode, "geo") == 0)
				{
					// It is a complex node, probably either a string or a file(/texture)
					// I need to get the geo and work out which it is

					// Get the geo's node
					parTreeNode*	pobParamGeoRootNode = NULL;
					for(int iTempNo=0; iTempNo<pobRootGeoTemplatesNode->FindNumChildren(); iTempNo++)
					{
						const char* pcGeoTemplateName = pobRootGeoTemplatesNode->FindChildWithIndex(iTempNo)->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);
						if(strcmp(pcGeoTemplateName, pcNodeName) == 0)
						{
							// Bingo!
							pobParamGeoRootNode  = pobRootGeoTemplatesNode->FindChildWithIndex(iTempNo);
							break;
						}
					}
					Assertf(pobParamGeoRootNode, "Unable to find sub geo template <%s> child tag in geo template file %s", pcNodeName, s->GetName());

					// What type is it?
					const char* pcNameOfFirstChild = pobParamGeoRootNode->FindChildWithIndex(0)->FindChildWithIndex(0)->GetElement().GetName();
					/* if(strcmp(pcNameOfFirstChild, "bool") == 0)
					{
						// It is a bool node, so create a bool param and add it
						pobParam = rage_new shaderMaterialGeoParamDescriptionBool(pobParamGeoRootNode);
					}
					else */ if(strcmp(pcNameOfFirstChild, "float") == 0)
					{
						// It is a float node, so create a float param and add it
						pobParam = rage_new shaderMaterialGeoParamDescriptionFloat(pobParamGeoRootNode);
					}
					/* else if(strcmp(pcNameOfFirstChild, "int") == 0)
					{
						// It is an int node, so create an int param and add it
						pobParam = rage_new shaderMaterialGeoParamDescriptionInt(pobParamGeoRootNode);
					} */
					else if(strcmp(pcNameOfFirstChild, "string") == 0)
					{
						// It is a string node, so create a string param and add it
						pobParam = rage_new shaderMaterialGeoParamDescriptionString(pobParamGeoRootNode);
					}
					else if(strcmp(pcNameOfFirstChild, "file") == 0)
					{
						// It is a file node, so create a texture param and add it
						pobParam = rage_new shaderMaterialGeoParamDescriptionTexture(pobParamGeoRootNode);
					}
					else if(strcmp(pcNameOfFirstChild, "Vector2") == 0)
					{
						// It is a Vector2 node, so create a Vector2 param and add it
						pobParam = rage_new shaderMaterialGeoParamDescriptionVector2(pobParamGeoRootNode);
					}
					else if(strcmp(pcNameOfFirstChild, "Vector3") == 0)
					{
						// It is a Vector3 node, so create a Vector3 param and add it
						pobParam = rage_new shaderMaterialGeoParamDescriptionVector3(pobParamGeoRootNode);
					}
					else if(strcmp(pcNameOfFirstChild, "Vector4") == 0)
					{
						// It is a Vector4 node, so create a Vector4 param and add it
						pobParam = rage_new shaderMaterialGeoParamDescriptionVector4(pobParamGeoRootNode);
					}
					else
					{
						Assertf(false, "Unable to handle tag %s in geo template file %s", pcNameOfFirstChild, s->GetName());						
					}
				}
				else
				{
					// Badness
					Assertf(false, "The node is of type <%s> but I have no idea what to do with it", pcTypeOfNode);
				}

				// Set the name of the param
				if(strstr(pcNodeName, "Nonsense") == 0)
				{
					pobParam->SetName(pcNodeName);
				}
				if(strstr(pcUiName, "Nonsense") == 0)
				{
					pobParam->SetUIName(pcUiName);
				}

				// Add it
				m_apShaderParamDescriptions.PushAndGrow(pobParam);
			}
		}
		else if(strcmp(pcSubGeoTemplateName, "SourceVertexColourSets") == 0)
		{
			// VertexColourSets, so pass it on
			// pcSubGeoTemplateName = pobRootGeoTemplateNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);
			m_obSourceVertexColourDescriptions.ReadFromGeoTemplate(pobRootGeoTemplateNode, pobRootGeoTemplatesNode);
		}
		else if(strcmp(pcSubGeoTemplateName, "RunTimeVertexColourSets") == 0)
		{
			// VertexColourSets, so pass it on
			// pcSubGeoTemplateName = pobRootGeoTemplateNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);
			m_obRunTimeVertexColourDescriptions.ReadFromGeoTemplate(pobRootGeoTemplateNode, pobRootGeoTemplatesNode);
		}
	}

//	m_Material.SetShaderFullPathName(m_sShaderFilename);
//	m_Material.SetGeoTemplatePathAndFilename(pcGeoTemplatePathAndFilename);
	delete pobTree;
	pobTree = NULL;

	// Return success
	return true;
}

bool shaderMaterialGeoTemplate::LoadFromGeoTemplate(const char * szFilename)
{
	bool bResult = false;
	SetTemplatePathAndFilename(szFilename);
	fiStream * streamMtl = fiStream::PreLoad(fiStream::Open(szFilename));
	if (streamMtl)
	{
		bResult = LoadFromGeoTemplate(streamMtl);
		streamMtl->Close();
	}
	return bResult;
}

bool shaderMaterialGeoTemplate::SaveToGeoTemplate(fiStream * s) const
{
	// Construct an XML parser tree, for writting out
	parTreeNode* pobRootNode = rage_new parTreeNode();

	// Header
//	fprintf(s, "<?xml version=\"1.0\"?>\n");
//	fprintf(s, "\n");

	// Open template
	pobRootNode->GetElement().SetName("GeoTemplate");
	//pobRootNode->GetElement().AddAttribute("name", GetTemplateName());
	//pobRootNode->GetElement().AddAttribute("typename", GetTemplateName());
	//pobRootNode->GetElement().AddAttribute("instancename", GetTemplateName());
	//pobRootNode->GetElement().AddAttribute("instanceFileExtension", ".mtlgeo");

	// Open Geo GeoTemplates
	parTreeNode* pobGeoTemplatesNode = rage_new parTreeNode();
	pobGeoTemplatesNode->GetElement().SetName("GeoTemplates");
	pobGeoTemplatesNode->AppendAsChildOf(pobRootNode);

	// Shader params
	parTreeNode* pobShaderParamsNode = rage_new parTreeNode();
	pobShaderParamsNode->GetElement().SetName("GeoTemplate");
	pobShaderParamsNode->GetElement().AddAttribute("name", "ShaderMembers");
	pobShaderParamsNode->AppendAsChildOf(pobGeoTemplatesNode);

	parTreeNode* pobShaderParamsMembersNode = rage_new parTreeNode();
	pobShaderParamsMembersNode->GetElement().SetName("Members");
	pobShaderParamsMembersNode->AppendAsChildOf(pobShaderParamsNode);

	// Next write out the parameters of the actual material
	for(int i=0; i<m_apShaderParamDescriptions.GetCount(); i++)
	{
		m_apShaderParamDescriptions[i]->WriteAsGeoTemplateMember(pobShaderParamsMembersNode, pobGeoTemplatesNode);
	}

	// Vertex colours
	m_obSourceVertexColourDescriptions.WriteToGeoTemplate(NULL, pobGeoTemplatesNode);
	m_obRunTimeVertexColourDescriptions.WriteToGeoTemplate(NULL, pobGeoTemplatesNode);

	// Write out the members
	parTreeNode* pobMembersNode = rage_new parTreeNode();
	pobMembersNode->GetElement().SetName("Members");

	// ShaderFilename
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("file");
		pobNode->GetElement().AddAttribute("name", "ShaderFilename");
		pobNode->GetElement().AddAttribute("eo", "type");
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->GetElement().AddAttribute("init", (const char*)GetShaderPathAndFilename());
		pobNode->AppendAsChildOf(pobMembersNode);
	}
	// GeoTemplateFilename
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("file");
		pobNode->GetElement().AddAttribute("name", "TemplateFilename");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->GetElement().AddAttribute("init", (const char*)m_strTemplatePathAndFilename);
		pobNode->AppendAsChildOf(pobMembersNode);
	}
	// ShaderMembers
	{
		char acBuffer[255];
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("geo");
		pobNode->GetElement().AddAttribute("name", "ShaderMembers");
		formatf(acBuffer, sizeof(acBuffer), "%s/ShaderMembers", (const char*)GetTemplateName());
		pobNode->GetElement().AddAttribute("template", acBuffer);
		pobNode->GetElement().AddAttribute("mustExist", "true");
		pobNode->GetElement().AddAttribute("canDerive", "false");
		pobNode->AppendAsChildOf(pobMembersNode);
	}
	// SourceVertexColourSets
	{
		char acBuffer[255];
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("geo");
		pobNode->GetElement().AddAttribute("name", "SourceVertexColourSets");
		formatf(acBuffer, sizeof(acBuffer), "%s/SourceVertexColourSets", (const char*)GetTemplateName());
		pobNode->GetElement().AddAttribute("template", acBuffer);
		pobNode->GetElement().AddAttribute("mustExist", "true");
		pobNode->GetElement().AddAttribute("canDerive", "false");
		pobNode->AppendAsChildOf(pobMembersNode);
	}
	// RunTimeVertexColourSets
	{
		char acBuffer[255];
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("geo");
		pobNode->GetElement().AddAttribute("name", "RunTimeVertexColourSets");
		formatf(acBuffer, sizeof(acBuffer), "%s/RunTimeVertexColourSets", (const char*)GetTemplateName());
		pobNode->GetElement().AddAttribute("template", acBuffer);
		pobNode->GetElement().AddAttribute("mustExist", "true");
		pobNode->GetElement().AddAttribute("canDerive", "false");
		pobNode->AppendAsChildOf(pobMembersNode);
	}
	// BucketNumber
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("int");
		pobNode->GetElement().AddAttribute("name", "BucketNumber");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->GetElement().AddAttribute("init", m_iBucketNumber);
		pobNode->AppendAsChildOf(pobMembersNode);
	}

	pobMembersNode->AppendAsChildOf(pobRootNode);

	// Blat it all out
	parTree obTree;
	obTree.SetRoot(pobRootNode);
	PARSER.SaveTree(s, &obTree);

	return true;
}

bool shaderMaterialGeoTemplate::SaveToGeoTemplate(const char * szFilename) const
{
	// Make sure the destination folder exists
	ASSET.CreateLeadingPath(szFilename);

	// Write it out
	(const_cast<shaderMaterialGeoTemplate*>(this))->SetTemplatePathAndFilename(szFilename);
	bool bResult = false;
	fiStream * streamMtl = fiStream::Create(GetTemplatePathAndFilename());
	if (streamMtl)
	{
		bResult = SaveToGeoTemplate(streamMtl);
		streamMtl->Close();
	}
	return bResult;
}

const shaderMaterialGeoParamDescription* shaderMaterialGeoTemplate::GetParamDescription(const char* pcParamName) const
{
	for(int i=0; i<m_apShaderParamDescriptions.GetCount(); i++)
	{
		if(strcmp(m_apShaderParamDescriptions[i]->GetName(), pcParamName) == 0)
		{
			return m_apShaderParamDescriptions[i];
		}
	}
	return NULL;
}

void shaderMaterialGeoTemplate::SetTemplatePathAndFilename(const char* pcTemplatePathAndFilename)
{
	char acBuffer[1024];
	CleanFilename(acBuffer, pcTemplatePathAndFilename);
	m_strTemplatePathAndFilename = acBuffer;
	
	const char* pcPosOfFilenameStart = &(acBuffer[0]);
	if(strrchr(acBuffer, '/') != NULL)
	{
		pcPosOfFilenameStart = strrchr(acBuffer, '/') + 1;
	}
	if(strrchr(acBuffer, '.') != NULL)
	{
		char* pcPosOfDot = strrchr(acBuffer, '.');
		(*pcPosOfDot) = '\0';
	}
	m_strTemplateName = pcPosOfFilenameStart;
}

void shaderMaterialGeoTemplate::SetShaderPathAndFilename(const char* pcShaderPathAndFilename)
{
	char acBuffer[1024];
	CleanFilename(acBuffer, pcShaderPathAndFilename);
	m_strShaderPathAndFilename = acBuffer;

	const char* pcPosOfFilenameStart = &(acBuffer[0]);
	if(strrchr(acBuffer, '/') != NULL)
	{
		pcPosOfFilenameStart = strrchr(acBuffer, '/') + 1;
	}
	if(strrchr(acBuffer, '.') != NULL)
	{
		char* pcPosOfDot = strrchr(acBuffer, '.');
		(*pcPosOfDot) = '\0';
	}
	m_strShaderName = pcPosOfFilenameStart;
}
