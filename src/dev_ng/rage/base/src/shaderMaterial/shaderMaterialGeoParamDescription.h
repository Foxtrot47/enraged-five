// 
// shaderMaterial/shaderMaterialGeoParamDescription.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef SHADER_MATERIAL_GEO_PARAM_H
#define SHADER_MATERIAL_GEO_PARAM_H

#include "parser/macros.h"
#include "grCore/effect.h"
#include "grCore/effect_typedefs.h"
#include "string/string.h"

//#define FORCE_GEO_BASED_MTLS

namespace rage {

class grcEffect;
typedef grcEffect::VariableInfo grmVariableInfo;
class fiTokenizer;
class parTreeNode;

class shaderMaterialGeoParamDescription
{
public:
	enum ValueSource	
	{
		GeoInstance = 1,
		GeoType = 2,
		GeoTemplate = 4
	};

	shaderMaterialGeoParamDescription();
	virtual ~shaderMaterialGeoParamDescription();

	// Pure virtuals
	virtual int	GetType()				const {return grcEffect::VT_NONE;}
	virtual const char * GetTypeName()	const {return grcEffect::GetTypeName(grcEffect::VT_NONE);}

	// Accessors
	const char *	GetName()		const {return m_Name;}
	const char *	GetUIName()		const {return m_UIName;}
	const char *	GetUIWidget()	const {return m_UIWidget;}
	const char *	GetUIHelp()		const {return m_UIHelp;}
	const char *	GetUIHint()		const {return m_UIHint;}
	float			GetUIMin()		const {return m_UIMin;}
	float			GetUIMax()		const {return m_UIMax;}
	float			GetUIStep()		const {return m_UIStep;}
//	bool			GetUIHidden()	const {return m_UIHidden;}
	int				GetSourceOfParamValue()	const {return m_eValueSource;}
	int				GetUvSetIndex()	const {return m_UvSetIndex;}
	const char *	GetUvSetName()	const {return m_UvSetName;}
	const char *	GetTextureOutputFormats()	const {return m_TextureOutputFormats;}
	int				GetNoOfMipLevels() const {return m_iNoOfMipLevels;}
	int				GetMaterialDataUvSetIndex() const {return m_MaterialDefaultValueUvSetIndex;}

    virtual void Reset();

	virtual void Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase=true);
	virtual bool IsEqual(const shaderMaterialGeoParamDescription * param) const;

	// This function reads all the paramdata
	virtual bool ReadGeoParamDescriptionData(fiTokenizer & T);

	// This function writes out all the param data to the .shadergroup format
	virtual bool WriteGeoParamDescriptionData(fiTokenizer & T) const;

	// This function cleans up any of the parameter data
	virtual bool CleanUp() {return true;}

	bool Init(const grmVariableInfo & varInfo, grcEffect & effect);

	// Modifiers
	void SetName(const char * name){m_Name = name;}
	void SetUIName(const char * name)	{m_UIName = name;}
	void SetUIHint(const char * hint)	{m_UIHint = hint;}
	void SetUIWidget(const char * widget)	{m_UIWidget = widget;}
//	void SetUIHidden(bool bHidden)	{m_UIHidden = bHidden;}
	void SetDefaultValueUvSetIndex(int iMaterialDefaultValueUvSetIndex) {m_MaterialDefaultValueUvSetIndex = iMaterialDefaultValueUvSetIndex;}
	void SetUIMin(const float fUiMin) {m_UIMin = fUiMin;}
	void SetUIMax(const float fUiMax) {m_UIMax = fUiMax;}
	void SetUIStep(const float fUiStep) {m_UIStep = fUiStep;}
	void SetUvSetIndex(int iUvSetIndex)	{m_UvSetIndex = iUvSetIndex;}
	void SetUvSetName(const char* pcUvSetName)	{m_UvSetName = pcUvSetName;}
	void SetTextureOutputFormats(const char* pcTextureOutputFormats)	{m_TextureOutputFormats = pcTextureOutputFormats;}
	void SetValueSource(int iValueSource) {m_eValueSource = iValueSource;}

	virtual void SetDefaultValue(const bool & )			{AssertMsg(0,"Invalid call of the SetDefaultValue function");}
	virtual void SetDefaultValue(const float& )			{AssertMsg(0,"Invalid call of the SetDefaultValue function");}
	virtual void SetDefaultValue(const int& )			{AssertMsg(0,"Invalid call of the SetDefaultValue function");}
	virtual void SetDefaultValue(const Matrix34& )		{AssertMsg(0,"Invalid call of the SetDefaultValue function");}
	virtual void SetDefaultValue(const Matrix44& )		{AssertMsg(0,"Invalid call of the SetDefaultValue function");}
	virtual void SetDefaultValue(const char*)			{AssertMsg(0,"Invalid call of the SetDefaultValue function");}
	virtual void SetDefaultValue(const Vector2& )		{AssertMsg(0,"Invalid call of the SetDefaultValue function");}
	virtual void SetDefaultValue(const Vector3& )		{AssertMsg(0,"Invalid call of the SetDefaultValue function");}
	virtual void SetDefaultValue(const Vector4& )		{AssertMsg(0,"Invalid call of the SetDefaultValue function");}

	enum LIGHTMAP_TYPES
	{
		LT_LIGHTMAP = 0,
		LT_LIGHTMAP_HDR_COLOR,
		LT_LIGHTMAP_HDR_EXP,
		LIGHTMAP_TYPE_COUNT
	};

	static const char * LIGHTMAP_TYPE_NAMES[LIGHTMAP_TYPE_COUNT];

	bool IsLightmap(const int lightmapType) const;

	virtual void	WriteAsGeoTemplateMember(parTreeNode* /*pobParent*/, parTreeNode* /*pobGeoTemplatesNode*/) const	{AssertMsg(0,"Invalid call to WriteAsGeoTemplateMember");}
	virtual void	WriteAsGeoInstanceMember(parTreeNode* /*pobParent*/, const char* /*pcTemplateName*/) const	{AssertMsg(0,"Invalid call to WriteAsGeoInstanceMember");}

protected:
	shaderMaterialGeoParamDescription(const parTreeNode*	pobGeoParamDescriptionGeoRootNode);

	// Written data
	ConstString			m_Name;			// Semantic name
	ConstString			m_UIName;		// Name to display in widgets
	ConstString			m_UIWidget;		// Describes which type of UI widget to display
	ConstString			m_UIHelp;		// Extra info for widget tool tips
	ConstString			m_UIHint;
	float				m_UIMin;		// Minimum allowed value for a widget
	float				m_UIMax;
	float				m_UIStep;
	bool				m_UIHidden;		// Flag if hidden to the UI
	int					m_UvSetIndex;	// Uvset index for this texture
	ConstString			m_UvSetName;	// Uvset name for this texture
	ConstString			m_TextureOutputFormats;		// The output formats of the texture, this is a ; seperated list of 'platform'='format' pairs
	int					m_iNoOfMipLevels;			// No of mip map levels to create (If this value is zero, a complete mipmap chain is created.  This also the default.)
	int					m_MaterialDefaultValueUvSetIndex;	// DefaultValue for the material that gets pushed into the given texture coord.
													// -1 means it does not have any material data
	int					m_eValueSource;
};

} // end using namespace rage

#endif
