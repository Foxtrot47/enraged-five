// 
// shaderMaterial/shaderMaterialGeoParamValueMatrix34.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_MATRIX34_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_MATRIX34_H

#include "parser/macros.h"
#include "vector/matrix34.h"

#include "shaderMaterialGeoParamValue.h"

namespace rage {

class shaderMaterialGeoParamValueMatrix34 : public shaderMaterialGeoParamValue
{
public:

	shaderMaterialGeoParamValueMatrix34(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValueMatrix34(const shaderMaterialGeoParamValueMatrix34*	pobSourceParam);
	virtual ~shaderMaterialGeoParamValueMatrix34();

	int	GetType()				const	{return grcEffect::VT_MATRIX34;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_MATRIX34);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamValueMatrix34 * param);
	bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	bool Update(const shaderMaterialGeoParamValue * param);
	bool Update(const char * param, const float f);
	bool Update(const Matrix34& m);

	void SetDefaultValue(grcEffect & effect, grcEffectVar & var);

	bool ReadGeoParamValueData(fiTokenizer & T);
	bool WriteGeoParamValueData(fiTokenizer & T) const;

	Matrix34 GetValue() const {Matrix34 m; m.a = m_mValueA; m.b = m_mValueB; m.c = m_mValueC; m.d = m_mValueD; return m;}
	void SetValue(const Matrix34& obMatrix) 
	{
		m_mValueA = obMatrix.a; 
		m_mValueB = obMatrix.b; 
		m_mValueC = obMatrix.c; 
		m_mValueD = obMatrix.d; 
	}

	virtual void	WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// Helper functions
	bool UpdateVector(const char * param, const float f, const int len, Vector3 & v);

	// Value for this param
	Vector3 m_mValueA;
	Vector3 m_mValueB;
	Vector3 m_mValueC;
	Vector3 m_mValueD;
};

} // end namespace rage

#endif
