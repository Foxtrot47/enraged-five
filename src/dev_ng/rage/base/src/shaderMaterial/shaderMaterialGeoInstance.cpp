// 
// shaderMaterialGeoInstance/shaderMaterialGeoInstance.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "shaderMaterialGeoParamDescription.h"
// #include "shaderMaterialGeoParamDescriptionBool.h"
#include "shaderMaterialGeoParamDescriptionFloat.h"
// #include "shaderMaterialGeoParamDescriptionInt.h"
#include "shaderMaterialGeoParamDescriptionMatrix34.h"
#include "shaderMaterialGeoParamDescriptionMatrix44.h"
#include "shaderMaterialGeoParamDescriptionString.h"
#include "shaderMaterialGeoParamDescriptionTexture.h"
#include "shaderMaterialGeoParamDescriptionVector2.h"
#include "shaderMaterialGeoParamDescriptionVector3.h"
#include "shaderMaterialGeoParamDescriptionVector4.h"
#include "shaderMaterialGeoParamValue.h"
// #include "shaderMaterialGeoParamValueBool.h"
#include "shaderMaterialGeoParamValueFloat.h"
// #include "shaderMaterialGeoParamValueInt.h"
#include "shaderMaterialGeoParamValueMatrix34.h"
#include "shaderMaterialGeoParamValueMatrix44.h"
#include "shaderMaterialGeoParamValueString.h"
#include "shaderMaterialGeoParamValueTexture.h"
#include "shaderMaterialGeoParamValueVector2.h"
#include "shaderMaterialGeoParamValueVector3.h"
#include "shaderMaterialGeoParamValueVector4.h"
#include "shaderMaterialGeoManager.h"
#include "shaderMaterialGeoInstance.h"

#include "bank/msgbox.h"
#include "file/asset.h"
#include "file/device.h"
#include "grcore/texturereference.h"
#include "grmodel/namedbuckets.h"
#include "grmodel/shader.h"
#include "grmodel/shaderfx.h"
#include "parser/manager.h"

#if __WIN32PC
#pragma warning(push)
#pragma warning(disable : 4668)
#include <Rpc.h>

#pragma comment(lib, "rpcrt4.lib")

#pragma warning(pop)
#endif

using namespace rage;


shaderMaterialGeoInstance::shaderMaterialGeoInstance()
{
	m_bInstance = true;
}

shaderMaterialGeoInstance::~shaderMaterialGeoInstance()
{
}

bool shaderMaterialGeoInstance::LoadFromGeoInstance(const char * szFilename)
{
	bool bResult = false;
	fiStream * streamMtl = fiStream::PreLoad(fiStream::Open(szFilename));
	if (streamMtl)
	{
		SetTypePathAndFilename(szFilename);
		parTree*		pobTree = PARSER.LoadTree(streamMtl);
		parTreeNode*	pobRootNode = pobTree->GetRoot();

		//////////////////////////////////////////////////////////////////////////
		//NOTE: this is being done because people can copy/paste/rename mtlgeo files
		//	and thus duplicate the UUID that is supposed to be unique. So always assign
		//	and new one when we are loading. The loading of the old one is because assignment
		//	of the GUID only can happen in WIN32 so Xbox and PSN (Wii?) cant do this and need to keep
		//	the old one. Concerns about duplicate GUIDs are quelled by the fact that file names
		//	are used as identifiers instead. Geo instances are the only thing done this way.
		// Get the UUID
		const char* uuid = pobRootNode->GetElement().FindAttributeStringValue("uuid", "No default UID found in file", NULL, 0);
		//set it
		SetUIDToString(uuid);
		//make me have one.
		AssignNewUID();
		//////////////////////////////////////////////////////////////////////////

		const char* pcParentTypeName = NULL;

		//Displayf("Begin Loading Parent type for %s.", szFilename);
		parTreeNode* pobParentTypeWFullPath = pobRootNode->FindChildWithName("TypeFilename");
		if(pobParentTypeWFullPath)//try new way
		{
			pcParentTypeName = pobParentTypeWFullPath->GetData();
			Assertf(strlen(pcParentTypeName) != 0, "Unable to open %s file [%s] with empty parent type name", "mtl geo", szFilename);
			m_pobParentType = SHADER_MATERIAL_GEO_MANAGER.LoadShaderMaterialGeoType(pcParentTypeName);
		}
		else //try old way
		{
			pcParentTypeName = pobRootNode->GetElement().FindAttributeStringValue("base", "", NULL, 0);
			//old way
			Assertf(strlen(pcParentTypeName) != 0, "Unable to open %s file [%s] with empty parent type name", "mtl geo", szFilename);
			m_pobParentType = SHADER_MATERIAL_GEO_MANAGER.LoadShaderMaterialGeoType(pcParentTypeName);
		}
		//Displayf("End Loading Parent type for %s.", szFilename);

		if(m_pobParentType)
		{
			SetParentTypePathAndFilename(m_pobParentType->GetTypePathAndFilename());
			CopyParamValues(*m_pobParentType);

			// Now I am a pure shader with all my values set from the template and all my parents, 
			//	so set the overridden "shaderMaterialGeoParamDescription::GeoInstance" values from my mtl geo file
			parTreeNode*	pobShaderMembersNode = pobRootNode->FindChildWithName("ShaderMembers");
			if(pobShaderMembersNode)
			{
				for (int i=0; i<m_apShaderParamValues.GetCount(); i++) 
				{
					// Get the param in the geo file
					parTreeNode*	pobShaderMemberNode = pobShaderMembersNode->FindChildWithName(m_apShaderParamValues[i]->GetName());
					if(pobShaderMemberNode)
					{
						//dont enforce this now... will do it later.
						//if(m_apShaderParamValues[i]->GetSourceOfParamValue() & shaderMaterialGeoParamDescription::GeoInstance)
						{
							LoadParameterFromGeoType(pobShaderMemberNode, m_apShaderParamValues[i]);
						}
					}
				}
			}
			bResult = true;
		}
		else
		{
			char acBuffer[1024];
			CleanFilename(acBuffer, pcParentTypeName);

			//find the last \ or /
			if(strrchr(acBuffer, '/') != NULL)
			{
				char* pcPosOfSlash = strrchr(acBuffer, '/') + 1;
				(*pcPosOfSlash) = '\0';
			}
			if(strrchr(acBuffer, '\\') != NULL)
			{
				char* pcPosOfSlash = strrchr(acBuffer, '\\') + 1;
				(*pcPosOfSlash) = '\0';
			}

			Assertf(false, "Unable to load geotype [%s] used by mtlgeo [%s].  Solution: please make sure you have the latest type files located here: [%s].", pcParentTypeName, szFilename, acBuffer);
			Errorf("Unable to load geotype [%s] used by mtlgeo [%s].  Solution: please make sure you have the latest type files located here: [%s].", pcParentTypeName, szFilename, acBuffer);
		}
		streamMtl->Close();
	}

	return bResult;
}

bool shaderMaterialGeoInstance::ConstructFromParentType(const shaderMaterialGeoType* pobParentShaderMaterialGeoType)
{
	Assertf(pobParentShaderMaterialGeoType, "Unable to create a Instance from a NULL Type");

	m_pobParentType = pobParentShaderMaterialGeoType;
	SetParentTypePathAndFilename(pobParentShaderMaterialGeoType->GetTypePathAndFilename());
	CopyParamValues(*pobParentShaderMaterialGeoType);

	// Clear my filename
	SetTypePathAndFilename("");
	AssignNewUID();
	return true;
}

bool shaderMaterialGeoInstance::SaveTo(const char * szFilename) const
{
	//////////////////////////////////////////////////////////////////////////
	// THIS SEEMS LAME! dont know why we do this, but to be sure
	// Make sure I have a good extension
	char acGeoMtlPathAndFilename[512];
	CleanFilename(acGeoMtlPathAndFilename, szFilename);
	if(strrchr(acGeoMtlPathAndFilename, '.') != NULL)
	{
		char* pcPosOfDot = strrchr(acGeoMtlPathAndFilename, '.');
		(*pcPosOfDot) = '\0';
	}
	strcat(acGeoMtlPathAndFilename, ".mtlgeo");
	//////////////////////////////////////////////////////////////////////////

	//Assign a new guid?
	if (strcmp(GetInstancePathAndFilename(), acGeoMtlPathAndFilename))
	{
		AssignNewUID();
	}

	// Make sure the destination folder exists
	ASSET.CreateLeadingPath(acGeoMtlPathAndFilename);

	// Build the tree
	const shaderMaterialGeoTemplate* pobTemplate = GetTemplate();//get my template

	char acTempBuffer[255];
	// Construct an XML parser tree, for writting out
	parTreeNode* pobRootNode = rage_new parTreeNode();

	// Open instance
	pobRootNode->GetElement().SetName(pobTemplate->GetTemplateName());
	pobRootNode->GetElement().AddAttribute("template", pobTemplate->GetTemplateName());
	if(m_pobParentType)
	{
		char acParentTypeName[255];
		strcpy(acParentTypeName, m_pobParentType->GetTypeFilename());
		*strchr(acParentTypeName, '.') = '\0';
		pobRootNode->GetElement().AddAttribute("base", acParentTypeName);
	}
	pobRootNode->GetElement().AddAttribute("name", GetTypeName());
	pobRootNode->GetElement().AddAttribute("uuid", GetUID());

	// Write out Template name
	parTreeNode* pobTemplateNameNode = rage_new parTreeNode();
	pobTemplateNameNode->GetElement().SetName("TemplateFilename");
	pobTemplateNameNode->SetData((const char*)pobTemplate->GetTemplatePathAndFilename(), StringLength(pobTemplate->GetTemplatePathAndFilename()));
	pobTemplateNameNode->AppendAsChildOf(pobRootNode);

	// Write out Type name
	if(m_pobParentType)
	{
		parTreeNode* pobTypeNameNode = rage_new parTreeNode();
		pobTypeNameNode->GetElement().SetName("TypeFilename");
		pobTypeNameNode->SetData((const char*)m_pobParentType->GetTypePathAndFilename(), StringLength(m_pobParentType->GetTypePathAndFilename()));
		pobTypeNameNode->AppendAsChildOf(pobRootNode);
	}

	// Write out shader members
	parTreeNode* pobShaderMembersNode = rage_new parTreeNode();
	pobShaderMembersNode->GetElement().SetName("ShaderMembers");
	pobShaderMembersNode->GetElement().AddAttribute("type", "ShaderMembers");
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s/ShaderMembers", pobTemplate->GetTemplateName());
	pobShaderMembersNode->GetElement().AddAttribute("template", acTempBuffer);
	pobShaderMembersNode->AppendAsChildOf(pobRootNode);
	for(int i=0; i<m_apShaderParamValues.GetCount(); i++)
	{
		Assert(m_apShaderParamValues[i]->GetParamDescription());
		if (m_apShaderParamValues[i]->GetParamDescription()->GetSourceOfParamValue() & shaderMaterialGeoParamDescription::GeoInstance)
		{
			m_apShaderParamValues[i]->WriteAsGeoTypeMember(pobShaderMembersNode, pobTemplate->GetTemplateName());
		}
	}

	bool bResult = false;
	fiStream * streamMtl = fiStream::Create(acGeoMtlPathAndFilename);
	if (streamMtl)
	{
		// Write it all out
		parTree obTree;
		obTree.SetRoot(pobRootNode);
		PARSER.SaveTree(streamMtl, &obTree);
		streamMtl->Close();
		bResult = true;
	}
	else
	{
		Assertf(false, "Unable to open file [%s] to save geo instance", acGeoMtlPathAndFilename);
		Errorf("Unable to open file [%s] to save geo instance", acGeoMtlPathAndFilename);
		delete pobRootNode;
	}

	return bResult;
}

const char* shaderMaterialGeoInstance::GetTemplateName() const
{
	const shaderMaterialGeoTemplate* pobTemplate = GetTemplate();//get my template
	Assertf(pobTemplate,"no valid template for {%s}", GetInstancePathAndFilename());
	return pobTemplate->GetTemplateName();
}

void shaderMaterialGeoInstance::CopyParamValuesSelectively(const shaderMaterialGeoType & obThat)
{
	//Pretty lame here ... should not have to do this.
	// Nuke the old ones that are not unique to me
	atArray<shaderMaterialGeoParamValue *> saveList;
	for(int i=0; i<m_apShaderParamValues.GetCount(); i++)
	{
		if ((m_apShaderParamValues[i]->GetSourceOfParamValue() & shaderMaterialGeoParamDescription::GeoInstance))
		{
			saveList.PushAndGrow(m_apShaderParamValues[i]);
		}
		else
		{
			delete m_apShaderParamValues[i];
		}
	}
	m_apShaderParamValues.clear();

	// Create new ones
	for(int i=0; i<obThat.GetNoOfShaderParams(); i++)
	{
		const shaderMaterialGeoParamValue * pobSourceParamValue = obThat.GetShaderParam(i);

		if (pobSourceParamValue->GetSourceOfParamValue() & shaderMaterialGeoParamDescription::GeoInstance)
		{
			continue;
		}
		
		shaderMaterialGeoParamValue * pobParamValue = NULL;
		switch(pobSourceParamValue->GetType())
		{
		/* case grcEffect::VT_INT:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueInt((const shaderMaterialGeoParamValueInt*)pobSourceParamValue);
				break;
			} */
		case grcEffect::VT_FLOAT:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueFloat((const shaderMaterialGeoParamValueFloat*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_VECTOR2:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueVector2((const shaderMaterialGeoParamValueVector2*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_VECTOR3:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueVector3((const shaderMaterialGeoParamValueVector3*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_VECTOR4:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueVector4((const shaderMaterialGeoParamValueVector4*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_TEXTURE:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueTexture((const shaderMaterialGeoParamValueTexture*)pobSourceParamValue);
				break;
			}
		/* case grcEffect::VT_BOOL:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueBool((const shaderMaterialGeoParamValueBool*)pobSourceParamValue);
				break;
			} */
		case grcEffect::VT_MATRIX34:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueMatrix34((const shaderMaterialGeoParamValueMatrix34*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_MATRIX44:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueMatrix44((const shaderMaterialGeoParamValueMatrix44*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_STRING:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueString((const shaderMaterialGeoParamValueString*)pobSourceParamValue);
				break;
			}
		default:
			AssertMsg(0 , "Invalid param type");
		}

		m_apShaderParamValues.PushAndGrow(pobParamValue);
	}

	//merge the two lists
	for(int i=0; i<saveList.GetCount(); i++)
	{
		m_apShaderParamValues.PushAndGrow(saveList[i]);
	}
}

const shaderMaterialGeoTemplate* shaderMaterialGeoInstance::GetTemplate() const
{
	const shaderMaterialGeoType* type = GetParentType();
	Assertf(type, "Some how I have a NULL Type on a loaded shaderMaterialGeoInstance");
	return type->GetTemplate();
}
