// 
// shaderMaterial/shaderMaterialGeoParamDescriptionTexture.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamDescriptionTexture.h"

#include "file/asset.h"
#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"

using namespace rage;

shaderMaterialGeoParamDescriptionTexture::shaderMaterialGeoParamDescriptionTexture() : shaderMaterialGeoParamDescription()
{
	Reset();
}

shaderMaterialGeoParamDescriptionTexture::~shaderMaterialGeoParamDescriptionTexture()
{
	Reset();
}

shaderMaterialGeoParamDescriptionTexture::shaderMaterialGeoParamDescriptionTexture(const parTreeNode*	pobGeoParamDescriptionGeoRootNode)
: shaderMaterialGeoParamDescription(pobGeoParamDescriptionGeoRootNode)
{
	m_MaterialDefaultValueUvSetIndex = -1;
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamDescriptionGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildDefaultValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "filename") == 0)
		{
			const char* pcDefaultValue = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("init", "none", NULL, 0);
			SetDefaultValue(pcDefaultValue);
			const char* pcUiName = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("displayName", "Unable to file displayName", NULL, 0);
			SetUIName(pcUiName);

			const char* pcInstantiation = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("instantiation", "Unable to file displayName", NULL, 0);
			if(strcmp(pcInstantiation, "instance") == 0)
			{
				SetValueSource(shaderMaterialGeoParamDescription::GeoInstance);
			}
			else if(strcmp(pcInstantiation, "type") == 0)
			{
				SetValueSource(shaderMaterialGeoParamDescription::GeoType);
			}
			else if(strcmp(pcInstantiation, "both") == 0)
			{
				if(GetSourceOfParamValue() == shaderMaterialGeoParamDescription::GeoType)
				{
					// Evil Hack Do nothing!!!!
				}
				else
				{
					SetValueSource(shaderMaterialGeoParamDescription::GeoInstance |shaderMaterialGeoParamDescription::GeoType);
				}
			}
			else
			{
				Assertf(false, "Unknown instantiation level (%s) for param %s", pcInstantiation, GetName());
			}
		}
		else if(strcmp(pcNodeName, "outputformat") == 0)
		{
			const char* pcDefaultValue = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("init", "none", NULL, 0);
			SetTextureOutputFormats(pcDefaultValue);
		}
		else if(strcmp(pcNodeName, "uvsetindex") == 0)
		{
			int iDefaultValue = pobChildDefaultValueNode->GetElement().FindAttributeIntValue("init", 0);
			SetUvSetIndex(iDefaultValue);
		}
		else if(strcmp(pcNodeName, "uvsetname") == 0)
		{
			const char* pcDefaultValue = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("init", "none", NULL, 0);
			SetUvSetName(pcDefaultValue);
		}
	}
}

void shaderMaterialGeoParamDescriptionTexture::Reset()
{
	shaderMaterialGeoParamDescription::Reset();
	m_sDefaultValue = "none";
}

void shaderMaterialGeoParamDescriptionTexture::Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamDescription::Copy(param, copyBase);

	m_sDefaultValue = ((shaderMaterialGeoParamDescriptionTexture *)param)->m_sDefaultValue;
}

bool shaderMaterialGeoParamDescriptionTexture::IsEqual(const shaderMaterialGeoParamDescription * param) const
{
	if (!shaderMaterialGeoParamDescription::IsEqual(param))
	{
		return false;
	}

	// If we have a material data just assume equal
	if (m_MaterialDefaultValueUvSetIndex >= 0)
	{
		return true;
	}

	return (strcmp((const char *)m_sDefaultValue, (const char *)((shaderMaterialGeoParamDescriptionTexture *)param)->m_sDefaultValue)==0);
}

bool shaderMaterialGeoParamDescriptionTexture::CleanUp()
{
	if (strlen(m_sDefaultValue)==0)
	{
		m_sDefaultValue = "null";
	}
	else
	{
		char szDefaultValue[256];
		strcpy(szDefaultValue, m_sDefaultValue);
		for (u32 i=0; i<strlen(szDefaultValue); i++)
		{
			if (szDefaultValue[i]=='\\')
			{
				szDefaultValue[i] = '/';
			}
		}
		m_sDefaultValue = szDefaultValue;
	}

	return true;
}

bool shaderMaterialGeoParamDescriptionTexture::ReadGeoParamDescriptionData(fiTokenizer & T)
{
	shaderMaterialGeoParamDescription::ReadGeoParamDescriptionData(T);

	char szToken[128];
	T.GetToken(szToken, sizeof(szToken));
	m_sDefaultValue = szToken;

	return true;
}

bool shaderMaterialGeoParamDescriptionTexture::WriteGeoParamDescriptionData(fiTokenizer & T) const
{
	shaderMaterialGeoParamDescription::WriteGeoParamDescriptionData(T);

	T.StartBlock();
	{
		ConstString sTextureName;
		GetTextureName(sTextureName);

		T.StartLine();
		T.PutStr("%s %s", GetTypeName(), sTextureName.m_String);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void shaderMaterialGeoParamDescriptionTexture::GetTextureName(ConstString & sTextureName, const char * szTextureFilename)
{
	char texName[128];
	memset(texName, 0, sizeof(texName));
	fiAssetManager::BaseName(texName, sizeof(texName), fiAssetManager::FileName(szTextureFilename));
	strcat(texName, ".dds");
	sTextureName = texName;
}

void shaderMaterialGeoParamDescriptionTexture::GetTextureName(ConstString & sTextureName) const
{
	shaderMaterialGeoParamDescriptionTexture::GetTextureName(sTextureName, m_sDefaultValue);
}

void shaderMaterialGeoParamDescriptionTexture::SetDefaultValue(const char * szDefaultValue)
{
	m_sDefaultValue = szDefaultValue;
}

void	shaderMaterialGeoParamDescriptionTexture::WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const
{
	// Textures have a lot of info, so create a geo template just for it
	parTreeNode* pobGeoTemplateNode = rage_new parTreeNode();
	pobGeoTemplateNode->GetElement().SetName("GeoTemplate");
	pobGeoTemplateNode->GetElement().AddAttribute("name", GetName());
	pobGeoTemplateNode->AppendAsChildOf(pobGeoTemplatesNode);

	parTreeNode* pobGeoTemplateMembersNode = rage_new parTreeNode();
	pobGeoTemplateMembersNode->GetElement().SetName("Members");
	pobGeoTemplateMembersNode->AppendAsChildOf(pobGeoTemplateNode);

	// Add file
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("file");
		pobNode->GetElement().AddAttribute("name", "filename");
		if((GetSourceOfParamValue() & GeoInstance) && (GetSourceOfParamValue() & GeoType))
		{
			pobNode->GetElement().AddAttribute("instantiation", "both");
		}
		else if(GetSourceOfParamValue() & GeoInstance)
		{
			pobNode->GetElement().AddAttribute("instantiation", "instance");
		}
		else
		{
			pobNode->GetElement().AddAttribute("instantiation", "type");
		}
		pobNode->GetElement().AddAttribute("init", GetDefaultValue());
		pobNode->GetElement().AddAttribute("displayName", GetUIName());
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add output formats
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("string");
		pobNode->GetElement().AddAttribute("name", "outputformat");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("init", GetTextureOutputFormats());
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add UV index
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("int");
		pobNode->GetElement().AddAttribute("name", "uvsetindex");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("init", GetUvSetIndex());
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add UV name
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("string");
		pobNode->GetElement().AddAttribute("name", "uvsetname");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("init", GetUvSetName());
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add uiwidget
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("string");
		pobNode->GetElement().AddAttribute("name", "uiwidget");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("init", GetUIWidget());
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	//// Add materialdatauvsetindex flag
	//{
	//	parTreeNode* pobNode = new parTreeNode();
	//	pobNode->GetElement().SetName("int");
	//	pobNode->GetElement().AddAttribute("name", "materialdatauvsetindex");
	//	pobNode->GetElement().AddAttribute("instantiation", "type");
	//	pobNode->GetElement().AddAttribute("init", GetMaterialDataUvSetIndex());
	//	pobNode->GetElement().AddAttribute("locked", "true");
	//	pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	//}

	// Add the template to the list of params
	// Get parent template name
	const char* pcParentsTemplateName = pobGeoTemplatesNode->GetParent()->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

	// Create param node
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName("geo");
	pobNode->GetElement().AddAttribute("name", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcParentsTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);
	pobNode->GetElement().AddAttribute("mustExist", "true");
	pobNode->GetElement().AddAttribute("canDerive", "false");
	pobNode->AppendAsChildOf(pobParent);
}

void	shaderMaterialGeoParamDescriptionTexture::WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open texture
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add filename
	parTreeNode* pobFileNode = rage_new parTreeNode();
	pobFileNode->GetElement().SetName("filename");
	pobFileNode->SetData(GetDefaultValue(), StringLength(GetDefaultValue()));
	pobFileNode->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}

