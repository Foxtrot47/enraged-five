// 
// shaderMaterial/shaderMaterialGeoParamDescriptionMatrix44.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"
#include "vector/matrix44.h"

#include "shaderMaterialGeoParamDescriptionMatrix44.h"

using namespace rage;

shaderMaterialGeoParamDescriptionMatrix44::shaderMaterialGeoParamDescriptionMatrix44() : shaderMaterialGeoParamDescription()
{
	Reset();
}

shaderMaterialGeoParamDescriptionMatrix44::~shaderMaterialGeoParamDescriptionMatrix44()
{
	Reset();
}

void shaderMaterialGeoParamDescriptionMatrix44::Reset()
{
	shaderMaterialGeoParamDescription::Reset();


	m_mDefaultValueA = M44_IDENTITY.a;
	m_mDefaultValueB = M44_IDENTITY.b;
	m_mDefaultValueC = M44_IDENTITY.c;
	m_mDefaultValueD = M44_IDENTITY.d;
}

void shaderMaterialGeoParamDescriptionMatrix44::Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamDescription::Copy(param, copyBase);

	m_mDefaultValueA = ((shaderMaterialGeoParamDescriptionMatrix44 *)param)->m_mDefaultValueA;
	m_mDefaultValueB = ((shaderMaterialGeoParamDescriptionMatrix44 *)param)->m_mDefaultValueB;
	m_mDefaultValueC = ((shaderMaterialGeoParamDescriptionMatrix44 *)param)->m_mDefaultValueC;
	m_mDefaultValueD = ((shaderMaterialGeoParamDescriptionMatrix44 *)param)->m_mDefaultValueD;
}

bool shaderMaterialGeoParamDescriptionMatrix44::IsEqual(const shaderMaterialGeoParamDescription * param) const
{
	if (!shaderMaterialGeoParamDescription::IsEqual(param))
	{
		return false;
	}

	// If we have a material data just assume equal
	if (m_MaterialDefaultValueUvSetIndex >= 0)
	{
		return true;
	}

	if (m_mDefaultValueA != ((shaderMaterialGeoParamDescriptionMatrix44 *)param)->m_mDefaultValueA)
	{
		return false;
	}

	if (m_mDefaultValueB != ((shaderMaterialGeoParamDescriptionMatrix44 *)param)->m_mDefaultValueB)
	{
		return false;
	}

	if (m_mDefaultValueC != ((shaderMaterialGeoParamDescriptionMatrix44 *)param)->m_mDefaultValueC)
	{
		return false;
	}

	if (m_mDefaultValueD != ((shaderMaterialGeoParamDescriptionMatrix44 *)param)->m_mDefaultValueD)
	{
		return false;
	}

	return true;
}

bool shaderMaterialGeoParamDescriptionMatrix44::ReadGeoParamDescriptionData(fiTokenizer & T)
{
	shaderMaterialGeoParamDescription::ReadGeoParamDescriptionData(T);

	T.GetVector(m_mDefaultValueA);
	T.GetVector(m_mDefaultValueB);
	T.GetVector(m_mDefaultValueC);
	T.GetVector(m_mDefaultValueD);

	return true;
}

bool shaderMaterialGeoParamDescriptionMatrix44::WriteGeoParamDescriptionData(fiTokenizer & T) const
{
	shaderMaterialGeoParamDescription::WriteGeoParamDescriptionData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", GetTypeName(),
				  m_mDefaultValueA.x, m_mDefaultValueA.y, m_mDefaultValueA.z, m_mDefaultValueA.w,
				  m_mDefaultValueB.x, m_mDefaultValueB.y, m_mDefaultValueB.z, m_mDefaultValueB.w,
				  m_mDefaultValueC.x, m_mDefaultValueC.y, m_mDefaultValueC.z, m_mDefaultValueC.w,
				  m_mDefaultValueD.x, m_mDefaultValueD.y, m_mDefaultValueD.z, m_mDefaultValueD.w);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}


void	shaderMaterialGeoParamDescriptionMatrix44::WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* /*pobGeoTemplatesNode*/) const
{
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName("Vector4");
	pobNode->GetElement().AddAttribute("name", GetName());
	if((GetSourceOfParamValue() & GeoInstance) && (GetSourceOfParamValue() & GeoType))
	{
		pobNode->GetElement().AddAttribute("instantiation", "both");
	}
	else if(GetSourceOfParamValue() & GeoInstance)
	{
		pobNode->GetElement().AddAttribute("instantiation", "instance");
	}
	else
	{
		pobNode->GetElement().AddAttribute("instantiation", "type");
	}
	pobNode->GetElement().AddAttribute("displayName", GetUIName());

	parTreeNode* pobANode = rage_new parTreeNode();
	pobANode->GetElement().SetName("a");
	pobANode->GetElement().AddAttribute("x", GetDefaultValue().a.x);
	pobANode->GetElement().AddAttribute("y", GetDefaultValue().a.y);
	pobANode->GetElement().AddAttribute("z", GetDefaultValue().a.z);
	pobANode->GetElement().AddAttribute("w", GetDefaultValue().a.w);
	pobANode->AppendAsChildOf(pobNode);

	parTreeNode* pobBNode = rage_new parTreeNode();
	pobBNode->GetElement().SetName("b");
	pobBNode->GetElement().AddAttribute("x", GetDefaultValue().b.x);
	pobBNode->GetElement().AddAttribute("y", GetDefaultValue().b.y);
	pobBNode->GetElement().AddAttribute("z", GetDefaultValue().b.z);
	pobBNode->GetElement().AddAttribute("w", GetDefaultValue().b.w);
	pobBNode->AppendAsChildOf(pobNode);

	parTreeNode* pobCNode = rage_new parTreeNode();
	pobCNode->GetElement().SetName("c");
	pobCNode->GetElement().AddAttribute("x", GetDefaultValue().c.x);
	pobCNode->GetElement().AddAttribute("y", GetDefaultValue().c.y);
	pobCNode->GetElement().AddAttribute("z", GetDefaultValue().c.z);
	pobCNode->GetElement().AddAttribute("w", GetDefaultValue().c.w);
	pobCNode->AppendAsChildOf(pobNode);

	parTreeNode* pobDNode = rage_new parTreeNode();
	pobDNode->GetElement().SetName("d");
	pobDNode->GetElement().AddAttribute("x", GetDefaultValue().d.x);
	pobDNode->GetElement().AddAttribute("y", GetDefaultValue().d.y);
	pobDNode->GetElement().AddAttribute("z", GetDefaultValue().d.z);
	pobDNode->GetElement().AddAttribute("w", GetDefaultValue().d.w);
	pobDNode->AppendAsChildOf(pobNode);

	pobNode->AppendAsChildOf(pobParent);
}

void	shaderMaterialGeoParamDescriptionMatrix44::WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* /*pcTemplateName*/) const
{
	Assertf(0, "This won't write data correctly!");
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("content", "vector4_array");

	char acTempBuffer[255];
	formatf(acTempBuffer, sizeof(acTempBuffer), "  \t\t\t\t\t\t%f\t%f\t%f\t%f\n", GetDefaultValue().a.x, GetDefaultValue().a.y, GetDefaultValue().a.z, GetDefaultValue().a.w );
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s\t\t\t\t\t\t%f\t%f\t%f\t%f\n", acTempBuffer, GetDefaultValue().b.x, GetDefaultValue().b.y, GetDefaultValue().b.z, GetDefaultValue().b.w );
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s\t\t\t\t\t\t%f\t%f\t%f\t%f\n", acTempBuffer, GetDefaultValue().c.x, GetDefaultValue().c.y, GetDefaultValue().c.z, GetDefaultValue().c.w );
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s\t\t\t\t\t\t%f\t%f\t%f\t%f\n", acTempBuffer, GetDefaultValue().d.x, GetDefaultValue().d.y, GetDefaultValue().d.z, GetDefaultValue().d.w );

	pobNode->SetData(acTempBuffer, StringLength(acTempBuffer));

	pobNode->AppendAsChildOf(pobParent);
}

