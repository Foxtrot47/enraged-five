// 
// shaderMaterial/shaderMaterialGeoParamValueInt.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if 0

#include "file/token.h"
#include "grmodel/shader.h"
#include "vector/vector2.h"

#include "shaderMaterialGeoParamValueInt.h"

using namespace rage;

shaderMaterialGeoParamValueInt::shaderMaterialGeoParamValueInt(const shaderMaterialGeoParamDescription*	pobParamDescription) : shaderMaterialGeoParamValue(pobParamDescription)
{
	Reset();
}

shaderMaterialGeoParamValueInt::shaderMaterialGeoParamValueInt(const shaderMaterialGeoParamValueInt*	pobSourceParam)
:shaderMaterialGeoParamValue(pobSourceParam)
{
	Copy(pobSourceParam);
}

shaderMaterialGeoParamValueInt::~shaderMaterialGeoParamValueInt()
{
	Reset();
}

shaderMaterialGeoParamValueInt::shaderMaterialGeoParamValueInt(const parTreeNode*	pobGeoParamValueGeoRootNode)
: shaderMaterialGeoParamValue(pobGeoParamValueGeoRootNode)
{
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamValueGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "intvalue") == 0)
		{
			int iValue = pobChildValueNode->GetElement().FindAttributeIntValue("init", 0);
			SetValue(iValue);
		}
	}
}

void shaderMaterialGeoParamValueInt::Reset()
{
	shaderMaterialGeoParamValue::Reset();

	m_nValue = 0;
}

void shaderMaterialGeoParamValueInt::Copy(const shaderMaterialGeoParamValueInt * param)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamValue::Copy(param);

	m_nValue = ((shaderMaterialGeoParamValueInt *)param)->m_nValue;
}

bool shaderMaterialGeoParamValueInt::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!shaderMaterialGeoParamValue::IsEqual(param))
	{
		return false;
	}

	return m_nValue == ((shaderMaterialGeoParamValueInt *)param)->m_nValue;
}

bool shaderMaterialGeoParamValueInt::Update(const int n)
{
	if (m_nValue != n)
	{
		m_nValue = n;
		CleanUp();
		return true;
	}
	
	return false;
}

bool shaderMaterialGeoParamValueInt::Update(const shaderMaterialGeoParamValue * param)
{
	Assert(GetType() == param->GetType());

	return Update(((shaderMaterialGeoParamValueInt *)param)->m_nValue);
}

void shaderMaterialGeoParamValueInt::SetDefaultValue(grcEffect & effect, grcEffectVar & var)
{
	effect.GetVar(var, m_nValue);
}

void shaderMaterialGeoParamValueInt::SetMaterialValue(Vector2 & vValue)
{
	vValue.x = (float)m_nValue;
	vValue.y = 0.0f;
}

bool shaderMaterialGeoParamValueInt::CleanUp()
{
	if (m_nValue < (int)(GetParamDescription()->GetUIMin()+0.5f))
	{
		m_nValue = (int)(GetParamDescription()->GetUIMin()+0.5f);
	}

	if (m_nValue > (int)(GetParamDescription()->GetUIMax()+0.5f))
	{
		m_nValue = (int)(GetParamDescription()->GetUIMax()+0.5f);
	}

	return true;
}

bool shaderMaterialGeoParamValueInt::ReadGeoParamValueData(fiTokenizer & T)
{
	shaderMaterialGeoParamValue::ReadGeoParamValueData(T);

	m_nValue = T.GetInt();

	return true;
}

bool shaderMaterialGeoParamValueInt::WriteGeoParamValueData(fiTokenizer & T) const
{
	shaderMaterialGeoParamValue::WriteGeoParamValueData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %d", GetTypeName(), m_nValue);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void	shaderMaterialGeoParamValueInt::WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open string
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add intvalue
	parTreeNode* pobIntNode = rage_new parTreeNode();
	pobIntNode->GetElement().SetName("intvalue");
	pobIntNode->GetElement().AddAttribute("value", GetValue());
	pobIntNode->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}

void shaderMaterialGeoParamValueInt::CopyValueIntoVector2(Vector2 & vData) const
{
	vData.x = (float)GetValue();
	vData.y = 0.0f;
}

#endif
