// 
// shaderMaterial/shaderMaterialGeoParamValueVector4.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamValueVector4.h"

#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"

using namespace rage;

shaderMaterialGeoParamValueVector4::shaderMaterialGeoParamValueVector4(const shaderMaterialGeoParamDescription*	pobParamDescription) : shaderMaterialGeoParamValue(pobParamDescription)
{
	Reset();
}

shaderMaterialGeoParamValueVector4::shaderMaterialGeoParamValueVector4(const shaderMaterialGeoParamValueVector4*	pobSourceParam)
:shaderMaterialGeoParamValue(pobSourceParam)
{
	Copy(pobSourceParam);
}

shaderMaterialGeoParamValueVector4::~shaderMaterialGeoParamValueVector4()
{
	Reset();
}

shaderMaterialGeoParamValueVector4::shaderMaterialGeoParamValueVector4(const parTreeNode*	pobGeoParamValueGeoRootNode)
: shaderMaterialGeoParamValue(pobGeoParamValueGeoRootNode)
{
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamValueGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "Vector4value") == 0)
		{
			Vector4 obInitVector;
			parTreeNode* pobInitNode = pobChildValueNode->FindChildWithName("init");
			Assertf(pobInitNode, "Unable to find <init> child tag in geo template file");

			obInitVector.x = pobInitNode->GetElement().FindAttributeFloatValue("x", 0.0f);
			obInitVector.y = pobInitNode->GetElement().FindAttributeFloatValue("y", 0.0f);
			obInitVector.z = pobInitNode->GetElement().FindAttributeFloatValue("z", 0.0f);
			obInitVector.w = pobInitNode->GetElement().FindAttributeFloatValue("w", 0.0f);
			SetValue(obInitVector);
		}
	}
	}

void shaderMaterialGeoParamValueVector4::Reset()
{
	shaderMaterialGeoParamValue::Reset();

	m_vValue.Zero();
}

void shaderMaterialGeoParamValueVector4::Copy(const shaderMaterialGeoParamValueVector4 * param)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamValue::Copy(param);

	m_vValue = ((shaderMaterialGeoParamValueVector4 *)param)->m_vValue;
}

bool shaderMaterialGeoParamValueVector4::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!shaderMaterialGeoParamValue::IsEqual(param))
	{
		return false;
	}

	// If we have a material data just assume equal
	return m_vValue == ((shaderMaterialGeoParamValueVector4 *)param)->m_vValue;
}

bool shaderMaterialGeoParamValueVector4::Update(const char * param, const float f0, const float f1, const float f2)
{
	int len = (int) strlen(param);

	if (stricmp(GetParamDescription()->GetUIWidget(), "color")==0)
	{
		if (param[len-1] == 'R')
		{
			if (m_vValue.x != f0)
			{
				m_vValue.x = f0;
				return true;
			}
		}
		else if (param[len-1] == 'G')
		{
			if (m_vValue.y != f0)
			{
				m_vValue.y = f0;
				return true;
			}
		}
		else if (param[len-1] == 'B')
		{
			if (m_vValue.z != f0)
			{
				m_vValue.z = f0;
				return true;
			}
		}
		else if (param[len-5] == 'A' &&
			     param[len-4] == 'l' &&
				 param[len-3] == 'p' &&
				 param[len-2] == 'h' &&
				 param[len-1] == 'a')
		{
			if (m_vValue.w != f0)
			{
				m_vValue.w = f0;
				return true;
			}
		}
		else
		{
			if (m_vValue.x != f0 ||
				m_vValue.y != f1 ||
				m_vValue.z != f2)
			{
				m_vValue.x = f0;
				m_vValue.y = f1;
				m_vValue.z = f2;
				return true;
			}
		}
	}
	else
	{
		if (param[len-1] == 'X')
		{
			if (m_vValue.x != f0)
			{
				m_vValue.x = f0;
				return true;
			}
		}
		else if (param[len-1] == 'Y')
		{
			if (m_vValue.y != f0)
			{
				m_vValue.y = f0;
				return true;
			}
		}
		else if (param[len-1] == 'Z')
		{
			if (m_vValue.z != f0)
			{
				m_vValue.z = f0;
				return true;
			}
		}
		else if (param[len-1] == 'W')
		{
			if (m_vValue.w != f0)
			{
				m_vValue.w = f0;
				return true;
			}
		}
	}

	return false;
}


bool shaderMaterialGeoParamValueVector4::Update(const char * param, const float f0)
{
	int len = (int) strlen(param);

	if (param[len-1] == 'R' || param[len-1] == 'X')
	{
		if (m_vValue.x != f0)
		{
			m_vValue.x = f0;
			return true;
		}
		
	}
	else if (param[len-1] == 'G' || param[len-1] == 'Y')
	{
		if (m_vValue.y != f0)
		{
			m_vValue.y = f0;
			return true;
		}
	}
	else if (param[len-1] == 'B' || param[len-1] == 'Z')
	{
		if (m_vValue.z != f0)
		{
			m_vValue.z = f0;
			return true;
		}
	}
	else if (param[len-1] == 'A' || param[len-1] == 'W')
	{
		if (m_vValue.w != f0)
		{
			m_vValue.w = f0;
			return true;
		}
	}
	return false;
}

bool shaderMaterialGeoParamValueVector4::Update(const shaderMaterialGeoParamValue * param)
{
	Assert(GetType() == param->GetType());

	m_vValue = ((shaderMaterialGeoParamValueVector4 *)param)->m_vValue;

	return true;
}

void shaderMaterialGeoParamValueVector4::SetDefaultValue(grcEffect & effect, grcEffectVar & var)
{
	effect.GetVar(var, m_vValue);
}

bool shaderMaterialGeoParamValueVector4::ReadGeoParamValueData(fiTokenizer & T)
{
	shaderMaterialGeoParamValue::ReadGeoParamValueData(T);

	T.GetVector(m_vValue);

	return true;
}

bool shaderMaterialGeoParamValueVector4::WriteGeoParamValueData(fiTokenizer & T) const
{
	shaderMaterialGeoParamValue::WriteGeoParamValueData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %f %f %f %f", GetTypeName(), m_vValue.x, m_vValue.y, m_vValue.z, m_vValue.w);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void	shaderMaterialGeoParamValueVector4::WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open string
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add Vector4value
	parTreeNode* pobVector4Node = rage_new parTreeNode();
	pobVector4Node->GetElement().SetName("Vector4value");
	pobVector4Node->GetElement().AddAttribute("x", GetValue().x);
	pobVector4Node->GetElement().AddAttribute("y", GetValue().y);
	pobVector4Node->GetElement().AddAttribute("z", GetValue().z);
	pobVector4Node->GetElement().AddAttribute("w", GetValue().w);
	pobVector4Node->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}


bool shaderMaterialGeoParamValueVector4::Update(const Vector4& obValue)
{
	if(m_vValue == obValue)
	{
		return false;
	}
	m_vValue = obValue;
	return true;
}

