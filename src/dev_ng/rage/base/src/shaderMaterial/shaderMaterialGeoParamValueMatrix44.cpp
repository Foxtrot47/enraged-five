// 
// shaderMaterial/shaderMaterialGeoParamValueMatrix44.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamValueMatrix44.h"

#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"
#include "vector/matrix44.h"

using namespace rage;

shaderMaterialGeoParamValueMatrix44::shaderMaterialGeoParamValueMatrix44(const shaderMaterialGeoParamDescription*	pobParamDescription) : shaderMaterialGeoParamValue(pobParamDescription)
{
	Reset();
}

shaderMaterialGeoParamValueMatrix44::shaderMaterialGeoParamValueMatrix44(const shaderMaterialGeoParamValueMatrix44*	pobSourceParam)
:shaderMaterialGeoParamValue(pobSourceParam)
{
	Copy(pobSourceParam);
}

shaderMaterialGeoParamValueMatrix44::~shaderMaterialGeoParamValueMatrix44()
{
	Reset();
}

void shaderMaterialGeoParamValueMatrix44::Reset()
{
	shaderMaterialGeoParamValue::Reset();


	m_mValueA = M44_IDENTITY.a;
	m_mValueB = M44_IDENTITY.b;
	m_mValueC = M44_IDENTITY.c;
	m_mValueD = M44_IDENTITY.d;
}

void shaderMaterialGeoParamValueMatrix44::Copy(const shaderMaterialGeoParamValueMatrix44 * param)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamValue::Copy(param);

	m_mValueA = ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueA;
	m_mValueB = ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueB;
	m_mValueC = ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueC;
	m_mValueD = ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueD;
}

bool shaderMaterialGeoParamValueMatrix44::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!shaderMaterialGeoParamValue::IsEqual(param))
	{
		return false;
	}

	if (m_mValueA != ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueA)
	{
		return false;
	}

	if (m_mValueB != ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueB)
	{
		return false;
	}

	if (m_mValueC != ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueC)
	{
		return false;
	}

	if (m_mValueD != ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueD)
	{
		return false;
	}

	return true;
}

bool shaderMaterialGeoParamValueMatrix44::UpdateVector(const char * param, const float f, const int len, Vector4 & v)
{
	if (param[len-1] == 'X')
	{
		if (v.x != f)
		{
			v.x = f;
			return true;
		}
	}
	else if (param[len-1] == 'Y')
	{
		if (v.y != f)
		{
			v.y = f;
			return true;
		}
	}
	else if (param[len-1] == 'Z')
	{
		if (v.z != f)
		{
			v.z = f;
			return true;
		}
	}
	else if (param[len-1] == 'W')
	{
		if (v.w != f)
		{
			v.w = f;
			return true;
		}
	}

	return false;
}

bool shaderMaterialGeoParamValueMatrix44::Update(const char * param, const float f)
{
	int len = (int) strlen(param);
	if (param[len-2] == 'A')
	{
		return UpdateVector(param, f, len, m_mValueA);
	}
	else if (param[len-2] == 'B')
	{
		return UpdateVector(param, f, len, m_mValueB);
	}
	else if (param[len-2] == 'C')
	{
		return UpdateVector(param, f, len, m_mValueC);
	}
	else if (param[len-2] == 'D')
	{
		return UpdateVector(param, f, len, m_mValueD);
	}

	return false;
}

bool shaderMaterialGeoParamValueMatrix44::Update(const shaderMaterialGeoParamValue * param)
{
	Assert(GetType() == param->GetType());

	m_mValueA = ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueA;
	m_mValueB = ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueB;
	m_mValueC = ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueC;
	m_mValueD = ((shaderMaterialGeoParamValueMatrix44 *)param)->m_mValueD;

	return true;
}

void shaderMaterialGeoParamValueMatrix44::SetDefaultValue(grcEffect & effect, grcEffectVar & var)
{
	Matrix44 m44;
	effect.GetVar(var, m44);

	m_mValueA = m44.a;
	m_mValueB = m44.b;
	m_mValueC = m44.c;
	m_mValueD = m44.d;
}

bool shaderMaterialGeoParamValueMatrix44::ReadGeoParamValueData(fiTokenizer & T)
{
	shaderMaterialGeoParamValue::ReadGeoParamValueData(T);

	T.GetVector(m_mValueA);
	T.GetVector(m_mValueB);
	T.GetVector(m_mValueC);
	T.GetVector(m_mValueD);

	return true;
}

bool shaderMaterialGeoParamValueMatrix44::WriteGeoParamValueData(fiTokenizer & T) const
{
	shaderMaterialGeoParamValue::WriteGeoParamValueData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", GetTypeName(),
				  m_mValueA.x, m_mValueA.y, m_mValueA.z, m_mValueA.w,
				  m_mValueB.x, m_mValueB.y, m_mValueB.z, m_mValueB.w,
				  m_mValueC.x, m_mValueC.y, m_mValueC.z, m_mValueC.w,
				  m_mValueD.x, m_mValueD.y, m_mValueD.z, m_mValueD.w);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void	shaderMaterialGeoParamValueMatrix44::WriteAsGeoTypeMember(parTreeNode* pobParent, const char* /*pcTemplateName*/) const
{
	Assertf(0, "This won't write data correctly!");
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("content", "vector4_array");

	char acTempBuffer[255];
	formatf(acTempBuffer, sizeof(acTempBuffer), "  \t\t\t\t\t\t%f\t%f\t%f\t%f\n", GetValue().a.x, GetValue().a.y, GetValue().a.z, GetValue().a.w );
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s\t\t\t\t\t\t%f\t%f\t%f\t%f\n", acTempBuffer, GetValue().b.x, GetValue().b.y, GetValue().b.z, GetValue().b.w );
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s\t\t\t\t\t\t%f\t%f\t%f\t%f\n", acTempBuffer, GetValue().c.x, GetValue().c.y, GetValue().c.z, GetValue().c.w );
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s\t\t\t\t\t\t%f\t%f\t%f\t%f\n", acTempBuffer, GetValue().d.x, GetValue().d.y, GetValue().d.z, GetValue().d.w );

	pobNode->SetData(acTempBuffer, StringLength(acTempBuffer));

	pobNode->AppendAsChildOf(pobParent);
}

bool shaderMaterialGeoParamValueMatrix44::Update(const Matrix44& m)
{
	if((m_mValueA == m.a) && ( m_mValueB == m.b) && ( m_mValueC == m.c) && ( m_mValueD == m.d))
	{
		// Same as current value
		return false;
	}
	m_mValueA = m.a; m_mValueB = m.b; m_mValueC = m.c; m_mValueD = m.d;
	return true;
}

