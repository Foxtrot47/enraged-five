// 
// shaderMaterial/shaderMaterialGeoParamValueVector3.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_VECTOR3_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_VECTOR3_H

#include "parser/macros.h"
#include "vector/vector3.h"

#include "shaderMaterialGeoParamValue.h"

namespace rage {

class shaderMaterialGeoParamValueVector3 : public shaderMaterialGeoParamValue
{
public:

	shaderMaterialGeoParamValueVector3(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValueVector3(const shaderMaterialGeoParamValueVector3*	pobSourceParam);
	virtual ~shaderMaterialGeoParamValueVector3();

	shaderMaterialGeoParamValueVector3(const parTreeNode*	pobGeoParamValueGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_VECTOR3;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_VECTOR3);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamValueVector3 * param);
	bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	bool Update(const shaderMaterialGeoParamValue * param);
	bool Update(const char * param, const float f0, const float f1, const float f2);
	bool Update(const char * param, const float f);
	bool Update(const Vector3& obValue);

	void SetDefaultValue(grcEffect & effect, grcEffectVar & var);

	bool ReadGeoParamValueData(fiTokenizer & T);
	bool WriteGeoParamValueData(fiTokenizer & T) const;

	const Vector3 & GetValue() const {return m_vValue;}
	void SetValue(const Vector3& vValue) {m_vValue.Set(vValue);}

	virtual void	WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// Value for this param
	Vector3 m_vValue;
};

} // end namespace rage

#endif
