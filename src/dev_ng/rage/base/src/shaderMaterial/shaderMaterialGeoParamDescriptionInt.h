// 
// shaderMaterial/shaderMaterialGeoParamDescriptionInt.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_INT_H
#define SHADER_MATERIAL_GEO_PARAM_INT_H

#include "parser/manager.h"
#include "shaderMaterialGeoParamDescription.h"

namespace rage {

class shaderMaterialGeoParamDescriptionInt : public shaderMaterialGeoParamDescription
{
public:

	shaderMaterialGeoParamDescriptionInt();
	virtual ~shaderMaterialGeoParamDescriptionInt();

	shaderMaterialGeoParamDescriptionInt(const parTreeNode*	pobGeoParamDescriptionGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_INT;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_INT);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase=true);
	bool IsEqual(const shaderMaterialGeoParamDescription * param) const;

	bool CleanUp();

	bool ReadGeoParamDescriptionData(fiTokenizer & T);
	bool WriteGeoParamDescriptionData(fiTokenizer & T) const;

	int GetDefaultValue() const {return m_nDefaultValue;}
	void SetDefaultValue(const int& nDefaultValue) {m_nDefaultValue = nDefaultValue;}

	virtual void	WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const;
	virtual void	WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// DefaultValue for this param
	int m_nDefaultValue;
};

} // end namespace rage

#endif
