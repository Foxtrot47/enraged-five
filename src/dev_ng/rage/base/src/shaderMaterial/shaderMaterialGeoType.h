// 
// shaderMaterialGeoType/shaderMaterialGeoType.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef SHADER_MATERIAL_GEO_TYPE_H
#define SHADER_MATERIAL_GEO_TYPE_H

#include "parser/macros.h"
#include "string/string.h"
#include "shaderMaterialGeoType.h"
#include "shaderMaterialGeoParamValue.h"

namespace rage {

	class	fiTokenizer;
	class	grmShaderVar;

	class shaderMaterialGeoType
	{
	public:

		shaderMaterialGeoType();
		shaderMaterialGeoType(const shaderMaterialGeoType & /*that*/);	// Copy constructor
		virtual ~shaderMaterialGeoType();

		shaderMaterialGeoType & operator=(const shaderMaterialGeoType & rhs);	// Assignment operator
		bool operator==(const shaderMaterialGeoType &that) const {return IsEqual(that);}
		bool IsEqual(const shaderMaterialGeoType &that) const;

		bool InitialiseFromTemplate(const shaderMaterialGeoTemplate* pobShaderMaterialGeoTemplate);
		virtual bool ConstructFromParentType(const shaderMaterialGeoType* pobParentShaderMaterialGeoType);
		bool ChangeParentType(const shaderMaterialGeoType* pobNewParentShaderMaterialGeoType);
		bool LoadFromGeoType(const char * szFilename);
		virtual bool SaveTo(const char * szFilename) const;
		
		//sets the value of the grmShaderVar from the stored data
		bool SetShaderVariableValue(grmShaderVar & var) const;
		//Sets the value of the stored data from grmShaderVar
		bool UpdateShaderVariable(const grmShaderVar & var);

		// Accessors
		const char* GetTypeFilename() const;
		const char* GetTypePathAndFilename() const;
		const char* GetTypeName() const;
		const char* GetParentTypeFilename() const;
		const char* GetParentTypePathAndFilename() const;
		const char* GetParentTypeName() const;

		const char* GetUID() const;

		const shaderMaterialGeoType* GetParentType() const;
		virtual const shaderMaterialGeoTemplate* GetTemplate() const;

		// Param accessors
		int GetNoOfParams() const {return (GetNoOfShaderParams());}
		int GetNoOfShaderParams() const {return (m_apShaderParamValues.GetCount());}

		const shaderMaterialGeoParamValue * GetParam(const char* pcParamName) const;
		const shaderMaterialGeoParamValue * GetParamByUIName(const char* pcParamName) const;
		const shaderMaterialGeoParamValue * GetParam(int iParamNo) const;
		const shaderMaterialGeoParamValue * GetShaderParam(const char* pcParamName) const;
		const shaderMaterialGeoParamValue * GetShaderParamByUIName(const char* pcParamName) const;
		const shaderMaterialGeoParamValue * GetShaderParam(int iParamNo) const;
		
		shaderMaterialGeoParamValue * GetParam(const char* pcParamName);
		shaderMaterialGeoParamValue * GetParamByUIName(const char* pcParamName);
		shaderMaterialGeoParamValue * GetParam(int iParamNo);
		shaderMaterialGeoParamValue * GetShaderParam(const char* pcParamName);
		shaderMaterialGeoParamValue * GetShaderParamByUIName(const char* pcParamName);
		shaderMaterialGeoParamValue * GetShaderParam(int iParamNo);
		
		int GetUvSetCount() const;

		virtual void CopyParamValuesSelectively(const shaderMaterialGeoType & obThat) { CopyParamValues(obThat); }

	protected:
		bool	SaveToGeoType(const char * szFilename) const;
		bool	m_bInstance;

		// Loading and saving
		void AddParamValuesFromParamDescriptions(const atArray <shaderMaterialGeoParamDescription *>*	papParamDescriptions, atArray <shaderMaterialGeoParamValue *>*	papParamValues);
		bool LoadParameterFromGeoType(parTreeNode*	pobMemberNode, shaderMaterialGeoParamValue* pobParam);
		bool LoadFromGeoType(fiStream * s);
		bool SaveToGeoType(fiStream * s) const;

		void SetParentTypePathAndFilename(const char* pcParentTypePathAndFilename);
		void SetTypePathAndFilename(const char* pcTypePathAndFilename);
		
		void Copy(const shaderMaterialGeoType & obThat);
		void CopyParamValues(const shaderMaterialGeoType & obThat);

		void AssignNewUID() const;
		void SetUIDToString(const char* pcDebugText) const
		{
			m_sGeoTypeUID = pcDebugText;
		}

		// Useful info
		ConstString m_strParentTypePathAndFilename;
		ConstString m_strTypePathAndFilename;
		ConstString m_strParentTypeName;
		ConstString m_strTypeName;
		atArray <shaderMaterialGeoParamValue *>	m_apShaderParamValues;
		const shaderMaterialGeoTemplate*		m_pobTemplate;
		const shaderMaterialGeoType*			m_pobParentType;
		mutable	ConstString						m_sGeoTypeUID;
	};

} // end namespace rage

#endif
