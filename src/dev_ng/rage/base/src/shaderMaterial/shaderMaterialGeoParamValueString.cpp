// 
// shaderMaterial/shaderMaterialGeoParamValueString.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamValueString.h"

#include "file/asset.h"
#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"

using namespace rage;

shaderMaterialGeoParamValueString::shaderMaterialGeoParamValueString(const shaderMaterialGeoParamDescription*	pobParamDescription) : shaderMaterialGeoParamValue(pobParamDescription)
{
	Reset();
}

shaderMaterialGeoParamValueString::shaderMaterialGeoParamValueString(const shaderMaterialGeoParamValueString*	pobSourceParam)
:shaderMaterialGeoParamValue(pobSourceParam)
{
	Copy(pobSourceParam);
}

shaderMaterialGeoParamValueString::~shaderMaterialGeoParamValueString()
{
	Reset();
}

shaderMaterialGeoParamValueString::shaderMaterialGeoParamValueString(const parTreeNode*	pobGeoParamValueGeoRootNode)
: shaderMaterialGeoParamValue(pobGeoParamValueGeoRootNode)
{
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamValueGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "stringcontents") == 0)
		{
			const char* pcValue = pobChildValueNode->GetElement().FindAttributeStringValue("init", "none", NULL, 0);
			SetValue(pcValue);
		}
	}
}

void shaderMaterialGeoParamValueString::Reset()
{
	shaderMaterialGeoParamValue::Reset();

	m_sValue = "default";
}

void shaderMaterialGeoParamValueString::Copy(const shaderMaterialGeoParamValueString * param)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamValue::Copy(param);

	m_sValue = ((shaderMaterialGeoParamValueString *)param)->m_sValue;
}

bool shaderMaterialGeoParamValueString::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!shaderMaterialGeoParamValue::IsEqual(param))
	{
		return false;
	}

	return (strcmp((const char *)m_sValue, (const char *)((shaderMaterialGeoParamValueString *)param)->m_sValue)==0);
}

bool shaderMaterialGeoParamValueString::Update(const char * data)
{
	if (!m_sValue || strcmp((const char *)m_sValue, data)!=0)
	{
		m_sValue = data;
		return true;
	}

	return false;
}

bool shaderMaterialGeoParamValueString::Update(const shaderMaterialGeoParamValue * param)
{
	Assert(GetType() == param->GetType());

	return Update(((shaderMaterialGeoParamValueString *)param)->m_sValue);
}

void shaderMaterialGeoParamValueString::SetDefaultValue(grcEffect & , grcEffectVar & )
{
	m_sValue = "default";
}

bool shaderMaterialGeoParamValueString::CleanUp()
{
	if (strlen(m_sValue)==0)
	{
		m_sValue = "null";
	}

	return true;
}

bool shaderMaterialGeoParamValueString::ReadGeoParamValueData(fiTokenizer & T)
{
	shaderMaterialGeoParamValue::ReadGeoParamValueData(T);

	char szToken[128];
	T.GetToken(szToken, sizeof(szToken));
	m_sValue = szToken;

	return true;
}

bool shaderMaterialGeoParamValueString::WriteGeoParamValueData(fiTokenizer & T) const
{
	shaderMaterialGeoParamValue::WriteGeoParamValueData(T);

	T.StartBlock();
	{	
		T.StartLine();
		T.PutStr("%s \"%s\"", GetTypeName(), m_sValue.m_String);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void shaderMaterialGeoParamValueString::SetValue(const char * szValue)
{
	m_sValue = szValue;
}

void	shaderMaterialGeoParamValueString::WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open string
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add stringcontents
	parTreeNode* pobStringNode = rage_new parTreeNode();
	pobStringNode->GetElement().SetName("stringcontents");
	if(GetValue())
	{
		pobStringNode->SetData(GetValue(), StringLength(GetValue()));
	}
	pobStringNode->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}

