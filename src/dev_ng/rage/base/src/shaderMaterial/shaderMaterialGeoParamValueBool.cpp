// 
// shaderMaterial/shaderMaterialGeoParamValueBool.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if 0

#include "grmodel/shader.h"
#include "file/token.h"
#include "vector/vector2.h"

#include "shaderMaterialGeoParamValueBool.h"

using namespace rage;

shaderMaterialGeoParamValueBool::shaderMaterialGeoParamValueBool(const shaderMaterialGeoParamDescription*	pobParamDescription) : shaderMaterialGeoParamValue(pobParamDescription)
{
	Reset();
}

shaderMaterialGeoParamValueBool::shaderMaterialGeoParamValueBool(const shaderMaterialGeoParamValueBool*	pobSourceParam)
:shaderMaterialGeoParamValue(pobSourceParam)
{
	Copy(pobSourceParam);
}


shaderMaterialGeoParamValueBool::~shaderMaterialGeoParamValueBool()
{
	Reset();
}

shaderMaterialGeoParamValueBool::shaderMaterialGeoParamValueBool(const parTreeNode*	pobGeoParamValueGeoRootNode)
: shaderMaterialGeoParamValue(pobGeoParamValueGeoRootNode)
{
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamValueGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "boolvalue") == 0)
		{
			bool iValue = pobChildValueNode->GetElement().FindAttributeBoolValue("init", 0);
			SetValue(iValue);
		}
	}
}

void shaderMaterialGeoParamValueBool::Reset()
{
	shaderMaterialGeoParamValue::Reset();

	m_bValue = false;
}

void shaderMaterialGeoParamValueBool::Copy(const shaderMaterialGeoParamValueBool * param)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamValue::Copy(param);

	m_bValue = ((shaderMaterialGeoParamValueBool *)param)->m_bValue;
}

bool shaderMaterialGeoParamValueBool::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!shaderMaterialGeoParamValue::IsEqual(param))
	{
		return false;
	}

	return m_bValue == ((shaderMaterialGeoParamValueBool *)param)->m_bValue;
}

bool shaderMaterialGeoParamValueBool::Update(const bool b)
{
	if (m_bValue != b)
	{
		m_bValue = b;
		return true;
	}

	return false;
}

bool shaderMaterialGeoParamValueBool::Update(const shaderMaterialGeoParamValue * param)
{
	Assert(GetType() == param->GetType());

	return Update(((shaderMaterialGeoParamValueBool *)param)->m_bValue);
}

void shaderMaterialGeoParamValueBool::SetDefaultValue(grcEffect & effect, grcEffectVar & var)
{
	effect.GetVar(var, m_bValue);
}

void shaderMaterialGeoParamValueBool::SetMaterialValue(Vector2 & vValue)
{
	vValue.x = (float)m_bValue;
	vValue.y = 0.0f;
}

bool shaderMaterialGeoParamValueBool::ReadGeoParamValueData(fiTokenizer & T)
{
	shaderMaterialGeoParamValue::ReadGeoParamValueData(T);

	m_bValue = T.GetInt()==0 ? false : true;

	return true;
}

bool shaderMaterialGeoParamValueBool::WriteGeoParamValueData(fiTokenizer & T) const
{
	shaderMaterialGeoParamValue::WriteGeoParamValueData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %d", GetTypeName(), m_bValue);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void	shaderMaterialGeoParamValueBool::WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open string
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add intvalue
	parTreeNode* pobIntNode = rage_new parTreeNode();
	pobIntNode->GetElement().SetName("boolvalue");
	pobIntNode->GetElement().AddAttribute("value", (GetValue() ? "true" : "false"));
	pobIntNode->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}

void shaderMaterialGeoParamValueBool::CopyValueIntoVector2(Vector2 & vData) const
{
	vData.x = (float)GetValue();
	vData.y = 0.0f;
}

#endif
