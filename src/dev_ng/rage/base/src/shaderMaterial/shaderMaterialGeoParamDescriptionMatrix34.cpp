// 
// shaderMaterial/shaderMaterialGeoParamDescriptionMatrix34.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"
#include "vector/matrix34.h"

#include "shaderMaterialGeoParamDescriptionMatrix34.h"

using namespace rage;

shaderMaterialGeoParamDescriptionMatrix34::shaderMaterialGeoParamDescriptionMatrix34() : shaderMaterialGeoParamDescription()
{
	Reset();
}

shaderMaterialGeoParamDescriptionMatrix34::~shaderMaterialGeoParamDescriptionMatrix34()
{
	Reset();
}

void shaderMaterialGeoParamDescriptionMatrix34::Reset()
{
	shaderMaterialGeoParamDescription::Reset();

	m_mDefaultValueA = M34_IDENTITY.a;
	m_mDefaultValueB = M34_IDENTITY.b;
	m_mDefaultValueC = M34_IDENTITY.c;
	m_mDefaultValueD = M34_IDENTITY.d;
}

void shaderMaterialGeoParamDescriptionMatrix34::Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamDescription::Copy(param, copyBase);

	m_mDefaultValueA = ((shaderMaterialGeoParamDescriptionMatrix34 *)param)->m_mDefaultValueA;
	m_mDefaultValueB = ((shaderMaterialGeoParamDescriptionMatrix34 *)param)->m_mDefaultValueB;
	m_mDefaultValueC = ((shaderMaterialGeoParamDescriptionMatrix34 *)param)->m_mDefaultValueC;
	m_mDefaultValueD = ((shaderMaterialGeoParamDescriptionMatrix34 *)param)->m_mDefaultValueD;
}

bool shaderMaterialGeoParamDescriptionMatrix34::IsEqual(const shaderMaterialGeoParamDescription * param) const
{
	if (!shaderMaterialGeoParamDescription::IsEqual(param))
	{
		return false;
	}

	// If we have a material data just assume equal
	if (m_MaterialDefaultValueUvSetIndex >= 0)
	{
		return true;
	}

	if (m_mDefaultValueA != ((shaderMaterialGeoParamDescriptionMatrix34 *)param)->m_mDefaultValueA)
	{
		return false;
	}

	if (m_mDefaultValueB != ((shaderMaterialGeoParamDescriptionMatrix34 *)param)->m_mDefaultValueB)
	{
		return false;
	}

	if (m_mDefaultValueC != ((shaderMaterialGeoParamDescriptionMatrix34 *)param)->m_mDefaultValueC)
	{
		return false;
	}

	if (m_mDefaultValueD != ((shaderMaterialGeoParamDescriptionMatrix34 *)param)->m_mDefaultValueD)
	{
		return false;
	}

	return true;
}

bool shaderMaterialGeoParamDescriptionMatrix34::ReadGeoParamDescriptionData(fiTokenizer & T)
{
	shaderMaterialGeoParamDescription::ReadGeoParamDescriptionData(T);

	T.GetVector(m_mDefaultValueA);
	T.GetVector(m_mDefaultValueB);
	T.GetVector(m_mDefaultValueC);
	T.GetVector(m_mDefaultValueD);

	return true;
}

bool shaderMaterialGeoParamDescriptionMatrix34::WriteGeoParamDescriptionData(fiTokenizer & T) const
{
	shaderMaterialGeoParamDescription::WriteGeoParamDescriptionData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %f %f %f %f %f %f %f %f %f %f %f %f", GetTypeName(),
				  m_mDefaultValueA.x, m_mDefaultValueA.y, m_mDefaultValueA.z,
			      m_mDefaultValueB.x, m_mDefaultValueB.y, m_mDefaultValueB.z,
			      m_mDefaultValueC.x, m_mDefaultValueC.y, m_mDefaultValueC.z,
			      m_mDefaultValueD.x, m_mDefaultValueD.y, m_mDefaultValueD.z);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}


void	shaderMaterialGeoParamDescriptionMatrix34::WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* /*pobGeoTemplatesNode*/) const
{
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName("Vector4");
	pobNode->GetElement().AddAttribute("name", GetName());
	if((GetSourceOfParamValue() & GeoInstance) && (GetSourceOfParamValue() & GeoType))
	{
		pobNode->GetElement().AddAttribute("instantiation", "both");
	}
	else if(GetSourceOfParamValue() & GeoInstance)
	{
		pobNode->GetElement().AddAttribute("instantiation", "instance");
	}
	else
	{
		pobNode->GetElement().AddAttribute("instantiation", "type");
	}
	pobNode->GetElement().AddAttribute("displayName", GetUIName());

	parTreeNode* pobANode = rage_new parTreeNode();
	pobANode->GetElement().SetName("a");
	pobANode->GetElement().AddAttribute("x", GetDefaultValue().a.x);
	pobANode->GetElement().AddAttribute("y", GetDefaultValue().a.y);
	pobANode->GetElement().AddAttribute("z", GetDefaultValue().a.z);
	pobANode->AppendAsChildOf(pobNode);

	parTreeNode* pobBNode = rage_new parTreeNode();
	pobBNode->GetElement().SetName("b");
	pobBNode->GetElement().AddAttribute("x", GetDefaultValue().b.x);
	pobBNode->GetElement().AddAttribute("y", GetDefaultValue().b.y);
	pobBNode->GetElement().AddAttribute("z", GetDefaultValue().b.z);
	pobBNode->AppendAsChildOf(pobNode);

	parTreeNode* pobCNode = rage_new parTreeNode();
	pobCNode->GetElement().SetName("c");
	pobCNode->GetElement().AddAttribute("x", GetDefaultValue().c.x);
	pobCNode->GetElement().AddAttribute("y", GetDefaultValue().c.y);
	pobCNode->GetElement().AddAttribute("z", GetDefaultValue().c.z);
	pobCNode->AppendAsChildOf(pobNode);

	parTreeNode* pobDNode = rage_new parTreeNode();
	pobDNode->GetElement().SetName("d");
	pobDNode->GetElement().AddAttribute("x", GetDefaultValue().d.x);
	pobDNode->GetElement().AddAttribute("y", GetDefaultValue().d.y);
	pobDNode->GetElement().AddAttribute("z", GetDefaultValue().d.z);
	pobDNode->AppendAsChildOf(pobNode);

	pobNode->AppendAsChildOf(pobParent);
}

void	shaderMaterialGeoParamDescriptionMatrix34::WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* /*pcTemplateName*/) const
{
	Assertf(0, "This won't write data correctly!");
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("content", "vector4_array");

	char acTempBuffer[255];
	formatf(acTempBuffer, sizeof(acTempBuffer), "  \t\t\t\t\t\t%f\t%f\t%f\n", GetDefaultValue().a.x, GetDefaultValue().a.y, GetDefaultValue().a.z);
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s\t\t\t\t\t\t%f\t%f\t%f\n", acTempBuffer, GetDefaultValue().b.x, GetDefaultValue().b.y, GetDefaultValue().b.z);
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s\t\t\t\t\t\t%f\t%f\t%f\n", acTempBuffer, GetDefaultValue().c.x, GetDefaultValue().c.y, GetDefaultValue().c.z);
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s\t\t\t\t\t\t%f\t%f\t%f\n", acTempBuffer, GetDefaultValue().d.x, GetDefaultValue().d.y, GetDefaultValue().d.z);

	pobNode->SetData(acTempBuffer, StringLength(acTempBuffer));

	pobNode->AppendAsChildOf(pobParent);
}

