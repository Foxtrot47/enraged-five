// 
// shaderMaterialGeoTemplate/shaderMaterialGeoTemplate.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef SHADER_MATERIAL_GEO_TEMPLATE_H
#define SHADER_MATERIAL_GEO_TEMPLATE_H

#include "atl/array.h"
#include "parser/macros.h"
#include "string/string.h"
#include "shaderMaterialGeoParamDescription.h"

namespace rage {

class	fiTokenizer;

class shaderMaterialGeoTemplate
{
public:

	shaderMaterialGeoTemplate();
	shaderMaterialGeoTemplate(const char * szFilename);
	virtual ~shaderMaterialGeoTemplate();

	bool CreateFromShader(const char * pcShaderFilename);
	bool LoadFromGeoTemplate(const char * szFilename);
	bool SaveTo(const char * szFilename) const;

	int GetBucketNumber() const {return m_iBucketNumber;}
	const shaderMaterialGeoParamDescription* GetParamDescription(const char* pcParamName) const;

	const char* GetTemplateName() const
	{
		return m_strTemplateName.c_str();
	}
	const char* GetTemplateFilename() const
	{
		const char* pcPosOfSlash = strrchr(m_strTemplatePathAndFilename.c_str(), '/');
		if(pcPosOfSlash == NULL)
		{
			pcPosOfSlash = strrchr(m_strTemplatePathAndFilename.c_str(), '\\');
		}
		if(pcPosOfSlash != NULL)
		{
			pcPosOfSlash++;
		}
		return pcPosOfSlash;
	}
	const char* GetTemplatePathAndFilename() const
	{
		return m_strTemplatePathAndFilename.c_str();
	}

	const char* GetShaderName() const
	{
		return m_strShaderName.c_str();
	}
	const char* GetShaderFilename() const
	{
		const char* pcPosOfSlash = strrchr(m_strShaderPathAndFilename.c_str(), '/');
		if(pcPosOfSlash == NULL)
		{
			pcPosOfSlash = strrchr(m_strShaderPathAndFilename.c_str(), '\\');
		}
		if(pcPosOfSlash == NULL)
		{
			pcPosOfSlash = m_strShaderPathAndFilename.c_str();
		}
		else
		{
			pcPosOfSlash++;
		}
		return pcPosOfSlash;
	}
	const char* GetShaderPathAndFilename() const
	{
		return m_strShaderPathAndFilename.c_str();
	}

	const atArray <shaderMaterialGeoParamDescription *>* GetShaderParamDescriptions() const {return &m_apShaderParamDescriptions;}
	const rsmSourceVertexColourDescriptions& GetSourceVertexColourDescriptions() const {return m_obSourceVertexColourDescriptions;}
	const rsmRunTimeVertexColourDescriptions& GetRunTimeVertexColourDescriptions() const {return m_obRunTimeVertexColourDescriptions;}

private:

	// Loading and saving
	bool LoadFromGeoTemplate(fiStream * s);
	bool SaveToGeoTemplate(const char * szFilename) const;
	bool SaveToGeoTemplate(fiStream * s) const;
	void SetTemplatePathAndFilename(const char* pcTemplatePathAndFilename);
	void SetShaderPathAndFilename(const char* pcShaderPathAndFilename);
	void SetBucketNumber(int iBucketNumber) {m_iBucketNumber = iBucketNumber;}

	// Useful info
	ConstString m_strTemplateName;
	ConstString m_strTemplatePathAndFilename;
	ConstString m_strShaderName;
	ConstString m_strShaderPathAndFilename;
	atArray <shaderMaterialGeoParamDescription *>	m_apShaderParamDescriptions;
	int			m_iBucketNumber;

	// Vertex Colour Handling
	rsmSourceVertexColourDescriptions m_obSourceVertexColourDescriptions;
	rsmRunTimeVertexColourDescriptions m_obRunTimeVertexColourDescriptions;
};

} // end namespace rage

#endif
