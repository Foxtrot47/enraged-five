// 
// shaderMaterial/shaderMaterialGeoParamValueVector3.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamValueVector3.h"

#include "file/token.h"
#include "grmodel/shader.h"
#include "parser/treenode.h"

using namespace rage;

shaderMaterialGeoParamValueVector3::shaderMaterialGeoParamValueVector3(const shaderMaterialGeoParamDescription*	pobParamDescription) : shaderMaterialGeoParamValue(pobParamDescription)
{
	Reset();
}

shaderMaterialGeoParamValueVector3::shaderMaterialGeoParamValueVector3(const shaderMaterialGeoParamValueVector3*	pobSourceParam)
:shaderMaterialGeoParamValue(pobSourceParam)
{
	Copy(pobSourceParam);
}

shaderMaterialGeoParamValueVector3::~shaderMaterialGeoParamValueVector3()
{
	Reset();
}

shaderMaterialGeoParamValueVector3::shaderMaterialGeoParamValueVector3(const parTreeNode*	pobGeoParamValueGeoRootNode)
: shaderMaterialGeoParamValue(pobGeoParamValueGeoRootNode)
{
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamValueGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "Vector3value") == 0)
		{
			Vector3 obInitVector;
			parTreeNode* pobInitNode = pobChildValueNode->FindChildWithName("init");
			Assertf(pobInitNode, "Unable to find <init> child tag in geo template file");

			obInitVector.x = pobInitNode->GetElement().FindAttributeFloatValue("x", 0.0f);
			obInitVector.y = pobInitNode->GetElement().FindAttributeFloatValue("y", 0.0f);
			obInitVector.z = pobInitNode->GetElement().FindAttributeFloatValue("z", 0.0f);
			SetValue(obInitVector);
		}
	}
}

void shaderMaterialGeoParamValueVector3::Reset()
{
	shaderMaterialGeoParamValue::Reset();

	m_vValue.Zero();
}

void shaderMaterialGeoParamValueVector3::Copy(const shaderMaterialGeoParamValueVector3 * param)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamValue::Copy(param);

	m_vValue = ((shaderMaterialGeoParamValueVector3 *)param)->m_vValue;
}

bool shaderMaterialGeoParamValueVector3::IsEqual(const shaderMaterialGeoParamValue * param) const
{
	if (!shaderMaterialGeoParamValue::IsEqual(param))
	{
		return false;
	}

	return m_vValue == ((shaderMaterialGeoParamValueVector3 *)param)->m_vValue;
}

bool shaderMaterialGeoParamValueVector3::Update(const char * param, const float f0, const float f1, const float f2)
{
	int len = (int) strlen(param);

	if (stricmp(GetParamDescription()->GetUIWidget(), "color")==0)
	{
		if (param[len-1] == 'R')
		{
			if (m_vValue.x != f0)
			{
				m_vValue.x = f0;
				return true;
			}
		}
		else if (param[len-1] == 'G')
		{
			if (m_vValue.y != f0)
			{
				m_vValue.y = f0;
				return true;
			}
		}
		else if (param[len-1] == 'B')
		{
			if (m_vValue.z != f0)
			{
				m_vValue.z = f0;
				return true;
			}
		}
		else
		{
			if (m_vValue.x != f0 ||
				m_vValue.y != f1 ||
				m_vValue.z != f2)
			{
				m_vValue.x = f0;
				m_vValue.y = f1;
				m_vValue.z = f2;
				return true;
			}
		}
	}
	else
	{
		if (param[len-1] == 'X')
		{
			if (m_vValue.x != f0)
			{
				m_vValue.x = f0;
				return true;
			}
		}
		else if (param[len-1] == 'Y')
		{
			if (m_vValue.y != f0)
			{
				m_vValue.y = f0;
				return true;
			}
		}
		else if (param[len-1] == 'Z')
		{
			if (m_vValue.z != f0)
			{
				m_vValue.z = f0;
				return true;
			}
		}
	}

	return false;
}

bool shaderMaterialGeoParamValueVector3::Update(const char * param, const float f0)
{
	int len = (int) strlen(param);

	if (param[len-1] == 'R' || param[len-1] == 'X')
	{
		if (m_vValue.x != f0)
		{
			m_vValue.x = f0;
			return true;
		}
		
	}
	else if (param[len-1] == 'G' || param[len-1] == 'Y')
	{
		if (m_vValue.y != f0)
		{
			m_vValue.y = f0;
			return true;
		}
	}
	else if (param[len-1] == 'B' || param[len-1] == 'Z')
	{
		if (m_vValue.z != f0)
		{
			m_vValue.z = f0;
			return true;
		}
	}
	return false;
}

bool shaderMaterialGeoParamValueVector3::Update(const shaderMaterialGeoParamValue * param)
{
	Assert(GetType() == param->GetType());

	m_vValue = ((shaderMaterialGeoParamValueVector3 *)param)->m_vValue;

	return true;
}

void shaderMaterialGeoParamValueVector3::SetDefaultValue(grcEffect & effect, grcEffectVar & var)
{
	effect.GetVar(var, m_vValue);
}

bool shaderMaterialGeoParamValueVector3::ReadGeoParamValueData(fiTokenizer & T)
{
	shaderMaterialGeoParamValue::ReadGeoParamValueData(T);

	T.GetVector(m_vValue);

	return true;
}

bool shaderMaterialGeoParamValueVector3::WriteGeoParamValueData(fiTokenizer & T) const
{
	shaderMaterialGeoParamValue::WriteGeoParamValueData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %f %f %f", GetTypeName(), m_vValue.x, m_vValue.y, m_vValue.z);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void	shaderMaterialGeoParamValueVector3::WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open string
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add Vector3value
	parTreeNode* pobVector3Node = rage_new parTreeNode();
	pobVector3Node->GetElement().SetName("Vector3value");
	pobVector3Node->GetElement().AddAttribute("x", GetValue().x);
	pobVector3Node->GetElement().AddAttribute("y", GetValue().y);
	pobVector3Node->GetElement().AddAttribute("z", GetValue().z);
	pobVector3Node->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}


bool shaderMaterialGeoParamValueVector3::Update(const Vector3& obValue)
{
	if(m_vValue == obValue)
	{
		return false;
	}
	m_vValue = obValue;
	return true;
}

