// 
// shaderMaterial/shaderMaterialGeoParamDescriptionInt.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "file/token.h"
#include "grmodel/shader.h"
#include "vector/vector2.h"

#include "shaderMaterialGeoParamDescriptionInt.h"

using namespace rage;

shaderMaterialGeoParamDescriptionInt::shaderMaterialGeoParamDescriptionInt() : shaderMaterialGeoParamDescription()
{
	Reset();
}

shaderMaterialGeoParamDescriptionInt::~shaderMaterialGeoParamDescriptionInt()
{
	Reset();
}

shaderMaterialGeoParamDescriptionInt::shaderMaterialGeoParamDescriptionInt(const parTreeNode*	pobGeoParamDescriptionGeoRootNode)
: shaderMaterialGeoParamDescription(pobGeoParamDescriptionGeoRootNode)
{
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamDescriptionGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildDefaultValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "intvalue") == 0)
		{
			int iDefaultValue = pobChildDefaultValueNode->GetElement().FindAttributeIntValue("init", 0);
			SetDefaultValue(iDefaultValue);
			int fMin = pobChildDefaultValueNode->GetElement().FindAttributeIntValue("min", 0);
			SetUIMin((float)fMin);
			int fMax = pobChildDefaultValueNode->GetElement().FindAttributeIntValue("max", 1);
			SetUIMax((float)fMax);
			int fStep = pobChildDefaultValueNode->GetElement().FindAttributeIntValue("step", 1);
			SetUIStep((float)fStep);
			const char* pcUiName = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("displayName", "Unable to file displayName", NULL, 0);
			SetUIName(pcUiName);

			const char* pcInstantiation = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("instantiation", "Unable to file displayName", NULL, 0);
			if(strcmp(pcInstantiation, "instance") == 0)
			{
				SetValueSource(shaderMaterialGeoParamDescription::GeoInstance);
			}
			else if(strcmp(pcInstantiation, "type") == 0)
			{
				SetValueSource(shaderMaterialGeoParamDescription::GeoType);
			}
			else if(strcmp(pcInstantiation, "both") == 0)
			{
				if(GetSourceOfParamValue() == shaderMaterialGeoParamDescription::GeoType)
				{
					// Evil Hack Do nothing!!!!
				}
				else
				{
					SetValueSource(shaderMaterialGeoParamDescription::GeoInstance |shaderMaterialGeoParamDescription::GeoType);
				}
			}
			else
			{
				Assertf(false, "Unknown instantiation level (%s) for param %s", pcInstantiation, GetName());
			}
		}
	}
}

void shaderMaterialGeoParamDescriptionInt::Reset()
{
	shaderMaterialGeoParamDescription::Reset();

	m_nDefaultValue = 0;
}

void shaderMaterialGeoParamDescriptionInt::Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamDescription::Copy(param, copyBase);

	m_nDefaultValue = ((shaderMaterialGeoParamDescriptionInt *)param)->m_nDefaultValue;
}

bool shaderMaterialGeoParamDescriptionInt::IsEqual(const shaderMaterialGeoParamDescription * param) const
{
	if (!shaderMaterialGeoParamDescription::IsEqual(param))
	{
		return false;
	}

	// If we have a material data just assume equal
	if (m_MaterialDefaultValueUvSetIndex >= 0)
	{
		return true;
	}

	return m_nDefaultValue == ((shaderMaterialGeoParamDescriptionInt *)param)->m_nDefaultValue;
}

bool shaderMaterialGeoParamDescriptionInt::CleanUp()
{
	if (m_nDefaultValue < (int)(m_UIMin+0.5f))
	{
		m_nDefaultValue = (int)(m_UIMin+0.5f);
	}

	if (m_nDefaultValue > (int)(m_UIMax+0.5f))
	{
		m_nDefaultValue = (int)(m_UIMax+0.5f);
	}

	return true;
}

bool shaderMaterialGeoParamDescriptionInt::ReadGeoParamDescriptionData(fiTokenizer & T)
{
	shaderMaterialGeoParamDescription::ReadGeoParamDescriptionData(T);

	m_nDefaultValue = T.GetInt();

	return true;
}

bool shaderMaterialGeoParamDescriptionInt::WriteGeoParamDescriptionData(fiTokenizer & T) const
{
	shaderMaterialGeoParamDescription::WriteGeoParamDescriptionData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %d", GetTypeName(), m_nDefaultValue);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void	shaderMaterialGeoParamDescriptionInt::WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const
{
	// Uh-oh, things are gonna get tricky
	// Strings have a lot of info, so create a geo template just for it
	parTreeNode* pobGeoTemplateNode = rage_new parTreeNode();
	pobGeoTemplateNode->GetElement().SetName("GeoTemplate");
	pobGeoTemplateNode->GetElement().AddAttribute("name", GetName());
	pobGeoTemplateNode->AppendAsChildOf(pobGeoTemplatesNode);

	parTreeNode* pobGeoTemplateMembersNode = rage_new parTreeNode();
	pobGeoTemplateMembersNode->GetElement().SetName("Members");
	pobGeoTemplateMembersNode->AppendAsChildOf(pobGeoTemplateNode);

	// Add intvalue
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("int");
		pobNode->GetElement().AddAttribute("name", "intvalue");
		if((GetSourceOfParamValue() & GeoInstance) && (GetSourceOfParamValue() & GeoType))
		{
			pobNode->GetElement().AddAttribute("instantiation", "both");
		}
		else if(GetSourceOfParamValue() & GeoInstance)
		{
			pobNode->GetElement().AddAttribute("instantiation", "instance");
		}
		else
		{
			pobNode->GetElement().AddAttribute("instantiation", "type");
		}
		pobNode->GetElement().AddAttribute("init", GetDefaultValue());
		pobNode->GetElement().AddAttribute("displayName", GetUIName());
		pobNode->GetElement().AddAttribute("min", (int)GetUIMin());
		pobNode->GetElement().AddAttribute("max", (int)GetUIMax());
		pobNode->GetElement().AddAttribute("step", (int)GetUIStep());
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add uihint
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("string");
		pobNode->GetElement().AddAttribute("name", "uihint");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("init", GetUIHint());
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add uiwidget
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("string");
		pobNode->GetElement().AddAttribute("name", "uiwidget");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("init", GetUIWidget());
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add materialdatauvsetindex flag
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("int");
		pobNode->GetElement().AddAttribute("name", "materialdatauvsetindex");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("init", GetMaterialDataUvSetIndex());
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add the template to the list of params
	// Get parent template name
	const char* pcParentsTemplateName = pobGeoTemplatesNode->GetParent()->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

	// Create param node
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName("geo");
	pobNode->GetElement().AddAttribute("name", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcParentsTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);
	pobNode->GetElement().AddAttribute("mustExist", "true");
	pobNode->GetElement().AddAttribute("canDerive", "false");
	pobNode->AppendAsChildOf(pobParent);
}

void	shaderMaterialGeoParamDescriptionInt::WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open string
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add intvalue
	parTreeNode* pobIntNode = rage_new parTreeNode();
	pobIntNode->GetElement().SetName("intvalue");
	pobIntNode->GetElement().AddAttribute("value", GetDefaultValue());
	pobIntNode->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}

