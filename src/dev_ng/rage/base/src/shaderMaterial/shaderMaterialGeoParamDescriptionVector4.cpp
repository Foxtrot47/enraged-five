// 
// shaderMaterial/shaderMaterialGeoParamDescriptionVector4.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamDescriptionVector4.h"

#include "grmodel/shader.h"
#include "file/token.h"
#include "parser/treenode.h"

using namespace rage;

shaderMaterialGeoParamDescriptionVector4::shaderMaterialGeoParamDescriptionVector4() : shaderMaterialGeoParamDescription()
{
	Reset();
}

shaderMaterialGeoParamDescriptionVector4::~shaderMaterialGeoParamDescriptionVector4()
{
	Reset();
}

shaderMaterialGeoParamDescriptionVector4::shaderMaterialGeoParamDescriptionVector4(const parTreeNode*	pobGeoParamDescriptionGeoRootNode)
: shaderMaterialGeoParamDescription(pobGeoParamDescriptionGeoRootNode)
{
	m_MaterialDefaultValueUvSetIndex = -1;
	// Get what info I can out of the geo node
	parTreeNode*	pobMembersNode = pobGeoParamDescriptionGeoRootNode->FindChildWithName("Members");
	Assertf(pobMembersNode, "Unable to find <Members> tag for sub geo in geo template file");

	// Get data
	for(int i=0; i<pobMembersNode->FindNumChildren(); i++)
	{
		parTreeNode*	pobChildDefaultValueNode = pobMembersNode->FindChildWithIndex(i);

		// Get the name of the node
		const char* pcNodeName = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

		if(strcmp(pcNodeName, "Vector4value") == 0)
		{
			float fMin = pobChildDefaultValueNode->GetElement().FindAttributeFloatValue("min", 0.0f);
			SetUIMin(fMin);
			float fMax = pobChildDefaultValueNode->GetElement().FindAttributeFloatValue("max", 1.0f);
			SetUIMax(fMax);

			Vector4 obInitVector;
			parTreeNode* pobInitNode = pobChildDefaultValueNode->FindChildWithName("init");
			Assertf(pobInitNode, "Unable to find <init> child tag in geo template file");

			obInitVector.x = pobInitNode->GetElement().FindAttributeFloatValue("x", 0.0f);
			obInitVector.y = pobInitNode->GetElement().FindAttributeFloatValue("y", 0.0f);
			obInitVector.z = pobInitNode->GetElement().FindAttributeFloatValue("z", 0.0f);
			obInitVector.w = pobInitNode->GetElement().FindAttributeFloatValue("w", 0.0f);
			SetDefaultValue(obInitVector);

			const char* pcUiName = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("displayName", "Unable to file displayName", NULL, 0);
			SetUIName(pcUiName);

			const char* pcInstantiation = pobChildDefaultValueNode->GetElement().FindAttributeStringValue("instantiation", "Unable to file displayName", NULL, 0);
			if(strcmp(pcInstantiation, "instance") == 0)
			{
				SetValueSource(shaderMaterialGeoParamDescription::GeoInstance);
			}
			else if(strcmp(pcInstantiation, "type") == 0)
			{
				SetValueSource(shaderMaterialGeoParamDescription::GeoType);
			}
			else if(strcmp(pcInstantiation, "both") == 0)
			{
				if(GetSourceOfParamValue() == shaderMaterialGeoParamDescription::GeoType)
				{
					// Evil Hack Do nothing!!!!
				}
				else
				{
					SetValueSource(shaderMaterialGeoParamDescription::GeoInstance |shaderMaterialGeoParamDescription::GeoType);
				}
			}
			else
			{
				Assertf(false, "Unknown instantiation level (%s) for param %s", pcInstantiation, GetName());
			}
		}
	}
	}

void shaderMaterialGeoParamDescriptionVector4::Reset()
{
	shaderMaterialGeoParamDescription::Reset();

	m_vDefaultValue.Zero();
}

void shaderMaterialGeoParamDescriptionVector4::Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase)
{
	if (!param)
	{
		return;
	}

	shaderMaterialGeoParamDescription::Copy(param, copyBase);

	m_vDefaultValue = ((shaderMaterialGeoParamDescriptionVector4 *)param)->m_vDefaultValue;
}

bool shaderMaterialGeoParamDescriptionVector4::IsEqual(const shaderMaterialGeoParamDescription * param) const
{
	if (!shaderMaterialGeoParamDescription::IsEqual(param))
	{
		return false;
	}

	// If we have a material data just assume equal
	if (m_MaterialDefaultValueUvSetIndex >= 0)
	{
		return true;
	}

	return m_vDefaultValue == ((shaderMaterialGeoParamDescriptionVector4 *)param)->m_vDefaultValue;
}

bool shaderMaterialGeoParamDescriptionVector4::ReadGeoParamDescriptionData(fiTokenizer & T)
{
	shaderMaterialGeoParamDescription::ReadGeoParamDescriptionData(T);

	T.GetVector(m_vDefaultValue);

	return true;
}

bool shaderMaterialGeoParamDescriptionVector4::WriteGeoParamDescriptionData(fiTokenizer & T) const
{
	shaderMaterialGeoParamDescription::WriteGeoParamDescriptionData(T);

	T.StartBlock();
	{
		T.StartLine();
		T.PutStr("%s %f %f %f %f", GetTypeName(), m_vDefaultValue.x, m_vDefaultValue.y, m_vDefaultValue.z, m_vDefaultValue.w);
		T.EndLine();
	}
	T.EndBlock();

	return true;
}

void	shaderMaterialGeoParamDescriptionVector4::WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const
{
	// Uh-oh, things are gonna get tricky
	// Strings have a lot of info, so create a geo template just for it
	parTreeNode* pobGeoTemplateNode = rage_new parTreeNode();
	pobGeoTemplateNode->GetElement().SetName("GeoTemplate");
	pobGeoTemplateNode->GetElement().AddAttribute("name", GetName());
	pobGeoTemplateNode->AppendAsChildOf(pobGeoTemplatesNode);

	parTreeNode* pobGeoTemplateMembersNode = rage_new parTreeNode();
	pobGeoTemplateMembersNode->GetElement().SetName("Members");
	pobGeoTemplateMembersNode->AppendAsChildOf(pobGeoTemplateNode);

	// Add Vector4value
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("Vector4");
		pobNode->GetElement().AddAttribute("name", "Vector4value");
		if((GetSourceOfParamValue() & GeoInstance) && (GetSourceOfParamValue() & GeoType))
		{
			pobNode->GetElement().AddAttribute("instantiation", "both");
		}
		else if(GetSourceOfParamValue() & GeoInstance)
		{
			pobNode->GetElement().AddAttribute("instantiation", "instance");
		}
		else
		{
			pobNode->GetElement().AddAttribute("instantiation", "type");
		}
		pobNode->GetElement().AddAttribute("displayName", GetUIName());
		pobNode->GetElement().AddAttribute("min", (float)GetUIMin());
		pobNode->GetElement().AddAttribute("max", (float)GetUIMax());

		{
			parTreeNode* pobInitNode = rage_new parTreeNode();
			pobInitNode->GetElement().SetName("init");
			pobInitNode->GetElement().AddAttribute("x", GetDefaultValue().x);
			pobInitNode->GetElement().AddAttribute("y", GetDefaultValue().y);
			pobInitNode->GetElement().AddAttribute("z", GetDefaultValue().z);
			pobInitNode->GetElement().AddAttribute("w", GetDefaultValue().w);

			pobInitNode->AppendAsChildOf(pobNode);
		}

		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add uihint
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("string");
		pobNode->GetElement().AddAttribute("name", "uihint");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("init", GetUIHint());
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	// Add uiwidget
	{
		parTreeNode* pobNode = rage_new parTreeNode();
		pobNode->GetElement().SetName("string");
		pobNode->GetElement().AddAttribute("name", "uiwidget");
		pobNode->GetElement().AddAttribute("instantiation", "type");
		pobNode->GetElement().AddAttribute("init", GetUIWidget());
		pobNode->GetElement().AddAttribute("locked", "true");
		pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	}

	//// Add materialdatauvsetindex flag
	//{
	//	parTreeNode* pobNode = new parTreeNode();
	//	pobNode->GetElement().SetName("int");
	//	pobNode->GetElement().AddAttribute("name", "materialdatauvsetindex");
	//	pobNode->GetElement().AddAttribute("instantiation", "type");
	//	pobNode->GetElement().AddAttribute("init", GetMaterialDataUvSetIndex());
	//	pobNode->GetElement().AddAttribute("locked", "true");
	//	pobNode->AppendAsChildOf(pobGeoTemplateMembersNode);
	//}

	// Add the template to the list of params
	// Get parent template name
	const char* pcParentsTemplateName = pobGeoTemplatesNode->GetParent()->GetElement().FindAttributeStringValue("name", "Nonsense", NULL, 0);

	// Create param node
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName("geo");
	pobNode->GetElement().AddAttribute("name", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcParentsTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);
	pobNode->GetElement().AddAttribute("mustExist", "true");
	pobNode->GetElement().AddAttribute("canDerive", "false");
	pobNode->AppendAsChildOf(pobParent);
}

void	shaderMaterialGeoParamDescriptionVector4::WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const
{
	// Open string
	char acBuffer[256];
	parTreeNode* pobNode = rage_new parTreeNode();
	pobNode->GetElement().SetName(GetName());
	pobNode->GetElement().AddAttribute("type", GetName());
	formatf(acBuffer, sizeof(acBuffer), "%s/%s", pcTemplateName, GetName());
	pobNode->GetElement().AddAttribute("template", acBuffer);

	// Add Vector4value
	parTreeNode* pobVector4Node = rage_new parTreeNode();
	pobVector4Node->GetElement().SetName("Vector4value");
	pobVector4Node->GetElement().AddAttribute("x", GetDefaultValue().x);
	pobVector4Node->GetElement().AddAttribute("y", GetDefaultValue().y);
	pobVector4Node->GetElement().AddAttribute("z", GetDefaultValue().z);
	pobVector4Node->GetElement().AddAttribute("w", GetDefaultValue().w);
	pobVector4Node->AppendAsChildOf(pobNode);

	// Add to parent
	pobNode->AppendAsChildOf(pobParent);
}

