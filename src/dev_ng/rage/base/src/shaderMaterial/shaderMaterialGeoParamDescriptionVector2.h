// 
// shaderMaterial/shaderMaterialGeoParamDescriptionVector2.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_VECTOR2_H
#define SHADER_MATERIAL_GEO_PARAM_VECTOR2_H

#include "parser/macros.h"
#include "vector/vector2.h"

#include "shaderMaterialGeoParamDescription.h"

namespace rage {

class shaderMaterialGeoParamDescriptionVector2 : public shaderMaterialGeoParamDescription
{
public:

	shaderMaterialGeoParamDescriptionVector2();
	virtual ~shaderMaterialGeoParamDescriptionVector2();

	shaderMaterialGeoParamDescriptionVector2(const parTreeNode*	pobGeoParamDescriptionGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_VECTOR2;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_VECTOR2);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase=true);
	bool IsEqual(const shaderMaterialGeoParamDescription * param) const;

	bool ReadGeoParamDescriptionData(fiTokenizer & T);
	bool WriteGeoParamDescriptionData(fiTokenizer & T) const;

	const Vector2 & GetDefaultValue() const {return m_vDefaultValue;}
	void SetDefaultValue(const Vector2& vDefaultValue) {m_vDefaultValue.Set(vDefaultValue);}

	virtual void	WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const;
	virtual void	WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// DefaultValue for this param
	Vector2 m_vDefaultValue;
};

} // end namespace rage

#endif
