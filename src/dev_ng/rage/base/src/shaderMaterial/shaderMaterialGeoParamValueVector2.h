// 
// shaderMaterial/shaderMaterialGeoParamValueVector2.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_VALUE_VECTOR2_H
#define SHADER_MATERIAL_GEO_PARAM_VALUE_VECTOR2_H

#include "parser/macros.h"
#include "vector/vector2.h"

#include "shaderMaterialGeoParamValue.h"

namespace rage {

class shaderMaterialGeoParamValueVector2 : public shaderMaterialGeoParamValue
{
public:

	shaderMaterialGeoParamValueVector2(const shaderMaterialGeoParamDescription*	pobParamDescription);
	shaderMaterialGeoParamValueVector2(const shaderMaterialGeoParamValueVector2*	pobSourceParam);
	virtual ~shaderMaterialGeoParamValueVector2();

	shaderMaterialGeoParamValueVector2(const parTreeNode*	pobGeoParamValueGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_VECTOR2;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_VECTOR2);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamValueVector2 * param);
	bool IsEqual(const shaderMaterialGeoParamValue * param) const;

	bool Update(const shaderMaterialGeoParamValue * param);
	bool Update(const char * param, const float f);
	bool Update(const Vector2& obValue);

	void SetDefaultValue(grcEffect & effect, grcEffectVar & var);

	void SetMaterialValue(Vector2 & vValue);
	void CopyValueIntoVector2(Vector2 & vData) const;

	bool ReadGeoParamValueData(fiTokenizer & T);
	bool WriteGeoParamValueData(fiTokenizer & T) const;

	const Vector2 & GetValue() const {return m_vValue;}
	void SetValue(const Vector2& vValue) {m_vValue.Set(vValue);}

	virtual void	WriteAsGeoTypeMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:

	// Value for this param
	Vector2 m_vValue;
};

} // end namespace rage

#endif
