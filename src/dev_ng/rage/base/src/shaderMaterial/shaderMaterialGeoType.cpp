// 
// shaderMaterialGeoType/shaderMaterialGeoType.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderMaterialGeoParamDescription.h"
// #include "shaderMaterialGeoParamDescriptionBool.h"
#include "shaderMaterialGeoParamDescriptionFloat.h"
// #include "shaderMaterialGeoParamDescriptionInt.h"
#include "shaderMaterialGeoParamDescriptionMatrix34.h"
#include "shaderMaterialGeoParamDescriptionMatrix44.h"
#include "shaderMaterialGeoParamDescriptionString.h"
#include "shaderMaterialGeoParamDescriptionTexture.h"
#include "shaderMaterialGeoParamDescriptionVector2.h"
#include "shaderMaterialGeoParamDescriptionVector3.h"
#include "shaderMaterialGeoParamDescriptionVector4.h"
#include "shaderMaterialGeoParamValue.h"
// #include "shaderMaterialGeoParamValueBool.h"
#include "shaderMaterialGeoParamValueFloat.h"
// #include "shaderMaterialGeoParamValueInt.h"
#include "shaderMaterialGeoParamValueMatrix34.h"
#include "shaderMaterialGeoParamValueMatrix44.h"
#include "shaderMaterialGeoParamValueString.h"
#include "shaderMaterialGeoParamValueTexture.h"
#include "shaderMaterialGeoParamValueVector2.h"
#include "shaderMaterialGeoParamValueVector3.h"
#include "shaderMaterialGeoParamValueVector4.h"
#include "shaderMaterialGeoManager.h"
#include "shaderMaterialGeoType.h"

#include "bank/msgbox.h"
#include "file/asset.h"
#include "file/device.h"
#include "grcore/texturereference.h"
#include "grmodel/namedbuckets.h"
#include "grmodel/shader.h"
#include "grmodel/shadervar.h"
#include "parser/manager.h"

#if __WIN32PC
#pragma warning(push)
#pragma warning(disable : 4668)
#include <Rpc.h>

#pragma comment(lib, "rpcrt4.lib")

#pragma warning(pop)
#endif

using namespace rage;

shaderMaterialGeoType::shaderMaterialGeoType()
{
	m_strParentTypePathAndFilename = "";
	m_strTypePathAndFilename = "";
	m_strParentTypeName = "";
	m_strTypeName = "";
	SetUIDToString("Default assigned in shaderMaterialGeoType::shaderMaterialGeoType()");

	m_pobTemplate = NULL;
	m_pobParentType = NULL;
	m_bInstance = false;

	AssignNewUID();
}

shaderMaterialGeoType::~shaderMaterialGeoType()
{
	for(int i=0; i<m_apShaderParamValues.GetCount(); i++)
	{
		delete m_apShaderParamValues[i];
	}
}

shaderMaterialGeoType::shaderMaterialGeoType(const shaderMaterialGeoType & obThat)	// Copy constructor
{
	m_strParentTypePathAndFilename = "";
	m_strTypePathAndFilename = "";
	m_strParentTypeName = "";
	m_strTypeName = "";
	SetUIDToString("Default assigned in shaderMaterialGeoType::shaderMaterialGeoType(const shaderMaterialGeoType & obThat)");

	m_pobParentType = NULL;

	m_bInstance = obThat.m_bInstance;

	Copy(obThat);
}

shaderMaterialGeoType & shaderMaterialGeoType::operator=(const shaderMaterialGeoType & rhs)	// Assignment operator
{
	Copy(rhs);
	return *this;
}

void shaderMaterialGeoType::AddParamValuesFromParamDescriptions(const atArray <shaderMaterialGeoParamDescription *>*	papParamDescriptions, atArray <shaderMaterialGeoParamValue *>*	papParamValues)
{
	for(int i=0; i<papParamDescriptions->GetCount(); i++)
	{
		const shaderMaterialGeoParamDescription * pobParamDesc = (*papParamDescriptions)[i];
		shaderMaterialGeoParamValue * pobParamValue = NULL;
		switch(pobParamDesc->GetType())
		{
		/* case grcEffect::VT_INT:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueInt(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionInt*)pobParamDesc)->GetDefaultValue());
				break;
			} */
		case grcEffect::VT_FLOAT:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueFloat(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionFloat*)pobParamDesc)->GetDefaultValue());
				break;
			}
		case grcEffect::VT_VECTOR2:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueVector2(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionVector2*)pobParamDesc)->GetDefaultValue());
				break;
			}
		case grcEffect::VT_VECTOR3:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueVector3(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionVector3*)pobParamDesc)->GetDefaultValue());
				break;
			}
		case grcEffect::VT_VECTOR4:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueVector4(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionVector4*)pobParamDesc)->GetDefaultValue());
				break;
			}
		case grcEffect::VT_TEXTURE:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueTexture(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionTexture*)pobParamDesc)->GetDefaultValue());
				break;
			}
		/* case grcEffect::VT_BOOL:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueBool(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionBool*)pobParamDesc)->GetDefaultValue());
				break;
			} */
		case grcEffect::VT_MATRIX34:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueMatrix34(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionMatrix34*)pobParamDesc)->GetDefaultValue());
				break;
			}
		case grcEffect::VT_MATRIX44:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueMatrix44(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionMatrix44*)pobParamDesc)->GetDefaultValue());
				break;
			}
		case grcEffect::VT_STRING:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueString(pobParamDesc);
				pobParamValue->Update(((shaderMaterialGeoParamDescriptionString*)pobParamDesc)->GetDefaultValue());
				break;
			}
		default:
			AssertMsg(0 , "Invalid param type");
		}

		pobParamValue->SetName(pobParamDesc->GetName());
		papParamValues->PushAndGrow(pobParamValue);
	}
}

bool shaderMaterialGeoType::InitialiseFromTemplate(const shaderMaterialGeoTemplate* pobShaderMaterialGeoTemplate)
{
	Assertf(pobShaderMaterialGeoTemplate, "Unable to create a Instance from a NULL Template");

	// Make a default Instance from a GeoTemplate where all the values are just default
	m_pobTemplate = pobShaderMaterialGeoTemplate;
	//MAKE NOT LAME: ADD ref

	// Fake a default path for me
	atString strGeoTypeFakedPathAndFilename = shaderMaterialGeoManager::GetBaseGEOPath();
	if(strGeoTypeFakedPathAndFilename != "")
	{
		strGeoTypeFakedPathAndFilename += "GEOTypes/";
	}
	strGeoTypeFakedPathAndFilename += pobShaderMaterialGeoTemplate->GetTemplateName();
	strGeoTypeFakedPathAndFilename += ".geotype";
	SetTypePathAndFilename(strGeoTypeFakedPathAndFilename);

	// Add default param values
	m_apShaderParamValues.clear();
	const atArray <shaderMaterialGeoParamDescription *>*	papSPDescriptions = m_pobTemplate->GetShaderParamDescriptions();
	AddParamValuesFromParamDescriptions(papSPDescriptions, &m_apShaderParamValues);

	return true;
}

bool shaderMaterialGeoType::ConstructFromParentType(const shaderMaterialGeoType* pobParentShaderMaterialGeoType)
{
	Assertf(pobParentShaderMaterialGeoType, "Unable to create a Instance from a NULL Type");

	// First copy the parent over myself
	Copy(*pobParentShaderMaterialGeoType);

	// Then set the given type as my parent
	m_pobParentType = pobParentShaderMaterialGeoType;
	m_strParentTypePathAndFilename = pobParentShaderMaterialGeoType->GetTypePathAndFilename();

	// Clear my filename
	SetTypePathAndFilename("");
	AssignNewUID();
	return true;
}

bool shaderMaterialGeoType::ChangeParentType(const shaderMaterialGeoType* pobNewParentShaderMaterialGeoType)
{
	Assertf(pobNewParentShaderMaterialGeoType, "Unable to change parent type to NULL Type");

	// Make a backup of myself
	shaderMaterialGeoType* pobBackUpOfMyOwnBadSelf = rage_new shaderMaterialGeoType();
	pobBackUpOfMyOwnBadSelf->Copy(*this);

	// Copy the new parent over myself
	Copy(*pobNewParentShaderMaterialGeoType);

	// Then set the given type as my parent
	m_pobParentType = pobNewParentShaderMaterialGeoType;
	m_strParentTypePathAndFilename = pobNewParentShaderMaterialGeoType->GetTypePathAndFilename();

	// Copy values from my backup over myself
	SetTypePathAndFilename(pobBackUpOfMyOwnBadSelf->GetTypePathAndFilename());
	CopyParamValues(*pobBackUpOfMyOwnBadSelf);
	SetUIDToString(pobBackUpOfMyOwnBadSelf->GetUID());

	// Clean up
	delete pobBackUpOfMyOwnBadSelf;
	return true;
}

bool shaderMaterialGeoType::SaveTo(const char * szFilename) const
{
	// Make sure I have a good extension
	char acGeoMtlPathAndFilename[512];
	CleanFilename(acGeoMtlPathAndFilename, szFilename);
	if(strrchr(acGeoMtlPathAndFilename, '.') != NULL)
	{
		char* pcPosOfDot = strrchr(acGeoMtlPathAndFilename, '.');
		(*pcPosOfDot) = '\0';
	}
	strcat(acGeoMtlPathAndFilename, ".geoType");
	return SaveToGeoType(acGeoMtlPathAndFilename);
}

bool shaderMaterialGeoType::SetShaderVariableValue(grmShaderVar & var) const
{
	const shaderMaterialGeoParamValue * p = GetParam(var.GetName());
	if (!p)
	{
		return false;
	}

	switch (var.GetType())
	{
		// DO NOTHING
	case grcEffect::VT_STRING:
		break;
	case grcEffect::VT_TEXTURE:
		{
			sysMemStartTemp();
			ConstString sTextureName;
			((shaderMaterialGeoParamValueTexture *)p)->GetTextureName(sTextureName);
			sysMemEndTemp();

			if (strcmp((const char *)sTextureName, "none") && strcmp((const char *)sTextureName,"none.dds"))
#if __RESOURCECOMPILER
				var.Set(rage_new grcTextureReference((const char *)sTextureName,NULL));
#else
				var.Set(grcTextureFactory::GetInstance().Create((const char *)sTextureName));
#endif
			else
				var.Set((grcTexture*)0);
			sysMemStartTemp();
			sTextureName = NULL;
			sysMemEndTemp();
		}
		break;
	/* case grcEffect::VT_BOOL:
		var.m_Data.i = ((shaderMaterialGeoParamValueBool *)p)->GetValue()==false ? 0 : 1;		
		break;
	case grcEffect::VT_INT:
		var.m_Data.i = ((shaderMaterialGeoParamValueInt *)p)->GetValue();
		break; */
	case grcEffect::VT_FLOAT:
		var.Set(((shaderMaterialGeoParamValueFloat *)p)->GetValue());
		break;
	case grcEffect::VT_VECTOR2:
		{
			var.Set(((shaderMaterialGeoParamValueVector2 *)p)->GetValue());
			break;
		}
	case grcEffect::VT_VECTOR3:
		{
			var.Set(((shaderMaterialGeoParamValueVector3 *)p)->GetValue());
			break;
		}
	case grcEffect::VT_VECTOR4:
		{
			var.Set(((shaderMaterialGeoParamValueVector4 *)p)->GetValue());
			break;
		}
	case grcEffect::VT_MATRIX34:
		var.Set(((shaderMaterialGeoParamValueMatrix34 *)p)->GetValue());
		break;
	case grcEffect::VT_MATRIX44:
		var.Set(((shaderMaterialGeoParamValueMatrix44 *)p)->GetValue());
		break;
	default:
		AssertMsg(0 , "Invalid param type");	
	}

	return true;
}

bool shaderMaterialGeoType::UpdateShaderVariable(const grmShaderVar & var)
{
	shaderMaterialGeoParamValue * p = GetParam(var.GetName());
	if (!p)
	{
		return false;
	}

	float f;
	Vector2 v2;
	Vector3 v3;
	Vector4 v4;
	Matrix34 m34;
	Matrix44 m44;
	switch (var.GetType())
	{
		// DO NOTHING
	case grcEffect::VT_STRING:
	case grcEffect::VT_TEXTURE:
		break;
	/* case grcEffect::VT_BOOL:
		p->Update(var.m_Data.i==0?false:true);
		break;
	case grcEffect::VT_INT:
		p->Update(var.m_Data.i);
		break; */
	case grcEffect::VT_FLOAT:
		var.Get(f);
		p->Update(f);
		break;
	case grcEffect::VT_VECTOR2:
		var.Get(v2);
		p->Update("X", v2.x);
		p->Update("Y", v2.y);
		break;
	case grcEffect::VT_VECTOR3:
		var.Get(v3);
		p->Update("X", v3.x);
		p->Update("Y", v3.y);
		p->Update("Z", v3.z);
		break;
	case grcEffect::VT_VECTOR4:
		var.Get(v4);
		p->Update("X", v4.x);
		p->Update("Y", v4.y);
		p->Update("Z", v4.z);
		p->Update("W", v4.w);
		break;
	case grcEffect::VT_MATRIX34:
		var.Get(m34);
		p->Update("AX", m34.a.x);
		p->Update("AY", m34.a.y);
		p->Update("AZ", m34.a.z);

		p->Update("BX", m34.b.x);
		p->Update("BY", m34.b.y);
		p->Update("BZ", m34.b.z);

		p->Update("CX", m34.c.x);
		p->Update("CY", m34.c.y);
		p->Update("CZ", m34.c.z);

		p->Update("DX", m34.d.x);
		p->Update("DY", m34.d.y);
		p->Update("DZ", m34.d.z);
		break;
	case grcEffect::VT_MATRIX44:
		var.Get(m44);
		p->Update("AX", m44.a.x);
		p->Update("AY", m44.a.y);
		p->Update("AZ", m44.a.z);
		p->Update("AW", m44.a.w);

		p->Update("BX", m44.b.x);
		p->Update("BY", m44.b.y);
		p->Update("BZ", m44.b.z);
		p->Update("BW", m44.b.w);

		p->Update("CX", m44.c.x);
		p->Update("CY", m44.c.y);
		p->Update("CZ", m44.c.z);
		p->Update("CW", m44.c.w);

		p->Update("DX", m44.d.x);
		p->Update("DY", m44.d.y);
		p->Update("DZ", m44.d.z);
		p->Update("DW", m44.d.w);
		break;
	default:
		AssertMsg(0 , "Invalid param type");	
	}

	return true;
}

bool shaderMaterialGeoType::LoadParameterFromGeoType(parTreeNode*	pobMemberNode, shaderMaterialGeoParamValue* pobParam)
{
	if(pobParam->GetSourceOfParamValue() == shaderMaterialGeoParamDescription::GeoTemplate) 
	{
		// Global, so bail
		return true;
	}
	// Found member in geo file, so get its value
	switch(pobParam->GetType())
	{
	/* case grcEffect::VT_INT:
		{
			parTreeNode*	pobChildNode = pobMemberNode->GetChild();
			int iData = 0;
			if(pobChildNode)
			{
				iData = pobChildNode->GetElement().FindAttributeIntValue("value", 0);
			}
			else
			{
				iData = pobMemberNode->GetElement().FindAttributeIntValue("value", 0);
			}
			((shaderMaterialGeoParamValueInt*)pobParam)->SetValue(iData);
			break;
		} */
	case grcEffect::VT_FLOAT:
		{
			parTreeNode*	pobChildNode = pobMemberNode->GetChild();
			float fData = 0;
			if(pobChildNode)
			{
				fData = pobChildNode->GetElement().FindAttributeFloatValue("value", 0.0f);
			}
			else
			{
				fData = pobMemberNode->GetElement().FindAttributeFloatValue("value", 0.0f);
			}
			((shaderMaterialGeoParamValueFloat*)pobParam)->SetValue(fData);
			break;
		}
	case grcEffect::VT_VECTOR2:
		{
			parTreeNode*	pobChildNode = pobMemberNode->GetChild();
			Vector2 obData;
			if(pobChildNode)
			{
				obData.x = pobChildNode->GetElement().FindAttributeFloatValue("x", 0.0f);
				obData.y = pobChildNode->GetElement().FindAttributeFloatValue("y", 0.0f);
			}
			else
			{
				obData.x = pobMemberNode->GetElement().FindAttributeFloatValue("x", 0.0f);
				obData.y = pobMemberNode->GetElement().FindAttributeFloatValue("y", 0.0f);
			}

			((shaderMaterialGeoParamValueVector2*)pobParam)->SetValue(obData);
			break;
		}
	case grcEffect::VT_VECTOR3:
		{
			parTreeNode*	pobChildNode = pobMemberNode->GetChild();
			Vector3 obData;
			if(pobChildNode)
			{
				obData.x = pobChildNode->GetElement().FindAttributeFloatValue("x", 0.0f);
				obData.y = pobChildNode->GetElement().FindAttributeFloatValue("y", 0.0f);
				obData.z = pobChildNode->GetElement().FindAttributeFloatValue("z", 0.0f);
			}
			else
			{
				obData.x = pobMemberNode->GetElement().FindAttributeFloatValue("x", 0.0f);
				obData.y = pobMemberNode->GetElement().FindAttributeFloatValue("y", 0.0f);
				obData.z = pobMemberNode->GetElement().FindAttributeFloatValue("z", 0.0f);
			}

			((shaderMaterialGeoParamValueVector3*)pobParam)->SetValue(obData);
			break;
		}
	case grcEffect::VT_VECTOR4:
		{
			parTreeNode*	pobChildNode = pobMemberNode->GetChild();
			Vector4 obData;
			if(pobChildNode)
			{
				obData.x = pobChildNode->GetElement().FindAttributeFloatValue("x", 0.0f);
				obData.y = pobChildNode->GetElement().FindAttributeFloatValue("y", 0.0f);
				obData.z = pobChildNode->GetElement().FindAttributeFloatValue("z", 0.0f);
				obData.w = pobChildNode->GetElement().FindAttributeFloatValue("w", 0.0f);
			}
			else
			{
				obData.x = pobMemberNode->GetElement().FindAttributeFloatValue("x", 0.0f);
				obData.y = pobMemberNode->GetElement().FindAttributeFloatValue("y", 0.0f);
				obData.z = pobMemberNode->GetElement().FindAttributeFloatValue("z", 0.0f);
				obData.w = pobMemberNode->GetElement().FindAttributeFloatValue("w", 0.0f);
			}

			((shaderMaterialGeoParamValueVector4*)pobParam)->SetValue(obData);
			break;
		}
	case grcEffect::VT_TEXTURE:
		{
			parTreeNode*	pobChildNode = pobMemberNode->GetChild();
			const char* pcData;
			if(pobChildNode)
			{
				pcData = pobChildNode->GetData();
			}
			else
			{
				pcData = pobMemberNode->GetData();
			}
			((shaderMaterialGeoParamValueTexture*)pobParam)->SetValue(pcData);
			break;
		}
	/* case grcEffect::VT_BOOL:
		{
			parTreeNode*	pobChildNode = pobMemberNode->GetChild();
			bool bData = false;
			if(pobChildNode)
			{
				const char* pcData = pobChildNode->GetElement().FindAttributeStringValue("value", "false", NULL, 0);
				bData = (strcmp(pcData, "true") == 0);
			}
			else
			{
				const char* pcData = pobMemberNode->GetElement().FindAttributeStringValue("value", "false", NULL, 0);
				bData = (strcmp(pcData, "true") == 0);
			}
			((shaderMaterialGeoParamValueBool*)pobParam)->SetValue(bData);
			break;
		} */
	case grcEffect::VT_MATRIX34:
		{
			Matrix34 obData;
			const char* pcData = pobMemberNode->GetData();
			sscanf(pcData, 
				"%f\t%f\t%f\n%f\t%f\t%f\n%f\t%f\t%f\n%f\t%f\t%f\n", 
				&obData.a.x, &obData.a.y, &obData.a.z, 
				&obData.b.x, &obData.b.y, &obData.b.z, 
				&obData.c.x, &obData.c.y, &obData.c.z, 
				&obData.d.x, &obData.d.y, &obData.d.z
				);
			((shaderMaterialGeoParamValueMatrix34*)pobParam)->SetValue(obData);
			break;
		}
	case grcEffect::VT_MATRIX44:
		{
			Matrix44 obData;
			const char* pcData = pobMemberNode->GetData();
			sscanf(pcData, 
				"%f\t%f\t%f\t%f\n%f\t%f\t%f\t%f\n%f\t%f\t%f\t%f\n%f\t%f\t%f\t%f\n", 
				&obData.a.x, &obData.a.y, &obData.a.z, &obData.a.w, 
				&obData.b.x, &obData.b.y, &obData.b.z, &obData.b.w, 
				&obData.c.x, &obData.c.y, &obData.c.z, &obData.c.w, 
				&obData.d.x, &obData.d.y, &obData.d.z, &obData.d.w
				);
			((shaderMaterialGeoParamValueMatrix44*)pobParam)->SetValue(obData);
			break;
		}
	case grcEffect::VT_STRING:
		{
			parTreeNode*	pobChildNode = pobMemberNode->GetChild();
			const char* pcData;
			if(pobChildNode)
			{
				pcData = pobChildNode->GetData();
			}
			else
			{
				pcData = pobMemberNode->GetData();
			}
			((shaderMaterialGeoParamValueString*)pobParam)->SetValue(pcData);
			break;
		}
	default:
		AssertMsg(0 , "Invalid param type");
	}
	return true;
}

bool shaderMaterialGeoType::LoadFromGeoType(fiStream * s)
{
	parTree*		pobTree = PARSER.LoadTree(s);
	parTreeNode*	pobRootNode = pobTree->GetRoot();

	// Get the UUID
	SetUIDToString(pobRootNode->GetElement().FindAttributeStringValue("uuid", "No default UID found in file", NULL, 0));
	const char* pcTemplateName = pobRootNode->GetElement().FindAttributeStringValue("template", "", NULL, 0);
	Assertf(strlen(pcTemplateName) != 0, "Unable to open %s file %s with empty template name", (m_bInstance ? "geomtl" : "geo mtl type"), GetTypePathAndFilename());

	// Connect up to parent Geo Type and template
	if(strlen(pcTemplateName) > 1)
	{
		m_pobTemplate = SHADER_MATERIAL_GEO_MANAGER.LoadShaderMaterialGeoTemplate(pcTemplateName);

		if(!m_pobTemplate)
		{
			atString strGeoTemplatePathAndFilename = shaderMaterialGeoManager::GetBaseGEOPath();
			atString strTemplatePathAndFilename = shaderMaterialGeoManager::GetPathToTemplates();
			char acAssertMessage[4096];
			sprintf(acAssertMessage, "Unable to find template %s for %s file %s.  I tried to load it from %sGEOTemplates/%s.geotemplate and if that failed I tried to load it from %s%s.geotemplate", pcTemplateName, (m_bInstance ? "mtl geo instance" : "mtl geo type"), GetTypePathAndFilename(), strGeoTemplatePathAndFilename.c_str(), pcTemplateName, strTemplatePathAndFilename.c_str(), pcTemplateName);
			Errorf(acAssertMessage);
#if __ASSERT
//			Assertf(m_pobTemplate, acAssertMessage);
#endif
			return false;
		}
	}

	const char* pcParentTypeName = NULL;

	parTreeNode* pobParentTypeWFullPath = pobRootNode->FindChildWithName("TypeFilename");
	if(pobParentTypeWFullPath) //try new way
	{
		pcParentTypeName = pobParentTypeWFullPath->GetData();
		Assertf(strlen(pcParentTypeName) != 0, "Unable to open %s file [%s] with empty parent type name", "mtl geo", s->GetName());
		m_pobParentType = SHADER_MATERIAL_GEO_MANAGER.LoadShaderMaterialGeoType(pcParentTypeName);
	}
	else //try old way
	{
		pcParentTypeName = pobRootNode->GetElement().FindAttributeStringValue("base", NULL, NULL, 0);
		if (pcParentTypeName)
		{
			m_pobParentType = SHADER_MATERIAL_GEO_MANAGER.LoadShaderMaterialGeoType(pcParentTypeName);
		}
	}

	// Create a clean version from the template directly
	if(m_pobTemplate)
	{
		InitialiseFromTemplate(m_pobTemplate);
	}

	// Set my values to the same as my parent types
	if(m_pobParentType)
	{
		// Create a clean version from my parent type
		CopyParamValues(*m_pobParentType);
		SetParentTypePathAndFilename(m_pobParentType->GetTypePathAndFilename());
	}

	// Now I am a pure shader with all my values set from the template and parent, so set them to what I think they should be
	// Do the shader members
	parTreeNode*	pobShaderMembersNode = pobRootNode->FindChildWithName("ShaderMembers");
	if(pobShaderMembersNode)
	{
		for (int i=0; i<m_apShaderParamValues.GetCount(); i++) 
		{
			// Get the param in the geo file
			parTreeNode*	pobShaderMemberNode = pobShaderMembersNode->FindChildWithName(m_apShaderParamValues[i]->GetName());
			if(pobShaderMemberNode)
			{
				if((m_apShaderParamValues[i]->GetSourceOfParamValue() & shaderMaterialGeoParamDescription::GeoType))
				{
					LoadParameterFromGeoType(pobShaderMemberNode, m_apShaderParamValues[i]);
				}
			}
		}
	}
	
	// Clean up
	delete pobTree;

	// Assign a new UID
	if(m_bInstance)
	{
		AssignNewUID();
	}

	// Return success
	return true;
}

bool shaderMaterialGeoType::LoadFromGeoType(const char * szFilename)
{
	bool bResult = false;
	SetTypePathAndFilename(szFilename);
	fiStream * streamMtl = fiStream::PreLoad(fiStream::Open(szFilename));
	if (streamMtl)
	{
		bResult = LoadFromGeoType(streamMtl);
		streamMtl->Close();
	}
	SetTypePathAndFilename(szFilename);
	return bResult;
}

bool shaderMaterialGeoType::SaveToGeoType(fiStream * s) const
{
//	Assertf(m_pobParentType, "Trying to save a material Geo Instance file, but the Geo Instance does not have a Type");
	AssignNewUID();

	char acTempBuffer[255];

	// Construct an XML parser tree, for writting out
	parTreeNode* pobRootNode = rage_new parTreeNode();

	// Open instance
	pobRootNode->GetElement().SetName(m_pobTemplate->GetTemplateName());
	pobRootNode->GetElement().AddAttribute("template", m_pobTemplate->GetTemplateName());
	if(m_pobParentType)
	{
		char acParentTypeName[255];
		strcpy(acParentTypeName, m_pobParentType->GetTypeFilename());
		*strchr(acParentTypeName, '.') = '\0';
		pobRootNode->GetElement().AddAttribute("base", acParentTypeName);
	}
	pobRootNode->GetElement().AddAttribute("name", GetTypeName());
	pobRootNode->GetElement().AddAttribute("uuid", GetUID());

	// Write out Template name
	parTreeNode* pobTemplateNameNode = rage_new parTreeNode();
	pobTemplateNameNode->GetElement().SetName("TemplateFilename");
	pobTemplateNameNode->SetData((const char*)m_pobTemplate->GetTemplatePathAndFilename(), StringLength(m_pobTemplate->GetTemplatePathAndFilename()));
	pobTemplateNameNode->AppendAsChildOf(pobRootNode);

	// Write out Type name
	if(m_pobParentType)
	{
		parTreeNode* pobTypeNameNode = rage_new parTreeNode();
		pobTypeNameNode->GetElement().SetName("TypeFilename");
		pobTypeNameNode->SetData((const char*)m_pobParentType->GetTypePathAndFilename(), StringLength(m_pobParentType->GetTypePathAndFilename()));
		pobTypeNameNode->AppendAsChildOf(pobRootNode);
	}

	// Write out shader members
	parTreeNode* pobShaderMembersNode = rage_new parTreeNode();
	pobShaderMembersNode->GetElement().SetName("ShaderMembers");
	pobShaderMembersNode->GetElement().AddAttribute("type", "ShaderMembers");
	formatf(acTempBuffer, sizeof(acTempBuffer), "%s/ShaderMembers", m_pobTemplate->GetTemplateName());
	pobShaderMembersNode->GetElement().AddAttribute("template", acTempBuffer);
	pobShaderMembersNode->AppendAsChildOf(pobRootNode);
	for(int i=0; i<m_apShaderParamValues.GetCount(); i++)
	{
		if (m_apShaderParamValues[i]->GetSourceOfParamValue() & shaderMaterialGeoParamDescription::GeoType)
		{
			m_apShaderParamValues[i]->WriteAsGeoTypeMember(pobShaderMembersNode, m_pobTemplate->GetTemplateName());
		}
	}

	// Write it all out
	parTree obTree;
	obTree.SetRoot(pobRootNode);
	PARSER.SaveTree(s, &obTree);

	return true;
}

bool shaderMaterialGeoType::SaveToGeoType(const char * szFilename) const
{
	// Make sure the destination folder exists
	ASSET.CreateLeadingPath(szFilename);

	// Write it out
	(const_cast<shaderMaterialGeoType*>(this))->SetTypePathAndFilename(szFilename);
	bool bResult = false;
	fiStream * streamMtl = fiStream::Create(GetTypePathAndFilename());
	if (streamMtl)
	{
		char acGeoTypeName[255];
		strcpy(acGeoTypeName, strrchr(szFilename, '/') + 1);
		*strchr(acGeoTypeName, '.') = '\0';
		bResult = SaveToGeoType(streamMtl);
		streamMtl->Close();
	}
	return bResult;
}

const shaderMaterialGeoParamValue * shaderMaterialGeoType::GetParam(const char* pcParamName) const
{
	const shaderMaterialGeoParamValue * pobReturnMe = GetShaderParam(pcParamName);
	return pobReturnMe;
}


shaderMaterialGeoParamValue * shaderMaterialGeoType::GetParam(const char* pcParamName)
{
	shaderMaterialGeoParamValue * pobReturnMe = GetShaderParam(pcParamName);
	return pobReturnMe;
}

const shaderMaterialGeoParamValue * shaderMaterialGeoType::GetParamByUIName(const char* pcParamName) const
{
	const shaderMaterialGeoParamValue * pobReturnMe = GetShaderParamByUIName(pcParamName);
	return pobReturnMe;
}


shaderMaterialGeoParamValue * shaderMaterialGeoType::GetParamByUIName(const char* pcParamName)
{
	shaderMaterialGeoParamValue * pobReturnMe = GetShaderParamByUIName(pcParamName);
	return pobReturnMe;
}

const shaderMaterialGeoParamValue * shaderMaterialGeoType::GetParam(int iParamNo) const
{
	return GetShaderParam(iParamNo);
}

shaderMaterialGeoParamValue * shaderMaterialGeoType::GetParam(int iParamNo)
{
	Assertf(iParamNo < GetNoOfParams(), "Unable to get parameter number %d as there are only %d parameters", iParamNo, GetNoOfParams());
	return GetShaderParam(iParamNo);
}

const shaderMaterialGeoParamValue * shaderMaterialGeoType::GetShaderParam(const char* pcParamName) const
{
	for(int i=0; i<m_apShaderParamValues.GetCount(); i++)
	{
		if(strcmp(m_apShaderParamValues[i]->GetName(), pcParamName) == 0)
		{
			return m_apShaderParamValues[i];
		}
	}
	return NULL;
}

shaderMaterialGeoParamValue * shaderMaterialGeoType::GetShaderParam(const char* pcParamName)
{
	for(int i=0; i<m_apShaderParamValues.GetCount(); i++)
	{
		if(strcmp(m_apShaderParamValues[i]->GetName(), pcParamName) == 0)
		{
			return m_apShaderParamValues[i];
		}
	}
	return NULL;
}


const shaderMaterialGeoParamValue * shaderMaterialGeoType::GetShaderParamByUIName(const char* pcParamName) const
{
	for(int i=0; i<m_apShaderParamValues.GetCount(); i++)
	{
		if(strcmp(m_apShaderParamValues[i]->GetParamDescription()->GetUIName(), pcParamName) == 0)
		{
			return m_apShaderParamValues[i];
		}
	}
	return NULL;
}

shaderMaterialGeoParamValue * shaderMaterialGeoType::GetShaderParamByUIName(const char* pcParamName)
{
	for(int i=0; i<m_apShaderParamValues.GetCount(); i++)
	{
		if(strcmp(m_apShaderParamValues[i]->GetParamDescription()->GetUIName(), pcParamName) == 0)
		{
			return m_apShaderParamValues[i];
		}
	}
	return NULL;
}

const shaderMaterialGeoParamValue * shaderMaterialGeoType::GetShaderParam(int iParamNo) const
{
	return m_apShaderParamValues[iParamNo];
}

shaderMaterialGeoParamValue * shaderMaterialGeoType::GetShaderParam(int iParamNo)
{
	return m_apShaderParamValues[iParamNo];
}

void shaderMaterialGeoType::SetParentTypePathAndFilename(const char* pcParentTypePathAndFilename)
{
	char acBuffer[1024];
	CleanFilename(acBuffer, pcParentTypePathAndFilename);
	m_strParentTypePathAndFilename = acBuffer;

	const char* pcPosOfFilenameStart = &(acBuffer[0]);
	if(strrchr(acBuffer, '/') != NULL)
	{
		pcPosOfFilenameStart = strrchr(acBuffer, '/') + 1;
	}
	if(strrchr(acBuffer, '.') != NULL)
	{
		char* pcPosOfDot = strrchr(acBuffer, '.');
		(*pcPosOfDot) = '\0';
	}
	m_strParentTypeName = pcPosOfFilenameStart;
}

void shaderMaterialGeoType::SetTypePathAndFilename(const char* pcTypePathAndFilename)
{
	char acBuffer[1024];
	CleanFilename(acBuffer, pcTypePathAndFilename);
	m_strTypePathAndFilename = acBuffer;

	const char* pcPosOfFilenameStart = &(acBuffer[0]);
	if(strrchr(acBuffer, '/') != NULL)
	{
		pcPosOfFilenameStart = strrchr(acBuffer, '/') + 1;
	}
	if(strrchr(acBuffer, '.') != NULL)
	{
		char* pcPosOfDot = strrchr(acBuffer, '.');
		(*pcPosOfDot) = '\0';
	}
	m_strTypeName = pcPosOfFilenameStart;
}

void shaderMaterialGeoType::Copy(const shaderMaterialGeoType & obThat)
{
	// Clear myself
	m_strParentTypePathAndFilename = "";
	m_strTypePathAndFilename = "";
	m_strParentTypeName = "";
	m_strTypeName = "";
	m_pobTemplate = NULL;
	m_pobParentType = NULL;
	SetUIDToString("Default assigned in shaderMaterialGeoType::Copy(const shaderMaterialGeoType & obThat)");

	// Copy everything across
	m_strParentTypePathAndFilename = obThat.m_strParentTypePathAndFilename;
	m_strTypePathAndFilename = obThat.m_strTypePathAndFilename;
	m_strParentTypeName = obThat.m_strParentTypeName;
	m_strTypeName = obThat.m_strTypeName;
	
	m_pobTemplate = obThat.m_pobTemplate;
	m_pobParentType = obThat.m_pobParentType;
	SetUIDToString(obThat.GetUID());
	m_bInstance = obThat.m_bInstance;

	// Do the params
	CopyParamValues(obThat);
}

void shaderMaterialGeoType::CopyParamValues(const shaderMaterialGeoType & obThat)
{
	// Nuke the old ones
	for(int i=0; i<m_apShaderParamValues.GetCount(); i++)
	{
		delete m_apShaderParamValues[i];
	}
	m_apShaderParamValues.clear();
	
	// Create new ones
	for(int i=0; i<obThat.m_apShaderParamValues.GetCount(); i++)
	{
		const shaderMaterialGeoParamValue * pobSourceParamValue = obThat.m_apShaderParamValues[i];
		shaderMaterialGeoParamValue * pobParamValue = NULL;
		switch(pobSourceParamValue->GetType())
		{
		/* case grcEffect::VT_INT:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueInt((const shaderMaterialGeoParamValueInt*)pobSourceParamValue);
				break;
			} */
		case grcEffect::VT_FLOAT:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueFloat((const shaderMaterialGeoParamValueFloat*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_VECTOR2:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueVector2((const shaderMaterialGeoParamValueVector2*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_VECTOR3:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueVector3((const shaderMaterialGeoParamValueVector3*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_VECTOR4:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueVector4((const shaderMaterialGeoParamValueVector4*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_TEXTURE:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueTexture((const shaderMaterialGeoParamValueTexture*)pobSourceParamValue);
				break;
			}
		/* case grcEffect::VT_BOOL:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueBool((const shaderMaterialGeoParamValueBool*)pobSourceParamValue);
				break;
			} */
		case grcEffect::VT_MATRIX34:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueMatrix34((const shaderMaterialGeoParamValueMatrix34*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_MATRIX44:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueMatrix44((const shaderMaterialGeoParamValueMatrix44*)pobSourceParamValue);
				break;
			}
		case grcEffect::VT_STRING:
			{
				pobParamValue = rage_new shaderMaterialGeoParamValueString((const shaderMaterialGeoParamValueString*)pobSourceParamValue);
				break;
			}
		default:
			AssertMsg(0 , "Invalid param type");
		}

		m_apShaderParamValues.PushAndGrow(pobParamValue);
	}
}

bool shaderMaterialGeoType::IsEqual(const shaderMaterialGeoType &obThat) const
{
	// First, the easy stuff
	if(m_strParentTypePathAndFilename != obThat.m_strParentTypePathAndFilename) return false;
	if(m_strTypePathAndFilename != obThat.m_strTypePathAndFilename) return false;
	if(m_strParentTypeName != obThat.m_strParentTypeName) return false;
	if(m_strTypeName != obThat.m_strTypeName) return false;
	
	if(m_pobTemplate != obThat.m_pobTemplate) return false;
	if(m_pobParentType != obThat.m_pobParentType) return false;
	if(strcmp(GetUID(), obThat.GetUID()) != 0) return false;
	if(m_bInstance != obThat.m_bInstance) return false;

	// Next the params
	if(m_apShaderParamValues.GetCount() != obThat.m_apShaderParamValues.GetCount()) return false;
	for(int i=0; i<obThat.m_apShaderParamValues.GetCount(); i++)
	{
		if(!m_apShaderParamValues[i]->IsEqual(obThat.m_apShaderParamValues[i])) return false;
	}

	// All equal
	return true;
}

int shaderMaterialGeoType::GetUvSetCount() const
{
	int uvIndex = 0;
	for (int i=0; i<GetNoOfParams(); i++)
	{
		const shaderMaterialGeoParamValue * pobParam = GetParam(i);
		if (pobParam->GetType() == grcEffect::VT_TEXTURE)
		{
			if((pobParam->GetParamDescription()->GetUvSetIndex()+1)>uvIndex)
			{
				uvIndex =  pobParam->GetParamDescription()->GetUvSetIndex()+1;
			}
		}
	}

	return uvIndex;
}


const char* shaderMaterialGeoType::GetTypeFilename() const
{
	const char* pcPosOfSlash = strrchr(m_strTypePathAndFilename.c_str(), '/');
	if(pcPosOfSlash == NULL)
	{
		pcPosOfSlash = strrchr(m_strTypePathAndFilename.c_str(), '\\');
	}

	//if it still == null then no path info must be present
	if (pcPosOfSlash == NULL)
	{
		pcPosOfSlash = m_strTypePathAndFilename.c_str();
	}
	else
	{
		//move past the slash
		pcPosOfSlash++;
	}
	return pcPosOfSlash;
}
const char* shaderMaterialGeoType::GetTypePathAndFilename() const
{
	return m_strTypePathAndFilename.c_str();
}
const char* shaderMaterialGeoType::GetTypeName() const
{
	return m_strTypeName.c_str();
}
const char* shaderMaterialGeoType::GetParentTypeFilename() const
{
	const char* pcPosOfSlash = strrchr(m_strParentTypePathAndFilename.c_str(), '/');
	if(pcPosOfSlash == NULL)
	{
		pcPosOfSlash = strrchr(m_strParentTypePathAndFilename.c_str(), '\\');
	}
	if(pcPosOfSlash != NULL)
	{
		pcPosOfSlash++;
	}
	return pcPosOfSlash;
}
const char* shaderMaterialGeoType::GetParentTypePathAndFilename() const
{
	return m_strParentTypePathAndFilename.c_str();
}
const char* shaderMaterialGeoType::GetParentTypeName() const
{
	return m_strParentTypeName.c_str();
}
const char* shaderMaterialGeoType::GetUID() const
{
	return m_sGeoTypeUID.c_str();
}

const shaderMaterialGeoType* shaderMaterialGeoType::GetParentType() const
{
	return m_pobParentType;
}

const shaderMaterialGeoTemplate* shaderMaterialGeoType::GetTemplate() const
{
	Assertf(m_pobTemplate, "Some how I have a NULL Template on a loaded shaderMaterialGeoType");
	return m_pobTemplate;
}

void shaderMaterialGeoType::AssignNewUID() const
{
#if __WIN32PC
	// Get New UUID
	UUID obUUID;
	unsigned char* sTemp = NULL;
	UuidCreateSequential( &obUUID );
	UuidToString(&obUUID, &sTemp);
	SetUIDToString((const char*)sTemp);
	RpcStringFree(&sTemp);
#endif
}
