// 
// shaderMaterial/shaderMaterialGeoParamDescriptionBool.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADER_MATERIAL_GEO_PARAM_BOOL_H
#define SHADER_MATERIAL_GEO_PARAM_BOOL_H

#include "parser/manager.h"
#include "shaderMaterialGeoParamDescription.h"

namespace rage {

class shaderMaterialGeoParamDescriptionBool : public shaderMaterialGeoParamDescription
{
public:

	shaderMaterialGeoParamDescriptionBool();
	virtual ~shaderMaterialGeoParamDescriptionBool();

	shaderMaterialGeoParamDescriptionBool(const parTreeNode*	pobGeoParamDescriptionGeoRootNode);

	int	GetType()				const	{return grcEffect::VT_BOOL;}
	const char * GetTypeName()	const	{return grcEffect::GetTypeName(grcEffect::VT_BOOL);}

	virtual void Reset();

	void Copy(const shaderMaterialGeoParamDescription * param, const bool copyBase=true);
	bool IsEqual(const shaderMaterialGeoParamDescription * param) const;

	bool ReadGeoParamDescriptionData(fiTokenizer & T);
	bool WriteGeoParamDescriptionData(fiTokenizer & T) const;

	bool GetDefaultValue() const {return m_bDefaultValue;}
	void SetDefaultValue(const bool& bDefaultValue) {m_bDefaultValue = bDefaultValue;}

	virtual void	WriteAsGeoTemplateMember(parTreeNode* pobParent, parTreeNode* pobGeoTemplatesNode) const;
	virtual void	WriteAsGeoInstanceMember(parTreeNode* pobParent, const char* pcTemplateName) const;

private:
	// DefaultValue for this param
	bool m_bDefaultValue;
};

} // end namespace rage

#endif
