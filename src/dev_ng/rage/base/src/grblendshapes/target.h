// 
// grblendshapes/target.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRBLENDSHAPES_TARGET_H
#define GRBLENDSHAPES_TARGET_H

#include "grblendshapes/morphable.h"
#include "data/base.h"
#include "data/struct.h"

#include "blendshapes_config.h"

#if ENABLE_BLENDSHAPES

namespace rage
{

/* PURPOSE:
	This class is a mapping for a blend target.  It contains a list of morphables
	that contain delta information for this target.  Invoking the blend function on
	this target, will add the deltas for this target to the respective offset buffers.
*/
class grbTarget : public datBase
{
public:
	grbTarget(const char* name);
	grbTarget(datResource& rsc);
	~grbTarget();

	DECLARE_PLACE(grbTarget);

	/*	PURPOSE:
			Set the index of this target within all the morphables.
		PARAMS:
			targetIndex - The index of the target data in all of the morpahbles.
		NOTES:
			All of the morphables are assumed to be set up in the same order.  This
			target index identifies the target data within the morphable.
	*/
	void SetTargetIndex(int targetIndex);

	/*	PURPOSE:
			Function to preallocate the array of morphables
		PARAMS:
			morphableCount - The number of morphables to allocate space for
	*/
	void AllocateMorphables(int morphableCount);

	/*	PURPOSE:
			Add a morphable to this blend target
		PARAMS:
			morphable - A pointer to the morpahble to add.
	*/
	void AddMorphable(grbMorphable* morphable);

	/*	PURPOSE:
			Get the name of this target
		RETURNS:
			A null terminated string containing the name of this target
	*/
	const char* GetName() const									{ return m_Name; }

	/*	PURPOSE:
			Add the deltas for this target to the offset buffers in the morpahbles at the
			specified blend percentage.
		PARAMS:
			blendPercent - The percentage of this target to blend
	*/
	void Blend(float blendPercent);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

protected:
	char							m_Name[64];
	int								m_TargetIndex;
	atArray<datRef<grbMorphable> >	m_Morphables;
};


} // namespace rage

#endif // ENABLE_BLENDSHAPES

#endif	// GRBLENDSHAPES_TARGET_H
