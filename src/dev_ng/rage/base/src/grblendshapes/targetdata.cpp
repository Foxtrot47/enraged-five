//
// grblendshapes/targetdata.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "grblendshapes/targetdata.h"

#include "atl/array_struct.h"
#include "data/safestruct.h"
#include "diag/tracker.h"
#include "file/asset.h"
#include "file/stream.h"
#include "grblendshapes/blendshapes.h"
#include "grcore/device.h"
#include "grcore/fvf.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/grmodel_config.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/cache.h"
#include "system/magicnumber.h"
#include "system/performancetimer.h"
#include "morphable.h"

#include <algorithm>

#if ENABLE_BLENDSHAPES

namespace rage
{

#if __STATS
	namespace BlendShapesStats {
		PF_PAGE(Page,"Blend Shapes Stats");
		PF_GROUP(Blending);
		PF_LINK(Page, Blending);
		PF_TIMER(BlendTimer, Blending);
		PF_COUNTER(BlendCalls, Blending);
		PF_COUNTER(BlendVerts, Blending);
	};
#endif

#if !__FINAL
int g_TargetDataVertexCount;
#endif

const int grbTargetData::kPositionShift = 12;
const float grbTargetData::kPositionEpsilon = 2.0f / (1 << kPositionShift);
const float grbTargetData::kPositionEpsilon2 = kPositionEpsilon * kPositionEpsilon;
#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
const float grbTargetData::kNormalEpsilon = 0.5f / 128.0f;
const float grbTargetData::kNormalEpsilon2 = kNormalEpsilon * kNormalEpsilon;
#endif // USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS

grbTargetData::SpecializedBlendFunction grbTargetData::sm_BlendFunctions[] =
{
	&grbTargetData::BlendAddPos,
	&grbTargetData::BlendAddPosNrm,
	&grbTargetData::BlendAddPosNrmTan,
};

#if 1 || __RESOURCECOMPILER
static inline u32 PackChannelToByte(float f,int shift) {
	if (f < -1.0f) f = -1.0f;
	else if (f > 1.0f) f = 1.0f;
	return ((int(f * 127.0f)) & 0xFF) << shift;
}

static inline u64 PackChannelToHalf(float f,int shift) {
	return (u64)((int(f * (1 << grbTargetData::kPositionShift))) & 0xFFFF) << shift;
}

static u32 _PackNormal(const Vector3 &v) {
	return PackChannelToByte(v.x,24) | PackChannelToByte(v.y,16) | PackChannelToByte(v.z,8);
}

static u64 _PackPosition(const Vector3 &v) {
	return PackChannelToHalf(v.x,48) | PackChannelToHalf(v.y,32) | PackChannelToHalf(v.z,16);
}
#endif // __RESOURCECOMPILER

#if RSG_CPU_INTEL
static inline Vector3 _UnpackNormal(grbTargetData::QuantisedNormalParam pn) {
	int x = ((int)pn << 0) >> 24;
	int y = ((int)pn << 8) >> 24;
	int z = ((int)pn << 16) >> 24;
	const float convert = 1.0f / 128.0f;
	return Vector3(x * convert, y * convert, z * convert);
}

static inline Vector3 _UnpackPosition(grbTargetData::QuantisedPositionParam pn) {
	int x = int(((s64)pn << 0) >> 48);
	int y = int(((s64)pn << 16) >> 48);
	int z = int(pn) >> 16;
	const float convert = 1.0f / (1 << grbTargetData::kPositionShift);
	return  Vector3(x * convert, y * convert, z * convert);
}

#else

static inline __vector4 _UnpackNormal(grbTargetData::QuantisedNormalParam pn) {
	_cvector4 ixyzPacked = (_cvector4) __lvlx(&pn,0);
	_hvector4 ixyzHalf = __vupkhsb(ixyzPacked);
	_ivector4 ixyz = __vupkhsh(ixyzHalf);
	__vector4 result = __vcsxwfp(ixyz, 7);
	return result;
}

static inline __vector4 _UnpackPosition(grbTargetData::QuantisedPositionParam pn) {
	// We can cheat because we know the input address is always 64bit aligned
	_hvector4 ixyzHalf = (_hvector4) __lvlx((int*)&pn,0);
	_ivector4 ixyz = __vupkhsh(ixyzHalf);
	__vector4 result = __vcsxwfp(ixyz, grbTargetData::kPositionShift);
	return result;
}

#endif // __WIN32PC

#if QUANTISE_BLENDWEIGHT_DATA
#define PackNormal(x)		_PackNormal(x)
#define UnpackNormal(x)		_UnpackNormal(x)
#define PackPosition(x)		_PackPosition(x)
#define UnpackPosition(x)	_UnpackPosition(x)
#else
#define PackNormal(x)		(x)
#define UnpackNormal(x)		(x)
#define PackPosition(x)		(x)
#define UnpackPosition(x)	(x)
#endif // QUANTISE_BLENDWEIGHT_DATA

IMPLEMENT_PLACE(grbTargetData);

grbTargetData::grbTargetData(int vertexCount, const grcFvf* fvf)
{
	CompileTimeAssert(NELEM(sm_BlendFunctions) == bfidCount);

	m_TotalMeshVerts = vertexCount;

	m_HasPositions = fvf->GetPosChannel();
	m_HasNormals = fvf->GetNormalChannel();
	m_HasTangents = fvf->GetTangentChannel(0);
}

grbTargetData::grbTargetData(class datResource& rsc)
#if QUANTISE_BLENDWEIGHT_DATA
: m_Positions(rsc)
, m_Normals(rsc)
, m_Tangents(rsc)
, m_VertexMap(rsc)
#endif // QUANTISE_BLENDWEIGHT_DATA
{
#if !QUANTISE_BLENDWEIGHT_DATA
	atArray<QuantisedPosition> positions(rsc);
	m_Positions.Resize(positions.GetCount());
	for (int i = 0; i < m_Positions.GetCount(); ++i)
		m_Positions[i] = _UnpackPosition(positions[i]);

	atArray<QuantisedNormal> normals(rsc);
	m_Normals.Resize(normals.GetCount());
	for (int i = 0; i < m_Normals.GetCount(); ++i)
		m_Normals[i] = _UnpackNormal(normals[i]);

	atArray<QuantisedNormal> tangents(rsc);
	m_Tangents.Resize(tangents.GetCount());
	for (int i = 0; i < m_Tangents.GetCount(); ++i)
		m_Tangents[i] = _UnpackNormal(tangents[i]);

	atArray<int> vertexMap(rsc);
	m_VertexMap.Swap(vertexMap);
#endif // !QUANTISE_BLENDWEIGHT_DATA
}

grbTargetData::~grbTargetData()
{
}

void grbTargetData::SetDeltas(const atArray<VertexPosNrmTanDelta>& deltas)
{
	int deltaCount = 0;

	for( int i = 0; i < deltas.GetCount(); i++ )
	{
		if( (m_HasPositions && deltas[i].Position.Mag2() > kPositionEpsilon2)
#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
			|| (m_HasNormals && deltas[i].Normal.Mag2() > kNormalEpsilon2)
			|| (m_HasTangents && deltas[i].Tangent.Mag2() > kNormalEpsilon2)
#endif // USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
			)
		++deltaCount;
	}

	// Allocate space for the deltas
	if (deltaCount > 0)
	{
		m_VertexMap.Resize(deltaCount);

		if( m_HasPositions )
			m_Positions.Resize(deltaCount);
		if( m_HasNormals )
			m_Normals.Resize(deltaCount);
		if( m_HasTangents )
			m_Tangents.Resize(deltaCount);
	}

	// Copy the data over and map it
	deltaCount = 0;
	for( int i = 0; i < deltas.GetCount(); i++ )
	{
		if( (m_HasPositions && deltas[i].Position.Mag2() > kPositionEpsilon2)
#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
			|| (m_HasNormals && deltas[i].Normal.Mag2() > kNormalEpsilon2)
			|| (m_HasTangents && deltas[i].Tangent.Mag2() > kNormalEpsilon2)
#endif // USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
			)
		{
			if( m_HasPositions)
				m_Positions[deltaCount] = PackPosition(deltas[i].Position);

			if( m_HasNormals )
				m_Normals[deltaCount] = PackNormal(deltas[i].Normal);

			if( m_HasTangents )
				m_Tangents[deltaCount] = PackNormal(deltas[i].Tangent);

			m_VertexMap[deltaCount] = i;
			deltaCount++;
		}
	}

	//SortVertexAttributes();
}

void grbTargetData::BlendAdd(float t, grcVertexBuffer* outOffsets)
{
	Assert(outOffsets);
#if MORPH_DYNAMIC_BUFFER
	Assert(outOffsets->IsDynamic());
#endif

	const grcFvf* pFvf = outOffsets->GetFvf();

	BlendFunctionID bfid;
	Assert(m_HasPositions && pFvf->GetPosChannel());
	if( m_HasNormals && pFvf->GetNormalChannel() )
	{
		if( m_HasTangents && pFvf->GetTangentChannel(0) )
		{
			bfid = bfidPosNrmTan;
		}
		else
		{
			bfid = bfidPosNrm;
		}
	}
	else
	{
		bfid = bfidPos;
	}

#if __STATS
	using namespace BlendShapesStats;
	PF_START(BlendTimer);
#endif
	Vector3 vT;
	vT.Set(t);
	(this->*(sm_BlendFunctions[bfid]))(vT, outOffsets);

#if __STATS
	PF_STOP(BlendTimer);
	PF_INCREMENT(BlendCalls);
	PF_INCREMENTBY(BlendVerts, m_VertexMap.GetCount());
#endif
#if !__FINAL
	g_TargetDataVertexCount += m_VertexMap.GetCount();
#endif
}

int grbTargetData::GetVertexCount() const
{
	return (int)m_TotalMeshVerts;
}

#if __DECLARESTRUCT
void grbTargetData::DeclareStruct(datTypeStruct &s)
{
	SSTRUCT_BEGIN(grbTargetData)
		SSTRUCT_FIELD(grbTargetData, m_TotalMeshVerts)
		SSTRUCT_FIELD(grbTargetData, m_HasPositions)
		SSTRUCT_FIELD(grbTargetData, m_HasNormals)
		SSTRUCT_FIELD(grbTargetData, m_HasTangents)
		SSTRUCT_FIELD(grbTargetData, m_Pad)
		SSTRUCT_FIELD(grbTargetData, m_Positions)
		SSTRUCT_FIELD(grbTargetData, m_Normals)
		SSTRUCT_FIELD(grbTargetData, m_Tangents)
		SSTRUCT_FIELD(grbTargetData, m_VertexMap)
	SSTRUCT_END(grbTargetData)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////////////////////////
// Blend Functions

#if __WIN32PC || __PSP2
void grbTargetData::BlendAddPosNrm(Vector3::Vector3Param t, grcVertexBuffer* outOffsets)
{
	grcVertexBufferEditor outOffsetsEditor(outOffsets);
	for( int i = 0; i < m_VertexMap.GetCount(); i++ )
	{
		int vertIndex = m_VertexMap[i];

		Vector3 vPos = outOffsetsEditor.GetPosition(vertIndex);
		vPos.AddScaled(UnpackPosition(m_Positions[i]), t);
		outOffsetsEditor.SetPosition(vertIndex, vPos);

		Vector3 vNrm = outOffsetsEditor.GetNormal(vertIndex);
		vNrm.AddScaled(UnpackNormal(m_Normals[i]), t);
		outOffsetsEditor.SetNormal(vertIndex, vNrm);
	}
}

void grbTargetData::BlendAddPos(Vector3::Vector3Param t, grcVertexBuffer* outOffsets)
{
	grcVertexBufferEditor outOffsetsEditor(outOffsets);
	for( int i = 0; i < m_VertexMap.GetCount(); i++ )
	{
		int vertIndex = m_VertexMap[i];

		Vector3 vPos = outOffsetsEditor.GetPosition(vertIndex);
		vPos.AddScaled(UnpackPosition(m_Positions[i]), t);
		outOffsetsEditor.SetPosition(vertIndex, vPos);
	}
}

void grbTargetData::BlendAddPosNrmTan(Vector3::Vector3Param t, grcVertexBuffer* outOffsets)
{
	struct Vertex {
		Vector3 Pos;
		Vector3 Nrm;
		Vector3 Tan;
	};

	Vertex* RESTRICT vBase = (Vertex*) outOffsets->GetFastLockPtr();
	Assert(outOffsets->GetVertexStride() == 3 * 16);

	for( int i = 0; i < m_VertexMap.GetCount(); i++ )
	{
		int vertIndex = m_VertexMap[i];

		vBase[vertIndex].Pos.AddScaled(UnpackPosition(m_Positions[i]), t);

		vBase[vertIndex].Nrm.AddScaled(UnpackNormal(m_Normals[i]), t);

		vBase[vertIndex].Tan.AddScaled(UnpackNormal(m_Tangents[i]),t);
	}
}
#else
void grbTargetData::BlendAddPosNrm(Vector3::Vector3Param t_, grcVertexBuffer* outOffsets)
{
	struct Vertex {
		__vector4 Pos;
		__vector4 Nrm;
	};
	__vector4 t = *(__vector4*)&t_;

	Vertex * RESTRICT vBase = (Vertex*) outOffsets->GetFastLockPtr();

	int iCount = m_VertexMap.GetCount();

	// Check the blendshape is not empty
	// i.e there are no positions, normals or tangents
	if (iCount == 0)
	{
		return;
	}

	const Position *RESTRICT vPos = m_Positions.GetElements();
	const Normal * RESTRICT vNrm = m_Normals.GetElements();

	const int * RESTRICT iMap = &m_VertexMap[0];

	int i;
	for( i = 0; i < iCount-3; i+=4 )
	{
		// Prefetch the next eight inputs
		PrefetchDC(iMap+i+4);
		PrefetchDC(vPos+i+4);
		PrefetchDC(vNrm+i+4);
		PrefetchDC(vBase+iMap[i+4]);

		int vertIndex0 = iMap[i];
		int vertIndex1 = iMap[i+1];
		int vertIndex2 = iMap[i+2];
		int vertIndex3 = iMap[i+3];

		__vector4 pos0 = vBase[vertIndex0].Pos;
		pos0 = __vmaddfp(UnpackPosition(vPos[i]),t,pos0);
		__vector4 pos1 = vBase[vertIndex1].Pos;
		pos1 = __vmaddfp(UnpackPosition(vPos[i+1]),t,pos1);
		__vector4 pos2 = vBase[vertIndex2].Pos;
		pos2 = __vmaddfp(UnpackPosition(vPos[i+2]),t,pos2);
		__vector4 pos3 = vBase[vertIndex3].Pos;
		pos3 = __vmaddfp(UnpackPosition(vPos[i+3]),t,pos3);

		__vector4 nrm0 = vBase[vertIndex0].Nrm;
		nrm0 = __vmaddfp(UnpackNormal(vNrm[i]),t,nrm0);
		__vector4 nrm1 = vBase[vertIndex1].Nrm;
		nrm1 = __vmaddfp(UnpackNormal(vNrm[i+1]),t,nrm1);
		__vector4 nrm2 = vBase[vertIndex2].Nrm;
		nrm2 = __vmaddfp(UnpackNormal(vNrm[i+2]),t,nrm2);
		__vector4 nrm3 = vBase[vertIndex3].Nrm;
		nrm3 = __vmaddfp(UnpackNormal(vNrm[i+3]),t,nrm3);

		vBase[vertIndex0].Pos = pos0;
		vBase[vertIndex0].Nrm = nrm0;

		vBase[vertIndex1].Pos = pos1;
		vBase[vertIndex1].Nrm = nrm1;

		vBase[vertIndex2].Pos = pos2;
		vBase[vertIndex2].Nrm = nrm2;

		vBase[vertIndex3].Pos = pos3;
		vBase[vertIndex3].Nrm = nrm3;
	}

	for (; i < iCount; i++ )
	{
		int vertIndex0 = iMap[i];

		__vector4 pos0 = vBase[vertIndex0].Pos;
		pos0 = __vmaddfp(UnpackPosition(vPos[i]),t,pos0);

		__vector4 nrm0 = vBase[vertIndex0].Nrm;
		nrm0 = __vmaddfp(UnpackNormal(vNrm[i]),t,nrm0);

		vBase[vertIndex0].Pos = pos0;
		vBase[vertIndex0].Nrm = nrm0;
	}
}

void grbTargetData::BlendAddPos(Vector3::Vector3Param t_, grcVertexBuffer* outOffsets)
{
	__vector4 t = *(__vector4*)&t_;

	__vector4 * RESTRICT vBase = (__vector4*) outOffsets->GetFastLockPtr();

	int iCount = m_VertexMap.GetCount();

	// Check the blendshape is not empty
	// i.e there are no positions
	if (iCount == 0)
	{
		return;
	}

	const Position * RESTRICT vPos = m_Positions.GetElements();

	const int * RESTRICT iMap = m_VertexMap.GetElements();

	int i;
	for( i = 0; i < iCount-3; i+=4 )
	{
		// Prefetch the next eight inputs
		PrefetchDC(iMap+i+4);
		PrefetchDC(vPos+i+4);
		PrefetchDC(vBase+iMap[i+4]);

		int vertIndex0 = iMap[i];
		int vertIndex1 = iMap[i+1];
		int vertIndex2 = iMap[i+2];
		int vertIndex3 = iMap[i+3];

		vBase[vertIndex0] = __vmaddfp(UnpackPosition(vPos[i]),t,vBase[vertIndex0]);
		vBase[vertIndex1] = __vmaddfp(UnpackPosition(vPos[i+1]),t,vBase[vertIndex1]);
		vBase[vertIndex2] = __vmaddfp(UnpackPosition(vPos[i+2]),t,vBase[vertIndex2]);
		vBase[vertIndex3] = __vmaddfp(UnpackPosition(vPos[i+3]),t,vBase[vertIndex3]);
	}

	for (; i < iCount; i++ )
	{
		int vertIndex0 = iMap[i];

		vBase[vertIndex0] = __vmaddfp(UnpackPosition(vPos[i]),t,vBase[vertIndex0]);
	}
}

void grbTargetData::BlendAddPosNrmTan(Vector3::Vector3Param t_, grcVertexBuffer* outOffsets)
{
	struct Vertex {
		__vector4 Pos;
		__vector4 Nrm;
		__vector4 Tan;
	};
	__vector4 t = *(__vector4*)&t_;

	Vertex * RESTRICT vBase = (Vertex*) outOffsets->GetFastLockPtr();

	int iCount = m_VertexMap.GetCount();

	// Check the blendshape is not empty
	// i.e there are no positions, normals or tangents
	if (iCount == 0)
	{
		return;
	}

	const Position * RESTRICT vPos = m_Positions.GetElements();
	const Normal * RESTRICT vNrm = m_Normals.GetElements();
	const Normal * RESTRICT vTan = m_Tangents.GetElements();

	const int * RESTRICT iMap = m_VertexMap.GetElements();

	Assert(outOffsets->GetVertexStride() == 3 * 16);

	int i;
	for( i = 0; i < iCount-3; i+=4 )
	{
		// Prefetch the next eight inputs
		PrefetchDC(iMap+i+4);
		PrefetchDC(vPos+i+4);
		PrefetchDC(vNrm+i+4);
		PrefetchDC(vTan+i+4);
		PrefetchDC(vBase+iMap[i+4]);

		int vertIndex0 = iMap[i];
		int vertIndex1 = iMap[i+1];
		int vertIndex2 = iMap[i+2];
		int vertIndex3 = iMap[i+3];

		__vector4 pos0 = vBase[vertIndex0].Pos;
		pos0 = __vmaddfp(UnpackPosition(vPos[i]),t,pos0);
		__vector4 pos1 = vBase[vertIndex1].Pos;
		pos1 = __vmaddfp(UnpackPosition(vPos[i+1]),t,pos1);
		__vector4 pos2 = vBase[vertIndex2].Pos;
		pos2 = __vmaddfp(UnpackPosition(vPos[i+2]),t,pos2);
		__vector4 pos3 = vBase[vertIndex3].Pos;
		pos3 = __vmaddfp(UnpackPosition(vPos[i+3]),t,pos3);

		__vector4 nrm0 = vBase[vertIndex0].Nrm;
		nrm0 = __vmaddfp(UnpackNormal(vNrm[i]),t,nrm0);
		__vector4 nrm1 = vBase[vertIndex1].Nrm;
		nrm1 = __vmaddfp(UnpackNormal(vNrm[i+1]),t,nrm1);
		__vector4 nrm2 = vBase[vertIndex2].Nrm;
		nrm2 = __vmaddfp(UnpackNormal(vNrm[i+2]),t,nrm2);
		__vector4 nrm3 = vBase[vertIndex3].Nrm;
		nrm3 = __vmaddfp(UnpackNormal(vNrm[i+3]),t,nrm3);

		__vector4 tan0 = vBase[vertIndex0].Tan;
		tan0 = __vmaddfp(UnpackNormal(vTan[i]),t,tan0);
		__vector4 tan1 = vBase[vertIndex1].Tan;
		tan1 = __vmaddfp(UnpackNormal(vTan[i+1]),t,tan1);
		__vector4 tan2 = vBase[vertIndex2].Tan;
		tan2 = __vmaddfp(UnpackNormal(vTan[i+2]),t,tan2);
		__vector4 tan3 = vBase[vertIndex3].Tan;
		tan3 = __vmaddfp(UnpackNormal(vTan[i+3]),t,tan3);

		vBase[vertIndex0].Pos = pos0;
		vBase[vertIndex0].Nrm = nrm0;
		vBase[vertIndex0].Tan = tan0;

		vBase[vertIndex1].Pos = pos1;
		vBase[vertIndex1].Nrm = nrm1;
		vBase[vertIndex1].Tan = tan1;

		vBase[vertIndex2].Pos = pos2;
		vBase[vertIndex2].Nrm = nrm2;
		vBase[vertIndex2].Tan = tan2;

		vBase[vertIndex3].Pos = pos3;
		vBase[vertIndex3].Nrm = nrm3;
		vBase[vertIndex3].Tan = tan3;
	}

	for (; i < iCount; i++ )
	{
		int vertIndex0 = iMap[i];

		__vector4 pos0 = vBase[vertIndex0].Pos;
		pos0 = __vmaddfp(UnpackPosition(vPos[i]),t,pos0);

		__vector4 nrm0 = vBase[vertIndex0].Nrm;
		nrm0 = __vmaddfp(UnpackNormal(vNrm[i]),t,nrm0);

		__vector4 tan0 = vBase[vertIndex0].Tan;
		tan0 = __vmaddfp(UnpackNormal(vTan[i]),t,tan0);

		vBase[vertIndex0].Pos = pos0;
		vBase[vertIndex0].Nrm = nrm0;
		vBase[vertIndex0].Tan = tan0;
	}
}
#endif // __WIN32PC

template <typename _Type>
class VertexAttributeSorter
{
public:
	VertexAttributeSorter(const atArray<_Type>& vertexAttributes, const atArray<int>& vertexRemap)
	{
		sm_VertexAttributes = &vertexAttributes;
		sm_VertexRemap = &vertexRemap;
	}

	static int Sort(const void* _a, const void* _b)
	{
		const _Type* baseAttribute = sm_VertexAttributes->GetElements();
		const _Type* a = reinterpret_cast<const _Type*>(_a);
		const _Type* b = reinterpret_cast<const _Type*>(_b);
		Assert(a >= baseAttribute);
		Assert(b >= baseAttribute);
		int vertexIndexA = (*sm_VertexRemap)[(u32)(a - baseAttribute)];
		int vertexIndexB = (*sm_VertexRemap)[(u32)(b - baseAttribute)];
		if (vertexIndexA < vertexIndexB)
			return -1;
		else if (vertexIndexA == vertexIndexB)
			return 0;
		else
			return 1;
	}

private:
	VertexAttributeSorter operator=(const VertexAttributeSorter& rhs);

	static const atArray<_Type>* sm_VertexAttributes;
	static const atArray<int>* sm_VertexRemap;
};
template <typename _Type>
const atArray<_Type>* VertexAttributeSorter<_Type>::sm_VertexAttributes;
template <typename _Type>
const atArray<int>* VertexAttributeSorter<_Type>::sm_VertexRemap;

void grbTargetData::SortVertexAttributes()
{
	if (m_HasPositions)
	{
		VertexAttributeSorter<Position> sorter(m_Positions, m_VertexMap);
		qsort(m_Positions.GetElements(), m_Positions.GetCount(), sizeof(Position), &VertexAttributeSorter<Position>::Sort);
	}
	if (m_HasNormals)
	{
		VertexAttributeSorter<Normal> sorter(m_Normals, m_VertexMap);
		qsort(m_Normals.GetElements(), m_Normals.GetCount(), sizeof(Normal), &VertexAttributeSorter<Normal>::Sort);
	}
	if (m_HasTangents)
	{
		VertexAttributeSorter<Normal> sorter(m_Tangents, m_VertexMap);
		qsort(m_Tangents.GetElements(), m_Tangents.GetCount(), sizeof(Normal), &VertexAttributeSorter<Normal>::Sort);
	}

	std::sort(m_VertexMap.GetElements(), m_VertexMap.GetElements() + m_VertexMap.GetCount());
}

}

#endif // ENABLE_BLENDSHAPES
