// 
// grblendshapes/targetdata.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRBLENDSHAPES_TARGETDATA_H
#define GRBLENDSHAPES_TARGETDATA_H

#include "atl/array.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "grmodel/grmodel_config.h"

#include "blendshapes_config.h"

#if ENABLE_BLENDSHAPES
namespace rage
{

class grcVertexBuffer;
class grcFvf;

#define QUANTISE_BLENDWEIGHT_DATA	(1 || __RESOURCECOMPILER) // Use UK spelling to keep James happy

/* PURPOSE:
	A class containing blend target deltas for a section of the mesh
*/
class grbTargetData
{
	grbTargetData()	{}
public:
	static const int kPositionShift;
	static const float kPositionEpsilon;
	static const float kPositionEpsilon2;
#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
	static const float kNormalEpsilon;
	static const float kNormalEpsilon2;
#endif // USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS

	typedef u32 QuantisedNormal;
	typedef u64 QuantisedPosition;
	typedef const QuantisedNormal& QuantisedNormalParam;
	typedef const QuantisedPosition& QuantisedPositionParam;

#if QUANTISE_BLENDWEIGHT_DATA
	typedef QuantisedNormal Normal;
	typedef QuantisedPosition Position;
#else
	typedef Vector3 Normal;
	typedef Vector3 Position;
#endif // QUANTISE_BLENDWEIGHT_DATA

	grbTargetData(int vertexCount, const grcFvf* fvf);
	grbTargetData(class datResource&);
	~grbTargetData();
	DECLARE_PLACE(grbTargetData);

	/*	PURPOSE
			Compute offsets for the given t value and add them to the given offset buffer
		PARAMS
			t - the percentage of each delta to retrieve.  This should be between 0.0f and 1.0f
			outOffsets - a pointer to a grcVertexBuffer that will hold the computed offsets
	*/
	void BlendAdd(float t, grcVertexBuffer* outOffsets);

	/*	PURPOSE
			Get the nubmer of vertices in this target
		RETURNS
			An integer containing the number of vertices in this target
	*/
	int GetVertexCount() const;

	struct VertexPosDelta
	{
		Vector3	Position;
	};
	struct VertexPosNrmDelta : public VertexPosDelta
	{
		Vector3	Normal;
	};
	struct VertexPosNrmTanDelta : public VertexPosNrmDelta
	{
		Vector3 Tangent;
	};

	// PURPOSE: Apply the deltas
	void SetDeltas(const atArray<VertexPosNrmTanDelta>& deltas);

	bool HasPositions() const { return m_HasPositions; }
	bool HasNormals() const { return m_HasNormals; }
	bool HasTangents() const { return m_HasTangents; }

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

private:
	enum BlendFunctionID
	{
		bfidPos = 0,
		bfidPosNrm,
		bfidPosNrmTan,
		
		bfidCount
	};

	typedef void (grbTargetData::*SpecializedBlendFunction)(Vector3::Vector3Param t, grcVertexBuffer* outOffsets);
	static SpecializedBlendFunction sm_BlendFunctions[bfidCount];

	void BlendAddPosNrm(Vector3::Vector3Param t, grcVertexBuffer* outOffsets);
	void BlendAddPos(Vector3::Vector3Param t, grcVertexBuffer* outOffsets);
	void BlendAddPosNrmTan(Vector3::Vector3Param t, grcVertexBuffer* outOffsets);

	void SortVertexAttributes();

	u32					m_TotalMeshVerts;
	bool				m_HasPositions;
	bool				m_HasNormals;
	bool				m_HasTangents;
	char				m_Pad;

	atArray<Position>	m_Positions;
	atArray<Normal>		m_Normals;
	atArray<Normal>		m_Tangents;
	atArray<int>		m_VertexMap;
};

} // namespace rage

#endif // ENABLE_BLENDSHAPES

#endif	// GRBLENDSHAPES_TARGETDATA_H
