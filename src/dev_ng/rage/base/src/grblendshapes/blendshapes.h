// 
// grblendshapes/blendshapes.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef GRBLENDSHAPES_BLENDSHAPES_H
#define GRBLENDSHAPES_BLENDSHAPES_H

#include "atl/array.h"
#include "data/struct.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

#include "blendshapes_config.h"

#if ENABLE_BLENDSHAPES

namespace rage 
{

class fiTokenizer;
class grbMorphable;
class grbTargetManager;
class grcFvf;
class rmcDrawable;

/* PURPOSE:
	The main blendshape interface, provides access to blending functionality
*/
class grbBlendShape
{
	grbBlendShape()		{}
public:
	grbBlendShape(grbTargetManager* manager, fiTokenizer *T,rmcDrawable *pDrawable);
	grbBlendShape(class datResource &);
	~grbBlendShape();
	DECLARE_PLACE(grbBlendShape);

	/*	PURPOSE
			Resets all the morphable offsets to zero
	*/
	void Reset();

	/*	PURPOSE
			Validate offset vertex buffer "fast lock" pointers
	*/
	void Prepare();

	/*	PURPOSE
			Validate offset vertex buffer "fast unlock" pointers
	*/
	void Release();

	/*	PURPOSE
			Get the number of morphables in this blendshape
		RETURNS
			The number of morphables in this blendshape
	*/
	int GetMorphableCount() const									{ return m_Morphs.GetCount(); }

	/*	PURPOSE
			Get the morphable for the specified index
		PARAMS
			morphableIndex - the index of the morphable to retrieve
		RETURNS
			A pointer to the morphable specified by morphableIndex
	*/
	const grbMorphable* GetMorphable(int morphableIndex) const		{ return m_Morphs[morphableIndex]; }
	grbMorphable* GetMorphable(int morphableIndex)					{ return m_Morphs[morphableIndex]; }

	/*	PURPOSE
			Get lod group this blend shape applies to
		RETURNS
			An integer containing the index of the lod group this blend shape applies to
	*/
	int GetLodGroup() const											{ return m_LodGroup; }

	/*	PURPOSE
			Get the model index this blend shape applies to
		RETURNS
			An integer containing the index of the model this blend shape applies to
	*/
	int GetModelIndex() const										{ return m_ModelIndex; }

	/*	PURPOSE
			Get the name of this blendshape
		RETURNS
			A pointer to a null terminated string containing the name of this blendshape
	*/
	const char* GetName() const										{ return m_Name; }

	// PURPOSE: Swap the buffers
	void SwapBuffers();

	// PURPOSE: Check whether or not we're using Edge
	bool IsUsingEdge() const;

	// PURPOSE: Return how many buffers we need
	u32 GetNumMorphableBuffers() const;

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

private:
	char								m_Name[128];
	int									m_LodGroup;
	int									m_ModelIndex;
	atArray<datOwner<grbMorphable> >	m_Morphs;
	datRef<grbTargetManager>			m_Manager;

};

}

#endif // ENABLE_BLENDSHAPES

#endif // BLENDSHAPES_H
