// 
// grblendshapes/blendshapes.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "grblendshapes/blendshapes.h"

#include "grblendshapes/morphable.h"
#include "grblendshapes/target.h"
#include "grblendshapes/targetData.h"
#include "grblendshapes/manager.h"
#include "grmodel/model.h"		// for edge hackery
#include "grmodel/geometry.h"		// for edge hackery
#include "rmcore/drawable.h"

#include "atl/array_struct.h"
#include "data/resourcehelpers.h"
#include "data/struct.h"
#include "file/token.h"
#include "diag/tracker.h"
#include "system/memory.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "system/param.h"

#if ENABLE_BLENDSHAPES

// XPARAM(vertexopt);

namespace rage
{

IMPLEMENT_PLACE(grbBlendShape)

#if MESH_LIBRARY
grbBlendShape::grbBlendShape(grbTargetManager* manager, fiTokenizer* T,rmcDrawable *pDrawable)
{
	RAGE_TRACK(BlendShapeBegin);

	m_Manager = manager;

	// Get the mesh filename
	T->GetToken(m_Name, sizeof(m_Name));

	// Get the lod group index
	m_LodGroup = T->GetInt();
	
	// Get the model index
	m_ModelIndex = T->GetInt();

#if __RESOURCECOMPILER || __PS3
	// Get the actual model so we can get at the blend data.
	grmModel &model = *pDrawable->GetLodGroup().GetLod(LOD_HIGH).GetModel(m_ModelIndex);
#endif

	// Load the blend target data
	{
		// Load the mesh on the temp heap
		sysMemStartTemp();
		mshMesh* mesh = rage_new mshMesh;
		bool success = SerializeFromFile(m_Name,*mesh,"mesh");
		sysMemEndTemp();
		if( success )
		{
			sysMemStartTemp();
			grmModel::Prepare(*mesh,&pDrawable->GetShaderGroup());
			sysMemEndTemp();

			int targetCount = mesh->GetBlendTargetCount();

			// Create a morphable for each material
			int materialCount = mesh->GetMtlCount();
			m_Morphs.Reserve(materialCount);
			for( int i = 0; i < materialCount; i++ )
			{
				const mshMaterial& mtl = mesh->GetMtl(i);

				// Create the morphable and add it to the list
				grbMorphable* pMorph = rage_new grbMorphable(this, i);
				m_Morphs.Grow() = pMorph;

				// Load the data for all the targets
#if __RESOURCECOMPILER || __PS3
				if (model.GetGeometry(i).GetType() == grmGeometry::GEOMETRYEDGE && ((grmGeometryEdge&)model.GetGeometry(i)).GetBlendHeader(0))
					pMorph->InitEdge((grmGeometryEdge&)model.GetGeometry(i));
				else
#endif
					pMorph->Init(targetCount, mtl);
			}

			// Create a new blend target object for each target
			for( int i = 0; i < targetCount; i++ )
			{
				// Create the target
				grbTarget* pTarget = m_Manager->CreateBlendTarget(mesh->GetBlendTargetName(i));
				Assert(pTarget);

				// Setup the target
				pTarget->SetTargetIndex(i);
				pTarget->AllocateMorphables(materialCount);
				for( int j = 0; j < materialCount; j++ )
				{
					pTarget->AddMorphable(m_Morphs[j]);
				}
			}
		}

		// Delete the temp mesh
		sysMemStartTemp();
		delete mesh;
		sysMemEndTemp();
	}
	RAGE_TRACK(BlendShapeEnd);
}
#endif

grbBlendShape::grbBlendShape(class datResource& rsc) : m_Morphs(rsc, true)
{
}

grbBlendShape::~grbBlendShape()
{
	for( int i = 0; i < m_Morphs.GetCount(); i++ )
	{
		delete m_Morphs[i];
	}
}

void grbBlendShape::Prepare()
{
	for( int i = 0; i < m_Morphs.GetCount(); i++ )
	{
		m_Morphs[i]->Prepare();
	}
}

void grbBlendShape::Release()
{
	for( int i = 0; i < m_Morphs.GetCount(); i++ )
	{
		m_Morphs[i]->Release();
	}
}

void grbBlendShape::Reset()
{
	for( int i = 0; i < m_Morphs.GetCount(); i++ )
	{
		m_Morphs[i]->Zero();
	}
}

#if __DECLARESTRUCT
void grbBlendShape::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(grbBlendShape);
	STRUCT_CONTAINED_ARRAY(m_Name);
	STRUCT_FIELD(m_LodGroup);
	STRUCT_FIELD(m_ModelIndex);
	STRUCT_FIELD(m_Morphs);
	STRUCT_FIELD(m_Manager);
	STRUCT_END();
}
#endif

void grbBlendShape::SwapBuffers()
{
	for( int i = 0; i < m_Morphs.GetCount(); i++ )
	{
		m_Morphs[i]->SwapBuffers();
	}
}

bool grbBlendShape::IsUsingEdge() const
{
	bool isUsingEdge = false;
	for( int i = 0; i < m_Morphs.GetCount() && !isUsingEdge; i++ )
	{
		isUsingEdge = m_Morphs[i]->IsUsingEdge();
	}
	return isUsingEdge;
}

u32 grbBlendShape::GetNumMorphableBuffers() const
{
	u32 numMorphables = 0;
	for( int i = 0; i < m_Morphs.GetCount(); i++ )
	{
		numMorphables = Max(numMorphables, m_Morphs[i]->GetNumMorphableBuffers());
	}
	return numMorphables;
}
} // namespace rage

#endif // ENABLE_BLENDSHAPES

