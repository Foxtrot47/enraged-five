// 
// grblendshapes/morphable.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "grblendshapes/morphable.h"

#include "grblendshapes/blendshapes.h"
#include "grblendshapes/targetdata.h"

#include "atl/array_struct.h"
#include "data/resourcehelpers.h"
#include "file/token.h"
#include "grcore/device.h"
#include "grcore/fvf.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/grmodel_config.h"
#include "system/memory.h"
#include "diag/tracker.h"

#if ENABLE_BLENDSHAPES

#if __PS3
#define ISUSINGEDGE Likely(IsUsingEdge())
#else
#define ISUSINGEDGE Unlikely(IsUsingEdge())
#endif // __PS3

namespace rage
{

bool HasNormalDeltas(const mshMaterial& mtl, int targetCount);
bool HasTangentDeltas(const mshMaterial& mtl, int targetCount);
bool HasAnyDeltas(const mshMaterial& mtl, int target);

void grbMorphable::SwapBuffers()
{
	if (ISUSINGEDGE) {
		m_DrawBufferIdx = m_UpdateBufferIdx;
		++m_UpdateBufferIdx %= GetNumMorphableBuffers();
		return;
	}
	else if (!m_Offsets[m_DrawBufferIdx])
		return;

	m_DrawBufferIdx = m_UpdateBufferIdx;
	++m_UpdateBufferIdx %= GetNumMorphableBuffers();
}

grbMorphable::grbMorphable(grbBlendShape* parent, int materialIndex)
: m_Parent(parent) // Remember our parent
, m_MaterialIndex(materialIndex)
, m_Fvf(0) // Initialize flexible vertex format to zero because its required to be set externally
, m_DrawBufferIdx(0)
, m_UpdateBufferIdx(m_DrawBufferIdx + 1)
, m_IsResource(false) // Set to false initially so that GetNumMorphableBuffers returns the number we want
{
	// Clear all the offset buffer pointers
	m_Offsets.Resize(GetNumMorphableBuffers());
	for( u32 i = 0; i < GetNumMorphableBuffers(); i++ )
	{
		m_Offsets[i] = 0;
	}

	// Really set whether or not this morphable is resourced
	m_IsResource = __RESOURCECOMPILER != 0;
}

grbMorphable::grbMorphable(class datResource& rsc)
: m_Targets(rsc, true)
, m_Offsets(rsc, true)
, m_DrawBufferIdx(0)
, m_UpdateBufferIdx(m_DrawBufferIdx + 1)
, m_BlendHeaders(rsc, true)
, m_IsResource(true) // Just to make sure, because we weren't setting this properly on non-PS3 code paths for a while
{
}

grbMorphable::~grbMorphable()
{
	for( int i = 0; i < m_Targets.GetCount(); i++ )
		delete m_Targets[i];

	for( int i = 0; i < m_Offsets.GetCount(); i++ )
		delete m_Offsets[i];

	delete m_Fvf;

	if (m_IsResource)
	{
		for (int i=0; i<m_BlendHeaders.GetCount(); i++)
			delete [] m_BlendHeaders[i];
	}
}

#if __RESOURCECOMPILER || __PS3
void grbMorphable::InitEdge(grmGeometryEdge &edgeGeom)
{
	// Copy from temporary storage into our own buffer
	m_BlendHeaders.Reserve(edgeGeom.GetBlendHeaderCount());
	for (int i=0; i<edgeGeom.GetBlendHeaderCount(); i++) {
		grmGeometryEdgeBlendHeader *bh = edgeGeom.GetBlendHeader(i);
#if __RESOURCECOMPILER
		if (bh) {
			m_BlendHeaders.Append() = (grmGeometryEdgeBlendHeader*) rage_aligned_new(16) u8[bh->BlendSize];
			memcpy(m_BlendHeaders[i], bh, bh->BlendSize);
			// Relocate pointers after copy
			ptrdiff_t delta = (char*)m_BlendHeaders[i].ptr - (char*)bh;
			for (int j=0; j<bh->BlendCount; j++) {
				char* addr = (char*)m_BlendHeaders[i]->Blends[j].Addr;
				m_BlendHeaders[i]->Blends[j].Addr = (addr?addr + delta:0);
			}
		}
		else
			m_BlendHeaders.Append() = NULL;
#else
		m_BlendHeaders.Append() = bh;
#endif // __RESOURCECOMPILER
	}

#if __RESOURCECOMPILER
	// Free the header in the original object
	edgeGeom.FreeBlendHeaders();
#endif // __RESOURCECOMPILER
}
#endif

#if MESH_LIBRARY
void grbMorphable::Init(int numTargets, const mshMaterial& mtl)
{
	bool bHasNormals = HasNormalDeltas(mtl, numTargets);
	bool bHasTangents = HasTangentDeltas(mtl, numTargets);

	// Create the fvf
	m_Fvf = rage_new grcFvf();
	m_Fvf->SetDynamicOrder(1);
	m_Fvf->SetPosChannel(true);
	m_Fvf->SetNormalChannel(bHasNormals);
	m_Fvf->SetTangentChannel(0, bHasTangents);

	// Allocate the space in the array of targets
	m_Targets.Resize(numTargets);

	// Create the target data objects
	m_VertexCount = mtl.GetVertexCount();
	bool createOffsetBuffers = false;
	for( int i = 0; i < numTargets; i++ )
	{
		grbTargetData* pData = 0;
		if( HasAnyDeltas(mtl, i) )
		{
			createOffsetBuffers = true;
			pData = rage_new grbTargetData(m_VertexCount, m_Fvf);

			sysMemStartTemp();
			atArray<grbTargetData::VertexPosNrmTanDelta> deltas;			
			deltas.Resize(m_VertexCount);
			sysMemEndTemp();

			// Grab all the deltas for this target
            for( int j = 0; j < m_VertexCount; j++ )
			{
				Convert(deltas[j].Position,	mtl.GetVertex(j).BlendTargetDeltas[i].Position);
				Convert(deltas[j].Normal,	mtl.GetVertex(j).BlendTargetDeltas[i].Normal);
				Convert(deltas[j].Tangent,	mtl.GetVertex(j).BlendTargetDeltas[i].Tangent);
			}

			// Set them in the target data
			pData->SetDeltas(deltas);
			
			sysMemStartTemp();
			deltas.Reset();
			sysMemEndTemp();
		}
	
		m_Targets[i] = pData;
	}

	// Create the offset buffers
	if (createOffsetBuffers)
		CreateOffsetBuffer();
}
#endif

void grbMorphable::Prepare()
{
	if (ISUSINGEDGE || !m_Offsets[m_UpdateBufferIdx])
		return;

	AssertVerify(m_Offsets[m_UpdateBufferIdx]->Lock((MORPH_DYNAMIC_BUFFER) ? grcsDiscard : 0));
}

void grbMorphable::Release()
{
	if (ISUSINGEDGE || !m_Offsets[m_UpdateBufferIdx])
		return;

	m_Offsets[m_UpdateBufferIdx]->UnlockRW();
}

void grbMorphable::Zero()
{
	if (ISUSINGEDGE || !m_Offsets[m_UpdateBufferIdx])
		return;

	grcVertexBufferEditor editor(m_Offsets[m_UpdateBufferIdx]);
	editor.Zero();
}

void grbMorphable::BlendTargetAdd(int target, float t)
{
	if (ISUSINGEDGE) {
		for (int i=0; i<m_BlendHeaders.GetCount(); i++) {
			if (m_BlendHeaders[i]) {
				Assert(target < m_BlendHeaders[i]->BlendCount);

				if (m_BlendHeaders[i]->Blends[target].Addr)
				{
					grmGeometryEdgeBlend& blend = m_BlendHeaders[i]->Blends[target];
					Assert(m_UpdateBufferIdx < NELEM(blend.Alpha));
					blend.Alpha[m_UpdateBufferIdx] = t;
				}
			}
		}
		//Displayf("target %d alpha %f",target,t);
		return;
	}
	else if (!m_Offsets[m_UpdateBufferIdx])
		return;

	Assert(target < m_Targets.GetCount());

	grbTargetData* pTarget = m_Targets[target];
	if( pTarget )
	{
		pTarget->BlendAdd(t, m_Offsets[m_UpdateBufferIdx]);
	}
}

void grbMorphable::BlendTargets(int numTargets, const int* targets, const float* ts)
{
	if (ISUSINGEDGE || !m_Offsets[m_UpdateBufferIdx])
		return;

	grcVertexBufferEditor editor(m_Offsets[m_UpdateBufferIdx]);
	editor.Zero();

	for( int i = 0; i < numTargets; i++ )
	{
		Assert(targets[i] < m_Targets.GetCount());
		grbTargetData* pTarget = m_Targets[targets[i]];
		if( pTarget )
		{
			pTarget->BlendAdd(ts[i], m_Offsets[m_UpdateBufferIdx]);
		}
	}
}

void grbMorphable::SetFvf(const grcFvf* pFvf)
{
	delete m_Fvf;

	m_Fvf = rage_new grcFvf();
	m_Fvf->SetDynamicOrder(1);

	u32 traitsMask = 0;
	const u32 kHasPositionsTrait = 1 << 0;
	const u32 kHasNormalsTrait = 1 << 1;
	const u32 kHasTangentsTrait = 1 << 2;
	const u32 kHasAllTraits = kHasPositionsTrait | kHasNormalsTrait | kHasTangentsTrait;

	for (int i = 0; i < m_Targets.GetCount() && traitsMask != kHasAllTraits; ++i)
	{
		if (m_Targets[i])
		{
			if (m_Targets[i]->HasPositions())
				traitsMask |= kHasPositionsTrait;
			if (m_Targets[i]->HasNormals())
				traitsMask |= kHasNormalsTrait;
			if (m_Targets[i]->HasTangents())
				traitsMask |= kHasTangentsTrait;
		}
	}
	m_Fvf->SetPosChannel(pFvf->GetPosChannel() && (traitsMask & kHasPositionsTrait) != 0);
	m_Fvf->SetNormalChannel(pFvf->GetNormalChannel() && (traitsMask & kHasNormalsTrait) != 0, grcFvf::grcdsFloat3);
	m_Fvf->SetDiffuseChannel(pFvf->GetDiffuseChannel());
	m_Fvf->SetTangentChannel(0, pFvf->GetTangentChannel(0) && (traitsMask & kHasTangentsTrait) != 0, grcFvf::grcdsFloat4);

	// Create the offset buffer
	if (traitsMask != 0)
		CreateOffsetBuffer();
}

void grbMorphable::CreateOffsetBuffer()
{
	for( int i = 0; i < m_Offsets.GetCount(); i++ )
	{
		if( !m_Offsets[i] )
		{
			Assert(m_Fvf);
			const bool bReadWrite = true;
			const bool bDynamic = (MORPH_DYNAMIC_BUFFER) ? true : false;
			m_Offsets[i] = grcVertexBuffer::Create(GetVertexCount(), *m_Fvf, bReadWrite, bDynamic, NULL);
			Assert(m_Offsets[i]);
			{
				grcVertexBufferEditor editor(m_Offsets[i]);
				editor.Zero();
			}
			if (!bDynamic)
			{
				m_Offsets[i]->MakeReadOnly();
			}
		}
	}
}

#if __DECLARESTRUCT
void grbMorphable::DeclareStruct(datTypeStruct &s)
{
	SSTRUCT_BEGIN(grbMorphable)
	SSTRUCT_CONTAINED_ARRAY(grbMorphable, m_Name)
	SSTRUCT_FIELD(grbMorphable, m_MaterialIndex)
	SSTRUCT_FIELD(grbMorphable, m_Targets)
	SSTRUCT_FIELD(grbMorphable, m_Parent)
	SSTRUCT_FIELD(grbMorphable, m_Offsets)
	SSTRUCT_FIELD(grbMorphable, m_Fvf)
	SSTRUCT_FIELD(grbMorphable, m_VertexCount)
	SSTRUCT_FIELD(grbMorphable, m_DrawBufferIdx)
	SSTRUCT_FIELD(grbMorphable, m_UpdateBufferIdx)
	SSTRUCT_FIELD(grbMorphable, m_IsResource)
	SSTRUCT_CONTAINED_ARRAY(grbMorphable, m_Pad)
	SSTRUCT_FIELD(grbMorphable, m_BlendHeaders)
	SSTRUCT_END(grbMorphable)
}
#endif

IMPLEMENT_PLACE(grbMorphable);

#if MESH_LIBRARY

#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
bool HasNormalDeltas(const mshMaterial& mtl, int targetCount)
#else
bool HasNormalDeltas(const mshMaterial& UNUSED_PARAM(mtl), int UNUSED_PARAM(targetCount))
#endif
{
#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
	for( int target = 0; target < targetCount; target++ )
	{
		for( int i = 0; i < mtl.GetVertexCount(); i++ )
		{
			if( mtl.GetVertex(i).BlendTargetDeltas[target].Normal.Mag2() > grbTargetData::kNormalEpsilon2 )
			{
				return true;
			}
		}
	}
#endif // USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS

	return false;
}

#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
bool HasTangentDeltas(const mshMaterial& mtl, int targetCount)
#else
bool HasTangentDeltas(const mshMaterial& UNUSED_PARAM(mtl), int UNUSED_PARAM(targetCount))
#endif // USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
{
#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
	for( int target = 0; target < targetCount; target++ )
	{
		for( int i = 0; i < mtl.GetVertexCount(); i++ )
		{
			if( mtl.GetVertex(i).BlendTargetDeltas[target].Tangent.Mag2() > grbTargetData::kNormalEpsilon2 )
			{
				return true;
			}
		}
	}
#endif // USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS

	return false;
}

bool HasAnyDeltas(const mshMaterial& mtl, int target)
{
	for( int i = 0; i < mtl.GetVertexCount(); i++ )
	{
		if( mtl.GetVertex(i).BlendTargetDeltas[target].Position.Mag2() > grbTargetData::kPositionEpsilon2
#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
			|| mtl.GetVertex(i).BlendTargetDeltas[target].Normal.Mag2() > grbTargetData::kNormalEpsilon2
			|| mtl.GetVertex(i).BlendTargetDeltas[target].Tangent.Mag2() > grbTargetData::kNormalEpsilon2
#endif // USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
			)
		{
			return true;
		}
	}

	return false;
}

#endif //MESH_LIBRARY

u32 grbMorphable::GetNumMorphableBuffers() const
{
	// If the morphable is resourced, then use the offset array count, so that resources are backwards-compatible
	if (Likely(m_IsResource))
	{
		return m_Offsets.GetCount();
	}
	else
	{
#if HACK_GTA4 || HACK_MC4 || HACK_RDR2
		return 3;
#else
		if (ISUSINGEDGE)
			return 3;
		else
			return 2;
#endif // HACK_GTA4 || HACK_MC4
	}
}

} // namespace rage

#endif // ENABLE_BLENDSHAPES



