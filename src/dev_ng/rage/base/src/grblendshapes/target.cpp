//  
// grblendshapes/target.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "grblendshapes/target.h"

#include "atl/array_struct.h"

#if ENABLE_BLENDSHAPES

namespace rage
{

grbTarget::grbTarget(const char* name)
{
	m_TargetIndex = 0;
	strncpy(m_Name, name, sizeof(m_Name));
	
}

grbTarget::grbTarget(datResource& rsc) : m_Morphables(rsc, true)
{
}

grbTarget::~grbTarget()
{
}

IMPLEMENT_PLACE(grbTarget);

void grbTarget::SetTargetIndex(int targetIndex)
{
	m_TargetIndex = targetIndex;
}

void grbTarget::AllocateMorphables(int morphableCount)
{
	m_Morphables.Reserve(morphableCount);
}

void grbTarget::AddMorphable(grbMorphable* morphable)
{
	m_Morphables.Grow() = morphable;
}

void grbTarget::Blend(float blendPercent)
{
	for( int i = 0; i < m_Morphables.GetCount(); i++ )
	{
		m_Morphables[i]->BlendTargetAdd(m_TargetIndex, blendPercent);
	}
}

#if __DECLARESTRUCT
void grbTarget::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(grbTarget);
	STRUCT_CONTAINED_ARRAY(m_Name);
	STRUCT_FIELD(m_TargetIndex);
	STRUCT_FIELD(m_Morphables);
	STRUCT_END();
}
#endif

} // namespace rage

#endif // ENABLE_BLENDSHAPES
