// 
// grblendshapes/manager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "grblendshapes/manager.h"
#include "grblendshapes/target.h"
#include "grblendshapes/blendshapes.h"

#include "atl/array_struct.h"
#include "atl/map_struct.h"
#include "atl/string.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "rmcore/typefileparser.h"

#if ENABLE_BLENDSHAPES

namespace rage
{

grbTargetManager::grbTargetManager()
: pgBaseRefCounted(1)
{
}

grbTargetManager::grbTargetManager(datResource& rsc) : m_BlendShapes(rsc, true), m_Targets(rsc), pgBaseRefCounted(rsc)
{
	atMap<u16, datOwner<grbTarget> >::Iterator entry = m_Targets.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		entry.GetData().Place(&entry.GetData(),rsc);
	}
}

grbTargetManager::~grbTargetManager()
{
	for (int i=0; i<m_BlendShapes.GetCount(); i++)
		delete m_BlendShapes[i];

	atMap<u16, datOwner<grbTarget> >::Iterator entry = m_Targets.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		delete entry.GetData();
	}
}

IMPLEMENT_PLACE(grbTargetManager);

#if MESH_LIBRARY
grbTarget* grbTargetManager::CreateBlendTarget(const char* targetName)
{
	u16 targetID = crSkeletonData::ConvertBoneNameToId(targetName);

	grbTarget* pTarget = rage_new grbTarget(targetName);

	sysMemStartTemp();
	m_Targets.Insert(targetID, pTarget);
	sysMemEndTemp();

	return pTarget;
}

void grbTargetManager::LoadBlendShapes( rmcTypeFileCbData *data )
{
	fiTokenizer *T = data->m_T;
	T->MatchToken("{");

	CreateBlendShape(T);

	T->MatchToken("}");
}

bool grbTargetManager::Load(const char *basename, rmcDrawable *pDrawable)
{
	bool result = false;
	fiStream *S = ASSET.Open(basename, "type");
	m_Drawable = pDrawable;
	if (S) 
	{
		fiTokenizer T;
		T.Init(basename,S);

		// Register types
		rmcTypeFileParser defaultParser;
		defaultParser.RegisterLoader( "blendshapes", "blendshape", datCallback(MFA1(grbTargetManager::LoadBlendShapes), this, 0, true));

		result = defaultParser.ProcessTypeFile( T );
		S->Close();

		sysMemStartTemp();
		atMap<u16, datOwner<grbTarget> > tmpTargets(m_Targets);
		m_Targets.Kill();
		sysMemEndTemp();

		m_Targets = tmpTargets;

		sysMemStartTemp();
		tmpTargets.Kill();
		sysMemEndTemp();
	}
	m_Drawable = NULL;
	return result;
}

grbBlendShape* grbTargetManager::CreateBlendShape(fiTokenizer* T)
{
	grbBlendShape* pBlendShape = rage_new grbBlendShape(this, T, m_Drawable);
	m_BlendShapes.Grow() = pBlendShape;

	return pBlendShape;
}
#endif

void grbTargetManager::ResetBlendShapeOffsets()
{
	for( int i = 0; i < m_BlendShapes.GetCount(); i++ )
	{
		m_BlendShapes[i]->Reset();
	}
}

void grbTargetManager::PrepareBlendShapeOffsets()
{
	for( int i = 0; i < m_BlendShapes.GetCount(); i++ )
	{
		m_BlendShapes[i]->Prepare();
	}
}

void grbTargetManager::ReleaseBlendShapeOffsets()
{
	for( int i = 0; i < m_BlendShapes.GetCount(); i++ )
	{
		m_BlendShapes[i]->Release();
	}
}

void grbTargetManager::SwapBuffers()
{
	for( int i = 0; i < m_BlendShapes.GetCount(); i++ )
	{
		m_BlendShapes[i]->SwapBuffers();
	}
}

bool grbTargetManager::IsUsingEdge() const
{
	bool isUsingEdge = false;
	for( int i = 0; i < m_BlendShapes.GetCount() && !isUsingEdge; i++ )
	{
		isUsingEdge = m_BlendShapes[i]->IsUsingEdge();
	}
	return isUsingEdge;
}

void grbTargetManager::BlendTarget(u16 targetID, float blendPercent)
{
	datOwner<grbTarget>* t = m_Targets.Access(targetID);
	Assert(t);
	if( t )
	{
		grbTarget* pTarget = t->ptr;
		Assert(pTarget);
		if( pTarget )
		{
			pTarget->Blend(blendPercent);
		}
	}
}

u32 grbTargetManager::GetNumMorphableBuffers() const
{
	u32 numMorphables = 0;
	for( int i = 0; i < m_BlendShapes.GetCount(); i++ )
	{
		numMorphables = Max(numMorphables, m_BlendShapes[i]->GetNumMorphableBuffers());
	}
	return numMorphables;
}

#if __DECLARESTRUCT
void grbTargetManager::DeclareStruct(datTypeStruct &s)
{
	pgBaseRefCounted::DeclareStruct(s);
	STRUCT_BEGIN(grbTargetManager);
	STRUCT_FIELD(m_BlendShapes);
	STRUCT_FIELD(m_Targets);
	STRUCT_IGNORE(m_Drawable);
	STRUCT_END();
}
#endif

}	// namespace rage

#endif // ENABLE_BLENDSHAPES
