// 
// grblendshapes/morphable.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRBLENDSHAPES_MORPHABLE_H
#define GRBLENDSHAPES_MORPHABLE_H

#include "atl/array.h"
#include "data/struct.h"
#include "grcore/vertexbuffer.h"
#include "grmodel/geometry.h"
#include "mesh/mesh.h"

#include "blendshapes_config.h"

#if ENABLE_BLENDSHAPES

#if HACK_GTA4
	#define MORPH_DYNAMIC_BUFFER (0)	//!__WIN32PC)
#else
	#define MORPH_DYNAMIC_BUFFER (!__WIN32PC)
#endif

namespace rage
{

class fiTokenizer;
class grbBlendShape;
class grbTargetData;
class grcFvf;

/* PURPOSE:
	A container class to manage blend targets of an individual submesh
*/
class grbMorphable
{
	grbMorphable() {}
public:
	grbMorphable(grbBlendShape* parent, int materialIndex);
	grbMorphable(class datResource &);
	~grbMorphable();

	DECLARE_PLACE(grbMorphable);


	/*	PURPOSE
			Creates the target data from the mshMaterial that is passed in
		PARAMS
			numTargets - the number of targets to create
			mtl - the mshMaterial containing vertex deltas
	*/
	void Init(int numTargets, const mshMaterial& mtl);

	void InitEdge(grmGeometryEdge &edgeGeom);

	/*	PURPOSE
			Zeros all the offsets in the offsetbuffer
	*/
	void Zero();

	/*	PURPOSE
			Prepares (locks) the next-build offset buffer
	*/
	void Prepare();

	/*	PURPOSE
			Releases (unlocks) the next-build offset buffer
	*/
	void Release();

	/*	PURPOSE
			Get the name of this morphable
		RETURNS
			A null terminated string containing the name of this morphable
	*/
	const char* GetName() const							{ return m_Name; }

	/*	PURPOSE
			Get the number of vertices this morphable is responsible for
		RETURNS
			An integer containing the number of vertices this morphable contains
	*/
	int GetVertexCount() const							{ return m_VertexCount; }

	/*	PURPOSE
			Get the material index this morphable represents
		RETURNS
			An integer containing the the material index this morphable represents
	*/
	int GetMaterialIndex() const						{ return m_MaterialIndex; }

	/*	PURPOSE
			Get the offset buffer containing the current blend offsets
		RETURNS
			A pointer to a grcVertexBuffer containing the current blend offsets
	*/
	const grcVertexBuffer* GetDrawBuffer() const		{ return m_Offsets[m_DrawBufferIdx]; }
	grcVertexBuffer* GetDrawBuffer()					{ return m_Offsets[m_DrawBufferIdx]; }
	const grcVertexBuffer* GetUpdateBuffer() const		{ return m_Offsets[m_UpdateBufferIdx]; }
	grcVertexBuffer* GetUpdateBuffer()					{ return m_Offsets[m_UpdateBufferIdx]; }

	/*	PURPOSE
			Get the number of blend targets contained in this morphable
		RETURNS
			An integer containing the number of blend targets
	*/
	int GetTargetCount() const							{ return m_Targets.GetCount(); }

	/*	PURPOSE
			Perform a single blend to the specified target at the specified percentage and addds it to the exisitng blend
		PARAMS
			target - the index of the target to blend to
			t - the percentage to blend. This value should be between 0.0f and 1.0f
	*/
	void BlendTargetAdd(int target, float t);

	/*	PURPOSE
			Perform a composite blend to the specified targets at the specified percentages
		PARAMS
			numTargets - the number of targets to blend to
			targets - the indices of the targets to blend to
			ts - the percentages to blend. These values should be between 0.0f and 1.0f
	*/
	void BlendTargets(int numTargets, const int* targets, const float* ts);

	/*	PURPOSE
			Specify the Fvf of the internal offset buffer
		PARAMS
			pFvf - a pointer to a grcFvf object describing the desired format of the internal offset buffer
	*/
	void SetFvf(const grcFvf* pFvf);

	// PURPOSE: Swap the morphable offset buffers
	void SwapBuffers();

	// PURPOSE: Get the morphable draw offset buffer index
	int GetDrawBufferIdx() const { return m_DrawBufferIdx; }

	// PURPOSE: Get the morphable build offset buffer index
	int GetUpdateBufferIdx() const { return m_UpdateBufferIdx; }

	typedef atArray<datOwner<grmGeometryEdgeBlendHeader> > BlendHeaders;
	const BlendHeaders& GetBlendHeaders() const { return m_BlendHeaders; }

	// PURPOSE: Check whether or not we're using Edge
	bool IsUsingEdge() const { return m_BlendHeaders.GetCount() > 0; }

	// PURPOSE: Return how many buffers we need
	u32 GetNumMorphableBuffers() const;

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

private:
	// PURPOSE: Explicitly create the offset buffer - useful for building resources
	void CreateOffsetBuffer();

	char								m_Name[64];
	int									m_MaterialIndex;
	atArray<datOwner<grbTargetData> >	m_Targets;
	datRef<grbBlendShape>				m_Parent;
	atArray<datOwner<grcVertexBuffer> >	m_Offsets;
	datOwner<grcFvf>					m_Fvf;
	int									m_VertexCount;
	int									m_DrawBufferIdx;
	int									m_UpdateBufferIdx;
	bool								m_IsResource;
	u8									m_Pad[3];
	BlendHeaders						m_BlendHeaders;
};

}

#endif // ENABLE_BLENDSHAPES

#endif
