// 
// grblendshapes/blendshapes_config.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#ifndef GRBLENDSHAPES_BLENDSHAPESCONFIG_H
#define GRBLENDSHAPES_BLENDSHAPESCONFIG_H

#define ENABLE_BLENDSHAPES (__TOOL || !HACK_GTA4)

#if ENABLE_BLENDSHAPES
#define ENABLE_BLENDSHAPES_ONLY(x) x
#else // ENABLE_BLENDSHAPES
#define ENABLE_BLENDSHAPES_ONLY(x)
#endif // ENABLE_BLENDSHAPES


#endif // GRBLENDSHAPES_BLENDSHAPESCONFIG_H
