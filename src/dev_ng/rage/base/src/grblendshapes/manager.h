// 
// grblendshapes/manager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRBLENDSHAPES_MANAGER_H
#define GRBLENDSHAPES_MANAGER_H

#include "atl/array.h"
#include "atl/map.h"
#include "data/base.h"
#include "data/struct.h"
#include "file/token.h"
#include "paging/base.h"

#include "blendshapes_config.h"

#if ENABLE_BLENDSHAPES

namespace rage
{

class grbTarget;
class grbBlendShape;
class rmcDrawable;
struct rmcTypeFileCbData;


/*	PURPOSE:
		A manager class for a group of blend targets
*/
class grbTargetManager : public pgBaseRefCounted
{
public:
	grbTargetManager();
	grbTargetManager(class datResource&);

	static const int RORC_VERSION = 4;		// for edge support
	DECLARE_PLACE(grbTargetManager);

	/*	PURPOSE:
			Create a rage_new blend target manager from a .type file
		PARAMS:
			typeName - The name of the type file containing the blendshapes block
			pDrawable - Pointer to drawable (required for edge to retrieve the blend data)
		RETURNS:
			A pointer to a grbTargetManager object. */
	bool Load(const char *typeName, rmcDrawable* pDrawable);

	/*	PURPOSE:
			Create a rage_new blend target
		PARAMS:
			targetName - The name of the blend target to be created
		RETURNS:
			A pointer to a grbTarget object.
		NOTES:
			This converts the targetName into a 16 bit hash using crAnimTrack::ConvertBoneNameToID. It
			then creates a rage_new target and stores it in a map using the 16bit ID as the key.
	*/
	grbTarget* CreateBlendTarget(const char* targetName);

	/*	PURPOSE:
			Create a rage_new blend shape
		PARAMS:
			T - The stream to load blend shape info from
		RETURNS:
			A pointer to a newly created grbBlendShape object.
	*/
	grbBlendShape* CreateBlendShape(fiTokenizer* T);

	/*	PURPOSE:
			Get the number of blend shape objects owned by this manager
		RETURNS:
			An integer containing the current number of blend shapes.
	*/
	int GetBlendShapeCount() const								{ return m_BlendShapes.GetCount(); }

	/*	PURPOSE:
			Get a pointer to the blend shape that has the specified index
		PARAMS:
			index - The zero based index of the blend shape to retrieve
		RETURNS:
			A pointer to a grbBlendShape object
	*/
	grbBlendShape* GetBlendShape(int index) const				{ return m_BlendShapes[index]; }

	/*	PURPOSE:
			Get the number of blend targets contained in this manager
		RETURNS:
			An integer containing the current number of blend targets
	*/
	int GetBlendTargetCount() const								{ return m_Targets.GetNumUsed(); }


	/*	PURPOSE:
			Create an iterator that can be used to iterate over all the blend
			targets owned by this manager.
		RETURNS:
			An atMap::ConstIterator representing the blend target map
	*/
	atMap<u16, datOwner<grbTarget> >::ConstIterator	GetBlendTargetIterator() const	{ return m_Targets.CreateIterator(); }

	/*	PURPOSE:
			Reset all the offset buffers contained within this manager back to zero
	*/
	void ResetBlendShapeOffsets();

	/*	PURPOSE:
			Prepare all the offset buffers contained within this manager back for subsequent blending
	*/
	void PrepareBlendShapeOffsets();

	/*	PURPOSE:
			Release all the offset buffers contained within this manager back for subsequent blending
	*/
	void ReleaseBlendShapeOffsets();

	// PURPOSE: Swap blend shape buffers
	void SwapBuffers();

	// PURPOSE: Check whether or not we're using Edge
	bool IsUsingEdge() const;

	/*	PURPOSE:
			Blend the specified target to the specified percent
		PARAMS:
			targetID - The 16bit ID of the target to blend
			blendPercent - The percentage of the blend to apply. Value must be between 0.0 and 1.0
		NOTES:
			This will add the target deltas into all the morphable offset buffers that contain this
			blend target.
	*/
	void BlendTarget(u16 targetID, float blendPercent);

	// PURPOSE: Return how many buffers we need
	u32 GetNumMorphableBuffers() const;

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

protected:
	atArray<datOwner<grbBlendShape> >	m_BlendShapes;
	atMap<u16, datOwner<grbTarget> >	m_Targets;
	rmcDrawable							*m_Drawable;
	void LoadBlendShapes( rmcTypeFileCbData *data );

private:
	friend class pgBaseRefCounted;
	virtual ~grbTargetManager();
};

} // namespace rage

#endif // ENABLE_BLENDSHAPES

#endif // GRBLENDSHAPES_MANAGER_H
