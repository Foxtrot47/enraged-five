#include "ragesecwinapi.h"

#if RSG_PC
namespace rage
{
	using namespace papi;

	// Initializing PAPI here, so that it is available for the NtDll and Kernel32 initializers.
	// We need proper initialization when we do a refactor for PAPI across the board.
	const papi::Status g_PapiInit = papi::Initialize();

	namespace NtDll
	{
		// Define NtDll function cache.
		u64 g_functionCache[NUM_FUNCTIONS];
		// Define NtDll validity flag.
		bool g_bDllScrewed = false;

		NtOpenProcessFnPtr NtOpenProcess;
		NtQueryVirtualMemoryFnPtr NtQueryVirtualMemory;
		NtQuerySystemInformationFnPtr NtQuerySystemInformation;
		NtQueryInformationProcessFnPtr NtQueryInformationProcess;

		// Initializes the RageSec NtDll module.
		NtDllInitialiser g_NtDll;

		void InitNtDll()
		{
			if (g_PapiInit != papi::Status::Success)
			{
				RageSecError("Failed to initialize RageSec WinAPI.");
				return;
			}

			HMODULE hNtdll = GetModuleHandle("ntdll.dll");
			NtOpenProcess = (NtOpenProcessFnPtr)GetProcAddress(hNtdll, "NtOpenProcess");
			NtQueryVirtualMemory = (NtQueryVirtualMemoryFnPtr)GetProcAddress(hNtdll, "NtQueryVirtualMemory");
			NtQuerySystemInformation = (NtQuerySystemInformationFnPtr)GetProcAddress(hNtdll, "NtQuerySystemInformation");
			NtQueryInformationProcess = (NtQueryInformationProcessFnPtr)GetProcAddress(hNtdll, "NtQueryInformationProcess");

			InitProtection();
		}
	}

	namespace Kernel32
	{
		// Define Kernel32 function cache.
		u64 g_functionCache[NUM_FUNCTIONS];
		// Define Kernel32 validity flag.
		bool g_bDllScrewed = false;
		
		GetModuleFileNameFnPtr GetModuleFileNameFn;
		GetModuleFileNameExFnPtr GetModuleFileNameExFn;

		LocalFreeFnPtr LocalFree;
		LocalAllocFnPtr LocalAlloc;
		OpenProcessFnPtr OpenProcess;
		CloseHandleFnPtr CloseHandle;
		GetCurrentProcessFnPtr GetCurrentProcess;
		EnumProcessModulesFnPtr EnumProcessModules;
		GetModuleFileNameAFnPtr GetModuleFileNameA;
		GetModuleFileNameWFnPtr GetModuleFileNameW;
		GetModuleFileNameExAFnPtr GetModuleFileNameExA;
		GetModuleFileNameExWFnPtr GetModuleFileNameExW;
		ReadProcessMemoryFnPtr ReadProcessMemory;
		GetModuleInformationFnPtr GetModuleInformation;

		// Initializes the RageSec Kernel32 module.
		const Kernel32Initialiser g_Kernel32;

		void InitKernel32()
		{
			if (g_PapiInit != papi::Status::Success)
			{
				RageSecError("Failed to initialize RageSec WinAPI.");
				return;
			}

			HMODULE hKernel32 = GetModuleHandle("kernel32.dll");
			LocalFree = (LocalFreeFnPtr)GetProcAddress(hKernel32, "LocalFree");
			LocalAlloc = (LocalAllocFnPtr)GetProcAddress(hKernel32, "LocalAlloc");
			OpenProcess = (OpenProcessFnPtr)GetProcAddress(hKernel32, "OpenProcess");
			CloseHandle = (CloseHandleFnPtr)GetProcAddress(hKernel32, "CloseHandle");
			GetCurrentProcess = (GetCurrentProcessFnPtr)GetProcAddress(hKernel32, "GetCurrentProcess");
			GetModuleFileNameA = (GetModuleFileNameAFnPtr)GetProcAddress(hKernel32, "GetModuleFileNameA");
			GetModuleFileNameW = (GetModuleFileNameWFnPtr)GetProcAddress(hKernel32, "GetModuleFileNameA");
			ReadProcessMemory = (ReadProcessMemoryFnPtr)GetProcAddress(hKernel32, "ReadProcessMemory");

			HMODULE hPsapi = GetModuleHandle("psapi.dll");
			EnumProcessModules = (EnumProcessModulesFnPtr)GetProcAddress(hPsapi, "EnumProcessModules");
			GetModuleFileNameExA = (GetModuleFileNameExAFnPtr)GetProcAddress(hPsapi, "GetModuleFileNameExA");
			GetModuleFileNameExW = (GetModuleFileNameExWFnPtr)GetProcAddress(hPsapi, "GetModuleFileNameExW");
			GetModuleInformation = (GetModuleInformationFnPtr)GetProcAddress(hPsapi, "GetModuleInformation");

#ifdef UNICODE
			GetModuleFileNameFn = GetModuleFileNameW;
			GetModuleFileNameExFn = GetModuleFileNameExW;
#else
			GetModuleFileNameFn	= GetModuleFileNameA;
			GetModuleFileNameExFn = GetModuleFileNameExA;
#endif // !UNICODE

			InitProtection();
		}
	}
}

#endif // RSG_PC
