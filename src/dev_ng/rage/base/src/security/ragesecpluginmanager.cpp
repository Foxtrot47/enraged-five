#include <string.h>
#include <string>

#include "atl/map.h"
#include "atl/queue.h"
#include "file/default_paths.h"
#include "file/limits.h"
#include "net/task.h"
#include "net/status.h"
#include "rline/rlpc.h"
#include "security/ragesecmemoryloader.h"
#include "security/ragesecengine.h"
#include "security/ragesecenginetasks.h"
#include "security/ragesecpluginmanager.h"
#include "security/ragesecplugin.h"
#include "system/exec.h"

#if USE_RAGESEC

#if RAGE_SEC_LOAD_LOCAL_DLL_PLUGINS
#pragma warning(disable: 4668)
#include <windows.h>
#include <shobjidl.h> // IShellLink
#include <shlguid.h> // CLSID_ShellLink

#include "data/certificateverify.h"
#pragma warning(error: 4668)
#endif 

using namespace rage;
atMap<unsigned int, RageSecPlugin*> rageSecPluginManager::sm_registeredPlugins;
atMap<unsigned int, netStatus *> rageSecPluginManager::sm_pluginsStatus;
unsigned int rageSecPluginManager::sm_idTracker;
static unsigned sm_rspmTrashValue = 0;
atArray<void*> rageSecPluginManager::sm_dllsLoaded;
bool rageSecPluginManager::sm_needsRebalancing;

void rageSecPluginManager::GetPluginUpdateReferences(atArray<u32*> &references)
{
	atMap<unsigned int, RageSecPlugin*>::Iterator entry = sm_registeredPlugins.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
		references.PushAndGrow((*entry)->updateReference);
}

bool rageSecPluginManager::RegisterPluginFunction(RageSecPluginParameters * params, unsigned int key)
{
	//@@: location RAGESECPLUGINMANAGER_REGISTERPLUGINFUNCTION_ENTRY
	if(		(PLUGIN_VERSION_MINOR != params->version.minor) 
		&&	(PLUGIN_VERSION_MAJOR != params->version.major))
	{
		RageSecError("Version mismatch on plugin registration!");
		return false;
	}

	if(sm_registeredPlugins.Access(key))
	{
		RageSecError("Plugin already registered");
		return false;
	}
	// Do some basic sanity checking on our parameter identification
	// number
	if(params->id == 0)
		params->id = ++sm_idTracker;
	else
		params->id = key;

	// Create a new plugin object with the state, such that we can
	// manage it accordingly
	RageSecPlugin *plugin = rage_new RageSecPlugin();
	plugin->parameters = *params;
	plugin->state = STATE_INSTANTIATED;
	plugin->SetTime(sysTimer::GetSystemMsTime());

	sm_registeredPlugins.Insert(key, plugin);
	//@@: location RAGESECPLUGINMANAGER_REGISTERPLUGINFUNCTION_EXIT
	sm_rspmTrashValue-=sysTimer::GetSystemMsTime();
	return true;
}

void rageSecPluginManager::DestroyPlugin(unsigned int id)
{
	// Access the status field
	//@@: location RAGESECPLUGINMANAGER_DESTROYPLUGIN_ENTRY
	netStatus **pluginStatusPtr = sm_pluginsStatus.Access(id);
	//@@: range RAGESECPLUGINMANAGER_DESTROYPLUGIN {

	if(pluginStatusPtr == NULL)
	{
		RageSecDebugf3("Couldn't free plugin with ID %d - wasn't found in the Plugin Status Map", id);
		return;
	}
	// Assign it for readable indexing
	netStatus *pluginStatus = *pluginStatusPtr;
	// Only worry about the plugins taht are pending status
	while(pluginStatus->Pending())
	{
		pluginStatus->SetCanceled();
	}

	// Okay, so we're out of the pending state, and we can go ahead and start freeing up
	// the memory that we've associated with the plugin

	// First, lets call the plugin's destroy function, if it exists
	RageSecPlugin **pluginPtr = sm_registeredPlugins.Access(id);
	if(pluginPtr == NULL)
	{
		RageSecDebugf3("Couldn't free plugin with ID %d - wasn't found in the Plugin Registration Map", id);
		return;
	}


	RageSecPlugin *plugin = *pluginPtr;

	// Check for nullity, then call
	if(plugin->parameters.Destroy!=NULL)
	{
		plugin->parameters.Destroy();
	}
	// Delete the entry from our two maps
	// Okay, now lets go ahead and destroy the associated netStatus object

	// Check to be sure it isn't null, then free
	if(pluginStatus)
	{
		delete pluginStatus;
		pluginStatus = NULL;
		sm_rspmTrashValue = 0;
	}

	// Lastly, delete our plugin, which should sanitize each of the fieldsf
	memset(plugin, 0, sizeof(RageSecPlugin));
	//@@: } RAGESECPLUGINMANAGER_DESTROYPLUGIN



	// Now indicate that we need to re-balance our game plugin checkers
	//@@: location RAGESECPLUGINMANAGER_DESTROYPLUGIN_EXIT
	SetRebalance(true);
}
void rageSecPluginManager::ShutdownClass()
{
	// Okay! Lets cleanup all of our memory

	// The first thing we want to do is go through each of our live plugins
	// and then cancel them
	//@@: location RAGESECPLUGINMANAGER_SHUTDOWNCLASS_ENTRY
	atMap<unsigned int, RageSecPlugin*>::Iterator entry = sm_registeredPlugins.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
		DestroyPlugin(entry.GetKey());

	// Clear out our maps
	//@@: location RAGESECPLUGINMANAGER_SHUTDOWNCLASS_EXIT
	sm_registeredPlugins.Kill();
	sm_pluginsStatus.Kill();

#if RAGE_SEC_SUPPORT_DLL_PLUGINS	
	// Now all that's left is to unload the DLLs
	for(int i = 0; i < sm_dllsLoaded.GetCount(); i++)
		sysFreeLibrary((sysLibrary*)sm_dllsLoaded[i]);
#endif

	// From here we should be good. Hooray no leaks!
}

bool rageSecPluginManager::CancelTasks()
{
	bool anyCanceled = false;

	atMap<unsigned int, netStatus *>::Iterator entry = sm_pluginsStatus.CreateIterator();

	for ( entry.Start(); !entry.AtEnd(); entry.Next() )
	{
		netStatus* status = entry.GetData();

		if (status != nullptr && status->Pending())
		{
			netTask::Cancel(status);
			anyCanceled = true;
		}
	}

	return anyCanceled;
}

#if RAGE_SEC_SUPPORT_DLL_PLUGINS	
void rageSecPluginManager::LoadDll(const wchar_t * path)
{
	FILE *fp;
	unsigned char *data=NULL;
	size_t size;

	fp = _wfopen(path, L"rb");
	if (fp == NULL)
	{
		//_tprintf(_T("Can't open DLL file \"%s\"."), DLL_FILE);
		RageSecError("Can't open DLL File \"%s\".", path);
		return;
	}

	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	data = (unsigned char *)malloc(size);
	fseek(fp, 0, SEEK_SET);
	fread(data, 1, size, fp);
	fclose(fp);


	HMODULE dllHandle = RageSecMemoryLoader::LoadIntoMemory(data);

	if (dllHandle == NULL)
	{
		RageSecError("Error in loading test DLL");
		return;
	}
	else
	{
		RageSecDebugf3("Successfully loaded local dll plugin for testing.");
	}


	
#if RAGE_SEC_CHECK_PLUGIN_SIGNATURE
#if RSG_FINAL
	// In the event that the DLL has no signature, or a bad one, just mosey on and return. 
	// No need to do anything fancy.
	if (!CertificateVerify::Verify((wchar_t*)path, RockstarCertificateVerification))
		return;
#else
	CertificateVerify::Verify((wchar_t*)path, RockstarCertificateVerification);
#endif
#endif

	RageSecPluginInitializeFunction initFunction = (RageSecPluginInitializeFunction)(RageSecMemoryLoader::GetProcAddressByOrdinal(dllHandle, RAGE_SEC_INIT_FUNCTION_ORDINAL_NUMBER));

	// Check to be sure that the initialization function isn't NULL
	if (initFunction == NULL) // dynamic library missing entry point?
	{
		RageSecError("Unable to find initialization function RageSecPlugin_Init in local DLL");
		return;
	}

	// Now call the initialization function, to register the DLL in however way it needs
	// to with the plugin manager
	if (!initFunction()) // dynamic library missing entry point?
	{
		RageSecError("Unable to find initialization function RageSecPlugin_Init in local DLL");
		return;
	}

	// At this point, we should be good to go ahead to add the DLL to our atArray
	// this way we can manage all of the DLL's and unload them appropriately
	sm_dllsLoaded.PushAndGrow(dllHandle);
}
#endif

void rageSecPluginManager::CreatePluginTask(RageSecPlugin *plugin, netStatus *status)
{
	//@@: location RAGESECPLUGINMANAGER_CREATEPLUGINTASK
	RageSecPluginParameters curr = plugin->parameters;
		
	// Define our task
	rageSecGenericTask *task = NULL;
	rtry												
	{	
		// Create, configure, and call our task	
		//@@: range RAGESECPLUGINMANAGER_CREATEPLUGINTASK_INNER {
		rverify(netTask::Create(&task, status), catchall, RageSecError("Task Creation Failed for plugin 0x%x", plugin->Id()));
		// Configure our task, and pass along the function that actually does the work for us in a separate
		// function - that's where the heavy lifting should go.

		rverify(task->Configure(curr.DoWork, plugin->Id()), catchall, RageSecError("Task Configuration Failed for plugin 0x%x",plugin->Id()));
		rverify(netTask::Run(task), catchall, RageSecError("Task Running Failed for plugin 0x%x",plugin->Id()));
		//@@: } RAGESECPLUGINMANAGER_CREATEPLUGINTASK_INNER


		// Indicate that we've kicked off the task.
		RageSecDebugf3("Running plugin 0x%x", plugin->Id());
	}													
	rcatchall											
	{													
		if(task != NULL)									
		{													
			netTask::Destroy(task);								
		}
	}
	//@@: location RAGESECPLUGINMANAGER_CREATEPLUGINTASK_EXIT
	sm_rspmTrashValue-=sysTimer::GetSystemMsTime();
	return;
}

void rageSecPluginManager::Update()
{

	atMap<unsigned int, RageSecPlugin*>::Iterator entry = sm_registeredPlugins.CreateIterator();
	atArray<unsigned int> queuedForMapRemoval;
	u32 currentTime = sysTimer::GetSystemMsTime();

	// Do the work for each plugin
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{	
		//@@: range RAGESECPLUGINMANAGER_UPDATE {
		RageSecPlugin *plugin = entry.GetData();
		// Update the timer
		plugin->SetTime(currentTime);
		if(plugin->state == STATE_INSTANTIATED)
		{
			//@@: location RAGESECPLUGINMANAGER_UPDATE_INSTANTIATED
			if(plugin->parameters.Create!=NULL)
			{
				plugin->parameters.Create();				
			}
			plugin->SetState(STATE_CREATED);
		}
		else if(plugin->State() == STATE_CREATED)
		{
			//@@: location RAGESECPLUGINMANAGER_UPDATE_CREATED
			if(plugin->parameters.Configure!=NULL)
			{
				plugin->parameters.Configure();
			}
			plugin->SetState(STATE_CONFIGURED_AND_QUEUED);
		}
		else if(plugin->State() == STATE_CONFIGURED_AND_QUEUED)
		{
			// Handle the initial delay for scheduled calls
			if(plugin->Type() == EXECUTE_SCHEDULED)	
			{
				RageSecPluginExtendedInformation ei = plugin->parameters.extendedInfo;
				if(sysTimer::HasElapsedIntervalMs(ei.startTime, ei.delay))
				{
					plugin->SetState(STATE_GRINDING);
				}
			}
			else if(plugin->Type() == EXECUTE_PERIODIC)	
			{
				RageSecPluginExtendedInformation ei = plugin->parameters.extendedInfo;
				if(sysTimer::HasElapsedIntervalMs(ei.startTime, ei.frequencyInMs))
				{
					plugin->SetState(STATE_GRINDING);
				}
			}
			else if(plugin->Type() == EXECUTE_PERIODIC_RANDOM)	
			{
				RageSecPluginExtendedInformation ei = plugin->parameters.extendedInfo;
				if(sysTimer::HasElapsedIntervalMs(ei.startTime, ei.checkTime))
				{
					plugin->SetState(STATE_GRINDING);
				}
			}
			else
			{
				plugin->SetState(STATE_GRINDING);
			}
		}
		else if(plugin->State() == STATE_GRINDING)
		{
			// Here is where I'm going to queue up the  work and actually execute the task
			// We don't really need a 'completed' state because the notion of being 'complete'
			// is insufficient; the netStatus object that's created in UpdateTaskExecution
			// that gets passed along to the task is going to tell us more information than
			// we could ever get out of a sheer 'completed,' state. So what we need to do is
			// take this this task ID, check our map, then inspect the netStatus object.
			// If it's still pending, then we'll let it grind away. If it's failed or succeeded
			// then we can decide what to do with it. The 'failed' state means that something
			// systematically has gone wrong - so I expect it always to succeed in that
			// the operation completed. From there, depending on the type of task, we can decide
			// whether it needs destroying, or if we just want to reset it accordingly.

			// First check to be sure it's in our map
			netStatus **pluginStatusPtr = sm_pluginsStatus.Access(plugin->Id());
			netStatus *pluginStatus;
			// If it isn't in our map, we want to create a netStatus object 
			// for this plugin to use
			if(pluginStatusPtr == NULL)
			{
				pluginStatus = rage_new netStatus();
				sm_pluginsStatus.Insert(plugin->Id(), pluginStatus);
			}
			else
				pluginStatus = *pluginStatusPtr;


			// If nothing's happened with the task, ever, go ahead and execute it to 
			// get it rolling
			if(pluginStatus->None())
			{
				// Handle the initial delay for scheduled calls
				CreatePluginTask(plugin, pluginStatus);
			}
			// Break away if the task is still pending
			else if(pluginStatus->Pending())
			{
				continue;
			}
			// Only do something if we've positively completed, which is the expected
			// result
			else if (pluginStatus->Succeeded())
			{
				// First thing we want to do is call it's OnSuccess function
				// so it can handle itself, before doing
				// of our cleanup or re-initializations
				if(plugin->parameters.OnSuccess!=NULL)
					plugin->parameters.OnSuccess();
				// In the case of periodic executiion
				if(plugin->Type() == EXECUTE_PERIODIC)	
				{
					pluginStatus->Reset();
					plugin->parameters.extendedInfo.startTime = sysTimer::GetSystemMsTime();
					plugin->SetState(STATE_CONFIGURED_AND_QUEUED);
				}
				// In the case of periodic executiion with a hint of randomness
				else if(plugin->Type() == EXECUTE_PERIODIC_RANDOM)	
				{		
					pluginStatus->Reset();
					plugin->parameters.extendedInfo.startTime = sysTimer::GetSystemMsTime();
					plugin->parameters.extendedInfo.checkTime = fwRandom::GetRandomNumberInRange(0,plugin->parameters.extendedInfo.frequencyInMs);
					plugin->SetState(STATE_CONFIGURED_AND_QUEUED);
				}
				else if(plugin->Type() == EXECUTE_ALWAYS)	
				{
					// In this event, lets just reset the  netStatus object, and execute 
					// our task over again, since we're an 'always on' type of service
					pluginStatus->Reset();
					plugin->SetState(STATE_CONFIGURED_AND_QUEUED);
				}
				// If we're neither of the two states where we need to continue
				// executing, then lets just move onto the destruction
				else
				{
					plugin->SetState(STATE_NEEDS_DESTROYING);
				}
			}
			else if (pluginStatus->Failed())
			{
				// In this event, something's cone catastrophically wrong. Log it, and 
				// perhaps inspect it later to figure out what the french toast I 
				// screwed up
				RageSecError("Plugin with ID %d failed", plugin->Id());
				// Now call our OnFailure function
				if(plugin->parameters.OnFailure!=NULL)
					plugin->parameters.OnFailure();
				// We'll need to cleanup here also - this is provided
				// our extended info tells us that it shouldn't continue
				plugin->SetState(STATE_NEEDS_DESTROYING);
			}
			else if (pluginStatus->Canceled())
			{
				// TODO: This is going to happen if we detect that one of the threads
				// has been terminated (for a suspicious reason) and needs to recover
				if(plugin->parameters.OnCancel!=NULL)
					plugin->parameters.OnCancel();

				// Go ahead and destroy it.
				plugin->SetState(STATE_NEEDS_DESTROYING);
			}
			sm_rspmTrashValue+=sysTimer::GetSystemMsTime();
		}
		else if(plugin->State() == STATE_NEEDS_DESTROYING)
		{
			// When we want to destroy a plugin, we're removing it from 
			// existence for good - so we have to 
			// 1) Cleanup the netStatus object we've associated with it
			// 2) Remove it from our maps
			// 3) Cleanup our plugin by calling the destroy function
			//@@: location RAGESECPLUGINMANAGER_UPDATE_DESTROYING
			netStatus *pluginStatus = *sm_pluginsStatus.Access(plugin->Id());
			// Add it to our queue to delete; we can't remove them from the
			// maps immediately because we're mid-traversal of one
			//@@: range RAGESECPLUGINMANAGER_UPDATE_DESTROY_INNER {
			queuedForMapRemoval.PushAndGrow(plugin->Id());
			
			// Mark it NULL
			pluginStatus = NULL;

			// Check to be sure that they've provided me a 
			// destruction function
			RageSecDebugf3("Destroying plugin 0x%x", plugin->Id());
			if(plugin->parameters.Destroy!=NULL)
			{
				plugin->parameters.Destroy();
			}
			else
			{
				// Warn them. This seems dodgey.
				RageSecWarningf("No destruction function found for plugin with ID %d", plugin->Id());
			}

			// Now lets delete all the people watching us.


			// Then zero out our reference
			plugin->updateReference = 0;;

			// Finally, lets destroy our plugin object
			DestroyPlugin(plugin->Id());
			//@@: } RAGESECPLUGINMANAGER_UPDATE_DESTROY_INNER

			// Now zero out the memory
			//@@: location RAGESECPLUGINMANAGER_UPDATE_DESTROY_ZERO_MEMORY
			memset(plugin, 0, sizeof(RageSecPlugin));
		}
		sm_rspmTrashValue-=sysTimer::GetSystemMsTime();
		//@@: } RAGESECPLUGINMANAGER_UPDATE

	}

	// Now that we're out of the map, lets go ahead and remove 
	// the entries that need to be sanitized from our two maps
	for(int i = 0; i < queuedForMapRemoval.GetCount(); i++)
	{
		sm_pluginsStatus.Delete(queuedForMapRemoval[i]);
		sm_registeredPlugins.Delete(queuedForMapRemoval[i]);
	}
}
void rageSecPluginManager::Init()
{

	// If we're on PC and in bank, go ahead and load a local file for
	// testing
#if RAGE_SEC_LOAD_LOCAL_DLL_PLUGINS
	// Set the search path for all the DLLs

	// Convert the char * that we have baked in
	atString toolsRoot(RS_TOOLSROOT);
	//size_t convertStrSize = strlen(RS_TOOLSROOT) +1;;
	//wchar_t convertStr[RAGE_MAX_PATH];
	//mbstowcs (convertStr, RS_TOOLSROOT, convertStrSize);
	//std::wstring toolsRoot = convertStr;

	// Create the strings that we'll need to traverse
	atString dllDirAdd("/bin/RageSecPlugins/");
	atString dllQueryStrAdd("/bin/RageSecPlugins/*.*");
	
	// Build them up fully
	atString dllQueryString = toolsRoot;
	dllQueryString+=dllQueryStrAdd;
	
	atString dllDir = toolsRoot;
	dllDir+=dllDirAdd;
	

	wchar_t wideDllQueryString[RAGE_MAX_PATH] = {0};
	MultiByteToWideChar(CP_UTF8 , NULL , dllQueryString.c_str(), -1, wideDllQueryString, RAGE_MAX_PATH);
	
	/*
	const wchar_t * dllDir = RS_TOOLSROOT 
	const wchar_t * dllQueryString = RS_TOOLSROOT "/bin/RageSecPlugins/*.*";
	*/

	// Create our file descriptor
	WIN32_FIND_DATAW fd;
	// Find the first file
	HANDLE hFind = ::FindFirstFileW(wideDllQueryString, &fd); 
	// Make sure it isn't invalid
	if(hFind != INVALID_HANDLE_VALUE) 
	{ 
		// Iterate through all the files, as much as we can
		do 
		{ 
			if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
				// Its okay that this is expsnive, because local plugins will just occur on 
				// development machines
				atString dllFullPath = dllDir;
				wchar_t wideDllPath[RAGE_MAX_PATH] = {0};
				MultiByteToWideChar(CP_UTF8 , NULL , dllFullPath.c_str(), -1, wideDllPath, RAGE_MAX_PATH);
				wcsncat(wideDllPath,  fd.cFileName, wcslen(fd.cFileName));
				LoadDll(wideDllPath);
			}
		} while(::FindNextFileW(hFind, &fd)); 
		// Close our handle
		::FindClose(hFind); 
	} 

	// Bueno, now we can return

#endif
}

void rageSecPluginManager::ReInit()
{
	// If we're on PC and in bank, go ahead and load a local file for
	// testing
#if RAGE_SEC_LOAD_LOCAL_DLL_PLUGINS
	// Set the search path for all the DLLs

	// Convert the char * that we have baked in
	atString toolsRoot(RS_TOOLSROOT);
	//size_t convertStrSize = strlen(RS_TOOLSROOT) +1;;
	//wchar_t convertStr[RAGE_MAX_PATH];
	//mbstowcs (convertStr, RS_TOOLSROOT, convertStrSize);
	//std::wstring toolsRoot = convertStr;

	// Create the strings that we'll need to traverse
	atString dllDirAdd("/bin/RageSecPlugins/");
	atString dllQueryStrAdd("/bin/RageSecPlugins/*.*");
	
	// Build them up fully
	atString dllQueryString = toolsRoot;
	dllQueryString+=dllQueryStrAdd;
	
	atString dllDir = toolsRoot;
	dllDir+=dllDirAdd;
	

	wchar_t wideDllQueryString[RAGE_MAX_PATH] = {0};
	MultiByteToWideChar(CP_UTF8 , NULL , dllQueryString.c_str(), -1, wideDllQueryString, RAGE_MAX_PATH);
	
	/*
	const wchar_t * dllDir = RS_TOOLSROOT 
	const wchar_t * dllQueryString = RS_TOOLSROOT "/bin/RageSecPlugins/*.*";
	*/

	// Create our file descriptor
	WIN32_FIND_DATAW fd;
	// Find the first file
	HANDLE hFind = ::FindFirstFileW(wideDllQueryString, &fd); 
	// Make sure it isn't invalid
	if(hFind != INVALID_HANDLE_VALUE) 
	{ 
		// Iterate through all the files, as much as we can
		do 
		{ 
			if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
				// Its okay that this is expsnive, because local plugins will just occur on 
				// development machines
				atString dllFullPath = dllDir;
				wchar_t wideDllPath[RAGE_MAX_PATH] = {0};
				MultiByteToWideChar(CP_UTF8 , NULL , dllFullPath.c_str(), -1, wideDllPath, RAGE_MAX_PATH);
				wcsncat(wideDllPath,  fd.cFileName, wcslen(fd.cFileName));
				LoadDll(wideDllPath);
			}
		} while(::FindNextFileW(hFind, &fd)); 
		// Close our handle
		::FindClose(hFind); 
	} 

	// Bueno, now we can return

#endif
}


#endif