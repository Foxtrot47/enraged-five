#ifndef SECURITY_MEMORY_LOADER_H
#define SECURITY_MEMORY_LOADER_H

#include "security/ragesecengine.h"
#include "security/ragesecmemoryloader.h"
#include "security/ragesecpluginmanager.h"

#if USE_RAGESEC
#if RAGE_SEC_SUPPORT_DLL_PLUGINS
#include <windows.h>
#include <iomanip>
// I'm #def'ining this to 0.
// Steve M. makes a good point - the game should have all the functionality I'd need from the common DLL's loaded by the game.
// I need to test it to be sure, but that's why I'm wrapping it behind a #def.
// Function Headers
namespace rage
{

class RageSecMemoryLoader
{
public:
	static HMODULE LoadIntoMemory( const void *);
	static void FreeLibrary(HMODULE);
	static FARPROC GetProcAddressByOrdinal(HMODULE, DWORD);
private:
	typedef struct 
	{
		PIMAGE_NT_HEADERS	headers;
		unsigned char *		imageBytes;
		unsigned char *		extraBytes;
		HMODULE	*			modules;
		unsigned int		numberModules;
		BOOL				isInitialized;
		BOOL				isRelocated;
		DWORD				pageSize;
	} RAGESEC_LOADLIBRARYCONTEXT, *PRAGESEC_LOADLIBRARYCONTEXT;

	typedef struct 
	{
		void *	address;
		void *	alignedAddress;
		DWORD	sectionSize;
		DWORD	sectionCharacteristics;
		BOOL	isLastSection;
	} RAGESEC_SECTIONDATA, *PRAGESEC_SECTIONDATA;

	// Protection flags for memory pages (Executable, Readable, Writeable)
	static int MemoryProtectionFlags[2][2][2];


	// This is the internally called function call that also utilises the context that 
	// can be passed around in between functions to store information about the current
	// module attempting to be loaded
	//static HMODULE RageSecLoadLibrary(PRAGESEC_LOADLIBRARYCONTEXT, const void * );
	// This function is more poorly named - it's the vanilla call to LoadLibrary.
	// The reason this exists is that so when we're resolving our imports, we can 
	// just call the windows representation to load the imports. There's no reason
	// to dynamically allocate all the space for things like KERNEL32.
	// At least not yet.
	//static HMODULE WindowsLoadLibrary(LPCSTR);
	//// The reason why this is contained in the private area is because
	//// this is exclusively used to free the imports, and should not be used to 
	//// free the library that we're actually attempting to load.
	//static void WindowsFreeLibrary(HMODULE);
	//
	//// The reason why this is contained in the private area is because
	//// this is exclusively used to get the process address for functions within
	//// our import table. Note that the argument is that of LPCSTR.
	//// This is because MSDN actually has no equivalent for GetProcAddress for
	//// a wide-byte string. They're weirdly expecting us to convert it using
	//// their translation function.
	//// Info, here:
	//// http://forums.codeguru.com/showthread.php?392505-wchar-version-GetProcAddress(-)-MapiFileDesc(-)
	//// It should be fine as well, as we'll only call it on the resolved imports.
	//static FARPROC WindowsGetProcAddress(HMODULE, LPCSTR);

	// This function is responsible for re-construction the section headers
	// of a binary.
	static BOOL	LoadSections(const unsigned char *, PIMAGE_NT_HEADERS , PRAGESEC_LOADLIBRARYCONTEXT);

	// This function is responsible for re-constructing the import table of a binary
	// at run-time. 
	static BOOL	LoadImportTable(PRAGESEC_LOADLIBRARYCONTEXT);



	// This function is responsible for performing all the fixups necessary to handle the fact
	// that the DLL is going to be loaded into a different address every iteration at run-time.
	static BOOL	PerformBaseRelocation(PRAGESEC_LOADLIBRARYCONTEXT, SIZE_T);
	static BOOL CompleteSections(PRAGESEC_LOADLIBRARYCONTEXT);
	static BOOL CompleteSectionData(PRAGESEC_LOADLIBRARYCONTEXT, PRAGESEC_SECTIONDATA);
	static BOOL HandleThreadLocalStorage(PRAGESEC_LOADLIBRARYCONTEXT);
	static BOOL CanReadMemory(void *);
	static DWORD CalculateSectionSize(PRAGESEC_LOADLIBRARYCONTEXT, PIMAGE_SECTION_HEADER);
	
	
	
};

} // namespace rage
#endif // RAGE_SEC_SUPPORT_DLL_PLUGINS
#endif // USE_RAGESEC
#endif // !SECURITY_MEMORY_LOADER_H

