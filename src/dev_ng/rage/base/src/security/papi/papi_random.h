#ifndef SECURITY_PAPI_PAPI_RANDOM_H_
#define SECURITY_PAPI_PAPI_RANDOM_H_

#if RSG_PC
namespace rage
{
	namespace papi
	{
#pragma warning( push )
#pragma warning( disable :  4189 )
		u64 crcinline GenerateRandom()
		{
			u64 random = 0;
			uintptr_t peb = 0;
			uintptr_t ldr = 0;
			PebLdrData* loaderTable = nullptr;
			LdrDataTableEntry* loaderTableEntry = nullptr;

			peb = GetPeb();
			loaderTable = *reinterpret_cast<PebLdrData**>(GetPeb() + PebLdrOffset);
			if (loaderTable->Length != sizeof(PebLdrData))
			{
				return 0;
			}

			random = ((__rdtsc() << 32) | __rdtsc());
			random = Crc64(&peb, sizeof(peb), random);
			random = Crc64(&loaderTable, loaderTable->Length, random);

			loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTable->InLoadOrderModuleList.Flink);
			do
			{
				random = Crc64(&loaderTableEntry->BaseAddress, sizeof(loaderTableEntry->BaseAddress), random);
				loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTableEntry->InLoadOrderLinks.Flink);
			} while (loaderTableEntry->InLoadOrderLinks.Flink != loaderTable->InLoadOrderModuleList.Flink);

			random = Crc64(&random, sizeof(random), random);

			return random;
		}
#pragma warning( pop )
	} // namespace papi
} // namespace rage
#endif // RSG_PC
#endif // !SECURITY_PAPI_PAPI_RANDOM_H_
