#ifndef SECURITY_PAPI_PAPI_FUNCTION_H_
#define SECURITY_PAPI_PAPI_FUNCTION_H_
#if RSG_PC
namespace rage
{
namespace papi
{
	papiinline uintptr_t GetFunctionAddressByHash(MODULE_ID moduleId, FUNCTION_ID functionId, bool lockLoader = true)
	{
		uintptr_t moduleBase = 0;
		uintptr_t functionAddress = 0;
		PIMAGE_DOS_HEADER dosHeader = 0;
		PIMAGE_NT_HEADERS ntHeaders = 0;
		PIMAGE_SECTION_HEADER sectionHeader = 0;
		PIMAGE_EXPORT_DIRECTORY exportDirectory = 0;
		PDWORD addressOfNames = 0;
		PDWORD addressOfFunctions = 0;
		PUSHORT addressOfNameOrdinals = 0;

		moduleBase = GetModuleBaseByHash(moduleId, lockLoader);
		if (moduleBase == 0)
		{
			// Failed to find module base
			return 0;
		}

		dosHeader = reinterpret_cast<PIMAGE_DOS_HEADER>(moduleBase);
		if (dosHeader->e_magic != IMAGE_DOS_SIGNATURE)
		{
			// Invalid DOS Header
			return 0;
		}

		ntHeaders = reinterpret_cast<PIMAGE_NT_HEADERS>(moduleBase + dosHeader->e_lfanew);
		if (ntHeaders->Signature != IMAGE_NT_SIGNATURE)
		{
			// Invalid NT Headers
			return 0;
		}

		if (ntHeaders->FileHeader.Machine == IMAGE_FILE_MACHINE_IA64 || ntHeaders->FileHeader.Machine == IMAGE_FILE_MACHINE_AMD64)
		{
			sectionHeader = reinterpret_cast<PIMAGE_SECTION_HEADER>(moduleBase + dosHeader->e_lfanew + sizeof(IMAGE_NT_HEADERS));
			exportDirectory = reinterpret_cast<PIMAGE_EXPORT_DIRECTORY>(moduleBase + ntHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
		}
		else
		{
			// Not a 64-bit image
			return 0;
		}

		if (!exportDirectory)
		{
			// No export directory available
			return 0;
		}

		addressOfNames = reinterpret_cast<PDWORD>(moduleBase + exportDirectory->AddressOfNames);
		addressOfFunctions = reinterpret_cast<PDWORD>(moduleBase + exportDirectory->AddressOfFunctions);
		addressOfNameOrdinals = reinterpret_cast<PUSHORT>(moduleBase + exportDirectory->AddressOfNameOrdinals);

		for (DWORD exportIndex = 0; exportIndex < exportDirectory->NumberOfNames; exportIndex++)
		{
			if (GenerateHashEx(reinterpret_cast<const char*>(moduleBase + *addressOfNames)) == functionId)
			{
				bool forwarded = (addressOfFunctions[*addressOfNameOrdinals] >= ntHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress) &&
					(addressOfFunctions[*addressOfNameOrdinals] < ntHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress +
						ntHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size);
				if (forwarded)
				{
					const char* forwarderString = reinterpret_cast<char*>(moduleBase + addressOfFunctions[*addressOfNameOrdinals]);
					char forwardedModuleName[MAX_PATH] = { 0 };
					char forwardedFunctionName[MAX_PATH] = { 0 };
					_strncpy(forwardedModuleName, forwarderString, _strrchr(forwarderString, '.') - forwarderString);
					_strncpy(forwardedFunctionName, _strrchr(forwarderString, '.') + 1, _strlen(forwarderString) - (_strrchr(forwarderString, '.') - forwarderString - 1));

					if (strstr(forwardedFunctionName, "#") != nullptr)
					{
						// Import by ordinal
						int ordinal = strtol(strstr(forwardedFunctionName, "#") + sizeof(char), nullptr, 10);
						functionAddress = (uintptr_t)GetProcAddress(GetModuleHandleA(forwardedModuleName), (LPCSTR)IMAGE_ORDINAL(ordinal));
					}
					else
					{
						functionAddress = (uintptr_t)GetProcAddress(GetModuleHandleA(forwardedModuleName), forwardedFunctionName);
					}
				}
				else
				{
					functionAddress = moduleBase + addressOfFunctions[*addressOfNameOrdinals];
				}
				break;
			}
			addressOfNames++;
			addressOfNameOrdinals++;
		}

		return functionAddress;
	}

#pragma warning( push )
#pragma warning( disable :  4189 )
	papiinline uintptr_t GetFunctionAddressByHashEx(MODULE_ID moduleId, FUNCTION_ID functionId, bool lockLoader = true)
	{
		uintptr_t moduleBase = 0;
		uintptr_t functionAddress = 0;
		PIMAGE_DOS_HEADER dosHeader = 0;
		PIMAGE_NT_HEADERS ntHeaders = 0;
		PIMAGE_SECTION_HEADER sectionHeader = 0;
		PIMAGE_EXPORT_DIRECTORY exportDirectory = 0;
		PDWORD addressOfNames = 0;
		PDWORD addressOfFunctions = 0;
		PUSHORT addressOfNameOrdinals = 0;

		if (!Context::Instance().Validate())
		{
			return 0;
		}

		moduleBase = GetModuleBaseByHash(moduleId, lockLoader);
		if (moduleBase == 0)
		{
			// Failed to find module base
			return 0;
		}

		dosHeader = reinterpret_cast<PIMAGE_DOS_HEADER>(moduleBase);
		if (dosHeader->e_magic != IMAGE_DOS_SIGNATURE)
		{
			// Invalid DOS Header
			return 0;
		}

		ntHeaders = reinterpret_cast<PIMAGE_NT_HEADERS>(moduleBase + dosHeader->e_lfanew);
		if (ntHeaders->Signature != IMAGE_NT_SIGNATURE)
		{
			// Invalid NT Headers
			return 0;
		}

		if (ntHeaders->FileHeader.Machine == IMAGE_FILE_MACHINE_IA64 || ntHeaders->FileHeader.Machine == IMAGE_FILE_MACHINE_AMD64)
		{
			sectionHeader = reinterpret_cast<PIMAGE_SECTION_HEADER>(moduleBase + dosHeader->e_lfanew + sizeof(IMAGE_NT_HEADERS));
			exportDirectory = reinterpret_cast<PIMAGE_EXPORT_DIRECTORY>(moduleBase + ntHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
		}
		else
		{
			// Not a 64-bit image
			return 0;
		}

		if (!exportDirectory)
		{
			// No export directory available
			return 0;
		}

		addressOfNames = reinterpret_cast<PDWORD>(moduleBase + exportDirectory->AddressOfNames);
		addressOfFunctions = reinterpret_cast<PDWORD>(moduleBase + exportDirectory->AddressOfFunctions);
		addressOfNameOrdinals = reinterpret_cast<PUSHORT>(moduleBase + exportDirectory->AddressOfNameOrdinals);

		for (DWORD exportIndex = 0; exportIndex < exportDirectory->NumberOfNames; exportIndex++)
		{
			if (GenerateHashEx(reinterpret_cast<const char*>(moduleBase + *addressOfNames)) == functionId)
			{
				bool forwarded = (addressOfFunctions[*addressOfNameOrdinals] >= ntHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress) &&
					(addressOfFunctions[*addressOfNameOrdinals] < ntHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress +
						ntHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size);
				if (forwarded)
				{
					bool ordinal = false;
					const char* forwarderString = reinterpret_cast<char*>(moduleBase + addressOfFunctions[*addressOfNameOrdinals]);
					char forwardedModuleName[MAX_PATH] = { 0 };
					char forwardedFunctionName[MAX_PATH] = { 0 };
					_strncpy(forwardedModuleName, forwarderString, _strrchr(forwarderString, '.') - forwarderString);
					_strncpy(forwardedFunctionName, _strrchr(forwarderString, '.') + 1, _strlen(forwarderString) - (_strrchr(forwarderString, '.') - forwarderString - 1));

					AnsiString ansiString = { 0 };
					UnicodeString unicodeString = { 0 };
					wchar_t targetModuleName[MAX_PATH] = { 0 };

					_RtlInitAnsiString(&ansiString, forwardedModuleName);
					_RtlInitUnicodeString(&unicodeString, targetModuleName, sizeof(targetModuleName) / sizeof(wchar_t));

					uintptr_t pRtlAnsiStringToUnicodeString = Context::Instance().GetRtlAnsiStringToUnicodeString();
					if (!pRtlAnsiStringToUnicodeString)
					{
						continue;
					}

					reinterpret_cast<RTLANSISTRINGTOUNICODESTRING>(Context::Instance().GetRtlAnsiStringToUnicodeString())(&unicodeString, &ansiString, FALSE);


					uintptr_t pLdrGetDllHandle = Context::Instance().GetLdrGetDllHandle();
					if (!pLdrGetDllHandle)
					{
						continue;
					}

					uintptr_t moduleHandle = 0;
					if (reinterpret_cast<LDRGETDLLHANDLE>(Context::Instance().GetLdrGetDllHandle())(nullptr, 0, &unicodeString, &moduleHandle) != 0 || moduleHandle == 0)
					{
						return 0;
					}

					if (strstr(forwardedFunctionName, "#") != nullptr)
					{
						// Import by ordinal
						int ordinal = strtol(strstr(forwardedFunctionName, "#") + sizeof(char), nullptr, 10);

						uintptr_t pLdrGetProcedureAddress = Context::Instance().GetLdrGetProcedureAddress();
						if (!pLdrGetProcedureAddress)
						{
							continue;
						}

						reinterpret_cast<LDRGETPROCEDUREADDRESS>(pLdrGetProcedureAddress)(moduleHandle, 0, ordinal, &functionAddress);
					}
					else
					{
						_RtlInitAnsiString(&ansiString, forwardedFunctionName);

						uintptr_t pLdrGetProcedureAddress = Context::Instance().GetLdrGetProcedureAddress();
						if (!pLdrGetProcedureAddress)
						{
							continue;
						}

						reinterpret_cast<LDRGETPROCEDUREADDRESS>(pLdrGetProcedureAddress)(moduleHandle, &ansiString, 0, &functionAddress);
					}
				}
				else
				{
					functionAddress = moduleBase + addressOfFunctions[*addressOfNameOrdinals];
				}
				break;
			}
			addressOfNames++;
			addressOfNameOrdinals++;
		}

		return functionAddress;
	}
#pragma warning( pop )
} // namespace papi
} // namespace rage
#endif // RSG_PC
#endif // !SECURITY_PAPI_PAPI_FUNCTION_H_
