#ifndef SECURITY_PAPI_PAPIPEB_H
#define SECURITY_PAPI_PAPIPEB_H

#if RSG_PC
namespace rage
{
	namespace papi
	{
		/*
		This routine return the Process Environment Block (PEB)
		Register: 64-bit GS[0x60]
		*/
		__forceinline uintptr_t GetPeb()
		{
			return (uintptr_t)__readgsqword(0x60);
		}
	}
}
#endif // RSG_PC
#endif // SECURITY_PAPI_PAPIPEB_H