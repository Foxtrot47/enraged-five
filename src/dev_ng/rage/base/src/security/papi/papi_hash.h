#ifndef SECURITY_PAPI_PAPIHASH_H
#define SECURITY_PAPI_PAPIHASH_H

#if RSG_PC
namespace rage
{
	namespace papi
	{
		// Fnv1 constants
		const u64 Fnv164_Prime = 0x100000001B3ull;
		const u64 Fnv164_Basis = 0xCBF29CE484222325ull;

		char papiinline __to_lower_run_time(const char c) {
			return (c >= 'A' && c <= 'Z') ? c + ('a' - 'A') : c;
		}

		wchar_t papiinline __to_lower_run_time(const wchar_t c) {
			return (c >= L'A' && c <= L'Z') ? c + (L'a' - L'A') : c;
		}

		// Runtime hash function
		u64 papiinline GenerateHashEx(const char* string)
		{
			u64 value = Fnv164_Basis;

			while (*string)
			{
				value ^= __to_lower_run_time(*string);
				value *= Fnv164_Prime;
				string++;
			}

			return value;
		}

		// Run-time hash function
		u64 papiinline GenerateHashEx(const char* string, size_t length)
		{
			u64 value = Fnv164_Basis;

			size_t offset = 0;
			while (*string && offset < length)
			{
				// NOTE[CD] : Change __to_lower_run_time with tolower in case of problems 
				value ^= __to_lower_run_time(*string);
				value *= Fnv164_Prime;
				string++;
				offset++;
			}

			return value;
		}

		// Run-time hash function
		u64 papiinline GenerateHashEx(const wchar_t* string)
		{
			u64 value = Fnv164_Basis;

			while (*string)
			{
				// NOTE[CD] : Change __to_lower_run_time with tolower in case of problems 
				value ^= __to_lower_run_time(*string);
				value *= Fnv164_Prime;
				string++;
			}

			return value;
		}

		// Run-time hash function
		u64 papiinline GenerateHashEx(const wchar_t* string, size_t length)
		{
			u64 value = Fnv164_Basis;
			size_t offset = 0;
			while (*string && offset < length)
			{
				// NOTE[CD] : Change __to_lower_run_time with tolower in case of problems 
				value ^= __to_lower_run_time(*string);
				value *= Fnv164_Prime;
				string++;
			}

			return value;
		}

		// Compile time hash function
		template <u64 T, u64 I>
		struct Fnv164Hash
		{
			__forceinline static u64 Hash(const char(&str)[T])
			{
				u64 val = (__to_lower_run_time(str[I - 1]) ^ Fnv164Hash<T, I - 1>::Hash(str));
				return T == I ? val : val * Fnv164_Prime;
			}
		};

		// Compile time hash function base
		template <u64 T>
		struct Fnv164Hash<T, 1>
		{
			__forceinline static u64 Hash(const char(&str)[T])
			{
				return (__to_lower_run_time(str[0]) ^ Fnv164_Basis) * Fnv164_Prime;
			}
		};

		// Compile time hash function wrapper
		class HashString
		{
		public:
			template <u64 T>
			__forceinline HashString(const char(&str)[T])
				: m_hash(Fnv164Hash<T, T>::Hash(str))
			{

			}
			const __forceinline u64 GetHash()
			{
				return m_hash;
			}
		private:
			u64 m_hash;
		};
	} // namespace papi
} // namespace rage
#endif // RSG_PC
#endif // SECURITY_PAPI_PAPIHASH_H
