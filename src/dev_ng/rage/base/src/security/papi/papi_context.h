#ifndef SECURITY_PAPI_PAPI_CONTEXT_H_
#define SECURITY_PAPI_PAPI_CONTEXT_H_

#if RSG_PC
namespace rage
{
	namespace papi
	{
		template<typename T>
		class Value
		{
		public:
			papiinline Value()
			{
				m_value = GenerateRandom();
				m_valueKeys.primaryKey = GenerateRandom();
				m_valueKeys.secondaryKey = GenerateRandom();
				m_checksum = GenerateRandom();
				m_checksumKeys.primaryKey = GenerateRandom();
				m_checksumKeys.secondaryKey = GenerateRandom();
			}

			crcinline Value(T value)
			{
				m_valueKeys.primaryKey = GenerateRandom();
				m_valueKeys.secondaryKey = GenerateRandom();
				m_checksumKeys.primaryKey = GenerateRandom();
				m_checksumKeys.secondaryKey = GenerateRandom();

				u64 valueKey = Crc64(&m_valueKeys.primaryKey, sizeof(m_valueKeys.primaryKey), 0);
				valueKey = Crc64(&m_valueKeys.secondaryKey, sizeof(m_valueKeys.secondaryKey), valueKey);

				u64 checksumKey = Crc64(&m_checksumKeys.primaryKey, sizeof(m_checksumKeys.primaryKey), 0);
				checksumKey = Crc64(&m_checksumKeys.secondaryKey, sizeof(m_checksumKeys.secondaryKey), checksumKey);

				m_checksum = Crc64(&value, sizeof(T), checksumKey);
				m_value = value ^ valueKey;
			}

			void papiinline Set(T value)
			{
				m_valueKeys.primaryKey = GenerateRandom();
				m_valueKeys.secondaryKey = GenerateRandom();
				m_checksumKeys.primaryKey = GenerateRandom();
				m_checksumKeys.secondaryKey = GenerateRandom();

				u64 valueKey = Crc64(&m_valueKeys.primaryKey, sizeof(m_valueKeys.primaryKey), 0);
				valueKey = Crc64(&m_valueKeys.secondaryKey, sizeof(m_valueKeys.secondaryKey), valueKey);

				u64 checksumKey = Crc64(&m_checksumKeys.primaryKey, sizeof(m_checksumKeys.primaryKey), 0);
				checksumKey = Crc64(&m_checksumKeys.secondaryKey, sizeof(m_checksumKeys.secondaryKey), checksumKey);

				m_checksum = Crc64(&value, sizeof(T), checksumKey);
				m_value = value ^ valueKey;
			}

			T crcinline Get()
			{
				u64 valueKey = Crc64(&m_valueKeys.primaryKey, sizeof(m_valueKeys.primaryKey), 0);
				valueKey = Crc64(&m_valueKeys.secondaryKey, sizeof(m_valueKeys.secondaryKey), valueKey);
				return m_value ^ valueKey;
			}

			bool crcinline Validate()
			{
				u64 valueKey = Crc64(&m_valueKeys.primaryKey, sizeof(m_valueKeys.primaryKey), 0);
				valueKey = Crc64(&m_valueKeys.secondaryKey, sizeof(m_valueKeys.secondaryKey), valueKey);
				u64 checksumKey = Crc64(&m_checksumKeys.primaryKey, sizeof(m_checksumKeys.primaryKey), 0);
				checksumKey = Crc64(&m_checksumKeys.secondaryKey, sizeof(m_checksumKeys.secondaryKey), checksumKey);

				T value = m_value ^ valueKey;
				if (Crc64(&value, sizeof(value), checksumKey) != m_checksum)
				{
					return false;
				}
				return true;
			}

		private:
			T m_value;
			u64 m_checksum;
			struct
			{
				u64 primaryKey;
				u64 secondaryKey;
			}m_valueKeys;
			struct
			{
				u64 primaryKey;
				u64 secondaryKey;
			}m_checksumKeys;
		};

		class Context
		{
		private:
			Context() {};

			Context(const Context&) {};
			Context& operator=(const Context &) {};
			Context(Context &&) {};
			Context& operator=(Context &&) {};

		public:
			static Context& Instance()
			{
				static Context instance;
				return instance;
			}

			void papiinline SetLdrLockLoaderLock(uintptr_t value)
			{
				m_ldrLockLoaderLock.Set(value);
				m_ldrLockLoaderLockShadowCopy.Set(value);
			}

			uintptr_t papiinline GetLdrLockLoaderLock()
			{
				if (!m_ldrLockLoaderLock.Validate() ||
					!m_ldrLockLoaderLockShadowCopy.Validate() ||
					m_ldrLockLoaderLock.Get() != m_ldrLockLoaderLockShadowCopy.Get())
				{
					return 0;
				}
				return m_ldrLockLoaderLock.Get();
			}

			void papiinline SetLdrUnlockLoaderLock(uintptr_t value)
			{
				m_ldrUnlockLoaderLock.Set(value);
				m_ldrUnlockLoaderLockShadowCopy.Set(value);
			}

			uintptr_t papiinline GetLdrUnlockLoaderLock()
			{
				if (!m_ldrUnlockLoaderLock.Validate() ||
					!m_ldrUnlockLoaderLockShadowCopy.Validate() ||
					m_ldrUnlockLoaderLock.Get() != m_ldrUnlockLoaderLockShadowCopy.Get())
				{
					return 0;
				}
				return m_ldrUnlockLoaderLock.Get();
			}

			void papiinline SetLdrGetDllHandle(uintptr_t value)
			{
				m_ldrGetDllHandle.Set(value);
				m_ldrGetDllHandleShadowCopy.Set(value);
			}

			uintptr_t papiinline GetLdrGetDllHandle()
			{
				if (!m_ldrGetDllHandle.Validate() ||
					!m_ldrGetDllHandleShadowCopy.Validate() ||
					m_ldrGetDllHandle.Get() != m_ldrGetDllHandleShadowCopy.Get())
				{
					return 0;
				}
				return m_ldrGetDllHandle.Get();
			}

			void papiinline SetLdrGetProcedureAddress(uintptr_t value)
			{
				m_ldrGetProcedureAddress.Set(value);
				m_ldrGetProcedureAddressShadowCopy.Set(value);
			}

			uintptr_t papiinline GetLdrGetProcedureAddress()
			{
				if (!m_ldrGetProcedureAddress.Validate() ||
					!m_ldrGetProcedureAddressShadowCopy.Validate() ||
					m_ldrGetProcedureAddress.Get() != m_ldrGetProcedureAddressShadowCopy.Get())
				{
					return 0;
				}
				return m_ldrGetProcedureAddress.Get();
			}

			void papiinline SetRtlAnsiStringToUnicodeString(uintptr_t value)
			{
				m_rtlAnsiStringToUnicodeString.Set(value);
				m_rtlAnsiStringToUnicodeStringShadowCopy.Set(value);
			}

			uintptr_t papiinline GetRtlAnsiStringToUnicodeString()
			{
				// Validate the value
				if (!m_rtlAnsiStringToUnicodeString.Validate()
					|| !m_rtlAnsiStringToUnicodeStringShadowCopy.Validate() ||
					m_rtlAnsiStringToUnicodeString.Get() != m_rtlAnsiStringToUnicodeStringShadowCopy.Get())
				{
					// Validation failed
					return 0;
				}
				// Validation succeeded
				return m_rtlAnsiStringToUnicodeString.Get();
			}

			bool papiinline Validate()
			{
				// Validate all the values of the context
				if (!m_ldrLockLoaderLock.Validate() ||
					!m_ldrUnlockLoaderLock.Validate() ||
					!m_ldrGetDllHandle.Validate() ||
					!m_ldrGetProcedureAddress.Validate() ||
					!m_rtlAnsiStringToUnicodeString.Validate() ||
					!m_ldrLockLoaderLockShadowCopy.Validate() ||
					!m_ldrUnlockLoaderLockShadowCopy.Validate() ||
					!m_ldrGetProcedureAddressShadowCopy.Validate() ||
					!m_ldrGetDllHandleShadowCopy.Validate() ||
					!m_rtlAnsiStringToUnicodeStringShadowCopy.Validate())
				{
					// Validation failed
					return false;
				}

				// Validation suceeded
				return true;
			}

		private:
			// Sensitive variables
			Value<uintptr_t> m_ldrLockLoaderLock;
			Value<uintptr_t> m_ldrUnlockLoaderLock;
			Value<uintptr_t> m_ldrGetDllHandle;
			Value<uintptr_t> m_ldrGetProcedureAddress;
			Value<uintptr_t> m_rtlAnsiStringToUnicodeString;
			// Secondary/shadow copies of the variables above
			Value<uintptr_t> m_ldrLockLoaderLockShadowCopy;
			Value<uintptr_t> m_ldrUnlockLoaderLockShadowCopy;
			Value<uintptr_t> m_ldrGetDllHandleShadowCopy;
			Value<uintptr_t> m_ldrGetProcedureAddressShadowCopy;
			Value<uintptr_t> m_rtlAnsiStringToUnicodeStringShadowCopy;
		};
	} // namespace papi
} // namespace rage
#endif // RSG_PC
#endif // !SECURITY_PAPI_PAPI_CONTEXT_H_
