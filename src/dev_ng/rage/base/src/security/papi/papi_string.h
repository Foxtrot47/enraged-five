#ifndef SECURITY_PAPI_PAPI_STRING_H_
#define SECURITY_PAPI_PAPI_STRING_H_

#if RSG_PC
namespace rage
{
	namespace papi
	{
		size_t papiinline _strlen(const char* str)
		{
			const char *start = str;
			while (*str)
				str++;
			return str - start;
		}

		int papiinline _strcmp(const char* s1, const char* s2)
		{
			while (*s1 != '\0' && *s1 == *s2)
			{
				s1++;
				s2++;
			}
			return (*(unsigned char *)s1) - (*(unsigned char *)s2);
		}

		int papiinline _strncmp(const char* s1, const char* s2, size_t n)
		{
			if (n == 0)
				return 0;
			while (n-- != 0 && *s1 == *s2)
			{
				if (n == 0 || *s1 == '\0')
					break;
				s1++;
				s2++;
			}
			return (*(unsigned char *)s1) - (*(unsigned char *)s2);
		}

		papiinline char* _strncpy(char* dst0, const char* src0, size_t count)
		{
			char *dscan;
			const char *sscan;
			dscan = dst0;
			sscan = src0;
			while (count > 0)
			{
				--count;
				if ((*dscan++ = *sscan++) == '\0')
					break;
			}
			while (count-- > 0)
				*dscan++ = '\0';
			return dst0;
		}

#pragma warning( push )
#pragma warning( disable: 4706 )
		papiinline char* _strrchr(const char* s, int i)
		{
			const char *last = NULL;
			if (i)
			{
				while ((s = strchr(s, i)))
				{
					last = s;
					s++;
				}
			}
			else
			{
				last = strchr(s, i);
			}

			return (char *)last;
		}
#pragma warning( pop )

		size_t papiinline _wcslen(const wchar_t* s)
		{
			const wchar_t *p;
			p = s;
			while (*p)
				p++;
			return p - s;
		}

		int papiinline _wcscmp(const wchar_t * s1, const wchar_t * s2)
		{
			while (*s1 == *s2++)
				if (*s1++ == 0)
					return (0);
			return (*s1 - *--s2);
		}

		int papiinline _wcsncmp(const wchar_t* s1, const wchar_t* s2, size_t n)
		{
			if (n == 0)
			{
				return (0);
			}
			do
			{
				if (*s1 != *s2++)
				{
					return (*s1 - *--s2);
				}
				if (*s1++ == 0)
					break;
			} while (--n != 0);
			return (0);
		}

		papiinline wchar_t* _wcsncpy(wchar_t* s1, const wchar_t* s2, size_t n)
		{
			wchar_t *dscan = s1;
			while (n > 0)
			{
				--n;
				if ((*dscan++ = *s2++) == L'\0')  break;
			}
			while (n-- > 0)  *dscan++ = L'\0';
			return s1;
		}

		void papiinline _RtlInitAnsiString(AnsiString* ansiString, char* string)
		{
			if (string == nullptr)
			{
				ansiString->Buffer = nullptr;
				ansiString->Length = 0;
				ansiString->MaximumLength = 0;
			}
			else
			{
				ansiString->Buffer = string;
				ansiString->Length = (u16)_strlen(string);
				ansiString->MaximumLength = (u16)ansiString->Length + sizeof(char);
			}
		}

		void papiinline _RtlInitAnsiString(AnsiString* ansiString, char* string, size_t stringSize)
		{
			if (string == nullptr)
			{
				ansiString->Buffer = nullptr;
				ansiString->Length = 0;
				ansiString->MaximumLength = 0;
			}
			else
			{
				ansiString->Buffer = string;
				ansiString->Length = 0;
				ansiString->MaximumLength = (u16)stringSize;
			}
		}

		void papiinline _RtlInitUnicodeString(UnicodeString* unicodeString, wchar_t* string)
		{
			if (string == nullptr)
			{
				unicodeString->Buffer = nullptr;
				unicodeString->Length = 0;
				unicodeString->MaximumLength = 0;
			}
			else
			{
				unicodeString->Buffer = string;
				unicodeString->Length = (u16)_wcslen(string);
				unicodeString->MaximumLength = (u16)unicodeString->Length + sizeof(char);
			}
		}

		void papiinline _RtlInitUnicodeString(UnicodeString* unicodeString, wchar_t* string, size_t stringSize)
		{
			if (string == nullptr)
			{
				unicodeString->Buffer = nullptr;
				unicodeString->Length = 0;
				unicodeString->MaximumLength = 0;
			}
			else
			{
				unicodeString->Buffer = string;
				unicodeString->Length = 0;
				unicodeString->MaximumLength = (u16)stringSize;
			}
		}
	} // namespace papi
} // namespace rage
#endif // RSG_PC
#endif // !SECURITY_PAPI_PAPI_STRING_H_
