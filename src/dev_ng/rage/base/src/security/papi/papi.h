#ifndef SECURITY_PAPI_PAPI_H_
#define SECURITY_PAPI_PAPI_H_

#if RSG_PC

RAGE_DECLARE_SUBCHANNEL(ragesecengine, papi);
#define papiAssert(cond)							RAGE_ASSERT(ragesecengine_papi,cond)
#define papiAssertf(cond,fmt,...)					RAGE_ASSERTF(ragesecengine_papi,cond,fmt,##__VA_ARGS__)
#define papiFatalAssertf(cond,fmt,...)				RAGE_FATALASSERTF(rragesecengine_papi,cond,fmt,##__VA_ARGS__)
#define papiVerifyf(cond,fmt,...)					RAGE_VERIFYF(ragesecengine_papi,cond,fmt,##__VA_ARGS__)
#define papiErrorf(fmt,...)							RAGE_ERRORF(ragesecengine_papi,fmt,##__VA_ARGS__)
#define papiWarningf(fmt,...)						RAGE_WARNINGF(ragesecengine_papi,fmt,##__VA_ARGS__)
#define papiDisplayf(fmt,...)						RAGE_DISPLAYF(ragesecengine_papi,fmt,##__VA_ARGS__)
#define papiDebugf1(fmt,...)						RAGE_DEBUGF1(ragesecengine_papi,fmt,##__VA_ARGS__)
#define papiDebugf2(fmt,...)						RAGE_DEBUGF2(ragesecengine_papi,fmt,##__VA_ARGS__)
#define papiDebugf3(fmt,...)						RAGE_DEBUGF3(ragesecengine_papi,fmt,##__VA_ARGS__)
#define papiLogf(severity,fmt,...)					RAGE_LOGF(ragesecengine_papi,severity,fmt,##__VA_ARGS__)

#if !RSG_FINAL
#define crcinline inline
#else
#define crcinline inline
#endif // RSG_FINAL

#if !RSG_FINAL && !__PROFILE
#define papiinline inline 
#else
#define papiinline inline
#endif

#include "papi_types.h"

namespace rage
{
	namespace papi
	{
		const u32 PebBeingDebuggedOffset = 2;
		const u32 PebImageBaseAddressOffset = 16;
		const u32 PebLdrOffset = 24;
		const u32 PebApiSetMapOffset = 104;
		const u32 PebLoaderLockOffset = 272;
		const u32 PebOSMajorVersionOffset = 280;
		const u32 PebOSMinorVersionOffset = 284;
		const u32 PebOSBuildNumberOffset = 288;
		const u32 PebSessionIdOffset = 704;
		const u32 PebCSDVersionOffset = 744;
		const u32 PebAppCompatInfoOffset = 736;

		typedef u64 MODULE_ID;
		typedef u64 FUNCTION_ID;

		typedef NTSTATUS(WINAPI* LDRGETDLLHANDLE)(wchar_t* modulePath, u64 unused, UnicodeString* moduleFileName, uintptr_t* moduleHandle);
		typedef NTSTATUS(WINAPI* LDRLOADDLL)(wchar_t* pathToFile, u32 flags, UnicodeString* moduleFileName, uintptr_t moduleHandle);
		typedef NTSTATUS(WINAPI* LDRUNLOADDLL)(uintptr_t moduleHandle);
		typedef NTSTATUS(WINAPI* LDRGETPROCEDUREADDRESS)(uintptr_t moduleHandle, AnsiString* functionName, u32 functionOrdinal, uintptr_t* functionAddress);
		typedef NTSTATUS(WINAPI* LDRLOCKLOADERLOCK)(u32 flags, u32* state, u64 * cookie);
		typedef NTSTATUS(WINAPI* LDRUNLOCKLOADERLOCK)(u32 flags, u64  Cookie);
		typedef NTSTATUS(WINAPI* RTLACQUIREPEBLOCK)();
		typedef NTSTATUS(WINAPI* RTLRELEASEPEBLOCK)();
		typedef VOID(WINAPI* RTLGETNTVERSIONNUMBERS)(u32 *MajorVersion, u32 *MinorVersion, u32 *BuildNumber);
		typedef NTSTATUS(WINAPI* RTLANSISTRINGTOUNICODESTRING)(UnicodeString* destinationString, AnsiString* sourceString,
			bool allocateDestinationString);

		const MODULE_ID NTDLL_MODULE_ID = 0xbb7bb9a74c2f14fb;
		const MODULE_ID KERNEL32_MODULE_ID = 0xe14b18a7acf9c443;
		const MODULE_ID KERNELBASE_MODULE_ID = 0xacd80f50f7102617;
		const MODULE_ID ADVAPI32_MODULE_ID = 0xd98aeb041e16f7b5;
		const MODULE_ID USER32_MODULE_ID = 0x4edb7e023b282399;
		const MODULE_ID SHLWAPI_MODULE_ID = 0x770d7bd45a69a95b;
		const MODULE_ID DSOUND_MODULE_ID = 0xcd4b90cfdc684e08;
		const MODULE_ID DXGI_MODULE_ID = 0x2a23d19097137e2b;
		const MODULE_ID OLE32_MODULE_ID = 0xb1d302d2193de26e;
		const MODULE_ID COMBASE_MODULE_ID = 0x150abc0f6a5e7e69;
		const MODULE_ID IPHLPAPI_MODULE_ID = 0xf0fc2d54776870ca;
		const MODULE_ID SETUPAPI_MODULE_ID = 0x52ebaa2d3baf6df8;
		const MODULE_ID UCRTBASED_MODULE_ID = 0x1c08fd93d1714208;
		const MODULE_ID CFGMGR32_MODULE_ID = 0xdbaede9fdf804206;

		const FUNCTION_ID NTDLL_RTLACQUIREPEBLOCK_FUNCTION_ID = 0xdc6db6a634bbf2b5;
		const FUNCTION_ID NTDLL_RTLRELEASEPEBLOCK_FUNCTION_ID = 0x86219b4ccb75d482;

		const FUNCTION_ID NTDLL_LDRGETDLLHANDLE_FUNCTION_ID = 0x681dd6626a594f4f;
		const FUNCTION_ID NTDLL_LDRLOADDLL_FUNCTION_ID = 0x4b536e1ef90d8aff;
		const FUNCTION_ID NTDLL_LDRUNLOADDLL_FUNCTION_ID = 0x7aa0dad6762798ea;
		const FUNCTION_ID NTDLL_LDRGETPROCEDUREADDRESS_FUNCTION_ID = 0x8a84c34b907b3c4;
		const FUNCTION_ID NTDLL_LDRLOCKLOADERLOCK_FUNCTION_ID = 0x8ec4fe94e3acc772;
		const FUNCTION_ID NTDLL_LDRUNLOCKLOADERLOCK_FUNCTION_ID = 0x239487da0a2be225;
		const FUNCTION_ID NTDLL_RTLGETNTVERSIONNUMBERS_FUNCTION_ID = 0xab785fa62a9160ff;
		const FUNCTION_ID NTDLL_RTLANSISTRINGTOUNICODESTIRNG_FUNCTION_ID = 0x43e17c002593e0c4;

		const FUNCTION_ID NTDLL_RTLCHARTOINTEGER_FUNCTION_ID = 0x6a8ad2d0eaa2a13e;

		const FUNCTION_ID UCRTBASED_GMTIME64S_FUNCTION_ID = 0x774008cfe3e0cbfb;
		const FUNCTION_ID UCRTBASED_LOCALTIMES_FUNCTION_ID = 0x1e0a5dabc06d0d28;
		const FUNCTION_ID UCRTBASED_MKTIME64_FUNCTION_ID = 0xe5594060325fbd9d;

		const FUNCTION_ID SETUPAPI_SETUPDIGETCLASSDEVSW_FUNCTION_ID = 0x47cda3752c9b5dd2;
		const FUNCTION_ID SETUPAPI_SETUPDIENUMDEVICEINFO_FUNCTION_ID = 0x1BD2F3202238C91A;
		const FUNCTION_ID SETUPAPI_SETUPDIGETDEVICEREGISTRYPROPERTYW_FUNCTION_ID = 0x95F7127BD19AA9E4;
		const FUNCTION_ID SETUPAPI_SETUPDIGETDEVICEPROPERTYW_FUNCTION_ID = 0x7E68E8A879C7A86D;

		const FUNCTION_ID CFGMGR32_CM_GET_DEVICE_IDW_FUNCTION_ID = 0x30343ec5f6149ae4;

		papiinline uintptr_t GetModuleBaseByHash(MODULE_ID moduleId, bool lockLoader);
		papiinline uintptr_t GetFunctionAddressByHash(MODULE_ID moduleId, FUNCTION_ID functionId, bool lockLoader);

		enum Status
		{
			Success,
			Error_Invalid_Peb,
			Error_Structure_Mismatch,
			Error_Version_Mismatch,
			Error_Debugger_Present,
			Error_Failed_To_Find_Ntdll,
			Error_Failed_To_Find_Kernel32,
			Error_Failed_To_Find_KernelBase,
			Error_Failed_To_Find_RtlGetNtVersionNumbers,
			Error_Failed_To_Find_LdrLockLoaderLock,
			Error_Failed_To_Find_LdrUnlockLoaderLock,
			Error_Failed_To_Find_RtlAnsiStringToUnicodeString,
			Error_Failed_To_Find_LdrGetDllHandle,
			Error_Failed_To_Find_LdrGetProcedureAddress,
			Failure,
		};

		Status Initialize();
		void Shutdown();
#if RSG_DEV
		u64 papiValidateHash(const char *str, u64 code);
		u64 papiValidateHash(const wchar_t *str, u64 code);
#endif
	}
}

#include "papi_crc64.h"
#include "papi_peb.h"
#include "papi_random.h"
#include "papi_string.h"
#include "papi_hash.h"
#include "papi_context.h"
#include "papi_module.h"
#include "papi_function.h"

#if RSG_DEV
#define PAPIHASH(str,code)	papi::papiValidateHash(str, code);
#else
#define PAPIHASH(str,code)	static_cast<u64>(code)
#endif // RSG_DEV
#endif // RSG_PC
#endif // !SECURITY_PAPI_PAPI_H_
