#include "papi.h"

#if RSG_PC
RAGE_DECLARE_CHANNEL(ragesecengine)
RAGE_DEFINE_SUBCHANNEL(ragesecengine, papi)
namespace rage
{
	namespace papi
	{
#if RSG_DEV
		u64 papiValidateHash(const char *str, u64 code)
		{
			u64 generated = GenerateHashEx(str, strlen(str));
			papiAssertf(generated, "FNV Hash for PAPI Call [%s] resulted in 0", str);
			papiAssertf(generated == code, "Generated FNV Hash [%" I64FMT "x] for PAPI Call [%s] does not match the one specified [%" I64FMT "x] ", generated, str, code);
			return generated;
		}
		u64 papiValidateHash(const wchar_t *str, u64 code)
		{
			u64 generated = GenerateHashEx(str, wcslen(str));
			papiAssertf(generated, "FNV Hash for PAPI Call [%ws] resulted in 0", str);
			papiAssertf(generated == code, "Generated FNV Hash [%" I64FMT "x]  for PAPI Call [%ws] does not match the one specified [%" I64FMT "x] ", generated, str, code);
			return generated;
		}
#endif // RSG_DEV
#pragma warning( push )
#pragma warning( disable :  4189 )
		Status Initialize()
		{
			Status initStatus = Status::Success;

			u8 pebBeingDebugged = 0;
			u32 osMajorVersion = 0;
			u32 osMinorVersion = 0;
			u32 osBuildNumber = 0;
			u32 pebOSMajorVersion = 0;
			u32 pebOSMinorVersion = 0;
			u16 pebOSBuildNumber = 0;
			uintptr_t pebImageBaseAddress = 0;
			uintptr_t ldrLockLoaderLock = 0;
			uintptr_t ldrUnlockLoaderLock = 0;
			uintptr_t ldrGetDllHandle = 0;
			uintptr_t ldrGetProcedureAddress = 0;
			uintptr_t rtlAnsiStringToUnicodeString = 0;
			uintptr_t rtlGetNtVersionNumbers = 0;

			// Check if the debugger flag is set in the Peb (BeingDebugged)
			pebBeingDebugged = *reinterpret_cast<u8*>(GetPeb() + PebBeingDebuggedOffset);
			if (pebBeingDebugged != 0 && pebBeingDebugged != 1)
			{
				// This doesn't seem to be the Peb->BeingDebugged flag
				initStatus = Status::Error_Invalid_Peb;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			if (pebBeingDebugged == 1)
			{
				// Process is being debugged, abord
#if RSG_FINAL
				initStatus = Status::Error_Debugger_Present;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
#endif
			}

			if ((*reinterpret_cast<PebLdrData**>(GetPeb() + PebLdrOffset))->Length != sizeof(PebLdrData))
			{
				// Peb Ldr Data structure size mismatch, abord
				initStatus = Status::Error_Structure_Mismatch;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Get the address of Ntdll
			if (GetModuleBaseByHash(NTDLL_MODULE_ID, false) == 0)
			{
				// Failed to get the address of the Ntdll, abord
				initStatus = Status::Error_Failed_To_Find_Ntdll;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Get the address of RtlGetNtVersionNumbers
			rtlGetNtVersionNumbers = GetFunctionAddressByHash(NTDLL_MODULE_ID, NTDLL_RTLGETNTVERSIONNUMBERS_FUNCTION_ID, false);
			if (rtlGetNtVersionNumbers == 0)
			{
				// Failed to get the address of RtlGetNtVersionNumbers, abord
				initStatus = Status::Error_Failed_To_Find_RtlGetNtVersionNumbers;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Get the OS Major Version, Minor Version and Build Number using RtlGetNtVersionNumbers
			reinterpret_cast<RTLGETNTVERSIONNUMBERS>(rtlGetNtVersionNumbers)(&osMajorVersion, &osMinorVersion, &osBuildNumber);

			// Get the OS Major Version value from the PEB - OSMajorVersion field
			pebOSMajorVersion = *reinterpret_cast<u32*>(GetPeb() + PebOSMajorVersionOffset);
			// Get the OS Minor Version value from the PEB - OSMinorVersion field
			pebOSMinorVersion = *reinterpret_cast<u32*>(GetPeb() + PebOSMinorVersionOffset);
			// Get the OS Build Number value from the PEB - OSBuildNumber  field
			pebOSBuildNumber = *reinterpret_cast<u16*>(GetPeb() + PebOSBuildNumberOffset);

			// Compare the version values
			if (osMajorVersion != pebOSMajorVersion || osMajorVersion != pebOSMajorVersion ||
				pebOSBuildNumber != (u16)osBuildNumber)
			{
				// Mismatching OS version values, abort
				initStatus = Status::Error_Version_Mismatch;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Get the address of LdrLockLoaderLock
			ldrLockLoaderLock = GetFunctionAddressByHash(NTDLL_MODULE_ID, NTDLL_LDRLOCKLOADERLOCK_FUNCTION_ID, false);
			if (ldrLockLoaderLock == 0)
			{
				// Failed to get the address of LdrLockLoaderLock, abort
				initStatus = Status::Error_Failed_To_Find_LdrLockLoaderLock;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Get the address of LdrUnlockLoaderLock
			ldrUnlockLoaderLock = GetFunctionAddressByHash(NTDLL_MODULE_ID, NTDLL_LDRUNLOCKLOADERLOCK_FUNCTION_ID, false);
			if (ldrUnlockLoaderLock == 0)
			{
				// Failed to get the address of LdrUnlockLoaderLock, abort
				initStatus = Status::Error_Failed_To_Find_LdrUnlockLoaderLock;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Get the address of RtlAnsiStringToUnicodeString
			rtlAnsiStringToUnicodeString = GetFunctionAddressByHash(NTDLL_MODULE_ID, NTDLL_RTLANSISTRINGTOUNICODESTIRNG_FUNCTION_ID, false);
			if (rtlAnsiStringToUnicodeString == 0)
			{
				// Failed to get the address of RtlAnsiStringToUnicodeString, abort
				initStatus = Status::Error_Failed_To_Find_RtlAnsiStringToUnicodeString;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Get the address of LdrGetDllHandle
			ldrGetDllHandle = GetFunctionAddressByHash(NTDLL_MODULE_ID, NTDLL_LDRGETDLLHANDLE_FUNCTION_ID, false);
			if (ldrGetDllHandle == 0)
			{
				// Failed to get the address of the LdrGetDllHandle, abort
				initStatus = Status::Error_Failed_To_Find_LdrGetDllHandle;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Get the address of LdrGetProcedureAddress
			ldrGetProcedureAddress = GetFunctionAddressByHash(NTDLL_MODULE_ID, NTDLL_LDRGETPROCEDUREADDRESS_FUNCTION_ID, false);
			if (ldrGetProcedureAddress == 0)
			{
				// Failed to get the address of LdrGetProcedureAddress, abort
				initStatus = Status::Error_Failed_To_Find_LdrGetProcedureAddress;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Set the address of LdrLockLoaderLock
			Context::Instance().SetLdrLockLoaderLock(ldrLockLoaderLock);
			// Set the address of LdrUnlockLoaderLock
			Context::Instance().SetLdrUnlockLoaderLock(ldrUnlockLoaderLock);
			// Set the address of LdrGetDllHandle
			Context::Instance().SetLdrGetDllHandle(ldrGetDllHandle);
			// Set the address of LdrGetProcedureAddress
			Context::Instance().SetLdrGetProcedureAddress(ldrGetProcedureAddress);
			// Set the address of RtlAnsiStringToUnicodeString
			Context::Instance().SetRtlAnsiStringToUnicodeString(rtlAnsiStringToUnicodeString);

			// Get the base address of Kernel32
			if (GetModuleBaseByHash(KERNEL32_MODULE_ID, true) == 0)
			{
				// Failed to get the address of Kernel32, abord
				initStatus = Status::Error_Failed_To_Find_Kernel32;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			// Get the address of Kernelbase
			if (GetModuleBaseByHash(KERNELBASE_MODULE_ID, true) == 0)
			{
				// Failed to get the address of KernelBase, abord
				initStatus = Status::Error_Failed_To_Find_KernelBase;
				papiErrorf("PAPI failed to initialize with status %i", initStatus);
				return initStatus;
			}

			return initStatus;
		}
#pragma warning( pop )

		void Shutdown()
		{
			// Set the address of LdrLockLoaderLock, LdrUnlockLoaderLock, LdrGetDllHandle, LdrGetProcedureAddress
			// and RtlAnsiStringToUnicodeString to random values
			Context::Instance().SetLdrLockLoaderLock(GenerateRandom());
			Context::Instance().SetLdrUnlockLoaderLock(GenerateRandom());
			Context::Instance().SetLdrGetDllHandle(GenerateRandom());
			Context::Instance().SetLdrGetProcedureAddress(GenerateRandom());
			Context::Instance().SetRtlAnsiStringToUnicodeString(GenerateRandom());
		}
	} // namespace papi
} // namespace rage
#endif // RSG_PC