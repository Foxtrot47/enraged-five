#ifndef SECURITY_PAPI_PAPI_MODULE_H_
#define SECURITY_PAPI_PAPI_MODULE_H_

#if RSG_PC
namespace rage
{
	namespace papi
	{
		// This function return the number of loaded modules
		papiinline uintptr_t CountModules(bool lockLoader)
		{
			uintptr_t numberOfModules = 0;
			LdrDataTableEntry* loaderTableEntry = 0;
			PebLdrData* loaderTable = 0;
			u32 loaderLockState = 0;
			u64 loaderLockCookie = 0;

			if (lockLoader)
			{
				// The lockLoader flag is set - lock the Loader lock
				if (Context::Instance().GetLdrLockLoaderLock() == 0 ||
					Context::Instance().GetLdrUnlockLoaderLock() == 0)
				{
					// The LdrLockLoaderLock and/or LdrUnlockLoaderLock functions pointers are invalid, abord
					return 0;
				}

				// Call LdrLockLoaderLock
				reinterpret_cast<LDRLOCKLOADERLOCK>(Context::Instance().GetLdrLockLoaderLock())(1, &loaderLockState, &loaderLockCookie);
				if (loaderLockState != 1)
				{
					//Failed to lock the Loader lock, abord
					return 0;
				}
			}

			// Get the loader table
			loaderTable = *reinterpret_cast<PebLdrData**>(GetPeb() + PebLdrOffset);

			// Check if the length is matching
			if (loaderTable->Length != sizeof(PebLdrData))
			{
				// Length is not matching, abord
				if (lockLoader)
				{
					// The lockLoader flag is set - call LdrUnlockLoaderLock
					reinterpret_cast<LDRUNLOCKLOADERLOCK>(Context::Instance().GetLdrUnlockLoaderLock())(1, loaderLockCookie);
				}
				return 0;
			}
			loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTable->InLoadOrderModuleList.Flink);
			do
			{
				numberOfModules++;

				// Get the next entry in the loader table
				loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTableEntry->InLoadOrderLinks.Flink);
			} while (loaderTableEntry->InLoadOrderLinks.Flink != loaderTable->InLoadOrderModuleList.Flink);

			if (lockLoader)
			{
				// The lockLoader flag is set - call LdrUnlockLoaderLock
				reinterpret_cast<LDRUNLOCKLOADERLOCK>(Context::Instance().GetLdrUnlockLoaderLock())(1, loaderLockCookie);
			}

			return numberOfModules;
		}

		// This function return the address of a module by index
		papiinline uintptr_t GetModuleByIndex(bool lockLoader, u32 moduleIndex)
		{
			uintptr_t moduleBase = 0;
			LdrDataTableEntry* loaderTableEntry = 0;
			PebLdrData* loaderTable = 0;
			u32 loaderLockState = 0;
			u64 loaderLockCookie = 0;

			if (lockLoader)
			{
				// The lockLoader flag is set - lock the Loader lock
				if (Context::Instance().GetLdrLockLoaderLock() == 0 ||
					Context::Instance().GetLdrUnlockLoaderLock() == 0)
				{
					// The LdrLockLoaderLock and/or LdrUnlockLoaderLock functions pointers are invalid, abord
					return 0;
				}

				// Call LdrLockLoaderLock
				reinterpret_cast<LDRLOCKLOADERLOCK>(Context::Instance().GetLdrLockLoaderLock())(1, &loaderLockState, &loaderLockCookie);
				if (loaderLockState != 1)
				{
					//Failed to lock the Loader lock, abord
					return 0;
				}
			}

			// Get the loader table
			loaderTable = *reinterpret_cast<PebLdrData**>(GetPeb() + PebLdrOffset);

			// Check if the length is matching
			if (loaderTable->Length != sizeof(PebLdrData))
			{
				// Length is not matching, abord
				if (lockLoader)
				{
					// The lockLoader flag is set - call LdrUnlockLoaderLock
					reinterpret_cast<LDRUNLOCKLOADERLOCK>(Context::Instance().GetLdrUnlockLoaderLock())(1, loaderLockCookie);
				}
				return 0;
			}

			loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTable->InLoadOrderModuleList.Flink);
			do
			{
				// Generate a hash of the the module name and compare it to the module hash (ID) provided
				if (moduleIndex-- == 0)
				{
					// The hashes are matching
					moduleBase = loaderTableEntry->BaseAddress;
					break;
				}
				// Get the next entry in the loader table
				loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTableEntry->InLoadOrderLinks.Flink);
			} while (loaderTableEntry->InLoadOrderLinks.Flink != loaderTable->InLoadOrderModuleList.Flink);

			if (lockLoader)
			{
				// The lockLoader flag is set - call LdrUnlockLoaderLock
				reinterpret_cast<LDRUNLOCKLOADERLOCK>(Context::Instance().GetLdrUnlockLoaderLock())(1, loaderLockCookie);
			}

			return moduleBase;
		}

		// Returns the address of a module by name
		papiinline uintptr_t GetModuleByName(bool lockLoader, const wchar_t* moduleName)
		{
			uintptr_t moduleBase = 0;
			LdrDataTableEntry* loaderTableEntry = 0;
			PebLdrData* loaderTable = 0;
			u32 loaderLockState = 0;
			u64 loaderLockCookie = 0;

			if (lockLoader)
			{
				// The lockLoader flag is set - lock the Loader lock
				if (Context::Instance().GetLdrLockLoaderLock() == 0 ||
					Context::Instance().GetLdrUnlockLoaderLock() == 0)
				{
					// The LdrLockLoaderLock and/or LdrUnlockLoaderLock functions pointers are invalid, abord
					return 0;
				}

				// Call LdrLockLoaderLock
				reinterpret_cast<LDRLOCKLOADERLOCK>(Context::Instance().GetLdrLockLoaderLock())(1, &loaderLockState, &loaderLockCookie);
				if (loaderLockState != 1)
				{
					//Failed to lock the Loader lock, abord
					return 0;
				}
			}

			// Get the loader table
			loaderTable = *reinterpret_cast<PebLdrData**>(GetPeb() + PebLdrOffset);

			// Check if the length is matching
			if (loaderTable->Length != sizeof(PebLdrData))
			{
				// Length is not matching, abord
				if (lockLoader)
				{
					// The lockLoader flag is set - call LdrUnlockLoaderLock
					reinterpret_cast<LDRUNLOCKLOADERLOCK>(Context::Instance().GetLdrUnlockLoaderLock())(1, loaderLockCookie);
				}
				return 0;
			}

			loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTable->InLoadOrderModuleList.Flink);
			do
			{
				// Generate a hash of the the module name and compare it to the module hash (ID) provided
				if (_wcscmp(loaderTableEntry->BaseDllName.Buffer, moduleName) == 0)
				{
					// The hashes are matching
					moduleBase = loaderTableEntry->BaseAddress;
					break;
				}
				// Get the next entry in the loader table
				loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTableEntry->InLoadOrderLinks.Flink);
			} while (loaderTableEntry->InLoadOrderLinks.Flink != loaderTable->InLoadOrderModuleList.Flink);

			if (lockLoader)
			{
				// The lockLoader flag is set - call LdrUnlockLoaderLock
				reinterpret_cast<LDRUNLOCKLOADERLOCK>(Context::Instance().GetLdrUnlockLoaderLock())(1, loaderLockCookie);
			}

			return moduleBase;
		}

		// Returns the address of a module by hash
		papiinline uintptr_t GetModuleBaseByHash(MODULE_ID moduleId, bool lockLoader = true)
		{
			uintptr_t moduleBase = 0;
			LdrDataTableEntry* loaderTableEntry = 0;
			PebLdrData* loaderTable = 0;
			u32 loaderLockState = 0;
			u64 loaderLockCookie = 0;

			if (lockLoader)
			{
				// The lockLoader flag is set - lock the Loader lock
				if (Context::Instance().GetLdrLockLoaderLock() == 0 ||
					Context::Instance().GetLdrUnlockLoaderLock() == 0)
				{
					// The LdrLockLoaderLock and/or LdrUnlockLoaderLock functions pointers are invalid, abord
					return 0;
				}

				// Call LdrLockLoaderLock
				reinterpret_cast<LDRLOCKLOADERLOCK>(Context::Instance().GetLdrLockLoaderLock())(1, &loaderLockState, &loaderLockCookie);
				if (loaderLockState != 1)
				{
					//Failed to lock the Loader lock, abord
					return 0;
				}
			}

			// Get the loader table
			loaderTable = *reinterpret_cast<PebLdrData**>(GetPeb() + PebLdrOffset);

			// Check if the length is matching
			if (loaderTable->Length != sizeof(PebLdrData))
			{
				// Length is not matching, abord
				if (lockLoader)
				{
					// The lockLoader flag is set - call LdrUnlockLoaderLock
					reinterpret_cast<LDRUNLOCKLOADERLOCK>(Context::Instance().GetLdrUnlockLoaderLock())(1, loaderLockCookie);
				}
				return 0;
			}

			loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTable->InLoadOrderModuleList.Flink);
			do
			{
				// Generate a hash of the the module name and compare it to the module hash (ID) provided
				if (GenerateHashEx(reinterpret_cast<const wchar_t*>(loaderTableEntry->BaseDllName.Buffer)) == moduleId)
				{
					// The hashes are matching
					moduleBase = loaderTableEntry->BaseAddress;
					break;
				}
				// Get the next entry in the loader table
				loaderTableEntry = reinterpret_cast<LdrDataTableEntry*>(loaderTableEntry->InLoadOrderLinks.Flink);
			} while (loaderTableEntry->InLoadOrderLinks.Flink != loaderTable->InLoadOrderModuleList.Flink);

			if (lockLoader)
			{
				// The lockLoader flag is set - call LdrUnlockLoaderLock
				reinterpret_cast<LDRUNLOCKLOADERLOCK>(Context::Instance().GetLdrUnlockLoaderLock())(1, loaderLockCookie);
			}

			return moduleBase;
		}
	} // namespace papi
} // namespace rage
#endif // RSG_PC
#endif // !SECURITY_PAPI_PAPI_MODULE_H_
