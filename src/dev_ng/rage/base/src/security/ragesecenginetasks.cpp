#include "rline/rltask.h"
#include "security/ragesecengine.h"
#include "security/ragesecenginetasks.h"
#include "system/bootmgr.h"

#if USE_RAGESEC

using namespace rage;
extern sysThreadPool sm_rageSecThreadPool;
RAGE_DEFINE_SUBCHANNEL(ragesecengine, tasks);
rageSecGenericTask::rageSecGenericTask()
	: m_State(STATE_STARTED)
{
}

void rageSecGenericWorkItem::DoWork()
{
	RageSecDebugf3("Performing Work for plugin: 0x%x", m_id);
	m_function();
}

bool rageSecGenericWorkItem::Configure(RageSecPluginWorkFunction func, unsigned int id)
{
	rtry
	{
		// Check and assign our variables
		rverify(func, catchall, );
		m_function = func;
		rverify(id, catchall,);
		m_id = id;
		return true;
	}
	rcatchall
	{
		return false;
	}
}

netTaskStatus rageSecGenericTask::OnUpdate(int* /*resultCode*/)
{
	netTaskStatus status = NET_TASKSTATUS_PENDING;

	switch(m_State)
	{
	case STATE_STARTED:
		if(WasCanceled())
		{
			m_workItem.Cancel();
			netTaskDebug("Canceled - bailing 0x%08X", m_id);
			status = NET_TASKSTATUS_FAILED;
		}
		else
		{
			netTaskDebug("Configuring 0x%08X ", m_id);
			// Configure the work item status
			//@@: location RAGESECGENERICTASK_CONFIGURE
			m_workItem.Configure(m_function, m_id);
			// Queue the work item
			netTaskDebug("Queuing 0x%08X", m_id);
			sm_rageSecThreadPool.QueueWork(&m_workItem);
			m_State = STATE_WORKING;
		}
		break;
	case STATE_WORKING:
		if (m_workItem.Finished())
		{
			netTaskDebug("Completed 0x%08X", m_id);
			m_State = STATE_COMPLETED;
		}
		break;
	case STATE_COMPLETED:
		status = NET_TASKSTATUS_SUCCEEDED;
		break;
	default:
		status = NET_TASKSTATUS_FAILED;
		break;
	}

	return status;
}

#endif // USE_RAGESEC
