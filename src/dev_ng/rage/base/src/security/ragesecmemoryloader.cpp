#include "ragesecmemoryloader.h"
#if USE_RAGESEC
#if RAGE_SEC_SUPPORT_DLL_PLUGINS
#include <windows.h>
#include <winnt.h>

#define GET_HEADER_DICTIONARY(context, index)  &(context)->headers->OptionalHeader.DataDirectory[index]
#define ALIGN_DOWN(address, alignment)      (LPVOID)((uintptr_t)(address) & ~((alignment) - 1))

typedef BOOL (WINAPI *DllMainFunction)(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved);

// Indicate that we're in the rage namespace
using namespace rage;

int RageSecMemoryLoader::MemoryProtectionFlags[2][2][2]  = {
	{
		// not executable
		{PAGE_NOACCESS, PAGE_WRITECOPY},
		{PAGE_READONLY, PAGE_READWRITE},
	}, {
		// executable
		{PAGE_EXECUTE, PAGE_EXECUTE_WRITECOPY},
		{PAGE_EXECUTE_READ, PAGE_EXECUTE_READWRITE},
	},
};

HMODULE WindowsLoadLibrary(LPCTSTR filename)
{
	//HMODULE result = LoadLibraryEx(filename, NULL, LOAD_LIBRARY_SEARCH_DEFAULT_DIRS);
	HMODULE result = LoadLibrary(filename);
	if (result == NULL) {
		return NULL;
	}
	return result;
}

void WindowsFreeLibrary(HMODULE module)
{
	FreeLibrary((HMODULE) module);
}

FARPROC WindowsGetProcAddress(HMODULE module, LPCSTR name)
{
	return (FARPROC) GetProcAddress((HMODULE) module, name);
}

HMODULE RageSecMemoryLoader::LoadIntoMemory(const void * rawData)
{
	PRAGESEC_LOADLIBRARYCONTEXT ctx;
	PIMAGE_DOS_HEADER dosHeader;
	PIMAGE_NT_HEADERS ntHeader;

	unsigned char * bytes = NULL;
	unsigned char * headers = NULL;
	SIZE_T			relocationAddressDifference;
	SYSTEM_INFO		sysInfo;

	dosHeader = (PIMAGE_DOS_HEADER)rawData;
	// Check for MZ
	if(dosHeader->e_magic!= IMAGE_DOS_SIGNATURE)
	{
		RageSecError("Data loaded doesn't suggest that it's a DOS header");
		return NULL;
	}

	ntHeader = (PIMAGE_NT_HEADERS)&((const unsigned char*)(rawData))[dosHeader->e_lfanew];
	if(ntHeader->Signature != IMAGE_NT_SIGNATURE)
	{
		RageSecError("Data loaded doesn't suggest that it's in an appropriate executable format");
		return NULL;
	}

	if(ntHeader->FileHeader.Machine != IMAGE_FILE_MACHINE_AMD64)
	{
		RageSecError("Data loaded doesn't suggest that it's appropriately a 64-bit binary.");
		return NULL;
	}

	if(ntHeader->OptionalHeader.SectionAlignment & 1)
	{
		RageSecError("Data loaded doesn't suggest that the section alignments are compatible.");
		return NULL;
	}

	// Now lets allocate our bytes - at any address
	int allocationAttempts = 0;
	while (bytes == NULL && allocationAttempts < 5)
	{
		RageSecDebugf3("Attempting to allocate %d bytes for a module", ntHeader->OptionalHeader.SizeOfImage);
		bytes = (unsigned char *)VirtualAlloc(NULL, ntHeader->OptionalHeader.SizeOfImage, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
		allocationAttempts++;
	}
	// Check to be sure if we've loaded, and inform appropriately.
	if(bytes == NULL)
	{
		RageSecError("Failed to load module of %d bytes", ntHeader->OptionalHeader.SizeOfImage);
		return NULL;
	}

	ctx = rage_new RAGESEC_LOADLIBRARYCONTEXT;
	memset(ctx, 0, sizeof(RAGESEC_LOADLIBRARYCONTEXT));

	//ctx = (PRAGESEC_LOADLIBRARYCONTEXT)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(RAGESEC_LOADLIBRARYCONTEXT));
	if(ctx == NULL)
	{
		RageSecError("Failed to allocate space for the DLL context.");
		VirtualFree(bytes, 0, MEM_RELEASE);
	}

	// Copy over the data, for our context
	ctx->imageBytes = bytes;

	// Fetch our system information
	GetNativeSystemInfo(&sysInfo);

	// Copy over our headers into a commited block
	headers = (unsigned char *)VirtualAlloc(bytes, ntHeader->OptionalHeader.SizeOfHeaders, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	// Copy the bytes over with memcpy
	memcpy(headers, dosHeader, ntHeader->OptionalHeader.SizeOfHeaders);
	ctx->headers = (PIMAGE_NT_HEADERS)&((const unsigned char *)(headers))[dosHeader->e_lfanew];
	
	// Now indicate the image base that we loaded from the virtual allocation
	ctx->headers->OptionalHeader.ImageBase = (uintptr_t)bytes;

	if(!LoadSections((const unsigned char *) rawData, ntHeader, ctx))
	{
		RageSecError("There was an error when attempting to load the section data of the image.");
		FreeLibrary((HMODULE)ctx);
		return NULL;
	}

	relocationAddressDifference = (SIZE_T)(bytes - ntHeader->OptionalHeader.ImageBase);
	if(relocationAddressDifference == 0)
	{
		ctx->isRelocated = TRUE;
	}
	else
	{
		ctx->isRelocated = PerformBaseRelocation(ctx, relocationAddressDifference);
	}

	if(!LoadImportTable(ctx))
	{
		RageSecError("There was an error when attempting to load the import table of the image.");
		FreeLibrary((HMODULE)ctx);
		return NULL;
	}

	if(!CompleteSections(ctx))
	{
		RageSecError("There was an unable to complete the sections of the memory mapped binary.");
		FreeLibrary((HMODULE)ctx);
		return NULL;
	}

	if(!HandleThreadLocalStorage(ctx))
	{
		RageSecError("There was a failure in handling the thread level storage.");
		FreeLibrary((HMODULE)ctx);
		return NULL;
	}

	// I forgot that in my modules I don't actually have an entry point- just a pointer that
	// does stuff - so it doesnt' make sense to auto start DLL_MAIN.
#if 0
	// OMG now we can execute!
	if(ctx->headers->OptionalHeader.AddressOfEntryPoint != 0)
	{
		DllMainFunction dllMain = (DllMainFunction)(bytes + ctx->headers->OptionalHeader.AddressOfEntryPoint);
		BOOL didAttach = (*dllMain)((HINSTANCE)bytes, DLL_PROCESS_ATTACH, 0);
		if(!didAttach)
		{
			RageSecError("DLL initialization failed");
			FreeLibrary((HMODULE)ctx);
			return NULL;
		}
		ctx->isInitialized = TRUE;
	}
#else
	ctx->isInitialized = TRUE;
#endif
	return (HMODULE)ctx;
}
void RageSecMemoryLoader::FreeLibrary(HMODULE module)
{
	PRAGESEC_LOADLIBRARYCONTEXT ctx = (PRAGESEC_LOADLIBRARYCONTEXT)module;
	if(ctx == NULL)
	{
		return;
	}

	if(ctx->isInitialized)
	{
		DllMainFunction dllEntry = (DllMainFunction)(ctx->imageBytes+ctx->headers->OptionalHeader.AddressOfEntryPoint);
		// Call Detach
		(*dllEntry)((HINSTANCE)ctx->imageBytes, DLL_PROCESS_DETACH,0) ;
	}

	if(ctx->modules !=NULL)
	{
		for (unsigned int i = 0; i < ctx->numberModules; i++)
		{
			WindowsFreeLibrary(ctx->modules[i]);
		}
		free(ctx->modules);

	}

	if(ctx->imageBytes !=NULL)
	{
		VirtualFree(ctx->imageBytes, 0, MEM_RELEASE);
	}
}
FARPROC RageSecMemoryLoader::GetProcAddressByOrdinal(HMODULE module, DWORD ordinalNumber)
{
	PRAGESEC_LOADLIBRARYCONTEXT ctx = (PRAGESEC_LOADLIBRARYCONTEXT)module;
	unsigned char *codeBase = ((PRAGESEC_LOADLIBRARYCONTEXT)module)->imageBytes;
	PIMAGE_EXPORT_DIRECTORY exports;
	PIMAGE_DATA_DIRECTORY directory = GET_HEADER_DICTIONARY(ctx, IMAGE_DIRECTORY_ENTRY_EXPORT);
	if (directory->Size == 0) {
		RageSecError("No export table found.");
		return NULL;
	}

	exports = (PIMAGE_EXPORT_DIRECTORY) (codeBase + directory->VirtualAddress);
	if (exports->NumberOfFunctions == 0) {
		// DLL doesn't export anything
		RageSecError("No function at the DLL Export");
		return NULL;
	}

	if (exports->NumberOfFunctions < ordinalNumber) 
	{
		// Ordinal just doesn't exist within the available number of functions
		RageSecError("Invalid ordinal number.");
		return NULL;
	}

	// AddressOfFunctions contains the RVAs to the "real" functions
	return (FARPROC) (codeBase + (*(DWORD *) (codeBase + exports->AddressOfFunctions + ((ordinalNumber-1)*4))));
}
BOOL RageSecMemoryLoader::HandleThreadLocalStorage(PRAGESEC_LOADLIBRARYCONTEXT ctx)
{
	unsigned char * imageBase = ctx->imageBytes;
	PIMAGE_TLS_DIRECTORY tls;
	PIMAGE_TLS_CALLBACK *callback;

	PIMAGE_DATA_DIRECTORY dir = GET_HEADER_DICTIONARY(ctx, IMAGE_DIRECTORY_ENTRY_TLS);

	if(dir->VirtualAddress == 0)
		return TRUE;

	tls = (PIMAGE_TLS_DIRECTORY)(imageBase+dir->VirtualAddress);
	callback = (PIMAGE_TLS_CALLBACK *) tls->AddressOfCallBacks;
	if(callback)
	{
		while(*callback)
		{
			(*callback)((void*)imageBase, DLL_PROCESS_ATTACH, NULL);
			callback++;
		}
	}
	return TRUE;
}
DWORD RageSecMemoryLoader::CalculateSectionSize(PRAGESEC_LOADLIBRARYCONTEXT ctx, PIMAGE_SECTION_HEADER section)
{
	DWORD size = section->SizeOfRawData;
	if(size == 0)
	{
		if(section->Characteristics & IMAGE_SCN_CNT_INITIALIZED_DATA)
		{
			size = ctx->headers->OptionalHeader.SizeOfInitializedData;
		}
		else if(section->Characteristics & IMAGE_SCN_CNT_UNINITIALIZED_DATA)
		{
			size = ctx->headers->OptionalHeader.SizeOfUninitializedData;
		}
	}
	return size;
}
BOOL RageSecMemoryLoader::CompleteSections(PRAGESEC_LOADLIBRARYCONTEXT ctx)
{
	
	PIMAGE_SECTION_HEADER currSection = IMAGE_FIRST_SECTION(ctx->headers);

	uintptr_t imageOffset = (ctx->headers->OptionalHeader.ImageBase & 0xFFFFFFFF00000000);
	RAGESEC_SECTIONDATA sectionData;
	sectionData.address = (void *)((uintptr_t)currSection->Misc.PhysicalAddress | imageOffset);
	sectionData.alignedAddress = ALIGN_DOWN(sectionData.address, ctx->pageSize);
	sectionData.sectionSize = CalculateSectionSize(ctx, currSection);
	sectionData.sectionCharacteristics = currSection->Characteristics;

	sectionData.isLastSection = FALSE;
	currSection++;

	for(int i = 1; i< ctx->headers->FileHeader.NumberOfSections; i++, currSection++)
	{
		void * sectionAddress = (void *)((uintptr_t)currSection->Misc.PhysicalAddress | imageOffset);
		void * alignedAddress = ALIGN_DOWN(sectionAddress, ctx->pageSize);
		DWORD sectionSize = CalculateSectionSize(ctx, currSection);

		if(sectionData.alignedAddress == alignedAddress 
			|| (uintptr_t)sectionData.address + sectionData.sectionSize > (uintptr_t) alignedAddress)
		{
			if(	   (currSection->Characteristics & IMAGE_SCN_MEM_DISCARDABLE) == 0 
				|| (sectionData.sectionCharacteristics & IMAGE_SCN_MEM_DISCARDABLE) == 0)
			{
				sectionData.sectionCharacteristics = (sectionData.sectionCharacteristics | currSection->Characteristics) & ~IMAGE_SCN_MEM_DISCARDABLE;
			}
			else
			{
				sectionData.sectionCharacteristics |= currSection->Characteristics;
			}

			sectionData.sectionSize = (DWORD)((((uintptr_t)sectionAddress)+ sectionSize) - (uintptr_t)sectionData.address);
			continue;
		}

		if(!CompleteSectionData(ctx, &sectionData))
		{
			RageSecError("Failed to complete section.");
			return FALSE;
		}
	}
	return TRUE;
}
BOOL RageSecMemoryLoader::CompleteSectionData(PRAGESEC_LOADLIBRARYCONTEXT ctx, PRAGESEC_SECTIONDATA sectionData)
{
	DWORD sectionProtection = 0;
	DWORD oldSectionProtection = 0;
	BOOL executablePage = FALSE;
	BOOL readablePage = FALSE;
	BOOL writable = FALSE;

	// If there aren't any sections, just mosey on
	if(sectionData->sectionSize == 0)
		return TRUE;

	if(sectionData->sectionCharacteristics & IMAGE_SCN_MEM_DISCARDABLE)
	{
		// If we don't need the section, lets go ahead nd free it
		if(sectionData->address == sectionData->alignedAddress 
			&& 
			(
				sectionData->isLastSection 
				|| ctx->headers->OptionalHeader.SectionAlignment == ctx->pageSize 
				|| (sectionData->sectionSize % ctx->pageSize) == 0
			)
		)	
		{
			VirtualFree(sectionData->address, sectionData->sectionSize, MEM_DECOMMIT);
		}
		return true;
	}

	executablePage	= (sectionData->sectionCharacteristics & IMAGE_SCN_MEM_EXECUTE) != 0;
	readablePage	= (sectionData->sectionCharacteristics & IMAGE_SCN_MEM_READ) != 0;
	writable		= (sectionData->sectionCharacteristics & IMAGE_SCN_MEM_WRITE) != 0;

	sectionProtection = MemoryProtectionFlags[executablePage][readablePage][writable];
	if(sectionData->sectionCharacteristics & IMAGE_SCN_MEM_NOT_CACHED)
	{
		sectionProtection |= PAGE_NOCACHE;
	}

	if(!VirtualProtect(sectionData->address, sectionData->sectionSize, sectionProtection, &oldSectionProtection))
	{
		RageSecError("Unable to correctly protect the memory page accordingly.");
		return FALSE;
	}

	return TRUE;
}
BOOL RageSecMemoryLoader::PerformBaseRelocation(PRAGESEC_LOADLIBRARYCONTEXT ctx, SIZE_T relocationAddressDifference)
{

	unsigned char * imageBase = ctx->imageBytes;
	PIMAGE_BASE_RELOCATION relocation;

	PIMAGE_DATA_DIRECTORY dir = GET_HEADER_DICTIONARY(ctx, IMAGE_DIRECTORY_ENTRY_BASERELOC);
	if(dir->Size == 0)
	{
		if(relocationAddressDifference == 0)
			return TRUE;
		else
			return FALSE;
	}

	relocation = (PIMAGE_BASE_RELOCATION)(imageBase + dir->VirtualAddress);

	while(relocation->VirtualAddress > 0)
	{
		unsigned char * dest = imageBase + relocation->VirtualAddress;
		unsigned short *relocationInfo = (unsigned short *)((unsigned char *)relocation + sizeof(IMAGE_BASE_RELOCATION));
		for(int i = 0; i < ((relocation->SizeOfBlock-sizeof(IMAGE_BASE_RELOCATION)) / 2); i++, relocationInfo++)
		{
			// Ensure that this only works for 64 bit machines, otherwise it's GG
			DWORD * patchAddrHighLow;
			ULONGLONG *patchAddressSixtyFour = 0;
			int relocationType = 0;
			int relocationOffset = 0;
			relocationType = *relocationInfo >> 12;
			relocationOffset = *relocationInfo & 0x0FFF;
			
			if(relocationType == IMAGE_REL_BASED_HIGHLOW)
			{
				patchAddrHighLow = (DWORD*)(dest+relocationOffset);
				*patchAddrHighLow +=(DWORD)relocationAddressDifference;
			}
			else if(relocationType == IMAGE_REL_BASED_DIR64)
			{
				patchAddressSixtyFour = (ULONGLONG*)(dest+relocationOffset);
				*patchAddressSixtyFour +=(ULONGLONG)relocationAddressDifference;
			}
			else
			{
				// Do nothing
			}
		}

		// Iterate to the next relocation
		relocation = (PIMAGE_BASE_RELOCATION)(((char*)relocation) + relocation->SizeOfBlock);
	}
	return TRUE;
}
BOOL RageSecMemoryLoader::CanReadMemory(void *ptr)
{
	SIZE_T                          dw;
	MEMORY_BASIC_INFORMATION		mbi;
	int                             ok;

	dw = VirtualQuery(ptr, &mbi, sizeof(mbi));
	ok = (
			(mbi.Protect & PAGE_READONLY) 
		||	(mbi.Protect & PAGE_READWRITE) 
		||	(mbi.Protect & PAGE_WRITECOPY) 
		||	(mbi.Protect & PAGE_EXECUTE_READ) 
		||	(mbi.Protect & PAGE_EXECUTE_READWRITE) 
		||	(mbi.Protect & PAGE_EXECUTE_WRITECOPY)
		);

	// check the page is not a guard page
	if (mbi.Protect & PAGE_GUARD)
		ok = FALSE;
	if (mbi.Protect & PAGE_NOACCESS)
		ok = FALSE;

	return ok;
}

BOOL RageSecMemoryLoader::LoadImportTable(PRAGESEC_LOADLIBRARYCONTEXT ctx)
{
	unsigned char * imageBase = ctx->imageBytes;
	PIMAGE_IMPORT_DESCRIPTOR importDescriptor;

	PIMAGE_DATA_DIRECTORY dir = GET_HEADER_DICTIONARY(ctx, IMAGE_DIRECTORY_ENTRY_IMPORT);
	// If there are no imports, just mosey along
	if(dir->Size == 0)
		return TRUE;

	BOOL didFail = FALSE;
	importDescriptor = (PIMAGE_IMPORT_DESCRIPTOR)(imageBase + dir->VirtualAddress);
	while(CanReadMemory(importDescriptor) && importDescriptor->Name)
	{
		//RageSecDebugf3("Importing %s", importDescriptor->Name);
		uintptr_t * thunkRef;
		FARPROC * funcRef;
		HMODULE * temp;
		// Call Load Library on each of the imports
		// HMODULE moduleHandle = WindowsLoadLibrary((LPCSTR)(imageBase+importDescriptor->Name));
		HMODULE moduleHandle = LoadLibraryA((char*)(imageBase+importDescriptor->Name));
		if(moduleHandle == NULL)
		{
			RageSecError("Unable to load the dependent library %s.", (LPCSTR)(imageBase+importDescriptor->Name));
			didFail = TRUE;
			break;
		}
		// Realloc the number of modules that we've detected, by +1. Wish I could do another algorithm 
		// to be more efficient about this. I'll deal.
		temp = (HMODULE*)realloc(ctx->modules, (ctx->numberModules+1)*(sizeof(HMODULE)));
		if(temp == NULL)
		{
			RageSecError("Unable to reallocate the space used for allocateding the modules within a context.");
			didFail = TRUE;
			break;
		}

		ctx->modules = temp;

		ctx->modules[ctx->numberModules++] = moduleHandle;

		if(importDescriptor->OriginalFirstThunk)
		{
			thunkRef = (uintptr_t *)(imageBase + importDescriptor->OriginalFirstThunk);
			funcRef = (FARPROC *)(imageBase + importDescriptor->FirstThunk);
		}
		else
		{
			thunkRef = (uintptr_t *)(imageBase + importDescriptor->FirstThunk);
			funcRef = (FARPROC *)(imageBase + importDescriptor->FirstThunk);
		}

		while(*thunkRef)
		{
			if(IMAGE_SNAP_BY_ORDINAL(*thunkRef))
			{
				*funcRef = WindowsGetProcAddress(moduleHandle, (LPCSTR)IMAGE_ORDINAL(*thunkRef));
			}
			else
			{
				PIMAGE_IMPORT_BY_NAME thunkData = (PIMAGE_IMPORT_BY_NAME)(imageBase + (*thunkRef));
				*funcRef = WindowsGetProcAddress(moduleHandle, (LPCSTR)&thunkData->Name);
			}

			if(*funcRef == 0)
			{
				RageSecError("Unable to find reference to one of the imported functions");

			}

			thunkRef++;
			funcRef++;
		}

		if(didFail)
		{
			WindowsFreeLibrary(moduleHandle);
			RageSecError("The loading of the import address table has failed.");
			return FALSE;
		}

		importDescriptor++;
	}
	return TRUE;
}

BOOL RageSecMemoryLoader::LoadSections(const unsigned char * rawData, PIMAGE_NT_HEADERS ntHeader, PRAGESEC_LOADLIBRARYCONTEXT ctx)
{
	
	unsigned char * imageBase = ctx->imageBytes;
	unsigned char * dest = NULL;
	int size = 0;

	PIMAGE_SECTION_HEADER currSect = IMAGE_FIRST_SECTION(ctx->headers);
	for(int i = 0; i < ctx->headers->FileHeader.NumberOfSections; i++, currSect++)
	{
		if(currSect->SizeOfRawData == 0)
		{
			size = ntHeader->OptionalHeader.SectionAlignment;
			// Handle empty section
			if(size <= 0)
				continue;
			
			// Allocate our bytes.
			dest = (unsigned char *)VirtualAlloc(imageBase + currSect->VirtualAddress, size, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
			if(!dest)
			{
				RageSecError("Unable to allocate space for a section of the current module");
				return FALSE;
			}

			dest = imageBase + currSect->VirtualAddress;
			// Indicate what the physical address of the block allocated was
			currSect->Misc.PhysicalAddress = (DWORD)(uintptr_t)dest;
			// Now initialize it to 0, before we copy over our bytes
			memset(dest, 0, size);
		}

		// Copy over the data from our raw data
		dest = (unsigned char *)VirtualAlloc(imageBase+currSect->VirtualAddress, currSect->SizeOfRawData, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
		// Check to be sure that we allcoated
		if(!dest)
		{
			RageSecError("Unable to allocate space for the code block of a section.");
			return FALSE;
		}

		dest = imageBase + currSect->VirtualAddress;
		// Execute the memcpy
		memcpy(dest, rawData + currSect->PointerToRawData, currSect->SizeOfRawData);
	}

	return TRUE;

}
#endif // RAGE_SEC_SUPPORT_DLL_PLUGINS
#endif // USE_RAGESEC
