#ifndef SECURITY_RAGESECPLUGINMANAGER_H
#define SECURITY_RAGESECPLUGINMANAGER_H

#include "security/ragesecengine.h"

#if USE_RAGESEC
#include "atl/map.h"
#include "atl/queue.h"
#include "security/ragesecplugin.h"
// This class is responsible for managing 
// all of the plugins that are created and executed at runtime.
// All this is responsible for is storing the registered plugins
// and maintaining a state of them. Sound easy right?
#define RAGE_SEC_SUPPORT_DLL_PLUGINS	0 && (RSG_PC)
#define RAGE_SEC_LOAD_LOCAL_DLL_PLUGINS 0 && (RSG_PC && __BANK && RAGE_SEC_SUPPORT_DLL_PLUGINS)
#define RAGE_SEC_CHECK_PLUGIN_SIGNATURE 0 && (RSG_PC && __BANK)
#define RAGE_SEC_MAX_NUMBER_BONDERS					128
#define RAGE_SEC_MAX_NUMBER_REACTION_QUEUE_EVENTS	1024
#define RAGE_SEC_INIT_FUNCTION_ORDINAL_NUMBER		1

// Forward declare, so we can create our object.

namespace rage
{
	class rageSecPluginManager
	{
	private:
	
		// Global tracker that automatically increments when an object is registered and 
		// doesn't provide an identifier.
		// PS, why am i asking the plugins to provide their own identifier?
		static unsigned int sm_idTracker;

		// This map is responsible for storing the registered plugins, with the first parameter
		// being the unsigned int identifier of the plugin
		static atMap<unsigned int, RageSecPlugin*> sm_registeredPlugins;
		// This map is responsible for managing the status' of each of the workitems. 
		// This will be valuable for things like call backs, and also resetting for period tasks
		static atMap<unsigned int, netStatus *> sm_pluginsStatus;

		//PURPOSE:	Function that takes a plugin and delegates its worker function
		//			to the thread pool
		// plugin:	Pointer to a RageSecPlugin that has it's function pointers set
		//			to do work
		// status:	Pointer to a netStatus object to keep track of the plugin's 
		//			progress in time
		static void CreatePluginTask(RageSecPlugin *plugin, netStatus *status);

		// Array of our loaded DLLs, so we can clean them up later
		static atArray<void*> sm_dllsLoaded;

		//PURPOSE:	Facility function that destroys and cleans up 
		//			a single plugin provided it's plugin ID
		// id:		id of the plugin to destroy
		static void DestroyPlugin(unsigned int id);

		//PURPOSE:	Loads a DLL and it's plugins provided a path to the DLL
		// path:	ASCII string that points to the path of the DLL
		static void LoadDll(const wchar_t * path);
		
		static bool sm_needsRebalancing;

	public:
		static bool NeedsRebalancing() { return sm_needsRebalancing; }

		static void SetRebalance(bool b) { sm_needsRebalancing = b; }

		static void GetPluginUpdateReferences(atArray<u32*> &);
		

		// Method for registering a plugin with the game
		//	key		: unsigned int that represents a unique identifier for the plugin
		//	params	: RageSecPluginRegistrationParameters pointer object representing the qualifying
		//			  aspects of the plugin	
		static bool RegisterPluginFunction(RageSecPluginParameters * params, unsigned int = 0);
		
		// PURPOSE: Update loop that does quick management and delegating
		static void Update();
		// PURPOSE: Initialization function
		//			Also responsible for registering compiled-in-plugins
		static void Init();

		// PURPOSE: Re-Initialization function
		//			Also responsible for registering compiled-in-plugins
		static void ReInit();

		//PURPOSE:	Function that frees the memory associated with the plugin manager
		static void ShutdownClass();

		//PURPOSE:  To clean up any remaining tasks before shutting down
		static bool CancelTasks();

	};

} // rage namespace
#endif

#endif