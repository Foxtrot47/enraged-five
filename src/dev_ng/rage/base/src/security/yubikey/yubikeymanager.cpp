#include "yubikeymanager.h"

#if RAGE_SEC_ENABLE_YUBIKEY
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "input/keyboard.h"
#include "input/mapper.h"
#include "math/random.h"
#include "string/stringutil.h"
#include "system/xtl.h"
#include "yubikey.h"

// Define the channel
RAGE_DEFINE_SUBCHANNEL(ragesecengine, yubiKey)

#if YK_GENERATE_DECRYPT_KEY
#include "data/aes.h"
#include "data/base64.h"
#include "string/string.h"
#include "TFIT_defs_iAES3.h"
#include "TFIT_defs_iAES4.h"
#define YK_GENERATED_KEY_BUFFER 1024
#endif

#define YK_FULL_LOCAL_VERIFY 0
#define YW_REQUEST_ID_TAG "id"
#define YW_REQUEST_OTP_TAG "otp"
#define YW_REQUEST_NONCE_TAG "nonce"

#define YW_RESPONSE_STATUS_TAG "status"
#define YW_RESPONSE_STATUS_OK_TAG "OK"

// Public ID + Token
#if YK_FULL_LOCAL_VERIFY
#define YK_STR_PUBLIC_ID "vvvjujvdvire"
#define YK_STR_PRIVATE_KEY "ac0c9803f381618880cdf27f125f3da2"
#endif //YK_FULL_LOCAL_VERIFY

#define YK_YUBIKEY_VENDOR_ID 0x1050
#define YK_YUBIKEY_PRODUCT_ID 0x0407
#define YK_WOLF_KEY_SIZE 32

#if !RSG_FINAL 
	PARAM(ignoreYubikeyResponseVerification, "Ignore the YK server");
#endif //RSG_FINAL

#if YK_GENERATE_DECRYPT_KEY
PARAM(yubikeyTag, "Force the yubikey tag for debugging");
void Debug_GenerateKeys(AESKeyMgr::KeyDefinition& key, u8* wolfBuffer);
#endif //YK_GENERATE_DECRYPT_KEY

const char* URL_HOSTNAME = "ykval.rockstargames.com";
const char* SERVICE_PATH = "wsapi/2.0/verify";

namespace rage
{
	CYubiKeyManager* CYubiKeyManager::sm_instance = NULL;

	yubikeyVerificationHttpTask::yubikeyVerificationHttpTask()
	{
		m_responseOK = false;
	}

	yubikeyVerificationHttpTask::~yubikeyVerificationHttpTask()
	{

	}

	bool yubikeyVerificationHttpTask::Configure(const char* otp, const char* nonce)
	{
		rtry
		{
			rverify(otp, catchall,);
			rverify(nonce, catchall, );

			memset(m_nonce, 0, sizeof(m_nonce));
			memcpy(m_nonce, nonce, strlen(nonce));

			rlHttpTaskConfig finalConfig;
			finalConfig.m_HttpVerb = NET_HTTP_VERB_GET;

			rverify(rlHttpTask::Configure(&finalConfig), catchall, );

			// Use the OTP to fetch the ID
			u64 id = CYubiKeyManager::PublicIdStringToU64(otp);

			// Shove it into a string
			atStringBuilder sb;

			// Use the full ID, as it's always unique
			u32 IDV1 = (u8)(id >> 40);

			switch (IDV1)
			{
			case 0xCC:
			case 0xCE:
			case 0xCF:
			case 0xD0:
			case 0xD1:
			case 0xD5:
			case 0xD6:
				id = (id & 0xFF);
				break;
			default:
				id = (id & 0xFF) | (IDV1 << 8);
				break;
			}

			char convertBuf[64] = { '\0' };
			sprintf_s(convertBuf, "%" I64FMT "d", id);
			sb.Append(convertBuf);

			// Append the fields
			rverify(AddQueryParam(YW_REQUEST_ID_TAG, sb.ToString(), sb.Length()), catchall, );
			rverify(AddQueryParam(YW_REQUEST_OTP_TAG, otp, istrlen(otp)), catchall, );
			rverify(AddQueryParam(YW_REQUEST_NONCE_TAG, nonce, istrlen(nonce)), catchall, );
		}
		rcatchall
		{
			return false;
		}

		return true;
	}

	const char* yubikeyVerificationHttpTask::GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const
	{
		StringCopyTruncateFront(hostnameBuf, URL_HOSTNAME, sizeofBuf);
		return hostnameBuf;
	}

	bool yubikeyVerificationHttpTask::GetServicePath(char* svcPath, const unsigned maxLen) const
	{
		StringCopyTruncateFront(svcPath, SERVICE_PATH, maxLen);
		return true;
	}

	atMap<atString, atString> yubikeyVerificationHttpTask::ParseResponse(const char* response)
	{
		atMap<atString, atString> parsed;
		if (!response)
			return parsed;
		atString atResp(response);
		atArray<atString> responseLines;
		atResp.Split(responseLines, "\r\n");

		if (responseLines.GetCount() == 0)
			return parsed;

		for (int i = 0; i < responseLines.GetCount(); i++)
		{
			atArray<atString> linePair;
			int separator = responseLines[i].IndexOf("=");
			atString name;
			atString value;
			name.Set(responseLines[i], 0, separator);
			value.Set(responseLines[i], separator+1, responseLines[i].GetLength()-separator);
			parsed.Insert(name, value);
		}

		return parsed;
	}

	bool yubikeyVerificationHttpTask::ValidateParsedResponse(atMap<atString, atString> parsed)
	{
		atString *status = parsed.Access(YW_RESPONSE_STATUS_TAG);
		if (!status)
			return false;

		if (strncmp(status->c_str(), YW_RESPONSE_STATUS_OK_TAG, sizeof(YW_RESPONSE_STATUS_OK_TAG)) != 0)
			return false;

		atString *nonce = parsed.Access(YW_REQUEST_NONCE_TAG);
		if (!nonce)
			return false;

		if (strncmp(m_nonce, nonce->c_str(),nonce->GetLength()) != 0)
			return false;

		return true;
	}

	bool yubikeyVerificationHttpTask::ProcessResponse(const char* response, int& /*resultCode*/)
	{
		bool success = false;

		rtry
		{
			rcheck(response, catchall, rlError("Null response body"));
			atMap<atString, atString> parsed = ParseResponse(response);
#if !RSG_FINAL
			if (PARAM_ignoreYubikeyResponseVerification.Get() == false)
#endif //RSG_FINAL
			{
				rverifyall(ValidateParsedResponse(parsed));
			}

			CYubiKeyManager::GetInstance()->SetRemoteResponse(true);

#if YK_GENERATE_DECRYPT_KEY
			CYubiKeyManager::GetInstance()->SetRemoteResponse(true);
#endif //YK_GENERATE_DECRYPT_KEY
			success = true;
		}
		rcatchall
		{
			success = false;
			CYubiKeyManager::GetInstance()->SetRemoteResponse(false);
		}

		return success;
	}

	void CYubiKeyManager::Init()
	{
		m_oldDebugKeyboardState = ioMapper::GetEnableKeyboard();
		ioMapper::SetEnableKeyboard(false);
		Reset();
	}

	void CYubiKeyManager::Shutdown()
	{
		if (sm_instance != NULL)
		{
			delete sm_instance;
			sm_instance = NULL;
		}
	}

	void CYubiKeyManager::Reset()
	{
		m_isVerified = false;
		m_isLocalVerified = false;
		m_isRemoteVerified = false;
		m_isYubiKeyConnected = false;
		m_isDoneWorking = false;
		m_verificationAttempts = 0;
		m_hasRemoteResponse = false;
		m_netStatus.Reset();
	}


	static bool IsYubikey(HANDLE hDevice)
	{
		RID_DEVICE_INFO rdi;
		rdi.cbSize = sizeof(RID_DEVICE_INFO);
		UINT size = 256;
		TCHAR tBuffer[256] = { 0 };

		if (GetRawInputDeviceInfo(hDevice, RIDI_DEVICENAME, tBuffer, &size) < 0)
			return false;

		UINT cbSize = rdi.cbSize;
		if (GetRawInputDeviceInfo(hDevice, RIDI_DEVICEINFO, &rdi, &cbSize) < 0)
			return false;

		if (rdi.dwType == RIM_TYPEHID && rdi.hid.dwVendorId == YK_YUBIKEY_VENDOR_ID && rdi.hid.dwProductId == YK_YUBIKEY_PRODUCT_ID)
			return true;

		return false;
	}

	void CYubiKeyManager::CheckForYubikey()
	{
		bool foundYubiKey = false;

		UINT nDevices;
		if (GetRawInputDeviceList(NULL, &nDevices, sizeof(RAWINPUTDEVICELIST)) != 0)
			return;

		PRAWINPUTDEVICELIST pRawInputDeviceList = (PRAWINPUTDEVICELIST)malloc(sizeof(RAWINPUTDEVICELIST) * nDevices);
		if (pRawInputDeviceList == NULL)
			return;

		UINT nNoOfDevices = GetRawInputDeviceList(pRawInputDeviceList, &nDevices, sizeof(RAWINPUTDEVICELIST));
		if (nNoOfDevices == ((UINT)-1))
			return;

		for (UINT i = 0; i < nNoOfDevices; i++)
		{
			if (IsYubikey(pRawInputDeviceList[i].hDevice))
			{
				foundYubiKey = true;
				break;
			}
		}

		free(pRawInputDeviceList);

		m_isYubiKeyConnected = foundYubiKey;
	}

	CYubiKeyManager* CYubiKeyManager::Create()
	{
		if (sm_instance == NULL)
		{
			sm_instance = new CYubiKeyManager();
			sm_instance->Init();
		}

		return sm_instance;
	}


	void CYubiKeyManager::LocalVerify()
	{
		if (m_inputQueue.IsFull())
		{
			// the '\0' at the end is needed as they use strlen inside yubikey_parse :(
			m_lastInputKey[YW_STR_INPUT_BUFFER_SIZE] = '\0';
			for (int i = 0; i < YW_STR_INPUT_BUFFER_SIZE; ++i)
				m_lastInputKey[i] = m_inputQueue[i];
#if YK_FULL_LOCAL_VERIFY
			// Commenting this out; I don't necessarily care about a single identity any more
			if (strncmp(YK_STR_PUBLIC_ID, (char*)m_lastInputKey, YK_STR_PUBLIC_ID_SIZE) == 0)
			{
				{
					for (int i = YK_STR_PUBLIC_ID_SIZE; i < m_inputQueue.GetCount(); ++i)
						m_lastInputKey[i] = m_inputQueue[i];

					yubikey_token_st tok;
					yubikey_parse(&m_lastInputKey[YK_STR_PUBLIC_ID_SIZE], m_key, &tok);

					if (yubikey_crc_ok_p((uint8_t *)& tok))
					{
						m_isLocalVerified = true;
						for (int i = 0; i < YW_PRIVATE_ID_SIZE; ++i)
							m_privateId[i] = tok.uid[i];
					}
				}
			}
#endif // YK_FULL_LOCAL_VERIFY
			m_isLocalVerified = true;
			++m_verificationAttempts;
			m_inputQueue.Reset();
		}
	}

	void CYubiKeyManager::CreateHttpTask()
	{
		const u32 nonceSize = 16;
		char nonce[nonceSize + 1];

		mthRandom rnd(sysTimer::GetSystemMsTime());
		nonce[nonceSize] = 0;
		for (int i = 0; i < nonceSize; ++i)
			nonce[i] = (char)rnd.GetRanged('a', 'z');

		yubikeyVerificationHttpTask* task = NULL;

		rtry
		{
			rverify(rlGetTaskManager()->CreateTask(&task), catchall, );
			m_netStatus.Reset();

			// Configure the task
			rverify(rlTaskBase::Configure(task, (char*)m_lastInputKey, nonce, &m_netStatus), catchall, );
			rverify(rlGetTaskManager()->AddParallelTask(task), catchall, );
		}
		rcatchall
		{
			if (task != NULL)
			{
				if (task->IsPending())
				{
					task->Finish(rlTaskBase::FINISH_FAILED);
				}

				rlGetTaskManager()->DestroyTask(task);
			}
		}
	}

	CYubiKeyManager* CYubiKeyManager::GetInstance()
	{
		return sm_instance;
	}

	void CYubiKeyManager::OnDone()
	{
		m_isDoneWorking = true;
	}

	u64 CYubiKeyManager::PublicIdStringToU64(const char *input)
	{
		// Container for the id
		u64 id = 0;
		yubikey_modhex_decode((char*)&id, (char*)input, sizeof(u64));
		// It reads in, with incorrect endian-ness, and since 
		// the size of the ID is 6 bytes, we need to shift it so it 
		// aligns properly
		u64 inverted = sysEndian::BtoN(id << 16);
		return inverted;
	}

	void CYubiKeyManager::RemoteVerify()
	{
#if !YW_ENABLE_REMOTE_VERIFICATION
		m_isRemoteVerified = true;
		m_isVerified = true;
#else // YW_ENABLE_REMOTE_VERIFICATION
		if (m_netStatus.Pending())
		{
			// just wait
		}
		else if (m_netStatus.None())
		{

			CreateHttpTask();
		}
		else
		{
			// check if everything was okay
			if (m_hasRemoteResponse)
			{
				m_isRemoteVerified = true;
				m_isVerified = true;
			}
			else
			{
				Reset();
			}
		}
#endif // YW_ENABLE_REMOTE_VERIFICATION
	}

	void CYubiKeyManager::AddChar(char ch)
	{
		if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9'))
		{
			if (m_inputQueue.IsFull())
				m_inputQueue.Drop();
			m_inputQueue.Push((char)ch);
			LocalVerify();
		}
		else if (ch == 13)
		{
			m_inputQueue.Reset();
		}
		m_resetTimer = sysTimer::GetSystemMsTime();
	}

	bool CYubiKeyManager::Update()
	{
		if (m_isDoneWorking)
			return false;

		// Always check for the YubiKey in the event that it is disconnected.
		CheckForYubikey();

		// Process the result after the YubiKey has been disconnected.
		if (m_isVerified && !m_isYubiKeyConnected)
		{
			OnDone();
			return false;
		}

		// Queue up a check for a YubiKey
		if (!m_isYubiKeyConnected)
			return true;

		// YubiKey requires remote verification.
		if (m_isLocalVerified && !m_isRemoteVerified)
		{
			RemoteVerify();
			return true;
		}

		// Queue up remote verification
		if (m_isLocalVerified)
		{
			return true;
		}

		if (sysTimer::HasElapsedIntervalMs(m_resetTimer, RESET_TIMER_THRESHOLD))
		{
			yubiKeyDebugf1("Resetting the input queue.");
			m_inputQueue.Reset();
			m_resetTimer = sysTimer::GetSystemMsTime();
		}

		const char16 *pKeysPressed = ioKeyboard::GetInputText();
		if(pKeysPressed == NULL || pKeysPressed[0] == '\0')
			return true;
		else
			yubiKeyDebugf3("Received %s from input text.", pKeysPressed);

		for (u32 i = 0; i < ioKeyboard::TEXT_BUFFER_LENGTH && pKeysPressed[i] != '\0'; ++i)
			AddChar(((char*)&pKeysPressed[i])[0]);

		std::string inputQueueStr;
		for (int i = 0; i < m_inputQueue.GetSize(); ++i)
			inputQueueStr += m_inputQueue[i];

		yubiKeyDebugf1("Current input queue contents: %s", inputQueueStr.c_str());
		return true;
	}
} // namespace rage

#endif // ENABLE_SECURITY_YUBIKEY
