#ifndef RAGE_SEC_YUBIKEY_WRAPPER_H
#define RAGE_SEC_YUBIKEY_WRAPPER_H

#include "security/ragesecengine.h"

#if RAGE_SEC_ENABLE_YUBIKEY

#include <atl/queue.h>
#include <data/rson.h>
#include <rline/rlhttptask.h>

// Declare sub channel
RAGE_DECLARE_SUBCHANNEL(ragesecengine,yubiKey)
#define yubiKeyDisplayf(fmt,...)		RAGE_DISPLAYF(##ragesecengine##_##yubiKey,fmt,##__VA_ARGS__)
#define yubiKeyDebugf1(fmt,...)			RAGE_DEBUGF1(##ragesecengine##_##yubiKey,fmt,##__VA_ARGS__)
#define yubiKeyDebugf2(fmt,...)			RAGE_DEBUGF2(##ragesecengine##_##yubiKey,fmt,##__VA_ARGS__)
#define yubiKeyDebugf3(fmt,...)			RAGE_DEBUGF3(##ragesecengine##_##yubiKey,fmt,##__VA_ARGS__)

#define YW_ENABLE_REMOTE_VERIFICATION 1
#define YK_GENERATE_DECRYPT_KEY 0

	// string version (2 chars for every value)
#define YK_STR_PUBLIC_ID_SIZE 12
#define YK_STR_TOKEN_SIZE 32
#define YW_STR_INPUT_BUFFER_SIZE YK_STR_PUBLIC_ID_SIZE + YK_STR_TOKEN_SIZE
	// normal 
#define YW_YUBIKEY_KEY_SIZE 16
#define YW_PRIVATE_ID_SIZE 6

	namespace rage
{
	class yubikeyVerificationHttpTask : public rlHttpTask
	{
	public:
		RL_TASK_DECL(yubikeyVerificationHttpTask);

		yubikeyVerificationHttpTask();
		virtual ~yubikeyVerificationHttpTask();

		bool Configure(const char* otp, const char* nonce);

	protected:
		virtual bool UseHttps() const override { return true; }

		virtual const char* GetUrlHostName(char* hostnameBuf, const unsigned sizeofBuf) const;
		virtual bool GetServicePath(char* svcPath, const unsigned maxLen) const;
		virtual bool ProcessResponse(const char* response, int& resultCode);

	private:
		bool ProcessResponse(RsonReader& rr);
		bool ValidateParsedResponse(atMap<atString, atString> parsed);
		atMap<atString, atString> ParseResponse(const char*);
		char m_nonce[64];
		bool m_responseOK;
	};

	class CYubiKeyManager
	{
	public:
		static CYubiKeyManager* Create();
		static CYubiKeyManager* GetInstance();
		static u64 PublicIdStringToU64(const char *id);
		static void Shutdown();

		bool Update();

		bool IsVerified() { return m_isVerified; }
		bool IsYubiKeyConnected() { return m_isYubiKeyConnected; }
		u32 GetVerificationAttempts() { return m_verificationAttempts; }
		void SetRemoteResponse(bool value) { m_hasRemoteResponse = value; }

	private:
		void Init();
		void Reset();

		void LocalVerify();
		void RemoteVerify();
		void AddChar(char ch);
		void CheckForYubikey();
		void OnDone();
		void CreateHttpTask();

		u8 m_key[YW_YUBIKEY_KEY_SIZE];
		atQueue<char, YW_STR_INPUT_BUFFER_SIZE> m_inputQueue;
		u8 m_lastInputKey[YW_STR_INPUT_BUFFER_SIZE + 1];

		bool m_isVerified;
		bool m_isLocalVerified;
		bool m_isRemoteVerified;
		bool m_hasRemoteResponse;
		bool m_isYubiKeyConnected;
		bool m_oldDebugKeyboardState;
		bool m_isDoneWorking;
		u32 m_verificationAttempts;

		netStatus m_netStatus;

		static const unsigned int RESET_TIMER_THRESHOLD = 5000;
		u32 m_resetTimer;
		u8 m_privateId[YW_PRIVATE_ID_SIZE];

		static CYubiKeyManager* sm_instance;
	};
} // namespace rage

#endif // RAGE_SEC_ENABLE_YUBIKEY
#endif // RAGE_SEC_YUBIKEY_WRAPPER_H
