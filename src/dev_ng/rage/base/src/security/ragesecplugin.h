#ifndef SECURITY_RAGESECPLUGIN_H
#define SECURITY_RAGESECPLUGIN_H


#define PLUGIN_VERSION_MAJOR 1
#define PLUGIN_VERSION_MINOR 0

#include "security/obfuscatedtypes.h"
using namespace rage;

enum RageSecPluginExecutionType 
{
	EXECUTE_INVALID,
	EXECUTE_ONCE,
	EXECUTE_PERIODIC,
	EXECUTE_PERIODIC_RANDOM,
	EXECUTE_SCHEDULED,
	EXECUTE_ALWAYS
};

enum RageSecPluginState 
{
	STATE_INVALID,
	STATE_INSTANTIATED,
	STATE_CREATED,
	STATE_GRINDING,
	STATE_CONFIGURED_AND_QUEUED,
	STATE_NEEDS_DESTROYING
};
// Forward declaration of our service interface
// More below regarding this
class RageSecPluginManagerServices;

// Basic versioning for our plugins with a major & minor detail
class RageSecPluginVersion
{
public:
	unsigned int major;
	unsigned int minor;
};

// I know these are all the same, but I'm enumerating them separately in the event
// that some of them may need more or less arguments

// Creating the function to initialize the plugin
typedef bool (*RageSecPluginInitializeFunction)();
// Creating the return function pointer type for a create function
typedef bool (*RageSecPluginCreateFunction)();
// Creating the return function pointer type  for destruction
typedef bool (*RageSecPluginDestroyFunction)();
// Creating the return function pointer type  for the worker function; 
// this is actually what gets executed in the generic work item
typedef bool (*RageSecPluginWorkFunction)();
// Creating the cancellation function for a plugin
typedef bool (*RageSecPluginCancelFunction)();
// Creating the configuration function for a plugin
typedef bool (*RageSecPluginConfigurationFunction)();
// Creating the success function for a plugin
typedef bool (*RageSecPluginOnSuccessFunction)();
// Creating the failure function for a plugin
typedef bool (*RageSecPluginOnFailureFunction)();

class RageSecPluginExtendedInformation 
{
public:
	unsigned int startTime;
	unsigned int checkTime;
	unsigned int frequencyInMs;
	unsigned int delay;
};

// Basic parameters that a module needs ot specify in order
// to register itself with our plugin manager
// - The version it was compiled with
// - Pointer to it's create function
// - Pointer to it's work function 
// - Pointer to it's destruction function
// - The identifier; if not specified by the plugin, then it's set
// - The type of the plugin (based on the enum that helps characterize
//		what type of a plugin it is)
// - The type also dictates how the extended information is handled.
//		I didn't know of a good way to add even more parameters
//		to a generic plugin, so I made an extended information type
//		with fields that get conditionally used.
//		I tried utilizing a void * at runtime, but I ran into problems
//		when referencing the copy constructor to pass to the register
//		function. 
//		This is somewhat by design. This helps the plugin writer
//		not have to worry about the basic memory management of the plugin
//		itself, but they DO have to worry about any of the memory that they
//		globally allocate for the purposes of their work. I hope.
// by the engine itself.
class RageSecPluginParameters
{
public:
	// Functions
	
	RageSecPluginConfigurationFunction	Configure;
	RageSecPluginCreateFunction			Create;
	RageSecPluginWorkFunction			DoWork;
	RageSecPluginDestroyFunction		Destroy;
	RageSecPluginCancelFunction			OnCancel;
	RageSecPluginOnSuccessFunction		OnSuccess;
	RageSecPluginOnFailureFunction		OnFailure;
	
	// Common Variables
	unsigned int						id;
	RageSecPluginVersion				version;
	RageSecPluginExecutionType			type;
	RageSecPluginExtendedInformation    extendedInfo;
	
};

 
class RageSecPlugin
{
public:
	RageSecPlugin()
	{
		
		parameters.Configure = 0;
		parameters.Create = 0;
		parameters.DoWork = 0;
		parameters.Destroy = 0;
		parameters.OnCancel = 0;
		parameters.OnSuccess = 0;
		parameters.OnFailure = 0;

		parameters.id = 0;
		parameters.version.minor = 0;
		parameters.version.major = 0;
		parameters.type = EXECUTE_INVALID;
		parameters.extendedInfo.startTime = 0;
		parameters.extendedInfo.checkTime = 0;
		parameters.extendedInfo.frequencyInMs = 0;
		parameters.extendedInfo.delay = 0;
		// Initialize our time to be the construciton of this object
		updateReference = rage_new unsigned int;
	}
	~RageSecPlugin()
	{
		delete updateReference;
	}


	RageSecPluginParameters parameters;
	RageSecPluginState		state;
	unsigned int			*updateReference;
	// Functions to facilitate life
	RageSecPluginExecutionType Type()		{ return parameters.type;}
	unsigned int Id()						{ return parameters.id; }
	RageSecPluginState	State()				{ return state; }
	void SetState(RageSecPluginState st)	{ state = st; }
	void SetTime(unsigned int time)			{*updateReference = time; }
	
};
// Switching from an enum, and setting each of these as generated random numbers
// That way my code for each rage-sec-pop-reaction doesn't only
// optimize to be a bunch of decrements.
enum RageSecPluginGameReactionType
{
	REACT_INVALID					= 0xDE6C0BC0,
	REACT_TELEMETRY					= 0xFC5F8D39,
	REACT_P2P_REPORT				= 0xC34731F6,
	REACT_DESTROY_MATCHMAKING		= 0xC85BE919, 
	REACT_KICK_RANDOMLY				= 0xDC294D77,
	REACT_SET_VIDEO_MODDED_CONTENT	= 0xCA189698,
	REACT_GPU_CRASH					= 0xE4D062B8,
	REACT_GAMESERVER				= 0x4F7147A9 
};

#define GENERIC_GAME_ID 0xB370B4B2

class RageSecPluginGameReactionObject
{
public:
	RageSecPluginGameReactionObject()
	{
		type = REACT_INVALID;
		id = 0;
		data = 0;
	}
	RageSecPluginGameReactionObject(RageSecPluginGameReactionType _type, unsigned int _id, unsigned int _versionType, unsigned int _address,  unsigned int _data)
	{
		type = _type;
		id = _id;
		version.Set(_versionType);
		address.Set(_address);
		data.Set(_data);
	}
	RageSecPluginGameReactionObject(RageSecPluginGameReactionType _type, unsigned int _id)
	{
		type = _type;
		id = _id;
	}
	RageSecPluginGameReactionType type;
	unsigned int id;

	sysObfuscated<unsigned int> version;
	sysObfuscated<unsigned int> address;
	sysObfuscated<unsigned int> data;
};


// Here we define the things that we're going to expose in the game to the plugin.
// This isn't that big of a deal when we're creating plugins in the binary, but in the
// the context of a DLL, it may need to know what services the game can provide for it
// such as: telemetry reporting, p2p reporting, kicking, etc.

// Right now the only service I care about is registration
typedef bool (*RageSecEnginePluginRegistrationFunction)(RageSecPluginParameters * params, unsigned int key);
// Expanding the service to include Punishment actions
typedef bool (*RageSecEnginePluginQueueGameReactionObjectFunction)(RageSecPluginGameReactionObject obj);


#endif