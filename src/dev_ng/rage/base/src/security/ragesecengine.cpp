
#include "fwmaths/random.h"
#include "fwsys/gameskeleton.h"
#include "grcore/allocscope.h"
#include "grprofile/timebars.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/bootmgr.h"
#include "system/ipc.h"
#include "system/threadpool.h"
#include "security/ragesecengine.h"
#include "security/ragesecenginetasks.h"
#include "security/ragesecplugin.h"
#include "security/ragesecpluginmanager.h"
using namespace rage;

#if USE_RAGESEC
// Instantiate our static members

// RageSecEngine Static Members
// These are detailed in the header file, so I won't enumerate them here.
bool rageSecEngine::sm_initialized = false;
volatile bool rageSecEngine::sm_continueUpdate = true;
sysIpcThreadId rageSecEngine::sm_updateThread;
rageSecPluginManager rageSecEngine::sm_pluginManager;

// Global static variables (for threading)
sysThreadPool::Thread sm_rageSecWorkerThreads[2];
sysThreadPool sm_rageSecThreadPool;

atQueue<RageSecPluginGameReactionObject, RAGE_SEC_MAX_NUMBER_REACTION_QUEUE_EVENTS> rageSecEngine::sm_rageLandReactions;
static atQueue<RageSecPluginGameReactionObject, RAGE_SEC_MAX_NUMBER_REACTION_QUEUE_EVENTS> sm_queuedPeerReports;

// Define our channel
RAGE_DEFINE_CHANNEL(ragesecengine)

// Function is responsible for initializing RageSec.
void rageSecEngine::Init(u32 initializationMode)
{
	//@@: range RAGESECENGINE_INIT {
	// We only care about core initialization
	//@@: location RAGESECENGINE_INIT_ENTRY
	if(initializationMode != INIT_CORE)
		return;

	RageSecDebugf3("Initializing");

	
	if(sm_initialized)
	{
		RageSecError("Initalization failed; already initialized");
		return;
	}

	// Initialize our worker threads
	//@@: range RAGESECENGINE_INIT_INNER {
	sm_rageSecThreadPool.Init();
	sm_rageSecThreadPool.AddThread(&sm_rageSecWorkerThreads[0], "@SH", RAGESEC_THREADPOOL_CPU_1, sysIpcMinThreadStackSize, PRIO_NORMAL);
	sm_rageSecThreadPool.AddThread(&sm_rageSecWorkerThreads[1], "+A@", RAGESEC_THREADPOOL_CPU_2, sysIpcMinThreadStackSize, PRIO_NORMAL);

	//@@: } RAGESECENGINE_INIT_INNER
	// Create our thread
	RageSecDebugf3("Creating main thread");
#if RAGE_SEC_IN_SEPARATE_THREAD
	sm_updateThread = sysIpcCreateThread(rageSecEngine::rageSecUpdateThread, NULL, 1024 * 16, PRIO_BELOW_NORMAL, "RageSec Thread");

	// check to be sure that we didn't get an invalid thread ID
	if(sm_updateThread == sysIpcThreadIdInvalid)
	{
		RageSecError("Invalid thread created");
		return;
	}
#endif
	// Return happiness
	//@@: location RAGESECENGINE_INIT_EXIT
	// Set our initialization to true
	sm_initialized = true;
	return;
	//@@: } RAGESECENGINE_INIT

}
void rageSecEngine::rageSecUpdateThread(void*)
{
#if RAGE_SEC_IN_SEPARATE_THREAD
	// Initializing my time bars
	PF_INIT_TIMEBARS("RageSecurity", 1280, 1);
	// First thing we want to do is load up our default tasks
	// that we're going to want with every execution
	// Giving myself two milliseconds to boot
	PF_START_TIMEBAR_BUDGETED("RageSec Update", 2.0f);
	RegisterPeriodicTasks();
	Update();
#endif
}

// Function to re-initialize, in the event that one of the threads is suspiciously killed
void rageSecEngine::ReInit()
{
	// Re-create our threads
	if(!sm_rageSecWorkerThreads[0].IsAlive())
	{
		sm_rageSecThreadPool.ResetThread(&sm_rageSecWorkerThreads[0]);
		sm_rageSecThreadPool.AddThread(&sm_rageSecWorkerThreads[0], "@SH", RAGESEC_THREADPOOL_CPU_1, sysIpcMinThreadStackSize, PRIO_NORMAL);
	}
	if(!sm_rageSecWorkerThreads[1].IsAlive())	
	{
		sm_rageSecThreadPool.ResetThread(&sm_rageSecWorkerThreads[1]);
		sm_rageSecThreadPool.AddThread(&sm_rageSecWorkerThreads[1], "+A@", RAGESEC_THREADPOOL_CPU_2, sysIpcMinThreadStackSize, PRIO_NORMAL);
	}
	// I don't think I need to do anything for the plugin manager because it's a separate entity...I think.
	// Yeah - all the queued work just goes to the thread poo
	return;
}

void rageSecEngine::Update()
{
#if RAGE_SEC_IN_SEPARATE_THREAD
	while(true)
#endif
	{
		// Check to see if any of our threads are dead. If so, go ahead and re-initialize them.
		if(!sm_rageSecWorkerThreads[1].IsAlive() || !sm_rageSecWorkerThreads[0].IsAlive() )
		{
			//@@: location RAGESECENGINE_UPDATE_REINIT
			ReInit();
		}
			
		
		if(sm_initialized)
		{
			// Initalize our on-screen time bar
#if RAGE_SEC_IN_SEPARATE_THREAD
			PF_FRAMEINIT_TIMEBARS(0);
			// Do the meat of the work, and push the value 
			PF_PUSH_TIMEBAR_BUDGETED("RageSec Task Management", 1.0f);
#endif
			// Before we do any work, lets check the sanity of our threads
			
#if RAGE_SEC_ENABLE_PLUGIN_ACTIONS  
			//UpdatePeriodicTasks();
			sm_pluginManager.Update();
#endif

#if RAGE_SEC_IN_SEPARATE_THREAD
			// Pop the value
			PF_POP_TIMEBAR();
			// End the frame time bar
			PF_FRAMEEND_TIMEBARS();
#endif
		}
		
		// Do a sleep so we're not always grinding
#if RAGE_SEC_IN_SEPARATE_THREAD
		if (!sm_continueUpdate)
			break;
		else
			sysIpcSleep(33);
#endif
	}
}

void rageSecEngine::Shutdown(unsigned int shutdownMode)
{
	// Do something with our shutdown mode if we need to
	if(shutdownMode != SHUTDOWN_CORE)
		return;
	//@@: location RAGESECENGINE_SHUTDOWN_SHUTDOWN_CLASS
	ShutdownClass();
}

void rageSecEngine::ShutdownClass()
{
	// Only do work if we've actually initialized
	if(!sm_initialized)
		return;

	// Set our flag to indicate that we dont' need to update any more
	sm_continueUpdate = true;

	// Kill thread stuff
	sysIpcWaitThreadExit(sm_updateThread);
	sm_updateThread = sysIpcThreadIdInvalid;
	// Break our loop
	sm_initialized = false;

	sm_pluginManager.ShutdownClass();
}
#endif