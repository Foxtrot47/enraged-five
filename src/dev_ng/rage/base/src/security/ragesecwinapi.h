#ifndef SECURITY_RAGESECWINAPI_H
#define SECURITY_RAGESECWINAPI_H

#if RSG_PC
#include "system/xtl.h"
#include "papi/papi.h"

namespace rage
{
	extern const papi::Status g_PapiInit;

	namespace NtDll
	{
		extern NtOpenProcessFnPtr NtOpenProcess;
		extern NtQueryVirtualMemoryFnPtr NtQueryVirtualMemory;
		extern NtQuerySystemInformationFnPtr NtQuerySystemInformation;
		extern NtQueryInformationProcessFnPtr NtQueryInformationProcess;

		extern void InitNtDll();

		// Identifiers for NtDll functions.
		enum NtFunctionsToVerify
		{
			NT_QUERY_VIRTUAL_MEMORY,
			NT_QUERY_SYSTEM_INFORMATION,
			NT_OPEN_PROCESS,
			NT_QUERY_INFORMATION_PROCSES,

			NUM_FUNCTIONS
		};
		// Global NtDll function cache that stores NtDll function addresses.
		extern u64 g_functionCache[NtFunctionsToVerify::NUM_FUNCTIONS];
		// Global flag to indicate if NtDll has been invalidated.
		extern bool g_bDllScrewed;

		// Store the first 8 bytes of tracked NtDll functions.
		inline void InitProtection()
		{
			g_functionCache[NT_OPEN_PROCESS] = *(u64*)NtOpenProcess;
			g_functionCache[NT_QUERY_VIRTUAL_MEMORY] = *(u64*)NtQueryVirtualMemory;
			g_functionCache[NT_QUERY_SYSTEM_INFORMATION] = *(u64*)NtQuerySystemInformation;
			g_functionCache[NT_QUERY_INFORMATION_PROCSES] = *(u64*)NtQueryInformationProcess;
		}

		// Verify that the first 8 bytes of each function have not changed.
		inline void VerifyProtection()
		{
			if(g_bDllScrewed == false &&
				(
				g_functionCache[NT_OPEN_PROCESS] != *(u64*)NtOpenProcess||
				g_functionCache[NT_QUERY_VIRTUAL_MEMORY] != *(u64*)NtQueryVirtualMemory ||
				g_functionCache[NT_QUERY_SYSTEM_INFORMATION] != *(u64*)NtQuerySystemInformation ||
				g_functionCache[NT_QUERY_INFORMATION_PROCSES] != *(u64*)NtQueryInformationProcess
				)
				)
			{
				g_bDllScrewed = true;
			}
		}

		inline bool HasBeenHacked() { return g_bDllScrewed; }

		class NtDllInitialiser
		{
		public:
			NtDllInitialiser() { InitNtDll(); }
		};
	}

	namespace Kernel32
	{
		extern LocalFreeFnPtr LocalFree;
		extern LocalAllocFnPtr LocalAlloc;
		extern OpenProcessFnPtr OpenProcess;
		extern CloseHandleFnPtr CloseHandle;
		extern GetCurrentProcessFnPtr GetCurrentProcess;
		extern EnumProcessModulesFnPtr EnumProcessModules;
		extern GetModuleFileNameAFnPtr GetModuleFileNameA;
		extern GetModuleFileNameWFnPtr GetModuleFileNameW;
		extern GetModuleFileNameExAFnPtr GetModuleFileNameExA;
		extern GetModuleFileNameExWFnPtr GetModuleFileNameExW;
		extern ReadProcessMemoryFnPtr ReadProcessMemory;
		extern GetModuleInformationFnPtr GetModuleInformation;

		// Determine usage of unicode functions.
#ifdef UNICODE
#define GetModuleFileNameFnPtr		GetModuleFileNameWFnPtr
#define GetModuleFileNameExFnPtr	GetModuleFileNameExWFnPtr
#else
#define GetModuleFileNameFnPtr		GetModuleFileNameAFnPtr
#define GetModuleFileNameExFnPtr	GetModuleFileNameExAFnPtr
#endif // !UNICODE

		extern GetModuleFileNameFnPtr GetModuleFileNameFn;
		extern GetModuleFileNameExFnPtr GetModuleFileNameExFn;

		extern void InitKernel32();

		// Identifiers for Kernel32 functions.
		enum K32FunctionToVerify
		{
			// LocalFree
			LOCAL_FREE,
			// LocalAlloc
			LOCAL_ALLOC,
			// OpenProcess
			OPEN_PROCESS,
			// CloseHandle
			CLOSE_HANDLE,
			// GetCurrentProcess
			GET_CURRENT_PROCESS,
			// EnumProcessModules
			ENUM_PROCESS_MODULES,
			// GetModuleFileName
			GET_MODULE_FILENAME_A,
			GET_MODULE_FILENAME_W,
			// GetModuleFileNameEx
			GET_MODULE_FILENAME_EX_A,
			GET_MODULE_FILENAME_EX_W,
			// ReadProcessMemory
			READ_PROCESS_MEMORY,
			// GetModuleInformation
			GET_MODULE_INFORMATION,
			// Placeholder to get number of functions
			NUM_FUNCTIONS
		};
		// Global Kernel32 function cache that stores Kernel32 function addresses.
		extern u64 g_functionCache[K32FunctionToVerify::NUM_FUNCTIONS];
		// Global flag to indicate if Kernel32 has been invalidated.
		extern bool g_bDllScrewed;

		// Store the first 8 bytes of tracked Kernel32 functions.
		inline void InitProtection()
		{
			g_functionCache[LOCAL_FREE] = *(u64*)LocalFree;
			g_functionCache[LOCAL_ALLOC] = *(u64*)LocalAlloc;
			g_functionCache[OPEN_PROCESS] = *(u64*)OpenProcess;
			g_functionCache[CLOSE_HANDLE] = *(u64*)CloseHandle;
			g_functionCache[GET_CURRENT_PROCESS] = *(u64*)GetCurrentProcess;
			g_functionCache[ENUM_PROCESS_MODULES] = *(u64*)EnumProcessModules;
			g_functionCache[GET_MODULE_FILENAME_A] = *(u64*)GetModuleFileNameA;
			g_functionCache[GET_MODULE_FILENAME_W] = *(u64*)GetModuleFileNameW;
			g_functionCache[GET_MODULE_FILENAME_EX_A] = *(u64*)GetModuleFileNameExA;
			g_functionCache[GET_MODULE_FILENAME_EX_W] = *(u64*)GetModuleFileNameExW;
			g_functionCache[READ_PROCESS_MEMORY] = *(u64*)ReadProcessMemory;
			g_functionCache[GET_MODULE_INFORMATION] = *(u64*)GetModuleInformation;
		}

		// Verify that the first 8 bytes of each function have not changed.
		inline void VerifyProtection()
		{
			if(g_bDllScrewed == false &&
				(g_functionCache[LOCAL_FREE] != *(u64*)LocalFree ||
				g_functionCache[LOCAL_ALLOC] != *(u64*)LocalAlloc ||
				g_functionCache[OPEN_PROCESS] != *(u64*)OpenProcess ||
				g_functionCache[CLOSE_HANDLE] != *(u64*)CloseHandle ||
				g_functionCache[GET_CURRENT_PROCESS] != *(u64*)GetCurrentProcess ||
				g_functionCache[ENUM_PROCESS_MODULES] != *(u64*)EnumProcessModules ||
				g_functionCache[GET_MODULE_FILENAME_A] != *(u64*)GetModuleFileNameA ||
				g_functionCache[GET_MODULE_FILENAME_W] != *(u64*)GetModuleFileNameW ||
				g_functionCache[GET_MODULE_FILENAME_EX_A] != *(u64*)GetModuleFileNameExA ||
				g_functionCache[GET_MODULE_FILENAME_EX_W] != *(u64*)GetModuleFileNameExW ||
				g_functionCache[READ_PROCESS_MEMORY] != *(u64*)ReadProcessMemory ||
				g_functionCache[GET_MODULE_INFORMATION] != *(u64*)GetModuleInformation
				)
				)
			{
				g_bDllScrewed = true;
			}
		}

		inline bool HasBeenHacked() { return g_bDllScrewed; }

		class Kernel32Initialiser
		{
		public:
			Kernel32Initialiser() { InitKernel32(); }
		};
	}
}
#endif // RSG_PC
#endif // SECURITY_RAGESECWINAPI_H