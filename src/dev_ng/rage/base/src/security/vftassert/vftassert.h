#ifndef SECURITY_VFTASSERT_VFTASSERT_H
#define SECURITY_VFTASSERT_VFTASSERT_H 

#include "security/ragesecengine.h"

#include "system/xtl.h"
#include "system/dependency.h"
#include "fwmaths/random.h"

#if RAGE_SEC_ENABLE_VFT_ASSERT

namespace rage
{
// Declare sub channel
RAGE_DECLARE_SUBCHANNEL(ragesecengine,vftassert)
#define vftDisplayf(fmt,...)		RAGE_DISPLAYF(##ragesecengine##_##vftassert,fmt,##__VA_ARGS__)
#define vftDebugf1(fmt,...)			RAGE_DEBUGF1(##ragesecengine##_##vftassert,fmt,##__VA_ARGS__)
#define vftDebugf2(fmt,...)			RAGE_DEBUGF2(##ragesecengine##_##vftassert,fmt,##__VA_ARGS__)
#define vftDebugf3(fmt,...)			RAGE_DEBUGF3(##ragesecengine##_##vftassert,fmt,##__VA_ARGS__)

#define VFT_ASSERT_INLINE 0
#define VFT_ASSERT_SIGNATURE 0xFFFFFFFF

// Remove this in the future.
const u64 BaseImageSize = 0x140000000;

extern sysDependency dmgDep; // Global variable

// This function exists in the apicheckplugin, so use a common
// function when VFT checking is fully integrated.
__forceinline static HMODULE VftGetCurrentModule()
{ // NB: XP+ solution!
	HMODULE hModule = NULL;
	GetModuleHandleEx(
		GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS,
		(LPCTSTR)VftGetCurrentModule,
		&hModule);

	return hModule;
}

__forceinline static bool IsAddressInImage(u64 address)
{
	PIMAGE_DOS_HEADER dos_header = (PIMAGE_DOS_HEADER)VftGetCurrentModule();
	PIMAGE_NT_HEADERS nt_header = (PIMAGE_NT_HEADERS)((u64)dos_header + dos_header->e_lfanew);
	PIMAGE_OPTIONAL_HEADER optional_header = (PIMAGE_OPTIONAL_HEADER)&nt_header->OptionalHeader;

	u64 image_size = optional_header->SizeOfImage;
	u64 image_base = optional_header->ImageBase;

	return (address != 0) && (address > image_base) && (address < (image_base + image_size));
}

__forceinline static bool DamageVft(const sysDependency &dep)
{
	// Extract parameters from the sysDependency.
	u32 size = dep.m_Params[1].m_AsUInt;

	// Address of the virtual function table (VFT).
	u64* addrVft = (u64*)dep.m_Params[0].m_AsPtr;

	// Pointer to the VFT.
	u64* pVft = *reinterpret_cast<u64**>(addrVft);

	vftDisplayf("Damaging VFT at %#0llX, *pVft = %#0llX\n", *addrVft, *pVft);

	for (std::size_t i = 0; i < size; i++, pVft++)
	{
		u32 rngLow = fwRandom::GetRandomNumberInRange(0, 0x7FFFFFFF);
		u64 rngHigh = fwRandom::GetRandomNumberInRange(0, 0x7FFFFFFF);
		*reinterpret_cast<u64*>(pVft) = rngHigh << 32 | rngLow;
	}

	return true;
}

// Derives a class where the argument is the parent. The derived class
// differs from the parent, such that the derived class has a placeholder
// function. The function address is inserted into the VFT of the class.
// The function is post-patched with VFT_ASSERT_SIGNATURE, so that the 
// end of the VFT can be determined.
#define VFT_ASSERT_CREATE_SHELL(cName)	\
class VftAssert##cName : public cName	\
{										\
private:								\
	virtual u64 SignaturePlaceholder()	\
	{									\
		int x = 0x1234ABCD;				\
		std::string s("s");				\
		while (x > 1)					\
		{								\
			x--;						\
			x ^= 5;						\
			s += (char)x;				\
		}								\
		return x;						\
	}									\
};

#if VFT_ASSERT_INLINE
// Creates a dummy class with an identical VFT to determine the validity of the original VFT.
#define RAGE_SEC_ASSERT_VFT(cName) VFT_ASSERT_CREATE_SHELL(cName) VFT_ASSERT_OBJ(cName)

#define VFT_ASSERT_OBJ(cName)																							\
	auto cName##_base = new cName();																							\
	auto cName##_derived = new VftAssert##cName();																				\
																																\
	vftDebugf1("Getting protected VFT.\n");																					\
	/* Stores the offsets of each function in the virtual function table. */													\
																																\
	atVector<u64> cName##_vftDerived;																							\
																																\
	/* Address of the virtual function table (VFT). */																			\
	u64* cName##_derived_addrVft = reinterpret_cast<u64*>(cName##_derived);														\
																																\
	/* Print the address. */																									\
	vftDebugf1("The virtual table is located at: %#llx\n",																	\
	*cName##_derived_addrVft - (u64)VftGetCurrentModule() + BaseImageSize);														\
																																\
	/* Pointer to the VFT. */																									\
	u64* cName##_derived_pVft = *reinterpret_cast<u64**>(cName##_derived_addrVft);												\
																																\
	/* Get the offsets of all functions in the VFT. Stop iterating when */														\
	/* the end of the image is reached. Maybe there is a better exit condition. */												\
	while (IsAddressInImage((u64)cName##_derived_pVft))																			\
	{																															\
		/* A pointer to the function at the current index of the VFT. */														\
		u64* cName##_derived_pFunc = reinterpret_cast<u64*>(*cName##_derived_pVft);												\
																																\
		/* Check if the address is in the image. */																				\
		if (!IsAddressInImage((u64)cName##_derived_pFunc))																		\
		{																														\
			/* Move to the next VFT entry. */																					\
			cName##_derived_pVft++;																								\
			continue;																											\
		}																														\
																																\
		/* Check if the first 4 bytes of the function matches the signature. */													\
		/* TODO: Dynamic / random / longer signatures? */																		\
		if ((u32)*cName##_derived_pFunc == VFT_ASSERT_SIGNATURE)																\
		{																														\
			vftDebugf2("Signature %#0lX found at %#0llX.\n", VFT_ASSERT_SIGNATURE, (u64)cName##_derived_pVft);				\
			break;																												\
		}																														\
																																\
		/* Store the virtual function address. */																				\
		cName##_vftDerived.PushAndGrow((u64)cName##_derived_pFunc);																\
																																\
		/* Move to the next VFT entry.. */																						\
		cName##_derived_pVft++;																									\
	}																															\
	vftDebugf3("There are %d functions in the VFT.\n", (u32)cName##_vftDerived.GetCount());									\
																																\
	for (u32 i = 0; i < cName##_vftDerived.GetCount(); i++)																		\
	{																															\
		vftDebugf3("Virtual function address - relative: (%#0llx), actual: (%#0llx)\n",										\
		cName##_vftDerived[i] - (u64)VftGetCurrentModule() + BaseImageSize, cName##_vftDerived[i]);								\
	}																															\
																																\
	vftDebugf1("Getting actual VFT.\n");																					\
	/* Stores the offsets of each function in the virtual function table. */													\
	atVector<u64> cName##_vftBase;																								\
																																\
	/* Address of the virtual function table (VFT). */																			\
	u64* cName##_base_addrVft = reinterpret_cast<u64*>(cName##_base);															\
																																\
	/* Print the address. */																									\
	vftDebugf1("The virtual table is located at: %#0llX\n", *cName##_base_addrVft - (u64)VftGetCurrentModule() + BaseImageSize);\
																																\
	/* Pointer to the VFT. */																									\
	u64* cName##_base_pVft = *reinterpret_cast<u64**>(cName##_base_addrVft);													\
																																\
	/* Iterate the VFT up to the size specified. */																				\
	for (u32 i = 0; i < cName##_vftDerived.GetCount(); i++, cName##_base_pVft++)												\
	{																															\
		/* Validate the VFT ptr. */																								\
		if (!IsAddressInImage((u64)cName##_base_pVft))																			\
		{																														\
			break;																												\
		}																														\
																																\
		/* Check if the VFT[i] is in the correct address space. */																\
		if (!IsAddressInImage(*cName##_base_pVft))																				\
		{																														\
			continue;																											\
		}																														\
																																\
		/* Store the function address. */																						\
		cName##_vftBase.PushAndGrow(*cName##_base_pVft);																		\
	}																															\
																																\
	/* Check if the VFTs have the same size. */																					\
	/* TODO: Change this to an assert? */																						\
	if (cName##_vftDerived.GetCount() != cName##_vftBase.GetCount())															\
	{																															\
		vftDisplayf("Size mismatch between base and protected VFTs.\n");													\
	}																															\
																																\
	/* Reset address of the VFT. */																								\
	cName##_base_addrVft = reinterpret_cast<u64*>(cName##_base);																\
																																\
	/* Reset pointer to the VFT. */																								\
	cName##_base_pVft = *reinterpret_cast<u64**>(cName##_base_addrVft);															\
																																\
	/* Check if both VFTs reference the same addresses. */																		\
	bool cName##_isVftValid = std::equal(																						\
		cName##_vftDerived.begin(), cName##_vftDerived.end(),																	\
		cName##_vftBase.begin()) &&																								\
		cName##_vftDerived.GetCount() == cName##_vftBase.GetCount();															\
																																\
	/* EXPERIMENTAL: If the contents of both VFTs are the same, check */														\
	/* if the functions themselves were trampolined. */																			\
	if (cName##_isVftValid)																										\
	{																															\
		for (u32 i = 0; i < cName##_vftDerived.GetCount(); i++)																	\
		{																														\
			/* Trusting GetProtectedVirtualFunctionTable() stored */															\
			/* valid addresses. */																								\
			u8* ptr = reinterpret_cast<u8*>(cName##_vftDerived[i]);																\
			if (*ptr == 0xFF || *ptr == 0xEA || *ptr == 0xE9 || *ptr == 0xEB || *ptr == 0x90)									\
			{																													\
				vftDisplayf("First byte of the virtual function at %#0llX is a jump.\n", cName##_vftDerived[i]);			\
																																\
				/* Mark the VFT as invalid, so the trampolined function is never reached. */									\
				/* TODO: Don't flag to damage the VFT until enough testing is done. */											\
				cName##_isVftValid = true;																						\
			}																													\
		}																														\
	}																															\
																																\
	/* Damage the base VFT if it */																								\
	if (!cName##_isVftValid)																									\
	{																															\
		sysDependency::Callback* cb = &DamageVft;																				\
		dmgDep.Init(cb);																										\
		dmgDep.m_Priority = sysDependency::kPriorityLow;																		\
		dmgDep.m_Params[0].m_AsPtr = reinterpret_cast<void*>(cName##_base);														\
		dmgDep.m_Params[1].m_AsUInt = (u32)cName##_vftDerived.GetCount();														\
		sysDependencyScheduler::Insert(&dmgDep);																				\
	}																															\
	else																														\
	{																															\
		vftDebug3("No VFT mismatch detected.\n");																			\
	}
#else
#define RAGE_SEC_ASSERT_VFT(cName) VFT_ASSERT_CREATE_SHELL(cName) AssertVft(new cName(), new VftAssert##cName())
template <typename B, typename D>
__forceinline static void AssertVft(B* base, D* derived)
{
	vftDebugf1("Getting protected VFT (%s).\n", typeid(D).name());
	atVector<u64> vftDerived;
	GetProtectedVft(derived, &vftDerived, VFT_ASSERT_SIGNATURE);

	vftDebugf1("Getting actual VFT (%s)\n", typeid(B).name());
	atVector<u64> vftBase;
	GetVft(base, &vftBase, vftDerived.GetCount());

	// Check if the VFTs have the same size.
	// TODO: Change this to an assert?
	if (vftDerived.GetCount() != vftBase.GetCount())
	{
		vftDisplayf("Size mismatch between base and protected VFTs.\n");
	}

	// Check if both VFTs reference the same addresses.
	bool isVftValid = std::equal(vftDerived.begin(), vftDerived.end(), vftBase.begin()) &&
		vftDerived.GetCount() == vftBase.GetCount();

	// EXPERIMENTAL: If the contents of both VFTs are the same, check
	// if the functions themselves were trampolined.
	if (isVftValid)
	{
		for (int i = 0; i < vftDerived.GetCount(); i++)
		{
			// Trusting GetProtectedVirtualFunctionTable() gave me
			// valid addresses.
			u8* ptr = reinterpret_cast<u8*>(vftDerived[i]);
			if (*ptr == 0xFF || *ptr == 0xEA || *ptr == 0xE9 || *ptr == 0xEB || *ptr == 0x90)
			{
				vftDisplayf("First byte of the virtual function at %#0llX is a jump.\n", vftDerived[i]);

				// Mark the VFT as invalid, so the trampolined function is never reached.
				// TODO: Don't flag to damage the VFT until enough testing is done.
				isVftValid = true;
			}
		}
	}

	// Damage the base VFT if it was modified.
	if (!isVftValid)
	{
		sysDependency::Callback* cb = &DamageVft;
		dmgDep.Init(cb);
		dmgDep.m_Priority = sysDependency::kPriorityLow;
		dmgDep.m_Params[0].m_AsPtr = reinterpret_cast<void*>(base);
		dmgDep.m_Params[1].m_AsUInt = (u32)vftDerived.GetCount();
		sysDependencyScheduler::Insert(&dmgDep);
	}
}

template <typename T>
static void GetVft(T* obj, atVector<u64>* result, std::size_t size)
{
	// Address of the virtual function table (VFT).
	u64* addrVft = reinterpret_cast<u64*>(obj);

	// Print the address.
	vftDebugf1("The virtual table is located at: %#llx\n", *addrVft - (u64)VftGetCurrentModule() + BaseImageSize);

	// Pointer to the VFT.
	u64* pVft = *reinterpret_cast<u64**>(addrVft);

	// Iterate the VFT up to the size specified.
	for (std::size_t i = 0; i < size; i++, pVft++)
	{
		// Validate the VFT ptr.
		if (!IsAddressInImage((u64)pVft))
		{
			break;
		}

		// Check if the VFT[i] is in the correct address space.
		if (!IsAddressInImage(*pVft))
		{
			continue;
		}

		// Store the function address.
		result->PushAndGrow(*pVft);
	}
}

template <typename T>
static void GetProtectedVft(T* obj, atVector<u64>* result, u32 signature = VFT_ASSERT_SIGNATURE)
{
	// Address of the virtual function table (VFT).
	u64* addrVft = reinterpret_cast<u64*>(obj);

	// Print the address.
	vftDebugf1("The virtual table is located at: %#llx\n",
		*addrVft - (u64)VftGetCurrentModule() + BaseImageSize);

	// Pointer to the VFT.
	u64* pVft = *reinterpret_cast<u64**>(addrVft);

	// Stores the offsets of each function in the virtual function table.
	atVector<u64> functions;

	// Get the offsets of all functions in the VFT. Stop iterating when
	// the end of the image is reached. Maybe there is a better exit condition.
	while (IsAddressInImage((u64)pVft))
	{
		// A pointer to the function at the current index of the VFT.
		u64* pFunc = reinterpret_cast<u64*>(*pVft);

		// Check if the address is in the image.
		if (!IsAddressInImage((u64)pFunc))
		{
			vftDebugf2("%#0llx is not in the image\n", (u64)pFunc);
			// Move to the next VFT entry.
			pVft++;
			continue;
		}

		// Check if the first 4 bytes of the function matches the signature.
		// TODO: Dynamic / random / longer signatures?
		if ((u32)*pFunc == signature)
		{
			vftDebugf2("Signature %#0lX found at %#0llX.\n", signature, (u64)pVft);
			break;
		}

		vftDebugf3("Adding function at address %#0llx\n", (u64)pFunc);
		// Store the virtual function address.
		result->PushAndGrow((u64)pFunc);

		// Move to the next VFT entry..
		pVft++;
	}

	vftDebugf2("There are %zd functions in the VFT.\n", result->size());

	for (int i = 0; i < result->size(); i++)
	{
		vftDebugf3("Virtual function address - relative: (%#llx), actual: (%#llx)\n",
			result->at(i) - (u64)VftGetCurrentModule() + BaseImageSize, result->at(i));
	}
}
#endif
}
#else
#define RAGE_SEC_ASSERT_VFT(cName)
#endif

#endif