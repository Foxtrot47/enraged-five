#include "vftassert.h"

#if RAGE_SEC_ENABLE_VFT_ASSERT

namespace rage
{
	// Define the channel
	RAGE_DEFINE_SUBCHANNEL(ragesecengine,vftassert)

	sysDependency dmgDep; // Global variable
}
#endif // RAGE_SEC_ENABLE_VFT_ASSERT