#ifndef SECURITY_RAGESECENGINETASKS_H
#define SECURITY_RAGESECENGINETASKS_H

#include "ragesecengine.h"
#include "rline/rltask.h"
#include "fwmaths/random.h"
#include "fwsys/gameskeleton.h"
#include "grcore/allocscope.h"
#include "grprofile/timebars.h"
#include "net/task.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/ipc.h"
#include "system/threadpool.h"
using namespace rage;

#if USE_RAGESEC

RAGE_DECLARE_SUBCHANNEL(ragesecengine, tasks);
// This is the most basic implementation of a work item. It's an intentionally generic 
// because the heavy lifting should go to the service interface that's exposed to the
// plugin itself. The actual queueing of work and performing the task should be very very
// generic. I don't expect people to ever have to declare their own instantiation of 
// this object
class rageSecGenericWorkItem : public sysThreadPool::WorkItem
{
public:
	
	// Over-ride the DoWork method with something that simply calls the 
	// function provided in the plugin's workFunction function pointer
	virtual void DoWork();
	// PURPOSE: Configures the current work item to set the 
	// operating function provided by the plugin
	//	fxn:		RageSecPluginWorkFunction type that represents the function
	//				to execute when the task manager gets around to it.
	bool Configure(RageSecPluginWorkFunction fxn, unsigned int id);
private:
	// A simple function pointer of the function to execute. This should be provided
	// by the plugin in the workFunction parameter
	RageSecPluginWorkFunction m_function;

	// Identifying DWORD so we know which plugin is firing
	unsigned int m_id;
};

// This is intentionally a generic task. The idea of this is to create a generic task
// with a generic work item that just performs the magic that's stored within the plugin.
// The OnUpdate() is a basic traversal of 3 states: starting, working, and completed.
// Any plugin should be able to use this task to execute it's work. 
// By leveraging netTask and the thread pools I've created, I ensure that the main thread
// never hangs, and that the plugin can operate as modularly as possible.
class rageSecGenericTask : public netTask
{
public:
	// Declare the work item
	NET_TASK_DECL(rageSecGenericTask)
	// Indicate which logging channel we're using
	NET_TASK_USE_CHANNEL(ragesecengine_tasks);

	// Constructor - doesn't really do anything fancy other than initialize the state
	// to M_STARTED
	rageSecGenericTask();
	// Configuration function that just sets the internal member pointer for fxn 
	// to m_function accordingly
	virtual bool Configure(RageSecPluginWorkFunction fxn, unsigned int id)
	{
		m_function = fxn;
		m_id = id;
		return true;

	}
	// Simple updating function declaration. For some reason this takes a result code pointer?
	// I honestly have no idea why. I don't think I do anything with it. Looking at the source
	// and at other examples, I don't use it at all, so yolo life
	virtual netTaskStatus OnUpdate(int* resultCode);
	// For some reason I've exposed this to get the actual work item. I don't think I ever use it
	// but at the moment it's not doing me any real harm.
	virtual rageSecGenericWorkItem* GetWorkItem() { return &m_workItem;}

	// B* 2745642 - Implementing OnCancel
	virtual void OnCancel() { netTaskDebug("Canceled while in state %d", m_State); m_workItem.Cancel(); }

private:

	enum State
	{
		STATE_STARTED,
		STATE_WORKING,
		STATE_COMPLETED
	};

	rageSecGenericWorkItem			m_workItem;
	RageSecPluginWorkFunction		m_function;
	unsigned int					m_id;
	State							m_State;
	// This is the 

};

#endif // USE_RAGE_SEC

#endif // #define