// 
// security/ragesec.h 
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SECURITY_RAGESECENGINE_H
#define SECURITY_RAGESECENGINE_H

#define USE_RAGESEC 1

// Control whether RageSec should be utilized at all
#if USE_RAGESEC
#include "atl/vector.h"
#include "diag/channel.h"
#include "file/file_config.h"
#include "rline/rltask.h"
#include "system/criticalsection.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/threadpool.h"
#include "system/timer.h"

// Defines local to my project
#include "security/ragesecplugin.h"
#include "security/ragesecpluginmanager.h"

// #defines for each feature in RageSec, such that we can conditionally turn them on and off.
#define RAGE_SEC_ENABLE_PLUGIN_ACTIONS					1
#define RAGE_SEC_TASK_API_CHECK							1 && (RSG_PC || RSG_DURANGO || RSG_ORBIS)
#define RAGE_SEC_TASK_BASIC_DEBUGGER_CHECK				1 && RSG_PC
#define RAGE_SEC_TASK_CLOCK_GUARD						1 && RSG_PC
#define RAGE_SEC_TASK_VIDEO_MODIFICATION_CHECK			1 && RSG_PC
#define RAGE_SEC_TASK_LINK_DATA_REPORTER				1
#define RAGE_SEC_TASK_NETWORK_MONITOR_CHECK				0
#define RAGE_SEC_TASK_REVOLVING_CHECKER					1 && RSG_PC
#define RAGE_SEC_TASK_RTMA								1
#define RAGE_SEC_TASK_RTMA_HELPER						1
#define RAGE_SEC_TASK_SCRIPTHOOK						1 && RSG_PC
#define RAGE_SEC_TASK_TUNABLES_VERIFIER					1 && RSG_PC
#define RAGE_SEC_TASK_VEH_DEBUGGER						1 && RSG_PC
#define RAGE_SEC_IN_SEPARATE_THREAD						0
#define RAGE_SEC_ENABLE_REACTIONS						1
#define RAGE_SEC_ENABLE_REBALANCING						1
#define RAGE_SEC_ENABLE_VFT_ASSERT						1 && RSG_PC
#define RAGE_SEC_TASK_EC								1 && RSG_PC
#define RAGE_SEC_ENABLE_YUBIKEY							0 && RSG_PC
#define RAGE_SEC_TASK_CE_DETECTION						1 && RSG_PC
#define RAGE_SEC_TASK_DLLNAMECHECK						1 && RSG_PC
#define RAGE_SEC_TASK_PLAYERNAME_MONITOR_CHECK			1
#define RAGE_SEC_TASK_SCRIPT_VARIABLE_VERIFY			1 && RSG_PC

// Thread Pool defines
// Thread Pool #defines
#if RSG_DURANGO || RSG_ORBIS
#if !defined(RAGESEC_THREADPOOL_CPU_1)
#define RAGESEC_THREADPOOL_CPU_1 1
#endif

#if !defined(RAGESEC_THREADPOOL_CPU_2)
#define RAGESEC_THREADPOOL_CPU_2 2
#endif
#else // RSG_DURANGO || RSG_ORBIS
#if !defined(RAGESEC_THREADPOOL_CPU_1)
#define RAGESEC_THREADPOOL_CPU_1 0
#endif

#if !defined(RAGESEC_THREADPOOL_CPU_2)
#define RAGESEC_THREADPOOL_CPU_2 0
#endif
#endif // RSG_DURANGO || RSG_ORBIS

// Declare our channel so we can use it in anything that references this header file
RAGE_DECLARE_CHANNEL(ragesecengine)
#define RageSecAssertf(cond,fmt,...)				RAGE_ASSERTF(ragesecengine,cond,fmt,##__VA_ARGS__)
#define RageSecVerifyf(cond,fmt,...)				RAGE_VERIFYF(ragesecengine,cond,fmt,##__VA_ARGS__)
#define RageSecWarningf(fmt,...)					RAGE_WARNINGF(ragesecengine,fmt,##__VA_ARGS__)
#define RageSecDisplayf(fmt,...)				    RAGE_DISPLAYF(ragesecengine,fmt,##__VA_ARGS__)
#define RageSecDebugf1(fmt,...)					    RAGE_DEBUGF1(ragesecengine,fmt,##__VA_ARGS__)
#define RageSecDebugf2(fmt,...)					    RAGE_DEBUGF2(ragesecengine,fmt,##__VA_ARGS__)
#define RageSecDebugf3(fmt,...)					    RAGE_DEBUGF3(ragesecengine,fmt,##__VA_ARGS__)
#define RageSecError(fmt,...)						RAGE_ERRORF(ragesecengine, fmt,##__VA_ARGS__)

	
namespace rage
{
	class rageSecEngine
	{
		// These are all static becuase our Init() function is static, such that we can be registered
		// for Init() and Shutdown() in the game accordingly
	private:
		// Boolean to control whether we need to break our update loop
		// volatile, as it's potentially accessed by multiple threads.
		static volatile bool sm_continueUpdate;

		// Indicate whether initialization of RageSec has succeeded
		static bool sm_initialized;
		// Store the thread identifier of our update thread
		// This is sheerly for the management of RageSec. It will queue up the work
		// in the task manager, and make sure any incoming requests get managed
		// appropriately. The heavy lifting is done in work items provided
		// by the thread pool workers.
		static sysIpcThreadId sm_updateThread;

		// Plugin Manager.
		static rageSecPluginManager sm_pluginManager;

		// Rage-level reports
		// Here I'm creating an atArray of reaction objects that will be consumed
		// by the RageSecGameInterface. This will allow code in fw and rage code to 
		// still report without having to create a dependency on the game files.
		static atQueue<RageSecPluginGameReactionObject,RAGE_SEC_MAX_NUMBER_REACTION_QUEUE_EVENTS> sm_rageLandReactions;

	public:
		// Return if RageSec has been initialized or not
		static bool IsInitialized(){ return sm_initialized;}

		// Function used to initialize RageSec
		static void Init(unsigned int initializationMode);

		// Function used to re-initialize RageSec
		static void ReInit();

		// Shutdown shell function
		static void Shutdown(unsigned int shutdownMode);

		// ShutdownClass actually performs the destruction of internal
		// data members
		static void ShutdownClass();

		// Thread entry point
		static void rageSecUpdateThread(void *);

		// Actual Update call, for registration
		static void Update();

		// Fetch rage-land reactions
		static atQueue<RageSecPluginGameReactionObject,RAGE_SEC_MAX_NUMBER_REACTION_QUEUE_EVENTS> * GetRageLandReactions() { return &sm_rageLandReactions;}

	};

}
#endif

#endif