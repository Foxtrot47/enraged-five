#if __BANK
#include "bank/bank.h"
#endif

#include "data/resource.h"
#include "file/asset.h"
#include "diag/memstats.h"
#include "grcore/device.h"
#include "grcore/im.h"
//#include "grcore/state.h"
#include "grcore/viewport.h"
//#include "grcore/viewport_inline.h"
#include "vector/color32.h"
#include "occluder.h"

namespace rage {

}// namespace rage
#if 0

#define	__SPAN_MERGE				1
#define	__SBUFFER_OCCLUDER_DEBUG	1 && __DEV
#define	__SPANBUFFER_ENABLED		0 & 1
#define	_OCCLUDER_DEBUGGING			0 & __DEV



// global span buffer we allocate from.
s16			num_fragmented_spans;
s16			curr_free_span_in_array;						// current index of span in array that we can allocate. this is just incremented as we allocate from it.
s16			span_fragment_stack[ RM_OCCLUDE_SPANBUFF_MAX_SPANS ];	// Indices of spans in the array that are available for allocation
fSpan_t		span_array[ RM_OCCLUDE_SPANBUFF_MAX_SPANS ];

static	fSpan_t *s_pPrev = NULL;
static	int		s_spanCoverage;
static	int		g_SpansInUse = 0;
static	int		g_SpanMerges = 0;


/*
//
// hybrid s-buffer occluder system. 8/4/2004 (cmc) 10:14pm
//
Using a front to back traversal of the scene from the given viewpoint/view-direction, you do the following:

1. Check the current node against the view frustum. If it doesn't pass go to step 4. 

2. Start scan converting the node's bounding polyhedron and enter it into the S-Buffer. The instant you find a span which is even partially visible, stop and go to step 3. 

3. The current node is _potentially_ visible. If it's a leaf add the geometry to the S-Buffer and then send it to the rasterizer to be drawn.
	If it isn't a leaf, get the front-most child of the current node and go to step 1. 

4. Get the next front-most node and go to step 1. 


The method would test each node recursively against the results of the already drawn scene. There are some issues.

1. Adding a complex mesh to the S-Buffer is inefficient.
	Try adding a screen aligned facing quad with a depth equal to the farthest point of the object, with the quad's size determined by the bounding sphere.
	Depending on whether or not the quad you construct is contained within the projected circle sphere bound or contains the circle, you will get a conservative, or agressive occluder result.

2. If you stop scan-converting the bounding polyhedrons/quadsprite the instant a span is found to be visible,
	you will have an unpredictable results because you can get a best case scenario (top span is visible) or a worst case scenario (bottom span is visible) which both will yield a visible node.
	To help this out you can try span interleaving by testing "odd numbered" spans then, if necessary, "even numbered" spans.
	Alternating bottom, top and left,right alternate span tests would be an interesting method maybe.

3.	For mc3 most things are going to clip out against buildings of varying heights, sticking with a top->down span conversion and test will probably still work best.

*/

/*

 +-C-----+
 |  _B_  |
 | /   \ |
 || +-+ ||
 || |A| ||
 || +-+ ||
 | \___/ |
 +-------+

Agressive occlusion test:
For sphere bound 'B' use test depth sprite 'C' against span/tile buffers.
Conservative occlusion test:
For sphere bound 'B' use test depth sprite 'A' against span/tile buffers.

Insert depth sprite 'A' into tile/span buffers.
*/
//
// Notes: (in no particular order)
//
// 1) Depth tile pyramid should be re-evaluated after each convex object is processed. redoing it after each polygon would be wasteful because none of them overlap.
//
// 2) With an environment mostly 2.5D like a city, less vertical resolution should be ok.
//    Panning views and translation on the x-z plane shift occluders mostly along the x screenspace axis in some fashion,
//		with fusion needed more along the horizon line and silhouettes of mostly vertical surfaces such as buildings overlap commonly.
//
// 3) Adjust lod and texture quality of object rendered based on the level of occlusion the object recieves (objects 10% visible render with cheaper shaders, lower miplevels, lower geom lod or something..)
//
//
//
#endif

#if 0

s16		rmOccluderSystem::GetSpanBufferHeight( const int viewerID )
{
	return GetViewSSB( viewerID )->GetSpanBufferHeight();
}

s16		rmOccluderSystem::GetSpanBufferWidth( const int viewerID )
{
	return GetViewSSB( viewerID )->GetSpanBufferWidth();
}

void	rmOccluderSystem::ActivateSpanBuffer( const int viewerID, int screen_w, int screen_h )
{
	// enable span buffering for this view.
	// go through all scanlines for this view and lay down a 'far span'
	Assert( (viewerID >= 0) && (viewerID < RM_OCCLUDE_MAX_VIEWS) );
	Assert( screen_w > 0 );
	Assert( screen_h > 0 );

	this->GetViewSSB( viewerID )->ActivateSpanBuffer( (s16)screen_w, (s16)screen_h );
}


#if	__SPANBUFFER_ENABLED	
void	SpriteSpanBuffer::ActivateSpanBuffer( s16 width, s16 height )
#else
void	SpriteSpanBuffer::ActivateSpanBuffer( s16 /*width*/, s16 /*height*/ )
#endif
{
#if	__SPANBUFFER_ENABLED	
	Assert( rmOccluderSystem::GetInstance() != NULL );

	SetSpanBufferRect( width, height );

	for(int scanline=0; scanline < height; scanline++)
	{
		fSpan_t *pSpan = rmOccluderSystem::GetInstance()->AllocateSpan();
		 
		// setup far span covering the entire scanline.
		pSpan->depth	= RM_OCCLUDE_MAX_ZDEPTH;
		pSpan->next		= NULL;
		pSpan->start	= 0;
		pSpan->end		= width-1;

		// This new span is our list start.
		m_pScanlineSpanLists[ scanline ] = pSpan;
	}
#endif
}

// NOTE: if we do a 'freeze view' for span buffer occluder fusion, we will have to stop new spans from being added, and have this function do nothing (just return).
void	rmOccluderSystem::ResetAllSpanBuffers( void )
{
	g_SpanMerges = 0;// reset
	g_SpansInUse = 0;// reset
	int	totalViews = RM_OCCLUDE_MAX_VIEWS;

#if	__SPANBUFFER_ENABLED		
	int		height;

	#if 0 && __DEV
		Displayf(" g_SpansInUse %d   g_SpanMerges %d", g_SpansInUse, g_SpanMerges );
	#endif

	// Loop through viewers.
	for(int v=0; v < totalViews; v++)
	{
		// TODO: CHECK was this view ever active??? dont want to waste time clearing and setting up span buffers for views never used.
		if( !m_bIsSpanBufferActive[ v ] )
			continue;// inactive.

		SpriteSpanBuffer *pSSB = GetViewSSB( v );
		Assert( pSSB != NULL );

		height =  this->GetSpanBufferHeight( v );
	    
		// NOTE: Assumes all views will be released when the next frame starts, if this is not the case, we have to release each span in the scanlines individually (ick)
		for(int scanline=0; scanline < height; scanline++)
		{
			#if __DEV
				// walk the scanline, returning spans to the pool 1 at a time (debug mode only)

			#endif
			// reset ptr for the span list on this scanline.
			pSSB->ClearScanLineSpanListPtr( scanline );
		}

		// Done clearing scanlines, kill this spanbuffer off for this view.
		m_bIsSpanBufferActive[ v ] = false;// deactivate buffer - application has to activate it every frame so we handle resizing viewports and other crazyness.
	}

	#if __DEV
		// TODO: check that all spans were freed by checking count of span fragments and the array allocation count
	#endif

#else
	
	// Loop through viewers.
	for(int v=0; v < totalViews; v++)
	{
		m_bIsSpanBufferActive[ v ] = false;
	}

#endif

	// resetSpanAllocator!
	num_fragmented_spans = 0;
	curr_free_span_in_array = 0;

}




// get ptr to starting span for a given view+scanline.
fSpan_t*	rmOccluderSystem::GetStartingSpanForViewScanline	(const int viewerID, int scanline)
{
	return m_SpanBuff[ viewerID ].GetStartingSpanForScanline( scanline );
}

SpriteSpanBuffer*	rmOccluderSystem::GetViewSSB	(const int viewerID)
{
	return &m_SpanBuff[ viewerID ];
}


fSpan_t* rmOccluderSystem::AllocateSpan( void )
{
	int spanIndex;
#if __SBUFFER_OCCLUDER_DEBUG	
	g_SpansInUse++;
#endif
	// allocate from fragments first
	if( num_fragmented_spans )
	{
		spanIndex = span_fragment_stack[ num_fragmented_spans-1 ];

		num_fragmented_spans--; // popped one off stack

		return &span_array[ spanIndex ];
	}
	else
	{
		// No fragmented spans available, allocate from main array directly.
		Assert( curr_free_span_in_array < (RM_OCCLUDE_SPANBUFF_MAX_SPANS-1) );

		spanIndex = curr_free_span_in_array; // get free slot index.

		curr_free_span_in_array++; // move to next free array slot.

		return &span_array[ spanIndex ];// return ptr
	}
}


// something tells me this is not totally cross-platform friendly...
void rmOccluderSystem::ReleaseSpan( fSpan_t	*pFreedSpan )
{
	fSpan_t *pBase = &span_array[0];
	// convert from pointer back to index.

	int index = ptrdiff_t_to_int(pFreedSpan - pBase);
	//Displayf( " pBase %p pFreedSpan %p , delta %d index %d", pBase, pFreedSpan, delta, index );

#if __SBUFFER_OCCLUDER_DEBUG	
	Assert( (index >= 0 ) && (index < (u32)RM_OCCLUDE_SPANBUFF_MAX_SPANS) && "trying to release a bad span ptr");// make sure we aren't trying to free some out of range trash ptr
	// store index, not ptr to save memory.
	Assert( num_fragmented_spans < (RM_OCCLUDE_SPANBUFF_MAX_SPANS-1) );

	g_SpansInUse--;
#endif

	// push on stack
	span_fragment_stack[ num_fragmented_spans ] = (s16)index;
	num_fragmented_spans++;// update stack
}







void	rmOccluderSystem::SetStartingSpanScanlineForActiveView( fSpan_t *pStartSpan )
{
	int viewID				= GetActiveViewID();
	SpriteSpanBuffer *pSSB	= GetViewSSB( viewID );
		
	pSSB->m_pScanlineSpanLists[ m_activeScanline ] = pStartSpan;
}


int		rmOccluderSystem::TestSpanOnly( fSpan_t *pCurr, s16 newSpan_x0, s16 newSpan_x1, float depth ) 
{
	// walk span list for this scanline, test each entry against current span we try to insert.
	fSpan_t *pPrev = s_pPrev;

	Assert( pCurr != NULL );
	// get span extents
	s16 currSpan_x0 = -1;
	s16 currSpan_x1 = -1;

	// increment newSpan_x0 as we clip it against segments, moving through the spans for this scanline.
	while( pCurr != NULL )
	{
		currSpan_x0 = pCurr->start;
		currSpan_x1 = pCurr->end;

		pPrev = s_pPrev;

		if( pCurr == pCurr->next )
		{
			Displayf("curr == curr->next !");
			pCurr->next = NULL;// hack terminator
			return -1;// hack
		}

		bool	b_newSpanCloser;

		if( depth < pCurr->depth )// change this back to '<' check if we dont use float depth anymore?
			b_newSpanCloser = true;
		else
			b_newSpanCloser = false;

		// *****
		// TODO:
		// check if previous has the same depth as the span we are putting in.
		// if it does, this is a split edge span being inserted.
		// we can 'cleanup' the splits on this scanline later by doing another pass on this scanline, have a list of dirty scanlines that need to be cleaned up after each 'occluder primitive' is processed.
		// TODO: have the 'test span but dont add to buffer' function do cleanup automatically??? (Test regions crossing dirty splits fuses continuous spans with the same depth.)
		// *****

		// 1st case, new span is farther to the right than the span in the table we have (no overlap):
		//
		// class 'D'
		//		cccccc
		//				nnnnnnn
		if( currSpan_x1 <= newSpan_x0  )// new span meets up exactly with the tail of the current one or its before it. (doesnt have a 1pixel gap at least between them)
		{
			// Next Cur
			s_pPrev = pCurr;// track previous properly, may recusively insert spans!
			pCurr	= pCurr->next;
			// Continue Checking

			if( pCurr == pCurr->next )
			{
				Displayf("curr == curr->next !");
				pCurr->next = NULL;// hack terminator
				return -1;// hack
			}
			continue;			
		}
#if __DEV
		else if (newSpan_x0 >= GetViewSSB( GetActiveViewID() )->GetSpanBufferWidth() || newSpan_x1 < 0)
		{
			Assert( 0 && "offscreen span was asked to be inserted, higher level code should have clipped this" );// sanity check
		// Simply off-screen? (do this check again 'cause New is changin')
		// NOTE: we are also doing the screenclip test also at higher levels for trivial rejection of the entire primitive, not just the spans it turns into.

			// ##
			s_pPrev = pCurr;// track previous properly, may recusively insert spans!
			pCurr	= pCurr->next;
			// ##
			// Return
			return -1;// nothing added!
		}
#endif
		else if (newSpan_x0 == currSpan_x0)
		{
			if(newSpan_x1 == currSpan_x1)
			{
				// class 'C' (spans compared cover same area exactly)
				//
				// ccccc
				// nnnnn
				
				// exact span replacement
				if( b_newSpanCloser )
				{
					return 9999;// visible!
					/*
					pCurr->depth = depth;// update depth info

#if __SPAN_MERGE
					if( pPrev )
					{
						if(pPrev->depth == depth)
						{
#if __DEV
							g_SpanMerges++;
#endif
							//Displayf(" merging abutting same depth spans ");
							// previous span has same depth, extend it, and kill this one off.
							pPrev->next = pCurr->next;
							pPrev->end  = newSpan_x1;// extend

							this->ReleaseSpan( pCurr );						
							return 1;// all done.
						}
					}
					else
					{
						// no previous, must be a starting edge so there is no fragment to 'step back' to and merge.
					}
#endif
				}

				// ##
				s_pPrev = pCurr;// track previous properly, may recusively insert spans!
				pCurr	= pCurr->next;
				// ##
				// fallthrough case
				return -1;// new span was behind, nothing was added (fully occluded)
				*/
				}
				else
				{
					return -1;// behind!
				}


			}

			// wasnt an identical span case so fallthrough, resulting cases are....

			// class 'B'

			// (B1)
			// cccccc
			// nnnn
			// 
			// *or*
			//
			// (B2)
			// cccccc
			// nnnnnnnn

			// case B1
			if( currSpan_x1 > newSpan_x1 )
			{
				// current span extends past the current one....

				if ( b_newSpanCloser )
				{
					return 9999;// visible!
					/*
					// current span is behind the new one.

					// cccccc
					// nnnn

					// clips into:

					// xxxxcc
					// nnnn
					//fSpan_t *pNewSpan;

#if __SPAN_MERGE
					// check first if previous span is the same depth.
					if( pPrev && pPrev->depth == depth )
					{
						// it is, just extend the previous span's right edge, without allocating another span and clipping the left edge of pCurr;
#if __DEV
						g_SpanMerges++;
#endif
						
						pPrev->end   = newSpan_x1;
						pCurr->start = newSpan_x1;
						return 1;// (newSpan_x1 - newSpan_x0);// span pixels were updated.
					}
#endif

					
					// allocate new span, insert before the current one
					pNewSpan = AllocateSpan();
					pNewSpan->next = pCurr;// new span's next is 'current' because it comes before it now.
					pNewSpan->start= newSpan_x0;
					pNewSpan->end  = newSpan_x1;
					pNewSpan->depth= depth;

					// clip 'current' span's left edge to tail of the new span
					pCurr->start = newSpan_x1;
					Assert( pCurr->end	== currSpan_x1 );// sanity check

					// re-link lists properly.
					if( pPrev )
					{
						pPrev->next = pNewSpan;// previous span point to the newly inserted one thats going before pCurr

					}else{// no prev, this is new head of list.

						// new span is starting entry.
						SetStartingSpanScanlineForActiveView( pNewSpan );			
					}

					// ##
					s_pPrev = pCurr;// track previous properly, may recusively insert spans!
					pCurr	= pCurr->next;
					// ##
					*/
					//return -1;//(newSpan_x1 - newSpan_x0);
				}
				else
				{
					// current span is closer....

					// nnnn
					// cccccc

					// ##
					s_pPrev = pCurr;// track previous properly, may recusively insert spans!
					pCurr	= pCurr->next;
					// ##
					return -1;// new span is completely behind the existing one, so we add nothing.				
				}
			}
			else
			{
				// case B2
				//
				// (b2a)
				// cccccc
				// nnnnnnnn
				//
				// *or*
				//
				// (b2b)
				// nnnnnnnn
				// cccccc
				if( currSpan_x1 < newSpan_x1 )
				{					
					if ( b_newSpanCloser )
					{
						return 9999;// visible!

						/*
#if __SPAN_MERGE
						// check first if previous span is the same depth.
						if( pPrev && pPrev->depth == depth )
						{
#if __DEV
							g_SpanMerges++;
#endif
							// it is, just extend the previous span's right edge, without allocating another span and clipping the left edge of pCurr;
							
							pPrev->end   = pCurr->end;
							pPrev->next  = pCurr->next;// relink prev to next of 'curr'
							// release current, and insert test a new span that is the remainder if the new span.
							this->ReleaseSpan( pCurr );
							// xxxxxx
							// pppppp22

							return InsertSpan( pPrev->next, pPrev->end, newSpan_x1, depth );// insert test remainder.
						}
						else
#endif						
						{

							// (b2a)
							// cccccc
							// nnnnnnnn

							// new span extends past the end of current, so we have to chop it and insert test the remainder.
							// becomes:

							// xxxxxx
							// cccccc22

							// update curr to new closer depth.
							pCurr->depth = depth;
							s_spanCoverage += (currSpan_x1 - currSpan_x0);// track amount we added(updated depth) to buffer
						}
					}
					else
					{
						// if new span isnt closer, then its case (b2b)
						// we do nothing here, because the clipped span is done unconditionally below here.
					}
					// regardless of the new span depth relative to the 'current', the part extending past 'current' right edge is clipped and insertion tested.

					// insert remainder of new span, testing it against the next span in the list.

					s_pPrev = pCurr;// track previous properly, may recusively insert spans!

					Assert( pCurr->next != NULL );// should not be possible because we clip to screen edges!
					Assert( currSpan_x1 < newSpan_x1);// sanity check
					
					*/
					}
					else
					{
						// hmmmm.......
						s_pPrev = pCurr;// track previous properly, may recusively insert spans!
						return TestSpanOnly( pCurr->next, currSpan_x1, newSpan_x1, depth );
					}
				}
				else
				{
					Assert(0);// impossible case
				}
			}
		}
		else if ( newSpan_x0 > currSpan_x0 )
		{
			// class 'A'

			// check for case where ends meet exactly:
			//
			// cccccccc
			//    nnnnn
			if( newSpan_x1 == currSpan_x1 )
			{
				if ( b_newSpanCloser )
				{
					return 9999;// visible!
					/*
					// cccccccc
					//    nnnnn
					//
					// becomes:
					//
					// cccxxxxx
					//    nnnnn
					
					// allocate new span and relink
					fSpan_t *pNewSpan = AllocateSpan();
					pNewSpan->next  = pCurr->next;// new span's next is 'current' 's 'next' because it comes after it.
					pNewSpan->start = newSpan_x0;
					pNewSpan->end   = newSpan_x1;
					pNewSpan->depth = depth;

					// modify right edge of current now.
					pCurr->end  = newSpan_x0;
					pCurr->next = pNewSpan;// relink
	
					// ##
					s_pPrev = pNewSpan;// track previous properly, may recusively insert spans!
					pCurr	= pNewSpan->next;
					// ##
					// DONE, span was all taken care of.
					return (newSpan_x1 - newSpan_x0);
					*/
				}
				else
				{
					// new span completely covered by existing current span
					// ##
					s_pPrev = pCurr;// track previous properly, may recusively insert spans!
					pCurr	= pCurr->next;
					// ##
					return -1;// hidden!
				}	
			}
			else if( newSpan_x1 > currSpan_x1 )
			{
				//Displayf(" new %d %d    curr %d %d ", newSpan_x0, newSpan_x1, currSpan_x0, currSpan_x1 );
				Assert( newSpan_x0 < currSpan_x1 );// sanity check
				Assert( newSpan_x1 > currSpan_x0 );// sanity check
				// ccccccc
				//    nnnnnnnn

				// becomes:
				//
				// ccc   
				//    nnnn2222
				fSpan_t	*p_NextTestSpan = NULL;

				s16 currEnd = pCurr->end;

				if ( b_newSpanCloser )
				{
					return 9999;// visible!
					/*
					pCurr->end = newSpan_x0; // clip right edge of current

					// allocate new span and relink after current
					fSpan_t *pNewSpan = AllocateSpan();
					pNewSpan->next  = pCurr->next;// new span's next is 'current' 's 'next' because it comes after it.
					pNewSpan->start = newSpan_x0;
					pNewSpan->end   = currEnd;
					pNewSpan->depth = depth;

					// relink 'current'
					pCurr->next = pNewSpan;

					s_pPrev = pNewSpan;// track previous properly, may recusively insert spans!

					p_NextTestSpan = pNewSpan->next;
					*/
				}
				else
				{				   
					//        2222
					// ccccccc

					s_pPrev = pCurr;// track previous properly, may recusively insert spans!

					p_NextTestSpan = pCurr->next;
					// do nothing in this case, we are only chopping the new span and insertion testing it on the next span.

					// fallthrough to chopped span insertion....
				}

				// ##
				pCurr	= pCurr->next;
				// ##

				// insert remainder of new span, testing it against the next span in the list.
				return TestSpanOnly( p_NextTestSpan, currEnd, newSpan_x1, depth );
			}
			else
			{
				// ccccccccc
				//    nnn
				Assert( newSpan_x1 < currSpan_x1);// only possible case left

				if ( !b_newSpanCloser )
				{
					// ##
					s_pPrev = pCurr;// track previous properly, may recusively insert spans!
					pCurr	= pCurr->next;
					// ##
					return -1;// nothing added, its behind!
				}
				else
				{
					return 9999;// visible!
					/*
					// ccccccccc
					//    nnn
					//
					// becomes:
					//
					// ccc   222
					//    nnn

					// new span is closer, but splits the one behind it in half, insert 2 new spans.
					int a0,a1,a2,a3;
					float currDepth = pCurr->depth;

					// segment markers pushed into temps because linked segments get their markers resorted.
					a0 = pCurr->start;
					a1 = newSpan_x0;
					a2 = newSpan_x1;
					a3 = pCurr->end;

					fSpan_t *pNewSpan  = AllocateSpan();
					fSpan_t *pNewSpan2 = AllocateSpan();

					pCurr->start = a0;
					pCurr->end   = a1;
					//pCurr->depth = currDepth;// unnecessary, its already set

					// relink list and insert properly (careful of the order here)
					pNewSpan->next	= pNewSpan2;
					pNewSpan2->next = pCurr->next;
					pCurr->next		= pNewSpan;
					
					// insert new span after current. (middle section)						
					pNewSpan->start = a1;
					pNewSpan->end   = a2;
					pNewSpan->depth = depth;
					//pNewSpan->next  = pCurr->next;
					
					// allocate tailing section (same depth as current)					
					pNewSpan2->start = a2;
					pNewSpan2->end   = a3;
					pNewSpan2->depth = currDepth;//pCurr->depth;
					//pNewSpan2->next  = pCurr->next;

				// ##
				s_pPrev = pNewSpan2;// track previous properly, may recusively insert spans!
				pCurr	= pNewSpan2->next;
				// ##
					return (a3-a0)-(a2-a1);
					*/
			 	}
			}
		}

		Displayf("m_activeScanline %d -- newSpan %d %d currSpan %d %d ", m_activeScanline, newSpan_x0, newSpan_x1, currSpan_x0, currSpan_x1 );
		Assert(0);// should never get here!
		return 1;// hack 		
	}

	Displayf("m_activeScanline %d -- exit: newSpan %d %d currSpan %d %d ", m_activeScanline, newSpan_x0, newSpan_x1, currSpan_x0, currSpan_x1 );
	Assert(0);// should never get here!
	return 1;
}

// this function will be a whole lot faster if objects and their spans are processed in a front to back fashion.
int		rmOccluderSystem::InsertSpan( fSpan_t *pCurr, s16 newSpan_x0, s16 newSpan_x1, float depth ) 
{
	// walk span list for this scanline, test each entry against current span we try to insert.
	fSpan_t *pPrev = s_pPrev;
	Assert( pCurr != NULL );

	// get span extents
	s16 currSpan_x0;// = pCurr->start;
	s16 currSpan_x1;// = pCurr->end;

	//float prevSpanDepth= 999999.f;

	// increment newSpan_x0 as we clip it against segments, moving through the spans for this scanline.

	while( pCurr != NULL )
	{
		currSpan_x0 = pCurr->start;
		currSpan_x1 = pCurr->end;

		pPrev = s_pPrev;

		if( pCurr == pCurr->next )
		{
			Displayf("curr == curr->next !");
			pCurr->next = NULL;// hack terminator
			return 0;// hack
		}

		bool	b_newSpanCloser;

		if( depth < pCurr->depth )
			b_newSpanCloser = true;
		else
			b_newSpanCloser = false;

		// *****
		// TODO:
		// check if previous has the same depth as the span we are putting in.
		// if it does, this is a split edge span being inserted.
		// we can 'cleanup' the splits on this scanline later by doing another pass on this scanline, have a list of dirty scanlines that need to be cleaned up after each 'occluder primitive' is processed.
		// TODO: have the 'test span but dont add to buffer' function do cleanup automatically??? (Test regions crossing dirty splits fuses continuous spans with the same depth.)
		// *****

		// 1st case, new span is farther to the right than the span in the table we have (no overlap):
		//
		// class 'D'
		//		cccccc
		//				nnnnnnn
		if( currSpan_x1 <= newSpan_x0  )// new span meets up exactly with the tail of the current one or its before it. (doesnt have a 1pixel gap at least between them)
		{
			// Next Cur
			s_pPrev = pCurr;// track previous properly, may recusively insert spans!
			pCurr	= pCurr->next;
			// Continue Checking

		if( pCurr == pCurr->next )
		{
			Displayf("curr == curr->next !");


			pCurr->next = NULL;// hack terminator
			return 0;// hack
		}


			continue;			
		}
		else if (newSpan_x0 >= GetViewSSB( GetActiveViewID() )->GetSpanBufferWidth() || newSpan_x1 < 0)
		{
			Assert( 0 && "offscreen span was asked to be inserted, higher level code should have clipped this" );// sanity check
		// Simply off-screen? (do this check again 'cause New is changin')
		// NOTE: we are also doing the screenclip test also at higher levels for trivial rejection of the entire primitive, not just the spans it turns into.

			// ##
			s_pPrev = pCurr;// track previous properly, may recusively insert spans!
			pCurr	= pCurr->next;
			// ##
			// Return
			return 0;// nothing added!
		}
		else if (newSpan_x0 == currSpan_x0)
		{
			if(newSpan_x1 == currSpan_x1)
			{
				// class 'C' (spans compared cover same area exactly)
				//
				// ccccc
				// nnnnn
				
				// exact span replacement
				if( b_newSpanCloser )
				{
					pCurr->depth = depth;// update depth info

#if __SPAN_MERGE
					if( pPrev )
					{
						if(pPrev->depth == depth)
						{
#if __DEV
							g_SpanMerges++;
#endif
							//Displayf(" merging abutting same depth spans ");
							// previous span has same depth, extend it, and kill this one off.
							pPrev->next = pCurr->next;
							pPrev->end  = newSpan_x1;// extend

							this->ReleaseSpan( pCurr );
							
				
							return 0;// all done.
						}
					}
					else
					{
						// no previous, must be a starting edge so there is no fragment to 'step back' to and merge.
					}
#endif
				}

				// ##
				s_pPrev = pCurr;// track previous properly, may recusively insert spans!
				pCurr	= pCurr->next;
				// ##
				// fallthrough case
				return 0;// new span was behind, nothing was added (fully occluded)
			}

			// wasnt an identical span case so fallthrough, resulting cases are....

			// class 'B'

			// (B1)
			// cccccc
			// nnnn
			// 
			// *or*
			//
			// (B2)
			// cccccc
			// nnnnnnnn

			// case B1
			if( currSpan_x1 > newSpan_x1 )
			{
				// current span extends past the current one....

				if ( b_newSpanCloser )
				{
					// current span is behind the new one.

					// cccccc
					// nnnn

					// clips into:

					// xxxxcc
					// nnnn
					fSpan_t *pNewSpan;

#if __SPAN_MERGE
					// check first if previous span is the same depth.
					if( pPrev && pPrev->depth == depth )
					{
						// it is, just extend the previous span's right edge, without allocating another span and clipping the left edge of pCurr;
#if __DEV
						g_SpanMerges++;
#endif
						
						pPrev->end   = newSpan_x1;
						pCurr->start = newSpan_x1;
						return (newSpan_x1 - newSpan_x0);// span pixels were updated.
					}
#endif

					// allocate new span, insert before the current one
					pNewSpan = AllocateSpan();
					pNewSpan->next = pCurr;// new span's next is 'current' because it comes before it now.
					pNewSpan->start= newSpan_x0;
					pNewSpan->end  = newSpan_x1;
					pNewSpan->depth= depth;

					// clip 'current' span's left edge to tail of the new span
					pCurr->start = newSpan_x1;
					Assert( pCurr->end	== currSpan_x1 );// sanity check

					// re-link lists properly.
					if( pPrev )
					{
						pPrev->next = pNewSpan;// previous span point to the newly inserted one thats going before pCurr

					}else{// no prev, this is new head of list.

						// new span is starting entry.
						SetStartingSpanScanlineForActiveView( pNewSpan );			
					}

					// ##
					s_pPrev = pCurr;// track previous properly, may recusively insert spans!
					pCurr	= pCurr->next;
					// ##
					return (newSpan_x1 - newSpan_x0);
				}
				else
				{
					// current span is closer....

					// nnnn
					// cccccc

					// ##
					s_pPrev = pCurr;// track previous properly, may recusively insert spans!
					pCurr	= pCurr->next;
					// ##
					return 0;// new span is completely behind the existing one, so we add nothing.				
				}
			}
			else
			{
				// case B2
				//
				// (b2a)
				// cccccc
				// nnnnnnnn
				//
				// *or*
				//
				// (b2b)
				// nnnnnnnn
				// cccccc
				if( currSpan_x1 < newSpan_x1 )
				{					
					if ( b_newSpanCloser )
					{

#if __SPAN_MERGE
						// check first if previous span is the same depth.
						if( pPrev && pPrev->depth == depth )
						{
#if __DEV
							g_SpanMerges++;
#endif
							// it is, just extend the previous span's right edge, without allocating another span and clipping the left edge of pCurr;
							
							pPrev->end   = pCurr->end;
							pPrev->next  = pCurr->next;// relink prev to next of 'curr'
							// release current, and insert test a new span that is the remainder if the new span.
							this->ReleaseSpan( pCurr );
							// xxxxxx
							// pppppp22

							return InsertSpan( pPrev->next, pPrev->end, newSpan_x1, depth );// insert test remainder.
						}
						else
#endif
						{

							// (b2a)
							// cccccc
							// nnnnnnnn

							// new span extends past the end of current, so we have to chop it and insert test the remainder.
							// becomes:

							// xxxxxx
							// cccccc22

							// update curr to new closer depth.
							pCurr->depth = depth;
							s_spanCoverage += (currSpan_x1 - currSpan_x0);// track amount we added(updated depth) to buffer
						}
					}
					else
					{
						// if new span isnt closer, then its case (b2b)
						// we do nothing here, because the clipped span is done unconditionally below here.
					}
					// regardless of the new span depth relative to the 'current', the part extending past 'current' right edge is clipped and insertion tested.

					// insert remainder of new span, testing it against the next span in the list.

					s_pPrev = pCurr;// track previous properly, may recusively insert spans!

					Assert( pCurr->next != NULL );// should not be possible because we clip to screen edges!
					Assert( currSpan_x1 < newSpan_x1);// sanity check
					return InsertSpan( pCurr->next, currSpan_x1, newSpan_x1, depth );
				}
				else
				{
					Assert(0);// impossible case
				}
			}
		}
		else if ( newSpan_x0 > currSpan_x0 )
		{
			// class 'A'

			// check for case where ends meet exactly:
			//
			// cccccccc
			//    nnnnn
			if( newSpan_x1 == currSpan_x1 )
			{
				if ( b_newSpanCloser )
				{
					// cccccccc
					//    nnnnn
					//
					// becomes:
					//
					// cccxxxxx
					//    nnnnn
					
					// allocate new span and relink
					fSpan_t *pNewSpan = AllocateSpan();
					pNewSpan->next  = pCurr->next;// new span's next is 'current' 's 'next' because it comes after it.
					pNewSpan->start = newSpan_x0;
					pNewSpan->end   = newSpan_x1;
					pNewSpan->depth = depth;

					// modify right edge of current now.
					pCurr->end  = newSpan_x0;
					pCurr->next = pNewSpan;// relink
	
					// ##
					s_pPrev = pNewSpan;// track previous properly, may recusively insert spans!
					pCurr	= pNewSpan->next;
					// ##
					// DONE, span was all taken care of.
					return (newSpan_x1 - newSpan_x0);
				}
				else
				{
					// new span completely covered by existing current span
					// ##
					s_pPrev = pCurr;// track previous properly, may recusively insert spans!
					pCurr	= pCurr->next;
					// ##
					return 0;
				}	
			}
			else if( newSpan_x1 > currSpan_x1 )
			{
				//Displayf(" new %d %d    curr %d %d ", newSpan_x0, newSpan_x1, currSpan_x0, currSpan_x1 );
				Assert( newSpan_x0 < currSpan_x1 );// sanity check
				Assert( newSpan_x1 > currSpan_x0 );// sanity check
				// ccccccc
				//    nnnnnnnn

				// becomes:
				//
				// ccc   
				//    nnnn2222
				fSpan_t	*p_NextTestSpan = NULL;

				s16 currEnd = pCurr->end;

				if ( b_newSpanCloser )
				{
					pCurr->end = newSpan_x0; // clip right edge of current

					// allocate new span and relink after current
					fSpan_t *pNewSpan = AllocateSpan();
					pNewSpan->next  = pCurr->next;// new span's next is 'current' 's 'next' because it comes after it.
					pNewSpan->start = newSpan_x0;
					pNewSpan->end   = currEnd;
					pNewSpan->depth = depth;

					// relink 'current'
					pCurr->next = pNewSpan;

					s_pPrev = pNewSpan;// track previous properly, may recusively insert spans!

					p_NextTestSpan = pNewSpan->next;
				}
				else
				{				   
					//        2222
					// ccccccc

					s_pPrev = pCurr;// track previous properly, may recusively insert spans!

					p_NextTestSpan = pCurr->next;
					// do nothing in this case, we are only chopping the new span and insertion testing it on the next span.

					// fallthrough to chopped span insertion....
				}

				// ##
				pCurr	= pCurr->next;
				// ##

				// insert remainder of new span, testing it against the next span in the list.
				return InsertSpan( p_NextTestSpan, currEnd, newSpan_x1, depth );
			}
			else
			{
				// ccccccccc
				//    nnn
				Assert( newSpan_x1 < currSpan_x1);// only possible case left
				if ( !b_newSpanCloser )
				{
					// ##
					s_pPrev = pCurr;// track previous properly, may recusively insert spans!
					pCurr	= pCurr->next;
					// ##
					return 0;// nothing added, its behind!
				}
				else
				{
					// ccccccccc
					//    nnn
					//
					// becomes:
					//
					// ccc   222
					//    nnn

					// new span is closer, but splits the one behind it in half, insert 2 new spans.
					int a0,a1,a2,a3;
					float currDepth = pCurr->depth;

					// segment markers pushed into temps because linked segments get their markers resorted.
					a0 = pCurr->start;
					a1 = newSpan_x0;
					a2 = newSpan_x1;
					a3 = pCurr->end;

					fSpan_t *pNewSpan  = AllocateSpan();
					fSpan_t *pNewSpan2 = AllocateSpan();

					pCurr->start = (s16)a0;
					pCurr->end   = (s16)a1;
					//pCurr->depth = currDepth;// unnecessary, its already set

					// relink list and insert properly (careful of the order here)
					pNewSpan->next	= pNewSpan2;
					pNewSpan2->next = pCurr->next;
					pCurr->next		= pNewSpan;
					
					// insert new span after current. (middle section)						
					pNewSpan->start = (s16)a1;
					pNewSpan->end   = (s16)a2;
					pNewSpan->depth = depth;
					//pNewSpan->next  = pCurr->next;
					
					// allocate tailing section (same depth as current)					
					pNewSpan2->start = (s16)a2;
					pNewSpan2->end   = (s16)a3;
					pNewSpan2->depth = currDepth;//pCurr->depth;
					//pNewSpan2->next  = pCurr->next;

				// ##
				s_pPrev = pNewSpan2;// track previous properly, may recusively insert spans!
				pCurr	= pNewSpan2->next;
				// ##
					return (a3-a0)-(a2-a1);
			 	}
			}
		}

		return 1;// hack 
//		Assert(0);// should never get here!
	}

	return s_spanCoverage;
}








// TODO: make an Vector4Integer output version?
bool	rmOccluderSystem::TransformAndProject( Vector4 *pResult, const Vector3 &source )
{
	bool  bIsOutside;
	const int viewID    = GetActiveViewID();
	const Matrix44 &C	= GetViewer( viewID ).Get_composite();
	const Matrix44 &VT	= GetViewer( viewID ).Get_screen();	

	float z = source.x*C.a.z + source.y*C.b.z + source.z*C.c.z + C.d.z;
	float w = source.x*C.a.w + source.y*C.b.w + source.z*C.c.w + C.d.w;

	float x = source.x*C.a.x + source.y*C.b.x + source.z*C.c.x + C.d.x;
	float y = source.x*C.a.y + source.y*C.b.y + source.z*C.c.y + C.d.y;
	float invW = 1.0f / w ;
	float xDevScale		= VT.a.x;
	float yDevScale		= VT.b.y;
	float xDevTranslate = VT.d.x;
	float yDevTranslate = VT.d.y;

	

	if (z < -w || z > w)
		bIsOutside = true;// outside near/far space
	else
		bIsOutside = false;

	float	xx = (x * invW) * xDevScale + xDevTranslate;
	float	yy = (y * invW) * yDevScale + yDevTranslate;


	//FIXME: 2048 - (screen_dimension/2)
	xx-= 1792.f;
	yy-= 1824.f;

	//shift down 'y' based on sbuffer resolution relative to the actual render viewport ( >>1 means its half the height of the rendered screen)
	yy = yy * .5f;// >> 1;

	pResult->x = xx;
	pResult->y = yy;
	pResult->z = z;
	pResult->w = w;

	//Displayf(" %f %f %f %f ", xx, yy, z, w );

	return bIsOutside;
}


bool	rmOccluderSystem::IsSphereSpanBufferOccluded( const Vector3 &center, const float radius, const bool b_AddSpans )
{
#if __DEV
	if( g_SpansInUse > 2200 )
	{
		Displayf("almost out of spans! g_SpansInUse = %d ", g_SpansInUse );
		return false;// close to overflow.
	}
#endif


	// project sphere to screen, testing if its visible.
	float	radiusMod = radius;// *.77f;

	//if( radius > 100.f ) return false;// huge objects have sloppier approximation of their true occlusion volume, dont add or test them.
	//if( radius < 2.f ) return false;

	const int viewID    = GetActiveViewID();
	const Matrix44 &C	= GetViewer( viewID ).Get_composite();	//  RSTATE.GetComposite();
	const Matrix44 &VT	= GetViewer( viewID ).Get_screen();		//PIPE.GetViewport()->GetScreen();

	//----------------------
	// need to scale z of full composite by 1/16 to account for change in z - we can never use per vertex fog with the non-clipped path - might bet better to bake this scale in the screen matrix - but that will not play well with old code for now...
//	Matrix44 C;
//	Matrix44 scale;
//	scale.MakeScaleFull( 1.f/1.f, 1.f/1.f, 1.f );
//	C.Dot( scale, CA );
	//----------------------
//	float	InvScaleClipVolume = 1.0f/4.0f;// get this from viewport - hacked to 4x guardband right now.


	// Convert center point to eye space
	float z = center.x*C.a.z + center.y*C.b.z + center.z*C.c.z + C.d.z;
	float w = center.x*C.a.w + center.y*C.b.w + center.z*C.c.w + C.d.w;

	if (z < -w || z > w)
		return false;// outside near/far space

	float x = center.x*C.a.x + center.y*C.b.x + center.z*C.c.x + C.d.x;
	float y = center.x*C.a.y + center.y*C.b.y + center.z*C.c.y + C.d.y;
	float invW = 1.0f / w ;
	float xDevScale		= VT.a.x;
	float yDevScale		= VT.b.y;
	float xDevTranslate = VT.d.x;
	float yDevTranslate = VT.d.y;
#if 0
	z *= invW;// put in -1 to 1 range....
#endif

// (ax,ay)
//	+---------+
//  |         |
//  | (cx,cy) |
//  |    +    |
//  |         |
//  +---------+
//         (bx,by)

	float	topleft_x = ((x-radiusMod) * invW) * xDevScale + xDevTranslate;
	float	topleft_y = ((y-radiusMod) * invW) * yDevScale + yDevTranslate;

//	float	cent_x = ((x) * invW) * xDevScale + xDevTranslate;
//	float	cent_y = ((y) * invW) * yDevScale + yDevTranslate;

	float	botright_x = ((x+radiusMod) * invW) * xDevScale + xDevTranslate;
	float	botright_y = ((y+radiusMod) * invW) * yDevScale + yDevTranslate;

//	Displayf(" xDevTranslate %f  yDevTranslate %f ", xDevTranslate, yDevTranslate );
//	float	sx = ((x) * invW) * xDevScale + xDevTranslate; 
//	float	sy = ((y) * invW) * yDevScale + yDevTranslate;
// convert to integer
	int		ax = (int) (topleft_x * (1.f/1.f));
	int		ay = (int) (topleft_y * (1.f/1.f));

//	int		cx = (int) (cent_x * (1.f/1.f));
//	int		cy = (int) (cent_y * (1.f/1.f));

	int		bx = (int) (botright_x * (1.f/1.f));
	int		by = (int) (botright_y * (1.f/1.f));

	//FIXME: 2048 - (screen_dimension/2)
	ax-= 1792;
	ay-= 1824;

//	cx-= 1792;
//	cy-= 1792;

	bx-= 1792;
	by-= 1824;

	//shift down 'y' based on sbuffer resolution relative to the actual render viewport (>>1 means its half the height of the rendered screen)
	ay = ay >> 1;
	by = by >> 1;


//	Displayf("ctr( %f, %f, %f) rad( %f )  (Z %f) a( %d, %d ) b( %d, %d ) c( %d, %d )", center.x, center.y, center.z, radius, z, ax,ay,bx,by,cx,cy );
	// sanity check on sprite corner positions being min/max ordered.
	Assert( bx >= ax );
	Assert( ay >= by );// Y coordinates are flipped!

	//Displayf("tx,ty ( %f, %f )      bx,by ( %f, %f ) ", tx, ty, bx, by );
	/// InsertDepthSprite( int x0, int y0, int x1, int y1, float depth )

	z -= radius;


	if( z < 1.f )
		return false;

	if( !b_AddSpans )
	{
		if( !TestDepthSprite( ax, by, bx, ay, z ) ){
			return true;// occluded!		
		}
	}
	else
	{
		if( !InsertDepthSprite( ax,by, bx, ay, z ) ){
			//Displayf(" span occluded " );
			return true;// occluded!		
		}
	}

	return false;// hidden!
}


// projected screen space (pixel coords)
bool	rmOccluderSystem::TestDepthSprite( int x0, int y0, int x1, int y1, float depth )
{
	// step: verify inputs are the correct order, like the diagram. (with 0,0 being the top left screen corner)
	// (x0,y0)
	//		+-----------+
	//		|           |
	//		|           |
	//		+-----------+
	//					(x1,y1)
#if _OCCLUDER_DEBUGGING
	Assert( x1 >= x0 );
	Assert( y1 >= y0 );
#endif

	int screen_w = (int) GetSpanBufferWidth( GetActiveViewID() );
	int screen_h = (int) GetSpanBufferHeight( GetActiveViewID() );

	// STEP: test if rect is even onscreen (disjoint?)
	if( x1 <= 0  )
		return false; // off left edge.

	if( x0 >= screen_w )
		return false; // off right edge.

	if( y1 <= 0 )
		return false; // off top edge.

	if( y0 >= screen_h )
		return false; // off bottom edge.

	// STEP: clip it against screen, it wasn't completely off so must be contained/crossing/surrounding.
	if( x0 < 0 )// left edge
		x0=0;
	if( x1 >= screen_w )// right
		x1 = screen_w-1;

	if( y0 < 0 )// top
		y0=0;

	if( y1 >= screen_h ) // bottom
		y1 = screen_h-1;


	// STEP: reject 0 area surfaces that may result from clipping.
	if( (y1-y0) <= 0 )
		return false;// insertion failed

	if( (x1-x0) <= 0 )
		return false;// insertion failed

	// TODO: when we implement the heirarchical tile depth buffer, clip the depth sprite to that for trivial rejection tests before we break it down per scanline into spans.


	// push spans into the buffer.
	bool	b_spanInserted = false; // assume its not entered.

	int totalCoverage = 0;

	//
	// NOTE: y bottom edge of sprite is not inclusive '<' *not* '<='
	//			so stacked sprites dont have double coverage tests where a bottom of one sprite touches another sprite of equal depths' top edge.
	//
	for(int curr_y= y0; curr_y < y1; curr_y++)
	{
		m_activeScanline = (s16)curr_y;// update active scanline info so insertion code knows what span we are working on!

		//----------------------------
		// global system value resets
		s_pPrev = NULL;		// null before insertion of new span, because we are starting at the list beginning
		s_spanCoverage = 0; // reset recursive span coverage counter (sum this with the returned insertion call to get total pixels added)
		//----------------------------

		fSpan_t *pStartSpan = GetStartingSpanForViewScanline( GetActiveViewID(), curr_y );
		Assert( pStartSpan != NULL );

		int cover = TestSpanOnly( pStartSpan, (s16)x0, (s16)x1, depth );// TODO: add parameter to allow 1-4pixel spans to be ignored? NOTE: Actually this may prevent fusion in same cases, better not do that.
		// HOWEVER we could let it add the 1-2pixel span to create the fused spans, and return an occluded result for the object we are processing!

		cover += s_spanCoverage;	// account for recursive insertion
		totalCoverage += cover;		// total amount from all span tracking.

		// Did we add any pixels to the span buffer?
		if( cover > 3 )// TODO: make this tunable? so if we add maybe just 1 or 2 pixels per span its ok and we say its occluded anyways?
		{
			b_spanInserted = true;
			break;// early out, testing only.
		}
	}



	// STEP: Were all the spans occluded? then this object isnt visible if the depthsprite is considered to be representative of the rendered area of the object being tested.
	if( !b_spanInserted )
	{
		// not visible!
		return false;// no insertion occurred, object is considered to be not visible.
	}

	return true;// object was inserted. object is visible.
}

// projected screen space (pixel coords)
bool	rmOccluderSystem::InsertDepthSprite( int x0, int y0, int x1, int y1, float depth )
{
	// step: verify inputs are the correct order, like the diagram. (with 0,0 being the top left screen corner)
	// (x0,y0)
	//		+-----------+
	//		|           |
	//		|           |
	//		+-----------+
	//					(x1,y1)
#if _OCCLUDER_DEBUGGING
	Assert( x1 >= x0 );
	Assert( y1 >= y0 );
#endif

	int screen_w = (int) GetSpanBufferWidth( GetActiveViewID() );
	int screen_h = (int) GetSpanBufferHeight( GetActiveViewID() );

	// STEP: test if rect is even onscreen (disjoint?)
	if( x1 <= 0  )
		return false; // off left edge.

	if( x0 >= screen_w )
		return false; // off right edge.

	if( y1 <= 0 )
		return false; // off top edge.

	if( y0 >= screen_h )
		return false; // off bottom edge.

	// STEP: clip it against screen, it wasn't completely off so must be contained/crossing/surrounding.
	if( x0 < 0 )// left edge
		x0=0;
	if( x1 >= screen_w )// right
		x1 = screen_w-1;

	if( y0 < 0 )// top
		y0=0;

	if( y1 >= screen_h ) // bottom
		y1 = screen_h-1;


	// STEP: reject 0 area surfaces that may result from clipping.
	if( (y1-y0) <= 0 )
		return false;// insertion failed

	if( (x1-x0) <= 0 )
		return false;// insertion failed

	// TODO: when we implement the heirarchical tile depth buffer, clip the depth sprite to that for trivial rejection tests before we break it down per scanline into spans.


	// push spans into the buffer.
	bool	b_spanInserted = false; // assume its not entered.

	int totalCoverage = 0;

	//
	// NOTE: y bottom edge of sprite is not inclusive '<' *not* '<='
	//			so stacked sprites dont have double coverage tests where a bottom of one sprite touches another sprite of equal depths' top edge.
	//
	for(int curr_y= y0; curr_y < y1; curr_y++)
	{
		m_activeScanline = (s16)curr_y;// update active scanline info so insertion code knows what span we are working on!

		//----------------------------
		// global system value resets
		s_pPrev = NULL;		// null before insertion of new span, because we are starting at the list beginning
		s_spanCoverage = 0; // reset recursive span coverage counter (sum this with the returned insertion call to get total pixels added)
		//----------------------------

		fSpan_t *pStartSpan = GetStartingSpanForViewScanline( GetActiveViewID(), curr_y );
		Assert( pStartSpan != NULL );

		int cover = InsertSpan( pStartSpan, (s16)x0, (s16)x1, depth );// TODO: add parameter to allow 1-4pixel spans to be ignored? NOTE: Actually this may prevent fusion in same cases, better not do that.
		// HOWEVER we could let it add the 1-2pixel span to create the fused spans, and return an occluded result for the object we are processing!

		cover += s_spanCoverage;	// account for recursive insertion
		totalCoverage += cover;		// total amount from all span tracking.

		// Did we add any pixels to the span buffer?
		if( cover > 3 )// TODO: make this tunable? so if we add maybe just 1 or 2 pixels per span its ok and we say its occluded anyways?
		{
			b_spanInserted = true;
		}
	}



	// STEP: Were all the spans occluded? then this object isnt visible if the depthsprite is considered to be representative of the rendered area of the object being tested.
	if( !b_spanInserted )
	{
		// not visible!
		return false;// no insertion occurred, object is considered to be not visible.
	}

	return true;// object was inserted. object is visible.
}

#endif
