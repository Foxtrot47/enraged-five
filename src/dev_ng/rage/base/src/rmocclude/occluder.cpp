#include "bank/bank.h"
#include "data/resource.h"

#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"

#include "diag/memstats.h"
#include "system/timemgr.h"
#include "string/string.h"
#include "system/param.h"
#include "vector/matrix44.h"
#include "vectormath/legacyconvert.h"

#include "grcore/viewport.h"
/*
// apparently dont need these since i moved all the drawcode to its own file!
#include "grcore/device.h"
#include "grcore/state.h"
#include "grcore/im.h"
*/
#include "rmocclude/occluder.h"
#include "system/cache.h"


//namespace rage {


#define	__SPANBUFFER_ENABLED				0 & 1
#define	_OCCLUDER_IGNOREFACE_EDGEKILL		0	// should always be 0
#define	_OCCLUDER_DEBUGGING					0 & __DEV
#define	_OCCLUDER_WARNINGS					0
#define	_OCCLUDER_TIMING					0
#define	_OCCLUDER_REMOVE_UNDERGROUNDEDGE	1	// optimization to remove an edge if it is very far down.
#define	_OCCLUDER_REJECT_LOWEDGES			0	// remove edges very low to ground that we are above (paris waterfront)
#define	__POLYSPAN_DEBUG					0 & __DEV

#undef min
#define min(a, b)	(a) < (b) ? a : b

#if !__SPU
Color32					rmOccluderSystem::sm_debugColor(0.f, 1.f, 1.f, 1.f);
#endif

bool					rmOccluderSystem::sm_bOccludeActiveOccluders			= true;

bool					rmOccluderSystem::sm_UsingOccludedVolumeGlobalColor		= false;
bool					rmOccluderSystem::sm_bDrawOccludedVolumes				= false;
bool					rmOccluderSystem::sm_AreOccludedVolumesBuffered			= false;
bool					rmOccluderSystem::sm_AllowDebugDrawOfOccludedVolumes	= false;

//rmOccluderSystem *		rmOccluderSystem::sm_gCurrOccluderSysInstanceActive = NULL;// FIXME: remove this to support multiple instances/threads
bool					rmOccluderSystem::sm_FreezeAllOccluderViews			= false;
bool					rmOccluderSystem::sm_DbgFrustumCullAndPvsFrustumCull= false;

bool					rmOccluderSystem::sm_UseOccluders					= true;
rmPvsDebugDrawModeBits	rmOccluderSystem::sm_DebugDrawOptions				= pvsdd_Empty;

float					rmOccluderSystem::sm_DebugAnimTimeDelta				= 0.f;

// FRUSTUM
bool		rmOccluderSystem::sm_DebugDrawFrustum			= true;			
float		rmOccluderSystem::sm_DrawFrustumVolumeDistance	= 50.f;
bool		rmOccluderSystem::sm_DebugDrawFrustumAsOpaque	= false;
bool		rmOccluderSystem::sm_DebugDrawFrustumEdgesOnly	= true;


// UMBRA
bool		rmOccluderSystem::sm_DebugDrawUmbra				= true;
bool		rmOccluderSystem::sm_DebugDrawUmbraAsOpaque		= false;
float		rmOccluderSystem::sm_DrawUmbraVolumeDistance	= 50.f;
bool		rmOccluderSystem::sm_TraceViewpointToUmbra		= true;
float		rmOccluderSystem::sm_ViewpointTraceEdgeScale	= -0.35f;// should be negative!

// OCCLUDER
bool		rmOccluderSystem::sm_DrawOccluderVolumes		= false;
bool		rmOccluderSystem::sm_DrawOccluderVolumeNames	= true;
bool		rmOccluderSystem::sm_DrawActiveOccluderVolumes  = false;
bool		rmOccluderSystem::sm_DisplayEdgeClippingInfo	= true;
bool		rmOccluderSystem::sm_DisplayEntireOccluderVolume= true;
float		rmOccluderSystem::sm_DrawOccluderVolumeDistance = 300.f;

float		rmOccluderSystem::sm_OccluderDistanceActivationScale = 1.f;

int			rmOccluderSystem::sm_OccludedDrawBoundTotal = 0;	
int			rmOccluderSystem::sm_maxDebugOccluders = 0;		
OccludedBoundInfo *rmOccluderSystem::sm_a_bndInfo = NULL;

//static	float	solid_groundplane	= -40.f;// dont use edges that are below this point on Y axis.

// WARNING: FIXME: Box culling cant go on until we fix issues with city facade's inside occluders disappearing.
//						(they have to fix the occluder geoms to not contain a mesh's AABOX because it makes it cull out
//bool	g_bOccludeTestAABoxes	= false;
//bool	g_bFrustumTestAABoxes	= false;
bool	m_Use_VU0_Occlusion			= false;




// // NOTE: 3 planes per matrix, so each view takes 2 matrices (6planes to define the frustum (convex hull))
// Matrix44	sm_ScratchPadFastCullMtx[ 4 ];
// Matrix44	sm_rmOccludeFastCullMtx[ 2 * 2 * RM_OCCLUDE_MAX_VIEWS ];

bool	*gp_AsyncVU0_OccludeResult;	// storage for async occlusion test
//float	m_OccluderDistanceActivationScale = 1.f;


#if	__BANK && !__SPU
void	rmOccluderSystem::AddWidgets	(bkBank &bk)
{

	// If 'sm_AreOccludedVolumesBuffered' is ON and buffer space has been allocated to store occluded bound types, allow debug drawing.
	bk.AddToggle("sm_AllowDebugDrawOfOccludedVolumes",	&sm_AllowDebugDrawOfOccludedVolumes);
	bk.AddToggle("sm_AreOccludedVolumesBuffered",		&sm_AreOccludedVolumesBuffered);

	bk.AddToggle("m_UseOccluders",						&sm_UseOccluders);
	// If an object animates, it may become un-occluded if its no longer contained in the frozen occlusion frusta.
	// you really want to freeze any animation on animated objects in conjunction with this option. //  [3/15/2006 ccoffin]
	bk.AddToggle("sm_FreezeOcclusionFrusta",			&sm_FreezeAllOccluderViews			);

	bk.AddToggle("sm_bOccludeActiveOccluders",			&sm_bOccludeActiveOccluders			);


	bk.AddToggle("sm_DrawOccluderVolumes",				&sm_DrawOccluderVolumes				);// master draw toggle
	bk.AddToggle("sm_DrawOccluderVolumeNames",			&sm_DrawOccluderVolumeNames			);
	bk.AddToggle("sm_DrawActiveOccluderVolumes",		&sm_DrawActiveOccluderVolumes		);// special rendering to draw active occluders.

	bk.AddToggle("sm_DbgFrustumCullAndPvsFrustumCull",	&sm_DbgFrustumCullAndPvsFrustumCull	);
	bk.AddSlider("sm_DrawOccluderVolumeDistance",		&sm_DrawOccluderVolumeDistance ,1.0f, 10000.0f,1.0f);


	bk.PushGroup("ViewFrustum",true);
		bk.AddToggle("sm_DebugDrawFrustum",				&sm_DebugDrawFrustum);
		bk.AddToggle("sm_DebugDrawFrustumAsOpaque",		&sm_DebugDrawFrustumAsOpaque);
		bk.AddToggle("sm_DebugDrawFrustumEdgesOnly",	&sm_DebugDrawFrustumEdgesOnly);
		bk.AddSlider("sm_DrawFrustumVolumeDistance",	&sm_DrawFrustumVolumeDistance ,0.0f,100.0f,0.1f);// TODO: IMPLEMENT!
	bk.PopGroup();

	bk.PushGroup("Occluders",true);
		bk.AddToggle("sm_DisplayEdgeClippingInfo",		&sm_DisplayEdgeClippingInfo);
		bk.AddToggle("sm_DisplayEntireOccluderVolume",	&sm_DisplayEntireOccluderVolume);
		bk.AddSlider("sm_OccluderDistanceActivationScale",	&sm_OccluderDistanceActivationScale, 0.1f, 10.f, 0.2f);

	bk.PopGroup();

/*
	bkToggle* bkBank::AddToggle(const char *title,atBitSet *value,unsigned char mask,datCallback callback,const char *memo) {
		return (bkToggle*) AddWidget(*(rage_new bkToggle_atBitSet(callback,title,memo,value,mask)));
	}
	bank.AddSlider("Property Flags: ",&m_PropertyFlags,0,PROPERTY_FLAGS_ALL,1);
	bank.AddToggle("PRIORITY_PUSHER",&m_PropertyFlags,phArchetype::PROPERTY_PRIORITY_PUSHER);

	bank.AddButton("Save",datCallback(CFA1(phArchetype::SaveFromWidgets),this));
*/


	bk.PushGroup("Umbra",true);
		bk.AddToggle("DebugDrawUmbra",				&sm_DebugDrawUmbra );// control drawing any umbra related elements globally.
		bk.AddToggle("sm_DebugDrawUmbraAsOpaque",	&sm_DebugDrawUmbraAsOpaque );
		bk.AddSlider("DrawUmbraVolumeDistance",		&sm_DrawUmbraVolumeDistance ,0.0f,100.0f,0.1f);

		bk.AddToggle("TraceViewpointToUmbra",		&sm_TraceViewpointToUmbra );
		bk.AddSlider("ViewpointTraceEdgeScale",		&sm_ViewpointTraceEdgeScale ,-10.0f, 10.0f,0.1f);
	bk.PopGroup();



#if 0// need to redo these, make sure each one works properly before re-adding them to the bank.

	bk.AddSlider("ActivateDistScale",				&m_OccluderDistanceActivationScale, 0.1f, 10.f, 0.2f);

	// NOTE: Removes edges outside the view frustum (currently both endpoints of an edge must reside outside the plane which doesnt catch all cases)
	bk.AddToggle("m_UseOccluderFrustumFusion",		&m_UseOccluderFrustumFusion);// toggleable in __DEV builds, always ON in final.

	// Occluder debug drawing
	bk.AddToggle("m_DrawAllOccluders",				&m_DrawAllOccluders);
	bk.AddToggle("m_DrawActiveOccluders",			&m_DrawActiveOccluders);

	


	// span buffer
	bk.PushGroup("SpanBufferOcclusion",false);
		bk.AddToggle("m_UseSpanBuffering",				&m_UseSpanBuffering);
		bk.AddToggle("m_TestSpansBeforeObjectFrusta",	&m_TestSpansBeforeObjectFrusta);
		bk.AddToggle("m_AllowSpanDebugDrawing",			&m_AllowSpanDebugDrawing);
	bk.PopGroup();

#endif

}
#endif

#if !__SPU
PARAM( debugoccluders,"amount of occluder data buffered for visual debugging");
#endif
//-----------------------------------------------------------------------------
rmOccluderSystem::rmOccluderSystem(void)
{
	//Assert( sm_gCurrOccluderSysInstanceActive == NULL );
	//sm_gCurrOccluderSysInstanceActive	= this;
	gp_AsyncVU0_OccludeResult			= NULL;


	sm_OccludedDrawBoundTotal				= 0;
	sm_AllowDebugDrawOfOccludedVolumes		= false;
	sm_FreezeAllOccluderViews				= false;
	sm_bDrawOccludedVolumes					= false;
	sm_bOccludeActiveOccluders				= true;

	sm_UseOccluders					= true;
	m_UseOccluderFrustumFusion 		= true;
	
	m_DrawActiveOccluders			= false;
	m_UseSpanBuffering				= false;
	m_TestSpansBeforeObjectFrusta	= false;
	m_AllowSpanDebugDrawing			= false;

#if __DEV
	extern sysParam PARAM_debugoccluders;

	if( PARAM_debugoccluders.Get(sm_maxDebugOccluders) )
	{
		Displayf("Debug Occluders set to %d", sm_maxDebugOccluders );
	}
	else
	{
		Displayf("Debug Occluders Not Set" );
		sm_maxDebugOccluders = 2;
	}
#else
	sm_maxDebugOccluders = 0;
#endif	

	Assert( sm_a_bndInfo == NULL );// skip allocating when we start using multiple instances of rmOccluderSystem??? //  [3/22/2006 ccoffin]
	sm_a_bndInfo = rage_new OccludedBoundInfo[ sm_maxDebugOccluders ];

	// occluder pool data
	m_numOccluderPolys	= 0;
	m_OccluderPolyArray = NULL;

	m_numOccluderGeoms	= 0;
	m_OccluderGeomArray = NULL;

	// state stuff
	m_bFrameInProgress			= false;
	m_bOccludersEnabled			= true;// higher level load code flips this to on if we find and load a valid .occlude file... (see layercity.c)
	

	if( sm_maxDebugOccluders == 0 )
	{
		sm_AreOccludedVolumesBuffered	= false;
	}
	else
	{
		sm_AreOccludedVolumesBuffered	= true;// buffering on if they have -debugoccluders on the commandline, we assume they want to see the bounds
	}

	// clear array counters that track # of active occluders!
	//memset (m_numActiveOccluders, 0, sizeof m_numActiveOccluders );

	m_active_viewerID		= 0;
	m_OccludedCount			= 0;

#if HACK_GTA4
	m_pvsViewers = 0;
	m_numPvsViewers = 0;
	m_maxPvsViewers = 0;	
#endif // HACK_GTA4

	//DeactivateAllViews();// make sure all views are disabled until configured by app.
}


void rmOccluderSystem::Destroy()
{
	delete [] sm_a_bndInfo;
	sm_a_bndInfo = NULL;

	ReleasePolys( &(this->m_numOccluderPolys), &(this->m_OccluderPolyArray) );// release occluder polyset
	ReleaseGeoms( &(this->m_numOccluderGeoms), &(this->m_OccluderGeomArray) );// release occluder geomset

//	sm_gCurrOccluderSysInstanceActive = NULL;// FIXME: remove to support multiple instances.
	gp_AsyncVU0_OccludeResult = NULL;

	delete	this;
}



rmOccludeDynamicInst::rmOccludeDynamicInst()
{
	m_linked		= false;// not linked to an rmOccludePolyhedron yet!
	m_pLocalVerts	= NULL;
	m_pOccluder		= NULL;
}


rmOccludeDynamicInst::~rmOccludeDynamicInst()
{
	if( m_pOccluder )// if it had an occluder ptr, then it had to be linked.
	{
		Assert( m_linked );

		// copy back local space verts
		int numverts = m_pOccluder->GetNumVerts();

		for(int n=0;n< numverts; n++)
		{
			m_pOccluder->SetVertex( n , m_pLocalVerts[n] );// restore local space verts.			
		}
	}


	m_linked = false;
	delete [] m_pLocalVerts;
	m_pLocalVerts = NULL;
}


void	rmOccludeDynamicInst::LinkToBaseOccluder( rmOccludePolyhedron *pOccluder )
{
	Assert( pOccluder );
	Assert( m_linked == false );// we dont allow re-links because that would let people try to point more than 1 instance to a single occluder which isnt really safe.

	m_numVerts = pOccluder->GetNumVerts();
	Assert( m_numVerts );

	m_pLocalVerts = rage_new Vector3[ m_numVerts ];
	Assert( m_pLocalVerts );

	for(int n=0;n< m_numVerts; n++)
	{
		// copy out local space verts.
		m_pLocalVerts[n] = pOccluder->GetVertex(n);
	}

	m_pOccluder = pOccluder;
	m_linked	= true;
}

void	rmOccludeDynamicInst::Transform	(Matrix34 &localToWorld )// transform and write the results into the occluder it owns
{
	// Transform local verts stored in this instance, writing them into the occluder its linked itself to.

	// TODO: FIXME: Higher level code should skip the transform step and not attempt to add the occluder
	//					if its far away or the transform would place it out of view (use bounding sphere quick test). (cmc)

	// Transform one vertex at a time and poke results inthe base occluder object.
	Vector3	worldspaceVertex;
	int		vertCount = m_numVerts;

	rmOccludePolyhedron *pOccluder = GetLinkedOccluderPtr();
	Assert( pOccluder );
	Assert( pOccluder->GetNumVerts() == this->m_numVerts );// Make sure vertex pools match, if they don't, someone must have tried to change the link to another occluder object which isnt allowed.

	for(int i=0; i < vertCount ; i++)
	{
		localToWorld.Transform( m_pLocalVerts[i], worldspaceVertex  );
		// copy into occluder
		pOccluder->SetVertex( i, worldspaceVertex );
	}
}

/*
class rmOccludePolyhedronDynamic : public rmOccludePolyhedron
{
		//void				Copy			(rmOccludePolyhedron &);
		//void				Transform		(Matrix34 &);
		//void				Recalculate		(void);// NOTE: Cannot recalc an already active occluder once a frame starts...
};


// NOTE: matrices need to be grouped one after another in sequential order to simplify matrix loading in VU code based on viewerID
Matrix44*	rmOccluderSystem::GetViewerCullMtxPtr_0(const int viewerID)
{
	return this->GetViewer( viewerID ).GetCullMtx0();
	//return &sm_rmOccludeFastCullMtx[ viewerID << 1 ];
}

Matrix44*	rmOccluderSystem::GetViewerCullMtxPtr_1(const int viewerID)
{
	return this->GetViewer( viewerID ).GetCullMtx1();
	//return &sm_rmOccludeFastCullMtx[ (viewerID << 1)+1 ];
}

*/







// Holds the face definitions for faces used to build convex occluder polyherda

typedef struct s_vert
{
	Vector4	v4screenVtx;
    s32		x, y;
		
    struct s_vert *next;
	struct s_vert *prev;
} sVert;

typedef struct s_poly
{
        sVert  verts[32];
        //char   color;
} sPoly;


sVert *GetNextVertex_LeftSide( bool bIsClockwise, sVert *pCurr )
{
	if( bIsClockwise )
	{
		return pCurr->prev;
	}else{
		return pCurr->next;
	}
}

sVert *GetNextVertex_RightSide( bool bIsClockwise, sVert *pCurr )
{
	if( bIsClockwise )
	{
		return pCurr->next;
	}else{
		return pCurr->prev;							
	}
}

s32	calcDelta( sVert *start, sVert *end )
{
	//Assert( start	!= NULL );
	//Assert( end		!= NULL );
	s32	delta_Y;
	delta_Y =  (end->y - start->y);

	if( delta_Y == 0 )
	{
		Displayf("calcDelta delta_Y == 0 !" );
		delta_Y = 1;
	}

	return ( ((end->x - start->x)<<4) / delta_Y);
}




#if HACK_GTA4
void rmOccludeFacet::Init()
#else
rmOccludeFacet::rmOccludeFacet( void )
#endif
{
	m_num_verts				= 0;
	m_pVertIndices			= NULL;
	m_pEdgeIndices			= NULL;
//	m_pEdgeIndicesReversed	= NULL;
	m_FaceEdgeIndicesReverseBits = 0U;
}

#if HACK_GTA4
void rmOccludeFacet::Release()
#else
rmOccludeFacet::~rmOccludeFacet( void )
#endif // HACK_GTA4
{
	//Assert( m_pVertIndices != NULL );
	
	//if( m_pVertIndices )
	delete [] m_pVertIndices;
	delete [] m_pEdgeIndices;
	//delete [] m_pEdgeIndicesReversed;

	m_num_verts				= 0;
	m_pVertIndices			= NULL;
	m_pEdgeIndices			= NULL;
	// m_pEdgeIndicesReversed	= NULL;
	m_FaceEdgeIndicesReverseBits = 0U;
};

void	rmOccludeFacet::SetEdgeFlag(int edge, char flag )
{
#if 1
	if(flag)
	{
		// set
		m_FaceEdgeIndicesReverseBits |= (1U<<edge);
	}
	else
	{
		// clear bit
		m_FaceEdgeIndicesReverseBits &= ~(1U<<((u32)edge & ~0U)); 
	}
#else
	m_pEdgeIndicesReversed[edge] = flag;
#endif
}


void rmOccludeFacet::SetVertexIndex(int n, char indexSetValue )
{
	Assert( (n >= 0) && (n <= m_num_verts) );// make sure we are setting an index inthe allowable range.

	// cant range check the index set value, have no idea what the vertex pool size is.
	// if we passed a ptr of the owning polyhedron in, we could check that way.
	m_pVertIndices[n] = indexSetValue;
}

void	rmOccludeFacet::SetEdgeIndex(int edge, char value )
{
#if _OCCLUDER_DEBUGGING
	Assert( m_pEdgeIndices != NULL );
#endif

	m_pEdgeIndices[edge] = value;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// rmOccludePolyhedron

rmOccludePolyhedron::rmOccludePolyhedron()
{
	m_activeDist	= 0;
	m_num_edges		= 0;
	m_num_planes	= 0;
	m_num_verts		= 0;
	m_pEdges		= NULL;
	m_pFacets		= NULL;
#if __RM_OCCLUDE_STORED_GEOM_PLANES
	m_pPlanes		= NULL;
#endif
	m_pVerts		= NULL;

	SetActiveMode( true );// on by default to be backward compatible. Later, file format updates or other mechanisms can override this.
}

rmOccludePolyhedron::~rmOccludePolyhedron()
{
//	int nPlanes = m_num_planes;
#if HACK_GTA4
	for(int i=0; i<m_num_planes; ++i)
		m_pFacets[i].Release();
#endif // HACK_GTA4
	delete [] m_pFacets;
	m_pFacets = NULL;
#if __RM_OCCLUDE_STORED_GEOM_PLANES
	delete [] m_pPlanes;
	m_pPlanes = NULL;
#endif

	delete  [] m_pEdges;
	m_pEdges = NULL;

	delete [] m_pVerts;
	m_pVerts = NULL;
//	for(int n=0; n < nPlanes; n++)
//	{
//		DestroyFacet( n );
//	}
//	DestroyFacetsAndPlanes();
//	DestroyEdges();
//	DestroyVertexPool();
}

// step #4 allocate index pool for a facet. then flesh out its contents in next step.
void	rmOccludePolyhedron::CreateFacet( int facet, int num_ngon_verts )
{
	// pools use 'num_ngon_verts' to allocate pools because number of edges that defines a face always equals the number of edges.
	Assert( num_ngon_verts <= 32 );// if we want to store edge reversal data in a bitfield, limit edge indices per facet to 32.
	Assert( num_ngon_verts >=  3 );// make sure it has at least 3 edges to define a valid face.

	m_pFacets[ facet ].m_num_verts	= (char)num_ngon_verts;
	m_pFacets[ facet ].m_pVertIndices = rage_new char [num_ngon_verts];
	m_pFacets[ facet ].m_pEdgeIndices = rage_new char [num_ngon_verts];
	m_pFacets[ facet ].m_FaceEdgeIndicesReverseBits = 0U;
	//m_pFacets[ facet ].m_pEdgeIndicesReversed = new char [num_ngon_verts];
}


void	rmOccludePolyhedron::SetFacetIgnoredFlag( int facet, int isIgnored )
{
	if (isIgnored < 1) // 0 or negative value means false to us.
	{
		m_pFacets[ facet ].b_isIgnored = false;// dont ignore.
	}else{
		m_pFacets[ facet ].b_isIgnored = true;// ignored!
	}
}


void	rmOccludePolyhedron::DestroyFacet( int facet )
{
	m_pFacets[ facet ].m_num_verts		=	 0;
	m_pFacets[ facet ].m_pVertIndices	= NULL;
	m_pFacets[ facet ].m_pEdgeIndices	= NULL;
	//m_pFacets[ facet ].m_pEdgeIndicesReversed = NULL;

	m_pFacets[ facet ].m_FaceEdgeIndicesReverseBits = 0U;
}


void	rmOccludePolyhedron::SetFacetEdgeIndexReverseFlag( int facet, int edge, char flag )
{
	m_pFacets[facet].SetEdgeFlag( edge, flag );
}


void rmOccludePolyhedron::CalculateEdgeWindingData(void)
{
	for(int fc=0; fc < m_num_planes; fc++)
	{
		int num_verts = GetFacetNumVerts( fc );

		for( int v=0; v < num_verts; v++ )
		{
			int v1;

			if( (v+1) >= num_verts)
			{
				v1 = 0;		// wrap
			}else{
				v1 = v+1;	// get next
			}

			int i0 = GetFacetVertIndex( fc, v  );
			int i1 = GetFacetVertIndex( fc, v1 );

			// edge lookup.
			int edgeIdx = GetFacetEdgeIndex( fc , v );
			int e0 = GetEdgeVertexIndex( edgeIdx, 0 );
			int e1 = GetEdgeVertexIndex( edgeIdx, 1 );

			if( (e0 == i0) && (e1 == i1) )
			{
				// winding as normal
				SetFacetEdgeIndexReverseFlag( fc, v, 0 );
			}
			else if( (e0 == i1) && (e1 == i0) )
			{
				// backwards, set flag to 'reversed'
				SetFacetEdgeIndexReverseFlag( fc, v, 1 );
			}else{
				Displayf(" e0 %d e1 %d --- i0 %d i1 %d ", e0,e1,i0,i1 );
				Displayf(" face %d numverts %d vertex pair %d %d ", fc, num_verts, v, v1 );
				AssertMsg(0 , "rmOccludePolyhedron::CalculateEdgeWindingData - bad data indexing, edge indices must match the winding order of vertices" );
			}
		}
	}
	//Displayf(" CalculateEdgeWindingData success " );
}

void rmOccludePolyhedron::CalculateFacetPlanes(void)
{
#if	__RM_OCCLUDE_STORED_GEOM_PLANES
	/*
		Input data winds counter-clockwise (or CW???) , compute plane using just the first 3 points,
		assuming the tool that created the data made sure the whole  n-gon is coplanar (and convex).

	3--2
	| /|
    |/ |
	0--1

	*/
#if _OCCLUDER_DEBUGGING
	Assert( m_num_planes > 3 );// minimum number of planes to form a convex polyhedron.
	Assert( m_pPlanes != NULL);
#endif

	for(int p=0; p < m_num_planes; p++)
	{
		int		i0,i1,i2;
		Vector3	v0,v1,v2;
		
		i0 = GetFacetVertIndex( p, 0 );
		i1 = GetFacetVertIndex( p, 1 );
		i2 = GetFacetVertIndex( p, 2 );

		v0 = GetVertex(i0);
		v1 = GetVertex(i1);
		v2 = GetVertex(i2);
		// lookup 1st 3 indices of each poly, get vertices from pool and compute the plane equation.
		m_pPlanes[p].ComputePlane( v1, v0, v2 );

		// TODO: test angle between points used to create plane to see if its degenerate (line or extremely obtuse triangle).
	}
#else
	// does nothing.
#endif
}



void	rmOccludePolyhedron::SetFacetEdgeIndex(int facet, int edge, int value )
{
#if _OCCLUDER_DEBUGGING
	Assert( m_pFacets != NULL );
#endif
	m_pFacets[facet].SetEdgeIndex( edge, (char)value );
}

void rmOccludePolyhedron::SetFacetVertIndex(int facet, int vertnum, int value )
{
#if _OCCLUDER_DEBUGGING
	Assert( m_pFacets != NULL );
#endif
	m_pFacets[facet].SetVertexIndex( vertnum, (char)value );
}


bool rmOccludePolyhedron::AllocateVertexPool( int numVerts )
{
#if _OCCLUDER_DEBUGGING
	// make sure previous set was released
	Assert( m_num_verts == 0	);
	Assert( m_pVerts	== NULL	);
#endif

	Vector3 * p = rage_new Vector3 [numVerts];

	if (! p) return false;
	
	m_num_verts		= (s8)numVerts;
	m_pVerts		= p;

	return true;
}

void rmOccludePolyhedron::DestroyVertexPool(void)
{
	m_num_verts		= 0;

	delete [] m_pVerts;
	m_pVerts = NULL;
}


bool rmOccludePolyhedron::AllocateEdges( int numEdges )
{
#if _OCCLUDER_DEBUGGING
	// make sure previous set was released
	Assert( m_num_edges == 0	);
	Assert( m_pEdges	== NULL	);
#endif

	rmOccludeEdge * p = rage_new rmOccludeEdge [numEdges];

	if (! p) return false;
	
	m_num_edges		= (s8)numEdges;
	m_pEdges		= p;

	return true;
}

void rmOccludePolyhedron::DestroyEdges(void)
{
	m_num_edges		= 0;

	delete  [] m_pEdges;
	m_pEdges = NULL;
}


bool rmOccludePolyhedron::AllocateFacetsAndPlanes( int num_facets )
{
#if _OCCLUDER_DEBUGGING
	Assert( num_facets > 3 );// minimum plane count to define a cnv defined by 4 planes (tetrahedron)
#endif
	
	m_num_planes	= (s8)num_facets;

	m_pFacets		= rage_new rmOccludeFacet [num_facets];
#if HACK_GTA4
	for(int i=0; i<num_facets; ++i)
		m_pFacets[i].Init();
#endif // HACK_GTA4
#if __RM_OCCLUDE_STORED_GEOM_PLANES
	m_pPlanes		= rage_new Vector4 [num_facets];
#endif

	return true;
}

void rmOccludePolyhedron::DestroyFacetsAndPlanes( void )
{
	m_num_planes = 0;

	delete [] m_pFacets;
	m_pFacets = NULL;

#if __RM_OCCLUDE_STORED_GEOM_PLANES
	delete [] m_pPlanes;
	m_pPlanes = NULL;
#endif
}

#if !__RM_OCCLUDE_STORED_GEOM_PLANES
void  rmOccludePolyhedron::GetPlane( int planeIdx, Vector4 *pv4dest )
{
	Assert( (planeIdx >= 0) && (planeIdx < GetNumFacets()) );

	int		i0,i1,i2;
	Vector3	v0,v1,v2;
	
	i0 = GetFacetVertIndex( planeIdx, 0 );
	i1 = GetFacetVertIndex( planeIdx, 1 );
	i2 = GetFacetVertIndex( planeIdx, 2 );

	v0 = GetVertex(i0);
	v1 = GetVertex(i1);
	v2 = GetVertex(i2);
	// lookup 1st 3 indices of each poly, get vertices from pool and compute the plane equation.
	pv4dest->ComputePlane( v1, v0, v2 );

	// TODO: test angle between points used to create plane to see if its degenerate (line or extremely obtuse triangle).
}

#endif

#if HACK_GTA4
void rmOccludePolyhedron::SetVertices(Vector3* pVerts, u32 numVerts)
{
	m_num_verts = (s8)numVerts;
	m_pVerts = pVerts;
}
void rmOccludePolyhedron::SetEdges(rmOccludeEdge* pEdges, u32 numEdges)
{
	m_num_edges = (s8)numEdges;
	m_pEdges = pEdges;
}
void rmOccludePolyhedron::SetFacets(rmOccludeFacet* pFacets, Vector4* pPlanes, u32 numFacets)
{
	m_num_planes = (s8)numFacets;
	m_pPlanes = pPlanes;
	m_pFacets = pFacets;
}
#endif // HACK_GTA4

/*
PURPOSE
	Sets both edge indices of an edge
PARAMS
	e0	- edge index 
	e1	- edge index value to set

  	(E0)----->(E1)

RETURN
	none
NOTES
	none
*/
void	rmOccludePolyhedron::SetEdgeIndices(char n, char e0, char e1)
{
#if _OCCLUDER_DEBUGGING
	Assert( e0 < GetNumVerts() );// range check
	Assert( e1 < GetNumVerts() );// range check
	Assert( n  < GetNumEdges() );// range check
	Assert( m_pEdges != NULL );
#endif

	m_pEdges[n].SetEdge( 0, e0 );
	m_pEdges[n].SetEdge( 1, e1 );
}

void	rmOccluderSystem::StartNewFrame( void )
{
	Assert( !IsFrameInProgress() );
	//Warningf("Occluded count last frame %d --", m_OccludedCount);
	m_OccludedCount = 0;// reset tracking vars

	// TODO: To keep things easy for now, wipe all the viewers and make the app send them every frame.
	//			when we add stat tracking and some AI to the system we will want to keep viewers
	//			from getting wiped to support temporal coherence and other options.


	m_bFrameInProgress = true;
}


void	rmOccluderSystem::EndFrame(void)
{
	Assert( IsFrameInProgress() );
	m_bFrameInProgress = false;
}

void	rmOccluderSystem::SetOrthogonal(const pvsViewerID id,bool	UseOrthogonal)
{
	PvsViewer	*pViewer = &this->m_pvsViewers[ id ];// &m_views[ viewerID ];
	pViewer->SetIsOrthogonal(UseOrthogonal);

}
bool	rmOccluderSystem::GetOrthogonal(const pvsViewerID id)const
{
	const PvsViewer	*pViewer = &this->m_pvsViewers[ id ];// &m_views[ viewerID ];
	return pViewer->GetIsOrthogonal();

}

// NOTE: render view is different than 'pvs view'
void	rmOccluderSystem::SetRenderView( const pvsViewerID id, const grcViewport* vp, const Vector3 &renderpos, const Vector3 &viewDirection, const Matrix34 *pCameraMatrix )
{
	SetRenderView(&m_pvsViewers[id], vp, renderpos, viewDirection, pCameraMatrix);
}

// NOTE: render view is different than 'pvs view'
void	rmOccluderSystem::SetRenderView( PvsViewer* pViewer, const grcViewport* vp, const Vector3 &renderpos, const Vector3 &viewDirection, const Matrix34 *pCameraMatrix )
{
	Assert(vp);
	//-------------------------------------------
	Vector3 eyepoint;// NOTE: Always copy position to support freeze mode, make sure code below doesnt use the input param either.

	//-------------------------------------------
	Matrix34 cam34		= *pCameraMatrix;				// copy!
	Matrix44 cam44;
	Convert( cam44, *pCameraMatrix);// make a 4x4 version for the setcameramtx function in grcviewport!

	Matrix34 view34; // inverse of cam matrix which is a 3x4, we have to convert it to a 4x4 afterwards.
	if (!view34.Inverse( cam34 ))
		return;

	Matrix44 view44;
	Convert( view44, view34 );// create 4x4 version of view


	//pViewer->m_view44_render = view44;
	//pViewer->m_a_view44[ pvsvt_Render ] = view44;
	pViewer->SetMatrix( pvsvt_Render, pvsmt_View, &view44);

#if !__SPU
	// update 'render' viewport constantly.
	pViewer->m_perspVP_render.Perspective( vp->GetFOVY(), vp->GetAspectRatio(), vp->GetNearClip(), vp->GetFarClip() );

	pViewer->m_orthoVP_render.Ortho( 0.f, (float)vp->GetWidth(), (float)vp->GetHeight(), 0.f,
		vp->GetNearClip(), vp->GetFarClip() );// use -1.f, 1.f for near/far???

	pViewer->m_orthoVP_render.SetCameraMtx( MATRIX44_TO_MAT44V(cam44), MATRIX44_TO_MAT44V(view44));
	//-------------------------------------------
#endif


#if	__DEV
	if( !sm_FreezeAllOccluderViews )
#else
	if( 1 )
#endif
	{
		// TODO: Add check to verify direction of view is normalized?
		pViewer->SetView( renderpos, viewDirection );
		pViewer->SetCamMatrix( pCameraMatrix );

		eyepoint = renderpos; // use input param position, not freezing eyepoints
	}
	else
	{
		// frozen mode, use last submitted viewpoint instead, ignoring input param







		eyepoint = pViewer->GetViewPosition();
	}
	//-------------------------------------------

//	this->m_numActiveOccluders[ id ] = 0;// TODO: FIXME: make this optional???? support cached occlusion info??

	pViewer->m_pvsview_numActiveOccluders = 0;// clear occlusion buffer contents!
}

void	rmOccluderSystem::SetPvsView( const pvsViewerID id, const grcViewport* vp, const Matrix34 &camMtx )
{
	SetPvsView(&m_pvsViewers[id], vp, camMtx);
}

void	rmOccluderSystem::SetPvsView( PvsViewer* pViewer, const grcViewport* vp, const Matrix34 &camMtx )
{
	Assert(vp);

	// if we are freezing the pvs update, then we shouldn't be storing anything!
#if	__DEV
	if( !sm_FreezeAllOccluderViews )
#else
	if( 1 )
#endif
	{

	//  [3/14/2006 ccoffin] ---------------------------------------------------
	// Don't mess with the order of any of this code!
	Matrix34 cam34		= camMtx;				// copy!
	Matrix44 cam44;
	Convert( cam44, camMtx);// make a 4x4 version for the setcameramtx function in grcviewport!
	Matrix34 view34; // inverse of cam matrix which is a 3x4, we have to convert it to a 4x4 afterwards.
	if (!view34.Inverse( cam34 ))
		return;

	Matrix44 world44;
	world44.Identity();// everythings in worldspace already (for now) so this is identity.

	Matrix44 projection	= MAT44V_TO_MATRIX44(vp->GetProjection());
	Matrix44 screen		= MAT44V_TO_MATRIX44(vp->GetScreenMtx());	// GetScreenMtx replaces GetScreen??		

	Matrix44 view44;
	Convert( view44, view34 );// create 4x4 version of view

	//-------------------------------------------------------------------------
	// now populate member data of the viewer object.

#if !__SPU
	pViewer->m_perspVP_pvs.Perspective( vp->GetFOVY(), vp->GetAspectRatio(), vp->GetNearClip(), vp->GetFarClip() );

	pViewer->m_orthoVP_pvs.Ortho( 0.f, (float)vp->GetWidth(), (float)vp->GetHeight(), 0.f,
					vp->GetNearClip(), vp->GetFarClip() );// use -1.f, 1.f for near/far???
	pViewer->m_orthoVP_pvs.SetCameraMtx(MATRIX44_TO_MAT44V(cam44), MATRIX44_TO_MAT44V(view44));
#endif

/*
pvsmt_World			
pvsmt_View			
pvsmt_Projection	
pvsmt_Screen		
pvsmt_ModelView		
pvsmt_Composite		
pvsmt_FullComposite	
*/



	pViewer->SetMatrix( pvsvt_PVS, pvsmt_World,			&world44	);
	pViewer->SetMatrix( pvsvt_PVS, pvsmt_View,			&view44		);
	pViewer->SetMatrix( pvsvt_PVS, pvsmt_Projection,	&projection );
	pViewer->SetMatrix( pvsvt_PVS, pvsmt_Screen,		&screen		);

//	pViewer->m_world44_pvs		= world44;
	//pViewer->m_view44_pvs		= view44;
	//pViewer->m_projection44_pvs	= projection;
	//pViewer->m_screen44_pvs		= screen;

	//-------------------------------------------------------------------------
	// RegenerateCompositeMatrices....
	Matrix44 modelview44;
	modelview44.Dot( view44, world44 );
	pViewer->SetMatrix( pvsvt_PVS, pvsmt_ModelView,		&modelview44		);

	//pViewer->m_modelview44_pvs.Dot( pViewer->m_view44_pvs, pViewer->m_world44_pvs );

	Matrix44 composite44;
	composite44.Dot( projection, modelview44 );
	pViewer->SetMatrix( pvsvt_PVS, pvsmt_Composite,		&composite44		);

	//pViewer->m_composite44_pvs.Dot( pViewer->m_projection44_pvs, pViewer->m_modelview44_pvs );

	Matrix44 fullcomposite44;
	fullcomposite44.Dot( screen, composite44 );
	pViewer->SetMatrix( pvsvt_PVS, pvsmt_FullComposite,		&fullcomposite44	);

	//pViewer->m_fullcomposite44_pvs.Dot( pViewer->m_screen44_pvs, pViewer->m_composite44_pvs );


	//-------------------------------------------------------------------------
	// enum stolen from grcviewport that was private.
	enum { 
		clipEdgeLeft,		// Left edge of view frustum
		clipEdgeRight,		// Right edge of view frustum
		clipEdgeBottom,		// Bottom edge of view frustum
		clipEdgeTop,		// Top edge of view frustum
		clipEdgeNumEdges	// Number of clip plane equations
	};

	// Isn't this just wonderful, grcViewport doesn't expose this data and format it the way I want it so we'll just duplicate the code!
	//
	// Get pointers to fast culling matrices stored within each Pvsviewer object.
	Matrix44 *pFastCullMtx_0	= pViewer->GetCullMtx0();// m_OccludeSystem->GetViewer( viewportIndex ).GetCullMtx0();
	Matrix44 *pFastCullMtx_1	= pViewer->GetCullMtx1();// m_OccludeSystem->GetViewer( viewportIndex ).GetCullMtx1();
	Matrix44 *viewMtx			= &view44;

	// store all these intermediate variables in the pvsViewer object also???
	float ClipEdgeEqs[clipEdgeNumEdges][2];
	float aspect = vp->GetAspectRatio(); // m_Aspect? m_Aspect : m_AspectRatio;

	// turn in to radians, and get half angle
	float fovy = vp->GetFOVY() /* m_FovY */ * 0.5f * (3.14159265358979323846264338327950288f/180.0f);
	float tanFovy = tanf(fovy);
	float ZClipNear = vp->GetNearClip();
	float ZClipFar  = vp->GetFarClip();

	float cosVFOV=cosf(fovy);
	float sinVFOV=sinf(fovy);
	float hFOV=2.0f*atanf(tanFovy * aspect);
	float cosHFOV=cosf(0.5f*hFOV);
	float sinHFOV=sinf(0.5f*hFOV);

		pViewer->Set_AspectRatio(aspect);
		pViewer->Set_fovy(fovy);
		pViewer->Set_tanFovy(tanFovy);
		pViewer->Set_ZClipNear(ZClipNear);
		pViewer->Set_ZClipFar(ZClipFar);
		pViewer->Set_cosVFOV(cosVFOV);
		pViewer->Set_cosHFOV(cosHFOV);
		pViewer->Set_sinHFOV(sinHFOV);
		pViewer->Set_sinVFOV(sinVFOV);

	float magHorz=1.0f/sqrtf(cosHFOV*cosHFOV+sinHFOV*sinHFOV);
	ClipEdgeEqs[clipEdgeLeft][0]=-cosHFOV*magHorz;
	ClipEdgeEqs[clipEdgeLeft][1]=-sinHFOV*magHorz;
	ClipEdgeEqs[clipEdgeRight][0]=cosHFOV*magHorz;
	ClipEdgeEqs[clipEdgeRight][1]=-sinHFOV*magHorz;

	float magVert=1.0f/sqrtf(cosVFOV*cosVFOV+sinVFOV*sinVFOV);
	ClipEdgeEqs[clipEdgeBottom][0]=-cosVFOV*magVert;
	ClipEdgeEqs[clipEdgeBottom][1]=-sinVFOV*magVert;
	ClipEdgeEqs[clipEdgeTop][0]=cosVFOV*magVert;
	ClipEdgeEqs[clipEdgeTop][1]=-sinVFOV*magVert;

		#if 1//RH_PROJECTION		// remove z negation for GCube
		#define NEG(x) -(x)
		#else
		#define NEG(x) (x)
		#endif

	Matrix44 temp;
	temp.a.Set(ClipEdgeEqs[clipEdgeLeft][0],ClipEdgeEqs[clipEdgeRight][0],0,0);
	temp.b.Set(0,0,0,0 );
	temp.c.Set(NEG(ClipEdgeEqs[clipEdgeLeft][1]),NEG(ClipEdgeEqs[clipEdgeRight][1]),NEG(-1),NEG(1));
	temp.d.Set(0,0,ZClipNear,0); // hmm, we could do a mtx43 * mtx34... at least on the PS2
	pFastCullMtx_0->Dot(temp,*viewMtx);

	temp.a.Set(0,0, 0,0);
	temp.b.Set(ClipEdgeEqs[clipEdgeBottom][0],ClipEdgeEqs[clipEdgeTop][0], 0,0);
	temp.c.Set(NEG(ClipEdgeEqs[clipEdgeBottom][1]),NEG(ClipEdgeEqs[clipEdgeTop][1]),NEG(1),NEG(0));
	temp.d.Set(0,0,-ZClipFar,0);
	pFastCullMtx_1->Dot(temp,*viewMtx);

	}// if not using frozen view mode.
}

//  [3/28/2006]
void			rmOccluderSystem::EnableDrawAllOccluders		(const pvsViewerID	DEV_ONLY(id), const bool DEV_ONLY(mode))
{
#if __DEV
	PvsViewer	*pviewer = &m_pvsViewers[ id ];
	if( pviewer == NULL) return;

	pviewer->m_DrawAllOccluders = mode;
#endif // __DEV
}

#if 1

bool rmOccluderSystem::OccludeOccluders( const pvsViewerID viewerID )
{
#if __DEV
	// fixme: change testing method here for early exit.
	if( ! AreOccludersEnabled() )
		return false;

	if( !sm_UseOccluders )
		return false;
#endif

	PvsViewer	*pviewer = &m_pvsViewers[ viewerID ];
	if( pviewer == NULL) return false;

	// Get pointer to original polyheadron, then occlusion test it against the active occluder list, avoid testing the occlude testing against itself. 
	int numActiveOccluders = pviewer->GetNumActiveOccluders();

	for(int ao=0; ao < numActiveOccluders; ao++ )
	{
		// Extract the source polyhedron ptr from each active occluder.
		mcActiveOccluderPoly	*pActiveEntry = pviewer->GetActiveOccluderIndexPtr(ao);

		if( pActiveEntry == NULL )
			continue;

		if( ! pActiveEntry->IsActive() )
			continue; // Skip disabled active occluders

		rmOccludePolyhedron *pActiveOccluderHedra = (rmOccludePolyhedron*)(pActiveEntry->m_Id);

		rmPvsCullStatusBits status;

		//Vector4	tSphere;
		//pHedra->GetBoundSphere( tSphere );
		//this->TestSphereVisibility( viewerIDviewerID, tSphere, )
		// NOTE: cant do a sphere test unless we add an exempt hedra mask to the cull function

		// IDEA: Another way to do this without masking would be to lookup an active occluder planeset by polhedron ptr and temporarily disable it, call a standard occlusion test on it
		//       then restore the active state (if it was active)
		//

		status = this->TestVertexPoolVisibility( viewerID,
			pActiveOccluderHedra->m_pVerts,
			pActiveOccluderHedra->GetNumVerts(),
			(rmPvsCullModeBits)(pvscm_ForceOccludeTest | pvscm_OccludeTest),
			pActiveOccluderHedra // polyhedron to avoid testing against to prevent self occlusion and subsequent deactivation of its active occluder plane-set.
			);

		if(! rmOcclude::IsVisible( status ) )
		{
			pActiveEntry->SetActiveMode( false );; // disable!
		}
	}

	return true;
}
#endif

#if 0

bool rmOccluderSystem::OccludeOccluders( const pvsViewerID viewerID)
{
#if __DEV
	// fixme: change testing method here for early exit.
	if( ! AreOccludersEnabled() )
		return false;

	if( !sm_UseOccluders )
		return false;
#endif

	Assert( pHedra );

	PvsViewer	*pviewer = &m_pvsViewers[ viewerID ];
	if( pviewer == NULL) return false

	// , rmOccludePolyhedron *pHedra

	// Get pointer to original polyheadron, then occlusion test it against the active occluder list, avoid testing the occlude testing against itself. 
	int numActiveOccluers = this->
	rmOccludePolyhedron *pHedra

}
#endif

#if !__SPU
static const Vector3 vOneHundred(100.0f,100.0f,100.0f);
#endif

bool rmOccluderSystem::AddOccluderToPvsView( const pvsViewerID viewerID, rmOccludePolyhedron *pHedra, const bool bIsExternal  )
{
#if __DEV
	// fixme: change testing method here for early exit.
	if( ! AreOccludersEnabled() )
		return false;

	if( !sm_UseOccluders )
		return false;
#endif

	return AddOccluderToPvsView(&m_pvsViewers[viewerID], pHedra, bIsExternal);
}

//  [3/14/2006 ccoffin]
bool rmOccluderSystem::AddOccluderToPvsView( PvsViewer* pviewer, rmOccludePolyhedron *pHedra, const bool bIsExternal  )
{
	Assert( pHedra );

	if( ! pHedra->IsActive() )
		return false;// support activating/deactivating occluders - trivial accept/reject test. // ccoffin [8/24/2006]
	
	if( pviewer == NULL) return false;

	// If we don't have a free slot to store 
	mcActiveOccluderPoly* RESTRICT pFreeEntry = pviewer->GetActiveOccluderStoragePtr();
	if( pFreeEntry == NULL ) return false;
	
	// Handling occluder geometry
	pvsVtxClipFlags		t_VertexClipResults[64];
	DEV_ONLY(pvsFaceClipFlags	t_FaceClipResults[64]);

	{
		int	planesAdded = 0;// Clear count. As we add planes we increase the count - not all planes created by edges may need to be added (see below)

#if !HACK_GTA4 // gta does not require the distance check
		//----------------------------------------
		// Distance based activation from eyepoint
		float rejectDist = pHedra->GetActiveDist();

		if( rejectDist <= 1.f )// if the exported occluder file has a active dist of 0 then we use the 'default'
		{
			rejectDist = RM_OCCLUDE_DEFAULT_ACTIVE_DIST;// use default active distance.
		}

		rejectDist *= sm_OccluderDistanceActivationScale;
#endif // !HACK_GTA4
		// TODO: test #1: Frustum test sphere bound the encloses the polyhedron as quick test.

		// TODO: test #1A: proximity check of bound sphere data.
		//
		// Sphere bounds are bloated as hell in 3 dimensions since they extend vertically so much

		// NOTE: remember to xform bound if the occluder is animated (has local->world xform)

		//----------------------------------------
		// test #2: test if all vertices are outside a single plane of the frustum (simple test)
		if( pHedra->TestOutsideSinglePlane( &t_VertexClipResults[0], pviewer ) )
		{
#if _OCCLUDER_DEBUGGING
			Displayf(" geom outside frustum " );
#endif
			// TODO: add more clarification to culling results if its outside, needed if we support coherence.

			return false;// totally outside frustum on a side, skip processing this one.
		}

#if !HACK_GTA4 // gta does not require the distance check
		//----------------------------------------
		// Distance from eyepoint to center of polyhedron in 2-D
		Vector4	tmpSphere;
		pHedra->GetBoundSphere( &tmpSphere );
		Vector3 tmpCenter(  tmpSphere.x,
							eyepoint.y,// to make the distance check 2d essentially.
							tmpSphere.z );

		float culldist = eyepoint.Dist( tmpCenter );
		// distance to center, not counting the boundsphere for now.
		if( culldist > rejectDist ) return false;
		//----------------------------------------
#endif // !HACK_GTA4

		CompileTimeAssert(__alignof(mcActiveOccluderPoly) == 128);
		CompileTimeAssert(sizeof(mcActiveOccluderPoly) >= 256);
		// zero out cache lines to prevent a fetch from memory
		ZeroDC(pFreeEntry, 0);
		ZeroDC(pFreeEntry, 128);

		//----------------------------------------
		// RESET FREE ENTRY, moved this down so trivial reject cases above do not needlessly keep touching this part of memory.
		Assert( pHedra->IsActive() == true );// if its not active, we shouldnt be trying to add it.
		pFreeEntry->SetActiveMode( true );// inherit from the polyhedra whether its active or not.
		//done with ZeroDC - pFreeEntry->m_num_planes	= 0;
		//done with ZeroDC - pFreeEntry->m_pOccPoly		= NULL;
		//----------------------------------------

		// test #3: Figure out which faces on the geom face us.
		//			 To detect whether a polygon faces the viewer, use the dot product of the polygon's normal and direction to any of the polygon's vertices.
		//			 When this is less than 0, the polygon faces the viewer.)

		//Vector4 eyeToPoly;// define vector from eye position to 1st vertex on polygon in worldspace 

		// get 'eyepoint' from pvsViewer
		Vector3	eyepoint = pviewer->GetViewPosition();
		bool	isOrthogonal=pviewer->GetIsOrthogonal();
		Vector3 viewdir = pviewer->GetViewDirection();
		Vector3 eyeToVert = viewdir;

		int faceCount = pHedra->GetNumFacets();
		int fc;

		pvsEdgeData edgeFlags		= 0U;// clear - this tracks unclipped edges which form the convex silhouette of the convex hull, based on visible face tests.
		pvsEdgeData edgeReversed	= 0U;
#if _OCCLUDER_DEBUGGING
		Displayf(" geom %d faces %d ", g, faceCount );
#endif
		for( fc=0 ; fc < faceCount ; fc++ )
		{
			Vector4 facetPlane;
			pHedra->GetPlane(fc, &facetPlane);
			Vector4 dotP;
			int		vertIdx;
#if _OCCLUDER_DEBUGGING
			int		facingpolys = 0;
#endif

			// hackery to remove faces easily
#if _OCCLUDER_IGNOREFACE_EDGEKILL
			if(0)// pHedra->GetFacetIgnoredFlag( fc ) )
			{
				dotP = -1.f;// force 'facing away' result.
			}
			else
#endif
			{
				// loop through faces.			
				vertIdx = pHedra->GetFacetVertIndex( fc, 2 );					// get 1st vertex in face.
				Vector3 ptOnPoly = pHedra->GetVertex( vertIdx );
#if _OCCLUDER_DEBUGGING
				Displayf(" face %d pt on poly %f %f %f", fc, ptOnPoly.x, ptOnPoly.y, ptOnPoly.z );
#endif
				if(!isOrthogonal)
				{
					eyeToVert.Subtract( eyepoint, ptOnPoly );	// create vector from point on poly towards the eyepoint			
				}

#if _OCCLUDER_DEBUGGING
				//eyeToVert.Normalize();
				Displayf(" polyNormal %f %f %f ", polyNormal.x, polyNormal.y, polyNormal.z );
#endif

#if		__DEV
				// IMPORTANT: Thankfully all we care about is the sign of the result so nothing has to be normalized.
				//	eyeToVert.Normalize();// dont need to do this - left for reference.
#endif
				dotP = facetPlane.Dot3V(eyeToVert);
			}

			if (dotP.IsLessThanDoNotUse(_vzerofp))
			{
				// facing away, we dont want this face.
				DEV_ONLY(t_FaceClipResults[fc] = 0);

			}else{
				// facing us.
				DEV_ONLY(t_FaceClipResults[fc] = 1);
#if _OCCLUDER_DEBUGGING
				facingpolys++;
#endif

				// Get edges for this face which we can see, and do a bit toggle.
				// this results in edges that get toggled twice will revert to OFF (not a silhouette edge)

				// test #4: figure out which edges define the silhouette based on face visibility (based on face orientation and if all the vertices clip flags are off on the same side)
#if !HACK_GTA4
// see below
				if(1)
				{
					Vector4 facetPlane;

					pHedra->GetPlane( fc, &facetPlane );
					InsertPlaneAsActiveOccluder( planesAdded, pFreeEntry, facetPlane );// Add this face as an occluder plane.
#if _OCCLUDER_DEBUGGING
					Displayf(" facetPlane %d of geom added as plane #%d in active occluder %d ", fc, planesAdded, activeOccluders );
#endif
					planesAdded++;// increment count of planes added.
				}
#endif // !HACK_GTA4


				int num_face_edges = pHedra->GetFacetNumVerts( fc );
#if HACK_GTA4
				u32 off_one_plane = pvscull_frust_plane_outside_mask;
#endif // HACK_GTA4

				for(int fe=0; fe < num_face_edges; fe++ )
				{	
					int facetEdgeIdx = pHedra->GetFacetEdgeIndex( fc, fe );
#if HACK_GTA4
					int e0_vertIdx  = pHedra->GetEdgeVertexIndex( facetEdgeIdx, 0);//pHedra->m_pEdges[fe].GetEdge(0);
					int e1_vertIdx  = pHedra->GetEdgeVertexIndex( facetEdgeIdx, 1);//pHedra->m_pEdges[fe].GetEdge(0);
					off_one_plane&=t_VertexClipResults[e0_vertIdx];
					off_one_plane&=t_VertexClipResults[e1_vertIdx];
#endif // HACK_GTA4

					edgeFlags ^= (1 << facetEdgeIdx);// edge toggle

					// if edge is in 'set' state, update the reversed edge info.
					if( edgeFlags & (1 << facetEdgeIdx) )
					{
						// bit was set, not cleared, so record reverse flag setting.

						int flag = pHedra->GetFacetEdgeIndexReverseFlag( fc, fe );

						if( flag != 0 )
						{
							// flipped
							// set bit
							edgeReversed |= (1<<facetEdgeIdx);

						}else{
							// clear bit
							edgeReversed &= ~(1<<(facetEdgeIdx & ~0U)); 
						}
					}
				}
#if HACK_GTA4
				if(off_one_plane==0) //face is on screen
				{
					InsertPlaneAsActiveOccluder( planesAdded, pFreeEntry, facetPlane );// Add this face as an occluder plane.
#if _OCCLUDER_DEBUGGING
					Displayf(" facetPlane %d of geom added as plane #%d in active occluder %d ", fc, planesAdded, activeOccluders );
#endif
					planesAdded++;// increment count of planes added.
				}
#endif // HACK_GTA4
			}// end facing poly processing

#if _OCCLUDER_DEBUGGING
			Displayf(" polys facing us %d ", facingpolys );
#endif
		}// end face loop

#if _OCCLUDER_IGNOREFACE_EDGEKILL
		//---------------------------------------------------------------------------
		// edge killer (ignored faces only)
		{			
			faceCount = pHedra->GetNumFacets();

			for( fc=0 ; fc < faceCount ; fc++ )
			{
				int num_face_edges = pHedra->GetFacetNumVerts( fc );

				if( pHedra->GetFacetIgnoredFlag( fc ) )
				{
					Displayf("ingore face %d", fc );
					for(int fe=0; fe < num_face_edges; fe++ )
					{
						int facetEdgeIdx = pHedra->GetFacetEdgeIndex( fc, fe );
						// should this be clearing the bit instead of toggling it????
						//edgeFlags ^= (1 << facetEdgeIdx);// edge toggle												
						if( (edgeFlags & (1<<fe)) )
						{
							edgeFlags &= ~(1<<(facetEdgeIdx & ~0U)); // nuke edge
							Displayf("killed face %d edge %d", fc, facetEdgeIdx );
						}
					}
				}
			}
		}
		//---------------------------------------------------------------------------
#endif



#if _OCCLUDER_DEBUGGING
		for(int fe=0; fe < pHedra->GetNumEdges(); fe++ )
		{
			Displayf("edge %d   %d", fe,  (edgeFlags&(1<<fe)) );
		}
#endif

		// done looping through all faces now, edgeFlags holds a bitmask of which edges define the convex silhouette

		// test #4: add planes of visible faces

#if __SPU
		viewdir *= (__vector4)(100.0f,100.0f,100.0f,100.0f);
#else
		viewdir *= vOneHundred;		
#endif

		// test #6: remove remaining silhouette edges that are outside the frustum
		int UnClippedEdges		= 0;
		// int removedSilhouetteEdges	= 0;
		int num_face_edges			= pHedra->GetNumEdges();
		//int totalGeomEdges			= 0;
		// loop through all unique edges of geom
		for(int fe=0; fe < num_face_edges; fe++ )
		{
			if( (edgeFlags & (1<<fe)) )// bit set for this edge? then its a silhouette edge, otherwise its an interior edge.
			{
				// get edge vertices, then test if those vertices clip flags match each other (off the same side)
				int e0_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 0);//pHedra->m_pEdges[fe].GetEdge(0);
				int e1_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 1);// m_pEdges[fe].GetEdge(1);

				// If the union of the 2 edge vertices clip flags have any set bit left based on our mask test,
				//  then the edge is completely off the side of one of the clipping planes
#if !HACK_GTA4
// According to MikeD, this is bugged
// "causes fawlty occlusion, needs fixing to speed it up"
				if( pvscull_frust_plane_outside_mask & ((t_VertexClipResults[ e0_vertIdx ] & t_VertexClipResults[ e1_vertIdx ])))
				{
					//Displayf(" removed edge ");
					// clipped! dont add					
					// removedSilhouetteEdges++;					
					continue;
				}
#endif // !HACK_GTA4

#if _OCCLUDER_DEBUGGING
				Displayf(" added edge ");
#endif

				Vector3 v3edge0;
				Vector3 v3edge1;

				if( (edgeReversed & (1<<fe)) )// was the edge inserted as reversed?
				{
#if _OCCLUDER_DEBUGGING
					Displayf(" rev %d %d ", e1_vertIdx, e0_vertIdx );
#endif
					// fallthough: if we get to here, the edge is potentially visible so we need to add it.
					v3edge0 = pHedra->GetVertex( e1_vertIdx );
					v3edge1 = pHedra->GetVertex( e0_vertIdx );
				}else{
#if _OCCLUDER_DEBUGGING
					Displayf(" nrm %d %d ", e0_vertIdx, e1_vertIdx );					
#endif
					// fallthough: if we get to here, the edge is potentially visible so we need to add it.
					v3edge0 = pHedra->GetVertex( e0_vertIdx );
					v3edge1 = pHedra->GetVertex( e1_vertIdx );
				}
#if _OCCLUDER_DEBUGGING	
				Displayf("e0 %f %f %f ", v3edge0.x, v3edge0.y, v3edge0.z );
				Displayf("e1 %f %f %f ", v3edge1.x, v3edge1.y, v3edge1.z );
#endif


				// NOTE: If the exporter/loader code lets an occluder in that could overrun the maximum allowed active planes, set the plane added count to 0 and break out.
				// The code below should all fall through, causing this occluder to not be added to the scene - CMC
				if( planesAdded == RM_OCCLUDE_MAX_OCCLUDER_POLYEDGES )
				{
					planesAdded = 0;
					Displayf("Occluder has too many active planes ptr %p 1st vertex ( %f, %f, %f )",
						pHedra,
						pHedra->GetVertex(0).x,
						pHedra->GetVertex(0).y,
						pHedra->GetVertex(0).z
						);
					break;// abort loop! - 
				}

				// These are planes created by extruding the
				//  edges of the convex silhouette using the eyepoint as the 3rd point to define the plane in worldspace.
				//
				// TODO: I think we can+should store the silhouette data for use with the sbuffer and maybe we can get away with re-transforming it
				//			or exploit frame coherence in such a way that we can re-use it 'n' frames in a row?
				Vector3 thirdPoint3=eyepoint;
				if(isOrthogonal)
					thirdPoint3=v3edge0+viewdir;

				CreateEdgePlaneToActiveOccluder( planesAdded, pFreeEntry, thirdPoint3,
					v3edge0, 
					v3edge1  );// winding order swapped....?

				planesAdded++;// increment count of planes added.
				UnClippedEdges++;// edge tracker.
			}
		}
#if _OCCLUDER_DEBUGGING
		Displayf(" UnClippedEdges %d ", UnClippedEdges );
#endif

		//
		// test #?: TODO: test if eyepoint is contained within polyhedron, and spam a warning - also dont use that volume for occlusion!
		//

		if( planesAdded > 0 )
		{
			// make sure we added at least 1 plane.
#if !HACK_GTA4
 			if( !m_UseSpanBuffering )
 			{
 				pFreeEntry->num_silh_verts			= 0;
 			}
#endif // HACK_GTA4

			pFreeEntry->m_num_planes	= planesAdded;
			pFreeEntry->m_pOccPoly		= NULL;// different type data

			if( bIsExternal )
			{
				pFreeEntry->m_type		= RM_OCCLUDE_OCCLUDERTYPE_GEOM_EXTERNAL;
			}
			else
			{
				pFreeEntry->m_type		= RM_OCCLUDE_OCCLUDERTYPE_GEOM;
			}

			pFreeEntry->m_Id			= (ptrdiff_t)pHedra;// store ptr instead of ID since its from an external source.

#if __DEV
			pFreeEntry->StoreDebugDrawInfo( sm_DebugDrawOptions, &t_VertexClipResults[0], &t_FaceClipResults[0], &edgeFlags , &edgeReversed );
#endif


			// NOTE: Increase active occluder count here, its possible that all the edges were clipped out,
			//			but we still have a valid occluder poly covering the entire viewport that we want to test against still.
#if _OCCLUDER_DEBUGGING	
			Displayf(" planesAdded %d ", planesAdded );
			for(int t_plane=0; t_plane < planesAdded; t_plane++)
			{
				Vector4 *pPlane = &pviewer->m_occlusionPlanes[ t_plane ];
				Displayf(" plane %d  planeEq %f %f %f %f ", t_plane, pPlane->x, pPlane->y, pPlane->z, pPlane->w );
			}
#endif

		//	activeOccluders++;// track active count
		//	m_numActiveOccluders[ viewerID ] = activeOccluders;// update number of occluders

			pviewer->IncrementActiveOccluderCount();


			return true;// we added an occluder
		}
	}
	//-----------------------------------------------------


	return false;// nothing added. clipped away completely, out of view or something.
}






bool	rmOccluderSystem::AddOccluderToActiveView( const pvsViewerID /*id*/,  rmOccludePolyhedron * /*pHedra*/, const bool /* bIsExternal */)
{
	return true;
}












// do we really need this??? i forgot what i used it for...  [3/14/2006 ccoffin]
void	rmOccluderSystem::ViewSetupFinish( const pvsViewerID DEV_ONLY(id), const Matrix34 * DEV_ONLY(pCameraMatrix) )
{
#if __DEV
	PvsViewer *pviewer = GetPvsViewerPtr( id );

	// all done getting our list of useful occluders for this frame, if the occluder camera is in frozen mode, update the render matrix data.
	if( sm_FreezeAllOccluderViews )
	{
		pviewer->SetCamMatrix( pCameraMatrix );
	}
#endif
}

void	rmOccluderSystem::ProcessStaticOccluders( const pvsViewerID id  )
{
#if __DEV
	if( ! AreOccludersEnabled() )
		return;

	if( !sm_UseOccluders )
		return;
#endif

	PvsViewer *pviewer = GetPvsViewerPtr( id );

	// Loop through all occluder geometry we own and add them
	int geomcount = m_numOccluderGeoms;

	for(int g=0; g < geomcount; g++)
	{
		rmOccludePolyhedron *pHedra = &m_OccluderGeomArray[g];
		Assert( pHedra );

		// Early out if we hit the limit.
		if( pviewer->GetNumActiveOccluders() >= RM_OCCLUDE_MAX_ACTIVE_OCCLUDERS )
			break;// 	return;

		if( ! pHedra->IsActive() )
			continue;// support activating/deactivating occluders - trivial accept/reject test.

		// okay, this isn't occluded by existing occluders, add it.
		AddOccluderToPvsView( id, pHedra, false );
	}


	// 2nd pass processing goes here.
	if( sm_bOccludeActiveOccluders )
	{
		OccludeOccluders( id );// make this toggle-able
	}
#if 0
	// TODO: we need to change the active occluder list to a linked list to make addition/pruning of so called 'active occluders' is cleaner and easier
	//			once the base functionality is all working neatly.
	int numActiveOccluders = pviewer->GetNumActiveOccluders();

	for(int ao=0; ao < numActiveOccluders )
	{
		// If we don't have a free slot to store 
		mcActiveOccluderPoly	*pFreeEntry = pviewer->GetActiveOccluderIndexPtr(ao);
		if( pFreeEntry == NULL ) return false;

		if( ! pFreeEntry->IsActive() )
			continue; // Skip disabled active occluders

		if( pHedra == (rmOccludePolyhedron*)(pFreeEntry->m_Id) )
			continue; // Don't self occlude!

		// test bounding sphere of the occluder geometry against
		//Vector3				m_boundSphereCtr;
		//float				m_boundSphereRadius;
		Vector4	tSphere;
		pHedra->GetBoundSphere( tSphere );

		// TODO: make sure these bound checks work when using dynamic occluders.
		AddOccluderToPvsView( id, pHedra, false );
	}
#endif

}



#if 0
void	rmOccluderSystem::ProcessStaticOccluders( const Vector3 & /* xxxpos */, const Vector3 &/*viewDirection*/, const int viewerID, const Matrix34 * /*pCameraMatrix*/ )
{
#if __DEV
	if( ! AreOccludersEnabled() )
		return;

	if( !m_UseOccluders )
		return;
#endif

	Assert( (viewerID >= 0) && (viewerID < RM_OCCLUDE_MAX_VIEWS) );

	int activeOccluders = m_numActiveOccluders[ viewerID ];// find free slot # to store occluder planes if its valid.

	// NOTE: THIS ISNT A PROPER ASSERT, VEHICLE OCCLUDER GEOMS CAN ADD TO THE ACTIVE COUNT FOR A VIEWPORT BECAUSE WE PROCESS THEM FIRST SO THEY GET ADDED ALWAYS!
//	Assert( (activeOccluders == 0) && "calling ProcessStaticOccluders before ModifyViewer, active occluder count is nonzero");

	if( activeOccluders >= RM_OCCLUDE_MAX_ACTIVE_OCCLUDERS )
	{
	//	Displayf(" ran out of room to add more active occluders - currently %d active for viewID %d ",activeOccluders, viewerID );
		return;// hit limit, bail
	}
	// push active view ID
	const int pushedActiveViewID = this->GetActiveViewID();

	// set current
	this->SetActiveViewID( viewerID );


	// Loop through all occluder geometry we own and add them
	int geomcount = m_numOccluderGeoms;

	for(int g=0; g < geomcount; g++)
	{
		rmOccludePolyhedron *pHedra = &m_OccluderGeomArray[g];
		Assert( pHedra );

		AddOccluderToActiveView( pHedra, false );

		// Early out if we hit the limit.
		if( m_numActiveOccluders[ viewerID ] >= RM_OCCLUDE_MAX_ACTIVE_OCCLUDERS )
			break;// 	return;
	}	

#if	__DEV
//	Displayf("viewerID = %d    pushedID = %d    m_numActiveOccluders[ viewerID ] = %d", viewerID, pushedActiveViewID, m_numActiveOccluders[ viewerID ] );
#endif

	// pop active ID
	this->SetActiveViewID( pushedActiveViewID );
}

#endif


/*
bool	rmOccluderSystem::IsHotDogOccluded( const Vector3 &extentA, const Vector3 &extentB, const float radius )
{
#if __DEV
	if( ! AreOccludersEnabled() )
		return false;
#endif

	s32 total_active = m_numActiveOccluders[ GetActiveViewID() ];

	if( !total_active )
		return false;// none active!

	int viewID = GetActiveViewID();


	for( int idx=0; idx < total_active; idx++ )
	{
		bool bFail;

		if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly )// handle geom occluders (null ptr case) safely
		{
			// Test occluder poly first - halfspace checks
			if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly->GetPlane().DistanceToPlane( extentA ) > -radius )
			{
				continue;// outside or crossing occlusion frustum, move to next active.
			}

			if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly->GetPlane().DistanceToPlane( extentB ) > -radius )
			{
				continue;// outside or crossing occlusion frustum, move to next active.
			}
		}

		int numplanes = m_activeOccluders[ viewID ][ idx ].m_num_planes;

		bFail = false;

		// consider occluder, plane test against frusta
		for( int plane=0; plane < numplanes; plane++)
		{			
			if( m_activeOccluders[ viewID ][ idx ].m_occlusionPlanes[ plane ].DistanceToPlane( extentA ) > -radius )
			{
				bFail = true;// crossing, tag as failing test, break loop
				break;
			}			
		}

		// done testing, it either passed all tests and bFail stays false, or it was outside/crossing (true)
		if( !bFail )
		{
			// 1st sphere extent of hotdo is occluded completely by one of our occluders, test 2nd sphere.


			// consider occluder, plane test against frusta
			for( int plane=0; plane < numplanes; plane++)
			{			
				if( m_activeOccluders[ viewID ][ idx ].m_occlusionPlanes[ plane ].DistanceToPlane( extentB ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}
			}

			if( bFail )
			{
				// 2nd sphere was not completely occluded, continue to next loop.
				continue;
			}else{
				// Both 1st and 2nd sphere extents contained within the occluder, exit!

					#if __DEV
						m_OccludedCount++;// track

						if( m_AllowDebugDrawOfOccludedVolumes )
						{
							#if __PS2
								// Just buffer two spheres for now, retained rendering doesnt support different occluded bound types...
								if( m_OccludedDrawBoundTotal < m_maxDebugOccluders-1 )// minus 1 because we store 2 spheres.
								{
									if( OccludedVolumesBuffered_GetMode() )// bound buffering on for this test?
									{
										a_bndInfo[ m_OccludedDrawBoundTotal ].center = extentA;
										a_bndInfo[ m_OccludedDrawBoundTotal ].radius = radius;
										a_bndInfo[ m_OccludedDrawBoundTotal ].debugDrawColor = GetOccludedVolumeDebugColor();
										a_bndInfo[ m_OccludedDrawBoundTotal ].occludedViewID = viewID;
										m_OccludedDrawBoundTotal++;

										a_bndInfo[ m_OccludedDrawBoundTotal ].center = extentB;
										a_bndInfo[ m_OccludedDrawBoundTotal ].radius = radius;
										a_bndInfo[ m_OccludedDrawBoundTotal ].debugDrawColor = GetOccludedVolumeDebugColor();
										a_bndInfo[ m_OccludedDrawBoundTotal ].occludedViewID = viewID;
										m_OccludedDrawBoundTotal++;
									}
								}
							#else
							Displayf("FIX DEBUG DRAWING OF OCCLUDED BOUNDS");
#if 0
								bool oldZWrite = RSTATE.GetZWriteEnable();
								bool oldZtest = RSTATE.GetZTestEnable();
								bool oldLightingEn = RSTATE.GetLighting();
								bool oldAlphaBlendEnable = RSTATE.GetAlphaBlendEnable();

								RSTATE.SetLighting( false );
								RSTATE.SetZTestEnable( false );
								RSTATE.SetZWriteEnable( true );
								RSTATE.SetAlphaBlendEnable( false );

								vglColor4f( 1.0f, 0.0f, 1.0f, 1.f );
								vglDrawSphere( radius, extentA , 5, true );
								vglDrawSphere( radius, extentB , 5, true );

								// connecting segment between spheres until i draw a full style hotdog...
								vglBegin(drawLine,2);
								vglVertex3f( extentA.x, extentA.y, extentA.z );
								vglVertex3f( extentB.x, extentB.y, extentB.z );
								vglEnd();
								
								RSTATE.SetZWriteEnable(oldZWrite);
								RSTATE.SetZTestEnable( oldZtest );// restore
								RSTATE.SetLighting( oldLightingEn );// restore
								RSTATE.SetAlphaBlendEnable( oldAlphaBlendEnable );// restore
#endif
							#endif
						}
					#endif

				return true;
			}
		}
	}// end looping all active occluders

	return false;// nothing occluded this object - yuck!
}



*/

#if HACK_GTA4 && (__PS3 || __XENON)

__forceinline bool rmOccludePolyhedron::TestOutsideSinglePlane( pvsVtxClipFlags *pClipFlags, PvsViewer *pViewer )
{
	Assert( pClipFlags );
	Assert( pViewer );

	vec_uint4 prevFlag = {pvscull_frust_plane_outside_mask,pvscull_frust_plane_outside_mask,
						  pvscull_frust_plane_outside_mask,pvscull_frust_plane_outside_mask};
	const vec_uint4 zero = vec_splat_u32(0);
	const vec_float4 zerofp = (vec_float4)zero;
	const vec_uchar16 pack = {16,0,4,8, 16,0,4,8, 16,0,4,8, 16,0,4,8};
	Matrix44 m0 = *pViewer->GetPvsCullMtx0();
	Matrix44 m1 = *pViewer->GetPvsCullMtx1();
	int numVerts = GetNumVerts();
	Assert( (numVerts & 1) == 0 );
	Vector3* RESTRICT pVerts = GetVertices();
	// test vertex pool against each plane. 

#if 1
	// two at a time
	int n = 0;
	Vector3 v0 = pVerts[n];
	Vector3 v1 = pVerts[n+1];
	// dot with frustum planes
	Vector3 vTf0, vTf1, vTf2, vTf3, vTf4, vTf5, vTf6, vTf7;
	m0.Transform(v0, vTf0);
	m1.Transform(v0, vTf1);
	m0.Transform(v1, vTf2);
	m1.Transform(v1, vTf3);
	while (n < numVerts - 2)
	{
		// two at a time
		n += 2;
		v0 = pVerts[n];
		v1 = pVerts[n+1];
		// dot with frustum planes
		m0.Transform(v0, vTf4);
		m1.Transform(v0, vTf5);
		m0.Transform(v1, vTf6);
		m1.Transform(v1, vTf7);
		// Perform bounds check against w=0
		vec_uint4 c0 = (vec_uint4)vec_cmpb((vec_float4)vTf0, zerofp);
		vec_uint4 c1 = (vec_uint4)vec_cmpb((vec_float4)vTf1, zerofp);
		vec_uint4 c2 = (vec_uint4)vec_cmpb((vec_float4)vTf2, zerofp);
		vec_uint4 c3 = (vec_uint4)vec_cmpb((vec_float4)vTf3, zerofp);
		// Pack result pairs together
		c0 = vec_or(vec_sr(c0, vec_splat_u32(2)), c1);
		c2 = vec_or(vec_sr(c2, vec_splat_u32(2)), c3);
		// Pack each result into 3 bytes
		vec_uint4 r0 = vec_perm(c0, zero, pack);
		vec_uint4 r1 = vec_perm(c2, zero, pack);
		prevFlag = vec_and(prevFlag, r0); 
		prevFlag = vec_and(prevFlag, r1);
		// Store clip flags into memory
		vec_ste(r0, n*4-8, pClipFlags);
		vec_ste(r1, n*4-4, pClipFlags);
		vTf0 = vTf4; vTf1 = vTf5; vTf2 = vTf6; vTf3 = vTf7;
	}
	// Perform bounds check against w=0
	vec_uint4 c0 = (vec_uint4)vec_cmpb((vec_float4)vTf0, zerofp);
	vec_uint4 c1 = (vec_uint4)vec_cmpb((vec_float4)vTf1, zerofp);
	vec_uint4 c2 = (vec_uint4)vec_cmpb((vec_float4)vTf2, zerofp);
	vec_uint4 c3 = (vec_uint4)vec_cmpb((vec_float4)vTf3, zerofp);
	// Pack result pairs together
	c0 = vec_or(vec_sr(c0, vec_splat_u32(2)), c1);
	c2 = vec_or(vec_sr(c2, vec_splat_u32(2)), c3);
	// Pack each result into 3 bytes
	vec_uint4 r0 = vec_perm(c0, zero, pack);
	vec_uint4 r1 = vec_perm(c2, zero, pack);
	prevFlag = vec_and(prevFlag, r0); 
	prevFlag = vec_and(prevFlag, r1);
	// Store clip flags into memory
	vec_ste(r0, n*4, pClipFlags);
	vec_ste(r1, n*4+4, pClipFlags);
#else
	for(int n=0; n < numVerts; n += 2)
	{
		// two at a time
		Vector3 v0 = pVerts[n];
		Vector3 v1 = pVerts[n+1];
		// dot with frustum planes
		Vector3 vTf0, vTf1, vTf2, vTf3;
		m0.Transform(v0, vTf0);
		m1.Transform(v0, vTf1);
		m0.Transform(v1, vTf2);
		m1.Transform(v1, vTf3);
		// Perform bounds check against w=0
		vec_uint4 c0 = (vec_uint4)vec_cmpb(vTf0, zerofp);
		vec_uint4 c1 = (vec_uint4)vec_cmpb(vTf1, zerofp);
		vec_uint4 c2 = (vec_uint4)vec_cmpb(vTf2, zerofp);
		vec_uint4 c3 = (vec_uint4)vec_cmpb(vTf3, zerofp);
		// Pack result pairs together
		c0 = vec_or(vec_sr(c0, vec_splat_u32(2)), c1);
		c2 = vec_or(vec_sr(c2, vec_splat_u32(2)), c3);
		// Pack each result into 3 bytes
		vec_uint4 r0 = vec_perm(c0, zero, pack);
		vec_uint4 r1 = vec_perm(c2, zero, pack);
		prevFlag = vec_and(prevFlag, r0); 
		prevFlag = vec_and(prevFlag, r1);
		// Store clip flags into memory
		vec_ste(r0, n*4, pClipFlags);
		vec_ste(r1, n*4+4, pClipFlags);
	}
#endif

	return vec_any_ne(prevFlag, zero);
}

#else

bool rmOccludePolyhedron::TestOutsideSinglePlane( pvsVtxClipFlags *pClipFlags, PvsViewer *pViewer )
{
	Assert( pClipFlags );
	Assert( pViewer );

#define	MAX_GEOM_TEST_VERTS 16

	Vector4			av4_src;
	pvsVtxClipFlags		clipResult = 0;
	pvsVtxClipFlags		prevFlag   = 0;// must clear!

	int n;
	int numVerts = GetNumVerts();
	Assert( numVerts <= MAX_GEOM_TEST_VERTS );

	// test vertex pool against each plane.
	for( n=0; n < numVerts; n++)
	{
#if HACK_GTA4
		// fixme: incredibly inefficient code here, doing a vector copy and setting w to 0....
		av4_src = GetVertex4( n );	// copy vertex to source buffer.
		clipResult = pViewer->PointFrustumCheck_ClipStatus( av4_src);
#else
		av4_src.SetVector3ClearW(GetVertex(n)); // Need a Vector4 with w=0.0f

		clipResult = pViewer->SphereFrustumCheck_ClipStatus( av4_src );// rmOccluderSystem::sm_gCurrOccluderSysInstanceActive->InlineFastSphereVisCheck_ClipStatus_A( av4_src );
#endif // HACK_GTA4

		if( clipResult & pvscull_frust_plane_outside_mask )
		{
			pClipFlags[n]	= clipResult;
			prevFlag		= clipResult;
		}else{
			pClipFlags[n]	= clipResult;
		}
	}

	// loop through all clip flags, AND'ing them together.
	for( n=0; n < numVerts; n++)
	{
		prevFlag &= pClipFlags[n];
	}

	if( prevFlag & pvscull_frust_plane_outside_mask )
	{
		return true;// one of the outside plane flags was set for all the vertices tested, so return TRUE (outside a single plane)

		// clipStatus:  -N +N -R +R -L +L -F +F -T +T -B +B
		//  - means dist is < -radius,
		//  + means dist is > radius

		//	if      (clipStatus & 0x555)            return cullOutside;      // far enough outside at least one plane to be rejected
		//	else if ((clipStatus & 0xAAA) == 0xAAA) return cullInside;       // we are far enough inside all planes
		//	else if (!(clipStatus & 0x800))         return cullClippedZNear; // clipped the near plane (important for some things)
		//	else                                    return cullClipped;      // clipped one of the sides or far,  really "if ((clipCodes & 0x2AA) != 0x2AA)", but that is the only case left!

	}else{
		return false; // not outside, crossing or contained within.
	}
}


#endif // HACK_GTA4 && (__PS3 || __XENON)

#if HACK_GTA4

bool	rmOccluderSystem::IsSphereOccluded( const pvsViewerID	id, const Vector3 &center, const float radius, const bool UNUSED_PARAM(bFrustumTest),int *UNUSED_PARAM(occluded_by))
{
	//	AUTO_PUSH_TIMER("IsSphereOccluded");


#if __DEV
	if( ! AreOccludersEnabled() )
		return false;
#endif

	PvsViewer	*pviewer = GetPvsViewerPtr(id);

	// AF: don't bother with this check the for loop does exactly the same
	//if( pviewer->GetNumActiveOccluders() == 0 )return false;// no occluders, cant be occluded then!

	/* AF: This is never called with bFrustumTest set to true. Actually why is there a frustum check in this function
	if( bFrustumTest )
	{
	Matrix44	*pMtx0 = pviewer->GetCullMtx0();
	Matrix44	*pMtx1 = pviewer->GetCullMtx1();
	// todo: implement?
	// if outside frustum, return 'true' occluded result. maybe we should return an enum/bitflags instead!

	Vector4 distancesLRNZ;                                 // left, right, near and z distance
	Vector4 distancesBTF;                                  // bottom, top and far distances
	Vector4 sphere4(center.x,center.y,center.z,1.0f);       // until I make a vector4*mtx44 that ignores w!

	bool	bInside = false;
	u32		AllCorners_AndResult_clipStatus = ~0U;	// logical product of all corner tests

	// loop start (but its a sphere so there's no loop =P
	u32	clipStatus = 0U;						// *MUST RESET FOR EACH VERTEX TEST!*

	// fully outside?
	distancesLRNZ.Dot( sphere4, *pMtx0 );//test first 3 planes.

	//> float zDist = distancesLRNZ.w; // zdist to viewer (TODO: track min/max zdist of object if we processed all corners?)

	if (distancesLRNZ.x > radius)
	clipStatus |= 1<<6;
	else if (distancesLRNZ.x < -radius)
	clipStatus |= 1<<7;

	if (distancesLRNZ.y > radius)
	clipStatus |= 1<<8;
	else if (distancesLRNZ.y < -radius)
	clipStatus |= 1<<9;

	if (distancesLRNZ.z > radius)
	clipStatus |= 1<<10;
	else if (distancesLRNZ.z < -radius)
	clipStatus |= 1<<11;

	distancesBTF.Dot( sphere4, *pMtx1 );//test next 3 planes.

	if (distancesBTF.x > radius)
	clipStatus |= 1<<0;
	else if (distancesBTF.x < -radius)
	clipStatus |= 1<<1;

	if (distancesBTF.y > radius)
	clipStatus |= 1<<2;
	else if (distancesBTF.y < -radius)
	clipStatus |= 1<<3;

	if (distancesBTF.z > radius)
	clipStatus |= 1<<4;
	else if (distancesBTF.z < -radius)
	clipStatus |= 1<<5;

	AllCorners_AndResult_clipStatus &= clipStatus;

	if ((clipStatus & 0xAAA) == 0xAAA)// found a vertex that is completely inside, that means there's no way this thing is outside the frustum, break out of loop.
	{
	bInside = true;
	//	break;//  at least 1 vertex is inside, break out, its a waste to test anymore. Only thing that can hide this now is an occluder.
	}

	if( !bInside )
	{
	// didn't break out of loop, check out the clip flags for the box

	if( AllCorners_AndResult_clipStatus & 0x555 )
	{
	//all box corners vertices clip flags 'and'ed together show all the vertices were outside at least 1 single plane.
	return true;// occluded! (outside frustum
	}
	}
	}*/


	Vector4 radiusV(-radius);

	// Go through all occluders, test boxcorners against each plane - if one of the vertices fails the test then its crossing.
	//
	// note: if we need to implement an in/out/crossing result instead of pass/fail then we can't early out.
	int total_active = pviewer->GetNumActiveOccluders();

	for( int idx=0; idx < total_active; idx++ )
	{
		mcActiveOccluderPoly *pActiveOccluderPoly = pviewer->GetActiveOccluderIndexPtr( idx );

		if( pActiveOccluderPoly->m_pOccPoly )// handle geom occluders (null ptr case) safely
		{
			continue;// dont support poly plane occluders right now, just geom ones.

		}

		int numplanes = pActiveOccluderPoly->m_num_planes;

		bool bFail = false;

		for( int plane=0; plane < numplanes; plane++)
		{
			Vector4	*pPlane = &pActiveOccluderPoly->m_occlusionPlanes[ plane ];
			// sub loop, test each corner in transformed box, first case that is outside fails

			Vector4 dist = pPlane->DistanceToPlaneV(center);
			if (dist.IsGreaterThan(radiusV))
			{
				bFail = true;// crossing, tag as failing test, break loop
				break;
			}
		}

		if( !bFail )
		{
			// todo: buffer occluded volumes here! (in a different color too!)
			/*if( OccludedVolumesBuffered_GetMode() )// bound buffering on for this test?
			{
			AddBufferedDebugDrawVolume( center, radius, id, pvscull_all_clear );
			}*/

			return true; // occluded completely by one of our occluders
		}
	}// continue looping through rest of occluder plane sets.

	return false;// nothing occluded this object
}

bool	rmOccluderSystem::IsAABoxOccluded(	const pvsViewerID	id,
										  const Vector4		*pv4Min,
										  const Vector4		*pv4Max,
										  const Matrix34		*pMtxLocalToWorld,// if null, we assume identity xform.
										  const bool			UNUSED_PARAM(bFrustumTest),
										  int *UNUSED_PARAM(occluded_by)
										  )
{

	PvsViewer	*pviewer = GetPvsViewerPtr(id);
	Vector4	VectZero;
	VectZero.Zero();

	if( pviewer->GetNumActiveOccluders() == 0 )return false;// no occluders, cant be occluded then!


	Matrix44 worldCorners1,worldCorners2;
	Vector3	worldCornersArray0;
	Vector3	worldCornersArray1;
	Vector3	worldCornersArray2;
	Vector3	worldCornersArray3;
	Vector3	worldCornersArray4;
	Vector3	worldCornersArray5;
	Vector3	worldCornersArray6;
	Vector3	worldCornersArray7;

	worldCornersArray0.x = pv4Min->x;
	worldCornersArray0.y = pv4Min->y;
	worldCornersArray0.z = pv4Min->z;

	worldCornersArray4.x = pv4Max->x;
	worldCornersArray4.y = pv4Max->y;
	worldCornersArray4.z = pv4Max->z;

	worldCornersArray5.x = worldCornersArray4.x;
	worldCornersArray5.y = worldCornersArray4.y;
	worldCornersArray5.z = worldCornersArray0.z;

	worldCornersArray6.x = worldCornersArray4.x;
	worldCornersArray6.z = worldCornersArray4.z;
	worldCornersArray6.y = worldCornersArray0.y;

	worldCornersArray7.y = worldCornersArray0.y;
	worldCornersArray7.z = worldCornersArray0.z;
	worldCornersArray7.x = worldCornersArray4.x;

	worldCornersArray1.x = worldCornersArray0.x;
	worldCornersArray1.y = worldCornersArray0.y;
	worldCornersArray1.z = worldCornersArray4.z;

	worldCornersArray2.x = worldCornersArray0.x;
	worldCornersArray2.z = worldCornersArray0.z;
	worldCornersArray2.y = worldCornersArray4.y;

	worldCornersArray3.y = worldCornersArray4.y;
	worldCornersArray3.z = worldCornersArray4.z;
	worldCornersArray3.x = worldCornersArray0.x;

	if( pMtxLocalToWorld )// if we get passed NULL, we skip the transform assuming the aabox is using an identity matrix, which also means it stays axis aligned!
	{
		// transform box corners
		pMtxLocalToWorld->Transform(worldCornersArray0);
		pMtxLocalToWorld->Transform(worldCornersArray1);
		pMtxLocalToWorld->Transform(worldCornersArray2);
		pMtxLocalToWorld->Transform(worldCornersArray3);
		pMtxLocalToWorld->Transform(worldCornersArray4);
		pMtxLocalToWorld->Transform(worldCornersArray5);
		pMtxLocalToWorld->Transform(worldCornersArray6);
		pMtxLocalToWorld->Transform(worldCornersArray7);
	}

#if	__XENON 
	//convert to structure of arrays


	worldCorners1.a=(Vector4)worldCornersArray0;
	worldCorners1.b=(Vector4)worldCornersArray1;
	worldCorners1.c=(Vector4)worldCornersArray2;
	worldCorners1.d=(Vector4)worldCornersArray3;

	worldCorners2.a=(Vector4)worldCornersArray4;
	worldCorners2.b=(Vector4)worldCornersArray5;
	worldCorners2.c=(Vector4)worldCornersArray6;
	worldCorners2.d=(Vector4)worldCornersArray7;

	worldCorners1.a.w=-1.0f;
	worldCorners1.b.w=-1.0f;
	worldCorners1.c.w=-1.0f;
	worldCorners1.d.w=-1.0f;

	worldCorners2.a.w=-1.0f;
	worldCorners2.b.w=-1.0f;
	worldCorners2.c.w=-1.0f;
	worldCorners2.d.w=-1.0f;
#endif


	// Go through all occluders, test boxcorners against each plane - if one of the vertices fails the test then its crossing.
	//
	// note: if we need to implement an in/out/crossing result instead of pass/fail then we can't early out.
	int total_active = pviewer->GetNumActiveOccluders();

	for( int idx=0; idx < total_active; idx++ )
	{

		mcActiveOccluderPoly *pActiveOccluderPoly = pviewer->GetActiveOccluderIndexPtr( idx );

		if( pActiveOccluderPoly->m_pOccPoly )// handle geom occluders (null ptr case) safely
		{
			continue;// dont support poly plane occluders right now, just geom ones.

			// Test occluder poly first.
			//			if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly->GetPlane().DistanceToPlane( center ) > -radius )
			//			{
			//				continue;// outside or crossing occlusion frustum, move to next active.
			//			}
		}

		int numplanes = pActiveOccluderPoly->m_num_planes; 

		bool bFail = false;
#if	__XENON 

		for( int plane=0; plane < numplanes; plane++)
		{
			Vector4	*pPlane = &pActiveOccluderPoly->m_occlusionPlanes[ plane ];

			// sub loop, test each corner in transformed box, first case that is outside fails

			u32	OutBitsa;

			Vector4  ox = __vdot4fp(*pPlane, worldCorners1.a);					
			Vector4  oy = __vdot4fp(*pPlane, worldCorners1.b);					
			Vector4  oz = __vdot4fp(*pPlane, worldCorners1.c);					
			Vector4  ow = __vdot4fp(*pPlane, worldCorners1.d);					

			Vector4  ox2 = __vdot4fp(*pPlane, worldCorners2.a);					
			Vector4  oy2 = __vdot4fp(*pPlane, worldCorners2.b);					
			Vector4  oz2 = __vdot4fp(*pPlane, worldCorners2.c);					
			Vector4  ow2 = __vdot4fp(*pPlane, worldCorners2.d);					

			//6 cycle delay
			ox=__vmaxfp(ox,oy);
			//1 cycle delay
			oy=__vmaxfp(oz,ow);

			//1 cycle delay
			ox2=__vmaxfp(ox2,oy2);
			//1 cycle delay
			oy2=__vmaxfp(oz2,ow2);

			//8 cycle delay
			ox=__vmaxfp(ox,oy);
			//3 cycle delay
			ox2=__vmaxfp(ox2,oy2);
			//11 cycle delay
			ox=__vmaxfp(ox,ox2);
			//11 cycle delay
			__vcmpgtfpR(VectZero,ox,&OutBitsa); //OutBits bit 24 set if all true bit 26 set if all false



			if(((OutBitsa&0x80)>>7))
			{
			}
			else
			{
				bFail = true;// crossing, tag as failing test, break loop
				break;
			}
		} //end for 
#else

		{
			const	float radius = 0.f;// testing points, not spheres so it should be zero.



			for( int plane=0; plane < numplanes; plane++)
			{
				Vector4	*pPlane = &pActiveOccluderPoly->m_occlusionPlanes[ plane ];

				// sub loop, test each corner in transformed box, first case that is outside fails


				if( pPlane->DistanceToPlane( worldCornersArray0.xyzw ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}


				if( pPlane->DistanceToPlane( worldCornersArray4.xyzw ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}


				if( pPlane->DistanceToPlane( worldCornersArray1.xyzw ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCornersArray2.xyzw ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCornersArray3.xyzw ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCornersArray5.xyzw ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCornersArray6.xyzw ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCornersArray7.xyzw ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				/*
				if( m_activeOccluders[ viewID ][ idx ].m_occlusionPlanes[ plane ].DistanceToPlane( center ) > -radius )
				{
				bFail = true;// crossing, tag as failing test, break loop
				break;
				}			
				*/

			}
		}
#endif
		if( !bFail )
		{
			// todo: buffer occluded volumes here! (in a different color too!)

			// todo: pass back occluder ID# to object so it can remember which occluder hid it last time, for possible reordering?

			// todo: track number of objects hidden per occluder, adjust occluder test order based on last frame's stats?
			return true; // occluded completely by one of our occluders
		}

	}// continue looping through rest of occluder plane sets.



	return false;// nothing occluded this object - yuck!
}

void rmOccluderSystem::ReserveViewers(int count)
{
	// reserve a number of viewers.
	Assert(!m_pvsViewers);
	m_pvsViewers = (PvsViewer*)sysMemAllocator::GetCurrent().Allocate(sizeof(PvsViewer)*count,__alignof(PvsViewer));
#if HACK_GTA4
	sysMemSet(m_pvsViewers, 0x00, sizeof(PvsViewer)*count);
#endif
	m_numPvsViewers = 0;
	m_maxPvsViewers = count;	
}

pvsViewerID	rmOccluderSystem::CreateViewer()
{
	// grow atArray of pvsViewers, get index and return it.
	Assert(m_numPvsViewers < m_maxPvsViewers);
	return m_numPvsViewers++;
}

void rmOccluderSystem::DeleteViewer( const pvsViewerID  )
{
	Assert(0);
}

#endif // HACK_GTA4

/*
bool rmOccludePolyhedron::TestOutsideSinglePlane( int *pClipFlags )//[ MAX_GEOM_TEST_VERTS ];)
{
	#define	MAX_GEOM_TEST_VERTS 16

	Vector4 av4_src;//[ MAX_GEOM_TEST_VERTS ];
	//Vector4 av4_dst[ MAX_GEOM_TEST_VERTS ];
//	int		clipFlags[ MAX_GEOM_TEST_VERTS ];
	int		clipResult = 0;
	int		prevFlag   = 0;// must clear!

	int n;
	int numVerts = GetNumVerts();
	Assert( numVerts <= MAX_GEOM_TEST_VERTS );

	// test vertex pool against each plane.
	for( n=0; n < numVerts; n++)
	{
		GetVertex4( n, &av4_src );// copy vertex to source buffer.

		av4_src.w = 0.f;// hack radius to 0

		// it really isnt a good idea to rely on pipe being setup to the same viewport we are occlusion testing on!
#if 1
		// FIXME: USE INTERNAL FRUSTUM SPHERE VIS TEST against the currently active pvs view??????

		
//Assert(0 && "FIXME");
		clipResult = rmOccluderSystem::sm_gCurrOccluderSysInstanceActive->InlineFastSphereVisCheck_ClipStatus_A( av4_src );
//clipResult = true;
		//clipResult = PIPE.GetViewport()->InlineFastSphereVisCheck_ClipStatus( av4_src );
#else
		//RM_OCCLUDE->PrepDualFrustumCullMatrices( RM_OCCLUDE->GetActiveViewID(), RM_OCCLUDE->GetActiveViewID()+1 );
		if( RM_OCCLUDE->GetActiveViewID() & 1 )
		{
			clipResult = RM_OCCLUDE->InlineFastSphereVisCheck_ClipStatus_B( av4_src );
		}
		else
		{
			clipResult = RM_OCCLUDE->InlineFastSphereVisCheck_ClipStatus_A( av4_src );
		}
#endif

	//	clipResult = 0xAAA;// hack!

		if( clipResult & 0x555 )
		{
			pClipFlags[n]	= clipResult;//(clipResult & 0x555);
			prevFlag		= clipResult;//(clipResult & 0x555);
		}else{
			pClipFlags[n]	= clipResult;// we dont care about other flags right now.
		}
	}

	// loop through all clip flags, AND'ing them together.
	for( n=0; n < numVerts; n++)
	{
		prevFlag &= pClipFlags[n];
	}

	if( prevFlag & 0x555 )
	{
		return true;// one of the outside plane flags was set for all the vertices tested, so return TRUE (outside a single plane)


		// clipStatus:  -N +N -R +R -L +L -F +F -T +T -B +B
		//  - means dist is < -radius,
		//  + means dist is > radius

		//	if      (clipStatus & 0x555)            return cullOutside;      // far enough outside at least one plane to be rejected
		//	else if ((clipStatus & 0xAAA) == 0xAAA) return cullInside;       // we are far enough inside all planes
		//	else if (!(clipStatus & 0x800))         return cullClippedZNear; // clipped the near plane (important for some things)
		//	else                                    return cullClipped;      // clipped one of the sides or far,  really "if ((clipCodes & 0x2AA) != 0x2AA)", but that is the only case left!

	}else{
		return false; // not outside, crossing or contained within.
	}
}
*/


#if 0
// TODO: make a version for ps2 using asm/vu code
inline bool gfxViewport::InlineFast2SpheresOutsideSinglePlane(	const Vector4 &sphere_A,
																const Vector4 &sphere_B  ) const
{
	// There is no vector4*mtx44 that ignores the 'w' of the input vector, so copy and set w to 1....
	float	radius_a = sphere_A.w;
	float	radius_b = sphere_B.w;

	Vector4 center_a( sphere_A.x, sphere_A.y, sphere_A.z, 1.0f );
	Vector4 center_b( sphere_B.x, sphere_B.y, sphere_B.z, 1.0f );

	Vector4 distancesLRNZ_a;	// left, right, near and z distance
	Vector4 distancesLRNZ_b;	// left, right, near and z distance

	distancesLRNZ_a.Dot( center_a,sm_FastCullMtx[0] ); 
	distancesLRNZ_b.Dot( center_b,sm_FastCullMtx[0] ); 

	//
	// Test for fully outside cases on each plane
	//

	if( (distancesLRNZ_a.x > radius_a) && (distancesLRNZ_b.x > radius_b) ) return true; // both ends outside of LEFT  plane
	if( (distancesLRNZ_a.y > radius_a) && (distancesLRNZ_b.y > radius_b) ) return true; // both ends outside of RIGHT plane
	if( (distancesLRNZ_a.z > radius_a) && (distancesLRNZ_b.z > radius_b) ) return true; // both ends outside of NEAR  plane

	//-------------------------------------------------------------------------
	// Other plane tests in case it wasnt out on left, right or near planes...
	// Usually these cases will not be tested since most stuff gets culled against the left/right/near plane first.
	{
		Vector4 distancesBTF_a;	// bottom, top and far distances
		Vector4 distancesBTF_b;	// bottom, top and far distances
	
	  	distancesBTF_a.Dot( center_a ,sm_FastCullMtx[1] );
		distancesBTF_b.Dot( center_b ,sm_FastCullMtx[1] );
	
		if( (distancesBTF_a.x > radius_a) && (distancesBTF_b.x > radius_b) ) return true; // both ends outside of BOTTOM	plane
		if( (distancesBTF_a.y > radius_a) && (distancesBTF_b.y > radius_b) ) return true; // both ends outside of TOP		plane
		if( (distancesBTF_a.z > radius_a) && (distancesBTF_b.z > radius_b) ) return true; // both ends outside of FAR		plane
	}
	//-------------------------------------------------------------------------

	// If we got to here then the 2 spheres (or hotdog they define) is not completely outside of a single plane.

	// NOTE: If the 2 spheres are used to define an extruded volume (like a hotdog) the volume may not penetrate the convex hull
	// defined by the frustum planes. Which would be a false result.

	return false;
}
#endif

#undef __SPANBUFFER_ENABLED
#undef _OCCLUDER_IGNOREFACE_EDGEKILL
#undef _OCCLUDER_DEBUGGING
#undef _OCCLUDER_WARNINGS
#undef _OCCLUDER_TIMING
#undef _OCCLUDER_REMOVE_UNDERGROUNDEDGE
#undef _OCCLUDER_REJECT_LOWEDGES
#undef __POLYSPAN_DEBUG

//} // namespace rage
