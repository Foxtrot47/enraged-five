// 
// rmocclude/occluder_file.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "bank/bank.h"
#include "data/resource.h"

#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"

#include "diag/memstats.h"
#include "system/timemgr.h"
#include "string/string.h"
#include "system/param.h"
#include "grcore/device.h"
#include "grcore/viewport.h"
//#include "grcore/state.h"
#include "grcore/im.h"
#include "rmocclude/occluder.h"

bool	rmOccluderSystem::LoadExternalGeomDataset( const char * basename,
												   const char * ASSERT_ONLY(subf),
												   // destination ptrs
												   int						*p_numOccluderGeoms,
												   rmOccludePolyhedron		**pp_OccluderGeomArray,
												   int						*p_numOccluderPolys,
												   rmOccludePoly			**pp_OccluderPolyArray
												   )
{
	Assert( basename );
	Assert( subf );
	Assert( p_numOccluderGeoms );
	Assert( pp_OccluderGeomArray );
	Assert( p_numOccluderPolys );
	Assert( pp_OccluderPolyArray );


	//	fiStream * stream = ASSET.Open(subf, basename, "occlude");
	fiStream * stream = ASSET.Open( basename, "occlude");

	Displayf(" Loading Occluder *Geom* Dataset for %s ", basename );

	if (! stream)
	{
		char tmpErrorString[256];
		formatf( tmpErrorString, sizeof(tmpErrorString), "rmOccluderSystem::LoadDynamicGeomDataset() - could not open file: %s.occ", basename);
		return false;
	}

	fiAsciiTokenizer token;
	token.Init(basename, stream);

	diagLogMemoryMessage("Loading rmOccluderSystem -- LoadDynamicGeomDataset");
	diagLogMemoryPush();

	bool retval = Load( /* system,*/ token,
		p_numOccluderGeoms, pp_OccluderGeomArray,
		p_numOccluderPolys, pp_OccluderPolyArray
		);

	diagLogMemoryPop();
	diagLogMemory("rmOccluderSystem -- LoadDynamicGeomDataset");

	stream->Close();

	return retval;
}
/*
PURPOSE
Main entry point for loading an occluder file for the occlusion system

RETURN
bool - 'true' if the file loads successfully,
otherwise 'false' if an error occurs due to no file present or other errors happen during construction or parsing the file.
*/
bool rmOccluderSystem::LoadDataset ( /* rmOccluderSystem & system, */const char * basename, const char * /*subf*/)
{
	fiStream * stream = fiStream::PreLoad( ASSET.Open(/* subf, */ basename, /* "occlude"*/  "") );

	Displayf(" Loading Occluder Dataset for %s ", basename );

	if (! stream)
	{
		char tmpErrorString[256];
		formatf( tmpErrorString, sizeof(tmpErrorString), "rmOccluderSystem::LoadDataset() - could not open file: %s.occ", basename);
		return false;
	}

	fiAsciiTokenizer token;
	token.Init(basename, stream);

	diagLogMemoryMessage("Loading rmOccluderSystem");
	diagLogMemoryPush();
	bool retval = Load( /* system,*/ token,
		&this->m_numOccluderGeoms, &this->m_OccluderGeomArray,
		&this->m_numOccluderPolys, &this->m_OccluderPolyArray													
		);

	diagLogMemoryPop();
	diagLogMemory("rmOccluderSystem");

	stream->Close();

	return retval;
}


/*
PURPOSE
This function calls the fragment loaders for specific sections of the file in a fixed order.
Each sub function should be returning true unless an error occurs.

PARAMS

RETURN
bool - 'false' if loading failed, otherwise 'true' if everything went OK

*/
bool rmOccluderSystem::Load ( /* rmOccluderSystem & system, */ fiAsciiTokenizer & token,
							 int						*p_numOccluderGeoms,
							 rmOccludePolyhedron	**pp_OccluderGeomArray,
							 int						*p_numOccluderPolys,
							 rmOccludePoly			**pp_OccluderPolyArray
							 )
{
	int version = -1;

	version = LoadHeader(	token,
		p_numOccluderGeoms, pp_OccluderGeomArray,
		p_numOccluderPolys, pp_OccluderPolyArray );

	if( version == -1 )
	{
		return false;// version was incorrect or other error...
	}

	LoadGeoms( version, token, p_numOccluderGeoms, pp_OccluderGeomArray );

	// removing support for these eventually...
	LoadPolys( version, token, p_numOccluderPolys, pp_OccluderPolyArray );

	return true;
}

/*
PURPOSE
Allocate 'n' number of occluder polygon structs (arrayed) in preperation of loading each specific polygon's number of sides, vertices and other info.

PARAMS
int - number of polygons to allocate

RETURN
bool - 'true' if successful, 'false' if the allocation failed.
*/
bool rmOccluderSystem::AllocatePolys (	int n,
										int						*p_numOccluderPolys,
										rmOccludePoly			**pp_OccluderPolyArray
									  )
{
	// make sure previous set was released
	Assert( *p_numOccluderPolys	  == 0	  );
	Assert( *pp_OccluderPolyArray == NULL );

	// allocate array
	rmOccludePoly * p = rage_new rmOccludePoly [n];

	if (! p) return false;

	*p_numOccluderPolys			= n;
	*pp_OccluderPolyArray		= p;

	return true;
}


bool rmOccluderSystem::AllocateGeoms (	int n,
										int					*p_numOccluderGeoms,
										rmOccludePolyhedron	**pp_OccluderGeomArray								  
									 )
{
	// make sure previous set was released
	Assert( *p_numOccluderGeoms	  == 0	  );
	Assert( *pp_OccluderGeomArray == NULL );

	// allocate array
	rmOccludePolyhedron * p = rage_new rmOccludePolyhedron [n];

	if (! p) return false;

	*p_numOccluderGeoms			= n;
	*pp_OccluderGeomArray		= p;

	return true;
}

/*
PURPOSE
Destroy occluder poly dataset if it is present.
Usually called by externally managed data sets that the occluder system instance does not own.

PARAMS

RETURN

*/
void rmOccluderSystem::ReleasePolys (	int					*p_numOccluderPolys,
										rmOccludePoly		**pp_OccluderPolyArray
										)
{
	if( *pp_OccluderPolyArray )
		delete [] *pp_OccluderPolyArray;

	*pp_OccluderPolyArray = NULL;
	*p_numOccluderPolys = 0;
}

void rmOccluderSystem::ReleaseGeoms (	int						*p_numOccluderGeoms,
									 rmOccludePolyhedron	**pp_OccluderGeomArray									 									 
									 ) 
{
	if( *pp_OccluderGeomArray )
		delete [] *pp_OccluderGeomArray;

	*pp_OccluderGeomArray = NULL;
	*p_numOccluderGeoms = 0;
}

/*
PURPOSE
Load an occluder poly dataset.
Format in the text file per polygon:

verts: 4
activate: 0
-1347.59265 -51 -9.42844
-1282.37268 -51 23.80285
-1282.37268 20.14994 23.80285
-1347.59265 20.14994 -9.42844

PARAMS

RETURN

*/
bool rmOccluderSystem::LoadPolys (int				version,
								  fiAsciiTokenizer	& token,
								  int				*p_numOccluderPolys,
								  rmOccludePoly		**pp_OccluderPolyArray
								  )
{
	Vector3	vertex;

	switch(version)
	{
	case RM_OCCLUDE_FILE_VERSION:
	case RM_OCCLUDE_FILE_VERSION_2:
		Displayf(" rmOccluderSystem::LoadPolys() ");
		break;

	default:
		Errorf("rmOccluderSystem::LoadPolys() parse error.\n*** Unsupported occlusion file version %d - expected versions %d or %d ***",
			version, RM_OCCLUDE_FILE_VERSION, RM_OCCLUDE_FILE_VERSION_2 );
		return false;
		/*NOTREACHED*/
		break;
	}



	Assert( pp_OccluderPolyArray );
	rmOccludePoly	*pArray = *pp_OccluderPolyArray;
	Assert( pArray );

	Assert( p_numOccluderPolys );
	int polycount = *p_numOccluderPolys;	

	for( int n=0; n < polycount; n++)
	{
		int vertcount = token.MatchInt("verts:");

		float activeDist = token.MatchFloat("activate:");
		pArray[n].ActiveDist( activeDist );

		for( int k=0; k< vertcount; k++)
		{	
			vertex.x = token.GetFloat();
			vertex.y = token.GetFloat();
			vertex.z = token.GetFloat();

			pArray[n].SetVertex(k, vertex);
		}
		pArray[n].CalculatePlane();// compute plane equation now that we have all the vertices for the poly
	}

	Displayf(" rmOccluderSystem::LoadPolys() - %d polys loaded successfully.", polycount );
	return true;
}

bool rmOccluderSystem::LoadGeoms (int					version,
								  fiAsciiTokenizer		& T,
								  int					*p_numOccluderGeoms,
								  rmOccludePolyhedron	**pp_OccluderGeomArray
								  )
{
	Vector3	vertex;

	switch(version)
	{
		case RM_OCCLUDE_FILE_VERSION:
		case RM_OCCLUDE_FILE_VERSION_2:
			Displayf(" rmOccluderSystem::LoadGeoms() ");
			break;

		default:
			Errorf("rmOccluderSystem::LoadGeoms() parse error.\n*** Unsupported occlusion file version %d - expected versions %d or %d ***",
				version, RM_OCCLUDE_FILE_VERSION, RM_OCCLUDE_FILE_VERSION_2 );
			return false;
			/*NOTREACHED*/
			break;
	}

	Assert( pp_OccluderGeomArray );
	rmOccludePolyhedron	*pArray = *pp_OccluderGeomArray;
	Assert( pArray );

	Assert( p_numOccluderGeoms );
	int geomcount = *p_numOccluderGeoms;	

	if(version == RM_OCCLUDE_FILE_VERSION_2 )
	{
		// re-written, based on code from -> mc4/tools/src/mcexcityMayaShared/occluderShared.h
		T.MatchToken("{");// { right after the occluder count in the file.

		for( int n=0; n < geomcount; n++)
		{
			if (!T.CheckToken("Occluder"))
			{
				Assert(0);
				//return false;
			}

			//int nOccluderNumber = T.GetInt();		// instance number
			T.IgnoreToken();


			T.MatchToken("{");
			{
				char _szTypeName[256];// unused right now, not stored in member data, scratchbuff.
				char _szName[256];
				T.MatchToken("Type");
				T.GetToken(_szTypeName, sizeof(_szTypeName));

				T.MatchToken("Name");
				T.GetToken(_szName, sizeof(_szName));

				pArray[n].SetDebugStringName( &_szName[0] );
	#if 0
				// TODO: ADD THIS BACK IN IF WE NEED IT
				//float activeDist = token.MatchFloat("activate:");
				//pArray[n].ActiveDist( activeDist );
	#else
				pArray[n].ActiveDist( 1.f );
	#endif

				Vector4	t_sphere;
				T.MatchVector( "BoundingSphere", t_sphere );
				pArray[n].SetBoundSphere( t_sphere );

				//-----------------------------------------------------------------
				// VERTICES--------------------------------------------------------
				T.MatchToken("Vertices");
				int vertcount = T.GetInt();
				pArray[n].AllocateVertexPool( vertcount );

				T.MatchToken("{");
				{
					for (int k=0; k< vertcount; k++)
					{
						vertex.x = T.GetFloat();
						vertex.y = T.GetFloat();
						vertex.z = T.GetFloat();
						pArray[n].SetVertex(k, vertex);
					}
				}
				T.MatchToken("}");

				//-----------------------------------------------------------------
				// EDGES-----------------------------------------------------------
				T.MatchToken("Edges");
				int edgecount = T.GetInt();
				pArray[n].AllocateEdges( edgecount );

				T.MatchToken("{");
				{
					for (int e=0; e<edgecount; e++)
					{
						char e0, e1;
						e0 = (char) T.GetByte();
						e1 = (char) T.GetByte();
						pArray[n].SetEdgeIndices( (char) e, e0, e1 );
					}
				}
				T.MatchToken("}");

				//-----------------------------------------------------
				// load faces
				int facecount = T.MatchInt("Polygons");
				pArray[n].AllocateFacetsAndPlanes( facecount );

				T.MatchToken("{");
					for( int f=0; f< facecount; f++)
					{
						//-----------------------------------------------------
						// load face index pool
						//
						// Vertices  4
						// {
						// 	 0 1 3 2 
						// } 
						//
						int faceIndices = T.MatchInt("Vertices");
						Assert( faceIndices > 2 );

						T.MatchToken("{");
							pArray[n].CreateFacet( f, faceIndices );

							#if 0
								// NOT USED IN THIS VERSION.
								// int isIgnored = token.MatchInt("ignoreFace:");
								// pArray[n].SetFacetIgnoredFlag( f, isIgnored );
							#else
								pArray[n].SetFacetIgnoredFlag( f, false );
							#endif

							// load face definition indices
							for( int fi=0; fi <faceIndices; fi++)
							{
								int fIdx = T.GetByte();
								pArray[n].SetFacetVertIndex( f, fi, fIdx );
							}
						T.MatchToken("}");
						//-----------------------------------------------------

						//-----------------------------------------------------
						// load edge index pool, edges start from first vertex in n-gon, winding clockwise
						//
						// Edges 4
						// 4 8 7 0
						//
						int edgeIndices = T.MatchInt("Edges");			
						T.MatchToken("{");
							Assert( edgeIndices > 2 );
							Assert( faceIndices == edgeIndices );// number of verts and edge should always match
							// load face definition indices
							for( int ei=0; ei <edgeIndices; ei++)
							{
								int eIdx = T.GetByte();
								pArray[n].SetFacetEdgeIndex( f, ei, eIdx );
							}
						T.MatchToken("}");

						//-----------------------------------------------------
						// calculate plane for face.
					}
				T.MatchToken("}");

				// all facees loaded, calculate plane equations for each facet
				pArray[n].CalculateFacetPlanes();
				// set reverse edge flags for face
				pArray[n].CalculateEdgeWindingData();

				pArray[n].SetActiveMode( true );// on by default to be backward compatible. Later, file format updates or other mechanisms can override this.

				// NOTE: we dont compute planes for these anymore, its done on the fly when the occluder is considered 'active'
				//	pArray[n].CalculatePlane();// compute plane equation now that we have all the vertices for the poly
				//-----------------------------------------------------
			}
			T.MatchToken("}");// closing } at the end of each occluder
		}// geomcount loop

		T.MatchToken("}");// closing } at the end of the file

	}// VERSION 4
	else
	{
	// version 3
		for( int n=0; n < geomcount; n++)
		{
			int vertcount = T.MatchInt("verts:");

			float activeDist = T.MatchFloat("activate:");
			pArray[n].ActiveDist( activeDist );

			Vector4	t_sphere;
			T.MatchVector( "sphere:", t_sphere );
			pArray[n].SetBoundSphere( t_sphere );


			//-----------------------------------------------------
			// load vertex pool
			pArray[n].AllocateVertexPool( vertcount );

			for( int k=0; k< vertcount; k++)
			{	
				vertex.x = T.GetFloat();
				vertex.y = T.GetFloat();
				vertex.z = T.GetFloat();
				pArray[n].SetVertex(k, vertex);
			}
			//-----------------------------------------------------

			//-----------------------------------------------------
			// load edge pool
			int edgecount = T.MatchInt("edges:");	
			pArray[n].AllocateEdges( edgecount );

			for( int e=0; e< edgecount; e++)
			{
				char e0, e1;
				e0 = (char) T.GetByte();
				e1 = (char) T.GetByte();
				pArray[n].SetEdgeIndices( (char) e, e0, e1 );
			}
			//-----------------------------------------------------

			//-----------------------------------------------------
			// load faces
			int facecount = T.MatchInt("faces:");
			pArray[n].AllocateFacetsAndPlanes( facecount );

			for( int f=0; f< facecount; f++)
			{
				//-----------------------------------------------------
				// load face index pool
				//
				// faceIndices: 4
				// 0 1 2 3
				//
				int faceIndices = T.MatchInt("faceIndices:");
				Assert( faceIndices > 2 );

				pArray[n].CreateFacet( f, faceIndices );


				int isIgnored = T.MatchInt("ignoreFace:");
				pArray[n].SetFacetIgnoredFlag( f, isIgnored );

				// load face definition indices
				for( int fi=0; fi <faceIndices; fi++)
				{
					int fIdx = T.GetByte();
					pArray[n].SetFacetVertIndex( f, fi, fIdx );
				}
				//-----------------------------------------------------

				//-----------------------------------------------------
				// load edge index pool, edges start from first vertex in n-gon, winding clockwise
				//
				// edgeIndices:
				// 4 8 7 0
				//
				int edgeIndices = T.MatchInt("edgeIndices:");			
				Assert( edgeIndices > 2 );
				Assert( faceIndices == edgeIndices );// number of verts and edge should always match
				// load face definition indices
				for( int ei=0; ei <edgeIndices; ei++)
				{
					int eIdx = T.GetByte();
					pArray[n].SetFacetEdgeIndex( f, ei, eIdx );
				}
				//-----------------------------------------------------

				// calculate plane for face.
			}
			// all facees loaded, calculate plane equations for each facet
			pArray[n].CalculateFacetPlanes();
			// set reverse edge flags for face
			pArray[n].CalculateEdgeWindingData();

			pArray[n].SetActiveMode( true );// on by default to be backward compatible. Later, file format updates or other mechanisms can override this.

			// NOTE: we dont compute planes for these anymore, its done on the fly when the occluder is considered 'active'
			//	pArray[n].CalculatePlane();// compute plane equation now that we have all the vertices for the poly
			//-----------------------------------------------------
		}
	}// version 3

	Displayf(" rmOccluderSystem::LoadGeoms() - %d geoms loaded successfully.", geomcount );
	return true;
}

/*
PURPOSE
Load an occluder file header.
Format in the text file is:

version: 2
num_primitives: 17
...
...rest of data following 
,,,

PARAMS

RETURN
int - returns '-1' if an error occurs such as a mismatched version. Otherwise an int > 0 matching the appropriate file version if loading is successful.

NOTES
Vertex winding order of the polygon data is counter-clockwise.
*/
int rmOccluderSystem::LoadHeader ( fiAsciiTokenizer			& token, 
								   int						*p_numOccluderGeoms,
								   rmOccludePolyhedron		**pp_OccluderGeomArray,
								   int						*p_numOccluderPolys,
								   rmOccludePoly			**pp_OccluderPolyArray
								   )
{
	ReleasePolys( p_numOccluderPolys, pp_OccluderPolyArray );// Always do this first before we read any data, since version/count info is wiped
	ReleaseGeoms( p_numOccluderGeoms, pp_OccluderGeomArray );

	// Load header info
	int version =  token.MatchInt("version:");

	switch(version)
	{
		case RM_OCCLUDE_FILE_VERSION:
		case RM_OCCLUDE_FILE_VERSION_2:
			Displayf("rmOccluderSystem::LoadHeader() occlude file version: %d", version );
			break;

		default:
			Errorf("rmOccluderSystem::LoadHeader() parse error.\n*** Unsupported occlusion file version %d - expected versions %d or %d ***",
						version, RM_OCCLUDE_FILE_VERSION, RM_OCCLUDE_FILE_VERSION_2 );
			return -1;
			/*NOTREACHED*/
			break;
	}
 
	int geomcount		= token.MatchInt("Occluders");
	//int geomcount		= token.MatchInt("num_geoms:");
	int polycount		= 0;//token.MatchInt("num_primitives:");

	Displayf(" rmOccluderSystem::LoadHeader() Version %d - %d geoms      -", version, geomcount );
	Displayf(" rmOccluderSystem::LoadHeader() Version %d - %d primitives -", version, polycount );

	AllocateGeoms( geomcount, p_numOccluderGeoms, pp_OccluderGeomArray );
	AllocatePolys( polycount, p_numOccluderPolys, pp_OccluderPolyArray );// Allocate storage based on what the header says

	return version;
}

