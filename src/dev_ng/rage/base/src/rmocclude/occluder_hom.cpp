// 
// rmocclude/occluder_hom.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// NOTE: This file is just temporary 'code junkyard' for now until this class is implemented

#if 0


bool	rmOccluderSystem::IsAABoxOccluded(	const pvsViewerID	id,
										  const Vector4		*pv4Min,
										  const Vector4		*pv4Max,
										  const Matrix34		*pMtxLocalToWorld,// if null, we assume identity xform.
										  const bool			bFrustumTest
										  )
{
	PvsViewer	*pviewer = GetPvsViewerPtr(id);

	if( pviewer->GetNumActiveOccluders() == 0 )return false;// no occluders, cant be occluded then!

	Matrix44	*pMtx0 = pviewer->GetCullMtx0();
	Matrix44	*pMtx1 = pviewer->GetCullMtx1();


	// synthesize 8 corners in local space, transform to worldspace then test against occluders
	Vector3	worldCorners[8];

	worldCorners[0].x = pv4Min->x;
	worldCorners[0].y = pv4Min->y;
	worldCorners[0].z = pv4Min->z;

	worldCorners[4].x = pv4Max->x;
	worldCorners[4].y = pv4Max->y;
	worldCorners[4].z = pv4Max->z;

	worldCorners[5].x = worldCorners[4].x;
	worldCorners[5].y = worldCorners[4].y;
	worldCorners[5].z = worldCorners[0].z;

	worldCorners[6].x = worldCorners[4].x;
	worldCorners[6].z = worldCorners[4].z;
	worldCorners[6].y = worldCorners[0].y;

	worldCorners[7].y = worldCorners[0].y;
	worldCorners[7].z = worldCorners[0].z;
	worldCorners[7].x = worldCorners[4].x;

	worldCorners[1].x = worldCorners[0].x;
	worldCorners[1].y = worldCorners[0].y;
	worldCorners[1].z = worldCorners[4].z;

	worldCorners[2].x = worldCorners[0].x;
	worldCorners[2].z = worldCorners[0].z;
	worldCorners[2].y = worldCorners[4].y;

	worldCorners[3].y = worldCorners[4].y;
	worldCorners[3].z = worldCorners[4].z;
	worldCorners[3].x = worldCorners[0].x;

	if( pMtxLocalToWorld )// if we get passed NULL, we skip the transform assuming the aabox is using an identity matrix, which also means it stays axis aligned!
	{
		// transform box corners
		for(int i=0; i < 8 ; i++)
		{
			pMtxLocalToWorld->Transform(worldCorners[i]);
		}
	}

	const	float radius = 0.f;// testing points, not spheres so it should be zero.

	// corners are in worldspace now, frustum test if requested
	if( bFrustumTest )
	{
		// todo: implement?
		// if outside frustum, return 'true' occluded result. maybe we should return an enum/bitflags instead!

		Vector4 distancesLRNZ;                                 // left, right, near and z distance
		Vector4 distancesBTF;                                  // bottom, top and far distances
		Vector4 center;//(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!

		bool	bInside = false;
		u32	AllCorners_AndResult_clipStatus = ~0U;// logical product of all corner tests

		// NOTE: first time a corner's bitflags results are 'inside' all planes we can break out of the loop since we know its not going to be 
		for(int i=0; i < 8 ; i++)
		{
			// setup test point
			center.x = worldCorners[i].x;
			center.y = worldCorners[i].y;
			center.z = worldCorners[i].z;
			center.w = 1.f;

			u32	clipStatus = 0U;// reset for each vertex

			// fully outside?
			distancesLRNZ.Dot(center, *pMtx0 );//test first 3 planes.

			//> float zDist = distancesLRNZ.w; // zdist to viewer (TODO: track min/max zdist of object if we processed all corners?)

			if (distancesLRNZ.x > radius)
				clipStatus |= 1<<6;
			else if (distancesLRNZ.x < -radius)
				clipStatus |= 1<<7;

			if (distancesLRNZ.y > radius)
				clipStatus |= 1<<8;
			else if (distancesLRNZ.y < -radius)
				clipStatus |= 1<<9;

			if (distancesLRNZ.z > radius)
				clipStatus |= 1<<10;
			else if (distancesLRNZ.z < -radius)
				clipStatus |= 1<<11;

			distancesBTF.Dot(center, *pMtx1 );//test next 3 planes.

			if (distancesBTF.x > radius)
				clipStatus |= 1<<0;
			else if (distancesBTF.x < -radius)
				clipStatus |= 1<<1;

			if (distancesBTF.y > radius)
				clipStatus |= 1<<2;
			else if (distancesBTF.y < -radius)
				clipStatus |= 1<<3;

			if (distancesBTF.z > radius)
				clipStatus |= 1<<4;
			else if (distancesBTF.z < -radius)
				clipStatus |= 1<<5;

			AllCorners_AndResult_clipStatus &= clipStatus;

			if ((clipStatus & 0xAAA) == 0xAAA)// found a vertex that is completely inside, that means there's no way this thing is outside the frustum, break out of loop.
			{
				bInside = true;
				break;// 1 vertex is inside, break out, its a waste to test anymore. Only thing that can hide this now is an occluder.
			}
		}


		if( !bInside )
		{
			//didnt break out of loop, check out the clip flags for the box

			if( AllCorners_AndResult_clipStatus & 0x555 )
			{
				//all box corners vertices clip flags 'and'ed together show all the vertices were outside at least 1 single plane.
				return true;// occluded! (outside frustum
			}
		}
	}

	if(0)// !g_bOccludeTestAABoxes )
	{
		// no occlusion testing on boxes set, and the above frustum test failed, so its visible.
		return false;// not occluded!
	}
	else
	{
		// Go through all occluders, test boxcorners against each plane - if one of the vertices fails the test then its crossing.
		//
		// note: if we need to implement an in/out/crossing result instead of pass/fail then we can't early out.
		int total_active = pviewer->GetNumActiveOccluders();

		for( int idx=0; idx < total_active; idx++ )
		{
			mcActiveOccluderPoly *pActiveOccluderPoly = pviewer->GetActiveOccluderIndexPtr( idx );


			if( pActiveOccluderPoly->m_pOccPoly )// handle geom occluders (null ptr case) safely
			{
				continue;// dont support poly plane occluders right now, just geom ones.

				// Test occluder poly first.
				//			if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly->GetPlane().DistanceToPlane( center ) > -radius )
				//			{
				//				continue;// outside or crossing occlusion frustum, move to next active.
				//			}
			}

			int numplanes = pActiveOccluderPoly->m_num_planes;

			bool bFail = false;

			for( int plane=0; plane < numplanes; plane++)
			{
				Vector4	*pPlane = &pActiveOccluderPoly->m_occlusionPlanes[ plane ];
				// sub loop, test each corner in transformed box, first case that is outside fails

				Assert( !bFail );

				if( pPlane->DistanceToPlane( worldCorners[0] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[4] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}


				if( pPlane->DistanceToPlane( worldCorners[1] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[2] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[3] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[5] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[6] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[7] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				/*
				if( m_activeOccluders[ viewID ][ idx ].m_occlusionPlanes[ plane ].DistanceToPlane( center ) > -radius )
				{
				bFail = true;// crossing, tag as failing test, break loop
				break;
				}			
				*/
			}

			if( !bFail )
			{
				// todo: buffer occluded volumes here! (in a different color too!)

				// todo: pass back occluder ID# to object so it can remember which occluder hid it last time, for possible reordering?

				// todo: track number of objects hidden per occluder, adjust occluder test order based on last frame's stats?
				return true; // occluded completely by one of our occluders
			}

		}// continue looping through rest of occluder plane sets.

	}

	return false;// nothing occluded this object - yuck!
}

//-----------------------------------------------------------------------------
bool	rmOccluderSystem::IsSphereOccluded( const pvsViewerID	id, const Vector3 &center, const float radius, const bool bFrustumTest )
{
#if __DEV
	if( ! AreOccludersEnabled() )
		return false;
#endif

	PvsViewer	*pviewer = GetPvsViewerPtr(id);

	if( pviewer->GetNumActiveOccluders() == 0 )return false;// no occluders, cant be occluded then!

	if( bFrustumTest )
	{
		Matrix44	*pMtx0 = pviewer->GetCullMtx0();
		Matrix44	*pMtx1 = pviewer->GetCullMtx1();
		// todo: implement?
		// if outside frustum, return 'true' occluded result. maybe we should return an enum/bitflags instead!

		Vector4 distancesLRNZ;                                 // left, right, near and z distance
		Vector4 distancesBTF;                                  // bottom, top and far distances
		Vector4 sphere4(center.x,center.y,center.z,1.0f);       // until I make a vector4*mtx44 that ignores w!

		bool	bInside = false;
		u32		AllCorners_AndResult_clipStatus = ~0U;	// logical product of all corner tests

		// loop start (but its a sphere so there's no loop =P
		u32	clipStatus = 0U;						// *MUST RESET FOR EACH VERTEX TEST!*

		// fully outside?
		distancesLRNZ.Dot( sphere4, *pMtx0 );//test first 3 planes.

		//> float zDist = distancesLRNZ.w; // zdist to viewer (TODO: track min/max zdist of object if we processed all corners?)

		if (distancesLRNZ.x > radius)
			clipStatus |= 1<<6;
		else if (distancesLRNZ.x < -radius)
			clipStatus |= 1<<7;

		if (distancesLRNZ.y > radius)
			clipStatus |= 1<<8;
		else if (distancesLRNZ.y < -radius)
			clipStatus |= 1<<9;

		if (distancesLRNZ.z > radius)
			clipStatus |= 1<<10;
		else if (distancesLRNZ.z < -radius)
			clipStatus |= 1<<11;

		distancesBTF.Dot( sphere4, *pMtx1 );//test next 3 planes.

		if (distancesBTF.x > radius)
			clipStatus |= 1<<0;
		else if (distancesBTF.x < -radius)
			clipStatus |= 1<<1;

		if (distancesBTF.y > radius)
			clipStatus |= 1<<2;
		else if (distancesBTF.y < -radius)
			clipStatus |= 1<<3;

		if (distancesBTF.z > radius)
			clipStatus |= 1<<4;
		else if (distancesBTF.z < -radius)
			clipStatus |= 1<<5;

		AllCorners_AndResult_clipStatus &= clipStatus;

		if ((clipStatus & 0xAAA) == 0xAAA)// found a vertex that is completely inside, that means there's no way this thing is outside the frustum, break out of loop.
		{
			bInside = true;
			//	break;//  at least 1 vertex is inside, break out, its a waste to test anymore. Only thing that can hide this now is an occluder.
		}

		if( !bInside )
		{
			// didn't break out of loop, check out the clip flags for the box

			if( AllCorners_AndResult_clipStatus & 0x555 )
			{
				//all box corners vertices clip flags 'and'ed together show all the vertices were outside at least 1 single plane.
				return true;// occluded! (outside frustum
			}
		}
	}



	// Go through all occluders, test boxcorners against each plane - if one of the vertices fails the test then its crossing.
	//
	// note: if we need to implement an in/out/crossing result instead of pass/fail then we can't early out.
	int total_active = pviewer->GetNumActiveOccluders();

	for( int idx=0; idx < total_active; idx++ )
	{
		mcActiveOccluderPoly *pActiveOccluderPoly = pviewer->GetActiveOccluderIndexPtr( idx );

		if( pActiveOccluderPoly->m_pOccPoly )// handle geom occluders (null ptr case) safely
		{
			continue;// dont support poly plane occluders right now, just geom ones.

			// Test occluder poly first.
			//			if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly->GetPlane().DistanceToPlane( center ) > -radius )
			//			{
			//				continue;// outside or crossing occlusion frustum, move to next active.
			//			}
		}

		int numplanes = pActiveOccluderPoly->m_num_planes;

		bool bFail = false;

		for( int plane=0; plane < numplanes; plane++)
		{
			Vector4	*pPlane = &pActiveOccluderPoly->m_occlusionPlanes[ plane ];
			// sub loop, test each corner in transformed box, first case that is outside fails

			Assert( !bFail );

			if( pPlane->DistanceToPlane( center ) > -radius )
			{
				bFail = true;// crossing, tag as failing test, break loop
				break;
			}
		}

		if( !bFail )
		{
			// todo: buffer occluded volumes here! (in a different color too!)
			if( OccludedVolumesBuffered_GetMode() )// bound buffering on for this test?
			{
				AddBufferedDebugDrawVolume( center, radius, id, pvscull_all_clear );
			}

			// todo: pass back occluder ID# to object so it can remember which occluder hid it last time, for possible reordering?

			// todo: track number of objects hidden per occluder, adjust occluder test order based on last frame's stats?

			return true; // occluded completely by one of our occluders
		}

	}// continue looping through rest of occluder plane sets.

	return false;// nothing occluded this object
}

#if 0
------------------------


int activeView		= GetActiveViewID();
s32 total_active	= m_numActiveOccluders[ GetActiveViewID() ];

if( !total_active )
return false;// none active!


#if __SPANBUFFER_ENABLED
#if __DEV
if( m_UseSpanBuffering && m_TestSpansBeforeObjectFrusta )
#endif
{
	if(	IsSphereSpanBufferOccluded( center, radius, false ) )
	{
		return true;// hidden!
	}
	// TODO: If we got here, its visible.. based on the cullable type it is, should we keep testing it with other methods.
}
#endif

for( int idx=0; idx < total_active; idx++ )
{
	bool bFail;

	if( m_activeOccluders[ activeView ][ idx ].m_pOccPoly )// handle geom occluders (null ptr case) safely
	{
		// Test occluder poly first.
		if( m_activeOccluders[ activeView ][ idx ].m_pOccPoly->GetPlane().DistanceToPlane( center ) > -radius )
		{
			continue;// outside or crossing occlusion frustum, move to next active.
		}
	}

	int numplanes = m_activeOccluders[ activeView ][ idx ].m_num_planes;

	bFail = false;

	for( int plane=0; plane < numplanes; plane++)
	{

		if( m_activeOccluders[ activeView ][ idx ].m_occlusionPlanes[ plane ].DistanceToPlane( center ) > -radius )
		{
			bFail = true;// crossing, tag as failing test, break loop
			break;
		}			
	}

	if( !bFail )
	{
#if __DEV
		m_OccludedCount++;// track

		if( m_AllowDebugDrawOfOccludedVolumes )
		{
#if __PS2
			if( sm_OccludedDrawBoundTotal < m_maxDebugOccluders )
			{
				if( OccludedVolumesBuffered_GetMode() )// bound buffering on for this test?
				{
					sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].center = center;
					sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].radius = radius;
					sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].debugDrawColor = GetOccludedVolumeDebugColor();
					sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].occludedViewID = activeView;
					sm_OccludedDrawBoundTotal++;
				}
			}
#else
			Displayf("#2 fix debug draw of occlusion bound ");
#if 0
			bool oldZWrite = RSTATE.GetZWriteEnable();
			bool oldZtest = RSTATE.GetZTestEnable();
			bool oldLightingEn = RSTATE.GetLighting();
			bool oldAlphaBlendEnable = RSTATE.GetAlphaBlendEnable();

			RSTATE.SetLighting( false );
			RSTATE.SetZTestEnable( false );
			RSTATE.SetZWriteEnable( true );
			RSTATE.SetAlphaBlendEnable( false );

			vglColor4f( 0.0f, 1.0f, 1.0f, 1.f );
			vglDrawSphere( radius, center , 5, true );

			RSTATE.SetZWriteEnable(oldZWrite);
			RSTATE.SetZTestEnable( oldZtest );// restore
			RSTATE.SetLighting( oldLightingEn );// restore
			RSTATE.SetAlphaBlendEnable( oldAlphaBlendEnable );// restore
#endif
#endif
		}
#endif

		return true; // occluded completely by one of our occluders
	}
}

#if __SPANBUFFER_ENABLED
#if __DEV
if( m_UseSpanBuffering && !m_TestSpansBeforeObjectFrusta )
#endif
{
	if(	IsSphereSpanBufferOccluded( center, radius, false ) )
	{
		return true;// hidden!
	}

	// TODO: If we got here, its visible.. based on the cullable type it is, should we keep testing it with other methods.
}
#endif

return false;// nothing occluded this object - yuck!
}
#endif



bool	rmOccluderSystem::AddOccluderToActiveView( rmOccludePolyhedron *pHedra, const bool bIsExternal )
{
#if __DEV
	if( ! AreOccludersEnabled() )
		return false;

	if( !m_UseOccluders )
		return false;
#endif

	int viewerID = GetActiveViewID();

	Assert( (viewerID >= 0) && (viewerID < RM_OCCLUDE_MAX_VIEWS) );

	int activeOccluders = m_numActiveOccluders[ viewerID ];// find free slot # to store occluder planes if its valid.

	if( activeOccluders >= RM_OCCLUDE_MAX_ACTIVE_OCCLUDERS )
	{
		//	Displayf(" ran out of room to add more active occluders " );
		return false;// hit limit, bail
	}

#if _OCCLUDER_TIMING
	int rejectedEdgeCount = 0;
	int backplaneCulled   = 0;
	int bottomEdgeReject  = 0;
#endif

	Vector3	eyepoint = this->GetViewPosition( viewerID );

#if 1//  __PS2

	Assert( pHedra );

	// psx2 occluder geometry
	int	ClipResults[64];// TODO: push onto scratchpad?
	int	FaceResults[64];

	{
		int	planesAdded = 0;// Clear count. As we add planes we increase the count - not all planes created by edges may need to be added (see below)

		//----------------------------------------
		// Distance based activation from eyepoint
		float rejectDist = pHedra->GetActiveDist();

		if( rejectDist <= 1.f )
		{
			rejectDist = RM_OCCLUDE_DEFAULT_ACTIVE_DIST;// use default active distance.
		}

		rejectDist *= m_OccluderDistanceActivationScale;

		// TODO: test #1: Frustum test sphere bound the encloses the polyhedron as quick test.

		// TODO: test #1A: proximity check of bound sphere data.
		//
		// Distance from eyepoint to center of polyhedron in 2-D
		// Sphere bounds are bloated as hell in 3 dimensions since they extend vertically so much


		if(!bIsExternal)// fixme: instances have to move the bounding sphere also!
		{
			Vector4 sphere4;
			pHedra->GetBoundSphere( &sphere4 );

			float activateDist = rejectDist+sphere4.w;// distance from center+radius+activate distance = distance to check from center to eyepoint
			float newRadius2   = activateDist * activateDist;
			float dx, dz;

			dx  = (eyepoint.x - sphere4.x);
			dx *= dx;
			dz  = (eyepoint.z - sphere4.z);			
			dz *= dz;

			if( newRadius2 < (dx + dz) )
			{
#if 0 // _OCCLUDER_DEBUGGING
				Warningf("spheredist>> rejectDist %f ,activateDist %f , eyepoint %f %f %f, sphere %f %f %f  %f" , rejectDist, activateDist,
					eyepoint.x, eyepoint.y, eyepoint.z, sphere4.x, sphere4.y, sphere4.z, sphere4.w );
#endif
				return false;// too far away
			}else{
				//Displayf("dx %f dz %f >> rejectDist %f", dx, dz, rejectDist );
			}
		}
		//----------------------------------------

		// test #2: test if all vertices are outside a single plane of the frustum (simple test)
		//bool rmOccludePolyhedron::TestOutsideSinglePlane( int *pClipFlags )//[ MAX_GEOM_TEST_VERTS ];)
		if( pHedra->TestOutsideSinglePlane( &ClipResults[0] ) )
		{
#if _OCCLUDER_DEBUGGING
			Displayf(" geom outside frustum " );
#endif
			return false;// totally outside frustum on a side, skip processing this one.
		}


		// RESET FREE ENTRY, moved this down so trivial reject cases above dont needlessly keep touching this part of memory.
		m_activeOccluders[ viewerID ][ activeOccluders ].m_num_planes	= 0;
		m_activeOccluders[ viewerID ][ activeOccluders ].m_pOccPoly		= NULL;

		// test #3: Figure out which faces on the geom face us.
		//			 To detect whether a polygon faces the viewer, use the dot product of the polygon's normal and direction to any of the polygon's vertices.
		//			 When this is less than 0, the polygon faces the viewer.)

		//Vector4 eyeToPoly;// define vector from eye position to 1st vertex on polygon in worldspace 

		int faceCount = pHedra->GetNumFacets();
		int fc;
		int edgeFlags		= 0U;// clear - this tracks unclipped edges which form the convex silhouette of the convex hull, based on visible face tests.
		int edgeReversed	= 0U;
#if _OCCLUDER_DEBUGGING
		Displayf(" geom %d faces %d ", g, faceCount );
#endif
		for( fc=0 ; fc < faceCount ; fc++ )
		{
			Vector4 polyNormal;
			Vector3 eyeToVert;
			float	dotP;
			int		vertIdx;
			int		facingpolys = 0;

			// hackery to remove faces easily
#if _OCCLUDER_IGNOREFACE_EDGEKILL
			if(0)// pHedra->GetFacetIgnoredFlag( fc ) )
			{
				dotP = -1.f;// force 'facing away' result.
			}
			else
#endif
			{
				// loop through faces.			
				vertIdx = pHedra->GetFacetVertIndex( fc, 2 );					// get 1st vertex in face.
				Vector3 ptOnPoly = pHedra->GetVertex( vertIdx );
#if _OCCLUDER_DEBUGGING
				Displayf(" face %d pt on poly %f %f %f", fc, ptOnPoly.x, ptOnPoly.y, ptOnPoly.z );
#endif
				eyeToVert.Subtract( eyepoint, ptOnPoly );	// create vector from point on poly towards the eyepoint			

				pHedra->GetPlane( fc, &polyNormal );							// get normal for this face.
#if _OCCLUDER_DEBUGGING
				//eyeToVert.Normalize();
				Displayf(" polyNormal %f %f %f ", polyNormal.x, polyNormal.y, polyNormal.z );
#endif
				// thankfully all we care about is the sign of the result so nothing has to be normalized.

#if		__DEV
				//				eyeToVert.Normalize();
#endif


				dotP = __dot3(  polyNormal.x, eyeToVert.x,
					polyNormal.y, eyeToVert.y,
					polyNormal.z, eyeToVert.z );
			}



			if (dotP < 0.f)
			{
				// facing away, we dont want this face.
				FaceResults[fc] = 0;

			}else{
				// facing us.
				FaceResults[fc] = 1;
				facingpolys++;

				// Get edges for this face which we can see, and do a bit toggle.
				// this results in edges that get toggled twice will revert to OFF (not a silhouette edge)

				// test #4: figure out which edges define the silhouette based on face visibility (based on face orientation and if all the vertices clip flags are off on the same side)
				if(1)
				{
					Vector4 facetPlane;
					//					facetPlane = pHedra->GetPlane( fc );
					pHedra->GetPlane( fc, &facetPlane );
					InsertPlaneAsActiveOccluder( planesAdded, m_activeOccluders[ viewerID ][ activeOccluders ], facetPlane );// Add this face as an occluder plane.
#if _OCCLUDER_DEBUGGING
					Displayf(" facetPlane %d of geom added as plane #%d in active occluder %d ", fc, planesAdded, activeOccluders );
#endif
					planesAdded++;// increment count of planes added.
				}


				int num_face_edges = pHedra->GetFacetNumVerts( fc );

				for(int fe=0; fe < num_face_edges; fe++ )
				{	
					int facetEdgeIdx = pHedra->GetFacetEdgeIndex( fc, fe );

					edgeFlags ^= (1 << facetEdgeIdx);// edge toggle

					// if edge is in 'set' state, update the reversed edge info.
					if( edgeFlags & (1 << facetEdgeIdx) )
					{
						// bit was set, not cleared, so record reverse flag setting.

						int flag = pHedra->GetFacetEdgeIndexReverseFlag( fc, fe );

						if( flag != 0 )
						{
							// flipped
							// set bit
							edgeReversed |= (1<<facetEdgeIdx);

						}else{
							// clear bit
							edgeReversed &= ~(1<<(facetEdgeIdx & ~0U)); 
						}
					}
				}
			}// end facing poly processing

#if _OCCLUDER_DEBUGGING
			Displayf(" polys facing us %d ", facingpolys );
#endif
		}// end face loop

#if _OCCLUDER_IGNOREFACE_EDGEKILL
		//---------------------------------------------------------------------------
		// edge killer (ignored faces only)
		{			
			faceCount = pHedra->GetNumFacets();

			for( fc=0 ; fc < faceCount ; fc++ )
			{
				int num_face_edges = pHedra->GetFacetNumVerts( fc );

				if( pHedra->GetFacetIgnoredFlag( fc ) )
				{
					Displayf("ingore face %d", fc );
					for(int fe=0; fe < num_face_edges; fe++ )
					{
						int facetEdgeIdx = pHedra->GetFacetEdgeIndex( fc, fe );
						// should this be clearing the bit instead of toggling it????
						//edgeFlags ^= (1 << facetEdgeIdx);// edge toggle												
						if( (edgeFlags & (1<<fe)) )
						{
							edgeFlags &= ~(1<<(facetEdgeIdx & ~0U)); // nuke edge
							Displayf("killed face %d edge %d", fc, facetEdgeIdx );
						}
					}
				}
			}
		}
		//---------------------------------------------------------------------------
#endif



#if _OCCLUDER_DEBUGGING
		for(int fe=0; fe < pHedra->GetNumEdges(); fe++ )
		{
			Displayf("edge %d   %d", fe,  (edgeFlags&(1<<fe)) );
		}
#endif

		// done looping through all faces now, edgeFlags holds a bitmask of which edges define the convex silhouette

		// test #4: add planes of visible faces

		// test #6: remove remaining silhouette edges that are outside the frustum
		int UnClippedEdges		= 0;
		int removedSilhouetteEdges	= 0;
		int num_face_edges			= pHedra->GetNumEdges();
		//int totalGeomEdges			= 0;
		// loop through all unique edges of geom
		for(int fe=0; fe < num_face_edges; fe++ )
		{
			if( (edgeFlags & (1<<fe)) )// bit set for this edge? then its a silhouette edge, otherwise its an interior edge.
			{
				// get edge vertices, then test if those vertices clip flags match each other (off the same side)
				int e0_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 0);//pHedra->m_pEdges[fe].GetEdge(0);
				int e1_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 1);// m_pEdges[fe].GetEdge(1);

				// If the union of the 2 edge vertices clip flags have any set bit left based on our mask test,
				//  then the edge is completely off the side of one of the clipping planes
				if( 0x555 & ((ClipResults[ e0_vertIdx ] & ClipResults[ e1_vertIdx ])))
				{
					//Displayf(" removed edge ");
					// clipped! dont add					
					removedSilhouetteEdges++;					
					continue;
				}

#if _OCCLUDER_DEBUGGING
				Displayf(" added edge ");
#endif

				Vector3 v3edge0;
				Vector3 v3edge1;

				if( (edgeReversed & (1<<fe)) )// was the edge inserted as reversed?
				{
#if _OCCLUDER_DEBUGGING
					Displayf(" rev %d %d ", e1_vertIdx, e0_vertIdx );
#endif
					// fallthough: if we get to here, the edge is potentially visible so we need to add it.
					v3edge0 = pHedra->GetVertex( e1_vertIdx );
					v3edge1 = pHedra->GetVertex( e0_vertIdx );
				}else{
#if _OCCLUDER_DEBUGGING
					Displayf(" nrm %d %d ", e0_vertIdx, e1_vertIdx );					
#endif
					// fallthough: if we get to here, the edge is potentially visible so we need to add it.
					v3edge0 = pHedra->GetVertex( e0_vertIdx );
					v3edge1 = pHedra->GetVertex( e1_vertIdx );
				}
#if _OCCLUDER_DEBUGGING	
				Displayf("e0 %f %f %f ", v3edge0.x, v3edge0.y, v3edge0.z );
				Displayf("e1 %f %f %f ", v3edge1.x, v3edge1.y, v3edge1.z );
#endif


				// NOTE: If the exporter/loader code lets an occluder in that could overrun the maximum allowed active planes, set the plane added count to 0 and break out.
				// The code below should all fall through, causing this occluder to not be added to the scene - CMC
				if( planesAdded == RM_OCCLUDE_MAX_OCCLUDER_POLYEDGES )
				{
					planesAdded = 0;
					Displayf("Occluder has too many active planes ptr %p 1st vertex ( %f, %f, %f )",
						pHedra,
						pHedra->GetVertex(0).x,
						pHedra->GetVertex(0).y,
						pHedra->GetVertex(0).z
						);
					break;// abort loop! - 
				}

				CreateEdgePlaneToActiveOccluder( planesAdded, m_activeOccluders[ viewerID ][ activeOccluders ], eyepoint,
					v3edge1, 
					v3edge0  );// winding order swapped....?

				planesAdded++;// increment count of planes added.

				UnClippedEdges++;// edge tracker.
			}
		}
#if _OCCLUDER_DEBUGGING
		Displayf(" UnClippedEdges %d ", UnClippedEdges );
#endif


		if( ( (planesAdded > 0) && m_UseSpanBuffering ))// !removedSilhouetteEdges
		{
			t_polyEdgeData	EdgeWorkBuffer[32];
			Vector3			v3srcVtx;
			sPoly			polygon;
			bool			bBackProjection = false;// track this to reject silhouettes if we aren't clipping our polys (yet)
			float			farthest_z		= 0.f;

			int UnClippedSilhEdges = pHedra->CreateSilhouettePolygonFromActiveEdges( &EdgeWorkBuffer[0], edgeFlags );
			// Get vertices, project and push them into n-gon structure for rasterization.
			// get very 1st vertex
			v3srcVtx		= pHedra->GetVertex( EdgeWorkBuffer[0].e0 );
			bBackProjection = TransformAndProject( &(polygon.verts[ 0 ].v4screenVtx), v3srcVtx );
			farthest_z		= polygon.verts[ 0 ].v4screenVtx.z;

			// get integer x,y for rasterizing (convert to fixed point)
			polygon.verts[ 0 ].x = (s32)(polygon.verts[ 0 ].v4screenVtx.x* 16.f);
			polygon.verts[ 0 ].y = (s32)(polygon.verts[ 0 ].v4screenVtx.y* 16.f);
			polygon.verts[ 0 ].prev = &polygon.verts[ UnClippedSilhEdges-1 ];
			polygon.verts[ 0 ].next = &polygon.verts[1];			

#if __POLYSPAN_DEBUG
			Displayf(" UnClippedSilhEdges %d", UnClippedSilhEdges );
#endif

			for(int e=1; e < UnClippedSilhEdges; e++)
			{
				// Get tailing edge of vertex edge, add it to list.
				v3srcVtx = pHedra->GetVertex( EdgeWorkBuffer[ e-1 ].e1 );
				bBackProjection = TransformAndProject( &polygon.verts[ e ].v4screenVtx, v3srcVtx );

				// get integer x,y for rasterizing (convert to fixed point)
				polygon.verts[ e ].x = (s32)(polygon.verts[ e ].v4screenVtx.x*16.f);
				polygon.verts[ e ].y = (s32)(polygon.verts[ e ].v4screenVtx.y*16.f);
				// store ptrs
				polygon.verts[ e ].prev = &polygon.verts[ e-1 ];//pPrev;
				polygon.verts[ e ].next = &polygon.verts[ ((e+1)%UnClippedSilhEdges) ];

				float curr_z = polygon.verts[ e ].v4screenVtx.z;

				if( curr_z > farthest_z )
				{
					farthest_z = curr_z;
				}

				if( bBackProjection )
				{
					Displayf("backprojected");
					break;// invalid projection, bail out.
				}
				//pPrev = &polygon.verts[ e ];// update previous
			}
#if 0 // __DEV
			for(int e=0; e < UnClippedSilhEdges; e++)
			{
				Displayf(" (e %d) x=%f y=%f ", e, (polygon.verts[ e ].x / 16.f), (polygon.verts[ e ].y /16.f) );
				Displayf(" prev %p   this %p   next %p", polygon.verts[ e ].prev, &polygon.verts[e], polygon.verts[ e ].next );
			}
#endif
			if(bBackProjection)
			{
				// bad poly, dont span buffer, or clip the damn thing.
#if __DEV
				// zero out, bad poly needs clipping.
				m_activeOccluders[ viewerID ][ activeOccluders ].num_silh_verts			= 0;
#endif
			}
			else
			{
				// TODO: FIXME: OPTIMIZE: screen clip poly!

#if __POLYSPAN_DEBUG
				Displayf( " farthest_z of inserted spans %f ", farthest_z );
#endif
				// TODO: optimize: test herre if bounding rectangle (around n-gon) to see if its offscreen, or occluded at some higher level (tile z-buffer?)


				// Since we'll be rendering from top-to-bottom, we'll need to find our
				//  top vertex, so scan through the list of vertices and locate that top
				//  vertex

				// locate the topmost vertex.
				sVert	*pTop		= &polygon.verts[ 0 ];
				s32		smallest_y	= polygon.verts[ 0 ].y;
				s32		largest_y	= polygon.verts[ 0 ].y;

				// loop through polygon edges, find highests edge( lowest y value)
				for(int pv=0; pv < UnClippedSilhEdges; pv++ )
				{
					if( smallest_y > polygon.verts[ pv ].y )
					{
						smallest_y	= polygon.verts[ pv ].y;	// update y
						pTop		= &polygon.verts[ pv ];		// update starting 'top' vertex						
					}

					if( largest_y < polygon.verts[ pv ].y )
					{
						largest_y = polygon.verts[ pv ].y;
					}
				}

				s32		viewport_rightEdge	= GetViewSSB( GetActiveViewID() )->GetSpanBufferWidth() -1;
				s32		viewport_bottomEdge = GetViewSSB( GetActiveViewID() )->GetSpanBufferHeight();
				viewport_bottomEdge = viewport_bottomEdge >> 1;// scale down by quantized amount
				viewport_bottomEdge--;// adjust


				// convert to integer
				smallest_y = smallest_y >> 4;
				largest_y  = largest_y >> 4;

				if( largest_y > viewport_bottomEdge )
				{
					largest_y = viewport_bottomEdge;
				}


#if __POLYSPAN_DEBUG
				Displayf( "smallest_y %d   largest_y %d ", smallest_y, largest_y   );
#endif
				bool b_rasterPoly = true;// assume yes
				// test if its zero area vertically, or off the top/bottom of screen entirely.
				if( (largest_y - smallest_y) <= 0)
					b_rasterPoly = false;

				if( largest_y < 0 )// off top
					b_rasterPoly = false;

				if( smallest_y > viewport_bottomEdge )// off top
					b_rasterPoly = false;

#if 0// __DEV
				//------------------------------------------------------------
				// debug draw
				if( planesAdded > 0 )
				{// make sure we added at least 1 plane.
					for( int ndbgvtxs=0; ndbgvtxs < UnClippedSilhEdges; ndbgvtxs++)
					{
						m_activeOccluders[ viewerID ][ activeOccluders ].num_silh_verts			= UnClippedSilhEdges;
						m_activeOccluders[ viewerID ][ activeOccluders ].m_silh_x[ ndbgvtxs ]	= polygon.verts[ ndbgvtxs ].x >> 4;
						m_activeOccluders[ viewerID ][ activeOccluders ].m_silh_y[ ndbgvtxs ]	= polygon.verts[ ndbgvtxs ].y >> 4;
					}
				}
				//------------------------------------------------------------
#endif

				if( b_rasterPoly )
				{
					// Later, we'll need to discern the left edge from the right edge, so
					//  make two copies of this top vertex.  These will reperesent the top of
					//  the left edge and top of the right edge.

					sVert	lTemp = *pTop; // copy 'top' vertex
					sVert	rTemp = *pTop; // copy 'top' vertex
					sVert	*lTop;
					sVert	*rTop;
					sVert	*lBot;
					sVert	*rBot;
					s32		currentY = pTop->y >> 4;
					bool	bClockwiseWinding;

					if( pTop->prev->x < pTop->next->x )
					{
						bClockwiseWinding = true;
					}else{
						bClockwiseWinding = false;
					}

					lTop  = &lTemp;
					rTop  = &rTemp;
					lBot  = GetNextVertex_LeftSide( bClockwiseWinding, pTop);
					rBot  = GetNextVertex_RightSide( bClockwiseWinding, pTop);

					// x step as we scan down for each edge
					s32 lDelta = calcDelta(lTop, lBot);
					s32 rDelta = calcDelta(rTop, rBot);

					s32		lHeight = (lBot->y - lTop->y); // Height of the left edge
					s32		rHeight = (rBot->y - rTop->y); // Height of the right edge
					s32		edge_L = lTop->x;
					s32		edge_R = rTop->x;

					//step edges until we find one thats not zero height
					while(lHeight < 16)
					{
						edge_L= lBot->x;// correct x position			
						lTop = lBot;
						lBot = GetNextVertex_LeftSide( bClockwiseWinding, lBot);//lBot->prev;//getPrev(lBot);
						lDelta = calcDelta(lTop, lBot);
						lHeight = lBot->y - lTop->y; // Height of the left edge
					}

					//step edges until we find one thats not zero height
					while(rHeight < 16)
					{
						edge_R= rBot->x;// correct x position
						rTop = rBot;
						rBot = GetNextVertex_RightSide( bClockwiseWinding, rBot);//rBot->next;//getNext(rBot);
						rDelta = calcDelta(rTop, rBot);
						rHeight = rBot->y - rTop->y; // Height of the right edge
					}

					s32	  TotalPixelsAdded = 0;
#if __POLYSPAN_DEBUG
					Displayf("currentY %d > viewport_rightEdge %d viewport_bottomEdge %d", currentY, viewport_rightEdge, viewport_bottomEdge );
#endif
					// Of the two edges, determine which edge spans the fewest scanlines
					//  (shortest from top-to-bottom).
					const int viewerID = GetActiveViewID();
					for(;currentY < largest_y; currentY++)
					{
						bool bSkipSpan= false;
						s32	span_start = edge_L >>4;
						s32	span_end   = edge_R >>4;
						if( span_start > span_end )							
						{
							Displayf(" backwards Span! ");
							bSkipSpan=true; // off top edge, clip
							/*
							//Assert(0);
							s32 swap = span_start;
							span_start = span_end;
							span_end   = swap;

							bClockwiseWinding =! bClockwiseWinding;
							*/
						}

						if( currentY <= 0 )
							goto skip_span;

						// clip to viewport edges, then evaluate length of span and ordering.

						if( currentY < 0 )
							bSkipSpan=true; // off top edge, clip

						if( currentY > viewport_bottomEdge )
							bSkipSpan=true; // off bottom edge, clip

						if( span_end < 0 )
							bSkipSpan=true;// off left edge, reject

						if( span_start > viewport_rightEdge )
							bSkipSpan=true;// off right edge, reject

						// SPAN CLIPPING
						if( span_start < 1 )
							span_start = 1;// left edge clip

						if( span_end > viewport_rightEdge )
							span_end = viewport_rightEdge;// right edge

						if( (span_end - span_start) == 0)
							bSkipSpan=true;// zero width post clipping

						if( !bSkipSpan )
						{
							//Assert( span_start < span_end );
							m_activeScanline = (s16)currentY;// update active scanline info so insertion code knows what span we are working on!
							// test+insertion.

							s32 pixelsAdded = InsertSpan( GetStartingSpanForViewScanline( viewerID , currentY ),
								(s16)span_start,
								(s16)span_end,
								farthest_z );
							if( pixelsAdded )
							{
								// added something to buffer. stack tracking goes here.
								TotalPixelsAdded += pixelsAdded;
								// NOTE: To more closely evaluate the number of potential rendered pixels this object contributes, account for any vertical/horizontal quantization.
								//			(e.g.) TotalPixelsAdded *= (realviewport_height / sbuffer_height);
								//					so a half-height sbuffer reports 50% of the screen coverage per span.
							}
						}

skip_span:

						edge_L += lDelta;							
						edge_R += rDelta;
						lHeight-=16;// 28.4 fixed point...
						rHeight-=16;// 28.4 fixed point...

						// Step to next edge check.
						//  To do this we compare each edge's height with 0 (remember that they were decremented in the loop).
						//  The ones that decremented all the way to 0 are done and can be stepped.
						if (lHeight < 16)
						{
							// get fraction, step edge
							edge_L = lBot->x;// correct x position
							lTop = lBot;
							lBot = GetNextVertex_LeftSide( bClockwiseWinding, lBot);//lBot->prev;//getPrev(lBot);
							lDelta = calcDelta(lTop, lBot);
							lHeight = lBot->y - lTop->y; // Height of the left edge
						}

						if (rHeight < 16)
						{
							edge_R= rBot->x;// correct x position
							rTop = rBot;
							rBot = GetNextVertex_RightSide( bClockwiseWinding, rBot);//rBot->next;//getNext(rBot);
							rDelta = calcDelta(rTop, rBot);
							rHeight = rBot->y - rTop->y; // Height of the right edge
						}
					}
				}// if( b_rasterPoly )
				else
				{
#if __DEV
					// zero out, bad poly needs clipping.
					m_activeOccluders[ viewerID ][ activeOccluders ].num_silh_verts			= 0;
#endif
				}
			}// !bBackProjection

		}// if( (planesAdded > 0) && m_UseSpanBuffering )
		else
		{
			m_activeOccluders[ viewerID ][ activeOccluders ].num_silh_verts			= 0;
		}

		//
		// test #?: TODO: test if eyepoint is contained within polyhedron, and spam a warning - also dont use that volume for occlusion!
		//

		if( planesAdded > 0 )
		{// make sure we added at least 1 plane.
			m_activeOccluders[ viewerID ][ activeOccluders ].m_num_planes	= planesAdded;
			m_activeOccluders[ viewerID ][ activeOccluders ].m_pOccPoly		= NULL;// different type data

			if( bIsExternal )
			{
				m_activeOccluders[ viewerID ][ activeOccluders ].m_type		= RM_OCCLUDE_OCCLUDERTYPE_GEOM_EXTERNAL;
			}
			else
			{
				m_activeOccluders[ viewerID ][ activeOccluders ].m_type		= RM_OCCLUDE_OCCLUDERTYPE_GEOM;
			}
			m_activeOccluders[ viewerID ][ activeOccluders ].m_Id			= (ptrdiff_t) pHedra; // store ptr instead of ID since its from an external source.

			// NOTE: Increase active occluder count here, its possible that all the edges were clipped out,
			//			but we still have a valid occluder poly covering the entire viewport that we want to test against still.
#if _OCCLUDER_DEBUGGING	
			Displayf(" planesAdded %d ", planesAdded );
			for(int t_plane=0; t_plane < planesAdded; t_plane++)
			{
				Vector4 *pPlane = &m_activeOccluders[ viewerID ][ activeOccluders ].m_occlusionPlanes[ t_plane ];
				Displayf(" plane %d  planeEq %f %f %f %f ", t_plane, pPlane->x, pPlane->y, pPlane->z, pPlane->w );
			}
#endif

			activeOccluders++;// track active count
			m_numActiveOccluders[ viewerID ] = activeOccluders;// update number of occluders
			return true;// we added an occluder
		}
	}
	//-----------------------------------------------------
#endif// #if __PS2 psx2 occluder geometry


	return false;// nothing added. clipped away completely, out of view or something.
}
#endif


























/*
void	rmOccluderSystem::ModifyViewer( const Vector3 &xxxpos, const Vector3 &viewDirection, const int viewerID, const Matrix34 *pCameraMatrix )
{
//-------------------------------------------
Vector3 eyepoint;// NOTE: Always copy position to support freeze mode, make sure code below doesnt use the input param either.

#if	__DEV
if( !sm_FreezeAllOccluderViews )
#else
if( 1 )
#endif
{
// TODO: Add check to verify direction of view is normalized?
m_views[ viewerID ].SetView( xxxpos, viewDirection );
m_views[ viewerID ].SetCamMatrix( pCameraMatrix );

eyepoint = xxxpos; // use input param position, not freezing eyepoints
}
else
{
// frozen mode, use last submitted viewpoint instead, ignoring input param
eyepoint = m_views[ viewerID ].GetViewPosition();
}
//-------------------------------------------

this->m_numActiveOccluders[ viewerID ] = 0;
}


#if __PS2 && __DEV
void	rmOccluderSystem::ViewSetupFinish( const int viewerID, const Matrix34 *pCameraMatrix )
#else
void	rmOccluderSystem::ViewSetupFinish( const int viewerID , const Matrix34 *   pCameraMatrix  )
#endif
{
#if __PS2 && __DEV
// all done getting our list of useful occluders for this frame, if the occluder camera is in frozen mode, update the render matrix data.
if( m_FreezeAllOccluderViews )
{
m_views[ viewerID ].SetCamMatrix( pCameraMatrix );
}
#endif
}
*/


/*
// same as above, but does not calc/return zdistance of the sphere center
inline u32 gfxViewport::InlineFastSphereVisCheck_ClipStatus(const Vector4 &sphere) const
{

#if  !__PS2
Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
float radius = sphere.w;

Vector4 distancesLRNZ;                                 // left, right, near and z distance
Vector4 distancesBTF;                                  // bottom, top and far distances

// fully outside?
distancesLRNZ.Dot(center,sm_FastCullMtx[0]); 
if (distancesLRNZ.x > radius) return cullOutside;      // left
if (distancesLRNZ.y > radius) return cullOutside;      // right
if (distancesLRNZ.z > radius) return cullOutside;      // near

distancesBTF.Dot(center,sm_FastCullMtx[1]);				   // many times we don't need to do this one...
if (distancesBTF.x  > radius) return cullOutside;      // bottom
if (distancesBTF.y  > radius) return cullOutside;      // top
if (distancesBTF.z  > radius) return cullOutside;      // far

// Does it touch a clipping plane?
if (distancesLRNZ.z > -radius) return cullClippedZNear; // near
if (distancesLRNZ.x > -radius) return cullClipped;      // left
if (distancesLRNZ.y > -radius) return cullClipped;      // right
if (distancesBTF.x  > -radius) return cullClipped;      // bottom
if (distancesBTF.y  > -radius) return cullClipped;      // top
if (distancesBTF.z  > -radius) return cullClipped;		// far
return cullInside;
#else  // this is the pSX2 version. It should be all inlined macrocode and assembly
register int clipStatus;

asm __volatile__("
lqc2			vf8, 0x0(%1)    # load the sphere definition
lqc2			vf4, 0x0(%2)    # load the first cull transform mtx
lqc2			vf5, 0x10(%2)
lqc2			vf6, 0x20(%2)
lqc2			vf7, 0x30(%2)
vmulax.xyzw     ACC,   vf4,vf8  # transform the sphere center to get Left Right and Near distances, plus actual Z distance
vmadday.xyzw    ACC,   vf5,vf8 
vmaddaz.xyzw    ACC,   vf6,vf8 
vmaddw.xyzw     vf12,  vf7,vf0  # use vf0, since w is not 1 in the sphere vector4 
lqc2			vf4, 0x40(%2)	# load the second cull transform mtx
lqc2			vf5, 0x50(%2)
lqc2			vf6, 0x60(%2)
lqc2			vf7, 0x70(%2)
vmulax.xyzw     ACC,  vf4,vf8	# do second transform to get Top, Bottom and Far distance
vmadday.xyzw    ACC,  vf5,vf8 
vmaddaz.xyzw    ACC,  vf6,vf8 
vmaddw.xyzw     vf13, vf7,vf0   # use vf0, since w is not 1 in the sphere vector4 
ctc2			$0,$vi18 		# clear clip status...
vclipw.xyz		vf12xyz,vf8w	# use clip to do LRN compares
vclipw.xyz		vf13xyz,vf8w	# use clip to do BTF compares

VNOP							# make sure clip is done (these should pipeline with about CPU instructions)
VNOP
VNOP
VNOP		

cfc2			%0,$vi18 		# get the clip status

": "=r" (clipStatus) : "r" (&sphere) , "r" (sm_FastCullMtx));

// clipStatus:  -N +N -R +R -L +L -F +F -T +T -B +B
//  - means dist is < -radius,
//  + means dist is > radius
return clipStatus;

//	if      (clipStatus & 0x555)            return cullOutside;      // far enough outside at least one plane to be rejected
//	else if ((clipStatus & 0xAAA) == 0xAAA) return cullInside;       // we are far enough inside all planes
//	else if (!(clipStatus & 0x800))         return cullClippedZNear; // clipped the near plane (important for some things)
//	else                                    return cullClipped;      // clipped one of the sides or far,  really "if ((clipCodes & 0x2AA) != 0x2AA)", but that is the only case left!

#endif
}
*/











#if 0

void	rmOccluderSystem::BeginView_SystemOccludersXXXXXXXXXXXXXXXXX( const Vector3 &xxxpos, const Vector3 &viewDirection, const int viewerID, const Matrix34 *pCameraMatrix )
{
	Assert( (viewerID >= 0) && (viewerID < rmOcclude::MAX_VIEWS) );
	Assert( pCameraMatrix );

	//Displayf(" viewerID %d ModifyViewer %f %f %f",  viewerID, xxxpos.x, xxxpos.y, xxxpos.z );

#if __DEV
	if( rmOccluderSystem::GetInstance() )
	{
		rmOccluderSystem::GetInstance()->SetOccludedVolumeDebugColor( mkfrgba( 0.2f, 0.9f, 0.9f, 1.f ) );
	}
#endif

#if _OCCLUDER_TIMING
	int total_planes = 0;
#endif

	// Make sure a dataset is loaded, the code needs to handle the abscence of an occluder dataset without crashing.
	if( !m_OccluderPolyArray)
	{
		m_numActiveOccluders[ viewerID ] = 0;// make sure active count is set to 0
#if _OCCLUDER_WARNINGS
		Warningf("No active database of occluders!");
#endif
		// WARNING: We no longer exit - occluder system may be queried for its view matrix information still!!!
		// WARNING: We no longer exit - occluder system may be queried for its view matrix information still!!!
		// WARNING: We no longer exit - occluder system may be queried for its view matrix information still!!!
#if 0
		return;// bail out, nothing left to do.
#endif
	}

	//-------------------------------------------
	Vector3 eyepoint;// NOTE: Always copy position to support freeze mode, make sure code below doesnt use the input param either.

	if( !m_FreezeAllOccluderViews )
	{
		// TODO: Add check to verify direction of view is normalized?
		m_views[ viewerID ].SetView( xxxpos, viewDirection );
		m_views[ viewerID ].SetCamMatrix( pCameraMatrix );

		eyepoint = xxxpos; // use input param position, not freezing eyepoints
	}
	else
	{
		// frozen mode, use last submitted viewpoint instead, ignoring input param
		eyepoint = m_views[ viewerID ].GetViewPosition();
	}
	//-------------------------------------------

	if( !m_UseOccluders )// occluders disabled - set active count to zero and bail out.
	{
		this->m_numActiveOccluders[ viewerID ] = 0;
		return;
	}


#if _OCCLUDER_TIMING
	TIME.Update();
	Timer T;
#endif


	//-----------------------------------------------------
	// Search for best active occluders for this viewpoint
	int	activeOccluders = 0;

	// create an interest point placed along the vector that defines the look direction.
	Vector3	interestPoint;
	interestPoint = viewDirection;
	interestPoint.Normalize();// make sure its normalized
	interestPoint.Scale( 10.f );// scale it a bit (translating)
	interestPoint.Add( eyepoint );// add viewer postion so its in worldspace now.

#if _OCCLUDER_TIMING
	int rejectedEdgeCount = 0;
	int backplaneCulled   = 0;
	int bottomEdgeReject  = 0;
#endif


	//	const float	rejectDist  = 35.f;
	//	const float	rejectDist2 = rejectDist*rejectDist;
#if __PS2
	// psx2 occluder geometry



	//-----------------------------------------------------
	// OCCLUDER POLYHEDRON
	// Definition: A set of convex n-gons defining a convex volume
	int geomcount = m_numOccluderGeoms;
	int	ClipResults[32];// TODO: push onto scratchpad?
	int	FaceResults[32];

	for(int g=0; g < geomcount; g++)
	{
		rmOccludePolyhedron *pHedra = &m_OccluderGeomArray[g];

		//		int numverts	= pHedra->GetNumVerts();
		int	planesAdded = 0;// Clear count. As we add planes we increase the count - not all planes created by edges may need to be added (see below)

		if( activeOccluders >= rmOcclude::MAX_ACTIVE_OCCLUDERS )
			break;// hit limit, bail

		//----------------------------------------
		// Distance based activation from eyepoint
		float rejectDist = pHedra->GetActiveDist();

		if( rejectDist <= 1.f )
		{
			rejectDist = rmOcclude::DEFAULT_ACTIVE_DIST;// use default active distance.
		}

		// TODO: test #1: Frustum test sphere bound the encloses the polyhedron as quick test.

		// TODO: test #1A: proximity check of bound sphere data.
		//
		// Distance from eyepoint to center of polyhedron in 2-D
		// Sphere bounds are bloated as hell in 3 dimensions since they extend vertically so much
		{
			Vector4 sphere4;
			pHedra->GetBoundSphere( &sphere4 );

			float activateDist = rejectDist+sphere4.w;// distance from center+radius+activate distance = distance to check from center to eyepoint
			float newRadius2   = activateDist * activateDist;
			float dx, dz;

			dx  = (eyepoint.x - sphere4.x);
			dx *= dx;
			dz  = (eyepoint.z - sphere4.z);			
			dz *= dz;

			if( newRadius2 < (dx + dz) )
			{
#if 0 // _OCCLUDER_DEBUGGING
				Warningf("spheredist>> rejectDist %f ,activateDist %f , eyepoint %f %f %f, sphere %f %f %f  %f" , rejectDist, activateDist,
					eyepoint.x, eyepoint.y, eyepoint.z, sphere4.x, sphere4.y, sphere4.z, sphere4.w );
#endif
				continue;// too far!
			}else{
				//Displayf("dx %f dz %f >> rejectDist %f", dx, dz, rejectDist );
			}
		}
		//----------------------------------------

		// test #2: test if all vertices are outside a single plane of the frustum (simple test)
		//bool rmOccludePolyhedron::TestOutsideSinglePlane( int *pClipFlags )//[ MAX_GEOM_TEST_VERTS ];)
		if( pHedra->TestOutsideSinglePlane( &ClipResults[0] ) )
		{
#if _OCCLUDER_DEBUGGING
			Displayf(" geom outside frustum " );
#endif
			continue;// totally outside frustum on a side, skip processing this one.
		}


		// RESET FREE ENTRY, moved this down so trivial reject cases above dont needlessly keep touching this part of memory.
		m_activeOccluders[ viewerID ][ activeOccluders ].m_num_planes	= 0;
		m_activeOccluders[ viewerID ][ activeOccluders ].m_pOccPoly		= NULL;


		// test #3: Figure out which faces on the geom face us.
		//			 To detect whether a polygon faces the viewer, use the dot product of the polygon's normal and direction to any of the polygon's vertices.
		//			 When this is less than 0, the polygon faces the viewer.)

		//Vector4 eyeToPoly;// define vector from eye position to 1st vertex on polygon in worldspace 

		int faceCount = pHedra->GetNumFacets();
		int fc;
		int edgeFlags		= 0U;// clear - this tracks unclipped edges which form the convex silhouette of the convex hull, based on visible face tests.
		int edgeReversed	= 0U;
#if _OCCLUDER_DEBUGGING
		Displayf(" geom %d faces %d ", g, faceCount );
#endif
		for( fc=0 ; fc < faceCount ; fc++ )
		{
			Vector4 polyNormal;
			Vector3 eyeToVert;
			float	dotP;
			int		vertIdx;
			int		facingpolys = 0;

			// hackery to remove faces easily
#if _OCCLUDER_IGNOREFACE_EDGEKILL
			if(0)// pHedra->GetFacetIgnoredFlag( fc ) )
			{
				dotP = -1.f;// force 'facing away' result.
			}
			else
#endif
			{
				// loop through faces.			
				vertIdx = pHedra->GetFacetVertIndex( fc, 2 );					// get 1st vertex in face.
				Vector3 ptOnPoly = pHedra->GetVertex( vertIdx );
#if _OCCLUDER_DEBUGGING
				Displayf(" face %d pt on poly %f %f %f", fc, ptOnPoly.x, ptOnPoly.y, ptOnPoly.z );
#endif
				eyeToVert.Subtract( eyepoint, ptOnPoly );	// create vector from point on poly towards the eyepoint			

				pHedra->GetPlane( fc, &polyNormal );							// get normal for this face.
#if _OCCLUDER_DEBUGGING
				//eyeToVert.Normalize();
				Displayf(" polyNormal %f %f %f ", polyNormal.x, polyNormal.y, polyNormal.z );
#endif
				// thankfully all we care about is the sign of the result so nothing has to be normalized.
				dotP = __dot3(  polyNormal.x, eyeToVert.x,
					polyNormal.y, eyeToVert.y,
					polyNormal.z, eyeToVert.z );
			}



			if (dotP < 0.f)
			{
				// facing away, we dont want this face.
				FaceResults[fc] = 0;

			}else{
				// facing us.
				FaceResults[fc] = 1;
				facingpolys++;

				// Get edges for this face which we can see, and do a bit toggle.
				// this results in edges that get toggled twice will revert to OFF (not a silhouette edge)

				// test #4: figure out which edges define the silhouette based on face visibility (based on face orientation and if all the vertices clip flags are off on the same side)

				if(1)
				{
					Vector4 facetPlane;
					//					facetPlane = pHedra->GetPlane( fc );
					pHedra->GetPlane( fc, &facetPlane );
					InsertPlaneAsActiveOccluder( planesAdded, m_activeOccluders[ viewerID ][ activeOccluders ], facetPlane );// Add this face as an occluder plane.
#if _OCCLUDER_DEBUGGING
					Displayf(" facetPlane %d of geom added as plane #%d in active occluder %d ", fc, planesAdded, activeOccluders );
#endif
					planesAdded++;// increment count of planes added.
				}


				int num_face_edges = pHedra->GetFacetNumVerts( fc );

				for(int fe=0; fe < num_face_edges; fe++ )
				{	
					int facetEdgeIdx = pHedra->GetFacetEdgeIndex( fc, fe );

					edgeFlags ^= (1 << facetEdgeIdx);// edge toggle

					// if edge is in 'set' state, update the reversed edge info.
					if( edgeFlags & (1 << facetEdgeIdx) )
					{
						// bit was set, not cleared, so record reverse flag setting.

						int flag = pHedra->GetFacetEdgeIndexReverseFlag( fc, fe );

						if( flag != 0 )
						{
							// flipped
							// set bit
							edgeReversed |= (1<<facetEdgeIdx);

						}else{
							// clear bit
							edgeReversed &= ~(1<<(facetEdgeIdx & ~0U)); 
						}
					}
				}
			}// end facing poly processing

#if _OCCLUDER_DEBUGGING
			Displayf(" polys facing us %d ", facingpolys );
#endif
		}// end face loop

#if _OCCLUDER_IGNOREFACE_EDGEKILL
		//---------------------------------------------------------------------------
		// edge killer (ignored faces only)
		{			
			faceCount = pHedra->GetNumFacets();

			for( fc=0 ; fc < faceCount ; fc++ )
			{
				int num_face_edges = pHedra->GetFacetNumVerts( fc );

				if( pHedra->GetFacetIgnoredFlag( fc ) )
				{
					Displayf("ingore face %d", fc );
					for(int fe=0; fe < num_face_edges; fe++ )
					{
						int facetEdgeIdx = pHedra->GetFacetEdgeIndex( fc, fe );
						// should this be clearing the bit instead of toggling it????
						//edgeFlags ^= (1 << facetEdgeIdx);// edge toggle												
						if( (edgeFlags & (1<<fe)) )
						{
							edgeFlags &= ~(1<<(facetEdgeIdx & ~0U)); // nuke edge
							Displayf("killed face %d edge %d", fc, facetEdgeIdx );
						}
					}
				}
			}
		}
		//---------------------------------------------------------------------------
#endif



#if _OCCLUDER_DEBUGGING
		for(int fe=0; fe < pHedra->GetNumEdges(); fe++ )
		{
			Displayf("edge %d   %d", fe,  (edgeFlags&(1<<fe)) );
		}
#endif

		// done looping through all faces now, edgeFlags holds a bitmask of which edges define the convex silhouette

		// test #4: add planes of visible faces

		// test #6: remove remaining silhouette edges that are outside the frustum
		int UnClippedEdges		= 0;
		int removedSilhouetteEdges	= 0;
		int num_face_edges			= pHedra->GetNumEdges();
		//int totalGeomEdges			= 0;
		// loop through all unique edges of geom
		for(int fe=0; fe < num_face_edges; fe++ )
		{
			if( (edgeFlags & (1<<fe)) )// bit set for this edge? then its a silhouette edge, otherwise its an interior edge.
			{
				// get edge vertices, then test if those vertices clip flags match each other (off the same side)
				int e0_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 0);//pHedra->m_pEdges[fe].GetEdge(0);
				int e1_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 1);// m_pEdges[fe].GetEdge(1);

				// If the union of the 2 edge vertices clip flags have any set bit left based on our mask test,
				//  then the edge is completely off the side of one of the clipping planes
				if( 0x555 & ((ClipResults[ e0_vertIdx ] & ClipResults[ e1_vertIdx ])))
				{
					//Displayf(" removed edge ");
					// clipped! dont add					
					removedSilhouetteEdges++;					
					continue;
				}

#if _OCCLUDER_DEBUGGING
				Displayf(" added edge ");
#endif

				Vector3 v3edge0;
				Vector3 v3edge1;

				if( (edgeReversed & (1<<fe)) )// was the edge inserted as reversed?
				{
#if _OCCLUDER_DEBUGGING
					Displayf(" rev %d %d ", e1_vertIdx, e0_vertIdx );
#endif
					// fallthough: if we get to here, the edge is potentially visible so we need to add it.
					v3edge0 = pHedra->GetVertex( e1_vertIdx );
					v3edge1 = pHedra->GetVertex( e0_vertIdx );
				}else{
#if _OCCLUDER_DEBUGGING
					Displayf(" nrm %d %d ", e0_vertIdx, e1_vertIdx );					
#endif
					// fallthough: if we get to here, the edge is potentially visible so we need to add it.
					v3edge0 = pHedra->GetVertex( e0_vertIdx );
					v3edge1 = pHedra->GetVertex( e1_vertIdx );
				}
#if _OCCLUDER_DEBUGGING	
				Displayf("e0 %f %f %f ", v3edge0.x, v3edge0.y, v3edge0.z );
				Displayf("e1 %f %f %f ", v3edge1.x, v3edge1.y, v3edge1.z );
#endif

				// NOTE: If the exporter/loader code lets an occluder in that could overrun the maximum allowed active planes, set the plane added count to 0 and break out.
				// The code below should all fall through, causing this occluder to not be added to the scene - CMC
				if( planesAdded == rmOcclude::MAX_OCCLUDER_POLYEDGES )
				{
					planesAdded = 0;
					Displayf("Occluder has too many active planes ptr %p 1st vertex ( %f, %f, %f )",
						pHedra,
						pHedra->GetVertex(0).x,
						pHedra->GetVertex(0).y,
						pHedra->GetVertex(0).z
						);
					break;// abort loop! - 
				}

				CreateEdgePlaneToActiveOccluder( planesAdded, m_activeOccluders[ viewerID ][ activeOccluders ], eyepoint,
					v3edge1, 
					v3edge0  );// winding order swapped....?

				planesAdded++;// increment count of planes added.

				UnClippedEdges++;// edge tracker.
			}
		}
#if _OCCLUDER_DEBUGGING
		Displayf(" UnClippedEdges %d ", UnClippedEdges );
#endif


		if( ( (planesAdded > 0) && m_UseSpanBuffering ))// !removedSilhouetteEdges
		{
			t_polyEdgeData	EdgeWorkBuffer[32];
			Vector3			v3srcVtx;
			sPoly			polygon;
			bool			bBackProjection = false;// track this to reject silhouettes if we aren't clipping our polys (yet)
			float			farthest_z		= 0.f;

			int UnClippedSilhEdges = pHedra->CreateSilhouettePolygonFromActiveEdges( &EdgeWorkBuffer[0], edgeFlags );
			// Get vertices, project and push them into n-gon structure for rasterization.
			// get very 1st vertex
			v3srcVtx		= pHedra->GetVertex( EdgeWorkBuffer[0].e0 );
			bBackProjection = TransformAndProject( &(polygon.verts[ 0 ].v4screenVtx), v3srcVtx );
			farthest_z		= polygon.verts[ 0 ].v4screenVtx.z;

			// get integer x,y for rasterizing (convert to fixed point)
			polygon.verts[ 0 ].x = (s32)(polygon.verts[ 0 ].v4screenVtx.x* 16.f);
			polygon.verts[ 0 ].y = (s32)(polygon.verts[ 0 ].v4screenVtx.y* 16.f);
			polygon.verts[ 0 ].prev = &polygon.verts[ UnClippedSilhEdges-1 ];
			polygon.verts[ 0 ].next = &polygon.verts[1];			

#if __POLYSPAN_DEBUG
			Displayf(" UnClippedSilhEdges %d", UnClippedSilhEdges );
#endif

			for(int e=1; e < UnClippedSilhEdges; e++)
			{
				// Get tailing edge of vertex edge, add it to list.
				v3srcVtx = pHedra->GetVertex( EdgeWorkBuffer[ e-1 ].e1 );
				bBackProjection = TransformAndProject( &polygon.verts[ e ].v4screenVtx, v3srcVtx );

				// get integer x,y for rasterizing (convert to fixed point)
				polygon.verts[ e ].x = (s32)(polygon.verts[ e ].v4screenVtx.x*16.f);
				polygon.verts[ e ].y = (s32)(polygon.verts[ e ].v4screenVtx.y*16.f);
				// store ptrs
				polygon.verts[ e ].prev = &polygon.verts[ e-1 ];//pPrev;
				polygon.verts[ e ].next = &polygon.verts[ ((e+1)%UnClippedSilhEdges) ];

				float curr_z = polygon.verts[ e ].v4screenVtx.z;

				if( curr_z > farthest_z )
				{
					farthest_z = curr_z;
				}

				if( bBackProjection )
				{
					Displayf("backprojected");
					break;// invalid projection, bail out.
				}

				//pPrev = &polygon.verts[ e ];// update previous
			}
#if 0 // __DEV
			for(int e=0; e < UnClippedSilhEdges; e++)
			{
				Displayf(" (e %d) x=%f y=%f ", e, (polygon.verts[ e ].x / 16.f), (polygon.verts[ e ].y /16.f) );
				Displayf(" prev %p   this %p   next %p", polygon.verts[ e ].prev, &polygon.verts[e], polygon.verts[ e ].next );
			}
#endif
			if(bBackProjection)
			{
				// bad poly, dont span buffer, or clip the damn thing.
#if __DEV
				// zero out, bad poly needs clipping.
				m_activeOccluders[ viewerID ][ activeOccluders ].num_silh_verts			= 0;
#endif
			}
			else
			{
				// TODO: FIXME: OPTIMIZE: screen clip poly!

#if __POLYSPAN_DEBUG
				Displayf( " farthest_z of inserted spans %f ", farthest_z );
#endif
				// TODO: optimize: test herre if bounding rectangle (around n-gon) to see if its offscreen, or occluded at some higher level (tile z-buffer?)


				// Since we'll be rendering from top-to-bottom, we'll need to find our
				//  top vertex, so scan through the list of vertices and locate that top
				//  vertex

				// locate the topmost vertex.
				sVert	*pTop		= &polygon.verts[ 0 ];
				s32		smallest_y	= polygon.verts[ 0 ].y;
				s32		largest_y	= polygon.verts[ 0 ].y;

				// loop through polygon edges, find highests edge( lowest y value)
				for(int pv=0; pv < UnClippedSilhEdges; pv++ )
				{
					if( smallest_y > polygon.verts[ pv ].y )
					{
						smallest_y	= polygon.verts[ pv ].y;	// update y
						pTop		= &polygon.verts[ pv ];		// update starting 'top' vertex						
					}

					if( largest_y < polygon.verts[ pv ].y )
					{
						largest_y = polygon.verts[ pv ].y;
					}
				}

				s32		viewport_rightEdge	= GetViewSSB( GetActiveViewID() )->GetSpanBufferWidth() -1;
				s32		viewport_bottomEdge = GetViewSSB( GetActiveViewID() )->GetSpanBufferHeight();
				viewport_bottomEdge = viewport_bottomEdge >> 1;// scale down by quantized amount
				viewport_bottomEdge--;// adjust


				// convert to integer
				smallest_y = smallest_y >> 4;
				largest_y  = largest_y >> 4;

				if( largest_y > viewport_bottomEdge )
				{
					largest_y = viewport_bottomEdge;
				}


#if __POLYSPAN_DEBUG
				Displayf( "smallest_y %d   largest_y %d ", smallest_y, largest_y   );
#endif
				bool b_rasterPoly = true;// assume yes
				// test if its zero area vertically, or off the top/bottom of screen entirely.
				if( (largest_y - smallest_y) <= 0)
					b_rasterPoly = false;

				if( largest_y < 0 )// off top
					b_rasterPoly = false;

				if( smallest_y > viewport_bottomEdge )// off top
					b_rasterPoly = false;

#if 0// __DEV
				//------------------------------------------------------------
				// debug draw
				if( planesAdded > 0 )
				{// make sure we added at least 1 plane.
					for( int ndbgvtxs=0; ndbgvtxs < UnClippedSilhEdges; ndbgvtxs++)
					{
						m_activeOccluders[ viewerID ][ activeOccluders ].num_silh_verts			= UnClippedSilhEdges;
						m_activeOccluders[ viewerID ][ activeOccluders ].m_silh_x[ ndbgvtxs ]	= polygon.verts[ ndbgvtxs ].x >> 4;
						m_activeOccluders[ viewerID ][ activeOccluders ].m_silh_y[ ndbgvtxs ]	= polygon.verts[ ndbgvtxs ].y >> 4;
					}
				}
				//------------------------------------------------------------
#endif

				if( b_rasterPoly )
				{
					// Later, we'll need to discern the left edge from the right edge, so
					//  make two copies of this top vertex.  These will reperesent the top of
					//  the left edge and top of the right edge.

					sVert	lTemp = *pTop; // copy 'top' vertex
					sVert	rTemp = *pTop; // copy 'top' vertex
					sVert	*lTop;
					sVert	*rTop;
					sVert	*lBot;
					sVert	*rBot;
					s32		currentY = pTop->y >> 4;
					bool	bClockwiseWinding;

					if( pTop->prev->x < pTop->next->x )
					{
						bClockwiseWinding = true;
					}else{
						bClockwiseWinding = false;
					}

					lTop  = &lTemp;
					rTop  = &rTemp;
					lBot  = GetNextVertex_LeftSide( bClockwiseWinding, pTop);
					rBot  = GetNextVertex_RightSide( bClockwiseWinding, pTop);

					// x step as we scan down for each edge
					s32 lDelta = calcDelta(lTop, lBot);
					s32 rDelta = calcDelta(rTop, rBot);

					s32		lHeight = (lBot->y - lTop->y); // Height of the left edge
					s32		rHeight = (rBot->y - rTop->y); // Height of the right edge
					s32		edge_L = lTop->x;
					s32		edge_R = rTop->x;

					//step edges until we find one thats not zero height
					while(lHeight < 16)
					{
						edge_L= lBot->x;// correct x position			
						lTop = lBot;
						lBot = GetNextVertex_LeftSide( bClockwiseWinding, lBot);//lBot->prev;//getPrev(lBot);
						lDelta = calcDelta(lTop, lBot);
						lHeight = lBot->y - lTop->y; // Height of the left edge
					}

					//step edges until we find one thats not zero height
					while(rHeight < 16)
					{
						edge_R= rBot->x;// correct x position
						rTop = rBot;
						rBot = GetNextVertex_RightSide( bClockwiseWinding, rBot);//rBot->next;//getNext(rBot);
						rDelta = calcDelta(rTop, rBot);
						rHeight = rBot->y - rTop->y; // Height of the right edge
					}

					s32	  TotalPixelsAdded = 0;
#if __POLYSPAN_DEBUG
					Displayf("currentY %d > viewport_rightEdge %d viewport_bottomEdge %d", currentY, viewport_rightEdge, viewport_bottomEdge );
#endif
					// Of the two edges, determine which edge spans the fewest scanlines
					//  (shortest from top-to-bottom).
					const int viewerID = GetActiveViewID();
					for(;currentY < largest_y; currentY++)
					{
						bool bSkipSpan= false;
						s32	span_start = edge_L >>4;
						s32	span_end   = edge_R >>4;
						if( span_start > span_end )							
						{
							Displayf(" backwards Span! ");
							bSkipSpan=true; // off top edge, clip
							/*
							//Assert(0);
							s32 swap = span_start;
							span_start = span_end;
							span_end   = swap;

							bClockwiseWinding =! bClockwiseWinding;
							*/
						}

						if( currentY <= 0 )
							goto skip_span;

						// clip to viewport edges, then evaluate length of span and ordering.

						if( currentY < 0 )
							bSkipSpan=true; // off top edge, clip

						if( currentY > viewport_bottomEdge )
							bSkipSpan=true; // off bottom edge, clip

						if( span_end < 0 )
							bSkipSpan=true;// off left edge, reject

						if( span_start > viewport_rightEdge )
							bSkipSpan=true;// off right edge, reject

						// SPAN CLIPPING
						if( span_start < 1 )
							span_start = 1;// left edge clip

						if( span_end > viewport_rightEdge )
							span_end = viewport_rightEdge;// right edge

						if( (span_end - span_start) == 0)
							bSkipSpan=true;// zero width post clipping

						if( !bSkipSpan )
						{
							//Assert( span_start < span_end );
							m_activeScanline = (s16)currentY;// update active scanline info so insertion code knows what span we are working on!
							// test+insertion.

							s32 pixelsAdded = InsertSpan( GetStartingSpanForViewScanline( viewerID , currentY ),
								(s16)span_start,
								(s16)span_end,
								farthest_z );
							if( pixelsAdded )
							{
								// added something to buffer. stack tracking goes here.
								TotalPixelsAdded += pixelsAdded;
								// NOTE: To more closely evaluate the number of potential rendered pixels this object contributes, account for any vertical/horizontal quantization.
								//			(e.g.) TotalPixelsAdded *= (realviewport_height / sbuffer_height);
								//					so a half-height sbuffer reports 50% of the screen coverage per span.
							}
						}

skip_span:

						edge_L += lDelta;							
						edge_R += rDelta;
						lHeight-=16;// 28.4 fixed point...
						rHeight-=16;// 28.4 fixed point...

						// Step to next edge check.
						//  To do this we compare each edge's height with 0 (remember that they were decremented in the loop).
						//  The ones that decremented all the way to 0 are done and can be stepped.
						if (lHeight < 16)
						{
							// get fraction, step edge
							edge_L = lBot->x;// correct x position
							lTop = lBot;
							lBot = GetNextVertex_LeftSide( bClockwiseWinding, lBot);//lBot->prev;//getPrev(lBot);
							lDelta = calcDelta(lTop, lBot);
							lHeight = lBot->y - lTop->y; // Height of the left edge
						}

						if (rHeight < 16)
						{
							edge_R= rBot->x;// correct x position
							rTop = rBot;
							rBot = GetNextVertex_RightSide( bClockwiseWinding, rBot);//rBot->next;//getNext(rBot);
							rDelta = calcDelta(rTop, rBot);
							rHeight = rBot->y - rTop->y; // Height of the right edge
						}
					}
				}// if( b_rasterPoly )
				else
				{
#if __DEV
					// zero out, bad poly needs clipping.
					m_activeOccluders[ viewerID ][ activeOccluders ].num_silh_verts			= 0;
#endif
				}
			}// !bBackProjection

		}// if( (planesAdded > 0) && m_UseSpanBuffering )
		else
		{
			m_activeOccluders[ viewerID ][ activeOccluders ].num_silh_verts			= 0;
		}

		//
		// test #?: TODO: test if eyepoint is contained within polyhedron, and spam a warning - also dont use that volume for occlusion!
		//
		Assert( planesAdded <= rmOcclude::MAX_OCCLUDER_POLYEDGES );

		if( planesAdded > 0 )
		{// make sure we added at least 1 plane.
			m_activeOccluders[ viewerID ][ activeOccluders ].m_num_planes	= planesAdded;
			m_activeOccluders[ viewerID ][ activeOccluders ].m_pOccPoly		= NULL;// different type data
			m_activeOccluders[ viewerID ][ activeOccluders ].m_type			= rmOcclude::OCCLUDERTYPE_GEOM;
			m_activeOccluders[ viewerID ][ activeOccluders ].m_Id			= g;

			// NOTE: Increase active occluder count here, its possible that all the edges were clipped out,
			//			but we still have a valid occluder poly covering the entire viewport that we want to test against still.
#if _OCCLUDER_DEBUGGING	
			Displayf(" planesAdded %d ", planesAdded );
			for(int t_plane=0; t_plane < planesAdded; t_plane++)
			{
				Vector4 *pPlane = &m_activeOccluders[ viewerID ][ activeOccluders ].m_occlusionPlanes[ t_plane ];
				Displayf(" plane %d  planeEq %f %f %f %f ", t_plane, pPlane->x, pPlane->y, pPlane->z, pPlane->w );
			}
#endif

			activeOccluders++;// track active count
		}
	}
	//-----------------------------------------------------
#endif// #if __PS2 psx2 occluder geometry


#if 0// occluder n-gons



	//-----------------------------------------------------
	// For now just brute force search through the entire dataset
	// TODO: place polys in quadtree or some other spatial partitioning tree
	for(int n=0; n < m_numOccluderPolys; n++)
	{
		int numverts	= m_OccluderPolyArray[n].GetNumVerts();
		int	planesAdded = 0;// Clear count. As we add planes we increase the count - not all planes created by edges may need to be added (see below)

		// NOTE: you can hack here to force less than the max number of occluders to be active......

#if 0 // __DEV
		if( activeOccluders >= gMaxOccluder

#else
		if( activeOccluders >= rmOcclude::MAX_ACTIVE_OCCLUDERS )
			break;// hit limit, bail
#endif

		//----------------------------------------
		// Distance based activation from eyepoint
		float rejectDist = m_OccluderPolyArray[n].GetActiveDist();

		if( rejectDist <= 1.f )
		{
			rejectDist = rmOcclude::DEFAULT_ACTIVE_DIST;// use default active distance.
		}

		if( ! m_OccluderPolyArray[n].SphereIntersectTestHalfspace( eyepoint, rejectDist ) )
		{
			//Warningf("spheredist reject %d -" , n );
			continue;// too far
		}
		//----------------------------------------


		//----------------------------------------
		{
#if	_OCCLUDER_REJECT_LOWEDGES
			if( eyepoint.y >= -0.01f )// above street level
			{
				// reject polys where their highest edge point on the Y axis is less than 1.f
				float highest_Y = -99999.f;

				for( int k=0; k < numverts;k++ )
				{
					highest_Y = Max( highest_Y, m_OccluderPolyArray[n].GetVertex( k ).y );
				}

				if( ( highest_Y < eyepoint.y ) && ( highest_Y < 1.f ) )
					continue;// reject, occluder edge is not higher than 1m above eyepoint

				// TODO: cache this data?
			}
#endif
		}
		//----------------------------------------

		// TODO: Add view direction to plane normal test,
		//        factor in polygonal surface area to reject polys with little coverage or polys that are perpendicular to the view direction.

		rmOccludePoly	*pOccluderPoly = &m_OccluderPolyArray[n];

		m_activeOccluders[ viewerID ][ activeOccluders ].m_num_planes = 0;// zero out


		//--------------------------------------------------
		// Test to reject polygons behind the viewer position and behind the direction they are looking
		{
			int outcount = 0;
			for( int nv=0; nv < numverts;nv++ )
			{
				Vector3 eyeToVert;

				eyeToVert.Subtract( eyepoint, m_OccluderPolyArray[n].GetVertex( nv ));

				if( eyeToVert.Dot( viewDirection ) >= 0.f )
					break;// a vert lies in the forward halfspace, dont test anymore since it cant be all behind us

				outcount++;// fell through to here, vert is behind us
			}

			if( outcount == numverts )
			{
#if _OCCLUDER_TIMING
				backplaneCulled++;
#endif
				continue;// all out behind us, bail out
			}
		}
		//--------------------------------------------------


		// TODO: test occluder plane against frustum, if its all off one side of the frustum then dont add it.
#if __PS2
		if( m_OccluderPolyArray[n].TestOutsideSinglePlane() )
		{
			// TODO: add a different cull category here???
#if _OCCLUDER_TIMING
			backplaneCulled++;
#endif
			continue;// outside one of the planes, dont add even the occluder poly plane, or we get strange halfspace clip outs in front of us.
		}
#endif

		// TODO: Get dist to occluder plane, then clip occluder poly to frustum, and remove edges that are clipped off the original so we dont create a test plane with them.

		for( int k=0; k < numverts;k++ )
		{
			// TODO: Check if each edge is visible within the view frustum first,
			//			if no part of the edge lies within the frustum then the
			//			plane created by that edge does not need to be tested against for occlusion!

			// Calculate each extruded plane


			// Indices to verts that create polygon edges.
			int edge0 = k;
			int edge1;

			if( k == numverts-1 )
			{
				edge1 = 0;// wrap back to first vert 
			}else{
				edge1 = k+1;// get next
			}

			Vector3 v3edge0 = m_OccluderPolyArray[n].GetVertex( edge0 );
			Vector3 v3edge1 = m_OccluderPolyArray[n].GetVertex( edge1 );

#if	_OCCLUDER_REMOVE_UNDERGROUNDEDGE
			if( (v3edge0.y < solid_groundplane) && (v3edge1.y < solid_groundplane) )
			{
#if _OCCLUDER_TIMING
				bottomEdgeReject++;
#endif
				continue;// Never add this edge since it extends into the solid halfspace of the world (ground)

			}			
#endif



			// TODO: Add check to see if an edge is completely outside a single frustum plane, if so then dont add that plane....
#if 1// __PS2
#if	__DEV
			if (m_UseOccluderFrustumFusion)
#endif
			{
				// 1) If an edge is behind us, then skip it....
				// 2) If an edge is in front or crossing the near clip plane, test to see if its outside completely on the left/right/top/bottom sides
				Vector4 v4cyl_0;
				Vector4 v4cyl_1;

				v4cyl_0.Set( v3edge0.x, v3edge0.y, v3edge0.z, 0.001f );
				v4cyl_1.Set( v3edge1.x, v3edge1.y, v3edge1.z, 0.001f );

				if( PIPE.GetViewport()->InlineFast2SpheresOutsideSinglePlane( v4cyl_0, v4cyl_1 )	)	
				{
					// Edge is completely out on a single plane - skip adding it.
#if _OCCLUDER_TIMING
					rejectedEdgeCount++;
#endif
					continue;
				}
			}
#endif

			CreateEdgePlaneToActiveOccluder( planesAdded, m_activeOccluders[ viewerID ][ activeOccluders ], eyepoint,
				v3edge0, // m_OccluderPolyArray[n].GetVertex( edge1 ),
				v3edge1  //m_OccluderPolyArray[n].GetVertex( edge0 )
				);// winding order swapped....

			planesAdded++;// increment count of planes added.
		}

#if _OCCLUDER_TIMING
		total_planes += planesAdded; // track how many total planes we are testing for this view...
#endif

		m_activeOccluders[ viewerID ][ activeOccluders ].m_num_planes = planesAdded;

		// All done, if we added any planes, link the ptr of the occluder poly to the active one.
		/*
		if( planesAdded > 0 )
		{			
		m_activeOccluders[ viewerID ][ activeOccluders ].m_pOccPoly = pOccluderPoly;			
		}
		*/
		// Always link occluder poly since its one we always test against, number of additional planes is allowed to be 0
		// planes would be zero if the occluder poly is in our face and none of its edges can be seen.

		// NOTE: If we ever wanted to animate occluders we need to remove the linkback to the original occluder
		//			Active occluders would have to store *all* planes including the occluders so we could modify the source occluder poly
		//			while the occluder system queries the active poly list.
		m_activeOccluders[ viewerID ][ activeOccluders ].m_pOccPoly		= pOccluderPoly;
		m_activeOccluders[ viewerID ][ activeOccluders ].m_type			= rmOcclude::OCCLUDERTYPE_NGON;


		// NOTE: Increase active occluder count here, its possible that all the edges were clipped out,
		//			but we still have a valid occluder poly covering the entire viewport that we want to test against still.
		activeOccluders++;
	}
#endif



#if _OCCLUDER_DEBUGGING
	Displayf(" activeOccluders %d ", activeOccluders );
#endif



#if _OCCLUDER_TIMING
	{
		float ms = T.MsTime();
		/*
		char buf[256];
		formatf( buf,sizeof(buf),"%5.2fms - pvstest - nView %d", ms, nView );
		gfxDrawFont( 64, 140+(20*nView) ,buf);
		*/
		Warningf(" ModifyViewer - time %5.2fms - view %d - total planes (%d) rejected edges (%d) ber(%d) - bk(%d) -",
			ms, viewerID, total_planes,
			rejectedEdgeCount, bottomEdgeReject, backplaneCulled );

	}
#endif


	Assert( activeOccluders <= rmOcclude::MAX_ACTIVE_OCCLUDERS ); // make sure we didnt buffer overrun

	//Warningf(" activeOccluders for viewer %d = %d ", viewerID, activeOccluders );
	m_numActiveOccluders[ viewerID ] = activeOccluders;// register number of active occluders with system

	// All done getting our list of useful occluders for this frame, if the occluder camera is in frozen mode, update the render matrix data.
	if( m_FreezeAllOccluderViews )
	{
		m_views[ viewerID ].SetCamMatrix( pCameraMatrix );
	}
}



#if 0

bool	rmOccluderSystem::IsAABoxOccluded( const Vector4 *pv4Min, const Vector4 *pv4Max, const Matrix34 *pMtxLocalToWorld, const bool bFrustumTest, const bool bUseEE )
{
	/*
	#if __DEV
	if( !g_bFrustumTestAABoxes && !g_bOccludeTestAABoxes )
	{
	return false; // dont do anything just force failed result
	}
	#endif
	*/

	int	viewerID =  GetActiveViewID();
	s32 total_active = m_numActiveOccluders[ GetActiveViewID() ];

	//Assert( pMtxLocalToWorld );// todo: handle case of NULL and treat it as identity instead

	if( !total_active )
		return false;// none active!


	Matrix44	*pMtx0 = GetViewerCullMtxPtr_0( viewerID );
	Matrix44	*pMtx1 = GetViewerCullMtxPtr_1( viewerID );

	if( bUseEE )
	{
		// vu0 version!
	}


	// synthesize 8 corners in local space, transform to worldspace then test against occluders
	Vector3	worldCorners[8];// put this on SPR?

	int viewID = GetActiveViewID();


	//	MOVE	verts0,	AABoxLocalMax ; verts0 = 456
	//	MOVE	verts4,	AABoxLocalMin ; verts4 = 123
	worldCorners[0].x = pv4Min->x;
	worldCorners[0].y = pv4Min->y;
	worldCorners[0].z = pv4Min->z;

	worldCorners[4].x = pv4Max->x;
	worldCorners[4].y = pv4Max->y;
	worldCorners[4].z = pv4Max->z;
	//	worldCorners[0] = *pv4Min;
	//	worldCorners[4] = *pv4Max;

	//	worldCorners[0].w = 1.f;
	//	worldCorners[4].w = 1.f;


	//	MOVE.xyw	verts5, verts4	; verts5 = 12x
	worldCorners[5].x = worldCorners[4].x;
	worldCorners[5].y = worldCorners[4].y;
	//	worldCorners[5].w = worldCorners[4].w;

	//	MOVE.z		verts5, verts0	; verts5 = 126
	worldCorners[5].z = worldCorners[0].z;

	//	MOVE.xzw	verts6, verts4	; verts6 = 1x3
	worldCorners[6].x = worldCorners[4].x;
	worldCorners[6].z = worldCorners[4].z;
	//	worldCorners[6].w = worldCorners[4].w;

	//	MOVE.y		verts6, verts0	; verts6 = 153
	worldCorners[6].y = worldCorners[0].y;

	//	MOVE.yzw	verts7, verts0  ; verts7 = x56
	worldCorners[7].y = worldCorners[0].y;
	worldCorners[7].z = worldCorners[0].z;
	//	worldCorners[7].w = worldCorners[0].w;

	//	MOVE.x		verts7, verts4	; verts7 = 156
	worldCorners[7].x = worldCorners[4].x;


	//	MOVE.xyw	verts1, verts0	; verts1 = 45x
	worldCorners[1].x = worldCorners[0].x;
	worldCorners[1].y = worldCorners[0].y;
	//	worldCorners[1].w = worldCorners[0].w;

	//	MOVE.z		verts1, verts4	; verts1 = 453
	worldCorners[1].z = worldCorners[4].z;

	//	MOVE.xzw	verts2, verts0	; verts2 = 4x6
	worldCorners[2].x = worldCorners[0].x;
	worldCorners[2].z = worldCorners[0].z;
	//	worldCorners[2].w = worldCorners[0].w;

	//	MOVE.y		verts2, verts4	; verts2 = 426
	worldCorners[2].y = worldCorners[4].y;

	//	MOVE.yzw	verts3, verts4  ; verts3 = x23
	worldCorners[3].y = worldCorners[4].y;
	worldCorners[3].z = worldCorners[4].z;
	//	worldCorners[3].w = worldCorners[4].w;

	//	MOVE.x		verts3, verts0	; verts3 = 423
	worldCorners[3].x = worldCorners[0].x;

	if( pMtxLocalToWorld )// if we get passed NULL, we skip the transform assuming the aabox is using an identity matrix, which also means it stays axis aligned!
	{
		// transform box corners
		for(int i=0; i < 8 ; i++)
		{
			pMtxLocalToWorld->Transform(worldCorners[i]);
		}
	}


	const	float radius = 0.f;// testing points, not spheres so it should be zero.




	// corners are in worldspace now, frustum test if requested
	//if( g_bFrustumTestAABoxes && bFrustumTest )
	if( bFrustumTest )
	{
		// todo: implement
		// if outside frustum, return 'true' occluded result.

		Vector4 distancesLRNZ;                                 // left, right, near and z distance
		Vector4 distancesBTF;                                  // bottom, top and far distances
		Vector4 center;//(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!

		//
		// NOTE: We have to use the non scratchpad matrices because we dont have enough space to store all views there.
		//
		bool	bInside = false;
		u32	AllCorners_AndResult_clipStatus = ~0U;// logical product of all corner tests

		// NOTE: first time a corner's bitflags results are 'inside' all planes we can break out of the loop since we know its not going to be 
		for(int i=0; i < 8 ; i++)
		{
			// setup test point
			center.x = worldCorners[i].x;
			center.y = worldCorners[i].y;
			center.z = worldCorners[i].z;
			center.w = 1.f;

			u32	clipStatus = 0U;// reset for each vertex

			// fully outside?
			//				distancesLRNZ.Dot(center, sm_rmOccludeFastCullMtx[ viewerID << 1 ] );//test first 3 planes.
			distancesLRNZ.Dot(center, *pMtx0 );//test first 3 planes.

			//> float zDist = distancesLRNZ.w; // zdist to viewer (TODO: track min/max zdist of object if we processed all corners?)

			if (distancesLRNZ.x > radius)
				clipStatus |= 1<<6;
			else if (distancesLRNZ.x < -radius)
				clipStatus |= 1<<7;

			if (distancesLRNZ.y > radius)
				clipStatus |= 1<<8;
			else if (distancesLRNZ.y < -radius)
				clipStatus |= 1<<9;

			if (distancesLRNZ.z > radius)
				clipStatus |= 1<<10;
			else if (distancesLRNZ.z < -radius)
				clipStatus |= 1<<11;

			//				distancesBTF.Dot(center, sm_rmOccludeFastCullMtx[ (viewerID << 1)+1 ] );//test next 3 planes.
			distancesBTF.Dot(center, *pMtx1 );//test next 3 planes.

			if (distancesBTF.x > radius)
				clipStatus |= 1<<0;
			else if (distancesBTF.x < -radius)
				clipStatus |= 1<<1;

			if (distancesBTF.y > radius)
				clipStatus |= 1<<2;
			else if (distancesBTF.y < -radius)
				clipStatus |= 1<<3;

			if (distancesBTF.z > radius)
				clipStatus |= 1<<4;
			else if (distancesBTF.z < -radius)
				clipStatus |= 1<<5;

			AllCorners_AndResult_clipStatus &= clipStatus;

			if ((clipStatus & 0xAAA) == 0xAAA)// found a vertex that is completely inside, that means there's no way this thing is outside the frustum, break out of loop.
			{
				bInside = true;
				break;// 1 vertex is inside, break out, its a waste to test anymore. Only thing that can hide this now is an occluder.
			}
		}


		if( !bInside )
		{
			//didnt break out of loop, check out the clip flags for the box

			if( AllCorners_AndResult_clipStatus & 0x555 )
			{
				//all box corners vertices clip flags 'and'ed together show all the vertices were outside at least 1 single plane.
				return true;// occluded! (outside frustum
			}
		}
	}

	if(0)// !g_bOccludeTestAABoxes )
	{
		// no occlusion testing on boxes set, and the above frustum test failed, so its visible.
		return false;// not occluded!
	}
	else
	{
		for( int idx=0; idx < total_active; idx++ )
		{
			if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly )// handle geom occluders (null ptr case) safely
			{
				continue;// dont support poly plane occluders right now, just geom ones.
				// Test occluder poly first.
				//			if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly->GetPlane().DistanceToPlane( center ) > -radius )
				//			{
				//				continue;// outside or crossing occlusion frustum, move to next active.
				//			}
			}

			int numplanes = m_activeOccluders[ viewID ][ idx ].m_num_planes;

			bool bFail = false;

			for( int plane=0; plane < numplanes; plane++)
			{
				Vector4	*pPlane = &m_activeOccluders[ viewID ][ idx ].m_occlusionPlanes[ plane ];
				// sub loop, test each corner in transformed box, first case that is outside fails

				Assert( !bFail );

				if( pPlane->DistanceToPlane( worldCorners[0] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[4] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}


				if( pPlane->DistanceToPlane( worldCorners[1] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[2] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[3] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[5] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[6] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[7] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				/*
				if( m_activeOccluders[ viewID ][ idx ].m_occlusionPlanes[ plane ].DistanceToPlane( center ) > -radius )
				{
				bFail = true;// crossing, tag as failing test, break loop
				break;
				}			
				*/
			}

			if( !bFail )
			{
				// todo: buffer occluded volumes here! (in a different color too!)

				// todo: pass back occluder ID# to object so it can remember which occluder hid it last time, for possible reordering?

				// todo: track number of objects hidden per occluder, adjust occluder test order based on last frame's stats?
				return true; // occluded completely by one of our occluders
			}

		}// continue looping through rest of occluder plane sets.

	}

	return false;// nothing occluded this object - yuck!
}

//} // namespace rage
#endif

#endif

