#ifndef RM_OCCLUDE_OCCLUDER_H
#define RM_OCCLUDE_OCCLUDER_H

#include "atl/array.h"		// atArray support
#include "vector/color32.h"	// debug color
#include "system/ipc.h"		// multithread support.

#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "grcore/viewport.h"	// using grcCullStatus enum right now... ick
//#include "parsercore/utils.h"  // for bit set/clear template code...

// Trade memory for speed, if we dont store the planes of geoms then we need to compute them on the fly
// I don't think ill ever enable this since it imposes more restictions on the occlusion data access and causes issues
// with supporting occluders that are defined in local space and have a local->world xform. [3/13/2006 ccoffin]
#define	__RM_OCCLUDE_STORED_GEOM_PLANES		(HACK_GTA4 && 1)

namespace rage {
	class	bkBank;
	class	Color32;
	class	fiAsciiTokenizer;
	enum	grcCullStatus;
};	// namespace rage


class	SpriteSpanBuffer;
class	PvsViewer;
class	mcActiveOccluderPoly;
class	rmOccludePoly;


using namespace rage;

// Note: *Currently* these enums are used as indices in arrays of matrix and other c-array or atArrays!
enum  rmPvsViewType {
	pvsvt_PVS				=0U,
	pvsvt_Render			=1U,
	pvsvt_NumViewTypes		=2U
};

// Note: *in the future* these enums may be used as indices in arrays of matrix and other c-array or atArrays!
enum  rmPvsMatrixType { // all are 4x4 matrices
	pvsmt_World				=0U,
	pvsmt_View				,
	pvsmt_Projection		,
	pvsmt_Screen			,
	pvsmt_ModelView			,
	pvsmt_Composite			,
	pvsmt_FullComposite		,

	pvsmt_NumTypes
};


enum  rmPvsBoundType {
	pvsbt_Null				=0U<<0,
	pvsbt_Vertex			=1U<<0,
	pvsbt_Box				=1U<<1,
	pvsbt_Capsule			=1U<<2,
	pvsbt_Sphere			=1U<<3
};

// PURPOSE: To control debug drawing visualizations.
//
// 'pvsdd' = pvs 'debugdraw'
//
enum  rmPvsDebugDrawModeBits {

	pvsdd_Empty						=0U<<0,

	pvsdd_DrawOccluders				=1U<<0,
	pvsdd_DrawUmbra					=1U<<1,
	pvsdd_DrawCulledBounds			=1U<<2,
	pvsdd_DrawRemovedEdges			=1U<<3, // draws removed edges from occlusion hulls that lie outside the frustum.
	pvsdd_DrawSpanSilhouettes		=1U<<4, // draw screen space silhouettes that get pushed to the sbuffer system.
	pvsdd_DrawFrustum				=1U<<5,

	// 'pvsdd_DefaultSetting' is subject to change! for internal development use.
	pvsdd_DefaultSetting			=(
										pvsdd_DrawOccluders |
										pvsdd_DrawUmbra |
										pvsdd_DrawCulledBounds |
										pvsdd_DrawRemovedEdges |
										pvsdd_DrawFrustum
										),

	pvsdd_AllBits_mask				=~0U
};

//-----------------------------------------------------------------------------
// REFERENCE: old style bitflag format from ps2 vu clip unit
	//	if      (clipStatus & 0x555)            return cullOutside;      
	//	else if ((clipStatus & 0xAAA) == 0xAAA) return cullInside;       
	//	else if (!(clipStatus & 0x800))         return cullClippedZNear; 
	//	else                                    return cullClipped;      
	//
	// clipStatus:  -N +N -R +R -L +L -F +F -T +T -B +B
	//  - means dist is < -radius,
	//  + means dist is > radius

// NOTE: these are subject to change!

enum  rmPvsCullModeBits {

	pvscm_Empty						=0U<<0,

	// pvs techniques start
	pvscm_FrustumTest				=1U<<0,		// frustum cull the object - if not set, its assumed to be within/crossing the frustum.
	pvscm_OccludeTest				=1U<<1,		// occlusion test (if frustum test was specified, occlusion test happens after)

	// TODO: IMPLEMENT -> pvscv_SpanBufferTest			=1U<<2,

	// TODO: setup priority bits for frustum/span/worldspace occlusion culling techniques.


	// IMPORTANT: These are used to let you easily get around cases where you dont want objects inside occluders to become hidden.
	// This lets you use more aggressively sized volumes to hide stuff that isnt crossing/contained the occluder 'behind' it.
	// (e.g.) A common case where you'd use this is when you have a perfect box shaped object that is fitted with an AABOX and the occluder is the exact size as the bound+rendered
	// geometry.
	// Without using these flags, that object would never be visible because it would occlude itself (or flicker due to numerical inaccuracy) // cmc 3/27/2006
	pvscm_ExcludeObjectsClippingOccluderFrontfaces	= 1U<<20,//todo: implement!
	pvscm_ExcludeObjectsClippingOccluderBackfaces	= 1U<<21,//todo: implement!
	pvscm_ExcludeObjectsContainedInOccluder			= 1U<<22,//todo: implement!
	// todo: we should return some clip bitflags based on in/out/crossing results of the above flags!

	// multibitmasks
	pvscm_EnabledCulling_mask		=(pvscm_FrustumTest | pvscm_OccludeTest | pvscm_ExcludeObjectsContainedInOccluder),// frustum/occlude test

	// might remove these or change these. These should not be default params/options in the rmOcclude library right now.
	pvscm_ForceOccludeTest			=1U<<23,	// even if frustum trivially culls the object first, perform occlusion cull test.
	pvscm_EarlyInsideFrustumTest	=1U<<24,	// Skip testing remainder of all vertices of an aabox/hull/etc once we encounter one that lies inside the frustum.
		// WARNING: using 'pvscm_EarlyInsideFrustumTest' will cause the 'pvscull_frust_plane' to be invalid since they wont all be set due to the early loop exit!.
		// if you have any code looking at the frust cull plane bits you should not be enabling this!
		// WARNING: temporal coherence culling wont work with 'pvscm_EarlyInsideFrustumTest' enabled either.
#if __DEV
	pvscm_FrustumTest_FrozenAndRenderViews = 1U<<25, // when in frozen pvs view mode, frustum culling for certain functions clips against 2 frusta.
		//This is mainly done so things dont slow down horribly. Not something done in a final game obviously.
#else
//	pvscm_FrustumTest_FrozenAndRenderViews = pvscm_FrustumTest,
#endif


	pvscm_AllBits_mask				=~0U
};


enum  rmPvsCullStatusBits {
#if (__XENON || __PS3)
	// vcmpbfp friendly bit layout
	pvscull_frust_plane_posB	=1U<<23,
	pvscull_frust_plane_negB	=1U<<22,
	pvscull_frust_plane_posT	=1U<<15,
	pvscull_frust_plane_negT	=1U<<14,
	pvscull_frust_plane_posF	=1U<<7,
	pvscull_frust_plane_negF	=1U<<6,
	pvscull_frust_plane_posL	=1U<<21,
	pvscull_frust_plane_negL	=1U<<20,
	pvscull_frust_plane_posR	=1U<<13,
	pvscull_frust_plane_negR	=1U<<12,
	pvscull_frust_plane_posN	=1U<<5,
	pvscull_frust_plane_negN	=1U<<4,
#else
	pvscull_frust_plane_posB	=1U<<0,
	pvscull_frust_plane_negB	=1U<<1,
	pvscull_frust_plane_posT	=1U<<2,
	pvscull_frust_plane_negT	=1U<<3,
	pvscull_frust_plane_posF	=1U<<4,
	pvscull_frust_plane_negF	=1U<<5,
	pvscull_frust_plane_posL	=1U<<6,
	pvscull_frust_plane_negL	=1U<<7,
	pvscull_frust_plane_posR	=1U<<8,
	pvscull_frust_plane_negR	=1U<<9,
	pvscull_frust_plane_posN	=1U<<10,
	pvscull_frust_plane_negN	=1U<<11,
#endif

	pvscull_all_clear			=0,

	pvscull_frust_plane_outside_mask	= 
		pvscull_frust_plane_posB | pvscull_frust_plane_posT |
		pvscull_frust_plane_posF | pvscull_frust_plane_posL |
		pvscull_frust_plane_posR | pvscull_frust_plane_posN,

	pvscull_frust_plane_inside_mask	= 
		pvscull_frust_plane_negB | pvscull_frust_plane_negT |
		pvscull_frust_plane_negF | pvscull_frust_plane_negL |
		pvscull_frust_plane_negR | pvscull_frust_plane_negN,

	pvscull_frust_plane_bits_mask =
		pvscull_frust_plane_outside_mask | 
		pvscull_frust_plane_inside_mask,

	pvscull_CrossingFrustum		=1U<<24, // 'inside partially' to be implemented.
	pvscull_OutsideFrustum		=1U<<25,
	pvscull_InsideFrustum		=1U<<26,
	pvscull_Occluded			=1U<<27,
	pvscull_NotOccluded			=1U<<28,

#if __DEV
	pvscull_pvs_is_frozen		=1U<<29,// TODO: IMPLEMENT! //  [3/20/2006 ccoffin] allows the user to determine rendering behavior for visibility results based on frozen viewpoints.
#endif

	pvscull_AllBits_mask		=~0U
};



// convert cull status bits to a bool result of 'visible' or 'not visible'
namespace rmOcclude
{
	inline bool	IsVisible( rmPvsCullStatusBits eBits )
	{
		if( ((eBits & pvscull_OutsideFrustum) == pvscull_OutsideFrustum) ||
			((eBits & pvscull_Occluded) == pvscull_Occluded) )
		{
			return false;
		}

		return true;
	}
};
//-----------------------------------------------------------------------------

//typedef	sysIpcThreadId	pvsQueryId;// need for multi threading support!
typedef s32				pvsViewerID;
typedef u32				pvsEdgeData;
typedef u32				pvsVtxClipFlags;
typedef u32				pvsFaceClipFlags;

// file format version.
#define		RM_OCCLUDE_FILE_VERSION				3
#define		RM_OCCLUDE_FILE_VERSION_2			4

// Refers to max number of planes created from edges for a single occlusion object.
// this number could be higher, but you're approaching a state of diminishing returns by allowing this to be a very high number.
// the more faces an occlusion hull is defined by, the more potential edges that must be extruded to create an occlusion umbra 	[3/13/2006 ccoffin]
// 

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// NOTE: WARNING: Currently work buffers/types that reference this data are designed to hold 32 bits.
//					You must modify 'pvsEdgeData' type size if you change this.
#define		RM_OCCLUDE_MAX_OCCLUDER_POLYEDGES	16 - 2 * HACK_GTA4
#define		RM_OCCLUDE_MAX_OCCLUDER_FACES		30	// assumes we still store occluder mesh data as indexed data.
#define		RM_OCCLUDE_MAX_OCCLUDER_VERTICES	30	// assumes we still store occluder mesh data as indexed data.
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


// Need to loosen these restrictions up quite a bit. These serve more as a 'sanity check' on input/processing if we move from fixed arrays to atArrays or something.
// using fixed sized arrays makes things a bit more portable though to memory limited architectures such as vu/cell processors that have local memory. [3/13/2006 ccoffin]
#define		RM_OCCLUDE_MAX_VIEWS					8
#define		RM_OCCLUDE_MAX_ACTIVE_OCCLUDERS			(64 - 32 * HACK_GTA4)

// default we used in mc2+mc3 - should be used as reference. 1.f = 1 meter.
#define		RM_OCCLUDE_DEFAULT_ACTIVE_DIST			40.f

// span buffer values.
#define		RM_OCCLUDE_SPANBUFF_MAX_VERTICAL_RES	1200// if you are one of the 5 people that want to run higher res than this vertically... you need to give me the monitor that can handle it.
#define		RM_OCCLUDE_SPANBUFF_MAX_SPANS			4000// @12 bytes a piece
#define		RM_OCCLUDE_SPANBUFF_MAX_ZDEPTH			FLT_MAX

// change this to a bitfield later as we add more types that can be internal/externally managed?
#define		RM_OCCLUDE_OCCLUDERTYPE_GEOM			0
#define		RM_OCCLUDE_OCCLUDERTYPE_NGON			1
#define		RM_OCCLUDE_OCCLUDERTYPE_GEOM_EXTERNAL	2

#include "occluder_sbuffer.h"
#include "occluder_view.h"

typedef struct s_polyEdgeData
{
	char edge_idx;	//edge pool index
	char e0,e1;		// vertex pool indices

} t_polyEdgeData;

class rmOccludeFacet
{
public:
#if HACK_GTA4
	void					Init();
	void					Release();
#else
							rmOccludeFacet			(void);
							~rmOccludeFacet		(void);
#endif // HACK_GTA4
	//void					Allocate				(int num_vertices );
	//void					Release					(void);
	
	int						GetNumVerts				(void) const { return (m_num_verts); }
	int						GetEdgeIndex			(int edge ) const;
	int						GetEdgeIndexReversed	(int edge ) const;
	int						GetVertpoolIndex		(int vertex ) const;

	void					SetVertexIndex			(int vertex, char indexSetValue );
	void					SetEdgeIndex			(int edge, char value );
	//void					SetEdgeIndexReversed	(int edge, char value, char reverse );
	void					SetEdgeFlag				(int edge, char flag );
	bool					GetFacetIgnoredFlag		(void) const { return b_isIgnored; }

	// The polyhedron object containing these facets has to be in scope for you to get vertex position data
	// all you get here is the indices of the vertices in the polyhedron vertex pool that define the facets.

	//const Vector3 &		GetVertex				(int n );
	//void					SetVertex				(int n, Vector3 &position );

//protected:
	u32		m_FaceEdgeIndicesReverseBits;	// bitfield that says whether or not an edge index is reversed or not when processing this face to ensure proper winding order.
	bool	b_isIgnored;					// ignore this face, for creating uncapped volumes and removing edges.
	char	m_num_verts;					// number of vertices defining the face of this n-gon
	char *	m_pVertIndices;					// Circular winding of the convex occluder poly (must be CW)
	char *  m_pEdgeIndices;					// array of indexed edges. edge index 0 is defined by vertices 0 and 1 inthe index pool, edge #1 is vertices 1 and 2 on the n-gon an so forth...
};

class rmOccludeEdge
{
	public:
							//rmOccludeEdge	(void){};
							//~rmOccludeEdge	(void){};

		void				SetEdge(int e0_e1, char edge)	{if(e0_e1&1){e1 = edge;}else{e0 = edge;}; }
		int					GetEdge(int n) const			{if(n&1){return e1;}else{return e0;}; }
		char e0;
		char e1;
};

class rmOccludePolyhedron
{
	friend class rmOccluderSystem;// yuck
	public:
							rmOccludePolyhedron	(void);
							~rmOccludePolyhedron	(void);

		// step #1 allocate vertex pool shared by all 
		bool				AllocateVertexPool		( int numVerts );
		// step #2 allocate edge pool shared by all 
		bool				AllocateEdges			( int numEdges );
		// step #3 preallocate pool of empty facet structures and the plane equation pool
		bool				AllocateFacetsAndPlanes		( int num_facets );		
		// step #4 allocate index pool for a facet. then flesh out its contents in next step.
		void				CreateFacet				( int facet, int num_ngon_verts );
		void				SetFacetIgnoredFlag		( int facet, int isIgnored );
		// step #5 all facets create, create static plane equations for each facet now in a seperate pool.
		void				CalculateFacetPlanes	( void );
		// step #6 synthesize data the occlude file didnt give us that we require for things to work properly.
		void				CalculateEdgeWindingData( void );

#if HACK_GTA4
		void				SetVertices(Vector3* pVerts, u32 numVerts);
		void				SetEdges(rmOccludeEdge* pEdges, u32 numEdges);
		void				SetFacets(rmOccludeFacet* pFacets, Vector4* pPlanes, u32 numFacets);
#endif // !HACK_GTA4

		// steps to delete cleanly.
		// #1: facets first.
		void				DestroyFacet			( int facet );
		// #2: facets destroyed, kill off arrays containing facet and planes
		void				DestroyFacetsAndPlanes	(void);
		void				DestroyEdges			(void);
		void				DestroyVertexPool		(void);

		bool				GetFacetIgnoredFlag		(int facet ) const;
		int					GetFacetEdgeIndex		(int facet, int edgenum ) const;
		int					GetFacetEdgeIndexReverseFlag(int facet, int edgenum ) const;

		int					GetFacetNumVerts		(int facet ) const;
		int					GetFacetVertIndex		(int facet, int vertnum ) const;

		int					GetNumEdges				( void ) const		{ return (m_num_edges); }
		int					GetNumFacets			( void ) const		{ return (m_num_planes); }
		//const Vector4 &		GetPlane				(int n );// NOTE: not implemented due to plane generation method. use version below instead.
		void				GetPlane				(int planeIdx, Vector4 *pv4dest) const;
		Vector4*			GetPlanes() const 		{return m_pPlanes;}


		int					GetNumVerts				( void ) const		{ return (m_num_verts); }// get vertex pool count
		Vector3*			GetVertices				() const				{ return (m_pVerts); }
		Vector3				GetVertex				(int n ) const	{ return (m_pVerts[n]); }
		__vector4			GetVertex4				(int n) const	{ return (m_pVerts[n]); }

		void				SetVertex				(int n, Vector3::Param position );
		void				SetEdgeIndices			(char n, char e0, char e1);

		void				SetFacetVertIndex		(int facet, int vertnum, int Value );
		void				SetFacetEdgeIndex		(int facet, int edge,	 int Value );
		void				SetFacetEdgeIndexReverseFlag( int facet, int edge, char flag );

		int					GetEdgeVertexIndex		(int edge, int edge01 ) const;

		bool				SphereIntersectTest		( const Vector3 &center, const float radius );// see if a sphere is contained within this convex hull
		//bool				TestOutsideSinglePlane	( void );		// return true if its outside a single plane (poly does not cross into frustum)
		// deprecated--- bool				TestOutsideSinglePlane  ( int *pClipFlags );//phase out...


		float				GetActiveDist			(void) const	{ return (m_activeDist); }
		void				ActiveDist				(float dist )	{ (m_activeDist = dist); }
		void				SetBoundSphere			(Vector4 &sphere);
		void				GetBoundSphere			(Vector4 *pv4dest) const;

		int					CreateSilhouettePolygonFromActiveEdges( t_polyEdgeData *pOuputEdgeData, const u32 activeEdgeBits );

		bool				TestOutsideSinglePlane		( pvsVtxClipFlags *pClipFlags, PvsViewer *pViewer );


		//-------------------------------------------------------------------------
		// ccoffin [8/24/2006]
		// used to activate/deactivate an occluder for *any reason* - ALL OCCLUSION CULLING FUNCTIONS NEED TO RESPECT THIS FLAG.
		// we can also use this to facilitate 'patch occlusion data' later on in regards to downloadable content, so if we demolish a building or increase/modify visiblity in areas, we
		// won't erroneously occlude things. Obviously this has to work in conjunction with patched PVS data as well.

		// This occluder is conditionally added to the system based on whether its been flagged as active or not.
		void				SetActiveMode( bool isActive ){ m_isActive = isActive; }	
		bool				IsActive(void){return m_isActive;}
		//-------------------------------------------------------------------------


		// DEBUG STUFF
		void				DebugDraw( bool IsDepthTestAndWriteDrawing = false);
		char*				GetDebugStringNamePtr(void)	{ return &m_szName[0]; } 
		int					GetDebugStringNameLen(void)	{ return (int)strlen(&m_szName[0]); }
		void				SetDebugStringName( char *srcString )
		{
			formatf( &m_szName[0], sizeof(m_szName), srcString );
		}

	private:

		// 16bytes
#if HACK_GTA4
		Vector4				m_boundSphere;	// sphere bound of this polyhedron for trivial quicktests (changed to vec3 and radius to get around the forced alignment and mem waste)
#else
		Vector3				m_boundSphereCtr;
		float				m_boundSphereRadius;
#endif

		// ptrs (4bytes ea)
#if __RM_OCCLUDE_STORED_GEOM_PLANES
		Vector4	*			m_pPlanes;		// Plane equations of the facets that create this polyhedron.
#endif
		// indexed face information, (CW orientation) - use indicies to get the appropriate verts out of the shared vertex pool owned by the polyhedron.
		rmOccludeFacet *	m_pFacets;		// an array of face information, count of 'm_num_planes'

		// OPTIMIZE: TODO: compress vertices to s16, relative to the bounding sphere?
		Vector3	*			m_pVerts;		// unique vertices that define the facets.	

		rmOccludeEdge *	m_pEdges;		// edge pool
//32 bytes...
		float				m_activeDist;	// Distance from eyepoint that occluder starts to be used at.
		s8					m_num_edges;	// indexed edge pool count
		s8					m_num_verts;	// Index vert pool count
		s8					m_num_planes;	// number of planes (facets) that define this poly
		s8					pad;// unused pad. (vector4 forces 16byte alignment)

		// 36bytes each.
		enum { MAX_OCCLUDERNAME_STRING_CHARACTERS = 64 };
		char				m_szName[ MAX_OCCLUDERNAME_STRING_CHARACTERS ];


		bool				m_isActive;// [8/24/2006] we have unused padding due to alignment requirements now.
};



class OccludedBoundInfo
{
//	OccludedBoundInfo();
//	~OccludedBoundInfo();

	friend class rmOccluderSystem;

public:
	rmPvsBoundType	GetType(void) const		{return m_boundtype;}
	pvsViewerID		GetViewerID(void) const	{return m_occludedViewID;}

	void			Render(void);

private:
	// comments after members denote usage of that field based on type.
	Vector3				m_min;		// aabox
	Vector3				m_max;		// aabox
	Matrix34			m_matrix;	// aabox
	Vector3				m_center;	// sphere/vertex
	float				m_radius;	// sphere
	Color32				m_debugDrawColor;
	pvsViewerID			m_occludedViewID;// view this was occluded in. (TODO: we could change this to a bitfield to record it in multiple views...)
	rmPvsBoundType	 	m_boundtype;
	rmPvsCullStatusBits	m_eStatusBits;

};

class rmOccludePoly
{
	public:
							rmOccludePoly	(void);//NOTE: allocates 4 verts all the time
							~rmOccludePoly	(void);
		//void				AllocateVerts	(int n );

		const Vector4 &		GetPlane		(void) const { return (m_plane);}
		const Vector3 &		GetVertex		(int n );
		void				GetVertex4		(int n, Vector4 *pv4 );

		int					GetNumVerts		(void) const { return (m_num_verts); }

		void				SetVertex		(int n, Vector3 &position );
		void				CalculatePlane	(void);// NOTE: Cannot recalc an already active occluder once a frame starts...

		bool				SphereIntersectTestHalfspace( const Vector3 &center, const float radius );
		bool				TestOutsideSinglePlane(void);// return true if its outside a single plane (poly does not cross into frustum)

		float				GetActiveDist	(void) const { return (m_activeDist); }
		void				ActiveDist		(float dist ){ (m_activeDist = dist); }
	private:
		
		//void				Copy			(rmOccludePoly &);
		//void				Transform		(Matrix34 &);
		//void				Recalculate		(void);// NOTE: Cannot recalc an already active occluder once a frame starts...

		Vector4				m_plane;		// Plane equation of the occluder poly
		s32					m_num_verts;	// number of verts that define this poly
		Vector3	*			m_pVerts;		// Circular winding of the convex occluder poly (must be CW)
		float				m_activeDist;	// Distance from eyepoint that occluder starts to be used at.

		s32					_pad;//unused, vector4 forces 16byte align
};









//-----------------------------------------------------------------------------
class rmOccluderSystem // not a singleton class!
{

	public:

		// TODO: Add helper function to determine which pvsview frusta intersect and/or are within a certain distance of one other.
		//		this would be useful in determining if we should iterate over multiple pvs views as we traverse a vast heirarchy of objects.
		//		perhaps the union of multple pvs frusta (and the resulting convex hull)
		//		could be used as an initial cull/gather test before passing it to each pvs view frusta for visibility testing //  [3/14/2006 ccoffin]

						rmOccluderSystem	(void);
		void			Destroy				(void);

#if	__BANK
void	AddWidgets	(bkBank &bk);
#endif

		bool			LoadDataset				(const char * basename, const char * subf);

		// Destroy with Release calls.
		static	bool	LoadExternalGeomDataset (	const char * basename,
													const char * subf,
													// destination ptrs
												  	int						*p_numOccluderGeoms,
													rmOccludePolyhedron		**pp_OccluderGeomArray,
												  	int						*p_numOccluderPolys,
													rmOccludePoly			**pp_OccluderPolyArray
													);

		//------------------------//
		// State Query/Management //
		//------------------------//
		bool			IsFrameInProgress	(void)			{ return( m_bFrameInProgress );		}
		void			OccludersEnabled	(bool bEnable )	{ m_bOccludersEnabled = bEnable;	}
		bool			AreOccludersEnabled (void)			{ return( m_bOccludersEnabled);		}
		// added to track if someone is trying to add viewpoints or new occluders during a frame
		void			StartNewFrame		(void);
		void			EndFrame			(void);





		//==============================================================================================
//		void			ViewMode			(const int viewerID, const bool bEnable );
//		void			DeactivateAllViews	(void);

//		Vector3 &		GetViewPosition		(const int viewerID);
//		Vector3 &		GetViewDirection	(const int viewerID);
//		const Matrix34* GetViewRenderCamMatrix	(const int viewerID);
		//const Matrix34* GetPVSCullCamMatrix	(const int viewerID);// TODO: Not implemented yet
	

		//bool			IsSpanBufferEnabled	(void)			{ return( m_bSpanBufferingEnabled); }

		//void			CacheOccludedVolumes(bool bCache );// system caches volumes to have AI determine better occluder choice next time
		//void			TemporalCoherenceDist( float dist );
		
		//-----------------------------//
		// Primitive Culling Functions //
		//-----------------------------//
		bool			IsSphereSpanBufferOccluded	( const Vector3 &center, const float radius, const bool b_AddSpans );
		bool			IsSphereOccluded			( const Vector3 &center, const float radius , int OccludeIndex );
		bool			IsHotDogOccluded			( const Vector3 &extentA, const Vector3 &extentB, const float radius );

		// used to frustum cull hotdog bounds quickly.
		bool			InlineFast2SpheresOutsideSinglePlane_A(	const Vector4 &sphere_A, const Vector4 &sphere_B  ) const;// Frustum Cull
		// we need a new version of these that returns z-distance...
		grcCullStatus	FrustumVisCheck				(const Vector4 &sphere,float& zDist);
		grcCullStatus	FrustumVisCheck				(const Vector4 &sphere,float& zDist, const int viewerID );



#if 0 //  [3/28/2006]
		int				GetViewActiveOccluderCount		(const int viewerID)
		{
			return m_numActiveOccluders[ viewerID ];// dont use!
		}
		int				GetOccludedCount			(void){ return(m_OccludedCount); }
		void			DebugDrawOccluders			(void);
#endif


		void			EnableDrawAllOccluders		(const pvsViewerID	DEV_ONLY(id), const bool DEV_ONLY(mode));





		//---------------------------------------------------------------------
		// span fusion buffer public functions
		// span fusion buffer public functions
		// span fusion buffer public functions
		// span fusion buffer public functions
		//
		// TODO: MIGRATE TO NEW CLASS STRUCTURE
		//
		void			ActivateSpanBuffer		( const int viewerID, int screen_w, int screen_h );
		void			ResetAllSpanBuffers		(void);// Call at end of frame when we know we dont need occlusion testing anymore and we are waiting on frame to finish rendering.

		// inserts a depth sprite for the currently active occluder view.
		bool			InsertDepthSprite		( int x0, int y0, int x1, int y1, float depth );
		bool			TestDepthSprite			( int x0, int y0, int x1, int y1, float depth );

		// transforms a position and projects it based on the currently active view.
		// DEPRECATE: NOTE: used with sbuffer code only, we need to redo this function and support 'render/pvs views and so on.'
		bool			TransformAndProject		( Vector4 *pResult ,  const Vector3 &source );

		// should be private...
		fSpan_t*		AllocateSpan			(void);
		//---------------------------------------------------------------------




		//---------------------------------------------------------------------
		// NEW! [3/14/2006 ccoffin]
		//
		// TODO: maybe the render view should make a copy of the grcViewport object as well (so we need to add that as a parameter)
		// that would probably facilitate window-in-window debug rendering/etc.
		void	SetRenderView			( const pvsViewerID id, const grcViewport* vp, const Vector3 &renderpos, const Vector3 &viewDirection, const Matrix34 *pCameraMatrix );
		static void	SetRenderView		( PvsViewer* pViewer, const grcViewport* vp, const Vector3 &renderpos, const Vector3 &viewDirection, const Matrix34 *pCameraMatrix );
		void	SetPvsView				( const pvsViewerID id, const grcViewport* vp, const Matrix34 &camMtx );
		static void	SetPvsView			( PvsViewer* pViewer, const grcViewport* vp, const Matrix34 &camMtx );
		void	SetOrthogonal(const pvsViewerID id,bool	UseOrthogonal);
		bool	GetOrthogonal(const pvsViewerID id)const;	
		void	SetActivationDistanceScale( const float val )
		{
			Assert( val > 0.f );
			sm_OccluderDistanceActivationScale = val;
		}

		float   GetActivationDistanceScale( void ) const
		{
			return sm_OccluderDistanceActivationScale;
		}
		
	


		bool	AddOccluderToActiveView	( const pvsViewerID	id, rmOccludePolyhedron *pHedra, const bool bIsExternal = false );
		void	ProcessStaticOccluders	( const pvsViewerID id  );
		void	ViewSetupFinish			( const pvsViewerID DEV_ONLY(id), const Matrix34 * DEV_ONLY(pCameraMatrix) );


		// added drawmodebits.. [3/20/2006 ccoffin]
		void	DrawDebugViewInfo		( const pvsViewerID DEV_ONLY(id), const rmPvsDebugDrawModeBits DEV_ONLY(drawmodebits) );// add picture-in-picture support, take a viewport/screen coords as input?

		// refactoring bound buffering code now. [3/22/2006 ccoffin]
		// allows you to block buffering of occluded volumes in certain code sections to make visual debugging easier.


		void	SetMode_OccludedVolumesBuffered( const bool bMode)	{ sm_AreOccludedVolumesBuffered = bMode; }
		bool	OccludedVolumesBuffered_GetMode( void )	const		{ return sm_AreOccludedVolumesBuffered; }


		void	SetMode_OccludedVolumeGlobalColor( bool mode )		{ sm_UsingOccludedVolumeGlobalColor = mode; }
		bool	GetMode_OccludedVolumeGlobalColor( void ) const		{ return sm_UsingOccludedVolumeGlobalColor; }
		void	SetOccludedVolumeDebugColor( Color32 pkdColor )		{ sm_debugColor = pkdColor; }
		Color32	GetOccludedVolumeDebugColor(void) const				{ return sm_debugColor; }



		// NEW! [3/14/2006 ccoffin]
		// fixme: change this so it returns a handle or something instead of an array index.

#if HACK_GTA4
		void			ReserveViewers(int count);
		pvsViewerID		CreateViewer(void);		
		void			DeleteViewer( const pvsViewerID id  );
#else // HACK_GTA4
		pvsViewerID		CreateViewer(void)
		{
			// grow atArray of pvsViewers, get index and return it.
			m_pvsViewers.Grow();
			return m_pvsViewers.GetCount()-1;
		};
		void	DeleteViewer( const pvsViewerID id  )
		{
			m_pvsViewers.Delete(id);
		}
#endif // HACK_GTA4
		PvsViewer*	GetPvsViewerPtr( const pvsViewerID id  ) const
		{
			PvsViewer *pviewer = &this->m_pvsViewers[ id ];
			FastAssert(pviewer);
			return pviewer;
		}

		// returns false if the occluder was not added for some reason.
		bool	AddOccluderToPvsView			( const pvsViewerID viewerID, rmOccludePolyhedron *pHedra, const bool bIsExternal  );
		static bool	AddOccluderToPvsView		( PvsViewer* pviewer, rmOccludePolyhedron *pHedra, const bool bIsExternal  );
		static void	InsertPlaneAsActiveOccluder	( const int planeNum, mcActiveOccluderPoly *Poly, Vector4::Param plane );
		static void	CreateEdgePlaneToActiveOccluder	( const int planeNum, mcActiveOccluderPoly *Poly,
													Vector3::Param pos, Vector3::Param edgeVert0, Vector3::Param edgeVert1 );




		//bool			IsSphereSpanBufferOccluded	( const pvsViewerID	id, const Vector3 &center, const float radius, const bool b_AddSpans );
		//bool			IsHotDogOccluded			( const pvsViewerID	id, const Vector3 &extentA, const Vector3 &extentB, const float radius );


		//---------------------------------------------------------------------




		//---------------------------------------------------------------------
		//  [3/15/2006 ccoffin] 'new/refactored' interface+members.




		//-----------------------------------------------------//
		// NEW Primitive Culling Functions [3/15/2006 ccoffin] //
		//-----------------------------------------------------//
	public:
		
#if HACK_GTA4
		bool			IsAABoxOccluded(	const pvsViewerID	id,
											const Vector4		*pv4Min,
											const Vector4		*pv4Max,
											const Matrix34		*pMtxLocalToWorld,// if null, we assume identity xform.
											const bool			bFrustumTest=true,
												  int	        *occluded_by=NULL
											);//  [3/14/2006 ccoffin]

		bool			IsSphereOccluded(	const pvsViewerID	id,
											const Vector3		&center,
											const float			radius,
											const bool			bFrustumTest=true,
												  int	        *occluded_by=NULL
											);//  [3/15/2006 ccoffin]

		bool			IsAABoxVisibleThroughOccluder(rmOccluderSystem *pOccSystem, int OccluderIndex,const Vector4 *pv4Min, const Vector4 *pv4Max, const Matrix34 *pMtxLocalToWorld);
		bool			IsSphereVisibleThroughOccluder(int id, const Vector3 &centre, float radius);
#endif // HACK_GTA4

		bool OccludeOccluders( const pvsViewerID viewerID );

		rmPvsCullStatusBits TestSphereVisibility(	const pvsViewerID		id,
													const Vector4			&Sphere,
													const rmPvsCullModeBits	cmBits
													);

		rmPvsCullStatusBits	TestSphereVisibility(	const pvsViewerID id,
													const Vector3 &CenterA,
													const float radius,
													const rmPvsCullModeBits	cmBits = pvscm_EnabledCulling_mask
													);

		rmPvsCullStatusBits	TestAABoxVisibility(	const pvsViewerID id,
													const Vector4		*pv4Min,
													const Vector4		*pv4Max,
													const Matrix34		*pMtxLocalToWorld,// if null, we assume identity xform.
													const rmPvsCullModeBits	cmBits = pvscm_EnabledCulling_mask
													);


		rmPvsCullStatusBits	TestCapsuleVisibility(	const pvsViewerID id,
													const Vector3 &extentA,
													const Vector3 &extentB,
													const float radius,
													const rmPvsCullModeBits	cmBits = pvscm_EnabledCulling_mask
													);//  [3/16/2006 ccoffin]

		rmPvsCullStatusBits	TestVertexPoolVisibility(const pvsViewerID	id,
													const Vector3		*pVertices,
													const int			vertexCount,
													// const Matrix34	*pMtxLocalToWorld,// if null, we assume identity xform.
													const rmPvsCullModeBits	cmBits = pvscm_EnabledCulling_mask,
													const rmOccludePolyhedron *pExemptHedra = NULL
													);//  [8/24/2006]


		// TODO: support culling of spatialdata types?
		static void		SetDebugAnimTimeDelta( float frame_dt )	{ sm_DebugAnimTimeDelta = frame_dt;	}
		static float	GetDebugAnimTimeDelta( void )			{ return sm_DebugAnimTimeDelta;		}


		static void		SetOccluderGlobalDebugDrawMode( const bool mode )
		{
			// need to refactor this interface.
			sm_DrawOccluderVolumes			= mode;
			sm_DrawActiveOccluderVolumes	= mode;
		}

	private:

		// Keeping this private since 'DrawDebugViewInfo' calls this.
		void			DrawDebugView_Frustum	( const pvsViewerID DEV_ONLY(id), const rmPvsDebugDrawModeBits DEV_ONLY(drawmodebits) );//  [3/21/2006 ccoffin]

//		bool				sm_IsOverridingDebugBoundColor;// TODO: IMPLEMENT
//		Color32				sm_debugColor;// TODO: IMPLEMENT - if 'sm_IsOverridingDebugBoundColor' is true, use this color setting when buffering occluded types for debug drawing..

		static	bool		sm_bOccludeActiveOccluders;
		static	bool		sm_bDrawOccludedVolumes;
		static	bool		sm_AreOccludedVolumesBuffered;
		static	bool		sm_AllowDebugDrawOfOccludedVolumes;
		static  bool		sm_UsingOccludedVolumeGlobalColor;
		static  Color32		sm_debugColor;

		static	int			sm_OccludedDrawBoundTotal;	// current buffered amount of 'OccludedBoundInfo' data.
		static	int			sm_maxDebugOccluders;		// max # to buffer up. allocate the appropriate number of 'OccludedBoundInfo' elements
		static	OccludedBoundInfo	*sm_a_bndInfo;

		bool			AddBufferedDebugDrawVolume(	const Vector3		&DEV_ONLY(center),
													const float			DEV_ONLY(radius),
													//const Color32		&DEV_ONLY(debugdrawcolor),
													const pvsViewerID	DEV_ONLY(id),
													const rmPvsCullStatusBits	DEV_ONLY(statusbits)
													);




		//---------------------------------------------------------------------
		// New global configuration options [3/20/2006 ccoffin]

		static bool		IsAbleToDebugDrawUmbraElements(void)// quick check to skip debug drawing code for certain classes of visualization.
		{
			#if __DEV
						if( !sm_FreezeAllOccluderViews )
							return false;

						// todo: implement! if( sm_DebugDrawOptions && 0)
						if( !sm_DebugDrawUmbra )
							return false;

						if( sm_FreezeAllOccluderViews && (!sm_DebugDrawUmbra) )
							return false;



						return true;
			#else
						return false;
			#endif
		}

		static bool		IsAbleToDebugDrawOccluderElements(void)
		{
			#if __DEV
						// todo: implement! if( sm_DebugDrawOptions && 0)

						if( (!sm_DrawOccluderVolumes) )
							return false;

						return true;
			#else
						return false;
			#endif
		}

		// Main.
		static  float		sm_DebugAnimTimeDelta;
		static	bool 		sm_UseOccluders;
		static	bool 		sm_FreezeAllOccluderViews;// global control
		static	bool		sm_DbgFrustumCullAndPvsFrustumCull;//perform 2 frustum culls, in final builds only pvs cull results are used+valid.

		// Frustum related
		static	bool		sm_DebugDrawFrustum;		
		static	float		sm_DrawFrustumVolumeDistance;
		static  bool		sm_DebugDrawFrustumAsOpaque;// useful for determining if an object 'pokes outside' the frustum or if its completely inside.
		static  bool		sm_DebugDrawFrustumEdgesOnly;

		// Occluder related
		static	bool		sm_DrawOccluderVolumes;
		static  bool		sm_DrawOccluderVolumeNames;
		static  bool		sm_DrawActiveOccluderVolumes;
		static	bool		sm_DisplayEdgeClippingInfo;
		static	bool		sm_DisplayEntireOccluderVolume;
		static  float		sm_DrawOccluderVolumeDistance;
		static  float		sm_OccluderDistanceActivationScale;

		// Umbra related
		static	bool		sm_DebugDrawUmbra; // global enable/disable.
		static  bool		sm_DebugDrawUmbraAsOpaque;// useful for determining if an object 'pokes outside' the umbra or if its completely inside.
		static	float		sm_DrawUmbraVolumeDistance;

		static	bool		sm_TraceViewpointToUmbra;
		static  float		sm_ViewpointTraceEdgeScale;



		// Controls buffering options when storing occluders and face/vtx/edge visibility info per viewport also!
		static	rmPvsDebugDrawModeBits	sm_DebugDrawOptions;		
#if HACK_GTA4
		PvsViewer*			m_pvsViewers;
		u32					m_numPvsViewers;
		u32					m_maxPvsViewers;
#else
		atArray <PvsViewer>				m_pvsViewers;// new viewer objects!
#endif
		//---------------------------------------------------------------------




		void			InsertPlaneAsActiveOccluder( const int planeNum, mcActiveOccluderPoly &Poly, const Vector4 &plane );
		void			CreateEdgePlaneToActiveOccluder( const int planeNum, mcActiveOccluderPoly &Poly, const Vector3 &pos, const Vector3 &edgeVert0, const Vector3 &edgeVert1 );


		//---------------------------------
		// Occluder poly data and loader //
		//---------------------------------
		static	bool	Load				( fiAsciiTokenizer & token,
												int						*p_numOccluderGeoms,
												rmOccludePolyhedron		**pp_OccluderGeomArray,
											  	int						*p_numOccluderPolys,
												rmOccludePoly			**pp_OccluderPolyArray
                                                );

		static	int		LoadHeader			( fiAsciiTokenizer & token,
												int						*p_numOccluderGeoms,
												rmOccludePolyhedron		**pp_OccluderGeomArray,
											  	int						*p_numOccluderPolys,
												rmOccludePoly			**pp_OccluderPolyArray
                                                );

		static	bool	LoadPolys			( int						version,
												fiAsciiTokenizer & token,  
											  	int						*p_numOccluderPolys,
												rmOccludePoly			**pp_OccluderPolyArray
												);

		static	bool	LoadGeoms			( int						version,
												fiAsciiTokenizer & token,
												int						*p_numOccluderGeoms,
												rmOccludePolyhedron		**pp_OccluderGeomArray
												);

		static	bool	AllocatePolys (	int n,
									  	int						*p_numOccluderPolys,
										rmOccludePoly			**pp_OccluderPolyArray
										);

		static	bool	AllocateGeoms (	int n,
										int						*p_numOccluderGeoms,
										rmOccludePolyhedron	**pp_OccluderGeomArray									  
										);

		static	void	ReleasePolys	(	int					*p_numOccluderPolys,
											rmOccludePoly		**pp_OccluderPolyArray
											);

		static	void	ReleaseGeoms	(	int						*p_numOccluderGeoms,
											rmOccludePolyhedron	**pp_OccluderGeomArray
											);

		//---------------------------------------------------------------------
		// local 'static' dataset....
		// todo: wrap this in a container class object, support multiple datasets more easily...
		int					m_numOccluderPolys;
		rmOccludePoly		*m_OccluderPolyArray;

		int					m_numOccluderGeoms;
		rmOccludePolyhedron	*m_OccluderGeomArray;
		//---------------------------------------------------------------------

				//ambiguous...
				int				m_OccludedCount;
				int				m_active_viewerID;
				bool			m_bFrameInProgress;


				// make these 'per view' maybe with a global override?
				bool			m_bOccludersEnabled;


				bool 			m_UseOccluderFrustumFusion;

				bool 			m_DrawActiveOccluders;

				bool 			m_UseSpanBuffering;
				bool 			m_TestSpansBeforeObjectFrusta;
				bool 			m_AllowSpanDebugDrawing;
				//	int				m_numActiveViewers;


#if 0 //  [3/28/2006]

					// OLD---------------
					PvsViewer		m_views[ RM_OCCLUDE_MAX_VIEWS ];

					// Each view has its own unique set of active occluders
					int					 m_numActiveOccluders[ RM_OCCLUDE_MAX_VIEWS ];
					mcActiveOccluderPoly m_activeOccluders[ RM_OCCLUDE_MAX_VIEWS ][ RM_OCCLUDE_MAX_ACTIVE_OCCLUDERS ];
					//------------------------
#endif

					//---------------------------------
					//---------------------------------
					//
					// SPAN BUFFER SYSTEM
					//		
					// NEW! //  [3/13/2006 ccoffin]
					atArray <SpriteSpanBuffer>	m_spanBuffers;

					// old version.
					bool				m_bIsSpanBufferActive[ RM_OCCLUDE_MAX_VIEWS ];// there should be no method to turn this off for a view, end of frame cleanup is the only thing that should modify this state.
					SpriteSpanBuffer	m_SpanBuff[ RM_OCCLUDE_MAX_VIEWS ];



					// get ptr to starting span for a given view+scanline.

					s16					m_activeScanline;// y scanline the current span insertion code is working on for the currently active view....

					fSpan_t	*			GetStartingSpanForViewScanline			(const int viewerID, int scanline);
					void				SetStartingSpanScanlineForActiveView	( fSpan_t *pStartSpan );
					SpriteSpanBuffer*	GetViewSSB								(const int viewerID);


					// span fusion buffer functions (private)		
					void			ReleaseSpan				( fSpan_t	*pFreedSpan );		
					s16				GetSpanBufferHeight		( const int viewerID );
					s16				GetSpanBufferWidth		( const int viewerID );		

					int				InsertSpan				( fSpan_t *pCurr, s16 newSpan_x0, s16 newSpan_x1, float depth ); 
					int				TestSpanOnly			( fSpan_t *pCurr, s16 newSpan_x0, s16 newSpan_x1, float depth );
					//---------------------------------
					//---------------------------------
};



// rmOccludeDynamic

// has a ptr to its original rmOccludePolyhedron
//class rmOccludeDynamicInst : public rmOccludePolyhedron
class rmOccludeDynamicInst
{
public:
	rmOccludeDynamicInst(void);	// flag ourselves as unlinked, clear out any local data.
	~rmOccludeDynamicInst(void);	// unlink and write back the local space vertices into the occluder before we destroy this object

	// Step 1, get local to world transform matrix and apply it to this dynamic occluder instance.
	//			the transformed points are stored into the instance vertex pool.

	void					LinkToBaseOccluder	( rmOccludePolyhedron *pOccluder );// this should be called in init code after the constructor sometime.
	//bool					Clone

	void					Transform			( Matrix34 &localToWorld);// transform and write the results into 

	bool					AddToView			( const rmOccluderSystem *pInst, const int viewerID );// add this transformed occluder to the currently active view.

	rmOccludePolyhedron*	GetLinkedOccluderPtr( void ) const { return m_pOccluder; }

private:
friend class rmOccludePolyhedron;

	bool				m_linked;		// tracks if this instance is linked already, to prevent someone from linking it to something else later
	rmOccludePolyhedron	*m_pOccluder;	// ptr to occluder we are instancing off of.

	int					m_numVerts;
	Vector3	*			m_pLocalVerts;		// local space verts copied from rmOccludePolyhedron
/*

// test and occluder plane creation functions that need to use world space vertices, (not local space) to create the proper output.
	const Vector3 &		GetVertex				(int n )				{ return (m_pWorldVerts[n]);}
	void				GetVertex4				(int n, Vector4 *pv4 )	{ *pv4 = m_pWorldVerts[n];	}
	void				GetBoundSphere			(Vector4 *pv4dest)
	{
		Vector3			m_boundSphereCtr;
		;
	}

	void				GetPlane				(int planeIdx, Vector4 *pv4dest);
	bool				TestOutsideSinglePlane  ( int *pClipFlags );

	bool				SphereIntersectTest		( const Vector3 &center, const float radius );

*/
	// pool of vertices that are in worldspace, not local.
	//Vector3	*			m_pWorldVerts;		// unique vertices that define the facets.
	// 
	//rmOccludePolyhedron
};




namespace rmOcclude
{
	// PURPOSE: Sets a bit in a bitfield
	// PARAMS:
	//	x - The bitfield (can be any size)
	//	y - The bit to set. For a single bit, y should be of the form 1 << N
	template<typename X> 
		inline void SET_BIT(X& x, int y) {
			// x |= y;
			x = (X)(x | y);
		}

		// PURPOSE: Clear a bit in a bitfield
		// PARAMS:
		//	x - The bitfield (can be any size)
		//	y - The bit to clear. For a single bit, y should be of the form 1 << N
		template<typename X> 
			inline void CLEAR_BIT(X& x, int y) {
				//x &= ~y;
				x = (X)(x & ~y);
			}

			// PURPOSE: Sets or clears a bit in a bitfield
			// PARAMS:
			//	x - The bitfield (can be any size)
			//  y - The bit to set or clear.
			//  val - If true, sets bit y, if false, clears bit y
			template<typename X>
				inline void SET_BIT(X& x, int y, bool val) {
					if (val) {
						SET_BIT(x, y);
					}
					else {
						CLEAR_BIT(x, y);
					}
				}

				// PURPOSE: Returns true if bit(s) y is set
				// PARAMS:
				//	x - The bitfield (can be any size)
				//	y - The bit(s) to check. For a single bit, y should be of the form 1 << N
				template<typename X> 
					inline bool MASK_MATCHES(const X& x, const int y) {
						return (x & (X)y) == (X)y;
					}

					template<typename X> 
						inline void AND_BITS(X& x, int y) {
							// x &= y;
							x = (X)(x & y);
						}

						// PURPOSE: Returns true if bit y is set
						// PARAMS:
						//	x - The bitfield (can be any size)
						//	y - The bit to check. For a single bit, y should be of the form 1 << N
						template<typename X> 
							inline bool IS_SET(const X& x, int y) {
								return (x & y) != 0;
							}
}

inline int rmOccludeFacet::GetVertpoolIndex( int vertex ) const
{
	return m_pVertIndices[vertex];
}

inline int rmOccludeFacet::GetEdgeIndex(int edge ) const
{
	return m_pEdgeIndices[edge];
}

inline int rmOccludeFacet::GetEdgeIndexReversed(int edge ) const
{
	return (1U<<edge) & m_FaceEdgeIndicesReverseBits;
}

inline void rmOccludePolyhedron::SetVertex( int n, Vector3::Param position )
{
	m_pVerts[ n ].Set( position );
}

inline void	rmOccludePolyhedron::GetBoundSphere( Vector4 *pv4dest ) const
{
#if HACK_GTA4
	*pv4dest = m_boundSphere;
#else
		pv4dest->x = m_boundSphereCtr.x;
		pv4dest->y = m_boundSphereCtr.y;
		pv4dest->z = m_boundSphereCtr.z;
		pv4dest->w = m_boundSphereRadius;
#endif
}

inline void	rmOccludePolyhedron::SetBoundSphere(Vector4 &sphere)
{
#if HACK_GTA4
	m_boundSphere = sphere;
#else
	m_boundSphereCtr.x	= sphere.x;
	m_boundSphereCtr.y	= sphere.y;
	m_boundSphereCtr.z	= sphere.z;
	m_boundSphereRadius = sphere.w;
#endif
}

inline bool	rmOccludePolyhedron::GetFacetIgnoredFlag( int facet ) const
{
	return m_pFacets[ facet ].b_isIgnored;
}

inline int rmOccludePolyhedron::GetFacetVertIndex(int facet, int vertnum ) const
{
	return m_pFacets[ facet ].GetVertpoolIndex( vertnum );
}

inline int	rmOccludePolyhedron::GetFacetNumVerts( int facet ) const
{
	return m_pFacets[ facet ].GetNumVerts();
}

inline int	rmOccludePolyhedron::GetFacetEdgeIndex(int facet, int edgenum ) const
{
	return m_pFacets[ facet ].GetEdgeIndex( edgenum );
}

inline int	rmOccludePolyhedron::GetEdgeVertexIndex(int edge, int edge01 ) const
{
	return m_pEdges[edge].GetEdge( edge01 );
}

inline int rmOccludePolyhedron::GetFacetEdgeIndexReverseFlag(int facet, int edgenum ) const
{
	return m_pFacets[facet].GetEdgeIndexReversed( edgenum );
}

#if __RM_OCCLUDE_STORED_GEOM_PLANES
inline void rmOccludePolyhedron::GetPlane( int planeIdx, Vector4 *pv4dest ) const
{
	*pv4dest = m_pPlanes[ planeIdx ];
}
#endif

inline void rmOccluderSystem::InsertPlaneAsActiveOccluder( const int planeNum, mcActiveOccluderPoly *Poly, Vector4::Param plane )
{
	Assert( planeNum < RM_OCCLUDE_MAX_OCCLUDER_POLYEDGES );

	Poly->m_occlusionPlanes[ planeNum ] = plane;
}


inline void rmOccluderSystem::CreateEdgePlaneToActiveOccluder(	const int planeNum, mcActiveOccluderPoly *Poly,
															  Vector3::Param pos, Vector3::Param edgeVert0, Vector3::Param edgeVert1 )
{
	/*
	From eye looking into screen:
	0-----1
	|\   /|
	| \ / |
	G<-E->G
	*/
	Assert( planeNum < RM_OCCLUDE_MAX_OCCLUDER_POLYEDGES );

	Poly->m_occlusionPlanes[ planeNum ].ComputePlane( pos, edgeVert0, edgeVert1 );
}


inline void	rmOccluderSystem::InsertPlaneAsActiveOccluder( const int planeNum, mcActiveOccluderPoly &Poly, const Vector4 &plane )
{
	Assert( planeNum < RM_OCCLUDE_MAX_OCCLUDER_POLYEDGES );

	Poly.m_occlusionPlanes[ planeNum ] = plane;
}

inline void	rmOccluderSystem::CreateEdgePlaneToActiveOccluder(	const int planeNum, mcActiveOccluderPoly &Poly,
															  const Vector3 &pos, const Vector3 &edgeVert0, const Vector3 &edgeVert1 )
{
	/*
	From eye looking into screen:
	0-----1
	|\   /|
	| \ / |
	G<-E->G
	*/
	Assert( planeNum < RM_OCCLUDE_MAX_OCCLUDER_POLYEDGES );

	Poly.m_occlusionPlanes[ planeNum ].ComputePlane( pos, edgeVert0, edgeVert1 );
}

#endif
