// 
// rmocclude/occluder_poly.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


//#include "occluder_math.h"
#include "occluder.h"
#include "occluder_poly.h"



/*
Purpose: Determine whether a sphere intersects a polygon and if its center occupies the forward halfspace.
Parameters:
center - The center of the sphere.
radius - The radius of the sphere.
numVerts - The number of vertices in the polygon.
verts - ptr to a continuous array of vector3 's, representing the vertices of the polygon winding counterclockwise.
polygonNormal - The polygon's normal.
Return: True if the sphere intersects the polygon and the center is on the side of the polygon plane normal supplied, false otherwise.

Notes:
- This routine assumes the polygon is convex.
- A sphere with a center point behind the forward halfspace of the polygon returns *FALSE* even if the sphere touches the poly
*/
namespace rmOcclude {
bool TestSphereToNgonHalfspace (const Vector3 &center, const float radius, const int numVerts, const Vector3 *verts,
								const Vector4 * polygonPlane ) //,
								//const Vector3 * polygonNormal)
{
	Assert(numVerts<=4);		//lint !e506 constant value boolean
	int i;
	int insideThisEdge[4];
	Vector3 vert1ToCenter, vert2ToCenter, polygonSide, edgeNormal;
	float a2, b2, c2, dist, dist2;

	Vector3	polygonNormal( polygonPlane->x, polygonPlane->y, polygonPlane->z );

	bool allInside=true, allOutside=true;

	// TODO: Make a version that returns an enumeration so we know how the specific results of the test we did?

	// Halfspace tests first.
	float	signedDist = polygonPlane->DistanceToPlane( center );

	if( signedDist < 0.f )
		return false;// ignore spheres with a center point behind the plane

	if( signedDist > radius )
		return false;// sphere does not touch the plane of the polygon, so it cant touch the poly.

	for (i=0; i<numVerts; i++)
	{
		vert1ToCenter.Subtract(center,(verts[i]));
		polygonSide.Subtract((verts[(i+1)%numVerts]),(verts[i]));

		edgeNormal.Cross( polygonNormal, polygonSide );


		if(edgeNormal.Dot(vert1ToCenter)>=0.0f)
		{
			// the point on the plane closest to the sphere center is inside this edge
			insideThisEdge[i]=1;
			allOutside=false;
			continue;
		}
		// the point on the plane closest to the sphere center is outside this edge
		insideThisEdge[i]=0;
		allInside=false;
		if(polygonSide.Dot(vert1ToCenter)<0.0f)
		{
			insideThisEdge[i]=-1;			// -1 means vertex1 is the closest edge point
			continue;
		}
		vert2ToCenter.Subtract(center,(verts[(i+1)%numVerts]));
		if(polygonSide.Dot(vert2ToCenter)>0.0f)
		{
			insideThisEdge[i]=-2;			// -2 means vertex2 is the closest edge point
			continue;
		}
		// the closest point on this edge to the sphere center is not a vertex and
		// the closest plane point to the sphere center is outside this edge, so
		// a point on this edge is the closest polygon point to the sphere center
		a2=vert1ToCenter.Mag2();
		b2=vert2ToCenter.Mag2();
		c2=polygonSide.Mag2();
		dist2=0.25f*(2.0f*(a2+b2)-c2-square(a2-b2)/c2);
		if(dist2>radius*radius)
		{
			// the whole polygon is outside the sphere
			return false;
		}
		// this edge point is inside the sphere
		return true;
	}

	if (allInside || allOutside)
	{
		// the closest polygon point to the sphere center is inside the polygon
		// dist will be the distance from the sphere center to the polygon's plane
		vert1ToCenter.Subtract(center,(verts[0]));
		dist=polygonNormal.Dot(vert1ToCenter);
		if(fabsf(dist)>radius)
		{
			return false;
		}
		return true;
	}

	// the closest polygon point is a vertex
	for(i=0;i<numVerts;i++)
	{
		if(insideThisEdge[i]!=-2) continue;			//lint !e644 not initialized but help seep up
		if(insideThisEdge[(i+1)%numVerts]==-1) break;
	}

	return (verts[(i+1)%numVerts].Dist2(center) <= radius*radius);
}	//lint !e818 suggestion on verts to be declared as pointing to const

};// namespace rage

rmOccludePoly::rmOccludePoly( void )// int numVerts)
{
	const int numVerts = 4;
	//Assert( numVerts == 4 && " rmOccludePoly::rmOccludePoly - Only support quads for now" );

	//Displayf(" rmOccludePoly::rmOccludePoly() - creating poly with %d verts -", numVerts );

	m_pVerts	= rage_new Vector3[ numVerts ];
	m_num_verts = numVerts;
}

rmOccludePoly::~rmOccludePoly( void )
{
	//Displayf(" calling rmOccludePoly::~rmOccludePoly() - freeing %d verts -", GetNumVerts() );

	if( m_pVerts )
		delete [] m_pVerts;

	m_pVerts = NULL;
};

bool rmOccludePoly::SphereIntersectTestHalfspace( const Vector3 &center, const float radius )
{
	return rmOcclude::TestSphereToNgonHalfspace( center, radius, m_num_verts, m_pVerts, &m_plane );
}

void	rmOccludePoly::SetVertex( int n, Vector3 &position )
{
	Assert( n < GetNumVerts() );// range check
	Assert( m_pVerts );			// ptr check

	m_pVerts[ n ].Set( position );
}


const Vector3 &	rmOccludePoly::GetVertex( int n )
{
	Assert( n < GetNumVerts() );// range check

	return this->m_pVerts[ n ];
}


void	rmOccludePoly::GetVertex4( int n, Vector4 *pv4 )
{
	Assert( n < GetNumVerts() );// range check

	pv4->x = m_pVerts[ n ].x;
	pv4->y = m_pVerts[ n ].y;
	pv4->z = m_pVerts[ n ].z;
	pv4->w = 1.f;
}

/*
PURPOSE
Compute the plane equation for the occluder poly based on the first 3 verts in its vertex array.

RETURN
none.
*/
void	rmOccludePoly::CalculatePlane(void)
{
	/*
	Input data winds counter-clockwise , compute plane using just the first 3 points,
	assuming the tool that created the data made sure the whole  n-gon is coplanar (and convex).

	3--2
	| /|
	|/ |
	0--1

	*/
#if __DEV
	Assert( m_num_verts > 2 );	// need at least 3 points to compute a plane....
	Assert( m_pVerts );			// pool allocated?
#endif

	m_plane.ComputePlane( m_pVerts[0], m_pVerts[1], m_pVerts[2] );
}

/*
bool rmOccludePoly::TestOutsideSinglePlane(void)
{
#define	MAX_TEST_VERTS 4

	Vector4 av4_src;//[ MAX_TEST_VERTS ];
	//Vector4 av4_dst[ MAX_TEST_VERTS ];
	int		clipFlags[ MAX_TEST_VERTS ];
	int		clipResult = 0;
	int		prevFlag = 0;

	int n;
	int numVerts = GetNumVerts();
	Assert( numVerts <= MAX_TEST_VERTS );

	// test vertex pool against each plane.
	for( n=0; n < numVerts; n++)
	{
		GetVertex4( n, &av4_src );// copy vertex to source buffer.

		av4_src.w = 0.f;// hack radius to 0

		Assert(0 && "FIXME");
		clipResult = 0x0;// FIXME->>>>>	//clipResult = PIPE.GetViewport()->InlineFastSphereVisCheck_ClipStatus( av4_src );

		if( clipResult & 0x555 )
		{
			clipFlags[n]	= clipResult;//(clipResult & 0x555);
			prevFlag		= clipResult;//(clipResult & 0x555);
		}else{
			clipFlags[n] = clipResult;// we dont care about other flags right now.
		}
	}

	// loop through all clip flags, AND'ing them together.
	for( n=0; n < numVerts; n++)
	{
		prevFlag &= clipFlags[n];
	}

	if( prevFlag & 0x555 )
	{
		return true;// one of the outside plane flags was set for all the vertices tested, so return TRUE (outside a single plane)


		// clipStatus:  -N +N -R +R -L +L -F +F -T +T -B +B
		//  - means dist is < -radius,
		//  + means dist is > radius

		//	if      (clipStatus & 0x555)            return cullOutside;      // far enough outside at least one plane to be rejected
		//	else if ((clipStatus & 0xAAA) == 0xAAA) return cullInside;       // we are far enough inside all planes
		//	else if (!(clipStatus & 0x800))         return cullClippedZNear; // clipped the near plane (important for some things)
		//	else                                    return cullClipped;      // clipped one of the sides or far,  really "if ((clipCodes & 0x2AA) != 0x2AA)", but that is the only case left!

	}else{
		return false; // not outside, crossing or contained within.
	}
}
*/
