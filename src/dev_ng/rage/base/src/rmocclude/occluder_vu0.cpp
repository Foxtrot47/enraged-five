// occluder_vu0.cpp 
#include	"bank/bank.h"
//#include	"core/output.h"
#include	"data/resource.h"
//#include	"data/asset.h"
//#include	"data/memstats.h"
//#include	"data/args.h"
//#include	"data/timemgr.h"
//#include	"data/string.h"
//#include	"data/param.h"

//#include	"gfx/pipe.h"
//#include	"gfx/viewport.h"
//#include	"gfx/viewport_inline.h"
//#include	"gfx/rstate.h"
//#include	"gfx/vgl.h"
//#include	"gfx/vglext.h"
//#include	"rmcore/state.h"



#include "grcore/device.h"
#include "grcore/im.h"
//#include "grcore/state.h"
#include "grcore/viewport.h"
#include "grcore/viewport_inline.h"
#include "vector/color32.h"




#include "rmocclude/occluder.h"

#if 0




// For non-ps2 builds, this code reverts to cpu versions where applicable and asynchronous 'prep' functions before batches are run do nothing.

extern	bool	*gp_AsyncVU0_OccludeResult	;	// storage for async occlusion test
extern	bool	g_bOccludeTestAABoxes;
extern	bool	g_bFrustumTestAABoxes;
extern  bool	g_VU0_occlusion;

#if __PS2
VUDATA( u32, occludeTestBound_DmaTag );// VU0 MICROCODE


#define	MAX_ACTIVE_OCCLUDERS_VU0		16
#define	OCCLUDER_PREP_DMA_CHAIN_SIZE	16

// FIXME: we need 1 of these for each 'view'
static	Vector4	OccluderPlanes_for_VU0[ 256 ];
static	u32		s_OccluderPlaneAndCMD_dmaChain[ OCCLUDER_PREP_DMA_CHAIN_SIZE ] ;
static	u32		commandList[ 256 ] ALIGNED (16 ); // MAX_ACTIVE_OCCLUDERS_VU0 * 4 * 16 ]	;

int OccluderViewID_in_VU0 = -1;// currently resident occluder data in vu0 memory. (FIXME: Should recreating the occluder system reset this?)

#endif

#define	VU0_OCCLUDEPLANE_DOWNLOAD_OFFSET	64
int		g_OccluderPlaneTableOffset[16];// 16 views supported.




#if __PS2
// since scratchpad only has enough for 4 matrices, this limits us to cull testing 1 player's normal view and envmap view at once.
extern	Matrix44	sm_ScratchPadFastCullMtx[  ] IN_SCRATCHPAD;
extern	Matrix44	sm_rmOccludeFastCullMtx[  ];// cant be scratchpad!!!!
#else
extern	Matrix44	sm_ScratchPadFastCullMtx[  ];
extern	Matrix44	sm_rmOccludeFastCullMtx[  ];// cant be scratchpad!!!!
#endif







#if __PS2
void	rmOccluderSystem::PrepOccluders_VU0( const int TotalViews, bool bForceLoad )
#else
void	rmOccluderSystem::PrepOccluders_VU0( const int /*viewerID*/, bool /*bForceLoad*/ )
#endif
{
#if __PS2

	Assert( (TotalViews > 0) && (TotalViews <= 4) );

	int	numPlanesPerView[4];// record number of planes per view.
	int ViewOffsets[4];
	//int	planeTallyForThisView = 0;
	s32 total_active = 0;
/*
	// Copy 'dma?' into vu0 mem.
	if( (viewerID == OccluderViewID_in_VU0 ) && !bForceLoad )
		return;// already resident.

	if( viewerID != 0 )
		return;
*/
	static bool loaded;
	if( bForceLoad || (!loaded))
	{
		//Displayf(" Downloading microcode ");
		// upload microcode program
		u32 addr	= (u32) (&occludeTestBound_DmaTag + 4);
		u32 qwc		= occludeTestBound_DmaTag & 0xFFFF;

		*D0_MADR	= addr;
		*D0_QWC		= qwc;
		*D0_CHCR	= DMA_CHCR_START;
		while (*D0_CHCR & DMA_CHCR_START)
			;// wait until done loading.... (move this wait to the bottom of setup function instead.)
		loaded = true;
	}
	
	//
	// Loop through occluder data, plane conversion and storage.
	//
	OccluderViewID_in_VU0	= 0;// update resident view ID so we know what vu0 is holding.	
	int	storageIDX			= 0;
	int	v4_countTotal		= 0;// plane count total...
	int OccluderPlaneCount	= 0;
	int	OccluderPlaneTableOffset = 0;// dont reset as we loop through views, this is gives us the jump offset for different views when kicking off microcode.

	for( int nView = 0; nView < TotalViews; nView++ )
	{
		total_active = m_numActiveOccluders[ nView ];

		// TODO: add frustum plane check 1st! then occlusion code will have a builtin frustum check to simplify things.
		for( int idx=0; idx < total_active; idx++ )
		{
			OccluderPlaneCount = 0;

			if( m_activeOccluders[ nView ][ idx ].m_pOccPoly )
			{
				Vector4	lastPlane;
				//FIXME: support old style quad occluders
				//continue;// old style quads, skipped for now.
				
				lastPlane = m_activeOccluders[ nView ][ idx ].m_pOccPoly->GetPlane();
				OccluderPlanes_for_VU0[ storageIDX++ ] = lastPlane;
				OccluderPlaneCount++;

				int numplanes = m_activeOccluders[ nView ][ idx ].m_num_planes;

				if( numplanes )
				{
					int plane;
					for( plane=0; plane < numplanes; plane++)
					{			
						// copy plane equation.
						OccluderPlanes_for_VU0[ storageIDX++ ] = m_activeOccluders[ nView ][ idx ].m_occlusionPlanes[ plane ];
						OccluderPlaneCount++;
					}
				}
				else
				{
					Assert(" occluder is active but has no planes registered!" && 0 );
				}
			}
			else		// remaining poly planes
			{
				// Get plane count for geom occluders.
				int numplanes = m_activeOccluders[ nView ][ idx ].m_num_planes;
				Assert(" occluder geom is active but has no planes registered!" && (numplanes> 0) );

				Vector4	PlaneEQ;
				int plane;

				Assert( numplanes > 0 );
				for( plane=0; plane < numplanes; plane++)
				{		
					PlaneEQ = m_activeOccluders[ nView ][ idx ].m_occlusionPlanes[ plane ];
					// copy plane equation.
					//Displayf(" plane %f %f %f %f ", PlaneEQ.x, PlaneEQ.y, PlaneEQ.z, PlaneEQ.w );
					OccluderPlanes_for_VU0[ storageIDX++ ] = PlaneEQ;//m_activeOccluders[ viewerID ][ idx ].m_occlusionPlanes[ plane ];
					OccluderPlaneCount++;
				}
			}
/*
		Command List
			--Start of View #0
			0	(number of planes for active occluder #0 for View #0 )
			1	(number of planes for active occluder #1 for View #0 )
			2	(number of planes for active occluder #2 for View #0 )
			3
			4
			5
			6
			7
			8
			9
			10
			11
			12
			13
			14
			15
			---Start of View #1
			16	(number of planes for active occluder #0 for View #1 )
			17	(number of planes for active occluder #1 for View #1 )
			18 .....
*/
			commandList[ (nView*64) + (idx*4) ] = OccluderPlaneCount;// record plane count for this occluder.

		}// loop all active occluders for this view.

		Assert( (nView >= 0) && (nView < 4) );

		numPlanesPerView[ nView ]	= OccluderPlaneCount;
		ViewOffsets[ nView ]		= OccluderPlaneTableOffset;

		g_OccluderPlaneTableOffset[ nView ]	= VU0_OCCLUDEPLANE_DOWNLOAD_OFFSET + OccluderPlaneTableOffset;

		OccluderPlaneTableOffset	+= OccluderPlaneCount;// compute offset for next view here.

	}// loop for all views.

	//Displayf(" storageIDX %d ", storageIDX );

	if( storageIDX )
	{
		v4_countTotal = storageIDX;// plane total (# of vector 4's

		int offset = 0;// not double buffered so this should be 0 i think...
		int stored = 0;
#if 0
		for( int idx=0; idx < 64; idx++ )
		{
			Displayf(" occluder %d = plane count = %d ", idx, commandList[ idx*4 ] );
		}

		for( int nn=0; nn < 4; nn++ )
		{
			Displayf(" plane offsets - view %d - addr %d ", nn, g_OccluderPlaneTableOffset[ nn ] );
		}

		for( int i=0; i < storageIDX; i++)
		{
			Displayf(" plane %f %f %f %f ",
						OccluderPlanes_for_VU0[i].x,
						OccluderPlanes_for_VU0[i].y,
						OccluderPlanes_for_VU0[i].z,
						OccluderPlanes_for_VU0[i].w
						);			
		}
#endif
		
		// Command List
		s_OccluderPlaneAndCMD_dmaChain[stored++] = RAW0_DMA_ID_REFE | 64;
		s_OccluderPlaneAndCMD_dmaChain[stored++] = (u32) (&commandList);
		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;
		s_OccluderPlaneAndCMD_dmaChain[stored++] = RAW_VIF_CMD_UNPACK_V4_32 | RAW_VIF_NUM( 64 ) | RAW_VIF_CMD_UNPACK_ADDR(offset) | RAW_VIF_CMD_UNPACK_UNSIGNED;

		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;
		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;
		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;
		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;


		s_OccluderPlaneAndCMD_dmaChain[stored++] = RAW0_DMA_ID_REFE | (((v4_countTotal * 16) + 15) >> 4);
		s_OccluderPlaneAndCMD_dmaChain[stored++] = (u32) (&OccluderPlanes_for_VU0[0].x);
		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;
		s_OccluderPlaneAndCMD_dmaChain[stored++] = RAW_VIF_CMD_UNPACK_V4_32 | RAW_VIF_NUM( v4_countTotal ) | RAW_VIF_CMD_UNPACK_ADDR( VU0_OCCLUDEPLANE_DOWNLOAD_OFFSET ) | RAW_VIF_CMD_UNPACK_UNSIGNED;

		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;
		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;
		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;
		s_OccluderPlaneAndCMD_dmaChain[stored++] = 0;



		FlushCache(WRITEBACK_DCACHE);// NOTE: WARNING: Have to do this to make sure the following dma's have their data written to memory.

		//Displayf("Downloading occluder data");

		// Download first packet, wait for it.
		*D0_TADR	= (u32) &s_OccluderPlaneAndCMD_dmaChain[0];
		*D0_QWC		= 0;
		*D0_CHCR	= DMA_CHCR_MOD_CHAIN | DMA_CHCR_TTE_ENA | DMA_CHCR_START;
		while (*D0_CHCR & 0x100)
			;

		// Download next packet, wait for it.
		*D0_TADR	= (u32) &s_OccluderPlaneAndCMD_dmaChain[8];
		*D0_QWC		= 0;
		*D0_CHCR	= DMA_CHCR_MOD_CHAIN | DMA_CHCR_TTE_ENA | DMA_CHCR_START;
		while (*D0_CHCR & 0x100)
			;


		//Displayf("Finished");
	}
#endif
}


void	IsSphere4Occluded_StartAsyncVU0( const Vector4 &sphere, bool *pResult )
{
#if	__PS2
	// start 
	int activeView = RM_OCCLUDE->GetActiveViewID();
#if __DEV
	if( (RM_OCCLUDE->AreOccludersEnabled()) ) // 1 && (activeView==0) )// only view0 does vu0 for now.
#else
	if(1)//  (activeView==0) )
#endif
	{
		s32 total_active = RM_OCCLUDE->GetViewActiveOccluderCount( activeView );
asm (".set noreorder");
		if( total_active == 0 )
		{
			// move 'fail' into vi04 so async getresult function returns the proper value
			//asm __volatile__("iddiu $vi04, $vi00, 0x0" );
			asm __volatile__("ctc2.i	$0,$vi04 " );// clear - we should interlock here in case something else was running.
			//asm __volatile__("cfc2.i %0, $vi04" : "=r"(occludeResult));// stored in vi04 by microcode
			return;// no occluders active!
		}
		
		// move bounding sphere to vureg
	//	asm __volatile__("lqc2 $vf8, 0x0(%0)" : : "r" (&sphere));// load bounding sphere where the microcode expects it
	//	asm __volatile__("lqc2 $vf8, %0" : : "r" (&sphere));// load bounding sphere where the microcode expects it
		asm __volatile__("lqc2 $vf8, %0" : : "m"(sphere));

		// move occluder count into vu registers before we fire it up
		asm __volatile__("ctc2.i %0, $vi03" : : "r"(total_active));

		// set offset to where we can find the plane count list for the active occluders
		int planeCountsAddr = activeView<<4;
		asm __volatile__("ctc2.i %0, $vi07" : : "r"(planeCountsAddr));
		// set plane offset based on the view we are processing.
		asm __volatile__("ctc2.i %0, $vi02" : : "r"(g_OccluderPlaneTableOffset[activeView] ));

		// execute microcode program
		asm __volatile__("vcallms OccludeTestSphere");
asm (" .set reorder");
		// vu0 running, do whatever you want here....
		gp_AsyncVU0_OccludeResult = pResult;// store ptr to location where we place the result at when its done

		return;
	}
	else
	{
		*pResult = false;
		return;
	}


#else

	if( !(RM_OCCLUDE->AreOccludersEnabled()) )
	{
		*pResult = false;
		return;
	}

	Vector3	center( sphere.x, sphere.y, sphere.z);
	float	radius = sphere.w;

	*pResult =  RM_OCCLUDE->IsSphereOccluded( center, radius );
#endif
}

void	IsSphereOccluded_StartAsyncVU0( const Vector3 &center, const float radius,  bool *pResult )
{
#if	__PSX2
	Vector4	sphere;		
	sphere.Set( center.x, center.y, center.z, radius );

	// start 
	int activeView = RM_OCCLUDE->GetActiveViewID();

#if __DEV
	if( (RM_OCCLUDE->AreOccludersEnabled()) )//  && 1 && (activeView==0) )// only view0 does vu0 for now.
#else
	if(1)// (activeView==0) )
#endif
	{
		s32 total_active = RM_OCCLUDE->GetViewActiveOccluderCount( activeView );
asm (".set noreorder");
		if( total_active == 0 )
		{
			// move 'fail' into vi04 so async getresult function returns the proper value
//			asm __volatile__("iddiu $vi04, $vi00, 0x0" );
			asm __volatile__("ctc2.i	$0,$vi04 " );// clear - we should interlock here in case something else was running.
			//asm __volatile__("cfc2.i %0, $vi04" : "=r"(occludeResult));// stored in vi04 by microcode
			return;// no occluders active!
		}
		
		// move bounding sphere to vureg
		asm __volatile__("lqc2	 $vf8, 0x0(%0)" : : "r" (&sphere));// load bounding sphere where the microcode expects it

		// move occluder count into vu registers before we fire it up
		asm __volatile__("ctc2.i %0, $vi03" : : "r"(total_active));
		// set offset to where we can find the plane count list for the active occluders
		int planeCountsAddr = activeView<<4;
		asm __volatile__("ctc2.i %0, $vi07" : : "r"(planeCountsAddr));
		// set plane offset based on the view we are processing.
		asm __volatile__("ctc2.i %0, $vi02" : : "r"(g_OccluderPlaneTableOffset[activeView] ));

		// execute microcode program
		asm __volatile__("vcallms OccludeTestSphere");
asm (" .set reorder");
		// vu0 running, do whatever you want here....
		gp_AsyncVU0_OccludeResult = pResult;// store ptr to location where we place the result at when its done

		return;
	}
	else
	{
		*pResult = false;
		return;
	}
#else

	if( !(RM_OCCLUDE->AreOccludersEnabled()) )
	{
		*pResult = false;
		return;
	}

	*pResult =  RM_OCCLUDE->IsSphereOccluded( center, radius );
#endif
}


bool		GetResult_SyncVU0( void )
{
#if	__PSX2
	volatile register int occludeResult;
	// interlock wait for the occlusion results
asm (".set noreorder");
	asm __volatile__("cfc2.i %0, $vi04" : "=r"(occludeResult));// stored in vi04 by microcode
asm (".set reorder");
	if( occludeResult == 777 )
	{
		//Displayf("****** occludeResult %d ", occludeResult  );
		if( gp_AsyncVU0_OccludeResult )
		{
			*gp_AsyncVU0_OccludeResult = true;// write
		}

		return true;
	}
	else
	{
		if( gp_AsyncVU0_OccludeResult )
		{
			*gp_AsyncVU0_OccludeResult = false;// write
		}
		return false;
	}
#else
	Assert(gp_AsyncVU0_OccludeResult);
	return *gp_AsyncVU0_OccludeResult;// return last stored result
#endif
}





#endif


