// occluder_view.h 
#ifndef RM_OCCLUDE_OCCLUDER_VIEW_H
#define RM_OCCLUDE_OCCLUDER_VIEW_H

#include <stddef.h>

//#include "rmocclude/occluder.h"
#include "vector/vector3.h"
//#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "grcore/viewport.h"

namespace rage {

//class Vector3;
class Vector4;
class Matrix34;
//class Matrix44;

}	// namespace rage
using namespace rage;

enum occluderCullModeBits {
	ocmb_spanTestRead		= 0x0000001,
	ocmb_spanTestReadWrite	= 0x0000002,
	ocmb_geomTestRead       = 0x0000004,

	// priority bits.
	ocmb_geom_b4_spans		= 0x0000100,
	ocmb_mask				= 0xfffffffU
};


// When an occluder is made active, the following extra data is generated and added to the active list.
class BEGIN_ALIGNED(128) mcActiveOccluderPoly
{
public:
	Vector4				m_occlusionPlanes[ RM_OCCLUDE_MAX_OCCLUDER_POLYEDGES ];
	rmOccludePoly*		m_pOccPoly;				// occluder that owns this extended data
	s32					m_type;
	ptrdiff_t			m_Id;// pool index of type - when the occluder is an externally managed source, this is a *POINTER*
	s32					m_num_planes;			// number of *unclipped* occlusion planes, one for each polyedge normally

#if !HACK_GTA4
	s32					num_silh_verts;
	s32					m_silh_x[32];
	s32					m_silh_y[32];
#endif // !HACK_GTA4


	//-------------------------------------------------------------------------
	// ccoffin [8/24/2006]
	// used to activate/deactivate an occluder for *any reason* - ALL OCCLUSION CULLING FUNCTIONS NEED TO RESPECT THIS FLAG.
	// we can also use this to facilitate 'patch occlusion data' later on in regards to downloadable content, so if we demolish a building or increase/modify visiblity in areas, we
	// won't erroneously occlude things. Obviously this has to work in conjunction with patched PVS data as well.
	void				SetActiveMode( bool isActive ){ m_isActive = isActive; }	
	bool				IsActive(void){return m_isActive;}
	//-------------------------------------------------------------------------

#if __DEV
	// WARNING: If any fields are null, then we skip storing that info...attempting to debug draw using that data will have undefined results!
	void				StoreDebugDrawInfo( rmPvsDebugDrawModeBits	ASSERT_ONLY(modebits),
											pvsVtxClipFlags		*pSrcVertexClipResultBuffer,
											pvsFaceClipFlags	*pSrcFaceResultBuffer,
											pvsEdgeData			*p_edgeFlags,
											pvsEdgeData			*p_edgeReversed
											)
	{
		Assert( modebits != pvsdd_AllBits_mask );

		if( p_edgeFlags )
			m_edgeFlags = *p_edgeFlags;

		if( p_edgeReversed )
			m_edgeReversed = *p_edgeReversed;

		if( pSrcVertexClipResultBuffer )
		{	
			for( int i=0; i< RM_OCCLUDE_MAX_OCCLUDER_VERTICES;i++ )
			{
				m_VertexClipResults[i]= pSrcVertexClipResultBuffer[i];
			}
		}

		if( pSrcFaceResultBuffer )
		{	
			for( int i=0; i< RM_OCCLUDE_MAX_OCCLUDER_FACES;i++ )
			{
				m_FaceResults[i]= pSrcFaceResultBuffer[i];
			}
		}
	}

	bool				IsVertexVisible(int idx )
	{
		int val =  m_VertexClipResults[idx];

		if(val)
			return true;
		else
			return false;
	}

	bool				IsFaceVisible(int idx )
	{
		int val =  m_FaceResults[idx];

		if(val)
			return true;
		else
			return false;
	}

	pvsEdgeData			GetDebug_PvsEdgeData(void)			{ return m_edgeFlags; }
	pvsEdgeData			GetDebug_PvsReverseEdgeData(void)	{ return m_edgeReversed; }

	bool				GetDebug_WasPvsEdgeRemoved( int e0_vertIdx, int e1_vertIdx )
	{
		// If the union of the 2 edge vertices clip flags have any set bit left based on our mask test,
		//  then the edge is completely off the side of one of the clipping planes
		if( pvscull_frust_plane_outside_mask /* 0x555 */ & ((m_VertexClipResults[ e0_vertIdx ] & m_VertexClipResults[ e1_vertIdx ])))
		{			
			return true;// edge off one side of frustum 
		}
		return false;
	}
#endif


private:

	pvsEdgeData m_edgeFlags;
	pvsEdgeData m_edgeReversed;
#if HACK_GTA4
	bool			m_isActive;	
	u32				m_pad;
#endif // HACK_GTA4
	
#if __DEV || !HACK_GTA4
	//todo: change this to a u64 or something.!
	pvsVtxClipFlags		m_VertexClipResults[64];
	pvsFaceClipFlags	m_FaceResults[64];
#endif

	// TODO: add extra data here for clipped/fused occluders for each view...
} ALIGNED(128);


//-----------------------------------------------------------------------------
//
// NOTE: This class is designed to be self contained so it does not reference any outside data.
// initializing a PvsViewer is something usually done once per frame so the copy/setup overhead is not
// something you need to be concerned about [3/13/2006 ccoffin]
//
class PvsViewer
{
	friend class rmOccluderSystem;// kinda hack'ish

public:
	PvsViewer				(void);

	int			GetNumActiveOccluders	(void) const { return m_pvsview_numActiveOccluders; }

	Vector3		GetViewPosition			(void) const { return (m_position); }
	Vector3		GetViewDirection			(void) const { return (m_viewDirection); }
	void			SetView						( const Vector3 &position, const Vector3 &viewDirection );

	void			ActiveMode					(bool bEnable){ m_bIsActive = bEnable; }
	bool			IsActive						(void){ return (m_bIsActive); }

	void			SetCamMatrix			(const Matrix34 *pCameraMatrix);
	const Matrix34*	GetRenderCamMatrix		(void){ return &m_CamMtx; }




	const Matrix44 &GetMatrix( const rmPvsViewType eViewtype, const rmPvsMatrixType eMatrixType )
	{
		switch( eMatrixType  )
		{
			default:
			Assert(0);// to shut the compiler up on release builds about not returning a value... 
			//fall through and return *something* below
			case	pvsmt_World:
						return Get_world( eViewtype );
						/*NOTREACHED*/
							break;

			case	pvsmt_View:	
						return Get_view( eViewtype );
						/*NOTREACHED*/
							break;

			case	pvsmt_Projection:	
						return Get_projection( eViewtype );
						/*NOTREACHED*/
							break;	

			case	pvsmt_Screen:		
						return Get_screen( eViewtype );
						/*NOTREACHED*/
							break;

			case	pvsmt_ModelView:		
						return Get_modelview( eViewtype );
						/*NOTREACHED*/
							break;

			case 	pvsmt_Composite:		
						return Get_composite( eViewtype );
						/*NOTREACHED*/
							break;

			case 	pvsmt_FullComposite:
						return Get_fullcomposite( eViewtype );
						/*NOTREACHED*/
							break;
		}
	}

	void SetMatrix( const rmPvsViewType eViewtype, const rmPvsMatrixType eMatrixType,  const Matrix44 *pMatrix )
	{
		FastAssert( pMatrix != NULL );

		switch( eMatrixType  )
		{
		case	pvsmt_World:
			 Set_world( eViewtype, pMatrix );
			break;

		case	pvsmt_View:	
			 Set_view( eViewtype, pMatrix );
			break;

		case	pvsmt_Projection:	
			 Set_projection( eViewtype, pMatrix );
			break;	

		case	pvsmt_Screen:		
			 Set_screen( eViewtype, pMatrix );
			break;

		case	pvsmt_ModelView:		
			 Set_modelview( eViewtype, pMatrix );
			break;

		case 	pvsmt_Composite:		
			 Set_composite( eViewtype, pMatrix );
			break;

		case 	pvsmt_FullComposite:
			 Set_fullcomposite( eViewtype, pMatrix );
			break;

		default:
			Assert(0);			
			break;
		}
	}





	void			SetCullingModeBits		( u32 mode ){ m_u32CullMode = mode; }
	u32				GetCullingModeBits		(void){ return m_u32CullMode;}

	void			SetRenderPhaseBits		(const u32 phase ){ m_u32RenderPhase = phase; }
	u32				GetRenderPhaseBits		(void){ return m_u32RenderPhase;}


	Matrix44*		GetCullMtx0				(void){ return &m_CullMtx0; }// phasing out.
	Matrix44*		GetCullMtx1				(void){ return &m_CullMtx1; }// phasing out.

	//  [3/20/2006 ccoffin]
// to be implemented!	Matrix44*		GetRenderCullMtx0		(void){ return &m_RenderCullMtx0; }
// to be implemented!	Matrix44*		GetRenderCullMtx1		(void){ return &m_RenderCullMtx1; }
	Matrix44*		GetPvsCullMtx0			(void){ return &m_CullMtx0; }// phasing out 'GetCullMtx0'
	Matrix44*		GetPvsCullMtx1			(void){ return &m_CullMtx1; }// phasing out 'GetCullMtx1'


	// uses internal frustum cull matrices to calculate visibility, returns clip code bits.
	pvsVtxClipFlags	SphereFrustumCheck_ClipStatus( Vector4::Param sphere );
	pvsVtxClipFlags	PointFrustumCheck_ClipStatus( Vector4::Param point );



// make these private later??

	float		Get_AspectRatio(){ return m_AspectRatio;}
	void		Set_AspectRatio(float val ){m_AspectRatio = val;}

	float		Get_ZClipNear(){ return m_ZClipNear;}
	void		Set_ZClipNear(float val ){m_ZClipNear = val;}

	float		Get_ZClipFar(){ return m_ZClipFar;}
	void		Set_ZClipFar(float val ){m_ZClipFar = val;}

	float		Get_fovy(){ return m_fovy;}
	void		Set_fovy(float val){ m_fovy = val;}

	float		Get_tanFovy(){ return m_tanFovy;}
	void		Set_tanFovy(float val ){ m_tanFovy = val;}

	float		Get_cosVFOV(){ return m_cosVFOV;}
	void		Set_cosVFOV(float val ){m_cosVFOV = val;}

	float		Get_cosHFOV(){ return m_cosHFOV;}
	void		Set_cosHFOV(float val ){m_cosHFOV = val;}

	float		Get_sinVFOV(){ return m_sinVFOV;}
	void		Set_sinVFOV(float val ){m_sinVFOV = val;}

	float		Get_sinHFOV(){ return m_sinHFOV;}
	void		Set_sinHFOV(float val ){m_sinHFOV = val;}

	grcViewport *GetOrthoViewport_render(void)			{ return &m_orthoVP_render;}
	grcViewport *GetPerspectiveViewport_render(void)	{ return &m_perspVP_render;}

	grcViewport *GetOrthoViewport_pvs(void)			{ return &m_orthoVP_pvs;}
	grcViewport *GetPerspectiveViewport_pvs(void)	{ return &m_perspVP_pvs;}


	void	SetIsOrthogonal(bool ortho){m_IsOrthogonal=ortho;}
	bool	GetIsOrthogonal() const {return m_IsOrthogonal;}

	//  [3/14/2006 ccoffin] calling code has to handle the possibility  of NULL being returned!
	mcActiveOccluderPoly* GetActiveOccluderStoragePtr( void )
	{
		if( m_pvsview_numActiveOccluders >= RM_OCCLUDE_MAX_ACTIVE_OCCLUDERS )
			return NULL;

		return &m_pvsview_activeOccluders[ m_pvsview_numActiveOccluders ];
	}

	mcActiveOccluderPoly* GetActiveOccluderIndexPtr( int index )
	{
		FastAssert( (index >= 0) && (index < m_pvsview_numActiveOccluders));
		return &m_pvsview_activeOccluders[ index ];
	}

private:

	void IncrementActiveOccluderCount(void) { m_pvsview_numActiveOccluders++; }

	mcActiveOccluderPoly m_pvsview_activeOccluders[ RM_OCCLUDE_MAX_ACTIVE_OCCLUDERS ];
	int					 m_pvsview_numActiveOccluders;


	grcViewport	m_orthoVP_pvs;
	grcViewport	m_perspVP_pvs;

	grcViewport	m_orthoVP_render;
	grcViewport	m_perspVP_render;

	Matrix44	m_a_world44[ pvsvt_NumViewTypes ];		
	Matrix44	m_a_view44[ pvsvt_NumViewTypes ];	
	Matrix44	m_a_projection44[ pvsvt_NumViewTypes ];
	Matrix44	m_a_screen44[ pvsvt_NumViewTypes ];
	// regenerated composite matrices
	Matrix44	m_a_modelview44[ pvsvt_NumViewTypes ];
	Matrix44	m_a_composite44[ pvsvt_NumViewTypes ];
	Matrix44	m_a_fullcomposite44[ pvsvt_NumViewTypes ];

												// cam matrix
												Matrix34		m_CamMtx;//todo: remove!

												// new! todo: support these [3/28/2006]
												Matrix34		m_CamMtx_pvs;
												Matrix34		m_CamMtx_render;




												Matrix44		m_CullMtx0;// todo: rename
												Matrix44		m_CullMtx1;// todo: rename

												Matrix44		m_RenderCullMtx0;
												Matrix44		m_RenderCullMtx1;


	bool			m_DrawAllOccluders;//debug draw! - __DEV
	bool			m_IsOrthogonal;

	bool			m_bIsActive;
	Vector3			m_position;
	Vector3			m_viewDirection;

	u32				m_u32CullMode;		// mode bits that specify how occlusion culling works.
	u32				m_u32RenderPhase;	// what types of objects we are rendering(reflections,props,ambients,cars,people, etc) (user data)


	// extra data
	float			m_AspectRatio;
	float			m_fovy;
	float			m_tanFovy;
	float			m_ZClipNear;
	float			m_ZClipFar;
	float			m_cosVFOV; 
	float			m_cosHFOV; 
	float			m_sinVFOV; 
	float			m_sinHFOV;

	//  [3/28/2006]--------------------------------------------------------------
	// 'SET' functions.
	void Set_world				( const rmPvsViewType eViewtype, const Matrix44 *pMatrix )
	{
		FastAssert( pMatrix != NULL );
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		m_a_world44[ eViewtype ] = *pMatrix;
	}

	void Set_view				( const rmPvsViewType eViewtype, const Matrix44 *pMatrix )
	{
		FastAssert( pMatrix != NULL );
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		m_a_view44[ eViewtype ] = *pMatrix;
	}

	void Set_projection			( const rmPvsViewType eViewtype, const Matrix44 *pMatrix )
	{
		FastAssert( pMatrix != NULL );
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		m_a_projection44[ eViewtype ] = *pMatrix;
	}

	void Set_screen				( const rmPvsViewType eViewtype, const Matrix44 *pMatrix )
	{
		FastAssert( pMatrix != NULL );
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		m_a_screen44[ eViewtype ] = *pMatrix;
	}

	void Set_modelview			( const rmPvsViewType eViewtype, const Matrix44 *pMatrix )
	{
		FastAssert( pMatrix != NULL );
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		m_a_modelview44[ eViewtype ] = *pMatrix;
	}

	void Set_composite			( const rmPvsViewType eViewtype, const Matrix44 *pMatrix )
	{
		FastAssert( pMatrix != NULL );
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		m_a_composite44[ eViewtype ] = *pMatrix;
	}

	void Set_fullcomposite		( const rmPvsViewType eViewtype, const Matrix44 *pMatrix )
	{
		FastAssert( pMatrix != NULL );
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		m_a_fullcomposite44[ eViewtype ] = *pMatrix;
	}

	//  [3/28/2006]--------------------------------------------------------------
	// 'GET' FUNCTIONS

	const Matrix44 &Get_world					( rmPvsViewType eViewtype )
	{
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		return m_a_world44[ eViewtype ];
	}

	const Matrix44 &Get_view					( rmPvsViewType eViewtype )
	{
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		return m_a_view44[ eViewtype ];
	}

	const Matrix44 &Get_projection				( rmPvsViewType eViewtype )
	{
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		return m_a_projection44[ eViewtype ];
	}

	const Matrix44 &Get_screen					( rmPvsViewType eViewtype )
	{
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		return m_a_screen44[ eViewtype ];
	}

	const Matrix44 &Get_modelview				( rmPvsViewType eViewtype )
	{
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		return m_a_modelview44[ eViewtype ];
	}

	const Matrix44 &Get_composite				( rmPvsViewType eViewtype )
	{
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		return m_a_composite44[ eViewtype ];
	}

	const Matrix44 &Get_fullcomposite			( rmPvsViewType eViewtype )
	{
		FastAssert( ((int)eViewtype >= 0) && ((int)eViewtype < (int)pvsvt_NumViewTypes) );
		return m_a_fullcomposite44[ eViewtype ];
	}

};
//-----------------------------------------------------------------------------


#endif // RM_OCCLUDE_OCCLUDER_VIEW_H
