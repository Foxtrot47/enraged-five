// 
// rmocclude/occluder_poly.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef RM_OCCLUDE_OCCLUDER_POLY_H
#define RM_OCCLUDE_OCCLUDER_POLY_H

bool TestSphereToNgonHalfspace (
								const Vector3 & center,
								const float radius,
								const int   numVerts,
								const Vector3 *verts,
								const Vector4 * polygonPlane );

/*
extern "C"	{

	bool TestSphereToNgonHalfspace (
		const Vector3 & center,
		const float radius,
		const int   numVerts,
		const Vector3 *verts,
		const Vector4 * polygonPlane );
}// extern "C"
*/

#endif // RM_OCCLUDE_OCCLUDER_POLY_H
