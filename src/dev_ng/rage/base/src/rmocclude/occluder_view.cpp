// occluder_view.cpp 

#include "occluder.h"
//#include "occluder_view.h"

/*
#include "bank/bank.h"
#include "data/resource.h"

#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"

#include "diag/memstats.h"
#include "system/timemgr.h"
#include "string/string.h"
#include "system/param.h"

#include "grcore/viewport.h"
*/

namespace rage {

} // namespace rage
using namespace rage;

//-----------------------------------------------------------------------------
PvsViewer::PvsViewer(void)
{
	Vector3	position( 0.f, 0.f, 0.f);
	Vector3 viewDirection( 0.f, 0.f, 1.f );

	m_CamMtx.Identity();

	SetView( position, viewDirection );

	m_DrawAllOccluders				= false;
	m_IsOrthogonal					= false;

}

void	PvsViewer::SetView( const Vector3 &position, const Vector3 &viewDirection )
{
	m_position.Set( position );
	m_viewDirection.Set( viewDirection );
}


void	PvsViewer::SetCamMatrix( const Matrix34 *pCameraMatrix )
{
	Assert( pCameraMatrix );

	m_CamMtx = *pCameraMatrix;// make copy
}


#define VMX_CLIP_STATUS		(__XENON)

#if __XENON
// _ReadWriteBarrier intrinsic -- seems to have no effect on local variables.
extern "C" void _ReadWriteBarrier();
#pragma intrinsic(_ReadWriteBarrier)
#endif

// This is an integer that is always zero. Since the compiler doesn't know that
// it is always zero it inhibits certain undesirable compiler optimizations and
// prevents unwanted read/write reodering.
int g_alwaysZeroOffset = 0;

pvsVtxClipFlags PvsViewer::SphereFrustumCheck_ClipStatus( Vector4::Param sphere )
{
	pvsVtxClipFlags	clipStatus = 0U;
	// VMX version of this function from Bruce Dawson of Microsoft. It removes all the floating
	// point compares
#if	VMX_CLIP_STATUS
	__vector4 vSphere = __lvx(&sphere, 0);
	__vector4 vRadius = __vspltw(vSphere, 3);
	__vector4 negRadius = __vsubfp(__vzero(), vRadius);

	__vector4 vX = __vspltw(vSphere, 0);
	__vector4 vY = __vspltw(vSphere, 1);
	__vector4 vZ = __vspltw(vSphere, 2);

	// Multiply by x and add in the values that would be multiplied by w=1.0f
	__vector4 vResult0 = __vmaddfp(vX, m_CullMtx0.a, m_CullMtx0.d);

	// This could probably be scheduled/interleaved slightly better.
	vResult0 = __vmaddfp(vY, m_CullMtx0.b, vResult0);
	vResult0 = __vmaddfp(vZ, m_CullMtx0.c, vResult0);

	// Multiply by x and add in the values that would be multiplied by w=1.0f
	__vector4 vResult1 = __vmaddfp(vX, m_CullMtx1.a, m_CullMtx1.d);

	// This could probably be scheduled/interleaved slightly better.
	vResult1 = __vmaddfp(vY, m_CullMtx1.b, vResult1);
	vResult1 = __vmaddfp(vZ, m_CullMtx1.c, vResult1);

	// Declare an array to store the results. An array is used instead of four
	// individually named variables because this helps convince the compiler to
	// write all four values to memory before trying to read any of them
	// back in.
	__vector4 vResults[4];

	// Do four compares, each generating three 32-bit bitmasks (w-component
	// is invalid) and store them to the array.
	__stvx(__vcmpgtfp(vResult0, vRadius), vResults, 0);     // vPos0
	__stvx(__vcmpgtfp(vResult1, vRadius), vResults, 16);    // vPos1
	__stvx(__vcmpgtfp(negRadius, vResult0), vResults, 32);  // vNeg0
	__stvx(__vcmpgtfp(negRadius, vResult1), vResults, 48);  // vNeg1

#if __XENON
	// This is an attempt to force the compiler to finish the stores above
	// before doing any loads. It seems to have no effect.
	_ReadWriteBarrier();
#endif

	// Get a pointer to the start of the array. Use the offset so that the
	// compiler assumes interference with all elements of the array, to
	// guarantee it will do all stores before any reads.
	__vector4* vResultsReader = vResults + g_alwaysZeroOffset;

	// Now we need some tricky/cool way of shifting/permuting the results
	// into fewer bytes. We could use VMX, but it is probably faster
	// (and is certainly simpler) to just use the integer unit.
	pvsVtxClipFlags flagsPos0 = (vResultsReader[0].u[0] & pvscull_frust_plane_posL) | (vResultsReader[0].u[1] & pvscull_frust_plane_posR) | (vResultsReader[0].u[2] & pvscull_frust_plane_posN);
	pvsVtxClipFlags flagsNeg0 = (vResultsReader[2].u[0] & pvscull_frust_plane_negL) | (vResultsReader[2].u[1] & pvscull_frust_plane_negR) | (vResultsReader[2].u[2] & pvscull_frust_plane_negN);
	pvsVtxClipFlags flagsPos1 = (vResultsReader[1].u[0] & pvscull_frust_plane_posB) | (vResultsReader[1].u[1] & pvscull_frust_plane_posT) | (vResultsReader[1].u[2] & pvscull_frust_plane_posF);
	pvsVtxClipFlags flagsNeg1 = (vResultsReader[3].u[0] & pvscull_frust_plane_negB) | (vResultsReader[3].u[1] & pvscull_frust_plane_negT) | (vResultsReader[3].u[2] & pvscull_frust_plane_negF);

	clipStatus = flagsPos0 | flagsNeg0 | flagsPos1 | flagsNeg1;
#elif __PPU
	__vector4 vSphere = sphere;
	__vector4 vRadius = __vspltw(vSphere, 3);
	__vector4 negRadius = __vsubfp(_vzero, vRadius);

	__vector4 vX = __vspltw(vSphere, 0);
	__vector4 vY = __vspltw(vSphere, 1);
	__vector4 vZ = __vspltw(vSphere, 2);

	// Multiply by x and add in the values that would be multiplied by w=1.0f
	__vector4 vResult0 = __vmaddfp(vX, m_CullMtx0.a, m_CullMtx0.d);

	// This could probably be scheduled/interleaved slightly better.
	vResult0 = __vmaddfp(vY, m_CullMtx0.b, vResult0);
	vResult0 = __vmaddfp(vZ, m_CullMtx0.c, vResult0);

	// Multiply by x and add in the values that would be multiplied by w=1.0f
	__vector4 vResult1 = __vmaddfp(vX, m_CullMtx1.a, m_CullMtx1.d);

	// This could probably be scheduled/interleaved slightly better.
	vResult1 = __vmaddfp(vY, m_CullMtx1.b, vResult1);
	vResult1 = __vmaddfp(vZ, m_CullMtx1.c, vResult1);

	// Declare an array to store the results. An array is used instead of four
	// individually named variables because this helps convince the compiler to
	// write all four values to memory before trying to read any of them
	// back in.
	__vector4 vResults[4];

	// Do four compares, each generating three 32-bit bitmasks (w-component
	// is invalid) and store them to the array.
	vResults[0] = (__vector4)__vcmpgtfp(vResult0, vRadius);    // vPos0
	vResults[1] = (__vector4)__vcmpgtfp(vResult1, vRadius);    // vPos1
	vResults[2] = (__vector4)__vcmpgtfp(negRadius, vResult0);  // vNeg0
	vResults[3] = (__vector4)__vcmpgtfp(negRadius, vResult1);  // vNeg1

	// Get a pointer to the start of the array. Use the offset so that the
	// compiler assumes interference with all elements of the array, to
	// guarantee it will do all stores before any reads.
	Vector4* vResultsReader = (Vector4*)vResults + g_alwaysZeroOffset;

	// Now we need some tricky/cool way of shifting/permuting the results
	// into fewer bytes. We could use VMX, but it is probably faster
	// (and is certainly simpler) to just use the integer unit.
	Vector4 vPos0(vResultsReader[0]);
	Vector4 vPos1(vResultsReader[1]);
	Vector4 vNeg0(vResultsReader[2]);
	Vector4 vNeg1(vResultsReader[3]);

	pvsVtxClipFlags flagsPos0 = (vPos0.ix & pvscull_frust_plane_posL) | (vPos0.iy & pvscull_frust_plane_posR) | (vPos0.iz & pvscull_frust_plane_posN);
	pvsVtxClipFlags flagsNeg0 = (vNeg0.ix & pvscull_frust_plane_negL) | (vNeg0.iy & pvscull_frust_plane_negR) | (vNeg0.iz & pvscull_frust_plane_negN);
	pvsVtxClipFlags flagsPos1 = (vPos1.ix & pvscull_frust_plane_posB) | (vPos1.iy & pvscull_frust_plane_posT) | (vPos1.iz & pvscull_frust_plane_posF);
	pvsVtxClipFlags flagsNeg1 = (vNeg1.ix & pvscull_frust_plane_negB) | (vNeg1.iy & pvscull_frust_plane_negT) | (vNeg1.iz & pvscull_frust_plane_negF);

	clipStatus = flagsPos0 | flagsNeg0 | flagsPos1 | flagsNeg1;
#else
	// Alias for sphere
	const Vector4 &v = sphere;
	// bdawson - note that constructing this object means that the call
	// to Dot will cause a load-hit-store. It would be better to have
	// a version of Dot that ignores w or else modify and pass the new value
	// in VMX or FPU registers.
	//Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
	//Vector4 distancesLRNZ;                                 // left, right, near and z distance
	//Vector4 distancesBTF;                                  // bottom, top and far distances

	float	radius = v.w;

	// fully outside?
	//distancesLRNZ.Dot(center,m_CullMtx0); //test first 3 planes.

	// Do the calculation inline to avoid function call overhead and
	// load-hit-stores, and to allow specialization (not using the w component)
	// Note that the components that would normally be multiplied by w are not
	// (it is assumed to be 1.0f) and they are added early to make better use
	// of fmadds.
	float tempx = v.x * m_CullMtx0.a.x + m_CullMtx0.d.x + v.y * m_CullMtx0.b.x + v.z * m_CullMtx0.c.x;
	float tempy = v.x * m_CullMtx0.a.y + m_CullMtx0.d.y + v.y * m_CullMtx0.b.y + v.z * m_CullMtx0.c.y;
	float tempz = v.x * m_CullMtx0.a.z + m_CullMtx0.d.z + v.y * m_CullMtx0.b.z + v.z * m_CullMtx0.c.z;

	if (tempx > radius)
		clipStatus |= pvscull_frust_plane_posL;
	else if (tempx < -radius)
		clipStatus |= pvscull_frust_plane_negL;

	if (tempy > radius)
		clipStatus |= pvscull_frust_plane_posR;
	else if (tempy < -radius)
		clipStatus |= pvscull_frust_plane_negR;

	if (tempz > radius)
		clipStatus |= pvscull_frust_plane_posN;
	else if (tempz < -radius)
		clipStatus |= pvscull_frust_plane_negN;

	//distancesBTF.Dot(center,m_CullMtx1);//test other 3 planes.
	tempx = v.x * m_CullMtx1.a.x + m_CullMtx1.d.x + v.y * m_CullMtx1.b.x + v.z * m_CullMtx1.c.x;
	tempy = v.x * m_CullMtx1.a.y + m_CullMtx1.d.y + v.y * m_CullMtx1.b.y + v.z * m_CullMtx1.c.y;
	tempz = v.x * m_CullMtx1.a.z + m_CullMtx1.d.z + v.y * m_CullMtx1.b.z + v.z * m_CullMtx1.c.z;
	if (tempx > radius)
		clipStatus |= pvscull_frust_plane_posB;
	else if (tempx < -radius)
		clipStatus |= pvscull_frust_plane_negB;

	if (tempy > radius)
		clipStatus |= pvscull_frust_plane_posT;
	else if (tempy < -radius)
		clipStatus |= pvscull_frust_plane_negT;

	if (tempz > radius)
		clipStatus |= pvscull_frust_plane_posF;
	else if (tempz < -radius)
		clipStatus |= pvscull_frust_plane_negF;

/*	static int success = 0;
	static int fail = 0;
	if (clipStatus != vClipStatus)
	{
		fail++;
		__debugbreak();
	}
	else
	{
		success++;
	}*/
#endif
	return clipStatus;// only low 12bits are valid, so it doesnt have to be unsigned.
}

pvsVtxClipFlags PvsViewer::PointFrustumCheck_ClipStatus( Vector4::Param point )
{
	pvsVtxClipFlags	clipStatus = 0U;
	// VMX version of this function from Bruce Dawson of Microsoft. It removes all the floating
	// point compares
#if	VMX_CLIP_STATUS
	__vector4 vPoint = __lvx(&point, 0);
	__vector4 vZero = _vzerofp;

	__vector4 vX = __vspltw(vPoint, 0);
	__vector4 vY = __vspltw(vPoint, 1);
	__vector4 vZ = __vspltw(vPoint, 2);

	// Multiply by x and add in the values that would be multiplied by w=1.0f
	__vector4 vResult0 = __vmaddfp(vX, m_CullMtx0.a, m_CullMtx0.d);

	// This could probably be scheduled/interleaved slightly better.
	vResult0 = __vmaddfp(vY, m_CullMtx0.b, vResult0);
	vResult0 = __vmaddfp(vZ, m_CullMtx0.c, vResult0);

	// Multiply by x and add in the values that would be multiplied by w=1.0f
	__vector4 vResult1 = __vmaddfp(vX, m_CullMtx1.a, m_CullMtx1.d);

	// This could probably be scheduled/interleaved slightly better.
	vResult1 = __vmaddfp(vY, m_CullMtx1.b, vResult1);
	vResult1 = __vmaddfp(vZ, m_CullMtx1.c, vResult1);

	// Declare an array to store the results. An array is used instead of four
	// individually named variables because this helps convince the compiler to
	// write all four values to memory before trying to read any of them
	// back in.
	__vector4 vResults[4];

	// Do four compares, each generating three 32-bit bitmasks (w-component
	// is invalid) and store them to the array.
	__stvx(__vcmpgtfp(vResult0, vZero), vResults, 0);     // vPos0
	__stvx(__vcmpgtfp(vResult1, vZero), vResults, 16);    // vPos1
	__stvx(__vcmpgtfp(vZero, vResult0), vResults, 32);  // vNeg0
	__stvx(__vcmpgtfp(vZero, vResult1), vResults, 48);  // vNeg1

#if __XENON
	// This is an attempt to force the compiler to finish the stores above
	// before doing any loads. It seems to have no effect.
	_ReadWriteBarrier();
#endif

	// Get a pointer to the start of the array. Use the offset so that the
	// compiler assumes interference with all elements of the array, to
	// guarantee it will do all stores before any reads.
	__vector4* vResultsReader = vResults + g_alwaysZeroOffset;

	// Now we need some tricky/cool way of shifting/permuting the results
	// into fewer bytes. We could use VMX, but it is probably faster
	// (and is certainly simpler) to just use the integer unit.
	pvsVtxClipFlags flagsPos0 = (vResultsReader[0].u[0] & pvscull_frust_plane_posL) | (vResultsReader[0].u[1] & pvscull_frust_plane_posR) | (vResultsReader[0].u[2] & pvscull_frust_plane_posN);
	pvsVtxClipFlags flagsNeg0 = (vResultsReader[2].u[0] & pvscull_frust_plane_negL) | (vResultsReader[2].u[1] & pvscull_frust_plane_negR) | (vResultsReader[2].u[2] & pvscull_frust_plane_negN);
	pvsVtxClipFlags flagsPos1 = (vResultsReader[1].u[0] & pvscull_frust_plane_posB) | (vResultsReader[1].u[1] & pvscull_frust_plane_posT) | (vResultsReader[1].u[2] & pvscull_frust_plane_posF);
	pvsVtxClipFlags flagsNeg1 = (vResultsReader[3].u[0] & pvscull_frust_plane_negB) | (vResultsReader[3].u[1] & pvscull_frust_plane_negT) | (vResultsReader[3].u[2] & pvscull_frust_plane_negF);

	clipStatus = flagsPos0 | flagsNeg0 | flagsPos1 | flagsNeg1;
#else
	// Alias for sphere
	const Vector4 &v = point;
	// bdawson - note that constructing this object means that the call
	// to Dot will cause a load-hit-store. It would be better to have
	// a version of Dot that ignores w or else modify and pass the new value
	// in VMX or FPU registers.
	//Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
	//Vector4 distancesLRNZ;                                 // left, right, near and z distance
	//Vector4 distancesBTF;                                  // bottom, top and far distances

	// fully outside?
	//distancesLRNZ.Dot(center,m_CullMtx0); //test first 3 planes.

	// Do the calculation inline to avoid function call overhead and
	// load-hit-stores, and to allow specialization (not using the w component)
	// Note that the components that would normally be multiplied by w are not
	// (it is assumed to be 1.0f) and they are added early to make better use
	// of fmadds.
	float tempx = v.x * m_CullMtx0.a.x + m_CullMtx0.d.x + v.y * m_CullMtx0.b.x + v.z * m_CullMtx0.c.x;
	float tempy = v.x * m_CullMtx0.a.y + m_CullMtx0.d.y + v.y * m_CullMtx0.b.y + v.z * m_CullMtx0.c.y;
	float tempz = v.x * m_CullMtx0.a.z + m_CullMtx0.d.z + v.y * m_CullMtx0.b.z + v.z * m_CullMtx0.c.z;

	if (tempx > 0.0f)
		clipStatus |= pvscull_frust_plane_posL;
	else if (tempx < 0.0f)
		clipStatus |= pvscull_frust_plane_negL;

	if (tempy > 0.0f)
		clipStatus |= pvscull_frust_plane_posR;
	else if (tempy < 0.0f)
		clipStatus |= pvscull_frust_plane_negR;

	if (tempz > 0.0f)
		clipStatus |= pvscull_frust_plane_posN;
	else if (tempz < 0.0f)
		clipStatus |= pvscull_frust_plane_negN;

	//distancesBTF.Dot(center,m_CullMtx1);//test other 3 planes.
	tempx = v.x * m_CullMtx1.a.x + m_CullMtx1.d.x + v.y * m_CullMtx1.b.x + v.z * m_CullMtx1.c.x;
	tempy = v.x * m_CullMtx1.a.y + m_CullMtx1.d.y + v.y * m_CullMtx1.b.y + v.z * m_CullMtx1.c.y;
	tempz = v.x * m_CullMtx1.a.z + m_CullMtx1.d.z + v.y * m_CullMtx1.b.z + v.z * m_CullMtx1.c.z;
	if (tempx > 0.0f)
		clipStatus |= pvscull_frust_plane_posB;
	else if (tempx < 0.0f)
		clipStatus |= pvscull_frust_plane_negB;

	if (tempy > 0.0f)
		clipStatus |= pvscull_frust_plane_posT;
	else if (tempy < 0.0f)
		clipStatus |= pvscull_frust_plane_negT;

	if (tempz > 0.0f)
		clipStatus |= pvscull_frust_plane_posF;
	else if (tempz < 0.0f)
		clipStatus |= pvscull_frust_plane_negF;

	/*	static int success = 0;
	static int fail = 0;
	if (clipStatus != vClipStatus)
	{
	fail++;
	__debugbreak();
	}
	else
	{
	success++;
	}*/
#endif
	return clipStatus;// only low 12bits are valid, so it doesnt have to be unsigned.
}

//-----------------------------------------------------------------------------



