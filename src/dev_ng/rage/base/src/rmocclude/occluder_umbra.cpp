// occluder_umbra.cpp 

#include "occluder_umbra.h"
#include "occluder.h"



namespace rage {

}// namespace rage



// After edge visibility bits are set by determining front/back facing polygons,
// we can connect an unsorted list of edges into a closed n-gon (this only works if the object is convex).
//
// 32 edge limit.
//
// returns the number of active edges (which is also the number of vertices the ngon uses coincidentally)
//
int	rmOccludePolyhedron::CreateSilhouettePolygonFromActiveEdges( t_polyEdgeData *pOuputEdgeData, const u32 activeEdgeBits )
{
	int		unsortedEdges[32];
	int		unsorted = 0;		// clear count
	int		head=0;				// clear count
	int		numActiveEdges = 0;	// clear count

	int		totalEdges = this->m_num_edges;
	int		i;

#if __DEV
	Assert( pOuputEdgeData != NULL );// make sure output buffer is valid.
#else
	if( pOuputEdgeData == NULL ) return 0;
#endif

	// get 1st entry, place it in the 0th slot.
	u32 flag = 1;
	for( i=0; i < totalEdges; i++)//check all bits.
	{
		// is this edge active?
		if( flag & activeEdgeBits )
		{
			// handle reversing?
			pOuputEdgeData[ head ].edge_idx = (s8)i;			
			pOuputEdgeData[ head ].e0 = (s8)m_pEdges[i].GetEdge(0);
			pOuputEdgeData[ head ].e1 = (s8)m_pEdges[i].GetEdge(1);            

			//Displayf(" edge index %d active e0 %d e1 %d ", i, pOuputEdgeData[ head ].e0, pOuputEdgeData[ head ].e1 );

			head++;
			i++;
			numActiveEdges++;// track total edges considered 'active'.
			break;
		}
		flag <<= 1;
	}
	Assert( numActiveEdges <= 32 );// too many edges! must be a bug because unique edge list per polyhedron is capped at 32 edges.


	Assert( head == 1 );// make sure we grabbed a valid starting edge.

	// loop through rest of bits. (not starting from 0, continuing after the 'head' edge we added.
	flag = 1<<i;
	for( i=i; i < totalEdges; i++)
	{
		// is this edge active?
		if( flag & activeEdgeBits )
		{
			//Displayf(" edge index %d active e0 %d e1 %d ", i, m_pEdges[i].GetEdge(0), m_pEdges[i].GetEdge(1) );

			unsortedEdges[ unsorted ] = i;
			unsorted++;
			numActiveEdges++;// track total edges considered 'active'.
			//Displayf(" edge index %d active ", i );
			Assert( numActiveEdges <= 32 );// too many edges! must be a bug because unique edge list per polyhedron is capped at 32 edges.
		}
		flag <<= 1;
	}
	Assert( numActiveEdges <= 32 );// too many edges! must be a bug because unique edge list per polyhedron is capped at 32 edges.

	//Displayf(" totalEdges %d numActiveEdges %d", totalEdges, numActiveEdges );

	//
	// FIXME: TODO: OPTIMIZE: Crappy N^2 evaluation, change this to use a linked list of unsorted edges instead to speed it up.
	//
	char prevEdgeVtxIdx = pOuputEdgeData[ 0 ].e1;

	for( int k=0; k < unsorted; k++)// already have 1 edge.
	{
		// loop through unsorted list, adding edges to silhouette as we find matches.
		for( int e=0; e < unsorted; e++)// already have 1 edge.
		{
			char edgeIndex = (s8)unsortedEdges[e];// get index

			if( edgeIndex != -1)
			{
				char e0;
				char e1;

				e0 = (s8)m_pEdges[ edgeIndex ].GetEdge(0);
				e1 = (s8)m_pEdges[ edgeIndex ].GetEdge(1);

				if( prevEdgeVtxIdx == e0 )// edge continues with same winding.
				{					
					pOuputEdgeData[ head ].edge_idx = edgeIndex;			
					pOuputEdgeData[ head ].e0 = e0;
					pOuputEdgeData[ head ].e1 = e1;

					prevEdgeVtxIdx = e1;// remember trailing edge vertex index.
					head++;

					unsortedEdges[e] = -1;// remove this edge from remaining candidate list.

					/*
					// wap to other unused edge buffer.
					break;// end loop test
					*/
				}
				else if( prevEdgeVtxIdx == e1 )// edge reverse
				{
					edgeIndex = (s8)unsortedEdges[e];// get index
					pOuputEdgeData[ head ].edge_idx = (s8)edgeIndex;			
					pOuputEdgeData[ head ].e0 = (s8)e1;
					pOuputEdgeData[ head ].e1 = (s8)e0;

					prevEdgeVtxIdx = e0;// remember trailing edge vertex index.
					head++;

					unsortedEdges[e] = -1;// remove this edge from remaining candidate list.

					/*
					// wap to other unused edge buffer.
					break;
					*/
				}
				else
				{
					// TODO: have this unused edge pushed into the 2nd buffer, switching over to it when the inner loop is completed,
					/*

					pOtherUnsortedEdges[ pOtherUnsortedCount ] = edgeIndex;
					*pOtherUnsortedCount++;

					*/

					// this edge doesn't continue off the current one's 'end' look for another fit.
				}	
			}
		}
		/*
		swap to other unused edge buffer.
		*/
	}

	if( pOuputEdgeData[ numActiveEdges-1 ].e1 != pOuputEdgeData[ 0 ].e0 )
	{
		Displayf(" head %d numActiveEdges %d ", head, numActiveEdges );
		for( int ed=0; ed < numActiveEdges; ed++)
		{

			Displayf(" ed %d ei %d  e0 %d  e1 %d ",
				ed, pOuputEdgeData[ed].edge_idx,
				pOuputEdgeData[ed].e0, pOuputEdgeData[ed].e1 );									
		}
		Assert( 0 );
	}

	Assert( pOuputEdgeData[ numActiveEdges-1 ].e1 == pOuputEdgeData[ 0 ].e0 );// make sure they wrap and connect properly, closing the polygon.
	Assert( head == numActiveEdges );// make sure we got all the edge data

	return numActiveEdges;
}
