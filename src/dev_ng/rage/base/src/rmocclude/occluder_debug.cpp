// occluder_debug.cpp 
#if 0
#include "bank/bank.h"
#include "data/resource.h"

#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"

#include "diag/memstats.h"
#include "system/timemgr.h"
#include "string/string.h"
#include "system/param.h"
#include "grcore/font.h"
#include "grcore/device.h"
#include "grcore/viewport.h"
#include "grcore/state.h"
#include "grcore/im.h"
#include "occluder.h"
//namespace rage {

extern int	    g_OccludedDrawBoundTotal;// FIXME! //  [3/8/2006 ccoffin]


void	OccludedBoundInfo::Render(void)
{

	switch( GetType() )
	{
		case pvsbt_Null:		
		case pvsbt_Vertex:	
		case pvsbt_Box:		
		case pvsbt_Capsule:	
		default:
			break;

		case pvsbt_Sphere:
			break;
	}
}



bool rmOccluderSystem::AddBufferedDebugDrawVolume(	const Vector3				&DEV_ONLY(center),
													const float					DEV_ONLY(radius),
													//const Color32				&DEV_ONLY(debugdrawcolor),
													const pvsViewerID			DEV_ONLY(id),
													const rmPvsCullStatusBits	DEV_ONLY(statusbits)
													)
{
#if __DEV
	int index = sm_OccludedDrawBoundTotal;
	if( sm_OccludedDrawBoundTotal < sm_maxDebugOccluders )
	{
		sm_a_bndInfo[ index ].m_center = center;
		sm_a_bndInfo[ index ].m_radius = radius;
		sm_a_bndInfo[ index ].m_debugDrawColor = GetOccludedVolumeDebugColor();
		sm_a_bndInfo[ index ].m_occludedViewID = id;
		sm_a_bndInfo[ index ].m_eStatusBits = statusbits;
		sm_OccludedDrawBoundTotal++;
		return true;
	}
#endif
	return false; // not added.
}

void rmOccluderSystem::DrawDebugView_Frustum( const pvsViewerID ASSERT_ONLY(id), const rmPvsDebugDrawModeBits ASSERT_ONLY(drawmodebits) )
{
#if __DEV
	Assert( drawmodebits != pvsdd_AllBits_mask );
	Assert( id >= 0 );
#endif
#if 0// __DEV

	// todo: if we do a debug draw in a window, copy the original view's backbuffer into the smaller window, then do all the debug drawing in here overlaid on the smaller debug window view!
	Assert( drawmodebits != pvsdd_AllBits_mask );

	grcWorldMtx( M34_IDENTITY );
	PvsViewer	*pviewer = GetPvsViewerPtr(id);
	Vector3 pvsViewPos = pviewer->GetViewPosition();
	grcState::SetDepthFunc(grcdfLessEqual);

	rmPvsViewType	eRender_viewtype = pvsvt_PVS;

	grcViewport* oldViewport=grcViewport::GetCurrent();
	if( 0 )
	{
		grcViewport::SetCurrent( pviewer->GetPerspectiveViewport_render() );
	}else{
		grcViewport::SetCurrent( pviewer->GetPerspectiveViewport_pvs() );
	}
	grcWorldMtx( M34_IDENTITY );


	//---------------------------------------------------------------------
	if( sm_DebugDrawFrustum)
	{

		/*
		Draw frustum (facing into -Z)
		(-+)(++)
		(--)(+-)

		near  far
		0-1   4-5 
		| |   | |
		2-3   6-7
		*/



		Vector3	frustumCornerRays[4];	// Vectors that trace from the eyepoint and define/intersect
										//   the corner edges of the frustum. (running through the near+far clipping plane eges.
		Vector3	frustumCorners[8];


		Vector3 frustumRingColor( .2f, .75f, 0.4f );
		Vector3 frustumColor( .75f, .75f, 0.f );
		Vector3 frustumColorFaded( 0.3f, 0.3f,0.f );

		//float attenuate_farclip_scale = 1.f;//0.2f;// make a widget for this!
		//float guardscale	=1.f;

		//float aspect_y		= 1.f;// / pviewer->Get_AspectRatio();// pviewer->Get_AspectRatio();
		//float aspect_x		= 1.f;//pviewer->Get_AspectRatio();

		//float cosHFOV = pviewer->Get_cosHFOV();
		//float sinHFOV = pviewer->Get_sinHFOV();
		//float cosVFOV = pviewer->Get_cosVFOV();
		//float sinVFOV = pviewer->Get_sinVFOV();

		//float magHorz=1.0f/sqrtf(cosHFOV*cosHFOV+sinHFOV*sinHFOV);
		//float magVert=1.0f/sqrtf(cosVFOV*cosVFOV+sinVFOV*sinVFOV);

		float off_x_near = pviewer->Get_cosVFOV() * pviewer->Get_ZClipNear();
		float off_y_near = pviewer->Get_sinHFOV() * pviewer->Get_ZClipNear();
		float off_z_near = pviewer->Get_ZClipNear();
		// TODO: handle aspect ratio!

		
		float off_x_far = pviewer->Get_cosVFOV() * pviewer->Get_ZClipFar();
		float off_y_far = pviewer->Get_sinHFOV() * pviewer->Get_ZClipFar();
		float off_z_far = pviewer->Get_ZClipFar();


		frustumCorners[0].Set( -off_x_near,  off_y_near, -off_z_near );
		frustumCorners[1].Set(  off_x_near,  off_y_near, -off_z_near );
		frustumCorners[2].Set( -off_x_near, -off_y_near, -off_z_near );
		frustumCorners[3].Set(  off_x_near, -off_y_near, -off_z_near );

		frustumCorners[4].Set( -off_x_far,  off_y_far, -off_z_far );
		frustumCorners[5].Set(  off_x_far,  off_y_far, -off_z_far );
		frustumCorners[6].Set( -off_x_far, -off_y_far, -off_z_far );
		frustumCorners[7].Set(  off_x_far, -off_y_far, -off_z_far );

		// corners are defined properly, create 'rays'
		frustumCornerRays[0] = frustumCorners[4]-frustumCorners[0];
		frustumCornerRays[1] = frustumCorners[5]-frustumCorners[1];
		frustumCornerRays[3] = frustumCorners[7]-frustumCorners[3];
		frustumCornerRays[2] = frustumCorners[6]-frustumCorners[2];
		// normalize!
		frustumCornerRays[0].Normalize();
		frustumCornerRays[1].Normalize();
		frustumCornerRays[2].Normalize();
		frustumCornerRays[3].Normalize();

		// set matrix to transform local->world.
		Matrix44 view44 = pviewer->Get_view( eRender_viewtype );
		Matrix34 view34;
		Convert( view34, view44 );
		view34.Inverse();
		grcWorldMtx( view34 );
		Vector3	opaqueFrustumColor;
		Vector3	opaqueFrustumColorFaded;
		//---------------------------------------------------------------------
		//--- Do all rendering related to frustum visualization after this point!
		//---------------------------------------------------------------------


#if 0
		//---------------------------------------------------------------------
		// sm_DrawFrustumVolumeDistance

		// TODO: 
		// 1) get vector from naer->far clip plane edges.
		// 2) normalize
		// 3) scale by 'sm_DrawUmbraVolumeDistance'

		//4) save normalized version and support multiple frustum 'rings' drawn at spaced intervals.
		// bool sm_DrawFrustumRings
		// float sm_DrawFrustumRingsInterval

		float	dt = rmOccluderSystem::GetDebugAnimTimeDelta();
		static float s_offset;
		s_offset += dt;
		float	spacingInterval = 15.f;

		float whole;
		float t_number = (s_offset / spacingInterval);
		float frac = modf( t_number, &whole )*spacingInterval;
		
		Vector3	cornerStep[4];
		cornerStep[0] = frustumCorners[0];
		cornerStep[1] = frustumCorners[1];
		cornerStep[2] = frustumCorners[2];
		cornerStep[3] = frustumCorners[3];
//		cornerStep[0] += frustumCornerRays[0] * frac;
//		cornerStep[1] += frustumCornerRays[1] * frac;
//		cornerStep[2] += frustumCornerRays[2] * frac;
//		cornerStep[3] += frustumCornerRays[3] * frac;

			
		for(int i=0; i< 10; i++ )
		{
			grcBegin(drawLineStrip, 5);
				grcColor3f( frustumRingColor );
					grcVertex3f( cornerStep[0] );
					grcVertex3f( cornerStep[1] );
					grcVertex3f( cornerStep[3] );
					grcVertex3f( cornerStep[2] );
					grcVertex3f( cornerStep[0] );
			grcEnd();

			cornerStep[0] += (frustumCornerRays[0] * spacingInterval);
			cornerStep[1] += (frustumCornerRays[1] * spacingInterval);
			cornerStep[2] += (frustumCornerRays[2] * spacingInterval);
			cornerStep[3] += (frustumCornerRays[3] * spacingInterval);

			cornerStep[0] += (frustumCornerRays[0] * frac);
			cornerStep[1] += (frustumCornerRays[1] * frac);
			cornerStep[2] += (frustumCornerRays[2] * frac);
			cornerStep[3] += (frustumCornerRays[3] * frac);

		}
		//---------------------------------------------------------------------
#endif
		//---------------------------------------------------------------------
		if( sm_DebugDrawFrustumAsOpaque )
		{
			opaqueFrustumColor.Set( .7f, .7f, .8f );
			opaqueFrustumColorFaded.Set( .2f, .2f, .3f );

			grcState::SetAlphaBlend(false);
			grcState::SetBlendSet(grcbsNormal);
			grcState::SetDepthTest(true);
			grcState::SetDepthWrite(true);

		}else{

			opaqueFrustumColor.Set( .31f, .31f, .315f );
			opaqueFrustumColorFaded.Set( .205f, .205f, .21f );
			grcState::SetAlphaBlend(true);
			grcState::SetBlendSet(grcbsSubtract);
			grcState::SetDepthTest(true);
			grcState::SetDepthWrite(false);
		}
		/*
		Draw frustum
		(-+)(++)
		(--)(+-)
		01 45 
		23 67
		*/
		// draw frustum planes
		if( !sm_DebugDrawFrustumEdgesOnly )
		{
			grcBegin(drawTriStrip, 4);
			grcColor3f( opaqueFrustumColor );
			grcVertex3f( frustumCorners[0] );
			grcVertex3f( frustumCorners[1] );

			grcColor3f( opaqueFrustumColorFaded );
			grcVertex3f( frustumCorners[4] );
			grcVertex3f( frustumCorners[5] );
			grcEnd();


			grcBegin(drawTriStrip, 4);
			grcColor3f( opaqueFrustumColor );
			grcVertex3f( frustumCorners[1] );
			grcVertex3f( frustumCorners[3] );

			grcColor3f( opaqueFrustumColorFaded );
			grcVertex3f( frustumCorners[5] );
			grcVertex3f( frustumCorners[7] );
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcColor3f( opaqueFrustumColor );
			grcVertex3f( frustumCorners[3] );
			grcVertex3f( frustumCorners[2] );

			grcColor3f( opaqueFrustumColorFaded );
			grcVertex3f( frustumCorners[7] );
			grcVertex3f( frustumCorners[6] );
			grcEnd();

			grcBegin(drawTriStrip, 4);
			grcColor3f( opaqueFrustumColor );
			grcVertex3f( frustumCorners[2] );
			grcVertex3f( frustumCorners[0] );

			grcColor3f( opaqueFrustumColorFaded );
			grcVertex3f( frustumCorners[6] );
			grcVertex3f( frustumCorners[4] );
			grcEnd();
		}

		grcState::SetAlphaBlend(true);
		grcState::SetBlendSet(grcbsAdd);
		grcState::SetDepthFunc(grcdfLessEqual);
		grcState::SetDepthTest(true);
		grcState::SetDepthWrite(true);

		// draw near clip plane
		grcBegin(drawLineStrip, 5);
		grcColor3f(frustumColor);
		grcVertex3f( frustumCorners[0] );
		grcVertex3f( frustumCorners[1] );
		grcVertex3f( frustumCorners[3] );
		grcVertex3f( frustumCorners[2] );
		grcVertex3f( frustumCorners[0] );
		grcEnd();

		// draw far clip plane
		grcBegin(drawLineStrip, 5);
		grcColor3f(frustumColorFaded);
		grcVertex3f( frustumCorners[4] );
		grcVertex3f( frustumCorners[5] );
		grcVertex3f( frustumCorners[7] );
		grcVertex3f( frustumCorners[6] );
		grcVertex3f( frustumCorners[4] );
		grcEnd();

		// draw edges
		grcBegin(drawLineStrip, 2);
		grcColor3f(frustumColor);
		grcVertex3f( frustumCorners[0] );
		grcColor3f(frustumColorFaded);
		grcVertex3f( frustumCorners[4] );
		grcEnd();

		grcBegin(drawLineStrip, 2);
		grcColor3f(frustumColor);
		grcVertex3f( frustumCorners[1] );
		grcColor3f(frustumColorFaded);
		grcVertex3f( frustumCorners[5] );
		grcEnd();

		grcBegin(drawLineStrip, 2);
		grcColor3f(frustumColor);
		grcVertex3f( frustumCorners[2] );
		grcColor3f(frustumColorFaded);
		grcVertex3f( frustumCorners[6] );
		grcEnd();

		grcBegin(drawLineStrip, 2);
		grcColor3f(frustumColor);
		grcVertex3f( frustumCorners[3] );
		grcColor3f(frustumColorFaded);
		grcVertex3f( frustumCorners[7] );
		grcEnd();

	}
	//---------------------------------------------------------------------


	// restore old viewport:
	grcViewport::SetCurrent(oldViewport);
#endif // end #if __DEV
}

/*
void rmOccluderSystem::StoreOccludedBound()
{
	static bool sm_IsBufferingDebugBounds;
	static bool sm_OccludedDrawBoundTotal;
	static bool sm_maxDebugOccluders;
#if __DEV
	if( sm_IsBufferingDebugBounds )
	{
		if( sm_OccludedDrawBoundTotal < sm_maxDebugOccluders )
		{
			if( OccludedVolumesBuffered_GetMode() )// bound buffering on for this test?
			{
				a_bndInfo[ m_OccludedDrawBoundTotal ].center = extentA;
				a_bndInfo[ m_OccludedDrawBoundTotal ].radius = radius;
				a_bndInfo[ m_OccludedDrawBoundTotal ].debugDrawColor = GetOccludedVolumeDebugColor();
				a_bndInfo[ m_OccludedDrawBoundTotal ].occludedViewID = viewID;
				m_OccludedDrawBoundTotal++;

				a_bndInfo[ m_OccludedDrawBoundTotal ].center = extentB;
				a_bndInfo[ m_OccludedDrawBoundTotal ].radius = radius;
				a_bndInfo[ m_OccludedDrawBoundTotal ].debugDrawColor = GetOccludedVolumeDebugColor();
				a_bndInfo[ m_OccludedDrawBoundTotal ].occludedViewID = viewID;
				m_OccludedDrawBoundTotal++;
			}
		}
	}
#endif
}
*/
// FALSE by default so it overwrites onto the screen for easy visibility.
void	rmOccludePolyhedron::DebugDraw( bool IsDepthTestAndWriteDrawing )
{
	int facecount = this->GetNumFacets();

	//set these states both to TRUE if you want the umbra+frustum cut through wireframes to show points of intersection.
	bool	old_DepthTest = grcState::GetDepthTest();
	bool	old_DepthWrite = grcState::GetDepthTest();

	// special states for debug drawing this element.
	grcState::SetDepthTest( IsDepthTestAndWriteDrawing );
	grcState::SetDepthWrite( IsDepthTestAndWriteDrawing );

	// loop through faces.			
	for( int fc=0 ; fc < facecount ; fc++ )
	{
		//  [3/20/2006 ccoffin]
		// Based on debug draw mode, we may want to skip drawing or color certain faces a different color...
		//
		// Faces that create the occlusion umbra are green,
		// Faces that are culled away(backface) and lie within the umbra are colored red.
		//  if we draw them in additive mode, the edges should be colored yellow.
		// todo: support drawing the silhouette polyedges also.

		int		numverts = this->GetFacetNumVerts( fc );

		for( int vertIdx=0; vertIdx < numverts; vertIdx++)
		{
			int		i0,i1;
			int		vidx0, vidx1;
			Vector3	v0,v1;

			vidx0 = vertIdx;
			vidx1 = (vertIdx+1)%numverts;//wrap

			i0 = this->GetFacetVertIndex( fc, vidx0 );
			i1 = this->GetFacetVertIndex( fc, vidx1 );

			v0 = this->GetVertex(i0);
			v1 = this->GetVertex(i1);

			// optimize this so we send 1 large linestrip
			grcBegin(drawLineStrip, 2);						
			grcVertex3f( v0 );
			grcVertex3f( v1 );
			grcEnd();
		}
	}

	// Restore before exit.
	grcState::SetDepthTest( old_DepthTest );
	grcState::SetDepthWrite( old_DepthWrite );
}


void rmOccluderSystem::DrawDebugViewInfo( const pvsViewerID DEV_ONLY(id), const rmPvsDebugDrawModeBits DEV_ONLY(drawmodebits) )
{
#if __DEV

	//int polycount = 0;
	int		loopcount = 0;
	bool	drawActive = sm_DrawActiveOccluderVolumes;

	if (!sm_DrawOccluderVolumes)
		return;

	// todo: if we do a debug draw in a window, copy the original view's backbuffer into the smaller window, then do all the debug drawing in here overlaid on the smaller debug window view!
	Assert( drawmodebits != pvsdd_AllBits_mask );

	PvsViewer	*pviewer = GetPvsViewerPtr(id);
	Vector3		pvsViewPos = pviewer->GetViewPosition();

#if !__FINAL
	// set viewport:
	grcViewport* oldViewport=grcViewport::GetCurrent();
	grcViewport::SetCurrent( pviewer->GetPerspectiveViewport_render() );
	grcWorldMtx( M34_IDENTITY );


	Vec3V cullSphere(Vec3V::ZERO);
	char stringStr[32];
	sprintf(stringStr,"ORIGIN!");
	float x,y;

		//grcProjectBehind,		// Object has negative W, behind the player in negative clip space
		//grcProjectOffscreen, 
		//grcProjectVisible
	if(grcProject(cullSphere, x, y,
		RCC_MAT44V(pviewer->Get_view( pvsvt_Render )),
		//pviewer->GetPerspectiveViewport_render()->GetViewMtx(),
		*pviewer->GetPerspectiveViewport_render()
		) == grcProjectVisible)
	{
		grcFont::GetCurrent().DrawScaled(x, y, 1.0f, Color32(255, 255, 255), 1, 1, stringStr);
	}

	// restore old viewport:
	grcViewport::SetCurrent(oldViewport);
#endif
	//grcViewport::SetCurrent( pviewer->GetPerspectiveViewport() );
	//grcViewport::RegenerateMatrices();




/*
re-implement this! //  [3/14/2006 ccoffin]
	//-----------------------------
	// Added support to draw retained mode style bound information here - cmc	
	for(int i=0; i < g_OccludedDrawBoundTotal; i++)
	{
		if(  a_bndInfo[ i ].occludedViewID == id )
		{
			grcColor( a_bndInfo[ i ].debugDrawColor );// set color
			grcDrawSphere( a_bndInfo[i].radius, a_bndInfo[i].center , 6, true );
		}
	}
	g_OccludedDrawBoundTotal = 0;
	//-----------------------------
*/
	grcWorldMtx( M34_IDENTITY );
	grcColor3f( 0.0f, 1.0f, 1.0f );

	//
	// Debug draw occluder geoms
	//
	if( sm_DrawOccluderVolumes && !drawActive)// draw all occluders...
	{
		loopcount = m_numOccluderGeoms;

		rmOccludePolyhedron		*pHedra;
		grcLightingMode old_LightingMode = grcState::GetLightingMode();
		grcLightState::SetEnabled(false);

		Vector3 viewpos = pviewer->GetViewPosition();

		for(int n=0; n < loopcount; n++)
		{
			int geomIndex = n;
			pHedra = &m_OccluderGeomArray[ geomIndex ];// get ptr to occluder geom.

			// get distance from pvs viewpoint to the bounding sphere for each occluder, skip it if its too far.
			if(1)// distance culling occluders when drawing inactive ones.
			{
				Vector4 bndSphere;
				pHedra->GetBoundSphere( &bndSphere );

				Vector3 tmp;				
				tmp.x = viewpos.x - bndSphere.x;
				tmp.y = 0.f;// <--- HACK: FLAT 2D DISTANCE ---> viewpos.y - bndSphere.y;
				tmp.z = viewpos.z - bndSphere.z;

				float dist = tmp.Mag() - bndSphere.w;

				if( dist > sm_DrawOccluderVolumeDistance)
					continue;// loop to next.
			}

			// draw the volume!
			pHedra->DebugDraw();

			//--------------------------------------------------------
			if(sm_DrawOccluderVolumeNames)
			{
				// Draw debug name....
				bool	old_DepthTest = grcState::GetDepthTest();
				bool	old_DepthWrite = grcState::GetDepthWrite();

				grcState::SetDepthTest(false);
				grcState::SetDepthWrite(false);

				grcViewport* oldViewport=grcViewport::GetCurrent();
				grcViewport::SetCurrent( pviewer->GetPerspectiveViewport_render() );
				grcWorldMtx( M34_IDENTITY );

				Vector4 boundSphere;
				pHedra->GetBoundSphere( &boundSphere );

				Vector3 cullSphere( boundSphere.x, boundSphere.y, boundSphere.z );
				char stringStr[64];
				
				formatf(stringStr, sizeof(stringStr ), "%s", pHedra->GetDebugStringNamePtr() );
				float x,y;

				if(grcProject((Vector3&)cullSphere, x, y,
					pviewer->Get_view( pvsvt_Render ),
					*pviewer->GetPerspectiveViewport_render()
					) == grcProjectVisible)
				{
					grcFont::GetCurrent().DrawScaled(x, y, 1.0f, Color32(255, 255, 255), 1, 1, stringStr);
				}

				// restore old viewport:
				grcViewport::SetCurrent(oldViewport);

				// Restore!
				grcState::SetDepthTest(old_DepthTest);
				grcState::SetDepthWrite(old_DepthWrite);
			}
			//--------------------------------------------------------
		}

		DrawDebugView_Frustum( id, drawmodebits );

		grcState::SetLightingMode(old_LightingMode);
	}



	if( sm_DrawOccluderVolumes && drawActive )
	{
		// For active occluder only mode...
		// loop through all active occluders, finding the occluder geoms and debug drawing those only.
		loopcount = pviewer->GetNumActiveOccluders();// m_numActiveOccluders[ viewerID ];// draw only active polys for viewer 0
		grcWorldMtx( M34_IDENTITY );
		grcColor3f( 0.0f, 1.0f, 0.0f );

		for(int n=0; n < loopcount; n++)
		{		
			// TODO: Add view direction to plane normal test,
			//        factor in polygonal surface area to reject polys with little coverage or polys that are perpendicular to the view direction.
			rmOccludePolyhedron		*pHedra;
			mcActiveOccluderPoly	*pActivePoly = pviewer->GetActiveOccluderIndexPtr( n );

			if( ! pActivePoly->IsActive() )
				continue;// dont draw deactivated ones for now.

			if(1)//drawActive)
			{			
				if( NULL != pActivePoly->m_pOccPoly /*m_activeOccluders[ viewerID ][ n ].m_pOccPoly*/ )// handle geom occluders (null ptr case) safely
				{
					continue;//not a geom occluder, skip!!!
				}

				// if we fall through to here, then the occluder is a geom type....
				size_t geomId = pActivePoly->m_Id;// get pool index of the active geom occluder.
				pHedra =  (rmOccludePolyhedron*)geomId;// ID is the actual pointer, cast it!
			}else{

				// looping through all geoms
				int geomIndex = n;
				pHedra = &m_OccluderGeomArray[ geomIndex ];// get ptr to occluder geom.
			}


	//debugdrawviewfrustum!
			if( !sm_DebugDrawFrustumAsOpaque )
			{
				DrawDebugView_Frustum( id, drawmodebits );
			}


			grcWorldMtx( M34_IDENTITY );
			grcColor3f( 0.0f, 1.0f, 0.0f );

			//if( NULL == m_activeOccluders[ viewerID ][ n ].m_pOccPoly )
			if( rmOccluderSystem::IsAbleToDebugDrawOccluderElements() && sm_DrawOccluderVolumes )
			{

				if( rmOccluderSystem::IsAbleToDebugDrawOccluderElements() && sm_DisplayEntireOccluderVolume )
				{
					// handle geom case.
					//int geomId = m_activeOccluders[ viewerID ][ n ].m_Id;	
					//pHedra = &m_OccluderGeomArray[ geomId ];// get ptr to occluder geom.
					int facecount = pHedra->GetNumFacets();

					if( pActivePoly->m_type == RM_OCCLUDE_OCCLUDERTYPE_GEOM_EXTERNAL )
					{
						grcColor3f( 0.0f, 1.0f, 1.0f );
					}
					else
					{
						grcColor3f( 1.0f, 0.0f, 1.0f );
					}

					//set this state so umbra+frustum cut through wireframes to show points of intersection.
					grcState::SetDepthTest(true);
					grcState::SetDepthWrite(true);

					// loop through faces.			
					for( int fc=0 ; fc < facecount ; fc++ )
					{
						//  [3/20/2006 ccoffin]
						// Based on debug draw mode, we may want to skip drawing or color certain faces a different color...
						//
						// Faces that create the occlusion umbra are green,
						// Faces that are culled away(backface) and lie within the umbra are colored red.
						//  if we draw them in additive mode, the edges should be colored yellow.
						// todo: support drawing the silhouette polyedges also.


						int		numverts = pHedra->GetFacetNumVerts( fc );

						for( int vertIdx=0; vertIdx < numverts; vertIdx++)
						{
							int		i0,i1;
							int		vidx0, vidx1;
							Vector3	v0,v1;

							vidx0 = vertIdx;
							vidx1 = (vertIdx+1)%numverts;//wrap

							i0 = pHedra->GetFacetVertIndex( fc, vidx0 );
							i1 = pHedra->GetFacetVertIndex( fc, vidx1 );

							v0 = pHedra->GetVertex(i0);
							v1 = pHedra->GetVertex(i1);

							// optimize this so we send 1 large linestrip
							grcBegin(drawLineStrip, 2);						
							grcVertex3f( v0 );
							grcVertex3f( v1 );
							grcEnd();
						}
					}
				}


				//-----------------------------------------------------------------
				// silhouette edge draw here!
				if( rmOccluderSystem::IsAbleToDebugDrawOccluderElements() && sm_DisplayEdgeClippingInfo)
				{
					int num_face_edges			= pHedra->GetNumEdges();
					// loop through all unique edges of geom
					pvsEdgeData edgeFlags		= pActivePoly->GetDebug_PvsEdgeData();// readback buffered version of edge flags!

					for(int fe=0; fe < num_face_edges; fe++ )
					{
						if( (edgeFlags & (1<<fe)) )// bit set for this edge? then its a silhouette edge, otherwise its an interior edge.
						{
							int e0_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 0);
							int e1_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 1);

							Vector3 v3edge0 = pHedra->GetVertex( e1_vertIdx );
							Vector3 v3edge1 = pHedra->GetVertex( e0_vertIdx );

							// Draw silhouette edges
	#if 1
							grcState::SetAlphaBlend(false);
							grcState::SetBlendSet(grcbsNormal);
							// Draw silhouette edge
							// TODO: If its clipped, draw in a different shade???
							if( pActivePoly->GetDebug_WasPvsEdgeRemoved( e0_vertIdx, e1_vertIdx ))
							{
								grcColor3f( 1.0f, 0.0f, 0.0f );// edge draw color - edge 'outside' frustum.
							}else{
								grcColor3f( 0.0f, 1.0f, 0.0f );// edge draw color - within/crossing frustum.
							}

							if( ! pActivePoly->IsActive() )
							{
								grcColor3f( 1.0f, 1.0f, 0.0f );// self occluded or disabled for some other reason
							}

							grcBegin(drawLineStrip, 2);						
							grcVertex3f( v3edge0 );
							grcVertex3f( v3edge1 );
							grcEnd();
	#endif
				
						}
					}
				}
				// END -- silhouette edge draw 			
				//-----------------------------------------------------------------
	// we need rendering pass control for the visualizer stuff!

	// 1) go through and draw *ALL* wireframes of occlusion hulls/vis edges/etc with z-write on.
	// 2) go through and draw *ALL* umbra data with ztest on, but no z-write!

				//-----------------------------------------------------------------
				// START -- Draw Umbra volumes
				if( rmOccluderSystem::IsAbleToDebugDrawUmbraElements())
				{		
					int num_face_edges			= pHedra->GetNumEdges();
					// loop through all unique edges of geom
					pvsEdgeData edgeFlags		= pActivePoly->GetDebug_PvsEdgeData();// readback buffered version of edge flags!
					pvsEdgeData edgeReversed	= pActivePoly->GetDebug_PvsReverseEdgeData();// readback buffered version of edge flags!


					grcState::SetDepthWrite(true);
					grcState::SetDepthTest(true);
					grcState::SetDepthFunc(grcdfLessEqual);
					grcState::SetCullMode(grccmNone);

					for(int fe=0; fe < num_face_edges; fe++ )
					{
						if( (edgeFlags & (1<<fe)) )// bit set for this edge? then its a silhouette edge, otherwise its an interior edge.
						{
							int e0_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 0);
							int e1_vertIdx  = pHedra->GetEdgeVertexIndex( fe, 1);

							Vector3 v3edge0;
							Vector3 v3edge1;
							// bool	flip_A = false;

							if( (edgeReversed & (1<<fe)) )// was the edge inserted as reversed?
							{
								// flip_A = true;
								v3edge0 = pHedra->GetVertex( e1_vertIdx );
								v3edge1 = pHedra->GetVertex( e0_vertIdx );
							}else{
								v3edge0 = pHedra->GetVertex( e0_vertIdx );
								v3edge1 = pHedra->GetVertex( e1_vertIdx );
							}

							// vertex trace from eyepoint to edges.
							grcState::SetAlphaBlend(true);
							grcState::SetBlendSet(grcbsAdd);

							float	ViewpointTraceEdgeScale = sm_ViewpointTraceEdgeScale;//-.35f;
							float	UmbraTraceEdgeScale		= sm_DrawUmbraVolumeDistance;//40.f;
							Vector3	traceEdge0 = v3edge0 - pvsViewPos;
							Vector3	traceEdge1 = v3edge1 - pvsViewPos;

							Vector3	viewpoint_traceEdge0;
							Vector3	viewpoint_traceEdge1;


							traceEdge0.Normalize();
							traceEdge1.Normalize();
							// copy before they get scaled.
							viewpoint_traceEdge0 = traceEdge0;
							viewpoint_traceEdge1 = traceEdge1;

							// scale (negative) so the lines trace back towards the viewpoint.
							viewpoint_traceEdge0.Scale( ViewpointTraceEdgeScale );					
							viewpoint_traceEdge1.Scale( ViewpointTraceEdgeScale );

							// scale (positive) - create the umbra volume edges
							traceEdge0.Scale( UmbraTraceEdgeScale );					
							traceEdge1.Scale( UmbraTraceEdgeScale );

							// translate to worldspace!
							viewpoint_traceEdge0.Add(v3edge0);
							viewpoint_traceEdge1.Add(v3edge1);

							// translate to worldspace!
							traceEdge0.Add(v3edge0);
							traceEdge1.Add(v3edge1);


							//---------------------------------
							// Draw viewpoint->vertex edge lines.

							// common renderstates to all conditional rendering below
							grcState::SetAlphaBlend(true);
							grcState::SetBlendSet(grcbsAdd);

							if( sm_TraceViewpointToUmbra )
							{
								// note: since we're walking edges here, these get drawn twice! so tone down the alpha colors!
								grcBegin(drawLineStrip, 2);
									grcColor4f( 0.4f, 0.2f, 0.6f, 1.f );
									grcVertex3f( v3edge0 );
									grcColor4f( 0.0f, 0.0f, 0.0f, 1.f );
									grcVertex3f( viewpoint_traceEdge0 );
								grcEnd();

								grcBegin(drawLineStrip, 2);
									grcColor4f( 0.4f, 0.2f, 0.6f, 1.f );
									grcVertex3f( v3edge1 );
									grcColor4f( 0.0f, 0.0f, 0.0f, 1.f );
									grcVertex3f( viewpoint_traceEdge1 );
								grcEnd();
								//---------------------------------

								//---------------------------------
								// viewpoint to umbra edges trace.
								grcBegin(drawLineStrip, 2);						
									grcColor4f( 0.1f, 0.1f, 0.1f, 1.f );
									grcVertex3f( pvsViewPos );

									grcColor4f( 0.0f, 0.0f, 0.0f, 1.f );
									grcVertex3f( viewpoint_traceEdge0 );
								grcEnd();

								grcBegin(drawLineStrip, 2);						
									grcColor4f( 0.1f, 0.1f, 0.1f, 1.f );
									grcVertex3f( pvsViewPos );

									grcColor4f( 0.0f, 0.0f, 0.0f, 1.f );
									grcVertex3f( viewpoint_traceEdge1 );
								grcEnd();
								//---------------------------------
							}

							if( sm_DebugDrawUmbra )
							{
								if( sm_DebugDrawUmbraAsOpaque )
								{
									grcState::SetAlphaBlend(false);
								}
								else{
									grcState::SetAlphaBlend(true);
								}
								grcBegin(drawTriStrip, 4);
									
									grcColor4f( 0.1f, 0.0f, 0.1f, 1.f );
										grcVertex3f( v3edge0 );
										grcVertex3f( v3edge1 );
									
									grcColor4f( 0.0f, 0.0f, 0.5f, 1.f );
										grcVertex3f( traceEdge0 );
										grcVertex3f( traceEdge1 );
								grcEnd();

								// trace edges of umbra volume 
								grcState::SetAlphaBlend(true);
								grcState::SetBlendSet(grcbsAdd);
								grcColor4f( 0.2f, 0.7f, 0.7f, 0.7f );
								grcBegin(drawLineStrip, 2);						
									grcVertex3f( v3edge0 );
									grcVertex3f( traceEdge0 );
								grcEnd();

								grcBegin(drawLineStrip, 2);						
									grcVertex3f( v3edge1 );
									grcVertex3f( traceEdge1 );
								grcEnd();

								grcBegin(drawLineStrip, 2);						
									grcVertex3f( traceEdge0 );
									grcVertex3f( traceEdge1 );
								grcEnd();
							}
						}
					}

					// TODO: get vector from eyepoint to edge vertices to define the plane.
					// this will also help us define the direction vector from eyepoint to the silhouette edges so we can create a fan card to visualize the umbra volumes.

					// TODO: I think we need to check the edge reverse data flags so we can properly backface cull away or shade backfaces on the umbra
				}
				// END -- draw Umbra Volumes 			
				//-----------------------------------------------------------------

				//-----------------------------------------------------------------
				//set to reasonable defaults before leaving, especially since we messed with alpha/blend previously!
				grcState::SetAlphaBlend(false);
				grcState::SetBlendSet(grcbsNormal);
				grcState::SetDepthWrite(true);
				grcState::Default();
			}
		}// end of active occluder loop
	}// end of draw active

	DrawDebugView_Frustum( id, drawmodebits );

#endif
}



#if 0
					#if	!__DEV
					void	rmOccluderSystem::DrawDebugInfo( const int /* viewerID */)
					{
					}
					#else

					void	rmOccluderSystem::DrawDebugInfo( const int viewerID )
					{
					#if __DEV
						int polycount = 0;
						int	loopcount = 0;
						bool	drawActiveonly	= true;

					#if 0 // FIXME: PORT TO RAGE AND UNCOMMENT THIS BLOCK OF CODE // span buffer debug


						if( m_AllowSpanDebugDrawing )
						{	
							bool                        lighting;
							const gfxTexture            *texture;
							bool                        zTestEnable, zWriteEnable;
							gfxViewport                 *origViewport;

							origViewport=PIPE.GetViewport();
							PIPE.SetViewport(gfxPipeline::OrthoVP);
							RSTATE.SetIdentity();

							zTestEnable=RSTATE.GetZTestEnable();
							RSTATE.SetZTestEnable(false);
							zWriteEnable=RSTATE.GetZWriteEnable();
							RSTATE.SetZWriteEnable(false);

							texture= RSTATE.GetTexture();
							RSTATE.SetTexture(NoTexture);
							lighting=RSTATE.GetLighting();
							RSTATE.SetLighting(false);

							RSTATE.SetAlphaFunc(atstAlways);
							RSTATE.SetAlphaBlendEnable(false);
							RSTATE.SetColorMask(true, false);
							//----------------------------------------------


							int viewID				= GetActiveViewID();
							SpriteSpanBuffer *pSSB	= GetViewSSB( viewID );

							//pSSB->m_pScanlineSpanLists[ m_activeScanline ] = pStartSpan;


							RSTATE.SetWorld(Matrix34::I);
							vglBindTexture(NoTexture);
							int height = GetSpanBufferHeight( GetActiveViewID() );

							int unmerged_edges = 0;

							height = height >> 1;
							for(int scanline=0; scanline < height; scanline++)
							{
								fSpan_t *pSpan = pSSB->m_pScanlineSpanLists[ scanline ];
								fSpan_t *pPrev = NULL;

								while( pSpan != NULL )
								{
									// Get depth and convert to color, then draw line.
									//
									vglBegin( drawLineStrip, 2 );
									float colorDepth;

									Assert( pSpan->depth > 0.f );// make sure nothing backprojected ever ended up in buffer

									if( pSpan->depth > 255.f)
									{
										colorDepth = 255.f;
									}
									else
									{
										colorDepth = pSpan->depth;
									}

									colorDepth = 255.f - colorDepth;
									colorDepth *= (1.f/255.f);
									// color is brightest closer, farther away it gets darker

									vglColor4f( 0.1f, colorDepth, 0.1f, 1.0f );
									vglVertex3f( pSpan->start,	scanline*2.f, 0.f );

									if( pPrev && pPrev->depth == pSpan->depth)
									{
										unmerged_edges++;
										vglColor4f( 0.1f, colorDepth, colorDepth, 1.0f );
										// unmerged edge!
									}

									vglVertex3f( pSpan->end,	scanline*2.f, 0.f );
									vglEnd();

									pPrev = pSpan;
									pSpan = pSpan->next;// go to next;
								}

							}
							Displayf(" unmerged_edges %d ", unmerged_edges );


							//---------------------------------------------------------------------------
							// debug draw projected convex silhouettes
							//
							vglColor4f( 1.f, 0.f, 1.f, 1.0f );

							if(drawActiveonly)// draw active only
							{
								// loop through all active occluders, finding the occluder geoms and debug drawing those only.
								loopcount = m_numActiveOccluders[ viewerID ];// draw only active polys for viewer 0
							}else{
								loopcount = 0;
							}

							for(int loop = 0; loop < loopcount; loop++ )
							{
								s32 UnClippedSilhEdges  = m_activeOccluders[ viewerID ][ loop ].num_silh_verts;

								if( (UnClippedSilhEdges > 2 ) && (UnClippedSilhEdges < 12) )
								{			
									vglBegin( drawLineStrip, UnClippedSilhEdges+1 );
									for(int n=0; n < UnClippedSilhEdges+1; n++)
									{
										int index = n%UnClippedSilhEdges;				
										vglVertex3f(	(float) (m_activeOccluders[ viewerID ][ loop ].m_silh_x[ index ]),
											((float) (m_activeOccluders[ viewerID ][ loop ].m_silh_y[ index ])*2.f), 0.f );
									}
									vglEnd();
								}
							}
							//---------------------------------------------------------------------------

							// restore render states
							PIPE.SetViewport(origViewport);
							RSTATE.SetLighting(lighting);
							if(texture)
								RSTATE.SetTexture(texture);
							RSTATE.SetZTestEnable(zTestEnable);
							RSTATE.SetZWriteEnable(zWriteEnable);
						}



					#endif


					#if 0


						// top down debug draw mode
						bool                        lighting;
						const gfxTexture            *texture;
						bool                        zTestEnable, zWriteEnable;
						gfxViewport                 *origViewport;

						origViewport=PIPE.GetViewport();
						PIPE.SetViewport(gfxPipeline::OrthoVP);
						RSTATE.SetIdentity();

						zTestEnable=RSTATE.GetZTestEnable();
						RSTATE.SetZTestEnable(false);
						zWriteEnable=RSTATE.GetZWriteEnable();
						RSTATE.SetZWriteEnable(false);

						texture= RSTATE.GetTexture();
						RSTATE.SetTexture(NoTexture);
						lighting=RSTATE.GetLighting();
						RSTATE.SetLighting(false);

						RSTATE.SetAlphaFunc(atstAlways);
						RSTATE.SetAlphaBlendEnable(false);
						RSTATE.SetColorMask(true, false);

						//RSTATE.SetBlendSet(blendSet_One_Zero);
						/*

						Matrix34 cam;
						RSTATE.GetCamera().ToMatrix34(cam);
						cam.a *= 1;
						cam.c *= -1;
						cam.d += cam.c * 3.0f;
						RSTATE.SetWorld(cam);

						RSTATE.SetColor(mkrgb(255,255,255));
						RSTATE.SetTexture(NoTexture);
						RSTATE.SetZWriteEnable(false);
						RSTATE.SetZTestEnable(false);
						//gfxViewport *oldVp = PIPE.GetViewport();
						//PIPE.SetViewport(gfxPipeline::OrthoVP);
						*/
						/*
						for(int i=0; i<256; i++)
						{
						float xpos = i / 256.0f;
						float ypos = g_ledAlphaRemapTable[i];

						vglBegin(drawLine, 2);
						vglColor4f(0.0f, 1.0f, 0.0f, 1.0f);
						vglVertex3f(xpos,-0.5f, 0.0f);
						vglVertex3f(xpos, (ypos*0.5f) - 0.5f, 0.0f);
						vglEnd();			
						}
						*/

						float	topView_tlx;
						float	topView_tly;
						float	topView_brx;
						float	topView_bry;
						float	topView_w;
						float	topView_h;

						topView_tlx = 60.f;
						topView_tly = 60.f;

						topView_brx = topView_tlx + 256.f;
						topView_bry = topView_tly + 256.f;

						topView_w = topView_brx - topView_tlx;
						topView_h = topView_bry - topView_tly;


						float	eye_x = topView_tlx + (topView_w * .5f);
						float	eye_y = topView_tly + (topView_h * .5f);


						Vector3	viewposWorld = GetViewPosition( viewerID );
						Vector3	viewdirWorld = GetViewDirection( viewerID );

						float	unitscale = 1.f;// 1meter = 1pixel?
						// place eyepoint in center of our debug region
						RSTATE.SetWorld(Matrix34::I);

						// background poly. (make this modulate blend??
						/*
						0-2
						|/|
						1-3
						*/

						vglBeginBatch();
						vglBindTexture(NoTexture);

						vglBegin( drawTriStrip, 4 );
						vglColor4f( 0.1f, 0.1f, 0.1f, 1.0f );
						vglVertex3f( topView_tlx, topView_tly, 0.f );
						vglVertex3f( topView_tlx, topView_bry, 0.f );
						vglVertex3f( topView_brx, topView_tly, 0.f );
						vglVertex3f( topView_brx, topView_bry, 0.f );
						vglEnd();


						Matrix34	m34_zrot;
						Matrix34	m34topView;

						m34_zrot.Identity();
						m34topView.Identity();	

						float zoomFactor = 2.f;
						m34topView.Scale( zoomFactor );
						//g_spinHack += 0.1f;

						// translate to center of debug view 'window' , accounting for scale of scene
						m34topView.d.x = ((-viewposWorld.x * zoomFactor) );
						m34topView.d.y = ((-viewposWorld.z * zoomFactor) );


						m34_zrot.RotateZ( -(atan2f( viewdirWorld.z, viewdirWorld.x ) -1.5707f) );

						// composite
						m34topView.Dot( m34topView, m34_zrot );

						// post translate to window
						m34topView.d.x += eye_x;
						m34topView.d.y += eye_y;


						RSTATE.SetWorld( m34topView );



						// draw circles instead of bounding spheres for top view...
						//-----------------------------
						// Added support to draw retained mode style bound information here - cmc	
						for(int i=0; i < g_OccludedDrawBoundTotal; i++)
						{		
							vglColor( a_bndInfo[ i ].debugDrawColor );// set color

							int		j;
							float	x, y;
							float	r = a_bndInfo[i].radius;
							int		steps = 8;

							Vector3	pos;
							Vector3 sphereCenter = a_bndInfo[i].center;

							vglBegin( drawLineStrip,steps+1 );
							int vglVerts = steps+1;

							for(j=0;j<vglVerts;j++)
							{
								x=r*cosf(2.0f*PI*float(j)/float(steps));
								y=r*sinf(2.0f*PI*float(j)/float(steps));

								//			pos.x = eye_x +  (((sphereCenter.x+x) - viewposWorld.x)*unitscale);
								//			pos.y = eye_y +  (((sphereCenter.z+y) - viewposWorld.z)*unitscale);
								pos.x = (((sphereCenter.x+x) )*unitscale);
								pos.y = (((sphereCenter.z+y) )*unitscale);


								vglVertex3f( pos.x, pos.y, 0.f );
							}
							vglEnd();

							//		vglDrawSphere( a_bndInfo[i].radius, a_bndInfo[i].center , 6, true );
						}
						g_OccludedDrawBoundTotal = 0; //reset count.
						//-----------------------------


						//
						// Debug draw occluder geoms
						//

						if(drawActiveonly)// draw active only
						{
							// loop through all active occluders, finding the occluder geoms and debug drawing those only.
							loopcount = m_numActiveOccluders[ viewerID ];// draw only active polys for viewer 0
						}else{
							loopcount = m_numOccluderGeoms;
						}

						for(int n=0; n < loopcount; n++)
						{		
							// TODO: Add view direction to plane normal test,
							//        factor in polygonal surface area to reject polys with little coverage or polys that are perpendicular to the view direction.
							rmOccludePolyhedron	*pHedra;
							int						geomId;


							if(drawActiveonly)
							{
								if( NULL != m_activeOccluders[ viewerID ][ n ].m_pOccPoly )// handle geom occluders (null ptr case) safely
									continue;//not a geom occluder, skip!!!

								// if we fall through to here, then the occluder is a geom type....
								int geomId = m_activeOccluders[ viewerID ][ n ].m_Id;// get pool index of the active geom occluder.

								if( m_activeOccluders[ viewerID ][ n ].m_type == RM_OCCLUDE_OCCLUDERTYPE_GEOM_EXTERNAL )
								{
									pHedra =  (rmOccludePolyhedron*)geomId;// ID is the actual pointer, cast it!
								}
								else
								{
									// it must be a static occluder managed by the occluder system, so the ID is the occluder index in the array of occluder geoms instead.
									pHedra = &m_OccluderGeomArray[ geomId ];// get ptr to occluder geom.
								}

							}else{

								// looping through all geoms
								geomId = n;
								pHedra = &m_OccluderGeomArray[ geomId ];// get ptr to occluder geom.
							}


							//if( NULL == m_activeOccluders[ viewerID ][ n ].m_pOccPoly )
							{
								// handle geom case.
								//int geomId = m_activeOccluders[ viewerID ][ n ].m_Id;	
								//pHedra = &m_OccluderGeomArray[ geomId ];// get ptr to occluder geom.

								int facecount = pHedra->GetNumFacets();
								vglColor3f( 0.0f, 1.0f, 0.0f );

								// loop through faces.			
								for( int fc=0 ; fc < facecount ; fc++ )
								{
									int		numverts = pHedra->GetFacetNumVerts( fc );

									for( int vertIdx=0; vertIdx < numverts; vertIdx++)
									{
										int		i0,i1;
										int		vidx0, vidx1;
										Vector3	v0,v1;

										vidx0 = vertIdx;
										vidx1 = (vertIdx+1)%numverts;//wrap

										i0 = pHedra->GetFacetVertIndex( fc, vidx0 );
										i1 = pHedra->GetFacetVertIndex( fc, vidx1 );

										v0 = pHedra->GetVertex(i0);
										v1 = pHedra->GetVertex(i1);

										// optimize this so we send 1 large linestrip
										vglBegin(drawLineStrip, 2);						
										//vglVertex3f( v0 );
										//vglVertex3f( v1 );
										// transform relative to eyepoint
										vglVertex3f( 0.f + (( v0.x  )*unitscale),
											0.f + (( v0.z  )*unitscale),
											0.f);	 

										vglVertex3f( 0.f + (( v1.x  )*unitscale),
											0.f + (( v1.z  )*unitscale),
											0.f);					
										vglEnd();
									}
								}	
							}
						}








						// restore render states
						PIPE.SetViewport(origViewport);
						RSTATE.SetLighting(lighting);
						if(texture)
							RSTATE.SetTexture(texture);
						RSTATE.SetZTestEnable(zTestEnable);
						RSTATE.SetZWriteEnable(zWriteEnable);


						return;//
					#endif



						if(( !m_DrawActiveOccluders ) && ( !m_DrawAllOccluders ) &&( !m_AllowDebugDrawOfOccludedVolumes ) )
							return;// all switches to draw debug stuff are off, just exit then!
					#if 0
						if( m_DrawAllOccluders )
							drawActiveonly = false;// force draw them all. TODO: draw active ones in different color than inactive?
					#endif

					#if 1 // FIXME: PORT TO RAGE
						//-----------------------------
						// save render states we change
					#if 0
						bool oldZWrite = RSTATE.GetZWriteEnable();
						bool oldZTest  = RSTATE.GetZTestEnable();
						bool oldLighting = RSTATE.GetLighting();

						RSTATE.SetZWriteEnable(true);
						RSTATE.SetZTestEnable(false);   
						RSTATE.SetLighting(false);
						//-----------------------------
						//RSTATE.SetWorld(Matrix34::I);
					#endif
						grcWorldMtx( M34_IDENTITY );
						//vglBeginBatch();
						//vglBindTexture(NoTexture);
						grcColor3f( 0.0f, 1.0f, 0.0f );
					#endif

						//-----------------------------
						// Added support to draw retained mode style bound information here - cmc	
						for(int i=0; i < g_OccludedDrawBoundTotal; i++)
						{
							if(  a_bndInfo[ i ].occludedViewID == viewerID )
							{
					#if 1 // FIXME: PORT TO RAGE
								grcColor( a_bndInfo[ i ].debugDrawColor );// set color
								grcDrawSphere( a_bndInfo[i].radius, a_bndInfo[i].center , 6, true );
					#endif
							}
						}
						g_OccludedDrawBoundTotal = 0;
						//-----------------------------

						grcWorldMtx( M34_IDENTITY );
						grcColor3f( 0.0f, 1.0f, 0.0f );

						//
						// Debug draw occluder geoms
						//

						if(drawActiveonly)// draw active only
						{
							// loop through all active occluders, finding the occluder geoms and debug drawing those only.
							loopcount = m_numActiveOccluders[ viewerID ];// draw only active polys for viewer 0
						}else{
							loopcount = m_numOccluderGeoms;
						}

						for(int n=0; n < loopcount; n++)
						{		
							// TODO: Add view direction to plane normal test,
							//        factor in polygonal surface area to reject polys with little coverage or polys that are perpendicular to the view direction.
							rmOccludePolyhedron		*pHedra;

							if(drawActiveonly)
							{
								if( NULL != m_activeOccluders[ viewerID ][ n ].m_pOccPoly )// handle geom occluders (null ptr case) safely
									continue;//not a geom occluder, skip!!!

								// if we fall through to here, then the occluder is a geom type....
								size_t geomId = m_activeOccluders[ viewerID ][ n ].m_Id;// get pool index of the active geom occluder.

					#if 1
								pHedra =  (rmOccludePolyhedron*)geomId;// ID is the actual pointer, cast it!
					#else
								if( m_activeOccluders[ viewerID ][ n ].m_type == RM_OCCLUDE_OCCLUDERTYPE_GEOM_EXTERNAL )
								{
									pHedra =  (rmOccludePolyhedron*)geomId;// ID is the actual pointer, cast it!
								}
								else
								{
									// it must be a static occluder managed by the occluder system, so the ID is the occluder index in the array of occluder geoms instead.
									pHedra = &m_OccluderGeomArray[ geomId ];// get ptr to occluder geom.
								}
					#endif


							}else{

								// looping through all geoms
								int geomIndex = n;
								pHedra = &m_OccluderGeomArray[ geomIndex ];// get ptr to occluder geom.
							}


							//if( NULL == m_activeOccluders[ viewerID ][ n ].m_pOccPoly )
							{
								// handle geom case.
								//int geomId = m_activeOccluders[ viewerID ][ n ].m_Id;	
								//pHedra = &m_OccluderGeomArray[ geomId ];// get ptr to occluder geom.
								int facecount = pHedra->GetNumFacets();

								if( m_activeOccluders[ viewerID ][ n ].m_type == RM_OCCLUDE_OCCLUDERTYPE_GEOM_EXTERNAL )
								{
									grcColor3f( 0.0f, 1.0f, 1.0f );
								}
								else
								{
									grcColor3f( 1.0f, 0.0f, 1.0f );
								}

								// loop through faces.			
								for( int fc=0 ; fc < facecount ; fc++ )
								{
									int		numverts = pHedra->GetFacetNumVerts( fc );

									for( int vertIdx=0; vertIdx < numverts; vertIdx++)
									{
										int		i0,i1;
										int		vidx0, vidx1;
										Vector3	v0,v1;

										vidx0 = vertIdx;
										vidx1 = (vertIdx+1)%numverts;//wrap

										i0 = pHedra->GetFacetVertIndex( fc, vidx0 );
										i1 = pHedra->GetFacetVertIndex( fc, vidx1 );

										v0 = pHedra->GetVertex(i0);
										v1 = pHedra->GetVertex(i1);

										// optimize this so we send 1 large linestrip
										grcBegin(drawLineStrip, 2);						
										grcVertex3f( v0 );
										grcVertex3f( v1 );
										grcEnd();
									}
								}
							}
						}

						//
						// Debug draw occluder N-gons
						//

						if(drawActiveonly)// draw active only
						{
							polycount = m_numActiveOccluders[ viewerID ];// draw only active polys for viewer 0
						}else{
							polycount = m_numOccluderPolys;
						}

						for(int n=0; n < polycount; n++)
						{
							rmOccludePoly	*pOccluderPoly;
							int numverts;
							// TODO: Add view direction to plane normal test,
							//        factor in polygonal surface area to reject polys with little coverage or polys that are perpendicular to the view direction.

							if(drawActiveonly)
							{
								if( NULL == m_activeOccluders[ viewerID ][ n ].m_pOccPoly )// handle geom occluders (null ptr case) safely
									continue;

								pOccluderPoly	= m_activeOccluders[ viewerID ][ n ].m_pOccPoly;
								Assert( pOccluderPoly );
								numverts		= m_activeOccluders[ viewerID ][ n ].m_pOccPoly->GetNumVerts();

							}else{
								numverts		= m_OccluderPolyArray[n].GetNumVerts();
								pOccluderPoly	= &m_OccluderPolyArray[n];
							}

							// Draw normal
							Vector3	normal_pos;

							normal_pos.Average( pOccluderPoly->GetVertex( 0 ), pOccluderPoly->GetVertex( 1 ) );
							normal_pos.Average( pOccluderPoly->GetVertex( 2 ) );

							Vector3 normal_end( pOccluderPoly->GetPlane().x, pOccluderPoly->GetPlane().y, pOccluderPoly->GetPlane().z );

							normal_end.Scale( 5.f );
							normal_end.Add( normal_pos );
							grcBegin(drawLineStrip, 2);
							grcColor3f( 1.0f, 0.0f, 0.0f );
							grcVertex3f( normal_pos );
							grcVertex3f( normal_end );
							grcEnd();

							// Draw direction of view
							normal_end = GetViewDirection( viewerID );
							normal_end.Add( normal_pos );
							grcBegin(drawLineStrip, 2);
							grcColor3f( 1.0f, 0.0f, 1.0f );
							grcVertex3f( normal_pos );
							grcVertex3f( normal_end );
							grcEnd();

							for( int k=0; k < numverts;k++ )
							{
								// TODO: Check if each edge is visible within the view frustum first,
								//			if no part of the edge lies within the frustum then the
								//			plane created by that edge does not need to be tested against for occlusion!

								// Calculate each extruded plane

								// Indices to verts that create polygon edges.
								int edge1;
								if( k == numverts-1 )
								{
									edge1 = 0;// wrap back to first vert 
								}else{
									edge1 = k+1;// get next
								}

								int edge0 = k;
								grcBegin(drawLineStrip, 2);
								grcColor3f( 1.0f, 1.0f, 0.0f );
								grcVertex3f( pOccluderPoly->GetVertex( edge0 ) );
								grcVertex3f( pOccluderPoly->GetVertex( edge1 ) );
								grcEnd();
							}

							// Draw clipped occluder polys (TEST)
					#if 0
							if(drawActiveonly)
							{
								int		numOutVerts = 0;

								Vector4	testClipPlane( 0.f, 1.f, 0.f, 1.f );
								Vector4	v4OutVertArray[16];
								Vector3	v3SrcVertArray[16];
								Vector3 *pVertArray = &v3SrcVertArray[0];// pOccluderPoly->GetVertexPoolPtr();

								Assert( pOccluderPoly->GetNumVerts() < 16 );

								// FIXME:lame copy since I made ptr to vertpool private, this is test code anyways.
								for( int src=0; src < numverts; src++)
								{
									v3SrcVertArray[src].x = pOccluderPoly->GetVertex( src ).x;
									v3SrcVertArray[src].y = pOccluderPoly->GetVertex( src ).y;
									v3SrcVertArray[src].z = pOccluderPoly->GetVertex( src ).z;
								}


								if( clipNgonToNplanes( 1, &testClipPlane, numverts, pVertArray, &numOutVerts, &v4OutVertArray[0]) )
								{
									//Warningf(" clipped poly returned %d verts ", numverts );

									for( int pol=0; pol < numverts;pol++ )
									{
										int edge0 = pol;
										int edge1;

										if( pol == numverts-1 )
										{
											edge1 = 0;// wrap back to first vert 
										}else{
											edge1 = pol+1;// get next
										}
					#if 1 // FIXME: PORT TO RAGE
										grcBegin(drawLineStrip, 2);
										grcColor3f( 1.0f, 0.0f, 1.0f );
										grcVertex3f( v4OutVertArray[edge0].x, v4OutVertArray[edge0].y, v4OutVertArray[edge0].z );
										grcVertex3f( v4OutVertArray[edge1].x, v4OutVertArray[edge1].y, v4OutVertArray[edge1].z );
										grcEnd();
					#endif

									}
								}
							}
					#endif
							/*
							bool clipNgonToNplanes(
							int		num_planes,
							Vector4 *pav4TestPlanes,
							int		numPolyVerts,
							const	Vector3 *paPolyVerts,
							int		*pOutputVertCount,
							Vector4 *paClippedPolyDestBuffer // ptr to storage to hold clipped poly
							)
							*/
						}
					#if 0 // FIXME: PORT TO RAGE
						vglEndBatch();
						//-----------------------------
						// restore render states we changed
						RSTATE.SetZWriteEnable(oldZWrite);
						RSTATE.SetZTestEnable(oldZTest);
						RSTATE.SetLighting(oldLighting);
						//-----------------------------
					#endif


					#endif // #if __DEV
					}
					#endif


					//} // namespace rage
#endif

#endif		/// 0