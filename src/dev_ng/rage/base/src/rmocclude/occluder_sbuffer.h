// occluder_sbuffer.h 
#ifndef RM_OCCLUDE_OCCLUDER_SBUFFER_H
#define RM_OCCLUDE_OCCLUDER_SBUFFER_H

//#include "occluder.h"

#include "vector/vec.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
//#include "rmocclude/occluder.h"

using namespace rage;


// facing the screen so we dont need any gradient information.
typedef struct fSpan_s
{
	float	depth;	// OPTIMIZE: make it integer like u16?? (might lose alot of occlusion accuracy dropping this down from a float though at far distances where occlusion is needed most sometimes!)
	s16		start;	// OPTIMIZE: make s16?
	s16		end;	// OPTIMIZE: make s16??
	// OPTIMIZE: could change this to an index if we are dying for memory (would slow span processing maybe because of index to ptr conversions)
	struct	fSpan_s *next;

#if 0
	// If we wanted to go overboard saving memory:
	//	max of 256 wide span buffer, 16 bit depth and change pointer to an index would shrink it to 6bytes.
	//
	//u8		start;
	//u8		end;
	//s16		depth;
	//s16		nextSpanIdx;
#endif

} fSpan_t;// 12 bytes
// 1 of these per 'view'
class SpriteSpanBuffer
{
public:

	void	ActivateSpanBuffer			(s16 width, s16 height);

	s16		GetSpanBufferWidth			(void)	{ return m_screen_w;}
	s16		GetSpanBufferHeight			(void)	{ return m_screen_h;}


	// projection matrices

	// local to viewspace

	// get ptr to starting span for a given scanline.
	fSpan_t	*GetStartingSpanForScanline	(int scanline){ return m_pScanlineSpanLists[ scanline ]; }

	// only to be called when resetting views.
	void	ClearScanLineSpanListPtr	(int scanline){ m_pScanlineSpanLists[ scanline ] = NULL; }

	// array of ptrs to spans for each scanline.
	fSpan_t	*m_pScanlineSpanLists[ 1200 ];//rmOcclude::MAX_VERTICAL_RES ];

private:

	void	SetSpanBufferRect			(s16 width, s16 height) { m_screen_w = width; m_screen_h = height; }

	s16		m_screen_w;
	s16		m_screen_h;
};





#endif // RM_OCCLUDE_OCCLUDER_SBUFFER_H
