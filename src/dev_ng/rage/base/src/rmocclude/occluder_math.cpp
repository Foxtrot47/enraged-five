// 
// rmocclude/occluder_math.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "occluder_math.h"
#include "occluder.h"





/*
u32 PvsViewer::SphereFrustumCheck_ClipStatus( const Vector4 &sphere )
{
	Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
	float radius = sphere.w;

	Vector4 distancesLRNZ;                                 // left, right, near and z distance
	Vector4 distancesBTF;                                  // bottom, top and far distances

	u32	clipStatus = 0U;

	// fully outside?
	distancesLRNZ.Dot(center,m_CullMtx0); //test first 3 planes.

	if (distancesLRNZ.x > radius)
		clipStatus |= 1<<6;
	else if (distancesLRNZ.x < -radius)
		clipStatus |= 1<<7;

	if (distancesLRNZ.y > radius)
		clipStatus |= 1<<8;
	else if (distancesLRNZ.y < -radius)
		clipStatus |= 1<<9;

	if (distancesLRNZ.z > radius)
		clipStatus |= 1<<10;
	else if (distancesLRNZ.z < -radius)
		clipStatus |= 1<<11;

	distancesBTF.Dot(center,m_CullMtx1);//test other 3 planes.
	if (distancesBTF.x > radius)
		clipStatus |= 1<<0;
	else if (distancesBTF.x < -radius)
		clipStatus |= 1<<1;

	if (distancesBTF.y > radius)
		clipStatus |= 1<<2;
	else if (distancesBTF.y < -radius)
		clipStatus |= 1<<3;

	if (distancesBTF.z > radius)
		clipStatus |= 1<<4;
	else if (distancesBTF.z < -radius)
		clipStatus |= 1<<5;

	return (u32)clipStatus;// only low 12bits are valid, so it doesnt have to be unsigned.
}
*/















//using namespace rage;
#if 0
extern	Matrix44	sm_ScratchPadFastCullMtx[];
extern	Matrix44	sm_rmOccludeFastCullMtx[];













// same as above, but does not calc/return zdistance of the sphere center
s32 rmOccluderSystem::InlineFastSphereVisCheck_ClipStatus_B(const Vector4 &sphere) const
{
#if  !__PS2

	Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
	float radius = sphere.w;

	Vector4 distancesLRNZ;                                 // left, right, near and z distance
	Vector4 distancesBTF;                                  // bottom, top and far distances

	u32	clipStatus = 0U;

	// fully outside?
	distancesLRNZ.Dot(center,sm_ScratchPadFastCullMtx[2]); //test first 3 planes.

	if (distancesLRNZ.x > radius)
		clipStatus |= 1<<6;
	else if (distancesLRNZ.x < -radius)
		clipStatus |= 1<<7;

	if (distancesLRNZ.y > radius)
		clipStatus |= 1<<8;
	else if (distancesLRNZ.y < -radius)
		clipStatus |= 1<<9;

	if (distancesLRNZ.z > radius)
		clipStatus |= 1<<10;
	else if (distancesLRNZ.z < -radius)
		clipStatus |= 1<<11;

	distancesBTF.Dot(center,sm_ScratchPadFastCullMtx[3]);//test other 3 planes.
	if (distancesBTF.x > radius)
		clipStatus |= 1<<0;
	else if (distancesBTF.x < -radius)
		clipStatus |= 1<<1;

	if (distancesBTF.y > radius)
		clipStatus |= 1<<2;
	else if (distancesBTF.y < -radius)
		clipStatus |= 1<<3;

	if (distancesBTF.z > radius)
		clipStatus |= 1<<4;
	else if (distancesBTF.z < -radius)
		clipStatus |= 1<<5;

	return (s32)clipStatus;// only low 12bits are valid, so it doesnt have to be unsigned.
#else  // this is the pSX2 version. It should be all inlined macrocode and assembly
	register s32 clipStatus;

	asm __volatile__(
		"lqc2			vf8, 0x0(%1)    # load the sphere definition\n"
		"lqc2			vf4, 0x0(%2)    # load the first cull transform mtx\n"
		"lqc2			vf5, 0x10(%2)\n"
		"lqc2			vf6, 0x20(%2)\n"
		"lqc2			vf7, 0x30(%2)\n"
		"vmulax.xyzw     ACC,   vf4,vf8  # transform the sphere center to get Left Right and Near distances, plus actual Z distance\n"
		"vmadday.xyzw    ACC,   vf5,vf8 \n"
		"vmaddaz.xyzw    ACC,   vf6,vf8 \n"
		"vmaddw.xyzw     vf12,  vf7,vf0  # use vf0, since w is not 1 in the sphere vector4 \n"
		"lqc2			vf4, 0x40(%2)	# load the second cull transform mtx\n"
		"lqc2			vf5, 0x50(%2)\n"
		"lqc2			vf6, 0x60(%2)\n"
		"lqc2			vf7, 0x70(%2)\n"
		"vmulax.xyzw     ACC,  vf4,vf8	# do second transform to get Top, Bottom and Far distance\n"
		"vmadday.xyzw    ACC,  vf5,vf8 \n"
		"vmaddaz.xyzw    ACC,  vf6,vf8 \n"
		"vmaddw.xyzw     vf13, vf7,vf0   # use vf0, since w is not 1 in the sphere vector4 \n"
		"ctc2			$0,$vi18 		# clear clip status...\n"
		"vclipw.xyz		vf12xyz,vf8w	# use clip to do LRN compares\n"
		"vclipw.xyz		vf13xyz,vf8w	# use clip to do BTF compares\n"

		"VNOP							# make sure clip is done (these should pipeline with about CPU instructions)\n"
		"VNOP\n"
		"VNOP\n"
		"VNOP		\n"

		"cfc2			%0,$vi18 		# get the clip status\n"

		: "=r" (clipStatus) : "r" (&sphere) , "r" (sm_ScratchPadFastCullMtx));

	// clipStatus:  -N +N -R +R -L +L -F +F -T +T -B +B
	//  - means dist is < -radius,
	//  + means dist is > radius
	return clipStatus;		
	//	if      (clipStatus & 0x555)            return cullOutside;      // far enough outside at least one plane to be rejected
	//	else if ((clipStatus & 0xAAA) == 0xAAA) return cullInside;       // we are far enough inside all planes
	//	else if (!(clipStatus & 0x800))         return cullClippedZNear; // clipped the near plane (important for some things)
	//	else                                    return cullClipped;      // clipped one of the sides or far,  really "if ((clipCodes & 0x2AA) != 0x2AA)", but that is the only case left!		
#endif
}


// TODO: make a version for ps2 using asm/vu code
bool rmOccluderSystem::InlineFast2SpheresOutsideSinglePlane_A(	const Vector4 &sphere_A,
															  const Vector4 &sphere_B  ) const
{
	// There is no vector4*mtx44 that ignores the 'w' of the input vector, so copy and set w to 1....
	float	radius_a = sphere_A.w;
	float	radius_b = sphere_B.w;

	Vector4 center_a( sphere_A.x, sphere_A.y, sphere_A.z, 1.0f );
	Vector4 center_b( sphere_B.x, sphere_B.y, sphere_B.z, 1.0f );

	Vector4 distancesLRNZ_a;	// left, right, near and z distance
	Vector4 distancesLRNZ_b;	// left, right, near and z distance

	distancesLRNZ_a.Dot( center_a,sm_ScratchPadFastCullMtx[0] ); 
	distancesLRNZ_b.Dot( center_b,sm_ScratchPadFastCullMtx[0] ); 

	//
	// Test for fully outside cases on each plane
	//

	if( (distancesLRNZ_a.x > radius_a) && (distancesLRNZ_b.x > radius_b) ) return true; // both ends outside of LEFT  plane
	if( (distancesLRNZ_a.y > radius_a) && (distancesLRNZ_b.y > radius_b) ) return true; // both ends outside of RIGHT plane
	if( (distancesLRNZ_a.z > radius_a) && (distancesLRNZ_b.z > radius_b) ) return true; // both ends outside of NEAR  plane

	//-------------------------------------------------------------------------
	// Other plane tests in case it wasnt out on left, right or near planes...
	// Usually these cases will not be tested since most stuff gets culled against the left/right/near plane first.
	{
		Vector4 distancesBTF_a;	// bottom, top and far distances
		Vector4 distancesBTF_b;	// bottom, top and far distances

		distancesBTF_a.Dot( center_a ,sm_ScratchPadFastCullMtx[1] );
		distancesBTF_b.Dot( center_b ,sm_ScratchPadFastCullMtx[1] );

		if( (distancesBTF_a.x > radius_a) && (distancesBTF_b.x > radius_b) ) return true; // both ends outside of BOTTOM	plane
		if( (distancesBTF_a.y > radius_a) && (distancesBTF_b.y > radius_b) ) return true; // both ends outside of TOP		plane
		if( (distancesBTF_a.z > radius_a) && (distancesBTF_b.z > radius_b) ) return true; // both ends outside of FAR		plane
	}
	//-------------------------------------------------------------------------

	// If we got to here then the 2 spheres (or hotdog they define) is not completely outside of a single plane.

	// NOTE: If the 2 spheres are used to define an extruded volume (like a hotdog) the volume may not penetrate the convex hull
	// defined by the frustum planes. Which would be a false result.
	return false;
}

// read only style instance of an occlusion query object
// this should


// 1) 'Lock' occlusion viewer
//		setup view info.
// 2) Hook query instances to a setup occlusion viewpoint.
//		you can only hook occlusion query instances to a locked occlusion view.
//
//   occlusion viewer allocates query objects and man
// 3) 'UnLock' occlusion viewer



/*
// STEP #1. Declare our thread function

//-- We declare the function that will be run in our separate thread.
DECLARE_THREAD_FUNC(foobar) {
	for (;;) {
		Displayf("foobar! %p",ptr);
		sysIpcSleep(750);
	}
}
// -STOP

//-- We create the thread.  This thread runs the function <c>foobar(void* ptr)</c>, with a stack size of 4 KB, and the thread is suspended (not started.)
sysIpcThreadId id = sysIpcCreateThread(foobar,NULL,4096,PRIO_NORMAL);


// PURPOSE: A cross-platform thread identifier (guaranteed globally unique)
#if __PPU
typedef u64 sysIpcThreadId;
#else
typedef struct sysIpcThreadIdTag * sysIpcThreadId;
#endif

class occlusionQueryInst
{

	// Copy of all local data we need to perform a successful query so its multithread safe.
	// think of this class like a mini-vu implementation of mc3 occlusion culling.
	
	u32		IsSphereOccluded( const Vector4 &sphere );

	bool	PrepBatchCull( const atArray<Vector4> &SphereArray );
	bool	PrepBatchCull( const Vector4 *pShperes );

	//bool	StartBatchCull (datCallback);

private:
	s32			m_hulls;

	Matrix44	sm_ScratchPadFastCullMtx[ 2 ] ;// '2' per normal 6 plane frustum
	Vector4		m_planes[ 256 ];// size of this class should fit in cell memory and facilitate double/triple buffering if necessary.

};

*/




// same as above, but does not calc/return zdistance of the sphere center
s32 rmOccluderSystem::InlineFastSphereVisCheck_ClipStatus_A(const Vector4 &sphere) const
{
#if  !__PS2

	Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
	float radius = sphere.w;

	Vector4 distancesLRNZ;                                 // left, right, near and z distance
	Vector4 distancesBTF;                                  // bottom, top and far distances

	u32	clipStatus = 0U;

	// fully outside?
	distancesLRNZ.Dot(center,sm_ScratchPadFastCullMtx[0]); //test first 3 planes.

	if (distancesLRNZ.x > radius)
		clipStatus |= 1<<6;
	else if (distancesLRNZ.x < -radius)
		clipStatus |= 1<<7;

	if (distancesLRNZ.y > radius)
		clipStatus |= 1<<8;
	else if (distancesLRNZ.y < -radius)
		clipStatus |= 1<<9;

	if (distancesLRNZ.z > radius)
		clipStatus |= 1<<10;
	else if (distancesLRNZ.z < -radius)
		clipStatus |= 1<<11;

	distancesBTF.Dot(center,sm_ScratchPadFastCullMtx[1]);//test other 3 planes.
	if (distancesBTF.x > radius)
		clipStatus |= 1<<0;
	else if (distancesBTF.x < -radius)
		clipStatus |= 1<<1;

	if (distancesBTF.y > radius)
		clipStatus |= 1<<2;
	else if (distancesBTF.y < -radius)
		clipStatus |= 1<<3;

	if (distancesBTF.z > radius)
		clipStatus |= 1<<4;
	else if (distancesBTF.z < -radius)
		clipStatus |= 1<<5;

	return (s32)clipStatus;// only low 12bits are valid, so it doesnt have to be unsigned.
#else  // this is the pSX2 version. It should be all inlined macrocode and assembly
	register s32 clipStatus;

	asm __volatile__(
		"lqc2			vf8, 0x0(%1)    # load the sphere definition\n"
		"lqc2			vf4, 0x0(%2)    # load the first cull transform mtx\n"
		"lqc2			vf5, 0x10(%2)\n"
		"lqc2			vf6, 0x20(%2)\n"
		"lqc2			vf7, 0x30(%2)\n"
		"vmulax.xyzw     ACC,   vf4,vf8  # transform the sphere center to get Left Right and Near distances, plus actual Z distance\n"
		"vmadday.xyzw    ACC,   vf5,vf8 \n"
		"vmaddaz.xyzw    ACC,   vf6,vf8 \n"
		"vmaddw.xyzw     vf12,  vf7,vf0  # use vf0, since w is not 1 in the sphere vector4 \n"
		"lqc2			vf4, 0x40(%2)	# load the second cull transform mtx\n"
		"lqc2			vf5, 0x50(%2)\n"
		"lqc2			vf6, 0x60(%2)\n"
		"lqc2			vf7, 0x70(%2)\n"
		"vmulax.xyzw     ACC,  vf4,vf8	# do second transform to get Top, Bottom and Far distance\n"
		"vmadday.xyzw    ACC,  vf5,vf8 \n"
		"vmaddaz.xyzw    ACC,  vf6,vf8 \n"
		"vmaddw.xyzw     vf13, vf7,vf0   # use vf0, since w is not 1 in the sphere vector4 \n"
		"ctc2			$0,$vi18 		# clear clip status...\n"
		"vclipw.xyz		vf12xyz,vf8w	# use clip to do LRN compares\n"
		"vclipw.xyz		vf13xyz,vf8w	# use clip to do BTF compares\n"

		"VNOP							# make sure clip is done (these should pipeline with about CPU instructions)\n"
		"VNOP\n"
		"VNOP\n"
		"VNOP		\n"

		"cfc2			%0,$vi18 		# get the clip status\n"

		: "=r" (clipStatus) : "r" (&sphere) , "r" (sm_ScratchPadFastCullMtx));

	// clipStatus:  -N +N -R +R -L +L -F +F -T +T -B +B
	//  - means dist is < -radius,
	//  + means dist is > radius
	return clipStatus;		
	//	if      (clipStatus & 0x555)            return cullOutside;      // far enough outside at least one plane to be rejected
	//	else if ((clipStatus & 0xAAA) == 0xAAA) return cullInside;       // we are far enough inside all planes
	//	else if (!(clipStatus & 0x800))         return cullClippedZNear; // clipped the near plane (important for some things)
	//	else                                    return cullClipped;      // clipped one of the sides or far,  really "if ((clipCodes & 0x2AA) != 0x2AA)", but that is the only case left!		
#endif
}

grcCullStatus rmOccluderSystem::DualFrustumVisCheck_A(const Vector4 &sphere,float& zDist )
{
#if __PS2
	register int clipStatus;
	asm __volatile__(
		"lqc2			vf8, 0x0(%1)    # load the sphere definition\n"
		"lqc2			vf4, 0x0(%2)    # load the first cull transform mtx\n"
		"lqc2			vf5, 0x10(%2)\n"
		"lqc2			vf6, 0x20(%2)\n"
		"lqc2			vf7, 0x30(%2)\n"
		"vmulax.xyzw     ACC,   vf4,vf8  # transform the sphere center to get Left Right and Near distances, plus actual Z distance\n"
		"vmadday.xyzw    ACC,   vf5,vf8 \n"
		"vmaddaz.xyzw    ACC,   vf6,vf8 \n"
		"vmaddw.xyzw     vf12,  vf7,vf0  # use vf0, since w is not 1 in the sphere vector4 \n"
		"lqc2			vf4, 0x40(%2)	# load the second cull transform mtx\n"
		"lqc2			vf5, 0x50(%2)\n"
		"lqc2			vf6, 0x60(%2)\n"
		"lqc2			vf7, 0x70(%2)\n"
		"vmulax.xyzw     ACC,  vf4,vf8	# do second transform to get Top, Bottom and Far distance\n"
		"vmadday.xyzw    ACC,  vf5,vf8 \n"
		"vmaddaz.xyzw    ACC,  vf6,vf8 \n"
		"vmaddw.xyzw     vf13, vf7,vf0   # use vf0, since w is not 1 in the sphere vector4 \n"
		"qmfc2			$2, vf12		# put vector with zdist into COP2 register \n"
		"ctc2			$0,$vi18 		# clear clip status...\n"
		"vclipw.xyz		vf12xyz,vf8w	# use clip to do LRN compares\n"
		"vclipw.xyz		vf13xyz,vf8w	# use clip to do BTF compares\n"

		"pextuw			$3,$0,$2		# get w down to the bits 64..96\n"
		"pextuw			$2,$0,$3		# get w down to the bits 0..32\n"
		"sw				$2,0x0(%3)		# save zdist\n"

		"VNOP							# make sure clip is done (these should pipeline with about CPU instructions)\n"
		"VNOP\n"
		"VNOP\n"

		"cfc2			%0,$vi18 		# get the clip status\n"

		: "=r" (clipStatus) : "r" (&sphere) , "r" (sm_ScratchPadFastCullMtx), "r" (&zDist): "$2", "$3");

	// bits:  -N +N -R +R -L +L -F +F -T +T -B +B
	//  - means dist is < -radius,
	//  + means dist is > radius

	if      (clipStatus & 0x555)            return cullOutside;      // far enough outside at least one plane to be rejected
	else if ((clipStatus & 0xAAA) == 0xAAA) return cullInside;       // we are far enough inside all planes
	else if (!(clipStatus & 0x800))         return cullClippedZNear; // clipped the near plane (important for some things)
	else                                    return cullClipped;      // clipped one of the sides or far,  really "if ((clipCodes & 0x2AA) != 0x2AA)", but that is the only case left!
#else
	Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
	float radius = sphere.w;

	Vector4 distancesLRNZ;                                 // left, right, near and z distance
	Vector4 distancesBTF;                                  // bottom, top and far distances

	// fully outside?
	distancesLRNZ.Dot(center,sm_ScratchPadFastCullMtx[0]); 
	zDist = distancesLRNZ.w; 
	if (distancesLRNZ.x > radius) return cullOutside;      // left
	if (distancesLRNZ.y > radius) return cullOutside;      // right
	if (distancesLRNZ.z > radius) return cullOutside;      // near

	distancesBTF.Dot(center,sm_ScratchPadFastCullMtx[1]);				   // many times we don't need to do this one...
	if (distancesBTF.x  > radius) return cullOutside;      // bottom
	if (distancesBTF.y  > radius) return cullOutside;      // top
	if (distancesBTF.z  > radius) return cullOutside;      // far

	// Does it touch a clipping plane?
#if 1
	// FIXME: if (distancesLRNZ.z > -radius) return cullClippedZNear; // near
	if (distancesLRNZ.z > -radius) return cullClipped;// need near zclip flag or enum
#endif

	if (distancesLRNZ.x > -radius) return cullClipped;      // left
	if (distancesLRNZ.y > -radius) return cullClipped;      // right
	if (distancesBTF.x  > -radius) return cullClipped;      // bottom
	if (distancesBTF.y  > -radius) return cullClipped;      // top
	if (distancesBTF.z  > -radius) return cullClipped;		// far
	return cullInside;
#endif
}


grcCullStatus rmOccluderSystem::DualFrustumVisCheck_B(const Vector4 &sphere,float& zDist )
{
#if __PS2
	register int clipStatus;
	asm __volatile__(
		"lqc2			vf8, 0x0(%1)    # load the sphere definition\n"
		"lqc2			vf4, 0x80(%2)    # load the first cull transform mtx\n"
		"lqc2			vf5, 0x90(%2)\n"
		"lqc2			vf6, 0xA0(%2)\n"
		"lqc2			vf7, 0xB0(%2)\n"
		"vmulax.xyzw     ACC,   vf4,vf8  # transform the sphere center to get Left Right and Near distances, plus actual Z distance\n"
		"vmadday.xyzw    ACC,   vf5,vf8 \n"
		"vmaddaz.xyzw    ACC,   vf6,vf8 \n"
		"vmaddw.xyzw     vf12,  vf7,vf0  # use vf0, since w is not 1 in the sphere vector4 \n"
		"lqc2			vf4, 0xC0(%2)	# load the second cull transform mtx\n"
		"lqc2			vf5, 0xD0(%2)\n"
		"lqc2			vf6, 0xE0(%2)\n"
		"lqc2			vf7, 0xF0(%2)\n"
		"vmulax.xyzw     ACC,  vf4,vf8	# do second transform to get Top, Bottom and Far distance\n"
		"vmadday.xyzw    ACC,  vf5,vf8 \n"
		"vmaddaz.xyzw    ACC,  vf6,vf8 \n"
		"vmaddw.xyzw     vf13, vf7,vf0   # use vf0, since w is not 1 in the sphere vector4 \n"
		"qmfc2			$2, vf12		# put vector with zdist into COP2 register \n"
		"ctc2			$0,$vi18 		# clear clip status...\n"
		"vclipw.xyz		vf12xyz,vf8w	# use clip to do LRN compares\n"
		"vclipw.xyz		vf13xyz,vf8w	# use clip to do BTF compares\n"

		"pextuw			$3,$0,$2		# get w down to the bits 64..96\n"
		"pextuw			$2,$0,$3		# get w down to the bits 0..32\n"
		"sw				$2,0x0(%3)		# save zdist\n"

		"VNOP							# make sure clip is done (these should pipeline with about CPU instructions)\n"
		"VNOP\n"
		"VNOP\n"

		"cfc2			%0,$vi18 		# get the clip status\n"

		: "=r" (clipStatus) : "r" (&sphere) , "r" (sm_ScratchPadFastCullMtx), "r" (&zDist): "$2", "$3");

	// bits:  -N +N -R +R -L +L -F +F -T +T -B +B
	//  - means dist is < -radius,
	//  + means dist is > radius

	if      (clipStatus & 0x555)            return cullOutside;      // far enough outside at least one plane to be rejected
	else if ((clipStatus & 0xAAA) == 0xAAA) return cullInside;       // we are far enough inside all planes
	else if (!(clipStatus & 0x800))         return cullClippedZNear; // clipped the near plane (important for some things)
	else                                    return cullClipped;      // clipped one of the sides or far,  really "if ((clipCodes & 0x2AA) != 0x2AA)", but that is the only case left!
#else
	Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
	float radius = sphere.w;

	Vector4 distancesLRNZ;                                 // left, right, near and z distance
	Vector4 distancesBTF;                                  // bottom, top and far distances

	// fully outside?
	distancesLRNZ.Dot(center,sm_ScratchPadFastCullMtx[2]); 
	zDist = distancesLRNZ.w; 
	if (distancesLRNZ.x > radius) return cullOutside;      // left
	if (distancesLRNZ.y > radius) return cullOutside;      // right
	if (distancesLRNZ.z > radius) return cullOutside;      // near

	distancesBTF.Dot(center,sm_ScratchPadFastCullMtx[3]);				   // many times we don't need to do this one...
	if (distancesBTF.x  > radius) return cullOutside;      // bottom
	if (distancesBTF.y  > radius) return cullOutside;      // top
	if (distancesBTF.z  > radius) return cullOutside;      // far

	// Does it touch a clipping plane?
	//if (distancesLRNZ.z > -radius) return cullClippedZNear; // near

#if 1
	// FIXME: if (distancesLRNZ.z > -radius) return cullClippedZNear; // near
	if (distancesLRNZ.z > -radius) return cullClipped;// need near zclip flag or enum
#endif

	if (distancesLRNZ.x > -radius) return cullClipped;      // left
	if (distancesLRNZ.y > -radius) return cullClipped;      // right
	if (distancesBTF.x  > -radius) return cullClipped;      // bottom
	if (distancesBTF.y  > -radius) return cullClipped;      // top
	if (distancesBTF.z  > -radius) return cullClipped;		// far
	return cullInside;
#endif
}



void	rmOccluderSystem::PrepDualFrustumCullMatrices( const int viewerID_0, const int viewerID_1 )
{
	//return;
	Matrix44 *pMtx_0_0	=	GetViewerCullMtxPtr_0( viewerID_0 );
	Matrix44 *pMtx_0_1	=	GetViewerCullMtxPtr_1( viewerID_0 );
	Matrix44 *pMtx_1_0	=	GetViewerCullMtxPtr_0( viewerID_1 );
	Matrix44 *pMtx_1_1	=	GetViewerCullMtxPtr_1( viewerID_1 );

	sm_ScratchPadFastCullMtx[0] = *pMtx_0_0;
	sm_ScratchPadFastCullMtx[1] = *pMtx_0_1;
	sm_ScratchPadFastCullMtx[2] = *pMtx_1_0;
	sm_ScratchPadFastCullMtx[3] = *pMtx_1_1;
}

void	rmOccluderSystem::PrepDualFrustumCullMatrices( Matrix44 *pCullMtx_0_0, Matrix44 *pCullMtx_0_1, Matrix44 *pCullMtx_1_0, Matrix44 *pCullMtx_1_1 )
{
	//return;
	sm_ScratchPadFastCullMtx[0] = *pCullMtx_0_0;
	sm_ScratchPadFastCullMtx[1] = *pCullMtx_0_1;
	sm_ScratchPadFastCullMtx[2] = *pCullMtx_1_0;
	sm_ScratchPadFastCullMtx[3] = *pCullMtx_1_1;
	// load to VU regs now.
}

grcCullStatus rmOccluderSystem::FrustumVisCheck(const Vector4 &sphere,float& zDist, const int viewerID )
{
	Vector4 distancesLRNZ;                                 // left, right, near and z distance
	Vector4 distancesBTF;                                  // bottom, top and far distances
	Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
	float radius = sphere.w;


	//
	// NOTE: We have to use the non scratchpad matrices because we dont have enough space to store all views there.
	//

	// fully outside?
	distancesLRNZ.Dot(center, sm_rmOccludeFastCullMtx[ viewerID << 1 ] ); // sm_FastCullMtx[0]); 
	zDist = distancesLRNZ.w; 
	if (distancesLRNZ.x > radius) return cullOutside;      // left
	if (distancesLRNZ.y > radius) return cullOutside;      // right
	if (distancesLRNZ.z > radius) return cullOutside;      // near

	distancesBTF.Dot(center, sm_rmOccludeFastCullMtx[ (viewerID << 1)+1 ] ); // sm_FastCullMtx[1]);				   // many times we don't need to do this one...
	if (distancesBTF.x  > radius) return cullOutside;      // bottom
	if (distancesBTF.y  > radius) return cullOutside;      // top
	if (distancesBTF.z  > radius) return cullOutside;      // far

	// Does it touch a clipping plane?
	if (distancesLRNZ.z > -radius) return cullClipped; // FIXME -> cullClippedZNear; // near
	if (distancesLRNZ.x > -radius) return cullClipped;      // left
	if (distancesLRNZ.y > -radius) return cullClipped;      // right
	if (distancesBTF.x  > -radius) return cullClipped;      // bottom
	if (distancesBTF.y  > -radius) return cullClipped;      // top
	if (distancesBTF.z  > -radius) return cullClipped;		// far
	return cullInside;
}


grcCullStatus rmOccluderSystem::FrustumVisCheck(const Vector4 &sphere,float& zDist)
{
	const int viewerID = GetActiveViewID();
	//#if !__PS2
	// cross platform version.
	Vector4 distancesLRNZ;                                 // left, right, near and z distance
	Vector4 distancesBTF;                                  // bottom, top and far distances
	Vector4 center(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!
	float radius = sphere.w;

	//
	// NOTE: We have to use the non scratchpad matrices because we dont have enough space to store all views there.
	//

	// fully outside?
	distancesLRNZ.Dot(center, sm_rmOccludeFastCullMtx[ viewerID << 1 ] ); // sm_FastCullMtx[0]); 
	zDist = distancesLRNZ.w; 
	if (distancesLRNZ.x > radius) return cullOutside;      // left
	if (distancesLRNZ.y > radius) return cullOutside;      // right
	if (distancesLRNZ.z > radius) return cullOutside;      // near

	distancesBTF.Dot(center, sm_rmOccludeFastCullMtx[ (viewerID << 1)+1 ] ); // sm_FastCullMtx[1]);				   // many times we don't need to do this one...
	if (distancesBTF.x  > radius) return cullOutside;      // bottom
	if (distancesBTF.y  > radius) return cullOutside;      // top
	if (distancesBTF.z  > radius) return cullOutside;      // far

	// Does it touch a clipping plane?
	if (distancesLRNZ.z > -radius) return cullClipped; // FIXME: -> cullClippedZNear; // near
	if (distancesLRNZ.x > -radius) return cullClipped;      // left
	if (distancesLRNZ.y > -radius) return cullClipped;      // right
	if (distancesBTF.x  > -radius) return cullClipped;      // bottom
	if (distancesBTF.y  > -radius) return cullClipped;      // top
	if (distancesBTF.z  > -radius) return cullClipped;		// far
	return cullInside;
}
#endif
