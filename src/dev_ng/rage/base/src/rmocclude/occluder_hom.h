// 
// rmocclude/occluder_hom.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef RM_OCCLUDE_OCCLUDER_HOM_H
#define RM_OCCLUDE_OCCLUDER_HOM_H
#if 0









typedef enum  rmPvsCullStatusBits {
	pvscull_all_clear			=0U<<0,
	pvscull_frust_plane_posB	=1U<<0,
	pvscull_frust_plane_negB	=1U<<1,
	pvscull_frust_plane_posT	=1U<<2,
	pvscull_frust_plane_negT	=1U<<3,
	pvscull_frust_plane_posF	=1U<<4,
	pvscull_frust_plane_negF	=1U<<5,
	pvscull_frust_plane_posL	=1U<<6,
	pvscull_frust_plane_negL	=1U<<7,
	pvscull_frust_plane_posR	=1U<<8,
	pvscull_frust_plane_negR	=1U<<9,
	pvscull_frust_plane_posN	=1U<<10,
	pvscull_frust_plane_negN	=1U<<11,

	pvscull_frust_plane_outside_mask	=0x555,
	pvscull_frust_plane_inside_mask		=0xAAA,
	pvscull_frust_plane_bits_mask		=0xFFF,

	pvscull_CrossingFrustum		=1U<<24, // 'inside partially' to be implemented.
	pvscull_OutsideFrustum		=1U<<25,
	pvscull_InsideFrustum		=1U<<26,
	pvscull_Occluded			=1U<<27,
	pvscull_NotOccluded			=1U<<28,

#if __DEV
	pvscull_pvs_is_frozen		=1U<<29,// TODO: IMPLEMENT! //  [3/20/2006 ccoffin] allows the user to determine rendering behavior for visibility results based on frozen viewpoints.
#endif

	pvscull_AllBits_mask		=~0U
};


typedef enum  rmPvsCullModeBits {

	pvscm_Empty					=0U<<0,

	// pvs techniques start
	pvscm_FrustumTest				=1U<<0,		// frustum cull the object - if not set, its assumed to be within/crossing the frustum.
	pvscm_OccludeTest				=1U<<1,		// occlusion test (if frustum test was specified, occlusion test happens after)

	// TODO: IMPLEMENT -> pvscv_SpanBufferTest	=1U<<2,

	// TODO: setup priority bits for frustum/span/worldspace occlusion culling techniques.

	// multibitmasks
	pvscm_EnabledCulling_mask			=(pvscm_FrustumTest|pvscm_OccludeTest),// frustum/occlude test

	// might remove these or change these. These should not be default params/options in the rmOcclude library right now.
	pvscm_ForceOccludeTest			=1U<<23,	// even if frustum trivially culls the object first, perform occlusion cull test.
	pvscm_EarlyInsideFrustumTest	=1U<<24,	// Skip testing remainder of all vertices of an aabox/hull/etc once we encounter one that lies inside the frustum.
	// WARNING: using 'pvscm_EarlyInsideFrustumTest' will cause the 'pvscull_frust_plane' to be invalid since they wont all be set due to the early loop exit!.
	// if you have any code looking at the frust cull plane bits you should not be enabling this!
	// WARNING: temporal coherence culling wont work with 'pvscm_EarlyInsideFrustumTest' enabled either.
#if __DEV
	pvscm_FrustumTest_FrozenAndRenderViews = 1U<<25, // when in frozen pvs view mode, frustum culling for certain functions clips against 2 frusta.
	//This is mainly done so things dont slow down horribly. Not something done in a final game obviously.
#else
	//	pvscm_FrustumTest_FrozenAndRenderViews = pvscm_FrustumTest,
#endif


	pvscm_AllBits_mask				=~0U
};


rmPvsCullStatusBits	TestSphereVisibility(	const pvsViewerID id,
										 const Vector3 &CenterA,
										 const float radius,
										 const rmPvsCullModeBits	cmBits = pvscm_EnabledCulling_mask
										 );//  [3/16/2006 ccoffin]

rmPvsCullStatusBits	TestAABoxVisibility(	const pvsViewerID id,
										const Vector4		*pv4Min,
										const Vector4		*pv4Max,
										const Matrix34		*pMtxLocalToWorld,// if null, we assume identity xform.
										const rmPvsCullModeBits	cmBits = pvscm_EnabledCulling_mask
										);//  [3/16/2006 ccoffin]


//--sample init code from new interface (work in progress)

m_OccludeSystem = rage_new rmOccluderSystem;
m_testPvsViewID = m_OccludeSystem->CreateViewer();// should not be done every frame! (do this during setup)
m_OccludeSystem->LoadDataset( "T:\\rage\\assets\\sample_rmOcclude\\cube.occlude", "" );

//--sample 'update' code - (work in progress)
// new occlusion interface! [3/14/2006 ccoffin]

// new and 'simpler' interface for setting up a view in the pvs system.
// synthesizes all data that is passed into 'SetViewMatrices' as well as generating the appropriate culling plane data/matrices for
// efficient frustum culling without the hassle of managing the 
Matrix34    camMtx	= this->GetCameraMatrix();
pvsViewerID pvsID	= m_testPvsViewID;

m_OccludeSystem->SetPvsView( pvsID, this->GetViewport(), this->GetCameraMatrix() );
// Note: Render view can different than 'pvs' view because the camera can detach from the pvs view and become different (usually for debug reasons).
m_OccludeSystem->SetRenderView( pvsID, camMtx.d, camMtx.c, &camMtx );
m_OccludeSystem->ProcessStaticOccluders( pvsID );// determine occluders based on pvs+render views.

// simple culling test

for(int i =0 ; i < this->m_numTestSpheres ;i++ )
{
	m_Spheres[i].Update( frame_dt );

	bool	IsVisible = rmOcclude::IsVisible( m_OccludeSystem->TestSphereVisibility( pvsID, m_Spheres[i].GetMatrix()->d,m_Spheres[i].GetRadius() ));

	m_Spheres[i].SetVisibility( 0, IsVisible );// store result.
}

// drawing
for(int i =0 ; i < this->m_numTestSpheres ;i++ )
{
	m_Spheres[i].Draw( viewIndex );// draw function checks stored cull results based on view index
}	


#endif
#endif // RM_OCCLUDE_OCCLUDER_HOM_H
