// 
// rmocclude/occluder_primitive.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// 
// rmocclude/occlude_primitive.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "bank/bank.h"
#include "data/resource.h"

#include "file/asset.h"
#include "diag/memstats.h"
#include "system/timemgr.h"
#include "string/string.h"
#include "system/param.h"

#include "grcore/device.h"
#include "grcore/viewport.h"
//#include "grcore/state.h"
#include "grcore/im.h"


#include "occluder.h"
#include "occluder_primitive.h"

#define		__SPANBUFFER_ENABLED			0
#define		__EARLY_OUT_AABOX_CULLBITS_TEST 0





// Changed the wording on this cull test because returning 'true' when an object is outside of the view frustum with a function named
// 'IsHotDogOccluded' is confusing to some people.
rmPvsCullStatusBits rmOccluderSystem::TestCapsuleVisibility(	const pvsViewerID id,
																const Vector3 &extentA,
																const Vector3 &extentB,
																const float radius,
																const rmPvsCullModeBits	cmBits
																)
{
	bool bFrustumTest			= (cmBits&pvscm_FrustumTest)		== pvscm_FrustumTest;
	bool bOcclusionTestEnabled	= (cmBits&pvscm_OccludeTest)		== pvscm_OccludeTest;
	bool bAlwaysOcclusionTest	= (cmBits&pvscm_ForceOccludeTest)	== pvscm_ForceOccludeTest;

	if( (cmBits & pvscm_EnabledCulling_mask) == 0 )
	{
		return pvscull_all_clear;// no culling done so we don't know!
	}

	rmPvsCullStatusBits	cullBits	= pvscull_all_clear;
	PvsViewer			*pviewer	= GetPvsViewerPtr(id);

	if( bFrustumTest )
	{
		Matrix44	*pMtx0 = pviewer->GetCullMtx0();
		Matrix44	*pMtx1 = pviewer->GetCullMtx1();
		// maybe we should return an enum/bitflags instead of a bool result.

		Vector4 distancesLRNZ;                                 // left, right, near and z distance
		Vector4 distancesBTF;                                  // bottom, top and far distances
		Vector4 sphere4_A(extentA.x,extentA.y,extentA.z,1.0f); // until I make a vector4*mtx44 that ignores w!
		Vector4 sphere4_B(extentB.x,extentB.y,extentB.z,1.0f); // until I make a vector4*mtx44 that ignores w!

		bool	bInside = false;
		u32		AllVerts_AndResult_clipStatus = pvscull_frust_plane_bits_mask;	// logical product of all corner tests

		u32		clipStatus_A	= 0U;						// *MUST RESET FOR EACH VERTEX TEST!*
		u32		clipStatus_B	= 0U;						// *MUST RESET FOR EACH VERTEX TEST!*

		// fully outside?
		distancesLRNZ.Dot( sphere4_A, *pMtx0 );//test first 3 planes.
		//> float zDist = distancesLRNZ.w; // zdist to viewer (TODO: track min/max zdist of object if we processed all corners?)

		if (distancesLRNZ.x > radius)
			clipStatus_A |= pvscull_frust_plane_posL;
		else if (distancesLRNZ.x < -radius)
			clipStatus_A |= pvscull_frust_plane_negL;

		if (distancesLRNZ.y > radius)
			clipStatus_A |= pvscull_frust_plane_posR;
		else if (distancesLRNZ.y < -radius)
			clipStatus_A |= pvscull_frust_plane_negR;

		if (distancesLRNZ.z > radius)
			clipStatus_A |= pvscull_frust_plane_posN;
		else if (distancesLRNZ.z < -radius)
			clipStatus_A |= pvscull_frust_plane_negN;

		distancesBTF.Dot( sphere4_A, *pMtx1 );//test next 3 planes.

		if (distancesBTF.x > radius)
			clipStatus_A |= pvscull_frust_plane_posB;
		else if (distancesBTF.x < -radius)
			clipStatus_A |= pvscull_frust_plane_negB;

		if (distancesBTF.y > radius)
			clipStatus_A |= pvscull_frust_plane_posT;
		else if (distancesBTF.y < -radius)
			clipStatus_A |= pvscull_frust_plane_negT;

		if (distancesBTF.z > radius)
			clipStatus_A |= pvscull_frust_plane_posF;
		else if (distancesBTF.z < -radius)
			clipStatus_A |= pvscull_frust_plane_negF;


		//----------------B
		// fully outside?
		distancesLRNZ.Dot( sphere4_B, *pMtx0 );//test first 3 planes.
		//> float zDist = distancesLRNZ.w; // zdist to viewer (TODO: track min/max zdist of object if we processed all corners?)


		if (distancesLRNZ.x > radius)
			clipStatus_B |= pvscull_frust_plane_posL;
		else if (distancesLRNZ.x < -radius)
			clipStatus_B |= pvscull_frust_plane_negL;

		if (distancesLRNZ.y > radius)
			clipStatus_B |= pvscull_frust_plane_posR;
		else if (distancesLRNZ.y < -radius)
			clipStatus_B |= pvscull_frust_plane_negR;

		if (distancesLRNZ.z > radius)
			clipStatus_B |= pvscull_frust_plane_posN;
		else if (distancesLRNZ.z < -radius)
			clipStatus_B |= pvscull_frust_plane_negN;

		distancesBTF.Dot( sphere4_B, *pMtx1 );//test next 3 planes.

		if (distancesBTF.x > radius)
			clipStatus_B |= pvscull_frust_plane_posB;
		else if (distancesBTF.x < -radius)
			clipStatus_B |= pvscull_frust_plane_negB;

		if (distancesBTF.y > radius)
			clipStatus_B |= pvscull_frust_plane_posT;
		else if (distancesBTF.y < -radius)
			clipStatus_B |= pvscull_frust_plane_negT;

		if (distancesBTF.z > radius)
			clipStatus_B |= pvscull_frust_plane_posF;
		else if (distancesBTF.z < -radius)
			clipStatus_B |= pvscull_frust_plane_negF;

		AllVerts_AndResult_clipStatus &= clipStatus_A;
		AllVerts_AndResult_clipStatus &= clipStatus_B;

		if(	((clipStatus_A & pvscull_frust_plane_inside_mask) == pvscull_frust_plane_inside_mask) &&
			((clipStatus_B & pvscull_frust_plane_inside_mask) == pvscull_frust_plane_inside_mask) )// found a vertex that is completely inside, that means there's no way this thing is outside the frustum, break out of loop.
		{
			bInside = true;
			//	break;//  at least 1 vertex is inside, break out, its a waste to test anymore. Only thing that can hide this now is an occluder.
		}

		if( !bInside )
		{
			// didn't break out of loop, check out the clip flags
			if( AllVerts_AndResult_clipStatus & pvscull_frust_plane_outside_mask )
			{
				// all verts clip flags 'and'ed together show all the vertices were outside at least 1 single plane.

				if( bOcclusionTestEnabled && !bAlwaysOcclusionTest )// enabled and not forced regardless of frustum results.
				{
					return pvscull_OutsideFrustum;// (outside frustum)
				}
				else
				{
					rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
					//cullBits |= pvscull_OutsideFrustum;
					// fallthrough.
				}
			}
		}
	}
	// If we fell through to here, its within the frustum.

	// have to do these tests after the frustum test if its enabled, otherwise we return an incorrect value!
#if __DEV
	if( !AreOccludersEnabled()  )
	{
		rmOcclude::SET_BIT(cullBits, pvscull_InsideFrustum);
		return  cullBits;
	}
#endif

	if( pviewer->GetNumActiveOccluders() == 0 )
	{
		rmOcclude::SET_BIT(cullBits, pvscull_InsideFrustum);
		return  cullBits;
	}

	int total_active = pviewer->GetNumActiveOccluders();

	// Loop through occluders.
	for( int idx=0; idx < total_active; idx++ )
	{
		mcActiveOccluderPoly	*pActiveOccluderPoly = pviewer->GetActiveOccluderIndexPtr( idx );
		int						numplanes = pActiveOccluderPoly->m_num_planes;
		bool					bFail = false;

		if( ! pActiveOccluderPoly->IsActive() )// ccoffin [8/24/2006] skip deactivated occluders
		{
			continue;
		}

		if( pActiveOccluderPoly->m_pOccPoly )// handle geom occluders (null ptr case) safely
		{
			continue;
		}

		for( int plane=0; plane < numplanes; plane++)
		{
			Vector4	*pPlane = &pActiveOccluderPoly->m_occlusionPlanes[ plane ];
			// sub loop, test each both spheres, first case that is outside on either one fails and we test the next occluder.

			Assert( !bFail );

			if( pPlane->DistanceToPlane( extentA ) > -radius )
			{
				bFail = true;// crossing, tag as failing test, break loop and move to testing next occluder
				break;
			}

			if( pPlane->DistanceToPlane( extentB ) > -radius )
			{
				bFail = true;// crossing, tag as failing test, break loop and move to testing next occluder
				break;
			}
		}

		if( bFail )
		{
			continue;// move to testing next occluder
		}
		else
		{
#if __DEV
			// both spheres are contained inside the same occlusion hull.
			m_OccludedCount++;// track

			if( sm_AllowDebugDrawOfOccludedVolumes )
			{
				// Just buffer two spheres for now, retained rendering doesnt support different occluded bound types...
				if( sm_OccludedDrawBoundTotal < sm_maxDebugOccluders-1 )// minus 1 because we store 2 spheres.
				{
					if( OccludedVolumesBuffered_GetMode() )// bound buffering on for this test?
					{	
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_center = extentA;
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_radius = radius;
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_debugDrawColor = GetOccludedVolumeDebugColor();
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_occludedViewID = id;
						sm_OccludedDrawBoundTotal++;

						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_center = extentB;
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_radius = radius;
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_debugDrawColor = GetOccludedVolumeDebugColor();
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_occludedViewID = id;
						sm_OccludedDrawBoundTotal++;
					}		
				}
			}
#endif

			rmOcclude::SET_BIT(cullBits, pvscull_Occluded);
			return  cullBits;
		}
	}// end looping all active occluders

	rmOcclude::SET_BIT(cullBits, pvscull_NotOccluded);
	return  cullBits;
}





rmPvsCullStatusBits rmOccluderSystem::TestSphereVisibility(	const pvsViewerID		id,
															const Vector4			&Sphere,
															const rmPvsCullModeBits	cmBits
														   )
{
	Vector3			CenterA(Sphere.x, Sphere.y, Sphere.z );
    float			radius = Sphere.w;

	return TestSphereVisibility( id, CenterA, radius, cmBits);
}



// pretty much a cute+paste+modify of the capsule test code, just doing 1 instead of 2 sphere tests though.
rmPvsCullStatusBits rmOccluderSystem::TestSphereVisibility(	const pvsViewerID		id,
															const Vector3			&CenterA,
															const float				radius,
															const rmPvsCullModeBits	cmBits
															)
{
	bool bFrustumTest			= (cmBits&pvscm_FrustumTest)		== pvscm_FrustumTest;
	bool bOcclusionTestEnabled	= (cmBits&pvscm_OccludeTest)		== pvscm_OccludeTest;
	bool bAlwaysOcclusionTest	= (cmBits&pvscm_ForceOccludeTest)	== pvscm_ForceOccludeTest;

	if( (cmBits & pvscm_EnabledCulling_mask) == 0 )
	{
		// no culling done at all so return 'undefined'.
		// Game side code will probably want to interpret this result as 'visible' (maybe).
		return pvscull_all_clear;
	}

	rmPvsCullStatusBits	cullBits	= pvscull_all_clear;
	PvsViewer	*pviewer			= GetPvsViewerPtr(id);
	
#define USE_VECTOR_COMPARE 1   // added by SR, to remove a large number of load hit stores and fcmp's
#if USE_VECTOR_COMPARE
	Vector4 radiusV; 
	radiusV.Set(-radius);
	Vector4 CenterV; 
	CenterV.SetVector3(CenterA);
	CenterV.w = -1; // for fast Ax+By+Cz-D calcualation with __vdot4fp
#endif


	if( bFrustumTest )
	{
		Vector4		distancesLRNZ;                                 // left, right, near and z distance
		Vector4		distancesBTF;                                  // bottom, top and far distances
		Vector4		sphere4_A(CenterA.x,CenterA.y,CenterA.z,1.0f); // until I make a vector4*mtx44 that ignores w!
		Matrix44	*pMtx0 = pviewer->GetCullMtx0();
		Matrix44	*pMtx1 = pviewer->GetCullMtx1();

		rmPvsCullStatusBits	AllVerts_AndResult_clipStatus	= pvscull_frust_plane_bits_mask;	// logical product of all corner tests
		rmPvsCullStatusBits	clipStatus_A					= pvscull_all_clear;	// *MUST RESET FOR EACH VERTEX TEST!*
		// bool				bInside = false;

		// fully outside?
		distancesLRNZ.Dot( sphere4_A, *pMtx0 );//test first 3 planes.
		//> float zDist = distancesLRNZ.w; // zdist to viewer (TODO: track min/max zdist of object if we processed all corners?)

		if (distancesLRNZ.x > radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_posL);
		else if (distancesLRNZ.x < -radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_negL);

		if (distancesLRNZ.y > radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_posR);
		else if (distancesLRNZ.y < -radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_negR);

		if (distancesLRNZ.z > radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_posN);
		else if (distancesLRNZ.z < -radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_negN);

		distancesBTF.Dot( sphere4_A, *pMtx1 );//test next 3 planes.

		if (distancesBTF.x > radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_posB);
		else if (distancesBTF.x < -radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_negB);

		if (distancesBTF.y > radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_posT);
		else if (distancesBTF.y < -radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_negT);

		if (distancesBTF.z > radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_posF);
		else if (distancesBTF.z < -radius)
			rmOcclude::SET_BIT(clipStatus_A, pvscull_frust_plane_negF);

//		AllVerts_AndResult_clipStatus &= clipStatus_A;
		rmOcclude::AND_BITS( AllVerts_AndResult_clipStatus, clipStatus_A );

		rmOcclude::SET_BIT( cullBits, AllVerts_AndResult_clipStatus);// make sure the 1st 12 bits are set in the returned value!

		//if(	((clipStatus_A & pvscull_frust_plane_inside_mask) == pvscull_frust_plane_inside_mask) )
		if( rmOcclude::MASK_MATCHES(clipStatus_A, pvscull_frust_plane_inside_mask))
		{
			// found a vertex that is completely inside, that means there's no way this thing is outside the frustum, break out of loop.
			// bInside = true;
			rmOcclude::SET_BIT(cullBits, pvscull_InsideFrustum);
			//cullBits |= pvscull_InsideFrustum;
		}
		else // -> if( !bInside )
		{
			// didn't break out of loop, check out the clip flags
			if( AllVerts_AndResult_clipStatus & pvscull_frust_plane_outside_mask )
			{
				// all verts clip flags 'and'ed together show all the vertices were outside at least 1 single plane.

				if( bOcclusionTestEnabled && !bAlwaysOcclusionTest )// enabled and not forced regardless of frustum results.
				{
					rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
					//cullBits |= pvscull_OutsideFrustum;

					
					

					return cullBits;// (outside frustum)
				}
				else
				{
					rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
					//cullBits |= pvscull_OutsideFrustum;
					// fall-through, don't exit function.
				}
			}
		}
	}
	// If we fell through to here, its within/crossing the frustum or they just want occlusion results and we skipped the frustum test.

	// have to do these tests after the frustum test if its enabled, otherwise we return an incorrect value!
#if __DEV
	if( !AreOccludersEnabled()  )
	{
		//cullBits |= pvscull_InsideFrustum; // cant do this - frustum culling was possibly not enabled!
		return  cullBits;
	}
#endif

	if( pviewer->GetNumActiveOccluders() == 0 )
	{
		//cullBits |= pvscull_InsideFrustum; // cant do this - frustum culling was possibly not enabled!
		return cullBits;// no active occluders, cant be occluded so just return frustum cull result.
	}

	int total_active = pviewer->GetNumActiveOccluders();

	// Loop through occluders.
	for( int idx=0; idx < total_active; idx++ )
	{
		mcActiveOccluderPoly	*pActiveOccluderPoly = pviewer->GetActiveOccluderIndexPtr( idx );
		int						numplanes = pActiveOccluderPoly->m_num_planes;
		bool					bFail = false;

		if( ! pActiveOccluderPoly->IsActive() )// ccoffin [8/24/2006] skip deactivated occluders
		{
			continue;
		}

		if( pActiveOccluderPoly->m_pOccPoly )// handle geom occluders (null ptr case) safely
		{
			continue;
		}

		for( int plane=0; plane < numplanes; plane++)
		{
			Vector4	*pPlane = &pActiveOccluderPoly->m_occlusionPlanes[ plane ];
			// sub loop, test each both spheres, first case that is outside on either one fails and we test the next occluder.

			Assert( !bFail );
			
#if USE_VECTOR_COMPARE
			if( pPlane->DotV(CenterV).IsGreaterThan(radiusV) )
#else
			if( pPlane->DistanceToPlane( CenterA ) > -radius )
#endif		
			{
				bFail = true;// crossing, tag as failing test, break loop and move to testing next occluder
				break;
			}
		}

		if( bFail )
		{
			continue;// move to testing next occluder
		}
		else
		{
#if __DEV
			// both spheres are contained inside the same occlusion hull.
			m_OccludedCount++;// track

			if( sm_AllowDebugDrawOfOccludedVolumes )
			{
				// Just buffer two spheres for now, retained rendering doesnt support different occluded bound types...
				if( sm_OccludedDrawBoundTotal < sm_maxDebugOccluders )
				{
					if( OccludedVolumesBuffered_GetMode() )// bound buffering on for this test?
					{	
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_center = CenterA;
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_radius = radius;
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_debugDrawColor = GetOccludedVolumeDebugColor();
						sm_a_bndInfo[ sm_OccludedDrawBoundTotal ].m_occludedViewID = id;
						sm_OccludedDrawBoundTotal++;
					}
				}
			}
#endif
			rmOcclude::SET_BIT(cullBits, pvscull_Occluded);
			//cullBits |= pvscull_Occluded;
			return cullBits;
		}
	}// end looping all active occluders

	rmOcclude::SET_BIT(cullBits, pvscull_NotOccluded);
	//cullBits |= pvscull_NotOccluded;
	return cullBits;
}







// Made sure clipping plane flags are properly or'd into the returned 'rmPvsCullStatusBits' for all code branches inside the new primitive culling functions.
// Note: If using the 'rmPvsCullModeBits' flag 'pvscm_EarlyInsideFrustumTest', the logically and'ed results of all vertices may not be complete due to a loop optimization.
//	  If you require the unioned result of all clipflags for all vertices, do not use the bitflag 'pvscm_EarlyInsideFrustumTest'. This feature was requested by R* North.
//  ccoffin [4/3/2006]
rmPvsCullStatusBits	rmOccluderSystem::TestAABoxVisibility(	const pvsViewerID	id,
															const Vector4		*pv4Min,
															const Vector4		*pv4Max,
															const Matrix34		*pMtxLocalToWorld,// if null, we assume identity xform.
															const rmPvsCullModeBits	cmBits
															)
{

	bool bFrustumTest				= (cmBits&pvscm_FrustumTest)			== pvscm_FrustumTest;
	bool bOcclusionTestEnabled		= (cmBits&pvscm_OccludeTest)			== pvscm_OccludeTest;
	bool bAlwaysOcclusionTest		= (cmBits&pvscm_ForceOccludeTest)		== pvscm_ForceOccludeTest;
	bool bEarlyInsideFrustumTest	= (cmBits&pvscm_EarlyInsideFrustumTest) == pvscm_EarlyInsideFrustumTest;

	if( (cmBits & pvscm_EnabledCulling_mask) == 0 )
	{
		return pvscull_all_clear;// no culling done o we don't know!
	}

	rmPvsCullStatusBits	cullBits	= pvscull_all_clear;
	PvsViewer	*pviewer			= GetPvsViewerPtr(id);

	Matrix44	*pMtx0 = pviewer->GetCullMtx0();
	Matrix44	*pMtx1 = pviewer->GetCullMtx1();

	// synthesize 8 corners in local space, transform to worldspace then test against occluders
	Vector3	worldCorners[8];

	worldCorners[0].x = pv4Min->x;
	worldCorners[0].y = pv4Min->y;
	worldCorners[0].z = pv4Min->z;

	worldCorners[4].x = pv4Max->x;
	worldCorners[4].y = pv4Max->y;
	worldCorners[4].z = pv4Max->z;

	worldCorners[5].x = worldCorners[4].x;
	worldCorners[5].y = worldCorners[4].y;
	worldCorners[5].z = worldCorners[0].z;

	worldCorners[6].x = worldCorners[4].x;
	worldCorners[6].z = worldCorners[4].z;
	worldCorners[6].y = worldCorners[0].y;

	worldCorners[7].y = worldCorners[0].y;
	worldCorners[7].z = worldCorners[0].z;
	worldCorners[7].x = worldCorners[4].x;

	worldCorners[1].x = worldCorners[0].x;
	worldCorners[1].y = worldCorners[0].y;
	worldCorners[1].z = worldCorners[4].z;

	worldCorners[2].x = worldCorners[0].x;
	worldCorners[2].z = worldCorners[0].z;
	worldCorners[2].y = worldCorners[4].y;

	worldCorners[3].y = worldCorners[4].y;
	worldCorners[3].z = worldCorners[4].z;
	worldCorners[3].x = worldCorners[0].x;

	if( pMtxLocalToWorld )// if we get passed NULL, we skip the transform assuming the aabox is using an identity matrix, which also means it stays axis aligned!
	{
		// transform box corners
		for(int i=0; i < 8 ; i++)
		{
			pMtxLocalToWorld->Transform(worldCorners[i]);
		}
	}

	const	float radius = 0.f;// testing points, not spheres so it should be zero.
	u32	AllCorners_AndResult_clipStatus = pvscull_frust_plane_bits_mask;// logical product of all vertex tests

	// corners are in worldspace now, frustum test if requested
	if( bFrustumTest )
	{
		// todo: implement?
		// if outside frustum, return 'true' occluded result. maybe we should return an enum/bitflags instead!
		bool	bInside = false;

		// NOTE: first time a corner's bitflags results are 'inside' all planes we can break out of the loop since we know its not going to be 
		for(int i=0; i < 8 ; i++)
		{
			Vector4 distancesLRNZ;                                 // left, right, near and z distance
			Vector4 distancesBTF;                                  // bottom, top and far distances
			Vector4 center;//(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!

			// setup test point
			center.x = worldCorners[i].x;
			center.y = worldCorners[i].y;
			center.z = worldCorners[i].z;
			center.w = 1.f;

			u32	clipStatus = 0U;// reset for each vertex

			// fully outside?
			distancesLRNZ.Dot(center, *pMtx0 );//test first 3 planes.

			//> float zDist = distancesLRNZ.w; // zdist to viewer (TODO: track min/max zdist of object if we processed all corners?)

			if (distancesLRNZ.x > radius)
				clipStatus |= pvscull_frust_plane_posL;
			else if (distancesLRNZ.x < -radius)
				clipStatus |= pvscull_frust_plane_negL;

			if (distancesLRNZ.y > radius)
				clipStatus |= pvscull_frust_plane_posR;
			else if (distancesLRNZ.y < -radius)
				clipStatus |= pvscull_frust_plane_negR;

			if (distancesLRNZ.z > radius)
				clipStatus |= pvscull_frust_plane_posN;
			else if (distancesLRNZ.z < -radius)
				clipStatus |= pvscull_frust_plane_negN;

			distancesBTF.Dot(center, *pMtx1 );//test next 3 planes.

			if (distancesBTF.x > radius)
				clipStatus |= pvscull_frust_plane_posB;
			else if (distancesBTF.x < -radius)
				clipStatus |= pvscull_frust_plane_negB;

			if (distancesBTF.y > radius)
				clipStatus |= pvscull_frust_plane_posT;
			else if (distancesBTF.y < -radius)
				clipStatus |= pvscull_frust_plane_negT;

			if (distancesBTF.z > radius)
				clipStatus |= pvscull_frust_plane_posF;
			else if (distancesBTF.z < -radius)
				clipStatus |= pvscull_frust_plane_negF;

			AllCorners_AndResult_clipStatus &= clipStatus;

			if ((clipStatus & pvscull_frust_plane_inside_mask) == pvscull_frust_plane_inside_mask)// found a vertex that is completely inside, that means there's no way this thing is outside the frustum, break out of loop.
			{
				bInside = true;
				if( bEarlyInsideFrustumTest )
				{
					break;// 1 vertex is inside, break out, its a waste to test anymore. Only thing that can hide this now is an occluder.
				}
			}
		}

		if( !bInside )
		{
			// didn't break out of loop, check out the clip flags
			if( AllCorners_AndResult_clipStatus & pvscull_frust_plane_outside_mask )
			{
				// all verts clip flags 'and'ed together show all the vertices were outside at least 1 single plane.

				if( bOcclusionTestEnabled && !bAlwaysOcclusionTest )// enabled and not forced regardless of frustum results.
				{
					rmOcclude::SET_BIT(cullBits, AllCorners_AndResult_clipStatus);// mix in all the plane clipping results before leaving the function.

					rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
					return cullBits;
					//return pvscull_OutsideFrustum;// (outside frustum)
				}
				else
				{
					rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
					// cullBits |= pvscull_OutsideFrustum;
					// fallthrough.
				}
			}
		}
	}

	rmOcclude::SET_BIT(cullBits, AllCorners_AndResult_clipStatus);// mix in all the plane clipping results before leaving the function.

	// defer this test until after frustum test if its enabled.
	if( pviewer->GetNumActiveOccluders() == 0 ) return pvscull_InsideFrustum;


	if(0)// !g_bOccludeTestAABoxes )
	{
		// no occlusion testing on boxes set, and the above frustum test failed, so its visible.
		rmOcclude::SET_BIT(cullBits, pvscull_InsideFrustum);
		return cullBits;
		//return pvscull_InsideFrustum;
	}
	else
	{
		// Go through all occluders, test boxcorners against each plane - if one of the vertices fails the test then its crossing.
		//
		// note: if we need to implement an in/out/crossing result instead of pass/fail then we can't early out.
		int total_active = pviewer->GetNumActiveOccluders();

		for( int idx=0; idx < total_active; idx++ )
		{
			mcActiveOccluderPoly *pActiveOccluderPoly = pviewer->GetActiveOccluderIndexPtr( idx );

			if( ! pActiveOccluderPoly->IsActive() )// ccoffin [8/24/2006] skip deactivated occluders
			{
				continue;
			}

			if( pActiveOccluderPoly->m_pOccPoly )// handle geom occluders (null ptr case) safely
			{
				continue;// dont support poly plane occluders right now, just geom ones.

				// Test occluder poly first.
				//			if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly->GetPlane().DistanceToPlane( center ) > -radius )
				//			{
				//				continue;// outside or crossing occlusion frustum, move to next active.
				//			}
			}

			int numplanes = pActiveOccluderPoly->m_num_planes;

			bool bFail = false;

			for( int plane=0; plane < numplanes; plane++)
			{
				Vector4	*pPlane = &pActiveOccluderPoly->m_occlusionPlanes[ plane ];
				// sub loop, test each corner in transformed box, first case that is outside fails

				Assert( !bFail );

				if( pPlane->DistanceToPlane( worldCorners[0] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[4] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}


				if( pPlane->DistanceToPlane( worldCorners[1] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[2] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[3] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[5] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[6] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				if( pPlane->DistanceToPlane( worldCorners[7] ) > -radius )
				{
					bFail = true;// crossing, tag as failing test, break loop
					break;
				}

				/*
				if( m_activeOccluders[ viewID ][ idx ].m_occlusionPlanes[ plane ].DistanceToPlane( center ) > -radius )
				{
				bFail = true;// crossing, tag as failing test, break loop
				break;
				}			
				*/
			}

			if( !bFail )
			{
				// todo: buffer occluded volumes here! (in a different color too!)

				// todo: pass back occluder ID# to object so it can remember which occluder hid it last time, for possible reordering?

				// todo: track number of objects hidden per occluder, adjust occluder test order based on last frame's stats?
/*

				if( !bAlwaysOcclusionTest )
				{
					rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
					return cullBits;
					//return pvscull_OutsideFrustum;// (outside frustum)
				}
				else
				{
					rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
				//	cullBits |= pvscull_OutsideFrustum;
					// fallthrough.
				}
*/
				rmOcclude::SET_BIT(cullBits, pvscull_Occluded );
				return cullBits; // occluded completely by one of our occluders
			}

		}// continue looping through rest of occluder plane sets.

	}

	rmOcclude::SET_BIT(cullBits, pvscull_NotOccluded );
	return cullBits;// failed occlusion tests.
}



// Made sure clipping plane flags are properly or'd into the returned 'rmPvsCullStatusBits' for all code branches inside the new primitive culling functions.
// Note: If using the 'rmPvsCullModeBits' flag 'pvscm_EarlyInsideFrustumTest', the logically and'ed results of all vertices may not be complete due to a loop optimization.
//	  If you require the unioned result of all clipflags for all vertices, do not use the bitflag 'pvscm_EarlyInsideFrustumTest'. This feature was requested by R* North.
// ccoffin [8/24/2006]
rmPvsCullStatusBits	rmOccluderSystem::TestVertexPoolVisibility(	const pvsViewerID	id,
														  const Vector3		*pVertices,
														  const	int			vertexCount,
														 // const Matrix34	*pMtxLocalToWorld,// if null, we assume identity xform.
														  const rmPvsCullModeBits	cmBits,
														  const rmOccludePolyhedron *pExemptHedra // NULL by default if this feature isn't used.
														  )
{

	bool bFrustumTest				= (cmBits&pvscm_FrustumTest)			== pvscm_FrustumTest;
	bool bOcclusionTestEnabled		= (cmBits&pvscm_OccludeTest)			== pvscm_OccludeTest;
	bool bAlwaysOcclusionTest		= (cmBits&pvscm_ForceOccludeTest)		== pvscm_ForceOccludeTest;
	bool bEarlyInsideFrustumTest	= (cmBits&pvscm_EarlyInsideFrustumTest) == pvscm_EarlyInsideFrustumTest;

	if( (cmBits & pvscm_EnabledCulling_mask) == 0 )
	{
		return pvscull_all_clear;// no culling done o we don't know!
	}

	rmPvsCullStatusBits	cullBits	= pvscull_all_clear;
	PvsViewer	*pviewer			= GetPvsViewerPtr(id);

	Matrix44	*pMtx0 = pviewer->GetCullMtx0();
	Matrix44	*pMtx1 = pviewer->GetCullMtx1();

	const	float radius = 0.f;// testing points, not spheres so it should be zero.
	u32	AllCorners_AndResult_clipStatus = pvscull_frust_plane_bits_mask;// logical product of all vertex tests

	// corners are in worldspace now, frustum test if requested
	if( bFrustumTest )
	{
		// todo: implement?
		// if outside frustum, return 'true' occluded result. maybe we should return an enum/bitflags instead!
		bool	bInside = false;

		// NOTE: first time a corner's bitflags results are 'inside' all planes we can break out of the loop since we know its not going to be 
		for(int i=0; i < vertexCount ; i++)
		{
			Vector4 distancesLRNZ;                                 // left, right, near and z distance
			Vector4 distancesBTF;                                  // bottom, top and far distances
			Vector4 center;//(sphere.x,sphere.y,sphere.z,1.0f);       // until I make a vector4*mtx44 that ignores w!

			// setup test point
			center.x = pVertices[i].x;
			center.y = pVertices[i].y;
			center.z = pVertices[i].z;
			center.w = 1.f;

			u32	clipStatus = 0U;// reset for each vertex

			// fully outside?
			distancesLRNZ.Dot(center, *pMtx0 );//test first 3 planes.

			//> float zDist = distancesLRNZ.w; // zdist to viewer (TODO: track min/max zdist of object if we processed all corners?)

			if (distancesLRNZ.x > radius)
				clipStatus |= pvscull_frust_plane_posL;
			else if (distancesLRNZ.x < -radius)
				clipStatus |= pvscull_frust_plane_negL;

			if (distancesLRNZ.y > radius)
				clipStatus |= pvscull_frust_plane_posR;
			else if (distancesLRNZ.y < -radius)
				clipStatus |= pvscull_frust_plane_negR;

			if (distancesLRNZ.z > radius)
				clipStatus |= pvscull_frust_plane_posN;
			else if (distancesLRNZ.z < -radius)
				clipStatus |= pvscull_frust_plane_negN;

			distancesBTF.Dot(center, *pMtx1 );//test next 3 planes.

			if (distancesBTF.x > radius)
				clipStatus |= pvscull_frust_plane_posB;
			else if (distancesBTF.x < -radius)
				clipStatus |= pvscull_frust_plane_negB;

			if (distancesBTF.y > radius)
				clipStatus |= pvscull_frust_plane_posT;
			else if (distancesBTF.y < -radius)
				clipStatus |= pvscull_frust_plane_negT;

			if (distancesBTF.z > radius)
				clipStatus |= pvscull_frust_plane_posF;
			else if (distancesBTF.z < -radius)
				clipStatus |= pvscull_frust_plane_negF;

			AllCorners_AndResult_clipStatus &= clipStatus;

			if ((clipStatus & pvscull_frust_plane_inside_mask) == pvscull_frust_plane_inside_mask)// found a vertex that is completely inside, that means there's no way this thing is outside the frustum, break out of loop.
			{
				bInside = true;
				if( bEarlyInsideFrustumTest )
				{
					break;// 1 vertex is inside, break out, its a waste to test anymore. Only thing that can hide this now is an occluder.
				}
			}
		}

		if( !bInside )
		{
			// didn't break out of loop, check out the clip flags
			if( AllCorners_AndResult_clipStatus & pvscull_frust_plane_outside_mask )
			{
				// all verts clip flags 'and'ed together show all the vertices were outside at least 1 single plane.

				if( bOcclusionTestEnabled && !bAlwaysOcclusionTest )// enabled and not forced regardless of frustum results.
				{
					rmOcclude::SET_BIT(cullBits, AllCorners_AndResult_clipStatus);// mix in all the plane clipping results before leaving the function.

					rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
					return cullBits;
					//return pvscull_OutsideFrustum;// (outside frustum)
				}
				else
				{
					rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
					// cullBits |= pvscull_OutsideFrustum;
					// fallthrough.
				}
			}
		}
	}

	rmOcclude::SET_BIT(cullBits, AllCorners_AndResult_clipStatus);// mix in all the plane clipping results before leaving the function.

	// defer this test until after frustum test if its enabled.
	if( pviewer->GetNumActiveOccluders() == 0 ) return pvscull_InsideFrustum;


	if(0)// !g_bOccludeTestAABoxes )
	{
		// no occlusion testing on boxes set, and the above frustum test failed, so its visible.
		rmOcclude::SET_BIT(cullBits, pvscull_InsideFrustum);
		return cullBits;
		//return pvscull_InsideFrustum;
	}
	else
	{
		// Go through all occluders, test boxcorners against each plane - if one of the vertices fails the test then its crossing.
		//
		// note: if we need to implement an in/out/crossing result instead of pass/fail then we can't early out.
		int total_active = pviewer->GetNumActiveOccluders();

		for( int idx=0; idx < total_active; idx++ )
		{
			mcActiveOccluderPoly *pActiveOccluderPoly = pviewer->GetActiveOccluderIndexPtr( idx );

			if( pExemptHedra != NULL )
			{
				if( (rmOccludePolyhedron *) (pActiveOccluderPoly->m_Id) == pExemptHedra )
					continue;// to prevent self occlusion with active occluder planes against their source polyhedra.
			}

			if( ! pActiveOccluderPoly->IsActive() )// ccoffin [8/24/2006] skip deactivated occluders
			{
				continue;
			}

			if( pActiveOccluderPoly->m_pOccPoly )// handle geom occluders (null ptr case) safely
			{
				continue;// dont support poly plane occluders right now, just geom ones.

				// Test occluder poly first.
				//			if( m_activeOccluders[ viewID ][ idx ].m_pOccPoly->GetPlane().DistanceToPlane( center ) > -radius )
				//			{
				//				continue;// outside or crossing occlusion frustum, move to next active.
				//			}
			}

			int numplanes = pActiveOccluderPoly->m_num_planes;

			bool bFail = false;

			for( int plane=0; plane < numplanes; plane++)
			{
				Vector4	*pPlane = &pActiveOccluderPoly->m_occlusionPlanes[ plane ];
				// sub loop, test each corner in transformed box, first case that is outside fails

				Assert( !bFail );

				// subloop over all vertices
				for( int vtxIdx = 0; vtxIdx < vertexCount; vtxIdx++ )
				{		
					if( pPlane->DistanceToPlane( pVertices[ vtxIdx ] ) > -radius )
					{
						bFail = true;// crossing, tag as failing test, break loop
						break;
					}
				}

				if( bFail )
					break;

				/*
				if( m_activeOccluders[ viewID ][ idx ].m_occlusionPlanes[ plane ].DistanceToPlane( center ) > -radius )
				{
				bFail = true;// crossing, tag as failing test, break loop
				break;
				}			
				*/
			}

			if( !bFail )
			{
				// todo: buffer occluded volumes here! (in a different color too!)

				// todo: pass back occluder ID# to object so it can remember which occluder hid it last time, for possible reordering?

				// todo: track number of objects hidden per occluder, adjust occluder test order based on last frame's stats?
				/*

				if( !bAlwaysOcclusionTest )
				{
				rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
				return cullBits;
				//return pvscull_OutsideFrustum;// (outside frustum)
				}
				else
				{
				rmOcclude::SET_BIT(cullBits, pvscull_OutsideFrustum);
				//	cullBits |= pvscull_OutsideFrustum;
				// fallthrough.
				}
				*/
				rmOcclude::SET_BIT(cullBits, pvscull_Occluded );
				return cullBits; // occluded completely by one of our occluders
			}

		}// continue looping through rest of occluder plane sets.

	}

	rmOcclude::SET_BIT(cullBits, pvscull_NotOccluded );
	return cullBits;// failed occlusion tests.
}

#undef __SPANBUFFER_ENABLED
#undef __EARLY_OUT_AABOX_CULLBITS_TEST
