#if RSG_ORBIS  || __RESOURCECOMPILER || RSG_TOOL

#ifndef _INTPTR
#define _INTPTR 1
#endif

unsigned char unique_multikey_gta5_ps4[101][32] = {
	{ 0x87, 0x5c, 0x8e, 0xfd, 0x89, 0x15, 0xd0, 0xc8, 0x72, 0x32, 0xfb, 0x0f, 0x4f, 0x20, 0xe1, 0x49, 0x4c, 0x83, 0xcc, 0xfc, 0x06, 0x6a, 0x22, 0x96, 0x8a, 0x63, 0x40, 0xa5, 0x35, 0x6d, 0x04, 0x95},
	{ 0x2e, 0xf7, 0xc3, 0xca, 0x35, 0x22, 0x9b, 0x8a, 0x59, 0x4a, 0x03, 0xcf, 0x21, 0xd0, 0xd6, 0xc9, 0xac, 0x95, 0x89, 0xf2, 0x9a, 0x48, 0x5f, 0x59, 0x08, 0xd3, 0x11, 0x1b, 0x18, 0xba, 0x45, 0x08},
	{ 0x4b, 0x8b, 0x1b, 0x92, 0x52, 0xdb, 0x57, 0xc6, 0x5a, 0xa8, 0xec, 0x6b, 0x82, 0x01, 0x31, 0x75, 0x45, 0x58, 0xf7, 0xb6, 0xa9, 0x24, 0x94, 0xf9, 0x9e, 0x03, 0xc1, 0x2a, 0xe3, 0x57, 0xcc, 0x8e},
	{ 0x55, 0xc7, 0xa1, 0xc0, 0x83, 0xc1, 0x88, 0xde, 0xc4, 0x34, 0xdb, 0xbf, 0xd4, 0x0c, 0x0c, 0xe9, 0x10, 0x0f, 0x0b, 0x15, 0x29, 0x8d, 0x55, 0xdc, 0x20, 0x5f, 0x61, 0xdc, 0xb2, 0x0a, 0x5e, 0xb2},
	{ 0xd6, 0xe6, 0x00, 0x37, 0xf9, 0x47, 0x42, 0x11, 0xca, 0xe6, 0xda, 0xac, 0x03, 0xf7, 0x2c, 0xbe, 0x23, 0x5a, 0xdb, 0x0f, 0xc3, 0x2a, 0x02, 0xe0, 0x7a, 0xb0, 0x8e, 0xd5, 0x53, 0xd3, 0x10, 0xe6},
	{ 0x35, 0xf7, 0xb5, 0xaa, 0x44, 0x27, 0x4a, 0x9e, 0x31, 0x1e, 0x21, 0x0d, 0xa4, 0x38, 0xa7, 0x9c, 0xef, 0x31, 0x44, 0xd4, 0xb5, 0xfc, 0x17, 0x6a, 0x39, 0x9e, 0xac, 0xe0, 0x93, 0x6a, 0x93, 0xbe},
	{ 0x57, 0xae, 0x5a, 0x83, 0xee, 0x14, 0x52, 0xf6, 0x77, 0x48, 0xa3, 0x03, 0x88, 0x69, 0x37, 0xa6, 0x33, 0xa0, 0xf2, 0x80, 0x97, 0xfb, 0xea, 0xee, 0x4f, 0xc5, 0x7b, 0x67, 0x4a, 0xb3, 0xa1, 0x1f},
	{ 0x28, 0x4b, 0x46, 0xc7, 0xca, 0x60, 0xfe, 0x73, 0x9f, 0x32, 0xa1, 0x6d, 0x6e, 0x1e, 0xe9, 0xe4, 0x08, 0x3b, 0x9c, 0xee, 0x93, 0xe6, 0x98, 0x56, 0xb4, 0x81, 0x9d, 0xbc, 0x3d, 0x13, 0x87, 0xa0},
	{ 0x2c, 0x23, 0x8a, 0xcc, 0x03, 0x29, 0xfe, 0x5b, 0xe2, 0x31, 0x6c, 0x33, 0xfb, 0xde, 0x25, 0x76, 0xdb, 0xfe, 0x2e, 0x5b, 0x1c, 0xe0, 0x5a, 0xce, 0xe9, 0xd0, 0x54, 0x9e, 0x49, 0xac, 0xb8, 0x2c},
	{ 0x98, 0xb6, 0x43, 0xaf, 0x9b, 0x13, 0xd0, 0xcd, 0x52, 0x0e, 0x57, 0x68, 0x13, 0x12, 0xcf, 0x0c, 0x9f, 0x7b, 0x77, 0x69, 0xeb, 0x0e, 0xf0, 0x9a, 0x07, 0xc0, 0x1a, 0x9d, 0x31, 0x50, 0x3e, 0x29},
	{ 0x16, 0x9f, 0xa5, 0x14, 0x40, 0x9f, 0x66, 0x9e, 0x61, 0x40, 0x9b, 0x35, 0x59, 0x2f, 0x01, 0xb6, 0x50, 0x2f, 0x88, 0xbf, 0xcb, 0x65, 0x18, 0xdc, 0xb8, 0x79, 0x9c, 0x4e, 0x3b, 0x93, 0x04, 0xee},
	{ 0x0c, 0x1d, 0x8d, 0x95, 0x01, 0x81, 0x05, 0x29, 0x77, 0x78, 0x66, 0x0c, 0x2b, 0xfe, 0xa8, 0x7b, 0x77, 0x78, 0xc5, 0xf9, 0x46, 0x4e, 0xf2, 0x02, 0xb9, 0x9d, 0x12, 0xdc, 0xa2, 0x45, 0x6b, 0x06},
	{ 0xac, 0x53, 0x94, 0x6b, 0x04, 0x2e, 0xef, 0x1d, 0x64, 0xdf, 0x82, 0x97, 0x59, 0x91, 0xcc, 0x65, 0x4c, 0x58, 0x6d, 0xee, 0xd0, 0xc5, 0xc2, 0x84, 0x89, 0x9c, 0x5c, 0x67, 0x7d, 0xc6, 0x02, 0x8f},
	{ 0x6a, 0x85, 0xac, 0x34, 0xd2, 0x39, 0x4e, 0x1f, 0x83, 0x2e, 0x68, 0xa4, 0xb8, 0x42, 0x63, 0x3f, 0x3f, 0x32, 0x8b, 0xdd, 0x72, 0xac, 0xfd, 0x92, 0xb5, 0x1f, 0xa4, 0x54, 0x09, 0x3e, 0xe2, 0xed,},
	{ 0x76, 0xe5, 0x1a, 0xb7, 0x89, 0x0f, 0x38, 0xd9, 0x59, 0xb4, 0x55, 0xd4, 0xfb, 0x83, 0x11, 0xd8, 0x10, 0xb9, 0xd5, 0xb6, 0x41, 0xa6, 0x19, 0x3c, 0x4f, 0x70, 0xe1, 0x44, 0x11, 0x48, 0xed, 0x08},
	{ 0x74, 0xa7, 0xa8, 0x29, 0x7e, 0x0a, 0x39, 0xec, 0x57, 0x2e, 0x33, 0xae, 0xfb, 0xec, 0x1a, 0x92, 0x45, 0x39, 0xf6, 0x74, 0x02, 0x91, 0xc1, 0x1f, 0xdb, 0x03, 0x1c, 0x6b, 0xa4, 0xc2, 0xaa, 0xae},
	{ 0x58, 0xc6, 0x48, 0x34, 0x41, 0xc1, 0xcb, 0x68, 0x01, 0x8f, 0xef, 0x76, 0x28, 0xbb, 0x0a, 0x2c, 0x4d, 0x21, 0x0e, 0xe6, 0x5b, 0x58, 0x12, 0x93, 0x47, 0x9a, 0x90, 0xd6, 0x35, 0x17, 0xea, 0x0f},
	{ 0xe3, 0xba, 0x88, 0x9d, 0x16, 0xae, 0xb0, 0xda, 0xc8, 0xc7, 0x7d, 0x85, 0xa3, 0xb5, 0x83, 0x0b, 0x45, 0xd2, 0xfe, 0xbd, 0xd3, 0x19, 0x81, 0xdd, 0xa0, 0x34, 0xbe, 0xb1, 0x51, 0xb4, 0xb2, 0xfc},
	{ 0x7b, 0x58, 0xad, 0xac, 0xce, 0x8c, 0xa4, 0x93, 0x28, 0xed, 0xde, 0x75, 0xf5, 0xea, 0x53, 0x4c, 0xb0, 0x34, 0x05, 0x56, 0x0e, 0x9c, 0x7a, 0xfb, 0x13, 0xec, 0x7c, 0xc1, 0xca, 0xba, 0x1c, 0x31},
	{ 0xeb, 0xc4, 0x37, 0x26, 0xbd, 0x7e, 0x52, 0xae, 0xf9, 0xd0, 0xa4, 0xad, 0x07, 0x37, 0xcc, 0x6c, 0xb3, 0xf0, 0x05, 0xda, 0x37, 0xe0, 0xf8, 0xaa, 0x58, 0xab, 0x7e, 0xbb, 0xd5, 0x3f, 0x73, 0x77},
	{ 0xac, 0xbe, 0x22, 0xe8, 0x97, 0x42, 0x9e, 0x05, 0x9d, 0x33, 0xd5, 0x53, 0xbb, 0x88, 0x7d, 0xab, 0xa6, 0xd7, 0x68, 0xb2, 0xb2, 0xcc, 0x91, 0xf5, 0xca, 0x49, 0x9d, 0xa1, 0x14, 0x06, 0x46, 0xde},
	{ 0x95, 0x42, 0x78, 0x2d, 0x9f, 0x67, 0xf2, 0x41, 0x1f, 0x8b, 0xb9, 0xf7, 0xca, 0x47, 0xf0, 0x21, 0x08, 0x48, 0xf9, 0x66, 0x76, 0xdd, 0x35, 0xa2, 0x87, 0x60, 0xa2, 0x0f, 0x3e, 0xfe, 0xe2, 0x3e},
	{ 0xdd, 0x43, 0x01, 0xa9, 0xe5, 0xa1, 0x5e, 0x0c, 0x45, 0x9a, 0x93, 0x5b, 0x14, 0xa7, 0xdd, 0x54, 0xa9, 0xdb, 0x10, 0x15, 0x76, 0x02, 0xff, 0x1c, 0x29, 0xe0, 0x0b, 0xcd, 0xc0, 0x19, 0x05, 0xe1},
	{ 0x45, 0xde, 0x8c, 0x48, 0x02, 0xf1, 0x08, 0xf4, 0xab, 0xa5, 0x71, 0x79, 0xb2, 0x11, 0xbc, 0x69, 0x97, 0xd6, 0x57, 0xdf, 0x6a, 0xf1, 0xee, 0x65, 0xe7, 0xbe, 0x6a, 0xae, 0x64, 0x32, 0xbb, 0xdc},
	{ 0x0c, 0xc4, 0xe0, 0xbb, 0xef, 0xcf, 0xf7, 0xf5, 0x2b, 0xdc, 0x88, 0x5c, 0xe5, 0xc3, 0x3c, 0x13, 0xd2, 0x5a, 0x3c, 0x1f, 0x9a, 0xc8, 0xae, 0xe3, 0xa7, 0xd3, 0x84, 0xc1, 0xd3, 0x19, 0xc9, 0x3a},
	{ 0x74, 0xf8, 0x1d, 0xab, 0xe4, 0x25, 0x96, 0x03, 0x02, 0x20, 0x26, 0x8d, 0x0e, 0x8c, 0x42, 0xfe, 0x47, 0x35, 0x3e, 0x29, 0xad, 0x69, 0x30, 0x1b, 0xc9, 0x40, 0x6a, 0xba, 0xe5, 0xf3, 0x03, 0x84},
	{ 0xb3, 0xef, 0xa2, 0xb7, 0xd0, 0xbc, 0xe5, 0x35, 0x54, 0x94, 0x4b, 0xae, 0xac, 0xb8, 0xdc, 0xad, 0xe6, 0x73, 0x88, 0xcb, 0xe1, 0x04, 0x91, 0x66, 0x0c, 0x88, 0xfe, 0x54, 0x0e, 0x8f, 0x41, 0xf3},
	{ 0xb4, 0x31, 0xaa, 0x6c, 0x0d, 0xed, 0xfe, 0x8d, 0xcf, 0xcb, 0xc4, 0x8f, 0x9f, 0xa7, 0x67, 0x64, 0xb8, 0xeb, 0x72, 0x83, 0x39, 0x4d, 0x70, 0xb6, 0x7d, 0x97, 0x94, 0xc5, 0x1d, 0x9b, 0xc4, 0x12},
	{ 0x66, 0x4d, 0xbd, 0xdc, 0x32, 0xf6, 0x31, 0x7c, 0x7a, 0x17, 0xed, 0x62, 0x58, 0x65, 0xa8, 0x6e, 0x47, 0x05, 0xa2, 0x58, 0x41, 0x6b, 0x2d, 0x2c, 0x44, 0x72, 0xad, 0xd4, 0x8f, 0x79, 0xde, 0x6e},
	{ 0x92, 0x2a, 0xf6, 0x9d, 0xe4, 0xa1, 0x70, 0x3e, 0x0b, 0xc7, 0xc8, 0x45, 0x10, 0x60, 0x89, 0x53, 0x3d, 0x90, 0xb4, 0x32, 0xdf, 0x1b, 0x03, 0x05, 0x9a, 0xde, 0x81, 0xd3, 0x91, 0xab, 0x6b, 0x7b},
	{ 0x8c, 0x1c, 0xce, 0x0e, 0xcb, 0x96, 0xc8, 0x4e, 0x40, 0xbe, 0xfa, 0xb4, 0x84, 0xc0, 0x24, 0x42, 0x38, 0xad, 0x89, 0xab, 0x53, 0x3f, 0x39, 0x0b, 0x46, 0x3e, 0xbb, 0x87, 0x44, 0x65, 0xaa, 0x24},
	{ 0x07, 0xf3, 0x75, 0xc9, 0x81, 0x51, 0xfa, 0x38, 0x73, 0x08, 0x25, 0x95, 0x26, 0xf8, 0x93, 0x6d, 0xf3, 0xb4, 0xae, 0x0a, 0x22, 0xc6, 0x48, 0xa1, 0xfe, 0x18, 0xff, 0xc7, 0xa1, 0x78, 0xf0, 0xd8},
	{ 0x22, 0xce, 0x6b, 0xeb, 0xd6, 0xa4, 0xf1, 0x32, 0xb5, 0x4a, 0x6a, 0x96, 0x2d, 0xdd, 0x66, 0xc2, 0xfd, 0x0a, 0x10, 0x48, 0xa6, 0xb4, 0x61, 0x7d, 0x41, 0xf2, 0xde, 0x31, 0x1c, 0x88, 0x70, 0xb9},
	{ 0x5a, 0x64, 0x1b, 0x82, 0x8c, 0x6e, 0xf0, 0x88, 0xe8, 0x7b, 0x89, 0x5c, 0xe5, 0x4a, 0x98, 0x68, 0x2f, 0xa0, 0x40, 0xc9, 0x06, 0x51, 0x04, 0x36, 0x54, 0x12, 0x6e, 0xda, 0x94, 0x3c, 0x22, 0x68},
	{ 0xde, 0x28, 0xc4, 0xfb, 0x85, 0x48, 0x90, 0x8b, 0x76, 0x37, 0x40, 0xb5, 0x76, 0x5b, 0xfe, 0x1c, 0xcd, 0x8b, 0x67, 0xb0, 0x27, 0x5e, 0x56, 0x72, 0x13, 0xd3, 0xeb, 0xda, 0xbe, 0x19, 0x0c, 0xa9},
	{ 0x51, 0x60, 0xb7, 0x41, 0x91, 0x43, 0x68, 0xce, 0x91, 0xa1, 0x05, 0x2e, 0x37, 0x7d, 0xe8, 0xd8, 0x05, 0xa7, 0x70, 0x27, 0x9e, 0xf1, 0x57, 0xd0, 0x12, 0x30, 0x65, 0xc4, 0x63, 0xa9, 0xdb, 0x43},
	{ 0x66, 0x71, 0x9b, 0x36, 0x55, 0x50, 0xb8, 0x87, 0xe4, 0x34, 0x77, 0x9a, 0x52, 0xdd, 0x6b, 0xf1, 0xd5, 0xb1, 0x80, 0xdd, 0x68, 0x7e, 0x0b, 0xef, 0xb9, 0x52, 0xbc, 0x2e, 0xb6, 0xc5, 0x54, 0xde},
	{ 0xd2, 0x15, 0xce, 0x6e, 0xe3, 0x44, 0x39, 0x11, 0x2d, 0x08, 0x71, 0x95, 0x26, 0xd0, 0xba, 0x32, 0xf4, 0x7d, 0x17, 0x2b, 0x57, 0x8c, 0x31, 0xc0, 0xe2, 0x97, 0x14, 0xb2, 0xb3, 0x10, 0x49, 0x5b},
	{ 0xdb, 0xd1, 0x45, 0x9b, 0x4d, 0xc2, 0xa7, 0xa0, 0xe6, 0xe5, 0x24, 0x5e, 0xa4, 0xe1, 0xc0, 0xc8, 0x15, 0x1a, 0x7a, 0x89, 0x7b, 0x5d, 0xcf, 0xc1, 0x80, 0x37, 0xa8, 0x89, 0x6c, 0x33, 0xc3, 0x54},
	{ 0x1c, 0x89, 0x42, 0x0a, 0xa7, 0x1f, 0x33, 0xd0, 0x51, 0x9f, 0xcd, 0x0a, 0x9f, 0x39, 0x26, 0x64, 0xf9, 0x50, 0x10, 0x04, 0xe5, 0x9b, 0x3e, 0xa2, 0x18, 0x3a, 0xda, 0x3d, 0x42, 0x53, 0x6f, 0x28},
	{ 0x09, 0x87, 0x8d, 0x95, 0x67, 0xb1, 0xeb, 0x50, 0x1e, 0x78, 0xb8, 0xee, 0x7f, 0xf8, 0x11, 0xfe, 0x6a, 0xba, 0xc9, 0x3f, 0xfb, 0xe4, 0x7d, 0xcb, 0x0b, 0xd0, 0x2f, 0xe5, 0x7d, 0xb1, 0x83, 0x05},
	{ 0x9e, 0x5e, 0x02, 0xf6, 0x98, 0x75, 0x17, 0x6a, 0x1a, 0x46, 0x39, 0x7d, 0x27, 0x49, 0xe8, 0xf7, 0xe5, 0x92, 0x48, 0xf0, 0x53, 0x11, 0xac, 0x3b, 0xb2, 0x7c, 0xa2, 0xee, 0xce, 0x6f, 0x89, 0xc0},
	{ 0xbd, 0xd1, 0xcb, 0xba, 0x98, 0x3f, 0x64, 0x78, 0x3c, 0xb1, 0x29, 0x9a, 0x47, 0xc1, 0x94, 0x5d, 0xd6, 0x88, 0x5a, 0x3e, 0xa9, 0xa6, 0x4b, 0x2b, 0x3a, 0x02, 0x11, 0x27, 0x45, 0x2d, 0xa3, 0x95},
	{ 0xe5, 0x6f, 0x0c, 0xf7, 0x7f, 0x41, 0x72, 0xde, 0xaa, 0xe2, 0x9d, 0x07, 0xab, 0xfc, 0x7d, 0xe1, 0xb4, 0x7b, 0x4a, 0x38, 0x76, 0x0b, 0x5b, 0xe5, 0x8d, 0x11, 0x6b, 0x16, 0x9b, 0xb5, 0x34, 0xf3},
	{ 0x7b, 0x72, 0xfc, 0x76, 0xf1, 0x70, 0xaf, 0xfc, 0xe1, 0x92, 0xe7, 0x4f, 0x3a, 0x89, 0xe0, 0x9d, 0x01, 0x4a, 0x50, 0x3e, 0x86, 0x93, 0x06, 0x6a, 0x82, 0x82, 0x3e, 0x8f, 0x12, 0xa0, 0x25, 0x09},
	{ 0x98, 0x02, 0xfa, 0x1b, 0x32, 0x0a, 0x6e, 0x4e, 0x3c, 0xdb, 0x8b, 0x1d, 0x13, 0x49, 0x2d, 0x66, 0x8d, 0xca, 0x45, 0x67, 0x2c, 0xdf, 0x73, 0x5d, 0xfa, 0x17, 0x14, 0x6e, 0x51, 0x49, 0xa9, 0xd3},
	{ 0x29, 0x97, 0xc4, 0x7a, 0xd0, 0x89, 0xb6, 0xa2, 0xf7, 0x41, 0x65, 0x36, 0xe9, 0x50, 0x87, 0x60, 0x0f, 0x5a, 0x4e, 0x61, 0x90, 0x0e, 0x77, 0x66, 0xf1, 0xbb, 0xbe, 0x7f, 0x03, 0xc1, 0xc0, 0x2c},
	{ 0x9a, 0xdb, 0x7f, 0xa8, 0x17, 0x1e, 0x10, 0xfb, 0xbd, 0xcf, 0x90, 0xbf, 0xd9, 0x53, 0x43, 0xc4, 0x99, 0xf7, 0x2b, 0xa1, 0xf0, 0xf2, 0x4b, 0x68, 0xd4, 0x09, 0xad, 0x81, 0xfb, 0xc3, 0x7f, 0xab},
	{ 0x2d, 0xd6, 0xbd, 0x6a, 0x5f, 0xcb, 0xcc, 0xe0, 0x14, 0x08, 0x16, 0x89, 0x37, 0xe2, 0xaa, 0xbe, 0x79, 0x23, 0x9f, 0xcb, 0xf8, 0x76, 0xdd, 0x4c, 0x41, 0x49, 0x53, 0x6e, 0xea, 0x8b, 0x7d, 0xdb},
	{ 0x5b, 0x9a, 0x52, 0x63, 0xcf, 0xda, 0x8c, 0x20, 0xc7, 0x06, 0xf6, 0x6d, 0xcf, 0x58, 0x20, 0xc5, 0x1a, 0xf6, 0xa2, 0x65, 0xb4, 0x55, 0x39, 0x1f, 0x33, 0x93, 0x88, 0xaa, 0xf4, 0xd0, 0x0a, 0xd0},
	{ 0xa6, 0x02, 0x9e, 0x07, 0xff, 0xe1, 0xaa, 0x45, 0xe0, 0xcb, 0xcb, 0x1e, 0xe8, 0xc7, 0x23, 0x4f, 0xcc, 0x5f, 0x76, 0xad, 0x69, 0x11, 0xce, 0xea, 0xe2, 0xa6, 0x4a, 0xbe, 0xed, 0x50, 0x1b, 0x61},
	{ 0xeb, 0x7e, 0xf2, 0x10, 0x7f, 0x56, 0xe9, 0xb5, 0x5c, 0x95, 0xbc, 0xbe, 0xf3, 0x0e, 0x42, 0x92, 0x2a, 0x45, 0x7a, 0xa0, 0x3d, 0x9c, 0xbf, 0xf0, 0x8a, 0x7a, 0x4d, 0x6b, 0xd4, 0x28, 0x96, 0x9b},
	{ 0x62, 0xf2, 0x72, 0x5a, 0xa5, 0xfa, 0x2b, 0x71, 0x53, 0xb5, 0x39, 0xd4, 0x0d, 0x1f, 0x7c, 0x4b, 0xc8, 0xad, 0xa3, 0x5b, 0xe2, 0x48, 0xaa, 0x89, 0xce, 0x3d, 0xd7, 0xdc, 0xa7, 0x96, 0x15, 0x2a},
	{ 0x4c, 0xe1, 0x51, 0x03, 0xa1, 0x7f, 0x1e, 0x25, 0xf0, 0xb7, 0xbb, 0xb5, 0x30, 0xfb, 0xe3, 0x79, 0x4b, 0xad, 0xd4, 0x89, 0xd1, 0x64, 0xfa, 0x70, 0xb1, 0xeb, 0xb9, 0x02, 0xff, 0x4d, 0xd1, 0xd4},
	{ 0xc7, 0xbc, 0x75, 0x83, 0xd5, 0x25, 0x15, 0x2a, 0xd4, 0x2c, 0x56, 0xe8, 0xd6, 0x64, 0xdd, 0xee, 0x41, 0x76, 0x9d, 0x98, 0xf1, 0x35, 0x85, 0x13, 0x2e, 0x2c, 0x82, 0x66, 0xb9, 0x8b, 0x70, 0x89},
	{ 0xfe, 0xbc, 0x6b, 0x25, 0x6b, 0xd5, 0xc8, 0x17, 0x0b, 0x23, 0x54, 0xef, 0x65, 0x92, 0xd4, 0x13, 0x9f, 0xbc, 0x5d, 0x25, 0x9a, 0xcb, 0xdc, 0x4e, 0x0f, 0x89, 0x95, 0xc7, 0x69, 0x8a, 0x92, 0x8e},
	{ 0xe6, 0x31, 0x47, 0xae, 0xa4, 0x58, 0x1e, 0x6f, 0x45, 0x11, 0xea, 0x33, 0x54, 0x3d, 0xc2, 0x8d, 0xe1, 0x4f, 0x71, 0xce, 0x53, 0x56, 0x13, 0x88, 0x1a, 0xa3, 0x0f, 0xa5, 0xb4, 0xcc, 0x04, 0xa0},
	{ 0x0e, 0x4c, 0xbc, 0x71, 0xfa, 0xad, 0xb9, 0xe8, 0x52, 0x2c, 0x03, 0x69, 0x32, 0x72, 0x5a, 0x60, 0xd2, 0xe7, 0xc5, 0x0e, 0x64, 0x1a, 0x7f, 0xa9, 0x0c, 0x77, 0x50, 0xa3, 0x8c, 0xb0, 0x68, 0x47},
	{ 0x11, 0x68, 0x87, 0xd2, 0xcd, 0x9d, 0x59, 0xda, 0xc4, 0xb9, 0x8a, 0x98, 0xb8, 0x9a, 0xe8, 0xac, 0x13, 0x31, 0x58, 0xc3, 0xdd, 0xd3, 0x14, 0x03, 0xe7, 0xee, 0xf6, 0xb6, 0xd7, 0x8a, 0x2d, 0x4e},
	{ 0x96, 0xc2, 0xbb, 0x53, 0x76, 0x42, 0xd0, 0x66, 0x3c, 0x1e, 0x8b, 0xe0, 0x9d, 0xc7, 0x3c, 0x5b, 0x58, 0x26, 0x7f, 0xe1, 0xb9, 0x28, 0x8f, 0x3e, 0x64, 0x43, 0x52, 0x5e, 0x40, 0x47, 0xee, 0xa7},
	{ 0x7d, 0x4d, 0x85, 0x55, 0x17, 0x59, 0x04, 0xeb, 0x2e, 0xee, 0x40, 0x9f, 0x0a, 0xc9, 0x92, 0x1e, 0x8c, 0x46, 0xb4, 0x71, 0xe4, 0xe4, 0x37, 0x85, 0x34, 0xe0, 0x73, 0x30, 0x55, 0x9c, 0x28, 0x11},
	{ 0x80, 0xe6, 0x36, 0xc7, 0xc6, 0x95, 0xd9, 0x95, 0x6f, 0xdd, 0x5a, 0x2c, 0x72, 0x70, 0x83, 0x4f, 0x6d, 0xe7, 0x18, 0xe4, 0x9b, 0x3a, 0x68, 0x0d, 0x71, 0xb1, 0x90, 0x91, 0xe6, 0x9d, 0x24, 0x70},
	{ 0x07, 0x7a, 0xe2, 0xb3, 0xb2, 0xda, 0xe5, 0xcb, 0x7f, 0xdb, 0xde, 0xf8, 0xf4, 0x52, 0xb2, 0x73, 0xb6, 0x70, 0xd9, 0x08, 0x3c, 0x5c, 0x29, 0x08, 0xaa, 0xcd, 0x7a, 0xcf, 0x91, 0x20, 0x9b, 0x22},
	{ 0x67, 0x93, 0xfb, 0x57, 0x39, 0x76, 0x9b, 0x8c, 0xfa, 0x0a, 0x95, 0x4c, 0x70, 0x9a, 0x7c, 0x19, 0xa5, 0xb4, 0x6e, 0x24, 0x07, 0x2b, 0x13, 0x6e, 0xee, 0x85, 0xec, 0x76, 0x15, 0x1a, 0xae, 0x65},
	{ 0xaa, 0x56, 0xb5, 0xa4, 0x74, 0xd9, 0xca, 0x5a, 0x32, 0xa9, 0x6d, 0xbd, 0x83, 0x94, 0x37, 0x19, 0xbf, 0x41, 0x9a, 0x6d, 0x98, 0x65, 0x2f, 0xea, 0xd3, 0x72, 0x64, 0x10, 0x75, 0xd4, 0xa0, 0x29},
	{ 0xf5, 0x94, 0x86, 0xda, 0x94, 0xc1, 0x28, 0x4c, 0xb7, 0x16, 0x3b, 0xe8, 0x6d, 0x64, 0x95, 0x04, 0x51, 0xe2, 0x8c, 0xff, 0xba, 0x2b, 0x8c, 0x90, 0x5b, 0xd0, 0xaa, 0x3c, 0xbb, 0x57, 0x20, 0x77},
	{ 0x33, 0xf9, 0x68, 0xc6, 0x80, 0xed, 0x23, 0xe6, 0x20, 0x88, 0xd9, 0x0d, 0x2c, 0xd0, 0x64, 0xe2, 0x16, 0x4d, 0x9a, 0x4a, 0x09, 0x01, 0xcc, 0xe5, 0x5e, 0x19, 0x35, 0xce, 0x3d, 0xda, 0x5f, 0x7a},
	{ 0x25, 0x39, 0xf5, 0xcd, 0x32, 0xa2, 0x13, 0x82, 0x06, 0x77, 0x0e, 0x30, 0x47, 0x8b, 0xf9, 0xe2, 0x98, 0x77, 0x3d, 0xcb, 0x84, 0x43, 0x86, 0x2c, 0x66, 0xad, 0xf3, 0x3c, 0xbf, 0xc2, 0xf0, 0x43},
	{ 0xed, 0xc6, 0xc7, 0xff, 0x53, 0x3f, 0xd5, 0xf4, 0xe6, 0xd8, 0xcd, 0x99, 0x90, 0x94, 0x88, 0x26, 0x5b, 0xf4, 0x30, 0xc5, 0x0d, 0x94, 0xae, 0x6b, 0x74, 0xa6, 0x54, 0x03, 0x6d, 0xb6, 0xdc, 0xe6},
	{ 0xf8, 0xec, 0x5e, 0xfd, 0xa2, 0x76, 0xa9, 0xc2, 0x20, 0xb4, 0xb0, 0x8f, 0x70, 0xcf, 0x83, 0xe9, 0x36, 0x2c, 0x79, 0xa8, 0x8d, 0x0a, 0x45, 0x91, 0xb2, 0xeb, 0xf1, 0x14, 0x41, 0x11, 0x87, 0x20},
	{ 0xfa, 0x63, 0x8f, 0x3d, 0x18, 0xf9, 0x68, 0x68, 0x6d, 0x8d, 0xd4, 0x6b, 0xa9, 0x79, 0x84, 0xd9, 0x81, 0x1d, 0xe5, 0x32, 0x81, 0x07, 0x21, 0x2f, 0xa9, 0xb7, 0x55, 0xf9, 0xe0, 0x2a, 0x75, 0x8f},
	{ 0x40, 0x93, 0xcf, 0xf0, 0xeb, 0xfe, 0xde, 0xca, 0x7f, 0x35, 0xd1, 0x8e, 0xbb, 0x22, 0x55, 0x42, 0xd6, 0xe6, 0xff, 0xc4, 0x8b, 0x21, 0x2f, 0x47, 0xd7, 0x46, 0x2a, 0xe5, 0x9f, 0xec, 0x5d, 0x70},
	{ 0x7a, 0x31, 0xf6, 0xc7, 0x69, 0xd7, 0x4c, 0x6a, 0xa3, 0x9d, 0x27, 0xa7, 0x23, 0x5f, 0xbb, 0xc4, 0x82, 0x41, 0xb7, 0x32, 0x9a, 0xa4, 0xc0, 0x52, 0x80, 0xbf, 0x07, 0x6e, 0x91, 0xe3, 0x8b, 0x24},
	{ 0xdc, 0x2c, 0x06, 0x1b, 0x1f, 0xb2, 0x24, 0xe9, 0x3a, 0x3f, 0xc3, 0x29, 0x1a, 0x72, 0xa0, 0xc1, 0xa2, 0x57, 0xf7, 0x42, 0xa4, 0xb0, 0x3f, 0x65, 0xa8, 0x88, 0xe0, 0x81, 0x75, 0x53, 0x19, 0xba},
	{ 0x63, 0x99, 0x85, 0x13, 0x76, 0x67, 0x3a, 0x08, 0x99, 0xe5, 0x25, 0x0e, 0x71, 0xbc, 0xab, 0x30, 0xe8, 0x78, 0x65, 0x8b, 0x4c, 0xd8, 0x63, 0xa4, 0x98, 0x4d, 0xe6, 0x00, 0x74, 0x78, 0x10, 0x34},
	{ 0xf8, 0xe9, 0x17, 0x37, 0x9d, 0x5a, 0x4d, 0xab, 0x0f, 0xfe, 0x3b, 0x47, 0xb9, 0x06, 0x40, 0xd7, 0x32, 0x97, 0xd9, 0xfd, 0x22, 0xf7, 0x7e, 0xd6, 0xd7, 0x58, 0x7e, 0x68, 0x06, 0x51, 0x58, 0xb0},
	{ 0xa0, 0x96, 0x6c, 0x5e, 0xf9, 0x49, 0x5a, 0x23, 0x1b, 0xed, 0x46, 0x90, 0x57, 0x8a, 0x9e, 0x36, 0xa4, 0xb0, 0xe5, 0x32, 0x44, 0xc2, 0xf0, 0x7e, 0x41, 0xa9, 0xc2, 0x67, 0x7a, 0x3c, 0x1b, 0xc0},
	{ 0xfc, 0x5f, 0x84, 0x4d, 0x2e, 0x24, 0xdc, 0x6c, 0xa9, 0x27, 0x5c, 0x90, 0x49, 0xa3, 0x50, 0x05, 0xb9, 0xc9, 0x4f, 0x47, 0x81, 0xe2, 0xb5, 0xec, 0xb2, 0x93, 0xdc, 0x02, 0x13, 0xe3, 0x27, 0x23},
	{ 0x21, 0x27, 0xd3, 0xc0, 0xb1, 0x41, 0x21, 0x7e, 0x2f, 0xf4, 0xe9, 0x56, 0xd6, 0x9f, 0x9f, 0x52, 0x1d, 0xee, 0xcb, 0x66, 0xb6, 0xaf, 0x20, 0x9b, 0xa2, 0xd3, 0x93, 0xcd, 0xb2, 0xa6, 0x9a, 0x7c},
	{ 0xeb, 0xdc, 0x5f, 0x26, 0xa1, 0xea, 0x0a, 0x13, 0x13, 0x11, 0xd0, 0x52, 0xdc, 0x7e, 0xa2, 0x56, 0x96, 0xa6, 0x6d, 0x1e, 0xaa, 0x01, 0x78, 0xca, 0x51, 0x9b, 0xdd, 0x14, 0x68, 0x3e, 0x5a, 0x88},
	{ 0x7c, 0x52, 0x35, 0x9e, 0x9e, 0x7f, 0xe3, 0x98, 0x2c, 0xe9, 0xe5, 0xe0, 0xf0, 0x08, 0x7c, 0x69, 0x09, 0xc6, 0xab, 0x87, 0xca, 0xa7, 0x8c, 0x49, 0xcd, 0x43, 0xc9, 0xa3, 0xd8, 0x06, 0xe8, 0xfd},
	{ 0x9c, 0xed, 0x92, 0x19, 0x0d, 0xbe, 0xc4, 0x56, 0x99, 0xb3, 0x7c, 0x18, 0x27, 0xcf, 0x9b, 0x67, 0xd4, 0x93, 0xe5, 0xf7, 0x25, 0x8f, 0xca, 0x15, 0xdb, 0xa1, 0x19, 0xaa, 0x70, 0xa2, 0xc4, 0x7f},
	{ 0x9f, 0xcc, 0x90, 0x04, 0xa5, 0xf8, 0x44, 0x5a, 0xca, 0xc7, 0x39, 0x07, 0xad, 0xea, 0x39, 0x98, 0x0c, 0x59, 0x6e, 0xf2, 0x0b, 0x41, 0xd2, 0x2c, 0x68, 0x4b, 0xba, 0x29, 0x2a, 0xfd, 0x43, 0x3f},
	{ 0x37, 0x14, 0x80, 0x6b, 0xb9, 0x55, 0xa3, 0x86, 0x91, 0xa7, 0x4a, 0x22, 0x52, 0xdb, 0x14, 0x87, 0x4d, 0xca, 0x8a, 0xa9, 0x6a, 0xfb, 0x94, 0x07, 0x4a, 0x0b, 0xc2, 0x4d, 0x8f, 0x26, 0xec, 0x30},
	{ 0xa6, 0xb8, 0xb9, 0xee, 0x88, 0x81, 0xbc, 0x3a, 0x9e, 0xa3, 0xd7, 0xde, 0x1a, 0xf0, 0x60, 0x41, 0xea, 0xa8, 0xde, 0x4d, 0xef, 0x83, 0x81, 0x08, 0x5f, 0xb7, 0x11, 0x36, 0xc9, 0xfc, 0xd5, 0x63},
	{ 0xd6, 0xdd, 0xa8, 0xe4, 0x92, 0x7b, 0x41, 0x15, 0xf5, 0x67, 0x6c, 0x66, 0x3a, 0xa8, 0x08, 0xaa, 0x74, 0xa3, 0x6a, 0x93, 0x08, 0x52, 0xb2, 0x0c, 0xb6, 0xf8, 0xca, 0x3a, 0x66, 0x48, 0xaf, 0xc2},
	{ 0x57, 0x7a, 0x04, 0xd4, 0x8c, 0x30, 0x67, 0x08, 0x28, 0xad, 0x74, 0xa9, 0xea, 0xe5, 0x4a, 0x97, 0xa5, 0x9d, 0xf9, 0x0c, 0x71, 0xb7, 0x12, 0x29, 0x94, 0xc1, 0xb1, 0x56, 0x28, 0x5c, 0xde, 0x77},
	{ 0x61, 0xef, 0x47, 0xd8, 0xe0, 0x2d, 0x95, 0x5a, 0x7c, 0xc3, 0xa1, 0xd1, 0x0e, 0x58, 0x91, 0xcf, 0xea, 0x84, 0xf8, 0xb0, 0x6e, 0x37, 0xa6, 0x93, 0x9e, 0x14, 0x99, 0x6f, 0x6d, 0xe7, 0xb5, 0xb0},
	{ 0xd9, 0xf9, 0x1f, 0x39, 0x0b, 0x7f, 0x63, 0x1b, 0xa1, 0xe3, 0x58, 0x20, 0xf3, 0x05, 0x31, 0x2e, 0x3f, 0x37, 0x32, 0xb2, 0x2d, 0x7a, 0x9d, 0xa5, 0x04, 0x36, 0x82, 0x41, 0x85, 0xd5, 0x0e, 0x73},
	{ 0x3c, 0x00, 0xfe, 0xae, 0xe7, 0x66, 0x22, 0x1d, 0x51, 0x7b, 0xcd, 0x07, 0x85, 0xb1, 0xd1, 0x47, 0x9c, 0xbb, 0x70, 0x73, 0x80, 0xb1, 0x38, 0x4f, 0xd1, 0x78, 0x08, 0x95, 0x5c, 0x0c, 0x45, 0x65},
	{ 0x72, 0x12, 0xfe, 0x99, 0x41, 0x8d, 0x8f, 0x96, 0x9f, 0xc3, 0x83, 0x08, 0x0c, 0xe7, 0xec, 0xe8, 0xd5, 0x01, 0x01, 0xe9, 0x55, 0x1f, 0xfe, 0xa8, 0x64, 0x3b, 0x3e, 0x17, 0xa4, 0xaa, 0x6e, 0x2d},
	{ 0xf3, 0x00, 0xf6, 0xd5, 0x9d, 0x62, 0x5f, 0xc1, 0x1d, 0xb5, 0xdb, 0xcb, 0x7b, 0x2e, 0x8f, 0xd7, 0x92, 0xf6, 0x57, 0x16, 0xa4, 0x39, 0xfa, 0x2f, 0x3d, 0xe6, 0x5b, 0x31, 0xd7, 0x43, 0x7e, 0x32},
	{ 0xcf, 0x92, 0x1f, 0x55, 0xda, 0x04, 0xa1, 0x5d, 0xef, 0xae, 0x15, 0x94, 0xa9, 0xe9, 0xe7, 0x08, 0x05, 0x06, 0xe2, 0xfd, 0xed, 0xbd, 0xeb, 0x73, 0xf3, 0x61, 0x62, 0x84, 0x7a, 0x38, 0x51, 0x73},
	{ 0x27, 0x7c, 0x7d, 0xe2, 0x0e, 0xe1, 0x37, 0x1e, 0x14, 0xd5, 0xcb, 0x54, 0x43, 0xe5, 0x8d, 0x67, 0xfd, 0x6e, 0x51, 0xe2, 0x75, 0xc7, 0xa0, 0x24, 0x39, 0x36, 0xf6, 0xd9, 0x3f, 0x44, 0x5c, 0x33},
	{ 0x76, 0xfe, 0x6b, 0xf5, 0xc2, 0xc2, 0xe1, 0x01, 0x95, 0xc8, 0x7e, 0x95, 0x0d, 0x02, 0xcd, 0x02, 0xc1, 0x6c, 0x30, 0xd2, 0xb9, 0x8e, 0xec, 0xcb, 0x81, 0x59, 0x4d, 0x6b, 0xe2, 0x30, 0x35, 0x8f},
	{ 0x05, 0x09, 0x2b, 0x65, 0xb2, 0xaf, 0x19, 0x7c, 0x65, 0x4f, 0xb9, 0x5d, 0x50, 0xa9, 0xb6, 0xbd, 0xd0, 0x52, 0xc6, 0x97, 0xc7, 0xc3, 0x87, 0x01, 0xde, 0xd8, 0x6e, 0x85, 0xfc, 0x66, 0xe7, 0xb8},
	{ 0xe1, 0x3a, 0x47, 0x30, 0xd6, 0x4b, 0xd8, 0xc6, 0x55, 0xc0, 0x76, 0x23, 0x8f, 0x90, 0x86, 0x4c, 0xb6, 0xed, 0xd4, 0x80, 0x8f, 0xf4, 0xb1, 0x5f, 0xfe, 0x8e, 0xdd, 0x83, 0xd4, 0xb9, 0x25, 0xbc},
	{ 0x1e, 0x35, 0xa2, 0xc5, 0xc8, 0xc3, 0x47, 0xbb, 0xdd, 0x0a, 0x9a, 0x61, 0x04, 0x99, 0x86, 0x70, 0x81, 0x71, 0xbd, 0x7d, 0x21, 0x1f, 0xc9, 0xcc, 0x1e, 0xff, 0xd4, 0xf8, 0x1c, 0x44, 0xf6, 0xd5},
	{ 0x82, 0xa1, 0xe3, 0x20, 0xd0, 0xef, 0x61, 0xaf, 0x31, 0x21, 0x52, 0x01, 0x2b, 0xd3, 0x9a, 0xb4, 0xf3, 0x3b, 0xa5, 0xc6, 0x28, 0x85, 0x53, 0x95, 0xf1, 0x5e, 0xb6, 0x9d, 0x95, 0xc2, 0x61, 0xcd},
	{ 0x34, 0x01, 0xfd, 0xaf, 0x09, 0x47, 0x39, 0xdb, 0x94, 0x8d, 0xe1, 0x14, 0xda, 0x6a, 0x4e, 0xed, 0x21, 0x79, 0xa2, 0x34, 0xf8, 0x0b, 0x18, 0x3e, 0x8b, 0x46, 0x66, 0xba, 0x2a, 0x75, 0x18, 0x3c},
	{ 0x1a, 0xf6, 0xd3, 0x4e, 0x20, 0x31, 0x86, 0xa9, 0x4c, 0x55, 0x38, 0x6d, 0x02, 0xb3, 0x43, 0xd4, 0x99, 0x67, 0xe9, 0x9a, 0x0e, 0x3c, 0x04, 0xd5, 0x59, 0x5f, 0xd3, 0x01, 0x9a, 0x87, 0x9c, 0xe1}
};

#endif // RSG_ORBIS
