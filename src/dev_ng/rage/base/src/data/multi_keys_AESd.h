#ifndef _MULTI_KEY_AESD_H_
#define _MULTI_KEY_AESD_H_

#if RSG_DURANGO  || __RESOURCECOMPILER || RSG_TOOL
#ifndef _INTPTR
#define _INTPTR 1
#endif
#include <stdio.h>
#ifdef __cplusplus
extern "C" { 
#endif


extern unsigned char unique_multikey_gta5_xboxone[101][32];

#ifdef __cplusplus
}
#endif

#endif // RSG_DURANGO

#endif //_MULTI_KEY_AESD_H_

