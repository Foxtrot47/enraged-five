#if RSG_DURANGO || __RESOURCECOMPILER || RSG_TOOL

#ifndef _INTPTR
#define _INTPTR 1
#endif

unsigned char unique_multikey_gta5_xboxone[101][32] = {
	{ 0x45, 0xed, 0x70, 0x39, 0xe7, 0xa6, 0xdf, 0x25, 0xb6, 0xb5, 0xc6, 0x5f, 0x4b, 0xe9, 0xec, 0x0b, 0xd0, 0x0d, 0x87, 0xe2, 0xa3, 0xb3, 0xbc, 0x49, 0xfe, 0x3c, 0xc7, 0x1d, 0x27, 0x61, 0x61, 0xa7},
	{ 0x29, 0x25, 0xc2, 0x83, 0x5d, 0x53, 0x6c, 0x6b, 0x10, 0xbd, 0x2d, 0xb1, 0x20, 0x7a, 0x5e, 0x68, 0x83, 0xf2, 0xdc, 0x15, 0x13, 0xd5, 0x9d, 0x0d, 0x0a, 0x56, 0x64, 0x8c, 0xc4, 0x48, 0x40, 0xfe},
	{ 0x90, 0x3e, 0x87, 0x46, 0xf8, 0x07, 0x70, 0x9f, 0x89, 0xbe, 0xd3, 0x64, 0x34, 0x37, 0xff, 0x87, 0x4e, 0x7e, 0x89, 0xea, 0x8c, 0xfe, 0x02, 0x92, 0xab, 0xe8, 0xcf, 0xae, 0xf0, 0x3f, 0x3b, 0x92},
	{ 0xc1, 0x93, 0xfb, 0x4f, 0xd4, 0xb9, 0x14, 0x79, 0xea, 0x2d, 0x63, 0xa8, 0x3e, 0x54, 0x18, 0x5a, 0xde, 0x10, 0x12, 0x2c, 0xc1, 0xee, 0x0a, 0xf4, 0x1d, 0x2c, 0xb4, 0x01, 0x7f, 0xa2, 0x98, 0x04},
	{ 0x60, 0xca, 0x10, 0x17, 0xfc, 0x2c, 0xd6, 0xca, 0x14, 0x71, 0x95, 0x43, 0xd7, 0x24, 0xe8, 0xb7, 0xc1, 0x5e, 0xc4, 0xb2, 0x68, 0x54, 0x2f, 0x8f, 0xc3, 0x45, 0x03, 0x4c, 0x66, 0x09, 0x64, 0x90},
	{ 0xd7, 0xbc, 0x48, 0x05, 0xa0, 0xb3, 0xfa, 0xbe, 0xbc, 0x5f, 0x49, 0x69, 0x8f, 0xd8, 0x1b, 0x29, 0x7d, 0xd5, 0x4f, 0x49, 0xd2, 0x24, 0x65, 0xf6, 0xcd, 0x0a, 0x48, 0x98, 0x0f, 0xe6, 0x80, 0xb5,},
	{ 0x0a, 0x01, 0x07, 0x09, 0x70, 0xa6, 0x65, 0x39, 0x8a, 0x12, 0xa5, 0x46, 0x54, 0x0f, 0x96, 0xec, 0xc1, 0x31, 0xd9, 0xc2, 0x63, 0x3d, 0x61, 0x29, 0x69, 0x7c, 0xc9, 0x21, 0xcb, 0x73, 0x1a, 0x14,},
	{ 0xee, 0xbe, 0xba, 0x81, 0x2c, 0xa5, 0x4d, 0x57, 0xff, 0x5e, 0x17, 0x12, 0x82, 0x36, 0x29, 0x01, 0xa6, 0x34, 0xb4, 0xeb, 0xaa, 0x08, 0xfc, 0xcc, 0xe1, 0xf2, 0x36, 0xf0, 0x35, 0x96, 0x38, 0x9e,},
	{ 0x97, 0x74, 0x36, 0x9d, 0x84, 0xff, 0x0a, 0x11, 0xc2, 0x4a, 0x77, 0x84, 0x63, 0xba, 0xe5, 0x64, 0x6f, 0x48, 0xad, 0x4e, 0xda, 0x31, 0x53, 0xb4, 0x38, 0x99, 0xa2, 0xfd, 0xf8, 0xb1, 0x3b, 0xad,},
	{ 0xba, 0xd8, 0xe0, 0xf6, 0xc9, 0xf7, 0x04, 0xc7, 0x78, 0x99, 0xff, 0xd4, 0x64, 0x88, 0x60, 0x67, 0x3c, 0x85, 0x1d, 0x67, 0x38, 0xec, 0xf3, 0x01, 0xaf, 0x73, 0xd1, 0xc7, 0xfc, 0x02, 0xe2, 0x5a,},
	{ 0xba, 0xd8, 0xe0, 0xf6, 0xc9, 0xf7, 0x04, 0xc7, 0x78, 0x99, 0xff, 0xd4, 0x64, 0x88, 0x60, 0x67, 0x3c, 0x85, 0x1d, 0x67, 0x38, 0xec, 0xf3, 0x01, 0xaf, 0x73, 0xd1, 0xc7, 0xfc, 0x02, 0xe2, 0x5a,},
	{ 0x60, 0x93, 0x68, 0xe9, 0x85, 0x9f, 0xe1, 0x43, 0x36, 0xa0, 0x77, 0xe7, 0x04, 0xf7, 0x8e, 0x21, 0x0f, 0x28, 0x50, 0x77, 0x0f, 0x33, 0x7e, 0xda, 0x40, 0x27, 0xa5, 0x5e, 0xcb, 0x04, 0xcd, 0xf2,},
	{ 0x40, 0xae, 0xa1, 0xe8, 0x84, 0x06, 0x85, 0xb8, 0xb0, 0xeb, 0xbc, 0xed, 0xb4, 0xaa, 0x15, 0x6b, 0x77, 0xb2, 0x42, 0x6c, 0x93, 0xf2, 0x5b, 0xe6, 0xd2, 0x9f, 0xaf, 0x7c, 0xfd, 0x52, 0x1b, 0x60,},
	{ 0xdf, 0xad, 0x88, 0xf2, 0x0c, 0xe1, 0x2f, 0x70, 0xf7, 0x83, 0xf1, 0x7b, 0x83, 0x4e, 0xfa, 0xba, 0xac, 0x1c, 0xb9, 0x1b, 0xea, 0x33, 0x4d, 0x21, 0x3c, 0x10, 0xbb, 0x4d, 0xbd, 0xc5, 0xac, 0x3c,},
	{ 0xce, 0xf0, 0xc5, 0x7a, 0x0d, 0x49, 0x33, 0xc1, 0x11, 0xb6, 0x04, 0xa5, 0x38, 0x24, 0x66, 0x73, 0x54, 0xcc, 0x7a, 0x27, 0x67, 0x4f, 0x49, 0xd7, 0x35, 0xe4, 0xcd, 0x51, 0x67, 0xe4, 0xa3, 0xfa},
	{ 0x43, 0x20, 0x57, 0x91, 0xe5, 0x2f, 0x01, 0xbd, 0xe8, 0x4b, 0x5a, 0x7a, 0x8c, 0x44, 0x95, 0xd1, 0x2f, 0x46, 0x48, 0x49, 0xfc, 0x03, 0x3e, 0x5a, 0x96, 0x24, 0x29, 0x02, 0x82, 0xb2, 0x30, 0x5b},
	{ 0xe9, 0xc1, 0x82, 0x7f, 0x2d, 0x52, 0x35, 0x91, 0xf6, 0x71, 0x2e, 0x9b, 0xbd, 0x32, 0x42, 0x5e, 0x59, 0xe9, 0x0c, 0x43, 0x6f, 0x45, 0x6d, 0x76, 0x29, 0xc2, 0xe2, 0xe9, 0xfd, 0xe3, 0xc7, 0xed},
	{ 0x74, 0xe8, 0x3d, 0x94, 0x20, 0xec, 0x60, 0xef, 0xff, 0x00, 0xe8, 0x54, 0xb9, 0x1a, 0xea, 0x59, 0x46, 0x6b, 0x50, 0x28, 0xb0, 0xcc, 0x5e, 0x67, 0x87, 0xda, 0x4a, 0xac, 0x34, 0x69, 0x0f, 0xb0},
	{ 0x8b, 0x92, 0xdf, 0x11, 0xa2, 0xd6, 0xe0, 0x46, 0xdc, 0x53, 0x29, 0xf5, 0xf5, 0xad, 0x1d, 0xc2, 0xa7, 0x39, 0xb0, 0x4c, 0x02, 0x2f, 0x30, 0xa9, 0x7e, 0x47, 0x8c, 0xe0, 0x33, 0x37, 0xd2, 0x3f},
	{ 0x0d, 0xc8, 0xee, 0x25, 0x24, 0xd8, 0x3c, 0xd2, 0xb1, 0x6e, 0xf4, 0xa7, 0xfb, 0xd7, 0x31, 0xfd, 0x75, 0xcc, 0x8a, 0x8b, 0xd7, 0x08, 0xc4, 0xb6, 0x66, 0x67, 0xa2, 0x0d, 0x63, 0xcd, 0x82, 0xcc},
	{ 0x8a, 0xff, 0xb3, 0x3c, 0x48, 0x64, 0x89, 0xa1, 0xa9, 0xae, 0x98, 0xf2, 0xb7, 0xae, 0x66, 0x34, 0x49, 0xfb, 0x60, 0x0c, 0x8d, 0x96, 0xb9, 0xbd, 0x88, 0x60, 0xa0, 0x86, 0xe1, 0x5e, 0x4f, 0x11},
	{ 0xf5, 0x0c, 0xe5, 0x11, 0x11, 0xe0, 0x43, 0xaa, 0xbc, 0x67, 0x61, 0x8e, 0x07, 0x8b, 0xf5, 0x53, 0x7d, 0xc0, 0xfc, 0xda, 0x3b, 0x12, 0xab, 0x6f, 0x6b, 0x54, 0xb6, 0x96, 0x98, 0x84, 0x8d, 0xe5},
	{ 0x96, 0x69, 0xce, 0xee, 0x5d, 0xc5, 0xeb, 0x22, 0xca, 0xe6, 0x68, 0xf9, 0xad, 0xdc, 0x58, 0x5e, 0xc7, 0x91, 0xd5, 0x17, 0x0e, 0xa6, 0x5b, 0x29, 0xe3, 0xda, 0x90, 0x2f, 0xd2, 0x81, 0x61, 0xaa},
	{ 0x74, 0xb2, 0xa1, 0x15, 0x07, 0x8b, 0x13, 0x90, 0x4f, 0xda, 0x69, 0x6f, 0x65, 0xe0, 0x5e, 0xe6, 0x2d, 0xfc, 0x65, 0x7c, 0x22, 0x24, 0x89, 0xbe, 0x9e, 0x34, 0x15, 0x5b, 0xfe, 0xd3, 0x00, 0x91},
	{ 0x08, 0xa1, 0x24, 0x3d, 0xce, 0xc1, 0x91, 0xc9, 0x98, 0x29, 0xf1, 0x3f, 0x1c, 0x3d, 0xc4, 0x06, 0x64, 0xbc, 0xd4, 0xd8, 0x6b, 0xa6, 0xda, 0xc7, 0xa2, 0x2c, 0xc2, 0xca, 0x5f, 0x16, 0x35, 0xb8},
	{ 0x2f, 0x02, 0x84, 0x10, 0x4d, 0x1d, 0xa8, 0x14, 0x20, 0xaa, 0x2f, 0xbc, 0x29, 0x47, 0x3d, 0xf4, 0xad, 0xdb, 0x2c, 0x60, 0x11, 0x9a, 0x2a, 0x6b, 0xba, 0x0a, 0xcf, 0x22, 0x82, 0x69, 0x9b, 0x5e},
	{ 0x6f, 0x45, 0x9d, 0x75, 0x4c, 0x38, 0x22, 0x03, 0x5b, 0x7c, 0x90, 0xc2, 0xc2, 0x0e, 0xda, 0xe3, 0xce, 0xb8, 0x0f, 0x30, 0xe2, 0xb0, 0xb3, 0xc0, 0x58, 0x46, 0xcc, 0x64, 0xe7, 0xe2, 0x39, 0xf9},
	{ 0x82, 0x5d, 0x5c, 0xc7, 0x80, 0xc5, 0x4c, 0xcc, 0x6a, 0x59, 0xa9, 0xcb, 0x97, 0xaf, 0xe8, 0xc6, 0xd0, 0xe3, 0x11, 0x8a, 0x6f, 0x80, 0x49, 0x07, 0x71, 0x9b, 0x1f, 0xa1, 0x2b, 0x54, 0x90, 0xcc},
	{ 0x74, 0x2e, 0x8b, 0x33, 0x91, 0x34, 0x5a, 0x0e, 0x3b, 0xb8, 0x79, 0x62, 0xbf, 0xf6, 0xec, 0xf8, 0x69, 0x98, 0xfc, 0x79, 0x2d, 0xca, 0xc7, 0xa3, 0xa9, 0x42, 0x85, 0x83, 0xdc, 0xc0, 0x51, 0x7c},
	{ 0xb5, 0xb4, 0x6f, 0xfc, 0x8e, 0x81, 0x38, 0x2a, 0xbd, 0x1d, 0x3f, 0x5b, 0x96, 0x1f, 0xb5, 0x33, 0x94, 0xae, 0x27, 0x58, 0xf9, 0x7e, 0xe8, 0x1c, 0xc5, 0xb0, 0x88, 0x8d, 0xb7, 0x60, 0xd3, 0x46},
	{ 0x56, 0x33, 0x18, 0x43, 0xf7, 0x84, 0x1d, 0x76, 0x21, 0x14, 0x90, 0xb2, 0x07, 0xc7, 0x6c, 0xd3, 0x9d, 0x0b, 0xd3, 0x97, 0x6d, 0xbe, 0x5f, 0xa0, 0xca, 0x5f, 0x81, 0x19, 0x83, 0x65, 0x9d, 0x75},
	{ 0xd1, 0xf2, 0xe9, 0x08, 0xc5, 0x01, 0xc2, 0x4b, 0x4a, 0x0d, 0xef, 0x3b, 0x16, 0xd1, 0x71, 0x78, 0xe4, 0x00, 0x4e, 0x18, 0xa3, 0x92, 0x06, 0x46, 0xfb, 0x98, 0xde, 0x5a, 0x05, 0x36, 0x01, 0x54},
	{ 0x3f, 0xaa, 0xbf, 0x90, 0x7e, 0x91, 0xca, 0xfa, 0xaa, 0x09, 0xb4, 0xf8, 0x55, 0xda, 0x67, 0x1f, 0xcc, 0x20, 0x7d, 0x64, 0x6e, 0x96, 0xe3, 0xfd, 0xed, 0x61, 0x41, 0xe3, 0x6f, 0xa9, 0xd8, 0x36},
	{ 0x8b, 0xb5, 0xfb, 0x7c, 0xc5, 0xb0, 0x00, 0xd5, 0x9c, 0x34, 0x83, 0x50, 0x64, 0x07, 0x9f, 0xd0, 0x8d, 0xc0, 0x21, 0xbc, 0x38, 0x2e, 0xfd, 0xcc, 0xd9, 0xfe, 0xe2, 0x3e, 0x0a, 0x31, 0xca, 0x50},
	{ 0xe1, 0x30, 0x3a, 0x31, 0x40, 0x30, 0xed, 0x72, 0x48, 0x8d, 0x6e, 0x45, 0x21, 0xfa, 0x87, 0x6b, 0x70, 0x63, 0x53, 0x00, 0x5f, 0xb6, 0x5a, 0x5d, 0xa2, 0x3a, 0x8a, 0x60, 0x5d, 0xa6, 0x38, 0xa4},
	{ 0x3a, 0x34, 0x05, 0xd1, 0x49, 0x0f, 0x10, 0xac, 0x66, 0x06, 0xe5, 0xe4, 0x5d, 0xca, 0xc9, 0xc4, 0xeb, 0xb6, 0x1c, 0xdd, 0xc5, 0x39, 0x5e, 0x43, 0x1a, 0xfc, 0xdc, 0xe8, 0xa5, 0x0c, 0x3d, 0x56},
	{ 0xc7, 0xcb, 0x26, 0x3a, 0xe9, 0x6b, 0x6e, 0xb2, 0xb1, 0xad, 0x8e, 0x80, 0x8a, 0x6c, 0xff, 0x99, 0x51, 0x89, 0x24, 0xfe, 0x77, 0x84, 0x38, 0x9a, 0x19, 0xa1, 0x27, 0xa4, 0x6b, 0xe0, 0x31, 0xd3},
	{ 0x0a, 0xf4, 0x2a, 0xca, 0x01, 0x31, 0x4a, 0x78, 0x20, 0xcf, 0x07, 0x18, 0x6b, 0xc5, 0xc7, 0x44, 0x6d, 0xed, 0x0e, 0x5b, 0xe6, 0xee, 0x7e, 0x35, 0x17, 0x1b, 0xad, 0xce, 0x79, 0xb2, 0x4f, 0xf6},
	{ 0x47, 0x9b, 0x71, 0x25, 0xc4, 0xee, 0xb7, 0xa9, 0x3d, 0xe4, 0xf8, 0x73, 0x79, 0xac, 0x99, 0xf8, 0xa9, 0x53, 0x39, 0x8c, 0xa9, 0x3b, 0xc9, 0x79, 0xa3, 0x33, 0xb1, 0x0b, 0x97, 0xe2, 0x52, 0xcb},
	{ 0x13, 0x1f, 0x4d, 0xb8, 0xbc, 0x4a, 0xea, 0xc9, 0x63, 0x45, 0xc4, 0x09, 0x02, 0x1d, 0x35, 0x70, 0xc3, 0x6c, 0xca, 0x04, 0x3b, 0xd6, 0x63, 0xba, 0xf3, 0x24, 0x62, 0xd3, 0x14, 0x71, 0x55, 0xb8},
	{ 0x44, 0xbc, 0x5a, 0x8c, 0x40, 0x27, 0xb6, 0x45, 0x67, 0x14, 0xba, 0xd1, 0x6e, 0xc4, 0x1c, 0x18, 0x74, 0xd9, 0x47, 0x8d, 0xaf, 0x09, 0x24, 0x5a, 0x1f, 0xc6, 0x5d, 0x05, 0x38, 0x61, 0x98, 0x9a},
	{ 0xc8, 0x46, 0x31, 0x25, 0xbf, 0x22, 0xea, 0xd5, 0x63, 0x77, 0x06, 0xd9, 0xc7, 0x16, 0xf5, 0xfa, 0x82, 0x36, 0x0c, 0xd2, 0xc7, 0x72, 0x81, 0x8f, 0x9b, 0xca, 0xaf, 0xe4, 0x76, 0xf8, 0x46, 0xfb},
	{ 0x29, 0x50, 0xf3, 0x11, 0xa5, 0x5c, 0xe2, 0xa0, 0x5e, 0x7e, 0x23, 0xac, 0x27, 0x8a, 0x56, 0xe8, 0x5b, 0x00, 0xa4, 0x44, 0x48, 0x55, 0x72, 0x7a, 0xa0, 0x07, 0x6a, 0x5f, 0x0c, 0x10, 0xbe, 0xd4},
	{ 0xcf, 0xd9, 0x43, 0x2f, 0x43, 0x3c, 0x1b, 0x83, 0xc1, 0xa4, 0x79, 0x4d, 0x24, 0xc5, 0xd6, 0x7a, 0x16, 0xd9, 0xfb, 0xe6, 0xf1, 0xf1, 0xba, 0x7d, 0x4b, 0x09, 0x6f, 0x2e, 0x24, 0x2a, 0x33, 0x5d},
	{ 0x5c, 0xd6, 0x15, 0x77, 0xd4, 0x74, 0xb9, 0x9c, 0xac, 0x82, 0x0d, 0x85, 0x46, 0xb6, 0xcc, 0x04, 0x3f, 0x72, 0x7e, 0xd9, 0x57, 0xda, 0xb3, 0xbf, 0x5d, 0x88, 0x47, 0xbd, 0xfd, 0xee, 0xf8, 0x48},
	{ 0x83, 0x76, 0xe6, 0xcb, 0x01, 0xc7, 0x2c, 0x1e, 0x49, 0xc3, 0x0e, 0xf4, 0x9a, 0x3f, 0x20, 0xf5, 0xac, 0x98, 0x8e, 0xcb, 0xee, 0x99, 0xdd, 0x03, 0x9f, 0xd0, 0xcb, 0x14, 0xd3, 0x9a, 0x54, 0x60},
	{ 0x5a, 0xa4, 0x50, 0xf8, 0x4c, 0x75, 0xeb, 0x2c, 0x20, 0x3c, 0xc4, 0x7f, 0x74, 0x7f, 0x9e, 0x41, 0xba, 0xc7, 0x55, 0x7c, 0x40, 0xcf, 0xd7, 0x86, 0xd6, 0xb1, 0xca, 0x6b, 0x23, 0xf0, 0xb0, 0x88},
	{ 0xb9, 0xb5, 0xd3, 0x20, 0xf0, 0x84, 0xf7, 0x91, 0x24, 0x2c, 0x26, 0xaa, 0xef, 0x87, 0xb5, 0x99, 0xc9, 0x79, 0x46, 0x07, 0xa0, 0x60, 0x00, 0xdf, 0x25, 0x93, 0xd7, 0x7e, 0x05, 0xab, 0xc7, 0x4a},
	{ 0x7e, 0x81, 0x44, 0x9a, 0x8b, 0x81, 0xf4, 0xf7, 0x52, 0x62, 0x74, 0xfe, 0x09, 0x0c, 0x48, 0xc1, 0xbb, 0xb3, 0xb5, 0xc3, 0xc0, 0xcb, 0x28, 0xa8, 0xb1, 0x6e, 0xc6, 0xba, 0x56, 0x98, 0xc7, 0xca},
	{ 0x3e, 0xf2, 0xe6, 0x24, 0xbf, 0x6e, 0x52, 0xf6, 0x74, 0x8b, 0x31, 0x5d, 0xc1, 0x1b, 0x50, 0x51, 0x5b, 0x53, 0x23, 0x5a, 0x79, 0x19, 0x59, 0xa2, 0xb4, 0xc2, 0xf0, 0xff, 0x43, 0x05, 0x67, 0x0b},
	{ 0x8f, 0xa4, 0xbd, 0xf4, 0x82, 0x45, 0xe1, 0xef, 0x61, 0x09, 0xcf, 0x95, 0xb2, 0x71, 0x15, 0xda, 0xdb, 0x14, 0xab, 0x0f, 0x20, 0x56, 0xe1, 0x34, 0x94, 0xdf, 0x5f, 0xfc, 0xd8, 0x66, 0xce, 0x1b},
	{ 0xf5, 0x12, 0x8f, 0x8a, 0xa9, 0x79, 0x20, 0xc8, 0xb3, 0x2a, 0x41, 0xc5, 0x57, 0x97, 0xf0, 0x71, 0x61, 0xf4, 0x67, 0x4a, 0x84, 0x59, 0x99, 0x68, 0x66, 0x91, 0x30, 0x70, 0x21, 0x01, 0xaa, 0x12},
	{ 0x8e, 0x98, 0xcd, 0x99, 0x11, 0x92, 0x79, 0xb3, 0x91, 0x8e, 0xdb, 0xb2, 0xfb, 0xd4, 0x85, 0x1d, 0x85, 0x70, 0x9c, 0xec, 0xd7, 0x2c, 0x59, 0x5a, 0xea, 0xc0, 0x6d, 0x82, 0xd0, 0x12, 0x7d, 0x9f},
	{ 0x6e, 0x85, 0xa1, 0xcc, 0x48, 0x93, 0xd7, 0xfd, 0xbe, 0x6e, 0xb6, 0xee, 0x69, 0xd9, 0x97, 0xfe, 0x1a, 0xb8, 0x36, 0x9a, 0xde, 0x36, 0xaa, 0xad, 0x84, 0xa5, 0x22, 0xcf, 0x05, 0x5a, 0x6a, 0x95},
	{ 0x85, 0x95, 0x9c, 0x0d, 0xc9, 0x4e, 0x59, 0xc8, 0xe7, 0x1e, 0xd0, 0xb4, 0x8c, 0x30, 0x19, 0x1a, 0x45, 0x57, 0xb0, 0x16, 0xd6, 0x71, 0xf1, 0x31, 0x98, 0xc8, 0xe5, 0x9d, 0x36, 0xca, 0xd9, 0xd7},
	{ 0xdd, 0xe4, 0x79, 0x91, 0xec, 0xef, 0x88, 0xf9, 0xe7, 0x03, 0x1a, 0x73, 0x94, 0xa6, 0x63, 0x3a, 0x17, 0x76, 0x00, 0x3b, 0x62, 0xca, 0x82, 0xa4, 0x9b, 0x1c, 0x07, 0xd2, 0xb8, 0xae, 0x6b, 0xed},
	{ 0x31, 0xe7, 0x65, 0x69, 0x48, 0x22, 0xa3, 0xd4, 0x63, 0x6f, 0x9f, 0x01, 0x28, 0xe5, 0x8a, 0x55, 0x52, 0x62, 0xda, 0x93, 0x02, 0xc3, 0x1e, 0xb3, 0x47, 0xdc, 0xb7, 0xc0, 0x65, 0xb4, 0x0b, 0xea},
	{ 0x2d, 0x44, 0xa0, 0x96, 0x48, 0x0c, 0x69, 0xbc, 0xe2, 0x9c, 0x07, 0x1c, 0x6f, 0x4c, 0xa5, 0xb4, 0x36, 0x0a, 0x10, 0xab, 0x7f, 0xb8, 0x52, 0x14, 0x91, 0x79, 0x79, 0x84, 0xb7, 0xfc, 0xef, 0x56},
	{ 0x56, 0x1e, 0x06, 0x6a, 0x92, 0x7e, 0x86, 0x0b, 0x54, 0x3b, 0xe0, 0xcd, 0xae, 0x22, 0xc0, 0xcd, 0xd7, 0x3a, 0xa6, 0x38, 0x7d, 0x00, 0xaa, 0xb1, 0x4a, 0xc8, 0xa5, 0xbe, 0x64, 0x49, 0xbb, 0xc2},
	{ 0x4c, 0xca, 0x47, 0xd9, 0x67, 0x8c, 0x9f, 0x89, 0xf2, 0xe3, 0x3c, 0xf4, 0x83, 0xea, 0x21, 0xd4, 0x21, 0x98, 0x62, 0xbb, 0x3f, 0x3f, 0xb1, 0x96, 0xb2, 0xbf, 0x8f, 0xab, 0xd8, 0x19, 0x39, 0xfc},
	{ 0x91, 0x5e, 0x7a, 0xa4, 0x3b, 0xbe, 0xa1, 0xc3, 0x6b, 0x08, 0xce, 0x21, 0x15, 0x64, 0x1a, 0xe6, 0x33, 0x04, 0x6a, 0x2b, 0x9e, 0x33, 0x46, 0x15, 0x05, 0x86, 0xb3, 0xed, 0xbb, 0x06, 0x7a, 0xbd},
	{ 0x80, 0xe5, 0x22, 0x60, 0xb1, 0xfc, 0x8d, 0x3b, 0x3e, 0xf4, 0xa2, 0x55, 0xd9, 0xca, 0xe4, 0x66, 0xbd, 0x2f, 0x40, 0x1e, 0x94, 0x12, 0xae, 0x65, 0x2b, 0x87, 0xe0, 0xf4, 0xc3, 0xbc, 0x49, 0xe8},
	{ 0x61, 0x0a, 0x19, 0x40, 0x5f, 0xd2, 0xb4, 0x7a, 0x9f, 0xc0, 0xff, 0xe3, 0x9f, 0xf6, 0x88, 0xa0, 0xfd, 0xe3, 0xd3, 0x2a, 0xd5, 0xcb, 0xee, 0xc9, 0x1e, 0x2b, 0xab, 0x67, 0x6b, 0x20, 0x46, 0x02},
	{ 0x2c, 0x3e, 0x6f, 0x33, 0xaf, 0xb7, 0xc5, 0x02, 0x26, 0x16, 0x9e, 0x9b, 0x22, 0x67, 0xa1, 0x73, 0x82, 0x86, 0x3b, 0xe6, 0x58, 0x81, 0xf7, 0xd8, 0xcc, 0xc5, 0x29, 0x2a, 0xde, 0xab, 0x9a, 0xb1},
	{ 0x82, 0x4c, 0x85, 0x91, 0x20, 0x54, 0xcf, 0x5f, 0xb9, 0x22, 0x3f, 0x56, 0x1d, 0xd9, 0xd6, 0xac, 0x7f, 0x98, 0x17, 0x0e, 0x17, 0x19, 0x5e, 0x7e, 0x05, 0x67, 0xda, 0xbe, 0xba, 0xe4, 0x17, 0x76},
	{ 0xdf, 0x41, 0x25, 0x7c, 0x56, 0x17, 0x81, 0x74, 0x3f, 0xad, 0x2f, 0x44, 0x04, 0x15, 0xbb, 0x59, 0x8b, 0xc9, 0x6a, 0xc8, 0x7d, 0x6a, 0x9d, 0x4f, 0x1e, 0x1a, 0xd7, 0x9c, 0x72, 0xb6, 0xfe, 0x13},
	{ 0x56, 0xda, 0x9f, 0x0e, 0xaa, 0x07, 0xe1, 0x83, 0x19, 0x02, 0x3d, 0x8c, 0x76, 0x49, 0x9e, 0x6a, 0xf4, 0x40, 0x8d, 0xf0, 0xc8, 0xe3, 0xf9, 0xdb, 0xc9, 0xd7, 0x35, 0xde, 0x8f, 0xb9, 0xfd, 0x5a},
	{ 0x1d, 0xeb, 0xe6, 0x1b, 0x01, 0xae, 0x43, 0x53, 0xc1, 0xa0, 0x39, 0x40, 0x85, 0xe2, 0x6c, 0x3c, 0x75, 0x7b, 0x23, 0xa4, 0x95, 0x76, 0x91, 0x2b, 0x54, 0xf6, 0x22, 0xda, 0x2a, 0xf0, 0x9d, 0x80},
	{ 0x9a, 0xbc, 0x89, 0x42, 0xcb, 0x20, 0x50, 0xa7, 0x6c, 0xdf, 0xe2, 0x62, 0x32, 0x10, 0x2f, 0xa5, 0x33, 0xe5, 0x6a, 0x45, 0xaf, 0xce, 0xc9, 0x24, 0x91, 0xb8, 0x91, 0x5b, 0xee, 0x72, 0x68, 0xd1},
	{ 0x38, 0x8b, 0x26, 0x16, 0xd8, 0x65, 0x48, 0xcd, 0xa8, 0xfd, 0x30, 0x1a, 0x17, 0xd3, 0xfd, 0x2d, 0x76, 0x93, 0x3b, 0x85, 0x19, 0xba, 0xea, 0x83, 0xcc, 0xef, 0x60, 0x0e, 0x06, 0x11, 0x2a, 0xfa},
	{ 0x9d, 0x19, 0x8d, 0xb6, 0xb0, 0xa4, 0xb0, 0x51, 0x0e, 0xa1, 0xd7, 0xe8, 0x4c, 0xc2, 0xd4, 0xc5, 0x80, 0xc1, 0x3d, 0x89, 0x3a, 0xde, 0xbc, 0xc6, 0x59, 0x7e, 0x35, 0xd9, 0x2e, 0x00, 0x1a, 0xa9},
	{ 0xa7, 0xb8, 0x03, 0x25, 0xee, 0xca, 0xd0, 0x05, 0xda, 0xee, 0x7e, 0x58, 0x62, 0x3b, 0x88, 0x8a, 0xc4, 0x79, 0x65, 0x96, 0x46, 0xae, 0x03, 0x93, 0x83, 0x87, 0xb2, 0x0b, 0xd9, 0x99, 0xcf, 0x8f},
	{ 0xd9, 0x15, 0xc7, 0x1e, 0x3b, 0xf2, 0x65, 0x34, 0x7c, 0x5b, 0xb8, 0xbe, 0x49, 0xb3, 0x40, 0x36, 0x9b, 0x60, 0xf5, 0xe4, 0xe0, 0x0d, 0x43, 0x51, 0x97, 0xaf, 0xed, 0xc0, 0x59, 0x63, 0x7a, 0xc0},
	{ 0x79, 0xe8, 0x38, 0x37, 0x47, 0x70, 0x93, 0x68, 0xe9, 0x12, 0xac, 0x13, 0xa7, 0xd6, 0x24, 0xa5, 0x66, 0xeb, 0x66, 0x31, 0xb9, 0xd2, 0x88, 0x5a, 0x41, 0x61, 0xad, 0x5a, 0xb4, 0x83, 0xea, 0xf7},
	{ 0x14, 0xbf, 0xb3, 0x9d, 0x3c, 0x1d, 0x62, 0xa0, 0x0f, 0x42, 0x42, 0x7c, 0x66, 0x17, 0x10, 0xf0, 0x4a, 0x35, 0x61, 0x88, 0x95, 0xb8, 0x3e, 0xe7, 0xac, 0x1a, 0x8d, 0xac, 0x34, 0x9f, 0x1b, 0x51},
	{ 0x8d, 0x96, 0x79, 0x7f, 0x52, 0x6c, 0x47, 0x61, 0x61, 0x50, 0xa2, 0xf2, 0xf5, 0x20, 0x6f, 0xca, 0x42, 0xbd, 0xa3, 0x04, 0x96, 0xea, 0xea, 0x4c, 0xd3, 0x0d, 0x4a, 0xf5, 0x80, 0x10, 0xbf, 0x0c},
	{ 0xbf, 0x4b, 0x8f, 0x98, 0x90, 0xd5, 0x13, 0x7e, 0x0c, 0xb4, 0x35, 0x93, 0x5a, 0xe3, 0xbf, 0xaa, 0x10, 0xdf, 0x83, 0x3c, 0x4d, 0x50, 0xe4, 0x27, 0xc9, 0xdc, 0x14, 0x41, 0x8f, 0x57, 0x07, 0x18},
	{ 0x0d, 0xb3, 0xba, 0x9e, 0x59, 0x62, 0xd3, 0x26, 0x5e, 0x1f, 0x1f, 0x7a, 0x65, 0xf3, 0xf9, 0x0a, 0x22, 0x8f, 0x06, 0xee, 0x02, 0x68, 0xff, 0x6c, 0x0d, 0x10, 0xc0, 0x97, 0x14, 0xc5, 0xe2, 0x90},
	{ 0x03, 0x99, 0x95, 0xfe, 0x53, 0x96, 0xa9, 0xaf, 0xd5, 0x0d, 0x89, 0xa3, 0xc6, 0x1e, 0x84, 0xef, 0xf1, 0xae, 0x97, 0x3f, 0x78, 0x17, 0x96, 0x70, 0x5c, 0x82, 0xc0, 0xaf, 0x3b, 0xda, 0x4b, 0xa0},
	{ 0xe8, 0x67, 0x5f, 0xb1, 0xd9, 0x2f, 0xaf, 0x98, 0x6a, 0x83, 0x0b, 0x95, 0x10, 0xba, 0x50, 0x29, 0xac, 0xd5, 0x21, 0x59, 0xc1, 0x45, 0xa7, 0x0a, 0x16, 0x92, 0x55, 0x4b, 0x97, 0x5d, 0x39, 0xc5},
	{ 0x9b, 0xb6, 0xf4, 0x72, 0x7d, 0x31, 0x42, 0xf9, 0x7a, 0xbe, 0x75, 0xcc, 0xc6, 0xe1, 0x0f, 0x54, 0x4e, 0xf8, 0xdd, 0xde, 0x28, 0x73, 0x05, 0x2a, 0xd6, 0xba, 0x70, 0xda, 0x77, 0xea, 0x8d, 0x50},
	{ 0x71, 0xb1, 0xfb, 0x24, 0xa6, 0x34, 0xc1, 0x28, 0xb3, 0x7c, 0x78, 0xf4, 0xb7, 0x79, 0x55, 0x56, 0x99, 0xd5, 0x5f, 0x62, 0x98, 0x66, 0x76, 0x48, 0x81, 0x56, 0x5e, 0x59, 0xf3, 0x82, 0x00, 0xe8},
	{ 0x06, 0xbe, 0xa7, 0xa2, 0x91, 0xa3, 0x0f, 0x1c, 0x5e, 0xf8, 0x16, 0xfa, 0x89, 0xfa, 0x1e, 0x89, 0x76, 0x80, 0xd7, 0xe9, 0x24, 0xb3, 0x23, 0xb8, 0x29, 0xe4, 0x5e, 0x3c, 0xff, 0x31, 0x17, 0x71},
	{ 0x59, 0x90, 0xbf, 0x0c, 0xdc, 0x60, 0x58, 0x41, 0xca, 0x58, 0xad, 0x22, 0x02, 0x00, 0x0e, 0xa5, 0xe3, 0xd0, 0x6e, 0x9b, 0xcb, 0x65, 0xd6, 0x49, 0x5b, 0x20, 0xd2, 0x52, 0xeb, 0x36, 0x8d, 0x48},
	{ 0x19, 0xd5, 0xbc, 0x5b, 0x73, 0xea, 0xae, 0x99, 0x89, 0x66, 0xe2, 0x00, 0x3d, 0x49, 0xa5, 0xfe, 0x48, 0x9c, 0x8d, 0xce, 0x8a, 0xae, 0xe0, 0x9a, 0xc5, 0x94, 0xd8, 0xde, 0x0b, 0xdc, 0x0a, 0x51},
	{ 0xb6, 0x96, 0x9d, 0xf6, 0xa1, 0x96, 0x25, 0xf4, 0xc7, 0x43, 0x99, 0x1f, 0x7a, 0x79, 0xc1, 0x09, 0x2e, 0x2b, 0xe7, 0xdf, 0xa9, 0x05, 0x3b, 0x7a, 0x91, 0x3a, 0x4a, 0xac, 0xeb, 0x48, 0x3a, 0x72},
	{ 0x2c, 0x0a, 0x57, 0x65, 0x23, 0xbb, 0x97, 0x07, 0x15, 0x8c, 0x88, 0x0c, 0xbd, 0xfc, 0xb6, 0x6b, 0x45, 0x17, 0xcf, 0x61, 0x88, 0x90, 0xa5, 0x27, 0x6b, 0xf8, 0xe0, 0x4c, 0x3c, 0xc3, 0x29, 0x66},
	{ 0x0a, 0x15, 0xa7, 0xaa, 0x06, 0x8d, 0x2f, 0x29, 0xc3, 0x18, 0x52, 0x3b, 0x34, 0x78, 0x63, 0x44, 0xe6, 0x17, 0x75, 0xbf, 0x81, 0x00, 0x02, 0xf2, 0x1c, 0x95, 0xf4, 0xfc, 0x7d, 0x7b, 0x0a, 0xcf},
	{ 0x7d, 0x34, 0x81, 0x58, 0xdd, 0x3f, 0x8d, 0x22, 0xac, 0x2c, 0x89, 0x54, 0x90, 0xc3, 0x2b, 0xd1, 0xed, 0xf4, 0xf5, 0x7f, 0xb3, 0xc5, 0x39, 0x1e, 0x5e, 0xcc, 0x0e, 0x01, 0x03, 0x0b, 0x84, 0x44},
	{ 0xf4, 0xab, 0xf7, 0x6e, 0x82, 0xa5, 0x16, 0xab, 0x19, 0x72, 0x3b, 0x45, 0xcc, 0xe7, 0xa8, 0xa8, 0x12, 0xbd, 0xe5, 0x03, 0x9f, 0x20, 0x64, 0xae, 0x64, 0x29, 0x08, 0xcf, 0x54, 0x54, 0xa7, 0xa5},
	{ 0xaf, 0xd2, 0xd1, 0x73, 0x67, 0xaf, 0xfe, 0x5c, 0x0d, 0x46, 0xc2, 0xa4, 0x52, 0x52, 0x87, 0x6f, 0xec, 0xc4, 0x02, 0x9c, 0x41, 0xc7, 0x5a, 0xfd, 0x0e, 0xd9, 0x73, 0xfc, 0x4d, 0xdf, 0x02, 0x93},
	{ 0xc4, 0xa0, 0x58, 0x3b, 0x7d, 0x33, 0x59, 0x7e, 0xf1, 0x1c, 0xfb, 0xa3, 0x8f, 0xd2, 0x49, 0xf5, 0x3a, 0xdc, 0xfb, 0x1c, 0xdf, 0xbe, 0x07, 0xf1, 0x39, 0x5d, 0xd4, 0xf1, 0xaf, 0x41, 0x60, 0xe5},
	{ 0x35, 0xdf, 0xa8, 0x9f, 0xaa, 0xbe, 0xf4, 0x0b, 0xdb, 0x08, 0x1c, 0x81, 0x23, 0xe5, 0x3a, 0x67, 0x74, 0x04, 0x62, 0xa4, 0x7c, 0xd9, 0x32, 0x1c, 0x55, 0x83, 0x7d, 0x65, 0x46, 0x78, 0x75, 0x12},
	{ 0x1e, 0xd9, 0x27, 0xb3, 0x8a, 0x7a, 0x30, 0x4f, 0xa3, 0x79, 0x23, 0x07, 0x19, 0xcd, 0x59, 0x5b, 0x5a, 0x2f, 0x4f, 0xb0, 0x58, 0x38, 0x2a, 0x58, 0x47, 0x29, 0x2a, 0x6d, 0xa0, 0xb7, 0xae, 0xb0},
	{ 0x6e, 0xc9, 0xf7, 0xf2, 0x1f, 0xb6, 0xdb, 0x92, 0xf4, 0x23, 0x10, 0x0c, 0x6e, 0x83, 0xa5, 0x48, 0x73, 0x1b, 0x21, 0xd1, 0x59, 0xfd, 0x72, 0x01, 0x2b, 0x9e, 0xa1, 0xe4, 0xca, 0x12, 0x22, 0x4d},
	{ 0x61, 0xb5, 0x67, 0x99, 0x4b, 0x6a, 0xce, 0xc5, 0x2b, 0xf4, 0x85, 0x07, 0x9d, 0x6c, 0x4e, 0xfd, 0xcc, 0x91, 0x01, 0x53, 0x40, 0xe6, 0x07, 0x4c, 0xae, 0x5d, 0xce, 0xae, 0x0a, 0x62, 0xb5, 0x7c},
	{ 0x1d, 0x6b, 0x02, 0x2d, 0x61, 0xb0, 0xb3, 0xde, 0x05, 0xa7, 0xbd, 0x36, 0x12, 0xc1, 0xfc, 0x7c, 0x90, 0xcc, 0xde, 0x6c, 0x3c, 0x88, 0x19, 0x68, 0x26, 0x64, 0x49, 0xe5, 0xa9, 0xeb, 0xfa, 0x0f},
	{ 0x0d, 0xe4, 0x2d, 0x48, 0x19, 0xa3, 0x78, 0xeb, 0xf1, 0x46, 0x47, 0x51, 0xe8, 0xcf, 0x82, 0xa7, 0xbf, 0x6b, 0x39, 0x5c, 0x00, 0x21, 0xae, 0xc5, 0xe8, 0xc1, 0x25, 0x39, 0xaa, 0x9c, 0x6a, 0x07},
	{ 0x80, 0xa3, 0x9e, 0x06, 0x6c, 0x8c, 0xb8, 0x86, 0xe7, 0x4e, 0xb0, 0x6a, 0x9a, 0x8f, 0x07, 0x2d, 0xe4, 0x4d, 0x61, 0x07, 0xd6, 0x6f, 0x41, 0xc6, 0x76, 0x57, 0x66, 0x7b, 0x9a, 0x83, 0x86, 0xb5},
	{ 0x98, 0xaf, 0x73, 0x47, 0x57, 0x8e, 0x15, 0xe7, 0x06, 0xf5, 0xce, 0x6d, 0x97, 0x5d, 0xa4, 0x2a, 0x5c, 0x15, 0x15, 0xa7, 0xf7, 0x43, 0x3e, 0xc1, 0x6d, 0x12, 0x3d, 0x47, 0x4d, 0x2e, 0x71, 0x7c},
	{ 0x98, 0x26, 0x81, 0xe8, 0x7c, 0x48, 0x8e, 0x35, 0x73, 0x8e, 0x61, 0x13, 0xba, 0xf3, 0xa1, 0xf6, 0x28, 0x42, 0xe7, 0x7b, 0xde, 0xd5, 0x98, 0x72, 0xc9, 0xd2, 0xcb, 0xab, 0x0a, 0x3c, 0x81, 0x5f}
};

#endif