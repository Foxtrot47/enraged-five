#ifndef _MULTI_KEY_AESO_H_
#define _MULTI_KEY_AESO_H_

#if RSG_ORBIS  || __RESOURCECOMPILER || RSG_TOOL
#ifndef _INTPTR
#define _INTPTR 1
#endif
#include <stdio.h>
#ifdef __cplusplus
extern "C" { 
#endif

//#if RSG_ORBIS
extern unsigned char unique_multikey_gta5_ps4[101][32];
//#endif

#ifdef __cplusplus
}
#endif

#endif // RSG_ORBIS
#endif //_MULTI_KEY_AESO_H_

