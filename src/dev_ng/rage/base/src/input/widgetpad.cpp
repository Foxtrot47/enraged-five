// 
// input/widgetpad.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK

#include "widgetpad.h"

#include "pad.h"
#include "bank/packet.h"
#include "bank/pane.h"

using namespace rage;

static bool s_registered = false;

ioWidgetPad::ioWidgetPad( const char *title, int padIndex, EPlatform platform, const char* memo, const char* fillColor, bool readOnly ) 
	: bkWidget( NullCB, title, memo, fillColor, readOnly )
	, m_padIndex(padIndex)
	, m_platform(platform)
{
	for ( int i = 0; i < 4; ++i )
	{
		m_axis[i] = 0x80;
	}

	for ( int i = 0; i < ioPad::NUMBUTTONS; ++i )
	{
		m_analogButtons[i] = 0;
	}

	m_buttons = 0;
}

ioWidgetPad::~ioWidgetPad() 
{

}

int ioWidgetPad::DrawLocal( int x, int y )
{
	Drawf( x, y, m_Title );
	return bkWidget::DrawLocal( x, y );
}

void ioWidgetPad::AddHandlers()
{
	if ( !s_registered )
	{
		bkRemotePacket::AddType( ioWidgetPad::GetStaticGuid(), ioWidgetPad::RemoteHandler );
		s_registered = true;
	}
}

int ioWidgetPad::GetGuid() const 
{
	return GetStaticGuid();
}

void ioWidgetPad::Update()
{
	ioPad& pad = ioPad::GetPad( m_padIndex );
	for ( int i = 0; i < 4; ++i )
	{
		pad.SetBankAxis( i, m_axis[i] );
	}

	if ( m_buttons != 0 )
	{
		pad.SetBankButtons( m_buttons );
	}

	for ( int i = 0; i < ioPad::NUMBUTTONS; ++i )
	{
		if ( m_analogButtons[i] != 0 )
		{
			pad.SetBankAnalogButtons( i, m_analogButtons[i] );
		}
	}
}

void ioWidgetPad::RemoteCreate()
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin( bkRemotePacket::CREATE,GetStaticGuid(),this );
	p.WriteWidget( m_Parent );
	p.Write_const_char( m_Title );
	p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
	p.Write_s32( m_padIndex );
	p.Write_s32( m_platform );
	p.Send();
}

void ioWidgetPad::RemoteHandler( const bkRemotePacket& packet ) 
{
	if ( packet.GetCommand() == bkRemotePacket::CREATE )
	{
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if ( !parent )
		{
			return;
		}

		const char *title = packet.Read_const_char();
		const char* tooltip = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
		int padIndex = packet.Read_s32();
		EPlatform platform = (EPlatform)packet.Read_s32();
		packet.End();

		ioWidgetPad& widget = *(rage_new ioWidgetPad( title, padIndex, platform, tooltip, fillColor, readOnly ));
		bkRemotePacket::SetWidgetId( widget, id );
		parent->AddChild( widget );
	}
	else if ( packet.GetCommand() == bkRemotePacket::USER + 0 )
	{
		// digital pad pressed
		packet.Begin();

		ioWidgetPad* widget = packet.ReadWidget<ioWidgetPad>();
		if ( !widget )
		{
			return;
		}

		int button = packet.Read_s32();
		packet.End();

		widget->m_buttons |= button;

#if __PPU
		switch ( button )	
		{
		case ioPad::L1:
			widget->m_analogButtons[ioPad::L1_INDEX] = 0xff;
			break;
		case ioPad::R1:
			widget->m_analogButtons[ioPad::R1_INDEX] = 0xff;
			break;
		case ioPad::L2:
			widget->m_analogButtons[ioPad::L2_INDEX] = 0xff;
			break;
		case ioPad::R2:
			widget->m_analogButtons[ioPad::R2_INDEX] = 0xff;
			break;
		}
#elif __WIN32PC || __XENON
		if ( button == ioPad::L2 )
		{
			widget->m_analogButtons[ioPad::L2_INDEX] = 0xff;
		}
		else if ( button == ioPad::R2 )
		{
			widget->m_analogButtons[ioPad::R2_INDEX] = 0xff;
		}
#endif
	}
	else if ( packet.GetCommand() == bkRemotePacket::USER + 1 )
	{
		// digital pad released
		packet.Begin();

		ioWidgetPad* widget = packet.ReadWidget<ioWidgetPad>();
		if ( !widget )
		{
			return;
		}

		int button = packet.Read_s32();
		packet.End();

		widget->m_buttons &= ~button;

#if __PPU
		switch ( button )	
		{
		case ioPad::L1:
			widget->m_analogButtons[ioPad::L1_INDEX] = 0;
			break;
		case ioPad::R1:
			widget->m_analogButtons[ioPad::R1_INDEX] = 0;
			break;
		case ioPad::L2:
			widget->m_analogButtons[ioPad::L2_INDEX] = 0;
			break;
		case ioPad::R2:
			widget->m_analogButtons[ioPad::R2_INDEX] = 0;
			break;
		}
#elif __WIN32PC || __XENON
		if ( button == ioPad::L2 )
		{
			widget->m_analogButtons[ioPad::L2_INDEX] = 0;
		}
		else if ( button == ioPad::R2 )
		{
			widget->m_analogButtons[ioPad::R2_INDEX] = 0;
		}
#endif
	}
	else if ( (packet.GetCommand() == bkRemotePacket::USER + 2) || (packet.GetCommand() == bkRemotePacket::USER + 3) )
	{
		// analog stick pressed or moved
		packet.Begin();

		ioWidgetPad* widget = packet.ReadWidget<ioWidgetPad>();
		if ( !widget )
		{
			return;
		}

		EAnalogStick stick = (EAnalogStick)packet.Read_s32();
		u8 x = packet.Read_u8();
		u8 y = packet.Read_u8();
		packet.End();

		if ( stick == LEFT_STICK )
		{
			widget->m_axis[0] = x;
			widget->m_axis[1] = y;
		}
		else if ( stick == RIGHT_STICK )
		{
			widget->m_axis[2] = x;
			widget->m_axis[3] = y;
		}
	}
	else if ( packet.GetCommand() == bkRemotePacket::USER + 4 )
	{
		// analog stick released
		packet.Begin();

		ioWidgetPad* widget = packet.ReadWidget<ioWidgetPad>();
		if ( !widget )
		{
			return;
		}

		EAnalogStick stick = (EAnalogStick)packet.Read_s32();
		packet.Read_u8();	// x
		packet.Read_u8();	// y
		packet.End();

		if ( stick == LEFT_STICK )
		{
			widget->m_axis[0] = 0x80;
			widget->m_axis[1] = 0x80;
		}
		else if ( stick == RIGHT_STICK )
		{
			widget->m_axis[2] = 0x80;
			widget->m_axis[3] = 0x80;
		}
	}
}

#if __WIN32PC
#include "system/xtl.h"

void ioWidgetPad::WindowCreate() 
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{
		GetPane()->AddLastWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX );
	}
}
#endif // __WIN32PC


#endif // __BANK
