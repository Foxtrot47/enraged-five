//
// input/keys.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_KEYS_H
#define INPUT_KEYS_H

// The values for the keys were chosen to match DirectInput's layout
// the KEY_ values are system-independent and are NOW defined in mapper_defs.h
#include "mapper_defs.h"

/* Convenience macros which expand to use the two-parameter version of ioKeyboard::KeyPressed */
#define KEY_CONTROL			KEY_LCONTROL, KEY_RCONTROL
#define KEY_SHIFT			KEY_LSHIFT, KEY_RSHIFT
#define KEY_ALT				KEY_LMENU, KEY_RMENU
#define KEY_MENU			KEY_ALT

#endif
