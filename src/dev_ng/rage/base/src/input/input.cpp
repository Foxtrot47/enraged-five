//
// input/input.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "input.h"
#include "input_channel.h"

#include "eventq.h"

#include "remote_input.h"

#include "vector/vector3.h"

#include "bank/packet.h"
#include "grcore/device.h"
#include "input/mapper.h"
#include "profile/timebars.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/wndproc.h"

#include "diag/channel.h"

#include <stdio.h>

using namespace rage;

#define ENABLE_TOGGLES		(!__FINAL)


RAGE_DEFINE_CHANNEL(Input)

#if ENABLE_TOGGLES
PARAM(toggle0,"[input] Connect toggle0 (DSW4 or Shift-F9) to named parameter");
PARAM(toggle1,"[input] Connect toggle1 (DSW5 or Shift-F10) to named parameter");
PARAM(toggle2,"[input] Connect toggle2 (DSW6 or Shift-F11) to named parameter");
PARAM(toggle3,"[input] Connect toggle3 (DSW7 or Shift-F12) to named parameter");

const int MaxToggles = 4;
static sysParam *Toggles[MaxToggles];
#endif // ENABLE_TOGGLES

// Sometimes, you want to deal with these yourself.  Set this before Begin.
bool ioInput::sm_hasBegun = false;
bool ioInput::sm_inWindow = false;
bool ioInput::sm_UseJoystick = true;
bool ioInput::sm_UseKeyboard = true;
bool ioInput::sm_UseMouse = __WIN32PC;

#if RSG_PC && !RSG_FINAL
PARAM(enableXInputEmulation, "[input] Enables XInput emulation from DirectInput devices.");
#endif // RSG_PC && !RSG_FINAL

#if __BANK
bool ioInput::sm_IsGhettoKeyboardEnabled;
#endif // __BANK

const int MaxUpdaters = 4;
ioInputUpdater ioInput::sm_Updaters[MaxUpdaters];

#if ENABLE_TOGGLES
static void ConnectToggle(const sysParam &src,sysParam *&dest) {
	const char *name;
	if (src.Get(name) && *name) {
		dest = sysParam::GetParamByName(name);
		if (!dest)
			Errorf("Toggle '%s' isn't a known parameter name, ignored!",name);
	}
}

static void ToggleFromKey(sysParam *dest) {
	if (dest) {
		if (dest->Get()) {
			Displayf("Debug keyboard toggling %s OFF",dest->GetName());
			dest->Set(NULL);
		}
		else {
			Displayf("Debug keyboard toggling %s ON",dest->GetName());
			dest->Set("");
		}
	}
}

#if __PS3
namespace rage { extern u64 g_DipSwitches; }
static u64 CurrentDipSwitches;

static void ToggleFromSwitch(sysParam *dest,u64 mask) {
	// If the parameter is hooked up, and the specified switch just changed...
	if (dest && ((g_DipSwitches ^ CurrentDipSwitches) & mask)) {
		if (g_DipSwitches & mask) {
			Displayf("DIP switch toggling %s ON",dest->GetName());
			dest->Set("");
		}
		else {
			Displayf("DIP switch toggling %s OFF",dest->GetName());
			dest->Set(NULL);
		}
	}
}
#endif	// __PS3
#endif	// ENABLE_TOGGLES

void ioInput::Begin(bool inWindow, bool exclusiveMouse /*= false*/) {
	sm_hasBegun = true;

	sm_inWindow = inWindow && __WIN32PC;

#if REMOTE_INPUT_SUPPORT
	ioRemoteInput::Begin();
	if (ioRemoteInput::IsEnabled() == true)
	{
		sm_UseKeyboard = true;
		sm_UseMouse = true;
	}
#endif // REMOTE_INPUT_SUPPORT

#if __BANK
	// if we're connected to RAG, keyboard and mouse input is supported:
	if (bkRemotePacket::IsConnectedToRag())
	{
		sm_UseKeyboard=true;
		sm_UseMouse=true;
	}
#endif // __BANK

#if __WIN32PC
	g_WindowProc = InputWindowProc;
#endif // __WIN32PC

	if (sm_UseMouse)
		MOUSE.Begin(sm_inWindow, exclusiveMouse);
	if (sm_UseKeyboard)
		KEYBOARD.Begin(sm_inWindow);

#if __WIN32PC
	if(ioInput::sm_UseJoystick)
		ioJoystick::BeginAndCalibrateAll("platform:/data/control/calibration.meta", "user:/control/calibration.xml");
#endif // __WIN32PC

	ioPad::BeginAll();

	ioHeadTracking::Begin(sm_inWindow);

#if ENABLE_TOGGLES
	ConnectToggle(PARAM_toggle0,Toggles[0]);
	ConnectToggle(PARAM_toggle1,Toggles[1]);
	ConnectToggle(PARAM_toggle2,Toggles[2]);
	ConnectToggle(PARAM_toggle3,Toggles[3]);
#endif // ENABLE_TOGGLES
}


void ioInput::End() {
	ioPad::EndAll();
#if __WIN32PC
	if(ioInput::sm_UseJoystick)
		ioJoystick::EndAll();
#endif // __WIN32PC
	if (sm_UseKeyboard)
		ioKeyboard::End();
	if (sm_UseMouse)
		ioMouse::End();

	REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::End());

	ioHeadTracking::End();
}


void ioInput::Update(bool ignorePadInput, bool ignoreMouseInput, bool ignoreKeyboardInput, bool checkForNewDevice) 
{
	PF_PUSH_TIMEBAR("MOUSE.Update");
	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioInput::Update() - MOUSE.Update"));
	if (sm_UseMouse)
		MOUSE.Update(ignoreMouseInput);
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	PF_POP_TIMEBAR();

	PF_PUSH_TIMEBAR("KEYBOARD.Update");
	if (sm_UseKeyboard)
		KEYBOARD.Update(ignoreKeyboardInput);

	REMOTE_INPUT_SUPPORT_ONLY(PF_PUSH_TIMEBAR("ioRemoteInput.Update"));
	REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::Update());
	REMOTE_INPUT_SUPPORT_ONLY(PF_POP_TIMEBAR());
	
#if ENABLE_TOGGLES
	if (KEYBOARD.KeyDown(KEY_SHIFT)) {
		if (KEYBOARD.KeyPressed(KEY_F9)) ToggleFromKey(Toggles[0]);
		if (KEYBOARD.KeyPressed(KEY_F10)) ToggleFromKey(Toggles[1]);
		if (KEYBOARD.KeyPressed(KEY_F11)) ToggleFromKey(Toggles[2]);
		if (KEYBOARD.KeyPressed(KEY_F12)) ToggleFromKey(Toggles[3]);
	}
#if __PS3
	// Only watch for changes; this also has the nice side effect of never
	// running if there are no dip switches present on the hardware.
	if (g_DipSwitches != CurrentDipSwitches) {
		ToggleFromSwitch(Toggles[0],16);
		ToggleFromSwitch(Toggles[1],32);
		ToggleFromSwitch(Toggles[2],64);
		ToggleFromSwitch(Toggles[3],128);
		CurrentDipSwitches = g_DipSwitches;
	}
#endif
#endif
	PF_POP_TIMEBAR();

#if __WIN32PC
	PF_PUSH_TIMEBAR("ioJoystick.Update");
	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioInput::Update() - ioJoystick.Update"));
	if(ioInput::sm_UseJoystick) {
	  ioJoystick::PollAll();	
	  ioJoystick::UpdateAll();
	}
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	PF_POP_TIMEBAR();
#endif

#if __BANK
	UpdateGhettoPad();
#endif // __BANK

	PF_PUSH_TIMEBAR("ioPad.Update");
	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioInput::Update() - ioPad.Update"));
	ioPad::UpdateAll(ignorePadInput, checkForNewDevice);
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	PF_POP_TIMEBAR();

#if !__NO_OUTPUT && !__DEV
	u32 heldButtons = ioPad::GetPad(0).GetButtons() ^ ioPad::GetPad(1).GetButtons();
	u32 pressedButtons = ioPad::GetPad(0).GetPressedButtons() ^ ioPad::GetPad(1).GetPressedButtons();
	if (heldButtons == (ioPad::L1 | ioPad::R1 | ioPad::L3 | ioPad::R3) && pressedButtons) {
		if (!diagChannel::GetOutput())
		{
			printf("Turning output ON\n");
			diagChannel::SetOutput(true);
		}
		else
		{
			printf("Turning output OFF\n");
			diagChannel::SetOutput(false);
		}
	}
#endif

	PF_PUSH_TIMEBAR("Misc");
	EVENTQ.UpdateFromPipe();

	for (int i=0; i<MaxUpdaters; i++) {
		if (sm_Updaters[i]) {
			(*sm_Updaters[i])();
		}
	}
	PF_POP_TIMEBAR();

#if RSG_PC && !RSG_FINAL
	// Hack to get the debug camera working on PS4 controllers on PC. This is needed for people working from home.
	if(!PARAM_enableXInputEmulation.Get())
#endif // RSG_PC && !RSG_FINAL
	{
		// Moved to end because sm_Updaters may modify pad values (e.g. ioFFWheel)
		PF_PUSH_TIMEBAR("ioPad::UpdateDebugAll");
		ioPad::UpdateDebugAll();
		PF_POP_TIMEBAR();
	}

	PF_PUSH_TIMEBAR("ioHeadTracking::Update");
	ioHeadTracking::Update();
	PF_POP_TIMEBAR();

}

void ioInput::UpdateLocale()
{
	WIN32PC_ONLY(ioKeyboard::UpdateKeyboardLayoutSettings());
}

#if RSG_PC
ioInput::DeviceCaptureState ioInput::sm_DeviceCaptureState = ioInput::CAPTURE_MOUSE;
sysIpcEvent ioInput::sm_MessagePumpUpdatedEvent = sysIpcCreateEvent();

void ioInput::RecaptureLostDevices()
{
	switch(sm_DeviceCaptureState)
	{
	case CAPTURE_MOUSE:
		ioMouse::RecaptureLostDevices();
		sm_DeviceCaptureState = CAPTURE_KEYBOARD;
		break;

	case CAPTURE_KEYBOARD:
		ioKeyboard::RecaptureLostDevices();
		sm_DeviceCaptureState = CAPTURE_JOYSTICK;
		break;

	case CAPTURE_JOYSTICK:
		ioJoystick::RecaptureLostDevices();
		sm_DeviceCaptureState = CAPTURE_MOUSE;
		break;

	default:
		inputAssertf(false, "Unknown capture state %d", sm_DeviceCaptureState);
		sm_DeviceCaptureState = CAPTURE_MOUSE;
		break;
	}
}

void ioInput::WaitForMessagePumpUpdateSignal()
{
	sysIpcWaitEvent(sm_MessagePumpUpdatedEvent);
}

void ioInput::SignalMessagePumpUpdate()
{
	sysIpcSetEvent(sm_MessagePumpUpdatedEvent);
}
#endif // RSG_PC

#if __BANK
void ioInput::UpdateGhettoPad()
{
	// Toggle ghetto pad
	if (ioMapper::DebugKeyPressed(KEY_G)) {
		if (ioMapper::DebugKeyDown(KEY_LCONTROL, KEY_RCONTROL)) {
			sm_IsGhettoKeyboardEnabled = !sm_IsGhettoKeyboardEnabled;
			Displayf("Ghetto keyboard %d", (int) sm_IsGhettoKeyboardEnabled);

			// G? Nobody pressed G!
			ioKeyboard::OverrideKey(KEY_G, false);
		}
	}

	ioKeyboard::SetUseKeypadMode(sm_IsGhettoKeyboardEnabled, false);

	if (sm_IsGhettoKeyboardEnabled) {
		// Map keys to buttons and pad controls
		static int keyToButtonMapping[][2] = {
			// Digital buttons - use negative value to indicate
			{	KEY_RETURN, -ioPad::RDOWN},
			{	KEY_NUMPAD0, -ioPad::R2},
			{	KEY_HOME, -ioPad::START},					
			{	KEY_NUMPAD8, -ioPad::LUP},					
			{	KEY_NUMPAD2, -ioPad::LDOWN},					
			{	KEY_NUMPAD4, -ioPad::LLEFT},					
			{	KEY_NUMPAD6, -ioPad::LRIGHT},					
			{	KEY_NUMPAD7, -ioPad::L2},					
			{	KEY_NUMPAD9, -ioPad::R2},					
			{	KEY_NUMPAD1, -ioPad::L1},					
			{	KEY_NUMPAD3, -ioPad::R1},					

			// Analog buttons - use the normal value
			{	KEY_UP, ioPad::RUP},
			{	KEY_DOWN, ioPad::RDOWN},
			{	KEY_LEFT, ioPad::RLEFT},
			{	KEY_RIGHT, ioPad::RRIGHT},
			{	KEY_W, ioPad::LUP},
			{	KEY_S, ioPad::LDOWN},
			{	KEY_A, ioPad::LLEFT},
			{	KEY_D, ioPad::LRIGHT},
		};

		for (int x=0; x<NELEM(keyToButtonMapping); x++) {
			int key = keyToButtonMapping[x][0];
			if (ioKeyboard::KeyDown(key)) {
				int button = keyToButtonMapping[x][1];
				if (button < 0) {
					ioPad::GetPad(0).ButtonPressedCB(reinterpret_cast<CallbackData>(-button));
				} else {
					ioPad::GetPad(0).AnalogStickPressedCB(reinterpret_cast<CallbackData>(button));
				}

				ioKeyboard::OverrideKey(key, false);
			}
		}
	}
}
#endif // __BANK


void ioInput::RegisterUpdater(ioInputUpdater updater) {
	int i;
	for (i=0; i<MaxUpdaters; i++) {
		if (sm_Updaters[i] == updater) {
			return;
		}
	}
	for (i=0; i<MaxUpdaters; i++) {
		if (!sm_Updaters[i]) {
			sm_Updaters[i] = updater;
			return;
		}
	}
	Quitf(ERR_GEN_INPUT_1,"ioInput::RegisterUpdater - Out of slots");
}

float rage::ioAddDeadZone(float originalValue,float deadZone)
{
	Assert (deadZone>=0.0f && deadZone<1.0f);

	float val;

	if (originalValue>deadZone) {
		val = (originalValue-deadZone)/(1.0f-deadZone);
	}
	else if (originalValue<-deadZone) {
		val = (originalValue+deadZone)/(1.0f-deadZone);	
	}
	else {
		val = 0.0f;
	}

	return val;
}

float rage::ioAddRoundDeadZone(float& x, float& y, float deadZone, bool saturate)
{
	float z = 0.0f;
	return ioAddSphereDeadZone(x,y,z,deadZone,saturate);
}

float rage::ioAddSphereDeadZone(float& x, float& y, float& z, float deadZone, bool saturate)
{
	Vector3 v(x,y,z);

	float fOrigMag = v.Mag();

	if(fOrigMag < deadZone)
	{
		//Displayf("OLD: %f %f, mag: %f NEW: %f %f, mag: %f",x,y,fOrigMag,0.0f,0.0f,0.0f);
		x = y = z = 0.0f;

		return 0.0f;
	}
	else
	{
		float fOldMag = fOrigMag; //used to avoid calling sqrtf twice (once in Mag, once in Normalize) for speed

		//clip at 1.0f magnitude:
		if(saturate && fOldMag > 1.0f)
		{
			fOldMag = 1.0f;
		}

		float fNewMag = (fOldMag - deadZone) / (1.0f - deadZone); //offset and then scale the magnitude for smooth ramp up

		v.Scale(fNewMag / fOrigMag); //Think of this as a faster version of v.Normalize(), v.Scale(fNewMag).

		//Displayf("OLD: %f %f, mag: %f NEW: %f %f, mag: %f",x,y,fOrigMag,v.x,v.y,fNewMag);

		x = v.x;
		y = v.y;
		z = v.z;

		return fNewMag;
	}
}
