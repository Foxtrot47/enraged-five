// 
// input/widgetpad.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef INPUT_WIDGETPAD_H 
#define INPUT_WIDGETPAD_H 

#if __BANK

#include "bank/widget.h"

#include "pad.h"

namespace rage 
{

class ioWidgetPad : public bkWidget
{
public:
	// Keep in sync with base/tools/ragInput/WidgetPadVisible.cs
	enum EPlatform
	{
		WIN32_PLATFORM,
		XENON_PLATFORM,
		PS3_PLATFORM
	};

	enum EAnalogStick
	{
		NONE,
		LEFT_STICK,
		RIGHT_STICK
	};

	ioWidgetPad( const char *title, int padIndex, EPlatform platform, const char* memo=0, const char* fillColor=0, bool readOnly=false );
	~ioWidgetPad();

	static void AddHandlers();

	static int GetStaticGuid() { return BKGUID('i','n','p','t'); }
	int GetGuid() const;

	void Update();

protected:
	int DrawLocal( int x, int y );
	
	static void RemoteHandler(const bkRemotePacket& p);

#if __WIN32PC
	void WindowCreate();
	rageLRESULT WindowMessage( rageUINT /*msg*/, rageWPARAM /*wParam*/, rageLPARAM /*lParam*/ ) { return 0; }
#endif

private:
	void RemoteCreate();

	EPlatform m_platform;
	int m_padIndex;

	u8 m_axis[4];
	unsigned char m_analogButtons[ioPad::NUMBUTTONS];
	int m_buttons;
};

} // namespace rage

#endif // __BANK

#endif // INPUT_WIDGETPAD_H 
