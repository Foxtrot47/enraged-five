//
// input/input_channel.h
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_INPUT_CHANNEL_H
#define INPUT_INPUT_CHANNEL_H

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(Input)

#define inputAssert(cond)					RAGE_ASSERT(Input,cond)
#define inputAssertf(cond,fmt,...)			RAGE_ASSERTF(Input,cond,fmt,##__VA_ARGS__)
#define inputFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(Input,cond,fmt,##__VA_ARGS__)
#define inputVerify(cond)					RAGE_VERIFY(Input,cond)
#define inputVerifyf(cond,fmt,...)			RAGE_VERIFYF(Input,cond,fmt,##__VA_ARGS__)
#define inputErrorf(fmt,...)				RAGE_ERRORF(Input,fmt,##__VA_ARGS__)
#define inputWarningf(fmt,...)				RAGE_WARNINGF(Input,fmt,##__VA_ARGS__)
#define inputDisplayf(fmt,...)				RAGE_DISPLAYF(Input,fmt,##__VA_ARGS__)
#define inputDebugf1(fmt,...)				RAGE_DEBUGF1(Input,fmt,##__VA_ARGS__)
#define inputDebugf2(fmt,...)				RAGE_DEBUGF2(Input,fmt,##__VA_ARGS__)
#define inputDebugf3(fmt,...)				RAGE_DEBUGF3(Input,fmt,##__VA_ARGS__)
#define inputLogf(severity,fmt,...)			RAGE_LOGF(Input,severity,fmt,##__VA_ARGS__)

#endif // INPUT_INPUT_CHANNEL_H
