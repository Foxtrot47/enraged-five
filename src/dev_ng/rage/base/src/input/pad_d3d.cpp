// 
// input/pad_d3d.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "pad.h"
#include "widgetpad.h"
#include "xinput_config.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/file_config.h"
#include "math/amath.h"

#if __WIN32PC && !USE_XINPUT

#include "joystick.h"
#include "system/param.h"

#if RSG_PC && !__FINAL && !__TOOL && !RSG_RSC
namespace rage
{
	extern HWND g_hwndMain;
	XPARAM(ignoreFocus);
}
#endif

using namespace rage;

void ioPad::Begin(int /*index*/) {
}


void ioPad::End() {
}

void ioPad::Update(bool ignoreInput)
{
	ClearInputs();

#if RSG_PC && !__FINAL && !RSG_RSC
	// ignore controller input if we have multiple instances 
	// running on the same PC and we're in the background.
	if (sm_IgnoreControllerInput)
	{
		bool hasFocus = PARAM_ignoreFocus.Get() || (::GetForegroundWindow() == g_hwndMain);
#if __BANK
		if(!hasFocus)
		{
			// might be in rag so see if the parent window has focus.
			HWND window = ::GetParent(g_hwndMain);
			while(!hasFocus && window != NULL)
			{
				hasFocus = (::GetForegroundWindow() == window);
				window = ::GetParent(window);
			}
		}
#endif // __BANK

		if(!hasFocus)
		{
			StopShakingNow();
			return;
		}
	}
#endif

	if(ignoreInput)
	{
#if __WIN32PC
		StopShakingNow();
#endif
		return;
	}

	if (ioJoystick::GetStickCount() > m_PadIndex)
	{
		const ioJoystick &S = ioJoystick::GetStick(m_PadIndex);

		/* Displayf("AXIS: %d %d %d %d %d %d %d %d BTN %x",
			S.GetAxis(0),S.GetAxis(1),S.GetAxis(2),S.GetAxis(3),
			S.GetAxis(4),S.GetAxis(5),S.GetAxis(6),S.GetAxis(7),S.GetButtons());  */

		if (true /*m_IsAnalog*/)
		{
			m_Axis[0] = (u8) (S.GetAxis(0) >> 8);
			m_Axis[1] = (u8) (S.GetAxis(1) >> 8);
			if (S.GetAxisCount() >= 4)
			{
				if (S.GetJoyType()==ioJoystick::TYPE_DUAL_USB_FFJ_MP866)
				{
					m_Axis[2] = (u8) (S.GetAxis(6) >> 8);
					m_Axis[3] = (u8) (S.GetAxis(2) >> 8);
				}
				else if (S.GetJoyType()==ioJoystick::TYPE_SMARTJOYPAD || 
					     S.GetJoyType()==ioJoystick::TYPE_XKPC2002 || 
						 S.GetJoyType()==ioJoystick::TYPE_DUALJOYPAD || 
						 S.GetJoyType()==ioJoystick::TYPE_PSX_TO_USB_CONVERTER)
				{
					m_Axis[2] = (u8) (S.GetAxis(2) >> 8);
					m_Axis[3] = (u8) (S.GetAxis(5) >> 8);
				}
				else if (S.GetJoyType()==ioJoystick::TYPE_KIKYXSERIES)
				{
					m_Axis[2] = (u8) (S.GetAxis(5) >> 8);
					m_Axis[3] = (u8) (S.GetAxis(2) >> 8);
				}
				else if (S.GetJoyType()==ioJoystick::TYPE_LOGITECH_CORDLESS)
				{
					m_Axis[2] = (u8) (S.GetAxis(5) >> 8);
					m_Axis[3] = (u8) (S.GetAxis(6) >> 8);
				}
				else if (S.GetJoyType()==ioJoystick::TYPE_XBCD_XBOX_USB)
				{
					m_Axis[2] = (u8) (S.GetAxis(3) >> 8);
					m_Axis[3] = (u8) (S.GetAxis(4) >> 8);
				}
				else if (S.GetJoyType()==ioJoystick::TYPE_TIGERGAME_XBOX_USB)
				{
					m_Axis[2] = (u8) (S.GetAxis(2) >> 8);
					m_Axis[3] = (u8) (S.GetAxis(5) >> 8);
				}
				else if (S.GetJoyType()==ioJoystick::TYPE_XNA_GAMEPAD_XENON)
				{
					m_Axis[2] = 0xff - (u8) (S.GetAxis(3) >> 8);
					m_Axis[3] = 0xff - (u8) (S.GetAxis(4) >> 8);
				}
				else
				{
					m_Axis[2] = (u8) (S.GetAxis(2) >> 8);
					m_Axis[3] = (u8) (S.GetAxis(3) >> 8);
				}
			}
			else if (S.GetPOVCount())
			{
				short dir = (short) S.GetPOV(0);
				m_Axis[2] = m_Axis[3] = 0x80;
				if (dir == 0)
				{
					m_Axis[3] = 0;
				}
				else if (dir == 9000)
				{
					m_Axis[2] = 0xff;
				}
				else if (dir == 18000)
				{
					m_Axis[3] = 0xff;
				}
				else if (dir == 27000)
				{
					m_Axis[2] = 0;
				}
			}

			// Map POV hat.
			if (S.GetJoyType()==ioJoystick::TYPE_XNA_GAMEPAD_XENON)
			{
				// Map POV to left dpad.
				short dir = (short) S.GetPOV(0);
				if (dir >= 0)
				{
					if (dir < 9000 || dir > 27000)
					{
						m_Buttons |= ioPad::LUP;
					}
					if (dir > 0 && dir < 18000)
					{
						m_Buttons |= ioPad::LRIGHT;
					}
					if (dir > 9000 && dir < 27000)
					{
						m_Buttons |= ioPad::LDOWN;
					}
					if (dir > 18000 && dir < 36000)
					{
						m_Buttons |= ioPad::LLEFT;
					}
				}
			}
			else if (S.GetPOVCount() && S.GetButtonCount()+4<32)
			{
				// This maps POV directly to m_Buttons, and
				// doesn't offer the chance to remap.
				short dir = (short) S.GetPOV(0);
				if (dir == 0)
				{
					m_Buttons |= (1 << (S.GetButtonCount()));
				}
				else if (dir == 4500)
				{
					m_Buttons |= (1 << (S.GetButtonCount()));
					m_Buttons |= (1 << (S.GetButtonCount()+1));
				}
				else if (dir == 9000)
				{
					m_Buttons |= (1 << (S.GetButtonCount()+1));
				}
				else if (dir == 13500)
				{
					m_Buttons |= (1 << (S.GetButtonCount()+1));
					m_Buttons |= (1 << (S.GetButtonCount()+2));
				}
				else if (dir == 18000)
				{
					m_Buttons |= (1 << (S.GetButtonCount()+2));
				}
				else if (dir == 22500)
				{
					m_Buttons |= (1 << (S.GetButtonCount()+2));
					m_Buttons |= (1 << (S.GetButtonCount()+3));
				}
				else if (dir == 27000)
				{
					m_Buttons |= (1 << (S.GetButtonCount()+3));
				}
				else if (dir == 31500)
				{
					m_Buttons |= (1 << (S.GetButtonCount()+3));
					m_Buttons |= (1 << (S.GetButtonCount()));
				}
			}

			if (S.GetJoyType()==ioJoystick::TYPE_DUALJOYPAD || 
				S.GetJoyType()==ioJoystick::TYPE_DUAL_USB_FFJ_MP866)
			{
				short dir = (short) S.GetPOV(0);
				if (dir != -1)
				{
					if (dir < 9000 || dir > 27000)
					{
						m_Buttons |= ioPad::LUP;
					}
					else if (dir > 9000 && dir < 27000)
					{
						m_Buttons |= ioPad::LDOWN;
					}
					if (dir > 0 && dir < 18000)
					{
						m_Buttons |= ioPad::LRIGHT;
					}
					else if (dir > 18000)
					{
						m_Buttons |= ioPad::LLEFT;
					}
				}
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_TIGERGAME_XBOX_USB)
			{
				// digital
				if (S.GetAxis(7) > 0x7fff)
				{
					m_Buttons |= ioPad::L2;
					//m_AnalogButtons[analogL2] = S.GetAxis(7);
				}
				if (S.GetAxis(6) > 0x7fff)
				{
					m_Buttons |= ioPad::R2;
					//m_AnalogButtons[analogR2] = S.GetAxis(6);
				}
			}
			else if (S.GetJoyType()==ioJoystick::TYPE_XNA_GAMEPAD_XENON)
			{
				// digital
				if (S.GetAxis(2) > 0x7fff)
				{
					m_Buttons |= ioPad::L2;
				}
				if (S.GetAxis(2) < 0x7fff)
				{
					m_Buttons |= ioPad::R2;
				}
				// analog
				int value;
				// right
				value = 0x7fff - S.GetAxis(2);
				value = Max(value,0);
				m_AnalogButtons[ioPad::R2_INDEX] = (u8) (value >> 7);
				// left
				value = S.GetAxis(2) - 0x7fff;
				value = Clamp(value,0,0x7fff);
				m_AnalogButtons[ioPad::L2_INDEX] = (u8) (value >> 7);
			}
			else if (S.GetAxisCount() == 6)
			{	// last two axes are really the D-pad
				if (S.GetAxis(7) < 20000)
				{
					m_Buttons |= ioPad::LLEFT;
				}
				else if (S.GetAxis(7) > 40000)
				{
					m_Buttons |= ioPad::LRIGHT;
				}
				if (S.GetAxis(6) < 20000)
				{
					m_Buttons |= ioPad::LUP;
				}
				else if (S.GetAxis(6) > 40000)
				{
					m_Buttons |= ioPad::LDOWN;
				}
			}
		}

		int whichMap = (int) S.GetJoyType();

		// first map is direct pad, second is smart joypad:
		CompileTimeAssert(13==ioJoystick::NUM_TYPES);
		static unsigned map[ioJoystick::NUM_TYPES][16] =
		{ 
			// UNKNOWN
			{ioPad::RUP,	ioPad::RLEFT,	ioPad::RDOWN,	ioPad::RRIGHT,
			 ioPad::R1,		ioPad::R2,		ioPad::L1,		ioPad::L2,
			 ioPad::START,	ioPad::SELECT,	ioPad::R3,		ioPad::L3},

			// DIRECTPAD
			{ioPad::RUP,	ioPad::RLEFT,	ioPad::RDOWN,	ioPad::RRIGHT,
			 ioPad::R1,		ioPad::R2,		ioPad::L1,		ioPad::L2,
			 ioPad::START,	ioPad::SELECT,	ioPad::R3,		ioPad::L3},

			 // SMARTJOYPAD
			{ioPad::RUP,	ioPad::RRIGHT,	ioPad::RDOWN,	ioPad::RLEFT,
			 ioPad::L2,		ioPad::R2,		ioPad::L1,		ioPad::R1,
			 ioPad::SELECT,	ioPad::START,	ioPad::R3,		ioPad::L3,
			 ioPad::LUP,	ioPad::LRIGHT,	ioPad::LDOWN,	ioPad::LLEFT},

			 // TYPE_GAMEPADPRO
			{ioPad::RDOWN,	ioPad::RRIGHT,	ioPad::R2,		ioPad::RLEFT,
			ioPad::RUP,		ioPad::L2,		ioPad::L1,		ioPad::R1,
			ioPad::START,	0,				0,				0},

			// TYPE_XKPC2002
			{ioPad::RUP,	ioPad::RRIGHT,	ioPad::RDOWN,	ioPad::RLEFT,
			 ioPad::L2,		ioPad::R2,		ioPad::L1,		ioPad::R1,
			 ioPad::SELECT,	ioPad::L3,		ioPad::R3,		ioPad::START,
			 ioPad::LUP,	ioPad::LRIGHT,	ioPad::LDOWN,	ioPad::LLEFT},

			 // TYPE_DUALPAD
			{ioPad::RUP,	ioPad::RRIGHT,	ioPad::RDOWN,	ioPad::RLEFT,
			 ioPad::L2,		ioPad::R2,		ioPad::L1,		ioPad::R1,
			 ioPad::START,	ioPad::SELECT,	ioPad::L3,		ioPad::R3,
			 ioPad::LUP,	ioPad::LRIGHT,	ioPad::LDOWN,	ioPad::LLEFT},

			// TYPE_DUAL_USB_FFJ_MP866
			{ioPad::RUP,	ioPad::RRIGHT,	ioPad::RDOWN,	ioPad::RLEFT,
			 ioPad::L1,		ioPad::R1,		ioPad::L2,		ioPad::R2,
			 ioPad::START,	ioPad::SELECT,	ioPad::L3,		ioPad::R3,
			 ioPad::LUP,	ioPad::LRIGHT,	ioPad::LDOWN,	ioPad::LLEFT},

			// TYPE_KIKYXSERIES
			{ioPad::RUP,	ioPad::RRIGHT,	ioPad::RDOWN,	ioPad::RLEFT,
			 ioPad::L2,		ioPad::R2,		ioPad::L1,		ioPad::R1,
			 ioPad::START,	ioPad::SELECT,	ioPad::L3,		ioPad::R3,
			 ioPad::LUP,	ioPad::LRIGHT,	ioPad::LDOWN,	ioPad::LLEFT},

			// TYPE_LOGITECH_CORDLESS
			{ioPad::RDOWN,	ioPad::RLEFT,	ioPad::RUP,		ioPad::RRIGHT,
			ioPad::R1,		ioPad::R2,		ioPad::L1,		ioPad::L2,
			ioPad::START,	ioPad::SELECT,	ioPad::R3,		ioPad::L3,
			0,				0,				0,				0},

			// TYPE_PSX_TO_USB_CONVERTER
			{ioPad::RUP,	ioPad::RRIGHT,	ioPad::RDOWN,	ioPad::RLEFT,
			ioPad::L2,		ioPad::R2,		ioPad::L1,		ioPad::R1,
			ioPad::SELECT,	ioPad::START,	ioPad::L3,		ioPad::R3,
			ioPad::LUP,		ioPad::LRIGHT,	ioPad::LDOWN,	ioPad::LLEFT},

			// TYPE_XBCD_XBOX_USB
			{ioPad::RDOWN,	ioPad::RRIGHT,	ioPad::RLEFT,	ioPad::RUP,
			ioPad::R1,		ioPad::L1,		ioPad::START,	ioPad::SELECT,
			ioPad::L3,		ioPad::R3,		ioPad::L2,		ioPad::R2,
			ioPad::LUP,		ioPad::LRIGHT,	ioPad::LDOWN,	ioPad::LLEFT},

			// TYPE_TIGERGAME_XBOX_USB
			{ioPad::RDOWN,	ioPad::RRIGHT,	ioPad::RLEFT,	ioPad::RUP,
			ioPad::R1,		ioPad::L1,		ioPad::L3,		ioPad::R3,
			ioPad::START,	ioPad::SELECT,	ioPad::L2,		ioPad::R2,
			ioPad::LUP,		ioPad::LRIGHT,	ioPad::LDOWN,	ioPad::LLEFT},

			// TYPE_XNA_GAMEPAD_XENON
			{ioPad::RDOWN,	ioPad::RRIGHT,	ioPad::RLEFT,	ioPad::RUP,
			ioPad::L1,		ioPad::R1,		ioPad::SELECT,	ioPad::START,
			ioPad::L3,		ioPad::R3,		0,				0,
			0,				0,				0,				0},
		};

		for (int i=0; i<16; i++)
		{
			if (S.GetButtons() & (1<<i))
			{
				m_Buttons |= map[whichMap][i];
			}
		}

#if __BANK
		for ( int i = 0; i < 4; ++i )
		{
			if ( m_bankAxis[i] != 0x80 )
			{
				m_Axis[i] = m_bankAxis[i];
			}
		}

		m_Buttons |= m_bankButtons;

		if ( m_bankButtons & L2 )
		{
			m_AnalogButtons[L2_INDEX] = m_bankAnalogButtons[L2_INDEX];
		}

		if ( m_bankButtons & R2 )
		{
			m_AnalogButtons[R2_INDEX] = m_bankAnalogButtons[R2_INDEX];
		}

		ClearBankInputs();
#endif
	}

	UpdateDebug();
}



void ioPad::BeginAll() {
	for (int i=0;i<MAX_PADS;i++) {
		sm_Pads[i].m_PadIndex = i;
		sm_Pads[i].Begin(i);
	}
}



void ioPad::EndAll() {
	for (int i=MAX_PADS-1;i>=0;i--) {
		sm_Pads[i].End();
	}
}



void ioPad::UpdateAll(bool ignoreInput, bool UNUSED_PARAM(checkForNewDevice)) {
	for (int i=0;i<MAX_PADS;i++)
	{
		sm_Pads[i].Update(ignoreInput);
	}
}

void ioPad::CheckForNewlyPluggedInDevices() {
}

void ioPad::ReattachAll() {
}

void ioPad::StopShakingNow() {
}


#if __BANK

void ioPad::AddPadWidgets()
{
	bkBank *pBank = BANKMGR.FindBank( "rage - Pad" );
	if ( pBank == NULL )
	{
		pBank = &BANKMGR.CreateBank( "rage - Pad" );
	}

	char name[16];
	sprintf( name, "Pad %d", m_PadIndex );

	if ( bkRemotePacket::IsConnectedToRag() )
	{
		ioWidgetPad* pWidget = rage_new ioWidgetPad( name, m_PadIndex, ioWidgetPad::WIN32_PLATFORM );
		pBank->AddWidget( *pWidget );
	}
	else
	{
		pBank->PushGroup( name );
		{
			pBank->PushGroup( "Shoulder Buttons" );
			{
				pBank->AddButton( "Left Button", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L1 ) );
				pBank->AddButton( "Right Button", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R1 ) );
				pBank->AddButton( "Left Trigger", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L2 ) );
				pBank->AddButton( "Right Trigger", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R2 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Left Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData) LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LDOWN ) );
				pBank->AddButton( "Thumb", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Pad" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::ButtonPressedCB ), this,(CallbackData)LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LDOWN ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Right Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData)RRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "Thumb", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Buttons" );
			{
				pBank->AddButton( "A", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "B", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RRIGHT ) );
				pBank->AddButton( "X", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "Y", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RUP ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Other" );
			{
				pBank->AddButton( "Back", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)SELECT ) );
				pBank->AddButton( "Start", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)START ) );
			}
			pBank->PopGroup();
		}
		pBank->PopGroup();
	}
}

void ioPad::AnalogStickPressedCB( CallbackData data )
{
	int button = reinterpret_cast<int>( data );

	switch ( button )
	{
	case LUP:
		m_bankAxis[1] = 0x0;
		break;
	case LLEFT:
		m_bankAxis[0] = 0x0;
		break;
	case LRIGHT:
		m_bankAxis[0] = 0xff;
		break;
	case LDOWN:
		m_bankAxis[1] = 0xff;
		break;
	case RUP:
		m_bankAxis[3] = 0x0;
		break;
	case RLEFT:
		m_bankAxis[2] = 0x0;
		break;
	case RRIGHT:
		m_bankAxis[2] = 0xff;
		break;
	case RDOWN:
		m_bankAxis[3] = 0xff;
		break;
	}
}

void ioPad::ButtonPressedCB( CallbackData data )
{
	int button = reinterpret_cast<int>( data );

	m_bankButtons |= button;

	if ( button == L2 )
	{
		m_bankAnalogButtons[L2_INDEX] = 0xff;
	}
	else if ( button == R2 )
	{
		m_bankAnalogButtons[R2_INDEX] = 0xff;
	}
}

#endif

#endif	// __WIN32PC
