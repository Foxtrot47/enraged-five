//
// input/winpriv.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

//DOM-IGNORE-BEGIN

#if __WIN32PC

#include "winpriv.h"
#include "keys.h"
#include "input.h"
#include "mouse.h"
#include "eventq.h"
#include "grcore/device.h"
#include "grcore/channel.h"
#include "system/timer.h"

#include <dbt.h>

LPDIRECTINPUTW lpDI;

#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")

#include "mouse.h"

extern int ioMouse_dZ_Accum;
extern struct IDirectInputDeviceW *g_keybDev;

using namespace rage;

/* static int TranslateButtons(int fkey) {
	int flags = 0;
	if (fkey & MK_LBUTTON) flags |= ioMouse::MOUSE_LEFT;
	if (fkey & MK_RBUTTON) flags |= ioMouse::MOUSE_RIGHT;
	if (fkey & MK_MBUTTON) flags |= ioMouse::MOUSE_MIDDLE;
	return flags;
} */

//ADDED FOR GFWL
//InputWindowProcCallback sm_InputWindowProcCallback = 0;
LRESULT (CALLBACK *sm_InputWindowProcCallback)(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) = 0;

static int TranslateKey(unsigned virtualCode, unsigned scanCode, bool extended)
{
	// Do not translate pause, it returns KEY_NUMLOCK but does not set the extended bit!
	if(virtualCode == KEY_PAUSE)
	{
		return KEY_PAUSE;
	}

	virtualCode = ioKeyboard::GetRageKeyCode(scanCode);
	switch(virtualCode)
	{
	case VK_SHIFT:
		virtualCode = MapVirtualKey(scanCode, MAPVK_VSC_TO_VK_EX);
		break;
		
	case VK_CONTROL:
		virtualCode = (extended) ? KEY_RCONTROL : KEY_LCONTROL;
		break;

	case VK_MENU:
		virtualCode = (extended) ? KEY_RMENU : KEY_LMENU;
		break;

	case VK_RETURN:
		virtualCode = (extended) ? KEY_NUMPADENTER: KEY_RETURN;
		break;

	case VK_INSERT:
		if(!extended) virtualCode = KEY_NUMPAD0;
		break;

	case VK_END:
		if(!extended) virtualCode = KEY_NUMPAD1;
		break;

	case VK_DOWN:
		if(!extended) virtualCode = KEY_NUMPAD2;
		break;

	case VK_NEXT:
		if(!extended) virtualCode = KEY_NUMPAD3;
		break;

	case VK_LEFT:
		if(!extended) virtualCode = KEY_NUMPAD4;
		break;

	case VK_CLEAR:
		if(!extended) virtualCode = KEY_NUMPAD5;
		break;

	case VK_RIGHT:
		if(!extended) virtualCode = KEY_NUMPAD6;
		break;

	case VK_HOME:
		if(!extended) virtualCode = KEY_NUMPAD7;
		break;

	case VK_UP:
		if(!extended) virtualCode = KEY_NUMPAD8;
		break;

	case VK_PRIOR:
		if(!extended) virtualCode = KEY_NUMPAD9;
		break;

	case VK_DELETE:
		if(!extended) virtualCode = KEY_DECIMAL;
		break;

	case VK_MULTIPLY:
		if(extended) virtualCode = KEY_SNAPSHOT;
		break;

	case VK_NUMLOCK:
		if(!extended) virtualCode = KEY_PAUSE;
		break;

	default:
		break;
	}

	return virtualCode;
}

#define QM(name) \
	ioMouse::m_wX = ((int)(short)LOWORD(lParam)); \
	ioMouse::m_wY = ((int)(short)HIWORD(lParam)); 


const int maxHandlers = 4;
static UINT msg_array[maxHandlers];
static winDispatchable *dispatch_array[maxHandlers];
static int handlerCount;

enum { EXTENDED_BIT = 0x1000000 };

winDispatchable::winDispatchable(UINT _msg) {
	Assert(handlerCount != maxHandlers && "Out of winDispatchables!");
	msg_array[handlerCount] = _msg;
	dispatch_array[handlerCount] = this;
	++handlerCount;
}

winDispatchable::~winDispatchable() {
	AssertVerify(dispatch_array[--handlerCount] == this);
}


LRESULT CALLBACK rage::InputWindowProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) 
{
	//ADDED FOR GFWL
	if(sm_InputWindowProcCallback)
	{
		if((*sm_InputWindowProcCallback)(hwnd, msg, wParam, lParam))
		{
			return 0;
		}
	}

	if (!GRCDEVICE.IsCreated() || 
		GRCDEVICE.IsPaused() ||  
		GRCDEVICE.IsInReset() ||  
		GRCDEVICE.IsInsideDeviceChange() ||
		GRCDEVICE.IsMinimized() ||
		GRCDEVICE.IsInSizeMove() ||
		GRCDEVICE.IsReleasing() ||
		GRCDEVICE.IsInReset())
	{
		return DefWindowProcW(hwnd,msg,wParam,lParam);
	}

	if(ioKeyboard::IsIgnoringInput())
	{
		switch(msg)
		{
			case WM_IME_SETCONTEXT:
			case WM_IME_STARTCOMPOSITION:
			case WM_IME_COMPOSITION:
			case WM_IME_ENDCOMPOSITION:
			case WM_IME_NOTIFY:
				return DefWindowProcW(hwnd,msg,wParam,lParam);
		}
	}

	RECT rect;
	GetClientRect(hwnd,&rect);

	switch (msg) {
	case WM_CREATE:
	case WM_INPUTLANGCHANGE:
		ioKeyboard::SetKeyboardLayoutFromThreadLayout();
		return 0;
	case WM_ACTIVATEAPP:
		if ((BOOL) wParam) {
			char buf;
			ioKeyboard::GetBufferedInput(&buf,0);
		}
		return DefWindowProcW(hwnd,msg,wParam,lParam);
	case WM_INPUT:
		{
			ioKeyboard::SetKeyboardLayoutForThread();
			// Taken from the Microsoft's documentation website.
			UINT dwSize;

			GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize, 
				sizeof(RAWINPUTHEADER));

			// According to windows documentation the size can vary. All documentation allocate the memory on the heap.
			LPBYTE lpb = Alloca(BYTE, dwSize);
			if (lpb == NULL) 
			{
				return 0;
			} 

			if (Verifyf(GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER)) == dwSize, "GetRawInputData does not return correct size!" ))
			{
				RAWINPUT* raw = (RAWINPUT*)lpb;
				if (raw->header.dwType == RIM_TYPEKEYBOARD) 
				{
					const RAWKEYBOARD& keyboard = raw->data.keyboard;

					// Ignore key 255 as it is sent as a press sequence and is not a real buttons.
					if(keyboard.VKey != 255)
					{
						u32 scanCode = keyboard.MakeCode;
						// RI_KEY_E1 apparently means the key is part of an escape sequence so we have to correct the scan code.
						if((keyboard.Flags & RI_KEY_E1) != 0)
						{
							scanCode = ioKeyboard::GetKeyScanCode(keyboard.VKey);
						}

						bool extended = (keyboard.Flags & RI_KEY_E0) != 0;
						int key = TranslateKey(keyboard.VKey, scanCode, extended);
						ioKeyboard::SetKey(key, (keyboard.Flags & RI_KEY_BREAK) == 0);
					}
				}
				else if (ioMouse::GetMouseType() == ioMouse::RAW_MOUSE && raw->header.dwType == RIM_TYPEMOUSE)
				{
					const RAWMOUSE& mouse = raw->data.mouse;
					// add to accumulate.
					ioMouse::s_RawInput.Lock();												// LOCK
					int renderIndex = (ioMouse::m_UpdateIndex+1) & 0x1;

					// Accumulate the new entry.
					ioMouse::m_wdX[renderIndex] += static_cast<int>(mouse.lLastX);
					ioMouse::m_wdY[renderIndex] += static_cast<int>(mouse.lLastY);
					ioMouse::s_RawInput.Unlock();											// UNLOCK

					if((mouse.usButtonFlags & RI_MOUSE_WHEEL))
					{
						// We cast to short to get the sign back as the value is a short stored in an unsigned value.
						ioMouse_dZ_Accum += static_cast<short>(mouse.usButtonData) / WHEEL_DELTA;
					}

					if((mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_UP))
					{
						if(ioMouse::IsLeftRightButtonSwapped())
						{
							ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_RIGHT;
						}
						else
						{
							ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_LEFT;
						}
					}
					else if((mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN))
					{
						if(ioMouse::IsLeftRightButtonSwapped())
						{
							ioMouse::m_WindowButtons |= ioMouse::MOUSE_RIGHT;
						}
						else
						{
							ioMouse::m_WindowButtons |= ioMouse::MOUSE_LEFT;
						}
					}

					if((mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP))
					{
						if(ioMouse::IsLeftRightButtonSwapped())
						{
							ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_LEFT;
						}
						else
						{
							ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_RIGHT;
						}
					}
					else if((mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN))
					{
						if(ioMouse::IsLeftRightButtonSwapped())
						{
							ioMouse::m_WindowButtons |= ioMouse::MOUSE_LEFT;
						}
						else
						{
							ioMouse::m_WindowButtons |= ioMouse::MOUSE_RIGHT;
						}
					}

					if((mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_UP))
					{
						ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_MIDDLE;
					}
					else if((mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_DOWN))
					{
						ioMouse::m_WindowButtons |= ioMouse::MOUSE_MIDDLE;
					}

					if((mouse.usButtonFlags & RI_MOUSE_BUTTON_4_UP))
					{
						ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_EXTRABTN1;
					}
					else if((mouse.usButtonFlags & RI_MOUSE_BUTTON_4_DOWN))
					{
						ioMouse::m_WindowButtons |= ioMouse::MOUSE_EXTRABTN1;
					}

					if((mouse.usButtonFlags & RI_MOUSE_BUTTON_5_UP))
					{
						ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_EXTRABTN2;
					}
					else if((mouse.usButtonFlags & RI_MOUSE_BUTTON_5_DOWN))
					{
						ioMouse::m_WindowButtons |= ioMouse::MOUSE_EXTRABTN2;
					}
				}
			}
		}
		return 0;
	case WM_MOUSEMOVE:
		QM(mouseMove);
		return 0;
	case 0x20A: /*WM_MOUSEWHEEL*/
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			ioMouse_dZ_Accum += ((short) HIWORD(wParam) / 120);
		}
		return 0;
	case 0x020B: // WM_XBUTTONDOWN:
	case 0x020D: // WM_XBUTTONDBLCLK:
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			// The high-order word indicates which button was pressed
			if (HIWORD(wParam)==1) // XBUTTON1
			{
				ioMouse::m_WindowButtons |= ioMouse::MOUSE_EXTRABTN1;
				QM(mouseX1Down);
			}
			else if (HIWORD(wParam)==2) // XBUTTON2
			{
				ioMouse::m_WindowButtons |= ioMouse::MOUSE_EXTRABTN2;
				QM(mouseX2Down);
			}
		}
		ioMouse::Capture();
		return true; // return true if you process this message (unlike with other mouse functions)
	case 0x020C: // WM_XBUTTONUP:
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			if (HIWORD(wParam)==1) // XBUTTON1
			{
				ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_EXTRABTN1;
				QM(mouseX1Up);
			}
			else if (HIWORD(wParam)==2) // XBUTTON2
			{
				ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_EXTRABTN2;
				QM(mouseX2Up);
			}
		}
		ioMouse::ReleaseCapture();
		return true; // return true if you process this message (unlike with other mouse functions)
	case WM_LBUTTONDBLCLK:
	case WM_LBUTTONDOWN:
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			ioMouse::m_WindowButtons |= ioMouse::MOUSE_LEFT;
			QM(mouseLeftDown);
		}
		ioMouse::Capture();
		return 0;
	case WM_LBUTTONUP:
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_LEFT;
			QM(mouseLeftUp);
		}
		ioMouse::ReleaseCapture();
		return 0;
	case WM_RBUTTONDBLCLK:
	case WM_RBUTTONDOWN:
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			ioMouse::m_WindowButtons |= ioMouse::MOUSE_RIGHT;
			QM(mouseRightDown);
		}
		ioMouse::Capture();
		return 0;
	case WM_RBUTTONUP:
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_RIGHT;
			QM(mouseRightUp);
		}
		ioMouse::ReleaseCapture();
		return 0;
	case WM_MBUTTONDBLCLK:
	case WM_MBUTTONDOWN:
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			ioMouse::m_WindowButtons |= ioMouse::MOUSE_MIDDLE;
			QM(mouseMiddleDown);
			ioMouse::Capture();
		}
		return 0;
	case WM_MBUTTONUP:
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_MIDDLE;
			QM(mouseMiddleUp);

		}
		ioMouse::ReleaseCapture();
		return 0;
	case WM_CAPTURECHANGED:
		if(ioMouse::GetMouseType() != ioMouse::RAW_MOUSE)
		{
			ioMouse::m_WindowButtons = 0;
		}
		ioMouse::CaptureLost();
		return 0;
	case WM_SYSKEYDOWN:
#if DIRECT_CONTROL_OF_ALT_ENTER
		if (wParam == VK_RETURN)
		{
			static u32 fullScreenToggleLockout = 0;
			if (sysTimer::GetSystemMsTime() > fullScreenToggleLockout)
			{
				pcdDisplayf("ALT-ENTER was pressed");
				GRCDEVICE.ToggleFullscreen();
				fullScreenToggleLockout = sysTimer::GetSystemMsTime() + 2000;		// allow toggling again in 2 seconds
			}
		}
#endif
		if (wParam == VK_F4)	// ALT+F4
			SendMessageW(hwnd,WM_CLOSE,0,0);
	case WM_KEYDOWN:
		if (!ioKeyboard::IsUsingRawInput())
		{
			ioKeyboard::SetKeyboardLayoutForThread();
			int key = TranslateKey( (int)wParam,
				((unsigned)lParam >> ioKeyboard::LEGACY_KEY_CODE_BIT_OFFSET) & 0xFF,
				((int)lParam & EXTENDED_BIT) == EXTENDED_BIT);

			// DirectInput does not support the pause or print screen keys so use windows messages.
			if (!g_keybDev || key == KEY_PAUSE || key == KEY_SNAPSHOT)
			{
				ioKeyboard::SetKey(key,true);
			}

			ioEventQueue::Queue(ioEvent::IO_KEY_DOWN,0,0,key);
		}
		return 0;

#if IME_TEXT_INPUT
	case WM_IME_SETCONTEXT:
		// Actively disable the IME windows as they will kick us out of fullscreen. We will render these ourselves.
		lParam &= ~ISC_SHOWUIALL;
		return ::DefWindowProcW(hwnd, msg, wParam, lParam);

	case WM_IME_STARTCOMPOSITION:
		ioKeyboard::ImeStartTextInput();
		return 0;


	case WM_IME_COMPOSITION:
		if (lParam & GCS_RESULTSTR)
		{
			ioKeyboard::ImeRetrieveTextResult();
		}
		
		if(lParam & GCS_COMPSTR)
		{
			ioKeyboard::ImeUpdateCompositionText();
		}
		return 0;

	case WM_IME_ENDCOMPOSITION:
		ioKeyboard::ImeStopTextInput();
		return 0;

	case WM_IME_NOTIFY:
		switch(wParam)
		{
		case IMN_OPENCANDIDATE:
		case IMN_CHANGECANDIDATE:
			ioKeyboard::ImeUpdateCandidateTexts();
			return 0;

		default:
			return ::DefWindowProcW(hwnd, msg, wParam, lParam);
		}
#endif // IME_TEXT_INPUT

	case WM_CHAR:
		{
			int key = TranslateKey( (int)wParam,
				((unsigned)lParam >> ioKeyboard::LEGACY_KEY_CODE_BIT_OFFSET) & 0xFF, // bits 16-23 - scan code
				((int)lParam & EXTENDED_BIT) == EXTENDED_BIT);
			ioKeyboard::SetTextCharPressed(static_cast<char16>(wParam), (ioMapperParameter)key);
			// Be careful with casting here or we'll break DBCS
			ioEventQueue::Queue(ioEvent::IO_KEY_CHAR,0,0,(int)wParam);
		}
		return 0;
	case WM_KEYUP:
	case WM_SYSKEYUP:
		if (!ioKeyboard::IsUsingRawInput())
		{
			ioKeyboard::SetKeyboardLayoutForThread();
			int key = TranslateKey( (int)wParam,
				((unsigned)lParam >> ioKeyboard::LEGACY_KEY_CODE_BIT_OFFSET) & 0xFF,
				((int)lParam & EXTENDED_BIT) == EXTENDED_BIT);

			// DirectInput does not support the pause or print screen keys so use windows messages.
			if (!g_keybDev || key == KEY_PAUSE || key == KEY_SNAPSHOT)
			{
				ioKeyboard::SetKey(key,false);
			}

			ioEventQueue::Queue(ioEvent::IO_KEY_UP,0,0,key);
		}
		return 0;

	case WM_DEVICECHANGE:
		if (wParam == DBT_DEVNODES_CHANGED)
		{
			ioPad::CheckForNewlyPluggedInDevices();
		}
		return DefWindowProcW(hwnd,msg,wParam,lParam);

	default: {
			for (int i=0; i<handlerCount; i++)
				if (msg_array[i] == msg)
					dispatch_array[i]->WindowProc(hwnd,msg,wParam,lParam);
			return DefWindowProcW(hwnd,msg,wParam,lParam);
		}
	}
}


void rage::diInit() {
	if (!lpDI) {
#if DIRECTINPUT_VERSION > 0x700
		ditry(DirectInput8Create(GetModuleHandle(0),DIRECTINPUT_VERSION,IID_IDirectInput8W,(void**)&lpDI,NULL));
#else
		ditry(DirectInputCreate(GetModuleHandle(0),DIRECTINPUT_VERSION,&lpDI,NULL));
#endif
	}
}

#endif

//DOM-IGNORE-END

#undef QM
