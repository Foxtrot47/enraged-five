// 
// input/remote_input.cpp 
// 
// Copyright (C) 1999-2020 Rockstar Games.  All Rights Reserved. 
// 

#include "remote_input.h"

#if REMOTE_INPUT_SUPPORT

#include "input/eventq.h"
#include "input/keyboard.h"
#include "input/mouse.h"
#include "system/memory.h"
#include "system/param.h"

#if RSG_ORBIS
#include <libnet/nettypes.h>
#include <libnet/in.h>
#include <libnet/inet.h>
#include <libnet/socket.h>
#include <pad.h>
#elif RSG_DURANGO
#include <winsock2.h>
#include <xdk.h>
#elif RSG_PC
#pragma warning(push)
#pragma warning(disable: 4668)
#include <winsock2.h>
#pragma warning(pop)
#include <XInput.h>
#if __D3D11_1
#pragma comment(lib,"XInput9_1_0.lib")
#else
#pragma comment(lib,"xinput.lib")
#endif // !__D3D11_1
#endif // RSG_PC

using namespace rage;

PARAM(remoteInput, "[RAGE] Listen to packets from a RemoteInputServer");
PARAM(remoteXInput, "[RAGE] [Deprecated] Listen to packets from xinputserver");

#define REMOTE_INPUT_DEFAULT_PORT 27123

ioRemoteInput::ioRemoteInput()
	: m_port(REMOTE_INPUT_DEFAULT_PORT)
	, m_socket(fiHandleInvalid)
	, m_clientAddress(0)
	, m_previousClientAddress(0)
{
	m_remotePads.SetCount(REMOTE_INPUT_MAX_PADS);
	m_remoteOutputPads.SetCount(REMOTE_INPUT_MAX_PADS);

	Reset();
}

ioRemoteInput::~ioRemoteInput()
{
	Reset();
}

ioRemoteInput& ioRemoteInput::Get()
{
	return ioRemoteInput_Inst::InstanceRef();
}

void ioRemoteInput::Begin()
{
	if (IsEnabled() == true)
	{
		return;
	}

	if (!PARAM_remoteInput.Get() && !PARAM_remoteXInput.Get())
	{
		return;
	}

	USE_DEBUG_MEMORY();
	
	ioRemoteInput_Inst::Instantiate();

	ioRemoteInput& remoteInput = Get();

	if (PARAM_remoteInput.Get() == true)
	{
		PARAM_remoteInput.Get(remoteInput.m_port);
	}
	else if (PARAM_remoteXInput.Get() == true)
	{
		PARAM_remoteXInput.Get(remoteInput.m_port);
	}	
		
	remoteInput.Connect();
}

void ioRemoteInput::Update()
{
	if (IsEnabled() == false)
	{
		return;
	}

	Get().Update_Internal();
}

void ioRemoteInput::End()
{
	if (IsEnabled() == false)
	{
		return;
	}
	
	Get().Disconnect();

	USE_DEBUG_MEMORY();
	
	ioRemoteInput_Inst::Destroy();
}

bool ioRemoteInput::IsEnabled()
{
	return ioRemoteInput_Inst::IsInstantiated();
}

bool ioRemoteInput::IsPadConnected(int index)
{
	if (IsEnabled() == false)
	{
		return false;
	}

	return Get().m_remotePads[index].remoteUpdateCount > 0;
}

bool ioRemoteInput::IsMouseConnectAndHasPendingInput()
{
	if (IsEnabled() == false)
	{
		return false;
	}

	const ioRemoteInput& remoteInput = Get();

	if (remoteInput.m_remoteMouse.remoteUpdateCount <= 0)
	{
		return false;
	}

	return remoteInput.m_remoteMouse.buttons != 0;
}

void ioRemoteInput::SetPadVibration(int index, u16 heavyMotor, u16 lightMotor)
{
	if (IsEnabled() == false)
	{
		return;
	}
	
	if (index < 0 || index > REMOTE_INPUT_MAX_PADS)
	{
		return;
	}

	ioRemoteInput& remoteInput = Get();
	
	ioRemotePad_Vibration& vibration = remoteInput.m_remoteOutputPads[index].vibration;

	bool shouldUpdate = vibration.leftMotor != heavyMotor || vibration.rightMotor != lightMotor || heavyMotor != 0 || lightMotor != 0;
	
	vibration.leftMotor		= heavyMotor;
	vibration.rightMotor	= lightMotor;

	remoteInput.m_hasPadOutput[index] |= shouldUpdate;
}

void ioRemoteInput::SetPadColor(int index, u8 red, u8 green, u8 blue)
{
	if (IsEnabled() == false)
	{
		return;
	}
	
	if (index < 0 || index > REMOTE_INPUT_MAX_PADS)
	{
		return;
	}

	ioRemoteInput& remoteInput = Get();
	
	ioRemotePad_SceColor& color = remoteInput.m_remoteOutputPads[index].color;

	bool shouldUpdate = color.red != red || color.green != green || color.blue != blue;
	
	color.red		= red;
	color.green		= green;
	color.blue		= blue;
	color.update	= true;

	remoteInput.m_hasPadOutput[index] |= shouldUpdate;
}

void ioRemoteInput::ResetPadColor(int index)
{
	if (IsEnabled() == false)
	{
		return;
	}
	
	if (index < 0 || index > REMOTE_INPUT_MAX_PADS)
	{
		return;
	}

	ioRemoteInput& remoteInput = Get();

	remoteInput.m_remoteOutputPads[index].color.reset = true;
	remoteInput.m_hasPadOutput[index] = true;
}

#if RSG_ORBIS
void ioRemoteInput::UpdatePad(int index, ScePadData& data)
{
	if (index < 0 || index > REMOTE_INPUT_MAX_PADS)
	{
		return;
	}
	
	ioRemotePad& rp = m_remotePads[index];

	data.buttons = 0;
	data.leftStick.x	= (rp.leftX + 32768) >> 8;
	data.leftStick.y	= (-rp.leftY + 32767) >> 8;
	data.rightStick.x	= (rp.rightX + 32768) >> 8;
	data.rightStick.y	= (-rp.rightY + 32767) >> 8;
	data.analogButtons.l2 = rp.leftTrigger;
	data.analogButtons.r2 = rp.rightTrigger;
	if (rp.buttons & ioRemotePad::A) data.buttons |= SCE_PAD_BUTTON_CROSS;
	if (rp.buttons & ioRemotePad::B) data.buttons |= SCE_PAD_BUTTON_CIRCLE;
	if (rp.buttons & ioRemotePad::X) data.buttons |= SCE_PAD_BUTTON_SQUARE;
	if (rp.buttons & ioRemotePad::Y) data.buttons |= SCE_PAD_BUTTON_TRIANGLE;
	if (rp.buttons & ioRemotePad::LEFT_SHOULDER) data.buttons |= SCE_PAD_BUTTON_L1;
	if (rp.buttons & ioRemotePad::RIGHT_SHOULDER) data.buttons |= SCE_PAD_BUTTON_R1;
	if (rp.buttons & ioRemotePad::LEFT_THUMB) data.buttons |= SCE_PAD_BUTTON_L3;
	if (rp.buttons & ioRemotePad::RIGHT_THUMB) data.buttons |= SCE_PAD_BUTTON_R3;
	if (rp.buttons & ioRemotePad::DPAD_DOWN) data.buttons |= SCE_PAD_BUTTON_DOWN;
	if (rp.buttons & ioRemotePad::DPAD_RIGHT) data.buttons |= SCE_PAD_BUTTON_RIGHT;
	if (rp.buttons & ioRemotePad::DPAD_LEFT) data.buttons |= SCE_PAD_BUTTON_LEFT;
	if (rp.buttons & ioRemotePad::DPAD_UP) data.buttons |= SCE_PAD_BUTTON_UP;
	if (rp.buttons & ioRemotePad::START) data.buttons |= SCE_PAD_BUTTON_OPTIONS;
	if (rp.buttons & ioRemotePad::BACK) data.buttons |= SCE_PAD_BUTTON_TOUCH_PAD;
	if (rp.leftTrigger > 50) data.buttons |= SCE_PAD_BUTTON_L2;
	if (rp.rightTrigger > 50) data.buttons |= SCE_PAD_BUTTON_R2;

	data.touchData.touchNum = 0;
	for (int i = 0; i < REMOTE_INPUT_MAX_TOUCH_INPUT; ++i)
	{
		ioRemotePad_TouchInput& touchInput = rp.sce_TouchInput[i];
		if (touchInput.hasData == false)
		{
			continue;
		}
		
		ScePadTouch& sceTouchData = data.touchData.touch[i];
		
		sceTouchData.x	= touchInput.x;
		sceTouchData.y	= touchInput.y;
		sceTouchData.id = touchInput.id;
		
		data.touchData.touchNum++;
	}
	
	data.connected = true;
}
#endif //RSG_ORBIS

#if RSG_PC
void ioRemoteInput::UpdatePad(int index, XINPUT_STATE& state)
{
	if (index < 0 || index > REMOTE_INPUT_MAX_PADS)
	{
		return;
	}
	
	ioRemotePad& rp = m_remotePads[index];

#if defined(_MSC_VER) && (_MSC_VER >= 1700)
	static_assert(sizeof(XINPUT_STATE) == sizeof(ioRemotePadBase), "ioRemotePad doesn't match the size of XINPUT_STATE, fix this code!");
#endif // defined(_MSC_VER) && (_MSC_VER >= 1700)

	sysMemCpy(&state, &rp, sizeof(XINPUT_STATE));
}
#endif //RSG_PC

void ioRemoteInput::UpdateMouseButtons()
{
	ioMouse::m_Buttons = m_remoteMouse.buttons;
}

void ioRemoteInput::Connect()
{
#if RSG_ORBIS
	m_socket = (fiHandle)sceNetSocket("Remote Input Socket", SCE_NET_AF_INET, SCE_NET_SOCK_DGRAM, SCE_NET_IPPROTO_UDP);
	if (!fiIsValidHandle(m_socket))
	{
		Quitf("Remote Input socket failed");
	}

	SceNetSockaddrIn addr;
	addr.sin_len	= sizeof(addr);
	addr.sin_family = SCE_NET_AF_INET;
	addr.sin_port	= sceNetHtons((u16)m_port);
	addr.sin_addr.s_addr = SCE_NET_INADDR_ANY;
	
	if (sceNetBind((SceNetId)m_socket, (SceNetSockaddr*)&addr, sizeof(addr)) < 0)
	{
		Quitf("Remote Input bind failed");
	}
#elif RSG_DURANGO || RSG_PC
	m_socket = (fiHandle)socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (!fiIsValidHandle(m_socket))
	{
		Quitf("Remote Input socket failed");
	}

	sockaddr_in addr = { 0 };
	addr.sin_family = AF_INET;
	addr.sin_port	= htons((u16)m_port);
	addr.sin_addr.s_addr = INADDR_ANY;
	
	u_long nonblocking = 1;
	if (ioctlsocket((SOCKET)m_socket, FIONBIO, &nonblocking) < 0)
	{
		Quitf("Remote Input non-blocking setup failed %d", WSAGetLastError());
	}
	
	if (bind((SOCKET)m_socket, (sockaddr*)&addr, sizeof(addr)) < 0)
	{
		Quitf("Remote Input bind failed %d",WSAGetLastError());
	}
#endif //RSG_DURANGO || RSG_PC
}

void ioRemoteInput::Disconnect()
{
	Reset();
	
	if (fiIsValidHandle(m_socket))
	{
#if RSG_ORBIS
		sceNetShutdown((SceNetId)m_socket, SCE_NET_SHUT_RDWR);
		sceNetSocketClose((SceNetId)m_socket);	
#elif RSG_DURANGO || RSG_PC
		shutdown((SOCKET)m_socket, SD_BOTH);
		closesocket((SOCKET)m_socket);
#endif // RSG_DURANGO || RSG_PC

		m_socket = fiHandleInvalid;
	}
}

void ioRemoteInput::Update_Internal()
{
	if (PARAM_remoteInput.Get() == true)
	{
		Update_Input();
	}
	else if (PARAM_remoteXInput.Get() == true)
	{
		Update_XInput();
	}	
}

void ioRemoteInput::Update_Input()
{	
	ioRemoteInputHeader header;
	while (ReceiveData(&header, sizeof(ioRemoteInputHeaderBase), sizeof(ioRemoteInputHeader), true) == true)
	{
		if (header.type == ioRemoteType::IRT_Pad)
		{
			if (header.typeDataSize < sizeof(ioRemotePadBase))
			{
				return;
			}

			int maxSize = sizeof(ioRemotePadBase);

			switch (header.version)
			{
			case REMOTE_INPUT_VERSION_2:
				maxSize = sizeof(ioRemotePad_V2);
				break;
			}
				
			ioRemotePad pad;
			sysMemSet(&pad, 0, sizeof(ioRemotePad));
			
			if (ReceiveData(&pad, header.typeDataSize, maxSize) == true)
			{
				UpdateRemotePad(pad);
			}
		}
		else if (header.type == ioRemoteType::IRT_Mouse)
		{
			if (header.typeDataSize < sizeof(ioRemoteMouseBase))
			{
				return;
			}

			ioRemoteMouse mouse;
			sysMemSet(&mouse, 0, sizeof(ioRemoteMouse));

			if (ReceiveData(&mouse, header.typeDataSize, sizeof(ioRemoteMouse)) == true)
			{
				UpdateRemoteMouse(mouse);
			}
		}
		else if (header.type == ioRemoteType::IRT_Keyboard)
		{
			if (header.typeDataSize < sizeof(ioRemoteKeyboardBase))
			{
				return;
			}

			ioRemoteKeyboard keyboard;
			sysMemSet(&keyboard, 0, sizeof(ioRemoteKeyboard));
			
			if (ReceiveData(&keyboard, header.typeDataSize, sizeof(ioRemoteKeyboard)) == true)
			{
				UpdateRemoteKeyboard(keyboard);
			}
		}
	}

	ProcessOutput();
}

void ioRemoteInput::Update_XInput()
{
	ioRemotePad pad;
	sysMemSet(&pad, 0, sizeof(ioRemotePad));

	while (ReceiveData(&pad, sizeof(ioRemotePadBase), sizeof(ioRemotePadBase)) == true)
	{
		UpdateRemotePad(pad);
	}
}

void ioRemoteInput::ProcessOutput()
{
	if (m_previousClientAddress > 0 && m_previousClientAddress != m_clientAddress)
	{
		// Client address have changed, reset the previous client.
		for (u8 i = 0; i < REMOTE_INPUT_MAX_PADS; ++i)
		{
			ioRemotePad_Output padOutput;
			sysMemSet(&padOutput, 0, sizeof(ioRemotePad_Output));
			padOutput.index = i;

			SendRemoteOutput(ioRemoteType::IRT_Pad, &padOutput, sizeof(ioRemotePad_Output), m_previousClientAddress);
		}
	}

	if (m_clientAddress > 0)
	{
		for (u8 i = 0; i < REMOTE_INPUT_MAX_PADS; ++i)
		{
			if (m_hasPadOutput[i] == false)
			{
				continue;
			}

			ioRemotePad_Output& padOutput = m_remoteOutputPads[i];
			SendRemoteOutput(ioRemoteType::IRT_Pad, &padOutput, sizeof(ioRemotePad_Output), m_clientAddress);

			padOutput.color.update = false;
			padOutput.color.reset = false;

			m_hasPadOutput[i] = false;
		}
	}

	m_previousClientAddress = m_clientAddress;
}

bool ioRemoteInput::ReceiveData(void* data, int size, int maxSize, bool isHeader)
{
	if (size >= REMOTE_INPUT_BUFFER_SIZE || maxSize >= REMOTE_INPUT_BUFFER_SIZE)
	{
		return false;
	}

	u8 buffer[REMOTE_INPUT_BUFFER_SIZE];
	
	int totalReceived		= 0;
	int receivedCount		= 0;
	int usableSize			= Min(size, maxSize);
	int maxSizeToReceive	= maxSize; // This is the max size of the data we can expect to receive.
	int remainingToReceive	= REMOTE_INPUT_BUFFER_SIZE;

#if RSG_ORBIS
	SceNetSockaddrIn fromAddress;
	sysMemSet(&fromAddress, 0, sizeof(fromAddress));
#elif RSG_DURANGO || RSG_PC
	sockaddr_in fromAddress;
	sysMemSet(&fromAddress, 0, sizeof(fromAddress));
#endif // RSG_DURANGO || RSG_PC
	
	do
	{		
#if RSG_ORBIS
		SceNetSocklen_t addressLength = sizeof(fromAddress);
		receivedCount = sceNetRecvfrom((SceNetId)m_socket, (char*)buffer + totalReceived, remainingToReceive, SCE_NET_MSG_DONTWAIT, (SceNetSockaddr*)&fromAddress, &addressLength);
#elif RSG_DURANGO || RSG_PC
		int addressLength = sizeof(fromAddress);
		receivedCount = recvfrom((SOCKET)m_socket, (char*)buffer + totalReceived, remainingToReceive, 0, (sockaddr*)&fromAddress, &addressLength);
#endif // RSG_DURANGO || RSG_PC

		totalReceived		+= receivedCount > 0 ? receivedCount : 0;
		remainingToReceive	-= receivedCount;

	} while (receivedCount > 0 && totalReceived < maxSizeToReceive && remainingToReceive > 0);

	if (isHeader && totalReceived >= size)
	{
		int headerSize	= (int)((ioRemoteInputHeaderBase*)buffer)->headerSize;
		usableSize		= Min(headerSize, maxSize);
	}
	
	if (totalReceived >= usableSize)
	{
		int sizeToCopy = Min(totalReceived, maxSize);
		sysMemCpy(data, buffer, sizeToCopy);

#if REMOTE_INPUT_PLATFORMS
		if (isHeader == false)
		{
			m_clientAddress = (u32)fromAddress.sin_addr.s_addr;
		}
#endif // REMOTE_INPUT_PLATFORMS
		
		return true;
	}
	
	return false;
}

void ioRemoteInput::SendData(const void* data, int size, u32 clientAddress)
{
#if RSG_ORBIS
	SceNetSockaddrIn addr;
	addr.sin_len = sizeof(addr);
	addr.sin_family = SCE_NET_AF_INET;
	addr.sin_port = sceNetHtons((u16)m_port);
	addr.sin_addr.s_addr = clientAddress;
	
	sceNetSendto((SceNetId)m_socket, data, size, SCE_NET_MSG_DONTWAIT, (SceNetSockaddr*)&addr, sizeof(addr));
#elif RSG_DURANGO || RSG_PC
	sockaddr_in addr = { 0 };
	addr.sin_family = AF_INET;
	addr.sin_port = htons((u16)m_port);
	addr.sin_addr.s_addr = clientAddress;
	
	sendto((SOCKET)m_socket, (const char*)data, size, 0, (sockaddr*)&addr, sizeof(addr));
#else
	(void)data;
	(void)clientAddress;
	(void)size;
#endif // !REMOTE_INPUT_PLATFORMS
}

void ioRemoteInput::SendRemoteOutput(ioRemoteType type, const void* typeData, u16 typeDataSize, u32 clientAddress)
{
	ioRemoteOutputHeader header;
	SetupHeader(header, type, typeDataSize);

	SendData(&header, sizeof(ioRemoteOutputHeader), clientAddress);
	SendData(typeData, typeDataSize, clientAddress);
}

void ioRemoteInput::UpdateRemotePad(const ioRemotePad& pad)
{
	if (!pad.remoteUpdateCount || pad.remoteUpdateCount > m_remotePads[pad.index].remoteUpdateCount)
	{
		m_remotePads[pad.index] = pad;
	}
}

void ioRemoteInput::UpdateRemoteMouse(const ioRemoteMouse& mouse)
{
	if (!mouse.remoteUpdateCount || mouse.remoteUpdateCount > m_remoteMouse.remoteUpdateCount)
	{
		m_remoteMouse = mouse;
		
		ioMouse::m_X	= (int)RemapRange((float)m_remoteMouse.x, 0.0f, (float)m_remoteMouse.clientScreenWidth, 0.0f, (float)g_WindowWidth);
		ioMouse::m_Y	= (int)RemapRange((float)m_remoteMouse.y, 0.0f, (float)m_remoteMouse.clientScreenHeight, 0.0f, (float)g_WindowHeight);
		ioMouse::m_dZ	= m_remoteMouse.wheelDelta / 120;
	}
}

void ioRemoteInput::UpdateRemoteKeyboard(const ioRemoteKeyboard& keyboard)
{
	if (!keyboard.remoteUpdateCount || keyboard.remoteUpdateCount > m_remoteKeyboard.remoteUpdateCount)
	{
		m_remoteKeyboard = keyboard;

		int translatedKey = ioEventQueue::TranslateKey(m_remoteKeyboard.key);

		ioKeyboard::SetKey(translatedKey, m_remoteKeyboard.event == ioRemoteKeyboard::KEY_DOWN);

		if (m_remoteKeyboard.event == ioRemoteKeyboard::KEY_DOWN && m_remoteKeyboard.character != 0)
		{
			ioKeyboard::SetTextCharPressed((char16)m_remoteKeyboard.character, (ioMapperParameter)translatedKey);
		}
	}
}

void ioRemoteInput::Reset()
{
	for (u8 i = 0; i < REMOTE_INPUT_MAX_PADS; ++i)
	{
		sysMemSet(&m_remotePads[i], 0, sizeof(ioRemotePad));
		sysMemSet(&m_remoteOutputPads[i], 0, sizeof(ioRemotePad_Output));
		m_remotePads[i].index = i;
		m_remoteOutputPads[i].index = i;
		
		m_hasPadOutput[i] = false;
	}

	m_clientAddress = 0;
	m_previousClientAddress = 0;

	sysMemSet(&m_remoteMouse, 0, sizeof(ioRemoteMouse));
	sysMemSet(&m_remoteKeyboard, 0, sizeof(ioRemoteKeyboard));
}

void ioRemoteInput::SetupHeader(ioRemoteOutputHeader& header, ioRemoteType type, u16 typeDataSize)
{
	header.version		= REMOTE_OUTPUT_CURRENT_VERSION;
	header.headerSize	= sizeof(ioRemoteOutputHeader);
	header.type			= (u8)type;
	header.typeDataSize = typeDataSize;
	header.reserved		= 0;
}

#endif // REMOTE_INPUT_SUPPORT