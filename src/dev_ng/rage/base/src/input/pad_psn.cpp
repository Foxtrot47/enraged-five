// 
// input/pad_psn.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#if __PPU

#include "pad.h"

#include "widgetpad.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"

#include <cell/pad.h>
#include <cell/padfilter.h>
#include <sdk_version.h>
#include <stdio.h>

#pragma comment(lib,"padfilter")

#if !__FINAL
#include "system/param.h"

namespace rage
{
	XPARAM(norumble);
	PARAM(nosixaxis,"Disable use of PS3 orientation sensors");
} // namespace rage
#endif // !__FINAL

using namespace rage;

static CellPadInfo2 PadInfo2;
static CellPadData Data[ioPad::MAX_PADS];
static CellPadFilterIIRSos Filter[ioPad::MAX_PADS][ioPad::SENSOR_NUMAXES];

void ioPad::BeginAll() {
	Assert(MAX_PADS == 7);
	cellPadInit(MAX_PADS);
	for (int i=0; i<MAX_PADS; i++) {
		sm_Pads[i].m_IsConnected = false;
		sm_Pads[i].m_PadIndex = i;
#if !__FINAL
		// Always support analog if possible, but sensors can be disabled in nonfinal builds for testing.
		cellPadSetPortSetting(i, PARAM_nosixaxis.Get()? CELL_PAD_SETTING_PRESS_ON : CELL_PAD_SETTING_PRESS_ON  | CELL_PAD_SETTING_SENSOR_ON);	// NB: Docs are incorrect
#else
		cellPadSetPortSetting(i, CELL_PAD_SETTING_PRESS_ON | CELL_PAD_SETTING_SENSOR_ON);	// NB: Docs are incorrect
#endif
		for (int j=0; j<SENSOR_NUMAXES; j++)
			cellPadFilterIIRInit(&Filter[i][j],CELL_PADFILTER_IIR_CUTOFF_2ND_LPF_BT_020);
	}
}

void ioPad::EndAll() {
	cellPadEnd();
}

void ioPad::Update(bool UNUSED_PARAM(ignoreInput)) {
	int i  = this - sm_Pads;
	if (!(PadInfo2.port_status[i] & CELL_PAD_STATUS_CONNECTED)) {
		// Displayf("pad %d unplugged",i);
		ClearInputs();
		m_AreButtonsAnalog = false;
		m_HasSensors = false;
		return;
	}
	else
		m_LastButtons = m_Buttons;		// still need to do this even if not clearing inputs.

	/* Displayf("device %d status %x setting %x caps %x type %x",
		i,PadInfo2.port_status[i],PadInfo2.port_setting[i],PadInfo2.device_capability[i],PadInfo2.device_type[i]); */

	if (PadInfo2.device_type[i] == CELL_PAD_DEV_TYPE_LDD) {
		// Displayf("device %d is a wheel?",i);
		if(m_SubType != WHEEL)
		{
			sm_HasChanged = true;
			m_SubType = WHEEL;
		}
		return;
	}
	else {
		if(m_SubType != GAMEPAD)
		{
			sm_HasChanged = true;
			m_SubType = GAMEPAD;
		}
	}

	cellPadGetData(i,&Data[i]);

	// https://ps3.scedev.net/technotes/view/1055
	// if len is zero, it just means that data has not changed, not that no buttons are active.
	// Previously this mostly worked because we were re-reading old static data, but we would
	// think that the buttons were not analog whenever they didn't change.  Also, since we
	// enable the sensors by default, the length is NEVER zero in that case, so the bug will
	// never show up on stock sony controllers.
	if (Data[i].len > CELL_PAD_BTN_OFFSET_PRESS_RIGHT) {
		u8 b2 = Data[i].button[CELL_PAD_BTN_OFFSET_DIGITAL1];
		u8 b3 = Data[i].button[CELL_PAD_BTN_OFFSET_DIGITAL2];

		m_Buttons = 0;
		if (b2 & CELL_PAD_CTRL_LEFT) m_Buttons |= LLEFT;
		if (b2 & CELL_PAD_CTRL_DOWN) m_Buttons |= LDOWN;
		if (b2 & CELL_PAD_CTRL_RIGHT) m_Buttons |= LRIGHT;
		if (b2 & CELL_PAD_CTRL_UP) m_Buttons |= LUP;
		if (b2 & CELL_PAD_CTRL_START) m_Buttons |= START;
		if (b2 & CELL_PAD_CTRL_R3) m_Buttons |= R3;
		if (b2 & CELL_PAD_CTRL_L3) m_Buttons |= L3;
		if (b2 & CELL_PAD_CTRL_SELECT) m_Buttons |= SELECT;

		if (b3 & CELL_PAD_CTRL_SQUARE) m_Buttons |= RLEFT;
		if (b3 & CELL_PAD_CTRL_CROSS) m_Buttons |= RDOWN;
		if (b3 & CELL_PAD_CTRL_CIRCLE) m_Buttons |= RRIGHT;
		if (b3 & CELL_PAD_CTRL_TRIANGLE) m_Buttons |= RUP;
		if (b3 & CELL_PAD_CTRL_R1) m_Buttons |= R1;
		if (b3 & CELL_PAD_CTRL_L1) m_Buttons |= L1;
		if (b3 & CELL_PAD_CTRL_R2) m_Buttons |= R2;
		if (b3 & CELL_PAD_CTRL_L2) m_Buttons |= L2;

		m_Axis[0] = Data[i].button[CELL_PAD_BTN_OFFSET_ANALOG_LEFT_X];
		m_Axis[1] = Data[i].button[CELL_PAD_BTN_OFFSET_ANALOG_LEFT_Y];
		m_Axis[2] = Data[i].button[CELL_PAD_BTN_OFFSET_ANALOG_RIGHT_X];
		m_Axis[3] = Data[i].button[CELL_PAD_BTN_OFFSET_ANALOG_RIGHT_Y];
	}

	// length is in halfwords (same "namespace" as CELL_PAD_BTN...)
	if (Data[i].len > CELL_PAD_BTN_OFFSET_PRESS_R2) {	// pressure-sensitive data available?
		m_AreButtonsAnalog = true;
		m_AnalogButtons[LRIGHT_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_RIGHT];
		m_AnalogButtons[LLEFT_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_LEFT];
		m_AnalogButtons[LUP_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_UP];
		m_AnalogButtons[LDOWN_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_DOWN];

		m_AnalogButtons[RRIGHT_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_CIRCLE];
		m_AnalogButtons[RLEFT_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_SQUARE];
		m_AnalogButtons[RUP_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_TRIANGLE];
		m_AnalogButtons[RDOWN_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_CROSS];

		m_AnalogButtons[L1_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_L1];
		m_AnalogButtons[R1_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_R1];
		m_AnalogButtons[L2_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_L2];
		m_AnalogButtons[R2_INDEX] = (u8) Data[i].button[CELL_PAD_BTN_OFFSET_PRESS_R2];
	}
	else if (!m_AreButtonsAnalog) {
		m_AnalogButtons[LRIGHT_INDEX] = (m_Buttons & LRIGHT)? 255 : 0;
		m_AnalogButtons[LLEFT_INDEX] = (m_Buttons & LLEFT)? 255 : 0;
		m_AnalogButtons[LUP_INDEX] = (m_Buttons & LUP)? 255 : 0;
		m_AnalogButtons[LDOWN_INDEX] = (m_Buttons & LDOWN)? 255 : 0;

		m_AnalogButtons[RRIGHT_INDEX] = (m_Buttons & RRIGHT)? 255 : 0;
		m_AnalogButtons[RLEFT_INDEX] = (m_Buttons & RLEFT)? 255 : 0;
		m_AnalogButtons[RUP_INDEX] = (m_Buttons & RUP)? 255 : 0;
		m_AnalogButtons[RDOWN_INDEX] = (m_Buttons & RDOWN)? 255 : 0;

		m_AnalogButtons[L1_INDEX] = (m_Buttons & L1)? 255 : 0;
		m_AnalogButtons[R1_INDEX] = (m_Buttons & R1)? 255 : 0;
		m_AnalogButtons[L2_INDEX] = (m_Buttons & L2)? 255 : 0;
		m_AnalogButtons[R2_INDEX] = (m_Buttons & R2)? 255 : 0;
	}

#if __BANK
	for ( int i = 0; i < 4; ++i )
	{
		if ( m_bankAxis[i] != 0x80 )
		{
			m_Axis[i] = m_bankAxis[i];
		}
	}

	m_Buttons |= m_bankButtons;

	for ( int i = 0; i < NUMBUTTONS; ++i )
	{
		if ( m_bankAnalogButtons[i] != 0 )
		{
			m_AnalogButtons[i] = m_bankAnalogButtons[i];
			m_AreButtonsAnalog = true;
		}
	}

	ClearBankInputs();
#endif

	if (Data[i].len > CELL_PAD_BTN_OFFSET_SENSOR_G) {
		m_HasSensors = true;
		m_Sensors[SENSOR_X] = cellPadFilterIIRFilter(&Filter[i][SENSOR_X],Data[i].button[CELL_PAD_BTN_OFFSET_SENSOR_X]);
		m_Sensors[SENSOR_Y] = cellPadFilterIIRFilter(&Filter[i][SENSOR_Y],Data[i].button[CELL_PAD_BTN_OFFSET_SENSOR_Y]);
		m_Sensors[SENSOR_Z] = cellPadFilterIIRFilter(&Filter[i][SENSOR_Z],Data[i].button[CELL_PAD_BTN_OFFSET_SENSOR_Z]);
		m_Sensors[SENSOR_G] = cellPadFilterIIRFilter(&Filter[i][SENSOR_G],Data[i].button[CELL_PAD_BTN_OFFSET_SENSOR_G]);
	}
	else if (!m_HasSensors) {
		m_Sensors[SENSOR_X] = 512;
		m_Sensors[SENSOR_Y] = 512;
		m_Sensors[SENSOR_Z] = 512;
		m_Sensors[SENSOR_G] = 512;
	}

	//UpdateDebug();

	m_HasRumble = (PadInfo2.device_capability[i] & CELL_PAD_CAPABILITY_ACTUATOR) != 0;
#if !__FINAL
	m_HasRumble &= !PARAM_norumble.Get();
#endif // !__FINAL

	if (m_ActuatorDataChanged && m_HasRumble) {
		CellPadActParam rumble;
		// On 360, actuator zero is the big motor on the left, one is the small motor on the right.
		// On PS3, actuator one is the big motor on the left, zero is the small motor on the right.
		rumble.motor[0] = m_ActuatorsEnabled && m_ActuatorData[LIGHT_MOTOR] != 0;		// small motor only respects zero or one
		rumble.motor[1] = m_ActuatorsEnabled? m_ActuatorData[HEAVY_MOTOR] / 257 : 0;		// remap full 16 bit range for large motor

		// Reserved area must be zero
		rumble.reserved[0] = rumble.reserved[1] = rumble.reserved[2] = rumble.reserved[3] = rumble.reserved[4] = rumble.reserved[5] = 0;

		m_ActuatorDataChanged = false;
		cellPadSetActDirect(i,&rumble);
	}
}

void ioPad::UpdateAll(bool UNUSED_PARAM(ignoreInput), bool UNUSED_PARAM(checkForNewDevice)) {
	sm_HasChanged = false;

	CellPadInfo2 newPadInfo2;
	if (cellPadGetInfo2(&newPadInfo2))
	{
		Errorf("padgetinfo failed");
		return;
	}

	// Check if changed
	for(unsigned int index = 0; index < newPadInfo2.max_connect; index++)
	{
		if (PadInfo2.device_type[index] != newPadInfo2.device_type[index])
		{
			sm_HasChanged = true;
			break;
		}
	}

	PadInfo2 = newPadInfo2;

	sm_Intercepted = (PadInfo2.system_info & CELL_PAD_INFO_INTERCEPTED) != 0;

	for (int i=0; i<MAX_PADS; i++) {
		sm_Pads[i].m_IsConnected = (PadInfo2.port_status[i] & CELL_PAD_STATUS_CONNECTED) != 0;
		sm_Pads[i].Update();
	}
}

void ioPad::StopShakingNow()
{
	int i  = this - sm_Pads;

	if (cellPadGetInfo2(&PadInfo2))
	{
		Errorf("padgetinfo failed");
		return;
	}
	if (PadInfo2.port_status[i] == 0)
		return;

	m_HasRumble = (PadInfo2.device_capability[i] & CELL_PAD_CAPABILITY_ACTUATOR) != 0;

	if (m_HasRumble) 
	{
		for (int j=0; j<MAX_ACTUATORS; j++)
			m_ActuatorData[j] = 0;

		CellPadActParam rumble;
		// On 360, actuator zero is the big motor on the left, one is the small motor on the right.
		// On PS3, actuator one is the big motor on the left, zero is the small motor on the right.
		rumble.motor[0] = m_ActuatorsEnabled && m_ActuatorData[LIGHT_MOTOR] != 0;    // small motor only respects zero or one
		rumble.motor[1] = m_ActuatorsEnabled? m_ActuatorData[HEAVY_MOTOR] / 257 : 0; // remap full 16 bit range for large motor

		// Reserved area must be zero
		rumble.reserved[0] = rumble.reserved[1] = rumble.reserved[2] = rumble.reserved[3] = rumble.reserved[4] = rumble.reserved[5] = 0;

		m_ActuatorDataChanged = false;
		cellPadSetActDirect(i,&rumble);
	}
}


#if __BANK
void ioPad::AddPadWidgets()
{
	bkBank *pBank = BANKMGR.FindBank( "rage - Pad" );
	if ( pBank == NULL )
	{
		pBank = &BANKMGR.CreateBank( "rage - Pad" );
	}

	char name[16];
	sprintf( name, "Pad %d", m_PadIndex );

	if ( bkRemotePacket::IsConnectedToRag() )
	{
		ioWidgetPad* pWidget = rage_new ioWidgetPad( name, m_PadIndex, ioWidgetPad::PS3_PLATFORM );
		pBank->AddWidget( *pWidget );
	}
	else
	{
		pBank->PushGroup( name );
		{
			pBank->PushGroup( "Shoulder Buttons" );
			{
				pBank->AddButton( "L1", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L1 ) );
				pBank->AddButton( "R1", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R1 ) );
				pBank->AddButton( "L2", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L2 ) );
				pBank->AddButton( "R2", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R2 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Left Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData) LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LDOWN ) );
				pBank->AddButton( "L3", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Pad" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::ButtonPressedCB ), this,(CallbackData)LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LDOWN ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Right Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData)RRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "R3", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Buttons" );
			{
				pBank->AddButton( "X", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "O", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RRIGHT ) );
				pBank->AddButton( "[]", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "^", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RUP ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Other" );
			{
				pBank->AddButton( "Select", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)SELECT ) );
				pBank->AddButton( "Start", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)START ) );
			}
			pBank->PopGroup();
		}
		pBank->PopGroup();
	}
}

void ioPad::AnalogStickPressedCB( CallbackData data )
{
	int button = reinterpret_cast<int>( data );

	switch ( button )
	{
	case LUP:
		m_bankAxis[1] = 0x0;
		m_bankAnalogButtons[LUP_INDEX] = 0xff;
		break;
	case LLEFT:
		m_bankAxis[0] = 0x0;
		m_bankAnalogButtons[LLEFT_INDEX] = 0xff;
		break;
	case LRIGHT:
		m_bankAxis[0] = 0xff;
		m_bankAnalogButtons[LRIGHT_INDEX] = 0xff;
		break;
	case LDOWN:
		m_bankAxis[1] = 0xff;
		m_bankAnalogButtons[LDOWN_INDEX] = 0xff;
		break;
	case RUP:
		m_bankAxis[3] = 0x0;
		m_bankAnalogButtons[RUP_INDEX] = 0xff;
		break;
	case RLEFT:
		m_bankAxis[2] = 0x0;
		m_bankAnalogButtons[RLEFT_INDEX] = 0xff;
		break;
	case RRIGHT:
		m_bankAxis[2] = 0xff;
		m_bankAnalogButtons[RRIGHT_INDEX] = 0xff;
		break;
	case RDOWN:
		m_bankAxis[3] = 0xff;
		m_bankAnalogButtons[RDOWN_INDEX] = 0xff;
		break;
	}
}

void ioPad::ButtonPressedCB( CallbackData data )
{
	int button = reinterpret_cast<int>( data );

	m_bankButtons |= button;

	switch ( button )	
	{
	case L1:
		m_bankAnalogButtons[L1_INDEX] = 0xff;
		break;
	case R1:
		m_bankAnalogButtons[R1_INDEX] = 0xff;
		break;
	case L2:
		m_bankAnalogButtons[L2_INDEX] = 0xff;
		break;
	case R2:
		m_bankAnalogButtons[R2_INDEX] = 0xff;
		break;
	}
}
#endif

#endif	// __PPU
