#if RSG_DURANGO

// Main Header
#include "keyboard.h"

// Rage Headers
#include "keys.h"

#include "system/criticalsection.h"
#include "system/xtl.h"

#define RSG_USE_KEYBOARD_ACTIVATION_HANDLER (_XDK_VER >= 11511)

namespace rage
{
static sysCriticalSectionToken sg_ioKeyboardCs;

// These are WinRT types so they cannot be a part of ioKeyboard in the header otherwise code that depends on it will require
// WinRT enabled. So they are static globals to act as private members.
static Windows::Foundation::EventRegistrationToken sg_ioKeyboardKeyDownToken;
static Windows::Foundation::EventRegistrationToken sg_ioKeyboardKeyUpToken;
static Windows::Foundation::EventRegistrationToken sg_ioKeyboardCharReceivedToken;

#if RSG_USE_KEYBOARD_ACTIVATION_HANDLER
static Windows::Foundation::EventRegistrationToken sg_ioKeyboardActivatedToken;
#endif // RSG_USE_KEYBOARD_ACTIVATION_HANDLER


bool ioKeyboard::sm_KeyUpBufferState[256];
ioMapperParameter ioKeyboard::sm_LastKeyDown;

//////////////////////////////////////////////////////////////////////////
// NOTE:
//////////////////////////////////////////////////////////////////////////
//
// There is a bug in Durango where numlock does not work. This means that
// numpad keys are returning numlock off states (e.g. numpad 9 is
// returning PgUp). This has been reported on the forums at:
// https://forums.xboxlive.com/AnswerPage.aspx?qid=737aaf02-149a-4318-89c8-15e429b8df23&tgt=1
// 
// For now: fudge it so we can get numlock working.
// 
//////////////////////////////////////////////////////////////////////////
static bool s_NumlockOn = false;
static bool s_NumlockFirstPress = false;

static u32 ConvertKeyCode(u32 keyCode, Windows::UI::Core::CorePhysicalKeyStatus /*status*/)
{
	// If this gets any bigger it might be best to use a look up table.
	switch (keyCode)
	{
	// B* 2099791: Shift key no longer works because Microsoft changed it to always return VK_SHIFT
	// (see https://developer.xboxlive.com/en-us/platform/development/downloads/releasenotes/Pages/home.aspx#19.1) point 19.1.
	case VK_SHIFT:
	case Windows::System::VirtualKey::LeftShift:
		return KEY_LSHIFT;
	case Windows::System::VirtualKey::RightShift:
		return KEY_RSHIFT;

	// B* 2099791: Shift key no longer works because Microsoft changed it to always return VK_SHIFT
	// (see https://developer.xboxlive.com/en-us/platform/development/downloads/releasenotes/Pages/home.aspx#19.1) point 19.1.
	// The control keys appear to be affected too.
	case VK_CONTROL:
	case Windows::System::VirtualKey::LeftControl:
		return KEY_LCONTROL;
	case Windows::System::VirtualKey::RightControl:
		return KEY_RCONTROL;
		
	case VK_MENU:
	case Windows::System::VirtualKey::LeftMenu:
		return KEY_LMENU;
	case Windows::System::VirtualKey::RightMenu:
		return KEY_RMENU;
	case Windows::System::VirtualKey::LeftWindows:
		return KEY_LWIN;
	case Windows::System::VirtualKey::RightWindows:
		return KEY_RWIN;
	case Windows::System::VirtualKey::NumberKeyLock:
		return KEY_NUMLOCK;
	case Windows::System::VirtualKey::Add:
		return KEY_ADD;
	case Windows::System::VirtualKey::Separator:
		return KEY_DECIMAL;
	case Windows::System::VirtualKey::Subtract:
		return KEY_SUBTRACT;
	case Windows::System::VirtualKey::Divide:
		return KEY_DIVIDE;


	//////////////////////////////////////////////////////////////////////////
	// NOTE:
	//////////////////////////////////////////////////////////////////////////
	//
	// There is a bug in Durango where numlock does not work. This means that
	// numpad keys are returning numlock off states (e.g. numpad 9 is
	// returning PgUp). This has been reported on the forums at:
	// https://forums.xboxlive.com/AnswerPage.aspx?qid=737aaf02-149a-4318-89c8-15e429b8df23&tgt=1
	// 
	// For now: fudge it so we can get numlock working.
	// 
	//////////////////////////////////////////////////////////////////////////
	case Windows::System::VirtualKey::Insert:
		if(s_NumlockOn == false)
		{
			return KEY_INSERT;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad0:
		return KEY_NUMPAD0;

	case Windows::System::VirtualKey::End:
		if(s_NumlockOn == false)
		{
			return KEY_END;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad1:
		return KEY_NUMPAD1;

	case Windows::System::VirtualKey::Down:
		if(s_NumlockOn == false)
		{
			return KEY_DOWN;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad2:
		return KEY_NUMPAD2;

	case Windows::System::VirtualKey::PageDown:
		if(s_NumlockOn == false)
		{
			return KEY_PAGEDOWN;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad3:
		return KEY_NUMPAD3;

	case Windows::System::VirtualKey::Left:
		if(s_NumlockOn == false)
		{
			return KEY_LEFT;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad4:
		return KEY_NUMPAD4;

	case Windows::System::VirtualKey::Clear:
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad5:
		return KEY_NUMPAD5;

	case Windows::System::VirtualKey::Right:
		if(s_NumlockOn == false)
		{
			return KEY_RIGHT;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad6:
		return KEY_NUMPAD6;

	case Windows::System::VirtualKey::Home:
		if(s_NumlockOn == false)
		{
			return KEY_HOME;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad7:
		return KEY_NUMPAD7;

	case Windows::System::VirtualKey::Up:
		if(s_NumlockOn == false)
		{
			return KEY_UP;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad8:
		return KEY_NUMPAD8;

	case Windows::System::VirtualKey::PageUp:
		if(s_NumlockOn == false)
		{
			return KEY_PAGEUP;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::NumberPad9:
		return KEY_NUMPAD9;

	case Windows::System::VirtualKey::Delete:
		if(s_NumlockOn == false)
		{
			return KEY_DELETE;
		}
		// Intentional Fall through!
	case Windows::System::VirtualKey::Decimal:
		return KEY_DECIMAL;

	default:
		return keyCode;
	}
}

void ioKeyboard::Begin(bool)
{
	// No-op - all setup in InitKeyboardFromMainThread().
}

void ioKeyboard::InitKeyboardFromMainThread()
{
	sysMemSet(sm_KeyUpBufferState, 0, sizeof(sm_KeyUpBufferState));

	try
	{
		Windows::UI::Core::ICoreWindow^ window = Windows::UI::Core::CoreWindow::GetForCurrentThread();

		sg_ioKeyboardKeyDownToken = window->KeyDown += ref new Windows::Foundation::TypedEventHandler<Windows::UI::Core::CoreWindow^, Windows::UI::Core::KeyEventArgs^>(
			[](Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ args)
		{
			sysCriticalSection lock(sg_ioKeyboardCs);
			try
			{
				if(Verifyf(args != nullptr, "Error in KeyDown Event!"))
				{
					int code = ConvertKeyCode(static_cast<u32>(args->VirtualKey), args->KeyStatus);
					sm_CurrentState[code] = 0x80;

					if(code == KEY_NUMLOCK)
					{
						s_NumlockOn = true;
					}
				}
			}
			catch(Platform::Exception^)
			{
				Assertf(false, "Failed to retrieve key down information!");
			}
		});

		sg_ioKeyboardKeyUpToken = window->KeyUp += ref new Windows::Foundation::TypedEventHandler<Windows::UI::Core::CoreWindow^, Windows::UI::Core::KeyEventArgs^>(
			[](Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ args)
		{
			sysCriticalSection lock(sg_ioKeyboardCs);
			try
			{
				if(Verifyf(args != nullptr, "Error in KeyUp Event!"))
				{
					u32 code = ConvertKeyCode(static_cast<u32>(args->VirtualKey), args->KeyStatus);
					sm_LastKeyDown = static_cast<ioMapperParameter>(code);
					
					if(code != KEY_NUMLOCK)
					{
						sm_KeyUpBufferState[code] = true;
					}
					else if(s_NumlockFirstPress)
					{
						s_NumlockOn = false;
						s_NumlockFirstPress = false;
						sm_KeyUpBufferState[KEY_NUMLOCK] = true;
					}
					else
					{
						s_NumlockFirstPress = true;
					}
				}
			}
			catch(Platform::Exception^)
			{
				Assertf(false, "Failed to retrieve key down information!");
			}
		});

		sg_ioKeyboardCharReceivedToken = window->CharacterReceived += ref new Windows::Foundation::TypedEventHandler<Windows::UI::Core::CoreWindow^, Windows::UI::Core::CharacterReceivedEventArgs^>(
			[](Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::CharacterReceivedEventArgs^ args)
		{
			sysCriticalSection lock(sg_ioKeyboardCs);
			try
			{
				if(Verifyf(args != nullptr, "Error in CharacterReceived Event!"))
				{
					u32 code = args->KeyCode;
					SetTextCharPressed(static_cast<char16>(code), sm_LastKeyDown);
				}
			}
			catch(Platform::Exception^)
			{
				Assertf(false, "Failed to retrieve key down information!");
			}
		});

#if RSG_USE_KEYBOARD_ACTIVATION_HANDLER
		Windows::UI::Core::CoreDispatcher^ dispatcher = window->Dispatcher;
		if(Verifyf(dispatcher != nullptr, "Failed to get CoreDispatcher^ from window!"))
		{
			sg_ioKeyboardActivatedToken = dispatcher->AcceleratorKeyActivated += ref new Windows::Foundation::TypedEventHandler<Windows::UI::Core::CoreDispatcher^, Windows::UI::Core::AcceleratorKeyEventArgs^>(
				[](Windows::UI::Core::CoreDispatcher^, Windows::UI::Core::AcceleratorKeyEventArgs^ args)
			{
				sysCriticalSection lock(sg_ioKeyboardCs);
				try
				{
					if(Verifyf(args != nullptr, "Error in CharacterReceived Event!"))
					{
						u32 code = ConvertKeyCode(static_cast<u32>(args->VirtualKey), args->KeyStatus);
						if(args->KeyStatus.IsKeyReleased)
						{
							sm_KeyUpBufferState[code] = true;
						}
						else
						{
							sm_CurrentState[code] = 0x80;
						}
					}
				}
				catch(Platform::Exception^)
				{
					Assertf(false, "Failed to retrieve key down information!");
				}
			});
		}
#endif // RSG_USE_KEYBOARD_ACTIVATION_HANDLER
	}
	catch(Platform::Exception^)
	{
		Assertf(false, "Failed to activated keyboard!");
	}
}

void ioKeyboard::End()
{
	try
	{
		Windows::UI::Core::ICoreWindow^ window = Windows::UI::Core::CoreWindow::GetForCurrentThread();
		window->KeyDown           -= sg_ioKeyboardKeyDownToken;
		window->KeyUp             -= sg_ioKeyboardKeyUpToken;
		window->CharacterReceived -= sg_ioKeyboardCharReceivedToken;

#if RSG_USE_KEYBOARD_ACTIVATION_HANDLER
		Windows::UI::Core::CoreDispatcher^ dispatcher = window->Dispatcher;
		if(dispatcher != nullptr)
		{
			dispatcher->AcceleratorKeyActivated -= sg_ioKeyboardActivatedToken;
		}
#endif // RSG_USE_KEYBOARD_ACTIVATION_HANDLER
	}
	catch(Platform::Exception^)
	{
		Assertf(false, "Failed to activated keyboard!");
	}
}

void ioKeyboard::Update(bool UNUSED_PARAM(ignoreInput))
{
	sysCriticalSection lock(sg_ioKeyboardCs);
	sysMemCpy(sm_Keys[sm_Active ^1],sm_CurrentState,256);

	// Fix for url:bugstar:1734231.
	for(u32 i = 0; i < 256; ++i)
	{
		if(sm_KeyUpBufferState[i])
		{
			sm_CurrentState[i] = 0x00;
			sm_KeyUpBufferState[i] = false;
		}
	}

	sm_Active ^= 1;

	sysMemCpy(sm_TextBuffer, sm_CurrentTextBuffer, sizeof(sm_TextBuffer));
	sysMemSet(sm_CurrentTextBuffer, 0, sizeof(sm_CurrentTextBuffer));
	sm_CurrentLength = 0;
}

}

#endif //RSG_DURANGO
