
// 
// input/headtracking.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#define ENABLE_OVR (0 && RSG_PC && !RSG_TOOL)

#if ENABLE_OVR
#include "system/xtl.h"
#include "../../../3rdparty/OculusSDK/LibOVR/Include/OVR.h"
#include "../../../3rdparty/OculusSDK/LibOVR/Src/OVR_Device.h"
#include "../../../3rdparty/OculusSDK/LibOVR/Src/Util/Util_Render_Stereo.h"

#include "grcore/adapter.h"

#if __64BIT
#if __DEV
	#pragma comment(lib, "libovr64.lib")
#else // !__DEBUG
	#pragma comment(lib, "libovr64.lib")
#endif // __DEBUG
#else // !__64BIT
#if __DEV
	#pragma comment(lib, "libovr.lib")
#else // !__DEBUG
	#pragma comment(lib, "libovr.lib")
#endif // __DEBUG
#endif // __64BIT

#endif // ENABLE_OVR

#include "headtracking.h"
#include "system/param.h"

PARAM(Oculus, "Enable Oculus");
PARAM(FPSCam, "Enable FPSCam");

using namespace rage;

enum ViewType
{
    View_Perspective,
    View_XZ_UpY,
    View_XY_DownZ,
    View_Count
};

#if ENABLE_OVR
OVR::Ptr<OVR::DeviceManager>	sm_pManager = NULL;
OVR::Ptr<OVR::HMDDevice>		sm_pHMD = NULL;
OVR::Ptr<OVR::SensorDevice>		sm_pSensor = NULL;
OVR::Ptr<OVR::Profile>			sm_pUserProfile = NULL;
OVR::SensorFusion*				sm_pSFusion;
OVR::HMDInfo					sm_TheHMDInfo;
OVR::MessageHandler				sm_MessageHandler;

OVR::Util::Render::StereoConfig sm_SConfig;
#endif // ENABLE_OVR

bool				sm_Initialize = false;
double				sm_LastUpdate = 0.0f;

bool ioHeadTracking::Initialize()
{
	if (sm_Initialize)
		return true;

	sm_Initialize = true;

#if ENABLE_OVR
	OVR::System::Init();

	Assert(sm_pManager == NULL);

    sm_pManager = *OVR::DeviceManager::Create();
	Assert(sm_pManager != NULL);
	if (sm_pManager == NULL)
		return false;

	sm_pHMD = *sm_pManager->EnumerateDevices<OVR::HMDDevice>().CreateDevice();
	if (sm_pHMD)
	{
		sm_pSensor = *sm_pHMD->GetSensor();

		// This will initialize HMDInfo with information about configured IPD,
		// screen size and other variables needed for correct projection.
		// We pass HMD DisplayDeviceName into the renderer to select the
		// correct monitor in full-screen mode.
		if(sm_pHMD->GetDeviceInfo(&sm_TheHMDInfo))
		{
			//RenderParams.MonitorName = hmd.DisplayDeviceName;
			sm_SConfig.SetHMDInfo(sm_TheHMDInfo);
			grcAdapterManager::SetOculusMonitor(sm_TheHMDInfo.DisplayDeviceName);
		}

		// Retrieve relevant profile settings. 
		sm_pUserProfile = sm_pHMD->GetProfile();
		if (sm_pUserProfile)
		{
			sm_fPlayerHeight = sm_pUserProfile->GetEyeHeight();
		}
	}
	else
	{
		// If we didn't detect an HMD, try to create the sensor directly.
		// This is useful for debugging sensor interaction; it is not needed in
		// a shipping app.
		sm_pSensor = *sm_pManager->EnumerateDevices<OVR::SensorDevice>().CreateDevice();
	}

    // We'll handle it's messages in this case.
    sm_pManager->SetMessageHandler(&sm_MessageHandler);
	sm_pSFusion = rage_new OVR::SensorFusion();
	if (sm_pSensor)
	{
		sm_pSFusion->AttachToSensor(sm_pSensor);
        sm_pSFusion->SetDelegateMessageHandler(&sm_MessageHandler);
        sm_pSFusion->SetPredictionEnabled(true);

	}
	sm_bEnabled = true;

	if (PARAM_FPSCam.Get())
		sm_bUseFPSCamera = true;	

	return (sm_pSensor != NULL) ? true : false;
#else
	return true;
#endif // ENABLE_OVR

}

void ioHeadTracking::Begin(bool /*inWindow*/)
{
	if (!sm_Initialize)
	{
		AssertVerify(Initialize());
	}
}

// PURPOSE: Shuts down class.  Currently called automatically by graphics pipeline.
void ioHeadTracking::End()
{
#if ENABLE_OVR
	sm_pSensor.Clear();
	sm_pManager.Clear();
	OVR::System::Destroy();
#endif // ENABLE_OVR
}

void ioHeadTracking::Reset()
{
#if ENABLE_OVR
	if (sm_pSFusion)
		sm_pSFusion->Reset();
#endif // ENABLE_OVR
}

void ioHeadTracking::EnableMotionTracking(bool bEnable)
{
	sm_bEnabled = bEnable;
}

bool ioHeadTracking::IsMotionTrackingEnabled()
{
#if ENABLE_OVR
	return (PARAM_Oculus.Get() && sm_bEnabled /* && (sm_pSensor != NULL)*/) ? true : false;
#else
	return false;
#endif
}

void  ioHeadTracking::Update(float /*fPredictedDeltaTime*/)
{
#if ENABLE_OVR
	if (sm_pSFusion == NULL)
		return;

	ALIGNAS(16) OVR::Quatf quaternion;
	
	if (sm_bUsePredicatedMode)
	{
		quaternion = sm_pSFusion->GetPredictedOrientation();
		sm_qOrientation = *(Quaternion*)&quaternion;
	} 
	else
	{
		quaternion = sm_pSFusion->GetOrientation();
		sm_qOrientation = *(Quaternion*)&quaternion;
	}
#endif // ENABLE_OVR
}
