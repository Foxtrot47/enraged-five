<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
			  generate="class">

	<!-- These are needed as the mouse and pad buttons must coinside with the ones defined in mouse.h and pad.h (although these ones are OR'ed with a specific bit so their values do not clash with other values in
		 this enum -->
	<hinsert>
#include		"input/mouse.h"
#include		"input/pad.h"
	</hinsert>

	<!-- Input Device Types -->
	<enumdef type="rage::ioMapperSource" preserveNames="true">
		<enumval name="IOMS_UNDEFINED" value="-1" />
		<enumval name="IOMS_KEYBOARD" description="Parameter should be KEY_... constant" />
		<enumval name="IOMS_MOUSE_ABSOLUTEAXIS" description="Axis should be IOM_AXIS_X or IOM_AXIS_Y. Gives position in window." />
		<enumval name="IOMS_MOUSE_CENTEREDAXIS" description="Axis should be IOM_AXIS_X or IOM_AXIS_Y (for simulating a joystick or steering wheel) Gives position in window from the center." />
		<enumval name="IOMS_MOUSE_RELATIVEAXIS" description="Axis should be IOM_AXIS_X or IOM_AXIS_Y (for a first-person shooter)" />
		<enumval name="IOMS_MOUSE_SCALEDAXIS" description="Identical to IOMS_MOUSE_RELATIVEAXIS except the axis is scaled based on the mouse scaling settings." />
		<enumval name="IOMS_MOUSE_NORMALIZED" description="Similar to IOMS_MOUSE_RELATIVEAXIS but the value is normalized over several frames to deal with the noisy mouse input." />
		<enumval name="IOMS_MOUSE_WHEEL" description="Wheel roll up, or wheel roll down." />
		<enumval name="IOMS_MOUSE_BUTTON" description="Parameter should be bitmask of ioMouse::mouse{Left,Right,Middle}	Bits must match exactly" />
		<enumval name="IOMS_MOUSE_BUTTONANY" description="Same bitmask as above.	Set to 1 if ANY of the bits match" />
		<enumval name="IOMS_PAD_DIGITALBUTTON" description="Parameter should be bitmask of buttons that must be set (ioPad::L2,R2, etc) Must match exactly" />
		<enumval name="IOMS_PAD_DIGITALBUTTONANY" description="Same bitmask as above..	Set to 1 if ANY of the bits are a match" />
		<enumval name="IOMS_PAD_ANALOGBUTTON" description="Parameter should be button index (ioPad::analogL2,analogR2... constants)" />
		<enumval name="IOMS_PAD_AXIS" description="Parameter should be IOM_AXIS_LX, LY, etc" />
		<enumval name="IOMS_JOYSTICK_BUTTON" description="Parameter should be 0-31; all joystick buttons are assumed analog" />
		<enumval name="IOMS_JOYSTICK_AXIS" description="Parameter should be 0-7; DINPUT sliders are 6 and 7" />
		<enumval name="IOMS_JOYSTICK_IAXIS" description="Same as IOMS_JOYSTICK_AXIS but axis is inverted." />
		<enumval name="IOMS_JOYSTICK_AXIS_NEGATIVE" description="Parameter should be 0-7; DINPUT sliders are 6 and 7" />
		<enumval name="IOMS_JOYSTICK_AXIS_POSITIVE" description="Parameter should be 0-7; DINPUT sliders are 6 and 7" />
		<enumval name="IOMS_JOYSTICK_POV" description="Parameter should be (IOM1_POV_UP, IOM4_POV_RIGHT, etc)" />
		<enumval name="IOMS_JOYSTICK_POV_AXIS" description='Parameter should be two POV constants (IOM1_POV_UP, IOM4_POV_RIGHT, etc) shifted and OR&apos;d; "-1" input is at bits 0..7, "+1" input is at bits 8..15. If neither POV is pressed, the value will be "0".' />
		<enumval name="IOMS_PAD_DEBUGBUTTON" description="Parameter should be bitmask of buttons that must be set (ioPad::L2,R2, etc) Must match exactly" />
		<enumval name="IOMS_DIGITALBUTTON_AXIS" description='Parameter should be two ioPad::..._INDEX constants shifted and OR&apos;d; "-1" input is at bits 0..7, "+1" input is at bits 8..15. If neither button is pressed, the value will be "0".' />
		<enumval name="IOMS_MKB_AXIS" description='Parameters should be one ioMouse::... constant and one KEY_ constant shifted and OR&apos;d; "-1" input is at bits 0...7 with bit 16 set to 1 if the input is a mouse button. "+1" input is at bits 8...15 with bit 17 set to 1 if the input is a mouse button. If neither button is pressed, the value will be "0".' />
		<enumval name="IOMS_TOUCHPAD_ABSOLUTE_AXIS" description="Parameters should be IOM_AXIS_X or IOM_AXIS_Y" />
		<enumval name="IOMS_TOUCHPAD_CENTERED_AXIS" description="Parameters should be IOM_AXIS_X or IOM_AXIS_Y" />
		<enumval name="IOMS_GAME_CONTROLLED"	description="Parameter is controlled by game level code and is game specific."/>
		<enumval name="IOMS_FORCE32" value="0x7FFFFFFF" description="So that we know we can treat this as an integer" />
	</enumdef>

	<enumdef type="rage::ioConstants">
		<enumval name="IOMC_MKB_AXIS_MASK"				value="0x000FF" description="The bits that make up the negative input of the axis." />
		<enumval name="IOMC_MKB_POSITIVE_AXIS_SHIFT"	value="0x00008" description="The amount to shift positive input in the axis." />
		<enumval name="IOMC_MKB_AXIS_NEGATIVE_IS_MOUSE"	value="0x10000" description="The bit that indicates the input type of the negative input is a mouse button (no bits means keyboard button)." />
		<enumval name="IOMC_MKB_AXIS_POSITIVE_IS_MOUSE"	value="0x20000" description="The bit that indicates the input type of the positive input is a mouse button (no bits means keyboard button)." />
		<enumval name="IOMC_MKB_AXIS_NEGATIVE_IS_WHEEL"	value="0x40000" description="The bit that indicates the input type of the negative input is a mouse wheel (no bits means keyboard button)." />
		<enumval name="IOMC_MKB_AXIS_POSITIVE_IS_WHEEL"	value="0x80000" description="The bit that indicates the input type of the positive input is a mouse wheel (no bits means keyboard button)." />
	</enumdef>

	<!-- These are used to qickly find which type of device a particular parameter belongs to. It as also used to convert values in the rage::ioMapperParameters
		 (defined below) to their corresponding class constants. -->
	<enumdef type="rage::ioParameterMask">
		<enumval name="IOMT_KEYBOARD"				value="0x00000000" />
		<enumval name="IOMT_MOUSE_AXIS"				value="0x00000200" />
		<enumval name="IOMT_MOUSE_WHEEL"			value="0x00000400" />
		<enumval name="IOMT_MOUSE_BUTTON"			value="0x00000800" />
		<enumval name="IOMT_PAD_AXIS"				value="0x00001000" />
		<enumval name="IOMT_PAD_INDEX"				value="0x00002000" />
		<enumval name="IOMT_JOYSTICK_POV"			value="0x00004000" />
		<enumval name="IOMT_JOYSTICK_BUTTON"		value="0x00008000" />
		<enumval name="IOMT_JOYSTICK_AXIS"			value="0x00010000" />
		<enumval name="IOMT_PAD_BUTTON"				value="0x00020000" />
		<enumval name="IOMT_JOYSTICK_AXIS_NEGATIVE"	value="0x00040000" description="Axis negative direction only." />
		<enumval name="IOMT_JOYSTICK_AXIS_POSITIVE"	value="0x00080000" description="Axis positive direction only." />
	</enumdef>
	
	<!-- Parameter Values -->
	<enumdef type="rage::ioMapperParameter" preserveNames="true">
		<!-- Keyboard Values These key-code correspond to the virtual key codes (VK_*) in windows.-->
		<enumval name="KEY_NULL"			value="0x00" description="Represents an invalid key."/>
		<enumval name="KEY_BACK"			value="0x08" description="Backspace. -- These key-code correspond to the virtual key codes (VK_*) in windows."/>
		<enumval name="KEY_TAB"				value="0x09" />
		<enumval name="KEY_RETURN"			value="0x0D" description="'Enter' on main keyboard." />
		<enumval name="KEY_PAUSE"			value="0x13" description="Pause." />
		<enumval name="KEY_CAPITAL"			value="0x14" />
		<enumval name="KEY_ESCAPE"			value="0x1B" />
		<enumval name="KEY_SPACE"			value="0x20" />
		<enumval name="KEY_PAGEUP"			value="0x21" description="PgUp." />
		<enumval name="KEY_PRIOR"			value="0x21" description="PgUp on arrow keypad." />
		<enumval name="KEY_PAGEDOWN"		value="0x22" description="PgDn." />
		<enumval name="KEY_NEXT"			value="0x22" description="PgDn on arrow keypad." />
		<enumval name="KEY_END"				value="0x23" description="End on arrow keypad." />
		<enumval name="KEY_HOME"			value="0x24" description="Home on arrow keypad." />
		<enumval name="KEY_LEFT"			value="0x25" description="LeftArrow on arrow keypad." />
		<enumval name="KEY_UP"				value="0x26" description="UpArrow on arrow keypad." />
		<enumval name="KEY_RIGHT"			value="0x27" description="RightArrow on arrow keypad." />
		<enumval name="KEY_DOWN"			value="0x28" description="DownArrow on arrow keypad." />
		<enumval name="KEY_SNAPSHOT"		value="0x2C" />
		<enumval name="KEY_SYSRQ"			value="0x2C" description="SysRq. NOTE: Here for legacy support, use KEY_PRINT instead."/>
		<enumval name="KEY_INSERT"			value="0x2D" description="Insert on arrow keypad." />
		<enumval name="KEY_DELETE"			value="0x2E" description="Delete on arrow keypad." />
		<enumval name="KEY_0"				value="0x30" />
		<enumval name="KEY_1"				value="0x31" />
		<enumval name="KEY_2"				value="0x32" />
		<enumval name="KEY_3"				value="0x33" />
		<enumval name="KEY_4"				value="0x34" />
		<enumval name="KEY_5"				value="0x35" />
		<enumval name="KEY_6"				value="0x36" />
		<enumval name="KEY_7"				value="0x37" />
		<enumval name="KEY_8"				value="0x38" />
		<enumval name="KEY_9"				value="0x39" />
		<enumval name="KEY_A"				value="0x41" />
		<enumval name="KEY_B"				value="0x42" />
		<enumval name="KEY_C"				value="0x43" />
		<enumval name="KEY_D"				value="0x44" />
		<enumval name="KEY_E"				value="0x45" />
		<enumval name="KEY_F"				value="0x46" />
		<enumval name="KEY_G"				value="0x47" />
		<enumval name="KEY_H"				value="0x48" />
		<enumval name="KEY_I"				value="0x49" />
		<enumval name="KEY_J"				value="0x4A" />
		<enumval name="KEY_K"				value="0x4B" />
		<enumval name="KEY_L"				value="0x4C" />
		<enumval name="KEY_M"				value="0x4D" />
		<enumval name="KEY_N"				value="0x4E" />
		<enumval name="KEY_O"				value="0x4F" />
		<enumval name="KEY_P"				value="0x50" />
		<enumval name="KEY_Q"				value="0x51" />
		<enumval name="KEY_R"				value="0x52" />
		<enumval name="KEY_S"				value="0x53" />
		<enumval name="KEY_T"				value="0x54" />
		<enumval name="KEY_U"				value="0x55" />
		<enumval name="KEY_V"				value="0x56" />
		<enumval name="KEY_W"				value="0x57" />
		<enumval name="KEY_X"				value="0x58" />
		<enumval name="KEY_Y"				value="0x59" />
		<enumval name="KEY_Z"				value="0x5A" />
		<enumval name="KEY_LWIN"			value="0x5B" description="Left Windows key." />
		<enumval name="KEY_RWIN"			value="0x5C" description="Right Windows key." />
		<enumval name="KEY_APPS"			value="0x5D" description="AppMenu key, this is in the bottom right between the windows key and the Ctrl key." />
		<enumval name="KEY_NUMPAD0"			value="0x60" />
		<enumval name="KEY_NUMPAD1"			value="0x61" />
		<enumval name="KEY_NUMPAD2"			value="0x62" />
		<enumval name="KEY_NUMPAD3"			value="0x63" />
		<enumval name="KEY_NUMPAD4"			value="0x64" />
		<enumval name="KEY_NUMPAD5"			value="0x65" />
		<enumval name="KEY_NUMPAD6"			value="0x66" />
		<enumval name="KEY_NUMPAD7"			value="0x67" />
		<enumval name="KEY_NUMPAD8"			value="0x68" />
		<enumval name="KEY_NUMPAD9"			value="0x69" />
		<enumval name="KEY_MULTIPLY"		value="0x6A" description="'*' on numeric keypad." />
		<enumval name="KEY_ADD"				value="0x6B" description="'+' on numeric keypad." />
		<enumval name="KEY_SUBTRACT"		value="0x6D" description="'-' on numeric keypad." />
		<enumval name="KEY_DECIMAL"			value="0x6E" description="'.' on numeric keypad." />
		<enumval name="KEY_DIVIDE"			value="0x6F" description="'/' on numeric keypad." />
		<enumval name="KEY_F1"				value="0x70" />
		<enumval name="KEY_F2"				value="0x71" />
		<enumval name="KEY_F3"				value="0x72" />
		<enumval name="KEY_F4"				value="0x73" />
		<enumval name="KEY_F5"				value="0x74" />
		<enumval name="KEY_F6"				value="0x75" />
		<enumval name="KEY_F7"				value="0x76" />
		<enumval name="KEY_F8"				value="0x77" />
		<enumval name="KEY_F9"				value="0x78" />
		<enumval name="KEY_F10"				value="0x79" />
		<enumval name="KEY_F11"				value="0x7A" />
		<enumval name="KEY_F12"				value="0x7B" />
		<enumval name="KEY_F13"				value="0x7C" description="(NEC PC98)." />
		<enumval name="KEY_F14"				value="0x7D" description="(NEC PC98)." />
		<enumval name="KEY_F15"				value="0x7E" description="(NEC PC98)." />
		<enumval name="KEY_F16"				value="0x7F" />
		<enumval name="KEY_F17"				value="0x80" />
		<enumval name="KEY_F18"				value="0x81" />
		<enumval name="KEY_F19"				value="0x82" />
		<enumval name="KEY_F20"				value="0x83" />
		<enumval name="KEY_F21"				value="0x84" />
		<enumval name="KEY_F22"				value="0x85" />
		<enumval name="KEY_F23"				value="0x86" />
		<enumval name="KEY_F24"				value="0x87" />
		<enumval name="KEY_NUMLOCK"			value="0x90" />
		<enumval name="KEY_SCROLL"			value="0x91" description="Scroll Lock." />
		<enumval name="KEY_NUMPADEQUALS"	value="0x92" description="'=' on numeric keypad (NEC PC98)." />

		<enumval name="KEY_LSHIFT"			value="0xA0" />
		<enumval name="KEY_RSHIFT"			value="0xA1" />
		<enumval name="KEY_LCONTROL"		value="0xA2" />
		<enumval name="KEY_RCONTROL"		value="0xA3" />
		<enumval name="KEY_LMENU"			value="0xA4" description="Left Alt." />
		<enumval name="KEY_RMENU"			value="0xA5" description="Right Alt." />
		<!-- KEY_SEMICOLON is actually VK_OEM_1 and might NOT be a semicolon/colon on international keyboards! -->
		<enumval name="KEY_SEMICOLON"		value="0xBA" description="Actually eq. VK_OEM_1 that might not be ;: on international keyboards." />
		<enumval name="KEY_OEM_1"			value="0xBA" />
		<enumval name="KEY_PLUS"			value="0xBB" description="Actually eq. VK_OEM_PLUS that might not be = on international keyboards." />
		<enumval name="KEY_EQUALS"			value="0xBB" description="LEGACY, DO NOT USE. Use KEY_PLUS as it may not be the equals key on interational keyboards." />
		<enumval name="KEY_COMMA"			value="0xBC" />
		<enumval name="KEY_MINUS"			value="0xBD" description="'-' on main keyboard." />
		<enumval name="KEY_PERIOD"			value="0xBE" description="'.' on main keyboard." />
		<!-- KEY_SLASH is actually VK_OEM_2 and might NOT be a slash on international keyboards! -->
		<enumval name="KEY_SLASH"			value="0xBF" description="Actually eq. VK_OEM_2 that might not be /? on international keyboards." />
		<enumval name="KEY_OEM_2"			value="0xBF" />
		<!-- KEY_GRAVE (on US keyboards) is actually VK_OEM_3 and might NOT be a grave(`)/tilde(~) on international keyboards! -->
		<enumval name="KEY_GRAVE"			value="0xC0" description="Actually eq. VK_OEM_3 that might not be ` on international (non-US) keyboards." />
		<enumval name="KEY_OEM_3"			value="0xC0" />
		<!-- KEY_LBRACKET is actually VK_OEM_4 and might NOT be a [{ on international keyboards! -->
		<enumval name="KEY_LBRACKET"		value="0xDB" description="Actually eq. VK_OEM_4 that might not be [{ on international keyboards." />
		<enumval name="KEY_OEM_4"			value="0xDB" />
		<!-- KEY_BACKSLASH is actually VK_OEM_5 and might NOT be a \| on international keyboards! -->
		<enumval name="KEY_BACKSLASH"		value="0xDC" description="Actually eq. VK_OEM_5 that might not be \| on international keyboards." />
		<enumval name="KEY_OEM_5"			value="0xDC" />
		<!-- KEY_RBRACKET is actually VK_OEM_6 and might NOT be a ]} on international keyboards! -->
		<enumval name="KEY_RBRACKET"		value="0xDD" description="Actually eq. VK_OEM_6 that might not be ]} on international keyboards." />
		<enumval name="KEY_OEM_6"			value="0xDD" />
		<!-- KEY_APOSTROPHE is actually VK_OEM_7 and might NOT be a '" on international keyboards! -->
		<enumval name="KEY_APOSTROPHE"		value="0xDE" description="Actually eq. VK_OEM_7 that might not be '&quot; on international keyboards." />
		<enumval name="KEY_OEM_7"			value="0xDE" />
		
		<!--
			NOTE: KEY_OEM_8 is returned in some locals instead of KEY_OEM_3 (e.g. on UK Keyboards). As some keys move arround in different locals and
			we are interested in key positions. For this reason, we treat all keyboards as the american layout (with the extre OEM_102 key) in these key codes.
			If we recieve KEY_OEM_8, we convert it to KEY_GRAVE/KEY_OEM_3 istead. This allows us to have a consistant scan code. For this reason, we do not use
			KEY_OEM_8!
		
			<enumval name="KEY_OEM_8"			value="0xDF" />

		-->
		<enumval name="KEY_OEM_102"			value="0xE2" description="&lt; &lt; | on UK/Germany keyboards." />

		<!--
			NOTE: We use this as ABNT_C2 for Brazilian keyboards. However, if other keyboards end up having extra keys we will reuse this code so we do not
			end up with various region specific keycodes. We can add more if needed!
		-->
		<enumval name="KEY_RAGE_EXTRA1"		value="0xF0" description="Extra key for region specific buttons. More can be added as needed!" />
		<enumval name="KEY_RAGE_EXTRA2"		value="0xF1" description="Extra key for region specific buttons. More can be added as needed!" />
		<enumval name="KEY_RAGE_EXTRA3"		value="0xF2" description="Extra key for region specific buttons. More can be added as needed!" />
		<enumval name="KEY_RAGE_EXTRA4"		value="0xF4" description="Extra key for region specific buttons. More can be added as needed!" />

		<!--
			These keys are not defined in the VK_* list so we are adding our own codes (and values) here so that we can
			distinguish between inputs. All values are defined in the 100-1FF range and will not work with windows.
			on the PC.
		-->
		<enumval name="KEY_NUMPADENTER"				value="0xFD" description="Enter on numeric keypad." />
		<enumval name="KEY_CHATPAD_GREEN_SHIFT"		value="0xFE" description="Xbox 360 chatpad green shift." />
		<enumval name="KEY_CHATPAD_ORANGE_SHIFT"	value="0xFF" description="Xbox 360 chatpad red shift." />


		<!-- OLD - Remove -->
		<!--<enumval name="KEY_ABNT_C1"			value="0x73" description="'/' '?' on Portugese (Brazilian) keyboards." />
		<enumval name="KEY_YEN"				value="0x7D" description="(Japanese keyboard)." />
		<enumval name="KEY_ABNT_C2"			value="0x7E" description="Numpad '.' on Portugese (Brazilian) keyboards." />
		<enumval name="KEY_AT"				value="0x91" description="(NEC PC98)." />
		<enumval name="KEY_COLON"			value="0x92" description="(NEC PC98)." />
		<enumval name="KEY_UNDERLINE"		value="0x93" description="(NEC PC98)." />
		<enumval name="KEY_STOP"			value="0x95" description="(NEC PC98)." />
		<enumval name="KEY_UNLABELED"		value="0x97" description="(J3100)." />
		<enumval name="KEY_CALCULATOR"		value="0xA1" description="Calculator." />
		<enumval name="KEY_POWER"			value="0xDE" description="System Power." />
		<enumval name="KEY_WAKE"			value="0xE3" description="System Wake." />
		<enumval name="KEY_MYCOMPUTER"		value="0xEB" description="My Computer." />
		-->

		<!-- These are not defined in the VK_* list or have alternatives. They originated from the DIK_* list
			<enumval name="KEY_NUMPADCOMMA"		value="0xB3" description="',' on numeric keypad (NEC PC98)." /> 
		-->


		<!-- Unknown/Irrelevent Parameter -->
		<enumval name="IOM_AXIS_UNDEFINED"	value="-1"	description="Intentionally left out of the axis scope."/>
		<enumval name="IOM_UNDEFINED"		value="-1"	description="Intentionally left out of the axis scope."/>
		<enumval name="IOM_WHEEL_UNDEFINED"	value="-1"	description="Intentionally left out of the wheel scope." />
		<enumval name="IOM_POV_UNDEFINED"	value="-1"	description="Intentionally left out of the pov scope." />
		<enumval name="IOM_ANY_BUTTON"		value="-1"	description="Intentionally left out of the scope and is used for sources that specify any button." />
		
		<!-- Mouse Axis Types -->
		<enumval name="IOM_AXIS_X"			value="rage::IOMT_MOUSE_AXIS"	description="Mouse X axis." />
		<enumval name="IOM_AXIS_Y"											description="Mouse Y axis." />
		<enumval name="IOM_IAXIS_X"											description="Inverted mouse X axis." />
		<enumval name="IOM_IAXIS_Y"											description="Inverted mouse Y axis." />
		<!-- The basic amount of Mouse Axis Types -->
		<enumval name="BASIC_MOUSE_AXIS_MAX"								description="NOT an actual type but used to calculate the number of basic mouse axis types." />
		<enumval name="IOM_AXIS_X_LEFT"										description="Mouse X Left only (IOMS_MOUSE_RELATIVEDAXIS and IOMS_MOUSE_SCALEDAXIS only)." />
		<enumval name="IOM_AXIS_X_RIGHT"									description="Mouse X Right only (IOMS_MOUSE_RELATIVEDAXIS and IOMS_MOUSE_SCALEDAXIS only)." />
		<enumval name="IOM_AXIS_Y_UP"										description="Mouse Y Up only (IOMS_MOUSE_RELATIVEDAXIS and IOMS_MOUSE_SCALEDAXIS only)." />
		<enumval name="IOM_AXIS_Y_DOWN"										description="Mouse Y Down only (IOMS_MOUSE_RELATIVEDAXIS and IOMS_MOUSE_SCALEDAXIS only)." />
		<!-- The amount of Mouse Axis Types -->
		<enumval name="MOUSE_AXIS_MAX"										description="NOT an actual type but used to calculate the number of mouse axis types." />
		

		<!-- Mouse Wheel Types -->
		<enumval name="IOM_WHEEL_UP"		value="rage::IOMT_MOUSE_WHEEL"	description="Set to 1 if the mouse wheel was scrolled up." />
		<enumval name="IOM_WHEEL_DOWN"										description="Set to 1 if the mouse wheel was scrolled down." />
		<enumval name="IOM_AXIS_WHEEL_DELTA"								description="The amount the wheel was scrolled (positive for up, negative for down)." />
		<enumval name="IOM_IAXIS_WHEEL_DELTA"								description="Same as IOM_AXIS_WHEEL_DELTA except inverted (i.e. the values are inverted) for compatibility with gamepad axes which might be different." />
		<enumval name="IOM_AXIS_WHEEL"										description="Will produce +/- max-axis based upon the direction scrolled." />
		<enumval name="IOM_IAXIS_WHEEL"										description="Same as IOM_AXIS_WHEEL except inverted (i.e. the values are inverted) for compatibility with gamepad axes which might be different." />
		<enumval name="IOM_AXIS_WHEEL_RELATIVE"								description="Produces +/- max-axis for a period of time based on how many notches were scrolled." />
		<enumval name="IOM_IAXIS_WHEEL_RELATIVE"							description="Same as IOM_AXIS_WHEEL_RELATIVE except inverted (i.e. the values are inverted) for compatibility with gamepad axes which might be different." />
	
			
			
		<!-- Mouse Button Types -->
		<!-- NOTE: These *MUST* be the same as the ioMouse:: button values OR'ed with IOMT_MOUSE_BUTTON. This is so that the value is unique in this
			 enum but can be converted to an equivelent ioMouse:: button equivelent. -->
		<enumval name="MOUSE_LEFT"		value="rage::IOMT_MOUSE_BUTTON | rage::ioMouse::MOUSE_LEFT"			description="Bit defintions of the three standard mouse buttons." />
		<enumval name="MOUSE_RIGHT"		value="rage::IOMT_MOUSE_BUTTON | rage::ioMouse::MOUSE_RIGHT" />
		<enumval name="MOUSE_MIDDLE"	value="rage::IOMT_MOUSE_BUTTON | rage::ioMouse::MOUSE_MIDDLE" />
		<enumval name="MOUSE_EXTRABTN1"	value="rage::IOMT_MOUSE_BUTTON | rage::ioMouse::MOUSE_EXTRABTN1"	description="And then the other mouse buttons that may or may not exist (of which directInput allows for 8)."/>
		<enumval name="MOUSE_EXTRABTN2"	value="rage::IOMT_MOUSE_BUTTON | rage::ioMouse::MOUSE_EXTRABTN2" />
		<enumval name="MOUSE_EXTRABTN3"	value="rage::IOMT_MOUSE_BUTTON | rage::ioMouse::MOUSE_EXTRABTN3" />
		<enumval name="MOUSE_EXTRABTN4"	value="rage::IOMT_MOUSE_BUTTON | rage::ioMouse::MOUSE_EXTRABTN4" />
		<enumval name="MOUSE_EXTRABTN5"	value="rage::IOMT_MOUSE_BUTTON | rage::ioMouse::MOUSE_EXTRABTN5" />
		
			

		<!-- Pad Button Types -->
		<!-- NOTE: These *MUST* be the same as the ioPad:: button values OR'ed with IOMT_PAD_BUTTON. This is so that the value is unique in this
			 enum but can be converted to an equivelent ioPad:: button equivelent. -->
		<enumval name="L2"				value="rage::IOMT_PAD_BUTTON | rage::ioPad::L2_INDEX" />
		<enumval name="R2"				value="rage::IOMT_PAD_BUTTON | rage::ioPad::R2_INDEX" />
		<enumval name="L1"				value="rage::IOMT_PAD_BUTTON | rage::ioPad::L1_INDEX" />
		<enumval name="R1"				value="rage::IOMT_PAD_BUTTON | rage::ioPad::R1_INDEX" />
		<enumval name="RUP"				value="rage::IOMT_PAD_BUTTON | rage::ioPad::RUP_INDEX" />
		<enumval name="RRIGHT"			value="rage::IOMT_PAD_BUTTON | rage::ioPad::RRIGHT_INDEX" />
		<enumval name="RDOWN"			value="rage::IOMT_PAD_BUTTON | rage::ioPad::RDOWN_INDEX" />
		<enumval name="RLEFT"			value="rage::IOMT_PAD_BUTTON | rage::ioPad::RLEFT_INDEX" />
		<enumval name="SELECT"			value="rage::IOMT_PAD_BUTTON | rage::ioPad::SELECT_INDEX" />
		<enumval name="L3"				value="rage::IOMT_PAD_BUTTON | rage::ioPad::L3_INDEX" />
		<enumval name="R3"				value="rage::IOMT_PAD_BUTTON | rage::ioPad::R3_INDEX" />
		<enumval name="START"			value="rage::IOMT_PAD_BUTTON | rage::ioPad::START_INDEX" />
		<enumval name="LUP"				value="rage::IOMT_PAD_BUTTON | rage::ioPad::LUP_INDEX" />
		<enumval name="LRIGHT"			value="rage::IOMT_PAD_BUTTON | rage::ioPad::LRIGHT_INDEX" />
		<enumval name="LDOWN"			value="rage::IOMT_PAD_BUTTON | rage::ioPad::LDOWN_INDEX" />
		<enumval name="LLEFT"			value="rage::IOMT_PAD_BUTTON | rage::ioPad::LLEFT_INDEX" />
		<enumval name="TOUCH"			value="rage::IOMT_PAD_BUTTON | rage::ioPad::TOUCH_INDEX" />

		<!-- Pad Button Types -->
		<!-- NOTE: These *MUST* be the same as the ioPad:: button values OR'ed with IOMT_PAD_INDEX. This is so that the value is unique in this
			 enum but can be converted to an equivelent ioPad:: button equivelent. -->
		<enumval name="L2_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::L2_INDEX" />
		<enumval name="R2_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::R2_INDEX" />
		<enumval name="L1_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::L1_INDEX" />
		<enumval name="R1_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::R1_INDEX" />
		<enumval name="RUP_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::RUP_INDEX" />
		<enumval name="RRIGHT_INDEX"	value="rage::IOMT_PAD_INDEX | rage::ioPad::RRIGHT_INDEX" />
		<enumval name="RDOWN_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::RDOWN_INDEX" />
		<enumval name="RLEFT_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::RLEFT_INDEX" />
		<enumval name="SELECT_INDEX"	value="rage::IOMT_PAD_INDEX | rage::ioPad::SELECT_INDEX" />
		<enumval name="L3_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::L3_INDEX" />
		<enumval name="R3_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::R3_INDEX" />
		<enumval name="START_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::START_INDEX" />
		<enumval name="LUP_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::LUP_INDEX" />
		<enumval name="LRIGHT_INDEX"	value="rage::IOMT_PAD_INDEX | rage::ioPad::LRIGHT_INDEX" />
		<enumval name="LDOWN_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::LDOWN_INDEX" />
		<enumval name="LLEFT_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::LLEFT_INDEX" />
		<enumval name="TOUCH_INDEX"		value="rage::IOMT_PAD_INDEX | rage::ioPad::TOUCH_INDEX" />
		<enumval name="NUMBUTTONS"		value="rage::IOMT_PAD_INDEX | rage::ioPad::NUMBUTTONS" />


		
		<!-- Pad Axis Types -->
		<enumval name="IOM_AXIS_LX"			value="rage::IOMT_PAD_AXIS"	description="Left analog stick X." />
		<enumval name="IOM_AXIS_LY"										description="Left analog stick Y." />
		<enumval name="IOM_AXIS_RX"										description="Right analog stick X." />
		<enumval name="IOM_AXIS_RY"										description="Right analog stick Y." />
		<enumval name="IOM_AXIS_LUP"									description="Left analog stick up, as digital input." />
		<enumval name="IOM_AXIS_LDOWN"									description="Left analog stick down, as digital input." />
		<enumval name="IOM_AXIS_LLEFT"									description="Left analog stick left, as digital input." />
		<enumval name="IOM_AXIS_LRIGHT"									description="Left analog stick right, as digital input." />
		<enumval name="IOM_AXIS_LUR"									description="Left analog stick up + right, as digital input." />
		<enumval name="IOM_AXIS_LUL"									description="Left analog stick up + left, as digital input." />
		<enumval name="IOM_AXIS_LDR"									description="Left analog stick down + right, as digital input." />
		<enumval name="IOM_AXIS_LDL"									description="Left analog stick down + left, as digital input." />
		<enumval name="IOM_AXIS_RUP"									description="Right analog stick up, as digital input." />
		<enumval name="IOM_AXIS_RDOWN"									description="Right analog stick down, as digital input." />
		<enumval name="IOM_AXIS_RLEFT"									description="Right analog stick left, as digital input." />
		<enumval name="IOM_AXIS_RRIGHT"									description="Right analog stick right, as digital input." />
		<enumval name="IOM_AXIS_RUR"									description="Right analog stick up + right, as digital input." />
		<enumval name="IOM_AXIS_RUL"									description="Right analog stick up + left, as digital input." />
		<enumval name="IOM_AXIS_RDR"									description="Right analog stick down + right, as digital input." />
		<enumval name="IOM_AXIS_RDL"									description="Right analog stick down + left, as digital input." />
		<enumval name="IOM_AXIS_DPADX"									description="D-pad left/right as analog." />
		<enumval name="IOM_AXIS_DPADY"									description="D-pad up/down as analog." />
		<enumval name="IOM_AXIS_LY_UP"									description="Left analog stick up, as analog input." />
		<enumval name="IOM_AXIS_LY_DOWN"								description="Left analog stick down, as analog input." />
		<enumval name="IOM_AXIS_LX_LEFT"								description="Left analog stick left, as analog input." />
		<enumval name="IOM_AXIS_LX_RIGHT"								description="Left analog stick right, as analog input." />
		<enumval name="IOM_AXIS_RY_UP"									description="Right analog stick up, as analog input." />
		<enumval name="IOM_AXIS_RY_DOWN"								description="Right analog stick down, as analog input." />
		<enumval name="IOM_AXIS_RX_LEFT"								description="Right analog stick left, as analog input." />
		<enumval name="IOM_AXIS_RX_RIGHT"								description="Right analog stick right, as analog input." />

		<!-- Joystick Button Types -->
		<enumval name="IOM_JOYSTICK_BUTTON1"	value="rage::IOMT_JOYSTICK_BUTTON" />
		<enumval name="IOM_JOYSTICK_BUTTON2" />
		<enumval name="IOM_JOYSTICK_BUTTON3" />
		<enumval name="IOM_JOYSTICK_BUTTON4" />
		<enumval name="IOM_JOYSTICK_BUTTON5" />
		<enumval name="IOM_JOYSTICK_BUTTON6" />
		<enumval name="IOM_JOYSTICK_BUTTON7" />
		<enumval name="IOM_JOYSTICK_BUTTON8" />
		<enumval name="IOM_JOYSTICK_BUTTON9" />
		<enumval name="IOM_JOYSTICK_BUTTON10" />
		<enumval name="IOM_JOYSTICK_BUTTON11" />
		<enumval name="IOM_JOYSTICK_BUTTON12" />
		<enumval name="IOM_JOYSTICK_BUTTON13" />
		<enumval name="IOM_JOYSTICK_BUTTON14" />
		<enumval name="IOM_JOYSTICK_BUTTON15" />
		<enumval name="IOM_JOYSTICK_BUTTON16" />
		<enumval name="IOM_JOYSTICK_BUTTON17" />
		<enumval name="IOM_JOYSTICK_BUTTON18" />
		<enumval name="IOM_JOYSTICK_BUTTON19" />
		<enumval name="IOM_JOYSTICK_BUTTON20" />
		<enumval name="IOM_JOYSTICK_BUTTON21" />
		<enumval name="IOM_JOYSTICK_BUTTON22" />
		<enumval name="IOM_JOYSTICK_BUTTON23" />
		<enumval name="IOM_JOYSTICK_BUTTON24" />
		<enumval name="IOM_JOYSTICK_BUTTON25" />
		<enumval name="IOM_JOYSTICK_BUTTON26" />
		<enumval name="IOM_JOYSTICK_BUTTON27" />
		<enumval name="IOM_JOYSTICK_BUTTON28" />
		<enumval name="IOM_JOYSTICK_BUTTON29" />
		<enumval name="IOM_JOYSTICK_BUTTON30" />
		<enumval name="IOM_JOYSTICK_BUTTON31" />
		<enumval name="IOM_JOYSTICK_BUTTON32" />

		

		<!-- Joystick Button Types -->
		<enumval name="IOM_JOYSTICK_AXIS1"	value="rage::IOMT_JOYSTICK_AXIS" />
		<enumval name="IOM_JOYSTICK_AXIS2" />
		<enumval name="IOM_JOYSTICK_AXIS3" />
		<enumval name="IOM_JOYSTICK_AXIS4" />
		<enumval name="IOM_JOYSTICK_AXIS5" />
		<enumval name="IOM_JOYSTICK_AXIS6" />
		<enumval name="IOM_JOYSTICK_AXIS7" />
		<enumval name="IOM_JOYSTICK_AXIS8" />
		
		
		
		<!-- POV Types -->
		<enumval name="IOM_POV1_UP"		value="rage::IOMT_JOYSTICK_POV" />
		<enumval name="IOM_POV1_RIGHT" />
		<enumval name="IOM_POV1_DOWN" />
		<enumval name="IOM_POV1_LEFT" />
		<enumval name="IOM_POV2_UP" />
		<enumval name="IOM_POV2_RIGHT" />
		<enumval name="IOM_POV2_DOWN" />
		<enumval name="IOM_POV2_LEFT" />
		<enumval name="IOM_POV3_UP" />
		<enumval name="IOM_POV3_RIGHT" />
		<enumval name="IOM_POV3_DOWN" />
		<enumval name="IOM_POV3_LEFT" />
		<enumval name="IOM_POV4_UP" />
		<enumval name="IOM_POV4_RIGHT" />
		<enumval name="IOM_POV4_DOWN" />
		<enumval name="IOM_POV4_LEFT" />
		

	</enumdef>
</ParserSchema>
