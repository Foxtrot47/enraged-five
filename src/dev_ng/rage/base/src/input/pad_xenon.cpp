// 
// input/pad_xenon.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "xinput_config.h"

#if USE_XINPUT

#include "input_channel.h"
#include "pad.h"
#include "widgetpad.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/file_config.h"
#include "input/remote_input.h"
#include "profile/timebars.h"
#include "profile/telemetry.h"
#include "system/xtl.h"
#include "system/param.h"
#include "math/amath.h"

#if !__FINAL
#include "system/param.h"
#endif // !__FINAL

#include "grcore/config.h"

#if RSG_PC || RSG_DURANGO
#include <xinput.h>
#if RSG_PC && __D3D11_1
#pragma comment(lib,"XInput9_1_0.lib")
#else
#pragma comment(lib,"xinput.lib")
#endif // RSG_PC && __D3D11_1
#endif // RSG_PC || RSG_DURANGO

#if !__FINAL
namespace rage
{
	XPARAM(norumble);
	XPARAM(ignoreFocus);
#if RSG_PC && !__TOOL && !RSG_RSC
	extern HWND g_hwndMain;
#endif
} // namespace rage
#endif // !__FINAL
using namespace rage;

static inline u8 RemapAxis(short axis) { 
	return (u8)((axis >> 8) + 128); 
}

u8 xboxAnalogThreshold = 64;
static bool AnaDown(u8 b) { return b >= xboxAnalogThreshold; }

void ioPad::Update(bool ignoreInput) {
	PF_PUSH_TIMEBAR("ClearInputs");
	ClearInputs();
	PF_POP_TIMEBAR();

#if RSG_PC && !__FINAL && !__TOOL && !RSG_RSC
	// ignore controller input if we have multiple instances 
	// running on the same PC and we're in the background.
	if (sm_IgnoreControllerInput)
	{
		bool hasFocus = PARAM_ignoreFocus.Get() || (::GetForegroundWindow() == g_hwndMain);
#if __BANK
		if(!hasFocus)
		{
			// might be in rag so see if the parent window has focus.
			HWND window = ::GetParent(g_hwndMain);
			while(!hasFocus && window != NULL)
			{
				hasFocus = (::GetForegroundWindow() == window);
				window = ::GetParent(window);
			}
		}
#endif // __BANK

		if(!hasFocus)
		{
			StopShakingNow();
			return;
		}
	}
#endif

	if(ignoreInput)
	{
#if __WIN32PC
		PF_PUSH_TIMEBAR("StopShakingNow");
		StopShakingNow();
		PF_POP_TIMEBAR();
#endif
		return;
	}

	XINPUT_STATE state = { 0 };
	
	u32 device = u32(this - sm_Pads);
	
	PF_PUSH_TIMEBAR("XInputGetState");
	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioPad::Update() - XInputGetState() - 1"));
	DWORD result = XInputGetState(device, &state);
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	PF_POP_TIMEBAR();

	// Probably not connected.
	// New behavior in Feb: All pads claim to be disconnected until first use!
	//
	// NOTE: if the pad is disconnected, the result will be ERROR_DEVICE_NOT_CONNECTED, but we may as well assume
	//       it is disconnected even if the result is something other than ERROR_SUCCESS
	if (result != ERROR_SUCCESS REMOTE_INPUT_SUPPORT_ONLY(&& !ioRemoteInput::IsEnabled())) {
		m_IsConnected = false;
		inputDebugf2("Pad %d is no longer connected", device);
		return;
	}

	m_IsConnected = true;

#if REMOTE_INPUT_SUPPORT
	if (ioRemoteInput::IsPadConnected((int)device) == true)
	{
		ioRemoteInput::Get().UpdatePad((int)device, state);
	}
#endif // REMOTE_INPUT_SUPPORT

#if __XENON
	// The PC version of Xinput has several of these symbols defined inconsistently.
	XINPUT_CAPABILITIES caps;
	if (XInputGetCapabilities(device, 0, &caps) == ERROR_SUCCESS) {
		if(caps.Type == XINPUT_DEVTYPE_GAMEPAD)
		{
			switch (caps.SubType) {
			case XINPUT_DEVSUBTYPE_UNKNOWN: m_SubType = UNKNOWN; break;
			case XINPUT_DEVSUBTYPE_ARCADE_STICK: m_SubType = ARCADE_STICK; break;
			case XINPUT_DEVSUBTYPE_FLIGHT_STICK: m_SubType = FLIGHT_STICK; break;
			case XINPUT_DEVSUBTYPE_GAMEPAD: m_SubType = GAMEPAD; break;
			case XINPUT_DEVSUBTYPE_WHEEL: m_SubType = WHEEL; break;
			case XINPUT_DEVSUBTYPE_DANCEPAD: m_SubType = DANCE_COMMANDER; break;
			}
			m_HasForceFeedback = (caps.Flags & XINPUT_CAPS_FFB_SUPPORTED) != 0;
		}
	}
#endif

	XINPUT_GAMEPAD &pad = state.Gamepad;

	// Copy analog axes
	m_Axis[0] = RemapAxis(pad.sThumbLX);
	m_Axis[1] = (u8) ~RemapAxis(pad.sThumbLY);
	m_Axis[2] = RemapAxis(pad.sThumbRX);
	m_Axis[3] = (u8) ~RemapAxis(pad.sThumbRY);

	if (pad.wButtons & XINPUT_GAMEPAD_A) m_Buttons |= RDOWN;
	if (pad.wButtons & XINPUT_GAMEPAD_B) m_Buttons |= RRIGHT;
	if (pad.wButtons & XINPUT_GAMEPAD_X) m_Buttons |= RLEFT;
	if (pad.wButtons & XINPUT_GAMEPAD_Y) m_Buttons |= RUP;

	if (pad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) m_Buttons |= L1;
	if (pad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) m_Buttons |= R1;

	if (AnaDown(pad.bLeftTrigger)) m_Buttons |= L2;
	if (AnaDown(pad.bRightTrigger)) m_Buttons |= R2;

	if (pad.wButtons & XINPUT_GAMEPAD_START) m_Buttons |= START;
	if (pad.wButtons & XINPUT_GAMEPAD_BACK) m_Buttons |= SELECT;
	if (pad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) m_Buttons |= L3;
	if (pad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) m_Buttons |= R3;

	// Translate DPAD
	if (pad.wButtons & XINPUT_GAMEPAD_DPAD_UP) m_Buttons |= LUP;
	if (pad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) m_Buttons |= LDOWN;
	if (pad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) m_Buttons |= LLEFT;
	if (pad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) m_Buttons |= LRIGHT;


	m_AnalogButtons[L2_INDEX] = pad.bLeftTrigger;
	m_AnalogButtons[R2_INDEX] = pad.bRightTrigger;

#if __BANK
	for ( int i = 0; i < 4; ++i )
	{
		if ( m_bankAxis[i] != 0x80 )
		{
			m_Axis[i] = m_bankAxis[i];
		}
	}

	m_Buttons |= m_bankButtons;
	
	if ( m_bankButtons & L2 )
	{
		m_AnalogButtons[L2_INDEX] = m_bankAnalogButtons[L2_INDEX];
	}
	
	if ( m_bankButtons & R2 )
	{
		m_AnalogButtons[R2_INDEX] = m_bankAnalogButtons[R2_INDEX];
	}

	ClearBankInputs();
#endif

#if !__FINAL
	m_HasRumble &= !PARAM_norumble.Get();
#endif // !__FINAL

	// Actuators
	PF_PUSH_TIMEBAR("Actuators");
	if(m_HasRumble)
	{
		if (m_ActuatorDataChanged && !sm_Intercepted)
		{
			XINPUT_VIBRATION v = { m_ActuatorsEnabled? m_ActuatorData[HEAVY_MOTOR] : 0, m_ActuatorsEnabled? m_ActuatorData[LIGHT_MOTOR] : 0 };

			WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioPad::Update() - XInputSetState() - 1"));
			if( ERROR_SUCCESS==XInputSetState(device, &v) )
				m_ActuatorDataChanged = false;
			WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));

			REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::SetPadVibration((int)device, m_ActuatorsEnabled ? m_ActuatorData[HEAVY_MOTOR] : 0, m_ActuatorsEnabled ? m_ActuatorData[LIGHT_MOTOR] : 0));
		}
		else if(sm_Intercepted)
		{
			// clear all rumbles.
			XINPUT_VIBRATION v = { 0, 0 };
			WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioPad::Update() - XInputSetState() - 2"));
			XInputSetState(device, &v);
			WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));

			REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::SetPadVibration((int)device, 0, 0));

			// Ignore rubmles or else they will start when the input is no longer intercepted.
			m_ActuatorDataChanged = false;
		}
	}
	PF_POP_TIMEBAR();

	//UpdateDebug();
}


#if __XENON
static HANDLE s_Notify;
#endif

void ioPad::BeginAll() {
#if __XENON
	s_Notify = XNotifyCreateListener(XNOTIFY_SYSTEM);
#endif

	for (int i=0;i<MAX_PADS;i++) {
		sm_Pads[i].m_PadIndex = i;
	}
}


void ioPad::UpdateAll(bool ignoreInput, bool checkForNewDevice) {
#if __XENON
	DWORD msgId;
	ULONG_PTR value;
	if (s_Notify != NULL && XNotifyGetNext(s_Notify, XN_SYS_UI, &msgId, &value))
		sm_Intercepted = (value != 0);
#endif
	for (int i=0; i<MAX_PADS; i++)
	{
		if(sm_Pads[i].m_IsConnected || checkForNewDevice)
		{
			sm_Pads[i].Update(ignoreInput);
		}
		else
		{
			sm_Pads[i].ClearInputs();
		}
	}
}



void ioPad::CheckForNewlyPluggedInDevices() {

	TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioPad::CheckForNewlyPluggedInDevices()");
	XINPUT_STATE state;
	for(int i = 0; i < MAX_PADS; ++i)
	{
		if(!sm_Pads[i].m_IsConnected)
		{
			WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioPad::CheckForNewlyPluggedInDevices() - XInputGetState()"));
			sm_Pads[i].m_IsConnected = XInputGetState(i, &state) == ERROR_SUCCESS;
			WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
			inputDebugf2("XInput pad %d connected state %d", i, sm_Pads[i].m_IsConnected);
		}
	}
	TELEMETRY_END_ZONE(__FILE__,__LINE__);
}

void ioPad::ReattachAll() {
}

#if __BANK

void ioPad::AddPadWidgets()
{
	bkBank *pBank = BANKMGR.FindBank( "rage - Pad" );
	if ( pBank == NULL )
	{
		pBank = &BANKMGR.CreateBank( "rage - Pad" );
	}

	char name[16];
	sprintf( name, "Pad %d", m_PadIndex );

	if ( bkRemotePacket::IsConnectedToRag() )
	{
		ioWidgetPad* pWidget = rage_new ioWidgetPad( name, m_PadIndex, ioWidgetPad::XENON_PLATFORM );
		pBank->AddWidget( *pWidget );
	}
	else
	{
		pBank->PushGroup( name );
		{
			pBank->PushGroup( "Shoulder Buttons" );
			{
				pBank->AddButton( "Left Button", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L1 ) );
				pBank->AddButton( "Right Button", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R1 ) );
				pBank->AddButton( "Left Trigger", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L2 ) );
				pBank->AddButton( "Right Trigger", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R2 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Left Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData) LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LDOWN ) );
				pBank->AddButton( "Thumb", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Pad" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::ButtonPressedCB ), this,(CallbackData)LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LDOWN ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Right Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData)RRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "Thumb", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Buttons" );
			{
				pBank->AddButton( "A", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "B", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RRIGHT ) );
				pBank->AddButton( "X", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "Y", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RUP ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Other" );
			{
				pBank->AddButton( "Back", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)SELECT ) );
				pBank->AddButton( "Start", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)START ) );
			}
			pBank->PopGroup();
		}
		pBank->PopGroup();
	}
}

void ioPad::AnalogStickPressedCB( CallbackData data )
{
	size_t button = reinterpret_cast<size_t>( data );

	switch ( button )
	{
	case LUP:
		m_bankAxis[1] = 0x0;
		break;
	case LLEFT:
		m_bankAxis[0] = 0x0;
		break;
	case LRIGHT:
		m_bankAxis[0] = 0xff;
		break;
	case LDOWN:
		m_bankAxis[1] = 0xff;
		break;
	case RUP:
		m_bankAxis[3] = 0x0;
		break;
	case RLEFT:
		m_bankAxis[2] = 0x0;
		break;
	case RRIGHT:
		m_bankAxis[2] = 0xff;
		break;
	case RDOWN:
		m_bankAxis[3] = 0xff;
		break;
	}
}

void ioPad::ButtonPressedCB( CallbackData data )
{
	size_t button = reinterpret_cast<size_t>( data );

	m_bankButtons |= int(button);

	if ( button == L2 )
	{
		m_bankAnalogButtons[L2_INDEX] = 0xff;
	}
	else if ( button == R2 )
	{
		m_bankAnalogButtons[R2_INDEX] = 0xff;
	}
}

#endif // __BANK

void ioPad::EndAll() {
#if __XENON
	if (s_Notify != NULL)	// Docs claim NULL, not INVALID_HANDLE_VALUE
		CloseHandle(s_Notify);
#endif
}

void ioPad::StopShakingNow( )
{
	for (int i=0; i<MAX_ACTUATORS; i++)
		m_ActuatorData[i] = 0;

	// If we know the pad is not connected then do not try and stop it rumbling (XInputGetState is SLOW for disconnected devices on PC).
	if(m_IsConnected)
	{
		XINPUT_STATE state;
		u32 device = u32(this - sm_Pads);

		PF_PUSH_TIMEBAR("XInputGetState");
		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioPad::StopShakingNow() - XInputGetState()"));
		DWORD result = XInputGetState(device, &state);
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
		PF_POP_TIMEBAR();
		// Probably not connected.
		// New behavior in Feb: All pads claim to be disconnected until first use!
		//
		// NOTE: if the pad is disconnected, the result will be ERROR_DEVICE_NOT_CONNECTED, but we may as well assume
		//       it is disconnected even if the result is something other than ERROR_SUCCESS
		if (result != ERROR_SUCCESS) {
			m_IsConnected = false;
			return;
		}

		// Only stop rumbling if there is rumble data as The Xbox One controller hangs inside XInputSetState when we call it too often.
		if(m_ActuatorDataChanged || m_ActuatorData[HEAVY_MOTOR] != 0u || m_ActuatorData[LIGHT_MOTOR] != 0u)
		{
			m_ActuatorData[HEAVY_MOTOR] = 0u;
			m_ActuatorData[LIGHT_MOTOR] = 0u;

			XINPUT_VIBRATION v = { 0, 0 };
			WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioPad::StopShakingNow() - XInputSetState()"));
			if(XInputSetState(device, &v) == ERROR_SUCCESS)
			{
				m_ActuatorDataChanged = false;
			}
			else
			{
				m_ActuatorDataChanged = true;	// Need to queue up changes for later
			}
			WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
		}
	}

	REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::SetPadVibration(m_PadIndex, 0, 0));
}

#if RSG_PC && !RSG_FINAL
// Hack to get the PS4 controller working with the debug camera for working at home.
void ioPad::MergeButton(int button, float value)
{
	if(button >= 0 && button < NUMBUTTONS)
	{
		m_IsConnected = true;
		const int buttonBit = 0x1 << button;
		if(value >= 0.5f)
		{
			m_Buttons |= buttonBit;
		}

		if(Norm(m_AnalogButtons[button]) < value)
		{
			m_AnalogButtons[button] = static_cast<u8>(value * 255.f);
		}
	}
}

void ioPad::MergeAxis(int axis, float value)
{
	if(axis >= 0 && axis < 4)
	{
		if(Abs(Norm(m_Axis[axis])) < Abs(value))
		{
			m_IsConnected = true;
			m_Axis[axis] = static_cast<u8>((value * 127.5f) + 127.5f);
		}
	}
}
#endif // RSG_PC && !RSG_FINAL

#endif	// USE_XINPUT
