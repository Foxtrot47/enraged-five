// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 
#ifndef INPUT_VIRTUALKEYBOARD_H
#define INPUT_VIRTUALKEYBOARD_H


#if __PS3
#include <sysutil/sysutil_oskdialog.h>
#include <sys/memory.h>
#elif RSG_DURANGO
#include "system/criticalsection.h"
#endif
#include "string/unicode.h"

#include "file/file_config.h"

#if __STEAM_BUILD
#pragma warning(disable: 4265)
#include "../../3rdParty/Steam/public/steam/steam_api.h"
#pragma warning(error: 4265)
#endif // __STEAM_BUILD

namespace rage
{

// PURPOSE: ioVirtualKeyboard is a class used to display the system keyboard on consoles, to facilitate text input
class ioVirtualKeyboard
{
public:
	enum eKBState
	{
		kKBState_INVALID = -1,

		kKBState_PENDING,
		kKBState_SUCCESS,
		kKBState_CANCELLED,
		kKBState_FAILED,

		kKBState_COUNT
	};

	enum eTextType
	{
		kTextType_INVALID = -1,

		kTextType_DEFAULT,
		kTextType_EMAIL,
		kTextType_PASSWORD,
		kTextType_NUMERIC,
		kTextType_ALPHABET,
		kTextType_GAMERTAG,
		kTextType_FILENAME, // Only for PC

		kTextType_COUNT
	};

	enum eAlphabetType
	{
		kAlphabetType_INVALID = -1,

		kAlphabetType_BASIC_ENGLISH,	// English excluding accented characters. Differs slightly between platforms, but is subset of basic ASCII.
		kAlphabetType_ENGLISH,			
		kAlphabetType_CYRILLIC,
		kAlphabetType_JAPANESE,
		kAlphabetType_KOREAN,
		kAlphabetType_CHINESE,
		kAlphabetType_CHINESE_SIMPLIFIED,
		kAlphabetType_POLISH,

		kAlphabetType_COUNT
	};

	static const int MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD = 512;	//	On PS3, this is the value of CELL_OSKDIALOG_STRING_SIZE. I'm not sure what the 360 limit is.
	static const int VIRTUAL_KEYBOARD_RESULT_BUFFER_SIZE = ( MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD * 3 ) + 1;

	ioVirtualKeyboard();
	~ioVirtualKeyboard();

	struct Params
	{
		enum eMultiLineUsage
		{
			FORCE_SINGLE_LINE,
			FORCE_MULTI_LINE,
			USE_MULTI_LINE_FOR_LONG_STRINGS
		};

		Params(){ Reset(); } // GTAV
		eTextType	m_KeyboardType;
		eAlphabetType m_AlphabetType;
		const char16*		m_Title;
		const char16*		m_Description; /* Xenon only */
		const char16*		m_InitialValue;
		int			m_MaxLength;
		int			m_PlayerIndex;
		int			m_UserId;
		eMultiLineUsage	m_MultiLineUsage;	/* PC only */

		void Reset()
		{
			m_KeyboardType = kTextType_DEFAULT;
			m_AlphabetType = kAlphabetType_INVALID;
			m_Title = NULL;
			m_Description = NULL;
			m_InitialValue = NULL;
			m_MaxLength = MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD;
			m_PlayerIndex = 0;
			m_UserId = -1;
			m_MultiLineUsage = USE_MULTI_LINE_FOR_LONG_STRINGS;
		}
	};

	bool Show( const Params& params );
	void Update();
	void Cancel();

	bool		IsPending	() const	{ return GetState()==kKBState_PENDING;	}
	bool		Succeeded	() const	{ return GetState()==kKBState_SUCCESS; }
	eKBState	GetState	() const	{ return m_SystemUIOpen ? kKBState_PENDING : m_State; }
	char16*		GetResult	()			;

protected:
	bool StartInput( const Params& params );

	// NOTES:	On steam builds we could either be using the Steam Big Picture text input overlay or an alternative text
	//			input box (e.g. for keyboard input).
#if __STEAM_BUILD
	// PURPOSE:	Shows the steam overlay keyboard for Steam Big Picture mode.
	// NOTES:	This should be called from StartInput(). Once called, IsUsingSteamInputOverlay() will indicate if the Steam
	//			 Big Picture keyboard was successfully set up and we are using the big picture keyboard mode.
	void StartSteamInput( const Params& params );

	// PURPOSE:	Indicates if we are using the Steam Big Picture text input overlay.
	bool IsUsingSteamInputOverlay() const;

	// PURPOSE:	Updates the steam overlay if we are using it.
	void UpdateSteamInput();

	// PURPOSE:	Callback function to indicate the user has dismissed the steam text input.
	STEAM_CALLBACK(ioVirtualKeyboard, OnSteamInputDismissed, GamepadTextInputDismissed_t, m_SteamCallbackHandle);

	// PURPOSE: Indicates the steam of the steam overlay keyboard.
	// NOTES:	On steam builds we could either be using the text input overlay (steam big picture mode) or a text input box.
	//			If this state is kKBState_INVALID then we are *not* using the steam overlay keyboard and are using another form
	//			of text input.
	eKBState m_SteamKeyboardState;
#endif // __STEAM_BUILD

	enum 
	{
		KB_MAX_NUM_MESSAGES			= 30,
		KB_MAX_MESSAGES_PER_QUEUE	= 5,
		KB_DEFAULT_MESSAGE_PRIORITY	= 10,
		KB_DEFAULT_MESSAGE_DELAY_MS = 1000,
	};

	// Keyboard support
	char16		m_Result[MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD];
	char16		m_InitialValue[MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD];
	eKBState	m_State;
	bool		m_ProcessKeyboard;
	bool		m_SystemUIOpen;

#if __XENON
	void*	m_pXOverlapped;
#elif __PS3
	enum{
		MODE_IDLE = 0,
		MODE_OPEN,
		MODE_RUNNING,
		MODE_EXIT,
		MODE_CLOSE
	};

	static int oskdialog_mode;
	static void sysutil_callback( uint64_t status, uint64_t param, void * userdata );
	CellOskDialogInputFieldInfo			m_KeyboardInputInfo;
	CellOskDialogCallbackReturnParam	m_KeyboardOutputInfo;
	CellOskDialogParam					m_KeyboardDialogParam;

public:
	CellOskDialogCallbackReturnParam* GetKeyboardOutputInfo( void ) { return &m_KeyboardOutputInfo; }
#elif RSG_DURANGO
	// Durango uses async callbacks.
	sysCriticalSectionToken m_Cs;

	// As Durango is async we need to store the result between frames when it comes in. Also, a pointer to m_Result
	// is returned from GetResult() making m_Resut not thread safe!
	char16	 m_BufferedResult[MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD];
	eKBState m_BufferedState;
	int		 m_MaxLength;
#endif

private:
	// GTA5 doesn't support Chinese spaces
	void ReplaceUnsupportedSpaces(char16* str);

};

} // namespace rage

#endif 