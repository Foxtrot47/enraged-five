//
// input/eventq.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "eventq.h"

#include "keyboard.h"
#include "keys.h"
#include "mouse.h"

#include "system/ipc.h"
#if __WIN32PC
#if __BANK
#include "bank/bkmgr.h"
#include "rline/rlpc.h"
#endif
#include "system/xtl.h"
#endif
#include "string/string.h"

using namespace rage;

int ioEventQueue::sm_Head, ioEventQueue::sm_Tail;
ioEvent ioEventQueue::sm_TheQueue[MAX_EVENTS];

#define DEBUG_EVENTS 0

#if !__FINAL
sysNamedPipe ioEventQueue::sm_Pipe;
#endif

// const int COMMAND_SIZE = 5; -- this is an enum in the class now!
ioEventQueue::SCommand* ioEventQueue::sm_pBuffer = NULL;
int ioEventQueue::sm_numCommands = 0;
int ioEventQueue::sm_start = 0;
int ioEventQueue::sm_count = 0;

#if __WIN32PC && __BANK
static bool dont_post() { return false; }
bool (*ioEventQueue::ShouldPostMessages)() = dont_post;
#endif

ioEventQueue EVENTQ;


void ioEventQueue::ConnectPipe( const char* /*pipeName*/, const char* NOTFINAL_ONLY(addr), int NOTFINAL_ONLY(socketPort), int commandBufferSize )
{	
#if !__FINAL
	// connect to pipe:
	bool result=false;
	do {
		result = sm_Pipe.Open( addr, socketPort );
		if (!result)
		{
			sysIpcSleep(100);
		}
	} while(!result);

	sm_Pipe.SetNoDelay( true );
#endif // !__FINAL

    // make sure we have an even number of commands
    int modResult = commandBufferSize % COMMAND_SIZE;
    sm_numCommands = (commandBufferSize + modResult) / COMMAND_SIZE;

    Assert( sm_pBuffer == NULL );
    sm_pBuffer = rage_new SCommand[sm_numCommands * COMMAND_SIZE];
}

void ioEventQueue::DisconnectPipe()
{
#if !__FINAL
    sm_Pipe.Close();
#endif // !__FINAL
    
    if ( sm_pBuffer )
    {
        delete [] sm_pBuffer;
    }
}

bool ioEventQueue::Pop(ioEvent &dest) {
	if (sm_Head != sm_Tail) {
		sm_Tail = (sm_Tail + 1) & (MAX_EVENTS-1);
		dest = sm_TheQueue[sm_Tail];
		return true;
	}
	else
		return false;
}


bool ioEventQueue::Peek(ioEvent &dest,int &offset) {
	int tail = (sm_Tail + offset) & (MAX_EVENTS-1);
	if (sm_Head != tail) {
		dest = sm_TheQueue[tail];
		++offset;
		return true;
	}
	else
		return false;
}

#define QM(name) \
	ioMouse::m_X = (int)(sm_pBuffer[i+2].f*g_WindowWidth); \
	ioMouse::m_Y = (int)(sm_pBuffer[i+3].f*g_WindowHeight); \
	ioMouse::m_dZ = (int)(short)(sm_pBuffer[i+4].u)/120; \
	ioEventQueue::Queue(ioEvent::name,ioMouse::m_X,ioMouse::m_Y,TranslateButtons(sm_pBuffer[i+1].u))

enum
{
	// keyboard commands:
	QUEUE_CHAR			=0x0102,	// WM_CHAR
	QUEUE_SYSCHAR		=0x0106,	// WM_SYSCHAR

	QUEUE_KEYDOWN		=0x0100,	// WM_KEYDOWN
	QUEUE_SYSKEYDOWN	=0x0104,	// WM_SYSKEYDOWN

	QUEUE_KEYUP			=0x0101,	// WM_KEYDOWN
	QUEUE_SYSKEYUP		=0x0105,	// WM_SYSKEYDOWN

	// mouse commands:
	QUEUE_MOUSEMOVE		=0x0200,	// WM_MOUSEMOVE

	QUEUE_LBUTTONDOWN	=0x0201,	// WM_LBUTTONDOWN
	QUEUE_LBUTTONUP		=0x0202,	// WM_LBUTTONUP
	
	QUEUE_RBUTTONDOWN	=0x0204,	// WM_RBUTTONDOWN
	QUEUE_RBUTTONUP		=0x0205,	// WM_RBUTTONUP

	QUEUE_MBUTTONDOWN	=0x0207,	// WM_MBUTTONDOWN
	QUEUE_MBUTTONUP		=0x0208,	// WM_MBUTTONUP

	QUEUE_LBUTTON		=0x0001,	// MK_LBUTTON
	QUEUE_RBUTTON		=0x0002,	// MK_RBUTTON
	QUEUE_SHIFT			=0x0004,	// MK_SHIFT
	QUEUE_CONTROL		=0x0008,	// MK_CONTROL
	QUEUE_MBUTTON       =0x0010		// MK_MBUTTON
};

#if __WIN32PC
// keyboard commands:
CompileTimeAssert(QUEUE_CHAR==WM_CHAR);
CompileTimeAssert(QUEUE_SYSCHAR==WM_SYSCHAR);

CompileTimeAssert(QUEUE_KEYDOWN==WM_KEYDOWN);
CompileTimeAssert(QUEUE_SYSKEYDOWN==WM_SYSKEYDOWN);

CompileTimeAssert(QUEUE_KEYUP==WM_KEYUP);
CompileTimeAssert(QUEUE_SYSKEYUP==WM_SYSKEYUP);

// mouse commands:
CompileTimeAssert(QUEUE_MOUSEMOVE==WM_MOUSEMOVE);

CompileTimeAssert(QUEUE_LBUTTONDOWN==WM_LBUTTONDOWN);
CompileTimeAssert(QUEUE_LBUTTONUP==WM_LBUTTONUP);

CompileTimeAssert(QUEUE_RBUTTONDOWN==WM_RBUTTONDOWN);
CompileTimeAssert(QUEUE_RBUTTONUP==WM_RBUTTONUP);

CompileTimeAssert(QUEUE_MBUTTONDOWN==WM_MBUTTONDOWN);
CompileTimeAssert(QUEUE_MBUTTONUP==WM_MBUTTONUP);

CompileTimeAssert(QUEUE_LBUTTON==MK_LBUTTON);
CompileTimeAssert(QUEUE_RBUTTON==MK_RBUTTON);
CompileTimeAssert(QUEUE_SHIFT==MK_SHIFT);
CompileTimeAssert(QUEUE_CONTROL==MK_CONTROL);
CompileTimeAssert(QUEUE_MBUTTON==MK_MBUTTON);
#endif

#if !__FINAL
static unsigned char s_VirtualKeyToEventKey[] = {
		0,0,0,0,0,0,0,0,KEY_BACK,KEY_TAB,0,0,0,KEY_RETURN,0,0,KEY_LSHIFT,KEY_LCONTROL,KEY_LMENU,KEY_PAUSE, // 0-19
		KEY_CAPITAL,0,0,0,0,0,0,KEY_ESCAPE,0,0,0,0,KEY_SPACE,KEY_PRIOR,KEY_NEXT,KEY_END,KEY_HOME,KEY_LEFT,KEY_UP,KEY_RIGHT, // 20-39
		KEY_DOWN,0,0,0,KEY_SYSRQ,KEY_INSERT,KEY_DELETE,0,KEY_0,KEY_1,KEY_2,KEY_3,KEY_4,KEY_5,KEY_6,KEY_7,KEY_8,KEY_9,0,0, // 40-59
		0,0,0,0,0,KEY_A,KEY_B,KEY_C,KEY_D,KEY_E,KEY_F,KEY_G,KEY_H,KEY_I,KEY_J,KEY_K,KEY_L,KEY_M,KEY_N,KEY_O,  // 60-79
		KEY_P,KEY_Q,KEY_R,KEY_S,KEY_T,KEY_U,KEY_V,KEY_W,KEY_X,KEY_Y,KEY_Z,KEY_LWIN,KEY_RWIN,KEY_APPS,0,0,KEY_NUMPAD0,KEY_NUMPAD1,KEY_NUMPAD2,KEY_NUMPAD3, // 80-99
		KEY_NUMPAD4,KEY_NUMPAD5,KEY_NUMPAD6,KEY_NUMPAD7,KEY_NUMPAD8,KEY_NUMPAD9,KEY_MULTIPLY,KEY_ADD,0,KEY_SUBTRACT,KEY_DECIMAL,KEY_DIVIDE,KEY_F1,KEY_F2,KEY_F3,KEY_F4,KEY_F5,KEY_F6,KEY_F7,KEY_F8, // 100-119
		KEY_F9,KEY_F10,KEY_F11,KEY_F12,KEY_F13,KEY_F14,KEY_F15,0,0,0,0,0,0,0,0,0,0,0,0,0, // 120-139
		0,0,0,0,KEY_NUMLOCK,KEY_SCROLL,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 140-159
		KEY_LSHIFT,KEY_RSHIFT,KEY_LCONTROL,KEY_RCONTROL,KEY_LMENU,KEY_RMENU,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  //160-179
		0,0,0,0,0,0,KEY_SEMICOLON,KEY_EQUALS,KEY_COMMA,KEY_MINUS,KEY_PERIOD,KEY_SLASH,KEY_GRAVE,0,0,0,0,0,0,0,  //180-199
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  //200-219
		KEY_BACKSLASH,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  //220-239
};

static int TranslateButtons(int fkey) {
	int flags = 0;
	if (fkey & QUEUE_LBUTTON) flags |= ioMouse::MOUSE_LEFT;
	if (fkey & QUEUE_RBUTTON) flags |= ioMouse::MOUSE_RIGHT;
	if (fkey & QUEUE_MBUTTON) flags |= ioMouse::MOUSE_MIDDLE;
	return flags;
}

int ioEventQueue::TranslateKey(unsigned vkey) {
	if (vkey >= sizeof(s_VirtualKeyToEventKey))
		return 0;
	else
		return s_VirtualKeyToEventKey[vkey];
}
#endif // __DEV

#if __BE
inline unsigned short SWAPS(unsigned short s) { return (s << 8) | (s >> 8); }
inline unsigned SWAPL(unsigned s) {
	return (s >> 24) | ((s >> 8) & 0xFF00) | ((s << 8) & 0xFF0000) | (s << 24);
}
#endif

void ioEventQueue::UpdateFromPipe() 
{
	CompileTimeAssert(sizeof(ioEventQueue::SCommand) == 4);	// MAK: There is a >>2 down in the code that seems to assume that the size is 4 bytes.

#if !__FINAL
	// static int s_Handle = -999;
 
	//Displayf( "ioEventQueue" );

	if ( sm_Pipe.IsValid() )
	{
		bool leftovers = false;
#if __BE
		bool swapBits = true;
#endif

		if ( sm_count == 0 )
		{
			// no leftovers, so read from pipe
			sm_start = 0;
			sm_count = sm_Pipe.Read( sm_pBuffer, sm_numCommands * COMMAND_SIZE * sizeof(ioEventQueue::SCommand) ) >> 2;
		}
		else
		{
			// we're picking up where we left off
			leftovers = true;
#if __BE
			swapBits = false;
#endif
		}

		const int keysUsedSize = sizeof(s_VirtualKeyToEventKey);
		unsigned char keysUsed[keysUsedSize];
		memset( keysUsed, 0, keysUsedSize );
		unsigned char leftMouseButton = 0, rightMouseButton = 0, middleMouseButton = 0;

		Assert(sm_count <= sm_numCommands * COMMAND_SIZE);
		
		for ( int i = sm_start; i < sm_count; i += COMMAND_SIZE ) 
		{
			//Displayf( "%08x %08x %08x", sm_pBuffer[i], sm_pBuffer[i+1], sm_pBuffer[i+1] );
			int k, tk;
#if __BE
			// be careful, if we had leftovers we shouldn't swap the bits again
			if ( swapBits )
			{
				// swap bytes:
				for (int j=0; j<COMMAND_SIZE; j++)
				{
					sm_pBuffer[i+j].u = SWAPL(sm_pBuffer[i+j].u);
				}
			}
			else
			{
				swapBits = true;
			}
#endif

			switch ( sm_pBuffer[i].u ) 
			{
			case QUEUE_CHAR:
			case QUEUE_SYSCHAR:
				Queue( ioEvent::IO_KEY_CHAR, 0, 0, sm_pBuffer[i+1].u );

#if DEBUG_EVENTS
				Displayf( "EventQ: got key down: %d", sm_pBuffer[i+1].u );
#endif
				break;
			case QUEUE_KEYDOWN:
			case QUEUE_SYSKEYDOWN:
				k = sm_pBuffer[i+1].u;
				if ( (k < keysUsedSize) && keysUsed[k] )
				{
					// don't process the same key twice in one update loop
					sm_start = i;
					return;
				}

				keysUsed[k] = 1;

#if __WIN32PC && __BANK
				if(bkManager::GetRenderWindow() != NULL)
				{
					// the Social Club DLL needs to receive keyboard input via the windows message pump
					if((k == VK_HOME) || (*ShouldPostMessages)())
					{
						PostMessageA(g_hwndMain,WM_KEYDOWN,k,0);
					}
				}
#endif

 				tk = TranslateKey( k );
 				Queue( ioEvent::IO_KEY_DOWN, 0, 0, tk );
 				ioKeyboard::SetKey( tk, true );

#if DEBUG_EVENTS
				Displayf( "EventQ: got key down: %d",tk);
#endif
				break;
			case QUEUE_KEYUP:
			case QUEUE_SYSKEYUP:
				k = sm_pBuffer[i+1].u;
				if ( (k < keysUsedSize) && keysUsed[k] )
				{
					// don't process the same key twice in one update loop
					sm_start = i;
					return;
				}

				keysUsed[k] = 1;

#if __WIN32PC && __BANK && !__TOOL
				if(bkManager::GetRenderWindow() != NULL)
				{
					// the Social Club DLL needs to receive keyboard input via the windows message pump
					if((k == VK_HOME || k == VK_ESCAPE))
					{
						PostMessageA(g_hwndMain,WM_KEYUP,k,0);
					}
				}
#endif

				tk = TranslateKey( k );
 				Queue( ioEvent::IO_KEY_UP, 0, 0, tk );
 				ioKeyboard::SetKey( tk, false );

#if DEBUG_EVENTS
				Displayf("EventQ: got key up: %d",tk);
#endif
				break;
			case QUEUE_MOUSEMOVE:
				QM( IO_MOUSE_MOVE );
#if DEBUG_EVENTS
				Displayf( "EventQ: got mouse move" );
#endif
				break;
			case QUEUE_LBUTTONDOWN:
				if ( leftMouseButton )
				{
					// don't process the same button twice in one update loop
					sm_start = i;
					return;
				}

				leftMouseButton = 1;
				ioMouse::m_WindowButtons |= ioMouse::MOUSE_LEFT;
				QM( IO_MOUSE_LEFT_DOWN );

#if DEBUG_EVENTS
				Displayf( "EventQ: got left mouse button down" );
#endif
				break;
			case QUEUE_LBUTTONUP:
				if ( leftMouseButton )
				{
					// don't process the same button twice in one update loop
					sm_start = i;
					return;
				}

				leftMouseButton = 1;
				ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_LEFT;
				QM( IO_MOUSE_LEFT_UP );

#if DEBUG_EVENTS
				Displayf( "EventQ: got left mouse button up" );
#endif
				break;
			case QUEUE_RBUTTONDOWN:
				if ( rightMouseButton )
				{
					// don't process the same button twice in one update loop
					sm_start = i;
					return;
				}

				rightMouseButton = 1;
				ioMouse::m_WindowButtons |= ioMouse::MOUSE_RIGHT;
				QM( IO_MOUSE_RIGHT_DOWN );

#if DEBUG_EVENTS
				Displayf( "EventQ: got right mouse button down" );
#endif
				break;
			case QUEUE_RBUTTONUP:
				if ( rightMouseButton )
				{
					// don't process the same button twice in one update loop
					sm_start = i;
					return;
				}

				rightMouseButton = 1;
				ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_RIGHT;
				QM( IO_MOUSE_RIGHT_UP );

#if DEBUG_EVENTS
				Displayf( "EventQ: got right mouse button up" );
#endif
				break;
			case QUEUE_MBUTTONDOWN:
				if ( middleMouseButton )
				{
					// don't process the same button twice in one update loop
					sm_start = i;
					return;
				}

				middleMouseButton = 1;
				ioMouse::m_WindowButtons |= ioMouse::MOUSE_MIDDLE;
				QM( IO_MOUSE_MIDDLE_DOWN );

#if DEBUG_EVENTS
				Displayf( "EventQ: got middle mouse button down" );
#endif

				break;
			case QUEUE_MBUTTONUP:
				if ( middleMouseButton )
				{
					// don't process the same button twice in one update loop
					sm_start = i;
					return;
				}

				middleMouseButton = 1;
				ioMouse::m_WindowButtons &= ~ioMouse::MOUSE_MIDDLE;
				QM( IO_MOUSE_MIDDLE_UP );

#if DEBUG_EVENTS
				Displayf( "EventQ: got middle mouse button up" );
#endif
				break;
			default:
				Errorf( "Unknown event" );
				break;
			}
		}

		// if we got to here, we processed everything in the buffer
		sm_count = 0;

		if ( leftovers )
		{
			// if we had leftovers from the last time we called this function, call it again so we can catch up.
			UpdateFromPipe();
		}
	}
#endif
}

void ioEventQueue::Queue(ioEvent::ioEventType type,int x,int y,int data) {
	// Merge redundant events together if possible
	if (sm_Head != sm_Tail && (type == ioEvent::IO_MOUSE_MOVE) && sm_TheQueue[sm_Head].m_Type == type) 
	{
		//Displayf( "ioMouse (%d,%d)", x, y );
		sm_TheQueue[sm_Head].m_X = x;
		sm_TheQueue[sm_Head].m_Y = y;
		return;
	}

	// Check for overflow.
	int newHead = (sm_Head + 1) & MAX_EVENTS - 1;

	// Don't spew errors... a queue full state can easily happen if the
	// client doesn't use eventq and never calls Pop.
	if (newHead == sm_Tail)
		return;

	sm_TheQueue[newHead].m_Type = type;
	sm_TheQueue[newHead].m_X = x;
	sm_TheQueue[newHead].m_Y = y;
	sm_TheQueue[newHead].m_Data = data;
	sm_Head = newHead;
}


/* void ioEventQueue::Command(void *cmd) {
	Queue(ioEvent::IO_USER_COMMAND,0,0,(int)(size_t)cmd);
} */

#undef QM
