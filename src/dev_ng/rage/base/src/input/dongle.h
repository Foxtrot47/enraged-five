//
// input/dongle.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_DONGLE_H
#define INPUT_DONGLE_H

#include "bank/bank.h"
#include "file/device.h"

#if __XENON

namespace rage {

//	PURPOSE
//		Implements a simple copy protection mechanism.
//
//  DESCRIPTION
//		It will only allow to play the game if a special file exists on the
//		memory card. The idea is to hand out prepared memory cards that work as
//		a dongle.
//		
//		PS2 specific:
//		The file itself is copy protected, invalid from a users point of view,
//		contains a code and uses a special time stamp.  The file can not be copied
//		with the PS2 build in browser and is not accepted as a legal file if it's
//		copied with Sony's QA tool.
// <FLAG Component>
class ioDongle
{
public:
	typedef void (*DisplayMessageFunc)(const char*);

	//	PURPOSE: Creates the dongle file in the root directory of a memory card in slot 1.
	static void WriteCodeFile(const char* memcardString, const char* macaddrString, const rage::fiDevice::SystemTime PS3_ONLY(expiryDate), bool PS3_ONLY(bExpires), const char* mcFileName);

	// PURPOSE:
	//	Tries to read the dongle file from the memory card in slot 1. If it
	//	doesn't succeed (for whatever reason) it shows an error message and
	//	locks up.
	static bool ReadCodeFile(const char* memcardString, const char* mcFileName, char * debugString);

	// PURPOSE:
	//	Sets a callback to call when the ioDongle object needs to report back to the user.
	// NOTES:
	//	Reasons for displaying a message include reporting the results of writing the dongle file and
	//	reading a missing or invalid dongle file.
	//  If no message function is set, Displayf will be used.
	static void SetDisplayMessageFunc(DisplayMessageFunc func);

#if __XENON
	// PURPOSE:
	//  Only necessary to show the retarded xenon device selector ui, which needs a d3d present called
	//  at a reasonable rate to function properly
	// NOTES:
	//  Returns true when write has failed or succeeded
	static bool CheckWriteCodeFile();
#endif

#if __PS3
	static void RemoveDongle(const char * file);
	static bool LegacyReadCodeFile( const char* encodeString, const char* readBuffer, size_t readBufferSize);
#endif

protected:
	static DisplayMessageFunc sm_DisplayMessage;

	// PURPOSE:
	//	Some very cheesy encoding to avoid that the string that is written
	//	to the memory card directly shows up in the executeable.
	static void Encode(char *buffer, int sizeInBytes);
};

inline void ioDongle::SetDisplayMessageFunc(DisplayMessageFunc func)
{
	sm_DisplayMessage=func;
}


}	// namespace rage

#endif // __XENON

#endif //INPUT_DONGLE_H
