//
// input/mapper.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mapper.h"
#include "input.h"
#if !IS_CONSOLE
#include "joystick.h"
#endif
#include "file/asset.h"
#include "system/magicnumber.h"

#if IS_CONSOLE && __BANK
#include "bank/bkmgr.h"
#endif

#if !IS_CONSOLE
// The maximum number of devices (gamepad/joysticks) supported.
#define MAX_SUPPORTED_DEVICES 4
#endif // !IS_CONSOLE

using namespace rage;

// Setup a static unknown input source.
const ioSource ioSource::UNKNOWN_SOURCE = ioSource();

const ioValue::DisableOptions ioValue::DEFAULT_DISABLE_OPTIONS;

// Setup a static settings for device dependent dead zoning.
const ioValue::ReadOptions ioValue::DEFAULT_OPTIONS(ioValue::ReadOptions::SETUP_DEFAULT_BOUND_OPTIONS);
const ioValue::ReadOptions ioValue::DEFAULT_UNBOUND_OPTIONS(ioValue::ReadOptions::SETUP_DEFAULT_UNBOUND_OPTIONS);

#if __PPU
const float ioValue::DEFAULT_DEAD_ZONE_VALUE	= 0.33f;
#else
const float ioValue::DEFAULT_DEAD_ZONE_VALUE	= 0.25f;
#endif

const float ioValue::DEFAULT_MIN_DEAD_ZONE_VALUE = 0.075f;

const ioValue::ReadOptions ioValue::NO_DEAD_ZONE(0.0f, ioValue::ReadOptions::F_NO_DEAD_ZONE);
const ioValue::ReadOptions ioValue::ALWAYS_DEAD_ZONE(DEFAULT_DEAD_ZONE_VALUE, ioValue::ReadOptions::F_DEAD_ZONE_ALL_SOURCES);

#if RSG_EXTRA_MOUSE_SUPPORT
// These define the scale/resolution of the axis.
const ioValue::ValueType ioValue::MAX_AXIS		= 1.0f;
const ioValue::ValueType ioValue::MIN_AXIS		= -ioValue::MAX_AXIS;
const ioValue::ValueType ioValue::CENTER_AXIS	= 0.0f;

#else
// These define the scale/resolution of the axis.
const ioValue::ValueType ioValue::MAX_AXIS		= 255;
const ioValue::ValueType ioValue::MIN_AXIS		= -ioValue::MAX_AXIS;
const ioValue::ValueType ioValue::CENTER_AXIS	= 0;
#endif // RSG_EXTRA_MOUSE_SUPPORT

// Used for converted old values to new values and vice-versa.
const s32 ioValue::LEGACY_AXIS_SHIFT				= 127;
const ioValue::ValueType ioValue::LEGACY_AXIS_SCALE = ioValue::MAX_AXIS / static_cast<ioValue::ValueType>(ioValue::LEGACY_AXIS_SHIFT);

// For speed so we do not have to cast constantly. Always ensure these are identical.
const float ioValue::MAX_AXIS_F	= (float)ioValue::MAX_AXIS;

// Button on and off values, analogue buttons can be in between. For this reason it is best
// to ensure they are in the same scale as a positive axis. This is because a lot of old code
// is dependent on these being the same scale as an axis. It may be possible to change these
// at a later date.
const float ioValue::BUTTON_DOWN_THRESHOLD = 0.5f;

// Analogue buttons can have any value. This is the value to use to represent the button is
// in a state other than up (off).
const float ioValue::ANALOG_BUTTON_DOWN_THRESHOLD = 0.039f;

bool ioMapper::sm_EnableKeyboard = true;

const ioMapper::ScanOptions ioMapper::DEFAULT_DEVICE_SCAN_OPTIONS(ioMapper::ScanOptions::SETUP_DEFAULT_DEVICE_OPTIONS);

ioMapper::ioMapper() 
{
	m_PadIndex = 0;
	Reset();
}

int ioMapper::Map(ioMapperSource source,u32 param,ioValue &value,int padOverride) {
	if(Verifyf(m_Count < MaxMappings, "Too many mappings mapped inside mapper, this means some controls will not work!"))
	{
		Assertf(ioSource::IsValidDevice(padOverride), "Invalid device id, using default!");
		if(!ioSource::IsValidDevice(padOverride))
			padOverride = ioSource::IOMD_UNKNOWN;

		m_Sources[m_Count] = source;
		m_Parameters[m_Count] = param | (padOverride << 24);
		m_Values[m_Count] = &value;
		return m_Count++;
	}
	return -1; // overflow.
}

int ioMapper::Map(ioMapperSource source,u32 param,u32 param2,ioValue &value) {
	Map(source,param,value);
	return Map(source,param2,value);
}

void rage::ioMapper::Change(const u32 mappingIndex, const ioSource& newSource, ioValue &value)
{
	u32 mappingCount = 0u;
	for (int i=0; i<m_Count; i++)
	{
		if (m_Values[i] == &value)
		{
			if(mappingCount == mappingIndex)
			{
				m_Sources[i] = newSource.m_Device;
				s32 device = newSource.m_DeviceIndex;

				Assertf(ioSource::IsValidDevice(device), "Device id is invalid, Using default device!");
				if(!ioSource::IsValidDevice(device))
					device = ioSource::IOMD_UNKNOWN;

				m_Parameters[i] = newSource.m_Parameter | (device << 24);
				return;
			}
			++mappingCount;
		}
	}
	Map(newSource.m_Device,newSource.m_Parameter,value, newSource.m_DeviceIndex);
}

bool ioMapper::Remove(const ioSource& source, const ioValue& value)
{
	// NOTE: The order of the arrays IS important so we cannot do a quick remove by swapping the last entry with
	// the one to be removed and we must move the reset of the array down.
	for(int i = 0; i < m_Count; ++i)
	{
		if (m_Values[i] == &value && m_Sources[i] == source.m_Device && (m_Parameters[i] & 0xFFFFFF) == source.m_Parameter)
		{
			int newIndex = i;
			for(int oldIndex = newIndex +1; oldIndex < m_Count; ++oldIndex)
			{
				m_Parameters[newIndex]	= m_Parameters[oldIndex];
				m_Sources[newIndex]		= m_Sources[oldIndex];
				m_Values[newIndex]		= m_Values[oldIndex];

				newIndex = oldIndex;
			}
			m_Parameters[m_Count]	= ioSource::UNDEFINED_PARAMETER;
			m_Sources[m_Count]		= IOMS_UNDEFINED;
			m_Values[m_Count]		= NULL;
			--m_Count;

			return true;
		}
	}

	return false;
}

void ioMapper::RemoveDeviceMappings(const ioValue& value, s32 deviceId)
{
	if(!Verifyf(ioSource::IsValidDevice(deviceId), "Invalid device id (%d) used to remove mappings!",deviceId))
	{
		return;
	}

	int newPos = 0;

	// NOTE: The order of the arrays IS important so we cannot do a quick remove by swapping the last entry with
	// the one to be removed and we must move the reset of the array down.
	bool remove;
	for (int i = 0; i < m_Count; ++i)
	{
		remove = false;

		if(m_Values[i] == &value)
		{
			if(deviceId == ioSource::IOMD_ANY)
			{
				remove = true;
			}
			else if(deviceId != ioSource::IOMD_KEYBOARD_MOUSE)
			{	
				s32 mappingDeviceId = int(m_Parameters[i]) >> 24;

				if( deviceId == mappingDeviceId || (deviceId == ioSource::IOMD_DEFAULT && mappingDeviceId == GetPlayer()) )
				{
					remove = true;
				}
			}
			else
			{
				s32 mapperParam = ConvertParameterToMapperValue(m_Sources[i], m_Parameters[i]);

				if(    m_Sources[i] == IOMS_KEYBOARD || m_Sources[i] == IOMS_MKB_AXIS
					|| (mapperParam & IOMT_MOUSE_AXIS) || (mapperParam & IOMT_MOUSE_WHEEL) || (mapperParam & IOMT_MOUSE_BUTTON) )
				{
					remove = true;
				}
			}
		}

		// if we are not removing this element then copy it to its new position.
		if(!remove)
		{
			if(i != newPos)
			{
				m_Parameters[newPos] = m_Parameters[i];
				m_Sources[newPos]    = m_Sources[i];
				m_Values[newPos]     = m_Values[i];
			}
			++newPos;
		}
	}

	m_Count = newPos;
}

static int s_abs(int x) { return x>=0? x : -x; }

static float s_abs(float x) { return x>=0? x : -x; }

void ioMapper::BeginScan() {
}

#if !IS_CONSOLE
enum {
	POV_UP = 0,
	POV_RIGHT,
	POV_DOWN,
	POV_LEFT
};

static inline int MapHatToButton(int value) {
		if (value == 0)
			return POV_UP;
		else if (value == 9000)
			return POV_RIGHT;
		else if (value == 18000)
			return POV_DOWN;
		else if (value == 27000)
			return POV_LEFT;
	
	return IOM_POV_UNDEFINED;
}
#endif

bool ioMapper::Scan(ioSource &source, const ScanOptions &options) {

	if(ScanKeyboardMouse(source, options))
		return true;

	if(options.m_PadOptions != ScanOptions::NO_PAD)
	{
		if(options.m_DeviceID == ioSource::IOMD_DEFAULT)
		{
			if(ScanPad(source, options, m_PadIndex))
				return true;
		}
		else if(options.m_DeviceID == ioSource::IOMD_ANY)
		{
			// go through all devices.
			for(s32 i = 1; i < ioPad::MAX_PADS; ++i)
			{
				if(ScanPad(source, options, i))
					return true;
			}
		}
		else
		{
			if(ScanPad(source, options, options.m_DeviceID))
				return true;
		}
	}
	
#if RSG_PC
	if(options.m_JoystickOptions != ScanOptions::NO_JOYSTICK)
	{
		if(options.m_DeviceID == ioSource::IOMD_DEFAULT)
		{
			if(ScanJoystick(source, options, m_PadIndex))
				return true;
		}
		else if(options.m_DeviceID == ioSource::IOMD_ANY)
		{
			// go through all possible devices.
			for(s32 i = 0; i < ioJoystick::GetStickCount() && i < MAX_SUPPORTED_DEVICES; ++i)
			{
				if(ScanJoystick(source, options, i))
					return true;
			}
		}
		else
		{
			if(ScanJoystick(source, options, options.m_DeviceID))
				return true;
		}
	}
#endif // RSG_PC

	return false;
}


bool ioMapper::ScanKeyboardMouse(ioSource &source, const ScanOptions &options)
{
	int i;

	// scan for keyboard input.
	if((options.m_KeyboardOptions & ScanOptions::KEYBOARD_BUTTONS) == ScanOptions::KEYBOARD_BUTTONS)
	{
		// Fix for url:bugstar:2217025 by starting at 1 not 0. For some unknown reason, we recieve a key down with keycode 0.
		for (i=1; i<256; i++) {
			if (i != KEY_LWIN && i != KEY_RWIN && ioKeyboard::KeyReleased(i)) {
				source.m_Device			= IOMS_KEYBOARD;
				source.m_Parameter		= i;
				source.m_DeviceIndex	= ioSource::IOMD_KEYBOARD_MOUSE;
				return true;
			}
		}
	}

	// Scan for mouse movement.
	if((options.m_MouseOptions & ScanOptions::MOUSE_MOVEMENT) == ScanOptions::MOUSE_MOVEMENT)
	{
		if (s_abs(ioMouse::GetDX()) > 20) {
			source.m_Device			= IOMS_MOUSE_RELATIVEAXIS;
			source.m_Parameter		= IOM_AXIS_X;
			source.m_DeviceIndex	= ioSource::IOMD_KEYBOARD_MOUSE;
			return true;
		}
		if (s_abs(ioMouse::GetDY()) > 20) {
			source.m_Device			= IOMS_MOUSE_RELATIVEAXIS;
			source.m_Parameter		= IOM_AXIS_Y;
			source.m_DeviceIndex	= ioSource::IOMD_KEYBOARD_MOUSE;
			return true;
		}
	}

	// scan for mouse buttons.
	if((options.m_MouseOptions & ScanOptions::MOUSE_BUTTONS) == ScanOptions::MOUSE_BUTTONS)
	{
		for (i=0; i<ioMouse::MOUSE_NUM_BUTTONS; i++) {
			if (ioMouse::GetReleasedButtons() & (1<<i)) {
				source.m_Device			= IOMS_MOUSE_BUTTON;
				source.m_Parameter		= (1<<i);
				source.m_DeviceIndex	= ioSource::IOMD_KEYBOARD_MOUSE;
				return true;
			}
		}
	}

	// scan for mouse wheel.
	if((options.m_MouseOptions & ScanOptions::MOUSE_WHEEL) == ScanOptions::MOUSE_WHEEL)
	{
		if (ioMouse::HasWheel())
		{
			if (ioMouse::GetDZ()>0)
			{
				source.m_Device			= IOMS_MOUSE_WHEEL;
				source.m_Parameter		= IOM_WHEEL_UP;
				source.m_DeviceIndex	= ioSource::IOMD_KEYBOARD_MOUSE;
				return true;
			}

			if (ioMouse::GetDZ()<0)
			{
				source.m_Device			= IOMS_MOUSE_WHEEL;
				source.m_Parameter		= IOM_WHEEL_DOWN;
				source.m_DeviceIndex	= ioSource::IOMD_KEYBOARD_MOUSE;
				return true;
			}
		}
	}

	return false;
}

bool ioMapper::ScanPad(ioSource &source, const ScanOptions &options, s32 padIndex)
{
	// scan for pad buttons
	bool wantButtons  = (options.m_PadOptions & ScanOptions::PAD_BUTTONS) == ScanOptions::PAD_BUTTONS;
	bool wantDpadAxis = (options.m_PadOptions & ScanOptions::PAD_DPAD_AXIS) == ScanOptions::PAD_DPAD_AXIS;
	if(wantButtons || wantDpadAxis)
	{
		for (int i=0; i<16; i++) {
			if (ioPad::GetPad(padIndex).GetButtons() & (1<<i)) {
				if (wantDpadAxis) {
					if (i >= 12) {	// D-pad
						source.m_Device = IOMS_PAD_AXIS;
						if (i == 12 || i == 14)		// Up/Down
							source.m_Parameter = IOM_AXIS_DPADY;
						else
							source.m_Parameter = IOM_AXIS_DPADX;
					}
					else
						continue;
				}
				else if(wantButtons) {
					source.m_Device = IOMS_PAD_DIGITALBUTTON;
					source.m_Parameter = (1 << i);
				}

				source.m_DeviceIndex = padIndex;
				return true;
			}
		}
	}

	// scan pad axis.
	bool treatAxisAsDirection = (options.m_PadOptions & ScanOptions::PAD_AXIS_DIRECTION) == ScanOptions::PAD_AXIS_DIRECTION;
	if((options.m_PadOptions & ScanOptions::PAD_AXIS) == ScanOptions::PAD_AXIS || treatAxisAsDirection)
	{
		if (s_abs(ioPad::GetPad(padIndex).GetNormLeftX()) > ioValue::DEFAULT_DEAD_ZONE_VALUE) {
			source.m_Device = IOMS_PAD_AXIS;
			if (treatAxisAsDirection)
				source.m_Parameter = IOM_AXIS_LX;
			else if (ioPad::GetPad(padIndex).GetNormLeftX() > 0)
				source.m_Parameter = IOM_AXIS_LRIGHT;
			else
				source.m_Parameter = IOM_AXIS_LLEFT;

			source.m_DeviceIndex = padIndex;
			return true;
		}
		if (s_abs(ioPad::GetPad(padIndex).GetNormLeftY()) > ioValue::DEFAULT_DEAD_ZONE_VALUE) {
			source.m_Device = IOMS_PAD_AXIS;
			if (treatAxisAsDirection)
				source.m_Parameter = IOM_AXIS_LY;
			else if (ioPad::GetPad(padIndex).GetNormLeftY() > 0)
				source.m_Parameter = IOM_AXIS_LDOWN;
			else
				source.m_Parameter = IOM_AXIS_LUP;

			source.m_DeviceIndex = padIndex;
			return true;
		}
		if (s_abs(ioPad::GetPad(padIndex).GetNormRightX()) > ioValue::DEFAULT_DEAD_ZONE_VALUE) {
			source.m_Device = IOMS_PAD_AXIS;
			if (treatAxisAsDirection)
				source.m_Parameter = IOM_AXIS_RX;
			else if (ioPad::GetPad(padIndex).GetNormRightX() > 0)
				source.m_Parameter = IOM_AXIS_RRIGHT;
			else
				source.m_Parameter = IOM_AXIS_RLEFT;

			source.m_DeviceIndex = padIndex;
			return true;
		}
		if (s_abs(ioPad::GetPad(padIndex).GetNormRightY()) > ioValue::DEFAULT_DEAD_ZONE_VALUE) {
			source.m_Device = IOMS_PAD_AXIS;
			if (treatAxisAsDirection)
				source.m_Parameter = IOM_AXIS_RY;
			else if (ioPad::GetPad(padIndex).GetNormRightY() > 0)
				source.m_Parameter = IOM_AXIS_RDOWN;
			else
				source.m_Parameter = IOM_AXIS_RUP;

			source.m_DeviceIndex = padIndex;
			return true;
		}
	}

	return false;
}


#if RSG_PC

bool ioMapper::ScanJoystick(ioSource &source, const ScanOptions &options, s32 joystickIndex)
{
	const ioJoystick &J = ioJoystick::GetStick(joystickIndex);
	int i;

	Assertf(joystickIndex < MAX_SUPPORTED_DEVICES, "Invalid joystick id!");
	if(joystickIndex >= MAX_SUPPORTED_DEVICES)
		return false;

	if((options.m_JoystickOptions & ScanOptions::JOYSTICK_AXIS) == ScanOptions::JOYSTICK_AXIS)
	{
		for (i=0; i<8; i++) {
			static bank_float TRIGGER_THRESHOLD = 0.01f;
			
			float val = J.GetNormAxis(i);

			float threshold = ioValue::DEFAULT_DEAD_ZONE_VALUE;
			if(J.IsAxisTwoDirectional(i) == false)
			{
				threshold = TRIGGER_THRESHOLD;
			}

			if (s_abs(val) > threshold) {

				if((options.m_JoystickOptions & ScanOptions::JOYSTICK_AXIS_DIRECTION) != ScanOptions::JOYSTICK_AXIS_DIRECTION)
					source.m_Device = IOMS_JOYSTICK_AXIS;
				else if(val > 0)
					source.m_Device = IOMS_JOYSTICK_AXIS_POSITIVE;
				else
					source.m_Device = IOMS_JOYSTICK_AXIS_NEGATIVE;

				source.m_Parameter = i;
				source.m_DeviceIndex = joystickIndex;
				return true;
			}
		}
	}

	if((options.m_JoystickOptions & ScanOptions::JOYSTICK_BUTTONS) == ScanOptions::JOYSTICK_BUTTONS)
	{
		for (i=0; i<32; i++) {
			if ((J.GetLastAnalogButton(i) & 0x80) && J.GetAnalogButton(i) == 0) {
				source.m_Device = IOMS_JOYSTICK_BUTTON;
				source.m_Parameter = i;
				source.m_DeviceIndex = joystickIndex;
				return true;
			}
		}
	}

	if((options.m_JoystickOptions & ScanOptions::JOYSTICK_POV) == ScanOptions::JOYSTICK_POV)
	{
		for (i=0; i<4; i++) {
			int value = J.GetPOV(i);
			if (value != -1 && MapHatToButton(value) != IOM_POV_UNDEFINED) {
				source.m_Device = IOMS_JOYSTICK_POV;
				source.m_Parameter = MapHatToButton(value) + i*4;
				source.m_DeviceIndex = joystickIndex;
				return true;
			}
		}
	}

	return false;
}

#endif // RSG_PC


void ioMapper::ClearValues() const {
	for (int i=0; i<m_Count; i++) {
		ioValue::ValueType newValue = ioValue::CENTER_AXIS;

		unsigned p = m_Parameters[i] & 0xFFFFFF;

		switch (m_Sources[i]) {
			case IOMS_KEYBOARD:
				newValue = ioValue::CENTER_AXIS;
				break;
			case IOMS_MKB_AXIS:
			case IOMS_DIGITALBUTTON_AXIS:
			case IOMS_MOUSE_ABSOLUTEAXIS:
			case IOMS_MOUSE_RELATIVEAXIS:
			case IOMS_MOUSE_SCALEDAXIS:
				newValue = ioValue::CENTER_AXIS;
				break;
			case IOMS_MOUSE_BUTTON:
			case IOMS_MOUSE_BUTTONANY:
				newValue = ioValue::CENTER_AXIS;
				break;
			case IOMS_MOUSE_WHEEL:
				if (ioMouse::HasWheel())
				{
					newValue = ioValue::CENTER_AXIS;  //    IOM_WHEEL_UP and IOM_WHEEL_DOWN
				}
				break;
			case IOMS_PAD_DIGITALBUTTON:
			case IOMS_PAD_DIGITALBUTTONANY:
			case IOMS_PAD_ANALOGBUTTON:
			case IOMS_PAD_DEBUGBUTTON:
				newValue = ioValue::CENTER_AXIS;
				break;
			case IOMS_PAD_AXIS:
				switch (p)
				{
				case IOM_AXIS_LX:
				case IOM_AXIS_LY:
				case IOM_AXIS_RX:
				case IOM_AXIS_RY:
					newValue = ioValue::CENTER_AXIS;
					break;
				case IOM_AXIS_LUP:
				case IOM_AXIS_LDOWN:
				case IOM_AXIS_LLEFT:
				case IOM_AXIS_LRIGHT:
				case IOM_AXIS_LUR:
				case IOM_AXIS_LUL:
				case IOM_AXIS_LDR:
				case IOM_AXIS_LDL:
				case IOM_AXIS_RUP:
				case IOM_AXIS_RDOWN:
				case IOM_AXIS_RLEFT:
				case IOM_AXIS_RRIGHT:
				case IOM_AXIS_RUR:
				case IOM_AXIS_RUL:
				case IOM_AXIS_RDR:
				case IOM_AXIS_RDL:
				case IOM_AXIS_LY_UP:
				case IOM_AXIS_LY_DOWN:
				case IOM_AXIS_LX_LEFT:
				case IOM_AXIS_LX_RIGHT:
				case IOM_AXIS_RY_UP:
				case IOM_AXIS_RY_DOWN:
				case IOM_AXIS_RX_LEFT:
				case IOM_AXIS_RX_RIGHT:
					newValue = ioValue::CENTER_AXIS;
					break;
				case IOM_AXIS_DPADX:
				case IOM_AXIS_DPADY:
					newValue = ioValue::CENTER_AXIS;
					break;
				}
				break;

#if !IS_CONSOLE
			case IOMS_JOYSTICK_BUTTON:
				newValue = ioValue::CENTER_AXIS;
				break;
			case IOMS_JOYSTICK_AXIS:
				newValue = ioValue::CENTER_AXIS;
				break;
			case IOMS_JOYSTICK_POV: 
				newValue  = ioValue::CENTER_AXIS;
				break;
#endif
			case IOMS_FORCE32:      //    for completeness
				break;
			default:
				break;
		}
		m_Values[i]->SetCurrentValue(newValue, ioSource::UNKNOWN_SOURCE);
	}
}

//    end of ioMapper::Clear





static u8 s_UpdateID;

void ioMapper::Update(u32 timeMS, bool forceKeyboardMouse, float UNUSED_PARAM(timeStep)) {
	// Update last value from current and reset current to zero
	// The s_UpdateID parameter lets us quickly prevent the same action from
	// getting reset twice if more than one input is bound to it.
	++s_UpdateID;

	int i;
	for (i=0; i<m_Count; i++) {
		m_Values[i]->Update(s_UpdateID, timeMS);
	}

	ioValue::ReadOptions options = ioValue::NO_DEAD_ZONE;
	options.SetFlags(ioValue::ReadOptions::F_READ_DISABLED, true);

#if RSG_EXTRA_MOUSE_SUPPORT
	// only update from the second frame.
	if(m_lastMS == 0)
		m_lastMS = timeMS;
	else
	{
		u32 deltaTime = timeMS - m_lastMS;
		m_lastMS = timeMS;

		s32 mouseWheelDelta	 = ioMouse::GetDZ();

#if !ENABLE_INPUT_COMBOS
		m_MouseCenteredX += static_cast<ioValue::ValueType>(static_cast<float>(ioMouse::GetDX()) * m_MouseScale) * ioValue::LEGACY_AXIS_SCALE;
		m_MouseCenteredY += static_cast<ioValue::ValueType>(static_cast<float>(ioMouse::GetDY()) * m_MouseScale) * ioValue::LEGACY_AXIS_SCALE;
		m_MouseCenteredX = Clamp(m_MouseCenteredX, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
		m_MouseCenteredY = Clamp(m_MouseCenteredY, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
#endif // !ENABLE_INPUT_COMBOS

		// first check if the direction of the axis has changed, if so reset to zero.
		if((m_MouseWheelNotches < 0 && mouseWheelDelta > 0) || (m_MouseWheelNotches > 0 && mouseWheelDelta < 0))
		{
			m_MouseWheelNotches  = 0;
			m_MouseWheelTimer	 = 0;
		}
		else if(s_abs(m_MouseWheelNotches) > 0)
		{
			// update the mouse wheel timer if needed.
			m_MouseWheelTimer += deltaTime;

			m_MouseWheelNotches += mouseWheelDelta;
			s32 sign = (m_MouseWheelNotches > 0) ? 1 : -1;
			// Use while rather than if, in case the frame was longer than two m_MouseWheelDuration periods.
			while(m_MouseWheelTimer >= m_MouseWheelDuration && m_MouseWheelNotches != 0)
			{
				m_MouseWheelTimer	-= m_MouseWheelDuration;
				m_MouseWheelNotches	-= sign;
			}

			if(m_MouseWheelNotches == 0)
				m_MouseWheelTimer	 = 0;
		}
		else if(m_MouseWheelNotches == 0)
			m_MouseWheelNotches += mouseWheelDelta;
	}
#endif // RSG_EXTRA_MOUSE_SUPPORT

	// Now evaluate each source and wire its output to the bound value.
	// Output value is only replaced if the new value is larger than existing one.
	for (i=0; i<m_Count; i++) {

		s32 padIndex = int(m_Parameters[i]) >> 24;
		Assertf(ioSource::IsValidDevice(padIndex), "Invalid device id, using default!");

		if(padIndex == ioSource::IOMD_DEFAULT)
		{
			padIndex = m_PadIndex;
		}
		else if(!ioSource::IsValidDevice(padIndex))
		{
			padIndex = ioSource::IOMD_UNKNOWN;
		}
#if ENABLE_INPUT_COMBOS
		ioSource source(m_Sources[i], (m_Parameters[i] & 0x00ffffff), padIndex);
		float newValue = GetSourceValue(source, forceKeyboardMouse);

#if RSG_EXTRA_MOUSE_SUPPORT
		if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
		{
			if(source.m_Device == IOMS_MOUSE_SCALEDAXIS)
			{
				newValue *= m_MouseScale;
			}
			else if(source.m_Device == IOMS_MOUSE_WHEEL)
			{
				if(source.m_Parameter==IOM_AXIS_WHEEL_RELATIVE && m_MouseWheelNotches != 0) newValue = (m_MouseWheelNotches > 0) ? 1.0f : -1.0f;
				else if(source.m_Parameter==IOM_IAXIS_WHEEL_RELATIVE && m_MouseWheelNotches != 0) newValue = (m_MouseWheelNotches > 0) ? -1.0f : 1.0f;
			}
		}
#endif // RSG_EXTRA_MOUSE_SUPPORT

		const float currentAbsValue = s_abs(m_Values[i]->GetUnboundNorm(options));
#else
		ioValue::ValueType newValue = 0;
		unsigned p = m_Parameters[i] & 0xFFFFFF;

		switch (m_Sources[i]) {
		case IOMS_KEYBOARD:
			newValue = p != KEY_NULL && ioKeyboard::KeyDown(p) && (ioMapper::GetEnableKeyboard() || forceKeyboardMouse) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
#if RSG_PC
			if (newValue && p==KEY_TAB && (ioKeyboard::KeyPressed(KEY_LMENU) || ioKeyboard::KeyPressed(KEY_RMENU)))
				newValue = ioValue::CENTER_AXIS;
#endif // RSG_PC
			break;
		case IOMS_MKB_AXIS:
			if (ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
			{
				bool negativeDown = false;

#if RSG_EXTRA_MOUSE_SUPPORT
				if (IsMkbAxisNegativeAMouseButton(p))
				{
					unsigned button = GetMkbAxisNegative(p);
					negativeDown = (ioMouse::GetButtons() & button) == button;
				}
				else if(IsMkbAxisNegativeAMouseWheel(p))
				{
					unsigned wheel = GetMkbAxisNegative(p);
					if(wheel == IOM_WHEEL_UP)
						negativeDown = ioMouse::GetDZ() > 0;
					else if(wheel == IOM_WHEEL_DOWN)
						negativeDown = ioMouse::GetDZ() < 0;
				}
				else
#endif // RSG_EXTRA_MOUSE_SUPPORT
				{
					u32 keyCode = GetMkbAxisNegative(p);
					negativeDown = keyCode != KEY_NULL && ioKeyboard::KeyDown(keyCode) != 0;
				}

				bool positiveDown = false;
#if RSG_EXTRA_MOUSE_SUPPORT
				if(IsMkbAxisPositiveAMouseButton(p))
				{
					unsigned button = GetMkbAxisPositive(p);
					positiveDown = (ioMouse::GetButtons() & button) == button;
				}
				else if(IsMkbAxisPositiveAMouseWheel(p))
				{
					unsigned wheel = GetMkbAxisPositive(p);
					if(wheel == IOM_WHEEL_UP)
						positiveDown = ioMouse::GetDZ() > 0;
					else if(wheel == IOM_WHEEL_DOWN)
						positiveDown = ioMouse::GetDZ() < 0;
				}
				else
#endif // RSG_EXTRA_MOUSE_SUPPORT
				{
					u32 keyCode = GetMkbAxisPositive(p);
					positiveDown = keyCode != KEY_NULL && ioKeyboard::KeyDown(keyCode) != 0;
				}

				if(negativeDown && !positiveDown)
				{
					newValue = ioValue::MIN_AXIS;
				}
				else if(!negativeDown && positiveDown)
				{
					newValue = ioValue::MAX_AXIS;
				}
				else
				{
					newValue = ioValue::CENTER_AXIS;
				}
			}
			break;
		case IOMS_DIGITALBUTTON_AXIS:
			{
				int buttons = ioPad::GetPad(padIndex).GetButtons();
				u32 zeroButton = 1 << (p & 255);
				u32 oneButton = 1 << ((p >> 8) & 255);
				int state = ((buttons & zeroButton)? 1 : 0) | ((buttons & oneButton)? 2 : 0);
				if (state == 1)
					newValue = ioValue::MIN_AXIS;
				else if (state == 2)
					newValue = ioValue::MAX_AXIS;
				else
					newValue = ioValue::CENTER_AXIS;
				break;
			}
		case IOMS_MOUSE_ABSOLUTEAXIS:
			if (p==IOM_AXIS_X) newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormX() * ioValue::MAX_AXIS_F);
			else if (p==IOM_AXIS_Y) newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormY() * ioValue::MAX_AXIS_F);
			break;
#if RSG_EXTRA_MOUSE_SUPPORT
		case IOMS_MOUSE_CENTEREDAXIS:
			if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
			{
				if (p==IOM_AXIS_X) newValue = m_MouseCenteredX;
				else if (p==IOM_AXIS_X_RIGHT) newValue = Max(m_MouseCenteredX, ioValue::CENTER_AXIS);
				else if (p==IOM_AXIS_X_LEFT) newValue = Min(m_MouseCenteredX, ioValue::CENTER_AXIS);
				else if (p==IOM_IAXIS_X) newValue = -m_MouseCenteredX;
				else if (p==IOM_AXIS_Y) newValue = m_MouseCenteredY;
				else if (p==IOM_IAXIS_Y) newValue = -m_MouseCenteredY;
				else if (p==IOM_AXIS_Y_DOWN) newValue = Max(m_MouseCenteredY, ioValue::CENTER_AXIS);
				else if (p==IOM_AXIS_Y_UP) newValue = Min(m_MouseCenteredY, ioValue::CENTER_AXIS);
			}
			break;
		case IOMS_MOUSE_SCALEDAXIS:
#endif // RSG_EXTRA_MOUSE_SUPPORT
		case IOMS_MOUSE_RELATIVEAXIS:
			if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
			{
				if (p==IOM_AXIS_X || p==IOM_IAXIS_X || p==IOM_AXIS_X_LEFT || p==IOM_AXIS_X_RIGHT)
				{
					s32 invert;
					if(p == IOM_IAXIS_X)
					{
						invert = -1;
					}
					else
					{
						invert = 1;
					}
					newValue = static_cast<ioValue::ValueType>(ioMouse::GetDX() * invert) * ioValue::LEGACY_AXIS_SCALE;

					if((p==IOM_AXIS_X_LEFT && newValue > 0) || (p==IOM_AXIS_X_RIGHT && newValue < 0))
						newValue = 0;
				}
				else if (p==IOM_AXIS_Y || p==IOM_IAXIS_Y || p==IOM_AXIS_Y_UP || p==IOM_AXIS_Y_DOWN)
				{
					s32 invert;
					if(p == IOM_IAXIS_Y)
					{
						invert = -1;
					}
					else
					{
						invert = 1;
					}
					newValue = static_cast<ioValue::ValueType>(ioMouse::GetDY() * invert) * ioValue::LEGACY_AXIS_SCALE;

					if((p==IOM_AXIS_Y_UP && newValue > 0) || (p==IOM_AXIS_Y_DOWN && newValue < 0))
						newValue = 0;
				}

#if RSG_EXTRA_MOUSE_SUPPORT
				if(m_Sources[i] == IOMS_MOUSE_SCALEDAXIS)
				{
					const float timeScale = 0.25f; //was 30.0f * timeStep;
					newValue *= (m_MouseScale * timeScale);
				}
#endif // RSG_EXTRA_MOUSE_SUPPORT
			}
			break;
#if RSG_EXTRA_MOUSE_SUPPORT
		case IOMS_MOUSE_NORMALIZED:
			if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
			{
				if (p==IOM_AXIS_X || p==IOM_AXIS_X_LEFT || p==IOM_AXIS_X_RIGHT)
				{
					newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormalizedX() * ioValue::MAX_AXIS_F);

					if((p==IOM_AXIS_X_LEFT && newValue > 0) || (p==IOM_AXIS_X_RIGHT && newValue < 0))
						newValue = 0;
				}
				else if (p==IOM_AXIS_Y || p==IOM_AXIS_Y_UP || p==IOM_AXIS_Y_DOWN)
				{
					newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormalizedY() * ioValue::MAX_AXIS_F);

					if((p==IOM_AXIS_Y_UP && newValue > 0) || (p==IOM_AXIS_Y_DOWN && newValue < 0))
						newValue = 0;
				}
				else if(p == IOM_IAXIS_X)
				{
					newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormalizedX() * -ioValue::MAX_AXIS_F);
				}
				else if(p == IOM_IAXIS_Y)
				{
					newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormalizedY() * -ioValue::MAX_AXIS_F);
				}
			}
			break;
#endif // RSG_EXTRA_MOUSE_SUPPORT

		case IOMS_MOUSE_BUTTON:
			if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
			{
				newValue = (ioMouse::GetButtons() & p) == p ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			}
			break;
		case IOMS_MOUSE_WHEEL:
			if ((ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse) && ioMouse::HasWheel())
			{
				if (p==IOM_WHEEL_UP) newValue = (ioMouse::GetDZ() > 0) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
				else if (p==IOM_WHEEL_DOWN) newValue = (ioMouse::GetDZ() < 0) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
				else if (p==IOM_AXIS_WHEEL_DELTA) newValue = static_cast<ioValue::ValueType>(ioMouse::GetDZ());
				else if (p==IOM_IAXIS_WHEEL_DELTA) newValue = static_cast<ioValue::ValueType>(-ioMouse::GetDZ());
				else if (p==IOM_AXIS_WHEEL || p==IOM_IAXIS_WHEEL)
				{
					if(ioMouse::GetDZ() > 0) newValue = (p==IOM_AXIS_WHEEL) ? ioValue::MAX_AXIS : ioValue::MIN_AXIS;
					else if(ioMouse::GetDZ() < 0) newValue = (p==IOM_AXIS_WHEEL) ? ioValue::MIN_AXIS : ioValue::MAX_AXIS;
				}
#if RSG_EXTRA_MOUSE_SUPPORT
				else if(p==IOM_AXIS_WHEEL_RELATIVE && m_MouseWheelNotches != 0) newValue = (m_MouseWheelNotches > 0) ? ioValue::MAX_AXIS : ioValue::MIN_AXIS;
				else if(p==IOM_IAXIS_WHEEL_RELATIVE && m_MouseWheelNotches != 0) newValue = (m_MouseWheelNotches > 0) ? ioValue::MIN_AXIS : ioValue::MAX_AXIS;
#endif // RSG_EXTRA_MOUSE_SUPPORT
			}
			break;
		case IOMS_MOUSE_BUTTONANY:
			if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
			{
				newValue = (ioMouse::GetButtons() & p) != 0 ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			}
			break;
		case IOMS_PAD_DIGITALBUTTON:
#if IS_CONSOLE && __BANK
			// if local bank widgets are up, mask out the D-pad since it's usually connected
			// to something annoying like a polar camera or a car horn
			if (BANKMGR.IsUsingPad(m_PadIndex))
				newValue = (ioPad::GetPad(padIndex).GetButtons() & p & ~(ioPad::LLEFT | ioPad::LRIGHT | ioPad::LUP | ioPad::LDOWN)) == p ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else
#endif
			newValue = (ioPad::GetPad(padIndex).GetButtons() & p) == p? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		
			break;
		case IOMS_PAD_DEBUGBUTTON:
#if __DEBUGBUTTONS
# if IS_CONSOLE && __BANK
			// if local bank widgets are up, mask out the D-pad since it's usually connected
			// to something annoying like a polar camera or a car horn
			if (BANKMGR.IsUsingPad(m_PadIndex))
				newValue = (ioPad::GetPad(padIndex).GetDebugButtons() & p & ~(ioPad::LLEFT | ioPad::LRIGHT | ioPad::LUP | ioPad::LDOWN)) == p ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else
# endif
				newValue = (ioPad::GetPad(padIndex).GetDebugButtons() & p) == p ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
#else
			newValue = 0;
#endif	// __DEBUGBUTTONS
			break;
		case IOMS_PAD_DIGITALBUTTONANY:
#if IS_CONSOLE && __BANK
			// if local bank widgets are up, mask out the D-pad since it's usually connected
			// to something annoying like a polar camera or a car horn
			if (BANKMGR.IsUsingPad(padIndex))
				newValue = (ioPad::GetPad(padIndex).GetButtons() & p & ~(ioPad::LLEFT | ioPad::LRIGHT | ioPad::LUP | ioPad::LDOWN)) != 0 ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else
#endif
			newValue = (ioPad::GetPad(padIndex).GetButtons() & p) != 0 ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			break;		
		case IOMS_PAD_ANALOGBUTTON:
			newValue = ioPad::GetPad(padIndex).GetAnalogButton((ioPad::ePadButton) p);

			// The scale of a u8.
			EXTRA_MOUSE_SUPPORT_ONLY(newValue /= 255.0f);
			if(newValue==0)
			{
				newValue = (ioPad::GetPad(padIndex).GetButtons() & (1<<p)) != 0 ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			}
			break;
		case IOMS_PAD_AXIS:
			// NOTE: to support a circular dead zone for axis diagonals,
			//       use 0.57 for x and y (approximately 0.8 * sin(0.25 pi))

			// We subtract one to better center the axis as there is no center value in a u8.
			if (p==IOM_AXIS_LX) newValue =		Clamp((ioPad::GetPad(padIndex).GetLeftX()  - ioValue::LEGACY_AXIS_SHIFT) * ioValue::LEGACY_AXIS_SCALE, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
			else if (p==IOM_AXIS_LY) newValue = Clamp((ioPad::GetPad(padIndex).GetLeftY()  - ioValue::LEGACY_AXIS_SHIFT) * ioValue::LEGACY_AXIS_SCALE, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
			else if (p==IOM_AXIS_RX) newValue = Clamp((ioPad::GetPad(padIndex).GetRightX() - ioValue::LEGACY_AXIS_SHIFT) * ioValue::LEGACY_AXIS_SCALE, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
			else if (p==IOM_AXIS_RY) newValue = Clamp((ioPad::GetPad(padIndex).GetRightY() - ioValue::LEGACY_AXIS_SHIFT) * ioValue::LEGACY_AXIS_SCALE, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
			else if (p==IOM_AXIS_LUP)
				newValue = (ioPad::GetPad(padIndex).GetNormLeftY() < -0.80f && s_abs(ioPad::GetPad(padIndex).GetNormLeftX()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LDOWN)
				newValue = (ioPad::GetPad(padIndex).GetNormLeftY() > 0.80f && s_abs(ioPad::GetPad(padIndex).GetNormLeftX()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LLEFT)
				newValue = (ioPad::GetPad(padIndex).GetNormLeftX() < -0.80f && s_abs(ioPad::GetPad(padIndex).GetNormLeftY()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LRIGHT)
				newValue = (ioPad::GetPad(padIndex).GetNormLeftX() > 0.80f && s_abs(ioPad::GetPad(padIndex).GetNormLeftY()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LUR)
				newValue = (ioPad::GetPad(padIndex).GetNormLeftY() < -0.57f && ioPad::GetPad(padIndex).GetNormLeftX() > 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LUL)
				newValue = (ioPad::GetPad(padIndex).GetNormLeftY() < -0.57f && ioPad::GetPad(padIndex).GetNormLeftX() < -0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LDR)
				newValue = (ioPad::GetPad(padIndex).GetNormLeftY() > 0.57f && ioPad::GetPad(padIndex).GetNormLeftX() > 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LDL)
				newValue = (ioPad::GetPad(padIndex).GetNormLeftY() > 0.57f && ioPad::GetPad(padIndex).GetNormLeftX() < -0.57f)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RUP)
				newValue = (ioPad::GetPad(padIndex).GetNormRightY() < -0.80f && s_abs(ioPad::GetPad(padIndex).GetNormRightX()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RDOWN)
				newValue = (ioPad::GetPad(padIndex).GetNormRightY() >  0.80f && s_abs(ioPad::GetPad(padIndex).GetNormRightX()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RLEFT)
				newValue = (ioPad::GetPad(padIndex).GetNormRightX() < -0.80f && s_abs(ioPad::GetPad(padIndex).GetNormRightY()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RRIGHT)
				newValue = (ioPad::GetPad(padIndex).GetNormRightX() > 0.80f && s_abs(ioPad::GetPad(padIndex).GetNormRightY()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RUR)
				newValue = (ioPad::GetPad(padIndex).GetNormRightY() < -0.57f && ioPad::GetPad(padIndex).GetNormRightX() > 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RUL)
				newValue = (ioPad::GetPad(padIndex).GetNormRightY() < -0.57f && ioPad::GetPad(padIndex).GetNormRightX() < -0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RDR)
				newValue = (ioPad::GetPad(padIndex).GetNormRightY() > 0.57f && ioPad::GetPad(padIndex).GetNormRightX() > 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RDL)
				newValue = (ioPad::GetPad(padIndex).GetNormRightY() > 0.57f && ioPad::GetPad(padIndex).GetNormRightX() < -0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_DPADX) {
				// Both branches of this code are designed to behave sanely even if left/right
				// somehow happen to both register as active (ie on an old worn-out controller)
				{
					int buttons = ioPad::GetPad(padIndex).GetButtons() & (ioPad::LLEFT | ioPad::LRIGHT);
					newValue = buttons == ioPad::LLEFT ? ioValue::MIN_AXIS : buttons == ioPad::LRIGHT? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
				}
			}
			else if (p==IOM_AXIS_DPADY) {
				// Both branches of this code are designed to behave sanely even if up/down
				// somehow happen to both register as active (ie on an old worn-out controller)
				{
					int buttons = ioPad::GetPad(padIndex).GetButtons() & (ioPad::LUP | ioPad::LDOWN);
					newValue = buttons == ioPad::LUP ? ioValue::MIN_AXIS : buttons == ioPad::LDOWN? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
				}
			}
			else if (p==IOM_AXIS_LY_UP)
				newValue = (newValue=ioPad::GetPad(padIndex).GetLeftY())<127 ? ioValue::MAX_AXIS -(newValue*ioValue::LEGACY_AXIS_SCALE) : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LY_DOWN)
				newValue = (newValue=ioPad::GetPad(padIndex).GetLeftY())>127 ? (newValue*ioValue::LEGACY_AXIS_SCALE)-ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LX_LEFT)
				newValue = (newValue=ioPad::GetPad(padIndex).GetLeftX())<127 ? ioValue::MAX_AXIS -(newValue*ioValue::LEGACY_AXIS_SCALE) : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_LX_RIGHT)
				newValue = (newValue=ioPad::GetPad(padIndex).GetLeftX())>127 ? (newValue*ioValue::LEGACY_AXIS_SCALE)-ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RY_UP)
				newValue = (newValue=ioPad::GetPad(padIndex).GetRightY())<127 ? ioValue::MAX_AXIS -(newValue*ioValue::LEGACY_AXIS_SCALE) : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RY_DOWN)
				newValue = (newValue=ioPad::GetPad(padIndex).GetRightY())>127 ? (newValue*ioValue::LEGACY_AXIS_SCALE)-ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RX_LEFT)
				newValue = (newValue=ioPad::GetPad(padIndex).GetRightX())<127 ? ioValue::MAX_AXIS -(newValue*ioValue::LEGACY_AXIS_SCALE) : ioValue::CENTER_AXIS;
			else if (p==IOM_AXIS_RX_RIGHT)
				newValue = (newValue=ioPad::GetPad(padIndex).GetRightX())>127 ? (newValue*ioValue::LEGACY_AXIS_SCALE)-ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			break;
#if !IS_CONSOLE
		case IOMS_JOYSTICK_BUTTON:
			//newValue = Clamp((ioJoystick::GetStick(padIndex).GetAnalogButton(p) - s_BaseAxes[padIndex][p]) * ioValue::LEGACY_AXIS_SCALE, ioValue::CENTER_AXIS, ioValue::MAX_AXIS);
			newValue = Clamp(ioJoystick::GetStick(padIndex).GetAnalogButton(p) * ioValue::LEGACY_AXIS_SCALE, ioValue::CENTER_AXIS, ioValue::MAX_AXIS);
			break;
		case IOMS_JOYSTICK_AXIS:
			newValue = Clamp(static_cast<ioValue::ValueType>(ioJoystick::GetStick(padIndex).GetNormAxis(p) * ioValue::MAX_AXIS), -ioValue::MAX_AXIS, ioValue::MAX_AXIS);
			break;
		case IOMS_JOYSTICK_IAXIS:
			newValue = -Clamp(static_cast<ioValue::ValueType>(ioJoystick::GetStick(padIndex).GetNormAxis(p) * ioValue::MAX_AXIS), -ioValue::MAX_AXIS, ioValue::MAX_AXIS);
			break;
		case IOMS_JOYSTICK_AXIS_NEGATIVE:
			newValue = Clamp(static_cast<ioValue::ValueType>(-ioJoystick::GetStick(padIndex).GetNormAxis(p) * ioValue::MAX_AXIS), ioValue::CENTER_AXIS, ioValue::MAX_AXIS);
			break;
		case IOMS_JOYSTICK_AXIS_POSITIVE:
			newValue = Clamp(static_cast<ioValue::ValueType>(ioJoystick::GetStick(padIndex).GetNormAxis(p) * ioValue::MAX_AXIS), ioValue::CENTER_AXIS, ioValue::MAX_AXIS);
			break;
		case IOMS_JOYSTICK_POV: 
			{
				int pov = ioJoystick::GetStick(padIndex).GetPOV(p >> 2);
				if (pov != IOM_POV_UNDEFINED) {
					switch (p & 3) {
					case POV_UP:	newValue = (pov > 27000 || pov < 9000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
					case POV_RIGHT:	newValue = (pov > 0 && pov < 18000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
					case POV_DOWN:	newValue = (pov > 9000 && pov < 27000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
					case POV_LEFT:	newValue = (pov > 18000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
					}
				}
				break;
			}
		case IOMS_JOYSTICK_POV_AXIS:
			{
				int neg = ioJoystick::GetStick(padIndex).GetPOV((p & 0xFF) >> 2);
				int pos = ioJoystick::GetStick(padIndex).GetPOV((p & 0xFF00) >> 10);
			

				if(neg != IOM_POV_UNDEFINED && pos != IOM_POV_UNDEFINED)
				{
					ioValue::ValueType negValue = 0;
					ioValue::ValueType posValue = 0;

					// negative
					switch (p & 3) {
						case POV_UP:	negValue = (neg > 27000 || neg < 9000)? ioValue::MIN_AXIS : ioValue::CENTER_AXIS; break;
						case POV_RIGHT:	negValue = (neg > 0 && neg < 18000)? ioValue::MIN_AXIS : ioValue::CENTER_AXIS; break;
						case POV_DOWN:	negValue = (neg > 9000 && neg < 27000)? ioValue::MIN_AXIS : ioValue::CENTER_AXIS; break;
						case POV_LEFT:	negValue = (neg > 18000)? ioValue::MIN_AXIS : ioValue::CENTER_AXIS; break;
					}
					// positive
					switch (((p & 0xFF00) >> 8) & 3) {
						case POV_UP:	posValue = (pos > 27000 || pos < 9000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
						case POV_RIGHT:	posValue = (pos > 0 && pos < 18000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
						case POV_DOWN:	posValue = (pos > 9000 && pos < 27000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
						case POV_LEFT:	posValue = (pos > 18000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
					}

					newValue = negValue + posValue;
				}
			}
			break;
#endif // !IS_CONSOLE

		case IOMS_TOUCHPAD_ABSOLUTE_AXIS:
			{
#if RSG_ORBIS
				if(p == IOM_AXIS_X)
				{
					newValue = static_cast<ioValue::ValueType>(ioPad::GetPad(padIndex).GetTouchXNorm() * ioValue::MAX_AXIS_F);
				}
				else if(p == IOM_AXIS_Y)
				{
					newValue = static_cast<ioValue::ValueType>(ioPad::GetPad(padIndex).GetTouchYNorm() * ioValue::MAX_AXIS_F);
				}
#endif // RSG_ORBIS
			}
			break;

		case IOMS_TOUCHPAD_CENTERED_AXIS:
			{
#if RSG_ORBIS
				if(p == IOM_AXIS_X)
				{
					newValue = static_cast<ioValue::ValueType>(ioPad::GetPad(padIndex).GetTouchXCenterNorm(0) * ioValue::MAX_AXIS_F);
				}
				else if(p == IOM_AXIS_Y)
				{
					newValue = static_cast<ioValue::ValueType>(ioPad::GetPad(padIndex).GetTouchYCenterNorm(0) * ioValue::MAX_AXIS_F);
				}
#endif // RSG_ORBIS
			}
			break;

		default:
			break;
		}
		
		const ioValue::ValueType currentAbsValue = s_abs(m_Values[i]->GetValue(options));
#endif // ENABLE_INPUT_COMBOS

		// If the value has changed update the stored value.
	#if RSG_EXTRA_MOUSE_SUPPORT || RSG_TOUCHPAD
		// ... unless the source is mouse axis/wheel, then always accept it since this
		// won't have noise but may have a value which is less than controller noise.
		bool c_bIsMouseSource = false;
		bool c_bWasMouseSource = false;

	#if RSG_EXTRA_MOUSE_SUPPORT
		c_bIsMouseSource  = (m_Sources[i] > IOMS_KEYBOARD && m_Sources[i] < IOMS_MOUSE_BUTTON);
		c_bWasMouseSource = m_Values[i]->GetSource().m_Device > IOMS_KEYBOARD &&
							m_Values[i]->GetSource().m_Device < IOMS_MOUSE_BUTTON;
	#endif // RSG_EXTRA_MOUSE_SUPPORT

	#if RSG_TOUCHPAD
		c_bIsMouseSource  |= (m_Sources[i] == IOMS_TOUCHPAD_ABSOLUTE_AXIS ||
							  m_Sources[i] == IOMS_TOUCHPAD_CENTERED_AXIS);
		c_bWasMouseSource |= (m_Values[i]->GetSource().m_Device == IOMS_TOUCHPAD_ABSOLUTE_AXIS ||
							  m_Values[i]->GetSource().m_Device == IOMS_TOUCHPAD_CENTERED_AXIS);
	#endif // RSG_TOUCHPAD

		if ((!c_bWasMouseSource && currentAbsValue < s_abs(newValue)) ||
			( c_bIsMouseSource  && newValue != 0) )
	#else
		if (currentAbsValue < s_abs(newValue))
	#endif // RSG_EXTRA_MOUSE_SUPPORT || RSG_TOUCHPAD
		{
#if ENABLE_INPUT_COMBOS
			// Cast back to an s32 until ioValue is updated to use a float.
			m_Values[i]->SetCurrentValue(static_cast<ioValue::ValueType>(newValue * ioValue::MAX_AXIS_F), source);
#else
			m_Values[i]->SetCurrentValue(newValue, ioSource(m_Sources[i], (m_Parameters[i] & 0x00ffffff), padIndex));
#endif // ENABLE_INPUT_COMBOS
		}
	}
}


static const char *suffix = "pcmap";


bool ioMapper::Load(const char *name,int version) {
	fiStream *S = ASSET.Open(name,suffix);
	if (S) {
		bool result = Load(S,version);
		S->Close();
		return result;
	}
	else
		return false;
}



bool ioMapper::Save(const char *name,int version) {
	fiStream *S = ASSET.Create(name,suffix);
	if (S) {
		bool result = Save(S,version);
		S->Close();
		return result;
	}
	else
		return false;
}



bool ioMapper::Load(fiStream *S,int matchVersion) {
	int magic, version, count;
	S->ReadInt(&magic,1);
	S->ReadInt(&version,1);
	S->ReadInt(&count,1);
	if (magic != MAKE_MAGIC_NUMBER('C','M','A','P') || version != matchVersion || count != m_Count) {
		Errorf("ioMapper::Load - bad version or different number of ioValues");
		return false;
	}

	for (int i=0; i<count; i++) {
		u32 source;
		S->ReadInt(&source,1);
		m_Sources[i] = (rage::ioMapperSource) source;
		S->ReadInt(&m_Parameters[i],1);
		u32 isInverted = 0;
		S->ReadInt(&isInverted,1);
		if(isInverted == 1) {
			m_Values[i]->MakeInverted();
		} else {
			m_Values[i]->MakeNormal();
		}
		m_Values[i]->InitValueAndHistory(0,0);
	}
	m_Count = count;
	return true;
}


bool ioMapper::Save(fiStream *S,int matchVersion) {
	int magic = MAKE_MAGIC_NUMBER('C','M','A','P');
	S->WriteInt(&magic,1);
	S->WriteInt(&matchVersion,1);
	S->WriteInt(&m_Count,1);

	for (int i=0; i<m_Count; i++) {
		u32 source = m_Sources[i];
		S->WriteInt(&source,1);
		S->WriteInt(&m_Parameters[i],1);
		u32 isInverted = (m_Values[i]->IsInverted())?1:0;
		S->WriteInt(&isInverted,1);
	}
	return true;
}

void rage::ioMapper::GetMappedSources( const ioValue& value, ioSourceList& sources ) const
{
	// return all sources mapped to a value.
	for(s32 i = 0; i < m_Count; ++i)
	{
		if(&value == m_Values[i])
		{
			if(Verifyf(sources.size() < sources.GetMaxCount(), "Sources associated with input has exceeded the limit! The list WILL be truncated to prevent a crash and WILL need increasing!"))
			{
				// We use the high byte of the parameter as it contains pad information.
				s32 deviceId = int(m_Parameters[i]) >> 24;

				Assertf(ioSource::IsValidDevice(deviceId), "Invalid device assigned to input");
				if(!ioSource::IsValidDevice(deviceId))
					deviceId = ioSource::IOMD_DEFAULT;

				sources.Push( ioSource(m_Sources[i], 0xFFFFFF & m_Parameters[i], deviceId) );
			}
		}
	}
}

void rage::ioMapper::GetMappedValues( const ioSource& source, ioValueList& values ) const
{
	// return all sources mapped to a value.
	for(s32 i = 0; i < m_Count; ++i)
	{
		if(source.m_Device == m_Sources[i] && source.m_Parameter == (m_Parameters[i] & 0xFFFFFF))
		{
			if(Verifyf(values.size() < values.max_size(), "Inputs associated with source has exceeded the limit! The list WILL be truncated to prevent a crash and WILL need increasing!"))
			{
				values.Push(m_Values[i]);
			}
		}
	}
}

void rage::ioMapper::GetRelatedSources( const ioSource& EXTRA_MOUSE_SUPPORT_ONLY(source), ioMapper::ioSourceList& EXTRA_MOUSE_SUPPORT_ONLY(relatedSources) )
{
#if RSG_EXTRA_MOUSE_SUPPORT
	ioSource altSource = source;

	// if a mouse axis.
	if(source.m_Device >= IOMS_MOUSE_ABSOLUTEAXIS && source.m_Device <= IOMS_MOUSE_NORMALIZED)
	{
		bool isXAxis = IsMouseXAxis(static_cast<ioMapperParameter>(source.m_Parameter));

		// add each type of mouse axis.
		for(s32 device = IOMS_MOUSE_ABSOLUTEAXIS; device <= IOMS_MOUSE_NORMALIZED; ++device)
		{
			altSource.m_Device = static_cast<ioMapperSource>(device);

			for(u32 param = IOM_AXIS_X; param < MOUSE_AXIS_MAX; ++param)
			{
				if( param != BASIC_MOUSE_AXIS_MAX && 
					(altSource.m_Device != source.m_Device || source.m_Parameter != param) &&
					IsMouseXAxis(static_cast<ioMapperParameter>(param)) == isXAxis )
				{
					altSource.m_Parameter = param;
					SafeAddSourceToList(altSource, relatedSources);
				}
			}
		}
	}
	// if a mouse wheel
	else if(source.m_Device == IOMS_MOUSE_WHEEL)
	{
		// add each wheel type.
		for(u32 param = IOM_WHEEL_UP; param <= IOM_IAXIS_WHEEL_RELATIVE; ++param)
		{
			// do not add the original wheel type.
			altSource.m_Parameter = param;
			if(param != source.m_Parameter)
			{
				SafeAddSourceToList(altSource, relatedSources);
			}
		}
	}
#endif // RSG_EXTRA_MOUSE_SUPPORT
}

u32 ioMapper::GetMkbAxisPositive(u32 param, bool includeFlags)
{
	u32 positiveParam = (param >> IOMC_MKB_POSITIVE_AXIS_SHIFT) & IOMC_MKB_AXIS_MASK;

	if((param & IOMC_MKB_AXIS_POSITIVE_IS_WHEEL) == IOMC_MKB_AXIS_POSITIVE_IS_WHEEL)
	{
		positiveParam += IOMT_MOUSE_WHEEL;
	}

	if(includeFlags)
	{
		positiveParam |= (param & (IOMC_MKB_AXIS_POSITIVE_IS_MOUSE | IOMC_MKB_AXIS_POSITIVE_IS_WHEEL));
	}

	return positiveParam;
}

u32 ioMapper::GetMkbAxisNegative(u32 param, bool includeFlags)
{
	u32 negativeParam =  param & IOMC_MKB_AXIS_MASK;

	if((param & IOMC_MKB_AXIS_NEGATIVE_IS_WHEEL) == IOMC_MKB_AXIS_NEGATIVE_IS_WHEEL)
	{
		negativeParam += IOMT_MOUSE_WHEEL;
	}

	if(includeFlags)
	{
		negativeParam |= (param & (IOMC_MKB_AXIS_NEGATIVE_IS_MOUSE | IOMC_MKB_AXIS_POSITIVE_IS_WHEEL));
	}

	return negativeParam;
}

u32 ioMapper::MakeMkbAxis(ioMapperParameter positiveParam, ioMapperParameter negativeParam)
{
	// Convert mouse wheel values to lower than two bytes as thats all the space we have.
	u32 positive = positiveParam;
	if((positive & IOMT_MOUSE_WHEEL) == IOMT_MOUSE_WHEEL)
	{
		positive -= IOMT_MOUSE_WHEEL;
	}
	u32 negative = negativeParam;
	if((negative & IOMT_MOUSE_WHEEL) == IOMT_MOUSE_WHEEL)
	{
		negative -= IOMT_MOUSE_WHEEL;
	}

	u32 param = ((positive & IOMC_MKB_AXIS_MASK) << IOMC_MKB_POSITIVE_AXIS_SHIFT) | (negative & IOMC_MKB_AXIS_MASK);

#if RSG_EXTRA_MOUSE_SUPPORT
	if((positiveParam & IOMT_MOUSE_BUTTON) == IOMT_MOUSE_BUTTON)
	{
		param |= IOMC_MKB_AXIS_POSITIVE_IS_MOUSE;
	}
	else if((positiveParam & IOMT_MOUSE_WHEEL) == IOMT_MOUSE_WHEEL)
	{
		param |= IOMC_MKB_AXIS_POSITIVE_IS_WHEEL;
	}

	if((negativeParam & IOMT_MOUSE_BUTTON) == IOMT_MOUSE_BUTTON)
	{
		param |= IOMC_MKB_AXIS_NEGATIVE_IS_MOUSE;
	}
	else if((negativeParam & IOMT_MOUSE_WHEEL) == IOMT_MOUSE_WHEEL)
	{
		param |= IOMC_MKB_AXIS_NEGATIVE_IS_WHEEL;
	}
#endif // RSG_EXTRA_MOUSE_SUPPORT

	return param;
}

#if ENABLE_INPUT_COMBOS
float ioMapper::GetSourceValue(const ioSource& source, bool forceKeyboardMouse)
{
	ioValue::ValueType newValue = 0;

	switch (source.m_Device) {
	case IOMS_KEYBOARD:
		newValue = p != KEY_NULL && ioKeyboard::KeyDown(source.m_Parameter) && (ioMapper::GetEnableKeyboard() || forceKeyboardMouse) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
#if RSG_PC
		if (newValue && source.m_Parameter==KEY_TAB && (ioKeyboard::KeyPressed(KEY_LMENU) || ioKeyboard::KeyPressed(KEY_RMENU)))
			newValue = ioValue::CENTER_AXIS;
#endif // RSG_PC
		break;
	case IOMS_MKB_AXIS:
		if (ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
		{
			bool negativeDown;

#if RSG_EXTRA_MOUSE_SUPPORT
			if (IsMkbAxisNegativeAMouseButton(source.m_Parameter))
			{
				unsigned button = GetMkbAxisNegative(source.m_Parameter);
				negativeDown = (ioMouse::GetButtons() & button) == button;
			}
			else
#endif // RSG_EXTRA_MOUSE_SUPPORT
			{
				u32 keyCode = GetMkbAxisNegative(source.m_Parameter);
				negativeDown = keyCode != KEY_NULL && ioKeyboard::KeyDown(keyCode) != 0;
			}

			bool positiveDown;
#if RSG_EXTRA_MOUSE_SUPPORT
			if(IsMkbAxisPositiveAMouseButton(source.m_Parameter))
			{
				unsigned button = GetMkbAxisPositive(source.m_Parameter);
				positiveDown = (ioMouse::GetButtons() & button) == button;
			}
			else
#endif // RSG_EXTRA_MOUSE_SUPPORT
			{
				u32 keyCode = GetMkbAxisPositive(source.m_Parameter);
				positiveDown = keyCode != KEY_NULL && ioKeyboard::KeyDown(keyCode) != 0;
			}

			if(negativeDown && !positiveDown)
			{
				newValue = ioValue::MIN_AXIS;
			}
			else if(!negativeDown && positiveDown)
			{
				newValue = ioValue::MAX_AXIS;
			}
			else
			{
				newValue = ioValue::CENTER_AXIS;
			}
		}
		break;
	case IOMS_DIGITALBUTTON_AXIS:
		{
			int buttons = ioPad::GetPad(source.m_DeviceIndex).GetButtons();
			u32 zeroButton = 1 << (source.m_Parameter & 255);
			u32 oneButton = 1 << ((source.m_Parameter >> 8) & 255);
			int state = ((buttons & zeroButton)? 1 : 0) | ((buttons & oneButton)? 2 : 0);
			if (state == 1)
				newValue = ioValue::MIN_AXIS;
			else if (state == 2)
				newValue = ioValue::MAX_AXIS;
			else
				newValue = ioValue::CENTER_AXIS;
			break;
		}
	case IOMS_MOUSE_ABSOLUTEAXIS:
		if (source.m_Parameter==IOM_AXIS_X) newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormX() * ioValue::MAX_AXIS_F);
		else if (source.m_Parameter==IOM_AXIS_Y) newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormY() * ioValue::MAX_AXIS_F);
		break;
#if RSG_EXTRA_MOUSE_SUPPORT
	case IOMS_MOUSE_CENTEREDAXIS:
		if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
		{
			if (source.m_Parameter==IOM_AXIS_X || source.m_Parameter==IOM_AXIS_X_LEFT || source.m_Parameter==IOM_AXIS_X_RIGHT)
			{
				newValue = static_cast<s32>(ioMouse::GetNormalizedX() * ioValue::MAX_AXIS_F * 2.0f) - ioValue::MAX_AXIS;

				if((source.m_Parameter==IOM_AXIS_X_LEFT && newValue > 0) || (source.m_Parameter==IOM_AXIS_X_RIGHT && newValue < 0))
					newValue = 0;
			}
			else if (source.m_Parameter==IOM_AXIS_Y || source.m_Parameter==IOM_AXIS_Y_UP || source.m_Parameter==IOM_AXIS_Y_DOWN)
			{
				newValue = static_cast<s32>(ioMouse::GetNormalizedY() * ioValue::MAX_AXIS_F * 2.0f) - ioValue::MAX_AXIS;

				if((source.m_Parameter==IOM_AXIS_Y_UP && newValue > 0) || (source.m_Parameter==IOM_AXIS_Y_DOWN && newValue < 0))
					newValue = 0;
			}
		}
		break;
	case IOMS_MOUSE_SCALEDAXIS:
#endif // RSG_EXTRA_MOUSE_SUPPORT
	case IOMS_MOUSE_RELATIVEAXIS:
		if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
		{
			if (source.m_Parameter==IOM_AXIS_X || source.m_Parameter==IOM_IAXIS_X || source.m_Parameter==IOM_AXIS_X_LEFT || source.m_Parameter==IOM_AXIS_X_RIGHT)
			{
				s32 invert;
				if(source.m_Parameter == IOM_IAXIS_X)
				{
					invert = -1;
				}
				else
				{
					invert = 1;
				}
				newValue = static_cast<ioValue::ValueType>(ioMouse::GetDX() * invert) * ioValue::LEGACY_AXIS_SCALE;

				if((source.m_Parameter==IOM_AXIS_X_LEFT && newValue > 0) || (source.m_Parameter==IOM_AXIS_X_RIGHT && newValue < 0))
					newValue = 0;
			}
			else if (source.m_Parameter==IOM_AXIS_Y || source.m_Parameter==IOM_IAXIS_Y || source.m_Parameter==IOM_AXIS_Y_UP || source.m_Parameter==IOM_AXIS_Y_DOWN)
			{
				s32 invert;
				if(source.m_Parameter == IOM_IAXIS_Y)
				{
					invert = -1;
				}
				else
				{
					invert = 1;
				}
				newValue = static_cast<ioValue::ValueType>(ioMouse::GetDY() * invert) * ioValue::LEGACY_AXIS_SCALE;

				if((source.m_Parameter==IOM_AXIS_Y_UP && newValue > 0) || (source.m_Parameter==IOM_AXIS_Y_DOWN && newValue < 0))
					newValue = 0;
			}
		}
		break;
#if RSG_EXTRA_MOUSE_SUPPORT
	case IOMS_MOUSE_NORMALIZED:
		if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
		{
			if (source.m_Parameter==IOM_AXIS_X || source.m_Parameter==IOM_AXIS_X_LEFT || source.m_Parameter==IOM_AXIS_X_RIGHT)
			{
				newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormalizedX() * ioValue::MAX_AXIS_F);

				if((source.m_Parameter==IOM_AXIS_X_LEFT && newValue > 0) || (source.m_Parameter==IOM_AXIS_X_RIGHT && newValue < 0))
					newValue = 0;
			}
			else if (source.m_Parameter==IOM_AXIS_Y || source.m_Parameter==IOM_AXIS_Y_UP || source.m_Parameter==IOM_AXIS_Y_DOWN)
			{
				newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormalizedY() * ioValue::MAX_AXIS_F);

				if((source.m_Parameter==IOM_AXIS_Y_UP && newValue > 0) || (source.m_Parameter==IOM_AXIS_Y_DOWN && newValue < 0))
					newValue = 0;
			}
			else if(source.m_Parameter == IOM_IAXIS_X)
			{
				newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormalizedX() * -ioValue::MAX_AXIS_F);
			}
			else if(source.m_Parameter == IOM_IAXIS_Y)
			{
				newValue = static_cast<ioValue::ValueType>(ioMouse::GetNormalizedY() * -ioValue::MAX_AXIS_F);
			}
		}
		break;
#endif // RSG_EXTRA_MOUSE_SUPPORT

	case IOMS_MOUSE_BUTTON:
		if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
		{
			newValue = (ioMouse::GetButtons() & source.m_Parameter) == source.m_Parameter ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		}
		break;
	case IOMS_MOUSE_WHEEL:
		if ((ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse) && ioMouse::HasWheel())
		{
			if (source.m_Parameter==IOM_WHEEL_UP) newValue = (ioMouse::GetDZ() > 0) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (source.m_Parameter==IOM_WHEEL_DOWN) newValue = (ioMouse::GetDZ() < 0) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			else if (source.m_Parameter==IOM_AXIS_WHEEL_DELTA) newValue = static_cast<ioValue::ValueType>(ioMouse::GetDZ());
			else if (source.m_Parameter==IOM_IAXIS_WHEEL_DELTA) newValue = static_cast<ioValue::ValueType>(-ioMouse::GetDZ());
			else if (source.m_Parameter==IOM_AXIS_WHEEL || source.m_Parameter==IOM_IAXIS_WHEEL)
			{
				if(ioMouse::GetDZ() > 0) newValue = (source.m_Parameter==IOM_AXIS_WHEEL) ? ioValue::MAX_AXIS : ioValue::MIN_AXIS;
				else if(ioMouse::GetDZ() < 0) newValue = (source.m_Parameter==IOM_AXIS_WHEEL) ? ioValue::MIN_AXIS : ioValue::MAX_AXIS;
			}
		}
		break;
	case IOMS_MOUSE_BUTTONANY:
		if(ioMapper::GetEnabledKeyboardMouse() || forceKeyboardMouse)
		{
			newValue = (ioMouse::GetButtons() & source.m_Parameter) != 0 ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		}
		break;
	case IOMS_PAD_DIGITALBUTTON:
#if IS_CONSOLE && __BANK
		// if local bank widgets are up, mask out the D-pad since it's usually connected
		// to something annoying like a polar camera or a car horn
		if (BANKMGR.IsUsingPad(source.m_DeviceIndex))
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetButtons() & source.m_Parameter & ~(ioPad::LLEFT | ioPad::LRIGHT | ioPad::LUP | ioPad::LDOWN)) == source.m_Parameter ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else
#endif
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetButtons() & source.m_Parameter) == source.m_Parameter? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;

		break;
	case IOMS_PAD_DEBUGBUTTON:
#if __DEBUGBUTTONS
# if IS_CONSOLE && __BANK
		// if local bank widgets are up, mask out the D-pad since it's usually connected
		// to something annoying like a polar camera or a car horn
		if (BANKMGR.IsUsingPad(source.m_DeviceIndex))
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetDebugButtons() & source.m_Parameter & ~(ioPad::LLEFT | ioPad::LRIGHT | ioPad::LUP | ioPad::LDOWN)) == source.m_Parameter ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else
# endif
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetDebugButtons() & source.m_Parameter) == source.m_Parameter ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
#else
		newValue = 0;
#endif	// __DEBUGBUTTONS
		break;
	case IOMS_PAD_DIGITALBUTTONANY:
#if IS_CONSOLE && __BANK
		// if local bank widgets are up, mask out the D-pad since it's usually connected
		// to something annoying like a polar camera or a car horn
		if (BANKMGR.IsUsingPad(source.m_DeviceIndex))
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetButtons() & source.m_Parameter & ~(ioPad::LLEFT | ioPad::LRIGHT | ioPad::LUP | ioPad::LDOWN)) != 0 ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else
#endif
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetButtons() & source.m_Parameter) != 0 ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		break;		
	case IOMS_PAD_ANALOGBUTTON:
		newValue = ioPad::GetPad(source.m_DeviceIndex).GetAnalogButton((ioPad::ePadButton) source.m_Parameter);

		// The scale of a u8.
		EXTRA_MOUSE_SUPPORT_ONLY(newValue /= 255.0f);
		if(newValue==0)
		{
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetButtons() & (1<<source.m_Parameter)) != 0 ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		}
		break;
	case IOMS_PAD_AXIS:
		// NOTE: to support a circular dead zone for axis diagonals,
		//       use 0.57 for x and y (approximately 0.8 * sin(0.25 pi))

		// We subtract one to better center the axis as there is no center value in a u8.
		if (source.m_Parameter==IOM_AXIS_LX) newValue	   = Clamp((ioPad::GetPad(source.m_DeviceIndex).GetLeftX()  - ioValue::LEGACY_AXIS_SHIFT) * ioValue::LEGACY_AXIS_SCALE, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
		else if (source.m_Parameter==IOM_AXIS_LY) newValue = Clamp((ioPad::GetPad(source.m_DeviceIndex).GetLeftY()  - ioValue::LEGACY_AXIS_SHIFT) * ioValue::LEGACY_AXIS_SCALE, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
		else if (source.m_Parameter==IOM_AXIS_RX) newValue = Clamp((ioPad::GetPad(source.m_DeviceIndex).GetRightX() - ioValue::LEGACY_AXIS_SHIFT) * ioValue::LEGACY_AXIS_SCALE, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
		else if (source.m_Parameter==IOM_AXIS_RY) newValue = Clamp((ioPad::GetPad(source.m_DeviceIndex).GetRightY() - ioValue::LEGACY_AXIS_SHIFT) * ioValue::LEGACY_AXIS_SCALE, ioValue::MIN_AXIS, ioValue::MAX_AXIS);
		else if (source.m_Parameter==IOM_AXIS_LUP)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormLeftY() < -0.80f && s_abs(ioPad::GetPad(source.m_DeviceIndex).GetNormLeftX()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LDOWN)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormLeftY() > 0.80f && s_abs(ioPad::GetPad(source.m_DeviceIndex).GetNormLeftX()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LLEFT)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormLeftX() < -0.80f && s_abs(ioPad::GetPad(source.m_DeviceIndex).GetNormLeftY()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LRIGHT)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormLeftX() > 0.80f && s_abs(ioPad::GetPad(source.m_DeviceIndex).GetNormLeftY()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LUR)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormLeftY() < -0.57f && ioPad::GetPad(source.m_DeviceIndex).GetNormLeftX() > 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LUL)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormLeftY() < -0.57f && ioPad::GetPad(source.m_DeviceIndex).GetNormLeftX() < -0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LDR)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormLeftY() > 0.57f && ioPad::GetPad(source.m_DeviceIndex).GetNormLeftX() > 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LDL)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormLeftY() > 0.57f && ioPad::GetPad(source.m_DeviceIndex).GetNormLeftX() < -0.57f)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RUP)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormRightY() < -0.80f && s_abs(ioPad::GetPad(source.m_DeviceIndex).GetNormRightX()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RDOWN)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormRightY() >  0.80f && s_abs(ioPad::GetPad(source.m_DeviceIndex).GetNormRightX()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RLEFT)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormRightX() < -0.80f && s_abs(ioPad::GetPad(source.m_DeviceIndex).GetNormRightY()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RRIGHT)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormRightX() > 0.80f && s_abs(ioPad::GetPad(source.m_DeviceIndex).GetNormRightY()) < 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RUR)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormRightY() < -0.57f && ioPad::GetPad(source.m_DeviceIndex).GetNormRightX() > 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RUL)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormRightY() < -0.57f && ioPad::GetPad(source.m_DeviceIndex).GetNormRightX() < -0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RDR)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormRightY() > 0.57f && ioPad::GetPad(source.m_DeviceIndex).GetNormRightX() > 0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RDL)
			newValue = (ioPad::GetPad(source.m_DeviceIndex).GetNormRightY() > 0.57f && ioPad::GetPad(source.m_DeviceIndex).GetNormRightX() < -0.57f) ? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_DPADX) {
			// Both branches of this code are designed to behave sanely even if left/right
			// somehow happen to both register as active (ie on an old worn-out controller)
			{
				int buttons = ioPad::GetPad(source.m_DeviceIndex).GetButtons() & (ioPad::LLEFT | ioPad::LRIGHT);
				newValue = buttons == ioPad::LLEFT ? ioValue::MIN_AXIS : buttons == ioPad::LRIGHT? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			}
		}
		else if (source.m_Parameter==IOM_AXIS_DPADY) {
			// Both branches of this code are designed to behave sanely even if up/down
			// somehow happen to both register as active (ie on an old worn-out controller)
			{
				int buttons = ioPad::GetPad(source.m_DeviceIndex).GetButtons() & (ioPad::LUP | ioPad::LDOWN);
				newValue = buttons == ioPad::LUP ? ioValue::MIN_AXIS : buttons == ioPad::LDOWN? ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
			}
		}
		else if (source.m_Parameter==IOM_AXIS_LY_UP)
			newValue = (newValue=ioPad::GetPad(source.m_DeviceIndex).GetLeftY())<127 ? ioValue::MAX_AXIS -(newValue*ioValue::LEGACY_AXIS_SCALE) : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LY_DOWN)
			newValue = (newValue=ioPad::GetPad(source.m_DeviceIndex).GetLeftY())>127 ? (newValue*ioValue::LEGACY_AXIS_SCALE)-ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LX_LEFT)
			newValue = (newValue=ioPad::GetPad(source.m_DeviceIndex).GetLeftX())<127 ? ioValue::MAX_AXIS -(newValue*ioValue::LEGACY_AXIS_SCALE) : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_LX_RIGHT)
			newValue = (newValue=ioPad::GetPad(source.m_DeviceIndex).GetLeftX())>127 ? (newValue*ioValue::LEGACY_AXIS_SCALE)-ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RY_UP)
			newValue = (newValue=ioPad::GetPad(source.m_DeviceIndex).GetRightY())<127 ? ioValue::MAX_AXIS -(newValue*ioValue::LEGACY_AXIS_SCALE) : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RY_DOWN)
			newValue = (newValue=ioPad::GetPad(source.m_DeviceIndex).GetRightY())>127 ? (newValue*ioValue::LEGACY_AXIS_SCALE)-ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RX_LEFT)
			newValue = (newValue=ioPad::GetPad(source.m_DeviceIndex).GetRightX())<127 ? ioValue::MAX_AXIS -(newValue*ioValue::LEGACY_AXIS_SCALE) : ioValue::CENTER_AXIS;
		else if (source.m_Parameter==IOM_AXIS_RX_RIGHT)
			newValue = (newValue=ioPad::GetPad(source.m_DeviceIndex).GetRightX())>127 ? (newValue*ioValue::LEGACY_AXIS_SCALE)-ioValue::MAX_AXIS : ioValue::CENTER_AXIS;
		break;
#if !IS_CONSOLE
	case IOMS_JOYSTICK_BUTTON:
		//newValue = Clamp((ioJoystick::GetStick(padIndex).GetAnalogButton(p) - s_BaseAxes[padIndex][p]) * ioValue::LEGACY_AXIS_SCALE, ioValue::CENTER_AXIS, ioValue::MAX_AXIS);
		newValue = Clamp(ioJoystick::GetStick(source.m_DeviceIndex).GetAnalogButton(source.m_Parameter) * ioValue::LEGACY_AXIS_SCALE, ioValue::CENTER_AXIS, ioValue::MAX_AXIS);
		break;
	case IOMS_JOYSTICK_AXIS:
		newValue = Clamp(static_cast<ioValue::ValueType>(ioJoystick::GetStick(source.m_DeviceIndex).GetNormAxis(source.m_Parameter) * ioValue::MAX_AXIS), -ioValue::MAX_AXIS, ioValue::MAX_AXIS);
		break;
	case IOMS_JOYSTICK_IAXIS:
		newValue = -Clamp(static_cast<ioValue::ValueType>(ioJoystick::GetStick(source.m_DeviceIndex).GetNormAxis(source.m_Parameter) * ioValue::MAX_AXIS), -ioValue::MAX_AXIS, ioValue::MAX_AXIS);
		break;
	case IOMS_JOYSTICK_AXIS_NEGATIVE:
		newValue = Clamp(static_cast<ioValue::ValueType>(-ioJoystick::GetStick(source.m_DeviceIndex).GetNormAxis(source.m_Parameter) * ioValue::MAX_AXIS), ioValue::CENTER_AXIS, ioValue::MAX_AXIS);
		break;
	case IOMS_JOYSTICK_AXIS_POSITIVE:
		newValue = Clamp(static_cast<ioValue::ValueType>(ioJoystick::GetStick(source.m_DeviceIndex).GetNormAxis(source.m_Parameter) * ioValue::MAX_AXIS), ioValue::CENTER_AXIS, ioValue::MAX_AXIS);
		break;
	case IOMS_JOYSTICK_POV: 
		{
			int pov = ioJoystick::GetStick(source.m_DeviceIndex).GetPOV(source.m_Parameter >> 2);
			if (pov != IOM_POV_UNDEFINED) {
				switch (source.m_Parameter & 3) {
				case POV_UP:	newValue = (pov > 27000 || pov < 9000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
				case POV_RIGHT:	newValue = (pov > 0 && pov < 18000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
				case POV_DOWN:	newValue = (pov > 9000 && pov < 27000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
				case POV_LEFT:	newValue = (pov > 18000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
				}
			}
			break;
		}
	case IOMS_JOYSTICK_POV_AXIS:
		{
			int neg = ioJoystick::GetStick(source.m_DeviceIndex).GetPOV((source.m_Parameter & 0xFF) >> 2);
			int pos = ioJoystick::GetStick(source.m_DeviceIndex).GetPOV((source.m_Parameter & 0xFF00) >> 10);


			if(neg != IOM_POV_UNDEFINED && pos != IOM_POV_UNDEFINED)
			{
				ioValue::ValueType negValue = 0;
				ioValue::ValueType posValue = 0;

				// negative
				switch (source.m_Parameter & 3) {
				case POV_UP:	negValue = (neg > 27000 || neg < 9000)? ioValue::MIN_AXIS : ioValue::CENTER_AXIS; break;
				case POV_RIGHT:	negValue = (neg > 0 && neg < 18000)? ioValue::MIN_AXIS : ioValue::CENTER_AXIS; break;
				case POV_DOWN:	negValue = (neg > 9000 && neg < 27000)? ioValue::MIN_AXIS : ioValue::CENTER_AXIS; break;
				case POV_LEFT:	negValue = (neg > 18000)? ioValue::MIN_AXIS : ioValue::CENTER_AXIS; break;
				}
				// positive
				switch (((source.m_Parameter & 0xFF00) >> 8) & 3) {
				case POV_UP:	posValue = (pos > 27000 || pos < 9000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
				case POV_RIGHT:	posValue = (pos > 0 && pos < 18000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
				case POV_DOWN:	posValue = (pos > 9000 && pos < 27000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
				case POV_LEFT:	posValue = (pos > 18000)? ioValue::MAX_AXIS : ioValue::CENTER_AXIS; break;
				}

				newValue = negValue + posValue;
			}
		}
		break;
#endif // !IS_CONSOLE

	case IOMS_TOUCHPAD_ABSOLUTE_AXIS:
		{
#if RSG_ORBIS
			if(source.m_Parameter == IOM_AXIS_X)
			{
				newValue = static_cast<ioValue::ValueType>(ioPad::GetPad(source.m_DeviceIndex).GetTouchXNorm() * ioValue::MAX_AXIS_F);
			}
			else if(source.m_Parameter == IOM_AXIS_Y)
			{
				newValue = static_cast<ioValue::ValueType>(ioPad::GetPad(source.m_DeviceIndex).GetTouchYNorm() * ioValue::MAX_AXIS_F);
			}
#endif // RSG_ORBIS
		}
		break;

	case IOMS_TOUCHPAD_CENTERED_AXIS:
		{
#if RSG_ORBIS
			if(source.m_Parameter == IOM_AXIS_X)
			{
				newValue = static_cast<ioValue::ValueType>(ioPad::GetPad(source.m_DeviceIndex).GetTouchXCenterNorm(0) * ioValue::MAX_AXIS_F);
			}
			else if(source.m_Parameter == IOM_AXIS_Y)
			{
				newValue = static_cast<ioValue::ValueType>(ioPad::GetPad(source.m_DeviceIndex).GetTouchYCenterNorm(0) * ioValue::MAX_AXIS_F);
			}
#endif // RSG_ORBIS
		}
		break;

	default:
		break;
	}

	return static_cast<float>(newValue) / ioValue::MAX_AXIS_F;
}
#endif // ENABLE_INPUT_COMBOS


ioMapperParameter ioMapper::ConvertParameterToMapperValue( const ioMapperSource source, u32 deviceParameter )
{
	ioMapperParameter parameter = IOM_UNDEFINED;

	switch(source)
	{
	case IOMS_MKB_AXIS:
		if( IsMkbAxisNegativeAMouseButton(deviceParameter) ||
			IsMkbAxisPositiveAMouseButton(deviceParameter) )
		{
			parameter = static_cast<ioMapperParameter>(IOMT_MOUSE_BUTTON | (deviceParameter & IOMC_MKB_AXIS_MASK));
			Assertf((parameter & IOMT_MOUSE_BUTTON) == IOMT_MOUSE_BUTTON, "Invalid mouse button type mapped to input!");
		}
		else if( IsMkbAxisNegativeAMouseWheel(deviceParameter) ||
				 IsMkbAxisPositiveAMouseWheel(deviceParameter) )
		{
			parameter = static_cast<ioMapperParameter>(IOMT_MOUSE_WHEEL | (deviceParameter & IOMC_MKB_AXIS_MASK));
			Assertf((parameter & IOMT_MOUSE_WHEEL) == IOMT_MOUSE_WHEEL, "Invalid mouse wheel type mapped to input!");
		}
		else
		{
			parameter = static_cast<ioMapperParameter>(deviceParameter);
			Assertf((parameter & IOMT_KEYBOARD) == IOMT_KEYBOARD, "Invalid key type mapped to input!");
		}
		break;
	case IOMS_KEYBOARD:
		parameter = static_cast<ioMapperParameter>(deviceParameter);
		Assertf((parameter & IOMT_KEYBOARD) == IOMT_KEYBOARD, "Invalid key type mapped to input!");
		break;

	case IOMS_MOUSE_ABSOLUTEAXIS:
		parameter = static_cast<ioMapperParameter>(deviceParameter);
		Assertf((parameter & IOMT_MOUSE_AXIS) == IOMT_MOUSE_AXIS && parameter < BASIC_MOUSE_AXIS_MAX, "Invalid basic mouse axis type mapped to input!");
		break;
	case IOMS_MOUSE_RELATIVEAXIS:
	case IOMS_MOUSE_SCALEDAXIS:
	case IOMS_MOUSE_NORMALIZED:
	case IOMS_MOUSE_CENTEREDAXIS:
		parameter = static_cast<ioMapperParameter>(deviceParameter);
		Assertf((parameter & IOMT_MOUSE_AXIS) == IOMT_MOUSE_AXIS && parameter < MOUSE_AXIS_MAX && parameter != BASIC_MOUSE_AXIS_MAX, "Invalid mouse axis type mapped to input!");
		break;

	case IOMS_MOUSE_WHEEL:
		parameter = static_cast<ioMapperParameter>(deviceParameter);
		Assertf((parameter & IOMT_MOUSE_WHEEL) == IOMT_MOUSE_WHEEL, "Invalid mouse wheel type mapped to input!");
		break;

	case IOMS_MOUSE_BUTTON:
		parameter = static_cast<ioMapperParameter>(IOMT_MOUSE_BUTTON | deviceParameter);
		Assertf((parameter & IOMT_MOUSE_BUTTON) == IOMT_MOUSE_BUTTON, "Invalid mouse button type mapped to input!");
		break;

	case IOMS_MOUSE_BUTTONANY:
	case IOMS_PAD_DIGITALBUTTONANY:
		parameter = static_cast<ioMapperParameter>(deviceParameter);
		Assertf(parameter != IOM_ANY_BUTTON, "Parameter must be set to IOM_ANY_BUTTON for input using any button!");
		break;

	case IOMS_PAD_DIGITALBUTTON:
	case IOMS_PAD_DEBUGBUTTON:
		if(Verifyf(deviceParameter != 0, "Invalid device parameter for pad buttons!"))
		{
			int buttonId = 0;
			while(deviceParameter >>= 1)
			{
				++buttonId;
			}

			parameter = static_cast<ioMapperParameter>(IOMT_PAD_BUTTON | buttonId);
			Assertf((parameter & IOMT_PAD_BUTTON) == IOMT_PAD_BUTTON, "Invalid pad button type mapped to input!");
		}
		break;
	case IOMS_PAD_ANALOGBUTTON:
		parameter = static_cast<ioMapperParameter>(IOMT_PAD_INDEX | deviceParameter);
		Assertf((parameter & IOMT_PAD_INDEX) == IOMT_PAD_INDEX, "Invalid pad button type mapped to input!");
		break;


	case IOMS_PAD_AXIS:
		parameter = static_cast<ioMapperParameter>(deviceParameter);
		Assertf((parameter & IOMT_PAD_AXIS) == IOMT_PAD_AXIS, "Invalid pad axis type mapped to input!");
		break;

	case IOMS_JOYSTICK_BUTTON:
		parameter = static_cast<ioMapperParameter>(IOMT_JOYSTICK_BUTTON | deviceParameter);
		Assertf((parameter & IOMT_JOYSTICK_BUTTON) == IOMT_JOYSTICK_BUTTON, "Invalid joystick button type mapped to input!");
		break;

	case IOMS_JOYSTICK_AXIS:
	case IOMS_JOYSTICK_IAXIS:
	case IOMS_JOYSTICK_AXIS_NEGATIVE:
	case IOMS_JOYSTICK_AXIS_POSITIVE:
		parameter = static_cast<ioMapperParameter>(IOMT_JOYSTICK_AXIS | deviceParameter);
		Assertf((parameter & IOMT_JOYSTICK_AXIS) == IOMT_JOYSTICK_AXIS, "Invalid joystick axis type mapped to input!");
		break;

	case IOMS_JOYSTICK_POV:
	case IOMS_JOYSTICK_POV_AXIS:
		parameter = static_cast<ioMapperParameter>(IOMT_JOYSTICK_POV | deviceParameter);
		Assertf((parameter & IOMT_JOYSTICK_POV) == IOMT_JOYSTICK_POV, "Invalid joystick axis type mapped to input!");
		break;

	case IOMS_TOUCHPAD_ABSOLUTE_AXIS:
	case IOMS_TOUCHPAD_CENTERED_AXIS:
		parameter = static_cast<ioMapperParameter>(deviceParameter);
		Assertf((parameter & IOMT_MOUSE_AXIS) == IOMT_MOUSE_AXIS, "Invalid touchpad type!");
		break;

	default:
		Assertf(false, "Invalid source for input mapping!");
	}
	return parameter;
}

u32 ioMapper::ConvertParameterToDeviceValue(const ioMapperSource source, const ioMapperParameter mapperParameter)
{
	s32 parameter = mapperParameter;

	switch(source)
	{
	case IOMS_MKB_AXIS:
		Assertf( (parameter & IOMT_KEYBOARD) == IOMT_KEYBOARD ||
				 (parameter & IOMT_MOUSE_BUTTON) == IOMT_MOUSE_BUTTON ||
				 (parameter & IOMT_MOUSE_WHEEL) == IOMT_MOUSE_WHEEL,
				 "Invalid key or mouse button mapped to input!");
		break;

	case IOMS_KEYBOARD:
		Assertf((parameter & IOMT_KEYBOARD) == IOMT_KEYBOARD, "Invalid key type mapped to input!");
		break;

	case IOMS_MOUSE_ABSOLUTEAXIS:
		Assertf((parameter & IOMT_MOUSE_AXIS) == IOMT_MOUSE_AXIS && parameter < BASIC_MOUSE_AXIS_MAX, "Invalid basic mouse axis type mapped to input!");
		break;
	case IOMS_MOUSE_CENTEREDAXIS:
	case IOMS_MOUSE_RELATIVEAXIS:
	case IOMS_MOUSE_NORMALIZED:
	case IOMS_MOUSE_SCALEDAXIS:
		Assertf((parameter & IOMT_MOUSE_AXIS) == IOMT_MOUSE_AXIS && parameter < MOUSE_AXIS_MAX && parameter != BASIC_MOUSE_AXIS_MAX, "Invalid mouse axis type mapped to input!");
		break;

	case IOMS_MOUSE_WHEEL:
		Assertf((parameter & IOMT_MOUSE_WHEEL) == IOMT_MOUSE_WHEEL, "Invalid mouse wheel type mapped to input!");
		break;

	case IOMS_MOUSE_BUTTON:
		Assertf((parameter & IOMT_MOUSE_BUTTON) == IOMT_MOUSE_BUTTON, "Invalid mouse button type mapped to input!");
		parameter &= ~IOMT_MOUSE_BUTTON;
		break;

	case IOMS_MOUSE_BUTTONANY:
	case IOMS_PAD_DIGITALBUTTONANY:
		Assertf(parameter != IOM_ANY_BUTTON, "Parameter must be set to IOM_ANY_BUTTON for input using any button!");
		break;

	case IOMS_PAD_DIGITALBUTTON:
	case IOMS_PAD_DEBUGBUTTON:
		Assertf((parameter & IOMT_PAD_BUTTON) == IOMT_PAD_BUTTON, "Invalid pad button type mapped to input!");
		parameter &= ~IOMT_PAD_BUTTON;
		parameter = 1 << parameter;

		break;

	case IOMS_PAD_ANALOGBUTTON:
		Assertf((parameter & IOMT_PAD_INDEX) == IOMT_PAD_INDEX, "Invalid pad button type mapped to input!");
		parameter &= ~IOMT_PAD_INDEX;
		break;


	case IOMS_PAD_AXIS:
		Assertf((parameter & IOMT_PAD_AXIS) == IOMT_PAD_AXIS, "Invalid pad axis type mapped to input!");
		break;

	case IOMS_JOYSTICK_BUTTON:
		Assertf((parameter & IOMT_JOYSTICK_BUTTON) == IOMT_JOYSTICK_BUTTON, "Invalid joystick button type mapped to input!");
		parameter &= ~IOMT_JOYSTICK_BUTTON;
		break;

	case IOMS_JOYSTICK_AXIS:
	case IOMS_JOYSTICK_IAXIS:
	case IOMS_JOYSTICK_AXIS_NEGATIVE:
	case IOMS_JOYSTICK_AXIS_POSITIVE:
		Assertf((parameter & IOMT_JOYSTICK_AXIS) == IOMT_JOYSTICK_AXIS, "Invalid joystick axis type mapped to input!");
		parameter &= ~IOMT_JOYSTICK_AXIS;
		break;

	case IOMS_JOYSTICK_POV:
	case IOMS_JOYSTICK_POV_AXIS:
		Assertf((parameter & IOMT_JOYSTICK_POV) == IOMT_JOYSTICK_POV, "Invalid joystick axis type mapped to input!");
		parameter &= ~IOMT_JOYSTICK_POV;
		break;

	case IOMS_TOUCHPAD_ABSOLUTE_AXIS:
	case IOMS_TOUCHPAD_CENTERED_AXIS:
		Assertf((parameter & IOMT_MOUSE_AXIS) == IOMT_MOUSE_AXIS, "Invalid touchpad type!");
		break;

	default:
		Assertf(false, "Invalid source for input mapping!");
	}
	return parameter;
}

rage::ioMapper::ScanOptions::ScanOptions()
: m_MouseOptions(NO_MOUSE)
, m_KeyboardOptions(NO_KEYBOARD)
, m_PadOptions(NO_PAD)
#if RSG_PC
, m_JoystickOptions(NO_JOYSTICK)
#endif // RSG_PC
, m_DeviceID(ioSource::IOMD_UNKNOWN)
{}

#if RSG_PC
// The PC version is setup differently
rage::ioMapper::ScanOptions::ScanOptions( Setup_DefaultDeviceOptions )
: m_MouseOptions(MOUSE_BUTTONS | MOUSE_WHEEL)
, m_KeyboardOptions(KEYBOARD_BUTTONS)
, m_PadOptions(PAD_AXIS | PAD_AXIS_DIRECTION | PAD_BUTTONS)
, m_JoystickOptions(JOYSTICK_POV | JOYSTICK_BUTTONS | JOYSTICK_AXIS | JOYSTICK_AXIS_DIRECTION)
, m_DeviceID(ioSource::IOMD_ANY)
{}
#else
// The console version is only interested in the pad.
rage::ioMapper::ScanOptions::ScanOptions( Setup_DefaultDeviceOptions )
: m_MouseOptions(NO_MOUSE)
, m_KeyboardOptions(NO_KEYBOARD)
, m_PadOptions(PAD_AXIS | PAD_AXIS_DIRECTION | PAD_BUTTONS)
, m_DeviceID(ioSource::IOMD_DEFAULT)
{}
#endif // RSG_PC


float ioValue::GetHistoryNormalizedAtDepth(u8 depth, const ReadOptions& options) const 
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	AssertMsg(m_InvertValue == -1 || m_InvertValue == 1, "Invalid inversion value");

	ioValue::ValueType value = CENTER_AXIS;
	const HistoryValue& history = m_HistoryValues[GetHistoryIndex(depth)];

	if(IsEnabled() || options.IsFlagSet(ReadOptions::F_READ_DISABLED))
		value = m_InvertValue * history.m_value;

	return PerformInputFormatting( Normalize(value), history.m_source.m_Device, options );
}

u32 ioValue::GetHistoryTimeAtDepth(u8 depth) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	return m_HistoryValues[GetHistoryIndex(depth)].m_timeMS;
}

ioSource ioValue::GetHistorySourceAtDepth(u8 depth) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	return m_HistoryValues[GetHistoryIndex(depth)].m_source;
}

bool ioValue::IsDown(float threshold, const ReadOptions& options) const 
{ 
	if(threshold >= 0.0f)
		return GetNormalized(options) >= threshold; 
	else
		return GetNormalized(options) <= threshold; 
}

bool ioValue::HistoryHeldDown(u32 durationMS, float threshold, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check that the value was "down" over the whole duration.

	// Go over the history covered by the duration.
	u8 depth = 0;
	u32 timeNowMS = GetHistoryTimeAtDepth(0);
	u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);

	while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
	{
		// Make sure the value is correct (down).
		float value = GetHistoryNormalizedAtDepth(depth, options);
		if(		(threshold >= 0.0f && value >= threshold)
			||	(threshold < 0.0f  && value <= threshold) )
		{
			// Go deeper into the history.
			++depth;
		}
		else
		{
			// It is not currently down, so the condition can't be true.
			return false;
		}
	}

	// Seems to have been correct the whole time.
	return true;
}

bool ioValue::HistoryHeldDown(u32 durationMS, float downRangeMin, float downRangeMax, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check that the value was "down" over the whole duration.

	// Go over the history covered by the duration.
	u8 depth = 0;
	u32 timeNowMS = GetHistoryTimeAtDepth(0);
	u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
	while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
	{
		// Make sure the value is correct (down).
		if(InRange(GetHistoryNormalizedAtDepth(depth, options), downRangeMin, downRangeMax))
		{
			// Go deeper into the history.
			++depth;
		}
		else
		{
			// It is not currently down, so the condition can't be true.
			return false;
		}
	}

	// Seems to have been correct the whole time.
	return true;
}

bool ioValue::IsUp(float threshold, const ReadOptions& options) const 
{ 
	return !IsDown(threshold, options); 
}

bool ioValue::HistoryHeldUp(u32 durationMS, float threshold, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check that the value was "up" over the whole duration.

	// Go over the history covered by the duration.
	u8 depth = 0;
	u32 timeNowMS = GetHistoryTimeAtDepth(0);
	u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
	while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
	{
		float value = GetHistoryNormalizedAtDepth(depth, options);
		// Make sure the value is correct (up).
		if(		(threshold >= 0.0f && value < threshold)
			||	(threshold < 0.0f  && value > threshold) )
		{

			// Go deeper into the history.
			++depth;
		}
		else
		{
			// It is not currently up now, so the condition can't be true.
			return false;
		}
	}

	// Seems to have been correct the whole time.
	return true;
}

bool ioValue::HistoryHeldUp(u32 durationMS, float downRangeMin, float downRangeMax, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check that the value was "up" over the whole duration.

	// Go over the history covered by the duration.
	u8 depth = 0;
	u32 timeNowMS = GetHistoryTimeAtDepth(0);
	u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
	while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
	{
		// Make sure the value is correct (up).
		if(!InRange(GetHistoryNormalizedAtDepth(depth, options), downRangeMin, downRangeMax))
		{
			// Go deeper into the history.
			++depth;
		}
		else
		{
			// It is not currently up now, so the condition can't be true.
			return false;
		}
	}

	// Seems to have been correct the whole time.
	return true;
}

bool ioValue::WasDown(float threshold, const ReadOptions& options) const 
{ 
	if(threshold >= 0.0f)
		return GetLastNormalized(options) >= threshold;
	else
		return GetLastNormalized(options) <= threshold;
}

bool ioValue::HistoryPressedAndHeld(u32 durationMS, u32* pOutLastTimeMsWhereUp, float threshold, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check that the value is "down" now and was "up" at some point previously during the duration.

	// Make sure that it is currently down.
	if(IsDown(threshold))
	{
		// Go over the history covered by the duration.
		// Check if there was any value that was different (up).
		u8 depth = 1;
		u32 timeNowMS = GetHistoryTimeAtDepth(0);
		u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
		while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
		{
			float value = GetHistoryNormalizedAtDepth(depth, options);
			// Check if it is up.
			if(		(threshold >= 0.0f && value < threshold)
				||	(threshold < 0.0f  && value > threshold) )
			{
				if(pOutLastTimeMsWhereUp)
				{
					*pOutLastTimeMsWhereUp = GetHistoryTimeAtDepth(depth);
				}
				return true;
			}

			// Go deeper into the history.
			++depth;
		}

		// Seems to have been the down the whole time.
		return false;
	}
	else
	{
		// It is not currently down now, so the condition can't be true.
		return false;
	}
}

bool ioValue::HistoryPressedAndHeld(u32 durationMS, u32* pOutLastTimeMsWhereUp, float downRangeMin, float downRangeMax, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check that the value is "down" now and was "up" at some point previously during the duration.

	// Make sure that it is currently down.
	if(InRange(GetHistoryNormalizedAtDepth(0, options), downRangeMin, downRangeMax))
	{
		// Go over the history covered by the duration.
		// Check if there was any value that was different (up).
		u8 depth = 1;
		u32 timeNowMS = GetHistoryTimeAtDepth(0);
		u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
		while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
		{
			// Check if it is up.
			if(!InRange(GetHistoryNormalizedAtDepth(depth, options), downRangeMin, downRangeMax))
			{
				if(pOutLastTimeMsWhereUp)
				{
					*pOutLastTimeMsWhereUp = GetHistoryTimeAtDepth(depth);
				}
				return true;
			}

			// Go deeper into the history.
			++depth;
		}

		// Seems to have been the down the whole time.
		return false;
	}
	else
	{
		// It is not currently down now, so the condition can't be true.
		return false;
	}
}

bool ioValue::WasUp(float threshold, const ReadOptions& options) const 
{ 
	return !WasDown(threshold, options);
}

bool ioValue::HistoryReleasedAndHeld(u32 durationMS, u32* pOutLastTimeMsWhereDown, float threshold, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check that the value is "up" now and was "down" at some point previously during the duration.

	// Check that it is currently up.
	if(IsUp(threshold))
	{
		// Go over the history covered by the duration.
		// Check if there was a value that was different (down).
		u8 depth = 1;
		u32 timeNowMS = GetHistoryTimeAtDepth(0);
		u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
		while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
		{
			float value = GetHistoryNormalizedAtDepth(depth, options);

			// Check if it is down.
			if(		(threshold >= 0.0f && value >= threshold)
				||	(threshold < 0.0f  && value <= threshold) )
			{
				if(pOutLastTimeMsWhereDown)
				{
					*pOutLastTimeMsWhereDown = GetHistoryTimeAtDepth(depth);
				}
				return true;
			}

			// Go deeper into the history.
			++depth;
		}

		// Seems to have been up the whole time.
		return false;
	}
	else
	{
		// It is not currently up now, so the condition can't be true.
		return false;
	}
}

bool ioValue::HistoryReleasedAndHeld(u32 durationMS, u32* pOutLastTimeMsWhereDown, float downRangeMin, float downRangeMax, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");

	// Check that the value is "up" now and was "down" at some point previously during the duration.
	// Check that it is currently up.
	if(!InRange(GetHistoryNormalizedAtDepth(0, options), downRangeMin, downRangeMax))
	{
		// Go over the history covered by the duration.
		// Check if there was a value that was different (down).
		u8 depth = 1;
		u32 timeNowMS = GetHistoryTimeAtDepth(0);
		u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
		while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
		{
			// Check if it is down.
			if(InRange(GetHistoryNormalizedAtDepth(depth, options), downRangeMin, downRangeMax))
			{
				if(pOutLastTimeMsWhereDown)
				{
					*pOutLastTimeMsWhereDown = GetHistoryTimeAtDepth(depth);
				}
				return true;
			}

			// Go deeper into the history.
			++depth;
		}

		// Seems to have been up the whole time.
		return false;
	}
	else
	{
		// It is not currently up now, so the condition can't be true.
		return false;
	}
}

bool ioValue::IsReleasedAfterHistoryHeldDown(u32 durationMS, float threshold, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check that the value is "up" now and was "down" at some point previously during the duration.

	// Check that it is currently up.
	if(IsUp(threshold))
	{
		// Go over the history covered by the duration.
		// Check if there was a value that was up.
		u8 depth = 1;
		u32 timeNowMS = GetHistoryTimeAtDepth(0);
		u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
		while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
		{
			float value = GetHistoryNormalizedAtDepth(depth, options);

			// Check if it is up.
			if(		(threshold >= 0.0f && value < threshold)
				||	(threshold < 0.0f  && value > threshold) )
			{
				return false;
			}

			// Go deeper into the history.
			++depth;
		}

		// Seems to have been down the whole time.
		return true;
	}
	else
	{
		// It is not currently up now, so the condition can't be true.
		return false;
	}
}


bool ioValue::IsPressed(float threshold, const ReadOptions& options) const 
{ 
	return IsDown(threshold, options) && WasUp(threshold, options); 
}

bool ioValue::HistoryPressed(u32 durationMS, u32* pOutLastTimeMsTransitioned, float threshold, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check if data transitioned from "up" to "down" anywhere during the duration.
	u8 depth = 1;
	u32 timeNowMS = GetHistoryTimeAtDepth(0);
	u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
	// Changed loop exit condition so we look for entries at least durationMS instead of not more than durationMS.
	while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth-1) >= timeDurationStartMS))
	{
		// Check if there was a transition of the type we are looking for.
		float currentValue  = GetHistoryNormalizedAtDepth(depth - 1, options);
		float previousValue = GetHistoryNormalizedAtDepth(depth,	 options);

		// (is down now and was up)
		if(		(threshold >= 0.0f && currentValue >= threshold && previousValue < threshold)
			||	(threshold <  0.0f && currentValue <= threshold && previousValue > threshold) )
		{
			// Hand back the transition time.
			if(pOutLastTimeMsTransitioned)
			{
				*pOutLastTimeMsTransitioned = GetHistoryTimeAtDepth(depth);
			}

			// Let the caller know the condition was met.
			return true;
		}

		// Go deeper into the history.
		++depth;
	}

	// Seems to not have been any correct transitions the whole time.
	return false;
}

bool ioValue::HistoryPressed(u32 durationMS, u32* pOutLastTimeMsTransitioned, float downRangeMin, float downRangeMax, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check if data transitioned from "up" to "down" anywhere during the duration.
	u8 depth = 1;
	u32 timeNowMS = GetHistoryTimeAtDepth(0);
	u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
	// Changed loop exit condition so we look for entries at least durationMS instead of not more than durationMS.
	while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth-1) >= timeDurationStartMS))
	{
		// Check if there was a a transition of the type we are looking for.
		float currentValue =  GetHistoryNormalizedAtDepth(depth - 1, options);
		float previousValue = GetHistoryNormalizedAtDepth(depth,	options);

		// (is down now and was up)
		if(	 InRange(currentValue,	downRangeMin, downRangeMax) &&
			!InRange(previousValue, downRangeMin, downRangeMax) )
		{
			// Hand back the transition time.
			if(pOutLastTimeMsTransitioned)
			{
				*pOutLastTimeMsTransitioned = GetHistoryTimeAtDepth(depth);
			}

			// Let the caller know the condition was met.
			return true;
		}

		// Go deeper into the history.
		++depth;
	}

	// Seems to not have been any correct transitions the whole time.
	return false;
}

bool ioValue::IsReleased(float threshold, const ReadOptions& options) const 
{ 
	return IsUp(threshold, options) && WasDown(threshold, options); 
}

bool ioValue::HistoryReleased(u32 durationMS, u32* pOutLastTimeMsTransitioned, float threshold, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check if data transitioned from "down" to "up" anywhere during the duration.
	u8 depth = 1;
	u32 timeNowMS = GetHistoryTimeAtDepth(0);
	u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
	while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
	{
		// Check if there was a a transition of the type we are looking for.
		float currentValue  = GetHistoryNormalizedAtDepth(depth - 1, options);
		float previousValue = GetHistoryNormalizedAtDepth(depth,	 options);

		// (is up now and was down)
		if(		(threshold >= 0.0f && currentValue < threshold && previousValue >= threshold)
			||	(threshold < 0.0f  && currentValue > threshold && previousValue <= threshold) )
		{
			// Hand back the transition time.
			if(pOutLastTimeMsTransitioned)
			{
				*pOutLastTimeMsTransitioned = GetHistoryTimeAtDepth(depth);
			}

			// Let the caller know the condition was met.
			return true;
		}

		// Go deeper into the history.
		++depth;
	}

	// Seems to not have been any correct transitions the whole time.
	return false;
}

bool ioValue::HistoryReleased(u32 durationMS, u32* pOutLastTimeMsTransitioned, float downRangeMin, float downRangeMax, const ReadOptions& options) const
{
	AssertMsg(m_HistoryValues, "History not setup for ioValue");
	// Check if data transitioned from "down" to "up" anywhere during the duration.
	u8 depth = 1;
	u32 timeNowMS = GetHistoryTimeAtDepth(0);
	u32 timeDurationStartMS = (durationMS > timeNowMS)?0:(timeNowMS - durationMS);
	while((depth < MaxHistoryValues) && (GetHistoryTimeAtDepth(depth) >= timeDurationStartMS))
	{
		// Check if there was a a transition of the type we are looking for.
		float currentValue =  GetHistoryNormalizedAtDepth(depth - 1, options);
		float previousValue = GetHistoryNormalizedAtDepth(depth,	 options);

		// (is up now and was down)
		if( !InRange(currentValue, downRangeMin, downRangeMax) &&
			InRange(previousValue, downRangeMin, downRangeMax) )
		{
			// Hand back the transition time.
			if(pOutLastTimeMsTransitioned)
			{
				*pOutLastTimeMsTransitioned = GetHistoryTimeAtDepth(depth);
			}

			// Let the caller know the condition was met.
			return true;
		}

		// Go deeper into the history.
		++depth;
	}

	// Seems to not have been any correct transitions the whole time.
	return false;
}

void ioValue::Reset() 
{ 
	m_Value				= CENTER_AXIS;
	m_LastValue			= CENTER_AXIS;
	m_NextFrameValue	= CENTER_AXIS;
	m_LastUpdateID		= 0;
	m_Source			= ioSource::UNKNOWN_SOURCE;
	m_LastSource		= ioSource::UNKNOWN_SOURCE;
	m_NextFrameSource	= ioSource::UNKNOWN_SOURCE;
	m_enabled			= true;
	m_disableNextUpdate = false;
	if(m_HistoryValues)
	{
		for(u32 i = 0;i < MaxHistoryValues;++i)
		{
			m_HistoryValues[i].m_value	= CENTER_AXIS;
			m_HistoryValues[i].m_timeMS = 0;
			m_HistoryValues[i].m_source = ioSource::UNKNOWN_SOURCE;
		}
	}
}

void ioValue::InitValueAndHistory(ValueType value, u32 timeMS)
{
	m_Value = value;
	m_LastValue = value;

	if(m_HistoryValues)
	{
		for(u32 i = 0;i < MaxHistoryValues;++i)
		{
			m_HistoryValues[i].m_value = value;
			m_HistoryValues[i].m_timeMS = timeMS;
		}
	}
}

void ioValue::SetCurrentValue(ValueType value, const ioSource& source)
{
	if( (IsEnabled() || !m_DisableOptions.IsFlagSet(DisableOptions::F_DISABLE_UPDATE_WHEN_DISABLED)) &&
		(m_IgnoreSource == IOMS_UNDEFINED || m_IgnoreSource != source.m_Device) )
	{
		m_Value  = value;
		m_Source = source;
		if(m_HistoryValues)
		{
			m_HistoryValues[m_CurrentValueIndex].m_value  = value;
			m_HistoryValues[m_CurrentValueIndex].m_source = source;
		}
	}
}

void ioValue::SetNextValue(ValueType value, const ioSource& source)
{
	m_NextFrameValue  = value;
	m_NextFrameSource = source;
}

void ioValue::Update(u8 updateID, u32 timeMS)
{ 
	if (m_LastUpdateID != updateID)
	{
		if(IsEnabled() || !m_DisableOptions.IsFlagSet(DisableOptions::F_DISABLE_UPDATE_WHEN_DISABLED))
		{
			m_LastValue  = m_Value;
			m_LastSource = m_Source;

			if(m_IgnoreSource == IOMS_UNDEFINED || m_IgnoreSource != m_NextFrameSource.m_Device)
			{
				m_Value	 = m_NextFrameValue;
				m_Source = m_NextFrameSource;
			}
			else
			{
				m_Value  = CENTER_AXIS;
				m_Source = ioSource::UNKNOWN_SOURCE;
			}

			if(m_HistoryValues)
			{
				m_CurrentValueIndex = ((m_CurrentValueIndex + 1) % MaxHistoryValues);
				m_HistoryValues[m_CurrentValueIndex].m_value  = CENTER_AXIS;
				m_HistoryValues[m_CurrentValueIndex].m_source = ioSource::UNKNOWN_SOURCE;
				m_HistoryValues[m_CurrentValueIndex].m_timeMS = timeMS;
			}
		}
		else if(m_HistoryValues && IsEnabled() == false && m_DisableOptions.IsFlagSet(DisableOptions::F_DISABLE_UPDATE_WHEN_DISABLED))
		{
			// Update the history to be the current value so that history functions work. We fill the history with the last value the input had.
			m_CurrentValueIndex = ((m_CurrentValueIndex + 1) % MaxHistoryValues);
			m_HistoryValues[m_CurrentValueIndex].m_value  = m_LastValue;
			m_HistoryValues[m_CurrentValueIndex].m_source = m_LastSource;
			m_HistoryValues[m_CurrentValueIndex].m_timeMS = timeMS;
		}

		// Inputs are disabled for one frame only.
		if(m_disableNextUpdate)
		{
			m_enabled			= false;
			m_disableNextUpdate = false;
		}
		else
		{
			Enable();
		}

		if(m_IgnoreThisUpdate)
		{
			m_IgnoreThisUpdate = false;
		}
		else if(m_IgnoreSource != IOMS_UNDEFINED)
		{
			m_IgnoreSource = IOMS_UNDEFINED;
		}
	}

	m_NextFrameValue  = CENTER_AXIS;
	m_NextFrameSource = ioSource::UNKNOWN_SOURCE;
	m_LastUpdateID = updateID;
}

u8 ioValue::GetHistoryIndex(u8 depth) const
{
	FastAssert(depth < MaxHistoryValues);

	int index = 0;
	if( depth <= m_CurrentValueIndex)
	{
		index = m_CurrentValueIndex - depth;
	}
	else
	{
		if(depth>(MaxHistoryValues - 1)){depth=MaxHistoryValues-1;}
		index = (MaxHistoryValues) - (depth - m_CurrentValueIndex);
	}

	return (u8)index;
}


float ioValue::GetNorm(const ReadOptions& options) const 
{
	ReadOptions boundOptions = options;
	boundOptions.SetFlags(ReadOptions::F_UNBOUND, false);

	return GetNormalized(boundOptions);  
}

float ioValue::GetUnboundNorm(const ReadOptions& options) const 
{
	ReadOptions unboundOptions = options;
	unboundOptions.SetFlags(ReadOptions::F_UNBOUND, true);

	return GetNormalized(unboundOptions);  
}

float ioValue::GetLastNorm(const ReadOptions& options) const 
{
	ReadOptions boundOptions = options;
	boundOptions.SetFlags(ReadOptions::F_UNBOUND, false);

	return GetLastNormalized(boundOptions); 
}

float ioValue::GetLastUnboundNorm(const ReadOptions& options) const 
{
	ReadOptions unboundOptions = options;
	unboundOptions.SetFlags(ReadOptions::F_UNBOUND, true);

	return GetLastNormalized(unboundOptions); 
}

float ioValue::GetNorm01(const ReadOptions& options) const 
{
	return ClampIt(GetNormalized(options), 0.0f, 1.0f);
}

float ioValue::GetNormalized(const ReadOptions& options) const 
{
	return PerformInputFormatting( Normalize(GetValue(options)), m_Source.m_Device, options );
}

float ioValue::GetLastNormalized(const ReadOptions& options) const 
{ 
	return PerformInputFormatting( Normalize(GetLastValue(options)), m_LastSource.m_Device, options );
}

float ioValue::PerformInputFormatting(float value, const ioMapperSource source, const ReadOptions& options)
{
	if( options.m_DeadZone > 0.0f && 
		!options.IsFlagSet(ReadOptions::F_NO_DEAD_ZONE) &&
		( (RequiresDeadZone(source) || options.IsFlagSet(ReadOptions::F_DEAD_ZONE_ALL_SOURCES)) ))
	{
		value = ioAddDeadZone(value, options.m_DeadZone);
	}

	if(!options.IsFlagSet(ReadOptions::F_UNBOUND))
	{
		value = ClampIt(value, -1.0f, 1.0f);
	}

	return value;
}

ioValue::ValueType ioValue::GetValue(const ReadOptions& options) const 
{
	AssertMsg(m_InvertValue == -1 || m_InvertValue == 1, "Invalid inversion value");
	if(    IsEnabled() 
		|| options.IsFlagSet(ReadOptions::F_READ_DISABLED)
		|| (m_DisableOptions.IsFlagSet(DisableOptions::F_ALLOW_SUSTAINED) && options.IsFlagSet(ReadOptions::F_READ_SUSTAINED)) )
	{
		return m_InvertValue * m_Value;
	}
	else
	{
		return CENTER_AXIS;
	}
}

ioValue::ValueType ioValue::GetLastValue(const ReadOptions& options) const
{
	AssertMsg(m_InvertValue == -1 || m_InvertValue == 1, "Invalid inversion value");
	if(IsEnabled() || options.IsFlagSet(ReadOptions::F_READ_DISABLED))
	{
		return m_InvertValue * m_LastValue;
	}
	return CENTER_AXIS;
}
