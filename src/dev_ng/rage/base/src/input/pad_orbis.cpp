// 
// input/pad_orbis.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 


#if RSG_ORBIS

#include "pad.h"

#include "remote_input.h"

#include "widgetpad.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"

#include <sdk_version.h>
#include <pad.h>
#include <ime_dialog.h>
#include <user_service.h>
#pragma comment(lib,"ScePad_stub_weak")

#if !__FINAL
#include "system/param.h"

namespace rage
{
	XPARAM(norumble);
	PARAM(nosixaxis,"Disable use of PS3 orientation sensors");
	PARAM(padConnected,"Always pretend a pad is connected (to avoid warning screens)");
	XPARAM(unattended);
} // namespace rage
#endif // !__FINAL

namespace rage
{
extern SceUserServiceUserId g_UserIds[SCE_USER_SERVICE_MAX_LOGIN_USERS];
}

using namespace rage;

static const uint8_t INVALID_TOUCH_ID = 255;
static const float MAX_X_TOUCH = 1919.0f; // 0...1919 according to docs.
static const float MAX_Y_TOUCH = 942.0f;  // According to docs: 0 - 753: JDX-1000x series controllers for the PlayStation®4     
										  // development tool, 0 - 942: retail controllers and the CUH-ZCT1J/CAP-ZCT1J/CAP-ZCT1U
										  // controllers for the PlayStation®4 development tool.

// Used to scale for touch pad cursor to be more sensitive.
static const float CURSOR_SENITIVIY_SCALER = 2.0f;

static int PadHandles[ioPad::MAX_PADS];


void ioPad::SetIsConnected(bool bConnected)
{
	m_IsConnected = bConnected;

	if (bConnected)
	{
		m_HasBeenConnected = true;
	}
}

// On ORBIS, the system manages input and output devices (including the controller) by linking them to a specific user. 
// The concept of "controllers owned by this user" is implemented on the API level.
void ioPad::BeginAll() 
{
	CompileTimeAssert(ioPad::MAX_PADS == SCE_USER_SERVICE_MAX_LOGIN_USERS);
	
	// Initialize the SCE pad system
	int ret = scePadInit();
	Assertf(ret == SCE_OK, "scePadInit failed, controllers will likely not work (error: %d)", ret);

	for (int i=0; i<MAX_PADS; i++) {
		sm_Pads[i].SetIsConnected(false);
		sm_Pads[i].m_HasSensors = true;		// PS4 only supports PS4 controllers.

		sm_Pads[i].m_AreButtonsAnalog = true;
		sm_Pads[i].m_PadIndex = i;

		sm_Pads[i].m_CursorPrevious.x = 0;
		sm_Pads[i].m_CursorPrevious.y = 0;
		sm_Pads[i].m_CursorPrevious.id = INVALID_TOUCH_ID;
		sm_Pads[i].m_CursorX = 0.0f;
		sm_Pads[i].m_CursorY = 0.0f;

		if (g_UserIds[i] > 0)
		{
			PadHandles[i] = scePadOpen(g_UserIds[i], 0, 0, NULL);		// this is really a handle...
			if (PadHandles[i] < 0)
				Errorf("scePadOpen - returned %x",PadHandles[i]);
			else
			{
				ScePadData data;
				ret = scePadReadState(PadHandles[i],&data);
				if (ret == SCE_OK)
				{
					sm_Pads[i].SetIsConnected(data.connected);
					
					ScePadControllerInformation info;
					if(scePadGetControllerInformation(PadHandles[i], &info) == SCE_OK && info.connectionType == SCE_PAD_CONNECTION_TYPE_REMOTE)
					{
						sm_Pads[i].m_IsRemotePlayPad = true;
					}
					else
					{
						sm_Pads[i].m_IsRemotePlayPad = false;
					}
				}
				else
				{
					Warningf("scePadReadState failed for handle: %x", PadHandles[i]);
					sm_Pads[i].SetIsConnected(false);
					sm_Pads[i].m_IsRemotePlayPad = false;
				}
			}
		}
		else
		{
			PadHandles[i] = -1;
		}

		for(int j=0; j<MAX_TOUCH_NUM; j++)
		{
			sm_Pads[i].m_previousTouchData[j].id = INVALID_TOUCH_ID;
			sm_Pads[i].m_previousTouchData[j].x  = 0;
			sm_Pads[i].m_previousTouchData[j].y  = 0;

			sm_Pads[i].m_currentTouchData[j].id = INVALID_TOUCH_ID;
			sm_Pads[i].m_currentTouchData[j].x  = 0;
			sm_Pads[i].m_currentTouchData[j].y  = 0;
		}
	}
}

void ioPad::EndAll() {
	for (int i=0; i<MAX_PADS; i++)
		scePadClose(PadHandles[i]);
}

void ioPad::Update(bool UNUSED_PARAM(ignoreInput)) {
	int i  = this - sm_Pads;

	// The history is available too if we want to use that.
	ScePadData data;
	int err = 0;
	if (PadHandles[i] < 0 || (err = scePadReadState(PadHandles[i],&data)) != SCE_OK || !data.connected) {
		if (err)
			Errorf("ioPad::Update - scePadReadState returned %x",err);

#if REMOTE_INPUT_SUPPORT
		if (!ioRemoteInput::IsPadConnected(i))
		{
#endif // REMOTE_INPUT_SUPPORT		
			ClearInputs();
			SetIsConnected(false NOTFINAL_ONLY(|| PARAM_unattended.Get() || PARAM_padConnected.Get()) REMOTE_INPUT_SUPPORT_ONLY(|| ioRemoteInput::IsEnabled()));
			return;
#if REMOTE_INPUT_SUPPORT
		}
#endif // REMOTE_INPUT_SUPPORT		
	}

#if REMOTE_INPUT_SUPPORT
	if (ioRemoteInput::IsPadConnected(i))
	{
		// Valid remote pad data replaces actual controller data.
		ioRemoteInput::Get().UpdatePad(i, data);
	}
#endif // REMOTE_INPUT_SUPPORT

#if SCE_ORBIS_SDK_VERSION >= 0x00920020u
	// If intercepted bit is set, discard controller information.
	sm_Intercepted = data.buttons & SCE_PAD_BUTTON_INTERCEPTED;
	if (sm_Intercepted)
	{
		ClearInputs();
		return;
	}
#endif

	m_LastButtons = m_Buttons;
	m_Buttons = 0;

	ScePadControllerInformation info;
	if(scePadGetControllerInformation(PadHandles[i], &info) == SCE_OK && info.connectionType == SCE_PAD_CONNECTION_TYPE_REMOTE)
	{
		m_IsRemotePlayPad = true;

		// When using remote play, we remap our own keyboard layout based on the remote controller layout 3.
		// See url:bugstar:1856022 and url:bugstar:1819339.
		if (data.buttons & SCE_PAD_BUTTON_L3) m_Buttons |= L1;
		if (data.buttons & SCE_PAD_BUTTON_R3) m_Buttons |= R1;
		if (data.buttons & SCE_PAD_BUTTON_OPTIONS) m_Buttons |= START;
		if (data.buttons & SCE_PAD_BUTTON_UP) m_Buttons |= LUP;
		if (data.buttons & SCE_PAD_BUTTON_RIGHT) m_Buttons |= LRIGHT;
		if (data.buttons & SCE_PAD_BUTTON_DOWN) m_Buttons |= LDOWN;
		if (data.buttons & SCE_PAD_BUTTON_LEFT) m_Buttons |= LLEFT;
		if (data.buttons & SCE_PAD_BUTTON_L2) m_Buttons |= L3;
		if (data.buttons & SCE_PAD_BUTTON_R2) m_Buttons |= R3;
		if (data.buttons & SCE_PAD_BUTTON_L1) m_Buttons |= L2;
		if (data.buttons & SCE_PAD_BUTTON_R1) m_Buttons |= R2;
		if (data.buttons & SCE_PAD_BUTTON_TRIANGLE) m_Buttons |= RUP;
		if (data.buttons & SCE_PAD_BUTTON_CIRCLE) m_Buttons |= RRIGHT;
		if (data.buttons & SCE_PAD_BUTTON_CROSS) m_Buttons |= RDOWN;
		if (data.buttons & SCE_PAD_BUTTON_SQUARE) m_Buttons |= RLEFT;
		if (data.buttons & SCE_PAD_BUTTON_TOUCH_PAD) m_Buttons |= TOUCH;


		m_AnalogButtons[L2_INDEX] = (m_Buttons & L2)? 255 : 0;
		m_AnalogButtons[R2_INDEX] = (m_Buttons & R2)? 255 : 0;
	}
	else
	{
		m_IsRemotePlayPad = false;

		if (data.buttons & SCE_PAD_BUTTON_L3) m_Buttons |= L3;
		if (data.buttons & SCE_PAD_BUTTON_R3) m_Buttons |= R3;
		if (data.buttons & SCE_PAD_BUTTON_OPTIONS) m_Buttons |= START;
		if (data.buttons & SCE_PAD_BUTTON_UP) m_Buttons |= LUP;
		if (data.buttons & SCE_PAD_BUTTON_RIGHT) m_Buttons |= LRIGHT;
		if (data.buttons & SCE_PAD_BUTTON_DOWN) m_Buttons |= LDOWN;
		if (data.buttons & SCE_PAD_BUTTON_LEFT) m_Buttons |= LLEFT;
		if (data.buttons & SCE_PAD_BUTTON_L2) m_Buttons |= L2;
		if (data.buttons & SCE_PAD_BUTTON_R2) m_Buttons |= R2;
		if (data.buttons & SCE_PAD_BUTTON_L1) m_Buttons |= L1;
		if (data.buttons & SCE_PAD_BUTTON_R1) m_Buttons |= R1;
		if (data.buttons & SCE_PAD_BUTTON_TRIANGLE) m_Buttons |= RUP;
		if (data.buttons & SCE_PAD_BUTTON_CIRCLE) m_Buttons |= RRIGHT;
		if (data.buttons & SCE_PAD_BUTTON_CROSS) m_Buttons |= RDOWN;
		if (data.buttons & SCE_PAD_BUTTON_SQUARE) m_Buttons |= RLEFT;
		if (data.buttons & SCE_PAD_BUTTON_TOUCH_PAD) m_Buttons |= TOUCH;

		m_AnalogButtons[L2_INDEX] = data.analogButtons.l2;
		m_AnalogButtons[R2_INDEX] = data.analogButtons.r2;
	}

	SetIsConnected(data.connected);

	// Update the Touch Pad
	m_NumTouches = data.touchData.touchNum;
	if(m_NumTouches > 0)
	{
		// The touch button cannot be held down on the vita (it supports quick presses only). To support the button
		// being held (needed for some controls), we do not support gestures. Gestures are treated as the button being
		// pressed.
		if(m_IsRemotePlayPad)
		{
			m_Buttons |= TOUCH;
		}
		else
		{
			// Set the first touch pad data
			m_currentTouchData[0].id = data.touchData.touch[0].id;
			m_currentTouchData[0].x = data.touchData.touch[0].x;
			m_currentTouchData[0].y = data.touchData.touch[0].y;

			// If present, also set the second touch pad data
			if(data.touchData.touchNum == 2)
			{
				m_currentTouchData[1].id = data.touchData.touch[1].id;
				m_currentTouchData[1].x  = data.touchData.touch[1].x ;
				m_currentTouchData[1].y  = data.touchData.touch[1].y ;
			}
			else
			{
				m_currentTouchData[1].id = INVALID_TOUCH_ID;
			}

			// Current Touch data [0] is actually the Previous Touch Data [1] due to a multi-touch release into a single touch.
			// We do this so this does not look like a new touch event
			if(m_previousTouchData[1].id == m_currentTouchData[0].id && m_currentTouchData[0].id != INVALID_TOUCH_ID)
			{
				m_currentTouchData[0].id = INVALID_TOUCH_ID;
				m_currentTouchData[1].id = data.touchData.touch[0].id;
				m_currentTouchData[1].x  = data.touchData.touch[0].x ;
				m_currentTouchData[1].y  = data.touchData.touch[0].y ;
			}


			// Update the cursor position.
			if(m_currentTouchData[0].id != INVALID_TOUCH_ID && m_CursorPrevious.id != INVALID_TOUCH_ID)
			{
				m_CursorX += (static_cast<float>(m_currentTouchData[0].x - m_CursorPrevious.x) / (MAX_X_TOUCH * CURSOR_SENITIVIY_SCALER));
				m_CursorY += (static_cast<float>(m_currentTouchData[0].y - m_CursorPrevious.y) / (MAX_Y_TOUCH * CURSOR_SENITIVIY_SCALER));
				m_CursorX = Clamp(m_CursorX, 0.0f, 1.0f);
				m_CursorY = Clamp(m_CursorY, 0.0f, 1.0f);
			}

			m_CursorPrevious = m_currentTouchData[0];

			// Update the previous touch data for the next frame
			m_previousTouchData[0].id = m_currentTouchData[0].id;
			m_previousTouchData[0].x  = m_currentTouchData[0].x ;
			m_previousTouchData[0].y  = m_currentTouchData[0].y ;

			m_previousTouchData[1].id = m_currentTouchData[1].id;
			m_previousTouchData[1].x  = m_currentTouchData[1].x ;
			m_previousTouchData[1].y  = m_currentTouchData[1].y ;
		}
	}
	else
	{
		m_currentTouchData[0].id = INVALID_TOUCH_ID;
		m_currentTouchData[1].id = INVALID_TOUCH_ID;
		m_CursorPrevious.id		 = INVALID_TOUCH_ID;
	}

	m_Axis[0] = data.leftStick.x;
	m_Axis[1] = data.leftStick.y;
	m_Axis[2] = data.rightStick.x;
	m_Axis[3] = data.rightStick.y;

	m_AnalogButtons[LRIGHT_INDEX] = (m_Buttons & LRIGHT)? 255 : 0;
	m_AnalogButtons[LLEFT_INDEX] = (m_Buttons & LLEFT)? 255 : 0;
	m_AnalogButtons[LUP_INDEX] = (m_Buttons & LUP)? 255 : 0;
	m_AnalogButtons[LDOWN_INDEX] = (m_Buttons & LDOWN)? 255 : 0;

	m_AnalogButtons[RRIGHT_INDEX] = (m_Buttons & RRIGHT)? 255 : 0;
	m_AnalogButtons[RLEFT_INDEX] = (m_Buttons & RLEFT)? 255 : 0;
	m_AnalogButtons[RUP_INDEX] = (m_Buttons & RUP)? 255 : 0;
	m_AnalogButtons[RDOWN_INDEX] = (m_Buttons & RDOWN)? 255 : 0;

	m_AnalogButtons[L1_INDEX] = (m_Buttons & L1)? 255 : 0;
	m_AnalogButtons[R1_INDEX] = (m_Buttons & R1)? 255 : 0;

	// L2_INDEX and R2_INDEX are done when setting m_Buttons above as we alter key locations in remote play.

	m_AnalogButtons[TOUCH_INDEX] = (m_Buttons & TOUCH)? 255 : 0;

	// Motion Sensor Acceleration (G-Units)
	m_Acceleration.x = data.acceleration.x;
	m_Acceleration.y = data.acceleration.y;
	m_Acceleration.z = data.acceleration.z;

	// Motion Sensor Angular Velocity
	m_AngularVelocity.x = data.angularVelocity.x;
	m_AngularVelocity.y = data.angularVelocity.y;
	m_AngularVelocity.z = data.angularVelocity.z;

	// Motion Sensor Orientation
	m_Orientation.x = data.orientation.x;
	m_Orientation.y = data.orientation.y;
	m_Orientation.z = data.orientation.z;
	m_Orientation.w = data.orientation.w;

#if __BANK
	for ( int i = 0; i < 4; ++i )
	{
		if ( m_bankAxis[i] != 0x80 )
		{
			m_Axis[i] = m_bankAxis[i];
		}
	}

	m_Buttons |= m_bankButtons;

	for ( int i = 0; i < NUMBUTTONS; ++i )
	{
		if ( m_bankAnalogButtons[i] != 0 )
		{
			m_AnalogButtons[i] = m_bankAnalogButtons[i];
			m_AreButtonsAnalog = true;
		}
	}

	ClearBankInputs();
#endif

	//UpdateDebug();

	m_HasRumble = true;
#if !__FINAL
	m_HasRumble &= !PARAM_norumble.Get();
#endif // !__FINAL

	if (m_ActuatorDataChanged && m_HasRumble) {
		m_ActuatorDataChanged = false;
		ScePadVibrationParam rumble = { m_ActuatorsEnabled? m_ActuatorData[HEAVY_MOTOR] / 257 : 0, m_ActuatorsEnabled? m_ActuatorData[LIGHT_MOTOR] / 257 : 0 };
		scePadSetVibration(PadHandles[i],&rumble);

		REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::SetPadVibration(i, m_ActuatorsEnabled ? m_ActuatorData[HEAVY_MOTOR] : 0, m_ActuatorsEnabled ? m_ActuatorData[LIGHT_MOTOR] : 0));
	}
}

void ioPad::UpdateAll(bool UNUSED_PARAM(ignoreInput), bool UNUSED_PARAM(checkForNewDevice)) {
	sm_HasChanged = false;

	for (int i=0; i<MAX_PADS; i++) {
		sm_Pads[i].Update();
	}
}

void ioPad::StopShakingNow()
{
	ScePadVibrationParam rumble = { 0, 0 };
	scePadSetVibration(PadHandles[m_PadIndex], &rumble);

	REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::SetPadVibration(m_PadIndex, 0, 0));
}

void ioPad::SetLightbar(u8 red, u8 green, u8 blue)
{
	ScePadLightBarParam param;
	param.r = red;
	param.g = green;
	param.b = blue;

	scePadSetLightBar(PadHandles[m_PadIndex], &param);

	REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::SetPadColor(m_PadIndex, red, green, blue));
}

void ioPad::ResetLightbar()
{
#if SCE_ORBIS_SDK_VERSION >= (0x00930020u)
	scePadResetLightBar(PadHandles[m_PadIndex]);
#endif

	REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::ResetPadColor(m_PadIndex));
}

// When tilt correction is enabled and a reset is carried out, the controller's orientation is reset to Identity<0,0,0,1>,
// and then a correction is made to the gravitational direction.
void ioPad::ResetOrientation()
{
	scePadResetOrientation(PadHandles[m_PadIndex]);
}

// This function enables/disables the motion sensor.
// When the motion sensor is enabled, valid values will be stored in orientation, acceleration, and angularVelocity members of ScePadData.
void ioPad::SetMotionSensorState(bool bEnabled)
{
	scePadSetMotionSensorState(PadHandles[m_PadIndex], bEnabled);
}

// Tilt correction corrects drifts in the pitch and roll directions based on the outputs of the acceleration sensor. When tilt correction is enabled, 
// the orientation angles in the pitch and roll directions are corrected with gravitational force obtained from the acceleration sensor and computed 
// as world coordinates. In addition to being able to remove cumulative error due to angular velocity data accumulation, orientation in world coordinates
// will be easy to use in augmented reality, for example. On the other hand, a slight latency occurs for the correction. In addition, due to the 
// characteristics of its algorithm, tilt correction cannot correct rotation in the yaw direction.
void ioPad::SetTiltCorrectionState(bool bEnabled)
{
	scePadSetTiltCorrectionState(PadHandles[m_PadIndex], bEnabled);
}

// The angular velocity deadband filter removes noise from angular velocity signals. When the angular velocity deadband filter is enabled, 
// gyro lifts - where noise outputs from the angular velocity sensor are accumulated to create a growing discrepancy between the orientation 
// computation outputs and the actual controller orientation - can be prevented. On the other hand, sensitivity against delicate or slow 
// controllermovements may deteriorate; for example, the operation to take aim in a first-person shooter game may become less precise.
void ioPad::SetAngularVelocityDeadbandState(bool bEnabled)
{
	scePadSetAngularVelocityDeadbandState(PadHandles[m_PadIndex], bEnabled);
}

// PURPOSE: On Orbis, controllers are owned by a user account. 
//	When a user logs in, their pad is initialized using their user service ID
//  When a user logs out, their pad is shutdown
void ioPad::HandleUserServiceStatusChange(int userId)
{
	SetIsConnected(true);
	int i  = this - sm_Pads;

	SceUserServiceUserId id = (SceUserServiceUserId)userId;

	// sign out
	if (id == SCE_USER_SERVICE_USER_ID_INVALID)
	{
		scePadClose(PadHandles[i]);
		PadHandles[i] = -1;
	}
	else // sign in
	{
		PadHandles[i] = scePadOpen(id, 0, 0, NULL);
	}
}

float ioPad::GetTouchXCenterNorm(int index) const
{
	const TouchPadData& current  = GetTouchData(index);

	if(current.id != INVALID_TOUCH_ID)
	{
		return (static_cast<float>(current.x) / (MAX_X_TOUCH / 2.0f)) - 1.0f;
	}
	else
	{
		return 0.0f;
	}
}

float ioPad::GetTouchYCenterNorm(int index) const
{
	const TouchPadData& current  = GetTouchData(index);

	if(current.id != INVALID_TOUCH_ID)
	{
		return (static_cast<float>(current.y) / (MAX_Y_TOUCH / 2.0f)) - 1.0f;
	}
	else
	{
		return 0.0f;
	}
}

float ioPad::GetTouchXNorm() const
{
	return m_CursorX;
}

float ioPad::GetTouchYNorm() const
{
	return m_CursorY;
}

#if __BANK
// Why the hell isn't this in pad_common?
void ioPad::AddPadWidgets()
{
	bkBank *pBank = BANKMGR.FindBank( "rage - Pad" );
	if ( pBank == NULL )
	{
		pBank = &BANKMGR.CreateBank( "rage - Pad" );
	}

	char name[16];
	sprintf( name, "Pad %d", m_PadIndex );

	if ( bkRemotePacket::IsConnectedToRag() )
	{
		ioWidgetPad* pWidget = rage_new ioWidgetPad( name, m_PadIndex, ioWidgetPad::PS3_PLATFORM );
		pBank->AddWidget( *pWidget );
	}
	else
	{
		pBank->PushGroup( name );
		{
			pBank->PushGroup( "Shoulder Buttons" );
			{
				pBank->AddButton( "L1", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L1 ) );
				pBank->AddButton( "R1", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R1 ) );
				pBank->AddButton( "L2", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L2 ) );
				pBank->AddButton( "R2", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R2 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Left Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData) LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LDOWN ) );
				pBank->AddButton( "L3", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Pad" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::ButtonPressedCB ), this,(CallbackData)LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LDOWN ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Right Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData)RRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "R3", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Buttons" );
			{
				pBank->AddButton( "X", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "O", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RRIGHT ) );
				pBank->AddButton( "[]", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "^", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RUP ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Other" );
			{
				pBank->AddButton( "Select", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)SELECT ) );
				pBank->AddButton( "Start", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)START ) );
			}
			pBank->PopGroup();
		}
		pBank->PopGroup();
	}
}

void ioPad::AnalogStickPressedCB( CallbackData data )
{
	int button = (int)(size_t) data;

	switch ( button )
	{
	case LUP:
		m_bankAxis[1] = 0x0;
		m_bankAnalogButtons[LUP_INDEX] = 0xff;
		break;
	case LLEFT:
		m_bankAxis[0] = 0x0;
		m_bankAnalogButtons[LLEFT_INDEX] = 0xff;
		break;
	case LRIGHT:
		m_bankAxis[0] = 0xff;
		m_bankAnalogButtons[LRIGHT_INDEX] = 0xff;
		break;
	case LDOWN:
		m_bankAxis[1] = 0xff;
		m_bankAnalogButtons[LDOWN_INDEX] = 0xff;
		break;
	case RUP:
		m_bankAxis[3] = 0x0;
		m_bankAnalogButtons[RUP_INDEX] = 0xff;
		break;
	case RLEFT:
		m_bankAxis[2] = 0x0;
		m_bankAnalogButtons[RLEFT_INDEX] = 0xff;
		break;
	case RRIGHT:
		m_bankAxis[2] = 0xff;
		m_bankAnalogButtons[RRIGHT_INDEX] = 0xff;
		break;
	case RDOWN:
		m_bankAxis[3] = 0xff;
		m_bankAnalogButtons[RDOWN_INDEX] = 0xff;
		break;
	}
}

void ioPad::ButtonPressedCB( CallbackData data )
{
	int button = (int)(size_t)data;

	m_bankButtons |= button;

	switch ( button )	
	{
	case L1:
		m_bankAnalogButtons[L1_INDEX] = 0xff;
		break;
	case R1:
		m_bankAnalogButtons[R1_INDEX] = 0xff;
		break;
	case L2:
		m_bankAnalogButtons[L2_INDEX] = 0xff;
		break;
	case R2:
		m_bankAnalogButtons[R2_INDEX] = 0xff;
		break;
	}
}
#endif

#endif	// __PPU
