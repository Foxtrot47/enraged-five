// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "virtualkeyboard.h"

#include "pad.h"

//rage
#include "math/amath.h"
#include "string/stringhash.h"
#include "string/unicode.h"
#if __XENON
#include "system/xtl.h"
#endif

#if __PS3
// Must take care not to stomp on the callbacks registered by other systems.
// If you do, there is no warning whatsoever, the other callback just doesn't
// get called anymore. At present, slots are assigned as follows:
//
//    0 = exit game handler
//    1 = installer
//    2 = on screen keyboard
static int oskSysutilCallbackSlot = 2;
#endif

#if RSG_ORBIS
#include "grcore/device.h"
#include <ime_dialog.h>
#include <user_service.h>
#include <sdk_version.h>
#include <libsysmodule.h>
#pragma comment(lib,"libSceImeDialog_stub_weak.a")
#endif

using namespace rage;

//////////////////////////////////////////////////////////////////////////
/// ioVirtualKeyboard 
//////////////////////////////////////////////////////////////////////////
ioVirtualKeyboard::ioVirtualKeyboard()
: m_ProcessKeyboard( false )
#if __STEAM_BUILD
, m_SteamCallbackHandle(this, &ioVirtualKeyboard::OnSteamInputDismissed)
, m_SteamKeyboardState( kKBState_INVALID )
#endif // __STEAM_BUILD
, m_State( kKBState_INVALID )
, m_SystemUIOpen( false )
#if RSG_DURANGO
, m_Cs()
, m_BufferedState( kKBState_INVALID )
#endif // RSG_DURANGO
{
	
#if __XENON
	// Done this way to avoid crazy include dependencies
	m_pXOverlapped = (void*)(rage_new XOVERLAPPED);
#endif

#if RSG_ORBIS
	if ( sceSysmoduleLoadModule(SCE_SYSMODULE_IME_DIALOG) != SCE_OK ) 
	{
		 Quitf("Cannot load IME Dialog (Virtual Keyboard)");
	}
#endif

	m_Result[ 0 ] = L'\0';
	m_InitialValue[ 0 ] = L'\0';

#if RSG_DURANGO
	m_BufferedResult [ 0 ] = L'\0';
	m_MaxLength = MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD;
#endif // RSG_DURANGO
}


ioVirtualKeyboard::~ioVirtualKeyboard()
{
#if __XENON
	delete m_pXOverlapped;
#endif

}


void ioVirtualKeyboard::Update()
{
	if( m_ProcessKeyboard )
	{
#if __XENON
		if( ((XOVERLAPPED*)m_pXOverlapped)->dwExtendedError == ERROR_SUCCESS )
		{
			m_State = kKBState_SUCCESS;
			m_ProcessKeyboard = false;
		}
		else if( ((XOVERLAPPED*)m_pXOverlapped)->dwExtendedError == ERROR_CANCELLED )
		{
			m_ProcessKeyboard = false;
			m_State =kKBState_CANCELLED;
		}

#elif __PS3

		cellSysutilCheckCallback();
		if( oskdialog_mode == MODE_CLOSE )
		{
			cellOskDialogUnloadAsync( &m_KeyboardOutputInfo );
			switch (m_KeyboardOutputInfo.result)
			{
			case CELL_OSKDIALOG_INPUT_FIELD_RESULT_OK:
				Displayf("osk input result: CELL_OSKDIALOG_INPUT_FIELD_RESULT_OK");
				m_State = kKBState_SUCCESS;
				break;
			case CELL_OSKDIALOG_INPUT_FIELD_RESULT_CANCELED:
				Displayf("osk input result: CELL_OSKDIALOG_INPUT_FIELD_RESULT_CANCELED");
				m_State = kKBState_CANCELLED;
				break;
			case CELL_OSKDIALOG_INPUT_FIELD_RESULT_ABORT:
				Displayf("osk input result: CELL_OSKDIALOG_INPUT_FIELD_RESULT_ABORT");
				m_State = kKBState_CANCELLED;
				break;
			case CELL_OSKDIALOG_INPUT_FIELD_RESULT_NO_INPUT_TEXT:
				Displayf("osk input result: CELL_OSKDIALOG_INPUT_FIELD_RESULT_NO_INPUT_TEXT");
				m_State = kKBState_CANCELLED;
				break;
			default:
				Displayf("osk input result: unknown enum value (%d)", m_KeyboardOutputInfo.result);
				m_State = kKBState_CANCELLED;
				break;
			}

			m_ProcessKeyboard = false;
		}
		else if( oskdialog_mode == MODE_EXIT )
		{
			m_State = kKBState_CANCELLED;
			m_ProcessKeyboard = false;
		}

		// done processing? don't need the callback anymore
		if (!m_ProcessKeyboard)
		{
			cellSysutilUnregisterCallback( oskSysutilCallbackSlot );
		}

#elif __WIN32PC
#if __STEAM_BUILD
		if(IsUsingSteamInputOverlay())
		{
			UpdateSteamInput();
		}
		else
#endif // __STEAM_BUILD
		{
			m_State = kKBState_SUCCESS;
			m_ProcessKeyboard = false;
		}
#elif RSG_ORBIS

		SceImeDialogStatus status = sceImeDialogGetStatus();
		if (status == SCE_IME_DIALOG_STATUS_FINISHED)
		{
			SceImeDialogResult endStatus;
			sysMemSet(&endStatus, 0, sizeof(endStatus));
			int err = sceImeDialogGetResult(&endStatus);
			if(err != 0)
			{
				Displayf("sceImeDialogGetResult returned %x", err);
			}
			
			if (endStatus.endstatus == SCE_IME_DIALOG_END_STATUS_OK)
			{
				m_State = kKBState_SUCCESS;
			}
			else if (endStatus.endstatus == SCE_IME_DIALOG_END_STATUS_USER_CANCELED ||
				endStatus.endstatus == SCE_IME_DIALOG_END_STATUS_ABORTED)
			{
				m_State = kKBState_CANCELLED;
			}
			m_ProcessKeyboard = false;
		}

		if (!m_ProcessKeyboard)
		{
			sceImeDialogTerm();
		}
#elif RSG_DURANGO
		sysCriticalSection lock(m_Cs);
		if(m_BufferedState != kKBState_PENDING)
		{
			m_State = m_BufferedState;
			safecpy(m_Result, m_BufferedResult);
			m_ProcessKeyboard = false;
		}
#else
		m_ProcessKeyboard = false;
#endif
	}

	if (!m_ProcessKeyboard)
	{
		m_SystemUIOpen = ioPad::IsIntercepted();
	}
}

#if !RSG_WINRT
void ioVirtualKeyboard::Cancel()
{
#if RSG_ORBIS
	sceImeDialogAbort();
#endif
}
#endif // !RSG_WINRT


void ioVirtualKeyboard::ReplaceUnsupportedSpaces(char16* str)
{
	for(int i = 0; str[i] != 0; ++i)
	{
		if(
			str[i] == 0x3000 || // Ideographic
			str[i] == 0x205F || // Medium Mathematical
			str[i] == 0x202F || // Narrow No-Break
			str[i] == 0x200B || // Zero Width
			str[i] == 0x200A || // Hair
			str[i] == 0x2009 || // Thin
			str[i] == 0x2008 || // Punctuation
			str[i] == 0x2007 || // Figure
			str[i] == 0x2006 || // Six-Per-Em
			str[i] == 0x2005 || // Four-Per-Em
			str[i] == 0x2004 || // Three-Per-Em
			str[i] == 0x2003 || // Em Space
			str[i] == 0x2002 || // En Space
			str[i] == 0x2001 || // Em Quad
			str[i] == 0x2000 || // En Quad
			str[i] == 0x1680 || // Ogham Mark
			str[i] == 0x00A0    // No Break
			)
		{
			str[i] = ' ';
		}
	}
}

char16* ioVirtualKeyboard::GetResult( void )
{ 
#if __XENON || __WIN32PC || RSG_DURANGO
	ReplaceUnsupportedSpaces(m_Result);
	return (char16*)m_Result; 
#elif __PS3
	return (char16*)m_KeyboardOutputInfo.pResultString;
#elif RSG_ORBIS
	ReplaceUnsupportedSpaces(m_Result);
	return (char16*)m_Result;
#else
	// Otherwise we do nothing
	return NULL;
#endif
}

#if __PS3
int ioVirtualKeyboard::oskdialog_mode = MODE_IDLE;
void ioVirtualKeyboard::sysutil_callback( uint64_t status, uint64_t param, void* UNUSED_PARAM( userdata ) )
{
	(void)param;
	switch(status)
	{
	case CELL_SYSUTIL_OSKDIALOG_LOADED:
		break;

	case CELL_SYSUTIL_OSKDIALOG_FINISHED:
		oskdialog_mode = MODE_CLOSE;
		break;

	case CELL_SYSUTIL_OSKDIALOG_UNLOADED:
		break;

	case CELL_SYSUTIL_REQUEST_EXITGAME:
		oskdialog_mode = MODE_EXIT;
		break;

	case CELL_SYSUTIL_DRAWING_BEGIN:
	case CELL_SYSUTIL_DRAWING_END:
		break;
	case CELL_SYSUTIL_OSKDIALOG_INPUT_ENTERED:
	case CELL_SYSUTIL_OSKDIALOG_INPUT_CANCELED:
		cellOskDialogAbort();
		break;
	case CELL_SYSUTIL_OSKDIALOG_INPUT_DEVICE_CHANGED:
		if(param == CELL_OSKDIALOG_INPUT_DEVICE_KEYBOARD ){
			/*E If the input device becomes the hardware keyboard, */
			/*  stop receiving input from the on-screen keyboard dialog */
			cellOskDialogSetDeviceMask( CELL_OSKDIALOG_DEVICE_MASK_PAD );
		}
		break;
	case CELL_SYSUTIL_OSKDIALOG_DISPLAY_CHANGED:
		break;
	default:
		break;
	}
}

bool ioVirtualKeyboard::StartInput( const Params& params )
{
	eTextType eType = params.m_KeyboardType;
	int maxResultLength = Min(MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD, params.m_MaxLength);

	// Input field information
	m_KeyboardInputInfo.message = (uint16_t*)params.m_Title;
	// Initial text
	m_KeyboardInputInfo.init_text = (uint16_t*)m_InitialValue;

	// Length limitation for input text
	m_KeyboardInputInfo.limit_length = maxResultLength - 1;

	m_KeyboardOutputInfo.result = CELL_OSKDIALOG_INPUT_FIELD_RESULT_OK; // Result on-screen keyboard dialog termination 
	m_KeyboardOutputInfo.numCharsResultString = maxResultLength;	// Specify number of characters for returned text
	m_KeyboardOutputInfo.pResultString = (uint16_t*)(&m_Result);   // Buffer storing returned text

	// Set key layout to enable for the on-screen keyboard dialog
	cellOskDialogSetKeyLayoutOption( CELL_OSKDIALOG_FULLKEY_PANEL );

	// Set standard position */
	int32_t LayoutMode = CELL_OSKDIALOG_LAYOUTMODE_X_ALIGN_CENTER | CELL_OSKDIALOG_LAYOUTMODE_Y_ALIGN_CENTER;
	cellOskDialogSetLayoutMode( LayoutMode );

	// On-screen keyboard dialog utility activation parameters

	// Panel to display first
	m_KeyboardDialogParam.firstViewPanel = CELL_OSKDIALOG_PANELMODE_ALPHABET;

	// Select panels to be used using flags (alphabet input, hiragana input, etc.)
	if( eType == kTextType_PASSWORD )
		m_KeyboardDialogParam.allowOskPanelFlg = CELL_OSKDIALOG_PANELMODE_PASSWORD;
	else if ( ( eType == kTextType_ALPHABET ) || ( eType == kTextType_GAMERTAG ) )
	{
		m_KeyboardDialogParam.allowOskPanelFlg = CELL_OSKDIALOG_PANELMODE_ALPHABET;
		switch(params.m_AlphabetType)
		{
		case kAlphabetType_BASIC_ENGLISH:
			// Do nothing, core alphabet has all the characters we'll allow.
			break;

		case kAlphabetType_ENGLISH:
			m_KeyboardDialogParam.allowOskPanelFlg |= CELL_OSKDIALOG_PANELMODE_ENGLISH;
			break;
		case kAlphabetType_CYRILLIC:
			m_KeyboardDialogParam.allowOskPanelFlg |= CELL_OSKDIALOG_PANELMODE_RUSSIAN;
			m_KeyboardDialogParam.firstViewPanel = CELL_OSKDIALOG_PANELMODE_RUSSIAN;
			break;
		case kAlphabetType_CHINESE:
			m_KeyboardDialogParam.allowOskPanelFlg |= CELL_OSKDIALOG_PANELMODE_TRADITIONAL_CHINESE;
			m_KeyboardDialogParam.firstViewPanel = CELL_OSKDIALOG_PANELMODE_TRADITIONAL_CHINESE;
			break;
		case kAlphabetType_CHINESE_SIMPLIFIED:
			m_KeyboardDialogParam.allowOskPanelFlg |= CELL_OSKDIALOG_PANELMODE_SIMPLIFIED_CHINESE;
			m_KeyboardDialogParam.firstViewPanel = CELL_OSKDIALOG_PANELMODE_SIMPLIFIED_CHINESE;
			break;
		case kAlphabetType_KOREAN:
			m_KeyboardDialogParam.allowOskPanelFlg |= CELL_OSKDIALOG_PANELMODE_KOREAN;
			m_KeyboardDialogParam.firstViewPanel = CELL_OSKDIALOG_PANELMODE_KOREAN;
			break;
		case kAlphabetType_JAPANESE:
			m_KeyboardDialogParam.allowOskPanelFlg |= CELL_OSKDIALOG_PANELMODE_JAPANESE; // needs additional 2MB memory container
			m_KeyboardDialogParam.firstViewPanel = CELL_OSKDIALOG_PANELMODE_JAPANESE;
			break;
		case kAlphabetType_POLISH:
			m_KeyboardDialogParam.allowOskPanelFlg |= CELL_OSKDIALOG_PANELMODE_POLISH;
			m_KeyboardDialogParam.firstViewPanel = CELL_OSKDIALOG_PANELMODE_POLISH;
			break;
		default:
			m_KeyboardDialogParam.allowOskPanelFlg |= CELL_OSKDIALOG_PANELMODE_ENGLISH;
			break;
		}
	}
	else if ( eType == kTextType_NUMERIC )
	{
		m_KeyboardDialogParam.allowOskPanelFlg = CELL_OSKDIALOG_PANELMODE_NUMERAL;
	}
	else if ( eType == kTextType_EMAIL )
	{
		m_KeyboardDialogParam.allowOskPanelFlg = CELL_OSKDIALOG_PANELMODE_ALPHABET;	//	not CELL_OSKDIALOG_PANELMODE_ENGLISH
	}
	else
	{
		m_KeyboardDialogParam.allowOskPanelFlg = CELL_OSKDIALOG_PANELMODE_ALPHABET	|
			CELL_OSKDIALOG_PANELMODE_NUMERAL_FULL_WIDTH						|
			CELL_OSKDIALOG_PANELMODE_NUMERAL								|
			CELL_OSKDIALOG_PANELMODE_ENGLISH;
	}

	// Initial display position of the on-screen keyboard dialog (x, y)
	CellOskDialogPoint pos;
	pos.x = 0.0;
	pos.y = 0.0;
	m_KeyboardDialogParam.controlPoint = pos; 

	// Prohibited operation flag(s) (ex. CELL_OSKDIALOG_NO_SPACE)
	m_KeyboardDialogParam.prohibitFlgs = 0;
	if( eType == kTextType_EMAIL ||
		eType == kTextType_ALPHABET ||
		eType == kTextType_NUMERIC || 
		eType == kTextType_GAMERTAG)
		m_KeyboardDialogParam.prohibitFlgs |= CELL_OSKDIALOG_NO_RETURN; //space might be allowed according to http://www.remote.org/jochen/mail/info/chars.html

	cellSysutilRegisterCallback( oskSysutilCallbackSlot, sysutil_callback, NULL );

	// Add language support
	cellOskDialogAddSupportLanguage(m_KeyboardDialogParam.allowOskPanelFlg);

	oskdialog_mode = MODE_IDLE;

	int returnedErrorCode = cellOskDialogLoadAsync( SYS_MEMORY_CONTAINER_ID_INVALID, &m_KeyboardDialogParam, &m_KeyboardInputInfo);
	Displayf("cellOskDialogLoadAsync returned %x", returnedErrorCode);
	bool success = (returnedErrorCode == 0);

	if (success)
	{
		m_SystemUIOpen = true; // stays true until (1) we are done with the keyboard and (2) the system UI is no longer open
	}
	return success;
}
#elif __XENON
bool ioVirtualKeyboard::StartInput( const Params& params )
{
	eTextType eType = params.m_KeyboardType;
	int maxResultLength = Min(MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD, params.m_MaxLength);

	((XOVERLAPPED*)m_pXOverlapped)->dwExtendedError = ERROR_IO_PENDING;
	DWORD nKeyboardType = VKBD_DEFAULT;
	if( eType == kTextType_EMAIL )
		nKeyboardType = VKBD_LATIN_EMAIL;
	else if( eType == kTextType_PASSWORD )
		nKeyboardType = VKBD_LATIN_PASSWORD;
	else if( eType == kTextType_NUMERIC )
		nKeyboardType = VKBD_LATIN_NUMERIC;
	else if (eType == kTextType_GAMERTAG )
		nKeyboardType = VKBD_LATIN_GAMERTAG;
	else if( eType == kTextType_ALPHABET )
	{
		switch(params.m_AlphabetType)
		{
		case kAlphabetType_BASIC_ENGLISH:
			nKeyboardType = VKBD_LATIN_ALPHABET;
			break;
		case kAlphabetType_ENGLISH:
			nKeyboardType = VKBD_LATIN_FULL;
			break;
		case kAlphabetType_CYRILLIC:
			nKeyboardType = VKBD_RUSSIAN_FULL;
			break;
		case kAlphabetType_CHINESE:
			nKeyboardType = VKBD_TCH_FULL;
			break;
		case kAlphabetType_CHINESE_SIMPLIFIED:
			nKeyboardType = VKBD_SCH_FULL;
			break;
		case kAlphabetType_KOREAN:
			nKeyboardType = VKBD_KOREAN_FULL;
			break;
		case kAlphabetType_JAPANESE:
			nKeyboardType = VKBD_JAPANESE_FULL;
			break;
		case kAlphabetType_POLISH:
			nKeyboardType = VKBD_LATIN_EXTENDED;
			break;
		default:
			nKeyboardType = VKBD_LATIN_FULL;
			break;
		}
	}

	((XOVERLAPPED*)m_pXOverlapped)->hEvent = NULL;
	((XOVERLAPPED*)m_pXOverlapped)->pCompletionRoutine = NULL;

	DWORD hResult = XShowKeyboardUI( 
		params.m_PlayerIndex,
		nKeyboardType, 
		reinterpret_cast<LPCWSTR>(m_InitialValue), 
		reinterpret_cast<LPCWSTR>(params.m_Title), 
		reinterpret_cast<LPCWSTR>(params.m_Description), 
		reinterpret_cast<LPWSTR>(m_Result), 
		maxResultLength, 
		((XOVERLAPPED*)m_pXOverlapped)
		);

	if (hResult == ERROR_IO_PENDING)
	{
		m_SystemUIOpen = true; // stays true until (1) we are done with the keyboard and (2) the system UI is no longer open
		return true;
	}
	return false;
}
#elif __WIN32PC
bool ioVirtualKeyboard::StartInput( const Params& STEAMBUILD_ONLY(params) )
{
#if __STEAM_BUILD
	StartSteamInput(params);
	return IsUsingSteamInputOverlay();
#else
	m_SystemUIOpen = false;
	return false;
#endif // __STEAM_BUILD
}
#elif RSG_ORBIS
bool ioVirtualKeyboard::StartInput(const Params& params)
{
	int ret;
	uint32_t dialogSizeWidth;
	uint32_t dialogSizeHeight;

	SceImeDialogParam dialogParam;
	memset(&dialogParam, 0x00, sizeof(dialogParam));

	// initalize parameter of ime dialog
	sceImeDialogParamInit(&dialogParam);
	sceImeDialogGetPanelSize(&dialogParam,&dialogSizeWidth, &dialogSizeHeight);

	dialogParam.userId = params.m_UserId;
	dialogParam.option = 0;
	dialogParam.supportedLanguages = 0;
	dialogParam.type = SCE_IME_TYPE_DEFAULT;//BASIC_LATIN;

	// We need to populate the result with any supplied initial value before sending to SCE APIs
	safecpy( m_Result, m_InitialValue );
	dialogParam.inputTextBuffer = (wchar_t*)m_Result;

	dialogParam.title = (const wchar_t*)params.m_Title;
	dialogParam.maxTextLength = params.m_MaxLength;

	dialogParam.posx = (GRCDEVICE.GetWidth() / 2) - (dialogSizeWidth / 2);
	dialogParam.posy = (GRCDEVICE.GetHeight() / 2) - (dialogSizeHeight / 2);
	dialogParam.horizontalAlignment = SCE_IME_HALIGN_LEFT;
	dialogParam.verticalAlignment = SCE_IME_VALIGN_TOP;

	eTextType eType = params.m_KeyboardType;

	if( eType == kTextType_EMAIL )
	{
		dialogParam.type = SCE_IME_TYPE_MAIL;
		dialogParam.option = 0;
	}
	else if( eType == kTextType_PASSWORD )
	{
		// need to set type to Basic Latin to get password option to work
		dialogParam.type = SCE_IME_TYPE_BASIC_LATIN;
		dialogParam.option |= SCE_IME_OPTION_PASSWORD;
		dialogParam.option |= SCE_IME_OPTION_NO_AUTO_CAPITALIZATION;
	}
	else if( eType == kTextType_NUMERIC )
	{
		dialogParam.type = SCE_IME_TYPE_NUMBER;
		dialogParam.option |= SCE_IME_OPTION_NO_AUTO_CAPITALIZATION;
	}
	else if (eType == kTextType_GAMERTAG )
	{
		dialogParam.type = SCE_IME_TYPE_DEFAULT;
		dialogParam.option |= SCE_IME_OPTION_NO_AUTO_CAPITALIZATION;
	}
	else if( eType == kTextType_ALPHABET )
	{
		// dialogParam.supportedLanguages = 0 means all languages supported
	}

	ret = sceImeDialogInit(&dialogParam,NULL);

	if (ret != SCE_OK)
	{
		Displayf("sceImeDialogInit : 0x%08x\n", ret);
		return false;
	}

	return true;
}
// Durango uses WinRT code so they are seperated into virtualkeyboard_durango.cpp
#elif !RSG_DURANGO
bool ioVirtualKeyboard::StartInput( const Params& )
{
	return false;
}
#endif

bool ioVirtualKeyboard::Show( const Params& params )
{
	Assert(params.m_MaxLength >= 0 && params.m_MaxLength <= MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD);
	if (params.m_MaxLength < 0 || params.m_MaxLength > MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD)
	{
		return false;
	}

	if (Verifyf(m_State != kKBState_PENDING && !m_ProcessKeyboard, "Already displaying a keyboard!"))
	{
		m_Result[0] = '\0';

		m_InitialValue[0] = '\0';

		if( params.m_InitialValue )
		{
			wcsncpy( m_InitialValue, params.m_InitialValue, MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD);
			m_InitialValue[MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD-1] = '\0';
		}

		if(StartInput(params))
		{
			m_State = kKBState_PENDING;
			m_ProcessKeyboard = true;
		}
		else
		{
			m_State = kKBState_FAILED;
		}

		return m_ProcessKeyboard;
	}

	return false;
}

#if __STEAM_BUILD
void ioVirtualKeyboard::OnSteamInputDismissed( GamepadTextInputDismissed_t *pParam )
{
	if(pParam != NULL && pParam->m_bSubmitted && m_SteamKeyboardState != kKBState_CANCELLED)
	{
		m_SteamKeyboardState = kKBState_SUCCESS;
	}
	else
	{
		m_SteamKeyboardState = kKBState_CANCELLED;
	}
}

void ioVirtualKeyboard::StartSteamInput(const Params& params)
{
	m_SteamKeyboardState = ioVirtualKeyboard::kKBState_INVALID;

	ISteamUtils* pSteamUtils = SteamUtils();
	if(pSteamUtils != NULL)
	{
		EGamepadTextInputMode inputMode;
		if(params.m_KeyboardType == kTextType_PASSWORD)
		{
			inputMode = k_EGamepadTextInputModePassword;
		}
		else
		{
			inputMode = k_EGamepadTextInputModeNormal;
		}

		USES_CONVERSION;
		if(pSteamUtils->ShowGamepadTextInput(inputMode, 
											k_EGamepadTextInputLineModeSingleLine, 
											params.m_Description ? WIDE_TO_UTF8(params.m_Description) : "", 
											params.m_MaxLength,
											params.m_InitialValue ? WIDE_TO_UTF8(params.m_InitialValue) : ""))
		{
			Displayf("Showing virtual keyboard");
			m_SteamKeyboardState = kKBState_PENDING;
			m_SystemUIOpen = true;
		}
	}
}

bool ioVirtualKeyboard::IsUsingSteamInputOverlay() const
{
	return m_SteamKeyboardState != kKBState_INVALID;
}

void ioVirtualKeyboard::UpdateSteamInput()
{
	if(IsUsingSteamInputOverlay())
	{
		ISteamUtils* pSteamUtils = SteamUtils();
		if(pSteamUtils != NULL)
		{
			switch(m_SteamKeyboardState)
			{
			case kKBState_PENDING:
				m_State = kKBState_PENDING;
				break;

			case kKBState_SUCCESS:
				{
					char tmpResult[MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD];
					if(pSteamUtils->GetEnteredGamepadTextInput(tmpResult, MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD))
					{				
						Utf8ToWide(m_Result, tmpResult, MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD);

						m_ProcessKeyboard = false;
						m_State = kKBState_SUCCESS;
					}
					else
					{
						m_State = kKBState_FAILED;
					}

					m_SteamKeyboardState = kKBState_INVALID;
				}
				break;

			case kKBState_CANCELLED:
				m_State = kKBState_CANCELLED;
				m_SteamKeyboardState = kKBState_INVALID;
				break;

			default:
				m_State = kKBState_FAILED;
				// We use kkBState_INVALID as we are no longer using steam big picture text input.
				m_SteamKeyboardState = kKBState_INVALID;
				break;
			}
		}
		else
		{
		}
	}
}
#endif // __STEAM_BUILD



