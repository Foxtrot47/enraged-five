// 
// input/remote_input_durango.winrt.cpp 
// 
// Copyright (C) 1999-2020 Rockstar Games.  All Rights Reserved. 
// 

#include "remote_input_durango.winrt.h"

#if (REMOTE_INPUT_SUPPORT && RSG_DURANGO)

#include "remote_input.h"

#include <xdk.h>

using namespace rage;

void ioRemoteInput_WinRT::UpdatePad(int index, Windows::Xbox::Input::RawGamepadReading& reading)
{
	ioRemotePad& rp = ioRemoteInput::Get().m_remotePads[index];

	auto mapFloat = [](s16 s) { return s < 0 ? s / 32768.0f : s / 32767.0f; };
	reading.LeftThumbstickX = mapFloat(rp.leftX);
	reading.LeftThumbstickY = mapFloat(rp.leftY);
	reading.RightThumbstickX = mapFloat(rp.rightX);
	reading.RightThumbstickY = mapFloat(rp.rightY);
	reading.LeftTrigger = rp.leftTrigger / 255.0f;
	reading.RightTrigger = rp.rightTrigger / 255.0f;
	unsigned buttons = 0;
	if (rp.buttons & ioRemotePad::A) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::A;
	if (rp.buttons & ioRemotePad::B) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::B;
	if (rp.buttons & ioRemotePad::X) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::X;
	if (rp.buttons & ioRemotePad::Y) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::Y;
	if (rp.buttons & ioRemotePad::LEFT_SHOULDER) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::LeftShoulder;
	if (rp.buttons & ioRemotePad::RIGHT_SHOULDER) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::RightShoulder;
	if (rp.buttons & ioRemotePad::LEFT_THUMB) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::LeftThumbstick;
	if (rp.buttons & ioRemotePad::RIGHT_THUMB) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::RightThumbstick;
	if (rp.buttons & ioRemotePad::DPAD_DOWN) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::DPadDown;
	if (rp.buttons & ioRemotePad::DPAD_RIGHT) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::DPadRight;
	if (rp.buttons & ioRemotePad::DPAD_LEFT) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::DPadLeft;
	if (rp.buttons & ioRemotePad::DPAD_UP) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::DPadUp;
	if (rp.buttons & ioRemotePad::START) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::Menu;
	if (rp.buttons & ioRemotePad::BACK) buttons |= (unsigned)Windows::Xbox::Input::GamepadButtons::View;
	reading.Buttons = (Windows::Xbox::Input::GamepadButtons) buttons;
}

#endif // REMOTE_INPUT_SUPPORT && RSG_DURANGO
