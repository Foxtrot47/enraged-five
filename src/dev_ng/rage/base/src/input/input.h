//
// input/input.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_INPUT_H
#define INPUT_INPUT_H

/* This file implements a convenience class to unify the built-in guaranteed input devices */

#define HAVE_WHEEL		(0)
#define HAVE_MOUSE		(__WIN32PC)
#define HAVE_KEYBOARD	(__WIN32PC)
#define HAVE_JOYSTICK	(__WIN32PC)
#define HAVE_FFWHEEL	(__XENON||__PPU)
#define HAVE_HEADTRACKING (RSG_PC || RSG_ORBIS)

#include "joystick.h"
#include "keyboard.h"
#include "keys.h"
#include "mouse.h"
#include "pad.h"
#include "headtracking.h"

namespace rage {

typedef void (*ioInputUpdater)(void);

/*
	PURPOSE
		Unify startup, shutdown, and update several different subsystems
		in one place for convenience.
<FLAG Component>
*/
class ioInput {
public:

	// PURPOSE:
	// On by default, clear this to disable joystick support in case higher level
	// code wants control (originally necessary to avoid a lockup bug of a particular
	// driver revision of a particular USB steering wheel when more than one DirectInput
	// device is created on it).
	static bool GetUseJoystick();

	// <COMBINE GetUseJoystick>
	static void SetUseJoystick(bool b);

	// On PC, this is on by default.  On consoles, it is off by default due to potential 
	// conflicts with ioChat unless we're connected to rag.
	static bool GetUseKeyboard();

	// <COMBINE GetUseKeyboard>
	static void SetUseKeyboard(bool b);

	// On PC, this is on by default.  On consoles, it is dependent on if we're
	// connected to rag.
	static bool GetUseMouse();

	// <COMBINE GetUseMouse>
	static void SetUseMouse(bool b);

	// PURPOSE: Initialize class.  Call this after you've called grmSetup::BeginGfx.
	// PARAMS: inWindow - True if running in a window, else false
	//		   exclusiveMouse - True if app wants exclusive access to mouse, else false.  Defaults to false.
	static void Begin(bool inWindo, bool exclusiveMouse = false);

	// PURPOSE: Shuts down class.  Call this just before grmSetup::EndGfx.
	static void End(); 

	// PURPOSE: Update all input state.  Call this just after grmSetup::BeginUpdate.
	static void Update(bool ignorePadInput = false, bool ignoreMouseInput = false, bool ignoreKeyboardInput = false, bool checkForNewDevice = true); 

	// PURPOSE:	Updates the input locale for this thread.
	static void UpdateLocale();

	// PURPOSE: Tells us if Begin() has ever been called before End()
	// RETURNS: true or false
	static bool HasBegun();

	// PURPOSE: Returns the value of inWindow that was passed to Begin()
	static bool IsInWindow();

	// PURPOSE: Register new update callback
	// NOTES: Registering the same callback a second time will be ignored
	static void RegisterUpdater(ioInputUpdater updater);

#if RSG_PC
	// PURPOSE:	recaptures lost devices.
	static void RecaptureLostDevices();

	// PURPOSE:	Wait for the signal that the message pump has been updated.
	// NOTES:	On Windows, the device is updated in the message pump. If the input code is updated at the same time (on a different thread)
	//			then frames can be lost (race condition). This waits for a signal for when the message pump has been updated to ensure this
	//			doesn't happen. The message pump is also updated on multiple threads and its only the thread that owns the window that we
	//			are interested in.
	static void WaitForMessagePumpUpdateSignal();

	// PURPOSE:	Signal that the message pump has been updated.
	static void SignalMessagePumpUpdate();
#endif // RSG_PC

#if __BANK
	static bool *GetIsGhettoKeyboardEnabledPtr()		{ return &sm_IsGhettoKeyboardEnabled; }
#endif // __BANK

private:
#if __BANK
	// PURPOSE: Handle the "use keyboard as controller" hack
	static void UpdateGhettoPad();
#endif // __BANK

	static bool sm_hasBegun;
	static bool sm_inWindow;
	static bool sm_UseJoystick;
	static bool sm_UseMouse;
	static bool sm_UseKeyboard;

#if __BANK
	static bool sm_IsGhettoKeyboardEnabled;
#endif // __BANK

	static ioInputUpdater sm_Updaters[];

private:
#if RSG_PC
	// PURPOSE:	The device to try and re-capture if needed this frame.
	enum DeviceCaptureState
	{
		CAPTURE_MOUSE,
		CAPTURE_KEYBOARD,
		CAPTURE_JOYSTICK,
	};

	static DeviceCaptureState sm_DeviceCaptureState;

	// PURPOSE:	Signal to indicate that we should send a signal when the message pump has been updated.
	static sysIpcEvent sm_MessagePumpUpdatedEvent;
#endif // RSG_PC
};

inline bool ioInput::GetUseKeyboard()
{
	return sm_UseKeyboard;
}

inline void ioInput::SetUseKeyboard(bool b)
{
	sm_UseKeyboard=b;
}

inline bool ioInput::GetUseMouse()
{
	return sm_UseMouse;
}

inline void ioInput::SetUseMouse(bool b)
{
	sm_UseMouse=b;
}

inline bool ioInput::HasBegun()
{
	return sm_hasBegun;
}

inline bool ioInput::IsInWindow()
{
	return sm_inWindow;
}

// All access to ioInput class should go through this variable
extern ioInput INPUT;

// PURPOSE: Adds dead zone to a particular 1D input
// PARAMS: originalValue - Value to add dead zone to
//		deadZone - dead zone value
// RETURNS: Value with dead zone added (anything within deadZone of zero
//	is snapped to zero, and the rest of the range is renormalized accordingly)
extern float ioAddDeadZone(float originalValue,float deadZone = 0.1f);

// PURPOSE: Adjusts a 2D input for a round dead zone.
//			Square dead zones don't accurately model the analog sticks since they don't treat all
//			angles equally (especially the 45 degree angles).
// PARAMS:
//		x -  in: [-1..1] out: [-1..1] (inputs may be slightly outside the range).
//		y -  in: [-1..1] out: [-1..1] (inputs may be slightly outside the range).
//		deadZone - dead zone value [0..1], e.g. 0.2f.
//		saturate - clips all return values at 1.0.
// RETURNS: Magnitude [0..1].  Cartesian values are returned in the x and y parameters.
extern float ioAddRoundDeadZone(float& x, float& y, float deadZone = 0.2f, bool saturate = true);

// PURPOSE: Adjusts a 3D input for a spherical dead zone (e.g. something like the PS3 three-axis tilt controller).
//			Cube dead zones don't accurately model the analog sticks since they don't treat all
//			angles equally (especially the 45 degree angles).
// PARAMS:
//		x -  in: [-1..1] out: [-1..1] (inputs may be slightly outside the range).
//		y -  in: [-1..1] out: [-1..1] (inputs may be slightly outside the range).
//		z -  in: [-1..1] out: [-1..1] (inputs may be slightly outside the range).
//		deadZone - dead zone value [0..1], e.g. 0.2f.
//		saturate - clips all return values at 1.0.
// RETURNS: Magnitude [0..1].  Cartesian values are returned in the x, y, and z parameters.
extern float ioAddSphereDeadZone(float& x, float& y, float& z, float deadZone, bool saturate = true);

}	// namespace rage

#endif
