//
// input/remote_input_defs.h
//
// Copyright (C) 1999-2020 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_REMOTE_INPUT_DEFS_H
#define INPUT_REMOTE_INPUT_DEFS_H

#define REMOTE_INPUT_SUPPORT !__FINAL

#define REMOTE_INPUT_PLATFORMS (RSG_ORBIS || RSG_DURANGO || RSG_PC)

#if REMOTE_INPUT_SUPPORT
#define REMOTE_INPUT_SUPPORT_ONLY(x) x
#define REMOTE_INPUT_SUPPORT_SWITCH(x, y) x
#else
#define REMOTE_INPUT_SUPPORT_ONLY(x)
#define REMOTE_INPUT_SUPPORT_SWITCH(x, y) y
#endif // REMOTE_INPUT_SUPPORT

#if REMOTE_INPUT_SUPPORT

#define REMOTE_INPUT_BASE_VERSION	1
#define REMOTE_INPUT_VERSION_2		2

#define REMOTE_INPUT_CURRENT_VERSION REMOTE_INPUT_VERSION_2

#define REMOTE_OUTPUT_BASE_VERSION		1
#define REMOTE_OUTPUT_CURRENT_VERSION	REMOTE_OUTPUT_BASE_VERSION

#define REMOTE_INPUT_MAX_PADS 4
#define REMOTE_INPUT_MAX_TOUCH_INPUT 2

#define REMOTE_INPUT_BUFFER_SIZE 128

// Input Version Structure Info:
//
// Version [1] Structures:
// ioRemoteInputHeaderBase
// ioRemotePadBase
// ioRemoteMouseBase
// ioRemoteKeyboardBase
//
// Version [2] Structures:
// ioRemoteInputHeaderBase
// ioRemotePad_V2
// ioRemoteMouseBase
// ioRemoteKeyboardBase

//////////////////////////////////////////////////////////////////////////

// Output Version Structure Info:
//
// Version [1] Structures:
// ioRemoteOutputHeaderBase
// ioRemotePad_OutputBase

namespace rage
{
	enum ioRemoteType
	{
		IRT_None		= 0x0,
		IRT_Pad			= 0x1,
		IRT_Mouse		= 0x2,
		IRT_Keyboard	= 0x3,
	};

#pragma pack(push, 1)
// Input //////////////////////////////////////////////////////////////////////////
	typedef struct ioRemoteInputHeaderBase
	{
		u16					version;
		u16					headerSize;
		u8					type;
		u16					typeDataSize;
		u8					reserved;
	} ioRemoteInputHeader;
	
	struct ioRemotePadBase
	{
		// These are XINPUT_GAMEPAD_... values without that prefix:
		enum PAD_BUTTONS
		{
			DPAD_UP = 1, DPAD_DOWN = 2, DPAD_LEFT = 4, DPAD_RIGHT = 8, START = 16, BACK = 32, LEFT_THUMB = 64, RIGHT_THUMB = 128,
			LEFT_SHOULDER = 256, RIGHT_SHOULDER = 512, A = 4096, B = 8192, X = 16384, Y = 32768
		};
		u32		index : 2, remoteUpdateCount : 30;
		u16		buttons;
		u8		leftTrigger, rightTrigger;
		s16		leftX, leftY, rightX, rightY;
	};

	typedef struct ioRemotePad_TouchInputBase
	{
		u16		x, y;
		u8		id;
		bool	hasData;
	} ioRemotePad_TouchInput;

	typedef struct ioRemotePad_V2 : ioRemotePadBase
	{
		ioRemotePad_TouchInput sce_TouchInput[REMOTE_INPUT_MAX_TOUCH_INPUT];
	} ioRemotePad;

	typedef struct ioRemoteMouseBase
	{
		enum MOUSE_BUTTONS
		{
			MOUSE_LEFT = 0x01,
			MOUSE_RIGHT = 0x02,
			MOUSE_MIDDLE = 0x04,
			MOUSE_EXTRABTN1 = 0x08,
			MOUSE_EXTRABTN2 = 0x10,
			MOUSE_EXTRABTN3 = 0x20,
			MOUSE_EXTRABTN4 = 0x40,
			MOUSE_EXTRABTN5 = 0x80,
		};

		u32 remoteUpdateCount;
		u32 x, y;
		s32 wheelDelta;
		u32 clientScreenWidth, clientScreenHeight;
		u16 buttons;
	} ioRemoteMouse;

	typedef struct ioRemoteKeyboardBase
	{
		enum KEYBOARD_EVENTS
		{
			KEY_DOWN = 1,
			KEY_UP = 2,
		};
		
		u32 remoteUpdateCount;
		u32 event;
		s32 key;
		s32 character;
	} ioRemoteKeyboard;

// Output //////////////////////////////////////////////////////////////////////////

	typedef struct ioRemoteOutputHeaderBase
	{
		u16					version;
		u16					headerSize;
		u8					type;
		u16					typeDataSize;
		u8					reserved;
	} ioRemoteOutputHeader;
	
	struct ioRemotePad_Vibration
	{
		// Based on XInput
		u16 leftMotor;
		u16 rightMotor;
	};

	struct ioRemotePad_SceColor
	{
		u8		red, green, blue;
		bool	update;
		bool	reset;
		u8		reserved;
	};

	typedef struct ioRemotePad_OutputBase
	{
		u32 index : 2, remoteUpdateCount : 30;
		ioRemotePad_Vibration	vibration;
		ioRemotePad_SceColor	color;
		
	} ioRemotePad_Output;
#pragma pack(pop)

}	// namespace rage

#endif // REMOTE_INPUT_SUPPORT 

#endif // INPUT_REMOTE_INPUT_DEFS_H