// 
// input/pad_common.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "pad.h"

#include "widgetpad.h"
#include "system/timer.h"

using namespace rage;

#if !__FINAL
#include "system/param.h"

namespace rage
{
	PARAM(norumble, "[RAGE] disable rumble");
	PARAM(ignoreFocus, "[RAGE] pad does not require focus to function");
	PARAM(disableDebugPadButtons, "[RAGE] disable the debug buttons on a gamepad to emulate final.");
} // namespace rage
#endif // !__FINAL

ioPad ioPad::sm_Pads[MAX_PADS];
bool ioPad::sm_Intercepted = false;
bool ioPad::sm_HasChanged = false;

#if __WIN32PC && !__FINAL
bool ioPad::sm_IgnoreControllerInput = false;
#endif

ioPad::ioPad() {
	m_Buttons = 0;
	ClearInputs();
	m_IsConnected = true;		// for now

#if RSG_ORBIS
	m_HasBeenConnected = false;
#endif	//	RSG_ORBIS

	m_ActuatorsEnabled = true;
	m_ActuatorDataChanged = true;

	for (u32 i = 0; i < MAX_ACTUATORS; ++i)
	{
		m_ActuatorData[i] = 0;
	}
	m_SubType = UNKNOWN;
	m_HasForceFeedback = false;
	m_AreButtonsAnalog = false;
	m_HasSensors = false;
	m_HasRumble = USE_XINPUT;	// other platforms detect after first update
	m_AreTriggersAnalog = USE_XINPUT || __PS3 || RSG_ORBIS || RSG_DURANGO;

#if __BANK
	ClearBankInputs();
#endif
}

void ioPad::ClearInputs() {
	m_LastButtons = m_Buttons;
	m_Buttons = 0;
	m_Axis[0] = m_Axis[1] = m_Axis[2] = m_Axis[3] = 0x80;
	for (int i=0; i<NUMBUTTONS; i++)
		m_AnalogButtons[i] = 0;
	for (int i=0; i<SENSOR_NUMAXES; i++)
		m_Sensors[i] = 512;	// center point
}

#if __BANK
void ioPad::ClearBankInputs()
{
	for ( int i = 0; i < 4; ++i )
	{
		m_bankAxis[i] = 0x80;
	}

	for ( int i = 0; i < NUMBUTTONS; ++i )
	{
		m_bankAnalogButtons[i] = 0;
	}

	m_bankButtons = 0;
}
#endif

bool ioPad::GetActuatorsActivated() const {
	return m_ActuatorsEnabled;
}

void ioPad::SetActuatorsActivation(bool b) {
	if(m_ActuatorsEnabled != b)
	{
		m_ActuatorsEnabled = b;
		m_ActuatorDataChanged = true;
	}
}

void ioPad::SetActuatorValue(int iActuator,float fVal) {
	if ( fVal < 0.0f )
		fVal = 0.0f;
	else if ( fVal > 1.0f )
		fVal = 1.0f;

	u16 iValue = (u16) (fVal * 65535.0f);

	Assert(iActuator >= 0 && iActuator < MAX_ACTUATORS);
	if(iValue != 0 || m_ActuatorData[iActuator] != (u16)iValue)
	{
		m_ActuatorData[iActuator] = (u16)iValue;
		m_ActuatorDataChanged = true;
	}
}

#if __BANK
void ioPad::AddWidgets()
{
	ioWidgetPad::AddHandlers();

	// just add pad 0 for now
	sm_Pads[0].AddPadWidgets();
}
#endif

#if __DEBUGBUTTONS

unsigned ioPad::m_DebugButton = 
#if RSG_ORBIS
	ioPad::TOUCH | //orbis uses trackpad for debug too.
#else
	ioPad::SELECT | //use view input on xbox one.
#endif
	ioPad::START;

unsigned ioPad::m_CurrentDebugButton = 0;
unsigned ioPad::m_DebugButtonStartTime = 0;
const unsigned ioPad::DEBUG_BUTTONS_TIME_LIMIT = 250;


void ioPad::UpdateDebugAll()
{
	for (int i=0; i<MAX_PADS; i++) {
		if(sm_Pads[i].m_IsConnected)
			sm_Pads[i].UpdateDebug();
	}
}

void ioPad::UpdateDebug() {
	if(PARAM_disableDebugPadButtons.Get())
	{
		return;
	}

	m_LastDebugButtons = m_DebugButtons;
	m_DebugButtons = 0;
	switch (m_DebugState) {
	case 0:	// idle state
		if (!m_LastButtons && (GetButtons() & m_DebugButton) ) {
//			Displayf("Entering State 1");
			m_DebugState = 1;
			m_CurrentDebugButton = GetButtons() & m_DebugButton;

			if(m_CurrentDebugButton != ioPad::START)
			{
				m_DebugButtonStartTime = sysTimer::GetSystemMsTime();
			}

			SetButtons(0);
		}
		break;
	case 1:	// pressed start, have not released it yet
		if(m_DebugButtonStartTime != 0 && (m_DebugButtonStartTime + DEBUG_BUTTONS_TIME_LIMIT) < sysTimer::GetSystemMsTime())
		{
			m_DebugState = 0;
			m_DebugButtonStartTime = 0;
		}
		else if (GetButtons() & ~m_CurrentDebugButton) {	// pressed something else while holding start
//			Displayf("Entering State 2");
			m_DebugButtons = GetButtons();
			SetButtons(0);
			m_DebugState = 2;
		}
		else if (GetButtons() == 0) {	// pressed and released start by itself
//			Displayf("Sending Start ping, returning to State 0");
			SetButtons(m_CurrentDebugButton);	// set start bit for a frame (will reset next frame since start is no longer pressed)
			m_DebugState = 0;
		}
		else	// make sure start doesn't make it down...
			SetButtons(0);
		m_LastButtons = 0;
		break;
	case 2:	// pressed start and now pressed at least one other button
		m_DebugButtons = GetButtons();
		if (GetButtons() == 0) {
//			Displayf("All buttons released, returning to State 0");
			m_DebugState = 0;
		}
		else {
			SetButtons(0);
		}
		m_LastButtons = 0;
		break;
	}
}

#endif		// } __DEBUGBUTTONS

