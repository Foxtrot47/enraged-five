//
// input/mouse.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "input/mouse.h"

#include "bank/packet.h"
#include "input/input_channel.h"
#include "input/remote_input.h"
#include "profile/telemetry.h"
#include "system/bootmgr.h"
#include "system/param.h"
#include "system/wndproc.h"
#include "system/timer.h"
#include "math/amath.h"

using namespace rage;

bool ioMouse::m_UseWindow,ioMouse::m_HasWheel;
#if __WIN32PC
#include "system/xtl.h"
#include "grcore/device.h"

#define USE_DX_MOUSE_DATA_BUFFER	1
#define MOUSE_BUFFER_SIZE			16		// not used if USE_DX_MOUSE_DATA_BUFFER is zero

XPARAM(ragUseOwnWindow);
namespace rage
{
	XPARAM(setHwndMain);
}

rage::u32 rage::ioMouse::m_DoubleClickTimers[];

rage::u32 rage::ioMouse::m_DoubleClickTimeLimit = 0;
rage::u32 rage::ioMouse::m_DoubleClickedButtons = 500;
rage::u32 rage::ioMouse::m_ActivateTimer        = 0;

// These two variables do nothing if MOUSE_SMOOTHING is disabled.
int ioMouse::sm_MouseSmoothingFrames = (int)MOUSE_HISTORY_SIZE;	// disable smoothing by setting to 1 or less
float ioMouse::sm_MouseSmoothingThreshold  = 4.0f;				// disable threshold by setting to negative number
float ioMouse::sm_MouseSmoothingBlendLimit = 8.0f;

bool ioMouse::m_Exclusive = false;
bool ioMouse::m_IsCurrentlyExclusive = false;

bool ioMouse::m_ControllerDrivenMovement = false;

bool ioMouse::sm_Visible = false;

// the actual windows position of the cursor.
int ioMouse::m_wX, ioMouse::m_wY;

// The raw relative input.
int ioMouse::m_wdX[2], ioMouse::m_wdY[2];
int ioMouse::m_UpdateIndex = 0;
sysCriticalSectionToken ioMouse::s_RawInput;

rage::ioMouse::MouseType rage::ioMouse::m_MouseType = rage::ioMouse::RAW_MOUSE;

#if __FINAL
// only final mode defaults to not absolute (i.e. relative) mode.
bool ioMouse::m_AbsoluteOnly = false;
#else
bool ioMouse::m_AbsoluteOnly = true;
#endif // __FINAL

bool ioMouse::m_HasFocus = false;
bool ioMouse::sm_DiDeviceLost = false;
bool ioMouse::m_ClientAreaHasFocus = false;

// the window handle
extern HWND g_hwndMain;
int ioMouse::m_RefCount = 0;

#if RSG_ASSERT
bool ioMouse::sm_HasAssertFired = false;
u32  ioMouse::sm_FocusAfterAssertTime = 0u;
#endif // RSG_ASSERT

sysCriticalSectionToken ioMouse::s_CS;

#if MOUSE_SMOOTHING
s32		ioMouse::sm_MouseHistory[MOUSE_AXIS_TOTAL][MOUSE_HISTORY_SIZE];
float	ioMouse::sm_MouseWeightScale = 0.60f;								// [0,1)
s32		ioMouse::sm_CurrentMouseInput = 0;
#endif // MOUSE_SMOOTHING

#endif // __WIN32PC

int ioMouse::m_X, ioMouse::m_Y;
#if RSG_PC
float ioMouse::m_dX, ioMouse::m_dY;
int   ioMouse::m_dZ;
#else
int ioMouse::m_dX, ioMouse::m_dY, ioMouse::m_dZ;
#endif

#if RSG_EXTRA_MOUSE_SUPPORT
// Currently 1/30
const float ioMouse::AVERAGE_MOUSE_WEIGHT = 0.03333333333f;
float ioMouse::m_nX, ioMouse::m_nY;
#endif // RSG_EXTRA_MOUSE_SUPPORT

int ioMouse_dZ_Accum;
unsigned ioMouse::m_Buttons, ioMouse::m_LastButtons, ioMouse::m_WindowButtons;

bool ioMouse::m_EnableGlobalButtons = false;

#if RSG_ORBIS
#include <mouse.h>
#include <libsysmodule.h>
#include "grcore/device.h"

#pragma comment(lib,"libSceMouse_stub_weak.a")

// Have to keep track of mouse button state as mouseRead packets are only sent if mouse state changed.
// We need to be able to report no input, if it is being ignored, while still tracking the actual state.
unsigned ioMouse::m_InternalButtons;

static s32 sMouseHandle = -1;

void ioMouse::Begin(bool, bool) {
	m_UseWindow = false;
	m_HasWheel=true; // for now, assume we have a mouse wheel

	s32 result = sceSysmoduleLoadModule(SCE_SYSMODULE_MOUSE);
	if (result == SCE_OK )
	{
		result = sceMouseInit();
		if (result == SCE_OK)
		{
			SceUserServiceUserId initialUserId;
			result = sceUserServiceGetInitialUser(&initialUserId);
			if (result == SCE_OK)
			{
				sMouseHandle = sceMouseOpen(	initialUserId,
												SCE_MOUSE_PORT_TYPE_STANDARD,
												0,
												NULL	);
				if (sMouseHandle < 0)
				{
					Errorf("Orbis failed open mouse port.  Error = %d", result);
				}
			}
			else
			{
				Errorf("Orbis failed to get initial user id.  Error = %d", result);
			}
		}
		else
		{
			Errorf("Orbis Mouse Init failed.  Error = %d", result);
		}
	}
	else
	{
		Errorf("Orbis failed to load Mouse PRX module.  Error = %d", result);
	}

	m_dX = 0;
	m_dY = 0;
	m_dZ = 0;
	EXTRA_MOUSE_SUPPORT_ONLY(ClearAverageMovementBuffer();)
	ioMouse_dZ_Accum = 0;

	m_InternalButtons = 0;
	m_LastButtons = 0;
	m_Buttons = 0;
}

void ioMouse::End() {
	if (sMouseHandle >= 0)
	{
		s32 result = sceMouseClose(sMouseHandle);
		if (result == SCE_OK)
		{
			sMouseHandle = -1;
		}
		else
		{
			Errorf("Orbis failed to close mouse port.  Error = %d", result);
		}
	}
}

void ioMouse::Update(bool ignoreInput) {
	m_dX = 0;
	m_dY = 0;
	m_dZ = 0;
	ioMouse_dZ_Accum = 0;

	if (ignoreInput)
	{
		// Clear rest of mouse input
		m_LastButtons = 0;
		m_Buttons = 0;
		return;
	}

	// Looks like we only get a mouse data packet when mouse state has changed, so have to track button state from last packet received.
	// TODO: need to track it internally, so we can ignore mouse input without losing the button state.
	bool bButtonsWritten = false;
	m_LastButtons = m_Buttons;
	m_Buttons = 0;

	const s32 numMouseData = Min(16, SCE_MOUSE_MAX_DATA_NUM);
	SceMouseData aoMouseData[numMouseData];

	s32 numPackets =  sceMouseRead(	sMouseHandle,
									aoMouseData,
									numMouseData );

	// TODO: sort by timestamp?
	if (numPackets == 0)
	{
		m_Buttons = m_InternalButtons;
	}
	if (numPackets > 0)
	{
		const u32 c_uButtonMask = (u32)SCE_MOUSE_BUTTON_INTERCEPTED - 1;
		for (s32 packet = 0; packet < numPackets; packet++)
		{
			// Check if mouse data is valid and not being used by system.
			if ((aoMouseData[packet].buttons & SCE_MOUSE_BUTTON_INTERCEPTED) == 0 &&
				 aoMouseData[packet].connected)
			{
				m_dX += (int)aoMouseData[packet].xAxis;
				m_dY += (int)aoMouseData[packet].yAxis;
				m_dZ += (int)aoMouseData[packet].wheel;

				if (!bButtonsWritten)
				{
					m_InternalButtons = 0;				// Clear m_InternalButtons the first time.
					bButtonsWritten = false;
				}
				// Note: currently (v930.030) only 3 buttons are supported.
				m_InternalButtons |= (unsigned)(aoMouseData[packet].buttons & c_uButtonMask);
			}
		}

		m_Buttons = m_InternalButtons;

		EXTRA_MOUSE_SUPPORT_ONLY(UpdateAverageMovement();)

		m_X += m_dX;
		m_Y += m_dY;

		// Clamp to screen extents.
		m_X = Max(0, Min(GRCDEVICE.GetWidth(),  m_X));
		m_Y = Max(0, Min(GRCDEVICE.GetHeight(), m_Y));
	}
	else if (numPackets < 0)
	{
		Errorf("Orbis failed to read mouse.  Error = %d", numPackets);
	}

#if __BANK
	// If there is no PS4 mouse button input, then copy the Rag button input.
	if(bkRemotePacket::IsConnected() && m_Buttons == 0 && m_Buttons != m_WindowButtons)
	{
		m_Buttons = m_WindowButtons;
	}
#endif // __BANK

#if REMOTE_INPUT_SUPPORT
	if (ioRemoteInput::IsMouseConnectAndHasPendingInput() == true)
	{
		ioRemoteInput::Get().UpdateMouseButtons();
	}
#endif // REMOTE_INPUT_SUPPORT
}

#elif IS_CONSOLE

void ioMouse::Begin(bool, bool) {
	m_HasWheel=true; // for now, assume we have a mouse wheel
	EXTRA_MOUSE_SUPPORT_ONLY(ClearAverageMovementBuffer();)
}

void ioMouse::End() {
}

void ioMouse::Update(bool UNUSED_PARAM(ignoreInput)) {
	m_dZ = ioMouse_dZ_Accum;
	ioMouse_dZ_Accum = 0;
	m_LastButtons = m_Buttons;
	m_Buttons = m_WindowButtons;
	EXTRA_MOUSE_SUPPORT_ONLY(UpdateAverageMovement();)

#if REMOTE_INPUT_SUPPORT
	if (ioRemoteInput::IsMouseConnectAndHasPendingInput() == true)
	{
		ioRemoteInput::Get().UpdateMouseButtons();
	}
#endif // REMOTE_INPUT_SUPPORT
}

#elif RSG_PC

#include "winpriv.h"

static LPDIRECTINPUTDEVICEW s_mouseDev;
static LPDIRECTINPUTW s_mouseDI;
static DIMOUSESTATE2 s_mouseState;
static RECT s_ClientAreaRect;
static bool s_WindowUpdatedLastFrame = false;
static bool s_WindowUpdatedThisFrame = false;
static bool s_IsWindowed = false;
static bool s_IgnoreInput = false;

void ioMouse::Begin(bool inWin, bool isExclusive /*= false*/) {
	m_UseWindow = inWin;
	m_Exclusive = isExclusive;
	m_MouseType = RAW_MOUSE;
	sm_Visible = false;

	sysMemSet(&s_ClientAreaRect, 0, sizeof(s_ClientAreaRect));
	if(::GetClientRect(g_hwndMain, &s_ClientAreaRect))
	{
		::ClientToScreen(g_hwndMain, (LPPOINT)&s_ClientAreaRect.left);
		::ClientToScreen(g_hwndMain, (LPPOINT)&s_ClientAreaRect.right);
	}

	for(int i = 0; i < MOUSE_NUM_BUTTONS; ++i)
	{
		m_DoubleClickTimers[i] = 0;
	}

	// User's set double click time.
	m_DoubleClickTimeLimit = GetDoubleClickTime();
	m_ActivateTimer = 0;

	m_DoubleClickedButtons = 0;
	EXTRA_MOUSE_SUPPORT_ONLY(ClearAverageMovementBuffer());

#if MOUSE_SMOOTHING
	sm_CurrentMouseInput = 0;
	for (s32 axis = 0; axis < MOUSE_AXIS_TOTAL; axis++)
	{
		for (s32 j = 0; j < MOUSE_HISTORY_SIZE; j++)
		{
			sm_MouseHistory[axis][j] = 0;
		}
	}
#endif // MOUSE_SMOOTHING

	m_HasWheel = true;

	m_UpdateIndex = 0;

	// this doesn't seem necessary, and removes a cross-dependency on gfx.
	// except that it causes massive choppiness when debugging under W2K!
	HWND hwndTop=g_hwndMain;
	while (GetParent(hwndTop))
		hwndTop=GetParent(hwndTop);

	SetMouseType(m_MouseType);
}

void ioMouse::SetMouseType(MouseType type)
{
	m_MouseType = type;

	// Do some cleaning up.
	End();

#if REMOTE_INPUT_SUPPORT
	if (ioRemoteInput::IsMouseConnectAndHasPendingInput() == true)
	{
		m_MouseType = ioMouse::WINDOWS_MOUSE;
		return;
	}
#endif // REMOTE_INPUT_SUPPORT

#if __BANK
	// When running inside the rag window, only use old windows messages as raw input doesn't work and DirectInput only
	// works when not in exclusive mode.
	if((bkRemotePacket::IsConnected() || PARAM_setHwndMain.Get()) && !PARAM_ragUseOwnWindow.Get())
	{
		m_MouseType = ioMouse::WINDOWS_MOUSE;
		return;
	}
#endif // __BANK

	if(m_MouseType == RAW_MOUSE)
	{
		RAWINPUTDEVICE rid;

		rid.usUsagePage = 0x01; 
		rid.usUsage = 0x02; 
		rid.dwFlags = 0;
		rid.hwndTarget = 0;


		// If this fails then we will revert back to the old legacy windows messages.
		if(!inputVerifyf(RegisterRawInputDevices(&rid, 1, sizeof(rid)) != FALSE, "failed to use raw input, reverting to legacy windows messages!"))
		{
			m_MouseType = WINDOWS_MOUSE;
		}
	}
	else if(m_MouseType == DIRECTINPUT_MOUSE)
	{
		diInit();

		(s_mouseDI = lpDI)->AddRef();

		ditry(s_mouseDI->CreateDevice(GUID_SysMouse,&s_mouseDev,NULL));

		// this doesn't seem necessary, and removes a cross-dependency on gfx.
		// except that it causes massive choppiness when debugging under W2K!
		HWND hwndTop=g_hwndMain;
		while (GetParent(hwndTop))
			hwndTop=GetParent(hwndTop);

#	if __WIN32PC
		ditry(s_mouseDev->SetCooperativeLevel(hwndTop,DISCL_FOREGROUND | (m_Exclusive ? DISCL_EXCLUSIVE : DISCL_NONEXCLUSIVE)));
		m_IsCurrentlyExclusive = m_Exclusive;
#	else
		ditry(s_mouseDev->SetCooperativeLevel(hwndTop,DISCL_FOREGROUND | DISCL_NONEXCLUSIVE));
#	endif // __WIN32PC

		ditry(s_mouseDev->SetDataFormat(&c_dfDIMouse2));

		DIPROPDWORD   dipdw;
		const int MOUSE_BUFF_SIZE = 32;
		dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
		dipdw.diph.dwHeaderSize = sizeof(dipdw.diph);
		dipdw.diph.dwHow        = DIPH_DEVICE;
		dipdw.diph.dwObj        = 0;
		dipdw.dwData            = MOUSE_BUFF_SIZE;

		ditry(s_mouseDev->SetProperty(DIPROP_BUFFERSIZE,  &dipdw.diph));

		// check for mouse wheel:
		DIDEVCAPS  DIMouseCaps; 
 
		DIMouseCaps.dwSize = sizeof(DIDEVCAPS); 
		ditry(s_mouseDev->GetCapabilities(&DIMouseCaps)); 
		m_HasWheel = ((DIMouseCaps.dwFlags & DIDC_ATTACHED) && (DIMouseCaps.dwAxes > 2)); 
 
		s_mouseDev->Acquire();
	}
}

ioMouse::MouseType ioMouse::GetMouseType()
{
	return m_MouseType;
}

bool ioMouse::IsLeftRightButtonSwapped()
{
	return GetSystemMetrics(SM_SWAPBUTTON) != 0;
}

void ioMouse::End() {
	if (s_mouseDev) {
		s_mouseDev->Unacquire();
		s_mouseDev = 0;
	}
	if (s_mouseDI) {
		s_mouseDI->Release();
		s_mouseDI = 0;
	}
}

bool ioMouse::HasFocus()
{
	return !GRCDEVICE.GetLostFocusForAudio();
}

void ioMouse::SetCursorClip()
{
	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - SetCursorClip()"));

	const bool clip = m_ClientAreaHasFocus && !GetAbsoluteOnly();
	static bool isClippedToWindow = false;

	if(clip && (!isClippedToWindow || s_WindowUpdatedLastFrame || s_WindowUpdatedThisFrame))
	{
		::ClipCursor(&s_ClientAreaRect);
		isClippedToWindow = true;
	}
	else if(!clip && isClippedToWindow)
	{
		::ClipCursor(NULL);
		isClippedToWindow = false;
	}
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
}



#if RSG_ASSERT
void ioMouse::OnAssert()
{
	FlushMouse();
	sm_HasAssertFired = true;
	sm_FocusAfterAssertTime = 0u;
	UpdateCursorVisibility();
}
#endif // RSG_ASSERT

void ioMouse::Update(bool ignoreInput) {

	s_IgnoreInput = ignoreInput;

	// The number of ms until we re clamp the cursor.
	const static u32 RECLAMP_INTERVAL = 5000u;
	static u32 lastClamped = 0u;
	const u32 now = sysTimer::GetSystemMsTime();

	s_WindowUpdatedThisFrame = GRCDEVICE.IsInSizeMove() ||
		g_WindowWidth != GRCDEVICE.GetWidth() ||
		g_WindowHeight != GRCDEVICE.GetHeight() ||
		s_IsWindowed != GRCDEVICE.IsWindowed() ||
		(lastClamped + RECLAMP_INTERVAL) >= now;

	const bool windowUpdated = s_WindowUpdatedThisFrame || s_WindowUpdatedLastFrame;
	s_WindowUpdatedLastFrame = s_WindowUpdatedThisFrame;
	if(windowUpdated)
	{
		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - Get Client Rect"));
		if(::GetClientRect(g_hwndMain, &s_ClientAreaRect))
		{
			// Convert the client rect to screen coords (as outlined at http://support.microsoft.com/kb/11570).
			::ClientToScreen(g_hwndMain, (LPPOINT)&s_ClientAreaRect.left);
			::ClientToScreen(g_hwndMain, (LPPOINT)&s_ClientAreaRect.right);
		}

		s_IsWindowed = GRCDEVICE.IsWindowed();
		lastClamped = now;
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	}

	g_WindowWidth = GRCDEVICE.GetWidth();
	g_WindowHeight = GRCDEVICE.GetHeight();
	g_InvWindowWidth = 1.0f / float(GRCDEVICE.GetWidth());
	g_InvWindowHeight = 1.0f / float(GRCDEVICE.GetHeight());

	if(ignoreInput ASSERT_ONLY(|| sm_HasAssertFired))
	{
#if RSG_ASSERT
		if(sm_FocusAfterAssertTime != 0u)
		{
			if((sysTimer::GetSystemMsTime() - sm_FocusAfterAssertTime) > 500u)
			{
				sm_HasAssertFired = false;
				m_ActivateTimer = 0;
			}
		}
		else if(sm_HasAssertFired && HasFocus())
		{
			// We re-use the active timer as it is not being used here and it is timeing how long we have been active.
			sm_FocusAfterAssertTime = sysTimer::GetSystemMsTime();
		}
#endif // RSG_ASSERT

		FlushMouse();
		return;
	}

	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - HasFocus()"));
	m_HasFocus = HasFocus();
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	if(m_HasFocus == false)
	{
		m_ClientAreaHasFocus = false;
	}

	SetCursorClip();


	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - UpdateCursorVisibility()"));
	UpdateCursorVisibility();
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));


	POINT cursor;
	sysMemSet(&cursor, 0, sizeof(cursor));
	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - Get Cursor Position"));
	BOOL hasCursor = GetCursorPos(&cursor);
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));

#if REMOTE_INPUT_SUPPORT
	if (ioRemoteInput::IsMouseConnectAndHasPendingInput() == true)
	{
		m_LastButtons = m_Buttons;
		ioRemoteInput::Get().UpdateMouseButtons();
		return;
	}
#endif // REMOTE_INPUT_SUPPORT
	
	int width  = s_ClientAreaRect.right  - s_ClientAreaRect.left;
	int height = s_ClientAreaRect.bottom - s_ClientAreaRect.top;

	// If we do not have focus or we have had focus for smaller than the double click timer (a small time amount that is appropriate for the user) then ignore buttons.
	if(!m_HasFocus || (sysTimer::GetSystemMsTime() - m_ActivateTimer) < m_DoubleClickTimeLimit)
	{
		FlushMouse();

		if(!m_HasFocus)
		{
			m_ActivateTimer = sysTimer::GetSystemMsTime();
		}
		return;
	}

	if (!s_mouseDev) {
		m_dZ = ioMouse_dZ_Accum;
		ioMouse_dZ_Accum = 0;
		m_LastButtons = m_Buttons;
		m_Buttons = m_WindowButtons;

		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - s_RawInput.Lock()"));
		s_RawInput.Lock();
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
		// Clear previous values.
		m_wdX[m_UpdateIndex] = 0;
		m_wdY[m_UpdateIndex] = 0;
		// Swap buffer.
		m_UpdateIndex += 1;
		m_UpdateIndex &= 0x1;
		// Read new values.
		int   wdXCurrent = m_wdX[m_UpdateIndex];
		int   wdYCurrent = m_wdY[m_UpdateIndex];
		s_RawInput.Unlock();

		float fwdX = (float)wdXCurrent;
		float fwdY = (float)wdYCurrent;
	#if MOUSE_SMOOTHING
		if( sm_MouseSmoothingFrames > 1 && sm_MouseWeightScale > SMALL_FLOAT )
		{
			WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - mouse smoothing"));
			const float c_fCurrentInputX = fwdX;
			const float c_fCurrentInputY = fwdY;
			sm_MouseHistory[MOUSE_X_AXIS][sm_CurrentMouseInput] = wdXCurrent;
			sm_MouseHistory[MOUSE_Y_AXIS][sm_CurrentMouseInput] = wdYCurrent;

			// Calculate average value.
			float fWeightTotal = 0.0f;
			float fCurrentWeight = 1.0f;
			float fTotalWeightedHorz = 0.0f;
			float fTotalWeightedVert = 0.0f;
			s32 sCurrentIndex = sm_CurrentMouseInput;
			for (s32 i = 0; i < sm_MouseSmoothingFrames; i++)
			{
				fTotalWeightedHorz += (float)sm_MouseHistory[MOUSE_X_AXIS][sCurrentIndex] * fCurrentWeight;
				fTotalWeightedVert += (float)sm_MouseHistory[MOUSE_Y_AXIS][sCurrentIndex] * fCurrentWeight;
				fWeightTotal       += fCurrentWeight;
				fCurrentWeight     *= sm_MouseWeightScale;			// Scale used for next iteration.

				// Skip insignificant contributions.
				if (fCurrentWeight < 0.01f)
					break;

				sCurrentIndex--;
				if (sCurrentIndex < 0)
					sCurrentIndex = sm_MouseSmoothingFrames - 1;
			}

			float wdXnew = fTotalWeightedHorz/fWeightTotal;
			float wdYnew = fTotalWeightedVert/fWeightTotal;
			float wdXnewAbs = Abs(wdXnew);
			float wdYnewAbs = Abs(wdYnew);

			if( wdXnew != 0.0f && (wdXnewAbs <= sm_MouseSmoothingBlendLimit || sm_MouseSmoothingThreshold < 0.0f) )
			{
				fwdX = wdXnew;
				if(sm_MouseSmoothingThreshold > 0.0f && wdXnewAbs > sm_MouseSmoothingThreshold)
				{
					float fInterp = (sm_MouseSmoothingBlendLimit - wdXnewAbs) / (sm_MouseSmoothingBlendLimit - sm_MouseSmoothingThreshold);
					Assert(fInterp >= 0.0f);
					Assert(fInterp <= 1.0f);

					fwdX = Lerp(fInterp, c_fCurrentInputX, wdXnew);
				}
			}
			if( wdYnew != 0.0f && (wdYnewAbs <= sm_MouseSmoothingBlendLimit || sm_MouseSmoothingThreshold < 0.0f) )
			{
				fwdY = wdYnew;
				if(sm_MouseSmoothingThreshold > 0.0f && wdYnewAbs > sm_MouseSmoothingThreshold)
				{
					float fInterp = (sm_MouseSmoothingBlendLimit - wdYnewAbs) / (sm_MouseSmoothingBlendLimit - sm_MouseSmoothingThreshold);
					Assert(fInterp >= 0.0f);
					Assert(fInterp <= 1.0f);

					fwdY = Lerp(fInterp, c_fCurrentInputY, wdYnew);
				}
			}

			// If the final result is below the smoothing threshold and we are not using the average, clear the history,
			// so we start using the average which might be larger than the current value.
			if( fwdX == c_fCurrentInputX && fwdY == c_fCurrentInputY &&
				fwdX <= sm_MouseSmoothingThreshold && fwdY <= sm_MouseSmoothingThreshold)
			{
				sysMemSet(sm_MouseHistory, 0, sizeof(sm_MouseHistory));
			}
			else
			{
				if(fwdX == c_fCurrentInputX && fwdX <= sm_MouseSmoothingThreshold)
				{
					for(int entry = 0; entry < MOUSE_HISTORY_SIZE; entry++)
						sm_MouseHistory[MOUSE_X_AXIS][entry] = 0;
				}
				if(fwdY == c_fCurrentInputY && fwdY <= sm_MouseSmoothingThreshold)
				{
					for(int entry = 0; entry < MOUSE_HISTORY_SIZE; entry++)
						sm_MouseHistory[MOUSE_Y_AXIS][entry] = 0;
				}
			}

			sm_CurrentMouseInput++;
			if (sm_CurrentMouseInput >= sm_MouseSmoothingFrames)
				sm_CurrentMouseInput = 0;
		}
		else
		{
			// If smoothing is disabled, clear history so we won't have any stale value when the user turns it back on.
			sysMemSet(sm_MouseHistory, 0, sizeof(sm_MouseHistory));
		}
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	#endif // MOUSE_SMOOTHING

		// calculate every update just in case the screen position changes.
		int cX = width / 2;
		int cY = height / 2;

		float dX, dY;
		if(m_MouseType == RAW_MOUSE)
		{
			dX = fwdX;
			dY = fwdY;

			// We use the windows cursor position for absolute mouse positions.
			if(!m_ControllerDrivenMovement)
			{
				if (hasCursor)
				{
					m_X = Clamp(static_cast<int>(cursor.x - s_ClientAreaRect.left), 0, width);
					m_Y = Clamp(static_cast<int>(cursor.y - s_ClientAreaRect.top), 0, height);
				}
			}
		}
		else
		{
			dX = (float)(m_wX - cX);
			dY = (float)(m_wY - cY);

			// We cannot use the windows cursor position as are centering it every frame.
			if (!m_ControllerDrivenMovement)
			{
				if(GetAbsoluteOnly())
				{
					m_X = m_wX;
					m_Y = m_wY;
				}
				else
				{
					m_X += m_wX - (width / 2);
					m_Y += m_wY - (height / 2);
				}
				m_X = Clamp(m_X, 0, width);
				m_Y = Clamp(m_Y, 0, height);
			}
		}

		if(GetAbsoluteOnly())
		{
			m_dX = 0.0f;
			m_dY = 0.0f;

			EXTRA_MOUSE_SUPPORT_ONLY(ClearAverageMovementBuffer();)
		}
		else
		{
			m_dX = dX;
			m_dY = dY;
			EXTRA_MOUSE_SUPPORT_ONLY(UpdateAverageMovement();)

			if(m_MouseType == WINDOWS_MOUSE)
			{
				WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - Set cursor position"));
				// If we are not using raw input we now reset the windows position to the center
				// as we are using the cursor's delta to track movement.
				POINT pt;
				pt.x = cX;
				pt.y = cY;
				::ClientToScreen(g_hwndMain, &pt);
				if(SetPlatformCursorPosition(pt.x, pt.y))
				{
					m_wX = cX;
					m_wY = cY;
				}
				WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
			}

			SetCursorClip();
		}

		if(hasCursor && !GRCDEVICE.IsInSizeMove())
		{
			// Update the client area focus flag if the mouse is in the client area directly.
			if( cursor.x >= s_ClientAreaRect.left && cursor.x <= s_ClientAreaRect.right &&
				cursor.y >= s_ClientAreaRect.top && cursor.y <= s_ClientAreaRect.bottom )
			{
				m_ClientAreaHasFocus = true;
			}
			else
			{
				//url:bugstar:5661407 - PC: Playing in resolution above 1920x1080 in windowed mode resets the mouse
				if(!(m_X == 0 || m_Y == 0))
				{
					FlushMouse();
				}
			}
		}
		return;
	}

	if(sm_DiDeviceLost)
	{
		FlushMouse();
		return;
	}

#if USE_DX_MOUSE_DATA_BUFFER && MOUSE_BUFFER_SIZE > 0
	DIDEVICEOBJECTDATA rgdod[MOUSE_BUFFER_SIZE];
	DWORD dwItems = MOUSE_BUFFER_SIZE;


	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - GetDeviceData()"));
	HRESULT hres = s_mouseDev->GetDeviceData(sizeof(DIDEVICEOBJECTDATA), rgdod, &dwItems, 0);
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	if (hres != DI_OK && hres != DI_BUFFEROVERFLOW) {
		inputDebugf1("Failed to read mouse data (0x%08X)", hres);
		sm_DiDeviceLost = true;
		FlushMouse();
		return;
	}

	// Read the immediate mouse data, for buttons.
	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::Update() - GetDeviceState()"));
	hres = s_mouseDev->GetDeviceState(sizeof(s_mouseState),&s_mouseState);
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	if (hres != DI_OK){
		inputDebugf1("Failed to read mouse state (0x%08X)", hres);
		sm_DiDeviceLost = true;
		FlushMouse();
		return;
	}

	m_dX = 0.0f;
	m_dY = 0.0f;
	m_dZ = 0;

	int signedData = 0;
	for (DWORD i = 0; i < dwItems; i++)
	{
		signedData = *(int*)&rgdod[i].dwData;
		switch (rgdod[i].dwOfs)
		{
			case DIMOFS_X:
				if(GetAbsoluteOnly() == false)
				{
					m_dX += (float)signedData;
				}
				break;
			case DIMOFS_Y:
				if(GetAbsoluteOnly() == false)
				{
					m_dY += (float)signedData;
				}
				break;
			case DIMOFS_Z:
				m_dZ += signedData;
				break;
			default:
				break;
		}
	}

	m_dZ /= 120;
#else
	HRESULT hres = s_mouseDev->GetDeviceState(sizeof(s_mouseState),&s_mouseState);
	if(hres != DI_OK)
	{
		inputDebugf1("Failed to read mouse state (0x%08X)", hres);
		sm_DiDeviceLost = true;
		FlushMouse();
		return;
	}

	if(GetAbsoluteOnly() == false)
	{
		m_dX = (float)s_mouseState.lX;
		m_dY = (float)s_mouseState.lY;
	}
	m_dZ = s_mouseState.lZ / 120;
#endif // USE_DX_MOUSE_DATA_BUFFER && MOUSE_BUFFER_SIZE > 0


	m_LastButtons = m_Buttons;
	if (m_UseWindow && !m_Exclusive && !m_EnableGlobalButtons)
	{
		m_Buttons = m_WindowButtons;
	}
	else
	{
		m_Buttons = (s_mouseState.rgbButtons[0]? ioMouse::MOUSE_LEFT : 0) |
			(s_mouseState.rgbButtons[1]? ioMouse::MOUSE_RIGHT : 0) |
			(s_mouseState.rgbButtons[2]? ioMouse::MOUSE_MIDDLE : 0) |
			(s_mouseState.rgbButtons[3]? ioMouse::MOUSE_EXTRABTN1 : 0) | // these buttons are the back and forward buttons on the intellimouse (and other misc things)
			(s_mouseState.rgbButtons[4]? ioMouse::MOUSE_EXTRABTN2 : 0) |
			(s_mouseState.rgbButtons[5]? ioMouse::MOUSE_EXTRABTN3 : 0) |
			(s_mouseState.rgbButtons[6]? ioMouse::MOUSE_EXTRABTN4 : 0) |
			(s_mouseState.rgbButtons[7]? ioMouse::MOUSE_EXTRABTN5 : 0)
			;
	}

	UpdateDoubleClick();
		
	{
		if (hasCursor)
		{
			m_X = Clamp(static_cast<int>(cursor.x - s_ClientAreaRect.left), 0, width);
			m_Y = Clamp(static_cast<int>(cursor.y - s_ClientAreaRect.top), 0, height);
		}
	}

	EXTRA_MOUSE_SUPPORT_ONLY(UpdateAverageMovement();)

	if(hasCursor && !GRCDEVICE.IsInSizeMove())
	{
		// Update the client area focus flag if the mouse is in the client area directly.
		if( cursor.x >= s_ClientAreaRect.left && cursor.x <= s_ClientAreaRect.right &&
			cursor.y >= s_ClientAreaRect.top && cursor.y <= s_ClientAreaRect.bottom )
		{
			m_ClientAreaHasFocus = true;
		}
		else
		{
			FlushMouse();
		}
	}
}

void ioMouse::FlushMouse()
{
	// Changed default from 0,0 to center of window as script uses absolute mouse coordinates to rotate camera
	// and putting mouse cursor at 0,0 makes camera spin to the left when window loses focus.
	m_X   = GRCDEVICE.GetWidth()  >> 1;
	m_Y   = GRCDEVICE.GetHeight() >> 1;
	m_wX  = 0;
	m_wY  = 0;
	m_dX  = 0.0f;
	m_dY  = 0.0f;

	s_RawInput.Lock();
	m_wdX[0] = 0;
	m_wdY[0] = 0;
	m_wdX[1] = 0;
	m_wdY[1] = 0;
	s_RawInput.Unlock();

	m_WindowButtons  = 0;
	ioMouse_dZ_Accum = 0;
	m_LastButtons    = 0;
	m_Buttons        = 0;
	EXTRA_MOUSE_SUPPORT_ONLY(ClearAverageMovementBuffer();)

#if MOUSE_SMOOTHING
	sysMemSet(sm_MouseHistory, 0, sizeof(sm_MouseHistory));
	sm_CurrentMouseInput = 0;
#endif // MOUSE_SMOOTHING
}

void ioMouse::SetExclusive(bool bExclusive)
{
	// Note: if the mouse was not initially set to exclusive mode, this does nothing. (i.e. m_Exclusive must be true)
	if (s_mouseDev && m_Exclusive && bExclusive != m_IsCurrentlyExclusive)
	{
		// this doesn't seem necessary, and removes a cross-dependency on gfx.
		// except that it causes massive choppiness when debugging under W2K!
		HWND hwndTop=g_hwndMain;
		while (GetParent(hwndTop))
			hwndTop=GetParent(hwndTop);

		// Unacquire mouse, change cooperative level and then re-acquire the mouse.
		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::SetExclusive() - Unacquire()"));
		s_mouseDev->Unacquire();
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));

		ditry(s_mouseDev->SetCooperativeLevel(hwndTop, DISCL_FOREGROUND | (bExclusive ? DISCL_EXCLUSIVE : DISCL_NONEXCLUSIVE)));
		m_IsCurrentlyExclusive = bExclusive;

		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::SetExclusive() - Unacquire()"));
		s_mouseDev->Acquire();
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));

#if !__BANK
		::ShowCursor(!bExclusive);
#endif
	}
}

void ioMouse::DeviceLostCB()
{
	if (ioMouse::IsExclusiveSet())
	{
		ioMouse::SetExclusive(false);
	}

	g_WindowWidth = GRCDEVICE.GetWidth();
	g_WindowHeight = GRCDEVICE.GetHeight();
	g_InvWindowWidth = 1.0f / float(GRCDEVICE.GetWidth());
	g_InvWindowHeight = 1.0f / float(GRCDEVICE.GetHeight());
}

void ioMouse::DeviceResetCB()
{
	if (ioMouse::IsExclusiveSet())
	{
		ioMouse::SetExclusive(true);
	}

	g_WindowWidth = GRCDEVICE.GetWidth();
	g_WindowHeight = GRCDEVICE.GetHeight();
	g_InvWindowWidth = 1.0f / float(GRCDEVICE.GetWidth());
	g_InvWindowHeight = 1.0f / float(GRCDEVICE.GetHeight());
}

void ioMouse::SetCursorVisible(bool visible)
{
	SYS_CS_SYNC(s_CS);
	sm_Visible = visible;
	UpdateCursorVisibility();
}

void ioMouse::UpdateCursorVisibility()
{
	CURSORINFO ci;
	ci.cbSize = sizeof(CURSORINFO);

	// Safer than using GetCursorPos() as there is a bug in windows vista. See notes for this
	// function in the header file.
	if(GetCursorInfo(&ci) == TRUE)
	{
		const bool current = (ci.flags & CURSOR_SHOWING) != 0;

		SYS_CS_SYNC(s_CS);
		const bool show = sm_Visible || GetAbsoluteOnly() ASSERT_ONLY(|| sm_HasAssertFired);
		if(current == false && show)
		{
			while(::ShowCursor(true) < 0);
		}
		else if(current && show == false)
		{
			while(::ShowCursor(false) >= 0);
		}
	}
}

void ioMouse::SetMousePosition(int x, int y)
{
	m_X = Max(0, Min(g_WindowWidth,  x));
	m_Y = Max(0, Min(g_WindowHeight, y));
}

bool ioMouse::SetCursorPosition(float x, float y)
{
	Assertf(x >= 0.0f && x <= 1.0f, "Invalid x value for mouse position (%f)!", x);
	Assertf(y >= 0.0f && y <= 1.0f, "Invalid y value for mouse position (%f)!", y);
	Clamp(x, 0.0f, 1.0f);
	Clamp(y, 0.0f, 1.0f);


	if(GetMouseType() == WINDOWS_MOUSE)
	{
		m_X = static_cast<int>(x * static_cast<float>(g_WindowWidth));
		m_Y = static_cast<int>(y * static_cast<float>(g_WindowHeight));
		return true;
	}
	else
	{
		RECT rect;
		sysMemSet(&rect, 0, sizeof(rect));
		if(::GetClientRect(g_hwndMain, &rect))
		{
			// Convert the client rect to screen coords (as outlined at http://support.microsoft.com/kb/11570).
			::ClientToScreen(g_hwndMain, (LPPOINT)&rect.left);
			::ClientToScreen(g_hwndMain, (LPPOINT)&rect.right);
			
			int winX = static_cast<int>(x * static_cast<float>(rect.right - rect.left)) + rect.left;
			int winY = static_cast<int>(y * static_cast<float>(rect.bottom - rect.top)) + rect.top;
			return SetPlatformCursorPosition(winX, winY);
		}
	}

	return false;
}

bool ioMouse::GetPlatformCursorPosition( int* x, int* y )
{
	Assertf(x != NULL && y != NULL, "Pointer Invalid!");
	if(x == NULL || y == NULL)
	{
		return false;
	}

	CURSORINFO ci;
	ci.cbSize = sizeof(CURSORINFO);
	
	// Safer than using GetCursorPos() as there is a bug in windows vista. See notes for this
	// function in the header file.
	bool result = GetCursorInfo(&ci) == TRUE;

	if(result)
	{
		*x = static_cast<int>(ci.ptScreenPos.x);
		*y = static_cast<int>(ci.ptScreenPos.y);
	}
	
	return result;
}


bool ioMouse::SetPlatformCursorPosition( int x, int y )
{
	return SetCursorPos(x, y) == TRUE;
}



#if !__FINAL
void rage::ioMouse::SetAbsoluteOnly( bool absoluteOnly )
{
	// intentional scope lock as GetAbsoluteOnly() also locks.
	{
		SYS_CS_SYNC(s_CS);
		m_AbsoluteOnly = absoluteOnly;
	}
	if(GetAbsoluteOnly())
	{
	#if RSG_PC
		m_dX = 0.0f;
		m_dY = 0.0f;
	#else
		m_dX = 0;
		m_dY = 0;
	#endif

		ReleaseCapture();
	}
	else
	{
		// reset relative movement.
		RECT rect;
		if(::GetWindowRect(g_hwndMain, &rect))
		{
			int width = rect.right - rect.left;
			int height = rect.bottom - rect.top;

			int cX = width / 2;
			int cY = height / 2;

			POINT pt;
			pt.x = cX;
			pt.y = cY;
			::ClientToScreen(g_hwndMain, &pt);

			if(::SetCursorPos(pt.x, pt.y))
			{
				m_wX = cX;
				m_wY = cY;
			}
		}

		Capture();
	}
}
#endif // !__FINAL

void rage::ioMouse::CaptureLost()
{
	// sanity check just in case code calls this incorrectly.
	if(::GetCapture() != g_hwndMain)
		m_RefCount = 0;
}

void rage::ioMouse::RecaptureLostDevices()
{
	if(sm_DiDeviceLost)
	{
		inputDebugf2("Recapturing mouse.");
		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioMouse::RecaptureLostDevices() - Acquire()"));
		HRESULT hres = s_mouseDev->Acquire();
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));

		// Apparently S_FALSE means we already have it acquired.
		if(hres != S_FALSE && FAILED(hres))
		{
			inputDebugf2("Failed to capture mouse (0x%08X)", hres);
			FlushMouse();
		}
		else
		{
			inputDebugf1("Recaptured mouse.");
			sm_DiDeviceLost = false;
		}
	}
}

void rage::ioMouse::UpdateDoubleClick()
{
	u32 now = sysTimer::GetSystemMsTime();
	m_DoubleClickedButtons = 0;

	u32 justPressedButtons = GetPressedButtons();

	for(int i = 0; i < MOUSE_NUM_BUTTONS; ++i)
	{
		u32 bit = (1 << i);

		// If this button is currently pressed.
		if((justPressedButtons & bit) == bit)
		{
			if ((now - m_DoubleClickTimers[i]) <= m_DoubleClickTimeLimit)
			{
				m_DoubleClickedButtons |= bit;
				m_DoubleClickTimers[i] = 0;
			}
			else
			{
				m_DoubleClickTimers[i] = now;
			}
		}
	}
}

void ioMouse::Capture() {
	SYS_CS_SYNC(s_CS);
	m_RefCount++;
}


void ioMouse::ReleaseCapture() {
	SYS_CS_SYNC(s_CS);
	--m_RefCount;
	if(m_RefCount < 0)
		m_RefCount = 0;
}

bool ioMouse::IsIgnoringInput()
{
	return s_IgnoreInput;
}
#endif // RSG_ORBIS

#if RSG_EXTRA_MOUSE_SUPPORT
void ioMouse::UpdateAverageMovement()
{
	const bank_float SENSITIVITY = 0.02f;

	m_nX += static_cast<float>(m_dX) * SENSITIVITY;
	m_nY += static_cast<float>(m_dY) * SENSITIVITY;

	static bank_float TOLERANCE = 0.1f;
#if RSG_PC
	if( (m_dY != 0.0f || m_dX != 0.0f) &&
#else
	if( (m_dY != 0 || m_dX != 0) &&
#endif
		(m_nY > TOLERANCE || m_nY < -TOLERANCE || m_nX > TOLERANCE || m_nX < -TOLERANCE) )
	{
		float mag = Sqrtf(m_nX * m_nX + m_nY * m_nY);
		Assert(mag != 0.0f);
		m_nX /= mag;
		m_nY /= mag;
	}
	else
	{
		m_nX = 0.0f;
		m_nY = 0.0f;
	}
}


void ioMouse::ClearAverageMovementBuffer()
{
	m_nX = 0.0f;
	m_nY = 0.0f;
}
#endif // RSG_EXTRA_MOUSE_SUPPORT