//
// input/mapper.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_MAPPER_H
#define INPUT_MAPPER_H

#include "mapper_defs.h"

#include "atl/array.h"
#include "input/keyboard.h"
#include "input/mouse.h"
#include "math/simplemath.h"
#include "system/new.h"

#define ENABLE_INPUT_COMBOS (0)

#define MAPPER_DOWN_THRESHOLD 0x7F
#define MAPPER_MIN_THRESHOLD 0x80
#define MAPPER_MAX_THRESHOLD 0xFF

namespace rage {

// TODO: Allow each mapping to remember a separate pad input?
// TODO: Return sensible results if a pad is disconnected?

extern float ioAddDeadZone(float originalValue,float deadZone);

// Ensure enum range is just mouse axis. This is because RequiresDeadZone() checks if the input source is not a mouse axis.
// NOTE: this will require updating if an new mouse axis is added.
CompileTimeAssert(IOMS_MOUSE_WHEEL == (IOMS_MOUSE_ABSOLUTEAXIS + 5));

// PURPOSE: Store information about the source of an input.
struct ioSource
{ 
	// PURPOSE:	Represents an unknown parameter.
	const static u32 UNDEFINED_PARAMETER = 0xFFFFFFFF;

	enum
	{
		IOMD_KEYBOARD_MOUSE = -4,	// The keyboard and mouse device.
		IOMD_DEFAULT		= -1,	// Use the default device for a specific mapper.
		IOMD_ANY			= -2,	// Used ONLY for scanning devices for mapping input to indicate scan all devices!
		IOMD_UNKNOWN		= -3,	// Used ONLY for error handling indicating device id is unknown.
		IOMD_SYSTEM			= -5,	// The source was from a system event.
		IOMD_GAME			= -6,	// The source was from game level code.
	};

	// PURPOSE: Constructs a ioSource with a default value of undefined input.
	explicit ioSource(ioMapperSource device = IOMS_UNDEFINED, u32 parameter = UNDEFINED_PARAMETER, s32 padIndex = ioSource::IOMD_UNKNOWN);

	// PURPOSE: The source device e.g. IOM_KEYBOARD;
	ioMapperSource	m_Device;

	// PURPOSE: The source parameter e.g. KEY_M, ioPad::RLEFT etc.
	u32				m_Parameter;

	// PURPOSE: The device id that caused the input, this is for future proofing in the case of
	//			coop play etc and can be used to detect the device that cause the input (e.g.
	//			which controller in a local game).
	s32	m_DeviceIndex;

	const static ioSource	UNKNOWN_SOURCE;

	// PURPOSE:	Indicates if a device id is a real valid device.
	// PARAMS:	device - the device id to test (as s32).
	// RETURNS:	true if the device is a valid id.
	static bool IsValidDevice(s32 device);
};

/*
	PURPOSE
		This class implements a value attached to a particular data source. Values track their previous value so changes can be detected.
	NOTES
		Must be able to map different sources to same action
		Disallow mapping same button to multiple actions
		All players should not have to have the same input mappings
		Looking up an action's value should be O(1) time.
<FLAG Component>
*/
class ioValue {
	friend class ioMapper;
public:
#if RSG_EXTRA_MOUSE_SUPPORT
	typedef float ValueType;
#else
	typedef s32 ValueType;
#endif // RSG_EXTRA_MOUSE_SUPPORT

	// PURPOSE:	The default deadzone value to use for an axis.
	const static float DEFAULT_DEAD_ZONE_VALUE;

	// PURPOSE:	The min deadzone value to use for an axis (scaled by control options).
	const static float DEFAULT_MIN_DEAD_ZONE_VALUE;

	// PURPOSE:	The default value for a button to be classed as down.
	// NOTES:	This is the default value to use to determine if a button is down or not.
	const static float BUTTON_DOWN_THRESHOLD;

	// PURPOSE: The default value for an analogue button to be considered down.
	// NOTES:	This default value to use indicating an analogue-button's value is something meaningful i.e. its state is something other than up (off).
	//			This is roughly 10 in the old 0-255 scale.
	const static float ANALOG_BUTTON_DOWN_THRESHOLD;

	// PURPOSE: Actual internal representation of axes' maximum value (and buttons ON' value). This is here to determine what the value returned by GetValue()
	//	actually means.
	const static ValueType MAX_AXIS;

	// PURPOSE: Constructor
	ioValue();

	// PURPOSE: Settings to alter the behavior of inputs when disabled.
	struct DisableOptions
	{
		// PURPOSE:	Flags to alter the disable behavior.
		// NOTES:	Any attempt to disable an input that is already disabled the flags passed in will only be set if they are currently set and set
		//			in the new options.
		enum DisableFlags
		{
			// PURPOSE:	Disables input updating when input is disabled.
			// NOTES:	If this flag is set, then inputs will not update when disabled. As soon as they are enabled, they will contain the state
			//			they had when disabled.
			F_DISABLE_UPDATE_WHEN_DISABLED = 0x01,

			// PURPOSE:	Allow sustained inputs.
			// NOTES:	If this flag is set, reading a disabled input with the F_READ_SUSTAINED flag set in the ReadOptions structure will treat the
			//			input as if it was not disabled.
			F_ALLOW_SUSTAINED = 0x02,
		};

		// PRUPOSE:	Constructs a DisableOptions structure with no flags set.
		DisableOptions();

		// PURPOSE: Constructs a DisableOptions structure using the passed in flags.
		explicit DisableOptions(u32 flags);

		// PURPOSE: Sets the specified flags.
		// PARAMS:	flags - the flags to set.
		//			enabled - true to enabled the specified flags, false to disable them.
		// NOTES:	If any other flags are set, they will remain set.
		void SetFlags(u32 flags, bool enabled);

		// PURPOSE:	Determines if flags are set.
		// PARAMS:	flags - the flags to check.
		// RETURNS:	true if *ALL* the specified flags are set.
		bool IsFlagSet(u32 flags) const;

		// PURPOSE:	Merges the options passed in with these options.
		// PARAMS:	options - the options to merge with.
		void Merge(const DisableOptions& options);

		// PURPOSE: Flags to alter the disabled behavior.
		// NOTES:	See DisableFlags for possible flags.
		u32 m_DisableFlags;
	};


	// PURPOSE: Options setup to apply default source dependent dead-zoning.
	const static DisableOptions DEFAULT_DISABLE_OPTIONS;

	//..........................................................................

	// PURPOSE: Settings to alter the values returned by GetNorm(), GetUnboundNorm() and GetNorm01().
	struct ReadOptions
	{
		// PURPOSE:	Flags to alter the read behavior.
		enum ReadFlags
		{
			// PURPOSE:	Does not apply a dead zone whilst reading the input.
			// NOTES:	Setting a dead zone value to 0 will also have the side effect of disabling the dead zone.
			F_NO_DEAD_ZONE = 0x01,

			// PURPOSE:	Applies a dead zone no matter of the source.
			// NOTES:	If this flag is not set, a dead zone will be applied on a per-source basis (e.g. to game stick but not a mouse).
			//			Also note that F_NO_DEAD_ZONE *WILL* override this flag if it is set (i.e. no dead zone will be applied).
			F_DEAD_ZONE_ALL_SOURCES = 0x02,

			// PURPOSE: Read unbound values (values can exceed -1.0f...+1.0f range).
			F_UNBOUND = 0x04,

			// PURPOSE:	Reads the input value even if the input is disabled.
			F_READ_DISABLED = 0x08,

			// PURPOSE: Read a sustained input if allowed.
			// NOTES:	If an input has been disabled with the F_ALLOW_SUSTAINED flag set in the DisableOptions structure, then setting this flag will
			//			treat the input as if it is not disabled.
			F_READ_SUSTAINED = 0x10,
		};

		// PURPOSE: Constructs a ReadOptions with default values of no flags set and the dead zone set to 0 (i.e. no deadzone).
		ReadOptions();

		// PURPOSE: Constructs a ReadOptions with a specified dead zone that will be source dependent.
		// PARAMS:	deadZone - the dead zone to use for extracting a value.
		// NOTES:	Intentionally implicit (not using explicit keyword) as this version of the constructor is here for legacy compatibility and simplicity.
		//			Also not that this constructor also has some flags set by default, this is also for compatibility and simplicity of retrieving a value.
		ReadOptions(float deadZone);

		// PURPOSE: Constructs a ReadOptions with the default dead zone but using the flags passed in.
		// PARAMS:	flags - the flags to use for the read options.
		explicit ReadOptions(u32 flags);

		// PURPOSE:	Constructs a ReadOptions using the deadZone and flags passed in.
		// PARAMS:	deadZone - the dead zone to use for extracting a value.
		// PARAMS:	flags - the flags to use for the read options.
		ReadOptions(float deadZone, u32 flags);

		// PURPOSE: Sets the specified flags.
		// PARAMS:	flags - the flags to set.
		//			enabled - true to enabled the specified flags, false to disable them.
		// NOTES:	If any other flags are set, they will remain set.
		void SetFlags(u32 flags, bool enabled);

		// PURPOSE:	Determines if flags are set.
		// PARAMS:	flags - the flags to check.
		// RETURNS:	true if *ALL* the specified flags are set.
		bool IsFlagSet(u32 flags) const;

		// PURPOSE: The dead zone to use whilst extracting a value.
		float m_DeadZone;

		// PURPOSE: The read flags to alter the behavior of reading an input.
		u32	m_ReadFlags;

		// These are for internal defaults setup and should not be used.
		enum Setup_DeafultBoundOptions		{SETUP_DEFAULT_BOUND_OPTIONS};
		enum Setup_DeafultUnboundOptions	{SETUP_DEFAULT_UNBOUND_OPTIONS};
		ReadOptions(Setup_DeafultBoundOptions);
		ReadOptions(Setup_DeafultUnboundOptions);
	};

	// PURPOSE: Options setup to apply default source dependent dead-zoning.
	const static ReadOptions DEFAULT_OPTIONS;

	// PUSPOSE: Options setup to apply default source dependent dead-zoning with an unbound value.
	const static ReadOptions DEFAULT_UNBOUND_OPTIONS;

	// PURPOSE: Options setup to not apply any dead-zoning to values..
	const static ReadOptions NO_DEAD_ZONE;

	// PURPOSE: Options setup to always apply any dead-zoning to values..
	const static ReadOptions ALWAYS_DEAD_ZONE;

	// PURPOSE:	Returns true if the input source requires deadzoning.
	// PARAMS:	source - the source to check.
	static bool RequiresDeadZone(const ioMapperSource source);
	static bool RequiresDeadZone(const ioSource&  source);

	// RETURNS: Normalized (-1..+1) version of current data, with a dead zone applied
	// PARAMS:	options - indicates which options to perform on the input values.
	// NOTES:	This function ignores the Unbound flag in Options.
	float GetNorm(const ReadOptions& options = DEFAULT_OPTIONS) const;

	// PURPOSE: Normalized version of current data. If, however, the axis has a larger than normal value (e.g. from a mouse), it will not be clamped between -1..+1.
	// RETURNS: Normalized value that can exceed -1..+1.
	// PARAMS:  options - indicates which options to perform on the input values.
	// NOTES:	This function ignores the Unbound flag in Options.
	float GetUnboundNorm(const ReadOptions& options = DEFAULT_OPTIONS) const;

	// RETURNS: Normalized (-1..+1) version of previous data, with a dead zone applied// PARAMS: deadZone - dead zone to apply to input
	// PARAMS:	options - indicates which options to perform on the input values.
	// NOTES:	This function ignores the Unbound flag in Options.
	float GetLastNorm(const ReadOptions& options = DEFAULT_OPTIONS) const;

	// RETURNS: Normalized version of previous data. If, however, the axis has a larger than normal value (e.g. from a mouse), it will not be clamped between -1..+1.
	// PARAMS:	options - indicates which options to perform on the input values.
	// NOTES:	This function ignores the Unbound flag in Options.
	float GetLastUnboundNorm(const ReadOptions& options = DEFAULT_OPTIONS) const;


	// RETURNS: Normalized version of current data. If, however, the axis has a larger than normal value (e.g. from a mouse), it will not be clamped between -1..+1.
	// PARAMS:	options - indicates which options to perform on the input values.
	// NOTES:	With the default options, this function is identical to GetNorm(), if the UnboundValues option is set then this is identical to GetUnboundNorm().
	float GetNormalized(const ReadOptions& options = DEFAULT_OPTIONS) const;

	// RETURNS: Normalized version of previous data. If, however, the axis has a larger than normal value (e.g. from a mouse), it will not be clamped between -1..+1.
	// PARAMS:	options - indicates which options to perform on the input values.
	// NOTES:	With the default options, this function is identical to GetNorm(), if the UnboundValues option is set then this is identical to GetUnboundNorm().
	float GetLastNormalized(const ReadOptions& options = DEFAULT_OPTIONS) const;


	// RETURNS: Normalized (0..+1) version of current data
	// PARAMS:	options - indicates which options to perform on the input values.
	float GetNorm01(const ReadOptions& options = DEFAULT_OPTIONS) const;

	// RETURNS: Current data in the range of 0 - 255.
	// NOTE: This is deprecated and added for backward compatibility.
	u8  GetOld8BitValue() const;

	// RETURNS: The source of the input (e.g. IOMS_KEYBOARD).
	const ioSource& GetSource() const;

	// RETURNS: The source of the last input (e.g. IOMS_KEYBOARD).
	const ioSource& GetLastSource() const;
	
	// PURPOSE: Set ioValue to support history functionality
	void SupportHistory();

	// RETURNS: True if data represents a "down" value (makes most sense for keys and buttons)
	bool IsDown(float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if "down" now and was "down" during the whole duration.
	// PARAMS:	durationMS	- The time period to check if the button was pressed.
	//			threshold	- The threshold in which a button is considered pressed. If negative then the axis is assumed inverted (in the negative direction).
	//			options - indicates which options to perform on the input values.
	// NOTES:	If threshold is negative then the button will be considered pressed if its value is lower than the threshold (useful for axes and inverted buttons).
	bool HistoryHeldDown(u32 durationMS, float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if "down" now and was "down" during the whole duration.
	bool HistoryHeldDown(u32 durationMS, float downRangeMin, float downRangeMax, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if data represents an "up" value (makes most sense for keys and buttons)
	bool IsUp(float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if "up" now and was "up" during the whole duration.
	// PARAMS:	durationMS	- The time period to check if the button was pressed.
	//			threshold	- The threshold in which a button is considered pressed. If negative then the axis is assumed inverted (in the negative direction).
	//			options - indicates which options to perform on the input values.
	// NOTES:	If threshold is negative then the button will be considered pressed if its value is lower than the threshold (useful for axes and inverted buttons).
	bool HistoryHeldUp(u32 durationMS, float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if "up" now and was "up" during the whole duration.
	bool HistoryHeldUp(u32 durationMS, float downRangeMin, float downRangeMax, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if data was "down" on previous update
	bool WasDown(float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if "down" now and was "up" at some point previously during the duration.
	bool HistoryPressedAndHeld(u32 durationMS, u32* pOutLastTimeMsWhereUp = NULL, float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if "down" now and was "up" at some point previously during the duration.
	bool HistoryPressedAndHeld(u32 durationMS, u32* pOutLastTimeMsWhereUp, float downRangeMin, float downRangeMax, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if data was "up" on previous update
	bool WasUp(float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if "up" now and was "down" at some point previously during the duration.
	bool HistoryReleasedAndHeld(u32 durationMS, u32* pOutLastTimeMsWhereDown = NULL, float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;
	
	// RETURNS: True if "up" now and was "down" at some point previously during the duration.
	bool HistoryReleasedAndHeld(u32 durationMS, u32* pOutLastTimeMsWhereDown, float downRangeMin, float downRangeMax, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if data transitioned from "up" to "down" since last update
	bool IsPressed(float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if data transitioned from "up" to "down" anywhere during the duration.
	bool HistoryPressed(u32 durationMS, u32* pOutLastTimeMsTransitioned = NULL, float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if data transitioned from "up" to "down" anywhere during the duration.
	bool HistoryPressed(u32 durationMS, u32* pOutLastTimeMsTransitioned, float downRangeMin, float downRangeMax, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if data transitioned from "down" to "up" since last update
	bool IsReleased(float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;
	
	// RETURNS: True if data transitioned from "down" to "up" anywhere during the duration.
	bool HistoryReleased(u32 durationMS, u32* pOutLastTimeMsTransitioned = NULL, float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;
	
	// RETURNS: True if data transitioned from "down" to "up" anywhere during the duration.
	bool HistoryReleased(u32 durationMS, u32* pOutLastTimeMsTransitioned, float downRangeMin, float downRangeMax, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if "up" now and was "down" during the rest of the duration.
	bool IsReleasedAfterHistoryHeldDown(u32 uDurationMS, float threshold = BUTTON_DOWN_THRESHOLD, const ReadOptions& options = NO_DEAD_ZONE) const;

	// RETURNS: True if data is marked as inverted
	bool IsInverted() const;

	//..........................................................................
	// MANIPULATION FUNCTIONS
	// 
	// PURPOSE: Mark the data channel as inverted/non-inverted.
	// PARAMS:	invert - should the channel be marked as inverted.
	void SetInverted(bool invert);

	// PURPOSE: Mark the data channel as inverted
	void MakeInverted();

	// PURPOSE: Mark the data channel as non-inverted
	void MakeNormal();

	// PURPOSE: Enables a disabled input.
	void Enable();

	// PURPOSE:	Mark an input as disabled.
	// PARAMS:	allowUpdateWhenDisabled - if true, the input will still update even if it is disabled. This is usually the desired result.
	void Disable(const DisableOptions& options = DEFAULT_DISABLE_OPTIONS);

	// PURPOSE: Ignores an input source for one frame.
	// PARAMS:	source - the ioMapperSource to ignore.
	void Ignore(ioMapperSource source);

	// PURPOSE: Indicates if the input is enabled or disabled.
	// NOTES:	A disabled input will still function but will always have a value of zero [unless explicitly overriden when reading the value, see GetNoral() or GetUnboundNormal()].
	// RETURNS: True if the input is inverted.
	bool IsEnabled() const;

	// PURPOSE: Sets the state and full state history of an ioValue
	void InitValueAndHistory(ValueType value, u32 timeMS = 0);

	// PURPOSE: Sets the state of an ioValue
	// PARAMS:	value - The value to set the input to
	//			source - The source of the input device that caused the input (e.g. IOMS_KEYBOARD).
	void SetCurrentValue(ValueType value, const ioSource& source = ioSource::UNKNOWN_SOURCE);

	// PURPOSE: Sets the state of an ioValue for the next frame.s
	// PARAMS:	value - The value to set the input to.
	//			source - The source of the input device that caused the input (e.g. IOMS_KEYBOARD).
	void SetNextValue(ValueType value, const ioSource& source = ioSource::UNKNOWN_SOURCE);

	void Update(u8 updateID, u32 timeMS = 0);

	// PURPOSE: Resets the state of the ioValue
	void Reset();

#if __DEV
	// PURPOSE:	Set the id of this ioValue. This is for debugging only.
	// PARAMS:	id - the id to be set to.
	// NOTES:	This is so and ioValue 'knows' what it represents, this can be helpful for debugging.
	void SetId(u32 id);

	// PURPOSE:	Gets the id of this ioValue. This is for debugging only.
	// RETURNS:	The id that has been set in the ioValue.
	// NOTES:	This is so and ioValue 'knows' what it represents, this can be helpful for debugging.
	u32 GetId() const;
#endif // __DEV

private:

	// PURPOSE:	Actual internal representation of axes' minimum value.
	// NOTES:	This is here to determine what the value returned by GetValue() actually means.
	const static ValueType MIN_AXIS;

	// PURPOSE:	Actual internal representation of axes' center value (and buttons' OFF value).
	// NOTES:	This is here to determine what the value returned by GetValue() actually means.
	const static ValueType CENTER_AXIS;

	// PURPOSE: Actual internal representation of axes' maximum value. This is here to determine what the value returned by GetValue()
	//	actually means.
	//	NOTES:	This is a float version of the constant MAX_AXIS. It exists so we do not have to constantly cast MAX_AXIS to a float when
	//		doing scaling.
	const static float MAX_AXIS_F;

	// PURPOSE: The scale difference between legacy-axis/gamepad-axis/8-bit values and new s32 values.
	//	NOTES:	This is used for converted old values to new values and vice-versa.
	const static ValueType LEGACY_AXIS_SCALE;

	// PURPOSE: The shift difference between legacy-axis/gamepad-axis/8-bit values and new s32 values. i.e. how far the axis is shifted and in what direction.
	//	NOTES:	This is used for converted old values to new values and vice-versa.
	const static s32 LEGACY_AXIS_SHIFT;

	// PURPOSE:	Converts a normalized value (-1...+1) to an internal value equivalent.
	// PARAMS:	normValue - normalized value to convert. This can exceed the range of -1...+1.
	// RETURNS:	Internal value equivalent of a normalized value.
	// NOTES:	Values passed in can be negative and outside the range of -1...+1. E.g. a Value of 2.0f will return 2 * MAX_AXIS.
	static s32 Unormalize(const float normValue) { return (s32)(MAX_AXIS_F * normValue); }

	// PURPOSE: Internal clamping function, used to prevent having to include amath.h (and breaking mapper.cpp's redefines of abs)
	static float ClampIt(float test, float min, float max) { return ((test<min) ? min : (test>max) ? max : test); }
	static s32 ClampIt(s32 test, s32 min, s32 max) { return ((test<min) ? min : (test>max) ? max : test); }

	// PURPOSE: Normalizes so that an axis will be normalized to -1..+1. If, however, an axis has a larger than normal value (e.g. from a mouse), it will not be clamped between -1..+1.
	// PARAMS: value - value to normalize to -1..+1 range.
	// RETURNS: Normalized value that can exceed -1..+1.
	// NOTES: Normalized value that CAN exceed the rang of -1..+1, but won't give you an exact zero.
	static float Normalize(ValueType value);

	// PURPOSE:	Formats an input value based on the options passed in.
	// PARAMS:	value - the value to format.
	//			source - the source of the input.
	//			options - the options to perform on the input value.
	static float PerformInputFormatting(float value, const ioMapperSource source, const ReadOptions &options);

//TODO: Make private once tools are no longer needing this public.
public:
	// RETURNS: Current data (with axis inversion applied)
	// PARAMS:	options - the read options to use.
	ValueType GetValue(const ReadOptions& options = NO_DEAD_ZONE) const;
private:

	// RETURNS: Last data (with axis inversion applied)
	// PARAMS:	options - the read options to use.
	ValueType GetLastValue(const ReadOptions& options) const;

	// PURPOSE: Determines the index to use in history array.
	u8	GetHistoryIndex(u8 depth) const;

	// RETURNS: Previous data (with axis inversion applied)
	// PARAMS:	options - The options to perform on the input value.
	float GetHistoryNormalizedAtDepth(u8 depth, const ReadOptions& options) const;

	u32	GetHistoryTimeAtDepth(u8 depth) const;

	// RETURNS: Source (e.g. IOMS_KEYBOARD) of previous input.
	ioSource GetHistorySourceAtDepth(u8 depth) const;

	ValueType		m_InvertValue;
	ValueType		m_Value;
	ValueType		m_LastValue;
	ValueType		m_NextFrameValue;
	ioSource		m_Source; // The source of the device type that caused the input.
	ioSource		m_LastSource;
	ioSource		m_NextFrameSource;
	ioMapperSource	m_IgnoreSource;
	u8			m_LastUpdateID;
	u8			m_CurrentValueIndex;
	bool		m_enabled : 1;
	bool		m_disableNextUpdate : 1;
	bool		m_IgnoreThisUpdate : 1;
	
	// The disable options of the disabled input.
	DisableOptions m_DisableOptions;

	enum { MaxHistoryValues = 64 };
	struct HistoryValue
	{
		ValueType	m_value;
		u32			m_timeMS;
		ioSource	m_source;
	};
//	HistoryValue m_HistoryValues[MaxHistoryValues];
	HistoryValue* m_HistoryValues;

#if __DEV
	// This is an identifier to help track which input (ioValue) this is. As an ioValue does not know what it represents (the higher level code does),
	// the higher level can pass and id to help debugging inside the ioValue.
	u32 m_Id;
#endif // __DEV
};


/*
	PURPOSE
		Provides a simple, configurable level of abstraction between concrete controller
		inputs and virtual game inputs.

		Several different classes can potentially share the same ioMapper object, and each class
		can declare its own inputs (as individual ioValues) as necessary.  Associate an ioValue with
		a particular channel (param) of a particular input device type (source) by calling Map().
	
	<FLAG Component>
*/
class ioMapper {
public:
	// PURPOSE: Constructor
	ioMapper();

	enum {
		// PURPOSE: The base maximum number of sources that can be returned for an input.
		IOMAPPER_SOURCE_LIST_BASE = 32,

		// PURPOSE: The maximum number of mappings that can be returned from ioMapper::GetMappedValues().
		IOMAPPER_VALUE_LIST_LIMIT = 64,

#if RSG_PC
		// PRUPOSE: The maximum number of mappings that can be returned from ioMapper::GetSources().
		// NOTES:	IOMAPPER_SOURCE_LIST_BASE * 6 is for two sets of keyboard mappings and controller devices (including direct input).
		IOMAPPER_SOURCE_LIST_LIMIT = IOMAPPER_SOURCE_LIST_BASE * 6,
#else
		// PRUPOSE: The maximum number of mappings that can be returned from ioMapper::GetSources().
		IOMAPPER_SOURCE_LIST_LIMIT = IOMAPPER_SOURCE_LIST_BASE,
#endif // RSG_PC
	};

	// PURPOSE:	Data structure used for retrieving sources mapped to an ioValue.
	typedef atFixedArray<ioSource, IOMAPPER_SOURCE_LIST_LIMIT>	ioSourceList;

	// PURPOSE: Data structure used for retrieving values mapped to an source.
	typedef atFixedArray<ioValue*, IOMAPPER_VALUE_LIST_LIMIT>	ioValueList;

	// PURPOSE: Converts an ioMapperParameter to its device equivalent.
	// PARAMS:	source - the source of the parameter.
	//			mapperParameter - the parameter to convert.
	// RETURNS: The converted parameter.
	// NOTES:	The ioMapperParameter is used in the parser meta files for key mappings, however, when calling Map or Change
	//			the device equivalent is needed. This function will apply any conversions that are needed.
	static u32 ConvertParameterToDeviceValue(const ioMapperSource source, const ioMapperParameter mapperParameter);

	// PURPOSE: Converts an device parameter value to its ioMapperParameter equivalent.
	// PARAMS:	source - the source of the parameter.
	//			deviceParameter - the parameter to convert.
	// RETURNS: The converted parameter.
	// NOTES:	The ioMapperParameter is used in the parser meta files for key mappings, however, when calling Map or Change
	//			the device equivalent is needed. This function will apply any conversions that are needed.
	static ioMapperParameter ConvertParameterToMapperValue(const ioMapperSource source, u32 deviceParameter);

	// PURPOSE: Retrieves all sources that are related to a particular source.
	// PARAMS:	source - The source to find related sources to.
	//			sources - An array to populate with related input sources.
	// NOTES:	Used internally for retrieving related sources such such as alternative representations
	//			of a source (e.g. IOMS_MOUSE_ABSOLUTEAXIS and IOMS_MOUSE_NORMALIZED are both the mouse).
	static void GetRelatedSources(const ioSource& source, ioMapper::ioSourceList& relatedSources);


	// PURPOSE:	Controls which ioPad object to monitor for this ioMapper.
	// PARAMS:	player - index of ioPad number to attach this ioMapper to
	// NOTES:	It is possible to map a pad input to a specific controller as well, in which case
	//			that will take precedence over any player specified here.
	void SetPlayer(s32 player);

	// RETURNS: Current ioPad number attached to ioMapper
	s32 GetPlayer() const;

	// PURPOSE: Resets list of tracked objects
	void Reset();

	// PURPOSE: Options containing settings about which input devices to scan for input on.
	struct ScanOptions
	{
		// PURPOSE: Creates an ioScanOptions with no options set and the device index set to ioSource::UNKONWN_DEVICE.
		ScanOptions();

		// PURPOSE: Mouse scan options.
		enum MouseOptions
		{
			NO_MOUSE		= 0x0,
			MOUSE_BUTTONS	= 0x1,
			MOUSE_WHEEL		= 0x2,
			MOUSE_MOVEMENT	= 0x4
		};

		// PURPOSE: Keyboard scan options.
		enum KeyboardOptions
		{
			NO_KEYBOARD			= 0x0,
			KEYBOARD_BUTTONS	= 0x1
		};

		// PURPOSE: Pad scan options.
		enum PadOptions
		{
			
			NO_PAD				= 0x0,	// PURPOSE: Do not scan for pad input.
			PAD_BUTTONS			= 0x1,	// PURPOSE: Scan for pad input.
			PAD_DPAD_AXIS		= 0X2,	// PURPOSE: Treat dpad buttons as an axis.
			PAD_AXIS			= 0x4,	// PURPOSE: Scan for axis input.
			PAD_AXIS_DIRECTION	= 0x8,	// PURPOSE: Treat axis as directional input (each direction is a separate input source).
		};

		// PURPOSE: Mouse scan options.
		u16	m_MouseOptions;

		// PURPOSE: Keyboard scan options.
		u16	m_KeyboardOptions;

		// PURPOSE: Pad scan options.
		u16	m_PadOptions;

#if RSG_PC
		// PURPOSE: Joystick scan options.
		// NOTE: The joystick is direct input which is only available on PC.
		enum JoystickOptions
		{
			NO_JOYSTICK				= 0x0,
			JOYSTICK_BUTTONS		= 0x1,
			JOYSTICK_POV			= 0x2,
			JOYSTICK_AXIS			= 0x4,
			JOYSTICK_AXIS_DIRECTION	= 0x8,	// PURPOSE: Treat axis as directional input (each direction is a separate input source).
		};

		// PURPOSE: Joystick scan options.
		u16	m_JoystickOptions;
#endif // RSG_PC

		s32 m_DeviceID;

		// These are for internal defaults setup and should not be used.
		enum Setup_DefaultDeviceOptions		{SETUP_DEFAULT_DEVICE_OPTIONS};
		ScanOptions(Setup_DefaultDeviceOptions);
	};

	// PURPOSE:	Options setup with default scan options for the current platform.
	// NOTES:	On PS3/360 builds this will be setup to scan for pad input only (including axis) using the default pad index.
	//			On PC this will be setup to scan for pad/joystick using any device index and for keyboard and mouse (not the mouse axis).
	const static ScanOptions DEFAULT_DEVICE_SCAN_OPTIONS;

	// PURPOSE: Scans all current input devices for activity; intended for dynamic rebinding; returns true if it saw anything
	// PARAMS: source - object which receives the source of an input that had motion
	//		options - Indicates which devices to scan for.
	// RETURNS: True if any input seen, else false
	// NOTES: For mouse, will return iomRelativeAxis, not iomAbsoluteAxis.  Change 'source' if that's appropriate for your app.
	// For pad buttons, will return iomPadDigitalButton, not iomPadAnalogButton; the former is a bitmask, the latter is an index.
	// On PC builds, it will NOT scan ioPad devices for activity, instead scanning the joysticks directly.
	// Call BeginScan first to take snapshot of global device state so we can see what's moving.
	bool Scan(ioSource &source, const ScanOptions &options);

	// PURPOSE: Call this before Scan
	static void BeginScan();

	// PURPOSE: Call this after you're done scanning.  Currently a no-op.
	static void EndScan();

	// PURPOSE: Binds input source to specified value.  The same input can be attached
	// to more than one value, and multiple inputs can be bound to the same value.
	// The former can allow a single button to fire off different actions either at once
	// or in a context-dependent fashion.  The latter can allow more than one button
	// to have the same effect in-game.  If you're changing the binding of an action,
	// call Change instead, otherwise you'll end up with both the old and new bindings
	// affecting the same value object.
	// PARAMS: source - general source of input (mouse, keyboard, etc)
	//		param - specific data (which mouse axis or button, keyboard key, or pad axis or button)
	//		value - address of ioValue object which will be bound to this input (owned by caller)
	//		padOverride - If not ioSource::IOMD_DEFAULT, force input to always look at this pad index regardless of
	//			the player assigned to this mapper.  Only valid for pad sources.
	// RETURNS: Index in the array this new mapping uses
	int Map(ioMapperSource source,u32 param,ioValue &value,int padOverride = ioSource::IOMD_DEFAULT);

	// PURPOSE: Binds input source to specified value.  Any previous binding of the source to a different value 
	// (within this ioMapper object) is broken
	// PARAMS: source - general source of input (mouse, keyboard, etc)
	//		param - specific data (which mouse axis or button, keyboard key, or pad axis or button)
	//		value - address of ioValue object which will be bound to this input (owned by caller)
	//		allowDuplicates - whether or not we allow duplicate inputs (useful for modal inputs,
	//			where the context of the game controls how it's interpreted)
	// NOTES: This version is so that our special KEY_CTRL and KEY_SHIFT macros can work.
	// RETURNS: Index in the array this new mapping (the second one) uses
	int Map(ioMapperSource source,u32 param,u32 param2,ioValue &value);

	// PURPOSE: Call this function when re-binding an existing value object so that you
	//			don't get two inputs bound to the same value (see ioMapper::Map for details).
	// PARAMS:	mappingIndex - the mapping to replace for a given ioValue.
	//			newSource - the new source to use for the input. NOTE: mPadIndex is NOT ignored and must be set.
	//			value - address of ioValue object which will be bound to this input (owned by caller)
	// NOTES:	If value isn't already in the mapper object, Change just calls Map instead. This version works when there are multiple
	//			sources mapped to the same input. The mappingIndex is the mapping slot to replace for all devices. To find it, it will be
	//			the ioSource array index returned from GetMappedSources().
	void Change(const u32 mappingIndex, const ioSource& newSource, ioValue &value);

	// PURPOSE: Call this function to remove an existing mapping.
	// PARAMS:	source - the source of the input to remove.
	//			value - the input that the source is currently mapped to.
	// RETURNS:	true if the input was successfully removed.
	// NOTES:	This is relatively slow as it does a linear search followed by a linear copy of the remaining elements.
	bool Remove(const ioSource& source, const ioValue& value);

	// PURPOSE: Call this function to remove all the mappings to an ioValue from a particular device.
	// PARAMS:  value - the ioValue to remove all mappings to.
	//			deviceId - the id of the device to remove mapping to.
	// NOTES:	This is relatively slow as it does a linear search.
	void RemoveDeviceMappings(const ioValue& value, s32 deviceId);

	// PURPOSE: Updates all Map'd values based on current input state.
	// PARAMS:	timeMS - the time delta.
	//			enableKeyboardMouse - true to always enable keyboard and mouse, false to use internal keyboard mouse enable state.
	// NOTES: Should be called exactly once per frame or you'll miss IsPressed/IsReleased events.
	void Update(u32 timeMS = 0, bool forceKeyboardMouse = true, float timeStep=(1.0f/30.0f));

#if ENABLE_INPUT_COMBOS
	// PURPOSE:	Retrieves to normalized low level input value for a source.
	// PARAMS:	source - the source to read.
	//			enableKeyboardMouse - true to always enable keyboard and mouse, false to use internal keyboard mouse enable state.
	static float GetSourceValue(const ioSource& source, bool forceKeyboardMouse = true);
#endif // ENABLE_INPUT_COMBOS

	// PURPOSE: Clears all Mapped values to a valid null value for each map type
	void ClearValues() const;

	// RETURNS: Count of active mappings, for iterating
	int GetCount() const;

	// RETURNS: Mapper source associated with particular input
	// PARAMS:	i - Index of mapper sourceto return
	// NOTES:	DEPRACATED try to use GetMappedSources instead as a source can have multiple mappings!
	ioMapperSource GetMapperSource(int i) const;

	// PURPOSE:	Retrieves all sources mapped with a particular input value.
	// PARAMS:	value - An ioValue to check for mappings to.
	//			sources - An array to populate with input sources.
	void GetMappedSources(const ioValue& value, ioSourceList& sources) const;

	// PURPOSE:	Retrieves all values associated with a particular source.
	// PARAMS:	source - A source to check for mappings to.
	//			values - An array to populate with input sources.
	void GetMappedValues(const ioSource& source, ioValueList& values) const;

	// RETURNS: Parameter associated with particular input (ie which pad button is assigned to it)
	// PARAMS: i - Index of parameter to return
	unsigned GetParameter(int i) const;

	// RETURNS: Pointer to value object associated with particular input
	// PARAMS: i - Index of parameter to return
	ioValue* GetValue(int i) const;

	// RETURNS: Index within mapper of supplied value object, or -1 if not found
	// PARAMS: value - value object to match (matched by its address in memory)
	int FindValue(ioValue &value);

	// PURPOSE: Set specified parameter to a specified value
	// PARAMS: i - Index of parameter to set
	//		v - Value to set parameter to
	// NOTES: This allows you to dynamically reassign buttons, etc during gameplay.
	// You can't change the overall class of the input, although this could be added if necessary
	void SetParameter(int i,unsigned v);

#if RSG_EXTRA_MOUSE_SUPPORT
	// PURPOSE: Set the mouse scale for scaled mouse movement.
	// PARAMS: scale - The scale to use for mouse input.
	// NOTES: This scales mouse movement when using scaled mouse input. This should not be negative
	//		and should not be used for inverting axis.
	void SetMouseScale(const float scale);

	// RETURNS: The current mouse scale value used for scaled mouse movement.
	float GetMouseScale() const;

	// PURPOSE: Set the duration of time (in milliseconds) that each notch of the mouse wheel will produce an input for relative mouse wheel axes.
	void SetMouseWheelInputDuration(u32 timeMS);

	// RETURNS: The length of time (in milliseconds) that each not of the mouse wheel will produce an input for relative mouse wheel axes.
	u32 GetMouseWheelInputDuration() const;

	// PURPOSE: Set the amount the springed mouse position will decay back towards 0.0f over the space of a second.
	void SetMouseSpringDecayRate(float decayRate);

	// RETURNS: Get the amount the springed mouse position will decay back towards 0.0f over the space of a second.
	float GetMouseSpringDecayRate() const;

	// RETURNS: True if the specified param is a mouse X axis.
	static bool IsMouseXAxis(ioMapperParameter param);

	// RETURNS: True if the specified param is a mouse Y axis.
	static bool IsMouseYAxis(ioMapperParameter param);

#endif // RSG_EXTRA_MOUSE_SUPPORT

	// PURPOSE:	Extracts the positive param for IOMS_MKB_AXIS.
	// PARAMS:  param - the IOMS_MKB_AXIS parameter.
	//			includeFlags - if true, mouse flag will be preserved.
	// RETURNS: the positive parameter for an IOMS_MKB_AXIS.
	static u32 GetMkbAxisPositive(u32 param, bool includeFlags = false);

	// PURPOSE:	Extracts the negative param for IOMS_MKB_AXIS.
	// PARAMS:  param - the IOMS_MKB_AXIS parameter.
	//			includeFlags - if true, mouse flag will be preserved.
	// RETURNS: the negative parameter for an IOMS_MKB_AXIS.
	static u32 GetMkbAxisNegative(u32 param, bool includeFlags = false);

	// PURPOSE: Creates an IOMS_MKB_AXIS parameter from two IOMS_KEYBOARD or IOMS_MOUSE_BUTTON parameters.
	// PARAMS:	positiveParam - the parameter to be used as the positive parameter.
	//			negativeParam - the parameter to be used as the negative parameter.
	// RETURNS: the parameter made up of two parameter or 0 on error.
	static u32 MakeMkbAxis(ioMapperParameter positiveParam, ioMapperParameter negativeParam);

	// PURPOSE:	Indicates that the positive param for IOMS_MKB_AXIS is a mouse button.
	// PARAMS:  param - the IOMS_MKB_AXIS parameter.
	// RETURNS: true if the positive parameter for IOMS_MKB_AXIS is a mouse button.
	static bool IsMkbAxisPositiveAMouseButton(u32 param);

	// PURPOSE:	Indicates that the negative param for IOMS_MKB_AXIS is a mouse button.
	// PARAMS:  param - the IOMS_MKB_AXIS parameter.
	// RETURNS: true if the negative parameter for IOMS_MKB_AXIS is a mouse button.
	static bool IsMkbAxisNegativeAMouseButton(u32 param);

	// PURPOSE:	Indicates that the positive param for IOMS_MKB_AXIS is a mouse wheel.
	// PARAMS:  param - the IOMS_MKB_AXIS parameter.
	// RETURNS: true if the positive parameter for IOMS_MKB_AXIS is a mouse wheel.
	static bool IsMkbAxisPositiveAMouseWheel(u32 param);

	// PURPOSE:	Indicates that the negative param for IOMS_MKB_AXIS is a mouse wheel.
	// PARAMS:  param - the IOMS_MKB_AXIS parameter.
	// RETURNS: true if the negative parameter for IOMS_MKB_AXIS is a mouse wheel.
	static bool IsMkbAxisNegativeAMouseWheel(u32 param);

	// PURPOSE: Load a controller mapping from a named file
	// PARAMS:	name - Filename as per ASSET.Open
	//		version - Version number, to identify out-of-date config file
	// RETURNS: True on success, else false
	bool Load(const char *name,int version);

	// PURPOSE: Load a controller mapping from an already opened stream
	// PARAMS: S - Stream to load from
	//		version - Version number, to identify out-of-date config file
	// RETURNS: True on success, else false
	bool Load(class fiStream *S,int version);

	// PURPOSE:  a controller mapping to a named file
	// PARAMS:	name - Filename as per ASSET.Create
	//		version - Version number, to identify out-of-date config file
	// RETURNS: True on success, else false
	bool Save(const char *name,int version);

	// PURPOSE: Save a controller mapping to an already opened stream
	// PARAMS: S - Stream to save to
	//		version - Version number, to identify out-of-date config file
	// RETURNS: True on success, else false
	bool Save(class fiStream *S,int version);

	// PURPOSE: Sets the keyboard enabled state.
	static inline void SetEnableKeyboard(bool);

	// PURPOSE: Gets the keyboard enabled state.
	static inline bool GetEnableKeyboard();

	// PURPOSE: Gets the mouse and keyboard enabled state.
	static inline bool GetEnabledKeyboardMouse();

#if !__FINAL
	// PURPOSE: These are all variations on the ioKeyboard() commands but they make sure
	// we're in "debug" keyboard mode first
	static inline int DebugKeyDown(int key);
	static inline int DebugKeyDown(int key1, int key2);
	static inline int DebugKeyUp(int key);
	static inline int DebugKeyChanged(int key);
	static inline int DebugKeyPressed(int key);
	static inline int DebugKeyReleased(int key);
#endif

	static inline void SafeAddSourceToList(const ioSource& source, ioSourceList& list);

private:
	static bool ScanKeyboardMouse(ioSource &source, const ScanOptions &options);
	static bool ScanPad(ioSource &source, const ScanOptions &options, s32 padIndex);

#if RSG_PC
	// Joystick are direct input devices.
	static bool ScanJoystick(ioSource &source, const ScanOptions &options, s32 joystickIndex);
#endif // RSG_PC

	int				m_Count,	// Number of inputs being tracked by this instance
					m_PadIndex;	// ioPad index to watch for pad inputs

#if RSG_EXTRA_MOUSE_SUPPORT
#if !ENABLE_INPUT_COMBOS
	ioValue::ValueType m_MouseCenteredX;			// The current x value for mouse centered type.
	ioValue::ValueType 	m_MouseCenteredY;			// The current y value for mouse centered type.
#endif // !ENABLE_INPUT_COMBOS

	float	m_MouseScale;			// The scale settings for mouse movement.
	u32		m_MouseWheelDuration;	// The duration each notch of the mouse wheel should produce an input (in milliseconds).

	u32		m_MouseWheelTimer;		// A timer timing each notch of the mouse wheel to produce an input (in milliseconds).
	s32		m_MouseWheelNotches;	// The number of notches the mouse wheel has moved and in which direction (i.e. +/- number of notches).
	u32		m_lastMS;				// The last frames update time (in milliseconds).
#endif // RSG_EXTRA_MOUSE_SUPPORT

	// PURPOSE: The Maximum number of mappings per ioMapper.
	//   This is constant to avoid dynamic memory requirement, so we
	//   don't have to worry about heap blowouts.
	enum
	{
		// PURPOSE:	The base number of mappings a mapper should support for a device.
		BaseNumMappings = 115,
#if RSG_PC
		// PURPUSE: The number of mappings supported.
		// NOTES:	BaseNumMappings * 6 is for two sets of keyboard mappings and controller devices (including direct input).
		MaxMappings = BaseNumMappings * 6,
#elif RSG_ORBIS
		// PURPUSE: The number of mappings supported.
		// NOTES:	The PS4 has gestures/touchpad so it needs more space than other consoles.
		MaxMappings = BaseNumMappings + 16,
#else
		// PURPUSE: The number of mappings supported.
		MaxMappings = BaseNumMappings,
#endif
	};

	ioMapperSource m_Sources[MaxMappings];	// Array of input sources (m_Count are valid)
	u32		m_Parameters[MaxMappings];		// Array of parameters for input sources (m_Count are valid)
	ioValue *m_Values[MaxMappings];			// Pointer to caller-owned value structures

	static bool sm_EnableKeyboard;
};

inline ioSource::ioSource( ioMapperSource device, u32 parameter, s32 padIndex )
	: m_Device(device)
	, m_Parameter(parameter)
	, m_DeviceIndex(padIndex)
{}
 
inline bool ioSource::IsValidDevice(s32 device)
{
	return device == IOMD_DEFAULT || device >= 0 || device == IOMD_KEYBOARD_MOUSE || device == IOMD_GAME;
}


inline ioValue::DisableOptions::DisableOptions()
	: m_DisableFlags(0)
{}

inline ioValue::DisableOptions::DisableOptions(u32 flags)
	: m_DisableFlags(flags)
{}

inline void ioValue::DisableOptions::SetFlags(u32 flags, bool enabled)
{
	if(enabled)
	{
		m_DisableFlags |= flags;
	}
	else
	{
		m_DisableFlags &= ~flags;
	}
}

inline bool ioValue::DisableOptions::IsFlagSet(u32 flags) const
{
	return (m_DisableFlags & flags) == flags;
}

inline void ioValue::DisableOptions::Merge(const DisableOptions& options)
{
	m_DisableFlags &= options.m_DisableFlags;
}

inline ioValue::ReadOptions::ReadOptions()
	: m_DeadZone(DEFAULT_DEAD_ZONE_VALUE)
	, m_ReadFlags(0)
{}

inline ioValue::ReadOptions::ReadOptions(float deadZone)
	: m_DeadZone(deadZone)
	, m_ReadFlags(0)
{
	Assertf(m_DeadZone >= 0.0f, "DeadZone cannot be negative!");
}

inline ioValue::ReadOptions::ReadOptions(u32 flags)
	: m_DeadZone(DEFAULT_DEAD_ZONE_VALUE)
	, m_ReadFlags(flags)
{}

inline ioValue::ReadOptions::ReadOptions(float deadZone, u32 flags)
	: m_DeadZone(deadZone)
	, m_ReadFlags(flags)
{
	Assertf(m_DeadZone >= 0.0f, "DeadZone cannot be negative!");
}

inline ioValue::ReadOptions::ReadOptions(Setup_DeafultUnboundOptions)
	: m_DeadZone(DEFAULT_DEAD_ZONE_VALUE)
	, m_ReadFlags(F_UNBOUND)
{}

inline ioValue::ReadOptions::ReadOptions(Setup_DeafultBoundOptions)
	: m_DeadZone(DEFAULT_DEAD_ZONE_VALUE)
	, m_ReadFlags(0)
{}

inline void ioValue::ReadOptions::SetFlags(u32 flags, bool enabled)
{
	if(enabled)
	{
		m_ReadFlags |= flags;
	}
	else
	{
		m_ReadFlags &= ~flags;
	}
}

inline bool ioValue::ReadOptions::IsFlagSet(u32 flags) const
{
	return (m_ReadFlags & flags) == flags;
}

inline ioValue::ioValue()
	: m_InvertValue(1)
	, m_Value(0)
	, m_LastValue(0)
	, m_Source(ioSource::UNKNOWN_SOURCE)
	, m_LastSource(ioSource::UNKNOWN_SOURCE)
	, m_IgnoreSource(IOMS_UNDEFINED)
	, m_LastUpdateID(0)
	, m_CurrentValueIndex(0)
	, m_enabled(true)
	, m_disableNextUpdate(false)
	, m_IgnoreThisUpdate(false)
	, m_DisableOptions()
	, m_HistoryValues(NULL)
#if __DEV
	, m_Id(0xffffffff)
#endif // __DEV
{}

inline void ioValue::SupportHistory()
{
	if(m_HistoryValues)
		return;

	m_HistoryValues = rage_new HistoryValue[MaxHistoryValues];
	for(u8 i = 0;i < MaxHistoryValues;++i)
	{
		m_HistoryValues[i].m_value = 0;
		m_HistoryValues[i].m_timeMS = 0;
	}
}



//////////////////////////////////////////////////////////////////////////
// DEPRECATED Old style value
//////////////////////////////////////////////////////////////////////////
inline u8 ioValue::GetOld8BitValue() const
{
	// resolution might not be as accurate but it is faster to only use ints.
	return (u8) ClampIt(static_cast<s32>(GetValue(DEFAULT_OPTIONS) / LEGACY_AXIS_SCALE + LEGACY_AXIS_SHIFT), 0, 255);
}

inline const ioSource& ioValue::GetSource() const 
{
	return m_Source;
}

inline const ioSource& ioValue::GetLastSource() const
{
	return m_LastSource;
}

inline float ioValue::Normalize(ValueType value) 
{
	return static_cast<float>(value) / MAX_AXIS_F; 
}

inline bool ioValue::RequiresDeadZone( const ioMapperSource source )
{
	return	(source < IOMS_MOUSE_ABSOLUTEAXIS || source > IOMS_MOUSE_WHEEL)
			&& source != IOMS_TOUCHPAD_ABSOLUTE_AXIS
			&& source != IOMS_TOUCHPAD_CENTERED_AXIS
			&& source != IOMS_GAME_CONTROLLED;
}

inline bool ioValue::RequiresDeadZone( const ioSource& source )
{
	return RequiresDeadZone(source.m_Device);
}

inline bool ioValue::IsInverted() const 
{ 
	AssertMsg(m_InvertValue == -1 || m_InvertValue == 1, "Invalid inversion value");
	return m_InvertValue == -1; 
}

inline void ioValue::SetInverted(bool invert)
{
#if RSG_EXTRA_MOUSE_SUPPORT
	m_InvertValue = (invert) ? -1.0f : 1.0f;
#else
	m_InvertValue = (invert) ? -1 : 1;
#endif // RSG_EXTRA_MOUSE_SUPPORT
}

inline void ioValue::MakeInverted() 
{ 
	m_InvertValue = -1; 
}

inline void ioValue::MakeNormal() 
{ 
	m_InvertValue = 1; 
}

inline void ioValue::Enable()
{
	m_enabled = true;
	m_disableNextUpdate = false;
	m_DisableOptions = DisableOptions();
}

inline bool ioValue::IsEnabled() const
{
	return m_enabled && !m_disableNextUpdate;
}

#if __DEV
inline void ioValue::SetId(u32 id)
{
	m_Id = id;
}

inline u32 ioValue::GetId() const
{
	return m_Id;
}
#endif // __DEV

inline void ioValue::Disable(const DisableOptions &options)
{
	if(IsEnabled())
	{
		m_DisableOptions = options;
	}
	else
	{
		m_DisableOptions.Merge(options);
	}
	m_enabled = false;
	m_disableNextUpdate = true;
}

inline void ioValue::Ignore(ioMapperSource source)
{
	m_IgnoreSource = source;
	m_IgnoreThisUpdate = true;
}

inline void ioMapper::SetPlayer(s32 player) 
{
	m_PadIndex = player;
}

inline s32 ioMapper::GetPlayer() const 
{
	return m_PadIndex;
}

inline void ioMapper::Reset() 
{
	m_Count					= 0;

#if RSG_EXTRA_MOUSE_SUPPORT
	// reset mouse scaling.
#if !ENABLE_INPUT_COMBOS
	m_MouseCenteredX		= 0;
	m_MouseCenteredY		= 0;
#endif // !ENABLE_INPUT_COMBOS

	m_MouseScale			= 4.0f;
	m_MouseWheelDuration	= 100;
	m_MouseWheelTimer		= 0;
	m_MouseWheelNotches		= 0;
	m_lastMS				= 0;
#endif // RSG_EXTRA_MOUSE_SUPPORT
}

inline void ioMapper::EndScan() 
{ 
}

inline int ioMapper::GetCount() const 
{ 
	return m_Count; 
}

inline ioMapperSource ioMapper::GetMapperSource(int i) const 
{ 
	return m_Sources[i]; 
}

inline unsigned ioMapper::GetParameter(int i) const 
{ 
	return m_Parameters[i]; 
}

inline ioValue* ioMapper::GetValue(int i) const 
{ 
	return m_Values[i]; 
}

inline int ioMapper::FindValue(ioValue &value) {
	for (int i=0; i<m_Count; i++)
		if (&value == m_Values[i])
			return i;
	return -1;
}

inline void ioMapper::SetParameter(int i,unsigned v) 
{
	m_Parameters[i] = v; 
}

#if RSG_EXTRA_MOUSE_SUPPORT
inline void ioMapper::SetMouseScale(const float scale)
{
	AssertMsg(scale > 0.0f, "Scale should not be used to invert the mouse axes!");
	m_MouseScale = scale;
}

inline float ioMapper::GetMouseScale() const
{
	return m_MouseScale;
}

inline void ioMapper::SetMouseWheelInputDuration(u32 timeMS)
{
	AssertMsg(timeMS > 0, "Duration for mouse wheel input must not be 0.");
	m_MouseWheelDuration = timeMS;
}

inline u32 ioMapper::GetMouseWheelInputDuration() const
{
	return m_MouseWheelDuration;
}

inline bool ioMapper::IsMouseXAxis(ioMapperParameter param)
{
	return param == IOM_AXIS_X ||
		param == IOM_IAXIS_X ||
		param == IOM_AXIS_X_LEFT ||
		param == IOM_AXIS_X_RIGHT;
}

inline bool ioMapper::IsMouseYAxis(ioMapperParameter param)
{
	return param == IOM_AXIS_Y ||
		param == IOM_IAXIS_Y ||
		param == IOM_AXIS_Y_UP ||
		param == IOM_AXIS_Y_DOWN;
}
#endif //RSG_EXTRA_MOUSE_SUPPORT

inline void ioMapper::SetEnableKeyboard(bool flag)
{
	sm_EnableKeyboard = flag;
}

inline bool ioMapper::GetEnableKeyboard()
{
	return sm_EnableKeyboard;
}

inline bool ioMapper::GetEnabledKeyboardMouse()
{
	// For now, just use the keyboard enabled state but this can be seperated.
	return GetEnableKeyboard();
}

#if !__FINAL
inline int ioMapper::DebugKeyDown(int key)
{
	return GetEnableKeyboard() ? 0 : ioKeyboard::KeyDown(key);
}

inline int ioMapper::DebugKeyDown(int key1, int key2)
{
	return GetEnableKeyboard() ? 0 : ioKeyboard::KeyDown(key1, key2);
}

inline int ioMapper::DebugKeyUp(int key)
{
	return GetEnableKeyboard() ? 1 : ioKeyboard::KeyUp(key);
}

inline int ioMapper::DebugKeyChanged(int key)
{
	return GetEnableKeyboard() ? 0 : ioKeyboard::KeyChanged(key);
}

inline int ioMapper::DebugKeyPressed(int key)
{
	return GetEnableKeyboard() ? 0 : ioKeyboard::KeyPressed(key);
}

inline int ioMapper::DebugKeyReleased(int key)
{
	return GetEnableKeyboard() ? 0 : ioKeyboard::KeyReleased(key);
}
#endif

inline void ioMapper::SafeAddSourceToList(const ioSource& source, ioSourceList& list)
{
	if(Verifyf(list.size() < list.max_size(), "Number of sources exceeded list limit! The list WILL be truncated to prevent a crash and WILL need increasing!"))
	{
		list.Push(source);
	}
}

inline bool ioMapper::IsMkbAxisPositiveAMouseButton(u32 param)
{
	return (param & IOMC_MKB_AXIS_POSITIVE_IS_MOUSE) == IOMC_MKB_AXIS_POSITIVE_IS_MOUSE;
}

inline bool ioMapper::IsMkbAxisNegativeAMouseButton(u32 param)
{
	return (param & IOMC_MKB_AXIS_NEGATIVE_IS_MOUSE) == IOMC_MKB_AXIS_NEGATIVE_IS_MOUSE;
}

inline bool ioMapper::IsMkbAxisPositiveAMouseWheel(u32 param)
{
	return (param & IOMC_MKB_AXIS_POSITIVE_IS_WHEEL) == IOMC_MKB_AXIS_POSITIVE_IS_WHEEL;
}

inline bool ioMapper::IsMkbAxisNegativeAMouseWheel(u32 param)
{
	return (param & IOMC_MKB_AXIS_NEGATIVE_IS_WHEEL) == IOMC_MKB_AXIS_NEGATIVE_IS_WHEEL;
}

}	// namespace rage

#endif
