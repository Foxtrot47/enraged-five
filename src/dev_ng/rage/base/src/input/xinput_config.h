//
// input/xinput_config.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_XINPUT_CONFIG_H
#define INPUT_XINPUT_CONFIG_H

// Change 0 to __WIN32PC to enable XInput on PC builds, but then only XInput controllers will work.
#define USE_XINPUT	(RSG_XENON || RSG_PC)

#endif	// INPUT_XINPUT_CONFIG
