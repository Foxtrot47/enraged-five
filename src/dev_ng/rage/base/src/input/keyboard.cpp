//
// input/keyboard.cpp
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//

#include "keyboard.h"
#include "keys.h"
#include "eventq.h"

#include "bank/packet.h"
#include "profile/telemetry.h"
#include "string/string.h"
#include "string/unicode.h"

#include "system/bootmgr.h"
#include "system/param.h"
#include "system/wndproc.h"

#if RSG_PC
#include "system/xtl.h"
#include "system/ipc.h"
#endif // RSG_PC

#if __WIN32
#include "winpriv.h"
#pragma comment(lib,"imm32.lib")
#elif __PPU
#include <cell/keyboard.h>
#include <cell/keyboard/kb_codes.h>
#define MAX_KEYBD 1
static CellKbInfo		sKeybdInfo;
static unsigned char	sOldKeybdStatus[MAX_KEYBD] = {0};
#pragma comment(lib, "io_stub")
#elif RSG_ORBIS
#include "pad.h"

#include <sdk_version.h>
#include <user_service.h>
#include <libsysmodule.h>
# pragma comment(lib, "SceSysmodule_stub_weak")

// NOTE: 0 so that we use the Ime library for all builds as the debug keyboard library does not work on test kits (url:bugstar:2192762)!
#define USE_DEBUG_KEYBOARD_LIBRARY (0 && !__FINAL)

#if USE_DEBUG_KEYBOARD_LIBRARY
int s_KeyboardHandle;
# include <dbg_keyboard.h>
# pragma comment(lib, "SceDbgKeyboard_stub_weak")
#else
# include <libime.h>
# pragma comment(lib, "SceIme_stub_weak")
#endif // USE_DEBUG_KEYBOARD_LIBRARY
namespace rage
{
extern SceUserServiceUserId g_initialUserId;
}
#endif

using namespace rage;

#define DISABLE_KEYBOARD_EXCEPT_ARROW_KEYS_FOR_CONTENT_CONTROLLED_BUILD (0)

unsigned char ioKeyboard::sm_Keys[2][256];
unsigned char ioKeyboard::sm_CurrentState[256];
int ioKeyboard::sm_Active;
bool ioKeyboard::sm_UseKeyboardInBackground = false;
char16 ioKeyboard::sm_TextBuffer[TEXT_BUFFER_LENGTH];
char16 ioKeyboard::sm_CurrentTextBuffer[TEXT_BUFFER_LENGTH];
ioMapperParameter ioKeyboard::sm_CurrentTextBufferKeys[TEXT_BUFFER_LENGTH];
u32  ioKeyboard::sm_CurrentLength = 0;

#if __PPU
bool ioKeyboard::sm_UseKeypadMode;
#endif // __PPU

#if RSG_ORBIS
bool ioKeyboard::sm_bShutdown = false;
bool ioKeyboard::sm_bStart = false;
#endif


#if __PPU || RSG_ORBIS

// usb to rage key 
// see	- http://www.usb.org/developers/devclass_docs/Hut1_11.pdf
//		- \usr\local\0_8_0\cell\target\ppu\include\cell\keyboard\kb_codes.h
static const char sKeyUsb2Ascii[128] = 
"    abcdefghijklmnopqrstuvwxyz1234567890\015\033\010\011 -=[]\\ ;'`,./"	// this needs work
"                           /*-+\r1234567890.";	// this needs work
static const char sKeyUsb2AsciiShifted[128] = 
"    ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()\015\033\010\011 _+{}| :\"~<>?";  // 0-58

static const unsigned char sKeyUsb2Rage[ 256 ] =
{
	0,						// USBKEYC_NO_EVENT 0x00
	0,						// USBKEYC_E_ROLLOVER 0x01
	0,						// USBKEYC_E_POSTFAIL 0x02
	0,						// USBKEYC_E_UNDEF 0x03
	KEY_A,					// USBKEYC_A 0x04
	KEY_B,					// USBKEYC_B 0x05
	KEY_C,					// USBKEYC_C 0x06
	KEY_D,					// USBKEYC_D 0x07
	KEY_E,					// USBKEYC_E 0x08
	KEY_F,					// USBKEYC_F 0x09
	KEY_G,					// USBKEYC_G 0x0A
	KEY_H,					// USBKEYC_H 0x0B
	KEY_I,					// USBKEYC_I 0x0C
	KEY_J,					// USBKEYC_J 0x0D
	KEY_K,					// USBKEYC_K 0x0E
	KEY_L,					// USBKEYC_L 0x0F
	KEY_M,					// USBKEYC_M 0x10
	KEY_N,					// USBKEYC_N 0x11
	KEY_O,					// USBKEYC_O 0x12
	KEY_P,					// USBKEYC_P 0x13
	KEY_Q,					// USBKEYC_Q 0x14
	KEY_R,					// USBKEYC_R 0x15
	KEY_S,					// USBKEYC_S 0x16
	KEY_T,					// USBKEYC_T 0x17
	KEY_U,					// USBKEYC_U 0x18
	KEY_V,					// USBKEYC_V 0x19
	KEY_W,					// USBKEYC_W 0x1A
	KEY_X,					// USBKEYC_X 0x1B
	KEY_Y,					// USBKEYC_Y 0x1C
	KEY_Z,					// USBKEYC_Z 0x1D
	KEY_1,					// USBKEYC_1 0x1E
	KEY_2,					// USBKEYC_2 0x1F
	KEY_3,					// USBKEYC_3 0x20
	KEY_4,					// USBKEYC_4 0x21
	KEY_5,					// USBKEYC_5 0x22
	KEY_6,					// USBKEYC_6 0x23
	KEY_7,					// USBKEYC_7 0x24
	KEY_8,					// USBKEYC_8 0x25
	KEY_9,					// USBKEYC_9 0x26
	KEY_0,					// USBKEYC_0 0x27
	KEY_RETURN,				// USBKEYC_ENTER 0x28
	KEY_ESCAPE,				// USBKEYC_ESC 0x29
	KEY_BACK,				// USBKEYC_BS 0x2A
	KEY_TAB,				// USBKEYC_TAB 0x2B
	KEY_SPACE,				// USBKEYC_SPACE 0x2C
	KEY_MINUS,				// USBKEYC_MINUS 0x2D
	KEY_EQUALS,				// USBKEYC_EQUAL_101 0x2E
	KEY_LBRACKET,			// USBKEYC_LEFT_BRACKET_101 0x2F
	KEY_RBRACKET,			// USBKEYC_RIGHT_BRACKET_101 0x30
	KEY_BACKSLASH,			// USBKEYC_BACKSLASH_101 0x31
	0,						// 0x32 - this is the #~ key on UK keyboards
	KEY_SEMICOLON,			// USBKEYC_SEMICOLON 0x33
	KEY_APOSTROPHE,			// USBKEYC_QUOTATION_101 0x34
	//KEY_COLON,				// USBKEYC_COLON_106 0x34
	KEY_GRAVE,				// USBKEYC_106_KANJI 0x35
	KEY_COMMA,				// USBKEYC_COMMA 0x36
	KEY_PERIOD,				// USBKEYC_PERIOD 0x37
	KEY_SLASH,				// USBKEYC_SLASH 0x38
	KEY_CAPITAL,			// USBKEYC_CAPS_LOCK 0x39
	KEY_F1,					// USBKEYC_F1 0x3a
	KEY_F2,					// USBKEYC_F2 0x3b
	KEY_F3,					// USBKEYC_F3 0x3c
	KEY_F4,					// USBKEYC_F4 0x3d
	KEY_F5,					// USBKEYC_F5 0x3e
	KEY_F6,					// USBKEYC_F6 0x3f
	KEY_F7,					// USBKEYC_F7 0x40
	KEY_F8,					// USBKEYC_F8 0x41
	KEY_F9,					// USBKEYC_F9 0x42
	KEY_F10,				// USBKEYC_F10 0x43
	KEY_F11,				// USBKEYC_F11 0x44
	KEY_F12,				// USBKEYC_F12 0x45
	KEY_SYSRQ,				// USBKEYC_PRINTSCREEN 0x46
	KEY_SCROLL,				// USBKEYC_SCROLL_LOCK 0x47
	KEY_PAUSE,				// USBKEYC_PAUSE 0x48
	KEY_INSERT,				// USBKEYC_INSERT 0x49
	KEY_HOME,				// USBKEYC_HOME 0x4a
	KEY_PAGEUP,				// USBKEYC_PAGE_UP 0x4b
	KEY_DELETE,				// USBKEYC_DELETE 0x4c
	KEY_END,				// USBKEYC_END 0x4d
	KEY_PAGEDOWN,			// USBKEYC_PAGE_DOWN 0x4e
	KEY_RIGHT,				// USBKEYC_RIGHT_ARROW 0x4f
	KEY_LEFT,				// USBKEYC_LEFT_ARROW 0x50
	KEY_DOWN,				// USBKEYC_DOWN_ARROW 0x51
	KEY_UP,					// USBKEYC_UP_ARROW 0x52
	KEY_NUMLOCK,			// USBKEYC_NUM_LOCK 0x53
	KEY_SLASH,				// USBKEYC_KPAD_SLASH 0x54
	KEY_MULTIPLY,			// USBKEYC_KPAD_ASTERISK 0x55
	KEY_SUBTRACT,			// USBKEYC_KPAD_MINUS 0x56
	KEY_ADD,				// USBKEYC_KPAD_PLUS 0x57
	KEY_NUMPADEQUALS,		// USBKEYC_KPAD_ENTER 0x58
	KEY_NUMPAD1,			// USBKEYC_KPAD_1 0x59
	KEY_NUMPAD2,			// USBKEYC_KPAD_2 0x5A
	KEY_NUMPAD3,			// USBKEYC_KPAD_3 0x5B
	KEY_NUMPAD4,			// USBKEYC_KPAD_4 0x5C
	KEY_NUMPAD5,			// USBKEYC_KPAD_5 0x5D
	KEY_NUMPAD6,			// USBKEYC_KPAD_6 0x5E
	KEY_NUMPAD7,			// USBKEYC_KPAD_7 0x5F
	KEY_NUMPAD8,			// USBKEYC_KPAD_8 0x60
	KEY_NUMPAD9,			// USBKEYC_KPAD_9 0x61
	KEY_NUMPAD0,			// USBKEYC_KPAD_0 0x62
	KEY_DECIMAL,			// USBKEYC_KPAD_PERIOD 0x63
	0,						// 0x64
	KEY_APPS,				// USBKEYC_APPLICATION 0x65
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // 34 0x66-0x87
	0, //KEY_KANA,				// USBKEYC_KANA        0x88
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 35 0x89-0xAB
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 35 0xAC-0xCE
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 35 0xCD-0xF1
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // 14 0xF2-0xFF
};
#endif // __PS2 || __PPU

// Durango needs to use WinRT so the relevant WinRT codes are in a separate file (keyboard_durango.cpp) that has WinRT enabled!
#if IS_CONSOLE && !RSG_DURANGO

#if RSG_ORBIS

#if SCE_ORBIS_SDK_VERSION < (0x01500111u)
#define SCE_IME_KEYCODE_STATE_MODIFIER_L_SHIFT SCE_IME_KEYCODE_STATE_MODIFIRE_L_SHIFT
#define SCE_IME_KEYCODE_STATE_MODIFIER_R_SHIFT SCE_IME_KEYCODE_STATE_MODIFIRE_R_SHIFT
#define SCE_IME_KEYCODE_STATE_MODIFIER_L_ALT SCE_IME_KEYCODE_STATE_MODIFIRE_L_ALT
#define SCE_IME_KEYCODE_STATE_MODIFIER_R_ALT SCE_IME_KEYCODE_STATE_MODIFIRE_R_ALT
#define SCE_IME_KEYCODE_STATE_MODIFIER_L_CTRL SCE_IME_KEYCODE_STATE_MODIFIRE_L_CTRL
#define SCE_IME_KEYCODE_STATE_MODIFIER_R_CTRL SCE_IME_KEYCODE_STATE_MODIFIRE_R_CTRL
#define SCE_IME_KEYCODE_STATE_MODIFIER_L_GUI SCE_IME_KEYCODE_STATE_MODIFIRE_L_GUI
#define SCE_IME_KEYCODE_STATE_MODIFIER_R_GUI SCE_IME_KEYCODE_STATE_MODIFIRE_R_GUI
#endif

#if !USE_DEBUG_KEYBOARD_LIBRARY
void ImeEventCallback(void* arg, const SceImeEvent* oEvent)
{
	switch (oEvent->id)
	{
	case SCE_IME_KEYBOARD_EVENT_CONNECTION:
		inputDisplayf( "Keyboard is connected[uid=0x%08x]:Resource ID(%d, %d, %d)", oEvent->param.resourceIdArray.userId, oEvent->param.resourceIdArray.resourceId[0], oEvent->param.resourceIdArray.resourceId[1], oEvent->param.resourceIdArray.resourceId[2] );
		break;
	case SCE_IME_KEYBOARD_EVENT_DISCONNECTION:
		inputDisplayf( "Keyboard is disconnected[uid=0x%08x]:Resource ID(%d, %d, %d)", oEvent->param.resourceIdArray.userId, oEvent->param.resourceIdArray.resourceId[0], oEvent->param.resourceIdArray.resourceId[1], oEvent->param.resourceIdArray.resourceId[2] );
		break;
	case SCE_IME_KEYBOARD_EVENT_ABORT:
		inputDisplayf( "Keyboard manager was aborted" );
		*(bool*)arg = true;
		break;
	case SCE_IME_KEYBOARD_EVENT_OPEN:
		// Processing when keyboard reception is started
		// Do not clear state as we only get events when state changes.
		break;
	case SCE_IME_KEYBOARD_EVENT_KEYCODE_DOWN:
		// fall through
	case SCE_IME_KEYBOARD_EVENT_KEYCODE_UP:
		{
			const SceImeKeycode &data = oEvent->param.keycode;

			// TODO: Only process ASCII characters?
			if (data.keycode <= 255)
			{
				// Processing when a key is pressed
				int key = sKeyUsb2Rage[data.keycode];

			#if DISABLE_KEYBOARD_EXCEPT_ARROW_KEYS_FOR_CONTENT_CONTROLLED_BUILD
				if (key == KEY_UP || key == KEY_DOWN || key == KEY_LEFT || key == KEY_RIGHT)
			#endif // DISABLE_KEYBOARD_EXCEPT_ARROW_KEYS_FOR_CONTENT_CONTROLLED_BUILD
				{
					if (data.character > 0 && data.character < 128 && oEvent->id == SCE_IME_KEYBOARD_EVENT_KEYCODE_DOWN)
					{
						// TODO: We should only do this for keydown, right?
						EVENTQ.Queue(ioEvent::IO_KEY_CHAR,0,0,data.character);
						ioKeyboard::SetTextCharPressed(data.character, (ioMapperParameter)key);
					}

					ioKeyboard::SetKey(KEY_LSHIFT, (data.status & SCE_IME_KEYCODE_STATE_MODIFIER_L_SHIFT) != 0);
					ioKeyboard::SetKey(KEY_RSHIFT, (data.status & SCE_IME_KEYCODE_STATE_MODIFIER_R_SHIFT) != 0);
					ioKeyboard::SetKey(KEY_LMENU, (data.status & SCE_IME_KEYCODE_STATE_MODIFIER_L_ALT) != 0);
					ioKeyboard::SetKey(KEY_RMENU, (data.status & SCE_IME_KEYCODE_STATE_MODIFIER_R_ALT) != 0);
					ioKeyboard::SetKey(KEY_LCONTROL, (data.status & SCE_IME_KEYCODE_STATE_MODIFIER_L_CTRL) != 0);
					ioKeyboard::SetKey(KEY_RCONTROL, (data.status & SCE_IME_KEYCODE_STATE_MODIFIER_R_CTRL) != 0);
					ioKeyboard::SetKey(KEY_LWIN, (data.status & SCE_IME_KEYCODE_STATE_MODIFIER_L_GUI) != 0);
					ioKeyboard::SetKey(KEY_RWIN, (data.status & SCE_IME_KEYCODE_STATE_MODIFIER_R_GUI) != 0);

					if (key > 0)
					{
						ioKeyboard::SetKey(key, (oEvent->id != SCE_IME_KEYBOARD_EVENT_KEYCODE_UP));
					}
				}
			}
		}
		break;
	default:
		// Do nothing
		break;
	}
}
#endif // !USE_DEBUG_KEYBOARD_LIBRARY
#endif

void ioKeyboard::Begin(bool)
{
#if __PPU
	memset(sOldKeybdStatus, 0, sizeof(sOldKeybdStatus));
	if (cellKbInit(MAX_KEYBD))
	{
		Quitf("cellKbInit failed");
	}
#elif RSG_ORBIS
#if USE_DEBUG_KEYBOARD_LIBRARY
	const bool isDevkit = sysBootManager::IsDevkit();
	if (!isDevkit && sceSysmoduleLoadModule(SCE_SYSMODULE_LIBIME) != SCE_OK)
	{
		Quitf("TestKit - cannot load IME module");
	}
	else if (isDevkit && (sceSysmoduleLoadModule(SCE_SYSMODULE_DEBUG_KEYBOARD) != SCE_OK || sceDbgKeyboardInit()))
	{
		Quitf("Cannot load keyboard module");
	}
#else
	if (sceSysmoduleLoadModule(SCE_SYSMODULE_LIBIME) != SCE_OK)
	{
		 Quitf("Cannot load keyboard module");
	}
#endif // USE_DEBUG_KEYBOARD_LIBRARY

	sm_bStart = true;
#endif
}

#if RSG_ORBIS
bool ioKeyboard::StartKeyboard()
{
#if USE_DEBUG_KEYBOARD_LIBRARY
	if (sysBootManager::IsDevkit())
	{
		s_KeyboardHandle = sceDbgKeyboardOpen(g_initialUserId,0,0,NULL);
		return s_KeyboardHandle >= 0;
	}
#else
	SceImeKeyboardParam keyParam;
	SceImeEventHandler onImeEvent = ImeEventCallback;
	sysMemSet(&keyParam, 0, sizeof(keyParam));

	keyParam.option = SCE_IME_KEYBOARD_OPTION_DEFAULT;
	keyParam.handler = onImeEvent;
	keyParam.arg = &sm_bShutdown;

	int ret = sceImeKeyboardOpen(g_initialUserId, &keyParam);
	// wait for application to become active again
	if (ret == SCE_IME_ERROR_NOT_ACTIVE)
	{
		return false;
	}
	else if (ret == SCE_IME_ERROR_INTERNAL)
	{
		Assertf( false, "Internal IME keyboard error - keyboard will not work until console is rebooted.");
	}
#endif // USE_DEBUG_KEYBOARD_LIBRARY

	return true;
}
#endif

void ioKeyboard::End()
{
#if __PPU
	if ( cellKbEnd() )
	{
		Quitf("cellKbEnd failed");
	}

	memset(sOldKeybdStatus, 0, sizeof(sOldKeybdStatus));
#elif RSG_ORBIS
#if USE_DEBUG_KEYBOARD_LIBRARY
	if (sysBootManager::IsDevkit())
		sceDbgKeyboardClose(s_KeyboardHandle);
#else
	sceImeKeyboardClose(g_initialUserId);
#endif // USE_DEBUG_KEYBOARD_LIBRARY
	sm_bShutdown = false;
#endif
}

void ioKeyboard::Update(bool UNUSED_PARAM(ignoreInput)) 
{
	sysMemCpy(sm_TextBuffer, sm_CurrentTextBuffer, sizeof(sm_TextBuffer));
	memset(sm_CurrentTextBuffer, 0, sizeof(sm_CurrentTextBuffer));
	sm_CurrentLength = 0;

#if __XENON
	XINPUT_KEYSTROKE keystroke;
	while (XInputGetKeystroke(XUSER_INDEX_ANY,XINPUT_FLAG_KEYBOARD,&keystroke) == ERROR_SUCCESS) {
		// inputDisplayf("keystroke: vk %d uni %d fl %x ui %d hid %d", keystroke.VirtualKey,keystroke.Unicode,keystroke.Flags,keystroke.UserIndex,keystroke.HidCode);

		static unsigned char s_UnicodeToEventKey[256] = {
			0,0,0,0,0,0,0,0,KEY_BACK,0,KEY_RETURN,0,0,0,0,0,0,0,0,0, // 0-19
			0,0,0,0,0,0,0,KEY_ESCAPE,0,0,0,0,KEY_SPACE,0,0,0,0,0,0,KEY_APOSTROPHE, // 20-39
			0,0,0,0,KEY_COMMA,KEY_MINUS,KEY_PERIOD,0,KEY_0,KEY_1,KEY_2,KEY_3,KEY_4,KEY_5,KEY_6,KEY_7,KEY_8,KEY_9,0,KEY_SEMICOLON, // 40-59
			0,KEY_EQUALS,0,0,0,KEY_A,KEY_B,KEY_C,KEY_D,KEY_E,KEY_F,KEY_G,KEY_H,KEY_I,KEY_J,KEY_K,KEY_L,KEY_M,KEY_N,KEY_O,  // 60-79
			KEY_P,KEY_Q,KEY_R,KEY_S,KEY_T,KEY_U,KEY_V,KEY_W,KEY_X,KEY_Y,KEY_Z,0,KEY_BACKSLASH,0,0,0,KEY_GRAVE,KEY_A,KEY_B,KEY_C, // 80-99
			KEY_D,KEY_E,KEY_F,KEY_G,KEY_H,KEY_I,KEY_J,KEY_K,KEY_L,KEY_M,KEY_N,KEY_O,KEY_P,KEY_Q,KEY_R,KEY_S,KEY_T,KEY_U,KEY_V,KEY_W, // 100-119
			KEY_X,KEY_Y,KEY_Z,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 120-139
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 140-159
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  //160-179
			0,0,0,0,0,0,KEY_SEMICOLON,KEY_EQUALS,KEY_COMMA,KEY_MINUS,KEY_PERIOD,KEY_SLASH,0,0,0,0,0,0,0,0,  //180-199
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  //200-219
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  //220-239
		};
		static unsigned char s_VirtualKeyToEventKey[256] = {
			0,0,0,0,0,0,0,0,KEY_BACK,KEY_TAB,0,0,0,KEY_RETURN,0,0,KEY_LSHIFT,KEY_LCONTROL,KEY_LMENU,KEY_PAUSE, // 0-19
			KEY_CAPITAL,0,0,0,0,0,0,KEY_ESCAPE,0,0,0,0,KEY_SPACE,KEY_PRIOR,KEY_NEXT,KEY_END,KEY_HOME,KEY_LEFT,KEY_UP,KEY_RIGHT, // 20-39
			KEY_DOWN,0,KEY_SYSRQ,0,0,KEY_INSERT,KEY_DELETE,0,KEY_0,KEY_1,KEY_2,KEY_3,KEY_4,KEY_5,KEY_6,KEY_7,KEY_8,KEY_9,0,0, // 40-59
			0,0,0,0,0,KEY_A,KEY_B,KEY_C,KEY_D,KEY_E,KEY_F,KEY_G,KEY_H,KEY_I,KEY_J,KEY_K,KEY_L,KEY_M,KEY_N,KEY_O,  // 60-79
			KEY_P,KEY_Q,KEY_R,KEY_S,KEY_T,KEY_U,KEY_V,KEY_W,KEY_X,KEY_Y,KEY_Z,KEY_LWIN,KEY_RWIN,KEY_APPS,0,0,KEY_NUMPAD0,KEY_NUMPAD1,KEY_NUMPAD2,KEY_NUMPAD3, // 80-99
			KEY_NUMPAD4,KEY_NUMPAD5,KEY_NUMPAD6,KEY_NUMPAD7,KEY_NUMPAD8,KEY_NUMPAD9,KEY_MULTIPLY,KEY_ADD,0,KEY_SUBTRACT,KEY_DECIMAL,KEY_DIVIDE,KEY_F1,KEY_F2,KEY_F3,KEY_F4,KEY_F5,KEY_F6,KEY_F7,KEY_F8, // 100-119
			KEY_F9,KEY_F10,KEY_F11,KEY_F12,KEY_F13,KEY_F14,KEY_F15,0,0,0,0,0,0,0,0,0,0,0,0,0, // 120-139
			0,0,0,0,KEY_NUMLOCK,KEY_SCROLL,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 140-159
			KEY_LSHIFT,KEY_RSHIFT,KEY_LCONTROL,KEY_RCONTROL,KEY_LMENU,KEY_RMENU,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  //160-179
			0,0,0,0,0,0,KEY_SEMICOLON,KEY_EQUALS,KEY_COMMA,KEY_MINUS,KEY_PERIOD,KEY_SLASH,KEY_GRAVE,0,0,0,0,0,0,0,  //180-199
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,KEY_LBRACKET,  //200-219
			KEY_BACKSLASH,KEY_RBRACKET,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  //220-239
		};
		int index=0;
		if(keystroke.VirtualKey == 0x5891)
			index = KEY_CHATPAD_GREEN_SHIFT;
		else if(keystroke.VirtualKey == 0x5892)
			index = KEY_CHATPAD_ORANGE_SHIFT;
		else if(keystroke.Unicode != 0)
			index = s_UnicodeToEventKey[keystroke.Unicode];
		// The following HID codes can be found at http://download.microsoft.com/download/1/6/1/161ba512-40e2-4cc9-843a-923143f3456c/translate.pdf
		else if(keystroke.VirtualKey == VK_CONTROL)
		{
			// 0xe0 is left, 0xe4 is right
			if(keystroke.HidCode == 0xe0)
			{
				index = KEY_LCONTROL;
			}
			else // assume 0xe4
			{
				index = KEY_RCONTROL;
			}
		}
		else if(keystroke.VirtualKey == VK_SHIFT)
		{
			// 0xe1 is left, 0xe5 is right
			if(keystroke.HidCode == 0xe1)
			{
				index = KEY_LSHIFT;
			}
			else // assume 0xe5
			{
				index = KEY_RSHIFT;
			}
		}
		else if(keystroke.VirtualKey == VK_MENU) // menu == alt
		{
			// 0xe2 is left, 0xe6 is right
			if(keystroke.HidCode == 0xe2)
			{
				index = KEY_LMENU;
			}
			else // assume 0xe6
			{
				index = KEY_RMENU;
			}
		}

		if(index == 0 && keystroke.VirtualKey < 256)
			index = s_VirtualKeyToEventKey[keystroke.VirtualKey];

#if DISABLE_KEYBOARD_EXCEPT_ARROW_KEYS_FOR_CONTENT_CONTROLLED_BUILD
		if (index == KEY_UP || index == KEY_DOWN || index == KEY_LEFT || index == KEY_RIGHT)
#endif // DISABLE_KEYBOARD_EXCEPT_ARROW_KEYS_FOR_CONTENT_CONTROLLED_BUILD
		{
			if ((keystroke.Flags & XINPUT_KEYSTROKE_KEYDOWN) && keystroke.Unicode < 128) {
			int code = keystroke.Unicode;
				if (!code && keystroke.VirtualKey >= 96 && keystroke.VirtualKey <= 111) // translate numpad
					code = "0123456789*+\r-./"[keystroke.VirtualKey - 96];
				EVENTQ.Queue(ioEvent::IO_KEY_CHAR,0,0,code);
				SetTextCharPressed((char16)code, (ioMapperParameter)index);
			}

			if (index) {
				if (keystroke.Flags & XINPUT_KEYSTROKE_KEYDOWN)
					sm_CurrentState[index] = 0x80;
				else if (keystroke.Flags & XINPUT_KEYSTROKE_KEYUP)
					sm_CurrentState[index] = 0;
			}
		}
	}
#endif

#if __PPU
	cellKbGetInfo(&sKeybdInfo);

	for (int device = 0; device < MAX_KEYBD; ++device) 
	{
		if (sKeybdInfo.status[device])
		{
			if (sOldKeybdStatus[device] == 0)
			{
				// keyboard acquired
				cellKbSetLEDStatus(device, 0);
				cellKbSetCodeType(device, CELL_KB_CODETYPE_RAW);
				SetUseKeypadMode(sm_UseKeypadMode, true);
				cellKbClearBuf(device);
			}

			sOldKeybdStatus[device] = sKeybdInfo.status[device];

			CellKbData data;
			if (cellKbRead(device, &data) || !data.len)
				continue;

			int i;
			if (data.len > 0)
				memset(sm_CurrentState,0,sizeof(sm_CurrentState));

			sm_CurrentState[ KEY_LMENU ] = (data.mkey & CELL_KB_MKEY_L_ALT)? 0x80 : 0x00;
			sm_CurrentState[ KEY_RMENU ] = (data.mkey & CELL_KB_MKEY_R_ALT)? 0x80 : 0x00;
			sm_CurrentState[ KEY_LCONTROL ] = (data.mkey & CELL_KB_MKEY_L_CTRL)? 0x80 : 0x00;
			sm_CurrentState[ KEY_RCONTROL ] = (data.mkey & CELL_KB_MKEY_R_CTRL)? 0x80 : 0x00;
			sm_CurrentState[ KEY_LSHIFT ] = (data.mkey & CELL_KB_MKEY_L_SHIFT)? 0x80 : 0x00;
			sm_CurrentState[ KEY_RSHIFT ] = (data.mkey & CELL_KB_MKEY_R_SHIFT)? 0x80 : 0x00;
			sm_CurrentState[ KEY_LWIN ] = (data.mkey & CELL_KB_MKEY_L_WIN)? 0x80 : 0x00;
			sm_CurrentState[ KEY_RWIN ] = (data.mkey & CELL_KB_MKEY_R_WIN)? 0x80 : 0x00;

			for (i = 0; i < data.len; ++i)
			{
				int key = sKeyUsb2Rage[data.keycode[i] & 255];

#if DISABLE_KEYBOARD_EXCEPT_ARROW_KEYS_FOR_CONTENT_CONTROLLED_BUILD
				if (key == KEY_UP || key == KEY_DOWN || key == KEY_LEFT || key == KEY_RIGHT)
#endif // DISABLE_KEYBOARD_EXCEPT_ARROW_KEYS_FOR_CONTENT_CONTROLLED_BUILD
				{
					if (data.keycode[i] && data.keycode[i] < 128)
					{
						int ascii = data.mkey & (CELL_KB_MKEY_L_SHIFT | CELL_KB_MKEY_R_SHIFT)? 
							sKeyUsb2AsciiShifted[data.keycode[i]] : sKeyUsb2Ascii[data.keycode[i]];
						// inputDisplayf("key %d ascii %d [%c]",data.keycode[i],ascii,ascii);
						EVENTQ.Queue(ioEvent::IO_KEY_CHAR,0,0,ascii);
						SetTextCharPressed((char16)ascii, (ioMapperParameter)key);
					}
					
					if (key)
						sm_CurrentState[key] = 0x80;
				}
			}
		}
		else
		{
			// keyboard lost
			sOldKeybdStatus[device] = 0;

			//don't reset because of Rag connectivity
			//for(int i = 0; i < 256; ++i)
			//{
			//	sm_CurrentState[i] = 0x00;
			//}
		}
	}
#endif

#if RSG_ORBIS
	if (sm_bStart && StartKeyboard())
	{
		sm_bStart = false;
	}

	if (sm_bShutdown)
	{
		End();
		sm_bStart = true;
	}

#if USE_DEBUG_KEYBOARD_LIBRARY
	if (sysBootManager::IsDevkit())
	{
		const int MAX_NUM_KEY_CODES = SCE_DBG_KEYBOARD_MAX_KEYCODES;

		static SceDbgKeyboardData keyData[MAX_NUM_KEY_CODES];
		static int numKeyCodes = 0;
		static int index = 0;
	
		// We get the keyboard history in blocks and go one a frame/call to Update(). We do this so that we can
		// receive keyboard events during an assert (url:bugstar:1750162 which is for url:bugstar:1734231).
		if(index >= numKeyCodes)
		{
			numKeyCodes = sceDbgKeyboardRead(s_KeyboardHandle, keyData, MAX_NUM_KEY_CODES);
			index = 0;
		}

		if (index < numKeyCodes && keyData[index].length > 0 && keyData[index].connected) 
		{
			SceDbgKeyboardData& data = keyData[index];

			// Clear the keyboard state when we have a key event. All currently held down keys will be marked as pressed below.
 			memset(sm_CurrentState,0,sizeof(sm_CurrentState));

			sm_CurrentState[ KEY_LMENU ] = (data.modifierKey & SCE_DBG_KEYBOARD_MKEY_L_ALT)? 0x80 : 0x00;
			sm_CurrentState[ KEY_RMENU ] = (data.modifierKey & SCE_DBG_KEYBOARD_MKEY_R_ALT)? 0x80 : 0x00;
			sm_CurrentState[ KEY_LCONTROL ] = (data.modifierKey & SCE_DBG_KEYBOARD_MKEY_L_CTRL)? 0x80 : 0x00;
			sm_CurrentState[ KEY_RCONTROL ] = (data.modifierKey & SCE_DBG_KEYBOARD_MKEY_R_CTRL)? 0x80 : 0x00;
			sm_CurrentState[ KEY_LSHIFT ] = (data.modifierKey & SCE_DBG_KEYBOARD_MKEY_L_SHIFT)? 0x80 : 0x00;
			sm_CurrentState[ KEY_RSHIFT ] = (data.modifierKey & SCE_DBG_KEYBOARD_MKEY_R_SHIFT)? 0x80 : 0x00;
			sm_CurrentState[ KEY_LWIN ] = (data.modifierKey & SCE_DBG_KEYBOARD_MKEY_L_WIN)? 0x80 : 0x00;
			sm_CurrentState[ KEY_RWIN ] = (data.modifierKey & SCE_DBG_KEYBOARD_MKEY_R_WIN)? 0x80 : 0x00;

			for (int i=0; i<data.length; i++) {
				if (!data.keyCode[i])
				{
					continue;
				}

				int key = sKeyUsb2Rage[data.keyCode[i] & 255];

	#if DISABLE_KEYBOARD_EXCEPT_ARROW_KEYS_FOR_CONTENT_CONTROLLED_BUILD
				if (key == KEY_UP || key == KEY_DOWN || key == KEY_LEFT || key == KEY_RIGHT)
	#endif // DISABLE_KEYBOARD_EXCEPT_ARROW_KEYS_FOR_CONTENT_CONTROLLED_BUILD
				{
					// inputDisplayf("key code %x mod %x",data.keyCode[i],data.modifierKey);
					if (data.keyCode[i] && data.keyCode[i] < 128)
					{
						int ascii = data.modifierKey & (SCE_DBG_KEYBOARD_MKEY_L_SHIFT | SCE_DBG_KEYBOARD_MKEY_R_SHIFT)? 
							sKeyUsb2AsciiShifted[data.keyCode[i]] : sKeyUsb2Ascii[data.keyCode[i]];
						// inputDisplayf("key %d ascii %d [%c]",data.keyCode[i],ascii,ascii);
						EVENTQ.Queue(ioEvent::IO_KEY_CHAR,0,0,ascii);

						SceDbgKeyboardCharData charData;
						if (sceDbgKeyboardGetKey2Char(s_KeyboardHandle, SCE_DBG_KEYBOARD_MAPPING_101, data.led, data.modifierKey, data.keyCode[i], &charData) == SCE_OK &&
							charData.processed &&
							key && sm_Keys[sm_Active][key] == 0 )
						{
							SetTextCharPressed(charData.charCode, (ioMapperParameter)key);
						}
					}

					if (key)
						sm_CurrentState[key] = 0x80;
				}
			}
		}

		++index;
	}
#else
	SceImeEventHandler onImeEvent = ImeEventCallback;
	int ret = sceImeUpdate(onImeEvent);
	if (ret != SCE_OK)
	{
		// TODO: error handling?
	}
#endif // USE_DEBUG_KEYBOARD_LIBRARY

	// On PS4 we still receive input when the virtual keyboard is shown. We don't want this. We use pad intercepted
	// rather than virtual keyboard showing to catch any other cases where this might happen.
	if(ioPad::IsIntercepted())
	{
		sysMemSet(sm_CurrentState, 0, 256);
	}
#endif // RSG_ORBIS

	sysMemCpy(sm_Keys[sm_Active^1],sm_CurrentState,256);
	sm_Active ^= 1;
}

#if __PPU
void ioKeyboard::SetUseKeypadMode(bool keypadMode, bool forceUpdate)
{
	if (forceUpdate || keypadMode != sm_UseKeypadMode)
	{
		cellKbSetReadMode(0, (keypadMode) ? CELL_KB_RMODE_PACKET : CELL_KB_RMODE_INPUTCHAR);
		sm_UseKeypadMode = keypadMode;
	}
}
#endif // __PPU


#elif RSG_PC // IS_CONSOLE && !RSG_DURANGO
static LPDIRECTINPUTW s_keybDI;
bool ioKeyboard::sm_UsingRawInput = false;
bool ioKeyboard::sm_CapsLockToggled = false;
bool ioKeyboard::sm_AllowLocalKeyboardLayout = false;
bool ioKeyboard::sm_DiDeviceLost = false;
bool ioKeyboard::sm_IgnoreInput = false;
LPDIRECTINPUTDEVICEW g_keybDev;

const int KB_BUFF_SIZE = 32;

XPARAM(usedi);
XPARAM(ragUseOwnWindow);
namespace rage
{
	XPARAM(setHwndMain);
}
PARAM(useRawKeyboard, "[input] Use raw input for keyboard.");
PARAM(useWindowsKeyboard, "[Input] Use windows messages for keyboard.");

NOSTRIP_PC_PARAM(internationalKeyboardMode, "[input] Use international keyboard mode that loads the US keyboard.");
NOSTRIP_PC_PARAM(noInternationalKeyboardMode, "[input] Do not use international keyboard mode that loads the US keyboard.");

#if __DEV
PARAM(noBkgInput, "[input] Normally __DEV PC builds will get keyboard input even when the window is in the background. Use this to disable background key presses.");
PARAM(allowbackgroundinput, "[input] Force Accept keyboard and controller input when the process doesn't have focus.");
#endif

// NOTE: These are static here so that the header for HKL does not need to be included in this header.
static HKL s_USKeyboardLayout = 0;
static HKL s_LocalKeyboardLayout = 0;
static bool s_CleanupKeyboardLayout = false;

static const unsigned char sKeyDI2Rage[ 256 ] =
{
	0,
	KEY_ESCAPE,				// DIK_ESCAPE 0x01
	KEY_1,					// DIK_1 0x02
	KEY_2,					// DIK_2 0x03
	KEY_3,					// DIK_3 0x04
	KEY_4,					// DIK_4 0x05
	KEY_5,					// DIK_5 0x06
	KEY_6,					// DIK_6 0x07
	KEY_7,					// DIK_7 0x08
	KEY_8,					// DIK_8 0x09
	KEY_9,					// DIK_9 0x0A
	KEY_0,					// DIK_0 0x0B
	KEY_MINUS,				// DIK_MINUS 0x0C
	KEY_EQUALS,				// DIK_EQUALS 0x0D
	KEY_BACK,				// DIK_BACK 0x0E
	KEY_TAB,				// DIK_TAB 0x0F
	KEY_Q,					// DIK_Q 0x10
	KEY_W,					// DIK_W 0x11
	KEY_E,					// DIK_E 0x12
	KEY_R,					// DIK_R 0x13
	KEY_T,					// DIK_T 0x14
	KEY_Y,					// DIK_Y 0x15
	KEY_U,					// DIK_U 0x16
	KEY_I,					// DIK_I 0x17
	KEY_O,					// DIK_O 0x18
	KEY_P,					// DIK_P 0x19
	KEY_LBRACKET,			// DIK_LBRACKET 0x1A
	KEY_RBRACKET,			// DIK_RBRACKET 0x1B
	KEY_RETURN,				// DIK_RETURN 0x1C
	KEY_LCONTROL,			// DIK_LCONTROL 0x1D
	KEY_A,					// DIK_A 0x1E
	KEY_S,					// DIK_S 0x1F
	KEY_D,					// DIK_D 0x20
	KEY_F,					// DIK_F 0x21
	KEY_G,					// DIK_G 0x22
	KEY_H,					// DIK_H 0x23
	KEY_J,					// DIK_J 0x24
	KEY_K,					// DIK_K 0x25
	KEY_L,					// DIK_L 0x26
	KEY_SEMICOLON,			// DIK_SEMICOLON 0x27
	KEY_APOSTROPHE,			// DIK_APOSTROPHE 0x28
	KEY_GRAVE,				// DIK_GRAVE 0x29
	KEY_LSHIFT,				// DIK_LSHIFT 0x2A
	KEY_BACKSLASH,			// DIK_BACKSLASH 0x2B
	KEY_Z,					// DIK_Z 0x2C
	KEY_X,					// DIK_X 0x2D
	KEY_C,					// DIK_C 0x2E
	KEY_V,					// DIK_V 0x2F
	KEY_B,					// DIK_B 0x30
	KEY_N,					// DIK_N 0x31
	KEY_M,					// DIK_M 0x32
	KEY_COMMA,				// DIK_COMMA 0x33
	KEY_PERIOD,				// DIK_PERIOD 0x34
	KEY_SLASH,				// DIK_SLASH 0x35
	KEY_RSHIFT,				// DIK_RSHIFT 0x36
	KEY_MULTIPLY,			// DIK_MULTIPLY 0x37
	KEY_LMENU,				// DIK_LMENU 0x38
	KEY_SPACE,				// DIK_SPACE 0x39
	KEY_CAPITAL,			// DIK_CAPITAL 0x3a
	KEY_F1,					// DIK_F1 0x3b
	KEY_F2,					// DIK_F2 0x3c
	KEY_F3,					// DIK_F3 0x3d
	KEY_F4,					// DIK_F4 0x3e
	KEY_F5,					// DIK_F5 0x3f
	KEY_F6,					// DIK_F6 0x40
	KEY_F7,					// DIK_F7 0x41
	KEY_F8,					// DIK_F8 0x42
	KEY_F9,					// DIK_F9 0x43
	KEY_F10,				// DIK_F10 0x44
	KEY_NUMLOCK,			// DIK_NUMLOCK 0x45
	KEY_SCROLL,				// DIK_SCROLL 0x46
	KEY_NUMPAD7,			// DIK_NUMPAD7 0x47
	KEY_NUMPAD8,			// DIK_NUMPAD8 0x48
	KEY_NUMPAD9,			// DIK_NUMPAD9 0x49
	KEY_SUBTRACT,			// DIK_SUBTRACT 0x4a
	KEY_NUMPAD4,			// DIK_NUMPAD4 0x4b
	KEY_NUMPAD5,			// DIK_NUMPAD5 0x4c
	KEY_NUMPAD6,			// DIK_NUMPAD6 0x4d
	KEY_ADD,				// DIK_ADD 0x4e
	KEY_NUMPAD1,			// DIK_NUMPAD1 0x4f
	KEY_NUMPAD2,			// DIK_NUMPAD2 0x50
	KEY_NUMPAD3,			// DIK_NUMPAD3 0x51
	KEY_NUMPAD0,			// DIK_NUMPAD0 0x52
	KEY_DECIMAL,			// DIK_DECIMAL 0x53
	0, 0, // 2 0x54-0x55
	KEY_OEM_102,			// DIK_OEM_102 0x56 - <> or \| on RT 102-key keyboard (Non-U.S.)
	KEY_F11,				// DIK_F11 0x57
	KEY_F12,				// DIK_F12 0x58
	0, 0, 0, 0,	0, 0, 0, 0, 0, 0, 0, // 11 0x59-0x63
	KEY_F13,				// DIK_F13 0x64
	KEY_F14,				// DIK_F14 0x65
	KEY_F15,				// DIK_F15 0x66
	0, 0, 0, 0,	0, 0, 0, 0, 0, // 8 0x67-0x6F
	KEY_RAGE_EXTRA4,			// DIK_KANA 0x70
	0, 0, // 2 0x71-0x72
	KEY_RAGE_EXTRA1,			// DIK_ABNT_C1 0x73
	0, 0, 0, 0, 0, // 5 0x74-0x78
	KEY_RAGE_EXTRA3,			// DIK_CONVERT 0x79
	0, // 1 0x7A
	KEY_RAGE_EXTRA2,			// DIK_NOCONVERT 0x7B
	0, // 1 0x7C
	KEY_RAGE_EXTRA1,			// DIK_YEN 0x7D
	0,						// DIK_ABNT_C2 0x7E
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 15 0x7F-0x8C
	KEY_NUMPADEQUALS,		// DIK_NUMPADEQUALS 0x8D
	0, 0, // 2 0x8E-0x8F
	KEY_PLUS,					// NOTE: This is not KEY_PLUS but it doesn't exist on Japanese keyboards and is in the same place so re-use it. IK_PREVTRACK (DIK_CIRCUMFLEX) on Japanese keyboards 0x90
	KEY_OEM_102,				// NOTE: This is NOT KEY_OEM_102 but it doesn't exist on Japanese keyboards so re-use the code. // DIK_AT 0x91
	KEY_APOSTROPHE,				// NOTE: This is NOT KEY_APOSTROPHE but it doesn't exist on Japanese keyboards so re-use the code. // DIK_COLON 0x92
	0,						// DIK_UNDERLINE 0x93
	KEY_GRAVE,				// This would be KEY_KANJI but is the same key as KEY_GRAVE. DIK_KANJI 0x94
	0,						// DIK_STOP 0x95
	0, //KEY_AX,					// DIK_AX 0x96
	0,						// DIK_UNLABELED 0x97
	0, // 1 0x98
	0, //KEY_NEXTTRACK,			// DIK_NEXTTRACK 0x99
	0, 0, // 2 0x9A-0x9B
	KEY_NUMPADENTER,		// DIK_NUMPADENTER 0x9C
	KEY_RCONTROL,	 		// DIK_RCONTROL 0x9D
	0, 0, // 2 0x9E-0x9F
	0, //KEY_MUTE,		 		// DIK_MUTE 0xA0
	0,						// DIK_CALCULATOR 0xA1
	0, //KEY_PLAYPAUSE,			// DIK_PLAYPAUSE 0xA2
	0, // 1 0xA3
	0, //KEY_MEDIASTOP,			// DIK_MEDIASTOP 0xA4
	0, 0, 0, 0, 0, 0, 0, 0, 0, // 0xA5-0xAD
	0, //KEY_VOLUMEDOWN,			// DIK_VOLUMEDOWN 0xAE
	0, // 1 0xAF
	0, //KEY_VOLUMEUP,			// DIK_VOLUMEUP 0xB0
	0, // 1 0xB1
	0, //KEY_WEBHOME,			// DIK_WEBHOME 0xB2
	0,						// DIK_NUMPADCOMMA 0xB3
	0, // 1 0xB4
	KEY_DIVIDE,				// DIK_DIVIDE 0xB5
	0, // 1 0xB6
	KEY_SYSRQ,				// DIK_SYSRQ 0xB7
	KEY_RMENU,				// DIK_RMENU 0xB8
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 12 0xB9-0xC4
	KEY_PAUSE,				// DIK_PAUSE 0xC5
	0, // 1 0xC6
	KEY_HOME,				// DIK_HOME 0xC7
	KEY_UP,					// DIK_UP 0xC8
	KEY_PRIOR,				// DIK_PRIOR 0xC9
	0, // 1 OxCA
	KEY_LEFT,				// DIK_LEFT 0xCB
	0, // 1 0xCC
	KEY_RIGHT,				// DIK_RIGHT 0xCD
	0, // 1 0xCE
	KEY_END,				// DIK_END 0xCF
	KEY_DOWN,				// DIK_DOWN 0xD0
	KEY_NEXT,				// DIK_NEXT 0xD1
	KEY_INSERT,				// DIK_INSERT 0xD2
	KEY_DELETE,				// DIK_DELETE 0xD3
	0, 0, 0, 0, 0, 0, 0, // 7 0xD4-0xDA
	KEY_LWIN,				// DIK_LWIN 0xDB
	KEY_RWIN,				// DIK_RWIN 0xDC
	KEY_APPS,				// DIK_APPS 0xDD
	0,						// DIK_POWER 0xDE
	0, //KEY_SLEEP,				// DIK_SLEEP 0xDF
	0, 0, 0, // 3 0xE0-0xE2
	0,						// DIK_WAKE 0xE3
	0, // 1 0xE4
	0, //KEY_WEBSEARCH,			// DIK_WEBSEARCH 0xE5
	0, //KEY_WEBFAVORITES,		// DIK_WEBFAVORITES 0xE6
	0, //KEY_WEBREFRESH,			// DIK_WEBREFRESH 0xE7
	0, //KEY_WEBSTOP,			// DIK_WEBSTOP 0xE8
	0, //KEY_WEBFORWARD,			// DIK_WEBFORWARD 0xE9
	0, //KEY_WEBBACK,			// DIK_WEBBACK 0xEA
	0,						// DIK_MYCOMPUTER 0xEB
	0, //KEY_MAIL,				// DIK_MAIL 0xEC
	0, //KEY_MEDIASELECT,		// DIK_MEDIASELECT 0xED
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // 18 0xEE-0xFF
};


unsigned char ioKeyboard::GetRageKeyCode(u32 scanCode)
{
	if(s_USKeyboardLayout != 0)
	{
		return (unsigned char)MapVirtualKeyExW(scanCode, MAPVK_VSC_TO_VK, s_USKeyboardLayout);
	}
	else
	{
		return (unsigned char)MapVirtualKeyW(scanCode, MAPVK_VSC_TO_VK);
	}
}

void ioKeyboard::Begin(bool) {
	IME_TEXT_INPUT_ONLY(ImeBegin());

	sm_TextBuffer[0]        = '\0';
	sm_CurrentTextBuffer[0] = '\0';
	sm_CurrentLength        = 0;
	sm_UsingRawInput        = false;
	sm_CapsLockToggled      = false;

	bool s_LoadUsKeyboard = false;

	s_LocalKeyboardLayout = GetKeyboardLayout(0);

	DWORD primaryLang = PRIMARYLANGID(GetSystemDefaultUILanguage());
	s_LoadUsKeyboard  = !PARAM_noInternationalKeyboardMode.Get() && 
		(primaryLang == LANG_JAPANESE || primaryLang == LANG_CHINESE_TRADITIONAL || primaryLang == LANG_CHINESE_SIMPLIFIED || primaryLang == LANG_KOREAN);

	if (PARAM_internationalKeyboardMode.Get() || s_LoadUsKeyboard || PARAM_useRawKeyboard.Get() || PARAM_useWindowsKeyboard.Get())
	{
		Assertf(s_USKeyboardLayout == 0, "A keyboard layout is already loaded!");

		// Check if the US keyboard layout is loaded.
		int numLayouts = GetKeyboardLayoutList(0, NULL);
		HKL* layouts = Alloca(HKL, numLayouts);
		if(layouts != NULL)
		{
			numLayouts = GetKeyboardLayoutList(numLayouts, layouts);
		}

		// This number returned from MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US) as a string.
		const wchar_t* AMERICAN_KEYBOARD_LAYOUT = L"00000409";
		s_USKeyboardLayout = LoadKeyboardLayoutW(AMERICAN_KEYBOARD_LAYOUT, 0);
		Assertf(s_USKeyboardLayout != 0, "Failed to get keyboard layout!");

		s_CleanupKeyboardLayout = true; 

		// Check if the US keyboard layout was already loaded, if so, we do not remove it later.
		for(int i = 0; i < numLayouts; ++i)
		{
			if(s_USKeyboardLayout == layouts[i])
			{
				s_CleanupKeyboardLayout = false;
				break;
			}
		}
	}

	if(PARAM_useRawKeyboard.Get() || PARAM_useWindowsKeyboard.Get())
	{
#if __BANK
		// use old windows messages way to calculate relative mouse movement.
		if((bkRemotePacket::IsConnected() || PARAM_setHwndMain.Get()) && !PARAM_ragUseOwnWindow.Get())
		{
			return;
		}
#endif // __BANK

		RAWINPUTDEVICE rid;

		rid.usUsagePage = 0x01; 
		rid.usUsage = 0x06; 
		rid.dwFlags = 0;
		rid.hwndTarget = 0;


		// If this fails then we will revert back to the old legacy windows messages.
		if(PARAM_useRawKeyboard.Get() && inputVerifyf(RegisterRawInputDevices(&rid, 1, sizeof(rid)) != FALSE, "failed to use raw input, reverting to legacy windows messages!"))
		{
			sm_UsingRawInput = true;
		}
		return;
	}

#if __BANK
	// rag will send input via event queue:
	if ((bkRemotePacket::IsConnectedToRag() || PARAM_setHwndMain.Get()) && !PARAM_ragUseOwnWindow.Get())
		return;
#endif

	diInit();
	(s_keybDI = lpDI)->AddRef();

	ditry(s_keybDI->CreateDevice(GUID_SysKeyboard,&g_keybDev,NULL));

	// this doesn't actually appear to be necessary after all.
	// also removes a cross-dependency on gfx.
	// but is required to avoid choppiness under 2000?
	HWND hwndTop=g_hwndMain;
	while (GetParent(hwndTop)) {
		hwndTop=GetParent(hwndTop);
	}

#if __DEV

	bool allowBackgroundAccess = sm_UseKeyboardInBackground || PARAM_allowbackgroundinput.Get();
	if (PARAM_noBkgInput.Get()) {
		allowBackgroundAccess = false;
	}

	ditry(g_keybDev->SetCooperativeLevel(hwndTop, (allowBackgroundAccess ? DISCL_BACKGROUND : DISCL_FOREGROUND) | DISCL_NONEXCLUSIVE));
#else
    ditry(g_keybDev->SetCooperativeLevel(hwndTop, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE));
#endif

	ditry(g_keybDev->SetDataFormat(&c_dfDIKeyboard));

    DIPROPDWORD   dipdw;
	dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
	dipdw.diph.dwHeaderSize = sizeof(dipdw.diph);
	dipdw.diph.dwHow        = DIPH_DEVICE;
	dipdw.diph.dwObj        = 0;
	dipdw.dwData            = KB_BUFF_SIZE;

	ditry(g_keybDev->SetProperty(DIPROP_BUFFERSIZE,  &dipdw.diph));

	if(g_keybDev->Acquire() != DI_OK)
	{
		sm_DiDeviceLost = true;
	}
}

void ioKeyboard::End() {
	IME_TEXT_INPUT_ONLY(ImeEnd());

	if (g_keybDev) {
		g_keybDev->Unacquire();
		g_keybDev->Release(); 
		g_keybDev = 0;
	}
	if (s_keybDI) {
		s_keybDI->Release();
		s_keybDI = 0;
	}

	if(s_USKeyboardLayout != 0 && s_CleanupKeyboardLayout)
	{
		UnloadKeyboardLayout(s_USKeyboardLayout);
		s_USKeyboardLayout = 0;
	}

	s_CleanupKeyboardLayout = false;
}

void ioKeyboard::Update(bool ignoreInput) {

	// We still want to do this when ignoreInput is true as this is handled by the system and it insures our structures are in sync.
	IME_TEXT_INPUT_ONLY(ImeUpdate());

	sm_IgnoreInput = ignoreInput;
	SetKeyboardLayoutForThread();

	if(ignoreInput)
	{
		FlushKeyboard();
		return;
	}

	sysMemCpy(sm_TextBuffer, sm_CurrentTextBuffer, sizeof(sm_TextBuffer));
	memset(sm_CurrentTextBuffer, 0, sizeof(sm_CurrentTextBuffer));
	sm_CurrentLength = 0;

	if(!g_keybDev)
	{
		bool hasFocus = (::GetForegroundWindow() == g_hwndMain);
#if __BANK
		if(!hasFocus)
		{
			// might be in rag so see if the parent window has focus.
			HWND window = ::GetParent(g_hwndMain);
			while(!hasFocus && window != NULL)
			{
				hasFocus = (::GetForegroundWindow() == window);
				window = ::GetParent(window);
			}
		}
#endif // __BANK

		if(!hasFocus)
		{
			memset(sm_CurrentState,0,256);
		}
		sysMemCpy(sm_Keys[sm_Active ^1],sm_CurrentState,256);
	}
	else{
		if(sm_DiDeviceLost)
		{
			FlushKeyboard();
			return;
		}

		unsigned char state[256];
		sysMemSet(state, 0, 256);
		int current = sm_Active^1;

		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioKeyboard::Update() - GetDeviceState()"));
		HRESULT hres = g_keybDev->GetDeviceState(256,state);
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
		if (hres != DI_OK)
		{
			inputDebugf1("Failed to read keyboard (0x%08X)", hres);
			FlushKeyboard();
			sm_DiDeviceLost = true;
			return;
		}

		sysMemSet(sm_Keys[current], 0, 256);
		for(int i = 0; i < 256; ++i) {
			sm_Keys[current][sKeyDI2Rage[i]] |= state[i];
			sm_Keys[current][i] |= sm_CurrentState[i];
		}
	}

	sm_CapsLockToggled = (::GetKeyState(VK_CAPITAL) & 1) != 0;
	sm_Active ^= 1;
}


int ioKeyboard::GetBufferedInput(char *dest,int maxLen) {
	if (!g_keybDev) {
		return 0;
	}

	int count = 0;
	DIDEVICEOBJECTDATA rgdod[KB_BUFF_SIZE]; 
	DWORD dwItems = KB_BUFF_SIZE; 
	HRESULT hres = g_keybDev->GetDeviceData(sizeof(DIDEVICEOBJECTDATA), rgdod, &dwItems, 0); 

	if (SUCCEEDED(hres)) { 
		// dwItems = number of elements read (could be zero)
		/* if (hres == DI_BUFFEROVERFLOW) { 
			Errorf("Keyboard buffer had overflowed.");
		}*/
		if (dwItems) {
			dwItems = dwItems > KB_BUFF_SIZE ? KB_BUFF_SIZE : dwItems;
			for (DWORD i=0; i<dwItems; i++) {
				if (rgdod[i].dwData & 0x80) {	// button down?
					// Make keypresses "sticky" so that KeyPressed works at lower framerates
					if (!dest) {
						sm_Keys[sm_Active][rgdod[i].dwOfs & 0xFF] = 0x80;
					}
					if (count < maxLen) {
						dest[count++] = (char)(rgdod[i].dwOfs & 0xFF);
					}
				}
			}
		}
		return count;
	} 
	else {
		return 0;
	}
}


u32 ioKeyboard::GetKeyScanCode(u32 keyCode)
{
	u32 scanCode = 0;
	// Numlock and pause seem to share the same scan code. MapVirualKey appears to not set the
	// extended bit so we set it here if the key is numlock.
	switch(keyCode)
	{
	case KEY_NUMLOCK:
	case KEY_RMENU:
	case KEY_RCONTROL:
	case KEY_NUMPADENTER:
	case KEY_UP:
	case KEY_DOWN:
	case KEY_LEFT:
	case KEY_RIGHT:
	case KEY_INSERT:
	case KEY_HOME:
	case KEY_PAGEUP:
	case KEY_PAGEDOWN:
	case KEY_DELETE:
	case KEY_END:

		scanCode = 0x100;

		// Numpad enter is not a valid virtual key (it is one we defined ourselves) so handle it here.
		if(keyCode == KEY_NUMPADENTER)
		{
			keyCode = VK_RETURN;
		}
		break;
	
	default:
		break;
	}
	
	SetKeyboardLayoutForThread();
	
	// MapVirtualKey() apparently does not work for VK_PAUSE.
	scanCode |= (keyCode == VK_PAUSE) ? 0x45 : MapVirtualKeyW(keyCode, MAPVK_VK_TO_VSC);

	return scanCode;
}

void ioKeyboard::GetKeyName(u32 keyCode, char16* buffer, int bufferSize)
{
	if(inputVerifyf(buffer && bufferSize > 0, "Invalid buffer passed to ioKeyboard::GetKeyName()!"))
	{
		buffer[0] = '\0';
		GetKeyNameTextW(GetKeyScanCode(keyCode) << LEGACY_KEY_CODE_BIT_OFFSET, reinterpret_cast<wchar_t*>(buffer), bufferSize);
		if(buffer[0] != '\0')
		{
			_wcsupr(reinterpret_cast<wchar_t*>(buffer));
		}
		else
		{
			Errorf("No name returned for key (%d).", keyCode);
		}
	}
}

void ioKeyboard::SetKeyboardLayoutForThread()
{
	HKL layout = s_USKeyboardLayout;
	if(layout == 0 || sm_AllowLocalKeyboardLayout || sm_IgnoreInput)
	{
		layout = s_LocalKeyboardLayout;
	}

	if((layout != 0) && (sm_IgnoreInput == false))
	{
		inputVerifyf(ActivateKeyboardLayout(layout, KLF_RESET) != 0, "Error activating keyboard layout!");
	}
}

void ioKeyboard::UpdateKeyboardLayoutSettings()
{
	ioKeyboard::SetKeyboardLayoutForThread();

	HIMC hImc = ImmGetContext(g_hwndMain);
	DWORD conversionMode = 0, sentanceMode = 0;
	if(inputVerifyf(ImmGetConversionStatus(hImc, &conversionMode, &sentanceMode), "Failed to get IME conversion mode!"))
	{
		// We do not have fonts for half width katakana so set the layout to hiragana.
		const DWORD halfWidthKatakana = IME_CMODE_ROMAN | IME_CMODE_JAPANESE | IME_CMODE_KATAKANA | IME_CMODE_LANGUAGE;
		if(conversionMode == halfWidthKatakana)
		{
			const DWORD hiragana = IME_CMODE_ROMAN | IME_CMODE_JAPANESE | IME_CMODE_FULLSHAPE;
			inputVerifyf(ImmSetConversionStatus(hImc, hiragana, sentanceMode), "Failed to set IME conversion mode!");
		}
	}

	ImmReleaseContext(g_hwndMain, hImc);
}

void ioKeyboard::SetKeyboardLayoutFromThreadLayout()
{
	if(sm_AllowLocalKeyboardLayout || s_LocalKeyboardLayout == 0)
	{
		s_LocalKeyboardLayout = GetKeyboardLayout(0);
	}
}

void ioKeyboard::RecaptureLostDevices()
{
	if(sm_DiDeviceLost)
	{
		inputDebugf2("Recapturing keyboard.");
		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioKeyboard::RecaptureLostDevices() - Acquire()"));
		HRESULT hres = g_keybDev->Acquire();
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));

		// Apparently S_FALSE means we already have it acquired.
		if(hres != S_FALSE && FAILED(hres))
		{
			inputDebugf2("Failed to capture keyboard (0x%08X)", hres);
			FlushKeyboard();
		}
		else
		{
			sm_DiDeviceLost = false;
			inputDebugf1("Recaptured keyboard.");
		}
	}
}

#if IME_TEXT_INPUT
// Windows IME implementation.
void ioKeyboard::ImeRetrieveTextResult()
{
	HIMC hImc = ImmGetContext(g_hwndMain);

	const u32 numEmptyChars = TEXT_BUFFER_LENGTH - sm_CurrentLength;

	// + sizeof(wchar_t) to buffer size as according to https://msdn.microsoft.com/en-us/library/windows/desktop/dd319104%28v=vs.85%29.aspx it does not include
	// NULL terminator.
	inputAssertf(ImmGetCompositionStringW(hImc, GCS_RESULTSTR, NULL, 0) + sizeof(wchar_t) <= numEmptyChars * sizeof(wchar_t),
		"Buffer not large enough for IME composition text!" );


	// ImmGetCompositionStringW does not add the NULL terminator so null array first.
	sysMemSet(&sm_CurrentTextBuffer[sm_CurrentLength], 0, numEmptyChars * sizeof(char16));

	// -1 for NULL space.
	ImmGetCompositionStringW( hImc,
		GCS_RESULTSTR,
		reinterpret_cast<wchar_t*>(&sm_CurrentTextBuffer[sm_CurrentLength]),
		(numEmptyChars - 1) * sizeof(wchar_t));

	// Fill sm_CurrentTextBufferKeys from the current position to the end will NULL so that it does not contain incorrect key values!
	u32 oldLength = sm_CurrentLength;
	sm_CurrentLength = StringLength(sm_CurrentTextBuffer);	
	while(oldLength < sm_CurrentLength)
	{
		sm_CurrentTextBufferKeys[oldLength] = KEY_NULL;
		++oldLength;
	}

	ImmReleaseContext(g_hwndMain, hImc);
}

void ioKeyboard::ImeUpdateCandidateTexts()
{
	HIMC hImc = ImmGetContext(g_hwndMain);
	const u32 len = ImmGetCandidateListW(hImc, 0, NULL, 0);
	if(len)
	{
		LPCANDIDATELIST pCandidateList = NULL;

		// To ensure we do not use too much memory on the stack we use the heap if we are going to RageAlloca() a lot of memory.
		const bool useHeap = len > 2048;
		if(useHeap)
		{
			pCandidateList = reinterpret_cast<LPCANDIDATELIST>(rage_new u8[len]);
		}
		else
		{
			pCandidateList =  reinterpret_cast<LPCANDIDATELIST>(RageAlloca(len));
		}

		if(inputVerifyf(ImmGetCandidateListW(hImc, 0, pCandidateList, len), "ImmGetCandidateListW failed!"))
		{
			inputAssertf( pCandidateList->dwPageSize <= MAX_IME_CANDIDATES,
				"Too many candidates available (%u), MAX_IME_CANDIDATES (%u) might need increasing!",
				pCandidateList->dwPageSize,
				MAX_IME_CANDIDATES );

			const DWORD maxCandidates = Min(pCandidateList->dwPageSize, static_cast<DWORD>(MAX_IME_CANDIDATES));
			const DWORD lastCandidateIndex = Min(pCandidateList->dwPageStart + maxCandidates, pCandidateList->dwCount);

			sysMemSet(sm_ImeState[IME_CURRENT_STATE].m_Candidates, 0, sizeof(sm_ImeState[IME_CURRENT_STATE].m_Candidates));
			for(u32 i = pCandidateList->dwPageStart, candidateIndex = 0u; i < lastCandidateIndex; ++i, ++candidateIndex)
			{
				// NOTE: Yuk, Microsoft give us the offset from sm_pImeCandidateList to the wchar_t* string we are after rather than just giving us the pointer.
				// That means to get he pointer, we need to cast sm_pImeCandidateList to a u8 to add the offset correctly. We then cast the result to a char16 to return.
				const char16* candidate = reinterpret_cast<const char16*>(reinterpret_cast<const u8*>(pCandidateList) + pCandidateList->dwOffset[i]);

				inputAssertf(StringLength(candidate) <= TEXT_BUFFER_LENGTH, "Buffer not large enough to receive buffer!");
				safecpy(sm_ImeState[IME_CURRENT_STATE].m_Candidates[candidateIndex], candidate);
			}
			
			sm_ImeState[IME_CURRENT_STATE].m_NumCandidates = lastCandidateIndex - pCandidateList->dwPageStart;
			sm_ImeState[IME_CURRENT_STATE].m_SelectedCandidateIndex = pCandidateList->dwSelection - pCandidateList->dwPageStart;
		}

		if(useHeap)
		{
			delete[] pCandidateList;
		}
	}

	ImmReleaseContext(g_hwndMain, hImc);
}

void ioKeyboard::ImeUpdateCompositionText()
{
	HIMC hImc = ImmGetContext(g_hwndMain);

	if(!sm_AllowLocalKeyboardLayout)
	{
		ImmSetCompositionStringW( hImc, SCS_SETSTR,  "", 1, NULL, 0);
		return;
	}

	// + sizeof(wchar_t) to buffer size as according to https://msdn.microsoft.com/en-us/library/windows/desktop/dd319104%28v=vs.85%29.aspx
	// it does not include NULL terminator.
	inputAssertf( ImmGetCompositionStringW(hImc, GCS_COMPSTR, NULL, 0) + sizeof(wchar_t) <= sizeof(sm_ImeState[IME_CURRENT_STATE].m_CompositionText),
		"Buffer not large enough for IME composition text!" );

	// ImmGetCompositionStringW does not add the NULL terminator so null array first.
	sysMemSet(sm_ImeState[IME_CURRENT_STATE].m_CompositionText, 0, sizeof(sm_ImeState[IME_CURRENT_STATE].m_CompositionText));

	// -sizeof(wchar_t) for NULL space.
	ImmGetCompositionStringW( hImc,
		GCS_COMPSTR,
		reinterpret_cast<wchar_t*>(sm_ImeState[IME_CURRENT_STATE].m_CompositionText),
		sizeof(sm_ImeState[IME_CURRENT_STATE].m_CompositionText) - sizeof(wchar_t) );

	ImmReleaseContext(g_hwndMain, hImc);
}

#endif // IME_TEXT_INPUT

void ioKeyboard::FlushKeyboard()
{
	memset(sm_Keys[0], 0, 256);
	memset(sm_Keys[1], 0, 256);
	memset(sm_CurrentState, 0, 256);
}

#endif // IS_CONSOLE && !RSG_DURANGO

#if IME_TEXT_INPUT
ioKeyboard::ImeState ioKeyboard::sm_ImeState[IME_NUM_STATES];
void ioKeyboard::ImeBegin()
{
	sm_ImeState[IME_ACTIVE_STATE].Clear();
	sm_ImeState[IME_CURRENT_STATE].Clear();
}

void ioKeyboard::ImeEnd()
{
	sm_ImeState[IME_ACTIVE_STATE].Clear();
	sm_ImeState[IME_CURRENT_STATE].Clear();
}

void ioKeyboard::ImeState::Clear()
{
	m_NumCandidates = 0u;
	m_SelectedCandidateIndex = ioKeyboard::IME_INVALID_CANDIDATE_INDEX;
	sysMemSet(m_CompositionText, 0, sizeof(m_CompositionText));
	sysMemSet(m_Candidates,      0, sizeof(m_Candidates));
	m_InProgress = false;
}

void ioKeyboard::ImeUpdate()
{
	// We do not swap the IME state like we do the keyboard buffer. Instead we copy the contents from current to active.
	// The reason for this is, unlike the keyboard state, the IME state needs to persist over frames until it has been changed.
	sysMemCpy(&sm_ImeState[IME_ACTIVE_STATE], &sm_ImeState[IME_CURRENT_STATE], sizeof(sm_ImeState[IME_ACTIVE_STATE]));
}

void ioKeyboard::ImeStartTextInput()
{
	sm_ImeState[IME_CURRENT_STATE].Clear();
	ImeUpdateCompositionText();
	sm_ImeState[IME_CURRENT_STATE].m_InProgress = true;
}

void ioKeyboard::ImeStopTextInput()
{
	sm_ImeState[IME_CURRENT_STATE].Clear();
}
#endif // IME_TEXT_INPUT

bool ioKeyboard::SetTextCharPressed( char16 key, ioMapperParameter rawKey )
{
	if(sm_CurrentLength < TEXT_BUFFER_LENGTH - 1)
	{
		if (inputVerifyf(key, "Got a null char pressed for key %d", rawKey))
		{
			sm_CurrentTextBufferKeys[sm_CurrentLength] = rawKey;
			sm_CurrentTextBuffer[sm_CurrentLength] = key;
			sm_CurrentLength++;
		}
		return true;
	}
	return false;
}

const char16* ioKeyboard::GetInputText()
{
	//if (sm_TextBuffer[0])	inputDisplayf("Returning [%s]",sm_TextBuffer);
	return sm_TextBuffer;
}

char16 ioKeyboard::GetPrintableChar(ioMapperParameter rawKey)
{
	for(u32 i = 0; i < sm_CurrentLength; i++)
	{
		if (sm_CurrentTextBufferKeys[i] == rawKey) {
			return sm_CurrentTextBuffer[i];
		}
	}
	return 0;
}
