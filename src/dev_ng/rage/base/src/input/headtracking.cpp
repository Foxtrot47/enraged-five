
// 
// input/headtracking.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "headtracking.h"
#include "bank/bkmgr.h"
#include "bank/bank.h"

using namespace rage;

bool		ioHeadTracking::sm_bEnabled = false;
bool		ioHeadTracking::sm_bUseFPSCamera = false;

Quaternion	ioHeadTracking::sm_qOrientation = Quaternion(0,0,0,1);
Vector3		ioHeadTracking::sm_vRelativeOffset = Vector3(0,0,0);

bool		ioHeadTracking::sm_bUsePredicatedMode = false;
float		ioHeadTracking::sm_fTimePredicted = 1 / 60.0f;

Vector3		ioHeadTracking::sm_vAcceleration = Vector3(0,0,0);
Vector3		ioHeadTracking::sm_vAngularVelocity = Vector3(0,0,0);

float		ioHeadTracking::sm_fPlayerHeight = 1.6f;


#if __BANK
void ioHeadTracking::InitWidgets()
{
	bkBank *bankPtr = BANKMGR.FindBank("Head Tracking");
	if(bankPtr)
	{
		bankPtr->Destroy();
	}
	bkBank& bank = BANKMGR.CreateBank("Head Tracking");
	bank.AddToggle("Enable", &sm_bEnabled);
	bank.AddToggle("First Person Camera", &sm_bUseFPSCamera);
	bank.AddSlider("Player Height", &sm_fPlayerHeight, 1.0f, 3.0f, 0.01f);
}
#endif // __BANK