//
// input/pad.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_PAD_H
#define INPUT_PAD_H

#include "xinput_config.h"

#include "data/base.h"
#include "data/callback.h"

#if RSG_ORBIS
#include "vector/vector3.h"
#include "vector/quaternion.h"
#endif

#define __DEBUGBUTTONS !__FINAL
#define HAS_TRIGGER_RUMBLE (RSG_DURANGO)
#define RSG_TOUCHPAD (RSG_ORBIS)

#if HAS_TRIGGER_RUMBLE
#define TRIGGER_RUMBLE_ONLY(x)	x
#else
#define TRIGGER_RUMBLE_ONLY(x)
#endif // HAS_TRIGGER_RUMBLE_ONLY

namespace rage {

/*
	PURPOSE
		Abstracts the game pad behind a portable class.  All platforms are made to look like
		the PS2 dual shock controller, including joysticks on the PC.  Furthermore, several
		different USB-based PS2 controller adapters are directly supported by this code.
	
	NOTES
		DANGER: Updating the pad too fast (ie in a tight loop, not once
		per frame) seems to cause bogus reads.  In the real world this
		should not be a problem.

	<FLAG Component>
*/
class ioPad : public datBase 
{
public:
	static const int MAX_TOUCH_NUM = 2;

	enum SubType { UNKNOWN, ARCADE_STICK, FLIGHT_STICK, GAMEPAD, WHEEL, DANCE_COMMANDER };

	// PURPOSE: Constructor
	ioPad();

	// PURPOSE: Destructor
	~ioPad() { }

	enum PadActuator
	{
		HEAVY_MOTOR = 0,
		LIGHT_MOTOR,

#if HAS_TRIGGER_RUMBLE
		LEFT_TRIGGER,
		RIGHT_TRIGGER,
#endif // HAS_TRIGGER_RUMBLE

		MAX_ACTUATORS,
	};

private:
	void Begin(int index);
	void End();
	void Update(bool ignoreInput = false);

#if RSG_ORBIS
	void SetIsConnected(bool bConnected);
#endif	//	RSG_ORBIS

public:
	// PURPOSE: Initialize all possible pads on system
	static void BeginAll();

	// PURPOSE: Shut down all possible pads on system
	static void EndAll();

	// PURPOSE: Updates all possible pads on system (gets current values, identifies buttons that have changed)
	static void UpdateAll(bool ignoreInput = false, bool checkForNewDevice = true);

	// PURPOSE: Re-checks for newly plugged in devices.
	// NOTES:	The xbox pad can take a long time to poll when not plugged in (at least on pc). This allows us to check for new devices
	//			rather than checking in UpdateAll(). Currently this is only usful for xenon pads.
	static void CheckForNewlyPluggedInDevices();

	// PURPOSE: Force all pads to reinitialize.  (See Steve Reed about this?)
	static void ReattachAll();

#if __BANK
	// PURPOSE: Adds a bank of widgets for simulating the input from pad 0
	static void AddWidgets();
	
	void AddPadWidgets();
#endif

	// RETURNS: Reference to the specified pad object (always numbered left to right)
	// PARAMS: Index of pad to return
	static inline ioPad& GetPad(int i);

	enum {	// bitmask of values returned by GetButtons
		L2=0x0001, R2=0x0002, L1=0x0004, R1=0x0008,
		RUP=0x0010, RRIGHT=0x0020, RDOWN=0x0040, RLEFT=0x0080,
		SELECT=0x0100, L3=0x0200, R3=0x0400, START=0x0800,
		LUP=0x1000, LRIGHT=0x2000, LDOWN=0x4000, LLEFT=0x8000,
		TOUCH=0x10000
	};

	enum ePadButton {	// log2(bitmask) of values returned by GetButtons
		L2_INDEX, R2_INDEX, L1_INDEX, R1_INDEX,
		RUP_INDEX, RRIGHT_INDEX, RDOWN_INDEX, RLEFT_INDEX,
		SELECT_INDEX, L3_INDEX, R3_INDEX, START_INDEX,
		LUP_INDEX, LRIGHT_INDEX, LDOWN_INDEX, LLEFT_INDEX, TOUCH_INDEX,
		NUMBUTTONS
	};
	// RETURNS: Current digital button state
	inline u32 GetButtons() const;

	// PURPOSE: Forces digital button state to specified value
	// PARAMS:	b - New button bitmask
	inline void SetButtons(u32 b);
	inline void SetAxis( int axis, u8 value );

#if __BANK
	inline void SetBankButtons( u32 b );

	inline void SetBankAnalogButtons( int button, unsigned char value );

	inline void SetBankAxis( int axis, u8 value );
#endif

#if RSG_PC && !RSG_FINAL
	// Hack to get the PS4 controller working with the debug camera for working at home.
	void MergeButton(int button, float value);
	void MergeAxis(int axis, float value);
#endif // RSG_PC && !RSG_FINAL

	// PURPOSE:	Returns left analog stick X value
	inline u8 GetLeftX() const;

	// PURPOSE: Returns left analog stick Y value
	inline u8 GetLeftY() const;

	// PURPOSE: Returns right analog stick X value
	inline u8 GetRightX() const;

	// PURPOSE: Returns right analog stick Y value
	inline u8 GetRightY() const;

	// PURPOSE: Returns device subtype
	inline SubType GetSubType() const;

	// PURPOSE: Returns whether device has FF or not.
	inline bool HasForceFeedback() const;

	// RETURNS: Normalized version of value
	// PARAMS:	value - Value to normalize (presumably an analog axis)
	// NOTES:	Will give you a range between -1.0 and 1.0, but will not give you 0.0
	static inline float Norm(u8 value);

	// RETURNS: Normalized value of right analog stick X, as per ioPad::Norm
	inline float GetNormRightX() const;

	// RETURNS: Normalized value of right analog stick Y, as per ioPad::Norm (-1 is up, +1 is down)
	inline float GetNormRightY() const;

	// RETURNS: Normalized value of left  analog stick X, as per ioPad::Norm
	inline float GetNormLeftX() const;

	// RETURNS: Normalized value of left analog stick Y, as per ioPad::Norm (-1 is up, +1 is down)
	inline float GetNormLeftY() const;

	// RETURNS: Bitmask of buttons which have changed since last update
	inline u32 GetChangedButtons() const;

	// RETURNS: Bitmask of buttons which were just pressed since last update
	inline u32 GetPressedButtons() const;

	// RETURNS: Bitmask of buttons which were just released since last update
	inline u32 GetReleasedButtons() const;

	// Enumerant for SIXAXIS sensor axes.  (Should revise for Wii...)
	enum ioPadSensorAxis {
		SENSOR_X, SENSOR_Y, SENSOR_Z, SENSOR_G, SENSOR_NUMAXES
	};

	struct TouchPadData
	{
		u16 x;
		u16 y;
		u8 id;
	};

	inline bool IsAnalog() const;

	// This returns false on Xenon, because only L2 and R2 are analog.
	inline bool AreButtonsAnalog() const;

	inline bool AreTriggersAnalog() const;

	inline bool HasSensors() const;

	inline u8 GetAnalogButton(ePadButton b) const;

	inline float GetNormAnalogButton(ePadButton b) const;

	// RETURNS: Sensor axis value (see Sony docs for details)
	inline u16 GetSensorAxis(int axis) const;

	// RETURNS: Sensor axis value (see Sony docs for details)
	inline float GetNormSensorAxis(int axis) const;

	// RETURNS: True if the pad is connected and usable.
	inline bool IsConnected() const;

	// PURPOSE:	Sets rumble value for actuator
	// PARAMS:	actuatorIndex - either 0 or 1
	//			value - Rumble value (0=min, 1=max)
	void SetActuatorValue(int actuatorIndex,float value);

	// PURPOSE:	Enable or disable actuators
	// PARAMS:	flag - True to enable actuators, false to shut them off.
	void SetActuatorsActivation(bool flag);

	// PURPOSE: Returns True if actuators are enabled
	bool GetActuatorsActivated() const;

	// PURPOSE: Check to see if actuators have changed, the flag is set to false during update when
	//          successfuly set
	inline bool GetActuatorsChanged() const;

	// RETURNS:	Number of actuators on pad
	inline int GetNumActuators() const;

	// RETURNS: True if hardware supports rumble or not.
	inline bool HasRumble() const;

	// PURPOSE: Force pad to stop shaking.
	void StopShakingNow( );

	// RETURNS: True if input is intercepted (typically means system UI is up, and you'll want to pause if not networked)
	static inline bool IsIntercepted();

	// RETURNS: True if input type has changed
	static inline bool HasChanged();

	// RETURNS Numbers of current touches on Touchpad
	inline u8 GetNumTouches() const;

	// RETURNS: Current TouchPad state at index
	inline const TouchPadData& GetTouchData(int index) const;

	// RETURNS: The x coord of the TouchPad state, this frame, in the range -1...1.
	float GetTouchXCenterNorm(int index) const;

	// RETURNS: The y coord of the TouchPad state, this frame, in the range -1...1.
	float GetTouchYCenterNorm(int index) const;

	// RETURNS:	The x coord of the TouchPad state, this frame, in the range 0...1.
	float GetTouchXNorm() const;

	// RETURNS:	The y coord of the TouchPad state, this frame, in the range 0...1.
	float GetTouchYNorm() const;

#if RSG_ORBIS
	// PURPOSE: Reset the controller orientation, i.e, set the current orientation to Identity(0,0,0,1)
	void ResetOrientation();

	// PURPOSE: Set the RGB values of the Orbis lightbar
	void SetLightbar(u8 red, u8 green, u8 blue);

	// PURPOSE: Reset the Orbis lightbar to its default (index 0 = blue, 1 = red, 2 = green, 3 = pink)
	void ResetLightbar();
	 
	// PURPOSE: Set the Orbis motion sensor state (on or off)
	void SetMotionSensorState(bool bEnabled);

	// PURPOSE: Set the Orbis tilt correction state (on or off)
	void SetTiltCorrectionState(bool bEnabled);

	// PURPOSE: Set the Orbis angular velocity state (on or off)
	void SetAngularVelocityDeadbandState(bool bEnabled);

	// PURPOSE: On Orbis, controllers are owned by a user account. 
	//	When a user logs in, their pad is initialized using their user service ID
	//  When a user logs out, their pad is shutdown
	void HandleUserServiceStatusChange(int userId);

	// PURPOSE: Returns the Orbis controller's acceleration
	inline const Vector3& GetAcceleration() const;

	// PURPOSE: Returns the Orbis controller's angular velocity
	inline const Vector3& GetAngularVelocity() const;

	// PURPOSE: Returns the Orbis controller orientation
	inline const Quaternion& GetOrientation() const;

	// PURPOSE: Returns true if the pad is a remote play pad.
	inline bool IsRemotePlayPad() const;

	// PURPOSE: Returns true if the pad has ever been connected in the past.
	inline bool HasBeenConnected() const;
#endif

#if __DEBUGBUTTONS
	// RETURNS: Bitmask of debug buttons which are currently down
	// NOTES: Debug buttons are buttons that are pressed while holding down Start
	unsigned GetDebugButtons() const { return m_DebugButtons; }

	// RETURNS: Bitmask of debug buttons which have changed since last update
	// NOTES: Debug buttons are buttons that are pressed while holding down Start
	unsigned GetChangedDebugButtons() const { return m_LastDebugButtons ^ GetDebugButtons(); }

	// RETURNS: Bitmask of debug buttons which were just pressed since last update
	// NOTES: Debug buttons are buttons that are pressed while holding down Start
	unsigned GetPressedDebugButtons() const { return GetChangedDebugButtons() & GetDebugButtons(); }

	// RETURNS: Bitmask of debug buttons which were just released since last update
	// NOTES: Debug buttons are buttons that are pressed while holding down Start
	unsigned GetReleasedDebugButtons() const { return GetChangedDebugButtons() & m_LastDebugButtons; }

	// PURPOSE: Forces debug button state to specified value
	// PARAMS:	buttons - New button bitmask
	inline void SetDebugButtons(u32 buttons) { m_DebugButtons = buttons; }

	// PURPOSE: Change the debug button from the default (Start)
	// PARAMS: button - New debug button
	static void SetDebugButton(unsigned button) { m_DebugButton = button; }

	// RETURNS: Bitmask of buttons that are acting as the debug activation button. 
	// NOTES: E.g. start.
	static unsigned GetDebugButton() { return m_DebugButton; }

	// RETURNS: Currently chosen debug button (from m_DebugButton bitmask).
	// NOTES: 
	static unsigned GetCurrentDebugButton() { return m_CurrentDebugButton; }

	static void UpdateDebugAll();

	void UpdateDebug();
#else
	static void UpdateDebugAll() { }

	void UpdateDebug() { }
#endif

#if __PSP2
	enum {MAX_PADS=1};
#elif __PPU
	enum {MAX_PADS=7};
#else
	enum {MAX_PADS=4};
#endif

	int GetPadIndex() const {return m_PadIndex;}

#if __WIN32PC && !__FINAL
	// For multiple instances on the same PC
	static bool sm_IgnoreControllerInput;
#endif

private:
	int m_PadIndex;
	u32 m_Buttons;
	u32 m_LastButtons;
	u8 m_Axis[4];
	unsigned char m_AnalogButtons[NUMBUTTONS];
	unsigned short m_Sensors[SENSOR_NUMAXES];
	bool m_IsConnected;
	bool m_ActuatorsEnabled;
	bool m_ActuatorDataChanged;
	bool m_HasForceFeedback;

	bool m_AreButtonsAnalog;
	bool m_AreTriggersAnalog;
	bool m_HasSensors;
	SubType m_SubType;
	bool m_HasRumble;

	// PURPOSE: All Update functions call this first to copy previous button
	// state and re-center analog inputs.  This should be called exactly once
	// per update so that higher-level code will never miss a press or release
	// event even on communication error or disconnect.
	void ClearInputs();

	static ioPad sm_Pads[MAX_PADS];
	u16 m_ActuatorData[MAX_ACTUATORS];
	static bool sm_Intercepted;
	static bool sm_HasChanged;

	u8 m_NumTouches;
	TouchPadData m_previousTouchData[MAX_TOUCH_NUM];
	TouchPadData m_currentTouchData[MAX_TOUCH_NUM];

#if RSG_ORBIS
	Vector3 m_Acceleration;
	Vector3 m_AngularVelocity;
	Quaternion m_Orientation;
	bool m_IsRemotePlayPad;
	bool m_HasBeenConnected;

	// PURPOSE:	The previous point used for the cursor.
	TouchPadData m_CursorPrevious;
	float m_CursorX;
	float m_CursorY;
#endif

#if __DEBUGBUTTONS
	static unsigned m_DebugButton;
	static unsigned m_CurrentDebugButton;
	static unsigned m_DebugButtonStartTime;
	const static unsigned DEBUG_BUTTONS_TIME_LIMIT;
	unsigned m_DebugButtons, m_LastDebugButtons;
	int m_DebugState;
#endif

#if __BANK
	void ClearBankInputs();

public:
	void AnalogStickPressedCB( CallbackData data );
	void ButtonPressedCB( CallbackData data );
private:

	u8 m_bankAxis[4];
	unsigned char m_bankAnalogButtons[NUMBUTTONS];
	int m_bankButtons;
#endif
};



inline ioPad& ioPad::GetPad(int i) {
	FastAssert(i>=0 && i<MAX_PADS);
	return sm_Pads[i];
}

inline float ioPad::Norm(u8 value) { 
	float n = ((float)value - 127.5f) * (1.0f / 127.0f);
	if (n < -1.0f)
		return -1.0f;
	else if (n > 1.0f)
		return 1.0f;
	else
		return n;
}

inline u32 ioPad::GetButtons() const {
	return m_Buttons;
}

inline void ioPad::SetButtons(u32 b) {
	m_Buttons = b;
}

inline void ioPad::SetAxis( int axis, u8 value ) {
	FastAssert(axis>=0 && axis<4);
	m_Axis[axis] = value;
}

#if __BANK
inline void ioPad::SetBankButtons( u32 b )
{
	m_bankButtons = b;
}

inline void ioPad::SetBankAnalogButtons( int button, unsigned char value )
{
	if ( button < NUMBUTTONS )
	{
		m_bankAnalogButtons[button] = value;
	}
}

inline void ioPad::SetBankAxis( int axis, u8 value )
{
	if ( axis < 4 )
	{
		m_bankAxis[axis] = value;
	}
}
#endif


inline u8 ioPad::GetLeftX() const {
	return m_Axis[0];
}

inline u8 ioPad::GetLeftY() const {
	return m_Axis[1];
}

inline u8 ioPad::GetRightX() const {
	return m_Axis[2];
}

inline u8 ioPad::GetRightY() const {
	return m_Axis[3];
}

inline float ioPad::GetNormLeftX() const {
	return Norm(GetLeftX());
}

inline float ioPad::GetNormLeftY() const {
	return Norm(GetLeftY());
}

inline float ioPad::GetNormRightX() const {
	return Norm(GetRightX());
}

inline float ioPad::GetNormRightY() const {
	return Norm(GetRightY());
}

inline u32 ioPad::GetChangedButtons() const { 
	return m_LastButtons ^ GetButtons(); 
}

inline u32 ioPad::GetPressedButtons() const { 
	return GetChangedButtons() & GetButtons(); 
}

inline u32 ioPad::GetReleasedButtons() const { 
	return GetChangedButtons() & m_LastButtons; 
}

inline bool ioPad::IsAnalog() const {
	return true;
}

inline bool ioPad::AreButtonsAnalog() const {
	return m_AreButtonsAnalog;
}

inline bool ioPad::AreTriggersAnalog() const {
	return m_AreTriggersAnalog;
}

inline bool ioPad::HasSensors() const {
	return m_HasSensors;
}

inline u8 ioPad::GetAnalogButton(ePadButton b) const {
	return m_AnalogButtons[b];
}

inline float ioPad::GetNormAnalogButton(ePadButton b) const {
	return m_AnalogButtons[b] / 255.0f;
}

inline bool ioPad::IsConnected() const {
	return m_IsConnected;
}

inline int ioPad::GetNumActuators() const {
	return MAX_ACTUATORS;
}

inline bool ioPad::HasRumble() const {
	return m_HasRumble;
}

inline bool ioPad::GetActuatorsChanged() const {
	return m_ActuatorDataChanged;
}

inline ioPad::SubType ioPad::GetSubType() const {
	return m_SubType;
}

inline bool ioPad::HasForceFeedback() const {
	return m_HasForceFeedback;
}

inline u16 ioPad::GetSensorAxis(int axis) const {
	return m_Sensors[axis];
}

inline float ioPad::GetNormSensorAxis(int axis) const {
	return ((float)(m_Sensors[axis] - 512)) / (axis == SENSOR_G? 113.0f : 123.0f);
}

inline bool ioPad::IsIntercepted() {
	return sm_Intercepted;
}

inline bool ioPad::HasChanged() {
	return sm_HasChanged;
}

inline u8 ioPad::GetNumTouches() const {
	return m_NumTouches;
}

inline const ioPad::TouchPadData& ioPad::GetTouchData(int index) const {
	FastAssert(index>=0 && index<MAX_TOUCH_NUM);
	return m_currentTouchData[index];
}

#if RSG_ORBIS
inline const Vector3& ioPad::GetAcceleration() const {
	return m_Acceleration;
}

inline const Vector3& ioPad::GetAngularVelocity() const {
	return m_AngularVelocity;
}

inline const Quaternion& ioPad::GetOrientation() const {
	return m_Orientation;
}

inline bool ioPad::IsRemotePlayPad() const {
	return m_IsRemotePlayPad;
}

inline bool ioPad::HasBeenConnected() const {
	return m_HasBeenConnected;
}
#endif
}	// namespace rage

#endif
