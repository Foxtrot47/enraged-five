//
// input/winpriv.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_WINPRIV_H
#define INPUT_WINPRIV_H

#if RSG_XENON
#include "system/xtl.h"
#elif RSG_PC
#define DIRECTINPUT_VERSION 0x0800
#include "system/xtl.h"
#define INPUT WIN32_INPUT	// WinCE hack
#include <dinput.h>
#undef INPUT

extern LPDIRECTINPUTW lpDI;
// extern HWND g_hwndMain;

#define ditry(x)    do { HRESULT hr; if ((hr=(x)) != DI_OK) \
        Quitf(ERR_GEN_INPUT_2,"%s failed, code=%08x (%d)",#x,hr,hr&0xffff); } while (0)

namespace rage {

LRESULT CALLBACK InputWindowProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

//ADDED FOR GFWL
//typedef LRESULT (CALLBACK *InputWindowProcCallback)(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);	
//extern InputWindowProcCallback sm_InputWindowProcCallback;

extern void diInit();

/*
	PURPOSE
		Allow higher level code to patch into custom WM_ messages by deriving from
		this base class
*/
class winDispatchable {
public:
	// PURPOSE: Constructor
	// PARAMS: message - WM_... message we want to capture.
	// NOTES: Only messages that are not already handled can be captured here.
	//		See InputWindowProc for details.
	winDispatchable(UINT message);

	// PURPOSE: Destructor
	virtual ~winDispatchable();

	// PURPOSE: Entry point for user message handling
	// PARAMS: hwnd - Window handle, as per Win32 standard
	//		msg - Message, as per Win32 standard
	//		wparam - Message-specific value, as per Win32 standard
	//		lparam - Message-specific value, as per Win32 standard
	// RESULTS: As per Win32 standard
	// NOTES: More than one winDispatchable using the same message can be
	// registered; all will be called.  DefWindowProc is also always called
	// as well.
	virtual LRESULT WindowProc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam) = 0;
};

}	// namespace rage

#endif

#endif
