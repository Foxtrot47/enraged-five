//
// input/remote_input_durango.winrt.h
//
// Copyright (C) 1999-2020 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_REMOTE_INPUT_DURANGO_WINRT_H
#define INPUT_REMOTE_INPUT_DURANGO_WINRT_H

#include "remote_input_defs.h"

#if (REMOTE_INPUT_SUPPORT && RSG_DURANGO)

namespace rage
{

class ioRemoteInput_WinRT
{
public:
	
	static void	UpdatePad(int index, Windows::Xbox::Input::RawGamepadReading& reading);
	
};

}	// namespace rage

#endif // REMOTE_INPUT_SUPPORT && RSG_DURANGO 

#endif // INPUT_REMOTE_INPUT_DURANGO_WINRT_H