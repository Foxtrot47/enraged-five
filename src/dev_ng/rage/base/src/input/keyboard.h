//
// input/keyboard.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_KEYBOARD_H
#define INPUT_KEYBOARD_H

#include "mapper_defs.h"
#include "input_channel.h"

#define IME_TEXT_INPUT (1 && RSG_PC)

#if IME_TEXT_INPUT
#define IME_TEXT_INPUT_ONLY(...)	__VA_ARGS__
#else
#define IME_TEXT_INPUT_ONLY(...)
#endif // IME_TEXT_INPUT

namespace rage {
// Typedef for char16 so string/unicode.h is not pulled in.
typedef u16 char16;

//	PURPOSE
//		Abstract the keyboard.  Supported on PC, and, on __DEV builds, the PS with USB.
//		With the exception of GetBufferedInput, all key references should use the KEY_...
//		definitions in input/keys.h
//
//	<FLAG Component>
class ioKeyboard {
public:
	// PURPOSE: Initialize class.  Currently called automatically by graphics pipeline.
	// PARAMS: inWindow - True if running in a window, else false
	static void Begin(bool inWindow);

	// PURPOSE: Shuts down class.  Currently called automatically by graphics pipeline.
	static void End();

	// PURPOSE: Update keyboard state.  Currently called automatically by graphics pipeline.
	// NOTES: Call this once per frame; strictly speaking, you can call this as often as you
	// want, but the KeyPressed, KeyReleased, and KeyChanged functions are always relative
	// to the most recent Update call.
	static void Update(bool ignoreInput = false);

	// PURPOSE: Return buffered input since last call (suitable for user input)
	// PARAMS: dest - Character buffer to fill.  Can be NULL if
	//				maxDest is zero, in which case the keyboard is just flushed.
	//		maxDest - Size of dest, max number of characters we can store
	// RETURNS: Number of characters stored
	// NOTES: If there are more characters available that can be stored,
	//	they are simply thrown out.  This guarantees the key buffer can
	//	be flushed reliably.
	// Only works on PC; for PS2, see ioChat class instead
	// SEE ALSO: ioChat
	static int GetBufferedInput(char *dest,int maxLen);

	// PURPOSE: Check to see if a key is currently down
	// PARAMS: key - key to test
	// RETURNS: Nonzero if specified key is currently down
	static int KeyDown(int key);

	// PURPOSE: Check to see if either of two keys is currently down
	// PARAMS: key1 - First key to test
	//		key2 - Second key to test
	// RETURNS: Nonzero if either specified key is currently down
	// NOTES: This function exists because KEY_CONTROL, KEY_SHIFT, and KEY_ALT
	//		all expand to two separate keys, so this is a convenient way to test
	//		for either modifier key.
	static int KeyDown(int key1,int key2);

	// PURPOSE: Check to see if a key is currently up
	// PARAMS: key - key to test
	// RETURNS: Nonzero if specified key is currently up
	static int KeyUp(int key);

	// PURPOSE: Check to see if a key changed since the last Update
	// PARAMS: key - key to test
	// RETURNS: Nonzero if specified key has changed state since last Update call
	static int KeyChanged(int key);

	// PURPOSE: Check to see if the key was pressed since the last Update
	// PARAMS: key - key to test
	// RETURNS: Nonzero if specified key was just pressed since last Update call
	static int KeyPressed(int key);

	// PURPOSE: Check to see if the key was released since the last 
	// PARAMS: key - key to test
	// RETURNS: Nonzero if specified key was just released since last Update call
	static int KeyReleased(int key);

	// PURPOSE: Used during keyboard emulation on the PS2, not for external use
	static void SetKey(int key,bool active);

	// On PC, this is on by default. On consoles it has no effect.
	static bool GetUseKeyboardInBackground();

	// <COMBINE GetUseKeyboardInBackground>
	static void SetUseKeyboardInBackground(bool b);

	// PURPOSE: PS3 only: Set whether or not the keyboard is used in keypad mode. In this mode, keypresses are relayed without any
	// processing. Otherwise, they're treated like the user is trying to enter text - holding down a key will only send repeated, discrete
	// keypresses.
	//
	// PARAMS:
	//    keypadMode - True for keypad mode, false for text input mode
	//    forceUpdate - True to force switching the mode (which might involve system calls), false only if it changed from the last time
	static void SetUseKeypadMode(bool keypadMode, bool forceUpdate);

	// PURPOSE: Sets a text literal such as 'a' as being pressed. This will retrievable from GetInputText() after the next update.
	// PARAMS:  unicode - The unicode text character that is currently pressed.
	// RETURNS: True if char was successfully added or false if the buffer is full.
	static bool SetTextCharPressed(char16 letter, ioMapperParameter rawKey);

	// PURPOSE: Retrives input text from last frame set by SetTextCharPressed()
	static const char16* GetInputText();

	// PURPOSE: Gets the printable representation of a character. 
	// PARAMS: rawKey - must be one of the keys pressed during the last update
	// RETURNS: The unicode character that was created by pressing rawKey (plus any modifiers that were active at the time)
	// NOTES: Currently this is ascii only, because SetTextCharPressed only adds ascii chars to the buffer
	static char16 GetPrintableChar(ioMapperParameter rawKey);

	// PURPOSE: Check to see if _any_ keys have changed since the last update
	static bool AnyKeyChanged();

	// PURPOSE: Override a specific key, pretend it was pressed/not pressed
#if !__FINAL
	static void OverrideKey(int key, bool status);
#endif // __BANK

		// PURPOSE: Maximum string length.
	enum { TEXT_BUFFER_LENGTH = 31, }; // 30 + NULL terminator.

#if RSG_PC
	static bool IsUsingRawInput();

	// PURPOSE: Retrieves the toggle state of the Caps Lock key.
	static bool IsCapsLockToggled();

	// PURPOSE: Flushes the keyboard settings all keys as unpressed.
	static void FlushKeyboard();

	// PURPOSE:	Converts a scan code to a rage key code (ioMapperParameter).
	//			isExtendedScanCode - indicates if the scan code is an extended key.
	// RETURNS:	The rage key code (an ioMapperParameter KEY_* code).
	static unsigned char GetRageKeyCode(u32 scanCode);

	// PURPOSE: Retrieves the scan code for a given key.
	// PARAMS:  keycode - the key code (from ioMapperParameter) of the key to get the scan code for.
	// RETURNS: The keys scan code.
	static u32 GetKeyScanCode(u32 keyCode);

	// PURPOSE: Retrieves a key name for a key.
	// PARAMS:  keyCode - the key code (from ioMapperParameter) of the key to get the name for.
	//          buffer - a buffer to fill the name with.
	//          bufferSize - the size of the buffer passed in.
	static void GetKeyName(u32 keyCode, char16* buffer, int bufferSize);

	// PURPOSE:	Sets the keyboard layout for all threads based on the keyboard layout of the calling thread.
	// NOTES:	This should only be called from the windows message proc. In windows, different threads have
	//			different keyboard layouts. This is used to make all of our threads use the same keyboard layout.
	static void SetKeyboardLayoutFromThreadLayout();

	// PURPOSE: Sets the keyboard layout for this thread to the game keyboard layout.
	static void SetKeyboardLayoutForThread();

	// PURPOSE:	Set allow keyboard layout switching.
	// PARAMS:	enable - true to allow the user to set their keyboard layout.
	// NOTES:	On some platforms with IME input (e.g. Japanese) we set the keyboard layout to US English. The reason
	//			for this is some keys (most notably the Caps Lock key) do not behave correctly (e.g. The user has to
	//			press Caps Lock to turn on Caps Lock but they Press Shift+Caps Lock to turn it off). This stops the
	//			key being usable in game. We use the US keyboard layout to work around this issue. This allows the user
	//			to change this to their own layout so that they can enter text in their own language.
	static void SetKeyboardLayoutSwitchingEnable(bool enable);

	// PURPOSE: Updates the keyboard layout settings.
	// NOTES:	This must be called on the thread that created the window and processes the windows messages.
	static void UpdateKeyboardLayoutSettings();

	// PURPOSE:	Recapture any devices that are lost.
	static void RecaptureLostDevices();

	// PURPOSE:	Return true if we're ignoring input
	static bool IsIgnoringInput() {return sm_IgnoreInput;}

	enum { LEGACY_KEY_CODE_BIT_OFFSET = 16 };
#endif

#if IME_TEXT_INPUT
	// PURPOSE:	Starts an IME text input session.
	static void ImeStartTextInput();

	// PURPOSE:	Updates the IME composition text.
	// NOTES:	The composition test is the text being entered.
	static void ImeUpdateCompositionText();

	// PURPOSE:	Updates the IME text candidates.
	// NOTES:	The text candidates are the available candidates that the entered text could be. These are paged so there
	//			could be more than available than we return. The system handles the paging.
	static void ImeUpdateCandidateTexts();

	// PURPOSE:	Stops an IME text input session.
	static void ImeStopTextInput();

	// PURPOSE:	Retrieves the IME candidate that the user selected.
	// NOTES:	The selected text will be added to the text available in GetInputText().
	static void ImeRetrieveTextResult();

	// PURPOSE:	Indicates if an IME text input session is in progress.
	static bool ImeIsInProgress();

	// PURPOSE:	Indicates if there are any IME text candidates available.
	static bool ImeHasCandidates();

	// PURPOSE:	Indicates the number of IME text candidates available.
	static u32 ImeGetNumberOfCandidates();

	// PURPOSE:	Indicates which text candidate is currently selected by the user.
	// RETURNS:	The selected candidate index or IME_INVALID_CANDIDATE_INDEX if no candidate is selected.
	// NOTES:	This is used so that we can highlight the relevant candidate and can be passed in to ImeGetCandidateText()
	//			to get the relevant text (unless IME_INVALID_CANDIDATE_INDEX is returned). The candidate selection is
	//			handled by the system.
	static u32 ImeGetSelectedCandidateIndex();

	// PURPOSE:	Retrieves the text of a candidate.
	// PARAMS:	candidateIndex - The index of a candidate. This must be between 0 and ImeGetNumberOfCandidates().
	static const char16* ImeGetCandidateText(u32 candidateIndex);

	// PURPOSE:	Retrieves the composition text.
	// NOTES:	The composition test is the text being entered.
	static const char16* ImeGetCompositionText();

	// PURPOSE:	Maximum number of string candidates.
	// NOTES:	This is 9 as the user can select text from 1-9 by pressing the relevant number.
	const static u32 MAX_IME_CANDIDATES = 9;

	// PURPOSE:	Indicates an invalid candidate index.
	const static u32 IME_INVALID_CANDIDATE_INDEX = MAX_IME_CANDIDATES;
#endif // IME_TEXT_INPUT

#if RSG_DURANGO
	// PURPOSE:	Initialise keyboard from the main application thread.
	// NOTES:	This would usually be done in Begin(), but that occurs from the update thread, where we can't
	//          access the main window. Instead, initialise the keyboard early on, from the main application thread.
	static void InitKeyboardFromMainThread();
#endif // RSG_DURANGO

private:
	static unsigned char sm_Keys[2][256];
	static unsigned char sm_CurrentState[256];

#if RSG_DURANGO
	// PURPOSE:	Buffer to store key up events.
	// NOTES:	To fix url:bugstar:1734231 where pressing space when an assert is up we buffer all key up events
	//			into sm_KeyUpBufferState instead of sm_CurrentState. This gets copied over sm_CurrentState during
	//			Update(). This ensures we have at least one frame of the key being down (in cases where the key down
	//			and key up events are fired in the same frame). A value of true means the key up event occurred.
	static bool sm_KeyUpBufferState[256];

	// PURPOSE: The last key that was pressed. Since KeyDown and Char events are separate, and for platform consistency we need
	// to report them together when calling SetTextCharPressed, we need to remember the last key pressed before a Char event came in.
	static ioMapperParameter sm_LastKeyDown;
#endif // RSG_DURANGO

	static int sm_Active;
	static bool sm_UseKeyboardInBackground;
	static char16 sm_TextBuffer[TEXT_BUFFER_LENGTH];
	static char16 sm_CurrentTextBuffer[TEXT_BUFFER_LENGTH];
	static ioMapperParameter sm_CurrentTextBufferKeys[TEXT_BUFFER_LENGTH];
	static u32  sm_CurrentLength;

#if __PPU
	static bool sm_UseKeypadMode;
#endif // __PPU

#if RSG_PC
	static bool sm_UsingRawInput;
	static bool sm_CapsLockToggled;
	static bool sm_AllowLocalKeyboardLayout;
	static bool	sm_DiDeviceLost;
	static bool sm_IgnoreInput;
#endif // RSG_PC

#if IME_TEXT_INPUT
	// PURPOSE:	Initialize IME text input.
	static void ImeBegin();

	// PURPOSE:	Shuts down IME text input.
	static void ImeEnd();

	// PURPOSE:	Updates the IME internal state.
	static void ImeUpdate();

	enum ImeStateType
	{
		// PURPOSE: The index of the active IME state.
		IME_ACTIVE_STATE = 0,

		// PURPOSE:	The index of the currently editing IME state.
		IME_CURRENT_STATE,

		// PURPOSE:	The number of IME editing states.
		IME_NUM_STATES,
	};

	// PURPOSE:	Contains the state of IME text input.
	struct ImeState
	{
		// PURPOSE: Constructs an IME state.
		ImeState();

		// PURPOSE: Clears an IME state.
		void Clear();

		// PURPOSE:	The number of IME candidates available.
		u32 m_NumCandidates;

		// PURPOSE: The index of the selected IME candidate.
		u32 m_SelectedCandidateIndex;

		// PURPOSE:	The IME composition text.
		char16 m_CompositionText[TEXT_BUFFER_LENGTH];

		// PURPOSE:	The IME candidates.
		char16 m_Candidates[MAX_IME_CANDIDATES][TEXT_BUFFER_LENGTH];

		// PURPOSE: Indicates that IME text input is in progress.
		bool m_InProgress;
	};

	// PURPOSE:	The states of the IME text input.
	static ImeState sm_ImeState[IME_NUM_STATES];

#endif // IME_TEXT_INPUT

#if RSG_ORBIS
	static bool StartKeyboard();
	static bool sm_bShutdown;
	static bool sm_bStart;
#endif
};

extern ioKeyboard KEYBOARD;


inline int ioKeyboard::KeyDown(int key) {
	return sm_Keys[sm_Active][key];
}

inline int ioKeyboard::KeyDown(int key1,int key2) {
	return KeyDown(key1) | KeyDown(key2); 
}

inline int ioKeyboard::KeyUp(int key) {
	return !KeyDown(key);
}

inline int ioKeyboard::KeyChanged(int key) {
	return sm_Keys[0][key] ^ sm_Keys[1][key];
}

inline int ioKeyboard::KeyPressed(int key) {
	return sm_Keys[sm_Active][key] & KeyChanged(key);
}

inline int ioKeyboard::KeyReleased(int key) {
	return sm_Keys[sm_Active^1][key] & KeyChanged(key);
}

inline void ioKeyboard::SetKey(int key,bool active) {
	sm_CurrentState[key] = (unsigned char)(active << 7);
}

#if !__FINAL
inline void ioKeyboard::OverrideKey(int key,bool active) {
	sm_Keys[sm_Active][key] = (unsigned char)(active << 7);
}
#endif // !__FINAL


inline bool ioKeyboard::GetUseKeyboardInBackground()
{
	return sm_UseKeyboardInBackground;
}

inline void ioKeyboard::SetUseKeyboardInBackground(bool b)
{
	sm_UseKeyboardInBackground = b;
}

#if !__PPU
__forceinline void ioKeyboard::SetUseKeypadMode(bool, bool) {}
#endif // !__PPU

inline bool ioKeyboard::AnyKeyChanged()
{
	// Could use vectors for this too?
	const int words = 256/4;
	u32* __restrict keyWords0 = reinterpret_cast<u32*>(&sm_Keys[0][0]);
	u32* __restrict keyWords1 = reinterpret_cast<u32*>(&sm_Keys[1][0]);

	u32 changed = 0;
	for(int i = 0; i < words; i++)
	{
		changed |= keyWords0[i] ^ keyWords1[i];
	}
	return changed != 0;
}

#if RSG_PC
inline bool ioKeyboard::IsUsingRawInput()
{
	return sm_UsingRawInput;
}

inline bool ioKeyboard::IsCapsLockToggled()
{
	return sm_CapsLockToggled;
}

inline void ioKeyboard::SetKeyboardLayoutSwitchingEnable(bool enable)
{
	sm_AllowLocalKeyboardLayout = enable;
}

#endif // RSG_PC

#if IME_TEXT_INPUT
inline const char16* ioKeyboard::ImeGetCompositionText()
{
	return sm_ImeState[IME_ACTIVE_STATE].m_CompositionText;
}

inline bool ioKeyboard::ImeIsInProgress()
{
	return sm_ImeState[IME_ACTIVE_STATE].m_InProgress;
}

inline bool ioKeyboard::ImeHasCandidates()
{
	return ImeGetNumberOfCandidates() > 0;
}

inline u32 ioKeyboard::ImeGetNumberOfCandidates()
{
	return sm_ImeState[IME_ACTIVE_STATE].m_NumCandidates;
}

inline u32 ioKeyboard::ImeGetSelectedCandidateIndex()
{
	return sm_ImeState[IME_ACTIVE_STATE].m_SelectedCandidateIndex;
}

inline const char16* ioKeyboard::ImeGetCandidateText(u32 candidateIndex)
{
	if(inputVerifyf(candidateIndex < ImeGetNumberOfCandidates(), "Invalid index %d, count %d!", candidateIndex, ImeGetNumberOfCandidates()))
	{
		return sm_ImeState[IME_ACTIVE_STATE].m_Candidates[candidateIndex];
	}

	return reinterpret_cast<const char16*>(L""); // Empty string.
}

inline ioKeyboard::ImeState::ImeState()
{
	Clear();
}
#endif // IME_TEXT_INPUT

}	// namespace rage

#endif
