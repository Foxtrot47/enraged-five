#if RSG_DURANGO

#include <xdk.h>
#include "virtualkeyboard.h"
#include "math/amath.h"

namespace rage
{
// Durango has long namespaces that they like to change. We pull them into a smaller usable namespace.
namespace dns
{
	using Windows::Foundation::AsyncOperationCompletedHandler;
	using Windows::Foundation::AsyncStatus;
	using Windows::Foundation::IAsyncOperation;
#if _XDK_VER < 9516
	using Windows::Xbox::Input::VirtualKeyboard;
	using Windows::Xbox::Input::VirtualKeyboardInputScope;
#else
	using Windows::Xbox::UI::SystemUI;
	using Windows::Xbox::UI::VirtualKeyboardInputScope;
#endif
}

// If we had made this a member of ioVirtualKeyboard we would have to reference WinRT types in the header.
static dns::VirtualKeyboardInputScope ConvertToDurangoInputType(rage::ioVirtualKeyboard::eTextType keyboardType)
{
	switch (keyboardType)
	{
	case ioVirtualKeyboard::kTextType_DEFAULT:
		return dns::VirtualKeyboardInputScope::Default;

	case ioVirtualKeyboard::kTextType_EMAIL:
		return dns::VirtualKeyboardInputScope::EmailSmtpAddress;

	case ioVirtualKeyboard::kTextType_PASSWORD:
		return dns::VirtualKeyboardInputScope::Password;

	case ioVirtualKeyboard::kTextType_NUMERIC:
		return dns::VirtualKeyboardInputScope::Number;

	case ioVirtualKeyboard::kTextType_ALPHABET:
		return dns::VirtualKeyboardInputScope::Default;

	default:
		return dns::VirtualKeyboardInputScope::Default;
	}
}

Windows::Foundation::IAsyncOperation<Platform::String^>^ op = nullptr;

bool ioVirtualKeyboard::StartInput( const Params& params )
{
	bool result = false;
	try
	{
		// Setting op->Completed could call the callback so we neet to ensure we m_Cs is not locked by then.
		{
			sysCriticalSection lock(m_Cs);
			Displayf("ioVirtualKeyboard::StartInput()");
			
			Displayf("ioVirtualKeyboard::m_State was %d now %d.", m_State, kKBState_PENDING);
			m_State = kKBState_PENDING;

			Displayf("ioVirtualKeyboard::m_BufferedState was %d now %d.", m_BufferedState, kKBState_PENDING);
			m_BufferedState = kKBState_PENDING;
			m_BufferedResult [0] = L'\0';
			m_MaxLength = params.m_MaxLength;
		}

		Assertf(op == nullptr, "Another keyboard request is already running.");
		// We do not currently support user index numbers on durango as durango does not have this concept. So just use current user until we fix this.
		op = dns::SystemUI::ShowVirtualKeyboardAsync(
			Platform::StringReference(reinterpret_cast<const wchar_t* const>(m_InitialValue)),
			Platform::StringReference( params.m_Title ? reinterpret_cast<const wchar_t* const>(  params.m_Title ) : L"" ),
			Platform::StringReference( params.m_Description ? reinterpret_cast<const wchar_t* const>( params.m_Description ) : L"" ),
			ConvertToDurangoInputType(params.m_KeyboardType));

		if(op != nullptr)
		{
			op->Completed = ref new dns::AsyncOperationCompletedHandler< Platform::String^ >( 
				[this](dns::IAsyncOperation<Platform::String^>^ operation, dns::AsyncStatus status) 
			{
				// Normally I would call a private member of ioVirtualKeyboard here but this would require us to
				// reference WinRT in the header file so I will put the body in this lambda function.
				sysCriticalSection lock(m_Cs);

				Displayf("ioVirtualKeyboard::StartInput::(Lambda Completed Callback):");
				try
				{
					switch (status)
					{
					case dns::AsyncStatus::Completed:
						{
							// According to the Kinect sample, we should keep a reference to the string when accessing its raw
							// data as it may be a copy that is reference counted (and cleaned up when we access the data).
							Platform::String^ enteredText = operation->GetResults();

							safecpy( m_BufferedResult,
								reinterpret_cast<const char16*>(enteredText->Data()),
								Min(m_MaxLength, MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD) );

							Displayf("ioVirtualKeyboard::m_BufferedState set to %d (success)", kKBState_SUCCESS);
							m_BufferedState = kKBState_SUCCESS;
						}
						break;
					case dns::AsyncStatus::Canceled:
						m_BufferedState = kKBState_CANCELLED;
						Displayf("ioVirtualKeyboard::m_BufferedState set to %d (cancelled)", kKBState_CANCELLED);
						break;
					case dns::AsyncStatus::Error:
						m_BufferedState = kKBState_FAILED;
						Displayf("ioVirtualKeyboard::m_BufferedState set to %d (failed - error)", kKBState_FAILED);
						break;
					case dns::AsyncStatus::Started:
						break;
					default:
						m_BufferedState = kKBState_FAILED;
						Displayf("ioVirtualKeyboard::m_BufferedState set to %d (failed - unknown)", kKBState_FAILED);
					}
				}
				catch(Platform::Exception^)
				{
					Assertf(false, "Failed to retrieve result from virtual keyboard!");
					Displayf("ioVirtualKeyboard::m_BufferedState set to %d (failed - exception)", kKBState_FAILED);
					m_BufferedState = kKBState_FAILED;
				}
				op = nullptr;
			}
			);
		}
		result = true;
	}
	catch(Platform::Exception^ e)
	{
		Assertf(false, "Failed to show virtual keyboard");
	}

	return result;
}


void ioVirtualKeyboard::Cancel()
{
	sysCriticalSection lock(m_Cs);
	if(op)
	{
		op->Cancel();
		op = nullptr;
		m_ProcessKeyboard = false;
		m_BufferedState = m_State = kKBState_CANCELLED;
	}
}

}

#endif // RSG_DURANGO