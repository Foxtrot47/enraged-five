<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
							generate="class">

	<hinsert>
#include	"mapper_defs.h"
	</hinsert>

	<autoregister allInFile="true"/>

	<structdef type="rage::InputCalibration::Data" preserveNames="true">
		<enum name="m_Parameter"	type="rage::ioMapperParameter" />
		<int name="m_Value" />
	</structdef>

	<structdef type="rage::InputCalibration::Calibration" preserveNames="true">
		<array name="m_Data"  type="atArray">
			<struct type="rage::InputCalibration::Data" />
		</array>
	</structdef>

	<structdef type="rage::InputCalibration::Calibrations" preserveNames="true">
		<map name="m_Devices" type="atMap" key="atFinalHashString">
			<struct type="rage::InputCalibration::Calibration" />
		</map>
	</structdef>
</ParserSchema>