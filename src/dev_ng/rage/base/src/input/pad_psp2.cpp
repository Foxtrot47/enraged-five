// 
// input/pad_psn.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#if __PSP2

#include "pad.h"

#include "widgetpad.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"

#include <ctrl.h>

#pragma comment(lib,"SceCtrl_stub")

using namespace rage;

static SceCtrlData Data;

void ioPad::BeginAll() {
	sceCtrlSetSamplingMode(SCE_CTRL_MODE_DIGITALANALOG);

	for (int i=0;i<MAX_PADS;i++) {
		sm_Pads[i].m_PadIndex = i;
	}
}

void ioPad::EndAll() {
}

void ioPad::Update() {
	sceCtrlReadBufferPositive(0, &Data, 1);

	m_LastButtons = m_Buttons;		// still need to do this even if not clearing inputs.

#define Translate(a,b)	if (Data.buttons & a) m_Buttons |= b

	Translate(SCE_CTRL_SELECT,SELECT);
	Translate(SCE_CTRL_START,START);
	Translate(SCE_CTRL_Lup,LUP);
	Translate(SCE_CTRL_Lright,LRIGHT);
	Translate(SCE_CTRL_Ldown,LDOWN);
	Translate(SCE_CTRL_Lleft,LLEFT);
	Translate(SCE_CTRL_Rup,RUP);
	Translate(SCE_CTRL_Rright,RRIGHT);
	Translate(SCE_CTRL_Rdown,RDOWN);
	Translate(SCE_CTRL_Rleft,RLEFT);
	Translate(SCE_CTRL_L,L1);
	Translate(SCE_CTRL_R,R1);

#undef Translate

	m_Axis[0] = Data.lx;
	m_Axis[1] = Data.ly;
	m_Axis[2] = Data.rx;
	m_Axis[3] = Data.ry;

#if __BANK
	for ( int i = 0; i < 4; ++i )
	{
		if ( m_bankAxis[i] != 0x80 )
		{
			m_Axis[i] = m_bankAxis[i];
		}
	}

	m_Buttons |= m_bankButtons;

	for ( int i = 0; i < NUMBUTTONS; ++i )
	{
		if ( m_bankAnalogButtons[i] != 0 )
		{
			m_AnalogButtons[i] = m_bankAnalogButtons[i];
			m_AreButtonsAnalog = true;
		}
	}

	ClearBankInputs();
#endif

}

void ioPad::UpdateAll(bool UNUSED_PARAM(ignoreInput), bool UNUSED_PARAM(checkForNewDevice)) {
	sm_HasChanged = false;

	sm_Pads[0].Update();

	sm_Intercepted = (Data.buttons & SCE_CTRL_INTERCEPTED) != 0;
}

void ioPad::StopShakingNow()
{
}


#if __BANK
void ioPad::AddPadWidgets()
{
	bkBank *pBank = BANKMGR.FindBank( "rage - Pad" );
	if ( pBank == NULL )
	{
		pBank = &BANKMGR.CreateBank( "rage - Pad" );
	}

	char name[16];
	sprintf( name, "Pad %d", m_PadIndex );

	if ( bkRemotePacket::IsConnectedToRag() )
	{
		ioWidgetPad* pWidget = rage_new ioWidgetPad( name, m_PadIndex, ioWidgetPad::PS3_PLATFORM );
		pBank->AddWidget( *pWidget );
	}
	else
	{
		pBank->PushGroup( name );
		{
			pBank->PushGroup( "Shoulder Buttons" );
			{
				pBank->AddButton( "L1", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L1 ) );
				pBank->AddButton( "R1", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R1 ) );
				pBank->AddButton( "L2", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L2 ) );
				pBank->AddButton( "R2", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R2 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Left Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData) LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)LDOWN ) );
				pBank->AddButton( "L3", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)L3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Pad" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::ButtonPressedCB ), this,(CallbackData)LRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)LDOWN ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Right Analog Stick" );
			{
				pBank->AddButton( "Up", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RUP ) );
				pBank->AddButton( "Left", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "Right", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this,(CallbackData)RRIGHT ) );
				pBank->AddButton( "Down", datCallback( MFA1( ioPad::AnalogStickPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "R3", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)R3 ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Digital Buttons" );
			{
				pBank->AddButton( "X", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RDOWN ) );
				pBank->AddButton( "O", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RRIGHT ) );
				pBank->AddButton( "[]", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RLEFT ) );
				pBank->AddButton( "^", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)RUP ) );
			}
			pBank->PopGroup();

			pBank->PushGroup( "Other" );
			{
				pBank->AddButton( "Select", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)SELECT ) );
				pBank->AddButton( "Start", datCallback( MFA1( ioPad::ButtonPressedCB ), this, (CallbackData)START ) );
			}
			pBank->PopGroup();
		}
		pBank->PopGroup();
	}
}

void ioPad::AnalogStickPressedCB( CallbackData data )
{
	int button = reinterpret_cast<int>( data );

	switch ( button )
	{
	case LUP:
		m_bankAxis[1] = 0x0;
		m_bankAnalogButtons[LUP_INDEX] = 0xff;
		break;
	case LLEFT:
		m_bankAxis[0] = 0x0;
		m_bankAnalogButtons[LLEFT_INDEX] = 0xff;
		break;
	case LRIGHT:
		m_bankAxis[0] = 0xff;
		m_bankAnalogButtons[LRIGHT_INDEX] = 0xff;
		break;
	case LDOWN:
		m_bankAxis[1] = 0xff;
		m_bankAnalogButtons[LDOWN_INDEX] = 0xff;
		break;
	case RUP:
		m_bankAxis[3] = 0x0;
		m_bankAnalogButtons[RUP_INDEX] = 0xff;
		break;
	case RLEFT:
		m_bankAxis[2] = 0x0;
		m_bankAnalogButtons[RLEFT_INDEX] = 0xff;
		break;
	case RRIGHT:
		m_bankAxis[2] = 0xff;
		m_bankAnalogButtons[RRIGHT_INDEX] = 0xff;
		break;
	case RDOWN:
		m_bankAxis[3] = 0xff;
		m_bankAnalogButtons[RDOWN_INDEX] = 0xff;
		break;
	}
}

void ioPad::ButtonPressedCB( CallbackData data )
{
	int button = reinterpret_cast<int>( data );

	m_bankButtons |= button;

	switch ( button )	
	{
	case L1:
		m_bankAnalogButtons[L1_INDEX] = 0xff;
		break;
	case R1:
		m_bankAnalogButtons[R1_INDEX] = 0xff;
		break;
	case L2:
		m_bankAnalogButtons[L2_INDEX] = 0xff;
		break;
	case R2:
		m_bankAnalogButtons[R2_INDEX] = 0xff;
		break;
	}
}
#endif

#endif	// __PPU
