//
// input/headtracking.h
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_HEAD_TRACKING_H
#define INPUT_HEAD_TRACKING_H

#include "data/base.h"
#include "data/callback.h"

#include "vector/vector3.h"
#include "vector/quaternion.h"


namespace rage {

class ioHeadTracking : public datBase
{
public:
	// PURPOSE: Initialize class.  Currently called automatically by graphics pipeline.
	// PARAMS: inWindow - True if running in a window, else false
	static void Begin(bool inWindow);

	// PURPOSE: Shuts down class.  Currently called automatically by graphics pipeline.
	static void End();

	static void Update(float fPredictedDeltaTime = 0.0f);

	static void Reset(); // Reset current orientation
	static bool IsMotionTrackingEnabled();
	static void EnableMotionTracking(bool bEnable = true);
	static bool UseFPSCamera() { return sm_bUseFPSCamera; }
	static void EnableFPSCamera(bool bEnable = true) { sm_bUseFPSCamera = bEnable; }

	static Quaternion	GetOrientation() { return sm_qOrientation; }
	static Vector3		GetRelativeOffset() { return sm_vRelativeOffset; }
   // Obtain the last absolute acceleration reading, in m/s^2.
    static Vector3		GetAcceleration() { return sm_vAcceleration; }
    // Obtain the last angular velocity reading, in rad/s.
    static Vector3		GetAngularVelocity() { return sm_vAngularVelocity; }
	static float		GetPlayerHeight() { return sm_fPlayerHeight; }

	static void SetPredicatedTime(float fExpectedDeltaTime) { sm_fTimePredicted = fExpectedDeltaTime; }

#if RSG_PC
	static void DeviceLostCB();
	static void DeviceResetCB();
#endif 
	static bool Initialize();

	BANK_ONLY(static void InitWidgets();)

private:
	static bool			sm_bEnabled;
	static bool			sm_bUseFPSCamera;

	static Quaternion	sm_qOrientation;
	static Vector3		sm_vRelativeOffset;

	static bool			sm_bUsePredicatedMode;
	static float		sm_fTimePredicted;

	static Vector3		sm_vAcceleration; // m/s^2
	static Vector3		sm_vAngularVelocity; // rad/s

	static float		sm_fPlayerHeight;
};

} // namespace rage

#endif // INPUT_HEAD_TRACKING_H