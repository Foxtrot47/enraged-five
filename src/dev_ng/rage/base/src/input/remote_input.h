//
// input/remote_input.h
//
// Copyright (C) 1999-2020 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_REMOTE_INPUT_H
#define INPUT_REMOTE_INPUT_H

#include "remote_input_defs.h"

#if REMOTE_INPUT_SUPPORT

#include "atl/array.h"
#include "atl/singleton.h"
#include "file/handle.h"
#include "vector/color32.h"

#if RSG_ORBIS
class ScePadData;
#elif RSG_PC
struct _XINPUT_STATE;
typedef _XINPUT_STATE XINPUT_STATE;
#endif // RSG_PC

namespace rage {

class ioRemoteInput;
typedef atSingleton<ioRemoteInput> ioRemoteInput_Inst;
	
/*
	PURPOSE
		Handles the remote input that can come over a UDP connection and update the low level devices.
	<FLAG Component>
*/	
class ioRemoteInput
{
	friend class atSingleton<ioRemoteInput>;
#if RSG_DURANGO
	friend class ioRemoteInput_WinRT;
#endif // RSG_DURANGO

public:

	static ioRemoteInput& Get();
	
	static void		Begin();
	static void		Update();
	static void		End();

	static bool		IsEnabled();
	static bool		IsPadConnected(int index);

	// PURPOSE:
	// Checks if the device is connected and has received remote input this frame.
	static bool		IsMouseConnectAndHasPendingInput();

	static void		SetPadVibration(int index, u16 heavyMotor, u16 lightMotor);
	static void		SetPadColor(int index, u8 red, u8 green, u8 blue);
	static void		ResetPadColor(int index);

#if RSG_ORBIS
	void			UpdatePad(int index, ScePadData& data);
#elif RSG_PC
	void			UpdatePad(int index, XINPUT_STATE& state);
#endif // RSG_PC

	void			UpdateMouseButtons();
	
private:
	
	typedef atFixedArray<ioRemotePad,			REMOTE_INPUT_MAX_PADS>	ioRemotePads;
	typedef atFixedArray<ioRemotePad_Output,	REMOTE_INPUT_MAX_PADS>	ioRemoteOutputPads;
	
					ioRemoteInput();
					~ioRemoteInput();
	
	void			Connect();
	void			Disconnect();

	void			Update_Internal();
	void			Update_Input();
	void			Update_XInput();

	void			ProcessOutput();

	bool			ReceiveData(void* data, int size, int maxSize, bool isHeader = false);
	void			SendData(const void* data, int size, u32 clientAddress);
	void			SendRemoteOutput(ioRemoteType type, const void* typeData, u16 typeDataSize, u32 clientAddress);

	void			UpdateRemotePad(const ioRemotePad& pad);
	void			UpdateRemoteMouse(const ioRemoteMouse& mouse);
	void			UpdateRemoteKeyboard(const ioRemoteKeyboard& keyboard);
	
	void			Reset();

	static void		SetupHeader(ioRemoteOutputHeader& header, ioRemoteType type, u16 typeDataSize);
	
	ioRemotePads		m_remotePads;
	ioRemoteMouse		m_remoteMouse;
	ioRemoteKeyboard	m_remoteKeyboard;

	ioRemoteOutputPads	m_remoteOutputPads;
	bool				m_hasPadOutput[REMOTE_INPUT_MAX_PADS];
	
	fiHandle			m_socket;

	u32					m_clientAddress;
	u32					m_previousClientAddress;
	
	u32					m_port;
};

}	// namespace rage

#endif // REMOTE_INPUT_SUPPORT 

#endif // INPUT_REMOTE_INPUT_H