
#define DURANGO_MAX_PADS 1

namespace rage
{

class durangoPads
{
public:
	static u64 GetLastUsedGamePadId();
	static u64 sm_LastUsedGamePadId;
};

class durangoPadState
{
public:
	enum digital
	{
		DURANGO_DIGITAL_A = 0,
		DURANGO_DIGITAL_B,
		DURANGO_DIGITAL_D_UP,
		DURANGO_DIGITAL_D_DOWN,
		DURANGO_DIGITAL_D_LEFT,
		DURANGO_DIGITAL_D_RIGHT,
		DURANGO_DIGITAL_BACK,
		DURANGO_DIGITAL_START,
		DURANGO_DIGITAL_X,
		DURANGO_DIGITAL_Y,
		DURANGO_DIGITAL_LEFT_THUMB,
		DURANGO_DIGITAL_RIGHT_THUMB,
		DURANGO_DIGITAL_LEFT_SHOULDER,
		DURANGO_DIGITAL_RIGHT_SHOULDER,
	};

	enum analogue
	{
		DURANGO_ANALOGUE_LEFT_STICK_X = 0,
		DURANGO_ANALOGUE_LEFT_STICK_Y,
		DURANGO_ANALOGUE_RIGHT_STICK_X,
		DURANGO_ANALOGUE_RIGHT_STICK_Y,
		DURANGO_ANALOGUE_LEFT_TRIGGER,
		DURANGO_ANALOGUE_RIGHT_TRIGGER,
		DURANGO_ANALOGUE_MAX
	};

public:
	durangoPadState()
	{
		int i;
		m_Digital = 0;
		// DURANGO TODO:- Sort out initial value from events or whatever.
		m_IsConnected = true;

		for(i=0; i<DURANGO_ANALOGUE_MAX; i++)
		{
			m_Analogue[i] = 0.0f;
		}
	}
	~durangoPadState()
	{
	}
public:
	// To be called by WinRT based IGamePad code.
	void SetDigitalState(int digitalState)
	{
		m_Digital = digitalState;
	}
	void SetAnalogueState(analogue which, float value)
	{
		m_Analogue[which] = value;
	}
	void SetIsConnected(bool connected)
	{
		m_IsConnected = connected;
	}
	// To be read from pad_durango.cpp
	bool IsPressed(digital which)
	{
		if(m_Digital & (0x1 << (int)which))
		{
			return true;
		}
		return false;
	}
	float GetValue(analogue which)
	{
		return m_Analogue[which];
	}
	bool IsConnected()
	{
		return m_IsConnected;
	}
private:
	bool m_IsConnected;
	int	m_Digital;
	float m_Analogue[DURANGO_ANALOGUE_MAX];
};


class durango_rumble_envelope
{
public:
	durango_rumble_envelope()
	{
	}
	~durango_rumble_envelope()
	{
	}
};

void SetDurangoWinRTSideFunctions(void (*pPadStateFunc)(rage::durangoPadState *pPadStates, int noOfPadStates), 
								  void (*pBeginRumble)(rage::durango_rumble_envelope *pRumbleEnvelope), 
								  void (*pStopRumble)());

};
