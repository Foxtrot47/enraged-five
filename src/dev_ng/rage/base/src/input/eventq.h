//
// input/eventq.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_EVENTQ_H
#define INPUT_EVENTQ_H

#if !__FINAL
#include "system/namedpipe.h"
#endif

namespace rage {

	//DOM-IGNORE-BEGIN

/*
	PURPOSE
		Container structure for a single event object
*/
struct ioEvent {
	// Enumerant for available event types
	enum ioEventType { 
		IO_MOUSE_MOVE,				// Mouse movement.  Queue will merge adjacent move messages when possible
		IO_MOUSE_LEFT_DOWN,			// Left mouse button pressed
		IO_MOUSE_LEFT_UP,			// Left mouse button released
		IO_MOUSE_RIGHT_DOWN,		// Right mouse button pressed
		IO_MOUSE_RIGHT_UP,			// Right mouse button released
		IO_MOUSE_MIDDLE_DOWN,		// Middle mouse button pressed
		IO_MOUSE_MIDDLE_UP,			// Middle mouse button released
		IO_MOUSE_X1_DOWN,			// First X mouse button pressed
		IO_MOUSE_X1_UP,				// First X mouse button released
		IO_MOUSE_X2_DOWN,			// Second X mouse button pressed
		IO_MOUSE_X2_UP,				// Second X mouse button released
		IO_KEY_DOWN,				// Key down (stored as virtual key, or KEY_... value as per input/keys.h)
		IO_KEY_CHAR,				// ASCII representation of keypress (with typematic repeat, suitable for user input)
		IO_KEY_UP,					// Key up (stored as virtual key, or KEY_... value as per input/keys.h)
		IO_USER_COMMAND				// User-defined command
	} m_Type;	// Event type
	int m_X,	// Event X coordinate (only valid if mouse event)
		m_Y;	// Event Y coordinate (only valid if mouse event)
	int m_Z;	// Event Z (mouse wheel) coordinate (only valid if mouse event)
	int m_Data;		// New mouse button (if mouse event) or ascii code of key (if key event)
};

// DOM-IGNORE-END

//
//	PURPOSE
//		Implements a global event queue for an application.  This class is a bit Win32-centric
//		and can be viewed as an abstraction of the Windows message queue since it offers things
//		an interactive application will want -- mouse and keyboard messages.
//
//		This class is fed by InputWindowProc in winpriv.cpp, our default Window message handler.
//		<FLAG Component>
//
class ioEventQueue {
public:
    enum { COMMAND_SIZE = 5 };

	//
	// PURPOSE
	//	connects to a named pipe with the prefix contained in pipePrefix.
	// PARAMS
	//	pipeName - the name of the pipe
	//  addr - the address to use if we connect using sockets
	//  port - the socket port to use if we connect using sockets
    //  commandBufferSize - the size of the buffer to allocate.  It should be a multiple of COMMAND_SIZE.  If not, the
    //    size will be increased to the next nearest multiple.
	//
	static void ConnectPipe( const char* pipeName, const char* addr, int port, int commandBufferSize=COMMAND_SIZE*256 );

    //
    // PURPOSE
    //  disconnects the pipe and frees the command buffer
    //
    static void DisconnectPipe();

	// PURPOSE: Pop an event from the event queue, if one is available
	// PARAMS: ev - Variable receiving event (valid only if function returns true)
	// RETURNS: True if event was available, else false
	static bool Pop(ioEvent& ev);

	// PURPOSE: Inserts an arbitrary event into the head of the queue
	// PARAMS: type - ioEventType from ioEvent structure
	//		x - X window-relative position of mouse (if mouse message)
	//		y - Y window-relative position of mouse (if mouse message)
	//		data - Button state, key data, or user payload
	// NOTES: No error is returned on a full queue.  The system maintains the
	//	event queue even if applications don't use it, so an error spew would
	//	be really irritating.
	static void Queue(ioEvent::ioEventType type,int x,int y,int data);

	// PURPOSE: Obtain the next event from the queue without removing it
	// PARAMS: ev - Variable receiving event (valid only if function returns true)
	//		offset - Offset from start of queue to get event.  If a valid event
	//			is returned, offset is also incremented
	// RETURNS: True if event was available, else false
	// NOTES:
	// To scan the event queue without draining it, do this:
	// <CODE>
	// int offset = 0;
	// ioEvent EV;
	// while (Peek(EV,offset)) {
	//   // process events ...
	// } 
	// </CODE>
	static bool Peek(ioEvent& ev,int &offset);

	// PURPOSE: Resets the contents of the event queue
	static void Drain();

	// PURPOSE: Queue an arbitrary command in the queue (for an application-level message queue)
	// PARAMS: arg - Arbitrary value
	// NOTES: An event of type userCommand with m_Data=arg is placed into the event queue
	//		This function just calls Queue to do the real work
	static void Command(void *arg);

	//
	// PURPOSE
	//	translates a virtual Windows key to the RAGE keyboard key.
	// PARAMS
	//	vkey - the virtual key to translate
	static int TranslateKey(unsigned vkey);

	// PURPOSE: updates input from the pipe
	static void UpdateFromPipe();

#if __WIN32PC && __BANK
	static bool (*ShouldPostMessages)();
#endif

private:
	enum { MAX_EVENTS = 32 };

	static int sm_Head, sm_Tail;
#if !__FINAL
	static sysNamedPipe sm_Pipe;
#endif
	static ioEvent sm_TheQueue[MAX_EVENTS];

	struct SCommand
	{
		union 
		{ 
			float f; 
			unsigned u; 
		};
	};

	static SCommand *sm_pBuffer;
    static int sm_numCommands;
	static int sm_start;
	static int sm_count;
};


inline void ioEventQueue::Drain() 
{ 
	sm_Tail = sm_Head; 
}

// All event queue operations should go through this global object.
extern ioEventQueue EVENTQ;
}

#endif // INPUT_EVENTQ_H
