//
// input/joystick.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __WIN32PC

#include "joystick.h"

#include "input_channel.h"
#include "mapper.h"

#include "parser/manager.h"
#include "profile/telemetry.h"
#include "system/alloca.h"
#include "system/param.h"
#include "string/string.h"
#include "string/unicode.h"


#if __WIN32
#include "winpriv.h"
extern HWND g_hwndMain;

rage::dev_float rage::ioJoystick::sm_JoystickAxisTypeDetection = 0.05f;

rage::atFixedString<rage::RAGE_MAX_PATH> rage::ioJoystick::sm_userCalibrationFile;

#endif

#define ESTIMATED_CENTER_POS	32768

using namespace rage;

static int s_joyDebug;

// initiate the number of arrays for stick
ioJoystick ioJoystick::sm_Sticks[MAX_STICKS];
int ioJoystick::sm_StickCount;

#if DIRECTINPUT_VERSION > 0x300

CompileTimeAssert(ioJoystick::IOJ_INFINITE == INFINITE);

#if __WIN32
int ioJoystick::GetDeviceInfo(DIDEVICEINSTANCEW &info) const
{
	Assert(m_Device);
	info.dwSize = sizeof(DIDEVICEINSTANCEW);
	return m_Device->GetDeviceInfo(&info);
}

int ioJoystick::GetDevType() const
{
	DIDEVICEINSTANCEW deviceInfo;
	GetDeviceInfo(deviceInfo);
	return deviceInfo.dwDevType;
}

void ioJoystick::GetProductName(u16 *_name, u32 size) const
{
#if __UNICODE
	CompileTimeAssert(sizeof(u16)==sizeof(WCHAR));
	WCHAR *name = (WCHAR*)_name;
	DIDEVICEINSTANCEW deviceInfo;
	CompileTimeAssert(sizeof(u16)==sizeof(deviceInfo.tszProductName[0])); // make sure we're getting a wide string
	GetDeviceInfo(deviceInfo);
	wcsncpy(name,deviceInfo.tszProductName,size-1);
	name[size-1]=0;
#else // __UNICODE
	inputErrorf("ioJoystick::GetProductName() - not supported with __UNICODE==0");
	size=size;
	_name[0]=0;
#endif // __UNICODE
}

bool ioJoystick::IsValid() const
{
	return m_Device!=0;
}

BOOL CALLBACK ioJoystick::EnumDeviceProc(LPCDIDEVICEINSTANCEW lpddi,LPVOID /*pvRef*/) {
#if __UNICODE
#if !__NO_OUTPUT
	// unsigned char type = (unsigned char) lpddi->dwDevType;
	unsigned char subtype = (unsigned char) (lpddi->dwDevType >> 8);
#endif

	USES_CONVERSION;
	CompileTimeAssert(sizeof(WCHAR) == sizeof(char16));
	CompileTimeAssert(sizeof(char16) == sizeof(lpddi->tszProductName[0])); // make sure we're getting a wide string
	const char *productName = W2A((char16*)lpddi->tszProductName);
	const char *instanceName = W2A((char16*)lpddi->tszInstanceName);

	inputDebugf1("Joystick Subtype = %d, Instance = [%s], Product = [%s]",subtype,instanceName,productName);
	char *jt = getenv("JOYSTICK_TYPE");
	if (jt)
	{
		sm_Sticks[sm_StickCount].m_Type=(ioJoystick::EnumJoyType)atoi(jt);
	}
	else if (!stricmp("PSX/USB Pad Adaptor (4-axis, 12-button, POV,effects)", productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_KIKYXSERIES;
	}
	else if (!stricmp("USB PSX JOYDAPTER",productName) ||
		!stricmp("USB PSX JOYDAPTER",instanceName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
	}
	else if (!stricmp("PSX for USB Converter",instanceName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_PSX_TO_USB_CONVERTER;
	}
	else if (!stricmp("DirectPad Pro Controller",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_DIRECTPAD;
	}
	else if (!strncmp("Microsoft SideWinder Game Pad Pro",productName,33) || !strncmp("SideWinder Game Pad Pro",instanceName,23))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_GAMEPADPRO;
	}
	else if (!stricmp("HID-compliant game controller",productName) || !stricmp("USB Human Interface Device",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
	}
    else if (!stricmp("MP-8866 Dual USB Joypad",productName))
	{
        sm_Sticks[sm_StickCount].m_Type=TYPE_DUALJOYPAD;
	}
	else if (!stricmp("MP-866 Dual USB Joypad",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_DUAL_USB_FFJ_MP866;
	}
	else if (!stricmp("Dual USB Force Feedback Joypad (MP-8866)",productName))
	{
        sm_Sticks[sm_StickCount].m_Type=TYPE_DUALJOYPAD;
	}
	else if (!stricmp("GIC USB Joystick",productName))
	{
        sm_Sticks[sm_StickCount].m_Type=TYPE_XKPC2002;
	}
	else if (!stricmp("Logitech WingMan Cordless Gamepad USB",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_LOGITECH_CORDLESS;
	}
	else if (!stricmp("WingMan Cordless Gamepad",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_LOGITECH_CORDLESS;
	}
	else if (!stricmp("SmartJoy PLUS Adapter",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_DUALJOYPAD;
	}
	else if(!stricmp("Psjoy Vibration feedback (USB) Joystick", productName))
	{	
		sm_Sticks[sm_StickCount].m_Type = TYPE_XKPC2002;
	}
	else if(!stricmp("PS Vibration Feedback Converter ", productName))
	{	
		sm_Sticks[sm_StickCount].m_Type = TYPE_XKPC2002;
	}
	else if (!stricmp("MP-8888 USB Joypad",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
	}
	else if (!stricmp("4 axis 16 button joystick",productName)) { // this is how the SmartJoy pads are being reported by DX8 runtime
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
		inputErrorf("*****");
		inputErrorf("***** Do you have a 'smart joy pad' or a 'PS-PC USB XK-PC200x'?");
		inputErrorf("***** Add JOYSTICK_TYPE=2 to your environment for 'smart joy pad'");
		inputErrorf("***** Add JOYSTICK_TYPE=4 to your environment for 'XK-PC200x'");
		inputErrorf("*****");
	}
	else if (!stricmp("Interact Gaming Device",productName)) // the temporary Xbox USB controllers report thisb
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_SMARTJOYPAD;
	}
	else if (!stricmp("XBCD XBox Gamepad",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_XBCD_XBOX_USB;
	}
	else if (!stricmp("TigerGame Xbox to USB Controller",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_TIGERGAME_XBOX_USB;
	}
	else if (!stricmp("XNA Gamepad",productName) || !stricmp("XBOX 360 For Windows (Controller)",productName) ||
		!stricmp("Microsoft Xbox Wired Gamepad (Controller)",productName) ||
		!stricmp("Controller (XBOX 360 For Windows)",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_XNA_GAMEPAD_XENON;
	}
	else if (!stricmp("SmartJoy PLUS USB Adapter",productName))
	{
		sm_Sticks[sm_StickCount].m_Type=TYPE_DUALJOYPAD;
	}
	else if (!strcmp("USB Vibration Gamepad", productName))
	{
		sm_Sticks[sm_StickCount].m_Type = TYPE_XKPC2002;
	}
	else
#endif // __UNICODE
	{
		inputErrorf("UNKNOWN joypad type found.");
		sm_Sticks[sm_StickCount].m_Type=TYPE_UNKNOWN;
	}

#if !__UNICODE
	inputErrorf("ioJoystick::EnumDeviceProc() - not supported with __UNICODE==0");

#endif // !__UNICODE

	sm_Sticks[sm_StickCount].m_ProductGuid	= lpddi->guidProduct;
	sm_Sticks[sm_StickCount].m_InstanceGuid	= lpddi->guidInstance;

	formatf( sm_Sticks[sm_StickCount++].m_ProductGuidStr,
		37,
		"%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
		lpddi->guidProduct.Data1,
		lpddi->guidProduct.Data2,
		lpddi->guidProduct.Data3,
		lpddi->guidProduct.Data4[0],
		lpddi->guidProduct.Data4[1],
		lpddi->guidProduct.Data4[2],
		lpddi->guidProduct.Data4[3],
		lpddi->guidProduct.Data4[4],
		lpddi->guidProduct.Data4[5],
		lpddi->guidProduct.Data4[6],
		lpddi->guidProduct.Data4[7] );

	if(sm_StickCount >= COUNTOF(sm_Sticks))
	{
		return DIENUM_STOP;
	}

	return DIENUM_CONTINUE;
}

#if !__FINAL
void ioJoystick::PrintDeviceCaps() const
{
	Assert(m_Device);
    DIDEVCAPS DevCaps;
	DevCaps.dwSize = sizeof(DIDEVCAPS);
    m_Device->GetCapabilities(&DevCaps);

	DIDEVICEINSTANCEW DevInst;
	DevInst.dwSize = sizeof(DIDEVICEINSTANCEW);
	m_Device->GetDeviceInfo(&DevInst);
	DWORD flags = DevCaps.dwFlags;

	USES_CONVERSION;

	inputDisplayf("JOYSTICK DEVICE DESCRIPTION:");

	CompileTimeAssert(sizeof(WCHAR) == sizeof(char16));
	CompileTimeAssert(sizeof(char16) == sizeof(DevInst.tszProductName[0]));
	inputDisplayf("%s", W2A((char16*)DevInst.tszInstanceName));
	inputDisplayf("%s", W2A((char16*)DevInst.tszProductName));

	inputDisplayf("GUID Product Code = %x", DevInst.guidProduct);
	inputDisplayf("Axes = %d, Butons = %d, POVs = %d", DevCaps.dwAxes, DevCaps.dwButtons, DevCaps.dwPOVs);
	inputDisplayf("The Joystick Sub-type returned indicates:");
	//Microsoft SideWinder game pad
	//Microsoft SideWinder Force Feedback Wheel
	//Microsoft SideWinder Force Feedback Pro
	/*
	switch (GET_DIDEVICE_SUBTYPE(DevInst.dwDevType)){
		case DIDEVTYPEJOYSTICK_TRADITIONAL:
			inputDisplayf("A traditional joystick.");
			break;
  		case DIDEVTYPEJOYSTICK_FLIGHTSTICK:
			inputDisplayf("A joystick optimized for flight simulation.");
			break;
		case DIDEVTYPEJOYSTICK_GAMEPAD:
			inputDisplayf("A device whose primary purpose is to provide button input.");
			break;
		case DIDEVTYPEJOYSTICK_RUDDER:
			inputDisplayf("A device for yaw control.");
			break;
		case DIDEVTYPEJOYSTICK_WHEEL:
			inputDisplayf("A steering wheel.");
			break;
		case DIDEVTYPEJOYSTICK_HEADTRACKER:
			inputDisplayf("A device that tracks the movement of the user's head");
			break;
		default:
		case DIDEVTYPEJOYSTICK_UNKNOWN:
			inputDisplayf("The subtype could not be determined.");
			break;
	}
	*/
	inputDisplayf("Device Caps::Flags indicate device has:");
	if (flags & DIDC_ATTACHED) 
	{
		inputDisplayf("Been physically attatched");
	}
	if (flags & DIDC_DEADBAND) 
	{
		inputDisplayf("Support for deadband");
	}
	if (flags & DIDC_EMULATED) 
	{
		inputDisplayf("Emulated functionality");
	}
	if (flags & DIDC_FORCEFEEDBACK) 
	{
		inputDisplayf("Force-feedback");
	}
	if (flags & DIDC_FFFADE) 
	{
		inputDisplayf("Support for fade(FF)");
	}
	if (flags & DIDC_FFATTACK) 
	{
		inputDisplayf("Support for Attack env (FF)");
	}
	if (flags & DIDC_POLLEDDATAFORMAT) 
	{
		inputDisplayf("One device (at least) that is polled in the current data format");
	}
	if (flags & DIDC_POLLEDDEVICE) 
	{
		inputDisplayf("One device (at least) that is polled");
	}
	if (flags & DIDC_POSNEGCOEFFICIENTS) 
	{
		inputDisplayf("Support for two coefficient values for FF displacement");
	}
	if (flags & DIDC_POSNEGSATURATION) 
	{
		inputDisplayf("Support for max FF saturation");
	}
	if (flags & DIDC_SATURATION) 
	{
		inputDisplayf("Support for condition saturation");
	}

}
#else // !__FINAL
// void ioJoystick::PrintDeviceCaps(){}
#endif // !__FINAL

int __stdcall ioJoystick::EnumObjectProc(LPCDIDEVICEOBJECTINSTANCEW lpddoi,LPVOID pvRef) {
	// ioJoystick *ths = (ioJoystick*)(pvRef);

	ioJoystick *device = (ioJoystick*)pvRef;

	if ((lpddoi->dwType & DIDFT_ABSAXIS) != 0 || (lpddoi->dwType & DIDFT_RELAXIS) != 0)
	{
		int axisId = NUM_AXES;

		if( lpddoi->guidType == GUID_XAxis )
		{
			axisId = AXIS_LX;
		}
		if( lpddoi->guidType == GUID_YAxis )
		{
			axisId = AXIS_LY;
		}
		if( lpddoi->guidType == GUID_ZAxis )
		{
			axisId = AXIS_LZ;
		}
		if( lpddoi->guidType == GUID_RxAxis )
		{
			axisId = AXIS_LRX;
		}
		if( lpddoi->guidType == GUID_RyAxis )
		{
			axisId = AXIS_LRY;
		}
		if( lpddoi->guidType == GUID_RzAxis )
		{
			axisId = AXIS_LRZ;
		}
		if( lpddoi->guidType == GUID_Slider )
		{
			if(device->m_NumSliders == 0)
			{
				axisId = AXIS_SLIDER0;
				++device->m_NumSliders;
			}
			else if(device->m_NumSliders == 1)
			{
				axisId = AXIS_SLIDER1;
				++device->m_NumSliders;
			}
		}
	
		if(axisId != NUM_AXES)
		{
			DIPROPRANGE range;
			range.diph.dwSize = sizeof(DIPROPRANGE);
			range.diph.dwHeaderSize = sizeof(DIPROPHEADER);
			range.diph.dwHow = DIPH_BYID;
			range.diph.dwObj = lpddoi->dwType;
		
			if (device->m_Device->GetProperty(DIPROP_RANGE, &range.diph) == DI_OK && range.lMin != DIPROPRANGE_NOMIN && range.lMax != DIPROPRANGE_NOMAX)
			{
				device->m_AxesMax[axisId] = range.lMax;
				device->m_AxesMin[axisId] = range.lMin;
			}

			// We keep track of whether this input is enabled because different controllers can have different configurations of axis.
			device->m_AxesEnabled[axisId] = true;
		}
	}
	
    
#if __DEV
	unsigned didft = lpddoi->dwType;
	unsigned short instNum = DIDFT_GETINSTANCE(lpddoi->dwType);

	USES_CONVERSION;
	inputDisplayf("Instnum %d, Type: ",instNum);
	if (didft & DIDFT_ABSAXIS) 
	{
		inputDisplayf("ABS ");
	}
	if (didft & DIDFT_RELAXIS) 
	{
		inputDisplayf("REL ");
	}
	if (didft & DIDFT_AXIS) 
	{
		inputDisplayf("AXIS ");
	}
	if (didft & DIDFT_TGLBUTTON) 
	{
		inputDisplayf("TOGGLE ");
	}
	if (didft & DIDFT_PSHBUTTON) 
	{
		inputDisplayf("PUSH ");
	}
	if (didft & DIDFT_BUTTON) 
	{
		inputDisplayf("BUTTON ");
	}
	if (didft & DIDFT_POV) 
	{
		inputDisplayf("POV ");
	}
	CompileTimeAssert(sizeof(WCHAR) == sizeof(char16));
	CompileTimeAssert(sizeof(char16) == sizeof(lpddoi->tszName[0]));
	inputDisplayf("named [%s]",W2A((char16*)lpddoi->tszName));
#else // __DEV
	lpddoi;
#endif // __DEV

	return DIENUM_CONTINUE;
}


int ioJoystick::EnumFFAxisProc(const DIDEVICEOBJECTINSTANCEW* lpsddoi,void* data)
{
	int* numAxis = (int*)data;

    if((lpsddoi->dwFlags & DIDOI_FFACTUATOR) != 0)
	{
        (*numAxis)++;
	}

    return DIENUM_CONTINUE;
}

void ioJoystick::ApplyForce(s32 X, s32 Y, s32 timeMS)
{
	if(m_ForceType == IOJF_NONE || m_Effect == NULL)
	{
		return;
	}

	// scale from game range to controller range.
	X = (ClampIt(X, -IOJ_MAX_FORCE, IOJ_MAX_FORCE) * DI_FFNOMINALMAX) / IOJ_MAX_FORCE;
	Y = (ClampIt(Y, -IOJ_MAX_FORCE, IOJ_MAX_FORCE) * DI_FFNOMINALMAX) / IOJ_MAX_FORCE;

	// Modifying an effect is basically the same as creating a new one, except
	// you need only specify the parameters you are modifying
	LONG rglDirection[2] = { 0, 0 };

	DICONSTANTFORCE cf;

	if(m_NumFFAxis == 1 )
	{
		// If only one force feedback axis, then apply only one direction and 
		// keep the direction at zero
		cf.lMagnitude = X;
		rglDirection[0] = 1;
	}
	else
	{
		// If two force feedback axis, then apply magnitude from both directions 
		rglDirection[0] = X;
		rglDirection[1] = Y;
		cf.lMagnitude = (DWORD)sqrt(static_cast<double>(X) * static_cast<double>(X) +
			static_cast<double>(Y) * static_cast<double>(Y));

		cf.lMagnitude = ClampIt(cf.lMagnitude, -DI_FFNOMINALMAX, DI_FFNOMINALMAX);
	}

	DIEFFECT eff;
	ZeroMemory( &eff, sizeof( eff ) );
	eff.dwSize = sizeof( DIEFFECT );
	eff.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
	eff.cAxes = m_NumFFAxis;
	eff.rglDirection = rglDirection;
	eff.lpEnvelope = 0;
	eff.cbTypeSpecificParams = sizeof( DICONSTANTFORCE );
	eff.lpvTypeSpecificParams = &cf;
	eff.dwStartDelay = 0;
	eff.dwDuration = (timeMS == IOJ_INFINITE) ? timeMS : (timeMS * IOJ_TIMESCALE); //  dwDuration is in microseconds and timeMS is in milliseconds.
	eff.dwSamplePeriod = 0;
	eff.dwGain = DI_FFNOMINALMAX;
	eff.dwTriggerButton = DIEB_NOTRIGGER;
	eff.dwTriggerRepeatInterval = 0;

	// Now set the new parameters and start the effect immediately.
	m_Effect->SetParameters( &eff, DIEP_DIRECTION |
		DIEP_TYPESPECIFICPARAMS |
		DIEP_START |
		DIEP_DURATION );
}

void ioJoystick::StopAllForces()
{
	for (s32 i = 0; i < sm_StickCount; ++i)
	{
		sm_Sticks[i].ApplyForce(0, 0, IOJ_INFINITE);
	}
}

void ioJoystick::CaptureDevice()
{
	inputDebugf2("Recapturing DirectInput gamepad.");
	WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioJoystick::CaptureDevice() - GetDeviceState()"));
	HRESULT res = m_Device->Acquire();
	WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	if(res == DI_OK && m_ForceType != IOJF_NONE && m_Effect == NULL)
	{
		inputDebugf1("Recaptured DirectInput gamepad.");
		// currently we only support two directions.
		if(m_NumFFAxis > 2)
		{
			m_NumFFAxis = 2;
		}

		// This application needs only one effect: Applying raw forces.
		DWORD rgdwAxes[2] = { DIJOFS_X, DIJOFS_Y };
		LONG rglDirection[2] = { 0, 0 };
		DICONSTANTFORCE cf = { 0 };

		DIEFFECT eff;
		ZeroMemory( &eff, sizeof( eff ) );
		eff.dwSize = sizeof( DIEFFECT );
		eff.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
		eff.dwDuration = INFINITE;
		eff.dwSamplePeriod = 0;
		eff.dwGain = DI_FFNOMINALMAX;
		eff.dwTriggerButton = DIEB_NOTRIGGER;
		eff.dwTriggerRepeatInterval = 0;
		eff.cAxes = m_NumFFAxis;
		eff.rgdwAxes = rgdwAxes;
		eff.rglDirection = rglDirection;
		eff.lpEnvelope = 0;
		eff.cbTypeSpecificParams = sizeof( DICONSTANTFORCE );
		eff.lpvTypeSpecificParams = &cf;
		eff.dwStartDelay = 0;

		// Create the prepared effect
		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioJoystick::CaptureDevice() - CreateEffect()"));
		res = m_Device->CreateEffect(GUID_ConstantForce, &eff, &m_Effect, NULL);
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
		if(inputVerifyf(res == DI_OK, "Failed to create force feedback effect, error code 0x%X", res) && m_Effect != NULL)
		{
			WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioJoystick::CaptureDevice() - Start()"));
			m_Effect->Start(1, 0);
			WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
		}

		m_DiDeviceLost = false;
	}
	else if(res == DI_OK)
	{
		inputDebugf1("Recaptured DirectInput gamepad.");
		m_DiDeviceLost = false;
	}
	// Apparently S_FALSE means we already have it acquired.
	else if(res == S_FALSE)
	{
		inputDebugf1("DirectInput gamepad already captured.");
		m_DiDeviceLost = false;
	}
	else
	{
		inputDebugf2("Failed to recaptured DirectInput gamepad (0x%08X)", res);
		m_DiDeviceLost = true;
	}
}

void ioJoystick::RecaptureLostDevices()
{
	for(int i = 0; i < sm_StickCount; ++i)
	{
		if(sm_Sticks[i].m_Device && sm_Sticks[i].m_DiDeviceLost)
		{
			sm_Sticks[i].CaptureDevice();
		}
	}
}

void ioJoystick::BeginAndCalibrateAll(const char* defaultCalibrationFile, const char* userCalibrationFile)
{
	BeginAll();
	sm_userCalibrationFile = userCalibrationFile;

	INIT_PARSER;

	// load calibration data.
	InputCalibration::Calibrations calibrations;

	// If the file exists and we FAILED to load it.
	if(    ASSET.Exists(defaultCalibrationFile, "")
		&& !inputVerifyf(PARSER.LoadObject(defaultCalibrationFile, "", calibrations), "Error loading default joystick calibration settings!") )
	{
		calibrations.m_Devices.Reset();
	}

	// load the user configuration file.
	if(sm_userCalibrationFile.length() > 0 && ASSET.Exists(sm_userCalibrationFile.c_str(), ""))
	{
		InputCalibration::Calibrations userCalibrations;
		if(inputVerifyf(PARSER.LoadObject(sm_userCalibrationFile.c_str(), "", userCalibrations), "Error loading default joystick calibration settings!"))
		{
			// 
			atMap<atFinalHashString, InputCalibration::Calibration>::Iterator iterator = userCalibrations.m_Devices.CreateIterator();
			for(iterator.Start(); !iterator.AtEnd(); iterator.Next())
			{
				calibrations.m_Devices[iterator.GetKey()] = iterator.GetData();
			}
		}
	}

	SHUTDOWN_PARSER;

	for(int deviceIndex = 0; deviceIndex < sm_StickCount; ++deviceIndex)
	{
		ioJoystick& stick = sm_Sticks[deviceIndex];

		// now calibrate the devices.
		InputCalibration::Calibration* calibration = calibrations.m_Devices.Access(atFinalHashString(stick.GetProductGuidStr()));
		if(calibration != NULL)
		{
			for(int axisIndex = 0; axisIndex < NUM_AXES; ++axisIndex)
			{
				ioMapperParameter param = ioMapper::ConvertParameterToMapperValue(IOMS_JOYSTICK_AXIS, axisIndex);

				bool found = false;
				// loop through and find parameter as there is no guarantee of the order.
				for(int dataIndex = 0; !found && dataIndex < calibration->m_Data.size(); ++dataIndex)
				{
					if(calibration->m_Data[dataIndex].m_Parameter == param)
					{
						stick.m_AxesCalibration[axisIndex] = calibration->m_Data[dataIndex].m_Value;
						found = true;
					}
				}
			}

			stick.m_IsCalibrated = true;
		}
	}
}

bool ioJoystick::SaveUserCalibrations(bool overwriteMallFormedFiles)
{
	bool saved = false;

	INIT_PARSER;

	if(sm_userCalibrationFile.length() > 0)
	{
		InputCalibration::Calibrations calibrations;

		bool fileExists = ASSET.Exists(sm_userCalibrationFile.c_str(), "");
		bool fileLoaded = false;
		if(fileExists)
		{
			parSettings settings;
			// we want to detect errors when parsing the xml.
			settings.SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
			fileLoaded = PARSER.LoadObject(sm_userCalibrationFile.c_str(), "", calibrations, &settings);
			inputAssertf(fileLoaded, "Failed to load user input calibration file!");
		}

		if(!fileExists || fileLoaded || overwriteMallFormedFiles)
		{
			for(int stickIndex = 0; stickIndex < sm_StickCount; ++stickIndex)
			{
				ioJoystick& stick = sm_Sticks[stickIndex];
				
				// only save calibrated devices.
				if(stick.IsCalibrated())
				{
					// find the corresponding calibration.
					InputCalibration::Calibration& calibration = calibrations.m_Devices[atFinalHashString(stick.GetProductGuidStr())];

					for(int axisIndex = 0; axisIndex < NUM_AXES; ++axisIndex)
					{
						if(stick.m_AxesEnabled[axisIndex])
						{
							bool updated = false;
							ioMapperParameter param = ioMapper::ConvertParameterToMapperValue(IOMS_JOYSTICK_AXIS, axisIndex);

							// find and replace the current entry.
							for(int dataIndex = 0; !updated && dataIndex < calibration.m_Data.size(); ++dataIndex)
							{
								if(calibration.m_Data[dataIndex].m_Parameter == param)
								{
									calibration.m_Data[dataIndex].m_Value = stick.m_AxesCalibration[axisIndex];
									updated = true;
								}
							}

							// If entry doesn't exist then add it.
							if(!updated)
							{
								InputCalibration::Data data;

								data.m_Parameter = param;
								data.m_Value     = stick.m_AxesCalibration[axisIndex];

								calibration.m_Data.PushAndGrow(data);
							}
							
						}
					}
				}
			}

			saved = PARSER.SaveObjectAnyBuild(sm_userCalibrationFile.c_str(), "", &calibrations);
			inputAssertf(saved, "Error saving user input calibration file!");
		}
	}

	SHUTDOWN_PARSER;

	return saved;
}

void ioJoystick::AutoCalibrate(bool overrideCurrentCalibration)
{
	if(m_IsCalibrated == false || overrideCurrentCalibration)
	{
		for(int i = 0; i <  NUM_AXES; ++i)
		{
			m_AxesCalibration[i] = m_Axes[i];
		}
	
		m_IsCalibrated = true;
	}
}

bool ioJoystick::IsAxisTwoDirectional(int axis) const
{
	// if we know the range (we generally always will).
	if(m_AxesMax[axis] != m_AxesMin[axis] && m_IsCalibrated)
	{
		float range = static_cast<float>(m_AxesMax[axis] - m_AxesMin[axis]);
		// the value of the calibrated axis position in the range of 0...1.
		float calPos = static_cast<float>(m_AxesCalibration[axis] - m_AxesMin[axis]) / range;

		// if the calibrated position is near the max value then the input is one directional from max -> min.
		if(IsNearZero(calPos - 1.0f) || IsNearZero(calPos))
		{
			return false;
		}
	}

	return true;
}

#endif // __WIN32


float ioJoystick::GetNormAxis(int axis) const
{
#if __WIN32
	if(!m_AxesEnabled[axis])
	{
		// axis does not exist
		return 0.0f;
	}

	// if we know the range (we generally always will).
	if(m_AxesMax[axis] != m_AxesMin[axis])
	{
		float range = static_cast<float>(m_AxesMax[axis] - m_AxesMin[axis]);

		// the value of the axis position in the range of 0...1.
		float axisNorm = static_cast<float>(m_Axes[axis] - m_AxesMin[axis]) / range;

		if(m_IsCalibrated)
		{
			// the value of the calibrated axis position in the range of 0...1.
			float calPos = static_cast<float>(m_AxesCalibration[axis] - m_AxesMin[axis]) / range;

			// if the calibrated position is near the max value then the input is one directional from max -> min.
			if(IsNearZero(calPos - 1.0f))
			{
				return 1.0f - axisNorm;
			}

			// if the calibrated position is near the min value then the input is one directional from min -> max.
			if(IsNearZero(calPos))
			{
				return axisNorm;
			}
		}
		return axisNorm * 2.0f - 1.0f;
	}
#endif // __WIN32

	// estimate it.
	return ( m_Axes[axis] - ESTIMATED_CENTER_POS) * (1.0f / ESTIMATED_CENTER_POS);
}


void ioJoystick::BeginAll() {
	// ARGS_SET_DEBUG_LEVEL(s_joyDebug);

#if __WIN32
	diInit();

	// We only enumerate attached devices.
	lpDI->EnumDevices(
#if DIRECTINPUT_VERSION > 0x700
		DI8DEVCLASS_GAMECTRL,
#else // DIRECTINPUT_VERSION > 0x700
		DIDEVTYPE_JOYSTICK,
#endif // DIRECTINPUT_VERSION > 0x700
		EnumDeviceProc,0,DIEDFL_ATTACHEDONLY);
#endif // __WIN32

	for (int i=0; i<sm_StickCount; i++)
	{
		sm_Sticks[i].Begin();
	}
}

void ioJoystick::PollAll() {
	for (int i=0; i<sm_StickCount; i++)
	{
		sm_Sticks[i].Poll();
	}
}

void ioJoystick::UpdateAll() {
	for (int i=0; i<sm_StickCount; i++)
	{
		sm_Sticks[i].Update();
	}
}

void ioJoystick::EndAll() {
	while (sm_StickCount)
	{
		sm_Sticks[--sm_StickCount].End();
	}
}

void ioJoystick::Begin() {

	m_IsWheel = false;
	m_HasInputFocus = false;
	m_FirstFrameAfterFocusGain = true;

#if __WIN32
	m_DiDeviceLost = true;

	for(int i = 0; i < MAX_AXES; ++i)
	{
		m_Axes[i] = 0;
		m_AxesMax[i] = -1;
		m_AxesMin[i] = -1;
		m_AxesEnabled[i] = false;
		m_AxesCalibration[i] = 0;
		m_AxisFocusGainValues[i] = 0;
	}
	m_ForceType = IOJF_NONE;
	m_Effect = NULL;
	m_IsCalibrated = false;

	m_NumSliders = 0;

	LPDIRECTINPUTDEVICEW dev = 0;
	ditry(lpDI->CreateDevice(m_InstanceGuid,&dev,0));

#if DIRECTINPUT_VERSION > 0x700
	ditry(dev->QueryInterface(IID_IDirectInputDevice8W,(LPVOID*)&m_Device));
#else // DIRECTINPUT_VERSION > 0x700
	ditry(dev->QueryInterface(IID_IDirectInputDevice2W,(LPVOID*)&m_Device));
#endif // DIRECTINPUT_VERSION > 0x700

	dev->Release();

	if (m_Device)
	{
		DIDEVICEINSTANCEW deviceInfo;
		GetDeviceInfo(deviceInfo);
		m_IsWheel = ((deviceInfo.dwDevType&0xff)== DI8DEVTYPE_DRIVING );
	}

	ditry(m_Device->EnumObjects(EnumObjectProc,this,DIDFT_ALL));

	m_NumFFAxis = 0;
	// Enumerate and count the axes of the joystick 
	ditry(m_Device->EnumObjects(EnumFFAxisProc,(VOID*)&m_NumFFAxis, DIDFT_AXIS));

	ditry(m_Device->SetDataFormat(&c_dfDIJoystick2));

	// Set the cooperative level to let DInput know how this device should
	// interact with the system and with other DInput applications.
	// Exclusive access is required in order to perform force feedback.
	HRESULT result = m_Device->SetCooperativeLevel(g_hwndMain, DISCL_EXCLUSIVE | DISCL_FOREGROUND);
	if(result != DI_OK)
	{
		result = m_Device->SetCooperativeLevel(g_hwndMain, DISCL_FOREGROUND);
	}
	
	m_HasInputFocus = (result == DI_OK);
	if(m_HasInputFocus)
	{
		//DIDEVCAPS dc;
		DIDEVCAPS_DX3 dc;
		memset(&dc,0,sizeof(dc));
		dc.dwSize = sizeof(dc);
		ditry(m_Device->GetCapabilities((DIDEVCAPS*)&dc));
		m_AxisCount = dc.dwAxes;
		m_ButtonCount = dc.dwButtons;
		m_POVCount = dc.dwPOVs;
	
		if((dc.dwFlags & DIDC_FORCEFEEDBACK) == DIDC_FORCEFEEDBACK)
		{
			DIDEVICEINSTANCEW deviceInfo;
			GetDeviceInfo(deviceInfo);

			DWORD type = (deviceInfo.dwDevType & 0xff);
			if(type == DI8DEVTYPE_DRIVING || type == DI8DEVTYPE_FLIGHT || type == DI8DEVTYPE_JOYSTICK)
			{
				m_ForceType = IOJF_DIRECTIONAL;
			}
			else
			{
				m_ForceType = IOJF_RUMBLE;
			}
		}

		CaptureDevice();
	}

#endif // __WIN32
	inputDebugf1("%d axes, %d buttons, %d POV hats",m_AxisCount,m_ButtonCount,m_POVCount);
}

void ioJoystick::Poll() {
	if (m_Device)
	{
		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioJoystick::Poll() - Poll()"));
		m_Device->Poll();
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
	}
}

void ioJoystick::Update() {
#if __WIN32
	if (m_Device && m_DiDeviceLost == false) {

		DIJOYSTATE2 state;
		WIN32PC_ONLY(TELEMETRY_START_ZONE(PZONE_NORMAL, __FILE__,__LINE__,"ioJoystick::Update() - GetDeviceState()"));
		HRESULT res = m_Device->GetDeviceState(sizeof(state),&state);
		WIN32PC_ONLY(TELEMETRY_END_ZONE(__FILE__,__LINE__));
		if(res != DI_OK)
		{
			inputDebugf1("Failed to read gamepad (0x%08X)", res);
			m_DiDeviceLost = true;
			m_HasInputFocus = false;

			m_FirstFrameAfterFocusGain = true;
			FlushValues();
			return;
		}

		unsigned buttons = 0, mask = 1;
		const int numButtons = sizeof(state.rgbButtons) < 30 ? sizeof(state.rgbButtons) : 30;
		for (int i=0; i<numButtons; i++,mask<<=1)
		{
			if (state.rgbButtons[i] & 0x80)
			{
				buttons |= mask;
			}
		}

		m_LastButtons = m_Buttons;
		m_Buttons = buttons;

		sysMemCpy(m_LastAnalogButtons, m_AnalogButtons, MAX_BUTTONS);
		sysMemCpy(m_AnalogButtons, state.rgbButtons, MAX_BUTTONS);

		// hasGarbage is true whilst we do not have focus, its and there is no change in input value between frames.
		// NOTE: These are in this order for early out.
		bool hasGarbage = !m_HasInputFocus &&
						  !m_FirstFrameAfterFocusGain && // Whilst m_FirstFrameAfterFocusGain means it IS garbage, we use not here so we do not do the expensive comparisons.
						  GetChangedButtons() == 0 &&
						  memcmp(m_AnalogButtons, m_LastAnalogButtons, sizeof(m_AnalogButtons)) == 0&&
						  m_AxisFocusGainValues[AXIS_LX] == state.lX &&
						  m_AxisFocusGainValues[AXIS_LY] == state.lY  &&
						  m_AxisFocusGainValues[AXIS_LZ] == state.lZ &&
						  m_AxisFocusGainValues[AXIS_LRX] == state.lRx &&
						  m_AxisFocusGainValues[AXIS_LRY] == state.lRy &&
						  m_AxisFocusGainValues[AXIS_LRZ] == state.lRz &&
						  m_AxisFocusGainValues[AXIS_SLIDER0] == state.rglSlider[0] &&
						  m_AxisFocusGainValues[AXIS_SLIDER1] == state.rglSlider[1] &&
						  m_POVs[0] == (short)state.rgdwPOV[0] &&
						  m_POVs[1] == (short)state.rgdwPOV[1] &&
						  m_POVs[2] == (short)state.rgdwPOV[2] &&
						  m_POVs[3] == (short)state.rgdwPOV[3];

		if(hasGarbage || m_FirstFrameAfterFocusGain)
		{
			// Reset m_FirstFrameAfterFocusGain as this is used on the first frame of receiving input.
			m_FirstFrameAfterFocusGain = false;

			m_AxisFocusGainValues[AXIS_LX] = state.lX;
			m_AxisFocusGainValues[AXIS_LY] = state.lY;
			m_AxisFocusGainValues[AXIS_LZ] = state.lZ;
			m_AxisFocusGainValues[AXIS_LRX] = state.lRx;
			m_AxisFocusGainValues[AXIS_LRY] = state.lRy;
			m_AxisFocusGainValues[AXIS_LRZ] = state.lRz;
			m_AxisFocusGainValues[AXIS_SLIDER0] = state.rglSlider[0];
			m_AxisFocusGainValues[AXIS_SLIDER1] = state.rglSlider[1];
		}
		else
		{
			m_HasInputFocus = true;

			m_Axes[AXIS_LX] = state.lX;
			m_Axes[AXIS_LY] = state.lY;
			m_Axes[AXIS_LZ] = state.lZ;
			m_Axes[AXIS_LRX] = state.lRx;
			m_Axes[AXIS_LRY] = state.lRy;
			m_Axes[AXIS_LRZ] = state.lRz;
			m_Axes[AXIS_SLIDER0] = state.rglSlider[0];
			m_Axes[AXIS_SLIDER1] = state.rglSlider[1];

			inputDebugf3("Axis Values: Device: %s, lX: %d, lY: %d, lZ: %d, lRx: %d, lRy: %d, lRz: %d, Slider 0: %d, Slider 1: %d",
				GetProductGuidStr(),
				state.lX,
				state.lY,
				state.lZ,
				state.lRx,
				state.lRy,
				state.lRz,
				state.rglSlider[0],
				state.rglSlider[1]);
		}


		m_POVs[0] = (short)state.rgdwPOV[0];
		m_POVs[1] = (short)state.rgdwPOV[1];
		m_POVs[2] = (short)state.rgdwPOV[2];
		m_POVs[3] = (short)state.rgdwPOV[3];
	}
	else
	{
		m_FirstFrameAfterFocusGain = true;
		FlushValues();
	}
#endif
}

void ioJoystick::End() {
#if __WIN32

	if(m_Effect) {
		m_Effect->Release();
		m_Effect = 0;
	}
	if (m_Device) {
		m_Device->Unacquire();

		m_Device->Release();
		m_Device = 0;
	}
#endif
}

void ioJoystick::FlushValues()
{
	// Update last button values so we get informed of a release event.
	m_LastButtons = m_Buttons;
	sysMemCpy(m_LastAnalogButtons, m_AnalogButtons, MAX_BUTTONS);

	m_Buttons = 0;
	sysMemSet(m_AnalogButtons, 0, MAX_BUTTONS);

	for(u32 i = 0; i < MAX_AXES; ++i)
	{
		if(m_AxesMax[i] != m_AxesMin[i] && m_IsCalibrated)
		{
			m_Axes[i] = m_AxesCalibration[i];
		}
		else
		{
			// Estimate it.
			m_Axes[i] = ESTIMATED_CENTER_POS;
		}
	}

	for(u32 i = 0; i < MAX_POVS; ++i)
	{
		// According to https://msdn.microsoft.com/en-us/library/windows/desktop/microsoft.directx_sdk.reference.dijoystate2%28v=vs.85%29.aspx
		// -1 is center.
		m_POVs[i] = -1;
	}
}

#else

// Stubbed-out versions for DI 3

// PURPOSE:	Update all devices
void ioJoystick::UpdateAll() { }

// PURPOSE:	Poll all devices
void ioJoystick::PollAll() { }

// PURPOSE:	Initialize all devices
void ioJoystick::BeginAll() { }

// PURPOSE:	Shut down all devices
void ioJoystick::EndAll() { }

#endif

#else	// __WIN32

#include "joystick.h"

rage::ioJoystick rage::ioJoystick::sm_Sticks[MAX_STICKS];
int rage::ioJoystick::sm_StickCount;

#endif	// __WIN32
