#if RSG_DURANGO

#include "pad.h"
#include "pad_durango.h"

#include "remote_input.h"
#include "remote_input_durango.winrt.h"

// rage headers.
#include "system/criticalsection.h"
#include "system/param.h"
#include "system/service.h"
#include "system/userlist_durango.winrt.h"

#include <xdk.h>

namespace rage
{
XPARAM(norumble);
#if !__FINAL
XPARAM(unattended);
PARAM(padConnected,"Always pretend a pad is connected (to avoid warning screens)");
#endif

// So we do not pollute the rage or global namespaces, wrap durango's long namespaces in their own namespace.
namespace dns
{
	using Windows::Foundation::Collections::IVectorView;
	using Windows::Foundation::EventHandler;
	using Windows::Foundation::EventRegistrationToken;
	using Windows::Foundation::TypedEventHandler;
	using Windows::Xbox::Input::Controller;
	using Windows::Xbox::Input::ControllerPairingChangedEventArgs;
	using Windows::Xbox::Input::Gamepad;
	using Windows::Xbox::Input::GamepadAddedEventArgs;
	using Windows::Xbox::Input::GamepadButtons;
	using Windows::Xbox::Input::GamepadRemovedEventArgs;
	using Windows::Xbox::Input::GamepadVibration;
	using Windows::Xbox::Input::IGamepad;
	using Windows::Xbox::Input::RawGamepadReading;
	using Windows::Xbox::Media::GameTransportControls;
	using Windows::Xbox::Media::GameTransportControlsButtonPressedEventArgs;
}

//////////////////////////////////////////////////////////////////////////
// ioPad Data
//////////////////////////////////////////////////////////////////////////
//
// As ioPad is a common interface for all platforms. Data specific to
// Durango are stored as static globals and act as private data.
//////////////////////////////////////////////////////////////////////////

// The WinRT Gamepads.
#if _XDK_VER >= 9698
static dns::GameTransportControls^ s_GamepadMediaControls = nullptr;
static dns::EventRegistrationToken s_GamepadMediaControlsToken;
#endif // _XDK_VER >= 9698

u64 durangoPads::sm_LastUsedGamePadId = 0;

// Due to the event driven nature of the WinRT interface we need to ensure
// thread safety.
static sysCriticalSectionToken s_GamepadCs;

static bool s_MediaControlView;
static bool s_MediaControlMenu;

// PURPOSE:	Callback to receive constrained/unconstrained state.
// NOTES:	This is used to indicate the pad intercepted state.
static ServiceDelegate s_PadServiceDelegate;

// PURPOSE: Flag to indicate the game does not have focus.
static bool s_HasFocus = true;

// PURPOSE: Flag to indicate the game does not have input focus.
static bool s_HasInputFocus = true;

static void OnServiceEvent( sysServiceEvent* evt )
{
	if(evt != NULL)
	{
		if(evt->GetType() == sysServiceEvent::FOCUS_LOST)
		{
			sysCriticalSection lock(s_GamepadCs);
			s_HasFocus = false;
		}
		else if(evt->GetType() == sysServiceEvent::FOCUS_GAINED)
		{
			sysCriticalSection lock(s_GamepadCs);
			s_HasFocus = true;
		}
		if(evt->GetType() == sysServiceEvent::INPUT_FOCUS_LOST)
		{
			sysCriticalSection lock(s_GamepadCs);
			s_HasInputFocus = false;
		}
		else if(evt->GetType() == sysServiceEvent::INPUT_FOCUS_GAINED)
		{
			sysCriticalSection lock(s_GamepadCs);
			s_HasInputFocus = true;
		}
	}
}

static void OnMediaEvent(dns::GameTransportControlsButtonPressedEventArgs^ args)
{
	sysCriticalSection lock(s_GamepadCs);
	switch (args->Button)
	{
	case Windows::Xbox::Media::GameTransportControlsButton::Menu:
		s_MediaControlMenu = true;
		break;
	case Windows::Xbox::Media::GameTransportControlsButton::View:
		s_MediaControlView = true;
		break;
	default:
		break;
	}
}

static inline u8 RemapAxis(float axis) 
{ 
	// Map from [-1, 1] to [0, 255].
	return (u8)(255.0f*((axis + 1.0f)/2.0f));
}

static inline bool IsButtonsPressed(dns::GamepadButtons state, dns::GamepadButtons buttonsToTest)
{
	return (state & buttonsToTest) == buttonsToTest;
}

static bank_float durangoAnalogThreshold = 0.25f;

static inline bool AnaDown(float b) 
{ 
	return b >= durangoAnalogThreshold; 
}

static inline float ConvertRumbleScale(u16 intensity)
{
	return static_cast<float>(intensity) / 65535.0f;
}

void ioPad::BeginAll()
{
	// When setting up a WinRT callback, if an event is ready, this thread
	// will call the callback. As the callback also locks s_GamepadCs, we use a
	// scope here to ensure it is not locked in this case.
	{
		sysCriticalSection lock(s_GamepadCs);
		s_MediaControlMenu = false;
		s_MediaControlView = false;

		for(s32 i = 0; i < MAX_PADS; ++i)
		{
			sm_Pads[i].m_IsConnected = false;
#if __FINAL
			sm_Pads[i].m_HasRumble = true;
#else
			sm_Pads[i].m_HasRumble = !PARAM_norumble.Get();
#endif // __FINAL
			sm_Pads[i].m_PadIndex = i;

		}
	}

	try
	{
		sysCriticalSection lock(s_GamepadCs);
#if _XDK_VER >= 9698
		if(s_GamepadMediaControls == nullptr)
#endif // _XDK_VER >= 9698
		{
#if _XDK_VER >= 9698
			s_GamepadMediaControls = ref new dns::GameTransportControls();
			s_GamepadMediaControlsToken = s_GamepadMediaControls->ButtonPressed += ref new dns::TypedEventHandler<dns::GameTransportControls^, dns::GameTransportControlsButtonPressedEventArgs^>(
				[] (dns::GameTransportControls^, dns::GameTransportControlsButtonPressedEventArgs^ args)
			{
				OnMediaEvent(args);	
			}
			);

			s_GamepadMediaControls->IsMenuEnabled = true;
			s_GamepadMediaControls->IsViewEnabled = true;
		}
#endif // _XDK_VER >= 9698
	}
	catch(Platform::Exception^)
	{
		Assertf(false, "Error setting up gamepad event handlers!");
	}


	s_PadServiceDelegate.Bind(&OnServiceEvent);
	g_SysService.AddDelegate(&s_PadServiceDelegate);
}

void ioPad::EndAll()
{
	sysCriticalSection lock(s_GamepadCs);

	g_SysService.RemoveDelegate(&s_PadServiceDelegate);
	s_PadServiceDelegate.Reset();

	try
	{
#if _XDK_VER >= 9698
		if(s_GamepadMediaControls != nullptr)
#endif // _XDK_VER >= 9698
		{
#if _XDK_VER >= 9698
			s_GamepadMediaControls->ButtonPressed -= s_GamepadMediaControlsToken;
			s_GamepadMediaControls = nullptr;
#endif // _XDK_VER >= 9698
		}
	}
	catch(Platform::Exception^)
	{
		Assertf(false, "Error removing gamepad event handlers!");
	}
}

void ioPad::UpdateAll(bool UNUSED_PARAM(ignoreInput), bool)
{
	// NOTE: We lock s_GamepadCs here and not in Update() so that pad states do not change between two different pad Update() calls.
	sysCriticalSection lock(s_GamepadCs);

	// Reset controller.
	for(u32 i = 0; i < MAX_PADS; ++i)
	{
		sm_Pads[i].Update();
	}

	s_MediaControlMenu = false;
	s_MediaControlView = false;

	sm_Intercepted = (!s_HasFocus || !s_HasInputFocus);
}

void ioPad::Update(bool ignoreInput)
{
	// NOTE: We do not lock s_GamepadCs here even though we are accessing s_DurangoGamepads. This is because it is Locked inside UpdateAll() so that
	// state does not change between two different pad Udate() calls.
	u32 index = GetPadIndex();
	ClearInputs();

	dns::IGamepad^ gamepad = nullptr; 

	const sysDurangoUserInfo* info = sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(index);
	if(info != nullptr)
	{
		gamepad = info->GetPlatformGamepad();
	}

	m_IsConnected = (gamepad != nullptr) NOTFINAL_ONLY(|| PARAM_unattended.Get() || PARAM_padConnected.Get() REMOTE_INPUT_SUPPORT_ONLY(|| ioRemoteInput::IsEnabled()));

	if((gamepad != nullptr REMOTE_INPUT_SUPPORT_ONLY(|| ioRemoteInput::IsEnabled())) && (ignoreInput == false))
	{
		try
		{
			dns::RawGamepadReading reading = REMOTE_INPUT_SUPPORT_SWITCH(gamepad != nullptr ? gamepad->GetRawCurrentReading() : dns::RawGamepadReading(), gamepad->GetRawCurrentReading());

#if REMOTE_INPUT_SUPPORT
			if (ioRemoteInput::IsPadConnected((int)index) == true)
			{
				ioRemoteInput_WinRT::UpdatePad(index, reading);
			}
			else if (gamepad == nullptr)
			{
				return;
			}
#endif // REMOTE_INPUT_SUPPORT

			m_Axis[0] = RemapAxis(reading.LeftThumbstickX);
			m_Axis[1] = (u8) ~RemapAxis(reading.LeftThumbstickY);
			m_Axis[2] = RemapAxis(reading.RightThumbstickX);
			m_Axis[3] = (u8) ~RemapAxis(reading.RightThumbstickY);

			m_AnalogButtons[L2_INDEX] = u8(reading.LeftTrigger * 255.0f);
			m_AnalogButtons[R2_INDEX] = u8(reading.RightTrigger * 255.0f);

			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::A)) m_Buttons |= RDOWN;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::B)) m_Buttons |= RRIGHT;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::X)) m_Buttons |= RLEFT;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::Y)) m_Buttons |= RUP;

			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::LeftShoulder)) m_Buttons |= L1;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::RightShoulder)) m_Buttons |= R1;

			if (AnaDown(reading.LeftTrigger)) m_Buttons |= L2;
			if (AnaDown(reading.RightTrigger)) m_Buttons |= R2;

			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::Menu) || s_MediaControlMenu) m_Buttons |= START;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::View) || s_MediaControlView) m_Buttons |= SELECT;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::LeftThumbstick)) m_Buttons |= L3;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::RightThumbstick)) m_Buttons |= R3;

			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::DPadUp)) m_Buttons |= LUP;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::DPadDown)) m_Buttons |= LDOWN;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::DPadLeft)) m_Buttons |= LLEFT;
			if (IsButtonsPressed(reading.Buttons, dns::GamepadButtons::DPadRight)) m_Buttons |= LRIGHT;

			if(HasRumble())
			{
				dns::GamepadVibration vibration;
				vibration.LeftMotorLevel = ConvertRumbleScale(m_ActuatorData[HEAVY_MOTOR]);
				vibration.LeftTriggerLevel = ConvertRumbleScale(m_ActuatorData[LEFT_TRIGGER]);
				vibration.RightMotorLevel = ConvertRumbleScale(m_ActuatorData[LIGHT_MOTOR]);
				vibration.RightTriggerLevel = ConvertRumbleScale(m_ActuatorData[RIGHT_TRIGGER]);
				REMOTE_INPUT_SUPPORT_ONLY(if (gamepad != nullptr)) gamepad->SetVibration(vibration);

				REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::SetPadVibration(m_PadIndex, m_ActuatorsEnabled ? m_ActuatorData[HEAVY_MOTOR] : 0, m_ActuatorsEnabled ? m_ActuatorData[LIGHT_MOTOR] : 0));
			}

			if (m_Buttons REMOTE_INPUT_SUPPORT_ONLY(&& gamepad != nullptr))
			{
				durangoPads::sm_LastUsedGamePadId = gamepad->Id;
			}
		}
		catch(Platform::Exception^)
		{
			Assertf(false, "Error Reading gamepad, this usually means it was disconnected whilst we were reading it!");
		}
	}
}

#if __BANK
void ioPad::AddPadWidgets()
{

}
#endif

void ioPad::StopShakingNow()
{
	sysCriticalSection lock(s_GamepadCs);
	try
	{

		const sysDurangoUserInfo* info = sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(GetPadIndex());
		if(info != nullptr)
		{
			dns::IGamepad^ gamepad = info->GetPlatformGamepad();

			if(gamepad != nullptr)
			{
				dns::GamepadVibration vibration;
				vibration.LeftMotorLevel = 0.0f;
				vibration.LeftTriggerLevel = 0.0f;
				vibration.RightMotorLevel = 0.0f;
				vibration.RightTriggerLevel = 0.0f;
				gamepad->SetVibration(vibration);
			}
		}
	}
	catch(Platform::Exception ^)
	{
		Assertf(false, "Failed to stop pad shaking!");
	}

	REMOTE_INPUT_SUPPORT_ONLY(ioRemoteInput::SetPadVibration(m_PadIndex, 0, 0));
}

#if __BANK
void ioPad::AnalogStickPressedCB( CallbackData data )
{
	size_t button = reinterpret_cast<size_t>( data );

	switch ( button )
	{
	case LUP:
		m_bankAxis[1] = 0x0;
		break;
	case LLEFT:
		m_bankAxis[0] = 0x0;
		break;
	case LRIGHT:
		m_bankAxis[0] = 0xff;
		break;
	case LDOWN:
		m_bankAxis[1] = 0xff;
		break;
	case RUP:
		m_bankAxis[3] = 0x0;
		break;
	case RLEFT:
		m_bankAxis[2] = 0x0;
		break;
	case RRIGHT:
		m_bankAxis[2] = 0xff;
		break;
	case RDOWN:
		m_bankAxis[3] = 0xff;
		break;
	}
}

void ioPad::ButtonPressedCB( CallbackData data )
{
	size_t button = reinterpret_cast<size_t>( data );

	m_bankButtons |= int(button);

	if ( button == L2 )
	{
		m_bankAnalogButtons[L2_INDEX] = 0xff;
	}
	else if ( button == R2 )
	{
		m_bankAnalogButtons[R2_INDEX] = 0xff;
	}
}

#endif

u64 durangoPads::GetLastUsedGamePadId()
{
	return sm_LastUsedGamePadId;
}

};

#endif // RSG_DURANGO
