//
// input/joystick.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_JOYSTICK_H
#define INPUT_JOYSTICK_H

/* Unlike the keyboard and mouse, there can be more than one joystick,
	so this is not a pure static class. */

#if __WIN32
#include "mapper_defs.h"
#include "input_calibration.h"

#include "atl/array.h"
#include "atl/atfixedstring.h"
#include "atl/map.h"
#include "file/limits.h"

struct DIDEVICEINSTANCEA;
struct DIDEVICEINSTANCEW;
struct DIDEVICEOBJECTINSTANCEA;
struct DIDEVICEOBJECTINSTANCEW;
struct IDirectInputDevice2A;
struct IDirectInputDevice2W;
struct IDirectInputEffect;

#ifndef GUID_DEFINED
#define GUID_DEFINED
typedef struct  _GUID
    {
    unsigned Data1;
    unsigned short Data2;
    unsigned short Data3;
    unsigned char Data4[ 8 ];
    }   GUID;
#endif	// GUID_DEFINED
#endif  // __WIN32

namespace rage {

// PURPOSE:
//	This class should not be directly used in any projects because it only
//	works on the PC.  All projects should use the ioPad class instead, which
//	itself uses ioJoystick internally on PC builds.
// <FLAG Component>
class ioJoystick {
public:
	// PURPOSE:	Begin to enumerate each attached devices
	static void BeginAll();

#if __WIN32
	// PURPOSE:	Begin to enumerate each attached devices and tries to calibrate all devices from a calibration file.
	// PARAMS:	defaultCalibrationFile - the path to the default calibration file to use for calibrating the devices.
	//			userCalibrationFile - the path to the user calibration file that will override any settings in the default file.
	//				ideally this file should be writable as well in order to save any changes the user makes or safe info for devices
	//				not listed in the default file.
	// NOTES:	Call this instead of BeginAll(), only one should be called. This function will only calibrate devices listed in the two files.
	//			If the device is unknown, it will not be calibrated (but is still usable).
	static void BeginAndCalibrateAll(const char* defaultCalibrationFile, const char* userCalibrationFile = NULL);
#endif // __WIN32

	// PURPOSE:	Run Updates for all devices
	static void UpdateAll();

	// PURPOSE:	Run Poll function for each devices
	static void PollAll();

	// PURPOSE: Run End function for all devices
	static void EndAll();

	// PURPOSE:	Get the number of devices
	// RETURNS:	int - the number of devices
	static int GetStickCount();

	// PURPOSE:	Get the desire device refference
	// PARAMS:	i - the index for the device's array. 
	// RETURNS:	const ioJoystikc & - the const refferenced object
	static const ioJoystick& GetStick(int i);

	// PURPOSE: Get number of Axis that available in the device
	// RETURNS:	unsgined int - the total number of axis on the device
	unsigned GetAxisCount() const;

	// PURPOSE:	Get the number of button on the device
	// RETURNS:	unsgined int - the total number of buttons on the device
	unsigned GetButtonCount() const;

	// PURPOSE:	Get POV Count
	// RETURNS:	unsgined int - the total number of POV in the device
	unsigned GetPOVCount() const;

	// Enum setup
	enum { MAX_STICKS = 4, MAX_AXES = 8,  MAX_BUTTONS = 32, MAX_POVS = 4 };
    
	// the order of the axes (active axes get sorted to the top)
	// these are the directInput meanings of each axis, and where that axis can be found
	enum { AXIS_LX, AXIS_LY, AXIS_LZ, AXIS_LRX, AXIS_LRY, AXIS_LRZ, AXIS_SLIDER0, AXIS_SLIDER1, NUM_AXES};

	// PURPOSE:	Get the desire axis
	// PARAMS:	axis - the index number of the Axes array
	// RETURNS:	int - the desire Axis
	int GetAxis(int axis) const;

	// PURPOSE:	Get the Norm Axis
	// PARAMS:	axis - the desire index from the Axes's array
	// RETURNS:	float - the Norm for the desire axis
	float GetNormAxis(int axis) const;

	// PURPOSE:	Get the button state bitsete
	// RETURNS:	unsigned int - bitset with 0 = button not pressed, 1 = button pressed.
	unsigned GetButtons() const;

	// PURPOSE: Gets the value of the nth analog button
	unsigned char GetAnalogButton(int btn) const;

	// PURPOSE: Gets the previous value of the nth analog button
	unsigned char GetLastAnalogButton(int btn) const;

	// PURPOSE:	Get the desire POV
	// PARAMS:	pov - the desire index from the POVs's array
	// RETURNS:	int - the desire POV
	int GetPOV(int pov) const;

	// PURPOSE:	Get the changed buttons
	// RETURNS:	unsigned int - the changed buttons
	unsigned GetChangedButtons() const;

	// PURPOSE:	Get Pressed Buttons
	// RETURNS:	unsigned int - the pressed buttons
	unsigned GetPressedButtons() const;

	// PURPOSE:	get Release Buttons
	// RETURNS:	unsigned int - the released buttons
	unsigned GetReleasedButtons() const;

	// PURPOSE:	Determine if this connected controller is a steering wheel
	// PARAMS:	NONE
	// RETURNS: true if this is a driving type device
	bool IsWheel() const;

	// set up Enum for EnumJoyType
	enum EnumJoyType {
		TYPE_UNKNOWN,
		TYPE_DIRECTPAD,
		TYPE_SMARTJOYPAD,
		TYPE_GAMEPADPRO,
		TYPE_XKPC2002,
		TYPE_DUALJOYPAD,
		TYPE_DUAL_USB_FFJ_MP866,
		TYPE_KIKYXSERIES,
		TYPE_LOGITECH_CORDLESS,
		TYPE_PSX_TO_USB_CONVERTER,
		TYPE_XBCD_XBOX_USB,
		TYPE_TIGERGAME_XBOX_USB,
		TYPE_XNA_GAMEPAD_XENON,
		NUM_TYPES};

	// PURPOSE:	Get the joystick type
	// RETURNS:	int - m_Type ( this joy type )
	int GetJoyType() const;

#if __WIN32	
	// PURPOSE: Get the device type unique to that directinput device
	// RETURNS:	int - device type
	int GetDevType() const;

	// PURPOSE:	Get the name of the attached device
	// PARAMS:	name - wide character pointer to contain NULL terminated string; size - max length of string (including trailing 0)
	void GetProductName(unsigned short *name, unsigned size) const;


	bool IsValid() const;

	// PURPOSE:	The type of force a device supports.
	// NOTES:	Currently devices can only support one type of force, however, the code has been written is the assumption
	// 			that this might change. Therefore you should check the specific bits.
	enum ForceType {
		IOJF_NONE        = 0x0,
		IOJF_RUMBLE      = 0x1,
		IOJF_DIRECTIONAL = 0x2,
	};

	s32 GetForceType() const;


	// PURPOSE:	function to get information about the attached stick for use in higher level game code
	// PARAMS:	info	- reference to a DIDEVICEINSTANCEA device information struct to be populated by this function
	// RETURNS: HRESULT returned from m_Device->GetDeviceInfo
	int GetDeviceInfo(DIDEVICEINSTANCEW &) const;

	// PURPOSE:	Display the capabilities of the directInput device
	void PrintDeviceCaps() const;

	// PURPOSE: function to get the product GUID.
	// RETURNS: the product GUID.
	const GUID& GetProductGuid() const;

	// PURPOSE: function to get the product GUID as a string.
	// RETURNS: the product GUID as a string (char *).
	const char* GetProductGuidStr() const;

	// PURPOSE:	function to get the calibrated state of a device.
	// RETURNS:	true if the device is calibrated.
	bool IsCalibrated() const;

	// PURPOSE:	function to indicate if an axis is two directional.
	// RETURNS:	true if axis is two directional.
	// NOTES:	if we cannot determine if the axis is two directional we assume it as as most are.
	bool IsAxisTwoDirectional(int axis) const;

	// PURPOSE: function to automatically calibrate the device.
	// PARAMS:	overrideCurrentCalibration - overrides the current calibration data if there is any.
	// NOTES:	the function uses the current value of axes as the calibration points.
	void AutoCalibrate(bool overrideCurrentCalibration);

	// PURPOSE: function to automatically calibrate the device.
	// PARAMS:	stick - the stick to calibrate.
	// PARAMS:	overrideCurrentCalibration - overrides the current calibration data if there is any.
	// NOTES:	the function uses the current value of axes as the calibration points.
	static void AutoCalibrate(int stick, bool overrideCurrentCalibration);

	// PURPOSE: function to save the user calibrations.
	// PARAMS:	overwriteMallFormedFiles - if true and the user's file is mall-formed it will still be overwritten (see NOTES).
	// RETURN:	true on success or false on any error.
	// NOTES:	the file that is saved to the path specified in BeginAndCalibrateAll(). If the user file is mallformed then we cannot load
	//				any other user calibrations which means, if we save, these changes will be lost. the parameter overwriteMallFormedFiles
	//				indicates what should happen in this case.
	static bool SaveUserCalibrations(bool overwriteMallFormedFiles);

	enum {
		IOJ_INFINITE = 0xFFFFFFFF,	// Infinite duration.
		IOJ_MAX_FORCE = 255,		// The max force that can be passed to a DirectInput Device.
		IOJ_TIMESCALE = 1000,		// Used internally to convert milliseconds to DirectInput Time.
	};	

	// PURPOSE:	applies a directional force in the given x, y direction to devices that support it.
	// PARAMS:	stickId - the stick to apply the force to.
	//			X - the X force.
	//			Y - the Y force.
	//			timeMS - the duration of the force in milliseconds.
	static void ApplyDirectionalForce(s32 stickId, float X, float Y, s32 timeMS);
	
	// PURPOSE:	applies a rumble force to devices that support it.
	// PARAMS:	stickId - the stick to apply the force to.
	//			motorFrequency0 - the frequency to apply to motor 0.
	//			motorFrequency1 - the frequency to apply to motor 1.
	//			timeMS - the duration of the force in milliseconds.
	static void ShakeDevice(s32 stickId, s32 motorFrequency0, s32 motorFrequency1, s32 timeMS);

	// PURPOSE: stops any device forces/rumbles.
	static void StopAllForces();

	// PURPOSE: Indicates if the device has input focus.
	bool HasInputFocus() const;

	// PURPOSE:	recaptures lost devices.
	static void RecaptureLostDevices();
#endif

protected:
	// PURPOSE:	Begin to initialize the device 
	void Begin();

	// PURPOSE:	Poll the device. MUST call this before every Update!
	void Poll();

	// PURPOSE:	Update the device
	void Update();

	// PURPOSE:	Shut down device
	void End();

private:
	// PURPOSE: Flushes the joystick's values.
	void FlushValues();

	// number of Axis, Buttons and POV count
	int m_AxisCount, m_ButtonCount, m_POVCount;

	// array of Axis
	int m_Axes[MAX_AXES], m_AxisFocusGainValues[MAX_AXES];

	unsigned char m_AnalogButtons[MAX_BUTTONS], m_LastAnalogButtons[MAX_BUTTONS];

	// number of this buttons, and last buttons
	unsigned m_Buttons, m_LastButtons;

	// intentional, because some drivers mess up and report 0xffff instead of -1
	// highest value a pov will likely return is 7/8*36000, which still fits
	// in a signed short.
	short m_POVs[MAX_POVS];	

	// type of the device
	EnumJoyType	m_Type;

	// PURPOSE: Indicates the device is a steering wheel.
	bool m_IsWheel;

	// PURPOSE: Indicates that we have input focus on the device.
	bool m_HasInputFocus;

	// PURPOSE: Indicates that focus was gained this frame.
	bool m_FirstFrameAfterFocusGain;

	// PURPOSE: Indicates that the device is lost.
	bool m_DiDeviceLost;

#if __WIN32
	static dev_float sm_JoystickAxisTypeDetection;

	GUID m_InstanceGuid;
	GUID m_ProductGuid;
	char m_ProductGuidStr[37];
	IDirectInputDevice2W *m_Device;

	// array of Axis min and max values.
	int m_AxesMax[NUM_AXES];
	int m_AxesMin[NUM_AXES];

	// sliders cannot be calebrated
	int m_AxesCalibration[NUM_AXES];

	// The number of force feedback axis supported.
	int m_NumFFAxis;

	// The number of sliders.
	int m_NumSliders;

	// The force feed back type supported.
	int m_ForceType;

	bool m_AxesEnabled[NUM_AXES];

	// indicates the value has been calibrated.
	bool m_IsCalibrated;

	IDirectInputEffect* m_Effect;

	// PURPOSE:	callback function for EnumDeviceProc
	// PARAMS:	lpddi	- long pointer to the device instance 
	//			LPVOID	- long void pointer
	// RETURNS: DIENUM_CONTINUE
	static int __stdcall EnumDeviceProc(const DIDEVICEINSTANCEW*,void*);
	static int __stdcall EnumObjectProc(const DIDEVICEOBJECTINSTANCEW*,void*);
	static int __stdcall EnumFFAxisProc(const DIDEVICEOBJECTINSTANCEW*,void*);

	// These are here so we do not need to pull a maths header file.
	static bool IsNearZero(float number);
	static float Abs(float val);
	static s32 ClampIt(s32 test, s32 min, s32 max);

	// path to user calibration file.
	static atFixedString<RAGE_MAX_PATH> sm_userCalibrationFile;

	// PURPOSE:	applies a force in the given x, y direction to devices that support it.
	// PARAMS:	X - the X force.
	//			Y - the Y force.
	//			timeMS - the duration of the force in milliseconds.
	void ApplyForce(s32 X, s32 Y, s32 timeMS);

	// PURPOSE: captures the device.
	void CaptureDevice();
#endif

	// static array member for the maximum device
	static ioJoystick sm_Sticks[MAX_STICKS];

	// static number of the device count
	static int sm_StickCount;

};


inline int ioJoystick::GetStickCount() 
{
	return sm_StickCount;
}

inline const ioJoystick& ioJoystick::GetStick(int i)
{
	return sm_Sticks[i];
}

inline unsigned ioJoystick::GetAxisCount() const
{
	return m_AxisCount;
}

inline unsigned ioJoystick::GetButtonCount() const
{
	return m_ButtonCount;
}

inline unsigned ioJoystick::GetPOVCount() const
{
	return m_POVCount;
}

inline int ioJoystick::GetAxis(int axis) const
{
	return m_Axes[axis];
}


inline unsigned ioJoystick::GetButtons() const
{
	return m_Buttons;
}

inline unsigned char ioJoystick::GetAnalogButton(int btn) const
{
	return m_AnalogButtons[btn];
}

inline unsigned char ioJoystick::GetLastAnalogButton(int btn) const
{
	return m_LastAnalogButtons[btn];
}

inline int ioJoystick::GetPOV(int pov) const
{
	return m_POVs[pov];
}

inline unsigned ioJoystick::GetChangedButtons() const
{
	return m_LastButtons ^ GetButtons();
}

inline unsigned ioJoystick::GetPressedButtons() const
{
	return GetChangedButtons() & GetButtons();
}

inline unsigned ioJoystick::GetReleasedButtons() const
{
	return GetChangedButtons() & m_LastButtons;
}

inline int ioJoystick::GetJoyType() const
{
	return m_Type;
}

#if __WIN32
inline bool ioJoystick::HasInputFocus() const
{
	return m_HasInputFocus;
}

inline const GUID& ioJoystick::GetProductGuid() const
{
	return m_ProductGuid;
}

inline const char* ioJoystick::GetProductGuidStr() const
{
	return m_ProductGuidStr;
}

inline s32 ioJoystick::GetForceType() const
{
	return m_ForceType;
}

inline void ioJoystick::ApplyDirectionalForce(s32 stickId, float X, float Y, s32 timeMS)
{
	if((sm_Sticks[stickId].m_ForceType & IOJF_DIRECTIONAL))
	{
		sm_Sticks[stickId].ApplyForce(static_cast<s32>(X * IOJ_MAX_FORCE), static_cast<s32>(Y * IOJ_MAX_FORCE), timeMS);
	}
}

inline void ioJoystick::ShakeDevice(s32 stickId, s32 motorFrequency0, s32 motorFrequency1, s32 timeMS)
{
	if((sm_Sticks[stickId].m_ForceType & IOJF_RUMBLE))
	{
		sm_Sticks[stickId].ApplyForce(motorFrequency0, motorFrequency1, timeMS);
	}
}

inline bool ioJoystick::IsNearZero (float number)
{
	return Abs(number)<sm_JoystickAxisTypeDetection;
}

inline float ioJoystick::Abs(float val)
{
	return (val >=0.0f) ? val : -val;
}

inline bool ioJoystick::IsCalibrated() const
{
	return m_IsCalibrated;
}

inline void ioJoystick::AutoCalibrate(int stick, bool overrideCurrentCalibration)
{
	Assertf(stick >= 0 && stick < sm_StickCount, "Invalid joystick!");
	if(stick >= 0 && stick < sm_StickCount)
	{
		sm_Sticks[stick].AutoCalibrate(overrideCurrentCalibration);
	}
}

inline s32 ioJoystick::ClampIt(s32 test, s32 min, s32 max)
{
	 return ((test<min) ? min : (test>max) ? max : test);
}

inline bool ioJoystick::IsWheel() const
{
	return m_IsWheel;
}

#endif // __WIN32

}	// namespace rage

#endif
