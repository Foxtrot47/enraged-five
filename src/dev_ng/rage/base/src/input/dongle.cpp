//
// input/dongle.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __XENON
#include "input/dongle.h"

#include "system/alloca.h"
#include "diag/output.h"
#include "string/string.h"
#include "string/unicode.h"
#include "data/aes.h"


#define SAFE_DONGLE		1		// If 1, dongles cannot be copied across MUs.

#if __XENON
#include <stdio.h>
#include <system/xtl.h>
#include <xdk.h>
#elif __PS3
#include <netex/libnetctl.h>
#include <sys/paths.h>
#include "file/stream.h"
#include "file/device.h"
#include "file/asset.h"
#include <cell/rtc.h>
#include <cell/sysmodule.h>
#include <sys/fs_external.h>
#include "system/memops.h"
#pragma comment(lib,"rtc_stub")
#endif // __PS2

using namespace rage;

#if __XENON
#define MAX_CONTENT_COUNT 16
bool				s_WritingCodeFile = false;
XCONTENTDEVICEID	s_DeviceID;
XOVERLAPPED			s_Overlapped;
char				s_FileName[XCONTENT_MAX_FILENAME_LENGTH];
char				s_DongleString[256];
const char*			s_SaveRoot = "save";
char				s_MacAddString[18];

#if SAFE_DONGLE
	#define DONGLE_SECURITY	| XCONTENTFLAG_NOPROFILE_TRANSFER | XCONTENTFLAG_NODEVICE_TRANSFER
#else // SAFE_DONGLE
	#define DONGLE_SECURITY
#endif // SAFE_DONGLE

#endif	//__XENON

#if __PS3
	#define GAMEDATA_DIR SYS_DEV_USB "/"
	#define GAMEDATA_DIR2 SYS_DEV_MS "/"
#endif // __PS3

void ioDongle::WriteCodeFile(const char* encodeString, const char* macaddrString, const fiDevice::SystemTime PS3_ONLY(expiryDate), bool PS3_ONLY(bExpires), const char* mcFileName)
{
#if __XENON
// TODO: Implement expiry date on XENON

	if( s_WritingCodeFile )
		return;

	size_t bufferLen = StringLength(encodeString)+StringLength(macaddrString)+1;

	ULARGE_INTEGER iBytesRequested;
#if _XDK_VER >= 2417
	iBytesRequested.QuadPart = XContentCalculateSize(bufferLen,0);
#else
	iBytesRequested.QuadPart = bufferLen;
#endif

	s_DeviceID = XCONTENTDEVICE_ANY;
	ZeroMemory(&s_Overlapped, sizeof(XOVERLAPPED));

#if _XDK_VER < 20500
	u32 showDeviceFlags = XCONTENTFLAG_FORCE_SHOW_UI;
#else
	u32 showDeviceFlags = 0;
#endif
	if( ERROR_IO_PENDING==XShowDeviceSelectorUI(XUSER_INDEX_ANY, XCONTENTTYPE_SAVEDGAME, showDeviceFlags, iBytesRequested, &s_DeviceID, &s_Overlapped) )
	{
		safecpy(s_FileName, mcFileName, sizeof(s_FileName));
		safecpy(s_DongleString, encodeString, sizeof(s_DongleString));
		safecpy(s_MacAddString, macaddrString, sizeof(s_MacAddString));
		s_WritingCodeFile = true;
	}
	else
	{
		(*sm_DisplayMessage)("Could not show the device selector ui.");
	}

#elif __PS3

	size_t bufferMacAddressLen = StringLength(macaddrString)+1;
	size_t bufferKeyStringLen  = StringLength(encodeString)+1;
	size_t bufferExpiryStringLen = 32;


	// Generate a system time string
	cellSysmoduleLoadModule(CELL_SYSMODULE_RTC);
	CellRtcDateTime cellDate;
	cellDate.year = expiryDate.wYear;
	cellDate.month = expiryDate.wMonth;
	cellDate.day = expiryDate.wDay;
	cellDate.hour = expiryDate.wHour;
	cellDate.minute = expiryDate.wMinute;
	cellDate.second = expiryDate.wSecond;
	cellDate.microsecond = 0;

	CellRtcTick cellExpiryTick;

	if(bExpires && cellRtcGetTick(&cellDate,&cellExpiryTick) != CELL_OK)
	{
	#if __BANK
		Displayf("[ioDongle] Error converting expiry time! Dongle not written");
	#endif
		return;
	}

	char* origBufferExpiryString = Alloca(char, bufferExpiryStringLen);

	if(bExpires)
		cellRtcFormatRfc2822LocalTime(origBufferExpiryString,&cellExpiryTick);
	else
		memset(origBufferExpiryString,0x00,bufferExpiryStringLen);
	cellSysmoduleUnloadModule(CELL_SYSMODULE_RTC);


	#if __BANK
		Displayf("[ioDongle] Key : %s || %d", encodeString, bufferKeyStringLen);
		Displayf("[ioDongle] Mac Address : %s || %d", macaddrString, bufferMacAddressLen);
		Displayf("[ioDongle] Expiry Date : %s || %d", origBufferExpiryString, bufferExpiryStringLen);
	#endif

	// We will use AES to encrypt the date, its length must be a multiple of 16
	if(bufferExpiryStringLen%16 >0)
		bufferExpiryStringLen += 16-(bufferExpiryStringLen % 16);

	size_t bufferLen = bufferMacAddressLen+bufferKeyStringLen+bufferExpiryStringLen;
	char* origBuffer = Alloca(char, bufferLen);
	// code to ensure release and beta versions of this work
	memset(origBuffer, 0xcd, bufferLen);

	char* origBufferMacAddress = Alloca(char, bufferMacAddressLen);
	char* origBufferKeyString  = Alloca(char, bufferKeyStringLen);

	// Encode Mac Address
	//formatf(origBufferMacAddress, bufferMacAddressLen, macaddrString);
	sysMemCpy(origBufferMacAddress,macaddrString,bufferMacAddressLen);
	Encode(origBufferMacAddress, bufferMacAddressLen);
	// Encode Key String
	//formatf(origBufferKeyString, bufferKeyStringLen, encodeString);
	sysMemCpy(origBufferKeyString,encodeString,bufferKeyStringLen);
	Encode(origBufferKeyString, bufferKeyStringLen);

	AES aes(AES_KEY_ID_DEFAULT);
	aes.Encrypt(origBufferExpiryString,bufferExpiryStringLen);

	// Copy Mac Address + Key String + Date string into origBuffer (for writing)
	sysMemCpy(origBuffer, origBufferMacAddress, bufferMacAddressLen);
	sysMemCpy(origBuffer+bufferMacAddressLen, origBufferKeyString, bufferKeyStringLen);
	sysMemCpy(origBuffer+bufferMacAddressLen+bufferKeyStringLen, origBufferExpiryString, bufferExpiryStringLen);

	char filePathName[CELL_FS_MAX_FS_PATH_LENGTH];
	snprintf(filePathName, CELL_FS_MAX_FS_PATH_LENGTH, "%s%s", GAMEDATA_DIR, mcFileName);
	fiStream *fileStream = fiStream::Create(&filePathName[0]);
	if(!fileStream)
	{
		Displayf("[ioDongle] ioDongle::WriteCodeFile() - DONGLE NOT WRITTEN");
		return;
	}

#if __BANK
	int bytesWritten =
#endif
		fileStream->Write(origBuffer, bufferLen);
#if __BANK
	Displayf("[ioDongle] ioDongle::WriteCodeFile() -  %d Bytes Written to file %s", bytesWritten, &filePathName[0]);
#endif // __BANK
	int ret = 0;
	ret = fileStream->Close();
	if (0!=ret)
	{
		Displayf("[ioDongle] ioDongle::WriteCodeFile() - ERROR CLOSING FILE");
		Displayf("[ioDongle] ioDongle::WriteCodeFile() - DONGLE NOT WRITTEN");
		return;
	}

	Displayf("[ioDongle] ioDongle::WriteCodeFile() - DONGLE WRITTEN");
	return;
#endif // __XENON
}

bool ioDongle::ReadCodeFile(const char* encodeString,const char* mcFileName, char * debugString)
{
#if __XENON
	bool success = false;
	const char* genericError = "UNAUTHORIZED demo copy.\n\nPlease insert an authorized memory card into Slot 1\nand reboot.";

	// Retrieve the xbox360 network address
	XNADDR lXnaddr; 
	DWORD lGetXnAddrStatus;
	lGetXnAddrStatus = XNetGetTitleXnAddr (&lXnaddr); // Verify errors on the status
	// Get the Hexa String of the Mac Address
	char macAddrString [18]; int ret;
	ret = sprintf(macAddrString, "%02X:%02X:%02X:%02X:%02X:%02X", lXnaddr.abEnet[0], lXnaddr.abEnet[1], 
														   lXnaddr.abEnet[2], lXnaddr.abEnet[3], 
														   lXnaddr.abEnet[4], lXnaddr.abEnet[5]);
	macAddrString[17] = '\0';

	DWORD cbBuffer;
	HANDLE hEnum;
	if( ERROR_SUCCESS==XContentCreateEnumerator(XUSER_INDEX_NONE, XCONTENTDEVICE_ANY, XCONTENTTYPE_PUBLISHER, 0, MAX_CONTENT_COUNT, &cbBuffer, &hEnum) )
	{
		XCONTENT_DATA contentData[MAX_CONTENT_COUNT];

		DWORD cItemsReturned = 0;
		if( ERROR_SUCCESS==XEnumerate(hEnum, &contentData[0], cbBuffer, &cItemsReturned, NULL) )
		{
			for(int i=0; i<(int)cItemsReturned && !success; i++)
			{
				if( strcmp(mcFileName, contentData[i].szFileName)==0 )
				{
					DWORD dwContentFlags = XCONTENTFLAG_OPENEXISTING;
					DWORD dwRet = XContentCreate(XUSER_INDEX_NONE, s_SaveRoot, &contentData[i], dwContentFlags, NULL, NULL, NULL);

					if( dwRet==ERROR_SUCCESS )
					{
						char fullFileName[XCONTENT_MAX_FILENAME_LENGTH];
						safecpy(fullFileName,s_SaveRoot,sizeof(fullFileName));
						safecat(fullFileName,":\\",sizeof(fullFileName));
						safecat(fullFileName,contentData[i].szFileName,sizeof(fullFileName));

						HANDLE hFile = CreateFile(fullFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

						if( hFile!=INVALID_HANDLE_VALUE )
						{
							// encodeString macAddrString
							DWORD numberOfBytesRead = 0;
							size_t bufferMacAddressLen = StringLength(macAddrString)+1;
							size_t bufferKeyStringLen = StringLength(encodeString)+1;
							size_t bufferLen = bufferMacAddressLen+bufferKeyStringLen;

							char* origBuffer = Alloca(char, bufferLen);
							char* readBuffer = Alloca(char, bufferLen);
							// code to ensure release and beta versions of this work
							memset(origBuffer, 0xcd, bufferLen);
							memset(readBuffer, 0xcd, bufferLen);

							char* origBufferMacAddress = Alloca(char, bufferMacAddressLen);
							char* origBufferKeyString = Alloca(char, bufferKeyStringLen);

							if( ReadFile(hFile, readBuffer, bufferLen, &numberOfBytesRead, NULL)!=0 && numberOfBytesRead==bufferLen )
							{
								// Encode Mac Address
								formatf(origBufferMacAddress, bufferMacAddressLen, macAddrString);
								Encode(origBufferMacAddress, bufferMacAddressLen);
								// Encode Key String
								formatf(origBufferKeyString, bufferKeyStringLen, encodeString);
								Encode(origBufferKeyString, bufferKeyStringLen);
								// Mac Address + Key String
								sysMemCpy(origBuffer, origBufferMacAddress, bufferMacAddressLen); 
								sysMemCpy(origBuffer+bufferMacAddressLen, origBufferKeyString, bufferKeyStringLen);

								if( memcmp(readBuffer, origBuffer, bufferLen) ==0 )
								{
									Displayf("Authorization OK");
									success = true;
								}
								else
								{
									if( memcmp(origBufferMacAddress, readBuffer, bufferMacAddressLen) !=0 )
									{
										strcpy(debugString, " UNAUTHORIZED demo copy - The dongle is for a different machine.! ");
										Displayf(" UNAUTHORIZED demo copy - The dongle is for a different machine.! ");
									}
									if( memcmp(origBufferKeyString, readBuffer+bufferMacAddressLen, bufferKeyStringLen) !=0 )
									{
										strcpy(debugString, " UNAUTHORIZED demo copy - The dongle key does not match the game.! ");
										Displayf(" UNAUTHORIZED demo copy - The dongle key does not match the game.! ");
									}
								}
							}
							else
							{
								strcpy(debugString, " Could not read the file! ");
								Displayf("Could not read the file[%s]",fullFileName);
							}

							CloseHandle(hFile);
						}
						else
						{
							strcpy(debugString, " Could not create the file! ");
							Displayf("Could not create the file[%s]",fullFileName);
						}

						XContentClose(s_SaveRoot, NULL);
					}
					else if( dwRet==ERROR_ACCESS_DENIED )
					{
						strcpy(debugString, " User must be signed in. ");
						Displayf("User must be signed in.");
					}
					else if( dwRet==ERROR_INVALID_NAME )
					{
						strcpy(debugString, " Invalid root save name. ");
						Displayf("Invalid root save name.");
					}
					else
					{
						strcpy(debugString, " Could not create the content package. ");
						Displayf("Could not create the content package.");
					}
				}
				else
				{
					strcpy(debugString, " UNAUTHORIZED demo copy - FileName and Content filename don't match.! ");
				}
			}
		}
		else
		{
			strcpy(debugString, " UNAUTHORIZED demo copy.! - Missing dongle file.");
		}

		CloseHandle(hEnum);
	}
	else
	{
		strcpy(debugString, " UNAUTHORIZED demo copy - XContentCreateEnumerator Failed.! ");
	}

	if( !success )
	{
		(*sm_DisplayMessage)(genericError);
		// while(1){}
	}

	return success;

#elif __PS3
	const char *genericError= "UNAUTHORIZED demo copy.\n\nPlease insert an authorized memory card into Slot 1\nand reboot.";

#pragma diag_suppress 552
	int err;
	CellNetCtlInfo cell_info;
	u8 mac[6];
	char macAddrString [18];

	// Look up current MAC address
	if(0 <= (err = cellNetCtlGetInfo(CELL_NET_CTL_INFO_ETHER_ADDR, &cell_info)))
	{
		CompileTimeAssert(sizeof(mac) == CELL_NET_CTL_ETHER_ADDR_LEN);
		sysMemCpy(mac, cell_info.ether_addr.data, CELL_NET_CTL_ETHER_ADDR_LEN);

		// Get the Hexa String of the Mac Address
		sprintf(macAddrString, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
		macAddrString[17] = '\0';
	}
	else
	{
		Errorf("[ioDongle] ReadCodeFile() - Error calling cellNetCtlGetInfo()  0x%08x", err);
	}

	// Note: This gets a bit messy since key and mac address are encoded one way (irreversibly) but 
	// date stamp is encoded using AES (reversibly). This is done to maintain backwards compatibility with
	// previous dongles

	/*-------------------------------*/
	size_t bufferMacAddressLen = StringLength(macAddrString)+1;
	size_t bufferKeyStringLen  = StringLength(encodeString)+1;
	size_t bufferDateStringLen = 32;
	size_t bufferLen = bufferMacAddressLen+bufferKeyStringLen+bufferDateStringLen;

//#if __BANK
//	Displayf("[ioDongle] ReadCodeFile() - Key : %s || %d", encodeString, bufferKeyStringLen);
//	Displayf("[ioDongle] ReadCodeFile() - Mac Address : %s || %d", &macAddrString[0], bufferMacAddressLen);
//#endif

	char* origBuffer = Alloca(char, bufferLen);
	char* readBuffer = Alloca(char, bufferLen);
	// code to ensure release and beta versions of this work
	memset(origBuffer, 0xcd, bufferLen);
	memset(readBuffer, 0xcd, bufferLen);

	char filePathName[CELL_FS_MAX_FS_PATH_LENGTH];
	snprintf(filePathName, CELL_FS_MAX_FS_PATH_LENGTH, "%s%s", GAMEDATA_DIR, mcFileName);
	fiStream *fileStream = fiStream::Open(&filePathName[0], true);
	if(!fileStream)
	{
		snprintf(filePathName, CELL_FS_MAX_FS_PATH_LENGTH, "%s%s", GAMEDATA_DIR2, mcFileName);
		fileStream = fiStream::Open(&filePathName[0], true);
	}
	if(!fileStream)
	{
	#if __BANK
		Displayf("[ioDongle] ReadCodeFile() - File %s does not exist", &filePathName[0]);
	#endif
		(*sm_DisplayMessage)(genericError);
		strcpy(debugString, genericError);
		return false;
	}

	int bytesRead = 0;
	if ( (bytesRead = fileStream->Read(readBuffer, bufferLen)) > 0)
	{

#if __BANK
		Displayf("[ioDongle] ReadCodeFile() -  %d Bytes Read from file %s", bytesRead, &filePathName[0]);
#endif // __BANK

		//int ret = 0;

		// Encode current key and mac address for comparison with read one
		// No need to do for date
		char* origBufferMacAddress = Alloca(char, bufferMacAddressLen);
		char* origBufferKeyString  = Alloca(char, bufferKeyStringLen);

		// code to ensure release and beta versions of this work
		memset(origBufferMacAddress, 0xcd, bufferMacAddressLen);
		memset(origBufferKeyString, 0xcd, bufferKeyStringLen);

		// Encode Mac Address
		//formatf(origBufferMacAddress, bufferMacAddressLen, macAddrString);
		sysMemCpy(origBufferMacAddress,macAddrString,bufferMacAddressLen);
		Encode(origBufferMacAddress, bufferMacAddressLen);

		// Encode Key String
		//formatf(origBufferKeyString, bufferKeyStringLen, encodeString);
		sysMemCpy(origBufferKeyString,encodeString,bufferKeyStringLen);
		Encode(origBufferKeyString, bufferKeyStringLen);

		// Mac Address + Key String
		sysMemCpy(origBuffer, origBufferMacAddress, bufferMacAddressLen); 
		sysMemCpy(origBuffer+bufferMacAddressLen, origBufferKeyString, bufferKeyStringLen);

		// Check if current date is past expiry
		cellSysmoduleLoadModule(CELL_SYSMODULE_RTC);

		// Look up current date and time
		CellRtcTick currentTick;
		cellRtcGetCurrentTick(&currentTick);	// Get current time (local time)

		// Compare with read date
		char* readDateStr = readBuffer+bufferMacAddressLen+bufferKeyStringLen;
		AES aes(AES_KEY_ID_DEFAULT);
		aes.Decrypt(readDateStr,bytesRead-bufferMacAddressLen-bufferKeyStringLen);
		bool bDateReadOK = false;
		bool bDateOK = false;
		CellRtcTick readTick;
		if(cellRtcParseDateTime(&readTick,readDateStr) == CELL_OK)
		{
			bDateReadOK = true;
			if(readTick.tick > currentTick.tick)
			{
				bDateOK = true;
			}
		}
		else
		{
	#if __DEV
			Displayf("[ioDongle] Notice: No expiry found in dongle");
	#endif
		}
		cellSysmoduleUnloadModule(CELL_SYSMODULE_RTC);

		// Check MAC
		bool bMacOK = false;
		if(memcmp(origBufferMacAddress, readBuffer, bufferMacAddressLen) == 0)
		{
			bMacOK = true;
		}

		// Check key
		bool bKeyOK = false;
		if(memcmp(origBufferKeyString, readBuffer + bufferMacAddressLen, bufferKeyStringLen) == 0)
		{
			bKeyOK = true;
		}

		//#if __BANK
		//	Displayf("[ioDongle] ReadCodeFile() - Original Buffer Content: %s", origBuffer);
		//#endif // __PSN

		// Note if no date could be read then we assume an expiry date was not set and continue loading
		if(bKeyOK && bMacOK && (bDateOK || !bDateReadOK))
		{
			Displayf("AUTHORIZATION OK");		

			fileStream->Close();
			return true;
		}
		else if (bMacOK)
		{
			if( !bKeyOK )
			{
				strcpy(debugString, " UNAUTHORIZED demo copy - The dongle key does not match the game.! ");
				Displayf(" UNAUTHORIZED demo copy - The dongle key does not match the game.! ");
			}

			if( !bDateOK && bDateReadOK)
			{
				strcpy(debugString, " UNAUTORIZED demo copy - The dongle has expired.! ");
				Displayf(" UNAUTHORIZED demo copy - The dongle has expired.! ");
			}
			return false;
		}
	}

	// if we got here we didn't match the dongle file
	strcpy(debugString, " UNAUTHORIZED demo copy - The dongle is for a different machine.! ");
	Displayf(" UNAUTHORIZED demo copy - The dongle is for a different machine.! ");

	Displayf("Authorization failed... ");

	return false;

#endif // __XENON

}

void sDisplayMessage(const char* OUTPUT_ONLY(msg))
{
	Displayf(msg);
}

ioDongle::DisplayMessageFunc ioDongle::sm_DisplayMessage = sDisplayMessage;


void ioDongle::Encode(char *buffer, int sizeInBytes)
{
	int						value;
	int						i;

	for(i=0; i<sizeInBytes; i++)
	{
		value= buffer[i];
		value += 61 - (77 * i)%13 + i/5;
		
		if(i>0)
		{
			value += buffer[i-1];
		}

		if(i<sizeInBytes-1)
		{
			value -= buffer[i+1] / 2;
		}

		value= value % 255;
		buffer[i]= (char)value;
	}
}

#if __XENON
bool ioDongle::CheckWriteCodeFile()
{
	if( !s_WritingCodeFile )
		return true;

	DWORD dwRet = XGetOverlappedResult(&s_Overlapped, NULL, FALSE);

	if( dwRet==ERROR_IO_INCOMPLETE )
	{
		return false;
	}
	else if( dwRet==ERROR_SUCCESS && s_DeviceID!=XCONTENTDEVICE_ANY )
	{
		const char16* displayName = reinterpret_cast<char16*>(L"Dongle");

		XCONTENT_DATA contentData;
		contentData.DeviceID = s_DeviceID;
		contentData.dwContentType = XCONTENTTYPE_PUBLISHER;
		safecpy(contentData.szDisplayName, displayName, XCONTENT_MAX_DISPLAYNAME_LENGTH);
		safecpy(contentData.szFileName, s_FileName, XCONTENT_MAX_FILENAME_LENGTH);

		DWORD dwContentFlags = XCONTENTFLAG_CREATEALWAYS DONGLE_SECURITY;
		DWORD dwRet = XContentCreate(XUSER_INDEX_NONE, s_SaveRoot, &contentData, dwContentFlags, NULL, NULL, NULL);

		if( dwRet==ERROR_SUCCESS )
		{
			char fullFileName[XCONTENT_MAX_FILENAME_LENGTH];
			safecpy(fullFileName,s_SaveRoot,sizeof(fullFileName));
			safecat(fullFileName,":\\",sizeof(fullFileName));
			safecat(fullFileName,s_FileName,sizeof(fullFileName));

			HANDLE hFile = CreateFile(fullFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

			if( hFile!=INVALID_HANDLE_VALUE )
			{
				size_t bufferKeyLen = StringLength(s_DongleString)+1;
				char* bufferDongleString = Alloca(char,bufferKeyLen);
				formatf(bufferDongleString, bufferKeyLen, s_DongleString);
				Encode(bufferDongleString, bufferKeyLen);

				size_t bufferMacLen = StringLength(s_MacAddString)+1;
				char* bufferMacAddress = Alloca(char,bufferMacLen);
				formatf(bufferMacAddress, bufferMacLen, s_MacAddString);
				Encode(bufferMacAddress, bufferMacLen);

				// Encoded Mac Address + Key String
				size_t bufferLen = bufferMacLen+bufferKeyLen+1;
				char * buffer = Alloca(char, bufferLen);
				sysMemCpy(buffer, bufferMacAddress, bufferMacLen); 
				sysMemCpy(buffer+bufferMacLen, bufferDongleString, bufferKeyLen);

				DWORD bytesWritten = 0;
				if( WriteFile(hFile, buffer, bufferLen, &bytesWritten, NULL)!=0 && bytesWritten==bufferLen )
				{
					Displayf("Code file SUCCESSFULLY written. %d", bufferLen);

					(*sm_DisplayMessage)("Code file SUCCESSFULLY written.");
				}
				else
				{
					(*sm_DisplayMessage)("Could not write to the file.");
				}
				CloseHandle(hFile);
			}
			else
			{
				(*sm_DisplayMessage)("Could not create the file.");
			}

			XContentClose(s_SaveRoot, NULL);
		}
		else if( dwRet==ERROR_ACCESS_DENIED )
		{
			(*sm_DisplayMessage)("User must be signed in.");
		}
		else if( dwRet==ERROR_INVALID_NAME )
		{
			(*sm_DisplayMessage)("Invalid root save name.");
		}
		else
		{
			(*sm_DisplayMessage)("Could not create the content package.");
		}
	}
	else
	{
		(*sm_DisplayMessage)("Invalid device selected.");
	}

	s_WritingCodeFile = false;
	return true;
}

#endif

#if __PS3

void 
ioDongle::RemoveDongle(const char * file)
{
	char filePathName[CELL_FS_MAX_FS_PATH_LENGTH];

	snprintf(filePathName, CELL_FS_MAX_FS_PATH_LENGTH, "%s%s", GAMEDATA_DIR, file);
	const fiDevice *device = fiDevice::GetDevice(GAMEDATA_DIR, false);
	if(!device)
		return;
	if(device->Delete(&filePathName[0]))
	{
		Displayf("[ioDongle] DONGLE REMOVED : %s", &filePathName[0]);
	}
	else
	{
		Displayf("[ioDongle] DONGLE NOT REMOVED: %s", &filePathName[0]);
	}
}

bool rage::ioDongle::LegacyReadCodeFile( const char* encodeString, const char* readBuffer, size_t readBufferSize)
{
	int err;
	CellNetCtlInfo cell_info;
	u8 mac[6];
	char macAddrString [18];

	if(0 <= (err = cellNetCtlGetInfo(CELL_NET_CTL_INFO_ETHER_ADDR, &cell_info)))
	{
		CompileTimeAssert(sizeof(mac) == CELL_NET_CTL_ETHER_ADDR_LEN);
		sysMemCpy(mac, cell_info.ether_addr.data, CELL_NET_CTL_ETHER_ADDR_LEN);

		// Get the Hexa String of the Mac Address
		sprintf(macAddrString, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
		macAddrString[17] = '\0';
	}
	else
	{
		Errorf("[ioDongle] LegacyReadCodeFile() - Error calling cellNetCtlGetInfo()  0x%08x", err);
	}

	/*-------------------------------*/
	size_t bufferMacAddressLen = StringLength(macAddrString)+1;
	size_t bufferKeyStringLen  = StringLength(encodeString)+1;
	size_t bufferLen = bufferMacAddressLen+bufferKeyStringLen;

	//#if __BANK
	//	Displayf("[ioDongle] ReadCodeFile() - Key : %s || %d", encodeString, bufferKeyStringLen);
	//	Displayf("[ioDongle] ReadCodeFile() - Mac Address : %s || %d", &macAddrString[0], bufferMacAddressLen);
	//#endif

	char* origBuffer = Alloca(char, bufferLen);
	// code to ensure release and beta versions of this work
	memset(origBuffer, 0xcd, bufferLen);

	if(readBufferSize < bufferLen)
	{
		Displayf("[ioDongle]LegacyReadCodeCheck failed");
		return false;
	}

	char* origBufferMacAddress = Alloca(char, bufferMacAddressLen);
	char* origBufferKeyString  = Alloca(char, bufferKeyStringLen);
	// code to ensure release and beta versions of this work
	memset(origBufferMacAddress, 0xcd, bufferMacAddressLen);
	memset(origBufferKeyString, 0xcd, bufferKeyStringLen);

	// Encode Mac Address
	formatf(origBufferMacAddress, bufferMacAddressLen, macAddrString);
	Encode(origBufferMacAddress, bufferMacAddressLen);
	// Encode Key String
	formatf(origBufferKeyString, bufferKeyStringLen, encodeString);
	Encode(origBufferKeyString, bufferKeyStringLen);
	// Mac Address + Key String
	sysMemCpy(origBuffer, origBufferMacAddress, bufferMacAddressLen); 
	sysMemCpy(origBuffer+bufferMacAddressLen, origBufferKeyString, bufferKeyStringLen);

	//#if __BANK
	//	Displayf("[ioDongle] ReadCodeFile() - Original Buffer Content: %s", origBuffer);
	//#endif // __PSN

	if( memcmp(readBuffer, origBuffer, bufferLen) == 0 )
	{
		//Displayf("AUTHORIZATION OK");
		return true;
	}
	else
	{
		if( memcmp(origBufferMacAddress, readBuffer, bufferMacAddressLen) != 0 )
		{
			//Displayf(" UNAUTHORIZED demo copy - The dongle is for a different machine.! ");
		}

		if( memcmp(origBufferKeyString, readBuffer+bufferMacAddressLen, bufferKeyStringLen) !=0 )
		{
			//Displayf(" UNAUTHORIZED demo copy - The dongle key does not match the game.! ");
		}
		return false;
	}
}

#endif // __PSN

#endif	// __XENON
