//
// input/mouse.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INPUT_MOUSE_H
#define INPUT_MOUSE_H

#if __WIN32
struct HWND__;
#endif

#include "input/remote_input_defs.h"
#include "system/wndproc.h"
#include "system/criticalsection.h"

// NOTE: This will also alter the the numerical representation in ioValue in mapper.cpp. This should not be enabled for
//		 consoles and is only for platforms that support the keyboard/mouse controls. If you need to turn this on for
//		 consoles can you contact Thomas Randall to discuss what the side effects are.
#define RSG_EXTRA_MOUSE_SUPPORT (RSG_PC)	// Be sure to setup scan options.

#if RSG_EXTRA_MOUSE_SUPPORT
#define RSG_EXTRA_MOUSE_SUPPORT_SWITCH(x,y) (x)
#define EXTRA_MOUSE_SUPPORT_ONLY(x)	x
#else
#define EXTRA_MOUSE_SUPPORT_ONLY(x)
#define RSG_EXTRA_MOUSE_SUPPORT_SWITCH(x,y) (y)
#endif // RSG_EXTRA_MOUSE_SUPPORT

#define MOUSE_SMOOTHING			(RSG_PC)
#define MOUSE_HISTORY_SIZE			4


namespace rage {

/*
	PURPOSE:
		Provide a simple interface to the system mouse.  Can retrieve both
		absolute screen coordinates and relative deltas as necessary.

	<FLAG Component>
*/
class ioMouse {
#if __WIN32PC
	friend rageLRESULT __stdcall InputWindowProc(struct HWND__ *,rageUINT,rageWPARAM,rageLPARAM);
	friend rageLRESULT __stdcall InputWindowProcMFC(struct HWND__ *,rageUINT,rageWPARAM,rageLPARAM);
#endif
	friend class ioEventQueue;
	REMOTE_INPUT_SUPPORT_ONLY(friend class ioRemoteInput);
	
public:
	// PURPOSE: 
	//	Initialize class.  Currently called automatically by graphics pipeline.
	//	Mouse relative motion (including the Z wheel) is always done using the raw 
	//  mouse device (it doesn't interfere with higher-level mouse handling) and mouse 
	//	absolute screen position is always done using OS mouse messages. The pipeline 
	//  will set this appropriately; in particular, leaving inWindow false will cause 
	//	the application to respond to button presses and releases even when it doesn't 
	//  have focus, which often interferes with proper bank operation.
	// PARAMS: inWindow - True if running in a window, else false
	//		   exclusiveMouse - True if app wants exclusive access to mouse, else false.  Defaults to false.
	static void Begin(bool inWindow, bool isExclusive = false);

	// PURPOSE: Shuts down class.  Currently called automatically by graphics pipeline.
	static void End();

	// PURPOSE: Update mouse position.  Currently called automatically by graphics pipeline.
	// NOTES: Call this once per frame; strictly speaking, you can call this as often as you
	// want, but the DX, DY, DZ motion counters are always relative to the last call.
	static void Update(bool ignoreInput = false);

	// PURPOSE: Reset bitmask of known-pressed buttons to zero.
	// NOTES: This is mostly for handling popup menus, where we don't see the release message.
	static void Reset() { m_WindowButtons = 0; }

	// RETURNS: Current window X coordinate of mouse cursor
	// NOTES: These are coordinates relative to the pipeline window.  They will not update when the
	// mouse cursor is outside the pipeline window.
	// If you are not using the gfx library, THESE WILL NOT UPDATE.
	static int GetX() { return m_X; }

	// RETURNS: Current window Y coordinate of mouse cursor
	// NOTES: These are coordinates relative to the pipeline window.  They will not update when the
	// mouse cursor is outside the pipeline window.
	// If you are not using the gfx library, THESE WILL NOT UPDATE.
	static int GetY() { return m_Y; }

	// RETURNS: Current window X coordinate of mouse cursor, normalized from 0..1
	// NOTES: (It can be out of this range during a drag operation!).
	static float GetNormX() { return m_X * g_InvWindowWidth; }

	// RETURNS: Current window Y coordinate of mouse cursor, normalized from 0..1
	// NOTES: (It can be out of this range during a drag operation!).
	static float GetNormY() { return m_Y * g_InvWindowHeight; }

	// RETURNS: Current inverse width of window, used for normalization
	// NOTES: This is automatically initialized by the graphics pipeline
	static float GetInvWidth() { return g_InvWindowWidth; }

	// RETURNS: Current inverse height of window, used for normalization
	// NOTES: This is automatically initialized by the graphics pipeline
	static float GetInvHeight() { return g_InvWindowHeight; }

	// RETURNS: Raw X motion counter since last update. Can ultimately depend on 
	// the exact mouse type and driver version. These counters work even when the 
	// cursor is "jammed" against the edge of the screen.
#if RSG_PC
	static float GetDX() { return m_dX; }
#else
	static float GetDX() { return (float)m_dX; }
#endif

	// RETURNS: Raw Y motion counter since last update. Can ultimately depend on 
	// the exact mouse type and driver version. These counters work even when the 
	// cursor is "jammed" against the edge of the screen.
#if RSG_PC
	static float GetDY() { return m_dY; }
#else
	static float GetDY() { return (float)m_dY; }
#endif

	// RETURNS: Raw Z motion (mouse wheel) counter since last update, one count per 
	// "click" of the wheel. A positive value indicates that the wheel was rotated 
	// forward, away from the user; a negative value indicates that the wheel was 
	// rotated backward, toward the user. 
	static int GetDZ() { return m_dZ; }

#if RSG_EXTRA_MOUSE_SUPPORT
	// RETURNS: Normalized X motion, useful for detecting the general direction the
	// mouse is traveling in when raw movements are too noisy. Can ultimately depend on 
	// the exact mouse type and driver version. These counters work even when the 
	// cursor is "jammed" against the edge of the screen.
	static float GetNormalizedX() {return m_nX; }

	// RETURNS: Normalized Y motion, useful for detecting the general direction the
	// mouse is traveling in when raw movements are too noisy. Can ultimately depend on 
	// the exact mouse type and driver version. These counters work even when the 
	// cursor is "jammed" against the edge of the screen.
	static float GetNormalizedY() {return m_nY; }

	// zeroes out the averaged buffer, useful to start over from a zero'd point.
	static void ClearAverageMovementBuffer();
#endif // RSG_EXTRA_MOUSE_SUPPORT

	// RETURNS: Bitmask of buttons currently being pressed
	static unsigned GetButtons() { return m_Buttons; }

	// RETURNS: Bitmask of buttons which have changed since last Update call
	static unsigned GetChangedButtons() { return m_LastButtons ^ GetButtons(); }

	// RETURNS: Bitmask of buttons which have been pressed since last Update call
	static unsigned GetPressedButtons() { return GetChangedButtons() & GetButtons(); }

	// RETURNS: Bitmask of buttons which have been released since last Update call
	static unsigned GetReleasedButtons() { return GetChangedButtons() & m_LastButtons; }

	// PURPOSE: Enable querying of mouse button state regardless of the active window
	static void EnableGlobalButtons(const bool enable) { m_EnableGlobalButtons = enable; }

#if __WIN32PC
	// PURPOSE: Flushes the mouse state.
	static void FlushMouse();

#if RSG_ASSERT
	// PURPOSE: Stops the mouse button state on asserts.
	// NOTES:	This calls FlushMouse but does not allow button presses until time period of focus after an assert. The reason for this
	//			is, on pc, an assert can fire that flushes the mouse state but the user can re-click the game window when the assert
	//			is up. This causes some buttons to get stuck in a weired state.
	static void OnAssert();
#endif // RSG_ASSERT

	// PURPOSE:	Bitmask of buttons that have been double clicked since last Update call.
	static unsigned GetDoubleClickedButtons() { return m_DoubleClickedButtons; }

	// PURPOSE: Only applies when mouse is set to exclusive mode, allow application to change mouse to/from exclusive mode.
	// (needed for Games for Windows Live which traps windows messages, so mouse must be non-exclusive when guide is active)
	static void SetExclusive(bool bExclusive);

	// RETURNS: true if mouse was set to be exclusive.
	static bool IsExclusiveSet()		{ return (m_Exclusive); }

	// RETURNS: true if mouse is currently exclusive.
	static bool IsCurrentlyExclusive()	{ return (m_IsCurrentlyExclusive); }

	static void DeviceLostCB();
	static void DeviceResetCB();

	// PURPOSE:	Shows or hides the mouse cursor.
	static void SetCursorVisible(bool visible);

	// PURPOSE: reset our fake absolute mouse position, so something like pause menu can force the mouse to start in the same place.
	// PARAMS:	x - the x position to set the mouse to.
	//			y - the y position to set the mouse to.
	static void SetMousePosition(int x, int y);

	// PURPOSE: Sets the mouse cursor position to a valid position in the window.
	// PARAMS:	x - the x position to set the mouse to in the window (must be between 0 and 1).
	//			y - the y position to set the mouse to in the window (must be between 0 and 1).
	// RETURNS:	true if the cursor position was successfully set.
	static bool SetCursorPosition(float x, float y);

	// PURPOSE: Gets the mouse coordinates according to windows.
	// PARAMS:	x - pointer to store the x cursor position.
	//			y - pointer to story the y cursor position.
	// RETURNS:	true if successfully retrieved the mouse coordinates.
	// NOTES:	There is a bug in Windows Vista where GetCursorPos() fails when passing an address > 2GB when large address aware is enabled.
	//			Applications SHOULD use this function rather than calling GetCursorPos().
	//			This will not succeed when called from any other thread than the thread that owns the window.
	static bool GetPlatformCursorPosition(int* x, int* y);

	// PURPOSE: Sets the mouse coordinates according to windows.
	// PARAMS:	x - the x position to set the windows cursor to.
	//			y - the y position to set the windows cursor to.
	// RETURNS:	true if successfully set the mouse coordinates.
	// NOTES:	This will not succeed when called from any other thread than the thread that owns the window.
	static bool SetPlatformCursorPosition(int x, int y);
	
	// PURPOSE:	The types of mouse input.
	enum MouseType
	{
		// PURPOSE:	Raw mouse input.
		RAW_MOUSE,

		// PURPOSE: DirectInput mouse input.
		DIRECTINPUT_MOUSE,

		// PURPOSE:	Windows messages mouse input.
		// NOTES:	Windows messages do not support relative mouse input, this is simulated.
		WINDOWS_MOUSE,
	};

#if MOUSE_SMOOTHING
	enum _tag_Mouse_Axis
	{
		MOUSE_X_AXIS = 0,
		MOUSE_Y_AXIS,
		MOUSE_AXIS_TOTAL
	};
#endif

	// PURPOSE:	Sets the type of mouse input.
	static void SetMouseType(MouseType type);

	// PURPOSE:	Retrieves the type of mouse input.
	static MouseType GetMouseType();

	// PURPOSE:	Indicates that the left/right mouse buttons are swapped (for left handed mode).
	static bool IsLeftRightButtonSwapped();

#if !__FINAL
	// PURPOSE:	Sets whether only absolute positions should be tracked from windows messages.
	// NOTES:	When relative positions are tracked the windows mouse cursor is reset to the center of the screen every frame.
	static void SetAbsoluteOnly(bool absoluteOnly);
#endif // !__FINAL

	// PURPOSE:	Indicates whether only absolute positions should be tracked from windows messages.
	// NOTES:	When relative positions are tracked the windows mouse cursor is reset to the center of the screen every frame.
	static bool GetAbsoluteOnly() { SYS_CS_SYNC(s_CS); return m_AbsoluteOnly || !m_HasFocus || !m_ClientAreaHasFocus; }

	// PURPOSE: Captures the mouse for input from outside game window.
	static void Capture();

	// PURPOSE:	Return true if we're ignoring input
	static bool IsIgnoringInput();

	// PURPOSE: Releases a captured mouse for input from outside the game window.
	static void ReleaseCapture();

	static s32 GetCaptureCount() { SYS_CS_SYNC(s_CS); return m_RefCount; }

	// PURPOSE: Informs the mouse that the capture of the mouse has been lost.
	// NOTES:	This should only be called by code that handles the WM_CAPTURECHANGED windows message!
	static void CaptureLost();

	// PURPOSE: Sets whether mouse movement is faked with controller input.
	// NOTES:	Ignores WM_MOUSEMOVE windows messages when controller driven.
	static void SetControllerDrivenMovement(bool controllerDriven)	{ m_ControllerDrivenMovement = controllerDriven; }

	// PURPOSE:	Indicates that the client area has mouse focus.
	// NOTES:	It is possible for the window to have focus but not the client area. If the client area has focus then the mouse is controlling the game.
	static bool ClientAreaHasFocus()								{ return m_ClientAreaHasFocus; }

	// PURPOSE:	Recapture any devices that are lost.
	static void RecaptureLostDevices();
#endif // __WIN32PC

	// RETURNS: True if mouse has a wheel
	static bool HasWheel() { return m_HasWheel; }

	// Bit definitions of the three standard mouse buttons
	// And then the other mouse buttons that may or may not exist (of which directInput allows for 8)
	enum {	MOUSE_LEFT		= 0x01, 
			MOUSE_RIGHT		= 0x02, 
			MOUSE_MIDDLE	= 0x04, 
			MOUSE_EXTRABTN1	= 0x08, 
			MOUSE_EXTRABTN2	= 0x10,
			MOUSE_EXTRABTN3	= 0x20,
			MOUSE_EXTRABTN4	= 0x40,
			MOUSE_EXTRABTN5	= 0x80,

			MOUSE_NUM_BUTTONS = 8,
	};

#if __WIN32PC
	// These are only public to make it easier to add to a Rag widget.
	static int sm_MouseSmoothingFrames;
	static float sm_MouseSmoothingThreshold;
	static float sm_MouseSmoothingBlendLimit;
#endif
#if MOUSE_SMOOTHING
	// This are only public to make it easier to add to a Rag widget.
	static float sm_MouseWeightScale;
#endif


private:
	static bool m_UseWindow;
	static bool m_HasWheel;
	static bool m_EnableGlobalButtons;
#if __WIN32PC
	// PURPOSE: Sets the cursor clip state if needed.
	static void SetCursorClip();

	// PURPOSE: Indicates if the game has mouse focus.
	static bool HasFocus();

	// PURPOSE: Update the cursor visibility settings.
	static void UpdateCursorVisibility();

	static MouseType m_MouseType;
	static bool m_Exclusive;
	static bool m_IsCurrentlyExclusive;
	static bool m_ControllerDrivenMovement;

	// PURPOSE:	Indicates if the mouse cursor is visible.
	static bool sm_Visible;

	// These hold the actual position according to windows (not the game!).
	static int m_wX, m_wY;

	// These hold the raw windows relative input.
	static int m_wdX[2], m_wdY[2];
	static int m_UpdateIndex;
	static sysCriticalSectionToken s_RawInput;

#if MOUSE_SMOOTHING
	static s32	 sm_MouseHistory[MOUSE_AXIS_TOTAL][MOUSE_HISTORY_SIZE];
	static s32	 sm_CurrentMouseInput;
#endif

	// The time since a particular button was last pressed.
	static u32 m_DoubleClickTimers[MOUSE_NUM_BUTTONS];

	// The time window in which two consecutive presses are marked as a double click.
	static u32 m_DoubleClickTimeLimit;

	// The buttons that have been double-clicked.
	static u32 m_DoubleClickedButtons;

	// Indicates that the mouse can only use absolute positions as the mouse is currently being shared so we cannot reset its position every frame.
	static bool m_AbsoluteOnly;

	// Used to store previous value of m_AbsoluteOnly so we can restore it to its previous value.
	static bool m_HasFocus;
	static bool	sm_DiDeviceLost;

	// PURPOSE: Indicates that the game client area has lost focus and has not been given back.
	// NOTES:	This is not the same as m_HasFocus which is for the whole window, this is just the game client area.
	static bool m_ClientAreaHasFocus;

	static int m_RefCount;

	static sysCriticalSectionToken s_CS;

	// PURPOSE:	Updates mouse button double clicks when no relevant windows messages are available.
	static void UpdateDoubleClick();

#if RSG_ASSERT
	// PURPOSE: Indicate that an assert has fired and we should ignore mouse button state until a set time has passed.
	static bool sm_HasAssertFired;

	// PURPOSE: The time we get focus after an assert to deal with mouse buttons getting stuck.
	static u32  sm_FocusAfterAssertTime;
#endif // RSG_ASSERT

#endif // __WIN32PC
	static int m_X, m_Y;
#if RSG_PC
	static float m_dX, m_dY;
	static int m_dZ;
#else
	static int m_dX, m_dY, m_dZ;
#endif
	static unsigned m_Buttons, m_LastButtons, m_WindowButtons;

#if RSG_ORBIS
	static unsigned m_InternalButtons;
#endif // RSG_ORBIS

	// PURPOSE: Used to time how long the window has been active. If it is smaller than a (very small) threshold then we ignore mouse clicks.
	//			This is to stop the clicks causing input when first clicking on a window.
	static u32 m_ActivateTimer;

private:
#if RSG_EXTRA_MOUSE_SUPPORT
	static void UpdateAverageMovement();

	const static float AVERAGE_MOUSE_WEIGHT;
	static float m_nX, m_nY;
#endif // RSG_EXTRA_MOUSE_SUPPORT
};

extern ioMouse MOUSE;

}	// namespace rage

#endif
