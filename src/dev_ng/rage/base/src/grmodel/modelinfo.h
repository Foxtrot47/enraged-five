//
// grmodel/modelinfo.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef GRMODEL_MODELINFO_H
#define GRMODEL_MODELINFO_H

#include "vector/matrix34.h"

namespace rage {

struct grmModelInfo {
	Matrix34 BoundBoxMtx;
	Vector4 BoundSphere;
	Vector3 BoundBoxSize;
};

}

#endif
