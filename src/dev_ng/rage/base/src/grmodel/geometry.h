// 
// grmodel/geometry.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRMODEL_GEOMETRY_H
#define GRMODEL_GEOMETRY_H

#include "data/base.h"
#include "data/safestruct.h"
#include "grcore/device.h"
#include "grcore/config.h"
#include "grcore/fvf.h"
#include "grcore/effect_config.h"		// for HACK_GTA4_MODELINFOIDX_ON_SPU, sigh
#include "mesh/mesh_config.h"
#include "vector/vector3.h"

#define MAX_VERT_BUFFERS (4)

#define USE_EXTRA_VERTS				1

#ifndef PH_NON_SPU_VIRTUAL
#if __SPU
#define PH_NON_SPU_VIRTUAL 
#define PH_NON_SPU_VIRTUAL_ONLY(X) X
#else
#define PH_NON_SPU_VIRTUAL virtual
#define PH_NON_SPU_VIRTUAL_ONLY(X)
#endif
#endif // ndef PH_NON_SPU_VIRTUAL

struct EdgeGeomPpuConfigInfo;

namespace rage {

struct rageEdgeGeomPpuConfigInfo;

class grcInstanceBuffer;
class mshMesh;
struct mshMaterial;
struct mshPacket;
class grmModel;
class Matrix34;
struct Matrix43;
class grmMatrixSet;
class grcVertexBufferEditor;
struct grmGeometryEdgeBlendHeader;
#if HACK_GTA4_MODELINFOIDX_ON_SPU
	struct CGta4DbgSpuInfoStruct;
#endif		

extern int grmStripCount,grmStripLength;

// GeometryCreateParams holds platform specific geometry tuning information
struct GeometryCreateParams {
#if HACK_GTA4
	GeometryCreateParams(bool e = true, bool etc = false, bool vehdmg = false, bool pedgeom=false, bool slodgeom=false, bool scg = false)
		: Edge(e)
		, EdgeTintColors(etc)
		, EdgeTintColorsTrees(etc)
		, EdgeVehicleDamage(vehdmg)
		, EdgePedGeom(pedgeom)
		, EdgeSlodGeom(slodgeom)
		, SkipCentroidGeneration(scg)
		, ReadWriteVtxBuf(false)
		, IsGpuInstanced(false)
		, CreateIndexBufferVertStream(false)
		, IsMicroMorph(false)
	{}
	bool Edge;
	bool EdgeTintColors;
	bool EdgeTintColorsTrees;
	bool EdgeVehicleDamage;
	bool EdgePedGeom;
	bool EdgeSlodGeom;
#else
	GeometryCreateParams(bool e = true, bool scg = false)
		: Edge(e)
		, SkipCentroidGeneration(scg)
		, ReadWriteVtxBuf(false)
		, IsGpuInstanced(false)
		, CreateIndexBufferVertStream(false)
		, IsMicroMorph(false)
	{}
	bool Edge;
#endif
	bool SkipCentroidGeneration;
	bool ReadWriteVtxBuf;
	bool IsGpuInstanced;
	bool CreateIndexBufferVertStream;	//This allows variable sized instance batches on xbox by using IDirect3DDevice9::DrawVertices
	bool IsMicroMorph;
};

class grmGeometry: public datBase {
public:
	grmGeometry(u8 type);
	grmGeometry(datResource&);
	virtual ~grmGeometry();
	DECLARE_PLACE(grmGeometry);

	static const int RORC_VERSION = 16;	

	/*	PURPOSE
			Initialize the geometry, allowing it to create the platform specific vertex data it needs
		PARAMS
			mesh - Toplevel mesh, used for blendtarget construction
			mtlIndex - The material that is used for this geometry chunk
			channelMask - Bitmask of which channels to allow through (generally driven by the shader)
			params - Platform specific tuning parameters (optional)
	*/
#if MESH_LIBRARY
	virtual bool Init(const mshMesh &mesh,int mtlIndex,u32 channelMask, const bool isCharClothMesh,const bool isEnvClothMesh,GeometryCreateParams* params = NULL, int extraVerts = 0) = 0;
#endif

	virtual void Draw() const = 0;
	virtual void DrawInstanced(grcInstanceBuffer *) const { }

	virtual void DirectDraw() const { Draw(); }
	virtual void Prefetch() const {}


	virtual void DrawSkinned(const grmMatrixSet &ms) const = 0;

	// RETURNS: A pointer to the fvf for this geometry object
	PH_NON_SPU_VIRTUAL const grcFvf* GetFvf() const;

	// RETURNS: A pointer to the matrix palette for this geometry
	virtual const u16* GetMatrixPalette() const;

	// RETURNS: the number of matrices in the matrix paletter
	virtual u16 GetMatrixPaletteCount() const;

	virtual void SetOffsets(grcVertexBuffer* pOffsets);

	virtual bool HasOffsets() const { return false; }

	virtual void SetBlendHeaders(u32 drawBuffer, grmGeometryEdgeBlendHeader **pHeader);

	PH_NON_SPU_VIRTUAL const grcVertexBuffer* GetVertexBuffer(bool bDrawBuffer = false) const;

	PH_NON_SPU_VIRTUAL grcVertexBuffer* GetVertexBuffer(bool bDrawBuffer = false);

	PH_NON_SPU_VIRTUAL const grcVertexBuffer* GetVertexBufferByIndex(unsigned vbIndex) const;

	PH_NON_SPU_VIRTUAL grcVertexBuffer* GetVertexBufferByIndex(unsigned vbIndex);

	static int GetVertexBufferIndex(bool bDrawBuffer = false);

	virtual void SetIndexBuffersPtr(grcIndexBuffer* IB);

	// PURPOSE:	Replace the current vertex buffer with a new one
	virtual void SetVertexBuffer(grcVertexBuffer *newBuffer);

	// PURPOSE: Substitute the vertex buffers for another set; the existing ones are not deallocated.
	virtual void ReplaceVertexBuffers(const atFixedArray<grcVertexBuffer*, MAX_VERT_BUFFERS> &);

	// RETURNS: A pointer to the index buffer for this geometry object
	PH_NON_SPU_VIRTUAL const grcIndexBuffer* GetIndexBuffer(bool bDrawBuffer = false) const;

	// RETURNS: A pointer to the index buffer for this geometry object
	PH_NON_SPU_VIRTUAL grcIndexBuffer* GetIndexBuffer(bool bDrawBuffer = false);

	// RETURNS: A pointer to a index buffer for this geometry object
	PH_NON_SPU_VIRTUAL const grcIndexBuffer* GetIndexBufferByIndex(unsigned ibIndex) const;

	// RETURNS: A pointer to a index buffer for this geometry object
	PH_NON_SPU_VIRTUAL grcIndexBuffer* GetIndexBufferByIndex(unsigned ibIndex);

	// RETURNS: The rendering primitive type for this geometry
	PH_NON_SPU_VIRTUAL u16 GetPrimitiveType() const;

	// RETURNS: The number of rendering primitives in this geometry
	PH_NON_SPU_VIRTUAL u32 GetPrimitiveCount() const;

#if USE_EXTRA_VERTS
	virtual u32 GetExtraVertsCount() const { return 0; }	
#endif

	virtual int GetCurrentVertsCount() const { return 0; }	

	// RETURNS: The number of vertices in this geometry
	PH_NON_SPU_VIRTUAL u32 GetVertexCount() const;

	// RETURNS: The number of indices in this geometry
	PH_NON_SPU_VIRTUAL u32 GetIndexCount() const;

	// PURPOSE: Recomputes all the normals, tangents, and binormals for this submesh
	// PARAMS
	//		readOnlyIndexBuffer - skips the buffer lock if using read only Index buffers
	PH_NON_SPU_VIRTUAL void RecomputeNormalsAndTangents(int nVerts);

	// PURPOSE: Recomputes all the normals for this submesh
	//		readOnlyIndexBuffer - skips the buffer lock if using read only Index buffers
	PH_NON_SPU_VIRTUAL void RecomputeNormals(int nVerts);

	// PARAMS: if true, 'createVB' will allocate and create the vertex buffers
	virtual grmGeometry* CloneWithNewVertexData(bool createVB, void* preAllocatedMemory, int& memoryAvailable) const = 0;

	virtual void EnableTripleBuffer(int tripleBufferFlags, void* preAllocatedMemory, int& memoryAvailable);

	virtual void DestroyVertexBuffersHackForFragmentCloth();

	// Do an N-way blend of other geometries into this one
	virtual void BlendPositionsFromOtherGeometries(grmGeometry *geom1,grmGeometry *geom2,float alpha1,float alpha2) = 0;
	virtual void AddPositionsFromOtherGeometries(grmGeometry* UNUSED_PARAM(geom), float UNUSED_PARAM(alpha)) {}

	enum AdjacencyStatus
	{
		AS_UNSUPPORTED_PLATFORM,
		AS_UNSUPPORTED_PRIMITIVE_TYPE,
		AS_UNEXPECTED_ERROR,
		AS_IN_PROGRESS,
		AS_ALREADY_DONE,
		AS_GENERATED,
	};

	// Transform triangle list into adjacency triangle list (from 3 indices per primitive to 6)
	virtual AdjacencyStatus GenerateAdjacency(const bool&) { return AS_UNSUPPORTED_PLATFORM; }

	grcVertexDeclaration*	GetDecl() const { return m_VtxDecl; }

	//NOTE: Setting vertex declaration should only be used if you are doing some custom rendering and will never need to use this geometry object's render path.
	void SetCustomDecl(grcVertexDeclaration *decl);
	grcVertexDeclaration  *GetCustomDecl() const { return m_VtxDecl; }

	static void SetMaxMatricesPerMaterial(int i) {s_MaxMatricesPerMaterial = i;}

	static int GetMaxMatricesPerMaterial() {return s_MaxMatricesPerMaterial;}

#if __ASSERT
	static void SetCheckSecondaryStreamVertexLength(bool b) {s_CheckSecondaryStreamVertexLength = b;}
#endif // __ASSERT

	// PURPOSE: specify how many buffers should be created when requesting a "double buffered" vertex buffer
	// PARAMS:
	//		   count - the number of buffer to generate, default is 3, min is 2 max is 4 
	//
	// NOTES:  2 works well for single threaded rendering apps, 3 for using multithreaded rendering (1 for update/1 for draw/1 for GPU)
	//         and 4 is needed on Xenon for multithread rendering and Predicted tiling ((1 for update/1 for draw/2 for double buffered command buffer)
	// NOTE:  This function should be called early, prior to generating any vertex buffers, and must match the value used when resources were generated.
	static void SetVertexMultiBufferCount(int count) {sm_MultiBufferCount = count;}

	static u32 GetVertexMultiBufferCount() { return sm_MultiBufferCount; }

	// PURPOSE: Given a mesh material, determine the FVF needed to contain the data in the mesh packet
	static void ComputeFVF (const mshMaterial &mtl, int mtxCount, u32 channelMask, grcFvf &outFvf, bool bDynamic = false);

	// These counters are updated as models are loaded.
	static int sm_TotalIndexCount, sm_TotalIndexSize;
	static int sm_TotalPrimCount;
	static int sm_TotalVertexCount, sm_TotalVertexSize;
	static int sm_TotalChannelSize[grcFvf::grcfcCount];

	static void ResetCounters() {
		sm_TotalIndexCount = sm_TotalIndexSize = sm_TotalPrimCount = sm_TotalVertexCount = sm_TotalVertexSize = 0;
		sysMemSet(sm_TotalChannelSize, 0, sizeof(sm_TotalChannelSize));
	}

	// RETURNS: type of the geometry (currently used to use non skinned shaders when using edge skinning)
	int GetType() const {return m_Type;}
	enum { GEOMETRYQB, GEOMETRYEDGE };

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s) = 0;
#endif

protected:
	grcVertexDeclaration*						m_VtxDecl;
	int											m_Type;			// really only needs to be a u8.

	static int									s_BuildBuffer[2];					// Index of the buffer currently able to be built
	static int									s_DrawBuffer[2];					// Index of the buffer currently able to be drawn
	static int									s_MaxMatricesPerMaterial;
	static int									sm_MultiBufferCount;
#if __ASSERT
	static bool									s_CheckSecondaryStreamVertexLength;
#endif	
};

/*	PURPOSE
		Represents a "packet" of vertex data, which is the data sent down to the 
		hardware for rendering.  This is VERY platform specific, but grmGeometry
		represents a platform independent version interface.

		Quad-buffered version necessary only for cloth.
*/
class grmGeometryQB : public grmGeometry {
	struct Packet;
public:
	grmGeometryQB();
	grmGeometryQB(class datResource&);
	~grmGeometryQB();

	// PARAMS: if true, 'createVB' will allocate and create the vertex buffers
	grmGeometry* CloneWithNewVertexData(bool createVB, void* preAllocatedMemory, int& memoryAvailable) const;

	void DestroyVertexBuffersHackForFragmentCloth();

	bool Init(const mshMesh& mesh,int mtlIndex,u32 channelMask,const bool isCharClothMesh,const bool isEnvClothMesh,GeometryCreateParams* params = NULL, int extraVerts = 0);

	// PURPOSE: Render the geometry packet
	virtual void Draw() const;

	// PURPOSE: Render the skinned geometry packet
	virtual void DrawSkinned(const grmMatrixSet &ms) const;

	virtual void DrawInstanced(grcInstanceBuffer *) const;

	// PURPOSE: Render the geo packet as simple as possible
	void DirectDraw() const;

	void Prefetch() const;		// prefetchs the vertex and index buffers

#define IDENTITY_MTX_PALETTE_IS_NULL		0 // RSG_ORBIS

#if IDENTITY_MTX_PALETTE_IS_NULL
	inline const u16* GetMatrixPalette() const { return m_MtxPalette? m_MtxPalette : sm_OneToOne; }
	static u16 sm_OneToOne[];
#else
	// RETURNS: A pointer to the matrix palette for this geometry
	inline const u16* GetMatrixPalette() const { return m_MtxPalette; }
#endif

	// RETURNS: the number of matrices in the matrix paletter
	inline u16 GetMatrixPaletteCount() const { return m_MtxCount; }

	// PURPOSE: Set the offset buffer to use when rendering this packet
	void SetOffsets(grcVertexBuffer* pOffsets);

	// RETURNS: A pointer to the fvf for this geometry object
	const grcFvf* GetFvf() const;//		{ return &m_Fvf; }

	// RETURNS: True if this geometry has an offset buffer
	bool HasOffsets() const		{ return m_OffsetBuffer != 0; }

	// PURPOSE: Enable or disable double buffering of vertex buffers
	// PARAMS:
	//	tripleBufferFlags - input param is bit mask using enBUFFER_TYPE
	// NOTES: 
	//	this function will create an additional vertex buffer if the flags are set.  This new vertex buffer will
	//	contain a copy of the data in the original vertex buffer.  Subsequent calls to GetVertexBuffer will return
	//	a pointer to the buildable buffer.  Subsequent calls to draw will use the drawable buffer.  Call SwapBuffers
	//	to alternate between the two
	void EnableTripleBuffer(int tripleBufferFlags, void* preAllocatedMemory, int& memoryAvailable);

	// PURPOSE: Switches vertex buffers between drawing and building for all geometry objects
	static void SwapBuffers();
	static void SwapSecondaryBuffers();
	static void SwapBuffersInternal(int idx);
	
	// PURPOSE: Reorder vertex buffer for more optimal CPU usage
	void MakeDynamic();

	// RETURNS: A pointer to the vertex buffer for this geometry object
	const grcVertexBuffer* GetVertexBuffer(bool bDrawBuffer = false) const;

	// RETURNS: A pointer to the vertex buffer for this geometry object
	grcVertexBuffer* GetVertexBuffer(bool bDrawBuffer = false);

	const grcVertexBuffer* GetVertexBufferByIndex(unsigned vbIndex) const;

	grcVertexBuffer* GetVertexBufferByIndex(unsigned vbIndex);

	// PURPOSE:	Replace the current vertex buffer with a new one
	void SetVertexBuffer(grcVertexBuffer *newBuffer);

	// PURPOSE: Substitute the vertex buffers for another set; the existing ones are not deallocated.
	void ReplaceVertexBuffers(const atFixedArray<grcVertexBuffer*, MAX_VERT_BUFFERS> &);

	// RETURNS: A pointer to the index buffer for this geometry object
	const grcIndexBuffer* GetIndexBuffer(bool bDrawBuffer = false) const;

	// RETURNS: A pointer to the index buffer for this geometry object
	grcIndexBuffer* GetIndexBuffer(bool bDrawBuffer = false);

	// RETURNS: A pointer to a index buffer for this geometry object
	const grcIndexBuffer* GetIndexBufferByIndex(unsigned ibIndex) const;

	// RETURNS: A pointer to a index buffer for this geometry object
	grcIndexBuffer* GetIndexBufferByIndex(unsigned ibIndex);

	// RETURNS: The rendering primitive type for this geometry
	u16 GetPrimitiveType() const { return m_PrimType; }

	// RETURNS: The number of rendering primitives in this geometry
	u32 GetPrimitiveCount() const { return m_PrimCount; }

	// RETURNS: The number of vertices in this geometry
	u32 GetVertexCount() const;

	// RETURNS: The number of indices in this geometry
	u32 GetIndexCount() const { return m_IndexCount; }

	// PURPOSE: Recomputes all the normals, tangents, and binormals for this submesh
	void RecomputeNormalsAndTangents(int nVerts);

	// PURPOSE: Recomputes all the normals for this submesh
	void RecomputeNormals(int nVerts);

	inline void RecomputeTangent(	Vec3V_In vecLUV_0, Vec3V_In vecLUV_1 , Vec3V_In vecLUV_2
		, Vec3V_In vEdge1, Vec3V_In vEdge2 
		, Vec3V_InOut tangent0, Vec3V_InOut tangent1, Vec3V_InOut tangent2
		, Vec3V_InOut biNormal0, Vec3V_InOut biNormal1, Vec3V_InOut biNormal2 );

	inline void RecomputeNormal(	Vec3V_In vEdge1, Vec3V_In vEdge2, Vec3V_InOut normal0, Vec3V_InOut normal1, Vec3V_InOut normal2 );

	inline void RecomputeBiNormal4( Vec3V_InOut tangentPtr0, Vec3V_InOut tangentPtr1, Vec3V_InOut tangentPtr2, Vec3V_InOut tangentPtr3
		, Vec3V_In tangent0, Vec3V_In tangent1, Vec3V_In tangent2, Vec3V_In tangent3
		, Vec3V_In n0, Vec3V_In n1, Vec3V_In n2, Vec3V_In n3
		, Vec3V_In biNormal0, Vec3V_In biNormal1, Vec3V_In biNormal2, Vec3V_In biNormal3 );

	inline u8 GetDoubleBuffered() const { return (m_DoubleBuffered & ~USE_SECONDARY_BUFFER_INDICES); }

	inline int GetBufferIndex() const { return (m_DoubleBuffered & USE_SECONDARY_BUFFER_INDICES) >> 2; }
	inline int GetBuildBuffer() const { return s_BuildBuffer[GetBufferIndex()]; }
	inline int GetDrawBuffer() const { return s_DrawBuffer[GetBufferIndex()]; }

	// Makes this geometry use the secondary draw and build indices.
	void SetUseSecondaryQBIndex(bool onOff);

	// We don't currently support skinned cpv-instanced objects on the assumption it's
	// only useful for static objects anyway.

	// PURPOSE: enable experimental code to try to reduce skin matrix downloading by detecting that matrices did not actually change between packets.
	//			disabled by default.
	// PARAMS:
	//		   enable - turn on/off the skined matrix change checking
	static void EnableSkinnedMatrixPretest(bool enable);

	// PURPOSE: function called by grmShaderFx::DrawSkinned() to begin/end experimental matrix index checking before downloading to the shader 
	// PARAMS:
	//		   start - true if starting new set of packets that share matrices, false when done with the set
	//         mtxCount - the max number of matrices in the object
	static void ResetPacketMatrices(bool start, int mtxCount=0);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	int GetStride() const;

	int GetCurrentVertsCount() const { return (int)m_VertexCount; }

#if USE_EXTRA_VERTS
	u32 GetExtraVertsCount() const { return (u32)m_ExtraVerts; }	
	void AllocFromExtra( int primToAlloc );
#endif

	virtual void SetIndexBuffersPtr(grcIndexBuffer* IB);

	virtual void BlendPositionsFromOtherGeometries(grmGeometry *geom1,grmGeometry *geom2,float alpha1,float alpha2);
	virtual void AddPositionsFromOtherGeometries(grmGeometry* geom, float alpha);

	virtual AdjacencyStatus GenerateAdjacency(const bool &isAlive);

	enum enBUFFER_TYPE
	{
		DONT_TRIPPLE_BUFFER		= 0,
		TRIPLE_VERTEX_BUFFER	= 1 << 0,
		TRIPLE_INDEX_BUFFER		= 1 << 1,
		USE_SECONDARY_BUFFER_INDICES = 1 << 2, // Or'd into the above.
	};

protected:
	atRangeArray<datOwner<grcVertexBuffer>, MAX_VERT_BUFFERS>	m_VB;
	atRangeArray<datOwner<grcIndexBuffer>,  MAX_VERT_BUFFERS>	m_IB;
	u32											m_IndexCount;
	u32											m_PrimCount;
	u16											m_VertexCount;
	u8											m_PrimType;
	u8											m_DoubleBuffered;
	u16*										m_MtxPalette; // data/struct.h requires that the array goes before the count
#if USE_EXTRA_VERTS
	u8											m_Stride;
	u8											m_ExtraVerts;
#else
	u16											m_Stride;
#endif
	u16											m_MtxCount;
	void*										m_VertexData;	// (must be at offset multiple of 16)
	grcVertexDeclaration*						m_VtxDeclOffset;
	datRef<grcVertexBuffer>						m_OffsetBuffer;
	u32											m_IndexOffset;

	friend class clothVertexBufferMemory;
	friend class clothController;
};

struct grmGeometryEdgeBlend {
	u32 Size;
	void *Addr;
	float Alpha[3];
	u32 Pad[3]; // 16-byte aligned
};

struct grmGeometryEdgeBlendHeader {
	grmGeometryEdgeBlendHeader(datResource&);
	int BlendCount;
	u32 BlendSize;
	u32 pad0, pad1;
	grmGeometryEdgeBlend Blends[0];

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif
	DECLARE_PLACE(grmGeometryEdgeBlendHeader);
};

// TODO: Ought to be in separate header.
class grmGeometryEdge: public grmGeometry
{
public:
	grmGeometryEdge();
	~grmGeometryEdge();
	grmGeometryEdge(datResource&);

	static bool IsAcceptableChannelMask(u32 channelMask);

	bool Init(const mshMesh &mesh,int mtlIndex,u32 channelMask,const bool isCharClothMesh,const bool isEnvClothMesh,GeometryCreateParams* params = NULL, int extraVerts = 0);
	virtual void Draw() const;
	virtual void DrawSkinned(const grmMatrixSet &ms) const;
	virtual const u16* GetMatrixPalette() const;
	virtual u16 GetMatrixPaletteCount() const;
	int	GetVertexAndIndex(Vector4 *pDstVerts, u32 maxDstVertsNum, Vector4 **pDstVertsPtrs, u16 *pDstIndices, u32 maxDstIndicesNum, u8 *iAndW, u32 iAndWsize, s32 *indexOffset=NULL, s32* indexStride=NULL, s32 *boneindexOffset1=NULL, s32 *boneindexOffset2=NULL, s32 *boneGroup2At=NULL, u32 *numVerts=NULL
							#if HACK_GTA4_MODELINFOIDX_ON_SPU
								,CGta4DbgSpuInfoStruct *gta4SpuInfoStruct=NULL
							#endif		
							,Matrix43* ms = NULL, u8 extractMask = 0xff) const;
	bool BegineGetVertexAndIndexAsync(u32** handle, Vector4 *pDstVerts, u32 maxDstVertsNum, Vector4 **pDstVertsPtrs, u16 *pDstIndices, u32 maxDstIndicesNum, u8 *iAndW, u32 iAndWsize, s32 *indexOffset=NULL, s32* indexStride=NULL, s32 *boneindexOffset1=NULL, s32 *boneindexOffset2=NULL, s32 *boneGroup2At=NULL, u32 *numVerts=NULL, u32 *numIndices=NULL
							#if HACK_GTA4_MODELINFOIDX_ON_SPU
									,CGta4DbgSpuInfoStruct *gta4SpuInfoStruct=NULL
							#endif		
							,Matrix43* ms = NULL, u8 extractMask = 0xff) const;
	bool FinalizeGetVertexAndIndexAsync(u32* handle) const;
	virtual void SetBlendHeaders(u32 drawBuffer, grmGeometryEdgeBlendHeader **pHeader);

	bool ClipGeomWithPlanes(const Vector4 **clipPlanes, int ClipPlaneCount, const u8 *boneRemapLut, unsigned boneRemapLutSize, Vector4 **OutputArray, Vector4* EndOfOutputArray, void *damageTexture, float boundsRadius, const Vector3* pBoneNormals, float dotProdThreshold
							#if HACK_GTA4_MODELINFOIDX_ON_SPU
								,CGta4DbgSpuInfoStruct *gta4SpuInfoStruct=NULL
							#endif		
							) const;
	
	u32 GetPrimitiveCount() const;
	u16 GetPrimitiveType() const { return drawTris;}
	u32 GetVertexCount() const;

	bool IsSkinned() const;

	const grmGeometryEdgeBlendHeader* GetBlendHeader(int i) const;
	grmGeometryEdgeBlendHeader* GetBlendHeader(int i);
	int GetBlendHeaderCount() const { return m_Count; }

	void FreeBlendHeaders();

	const EdgeGeomPpuConfigInfo* GetEdgeGeomPpuConfigInfos() const { return (EdgeGeomPpuConfigInfo*)m_Data; }
	int GetEdgeGeomPpuConfigInfoCount() const { return m_Count; }

	Vector3 GetOffset() const { return m_Offset; }

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	// PARAMS: if true, 'createVB' will allocate and create the vertex buffers
	grmGeometry* CloneWithNewVertexData(bool createVB, void* preAllocatedMemory, int& memoryAvailable) const;

	// Do an 2-way blend of other geometries into this one
	// You can perform an N-way blend by doing the extra passes with the previous output as one of the inputs
	virtual void BlendPositionsFromOtherGeometries(grmGeometry *geom1,grmGeometry *geom2,float alpha1,float alpha2);

private:
	rageEdgeGeomPpuConfigInfo *m_Data;
	Vector3 m_Offset;
	int m_Count;
	u8 m_Pad[12];
};


inline const grcVertexBuffer* grmGeometryQB::GetVertexBufferByIndex(unsigned vbIndex) const
{
	Assert( vbIndex < MAX_VERT_BUFFERS );
	return m_VB[vbIndex];
}

inline grcVertexBuffer* grmGeometryQB::GetVertexBufferByIndex(unsigned vbIndex)
{
	return const_cast<grcVertexBuffer*>(const_cast<const grmGeometryQB*>(this)->GetVertexBufferByIndex(vbIndex));
}

inline const grcIndexBuffer* grmGeometryQB::GetIndexBufferByIndex(unsigned ibIndex) const
{
	Assert( ibIndex < MAX_VERT_BUFFERS );
	return m_IB[ibIndex];
}

inline grcIndexBuffer* grmGeometryQB::GetIndexBufferByIndex(unsigned ibIndex)
{
	return const_cast<grcIndexBuffer*>(const_cast<const grmGeometryQB*>(this)->GetIndexBufferByIndex(ibIndex));
}

}	// namespace rage

#endif
