//
// grmodel/shader.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef GRMODEL_SHADER_H
#define GRMODEL_SHADER_H


// #include "shadervar.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "data/base.h"
#include "data/resource.h"
#include "string/string.h"
#include "vector/color32.h"
#include "grcore/effect.h"
#include "grcore/texture.h"
#include "grcore/im.h"
#include "system/codecheck.h"


namespace rage {

class atString;
class grmModel;
class grmMatrixSet;
class grcTexture;
class Matrix34;
class grmShaderGroup;
class fiBaseTokenizer;
class fiTokenizer;
class shaderMaterialGeoInstance;
#if __D3D11
class grcBufferD3D11Resource;
#endif 

extern bool grcFlashEnable;		// This changes once every second or so and is used to control flashing shaders

/*
PURPOSE
	Factory object for shaders; it is allowed to create a different subclass of
	grmShader depending on the supplied filename.
	It acts as more of a "multi-ton" than a singleton, so the usual GetInstance,
	Push/Pop instance calls exist.
<FLAG Component>
*/
class grmShaderFactory {
public:
	grmShaderFactory();
	virtual ~grmShaderFactory();

	// DOM-IGNORE-BEGIN
	static grmShaderFactory& GetInstance() { FastAssert(sm_Instance); return *sm_Instance; }
	static void SetInstance(grmShaderFactory &instance) { sm_Instance = &instance; }
	static void PushInstance(grmShaderFactory &newInst) {
		FastAssert(!sm_PrevInstance);
		sm_PrevInstance = sm_Instance;
		sm_Instance = &newInst;
	}
	static void PopInstance() {
		FastAssert(sm_PrevInstance);
		sm_Instance = sm_PrevInstance;
		sm_PrevInstance = 0;
	}
	// DOM-IGNORE-END

	// PURPOSE: Create the standard RAGE shader factory
	// PARAMS: bMakeActive - set to true to make it the active shader factory
	// RETURNS: An instance of grmShaderFactory
	static grmShaderFactory *CreateStandardShaderFactory(bool bMakeActive = true);

#if __TOOL	
	// RETURNS: The relative path to the shader lib folder
	static const char *GetShaderLibPath();

	// PURPOSE: Sets the relative path to the shader lib folder (allocates & deallocates memory)
	static void SetShaderLibPath(const char *path);

	// PURPOSE: Clean up memory allocated by SetShaderLibPath
	static void ClearShaderLibPath();
#endif	// __TOOL

	virtual class grmShader*		Create() = 0;

	// PURPOSE: Create a shadergroup
	virtual class grmShaderGroup*   CreateGroup() = 0;
	
	// PURPOSE: Load a shadergroup from the specified filename or tokenizer
	virtual class grmShaderGroup*	LoadGroup(fiTokenizer & T) = 0;
	virtual class grmShaderGroup*	LoadGroup(const char *filename) = 0;
	
	// PURPOSE: Allow shaderfactory to deal with shader as it's streamed in
	virtual void PlaceShader(class datResource&,grmShader** shaders,int count) = 0;
	// PURPOSE: Allow shaderfactory to deal with shadergroup as it's streamed in
	virtual void PlaceShaderGroup(class datResource&,grmShaderGroup* &) = 0;
	// PURPOSE: Iterate through all shaders in the specified path and load them into memory
	virtual void PreloadShaders( const char *path, const char* preloadFileName = "preload" ) = 0;

protected:
	static void CreateCommon();

private:
	static grmShaderFactory *sm_Instance;
	static grmShaderFactory *sm_PrevInstance;
#if __TOOL
	static const char *sm_ShaderLibPath;
#endif	// __TOOL
};


/*	PURPOSE
		The default shader factory used by RAGE.  Most custom factories can just
		derive off of this since it handles most of the grunt work for you.
*/
class grmShaderFactoryStandard: public grmShaderFactory {
public:
	// DOM-IGNORE-BEGIN
	// overrides
	grmShader* Create();
	grmShaderGroup*   CreateGroup();
	grmShaderGroup*	  LoadGroup(fiTokenizer & T);
	grmShaderGroup*   LoadGroup(const char *filename);
	void PlaceShader(class datResource&,grmShader** shaders,int count);
	void PlaceShaderGroup(datResource &rsc,grmShaderGroup* &group);
	void PreloadShaders( const char *path, const char* preloadFileName = "preload" );
	// DOM-IGNORE-END
};


typedef grcEffect::VariableInfo grmVariableInfo;

/*
PURPOSE
	grmShader is the base class for all shaders used by grcore.  Shaders may have one or more passes
	and may affect any number of graphics states.
<FLAG Component>
*/
class grmShader {
	friend class grmShaderSorter;
public:
	static const int RORC_VERSION = 10 + grcEffect::RORC_VERSION;

	explicit grmShader(const grmShader* copyme);

	grmShader();
	grmShader(class datResource&);
	DECLARE_PLACE(grmShader);
	~grmShader();

	// PURPOSE:	Sets up default technique groups
	static void InitClass();

	// PURPOSE:	Tears down default technique groups
	static void ShutdownClass();

#if RAGE_INSTANCED_TECH
	static int GetInstancedTechOffset()	{ return sm_InstancedTechOffset; }
	static void SetInstancedDrawMode(bool bInstanced);
#endif
	// static grmShader* GetFirst() { return sm_First; }

	// grmShader* GetNext() { return m_Next; }

	// PURPOSE: Blits a rectangle using this shader, handles shader variables & multiple passes
	// PARAMS: x1, y1 - The top left corner in screen coordinates of the blit
	//			x2, y2 - The bottom right corner in screen coordinates of the blit
	//			z - The screen space depth to use for the blit
	//			u1, v1 - The floating point u,v texture coords for top left of blit
	//			u2, v2 - The floating point u,v texture coords for bottom right of blit
	//			Color - the color to use for the blit			
	void TWODBlit(float x1,float y1,float x2,float y2,float z,float u1,float v1,float u2,float v2,Color32 color,grcEffectTechnique tech) const;

	// PURPOSE: Blits a rectangle using this shader, handles shader variables & multiple passes
	// PARAMS: x1, y1 - The top left corner in screen coordinates of the blit
	//			x2, y2 - The bottom right corner in screen coordinates of the blit
	//			z - The screen space depth to use for the blit
	//			u1, v1 - The floating point u,v texture coords for top left of blit
	//			u2, v2 - The floating point u,v texture coords for bottom right of blit
	//			Color - the color to use for the blit			
	void BlitIntoCubeMapFace(grcCubeFace face, grcEffectTechnique technique);

	enum { FLAG_USES_PRESETS = BIT0,
			FLAG_FLASH_INSTANCES = BIT1,	// For debugging - flash this shader if this set
			FLAG_DRAW_WIREFRAME = BIT2,		// For debugging - draw objects using this shader in wireframe mode
			FLAG_DISABLE_INSTANCES = BIT3,	// For debugging - do not render this shader if this is set
			FLAG_INHERITS_DRAWBUCKET = BIT4,
	};

	enum { BASIC, TEMPLATE, FX, RMC_TYPECOUNT };
	
	// PURPOSE: Types we support in shaders natively:
	//	Standard models (no per-vertex color data)
	//	Skinned, standard skinned models (includes skeletons)
	//	Blits, positions are pretransformed (now a technique group instead of a technique since it's rarely used)
	enum eDrawType { 
		RMC_DRAW = 0, 
		RMC_DRAWSKINNED, 
	#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES	
		RMC_DRAW_TESSELLATED, 
		RMC_DRAWSKINNED_TESSELLATED, 
	#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES	
	#if RAGE_INSTANCED_TECH
		RMC_DRAW_INSTANCED,
		RMC_DRAWSKINNED_INSTANCED,
	#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
		RMC_DRAW_INSTANCED_TESSELLATED,
		RMC_DRAWSKINNED_INSTANCED_TESSELLATED,
	#endif
	#endif // instanced enum types must be placed as the last ones
		RMC_DRAWCOUNT };

	/*	PURPOSE
		Load shader from a specified template.
		PARAMS
		templateName - If the string ends in .sps, we look up the preset and use the preset
						to determine the actual shader to load.  If it has no extension, it is
						assumed to directly reference a shader by name.
		T - tokenizer; if not null, it is presumed to point at the start of .sva-style initialization
						text which is used to specify the per-instance data.  Parsing will terminate
						on an unbalanced closing curly brace or EOF, allowing us to parse either separate
						.sva files or inlined sva information.
		fallback - if this is true, then grcEffect::CreateWithFallback will be used, otherwise grcEffect::Create will be used
	*/
	bool Load(const char *templateName,fiTokenizer *T = NULL,bool fallback = true);

	/*	PURPOSE
		Draws a piece of geometry using this shader.  Typical implementation will call this->Bind and then
		geom.Draw.  Draw is provided as an explicit entry point for multipass effects and to allow any
		cleanup necessary after rendering the geometry.
		PARAMS
		model - Model containing the geometry we want to render with this shader
		data - Per-instance local variables to use when binding the shader (only used by grmShaderComplex and its subclasses)
		geom - Index of geometry to render with this shader
		lod - Level of detail to use when rendering shaders.  For grmShaderComplex, every pass has an lod associated with
			it in the shader language; if the per-pass lod is greater-than-or-equal-to this lod, the pass is rendered.
	*/
	void Draw(const grmModel& model,int geom, int lod,bool restoreState) const;
	void DrawInternal(eDrawType type, const grmModel& model, int geom, bool restoreState) const;

	/*	PURPOSE
		Draws a piece of geometry using this shader and a set of bone matrices.  Typical implementation will call this->Bind 
		and then geom.DrawSkinned.  DrawSkinned is provided as an explicit entry point for multipass effects and to allow any
		cleanup necessary after rendering the geometry.
		PARAMS
		model - Model containing the geometry we want to render with this shader
		data - Per-instance local variables to use when binding the shader (only used by grmShaderComplex and its subclasses)
		geom - Index of geometry to render with this shader
		mtxts - Array of bone matrices
		mtxCount - Count of bone matrices
		lod - Level of detail to use when rendering shaders.  For grmShaderComplex, every pass has an lod associated with
			it in the shader language; if the per-pass lod is greater-than-or-equal-to this lod, the pass is rendered.
	*/
	void DrawSkinned(const grmModel& model,int geom, const grmMatrixSet &ms, int lod, bool restoreState) const;
	void DrawSkinnedInternal(eDrawType type, const grmModel& model, int geom, const grmMatrixSet &ms, bool restoreState) const;

	/*	PURPOSE
		Does any prep work that may be needed by a shader before any passes are rendered.
		This will only be needed on rare occasions, but is here in case shaders are used for
		procedural geometry.  If you are using shaders with VGL, you will be better prepared
		for future shaders if you call BeginDraw() and EndDraw() methods outside of your main
		pass loop.
		RETURNS
	*/
	RETURNCHECK(int) BeginDraw(eDrawType type, bool restoreState, grcEffectTechnique techOverride = grcetNONE) const;
	
	/*	PURPOSE
		Does any cleanup work that may be needed by a shader after all passes are rendered.
		This will only be needed on rare occasions, but is here in case shaders are used for
		procedural geometry.  If you are using shaders with VGL, you will be better prepared
		for future shaders if you call BeginDraw() and EndDraw() methods outside of your
		render pass loop.
	*/
	void EndDraw() const;

	/*	PURPOSE
		Sets up all render state associated with a shader.  Provided as a separate entry point from the Draw... functions so that
		shaders can be used for particles or anything else that doesn't have an grmModel associated with it.
		PARAMS
		locals - Array of local variables to use (grmShaderComplex only at present)
		globals - Array of global variables (like time, etc) to use (grmShaderComplex only at present)
		pass - Which pass of a multi-pass shader to make active
	*/
	void Bind(int pass = 0) const;

	void IncrementalBind( int pass = 0 ) const;

	/*	PURPOSE
		Clean up any render state associated with a shader.  This is primarily used as a
		separate entry point from the various Draw methods, so that it can be used with
		procedural geometry, such as vgl and particles.
	 */
	void UnBind() const;

#if __PPU
	RETURNCHECK(int) RecordBeginDraw(eDrawType type, bool restoreState, grcEffectTechnique techOverride = grcetNONE) const;
	void RecordEndDraw() const;
	void RecordBind(int pass) const;
	void RecordUnBind() const;
#endif


#if __PS3
	/*	PURPOSE
		PS3 only - Gets the (default) number of registers used by a shader
	 */
	u8 GetInitialRegisterCount(int pass = 0) const;
	void SetInitialRegisterCount(const char *technique, int pass, int registerCount);
#endif

#if !__SPU
	/* RETURNS: Name of current shader in human-readable form, useful for debugging */
	const char* GetName() const { return m_EffectInstance.GetBasis().GetEffectName(); }

	/* RETURNS: Name of mtl file used only by shaderfx */
	const char* GetMtlFileName() const { return m_EffectInstance.GetMaterialName(); }

#if EFFECT_PRESERVE_STRINGS
	/* RETURNS: The number of instanced variables for this shader */
	int GetInstancedVariableCount() const { return m_EffectInstance.GetBasis().GetInstancedVariableCount(); }

	/*	PURPOSE
		Get the count of instanced shader variables for a particular shader -- useful for tool builds
		PARAMS
		idx - the index of the instanced variable to get -- should be between 0 & NumInstancedVars
		outInfo - the data structure to be filled by the shader
	 */
	void GetInstancedVariableInfo(int idx, grmVariableInfo &outInfo) const { return m_EffectInstance.GetBasis().GetInstancedVariableInfo(idx,outInfo); }
#endif

	// PURPOSE: Bitmask of which passes to draw; intended for debugging but always available, just in case.
	static void SetMaxPasses(int maxPasses) { sm_MaxPasses = maxPasses; }

	// RETURNS: The number of passes needed by this shader
	static int &GetMaxPasses() { return sm_MaxPasses; }

	// PURPOSE: Set a string name for a particular bucket, some shaders can be configured to use this
	// CALLER IS RESPONSIBLE FOR MANAGING PASS NAME STRING MEMORY
	static void SetDrawBucketName(int pass,const char *name) { sm_DrawBucketNames[pass] = name; }

	// RETURNS: The string name for a bucket given a particular pass
	static const char *GetDrawBucketName(int pass) { return sm_DrawBucketNames[pass]; }
#endif	// __SPU

#if __BANK
	// PURPOSE: Add widgets for this shader
	void AddWidgets(class bkBank&);
#endif

	// RETURNS: The flags used for this shader
	// NOTES: Could use InstanceData?
	// int GetFlags() const { return m_Flags; }

	// PURPOSE: Sets the non-user flag value
	// void SetFlags(u8 flags) { m_Flags = flags; }

	// RETURNS: The drawbucket this shader needs
	int GetDrawBucket() const { return m_EffectInstance.DrawBucket; }

	u32 GetDrawBucketMask() const { return m_EffectInstance.DrawBucketMask; }

	bool IsInstanced() const { return m_EffectInstance.IsInstanced; }

	// PURPOSE: Sets the drawbucket of this shader
	void SetDrawBucket(int id) { Assign(m_EffectInstance.DrawBucket, id); m_EffectInstance.DrawBucketMask = BUCKETMASK_GENERATE(id); }
	void SetDrawBucketMask(u32 mask) { m_EffectInstance.DrawBucketMask = mask; }

#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif // __DECLARESTRUCT

	// PURPOSE: Set the default draw bucket to init shaders to
	static void SetDefaultDrawBucket(int drawBucket) { sm_DefaultDrawBucket = drawBucket; }

	// RETURNS: The default draw bucket to use for a shader
	static int GetDefaultDrawBucket() { return sm_DefaultDrawBucket; }
	static int sm_DefaultDrawBucket;

	// PURPOSE: Init the global shader data object
	static void InitGlobals();

	// PURPOSE: Kill the global shader data object
	static void ShutdownGlobals();

	inline grcEffectVar LookupVar(const char *name,bool mustExist = true) const;
	inline grcEffectVar LookupVarByHash(u32 hashCode) const;
	inline bool IsVar(grcEffectVar var) const;

	inline grcEffectTechnique LookupTechnique(const char *name,bool mustExist = true) const;
	inline int GetPassCount(grcEffectTechnique tech) const;

	inline int GetTechniqueCount() const;
	inline grcEffectTechnique GetTechniqueByIndex(int idx) const;
#if EFFECT_PRESERVE_STRINGS
	inline const char *GetTechniqueName(grcEffectTechnique tech) const;
	inline const char* GetCurrentTechniqueName() const;
#endif
	inline grcEffectTechnique GetCurrentTechnique() const;

	inline const grcInstanceData &GetInstanceData() const;

	inline grcInstanceData &GetInstanceData();

	// RETURNS:	A unique hash code based on the underlying effect's name.
	// NOTES:	Different shaders based on the same underlying effect will return the same hash code.
	//			This hash code will be stable across runs since it's based on the effect filename.
	inline u32 GetHashCode() const { return m_EffectInstance.GetBasis().GetHashCode(); }

	/* void SetSortKey(u32 sortKey) { m_EffectInstance.SortKey = sortKey; } 

	int GetSortKey() const { return m_EffectInstance.SortKey; }  */

	u8 GetUserFlags() const { return (u8) m_EffectInstance.UserFlags; }

	void SetUserFlags(u8 flags) { m_EffectInstance.UserFlags = flags; }

	inline void SetVar(grcEffectVar var,const grcTexture *v);
#if SUPPORT_UAV
	inline void SetVar(grcEffectVar var,const grcBufferUAV *v);
	inline void SetVarUAV(grcEffectVar var,grcBufferUAV *value,int initCount=-1);
	inline void SetVarUAV(grcEffectVar var,const grcTextureUAV *value);
#endif	//SUPPORT_UAV
	inline void SetVar(grcEffectVar var,int v);
	inline void SetVar(grcEffectVar var,const IntVector &v);
	inline void SetVar(grcEffectVar var,float v);
	inline void SetVar(grcEffectVar var,const Vector2& v);
	inline void SetVar(grcEffectVar var,const Vector3& v);
	inline void SetVar(grcEffectVar var,Color32 v);
	inline void SetVar(grcEffectVar var,const Vector4& v);
	inline void SetVar(grcEffectVar var,const Matrix34& v);
	inline void SetVar(grcEffectVar var,const Matrix44& v);
	
	inline void SetVar(grcEffectVar var,const float *v,int count);
	inline void SetVar(grcEffectVar var,const Vector2* v,int count);
	inline void SetVar(grcEffectVar var,const Vector3* v,int count);
	inline void SetVar(grcEffectVar var,const Color32 *v,int count);
	inline void SetVar(grcEffectVar var,const Vector4* v,int count);
	inline void SetVar(grcEffectVar var,const Matrix34* v,int count);
	inline void SetVar(grcEffectVar var,const Matrix44* v,int count);

	inline void SetVar(grcEffectVar var,const Vec2V & v);
	inline void SetVar(grcEffectVar var,const Vec3V & v);
	inline void SetVar(grcEffectVar var,const Vec4V& v);
	inline void SetVar(grcEffectVar var,const Vec4f& v);
	inline void SetVar(grcEffectVar var,const Mat34V& v);
	inline void SetVar(grcEffectVar var,const Mat44V& v);

	inline void SetVar(grcEffectVar var,const Vec2V* v,int count);
	inline void SetVar(grcEffectVar var,const Vec3V* v,int count);
	inline void SetVar(grcEffectVar var,const Vec4V * v,int count);
	inline void SetVar(grcEffectVar var,const Vec4f * v,int count);
	inline void SetVar(grcEffectVar var,const Mat34V * v,int count);
	inline void SetVar(grcEffectVar var,const Mat44V * v,int count);
	inline void RecordSetVar(grcEffectVar var,const grcTexture *v)	{ m_EffectInstance.GetBasis().RecordSetVar(m_EffectInstance, var, v); }
	inline void RecordSetVar(grcEffectVar var,float v)				{ m_EffectInstance.GetBasis().RecordSetVar(m_EffectInstance, var, v); }
	inline void RecordSetVar(grcEffectVar var,const Vector2& v)		{ m_EffectInstance.GetBasis().RecordSetVar(m_EffectInstance, var, v); }
	inline void RecordSetVar(grcEffectVar var,const Vector3& v)		{ m_EffectInstance.GetBasis().RecordSetVar(m_EffectInstance, var, v); }
	inline void RecordSetVar(grcEffectVar var,const Vector4& v)		{ m_EffectInstance.GetBasis().RecordSetVar(m_EffectInstance, var, v); }

	inline void SetVarByRef(grcEffectVar var,const Vector2& v);
	inline void SetVarByRef(grcEffectVar var,const Vector3& v);
	inline void SetVarByRef(grcEffectVar var,const Vector4& v);
	inline void SetVarByRef(grcEffectVar var,const Matrix34& v);
	inline void SetVarByRef(grcEffectVar var,const Matrix44& v);
	inline void SetVarByRef(grcEffectVar var,const Vector3* v,int count);
	inline void SetVarByRef(grcEffectVar var,const Vector4* v,int count);
	inline void SetVarByRef(grcEffectVar var,const Matrix34* v,int count);
	inline void SetVarByRef(grcEffectVar var,const Matrix44* v,int count);

	inline void SetVarByRef(grcEffectVar var,const Vec2V& v);
	inline void SetVarByRef(grcEffectVar var,const Vec3V& v);
	inline void SetVarByRef(grcEffectVar var,const Vec4V& v);
	inline void SetVarByRef(grcEffectVar var,const Vec4f& v);
	inline void SetVarByRef(grcEffectVar var,const Mat34V& v);
	inline void SetVarByRef(grcEffectVar var,const Mat44V& v);
	inline void SetVarByRef(grcEffectVar var,const Vec3V* v,int count);
	inline void SetVarByRef(grcEffectVar var,const Vec4V* v,int count);
	inline void SetVarByRef(grcEffectVar var,const Vec4f* v,int count);
	inline void SetVarByRef(grcEffectVar var,const Mat34V* v,int count);
	inline void SetVarByRef(grcEffectVar var,const Mat44V* v,int count);

#if __D3D11 || RSG_ORBIS
	inline grcCBuffer *GetVarCBufferAndOffset(grcEffectVar var, int &Offset);
#endif //__D3D11

	inline void GetVar(grcEffectVar var,grcTexture*&v) const;
	inline void GetVar(grcEffectVar var,float &v) const;
	inline void GetVar(grcEffectVar var,Vector2& v) const;
	inline void GetVar(grcEffectVar var,Vector3& v) const;
	inline void GetVar(grcEffectVar var,Vector4& v) const;
	inline void GetVar(grcEffectVar var,Matrix34& v) const;
	inline void GetVar(grcEffectVar var,Matrix44& v) const;

	inline int GetVarCount() const;
	inline grcEffectVar GetVarByIndex(int idx) const;
#if EFFECT_PRESERVE_STRINGS
	inline void GetVarDesc(grcEffectVar var,const char *&name,grcEffect::VarType &type,int &annotationCount,bool &isGlobal,const char **actualName = NULL) const;
#endif // EFFECT_PRESERVE_STRINGS

#if !__SPU
	inline grcSamplerStateHandle GetSamplerState(grcEffectVar var) const;
	inline void SetSamplerState(grcEffectVar var,grcSamplerStateHandle h);
	inline void PushSamplerState(grcEffectVar var,grcSamplerStateHandle h);
	inline void PopSamplerState(grcEffectVar var);
#endif

	inline bool InheritsBucketId() const;

#if __BANK
	bool IsFlashing() const								{ return grcFlashEnable && (m_EffectInstance.UserFlags & FLAG_FLASH_INSTANCES); }
	bool IsDisabled() const								{ return (m_EffectInstance.UserFlags & FLAG_DISABLE_INSTANCES) != 0; }
	// RETURNS: TRUE if this object is to be rendered in wireframe mode for debugging purposes
	bool IsDebugWireframeOverride() const				{ return (m_EffectInstance.UserFlags & FLAG_DRAW_WIREFRAME) != 0; }
#endif // __BANK

	static void ReloadAll();

	void Optimize(const grmModel& /*model*/,int /*geom*/);

	void CopyPassData(grcEffectTechnique techHandle, int passIndex, grmShader *const source) const;

#if RSG_PC || RSG_DURANGO || RSG_ORBIS
	const grcComputeProgram* GetComputeProgram(int pass) const	{ return m_EffectInstance.GetBasis().GetComputeProgram(pass); }
	grcComputeProgram* GetComputeProgram(int pass) 				{ return m_EffectInstance.GetBasis().GetComputeProgram(pass); }
#endif

#if !__SPU
	u32 GetChannelMask() const { return m_EffectInstance.GetBasis().GetDcl(); }

	int GetTemplateIndex() const { return m_EffectInstance.GetBasis().GetOrdinal(); }

	static int RegisterTechniqueGroup(const char *name) { return grcEffect::RegisterTechniqueGroup(name); }

	static int FindTechniqueGroupId(const char *name) { return grcEffect::FindTechniqueGroupId(name); }

	static void SetForcedTechniqueGroupId(int groupId);
#endif
	static int GetForcedTechniqueGroupId() { return sm_ForcedTechniqueGroupId; }

	grcEffectTechnique GetTechniqueHandle(int techGroupId,eDrawType type) const;
	bool IsTechniquePresent(int techGroupId) const;

	static int GetTechniqueGroupId();


protected:
	grcInstanceData m_EffectInstance;

	static DECLARE_MTR_THREAD int sm_MaxPasses;
	static DECLARE_MTR_THREAD s32 sm_ForcedTechniqueGroupId;
	static const char *sm_DrawBucketNames[32];

#if RAGE_INSTANCED_TECH
	static DECLARE_MTR_THREAD int sm_InstancedTechOffset;
#endif
};


inline bool grmShader::InheritsBucketId() const {
	return (m_EffectInstance.UserFlags & FLAG_INHERITS_DRAWBUCKET) != 0;
}

inline const grcInstanceData& grmShader::GetInstanceData() const {
	return m_EffectInstance;
}

inline grcInstanceData& grmShader::GetInstanceData() {
	return m_EffectInstance;
}

inline void grmShader::SetVar(grcEffectVar var,const grcTexture *v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

#if SUPPORT_UAV
inline void grmShader::SetVar(grcEffectVar var,const grcBufferUAV *v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}
inline void grmShader::SetVarUAV(grcEffectVar var,grcBufferUAV *v,int initCount) {
	m_EffectInstance.GetBasis().SetVarUAV(m_EffectInstance, var, v, initCount);
}
inline void grmShader::SetVarUAV(grcEffectVar var,const grcTextureUAV *v) {
	m_EffectInstance.GetBasis().SetVarUAV(m_EffectInstance, var, v);
}
#endif	//SUPPORT_UAV

inline void grmShader::SetVar(grcEffectVar var,int v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const IntVector &v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,float v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Vector2& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Vector3& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,Color32 v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Vector4& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Matrix34& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Matrix44& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vector2& v) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vector3& v) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vector4& v) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Matrix34& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Matrix44& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const float *v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Vector2* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Vector3* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Color32* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Vector4* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Matrix34* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Matrix44 *v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vector3* v,int count) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vector4* v,int count) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Matrix34* v,int count) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Matrix44 *v,int count) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Vec2V& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Vec3V& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Vec4V& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Vec4f& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Mat34V& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Mat44V& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vec2V& v) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vec3V& v) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vec4V& v) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vec4f& v) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Mat34V& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Mat44V& v) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v);
}

inline void grmShader::SetVar(grcEffectVar var,const Vec2V* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Vec3V* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Vec4V* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Vec4f* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Mat34V* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVar(grcEffectVar var,const Mat44V* v,int count) {
	m_EffectInstance.GetBasis().SetVar(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vec3V* v,int count) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vec4V* v,int count) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Vec4f* v,int count) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Mat34V* v,int count) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v, count);
}

inline void grmShader::SetVarByRef(grcEffectVar var,const Mat44V* v,int count) {
	m_EffectInstance.GetBasis().SetVarByRef(m_EffectInstance, var, v, count);
}

#if (__D3D11 || RSG_ORBIS)
inline grcCBuffer *grmShader::GetVarCBufferAndOffset(grcEffectVar var, int &Offset) {
	return m_EffectInstance.GetBasis().GetCBufferAndOffset(var, Offset);
}
#endif //(__D3D11 || RSG_ORBIS)

inline void grmShader::GetVar(grcEffectVar var,grcTexture*&v) const {
	m_EffectInstance.GetBasis().GetVar(m_EffectInstance, var, v);
}

inline void grmShader::GetVar(grcEffectVar var,float &v) const {
	m_EffectInstance.GetBasis().GetVar(m_EffectInstance, var, v);
}

inline void grmShader::GetVar(grcEffectVar var,Vector2& v) const {
	m_EffectInstance.GetBasis().GetVar(m_EffectInstance, var, v);
}

inline void grmShader::GetVar(grcEffectVar var,Vector3& v) const {
	m_EffectInstance.GetBasis().GetVar(m_EffectInstance, var, v);
}

inline void grmShader::GetVar(grcEffectVar var,Vector4& v) const {
	m_EffectInstance.GetBasis().GetVar(m_EffectInstance, var, v);
}

inline void grmShader::GetVar(grcEffectVar var,Matrix34& v) const {
	m_EffectInstance.GetBasis().GetVar(m_EffectInstance, var, v);
}

inline void grmShader::GetVar(grcEffectVar var,Matrix44& v) const {
	m_EffectInstance.GetBasis().GetVar(m_EffectInstance, var, v);
}

inline grcEffectVar grmShader::LookupVar(const char *name,bool mustExist /* = true */) const {
	return m_EffectInstance.GetBasis().LookupVar(name,mustExist);
}

inline grcEffectVar grmShader::LookupVarByHash(u32 hash) const {
	return m_EffectInstance.GetBasis().LookupVarByHash(hash);
}

inline bool grmShader::IsVar(grcEffectVar var) const {
	return m_EffectInstance.GetBasis().IsVar(var);
}

inline grcEffectTechnique grmShader::LookupTechnique(const char *name,bool mustExist /* = true */) const {
	return m_EffectInstance.GetBasis().LookupTechnique(name,mustExist);
}

inline int grmShader::GetTechniqueCount() const {
	return m_EffectInstance.GetBasis().GetTechniqueCount();
}

inline grcEffectTechnique grmShader::GetTechniqueByIndex(int idx) const{
	return m_EffectInstance.GetBasis().GetTechniqueByIndex(idx);
}

#if EFFECT_PRESERVE_STRINGS
inline const char *grmShader::GetTechniqueName(grcEffectTechnique tech) const{ 
	return m_EffectInstance.GetBasis().GetTechniqueName(tech);
}
#endif

#if !__SPU && EFFECT_PRESERVE_STRINGS
inline const char* grmShader::GetCurrentTechniqueName() const{ 
	return m_EffectInstance.GetBasis().GetCurrentTechniqueName();
}
#endif

inline grcEffectTechnique grmShader::GetCurrentTechnique() const{ 
	return m_EffectInstance.GetBasis().GetCurrentTechnique();
}

#if !__SPU
inline int grmShader::GetVarCount() const {
	return m_EffectInstance.GetBasis().GetVarCount();
}

inline grcEffectVar grmShader::GetVarByIndex(int idx) const {
	return m_EffectInstance.GetBasis().GetVarByIndex(idx);
}

#if EFFECT_PRESERVE_STRINGS
inline void grmShader::GetVarDesc(grcEffectVar var,const char *&name,grcEffect::VarType &type,int &annotationCount,bool &isGlobal,const char **actualName) const {
	return m_EffectInstance.GetBasis().GetVarDesc(var,name,type,annotationCount,isGlobal,actualName);
}
#endif

inline grcEffectTechnique grmShader::GetTechniqueHandle(int techGroupId,eDrawType type) const {
	return m_EffectInstance.GetBasis().GetDrawTechnique(techGroupId,(int)type);
}

inline bool grmShader::IsTechniquePresent(int techGroupId) const
{
	if(GetTechniqueHandle(techGroupId, RMC_DRAW))
	{
		return true;
	}
	if(GetTechniqueHandle(techGroupId, RMC_DRAWSKINNED))
	{
		return true;
	}
	return false;
}

#endif

#if !__SPU
inline grcSamplerStateHandle grmShader::GetSamplerState(grcEffectVar var) const
{
	return m_EffectInstance.GetBasis().GetSamplerState(m_EffectInstance, var);
}

inline void grmShader::SetSamplerState(grcEffectVar var,grcSamplerStateHandle h)
{
	m_EffectInstance.GetBasis().SetSamplerState(m_EffectInstance,var,h);
}

inline void grmShader::PushSamplerState(grcEffectVar var,grcSamplerStateHandle h)
{
	m_EffectInstance.GetBasis().PushSamplerState(m_EffectInstance,var,h);
}

inline void grmShader::PopSamplerState(grcEffectVar var)
{
	m_EffectInstance.GetBasis().PopSamplerState(m_EffectInstance,var);
}

inline int grmShader::GetPassCount(grcEffectTechnique tech) const
{
	return m_EffectInstance.GetBasis().GetPassCount(tech);
}
#endif

}	// namespace rage

#endif
