// 
// grmodel/shadersorter.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef GRMODEL_SHADERSORTER_H
#define GRMODEL_SHADERSORTER_H

#include "atl/array.h"

namespace rage {

class grmShader;
class grmModel;
class grmShaderData;

/*
	The grmShaderSorter class accumulates a list of models and shaders and renders them with minimal shader changes.

	When you call grmModel::SetShaderSorter, all subsequent grmModel::Draw calls (whether through rmcDrawable, 
	rmcLodGroup, etc) get accumulated (via the multi-parameter Draw call) in order, sorted by shader, and the 
	system will dump them out again when you call grmShaderSorter::Draw with minimal state changes.

	The intended use is when dumping the contents of a higher-level draw bucket class; you dump the shader sorter 
	once per bucket so that while local draworder may change, global draworder is preserved.

	There are two important restrictions: 
	
		First, we don't record any matrix changes, so it's only useful for things already in world space, like 
		pong levels. This restriction could be relaxed, at some cost in performance. 
		
		Second, we recycle the local variables of the first item added for all subsequent items, because currently 
		the only way to update locals is to go through a shader flush which more or less defeats the whole purpose. 
		Note that globals and instance parameters (artist-set values like textures) still operate correctly.
*/


#if __XENON
class grmGeometryQB;
typedef grmGeometryQB	grmStaticGeometry;
#elif __PPU
class grmGeometryEdge;
typedef grmGeometryEdge	grmStaticGeometry;
#else
class grmGeometry;
typedef grmGeometry		grmStaticGeometry;
#endif

#define _USE_FIRST		1 // __XENON		// Why the hell does this crash on PS3?


class grmShaderSorter 
{
	static const int MaxShaderTemplatesAmount = 256;




public:
	enum SortTypes
	{
		MATERIAL = 0,
		SHADER_AND_MATERIAL
	};


	grmShaderSorter();
	~grmShaderSorter();

	// PURPOSE:	Initialize the sorter object.  Allocates two arrays from the heap internally.
	// PARAMS:	maxShaders - Maximum number of unique shaders to support (8 bytes each)
	//			maxItems - Maximum number of pieces of geometry that will use this shader (12 bytes each)
	//			maxShaderTemplates - Maximum number of shader templates to support 
	void Init( int maxShaders,int maxItems, int maxShaderTemplates = MaxShaderTemplatesAmount);

	// PURPOSE:	Used internally by grmModel::Draw when a shader sorter is active.  Adds the specified
	//			material of the specified model to a list associated with the specified shader.
	// PARAMS:	shader - Shader to add this material to
	//			model - Model containing the material to add to the shader
	//			materialIndex - Index of the material within the model to actually render.
	//			lod - Unused.
	void Draw(const grmShader &shader,const grmModel &model,int materialIndex,int lod);

	// PURPOSE:	Used internally by grmModel::Draw when a shader sorter is active.  Adds the specified
	//			material of the specified model to a list associated with the specified shader.
	// PARAMS:	shader - Shader to add this material to
	//			model - Model containing the material to add to the shader
	//			materialIndex - Index of the material within the model to actually render.
	//			lod - Unused.
	void Draw(const grmShader& shader,const grmStaticGeometry* geometry );

	void DrawNoState(const grmShader* shader,const grmModel &model,int materialIndex,int lod);

	// PURPOSE:	Dump the accumulated shader lists and reset them to empty.
	void Draw();

	// PURPOSE:	Dump the accumulated shader lists and reset them to empty, but with no state changes (for shadows)
	void DrawNoState();

	void DrawNoStateFast();

	// Resets the shadersorter to the start of the list
	void Reset();

private:

	struct HeadData {
		const grmShader *Shader;
		u16 Last,Next;
#if _USE_FIRST
		u16 First;
#endif
	};
	struct SortData {
		const grmStaticGeometry *Model;
		u16 Next, Pad;
	};
	atArray<HeadData> m_HeadData;
	atArray<SortData> m_SortData;
	atArray<u16>	  m_ShaderToHeadLUT;
	const grmShader*  m_ForceShader;
};

}		// namespace rage

#endif	// GRMODEL_SHADERSORTER_H
