// 
// grmodel/namedbuckets.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "namedbuckets.h"

#include "atl/string.h"
#include "atl/atFixedString.h"
#include "grcore/channel.h"

#include <map>

namespace rage {

/*
PURPOSE
	Converts to and from set names for buckets. This names are given at application time allowing for flexible bucket mapping.
*/
typedef atFixedString<32>	StringType;
typedef std::map<StringType, int> LookupType;
typedef std::pair<StringType, int> LookupElementType;

static LookupType& Get()
	{
		static LookupType	bucketLookup;
		return bucketLookup;
	}

void grmNamedBuckets::Set( const char* namedString )
	{
		Clear();
		char string[ 256];
		grcAssertf( strlen( namedString) < 256, "Name longer than 256 characters: %s", namedString);
		strcpy( string, namedString );

		char* str = string;

		bool end = false;
		while( !end )
		{

			const char* start = str;
			while ( *str!= '&' && *str !=' ' && *str != '\0')
			{
				str++;
			}
			
			if ( *str == '\0')
			{
				end = true;
			}
			// copy in the end part
			if ( start != str )
			{
				*str = '\0';
				Add( start );
			}
			str++;
		};
	}

grmNamedBuckets&  grmNamedBuckets::Add( u32 val )
	{
		m_bucketID |= val;
		return *this;
	}

int grmNamedBuckets::GetNumber() const
	{
		grcAssertf( m_bucketID != 0, "Invalid bucket ID" );
		int index = 0;
		int val = m_bucketID;
		while( ( val & 0x1 ) == 0 )
		{
			val >>= 1;
			index++;
		}
		return index;
	}

grmNamedBuckets& grmNamedBuckets::Add( const char* str )
	{
		LookupType::iterator res = Get().find( str );
		if( res != Get().end() )
		{
			Add( res->second );
		}
		return *this;
	}

void grmNamedBuckets::AddBucket( int BucketNo, atString name )
	{
		grcAssertf( BucketNo >= 0 && BucketNo < 32, "BucketNo (%d) out of range [0,31]", BucketNo);
		Get().insert( std::make_pair( StringType(name), 1 << BucketNo ) );
	}

} // namespace rage
