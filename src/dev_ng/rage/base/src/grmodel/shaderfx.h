//
// grmodel/shaderfx.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef GRMODEL_SHADERFX_H
#define GRMODEL_SHADERFX_H

#include "shader.h"

namespace rage {

typedef grmShader grmShaderFx;

}

#endif	// GRMODEL_SHADERFX

