//
// curvedModel.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//


#include "curve/curve.h"

#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/indexbuffer.h"
#include "grcore/device.h"
#include "grmodel/modelfactory.h"
#include "vector/vector3.h"
#include "math/amath.h"

#include "data/struct.h"
#include "math/simplemath.h"
#include "vector/geometry.h"

#include "grcore/im.h"
#include "grmodel/setup.h"
#include "curvedModel.h"

#include "curve/curvecatrom.h"

namespace rage
{

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void preCalcCatmullRom(
					   Vec4V_InOut	vx,			// output coefficients for the x component
					   Vec4V_InOut	vy,			// output coefficients for the y component
					   Vec4V_InOut	vz,			// output coefficients for the z component
					   Vec3V_In		p1,			// point before the segments point
					   Vec3V_In		p2,			// point on the main segment
					   Vec3V_In		p3,			// point after the segment
					   Vec3V_In		p4)			// second point after the segment
{
	const ScalarV half = ScalarV(V_HALF);
	const ScalarV two = ScalarV(V_TWO);
	const ScalarV onePointFive = ScalarV(V_ONE) + half;
	const ScalarV twoPointFive = two + half;

	Vec3V r1 = p2;
	Vec3V r2 = (p3 - p1)*half;
	Vec3V r3 = p1 - twoPointFive*p2 + two*p3 - half*p4;
	Vec3V r4 = (p4 - p1)*half + (p2 - p3)*onePointFive;

	Transpose3x4to4x3(vx, vy, vz, r1, r2, r3, r4);
}

#if 0

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
Vector3 curvePointFromCurveCoeffs( 
								  Vector4& vx,	// coefficients for the x component
								  Vector4& vy,  // coefficients for the y component
								  Vector4& vz,	// coefficients for the z component
								  float t		// point on the curve to generate
								  )

{
	Vector4		tv( 1.0f, t, t* t, t * t * t );
	return Vector3( tv.Dot( vx ), tv.Dot( vy ), tv.Dot( vz ));
}

void testCurveFast( cvCurveCatRom& curve, Vector4*	CurveCoefficients, int seg, float t )
{
	Vector3 correct;
	int idx = seg * 3;
	Vector3 incorrect = curvePointFromCurveCoeffs( CurveCoefficients[ idx], CurveCoefficients[ idx + 1],	CurveCoefficients[ idx + 2], t );
	curve.SolveSegment( correct, seg, t );

	grcAssertf( correct == incorrect );
}
//
// PURPOSE
//  Tests that the fast curve generator code gets the same results as the rage curve classes.
//
void testFastCurveGen()
{

	Vector4	CurveCoefficients[ MAX_CURVE_POINTS * 3 ];
	SpiralVertices		testVerts( 4.0f, 6.0f, 8.0f, 15 );
	FastCurveGen<SpiralVertices> curveFast( testVerts );	

	int cnt = 0;
	while( !curveFast.IsFinished())
	{
		curveFast.CalculateNextSegmentCoefficients( CurveCoefficients[ cnt ] , 
			CurveCoefficients[ cnt + 1 ], 
			CurveCoefficients[ cnt   + 2 ]);
		cnt+=3;
	}
	int numVerts = testVerts.NumberVertices() + 1;
	cvCurveCatRom	curve;
	curve.AllocateVertices( numVerts );
	curve.SetNumVertices( numVerts );
	curve.SetLooping( false );

	for ( int i = 0; i < numVerts; i++ )
	{
		Vector3& v = curve.GetVertex( i);
		v = testVerts(i );
	}
	curve.PostInit();

	// now they should be the same
	testCurveFast( curve, CurveCoefficients, 4, 0.5f );
	testCurveFast( curve, CurveCoefficients, 1, 0.1f );
	testCurveFast( curve, CurveCoefficients, 9, 0.9f );
	testCurveFast( curve, CurveCoefficients, 0, 0.0f );
}

#endif // 0



///////////////////////////////////////////////////////////////
TubeModel::TubeModel( 
					 int numPoints,		//  number of segments along it's length
					 int NumSegments,		// number of segments along it's width
					 bool	cap,				// wheter to cap the tube at the ends
					 Vector2 uvScale
					 )
{

	//  Set up a vertex format.
	grcFvf fvf;
	fvf.SetPosChannel(true);			// position
	fvf.SetNormalChannel( true );		// normal
	fvf.SetTextureChannel( 0, true, grcFvf::grcdsFloat3 );  // tangent
	fvf.SetTextureChannel( 1, true, grcFvf::grcdsFloat2 );  // UV

	m_VertexDecl = grmModelFactory::BuildDeclarator(&fvf, NULL, NULL, NULL);

	// Create an index buffer and fill it with data as well.
	createIndices(  numPoints,  NumSegments );

	//  Create a vertex buffer and fill it with some interesting data
	createVertices( fvf, numPoints,  NumSegments , cap, uvScale);
}
//
//
TubeModel::~TubeModel()
{
	delete m_IndexBuffer;
	delete m_VertexBuffer;
	grmModelFactory::FreeDeclarator( m_VertexDecl);
}
//
//
void TubeModel::createIndices(  int maxPoints, int numSegments )
{
	numSegments++;  // add one extra for the uvs
	int NumberOfIndices =( maxPoints  ) * ( 2 * numSegments + 4 )  -  2 ;

	// DX11 TODO: Should have a CreateWithData function instead of our lame lock sharing
#if !__D3D11
	m_IndexBuffer = grcIndexBuffer::Create(NumberOfIndices);
#else //!__D3D11
	m_IndexBuffer = grcIndexBuffer::Create(NumberOfIndices, false);
#endif //!__D3D11
	u16 *lockPtr = m_IndexBuffer->LockRW();
	int offset = 0;

	for (int i=0; i< maxPoints ; i++) 
	{
		for (int j=0; j< numSegments ; j++) 
		{
			// tri strip
			Assign(lockPtr[offset++], ( i + 1) *numSegments + j);
			Assign(lockPtr[offset++], i *numSegments + j);

		}
		Assign(lockPtr[offset++], ( i + 1)  *numSegments );
		Assign(lockPtr[offset++], i *numSegments );

		if( i != maxPoints - 2 )
		{
			Assign(lockPtr[offset++], ( i + 1 )*numSegments );
			Assign(lockPtr[offset++], ( i + 1)  *numSegments );
		}
	}

	grcAssertf( NumberOfIndices == offset, "Index count mismatch - should have been %d, was %d", NumberOfIndices, offset );
	m_IndexBuffer->UnlockRW();
}
//
//
void TubeModel::createVertices( grcFvf& fvf, int maxPoints, int numSegments , bool cap, Vector2 uvScale )
{
	numSegments++;
	int NumberOfVertices = ( maxPoints + 1 )* numSegments;

	// DX11 TODO: Should have a CreateWithData function instead of our lame lock sharing
	const bool bReadWrite = true;
	const bool bDynamic = !__RESOURCECOMPILER && __PS3;
	m_VertexBuffer = grcVertexBuffer::Create( NumberOfVertices, fvf, bReadWrite, bDynamic, NULL );
	{
		grcVertexBufferEditor editor(m_VertexBuffer);

		int idx = 0;
		float angleRange = 1.0f/(float)(numSegments -1);
		for (int i=0; i< maxPoints + 1; i++)
		{
			float height = (float) i / (float) maxPoints;

			for ( int j = 0; j < numSegments; j++ )
			{
				// get offset
				float angle = (float)j *angleRange;
				float theta = ( angle ) * 2.0f * PI;
				Vector3 position = Vector3( cos( theta )  ,  height , sin( theta) );
				Vector3 normal = Vector3( cos( theta )  ,  0.0f , sin( theta) );
				Vector3 tangent = Vector3( sin( theta), 0.0f, cos( theta ));

				if ( cap &&  i == ( maxPoints ) )
				{
					position = Vector3( 0.0f ,  height , 0.0f );
					normal  = Vector3( 0.0f, 1.0f, 0.0f );
				}

				editor.SetPosition( idx, position );
				editor.SetNormal( idx, normal );

				editor.SetUV( idx, 0, tangent );
				Vector2 uvs = Vector2( angle, height ) * uvScale;

				editor.SetUV( idx++, 1,uvs);

			}
		}
	}

	m_VertexBuffer->MakeReadOnly();
}

void TubeModel::render( grmShader *shader, grcEffectTechnique technique, int pass )
{
	
	int passCount = shader->BeginDraw(grmShader::RMC_DRAW,true, technique);

	if( passCount )
	{
		shader->Bind(Min(pass,passCount-1)); // Naaastyyy, but at least no one has to worry...
		GRCDEVICE.DrawIndexedPrimitive(drawTriStrip,m_VertexDecl,*m_VertexBuffer,*m_IndexBuffer,m_IndexBuffer->GetIndexCount());
		shader->UnBind();
	}
	shader->EndDraw();
}

void TubeModel::renderNoShaders()
{
	GRCDEVICE.DrawIndexedPrimitive(drawTriStrip,m_VertexDecl,*m_VertexBuffer,*m_IndexBuffer,m_IndexBuffer->GetIndexCount());
}


};// namespace rage
