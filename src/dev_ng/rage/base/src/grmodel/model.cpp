//
// grmodel/model.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

/*
Optimization notes; things I tried on 10 Apr 2009 that were ultimately a wash

Avoid virtual call overhead on 360:
#define grmGeometry_Invoke(rGeom,body) ((grmGeometryQB*)&(rGeom))->grmGeometryQB::Body
Similar to Raymond's grmGeometryStatic stuff in shadersorter.

Attempted to eliminate cull callback in favor of hard-coding full test at first
and vis-only test if not trivial accept.  (Also gets us in line more with PS3 SPU)

Don't bother doing restoreState logic, and keep the loop logic simpler?

Forced shader should really be dropped in favor of existing DrawNoShaders methods.

Drop geometry::Draw(int) version; make caller call GetGeometry() themselves to avoid
cluttering up public interface and avoiding a small middle function.

Prefetching?  Also looked into shrinking grmGeometryQB down to just a raw VB and IB.
The problem is that even the 300k cycles it claimed it was costing me is still only
0.1ms for a lot of complexity.

Eliminate InheritsBucketId and a lot of forcedBucket logic?
*/


#include "model.h"
#include "geometry.h"
#include "matrixset.h"
#include "modelinfo.h"
#include "modelfactory.h"
#include "shader.h"
#include "shadergroup.h"
#include "shadersorter.h"

#include "data/resource.h"
#include "diag/tracker.h"
#include "file/asset.h"			// sigh
#include "file/device.h"
#include "grcore/channel.h"
#include "grcore/effect.h"
#include "grcore/channel.h"
#include "grcore/instancebuffer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "system/alloca.h"
#include "system/cache.h"
#include "system/memory.h"
#include "system/param.h"
#include "vector/geometry.h"
#include "math/float16.h"

#include "grcore/effect_config.h"
#if GRCORE_ON_SPU > 1
#include "grcore/wrapper_gcm.h"
#include "grmodel/grmodelspu.h"
#endif

#if __WIN32
#include "grcore/config.h"
#include "grcore/device.h"
#include "system/xtl.h"
#include "system/d3d9.h"
#endif
#include "grcore/viewport.h"
#include "grcore/viewport_inline.h"
#include "shaderlib/skinning_method_config.h"

#if __RESOURCECOMPILER
#include "fragment/type.h"
#endif

#define RAGE_ENABLE_OFFSET
#define	PATCH_DISPLACEMENT		1

using namespace rage;

int grmModel::sm_MemoryBucket = 2;
bool grmModel::sm_EdgeGeomSkinned = false;
bool grmModel::sm_EdgeGeomUnskinned = false;

int rage::grmStripLength, rage::grmStripCount;

// statics:
__THREAD grmShader*								grmModel::sm_ForceShader			= NULL;
DECLARE_MTR_THREAD grmModel::ModelCullbackType	grmModel::sm_Cullback				= ModelVisibleWithAABB;
grmModel::MeshPrepareHook			grmModel::sm_MeshPrepareHook;
grmModel::MeshLoaderHookParams		grmModel::sm_MeshLoaderHookFunctor;
grmModel::PreModelHookParams		grmModel::sm_PreModelCreateHookFunctor;
grmModel::PreModelHookParams2		grmModel::sm_PreModelCreateHookFunctor2;
grmModel::PreModelHookParams3		grmModel::sm_PreModelCreateHookFunctor3;
grmModel::GeometryParamsHook		grmModel::sm_GeometryParamsHookFunctor;

#if __BANK
DECLARE_MTR_THREAD grmModel::CustomShaderParamsFuncType grmModel::sm_CustomShaderParamsFunc = NULL;
#endif // __BANK

#if !__FINAL
PARAM(rscmodel,"[grmodel] Sets size of resource model heap; parameter is heap size in kilobytes, default is 512.");
PARAM(regenmodel,"[grmodel] Force model resources to be regenerated (default is to perform an out-of-date check)");
PARAM(edgegeom,"[grmodel] Use Edge geometry on PS3 (both by default, or =skinned or =unskinned)");
#endif

grmModel::grmModel() : m_AABBs(0), m_ShaderIndex(0) , m_MatrixCount(0), m_Flags(0), m_MatrixIndex(0), m_Mask(0xFF), m_SkinFlagAndTessellatedGeometryCount(0), m_Count(0) { 
}


grmModel::~grmModel() {
	delete[] m_ShaderIndex;

	for (int i=0; i<m_Geometries.GetCount(); i++)
		delete m_Geometries[i];

	// grcAssertf(!IsResourced() && "use Delete, not delete");
	delete[] (char*)m_AABBs;
}
 
void grmModel::CloneWithNewVertexData( grmModel &cloneme, const int *geomIdx, int nGeom, bool createVB, void* preAllocatedMemory, int& memoryAvailable)
{
	int count = m_Count = cloneme.m_Count;

	m_ShaderIndex = rage_contained_new u16[count];
	memcpy( m_ShaderIndex, cloneme.m_ShaderIndex, sizeof(u16)* count );

	m_MatrixCount = cloneme.m_MatrixCount;
	m_Flags = cloneme.m_Flags;
	m_Type = cloneme.m_Type;
	m_MatrixIndex = cloneme.m_MatrixIndex;
	m_Mask = cloneme.m_Mask;
	m_SkinFlagAndTessellatedGeometryCount = cloneme.m_SkinFlagAndTessellatedGeometryCount;
	m_AABBs = NULL;		// has the nice side effect of disabling model culling

	m_Geometries.Resize(count);
	int iGeom;
	for( iGeom = 0; iGeom < nGeom; iGeom++ )
	{
		m_Geometries[geomIdx[iGeom]] = cloneme.m_Geometries[geomIdx[iGeom]]->CloneWithNewVertexData(createVB, preAllocatedMemory, memoryAvailable);
	}
}

#if MESH_LIBRARY
void grmModel::ResetBoundingBox() {
	sm_ModelMin.Set(FLT_MAX,FLT_MAX,FLT_MAX);
	sm_ModelMax.Set(-FLT_MAX,-FLT_MAX,-FLT_MAX);
}
#endif

// PARAM(vertexopt,"Enable vertex cache optimization at load time.");

#if MESH_LIBRARY
int grmModel::sm_PacketLimit = 256*1024;

int grmModel::Packetize(const mshMesh &mesh,const mshMaterial &mtl,void *closure) {
	const grmShaderGroup *pShaderGroup = (const grmShaderGroup*) closure;

	const char *name = mtl.Name;
	if (name[0] == '#')
		++name;
	int mtlIdx = atoi(name);
	u32 mask = pShaderGroup->GetChannelMask(mtlIdx);

	grmVtxStreamConfigurator *vc = grmModelFactory::GetVertexConfigurator();
	int vsize = 0;

	if (mask & grcFvf::grcfcPositionMask)
		vsize += mesh.IsSkinned()? 12+8 : 12;
	if (mask & grcFvf::grcfcNormalMask)
		vsize += vc->AreNormalsPacked(0)? 4 : 12;
	if (mask & grcFvf::grcfcDiffuseMask)
		vsize += 4;
	if (mask & grcFvf::grcfcSpecularMask)
		vsize += 4;

	// I thought at one point if we had tangents but not binormals we used a Half4 on PS3 for tangents to save an attribute.
	// But I can't find that code now!  But this logic might need to be improved...
	/// if (g_sysPlatform == platform::PS3 && (mask & (grcFvf::grcfcTangent0 | grcFvf::grcfcBinormal0)) == grcFvf::grcfcTangent0

	if (mask & grcFvf::grcfcTangent0Mask)
		vsize += vc->AreNormalsPacked(0)? 4 : 12;
	if (mask & grcFvf::grcfcTangent1Mask)
		vsize += vc->AreNormalsPacked(0)? 4 : 12;
	if (mask & grcFvf::grcfcBinormal0Mask)
		vsize += vc->AreNormalsPacked(0)? 4 : 12;
	if (mask & grcFvf::grcfcBinormal1Mask)
		vsize += vc->AreNormalsPacked(0)? 4 : 12;

	// Normals are not stored packed on PC since hardware doesn't guarantee it, so we may need a float4 instead of a float3.
	if ((g_sysPlatform == platform::WIN32PC || g_sysPlatform == platform::WIN64PC || g_sysPlatform == platform::DURANGO || g_sysPlatform == platform::ORBIS) && (mask &  grcFvf::grcfcTangent0Mask) && !(mask & grcFvf::grcfcBinormal0Mask))
		vsize += 4;

	for (int i=0; i<8; i++)
		if (mask & (grcFvf::grcfcTexture0Mask << i))
			vsize += vc->AreTexCoordsPacked(i)? 4 : 8;
	
	grcDebugf1("mtl [%s] idx %d  name %s mask %x size %d",mtl.Name.c_str(),mtlIdx,pShaderGroup->GetShader(mtlIdx).GetName(),mask,vsize);
	
	int packetLimit = sm_PacketLimit;	//256K default
	// Do not split displacement-mapped objects (see B#1749832)
	if (grmShaderGroup::IsDisplacementMaterial(name, pShaderGroup))
		packetLimit = 2<<20;	//2Mb

	// Fit into chunks
	return packetLimit / vsize;
}


void grmModel::Prepare(mshMesh &mesh,const grmShaderGroup *pShaderGroup, bool optimize ) 
{
	if (optimize || __RESOURCECOMPILER)
	{ 
#if __RESOURCECOMPILER
		for (int i=0; i<mesh.GetMtlCount(); i++) {
			mshMaterial &mtl = mesh.GetMtl(i);
			const char *name = mtl.Name;
			if (name[0] == '#')
				++name;
			int mtlIdx = atoi(name);
			u32 mask = pShaderGroup->GetChannelMask(mtlIdx);
			int vc = mtl.GetVertexCount();

			grmMeshPrepareParam optimizeMeshParam = grcMeshPrepareDefault; 
			if (sm_MeshPrepareHook.IsValid())
			{
				optimizeMeshParam = sm_MeshPrepareHook(mesh, pShaderGroup, i);
			}

			switch (optimizeMeshParam)
			{
				case grcMeshPrepareDefault:
					if (!optimize)
						continue;
					break;

				case grcMeshPrepareDontOptimize:
					continue;

				case grcMeshPrepareOptimize:
					break;

				default:
					Quitf("Bad enum");
					continue;
			}

			// Clear channels we won't need so that welding works better.
			if (!(mask & grcFvf::grcfcNormalMask))
				for (int j=0; j<vc; j++)
					mtl.SetNormal(j,Vector3(0,0,1));

			if (!(mask & grcFvf::grcfcDiffuseMask))
				for (int j=0; j<vc; j++)
					mtl.SetCpv(j,Vector4(1,1,1,1));

			if (!(mask & grcFvf::grcfcSpecularMask))
				for (int j=0; j<vc; j++)
					mtl.SetCpv2(j,Vector4(1,1,1,1));

			for (int j=0; j<vc; j++)
				mtl.SetCpv3(j,Vector4(1,1,1,1));

			for (int k=0; k<mshMaxTanBiSets; k++) 
			{
				if (!(mask & (grcFvf::grcfcTangent0Mask << k)) && !(mask & (grcFvf::grcfcBinormal0Mask << k)))
				{
					for (int j=0; j<vc; j++)
						mtl.SetTangent(j,k,Vector3(0,0,1));

					for (int j=0; j<vc; j++)
						mtl.SetBinormal(j,k,Vector3(0,0,1));
				}
			}

			for (int k=0; k<mshMaxTexCoordSets; k++) {
				if (!(mask & (grcFvf::grcfcTexture0Mask << k)))
					for (int j=0; j<vc; j++)
						mtl.SetTexCoord(j,k,Vector2(0,0));
			}

			// Weld the result.
			mtl.WeldWithoutQuantize();
		}
#endif

		mesh.Triangulate();
		mesh.CacheOptimize();
		mesh.ReorderVertices();
	}

	// I think we want to packetize after optimization so that newly introduced locality is preserved.
	mesh.Packetize(grmModel::Packetize,(void*)pShaderGroup,SKINNING_COUNT_FOR_PLATFORM);
}


#if HACK_GTA4 && __RESOURCECOMPILER
//
//
// custom step to quickly detect obsolete sps materials still in use by assets:
//
bool CheckSpsMaterialName(const char* UNUSED_PARAM(basename), mshMesh& mesh, const grmShaderGroup *pShaderGroup)
{
	const u32 mtlCount = mesh.GetMtlCount();
	for(u32 i=0; i<mtlCount; i++)
	{
		u16 shaderIndex=(u16)-1;
		if (mesh.GetMtl(i).Name[0] == '#')
			shaderIndex = (u16) atoi(mesh.GetMtl(i).Name + 1);
		else
			shaderIndex = (u16) atoi(mesh.GetMtl(i).Name);

		const char* mtlfilename = pShaderGroup->GetShader(shaderIndex).GetMtlFileName();
		//const char* fxfilename= pShaderGroup->GetShader(shaderIndex).GetName();

		// using obsolete gta_*.sps?
		if( mtlfilename && (strstr(mtlfilename,"gta_")==mtlfilename) )
		{
			Warningf("Obsolete sps shader material detected: %s. Please change it to non-gta version.", mtlfilename);
		}
	}

	return(true);
}
#endif // HACK_GTA4 && __RESOURCECOMPILER...


#if __RESOURCECOMPILER
void ExtractSkinnedMatrices(atArray<int>& skinnedMatrices, mshMesh& mesh)
{
	int count = mesh.GetMtlCount();
	for (int i = 0; i < count; ++i)
	{
		int numVerts = mesh.GetMtl(i).GetVertexCount();
		for (int v = 0; v < numVerts; ++v)
		{
			const mshBinding& binding = mesh.GetMtl(i).GetBinding(v);
			//if (binding.IsPassThrough)
			//	continue;

			for (int m = 0; m < binding.Mtx.GetCount(); ++m)
			{
				if (binding.Wgt[m])
				{
					bool alreadyAdded = false;
					for (int b = 0; b < skinnedMatrices.GetCount(); ++b)
					{
						if (skinnedMatrices[b] == binding.Mtx[m])
						{
							alreadyAdded = true;
							break;
						}
					}

					if (!alreadyAdded)
					{
						sysMemStartTemp();
						skinnedMatrices.PushAndGrow(binding.Mtx[m]);
						sysMemEndTemp();
					}
				}
			}
		}
	}
}

void UpdateSkinnedMatrices(atArray<int>& skinnedMatrices, mshMesh& mesh)
{
	int count = mesh.GetMtlCount();
	for (int i = 0; i < count; ++i)
	{
		int numVerts = mesh.GetMtl(i).GetVertexCount();
		for (int v = 0; v < numVerts; ++v)
		{
			const mshBinding& binding = mesh.GetMtl(i).GetBinding(v);
			//if (binding.IsPassThrough)
			//	continue;

			for (int m = 0; m < binding.Mtx.GetCount(); ++m)
			{
				if (binding.Wgt[m])
				{
					for (int b = 0; b < skinnedMatrices.GetCount(); ++b)
					{
						if (skinnedMatrices[b] == binding.Mtx[m])
						{
							mshBinding* pBinding = (mshBinding*)&binding;
							pBinding->Mtx[m] = b;
							break;
						}
					}
				}
			}
		}
	}
}

int SortSkinnedMatrices(const int* a, const int* b)
{
	return *a - *b;
}
#endif // __RESOURCECOMPILER

grmModel* grmModel::Create(fiTokenizer &t,const char *basename,int mtxIndex,grmModelInfo *outInfo, const grmShaderGroup *pShaderGroup, u32 channelMask, bool optimize, int extraVerts, u8 mask, int lod) {
	grmModel *model = 0;

	sysMemUseMemoryBucket MODELS (sm_MemoryBucket);

	sysMemStartTemp();
	mshMesh &mesh = *(rage_new mshMesh);
	bool success = SerializeFromFile(basename,mesh,"mesh");

	if (success)
		Prepare( mesh, pShaderGroup, optimize );

#if HACK_GTA4 && __RESOURCECOMPILER
	if (success)
		CheckSpsMaterialName(basename, mesh, pShaderGroup);
#endif

	// END TEMPORARY BLOCK
	sysMemEndTemp();

	if (success)
	{
		if(		(!sm_PreModelCreateHookFunctor.IsValid() || sm_PreModelCreateHookFunctor(mesh, basename, lod,pShaderGroup))
			&&	(!sm_PreModelCreateHookFunctor2.IsValid() || sm_PreModelCreateHookFunctor2(mesh, t, pShaderGroup))
			)
		{
#if __RESOURCECOMPILER
			if (mesh.IsSkinned() && fragType::ms_pSkinnedBones)
			{
				ExtractSkinnedMatrices(*fragType::ms_pSkinnedBones, mesh);

				if (fragType::ms_pSkinnedBones->GetCount() > 1)
				{
					if (fragType::ms_canSortBones)
					{
						fragType::ms_pSkinnedBones->QSort(0, -1, SortSkinnedMatrices);
						UpdateSkinnedMatrices(*fragType::ms_pSkinnedBones, mesh);
						if (sm_PreModelCreateHookFunctor3.IsValid())
							sm_PreModelCreateHookFunctor3(fragType::ms_pSkinnedBones);
					}
					else
						Errorf("%s: Can't sort skinned bone array for lodded skeleton! Do we have several meshes skinned to several bones?", basename);
				}
				else if (fragType::ms_pSkinnedBones->GetCount() == 1 && (*fragType::ms_pSkinnedBones)[0] != 0)
				{
					Errorf("%s: skinned bone array contains one entry only but it isn't 0 as expected!", basename);
				}
			}
#endif
			model = Create(mesh, mtxIndex, outInfo, pShaderGroup, channelMask, extraVerts, mask);
		}
	}

	sysMemStartTemp();
	if (!sm_MeshLoaderHookFunctor.IsValid() || sm_MeshLoaderHookFunctor(mesh, basename))
		delete &mesh;
	sysMemEndTemp();

	return model;
}


grmModel* grmModel::Create(const mshMesh &mesh,int mtxIndex,grmModelInfo *outInfo, const grmShaderGroup *pShaderGroup, u32 channelMask, int extraVerts, u8 mask ) {
	grmModel *model = 0;

	model = rage_contained_new grmModel;
	if (!model->Load(mesh,outInfo, pShaderGroup, channelMask,extraVerts, mask)) {
		delete model;
		return NULL;
	}

	if (model)
		model->SetMatrixIndex(mtxIndex);

	return model;
}
#endif		// MESH_LIBRARY

/* This code is utter crap.  grmGeometry::SwapBuffers is static!
void grmModel::SwapBuffers()
{
	for( int i = 0; i < m_Count; i++ )
	{
		m_Geometries[i]->SwapBuffers();
	}
}
*/

grmGeometry& grmModel::GetGeometry(int i) const
{
	return *m_Geometries[i];
}

const grcFvf* grmModel::GetFvf(int i) const
{
	return m_Geometries[i]->GetFvf();
}


PARAM(nocpv,"[grmodel] Disable loading instance cpv files in grmodel");


int grmModel::sm_PositionFracBits;

static char s_CpvPath[64];

void grmSetCpvPath(const char *dir) {
	if (dir)
		safecpy(s_CpvPath, dir, sizeof(s_CpvPath));
	else
		s_CpvPath[0] = 0;
}

char* grmGetCpvPath() { return s_CpvPath; }

fiStream* __default_open_cpv_file(const char *basename) {
	char cpvfile[256];
	strcpy(cpvfile,basename);
	*strrchr(cpvfile,'.') = 0;
	if (s_CpvPath[0])
		ASSET.PushFolder(s_CpvPath);
	fiStream *S = ASSET.Open(cpvfile,"cpv",true /*no error spew since missing files are common*/);
	if (s_CpvPath[0])
		ASSET.PopFolder();
	return S;
}

#if MESH_LIBRARY

void grmModel::PatchBillboard(unsigned index, mshMesh &mesh, const grmShaderGroup *pShaderGroup)
{
	mshMaterial& mtl = mesh.GetMtl(index);

	Vector4 shaderVarScale(1.0f,1.0f,1.0f,1.0f);
	grmShader *pShader = pShaderGroup->GetShaderPtr(m_ShaderIndex[index]);
	if(pShader)
	{
		grcEffectVar dimVar = pShader->LookupVar("treeLod2Params");
		pShader->GetVar(dimVar, shaderVarScale);
		//Printf("\n treeLod2Params: shaderVarScale=(%.2f, %.2f)", shaderVarScale.x, shaderVarScale.y);
	}

	if((mtl.GetTriangleCount()%2)!=0)
	{
		Warningf("Billboard material (%s)'s mesh has uneven triangle count: %d.", (const char*)mtl.Name, mtl.GetTriangleCount());
	}
	
	const int aabbIdx = index + (m_Count>1);
	m_AABBs[aabbIdx].Invalidate();
	
	mshMaterial::TriangleIterator triIt = mtl.BeginTriangles();

	// special case:
	// if prev mesh (sharing the same shader) had uneven amount of tris, then skip 1st tri so proper pairing of BBs is maintained:
	if((index>0) && (m_ShaderIndex[index-1] == m_ShaderIndex[index]))
	{
		const mshMaterial& prevMtl = mesh.GetMtl(index-1);
		if((prevMtl.GetTriangleCount()%2)!=0)
		{
			++triIt;
		}
	}

	while(triIt != mtl.EndTriangles())
	{
		Vector3 centre(0.0f,0.0f,0.0f);
		Vector2 modelScale(1.0f,1.0f);	// local BB scale
		bool bBBprocess = true;

		for(int j=0; j<2; j++)
		{
			if(triIt == mtl.EndTriangles())
			{	// unpaired triangle in this mesh - well, so skip it:
				bBBprocess = false;
				break;
			}

			int vertindex[3];
			triIt.GetVertIndices(vertindex[0],vertindex[1],vertindex[2]);
			for(int v=0; v<3; v++)
			{
				const mshVertex& vert = mtl.GetVertex(vertindex[v]);
				centre += vert.Pos;
				
				if(vert.TexCount > 2)
				{
					modelScale = vert.Tex[2];
					modelScale.y = 1.0f - modelScale.y; // UV Max conversion
					//Printf("\n vert%d (%d): UV=(%.2f, %.2f)", v, vertindex[v], modelScale.x, modelScale.y);
				}
			}
			
			++triIt;
		}
		
		if(bBBprocess)
		{
			centre /= 6;
			//Printf("\n centre=(%2.f, %.2f, %.2f)", centre.x, centre.y, centre.z);
			
			const Vector2 triScale(shaderVarScale.x*modelScale.x, shaderVarScale.y*modelScale.y);
			const float halfDiaglength = sqrt(triScale.x*triScale.x + triScale.y*triScale.y);
			const Vector3 offset(halfDiaglength,halfDiaglength,halfDiaglength);

			//Printf("\n offsset=(%2.f, %.2f, %.2f)", offset.x, offset.y, offset.z);
			m_AABBs[aabbIdx].GrowPoint(VECTOR3_TO_VEC3V(centre - offset));
			m_AABBs[aabbIdx].GrowPoint(VECTOR3_TO_VEC3V(centre + offset));
		}
	}

	if(!m_AABBs[aabbIdx].IsValid())
	{
		m_AABBs[aabbIdx].SetMin(Vec3V(V_ZERO));
		m_AABBs[aabbIdx].SetMax(Vec3V(V_ONE));
	}

	if (m_Count > 1)
	{
		m_AABBs[0].GrowAABB(m_AABBs[aabbIdx]);	// update model's AABB
	}

	//Printf("\n m_AABBs[index].min=(%.2f, %.2f, %2.f)", m_AABBs[aabbIdx].GetMin().GetXf(), m_AABBs[aabbIdx].GetMin().GetYf(), m_AABBs[aabbIdx].GetMin().GetZf());
	//Printf("\n m_AABBs[index].max=(%.2f, %.2f, %2.f)", m_AABBs[aabbIdx].GetMax().GetXf(), m_AABBs[aabbIdx].GetMax().GetYf(), m_AABBs[aabbIdx].GetMax().GetZf());
}

#if PATCH_DISPLACEMENT
enum { NUM_TRACKED_UVS = 3, NUM_TRACKED_TRIANGLES = 8 };

struct DpmData	{
	Vector3 pos;
	Vector3 normal;
	float totalArea;
	unsigned numUniqueUVs;
	Vector2 uvs[NUM_TRACKED_UVS];
	bool meshBorder;
	int numTriangles;
	unsigned triangleId[NUM_TRACKED_TRIANGLES];
};

static int dpm_compare(const void *d0, const void *d1)
{
	const Vector3 &p0 = reinterpret_cast<const DpmData*>(d0)->pos;
	const Vector3 &p1 = reinterpret_cast<const DpmData*>(d1)->pos;
	return
		p0.x != p1.x ? (p0.x > p1.x ? 1 : -1) :
		p0.y != p1.y ? (p0.y > p1.y ? 1 : -1) :
		p0.z != p1.z ? (p0.z > p1.z ? 1 : -1) :
		0;
}

u32 grmModel::PatchDisplacement(mshMaterial& mtl)
{
	const u32 basetexChannel = 0, dominantTexChannel = 4, areaTexChannel = 5;

	sysMemStartTemp();
	{
		mshArray<DpmData> data( mtl.GetVertexCount() );
		
		//step-1: gather positions
		for (int i=0; i<mtl.GetVertexCount(); ++i)
		{
			DpmData key;
			memset(&key, 0, sizeof(key));
			key.pos = mtl.GetPos(i);
			data.Append() = key;
			data[i] = key;
		}

		//step-2: sort
		qsort( &data[0], mtl.GetVertexCount(), sizeof(DpmData), dpm_compare );

		//step-3: remove duplicates
		int newLength = 1;
		for (int i=1; i<mtl.GetVertexCount(); ++i)
		{
			if (dpm_compare(&data[i-1],&data[i]))
			{
				if (i != newLength)
				{
					data[newLength] = data[i];
				}
				++newLength;
			}
		}
		data.Resize(newLength);

		//step-4: fill up the temporary unique vertex data
		mshMaterial::TriangleIterator triIt;
		for (triIt = mtl.BeginTriangles(); triIt!=mtl.EndTriangles(); ++triIt)
		{
			int index[3];
			triIt.GetVertIndices(index[0], index[1], index[2]);
			Vector2 tA = mtl.GetTexCoord( index[0], basetexChannel );
			Vector2 tB = mtl.GetTexCoord( index[1], basetexChannel );
			Vector2 tC = mtl.GetTexCoord( index[2], basetexChannel );
			float cross = Cross( tB - tA, tC - tA );
			float area = sqrtf(abs( cross ));

			for (unsigned j=0; j<3; ++j)
			{
				const int i = index[j];
				DpmData key;
				memset(&key, 0, sizeof(key));
				key.pos = mtl.GetPos(i);
				
				DpmData *const match = reinterpret_cast<DpmData*>( bsearch( &key, &data[0], newLength, sizeof(DpmData), dpm_compare ));
				if (!match)
				{
					grcErrorf("PatchDisplacement[4]: qsort/bsearch failed on element %d", i);
					continue;
				}

				// 5a: update unique UV
				const Vector2 &uv = mtl.GetTexCoord( i, basetexChannel );
				unsigned k=0;
				while(k<match->numUniqueUVs && match->uvs[k]!=uv)
					k++;
				if (k<NUM_TRACKED_UVS && k==match->numUniqueUVs)
				{
					match->uvs[k] = uv;
					match->numUniqueUVs += 1;
				}

				// 5b: update triangle indices
				if (match->numTriangles < NUM_TRACKED_TRIANGLES)
				{
					match->triangleId[match->numTriangles] = 1+triIt.m_CurrTri;
				}

				match->normal += mtl.GetNormal(i);
				match->totalArea += area;
				match->numTriangles += 1;
			}
		}

		// step 5: detect mesh borders
		unsigned totalNumBorders = 0;
		for (triIt = mtl.BeginTriangles(); triIt!=mtl.EndTriangles(); ++triIt)
		{
			int index[3];
			triIt.GetVertIndices(index[0], index[1], index[2]);
			DpmData *matches[3] = {NULL};
			DpmData key;
			memset(&key, 0, sizeof(key));
			
			for (unsigned j=0; j<3; ++j)
			{
				key.pos = mtl.GetPos(index[j]);
				matches[j] = reinterpret_cast<DpmData*>( bsearch( &key, &data[0], newLength, sizeof(DpmData), dpm_compare ));
			}
			
			for (unsigned ja=0; ja<3; ++ja)
			{
				unsigned numSharedTriangles = 0;
				const unsigned jb = ja<2 ? (ja+1) : 0;
				const bool skipA = matches[ja]->meshBorder || matches[ja]->numTriangles+1>=NUM_TRACKED_TRIANGLES;
				const bool skipB = matches[jb]->meshBorder || matches[jb]->numTriangles+1>=NUM_TRACKED_TRIANGLES;
				
				if (skipA || skipB)
				{
					// vertices are either marked already or can not be marked due to insufficient data
					continue;
				}
				
				// scanning through both triangle ID lists simultaneously, exploiting the fact both are guarenteed to be sorted
				unsigned ka=0, kb=0;
				for(; ka<NUM_TRACKED_TRIANGLES && matches[ja]->triangleId[ka]; ++ka)
				{
					while(kb<NUM_TRACKED_TRIANGLES && matches[jb]->triangleId[kb] && matches[jb]->triangleId[kb] < matches[ja]->triangleId[ka])
						kb += 1;
					if (kb<NUM_TRACKED_TRIANGLES && matches[jb]->triangleId[kb] == matches[ja]->triangleId[ka])
					{
						// found a triangle with these two vertices
						numSharedTriangles += 1;
					}
				}
				
				if (numSharedTriangles < 2)
				{
					// edge is open from one side
					//if (!skipA)
					totalNumBorders += 1;
					matches[ja]->meshBorder = true;
					//if (!skipB)
					totalNumBorders += 1;
					matches[jb]->meshBorder = true;
				}
			}
		}

		grcDisplayf("PatchDisplacement[5]: marked %d border vertices", totalNumBorders);
		
		//step-8: average vertex data by positions
		for (int i=0; i<data.GetCount(); ++i)
		{
			const float kf = 1.f / data[i].numTriangles;
			data[i].normal *= kf;
			data[i].normal.Normalize();
			data[i].totalArea *= kf;
		}

		//step-7: fill up the actual vertex data
		for (int i=0; i<mtl.GetVertexCount(); ++i)
		{
			DpmData key;
			memset(&key, 0, sizeof(key));
			key.pos = mtl.GetPos(i);
			
			DpmData *const match = reinterpret_cast<DpmData*>( bsearch( &key, &data[0], newLength, sizeof(DpmData), dpm_compare ));
			if (!match)
			{
				grcErrorf("PatchDisplacement[6]: qsort/bsearch failed on element %d", i);
				continue;
			}
			if (!match->numTriangles)
			{
				grcWarningf("PatchDisplacement[6]: isolated vertex detected (%d)", i);
				continue;
			}

			mtl.SetNormal( i, match->normal );
			const Vector2 &uv = mtl.GetTexCoord( i, basetexChannel );
			mtl.SetTexCoord( i, dominantTexChannel, match->uvs[uv == match->uvs[0] ? 1 : 0] );	//putting the second UV here, only used when k==2
			const Vector2 area( match->totalArea,	// /numTriangles
				1.0f - (match->meshBorder ? 0.0f : match->numUniqueUVs) );	//compensating for the Y-inversion later on
			mtl.SetTexCoord( i, areaTexChannel, area );
		}

	}
	sysMemEndTemp();

	//done
	mtl.TexSetCount = areaTexChannel+1;
	return (grcFvf::grcfcTexture0Mask << dominantTexChannel) | (grcFvf::grcfcTexture0Mask << areaTexChannel);
}
#endif // PATCH_DISPLACEMENT

// there are no max() and min() for vector3's
#define VMAX(m,a,b) m.Set(Max(a.x,b.x), Max(a.y,b.y), Max(a.z,b.z))  
#define VMIN(m,a,b) m.Set(Min(a.x,b.x), Min(a.y,b.y), Min(a.z,b.z))  

// These track the cumulative AABB of all model data loaded so far.
Vector3 grmModel::sm_ModelMin(FLT_MAX,FLT_MAX,FLT_MAX);
Vector3 grmModel::sm_ModelMax(-FLT_MAX,-FLT_MAX,-FLT_MAX);

u32 grmModel::sm_GlobalChannelMask = ~0U;

int grmModel::sm_GeometriesLoaded = 0;		// Number of individual geometries loaded
int grmModel::sm_CPVBytesUsed = 0;			// Number of bytes spent on CPV data
int grmModel::sm_VertexPosBytesUsed = 0;	// Number of bytes spent on vertex position data
int grmModel::sm_VertexNormalBytesUsed = 0;	// Number of bytes spent on vertex normal data
int grmModel::sm_UVBytesUsed = 0;			// Number of bytes spent on UV data
int grmModel::sm_TotalBytesUsed = 0;		// Number of bytes spent on vertex data in total

bool grmModel::Load(const mshMesh &mesh,grmModelInfo *outInfo, const grmShaderGroup *pShaderGroup, u32 channelMask, int extraVerts, u8 mask) {
	
	grcAssertf(pShaderGroup || channelMask != ~0U, "Need to specify a shader group, channel mask, or both");
// #if !HACK_GTA4 || ((__RESOURCECOMPILER || __PPU) && !__FINAL)
// 	bool hasCloth = false;
// #endif // !HACK_GTA4

	if (!mesh.GetMtlCount()) {
		grcWarningf("Can not load a model with 0 materials");
		return false;
	}

	const int count = mesh.GetMtlCount();
	m_Count = (u16) count;
	m_ShaderIndex = rage_contained_new u16[count];
	m_Geometries.Resize(count);
	m_MatrixCount = (u8) mesh.GetMatrixCount();
	m_SkinFlagAndTessellatedGeometryCount = m_MatrixCount != 0;
	m_Mask = mask;

	m_Flags = (u8) ((mesh.IsSkinned())? MODEL_RELATIVE : 0);
	
	// We want to avoid the magic cookie overhead here (and the default ctor causing it)
	m_AABBs = (spdAABB*) rage_contained_new char[((count>1)?count+1:1)*sizeof(spdAABB)];
	
	if (outInfo) {
		outInfo->BoundBoxMtx = mesh.GetBoundBoxMatrix();
		outInfo->BoundBoxSize = mesh.GetBoundBoxSize();
		outInfo->BoundSphere = mesh.GetBoundSphere();
	}

	// Initialize the per-model AABB array.
	for (int i=0; i<count; i++)
		rage_placement_new(&m_AABBs[i + (count>1)]) spdAABB(&mesh.GetMtl(i).GetPos(0).x,mesh.GetMtl(i).GetVertexCount(),sizeof(mshVertex));
	// ...and then the final array based on the contained AABB endpoints.
	if (count > 1)
		rage_placement_new(&m_AABBs[0]) spdAABB((float*)&m_AABBs[1],count*2,sizeof(Vector4));

	// Process each geometry
	for (int i=0; i<count; i++)
	{
		if (mesh.GetMtl(i).Name[0] == '#')
			m_ShaderIndex[i] = (u16) atoi(mesh.GetMtl(i).Name + 1);
		else
			m_ShaderIndex[i] = (u16) atoi(mesh.GetMtl(i).Name);
		const char *colon = strchr(mesh.GetMtl(i).Name,':');
		if (colon)
			channelMask = (u32) atoi(colon+1);
		if (pShaderGroup && pShaderGroup->GetCount() > m_ShaderIndex[i]) {
			u32 shaderChannelMask = pShaderGroup->GetChannelMask(m_ShaderIndex[i]);
			// grcAssertf(shaderChannelMask);	// this can happen with old .shadert files (and presumably cheapshaders)
			if (shaderChannelMask)
				channelMask = shaderChannelMask;
			else if (sm_GlobalChannelMask == ~0U) {
				grcWarningf("Using default channel mask since none is available.  Make sure .dcl files are available and shader path is correct.");
				channelMask = (1<<grcFvf::grcfcPosition) | (1<<grcFvf::grcfcNormal) | (1<<grcFvf::grcfcDiffuse) | (1<<grcFvf::grcfcTexture0);
			}
		}

		grcAssertf(channelMask != ~0U,"No channel mask set, this would waste a ton of vertex buffer space.  One likely cause of this is that your shaders and/or shader templates are out of date, or at least not matching the shaders and/or shader templates used in the data.");
		if (channelMask == ~0U) {
			grcErrorf("********** YOUR CHANNEL MASK IS SHIT!  I'll fix it.");
			channelMask = (1<<grcFvf::grcfcPosition) | (1<<grcFvf::grcfcNormal) | (1<<grcFvf::grcfcDiffuse) | (1<<grcFvf::grcfcTexture0);
		}

		m_Geometries[i] = NULL;
#if __RESOURCECOMPILER || __PPU
 #if !__FINAL

		// edge doesn't support cloth, so when we're processing cloth we should ignore -edgegeom
		bool bUseEdgeGeom = PARAM_edgegeom.Get();

		// Env cloth will have a pinning channel
		bool isCloth = mesh.GetMtl(i).FindChannel(mshVtxBlindCLOTHPIN) >= 0;
		if ( isCloth && bUseEdgeGeom ) {
			sm_EdgeGeomSkinned = false;
			sm_EdgeGeomUnskinned = false;
			grcWarningf("-edgegeom doesn't work with cloth, ignoring that flag.");
		} else if (bUseEdgeGeom) {
			const char *flag = NULL;
			PARAM_edgegeom.Get(flag);
			if (flag && !strcmp(flag,"skinned"))
				sm_EdgeGeomSkinned = true;
			else if (flag && !strcmp(flag,"unskinned"))
				sm_EdgeGeomUnskinned = true;
			else
				sm_EdgeGeomSkinned = sm_EdgeGeomUnskinned = true;
		}
 #endif

		GeometryCreateParams params;
		bool edgeCapable = (g_sysPlatform == platform::PS3 && grmGeometryEdge::IsAcceptableChannelMask(channelMask & sm_GlobalChannelMask));
		bool edgeWant = (g_sysPlatform == platform::PS3 && ((mesh.IsSkinned() && sm_EdgeGeomSkinned) || (!mesh.IsSkinned() && sm_EdgeGeomUnskinned)));
		params.Edge = edgeCapable && edgeWant;
		if (sm_GeometryParamsHookFunctor.IsValid()) sm_GeometryParamsHookFunctor(*this, mesh, pShaderGroup, i, &params);
		params.Edge |= edgeCapable && edgeWant; // allow commandline to override all
		
		if (params.IsMicroMorph)
		{
			channelMask = 1 << grcFvf::grcfcPosition;
		}

		if( params.Edge) {
			m_Geometries[i] = (grmGeometry*)(rage_contained_new grmGeometryEdge);
		
			// let's try grmGeometryEdge first - if it fails because of an 
			// unsupported vertex format, fall back to grmGeometryQB.
			if (!m_Geometries[i]->Init(mesh,i,channelMask & sm_GlobalChannelMask
						,false,false
					#if HACK_GTA4
						,&params
					#endif
				)) {
				grcWarningf("EdgeGeom: grmGeometryEdge::Init() failed.. trying grmGeometryQB");
			#if	!__RESOURCECOMPILER
				delete m_Geometries[i];	// Can't do this on a resource build as its a resource heap		
			#endif
				m_Geometries[i] = NULL;
			}
		}
		GeometryCreateParams *const pParams = &params;
#else
		GeometryCreateParams params;
		if (sm_GeometryParamsHookFunctor.IsValid()) sm_GeometryParamsHookFunctor(*this, mesh, pShaderGroup, i, &params);
		GeometryCreateParams *const pParams = &params;
#endif
		
		const char* materialName = mesh.GetMtl(i).Name;
		if (!m_Geometries[i]) 
		{
			const bool isCharCloth		= grmShaderGroup::IsPedMaterial				( materialName, pShaderGroup );
			const bool isEnvCloth		= grmShaderGroup::IsEnvClothMaterial		( materialName, pShaderGroup );
#if PATCH_DISPLACEMENT
			const bool isDisplacement	= grmShaderGroup::IsDisplacementMaterial	( materialName, pShaderGroup );
			if (isDisplacement)
			{
				channelMask |= PatchDisplacement( const_cast<mshMesh&>(mesh).GetMtl(i) );
				grcDisplayf( "Displacement vertices: %u", mesh.GetMtl(i).GetVertexCount() );
			}
#endif // PATCH_DISPLACEMENT

			m_Geometries[i] = rage_contained_new grmGeometryQB;
			m_Geometries[i]->Init(mesh,i,channelMask & sm_GlobalChannelMask,isCharCloth,isEnvCloth,pParams,extraVerts); // fracbits is not used by the following function

			// Rack up the numbers.
			sm_GeometriesLoaded++;

			for (int channelType = 0; channelType < grcFvf::grcfcCount; channelType++)
			{
				int bytesUsed = grmGeometry::sm_TotalChannelSize[channelType];

				sm_TotalBytesUsed += bytesUsed;

				if (channelType == grcFvf::grcfcDiffuse)
				{
					sm_CPVBytesUsed += bytesUsed;
				}
				else if (channelType == grcFvf::grcfcPosition)
				{
					sm_VertexPosBytesUsed += bytesUsed;
				}
				else if (channelType == grcFvf::grcfcNormal || channelType == grcFvf::grcfcBinormal0 || channelType == grcFvf::grcfcTangent0 ||
						 channelType == grcFvf::grcfcBinormal1 || channelType == grcFvf::grcfcTangent1)
				{
					sm_VertexNormalBytesUsed += bytesUsed;
				}
   
				else if (channelType >= grcFvf::grcfcTexture0 && channelType <= grcFvf::grcfcTexture7)
				{
					sm_UVBytesUsed += bytesUsed;
				}
			}
		}

		if(grmShaderGroup::IsBillboardMaterial(materialName, pShaderGroup))
		{
			PatchBillboard(i, const_cast<mshMesh&>(mesh), pShaderGroup);
		}
	}

	sm_ModelMax.Max(sm_ModelMax,m_AABBs[0].GetMaxVector3());
	sm_ModelMin.Min(sm_ModelMin,m_AABBs[0].GetMinVector3());

	// LPXO: We rely on the shader index for decal layer ordering.  I.e. An artist will define the render
	// order of a series of sub-meshes based on the material id in the 3DS Max material editor
#if !HACK_GTA4
	// If we have enough information, re-sort by underlying effect hash code, then instance data sort key.
	// This was disabled on MC4 and GTA4, I believe because it broke blendshapes (which are loaded
	// separately and wouldn't know the new order).  As a rough approximation, then, let's only still
	// do this on unskinned clothless models.  Ideally we'd have a better test here.
	if (pShaderGroup && count > 1 && !m_SkinFlag && !isCloth) {
		// Lame-ass bubble sort ahead!
		for (int i=0; i<count; i++) {
			for (int j=i+1; j<count; j++) {
				const grmShader &shaderI = pShaderGroup->GetShader(m_ShaderIndex[i]);
				const grmShader &shaderJ = pShaderGroup->GetShader(m_ShaderIndex[j]);
				u32 keyI = shaderI.GetHashCode();
				u32 keyJ = shaderJ.GetHashCode();
				if (keyI == keyJ) {
					keyI = shaderI.GetSortKey();
					keyJ = shaderJ.GetSortKey();
				}
				if (keyI > keyJ) {
					// Swap.  Swapping the geometry objects is a little ugly...
					SwapEm(m_ShaderIndex[i], m_ShaderIndex[j]);
					if (m_AABBs)
						SwapEm(m_AABBs[i+1],m_AABBs[j+1]);
					SwapEm(m_Geometries[i], m_Geometries[j]);
				}
			}
		}
	}
#endif
	return true;
}


void grmModel::ResetByteCounters()
{
	sm_GeometriesLoaded = 0;
	sm_CPVBytesUsed = 0;
	sm_VertexPosBytesUsed = 0;
	sm_VertexNormalBytesUsed = 0;
	sm_UVBytesUsed = 0;
	sm_TotalBytesUsed = 0;
}
#endif		// MESH_LIBRARY


void grmModel::SetForceShader(grmShader *shad) {
	sm_ForceShader = shad;
#if GRCORE_ON_SPU > 1
	SPU_COMMAND(grmModel__SetForceShader,0);
	cmd->forceShader = shad;
#endif
}

u32 grmModel::ComputeBucketMask(const grmShaderGroup &group) const {
	u32 mask = 0;
	for (int i=0; i<m_Count; i++)
		mask |= group[m_ShaderIndex[i]].GetDrawBucketMask();

#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	bool hasTessBit = (mask & BUCKETMASK_TESSELLATION_BIT) != 0;
	mask = BUCKETMASK_SET_TESSELLATION_BIT(mask);
#endif

	u32 testMask = (mask & 0xff00);
	u32 probablyDefault = 0xff00;

	if( testMask == probablyDefault )
	{ // Probably a default mask, so let's rebuild
		unsigned int newMask = mask & 0xFF;
		newMask |= (m_Mask & 0xFF) << 8;
		mask = newMask;
	}

#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	if(hasTessBit)
	{
		mask = BUCKETMASK_SET_TESSELLATION_BIT(mask);
	}
	else
	{
		mask = BUCKETMASK_CLEAR_TESSELLATION_BIT(mask);
	}
#endif

	return mask;
}


void grmModel::Optimize(grmShaderGroup &group) {
	for (int i=0; i<m_Count; i++)
		group.GetShader(m_ShaderIndex[i]).Optimize(*this,i);
}


void grmModel::Draw(int geom) const {	
	m_Geometries[geom]->Draw();
}
void grmModel::DirectDraw(int geom) const 
{
#if __XENON
	const grmGeometryQB* g = static_cast<const grmGeometryQB*>( m_Geometries[geom].ptr );
	g->DirectDraw();
#else
	Draw( geom);
#endif
}

void grmModel::DrawSkinned(int geom,const grmMatrixSet &ms) const {
	m_Geometries[geom]->DrawSkinned(ms);
}


// grmShaderSorter *grmModel::sm_ShaderSorter;

grcCullStatus grmModel::ModelVisibleWithAABB(const spdAABB& aabb)
{
	// The combined overhead of computing NFNF every time the world matrix changed, and
	// the overhead of the additional math, made my test cases come out slightly slower.
	// However, taking the time to compute the full cull status (so that we can eliminate
	// further testing when culling does a trivial accept) was definitely a win.
	return grcViewport::GetAABBCullStatusInline(aabb.GetMinVector3(),aabb.GetMaxVector3(),grcViewport::GetCurrent()->GetCullLocalLRTB() /*,grcViewport::GetCurrent()->GetLocalNFNF()*/);
}

void grmModel::Draw(const grmShaderGroup &group,grcInstanceBuffer *ib,u32 bucketMask) const {
	// TODO: Add better culling here
	if( BUCKETMASK_MODELMATCH(m_Mask,bucketMask) )
		return;

	u32 forceBucketMask = ~0U;
	if (sm_ForceShader && sm_ForceShader->InheritsBucketId() == false) 
		forceBucketMask = sm_ForceShader->GetDrawBucketMask();

	for (int i=0; i<m_Count; i++)
	{
		const grmShader *shader = (sm_ForceShader) ? sm_ForceShader : &group[m_ShaderIndex[i]];
		u32 shaderBucketMask = (forceBucketMask==~0U) ? group[m_ShaderIndex[i]].GetDrawBucketMask() : forceBucketMask;

		//Make sure this is an instanced shader. If not, skip drawing as it won't work.
		if(BUCKETMASK_MATCH(shaderBucketMask, bucketMask) && shader->IsInstanced())
		{
			int numPasses = shader->BeginDraw(grmShader::RMC_DRAW, true);
			for (int p=0; p<numPasses; p++) {
				shader->Bind(p);
				m_Geometries[i]->DrawInstanced(ib);
				shader->UnBind();
			}
			shader->EndDraw();
		}
	}
}

#if RAGE_INSTANCED_TECH
grcCullStatus grmModel::ModelVisibleWithAABB_Instanced(const spdAABB& aabb)
{
	return grcViewport::GetInstAABBCullStatus(aabb.GetMinVector3(),aabb.GetMaxVector3());
}
#endif

#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES
void grmModel::Draw(const grmShaderGroup &group,u32 bucketMask,int lod) const 
{
	if( BUCKETMASK_MODELMATCH(m_Mask,bucketMask) )
		return;

	// grmShaderSorter *sorter = sm_ShaderSorter;

	// Trivial accept or reject.
	// I tried out a version that always guaranteed culler was not null (using a stub that
	// always returned true) but it definitely was not a win.  Guess branch prediction helps
	// out enough to avoid the relatively expensive function pointer call most of the time.
	// If we're recording a command buffer, don't do per-model tests for obvious reasons.
	ModelCullbackType culler = g_grcCommandBuffer || !m_AABBs? NULL : sm_Cullback;
	grcCullStatus status = culler? culler(m_AABBs[0]) : cullClipped;
	if (!status)
		return;
	else if (status==cullInside)
		culler = NULL;

	u32 forceBucketMask = ~0U;
	if (sm_ForceShader && sm_ForceShader->InheritsBucketId() == false) 
		forceBucketMask = sm_ForceShader->GetDrawBucketMask();

	// One part in a model is probably worth special-casing, and cuts some branches out of the other case.
	if (m_Count == 1)
	{
#if __BANK
		if (sm_CustomShaderParamsFunc != NULL && !sm_CustomShaderParamsFunc(group, m_ShaderIndex[0], false))
		{
			return;
		}
#endif // __BANK
		const grmShader *shader = (sm_ForceShader) ? sm_ForceShader : &group[m_ShaderIndex[0]];
		u32 shaderBucketMask = (forceBucketMask==~0U) ? group[m_ShaderIndex[0]].GetDrawBucketMask() : forceBucketMask;

		if (BUCKETMASK_MATCH(shaderBucketMask, bucketMask) && !shader->IsInstanced()) 
		{
			{
			#if __PS3 && HACK_GTA4 && SPU_GCM_FIFO
				SPU_COMMAND(GTA4__SetTintDescriptor,0);
				cmd->tintDescriptorPtr	= NULL;
				cmd->tintDescriptorCount= 0;
				FastAssert(m_ShaderIndex[0] < 254);
				cmd->tintShaderIdx		= m_ShaderIndex[0]+1;
			#endif
			}

			//if (sorter)
			//	sorter->Draw(*shader,*this,0,lod);
			//else
				shader->Draw(*this,0,lod,true);

			{
			#if __PS3 && HACK_GTA4 && SPU_GCM_FIFO
				SPU_COMMAND(GTA4__SetTintDescriptor,0);
				cmd->tintDescriptorPtr	= NULL;
				cmd->tintDescriptorCount= 0;
				cmd->tintShaderIdx		= 0xff;	// tint palette reset
			#endif
			}
		}
	}
	else
	{
		status = culler? culler(m_AABBs[1]) : cullClipped;

		for (int i=0; i<m_Count; i++)
		{
			grcCullStatus nextStatus = culler? culler(m_AABBs[i+2]) : cullClipped;

			if (!status)
			{
				status = nextStatus;
				continue;
			}
			else
				status = nextStatus;

#if __BANK
			if (sm_CustomShaderParamsFunc != NULL && !sm_CustomShaderParamsFunc(group, m_ShaderIndex[i], false))
			{
				continue;
			}
#endif // __BANK
			const grmShader *shader = (sm_ForceShader) ? sm_ForceShader : &group[m_ShaderIndex[i]];
			u32 shaderBucketMask = (forceBucketMask==~0U) ? group[m_ShaderIndex[i]].GetDrawBucketMask() : forceBucketMask;

			if (BUCKETMASK_MATCH(shaderBucketMask, bucketMask) && !shader->IsInstanced()) 
			{
				{
				#if __PS3 && HACK_GTA4 && SPU_GCM_FIFO
					SPU_COMMAND(GTA4__SetTintDescriptor,0);
					cmd->tintDescriptorPtr		= NULL;
					cmd->tintDescriptorCount	= 0;
					FastAssert(m_ShaderIndex[i] < 254);
					cmd->tintShaderIdx			= m_ShaderIndex[i]+1;
				#endif
				}

				//if (sorter)
				//	sorter->Draw(*shader,*this,i,lod);
				// else {
					bool nextIsSame = sm_ForceShader || (i+1<m_Count && shader->GetHashCode() == group[m_ShaderIndex[i+1]].GetHashCode() && nextStatus);
					bool restoreState = (i+1==m_Count || !nextIsSame);
					shader->Draw(*this,i,lod,restoreState);
				// }

				{
				#if __PS3 && HACK_GTA4 && SPU_GCM_FIFO
					SPU_COMMAND(GTA4__SetTintDescriptor,0);
					cmd->tintDescriptorPtr		= NULL;
					cmd->tintDescriptorCount	= 0;
					cmd->tintShaderIdx			= 0xff;	// tint palette reset
				#endif
				}
			}
		}
	}
}

void grmModel::DrawSkinned(const grmShaderGroup &group,const grmMatrixSet &ms,u32 bucketMask,int lod) const {

	if( BUCKETMASK_MODELMATCH(m_Mask,bucketMask) )
		return;

	// NOTE: there is not vis check here (as above), due to changing shape of skinned objects...

	// This could get moved to rmcDrawable::DrawMulti...
#if MTX_IN_VB && __XENON
	GRCDEVICE.GetCurrent()->SetStreamSource(MTX_IN_VB_STREAM,ms.GetVertexBuffer(),0,MTX_IN_VB_HALF? 24 : 48);
#elif MTX_IN_TEX && __XENON
	GRCDEVICE.GetCurrent()->SetTexture(D3DVERTEXTEXTURESAMPLER0 + 3,ms.GetTexture());
#endif

	u32 forceBucketMask = ~0U;
	if (sm_ForceShader && sm_ForceShader->InheritsBucketId() == false ) 
		forceBucketMask = sm_ForceShader->GetDrawBucketMask();

	for (int i=0; i<m_Count; i++) {
#if __BANK
		if (sm_CustomShaderParamsFunc != NULL && !sm_CustomShaderParamsFunc(group, m_ShaderIndex[i], true))
		{
			continue;
		}
#endif // __BANK
		const grmShader *shader = (sm_ForceShader) ? sm_ForceShader : &group[m_ShaderIndex[i]];
		u32 shaderBucketMask = (forceBucketMask==~0U) ? group[m_ShaderIndex[i]].GetDrawBucketMask() : forceBucketMask;

		if (BUCKETMASK_MATCH(shaderBucketMask, bucketMask)) 
		{
			{
#if __PS3 && HACK_GTA4 && SPU_GCM_FIFO
				SPU_COMMAND(GTA4__SetTintDescriptor,0);
				cmd->tintDescriptorPtr		= NULL;
				cmd->tintDescriptorCount	= 0;
				FastAssert(m_ShaderIndex[i] < 254);
				cmd->tintShaderIdx			= m_ShaderIndex[i]+1;
#endif
			}

			bool nextIsSame = sm_ForceShader || (i+1<m_Count && shader->GetHashCode() == group[m_ShaderIndex[i+1]].GetHashCode());
			bool restoreState = (i+1==m_Count || !nextIsSame);
			shader->DrawSkinned(*this,i,ms,lod,restoreState);

			{
#if __PS3 && HACK_GTA4 && SPU_GCM_FIFO
				SPU_COMMAND(GTA4__SetTintDescriptor,0);
				cmd->tintDescriptorPtr		= NULL;
				cmd->tintDescriptorCount	= 0;
				cmd->tintShaderIdx			= 0xff;	// tint palette reset
#endif
			}
		}
	}

#if MTX_IN_VB && __XENON
	GRCDEVICE.GetCurrent()->SetStreamSource(MTX_IN_VB_STREAM,NULL,0,0);
#endif
}

#else // !RAGE_SUPPORT_TESSELLATION_TECHNIQUES

void grmModel::DrawUntessellatedPortion(const grmShaderGroup &group, u32 bucketMask, int lod DRAWABLE_STATS_ONLY(, u16 drawableStat)) const
{
	// Just draw the untessellated portion of the geometry list.
	int geometryCount = GetUntessellatedGeometryCount();

	// Unless we have the draw all override. In which case draw all the geometries.
	const bool drawAll = false;	//use DrawTessellatedPortion instead
	if(drawAll)
		geometryCount += GetTessellatedGeometryCount();

	if(geometryCount == 0)
		return;

	DrawPortion(grmShader::RMC_DRAW, 0, geometryCount, group, bucketMask, lod DRAWABLE_STATS_ONLY(, drawableStat));
}


void grmModel::DrawTessellatedPortion(const grmShaderGroup &group,u32 bucketMask,int lod,bool reallyTessellate DRAWABLE_STATS_ONLY(, u16 drawableStat)) const
{
	int geometryCount = GetTessellatedGeometryCount();

	if(geometryCount == 0)
		return;

	// Draw the tessellated portion of the geometry list.
	DrawPortion(reallyTessellate ? grmShader::RMC_DRAW_TESSELLATED : grmShader::RMC_DRAW,
		m_Count - geometryCount, m_Count, group, bucketMask, lod DRAWABLE_STATS_ONLY(, drawableStat));
}


void grmModel::DrawPortion(grmShader::eDrawType drawType, int startIdx, int loopEnd, const grmShaderGroup &group,u32 bucketMask,int /*lod*/ DRAWABLE_STATS_ONLY(, u16 drawableStat)) const
{
	if( BUCKETMASK_MODELMATCH(m_Mask,bucketMask) )
		return;

	ModelCullbackType culler = g_grcCommandBuffer || !m_AABBs? NULL : sm_Cullback;
	grcCullStatus status = culler? culler(m_AABBs[0]) : cullClipped;
	if (!status)
	{
		DRAWABLE_STATS_INC(ModelsCulled);
		return;
	}
	else if (status==cullInside)
		culler = NULL;
	DRAWABLE_STATS_INC(ModelsDrawn);

	u32 forceBucketMask = ~0U;
	if (sm_ForceShader && sm_ForceShader->InheritsBucketId() == false) 
		forceBucketMask = sm_ForceShader->GetDrawBucketMask();

	// One part in a model is probably worth special-casing, and cuts some branches out of the other case.
	if (m_Count == 1)
	{
	#if __BANK
		if (sm_CustomShaderParamsFunc != NULL && !sm_CustomShaderParamsFunc(group, m_ShaderIndex[0], false))
		{
			return;
		}
	#endif // __BANK
		const grmShader *shader = (sm_ForceShader) ? sm_ForceShader : &group[m_ShaderIndex[0]];
		u32 shaderBucketMask = (forceBucketMask==~0U) ? group[m_ShaderIndex[0]].GetDrawBucketMask() : forceBucketMask;

		if (BUCKETMASK_MATCH(shaderBucketMask, bucketMask) && !shader->IsInstanced()) 
		{
			shader->DrawInternal(drawType,*this,0,true);
		}
	}
	else
	{
		for (int i=startIdx; i<loopEnd; i++)
		{
			status = culler ? culler(m_AABBs[i + 1]) : cullClipped;

			if (!status)
			{
				DRAWABLE_STATS_INC(GeomsCulled);
				continue;
			}
			DRAWABLE_STATS_INC(GeomsDrawn);
			DRAWABLE_STATS_INC(DrawCallsPerContext[drawableStat]);

		#if __BANK
			if (sm_CustomShaderParamsFunc != NULL && !sm_CustomShaderParamsFunc(group, m_ShaderIndex[i], false))
			{
				continue;
			}
		#endif // __BANK

			const grmShader *shader = (sm_ForceShader) ? sm_ForceShader : &group[m_ShaderIndex[i]];
			u32 shaderBucketMask = (forceBucketMask==~0U) ? group[m_ShaderIndex[i]].GetDrawBucketMask() : forceBucketMask;

			if (BUCKETMASK_MATCH(shaderBucketMask, bucketMask) && !shader->IsInstanced()) 
			{
				/*
				bool nextIsSame = sm_ForceShader || (i+1<m_Count && shader->GetHashCode() == group[m_ShaderIndex[i+1]].GetHashCode() && nextStatus);
				bool restoreState = (i+1==m_Count || !nextIsSame);
				shader->Draw(*this,i,lod,restoreState);
				*/
				shader->DrawInternal(drawType, *this, i, true);
			}
		}
	}

}


void grmModel::DrawUntessellatedPortionSkinned(const grmShaderGroup &group,const grmMatrixSet &ms,u32 bucketMask,int lod DRAWABLE_STATS_ONLY(, u16 drawableStat)) const 
{
	int geometryCount = GetUntessellatedGeometryCount();

	// Draw the tessellated geometries also if required.
	const bool drawAll = false;	//use DrawTessellatedPortionSkinned instead
	if(drawAll)
		geometryCount += GetTessellatedGeometryCount();

	if(geometryCount == 0)
		return;

	DrawSkinnedPortion(grmShader::RMC_DRAWSKINNED, 0, geometryCount, group, ms, bucketMask, lod DRAWABLE_STATS_ONLY(, drawableStat));
}


void grmModel::DrawTessellatedPortionSkinned(const grmShaderGroup &group,const grmMatrixSet &ms,u32 bucketMask,int lod,bool reallyTessellate DRAWABLE_STATS_ONLY(, u16 drawableStat)) const 
{
	int geometryCount = GetTessellatedGeometryCount();

	if(geometryCount == 0)
		return;

	DrawSkinnedPortion(reallyTessellate ? grmShader::RMC_DRAWSKINNED_TESSELLATED : grmShader::RMC_DRAWSKINNED,
		m_Count - geometryCount, m_Count, group, ms, bucketMask, lod DRAWABLE_STATS_ONLY(, drawableStat));
}


void grmModel::DrawSkinnedPortion(grmShader::eDrawType drawType, int startIdx, int loopEnd, const grmShaderGroup &group,const grmMatrixSet &ms,u32 bucketMask,int /*lod*/ DRAWABLE_STATS_ONLY(, u16 drawableStat)) const
{
	if( BUCKETMASK_MODELMATCH(m_Mask,bucketMask) )
		return;

	u32 forceBucketMask = ~0U;
	if (sm_ForceShader && sm_ForceShader->InheritsBucketId() == false ) 
		forceBucketMask = sm_ForceShader->GetDrawBucketMask();
	DRAWABLE_STATS_INC(ModelsDrawn);

	for (int i=startIdx; i<loopEnd; i++) 
	{
#if __BANK
		if (sm_CustomShaderParamsFunc != NULL && !sm_CustomShaderParamsFunc(group, m_ShaderIndex[i], true))
		{
			continue;
		}
#endif // __BANK

		const grmShader *shader = (sm_ForceShader) ? sm_ForceShader : &group[m_ShaderIndex[i]];
		u32 shaderBucketMask = (forceBucketMask==~0U) ? group[m_ShaderIndex[i]].GetDrawBucketMask() : forceBucketMask;

		// Use the regular bucket match here (it will ignore the tessellation bit)
		if (BUCKETMASK_MATCH(shaderBucketMask, bucketMask)) 
		{
			bool nextIsSame = sm_ForceShader || (i+1<m_Count && shader->GetHashCode() == group[m_ShaderIndex[i+1]].GetHashCode());
			bool restoreState = (i+1==m_Count || !nextIsSame);
			shader->DrawSkinnedInternal(drawType, *this, i, ms, restoreState);
			DRAWABLE_STATS_INC(GeomsDrawn);
			DRAWABLE_STATS_INC(DrawCallsPerContext[drawableStat]);
		}
	}
}

#endif // !RAGE_SUPPORT_TESSELLATION_TECHNIQUES


grmModel::grmModel(datResource &rsc) : m_Geometries(GRCARRAY_PLACEELEMENTS,rsc) {
	rsc.PointerFixup(m_ShaderIndex);
	if (m_AABBs)
		rsc.PointerFixup(m_AABBs);
}


IMPLEMENT_PLACE(grmModel)

#if TEMP_RUNTIME_CALCULATE_TERRAIN_OPEN_EDGE_INFO
extern void BuildTerrainOpenEdgeInfo(grmGeometry &geometry);
extern void TestWeldLandscape(grmGeometry &geometry);
extern bool TestWeld(grmGeometry &geometry);
#endif // TEMP_RUNTIME_CALCULATE_TERRAIN_OPEN_EDGE_INFO

#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
void grmModel::SortForTessellation(const grmShaderGroup &group)
{
	class Bucket
	{
	public:
		Bucket()
		{
			m_Count = 0;
			m_pGeometries = NULL;
			m_pAABBs = NULL;
			m_pShaderIndices = NULL;
		}
		~Bucket()
		{
		}
		void SetBucketMemory(datOwner<grmGeometry> *pGeometries, spdAABB *pAABBs, u16 *pShaderIndices)
		{
			m_pGeometries = pGeometries;
			m_pAABBs = pAABBs;
			m_pShaderIndices = pShaderIndices;
		}
		void Add(datOwner<grmGeometry> pGeometry, spdAABB &AABB, u16 ShaderIndex)
		{
			Assert((m_pGeometries != NULL) && (m_pAABBs != NULL) && (m_pShaderIndices != NULL));
			m_pGeometries[m_Count] = pGeometry;
			m_pAABBs[m_Count] = AABB;
			m_pShaderIndices[m_Count++] = ShaderIndex;
		}
	public:
		int m_Count;
		datOwner<grmGeometry> *m_pGeometries;
		spdAABB *m_pAABBs;
		u16 *m_pShaderIndices;
	};

	u32 noOfTessellated = 0;

	if(m_Count == 1)
	{
		u32 drawBucketMask = group[m_ShaderIndex[0]].GetDrawBucketMask();

		if(drawBucketMask & BUCKETMASK_TESSELLATION_BIT)
		{
			noOfTessellated = 1;
		}
	}
	else
	{
		int i;
		spdAABB dummyAABB;
		Bucket tesellated;
		Bucket untesellated;
		{
			datOwner<grmGeometry> *pTessellatedGeometries = Alloca(datOwner<grmGeometry>, m_Count);
			datOwner<grmGeometry> *pUntessellatedGeometries = Alloca(datOwner<grmGeometry>, m_Count);

			spdAABB *pTessellatedAABBs = Alloca(spdAABB, m_Count);
			spdAABB *pUntessellatedAABBs = Alloca(spdAABB, m_Count);

			u16 *pTessellatedShaderIndices = Alloca(u16, m_Count);
			u16 *pUntessellatedShaderIndices = Alloca(u16, m_Count);
			{
				tesellated.SetBucketMemory(pTessellatedGeometries, pTessellatedAABBs, pTessellatedShaderIndices);
				untesellated.SetBucketMemory(pUntessellatedGeometries, pUntessellatedAABBs, pUntessellatedShaderIndices);

				for(i=0; i<m_Count; i++)
				{
					spdAABB &AABB = m_AABBs ? m_AABBs[i+1] : dummyAABB;
					u32 drawBucketMask = group[m_ShaderIndex[i]].GetDrawBucketMask();

					// Does the shader instance use tessellation?
					if(drawBucketMask & BUCKETMASK_TESSELLATION_BIT)
					{
						// Yes...place into the tessellated bucket.
						tesellated.Add(m_Geometries[i], AABB, m_ShaderIndex[i]);
					}
					else
					{
						// No...place into the un-tessellated bucket.
						untesellated.Add(m_Geometries[i], AABB, m_ShaderIndex[i]);
					}
				}

				// Place the un-tessellated geometries first.
				for(i=0; i<untesellated.m_Count; i++)
				{
					m_Geometries[i] = untesellated.m_pGeometries[i];
					if(m_AABBs)
						m_AABBs[i+1] = untesellated.m_pAABBs[i];
					m_ShaderIndex[i] = untesellated.m_pShaderIndices[i];
				}

				int offset = untesellated.m_Count;

				// Place the tessellated geometries at the end.
				for(i=0; i<tesellated.m_Count; i++)
				{
					m_Geometries[i+offset] = tesellated.m_pGeometries[i];
					if(m_AABBs)
						m_AABBs[i+1+offset] = tesellated.m_pAABBs[i];
					m_ShaderIndex[i+offset] = tesellated.m_pShaderIndices[i];
				}
				noOfTessellated = tesellated.m_Count;
			}
		}
	}

	AssertMsg(noOfTessellated <= 127, "grmModel::SortForTessellation()...grmModel/grmShaderGroup combination uses too many tessellated geometries (max is 127)\n");

	// Record the number of tessellated geometries in the upper bits of the skin flag.
	m_SkinFlagAndTessellatedGeometryCount = (m_SkinFlagAndTessellatedGeometryCount & 0x1) | ((u8)noOfTessellated << 0x1);

#if TEMP_RUNTIME_CALCULATE_TERRAIN_OPEN_EDGE_INFO
	for(int i=0; i<m_Count; i++)
	{
		const grmShader &shader = group[m_ShaderIndex[i]];

		if(shader.GetDrawBucketMask() & BUCKETMASK_TESSELLATION_BIT)
		{
			if(!strcmp(shader.GetName(), "terrain_cb_4lyr_2tex_blend"))
			{
				BuildTerrainOpenEdgeInfo(*m_Geometries[i]);
			}
		}
	}
#endif // TEMP_RUNTIME_CALCULATE_TERRAIN_OPEN_EDGE_INFO
}
#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES

#if __DECLARESTRUCT

void grmModel::DeclareStruct(datTypeStruct &s) {
	STRUCT_BEGIN(grmModel);
	STRUCT_FIELD(m_Geometries);
	if (m_Count>1) ++m_Count;
	STRUCT_DYNAMIC_ARRAY(m_AABBs,m_Count);
	if (m_Count>1) --m_Count;
	STRUCT_DYNAMIC_ARRAY(m_ShaderIndex,m_Count);
	STRUCT_FIELD(m_MatrixCount);
	STRUCT_FIELD(m_Flags);
	STRUCT_FIELD(m_Type);
	STRUCT_FIELD(m_MatrixIndex);
	STRUCT_FIELD(m_Mask);
	STRUCT_FIELD(m_SkinFlagAndTessellatedGeometryCount);
	STRUCT_FIELD(m_Count);
	STRUCT_END();
}

/* void grmModelGeom::DeclareStruct(datTypeStruct &s) {
	grmModel::DeclareStruct(s);
	STRUCT_BEGIN(grmModelGeom);
	STRUCT_FIELD(
}*/
#endif

#if !__FINAL
void grmModel::SetModelCullerDebugFlags(u8 PS3_ONLY(flags))
{
#if __PS3
	SPU_SIMPLE_COMMAND(grmModel__SetCullerDebugFlags, flags);
#endif // __PS3
}
#endif // !__FINAL

void grmModel::SetModelCullback(ModelCullbackType cb)
{
	sm_Cullback = cb;
#if __PS3 && SPU_GCM_FIFO
	SPU_SIMPLE_COMMAND(grmModel__SetCullerAABB, sm_Cullback==ModelVisibleWithAABB?1:0 );
#endif // SPU_GCM_FIFO
}

