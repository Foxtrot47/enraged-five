// 
// grmodel/matrixset.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRMODEL_MATRIXSET_H 
#define GRMODEL_MATRIXSET_H 

#include "grcore/channel.h"
#include "grcore/matrix43.h"
#include "shaderlib/skinning_method_config.h"

struct D3DVertexBuffer;
struct D3DTexture;

namespace rage {

class crSkeleton;
class grcCBuffer;

class grmMatrixSet {
public:
#if MTX_IN_VB_HALF && __XENON
	typedef Matrix43_Float16 MatrixType;
#else
	typedef Matrix43 MatrixType;
#endif

	grmMatrixSet(datResource&);

	DECLARE_PLACE(grmMatrixSet);

	// PURPOSE:	Create a matrixset on the heap
	static grmMatrixSet *Create(const crSkeleton &skel);

	// PURPOSE:	Create a matrixset on the heap
	static grmMatrixSet *Create(int boneCount);

	// PURPOSE:	Create a matrixset in a particular spot of memory, returning the number of bytes consumed.
	// NOTES:	Memory should be allocated via grmMatrixSet::Allocate static member function.
	static unsigned Create(void *placement,int boneCount);

	// PURPOSE:	Update a matrixset from a skeleton
	// PARAMS:	skel - Skeleton to attach to
	//			isSkinned - Whether skinned or not; typically want the result of pDrawable->IsSkinned() here.
	void Update(const crSkeleton &skel,bool isSkinned);

	// PURPOSE:	Zero the matrix after it was already updated.  (Needed by skinned fragments)
	void Zero(int index);

	// PURPOSE:	copy the matrix and flag from a different matrixset to the one (the  the matrix after it was already updated.  
	//          NOTE: it will remember the original matrix count it was created with, and that must be larger than the 
	//				  count of the set being copied.
	void CopySet(const grmMatrixSet & src);

	int GetMatrixCount() const { return m_MatrixCount; }

	MatrixType* GetMatrices() { grcAssertf(!__RESOURCECOMPILER, "Don't call this in __RESOURCECOMPILER builds"); SetDirty(true); return m_MatrixData; }

	const MatrixType* GetMatrices() const { grcAssertf(!__RESOURCECOMPILER, "Don't call this in __RESOURCECOMPILER builds"); return m_MatrixData; }

	// PURPOSE:	Reconstruct the original matrix (untransposed, and without the offset subtracted off).
	void ComputeMatrix(Matrix34 &dest,int idx) const;

#if __XENON && MTX_IN_VB
	D3DVertexBuffer *GetVertexBuffer() const { return (D3DVertexBuffer*)this; }
#endif

#if MTX_IN_CB
	void Bind(grcCBuffer* pCBuffer) const;
	void UnBind() const;
	void SetDirty(bool dirty=true) const;
	bool IsDirty() const;
	static void MakeDirty();
#else
	void Bind(grcCBuffer* /*pCBuffer*/) const {};
	void UnBind() const {};
	void SetDirty(bool /*dirty=true*/) const {};
	static void MakeDirty() {};
#endif // MTX_IN_CB

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

#if __RESOURCECOMPILER
	static unsigned ComputeSize(int boneCount);
#else
	static unsigned ComputeSize(int boneCount) { return sizeof(grmMatrixSet) + boneCount * sizeof(MatrixType); }
#endif

	// PURPOSE:	Retrieve isSkinned value from last Update
	bool IsSkinned() const { return m_IsSkinned != 0; }

	// PURPOSE:	Allocate memory suitable for grmMatrixSet placement new.
	// NOTES:	This is physical memory on Xenon, and 128-byte-aligned memory on PS3
	static void *Allocate(unsigned size);

	// PURPOSE:	Frees memory allocated by grmMatrixSet::Allocate.
	static void Free(void *ptr);

	static void operator delete(void*ptr) { Free(ptr); }
private:
	void Init(int boneCount);
	void InitDriverHeader();

	// These are resourced objects.  We could mark these __64BIT since only Xbox 360 needs them.
	// (Actually, we don't currently use the "fetch skinning from a vertex buffer" code path anyway, so these could be removed entirely)
	ATTR_UNUSED union
	{
		u32			D3DCommon;		// !MTX_IN_CB
		mutable u32	m_InstanceId;	// MTX_IN_CB
	};

	ATTR_UNUSED u32 D3DReferenceCount;
	ATTR_UNUSED u32 D3DFence;
	ATTR_UNUSED u32 D3DReadFence;

	u8 m_MatrixCount;
	u8 m_MaxMatrixCount;
	u8 m_IsSkinned;
	ATTR_UNUSED u8 m_Padding;

	ATTR_UNUSED u32 D3DBaseFlush;
	ATTR_UNUSED u32 D3DBaseAddress;
	ATTR_UNUSED u32 D3DSize;

	MatrixType m_MatrixData[0];	
};

} // namespace rage

#endif // GRMODEL_MATRIXSET_H 
