//
// grmodel/shadergroup.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "shadergroup.h"
#include "grmodel_config.h"
#include "grcore/channel.h"

#include "shader.h"

#include "atl/array_struct.h"
#include "bank/bank.h"
#include "data/resource.h"
#include "data/resourcehelpers.h"
#include "data/struct.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/token.h"
#include "grcore/effect_config.h"
#include "grcore/texture.h"
#include "grcore/texturereference.h"
#include "system/typeinfo.h"
#if USE_MTL_SYSTEM
#include "rageShaderMaterial/rageShaderMaterialGroup.h"
#endif

#if __PS3
#include "grmodel/grmodelspu.h"
#endif

#define MAINTAIN_SHADERDATAS	__PS3

#if !__FINAL
#include <algorithm>
#endif

namespace rage {


bool grmShaderGroup::sm_AutoTexDict = false;

bool grmShaderGroup::SetAutoTexDict(bool flag) {
	bool p = sm_AutoTexDict;
	sm_AutoTexDict = flag;
	return p;
}


grmShaderGroup::grmShaderGroup() 
: m_TextureDictionary(NULL)
, m_ContainerSizeQW(0)
, m_HasInstancedShader(false)
, pad0(0)
, m_ShaderDatas(NULL)
{
	//TODO: If not resourced, needs to scan for instanced shaders when they are added.
}



grmShaderGroup::~grmShaderGroup() {
	for (int i=0; i<GetCount(); i++) {
		if (m_Shaders[i]) {
			delete m_Shaders[i];
		}
	}
	delete[] m_ShaderDatas;
	delete m_TextureDictionary;
}


void grmShaderGroup::InitCount(int count) {
	m_Shaders.Reserve(count);
}


int grmShaderGroup::Add(const char *shaderName) {
	int i;
	for (i=0; i<GetCount(); i++) {
		if (m_Shaders[i] && !stricmp(shaderName,m_Shaders[i]->GetName())) {
			return i;
		}
	}
	grmShader *shader = grmShaderFactory::GetInstance().Create();
	shader->Load(shaderName);
	m_Shaders.Append() = shader;
	return i;
}


int grmShaderGroup::Add(grmShader *shader) {
	if (shader)
	{
		int i;
		for (i=0; i<GetCount(); i++) {
			if (shader == m_Shaders[i])
				return i;
		}
		m_Shaders.Append() = shader;
		return i;
	}
	else
	{
		// null shader
		m_Shaders.Append() = shader;
		return GetCount();
	}
}


bool grmShaderGroup::Remove(grmShader *shader, const bool owned) {
	bool result = false;

	if (shader)
	{
		int i;
		for (i=0; i<GetCount(); i++) {
			if (shader == m_Shaders[i])
				break;
		}
		
		if( i<GetCount() )
		{
			result = true;
			m_Shaders.Delete(i);
			if( owned )
			{
				delete shader;
			}
		}
	}

	return result;
}

static const char *getMtlFileName(const char* materialName, const grmShaderGroup* shaderGroup)
{
	grcAssertf(shaderGroup, "Invalid shader group");
	grcAssertf(materialName, "Invalid material name");

	u16 shaderIndex=(u16)-1;
	if (materialName[0] == '#')
		shaderIndex = (u16) atoi(materialName + 1);
	else
		shaderIndex = (u16) atoi(materialName);

	return shaderGroup->GetShader(shaderIndex).GetMtlFileName();
}

bool grmShaderGroup::IsEnvClothMaterial( const char* materialName, const grmShaderGroup* shaderGroup )
{
	const char* mtlfilename = getMtlFileName(materialName,shaderGroup);
	if (mtlfilename)
	{
		if(	!strcmpi(mtlfilename,"cloth_default.sps")			||			
			!strcmpi(mtlfilename,"cloth_normal_spec.sps")		||
			!strcmpi(mtlfilename,"cloth_normal_spec_tnt.sps")	||
			!strcmpi(mtlfilename,"cloth_normal_spec_alpha.sps") ||
			!strcmpi(mtlfilename,"cloth_normal_spec_cutout.sps")||
			!strcmpi(mtlfilename,"cloth_spec_alpha.sps")		||
			!strcmpi(mtlfilename,"cloth_spec_cutout.sps")		||
			!strcmpi(mtlfilename,"cloth_sheet_dynamic.sps")		||
			!strcmpi(mtlfilename,"vehicle_cloth.sps")			||
			!strcmpi(mtlfilename,"vehicle_cloth2.sps")			||
			false
			)
			return true;
	}
	return false;
}

bool grmShaderGroup::IsPedClothMaterial( const char* materialName, const grmShaderGroup* shaderGroup )
{
	const char* mtlfilename = getMtlFileName(materialName,shaderGroup);
	if (mtlfilename)
	{
		if(	!strcmpi(mtlfilename,"ped_cloth.sps")					||
			!strcmpi(mtlfilename,"ped_cloth_enveff.sps")			||
			!strcmpi(mtlfilename,"ped_default_cloth.sps")			||
			!strcmpi(mtlfilename,"ped_hair_cutout_alpha_cloth.sps")	||
			!strcmpi(mtlfilename,"ped_wrinkle_cloth.sps")			||
			!strcmpi(mtlfilename,"ped_wrinkle_cloth_enveff.sps")	)
		{
			return true;
		}
	}
	return false;
}

bool grmShaderGroup::IsPedMaterial( const char* materialName, const grmShaderGroup* shaderGroup )
{
	const char* mtlfilename = getMtlFileName(materialName,shaderGroup);
	if (mtlfilename)
	{
		if(	strstr(mtlfilename,"ped.sps") ||
			strstr(mtlfilename,"ped_")	
		  )
			return true;
	}
	return false;
}

bool grmShaderGroup::IsBillboardMaterial( const char* materialName, const grmShaderGroup* shaderGroup )
{
	const char* mtlfilename = getMtlFileName(materialName,shaderGroup);
	if (mtlfilename)
	{
		if(	strstr(mtlfilename,"trees_lod2") )
			return true;
	}
	return false;
}

bool grmShaderGroup::IsDisplacementMaterial( const char* materialName, const grmShaderGroup* shaderGroup )
{
	const char* name = getMtlFileName(materialName,shaderGroup);
	return name && strstr(name,"_dpm");
}

bool grmShaderGroup::IsCameraMaterial( const char* materialName, const grmShaderGroup* shaderGroup )
{
	const char* name = getMtlFileName(materialName,shaderGroup);
	return name && strstr(name,"_camera_");
}

#if MULTIPLE_RENDER_THREADS
static sysCriticalSectionToken s_LookupToken;
#define LOOKUP_CS_TOKEN() SYS_CS_SYNC(s_LookupToken)
#else
#define LOOKUP_CS_TOKEN()
#endif

grmShaderGroupVar grmShaderGroup::LookupVar(const char *name,bool mustExist) const {
	u32 hash = atStringHash(name);

	LOOKUP_CS_TOKEN();

	for (int i=0; i<m_ShaderGroupVars.GetCount(); i++)
		if (m_ShaderGroupVars[i].Hash == hash)
			return (grmShaderGroupVar)(i+1);

	int count = GetCount();
	grcEffectVar *list = Alloca(grcEffectVar,count);
	int numFound = 0;
	for (int i=0; i<count; i++) {
		if ((list[i] = m_Shaders[i]->LookupVarByHash(hash)) != grcevNONE)
			++numFound;
	}
	if (mustExist && !numFound) {
		Quitf(ERR_GFX_SHADER_VAR,"Required grmShaderGroupVar '%s' not found.",name);
	}
	if (!numFound)
		return grmsgvNONE;

	grcEffectVarEntry &newVar = m_ShaderGroupVars.Grow();
	newVar.Pairs.Reserve(numFound);
	for (int i=0; i<count; i++)
		if (list[i])
			newVar.Pairs.Append().Init(i,list[i]);
	newVar.Hash = hash;
	return (grmShaderGroupVar) m_ShaderGroupVars.GetCount();
}

#if (__D3D11 || RSG_ORBIS)
grmShaderGroupVar grmShaderGroup::LookupVarForInstancing(u32 HashName, u32 HashToRegisterWith) const
{
	LOOKUP_CS_TOKEN();

	// Check for the variable existing already..
	for (int i=0; i<m_ShaderGroupVars.GetCount(); i++)
		if (m_ShaderGroupVars[i].Hash == HashToRegisterWith)
			return (grmShaderGroupVar)(i+1);

	// Look the variable up in each shader.
	int count = GetCount();
	grcEffectVar *list = Alloca(grcEffectVar,count);
	int numFound = 0;
	for (int i=0; i<count; i++) {
		if ((list[i] = m_Shaders[i]->LookupVarByHash(HashName)) != grcevNONE)
			++numFound;
	}
	if (!numFound)
		return grmsgvNONE;

	// Add all entries.
	grcEffectVarEntry &newVar = m_ShaderGroupVars.Grow();
	newVar.Pairs.Reserve(count);
	for (int i=0; i<count; i++)
		newVar.Pairs.Append().Init(i,list[i]);
	newVar.Hash = HashToRegisterWith;
	return (grmShaderGroupVar) m_ShaderGroupVars.GetCount();
}
#endif // (__D3D11 || RSG_ORBIS)

bool grmShaderGroup::IsVar(grmShaderGroupVar var) const {
	if (AssertVerify(var))
		return false;

	LOOKUP_CS_TOKEN();

	int count = m_ShaderGroupVars[var-1].Pairs.GetCount();
	bool found = false;
	for (int i=0; i<count && !found; i++) {
		grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[i];
		found = m_Shaders[p.index]->IsVar((grcEffectVar)p.var);
	}
	return found;
}

grmShader* grmShaderGroup::LookupShader(const char *shaderName) const {
	for (int i=0; i<GetCount(); i++) {
		if (m_Shaders[i] && !stricmp(shaderName,m_Shaders[i]->GetName()))
			return m_Shaders[i];
	}
	return 0;
}

bool grmShaderGroup::CheckForShadersWithSuffix(const char *suffix) {
	size_t suffixLen = strlen(suffix);

	for (int i=0; i<GetCount(); i++) {
		if (m_Shaders[i]) {
			const char* shaderName = m_Shaders[i]->GetName();
			const char* shaderSuffix = shaderName + strlen(shaderName) - suffixLen;

			if (!stricmp(suffix,shaderSuffix))
				return true;
		}
	}
	return false;
}

#if SUPPORT_UAV
void grmShaderGroup::SetVar(grmShaderGroupVar var,const grcBufferUAV *v)
{
	LOOKUP_CS_TOKEN();

	int varCount = m_ShaderGroupVars[var-1].Pairs.GetCount(); 
	for (int i=0; i<varCount; i++) {
		grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[i];
		m_Shaders[p.index]->SetVar((grcEffectVar)p.var, v);
	}
}

void grmShaderGroup::SetVarUAV(grmShaderGroupVar var,grcBufferUAV *v,int initCount)
{
	LOOKUP_CS_TOKEN();

	int varCount = m_ShaderGroupVars[var-1].Pairs.GetCount(); 
	for (int i=0; i<varCount; i++) {
		grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[i];
		m_Shaders[p.index]->SetVarUAV((grcEffectVar)p.var, v, initCount);
	}
}

void grmShaderGroup::SetVarUAV(grmShaderGroupVar var,const grcTextureUAV *v)
{
	LOOKUP_CS_TOKEN();

	int varCount = m_ShaderGroupVars[var-1].Pairs.GetCount(); 
	for (int i=0; i<varCount; i++) {
		grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[i];
		m_Shaders[p.index]->SetVarUAV((grcEffectVar)p.var, v);
	}
}
#endif //SUPPORT_UAV

void grmShaderGroup::SetVarCommon(grmShaderGroupVar var,const void *data,int stride,int arrayCount) const
{
	if (!AssertVerify(var))
		return;
#if GRCORE_ON_SPU > 1
	if (GCM_CONTEXT) 
	{
		Assert(m_ShaderDatas);
		// stride is always sizeof(value)
		int byteCount = stride * arrayCount;
		SPU_COMMAND(grmShaderGroup__SetVarCommon,0,byteCount);
		cmd->datas = m_ShaderDatas;
		cmd->vars = (spuEffectVarPair*) m_ShaderGroupVars[var-1].Pairs.GetElements();
		Assign(cmd->stride,stride);
		Assign(cmd->arrayCount,arrayCount);
		Assign(cmd->varCount,m_ShaderGroupVars[var-1].Pairs.GetCount());
		Assign(cmd->shaderCount,m_Shaders.GetCount());
		u32 *dest = cmd->alignedPayload;
		Assert(((u32)dest & 15) == 0 || byteCount < 16);	// must be aligned if 16 bytes or larger
		const u32 *src = (u32*) data;
		do {
			*dest++ = *src++;
			byteCount -= 4;
		} while (byteCount);
	}
	else
#endif
	{
		LOOKUP_CS_TOKEN();

		int varCount = m_ShaderGroupVars[var-1].Pairs.GetCount(); 
		for (int i=0; i<varCount; i++) {
			grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[i];
			grcInstanceData &id = m_Shaders[p.index]->GetInstanceData();
			id.Basis->SetLocalCommon(id,(grcEffectVar)p.var,data,stride,arrayCount);
		}
	}
}

void grmShaderGroup::SetVarCommonByRef(grmShaderGroupVar var,const void *data,int stride,int arrayCount) const
{
	if (!AssertVerify(var))
		return;

#if GRCORE_ON_SPU > 1
	if (GCM_CONTEXT) 
	{
		Assert(m_ShaderDatas);
		// stride is always sizeof(value)
		SPU_COMMAND(grmShaderGroup__SetVarCommonByRef,0);
		cmd->datas = m_ShaderDatas;
		cmd->vars = (spuEffectVarPair*) m_ShaderGroupVars[var-1].Pairs.GetElements();
		Assign(cmd->stride,stride);
		Assign(cmd->arrayCount,arrayCount);
		Assign(cmd->varCount,m_ShaderGroupVars[var-1].Pairs.GetCount());
		Assign(cmd->shaderCount,m_Shaders.GetCount());
		cmd->src = const_cast<void*>(data);
	}
	else
#endif
	{
		LOOKUP_CS_TOKEN();

		int varCount = m_ShaderGroupVars[var-1].Pairs.GetCount(); 
		for (int i=0; i<varCount; i++) {
			grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[i];
			grcInstanceData &id = m_Shaders[p.index]->GetInstanceData();
			id.Basis->SetLocalCommonByRef(id,(grcEffectVar)p.var,data,stride,arrayCount);
		}
	}
}

#if (__D3D11 || RSG_ORBIS)
grcCBuffer *grmShaderGroup::GetVarCBufferAndOffset(grmShaderGroupVar var, int ShaderIndex, int &Offset) const
{
	grcCBuffer *pRet = NULL;

	if(var)
	{
		LOOKUP_CS_TOKEN();

		grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[ShaderIndex];
		pRet = m_Shaders[ShaderIndex]->GetVarCBufferAndOffset((grcEffectVar)p.var, Offset);
	}
	return pRet;
}
#endif // (__D3D11 || RSG_ORBIS)


void grmShaderGroup::SetVar(grmShaderGroupVar var,const grcTexture *tex) const
{
	if (!AssertVerify(var))
		return;

	LOOKUP_CS_TOKEN();

	int varCount = m_ShaderGroupVars[var-1].Pairs.GetCount(); 
	for (int i=0; i<varCount; i++) {
		grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[i];
		m_Shaders[p.index]->SetVar((grcEffectVar)p.var,tex);
	}
}

void grmShaderGroup::SetVar(grmShaderGroupVar var,Color32 value) const
{
	if (!AssertVerify(var))
		return;

	LOOKUP_CS_TOKEN();

	int varCount = m_ShaderGroupVars[var-1].Pairs.GetCount(); 
	for (int i=0; i<varCount; i++) {
		grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[i];
		m_Shaders[p.index]->SetVar((grcEffectVar)p.var,value);
	}
}

void grmShaderGroup::SetVar(grmShaderGroupVar var,const Color32 *value,int arrayCount) const
{
	if (!AssertVerify(var))
		return;

	LOOKUP_CS_TOKEN();

	int varCount = m_ShaderGroupVars[var-1].Pairs.GetCount(); 
	for (int i=0; i<varCount; i++) {
		grcEffectVarPair &p = m_ShaderGroupVars[var-1].Pairs[i];
		m_Shaders[p.index]->SetVar((grcEffectVar)p.var,value,arrayCount);
	}
}




#if !__FINAL
struct TexEntry {
	const char *name;
	u64 fileSize;
	u32 index;
};

struct TexEntrySorter {
	bool operator()(TexEntry const & attr1, TexEntry const & attr2) const {
		return attr1.fileSize > attr2.fileSize;
	}
};


pgDictionary<grcTexture>* grmShaderGroup::CreateTextureDictionary(const char **inputs,int inputCount,bool stripPath, grcTextureFactory::TextureCreateParams** params /*=NULL*/) {
	grcTextureFactory::GetInstance().ClearFreeList();

	TexEntry *te = Alloca(TexEntry, inputCount);
	int finalCount = 0;
	for (int i=0; i<inputCount; i++) {
		te[finalCount].name = inputs[i]; 
		te[finalCount].index = i;
		char buf[RAGE_MAX_PATH];
		ASSET.FullReadPath(buf,sizeof(buf),inputs[i],"dds");
		te[finalCount].fileSize = fiDevice::GetDevice(buf)->GetFileSize(buf);
		if (te[finalCount].fileSize)
			++finalCount;
		else
			grcWarningf("Texture '%s' not found, hopefully in parent dictionary at runtime?",buf);
	}

	if (!finalCount)
		return NULL;

	// sort by size
	sysMemStartTemp();
	std::sort(te, te+finalCount, TexEntrySorter());
	sysMemEndTemp();

	pgDictionary<grcTexture>* result = rage_new pgDictionary<grcTexture>(finalCount);
	for (int i=0; i<finalCount; i++)
	{
		char hashName[RAGE_MAX_PATH];
		safecpy(hashName,stripPath? ASSET.FileName(te[i].name) : te[i].name);
		char *ext = strrchr(hashName,'.');
		if (ext && !stricmp(ext,".dds"))
			*ext = '\0';
		if (result->Lookup(hashName))
		{
			grcWarningf("Duplicate texture %s not added to the texture dictionary", hashName);
		}
		else if (!(result->AddEntry(hashName, grcTextureFactory::GetInstance().Create(te[i].name, (params?params[te[i].index]:NULL)))))
		{
			grcErrorf("Could not add texture %s to the texture dictionary", hashName);
		}
	}

	return result;
}

pgDictionary<grcTexture>* grmShaderGroup::CreateTextureDictionary(const char *filename,bool stripPath) 
{
	fiStream *S = NULL;
	if(ASSET.Exists(filename,""))
	{
		S = strrchr(filename,'.')? ASSET.Open(filename,"") : NULL;
	}
	if ((!S) && (ASSET.Exists(filename,"txt")))
	{
		S = ASSET.Open(filename,"txt",true);
	}
	if ((!S) && (ASSET.Exists(filename,"textures")))
	{
		S = ASSET.Open(filename,"textures",true);
	}
	if (!S)
		return NULL;
	int ddsCount = 0;
	char buf[RAGE_MAX_PATH];
	while (fgetline(buf,sizeof(buf),S))
		++ddsCount;
	const char **inputs = Alloca(const char*,ddsCount);
	grcTextureFactory::TextureCreateParams** params = Alloca(grcTextureFactory::TextureCreateParams*,ddsCount);
	S->Seek(0);
	ddsCount = 0;

	fiTokenizer T(filename,S);
	char terminator = -1;
	while (T.GetTokenToChars(buf,sizeof(buf), "{\n", 2, terminator, false)!=-1)
	{
		inputs[ddsCount] = Alloca(char,strlen(buf)+1);
		params[ddsCount] = NULL;
		strcpy((char*)inputs[ddsCount], buf);
		
		if (terminator == '{')
		{
			params[ddsCount] = Alloca(grcTextureFactory::TextureCreateParams,1);
			params[ddsCount]->Format = grcTextureFactory::TextureCreateParams::TILED;
			params[ddsCount]->Memory = grcTextureFactory::TextureCreateParams::VIDEO;
			params[ddsCount]->LockFlags = 0;
			params[ddsCount]->Buffer = NULL;
			params[ddsCount]->Type = grcTextureFactory::TextureCreateParams::NORMAL;

			if (T.CheckIToken("Memory"))
			{
				T.GetToken(buf, sizeof(buf));
				
				if (stricmp(buf, "system") == 0)
				{
					params[ddsCount]->Memory = grcTextureFactory::TextureCreateParams::SYSTEM;
				}
				else if (stricmp(buf, "video") == 0)
				{
					params[ddsCount]->Memory = grcTextureFactory::TextureCreateParams::VIDEO;
				}
			}

			if (T.CheckIToken("Format"))
			{
				T.GetToken(buf, sizeof(buf));

				if (stricmp(buf, "tiled") == 0)
				{
					params[ddsCount]->Format = grcTextureFactory::TextureCreateParams::TILED;
				}
				else if (stricmp(buf, "linear") == 0)
				{
					params[ddsCount]->Format = grcTextureFactory::TextureCreateParams::LINEAR;
				}
			}

			if (T.CheckIToken("Flags"))
				params[ddsCount]->LockFlags = T.GetInt();
				
			T.MatchToken("}");
			T.SkipToEndOfLine();
		}
		++ddsCount;
	}
	// Close the file after calling shared function to avoid potential for compiler to screw
	// up and try to tail recurse even though we're using Alloca.
	pgDictionary<grcTexture>* result = NULL;
#if __RESOURCECOMPILER
	// For the resource compiler we may have a custom callback set; so we invoke that here
	// if its valid, otherwise defaulting to the local implementation.
	if ( NULL != ms_CustomCreateTextureDictionaryCallback )
		result = RunCreateTextureDictionaryCallback( inputs, ddsCount, stripPath, params );
	else
		result = CreateTextureDictionary( inputs, ddsCount, stripPath, params );
#else
	result = CreateTextureDictionary(inputs,ddsCount,stripPath,params);
#endif
	S->Close();
	return result;
}
#endif

bool grmShaderGroup::Load(fiTokenizer &T) {

#if !__FINAL
	pgDictionary<grcTexture> *local = NULL;
	if (!pgDictionary<grcTexture>::GetCurrent() && sm_AutoTexDict) {
		const char *groupName = T.GetName();
		char baseName[RAGE_MAX_PATH];
		strcpy(baseName,groupName);
		char *ext = strrchr(baseName,'.');
		if(ext)
			*ext = 0;

		local = CreateTextureDictionary(baseName,false);
		if (local)
			pgDictionary<grcTexture>::SetCurrent(m_TextureDictionary = local);
		else
			grcWarningf("Automatic texture dictionary requested but '%s' not found.",baseName);
	}
#endif

	int shaders = T.MatchInt("Shaders");
	InitCount(shaders);
	T.GetDelimiter("{");
	// See which version this type file is
	int vers = 0;
	if ( T.CheckToken("Vers:") ) {
		vers = T.GetInt();
	}
	else {
		Quitf(ERR_GFX_SHADER_TYP,"Your entity.type file is too old, needs to be Vers:1/2 with presets.");
	}

	for (int j=0; j<shaders; j++) {
		char presetName[256];
		int bucket = -1;
		grmShader *shader = grmShaderFactory::GetInstance().Create();
		bool loaded = false;
		if (vers == 2) {
			/*
				Vers: 2
				"T:/rage/art/AnimExpressions/RDR2_BountHunter_e03/materials/rage_diffuse_MediumGray.mtlgeo"
				Template "rage_diffuse"
				Bucket 0
				rage_diffuse 3 (count of parameters in sva, we don't need to know it)
				{
					(contents of .sva file go here)
				} 
			*/
			T.IgnoreToken();		// .mtl / mtlgeo name
			T.MatchToken("Template");
			T.IgnoreToken();		// usually matches the one below, but not always, and the one below is correct.
			if (T.CheckToken("Bucket"))
				bucket = T.GetInt();
			T.GetToken(presetName,sizeof(presetName));
			T.GetInt();				// parameter count, we don't need it
			T.MatchToken("{");
				loaded = shader->Load(presetName, &T);
				if (bucket != -1)
					shader->SetDrawBucket(bucket);
			T.MatchToken("}");
		}
		else if (vers == 1) {
			// default.sps 1 VisibleMesh_default_000.sva
			T.GetToken(presetName,sizeof(presetName));
			if (T.GetInt() == 1) {
				char svaName[256];
				T.GetToken(svaName,sizeof(svaName));
				fiStream *svaStream = ASSET.Open(svaName,"sva");
				if (svaStream) {
					if(svaStream->Size() == 0) 
					{
						grcWarningf("Invalid SVA file '%s'!", svaName);
					}
					else
					{
						fiTokenizer svaTokenizer;
						svaTokenizer.Init(svaName,svaStream);
						loaded = shader->Load(presetName,&svaTokenizer);
					}
					svaStream->Close();
				}
				else
					loaded = shader->Load(presetName);
			}
			else
				loaded = shader->Load(presetName);
		}
		if (!loaded) {
			if (!shader->Load("default"))
				Errorf("Shader '%s' doesn't exist, and cannot find a fallback for it.",presetName);
			else
				Warningf("Shader '%s' doesn't exist, but was able to find a fallback for it.",presetName);
		}

		Add(shader);
	}
	T.GetDelimiter("}");

#if !__FINAL
	if (local) {
		pgDictionary<grcTexture>::SetCurrent(NULL);
	}
#endif

	return true;
}


#if USE_MTL_SYSTEM
bool grmShaderGroup::Load(const rageShaderMaterialGroup & mtlGroup) {
	int shaders = mtlGroup.GetMaterialCount();
	InitCount(shaders);
	
	for (int j=0; j<shaders; j++) {
			const rageShaderMaterial & mtl = mtlGroup.GetMaterial(j);
			grmShader *shader = grmShaderFactory::GetInstance().Create();
			if (!shader->Load(mtl.GetShaderName()))
			{
				grcErrorf("Could not load the material %s", mtlGroup.GetMaterialFileName(j));
				return false;
			}
			Add(shader);
	}
	return true;
}
#else
bool grmShaderGroup::Load(const rageShaderMaterialGroup & ) {
	return false;
}
#endif

void grmShaderGroup::SaveAllMtlFiles() const
{
#if USE_MTL_SYSTEM && 0
	for (int i=0; i<GetCount(); i++) {
		if (m_Shaders[i]->GetMtlFileName())
		{
			m_Shaders[i]->SaveMtlFile();
		}
	}
#endif
}

void grmShaderGroup::ReloadAllMtlFiles() const
{
#if USE_MTL_SYSTEM && 0
	for (int i=0; i<GetCount(); i++) {
		if (m_Shaders[i]->GetMtlFileName())
		{
			m_Shaders[i]->ReloadMtlFile();
		}
	}
#endif
}

void grmShaderGroup::SaveAllMtlTypes() const
{
#if USE_MTL_SYSTEM && 0
	for (int i=0; i<GetCount(); i++) {
		if (m_Shaders[i]->GetMtlFileName())
		{
			m_Shaders[i]->SaveMtlType();
		}
	}
#endif
}

void grmShaderGroup::ReloadAllMtlTypes() const
{
#if USE_MTL_SYSTEM && 0
	for (int i=0; i<GetCount(); i++) {
		if (m_Shaders[i]->GetMtlFileName())
		{
			m_Shaders[i]->ReloadMtlType();
		}
	}
#endif
}

#if __BANK
void grmShaderGroup::AddWidgets(bkBank &B) {
#if USE_MTL_SYSTEM
	//do we have any mtls in this shader group
	for (int i=0; i<GetCount(); i++) {
		if (m_Shaders[i]->GetMtlFileName())
		{
			//yes mtls so add the buttons then exit the loop
			B.AddButton("Save All MTL Materials", datCallback(MFA(grmShaderGroup::SaveAllMtlFiles), this));
			B.AddButton("Reload All MTL Materials", datCallback(MFA(grmShaderGroup::ReloadAllMtlFiles), this));
			B.AddButton("Save All MTL Types", datCallback(MFA(grmShaderGroup::SaveAllMtlTypes), this));
			B.AddButton("Reload All MTL Types", datCallback(MFA(grmShaderGroup::ReloadAllMtlTypes), this));
			break;
		}
	}
#endif
	for (int i=0; i<GetCount(); i++) {
		if (m_Shaders[i]->GetMtlFileName())
		{
			B.PushGroup(fiAssetManager::FileName(m_Shaders[i]->GetMtlFileName()),false);
		}
		else
		{
			B.PushGroup(fiAssetManager::FileName(m_Shaders[i]->GetName()),false);
		}
		m_Shaders[i]->AddWidgets(B);
		B.PopGroup();
	}
}
#endif


grmShaderGroup::grmShaderGroup(datResource &rsc) : m_Shaders(rsc), m_ShaderGroupVars(rsc,true) {
	if (m_TextureDictionary)
		m_TextureDictionary->Push();

	Assert(datResource_IsDefragmentation || !m_ShaderDatas);

	Assertf(datResource_IsDefragmentation || m_ShaderGroupVars.GetCount() == 0,"Resourced shadergroupvar won't work right until resource is rebuilt.");

	if (m_Shaders.GetCount() > 0) {
#if MAINTAIN_SHADERDATAS
		if (!datResource_IsDefragmentation)
			m_ShaderDatas = rage_aligned_new(16) grcInstanceData::Entry*[m_Shaders.GetCount()];
		else
			rsc.PointerFixup(m_ShaderDatas);
#endif
		for (int i=0; i<m_Shaders.GetCount(); i++) {
			grmShaderFactory::GetInstance().PlaceShader(rsc,&m_Shaders[i].ptr,1);
#if MAINTAIN_SHADERDATAS
			m_ShaderDatas[i] = m_Shaders[i].ptr->GetInstanceData().Entries;
#endif
		}
	}

	if (m_TextureDictionary)
		m_TextureDictionary->Pop();

	ScanForInstancedShaders();
}

grmShaderGroup::grmShaderGroup(const grmShaderGroup* copyme)
#if !MAINTAIN_SHADERDATAS
	: m_ShaderDatas(NULL)
#endif
{
	Assert( copyme );
	const int numShaders = copyme->GetCount();
	if ( numShaders > 0 ) 
	{
		m_Shaders.Init(numShaders, numShaders);
#if MAINTAIN_SHADERDATAS
		this->m_ShaderDatas = rage_aligned_new(16) grcInstanceData::Entry*[m_Shaders.GetCount()];
#endif
		for (int i=0; i<numShaders; i++)
		{
			this->m_Shaders[i] = rage_aligned_new(16) grmShader( copyme->GetShaderPtr(i) );
#if MAINTAIN_SHADERDATAS
			this->m_ShaderDatas[i] = this->m_Shaders[i].ptr->GetInstanceData().Entries;
#endif
		}
	}

	const int shaderGroupVarsCount = copyme->GetShaderGroupVarCount();
	if( shaderGroupVarsCount > 0 )
	{
		m_ShaderGroupVars.Resize( shaderGroupVarsCount );
		const atArray<grcEffectVarEntry>& shaderGroupVars = copyme->GetShaderGroupVars();
		for( int i = 0; i < shaderGroupVarsCount; ++i )
		{
			m_ShaderGroupVars[i].Clone( shaderGroupVars[i] );
		}
	}	

	Assert( copyme->GetShaderGroupVarCount() == m_ShaderGroupVars.GetCount() );

	m_TextureDictionary = NULL;
	m_ContainerSizeQW = copyme->GetContainerSizeRaw();
	m_HasInstancedShader = copyme->HasInstancedShaders();
}


void grmShaderGroup::UpdateTextureReferences() {
	// for (int i=0; i<m_Shaders.GetCount(); i++)
	//	m_Shaders[i]->UpdateTextureReferences();
}


void grmShaderGroup::SetTexDict(pgDictionary<grcTexture> *newDict) {
	pgDictionary<grcTexture> *oldDict = m_TextureDictionary;
	grcAssertf(newDict, "Can't set texdict to NULL");

	if (oldDict == NULL)
	{
		newDict->AddRef();
		m_TextureDictionary = newDict;
		return;
	}

	grcAssertf(newDict->GetCount() == oldDict->GetCount(), "Dictionary counts don't match. Old: %d, New: %d", newDict->GetCount(), oldDict->GetCount());
	// for each texture in the original dictionary, locate all references to it
	// in any shader in this shadergroup.
	for (int i=0; i<oldDict->GetCount(); i++) {
		u32 texCode = oldDict->GetCode(i);
		grcTexture *oldTex = oldDict->GetEntry(i);
		grcTexture *newTex = newDict->Lookup(texCode);
		grcAssertf(newTex, "SetTexDict: The new texture dictionary does not have the texture '%s' in it.", oldTex->GetName());

		if (newTex) {
			// Iterate over all shaders...
			int shaderCount = m_Shaders.GetCount();
			for (int j=0; j<shaderCount; j++) {
				// Iterate through all variables...
				grcInstanceData &instanceData = m_Shaders[j]->GetInstanceData();
				grcEffect &effect = instanceData.GetBasis();
				int count = instanceData.GetCount();
				for (int k=0; k<count; k++) {
					if (instanceData.IsTexture(k) && instanceData.Data()[k].Texture && instanceData.Data()[k].Texture->GetReference() == oldTex) {
						// We need to call AddRef() on the existing texture or else the
						// system will try to delete it - which would be a bad idea, given that
						// it is probably inside a texture dictionary.
						instanceData.Data()[k].Texture->AddRef();
						// Rebind the dictionary reference
						effect.SetVar(instanceData, effect.GetVarByIndex(k), newTex);

						// Likewise, we should decrement the new texture since it already had a
						// reference before. If we don't, we might be end up with a dangling
						// reference.
						newTex->Release();
					}
				}
			}
		}
	}
	newDict->AddRef();
	m_TextureDictionary->Release();
	m_TextureDictionary = newDict;
}

#if __RESOURCECOMPILER
// Callback pointer.
grmShaderGroup::CustomCreateTextureDictionaryFuncType grmShaderGroup::ms_CustomCreateTextureDictionaryCallback = NULL;

// Method to set the texture dictionary custom create callback; for texture pipeline support.
void grmShaderGroup::SetCreateTextureDictionaryCallback( CustomCreateTextureDictionaryFuncType cb )
{
	grmShaderGroup::ms_CustomCreateTextureDictionaryCallback = cb;
}

// Invoke the texture dictionary custom create callback.
pgDictionary<grcTexture>* grmShaderGroup::RunCreateTextureDictionaryCallback( 
	const char **inputs,int inputCount,bool stripPath, grcTextureFactory::TextureCreateParams** params )
{
	Assertf( NULL != ms_CustomCreateTextureDictionaryCallback, 
		"Attempting to run invalid grmShaderGroup CreateTextureDictionary callback.  Aborting." );
	if ( !ms_CustomCreateTextureDictionaryCallback )
	{
		Errorf( "Attempting to run invalid grmShaderGroup CreateTextureDictionary callback.  Aborting." );
		return ( NULL );
	}
	return ( ms_CustomCreateTextureDictionaryCallback( inputs, inputCount, stripPath, params ) );
}
#endif // __RESOURCECOMPILER

void grmShaderGroup::ScanForInstancedShaders()
{
	m_HasInstancedShader = false;
	int count = GetCount();
	for(int i = 0; i < count; ++i)
	{
		if(GetShader(i).IsInstanced())
		{
			m_HasInstancedShader = true;
			break;	//Only need to find a single instanced shader to go on.
		}
	}
}

IMPLEMENT_PLACE(grmShaderGroup)

#if __DECLARESTRUCT
datSwapper_ENUM(grcEffectVar)

void grcEffectVarPair::DeclareStruct(class datTypeStruct &s)
{
	STRUCT_BEGIN(grcEffectVarPair);
	STRUCT_FIELD(index);
	STRUCT_FIELD(var);
	STRUCT_END();
}

void grcEffectVarEntry::DeclareStruct(class datTypeStruct &s)
{
	STRUCT_BEGIN(grcEffectVarEntry);
	STRUCT_FIELD(Hash);
	STRUCT_FIELD(Pairs);
	STRUCT_END();
}

void grmShaderGroup::DeclareStruct(datTypeStruct &s) {
	STRUCT_BEGIN(grmShaderGroup);
	STRUCT_FIELD(m_TextureDictionary);
	STRUCT_FIELD(m_Shaders);
	STRUCT_FIELD(m_ShaderGroupVars);
	STRUCT_FIELD(m_ContainerSizeQW);
	STRUCT_IGNORE(m_HasInstancedShader);
	STRUCT_IGNORE(pad0);
	STRUCT_IGNORE(m_ShaderDatas);
	STRUCT_END();
}
#endif

}	// namespace rage
