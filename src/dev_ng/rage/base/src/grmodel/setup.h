//
// grmodel/setup.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef GRMODEL_SETUP_H
#define GRMODEL_SETUP_H

#include "grcore/setup.h"

namespace rage {

//
// PURPOSE
//	Setup class for any testers or game code that uses grModel.
// SEE ALSO
//	grcSetup
// <FLAG Component>
//
class grmSetup : public grcSetup {
public:
	// PURPOSE:	Creates default texture, model, and shader factories.
	void CreateDefaultFactories();

	// PURPOSE: Destroys all current texture, model, and shader factories.
	//			Even if you created the factories yourself, you can still
	//			use this function to clean everything up.
	void DestroyFactories();

	void BeginUpdate();
};

}	// namespace rage

#endif
