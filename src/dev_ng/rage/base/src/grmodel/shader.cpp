//
// grmodel/shader.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "shader.h"
#include "grmodel_config.h"
#include "grcore/channel.h"

#include "model.h"
#include "shaderfx.h"
#include "shadergroup.h"

#include "geometry.h"

#if __XENON
#include "grcore/vertexbuffer.h"		// For Optimize
#endif

#include "bank/bank.h"
#include "data/callback.h"
#include "data/resource.h"
#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"
#include "grcore/config.h"
#include "grcore/device.h"
#include "grcore/light.h"
#include "grcore/texturereference.h"
#include "grcore/quads.h"
#include "grcore/effect_config.h"
#include "grcore/channel.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "grprofile/pix.h"
#include "vector/matrix44.h"

#if GRCORE_ON_SPU
#include "grcore/wrapper_gcm.h"
#include "grmodelspu.h"
#endif

#if USE_MTL_SYSTEM
#include "rageShaderMaterial/rageShaderMaterial.h"
#include "rageShaderMaterial/rageShaderMaterialTemplate.h"
#include "shaderMaterial/shaderMaterialGeoParamValue.h"
#endif
#include "file/remote.h"

#if __D3D
#include "system/xtl.h"
#include "system/d3d9.h"
#endif

#include <stdarg.h>

#define WARN_ON_MISSING_TECHNIQUE 0

namespace rage {

#if __ASSERT
	extern DECLARE_MTR_THREAD bool g_bIgnoreNullTextureOnBind;

// Only use the null texture check on bind when running with WARN_ON_MISSING_TECHNIQUE
// since it tends to report quite a few false positives
#if WARN_ON_MISSING_TECHNIQUE
	bool g_bDoNullTextureOnBindCheck = true;
#else
	bool g_bDoNullTextureOnBindCheck = false;
#endif

#endif // __ASSERT
// grmShader* grmShader::sm_First;

// static global default draw bucket
int grmShader::sm_DefaultDrawBucket = 0;

DECLARE_MTR_THREAD int grmShader::sm_MaxPasses = 8;

#if RAGE_INSTANCED_TECH
DECLARE_MTR_THREAD int grmShader::sm_InstancedTechOffset = 0;
#endif

#if __DEV || __BANK
static s32 s_ToolTechniqueGroup = -1;
#endif
static s32 s_BlitTechniqueGroup = -1;


grmShader::grmShader() { }

grmShader::grmShader(const grmShader* copyme)
{
	Assert( copyme );
	copyme->m_EffectInstance.Clone( m_EffectInstance );
}

void grmShader::InitClass() {
	// Register "default" and "unlit" technique groups
	ASSERT_ONLY(s32 unlitTechniqueGroup =) RegisterTechniqueGroup("unlit");
	ASSERT_ONLY(s32 defaultTechniqueGroup =) RegisterTechniqueGroup("default");
	s_BlitTechniqueGroup = RegisterTechniqueGroup("blit");
#if __DEV || __BANK
	s_ToolTechniqueGroup = RegisterTechniqueGroup("tool");
#endif
	grcAssertf(unlitTechniqueGroup == 0, "Unlit technique group index (now %d) must be 0", unlitTechniqueGroup);
	grcAssertf(defaultTechniqueGroup == 1, "Default technique group index (now %d) must be 1", defaultTechniqueGroup);

	// Set this explicitly so SPU gets it too
	SetForcedTechniqueGroupId(-1);
}


void grmShader::ShutdownClass() {
#if __DEV || __BANK
	s_ToolTechniqueGroup = -1;
#endif
	s_BlitTechniqueGroup = -1;

	grcEffect::ShutdownClass();
}

grmShader::~grmShader() {
}


void grmShader::ReloadAll() {
	grcDisplayf("Sorry, grmShader::ReloadAll doesn't work any longer.");
	/* grmShader *i = sm_First;
	while (i) {
		i->Reload();
		i = i->m_Next;
	} */
}


const char *grmShader::sm_DrawBucketNames[32];


grmShader::grmShader(datResource& rsc) : m_EffectInstance(rsc) 
{
}


void grmShader::Place(grmShader *that,datResource &rsc) {
	grmShaderFactoryStandard::GetInstance().PlaceShader(rsc,&that,1);
}

extern s32 s_BlitTechniqueGroup;

void grmShader::TWODBlit(float x1, float y1, float x2, float y2, float z, float u1, float v1, float u2, float v2, Color32 color,grcEffectTechnique tech) const 
{
	// this is a screen blit function ... so make sure it does not go over the range of -1..1
#if 0 // if we don't run this code, we can let the hardware do the clipping
	grcAssertf( (x1 >= -1.0f && x1 <= 1.0f) &&
			(x2 >= -1.0f && x2 <= 1.0f) &&
			(y1 >= -1.0f && y1 <= 1.0f) &&
			(y2 >= -1.0f && y2 <= 1.0f), "Out of bounds. Pt1: (%f,%f), Pt2: (%f,%f)", x1,y1,x2,y2);
#endif

	// Could handle this by not replacing the forced tech group if it's already set...
	int prevForced = -1;
	if (sm_ForcedTechniqueGroupId == -1)
		sm_ForcedTechniqueGroupId = s_BlitTechniqueGroup;
	else {
		if (!tech)
			grcWarningf("Shader %s is probably using wrong technique",GetName());
		prevForced = sm_ForcedTechniqueGroupId;
	}
	int passCount = BeginDraw(grmShader::RMC_DRAW,true,tech);
	sm_ForcedTechniqueGroupId = prevForced;
	for (int i = 0; i < passCount; ++i) 
	{
		Bind(i);
		
		// this is a full-screen blit: grcDrawQuadf(-1.0,1.0,1.0,-1.0,z,u1,v1,u2,v2,color);
		grcDrawSingleQuadf(x1,y1,x2,y2,z,u1,v1,u2,v2,color);
		UnBind();
	}
	EndDraw();
}

//
// Blitter to blit into a face of a cube map
// You might use this to run filters on a cube map like blur filters etc.
//
void grmShader::BlitIntoCubeMapFace(grcCubeFace face, 
									grcEffectTechnique technique)
{
	if ( BeginDraw(grmShader::RMC_DRAW, true, technique) )
	{
		Bind(0);
		grcBegin(drawTris, 6);

		if(face == grcNegativeZ)
		{
			// -Z
			grcVertex3f(-1.0f,  1.0f, -1.0f);
			grcVertex3f( 1.0f,  1.0f, -1.0f);
			grcVertex3f(-1.0f, -1.0f, -1.0f);
			grcVertex3f( 1.0f,  1.0f, -1.0f);
			grcVertex3f( 1.0f, -1.0f, -1.0f);
			grcVertex3f(-1.0f, -1.0f, -1.0f);
		}
		else if(face == grcPositiveZ)
		{
			// +Z
			grcVertex3f(-1.0f,  1.0f, 1.0f);
			grcVertex3f( 1.0f,  1.0f, 1.0f);
			grcVertex3f(-1.0f, -1.0f, 1.0f);
			grcVertex3f( 1.0f,  1.0f, 1.0f);
			grcVertex3f( 1.0f, -1.0f, 1.0f);
			grcVertex3f(-1.0f, -1.0f, 1.0f);
		}
		else if(face == grcNegativeY)
		{
			// -Y
			grcVertex3f(-1.0f, -1.0f, -1.0f);
			grcVertex3f( 1.0f, -1.0f, -1.0f);
			grcVertex3f(-1.0f, -1.0f,  1.0f);
			grcVertex3f( 1.0f, -1.0f, -1.0f);
			grcVertex3f( 1.0f, -1.0f,  1.0f);
			grcVertex3f(-1.0f, -1.0f,  1.0f);
		}
		else if(face == grcPositiveY)
		{
			// +Y
			grcVertex3f(-1.0f, 1.0f, -1.0f);
			grcVertex3f( 1.0f, 1.0f, -1.0f);
			grcVertex3f(-1.0f, 1.0f,  1.0f);
			grcVertex3f( 1.0f, 1.0f, -1.0f);
			grcVertex3f( 1.0f, 1.0f,  1.0f);
			grcVertex3f(-1.0f, 1.0f,  1.0f);
		}
		else if(face == grcNegativeX)
		{
			// -X
			grcVertex3f(-1.0f,  1.0f,  1.0f);
			grcVertex3f(-1.0f,  1.0f, -1.0f);
			grcVertex3f(-1.0f, -1.0f,  1.0f);
			grcVertex3f(-1.0f,  1.0f, -1.0f);
			grcVertex3f(-1.0f, -1.0f, -1.0f);
			grcVertex3f(-1.0f, -1.0f,  1.0f);
		}
		else if(face == grcPositiveX)
		{
			// +X
			grcVertex3f(1.0f,  1.0f,  1.0f);
			grcVertex3f(1.0f,  1.0f, -1.0f);
			grcVertex3f(1.0f, -1.0f,  1.0f);
			grcVertex3f(1.0f,  1.0f, -1.0f);
			grcVertex3f(1.0f, -1.0f, -1.0f);
			grcVertex3f(1.0f, -1.0f,  1.0f);
		}

		grcEnd();

		UnBind();
		EndDraw();
	}
}


DECLARE_MTR_THREAD int grmShader::sm_ForcedTechniqueGroupId = -1;

void grmShader::SetForcedTechniqueGroupId(int groupId) {
	grcAssertf(groupId < grcEffect::GetTechniqueGroupCount(), "group Id (%d) out of range [0, %d]", groupId, grcEffect::GetTechniqueGroupCount()-1);
#if GRCORE_ON_SPU
	SPU_SIMPLE_COMMAND(grmShaderFx__SetForcedTechniqueGroupId,groupId);
#endif
	sm_ForcedTechniqueGroupId = groupId;
}

void grmShader::Bind(int pass) const {
	m_EffectInstance.GetBasis().BeginPass(pass,m_EffectInstance);
}

void grmShader::IncrementalBind(int pass) const {
	m_EffectInstance.GetBasis().BeginPass(pass,m_EffectInstance);
}

void grmShader::UnBind() const {
	m_EffectInstance.GetBasis().EndPass();
}


#if __PS3
u8 grmShader::GetInitialRegisterCount(int pass /*= 0*/) const {
	return m_EffectInstance.GetBasis().GetInitialRegisterCount(pass);
}

void grmShader::SetInitialRegisterCount(const char *technique, int pass, int registerCount)
{
	grcEffect& effect = m_EffectInstance.GetBasis();
	grcEffectTechnique tech = effect.LookupTechnique(technique,true);
	if( tech != grcetNONE )
	{
		effect.SetInitialRegisterCount(tech, pass, registerCount);
	}
}

#endif


int grmShader::GetTechniqueGroupId() {
	if (sm_ForcedTechniqueGroupId != -1)
		return sm_ForcedTechniqueGroupId;
#if (__DEV || __BANK)
	else if (grcLightState::GetToolMode())
		return s_ToolTechniqueGroup;
#endif
	else 
		return grcLightState::IsEnabled();
}

#if __ASSERT
static DECLARE_MTR_THREAD const grmShader *s_BeginDrawn;
#endif

RETURNCHECK(int) grmShader::BeginDraw(eDrawType type, bool restoreState,grcEffectTechnique techOverride) const {

	grcAssertf(!s_BeginDrawn,"Shader %p:%s called BeginDraw but never called EndDraw.",s_BeginDrawn, s_BeginDrawn->GetName());
	ASSERT_ONLY(s_BeginDrawn = this);

	int groupId = grmShader::GetTechniqueGroupId();

#if RAGE_INSTANCED_TECH
	type = static_cast<grmShader::eDrawType>(type+sm_InstancedTechOffset);
#endif

#if ENABLE_PIX_TAGGING
	const grcEffectTechnique tech = techOverride? techOverride : GetTechniqueHandle(groupId,type);
	if( tech && PIXIsGPUCapturingCached() ) // Only add labels for GPU captures. These high frequency labels can take up 10-15% of the render thread time.
	{
		const char *shaderName = GetName();
		const char *techName = NULL;
#if EFFECT_PRESERVE_STRINGS
		techName = GetTechniqueName(tech);
#endif
		PIXBeginN(0, "%s - %s",shaderName,techName ? techName : "N/A");
	}
#endif // ENABLE_PIX_TAGGING

	// NOTE: This function might return zero now if handed an invalid technique handle.
	// EndDraw knows to ignore this properly, but calling code needs to know to not draw anything.
	int result = m_EffectInstance.GetBasis().BeginDraw(techOverride? techOverride : GetTechniqueHandle(groupId,type),restoreState);
#if WARN_ON_MISSING_TECHNIQUE && !__RESOURCECOMPILER
	if( result == 0 )
	{
		const char *shaderName = GetName();
		char techName[64] = "N/A";
#if EFFECT_PRESERVE_STRINGS
		if( tech )
			strcpy(techName, GetTechniqueName(tech));
		else
			formatf(techName, sizeof(techName) - 1, "techGroupId=%s - %s", grcEffect::GetTechniqueGroupName(groupId, (grcEffect::eDrawType)type), type == RMC_DRAWSKINNED ? "drawskinned" : "draw");
#endif
		static atMap<atString,bool> s_warned;
		atString shaderTechName(shaderName);
		shaderTechName += techName;

		if (s_warned.Access(shaderTechName) == NULL)
		{
			s_warned[shaderTechName] = true;
			Assertf(false,"Missing Technique %s - %s",shaderName,techName);
		}
	}
#endif // WARN_ON_MISSING_TECHNIQUE && !__RESOURCECOMPILER
#if DRAWABLE_STATS
	if(g_pCurrentStatsBucket && result==0)
	{
		g_pCurrentStatsBucket->MissingTechnique++;
	}
#endif
	return result;
}


void grmShader::EndDraw() const {
	grcAssertf(s_BeginDrawn,"grmShader::EndDraw called without BeginDraw first.");
	ASSERT_ONLY(s_BeginDrawn = NULL);

#if ENABLE_PIX_TAGGING
	if (grcEffect::GetCurrentTechnique() && PIXIsGPUCapturingCached())
		PIXEnd();
#endif

	m_EffectInstance.GetBasis().EndDraw();
}


#if COMMAND_BUFFER_SUPPORT && __XENON && !HACK_GTA4
	// !HACK_GTA4:			fix for Jimmy's BS#5701
	// Jimmy 360:			this goodness breaks smashables rendered with customized vertex formats and im custom VBs
	// possible solution:	make secretInfo creation dependent on some flag in grmShader? (we don't want this to happen only in few selected shaders)
void grmShader::Optimize(const grmModel& model,int geomIdx) {
	// Currently all techniques are assumed to use the same model, which is obviously suboptimal.
	grmGeometry& geom = model.GetGeometry(geomIdx);
	grcVertexBuffer* vertexBuffer = geom.GetVertexBuffer();
	If_Assert(vertexBuffer)
	{
		m_EffectInstance.GetBasis().SetDeclaration(geom.GetDecl(),vertexBuffer->GetVertexStride(),model.GetSkinFlag());
	}
}
#else
void grmShader::Optimize(const grmModel&,int) { }
#endif

void grmShader::CopyPassData(grcEffectTechnique techHandle, int passIndex, grmShader *const source) const
{
	m_EffectInstance.GetBasis().CopyPassData(techHandle, passIndex, m_EffectInstance, source->m_EffectInstance);
}

#if RAGE_INSTANCED_TECH
void grmShader::SetInstancedDrawMode(bool bInstanced)
{
	if (bInstanced)
		sm_InstancedTechOffset = RMC_DRAW_INSTANCED;
	else
		sm_InstancedTechOffset = 0;
}
#endif

#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES

void grmShader::Draw(const grmModel& model,int geom,int /*lod*/,bool restoreState) const {
#if __BANK
	// See if we need to flash this geometry
	if (IsFlashing() || IsDisabled())
		return;
#endif // __BANK

	ASSERT_ONLY(g_bIgnoreNullTextureOnBind = true;)

	int numPasses = BeginDraw(RMC_DRAW, restoreState);

	for (int i=0; i<numPasses; i++) {
		Bind(i);
		model.Draw(geom);
		UnBind();
	}
	EndDraw();

	ASSERT_ONLY(g_bIgnoreNullTextureOnBind = false;)
}


void grmShader::DrawSkinned(const grmModel &model,int geom,const grmMatrixSet &ms,int /*lod*/,bool restoreState) const {
#if __BANK
	// See if we need to flash this geometry
	if (IsFlashing() || IsDisabled())
		return;
#endif // __BANK

	ASSERT_ONLY(g_bIgnoreNullTextureOnBind = true;)

#if __D3D
	// grmGeometry::ResetPacketMatrices(true,mtxCount);
#endif
	rage::grmShader::eDrawType drawMode = RMC_DRAWSKINNED;
#if __PPU
	// Edge skinned models actually use non skinned shaders since skinning
	// is done on the SPU.
	if (model.GetGeometry(geom).GetType() == grmGeometry::GEOMETRYEDGE)
		drawMode = RMC_DRAW;
#endif

	int numPasses = BeginDraw(drawMode, restoreState);

	for (int i=0; i<numPasses; i++) {
		Bind(i);
		model.DrawSkinned(geom,ms);
		UnBind();
	}
	EndDraw();
#if __D3D
	// grmGeometry::ResetPacketMatrices(false);
#endif

	ASSERT_ONLY(g_bIgnoreNullTextureOnBind = false;)
}

#else // !RAGE_SUPPORT_TESSELLATION_TECHNIQUES

void grmShader::DrawInternal(eDrawType type, const grmModel& model, int geom, bool restoreState) const 
{
#if __BANK
	// See if we need to flash this geometry
	if (IsFlashing() || IsDisabled())
		return;
#endif // __BANK

	ASSERT_ONLY(g_bIgnoreNullTextureOnBind = true;)

	int numPasses = BeginDraw(type, restoreState);

	for (int i=0; i<numPasses; i++) {
		Bind(i);
		model.Draw(geom);
		UnBind();
	}
	EndDraw();

	ASSERT_ONLY(g_bIgnoreNullTextureOnBind = false;)
}

void grmShader::DrawSkinnedInternal(eDrawType type, const grmModel &model,int geom, const grmMatrixSet &ms, bool restoreState) const 
{
#if __BANK
	// See if we need to flash this geometry
	if (IsFlashing() || IsDisabled())
		return;
#endif // __BANK

	ASSERT_ONLY(g_bIgnoreNullTextureOnBind = true;)

	int numPasses = BeginDraw(type, restoreState);

	for (int i=0; i<numPasses; i++) {
		Bind(i);
		model.DrawSkinned(geom,ms);
		UnBind();
	}
	EndDraw();

	ASSERT_ONLY(g_bIgnoreNullTextureOnBind = false;)
}

void grmShader::Draw(const grmModel& model,int geom,int /*lod*/,bool restoreState) const 
{
	DrawInternal(RMC_DRAW, model, geom, restoreState);
}

void grmShader::DrawSkinned(const grmModel &model,int geom,const grmMatrixSet &ms,int /*lod*/,bool restoreState) const 
{
	DrawSkinnedInternal(RMC_DRAWSKINNED, model, geom, ms, restoreState);
}
#endif // !RAGE_SUPPORT_TESSELLATION_TECHNIQUES

bool grmShader::Load(const char *presetName,fiTokenizer *T,bool fallback) {
	const char *ext = strrchr(presetName,'.');
	if (ext && !strcmp(ext,".sps")) {
		grcMaterialLibrary *curMtlLib = static_cast<grcMaterialLibrary*>(grcMaterialLibrary::GetCurrent());
		grcAssertf(curMtlLib,"No active material library, trying to open preset '%s' (this usually means your -shaderpath or -shaderdbpath is wrong, or T:/rage/assets/tune/shaders/db/preload.list needs a forced update)",presetName);

		m_EffectInstance.Material = curMtlLib->Lookup(presetName);
		if (!m_EffectInstance.Material) {
			// Didn't work, try without the extension
			char acPresetNameWithoutExtension[32];
			strncpy(acPresetNameWithoutExtension, presetName, (ext - presetName));
			acPresetNameWithoutExtension[ext - presetName] = '\0';
			m_EffectInstance.Material = curMtlLib->Lookup(acPresetNameWithoutExtension);
			if (!m_EffectInstance.Material) {
				grcErrorf("Unable to find preset '%s' in any active material library.",presetName);
				return false;
			}
		}
		m_EffectInstance.Material->Clone(m_EffectInstance);
	}
	else {
		m_EffectInstance.Material = NULL;
		m_EffectInstance.Basis = fallback ? grcEffect::CreateWithFallback(presetName) : grcEffect::Create(presetName);
		if (!m_EffectInstance.Basis)
			return false;
		m_EffectInstance.Basis->Clone(m_EffectInstance);
	}
	if (T)
		return m_EffectInstance.Load(T->GetName(),*T,false);
	else
		return true;
}


#if __BANK
void grmShader::AddWidgets(bkBank &B) {
	m_EffectInstance.AddWidgets(B);
	B.AddToggle("Flash Instances", &m_EffectInstance.UserFlags, FLAG_FLASH_INSTANCES);
	B.AddToggle("Disable Instances", &m_EffectInstance.UserFlags, FLAG_DISABLE_INSTANCES);
	B.AddToggle("Force Wireframe Draw", &m_EffectInstance.UserFlags, FLAG_DRAW_WIREFRAME);
}
#endif


#if __DECLARESTRUCT
void grmShader::DeclareStruct(class datTypeStruct &s) {
	STRUCT_BEGIN(grmShader);
	STRUCT_FIELD(m_EffectInstance);
	STRUCT_END();
}
#endif

#if __PPU
RETURNCHECK(int) grmShader::RecordBeginDraw(eDrawType type, bool restoreState,grcEffectTechnique techOverride) const {
	// We know that nothing really gets recorded here, but higher-level code doesn't need to know that.
	// Also need to avoid the debug context push/pop being recorded as an SPU command.
	int groupId = grmShader::GetTechniqueGroupId();
	return m_EffectInstance.GetBasis().BeginDraw(techOverride? techOverride : GetTechniqueHandle(groupId,type),restoreState);
}

void grmShader::RecordEndDraw() const {
	// We know that nothing really gets recorded here, but higher-level code doesn't need to know that.
	m_EffectInstance.GetBasis().EndDraw();
}


void grmShader::RecordBind(int pass) const {
	m_EffectInstance.GetBasis().RecordBeginPass(pass,m_EffectInstance);
}


void grmShader::RecordUnBind() const {
	m_EffectInstance.GetBasis().RecordEndPass();
}
#endif	 // __PPU




}	// namespace rage
