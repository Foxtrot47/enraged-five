//  
// grmodel/geometry.cpp 
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
//

#include "geometry.h"
#include "matrixset.h"
#include "model.h"
#include "modelfactory.h"
#include "data/resourcehelpers.h"
#include "atl/array_struct.h"
#include "math/amath.h"
#include "grcore/channel.h" 
#include "grcore/device.h"
#include "grcore/edgeExtractgeomspu.h"
#include "grcore/instancebuffer.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/indexbuffer.h"
#include "grprofile/drawmanager.h"
#include "profile/element.h"
#include "system/cache.h"
#include "system/platform.h"
#include "diag/tracker.h"
#include "shaderlib/rage_constants.h"
#include "shaderlib/skinning_method_config.h"

#include "grcore/effect_config.h"

#if __PPU && USE_EDGE
#include "grcore/edge_jobs.h"
#endif // __PPU && USE_EDGE
 
#include "mesh/mesh.h"
#include "mesh/array.h"
#include "atl/bitset.h"

#include "system/alloca.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/performancetimer.h"

#include "vector/matrix44.h"
#include "vector/colors.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"

#include "grmodel/grmodel_config.h"
#include "grmodel/shaderfx.h"
#include "grmodel/shadergroup.h"

#include "grcore/fvfchannels.h"
#include "grmodel/grmodelspu.h"

#if __XENON
#include "system/xtl.h"
#elif __D3D11
#include "system/d3d11.h"
#elif RSG_ORBIS
#include "grcore/gfxcontext_gnm.h"
#endif

#if __DEV
// Debug output text only.
#include "rmcore/LodGroup.h"
#include "rmcore/drawable.h"
#endif // __DEV

#if (__PPU && USE_EDGE) || __RESOURCECOMPILER
#include "edge/libedgegeomtool/libedgegeomtool_wrap.h"
#include "edge/edge_version.h"
#include "grcore/wrapper_gcm.h"

// Dodgy dodgy redefinition-hack, but we get a bunch of conflicts with gcm_wrapper.h
// if we include gcm_tool.h here, which is the proper solution.
#if __RESOURCECOMPILER
namespace cell
{
namespace Gcm
{

struct CellGcmContext;
typedef int32_t (*CellGcmContextCallback)(struct CellGcmContextData*, uint32_t);
struct CellGcmContext
{
	uint32_t *begin;
	uint32_t *end;
	uint32_t *current;
	CellGcmContextCallback callback;
};

} // namespace Gcm
} // namespace cell
#endif // __RESOURCECOMPILER
#endif // (__PPU && USE_EDGE) || __RESOURCECOMPILER

#if __PPU && USE_EDGE
namespace rage {
	extern u16 g_VertexShaderInputs;
	extern spuGcmState s_spuGcmState;
}
#endif // __PPU && USE_EDGE

#if HACK_GTA4_MODELINFOIDX_ON_SPU
	namespace rage	{ extern CGta4DbgSpuInfoStruct	gGta4DbgInfoStruct; }
#endif

namespace rage {

PARAM(edgeoutputmask, "[RAGE] a bit mask containing which attributes we force through Edge");

#define HACK_GTA4_EDGEFIXEDPOINTPOS				(1 && HACK_GTA4)

#if HACK_GTA4_EDGEFIXEDPOINTPOS
//	PARAM(edgefixedpointpos, "[RAGE] a float representing the number of bits for the integer and fractional parts of a fixed point representation of the position attribute eg. 12.4 would be 12 bits for the integer and 4 bits for the fractional part");
//	PARAM(edgefixedpointposauto, "[RAGE] just specify the number of fractional bits. The integer bits are automatically calculated from the size of the geometry");
#else
	PARAM(edgefixedpointpos, "[RAGE] a float representing the number of bits for the integer and fractional parts of a fixed point representation of the position attribute eg. 12.4 would be 12 bits for the integer and 4 bits for the fractional part");
	PARAM(edgefixedpointposauto, "[RAGE] just specify the number of fractional bits. The integer bits are automatically calculated from the size of the geometry");
#endif
PARAM(edgeoutputfloat4pos, "[RAGE] output a float4 position from Edge instead of float3 by default");
PARAM(edgenoindexcomp, "[RAGE] don't use index compression");
PARAM(useshortuvs, "[RAGE] use 16 bit integer UV's");

extern u32 g_AllowVertexBufferVramLocks;

#if __PS3 && SPU_GCM_FIFO >= 2
extern u32 grcCurrentVertexOffsets[grcVertexDeclaration::c_MaxStreams];
#endif // __PS3 && SPU_GCM_FIFO >= 2

int grmGeometry::s_BuildBuffer[2] = { 0, 0 };
int grmGeometry::s_DrawBuffer[2] = { 0, 0 };
int grmGeometry::s_MaxMatricesPerMaterial = SKINNING_COUNT;
#if __ASSERT
bool grmGeometry::s_CheckSecondaryStreamVertexLength = true;
#endif // __ASSERT

int grmGeometry::sm_MultiBufferCount = (RSG_PC ? MAX_VERT_BUFFERS : 3); // number of buffers needed when double buffering - 2 for double, 3 for triple	- triple works best when using multithreaded rendering, to avoid stalls quadruple is needed for multithreaded rendering + predicated tiling (sigh)
int grmGeometry::sm_TotalIndexCount = 0;
int grmGeometry::sm_TotalIndexSize = 0;
int grmGeometry::sm_TotalPrimCount = 0;
int grmGeometry::sm_TotalVertexCount = 0;
int grmGeometry::sm_TotalVertexSize = 0;
int grmGeometry::sm_TotalChannelSize[grcFvf::grcfcCount] = {0};

bool s_UseFastMtxTest = false;
#define MAX_TEST_MTX 512
u16 s_MtxTestPallete[MAX_TEST_MTX]; // this may need to be bigger?
int s_MtxTestCount = -1;
bool s_EnableSkinnedMatrixPreTest = false;

namespace GraphicsStats {
	EXT_PF_COUNTER(grmGeometry_Draw_Calls);
	EXT_PF_COUNTER(grmGeometry_Draw_Vertices);
	EXT_PF_COUNTER(grmGeometry_Draw_Indices);
	EXT_PF_COUNTER(grmGeometry_Draw_Primitives);
}
using namespace GraphicsStats;


EXT_PFD_DECLARE_ITEM( ClothNormals );
EXT_PFD_DECLARE_ITEM( ClothTangents );


grmGeometry::grmGeometry(u8 type)
: m_VtxDecl(0)
, m_Type(type)
{
}

// RETURNS: A pointer to the fvf for this geometry object
const grcFvf* grmGeometry::GetFvf() const {
	grcAssertf(false,"GetFvf not implemented");
	return NULL;
}

// RETURNS: A pointer to the matrix palette for this geometry
const u16* grmGeometry::GetMatrixPalette() const {
	grcAssertf(false,"GetMatrixPalette not implemented");
	return NULL;
}

// RETURNS: the number of matrices in the matrix paletter
u16 grmGeometry::GetMatrixPaletteCount() const {
	grcAssertf(false,"GetMatrixPaletteCount not implemented");
	return 0;
}

void grmGeometry::SetOffsets(grcVertexBuffer*) {
	grcAssertf(false,"SetOffsets not implemented");
}

void grmGeometry::SetBlendHeaders(u32, grmGeometryEdgeBlendHeader**) {
	grcAssertf(false,"SetBlendHeaders not implemented");
}

const grcVertexBuffer* grmGeometry::GetVertexBuffer(bool) const {
	grcAssertf(false,"GetVertexBuffer not implemented");
	return NULL;
}

grcVertexBuffer* grmGeometry::GetVertexBuffer(bool) {
	grcAssertf(false,"GetVertexBuffer not implemented");
	return NULL;
}

const grcVertexBuffer* grmGeometry::GetVertexBufferByIndex(unsigned) const {
	grcAssertf(false,"GetVertexBufferByIndex not implemented");
	return NULL;
}

grcVertexBuffer* grmGeometry::GetVertexBufferByIndex(unsigned) {
	grcAssertf(false,"GetVertexBufferByIndex not implemented");
	return NULL;
}

void grmGeometry::SetIndexBuffersPtr(grcIndexBuffer*) {
	grcAssertf(false,"SetIndexBuffersPtr not implemented");
}

// PURPOSE:	Replace the current vertex buffer with a new one
void grmGeometry::SetVertexBuffer(grcVertexBuffer *) {
	grcAssertf(false,"SetVertexBuffer not implemented");
}

void grmGeometry::ReplaceVertexBuffers(const atFixedArray<grcVertexBuffer *,MAX_VERT_BUFFERS> &) {
	grcAssertf(false,"ReplaceVertexBuffers not implemented");
}

// RETURNS: A pointer to the index buffer for this geometry object
const grcIndexBuffer* grmGeometry::GetIndexBuffer(bool ) const {
	grcAssertf(false,"GetIndexBuffer not implemented");
	return NULL;
}

// RETURNS: A pointer to the index buffer for this geometry object
grcIndexBuffer* grmGeometry::GetIndexBuffer(bool ) {
	grcAssertf(false,"GetIndexBuffer not implemented");
	return NULL;
}

// RETURNS: A pointer to the index buffer for this geometry object
const grcIndexBuffer* grmGeometry::GetIndexBufferByIndex(unsigned) const {
	grcAssertf(false,"GetIndexBufferByIndex not implemented");
	return NULL;
}

// RETURNS: A pointer to the index buffer for this geometry object
grcIndexBuffer* grmGeometry::GetIndexBufferByIndex(unsigned) {
	grcAssertf(false,"GetIndexBufferByIndex not implemented");
	return NULL;
}

// RETURNS: The rendering primitive type for this geometry
u16 grmGeometry::GetPrimitiveType() const {
	grcAssertf(false,"GetPrimitiveType not implemented");
	return 0;
}

// RETURNS: The number of rendering primitives in this geometry
u32 grmGeometry::GetPrimitiveCount() const {
	grcAssertf(false,"GetPrimitiveCount not implemented");
	return 0;
}

// RETURNS: The number of rendering primitives in this geometry
u32 grmGeometry::GetVertexCount() const {
	grcAssertf(false,"GetVertexCount not implemented");
	return 0;
}

// RETURNS: The number of rendering primitives in this geometry
u32 grmGeometry::GetIndexCount() const {
	grcAssertf(false,"GetIndexCount not implemented");
	return 0;
}

// PURPOSE: Recomputes all the normals, tangents, and binormals for this submesh
void grmGeometry::RecomputeNormalsAndTangents( int /*nVerts*/ ) {
	grcAssertf(false,"RecomputeNormalsAndTangents not implemented");
}

// PURPOSE: Recomputes all the normals for this submesh
void grmGeometry::RecomputeNormals(int /*nVerts*/) {
	grcAssertf(false,"RecomputeNormals not implemented");
}

grmGeometryQB::grmGeometryQB()
: m_MtxPalette(NULL)
, m_VertexCount(0)
, m_IndexCount(0)
, m_Stride(0)
#if USE_EXTRA_VERTS
, m_ExtraVerts(0)
#endif
, m_MtxCount(0)
, m_VtxDeclOffset(0)
, m_DoubleBuffered(0)
, m_OffsetBuffer(NULL)
, grmGeometry(GEOMETRYQB)
{
	for (int i=0;i<sm_MultiBufferCount;i++)
	{
		m_VB[i] = NULL;
		m_IB[i] = NULL;
	}
}

grmGeometry::grmGeometry(datResource &)
{
}

grmGeometryQB::grmGeometryQB(datResource& rsc) : grmGeometry(rsc)
{
	rsc.PointerFixup(m_MtxPalette);
#if IDENTITY_MTX_PALETTE_IS_NULL
	// Temporary code that detects the identity mapping in existing resources.
	if (m_MtxPalette)
	{
		int i;
		for (i=0; i<m_MtxCount; i++)
			if (m_MtxPalette[i] != i)
				break;
		if (i == m_MtxCount)
			m_MtxPalette = NULL;
	}
#endif

	// GetFvf is virtual so this has to be here!
	if (!rsc.IsDefragmentation())
	{
		m_VtxDecl = grmModelFactory::BuildDeclarator(GetFvf(), 0, 0, 0);
	}

#if __PS3
	m_VertexData = m_VB[0]->GetVertexData();
	m_IndexOffset = static_cast<grcIndexBufferGCM*>(m_IB[0].ptr)->GetGCMOffset();
#if SPU_GCM_FIFO >= 2
	m_OffsetBuffer = (grcVertexBuffer*)(m_OffsetBuffer ? m_OffsetBuffer->GetVertexData() : NULL);
#endif // SPU_GCM_FIFO >= 2
#endif
}

grmGeometry::~grmGeometry() {
	grmModelFactory::FreeDeclarator(m_VtxDecl);
}

grmGeometryQB::~grmGeometryQB() {
	grmModelFactory::FreeDeclarator(m_VtxDeclOffset);

	for (int i=0;i<sm_MultiBufferCount;i++)
	{
		delete m_VB[i];
		m_VB[i] = NULL;
	}
	for (int i=0;i<sm_MultiBufferCount;i++)
	{
		delete m_IB[i];
		m_IB[i] = NULL;
	}
	delete[] m_MtxPalette;
}

grmGeometry* grmGeometryQB::CloneWithNewVertexData(bool createVB, void* preAllocatedMemory, int& 
#if !RSG_PC || __RESOURCECOMPILER || RSG_TOOL
	   memoryAvailable
#endif
												   ) const
{
	grmGeometryQB *that = rage_new grmGeometryQB;

	that->m_DoubleBuffered = GetDoubleBuffered();
	that->m_VtxDecl = m_VtxDecl;

	grcAssertf(!m_OffsetBuffer, "Can't clone a grcGeometryQB that has an offset buffer");

// NOTE: the size of passed preAllocatedMemory( if not NULL ) should be same as for sm_MultiBufferCount vertex buffers
// Some assumptions have been made when allocating the memory:
// 1. all buffers have the same stride and same vertex count
#if !RSG_PC || __RESOURCECOMPILER || RSG_TOOL
	int offset = 0;
#endif
	int iVB;
	for( iVB = 0; iVB < MAX_VERT_BUFFERS; iVB++ )
	{
		if( createVB && m_VB[iVB] )
		{
		#if !RSG_PC || __RESOURCECOMPILER || RSG_TOOL
			bool bReadWrite = true;
			bool bDynamic = (__WIN32PC) ? false : true;
			that->m_VB[iVB] = grcVertexBuffer::Create(m_VB[iVB]->GetVertexCount(), *GetFvf()
				, bReadWrite, bDynamic
				, preAllocatedMemory ? ((char*)preAllocatedMemory + offset) : NULL );

			const int vbMemAllocated = m_VB[iVB]->GetVertexCount() * GetFvf()->GetTotalSize();
			offset += vbMemAllocated;
			Assert( memoryAvailable >= vbMemAllocated );
			memoryAvailable -= vbMemAllocated;
			if (that->m_VB[iVB])
			{
				++g_AllowVertexBufferVramLocks;
				grcVertexBufferEditor vertexBufferEditor(that->m_VB[iVB]);
				vertexBufferEditor.Set(m_VB[iVB]);
				--g_AllowVertexBufferVramLocks;
			}
		#else //!RSG_PC || __RESOURCECOMPILER || RSG_TOOL
			Assert(m_VB[iVB]->GetVertexData());

			(void)preAllocatedMemory;
// TODO: Since PC is not using preallocated memory this is not needed here - svetli
//			offset += vbMemAllocated;
//			Assert( memoryAvailable >= vbMemAllocated );
//			memoryAvailable -= vbMemAllocated;
			
			that->m_VB[iVB] = grcVertexBuffer::Clone(m_VB[iVB]);
		#endif //!RSG_PC || __RESOURCECOMPILER || RSG_TOOL
		}
		else
		{
			that->m_VB[iVB] = NULL;
		}
	#if !RSG_PC || __RESOURCECOMPILER || RSG_TOOL

		if( that->m_VB[iVB] )
		{
			const int indexCount = m_IB[iVB]->GetIndexCount();
			that->m_IB[iVB] = grcIndexBuffer::Create(indexCount, true, preAllocatedMemory ? ((char*)preAllocatedMemory + offset) : NULL );
			
			const int ibMemAllocated = (indexCount * sizeof(u16) + 15) & (~15);  // align to 16 bytes, so the verts next start from 16 byte aligned address	
			offset += ibMemAllocated;
			Assert( memoryAvailable >= ibMemAllocated );
			memoryAvailable -= ibMemAllocated;

			that->m_IB[iVB]->Set( m_IB[iVB] );
		}
		else
		{
			that->m_IB[iVB] = NULL;
		}

	#else //!RSG_PC || __RESOURCECOMPILER || RSG_TOOL
		// DX11 TODO:- Destroy the clone. Plus sort out proper solution to Game/Render thread boundary problem.
		if(m_IB[iVB])
		{
			// On DX11 we need to make a clone so we can get READ access.
			that->m_IB[iVB] = grcIndexBuffer::Clone(m_IB[iVB]);
		}
		else
		{
			that->m_IB[iVB] = NULL;
		}
	#endif //!RSG_PC || __RESOURCECOMPILER || RSG_TOOL
	}

	that->m_IndexCount = m_IndexCount;
	that->m_PrimCount = m_PrimCount;
	that->m_VertexCount = m_VertexCount;
	that->m_PrimType = m_PrimType;
	that->m_MtxPalette = m_MtxPalette;
	that->m_Stride = m_Stride;
#if USE_EXTRA_VERTS
	that->m_ExtraVerts = m_ExtraVerts;
#endif
	that->m_MtxCount = m_MtxCount;
	that->m_OffsetBuffer = NULL;
	that->m_VtxDeclOffset = NULL;

	return that;
}

void grmGeometryQB::BlendPositionsFromOtherGeometries(grmGeometry *geom1,grmGeometry *geom2,float alpha1,float alpha2)
{
	grcVertexBuffer *const destVB  = m_VB[0];
	grcVertexBuffer *const src1VB = ((grmGeometryQB*)geom1)->m_VB[0];
	grcVertexBuffer *const src2VB = ((grmGeometryQB*)geom2)->m_VB[0];

	// Note that we perform a read-write lock on destVB here (not a write-only
	// lock), as it is possible that it aliases one of the source vertex
	// buffers.
	float *const dest = destVB->IsReadWrite() ? (float*)destVB->LockRW() : NULL;
	if (dest == NULL)
	{
		//destVB->UnlockRW();
		return;
	}

	const float *const src1 = (src1VB == destVB) ? dest : (float*)src1VB->LockRO();
	const float *const src2 = (src2VB == destVB) ? dest : (float*)src2VB->LockRO();

	const u32 vertexCount  = destVB->GetVertexCount();
	const u32 vertexStride = destVB->GetVertexStride() / sizeof(float);
	const u32 vertexLimit  = vertexCount * vertexStride;

	for (u32 i=0; i<vertexLimit; i+=vertexStride) {
		dest[i+0] = (src1[i+0] * alpha1) + (src2[i+0] * alpha2);
		dest[i+1] = (src1[i+1] * alpha1) + (src2[i+1] * alpha2);
		dest[i+2] = (src1[i+2] * alpha1) + (src2[i+2] * alpha2);
	}

	if (src2VB != destVB) src2VB->UnlockRO();
	if (src1VB != destVB) src1VB->UnlockRO();
	destVB->UnlockRW();
}

void grmGeometryQB::AddPositionsFromOtherGeometries(grmGeometry* geom, float alpha)
{
	grcVertexBuffer* const destVB  = m_VB[0];
	grcVertexBuffer* const srcVB = ((grmGeometryQB*)geom)->m_VB[0];

	float* const dest = destVB->IsReadWrite() ? (float*)destVB->LockRW() : NULL;
	if (dest == NULL)
		return;

    Assert(srcVB != destVB);
	const float* const src = (float*)srcVB->LockRO();

	const u32 vertexCount = destVB->GetVertexCount();
	const u32 destStride = destVB->GetVertexStride() / sizeof(float);
    const u32 srcStride = srcVB->GetVertexStride() / sizeof(float);
	const u32 vertexLimit  = vertexCount * destStride;
    const u32 srcLimit = vertexCount * srcStride;

    u32 f = 0;
	for (u32 i = 0; i < vertexLimit && f < srcLimit; i += destStride, f += srcStride)
	{
		dest[i+0] += src[f+0] * alpha;
		dest[i+1] += src[f+1] * alpha;
		dest[i+2] += src[f+2] * alpha;
	}

	srcVB->UnlockRO();
	destVB->UnlockRW();
}

// Generating adjacency information using the shceme in:
// http://msdn.microsoft.com/en-us/library/windows/desktop/bb509609%28v=vs.85%29.aspx

struct Key
{
	Vector3 a, b;
	bool operator ==(const Key &key) const
	{
		return
			(this->a == key.a && this->b == key.b) ||
			(this->a == key.b && this->b == key.a);
	}
};

struct KeyHash
{
	unsigned operator ()(const Key &key) const
	{
		unsigned hash = 0;
		for(int i=0; i*sizeof(unsigned) < sizeof(key); ++i)
		{
			hash ^= reinterpret_cast<const unsigned*>(&key)[i];
		}
		return hash;
	}
};

struct Value
{
	unsigned adjIndexId;
	u16 targetIndex;
};

grmGeometry::AdjacencyStatus grmGeometryQB::GenerateAdjacency(const bool &isAlive)
{
	if (m_PrimType == drawTrisAdj)
	{
		return AS_ALREADY_DONE;
	}

	if (m_PrimType != drawTris)
	{
		// only triangle list is supported at the moment
		return AS_UNSUPPORTED_PRIMITIVE_TYPE;
	}

	grcVertexBufferEditor vertexBufferEditor(GetVertexBuffer(), true, true);
	const u32 vertexStride = vertexBufferEditor.GetVertexBuffer()->GetVertexStride();
	const grcFvf *const fvf = GetVertexBuffer()->GetFvf();	
	if (!AssertVerify(fvf && fvf->GetPosChannel()))
	{
		return AS_UNEXPECTED_ERROR;
	}

	u8 *const basePtr = (u8*)vertexBufferEditor.GetVertexBuffer()->GetFastLockPtr();
	if (!basePtr)
	{
		return AS_UNEXPECTED_ERROR;
	}
	u8 *const posBasePtr = basePtr + fvf->GetOffset(grcFvf::grcfcPosition);

	grcIndexBuffer* RESTRICT pIndexBuffer = GetIndexBuffer();
	const u16* RESTRICT pIndexPtr = pIndexBuffer->LockRO();
	if (pIndexPtr == NULL)
	{
		return AS_UNEXPECTED_ERROR;
	}

	atMap<Key, Value, KeyHash> edgeMap;
	m_IndexCount *= 2;
	grcIndexBuffer* RESTRICT newIndexBuffer = grcIndexBuffer::Create(m_IndexCount, false);
	u16 *const pNewIndices = newIndexBuffer->LockWO();
	memset(pNewIndices, 0, m_IndexCount*sizeof(u16));
	
	for (int i=0; i<pIndexBuffer->GetIndexCount(); i+=3)
	{
		Vector3* pos[6] = {0};
		if (!isAlive || !GetIndexBuffer())
		{
			if (GetIndexBuffer())
			{
				pIndexBuffer->UnlockRO();
			}

			newIndexBuffer->UnlockWO();
			delete newIndexBuffer;
			return AS_UNEXPECTED_ERROR;
		}

		for (int k=0; k<3; ++k)
		{
			pos[3+k] = pos[k] = reinterpret_cast<Vector3*>(posBasePtr + pIndexPtr[i+k] * vertexStride);
			pNewIndices[(i+k)*2] = pIndexPtr[i+k];
		}

		for (int k=0; k<3; ++k)
		{
			Key key = {*pos[k], *pos[k+1]};			// looking at the edge between k and k+1 vertices
			key.a.w = key.b.w = 0.f;
			const u16 thirdIndex = pIndexPtr[i + (k+2)%3];	// the index of leftover third vertex
			const unsigned newIndexId = (i+k)*2 + 1;		// id of the adjacent index to this edge
			Value *const v = edgeMap.Access(key);
			if (v) {
				Assert(v->adjIndexId < m_IndexCount);
				pNewIndices[v->adjIndexId] = thirdIndex;
				pNewIndices[newIndexId] = v->targetIndex;
				edgeMap.Delete(key);
			}else {
				const Value value = { newIndexId, thirdIndex };
				edgeMap.Insert(key, value);
			}
		}
	}

	if (0)
	{
		// not needed, the shaders should handle -1 just fine
		const int numLeft = edgeMap.GetNumUsed();
		grcAssertf(!numLeft, "GenerateAdjacency: found %d open edges", numLeft);
		if (numLeft)
		{
			for (int i=0; i<edgeMap.GetNumSlots(); ++i)
			{
				if (edgeMap.GetEntry(i))
				{
					// resetting to 0: the safe choice
					pNewIndices[edgeMap.GetEntry(i)->data.adjIndexId] = 0;
				}
			}
		}
	}

	pIndexBuffer->UnlockRO();
	delete pIndexBuffer;
	newIndexBuffer->UnlockWO();
	m_PrimType = drawTrisAdj;

	for(int i=0; i < sm_MultiBufferCount; ++i)
	{
		if (m_IB[i] == (grcIndexBuffer*)pIndexBuffer)
			m_IB[i] = newIndexBuffer;
	}
	
	return AS_GENERATED;
}

void grmGeometry::EnableTripleBuffer(int /*tripleBufferFlags*/, void* /*preAllocatedMemory*/, int& /*memoryAvailable*/)
{
	grcAssertf(false,"EnableTripleBuffer not implemented!");
}

void grmGeometry::DestroyVertexBuffersHackForFragmentCloth()
{
	grcAssertf(false,"DestroyVertexBuffersHackForFragmentCloth not implemented!");
}

void grmGeometry::SetCustomDecl(grcVertexDeclaration *decl)
{
	grmModelFactory::FreeDeclarator(m_VtxDecl);
	m_VtxDecl = decl;
}

void grmGeometryQB::DestroyVertexBuffersHackForFragmentCloth()
{
#if RSG_PC
	if(m_OffsetBuffer)
	{
		m_OffsetBuffer->ReleaseD3DBufferHack();
		m_OffsetBuffer = NULL;
	}

	int iVB;
	for( iVB = 0; iVB < MAX_VERT_BUFFERS; iVB++ )
	{
		if(m_VB[iVB])
		{
			m_VB[iVB]->ReleaseD3DBufferHack();
			m_VB[iVB] = NULL;
		}
	}

#if !__RESOURCECOMPILER
	for( int i = 0; i < MAX_VERT_BUFFERS; i++ )
	{
		if(m_IB[i] != NULL)
		{
			// Can't delete the indexbuffers, since those were actually allocated from an allocator we no longer have access to.
			// The next best thing is to call ReleaseD3DBuffer, which does the same thing as grcIndexBuffer's destructor without
			// actually deleting the grcIndexBuffer.
			m_IB[i]->ReleaseD3DBuffer();
			m_IB[i] = NULL;
		}
	}
#endif      // !__RESOURCECOMPILER


#endif // RSG_PC
}

void grmGeometryQB::EnableTripleBuffer( int tripleBufferFlags, void* preAllocatedMemory,  int& 
#if !RSG_PC || __RESOURCECOMPILER || RSG_TOOL
			   memoryAvailable 
#endif
		   )
{
	Assert( m_VB[0] );
	Assert( m_VB[0]->GetFvf() );
	
// NOTE: the size of passed preAllocatedMemory( if not NULL ) should be enough for (sm_MultiBufferCount - 1) vertex buffers
	int offset = 0;

	m_DoubleBuffered  = (u8)tripleBufferFlags;
	if( GetDoubleBuffered() & TRIPLE_VERTEX_BUFFER )
	{
		for (int i=1;i<sm_MultiBufferCount;i++)
		{
		#if !RSG_PC || __RESOURCECOMPILER || RSG_TOOL
			if( !m_VB[i] )
			{
				Assertf(!(((size_t)preAllocatedMemory + offset) & (RAGE_VERTEXBUFFER_ALIGNMENT-1)),"Bad offset %p+%u from element size of %u, %u verts?",
					preAllocatedMemory,offset,m_VB[0]->GetFvf()->GetTotalSize(),m_VB[0]->GetVertexCount());
				// NOTE: extra verts are already taken into account
				const bool bReadWrite = true;
				const bool bDynamic = (__WIN32PC) ? false : true;
				m_VB[i] = grcVertexBuffer::Create(m_VB[0]->GetVertexCount(), *m_VB[0]->GetFvf()
					, bReadWrite, bDynamic
					, preAllocatedMemory ? ((char*)preAllocatedMemory + offset) : NULL );

				const int vbMemAllocated = m_VB[0]->GetVertexCount() * m_VB[0]->GetFvf()->GetTotalSize();
				offset += vbMemAllocated;
				Assert( memoryAvailable >= vbMemAllocated );
				memoryAvailable -= vbMemAllocated;
			}
			// NOTE: this need to be in scope so the destructor and unlock are called before the index buffer lock below
			if( m_VB[i]->IsValid() )
			{
				grcVertexBufferEditor vertexBufferEditor(m_VB[i]);
				vertexBufferEditor.Set(m_VB[0]);
			}
			
		#else //!RSG_PC || __RESOURCECOMPILER || RSG_TOOL
			if( !m_VB[i] )
			{
				// Clone the 0-th vertex buffer.
				m_VB[i] = grcVertexBuffer::Clone(m_VB[0]);
			}
			else
			{
				// Fill the vertex buffer using the 0-th one as a source (it will be created with read/write access when cloned above).
				grcVertexBufferEditor vertexBufferEditor(m_VB[i]);
				vertexBufferEditor.Set(m_VB[0]);
			}
			(void)preAllocatedMemory;
			offset += m_VB[0]->GetVertexCount() * m_VB[0]->GetFvf()->GetTotalSize();
		#endif //!RSG_PC || __RESOURCECOMPILER || RSG_TOOL

			if( GetDoubleBuffered() & TRIPLE_INDEX_BUFFER )
			{
				if( !m_IB[i] )
				{
					const int indexCount = m_IB[0]->GetIndexCount();
	
					m_IB[i] = grcIndexBuffer::Create(indexCount, true, preAllocatedMemory ? ((char*)preAllocatedMemory + offset) : NULL );
					const int ibMemAllocated = (indexCount * sizeof(u16) + 15) & (~15);  // align to 16 bytes, so the verts next start from 16 byte aligned address
					offset += ibMemAllocated;
		#if !RSG_PC || __RESOURCECOMPILER || RSG_TOOL
					Assert( memoryAvailable >= ibMemAllocated );
					memoryAvailable -= ibMemAllocated;					
		#endif
				}
				m_IB[i]->Set(m_IB[0]);
			}
		}
	}
}

void grmGeometryQB::SetUseSecondaryQBIndex(bool onOff)
{
	m_DoubleBuffered &= ~USE_SECONDARY_BUFFER_INDICES;
	m_DoubleBuffered |= onOff ? USE_SECONDARY_BUFFER_INDICES : 0;
}


void grmGeometryQB::SwapBuffers()
{
	SwapBuffersInternal(0);
}


void grmGeometryQB::SwapSecondaryBuffers()
{
	SwapBuffersInternal(1);
}


void grmGeometryQB::SwapBuffersInternal(int idx)
{
	s_DrawBuffer[idx] = s_BuildBuffer[idx];
	s_BuildBuffer[idx]  = (s_BuildBuffer[idx]+1)%sm_MultiBufferCount;
}


int grmGeometry::GetVertexBufferIndex(bool bDrawBuffer /* = false */)
{
	return bDrawBuffer ? s_DrawBuffer[0] : s_BuildBuffer[0];
}

const grcVertexBuffer* grmGeometryQB::GetVertexBuffer(bool bDrawBuffer) const
{
	int nIndex = 0;
	if( GetDoubleBuffered() & TRIPLE_VERTEX_BUFFER )
	{
		if( bDrawBuffer )
			nIndex = GetDrawBuffer();
		else
			nIndex = GetBuildBuffer();
	}
	return m_VB[nIndex];
}

grcVertexBuffer* grmGeometryQB::GetVertexBuffer(bool bDrawBuffer)
{
	return const_cast<grcVertexBuffer*>(const_cast<const grmGeometryQB*>(this)->GetVertexBuffer(bDrawBuffer));
}

void grmGeometryQB::SetVertexBuffer(grcVertexBuffer *newBuffer)
{
	grcAssertf(!GetDoubleBuffered(), "Can't set the vertex buffer on a double buffered grmGeometryQB");
	delete m_VB[0];
	m_VB[0] = newBuffer;
	m_Stride = (u8)newBuffer->GetVertexStride();
	grmModelFactory::FreeDeclarator(m_VtxDecl);
	m_VtxDecl = grmModelFactory::BuildDeclarator(newBuffer->GetFvf(), 0, 0, 0);
}


void grmGeometryQB::ReplaceVertexBuffers(const atFixedArray<grcVertexBuffer *,MAX_VERT_BUFFERS> &buffers)
{
	for (int i=0; i<buffers.GetCount(); i++)
		m_VB[i]=buffers[i];
}

void grmGeometryQB::SetIndexBuffersPtr(grcIndexBuffer* IB)
{	
	//Warning! this will cause double release in the ~grmGeometryQB

	for( int i=0; i < sm_MultiBufferCount; ++i )
	{
		m_IB[i] = IB;
	}
}



const grcIndexBuffer* grmGeometryQB::GetIndexBuffer(bool bDrawBuffer) const
{
	int nIndex = 0;
	if( GetDoubleBuffered() & TRIPLE_INDEX_BUFFER )
	{
		if( bDrawBuffer )
			nIndex = GetDrawBuffer();
		else
			nIndex = GetBuildBuffer();
	}
	return m_IB[nIndex];
}

grcIndexBuffer* grmGeometryQB::GetIndexBuffer(bool bDrawBuffer)
{
	return const_cast<grcIndexBuffer*>(const_cast<const grmGeometryQB*>(this)->GetIndexBuffer(bDrawBuffer));
}

u32 grmGeometryQB::GetVertexCount() const { 
	int count = 0;
	for (int i = 0; i != m_VB.GetMaxCount(); ++i) {
		count += m_VB[i]?m_VB[i]->GetVertexCount():0;
	}
	return count;
}


inline void grmGeometryQB::RecomputeTangent(  Vec3V_In vecLUV_0, Vec3V_In vecLUV_1 , Vec3V_In vecLUV_2
											, Vec3V_In vEdge1, Vec3V_In vEdge2 
											, Vec3V_InOut tangent0, Vec3V_InOut tangent1, Vec3V_InOut tangent2
											, Vec3V_InOut biNormal0, Vec3V_InOut biNormal1, Vec3V_InOut biNormal2 )
{
	const Vec3V vTexEdge1 = Subtract( vecLUV_1, vecLUV_0 );
	const Vec3V vTexEdge2 = Subtract( vecLUV_2, vecLUV_0 );

	ScalarV vTE1x = vTexEdge1.GetX(),
			vTE1y = vTexEdge1.GetY(),
			vTE2x = vTexEdge2.GetX(),
			vTE2y = vTexEdge2.GetY();

	ScalarV divisor = Subtract(Scale(vTE1x,vTE2y),Scale(vTE2x,vTE1y));
	ScalarV _zero(V_ZERO);
	divisor = SelectFT( IsEqual( divisor, _zero ), Invert( divisor ), _zero );

	const Vec3V tangent	= Scale( vEdge1, vTE2y ) - Scale( vEdge2, vTE1y );
	const Vec3V binormal= Scale( vEdge2, vTE1x ) - Scale( vEdge1, vTE2x );

	tangent0 = AddScaled( tangent0, tangent, divisor );
	tangent1 = AddScaled( tangent1, tangent, divisor );
	tangent2 = AddScaled( tangent2, tangent, divisor );

	biNormal0 = AddScaled( biNormal0, binormal, divisor );
	biNormal1 = AddScaled( biNormal1, binormal, divisor );
	biNormal2 = AddScaled( biNormal2, binormal, divisor );
}


inline void grmGeometryQB::RecomputeNormal(	Vec3V_In vEdge1, Vec3V_In vEdge2, Vec3V_InOut normal0, Vec3V_InOut normal1, Vec3V_InOut normal2 )
{
	const Vec3V normal = Cross(vEdge1, vEdge2);

	normal0 = Add( normal0, normal );
	normal1 = Add( normal1, normal );
	normal2 = Add( normal2, normal );
}

// NOTE: this is used only by cloth, which is quarantee to not have binormal channel
inline void SetTangentFast(Vec3V_InOut vTan, const Vector3& tangent, const Vector3& binormal, const Vector3& normal)
{
	// Determine the sign of the binormal
	Vector3 txn(tangent);
	txn.Cross(normal);
	Vector3 vDot = txn.DotV(binormal);
	Vector3 vTest = vDot.IsLessThanV4(VEC3_ZERO);
	Vector3 vBi = vTest.Select(-VEC3_ONEW, VEC3_ONEW);
	vTan = VECTOR3_TO_VEC3V( (tangent & VEC3_ANDW) | (vBi & VEC3_MASKW) );
}


const ScalarV SCALAR_ZERO		= ScalarV(V_ZERO);
const ScalarV SCALAR_ONE		= ScalarV(V_ONE);
const ScalarV SCALAR_M_ONE		= -ScalarV(V_ONE);

inline void grmGeometryQB::RecomputeBiNormal4(  Vec3V_InOut pTangentPtr0, Vec3V_InOut pTangentPtr1, Vec3V_InOut pTangentPtr2, Vec3V_InOut pTangentPtr3
											  , Vec3V_In tangent0, Vec3V_In tangent1, Vec3V_In tangent2, Vec3V_In tangent3
											  , Vec3V_In n0, Vec3V_In n1, Vec3V_In n2, Vec3V_In n3
											  , Vec3V_In biNormal0, Vec3V_In biNormal1, Vec3V_In biNormal2, Vec3V_In biNormal3 )
{
	const Vec3V _zero(V_ZERO);
	const ScalarV _vhalfPi( Vec::V4VConstant(V_PI_OVER_TWO) );
	const Vec3V _b = VECTOR3_TO_VEC3V( M34_IDENTITY.b );
	const Vec3V _a = VECTOR3_TO_VEC3V( M34_IDENTITY.a );

	Vec3V ntan0 = SelectFT( IsEqual( tangent0, _zero ), tangent0, SelectFT( IsLessThan( Dot(_b, n0), _vhalfPi) , _a, _b) );
	Vec3V ntan1 = SelectFT( IsEqual( tangent1, _zero ), tangent1, SelectFT( IsLessThan( Dot(_b, n1), _vhalfPi) , _a, _b) );
	Vec3V ntan2 = SelectFT( IsEqual( tangent2, _zero ), tangent2, SelectFT( IsLessThan( Dot(_b, n2), _vhalfPi) , _a, _b) );
	Vec3V ntan3 = SelectFT( IsEqual( tangent3, _zero ), tangent3, SelectFT( IsLessThan( Dot(_b, n3), _vhalfPi) , _a, _b) );

	// Compute Tangent
	const ScalarV ndt0 = Dot( n0, ntan0 );
	const ScalarV ndt1 = Dot( n1, ntan1 );
	const ScalarV ndt2 = Dot( n2, ntan2 );
	const ScalarV ndt3 = Dot( n3, ntan3 );

	Vec3V vTangent0 = AddScaled( ntan0, n0, -ndt0 );
	Vec3V vTangent1 = AddScaled( ntan1, n1, -ndt1 );
	Vec3V vTangent2 = AddScaled( ntan2, n2, -ndt2 );
	Vec3V vTangent3 = AddScaled( ntan3, n3, -ndt3 );

	const Vec3V g_UpV = VECTOR3_TO_VEC3V(g_UnitUp);

	vTangent0 = NormalizeSafe( vTangent0, g_UpV );
	vTangent1 = NormalizeSafe( vTangent1, g_UpV );
	vTangent2 = NormalizeSafe( vTangent2, g_UpV );
	vTangent3 = NormalizeSafe( vTangent3, g_UpV );

	// Compute Binormal
	Vec3V vBiNormal0 = Cross( n0, ntan0 );
	Vec3V vBiNormal1 = Cross( n1, ntan1 );
	Vec3V vBiNormal2 = Cross( n2, ntan2 );
	Vec3V vBiNormal3 = Cross( n3, ntan3 );

	ScalarV vScale0 = Dot( vBiNormal0, biNormal0);
	vScale0 = SelectFT( IsLessThan( vScale0, SCALAR_ZERO), SCALAR_M_ONE, SCALAR_ONE );

	ScalarV vScale1 = Dot(vBiNormal1, biNormal1);
	vScale1 = SelectFT( IsLessThan( vScale1, SCALAR_ZERO), SCALAR_M_ONE, SCALAR_ONE );

	ScalarV vScale2 = Dot(vBiNormal2, biNormal2);
	vScale2 = SelectFT( IsLessThan( vScale2, SCALAR_ZERO), SCALAR_M_ONE, SCALAR_ONE );

	ScalarV vScale3 = Dot(vBiNormal3, biNormal3);
	vScale3 = SelectFT( IsLessThan( vScale3, SCALAR_ZERO), SCALAR_M_ONE, SCALAR_ONE );

	vBiNormal0 = Scale( vBiNormal0, vScale0 );
	vBiNormal1 = Scale( vBiNormal1, vScale1 );
	vBiNormal2 = Scale( vBiNormal2, vScale2 );
	vBiNormal3 = Scale( vBiNormal3, vScale3 );

	vBiNormal0 = NormalizeSafe( vBiNormal0, g_UpV );
	vBiNormal1 = NormalizeSafe( vBiNormal1, g_UpV );
	vBiNormal2 = NormalizeSafe( vBiNormal2, g_UpV );
	vBiNormal3 = NormalizeSafe( vBiNormal3, g_UpV );

	SetTangentFast( pTangentPtr0, VEC3V_TO_VECTOR3( vTangent0 ), VEC3V_TO_VECTOR3( vBiNormal0 ), VEC3V_TO_VECTOR3( n0 ) );
	SetTangentFast( pTangentPtr1, VEC3V_TO_VECTOR3( vTangent1 ), VEC3V_TO_VECTOR3( vBiNormal1 ), VEC3V_TO_VECTOR3( n1 ) );
	SetTangentFast( pTangentPtr2, VEC3V_TO_VECTOR3( vTangent2 ), VEC3V_TO_VECTOR3( vBiNormal2 ), VEC3V_TO_VECTOR3( n2 ) );
	SetTangentFast( pTangentPtr3, VEC3V_TO_VECTOR3( vTangent3 ), VEC3V_TO_VECTOR3( vBiNormal3 ), VEC3V_TO_VECTOR3( n3 ) );
}


#define		CONST_INT_I0_I7(i)	const int	i0 = i,		\
											i1 = i + 1,	\
											i2 = i + 2,	\
											i3 = i + 3,	\
											i4 = i + 4,	\
											i5 = i + 5,	\
											i6 = i + 6,	\
											i7 = i + 7



void grmGeometryQB::RecomputeNormalsAndTangents(int nVerts)
{
	// Index buffer must always be locked before vertex buffer to prevent deadlock
	grcIndexBuffer* RESTRICT pIndexBuffer = GetIndexBuffer();
	int nIndices = pIndexBuffer->GetIndexCount();
	const u16* RESTRICT pIndexPtr = pIndexBuffer->LockRO();
	Assert(pIndexPtr);

	// Allocate local space
	grcVertexBuffer* RESTRICT pVertexBuffer = GetVertexBuffer();
	grcVertexBufferEditor vertexBufferEditor(pVertexBuffer);

// If there is a problem in general it is with drawable having more verts than the cloth
	Assertf( nVerts == GetCurrentVertsCount(), "The number of verts in the cloth: %d doesn't match the number of the verts in the drawable: %d. Please correct the asset ! ", nVerts, pVertexBuffer->GetVertexCount() );
#if __ASSERT
	if( nVerts != GetCurrentVertsCount() )
	{
		pIndexBuffer->UnlockRO();
		return;
	}
#endif

//	grcAssertf(pVertexBuffer->IsDynamic());

	Vec3V* RESTRICT pLNormals = Alloca(Vec3V, nVerts);
	Vec3V* RESTRICT pLPositions = Alloca(Vec3V, nVerts);

	sysMemSet(pLNormals, 0, sizeof(Vec3V) * nVerts);

	const int nVert4 = ((nVerts >> 2) << 2) - 4;
	const int nVert8 = ((nVerts >> 3) << 3) - 8;

	const u32 vertexStride = vertexBufferEditor.GetVertexBuffer()->GetVertexStride();
	u8* blockPtr = (u8*)vertexBufferEditor.GetVertexBuffer()->GetFastLockPtr();
	const grcFvf* fvf = pVertexBuffer->GetFvf();	
	Assert( fvf );
	Assert( fvf->GetTangentChannel(0) );

	Vec3V* RESTRICT pLUVs = Alloca(Vec3V, nVerts);
	Vec3V* RESTRICT pLTangents = Alloca(Vec3V, nVerts);
	Vec3V* RESTRICT pLBiNormals = Alloca(Vec3V, nVerts);	

	sysMemSet(pLTangents, 0, sizeof(Vec3V) * nVerts);
	sysMemSet(pLBiNormals, 0, sizeof(Vec3V) * nVerts);

	Vec3V _zero(V_ZERO);
	Vec3V vFlipV(V_Y_AXIS_WZERO);		
	const int uvSet = 0; // currently texture uvs and blend uvs are in grcfcTexture0 set
	const int uvOffset = fvf->GetOffset(static_cast<grcFvf::grcFvfChannels>(grcFvf::grcfcTexture0 + uvSet));

	Vec3V maskXY = Add( Vec3V(V_X_AXIS_WZERO), Vec3V(V_Y_AXIS_WZERO) );

	int i = 0;
	for( ; i < nVert8; i += 8 )
	{
		CONST_INT_I0_I7(i);

		const u32 offset0 = vertexStride * i0 + uvOffset;
		const u32 offset1 = vertexStride * i1 + uvOffset;
		const u32 offset2 = vertexStride * i2 + uvOffset;
		const u32 offset3 = vertexStride * i3 + uvOffset;
		const u32 offset4 = vertexStride * i4 + uvOffset;
		const u32 offset5 = vertexStride * i5 + uvOffset;
		const u32 offset6 = vertexStride * i6 + uvOffset;
		const u32 offset7 = vertexStride * i7 + uvOffset;

		// the mask is not needed but is practically free , sometimes there are 2 uv sets in the Texture0 channel

		pLUVs[i0] = SubtractScaled( vFlipV, *reinterpret_cast<Vec3V*>(blockPtr + offset0), maskXY );
		pLUVs[i1] = SubtractScaled( vFlipV, *reinterpret_cast<Vec3V*>(blockPtr + offset1), maskXY );
		pLUVs[i2] = SubtractScaled( vFlipV, *reinterpret_cast<Vec3V*>(blockPtr + offset2), maskXY );
		pLUVs[i3] = SubtractScaled( vFlipV, *reinterpret_cast<Vec3V*>(blockPtr + offset3), maskXY );
		pLUVs[i4] = SubtractScaled( vFlipV, *reinterpret_cast<Vec3V*>(blockPtr + offset4), maskXY );
		pLUVs[i5] = SubtractScaled( vFlipV, *reinterpret_cast<Vec3V*>(blockPtr + offset5), maskXY );
		pLUVs[i6] = SubtractScaled( vFlipV, *reinterpret_cast<Vec3V*>(blockPtr + offset6), maskXY );
		pLUVs[i7] = SubtractScaled( vFlipV, *reinterpret_cast<Vec3V*>(blockPtr + offset7), maskXY );
	}
	for(; i < nVerts; ++i )
	{
		pLUVs[i] = SubtractScaled( vFlipV, *reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i + uvOffset ), maskXY );
	}

	for( i = 0; i < nVert8; i += 8 )
	{
		CONST_INT_I0_I7(i);

		const u32 offset0 = vertexStride * i0;
		const u32 offset1 = vertexStride * i1;
		const u32 offset2 = vertexStride * i2;
		const u32 offset3 = vertexStride * i3;
		const u32 offset4 = vertexStride * i4;
		const u32 offset5 = vertexStride * i5;
		const u32 offset6 = vertexStride * i6;
		const u32 offset7 = vertexStride * i7;

		pLPositions[i0] = *reinterpret_cast<Vec3V*>(blockPtr + offset0);
		pLPositions[i1] = *reinterpret_cast<Vec3V*>(blockPtr + offset1);
		pLPositions[i2] = *reinterpret_cast<Vec3V*>(blockPtr + offset2);
		pLPositions[i3] = *reinterpret_cast<Vec3V*>(blockPtr + offset3);
		pLPositions[i4] = *reinterpret_cast<Vec3V*>(blockPtr + offset4);
		pLPositions[i5] = *reinterpret_cast<Vec3V*>(blockPtr + offset5);
		pLPositions[i6] = *reinterpret_cast<Vec3V*>(blockPtr + offset6);
		pLPositions[i7] = *reinterpret_cast<Vec3V*>(blockPtr + offset7);
	}	
	for(; i < nVerts; ++i )
	{
		pLPositions[i] = *reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i);
	}


	Assert( !fvf->GetBinormalChannel(0) );

	Vec3V vEdge1, vEdge2;
	for( i = 0; i < nIndices; i += 3 )
	{
		const u16* RESTRICT indices = &pIndexPtr[i];
		const int i0 = indices[0];
		const int i1 = indices[1];
		const int i2 = indices[2];

		Assert(i0<nVerts);
		Assert(i1<nVerts);
		Assert(i2<nVerts);

		Vec3V vEdge1 = Subtract( pLPositions[i1], pLPositions[i0] );
		Vec3V vEdge2 = Subtract( pLPositions[i2], pLPositions[i0] );
		RecomputeNormal( vEdge1, vEdge2, pLNormals[i0], pLNormals[i1], pLNormals[i2] );

		RecomputeTangent( pLUVs[i0] , pLUVs[i1], pLUVs[i2]
						, vEdge1, vEdge2, pLTangents[i0],pLTangents[i1], pLTangents[i2]
						, pLBiNormals[i0], pLBiNormals[i1], pLBiNormals[i2] );
	}		
	// why do we care about the UV set here, those are tangents?! - svetli
	const int tangentOffset = fvf->GetOffset(static_cast<grcFvf::grcFvfChannels>(grcFvf::grcfcTangent0 /*+ uvSet*/));
	const int normalOffset = fvf->GetOffset(static_cast<grcFvf::grcFvfChannels>(grcFvf::grcfcNormal));

	Vec3V g_UpV = VECTOR3_TO_VEC3V(g_UnitUp);
	ScalarV _vhalfPi( Vec::V4VConstant(V_PI_OVER_TWO) );
	Vec3V _b = VECTOR3_TO_VEC3V( M34_IDENTITY.b );
	Vec3V _a = VECTOR3_TO_VEC3V( M34_IDENTITY.a );

	for(i = 0; i < nVert4; i+= 4 )
	{
		const int i0 = i;
		const int i1 = i+1;
		const int i2 = i+2;
		const int i3 = i+3;

		Vec3V n0 = NormalizeSafe( pLNormals[i0], g_UpV );
		Vec3V n1 = NormalizeSafe( pLNormals[i1], g_UpV );
		Vec3V n2 = NormalizeSafe( pLNormals[i2], g_UpV );
		Vec3V n3 = NormalizeSafe( pLNormals[i3], g_UpV );

		pLNormals[i0] = n0;
		pLNormals[i1] = n1;
		pLNormals[i2] = n2;
		pLNormals[i3] = n3;

		Vec3V* fastN0 = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i0 + normalOffset);
		Vec3V* fastN1 = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i1 + normalOffset);
		Vec3V* fastN2 = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i2 + normalOffset);
		Vec3V* fastN3 = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i3 + normalOffset);

		fastN0[0] = n0;
		fastN1[0] = n1;
		fastN2[0] = n2;
		fastN3[0] = n3;	
// 
// 		u8* pTangentPtr0 = blockPtr + vertexStride * i0 + tangentOffset;
// 		u8* pTangentPtr1 = blockPtr + vertexStride * i1 + tangentOffset;
// 		u8* pTangentPtr2 = blockPtr + vertexStride * i2 + tangentOffset;
// 		u8* pTangentPtr3 = blockPtr + vertexStride * i3 + tangentOffset;

		Vec3V* fastT0 = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i0 + tangentOffset);
		Vec3V* fastT1 = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i1 + tangentOffset);
		Vec3V* fastT2 = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i2 + tangentOffset);
		Vec3V* fastT3 = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i3 + tangentOffset);

		RecomputeBiNormal4( fastT0[0], fastT1[0], fastT2[0], fastT3[0]
			, pLTangents[i0], pLTangents[i1], pLTangents[i2], pLTangents[i3]
			, n0, n1, n2, n3
			, pLBiNormals[i0], pLBiNormals[i1], pLBiNormals[i2], pLBiNormals[i3] );

	}
	for(; i < nVerts; i++ )
	{
		Vec3V n = NormalizeSafe( pLNormals[i], g_UpV );
		Vec3V* fastN = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i + normalOffset);
		fastN[0] = n;

		Vec3V ntan = SelectFT( IsEqual( pLTangents[i], _zero ), pLTangents[i], SelectFT( IsLessThan( Dot(_b, n), _vhalfPi) , _a, _b));

		ScalarV ndt = Dot( n, ntan );
		Vec3V vTangent = AddScaled( ntan, n, -ndt );
		vTangent = NormalizeSafe( vTangent, g_UpV );


		Vec3V vBiNormal = Cross( n, ntan );
		ScalarV vScale = Dot( vBiNormal, pLBiNormals[i]);
		BoolV IsLessThanV0 = IsLessThan( vScale, SCALAR_ZERO);
		vScale = SelectFT( IsLessThanV0, SCALAR_M_ONE, SCALAR_ONE );

		vBiNormal = Scale( vBiNormal, vScale );
		vBiNormal = NormalizeSafe( vBiNormal, g_UpV );

		//u8* pTangentPtr = blockPtr + vertexStride * i + tangentOffset;
		Vec3V* fastT = reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i + tangentOffset);
		SetTangentFast(fastT[0], VEC3V_TO_VECTOR3( vTangent ), VEC3V_TO_VECTOR3( vBiNormal),  VEC3V_TO_VECTOR3( n ) );
	}

	pIndexBuffer->UnlockRO();
}

void grmGeometryQB::RecomputeNormals( int nVerts )
{
	grcVertexBuffer* RESTRICT pVertexBuffer = GetVertexBuffer();
	// Index buffer must always be locked before vertex buffer to prevent deadlock
	grcIndexBuffer* RESTRICT pIndexBuffer = GetIndexBuffer();
	int nIndices = pIndexBuffer->GetIndexCount();
	const u16* RESTRICT pIndexPtr = pIndexBuffer->LockRO();
	Assert(pIndexPtr);
	if (pIndexPtr == NULL)
	{
		Errorf("RecomputeNormals Failed - Source Index Buffer is NULL");
		return;
	}

	grcVertexBufferEditor vertexBufferEditor(pVertexBuffer);
	
	// If there is a problem in general it is with drawable having more verts than the cloth
	Assertf( nVerts == GetCurrentVertsCount(), "The number of verts in the cloth: %d doesn't match the number of the verts in the drawable: %d. Please correct the asset ! ", nVerts, pVertexBuffer->GetVertexCount() );
#if __ASSERT
	if( nVerts != GetCurrentVertsCount() )
		return;
#endif

	grcAssertf(pVertexBuffer->IsDynamic(), "RecomputeNormals only works on dynamic vertex buffers");

	Vec3V* RESTRICT pLNormals = Alloca(Vec3V, nVerts);
	Vec3V* RESTRICT pLPositions = Alloca(Vec3V, nVerts);

	sysMemSet(pLNormals, 0, sizeof(Vector3) * nVerts);

	const u32 vertexStride = vertexBufferEditor.GetVertexBuffer()->GetVertexStride();
	u8* blockPtr = (u8*)vertexBufferEditor.GetVertexBuffer()->GetFastLockPtr();

	const int nVert4 = ((nVerts >> 2) << 2) - 4;
	const int nVert8 = ((nVerts >> 3) << 3) - 8;
	int i;
	for( i = 0; i < nVert8; i += 8 )
	{
		CONST_INT_I0_I7(i);

		const u32 offset  = vertexStride * i0;
		const u32 offset1 = vertexStride * i1;
		const u32 offset2 = vertexStride * i2;
		const u32 offset3 = vertexStride * i3;
		const u32 offset4 = vertexStride * i4;
		const u32 offset5 = vertexStride * i5;
		const u32 offset6 = vertexStride * i6;
		const u32 offset7 = vertexStride * i7;

		pLPositions[i0] = *reinterpret_cast<Vec3V*>(blockPtr + offset);
		pLPositions[i1] = *reinterpret_cast<Vec3V*>(blockPtr + offset1);
		pLPositions[i2] = *reinterpret_cast<Vec3V*>(blockPtr + offset2);
		pLPositions[i3] = *reinterpret_cast<Vec3V*>(blockPtr + offset3);
		pLPositions[i4] = *reinterpret_cast<Vec3V*>(blockPtr + offset4);
		pLPositions[i5] = *reinterpret_cast<Vec3V*>(blockPtr + offset5);
		pLPositions[i6] = *reinterpret_cast<Vec3V*>(blockPtr + offset6);
		pLPositions[i7] = *reinterpret_cast<Vec3V*>(blockPtr + offset7);
	}	

	for(; i < nVerts; i++ )
	{
		pLPositions[i] = *reinterpret_cast<Vec3V*>(blockPtr + vertexStride * i);
	}	



	for( i = 0; i < nIndices; i += 3 )
	{
		const u16* RESTRICT indices = &pIndexPtr[i];

		const int i0 = indices[0];
		const int i1 = indices[1];
		const int i2 = indices[2];

		Vec3V n1 = pLNormals[i0];
		Vec3V n2 = pLNormals[i1];
		Vec3V n3 = pLNormals[i2];

		Vec3V vEdge1 = Subtract( pLPositions[i1], pLPositions[i0] );
		Vec3V vEdge2 = Subtract( pLPositions[i2], pLPositions[i0] );

		Vec3V normal = Cross(vEdge1, vEdge2);

		pLNormals[i0] = Add( n1, normal );
		pLNormals[i1] = Add( n2, normal );
		pLNormals[i2] = Add( n3, normal );

		// DX11 TODO:- Investigate why we hit this Assert().
		AssertMsg( ((i0 < nVerts) && (i1 < nVerts) && (i2 < nVerts)), "grmGeometryQB::RecomputeNormals()...Index out of range!" );
	}

	Vector3* RESTRICT fastSetN = (Vector3 *)(((u8 *)(pVertexBuffer->GetFastLockPtr())) + vertexBufferEditor.GetNormalOffset());
	u32 fastStride = pVertexBuffer->GetVertexStride();
	grcAssertf( fastStride == ((fastStride >> 4) << 4), "Vertex stride (%d) is not a multiple of 16", fastStride );
	fastStride = fastStride >> 4;

	const Vec3V g_UpV = VECTOR3_TO_VEC3V(g_UnitUp);
	for(i = 0; i < nVert4; i+= 4 )
	{
		const int i0 = i;
		const int i1 = i+1;
		const int i2 = i+2;
		const int i3 = i+3;

		Vec3V n0 = NormalizeSafe( pLNormals[i0], g_UpV );
		Vec3V n1 = NormalizeSafe( pLNormals[i1], g_UpV );
		Vec3V n2 = NormalizeSafe( pLNormals[i2], g_UpV );
		Vec3V n3 = NormalizeSafe( pLNormals[i3], g_UpV );

		fastSetN[i0*fastStride].Set( VEC3V_TO_VECTOR3( n0 ) );
		fastSetN[i1*fastStride].Set( VEC3V_TO_VECTOR3( n1 ) );
		fastSetN[i2*fastStride].Set( VEC3V_TO_VECTOR3( n2 ) );
		fastSetN[i3*fastStride].Set( VEC3V_TO_VECTOR3( n3 ) );
	}
	for(; i < nVerts; i++ )
	{
		Vec3V n = NormalizeSafe( pLNormals[i], g_UpV );
		fastSetN[i*fastStride].Set( VEC3V_TO_VECTOR3( n ) );
	}

	GetIndexBuffer()->UnlockRO();

	/*timeit.Stop();
	Printf("C:");
	timeit.PrintReset(true);*/

}

#if MESH_LIBRARY
static bool CheckBit(const mshMaterial &mtl, u32 channelMask,int bit,bool isPresent) 
{
	// Either the shader doesn't want this data, or it's available.
	// (ie if the shader does want this data, it had better be there)
	bool required = (channelMask & (1 << bit)) != 0;
	if (required && !isPresent) {
#if !__TOOL && !__NO_OUTPUT
		static const char *channelName[] = { 
			"position", "weight", "binding", "normal", "diffuse", "specular", 
			"texcoord0", "texcoord1", "texcoord2", "texcoord3", "texcoord4", "texcoord5", "texcoord6", "texcoord7", 
			"tangent0", "tangent1", "binormal0", "binormal1"
		};
		CompileTimeAssert(NELEM(channelName) == grcFvf::grcfcCount);

		grcWarningf("Shader %s expects %s but not present in material!",(const char*)mtl.Name, channelName[bit]);
#else 
		(void)mtl;
#endif
	}
	return required;
}

void grmGeometry::ComputeFVF(const mshMaterial &mtl,int mtxCount,u32 channelMask,grcFvf &outFvf,bool bDynamic) {
	if (!channelMask)
		channelMask = ~0U;

	// NOTE: If we decide to re-enable packed positions, need to support a short3
	//		If we decide to re-enable packed texture coords, need to support short2
	outFvf.ClearAllChannels();

	if (bDynamic)
		outFvf.SetDynamicOrder(true);

	grmVtxStreamConfigurator *streamCfg = grmModelFactory::GetVertexConfigurator();

	if ((channelMask & grcFvf::grcfcPositionHalf4Mask)) {
		outFvf.SetPosChannel(true, grcFvf::grcdsHalf4);
	}
	else if ((channelMask & grcFvf::grcfcPositionFloat4Mask)) {
		outFvf.SetPosChannel(true, grcFvf::grcdsFloat4);
	}
	else {
		outFvf.SetPosChannel((channelMask & (grcFvf::grcfcPositionMask)) != 0, grcFvf::grcdsFloat3);
		outFvf.SetBlendWeightChannel(mtxCount != 0, grcFvf::grcdsColor);
		outFvf.SetBindingsChannel(mtxCount != 0);
	}

	if ( streamCfg->AreNormalsPacked() )
		outFvf.SetNormalChannel(CheckBit(mtl,channelMask,grcFvf::grcfcNormal,streamCfg->AreNormalsEnabled()), grcFvf::grcdsPackedNormal);
	else
		outFvf.SetNormalChannel(CheckBit(mtl,channelMask,grcFvf::grcfcNormal,streamCfg->AreNormalsEnabled()), grcFvf::grcdsFloat3);
	outFvf.SetDiffuseChannel((channelMask & (1<<grcFvf::grcfcDiffuse)) != 0, grcFvf::grcdsColor);
	outFvf.SetSpecularChannel((channelMask & (1<<grcFvf::grcfcSpecular)) != 0, grcFvf::grcdsColor);
	
	for (int i = 0; i < mshMaxTexCoordSets; ++i) {
		outFvf.SetTextureChannel( i, CheckBit(mtl,channelMask,grcFvf::grcfcTexture0+i,streamCfg->IsTexCoordEnabled(i) && mtl.TexSetCount > i), 
			streamCfg->AreTexCoordsPacked()? grcFvf::grcdsHalf2 : grcFvf::grcdsFloat2 );
	}

	for (int i = 0; i < mshMaxTanBiSets; ++i) {
		// Only Xenon supports four-element packed normals, so fall back on Half4 if necessary
		outFvf.SetTangentChannel( i, CheckBit(mtl,channelMask, grcFvf::grcfcTangent0+i,streamCfg->IsTangentEnabled(i) && mtl.TanBiSetCount > i), 
			/*streamCfg->IsBinormalEnabled(i) ? (streamCfg->AreNormalsPacked()? grcFvf::grcdsPackedNormal : grcFvf::grcdsFloat3) :*/ (streamCfg->AreNormalsPacked() ? (g_sysPlatform != platform::XENON? grcFvf::grcdsHalf4 : grcFvf::grcdsPackedNormal) : grcFvf::grcdsFloat4));
		outFvf.SetBinormalChannel( i, CheckBit(mtl,channelMask, grcFvf::grcfcBinormal0+i,streamCfg->IsBinormalEnabled(i) && mtl.TanBiSetCount > i), /*(streamCfg->AreNormalsPacked()? grcFvf::grcdsPackedNormal :*/ grcFvf::grcdsFloat3); // );
	}
}



void ComputeClothCustomFVF(const mshMaterial &mtl,u32 channelMask,grcFvf &outFvf) {
	if (!channelMask)
		channelMask = ~0U;

	outFvf.ClearAllChannels();

	grmVtxStreamConfigurator *streamCfg = grmModelFactory::GetVertexConfigurator();

	outFvf.SetPosChannel((channelMask & (grcFvf::grcfcPositionMask)) != 0, grcFvf::grcdsFloat3);

	Assert( mshMaxTanBiSets == 2 );
	const bool hasTangents = CheckBit(mtl,channelMask, grcFvf::grcfcTangent0+0,streamCfg->IsTangentEnabled(0) && mtl.TanBiSetCount > 0);

// NOTE: there shouldn't be second tangent channel
	Assert( !CheckBit(mtl,channelMask, grcFvf::grcfcTangent0+1,streamCfg->IsTangentEnabled(1) && mtl.TanBiSetCount > 1) );

	const bool hasNormals = streamCfg->AreNormalsEnabled();
	Assert( hasNormals );

 	if ( hasTangents )
 		outFvf.SetNormalChannel(CheckBit(mtl,channelMask,grcFvf::grcfcNormal,hasNormals), grcFvf::grcdsPackedNormal);
 	else
		outFvf.SetNormalChannel(CheckBit(mtl,channelMask,grcFvf::grcfcNormal,hasNormals), grcFvf::grcdsFloat3);

	outFvf.SetDiffuseChannel((channelMask & (1<<grcFvf::grcfcDiffuse)) != 0, grcFvf::grcdsColor);
	outFvf.SetSpecularChannel((channelMask & (1<<grcFvf::grcfcSpecular)) != 0, grcFvf::grcdsColor);

// 	for (int i = 0; i < mshMaxTexCoordSets; ++i) 
// 	{
// 		outFvf.SetTextureChannel( i, CheckBit(mtl,channelMask,grcFvf::grcfcTexture0+i,streamCfg->IsTexCoordEnabled(i) && mtl.TexSetCount > i), grcFvf::grcdsHalf2 );
// 	}
	outFvf.SetTextureChannel( 0 /*i*/, CheckBit(mtl,channelMask,grcFvf::grcfcTexture0 + 0 /*i*/,streamCfg->IsTexCoordEnabled(0 /*i*/) && mtl.TexSetCount > 0 /*i*/), grcFvf::grcdsHalf2 );

	outFvf.SetTangentChannel( 0, hasTangents, grcFvf::grcdsHalf4 );		// NOTE: tangents should be packed in 8 bytes

// NOTE: no binormal channels in the cloth
	Assert( CheckBit(mtl,channelMask, grcFvf::grcfcBinormal0+0,streamCfg->IsBinormalEnabled(0) && mtl.TanBiSetCount > 0) == false );
	Assert( CheckBit(mtl,channelMask, grcFvf::grcfcBinormal0+1,streamCfg->IsBinormalEnabled(1) && mtl.TanBiSetCount > 1) == false );

#if __ASSERT
	const char posOffset	= (char)outFvf.GetOffset(grcFvf::grcfcPosition);
	const char uvOffset		= (char)outFvf.GetOffset(grcFvf::grcfcTexture0);
	const char normalOffset	= (char)outFvf.GetOffset(grcFvf::grcfcNormal);
	const char diffuseOffset= (char)outFvf.GetOffset(grcFvf::grcfcDiffuse);
	
	Assert( posOffset == 0 );
	Assert( normalOffset == 12 );
	if ( hasTangents )
	{
		Assert( diffuseOffset == 16 );
		Assert( uvOffset == 20 );
		const char tangentOffset = (char)outFvf.GetOffset(grcFvf::grcfcTangent0);
		Assert( tangentOffset == 24 );
	}
	else
	{
		Assert( diffuseOffset == 24 );
		Assert( uvOffset == 28 );
	}

#endif
}




#if USE_EXTRA_VERTS
	#define UNUSED_PARAM_EXTRAVERTS(x) x
#else
	#define UNUSED_PARAM_EXTRAVERTS(x)
#endif

bool grmGeometryQB::Init(const mshMesh &mesh,int mtlIndex,u32 channelMask, const bool isCharClothMesh, const bool isEnvClothMesh, GeometryCreateParams* params, int UNUSED_PARAM_EXTRAVERTS(extraVerts) ) {
	const mshMaterial &mtl = mesh.GetMtl(mtlIndex);
	int mtxCount = mesh.GetMatrixCount();

	RAGE_TRACK(Graphics);
	RAGE_TRACK(grmGeometryQB);

	m_OffsetBuffer = 0;

	// Meshes need to be marked as dynamic in the future.  Right now the only thing using
	// dynamic vertex buffers is cloth and all cloth should have at least one pin value
	// So if the pinning channel exists, create this vertex buffer as dynamic
	bool bDynamic = mtl.FindChannel(mshVtxBlindCLOTHPIN) >= 0 && !isCharClothMesh && isEnvClothMesh;
	const bool bIsCloth = bDynamic;

	// Create an fvf to use as a mask
	grcFvf fvf;
	if( bDynamic )
	{
		ComputeClothCustomFVF(mtl, channelMask, fvf);
		Assert( fvf.GetTotalSize() == 32 );				// expected stride is always 32, with or without tangents
	}
	else
	{
		ComputeFVF(mtl, mtxCount, channelMask, fvf, bDynamic);
	}

	bDynamic = (__WIN32PC || RSG_DURANGO) ? false : bDynamic;

	bool bReadWriteVtxBuf = bDynamic;
	if (params && params->ReadWriteVtxBuf)
	{
		bReadWriteVtxBuf = true;
	}

#if __WIN32PC 
	if (!GRCDEVICE.IsCreated() && !sysMemAllocator::GetCurrent().IsBuildingResource()) {
		m_VtxDecl = NULL;
		return false;
	}
	WIN32PC_ONLY(if (!GRCDEVICE.GetCurrent() && !sysMemAllocator::GetCurrent().IsBuildingResource()) return false);
#endif
	WIN32PC_ONLY(if (!GRCDEVICE.IsCreated() && !sysMemAllocator::GetCurrent().IsBuildingResource()) return false);

	int primCount = mtl.Prim.GetCount();
	int idxCount = 0;
	bool tristripped;
	bool restarts = false; // still buggy, and vertex cache opt is better anyway.  was: g_sysPlatform != platform::WIN32PC;	// and xenon too eventually
	u8 *reverseMap = Alloca(u8,mtxCount);
	if (primCount == 1 && mtl.Prim[0].Type == mshTRIANGLES) {
		idxCount = mtl.Prim[0].Idx.GetCount();
		tristripped = false;
		m_PrimCount = (idxCount / 3);
		m_PrimType = drawTris;
	}
	else {
		idxCount = restarts? (primCount-1) : (primCount-1)*2;
		for (int i=0; i<primCount; i++)
			idxCount += mtl.Prim[i].Idx.GetCount();
		tristripped = true;
		m_PrimCount = (idxCount - 2);
		m_PrimType = drawTriStrip;
	}

	// grcAssertf( (idxCount < 65535), "Model requires %d vertex indices, the maximum allowed is 65535", idxCount);
	grcAssertf(mtl.GetVertexCount() < 65535, "Model requires %d vertices, the maximum allowed is 65535", mtl.GetVertexCount());

	m_IndexCount = idxCount;
	m_VertexCount = (u16) mtl.GetVertexCount();

	sm_TotalIndexCount += m_IndexCount;
	sm_TotalIndexSize += m_IndexCount*2;
	sm_TotalPrimCount += m_PrimCount;
	sm_TotalVertexCount += m_VertexCount;
	sm_TotalVertexSize += m_VertexCount * fvf.GetTotalSize();

	for (int i=0; i<grcFvf::grcfcCount; i++) {
		if (channelMask & (1 << i)) {
			sm_TotalChannelSize[i] += m_VertexCount * fvf.GetDataSizeFromType(fvf.GetDataSizeType((grcFvf::grcFvfChannels) i));
		}
	}

	// Get max matrices per material correct for target platform.
	// The preprocessor symbols are here just so they show up in a search since this code has to change
	// if we turn the extended skinning methods back off on 360.
#if __RESOURCECOMPILER
	s_MaxMatricesPerMaterial = SKINNING_COUNT_FOR_PLATFORM;
#endif

	if (mtxCount) {
		if (mtxCount <= s_MaxMatricesPerMaterial) {
			m_MtxCount = (u8) mtxCount;
#if IDENTITY_MTX_PALETTE_IS_NULL
			m_MtxPalette = NULL;
			for (int i=0; i<mtxCount; i++)
				reverseMap[i] = (u8) i;
#else
			m_MtxPalette = rage_contained_new u16[mtxCount];
			for (int i=0; i<mtxCount; i++) {
				m_MtxPalette[i] = (u16) i;
				reverseMap[i] = (u8) i;
			}
#endif
		}
		else {
			memset(reverseMap,0xFF,mtxCount);
			u16 *fwdMap = Alloca(u16,mtxCount);
			int localCount = 0;
			for (int j=0; j<mshMaxMatricesPerVertex; j++) {
				for (int i=0; i<mtl.GetVertexCount(); i++) {
					const mshBinding &B = mtl.GetBinding(i);
					if (B.IsPassThrough == false && B.Wgt[j] > 0.0f) {
						// Matrix used yet?
						if (reverseMap[B.Mtx[j]] == 0xFF) {
							grcAssertf(j || localCount < s_MaxMatricesPerMaterial,"Too many matrices in a single skinned material");
							if (localCount != s_MaxMatricesPerMaterial) {
								reverseMap[B.Mtx[j]] = (u8) localCount;
								fwdMap[localCount] = (rage::u16)B.Mtx[j];
								++localCount;
							}
						}
					}
				}
			}
			for( int j = 0; j < mtxCount; j++ )
			{
				if( reverseMap[j] == 0xFF )
					reverseMap[j] = 0;
			}
			
			m_MtxCount = (u8) localCount;
			if (localCount > 0)
			{
				m_MtxPalette = rage_contained_new u16[localCount];
				for (int i=0; i<localCount; i++)
					m_MtxPalette[i] = fwdMap[i];
			}
		}
	}

	// Fill in vertex buffer
	m_Stride = (u8) fvf.GetTotalSize();
	int vertsCount = mtl.GetVertexCount();

#if USE_EXTRA_VERTS
	m_ExtraVerts = (u8) extraVerts;
	vertsCount += m_ExtraVerts;
#endif

	const bool createReadWrite = true;
	m_VB[0] = grcVertexBuffer::Create(vertsCount, fvf, createReadWrite, bDynamic, NULL);
	{
		++g_AllowVertexBufferVramLocks;
		grcVertexBufferEditor vertexBufferEditor(m_VB[0]);

		for( int k = 0; k < mtl.GetVertexCount(); k++ ) 
		{
			vertexBufferEditor.SetVertex(k, mtl.GetVertex(k), reverseMap);
		}

		--g_AllowVertexBufferVramLocks;
	}
	if (!bReadWriteVtxBuf)
	{
		m_VB[0]->MakeReadOnly();
	}

	// Fill Index buffer
	const bool isGpuInstanced = params && params->IsGpuInstanced;
	m_IB[0] = grcIndexBuffer::Create(isGpuInstanced? m_IndexCount * grcInstanceBuffer::MaxPerDraw : m_IndexCount, bIsCloth);
	grcAssertf(m_IB[0], "Couldn't create index buffer");
	u16 *lockPtr = m_IB[0]->LockRW();
	grcAssertf(lockPtr, "Couldn't lock index buffer");

	int nIndex = 0;
	for( int i = 0; i < primCount; i++ )
	{
		const mshPrimitive& P = mtl.Prim[i];
		// flip the winding on odd-winding tristrips if we have restart support.
		if (P.Type == mshTRISTRIP2 && restarts) {
			int j = 0;
			Assign(lockPtr[nIndex++],P.Idx[j++]);
			while (j < P.Idx.GetCount()-1) {
				Assign(lockPtr[nIndex++],P.Idx[j+2]);
				Assign(lockPtr[nIndex++],P.Idx[j+2]);
				j+=2;
			}
			if (j != P.Idx.GetCount())
				Assign(lockPtr[nIndex++],P.Idx[j++]);
		}
		else {
			for( int j = 0; j < P.Idx.GetCount(); j++ )
			{
				u16 k = (u16) P.Idx[j];
				Assign(lockPtr[nIndex++], k);
				if (!restarts && tristripped && ((i && j==0) || (i+2!=primCount && j+2==P.Idx.GetCount()))) 
				{
					Assign(lockPtr[nIndex++], k);
				}
			}
		}
		if (restarts && i+2!=primCount)
			Assign(lockPtr[nIndex++], ~0U);
	}
	// When instancing, repeat the contents of the index buffer 7 times with a bias added in each time
	if (isGpuInstanced) {
		int c = m_IndexCount;
		for (int i=0; i<nIndex; i++) {
			for (int j=1; j<grcInstanceBuffer::MaxPerDraw; j++)
				Assign(lockPtr[i + j*c] , lockPtr[i] + j*c);
		}
	}

	const bool createIndexBufferVertStream = params && params->CreateIndexBufferVertStream;
	if (createIndexBufferVertStream)
	{
		Assertf(!(GetDoubleBuffered() & TRIPLE_VERTEX_BUFFER) && m_VB[3] == NULL, "WARNING! Cannot create index buffer vertex stream for tripple-buffered geometry!");

		grcFvf fvf;
		fvf.SetPosChannel(true, grcFvf::grcdsShort2);
		grcVertexBuffer *verts = grcVertexBuffer::Create(m_IndexCount, fvf, createReadWrite, false, NULL);
		grcAssertf(verts, "Couldn't create instancing vertex buffer");
		u32 *vertLock = static_cast<u32 *>(verts->LockWO());
		grcAssertf(vertLock, "Couldn't lock index buffer");

		for(u32 i = 0; i < m_IndexCount; ++i)
		{
			vertLock[i] = lockPtr[i] << 16 | lockPtr[i];
		}

		verts->UnlockWO();
		verts->MakeReadOnly();

		//We need to set this buffer to a resourced member of this class. Offset buffer is a datRef instead of datOwner, so resourcing will fail if it were used! So
		//instead, we'll store it in one of the quad-buffered vbs. This should be ok because the quad buffering should only be used for cloth, and this is only for grass.
		m_VB[3] = verts;
	}

	grcAssertf(nIndex == idxCount, "Index count mismatch - expecting %d and got %d", idxCount, nIndex);
	m_IB[0]->UnlockRW(); 

	m_VertexData = m_VB[0]->GetVertexData();
	m_IndexOffset = static_cast<grcIndexBufferGCM*>(m_IB[0].ptr)->GetGCMOffset();

	grmModelFactory::FreeDeclarator(m_VtxDecl);
	m_VtxDecl = NULL;
#if __WIN32PC
	if (!GRCDEVICE.IsCreated()) 
		m_VtxDecl = NULL; 
	else
#endif
		m_VtxDecl = grmModelFactory::BuildDeclarator(&fvf,NULL,NULL,NULL);

	// always succeeds in this implementation
	return true;
}

#endif		// MESH_LIBRARY
// PURPOSE: Render the geo packet as simple as possible
void grmGeometryQB::DirectDraw() const
{
	GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)m_PrimType,m_VtxDecl,*m_VB[0],*m_IB[0],m_IndexCount);

	PF_INCREMENT(grmGeometry_Draw_Calls);
	PF_INCREMENTBY(grmGeometry_Draw_Vertices,m_VertexCount);
	PF_INCREMENTBY(grmGeometry_Draw_Indices,m_IndexCount);
	PF_INCREMENTBY(grmGeometry_Draw_Primitives,m_PrimCount);

}
void grmGeometryQB::Prefetch() const
{
	PrefetchDC( m_VB[0] );
	PrefetchDC( m_IB[0] );
}

void grmGeometryQB::Draw() const { 
	Assert(m_VtxDecl != NULL);
	if (1) // WIN32PC_ONLY(if (m_VtxDecl))
	{
		unsigned int uDoubleBuffered = GetDoubleBuffered();
		const int nDrawVBIndex = (uDoubleBuffered & TRIPLE_VERTEX_BUFFER) ? GetDrawBuffer() : 0;
		const int nDrawIBIndex = (uDoubleBuffered & TRIPLE_INDEX_BUFFER) ? GetDrawBuffer() : 0;

		Assert(m_VB[nDrawVBIndex] && m_IB[nDrawIBIndex]);
		if (m_VB[nDrawVBIndex] && m_IB[nDrawIBIndex])
		{
			const grcVertexBuffer* pOffsets = m_OffsetBuffer;
			if(Unlikely(pOffsets))
			{
	#if __PS3 && SPU_GCM_FIFO >= 2
				grcCurrentVertexOffsets[1] = (u32)pOffsets;
	#else
				grcAssertf(s_CheckSecondaryStreamVertexLength == false || pOffsets->GetVertexCount() == m_VB[nDrawVBIndex]->GetVertexCount(), "You probably have a stale blend target manager");
				GRCDEVICE.SetStreamSource(1, *pOffsets, 0, pOffsets->GetVertexStride());
	#endif // __PS3 && SPU_GCM_FIFO >= 2
			
				GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)m_PrimType,m_VtxDeclOffset,*m_VB[nDrawVBIndex],*m_IB[nDrawIBIndex],m_IndexCount);
				GRCDEVICE.ClearStreamSource(1);
			}
			else
			{
				GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)m_PrimType,m_VtxDecl,*m_VB[nDrawVBIndex],*m_IB[nDrawIBIndex],m_IndexCount);
			}
		}

		PF_INCREMENT(grmGeometry_Draw_Calls);
		PF_INCREMENTBY(grmGeometry_Draw_Vertices,m_VertexCount);
		PF_INCREMENTBY(grmGeometry_Draw_Indices,m_IndexCount);
		PF_INCREMENTBY(grmGeometry_Draw_Primitives,m_PrimCount);
	}
}

void grmGeometryQB::DrawInstanced(grcInstanceBuffer *ib) const {
#if __XENON
	FLOAT instData[4] = { (float)m_IndexCount, 0, 0, 0 };
	GRCDEVICE.GetCurrent()->SetVertexShaderConstantF(INSTANCE_SHADER_CONTROL_SLOT,instData,1);
	for (grcInstanceBuffer *buf=ib; buf; buf=buf->GetNext()) {
		if(buf->GetCount() == 0)
			continue;

		buf->Bind();
		GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)m_PrimType,m_VtxDecl,*m_VB[0],*m_IB[0],m_IndexCount * buf->GetCount());
	}
#elif __PS3
	grcStateBlock::Flush();
	for (grcInstanceBuffer *buf=ib; buf; buf=buf->GetNext()) {
		if(buf->GetCount() == 0)
			continue;

		SPU_COMMAND(grcDevice__DrawInstancedPrimitive,CELL_GCM_PRIMITIVE_TRIANGLES);
		cmd->decl = (spuVertexDeclaration*) m_VtxDecl;
		cmd->vertexData[0] = (u32) const_cast<grcVertexBuffer&>(*m_VB[0]).GetVertexData();
		cmd->vertexData[1] = cmd->vertexData[2] = cmd->vertexData[3] = 0;
		cmd->indexData = ((grcIndexBufferGCM&)*m_IB[0]).GetGCMOffset();
		cmd->indexCount = m_IndexCount;
		buf->Bind(*cmd);
	}
#elif __D3D11
	GRCDEVICE.SetVertexDeclaration(m_VtxDecl);
	GRCDEVICE.SetIndices(*m_IB[0]);
	GRCDEVICE.SetStreamSource(0, *m_VB[0], 0, m_VB[0]->GetVertexStride());
	GRCDEVICE.SetUpPriorToDraw((grcDrawMode)m_PrimType);
	for (grcInstanceBuffer *buf=ib; buf; buf=buf->GetNext()) {
		if(buf->GetCount() == 0)
			continue;

		buf->Bind();
		GRCDEVICE.DrawInstancedIndexedPrimitive((grcDrawMode)m_PrimType,m_IndexCount,(int)buf->GetCount(),0,0,0,true);
	}
#elif RSG_ORBIS
	GRCDEVICE.SetVertexDeclaration(m_VtxDecl);
	GRCDEVICE.SetIndices(*m_IB[0]);
	GRCDEVICE.SetStreamSource(0, *m_VB[0], 0, m_VB[0]->GetVertexStride());
	GRCDEVICE.SetUpPriorToDraw((grcDrawMode)m_PrimType);
	Assertf(grcVertexProgram::GetCurrent(), "no currently bound vs.\n");
	for (grcInstanceBuffer *buf=ib; buf; buf=buf->GetNext()) {
		if(buf->GetCount() == 0)
			continue;

		buf->Bind();
		gfxc.setNumInstances(buf->GetCount());
		if (GRCDEVICE.NotifyDrawCall(1))
			gfxc.drawIndex(m_IndexCount,m_IB[0]->GetUnsafeReadPtr());
	}
	gfxc.setNumInstances(1);
#else
	for (grcInstanceBuffer *buf=ib; buf; buf=buf->GetNext()) {
		float *data = (float*) buf->GetData();
		size_t elemSizeQW = buf->GetElemSizeQW();
		if(buf->GetCount() == 0 || data == NULL)
			continue;

		for (size_t i=0; i<buf->GetCount(); i++, data += elemSizeQW * 4) {
			GRCDEVICE.SetVertexShaderConstant(INSTANCE_SHADER_CONSTANT_SLOT, data, elemSizeQW);
			// Tried hoisting the stream setup out on 360 and use DrawIndexedVertices, didn't make a measurable difference
			GRCDEVICE.DrawIndexedPrimitive((grcDrawMode)m_PrimType,m_VtxDecl,*m_VB[0],*m_IB[0],m_IndexCount);
		}
	}
#endif
}

void grmGeometryQB::EnableSkinnedMatrixPretest(bool enable)
{
	s_EnableSkinnedMatrixPreTest = enable;
}

void grmGeometryQB::ResetPacketMatrices(bool start, int mtxCount)  // this will be a member functions as some as I finish testing...
{
	if (start && mtxCount<MAX_TEST_MTX && s_EnableSkinnedMatrixPreTest)  // don't do it if we don't have enough indices
	{
		s_UseFastMtxTest = true;
		s_MtxTestCount = mtxCount;
		memset(s_MtxTestPallete,0xffff,mtxCount*sizeof(u16));
	}
	else
	{
		s_UseFastMtxTest = false; // so any high level draw code that does not know about use will still work...
	}
}

#if IDENTITY_MTX_PALETTE_IS_NULL
u16 grmGeometryQB::sm_OneToOne[252] = {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
	16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
	32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
	48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
	64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
	80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95,
	96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
	112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
	128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143,
	144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
	160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175,
	176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191,
	192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207,
	208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
	224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
	240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251
};
#endif

void grmGeometryQB::DrawSkinned(const grmMatrixSet &ms) const {
	const Matrix43 * RESTRICT mtx = ms.GetMatrices();
#if RSG_PS3 || (RSG_PC && !__D3D11)
	Matrix43 * RESTRICT dest = Alloca(Matrix43,m_MtxCount);
	for (int i=0; i<m_MtxCount; i++)
		dest[i] = mtx[m_MtxPalette[i]];
	GRCDEVICE.SetVertexShaderConstant(SKINNING_CBUFFER,(float*)dest,m_MtxCount*3);
#else
#if __XENON

	int vec4Count = ((m_MtxCount*3) + 3) & ~3;
    PrefetchDC( &m_MtxPalette[0] );

	// Always own the shader constants; this prevents the runtime from overwriting them with shadowed data in the Draw() below.
	GRCDEVICE.GetCurrent()->GpuOwnVertexShaderConstantF(SKINNING_CBUFFER,vec4Count);

	Matrix43 * RESTRICT dest;
	GRCDEVICE.GetCurrent()->GpuBeginVertexShaderConstantF4(SKINNING_CBUFFER, (D3DVECTOR4**)&dest,vec4Count);
		
	__vector4 T[3];

	for (int i=0; i<m_MtxCount; i++)
	{
		if( i< m_MtxCount-1)
			PrefetchDC( &mtx[m_MtxPalette[i+1]] );
			
		T[0] = __lvx( mtx,m_MtxPalette[i]*48 );
		T[1] = __lvx( mtx,m_MtxPalette[i]*48+16 );
		T[2] = __lvx( mtx,m_MtxPalette[i]*48+32 );

		__stvx_volatile( T[0], dest, (i*48) );
		__stvx_volatile( T[1], dest, (i*48)+16 );
		__stvx_volatile( T[2], dest, (i*48)+32 );
	}

	GRCDEVICE.GetCurrent()->GpuEndVertexShaderConstantF4();
#else
#if IDENTITY_MTX_PALETTE_IS_NULL
	if (m_MtxPalette == NULL)
		SKINNING_CBUFFER->SetDataDirect(mtx,ms.GetMatrixCount() * sizeof(Matrix43));
	else
#endif
	{
#if !MTX_IN_CB && !__RESOURCECOMPILER
		const u32 maxSkinningCount = SKINNING_CBUFFER->GetSize() / sizeof(Matrix43);

#if __ASSERT
		const char* modelName = "N/A";
		const char* drawableName = "N/A";
		DEV_ONLY(modelName = rmcLodGroup::debugRenderModel);
		DEV_ONLY(drawableName = rmcDrawable::sm_DebugDrawableName);
		grcAssertf(m_MtxCount <= ms.GetMatrixCount(), "This may require a simple re-export of the model '%s', drawable '%s' . See url:bugstar:1795795", modelName, drawableName);
#endif // __ASSERT

		grcAssertf(m_MtxCount <= 255, "Cannot handle this many bones");	// 255 == SKINNING_COUNT
		// NOTE: AH: It should be possible to remove this.
		u32 mtxCount = Min((u32)maxSkinningCount, (u32)m_MtxCount);
		mtxCount = Min(mtxCount, (u32)ms.GetMatrixCount());

		const u32 bufferSize = mtxCount*sizeof(Matrix43);
		Matrix43 * RESTRICT dest = (Matrix43*) GRCDEVICE.BeginVertexShaderConstantF(SKINNING_CBUFFER, bufferSize);
		
		// TODO: Optimise this case:
		//  1) Can do a memcpy() if the skeleton has fewer than SKINNING_COUNT bones
		//  2) Can actually just point the GPU at the source array in the first place (see IDENTITY_MTX_PALETTE_IS_NULL above).
		for (u32 i=0; i<mtxCount; i++)
			dest[i] = mtx[m_MtxPalette[i]];
//		memcpy((void*)dest, (void*)mtx, bufferSize);

		GRCDEVICE.EndVertexShaderConstantF(SKINNING_CBUFFER);
#elif !__RESOURCECOMPILER
		mtx;
		ms.Bind(SKINNING_CBUFFER);
#endif // MTX_IN_CB || !__RESOURCECOMPILER
	}
#endif
#endif

	Draw();

#if __XENON && defined(_DEBUG)
	// We only need to disown the constants in debug builds.  We skip this otherwise, because it has the side effect of marking
	// the variables dirty again, forcing them to be flushed by the next Draw() call.  This means we also don't need to do the
	// manual D3DTagCollection_Clear call here which had been effectively undoing the marking of them dirty.
	GRCDEVICE.GetCurrent()->GpuDisownVertexShaderConstantF(SKINNING_CBUFFER,vec4Count);
#endif // __XENON

#if MTX_IN_CB
	ms.UnBind();
#endif // MTX_IN_CB

}

void grmGeometryQB::SetOffsets(grcVertexBuffer* pOffsets)
{
	if (pOffsets && !m_VtxDeclOffset)
		m_VtxDeclOffset = grmModelFactory::BuildDeclarator(GetFvf(), pOffsets->GetFvf(), 0, 0);
	else if (!pOffsets && m_VtxDeclOffset) {
		grmModelFactory::FreeDeclarator(m_VtxDeclOffset);
		m_VtxDeclOffset = NULL;
	}
	m_OffsetBuffer = pOffsets;

#if __PS3 && SPU_GCM_FIFO >= 2
	m_OffsetBuffer = (grcVertexBuffer*)(m_OffsetBuffer ? m_OffsetBuffer->GetVertexData() : NULL);
#endif // __PS3 && SPU_GCM_FIFO >= 2
}

int grmGeometryQB::GetStride() const
{
	// Stride of the fvf and the vertex buffer may not match when doing cloth
	// because of 16-byte padding, so make sure we return the correct result.
	return m_VB[0]->GetVertexStride();
}

const grcFvf* grmGeometryQB::GetFvf() const
{
	return m_VB[0]->GetFvf();
}

void grmGeometryQB::MakeDynamic()
{
	// Reorder the vertex buffer(s)
	for (int i=0;i<sm_MultiBufferCount;i++)
	{
		if( m_VB[i] )
		{
			grcVertexBufferEditor vertexBufferEditor(m_VB[i]);
			vertexBufferEditor.MakeDynamic();
		}
	}


	// Recreate the vertex declarator
	grmModelFactory::FreeDeclarator(m_VtxDecl);
	m_VtxDecl = grmModelFactory::BuildDeclarator(GetFvf(), 0, 0, 0);
}


#if USE_EXTRA_VERTS

void grmGeometryQB::AllocFromExtra( int vertsToUse )
{
// NOTE: add tons of checks
	Assert( vertsToUse <= m_ExtraVerts );

	m_ExtraVerts -= (u8)vertsToUse;
	m_VertexCount += (u16) vertsToUse;
}

#endif

void grmGeometry::Place(grmGeometry *that,datResource &rsc)
{
	if (that->m_Type == GEOMETRYQB)
		::new (that) grmGeometryQB(rsc);
#if __PPU
	else if (that->m_Type == GEOMETRYEDGE)
		::new (that) grmGeometryEdge(rsc);
#endif
}

#if __DECLARESTRUCT
void grmGeometryQB::DeclareStruct(datTypeStruct &s) {	
	SSTRUCT_BEGIN(grmGeometryQB)
	SSTRUCT_FIELD_VP(grmGeometryQB, m_VtxDecl)
	SSTRUCT_FIELD(grmGeometryQB, m_Type)
	SSTRUCT_FIELD(grmGeometryQB, m_VB)
	SSTRUCT_FIELD(grmGeometryQB, m_IB)
	SSTRUCT_FIELD(grmGeometryQB, m_IndexCount)
	SSTRUCT_FIELD(grmGeometryQB, m_PrimCount)
	SSTRUCT_FIELD(grmGeometryQB, m_VertexCount)
	SSTRUCT_FIELD(grmGeometryQB, m_PrimType)
	SSTRUCT_FIELD(grmGeometryQB, m_DoubleBuffered)
	SSTRUCT_DYNAMIC_ARRAY(grmGeometryQB, m_MtxPalette, m_MtxCount)
	SSTRUCT_FIELD(grmGeometryQB, m_Stride)
#if USE_EXTRA_VERTS
	SSTRUCT_FIELD(grmGeometryQB, m_ExtraVerts)	
#endif
	SSTRUCT_FIELD(grmGeometryQB, m_MtxCount)
	SSTRUCT_FIELD_VP(grmGeometryQB, m_VertexData)
	SSTRUCT_FIELD_VP(grmGeometryQB, m_VtxDeclOffset)
	SSTRUCT_FIELD(grmGeometryQB, m_OffsetBuffer)
	SSTRUCT_FIELD(grmGeometryQB, m_IndexOffset)
	SSTRUCT_END(grmGeometryQB)
}
#endif


////////////// EDGE ///////////////

#if __RESOURCECOMPILER | __PPU

struct rageEdgeGeomPpuConfigInfo: public EdgeGeomPpuConfigInfo
{
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		// These are never stored in resource heaps
		blendShapes = NULL;
		blendShapeSizes = NULL;
		spuVertexesOffset0 = 0;

		SSTRUCT_BEGIN(rageEdgeGeomPpuConfigInfo)

		// Edge already byte swaps this for us
		SSTRUCT_SKIP(rageEdgeGeomPpuConfigInfo, spuConfigInfo, sizeof(EdgeGeomSpuConfigInfo))

		SSTRUCT_FIELD_VP(rageEdgeGeomPpuConfigInfo, indexes)
		SSTRUCT_CONTAINED_ARRAY(rageEdgeGeomPpuConfigInfo, indexesSizes)
		SSTRUCT_CONTAINED_ARRAY_VP(rageEdgeGeomPpuConfigInfo, spuVertexes)
		SSTRUCT_CONTAINED_ARRAY(rageEdgeGeomPpuConfigInfo, spuVertexesSizes)
		SSTRUCT_FIELD_VP(rageEdgeGeomPpuConfigInfo, rsxOnlyVertexes)
		SSTRUCT_FIELD(rageEdgeGeomPpuConfigInfo, rsxOnlyVertexesSize)
		SSTRUCT_CONTAINED_ARRAY(rageEdgeGeomPpuConfigInfo, skinMatricesByteOffsets)
		SSTRUCT_CONTAINED_ARRAY(rageEdgeGeomPpuConfigInfo, skinMatricesSizes)
		SSTRUCT_CONTAINED_ARRAY(rageEdgeGeomPpuConfigInfo, skinIndexesAndWeightsSizes)
		SSTRUCT_FIELD_VP(rageEdgeGeomPpuConfigInfo, skinIndexesAndWeights)
		SSTRUCT_FIELD(rageEdgeGeomPpuConfigInfo, ioBufferSize)
		SSTRUCT_FIELD(rageEdgeGeomPpuConfigInfo, scratchSizeInQwords)
		SSTRUCT_FIELD(rageEdgeGeomPpuConfigInfo, numBlendShapes)
		SSTRUCT_FIELD_VP(rageEdgeGeomPpuConfigInfo, blendShapeSizes)
		SSTRUCT_FIELD_VP(rageEdgeGeomPpuConfigInfo, blendShapes)
		SSTRUCT_CONTAINED_ARRAY(rageEdgeGeomPpuConfigInfo, fixedOffsetsSize)
		SSTRUCT_CONTAINED_ARRAY_VP(rageEdgeGeomPpuConfigInfo, fixedOffsets)
		SSTRUCT_CONTAINED_ARRAY_VP(rageEdgeGeomPpuConfigInfo, spuInputStreamDescs)
		SSTRUCT_FIELD_VP(rageEdgeGeomPpuConfigInfo, spuOutputStreamDesc)
		SSTRUCT_FIELD_VP(rageEdgeGeomPpuConfigInfo, rsxOnlyStreamDesc)
		SSTRUCT_CONTAINED_ARRAY(rageEdgeGeomPpuConfigInfo, spuInputStreamDescSizes)
		SSTRUCT_FIELD(rageEdgeGeomPpuConfigInfo, spuOutputStreamDescSize)
		SSTRUCT_FIELD(rageEdgeGeomPpuConfigInfo, rsxOnlyStreamDescSize)
		
		// Custom stuff
		SSTRUCT_FIELD(rageEdgeGeomPpuConfigInfo, versionTag)
		SSTRUCT_FIELD_VP(rageEdgeGeomPpuConfigInfo, baseAllocation)
		SSTRUCT_IGNORE(rageEdgeGeomPpuConfigInfo, spuVertexesOffset0)
		SSTRUCT_CONTAINED_ARRAY(rageEdgeGeomPpuConfigInfo, pad)

		SSTRUCT_END(rageEdgeGeomPpuConfigInfo)
	}
#endif
};

grmGeometryEdge::grmGeometryEdge() : grmGeometry(GEOMETRYEDGE)
{
}

grmGeometryEdge::~grmGeometryEdge()
{
	// Note: Deleting the data inside the rageEdgeGeomPpuConfigInfo here since 
	// introducing a destructor on that struct means the evil new [] size cookie
	// doesn't get swapped correctly.
	for (int i = 0; i != m_Count; ++i)
	{
		delete [] m_Data[i].baseAllocation;
#if __RESOURCECOMPILER
		physical_delete(m_Data[i].rsxOnlyVertexes);
		grcAssertf(!m_Data[i].blendShapes,"Should have called FreeBlendHeaders by now!"); 
#else
		physical_delete(gcm::LocalPtr((u32)m_Data[i].rsxOnlyVertexes));
		delete [] m_Data[i].blendShapes;
#endif // __RESOURCECOMPILER
	}

	delete [] m_Data;	
}

grmGeometryEdge::grmGeometryEdge(datResource &rsc)
: grmGeometry(rsc)
{
	rsc.PointerFixup(m_Data);
	for (int i = 0; i < m_Count; ++i)
	{
		// If defragmenting, might already have blendshapes attached.
		// They'll never be a part of this resource, so don't try to fix them up here, they 
		// need to be a trackable reference if they themselves are ever defragmented.
		grcAssertf(rsc.IsDefragmentation() || m_Data[i].numBlendShapes == 0, "Bad resource data - numBlendShapes = %d", m_Data[i].numBlendShapes);
		grcAssertf(rsc.IsDefragmentation() || m_Data[i].blendShapeSizes == NULL, "Bad resource data - expected null pointer, got %p", m_Data[i].blendShapeSizes);
		grcAssertf(rsc.IsDefragmentation() || m_Data[i].blendShapes == NULL, "Bad resource data - expected null pointer, got %p", m_Data[i].blendShapeSizes);

		// Pointer fixup
		rsc.PointerFixup(m_Data[i].indexes);
		rsc.PointerFixup(m_Data[i].spuVertexes[0]);
		rsc.PointerFixup(m_Data[i].spuVertexes[1]);
		rsc.PointerFixup(m_Data[i].skinIndexesAndWeights);
		rsc.PointerFixup(m_Data[i].fixedOffsets[0]);
		rsc.PointerFixup(m_Data[i].fixedOffsets[1]);
		rsc.PointerFixup(m_Data[i].spuInputStreamDescs[0]);
		rsc.PointerFixup(m_Data[i].spuInputStreamDescs[1]);
		rsc.PointerFixup(m_Data[i].spuOutputStreamDesc);
		rsc.PointerFixup(m_Data[i].rsxOnlyStreamDesc);

		// Custom stuff
		rsc.PointerFixup(m_Data[i].baseAllocation);

		if (m_Data[i].rsxOnlyVertexes) // Repair GCM offsets
		{
			if (rsc.IsDefragmentation())
			{
				m_Data[i].rsxOnlyVertexes = gcm::LocalPtr(u32(m_Data[i].rsxOnlyVertexes));
			}
			rsc.PointerFixup(m_Data[i].rsxOnlyVertexes);

			m_Data[i].rsxOnlyVertexes = (void*)gcm::LocalOffset(m_Data[i].rsxOnlyVertexes);
		}

		grcAssertf(m_Data[i].spuVertexes[0], "Bad resource data - missing position stream"); // We should always have a position only stream
		m_Data[i].spuVertexesOffset0 = gcm::MainOffset(m_Data[i].spuVertexes[0]);

#if !__FINAL
		// Check edge version tag
		if (m_Data->versionTag != u32(EDGE_SDK_VERSION))
		{
			// Your data is likely to be very seriously broken if you hit this error (ragebuilder
			// incompatible with rage)
			grcErrorf("Edge data not compatible with runtime - expected v%d.%d.%d.%d, got v%d.%d.%d.%d", 
				__EDGE_SDK_MAJOR, __EDGE_SDK_MINOR, __EDGE_SDK_DOT, __EDGE_SDK_BUILD,
				(m_Data->versionTag >> 20) & 0xfff, (m_Data->versionTag >> 16) & 0xf, (m_Data->versionTag >> 12) & 0xf, m_Data->versionTag & 0xfff
			);
		}
#endif // !__FINAL
	}

}

bool grmGeometryEdge::IsAcceptableChannelMask(u32 channelMask)
{
	return (channelMask != 25);
}

#if !__FINAL
// code replicated from grcVertexBufferEditor::SetVertex()
static Vector4 GetTanFlip(const Vector3& tan, const Vector3& bin, const Vector3& norm)
{
	Vector4 t4;
	Convert(t4, tan);
	// Determine the sign of the binormal
	Vector3 tan2 = tan;
	tan2.Cross(norm);

	// Doing a cross product will rarely yield a perfect match, so we have to do more magic

	// Grab the dominant term & use it for sign comparison
	Vector3 txnAbs;
	txnAbs.Abs(tan2);
	float testA, testB;
	if ( txnAbs.x > txnAbs.y ) {
		if ( txnAbs.x > txnAbs.z ) {
			testA = tan2.x;
			testB = bin.x;
		}
		else {
			testA = tan2.z;
			testB = bin.z;
		}
	}
	else if ( txnAbs.y > txnAbs.z ) {
		testA = tan2.y;
		testB = bin.y;
	}
	else {
		testA = tan2.z;
		testB = bin.z;
	}

	// Finally, do the comparison
	if ( (testA >= 0.f && testB >= 0) || (testA < 0.f && testB < 0.f) ) {
		t4.w = -1.0f;
	}
	else {
		t4.w = 1.0f;
	}

	return t4;
}

void grmGeometryEdge::FreeBlendHeaders()
{
	sysMemStartTemp();
	for (int i = 0; i < m_Count; ++i)
	{
		delete [] m_Data[i].blendShapes;
		m_Data[i].blendShapes = NULL;
		m_Data[i].numBlendShapes = 0;
	}
	sysMemEndTemp();
}

#if __DECLARESTRUCT
void grmGeometryEdgeBlendHeader::DeclareStruct(datTypeStruct &)
{
	int count = BlendCount;
	datSwapper(BlendCount);
	datSwapper(BlendSize);
	for (int i = 0; i < count; ++i)
	{
		datSwapper(Blends[i].Addr);
		datSwapper(Blends[i].Size);
		for (int k = 0; k < NELEM(Blends[i].Alpha); ++k)
		{
			datSwapper(Blends[i].Alpha[k]);
		}
	}
}
#endif // __DECLARESTRUCT

static void* rageEdgeGeomAllocFunc(size_t allocSize, const char* /*filename*/, const u32 /*lineNumber*/)
{
	return rage_new char[allocSize];
}

static void rageEdgeGeomFreeFunc(void *ptr, const char* /*filename*/, const u32 /*lineNumber*/)
{
	delete [] (char*&)ptr;
}

struct RageEdgeGeomCommandBufferHoleUserData
{
	u32 NumExtraVertexAttributes;
	u32 ChannelMask;
	bool HasDiffuse;
	bool HasSpecular;
};
static void rageEdgeGeomCommandBufferHoleSizeFunc(cell::Gcm::CellGcmContext* fakeContext, void* _userData)
{
	RageEdgeGeomCommandBufferHoleUserData* userData = (RageEdgeGeomCommandBufferHoleUserData*)_userData;
#if __PPU
	for (u32 i = 0; i < userData->NumExtraVertexAttributes; ++i)
	{
		cellGcmSetVertexDataArrayMeasure(fakeContext, 0, 0, 0, 0, CELL_GCM_VERTEX_F, CELL_GCM_LOCATION_LOCAL, 0);
	}

	float white[4] = {1,1,1,1};
	if(((userData->ChannelMask & grcFvf::grcfcDiffuseMask)==0) && (!userData->HasDiffuse))
	{
		cellGcmSetVertexData4fMeasure(fakeContext, grcFvf::grcfcDiffuse, white);
	}
	if(((userData->ChannelMask & grcFvf::grcfcSpecularMask)==0) && (!userData->HasSpecular))
	{
		cellGcmSetVertexData4fMeasure(fakeContext, grcFvf::grcfcSpecular, white);
	}
#else
	// 4 words/cellGcmSetVertexDataArray
	fakeContext->current += 4 * userData->NumExtraVertexAttributes;

	// 5 words/cellGcmSetVertexData4f
	if(((userData->ChannelMask & grcFvf::grcfcDiffuseMask)==0) && (!userData->HasDiffuse))
	{
		fakeContext->current += 5;
	}
	if(((userData->ChannelMask & grcFvf::grcfcSpecularMask)==0) && (!userData->HasSpecular))
	{
		fakeContext->current += 5;
	}
#endif // __PPU
}

bool grmGeometryEdge::Init(const mshMesh &mesh,int mtlIndex,u32 channelMask,const bool UNUSED_PARAM(isCharClothMesh),const bool UNUSED_PARAM(isEnvClothMesh),GeometryCreateParams* params, int /*extraVerts //for cloth only, not used here*/)
{
	RAGE_TRACK(Graphics);
	RAGE_TRACK(grmGeometryEdge);

	const mshMaterial& mtl = mesh.GetMtl(mtlIndex);
	grcAssertf(mtl.Prim.GetCount() == 1 && mtl.Prim[0].Type == mshTRIANGLES, "Invalid mesh for init. Prim count = %d, type = %d", mtl.Prim.GetCount(), mtl.Prim[0].Type);

#if HACK_GTA4
	if(params && params->EdgeTintColors && (!params->EdgeTintColorsTrees))
	{
		// EDGE Tint: remove specular from input (it will be generated during runtime):
		// Trees: they still require specular in input, as it holds their tint info
		channelMask &= ~grcFvf::grcfcSpecularMask;
	}
#endif
	grcAssertf((channelMask & grcFvf::grcfcPositionMask) != 0, "channelMask (0x%x) must include positions", channelMask);

	EdgeGeomSetAllocFunc(&rageEdgeGeomAllocFunc);
	EdgeGeomSetFreeFunc(&rageEdgeGeomFreeFunc);

	RageEdgeGeomCommandBufferHoleUserData commandBufferHoleSizeUserData;
	commandBufferHoleSizeUserData.HasDiffuse = false;
	commandBufferHoleSizeUserData.HasSpecular = false;

	bool success = true;
	m_Data = NULL;
	m_Count = 0;

	sysMemStartTemp(); // switch to temporary heap
	{
		bool isSkinned = mesh.IsSkinned() && !params->IsMicroMorph;
		bool hasBlendShapes = mesh.GetBlendTargetCount() > 0;

		EdgeGeomScene edgeScene;
		memset(&edgeScene, 0, sizeof(EdgeGeomScene));

		EdgeGeomSegmentFormat edgeFormat;
		memset(&edgeFormat, 0, sizeof(EdgeGeomSegmentFormat));
		edgeFormat.m_cullType = kCullBackFacesAndFrustum;
		edgeFormat.m_canBeOccluded = true;
		edgeFormat.m_indexesType = PARAM_edgenoindexcomp.Get() ? kIndexesU16TriangleListCCW : kIndexesCompressedTriangleListCCW;
		if (isSkinned)
		{
   			if (mtl.IsSingleBoneSkinned())
   			{
   				edgeFormat.m_skinType = kSkinSingleBoneNoScaling;
   			}
   			else
			{
				edgeFormat.m_skinType = kSkinNoScaling;
			}
		}
		else
		{
			edgeFormat.m_skinType = kSkinNone;
		}

		// Construct the triangle list
		u32 idxCount = mtl.Prim[0].Idx.GetCount();

		edgeScene.m_numTriangles = idxCount / 3;
		edgeScene.m_materialIdPerTriangle = (s32*)edgeGeomAlloc(edgeScene.m_numTriangles * sizeof(s32));
		memset(edgeScene.m_materialIdPerTriangle, 0, edgeScene.m_numTriangles * sizeof(s32));

		edgeScene.m_triangles = (u32*)(&mtl.Prim[0].Idx[0]);

		Vec3V v_min(V_FLT_MAX);
		for (int i = 0; i < mtl.GetVertexCount(); ++i)
		{
			const Vec3V v_pos(RCC_VEC3V(mtl.GetPos(i)));
			v_min = Min(v_min, v_pos);
		}

		const ScalarV v_one_s(V_ONE);
		const Vec4V v_one(v_one_s);
		Vec3V v_maxOffset(V_NEG_FLT_MAX);
		for (int i = 0; i < mtl.GetVertexCount(); ++i)
		{
			const Vec4V v_cpv(RCC_VEC4V(mtl.GetCpv(i)));
			const Vec4V v_cpv2(RCC_VEC4V(mtl.GetCpv2(i)));
			const Vec3V v_pos(RCC_VEC3V(mtl.GetPos(i)));
			v_maxOffset = Max(v_maxOffset, v_pos - v_min);
			commandBufferHoleSizeUserData.HasDiffuse |= IsEqualAll(v_cpv, v_one) == 0;
			commandBufferHoleSizeUserData.HasSpecular |= IsEqualAll(v_cpv2, v_one) == 0;
		}

		if (!commandBufferHoleSizeUserData.HasDiffuse)
		{
			channelMask &= ~grcFvf::grcfcDiffuseMask;
		}
		if (!commandBufferHoleSizeUserData.HasSpecular)
		{
			channelMask &= ~grcFvf::grcfcSpecularMask;
		}
		commandBufferHoleSizeUserData.ChannelMask = channelMask;

		u32 spuInputChannelMask = channelMask;
		// Stuff we don't care about
		spuInputChannelMask &= ~grcFvf::grcfcWeightMask;
		spuInputChannelMask &= ~grcFvf::grcfcBindingMask;
		
		const u32 specialCaseMask0 = grcFvf::grcfcPositionMask;
		const u32 specialCaseMask1 = specialCaseMask0 | grcFvf::grcfcNormalMask | grcFvf::grcfcTangent0Mask;
		const u32 specialCaseMask2 = specialCaseMask1 | grcFvf::grcfcBinormal0Mask;

		u32 spuOutputChannelMask = 0;
		PARAM_edgeoutputmask.Get(spuOutputChannelMask);

#if HACK_GTA4_EDGEFIXEDPOINTPOS
		u32 HACK_GTA4_EDGEFIXEDPOINTPOS_INT		= 8;
		u32 HACK_GTA4_EDGEFIXEDPOINTPOS_FRAC	= 12;
		u32 HACK_GTA4_EDGEFIXEDPOINTPOS_AUTO	= 12;
		
		if(params && params->EdgePedGeom)
		{	// increase frac quality for ped geometries:
			HACK_GTA4_EDGEFIXEDPOINTPOS_INT		= 6;
			HACK_GTA4_EDGEFIXEDPOINTPOS_FRAC	= 14;
			HACK_GTA4_EDGEFIXEDPOINTPOS_AUTO	= 14;
		}

		if(params && params->EdgeSlodGeom)
		{	// low quality pos for SLODs:
			HACK_GTA4_EDGEFIXEDPOINTPOS_INT		= 8;
			HACK_GTA4_EDGEFIXEDPOINTPOS_FRAC	= 1;
			HACK_GTA4_EDGEFIXEDPOINTPOS_AUTO	= 1;
		}

		int fixedPointParamCount = 1;
		Assert(fixedPointParamCount==1);
		bool useFixedPoint = true;
#else
		const char* fixedPointFormat[3];
		char buffer[128];
		int fixedPointParamCount = PARAM_edgefixedpointpos.GetArray(fixedPointFormat, 3, buffer, 128);
		grcAssertf(fixedPointParamCount != 2, "Only 1 or 3 fixed point params allowed");
		bool useFixedPoint = fixedPointParamCount > 0 || PARAM_edgefixedpointposauto.Get();
#endif
		if (useFixedPoint || hasBlendShapes)
		{
			spuOutputChannelMask |= grcFvf::grcfcPositionMask;
		}

		if (isSkinned || (USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS && hasBlendShapes))
		{
			spuOutputChannelMask |= channelMask & specialCaseMask2;
		}

#if HACK_GTA4
		if(params && params->EdgeTintColors)
		{
			// EDGE Tint: force COLOR0 stream to be in MainMem, add COLOR0+COLOR1 to output:
			if((spuInputChannelMask & grcFvf::grcfcDiffuseMask) == 0)
			{
				Errorf("EdgeTintColors: Diffuse color stream not present or all white (255,255,255,255)!");
			}
			spuOutputChannelMask |= grcFvf::grcfcDiffuseMask;
			spuOutputChannelMask |= grcFvf::grcfcSpecularMask;
		}

		if(params && params->EdgeVehicleDamage)
		{
			// Vehicle Damage SPU: force COLOR0 stream to be in MainMem (Color0.g contains damage scale):
			spuOutputChannelMask |= (spuInputChannelMask & grcFvf::grcfcDiffuseMask);	// force only if diffuse present in input
		}
#endif

		// Special case output vertex format
		if (PARAM_edgeoutputfloat4pos.Get())
		{
			switch (spuOutputChannelMask)
			{
			case specialCaseMask0:
				edgeFormat.m_spuOutputVertexFormat = edgeGeomGetRsxVertexFormat(EDGE_GEOM_RSX_VERTEX_FORMAT_F32c4);
				break;
			case specialCaseMask1:
				edgeFormat.m_spuOutputVertexFormat = edgeGeomGetRsxVertexFormat(EDGE_GEOM_RSX_VERTEX_FORMAT_F32c4_X11Y11Z10N_I16Nc4);
				break;
			case specialCaseMask2:
				edgeFormat.m_spuOutputVertexFormat = edgeGeomGetRsxVertexFormat(EDGE_GEOM_RSX_VERTEX_FORMAT_F32c4_X11Y11Z10N_X11Y11Z10N_X11Y11Z10N);
				break;
			default:
				break;
			}
		}
		else
		{
			switch (spuOutputChannelMask)
			{
			case specialCaseMask0:
				edgeFormat.m_spuOutputVertexFormat = edgeGeomGetRsxVertexFormat(EDGE_GEOM_RSX_VERTEX_FORMAT_F32c3);
				break;
			case specialCaseMask1:
				edgeFormat.m_spuOutputVertexFormat = edgeGeomGetRsxVertexFormat(EDGE_GEOM_RSX_VERTEX_FORMAT_F32c3_X11Y11Z10N_I16Nc4);
				break;
			case specialCaseMask2:
				edgeFormat.m_spuOutputVertexFormat = edgeGeomGetRsxVertexFormat(EDGE_GEOM_RSX_VERTEX_FORMAT_F32c3_X11Y11Z10N_X11Y11Z10N_X11Y11Z10N);
				break;
			default:
				break;
			}
		}
		
		// Everything else remains static in VRAM
		u32 rsxOnlyChannelMask = spuInputChannelMask & ~spuOutputChannelMask & ~grcFvf::grcfcPositionMask;

		// We only need to read in attributes which we're going to output from Edge
		spuInputChannelMask &= spuOutputChannelMask;
		spuInputChannelMask &= ~grcFvf::grcfcPositionMask;

#if USE_PACKED_TEXCOORDS
		u32 texCoordPackMask = 0;
		// Packed texture coordinates
		for(u32 i = 0; i < 6; ++i)
		{
			const u32 packMask = ( 3 << (i + grcFvf::grcfcTexture0));
			if (!(i & 1) && ((spuInputChannelMask & packMask) == packMask || (rsxOnlyChannelMask & packMask) == packMask))
			{
				spuInputChannelMask &= ~(1 << (i + grcFvf::grcfcTexture0 + 1));
				rsxOnlyChannelMask &= ~(1 << (i + grcFvf::grcfcTexture0 + 1));
				texCoordPackMask |= (1 << (i + grcFvf::grcfcTexture0));
			}
		}
#endif // USE_PACKED_TEXCOORDS

		EdgeGeomAttributeId tempAttributeIds[16];
		memset(tempAttributeIds, EDGE_GEOM_ATTRIBUTE_ID_UNKNOWN, sizeof(tempAttributeIds));
		u16 tempAttributeIndexes[16];
		memset(tempAttributeIndexes, 0, sizeof(tempAttributeIndexes));

		// Always assume primary stream with position only
		EdgeGeomSpuVertexFormat* inputSpuVertexFormat0;

		m_Offset.Zero();

		if (useFixedPoint)
		{
			inputSpuVertexFormat0 = (EdgeGeomSpuVertexFormat*)edgeGeomAlloc(sizeof(EdgeGeomSpuVertexFormat));
			inputSpuVertexFormat0->m_numAttributes = 1;

			EdgeGeomSpuVertexAttributeDefinition& attr = inputSpuVertexFormat0->m_attributeDefinition[0];
			attr.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_POSITION;
			attr.m_byteOffset = 0;
			attr.m_count = 3;
			attr.m_type = kSpuAttr_FixedPoint;
			attr.m_fixedPointBitDepthInteger[3] = 0;
			attr.m_fixedPointBitDepthFractional[3] = 0;

#if HACK_GTA4_EDGEFIXEDPOINTPOS
			int fracFormat = HACK_GTA4_EDGEFIXEDPOINTPOS_AUTO;
			bool useAutoFixedPoint = HACK_GTA4_EDGEFIXEDPOINTPOS_AUTO?1:0;
#else
			int fracFormat;
			bool useAutoFixedPoint = PARAM_edgefixedpointposauto.Get(fracFormat);
#endif
			if (useAutoFixedPoint)
			{
				for (int i = 0; i < 3; ++i)
				{
					attr.m_fixedPointBitDepthFractional[i] = fracFormat;
				}
				attr.m_fixedPointBitDepthInteger[0] = _FirstBitIndex(static_cast<u32>(v_maxOffset.GetXf())) + 1;
				attr.m_fixedPointBitDepthInteger[1] = _FirstBitIndex(static_cast<u32>(v_maxOffset.GetYf())) + 1;
				attr.m_fixedPointBitDepthInteger[2] = _FirstBitIndex(static_cast<u32>(v_maxOffset.GetZf())) + 1;
			}
			else
			{
#if HACK_GTA4_EDGEFIXEDPOINTPOS
				attr.m_fixedPointBitDepthInteger[0]		= HACK_GTA4_EDGEFIXEDPOINTPOS_INT;
				attr.m_fixedPointBitDepthFractional[0]	= HACK_GTA4_EDGEFIXEDPOINTPOS_FRAC;
#else
				for (int i = 0; i < fixedPointParamCount; ++i)
				{
					sscanf(fixedPointFormat[i], "%d.%d", &attr.m_fixedPointBitDepthInteger[i], &attr.m_fixedPointBitDepthFractional[i]);
				}
#endif
				// If only one format is specified use it for all components
				if (fixedPointParamCount == 1)
				{
					attr.m_fixedPointBitDepthInteger[1] = attr.m_fixedPointBitDepthInteger[2] = attr.m_fixedPointBitDepthInteger[0];
					attr.m_fixedPointBitDepthFractional[1] = attr.m_fixedPointBitDepthFractional[2] = attr.m_fixedPointBitDepthFractional[0];
				}
			}

			inputSpuVertexFormat0->m_vertexStride = 0;
			for (int i = 0; i < 3; ++i)
			{
				inputSpuVertexFormat0->m_vertexStride += attr.m_fixedPointBitDepthInteger[i] + attr.m_fixedPointBitDepthFractional[i];
			}
			if (useAutoFixedPoint)
			{
				u32 strideAligned = (inputSpuVertexFormat0->m_vertexStride + 7) & ~7;
				u32 strideAdjust = strideAligned - inputSpuVertexFormat0->m_vertexStride;
				// Try to distribute the integer bits across all three channels.  In the case of pedestrians,
				// it results in a nice 2.14 format that is much easier to understand instead of a bizarre 6.14,0.14,0.14 one.
				while (strideAdjust >= 3) {
					attr.m_fixedPointBitDepthInteger[0]++;
					attr.m_fixedPointBitDepthInteger[1]++;
					attr.m_fixedPointBitDepthInteger[2]++;
					inputSpuVertexFormat0->m_vertexStride += 3;
					strideAdjust -= 3;
				}
				attr.m_fixedPointBitDepthInteger[0] += strideAdjust; // Stick the extra bits on the x component
				inputSpuVertexFormat0->m_vertexStride += strideAdjust;
			}
			// Make sure we are multiples of a byte
			AlignedAssert(inputSpuVertexFormat0->m_vertexStride, 8);
			inputSpuVertexFormat0->m_vertexStride /= 8;
		}
		else
		{
			inputSpuVertexFormat0 = edgeGeomGetSpuVertexFormat(EDGE_GEOM_SPU_VERTEX_FORMAT_F32c3);
		}

		edgeFormat.m_spuInputVertexFormats[0] = inputSpuVertexFormat0;
		tempAttributeIds[edgeScene.m_numVertexAttributes++] = EDGE_GEOM_ATTRIBUTE_ID_POSITION;
		edgeScene.m_numFloatsPerVertex = 3;

		EdgeGeomSpuVertexFormat* inputSpuVertexFormat1 = NULL;
		if (spuInputChannelMask)
		{
			inputSpuVertexFormat1 = (EdgeGeomSpuVertexFormat*)edgeGeomAlloc(sizeof(EdgeGeomSpuVertexFormat));
			edgeFormat.m_spuInputVertexFormats[1] = inputSpuVertexFormat1;
			memset(inputSpuVertexFormat1, 0, sizeof(EdgeGeomSpuVertexFormat));
		}

		EdgeGeomRsxVertexFormat* rsxOnlyVertexFormat = NULL;
		if (rsxOnlyChannelMask)
		{
			rsxOnlyVertexFormat = (EdgeGeomRsxVertexFormat*)edgeGeomAlloc(sizeof(EdgeGeomRsxVertexFormat));
			edgeFormat.m_rsxOnlyVertexFormat = rsxOnlyVertexFormat;
			memset(rsxOnlyVertexFormat, 0, sizeof(EdgeGeomRsxVertexFormat));
		}

		EdgeGeomRsxVertexFormat* outputSpuVertexFormat = NULL;
		if (spuOutputChannelMask && !edgeFormat.m_spuOutputVertexFormat) // Could be special-case'ed above...
		{
			outputSpuVertexFormat = (EdgeGeomRsxVertexFormat*)edgeGeomAlloc(sizeof(EdgeGeomRsxVertexFormat));
			edgeFormat.m_spuOutputVertexFormat = outputSpuVertexFormat;
			memset(outputSpuVertexFormat, 0, sizeof(EdgeGeomRsxVertexFormat));
		}

		u32 currentChannelMask = spuInputChannelMask | spuOutputChannelMask | rsxOnlyChannelMask;
		for (int i = 0; currentChannelMask; ++i)
		{
			bool isChannelSet = (currentChannelMask & 1) != 0;
			currentChannelMask >>= 1;
			if (!isChannelSet)
				continue;

			EdgeGeomSpuVertexAttributeDefinition inputSpuAttributeDefinition;
			memset(&inputSpuAttributeDefinition, 0, sizeof(EdgeGeomSpuVertexAttributeDefinition));

			EdgeGeomRsxVertexAttributeDefinition rsxAttributeDefinition;
			memset(&rsxAttributeDefinition, 0, sizeof(EdgeGeomRsxVertexAttributeDefinition));
			
			tempAttributeIndexes[edgeScene.m_numVertexAttributes] = (u16)edgeScene.m_numFloatsPerVertex;

			switch (1 << i)
			{
			case grcFvf::grcfcPositionMask:
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_POSITION;
				inputSpuAttributeDefinition.m_count = 3;
				rsxAttributeDefinition.m_count = PARAM_edgeoutputfloat4pos.Get() ? 4 : 3;
				inputSpuAttributeDefinition.m_type = kSpuAttr_F32;
				rsxAttributeDefinition.m_type = kRsxAttr_F32;
				break;
			case grcFvf::grcfcNormalMask:
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_NORMAL;
				inputSpuAttributeDefinition.m_count = 3;
				inputSpuAttributeDefinition.m_type = kSpuAttr_UnitVector;
				rsxAttributeDefinition.m_count = 1;
				rsxAttributeDefinition.m_type = kRsxAttr_X11Y11Z10N;
				edgeScene.m_numFloatsPerVertex += 3;
				break;
			case grcFvf::grcfcDiffuseMask:
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_COLOR;
				inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 4;
				inputSpuAttributeDefinition.m_type = kSpuAttr_U8N;
				rsxAttributeDefinition.m_type = kRsxAttr_U8N;
				edgeScene.m_numFloatsPerVertex += inputSpuAttributeDefinition.m_count;
				break;
			case grcFvf::grcfcSpecularMask:
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_SPECULAR;
				inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 4;
				inputSpuAttributeDefinition.m_type = kSpuAttr_U8N;
				rsxAttributeDefinition.m_type = kRsxAttr_U8N;
				edgeScene.m_numFloatsPerVertex += inputSpuAttributeDefinition.m_count;
				break;
			case grcFvf::grcfcTexture0Mask:
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_UV0;
#if USE_PACKED_TEXCOORDS
				if (texCoordPackMask & grcFvf::grcfcTexture0Mask)
				{
					inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 4;
					edgeScene.m_numFloatsPerVertex += 4;
				}
				else
#endif // USE_PACKED_TEXCOORDS
				{
					inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 2;
					edgeScene.m_numFloatsPerVertex += 2;
				}
				inputSpuAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kSpuAttr_I16 : kSpuAttr_F16;
				rsxAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kRsxAttr_I16 : kRsxAttr_F16;
				break;
			case grcFvf::grcfcTexture1Mask:
				FastAssert(!USE_PACKED_TEXCOORDS);
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_UV1;
				inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 2;
				edgeScene.m_numFloatsPerVertex += 2;
				inputSpuAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kSpuAttr_I16 : kSpuAttr_F16;
				rsxAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kRsxAttr_I16 : kRsxAttr_F16;
				break;
			case grcFvf::grcfcTexture2Mask:
#if USE_PACKED_TEXCOORDS
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_UV1;
				if (texCoordPackMask & grcFvf::grcfcTexture2Mask)
				{
					inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 4;
					edgeScene.m_numFloatsPerVertex += 4;
				}
				else
#else
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_UV2;
#endif // USE_PACKED_TEXCOORDS
				{
					inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 2;
					edgeScene.m_numFloatsPerVertex += 2;
				}
				inputSpuAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kSpuAttr_I16 : kSpuAttr_F16;
				rsxAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kRsxAttr_I16 : kRsxAttr_F16;
				break;
			case grcFvf::grcfcTexture3Mask:
				FastAssert(!USE_PACKED_TEXCOORDS);
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_UV3;
				inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 2;
				edgeScene.m_numFloatsPerVertex += 2;
				inputSpuAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kSpuAttr_I16 : kSpuAttr_F16;
				rsxAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kRsxAttr_I16 : kRsxAttr_F16;
				break;
			case grcFvf::grcfcTexture4Mask:
#if USE_PACKED_TEXCOORDS
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_UV2;
				if (texCoordPackMask & grcFvf::grcfcTexture4Mask)
				{
					inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 4;
					edgeScene.m_numFloatsPerVertex += 4;
				}
				else
#else
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_UV4;
#endif // USE_PACKED_TEXCOORDS
				{
					inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 2;
					edgeScene.m_numFloatsPerVertex += 2;
				}
				inputSpuAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kSpuAttr_I16 : kSpuAttr_F16;
				rsxAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kRsxAttr_I16 : kRsxAttr_F16;
				break;
			case grcFvf::grcfcTexture5Mask:
				FastAssert(!USE_PACKED_TEXCOORDS);
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_UV5;
				inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 2;
				edgeScene.m_numFloatsPerVertex += 2;
				inputSpuAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kSpuAttr_I16 : kSpuAttr_F16;
				rsxAttributeDefinition.m_type = PARAM_useshortuvs.Get() ? kRsxAttr_I16 : kRsxAttr_F16;
				break;
			case grcFvf::grcfcTangent0Mask:
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_TANGENT;
				inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 4;
				inputSpuAttributeDefinition.m_type = kSpuAttr_UnitVector;
				rsxAttributeDefinition.m_type = kRsxAttr_I16N;

				edgeScene.m_numFloatsPerVertex += 4;
				break;
			case grcFvf::grcfcBinormal0Mask:
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_BINORMAL;
				inputSpuAttributeDefinition.m_count = 3;
				inputSpuAttributeDefinition.m_type = kSpuAttr_UnitVector;
				rsxAttributeDefinition.m_count = 1;
				rsxAttributeDefinition.m_type = kRsxAttr_X11Y11Z10N;
				edgeScene.m_numFloatsPerVertex += 3;
				break;
			case grcFvf::grcfcTangent1Mask:
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_TANGENT1;
				inputSpuAttributeDefinition.m_count = rsxAttributeDefinition.m_count = 4;
				inputSpuAttributeDefinition.m_type = kSpuAttr_UnitVector;
				rsxAttributeDefinition.m_type = kRsxAttr_I16N;

				edgeScene.m_numFloatsPerVertex += 4;
				break;
			case grcFvf::grcfcBinormal1Mask:
				inputSpuAttributeDefinition.m_attributeId = rsxAttributeDefinition.m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_BINORMAL1;
				inputSpuAttributeDefinition.m_count = 3;
				inputSpuAttributeDefinition.m_type = kSpuAttr_UnitVector;
				rsxAttributeDefinition.m_count = 1;
				rsxAttributeDefinition.m_type = kRsxAttr_X11Y11Z10N;
				edgeScene.m_numFloatsPerVertex += 3;
				break;
			default:
				// We don't care about this attribute, so don't store its offset
				tempAttributeIndexes[edgeScene.m_numVertexAttributes] = 0;
				continue;
			}

			grcAssertf(!tempAttributeIds[edgeScene.m_numVertexAttributes], "Bad attr ID. Expecting 0, got %d", tempAttributeIds[edgeScene.m_numVertexAttributes]);

			if ((spuInputChannelMask & (1 << i)) != 0)
			{
				// Copy members from our temporary attribute definition
				if (inputSpuVertexFormat1)
				{
					inputSpuVertexFormat1->m_attributeDefinition[inputSpuVertexFormat1->m_numAttributes].m_attributeId = inputSpuAttributeDefinition.m_attributeId;
					inputSpuVertexFormat1->m_attributeDefinition[inputSpuVertexFormat1->m_numAttributes].m_count = inputSpuAttributeDefinition.m_count;
					inputSpuVertexFormat1->m_attributeDefinition[inputSpuVertexFormat1->m_numAttributes].m_type = inputSpuAttributeDefinition.m_type;
					inputSpuVertexFormat1->m_attributeDefinition[inputSpuVertexFormat1->m_numAttributes].m_byteOffset = inputSpuVertexFormat1->m_vertexStride;
					inputSpuVertexFormat1->m_vertexStride += edgeGeomGetSpuVertexAttributeSize(inputSpuAttributeDefinition);
					++inputSpuVertexFormat1->m_numAttributes;
				}

				tempAttributeIds[edgeScene.m_numVertexAttributes] = inputSpuAttributeDefinition.m_attributeId;
				++edgeScene.m_numVertexAttributes;
			}
			else if ((rsxOnlyChannelMask & (1 << i)) != 0)
			{
				// Copy members from our temporary attribute definition
				if (rsxOnlyVertexFormat)
				{
					rsxOnlyVertexFormat->m_attributeDefinition[rsxOnlyVertexFormat->m_numAttributes].m_attributeId = rsxAttributeDefinition.m_attributeId;
					rsxOnlyVertexFormat->m_attributeDefinition[rsxOnlyVertexFormat->m_numAttributes].m_count = rsxAttributeDefinition.m_count;
					rsxOnlyVertexFormat->m_attributeDefinition[rsxOnlyVertexFormat->m_numAttributes].m_type = rsxAttributeDefinition.m_type;
					rsxOnlyVertexFormat->m_attributeDefinition[rsxOnlyVertexFormat->m_numAttributes].m_byteOffset = rsxOnlyVertexFormat->m_vertexStride;
					rsxOnlyVertexFormat->m_vertexStride += edgeGeomGetRsxVertexAttributeSize(rsxAttributeDefinition);
					++rsxOnlyVertexFormat->m_numAttributes;
				}

				tempAttributeIds[edgeScene.m_numVertexAttributes] = rsxAttributeDefinition.m_attributeId;
				++edgeScene.m_numVertexAttributes;
			}

			if (outputSpuVertexFormat && (spuOutputChannelMask & (1 << i)) != 0)
			{
				// Copy members from our temporary attribute definition
				outputSpuVertexFormat->m_attributeDefinition[outputSpuVertexFormat->m_numAttributes].m_attributeId = rsxAttributeDefinition.m_attributeId;
				outputSpuVertexFormat->m_attributeDefinition[outputSpuVertexFormat->m_numAttributes].m_count = rsxAttributeDefinition.m_count;
				outputSpuVertexFormat->m_attributeDefinition[outputSpuVertexFormat->m_numAttributes].m_type = rsxAttributeDefinition.m_type;
				outputSpuVertexFormat->m_attributeDefinition[outputSpuVertexFormat->m_numAttributes].m_byteOffset = outputSpuVertexFormat->m_vertexStride;
				outputSpuVertexFormat->m_vertexStride += edgeGeomGetRsxVertexAttributeSize(rsxAttributeDefinition);
				++outputSpuVertexFormat->m_numAttributes;
			}

			grcAssertf(edgeScene.m_numVertexAttributes <= NELEM(tempAttributeIds), "Too many vertex attributes. num (%d) should be <= %d", edgeScene.m_numVertexAttributes, NELEM(tempAttributeIds));
		}

		edgeScene.m_vertexAttributeIds = (EdgeGeomAttributeId*)edgeGeomAlloc(edgeScene.m_numVertexAttributes * sizeof(EdgeGeomAttributeId)); // possibly more than we need, but hey
		edgeScene.m_vertexAttributeIndexes = (u16*)edgeGeomAlloc(edgeScene.m_numVertexAttributes * sizeof(u16)); // ditto

		// Copy from the stack into our correctly sized attribute arrays
		for (int i = 0; i < edgeScene.m_numVertexAttributes; ++i)
		{
			edgeScene.m_vertexAttributeIds[i] = tempAttributeIds[i];
			edgeScene.m_vertexAttributeIndexes[i] = tempAttributeIndexes[i];
		}

		grcAssertf(inputSpuVertexFormat0 && edgeGeomSpuVertexFormatIsValid(*inputSpuVertexFormat0), "Invalid edge SPU vertex format");
		grcAssertf(!inputSpuVertexFormat1 || edgeGeomSpuVertexFormatIsValid(*inputSpuVertexFormat1), "Invalid edge SPU vertex format");
		grcAssertf(!rsxOnlyVertexFormat || edgeGeomRsxVertexFormatIsValid(*rsxOnlyVertexFormat), "Invalid edge RSX vertex format");
		grcAssertf(!edgeFormat.m_spuOutputVertexFormat || edgeGeomRsxVertexFormatIsValid(*edgeFormat.m_spuOutputVertexFormat), "Invalid edge RSX vertex format");

		edgeScene.m_numVertexes = mtl.GetVertexCount();

		if (hasBlendShapes)
		{
			edgeFormat.m_spuInputVertexDeltaFormat = (EdgeGeomSpuVertexFormat*)edgeGeomAlloc(sizeof(EdgeGeomSpuVertexFormat));
			memset(edgeFormat.m_spuInputVertexDeltaFormat, 0, sizeof(EdgeGeomSpuVertexFormat));

			u32 currentChannelMask = spuOutputChannelMask;
#if !USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
			currentChannelMask &= ~grcFvf::grcfcNormalMask;
			currentChannelMask &= ~grcFvf::grcfcTangent0Mask;
			currentChannelMask &= ~grcFvf::grcfcBinormal0Mask;
#endif // !USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
			for (u32 i = 0; currentChannelMask; ++i)
			{
				bool isChannelSet = (currentChannelMask & 1) != 0;
				currentChannelMask >>= 1;
				if (!isChannelSet)
					continue;

				#if HACK_GTA4
					// set byteOffset for the attribute (or we could call FinalizeSpuVertexFormat() afterwards):
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_byteOffset = edgeFormat.m_spuInputVertexDeltaFormat->m_vertexStride;
				#endif
				
				switch (1 << i)
				{
				case grcFvf::grcfcPositionMask:
#if HACK_GTA4
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_POSITION;
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_type = kSpuAttr_F32;
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_count = 3;
					edgeFormat.m_spuInputVertexDeltaFormat->m_vertexStride += 12;
#else
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_POSITION;
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_type = kSpuAttr_FixedPoint;
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_count = 3;
					for (u32 j = 0; j < edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_count; ++j)
					{
						edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_fixedPointBitDepthInteger[j] = 4;
						edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_fixedPointBitDepthFractional[j] = 12;
					}
					edgeFormat.m_spuInputVertexDeltaFormat->m_vertexStride += 6;
#endif
					break;
				case grcFvf::grcfcNormalMask:
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_NORMAL;
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_type = kSpuAttr_F16; // do NOT use a unit vector for vertex deltas; it will always have a magnitude of 1.0!
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_count = 3;
					edgeFormat.m_spuInputVertexDeltaFormat->m_vertexStride += 6;
					break;
				case grcFvf::grcfcTangent0Mask:
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_TANGENT;
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_type = kSpuAttr_F16; // do NOT use a unit vector for vertex deltas; it will always have a magnitude of 1.0!
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_count = 3;
					edgeFormat.m_spuInputVertexDeltaFormat->m_vertexStride += 6;
					break;
				case grcFvf::grcfcBinormal0Mask:
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_attributeId = EDGE_GEOM_ATTRIBUTE_ID_BINORMAL;
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_type = kSpuAttr_F16; // do NOT use a unit vector for vertex deltas; it will always have a magnitude of 1.0!
					edgeFormat.m_spuInputVertexDeltaFormat->m_attributeDefinition[edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes].m_count = 3;
					edgeFormat.m_spuInputVertexDeltaFormat->m_vertexStride += 6;
					break;
				default:
					continue;
				}

				++edgeFormat.m_spuInputVertexDeltaFormat->m_numAttributes;
				edgeScene.m_numFloatsPerDelta += 3;
				++edgeScene.m_numBlendedAttributes;
			}

			grcAssertf(edgeGeomSpuVertexFormatIsValid(*edgeFormat.m_spuInputVertexDeltaFormat), "Invalid edge SPU vertex format");
			
			edgeScene.m_numBlendShapes = mesh.GetBlendTargetCount();
			edgeScene.m_vertexDeltas = (float*)edgeGeomAlloc(edgeScene.m_numFloatsPerDelta * edgeScene.m_numVertexes * edgeScene.m_numBlendShapes * sizeof(float));
			edgeScene.m_blendedAttributeIndexes = (u16*)edgeGeomAlloc(edgeScene.m_numBlendedAttributes * sizeof(u16));
			edgeScene.m_blendedAttributeIds = (EdgeGeomAttributeId*)edgeGeomAlloc(edgeScene.m_numBlendedAttributes * sizeof(EdgeGeomAttributeId));

			{
				for (u16 i = 0, j = 0; i < edgeScene.m_numVertexAttributes; ++i)
				{
					switch (edgeScene.m_vertexAttributeIds[i])
					{
#if USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
					case EDGE_GEOM_ATTRIBUTE_ID_TANGENT:
					case EDGE_GEOM_ATTRIBUTE_ID_NORMAL:
					case EDGE_GEOM_ATTRIBUTE_ID_BINORMAL:
#endif // USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS
					case EDGE_GEOM_ATTRIBUTE_ID_POSITION:
						edgeScene.m_blendedAttributeIndexes[j] = j * 3;
						edgeScene.m_blendedAttributeIds[j] = edgeScene.m_vertexAttributeIds[i];
						++j;
						break;
					default:
						break;
					}
				}
			}

			// Fill in blend shape deltas here.
			// But in what order? blendShape is outermost, then vertices, then pos and normal for each vertex?
			float* destVertexDelta = edgeScene.m_vertexDeltas;
			for (u32 i = 0; i < edgeScene.m_numBlendShapes; ++i)
			{
				for (u32 j = 0; j < edgeScene.m_numVertexes; ++j, destVertexDelta += edgeScene.m_numFloatsPerDelta)
				{
					const mshBlendTargetDelta& delta = mtl.GetBlendTargetDelta(j,i);
					for (u32 k = 0; k < edgeScene.m_numBlendedAttributes; ++k)
					{
						u32 attributeIndex = edgeScene.m_blendedAttributeIndexes[k];

						switch (edgeScene.m_blendedAttributeIds[k])
						{
						case EDGE_GEOM_ATTRIBUTE_ID_TANGENT:
							if (delta.Normal.Mag2() < 1.0f / (128 * 128))
							{
								destVertexDelta[attributeIndex + 0] = 0.0f;
								destVertexDelta[attributeIndex + 1] = 0.0f;
								destVertexDelta[attributeIndex + 2] = 0.0f;
							}
							else
							{
								destVertexDelta[attributeIndex + 0] = delta.Tangent.x;
								destVertexDelta[attributeIndex + 1] = delta.Tangent.y;
								destVertexDelta[attributeIndex + 2] = delta.Tangent.z;
							}
							break;
						case EDGE_GEOM_ATTRIBUTE_ID_NORMAL:
							if (delta.Normal.Mag2() < 1.0f / (128 * 128))
							{
								destVertexDelta[attributeIndex + 0] = 0.0f;
								destVertexDelta[attributeIndex + 1] = 0.0f;
								destVertexDelta[attributeIndex + 2] = 0.0f;
							}
							else
							{
								destVertexDelta[attributeIndex + 0] = delta.Normal.x;
								destVertexDelta[attributeIndex + 1] = delta.Normal.y;
								destVertexDelta[attributeIndex + 2] = delta.Normal.z;
							}
							break;
						case EDGE_GEOM_ATTRIBUTE_ID_BINORMAL:
							if (delta.Normal.Mag2() < 1.0f / (128 * 128))
							{
								destVertexDelta[attributeIndex + 0] = 0.0f;
								destVertexDelta[attributeIndex + 1] = 0.0f;
								destVertexDelta[attributeIndex + 2] = 0.0f;
							}
							else
							{
								destVertexDelta[attributeIndex + 0] = delta.BiNormal.x;
								destVertexDelta[attributeIndex + 1] = delta.BiNormal.y;
								destVertexDelta[attributeIndex + 2] = delta.BiNormal.z;
							}
							break;
						case EDGE_GEOM_ATTRIBUTE_ID_POSITION:
							// Edge code doesn't check quantized value goes to zero, so we fudge that here.
							if (delta.Position.Mag2() < 1.0f / (2048 * 2048))
							{
								destVertexDelta[attributeIndex + 0] = 0.0f;
								destVertexDelta[attributeIndex + 1] = 0.0f;
								destVertexDelta[attributeIndex + 2] = 0.0f;
							}
							else
							{
								destVertexDelta[attributeIndex + 0] = delta.Position.x;
								destVertexDelta[attributeIndex + 1] = delta.Position.y;
								destVertexDelta[attributeIndex + 2] = delta.Position.z;
							}
							break;
						default:
							break;
						}
					}
				}
			}
			grcAssertf(destVertexDelta == edgeScene.m_vertexDeltas + edgeScene.m_numFloatsPerDelta * edgeScene.m_numVertexes * edgeScene.m_numBlendShapes, "Vertex delta mismatch");
		}

		edgeScene.m_vertexes = (float*)edgeGeomAlloc(edgeScene.m_numVertexes * edgeScene.m_numFloatsPerVertex * sizeof(float));

		// loop over the vertexes and copy the data
		for (u32 i = 0; i < edgeScene.m_numVertexes; ++i)
		{
			// skip to this vertex's floats
			float* destVertex = edgeScene.m_vertexes + i * edgeScene.m_numFloatsPerVertex;

			// loop over inputs
			for (u8 attrIdx = 0; attrIdx < edgeScene.m_numVertexAttributes; ++attrIdx)
			{
				float* dest = destVertex + edgeScene.m_vertexAttributeIndexes[attrIdx];

#if USE_PACKED_TEXCOORDS || __ASSERT
				u32 count;
				if (attrIdx < edgeScene.m_numVertexAttributes - 1)
				{
					count = edgeScene.m_vertexAttributeIndexes[attrIdx + 1] - edgeScene.m_vertexAttributeIndexes[attrIdx];
				}
				else
				{
					count = edgeScene.m_numFloatsPerVertex - edgeScene.m_vertexAttributeIndexes[attrIdx];
				}
#endif // USE_PACKED_TEXCOORDS || __ASSERT

				switch (edgeScene.m_vertexAttributeIds[attrIdx])
				{
				case EDGE_GEOM_ATTRIBUTE_ID_POSITION:
					dest[0] = mtl.GetPos(i).x;
					dest[1] = mtl.GetPos(i).y;
					dest[2] = mtl.GetPos(i).z;
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_NORMAL:
					dest[0] = mtl.GetNormal(i).x;
					dest[1] = mtl.GetNormal(i).y;
					dest[2] = mtl.GetNormal(i).z;
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_TANGENT:
					{
						// Compute tangent flip factor
						Vector3 tan = mtl.GetVertex(i).TanBi[0].T;
						Vector3 bin = mtl.GetVertex(i).TanBi[0].B;
						Vector3 norm = mtl.GetNormal(i);
						Vector4 tanFlip = GetTanFlip(tan, bin, norm);
						dest[0] = tanFlip.x;
						dest[1] = tanFlip.y;
						dest[2] = tanFlip.z;
						dest[3] = tanFlip.w;
					}
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_BINORMAL:
					dest[0] = mtl.GetVertex(i).TanBi[0].B.x;
					dest[1] = mtl.GetVertex(i).TanBi[0].B.y;
					dest[2] = mtl.GetVertex(i).TanBi[0].B.z;
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_UV0:
#if USE_PACKED_TEXCOORDS
					if (count == 4)
					{
						dest[2] = mtl.GetTexCoord(i, 1).x;
						dest[3] = 1.0f - mtl.GetTexCoord(i, 1).y;
					}
					else
#endif // USE_PACKED_TEXCOORDS
					{
						FastAssert(count == 2);
					}
					dest[0] = mtl.GetTexCoord(i, 0).x;
					dest[1] = 1.0f - mtl.GetTexCoord(i, 0).y;
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_UV1:
#if USE_PACKED_TEXCOORDS
					dest[0] = mtl.GetTexCoord(i, 2).x;
					dest[1] = 1.0f - mtl.GetTexCoord(i, 2).y;
					if (count == 4)
					{
						dest[2] = mtl.GetTexCoord(i, 3).x;
						dest[3] = 1.0f - mtl.GetTexCoord(i, 3).y;
					}
					else
#else
					dest[0] = mtl.GetTexCoord(i, 1).x;
					dest[1] = 1.0f - mtl.GetTexCoord(i, 1).y;
#endif // USE_PACKED_TEXCOORDS
					{
						FastAssert(count == 2);
					}
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_UV2:
#if USE_PACKED_TEXCOORDS
					dest[0] = mtl.GetTexCoord(i, 4).x;
					dest[1] = 1.0f - mtl.GetTexCoord(i, 4).y;
					if (count == 4)
					{
						dest[2] = mtl.GetTexCoord(i, 5).x;
						dest[3] = 1.0f - mtl.GetTexCoord(i, 5).y;
					}
					else
#else
					dest[0] = mtl.GetTexCoord(i, 2).x;
					dest[1] = 1.0f - mtl.GetTexCoord(i, 2).y;
#endif // USE_PACKED_TEXCOORDS
					{
						FastAssert(count == 2);
					}
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_UV3:
					FastAssert(!USE_PACKED_TEXCOORDS && count == 2);
					dest[0] = mtl.GetTexCoord(i, 3).x;
					dest[1] = 1.0f - mtl.GetTexCoord(i, 3).y;
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_UV4:
					FastAssert(!USE_PACKED_TEXCOORDS && count == 2);
					dest[0] = mtl.GetTexCoord(i, 4).x;
					dest[1] = 1.0f - mtl.GetTexCoord(i, 4).y;
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_UV5:
					FastAssert(!USE_PACKED_TEXCOORDS && count == 2);
					dest[0] = mtl.GetTexCoord(i, 5).x;
					dest[1] = 1.0f - mtl.GetTexCoord(i, 5).y;
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_COLOR:
					dest[0] = mtl.GetCpv(i).x;
					dest[1] = mtl.GetCpv(i).y;
					dest[2] = mtl.GetCpv(i).z;
					dest[3] = mtl.GetCpv(i).w;
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_SPECULAR:
					dest[0] = mtl.GetCpv2(i).x;
					dest[1] = mtl.GetCpv2(i).y;
					dest[2] = mtl.GetCpv2(i).z;
					dest[3] = mtl.GetCpv2(i).w;
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_TANGENT1:
					{
						// Compute tangent flip factor
						Vector3 tan = mtl.GetVertex(i).TanBi[1].T;
						Vector3 bin = mtl.GetVertex(i).TanBi[1].B;
						Vector3 norm = mtl.GetNormal(i);
						Vector4 tanFlip = GetTanFlip(tan, bin, norm);
						dest[0] = tanFlip.x;
						dest[1] = tanFlip.y;
						dest[2] = tanFlip.z;
						dest[3] = tanFlip.w;
					}
					break;
				case EDGE_GEOM_ATTRIBUTE_ID_BINORMAL1:
					dest[0] = mtl.GetVertex(i).TanBi[1].B.x;
					dest[1] = mtl.GetVertex(i).TanBi[1].B.y;
					dest[2] = mtl.GetVertex(i).TanBi[1].B.z;
					break;
				default:
					grcErrorf("Unsupported attribute ID");
					continue;
				}
			}
		}

		// Allocate and copy in skinning information if it's present.
		if (isSkinned)
		{
			edgeScene.m_matrixIndexesPerVertex = (s32*)edgeGeomAlloc(edgeScene.m_numVertexes * 4 * sizeof(s32));
			edgeScene.m_skinningWeightsPerVertex = (float*)edgeGeomAlloc(edgeScene.m_numVertexes * 4 * sizeof(float));
			edgeScene.m_passThroughPerVertex = (bool*)edgeGeomAlloc(edgeScene.m_numVertexes * sizeof(bool));

			// Make sure we pass in zero weights for subsequent bones when we
			// are single bone skinning otherwise Edge complains
			if (mtl.IsSingleBoneSkinned())
			{
				for (u32 i = 0; i < edgeScene.m_numVertexes; ++i)
				{
					edgeScene.m_matrixIndexesPerVertex[i * 4] = mtl.GetBinding(i).Mtx[0];
					edgeScene.m_skinningWeightsPerVertex[i * 4] = mtl.GetBinding(i).Wgt[0];
					edgeScene.m_passThroughPerVertex[i] = mtl.GetBinding(i).IsPassThrough;
					for (u32 j = 1; j < 4; ++j)
					{
						edgeScene.m_matrixIndexesPerVertex[i * 4 + j] = 0;
						edgeScene.m_skinningWeightsPerVertex[i * 4 + j] = 0.0f;
					}
				}
			}
			else
			{
				for (u32 i = 0; i < edgeScene.m_numVertexes; ++i)
				{
					for (u32 j = 0; j < 4; ++j)
					{
						edgeScene.m_matrixIndexesPerVertex[i * 4 + j] = mtl.GetBinding(i).Mtx[j];
						edgeScene.m_skinningWeightsPerVertex[i * 4 + j] = mtl.GetBinding(i).Wgt[j];
					}
					edgeScene.m_passThroughPerVertex[i] = mtl.GetBinding(i).IsPassThrough;
				}
			}
		}

		EdgeGeomSegment* segments = NULL;
		u32 numSegments = 0;
		// 15 instead of 16, because we explicitly always bind ATTR0 (position)
		commandBufferHoleSizeUserData.NumExtraVertexAttributes = 15 - (edgeFormat.m_spuOutputVertexFormat ? edgeFormat.m_spuOutputVertexFormat->m_numAttributes : 0);
		edgeGeomPartitionSceneIntoSegments(edgeScene, edgeFormat, &segments, &numSegments, NULL, &rageEdgeGeomCommandBufferHoleSizeFunc, &commandBufferHoleSizeUserData, params->SkipCentroidGeneration);

		// switch back to resource heap to copy output
		sysMemEndTemp();

		// write out each partition
		m_Count = numSegments;
		m_Data = rage_contained_new rageEdgeGeomPpuConfigInfo[m_Count];
		for (int i = 0; i < m_Count; ++i)
		{
			EdgeGeomSpuConfigInfo* spuConfigInfo = (EdgeGeomSpuConfigInfo*)segments[i].m_spuConfigInfo;
			memset(&m_Data[i], 0, sizeof(EdgeGeomPpuConfigInfo));
			CompileTimeAssert(sizeof(rageEdgeGeomPpuConfigInfo) == sizeof(EdgeGeomPpuConfigInfo));
			memcpy(&m_Data[i].spuConfigInfo, spuConfigInfo, sizeof(EdgeGeomSpuConfigInfo));
			// set version tag
			m_Data[i].versionTag = u32(EDGE_SDK_VERSION);
			// set the SPU params
			m_Data[i].ioBufferSize = segments[i].m_ioBufferSize;
			m_Data[i].scratchSizeInQwords = segments[i].m_scratchSize;

			// allocate one single block of data
			u32 indicesSize = segments[i].m_indexesSizes[0] + segments[i].m_indexesSizes[1];
			u32 spuVerts0Size = segments[i].m_spuVertexesSizes[0] + segments[i].m_spuVertexesSizes[1] + segments[i].m_spuVertexesSizes[2];
			u32 spuVerts1Size = segments[i].m_spuVertexesSizes[3] + segments[i].m_spuVertexesSizes[4] + segments[i].m_spuVertexesSizes[5];
			u32 skinSize = segments[i].m_skinIndexesAndWeightsSizes[0] + segments[i].m_skinIndexesAndWeightsSizes[1];
			u16 spuOutputStreamDescriptionSize = segments[i].m_spuOutputStreamDescription ? segments[i].m_spuOutputStreamDescriptionSize : 0;

			u32 spuSize = 
				indicesSize + spuVerts0Size + spuVerts1Size + skinSize +
				segments[i].m_fixedOffsetsSize[0] +
				segments[i].m_fixedOffsetsSize[1] +
				segments[i].m_spuInputStreamDescriptionSizes[0] +
				segments[i].m_spuInputStreamDescriptionSizes[1] +
				spuOutputStreamDescriptionSize +
				segments[i].m_rsxOnlyStreamDescriptionSize;

			// keep track of base allocation to free it (see rageEdgeGeomPpuConfigInfo destructor)
			u8* spuData = rage_new u8[spuSize];
			u8* currentSpuData = spuData;
			m_Data[i].baseAllocation = spuData;

			// copy indices
			memcpy(currentSpuData, segments[i].m_indexes, indicesSize);
			m_Data[i].indexes = indicesSize ? currentSpuData : NULL;
			currentSpuData += indicesSize;

			// copy sizes
			for (u32 j = 0; j < NELEM(m_Data[i].indexesSizes); ++j)
			{
				m_Data[i].indexesSizes[j] = segments[i].m_indexesSizes[j];
			}

			// copy spuVertexes[0]
			grcAssertf(spuVerts0Size > 0, "Vert0 stream is required"); // We should always have a position only stream
			memcpy(currentSpuData, segments[i].m_spuVertexes[0], spuVerts0Size);
			m_Data[i].spuVertexes[0] = currentSpuData;
#if !__RESOURCECOMPILER
			m_Data[i].spuVertexesOffset0 = gcm::MainOffset(m_Data[i].spuVertexes[0]);
#endif // !__RESOURCECOMPILER
			currentSpuData += spuVerts0Size;

			// copy spuVertexes[1]
			memcpy(currentSpuData, segments[i].m_spuVertexes[1], spuVerts1Size);
			m_Data[i].spuVertexes[1] = spuVerts1Size ? currentSpuData : NULL;
			currentSpuData += spuVerts1Size;
			
			// copy sizes
			for (u32 j = 0; j < NELEM(m_Data[i].spuVertexesSizes); ++j)
			{
				m_Data[i].spuVertexesSizes[j] = segments[i].m_spuVertexesSizes[j];
			}

			if (isSkinned)
			{
				// copy skinning info
				memcpy(currentSpuData, segments[i].m_skinIndexesAndWeights, skinSize);
				m_Data[i].skinIndexesAndWeights = skinSize ? currentSpuData : NULL;
				currentSpuData += skinSize;

				for (u32 j = 0; j < NELEM(m_Data[i].skinMatricesByteOffsets); ++j)
				{
					m_Data[i].skinMatricesByteOffsets[j] = segments[i].m_skinMatricesByteOffsets[j];
				}
				for (u32 j = 0; j < NELEM(m_Data[i].skinMatricesSizes); ++j)
				{
					m_Data[i].skinMatricesSizes[j] = segments[i].m_skinMatricesSizes[j];
				}
				for (u32 j = 0; j < NELEM(m_Data[i].skinIndexesAndWeightsSizes); ++j)
				{
					m_Data[i].skinIndexesAndWeightsSizes[j] = segments[i].m_skinIndexesAndWeightsSizes[j];
				}
			}

			// copy fixed offsets
			memcpy(currentSpuData, segments[i].m_fixedOffsetPtrs[0], segments[i].m_fixedOffsetsSize[0]);
			m_Data[i].fixedOffsets[0] = segments[i].m_fixedOffsetsSize[0] ? currentSpuData : NULL;
			currentSpuData += segments[i].m_fixedOffsetsSize[0];

			memcpy(currentSpuData, segments[i].m_fixedOffsetPtrs[1], segments[i].m_fixedOffsetsSize[1]);
			m_Data[i].fixedOffsets[1] = segments[i].m_fixedOffsetsSize[1] ? currentSpuData : NULL;
			currentSpuData += segments[i].m_fixedOffsetsSize[1];

			// copy sizes
			for (u32 j = 0; j < NELEM(m_Data[i].fixedOffsetsSize); ++j)
			{
				m_Data[i].fixedOffsetsSize[j] = segments[i].m_fixedOffsetsSize[j];
			}

			for (u32 j = 0; j < NELEM(m_Data[i].spuInputStreamDescs); ++j)
			{
				// copy input stream descriptions
				memcpy(currentSpuData, segments[i].m_spuInputStreamDescriptions[j], segments[i].m_spuInputStreamDescriptionSizes[j]);
				m_Data[i].spuInputStreamDescs[j] = segments[i].m_spuInputStreamDescriptions[j] ? currentSpuData : NULL;
				currentSpuData += segments[i].m_spuInputStreamDescriptionSizes[j];

				// copy sizes
				m_Data[i].spuInputStreamDescSizes[j] = segments[i].m_spuInputStreamDescriptionSizes[j];
			}

			// copy output stream description
			memcpy(currentSpuData, segments[i].m_spuOutputStreamDescription, spuOutputStreamDescriptionSize);
			m_Data[i].spuOutputStreamDesc = segments[i].m_spuOutputStreamDescription ? currentSpuData : NULL;
			currentSpuData += spuOutputStreamDescriptionSize;

			// copy size
			m_Data[i].spuOutputStreamDescSize = spuOutputStreamDescriptionSize;

			// copy RSX only stream description
			memcpy(currentSpuData, segments[i].m_rsxOnlyStreamDescription, segments[i].m_rsxOnlyStreamDescriptionSize);
			m_Data[i].rsxOnlyStreamDesc = segments[i].m_rsxOnlyStreamDescription ? currentSpuData : NULL;
			currentSpuData += segments[i].m_rsxOnlyStreamDescriptionSize;

			// copy size
			m_Data[i].rsxOnlyStreamDescSize = segments[i].m_rsxOnlyStreamDescriptionSize;

			// finally blendshapes. we keep all the data elsewhere so that it can be streamed in separately.
			// the rage blendshape code knows to look for it here. sigh.
			if (segments[i].m_numBlendShapes > 0 && segments[i].m_blendShapes != NULL)
			{
				// Count blendshape total size
				u32 blendShapeHeaderSize = sizeof(grmGeometryEdgeBlendHeader) + sizeof(grmGeometryEdgeBlend) * segments[i].m_numBlendShapes;
				u32 blendShapeSize = blendShapeHeaderSize;

				for (u32 j = 0; j < segments[i].m_numBlendShapes; ++j)
				{
					blendShapeSize += segments[i].m_blendShapeSizes[j];
				}

				// Always put blend data on temp heap because we copy it.
				sysMemStartTemp();
				u8* blendShapeData = (u8*)edgeGeomAlloc(blendShapeSize);
				ASSERT_ONLY(u8* blendShapeDataStart = blendShapeData;)
				m_Data[i].blendShapes = (u32*)blendShapeData;
				sysMemEndTemp();

				grmGeometryEdgeBlendHeader* blendShapeHeader = (grmGeometryEdgeBlendHeader*)blendShapeData;
				blendShapeHeader->BlendCount = segments[i].m_numBlendShapes;
				blendShapeHeader->BlendSize = blendShapeSize;
				blendShapeHeader->pad0 = 0;
				blendShapeHeader->pad1 = 0;

				blendShapeData += blendShapeHeaderSize;

				for (u32 j = 0; j < segments[i].m_numBlendShapes; ++j)
				{
					if (segments[i].m_blendShapes[j] != NULL)
					{
						u32 size = segments[i].m_blendShapeSizes[j];
						blendShapeHeader->Blends[j].Size = size;
						grcAssertf(blendShapeSize > 0, "Missing blend shape size");
						blendShapeHeader->Blends[j].Addr = blendShapeData;
						for (u32 k = 0; k < NELEM(blendShapeHeader->Blends[j].Alpha); ++k)
						{
							blendShapeHeader->Blends[j].Alpha[k] = 0.0f;
						}
						memcpy(blendShapeData, segments[i].m_blendShapes[j], size);
						blendShapeData += size;
					}
					else
					{
						blendShapeHeader->Blends[j].Size = 0;
						blendShapeHeader->Blends[j].Addr = 0;
						for (u32 k = 0; k < NELEM(blendShapeHeader->Blends[j].Alpha); ++k)
						{
							blendShapeHeader->Blends[j].Alpha[k] = 0.0f;
						}
					}
				}

				grcAssertf((blendShapeData - blendShapeDataStart) == (ptrdiff_t)blendShapeSize, "Blend shape size mismatch. (0x%p - 0x%p) != 0x%x", blendShapeData, blendShapeDataStart, blendShapeSize);						
			}

			grcAssertf(currentSpuData == (spuData + spuSize), "Size mismatch. 0x%p != (0x%p + 0x%x)", currentSpuData, spuData, spuSize);

			m_Data[i].rsxOnlyVertexesSize = segments[i].m_rsxOnlyVertexesSize;
			if (segments[i].m_rsxOnlyVertexesSize > 0)
			{
				m_Data[i].rsxOnlyVertexes = physical_new(segments[i].m_rsxOnlyVertexesSize, 16);

				// copy vertices
				memcpy(m_Data[i].rsxOnlyVertexes, segments[i].m_rsxOnlyVertexes, segments[i].m_rsxOnlyVertexesSize);
#if !__RESOURCECOMPILER
				m_Data[i].rsxOnlyVertexes = (void*)gcm::LocalOffset(m_Data[i].rsxOnlyVertexes);
#endif // !__RESOURCECOMPILER
			}
			else
			{
				m_Data[i].rsxOnlyVertexes = NULL;
			}
		}
	
		sysMemStartTemp();		// back to temp heap for destructors

		// clean up
		for (u32 i = 0; i < numSegments; ++i)
		{
			edgeGeomFreeSegmentData(segments[i]);
		}
		edgeGeomFree(segments);
	}
	sysMemEndTemp();	// and back to resource heap again on the way out after all locals have destructed.

	return success;
}
#else
bool grmGeometryEdge::Init(const mshMesh&, int, u32, const bool,const bool,GeometryCreateParams*,int) {
	return false;
}
#endif

grmGeometry* grmGeometryEdge::CloneWithNewVertexData(bool /*createVB*/, void* /*preAllocatedMemory*/, int& /*memoryAvailable*/) const
{
#if 1
	AssertMsg(false,"grmGeometryEdge::CloneWithNewVertexData is disabled");
	return NULL;
#else
	grmGeometryEdge *that = rage_new grmGeometryEdge;
	that->m_Offset = m_Offset;
	that->m_Count = m_Count;
	that->m_Data = rage_new rageEdgeGeomPpuConfigInfo[m_Count];
	for (int i = 0; i < m_Count; ++i)
	{
		// Copy everything over first.
		memcpy(&that->m_Data[i], &m_Data[i], sizeof(m_Data[i]));

		// allocate one single block of data
		u32 indicesSize = m_Data[i].indexesSizes[0] + m_Data[i].indexesSizes[1];
		u32 spuVerts0Size = m_Data[i].spuVertexesSizes[0] + m_Data[i].spuVertexesSizes[1] + m_Data[i].spuVertexesSizes[2];
		u32 spuVerts1Size = m_Data[i].spuVertexesSizes[3] + m_Data[i].spuVertexesSizes[4] + m_Data[i].spuVertexesSizes[5];
		u32 skinSize = m_Data[i].skinIndexesAndWeightsSizes[0] + m_Data[i].skinIndexesAndWeightsSizes[1];
		u16 spuOutputStreamDescriptionSize = m_Data[i].spuOutputStreamDesc ? m_Data[i].spuOutputStreamDescSize : 0;

		u32 spuSize = 
			indicesSize + spuVerts0Size + spuVerts1Size + skinSize +
			m_Data[i].fixedOffsetsSize[0] +
			m_Data[i].fixedOffsetsSize[1] +
			m_Data[i].spuInputStreamDescSizes[0] +
			m_Data[i].spuInputStreamDescSizes[1] +
			spuOutputStreamDescriptionSize +
			m_Data[i].rsxOnlyStreamDescSize;

		// keep track of base allocation to free it (see rageEdgeGeomPpuConfigInfo destructor)
		u8* spuData = createVB? rage_new u8[spuSize] : (u8*) preAllocatedMemory;
		u8* currentSpuData = spuData;
		that->m_Data[i].baseAllocation = spuData;

		// copy indices
		memcpy(currentSpuData, m_Data[i].indexes, indicesSize);
		that->m_Data[i].indexes = indicesSize ? currentSpuData : NULL;
		currentSpuData += indicesSize;

		// copy sizes
		for (u32 j = 0; j < NELEM(that->m_Data[i].indexesSizes); ++j)
		{
			that->m_Data[i].indexesSizes[j] = m_Data[i].indexesSizes[j];
		}

		// copy spuVertexes[0]
		grcAssertf(spuVerts0Size > 0, "Vert0 stream is required"); // We should always have a position only stream
		memcpy(currentSpuData, m_Data[i].spuVertexes[0], spuVerts0Size);
		that->m_Data[i].spuVertexes[0] = currentSpuData;
#if !__RESOURCECOMPILER
		that->m_Data[i].spuVertexesOffset0 = gcm::MainOffset(that->m_Data[i].spuVertexes[0]);
#endif // !__RESOURCECOMPILER
		currentSpuData += spuVerts0Size;

		// copy spuVertexes[1]
		memcpy(currentSpuData, m_Data[i].spuVertexes[1], spuVerts1Size);
		that->m_Data[i].spuVertexes[1] = spuVerts1Size ? currentSpuData : NULL;
		currentSpuData += spuVerts1Size;

		// copy sizes
		for (u32 j = 0; j < NELEM(that->m_Data[i].spuVertexesSizes); ++j)
		{
			that->m_Data[i].spuVertexesSizes[j] = m_Data[i].spuVertexesSizes[j];
		}

		// copy skinning info
		memcpy(currentSpuData, m_Data[i].skinIndexesAndWeights, skinSize);
		that->m_Data[i].skinIndexesAndWeights = skinSize ? currentSpuData : NULL;
		currentSpuData += skinSize;

		// copy fixed offsets
		memcpy(currentSpuData, m_Data[i].fixedOffsets[0], m_Data[i].fixedOffsetsSize[0]);
		that->m_Data[i].fixedOffsets[0] = m_Data[i].fixedOffsetsSize[0] ? currentSpuData : NULL;
		currentSpuData += m_Data[i].fixedOffsetsSize[0];

		memcpy(currentSpuData, m_Data[i].fixedOffsets[1], m_Data[i].fixedOffsetsSize[1]);
		that->m_Data[i].fixedOffsets[1] = m_Data[i].fixedOffsetsSize[1] ? currentSpuData : NULL;
		currentSpuData += m_Data[i].fixedOffsetsSize[1];

		for (u32 j = 0; j < NELEM(that->m_Data[i].spuInputStreamDescs); ++j)
		{
			// copy input stream descriptions
			memcpy(currentSpuData, m_Data[i].spuInputStreamDescs[j], m_Data[i].spuInputStreamDescSizes[j]);
			that->m_Data[i].spuInputStreamDescs[j] = m_Data[i].spuInputStreamDescs[j] ? currentSpuData : NULL;
			currentSpuData += m_Data[i].spuInputStreamDescSizes[j];
		}

		// copy output stream description
		memcpy(currentSpuData, m_Data[i].spuOutputStreamDesc, spuOutputStreamDescriptionSize);
		that->m_Data[i].spuOutputStreamDesc = m_Data[i].spuOutputStreamDesc ? currentSpuData : NULL;
		currentSpuData += spuOutputStreamDescriptionSize;

		// copy RSX only stream description
		memcpy(currentSpuData, m_Data[i].rsxOnlyStreamDesc, m_Data[i].rsxOnlyStreamDescSize);
		that->m_Data[i].rsxOnlyStreamDesc = m_Data[i].rsxOnlyStreamDesc? currentSpuData : NULL;
		currentSpuData += m_Data[i].rsxOnlyStreamDescSize;

		// copy size
		that->m_Data[i].rsxOnlyStreamDescSize = m_Data[i].rsxOnlyStreamDescSize;

		AssertMsg(m_Data[i].numBlendShapes == 0,"This code path doesn't support blend shapes");

		grcAssertf(currentSpuData == (spuData + spuSize), "Size mismatch. 0x%p != (0x%p + 0x%x)", currentSpuData, spuData, spuSize);

		if (m_Data[i].rsxOnlyVertexesSize > 0)
		{
			that->m_Data[i].rsxOnlyVertexes = physical_new(m_Data[i].rsxOnlyVertexesSize, 16);

#if !__RESOURCECOMPILER
			// copy vertices
			memcpy(that->m_Data[i].rsxOnlyVertexes, gcm::LocalPtr((u32)m_Data[i].rsxOnlyVertexes), m_Data[i].rsxOnlyVertexesSize);
			that->m_Data[i].rsxOnlyVertexes = (void*)gcm::LocalOffset(that->m_Data[i].rsxOnlyVertexes);
#endif // !__RESOURCECOMPILER
		}
		else
		{
			that->m_Data[i].rsxOnlyVertexes = NULL;
		}
	}

	return that;
#endif
}

#if __ASSERT
void AssertValid(const rageEdgeGeomPpuConfigInfo &info) {
	const EdgeGeomVertexStreamDescription &desc = *(EdgeGeomVertexStreamDescription*) info.spuInputStreamDescs[0];
	// Tell Dave if any of these asserts fail (or make sure EDGE is correctly using fixed-point compression on the data)
	// See edgeomgeom_decompress[_c].cpp under case EDGE_GEOM_ATTRIBUTE_FORMAT_FIXED_POINT: for how decompression works.
	// Bottom line, x is 2.14, y is 2.14, z is 2.14; Each one is biased by dest.fixedOffsets, which we verify are consistent.
	// (This could be relaxed pretty easily)
	Assert(desc.numAttributes == 1);
	Assert(desc.stride == 6);
	Assert(desc.numBlocks == 2);
	Assert(desc.blocks[0].attributeBlock.format == EDGE_GEOM_ATTRIBUTE_FORMAT_FIXED_POINT);
	Assert(desc.blocks[0].attributeBlock.fixedBlockOffset == 8);
	const EdgeGeomAttributeFixedBlock &fixed = desc.blocks[1].fixedBlock;
	// These should also add up to 48.
	Assert(fixed.integer0 == 2);
	Assert(fixed.mantissa0 == 14);
	Assert(fixed.integer1 == 2);
	Assert(fixed.mantissa1 == 14);
	Assert(fixed.integer2 == 2);
	Assert(fixed.mantissa2 == 14);
}
#endif	// __ASSERT

inline s32 min2(s32 a,s32 b) { return a<b? a : b; }

void grmGeometryEdge::BlendPositionsFromOtherGeometries(grmGeometry *geom1,grmGeometry *geom2,float alpha1,float alpha2)
{
	typedef u16 pos_t;

	u32 a1 = u32(alpha1 * 256.0f);
	u32 a2 = u32(alpha2 * 256.0f);
	// If the inputs were supposed to add up to 1.0f but don't any more, fix it.
	if (a1 + a2 == 255 || a1 + a2 == 257)
		a2 = 256 - a1;

	for (int i=0; i<m_Count; i++) {
		rageEdgeGeomPpuConfigInfo &dest = m_Data[i];
		const rageEdgeGeomPpuConfigInfo src1 = ((grmGeometryEdge*)geom1)->m_Data[i];
		const rageEdgeGeomPpuConfigInfo src2 = ((grmGeometryEdge*)geom2)->m_Data[i];
#if __ASSERT
		AssertValid(dest);
		AssertValid(src1);
		AssertValid(src2);

		Assert(!memcmp(&dest.spuConfigInfo,&src1.spuConfigInfo,sizeof(dest.spuConfigInfo)));
		Assert(!memcmp(&dest.spuConfigInfo,&src2.spuConfigInfo,sizeof(dest.spuConfigInfo)));
		Assert(dest.indexesSizes[0] == src1.indexesSizes[0] && dest.indexesSizes[1] == src1.indexesSizes[1]);
		Assert(dest.indexesSizes[0] == src2.indexesSizes[0] && dest.indexesSizes[1] == src2.indexesSizes[1]);
#endif

		// Assume 16-bit fixed-point positions, stored first.
		s32 *dOffsets = (s32*) dest.fixedOffsets[0];
		s32 *s1Offsets = (s32*) src1.fixedOffsets[0];
		s32 *s2Offsets = (s32*) src2.fixedOffsets[0];
		s32 s1OffsetX = s1Offsets[0], s1OffsetY = s1Offsets[1], s1OffsetZ = s1Offsets[2];
		s32 s2OffsetX = s2Offsets[0], s2OffsetY = s2Offsets[1], s2OffsetZ = s2Offsets[2];

		// Output geometry must have smallest min of any source channel
		s32 dx = min2(s1OffsetX,s2OffsetX);
		s32 dy = min2(s1OffsetY,s2OffsetY);
		s32 dz = min2(s1OffsetZ,s2OffsetZ);
		dOffsets[0] = dx;
		dOffsets[1] = dy;
		dOffsets[2] = dz;

		int numVertexes = dest.spuConfigInfo.numVertexes;
		u16 *d = (u16*) dest.spuVertexes[0];
		u16 *s1 = (u16*) src1.spuVertexes[0];
		u16 *s2 = (u16*) src2.spuVertexes[0];

		// Variable shifts are evil, so hard-code 2.14, 2.14, 2.14
		for (;numVertexes; --numVertexes,d+=3,s1+=3,s2+=3) {
			// Decompress all four source channels to same full 0.14 coordinate space.
			s32 s1x = s1[0] + s1OffsetX, s1y = s1[1] + s1OffsetY, s1z = s1[2] + s1OffsetZ;
			s32 s2x = s2[0] + s2OffsetX, s2y = s2[1] + s2OffsetY, s2z = s2[2] + s2OffsetZ;
			// Do the final blend in fixed point space (alpha is in 0.8 precision)
			s32 d0x = ((((s1x * a1) + (s2x * a2)) + 128) >> 8);
			s32 d0y = ((((s1y * a1) + (s2y * a2)) + 128) >> 8);
			s32 d0z = ((((s1z * a1) + (s2z * a2)) + 128) >> 8);
			// Finally shove it all back into the 48 bit destination, subtracting off the offset first
			Assert(d0x >= dx && d0y >= dy && d0z >= dz);
			d[0] = u16(d0x - dx);
			d[1] = u16(d0y - dy);
			d[2] = u16(d0z - dz);
		}
	}
}

void grmGeometryEdge::Draw() const
{
#if __PPU && USE_EDGE
# if SPU_GCM_FIFO
	grcStateBlock::Flush();
	SPU_COMMAND(grmGeometryEdge__Draw,0);
	cmd->geometry = (spuGeometryEdge*)(this);
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		cmd->gta4DebugInfo = gGta4DbgInfoStruct;
	#endif
# else
	// viewport / window
	grcViewport* viewport = grcViewport::GetCurrent();
	grcWindow window = viewport->GetWindow();

	// build edge localToWorld
	Matrix44 worldMtx = viewport->GetWorldMtx44();
	worldMtx.Transpose();
	CompileTimeAssert(sizeof(Matrix44) == sizeof(EdgeGeomLocalToWorldMatrix));
	memcpy(&s_spuGcmState.EdgeWorld, &worldMtx, sizeof(EdgeGeomLocalToWorldMatrix));

	// render each segment
	for (int i=0; i<m_Count; i++) {
		// Add the job
		GEOMETRY_JOBS.AddJob(GCM_CONTEXT, &m_Data[i], NULL, m_Offset,
			grcGeometryJobs::GetEdgeCullingMode(grcState::GetCullMode()), 			
			g_VertexShaderInputs,
			s_spuGcmState);
	}
# endif
#else
	grcAssertf(false, "PPU only function");
#endif
}

void grmGeometryEdgeBlendHeader::Place(grmGeometryEdgeBlendHeader *that,datResource &rsc)
{
	::new (that) grmGeometryEdgeBlendHeader(rsc);
}

const grmGeometryEdgeBlendHeader* grmGeometryEdge::GetBlendHeader(int i) const {
	// only valid during resource construction
	return (grmGeometryEdgeBlendHeader*)m_Data[i].blendShapes;
}

grmGeometryEdgeBlendHeader* grmGeometryEdge::GetBlendHeader(int i) {
	// only valid during resource construction
	return (grmGeometryEdgeBlendHeader*)m_Data[i].blendShapes;
}

void grmGeometryEdge::DrawSkinned(const grmMatrixSet &PPU_ONLY(ms)) const
{
#if __PPU && USE_EDGE
# if SPU_GCM_FIFO
	grcStateBlock::Flush();
	SPU_COMMAND(grmGeometryEdge__DrawSkinned,0);
	cmd->geometry = (spuGeometryEdge*)(this);
	cmd->ms = const_cast<grmMatrixSet*>(&ms);
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		cmd->gta4DebugInfo = gGta4DbgInfoStruct;
	#endif
# else
	// viewport / window
	grcViewport* viewport = grcViewport::GetCurrent();

	// build edge localToWorld
	Matrix44 worldMtx = viewport->GetWorldMtx44();
	worldMtx.Transpose();
	CompileTimeAssert(sizeof(Matrix44) == sizeof(EdgeGeomLocalToWorldMatrix));
	memcpy(&s_spuGcmState.EdgeWorld, &worldMtx, sizeof(EdgeGeomLocalToWorldMatrix));

	// render each segment
	for (int i=0; i<m_Count; i++) {
		// Add the job
		GEOMETRY_JOBS.AddJob(GCM_CONTEXT, &m_Data[i], ms.GetMatrices(), m_Offset,
			grcGeometryJobs::GetEdgeCullingMode(grcState::GetCullMode()), 			
			g_VertexShaderInputs,
			s_spuGcmState);
	}
# endif
#else
	grcAssertf(false, "PPU Only function");
#endif
}
u16	remap[256];
const u16* grmGeometryEdge::GetMatrixPalette() const
{
	static	u8	init=0;
	if(!init)
	{
		init=1;
		for(int i=0;i<256;i++)
		{
			remap[i]=(rage::u16)i;
		}
	}


	return remap;
}

// RETURNS: the number of matrices in the matrix paletter
u16 grmGeometryEdge::GetMatrixPaletteCount() const {
	return (m_Data[0].skinMatricesSizes[0] + m_Data[0].skinMatricesSizes[1]) / 48;
}

u32 grmGeometryEdge::GetPrimitiveCount() const {
	u32 numTotalIndices	= 0;

	for(int i=0; i<m_Count; i++) 
	{
		numTotalIndices	+= m_Data[i].spuConfigInfo.numIndexes;
	}

	return numTotalIndices / 3;
}

u32 grmGeometryEdge::GetVertexCount() const {
	u32 numTotalVerts	= 0;

	for(int i=0; i<m_Count; i++) 
	{
		numTotalVerts	+= m_Data[i].spuConfigInfo.numVertexes;
	}

	return numTotalVerts;
}

bool grmGeometryEdge::IsSkinned() const {
	for (int i=0; i<m_Count; i++) {
		if (m_Data[i].skinMatricesSizes[0] || m_Data[i].skinMatricesSizes[1]) {
			return true;
		}
	}
	return false;
}

void grmGeometryEdge::SetBlendHeaders(u32 drawBuffer, grmGeometryEdgeBlendHeader** blendShapeHeader) {
	for (int i = 0; i < m_Count; ++i)
	{
		if (!blendShapeHeader)
		{
			m_Data[i].blendShapes = NULL;
			m_Data[i].numBlendShapes = 0;
		}
		else if (!blendShapeHeader[i])
		{
			grcAssertf(m_Data[i].numBlendShapes == 0, "Invalid num blend shapes %d", m_Data[i].numBlendShapes);
		}
		else
		{
			m_Data[i].blendShapes = (u32*)blendShapeHeader[i];
			u16 numBlendShapes;
			Assign(numBlendShapes, blendShapeHeader[i]->BlendCount & 0x3fff);
			grcAssertf(drawBuffer<3, "Draw buffer must be 0, 1, or 2. Got %d", drawBuffer);
#if !GRCORE_ON_SPU
			numBlendShapes |= drawBuffer << 14; // store the drawbuffer in the top two bits
#endif // !GRCORE_ON_SPU
			m_Data[i].numBlendShapes = numBlendShapes;
		}
	}

#if GRCORE_ON_SPU
	if (blendShapeHeader)
	{
		SPU_SIMPLE_COMMAND(grmGeometryEdge__SetBlendHeaders, drawBuffer);
	}
#endif // GRCORE_ON_SPU
}

#if HACK_GTA4 && __PPU && USE_EDGE
//
//
//
//
bool grmGeometryEdge::ClipGeomWithPlanes(const Vector4 **clipPlanes, int ClipPlaneCount, const u8 *boneRemapLut, unsigned boneRemapLutSize, Vector4 **OutputArray, Vector4 *EndOfOutputArray, void *damageTexture, float boundsRadius, const Vector3* pBoneNormals, float dotProdThreshold
										#if HACK_GTA4_MODELINFOIDX_ON_SPU
											 ,CGta4DbgSpuInfoStruct *gta4SpuInfoStruct
										#endif
										 ) const
{
	// build edge localToWorld:
Matrix44 worldMtx;
	worldMtx.Identity();
	//	worldMtx.Transpose();

EdgeGeomLocalToWorldMatrix localToWorld;
	sysMemCpy(&localToWorld, &worldMtx, sizeof(localToWorld.matrixData));

	//
	// for each segment:
	//
bool ret = true;
	for (int i=0; i<m_Count; i++) 
	{
		ret &= GEOMETRY_JOBS.AddExtractJob(GCM_CONTEXT, &m_Data[i], &localToWorld, clipPlanes, ClipPlaneCount,
						boneRemapLut, boneRemapLutSize,
						OutputArray, EndOfOutputArray,
						NULL/*pDstVertsPtrs*/, NULL/*indexes*/,
						0/*vertexOffsetForBatch*/, 0/*totalverts*/,
						damageTexture, boundsRadius, pBoneNormals, dotProdThreshold
						#if HACK_GTA4_MODELINFOIDX_ON_SPU
							,gta4SpuInfoStruct
						#endif		
							,NULL, ~CExtractGeomParams::extractSkin); // everything but skinning
	}
	//edgeGeom::WaitForAllJobSegments();

	return ret;
}
#else // HACK_GTA4 && __PPU && USE_EDGE
bool	grmGeometryEdge::ClipGeomWithPlanes(const Vector4**,int,const u8*,unsigned,Vector4**,Vector4*,void*,float,const Vector3*,float
											#if HACK_GTA4_MODELINFOIDX_ON_SPU
												,CGta4DbgSpuInfoStruct*
											#endif		
											) const
{
	grcAssertf(false, "Not supported!");
	return false;
}
#endif // HACK_GTA4 && __PPU && USE_EDGE...


// verts, where the SPU should write back the verts
// indexes where the spu should write back the indexes
// iAndW the location of the blendindexs and weights, currently not compressed so we dont need the SPU to get these
// used by createsmashable to return all the geom so it can create smashed glass in car windows etc
#if HACK_GTA4 && __PPU && USE_EDGE
int	grmGeometryEdge::GetVertexAndIndex(Vector4 *pDstVerts, u32 maxDstVertsNum, Vector4 **pDstVertsPtrs, u16 *pDstIndexes, u32 maxDstIndicesNum,
										u8* iAndW, u32 iAndWsize, s32 *indexOffset, s32* indexStride, s32 *boneindexOffset1, s32 *boneindexOffset2, s32 *bonegroup2At,u32 *numVerts
										#if HACK_GTA4_MODELINFOIDX_ON_SPU
											,CGta4DbgSpuInfoStruct *gta4SpuInfoStruct
										#endif
										,Matrix43* ms, u8 extractMask) const
{
	// build edge localToWorld:
	Matrix44 worldMtx;
	worldMtx.Identity();

	EdgeGeomLocalToWorldMatrix localToWorld;
	sysMemCpy(&localToWorld, &worldMtx, sizeof(localToWorld.matrixData));


u32 numTotalVerts	= 0;
u32 numTotalIndices	= 0;
	
	for(int i=0; i<m_Count; i++) 
	{
		numTotalVerts	+= m_Data[i].spuConfigInfo.numVertexes;
		numTotalIndices	+= m_Data[i].spuConfigInfo.numIndexes;
	}

	if(numTotalVerts > maxDstVertsNum)
	{
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		if (gta4SpuInfoStruct)
			Printf("grmGeometryEdge::GetVertexAndIndex: Attempting to extract %d verts (max limit: %d) (modelIdx=%d, type=%d, caller=%x).", numTotalVerts, maxDstVertsNum, gta4SpuInfoStruct->gta4ModelInfoIdx, gta4SpuInfoStruct->gta4ModelInfoType, gta4SpuInfoStruct->gta4RenderPhaseID);
		else
			Printf("grmGeometryEdge::GetVertexAndIndex: Attempting to extract %d verts (max limit: %d).", numTotalVerts, maxDstVertsNum);
	#endif		
		//Assertf(0, "grmGeometryEdge::GetVertexAndIndex: Attempting to extract more than %d verts!", maxDstVertsNum);
		return(0);
	}
	
	if(numTotalIndices > maxDstIndicesNum)
	{
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		if (gta4SpuInfoStruct)
			Printf("grmGeometryEdge::GetVertexAndIndex: Attempting to extract %d indices (max limit: %d) (modelIdx=%d, type=%d, caller=%x).", numTotalIndices, maxDstIndicesNum, gta4SpuInfoStruct->gta4ModelInfoIdx, gta4SpuInfoStruct->gta4ModelInfoType, gta4SpuInfoStruct->gta4RenderPhaseID);
		else
			Printf("grmGeometryEdge::GetVertexAndIndex: Attempting to extract %d indices (max limit: %d).", numTotalIndices, maxDstIndicesNum);
	#endif		
		//Assertf(0, "grmGeometryEdge::GetVertexAndIndex: Attempting to extract more than %d indices!", maxDstIndicesNum);	
		return(0);
	}

u32 vertexOffsetForBatch=0;

	// extract from each each segment:
	for(int i=0; i<m_Count; i++) 
	{
		EdgeGeomPpuConfigInfo *configInfo = &m_Data[i];
		
		// Add the job:
		GEOMETRY_JOBS.AddExtractJob(GCM_CONTEXT, configInfo, &localToWorld,
				NULL/*clipPlanes*/, 0/*ClipPlaneCount*/,
				NULL/*boneRemapLut*/, 0/*boneRemapLutSize*/,
				pDstVerts, NULL/*EndOfOutputArray*/,
				i==0? pDstVertsPtrs : NULL,	// extract ptrs only for 1st segment
				pDstIndexes, vertexOffsetForBatch, numTotalVerts,
				NULL/*damageTexture*/, 0.0f/*boundsRadius*/,NULL/*pBoneNormals*/,0.0f/*dotProdThreshold*/
				#if HACK_GTA4_MODELINFOIDX_ON_SPU
					,gta4SpuInfoStruct
				#endif
				,ms, extractMask);

		vertexOffsetForBatch	+= configInfo->spuConfigInfo.numVertexes;	// this will be added onto the indexes before they are dma'ed out of the SPU
		pDstVerts				+= configInfo->spuConfigInfo.numVertexes;
		pDstIndexes				+= configInfo->spuConfigInfo.numIndexes;	// this may now not be on a 16 byte boundary so spu will have to deal with that in its dma
	}

	if(numVerts)
		*numVerts = numTotalVerts;

	if(iAndW)
	{
		s32 iAndWspaceLeft = iAndWsize;

		u8 *pIAndWptr = iAndW;
		for(int i=0; i<m_Count; i++) 
		{
			EdgeGeomPpuConfigInfo *configInfo = &m_Data[i];
			const u32 size = (u32)configInfo->skinIndexesAndWeightsSizes[0];
			
			iAndWspaceLeft -= size;
			if(iAndWspaceLeft < 0)
			{
				Assertf(false, "Not enough space for iAndWs (allocated: %d bytes)!", iAndWsize);
				break;
			}
			
			sysMemCpy(pIAndWptr, (u8*)configInfo->skinIndexesAndWeights, size);
			pIAndWptr += size;
		}
	}

EdgeGeomPpuConfigInfo *ppuConfigInfo0 = &m_Data[0];
	
	const uint8_t skinningFlavor = ppuConfigInfo0->spuConfigInfo.indexesFlavorAndSkinningFlavor & 0x0F;
	const bool useSingleBoneSkinning = 
		skinningFlavor == EDGE_GEOM_SKIN_SINGLE_BONE_NO_SCALING ||
		skinningFlavor == EDGE_GEOM_SKIN_SINGLE_BONE_UNIFORM_SCALING ||
		skinningFlavor == EDGE_GEOM_SKIN_SINGLE_BONE_NON_UNIFORM_SCALING;
	
	if( indexOffset )
	{
		*indexOffset = useSingleBoneSkinning ? 0 : 1;
	}
	
	if( indexStride )
	{
		*indexStride = useSingleBoneSkinning ? 1 : 8;
	}

//	u64 iAndWSizeA = ppuConfigInfo0->skinIndexesAndWeightsSizes[0]+ppuConfigInfo0->skinIndexesAndWeightsSizes[1];

	// the bone indexes have been fiddled to allow a single batch on the SPU to not have all the skinning matricies
	// we need to fix these 2
	const u32 sizeOfEdgeMatrix = sizeof(EdgeGeomLocalToWorldMatrix);	// 3x4*float = 3*16 bytes 
	CompileTimeAssert(sizeOfEdgeMatrix == 48);

	if(boneindexOffset2)
		*boneindexOffset2 =(ppuConfigInfo0->skinMatricesByteOffsets[1])/sizeOfEdgeMatrix; //how many matricies the dma skipped from the matrix palette start for the second half
	if(boneindexOffset1)
		*boneindexOffset1 =(ppuConfigInfo0->skinMatricesByteOffsets[0])/sizeOfEdgeMatrix; //how many matricies the dma skipped from the matrix palette start
	if(bonegroup2At)
		 *bonegroup2At    =(ppuConfigInfo0->skinMatricesSizes[0])/sizeOfEdgeMatrix;//how many matricies are in the first half

// every vert should have 4 indexes and 4 weights, Update: Numvertexes is rounded up to nearest 8
//	grcAssertf((int)iAndWSizeA==(int)((EdgeGeomSpuConfigInfo*)&m_Data[0])->numVertexes*8);

//	edgeGeom::WaitForAllJobSegments();

	return(numTotalIndices);
}

bool grmGeometryEdge::BegineGetVertexAndIndexAsync(u32** handle, Vector4 *pDstVerts, u32 maxDstVertsNum, Vector4 **pDstVertsPtrs, u16 *pDstIndexes, u32 maxDstIndicesNum,
	u8* iAndW, u32 iAndWsize,
	s32* indexOffset,
	s32* indexStride,
	s32* boneindexOffset1,
	s32* boneindexOffset2,
	s32* bonegroup2At,
	u32* numVerts, u32* numIndices
#if HACK_GTA4_MODELINFOIDX_ON_SPU
	,CGta4DbgSpuInfoStruct *gta4SpuInfoStruct
#endif
	,Matrix43* ms, u8 extractMask) const
{
	if(m_Count != 1)
	{
		Assertf(false, "grmGeometryEdge::GetVertexAndIndex async only works for m_Count == 1");
		return false;
	}

	// build edge localToWorld:
	Matrix44 worldMtx;
	worldMtx.Identity();

	EdgeGeomLocalToWorldMatrix localToWorld;
	sysMemCpy(&localToWorld, &worldMtx, sizeof(localToWorld.matrixData));


	u32 numTotalVerts	= 0;
	u32 numTotalIndices	= 0;

	for(int i=0; i<m_Count; i++) 
	{
		numTotalVerts	+= m_Data[i].spuConfigInfo.numVertexes;
		numTotalIndices	+= m_Data[i].spuConfigInfo.numIndexes;
	}

	if(numTotalVerts > maxDstVertsNum)
	{
#if HACK_GTA4_MODELINFOIDX_ON_SPU
		if (gta4SpuInfoStruct)
			Printf("grmGeometryEdge::GetVertexAndIndex: Attempting to extract %d verts (max limit: %d) (modelIdx=%d, type=%d, caller=%x).", numTotalVerts, maxDstVertsNum, gta4SpuInfoStruct->gta4ModelInfoIdx, gta4SpuInfoStruct->gta4ModelInfoType, gta4SpuInfoStruct->gta4RenderPhaseID);
		else
			Printf("grmGeometryEdge::GetVertexAndIndex: Attempting to extract %d verts (max limit: %d).", numTotalVerts, maxDstVertsNum);
#endif		
		//Assertf(0, "grmGeometryEdge::GetVertexAndIndex: Attempting to extract more than %d verts!", maxDstVertsNum);
		return(0);
	}

	if(numTotalIndices > maxDstIndicesNum)
	{
#if HACK_GTA4_MODELINFOIDX_ON_SPU
		if (gta4SpuInfoStruct)
			Printf("grmGeometryEdge::GetVertexAndIndex: Attempting to extract %d indices (max limit: %d) (modelIdx=%d, type=%d, caller=%x).", numTotalIndices, maxDstIndicesNum, gta4SpuInfoStruct->gta4ModelInfoIdx, gta4SpuInfoStruct->gta4ModelInfoType, gta4SpuInfoStruct->gta4RenderPhaseID);
		else
			Printf("grmGeometryEdge::GetVertexAndIndex: Attempting to extract %d indices (max limit: %d).", numTotalIndices, maxDstIndicesNum);
#endif		
		//Assertf(0, "grmGeometryEdge::GetVertexAndIndex: Attempting to extract more than %d indices!", maxDstIndicesNum);	
		return(0);
	}

	u32 vertexOffsetForBatch=0;

	EdgeGeomPpuConfigInfo *ppuConfigInfo0 = &m_Data[0]; // Get only the first segment

	// Add the job:
	GEOMETRY_JOBS.AddExtractJobAsync(GCM_CONTEXT, handle, ppuConfigInfo0, &localToWorld,
		NULL/*clipPlanes*/, 0/*ClipPlaneCount*/,
		NULL/*boneRemapLut*/, 0/*boneRemapLutSize*/,
		pDstVerts, NULL/*EndOfOutputArray*/,
		pDstVertsPtrs,	// extract ptrs - since we only extract only the 1st segment we just pass those in
		pDstIndexes, vertexOffsetForBatch, numTotalVerts,
		NULL/*damageTexture*/, 0.0f/*boundsRadius*/,NULL/*pBoneNormals*/,0.0f/*dotProdThreshold*/
#if HACK_GTA4_MODELINFOIDX_ON_SPU
		,gta4SpuInfoStruct
#endif
		,ms, extractMask);

	if(numVerts)
		*numVerts = numTotalVerts;
	if(numIndices)
		*numIndices = numTotalIndices;

	if(iAndW)
	{
		s32 iAndWspaceLeft = iAndWsize;

		u8 *pIAndWptr = iAndW;
		for(int i=0; i<m_Count; i++) 
		{
			EdgeGeomPpuConfigInfo *ppuConfigInfo0 = &m_Data[i];
			const u32 size = (u32)ppuConfigInfo0->skinIndexesAndWeightsSizes[0];

			iAndWspaceLeft -= size;
			if(iAndWspaceLeft < 0)
			{
				Assertf(false, "Not enough space for iAndWs (allocated: %d bytes)!", iAndWsize);
				break;
			}

			sysMemCpy(pIAndWptr, (u8*)ppuConfigInfo0->skinIndexesAndWeights, size);
			pIAndWptr += size;
		}
	}

	const uint8_t skinningFlavor = ppuConfigInfo0->spuConfigInfo.indexesFlavorAndSkinningFlavor & 0x0F;
	const bool useSingleBoneSkinning = 
		skinningFlavor == EDGE_GEOM_SKIN_SINGLE_BONE_NO_SCALING ||
		skinningFlavor == EDGE_GEOM_SKIN_SINGLE_BONE_UNIFORM_SCALING ||
		skinningFlavor == EDGE_GEOM_SKIN_SINGLE_BONE_NON_UNIFORM_SCALING;

	if( indexOffset )
	{
		*indexOffset = useSingleBoneSkinning ? 0 : 1;
	}

	if( indexStride )
	{
		*indexStride = useSingleBoneSkinning ? 1 : 8;
	}

	//	u64 iAndWSizeA = ppuConfigInfo0->skinIndexesAndWeightsSizes[0]+ppuConfigInfo0->skinIndexesAndWeightsSizes[1];

	// the bone indexes have been fiddled to allow a single batch on the SPU to not have all the skinning matricies
	// we need to fix these 2
	const u32 sizeOfEdgeMatrix = sizeof(EdgeGeomLocalToWorldMatrix);	// 3x4*float = 3*16 bytes 
	CompileTimeAssert(sizeOfEdgeMatrix == 48);

	if(boneindexOffset2)
		*boneindexOffset2 =(ppuConfigInfo0->skinMatricesByteOffsets[1])/sizeOfEdgeMatrix; //how many matricies the dma skipped from the matrix palette start for the second half
	if(boneindexOffset1)
		*boneindexOffset1 =(ppuConfigInfo0->skinMatricesByteOffsets[0])/sizeOfEdgeMatrix; //how many matricies the dma skipped from the matrix palette start
	if(bonegroup2At)
		*bonegroup2At    =(ppuConfigInfo0->skinMatricesSizes[0])/sizeOfEdgeMatrix;//how many matricies are in the first half

	return *handle != NULL;
}

bool grmGeometryEdge::FinalizeGetVertexAndIndexAsync(u32* handle) const
{
	return GEOMETRY_JOBS.FinalizeExtractJobAsync(GCM_CONTEXT, handle);
}
#else
int	grmGeometryEdge::GetVertexAndIndex(Vector4*,u32,Vector4**,u16*,u32,u8*,u32,s32*,s32*, s32 *, s32*,s32*,u32*
										#if HACK_GTA4_MODELINFOIDX_ON_SPU
											,CGta4DbgSpuInfoStruct*
										#endif		
									   ,Matrix43* /*ms*/, u8 /*extractMask*/) const
{
	grcAssertf(false, "Not supported!");
	return 0;
}

bool grmGeometryEdge::BegineGetVertexAndIndexAsync(u32**,Vector4*,u32,Vector4**,u16*,u32,u8*,u32,s32*,s32*, s32 *, s32*,s32*,u32*,u32*
#if HACK_GTA4_MODELINFOIDX_ON_SPU
	,CGta4DbgSpuInfoStruct*
#endif		
	,Matrix43* /*ms*/, u8 /*extractMask*/) const
{
	grcAssertf(false, "Not supported!");
	return 0;
}

bool grmGeometryEdge::FinalizeGetVertexAndIndexAsync(u32*) const
{
	grcAssertf(false, "Not supported!");
	return 0;
}
#endif

#if __DECLARESTRUCT
void grmGeometryEdge::DeclareStruct(datTypeStruct &s)
{
	SSTRUCT_BEGIN(grmGeometryEdge)
	SSTRUCT_FIELD_VP(grmGeometryEdge, m_VtxDecl)
	SSTRUCT_FIELD(grmGeometryEdge, m_Type)
	SSTRUCT_DYNAMIC_ARRAY(grmGeometryEdge, m_Data, m_Count)
	SSTRUCT_FIELD(grmGeometryEdge, m_Offset)
	SSTRUCT_FIELD(grmGeometryEdge, m_Count)
	SSTRUCT_CONTAINED_ARRAY(grmGeometryEdge, m_Pad)
	SSTRUCT_END(grmGeometryEdge)
}	
#endif

#else	// __RESOURCECOMPILER | __PPU

#if __DECLARESTRUCT
void grmGeometryEdgeBlendHeader::DeclareStruct(datTypeStruct &) {
}
#endif

void grmGeometryEdgeBlendHeader::Place(grmGeometryEdgeBlendHeader *that,datResource &rsc)
{
	::new (that) grmGeometryEdgeBlendHeader(rsc);
}

#endif	// __RESOURCECOMPILER | __PPU

grmGeometryEdgeBlendHeader::grmGeometryEdgeBlendHeader(datResource &rsc) {
	for (int i=0; i<BlendCount; i++) {
		rsc.PointerFixup(Blends[i].Addr);
		for (int j = 0; j < NELEM(Blends[i].Alpha); ++j)
			Blends[i].Alpha[j] = 0.0f;
	}
}

}	// namespace rage
