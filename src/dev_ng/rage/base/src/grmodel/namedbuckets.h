// 
// grmodel/namedbuckets.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRMODEL_NAMEDBUCKETS_H 
#define GRMODEL_NAMEDBUCKETS_H 


namespace rage {

	class atString;

	class grmNamedBuckets
	{
		u32		m_bucketID;
	public:
		grmNamedBuckets( const char* str ) { Set( str ); }
		grmNamedBuckets() : m_bucketID(0) {}

		void Set( const char* namedString );
		void Clear() { m_bucketID = 0;}

		grmNamedBuckets&  Add( u32 val );
		int GetNumber() const;
		grmNamedBuckets& Add( const char* str );
		static  void AddBucket( int BucketNo, atString name );
		u32 operator()() { return m_bucketID; }
	};



} // namespace rage

#endif // GRMODEL_NAMEDBUCKETS_H 
