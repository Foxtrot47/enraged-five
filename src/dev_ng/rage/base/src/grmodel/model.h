// 
// grmodel/model.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRMODEL_MODEL_H
#define GRMODEL_MODEL_H

#include "atl/atfunctor.h"
#include "atl/bitset.h"
#include "atl/map.h"
#include "data/base.h"
#include "data/resource.h"
#include "grcore/channel.h"
#include "grcore/device.h"
#include "grcore/array.h"
#include "grcore/cullstatus.h"
#include "mesh/mesh_config.h"
#include "spatialdata/aabb.h"
#include "shader.h"

#if __XENON
#include <xdk.h>
#endif

struct D3DVertexBuffer;

namespace rage {

struct mshMaterial;
class mshMesh;
class Matrix34;
class Vector3;
class Vector4;
class grmLodGroup;
class grmShaderGroup;
class grmMatrixSet;
class grmGeometry;
class grmShaderData;
class grmShader;
class grcFvf;
struct GeometryCreateParams;
class grmShaderSorter;
class fiTokenizer;
class grcInstanceBuffer;

struct grmModelInfo;

/* PURPOSE:
	Base class for all new model code rendered by RAGE.  It maintains information
	about shaders, matrices (i.e. for skinning) and other "generic" model information.
	Currently, this is only used as a jumping off point for models contained polygonal
	data, however, it could serve as a base class for curve surfaces or other procedural
	models.
*/
class grmModel: public datBase {
public:
	grmModel();
	grmModel(class datResource&);
	~grmModel();
	DECLARE_PLACE(grmModel);
#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	void SortForTessellation(const grmShaderGroup &group);
#endif // RAGE_SUPPORT_TESSELLATION_TECHNIQUES

	static int sm_MemoryBucket;

	// PURPOSE:	Prepare a mesh; optimize it, constrain to packet limits, etc.
	static void Prepare(mshMesh &mesh,const grmShaderGroup *pShaderGroup, bool optimize = true);

	typedef grcCullStatus (*ModelCullbackType)(const spdAABB&);

	void CloneWithNewVertexData( grmModel &cloneme, const int *geomIdx, int nGeom, bool createVB, void* preAllocatedMemory, int& memoryAvailable );

	int Release() const { delete this; return 0; }

	// DOM-IGNORE-BEGIN
	// AXE THIS?????
	bool TouchShaders(const grmShaderGroup &group, const float fDist = 1000000.0f) const;
	bool FixupShaders(const grmShaderGroup &group) const;
	bool CheckShadersAreResident(const grmShaderGroup &group);
	// DOM-IGNORE-END

	/*	PURPOSE
			Create a grmodel
		PARAMS
			basename - the base filename of the model to load / or the mesh object to load from
			mtxIndex - The index of the matrix to use when rendering (for hierarchical models)
		RETURNS
			a grmodel instance if filename exists, null otherwise
	*/
	static grmModel *Create(const mshMesh & mesh,int mtxIndex,grmModelInfo *outInfo, const grmShaderGroup *pShaderGroup, u32 channelMask=~0U, int extraVerts = 0, u8 mask = 0xFF);
	static grmModel *Create(fiTokenizer &t,const char *basename,int mtxIndex,grmModelInfo *outInfo, const grmShaderGroup *pShaderGroup, u32 channelMask=~0U,  bool optimize = true, int extraVerts = 0, u8 mask = 0xFF, int lod = 0 );

#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	/*	PURPOSE
			High-level entry point -- use this if you manually want to render a non-skinned model
		PARAMS
			group - The shadergroup to use for rendering
			bucket - The bucket to use for rendering -- only models with shaders matching this bucket will render
			lod - LOD information
	*/
	void Draw(const grmShaderGroup &group,u32 bucketMask,int lod) const;
#else
	void DrawUntessellatedPortion(const grmShaderGroup &group,u32 bucketMask,int lod DRAWABLE_STATS_ONLY(, u16 drawableStat)) const;
	void DrawTessellatedPortion(const grmShaderGroup &group,u32 bucketMask,int lod,bool reallyTessellate DRAWABLE_STATS_ONLY(, u16 drawableStat)) const;
	void DrawPortion(grmShader::eDrawType drawType, int startIdx, int loopEnd, const grmShaderGroup &group,u32 bucketMask,int lod DRAWABLE_STATS_ONLY(, u16 drawableStat)) const;
#endif
	void Draw(const grmShaderGroup &group,grcInstanceBuffer *ib,u32 bucketMask) const;

#if !RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	/*	PURPOSE
			High-level entry point -- use this if you manually want to render a skinned model
		PARAMS
			group - The shadergroup to use for rendering
			mtxs - the array of matrices to use for skinning
			mtxCount - the number of matrices in the matrix array
			bucket - The bucket to use for rendering -- only models with shaders matching this bucket will render
			lod - LOD information
	*/
	void DrawSkinned(const grmShaderGroup &group,const grmMatrixSet &ms,u32 bucketMask,int lod) const;
#else
	void DrawUntessellatedPortionSkinned(const grmShaderGroup &group,const grmMatrixSet &ms,u32 bucketMask,int lod DRAWABLE_STATS_ONLY(, u16 drawableStat)) const;
	void DrawTessellatedPortionSkinned(const grmShaderGroup &group,const grmMatrixSet &ms,u32 bucketMask,int lod,bool reallyTessellate DRAWABLE_STATS_ONLY(, u16 drawableStat)) const;
	void DrawSkinnedPortion(grmShader::eDrawType drawType, int startIdx, int loopEnd, const grmShaderGroup &group,const grmMatrixSet &ms,u32 bucketMask,int lod DRAWABLE_STATS_ONLY(, u16 drawableStat)) const;
#endif

	// PURPOSE: Draw method called by shader ONLY
	// PARAMS: geom - index of the geometry group to use
	void Draw(int geom) const;

	// PURPOSE: Draw method called by shader ONLY
	// PARAMS: geom - index of the geometry group to use
	void DrawSkinned(int geom,const grmMatrixSet &ms) const;

	// PURPOSE: DirectDraw method called by shader ONLY
	// PARAMS: geom - index of the geometry group to use
	void DirectDraw(int geom) const;

	// RETURNS: The number of matrices needed for this model
	int GetMatrixCount() const { return m_MatrixCount; }
	
	// RETURNS: The number of geometry groups in this model
	int GetGeometryCount() const { return m_Count; }
	
	// RETURNS: the shader index for a particular geometry group
	int GetShaderIndex(int i) const { return (int) m_ShaderIndex[i]; }

	// PURPOSE:	Sets the shader index
	void SetShaderIndex(int i,u16 v) { m_ShaderIndex[i] = v; }

	// RETURNS: the shader index for a particular geometry group
	u16* GetShaderIndexPtr() const { return m_ShaderIndex; }

	// PURPOSE:	Sets the shader index
	void SetShaderIndexPtr(u16* pShaderIdx) { m_ShaderIndex = pShaderIdx; }

	// PURPOSE: Swap geometry vertex buffers
	void SwapBuffers();
	
	// PURPOSE:	Run grmShader::Optimize on the model's shaders
	void Optimize(grmShaderGroup &group);

	// PURPOSE:	Prepare model for rendering in the near future
	void Configure(const grmShaderGroup &group) const;

	// RETURNS: The geometry for the specified index
	grmGeometry& GetGeometry(int i) const;

	bool HasGeometry(int i) const { return m_Geometries[i] ? true: false; }

	// RETURNS: Pointer to array of geometry pointers
	const datOwner<grmGeometry> *GetGeometryPtrArray() const {return &m_Geometries[0];}

	// RETURNS: The fvf for the specified index
	const grcFvf* GetFvf(int i) const;

	enum { MODEL_RELATIVE=0x01, RESOURCED=0x02 };
	// RETURNS: True if model is skinned
	bool IsModelRelative() const { return (m_Flags & MODEL_RELATIVE) != 0; }
	// RETURNS: True if model is resourced
	bool IsResourced() const { return (m_Flags & RESOURCED) != 0; }

	// PURPOSE: Changes the matrix index to use for the model
	void SetMatrixIndex(int idx) { m_MatrixIndex = (u8) idx; }
	// RETURNS: The matrix index used for this model
	int GetMatrixIndex() const { return m_MatrixIndex; }

	// PURPOSE: If this is forced nonzero, we use that precision explicitly instead
	// of autodetecting; useful for guaranteeing that pieces meet up properly.
	static void SetPositionFractionalBits(int fb) { sm_PositionFracBits = fb; }
	// RETURNS: The fractional 
	int GetPositionFractionalBits() { return sm_PositionFracBits; }

	// PURPOSE: Used to force a shader to be used for all future model rendering until cleared
	//			by passing in a null pointer.  Passing in null will restore the model's shaders
	//			for use in rendering
	static void SetForceShader(grmShader *shad = NULL);

#if __BANK
	typedef bool (*CustomShaderParamsFuncType)(const grmShaderGroup& group, int shaderIndex, bool bDrawSkinned);
	static void SetCustomShaderParamsFunc(CustomShaderParamsFuncType func) { sm_CustomShaderParamsFunc = func; }
	static bool HasCustomShaderParamsFunc() { return sm_CustomShaderParamsFunc != NULL; }
#endif // __BANK

	// RETURNS: The shader that is currently forced for all model rendering.
	//			NULL if forced shaders are disabled
	static grmShader *GetForceShader() { return sm_ForceShader; }

	// RETURNS: A 32 bit bitfield indicating which buckets are needed for this model given a
	//		particular shadergroup
	u32 ComputeBucketMask(const grmShaderGroup &group) const;

	// PURPOSE: Process the mesh object passed in to create the equivalent geometry packets
	// RETURNS: true on success
	bool Load(const mshMesh &mesh,grmModelInfo *outInfo, const grmShaderGroup *pShaderGroup, u32 channelMask=~0U, int extraVerts = 0, u8 mask = 0xFF);

#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif

#if !__FINAL
	static void SetModelCullerDebugFlags(u8 flags);
#endif // !__FINAL
	static ModelCullbackType GetModelCullback() { return sm_Cullback; }
	static void SetModelCullback(ModelCullbackType cb);

	// Use these to get desired behavior, or write your own cullback.
	static grcCullStatus ModelVisibleWithAABB(const spdAABB&);

#if RAGE_INSTANCED_TECH
	static grcCullStatus ModelVisibleWithAABB_Instanced(const spdAABB&);
#endif

    const spdAABB* GetAABBs() const {FastAssert(m_AABBs);return m_AABBs;}
	const spdAABB& GetModelAABB() const { FastAssert(m_AABBs);return m_AABBs[0];}
	const spdAABB& GetGeometryAABB(int index) const {FastAssert(m_AABBs);FastAssert(index<m_Count);return m_AABBs[m_Count>1?index+1:0];}
	// PURPOSE:	Sets the current shader sorter object (default is NULL for none).
	// RETURNS:	Previous shader sorter object (may be NULL).
	static grmShaderSorter* SetShaderSorter(grmShaderSorter* sorter)
	{
		grmShaderSorter *prev = sm_ShaderSorter;
		sm_ShaderSorter = sorter;
		return prev;
	}

	// PURPOSE: Accesses the shader sorter
	// RETURNS: Current shader sorter object (may be NULL).
	static grmShaderSorter* GetShaderSorter()
	{
		return sm_ShaderSorter;
	}

	// PURPOSE: set the tessellated geometry count to match the geometry count to force the tessellated technique
	void ForceTessallation()	{ m_SkinFlagAndTessellatedGeometryCount = ((u8)m_Count<<1); }

	//
	// PURPOSE
	//	Allows the client to specify a functor that is called for every mesh that is loaded in grmModel::Create().
	//  The functor is called with a reference to the mesh and the name of the mesh.
	//  If the client's functor returns false, then grmModel::Create() will not delete the mesh.
	//  If the client's functor returns true, then the mesh will be deleted which is the same 
	//  behavior if there wasn't a MeshLoaderHook() specified by the client.
	// PARAMS
	//	functor - the functor that will be called when a mesh is loaded.
	// NOTES
	//	This functor will NOT be called if a model has been resourced, so setting this functor is really 
	//  only useful for tool builds.
	typedef atFunctor2<bool,mshMesh&,const char*> MeshLoaderHookParams;
	static void SetMeshLoaderHook(MeshLoaderHookParams functor);

	// PURPOSE: Removes the mesh loader hook that may have been set by SetMeshLoaderHook().
	static void RemoveMeshLoaderHook();

	// PURPOSE: Allow params to be returned per mesh which configure the type of geometry to be created, and
	//			to specify additional settings for the specific geometry type.
	typedef atFunctor5<void, const grmModel&, const mshMesh&, const grmShaderGroup*, int, GeometryCreateParams*> GeometryParamsHook;
	static void SetGeometryParamsHook(GeometryParamsHook functor);

	// PURPOSE: Removes the geometry params hook.
	static void RemoveGeometryParamsHook();

	// PURPOSE: Allow params to be returned per mesh which configure whether a specific mesh gets optimized.
	typedef enum
	{
	 	grcMeshPrepareDefault,
	 	grcMeshPrepareOptimize,
	 	grcMeshPrepareDontOptimize
	} grmMeshPrepareParam;
	typedef atFunctor3<grmMeshPrepareParam, mshMesh&, const grmShaderGroup*, int> MeshPrepareHook;
	static void SetMeshPrepareHook(MeshPrepareHook functor);
	static void RemoveMeshPrepareHook();

	// Pre Model creation hook, the model won't be created if the callback return false.
	typedef atFunctor4<bool,mshMesh&,const char*,int,const grmShaderGroup*> PreModelHookParams;
	typedef atFunctor3<bool,mshMesh&,fiTokenizer &,const grmShaderGroup*> PreModelHookParams2;
	typedef atFunctor1<void,const atArray<int>*> PreModelHookParams3;
	static void SetPreModelCreateHook(PreModelHookParams functor);
	static void SetPreModelCreateHook2(PreModelHookParams2 functor);
	static void SetPreModelCreateHook3(PreModelHookParams3 functor);
	static void RemovePreModelCreateHook();
	static void RemovePreModelCreateHook2();
	static void RemovePreModelCreateHook3();

#if MESH_LIBRARY
	// PURPOSE: a callback to define mesh split into smaller meshes
	// RETURNS: maximum number of vertices for this batch
	static int Packetize(const mshMesh &mesh,const mshMaterial &mtl,void *closure);
#endif //MESH_LIBRARY

	// PURPOSE: accessor to determine whether to render with the skinned draw path or not
	// RETURNS: returns true if client should render with the skinned draw path
	bool GetSkinFlag() const;

	// PURPOSE: accessor to set whether to render with the skinned draw path or not
	// PARAMS:
	//	flag - true if client should render with the skinned draw path, false if not 
	void SetSkinFlag(bool flag);

	// PURPOSE: accessor to get the "buckety" mask.
	// PARAMS:
	u32 GetMask() const { return m_Mask; }
	
#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
	// PURPOSE: Returns the number of untessellated geometries.
	u32 GetUntessellatedGeometryCount() const;
	// PURPOSE: Returns the number of tessellated geometries.
	u32 GetTessellatedGeometryCount() const;
#endif //RAGE_SUPPORT_TESSELLATION_TECHNIQUES

	// PURPOSE: Set the global channel mask, which allows you to mask out
	//			data you do not want regardless of whether the model thinks
	//			it needs the data or not.
	// PARAMS:	newMask - New channel mask, bitmask of the grcfc... enumerants
	//				shifted into place
	// RETURNS:	Previous mask
	static u32 SetGlobalChannelMask(u32 newMask) {
		u32 old = sm_GlobalChannelMask;
		sm_GlobalChannelMask = newMask;
		return old;
	}

	// RETURNS:	Current global channel mask
	static u32 GetGlobalChannelMask() {
		return sm_GlobalChannelMask;
	}

	static void ResetBoundingBox();

	static void ResetByteCounters();

	static Vector3 sm_ModelMin;
	static Vector3 sm_ModelMax;

	static int sm_GeometriesLoaded;		// Number of individual geometries loaded
	static int sm_CPVBytesUsed;			// Number of bytes spent on CPV data
	static int sm_VertexPosBytesUsed;	// Number of bytes spent on vertex position data
	static int sm_VertexNormalBytesUsed;// Number of bytes spent on vertex normal data
	static int sm_UVBytesUsed;			// Number of bytes spent on UV data
	static int sm_TotalBytesUsed;		// Number of bytes spent on vertex data in total

	static bool sm_EdgeGeomSkinned, sm_EdgeGeomUnskinned;

protected:
	enum { MODELGEOM, RMC_TYPECOUNT };

	// methods:
	void PatchBillboard(unsigned index, mshMesh &mesh, const grmShaderGroup *pShaderGroup);
	u32	 PatchDisplacement(mshMaterial &mtl);

	// statics:
#if MESH_LIBRARY
	static int sm_PacketLimit;
#endif //MESH_LIBRARY
	static int sm_PositionFracBits;
	static __THREAD grmShader *sm_ForceShader;
	static MeshLoaderHookParams sm_MeshLoaderHookFunctor;
	static PreModelHookParams	sm_PreModelCreateHookFunctor;
	static PreModelHookParams2	sm_PreModelCreateHookFunctor2;
	static PreModelHookParams3	sm_PreModelCreateHookFunctor3;
	static GeometryParamsHook	sm_GeometryParamsHookFunctor;
	static MeshPrepareHook		sm_MeshPrepareHook;
	static DECLARE_MTR_THREAD ModelCullbackType	sm_Cullback;
	static u32 sm_GlobalChannelMask;
	static grmShaderSorter *sm_ShaderSorter;

#if __BANK
	static DECLARE_MTR_THREAD CustomShaderParamsFuncType sm_CustomShaderParamsFunc;
#endif // __BANK

	grcArray< datOwner<grmGeometry> > m_Geometries;
	spdAABB * m_AABBs; // one for each geometry + one for the whole model (unless there's only one model)
	u16 *m_ShaderIndex;	// data/struct.h requires that for dynamic arrays, the pointer comes before its count in the structure.
	u8 m_MatrixCount, m_Flags, m_Type, m_MatrixIndex, m_Mask, m_SkinFlagAndTessellatedGeometryCount;
	u16 m_Count;
};


inline void grmModel::SetMeshLoaderHook(MeshLoaderHookParams functor)
{
	sm_MeshLoaderHookFunctor=functor;
}

inline void grmModel::RemoveMeshLoaderHook()
{
	MeshLoaderHookParams functor;
	// copy an invalid functor:
	sm_MeshLoaderHookFunctor=functor;
}

inline void grmModel::SetMeshPrepareHook(MeshPrepareHook functor)
{
	sm_MeshPrepareHook=functor;
}

inline void grmModel::RemoveMeshPrepareHook()
{
	MeshPrepareHook functor;
	// copy an invalid functor:
	sm_MeshPrepareHook=functor;
}


inline void grmModel::SetPreModelCreateHook(PreModelHookParams functor)
{
	sm_PreModelCreateHookFunctor=functor;
}

inline void grmModel::RemovePreModelCreateHook()
{
	PreModelHookParams functor;
	// copy an invalid functor:
	sm_PreModelCreateHookFunctor=functor;
}

inline void grmModel::SetPreModelCreateHook2(PreModelHookParams2 functor)
{
	sm_PreModelCreateHookFunctor2=functor;
}

inline void grmModel::RemovePreModelCreateHook2()
{
	PreModelHookParams2 functor;
	// copy an invalid functor:
	sm_PreModelCreateHookFunctor2=functor;
}

inline void grmModel::SetPreModelCreateHook3(PreModelHookParams3 functor)
{
	sm_PreModelCreateHookFunctor3=functor;
}

inline void grmModel::RemovePreModelCreateHook3()
{
	PreModelHookParams3 functor;
	// copy an invalid functor:
	sm_PreModelCreateHookFunctor3=functor;
}

inline void grmModel::SetGeometryParamsHook(GeometryParamsHook functor)
{
	sm_GeometryParamsHookFunctor=functor;
}

inline void grmModel::RemoveGeometryParamsHook()
{
	GeometryParamsHook functor;
	// copy an invalid functor:
	sm_GeometryParamsHookFunctor=functor;
}

inline bool grmModel::GetSkinFlag() const 
{ 
	return (m_SkinFlagAndTessellatedGeometryCount & 0x1) != 0;
}

inline void grmModel::SetSkinFlag(bool flag) 
{ 
	m_SkinFlagAndTessellatedGeometryCount = (m_SkinFlagAndTessellatedGeometryCount & ~0x1) | (flag ? 1 : 0);
}

#if RAGE_SUPPORT_TESSELLATION_TECHNIQUES
inline u32 grmModel::GetUntessellatedGeometryCount() const
{
	return m_Count - GetTessellatedGeometryCount();
}

inline u32 grmModel::GetTessellatedGeometryCount() const
{
	return (m_SkinFlagAndTessellatedGeometryCount & ~0x1) >> 0x1;
}
#endif //RAGE_SUPPORT_TESSELLATION_TECHNIQUES

typedef grmModel grmModelGeom;	// don't break old code

}	// namespace rage

#endif
