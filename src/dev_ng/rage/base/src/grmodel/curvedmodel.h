//
// curvedModel/curvedModel.h
// 
// Copyright (C) 1999-2004 Rockstar Games.  All Rights Reserved.
//
#ifndef CURVED_MODEL_H
#define CURVED_MODEL_H

#include "curve/curve.h"
#include "grcore/channel.h"
#include "grcore/effect.h"
#include "grcore/fvf.h"
#include "grcore/vertexdecl.h"
#include "grmodel/shader.h"
#include "vector/matrix34.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"

namespace rage
{
	// fwd references
	class grcVertexBuffer;
	class grcIndexBuffer;
	class grmShader;



// constants
#if __WIN32PC && !__D3D11
#define MAX_CURVE_POINTS		18	// see shader_source\rope_default.fx, l 32
#else
#define MAX_CURVE_POINTS		38 // see shader_source\rope_default.fx, l 32
#endif

//
// PURPOSE
//  Calculates Catmull Rom curve coefficients for the four points. And returns them in 3 four element vectors to allow for
//  fast calculation on vector hardware.
//
void preCalcCatmullRom(
	Vec4V_InOut	vx,			// output coefficients for the x component
	Vec4V_InOut	vy,			// output coefficients for the y component
	Vec4V_InOut	vz,			// output coefficients for the z component
	Vec3V_In	p1,			// point before the segments point
	Vec3V_In	p2,			// point on the main segment
	Vec3V_In	p3,			// point after the segment
	Vec3V_In	p4);		// second point after the segment

//
// PURPOSE
//  Interface class for passing data to the FastCurveGen class. This abstracts out the generation of curved model
//  from the object that makes it.
//
// SEE ALSO
// FastCurveGen, SpiralVertices, RopeVertices
//
class CurveGenerator
{
public:
	virtual ~CurveGenerator() { }

	// PURPOSE
	//  Returns the position on the object at the given index
	//
	virtual Vector3 operator()( int index )  const = 0;

	// PURPOSE
	//  returns the total number of vertices to use. 
	//
	virtual int NumberVertices()  const = 0;

	// PURPOSE
	//  Returns the previous position  at the given index.
	//
	virtual Vector3 GetLastPosition( int index )  const = 0;
};

//
// PURPOSE
//  Example class for creating a curved model. It generates a series of points on a twisting spiral.
//	To show how the Curved Model could be used for roads.
//
// SEE ALSO
// FastCurveGen, SpiralVertices, RopeVertices
//
class SpiralVertices : public CurveGenerator
{

public:
	SpiralVertices( float height, float freq,  float radius, int num ) 
		: m_num( num ), m_radius( radius ), m_height( height ), m_freq( freq )
	{}

	Vector3 operator()( int index )   const
	{
		float t = (float) index / m_num;

		float widthScale = 1 - ( t * t);

		return Vector3( sin( t * m_freq ) * m_radius * widthScale, t * m_height, cos( t * m_freq )  * m_radius  * widthScale) ;
	}
	Vector3 GetLastPosition( int index )   const
	{ 
		return operator()( index );
	}
	int NumberVertices()  const
	{
		return m_num;
	}
private:
	int		m_num;
	float m_radius;
	float m_height;
	float m_freq;
};



//
// PURPOSE
//  Generates coefficients for fast curve coefficient generation. Also gets the tangent for the model.
//	It generates a set of coefficients so only three dot products are required to get the resultant
//	curve value for a given t value.
// PARAMS
//
//  VertGetter			-		Class which allows the Curve generator to read in information for the curve.
//								This is based on the CurveGenerator class interface.
// SEE ALSO
//  CurveGenerator
//
template<class VertGetter>
class FastCurveGen
{

public:

	//
	// PURPOSE
	//  Creates the Fast Curve Generator
	// PARAMS
	//
	//  getVert			-		Class which allows the Curve generator to read in information for the curve.
	//							This is based on the CurveGenerator class interface. This is the template parameter class
	FastCurveGen( VertGetter& getVert  )
		:	m_getVertex( getVert )
	{
		m_index = 3;
		const Vec3V p0 = GetVertex( 0 );
		const Vec3V p1 = GetVertex( 1 );
		const Vec3V p2 = GetVertex( 2 );
		m_p1 = p0*ScalarV(V_TWO) - p1;
		m_p2 = p0;
		m_p3 = p1;
		m_p4 = p2;
	}



	//
	// PURPOSE
	//  Returns true if there are no points left to calculate for the curve.
	//
	bool IsFinished() const
	{
		return  ( m_index - 3) == m_getVertex.NumberVertices() ;
	}


	//
	// PURPOSE
	//  Calculates the curve coefficients for the next segment on the curve object.
	// NOTES
	//	Uses the CurveGenerator style class to get the next point and shifts down the rest.
	//
	void CalculateNextSegmentCoefficients( Vec4V_InOut vx, Vec4V_InOut vy, Vec4V_InOut vz )
	{
		preCalcCatmullRom( vx, vy, vz, m_p1, m_p2, m_p3, m_p4 );

		// shift
		m_p1 = m_p2;
		m_p2 = m_p3;
		m_p3 = m_p4;
		m_p4 = GetVertex( m_index++ );
	}

	//
	// PURPOSE
	//  Calculates a tangent at the current point on the curve.
	// NOTES
	//	To stop the tangent jumping about the place the previous result is passed in.
	//	Also may want to abstract this out in the future to allow for control on the twist on the curve.
	//
	Vec3V_Out GetApproximateTangent(Vec3V_In previousTangent)
	{
		// Find the direction along the curve.
		Vec3V curveDirection = m_p3 - m_p2;

		if (!IsEqualAll(curveDirection,Vec3V(V_ZERO)))
		{
			// Some distance was traveled along the curve.
			// Get the normalized cross product of the curve direction and the previous tangent.
			Vec3V binormal = Cross(curveDirection, previousTangent);

			// Find the new tangent.
			Vec3V tangent = Cross(binormal,curveDirection);

			return NormalizeSafe(tangent, Vec3V(V_X_AXIS_WZERO), Vec3V(V_ZERO));
		}
		
		// No distance was traveled along the curve, so keep the same tangent.
		return previousTangent;
	}


	inline const Vec3V_Out GetLocalCV0() const { return m_p1; }
	inline const Vec3V_Out GetLocalCV1() const { return m_p2; }
	inline const Vec3V_Out GetLocalCV2() const { return m_p3; }
	inline const Vec3V_Out GetLocalCV3() const { return m_p4; }

private:

	inline Vec3V_Out GetVertex( int idx ) const
	{
		return VECTOR3_TO_VEC3V(m_getVertex( Min<int>( idx, m_getVertex.NumberVertices() - 1 ) ));
	}

	Vec3V		m_p1;
	Vec3V		m_p2;
	Vec3V		m_p3;
	Vec3V		m_p4;
	VertGetter	m_getVertex;
	int			m_index;
};


//
// PURPOSE
//  Calculates the spherical bound of a curved model of a given thickness. 
//	PARAMS
//		VERTGENERATOR		-  This is a class which follows the curveGenerator interface which supplies points for the curve.
// SEEALSO
//		curveGenerator
template<class VERTGENERATOR> 
Vector4 CalculateSphereBound( VERTGENERATOR& vert, // Class to generate points for curve
							  float radius	// radius of each element on sphere 
							  )
{
	FastAssert( vert.NumberVertices() > 0 );

	// first get the average vertex
	int numVerts = 	vert.NumberVertices();
	Vector3 centre = vert( 0);
	for ( int i =1 ; i < numVerts; i++)
	{
		centre += vert( i );
	}
	centre /= static_cast<float>( numVerts );

	// then get the maximum distance to any of the point from the average
	float maxRadius = -FLT_MAX;
	for ( int i = 0 ; i < numVerts; i++)
	{
		Vector3 diff = vert( i ) - centre;
		maxRadius = Max( diff.Mag2(), maxRadius );
	}
	return Vector4( centre.x, centre.y, centre.z,  sqrtf(maxRadius) + radius );
}


struct CurvedModelShaderVar {
	grcEffectVar m_curveCoefficients;
	grcEffectVar m_approximateTangent;
	grcEffectVar m_segmentSize;
	grcEffectVar m_radius;
	grcEffectVar m_UVScales;
};

//
// PURPOSE
//  Lookup a curved model shader's parameters Var id.;
//	PARAMS
// SEEALSO
//		curveGenerator, FastCurveGen
inline bool LookupCurvedModelShaderVar( 
							grmShader& effect,					// objects effect so that it's shader can be set.
							CurvedModelShaderVar &varStore
							)
{
	varStore.m_curveCoefficients = effect.LookupVar( "CurveCoefficients" );
	varStore.m_approximateTangent = effect.LookupVar( "ApproximateTangent" );
	varStore.m_segmentSize = effect.LookupVar( "SegmentSize" );
	varStore.m_radius = effect.LookupVar( "Radius" );
	varStore.m_UVScales = effect.LookupVar( "UVScales" );
	
	return	varStore.m_curveCoefficients != grcevNONE && 
			varStore.m_approximateTangent != grcevNONE && 
			varStore.m_segmentSize != grcevNONE && 
			varStore.m_radius != grcevNONE && 
			varStore.m_UVScales != grcevNONE;

}


//
// PURPOSE
//  Setups the a curved model shader's  parameters, to allow for an object to be rendered so that it's y axis follows the curve.
//  This allows for rendering things such as ropes,  dynamic trees which bend in the wind and curved roads.;
//	PARAMS
//		VERTGENERATOR		-  This is a class which follows the curveGenerator interface which supplies points for the curve.
//  NOTES
//		This function uses the FastCurveGen class to get the coefficients and details it needs for the curveGenerator derived class.
// SEEALSO
//		curveGenerator, FastCurveGen
template<class VERTGENERATOR> 
void setupCurvedModelShader( 
							const Matrix34& /*cameraMtx*/,			// Current camera matrix to render with
							VERTGENERATOR& vert,				// Class to generate points for curve
							float radius,						// radius of which object should be scaled
							const Vector2 &uvScale,
							grmShader& effect,					// objects effect so that it's shader can be set.
							CurvedModelShaderVar *varStore
							)
{

	grcAssertf(radius>0.0f,"Rope is trying to render with radius %f, but the radius must be greater than zero.",radius);
	grcAssertf(vert.NumberVertices()<=MAX_CURVE_POINTS, "Rope has %d vertices, but the maximum is %d.",vert.NumberVertices(),MAX_CURVE_POINTS);

	// Vec3V V = RCC_VEC3V(cameraMtx.c);

	FastCurveGen<VERTGENERATOR> curveCurrent( vert );

	Vec4V CurveCoefficients[ MAX_CURVE_POINTS * 3 ];
	Vec4V ApproxTangentsAndDist[ MAX_CURVE_POINTS  + 1];
	Vec3V approxTangent = Vec3V(V_X_AXIS_WZERO);

	// Setup Constants
	int vertexCount = 0;
	ScalarV distTravelled(V_ZERO);
	while (!curveCurrent.IsFinished())
	{
		curveCurrent.CalculateNextSegmentCoefficients(
			CurveCoefficients[ vertexCount *3 + 0 ], 
			CurveCoefficients[ vertexCount *3 + 1 ], 
			CurveCoefficients[ vertexCount *3 + 2 ]);

		// compute approximate tangent and store
		approxTangent = curveCurrent.GetApproximateTangent(approxTangent);
		ApproxTangentsAndDist[ vertexCount ] = Vec4V(approxTangent, distTravelled);

		const Vec3V p0 = curveCurrent.GetLocalCV0();
		const Vec3V p1 = curveCurrent.GetLocalCV1();
		distTravelled += Mag(p0 - p1);

		vertexCount++;
	}
	grcAssertf(vertexCount==vert.NumberVertices(),"The vertex list messed up while rendering rope, it counted %d but there are %d.",vertexCount,vert.NumberVertices());

	if ( vertexCount > 0 )
	{
		ApproxTangentsAndDist[ vertexCount] =  ApproxTangentsAndDist[ vertexCount -1 ];
	}

	CurvedModelShaderVar var_;
	CurvedModelShaderVar *var = varStore;
	
	if( var == NULL )
	{
		LookupCurvedModelShaderVar(effect,var_);
		var = &var_;
	}
	
	// Now setup the shader variables
	effect.SetVar( var->m_curveCoefficients, CurveCoefficients,  vertexCount * 3 );

	// linear interpolated values
	effect.SetVar( var->m_approximateTangent, ApproxTangentsAndDist,  vertexCount  + 1);

	// curve details
	float size = (float)vertexCount;
	effect.SetVar( var->m_segmentSize, size -1  );

	effect.SetVar( var->m_radius, radius );

	Vector3 uvScales(uvScale.x, uvScale.y, 0.0f);

	effect.SetVar( var->m_UVScales, uvScales );
	
	// now ready to call shader
}



//
// PURPOSE
//   Creates a model of a tube at the specified resolution and size
// NOTES
//	This is mainly designed for use with the curved model but could be used for other usage. It could be used with displacement
//	maps to create geometry
//
class TubeModel
{
public:
	//
	// PURPOSE
	//  Creates the tube model.
	// NOTES
	//  Creates A vertex buffer and Index buffer resources, so it can allocate a lot of memory.
	//	and may take a while to execute depending on the number of polys.
	//
	TubeModel(   int numPoints,	//  number of segments along it's length
		int NumSegments,	// number of segments along it's width
		bool cap = false,
		Vector2 uvScale = Vector2( 1.0f,1.0f )
		);

		//
		// PURPOSE
		//  destroys the tube model.
		// NOTES
		//  releases the vertex and index buffers.
		//
		~TubeModel();

		//
		// PURPOSE
		//  render the tube model with it's current setup.
		//
		void render( grmShader *shader, grcEffectTechnique tec = grcetNONE, int pass = 0 );

		//
		// PURPOSE
		//  render the tube model with it's current setup without shaders
		//
		void renderNoShaders();

private:
	void createIndices(  int maxPoints, int numSegments );
	void createVertices( grcFvf& fvf, int maxPoints, int numSegments , bool cap, Vector2 uvScale );


	grcVertexDeclaration *m_VertexDecl;
	grcVertexBuffer *m_VertexBuffer;
	grcIndexBuffer *m_IndexBuffer;
};

};// namespace rage
#endif
