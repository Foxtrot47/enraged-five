// 
// grmodel/modelfactory.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "modelfactory.h"
#include "model.h"
#include "grcore/channel.h"

#include "grcore/fvf.h"
#include "grcore/config.h"
#include "grcore/device.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "system/memory.h"
#include "grmodel_config.h"
#include "shaderlib/skinning_method_config.h"
#include "atl/array.h"

#include <algorithm>
#include "system/rsg_algorithm.h" //for lower bound

#if __WIN32
	#include "system/xtl.h"
	#include "system/d3d9.h"
#endif


using namespace rage;

struct DeclPair {
	u32 Hash;
	grcVertexDeclaration *Decl;

	static bool SearchCompare(const DeclPair& data, const u32 key) {
		return (data.Hash < key); 
	}	
	static bool SortCompare(const DeclPair& a, const DeclPair& b) {
		return (a.Hash < b.Hash);
	}
};

atFixedArray<DeclPair,50> sm_Decls;

grmVtxStreamConfigurator::grmVtxStreamConfigurator() {
	for (int i = 0; i < MAX_STREAMS; ++i) {
		m_Flags[i].Reset();
	}
	
	// Also, for Xenon builds, enable packed normals by default
	// Nvidia PC boards do not support this, unfortunately.  Dunno about ATI.
#if __XENON || __PPU
	SetPackNormals(true);
#endif
}


grmVtxStreamConfigurator::~grmVtxStreamConfigurator() {
	/* EMPTY */
}

void grmVtxStreamConfigurator::SetStageEnable(int baseBit, int stage, bool value, int stream) {
	grcAssertf(stage >= 0 && stage < MAX_TEX_STAGES, "Texture stage id out of range");
	if ( stream < 0 ) {
		for (int i = 0; i < MAX_STREAMS; ++i) {
			m_Flags[i].Set(baseBit + stage, value);
		}
	}
	else {
		grcAssertf(stream < MAX_STREAMS, "Invalid stream (%d)", stream);
		m_Flags[stream].Set(baseBit + stage, value);
	}
}

void grmVtxStreamConfigurator::SetEnableNormals(bool e, int stream) {
	SetStageEnable(BIT_DISABLE_NORMALS, 0, !e, stream);
}

void grmVtxStreamConfigurator::SetPackNormals(bool e, int stream) {
#if __WIN32PC && __D3D9
	if (!g_ByteSwap && g_pGRCDEVICE && GRCDEVICE.IsCreated()) {
		// See if video card can handle Packed Normals
		D3DCAPS9 caps;
		GRCDEVICE.GetCurrent()->GetDeviceCaps(&caps);
		if ( (caps.DeclTypes & D3DDTCAPS_DEC3N) != D3DDTCAPS_DEC3N ) {
#if __DEV
			if ( e  ) {
				grcWarningf("Ignoring normal pack request due to hardware limitations");
			}
#endif	// __DEV
			e = false;
		}
	}
#endif	// __WIN32PC && __D3D
	SetStageEnable(BIT_PACK_NORMALS, 0, e, stream);
}

void grmVtxStreamConfigurator::SetPackTexCoords(bool e, int stream) {
#if __WIN32PC && __D3D9
	if (!g_ByteSwap && g_pGRCDEVICE && GRCDEVICE.IsCreated()) {
		// See if video card can handle Packed Normals
		D3DCAPS9 caps;
		GRCDEVICE.GetCurrent()->GetDeviceCaps(&caps);
		if ( (caps.DeclTypes & D3DDTCAPS_FLOAT16_2) != D3DDTCAPS_FLOAT16_2) {
#if __DEV
			if ( e  ) {
				grcWarningf("Ignoring tex coord pack request due to hardware limitations");
			}
#endif	// __DEV
			e = false;
		}
	}
#endif	// __WIN32PC && __D3D
	SetStageEnable(BIT_PACK_TEXCOORDS, 0, e, stream);
}

void grmVtxStreamConfigurator::SetEnableTexCoord(int stage, bool e, int stream) {
	SetStageEnable(BIT_DISABLE_TEXCOORD0, stage, !e, stream);
}

void grmVtxStreamConfigurator::SetEnableTangent(int stage, bool e, int stream) {
	SetStageEnable(BIT_DISABLE_TANGENT0, stage, !e, stream);
}

void grmVtxStreamConfigurator::SetEnableBinormal(int stage, bool e, int stream) {
	SetStageEnable(BIT_DISABLE_BINORMAL0, stage, !e, stream);
}

bool grmVtxStreamConfigurator::IsStageEnabled(int baseBit, int stage, int stream) {
	grcAssertf(stage >= 0 && stage < MAX_TEX_STAGES,"Texture stage id out of range" );
	grcAssertf(stream < MAX_STREAMS && stream >= 0, "Stream (%d) must be in range [0, %d]", stream, MAX_STREAMS);
	return m_Flags[stream].IsSet(baseBit + stage);
}

bool grmVtxStreamConfigurator::AreNormalsPacked(int stream) {
	return IsStageEnabled(BIT_PACK_NORMALS, 0, stream);
}

bool grmVtxStreamConfigurator::AreTexCoordsPacked(int stream) {
	return IsStageEnabled(BIT_PACK_TEXCOORDS, 0, stream);
}

bool grmVtxStreamConfigurator::AreNormalsEnabled(int stream) {
	return !IsStageEnabled(BIT_DISABLE_NORMALS, 0, stream);
}

bool grmVtxStreamConfigurator::IsTexCoordEnabled(int stage, int stream) {
	return !IsStageEnabled(BIT_DISABLE_TEXCOORD0, stage, stream);
}

bool grmVtxStreamConfigurator::IsTangentEnabled(int stage, int stream) {
	return !IsStageEnabled(BIT_DISABLE_TANGENT0, stage, stream);
}

bool grmVtxStreamConfigurator::IsBinormalEnabled(int stage, int stream) {
	return !IsStageEnabled(BIT_DISABLE_BINORMAL0, stage, stream);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
grmModelFactory* grmModelFactory::sm_Instance;
grmModelFactory* grmModelFactory::sm_PrevInstance;
grmVtxStreamConfigurator grmModelFactory::sm_VtxCfg;


grmModelFactory::grmModelFactory() {
}


grmModelFactory::~grmModelFactory() {
	if (sm_Instance == this) {
		sm_Instance = 0;

		// Clear the tables
		while (sm_Decls.GetCount())
			sm_Decls.Pop().Decl->Release();
	}
}

grmModelFactory *grmModelFactory::CreateStandardModelFactory(bool bMakeActive)
{
	grmModelFactory *pFactory = rage_new grmModelFactory;

	if	(bMakeActive)
		sm_Instance = pFactory;

	return(pFactory);
}


#if MESH_LIBRARY
grmModel* grmModelFactory::Create(fiTokenizer &t,const char *name,int mtxIndex,grmModelInfo *outInfo, const grmShaderGroup *pShaderGroup,bool optimize,int extraVerts, u8 mask, int lod) {
	return grmModel::Create(t,name,mtxIndex,outInfo,pShaderGroup,~0U,optimize,extraVerts, mask, lod);
}
#endif

grmVtxStreamConfigurator *grmModelFactory::GetVertexConfigurator() {
	return &sm_VtxCfg;
}

int StuffElementArrayDynamic(atRangeArray<grcVertexElement, grmModelFactory::MaxVertElements> &elements, int currElemCount, const grcFvf* fvf, int stream, int& positionChannel, int& normalChannel, int& binormalChannel, int& tangentChannel, int& textureChannel, int& blendWeightChannel, int& bindingsChannel, int& UNUSED_PARAM(colorChannel))
{
	int addedCount = currElemCount;

	const int fixedVertSize=16;
	const int fixedTexSize=16;
	const int fixedBlendWeightSize=16;
	const int fixedBindingSize=4;
	const int fixedColorSize=4;

	if( fvf->GetPosChannel() )
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetPosition;
		elements[addedCount].channel = positionChannel++;
		grcAssertf(fvf->GetSize(grcFvf::grcfcPosition)==fixedVertSize, "%s fvf size was %d, expected %d", "Position", fvf->GetSize(grcFvf::grcfcPosition), fixedVertSize);
		elements[addedCount].size = fixedVertSize;
		elements[addedCount].format = grcFvf::grcdsFloat4;
		addedCount++;
	}

	if( fvf->GetNormalChannel() )
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetNormal;
		elements[addedCount].channel = normalChannel++;
		grcAssertf(fvf->GetSize(grcFvf::grcfcNormal)==fixedVertSize, "%s fvf size was %d, expected %d", "Normal", fvf->GetSize(grcFvf::grcfcNormal), fixedVertSize);
		elements[addedCount].size = fixedVertSize;
		elements[addedCount].format = grcFvf::grcdsFloat4;
		addedCount++;
	}

	for( int i = 0; i < grcTanBiCount; i++ )
	{
		if( fvf->GetBinormalChannel(i) )
		{
			elements[addedCount].stream = stream;
			elements[addedCount].type = grcVertexElement::grcvetBinormal;
			elements[addedCount].channel = binormalChannel++;
			grcAssertf(fvf->GetSize((grcFvf::grcFvfChannels)((int)(grcFvf::grcfcBinormal0)+i))==fixedVertSize, "%s %d fvf size was %d, expected %d", "Binormal", i, fvf->GetSize((grcFvf::grcFvfChannels)((int)(grcFvf::grcfcBinormal0)+i)), fixedVertSize);
			elements[addedCount].size = fixedVertSize;
			elements[addedCount].format = grcFvf::grcdsFloat4;
			addedCount++;
		}
	}

	for( int i = 0; i < grcTanBiCount; i++ )
	{
		if( fvf->GetTangentChannel(i) )
		{
			elements[addedCount].stream = stream;
			elements[addedCount].type = grcVertexElement::grcvetTangent;
			elements[addedCount].channel = tangentChannel++;
			grcAssertf(fvf->GetSize((grcFvf::grcFvfChannels)((int)(grcFvf::grcfcTangent0)+i))==fixedVertSize, "%s %d fvf size was %d, expected %d", "Tangent", i, fvf->GetSize((grcFvf::grcFvfChannels)((int)(grcFvf::grcfcTangent0)+i)), fixedVertSize);
			elements[addedCount].size = fixedVertSize;
			elements[addedCount].format = grcFvf::grcdsFloat4;
			addedCount++;
		}
	}

	for( int i = 0; i < grcTexStageCount; i++ )
	{
		if( fvf->GetTextureChannel(i) )
		{
			elements[addedCount].stream = stream;
			elements[addedCount].type = grcVertexElement::grcvetTexture;
			elements[addedCount].channel = textureChannel++;
			grcAssertf(fvf->GetSize((grcFvf::grcFvfChannels)((int)(grcFvf::grcfcTexture0)+i))==fixedTexSize, "%s %d fvf size was %d, expected %d", "Texture", i, fvf->GetSize((grcFvf::grcFvfChannels)((int)(grcFvf::grcfcTexture0)+i)), fixedVertSize);
			elements[addedCount].size = fixedTexSize;
			elements[addedCount].format = grcFvf::grcdsFloat4;
			addedCount++;
		}
	}

	if( fvf->GetBlendWeightChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetBlendWeights;
		elements[addedCount].channel = blendWeightChannel++;
		grcAssertf(fvf->GetSize(grcFvf::grcfcWeight)==fixedBlendWeightSize, "%s fvf size was %d, expected %d", "BlendWeight", fvf->GetSize(grcFvf::grcfcWeight), fixedBlendWeightSize);
		elements[addedCount].size = fixedBlendWeightSize;
		elements[addedCount].format = grcFvf::grcdsFloat4;
		addedCount++;
	}

	if( fvf->GetBindingsChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetBindings;
		elements[addedCount].channel = bindingsChannel++;
		grcAssertf(fvf->GetSize(grcFvf::grcfcBinding)==fixedBindingSize, "%s fvf size was %d, expected %d", "Binding", fvf->GetSize(grcFvf::grcfcBinding), fixedBindingSize);
		elements[addedCount].size = fixedBindingSize;
		elements[addedCount].format = fvf->GetDataSizeType(grcFvf::grcfcBinding);
		addedCount++;
	}	

	if( fvf->GetDiffuseChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetColor;
		elements[addedCount].channel = 0; // diffuse is always color channel 0
		grcAssertf(fvf->GetSize(grcFvf::grcfcDiffuse)==fixedColorSize, "%s fvf size was %d, expected %d", "Diffuse", fvf->GetSize(grcFvf::grcfcDiffuse), fixedColorSize);
		elements[addedCount].size = fixedColorSize;
		elements[addedCount].format = grcFvf::grcdsColor;
		addedCount++;
	}

	if( fvf->GetSpecularChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetColor;
		elements[addedCount].channel = 1;  // specular is always color channel 1 (if we use 'colorChannel' counter, specual ends up in channel 0 if there is not diffuse)
		grcAssertf(fvf->GetSize(grcFvf::grcfcSpecular)==fixedColorSize, "%s fvf size was %d, expected %d", "Specular", fvf->GetSize(grcFvf::grcfcSpecular), fixedColorSize);
		elements[addedCount].size = fixedColorSize;
		elements[addedCount].format = grcFvf::grcdsColor;
		addedCount++;
	}

	return addedCount;
}

int StuffElementArray(atRangeArray<grcVertexElement, grmModelFactory::MaxVertElements> &elements, int currElemCount, const grcFvf* fvf, int stream, int& positionChannel, int& normalChannel, int& binormalChannel, int& tangentChannel, int& textureChannel, int& blendWeightChannel, int& bindingsChannel, int& UNUSED_PARAM(colorChannel))
{
	int addedCount = currElemCount;

	if( fvf->GetPosChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetPosition;
		elements[addedCount].channel = positionChannel++;
		elements[addedCount].size = fvf->GetSize(grcFvf::grcfcPosition);
		elements[addedCount].format = fvf->GetDataSizeType(grcFvf::grcfcPosition);
		addedCount++;
	}

	if( fvf->GetBlendWeightChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetBlendWeights;
		elements[addedCount].channel = blendWeightChannel++;
		elements[addedCount].size = fvf->GetSize(grcFvf::grcfcWeight);
		elements[addedCount].format = fvf->GetDataSizeType(grcFvf::grcfcWeight);
		addedCount++;
	}

	if( fvf->GetBindingsChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetBindings;
		elements[addedCount].channel = bindingsChannel++;
		elements[addedCount].size = fvf->GetSize(grcFvf::grcfcBinding);
		elements[addedCount].format = fvf->GetDataSizeType(grcFvf::grcfcBinding);
		addedCount++;
	}

	if( fvf->GetNormalChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetNormal;
		elements[addedCount].channel = normalChannel++;
		elements[addedCount].size = fvf->GetSize(grcFvf::grcfcNormal);
		elements[addedCount].format = fvf->GetDataSizeType(grcFvf::grcfcNormal);
		addedCount++;
	}

	if( fvf->GetDiffuseChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetColor;
		elements[addedCount].channel = 0; // diffuse is always color channel 0
		elements[addedCount].size = fvf->GetSize(grcFvf::grcfcDiffuse);
		elements[addedCount].format = fvf->GetDataSizeType(grcFvf::grcfcDiffuse);
		addedCount++;
	}

	if( fvf->GetSpecularChannel() ) 
	{
		elements[addedCount].stream = stream;
		elements[addedCount].type = grcVertexElement::grcvetColor;
		elements[addedCount].channel = 1;  // specular is always color channel 1 (if we use 'colorChannel' counter, specual ends up in channel 0 if there is not diffuse)
		elements[addedCount].size = fvf->GetSize(grcFvf::grcfcSpecular);
		elements[addedCount].format = fvf->GetDataSizeType(grcFvf::grcfcSpecular);
		addedCount++;
	}

	for( int i = 0; i < grcTexStageCount; i++ )
	{
		if( fvf->GetTextureChannel(i) )
		{
			elements[addedCount].stream = stream;
			elements[addedCount].type = grcVertexElement::grcvetTexture;
			elements[addedCount].channel = textureChannel++;
			elements[addedCount].size = fvf->GetSize((grcFvf::grcFvfChannels)(grcFvf::grcfcTexture0 + i));
			elements[addedCount].format = fvf->GetDataSizeType((grcFvf::grcFvfChannels)(grcFvf::grcfcTexture0 + i));

#if USE_PACKED_TEXCOORDS // Shaders have to be updated to expect this.  (see matching USE_PACKED_TEXCOORDS check in grmodel/geometry.cpp)
			if ( i+1 < grcTexStageCount && fvf->GetTextureChannel(i+1) &&
					fvf->GetDataSizeType((grcFvf::grcFvfChannels)(grcFvf::grcfcTexture0 + i)) == fvf->GetDataSizeType((grcFvf::grcFvfChannels)(grcFvf::grcfcTexture0 + i + 1)) )
			{
				// Combine two half2's into a half4
				if (elements[addedCount].format == grcFvf::grcdsHalf2)
				{
					elements[addedCount].size *= 2;
					elements[addedCount].format = grcFvf::grcdsHalf4;
					++i;
				}
				// Combine two short2's into a short4
				if (elements[addedCount].format == grcFvf::grcdsShort2)
				{
					elements[addedCount].size *= 2;
					elements[addedCount].format = grcFvf::grcdsShort4;
					++i;
				}	
				// Combine two float2's into a float4
				else if (elements[addedCount].format == grcFvf::grcdsFloat2)
				{
					elements[addedCount].size *= 2;
					elements[addedCount].format = grcFvf::grcdsFloat4;
					++i;
				}
				// otherwise not a valid case, leave it alone.
			}
#endif // USE_PACKED_TEXCOORDS

			addedCount++;
		}
	}

	for( int i = 0; i < grcTanBiCount; i++ )
	{
		if( fvf->GetTangentChannel(i) )
		{
			elements[addedCount].stream = stream;
			elements[addedCount].type = grcVertexElement::grcvetTangent;
			elements[addedCount].channel = tangentChannel++;
			elements[addedCount].size = fvf->GetSize((grcFvf::grcFvfChannels)(grcFvf::grcfcTangent0 + i));
			elements[addedCount].format = fvf->GetDataSizeType((grcFvf::grcFvfChannels)(grcFvf::grcfcTangent0 + i));
			addedCount++;
		}
	}

	for( int i = 0; i < grcTanBiCount; i++ )
	{
		if( fvf->GetBinormalChannel(i) )
		{
			elements[addedCount].stream = stream;
			elements[addedCount].type = grcVertexElement::grcvetBinormal;
			elements[addedCount].channel = binormalChannel++;
			elements[addedCount].size = fvf->GetSize((grcFvf::grcFvfChannels)(grcFvf::grcfcBinormal0 + i));
			elements[addedCount].format = fvf->GetDataSizeType((grcFvf::grcFvfChannels)(grcFvf::grcfcBinormal0 + i));
			addedCount++;
		}
	}

	return addedCount;
}

int grmModelFactory::ComputeVertexElements(const grcFvf *fvf0, const grcFvf *fvf1, const grcFvf *fvf2, const grcFvf *fvf3, atRangeArray<grcVertexElement, MaxVertElements> &elements, atRangeArray<int, grcVertexElement::grcvetCount> &elemCounts)
{
	const grcFvf *fvfs[4] = {fvf0, fvf1, fvf2, fvf3};

	// Bypass hash table, always create
	sysMemStartTemp();
	int elementIndex = 0;
	std::fill(elemCounts.begin(), elemCounts.end(), 0);

	for(int i = 0; i < 4; ++i)
	{
		if(fvfs[i])
		{
			if( fvfs[i]->IsDynamicOrder() )
				elementIndex = StuffElementArrayDynamic(elements, elementIndex, fvfs[i], i, elemCounts[grcVertexElement::grcvetPosition], elemCounts[grcVertexElement::grcvetNormal], elemCounts[grcVertexElement::grcvetBinormal], elemCounts[grcVertexElement::grcvetTangent], elemCounts[grcVertexElement::grcvetTexture], elemCounts[grcVertexElement::grcvetBlendWeights], elemCounts[grcVertexElement::grcvetBindings], elemCounts[grcVertexElement::grcvetColor]);
			else
				elementIndex = StuffElementArray(elements, elementIndex, fvfs[i], i, elemCounts[grcVertexElement::grcvetPosition], elemCounts[grcVertexElement::grcvetNormal], elemCounts[grcVertexElement::grcvetBinormal], elemCounts[grcVertexElement::grcvetTangent], elemCounts[grcVertexElement::grcvetTexture], elemCounts[grcVertexElement::grcvetBlendWeights], elemCounts[grcVertexElement::grcvetBindings], elemCounts[grcVertexElement::grcvetColor]);
		}
	}

	if (fvfs[0] && fvfs[0]->GetBindingsChannel() &&
		((__XENON && MTX_IN_VB) || (__RESOURCECOMPILER && g_sysPlatform == platform::XENON)))
	{
		for (int i=0; i<3; i++)
		{
			elements[elementIndex].stream = MTX_IN_VB_STREAM;
			elements[elementIndex].type = grcVertexElement::grcvetPosition;
			elements[elementIndex].channel = 5+i;
			elements[elementIndex].size = MTX_IN_VB_HALF? 8 : 16;
			elements[elementIndex].format = MTX_IN_VB_HALF? grcFvf::grcdsHalf4 : grcFvf::grcdsFloat4;
			elementIndex++;
		}
	}

	sysMemEndTemp();

	return elementIndex;
}

grcVertexDeclaration *grmModelFactory::BuildCachedDeclarator(const grcFvf *fvf0, const grcFvf *fvf1, const grcFvf *fvf2, const grcFvf *fvf3, atRangeArray<grcVertexElement, MaxVertElements> &elements, int numElem)
{
	grcVertexDeclaration *retVal = 0;
	const grcFvf *fvfs[4] = {fvf0, fvf1, fvf2, fvf3};

#if __OPENGL
	grcAssertf(fvf1 == 0 && fvf2 == 0 && fvf3 == 0, "Only one vertex stream is allowed in OpenGL");
#endif

	// Build a label out of the vertex elements to use as a key
	u32 label = 0;
	for(int i = 0; i < numElem; ++i)
		label = atDataHash(reinterpret_cast<const char*>(&elements[i]), sizeof(grcVertexElement), label);

	// Find in sorted table.  Note that we don't mess with reference counts because declarators stay around for good
	// now to minimize fragmentation.
	DeclPair* ret = std::lower_boundRSG(sm_Decls.begin(), sm_Decls.end(), label, DeclPair::SearchCompare);
	if (ret != sm_Decls.end() && ret->Hash == label)
		return ret->Decl;

	// Not in hash table yet, so time to create
	sysMemStartTemp();
	bool strideOverride = fvfs[0] && !fvfs[1] && !fvfs[2] && !fvfs[3];

	retVal = GRCDEVICE.CreateVertexDeclaration(elements.GetElements(), numElem, strideOverride ? fvfs[0]->GetTotalSize() : 0);

	if (strideOverride && retVal)
	{
		if (fvfs[0]->GetTotalSize() < (int) retVal->Stream0Size)
		{
			grcDisplayf("fvf total size=%d; vertex declarator size=%d", fvfs[0]->GetTotalSize(), (int)retVal->Stream0Size);

			for (int i=0; i<numElem; i++)
			{
				grcDisplayf("elem %d. stream %d; type %d; channel %d; size %d; format %d",
					i,
					elements[i].stream,
					elements[i].type,
					elements[i].channel,
					elements[i].size,
					elements[i].format);
			}

			grcAssertf(false,"fvf/decl mismatch. Please see TTY spew.");
		}
	}

	if (!retVal) {
		grcWarningf("CreateVertexDeclaration failed?");
	}
	else {
		DeclPair &newKey = sm_Decls.Append();
		newKey.Hash = label;
		newKey.Decl = retVal;
		std::sort(sm_Decls.begin(), sm_Decls.end(), DeclPair::SortCompare);
		grcDebugf1("[GRAPHICS] Peak FVF usage is now %d items.",sm_Decls.GetCount());
	}
	sysMemEndTemp();

	return retVal;
}

grcVertexDeclaration *grmModelFactory::BuildDeclarator(const grcFvf *fvf0, const grcFvf *fvf1, const grcFvf *fvf2, const grcFvf *fvf3) 
{
	atRangeArray<grcVertexElement, MaxVertElements> elements;
	atRangeArray<int, grcVertexElement::grcvetCount> elemCounts;
	int numElem = ComputeVertexElements(fvf0, fvf1, fvf2, fvf3, elements, elemCounts);
	return BuildCachedDeclarator(fvf0, fvf1, fvf2, fvf3, elements, numElem);
}

//#endif

void grmModelFactory::FreeDeclarator(grcVertexDeclaration *) {
	// Intentionally do nothing, to minimize fragmentation due to only instances of certain declarators
	// coming in and out of memory
}

void grmModelFactory::PlaceModel(datResource &rsc,grmModel *&model) {
	if (model) {
		rsc.PointerFixup(model);
		// switch(model->GetType()...)
		::new (model) grmModelGeom(rsc);
	}
}
