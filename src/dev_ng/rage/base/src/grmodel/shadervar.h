// 
// grmodel/shadervar.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRMODEL_SHADERVAR_H 
#define GRMODEL_SHADERVAR_H 

#include "grcore/effect.h"

namespace rage {

class grmShader;

class grmShaderVar {
public:
	grmShaderVar(grcInstanceData&,grcEffectVar);

	const char *GetName() const { return Name; }
	grcEffect::VarType GetType() const { return Type; }

	void Set(const grcTexture* t) { Effect->SetVar(*Data,Handle,t); }
	void Set(float v) { Effect->SetVar(*Data,Handle,v); }
	void Set(const Vector2& v) { Effect->SetVar(*Data,Handle,v); }
	void Set(const Vector3& v) { Effect->SetVar(*Data,Handle,v); }
	void Set(const Vector4& v) { Effect->SetVar(*Data,Handle,v); }
	void Set(const Matrix34& v) { Effect->SetVar(*Data,Handle,v); }
	void Set(const Matrix44& v) { Effect->SetVar(*Data,Handle,v); }

	void Get(grcTexture*& t) const { Effect->GetVar(*Data,Handle,t); }
	void Get(float& v) const { Effect->GetVar(*Data,Handle,v); }
	void Get(Vector2& v) const { Effect->GetVar(*Data,Handle,v); }
	void Get(Vector3& v) const { Effect->GetVar(*Data,Handle,v); }
	void Get(Vector4& v) const { Effect->GetVar(*Data,Handle,v); }
	void Get(Matrix34& v) const { Effect->GetVar(*Data,Handle,v); }
	void Get(Matrix44& v) const { Effect->GetVar(*Data,Handle,v); }

	// PURPOSE:	Loads the shader variable from the supplied string payload.
	void Load(const char *src);

	// PURPOSE: Saves the shader variable's contents to the supplied string payload.
	void Save(char *dest,int destSize);

private:
	const char *Name;
	grcEffect *Effect;
	grcInstanceData *Data;
	grcEffectVar Handle;
	grcEffect::VarType Type;
};

} // namespace rage

#endif // GRMODEL_SHADERVAR_H 
