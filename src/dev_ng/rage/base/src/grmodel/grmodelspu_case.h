BEGIN_SPU_SIMPLE_COMMAND(grmShaderFx__SetForcedTechniqueGroupId)
	SPU_BOOKMARK(0x1400);
	pSpuGcmState->ForcedTechniqueGroupId = any->subcommand;
END_SPU_COMMAND

BEGIN_SPU_COMMAND(grmModel__SetForceShader)
	SPU_BOOKMARK(0x1410);
	spuScratch = scratchSave = initialScratchSave; // recycle scratch storage allocated by a previous SetForceShader command
	pSpuGcmState->forceShader = cmd->forceShader;
	UpdateForceShader();
	if (cmd->forceShader)	// If we set a shader, remember the new high water mark.
		scratchSave = spuScratch;
END_SPU_COMMAND

#if SPU_GCM_FIFO
BEGIN_SPU_COMMAND(grmGeometryEdge__Draw)
	SPU_BOOKMARK(0x1420);
#if !__FINAL
	pSpuGcmState->CurrentDrawable = NULL;
	pSpuGcmState->CurrentGeometry = cmd->geometry;
#endif // !__FINAL
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		pSpuGcmState->gSpuGta4DebugInfo = cmd->gta4DebugInfo;
	#endif
	spuGet(cmd->geometry);

#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
	pSpuGcmState->ModelCullStatus = 0;
#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES_FOR_NON_AABB_MODELS
	NOTFINAL_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_CULL_TRIANGLES) == 0))
	{
		Vec4V* transposedPlanes = (Vec4V*)pSpuGcmState->EdgeClipPlanesTransposed; // write transformed transposed clip planes back to pSpuGcmState->EdgeClipPlanesTransposed

		if (grcState__BuildEdgeClipPlanes(transposedPlanes, EDGE_NUM_TRIANGLE_CLIP_PLANES, pSpuGcmState))
		{
			// TODO -- how does this work? we need the localspace AABB of the model to determine ModelCullStatus
			pSpuGcmState->ModelCullStatus = 1;
		}
	}
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES_FOR_NON_AABB_MODELS
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES

	DEV_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_GRMGEOMETRYEDGE_DRAW) == 0))
	{
		cmd->geometry->Draw();
	}

	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		pSpuGcmState->gSpuGta4DebugInfo.Invalidate();
	#endif
END_SPU_COMMAND

BEGIN_SPU_COMMAND(grmGeometryEdge__DrawSkinned)
	SPU_BOOKMARK(0x1430);
#if !__FINAL
	pSpuGcmState->CurrentDrawable = NULL;
	pSpuGcmState->CurrentGeometry = cmd->geometry;
#endif // !__FINAL
	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		pSpuGcmState->gSpuGta4DebugInfo = cmd->gta4DebugInfo;
	#endif
	// This is a little weird -- we intentionally don't pull over cmd->geometry or cmd->ms, because
	// we need their PPU-side address in order to create the edge job.
	spuGet(cmd->geometry);

#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
	pSpuGcmState->ModelCullStatus = 0;
#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES_FOR_NON_AABB_MODELS
	NOTFINAL_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_CULL_TRIANGLES) == 0))
	{
		Vec4V* transposedPlanes = (Vec4V*)pSpuGcmState->EdgeClipPlanesTransposed; // write transformed transposed clip planes back to pSpuGcmState->EdgeClipPlanesTransposed

		if (grcState__BuildEdgeClipPlanes(transposedPlanes, EDGE_NUM_TRIANGLE_CLIP_PLANES, pSpuGcmState))
		{
			// TODO -- how does this work? we need the localspace AABB of the model to determine ModelCullStatus
			pSpuGcmState->ModelCullStatus = 1;
		}
	}
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES_FOR_NON_AABB_MODELS
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES

	DEV_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_GRMGEOMETRYEDGE_DRAWSKINNED) == 0))
	{
#if SPU_GCM_FIFO > 2
		spuMatrixSet *ms = (spuMatrixSet*) spuGetData(cmd->ms, sizeof(spuMatrixSet));
		ms->PPUAddress = cmd->ms->m_Matrices;
		cmd->geometry->DrawSkinned(*ms
	#if HACK_GTA4
				,true // trigger vertex cache flush (for skinned cars)
	#endif
			);
#else
		cmd->geometry->DrawSkinned(*cmd->ms
	#if HACK_GTA4
				,true // trigger vertex cache flush (for skinned cars)
	#endif
			);
#endif
	}

	#if HACK_GTA4_MODELINFOIDX_ON_SPU
		pSpuGcmState->gSpuGta4DebugInfo.Invalidate();
	#endif
END_SPU_COMMAND

BEGIN_SPU_SIMPLE_COMMAND(grmGeometryEdge__SetBlendHeaders)
   SPU_BOOKMARK(0x1440);
	pSpuGcmState->BlendShapeDrawBuffer = any->subcommand;
END_SPU_COMMAND

BEGIN_SPU_COMMAND(grmShaderGroup__SetVarCommon)
	effectCache.FlushAll();
	CopyMultiInstanceData(cmd,(char*)cmd->alignedPayload);
END_SPU_COMMAND

BEGIN_SPU_COMMAND(grmShaderGroup__SetVarCommonByRef)
	effectCache.FlushAll();
	char *src = spuScratch;
	uint32_t srcSize = ((cmd->arrayCount * cmd->stride) + 15) & ~15;
	spuScratch += srcSize;
	sysDmaGetAndWait(src,(uint64_t)cmd->src,srcSize,spuGetTag);
	CopyMultiInstanceData(cmd,src);
END_SPU_COMMAND

#endif

// EOF
