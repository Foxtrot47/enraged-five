// 
// grmodel/matrixset.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "matrixset.h"

#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "data/safestruct.h"
#include "system/memops.h"
#include "system/platform.h"

#include "shaderlib/skinning_method_config.h"

#if __XENON
#include "system/cache.h"
#include "system/xtl.h"
#include "system/xgraphics.h"
#endif

#if __RESOURCECOMPILER
#include "system/xtl.h"
#include "grcore/xenon_resource.h"
#elif __XENON
#include "system/xtl.h"
#include "math/float16.h"
#endif

#if MTX_IN_CB
#include "grcore/effect.h"
#endif // MTX_IN_CB

namespace rage {

CompileTimeAssert(sizeof(grmMatrixSet)==32);
CompileTimeAssert(sizeof(Matrix43)==48);

#if MTX_IN_CB
__THREAD u32 s_CurrentInstanceId=0;
__THREAD u32 s_NextInstanceId=1;
#endif // MTX_IN_CB

void grmMatrixSet::InitDriverHeader() {
	// Note, we don't use canned D3D functions because we can't stomp on Identifier field.
#if MTX_IN_VB && __XENON
	D3DCommon = D3DCOMMON_TYPE_VERTEXBUFFER | D3DCOMMON_CPU_CACHED_MEMORY;
	D3DReferenceCount = 1;
	D3DFence = 0;
	D3DReadFence = 0;
	// Leave Identifier field untouched, that's where the data we care about lives.
	D3DBaseFlush = D3DFLUSH_INITIAL_VALUE;;
	D3DBaseAddress = (u32)m_MatrixData | 3/*VERTEX*/;
	D3DSize = 2/*ENDIAN*/ | (m_MaxMatrixCount * sizeof(MatrixType)) | (/* REQUESTSIZE = 512bit */ 1<<28);
#elif __PS3
	D3DBaseAddress = (u32) m_MatrixData;		// PPUAddress on SPU side
#endif

	SetDirty(true);
}

grmMatrixSet::grmMatrixSet(datResource &) {
	InitDriverHeader();
}

IMPLEMENT_PLACE(grmMatrixSet)

#if __DECLARESTRUCT
void grmMatrixSet::DeclareStruct(datTypeStruct &s)
{
	SSTRUCT_BEGIN(grmMatrixSet)
	SSTRUCT_FIELD(grmMatrixSet,D3DCommon)
	SSTRUCT_FIELD(grmMatrixSet,D3DReferenceCount)
	SSTRUCT_FIELD(grmMatrixSet,D3DFence)
	SSTRUCT_FIELD(grmMatrixSet,D3DReadFence)
	SSTRUCT_FIELD(grmMatrixSet,m_MatrixCount)
	SSTRUCT_FIELD(grmMatrixSet,m_MaxMatrixCount)
	SSTRUCT_FIELD(grmMatrixSet,m_IsSkinned)
	SSTRUCT_FIELD(grmMatrixSet,m_Padding)
	SSTRUCT_FIELD(grmMatrixSet,D3DBaseFlush)
	SSTRUCT_FIELD(grmMatrixSet,D3DBaseAddress)
	SSTRUCT_FIELD(grmMatrixSet,D3DSize)
	SSTRUCT_END(grmMatrixSet)
}
#endif

void grmMatrixSet::Init(int boneCount) {
	Assign(m_MatrixCount,boneCount);
	Assign(m_MaxMatrixCount,boneCount);
	m_IsSkinned = 0;
	InitDriverHeader();
}

#if __RESOURCECOMPILER
unsigned grmMatrixSet::ComputeSize(int boneCount) {
	return (MTX_IN_VB && g_sysPlatform==platform::XENON)
		? sizeof(grmMatrixSet) + boneCount * (MTX_IN_VB_HALF? sizeof(Matrix43_Float16) : sizeof(Matrix43))
		: sizeof(grmMatrixSet) + boneCount * sizeof(MatrixType);
}
#endif

grmMatrixSet* grmMatrixSet::Create(int boneCount) {
	grmMatrixSet *result = (grmMatrixSet*) (Allocate(grmMatrixSet::ComputeSize(boneCount)));
	result->Init(boneCount);
	return result;
}

unsigned grmMatrixSet::Create(void *placement,int boneCount) {
	grmMatrixSet *result = (grmMatrixSet*) placement;
	result->Init(boneCount);
	return ComputeSize(boneCount);
}

grmMatrixSet* grmMatrixSet::Create(const crSkeleton &skel) {
	return Create(skel.GetBoneCount());
}

void grmMatrixSet::Update(const crSkeleton &skel,bool isSkinned) {
	grcAssertf(m_MatrixCount >= skel.GetSkeletonData().GetNumBones(), "Skeleton's bone count (%d) is too high for the matrix set (%d)", skel.GetSkeletonData().GetNumBones(), m_MatrixCount);

	skel.Attach(isSkinned, m_MatrixData);

	m_IsSkinned = isSkinned;

#if 0
#if __RESOURCECOMPILER
	if (MTX_IN_VB_HALF && g_sysPlatform==platform::XENON) {
		Matrix43_Float16 *dest = (Matrix43_Float16*) m_MatrixData;
		for (int i=0; i<mtxCount; i++)
			dest[i].CopyWithOffset(mtx[i],offset);
	}
#elif __XENON && MTX_IN_VB_HALF
	const __vector4 *srcMtx = (__vector4*) mtx;
	__vector4 *dest = (__vector4*) m_MatrixData;
	__vector4 offs = *(__vector4*)&offset;
	int count = mtxCount >> 1;
	while (count--) {
		// Read two full matrices, subtract the offset
		__vector4 a0 = srcMtx[0];
		__vector4 b0 = srcMtx[1];
		__vector4 c0 = srcMtx[2];
		__vector4 d0 = __vsubfp(srcMtx[3],offs);
		__vector4 a1 = srcMtx[4];
		__vector4 b1 = srcMtx[5];
		__vector4 c1 = srcMtx[6];
		__vector4 d1 = __vsubfp(srcMtx[7],offs);
		srcMtx += 8;

		// Transpose them
		__vector4 temp0 = __vmrghw(a0, c0);
	    __vector4 temp1 = __vmrghw(b0, d0);
		__vector4 temp2 = __vmrglw(a0, c0);
		__vector4 temp3 = __vmrglw(b0, d0);
		__vector4 x0 = __vmrghw(temp0, temp1);
		__vector4 y0 = __vmrglw(temp0, temp1);
		__vector4 z0 = __vmrghw(temp2, temp3);
		__vector4 temp4 = __vmrghw(a1, c1);
	    __vector4 temp5 = __vmrghw(b1, d1);
		__vector4 temp6 = __vmrglw(a1, c1);
		__vector4 temp7 = __vmrglw(b1, d1);
		__vector4 x1 = __vmrghw(temp4, temp5);
		__vector4 y1 = __vmrglw(temp4, temp5);
		__vector4 z1 = __vmrghw(temp6, temp7);

		// TODO: Clamp them...

		// Pack them
		x0 = __vpkd3d(x0,x0,VPACK_FLOAT16_4,VPACK_64LO,2);
		x0 = __vpkd3d(x0,y0,VPACK_FLOAT16_4,VPACK_64LO,0);
		z0 = __vpkd3d(z0,z0,VPACK_FLOAT16_4,VPACK_64LO,2);
		z0 = __vpkd3d(z0,x1,VPACK_FLOAT16_4,VPACK_64LO,0);
		y1 = __vpkd3d(y1,y1,VPACK_FLOAT16_4,VPACK_64LO,2);
		y1 = __vpkd3d(y1,z1,VPACK_FLOAT16_4,VPACK_64LO,0);

		// Store them
		dest[0] = x0;
		dest[1] = z0;
		dest[2] = y1;
		dest += 3;
	}
	// Do the leftover if there is one.
	// We know that the size will be allocated to a multiple of 16
	if (mtxCount & 1) {
		__vector4 a0 = srcMtx[0];
		__vector4 b0 = srcMtx[1];
		__vector4 c0 = srcMtx[2];
		__vector4 d0 = __vsubfp(srcMtx[3],offs);
		__vector4 temp0 = __vmrghw(a0, c0);
	    __vector4 temp1 = __vmrghw(b0, d0);
		__vector4 temp2 = __vmrglw(a0, c0);
		__vector4 temp3 = __vmrglw(b0, d0);
		__vector4 x0 = __vmrghw(temp0, temp1);
		__vector4 y0 = __vmrglw(temp0, temp1);
		__vector4 z0 = __vmrghw(temp2, temp3);
		x0 = __vpkd3d(x0,x0,VPACK_FLOAT16_4,VPACK_64LO,2);
		x0 = __vpkd3d(x0,y0,VPACK_FLOAT16_4,VPACK_64LO,0);
		z0 = __vpkd3d(z0,z0,VPACK_FLOAT16_4,VPACK_64LO,2);
		z0 = __vpkd3d(z0,z0 /*x1 - garbage is okay*/,VPACK_FLOAT16_4,VPACK_64LO,0);
		dest[0] = x0;
		dest[1] = z0;
	}
#endif

#endif // if 0

#if __XENON
	WritebackDC(this,ComputeSize(m_MatrixCount));
#endif

	SetDirty(true);
}


void grmMatrixSet::CopySet(const grmMatrixSet & src)
{
	grcAssertf(!__RESOURCECOMPILER, "Don't call this in resource compilers");		// could be supported if necessary, needs logic similar to Update
	grcAssertf(m_MaxMatrixCount>= src.m_MaxMatrixCount, "Trying to copy %d matricies, but there's only room for %d", src.m_MaxMatrixCount, m_MatrixCount);

	int count = src.m_MaxMatrixCount;
	if (m_MaxMatrixCount < src.m_MaxMatrixCount)
		count = m_MaxMatrixCount; // better than total failure, but should never happen!
	
	Assign(m_MatrixCount,count);
	
	m_IsSkinned = src.m_IsSkinned;

	sysMemCpy(m_MatrixData, src.m_MatrixData, m_MatrixCount*sizeof(MatrixType)); // or sizeof (Matrix43_Float16)?

#if __XENON
	WritebackDC(this,ComputeSize(m_MatrixCount));
#endif
	SetDirty(true);
}

#if MTX_IN_CB

void grmMatrixSet::Bind(grcCBuffer* pCBuffer) const 
{
#if __D3D11
	if (IsDirty())
	{
		const u32	bufferSize = m_MaxMatrixCount * sizeof(MatrixType);
		void*		pData = pCBuffer->BeginUpdate(bufferSize);
		Assert(bufferSize <= pCBuffer->GetSize());

		sysMemCpy(pData, (void*)m_MatrixData, bufferSize);
		pCBuffer->EndUpdate();
		SetDirty(false);
	}
#endif // __D3D11
}

void grmMatrixSet::UnBind() const 
{
}

void grmMatrixSet::SetDirty(bool dirty/*=true*/) const
{
	if (dirty)
	{
		m_InstanceId = s_NextInstanceId++;
	}
	else
	{
		s_CurrentInstanceId=m_InstanceId;
	}
}

bool grmMatrixSet::IsDirty() const
{ 
	return (s_CurrentInstanceId != m_InstanceId);
}

void grmMatrixSet::MakeDirty()
{
	s_CurrentInstanceId = 0;
}
#endif // MTX_IN_CB

void grmMatrixSet::Zero(int idx) {
	grcAssertf(!__RESOURCECOMPILER, "Don't call this in resource compilers");		// could be supported if necessary, needs logic similar to Update

	MatrixType &mtx = m_MatrixData[idx];
	mtx.x = mtx.y = mtx.z = Vec::V4VConstant(V_ZERO);

	SetDirty(true);
}


void grmMatrixSet::ComputeMatrix(Matrix34 &dest,int idx) const {
	grcAssertf(!__RESOURCECOMPILER, "Don't call this in resource compilers");		// could be supported if necessary, needs logic similar to Update
	grcAssertf((idx >= 0) && (idx < m_MatrixCount), "Invalid matrix index");

	if ((idx < 0) || (idx >= m_MatrixCount) || (idx >= m_MatrixCount))
		return;

	m_MatrixData[idx].ToMatrix34(RC_MAT34V(dest));
	SetDirty(true);
}


void *grmMatrixSet::Allocate(unsigned size) {
	return rage_aligned_new(128) char[size];
}


void grmMatrixSet::Free(void *ptr) {
	delete[] (char*) ptr;
}


} // namespace rage
