// 
// grmodel/shadersorter.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if 0
#include "shadersorter.h"
#include "shader.h"
#include "model.h"
#include "geometry.h"
#include "system/cache.h"
#include "string/stringhash.h"

namespace rage {

grmShaderSorter::grmShaderSorter() {
}


grmShaderSorter::~grmShaderSorter() {
}

void grmShaderSorter::Init(int maxShaders,int maxItems, int maxShaderTypes ) 
{
	m_HeadData.Reserve(maxShaders);
	m_SortData.Reserve(maxItems);
	m_ShaderToHeadLUT.Resize( maxShaderTypes );

	Reset();
}

void grmShaderSorter::DrawNoState(const grmShader* shader,const grmModel &model,int materialIndex,int /*lod*/)
{
	grcAssertf( grmModel::GetForceShader() != 0, "No forced shader was set" );

	SortData &sd = m_SortData.Append();
	m_ForceShader = shader;
	If_Assert( dynamic_cast<const grmStaticGeometry*>( &model.GetGeometry( materialIndex ) ) )
	{
		const grmStaticGeometry* g = reinterpret_cast<const grmStaticGeometry*>( &model.GetGeometry( materialIndex ));
		sd.Model = g;
		PrefetchDC( g );  // stick into L2 cache for later
	}
	sd.Next = 0;
}

// A bit of a hack in case we change our mind.
#define m_SortKey m_EffectInstance.SortKey

void grmShaderSorter::Draw(const grmShader& shader,const grmStaticGeometry* geometry )
{
	grcAssertf(!__TOOL, "Can't draw with the shader sorter in __TOOL builds");

	HeadData *hd = NULL;
	if (shader.m_SortKey == 0) 
	{
		u32 id = shader.GetTemplateIndex();
		if ( !m_ShaderToHeadLUT[id] )
		{
			grcAssertf( m_HeadData.GetCount() != 0,  "No shaders to draw with");
			Assign( m_ShaderToHeadLUT[id] ,m_HeadData.GetCount() );
			const_cast<grmShader&>(shader).m_SortKey = m_HeadData.GetCount();

			hd = &m_HeadData.Append();
			hd->Shader = &shader;
#if _USE_FIRST
			hd->First = 0;
#endif
			hd->Last = 0;
			hd->Next =0;
		}
		else
		{
			hd = &m_HeadData[m_ShaderToHeadLUT[id]];

			const_cast<grmShader&>(shader).m_SortKey = m_HeadData.GetCount();

			u16 hcnt;
			Assign( hcnt, m_HeadData.GetCount());

			HeadData *nextHd = &m_HeadData.Append();
			// insert in material order
			nextHd->Next = hd->Next;
			grcAssertf( nextHd->Next < hcnt, "Invalid index (%d) should be < %d", nextHd->Next, hcnt);

			hd->Next = hcnt;

			hd = nextHd;
			hd->Shader = &shader;
#if _USE_FIRST
			hd->First = 0;
#endif
			hd->Last = 0;
		}

	}
	else
		hd = &m_HeadData[shader.m_SortKey];

	u16 newSd = (u16) m_SortData.GetCount();

#if _USE_FIRST
	if (hd->Last)
		m_SortData[hd->Last].Next = newSd;
	hd->Last = newSd;
	if (!hd->First)
		hd->First = newSd;
#endif

	SortData &sd = m_SortData.Append();
	PrefetchDC( geometry );  // stick into L2 cache for later
	sd.Model = geometry;
#if _USE_FIRST
	sd.Next = 0;
#else
	sd.Next = hd->Last;
	hd->Last = newSd;
#endif

}

void grmShaderSorter::Draw(const grmShader& shader,const grmModel& model,int materialIndex,int /*lod*/) 
{
	grcAssertf(!__TOOL, "Can't draw with the shader sorter in __TOOL builds");

	HeadData *hd = NULL;
	if (shader.m_SortKey == 0) 
	{
		{
			u32 id = shader.GetTemplateIndex();
			
			if ( !m_ShaderToHeadLUT[id] )
			{
				grcAssertf( m_HeadData.GetCount() != 0,  "No shaders to draw with" );
				Assign( m_ShaderToHeadLUT[id] ,m_HeadData.GetCount() );
				const_cast<grmShader&>(shader).m_SortKey = m_HeadData.GetCount();
				
				hd = &m_HeadData.Append();
				hd->Shader = &shader;
#if _USE_FIRST
				hd->First = 0;
#endif
				hd->Last = 0;
				hd->Next =0;
			}
			else
			{
				hd = &m_HeadData[m_ShaderToHeadLUT[id]];

				const_cast<grmShader&>(shader).m_SortKey = m_HeadData.GetCount();
				
				u16 hcnt;
				Assign( hcnt, m_HeadData.GetCount());

				HeadData *nextHd = &m_HeadData.Append();
				// insert in material order
				nextHd->Next = hd->Next;
				grcAssertf( nextHd->Next < hcnt, "Invalid index (%d) should be < %d", nextHd->Next, hcnt);

				hd->Next = hcnt;
			
				hd = nextHd;
				hd->Shader = &shader;
#if _USE_FIRST
				hd->First = 0;
#endif
				hd->Last = 0;
			}
		}
	}
	else
		hd = &m_HeadData[shader.m_SortKey];

	u16 newSd = (u16) m_SortData.GetCount();

#if _USE_FIRST
	if (hd->Last)
		m_SortData[hd->Last].Next = newSd;
	hd->Last = newSd;
	if (!hd->First)
		hd->First = newSd;
#endif

	SortData &sd = m_SortData.Append();
	
	If_Assert( dynamic_cast<const grmStaticGeometry*>( &model.GetGeometry( materialIndex ) ) )
	{
		const grmStaticGeometry* g = reinterpret_cast<const grmStaticGeometry*>( &model.GetGeometry( materialIndex ));
		sd.Model = g;
		PrefetchDC( g );  // stick into L2 cache for later
	}
#if _USE_FIRST
	sd.Next = 0;
#else
	sd.Next = hd->Last;
	hd->Last = newSd;
#endif
}

#define  PREFETCH_IBS_VBS		1


#if _USE_FIRST
#define GETSTART( a )	a.First
#else
#define GETSTART( a )	a.Last
#endif

void grmShaderSorter::Draw() 
{
	grcAssertf(!__TOOL, "Can't draw with the shader sorter in __TOOL builds");

	// For each shader:
	GRCDEVICE.ClearCachedState();
	int shaderCount = m_HeadData.GetCount();

	for (int i=1; i<shaderCount; i++)  // go downwards
	{
		int	idx = GETSTART(m_HeadData[i]);
		
		if ( !idx )
		{
			continue;
		}
		ASSERT_ONLY(int templateId = -1 );

		// Sorts on first shaders and then materials
		// Sorting on shaders minimizes the number of shader programs set
		// and sorting on materials minimizes the number of constants and texture state
		// required.

		// do all shaders
		const grmShader *shader = m_HeadData[i].Shader;
		const_cast<grmShader*>(shader)->m_SortKey = 0;			// Remove from list

		int numPasses = shader->BeginDraw(grmShader::RMC_DRAW, true); // since base shaders are the same we can do this
				
		int sidx = idx;
		
		for (int p=0; p<numPasses; p++) 
		{
			int headIdx = i;
			idx = sidx;

			while (idx) 
			{
				shader = m_HeadData[headIdx].Shader;
				const_cast<grmShader*>(shader)->m_SortKey = 0;			// Remove from list
				
				ASSERT_ONLY( templateId = templateId == -1 ? shader->GetTemplateIndex() : templateId );
				grcAssertf( shader->GetTemplateIndex() == templateId, "Template ID mismatch, shader says %d, had %d", shader->GetTemplateIndex(), templateId);		
			
				if ( idx == sidx )  // do full bind on first shader
					shader->Bind(p);
				else
					shader->IncrementalBind(p);    	// else do low cost incremental bind does not require an unbind

				// loop on same materials
				while (idx) 
				{
				
#if PREFETCH_IBS_VBS
					if ( m_SortData[idx].Next )
					{
						m_SortData[ m_SortData[idx].Next ].Model->Prefetch();
					}
#endif
#if !__WIN32PC
					m_SortData[idx].Model->grmStaticGeometry::DirectDraw();
#else
					m_SortData[idx].Model->DirectDraw();
#endif


					ASSERT_ONLY( int oIdx = idx );

					idx = m_SortData[idx].Next;
					ASSERT_ONLY( if ( p == (numPasses -1) ) Assign(m_SortData[ oIdx ].Next , 0x7FFF) ); // set to check later accidental access
				}
					
				int nidx = 0;
				// loop on same shaders
				if ( m_HeadData[ headIdx ].Next )
				{
					int hidx = m_HeadData[ headIdx ].Next;
					grcAssertf( hidx > i, "Indices were out of order. Next was %d, start was %d", hidx, i);
					grcAssertf( hidx < m_HeadData.GetCount(), "Invalid index %d should be less than %d", hidx, m_HeadData.GetCount() );
					nidx = GETSTART(m_HeadData[hidx]);
					grcAssertf( nidx < m_SortData.GetCount(), "Invalid index %d should be less than %d", nidx, m_SortData.GetCount() );
#if _USE_FIRST
					m_HeadData[ hidx ].First = 0; // don't draw later
#else
					m_HeadData[ hidx ].Last = 0; // don't draw later
#endif

					ASSERT_ONLY( Assign(m_HeadData[ headIdx ].Next , 0x7FFF) ); // set to check later accidental access
					headIdx = hidx;
				}

				
				idx = nidx;
				//shader->UnBind();	
			}
			grcAssertf( shader, "Missing shader" );
			// bind overhead is heavyweight so moved later
			shader->UnBind();	
		}
		shader->EndDraw();
	}


	Reset();
}

void grmShaderSorter::Reset()
{
	// Reset the accumulation arrays.
	m_HeadData.Resize(1);
	m_SortData.Resize(1);
	memset( m_ShaderToHeadLUT.begin(), 0, sizeof( u16) * m_ShaderToHeadLUT.GetCount());

	m_ForceShader = 0;
}
void grmShaderSorter::DrawNoState() 
{
	GRCDEVICE.ClearCachedState();
	int shaderCount = m_HeadData.GetCount();

#if PREFETCH_IBS_VBS
	 m_SortData[0].Next = 0;
#endif

	// Assume that all shaders are really the same (MC4 hack) and just bind the first one
	if (shaderCount > 1) {
		const grmShader *theShader = grmModel::GetForceShader() ? grmModel::GetForceShader() : m_HeadData[1].Shader;
									
		if (theShader->BeginDraw(grmShader::RMC_DRAW, true)) {
			theShader->Bind(0);

			for (int i=1; i<shaderCount; i++) {
				const grmShader *shader = m_HeadData[i].Shader;
				const_cast<grmShader*>(shader)->m_SortKey = 0;			// Remove from list

				int idx = GETSTART(m_HeadData[i]);
				while (idx) {

#if PREFETCH_IBS_VBS
					int next = m_SortData[idx].Next;
					if (  m_SortData[next].Next )
					{
						m_SortData[  m_SortData[next].Next].Model->Prefetch();
					}
					
#endif

#if !__WIN32PC
					m_SortData[idx].Model->grmStaticGeometry::DirectDraw();
#else
					m_SortData[idx].Model->DirectDraw();
#endif
					idx = m_SortData[idx].Next;
				}
			}
			theShader->UnBind();
			theShader->EndDraw();
		}
	}

	// Reset the accumulation arrays.
	Reset();
}

}	// namespace rage

#endif
