// 
// grmodel/shadergroupvar.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef GRMODEL_SHADERGROUPVAR_H
#define GRMODEL_SHADERGROUPVAR_H

namespace rage {

typedef enum __grmShaderGroupVar { grmsgvNONE } grmShaderGroupVar;

};	// namespace rage

#endif	// GRMODEL_SHADERGROUPVAR_H
