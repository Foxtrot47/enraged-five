// 
// grmodel/grmodelspu.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRMODEL_GRMODELSPU_H
#define GRMODEL_GRMODELSPU_H 

#if __PS3

#include "grcore/grcorespu.h"
#include "grcore/effect.h"

struct EdgeGeomPpuConfigInfo;

namespace rage {

struct spuMatrixSet
{
	u32 D3DCommon;
	u32 D3DReferenceCount;
	u32 D3DFence;
	u32 D3DReadFence;

	u8 m_MatrixCount;				// +0
	u8 m_MaxMatrixCount;
	mutable u8 m_IsSkinned;
	u8 m_Padding;

	u32 D3DBaseFlush;
	spuMatrix43 *PPUAddress;
	u32 D3DSize;

	spuMatrix43 m_Matrices[0];

#if __SPU
	// works like grmMatrixSet::ComputeMatrix():
	void ComputeMatrix(spuMatrix44 &dest, int idx)
	{
		const spuMatrix43 &src	= m_Matrices[idx];
		dest.Transpose(src);
	}
#endif //__SPU
};

#if __SPU
spuMatrixSet *spuGet_(spuMatrixSet *&ptr)
{
	// Grab the base part of the matrixset.
	spuMatrixSet *newPtr = (spuMatrixSet*) spuGetData(ptr,sizeof(spuMatrixSet));
	// Then grab the payload (due to the way we allocate, it's guaranteed to still be contiguous)
	spuGetData(ptr+1,newPtr->m_MatrixCount * sizeof(spuMatrix43));
	// And update the base pointer.
	ptr = newPtr;
	return ptr;
}
#endif


struct spuGeometry
{
	u32 vptr;
	enum { GEOMETRYQB, GEOMETRYEDGE };
	spuVertexDeclaration*						m_VtxDecl;
	int											m_Type;			// really only needs to be a u8.

	static void Place(spuGeometry *that,datResource &rsc);
};

#define MAX_VERT_BUFFERS	(4)
#define	USE_EXTRA_VERTS		1

struct spuGeometryQB: public spuGeometry
{
	spuGeometryQB(datResource &rsc) {
		rsc.PointerFixup(m_MtxPalette);
	}
	IMPLEMENT_PLACE_INLINE(spuGeometryQB)

	atRangeArray<spuVertexBuffer*, MAX_VERT_BUFFERS>	m_VB;	// +12
	atRangeArray<spuIndexBuffer*,  MAX_VERT_BUFFERS>	m_IB;	// +28
	u32											m_IndexCount;	// +44
	u32											m_PrimCount;	// +48
	u16											m_VertexCount;	// +52
	u8											m_PrimType;
	u8											m_DoubleBuffered;
	u16*										m_MtxPalette;		// +56 data/struct.h requires that the array goes before the count
#if USE_EXTRA_VERTS
	u8											m_Stride;			// +60
	u8											m_ExtraVerts;		// +61
#else
	u16											m_Stride;			// +60
#endif
	u16											m_MtxCount;			// +62
	u32											m_VertexData;		// +64 - copied from m_VB[0] (for PS3) (must be at multiple of 16)
	spuVertexDeclaration*						m_VtxDeclOffset;	// +68
	spuVertexBuffer*							m_OffsetBuffer;		// +72
	u32											m_IndexOffset;		// +76 - copied from m_IB[0] (for PS3)

	void Draw();
	void DrawSkinned(spuMatrixSet&ms);
};

struct spuGeometryEdge: public spuGeometry
{
	spuGeometryEdge(datResource &rsc) {
		rsc.PointerFixup(m_Data);
	}
	IMPLEMENT_PLACE_INLINE(spuGeometryEdge)
	EdgeGeomPpuConfigInfo *m_Data;
	__vector4 m_Offset;
	int m_Count;

#if HACK_GTA4
	void Draw(bool flushVC=false);
	void DrawSkinned(spuMatrixSet&ms, bool flushVC=false);
#else
	void Draw();
	void DrawSkinned(spuMatrixSet&ms);
#endif
};

enum { RMC_DRAW, RMC_DRAWSKINNED, RMC_DRAWCOUNT };

struct spuTechniqueGroup
{
	u8 Groups[RMC_DRAWCOUNT]; 
};

struct spuModel;

struct spuShader
{
	explicit spuShader(datResource &rsc SPU_ONLY(,unsigned techniqueGroupId,unsigned drawTypeMask))
		: m_EffectInstance(rsc SPU_ONLY(,techniqueGroupId,drawTypeMask)) {}
#if __SPU
	IMPLEMENT_PLACE_INLINE_2(spuShader,unsigned,unsigned)
#else
	IMPLEMENT_PLACE_INLINE(spuShader)
#endif

	grcInstanceData m_EffectInstance;

	enum { FLAG_USES_PRESETS = BIT0,
		FLAG_FLASH_INSTANCES = BIT1,	// For debugging - flash this shader if this set
		FLAG_DRAW_WIREFRAME = BIT2,		// For debugging - draw objects using this shader in wireframe mode
		FLAG_DISABLE_INSTANCES = BIT3,	// For debugging - do not render this shader if this is set
		FLAG_INHERITS_DRAWBUCKET = BIT4,
	};

	int BeginDraw(int drawType,bool restoreState);
	void Bind(int pass);
	void UnBind();
	void EndDraw();
#if __BANK && __SPU
	bool IsFlashing(bool fe) const		{ return fe && (m_EffectInstance.UserFlags & FLAG_FLASH_INSTANCES); }
	bool IsDisabled() const				{ return (m_EffectInstance.UserFlags & FLAG_DISABLE_INSTANCES) != 0; }
#endif
};

struct spuShaderGroup
{
	explicit spuShaderGroup(datResource &rsc SPU_ONLY(,unsigned techniqueGroupId,unsigned drawTypeMask))
		: m_Shaders(GRCARRAY_PLACEELEMENTS,rsc SPU_ONLY(,techniqueGroupId,drawTypeMask)) {}
#if __SPU
	IMPLEMENT_PLACE_INLINE_2(spuShaderGroup,unsigned,unsigned)
#else
	IMPLEMENT_PLACE_INLINE(spuShaderGroup)
#endif

	u32 vptr;
	void *m_TextureDictionary;
	grcArray< datOwner<spuShader> > m_Shaders;
	atArray< void* > m_ShaderGroupVars_NotUsed;			
	u16 m_ContainerSizeQW, pad0, pad1, pad2;
};

struct spuModel
{
	explicit spuModel(datResource &rsc) : m_Geometries(GRCARRAY_PLACEELEMENTS,rsc) {
		rsc.PointerFixup(m_ShaderIndex);
		rsc.PointerFixup(m_AABBs);
	}
	IMPLEMENT_PLACE_INLINE(spuModel);
	u32 vptr;
	grcArray< datOwner<spuGeometry> > m_Geometries;
	__vector4* m_AABBs; // these are really spdAABB's, two vector4's per item.  1 item if one geometry, or 1+N if more than N (first is bbox for all)
	u16 *m_ShaderIndex;	// data/struct.h requires that for dynamic arrays, the pointer comes before its count in the structure.
	u8 m_MatrixCount, m_Flags, m_Type, m_MatrixIndex, m_Mask, m_SkinFlag;
	u16 m_Count;

	void Draw(spuShaderGroup &group,u32 bucketMask,int lod,u16 stats);
	void DrawSkinned(spuShaderGroup &group,spuMatrixSet &ms,u32 bucketMask,int lod,u16 stats) ;
};

CompileTimeAssert(sizeof(spuGeometryQB) >= sizeof(spuGeometryEdge));

struct spuCmd_grmGeometryEdge__Draw: public spuCmd_Any {
	spuGeometryEdge *geometry;
#if HACK_GTA4_MODELINFOIDX_ON_SPU
	CGta4DbgSpuInfoStruct	gta4DebugInfo;
#endif
};

struct spuCmd_grmGeometryEdge__DrawSkinned: public spuCmd_Any {	// Subcommand contains matrixset count to avoid extra fetch
	spuGeometryEdge *geometry;
#if __SPU
	spuMatrixSet *ms;
#else
	grmMatrixSet *ms;
#endif
#if HACK_GTA4_MODELINFOIDX_ON_SPU
	CGta4DbgSpuInfoStruct	gta4DebugInfo;
#endif
};

struct spuCmd_grmModel__SetForceShader: public spuCmd_Any {
	void *forceShader;
};

struct spuEffectVarPair { u8 index, var; };

struct spuCmd_grmShaderGroup__SetVarCommonBase: public spuCmd_Any {
	grcInstanceData::Entry **datas;
	spuEffectVarPair *vars;
	u8 stride, arrayCount, varCount, shaderCount;
};

CompileTimeAssert(sizeof(spuCmd_grmShaderGroup__SetVarCommonBase) == 16);

struct spuCmd_grmShaderGroup__SetVarCommon: public spuCmd_grmShaderGroup__SetVarCommonBase {
	u32 alignedPayload[0];
};

struct spuCmd_grmShaderGroup__SetVarCommonByRef: public spuCmd_grmShaderGroup__SetVarCommonBase {
	void *src;
};

enum spuCommands_grmodel {
	grmShaderFx__SetForcedTechniqueGroupId = GRCORE_COMMAND_COUNT,
	grmModel__SetForceShader,

	grmGeometryEdge__Draw,
	grmGeometryEdge__DrawSkinned,
	grmGeometryEdge__SetBlendHeaders,

	grmShaderGroup__SetVarCommon,
	grmShaderGroup__SetVarCommonByRef,

	GRMODEL_COMMAND_COUNT
};

} // namespace rage

#endif	// __PS3

#endif // GRCORE_GRCORESPU_H 
