// 
// grmodel/shadervar.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "shadervar.h"

#if __TOOL
#include "grcore/channel.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#endif

namespace rage {

#if EFFECT_PRESERVE_STRINGS

grmShaderVar::grmShaderVar(grcInstanceData &data,grcEffectVar var) : Data(&data), Handle(var) {
	Effect = &Data->GetBasis();
	int annoCount;
	bool isGlobal;
	Effect->GetVarDesc(Handle,Name,Type,annoCount,isGlobal);
}

#endif


#if __TOOL
void grmShaderVar::Load(const char *src) {
	float f[4];
	switch (Type) {
		case grcEffect::VT_FLOAT: sscanf(src,"%f",&f[0]); Effect->SetVar(*Data,Handle,f[0]); break;
		case grcEffect::VT_VECTOR2: sscanf(src,"%f %f",&f[0],&f[1]); Effect->SetVar(*Data,Handle,Vector2(f[0],f[1])); break;
		case grcEffect::VT_VECTOR3: sscanf(src,"%f %f %f",&f[0],&f[1],&f[2]); Effect->SetVar(*Data,Handle,Vector3(f[0],f[1],f[2])); break;
		case grcEffect::VT_VECTOR4: sscanf(src,"%f %f %f %f",&f[0],&f[1],&f[2],&f[3]); Effect->SetVar(*Data,Handle,Vector4(f[0],f[1],f[2],f[3])); break;
		default: grcAssertf(false,"Unhandled type %s",grcEffect::GetTypeName(Type)); break;
	}
}


void grmShaderVar::Save(char *dest,int destSize) {
	float f;
	Vector2 v2;
	Vector3 v3;
	Vector4 v4;
	switch (Type) {
		case grcEffect::VT_FLOAT: Effect->GetVar(*Data,Handle,f); formatf(dest,destSize,"%.8g",f); break;
		case grcEffect::VT_VECTOR2: Effect->GetVar(*Data,Handle,v2); formatf(dest,destSize,"%.8g %.8g",v2.x,v2.y); break;
		case grcEffect::VT_VECTOR3: Effect->GetVar(*Data,Handle,v3); formatf(dest,destSize,"%.8g %.8g %.8g",v3.x,v3.y,v3.z); break;
		case grcEffect::VT_VECTOR4: Effect->GetVar(*Data,Handle,v4); formatf(dest,destSize,"%.8g %.8g %.8g %.8g",v4.x,v4.y,v4.z,v4.w); break;
		default: grcAssertf(false,"Unhandled type %s",grcEffect::GetTypeName(Type)); break;
	}
}
#endif	// __TOOL

} // namespace rage
