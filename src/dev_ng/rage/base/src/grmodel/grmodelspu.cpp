#if __SPU

#include "shaderlib/rage_constants.h"		// for SKINNING_BASE
#include "atl/simplecache.h"
#include "grcore/channel.h"
#include "grcore/edgegeomvtx.h"
#include "grcore/fvfchannels.h"

#include "vectormath/classes.h"

#include <cell/gcm/gcm_method_data.h>


#define ROUNDUP(x)	(((x)+15)&~15)
spuShader *sm_ForceShader;
char ForceShaderBuffer[ROUNDUP(sizeof(spuShader))] ;
spuTechniqueGroup ForceShaderTechniqueGroup[64] ;

static void UpdateForceShader()
{
	if (pSpuGcmState->forceShader) {
		sm_ForceShader = (spuShader*) &ForceShaderBuffer;
		sysDmaGetAndWait(&ForceShaderBuffer, (uint64_t) pSpuGcmState->forceShader, ROUNDUP(sizeof(spuShader)), spuGetTag);
		sm_ForceShader->m_EffectInstance.SpuGet();
	}
	else
		sm_ForceShader = NULL;
}

void spuGeometry::Place(spuGeometry *that,datResource &rsc) 
{
	if (that->m_Type == spuGeometry::GEOMETRYQB)
		spuGeometryQB::Place((spuGeometryQB*)that,rsc);
	else
		spuGeometryEdge::Place((spuGeometryEdge*)that,rsc);
}

atSimpleCache4<spuVertexDeclaration> s_vtxDeclCache;

void spuGeometryQB::Draw()
{
	DEV_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_SPUGEOMETRYQB_DRAW) != 0)
	{
		return;
	})

	grcAssertf(m_Type == GEOMETRYQB, "Type mismatch - type is %d", m_Type);
	spuVertexDeclaration *vd = s_vtxDeclCache.Get(m_VtxDeclOffset ? m_VtxDeclOffset : m_VtxDecl);

	u32 offsets[]  = { m_VertexData, (u32)m_OffsetBuffer, 0, 0 };
	DrawIndexedPrimitive(vd,CELL_GCM_PRIMITIVE_TRIANGLES,offsets,m_IndexOffset,m_IndexCount);
}


static void GetEdgeVertexDataArrayFormatsFromStreamDesc(u32 *formats, const EdgeGeomVertexStreamDescription *desc)
{
	const unsigned stride = desc->stride;
	const unsigned numAttributes = desc->numAttributes;
	for (unsigned i=0; i<numAttributes; ++i)
	{
		const EdgeGeomAttributeBlock& attr = desc->blocks[i].attributeBlock;
		formats[attr.vertexProgramSlotIndex] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, stride, attr.componentCount, attr.format);
	}
}


static bool GetEdgeVertexDataArrayFormats(u32 *formats, const EdgeGeomPpuConfigInfo *ppuConf, u16 vertexInputs)
{
	Assert(((uptr)formats&15) == 0);

	// Initialize all the vertex formats to disabled
	CompileTimeAssert(DISABLED_VERTEX_FORMAT <= 0x3ffff);
	qword disabled = si_ila(DISABLED_VERTEX_FORMAT);
	si_stqd(disabled, si_from_ptr(formats), 0);
	si_stqd(disabled, si_from_ptr(formats), 16);
	si_stqd(disabled, si_from_ptr(formats), 32);
	si_stqd(disabled, si_from_ptr(formats), 48);

	// Fetch the spu output stream descriptor if we are going to need it
	const EdgeGeomVertexStreamDescription *spuOutputStreamDesc = NULL;
	if (ppuConf->spuConfigInfo.outputVertexFormatId == 0xff && ppuConf->spuOutputStreamDesc)
	{
		spuOutputStreamDesc = (EdgeGeomVertexStreamDescription*)spuGetData(ppuConf->spuOutputStreamDesc, ppuConf->spuOutputStreamDescSize);
	}

	// SPU generated vertex arrays
	const u8 outputVertexFormatId = GetEdgeOutputVertexFormatId(&ppuConf->spuConfigInfo, spuOutputStreamDesc, vertexInputs);
	switch (outputVertexFormatId)
	{
	case EDGE_GEOM_RSX_VERTEX_FORMAT_F32c3:
		formats[0]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 12, 3, CELL_GCM_VERTEX_F);
		break;
	case EDGE_GEOM_RSX_VERTEX_FORMAT_F32c3_X11Y11Z10N_X11Y11Z10N:
		formats[0]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 20, 3, CELL_GCM_VERTEX_F);
		formats[2]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 20, 1, CELL_GCM_VERTEX_CMP);
		formats[14] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 20, 1, CELL_GCM_VERTEX_CMP);
		break;
	case EDGE_GEOM_RSX_VERTEX_FORMAT_F32c3_X11Y11Z10N_I16Nc4:
		formats[0]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 3, CELL_GCM_VERTEX_F);
		formats[2]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 1, CELL_GCM_VERTEX_CMP);
		formats[14] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 4, CELL_GCM_VERTEX_S1);
		break;
	case EDGE_GEOM_RSX_VERTEX_FORMAT_F32c3_X11Y11Z10N_X11Y11Z10N_X11Y11Z10N:
		formats[0]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 3, CELL_GCM_VERTEX_F);
		formats[2]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 1, CELL_GCM_VERTEX_CMP);
		formats[14] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 1, CELL_GCM_VERTEX_CMP);
		formats[15] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 1, CELL_GCM_VERTEX_CMP);
		break;
	case EDGE_GEOM_RSX_VERTEX_FORMAT_F32c4:
		formats[0]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 16, 4, CELL_GCM_VERTEX_F);
		break;
	case EDGE_GEOM_RSX_VERTEX_FORMAT_F32c4_X11Y11Z10N_X11Y11Z10N:
		formats[0]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 4, CELL_GCM_VERTEX_F);
		formats[2]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 1, CELL_GCM_VERTEX_CMP);
		formats[14] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 24, 1, CELL_GCM_VERTEX_CMP);
		break;
	case EDGE_GEOM_RSX_VERTEX_FORMAT_F32c4_X11Y11Z10N_I16Nc4:
		formats[0]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 28, 4, CELL_GCM_VERTEX_F);
		formats[2]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 28, 1, CELL_GCM_VERTEX_CMP);
		formats[14] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 28, 4, CELL_GCM_VERTEX_S1);
		break;
	case EDGE_GEOM_RSX_VERTEX_FORMAT_F32c4_X11Y11Z10N_X11Y11Z10N_X11Y11Z10N:
		formats[0]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 28, 4, CELL_GCM_VERTEX_F);
		formats[2]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 28, 1, CELL_GCM_VERTEX_CMP);
		formats[14] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 28, 1, CELL_GCM_VERTEX_CMP);
		formats[15] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 28, 1, CELL_GCM_VERTEX_CMP);
		break;
	case EDGE_GEOM_RSX_VERTEX_FORMAT_EMPTY:
		return false;
	default:
		Assert(spuOutputStreamDesc);
		GetEdgeVertexDataArrayFormatsFromStreamDesc(formats, spuOutputStreamDesc);
		break;
	}

	// RSX only vertex arrays
	if (Likely(ppuConf->rsxOnlyStreamDesc))
	{
		const EdgeGeomVertexStreamDescription *desc
			= (EdgeGeomVertexStreamDescription*)spuGetData(ppuConf->rsxOnlyStreamDesc, ppuConf->rsxOnlyStreamDescSize);
		GetEdgeVertexDataArrayFormatsFromStreamDesc(formats, desc);
	}

	// Disable any vertex arrays not actually used by the vertex shader
	qword format0123 = si_lqd(si_from_ptr(formats), 0);
	qword format4567 = si_lqd(si_from_ptr(formats), 16);
	qword format89AB = si_lqd(si_from_ptr(formats), 32);
	qword formatCDEF = si_lqd(si_from_ptr(formats), 48);
	qword vertexInputsQ = si_from_ushort(vertexInputs);
	qword mask0 = si_fsm(vertexInputsQ);
	qword mask1 = si_fsm(si_rotmai(vertexInputsQ,-4));
	qword mask2 = si_fsm(si_rotmai(vertexInputsQ,-8));
	qword mask3 = si_fsm(si_rotmai(vertexInputsQ,-12));
	qword shufDCBA = {0x0c,0x0d,0x0e,0x0f, 0x08,0x09,0x0a,0x0b, 0x04,0x05,0x06,0x07, 0x00,0x01,0x02,0x03};
	mask0 = si_shufb(mask0, mask0, shufDCBA);
	mask1 = si_shufb(mask1, mask1, shufDCBA);
	mask2 = si_shufb(mask2, mask2, shufDCBA);
	mask3 = si_shufb(mask3, mask3, shufDCBA);
	format0123 = si_selb(disabled, format0123, mask0);
	format4567 = si_selb(disabled, format4567, mask1);
	format89AB = si_selb(disabled, format89AB, mask2);
	formatCDEF = si_selb(disabled, formatCDEF, mask3);
	si_stqd(format0123, si_from_ptr(formats), 0);
	si_stqd(format4567, si_from_ptr(formats), 16);
	si_stqd(format89AB, si_from_ptr(formats), 32);
	si_stqd(formatCDEF, si_from_ptr(formats), 48);

	return true;
}


static bool SetEdgeVertexDataArrayFormats(CellGcmContextData *ctx, const EdgeGeomPpuConfigInfo *ppuConf, u16 vertexInputs)
{
	// Determine the vertex array formats for the Edge geometry
	u32 formats[16] ;
	if (Unlikely(!GetEdgeVertexDataArrayFormats(formats, ppuConf, vertexInputs)))
	{
		return false;
	}

	// Generate the RSX command buffer for the vertex data array formats that
	// have actually changed.  Reserve some additional space for setting the
	// vertex colors if necissary.
	const u32 colorAdditionalCmdBufWords = 4;
	GenerateVertexFormatCmdBuf(ctx, pSpuGcmState, formats, colorAdditionalCmdBufWords);

	// Set the default vertex colors if the arrays are used by the vertex shader, but not present in the Edge geometry.
	u32 *cmd = ctx->current;
	enum { COLOR0 = 3 };
	if ((vertexInputs & (1<<COLOR0)) && formats[COLOR0] == DISABLED_VERTEX_FORMAT)
	{
		cmd[0] = CELL_GCM_METHOD(CELL_GCM_NV4097_SET_VERTEX_DATA4UB_M + CELL_GCM_COMMAND_CAST(COLOR0) * 4, 1);
		cmd[1] = 0xffffffff;
		cmd += 2;
	}
	enum { COLOR1 = 4 };
	if ((vertexInputs & (1<<COLOR1)) && formats[COLOR1] == DISABLED_VERTEX_FORMAT)
	{
		cmd[0] = CELL_GCM_METHOD(CELL_GCM_NV4097_SET_VERTEX_DATA4UB_M + CELL_GCM_COMMAND_CAST(COLOR1) * 4, 1);
		cmd[1] = 0xffffffff;
		cmd += 2;
	}
	ctx->current = cmd;
	Assert(ctx->current <= ctx->end);

	return true;
}


void spuGeometryEdge::Draw(
#if HACK_GTA4
						   bool bFlushVC
#endif
						   )
{
	grcStateBlock::Flush();
	char *scratchSave = spuScratch;
	EdgeGeomPpuConfigInfo* data = (EdgeGeomPpuConfigInfo*) spuGetData(m_Data, m_Count * sizeof(EdgeGeomPpuConfigInfo));
	if (Unlikely(!SetEdgeVertexDataArrayFormats(GCM_CONTEXT, data, g_VertexShaderInputs)))
	{
		return;
	}
	for (int i=0; i<m_Count; i++) {
#if HACK_GTA4
		if(bFlushVC)
		{
			GCM_DEBUG(GCM::cellGcmSetInvalidateVertexCache(GCM_CONTEXT));
		}
#endif

		// Add the job
		GEOMETRY_JOBS.AddJob(GCM_CONTEXT, &data[i], NULL,
			m_Offset, g_VertexShaderInputs, *pSpuGcmState);
	}
	spuScratch = scratchSave;
}

void spuGeometryQB::DrawSkinned(spuMatrixSet &ms)
{
	grcAssertf(m_Type == GEOMETRYQB, "Type mismatch - type is %d", m_Type);
	// spuGet skinning matrix bindings
	if (m_MtxCount)
	{
		// retrieve matrices from ppu
#if DEBUG_GET
		spuGetName = "render matrices";
#endif
		ValidateSpuPtr(m_MtxPalette);
		int skinBase = SKINNING_BASE;
		for (int i=0; i<m_MtxCount; i++,skinBase+=3)
		{
			// Send each skinning mtx across to GPU
			spuMatrix43 *thisMtx = ms.m_Matrices + m_MtxPalette[i];
			cellGcmSetVertexProgramParameterBlock(&ctxt,skinBase,3,(float*)thisMtx);
		}
	}
	// Fall through to shared code.
	Draw();
}

void spuGeometryEdge::DrawSkinned(spuMatrixSet &ms	// matrixset is actually still on PPU side!
#if HACK_GTA4
								  ,bool bFlushVC
#endif
								  )
{
	grcStateBlock::Flush();
	char *scratchSave = spuScratch;
	/// ValidateSpuPtr(m_Data);
	EdgeGeomPpuConfigInfo* data = (EdgeGeomPpuConfigInfo*) spuGetData(m_Data, m_Count * sizeof(EdgeGeomPpuConfigInfo));
	if (Unlikely(!SetEdgeVertexDataArrayFormats(GCM_CONTEXT, data, g_VertexShaderInputs)))
	{
		return;
	}
	for (int i=0; i<m_Count; i++) {
#if HACK_GTA4
		if(bFlushVC)
		{
			GCM_DEBUG(GCM::cellGcmSetInvalidateVertexCache(GCM_CONTEXT));
		}
#endif

		// Add the job
		GEOMETRY_JOBS.AddJob(GCM_CONTEXT, &data[i], 
#if SPU_GCM_FIFO > 2
			ms.PPUAddress,				// matrixset is local, but this has original EA of matrices
#else
			ms.m_Matrices,				// matrixset is still on ppu, so this resolves to correct EA
#endif
			m_Offset,
			g_VertexShaderInputs,
			*pSpuGcmState);
	}
	spuScratch = scratchSave;
}

static grcEffect *s_CurEffect;

int spuShader::BeginDraw(int drawType,bool restoreState)
{
	int groupId;
	if (pSpuGcmState->ForcedTechniqueGroupId != -1)
		groupId = pSpuGcmState->ForcedTechniqueGroupId;
	else
		groupId = pSpuGcmState->LightingMode;

	// Fetch the basis effect and some externally allocated instance data.
	s_CurEffect = s_effectCache->Get(m_EffectInstance.Basis);

	Assert((u32)m_EffectInstance.Entries < 256*1024);

	// grcDisplayf("BeginDraw(%d) - %d, %d passes",drawType,m_TechniqueGroups[groupId].Groups[drawType],passes);
	int passCount = s_CurEffect->BeginDraw(s_CurEffect->GetDrawTechnique(groupId,drawType),restoreState);
#if __DEV
	// Only push the marker if we're going to potentially do work
	if (passCount)
		cellGcmSetPerfMonPushMarker(GCM_CONTEXT,s_CurEffect->m_EffectName);
#endif

	DRAWABLESPU_STATS_ADD(MissingTechnique,!passCount);

	return passCount;
}

void spuShader::Bind(int pass)
{
	// Basis is already set up by BeginDraw
	grcAssertf(s_CurEffect, "Missing effect");
	s_CurEffect->BeginPass(pass,m_EffectInstance);
}

void spuShader::UnBind()
{
	grcAssertf(s_CurEffect, "Missing effect");
	// Basis is already set up by BeginDraw
	s_CurEffect->EndPass();
}

void spuShader::EndDraw()
{
#if __DEV
	// Only pop the marker if we might have rendered something
	if (grcEffect::GetCurrentTechnique())
		cellGcmSetPerfMonPopMarker(GCM_CONTEXT);
#endif

	// Basis is already set up by BeginDraw
	grcAssertf(s_CurEffect, "Missing effect");
	s_CurEffect->EndDraw();
	s_CurEffect = NULL;
}

inline u32 IsAABBVisibleInline_4(Vec3V_In boxMin, Vec3V_In boxMax, const Vec4V transposedPlanes[4])
{
	const Vec4V xxxx_min = Vec4V(SplatX(boxMin));
	const Vec4V yyyy_min = Vec4V(SplatY(boxMin));
	const Vec4V zzzz_min = Vec4V(SplatZ(boxMin));
	const Vec4V xxxx_max = Vec4V(SplatX(boxMax));
	const Vec4V yyyy_max = Vec4V(SplatY(boxMax));
	const Vec4V zzzz_max = Vec4V(SplatZ(boxMax));

	const Vec4V zero(V_ZERO);

	const Vec4V abcd_xxxx = transposedPlanes[0];
	const Vec4V abcd_yyyy = transposedPlanes[1];
	const Vec4V abcd_zzzz = transposedPlanes[2];
	const Vec4V abcd_wwww = transposedPlanes[3];

	const VecBoolV abcd_xxxx_lt_0 = IsLessThan(abcd_xxxx, zero);
	const VecBoolV abcd_yyyy_lt_0 = IsLessThan(abcd_yyyy, zero);
	const VecBoolV abcd_zzzz_lt_0 = IsLessThan(abcd_zzzz, zero);

	const Vec4V abcd_xxxx_p = SelectFT(abcd_xxxx_lt_0, xxxx_max, xxxx_min);
	const Vec4V abcd_yyyy_p = SelectFT(abcd_yyyy_lt_0, yyyy_max, yyyy_min);
	const Vec4V abcd_zzzz_p = SelectFT(abcd_zzzz_lt_0, zzzz_max, zzzz_min);
	const Vec4V abcd_dddd_p = AddScaled(AddScaled(AddScaled(abcd_wwww, abcd_zzzz_p, abcd_zzzz), abcd_yyyy_p, abcd_yyyy), abcd_xxxx_p, abcd_xxxx);

	const u32 visible = IsGreaterThanOrEqualAll(abcd_dddd_p, zero);

	return visible;
}

inline u32 IsAABBVisibleInline_4(Vec3V_In boxMin, Vec3V_In boxMax, const Vec4V transposedPlanes[4], u32* trivialAccept)
{
	const Vec4V xxxx_min = Vec4V(SplatX(boxMin));
	const Vec4V yyyy_min = Vec4V(SplatY(boxMin));
	const Vec4V zzzz_min = Vec4V(SplatZ(boxMin));
	const Vec4V xxxx_max = Vec4V(SplatX(boxMax));
	const Vec4V yyyy_max = Vec4V(SplatY(boxMax));
	const Vec4V zzzz_max = Vec4V(SplatZ(boxMax));

	const Vec4V zero(V_ZERO);

	const Vec4V abcd_xxxx = transposedPlanes[0];
	const Vec4V abcd_yyyy = transposedPlanes[1];
	const Vec4V abcd_zzzz = transposedPlanes[2];
	const Vec4V abcd_wwww = transposedPlanes[3];

	const VecBoolV abcd_xxxx_lt_0 = IsLessThan(abcd_xxxx, zero);
	const VecBoolV abcd_yyyy_lt_0 = IsLessThan(abcd_yyyy, zero);
	const VecBoolV abcd_zzzz_lt_0 = IsLessThan(abcd_zzzz, zero);

	const Vec4V abcd_xxxx_p = SelectFT(abcd_xxxx_lt_0, xxxx_max, xxxx_min);
	const Vec4V abcd_yyyy_p = SelectFT(abcd_yyyy_lt_0, yyyy_max, yyyy_min);
	const Vec4V abcd_zzzz_p = SelectFT(abcd_zzzz_lt_0, zzzz_max, zzzz_min);
	const Vec4V abcd_dddd_p = AddScaled(AddScaled(AddScaled(abcd_wwww, abcd_zzzz_p, abcd_zzzz), abcd_yyyy_p, abcd_yyyy), abcd_xxxx_p, abcd_xxxx);

	const u32 visible = IsGreaterThanOrEqualAll(abcd_dddd_p, zero);

	if (visible)
	{
		const Vec4V abcd_xxxx_n = SelectFT(abcd_xxxx_lt_0, xxxx_min, xxxx_max);
		const Vec4V abcd_yyyy_n = SelectFT(abcd_yyyy_lt_0, yyyy_min, yyyy_max);
		const Vec4V abcd_zzzz_n = SelectFT(abcd_zzzz_lt_0, zzzz_min, zzzz_max);
		const Vec4V abcd_dddd_n = AddScaled(AddScaled(AddScaled(abcd_wwww, abcd_zzzz_n, abcd_zzzz), abcd_yyyy_n, abcd_yyyy), abcd_xxxx_n, abcd_xxxx);

		*trivialAccept = IsGreaterThanOrEqualAll(abcd_dddd_n, zero);
	}

	return visible;
}

inline u32 IsAABBVisibleInline_Extended(Vec3V_In boxMin, Vec3V_In boxMax, const Vec4V transposedPlanes[EDGE_NUM_MODEL_CLIP_PLANES], u32* trivialAccept)
{
	const Vec4V xxxx_min = Vec4V(SplatX(boxMin));
	const Vec4V yyyy_min = Vec4V(SplatY(boxMin));
	const Vec4V zzzz_min = Vec4V(SplatZ(boxMin));
	const Vec4V xxxx_max = Vec4V(SplatX(boxMax));
	const Vec4V yyyy_max = Vec4V(SplatY(boxMax));
	const Vec4V zzzz_max = Vec4V(SplatZ(boxMax));

	const Vec4V zero(V_ZERO);

	const Vec4V abcd_xxxx = transposedPlanes[0];
	const Vec4V abcd_yyyy = transposedPlanes[1];
	const Vec4V abcd_zzzz = transposedPlanes[2];
	const Vec4V abcd_wwww = transposedPlanes[3];
	const Vec4V efgh_xxxx = transposedPlanes[4];
	const Vec4V efgh_yyyy = transposedPlanes[5];
	const Vec4V efgh_zzzz = transposedPlanes[6];
	const Vec4V efgh_wwww = transposedPlanes[7];
#if EDGE_NUM_MODEL_CLIP_PLANES > 8
	const Vec4V ijkl_xxxx = transposedPlanes[8];
	const Vec4V ijkl_yyyy = transposedPlanes[9];
	const Vec4V ijkl_zzzz = transposedPlanes[10];
	const Vec4V ijkl_wwww = transposedPlanes[11];
#endif // EDGE_NUM_MODEL_CLIP_PLANES > 8

	const VecBoolV abcd_xxxx_lt_0 = IsLessThan(abcd_xxxx, zero);
	const VecBoolV abcd_yyyy_lt_0 = IsLessThan(abcd_yyyy, zero);
	const VecBoolV abcd_zzzz_lt_0 = IsLessThan(abcd_zzzz, zero);
	const VecBoolV efgh_xxxx_lt_0 = IsLessThan(efgh_xxxx, zero);
	const VecBoolV efgh_yyyy_lt_0 = IsLessThan(efgh_yyyy, zero);
	const VecBoolV efgh_zzzz_lt_0 = IsLessThan(efgh_zzzz, zero);
#if EDGE_NUM_MODEL_CLIP_PLANES > 8
	const VecBoolV ijkl_xxxx_lt_0 = IsLessThan(ijkl_xxxx, zero);
	const VecBoolV ijkl_yyyy_lt_0 = IsLessThan(ijkl_yyyy, zero);
	const VecBoolV ijkl_zzzz_lt_0 = IsLessThan(ijkl_zzzz, zero);
#endif // EDGE_NUM_MODEL_CLIP_PLANES > 8

	const Vec4V abcd_xxxx_p = SelectFT(abcd_xxxx_lt_0, xxxx_max, xxxx_min);
	const Vec4V abcd_yyyy_p = SelectFT(abcd_yyyy_lt_0, yyyy_max, yyyy_min);
	const Vec4V abcd_zzzz_p = SelectFT(abcd_zzzz_lt_0, zzzz_max, zzzz_min);
	const Vec4V abcd_dddd_p = AddScaled(AddScaled(AddScaled(abcd_wwww, abcd_zzzz_p, abcd_zzzz), abcd_yyyy_p, abcd_yyyy), abcd_xxxx_p, abcd_xxxx);
	const Vec4V efgh_xxxx_p = SelectFT(efgh_xxxx_lt_0, xxxx_max, xxxx_min);
	const Vec4V efgh_yyyy_p = SelectFT(efgh_yyyy_lt_0, yyyy_max, yyyy_min);
	const Vec4V efgh_zzzz_p = SelectFT(efgh_zzzz_lt_0, zzzz_max, zzzz_min);
	const Vec4V efgh_dddd_p = AddScaled(AddScaled(AddScaled(efgh_wwww, efgh_zzzz_p, efgh_zzzz), efgh_yyyy_p, efgh_yyyy), efgh_xxxx_p, efgh_xxxx);
#if EDGE_NUM_MODEL_CLIP_PLANES > 8
	const Vec4V ijkl_xxxx_p = SelectFT(ijkl_xxxx_lt_0, xxxx_max, xxxx_min);
	const Vec4V ijkl_yyyy_p = SelectFT(ijkl_yyyy_lt_0, yyyy_max, yyyy_min);
	const Vec4V ijkl_zzzz_p = SelectFT(ijkl_zzzz_lt_0, zzzz_max, zzzz_min);
	const Vec4V ijkl_dddd_p = AddScaled(AddScaled(AddScaled(ijkl_wwww, ijkl_zzzz_p, ijkl_zzzz), ijkl_yyyy_p, ijkl_yyyy), ijkl_xxxx_p, ijkl_xxxx);
#endif // EDGE_NUM_MODEL_CLIP_PLANES > 8

	Vec4V dddd_p = Min(abcd_dddd_p, efgh_dddd_p);
#if EDGE_NUM_MODEL_CLIP_PLANES > 8
	dddd_p = Min(dddd_p, ijkl_dddd_p);
#endif // EDGE_NUM_MODEL_CLIP_PLANES > 8

	const u32 visible = IsGreaterThanOrEqualAll(dddd_p, zero);

	if (visible)
	{
		const Vec4V abcd_xxxx_n = SelectFT(abcd_xxxx_lt_0, xxxx_min, xxxx_max);
		const Vec4V abcd_yyyy_n = SelectFT(abcd_yyyy_lt_0, yyyy_min, yyyy_max);
		const Vec4V abcd_zzzz_n = SelectFT(abcd_zzzz_lt_0, zzzz_min, zzzz_max);
		const Vec4V abcd_dddd_n = AddScaled(AddScaled(AddScaled(abcd_wwww, abcd_zzzz_n, abcd_zzzz), abcd_yyyy_n, abcd_yyyy), abcd_xxxx_n, abcd_xxxx);
		const Vec4V efgh_xxxx_n = SelectFT(efgh_xxxx_lt_0, xxxx_min, xxxx_max);
		const Vec4V efgh_yyyy_n = SelectFT(efgh_yyyy_lt_0, yyyy_min, yyyy_max);
		const Vec4V efgh_zzzz_n = SelectFT(efgh_zzzz_lt_0, zzzz_min, zzzz_max);
		const Vec4V efgh_dddd_n = AddScaled(AddScaled(AddScaled(efgh_wwww, efgh_zzzz_n, efgh_zzzz), efgh_yyyy_n, efgh_yyyy), efgh_xxxx_n, efgh_xxxx);
#if EDGE_NUM_MODEL_CLIP_PLANES > 8
		const Vec4V ijkl_xxxx_n = SelectFT(ijkl_xxxx_lt_0, xxxx_min, xxxx_max);
		const Vec4V ijkl_yyyy_n = SelectFT(ijkl_yyyy_lt_0, yyyy_min, yyyy_max);
		const Vec4V ijkl_zzzz_n = SelectFT(ijkl_zzzz_lt_0, zzzz_min, zzzz_max);
		const Vec4V ijkl_dddd_n = AddScaled(AddScaled(AddScaled(ijkl_wwww, ijkl_zzzz_n, ijkl_zzzz), ijkl_yyyy_n, ijkl_yyyy), ijkl_xxxx_n, ijkl_xxxx);
#endif // EDGE_NUM_MODEL_CLIP_PLANES > 8

		Vec4V dddd_n = Min(abcd_dddd_n, efgh_dddd_n);
#if EDGE_NUM_MODEL_CLIP_PLANES > 8
		dddd_n = Min(dddd_n, ijkl_dddd_n);
#endif // EDGE_NUM_MODEL_CLIP_PLANES > 8

		*trivialAccept = IsGreaterThanOrEqualAll(dddd_n, zero);
	}

	return visible;
}

void spuModel::Draw(spuShaderGroup &group,u32 bucketMask,int lod,u16 stats)
{
	ValidateSpuPtr(m_Geometries.m_Elements);
	ValidateSpuPtr(m_ShaderIndex);

	DEV_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_SPUMODEL_DRAW) != 0)
	{
		return;
	})

	if( BUCKETMASK_MODELMATCH(m_Mask,bucketMask) )
		return;
		
	const Vec3V* aabbs = pSpuGcmState->CullerAABB ? (const Vec3V*)m_AABBs : NULL;
	const Vec4V* cm = (const Vec4V*)&pSpuGcmState->LocalLRTB;

	if (m_Count > 1 && aabbs)
	{
		const Vec3V boxMin = aabbs[0];
		const Vec3V boxMax = aabbs[1];
		aabbs += 2;

		if (!IsAABBVisibleInline_4(boxMin, boxMax, cm))
		{
			DEV_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_CULL_FRUSTUM_LRTB) == 0))
			{
				DRAWABLESPU_STATS_INC(ModelsCulled);
				return;
			}
		}
	}

	DRAWABLESPU_STATS_INC(ModelsDrawn);

#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
	pSpuGcmState->ModelCullStatus = 0;
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
	Vec4V transposedPlanes[EDGE_NUM_MODEL_CLIP_PLANES]; // only used locally
	grcState__BuildEdgeClipPlanes(transposedPlanes, EDGE_NUM_MODEL_CLIP_PLANES, pSpuGcmState);

#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
	for (int i = 0; i < EDGE_NUM_TRIANGLE_CLIP_PLANES; i++) // write transformed transposed clip planes back to pSpuGcmState->EdgeClipPlanesTransposed
	{
		pSpuGcmState->EdgeClipPlanesTransposed[i] = transposedPlanes[i].GetIntrin128();
	}
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES

#if HACK_GTA4
	u32* tintTab = NULL;
	const u32 tintDmaTag=8;
	if(pSpuGcmState->tintDescriptorPtr)
	{
		// note: drawablespu stack = 8KB
		// EDGE Tint #1: start fetching tint descriptor:
		const u32 dmaSize = ((pSpuGcmState->tintDescriptorCount*sizeof(u32))+0xf)&(~0xf);
		Assert(dmaSize < 256);
		tintTab = (u32*)AllocaAligned(u8, dmaSize, 16);
		sysDmaGet((void*)tintTab, (u32)pSpuGcmState->tintDescriptorPtr, dmaSize, tintDmaTag);
	}
#endif //HACK_GTA4...

	ValidateSpuPtr(&group);
	ValidateSpuPtr(group.m_Shaders.m_Elements);

	for (int i = 0; i < m_Count; i++)
	{
		if (aabbs)
		{
			const Vec3V boxMin = aabbs[0];
			const Vec3V boxMax = aabbs[1];
			aabbs += 2;

			if (pSpuGcmState->EdgeClipPlaneEnable == 0xff) // extended (shadows), ignore cm
			{
				u32 trivialAccept = 0;

				if (!IsAABBVisibleInline_Extended(boxMin, boxMax, transposedPlanes, &trivialAccept)) // EDGE_NUM_MODEL_CLIP_PLANES
				{
					DEV_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_CULL_EDGE_CLIP_PLANES) == 0))
					{
						DRAWABLESPU_STATS_INC(GeomsCulled);
						continue;
					}
				}

#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
				NOTFINAL_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_CULL_TRIANGLES) == 0))
				{
					pSpuGcmState->ModelCullStatus = trivialAccept ^ 1;
				}
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
			}
			else // not extended, clip using cm and optionally 1-4 clip planes
			{
				if (!IsAABBVisibleInline_4(boxMin, boxMax, cm)) // 4 planes (frustum LRTB)
				{
					DEV_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_CULL_FRUSTUM_LRTB) == 0))
					{
						DRAWABLESPU_STATS_INC(GeomsCulled);
						continue;
					}
				}
				else if (Unlikely(pSpuGcmState->EdgeClipPlaneEnable))
				{
					u32 trivialAccept = 0;

					if (!IsAABBVisibleInline_4(boxMin, boxMax, transposedPlanes, &trivialAccept)) // up to 4 more planes (e.g. water)
					{
						DEV_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_CULL_EDGE_CLIP_PLANES) == 0))
						{
							DRAWABLESPU_STATS_INC(GeomsCulled);
							continue;
						}
					}

#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
					NOTFINAL_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_CULL_TRIANGLES) == 0))
					{
						pSpuGcmState->ModelCullStatus = trivialAccept ^ 1;
					}
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
				}
			}
		}

		/// char *scratchSave = spuScratch;
		spuShader *shader = group.m_Shaders.m_Elements[m_ShaderIndex[i]];
		if (sm_ForceShader)
			shader = sm_ForceShader;
		ValidateSpuPtr(shader);

		BANK_ONLY(if (shader->IsFlashing(pSpuGcmState->FlashEnable) || shader->IsDisabled()) continue);

		u32 shaderBucketMask = shader->m_EffectInstance.DrawBucketMask;
		if (BUCKETMASK_MATCH(shaderBucketMask, bucketMask))
		{
#if HACK_GTA4
			// EDGE Tint #2:
			if(pSpuGcmState->tintDescriptorPtr)
			{	
				sysDmaWaitTagStatusAll(1<<tintDmaTag);	// wait for tintTab[]
				Assert(m_ShaderIndex[i] < pSpuGcmState->tintDescriptorCount);
				u32 tint = tintTab[ m_ShaderIndex[i] ];
				pSpuGcmState->edgeTintPalettePtr= tint? ((void*)(tint&~0xf)) : NULL;
				pSpuGcmState->edgeTintFlags		= tint&0xf;
			}
#endif //HACK_GTA4

			spuGeometry *geom = m_Geometries[i];
			ValidateSpuPtr(geom);
#if !__FINAL
			pSpuGcmState->CurrentGeometry = geom;
#endif // !__FINAL
			int numPasses = shader->BeginDraw(RMC_DRAW, true);
			DRAWABLESPU_STATS_INC(GeomsDrawn);
			for (int j=0; j<numPasses; j++) {
				shader->Bind(j);
				if (geom->m_Type == spuGeometry::GEOMETRYQB)
				{
					DRAWABLESPU_STATS_CONTEXT_INC(stats);
					((spuGeometryQB*)(geom))->Draw();
				}
				else
				{
					spuGeometryEdge* pSpuGeom = (spuGeometryEdge*)(geom);

					for(int k = 0; k < pSpuGeom->m_Count; k++)
					{
						DRAWABLESPU_STATS_CONTEXT_INC(stats);
					}

					pSpuGeom->Draw(
#if HACK_GTA4
					true	// flushVC
#endif
					);
				}
				shader->UnBind();
			}
			shader->EndDraw();

#if HACK_GTA4
			// EDGE Tint #3:
			if(pSpuGcmState->tintDescriptorPtr)
			{
				pSpuGcmState->edgeTintPalettePtr = NULL;
			}
#endif //HACK_GTA4...
		}
		/// spuScratch = scratchSave;
	}

#if HACK_GTA4
	// EDGE Tint #4:
	if(pSpuGcmState->tintDescriptorPtr)
	{
		sysDmaWaitTagStatusAll(1<<tintDmaTag); // wait for fetched desc in case it never happened during step #2
	}
#endif
}

void spuModel::DrawSkinned(spuShaderGroup &group,spuMatrixSet &ms,u32 bucketMask,int lod,u16 stats)
{
	DEV_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_SPUMODEL_DRAWSKINNED) != 0)
	{
		return;
	})

	if( BUCKETMASK_MODELMATCH(m_Mask,bucketMask) )
		return;

#if HACK_GTA4
	u32* tintTab = NULL;
	const u32 tintDmaTag=8;
	if(pSpuGcmState->tintDescriptorPtr)
	{
		// EDGE Tint #1: start fetching tint descriptor:
		const u32 dmaSize = ((pSpuGcmState->tintDescriptorCount*sizeof(u32))+0xf)&(~0xf);
		Assert(dmaSize < 256);
		tintTab = (u32*)AllocaAligned(u8, dmaSize, 16);
		sysDmaGet((void*)tintTab, (u32)pSpuGcmState->tintDescriptorPtr, dmaSize, tintDmaTag);
	}
#endif //HACK_GTA4...

	ValidateSpuPtr(m_Geometries.m_Elements);
	ValidateSpuPtr(m_ShaderIndex);
	ValidateSpuPtr(&group);
	ValidateSpuPtr(group.m_Shaders.m_Elements);

	DRAWABLESPU_STATS_INC(ModelsDrawn);

#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES
	pSpuGcmState->ModelCullStatus = 0;
#if ENABLE_EDGE_CULL_CLIPPED_TRIANGLES_FOR_NON_AABB_MODELS
	NOTFINAL_ONLY(if ((pSpuGcmState->CullerDebugFlags & CULLERDEBUGFLAG_CULL_TRIANGLES) == 0))
	{
		Vec4V* transposedPlanes = (Vec4V*)pSpuGcmState->EdgeClipPlanesTransposed; // write transformed transposed clip planes back to pSpuGcmState->EdgeClipPlanesTransposed

		if (grcState__BuildEdgeClipPlanes(transposedPlanes, EDGE_NUM_TRIANGLE_CLIP_PLANES, pSpuGcmState))
		{
			// TODO -- how does this work? we need the localspace AABB of the model to determine ModelCullStatus
			pSpuGcmState->ModelCullStatus = 1;
		}
	}
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES_FOR_NON_AABB_MODELS
#endif // ENABLE_EDGE_CULL_CLIPPED_TRIANGLES

	for (int i = 0; i < m_Count; i++)
	{
		/// char *scratchSave = spuScratch;
		spuShader *shader = (group.m_Shaders[m_ShaderIndex[i]]);
		if (sm_ForceShader)
			shader = sm_ForceShader;
		ValidateSpuPtr(shader);

		BANK_ONLY(if (shader->IsFlashing(pSpuGcmState->FlashEnable) || shader->IsDisabled()) continue);

		u32 shaderBucketMask = shader->m_EffectInstance.DrawBucketMask;
		if (BUCKETMASK_MATCH(shaderBucketMask, bucketMask))
		{
#if HACK_GTA4
			// EDGE Tint #2:
			if(pSpuGcmState->tintDescriptorPtr)
			{
				sysDmaWaitTagStatusAll(1<<tintDmaTag);	// wait for tintTab[]
				Assert(m_ShaderIndex[i] < pSpuGcmState->tintDescriptorCount);
				u32 tint = tintTab[ m_ShaderIndex[i] ];
				pSpuGcmState->edgeTintPalettePtr= tint? ((void*)(tint&~0xf)) : NULL;
				pSpuGcmState->edgeTintFlags		= tint&0xf;
			}
#endif //HACK_GTA4

			spuGeometry *geom = m_Geometries[i].ptr;
			ValidateSpuPtr(geom);
#if !__FINAL
			pSpuGcmState->CurrentGeometry = geom;
#endif // !__FINAL
			int numPasses = shader->BeginDraw(geom->m_Type == spuGeometry::GEOMETRYQB? RMC_DRAWSKINNED : RMC_DRAW, true);
			DRAWABLESPU_STATS_INC(GeomsDrawn);
			for (int j=0; j<numPasses; j++) {
				shader->Bind(j);
				if (geom->m_Type == spuGeometry::GEOMETRYQB)
				{
					DRAWABLESPU_STATS_CONTEXT_INC(stats);
					((spuGeometryQB*)(geom))->DrawSkinned(ms);
				}
				else
				{
					spuGeometryEdge* pSpuGeom = (spuGeometryEdge*)(geom);

					for(int k = 0; k < pSpuGeom->m_Count; k++)
					{
						DRAWABLESPU_STATS_CONTEXT_INC(stats);
					}

					pSpuGeom->DrawSkinned(ms
#if HACK_GTA4
					,true	// flushVC
#endif
					);
				}
				shader->UnBind();
			}
			shader->EndDraw();

#if HACK_GTA4
			// EDGE Tint #3:
			if(pSpuGcmState->tintDescriptorPtr)
			{
				pSpuGcmState->edgeTintPalettePtr = NULL;
			}
#endif //HACK_GTA4...
		}
		/// spuScratch = scratchSave;
	}

#if HACK_GTA4
	// EDGE Tint #4:
	if(pSpuGcmState->tintDescriptorPtr)
	{	
		sysDmaWaitTagStatusAll(1<<tintDmaTag); // wait for fetched desc in case it never happened during step #2
	}
#endif
}


#endif	// __SPU
