// 
// grmodel/shaderfactory.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "shader.h"
#include "shadergroup.h"
#include "grcore/channel.h"

#include "data/marker.h"
#include "file/asset.h"
#include "file/token.h"
#include "system/param.h"

#include "shaderfx.h"

#include <stdlib.h>

using namespace rage;

grmShaderFactory* grmShaderFactory::sm_Instance = 0;
grmShaderFactory* grmShaderFactory::sm_PrevInstance = 0;

#if __TOOL
const char *grmShaderFactory::sm_ShaderLibPath = 0;
#endif	// __TOOL



grmShaderFactory::grmShaderFactory() {
}



grmShaderFactory::~grmShaderFactory() {
	if (sm_Instance == this)
		sm_Instance = 0;
}


grmShader* grmShaderFactoryStandard::Create() {
	return rage_contained_new grmShaderFx;
}

#if __TOOL
const char *grmShaderFactory::GetShaderLibPath() {
	return sm_ShaderLibPath;
}

void grmShaderFactory::SetShaderLibPath(const char *path) {
	if ( sm_ShaderLibPath )
		StringFree( sm_ShaderLibPath );
	sm_ShaderLibPath = StringDuplicate(path);
}

void grmShaderFactory::ClearShaderLibPath()
{
	if( sm_ShaderLibPath ) {
		StringFree(sm_ShaderLibPath);
		sm_ShaderLibPath = 0;
	}
}
#endif



void grmShaderFactoryStandard::PlaceShader(datResource& rsc,grmShader **shaders,int count) {
	for (int i=0; i<count; i++) {
		rsc.PointerFixup(shaders[i]);
		::new (shaders[i]) grmShaderFx(rsc);
	}
}

void grmShaderFactoryStandard::PlaceShaderGroup(datResource &rsc,grmShaderGroup* &group) {
	if (group) {
		rsc.PointerFixup(group);
		::new (group) grmShaderGroup(rsc);
	}
}



void grmShaderFactoryStandard::PreloadShaders( const char *path, const char* preloadFileName /* = "preload" */) {
	RAGE_FUNC();

	grcEffect::Preload(path, preloadFileName);
}


grmShaderGroup* grmShaderFactoryStandard::CreateGroup() {
	return rage_contained_new grmShaderGroup;
}


grmShaderGroup* grmShaderFactoryStandard::LoadGroup(fiTokenizer & T) {

	if (T.CheckIToken("shadinggroup"))
	{
		T.GetDelimiter("{");
		if (T.CheckToken("Count"))
			T.IgnoreToken();
		grmShaderGroup *g = CreateGroup();
		if (!g->Load(T))
		{
			grcErrorf("Failed to load shading group");
			return NULL;
		}
		T.GetDelimiter("}");
		return g;
	}
	else
	{
		if (T.CheckToken("Count"))
			T.IgnoreToken();
		grmShaderGroup *g = CreateGroup();
		if (!g->Load(T))
		{
			grcErrorf("Failed to load shading group");
			return NULL;
		}
		return g;
	}
}

grmShaderGroup* grmShaderFactoryStandard::LoadGroup(const char *filename) {
	fiSafeStream S(ASSET.Open(filename,"shadergroup"));
	if (!S)
		return NULL;
	fiTokenizer T;
	T.Init(filename,S);
	return LoadGroup(T);
}


grmShaderFactory *grmShaderFactory::CreateStandardShaderFactory(bool bMakeActive) {
	grmShaderFactory *pFactory = rage_new grmShaderFactoryStandard;

	if	(bMakeActive)
		sm_Instance = pFactory;

	return(pFactory);
}
