//
// grmodel/setup.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "setup.h"

#include "modelfactory.h"
#include "shader.h"
#include "shaderfx.h"
#include "system/param.h"

using namespace rage;

XPARAM(nographics);

void grmSetup::CreateDefaultFactories() {
	grcSetup::CreateDefaultFactories();
	grmShader::InitClass();

	grmShaderFactory::CreateStandardShaderFactory();
	grmModelFactory::CreateStandardModelFactory();
}

void grmSetup::DestroyFactories() {
#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && __D3D11
	GRCDEVICE.LockContext();
#endif
	grmShader::ShutdownClass();
	delete &grmModelFactory::GetInstance();
	delete &grmShaderFactory::GetInstance();

	grcSetup::DestroyFactories();
#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && __D3D11
	GRCDEVICE.UnlockContext();
#endif
}


void grmSetup::BeginUpdate() {
	grcSetup::BeginUpdate();
}
