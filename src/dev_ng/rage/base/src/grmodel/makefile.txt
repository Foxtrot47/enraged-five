Project grmodel
Files {
	model.cpp
	model.h
	setup.cpp
	setup.h
	shader.cpp
	shader.h
	shadervar.cpp
	shadervar.h
	shaderfx.h
	curvedmodel.cpp
	curvedmodel.h
	shadergroup.cpp
	shadergroup.h
	matrixset.cpp
	matrixset.h
	modelfactory.cpp
	modelfactory.h
	namedbuckets.h
	namedbuckets.cpp
	shadersorter.cpp
	shadersorter.h
	geometry.cpp
	geometry.h
	shaderfactory.cpp
	modelinfo.h
	shadergroupvar.h
	grmodel_config.h
	grmodelspu.cpp
	grmodelspu.h
	grmodelspu_case.h
}
Custom {
	grmodel.dtx
}
