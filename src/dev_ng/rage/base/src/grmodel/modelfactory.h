// 
// grmodel/modelfactory.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRMODEL_MODELFACTORY_H
#define GRMODEL_MODELFACTORY_H

#include "atl/bitset.h"
#include "grcore/channel.h"
#include "grcore/device.h"
#include "mesh/mesh_config.h"

namespace rage {

class grmModel;
class grcFvf;
struct grmModelInfo;
class grmShaderGroup;

template <class, int> class atRangeArray;

/* PURPOSE
	grmVtxStreamConfigurator is a class that is used by RAGE's internal model building code
	to determine how to process mesh files as they are loaded.
	This can be used to control how data is packed up to be sent down to vertex shaders
	It is ONLY valid when model's are created.  You can not query this to see how an existing
	grmModel has been built.
*/
class grmVtxStreamConfigurator {
public:
	grmVtxStreamConfigurator();
	~grmVtxStreamConfigurator();

	// Set these flags to control the how the model vertex buffers are created at a global scale
	//	Currently only supported on D3D builds
	// PURPOSE: Enable/disable normals from the vertex stream
	// PARAMS:	e - Enable or disable
	//			stream - the vertex stream to configure (<0 for all vertex streams)
	void SetEnableNormals(bool e, int stream = -1);
	// PURPOSE: Enable/disable normal packing in the vertex stream
	// PARAMS:	e - Pack on or off
	//			stream - the vertex stream to configure (<0 for all vertex streams)
	void SetPackNormals(bool p, int stream = -1);
	// PURPOSE: Enable/disable tex coord packing in the vertex stream
	// PARAMS:	e - Pack on or off
	//			stream - the vertex stream to configure (<0 for all vertex streams)
	void SetPackTexCoords(bool p, int stream = -1);
	// PURPOSE: Enable/disable texture coords from the vertex stream
	// PARAMS:	stage - the texture stage to control
	//			e - Enable or disable
	//			stream - the vertex stream to configure (<0 for all vertex streams)
	void SetEnableTexCoord(int stage, bool e, int stream = -1);
	// PURPOSE: Enable/disable texture coords from the vertex stream
	// PARAMS:	stage - the texture stage to control
	//			e - Enable or disable
	//			stream - the vertex stream to configure (<0 for all vertex streams)
	void SetEnableTangent(int stage, bool e, int stream = -1);
	// PURPOSE: Enable/disable texture coords from the vertex stream
	// PARAMS:	stage - the texture stage to control
	//			e - Enable or disable
	//			stream - the vertex stream to configure (<0 for all vertex streams)
	void SetEnableBinormal(int stage, bool e, int stream = -1);


	// Query the various vertex buffer control bits
	// PURPOSE: Query to see if normals are enabled for a given texture stage
	// PARAMS:	stream - the vertex stream to query
	bool AreNormalsEnabled(int stream=0);
	// PURPOSE: Query to see if normals are packed for a given texture stage
	// PARAMS:	stream - the vertex stream to query
	bool AreNormalsPacked(int stream=0);
	// PURPOSE: Query to see if tex coords are packed for a given texture stage
	// PARAMS:	stream - the vertex stream to query
	bool AreTexCoordsPacked(int stream=0);
	// PURPOSE: Query to see if texture uv's are enabled for a given texture stage
	// PARAMS: stage - the texture stage to query
	//			stream - the vertex stream to query
	bool IsTexCoordEnabled(int stage, int stream=0);
	// PURPOSE: Query to see if tangents are enabled for a given texture stage
	// PARAMS: stage - the texture stage to query
	//			stream - the vertex stream to query
	bool IsTangentEnabled(int stage, int stream=0);
	// PURPOSE: Query to see if tangents are enabled for a given texture stage
	// PARAMS: stage - the texture stage to query
	//			stream - the vertex stream to query
	bool IsBinormalEnabled(int stage, int stream=0);

private:
	void SetStageEnable(int baseBit, int stage, bool value, int stream);
	bool IsStageEnabled(int baseBit, int stage, int stream);

	static const int MAX_STREAMS = 8;
	static const int MAX_TEX_STAGES = grcFvf::grcfcTexture7 - grcFvf::grcfcTexture0 + 1;
	static const int MAX_TANGENT_STAGES = grcFvf::grcfcTangent1 - grcFvf::grcfcTangent0 + 1; // See fvfchannels
	static const int MAX_BINORMAL_STAGES = grcFvf::grcfcBinormal1 - grcFvf::grcfcBinormal0 + 1; // See fvfchannels
	enum {	
		BIT_DISABLE_NORMALS,
		BIT_PACK_NORMALS,
		BIT_PACK_TEXCOORDS,
		BIT_DISABLE_TEXCOORD0,
		BIT_DISABLE_TANGENT0 = BIT_DISABLE_TEXCOORD0 + MAX_TEX_STAGES,
		BIT_DISABLE_BINORMAL0 = BIT_DISABLE_TANGENT0 + MAX_TANGENT_STAGES,
		BIT_COUNT = BIT_DISABLE_BINORMAL0 + MAX_BINORMAL_STAGES		// MAKE SURE THIS IS LAST
	};
	CompileTimeAssert((BIT_COUNT - BIT_DISABLE_TEXCOORD0) == (grcFvf::grcfcCount - grcFvf::grcfcTexture0));

	atFixedBitSet<BIT_COUNT>	m_Flags[MAX_STREAMS];
};


/*	PURPOSE
		grmModelFactory is responsible for creating models.  Projects are free to derive
		their own factories as their needs require.
		These factories were originally singletons, but have recently changed to pseudo
		multi-tons in order to better meet project's needs.
 */
class grmModelFactory {
public:
	grmModelFactory();
	virtual ~grmModelFactory();

	// RETURNS: An instance of this mdoel factory
	static grmModelFactory& GetInstance() { grcAssertf(sm_Instance , "You forgot to create an grmModelFactory"); return *sm_Instance; }
	
	// PURPOSE: To change the current working model factory
	static void SetInstance(grmModelFactory &instance) { sm_Instance = &instance; }
	
	// PURPOSE: Change the working model factory on a temporary basis (use PopInstance to restore)
	static void PushInstance(grmModelFactory &newInst) {
		FastAssert(!sm_PrevInstance);
		sm_PrevInstance = sm_Instance;
		sm_Instance = &newInst;
	}
	// PURPOSE: Restore the previously set model factor as the "active" model factory
	static void PopInstance() {
		FastAssert(sm_PrevInstance);
		sm_Instance = sm_PrevInstance;
		sm_PrevInstance = 0;
	}

	// PURPOSE: Create a base RAGE model factory
	// PARAMS: bMakeActive - set to true to force it to become the active model factory
	static grmModelFactory *CreateStandardModelFactory(bool bMakeActive = true);

#if MESH_LIBRARY
	// PURPOSE: Create a grmModel instance based on supplied name and matrix index
	virtual grmModel* Create(fiTokenizer &t, const char *name,int mtxIndex,grmModelInfo *outInfo, const grmShaderGroup* pShaderGroup = 0, bool optimize = true, int extraVerts = 0, u8 mask = 0xFF, int lod = 0);
#endif

	// PURPOSE: Deal with streaming a new grmModel object into memory
	virtual void PlaceModel(class datResource& rsc,grmModel*  &model);

	enum {MaxVertElements = 64};
	// PURPOSE:	Builds a vertex element array given an arbitrary number of fvf(s). (Allows custom modifications to the array before building a declarator)
	// PARAMS:	fvf0, fvf1, fvf2, fvf3 -	The fvf data for each of up to 4 streams, they can be null if not used
	//			elements -					[OUT] Array of vertex elements to be written.
	//			elemCounts -				[OUT] Array of ints to store the current count of each type of vertex element.
	// RETURNS: Returns number of entries written out to element array
	static int ComputeVertexElements(const grcFvf *fvf0, const grcFvf *fvf1, const grcFvf *fvf2, const grcFvf *fvf3, atRangeArray<grcVertexElement, MaxVertElements> &elements, atRangeArray<int, grcVertexElement::grcvetCount> &elemCounts);

	// PURPOSE: Builds a cached declarator from the vertex element array
	// PARAMS: fvf0, fvf1, fvf2, fvf3 -		The fvf data for each of up to 4 streams, they can be null if not used. (Should be the same that were used to create grcVertexElement array!)
	//			elements -					Array of vertex elements to be used in creating the vertex declaration.
	//			numElem -					Number of valid entries in the elements array.
	// RETURNS: a grcVertexDeclaration object, which can then be used for shaders
	static grcVertexDeclaration *BuildCachedDeclarator(const grcFvf *fvf0, const grcFvf *fvf1, const grcFvf *fvf2, const grcFvf *fvf3, atRangeArray<grcVertexElement, MaxVertElements> &elements, int numElem);

	// PURPOSE: Builds a declarator given up to 4 streams of fvf format(s)
	// PARAMS: fvf0, fvf1, fvf2, fvf3 - The fvf data for each of up to 4 streams, they can be null if not used
	//			forceRecreate - Forces the vertex declarator to be recreated even if it matches one already created
	// RETURNS: a grcVertexDeclaration object, which can then be used for shaders
	static grcVertexDeclaration *BuildDeclarator(const grcFvf *fvf0, const grcFvf *fvf1, const grcFvf *fvf2, const grcFvf *fvf3);

	static void FreeDeclarator(grcVertexDeclaration*);

	// RETURNS: The Vertex Stream Configurator object -- use this to control how models should be packaged up
	//	into vertex streams for use by the vertex shaders.
	static grmVtxStreamConfigurator *GetVertexConfigurator();

private:
	static grmModelFactory *sm_Instance;
	static grmModelFactory *sm_PrevInstance;
	static grmVtxStreamConfigurator sm_VtxCfg;
	// static atMap<ConstString, grcVertexDeclaration *> sm_DeclMap;
};


} // namespace rage


#endif	// GRMODEL_MODELFACTORY_H
