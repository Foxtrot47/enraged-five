// 
// grmodel/grmodel_config.h
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRMODEL_GRMODEL_CONFIG_H
#define GRMODEL_GRMODEL_CONFIG_H

#define USE_PACKED_TEXCOORDS (HACK_MC4)
#define USE_BLENDSHAPE_NORMALS_BINORMALS_TANGENTS (HACK_GTA4)

#define USE_MTL_SYSTEM		(HACK_RDR2)

#endif // GRMODEL_GRMODEL_CONFIG_H
