// 
// grmodel/shadergroup.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef GRMODEL_SHADERGROUP_H
#define GRMODEL_SHADERGROUP_H

#include "shader.h"			// FIX THIS
#include "shadergroupvar.h"

#include "atl/array.h"
#include "data/resource.h"
#include "data/struct.h"
#include "data/base.h"
#include "paging/dictionary.h"

#include "string/string.h"

namespace rage {

class grmShader;
class fiTokenizer;
class rageShaderMaterialGroup;

struct grcEffectVarPair { 
	u8 index, var; 
	void Init(int i,grcEffectVar v) { index = (u8)i; var=(u8) v; } 
#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif
};

struct grcEffectVarEntry {
	grcEffectVarEntry(datResource&rsc) : Pairs(rsc) { }
	grcEffectVarEntry() { }
	u32 Hash;
	atArray<grcEffectVarPair> Pairs;
	IMPLEMENT_PLACE_INLINE(grcEffectVarEntry);
#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif
	void Clone( const grcEffectVarEntry& copyme )
	{
		Hash = copyme.Hash;
		const int pairsCount = copyme.Pairs.GetCount();
		if( pairsCount > 0 )
		{
			Pairs.Resize( pairsCount );
			for( int i = 0; i < pairsCount; ++i )
			{
				Pairs[i] = copyme.Pairs[i];
			}
		}
	}
};

/* 
PURPOSE
	An grmShaderGroup is a collection of shaders that can be shared by multiple models,
	lod groups, or even types.  A simple case is multiple LOD's wanting to share the
	same shaders to keep continuity of texture slide.  Every type also maintains a master
	grmShaderData list for per-instance shader data.  Every instance that gets created
	can either use the type's data or create its own unique array.
<FLAG Component>
*/
class grmShaderGroup: public datBase
{
public:
	explicit grmShaderGroup(const grmShaderGroup* copyme);

	grmShaderGroup();
	grmShaderGroup(class datResource&);
	DECLARE_PLACE(grmShaderGroup);
	virtual ~grmShaderGroup();

	// PURPOSE: Minimal destructor
	void Destroy() { delete m_ShaderDatas; }

	// RETURNS: The count of shaders in this group
	inline int GetCount() const { return m_Shaders.GetCount(); }
	// RETURNS: Pointer to array of shader pointers
	inline const datOwner<grmShader> *GetShaderPtrArray() const { return &m_Shaders[0]; }
	// PURPOSE: Add a shader by filename to the group
	// RETURNS: The slot index of the shader
	int Add(const char *shaderName);
	// PURPOSE: Add a shader to the group
	// RETURNS: The slot index of the shader
	int Add(grmShader *shader);
	// PURPOSE: Remove a shader from the group and optionally delete it. 
	// RETURNS: true if the shader was found and removed.
	bool Remove(grmShader *shader, const bool owned = false);
	// RETURNS: A shader for the specified index
	inline const grmShader& operator[](int idx) const;
	// RETURNS: A shader for the specified index
	inline grmShader& GetShader(int idx) const;
	// RETURNS: A shader for the specified index
	inline grmShader* GetShaderPtr(int idx) const;
	// PURPOSE: Load a shadergroup from the tokenizer
	bool Load(fiTokenizer &T);
	// PURPOSE: Load a shadergroup from a rageShaderMaterialGroup object
	bool Load(const rageShaderMaterialGroup &mtlGroup);
	// RETURNS: Channel mask for the specified index
#if !__SPU
	inline u32 GetChannelMask(int idx) const { return m_Shaders[idx]->GetChannelMask(); }
#endif
	// RETURNS: Sort key for the specified index
	// inline u32 GetSortKey(int idx) const { return m_Shaders[idx]->GetSortKey(); }

	// PURPOSE: Used to preallocate shader array based on specified count
	void InitCount(int count);

	// PURPOSE: Find a shader by name -- performs a linear search
	// RETURNS: The shader if found, null if not
	grmShader *LookupShader(const char *shaderName) const;

	// PURPOSE: Check to see if there are any shaders that end in the provided suffix
	// RETURNS: True if there is a shader with that suffix, otherwise false
	bool CheckForShadersWithSuffix(const char *suffix);

	// PURPOSE: Save all the mtl files for this shader group
	// RETURNS: void
	void SaveAllMtlFiles() const;

	// PURPOSE: Reloads all the mtl files for this shader group
	// RETURNS: void
	void ReloadAllMtlFiles() const;

	// PURPOSE: Save all the mtl types for this shader group
	// RETURNS: void
	void SaveAllMtlTypes() const;

	// PURPOSE: Reloads all the mtl files for this shader group
	// RETURNS: void
	void ReloadAllMtlTypes() const;
	
#if __BANK
	// PURPOSE: Add the widgets for this shader
	void AddWidgets(class bkBank&);
#endif

	// PURPOSE: Called by streaming system
	void Delete() {};	//For str3aming

#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif

	// PURPOSE:	Look up a variable within the shadergroup
	// PARAMS:	name - Name to search all shaders for
	//			mustExist - True if variable must exist (will Quitf if not there)
	// RETURNS:	Handle value if found, or grmsgvNONE (0) if not found.
	// NOTES:	This function currently doesn't remember existing variables, so don't look up the
	//			same variable more than once (and certainly not on every game update).
	grmShaderGroupVar LookupVar(const char *name,bool mustExist = true) const;

#if __D3D11 || RSG_ORBIS
	// PURPOSE:	Look up a variable within the shadergroup
	// PARAMS:	name - Name to search all shaders for
	//			mustExist - True if variable must exist (will Quitf if not there)
	// RETURNS:	Handle value if found, or grmsgvNONE (0) if not found.
	// NOTES:	Instancing variables record a grcEffectVarPair for all shaders in the group, grcevNONE is stored where the variable
	//			doesn`t exists in a shader, this is required for fast indexing by shader group index. Variables are registered 
	//			under HashToRegisterWith, this way an instancing look up an exist along side a normal look up.
	grmShaderGroupVar LookupVarForInstancing(u32 HashName, u32 HashToRegisterWith) const;
#endif //__D3D11 || RSG_ORBIS
	
	// PURPOSE:	See whether a variable id exists within the shadergroup
	// RETURNS:	True or false if found.
	bool IsVar(grmShaderGroupVar var) const;

	// PURPOSE:	Set a variable in a shadergroup
	void SetVar(grmShaderGroupVar,const grcTexture *tex) const;
	void SetVar(grmShaderGroupVar var,int value) const { SetVarCommon(var,&value,4,1); }
	void SetVar(grmShaderGroupVar var,float value) const { SetVarCommon(var,&value,4,1); }
	void SetVar(grmShaderGroupVar var,const float *value,int count) const { SetVarCommon(var,value,4,count); }
	void SetVar(grmShaderGroupVar var,const Vector2 &value) const { SetVarCommon(var,&value,8,1); }
	void SetVar(grmShaderGroupVar var,const Vector2 *value,int count) const { SetVarCommon(var,value,8,count); }
	void SetVar(grmShaderGroupVar var,const Vector3 &value) const { SetVarCommon(var,&value,16,1); }
	void SetVar(grmShaderGroupVar var,const Vector3 *value,int count) const { SetVarCommon(var,value,16,count); }
	void SetVar(grmShaderGroupVar var,Color32 value) const;
	void SetVar(grmShaderGroupVar var,const Vector4 &value) const { SetVarCommon(var,&value,16,1); }
	void SetVar(grmShaderGroupVar var,const Color32 *value,int count) const;
	void SetVar(grmShaderGroupVar var,const Vector4 *value,int count) const { SetVarCommon(var,value,16,count); }
	void SetVar(grmShaderGroupVar var,const Matrix34 &value) const { SetVarCommon(var,&value,64,1); }
	void SetVar(grmShaderGroupVar var,const Matrix34 *value,int count) const { SetVarCommon(var,value,64,count); }
	void SetVar(grmShaderGroupVar var,const Matrix44 &value) const { SetVarCommon(var,&value,64,1); }
	void SetVar(grmShaderGroupVar var,const Matrix44 *value,int count) const { SetVarCommon(var,value,64,count); }

	void SetVar(grmShaderGroupVar var,const Vec2V &value) const { SetVarCommon(var,&value,16,1); }
	void SetVar(grmShaderGroupVar var,const Vec2V *value,int count) const { SetVarCommon(var,value,16,count); }
	void SetVar(grmShaderGroupVar var,const Vec3V &value) const { SetVarCommon(var,&value,16,1); }
	void SetVar(grmShaderGroupVar var,const Vec3V *value,int count) const { SetVarCommon(var,value,16,count); }
	void SetVar(grmShaderGroupVar var,const Vec4V &value) const { SetVarCommon(var,&value,16,1); }
	void SetVar(grmShaderGroupVar var,const Vec4V *value,int count) const { SetVarCommon(var,value,16,count); }
	void SetVar(grmShaderGroupVar var,const Vec4f &value) const { SetVarCommon(var,&value,16,1); }
	void SetVar(grmShaderGroupVar var,const Vec4f *value,int count) const { SetVarCommon(var,value,16,count); }
	void SetVar(grmShaderGroupVar var,const Mat34V &value) const { SetVarCommon(var,&value,64,1); }
	void SetVar(grmShaderGroupVar var,const Mat34V *value,int count) const { SetVarCommon(var,value,64,count); }
	void SetVar(grmShaderGroupVar var,const Mat44V &value) const { SetVarCommon(var,&value,64,1); }
	void SetVar(grmShaderGroupVar var,const Mat44V *value,int count) const { SetVarCommon(var,value,64,count); }

	void SetVarByRef(grmShaderGroupVar var,const Vector2 &value) const { SetVarCommonByRef(var,&value,8,1); }
	void SetVarByRef(grmShaderGroupVar var,const Vector3 &value) const { SetVarCommonByRef(var,&value,16,1); }
	void SetVarByRef(grmShaderGroupVar var,const Vector3 *value,int count) const { SetVarCommonByRef(var,value,16,count); }
	void SetVarByRef(grmShaderGroupVar var,const Vector4 &value) const { SetVarCommonByRef(var,&value,16,1); }
	void SetVarByRef(grmShaderGroupVar var,const Vector4 *value,int count) const { SetVarCommonByRef(var,value,16,count); }
	void SetVarByRef(grmShaderGroupVar var,const Matrix34 &value) const { SetVarCommonByRef(var,&value,64,1); }
	void SetVarByRef(grmShaderGroupVar var,const Matrix34 *value,int count) const { SetVarCommonByRef(var,value,64,count); }
	void SetVarByRef(grmShaderGroupVar var,const Matrix44 &value) const { SetVarCommonByRef(var,&value,64,1); }
	void SetVarByRef(grmShaderGroupVar var,const Matrix44 *value,int count) const { SetVarCommonByRef(var,value,64,count); }

	void SetVarByRef(grmShaderGroupVar var,const Vec2V &value) const { SetVarCommonByRef(var,&value,16,1); }
	void SetVarByRef(grmShaderGroupVar var,const Vec3V &value) const { SetVarCommonByRef(var,&value,16,1); }
	void SetVarByRef(grmShaderGroupVar var,const Vec3V *value,int count) const { SetVarCommonByRef(var,value,16,count); }
	void SetVarByRef(grmShaderGroupVar var,const Vec4V &value) const { SetVarCommonByRef(var,&value,16,1); }
	void SetVarByRef(grmShaderGroupVar var,const Vec4V *value,int count) const { SetVarCommonByRef(var,value,16,count); }
	void SetVarByRef(grmShaderGroupVar var,const Vec4f &value) const { SetVarCommonByRef(var,&value,16,1); }
	void SetVarByRef(grmShaderGroupVar var,const Vec4f *value,int count) const { SetVarCommonByRef(var,value,16,count); }
	void SetVarByRef(grmShaderGroupVar var,const Mat34V &value) const { SetVarCommonByRef(var,&value,64,1); }
	void SetVarByRef(grmShaderGroupVar var,const Mat34V *value,int count) const { SetVarCommonByRef(var,value,64,count); }
	void SetVarByRef(grmShaderGroupVar var,const Mat44V &value) const { SetVarCommonByRef(var,&value,64,1); }
	void SetVarByRef(grmShaderGroupVar var,const Mat44V *value,int count) const { SetVarCommonByRef(var,value,64,count); }

#if SUPPORT_UAV
	void SetVar(grmShaderGroupVar var,const grcBufferUAV *v);
	void SetVarUAV(grmShaderGroupVar var,grcBufferUAV *v,int initCount);
	void SetVarUAV(grmShaderGroupVar var,const grcTextureUAV *v);
#endif	//SUPPORT_UAV

	// PURPOSE:	Update all texture references (that were setup via grmTextureFactory::ConnectTextureReference)
	//			To point to any newly assigned textures
	void UpdateTextureReferences();

#if __BANK || __TOOL
	// PURPOSE:	Updates all shaders to reflect any changes made in presets (for interactive tools)
	// void UpdatePresets();
#endif

	// PURPOSE: Enable automatic texture dictionary creation
	// PARAMS:	flag - Enable or disable automatic creation
	// RETURNS:	Previous value of flag
	// NOTES:	Has no effect if higher-level code has already created a texture dictionary
	//			and made it active.  Defaults to off to avoid affecting existing code.
	static bool SetAutoTexDict(bool flag);

	// RETURNS:	Current automatic texture dictionary creation flag
	static bool GetAutoTexDict() { return sm_AutoTexDict; }

	// PURPOSE:	Binds drawable to a new auto texture dictionary.  You may want to AddRef
	//			the original one before changing it the first time or else it will
	//			get released.  
	// PARAMS:	newDict - New texture dictionary, cannot be NULL.
	// NOTES:	The shadergroup must already have a texture dictionary before you
	//			change it with this function.
	void SetTexDict(pgDictionary<grcTexture> *newDict);

	// PURPOSE:	Binds all references within a drawable to textures from a previous texture 
	//			dictionary to a new texture dictionary. You may want to AddRef
	//			the original one before changing it the first time or else it will
	//			get released. The textures are exchanges based on their index within the 
	//			dictionary.
	// PARAMS:	oldDict - Old texture dictionary, cannot be NULL.
	//			newDict - New texture dictionary, cannot be NULL.
	// NOTES:	The shadergroup must already have a texture dictionary before you
	//			change it with this function.  
	//			Does NOT exchange the auto texture dictionary with the new one.
	void ExchangeTexDict(pgDictionary<grcTexture>* oldDict, pgDictionary<grcTexture>* newDict);

	// RETURNS: Current texture dictionary associated with a drawable (built-in one
	//			from AutoTexDict, or the last SetTexDict call)
	pgDictionary<grcTexture> *GetTexDict() { return m_TextureDictionary; }
	const pgDictionary<grcTexture> *GetTexDict() const { return m_TextureDictionary; }

	// RETURNS: The MTL template name of a specific shader in this group.
	// const char *GetMtlTemplateName(int shader) const		{ return GetShader(shader).GetMtlTemplateName(); }
	
	// RETURNS: Copy The MTL template name to the buffer
	// void GetMtlTemplateName(int shader, char* buffer, int bufferSize) const { return GetShader(shader).GetMtlTemplateName(buffer, bufferSize); }

#if !__FINAL
	// PURPOSE:	Creates a texture dictionary based on a list of input filenames
	// PARAMS:	inputs - List of filenames
	//			inputCount - Number of items in input list
	//			stripPath - True to strip any leading path before computing the hash code for each entry.
	//			params - List of texture creation params
	// RETURNS:	New texture dictionary, or NULL if no textures were found
	// NOTES:	Missing files are ignored with a warning, on the assumption they are supplied at runtime
	//			by a parent texture dictionary.  Internally the function sorts textures by descending size
	//			to reduce fragmentation and improve Xenon texture interleaving.  The function also calls
	//			the texture factory ClearFreeList function necessary for Xenon texture interleaving.
	static pgDictionary<grcTexture>* CreateTextureDictionary(const char **inputs,int inputCount,bool stripPath, grcTextureFactory::TextureCreateParams** params = NULL);

	// PURPOSE:	Creates a texture dictionary based on a file of texture filenames.
	// PARAMS:	filename - Name of file containing texture names; both .txt and .textures extensions are checked.
	//			stripPath - True to strip any leading path before computing the hash code for each entry.
	// RETURNS:	New texture dictionary, or NULL if file not found or no textures were found
	static pgDictionary<grcTexture>* CreateTextureDictionary(const char *filename,bool stripPath);
#endif

	int GetShaderGroupVarCount() const { return m_ShaderGroupVars.GetCount();	}
	const atArray<grcEffectVarEntry>& GetShaderGroupVars() const { return m_ShaderGroupVars; }

	static bool IsEnvClothMaterial( const char* materialName, const grmShaderGroup* shaderGroup );
	static bool IsPedClothMaterial( const char* materialName, const grmShaderGroup* shaderGroup );
	static bool IsBillboardMaterial( const char* materialName, const grmShaderGroup* shaderGroup );
	static bool IsPedMaterial( const char* materialName, const grmShaderGroup* shaderGroup );
	static bool IsDisplacementMaterial	( const char* materialName, const grmShaderGroup* shaderGroup );
	static bool IsCameraMaterial( const char* materialName, const grmShaderGroup* shaderGroup );

	u16 GetContainerSizeRaw() const { return m_ContainerSizeQW; }
	u16 GetContainerSize() const { return m_ContainerSizeQW << 4; }
	void SetContainerSize(u32 size) { FastAssert((size & 0xFFFF0) == size); m_ContainerSizeQW = u16(size >> 4); }

	//Instancing Support
	bool HasInstancedShaders() const { return m_HasInstancedShader; }

private:
	void SetVarCommon(grmShaderGroupVar var,const void *data,int stride,int count) const;
	void SetVarCommonByRef(grmShaderGroupVar var,const void *data,int stride,int count) const;

	void ScanForInstancedShaders();
#if (__D3D11 || RSG_ORBIS)
public:
	grcCBuffer *GetVarCBufferAndOffset(grmShaderGroupVar var, int ShaderIndex, int &Offset) const;
private:
#endif //__D3D11 || RSG_ORBIS

	datOwner<pgDictionary<grcTexture> > m_TextureDictionary;
	grcArray<datOwner<grmShader> > m_Shaders;

	// These are never resourced.
	mutable atArray<grcEffectVarEntry> m_ShaderGroupVars;
	u16 m_ContainerSizeQW;
	bool m_HasInstancedShader;
	u8 pad0;
	mutable grcInstanceData::Entry **m_ShaderDatas;		// copy of m_Shaders[i].Data for each shader to save DMA stage on SPU
#if __WIN32PC && !__FINAL
	static bool sm_DeferredLoad;
#endif
	static bool sm_AutoTexDict;

	// Resource-compiler specific callback definition.
#if __RESOURCECOMPILER
public:
	// Callback type to override the CreateTextureDictionary methods; we need
	// to override those in the resource compiler so we can correctly determine
	// the texture sizes to sort (they may not be packed, but linked via
	// the texture pipeline template system).
	typedef pgDictionary<grcTexture>* (*CustomCreateTextureDictionaryFuncType)( 
		const char **inputs,int inputCount,bool stripPath, grcTextureFactory::TextureCreateParams** params );

	static void SetCreateTextureDictionaryCallback( CustomCreateTextureDictionaryFuncType cb );
	static pgDictionary<grcTexture>* RunCreateTextureDictionaryCallback( 
		const char **inputs,int inputCount,bool stripPath, grcTextureFactory::TextureCreateParams** params );

private:
	static CustomCreateTextureDictionaryFuncType ms_CustomCreateTextureDictionaryCallback;
#endif // __RESOURCECOMPILER
};

inline const grmShader& grmShaderGroup::operator[](int idx) const { 
	return *m_Shaders[idx];
}

inline grmShader& grmShaderGroup::GetShader(int idx) const {
	return *m_Shaders[idx];
}

inline grmShader* grmShaderGroup::GetShaderPtr(int idx) const {
	return m_Shaders[idx];
}

}	// namespace rage
#endif		// GRMODEL_SHADER_H
