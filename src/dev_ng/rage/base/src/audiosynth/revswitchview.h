// 
// audiosynth/revswitchview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_REVSWITCHVIEW_H
#define SYNTH_REVSWITCHVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uilabel.h"
#include "uiscrollbar.h"
#include "uidropdownlist.h"

namespace rage
{

	class synthReverseSwitch;
	class synthReverseSwitchView : public synthModuleView
	{
	public:

		synthReverseSwitchView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();
		virtual u32 GetModuleHeight() const;

	private:

		synthReverseSwitch *GetSwitch() const {return (synthReverseSwitch*)m_Module; }

		synthUIScrollbar m_Divisions;
		synthUIDropDownList m_ModeSwitch;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_REVSWITCHVIEW_H

