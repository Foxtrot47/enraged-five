// 
// audiosynth/compressor.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "compressor.h"

#include "synthutil.h"
#include "math/vecmath.h"

namespace rage
{
	using namespace Vec;

	synthCompressor::synthCompressor()
	{
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[kCompressorSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kCompressorRatio].Init(this, "Ratio", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kCompressorRatio].SetStaticValue(3.f);
		m_Inputs[kCompressorThreshold].Init(this, "Threshold", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kCompressorThreshold].SetStaticValue(0.707f);
		m_Inputs[kCompressorAttack].Init(this, "Attack", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kCompressorAttack].SetStaticValue(0.1f);
		m_Inputs[kCompressorRelease].Init(this, "Release", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kCompressorRelease].SetStaticValue(0.1f);

		m_State = V4VConstant(V_ZERO);
	}

	void synthCompressor::ResetProcessing()
	{
		m_State = V4VConstant(V_ZERO);
	}
	void synthCompressor::Synthesize()
	{		
		f32 *outputBuffer = m_Outputs[0].GetDataBuffer()->GetBuffer();

		if(m_Inputs[kCompressorSignal].GetDataState() != synthPin::SYNTH_PIN_DYNAMIC)
		{
			sysMemZeroBytes<sizeof(float) * kMixBufNumSamples>(outputBuffer);
			return;
		}
		
		synthUtil::CopyBuffer(m_Buffer.GetBufferV(), m_Inputs[kCompressorSignal].GetDataBuffer()->GetBufferV(), m_Buffer.GetSize());

		Process(m_Buffer.GetBuffer(), 
					m_Inputs[kCompressorThreshold].GetNormalizedValue(0),
					m_Inputs[kCompressorRatio].GetNormalizedValue(0),
					m_Inputs[kCompressorAttack].GetNormalizedValue(0),
					m_Inputs[kCompressorRelease].GetNormalizedValue(0),
					m_State, 
					m_Buffer.GetSize());
		
	}	

	void synthCompressor::Process(float *inOutPtr, const float threshold, const float ratio, const float attackInput, const float releaseInput, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		float gn1 = GetX(state);

		const float slope = Max(SMALL_FLOAT, 1.f - (1.f / Max(ratio, SMALL_FLOAT)));
		const float thresholdLin = Max(threshold, SMALL_FLOAT);
		const float invSlopedThreshold = 1.f / Powf(thresholdLin, slope);

		const float attackTime = Max(attackInput, SMALL_FLOAT);
		const float attack = 1.0f - exp( -2.2f * (1.0f / (f32)kMixerNativeSampleRate) / attackTime );

		const float releaseTime = Max(releaseInput, SMALL_FLOAT);
		const float release = 1.0f - exp( -2.2f * (1.0f / (f32)kMixerNativeSampleRate) / releaseTime );

		
		// Loop through all the samples
		for(u32 j = 0; j < numSamples; j++)
		{
			float xdn = Abs(inOutPtr[j]);
			xdn = Max(xdn, 1.0E-14f);
			float fn = Max(invSlopedThreshold * Powf(xdn, slope), 1.0f); //Don't expand.

			float a = Selectf(fn - gn1, attack, release);
			float gn = ((fn - gn1) * a) + gn1;

			// Output
			inOutPtr[j] = 1.f / gn;

			gn1 = gn;
		}

		SetX(state, gn1);
	}
}


