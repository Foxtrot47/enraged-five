// 
// audiosynth/clipper.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_CLIPPER_H
#define SYNTH_CLIPPER_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum synthClipperInputs
	{
		kClipperSignal= 0,
		kClipperThreshold,
		
		kClipperNumInputs
	};

	class synthClipper : public synthModuleBase<kClipperNumInputs, 1, SYNTH_CLIPPER>
	{
	public:
		SYNTH_MODULE_NAME("Clipper");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthClipper();
		
		virtual void SerializeState(synthModuleSerializer *serializer);

		virtual void Synthesize();

		
		enum synthClipperMode
		{
			HARD_CLIP = 0,
			SOFT_CLIP_POLY,
		};

		void SetMode(const synthClipperMode mode)
		{
			m_Mode = mode;
		}
		synthClipperMode GetMode() const { return m_Mode; }

		f32 ComputeValue(const f32 inputValue) const;

		static void HardClip(float *RESTRICT inOutBuffer, const float *RESTRICT thresholdBuffer, const u32 numSamples);
		static void HardClip(float *RESTRICT inOutBuffer, Vec::Vector_4V_In threshold, const u32 numSamples);
		static float HardClip(const float input, const float threshold);

		static void SoftClip(float *RESTRICT inOutBuffer, const float *RESTRICT thresholdBuffer, const u32 numSamples);
		static void SoftClip(float *RESTRICT inOutBuffer, Vec::Vector_4V_In threshold, const u32 numSamples);
		static float SoftClip(const float input, const float threshold);

	private:

		synthSampleFrame m_Buffer;
		synthClipperMode m_Mode;
	};
}
#endif // !SYNTH_MINIMAL_MODULES
#endif // SYNTH_CLIPPER_H


