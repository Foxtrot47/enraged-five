// 
// audiosynth/switch.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_SWITCH_H
#define SYNTH_SWITCH_H

#include "sampleframe.h"
#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "atl/array.h"
#include "math/amath.h"

namespace rage
{
	class synthSwitch : public synthModuleBase<17,1,SYNTH_SWITCH>
	{
	public:
		SYNTH_MODULE_NAME("Switch");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthSwitch();
		
		virtual void GetInputs(synthPin *&pins, u32 &numPins);
		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();


		enum { kMaxSwitchValues = 16};
		CompileTimeAssert(NumInputs == kMaxSwitchValues + 1);

		u32 GetNumSwitchValues() const { return m_NumSwitchValues; }
		void SetNumSwitchValues(const u32 numValues) { m_NumSwitchValues = Clamp(numValues, 1U, (u32)kMaxSwitchValues); }

		enum synthSwitchMode
		{
			kRescaledInput = 0,
			kNormalizedInput,
			kInterpInput,
			kEqualPowerXFade,
		};
	
		synthSwitchMode GetMode() const
		{
			return m_Mode;
		}

		void SetMode(const synthSwitchMode mode) { m_Mode = mode; }

		u32 ComputeActiveInputIndex() const;

		static void Process_Norm(float *inOut, const atFixedArray<const float *, 16> &otherInputs, const float t, const u32 numSamples);
		static void Process_Index(float *inOut, const atFixedArray<const float *, 16> &otherInputs, const float t, const u32 numSamples);
		static void Process_Lerp(float *inOut, const atFixedArray<const float *, 16> &otherInputs, const float t, const u32 numSamples);
		static void Process_EqualPower(float *inOut, const atFixedArray<const float *, 16> &otherInputs, const float t, const u32 numSamples);

		static float Process_Norm(float t, const atFixedArray<float, 16> &inputs);
		static float Process_Index(float t, const atFixedArray<float, 16> &inputs);
		static float Process_Lerp(float t, const atFixedArray<float, 16> &inputs);
		static float Process_EqualPower(float t, const atFixedArray<float, 16> &inputs);


	private:

		synthSampleFrame m_Buffer;

		atRangeArray<synthSampleFrame, 16> m_InputBuffers;


		u32 m_NumSwitchValues;
		synthSwitchMode m_Mode;
	};
}
#endif
#endif // SYNTH_SWITCH_H



