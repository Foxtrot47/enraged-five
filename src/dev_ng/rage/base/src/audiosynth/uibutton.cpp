#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uibutton.h"
#include "grmodel/shader.h"
#include "vector/geometry.h"
#include "uiplane.h"

namespace rage
{

	synthUIButton::synthUIButton()
        : m_Size(0.2f)
        , m_EdgeColour(0.f,0.2f,1.f,1.f)
        , m_IsFixedSize(false)
	{
		AddComponent(&m_Label);
	}

	synthUIButton::~synthUIButton()
	{

	}

	void synthUIButton::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
        Vector3 halfSizeVec;
        if(m_IsFixedSize)
        {
            halfSizeVec = m_FixedHalfSize;
        }
        else
        {
            // Auto size based on label
            float width, height;
            m_Label.ComputeWidthAndHeight(width,height);
            halfSizeVec = Vector3(width * 0.7f, height*0.6f, 0.f);        
        }
        Matrix34 mat = GetMatrix();
        v1 = mat.d - halfSizeVec;
        v2 = mat.d + halfSizeVec;
	}

	void synthUIButton::Update()
	{
		m_Label.SetMatrix(GetMatrix());
		m_Label.SetAlpha(GetAlpha());
		m_Label.SetString(m_Title);
        m_Label.SetSize(m_Size);

		synthUIView::Update();

        if(IsUnderMouse())
        {
            m_Label.SetUnderMouse(true);
        }
	}

	void synthUIButton::Render()
	{
		Matrix34 mat = GetMatrix();

        if(m_IsFixedSize)
        {
            Vector3 scalingVec = m_FixedHalfSize;
            mat.Scale(scalingVec);
        }
        else
        {
            // Note: we want the half-size in scalingVec, so 0.7 / 0.6 is adding a margin
            Vector3 scalingVec = Vector3(m_Label.ComputeWidth()*0.7f,m_Label.ComputeHeight()*0.6f,1.f);
            mat.Scale(scalingVec);
        }
        
		grcWorldMtx(mat);

		const bool isHighlighted = (IsUnderMouse() || m_Label.IsUnderMouse());
		SetShaderVar("g_HighlightScale", isHighlighted ? 1.f : 0.f);
		SetShaderVar("g_AlphaFade", m_Alpha);
		SetShaderVar("g_EdgeHighlightColour", m_EdgeColour);

		DrawVB("drawmenu");

		synthUIView::Render();
	}

	bool synthUIButton::HadClick() const
	{
		return IsUnderMouse() && synthUIPlane::GetMaster()->HadLeftClick();
	}
}
#endif // __SYNTH_EDITOR
