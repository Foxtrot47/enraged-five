
#ifndef SYNTH_EXP_H
#define SYNTH_EXP_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{

	enum synthLinToExpInputs
	{
		kLinearToExponentialSignal = 0,
		kLinearToExponentialExponent,
		
		kLinearToExponentialNumInputs
	};

	class synthLinearToExponential : public synthModuleBase<kLinearToExponentialNumInputs, 1, SYNTH_EXPONENTIAL>
	{
	public:
		SYNTH_MODULE_NAME("Exp");

		synthLinearToExponential();
		
		virtual void Synthesize();

	private:
		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_EXP_H


