// 
// audiosynth/power.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_POWER_H
#define SYNTH_POWER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthPower : public synthModuleBase<2,1,SYNTH_POWER>
	{
	public:

		synthPower();
		virtual ~synthPower(){};

		virtual const char *GetName() const
		{
			return "Power";
		}

		virtual void Synthesize();

		static void Process(float *RESTRICT const inOutBuffer, const float *RESTRICT const expBuffer, const u32 numSamples);
		static void Process(float *const inOutBuffer, Vec::Vector_4V_In exp, const u32 numSamples);
		static float Process(const float x, const float y);

	private:

		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_POWER_H


