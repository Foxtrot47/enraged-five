// 
// audiosynth/uiview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UIVIEW_H
#define SYNTH_UIVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "atl/array.h"

namespace rage
{
    class synthUIViewGroup;

// PURPOSE
//	Abstract UI container
class synthUIView : public synthUIComponent
{
	public:
		synthUIView() 
            : m_MouseHoverFade(0.f)
            , m_GroupId(-1)
			, m_Group(NULL)
        {

        }
		virtual ~synthUIView();

		virtual void Update();
        virtual void PerformLayout() { }

		virtual bool StartDrag() const = 0;
		virtual bool IsActive() const = 0;
        virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		virtual bool IsGroup() const { return false; }
		virtual bool IsModule() const { return false; }

		f32 GetHoverFade() const { return m_MouseHoverFade; }

        virtual void DeleteContents() {}
        virtual synthUIView *DuplicateContents(const bool UNUSED_PARAM(recreateConnections)) { return NULL; }


        bool IsGrouped() const{return (m_Group != NULL);}
        synthUIViewGroup *GetGroup() const;
		void AddToGroup(synthUIViewGroup *group);
        void RemoveFromGroup(const synthUIViewGroup *ASSERT_ONLY(group));
		bool IsInGroup(const synthUIViewGroup *group) const { return GetGroup() == group; }

		void SetGroupId(const s32 groupId) { m_GroupId = groupId; }
		s32 GetGroupId() const { return m_GroupId; }

        virtual float GetProcessingTime() const { return 0.f; }
		
	protected:
		virtual void Render();

		void AddComponent(synthUIComponent *component)
		{
			m_Components.PushAndGrow(component);
		}

		void RemoveComponent(synthUIComponent *component)
		{
			m_Components.DeleteMatches(component);
		}

		virtual bool DisableInteraction() { return false; }

		const atArray<synthUIComponent*> &GetComponents_const() const { return m_Components; }
		atArray<synthUIComponent*> &GetComponents() { return m_Components; }

private:

		atArray<synthUIComponent*> m_Components;
		f32 m_MouseHoverFade;
        s32 m_GroupId;

		synthUIViewGroup *m_Group;
};

} // namespace rage
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIVIEW_H
