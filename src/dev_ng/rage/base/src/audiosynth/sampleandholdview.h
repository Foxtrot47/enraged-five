// 
// audiosynth/sampleandholdview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SAMPLEANDHOLDVIEW_H
#define SYNTH_SAMPLEANDHOLDVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthSampleAndHold;
	class synthSampleAndHoldView : public synthModuleView
	{
	public:

		synthSampleAndHoldView(synthModule *module);
		virtual void Update();

	protected:

		virtual bool StartDrag() const;

	private:

		synthSampleAndHold *GetSampleAndHold() const { return (synthSampleAndHold*)m_Module; }
		synthUIDropDownList m_Mode;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_SAMPLEANDHOLDVIEW_H

