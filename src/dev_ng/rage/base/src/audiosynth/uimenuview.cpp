#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uimenuview.h"
#include "uimenuitem.h"
#include "uiplane.h"
#include "input/mouse.h"

namespace rage
{

	synthUIMenuView::synthUIMenuView()
	{
		ClearSelection();
	}

	synthUIMenuView::~synthUIMenuView()
	{
		for(atArray<synthUIMenuItem*>::iterator iter = m_Items.begin(); iter != m_Items.end(); iter++)
		{
			delete *iter;
		}
	}

	void synthUIMenuView::ClearSelection()
	{
		m_HasSelection = false;
		m_SelectedTag = ~0U;
		m_ActiveSubMenu = NULL;
		for(atArray<synthUIMenuItem*>::iterator iter = m_Items.begin(); iter != m_Items.end(); iter++)
		{
			if((*iter)->GetSubMenu() != NULL)
			{
				(*iter)->GetSubMenu()->ClearSelection();
			}
		}

	}
	void synthUIMenuView::Update()
	{
		s32 numItems = m_Items.size();
		if(numItems > 0)
		{
			// ensure we have a valid matrix in order to compute the item height
			m_Items[0]->SetMatrix(GetMatrix());
			const f32 itemHeight = m_Items[0]->ComputeHeight();
			const f32 halfHeight = itemHeight * (f32)numItems * 0.5f;

			Vector3 pos = GetMatrix().d;
			pos += halfHeight * GetMatrix().b;
			Matrix34 mat = GetMatrix();
			mat.d = pos;
			for(s32 i = 0; i < numItems; i++)
			{
				m_Items[i]->SetMatrix(mat);
				mat.d -= itemHeight * mat.b;

				if(m_Items[i]->GetSubMenu())
				{
					Matrix34 mat = m_Items[i]->GetMatrix();
					mat.d -= mat.a * (m_Items[i]->ComputeWidth() + m_Items[i]->GetSubMenu()->ComputeWidth()) * 0.5f;
					mat.d -= mat.b * (m_Items[i]->ComputeHeight() * 0.5f);
					m_Items[i]->GetSubMenu()->SetMatrix(mat);
				}
			}
		}
		synthUIView::Update();

		// collision test active submenu
		const bool hadMouseClick = ioMouse::GetReleasedButtons()&ioMouse::MOUSE_LEFT;

		bool isAnythingUnderMouse = false;
		for(atArray<synthUIMenuItem*>::iterator iter = m_Items.begin(); iter != m_Items.end(); iter++)
		{
			if((*iter)->IsUnderMouse() && !isAnythingUnderMouse)
			{
				m_SelectedTag = (*iter)->GetTag();
				if((*iter)->GetSubMenu())
				{
					(*iter)->GetSubMenu()->SetShouldDraw(ShouldDraw());
					m_ActiveSubMenu = (*iter)->GetSubMenu();
				}
				else if(hadMouseClick)
				{
					m_HasSelection = true;
				}
				isAnythingUnderMouse = true;
			}
			else if((*iter)->GetSubMenu() && (*iter)->GetSubMenu() != m_ActiveSubMenu)
			{
				(*iter)->GetSubMenu()->SetShouldDraw(false);
			}
		}

		if(m_ActiveSubMenu)
		{
			m_ActiveSubMenu->SetUnderMouse(IsUnderMouse());

			if(m_ActiveSubMenu->HasSelection())
			{
				m_HasSelection = true;
				m_SelectedTag = m_ActiveSubMenu->GetSelectedTag();
				m_ActiveSubMenu->SetShouldDraw(false);
				m_ActiveSubMenu = NULL;
			}
		}
		
	}

	void synthUIMenuView::SetTextSize(const f32 size)
	{
		for(atArray<synthUIMenuItem*>::iterator iter = m_Items.begin(); iter != m_Items.end(); iter++)
		{
			(*iter)->SetTextSize(size);
		}
		if(m_ActiveSubMenu)
		{
			m_ActiveSubMenu->SetTextSize(size);
		}
	}

	void synthUIMenuView::SortAlphabetically()
	{
		m_Items.QSort(0,-1,&synthUIMenuItem::CompareAlphabetically);
	}

	void synthUIMenuView::ResetItems()
	{
		for(atArray<synthUIMenuItem*>::iterator iter = m_Items.begin(); iter != m_Items.end(); iter++)
		{
			RemoveComponent(*iter);
		}
		m_Items.Reset();
	}

	void synthUIMenuView::Render()
	{
		synthUIView::Render();
	}

	void synthUIMenuView::AddItem(const char *title, u32 tag)
	{
		synthUIMenuItem *menuItem = rage_new synthUIMenuItem();
		menuItem->SetTitle(title);
		menuItem->SetTag(tag);

		m_Items.PushAndGrow(menuItem);
		AddComponent(menuItem);
	}

	void synthUIMenuView::SetTitleForTag(const u32 tag, const char *title)
	{
		for(atArray<synthUIMenuItem*>::iterator iter = m_Items.begin(); iter != m_Items.end(); iter++)
		{
			if((*iter)->GetTag() == tag)
			{
				(*iter)->SetTitle(title);
				break;
			}
		}
	}

	void synthUIMenuView::AddSubMenu(const char *title, synthUIMenuView *subMenu, Color32 color)
	{
		synthUIMenuItem *menuItem = rage_new synthUIMenuItem();
		menuItem->SetTitle(title);
		menuItem->SetSubMenu(subMenu);
		menuItem->SetEdgeColour(color);
		
		for(atArray<synthUIMenuItem*>::iterator iter = subMenu->m_Items.begin(); iter != subMenu->m_Items.end(); iter++)
		{
			(*iter)->SetEdgeColour(color);
		}
		
		m_Items.PushAndGrow(menuItem);
		AddComponent(menuItem);
		//AddComponent(subMenu);
	}

	void synthUIMenuView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		v1.Set(LARGE_FLOAT,LARGE_FLOAT,LARGE_FLOAT);
		v2 = -v1;
		for(atArray<synthUIMenuItem*>::const_iterator iter = m_Items.begin(); iter != m_Items.end(); iter++)
		{
			Vector3 t1,t2;
			(*iter)->GetBounds(t1,t2);
			
			v1.Min(v1,t1);
			v2.Max(v2,t2);

			if((*iter)->GetSubMenu() && (*iter)->GetSubMenu()->ShouldDraw())
			{
				Vector3 v3,v4;
				(*iter)->GetSubMenu()->GetBounds(v3,v4);
				v1.Min(v1,v3);
				v2.Max(v2,v4);
			}
		}
	}
}
#endif // __SYNTH_EDITOR
