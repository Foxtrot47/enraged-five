// 
// audiosynth/decimator.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_DECIMATOR_H
#define SYNTH_DECIMATOR_H
#include "synthdefs.h"

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"
#include "vectormath/vectormath.h"

namespace rage
{
	enum synthDecimatorInputs
	{
		kDecimatorSignal = 0,
		kDecimatorBitDepth,
		kDecimatorSampleRate,
	
		kDecimatorNumInputs
	};

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
	class synthDecimator : public synthModuleBase<kDecimatorNumInputs, 1, SYNTH_DECIMATOR>
	{
	public:
		SYNTH_MODULE_NAME("Decimator");

		synthDecimator();
		
		virtual void Synthesize();
	
		static void Process(float *inOut, const float bitDepth, const float sampleRate, Vec::Vector_4V_InOut state, const u32 numSamples);

	private:
		Vec::Vector_4V m_State;
		synthSampleFrame m_Buffer;
		
	};
}
#if __WIN32
#pragma warning(pop)
#endif

#endif // SYNTH_DECIMATOR_H


