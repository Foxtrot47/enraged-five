// 
// audiosynth/uilabel.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UILABEL_H
#define SYNTH_UILABEL_H

#include "synthdefs.h"

#if __SYNTH_EDITOR


#include "uicomponent.h"
#include "uiview.h"

#include "grcore/im.h"
#include "vector/vector3.h"
#include "vector/matrix34.h"

namespace rage
{
	class grcTexture;
	struct grcVertexDeclaration;
	class grcVertexBuffer;
	class grcIndexBuffer;
	class grmShader;

	class synthUILabel : public synthUIComponent
	{
	public:

		enum { kDefaultMaxWidth = 300 };
		synthUILabel(const s32 maxWidth = kDefaultMaxWidth);
		virtual ~synthUILabel();

		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		void SetString(const char *string);
		const char *GetString() const { return m_String; }

		void SetSize(const f32 size)
		{
			m_Scaling = size;
		}

		static bool InitClass();
		static void ShutdownClass();
		void RenderString(const char *str, f32 &horizontalScaling, const bool calculateWidthOnly);

		static u32 GetNumTexturesAllocated() { return sm_NumTexturesAllocated; }

		void SetDrawCaret(const bool draw) 
		{ 
			m_NeedRender |= (draw != m_DrawCaret); 
			m_DrawCaret = draw; 
		}
		void SetCaretPosition(const s32 characterIndex) 
		{
			m_NeedRender |= (m_CaretIndex != characterIndex);
			m_CaretIndex = characterIndex; 
		}

		void SetShouldAlwaysDraw(const bool should) { m_AlwaysDraw = should; }

		void SetActiveTextColour(const Color32 textColour)
		{
			m_ActiveTextColour = textColour;
		}

		void SetActiveShadowColour(const Color32 textColour)
		{
			m_ActiveShadowColour = textColour;
		}

		void SetInactiveTextColour(const Color32 textColour)
		{
			m_InactiveTextColour = textColour;
		}

		void SetInactiveShadowColour(const Color32 textColour)
		{
			m_InactiveShadowColour = textColour;
		}


	protected:
		virtual void Render();

		Color32 m_ActiveTextColour;
		Color32 m_InactiveTextColour;
		Color32 m_ActiveShadowColour;
		Color32 m_InactiveShadowColour;

		grcTexture *m_Texture;
		const char *m_String;
		f32 m_HorizontalScaling;
		f32 m_Scaling;
		f32 m_WidthScalar;
		u32 m_StringHash;
		u32 m_LastTimeRendered;

		s32 m_MaxWidth;

		s32 m_CaretIndex;
		bool m_DrawCaret;

		bool m_NeedRender;
		bool m_AlwaysDraw;

		static grcVertexDeclaration *sm_VertexDecl;
		static grcVertexBuffer *sm_VertexBuffer;
		static grcIndexBuffer *sm_IndexBuffer;
		static grmShader *sm_Shader;
		static u32 sm_NumTexturesAllocated;

	};

    class synthUILabelView : public synthUIView
    {
    public:
        synthUILabelView(const s32 maxWidth = synthUILabel::kDefaultMaxWidth)
            : m_Label(maxWidth)
        {
            AddComponent(&m_Label);
        }

        void Update()
        {
            m_Label.SetMatrix(GetMatrix());
            synthUIView::Update();
        }

        void GetBounds(Vector3 &v1, Vector3 &v2) const
        {
            m_Label.GetBounds(v1, v2);
        }

        bool StartDrag() const
        {
            return false;
        }

        bool IsActive() const
        {
            return IsUnderMouse();
        }

        void SetString(const char *string)
        {
            m_Label.SetString(string);
        }
        
        const char *GetString() const 
        { 
            return m_Label.GetString(); 
        }

        void SetSize(const f32 size)
        {
            m_Label.SetSize(size);
        }

        void SetDrawCaret(const bool draw) 
        { 
            m_Label.SetDrawCaret(draw);
        }
        void SetCaretPosition(const s32 characterIndex) 
        {
            m_Label.SetCaretPosition(characterIndex);
        }

        void SetShouldAlwaysDraw(const bool should) 
        { 
            m_Label.SetShouldAlwaysDraw(should);
        }

		synthUILabel &GetLabel() { return m_Label; }
    private:
        synthUILabel m_Label;
    };
}// namespace rage
#endif // __SYNTH_EDITOR
#endif // SYNTH_UILABEL_H
