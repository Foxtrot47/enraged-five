
#ifndef SYNTH_INPUTSMOOTHER_H
#define SYNTH_INPUTSMOOTHER_H

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthVariableInput : public synthModuleBase<1,1,SYNTH_VARIABLEINPUT>
	{
	public:
		SYNTH_MODULE_NAME("Variable In");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthVariableInput();
		~synthVariableInput();
		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();

		void SetExportedKey(const char *keyName);
		u32 GetExportedKey() const { return m_ExportedKey; }

		void SetValue(const f32 val);

#if __SYNTH_EDITOR
		const char *GetExportedKeyName() const { return m_ExportedKeyName; }
#endif

	private:

		u32 m_ExportedKey;

		SYNTH_EDITOR_ONLY(const char *m_ExportedKeyName);
	};
}

#endif // SYNTH_INPUTSMOOTHER_H


