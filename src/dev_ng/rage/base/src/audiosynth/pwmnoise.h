// 
// audiosynth/pwmnoise.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_PWMNOISE_H
#define SYNTH_PWMNOISE_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum synthPwmNoiseInputs
	{
		kPwmNoisePulseWidth = 0,
		kPwmNoisePulseAmplitude,
		kPwmNoisePulseRange,
		kPwmNoiseGain,
	
		kPwmNoiseNumInputs
	};

	class synthPwmNoise : public synthModuleBase<kPwmNoiseNumInputs, 1, SYNTH_PWMNOISEGENERATOR>
	{
	public:
		SYNTH_MODULE_NAME("PWMNoise");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthPwmNoise();

		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();

		void SetMaxPulseWidth(const s32 maxPulseWidth)
		{
			m_MaxPulseWidth = maxPulseWidth;
		}

		s32 GetMaxPulseWidth() const{return m_MaxPulseWidth;}
		void SetMinPulseWidth(const s32 minPulseWidth)
		{
			m_MinPulseWidth = minPulseWidth;
		}
		s32 GetMinPulseWidth() const{return m_MinPulseWidth;}

	private:

		s32 m_MaxPulseWidth, m_MinPulseWidth;
		
		f32 m_PulseAmplitude;
		s32 m_PulseSamples;
		bool m_OnOff;

		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_PWMNOISE_H
