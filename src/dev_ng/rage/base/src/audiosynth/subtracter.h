// 
// audiosynth/subtracter.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SUBTRACTER_H
#define SYNTH_SUBTRACTER_H

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthSubtracter : public synthModuleBase<2,1,SYNTH_SUBTRACTER>
	{
	public:

		synthSubtracter();
		virtual ~synthSubtracter(){};

		virtual const char *GetName() const
		{
			return "Subtract";
		}

		virtual void Synthesize();

	private:

		synthSampleFrame m_Buffer;
	};
}

#endif // SYNTH_SUBTRACTER_H


