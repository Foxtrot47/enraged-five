// 
// audiosynth/uicurveview.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UICURVEVIEW_H
#define SYNTH_UICURVEVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uiview.h"
#include "uilabel.h"
#include "vector/color32.h"
#include "vector/vector3.h"

namespace rage
{
	class grcTexture;
	class synthUICurveViewDataProvider
	{
	public:
		virtual void Evaluate(float *destBuffer, const u32 numSamples) const = 0;
	};

	class synthUICurveView : public synthUIView
	{
	public:
		synthUICurveView();
		virtual ~synthUICurveView();

		void SetProvider(const synthUICurveViewDataProvider *provider)
		{
			m_Provider = provider;
		}

		// synthUIComponent functionality
		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;
		
		virtual bool StartDrag() const { return false; } 
		virtual bool IsActive() const { return IsUnderMouse(); }

		void RefreshView()
		{
			m_NeedRefresh = true;
		}

		void SetAlwaysRefresh(const bool alwaysRefresh)
		{
			m_AlwaysRefresh = alwaysRefresh;
		}

		void SetGridColor(const Color32 color)
		{
			m_GridColor = color;
		}

		void SetDrawGrid(const bool shouldDrawGrid)
		{
			m_DrawGrid = shouldDrawGrid;
		}

		void SetGridSize(const s32 numVerticalSlices, const s32 numHorizontalSlices)
		{
			m_GridVerticalSlices = numVerticalSlices;
			m_GridHorizontalSlices = numHorizontalSlices;
		}

		void SetCurveColor(const Color32 curveColor)
		{
			m_CurveColor = curveColor;
		}

	protected:
		
		virtual void Render();

	private:

		void RasterizeCurve();
		void DrawGrid(const s32 numVertical, const s32 numHorizontal, const Color32 color) const;

		synthUILabel m_Label;

		grcTexture *m_Texture;
		const synthUICurveViewDataProvider *m_Provider;
		Color32 m_GridColor;
		Color32 m_CurveColor;
		s32 m_GridVerticalSlices;
		s32 m_GridHorizontalSlices;
		bool m_NeedRefresh;
		bool m_AlwaysRefresh;
		bool m_DrawGrid;
	};

} // namespace rage
#endif // __SYNTH_EDITOR
#endif // SYNTH_UICURVEVIEW_H
