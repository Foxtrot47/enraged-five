// 
// synthcoredisasm.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK

#include "synthcoredisasm.h"
#include "synthopcodes.h"
#include "synthesizer.h"

#include "audiohardware/channel.h"
#include "file/stream.h"
#include "file/token.h"

#include "audioengine/spuutil.h"

namespace rage
{

struct synthOpcodeState
{
	const char *opName;
	u32 numStateBlocks;
	bool isReplacing;
};

#define OP_DEFAULT(x) {#x, 0, false}
#define OP_REPL(x) {#x, 0, true}
#define OP_REPL_1SB(x) {#x,1,true}
#define OP_1SB(x) {#x,1,false}

const synthOpcodeState g_OpcodeInfo[] = {
	OP_DEFAULT(COPY_BUFFER),
	OP_DEFAULT(COPY_SCALAR),

	OP_REPL(CONVERT_BUFFER_TO_SIGNAL),
	OP_DEFAULT(CONVERT_SCALAR_TO_SIGNAL),
	OP_REPL(CONVERT_BUFFER_TO_NORMALIZED),
	OP_DEFAULT(CONVERT_SCALAR_TO_NORMALIZED),

	OP_DEFAULT(SAMPLE_BUFFER),

	OP_REPL(MULTIPLY_BUFFER_BUFFER),
	OP_REPL(MULTIPLY_BUFFER_SCALAR),
	OP_DEFAULT(MULTIPLY_SCALAR_SCALAR),

	OP_REPL(SUM_BUFFER_BUFFER),
	OP_REPL(SUM_BUFFER_SCALAR),
	OP_DEFAULT(SUM_SCALAR_SCALAR),

	OP_REPL(SUBTRACT_BUFFER_BUFFER),
	OP_REPL(SUBTRACT_BUFFER_SCALAR),
	OP_DEFAULT(SUBTRACT_SCALAR_BUFFER),
	OP_DEFAULT(SUBTRACT_SCALAR_SCALAR),

	OP_REPL(DIVIDE_BUFFER_BUFFER),
	OP_REPL(DIVIDE_BUFFER_SCALAR),
	OP_DEFAULT(DIVIDE_SCALAR_SCALAR),

	OP_REPL(RESCALE_BUFFER_BUFFER),
	OP_REPL(RESCALE_BUFFER_SCALAR),
	OP_DEFAULT(RESCALE_SCALAR),

	OP_REPL(HARDKNEE_BUFFER),
	OP_DEFAULT(HARDKNEE_SCALAR),

	OP_DEFAULT(NOISE),
	OP_1SB(RANDOM),

	OP_REPL(ABS_BUFFER),
	OP_DEFAULT(ABS_SCALAR),
	OP_REPL(FLOOR_BUFFER),
	OP_DEFAULT(FLOOR_SCALAR),
	OP_REPL(CEIL_BUFFER),
	OP_DEFAULT(CEIL_SCALAR),
	OP_REPL(ROUND_BUFFER),
	OP_DEFAULT(ROUND_SCALAR),
	OP_REPL(SIGN_BUFFER),
	OP_DEFAULT(SIGN_SCALAR),
	OP_REPL(MODULUS_BUFFER),
	OP_DEFAULT(MODULUS_SCALAR),
	OP_DEFAULT(POWER_SCALAR),
	OP_REPL(POWER_BUFFER),
	OP_1SB(MAX_SCALAR),
	OP_REPL_1SB(MAX_BUFFER),
	OP_REPL_1SB(COMPRESSOR_EG),
	OP_DEFAULT(UNUSED_1),

	OP_REPL(LERP_BUFFER),
	OP_DEFAULT(LERP_BUFFER_BUFFER),
	OP_DEFAULT(LERP_SCALAR),

	OP_REPL(HARD_CLIP_BUFFER_BUFFER),
	OP_REPL(HARD_CLIP_BUFFER_SCALAR),
	OP_DEFAULT(HARD_CLIP_SCALAR_SCALAR),
	OP_REPL(SOFT_CLIP_BUFFER_BUFFER),
	OP_REPL(SOFT_CLIP_BUFFER_SCALAR),
	OP_DEFAULT(SOFT_CLIP_SCALAR_SCALAR),

	OP_REPL_1SB(ENVELOPE_FOLLOWER_BUFFER),
	OP_1SB(ENVELOPE_FOLLOWER_SCALAR),

	OP_DEFAULT(BiquadCoefficients_LowPass2Pole),
	OP_DEFAULT(BiquadCoefficients_HighPass2Pole),
	OP_DEFAULT(BiquadCoefficients_BandPass),
	OP_DEFAULT(BiquadCoefficients_BandStop),
	OP_DEFAULT(BiquadCoefficients_LowPass4Pole),
	OP_DEFAULT(BiquadCoefficients_HighPass4Pole),
	OP_DEFAULT(BiquadCoefficients_PeakingEQ),

	OP_REPL_1SB(BiquadProcess_2Pole),
	OP_REPL_1SB(BiquadProcess_4Pole),

	OP_REPL_1SB(OnePole_LPF_BUFFER_BUFFER),
	OP_REPL_1SB(OnePole_LPF_BUFFER_SCALAR),
	OP_1SB(OnePole_LPF_SCALAR),
	OP_REPL_1SB(OnePole_HPF_BUFFER_BUFFER),
	OP_REPL_1SB(OnePole_HPF_BUFFER_SCALAR),
	OP_1SB(OnePole_HPF_SCALAR),

	OP_REPL_1SB(OSC_RAMP_BUFFER_BUFFER),
	OP_1SB(OSC_RAMP_BUFFER_SCALAR),
	OP_1SB(OSC_RAMP_SCALAR),

	OP_REPL(SINE_BUFFER),
	OP_DEFAULT(SINE_SCALAR),
	OP_REPL(COS_BUFFER),
	OP_DEFAULT(COS_SCALAR),
	OP_REPL(TRI_BUFFER),
	OP_DEFAULT(TRI_SCALAR),
	OP_REPL(SQUARE_BUFFER),
	OP_DEFAULT(SQUARE_SCALAR), 
	OP_REPL(SAW_BUFFER),
	OP_DEFAULT(SAW_SCALAR),

	OP_1SB(TRIGGER_LATCH),

	OP_1SB(ENV_GEN_LIN_INT_BUFFER),
	OP_1SB(ENV_GEN_LIN_OS_BUFFER),
	OP_1SB(ENV_GEN_LIN_RET_BUFFER),
	OP_1SB(ENV_GEN_EXP_INT_BUFFER),
	OP_1SB(ENV_GEN_EXP_OS_BUFFER),
	OP_1SB(ENV_GEN_EXP_RET_BUFFER),

	OP_1SB(TIMED_TRIGGER_INT),
	OP_1SB(TIMED_TRIGGER_OS),
	OP_1SB(TIMED_TRIGGER_RET),

	OP_DEFAULT(READ_VARIABLE),
	OP_DEFAULT(FINISHED_TRIGGER),

	OP_DEFAULT(READ_INPUT_0),
	OP_DEFAULT(READ_INPUT_1),
	OP_DEFAULT(READ_INPUT_2),
	OP_DEFAULT(READ_INPUT_3),
	OP_DEFAULT(READ_INPUT_4),
	OP_DEFAULT(READ_INPUT_5),
	OP_DEFAULT(READ_INPUT_6),
	OP_DEFAULT(READ_INPUT_7),

	OP_DEFAULT(NOTE_TO_FREQ_SCALAR),
	OP_REPL(NOTE_TO_FREQ_BUFFER),
	OP_1SB(SAH_STATIC_SCALAR),
	OP_REPL_1SB(DECIMATE),
	OP_1SB(COUNTER),
	OP_1SB(COUNTER_TRIGGER),

	OP_REPL(GATE_BUFFER_BUFFER),
	OP_REPL(GATE_BUFFER_SCALAR),
	OP_DEFAULT(GATE_SCALAR_SCALAR),

	OP_REPL_1SB(SMALLDELAY_FRAC),
	OP_REPL_1SB(SMALLDELAY_STATIC),
	OP_REPL_1SB(SMALLDELAY_FRAC_FB),	
	OP_REPL_1SB(SMALLDELAY_STATIC_FB),

	OP_1SB(TRIGGER_DIFF),
	OP_1SB(RANDOMIZE_ONINIT),
	OP_DEFAULT(HOLD_SAMPLE),
	OP_REPL_1SB(AW_FILTER),
	OP_REPL(LERP_THREE_BUFFERS),

	OP_DEFAULT(BiquadCoefficients_LowShelf2Pole),
	OP_DEFAULT(BiquadCoefficients_LowShelf4Pole),
	OP_DEFAULT(BiquadCoefficients_HighShelf2Pole),
	OP_DEFAULT(BiquadCoefficients_HighShelf4Pole),

	OP_REPL(Switch_Norm_Buffer),
	OP_REPL(Switch_Index_Buffer),
	OP_REPL(Switch_Lerp_Buffer),
	OP_REPL(Switch_EqualPower_Buffer),
	
	OP_DEFAULT(Switch_Norm_Scalar),
	OP_DEFAULT(Switch_Index_Scalar),
	OP_DEFAULT(Switch_Lerp_Scalar),
	OP_DEFAULT(Switch_EqualPower_Scalar),
	
	OP_REPL_1SB(Allpass_Static),
	OP_REPL_1SB(Allpass_Buffer),
};

atRangeArray<synthCoreDisasm::synthCoreTimings, synthOpcodes::NUM_OP_CODES> synthCoreDisasm::sm_OpCodeTimings;

void synthCoreDisasm::InitClass()
{
	sysMemZeroBytes<sizeof(sm_OpCodeTimings)>(&sm_OpCodeTimings);

#if __SYNTH_EDITOR
	// Try to load opcode timings
	fiStream *xbox360Stream = ASSET.Open("audio:/synthtimings_360.csv", "");
	if(!xbox360Stream)
		return;
	fiStream *ps3Stream = ASSET.Open("audio:/synthtimings_ps3.csv", "");
	if(!ps3Stream)
	{
		xbox360Stream->Close();
		return;
	}


	fiTokenizer xbTokenizer;
	fiTokenizer ps3Tokenizer;

	xbTokenizer.Init("Xbox360", xbox360Stream);
	ps3Tokenizer.Init("PS3", ps3Stream);
	

	char lineBuf[256];
	// Skip headers
	xbTokenizer.GetLine(lineBuf, sizeof(lineBuf));
	ps3Tokenizer.GetLine(lineBuf, sizeof(lineBuf));

	for(u32 i = 0; i < synthOpcodes::NUM_OP_CODES; i++)
	{
		u32 u0 = 0U;
		float f1 = 0.f;
		float f2 = 0.f;
		xbTokenizer.GetLine(lineBuf, sizeof(lineBuf));	
		const char *vals = strstr(lineBuf, ",")+1;
		sscanf(vals, "%u,%f,%f", &u0, &f1, &f2);
		sm_OpCodeTimings[i].Xbox360_Time = f2;

		ps3Tokenizer.GetLine(lineBuf, sizeof(lineBuf));
		vals = strstr(lineBuf, ",")+1;
		sscanf(vals, "%u,%f,%f", &u0, &f1, &f2);
		sm_OpCodeTimings[i].PS3_Time = f2;
	}
	
	xbox360Stream->Close();
	ps3Stream->Close();
#endif

}

bool synthCoreDisasm::Disassemble(const CompiledSynth *metadata)
{
	m_ShouldResolveConstantValues = true;
	m_ShouldPrintProgramBytes = false;

	m_SynthName = synthSynthesizer::GetMetadataManager().GetObjectNameFromNameTableOffset(metadata->NameTableOffset);
	m_NumBuffers = metadata->NumBuffers;
	m_NumRegisters = metadata->NumRegisters;

	m_NumOutputs = metadata->numOutputs;
	
	m_ProgramEntry = &metadata->Operations;
	m_ProgramSizeBytes = metadata->programSizeBytes;
	m_NumStateBlocks = metadata->numStateBlocks;
	m_Cost = metadata->costEstimate;
	
	const u32 *ptr = (const u32*)((const u8*)m_ProgramEntry + metadata->programSizeBytes);
	m_NumConstants = *ptr++;

	m_Constants = (const float*)ptr;

	const u32 *numInputVariables = (u32*)(m_Constants + m_NumConstants);
	m_InputVariables.SetCount(*numInputVariables);
	numInputVariables++;
	CompiledSynth::tInputVariables *var = (CompiledSynth::tInputVariables*)numInputVariables;
	for(s32 i = 0; i < m_InputVariables.GetCount(); i++)
	{
		m_InputVariables[i].NameHash = var[i].Name;
		m_InputVariables[i].Value = var[i].InitialValue;
	}

	m_TotalSynthSizeBytes = m_ProgramSizeBytes + m_NumConstants*sizeof(float) + m_InputVariables.GetCount()*sizeof(CompiledSynth::tInputVariables);
	return DisassembleProgram(m_ProgramEntry, m_ProgramSizeBytes);
}

bool synthCoreDisasm::DisassembleProgram(const u16 *programEntry, const u32 programSizeBytes)
{
	m_Disassembly.Reset();

	const u16 *pc = programEntry;
	
	while(pc < programEntry + programSizeBytes)
	{
		const u8 *opStartPtr = (const u8*)pc;

		synthOpcodeDecoder opcode = *(synthOpcodeDecoder*)pc++;

		const u32 numInputs = opcode.numInputs;
		const u32 numOutputs = opcode.numOutputs;

		if(opcode.opcode == synthOpcodes::END)
		{
			return true;
		}
		else if(opcode.opcode >= sizeof(g_OpcodeInfo)/sizeof(g_OpcodeInfo[0]))
		{
			return false;
		}
		
		
		DecodedOp &op = m_Disassembly.Grow();

		op.DataPtr = opStartPtr;
		op.Offset = (u32)(opStartPtr - (const u8*)programEntry);
		op.Opcode = opcode.opcode;

		op.OpcodeName = GetOpcodeName(op.Opcode);
		const bool isReplacing = IsReplacing(op.Opcode);
				
		if(opcode.opcode == synthOpcodes::COPY_BUFFER)
		{
			synthAssert(numInputs == 1);
			synthAssert(numOutputs >= 1);
			synthValidateReferenceType(*pc, 'B');

			if(numInputs != 1 || numOutputs == 0 || numOutputs > 16)
			{
				// Abort
				return false;
			}

			DecodedOp::Operand &inputOperand = op.Inputs.Append();
			inputOperand.Type = decode_synth_ref_type(*pc);
			inputOperand.Id = decode_synth_ref_id(*pc++);

			audAssert(numOutputs > 1);
			for(u32 i = 0; i < numOutputs - 1; i++)
			{
				synthValidateReferenceType(*pc, 'B');
				
				DecodedOp::Operand &outputOperand = op.Outputs.Append();
				outputOperand.Type = decode_synth_ref_type(*pc);
				outputOperand.Id = decode_synth_ref_id(*pc++);			

			}
			// Skip final (repeated) input
			pc++;
		}
		else if(opcode.opcode == synthOpcodes::COPY_SCALAR)
		{
			synthAssert(numInputs == 1);
			synthAssert(numOutputs >= 1);
			synthValidateReferenceType(*pc, 'R');

			if(numInputs != 1 || numOutputs == 0 || numOutputs > 16)
			{
				// Abort
				return false;
			}
			DecodedOp::Operand &inputOperand = op.Inputs.Append();
			inputOperand.Type = decode_synth_ref_type(*pc);
			inputOperand.Id = decode_synth_ref_id(*pc++);
			
			const u32 numDestinations = opcode.numOutputs;
			audAssert(numDestinations > 1);
			for(u32 i = 0; i < numDestinations - 1; i++)
			{
				synthValidateReferenceType(*pc, 'R');
				DecodedOp::Operand &outputOperand = op.Outputs.Append();
				outputOperand.Type = decode_synth_ref_type(*pc);
				outputOperand.Id = decode_synth_ref_id(*pc++);
			}

			// skip (repeated) input
			pc++;
		}
		else
		{
			// outputs first
			for(u32 i = 0; i < numOutputs; i++)
			{
				DecodedOp::Operand &operand = op.Outputs.Append();
				const u32 inputRef = *pc++;
				operand.Type = decode_synth_ref_type(inputRef);
				operand.Id = decode_synth_ref_id(inputRef);
			}

			// then inputs
			if(isReplacing)
			{
				op.Inputs.Append() = op.Outputs[0];
			}

			for(u32 i = 0 ; i < (isReplacing ? numInputs - 1 : numInputs); i++)
			{
				DecodedOp::Operand &operand = op.Inputs.Append();
				const u32 inputRef = *pc++;
				operand.Type = decode_synth_ref_type(inputRef);
				operand.Id = decode_synth_ref_id(inputRef);
			}

			// finally state block refs
			for(u32 i = 0; i < g_OpcodeInfo[op.Opcode].numStateBlocks; i++)
			{
				op.StateBlocks.Append() = *pc++;
			}		
		}

		op.SizeBytes = (u32)((u8*)pc - opStartPtr);
	}

	return false;
}

bool synthCoreDisasm::IsReplacing(const u32 opcode)
{
	return g_OpcodeInfo[opcode].isReplacing;
}

const char *synthCoreDisasm::GetOpcodeName(const u32 opcode)
{
	if(opcode < synthOpcodes::NUM_OP_CODES)
		return g_OpcodeInfo[opcode].opName;
	else if(opcode == synthOpcodes::END)
		return "END";
	else
		return "INVALID";
}

float synthCoreDisasm::ReadConstantValue(const u32 constantId)
{
	if(constantId == 0)
	{
		return 0.f;
	}
	else if(constantId == 1)
	{
		return 1.f;
	}
	else
		return m_Constants[constantId-2];
}

char *synthCoreDisasm::FormatOperand(const DecodedOp::Operand &operand, char *buf, const size_t bufSize)
{
	if(operand.Type == synthOpcodes::Constant && m_ShouldResolveConstantValues)
	{
		return formatf(buf, bufSize, "%g", ReadConstantValue(operand.Id));
	}
	else
	{
		const char typeNames[] = {
			'B',
			'R',
			'C',
			'V'
		};
		return formatf(buf, bufSize, "%c%d", typeNames[operand.Type], operand.Id);
	}
}


void synthCoreDisasm::ComputeRuntimeEstimate(float &ps3, float &xbox360) const
{
	ps3 = 0.f;
	xbox360 = 0.f;

	for(s32 i = 0; i < m_Disassembly.GetCount(); i++)
	{
		u32 opcode = m_Disassembly[i].Opcode;
		if(opcode < synthOpcodes::NUM_OP_CODES)
		{
			ps3 += sm_OpCodeTimings[opcode].PS3_Time;
			xbox360 += sm_OpCodeTimings[opcode].Xbox360_Time;
		}
	}
}

void synthCoreDisasm::DebugPrint()
{
	audDisplayf("Disassembly of synth %s, total size: %u bytes", m_SynthName, m_TotalSynthSizeBytes);
	audDisplayf("%u buffers, %u registers, %u state blocks, %u constants, %u outputs",
		m_NumBuffers,
		m_NumRegisters,
		m_NumStateBlocks,
		m_NumConstants,
		m_NumOutputs);
	float ps3Time = 0.f;
	float xbox360Time = 0.f;
	ComputeRuntimeEstimate(ps3Time, xbox360Time);
	audDisplayf("Runtime cost - %u (PS3: %.2fuS, Xbox360: %.2fuS)", m_Cost, ps3Time, xbox360Time);
	audDisplayf("");



	for(s32 i = 0; i < m_Disassembly.GetCount(); i++)
	{
		char lineBuf[1024];
		char operandBuf[128];
		DecodedOp &op = m_Disassembly[i];
		formatf(lineBuf, "0+%04X,%u\t", op.Offset, op.SizeBytes);

		if(m_ShouldPrintProgramBytes)
		{
			for(u32 j = 0; j < op.SizeBytes; j++)
			{
				formatf(operandBuf, "%02X", *(op.DataPtr+j));
				safecat(lineBuf, operandBuf);
			}
			safecat(lineBuf, "\t");
		}
		
		formatf(operandBuf, "%s ",op.OpcodeName);
		safecat(lineBuf, operandBuf);	
		
		for(s32 j = 0; j < op.Inputs.GetCount(); j++)
		{
			FormatOperand(op.Inputs[j], operandBuf, sizeof(operandBuf));
			safecat(lineBuf, operandBuf);

			if(j + 1 < op.Inputs.GetCount())
			{
				safecat(lineBuf, ", ");
			}
		}

		safecat(lineBuf, " => ");

		for(s32 j = 0; j < op.Outputs.GetCount(); j++)
		{
			FormatOperand(op.Outputs[j], operandBuf, sizeof(operandBuf));
			safecat(lineBuf, operandBuf);

			if(j + 1 < op.Outputs.GetCount())
			{
				safecat(lineBuf, ", ");
			}
		}

		if(op.StateBlocks.GetCount())
		{
			safecat(lineBuf, " [");
			for(s32 j = 0; j < op.StateBlocks.GetCount(); j++)
			{
				formatf(operandBuf,"%u", op.StateBlocks[j]);
				safecat(lineBuf, operandBuf);
			}
			safecat(lineBuf, "]");
		}

		audDisplayf("%s", lineBuf);
	}
}

#if __ASSERT
int synthCoreDisasm::SynthAssertFailed(const char *msg, const char *exp, const char *file, const int line) const
{
	char message[128];
#if __SPU
	ALIGNAS(16) char nameBuf[128] ;
	ALIGNAS(16) char nameBufAligned[128] ;
	dmaUnalignedGet(nameBuf, sizeof(nameBuf), nameBufAligned, 96, (u32)m_SynthName);
	const char *synthName = nameBufAligned;
#else
	const char *synthName = m_SynthName;
#endif

	formatf(message, "Synth: '%s' %s", synthName, msg ? msg : "");

#if __SPU
	audErrorf("synthAssert: %s", message);
	return AssertFailed(exp, file, line);
#else
	return diagAssertHelper(file, line, "%s: %s", exp, message);
#endif// __SPU
}
#endif //__ASSERT


} // namespace rage

#endif // __BANK

