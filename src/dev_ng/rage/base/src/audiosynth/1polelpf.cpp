// 
// audiosynth/1polelpf.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "math/amath.h"
#include "1polelpf.h"
#include "1poleview.h"
#include "pin.h"
#include "math/vecmath.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	SYNTH_EDITOR_LINK_VIEW(synth1PoleLPF,synth1PoleView);

	synth1PoleLPF::synth1PoleLPF() 
	{
		m_MinFrequency = 0.f;
		m_MaxFrequency = 20000.f;
		
		m_Outputs[0].Init(this,"Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);
		
		m_IsHighpass = false;
		
		m_Inputs[k1PoleGain].Init(this, "Gain", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[k1PoleGain].SetStaticValue(1.f);
		m_Inputs[k1PoleCutoff].Init(this, "Cutoff",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[k1PoleCutoff].SetStaticValue(1.f);
		m_Inputs[k1PoleSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[k1PoleSignal].SetConvertsInternally(false);

		ResetProcessing();
	}

	void synth1PoleLPF::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("MinFreq", m_MinFrequency);
		serializer->SerializeField("MaxFreq", m_MaxFrequency);
		serializer->SerializeField("IsHighpass", m_IsHighpass);
	}

	void synth1PoleLPF::ResetProcessing()
	{
		m_a0 = 1.f;
		m_b1 = 0.f;
		m_PrevSample = 0.f;
	}
	
	void synth1PoleLPF::Synthesize()
	{
		if(m_Inputs[k1PoleSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			const f32 gain = m_Inputs[k1PoleGain].GetNormalizedValue(0);
			const f32 cutoffScale = m_Inputs[k1PoleCutoff].GetNormalizedValue(0);
			const f32 cutoffFreq = Lerp(cutoffScale, m_MinFrequency, m_MaxFrequency);			

			const f32 input = m_Inputs[k1PoleSignal].GetSignalValue(0);
			Vector_4V state = V4LoadScalar32IntoSplatted(m_PrevSample);
			float output;
			if(m_IsHighpass)
			{
				output = Process_HPF(input, cutoffFreq, state);
			}
			else
			{
				output = Process_LPF(input, cutoffFreq, state);
			}
			
			m_PrevSample = GetX(state);
			m_Outputs[0].SetStaticValue(gain * output);
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			const Vector_4V minFreq = V4LoadScalar32IntoSplatted(m_MinFrequency);
			const Vector_4V maxFreq = V4LoadScalar32IntoSplatted(m_MaxFrequency);
			Vector_4V prevSample = V4LoadScalar32IntoSplatted(m_PrevSample);
			//-2 * PI * freq / Fc
			// = -2 * PI * 1/Fc * freq
			const Vector_4V freqScalar = V4VConstant<0xB909421E,0xB909421E,0xB909421E,0xB909421E>();
			Vector_4V *RESTRICT outputBuf = (Vector_4V*)m_Outputs[0].GetDataBuffer()->GetBuffer();

			const synthPin::synthPinDataFormat dataFormat = m_Inputs[k1PoleSignal].GetDataFormat();
			m_Outputs[0].SetDataFormat(dataFormat);

			if(m_Inputs[k1PoleCutoff].GetDataState() == synthPin::SYNTH_PIN_STATIC)
			{
				const Vector_4V freq = V4Lerp(m_Inputs[k1PoleCutoff].GetNormalizedValueV(0), minFreq, maxFreq);

				const Vector_4V x = audDriverUtil::V4ExpE(V4Scale(freq,freqScalar));
				const Vector_4V a0 = V4Subtract(V4VConstant(V_ONE),x);
				const Vector_4V b1 = V4Scale(V4VConstant(V_NEGONE),x);

				for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
				{						
					const Vector_4V gain = m_Inputs[k1PoleGain].GetNormalizedValueV(i);					
					const Vector_4V input = dataFormat == synthPin::SYNTH_PIN_DATA_SIGNAL ? m_Inputs[k1PoleSignal].GetSignalValueV(i)
						: m_Inputs[k1PoleSignal].GetNormalizedValueV(i);
					const Vector_4V a0_x_Input = V4Scale(a0,input);

					// now need to process serially since each sample depends on prev sample
					const Vector_4V lowpassX = V4Subtract(a0_x_Input,V4Scale(b1,prevSample));
					const Vector_4V lowpassY = V4Subtract(V4SplatY(a0_x_Input), V4Scale(b1,lowpassX));
					const Vector_4V lowpassZ = V4Subtract(V4SplatZ(a0_x_Input), V4Scale(b1,lowpassY));
					const Vector_4V lowpassW = V4Subtract(V4SplatW(a0_x_Input), V4Scale(b1,lowpassZ));

					const Vector_4V lowpassXY = V4PermuteTwo<X1,X2,X1,X2>(lowpassX,lowpassY);
					const Vector_4V lowpassZW = V4PermuteTwo<X1,X2,X1,X2>(lowpassZ,lowpassW);

					const Vector_4V lowpassOutput = V4PermuteTwo<X1,Y1,X2,Y2>(lowpassXY,lowpassZW);

					if(Likely(!m_IsHighpass))
					{
						//LPF
						*outputBuf++ = V4Scale(gain, lowpassOutput);
					}
					else
					{
						//HPF
						const Vector_4V highpassOutput = V4Subtract(input, lowpassOutput);
						*outputBuf++ = V4Scale(gain, highpassOutput);
					}
					prevSample = lowpassW;
				}// for each sample
			}
			else
			{
				// dynamic cutoff
				for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
				{
					const Vector_4V freq = V4Lerp(m_Inputs[k1PoleCutoff].GetNormalizedValueV(i), minFreq, maxFreq);

					const Vector_4V x = audDriverUtil::V4ExpE(V4Scale(freq,freqScalar));
					const Vector_4V a0 = V4Subtract(V4VConstant(V_ONE),x);
					const Vector_4V b1 = V4Scale(V4VConstant(V_NEGONE),x);
					const Vector_4V gain = m_Inputs[k1PoleGain].GetNormalizedValueV(i);

					const Vector_4V input = dataFormat == synthPin::SYNTH_PIN_DATA_SIGNAL ? m_Inputs[k1PoleSignal].GetSignalValueV(i)
						: m_Inputs[k1PoleSignal].GetNormalizedValueV(i);

					const Vector_4V a0_x_Input = V4Scale(a0,input);

					// now need to process serially since each sample depends on prev sample
					const Vector_4V lowpassX = V4Subtract(a0_x_Input,V4Scale(b1,prevSample));
					const Vector_4V lowpassY = V4Subtract(V4SplatY(a0_x_Input), V4Scale(b1,lowpassX));
					const Vector_4V lowpassZ = V4Subtract(V4SplatZ(a0_x_Input), V4Scale(b1,lowpassY));
					const Vector_4V lowpassW = V4Subtract(V4SplatW(a0_x_Input), V4Scale(b1,lowpassZ));

					const Vector_4V lowpassXY = V4PermuteTwo<X1,X2,X1,X2>(lowpassX,lowpassY);
					const Vector_4V lowpassZW = V4PermuteTwo<X1,X2,X1,X2>(lowpassZ,lowpassW);

					const Vector_4V lowpassOutput = V4PermuteTwo<X1,Y1,X2,Y2>(lowpassXY,lowpassZW);

					if(Likely(!m_IsHighpass))
					{
						//LPF
						*outputBuf++ = V4Scale(gain, lowpassOutput);
					}
					else
					{
						//HPF
						const Vector_4V highpassOutput = V4Subtract(input, lowpassOutput);
						*outputBuf++ = V4Scale(gain, highpassOutput);
					}
					prevSample = lowpassW;
				}// for each sample
			}

			m_PrevSample = GetX(prevSample);
		}//synthesize
	}

	void synth1PoleLPF::Process_LPF(float *RESTRICT const inOutBuffer, const float *RESTRICT const freqBuffer, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		//-2 * PI * freq / Fc
		// = -2 * PI * 1/Fc * freq
		const Vector_4V freqScalar = V4VConstant<0xB909421E,0xB909421E,0xB909421E,0xB909421E>();

		Vector_4V prevSample = state;

		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		const Vector_4V *freqPtr = reinterpret_cast<const Vector_4V*>(freqBuffer);
		u32 count = numSamples>>2;
		while(count--)
		{		
			const Vector_4V freq = *freqPtr++;

			const Vector_4V x = audDriverUtil::V4ExpE(V4Scale(freq,freqScalar));
			const Vector_4V a0 = V4Subtract(V4VConstant(V_ONE),x);
			const Vector_4V b1 = V4Scale(V4VConstant(V_NEGONE),x);				

			const Vector_4V input = *inOutPtr;

			const Vector_4V a0_x_Input = V4Scale(a0,input);

			// now need to process serially since each sample depends on prev sample
			const Vector_4V lowpassX = V4Subtract(a0_x_Input,V4Scale(b1,prevSample));
			const Vector_4V lowpassY = V4Subtract(V4SplatY(a0_x_Input), V4Scale(b1,lowpassX));
			const Vector_4V lowpassZ = V4Subtract(V4SplatZ(a0_x_Input), V4Scale(b1,lowpassY));
			const Vector_4V lowpassW = V4Subtract(V4SplatW(a0_x_Input), V4Scale(b1,lowpassZ));

			const Vector_4V lowpassXY = V4PermuteTwo<X1,X2,X1,X2>(lowpassX,lowpassY);
			const Vector_4V lowpassZW = V4PermuteTwo<X1,X2,X1,X2>(lowpassZ,lowpassW);

			const Vector_4V lowpassOutput = V4PermuteTwo<X1,Y1,X2,Y2>(lowpassXY,lowpassZW);

			*inOutPtr++ = lowpassOutput;

			prevSample = lowpassW;
		}
		state = prevSample;
	}

	void synth1PoleLPF::Process_LPF(float *RESTRICT const inOutBuffer, const float scalarFreq, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		const Vector_4V freq = V4LoadScalar32IntoSplatted(scalarFreq);
		//-2 * PI * freq / Fc
		// = -2 * PI * 1/Fc * freq
		const Vector_4V freqScalar = V4VConstant<0xB909421E,0xB909421E,0xB909421E,0xB909421E>();

		Vector_4V prevSample = state;

		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		
		const Vector_4V x = audDriverUtil::V4ExpE(V4Scale(freq,freqScalar));
		const Vector_4V a0 = V4Subtract(V4VConstant(V_ONE),x);
		const Vector_4V b1 = V4Scale(V4VConstant(V_NEGONE),x);

		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V input = *inOutPtr;

			const Vector_4V a0_x_Input = V4Scale(a0,input);

			// now need to process serially since each sample depends on prev sample
			const Vector_4V lowpassX = V4Subtract(a0_x_Input,V4Scale(b1,prevSample));
			const Vector_4V lowpassY = V4Subtract(V4SplatY(a0_x_Input), V4Scale(b1,lowpassX));
			const Vector_4V lowpassZ = V4Subtract(V4SplatZ(a0_x_Input), V4Scale(b1,lowpassY));
			const Vector_4V lowpassW = V4Subtract(V4SplatW(a0_x_Input), V4Scale(b1,lowpassZ));

			const Vector_4V lowpassXY = V4PermuteTwo<X1,X2,X1,X2>(lowpassX,lowpassY);
			const Vector_4V lowpassZW = V4PermuteTwo<X1,X2,X1,X2>(lowpassZ,lowpassW);

			const Vector_4V lowpassOutput = V4PermuteTwo<X1,Y1,X2,Y2>(lowpassXY,lowpassZW);

			*inOutPtr++ = lowpassOutput;

			prevSample = lowpassW;
		}
		state = prevSample;
	}

	float synth1PoleLPF::Process_LPF(const float input, const float freq, Vec::Vector_4V_InOut state)
	{
		float prevSample = GetX(state);
		const f32 effectiveSampleRate = 1.f / (f32(kMixBufNumSamples) / f32(kMixerNativeSampleRate));
		const f32 x = Powf(M_E, -2.0f*PI*freq/effectiveSampleRate);
		float a0 = 1.f - x;
		float b1 = -x;

		const f32 lowpassOutput = (a0*input - b1 * prevSample);
		SetX(state, lowpassOutput);
		return lowpassOutput;
	}

	void synth1PoleLPF::Process_HPF(float *RESTRICT const inOutBuffer, const float *RESTRICT const freqBuffer, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		//-2 * PI * freq / Fc
		// = -2 * PI * 1/Fc * freq
		const Vector_4V freqScalar = V4VConstant<0xB909421E,0xB909421E,0xB909421E,0xB909421E>();

		Vector_4V prevSample = state;

		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		const Vector_4V *freqPtr = reinterpret_cast<const Vector_4V*>(freqBuffer);
		u32 count = numSamples>>2;
		while(count--)
		{		
			const Vector_4V freq = *freqPtr++;

			const Vector_4V x = audDriverUtil::V4ExpE(V4Scale(freq,freqScalar));
			const Vector_4V a0 = V4Subtract(V4VConstant(V_ONE),x);
			const Vector_4V b1 = V4Scale(V4VConstant(V_NEGONE),x);				

			const Vector_4V input = *inOutPtr;

			const Vector_4V a0_x_Input = V4Scale(a0,input);

			// now need to process serially since each sample depends on prev sample
			const Vector_4V lowpassX = V4Subtract(a0_x_Input,V4Scale(b1,prevSample));
			const Vector_4V lowpassY = V4Subtract(V4SplatY(a0_x_Input), V4Scale(b1,lowpassX));
			const Vector_4V lowpassZ = V4Subtract(V4SplatZ(a0_x_Input), V4Scale(b1,lowpassY));
			const Vector_4V lowpassW = V4Subtract(V4SplatW(a0_x_Input), V4Scale(b1,lowpassZ));

			const Vector_4V lowpassXY = V4PermuteTwo<X1,X2,X1,X2>(lowpassX,lowpassY);
			const Vector_4V lowpassZW = V4PermuteTwo<X1,X2,X1,X2>(lowpassZ,lowpassW);

			const Vector_4V lowpassOutput = V4PermuteTwo<X1,Y1,X2,Y2>(lowpassXY,lowpassZW);

			*inOutPtr++ = V4Subtract(input, lowpassOutput);
			
			prevSample = lowpassW;
		}
		state = prevSample;
	}

	void synth1PoleLPF::Process_HPF(float *RESTRICT const inOutBuffer, const float scalarFreq, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		const Vector_4V freq = V4LoadScalar32IntoSplatted(scalarFreq);
		//-2 * PI * freq / Fc
		// = -2 * PI * 1/Fc * freq
		const Vector_4V freqScalar = V4VConstant<0xB909421E,0xB909421E,0xB909421E,0xB909421E>();

		Vector_4V prevSample = state;

		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);

		const Vector_4V x = audDriverUtil::V4ExpE(V4Scale(freq,freqScalar));
		const Vector_4V a0 = V4Subtract(V4VConstant(V_ONE),x);
		const Vector_4V b1 = V4Scale(V4VConstant(V_NEGONE),x);

		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V input = *inOutPtr;

			const Vector_4V a0_x_Input = V4Scale(a0,input);

			// now need to process serially since each sample depends on prev sample
			const Vector_4V lowpassX = V4Subtract(a0_x_Input,V4Scale(b1,prevSample));
			const Vector_4V lowpassY = V4Subtract(V4SplatY(a0_x_Input), V4Scale(b1,lowpassX));
			const Vector_4V lowpassZ = V4Subtract(V4SplatZ(a0_x_Input), V4Scale(b1,lowpassY));
			const Vector_4V lowpassW = V4Subtract(V4SplatW(a0_x_Input), V4Scale(b1,lowpassZ));

			const Vector_4V lowpassXY = V4PermuteTwo<X1,X2,X1,X2>(lowpassX,lowpassY);
			const Vector_4V lowpassZW = V4PermuteTwo<X1,X2,X1,X2>(lowpassZ,lowpassW);

			const Vector_4V lowpassOutput = V4PermuteTwo<X1,Y1,X2,Y2>(lowpassXY,lowpassZW);

			*inOutPtr++ = V4Subtract(input, lowpassOutput);

			prevSample = lowpassW;
		}
		state = prevSample;
	}

	float synth1PoleLPF::Process_HPF(const float input, const float freq, Vec::Vector_4V_InOut state)
	{
		float prevSample = GetX(state);
		const f32 effectiveSampleRate = 1.f / (f32(kMixBufNumSamples) / f32(kMixerNativeSampleRate));
		const f32 x = Powf(M_E, -2.0f*PI*freq/effectiveSampleRate);
		float a0 = 1.f - x;
		float b1 = -x;

		const f32 lowpassOutput = (a0*input - b1 * prevSample);
		SetX(state, lowpassOutput);
		return input - lowpassOutput;
	}
}
#endif
