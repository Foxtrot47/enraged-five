// 
// audiosynth/triggerview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "triggerview.h"
#include "trigger.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"
#include "uiplane.h"

namespace rage
{

	synthTriggerView::synthTriggerView(synthModule *module) : synthModuleView(module)
	{
		m_ModeSwitch.AddItem("Difference");
		m_ModeSwitch.AddItem("Latching");		
		AddComponent(&m_ModeSwitch);

		m_ModeSwitch.SetCurrentIndex(Min(1U,(u32)GetTrigger()->GetMode()));
	}

	void synthTriggerView::Update()
	{
		if(m_TitleLabel.IsUnderMouse() && synthUIPlane::GetMaster()->HadLeftClick())
		{
			GetTrigger()->Trigger();
		}		

		Matrix34 mtx = GetMatrix();

		// Mode label		
		mtx.d -= mtx.b*0.75f;
		
		m_ModeSwitch.SetMatrix(mtx);		
		m_ModeSwitch.SetAlpha(GetHoverFade());
		m_ModeSwitch.SetShouldDraw(ShouldDraw() && !IsMinimised());

		SetHideTitle(!IsMinimised() && m_ModeSwitch.IsShowingMenu());
		GetTrigger()->SetMode((synthTrigger::synthTriggerMode)m_ModeSwitch.GetCurrentIndex());

		synthModuleView::Update();
	}

	void synthTriggerView::Render()
	{
		synthModuleView::Render();
	}

	bool synthTriggerView::StartDrag() const
	{
		if(m_ModeSwitch.IsUnderMouse() || m_ModeSwitch.IsShowingMenu() || m_TitleLabel.IsUnderMouse())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthTriggerView::IsActive() const
	{
		return (IsUnderMouse());
	}
}
#endif // __SYNTH_EDITOR

