// 
// audiosynth/1poleview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_1POLEVIEW_H
#define SYNTH_1POLEVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uilabel.h"
#include "uimenuview.h"
#include "uirangeview.h"
#include "uidropdownlist.h"

namespace rage
{
	class synth1PoleLPF;
	class synth1PoleView : public synthModuleView
	{
	public:

		synth1PoleView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();

		synth1PoleLPF *GetFilterModule()
		{
			return (synth1PoleLPF*)m_Module;
		}
	private:
		synthUIDropDownList m_ModeSwitch;
		synthUIRangeView m_FreqRange;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_1POLEVIEW_H

