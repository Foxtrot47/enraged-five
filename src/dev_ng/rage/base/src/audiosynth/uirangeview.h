// 
// audiosynth/uirangeview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UIRANGEVIEW_H
#define SYNTH_UIRANGEVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uiscrollbar.h"
#include "uiview.h"
#include "vector/vector3.h"

namespace rage
{
	class synthUIRangeView : public synthUIView
	{

	public:

		synthUIRangeView();
		virtual ~synthUIRangeView();

		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		bool IsBeingDragged()
		{
			return m_MinScrollBar.IsBeingDragged()||m_MaxScrollBar.IsBeingDragged();
		}

		virtual bool StartDrag() const { return false; }

		f32 GetMinValue() const
		{
			return m_MinScrollBar.GetValue();
		}

		f32 GetMaxValue() const
		{
			return m_MaxScrollBar.GetValue();
		}

		f32 GetMinNormalizedValue() const
		{
			return m_MinScrollBar.GetNormalizedValue();
		}

		f32 GetMaxNormalizedValue() const
		{
			return m_MaxScrollBar.GetNormalizedValue();
		}

		void SetMinRange(const f32 min, const f32 max)
		{
			m_MinScrollBar.SetRange(min,max);
		}

		void SetMaxRange(const f32 min, const f32 max)
		{
			m_MaxScrollBar.SetRange(min,max);
		}

		void SetNormalizedValues(const f32 minVal, const f32 maxVal)
		{
			m_MinScrollBar.SetNormalizedValue(minVal);
			m_MaxScrollBar.SetNormalizedValue(maxVal);
		}

		void SetTextAlpha(const f32 alpha)
		{
			m_MinScrollBar.SetTextAlpha(alpha);
			m_MaxScrollBar.SetTextAlpha(alpha);
		}

		bool IsActive() const { return (m_MinScrollBar.IsActive()||m_MaxScrollBar.IsActive()); }

		void SetWidth(const f32 width)
		{
			m_Width = width;
		}

		void SetTitles(const char *minTitle, const char *maxTitle)
		{
			m_MinScrollBar.SetTitle(minTitle);
			m_MaxScrollBar.SetTitle(maxTitle);
		}

        void SetDrawTitles(const bool shouldDraw)
        {
            m_MaxScrollBar.SetDrawTitle(shouldDraw);
            m_MinScrollBar.SetDrawTitle(shouldDraw);
        }

	protected:
		virtual void Render();

	private:
		synthUIScrollbar m_MinScrollBar;
		synthUIScrollbar m_MaxScrollBar;
		f32 m_Width;
	};
}
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIRANGEVIEW_H
