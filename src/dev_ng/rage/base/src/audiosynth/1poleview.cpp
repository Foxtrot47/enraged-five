// 
// audiosynth/1poleview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "1poleview.h"
#include "1polelpf.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"
#include "uiplane.h"

namespace rage
{

	synth1PoleView::synth1PoleView(synthModule *module) : synthModuleView(module)
	{
		m_ModeSwitch.AddItem("LPF");
		m_ModeSwitch.AddItem("HPF");
		AddComponent(&m_ModeSwitch);
		m_ModeSwitch.SetCurrentIndex(GetFilterModule()->IsHighpass() ? 1 : 0);

		AddComponent(&m_FreqRange);

		const f32 maxUIVal = 20000.f;

		m_FreqRange.SetMinRange(0.f, maxUIVal);
		m_FreqRange.SetMaxRange(0.f, maxUIVal);

		m_FreqRange.SetNormalizedValues(GetFilterModule()->GetMinFrequency()/maxUIVal, GetFilterModule()->GetMaxFrequency()/maxUIVal);
	}

	void synth1PoleView::Update()
	{
		// fade out title when editing
		if(IsMinimised())
		{
			SetTitleAlpha(1.f);
		}
		else
		{
			SetTitleAlpha(1.f - GetHoverFade());
		}
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();

		// Mode label
		const f32 moduleHalfHeight = 0.5f * ComputeHeight();

		mtx.d += (mtx.b * 1.f);
		m_ModeSwitch.SetMatrix(mtx);
		m_ModeSwitch.SetAlpha(GetHoverFade());
		m_ModeSwitch.SetShouldDraw(!IsMinimised());

		GetFilterModule()->SetIsHighpass(m_ModeSwitch.GetCurrentIndex() == 1);
	
		mtx = GetMatrix();
		const Vector3 yOffset = mtx.b * moduleHalfHeight * 0.25f;
		mtx.d -= yOffset;
		m_FreqRange.SetMatrix(mtx);
		m_FreqRange.SetAlpha(GetHoverFade());
		m_FreqRange.SetTextAlpha(GetHoverFade());

		m_FreqRange.SetTitles("MinCutoff", "MaxCutoff");
		m_FreqRange.SetWidth(0.4f);
		m_FreqRange.SetShouldDraw(!IsMinimised());

		GetFilterModule()->SetMaxFrequency(m_FreqRange.GetMaxValue());
		GetFilterModule()->SetMinFrequency(m_FreqRange.GetMinValue());
		GetFilterModule()->GetInput(k1PoleCutoff).GetView()->SetStaticViewRange(
			GetFilterModule()->GetMinFrequency(), GetFilterModule()->GetMaxFrequency());

		synthModuleView::Update();
	}

	void synth1PoleView::Render()
	{
		synthModuleView::Render();
	}

	bool synth1PoleView::StartDrag() const
	{
		if(m_FreqRange.IsUnderMouse() || m_ModeSwitch.IsUnderMouse() || m_ModeSwitch.IsShowingMenu())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synth1PoleView::IsActive() const
	{
		return (IsUnderMouse() || m_FreqRange.IsActive());
	}
}
#endif // __SYNTH_EDITOR

