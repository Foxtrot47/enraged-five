

#ifndef SYNTH_AUDIOOUTPUT_H
#define SYNTH_AUDIOOUTPUT_H

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class fiStream;
	enum synthAudioOutputInputs
	{
		kAudioOutputSignal,
		kAudioOutputFinishedTrigger,

		kAudioOutputNumInputs
	};

	class synthAudioOutput : public synthModuleBase<kAudioOutputNumInputs,0,SYNTH_AUDIOOUTPUT>
	{
	public:
		SYNTH_MODULE_NAME("Audio Out");
		SYNTH_CUSTOM_MODULE_VIEW;
		synthAudioOutput();
		virtual ~synthAudioOutput();

		virtual void Synthesize();

		synthSampleFrame &GetBuffer()
		{
			return m_Buffer;
		}

		bool HasClipped() const {return m_HasClipped;}
		bool HasFinished() const { return m_HasFinished; }
		void ResetClip() { m_HasClipped = false;}

		float GetPeakLevel() const { return m_PeakLevel; }
		void ResetPeakLevel() { m_PeakLevel = 0.f; }
		float GetRMSLevel() const { return m_RMSLevel; }
		void ResetRMSLevel() { m_RMSLevel = 0.f; }

		void SetOutputBufferId(const u32 bufferId) { m_Buffer.SetBufferId(bufferId); }

	private:

		synthSampleFrame m_Buffer;
		
		float m_PeakLevel;
		float m_RMSLevel;
		bool m_HasClipped;
		bool m_HasFinished;
	};
}

#endif // SYNTH_AUDIOOUTPUT_H


