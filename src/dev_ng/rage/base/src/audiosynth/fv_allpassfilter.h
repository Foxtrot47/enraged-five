
#ifndef SYNTH_FV_ALLPASSFILTER_H
#define SYNTH_FV_ALLPASSFILTER_H

#include "synthdefs.h"

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "audioeffecttypes/allpassfilter.h"

namespace rage
{

	enum synthFvAllpassFilterInputs
	{
		kFvAllpassSignal = 0,
		kFvAllpassLength,
		kFvAllpassFeedback,
		kFvAllpassDecay,
		kFvAllpassNumInputs
	};
	
	class synthFvAllpassFilter : public synthModuleBase<kFvAllpassNumInputs,1, SYNTH_FVALLPASSFILTER>
	{
	public:

		synthFvAllpassFilter();
		virtual ~synthFvAllpassFilter();


		virtual void Synthesize();

		virtual const char *GetName() const
		{
			return "FV_AllPassFilter";
		}


	private:

		synthSampleFrame m_Buffer;
		audAllpassFilter m_Filter;

		u32 m_AllpassLength;
		float *m_AllpassBuffer;
	};
}

#endif // SYNTH_FV_ALLPASSFILTER_H


