// 
// audiosynth/audiooutputview.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "audiooutputview.h"
#include "audiooutput.h"
#include "module.h"
#include "moduleview.h"
#include "pin.h"
#include "pinview.h"
#include "synthesizer.h"
#include "synthesizerview.h"
#include "uiplane.h"
#include "audioeffecttypes/fft.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixing_vmath.inl"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthAudioOutput);

	synthAudioOutputView::synthAudioOutputView(synthModule *module) : synthModuleView(module)
	{
		AddComponent(&m_Layout);
		
		m_ClipLabel.SetString("Clip");
		Color32 red = Color32(1.f, 0.f, 0.f, 1.f);
		m_ClipLabel.GetLabel().SetActiveTextColour(red);
		m_ClipLabel.GetLabel().SetInactiveTextColour(red);
		m_ClipLabel.SetSize(0.2f);

		m_CurrentMeter.SetTitle("Current");
		m_PeakMeter.SetTitle("Peak");
		m_RMSMeter.SetTitle("RMS");

		m_Layout.Add(&m_PeakMeter);
		m_Layout.Add(&m_RMSMeter);
		m_Layout.Add(&m_CurrentMeter);

		m_Layout.SetOrientation(synthUIHorizontalLayout::kHorizontal);
		m_Layout.SetPadding(0.2f);

		m_FftPin.Init(m_Module, "Spectrum", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_FftPin.GetView()->SetDrawFilled(true);
		m_FftPin.GetView()->DisableConnections(true);
		m_FftPin.GetView()->DisableZooming(true);
		m_FftPin.GetView()->SetTargetScale(0.5f);
		m_FftPin.SetDataBuffer(&m_FftBuffer);
		AddComponent(m_FftPin.GetView());

		m_ClipAlpha = 0.f;
		m_PeakLevel = 0.f;

		const u32 numIndices = 2 + (u32)Sqrtf((f32)(kMixBufNumSamples>>1));
		m_Ip = rage_new s32[numIndices];
		m_W = rage_new f32[kMixBufNumSamples>>1];

		sysMemSet(m_Ip, 0, sizeof(s32) * numIndices);
		sysMemSet(m_W, 0, sizeof(f32) * (kMixBufNumSamples>>1));

	}

	synthAudioOutputView::~synthAudioOutputView()
	{
		delete[] m_Ip;
		delete[] m_W;
	}
	
	void synthAudioOutputView::Update()
	{
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		static const char *audioOutLabel[] = {"Audio Out 0","Audio Out 1","Audio Out 2","Audio Out 3"};
		m_TitleLabel.SetString(audioOutLabel[synthSynthesizerView::Get()->GetSynth()->GetOutputIndex(m_Module)]);

		Matrix34 mtx = GetMatrix();
		mtx.d -= mtx.c * 0.1f;
		mtx.d -= mtx.b * 2.f;
		m_Layout.SetMatrix(mtx);

		mtx = GetMatrix();
		mtx.d += mtx.b * 2.f;
		mtx.d -= mtx.c * 0.1f;
		m_FftPin.GetView()->SetDefaultYZoom(1.6f);
		m_FftPin.GetView()->SetMatrix(mtx);
		m_FftPin.GetView()->SetShouldDraw(ShouldDraw() && !IsMinimised());
		m_FftPin.GetView()->SetDefaultYZoom(1.6f);
		if(GetAudioOutput()->HasClipped())
		{
			GetAudioOutput()->ResetClip();
			m_ClipAlpha = 1.f;
		}
		else
		{
			m_ClipAlpha *= 0.9f;
			if(m_ClipAlpha < 0.01f)
			{
				m_ClipAlpha = 0.f;
			}
		}
		m_ClipLabel.GetLabel().SetAlpha(m_ClipAlpha);

		m_PeakLevel = Max(m_PeakLevel, GetAudioOutput()->GetPeakLevel());
		m_PeakMeter.SetLevel(m_PeakLevel);
		m_CurrentMeter.SetLevel(GetAudioOutput()->GetPeakLevel());
		m_RMSMeter.SetLevel(GetAudioOutput()->GetRMSLevel());		

		m_CurrentMeter.SetShouldDraw(ShouldDraw() && !IsMinimised());
		m_PeakMeter.SetShouldDraw(ShouldDraw() && !IsMinimised());
		m_RMSMeter.SetShouldDraw(ShouldDraw() && !IsMinimised());

		if(m_PeakMeter.IsUnderMouse() && synthSynthesizerView::Get()->GetUIPlane().HadLeftClick())
		{
			m_PeakLevel = 0.f;
		}

		GetAudioOutput()->ResetRMSLevel();
		GetAudioOutput()->ResetPeakLevel();
		

		synthModuleView::Update();
	}

	void synthAudioOutputView::Render()
	{
		synthModuleView::Render();
	}

	void synthAudioOutputView::PostSynthesize()
	{
		synthModuleView::PostSynthesize();

		if(GetAudioOutput()->GetInput(0).GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			sysMemCpy(m_FftBuffer.GetBuffer(), GetAudioOutput()->GetInput(0).GetDataBuffer()->GetBuffer(), sizeof(f32) * m_FftBuffer.GetSize());
			audFft::Rdft(m_FftBuffer.GetSize(), 1, m_FftBuffer.GetBuffer(), m_Ip, m_W);

			float temp[kMixBufNumSamples];

			const float oneOverHalfBufferSize = 1.f / (m_FftBuffer.GetSize()/2.f);
			for(u32 i = 0 ; i < kMixBufNumSamples>>1; i++)
			{
				f32 s1 = m_FftBuffer.GetBuffer()[i*2];
				f32 s2 =  m_FftBuffer.GetBuffer()[i*2 + 1];
				f32 v = Sqrtf(s1*s1 + s2*s2) * oneOverHalfBufferSize;
				temp[i*2] = temp[i*2+1] = v; 
			}

			sysMemCpy(m_FftBuffer.GetBuffer(), temp, sizeof(temp));
		}
		else
		{
			sysMemSet(m_FftBuffer.GetBuffer(), 0, m_FftBuffer.GetSize()*sizeof(float));
		}
	}

}
#endif // __SYNTH_EDITOR

