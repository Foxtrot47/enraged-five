#ifndef SYNTH_MIXER_H
#define SYNTH_MIXER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"
#include "synthdefs.h"

namespace rage
{
	enum synthMixerInputs { kMaxMixerInputs = 16};
	class synthMixer : public synthModuleBase<kMaxMixerInputs, 1, SYNTH_MIXER>
	{
		public:
		SYNTH_MODULE_NAME("Mixer");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthMixer();
		virtual void GetInputs(synthPin *&pins, u32 &numPins);
		virtual void Synthesize();
					
		private:
			
			synthSampleFrame m_Buffer;
		};
}
#endif
#endif // SYNTH_MIXER_H


