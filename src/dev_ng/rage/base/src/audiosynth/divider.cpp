// 
// audiosynth/divider.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "divider.h"
#include "multiplier.h"
#include "math/amath.h"

#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	synthDivider::synthDivider() 
	{
		m_Outputs[0].Init(this, "Result",synthPin::SYNTH_PIN_DATA_NORMALIZED,true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[0].Init(this, "Input1",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetConvertsInternally(false);
		m_Inputs[0].SetStaticValue(1.f);
		m_Inputs[1].Init(this, "Input2",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[1].SetStaticValue(1.f);		
		m_Inputs[1].SetConvertsInternally(false);
	}

	void synthDivider::Synthesize()
	{
		// NOTE: in this module the result of divide by zero is zero

		m_Outputs[0].SetDataFormat(
			(m_Inputs[0].GetDataFormat()==synthPin::SYNTH_PIN_DATA_SIGNAL||m_Inputs[1].GetDataFormat()==synthPin::SYNTH_PIN_DATA_SIGNAL) ? 
			synthPin::SYNTH_PIN_DATA_SIGNAL :
		synthPin::SYNTH_PIN_DATA_NORMALIZED);

		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC &&
			m_Inputs[1].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// both static
			const f32 divisor = m_Inputs[1].GetStaticValue();
			const f32 scalar = Selectf(divisor - SMALL_FLOAT, 1.f / divisor, 0.f);
			m_Outputs[0].SetStaticValue(m_Inputs[0].GetStaticValue() * scalar);
		}
		else if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// top value static, dynamic divisor
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			
			const Vector_4V splat = V4LoadScalar32IntoSplatted(m_Inputs[0].GetStaticValue());
			Vector_4V *RESTRICT buf = (Vector_4V*)m_Inputs[1].GetDataBuffer()->GetBuffer();
			Vector_4V *RESTRICT output = (Vector_4V*)m_Buffer.GetBuffer();

			for(u32 i = 0; i < m_Buffer.GetSize()>>2; i++)
			{
				Vector_4V invertedDivisor = V4InvertSafe(buf[i], V4VConstant(V_ZERO));
				output[i] = V4Scale(splat, invertedDivisor);
			}
		}
		else if(m_Inputs[1].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// divisor static - precompute inv
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			const Vector_4V invSplat = V4InvertSafe(V4LoadScalar32IntoSplatted(m_Inputs[1].GetStaticValue()),V4VConstant(V_ZERO));
			Vector_4V *RESTRICT buf = (Vector_4V*)m_Inputs[0].GetDataBuffer()->GetBuffer();
			Vector_4V *RESTRICT output = (Vector_4V*)m_Buffer.GetBuffer();
			for(u32 i = 0; i < m_Buffer.GetSize()>>2; i++)
			{
				output[i] = V4Scale(invSplat, buf[i]);
			}
		}
		else
		{
			// both dynamic
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			Vector_4V *RESTRICT buf0 = (Vector_4V*)m_Inputs[0].GetDataBuffer()->GetBuffer();
			Vector_4V *RESTRICT buf1 = (Vector_4V*)m_Inputs[1].GetDataBuffer()->GetBuffer();
			Vector_4V *RESTRICT output = (Vector_4V*)m_Buffer.GetBuffer();

			for(u32 i = 0; i < m_Buffer.GetSize()>>2; i++)
			{
				const Vector_4V in1 = buf0[i];
				const Vector_4V in2 = buf1[i];
				output[i] = V4InvScaleSafe(in1, in2);
			}
		}
	}



} // namespace rage
#endif
