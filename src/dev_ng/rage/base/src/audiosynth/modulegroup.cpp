// 
// audiosynth/modulegroup.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "audioengine/engineutil.h"
#include "module.h"
#include "modulegroup.h"
#include "moduleview.h"
#include "synthesizerview.h"
#include "uiplane.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "math/amath.h"
#include "system/timemgr.h"
#include "vector/colorvector3.h"

namespace rage
{
	u32 synthUIViewGroup::sm_NextGroupNum = 0;
	synthUIViewGroup::synthUIViewGroup() : m_TitleLabel(1024)
	{
		AddComponent(&m_TitleLabel);
		AddComponent(&m_CPUTimeLabel);

		ColorVector3 cv3;
		cv3.x = audEngineUtil::GetRandomNumberInRange(0.f, 1.f);
		cv3.y = 0.5f;
		cv3.z = 0.5f;
		cv3.HSVtoRGB();
		m_Colour = Color32(cv3);
		m_ShouldBeDeleted = false;
		m_ModuleUnderMouse = false;

		formatf(m_NameBuf, "Group%u", sm_NextGroupNum++);
	}

	synthUIViewGroup::~synthUIViewGroup()
	{
		for(atArray<synthUIView*>::iterator iter = m_Contents.begin(); iter != m_Contents.end(); iter++)
		{
			Assert((*iter)->IsInGroup(this));
			(*iter)->RemoveFromGroup(this);
		}
	}

	void synthUIViewGroup::SetName(const char *name)
	{
		strcpy_s(m_NameBuf, sizeof(m_NameBuf), name);
	}

	void synthUIViewGroup::SetColour(const u8 r, const u8 g, const u8 b)
	{
		m_Colour = Color32(r,g,b,255);
	}

	s32 synthUIViewGroup::GetId() const
	{
		return synthSynthesizerView::Get()->GetModulePlane().GetGroupId(this);
	}

	void synthUIViewGroup::GetBounds(Vector3 &outputV1, Vector3 &outputV2) const
	{
		outputV1 = Vector3(LARGE_FLOAT,LARGE_FLOAT,LARGE_FLOAT);
		outputV2 = Vector3(-LARGE_FLOAT,-LARGE_FLOAT,-LARGE_FLOAT);
		for(atArray<synthUIView*>::const_iterator iter = m_Contents.begin(); iter != m_Contents.end(); iter++)
		{
			Vector3 v3,v4;
			(*iter)->GetBounds(v3,v4);
			outputV1.Min(outputV1,v3);
			outputV2.Max(outputV2,v4);
		}
		Matrix34 bm = GetMatrix();
		outputV1 -= bm.a + bm.b;
		// add a border
		outputV2 += bm.a + bm.b*2.f;
		// add an extra border for the title
		//outputV2 += bm.b * 0.3f;
	}

	bool synthUIViewGroup::StartDrag() const
	{
		return IsUnderMouse();
	}

	void synthUIViewGroup::Add(synthUIView *newItem)
	{
        Assert(newItem!=this);
		Assert(newItem);
		if(newItem && newItem != this && !Contains(newItem))
		{
			atArray<synthUIViewGroup *> groupList;
			if(newItem->IsGroup())
			{
				// Ensure we don't add a group that indirectly contains this group
				synthUIViewGroup *otherGroup = (synthUIViewGroup*)newItem;
				otherGroup->FindGroups(groupList);
				if(groupList.Find(this) != -1)
				{
					return;
				}
			}

			m_Contents.PushAndGrow(newItem);
			m_ModuleOffsets.PushAndGrow(newItem->GetMatrix().d - GetMatrix().d);
			Assert(!newItem->IsInGroup(this));
			newItem->AddToGroup(this);
		}
	}

	void synthUIViewGroup::Remove(synthUIView *module)
	{
		Assert(module);
		Assert(module->IsInGroup(this));
		
		s32 index = m_Contents.Find(module);
		Assert(index >= 0);
		m_Contents.Delete(index);
		m_ModuleOffsets.Delete(index);

		module->RemoveFromGroup(this);
	}

	bool synthUIViewGroup::Contains(synthUIView *module) const
	{
		return (m_Contents.Find(module)>=0);
	}

	void synthUIViewGroup::Update()
	{
		SetAlpha(1.f);
		
        if(synthUIPlane::GetMaster()->GetDraggingView() && synthUIPlane::GetMaster()->GetDraggingView()->IsInGroup(this))
        {
            Vector3 rootPos = GetMatrix().d;
            // update stored relative positions
            for(s32 i = 0; i < m_Contents.GetCount(); i++)
            {
                m_ModuleOffsets[i] = m_Contents[i]->GetMatrix().d - rootPos;
            }
        }
        else
		{
			// position all grouped modules relative to the group itself
            for(s32 i = 0; i < m_Contents.GetCount(); i++)
            {
                Matrix34 finalMatrix;

                finalMatrix = GetMatrix();
                finalMatrix.d += m_ModuleOffsets[i];

                m_Contents[i]->SetMatrix(finalMatrix);
            }
		}

        Vector3 v1,v2;
		GetBounds(v1,v2);
	
		Vector3 m = (v1+v2)*0.5f;

		Matrix34 bm = GetMatrix();
		const f32 height = bm.b.Dot(v2-v1);
		//const f32 width = bm.a.Dot(v2-v1);

		bm.d = m + (bm.b * (0.5f*height - 0.5f*m_TitleLabel.ComputeHeight()));
		//bm.d += bm.a * (width + m_TitleLabel.ComputeWidth());
		m_TitleLabel.SetMatrix(bm);
		
		m_TitleLabel.SetShouldDraw(ShouldDraw());
		m_TitleLabel.SetString(m_NameBuf);
		m_TitleLabel.SetSize(0.35f);


		// bottom right corner
		bm = GetMatrix();
		bm.d = v1;
		bm.d += 0.5f*bm.a*m_CPUTimeLabel.ComputeWidth();
		bm.d += 0.5f*bm.b*m_CPUTimeLabel.ComputeHeight();
		m_CPUTimeLabel.SetMatrix(bm);
		m_CPUTimeLabel.SetShouldDraw(ShouldDraw());
		m_CPUTimeLabel.SetSize(0.22f);
		
		synthUIView::Update();

		// ctrl-delete to delete a group
		if(!synthUIPlane::GetMaster()->IsInteractionDisabled() && IsUnderMouse() && 
			synthUIPlane::GetMaster()->IsGroupKeyDown() &&	ioKeyboard::KeyPressed(KEY_DELETE))
		{
			m_ShouldBeDeleted = true;
			// if shift is held down then also delete all modules in this group
			if(ioKeyboard::KeyDown(KEY_SHIFT))
			{
				for(atArray<synthUIView*>::iterator iter = m_Contents.begin(); iter != m_Contents.end(); iter++)
				{
                    (*iter)->DeleteContents();
				}
			}
		}
		if(m_TitleLabel.IsUnderMouse() && synthUIPlane::GetMaster()->HadLeftDoubleClick())
		{
			if(synthSynthesizerView::SetModalComponent(&m_InputBox))
			{
				synthUIPlane::GetMaster()->SetIsKeyboardLocked(true);
				m_InputBox.Reset();
				m_InputBox.SetValue(m_NameBuf);
				m_InputBox.SetShouldDraw(true);
				m_InputBox.SetAllowAlphaChars(true);
				m_InputBox.SetTitle("Group Name");
				m_InputBox.Update();
			}
		}

		if(m_InputBox.ShouldDraw())
		{
			m_InputBox.Update();
			if(m_InputBox.HadEnter())
			{
				if(strlen(m_InputBox.GetKeyboardBuffer()))
				{
					strncpy(m_NameBuf, m_InputBox.GetKeyboardBuffer(), sizeof(m_NameBuf));
				}
				m_InputBox.SetShouldDraw(false);
				synthSynthesizerView::ClearModalComponent();
				synthUIPlane::GetMaster()->SetIsKeyboardLocked(false);
			}
		}

		bool moduleUnderMouse = false;
		f32 cpuSum = 0.f;
		for(s32 i = 0; i < m_Contents.GetCount(); i++)
		{
			if(m_Contents[i]->IsUnderMouse())
			{
				moduleUnderMouse = true;
			}

			cpuSum += m_Contents[i]->GetProcessingTime();
		}
		m_ModuleUnderMouse = moduleUnderMouse;
		const f32 cpuRatio = cpuSum / ((kMixBufNumSamples / (f32)kMixerNativeSampleRate) * 10.f);
		formatf(m_CPUTimeBuf, "Group: %.2f%%", cpuRatio);
		m_CPUTimeLabel.SetString(m_CPUTimeBuf);
	}

	void synthUIViewGroup::Render()
	{
		Vector3 v1,v2;
		GetBounds(v1,v2);
		DrawGroupBox(v1,v2);
		synthUIView::Render();
	}

	void synthUIViewGroup::DrawGroupBox(const Vector3 &v1, const Vector3 &v2)
	{
		Vector3 m = (v1+v2)*0.5f;
		Matrix34 bm = GetMatrix();
		bm.d = m;
		const f32 height = bm.b.Dot(v2-v1);
		const f32 width = bm.a.Dot(v2-v1);
		bm.Scale(width * 0.5f, height * 0.5f, 1.f);
		bm.d += bm.c * 0.1f;

		bool isTransparent = m_IsTransparent;
		if(!isTransparent)
		{
			// see if any of our modules are under the mouse
			for(atArray<synthUIView*>::iterator iter = m_Contents.begin(); iter != m_Contents.end(); iter++)
			{
				if((*iter)->IsUnderMouse())
				{
					isTransparent = true;
					break;
				}
			}
		}

		grcWorldMtx(bm);
		SetShaderVar("g_TextColour", m_Colour);
		SetShaderVar("g_CurrentValue", isTransparent ? 0.18f : 0.24f);
		SetShaderVar("g_AlphaFade", GetAlpha());
		DrawVB("drawgroup");
	}

    float synthUIViewGroup::GetProcessingTime() const
    {
        float processingTime = 0.f;
        for(s32 i = 0; i < m_Contents.GetCount(); i++)
        {
            processingTime += m_Contents[i]->GetProcessingTime();
        }
        return processingTime;
    }

    void synthUIViewGroup::DeleteContents()
    {
        for(s32 i = 0; i < m_Contents.GetCount(); i++)
        {
            m_Contents[i]->DeleteContents();
        }
    }

	void synthUIViewGroup::FindModules(atArray<synthModuleView*> &moduleList)
	{
		for(s32 i = 0; i < GetContents().GetCount(); i++)
		{
			if(GetContents()[i]->IsModule())
			{
				moduleList.Grow() = (synthModuleView*)GetContents()[i];
			}
			else if(GetContents()[i]->IsGroup())
			{
				((synthUIViewGroup*)GetContents()[i])->FindModules(moduleList);
			}
		}
	}

	void synthUIViewGroup::FindGroups(atArray<synthUIViewGroup*> &groupList)
	{
		for(s32 i = 0; i < GetContents().GetCount(); i++)
		{
			if(GetContents()[i]->IsGroup())
			{
				synthUIViewGroup *group = ((synthUIViewGroup*)GetContents()[i]);
				groupList.Grow() = group;
				group->FindGroups(groupList);
			}
		}
	}

	synthUIViewGroup *synthUIViewGroup::Clone() const
	{
		synthUIViewGroup *newGroup = rage_new synthUIViewGroup();
		newGroup->SetName(GetName());
		newGroup->SetMatrix(GetMatrix());
		newGroup->SetColour(GetColour());
		return newGroup;
	}

	synthUIView *synthUIViewGroup::DuplicateContents(const bool UNUSED_PARAM(recreateConnections))
	{
		synthUIViewGroup *newParentGroup = Clone();

		atArray<synthModuleView *> oldModules;
		atArray<synthModuleView *> newModules;
		atArray<synthUIViewGroup *> oldGroups;
		atArray<synthUIViewGroup *> newGroups;

		oldGroups.Grow() = this;
		newGroups.Grow() = newParentGroup;

		FindModules(oldModules);
		FindGroups(oldGroups);

		for(s32 i = 1; i < oldGroups.GetCount(); i++)
		{
			synthUIViewGroup *newGroup = oldGroups[i]->Clone();
			newGroups.Grow() = newGroup;
		}

		for(s32 i = 0; i < oldModules.GetCount(); i++)
		{
			synthModuleView *newModule = (synthModuleView*)oldModules[i]->DuplicateContents(false);
			newModules.Grow() = newModule;
			
			if(newModule)
			{
				if(oldModules[i]->IsGrouped())
				{
					newGroups[oldGroups.Find(oldModules[i]->GetGroup())]->Add(newModule);
				}
			}
		}		

		for(s32 i = 1; i < oldGroups.GetCount(); i++)
		{
			newGroups[oldGroups.Find(oldGroups[i]->GetGroup())]->Add(newGroups[i]);
		}

		// Some module types don't allow duplication, in which case we can end up with empty groups
		// Clear them out here.
		for(s32 i = 0; i < newGroups.GetCount(); i++)
		{
			atArray<synthModuleView*> moduleList;
			newGroups[i]->FindModules(moduleList);
			if(moduleList.GetCount() == 0)
			{
				delete newGroups[i];
				newGroups[i] = NULL;
			}
		}
		// If this entire operation resulted in an empty group bail out
		if(newGroups[0] == NULL)
		{
			return NULL;
		}

		// recreate input connections
		for(s32 i = 0; i < oldModules.GetCount(); i++)
		{	
			if(oldModules[i] && newModules[i])
			{
				synthPin *inputs;
				u32 numInputs;
			
				synthModule *oldModule = oldModules[i]->GetModule();
				synthModule *newModule = newModules[i]->GetModule();		

				oldModule->GetInputs(inputs, numInputs);
				for(u32 k = 0; k < numInputs; k++)
				{
					if(inputs[k].IsConnected())
					{
						Assert(inputs[k].GetOtherPin());
						Assert(inputs[k].GetOtherPin()->GetParentModule());
						Assert(inputs[k].GetOtherPin()->GetParentModule() != oldModule);
						const s32 parentIndex = oldModules.Find(inputs[k].GetOtherPin()->GetParentModule()->GetView());
						if(parentIndex != -1)
						{
							// we have a connection - find out what pin it is
							synthPin *outputs;
							u32 numOutputs;
							inputs[k].GetOtherPin()->GetParentModule()->GetOutputs(outputs, numOutputs);
							u32 outputIndex = 0;
							for(; outputIndex < numOutputs; outputIndex++)
							{
								if(&outputs[outputIndex] == inputs[k].GetOtherPin())
								{
									break;
								}
							}
							Assert(outputIndex < numOutputs);
							newModule->GetInput(k).Connect(newModules[parentIndex]->GetModule()->GetOutput(outputIndex));				
						}
						else
						{
							// connect to the external source
							synthPin *outputs;
							u32 numOutputs;
							inputs[k].GetOtherPin()->GetParentModule()->GetOutputs(outputs, numOutputs);
							u32 outputIndex = 0;
							for(; outputIndex < numOutputs; outputIndex++)
							{
								if(&outputs[outputIndex] == inputs[k].GetOtherPin())
								{
									Assert(outputIndex < numOutputs);
									newModule->GetInput(k).Connect(outputs[outputIndex]);
									break;
								}
							}						
						}
					}
				}
			}		
		}

		Matrix34 mat = GetMatrix();
		newParentGroup->SetMatrix(mat);
		synthSynthesizerView::Get()->GetSynth()->ComputeProcessingGraph();
		return newParentGroup;
	}
}
#endif // __SYNTH_EDITOR
