
#ifndef SYNTH_MULTITAPDELAY_H
#define SYNTH_MULTITAPDELAY_H
#include "synthdefs.h"

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum kMultitapNumInputs
	{
		kMultitapSignal = 0,
		kMultitapTap1,
		kMultitapTap1_Gain,
		kMultitapTap1_Feedback,
		kMultitapTap2,
		kMultitapTap2_Gain,
		kMultitapTap2_Feedback,
		kMultitapTap3,
		kMultitapTap3_Gain,
		kMultitapTap3_Feedback,
		kMultitapTap4,
		kMultitapTap4_Gain,
		kMultitapTap4_Feedback,

		kMultitapNumInputs
	};

	class synthMultiTapDelay : public synthModuleBase<kMultitapNumInputs, 1, SYNTH_MULTITAPDELAY>
	{
	public:

		SYNTH_MODULE_NAME("MultitapDelay");

		synthMultiTapDelay();
		virtual ~synthMultiTapDelay();

		virtual void Synthesize();

		enum
		{
			MAX_DELAY_SAMPLES = 48000,
		};

		void SetDelayLength(const u32 lengthSamples)
		{
			m_LengthSamples = lengthSamples;
		}

	private:

		float ReadTap(const u32 index);

		synthSampleFrame m_OutputBuffer;
		f32 *m_DelayBuffer;
		s32 m_DelayWriteIndex;

		u32 m_LengthSamples;
	};
}

#endif // SYNTH_MULTITAPDELAY_H



