// 
// audiosynth/sampleandholdview.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "sampleandhold.h"
#include "sampleandholdview.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

namespace rage {

	synthSampleAndHoldView::synthSampleAndHoldView(synthModule *module) : synthModuleView(module)
	{
		m_Mode.AddItem("Static Output");
		m_Mode.AddItem("Dynamic Output");
		AddComponent(&m_Mode);

		m_Mode.SetCurrentIndex(GetSampleAndHold()->GetDynamicOutput() ? 1 : 0);
	}

	void synthSampleAndHoldView::Update()
	{
		GetSampleAndHold()->SetDynamicOutput(m_Mode.GetCurrentIndex() == 1);
		Matrix34 mat = GetMatrix();
		Vector3 offset = -mat.b;
		mat.d += offset;
		//m_Mode.SetMenuOffset(-offset);
		m_Mode.SetMatrix(mat);
		m_Mode.SetAlpha(GetHoverFade());

		synthModuleView::Update();

		m_TitleLabel.SetShouldDraw(!m_Mode.IsShowingMenu());
	}

	bool synthSampleAndHoldView::StartDrag() const
	{
		return !m_Mode.IsUnderMouse() && !m_Mode.IsShowingMenu();
	}

} // namespace rage

#endif // __SYNTH_EDITOR
