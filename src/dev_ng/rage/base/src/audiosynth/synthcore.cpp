// 
// audiosynth/synthcore.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
//

#include "synthcore.h"
#include "synthcoredisasm.h"
#include "synthesizer.h"
#include "synthobjects.h"
#include "synthopcodes.h"
#include "synthutil.h"

#include "1polelpf.h"
#include "abs.h"
#include "awnoise.h"
#include "allpass.h"
#include "biquadfilter.h"
#include "ceil.h"
#include "clipper.h"
#include "counter.h"
#include "compressor.h"
#include "decimator.h"
#include "envelopefollower.h"
#include "envelopegenerator.h"
#include "floor.h"
#include "gate.h"
#include "hardknee.h"
#include "max.h"
#include "min.h"
#include "mod.h"
#include "note2freq.h"
#include "noisegenerator.h"
#include "oscillator.h"
#include "power.h"
#include "randomizer.h"
#include "rescaler.h"
#include "round.h"
#include "sampleandhold.h"
#include "sign.h"
#include "smalldelay.h"
#include "switch.h"
#include "timedtrigger.h"
#include "trigger.h"

#include "audioengine/engineutil.h"
#include "audioengine/spuutil.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/framebuffercache.h"
#include "audiohardware/framebufferpool.h"
#include "math/amath.h"
#include "system/performancetimer.h"
#include "vectormath/vectormath.h"

#include "audiohardware/mixing_vmath.inl"

namespace rage
{
atRangeArray<float *, synthCore::kMaxBuffers-1> synthCore::sm_WorkingBuffers;
Vec::Vector_4V synthUtil::sm_RandomSeed;
Vec::Vector_4V synthCore::sm_ZeroState;

SYNTH_ASSERT_ONLY(size_t synthCore::sm_ProcessingOffset = 0);
SYNTH_ASSERT_ONLY(u32 synthCore::sm_ProcessingOpcode = 0xff);
#if __ASSERT
atMap<u32, float> synthCore::sm_AssetPeakLevels;
#endif

BANK_ONLY(extern bool g_PcmSourceInjectNaN);
#define SYNTH_VALIDATE_INTERNAL_STATE 0

#if SYNTH_PROFILE
synthCore::synthProfileData synthCore::sm_ProfileData;
#endif // SYNTH_PROFILE

#if __SPU
	u8 *synthCore::sm_LocalProgramStorage = NULL;
	const u16 *synthCore::sm_LocalProgramEntry = NULL;

	enum { kMaxProgramSize = 8*1024 };

	void synthCore::InitClass()
	{		
		for(u32 i = 0; i < kMaxBuffers-1; i++)
		{
			sm_WorkingBuffers[i] = AllocateFromScratch<float>(kMixBufNumSamples, 16, "Synth working buffer");
		}

		sm_LocalProgramStorage = AllocateFromScratch<u8>(kMaxProgramSize, 16, "LocalProgramStorage");
	}

	const u16 *synthCore::FetchProgram(const s32 tag) const
	{
		const size_t totalSizeUnaligned = m_ProgramSizeBytes + 4 + (m_NumConstants * sizeof(float));

		// Grab from the nearest 16 byte aligned address
		const size_t unalignedEA = (size_t)m_ProgramEntry;
		const size_t alignedEA = unalignedEA & ~15;
		const size_t extraData = unalignedEA - alignedEA;
		// Align the total fetch size up to the nearest 16 byte boundary
		const size_t totalSize = (totalSizeUnaligned+extraData+15) & ~15;
		
		if(!audVerifyf(totalSize <= kMaxProgramSize, "Program size too large: %zu", totalSize+extraData))
		{
			return NULL;
		}

		sysDmaGet(sm_LocalProgramStorage, alignedEA, totalSize, tag);

		return (sm_LocalProgramEntry = (const u16 *)(sm_LocalProgramStorage + extraData));
	}

#else
	void synthCore::InitClass()
	{
		for(u32 i = 0; i < kMaxBuffers-1; i++)
		{
			sm_WorkingBuffers[i] = rage_aligned_new(128) float[kMixBufNumSamples];
		}

		sm_ZeroState = V4VConstant(V_ZERO);

		synthUtil::InitClass();

		SYNTH_PROFILE_ONLY(sysMemZeroBytes<sizeof(sm_ProfileData)>(&sm_ProfileData));
	}

	void synthCore::ShutdownClass()
	{
		for(u32 i = 0; i < kMaxBuffers-1; i++)
		{
			delete sm_WorkingBuffers[i];
		}
	}

	const u16 *synthCore::FetchProgram(const s32 UNUSED_PARAM(tag)) const
	{
		return m_ProgramEntry;
	}
#endif

	u32 synthCore::GetCostEstimate(const u32 synthNameHash)
	{
		const CompiledSynth *metadata = synthSynthesizer::FindOptimizedMetadata(synthNameHash);		
		if(!metadata)
		{
			return 0;
		}

		return metadata->costEstimate;
	}

	int synthCore::Compare(const InputVariableMap *a, const InputVariableMap *b)
	{
		//return static_cast<int>(a->NameHash - b->NameHash);
		if(a->NameHash == b->NameHash)
			return 0;
		else if(a->NameHash > b->NameHash)
			return 1;
		else
			return -1;
	}
		
	using namespace Vec;
	bool synthCore::Init(const u32 synthNameHash)
	{
		m_IsFinished = false;

		const CompiledSynth *metadata = synthSynthesizer::FindOptimizedMetadata(synthNameHash);
		if(!metadata)
		{
			m_IsFinished = true;
			return false;
		}

		BANK_ONLY(m_AssetNameHash = synthNameHash);
		DEV_ONLY(m_SynthName = synthSynthesizer::GetMetadataManager().GetObjectNameFromNameTableOffset(metadata->NameTableOffset));

		Assertf(metadata->NumRegisters <= kMaxRegisters, "%s: Num registers: %u, limit %u", m_SynthName, metadata->NumRegisters, kMaxRegisters);
		Assertf(metadata->NumBuffers <= kMaxBuffers, "%s: Num buffers: %u, limit %u", m_SynthName, metadata->NumBuffers, kMaxBuffers);
		Assertf(metadata->numOutputs <= audPcmSource::kMaxPcmSourceChannels, "%s: Num outputs %u, limit %u", m_SynthName, metadata->numOutputs, audPcmSource::kMaxPcmSourceChannels);
		
		if(metadata->NumRegisters > kMaxRegisters || metadata->NumBuffers > kMaxBuffers || metadata->numOutputs > audPcmSource::kMaxPcmSourceChannels)
		{
			return false;
		}

		Assign(m_NumBuffers, metadata->NumBuffers);
		Assign(m_NumRegisters, metadata->NumRegisters);

		Assign(m_NumOutputs, metadata->numOutputs);
		for(u32 i = 0; i < m_NumOutputs; i++)
		{
			m_OutputBufferMap[i] = metadata->Outputs[i].BufferId;
		}

		m_ProgramEntry = &metadata->Operations;
		Assign(m_ProgramSizeBytes, metadata->programSizeBytes);
		Assign(m_NumStateBlocks, metadata->numStateBlocks);
		if(!audVerifyf(m_NumStateBlocks <= kMaxStateBlocks, "%s: Num state blocks: %u, limit %u", m_SynthName, m_NumStateBlocks, kMaxStateBlocks))
		{
			m_IsFinished = true;
			return false;
		}

		const u32 *numConstants = (u32*)((u8*)m_ProgramEntry + metadata->programSizeBytes);
		Assign(m_NumConstants, *numConstants);

		const u32 *numInputVariables = (u32*)(GetConstants() + m_NumConstants);

		Assertf(*numInputVariables <= kMaxVariables, "%s: Num variables %u, limit is %u", m_SynthName, *numInputVariables, kMaxVariables);

		m_InputVariableMap.SetCount(*numInputVariables);
		m_InputVariableValues.SetCount(*numInputVariables);
		numInputVariables++;
		CompiledSynth::tInputVariables *var = (CompiledSynth::tInputVariables*)numInputVariables;
		for(s32 i = 0; i < m_InputVariableMap.GetCount(); i++)
		{
			m_InputVariableMap[i].NameHash = var[i].Name;
			m_InputVariableMap[i].Index = i;
			// Always default to unity frequency
			if(m_InputVariableMap[i].NameHash == ATSTRINGHASH("Frequency", 0x31FF1BC8))
			{
				m_InputVariableValues[i] = 1.f;
			}
			else
			{
				m_InputVariableValues[i] = var[i].InitialValue;
			}			
		}

		// Enable binary searching
		m_InputVariableMap.QSort(0, -1, synthCore::Compare);
		
		NOTFINAL_ONLY(m_RuntimeMs = 0.f);

		return true;
	}

	void synthCore::Shutdown()
	{
#if __ASSERT
		if(m_RuntimeMs > 0.f && m_PeakLevelLin <= 0.5f)
		{			
			float *prevMinPeak = sm_AssetPeakLevels.Access(m_AssetNameHash);
			if(prevMinPeak)
			{
				if(m_PeakLevelLin < *prevMinPeak)
				{
					//audWarningf("%s: New low peak level %fdB after %fms", m_SynthName, audDriverUtil::ComputeDbVolumeFromLinear(m_PeakLevelLin), m_RuntimeMs);
					*prevMinPeak = m_PeakLevelLin;
				}
			}
			else
			{
				//audWarningf("%s: Low peak level %fdB after %fms", m_SynthName, audDriverUtil::ComputeDbVolumeFromLinear(m_PeakLevelLin), m_RuntimeMs);
				sm_AssetPeakLevels.Insert(m_AssetNameHash, m_PeakLevelLin);
			}
		}
#endif
	}

	void synthCore::StartPhys()
	{
		// Allocate state blocks		
		if(m_NumStateBlocks)
		{
			const u32 numStateBlockSlotsToAllocate = m_NumStateBlocks > 32 ? 2 : 1;
			for(u32 i = 0; i < numStateBlockSlotsToAllocate; i++)
			{
				const u32 slot = g_FrameBufferPool->Allocate();
				if(slot == ~0U)
				{
					m_IsFinished = true;
					return;
				}
				g_FrameBufferCache->ZeroBuffer(slot);
				m_StateBlockIds.Append() = (u16)slot;
			}
		}
	}

	void synthCore::StopPhys()
	{
		for(s32 i = 0; i < m_StateBlockIds.GetCount(); i++)
		{
			g_FrameBufferPool->Free(m_StateBlockIds[i]);
		}
		m_StateBlockIds.Reset();
	}

	void synthCore::GenerateFrame(const float **inputBuffers, const atRangeArray<u16, audPcmSource::kMaxPcmSourceChannels> &outputBufferIds)
	{
#if __PPU
		// synthcore runs on SPU on PS3
		Assert(0);
		(void)outputBufferIds;
		(void)inputBuffers;
#else
		atRangeArray<float, kMaxRegisters> registers;
		atRangeArray<float *, kMaxBuffers> buffers;

#if !__FINAL
		sysPerformanceTimer timer("synthCoreTimer");
		timer.Start();
#endif

		sysMemZeroBytes<sizeof(buffers)>(&buffers);

		for(u32 outputIndex = 0; outputIndex < m_NumOutputs; outputIndex++)
		{
			Assert(outputBufferIds[outputIndex] != audFrameBufferPool::InvalidId);
			const u32 bufferId = outputBufferIds[outputIndex];
			g_FrameBufferCache->ZeroBuffer(bufferId);
			buffers[m_OutputBufferMap[outputIndex]] = g_FrameBufferCache->GetBuffer(bufferId);

		}

		if(synthSynthesizer::IsMetadataUnloading(m_ProgramEntry))
		{
			audDisplayf("Synth unloading %p", m_ProgramEntry);
			m_IsFinished = true;
		}

		if(m_IsFinished || (m_IsPaused && m_IsPauseRequested))
		{
			return;
		}
		
		u32 currentWorkingBuffer = 0;
		for(u32 i = 0; i < m_NumBuffers; i++)
		{
			if(buffers[i] == NULL)
			{
				buffers[i] = sm_WorkingBuffers[currentWorkingBuffer++];
			}
		}

#if SYNTH_VALIDATE_INTERNAL_STATE
		sysMemZeroBytes<sizeof(registers)>(&registers);
		for(u32 i = 0; i < m_NumBuffers; i++)
		{
			sysMemZeroBytes<sizeof(float) * kMixBufNumSamples>(buffers[i]);
		}
#endif

#if __SPU
		for(s32 i = 0; i < m_StateBlockIds.GetCount(); i++)
		{		
			// Prefetch only; we'll block later
			g_FrameBufferCache->PrefetchBuffer(m_StateBlockIds[i]);
		}
#endif

		synthEnvelopeGenerator::synthEnvelopeReleaseType envelopeReleaseType;
		synthEnvelopeGenerator::synthEnvelopeTriggerMode envelopeTriggerMode;
		
		const s32 tag = 7;
		const u16 *pc = FetchProgram(tag);
		
		for(s32 i = 0; i < m_StateBlockIds.GetCount(); i++)
		{
			m_StateBlockPtrs[i] = (Vector_4V*)g_FrameBufferCache->GetBuffer(m_StateBlockIds[i]);
		}

		SPU_ONLY(sysDmaWait(1<<tag));

		SYNTH_ASSERT_ONLY(const u16 *entryPoint = pc);
		
		bool finished = false;
		while(!finished)
		{
			ASSERT_ONLY(const u32 opcodeVal = *pc);
			synthOpcodeDecoder opcode = *(synthOpcodeDecoder*)pc++;
						
			SYNTH_ASSERT_ONLY(const u32 numInputs = opcode.numInputs);
			SYNTH_ASSERT_ONLY(const u32 numOutputs = opcode.numOutputs);

#if SYNTH_PROFILE
			sysPerformanceTimer opcodeTimer("opcodeTimer");
			opcodeTimer.Start();
#endif // SYNTH_PROFILE

			SYNTH_ASSERT_ONLY(sm_ProcessingOpcode = opcode.opcode);
			SYNTH_ASSERT_ONLY(sm_ProcessingOffset = (size_t)(pc - entryPoint));
		
			switch(opcode.opcode)
			{
			case synthOpcodes::END:
				finished = true;
				break;

			case synthOpcodes::COPY_BUFFER:
				{
					synthAssert(numInputs == 1);
					synthAssert(numOutputs >= 1);
					synthValidateReferenceType(*pc, 'B');
					const u32 inputId = decode_synth_ref_id(*pc++);
					
					if(synthVerify(inputId < m_NumBuffers))
					{
						const u32 numDestinations = opcode.numOutputs;
						audAssert(numDestinations > 1);
						for(u32 i = 0; i < numDestinations - 1; i++)
						{
							synthValidateReferenceType(*pc, 'B');
							const u32 outputId = decode_synth_ref_id(*pc++);

							if(synthVerify(outputId != inputId))
							{
								if (synthVerify(outputId < m_NumBuffers) && buffers[outputId] && buffers[inputId])
								{
									synthUtil::CopyBuffer(reinterpret_cast<Vector_4V*>(buffers[outputId]), reinterpret_cast<const Vector_4V*>(buffers[inputId]), kMixBufNumSamples);
								}							
							}						
						}
					}

					ASSERT_ONLY(const u32 actualInputId = *)pc++;
						
					Assert(actualInputId == inputId);
				}
				break;

			case synthOpcodes::COPY_SCALAR:
				{
					synthAssert(numInputs == 1);
					synthAssert(numOutputs >= 1);
					synthValidateReferenceType(*pc, 'R');
					const u32 inputId = decode_synth_ref_id(*pc++);
					const float inputValue = registers[inputId];
					const u32 numDestinations = opcode.numOutputs;
					audAssert(numDestinations > 1);
					for(u32 i = 0; i < numDestinations - 1; i++)
					{
						synthValidateReferenceType(*pc, 'R');
						const u32 outputId = decode_synth_ref_id(*pc++);

						synthAssert(outputId != inputId);				
						registers[outputId] = inputValue;
					}

					ASSERT_ONLY(const u32 actualInputId = *)pc++;
					Assert(actualInputId == inputId);
				}
				break;
				
				IMPLEMENT_UNARY_BUFFER(synthOpcodes::CONVERT_BUFFER_TO_SIGNAL, synthUtil::ConvertBufferToSignal);
				IMPLEMENT_UNARY_SCALAR(synthOpcodes::CONVERT_SCALAR_TO_SIGNAL, SCALAR_NtoS);
				IMPLEMENT_UNARY_BUFFER(synthOpcodes::CONVERT_BUFFER_TO_NORMALIZED, synthUtil::ConvertBufferToNormalised);
				IMPLEMENT_UNARY_SCALAR(synthOpcodes::CONVERT_SCALAR_TO_NORMALIZED, SCALAR_StoN);
			case synthOpcodes::SAMPLE_BUFFER:
				{
					synthAssert(numInputs == 1);
					synthAssert(numOutputs == 1);
					synthValidateReferenceType(*pc, 'R');
					const u32 outputId = decode_synth_ref_id(*pc++);
					synthValidateReferenceType(*pc, 'B');
					const u32 inputId = decode_synth_ref_id(*pc++);
					registers[outputId] = buffers[inputId][0];
				}
				break;
				IMPLEMENT_BINARY_BUFFER(synthOpcodes::MULTIPLY_BUFFER_BUFFER, synthUtil::ScaleBuffer);
				IMPLEMENT_BINARY_BUFFER_SCALAR(synthOpcodes::MULTIPLY_BUFFER_SCALAR, synthUtil::ScaleBuffer);
				IMPLEMENT_BINARY_SCALAR(synthOpcodes::MULTIPLY_SCALAR_SCALAR, SCALAR_MULTIPLY);
				IMPLEMENT_BINARY_BUFFER(synthOpcodes::SUM_BUFFER_BUFFER, synthUtil::Sum);
				IMPLEMENT_BINARY_BUFFER_SCALAR(synthOpcodes::SUM_BUFFER_SCALAR, synthUtil::Sum);
				IMPLEMENT_BINARY_SCALAR(synthOpcodes::SUM_SCALAR_SCALAR, SCALAR_SUM);								 
				IMPLEMENT_BINARY_BUFFER(synthOpcodes::SUBTRACT_BUFFER_BUFFER, synthUtil::Subtract);
				IMPLEMENT_BINARY_BUFFER_SCALAR(synthOpcodes::SUBTRACT_BUFFER_SCALAR, synthUtil::Subtract);
			case synthOpcodes::SUBTRACT_SCALAR_BUFFER:
				{
					synthAssert(numInputs == 2);
					synthAssert(numOutputs == 1);
					synthValidateReferenceType(*pc, 'B');
					const u32 inputOutputId = decode_synth_ref_id(*pc++);
					const Vector_4V scalarV = V4LoadScalar32IntoSplatted(LoadScalarReference(*pc++, registers));
					
					// This instruction isn't marked replacing, since that assumes the first input is the output
					// Skip the redundant input
					ASSERT_ONLY(u32 bufferInputId = *)pc++;
					Assert(bufferInputId == inputOutputId);
					synthUtil::Subtract(scalarV, buffers[inputOutputId], kMixBufNumSamples);
				}
				break;
				IMPLEMENT_BINARY_SCALAR(synthOpcodes::SUBTRACT_SCALAR_SCALAR, SCALAR_SUBTRACT);
				IMPLEMENT_BINARY_BUFFER(synthOpcodes::DIVIDE_BUFFER_BUFFER, synthUtil::Divide);
				IMPLEMENT_BINARY_BUFFER_SCALAR(synthOpcodes::DIVIDE_BUFFER_SCALAR, synthUtil::Divide);
				IMPLEMENT_BINARY_SCALAR(synthOpcodes::DIVIDE_SCALAR_SCALAR, SCALAR_DIVIDE);

			case synthOpcodes::RESCALE_BUFFER_BUFFER:
				{
					synthAssert(numInputs == 5);
					synthAssert(numOutputs == 1);
					synthValidateReferenceType(*pc, 'B');
					const u32 outputId = decode_synth_ref_id(*pc++);
					synthValidateReferenceType(*pc, 'B');
					const u32 minInId = decode_synth_ref_id(*pc++);
					synthValidateReferenceType(*pc, 'B');
					const u32 maxInId = decode_synth_ref_id(*pc++);
					synthValidateReferenceType(*pc, 'B');
					const u32 minOutId = decode_synth_ref_id(*pc++);
					synthValidateReferenceType(*pc, 'B');
					const u32 maxOutId = decode_synth_ref_id(*pc++);
					synthRescaler::Process(buffers[outputId], buffers[minInId], buffers[maxInId], buffers[minOutId], buffers[maxOutId], kMixBufNumSamples);
				}
				break;
			case synthOpcodes::RESCALE_BUFFER_SCALAR:
				{
					synthAssert(numInputs == 5);
					synthAssert(numOutputs == 1);
					synthValidateReferenceType(*pc, 'B');
					const u32 outputId = decode_synth_ref_id(*pc++);
					synthValidateScalarReference(*pc);
					const float minIn = LoadScalarReference(*pc++, registers);
					synthValidateScalarReference(*pc);
					const float maxIn = LoadScalarReference(*pc++, registers);
					synthValidateScalarReference(*pc);
					const float minOut = LoadScalarReference(*pc++, registers);
					synthValidateScalarReference(*pc);
					const float maxOut = LoadScalarReference(*pc++, registers);

					synthRescaler::Process(buffers[outputId], minIn, maxIn, minOut, maxOut, kMixBufNumSamples);
				}
				break;
			case synthOpcodes::RESCALE_SCALAR:
				{
					synthAssert(numInputs == 5);
					synthAssert(numOutputs == 1);
					synthValidateReferenceType(*pc, 'R');
					const u32 outputId = decode_synth_ref_id(*pc++);
					synthValidateScalarReference(*pc);
					const float input = LoadScalarReference(*pc++, registers);
					synthValidateScalarReference(*pc);
					const float minIn = LoadScalarReference(*pc++, registers);
					synthValidateScalarReference(*pc);
					const float maxIn = LoadScalarReference(*pc++, registers);
					synthValidateScalarReference(*pc);
					const float minOut = LoadScalarReference(*pc++, registers);
					synthValidateScalarReference(*pc);
					const float maxOut = LoadScalarReference(*pc++, registers);

					registers[outputId] = synthRescaler::Process(input, minIn, maxIn, minOut, maxOut);
				}
				break;

				IMPLEMENT_BINARY_BUFFER_COND(synthOpcodes::HARDKNEE_BUFFER, synthHardKnee::Process);
				IMPLEMENT_BINARY_SCALAR(synthOpcodes::HARDKNEE_SCALAR, synthHardKnee::Process);

				case synthOpcodes::NOISE:
					{
						synthAssert(numInputs == 0);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'B');

						const u32 outputId = decode_synth_ref_id(*pc++);
						synthNoiseGenerator::Generate(buffers[outputId], kMixBufNumSamples);
					}
					break;

				case synthOpcodes::RANDOM:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'R');

						const u32 outputRegId = decode_synth_ref_id(*pc++);
						const float trigger = LoadScalarReference(*pc++, registers);
						const float min = LoadScalarReference(*pc++, registers);
						const float max = LoadScalarReference(*pc++, registers);
						registers[outputRegId] = synthRandomizer::Process(trigger, min, max, GetStateBlock(*pc++));
					}
					break;
				IMPLEMENT_UNARY_BUFFER(synthOpcodes::ABS_BUFFER, synthAbs::Process);
				IMPLEMENT_UNARY_SCALAR(synthOpcodes::ABS_SCALAR, Abs);
				IMPLEMENT_UNARY_BUFFER(synthOpcodes::FLOOR_BUFFER, synthFloor::Process);
				IMPLEMENT_UNARY_SCALAR(synthOpcodes::FLOOR_SCALAR, Floorf);
				IMPLEMENT_UNARY_BUFFER(synthOpcodes::CEIL_BUFFER, synthCeil::Process);
				IMPLEMENT_UNARY_SCALAR(synthOpcodes::CEIL_SCALAR, ceilf);
				case synthOpcodes::LERP_BUFFER:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'B');

						const u32 outputId = decode_synth_ref_id(*pc++);
						synthValidateScalarReference(*pc);
						const float in1 = LoadScalarReference(*pc++, registers);
						const float in2 = LoadScalarReference(*pc++, registers);
						synthRescaler::Process(buffers[outputId], in1, in2, kMixBufNumSamples);
					}
					break;
				case synthOpcodes::LERP_BUFFER_BUFFER:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 1);
						
						synthValidateReferenceType(*pc, 'B');
						const u32 outputId = decode_synth_ref_id(*pc++);

						const float t = LoadScalarReference(*pc++, registers);
						synthValidateReferenceType(*pc, 'B');
						const u32 inputA = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc, 'B');
						const u32 inputB = decode_synth_ref_id(*pc++);

						float *bufA,*bufB;
						if(outputId == inputA)
						{
							bufA = buffers[inputA];
							bufB = buffers[inputB];
						}
						else
						{
							synthAssert(outputId == inputB);
							bufA = buffers[inputB];
							bufB = buffers[inputA];
						}
						
						synthRescaler::Process(bufA, bufB, t, kMixBufNumSamples);
					}
					break;
				case synthOpcodes::LERP_THREE_BUFFERS:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 1);

						synthValidateReferenceType(*pc, 'B');
						const u32 inOutId = decode_synth_ref_id(*pc++);
						
						synthValidateReferenceType(*pc, 'B');
						const u32 inputA = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc, 'B');
						const u32 inputB = decode_synth_ref_id(*pc++);


						synthRescaler::Process(buffers[inOutId], buffers[inputA], buffers[inputB], kMixBufNumSamples);
					}
					break;
				IMPLEMENT_TERNARY_SCALAR(synthOpcodes::LERP_SCALAR, Lerp);
				IMPLEMENT_UNARY_BUFFER(synthOpcodes::ROUND_BUFFER, synthRound::Process);
				IMPLEMENT_UNARY_SCALAR(synthOpcodes::ROUND_SCALAR, (float)round);
				IMPLEMENT_UNARY_BUFFER(synthOpcodes::SIGN_BUFFER, synthSign::Process);
				IMPLEMENT_UNARY_SCALAR(synthOpcodes::SIGN_SCALAR, Sign);
				IMPLEMENT_BINARY_BUFFER_COND(synthOpcodes::MODULUS_BUFFER, synthModulus::Process);
				IMPLEMENT_BINARY_SCALAR(synthOpcodes::MODULUS_SCALAR, synthModulus::Process);
				IMPLEMENT_BINARY_BUFFER_COND(synthOpcodes::POWER_BUFFER, synthPower::Process);
				IMPLEMENT_BINARY_SCALAR(synthOpcodes::POWER_SCALAR, synthPower::Process);

				IMPLEMENT_BINARY_BUFFER_SCALAR_WITH_STATE(synthOpcodes::MAX_BUFFER, synthMax::Process);

				case synthOpcodes::MAX_SCALAR:
					{
						synthAssert(numInputs == 2);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc,'R');
						const u32 outputReg = decode_synth_ref_id(*pc++);
						synthValidateScalarReference(*pc);
						const float input0 = LoadScalarReference(*pc++, registers);
						synthValidateScalarReference(*pc);
						const float triggerValue = LoadScalarReference(*pc++, registers);

						registers[outputReg] = synthMax::Process(input0, triggerValue, GetStateBlock(*pc++));
					}
					break;
				

				IMPLEMENT_BINARY_BUFFER(synthOpcodes::HARD_CLIP_BUFFER_BUFFER, synthClipper::HardClip);
				IMPLEMENT_BINARY_BUFFER_SCALAR(synthOpcodes::HARD_CLIP_BUFFER_SCALAR, synthClipper::HardClip);
				IMPLEMENT_BINARY_SCALAR(synthOpcodes::HARD_CLIP_SCALAR_SCALAR, synthClipper::HardClip);

				IMPLEMENT_BINARY_BUFFER(synthOpcodes::SOFT_CLIP_BUFFER_BUFFER, synthClipper::SoftClip);
				IMPLEMENT_BINARY_BUFFER_SCALAR(synthOpcodes::SOFT_CLIP_BUFFER_SCALAR, synthClipper::SoftClip);
				IMPLEMENT_BINARY_SCALAR(synthOpcodes::SOFT_CLIP_SCALAR_SCALAR, synthClipper::SoftClip);

				case synthOpcodes::ENVELOPE_FOLLOWER_BUFFER:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc,'B');
						const u32 inOutBuffer = decode_synth_ref_id(*pc++);
						synthValidateScalarReference(*pc);
						const float input1 = LoadScalarReference(*pc++, registers);
						synthValidateScalarReference(*pc);
						const float input2 = LoadScalarReference(*pc++, registers);

						synthEnvelopeFollower::Process(buffers[inOutBuffer], input1, input2, GetStateBlock(*pc++), kMixBufNumSamples);						
					}
					break;
				case synthOpcodes::ENVELOPE_FOLLOWER_SCALAR:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc,'R');
						const u32 outputReg = decode_synth_ref_id(*pc++);
						synthValidateScalarReference(*pc);
						const float input1 = LoadScalarReference(*pc++, registers);
						synthValidateScalarReference(*pc);
						const float input2 = LoadScalarReference(*pc++, registers);
						synthValidateScalarReference(*pc);
						const float input3 = LoadScalarReference(*pc++, registers);

						registers[outputReg] = synthEnvelopeFollower::Process(input1, input2, input3, GetStateBlock(*pc++));
					}
					break;

				IMPLEMENT_BIQUAD_COEFF_2IN(synthOpcodes::BiquadCoefficients_LowPass2Pole, synthBiquadFilter::ComputeCoefficients_LowPass2Pole);
				IMPLEMENT_BIQUAD_COEFF_2IN(synthOpcodes::BiquadCoefficients_HighPass2Pole, synthBiquadFilter::ComputeCoefficients_HighPass2Pole);
				IMPLEMENT_BIQUAD_COEFF_2IN(synthOpcodes::BiquadCoefficients_BandPass, synthBiquadFilter::ComputeCoefficients_BandPass);
				IMPLEMENT_BIQUAD_COEFF_2IN(synthOpcodes::BiquadCoefficients_BandStop, synthBiquadFilter::ComputeCoefficients_BandStop);
				IMPLEMENT_BIQUAD_COEFF_2IN(synthOpcodes::BiquadCoefficients_LowPass4Pole, synthBiquadFilter::ComputeCoefficients_LowPass4Pole);
				IMPLEMENT_BIQUAD_COEFF_2IN(synthOpcodes::BiquadCoefficients_HighPass4Pole, synthBiquadFilter::ComputeCoefficients_HighPass4Pole);

				case synthOpcodes::BiquadCoefficients_PeakingEQ:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 5);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_a0 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_a1 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_a2 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_b1 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_b2 = decode_synth_ref_id(*pc++);

						const float in0 = LoadScalarReference(*pc++, registers);
						const float in1 = LoadScalarReference(*pc++, registers);
						const float in2 = LoadScalarReference(*pc++, registers);
						synthBiquadFilter::ComputeCoefficients_PeakingEQ(in0,
							in1,
							in2,
							registers[reg_a0],
							registers[reg_a1],
							registers[reg_a2],
							registers[reg_b1],
							registers[reg_b2]);
					}
					break;

				case synthOpcodes::BiquadProcess_2Pole:
					{
						synthAssert(numInputs == 6);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'B');

						const u32 outputId = decode_synth_ref_id(*pc++);
						const float a0 = LoadScalarReference(*pc++, registers);
						const float a1 = LoadScalarReference(*pc++, registers);
						const float a2 = LoadScalarReference(*pc++, registers);
						const float b1 = LoadScalarReference(*pc++, registers);
						const float b2 = LoadScalarReference(*pc++, registers);

						synthBiquadFilter::Process_2Pole(buffers[outputId],
														a0,a1,a2,b1,b2,
														GetStateBlock(*pc++),
														kMixBufNumSamples);
					}
					break;

				case synthOpcodes::BiquadProcess_4Pole:
					{
						synthAssert(numInputs == 6);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'B');

						const u32 outputId = decode_synth_ref_id(*pc++);
						const float a0 = LoadScalarReference(*pc++, registers);
						const float a1 = LoadScalarReference(*pc++, registers);
						const float a2 = LoadScalarReference(*pc++, registers);
						const float b1 = LoadScalarReference(*pc++, registers);
						const float b2 = LoadScalarReference(*pc++, registers);

						synthBiquadFilter::Process_4Pole(buffers[outputId],
														a0,a1,a2,b1,b2,
														GetStateBlock(*pc++),
														kMixBufNumSamples);
					}
					break;

					IMPLEMENT_BINARY_BUFFER_WITH_STATE(synthOpcodes::OnePole_LPF_BUFFER_BUFFER, synth1PoleLPF::Process_LPF);
					IMPLEMENT_BINARY_BUFFER_SCALAR_WITH_STATE(synthOpcodes::OnePole_LPF_BUFFER_SCALAR, synth1PoleLPF::Process_LPF);
					IMPLEMENT_BINARY_SCALAR_WITH_STATE(synthOpcodes::OnePole_LPF_SCALAR, synth1PoleLPF::Process_LPF);
					IMPLEMENT_BINARY_BUFFER_WITH_STATE(synthOpcodes::OnePole_HPF_BUFFER_BUFFER, synth1PoleLPF::Process_HPF);
					IMPLEMENT_BINARY_BUFFER_SCALAR_WITH_STATE(synthOpcodes::OnePole_HPF_BUFFER_SCALAR, synth1PoleLPF::Process_HPF);
					IMPLEMENT_BINARY_SCALAR_WITH_STATE(synthOpcodes::OnePole_HPF_SCALAR, synth1PoleLPF::Process_HPF);
					
				
					// 1 buffer input, 1 (same) buffer output, with single state block
					IMPLEMENT_UNARY_BUFFER_WITH_STATE(synthOpcodes::OSC_RAMP_BUFFER_BUFFER, synthDigitalOscillator::GenerateRamp);
					// 1 scalar input, 1 buffer output, with single state block
					IMPLEMENT_UNARY_BUFFER_SCALAR_WITH_STATE(synthOpcodes::OSC_RAMP_BUFFER_SCALAR, synthDigitalOscillator::GenerateRamp);
					// 1 scalar input, 1 scalar output, with single state block
					IMPLEMENT_UNARY_SCALAR_WITH_STATE(synthOpcodes::OSC_RAMP_SCALAR, synthDigitalOscillator::GenerateRamp);

					IMPLEMENT_UNARY_BUFFER(synthOpcodes::SINE_BUFFER, synthDigitalOscillator::Sine);
					IMPLEMENT_UNARY_SCALAR(synthOpcodes::SINE_SCALAR, synthDigitalOscillator::Sine);
					IMPLEMENT_UNARY_BUFFER(synthOpcodes::COS_BUFFER, synthDigitalOscillator::Cosine);
					IMPLEMENT_UNARY_SCALAR(synthOpcodes::COS_SCALAR, synthDigitalOscillator::Cosine);
					IMPLEMENT_UNARY_BUFFER(synthOpcodes::TRI_BUFFER, synthDigitalOscillator::Triangle);
					IMPLEMENT_UNARY_SCALAR(synthOpcodes::TRI_SCALAR, synthDigitalOscillator::Triangle);
					IMPLEMENT_UNARY_BUFFER(synthOpcodes::SQUARE_BUFFER, synthDigitalOscillator::Square);
					IMPLEMENT_UNARY_SCALAR(synthOpcodes::SQUARE_SCALAR, synthDigitalOscillator::Square);
					IMPLEMENT_UNARY_BUFFER(synthOpcodes::SAW_BUFFER, synthDigitalOscillator::Saw);
					IMPLEMENT_UNARY_SCALAR(synthOpcodes::SAW_SCALAR, synthDigitalOscillator::Saw);

				case synthOpcodes::TRIGGER_LATCH:
					{
						synthAssert(numInputs == 2);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc,'R');
						const u32 outputReg = decode_synth_ref_id(*pc++);
						if(decode_synth_ref_type(*pc) == synthOpcodes::Buffer)
						{
							synthValidateReferenceType(*pc,'B');
							const u32 bufferInput0 = decode_synth_ref_id(*pc++);
							if(decode_synth_ref_type(*pc) == synthOpcodes::Buffer)
							{
								const u32 bufferInput1 = decode_synth_ref_id(*pc++);
								registers[outputReg] = synthTrigger::Process_Latch(buffers[bufferInput0], buffers[bufferInput1], GetStateBlock(*pc++), kMixBufNumSamples);
							}
							else
							{
								const Vector_4V threshold = V4LoadScalar32IntoSplatted(LoadScalarReference(*pc++, registers));
								registers[outputReg] = synthTrigger::Process_Latch(buffers[bufferInput0], threshold, GetStateBlock(*pc++), kMixBufNumSamples);
							}
						}
						else
						{
							const float input = LoadScalarReference(*pc++, registers);
							const float threshold = LoadScalarReference(*pc++, registers);
							registers[outputReg] = synthTrigger::Process_Latch(input, threshold, GetStateBlock(*pc++));
						}
					}
					break;

				case synthOpcodes::ENV_GEN_LIN_INT_BUFFER:
					envelopeTriggerMode = synthEnvelopeGenerator::Interruptible;
					envelopeReleaseType = synthEnvelopeGenerator::Linear;
					goto _EnvProcess;
				case synthOpcodes::ENV_GEN_LIN_OS_BUFFER:
					envelopeTriggerMode = synthEnvelopeGenerator::OneShot;
					envelopeReleaseType = synthEnvelopeGenerator::Linear;
					goto _EnvProcess;
				case synthOpcodes::ENV_GEN_LIN_RET_BUFFER:
					envelopeTriggerMode = synthEnvelopeGenerator::Retrigger;
					envelopeReleaseType = synthEnvelopeGenerator::Linear;
					goto _EnvProcess;
				case synthOpcodes::ENV_GEN_EXP_INT_BUFFER:
					envelopeTriggerMode = synthEnvelopeGenerator::Interruptible;
					envelopeReleaseType = synthEnvelopeGenerator::Exponential;
					goto _EnvProcess;
				case synthOpcodes::ENV_GEN_EXP_OS_BUFFER:
					envelopeTriggerMode = synthEnvelopeGenerator::OneShot;
					envelopeReleaseType = synthEnvelopeGenerator::Exponential;
					goto _EnvProcess;
				case synthOpcodes::ENV_GEN_EXP_RET_BUFFER:
					envelopeTriggerMode = synthEnvelopeGenerator::Retrigger;
					envelopeReleaseType = synthEnvelopeGenerator::Exponential;
					goto _EnvProcess;
				
_EnvProcess:
					{
						synthAssert(numInputs == 7);
						synthAssert(numOutputs == 1 || numOutputs == 2);

						synthValidateReferenceType(*pc,'B');
						const u32 outputId = decode_synth_ref_id(*pc++);

						u32 outputReg = ~0U;

						// Optional 'finished' output
						if(opcode.numOutputs == 2)
						{
							synthValidateReferenceType(*pc, 'R');
							outputReg = decode_synth_ref_id(*pc++);
						}

						const float predelay = LoadScalarReference(*pc++, registers);
						const float attack = LoadScalarReference(*pc++, registers);
						const float decay = LoadScalarReference(*pc++, registers);
						const float sustain = LoadScalarReference(*pc++, registers);
						const float hold = LoadScalarReference(*pc++, registers);
						const float release = LoadScalarReference(*pc++, registers);
						const float trigger = LoadScalarReference(*pc++, registers);

						

						const float result = synthEnvelopeGenerator::Process(envelopeReleaseType, 
															envelopeTriggerMode, 
															buffers[outputId], 
															kMixBufNumSamples,
															GetStateBlock(*pc++),
															predelay,
															attack,
															decay,
															sustain,
															hold,
															release,
															trigger);

						if(outputReg != ~0U)
						{
							registers[outputReg] = result;
						}
					}
					break;

				case synthOpcodes::TIMED_TRIGGER_INT:
					envelopeTriggerMode = synthEnvelopeGenerator::Interruptible;
					goto _TimedTriggerProcess;
				case synthOpcodes::TIMED_TRIGGER_OS:
					envelopeTriggerMode = synthEnvelopeGenerator::OneShot;
					goto _TimedTriggerProcess;
				case synthOpcodes::TIMED_TRIGGER_RET:
					envelopeTriggerMode = synthEnvelopeGenerator::Retrigger;
					goto _TimedTriggerProcess;

_TimedTriggerProcess:
					{
						synthAssert(numInputs == 6);
						synthAssert(numOutputs == 5);
						
						const u32 numPhases = 4;
						u32 phaseOutputs[numPhases];						
						for(u32 i = 0; i < numPhases; i++)
						{
							synthValidateReferenceType(*pc, 'R');
							phaseOutputs[i] = decode_synth_ref_id(*pc++);
						}

						synthValidateReferenceType(*pc, 'R');
						const u32 finishedReg = decode_synth_ref_id(*pc++);
								
						float trigger = LoadScalarReference(*pc++, registers);
						float predelay = LoadScalarReference(*pc++, registers);
						
						float inOut[numPhases] = {
											LoadScalarReference(*pc++, registers),
											LoadScalarReference(*pc++, registers),
											LoadScalarReference(*pc++, registers),
											LoadScalarReference(*pc++, registers)
						};

						registers[finishedReg] = synthTimedTrigger::Process(envelopeTriggerMode, 
																			trigger, 
																			predelay, 
																			GetStateBlock(*pc++), 
																			inOut[0], inOut[1], inOut[2], inOut[3]);

						for(u32 i = 0; i < numPhases; i++)
						{
							registers[phaseOutputs[i]] = inOut[i];
						}
					}
					break;

				case synthOpcodes::READ_VARIABLE:
					{
						synthAssert(numInputs == 1);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'R');
						u32 registerId = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc, 'V');
						u32 variableId = decode_synth_ref_id(*pc++);
						
						registers[registerId] = ReadVariable(variableId);
					}
					break;

				case synthOpcodes::FINISHED_TRIGGER:
					{
						synthAssert(numInputs == 1);
						synthAssert(numOutputs == 0);
						synthValidateScalarReference(*pc);
						const float triggerVal = LoadScalarReference(*pc++, registers);
						m_IsFinished = (triggerVal >= 1.f - SMALL_FLOAT);
					}
					break;
				case synthOpcodes::READ_INPUT_0:
				case synthOpcodes::READ_INPUT_1:
				case synthOpcodes::READ_INPUT_2:
				case synthOpcodes::READ_INPUT_3:
				case synthOpcodes::READ_INPUT_4:
				case synthOpcodes::READ_INPUT_5:
				case synthOpcodes::READ_INPUT_6:
				case synthOpcodes::READ_INPUT_7:
					{
						const u32 inputId = (opcode.opcode - synthOpcodes::READ_INPUT_0);
						synthAssert(numInputs == 0);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc,'B');
						const u32 outputId = decode_synth_ref_id(*pc++);
						if(inputBuffers && inputBuffers[inputId])
						{
							synthAssert(inputBuffers);
							synthAssert(inputBuffers[inputId]);
							sysMemCpy128(buffers[outputId], inputBuffers[inputId], sizeof(float) * kMixBufNumSamples);
						}
					}
					break;
				IMPLEMENT_UNARY_SCALAR(synthOpcodes::NOTE_TO_FREQ_SCALAR, synthNoteToFrequency::Process);
				IMPLEMENT_UNARY_BUFFER(synthOpcodes::NOTE_TO_FREQ_BUFFER, synthNoteToFrequency::Process);
				IMPLEMENT_BINARY_SCALAR_WITH_STATE(synthOpcodes::SAH_STATIC_SCALAR, synthSampleAndHold::ProcessStatic);

				case synthOpcodes::DECIMATE:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 1);
						
						synthValidateReferenceType(*pc, 'B');
						const u32 outputId = decode_synth_ref_id(*pc++);

						const float bitDepth = LoadScalarReference(*pc++, registers);
						const float sampleRate = LoadScalarReference(*pc++, registers);
						synthDecimator::Process(buffers[outputId], bitDepth, sampleRate, GetStateBlock(*pc++), kMixBufNumSamples);
					}
					break;

				case synthOpcodes::COUNTER:
				case synthOpcodes::COUNTER_TRIGGER:
					{
						synthAssert(numInputs == 4);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'R');
						u32 registerId = decode_synth_ref_id(*pc++);

						const float reset = LoadScalarReference(*pc++, registers);
						const float increment = LoadScalarReference(*pc++, registers);
						const float decrement = LoadScalarReference(*pc++, registers);
						const float autoResetThreshold = LoadScalarReference(*pc++, registers);

						if(opcode.opcode == synthOpcodes::COUNTER)
						{
							registers[registerId] = synthCounter::Process_Counter(reset, increment, decrement, autoResetThreshold, GetStateBlock(*pc++));
						}
						else
						{
							registers[registerId] = synthCounter::Process_Trigger(reset, increment, decrement, autoResetThreshold, GetStateBlock(*pc++));
						}
					}

					break;

					IMPLEMENT_BINARY_BUFFER(synthOpcodes::GATE_BUFFER_BUFFER, synthGate::Process);
					IMPLEMENT_BINARY_BUFFER_SCALAR(synthOpcodes::GATE_BUFFER_SCALAR, synthGate::Process);
					IMPLEMENT_BINARY_SCALAR(synthOpcodes::GATE_SCALAR_SCALAR, synthGate::Process);

				case synthOpcodes::SMALLDELAY_FRAC:
				case synthOpcodes::SMALLDELAY_STATIC:
					{
						synthAssert(numInputs == 2 || numInputs == 3);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'B');
						const u32 inputOutputId = decode_synth_ref_id(*pc++);
						const float delayLength = LoadScalarReference(*pc++, registers);
						if(opcode.numInputs == 3)
						{
							// Skip unused feedback.  This is conditional on numInputs for backwards compatibility
							pc++;
						}
						if(opcode.opcode == synthOpcodes::SMALLDELAY_FRAC)
						{
							synthSmallDelay::Process_Frac(buffers[inputOutputId], delayLength, GetStateBlock(*pc++), kMixBufNumSamples);
						}
						else
						{
							synthSmallDelay::Process_NonInterp(buffers[inputOutputId], delayLength, GetStateBlock(*pc++), kMixBufNumSamples);
						}
					}
					break;
				case synthOpcodes::SMALLDELAY_FRAC_FB:
				case synthOpcodes::SMALLDELAY_STATIC_FB:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'B');
						const u32 inputOutputId = decode_synth_ref_id(*pc++);
						const float delayLength = LoadScalarReference(*pc++, registers);
						const float feedback = LoadScalarReference(*pc++, registers);
						if(opcode.opcode == synthOpcodes::SMALLDELAY_FRAC_FB)
						{
							synthSmallDelay::Process_Frac_Feedback(buffers[inputOutputId], delayLength, feedback, GetStateBlock(*pc++), kMixBufNumSamples);
						}
						else
						{
							synthSmallDelay::Process_NonInterp_Feedback(buffers[inputOutputId], delayLength, feedback, GetStateBlock(*pc++), kMixBufNumSamples);
						}
					}
					break;
				case synthOpcodes::TRIGGER_DIFF:
					{
						synthAssert(numInputs == 2);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc,'R');
						const u32 outputReg = decode_synth_ref_id(*pc++);
																		
						const float input = LoadScalarReference(*pc++, registers);
						const float threshold = LoadScalarReference(*pc++, registers);
						registers[outputReg] = synthTrigger::Process_Diff(input, threshold, GetStateBlock(*pc++));
						
					}
					break;

				case synthOpcodes::RANDOMIZE_ONINIT:
					{
						synthAssert(numInputs == 2);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'R');

						const u32 outputRegId = decode_synth_ref_id(*pc++);
						const float min = LoadScalarReference(*pc++, registers);
						const float max = LoadScalarReference(*pc++, registers);
						registers[outputRegId] = synthRandomizer::Process_OnInit(min, max, GetStateBlock(*pc++));
					}
					break;
				case synthOpcodes::HOLD_SAMPLE:
					{
						synthAssert(numInputs == 1);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'B');

						const u32 outputId = decode_synth_ref_id(*pc++);
						const float inputVal = LoadScalarReference(*pc++, registers);
						synthUtil::HoldSample(buffers[outputId], inputVal, kMixBufNumSamples);
					}
					break;
				case synthOpcodes::COMPRESSOR_EG:
					{
						synthAssert(numInputs == 5);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'B');

						const u32 outputId = decode_synth_ref_id(*pc++);

						const float threshold = LoadScalarReference(*pc++, registers);
						const float ratio = LoadScalarReference(*pc++, registers);
						const float attack = LoadScalarReference(*pc++, registers);
						const float release = LoadScalarReference(*pc++, registers);

						synthCompressor::Process(buffers[outputId], threshold, ratio, attack, release, GetStateBlock(*pc++), kMixBufNumSamples);
					}
					break;
				case synthOpcodes::AW_FILTER:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 1);
						synthValidateReferenceType(*pc, 'B');
						const u32 outputId = decode_synth_ref_id(*pc++);

						synthValidateReferenceType(*pc, 'B');
						const u32 width = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc, 'B');
						const u32 widthVar = decode_synth_ref_id(*pc++);

						synthAWFilter::Process(buffers[outputId], buffers[width], buffers[widthVar], GetStateBlock(*pc++), kMixBufNumSamples);
					}
					break;
				case synthOpcodes::BiquadCoefficients_HighShelf2Pole:
				case synthOpcodes::BiquadCoefficients_HighShelf4Pole:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 5);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_a0 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_a1 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_a2 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_b1 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_b2 = decode_synth_ref_id(*pc++);

						const float in0 = LoadScalarReference(*pc++, registers);
						const float in1 = LoadScalarReference(*pc++, registers);
						const float in2 = LoadScalarReference(*pc++, registers);
						synthBiquadFilter::ComputeCoefficients_HighShelf(opcode.opcode == synthOpcodes::BiquadCoefficients_HighShelf4Pole, 
							in0,
							in1,
							in2,
							registers[reg_a0],
							registers[reg_a1],
							registers[reg_a2],
							registers[reg_b1],
							registers[reg_b2]);
					}
					break;
				case synthOpcodes::BiquadCoefficients_LowShelf2Pole:
				case synthOpcodes::BiquadCoefficients_LowShelf4Pole:
					{
						synthAssert(numInputs == 3);
						synthAssert(numOutputs == 5);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_a0 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_a1 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_a2 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_b1 = decode_synth_ref_id(*pc++);
						synthValidateReferenceType(*pc,'R');
						const u32 reg_b2 = decode_synth_ref_id(*pc++);

						const float in0 = LoadScalarReference(*pc++, registers);
						const float in1 = LoadScalarReference(*pc++, registers);
						const float in2 = LoadScalarReference(*pc++, registers);
						synthBiquadFilter::ComputeCoefficients_LowShelf(opcode.opcode == synthOpcodes::BiquadCoefficients_LowShelf4Pole, 
							in0,
							in1,
							in2,
							registers[reg_a0],
							registers[reg_a1],
							registers[reg_a2],
							registers[reg_b1],
							registers[reg_b2]);
					}
					break;
				case synthOpcodes::Switch_Norm_Buffer:
				case synthOpcodes::Switch_Index_Buffer:
				case synthOpcodes::Switch_Lerp_Buffer:
				case synthOpcodes::Switch_EqualPower_Buffer:
					{
						atFixedArray<const float *, 16> inputs;
						
						// First input/output buffer						
						synthValidateReferenceType(*pc, 'B');
						float *inOut = buffers[decode_synth_ref_id(*pc++)];
						inputs.Append() = inOut;

						// Then t-value (scalar)
						const float t = LoadScalarReference(*pc++, registers);

						// then 1-n inputs
						for(u32 i = 2; i < opcode.numInputs; i++)
						{
							synthValidateReferenceType(*pc, 'B');
							inputs.Append() = buffers[decode_synth_ref_id(*pc++)];
						}
						switch(opcode.opcode)
						{
						case synthOpcodes::Switch_Norm_Buffer:
							synthSwitch::Process_Norm(inOut, inputs, t, kMixBufNumSamples);
							break;
						case synthOpcodes::Switch_Index_Buffer:
							synthSwitch::Process_Index(inOut, inputs, t, kMixBufNumSamples);
							break;
						case synthOpcodes::Switch_Lerp_Buffer:
							synthSwitch::Process_Lerp(inOut, inputs, t, kMixBufNumSamples);
							break;
						case synthOpcodes::Switch_EqualPower_Buffer:
							synthSwitch::Process_EqualPower(inOut, inputs, t, kMixBufNumSamples);
							break;
						}

					}
					break;
				case synthOpcodes::Switch_Norm_Scalar:
				case synthOpcodes::Switch_Index_Scalar:
				case synthOpcodes::Switch_Lerp_Scalar:
				case synthOpcodes::Switch_EqualPower_Scalar:
					{
						atFixedArray<float, 16> inputs;

						// First output register
						synthValidateReferenceType(*pc, 'R');
						const u32 outputId = decode_synth_ref_id(*pc++);
						
						// then t-value (scalar)
						const float t = LoadScalarReference(*pc++, registers);

						// then 0-n inputs
						for(u32 i = 1; i < opcode.numInputs; i++)
						{							
							inputs.Append() = LoadScalarReference(*pc++, registers);
						}

						float outVal = 0.f;
						switch(opcode.opcode)
						{
						case synthOpcodes::Switch_Norm_Scalar:
							outVal = synthSwitch::Process_Norm(t, inputs);
							break;
						case synthOpcodes::Switch_Index_Scalar:
							outVal = synthSwitch::Process_Index(t, inputs);
							break;
						case synthOpcodes::Switch_Lerp_Scalar:
							outVal = synthSwitch::Process_Lerp(t, inputs);
							break;
						case synthOpcodes::Switch_EqualPower_Scalar:
							outVal = synthSwitch::Process_EqualPower(t, inputs);
							break;
						}

						registers[outputId] = outVal;

					}
					break;

					IMPLEMENT_BINARY_BUFFER_SCALAR_WITH_STATE(synthOpcodes::Allpass_Static, synthAllpassFilter::Process);
					IMPLEMENT_BINARY_BUFFER_WITH_STATE(synthOpcodes::Allpass_Buffer, synthAllpassFilter::Process);
			default:
				audAssertf(false, "Invalid synth opcode: %08X, offset %zu", opcodeVal, (pc-1) - m_ProgramEntry);
				break;
			}

#if SYNTH_PROFILE
			opcodeTimer.Stop();
			if(opcode.opcode < synthOpcodes::NUM_OP_CODES)
			{
				sm_ProfileData.OpCodeCounters[opcode.opcode]++;
				sm_ProfileData.OpCodeTimings[opcode.opcode] += opcodeTimer.GetTimeMS();
			}
#endif

#if SYNTH_VALIDATE_INTERNAL_STATE
			float regCheck = 0.f;
			for(u32 i = 0; i < kMaxRegisters; i++)
			{
				regCheck *= registers[i];
			}
			synthAssert(FPIsFinite(regCheck));

			for(u32 i = 0; i < m_NumBuffers; i++)
			{				
				synthAssert(FPIsFinite(ComputePeakLevel<kMixBufNumSamples>(buffers[i])));
			}
#endif
		}

#if __SPU
		for(s32 i = 0; i < m_StateBlockIds.GetCount(); i++)
		{
			g_FrameBufferCache->WriteAndFreeBuffer(m_StateBlockIds[i]);
		}
#endif
		// Clip output to 0dB
		for(u32 i = 0; i < m_NumOutputs; i++)
		{
			float *bufPtr = g_FrameBufferCache->GetBuffer(outputBufferIds[i]);
			if(bufPtr)
			{
				HardClipBuffer<kMixBufNumSamples>(bufPtr);

				if(m_IsPaused && !m_IsPauseRequested)
				{
					FadeInBuffer<kMixBufNumSamples>(bufPtr);
				}
				else if(!m_IsPaused && m_IsPauseRequested)
				{
					FadeOutBuffer<kMixBufNumSamples>(bufPtr);
				}

				SYNTH_ASSERT_ONLY(const float peak = ComputePeakLevel<kMixBufNumSamples>(bufPtr));
				SYNTH_ASSERT_ONLY(m_PeakLevelLin = Max(m_PeakLevelLin, peak));
				synthAssert(peak <= 1.f);
				synthAssert(FPIsFinite(peak));
			}
		}

		m_Context.IncrFrameCount();

#if !__FINAL
		timer.Stop();
		m_RuntimeMs += timer.GetTimeMS();
#endif

#endif // !__PPU
	}

	void synthCore::SkipFrame()
	{	
		if(!m_IsPaused)
		{
			m_Context.IncrFrameCount();
		}
	}

	float synthCore::LoadScalarReference(const u32 refVal, const atRangeArray<float,kMaxRegisters> &registers) const
	{
		synthValidateScalarReference(refVal);
		if(decode_synth_ref_type(refVal) == synthOpcodes::Register)
		{
			const u32 regId = decode_synth_ref_id(refVal);
			TrapGE(regId, (u32)m_NumRegisters);
			return registers[regId];
		}
		return ReadConstant(decode_synth_ref_id(refVal));
	}

	Vector_4V_Ref synthCore::GetStateBlock(const u32 index)
	{
		CompileTimeAssert(kMixBufNumSamples >= 128);
		// 32 blocks per slot
		const u32 slotId = index >> 5;
		const u32 slotIndex = index - (slotId<<5);
		audAssertf(m_StateBlockPtrs[slotId], "NULL state block reference; index %u slot %u", index, slotId);
		if (m_StateBlockIds[slotId] < audFrameBufferPool::InvalidId)
		{
			return *(m_StateBlockPtrs[slotId] + slotIndex);
		}
		return sm_ZeroState;
	}

#if !__SPU
	bool synthCore::ApplyPreset(const u32 presetNameHash)
	{
		Preset *preset = synthSynthesizer::GetMetadataManager().GetObject<Preset>(presetNameHash);
		if(preset)
		{
			for(u32 i = 0; i < preset->numVariables; i++)
			{
				const float randomOffset = audEngineUtil::GetRandomNumberInRange(-preset->Variable[i].Variance, preset->Variable[i].Variance);
				SetVariableValue(preset->Variable[i].Name, preset->Variable[i].Value + randomOffset);
			}
			return true;
		}
		return false;
	}
#endif

	void synthCore::SetVariableValue(const u32 nameHash, const float val)
	{
		if(nameHash == 0x31FF1BC8) //ATSTRINGHASH("Frequency", 0x31FF1BC8)
		{
			// Frequency scaling factor of 0 denotes paused, however the vast majority of synths won't
			// suit that structure, so instead catch it here and enter a paused state until we next get a
			// frequency scaling factor > 0
			if(val == 0.0f)
			{
				m_IsPauseRequested = true;
				return;
			}
			else
			{
				m_IsPauseRequested = false;
			}
		}

		int index = m_InputVariableMap.BinarySearch(InputVariableMap(nameHash));
		if(index != -1)
		{
			m_InputVariableValues[m_InputVariableMap[index].Index] = val;
		}
	}

	void synthCore::EndFrame()
	{
		m_IsPaused = m_IsPauseRequested;
	}

#if __ASSERT
	int synthCore::SynthAssertFailed(const char *msg, const char *exp, const char *file, const int line) const
	{
		char message[128];
#if __SPU
		ALIGNAS(16) char nameBuf[128] ;
		ALIGNAS(16) char nameBufAligned[128] ;
		dmaUnalignedGet(nameBuf, sizeof(nameBuf), nameBufAligned, 96, (u32)m_SynthName);
		const char *synthName = nameBufAligned;
#else
		const char *synthName = m_SynthName;
#endif
		
		formatf(message, "Synth: '%s' +%04X (%02X: %s) %s", 
							synthName, 
							(u32)sm_ProcessingOffset, 
							sm_ProcessingOpcode, 
							synthCoreDisasm::GetOpcodeName(sm_ProcessingOpcode), 
							msg ? msg : "");

#if __SPU
		audErrorf("synthAssert: %s", message);
		return AssertFailed(exp, file, line);
#else
		return diagAssertHelper(file, line, "%s: %s", exp, message);
#endif// __SPU
	}
#endif //__ASSERT

	void synthCorePcmSource::StartPhys(s32 UNUSED_PARAM(channel))
	{
		if(++m_NumPhysicalClients > 1)
		{
			// If this isn't the first physical client then we don't have to do much
			return;
		}

		AllocateBuffers(m_Synth.GetNumOutputs());
		
		m_Synth.StartPhys();
	}

	void synthCorePcmSource::StopPhys(s32 UNUSED_PARAM(channel))
	{
		if(--m_NumPhysicalClients == 0)
		{
			FreeBuffers();
			m_Synth.StopPhys();
		}
	}

	void synthCorePcmSource::BeginFrame()
	{
		AUD_PCMSOURCE_TIMER_BEGINFRAME;
		if(m_ExternalPcmSourceVariableBlock)
		{
			for(u32 loop = 0; loop < m_ExternalPcmSourceVariableBlock->GetNumVariables(); loop++)
			{
				f32* variable = m_ExternalPcmSourceVariableBlock->GetVariableAddress(loop);

				if(variable)
				{
					m_Synth.SetVariableValue(m_ExternalPcmSourceVariableBlock->GetVariableNameHash(loop), *variable);
				}
			}
		}
	}

	void synthCorePcmSource::GenerateFrame()
	{
		AUD_PCMSOURCE_TIMER_GENERATEFRAME;
		m_Synth.GenerateFrame(NULL, GetBufferIds());

#if RSG_BANK
		if(g_PcmSourceInjectNaN)
		{
			Vector_4V *buf = (Vector_4V*)g_FrameBufferCache->GetBuffer(GetBufferId(0));
			*buf = V4InvScale(*buf, V4VConstant(V_ONE_WZERO));			
			g_PcmSourceInjectNaN = false;
		}
#endif
	}

	void synthCorePcmSource::SkipFrame()
	{
		AUD_PCMSOURCE_TIMER_SKIPFRAME;
		m_Synth.SkipFrame();
	}

	void synthCorePcmSource::EndFrame()
	{
		AUD_PCMSOURCE_TIMER_ENDFRAME;
		m_Synth.EndFrame();
	}

#if !__SPU

	void synthCorePcmSource::SetParam(const u32 paramId, const u32 val)
	{
		switch(paramId)
		{
		case Params::CompiledSynthNameHash:
			Init(val);
			break;
		case Params::PresetNameHash:
			ApplyPreset(val);
			break;
		case Params::ExternalPcmSourceVariableBlock:
			{
				for(u32 loop = 0; loop < g_MaxPcmSourceVariableBlocks; loop++)
				{
					if(GetVariableBlock(loop)->GetPcmSource() == (s32)val)
					{
						m_ExternalPcmSourceVariableBlock = GetVariableBlock(loop);
						break;
					}
				}
			}
			break;
		}
	}

	void synthCorePcmSource::SetParam(const u32 paramId, const float val)
	{
		m_Synth.SetVariableValue(paramId, val);
	}

	bool synthCoreDspEffect::Init(const u32 numChannels, const u32 channelMask)
	{
		Assign(m_NumInputChannels, numChannels);
		m_ChannelMask = (u8)channelMask;

		m_NumChannelsToProcess = 0;
		for(u32 i=0, mask = 1; i<numChannels; i++)
		{
			if(m_ChannelMask & mask)
			{
				m_NumChannelsToProcess++;
			}
			mask <<= 1;
		}
		
		// skip processing until we've initialised our synth
		m_IsInitialised = false;
		m_Bypass = false;

		return true;
	}

	void synthCoreDspEffect::Shutdown()
	{
		if(m_IsInitialised)
		{
			m_Synth.StopPhys();
			m_Synth.Shutdown();
		}
		m_IsInitialised = false;
	}

	void synthCoreDspEffect::SetParam(const u32 paramId, const f32 value)
	{
		m_Synth.SetVariableValue(paramId, value);
	}

	void synthCoreDspEffect::SetParam(const u32 paramId, const u32 value)
	{	
		switch(paramId)
		{
		case synthCorePcmSource::Params::CompiledSynthNameHash:
			{		
				if(m_IsInitialised)
				{
					m_IsInitialised = false;
					m_Synth.StopPhys();
					m_Synth.Shutdown();
				}

				if(m_Synth.Init(value))
				{
					if(!audVerifyf(m_Synth.GetNumOutputs() == m_NumChannelsToProcess, "Synth hash %u used as DSP channel mismatch (%u vs %u)", value, m_Synth.GetNumOutputs(), m_NumChannelsToProcess))
					{
						m_Synth.Shutdown();
						m_IsInitialised = false;
					}
					else
					{
						m_Synth.StartPhys();
						m_IsInitialised = true;
					}
				}
			}
			break;
		case synthCorePcmSource::Params::PresetNameHash:
			m_Synth.ApplyPreset(value);
			break;
		case synthCorePcmSource::Params::Bypass:
			const bool bypass = (value != 0);
			m_Bypass = bypass;
			break;
		}

	}
#endif // !__SPU

#if !__SPU
	ALIGNAS(128) atRangeArray<atRangeArray<float, kMixBufNumSamples>, 8> g_SynthCoreDspBuffers;
#endif

	SPU_ONLY(extern bool g_DisplayAllocations);

	AUD_DEFINE_DSP_PROCESS_FUNCTION(synthCoreDspEffect)
	{
		if (m_Bypass || !m_IsInitialised)
			return;	

#if !__SPU || defined(AUD_SYNTHCORE_DSP_SPU)

		// Allocate working buffers
		SPU_ONLY(audAutoScratchBookmark scratchBookmark);
		SPU_ONLY(synthCore::InitClass());

		atRangeArray<u16, audPcmSource::kMaxPcmSourceChannels> outputBufferIds;
		float *inputBuffers[8] = {0};

		for(u32 i = 0, j = 0, mask = 1; i < buffer.NumChannels; i++)
		{
			if (buffer.ChannelBufferIds[i] < audFrameBufferPool::InvalidId && m_ChannelMask & mask)
			{
#if __SPU
				inputBuffers[j] = AllocateFromScratch<float>(kMixBufNumSamples, 16, "InputBuffer");
#else
				inputBuffers[j] = &g_SynthCoreDspBuffers[j][0];
#endif
				sysMemCpy128(inputBuffers[j], buffer.ChannelBuffers[i], kMixBufNumSamples*sizeof(float));
				outputBufferIds[j] = buffer.ChannelBufferIds[i];
				j++;
			}
			mask <<= 1;
		}

		m_Synth.GenerateFrame((const float**)inputBuffers, outputBufferIds);
#endif // !__SPU
	}

#if __ASSERT
	void synthCore::DumpQuietAssetList()
	{
		atMap<u32, float>::Iterator entry = sm_AssetPeakLevels.CreateIterator();
		for (entry.Start(); !entry.AtEnd(); entry.Next())
		{
			const char *synthName =	synthSynthesizer::GetMetadataManager().GetObjectName(entry.GetKey());
			const float volDecibels = audDriverUtil::ComputeDbVolumeFromLinear_Precise(entry.GetData());
			audDisplayf(", %s,%f", synthName, volDecibels);
		}
	}
#endif

#if !__FINAL && !__SPU
	void synthCorePcmSource::GetAssetInfo(audPcmSource::AssetInfo &BANK_ONLY(assetInfo)) const
	{
		BANK_ONLY(assetInfo.assetNameHash = m_Synth.GetAssetNameHash());
	}
#endif // !__FINAL && !__SPU

} // namespace rage
