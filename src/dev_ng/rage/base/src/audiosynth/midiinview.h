// 
// audiosynth/midiinview.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_MIDIINVIEW_H
#define SYNTH_MIDIINVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uilabel.h"
#include "uihorizontallayout.h"
#include "uiscrollbar.h"
#include "uidropdownlist.h"

namespace rage
{

	class synthMidiIn;
	class synthMidiInView : public synthModuleView
	{
	public:

		synthMidiInView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

	protected:
		virtual void Render();

	private:

		synthMidiIn *GetMidiIn() const {return (synthMidiIn*)m_Module; }

		synthUIScrollbar m_PolyphonySlider;
		synthUIScrollbar m_NoteSlider;
		synthUIDropDownList m_ChannelList;
		synthUILabelView m_DeviceNameLabel;
        synthUIHorizontalLayout m_Layout;
        synthUIHorizontalLayout m_SliderLayout;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_SWITCHVIEW_H

