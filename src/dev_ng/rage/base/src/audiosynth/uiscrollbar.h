// 
// audiosynth/uiscrollbar.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UISCROLLBAR_H
#define SYNTH_UISCROLLBAR_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uiview.h"
#include "uilabel.h"
#include "vector/vector3.h"

#include "numericinputbox.h"

namespace rage
{
	class synthUIScrollbar : public synthUIView
	{

	public:

		synthUIScrollbar();
		virtual ~synthUIScrollbar();

		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		bool IsBeingDragged() const
		{
			return m_IsBeingDragged;
		}

		f32 GetValue() const
		{
			return Lerp(m_CurrentValue, m_MinValue, m_MaxValue);
		}

		f32 GetNormalizedValue() const
		{
			return m_CurrentValue;
		}

		void SetRange(const f32 min, const f32 max)
		{
			m_MinValue = min;
			m_MaxValue = max;
		}

		void SetNormalizedValue(const f32 val)
		{
			m_CurrentValue = val;
		}

		void SetTextAlpha(const f32 alpha)
		{
			m_TextAlpha = alpha;
		}

		bool IsActive() const { return (IsUnderMouse() || IsBeingDragged()); }

		void SetWidth(const f32 width)
		{
			m_Width = width;
		}

		void SetTitle(const char *title)
		{
			m_TitleLabel.SetString(title);
			m_InputBox.SetTitle(title);
		}

		void SetDrawTitle(const bool draw)
		{
			m_DrawTitle = draw;
		}

		void SetDrawReflection(const bool draw)
		{
			m_DrawReflection = draw;
		}

		void SetReadOnly(const bool readOnly);
		
		virtual bool StartDrag() const { return false; }


		void SetHighlightColour(const Color32 color)
		{
			m_HighlightColour = color;
		}

		void SetRenderAsInteger(const bool renderAsInt)
		{
			m_RenderAsInteger = renderAsInt;
		}

	protected:
		virtual void Render();

	private:


		synthUITextBoxInput m_InputBox;

		synthUILabel m_ValueLabel;
		synthUILabel m_TitleLabel;
		char m_ValueString[32];
		s32 m_LastMouseY;
		f32 m_CurrentValue;
		f32 m_MinValue, m_MaxValue;
		f32 m_TextAlpha;
		f32 m_Width;

		Color32 m_HighlightColour;
		bool m_IsBeingDragged;
		bool m_DrawTitle;
		bool m_DrawReflection;
		bool m_ReadOnly;
		bool m_RenderAsInteger;
	};
}
#endif // __SYNTH_EDITOR
#endif // SYNTH_UISCROLLBAR_H
