
#ifndef SYNTH_1POLELPF_H
#define SYNTH_1POLELPF_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum synth1PoleLPFInputs
	{
		k1PoleSignal = 0,
		k1PoleGain,
		k1PoleCutoff,
		k1PoleNumInputs
	};

	class synth1PoleLPF : public synthModuleBase<k1PoleNumInputs,1, SYNTH_1POLE>
	{
		public:
			SYNTH_CUSTOM_MODULE_VIEW;

			synth1PoleLPF();
			virtual ~synth1PoleLPF(){};
			
			virtual void SerializeState(synthModuleSerializer *serializer);
						
			virtual void Synthesize();
			virtual void ResetProcessing();
			
			
			void SetMinFrequency(const f32 freq)
			{
				m_MinFrequency = freq;
			}
			
			void SetMaxFrequency(const f32 freq)
			{
				m_MaxFrequency = freq;
			}
			
			f32 GetMinFrequency() const { return m_MinFrequency; }
			f32 GetMaxFrequency() const { return m_MaxFrequency; }

			virtual const char *GetName() const
			{
				return "1PoleFilter";
			}

			void SetIsHighpass(const bool isHighpass)
			{
				m_IsHighpass = isHighpass;
			}
		
			bool IsHighpass() const { return m_IsHighpass; }

			static void Process_LPF(float *RESTRICT const inOutBuffer, const float *RESTRICT const freqBuffer, Vec::Vector_4V_InOut state, const u32 numSamples);
			static void Process_LPF(float *RESTRICT const inOutBuffer, const float freq, Vec::Vector_4V_InOut state, const u32 numSamples);
			static float Process_LPF(const float input, const float freq, Vec::Vector_4V_InOut state);

			static void Process_HPF(float *RESTRICT const inOutBuffer, const float *RESTRICT const freqBuffer, Vec::Vector_4V_InOut state, const u32 numSamples);
			static void Process_HPF(float *RESTRICT const inOutBuffer, const float freq, Vec::Vector_4V_InOut state, const u32 numSamples);
			static float Process_HPF(const float input, const float freq, Vec::Vector_4V_InOut state);

		private:
		
			synthSampleFrame m_Buffer;
			
			f32 m_MinFrequency;
			f32 m_MaxFrequency;
			
			f32 m_a0;
			f32 m_b1;
			
			f32 m_PrevSample;

			bool m_IsHighpass;
		};
}
#endif //!SYNTH_MINIMAL_MODULES

#endif // SYNTH_1POLELPF_H


