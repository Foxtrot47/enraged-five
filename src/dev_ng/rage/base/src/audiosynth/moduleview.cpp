// 
// audiosynth/moduleview.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "module.h"
#include "pin.h"
#include "modulegroup.h"
#include "moduleview.h"
#include "pinview.h"
#include "synthesizerview.h"
#include "uiplane.h"

#include "audioengine/engineutil.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mouse.h"
#include "profile/profiler.h"
#include "vector/geometry.h"
#include "math/amath.h"
#include "system/timemgr.h"


namespace rage
{
	PF_PAGE(AMP_Tool, "AMP performance monitor");
	PF_GROUP(ModuleViewUpdate);
	PF_GROUP(ModuleViewDraw);
	
	PF_LINK(AMP_Tool, ModuleViewUpdate);
	PF_LINK(AMP_Tool, ModuleViewDraw);
	PF_TIMER(TotalUpdateTime, ModuleViewUpdate);
	PF_TIMER(TotalPostSynthesizeTime, ModuleViewUpdate);
	PF_TIMER(TotalDrawTime, ModuleViewDraw);
	PF_TIMER(TotalDrawWiresTime, ModuleViewDraw);
	PF_TIMER(TotalDrawBackgroundTime, ModuleViewDraw);

	//Color32 g_ModuleEdgeColourHighlight(0.5f,0.5f,1.f,1.f);
	//Color32 g_ModuleEdgeColourNormal (1.f,0.35f,0.f);

	Color32 g_ModuleEdgeColourHighlight(0.2f,0.2f,1.f,1.f);
	Color32 g_ModuleEdgeColourNormal (0.f,0.f,0.8f,1.f);

	grcTexture *synthModuleView::sm_NoiseTexture = NULL;

	Color32 g_HighlightWireColour = Color32(0.25f,1.f,0.25f,1.f);
	Color32 g_SignalWireColour = Color32(0.f,0.f,1.f,1.f);
	Color32 g_NormalizedWireColour = Color32(0.0f,1.f,0.f,1.f);

	Color32 g_ModuleEdgeColour = Color32(1.f,0.35f,0.f,1.f);

	bool synthModuleView::InitClass()
	{
		Assert(!sm_NoiseTexture);
		enum {kNumSamples = 32};
		sm_NoiseTexture = synthUIComponent::CreateTexture(kNumSamples, 1, grcImage::R32F);
		if(!sm_NoiseTexture)
		{
			return false;
		}

		grcTextureLock lock;
		if(Verifyf(sm_NoiseTexture->LockRect(0,0,lock,grcsWrite), "Failed to LockRect, noise texture: %p", sm_NoiseTexture))
		{
			f32 *textPtr = static_cast<f32*>(lock.Base);
			for(u32 i = 0; i < kNumSamples; i++)
			{
				const f32 r = audEngineUtil::GetRandomNumberInRange(0.f,1.f);
				*textPtr++ = (r * r) * r;//> 0.85f ? 1.f : 0.f;
			}
			sm_NoiseTexture->UnlockRect(lock);
		}
		return true;
	}

	void synthModuleView::ShutdownClass()
	{
		if(sm_NoiseTexture)
		{
			sm_NoiseTexture->Release();
			sm_NoiseTexture = NULL;
		}
	}

	static const f32 ElementHeight = 2.7f;
	synthModuleView::synthModuleView(synthModule *module) : 
		m_Module(module),
		m_CurrentCPUSample(0),
		m_DragOffsetX(0.f),
		m_DragOffsetY(0.f),
		m_TitleAlpha(1.f),
		m_MeanProcessingTime(0.f),
		m_IsDragging(false),
		m_IsMinimised(false),
		m_HideTitle(false)
	{
		for(s32 i =0 ; i < kNumCPUSamples; i++)
		{
			m_CPUTimes[i] = 0.f;
		}
		AddComponent(&m_TitleLabel);
		AddComponent(&m_CPUTimeLabel);

		m_TitleLabel.SetString(m_Module->GetName());
	}

	synthModuleView::~synthModuleView()
	{
		// pins delete their own views
	}

	u32 synthModuleView::GetModuleHeight() const
	{
		u32 numInputs, numOutputs;
		synthPin *pins;
		m_Module->GetInputs(pins, numInputs);
		m_Module->GetOutputs(pins, numOutputs);
		return Max(numInputs,numOutputs);
	}

	void synthModuleView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		f32 width = GetModuleWidth();
		f32 height = Max(2.f,ElementHeight *(f32)(m_IsMinimised ? 1 : GetModuleHeight()) - 1.f);
		v1.Set(-width/2.f,-height/2.f,0.f);
		v2.Set(width/2.f,height/2.f,0.f);

		GetMatrix().Transform(v1);
		GetMatrix().Transform(v2);
	}

	void synthModuleView::Update()
	{
		PF_FUNC(TotalUpdateTime);

		if(!synthUIPlane::GetMaster()->IsInteractionDisabled())
		{
			if(IsUnderMouse() && synthUIPlane::GetMaster()->HadLeftDoubleClick() && StartDrag())
			{
				u32 numInputPins = 0, numOutputPins = 0;
				synthPin *pins = NULL;
				m_Module->GetInputs(pins,numInputPins);
				m_Module->GetInputs(pins,numOutputPins);

				// only go into a minimised state if this module features more than one in/out
				if(numInputPins > 1 || numOutputPins > 1)
				{
					m_IsMinimised = !m_IsMinimised;
				}
			}

			if(IsUnderMouse() && ioKeyboard::KeyPressed(KEY_DELETE) && !synthUIPlane::GetMaster()->IsGroupKeyDown())
			{
                synthSynthesizerView::Get()->GetModulePlane().DeleteModule(m_Module);
				return;
			}
		}
		Matrix34 rootMtx = GetMatrix();
		
		Matrix34 titleLabelMatrix = GetMatrix();
		titleLabelMatrix.d -= titleLabelMatrix.c * 0.1f;
		m_TitleLabel.SetMatrix(titleLabelMatrix);

		SetAlpha(Lerp(GetHoverFade(), 0.1f, 1.f));
		
		m_TitleLabel.SetSize(0.35f);
		m_TitleLabel.SetUnderMouse(IsUnderMouse());
		m_TitleLabel.SetAlpha(m_TitleAlpha);
		m_TitleLabel.SetShouldDraw(!m_HideTitle && ShouldDraw() && m_TitleLabel.GetAlpha()>0.f);

		m_CPUTimeLabel.SetSize(0.22f);
		m_CPUTimeLabel.SetShouldDraw(ShouldDraw());

		m_CPUTimes[m_CurrentCPUSample] = m_Module->GetCPUTime();
		m_CurrentCPUSample = (m_CurrentCPUSample+1)%kNumCPUSamples;
		f32 summedTime = 0.f;
		for(s32 i = 0; i < kNumCPUSamples; i++)
		{
			summedTime += m_CPUTimes[i];
		}

		const f32 meanTime = summedTime / (f32)kNumCPUSamples;
		const f32 realTimeRatio = meanTime / ((kMixBufNumSamples / (f32)kMixerNativeSampleRate) * 10.f);

		m_MeanProcessingTime = meanTime;

		formatf(m_CPUTimeBuf, sizeof(m_CPUTimeBuf),"%d: %.3f%%", m_Module->GetProcessingStage(), realTimeRatio);
		m_CPUTimeLabel.SetString(m_CPUTimeBuf);

		Matrix34 labelMtx = rootMtx;
		Vector3 v0,v1;
		GetBounds(v0,v1);
		labelMtx.d = v0;
		labelMtx.d += m_CPUTimeLabel.ComputeWidth() * labelMtx.a * 0.5f;
		labelMtx.d -= m_CPUTimeLabel.ComputeHeight() * labelMtx.b * 0.5f;

		m_CPUTimeLabel.SetMatrix(labelMtx);

		u32 numPins;
		synthPin *pins;
		m_Module->GetOutputs(pins, numPins);

		f32 pinAlpha = 0.f;
		if(numPins == 1 && GetModuleHeight() == 1)
		{
			pinAlpha = pins[0].GetView()->GetHoverFade();
		}
		// fade out CPU label if there is only one output as the pin text fades in
		m_CPUTimeLabel.SetAlpha(GetHoverFade()*GetAlpha()*(1.f - pinAlpha));

		Matrix34 elementMtx;
		elementMtx.Identity();

		
		m_Module->GetInputs(pins, numPins);

		Vector3 mousePos = synthUIPlane::GetMaster()->GetMousePosition();
		bool underMouse = false;

		f32 numInputsToDraw = (f32)(m_IsMinimised ? Min(1U,numPins) : numPins);
		f32 height = ElementHeight * numInputsToDraw;
		
        f32 width = 3.5f;
        if(OverrideBounds())
        {
            width = (ComputeWidth() * 0.5f) - 0.5f;
        }
        // need to take into account the height of an element (1 unit) shift it down another half
		elementMtx.MakeTranslate(width, height/2.f - (0.5f*ElementHeight),-0.1f);
		for(u32 i = 0 ; i < numPins; i++)
		{
			if(DefaultPinPositioning())
			{
				Matrix34 m;
				m.Dot(elementMtx,rootMtx);
				m_Module->GetInput(i).GetView()->SetMatrix(m);
			}
			m_Module->GetInput(i).GetView()->SetAlpha(1.f/*GetAlpha()*/);
			m_Module->GetInput(i).GetView()->SetShouldDraw(ShouldDraw());

			Vector3 v1,v2;
			m_Module->GetInput(i).GetView()->GetBounds(v1,v2);
			const Matrix34 &mat = m_Module->GetInput(i).GetView()->GetMatrix();
			Vector3 absFwd = mat.c;
			absFwd.Abs();
			v1 -= absFwd;
			v2 += absFwd;

			if(m_Module->GetInput(i).GetView()->ShouldDraw() && !underMouse && geomPoints::IsPointInBox(mousePos,v1,v2))
			{
				underMouse = true;
				m_Module->GetInput(i).GetView()->SetUnderMouse(IsUnderMouse());
			}
			else
			{
				m_Module->GetInput(i).GetView()->SetUnderMouse(false);
			}

			m_Module->GetInput(i).GetView()->Update();

			if(m_Module->GetInput(i).GetView()->IsUnderMouse())
			{
				SetUnderMouse(true);
			}
			if(!m_IsMinimised)
			{
				elementMtx.d.y -= ElementHeight;
				m_Module->GetInput(i).GetView()->SetShouldDraw(true);
			}
			else
			{
				if(i>=1)
				{
					m_Module->GetInput(i).GetView()->SetShouldDraw(false);
				}
			}
		}

		
		m_Module->GetOutputs(pins, numPins);
		const f32 numOutputsToDraw = (f32)(m_IsMinimised ? Min(1U,numPins) : numPins);
		height = ElementHeight * numOutputsToDraw;
		elementMtx.MakeTranslate(-3.5f,height/2.f - (0.5f*ElementHeight), -0.2f);


		for(u32 i = 0; i < numPins; i++)
		{
			if(DefaultPinPositioning())
			{
				Matrix34 m;
				m.Dot(elementMtx,rootMtx);
				m_Module->GetOutput(i).GetView()->SetMatrix(m);
			}

			m_Module->GetOutput(i).GetView()->SetAlpha(1.f/*GetAlpha()*/);
			m_Module->GetOutput(i).GetView()->SetShouldDraw(ShouldDraw());

			Vector3 v1,v2;
			m_Module->GetOutput(i).GetView()->GetBounds(v1,v2);
			const Matrix34 &mat = m_Module->GetOutput(i).GetView()->GetMatrix();
			Vector3 absFwd = mat.c;
			absFwd.Abs();
			v1 -= absFwd;
			v2 += absFwd;

			if(m_Module->GetOutput(i).GetView()->ShouldDraw() && !underMouse && geomPoints::IsPointInBox(mousePos,v1,v2))
			{
				underMouse = true;
				m_Module->GetOutput(i).GetView()->SetUnderMouse(IsUnderMouse());
			}
			else
			{
				m_Module->GetOutput(i).GetView()->SetUnderMouse(false);
			}

			m_Module->GetOutput(i).GetView()->Update();

			if(m_Module->GetOutput(i).GetView()->IsUnderMouse())
			{
				SetUnderMouse(true);
			}
			if(!m_IsMinimised)
			{
				elementMtx.d.y -= ElementHeight;
				m_Module->GetOutput(i).GetView()->SetShouldDraw(true);
			}
			else
			{
				if(i>=1)
				{
					m_Module->GetOutput(i).GetView()->SetShouldDraw(false);
				}
			}
		}

		synthUIView::Update();
	}

	void synthModuleView::PostSynthesize()
	{
		PF_FUNC(TotalPostSynthesizeTime);

		// allow pin views to update their buffers
		u32 numPins;
		synthPin *pins;
		m_Module->GetInputs(pins, numPins);
		for(u32 i = 0; i < numPins; i++)
		{
			if(m_Module->GetInput(i).HasView())
			{
				m_Module->GetInput(i).GetView()->PostSynthesize();
			}
		}
		m_Module->GetOutputs(pins, numPins);
		for(u32 i = 0; i < numPins; i++)
		{
			if(m_Module->GetOutput(i).HasView())
			{
				m_Module->GetOutput(i).GetView()->PostSynthesize();
			}
		}
	}

	void synthModuleView::DrawBackground() const
	{
		PF_FUNC(TotalDrawBackgroundTime);

		const bool isHighlighted = IsUnderMouse();
		if(m_Module->GetView()->IsGrouped())
		{
			SetShaderVar("g_EdgeHighlightColour", m_Module->GetView()->GetGroup()->GetColour());
		}
		else
		{
			SetShaderVar("g_EdgeHighlightColour", isHighlighted ? g_ModuleEdgeColourHighlight : g_ModuleEdgeColourNormal);
		}
		SetShaderVar("g_HighlightScale", isHighlighted ? 1.f : 0.f);
		SetShaderVar("g_AlphaFade", 1.f);
		Matrix34 mtx = GetMatrix();
		Vector3 scalingVec(4.f,(f32)(m_IsMinimised ? 1 : GetModuleHeight()),1.f);
        if(OverrideBounds())
        {
            float width,height;
            ComputeWidthAndHeight(width,height);
            scalingVec = Vector3(width*0.5f,height*0.5f,1.f);
        }
		mtx.Transform3x3(scalingVec);
		mtx.Scale(scalingVec);
		grcWorldMtx(mtx);
		DrawVB(GetShader()->LookupTechnique("drawmodule"));
	}

	void synthModuleView::DrawPins() const
	{
		u32 numPins;
		synthPin *pins;
		m_Module->GetInputs(pins, numPins);
		for(u32 i = 0 ; i <  numPins; i ++)
		{
			m_Module->GetInput(i).GetView()->Draw();
		}

		m_Module->GetOutputs(pins, numPins);
		for(u32 i = 0 ; i < numPins; i++)
		{
			m_Module->GetOutput(i).GetView()->Draw();
		}
	}

	void synthModuleView::Render()
	{
		PF_FUNC(TotalDrawTime);
		const bool isHighlighted = IsUnderMouse();
		if(m_Module->GetView()->IsGrouped())
		{
			SetShaderVar("g_EdgeHighlightColour", m_Module->GetView()->GetGroup()->GetColour());
		}
		else
		{
			SetShaderVar("g_EdgeHighlightColour", isHighlighted ? g_ModuleEdgeColourHighlight : g_ModuleEdgeColourNormal);
		}
		SetShaderVar("g_HighlightScale", isHighlighted ? 1.f : 0.f);
		Matrix34 mtx = GetMatrix();
		Vector3 scalingVec(4.f,(f32)(m_IsMinimised ? 1 : GetModuleHeight()),1.f);
        if(OverrideBounds())
        {
            float width,height;
            ComputeWidthAndHeight(width,height);
            scalingVec = Vector3(width*0.5f,height*0.5f,1.f);
        }
		mtx.Transform3x3(scalingVec);
		mtx.Scale(scalingVec);
		grcWorldMtx(mtx);
		DrawVB(GetShader()->LookupTechnique("drawmodule"));


		synthUIView::Render();
	}

	void synthModuleView::RenderWires() 
	{
		PF_FUNC(TotalDrawWiresTime);

		// render wires to any connected input
		u32 numPins;
		synthPin *pins;
		m_Module->GetInputs(pins, numPins);
		for(u32 i = 0 ; i <  numPins; i ++)
		{
			if(m_Module->GetInput(i).IsConnected())
			{
				static f32 pinStartOffset = 0.35f;

				Vector3 pin0;
				Vector3 pin1;


				Vector3 b0,b1;
				m_Module->GetInput(i).GetView()->GetBounds(b0,b1);

				const f32 pin0Width = pinStartOffset * m_Module->GetInput(i).GetView()->ComputeWidth(); 
				pin0 = m_Module->GetInput(i).GetView()->GetMatrix().d + pin0Width * m_Module->GetInput(i).GetView()->GetMatrix().a;		

				
				Vector3 b2,b3;
				m_Module->GetInput(i).GetOtherPin()->GetView()->GetBounds(b2,b3);
				const f32 pin1Width = -pinStartOffset * m_Module->GetInput(i).GetOtherPin()->GetView()->ComputeWidth();
				pin1 = m_Module->GetInput(i).GetOtherPin()->GetView()->GetMatrix().d + pin1Width * m_Module->GetInput(i).GetOtherPin()->GetView()->GetMatrix().a;
				
				// dont draw wires between modules in the same collapsed group			
				const bool shouldDrawHighlighted = m_Module->GetInput(i).GetView()->IsUnderMouse() || m_Module->GetInput(i).GetOtherPin()->GetView()->IsUnderMouse();
				f32 peakValue = m_Module->GetInput(i).GetView()->GetPeakValue();
				Color32 colour;
				f32 highlightAmount = 0.f;
				if(shouldDrawHighlighted)
				{
					highlightAmount = 1.f;
				}
				else
				{
					highlightAmount = 0.5f;
				}

				// Tab key highlights dynamic cables
				if(ioKeyboard::KeyDown(KEY_TAB))
				{
					if(m_Module->GetInput(i).GetOtherPin()->GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
					{
						highlightAmount = 2.f;
					}
					else
					{
						highlightAmount = -0.5f;
					}
				}	

				if(m_Module->GetInput(i).GetOtherPin()->GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
				{
					// signal colour
					colour = g_SignalWireColour;
				}
				else
				{
					// normalized data colour
					colour = g_NormalizedWireColour;
				}

				RenderWire(GetMatrix(), pin1, pin0, peakValue, colour, highlightAmount);
			
			}
		}
	}

	bool synthModuleView::StartDrag() const
	{		
		u32 numPins;
		synthPin *pins;
		m_Module->GetInputs(pins, numPins);
		for(u32 i = 0 ; i < (m_IsMinimised ? Min(numPins, 1U) : numPins); i ++)
		{
			if(m_Module->GetInput(i).GetView()->IsUnderMouse())
			{
				return false;
			}
		}

		m_Module->GetOutputs(pins, numPins);
		for(u32 i = 0 ; i < (m_IsMinimised ? Min(numPins, 1U) : numPins); i ++)
		{
			if(m_Module->GetOutput(i).GetView()->IsUnderMouse())
			{
				return false;
			}
		}
		return true;
	}

	void synthModuleView::RenderWire(const Matrix34 &planeWorldMtx, const Vector3 &p0World, const Vector3 &p1World, const f32 intensity, const Color32 colour, const f32 highlightAmount)
	{
		Vector3 p0,p1;
		planeWorldMtx.UnTransform3x3(p0World, p0);
		planeWorldMtx.UnTransform3x3(p1World, p1);

		Vector3 dir = (p1-p0);
		const f32 length = dir.Mag();
		
		dir.Normalize();

		const Vector3 centre = (p0+p1) * 0.5f + Vector3(0.f,0.f,0.2f);

		Vector3 p2;
		p2.x = p1.x;
		p2.y = p0.y;
		p2.z = p0.z;

		//const f32 adj = (p2-p0).Mag();
		const f32 opp = (p1-p2).Mag();
		const f32 hyp = (p1-p0).Mag();
		
		Matrix34 worldMtx;
		worldMtx.Identity();		
		worldMtx.Scale(length/2.f,0.6f,1.f);

		const f32 dotWithX = (p1-p0).Dot(XAXIS);
		const f32 dotWithY = (p1-p0).Dot(YAXIS);

		const f32 quadrantMult = Sign(dotWithX) * Sign(dotWithY);

		// if dot with X is positive then need to flip the wire along its x axis
		worldMtx.RotateZ(quadrantMult * AsinfSafe(opp/hyp) + (dotWithX>0.f ? PI : 0.f));
		worldMtx.RotateY(dir.z);
		
		worldMtx.Translate(centre);
		
		worldMtx.Dot3x3(planeWorldMtx);

		grcWorldMtx(worldMtx);
		SetShaderVar("g_WaveXOffset", (TIME.GetFrameCount() % 120)/120.f);
		SetShaderVar("g_CurrentValue",intensity);
		SetShaderVar("g_TextColour",colour);
		SetShaderVar("g_HighlightScale", highlightAmount);
		SetShaderVar("DiffuseTex", sm_NoiseTexture);
		
		DrawVB("drawwire");
	}
    
    void synthModuleView::DeleteContents()
    {
        synthSynthesizerView::Get()->GetModulePlane().DeleteModule(GetModule());
    }

    synthUIView *synthModuleView::DuplicateContents(const bool recreateConnections)
    {
		if(m_Module->GetTypeId() == SYNTH_AUDIOOUTPUT)
		{
			return NULL;
		}

		synthModuleBinaryMemSerializer serializer;

		m_Module->Serialize(&serializer);

		synthModule *newModule = synthModuleFactory::CreateModule(m_Module->GetTypeId());
		serializer.ResetPointer(true);

		newModule->Serialize(&serializer);

		if(recreateConnections)
		{
			if(m_Module->GetTypeId() == SYNTH_JUNCTIONPIN)
			{
				if(m_Module->GetOutput(0).GetView()->GetOutputDestinations().GetCount() > 0)
				{
					atArray<synthPin *> pinsToConnect;
					for(s32 i = 0; i < m_Module->GetOutput(0).GetView()->GetOutputDestinations().GetCount(); i++)
					{
						pinsToConnect.PushAndGrow(m_Module->GetOutput(0).GetView()->GetOutputDestinations()[i]);
					}
					for(s32 i = 0; i < pinsToConnect.GetCount() ; i++)
					{
						pinsToConnect[i]->Disconnect();
						pinsToConnect[i]->Connect(newModule->GetOutput(0));
					}
				}

				if(newModule->GetInput(0).IsConnected())
				{
					newModule->GetInput(0).Disconnect();
				}
				newModule->GetInput(0).Connect(m_Module->GetOutput(0));
			}
			else
			{
				if(!ioKeyboard::KeyDown(KEY_SHIFT))
				{
					// preserve inputs
					synthPin *pins = NULL;
					u32 numPins = 0;
					m_Module->GetInputs(pins,numPins);
	
					for(u32 i = 0; i < numPins; i++)
					{
						if(pins[i].IsConnected())
						{
							newModule->GetInput(i).Connect(*pins[i].GetOtherPin());
						}
					}
				}
			}
		}		
		
		newModule->GetView()->SetMatrix(GetMatrix());
		synthSynthesizerView::Get()->GetSynth()->AddModule(newModule);
		synthSynthesizerView::Get()->GetSynth()->ComputeProcessingGraph();

		return newModule->GetView();
    }
}

#endif // __SYNTH_EDITOR
