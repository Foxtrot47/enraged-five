// 
// audiosynth/egview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_EGVIEW_H
#define SYNTH_EGVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uilabel.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthEnvelopeGenerator;
	class grcTexture;
	class synthEnvelopeGeneratorView : public synthModuleView
	{
	public:

		synthEnvelopeGeneratorView(synthModule *module);
		~synthEnvelopeGeneratorView();
		virtual void Update();
		virtual bool StartDrag() const;
	protected:
		virtual void Render();
	private:
		synthEnvelopeGenerator *GetEnvelope() const { return (synthEnvelopeGenerator*)m_Module; }

		f32 m_ReleaseStartGain;
		synthUILabel m_TotalTimeLabel;
		synthUIDropDownList m_ReleaseType;
		synthUIDropDownList m_TriggerMode;
		char m_TotalTime[32];
		grcTexture *m_Texture;

		f32 m_LastSustainLevel;
		f32 m_LastTotalTime;
		s32 m_LastReleaseType;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_EGVIEW_H

