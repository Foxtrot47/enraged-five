
#ifndef SYNTH_CHARTRECORDER_H
#define SYNTH_CHARTRECORDER_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
    class synthModuleView;
    class synthChartRecorder : public synthModuleBase<2,0,SYNTH_CHARTRECORDER>
    {
    public:
        SYNTH_MODULE_NAME("Chart Recorder");

        synthChartRecorder();
        virtual ~synthChartRecorder();

        virtual void Synthesize();

#if __SYNTH_EDITOR

        void SetBufferLength(const u32 length) { m_Length = Min(length, m_AllocatedLength); }
        u32 GetBufferLength() const { return m_Length; }
        const float *GetBuffer() const { return m_Buffer; }
        u32 GetWriteOffset() const { return m_WriteOffsetSamples; }

#endif


    private:
        SYNTH_CUSTOM_MODULE_VIEW;

#if __SYNTH_EDITOR
        
        float *m_Buffer;
        u32 m_AllocatedLength;
        u32 m_Length;
        u32 m_WriteOffsetSamples;
#endif
    };
}
#endif // !SYNTH_MINIMAL_MODULES
#endif // SYNTH_FFT_H


