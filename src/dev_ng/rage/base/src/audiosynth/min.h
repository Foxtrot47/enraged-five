// 
// audiosynth/min.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_MIN_H
#define SYNTH_MIN_H

#include "sampleframe.h"
#include "module.h"
#include "pin.h"

#include "modulefactory.h"

namespace rage
{
	enum
	{
		kMinSignal = 0,
		kMinReset,

		kNumMinInputs
	};

	class synthMin : public synthModuleBase<kNumMinInputs,1,SYNTH_MIN>
	{
	public:

		SYNTH_MODULE_NAME("Min");

		synthMin();

		virtual void Synthesize();
		virtual void ResetProcessing();

		enum synthMinMode
		{
			kMinStatic = 0,
			kMinDynamic
		};

		void SetMode(const synthMinMode mode) { m_Mode = mode; }
		synthMinMode GetMode() const { return m_Mode; }

		static float Process(const float *RESTRICT const inBuffer, const float resetTrigger, Vec::Vector_4V_InOut state, const u32 numSamples);
		static float Process(const float input, const float resetTrigger, Vec::Vector_4V_InOut state);

	private:

		synthSampleFrame m_Buffer;
		synthMinMode m_Mode;

		f32 m_CurrentVal;
	};
}

#endif // SYNTH_MIN_H


