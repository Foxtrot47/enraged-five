#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "audiohardware/mixer.h"
#include "mverb.h"
#include "pin.h"

namespace rage
{
	synthMVerb::synthMVerb() 
	{
		m_Inputs[kMVerbSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kMVerbDampingFreq].Init(this, "DampingFreq",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMVerbDensity].Init(this, "Density",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMVerbBandwidthFreq].Init(this, "BWFreq",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMVerbDecay].Init(this, "Decay",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMVerbPredelay].Init(this, "Predelay",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMVerbSize].Init(this, "Size",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		
		m_Inputs[kMVerbEarlyMix].Init(this, "EarlyMix", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMVerbGain].Init(this, "EarlyMix", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMVerbMix].Init(this, "EarlyMix", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		
		m_Outputs[0].Init(this, "OutputL", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_BufferL);
		m_Outputs[1].Init(this, "OutputR", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[1].SetDataBuffer(&m_BufferR);

#if !__PS3
		m_Reverb.Reset();
		m_Reverb.SetSampleRate(kMixerNativeSampleRate);

		for(s32 i = 1; i <= kMVerbMix; i++)
		{
			m_Inputs[i].SetStaticValue(GetModelParam(i - 1));
		}
#endif
	}

	synthMVerb::~synthMVerb()
	{

	}

	void synthMVerb::Synthesize()
	{
#if !__PS3
		for(s32 i = 1; i <= kMVerbMix; i++)
		{
			if(i != kMVerbSize)
			{
				SetModelParam(i - 1, m_Inputs[i].GetNormalizedValue(0));
			}
		}

		if(m_Inputs[0].GetDataState()==synthPin::SYNTH_PIN_DYNAMIC)
		{
			float *outPtrs[] = {m_BufferL.GetBuffer(), m_BufferR.GetBuffer()};
			float *inPtrs[] = {m_Inputs[0].GetDataBuffer()->GetBuffer(),m_Inputs[0].GetDataBuffer()->GetBuffer()};
			m_Reverb.Process(inPtrs,outPtrs,kMixBufNumSamples);
		}
#endif
	}
}
#endif // __SYNTH_EDITOR
