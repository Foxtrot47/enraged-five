
#ifndef SYNTH_PINKINGFILTER_H
#define SYNTH_PINKINGFILTER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthPinkingFilter : public synthModuleBase<1,1,SYNTH_PINKINGFILTER>
	{
	public:

		synthPinkingFilter();
		virtual ~synthPinkingFilter(){};

		virtual void Synthesize();

		virtual const char *GetName() const
		{
			return "PinkingFilter";
		}

	private:

		synthSampleFrame m_Buffer;

		f32 m_b0;
		f32 m_b1;
		f32 m_b2;
		f32 m_b3;
		f32 m_b4;
		f32 m_b5;
		f32 m_b6;
	};
}
#endif
#endif // SYNTH_PINKINGFILTER_H


