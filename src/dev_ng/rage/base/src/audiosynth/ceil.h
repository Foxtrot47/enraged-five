// 
// audiosynth/ceil.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_CEIL_H
#define SYNTH_CEIL_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "pin.h"

#include "modulefactory.h"

namespace rage
{
	class synthCeil : public synthModuleBase<1,1,SYNTH_CEIL>
	{
	public:
		SYNTH_MODULE_NAME("Ceiling");

		synthCeil();
		virtual void Synthesize();

		static void Process(float *RESTRICT inOutBuffer, const u32 numSamples);
		static void Process(float *RESTRICT outBuffer, const float *RESTRICT inBuffer, const u32 numSamples);

	private:
		synthSampleFrame m_Buffer;
	};
}
#endif // !SYNTH_MINIMAL_MODULES
#endif // SYNTH_CEIL_H


