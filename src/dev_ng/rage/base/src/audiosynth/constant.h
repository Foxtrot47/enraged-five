// 
// audiosynth/constant.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOSYNTH_CONSTANT_H 
#define AUDIOSYNTH_CONSTANT_H 

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES
#include "module.h"
#include "modulefactory.h"

namespace rage {
	class synthConstant : public synthModuleBase<1,1,SYNTH_CONSTANT>
	{
	public:
		SYNTH_MODULE_NAME("Constant");

		synthConstant();
		virtual void Synthesize();

	};
} // namespace rage
#endif // !SYNTH_MINIMAL_MODULES
#endif // AUDIOSYNTH_CONSTANT_H 
