
#include "synthdefs.h"

#include "awnoise.h"
#include "noisegenerator.h"
#include "synthutil.h"

#include "audioengine/engineutil.h"
#include "math/random.h"
#include "math/amath.h"

namespace rage
{
	using namespace Vec;
	synthAWNoiseGenerator::synthAWNoiseGenerator() 
	{
		m_Inputs[kAWNoisePulseWidth].Init(this, "Width",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAWNoisePulseWidth].SetStaticValue(0.f);

		m_Inputs[kAWNoisePulseWidthVariance].Init(this, "WidthVar",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAWNoisePulseWidthVariance].SetStaticValue(1.f);

		m_Inputs[kAWNoiseAmplitude].Init(this, "Amplitude", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAWNoiseAmplitude].SetStaticValue(1.f);

		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_State = V4VConstant(V_ZERO);
	}

	void synthAWNoiseGenerator::ResetProcessing()
	{
		m_State = V4VConstant(V_ZERO);
	}

	void synthAWNoiseGenerator::Synthesize()
	{
		// Prepare input buffers
		Vector_4V *ptr1 = m_In1Buffer.GetBufferV();
		Vector_4V *ptr2 = m_In2Buffer.GetBufferV();
		for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
		{
			*ptr1 = m_Inputs[kAWNoisePulseWidth].GetNormalizedValueV(i);
			*ptr2 = m_Inputs[kAWNoisePulseWidthVariance].GetNormalizedValueV(i);
			ptr1++;
			ptr2++;
		}

		// Pre-populate input/output buffer with white noise
		synthNoiseGenerator::Generate(m_Buffer.GetBuffer(), kMixBufNumSamples);
		synthAWFilter::Process(m_Buffer.GetBuffer(), m_In1Buffer.GetBuffer(), m_In2Buffer.GetBuffer(), m_State, kMixBufNumSamples);


		ptr1 = m_In1Buffer.GetBufferV();
		for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
		{
			*ptr1++ = m_Inputs[kAWNoiseAmplitude].GetNormalizedValueV(i);
		}

		synthUtil::ScaleBuffer(m_Buffer.GetBuffer(), m_In1Buffer.GetBuffer(), kMixBufNumSamples);

		/*
		f32 lastVal = m_LastVal;

		for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i++)
		{
			const f32 noiseWidth = 1.f + (127.f * m_Inputs[kAWNoisePulseWidth].GetNormalizedValue(i))
					+ audEngineUtil::GetRandomNumberInRange(0.f, 64.f*m_Inputs[kAWNoisePulseWidthVariance].GetNormalizedValue(i));
			const f32 randomNoise = audEngineUtil::GetRandomNumberInRange(0.f,2.f);

			//smoothing
			const f32 noiseDelta = lastVal - randomNoise;
			const f32 stepAmount = noiseDelta / noiseWidth;
			const f32 outputVal = lastVal - stepAmount;
			lastVal = outputVal;
			m_Outputs[0].GetDataBuffer()->GetBuffer()[i] = (lastVal-1.f) * m_Inputs[kAWNoiseAmplitude].GetNormalizedValue(i);
		}
		m_LastVal = lastVal;
		*/
	}

	void synthAWFilter::Process(float *inOut, const float *pulseWidthIn, const float *pulseWidthVarianceIn, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		Vector_4V lastVal = state;

		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOut);
		const Vector_4V *pulseWidth = reinterpret_cast<const Vector_4V*>(pulseWidthIn);
		const Vector_4V *pulseWidthVariance = reinterpret_cast<const Vector_4V*>(pulseWidthVarianceIn);

		const Vector_4V v127 = V4VConstant<0x42FE0000,0x42FE0000,0x42FE0000,0x42FE0000>();
		const Vector_4V v64 = V4VConstant<0x42800000,0x42800000,0x42800000,0x42800000>();

		u32 count = numSamples>>2;
		while(count--)
		{
			Vector_4V scaledWidth = V4Scale(v64, *pulseWidthVariance++);
			Vector_4V widthVar = V4Scale(scaledWidth, synthUtil::RandNormV());

			Vector_4V noiseWidth = V4Add(widthVar, V4AddScaled(V4VConstant(V_ONE), v127, *pulseWidth++)); 
			Vector_4V randomNoise = *inOutPtr;
			Vector_4V invNoiseWidth = V4Invert(noiseWidth);

			//filtering; need to process serially
			//const f32 noiseDelta = lastVal - randomNoise;
			//const f32 stepAmount = noiseDelta / noiseWidth;
			//const f32 outputVal = lastVal - stepAmount;

			Vector_4V outX = V4SplatX(V4Subtract(lastVal, V4Scale(V4Subtract(lastVal, randomNoise), invNoiseWidth)));
			Vector_4V outY = V4SplatY(V4Subtract(outX, V4Scale(V4Subtract(outX, randomNoise), invNoiseWidth)));
			Vector_4V outZ = V4SplatZ(V4Subtract(outY, V4Scale(V4Subtract(outY, randomNoise), invNoiseWidth)));
			Vector_4V outW = V4SplatW(V4Subtract(outZ, V4Scale(V4Subtract(outZ, randomNoise), invNoiseWidth)));

			Vector_4V outXY = V4PermuteTwo<X1,Y2,X1,Y2>(outX, outY);
			Vector_4V outZW = V4PermuteTwo<X1,Y2,X1,Y2>(outZ, outW);
			Vector_4V out = V4PermuteTwo<X1,Y1,X2,Y2>(outXY,outZW);
			
			lastVal = outW; // already splatted
			
			*inOutPtr++ = out;

		}
		state = lastVal;
	}

	synthAWFilter::synthAWFilter() 
	{
		m_Inputs[kAWFilterInput].Init(this, "Width", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kAWFilterInput].SetStaticValue(0.f);
		
		m_Inputs[kAWFilterPulseWidth].Init(this, "Width",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAWFilterPulseWidth].SetStaticValue(0.f);

		m_Inputs[kAWFilterPulseWidthVariance].Init(this, "WidthVar",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAWFilterPulseWidthVariance].SetStaticValue(1.f);

		m_Inputs[kAWFilterAmplitude].Init(this, "Amplitude", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAWFilterAmplitude].SetStaticValue(1.f);

		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_State = V4VConstant(V_ZERO);
	}

	void synthAWFilter::ResetProcessing()
	{
		m_State = V4VConstant(V_ZERO);
	}

	void synthAWFilter::Synthesize()
	{
		// Prepare input buffers
		Vector_4V *ptr1 = m_In1Buffer.GetBufferV();
		Vector_4V *ptr2 = m_In2Buffer.GetBufferV();
		Vector_4V *inPtr = m_Buffer.GetBufferV();
		for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
		{
			*ptr1 = m_Inputs[kAWFilterPulseWidth].GetNormalizedValueV(i);
			*ptr2 = m_Inputs[kAWFilterPulseWidthVariance].GetNormalizedValueV(i);
			*inPtr = m_Inputs[kAWFilterInput].GetSignalValueV(i);
			ptr1++;
			ptr2++;
			inPtr++;
		}
		
		synthAWFilter::Process(m_Buffer.GetBuffer(), m_In1Buffer.GetBuffer(), m_In2Buffer.GetBuffer(), m_State, kMixBufNumSamples);

		ptr1 = m_In1Buffer.GetBufferV();
		for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
		{
			*ptr1++ = m_Inputs[kAWFilterAmplitude].GetNormalizedValueV(i);
		}

		synthUtil::ScaleBuffer(m_Buffer.GetBuffer(), m_In1Buffer.GetBuffer(), kMixBufNumSamples);
	}

	
}

