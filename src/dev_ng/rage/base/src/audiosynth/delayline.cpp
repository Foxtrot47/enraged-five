#include "delayline.h"

#if !SYNTH_MINIMAL_MODULES

#include "math/amath.h"
#include "system/memops.h"

namespace rage
{	
	synthDelayLine::synthDelayLine()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_OutputBuffer);
		sysMemSet128(m_OutputBuffer.GetBuffer(), 0, m_OutputBuffer.GetSize() * sizeof(f32));
		
		m_DelayBuffer = rage_aligned_new(16) f32[MAX_DELAY_SAMPLES];
		sysMemSet(m_DelayBuffer, 0, MAX_DELAY_SAMPLES * sizeof(f32));
		
		m_Inputs[kDelayLineDryMix].Init(this, "DryMix", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDelayLineDryMix].SetStaticValue(0.707f);
		m_Inputs[kDelayLineWetMix].Init(this, "WetMix",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDelayLineWetMix].SetStaticValue(0.707f);
		m_Inputs[kDelayLineFeedback].Init(this, "Feedback", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDelayLineFeedback].SetStaticValue(0.f);
		m_Inputs[kDelayLineSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kDelayLineDelay].Init(this, "DelayTime", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDelayLineDelay].SetStaticValue(1.f);
		
		m_DelayWriteIndex = 0;
		m_MinDelaySamples = 0.f;
		m_MaxDelaySamples = (f32)MAX_DELAY_SAMPLES;

		sysMemSet(m_SampleHistory, 0, sizeof(m_SampleHistory));
	}

	synthDelayLine::~synthDelayLine()
	{
		delete[] m_DelayBuffer;
		m_DelayBuffer = NULL;
	}
	
	void synthDelayLine::Synthesize()
	{
		f32 y0 = m_SampleHistory[0];
		f32 y1 = m_SampleHistory[1];
		f32 y2 = m_SampleHistory[2];
		f32 y3 = m_SampleHistory[3];

		f32 delayScaler = m_Inputs[kDelayLineDelay].GetNormalizedValue(0);
		f32 delayTimeSamples = Max(1.f, Clamp(Lerp(delayScaler, m_MinDelaySamples, m_MaxDelaySamples), 0.f, (f32)MAX_DELAY_SAMPLES));
		
		s32 delayTimeSamplesInt = static_cast<s32>(delayTimeSamples);

		for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i++)
		{
			const f32 dryMix = m_Inputs[kDelayLineDryMix].GetNormalizedValue(i);
			const f32 inputSample = m_Inputs[kDelayLineSignal].GetSignalValue(i);
			
			const f32 wetMix = m_Inputs[kDelayLineWetMix].GetNormalizedValue(i);
			const f32 feedback = m_Inputs[kDelayLineFeedback].GetNormalizedValue(i);
			
			f32 delayedSample = m_DelayBuffer[m_DelayWriteIndex];
			if(m_Inputs[kDelayLineDelay].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				//  interpolate to prevent clicking
				const f32 outputSample = delayedSample * 0.6f + y0 * 0.25f + y1 * 0.125f + y2 * 0.0625f + y3 * 0.03125f;
				delayedSample = outputSample;
				y3 = y2; y2 = y1; y1 = y0; y0 = outputSample;

				// recalculate delay line length
				delayScaler = m_Inputs[kDelayLineDelay].GetNormalizedValue(i);
				delayTimeSamples = Max(1.f, Clamp(Lerp(delayScaler, m_MinDelaySamples, m_MaxDelaySamples), 0.f, (f32)MAX_DELAY_SAMPLES));

				delayTimeSamplesInt = static_cast<s32>(delayTimeSamples);
			}
			
			// write back to delay buffer
			m_DelayBuffer[m_DelayWriteIndex] = inputSample + (delayedSample*feedback);
			
			// write output
			m_Outputs[0].GetDataBuffer()->GetBuffer()[i] = inputSample*dryMix + delayedSample*wetMix;
			
			// step through delay buffer
			m_DelayWriteIndex = (m_DelayWriteIndex + 1) % (delayTimeSamplesInt);
		}
		m_SampleHistory[0] = y0;
		m_SampleHistory[1] = y1;
		m_SampleHistory[2] = y2;
		m_SampleHistory[3] = y3;
	}//synthesize
}
#endif
