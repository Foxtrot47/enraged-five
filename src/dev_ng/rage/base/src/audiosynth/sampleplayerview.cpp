// 
// audiosynth/sampleplayerview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "audiohardware/driverutil.h"
#include "audiohardware/waveslot.h"
#include "audiodata/container.h"

#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"

#include "sampleplayer.h"
#include "sampleplayerview.h"
#include "synthesizerview.h"
#include "moduleview.h"
#include "pinview.h"
#include "uiplane.h"

#include "module.h"
#include "pin.h"

namespace rage
{
	synthSamplePlayerView::synthSamplePlayerView(synthModule *module) : synthModuleView(module), m_TotalTimeLabel(640)
	{
		AddComponent(&m_TotalTimeLabel);
		AddComponent(&m_ModeLabel);
		AddComponent(&m_WaveNameLabel);
		AddComponent(&m_BankList);
		
		m_InputBox.SetShouldDraw(false);
		m_InputBox.SetAllowAlphaChars(true);

		formatf(m_WaveName,"WaveName");		

		// Populate static bank list
		const u32 currentSlotId = GetSamplePlayer()->GetWavePlayer()->HasValidWave() ? GetSamplePlayer()->GetWavePlayer()->GetWaveSlotId() : ~0U;
		u32 idx = 0;
		u32 selectedIndex = 0;
		for(u32 i = 0; i < audWaveSlot::GetNumWaveSlots(); i++)
		{
			audWaveSlot *slot = audWaveSlot::GetWaveSlotFromIndexFast(i);
			if(!slot->IsStreaming() && slot->GetLoadedBankId() < AUD_INVALID_BANK_ID)
			{
				const char *bankName = slot->GetLoadedBankName();
				m_BankNameMap.Insert(m_BankList.AddItem(bankName), bankName);

				if(currentSlotId == i)
				{
					selectedIndex = idx;
				}
				idx++;
			}
		}
		m_BankList.SetCurrentIndex(selectedIndex);
		m_Texture = NULL;
	}

	synthSamplePlayerView::~synthSamplePlayerView()
	{
		if(m_Texture)
		{
			m_Texture->Release();
			m_Texture = NULL;
		}
	}

	void synthSamplePlayerView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		Vector3 v3,v4;
		synthModuleView::GetBounds(v1,v2);
		m_BankList.GetBounds(v3,v4);
		v1.Min(v1,v3);
		v2.Max(v2,v4);
	}

	void synthSamplePlayerView::Update()
	{
		const audWavePlayer *wavePlayer = GetSamplePlayer()->GetWavePlayer();
		Assert(wavePlayer);
		if(GetSamplePlayer()->GetWavePlayer()->HasValidWave())
		{
			const u32 numSamplesToRender = 1024;
			if(!m_Texture)
			{
				m_Texture = synthUIComponent::CreateTexture(numSamplesToRender, 1, grcImage::R32F);				
			}			

			grcTextureLock lock;

			m_Texture->LockRect(0,0,lock,grcsWrite);
			f32 *textPtrF32 = (f32*)lock.Base;

			// rasterize the wave envelope
			u32 lastSampleIndex = 0;
			for(s32 x = 0; x < numSamplesToRender; x++)
			{
				u32 sampleIndex = static_cast<u32>((x / (f32)numSamplesToRender) * wavePlayer->GetLengthSamples());

				f32 maxValue = 0.f;
				for(u32 s = lastSampleIndex; s < sampleIndex; s++)
				{
					const f32 val = (wavePlayer->GetWaveDataPtr()[sampleIndex] / 32768.f);
					if(Abs(val)>maxValue)
					{
						maxValue = val;
					}
				}

				lastSampleIndex = sampleIndex;
				textPtrF32[x] = (1.f+maxValue)/2.f;
			}

			m_Texture->UnlockRect(lock);

			formatf(m_TotalTime, sizeof(m_TotalTime), "%.2fs (%u samples@%.2fkHz)", wavePlayer->GetLengthSamples() / (f32)wavePlayer->GetAuthoredSampleRate(), wavePlayer->GetLengthSamples(), wavePlayer->GetAuthoredSampleRate()/1000.f);
			m_TotalTimeLabel.SetString(m_TotalTime);
			m_TotalTimeLabel.SetShouldDraw(!IsMinimised());
		}
		else
		{
			m_TotalTimeLabel.SetShouldDraw(false);
		}

		Matrix34 mtx = GetMatrix();
		mtx.d -= mtx.b*0.5f;
		m_BankList.SetMatrix(mtx);
		m_BankList.SetShouldDraw(!IsMinimised());
		m_BankList.SetAlpha(GetAlpha());
		
		mtx.d -= mtx.b*0.5f;
		m_WaveNameLabel.SetMatrix(mtx);
		m_WaveNameLabel.SetSize(0.25f);
		m_WaveNameLabel.SetString(strlen(m_WaveName) > 0 ? m_WaveName : "WaveName");
		m_WaveNameLabel.SetShouldDraw(!IsMinimised());
		m_WaveNameLabel.SetAlpha(GetAlpha());

		mtx.d -= mtx.b * 1.f;
		m_ModeLabel.SetMatrix(mtx);
		m_ModeLabel.SetSize(0.25f);
		const char *modeNames[synthSamplePlayer::kNumPlayModes] = 
		{
			"One Shot",
			"Looping"
		};
		m_ModeLabel.SetString(modeNames[GetSamplePlayer()->GetPlayMode()]);
		m_ModeLabel.SetShouldDraw(!IsMinimised());
		m_ModeLabel.SetAlpha(GetAlpha());

		mtx = GetMatrix();
		mtx.d += mtx.b * 1.f;
		m_TotalTimeLabel.SetMatrix(mtx);
		m_TotalTimeLabel.SetSize(0.15f);
		m_TotalTimeLabel.SetAlpha(GetAlpha());

		if(m_BankList.IsShowingMenu())
		{
			m_TotalTimeLabel.SetShouldDraw(false);
			m_ModeLabel.SetShouldDraw(false);
		}
		
		SetTitleAlpha(1.f - GetHoverFade());
		synthModuleView::Update();

		m_TitleLabel.SetUnderMouse(IsUnderMouse());
		m_TotalTimeLabel.SetUnderMouse(false);

		if(synthUIPlane::GetMaster()->HadLeftClick())
		{
			if(m_ModeLabel.IsUnderMouse())
			{
				const s32 mode = (GetSamplePlayer()->GetPlayMode()+1) % synthSamplePlayer::kNumPlayModes;
				GetSamplePlayer()->SetPlayMode((synthSamplePlayer::PlayMode)mode);
			}
		}

		if(m_InputBox.ShouldDraw())
		{
			m_InputBox.Update();
			if(m_InputBox.HadEnter())
			{
				formatf(m_WaveName, "%s", m_InputBox.GetKeyboardBuffer());				
				UpdateWaveSelection();
				m_InputBox.SetShouldDraw(false);
				synthSynthesizerView::ClearModalComponent();
			}
		}
		else
		{
			if(m_WaveNameLabel.IsUnderMouse() && synthUIPlane::GetMaster()->HadLeftDoubleClick())
			{
				if(synthSynthesizerView::SetModalComponent(&m_InputBox))
				{
					m_InputBox.Reset();
					m_InputBox.SetShouldDraw(true);
					m_InputBox.Update();
				}
			}
		}
	}

	void synthSamplePlayerView::Render()
	{	
		synthModuleView::Render();

		Matrix34 mtx = GetMatrix();
		if(!IsMinimised())
		{
			mtx.d += mtx.b * 2.f;
		}

		Vector3 scalingVec(2.f,0.9f,1.f);
		mtx.Transform3x3(scalingVec);
		mtx.Scale(scalingVec);
		grcWorldMtx(mtx);

		if(GetSamplePlayer()->GetWavePlayer()->HasValidWave())
		{
			SetShaderVar("FontTex",m_Texture);
			SetShaderVar("DiffuseTex",m_Texture);
			SetShaderVar("g_WaveXZoom", 1.f);
			SetShaderVar("g_WaveYZoom", 1.f);
			SetShaderVar("g_WaveXOffset", 0.f);
			SetShaderVar("g_GhostLevel", 0.f);
			SetShaderVar("g_TextColour", Color32(0.f,0.f,1.f,1.f));
			SetShaderVar("g_AlphaFade", GetAlpha());
			DrawVB("drawwave");		
		}
	}

	void synthSamplePlayerView::UpdateWaveSelection()
	{
		const char **bankName = m_BankNameMap.Access(m_BankList.GetCurrentIndex());
		if(!bankName)
		{
			return;
		}
		const u32 bankId = audWaveSlot::FindBankId(*bankName);
		if(bankId >= AUD_INVALID_BANK_ID)
		{
			audErrorf("Invalid bank name: %s", *bankName);
		}
		else
		{
			const s32 waveSlotId = audWaveSlot::GetWaveSlotIndex(audWaveSlot::FindLoadedBankWaveSlot((u16)bankId)); 
			if(waveSlotId != -1)
			{
				const audWaveSlot *waveSlot = audWaveSlot::GetWaveSlotFromIndex(waveSlotId);
				if(waveSlot->GetContainer().FindObject(m_WaveName) != adatContainer::InvalidId)
				{
					GetSamplePlayer()->SetWave((u32)waveSlotId, atStringHash(m_WaveName));
					// invalidate the texture
					if(m_Texture)
					{
						m_Texture->Release();
						m_Texture = NULL;
					}
				}
				else
				{
					audErrorf("Failed to find wave %s in bank %s", m_WaveName, *bankName);
				}
			}
			else
			{
				audErrorf("Bank not statically loaded: %s", *bankName);
			}
		}
	}

	bool synthSamplePlayerView::StartDrag() const
	{
		if(m_ModeLabel.IsUnderMouse() || m_BankList.IsUnderMouse() || m_BankList.IsShowingMenu() || m_WaveNameLabel.IsUnderMouse())
		{
			return false;
		}
		return synthModuleView::StartDrag();
	}
}

#endif // __SYNTH_EDITOR
