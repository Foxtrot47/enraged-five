// 
// audiosynth/pinview.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "pin.h"
#include "pinview.h"
#include "synthesizerview.h"
#include "uilabel.h"
#include "uiplane.h"

#include "audiohardware/mixer.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "input/keys.h"
#include "input/keyboard.h"
#include "input/mouse.h"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	grcRenderTarget *synthPinView::sm_SharedDepthRT = NULL;
	grcRenderTarget *synthPinView::sm_SharedRenderTarget = NULL;
	const Color32 g_SignalPinHighlight = Color32(0.f,0.f,1.f,1.f);
	const Color32 g_NormalizedPinHighlight = Color32(0.f,0.4f,0.0f,1.f);

	enum { kNumFramesToHoldValueDisplay = 0 };

	f32 synthPinView::sm_ZoomScale = 1.3f;
	bool g_DrawPinReflections = true;


	bool synthPinView::InitClass()
	{
		const u32 rtSize = kMixBufNumSamples;
		Assert(!sm_SharedDepthRT);
		Assert(!sm_SharedRenderTarget);
		sm_SharedDepthRT = grcTextureFactory::GetInstance().CreateRenderTarget("__pinview_depth",
			grcrtDepthBuffer, rtSize, rtSize, 16);
	
		sm_SharedRenderTarget = grcTextureFactory::GetInstance().CreateRenderTarget("__pinview_tgt",
			grcrtPermanent, rtSize, rtSize, 32);

		Assert(sm_SharedDepthRT);
		Assert(sm_SharedRenderTarget);
		if(!sm_SharedRenderTarget || !sm_SharedDepthRT)
		{
			return false;
		}

		// ensure the render targets start clear
		grcTextureFactory::GetInstance().LockRenderTarget(0, sm_SharedRenderTarget, sm_SharedDepthRT);
		GRCDEVICE.Clear(true, Color32(0,0,0),true, 1.f,false,0);
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);
	
		return true;
	}

	void synthPinView::ShutdownClass()
	{
		sm_SharedDepthRT->Release();
		sm_SharedDepthRT = NULL;

		sm_SharedRenderTarget->Release();
		sm_SharedRenderTarget = NULL;
	}

	synthPinView::synthPinView(synthPin *pin) : 
		m_Pin(pin),
		m_Texture(NULL),
		m_RTIndex(0),
		m_LastScale(0.f),
		m_TargetScale(0.f),
		m_OverridenTargetScale(0.f),
		m_LastMouseY(0),
		m_PeakValue(0.f),
		m_RMSValue(0.f),
		m_HeldPeakMaxValue(0.f),
		m_HeldPeakMinValue(0.f),
		m_WaveXZoom(1.f),
		m_WaveYZoom(1.f),
		m_DefaultYZoom(1.f),
		m_MinStaticValue(0.f),
		m_MaxStaticValue(1.f),
		m_WaveXOffset(0.f),
		m_FramesSinceStaticValueChanged(kNumFramesToHoldValueDisplay),
		m_DrawFilled(false),
		m_Animate(true),
		m_IsBeingDragged(false),
		m_ShouldBeHighlighted(false),
		m_DisableZooming(false),
		m_ShouldDrawTitle(true),
		m_ShouldDrawReflection(true),
		m_DisableConnections(false),
		m_OverridenDisplayRange(false),
		m_DrawLowLOD(true),
		m_IsTextureLowLOD(true)
	{
		if(pin->GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
		{
			m_MinStaticValue = -1.f;
			m_MaxStaticValue = 1.f;
		}
		else
		{
			m_MinStaticValue = 0.f;
			m_MaxStaticValue = 1.f;
		}
		if(pin->GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_Scrollbar.SetNormalizedValue(pin->GetNormalizedValue(0));
		}
		
		m_TrailRenderTargets[0] = m_TrailRenderTargets[1] = NULL;

		AddComponent(&m_PeakLabel);
		AddComponent(&m_Scrollbar);
		AddComponent(&m_Label);
	}

	synthPinView::~synthPinView()
	{
		// Ensure no other pin views remain connected
		for(s32 i = 0; i < m_OutputDestinationPins.GetCount(); i++)
		{
			m_OutputDestinationPins[i]->Disconnect();
		}

		if(m_Texture)
		{
			m_Texture->Release();
			m_Texture = NULL;
		}
		
		if(m_TrailRenderTargets[0])
		{
			m_TrailRenderTargets[0]->Release();
			m_TrailRenderTargets[0] = NULL;
		}

		if(m_TrailRenderTargets[1])
		{
			m_TrailRenderTargets[1]->Release();
			m_TrailRenderTargets[1] = NULL;
		}
	}

	bool synthPinView::IsActive() const
	{
		return (ioKeyboard::KeyDown(KEY_TAB) || m_FramesSinceStaticValueChanged<kNumFramesToHoldValueDisplay || IsUnderMouse() || IsBeingDragged() || m_Scrollbar.IsActive());
	}

	void synthPinView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		// add extra margin around pin when not zoomed
		//const f32 extraMargin = Lerp(m_LastScale, 1.09f, 1.f);

		v1.Set(-1.f,-1.f,0.f);
		v2.Set(1.f,1.f,0.f);
		Matrix34 mat = GetMatrix();
	
		const Vector3 scalingVec((1.f + m_LastScale*sm_ZoomScale*1.5f),
			(1.f + m_LastScale*sm_ZoomScale),
			1.f + m_LastScale*sm_ZoomScale);
		mat.Scale(scalingVec);
	
		// also move closer to the camera based on scale
		if(grcViewport::GetCurrent())
		{	
			Matrix34 cameraMtx = RCC_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());
			mat.d += 0.2f * m_LastScale * cameraMtx.c;
		}
		mat.Transform(v1);
		mat.Transform(v2);
	}

	void synthPinView::Notify_SetValue()
	{
		// this pin's value has been changed externally (preset / variable input)
		// update the scroll bar with the new value
		m_Scrollbar.SetNormalizedValue(m_Pin->GetNormalizedValue(0));
	}

	void synthPinView::Update()
	{
		synthUIView::Update();

		if(IsUnderMouse())
		{
			if(ioKeyboard::KeyPressed(KEY_P))
			{
				m_Animate = !m_Animate;
			}
			if(ioKeyboard::KeyDown(KEY_R))
			{
				ResetPeaks();
			}
		}

		// LOD based on screen-space test
		bool isOnScreen = false;
		if(grcViewport::GetCurrent())
		{
			const grcViewport *viewport = grcViewport::GetCurrent();
			Vector3 v0,v1;
			GetBounds(v0,v1);

			if(viewport->IsAABBVisible(v0,v1,viewport->GetFrustumLRTB()))
			{
				f32 x0,y0,x1,y1;
				
				viewport->Transform((Vec3V&)v0,x0,y0);
				viewport->Transform((Vec3V&)v1,x1,y1);
				const f32 xL = x1-x0;
				const f32 yL = y1-y0;
				const f32 screenSize = Sqrtf(xL*xL + yL*yL);

				static f32 lowLodScreenSize = 30.f;
				m_DrawLowLOD = screenSize < lowLodScreenSize;
				isOnScreen = true;
			}
		}
		
		static bool supportsF32Textures = __WIN32PC;
		if(isOnScreen && m_Animate && m_Pin->GetDataState() == synthPin::SYNTH_PIN_DYNAMIC && m_Pin->GetDataBuffer()->HasBuffer())
		{
			const s32 numSamplesToRender = m_DrawLowLOD ? 32 : kMixBufNumSamples;

			if(!m_Texture || m_IsTextureLowLOD != m_DrawLowLOD)
			{
				if(m_Texture)
				{
					m_Texture->Release();
					m_Texture = NULL;
				}
				m_IsTextureLowLOD = m_DrawLowLOD;
				m_Texture = synthUIComponent::CreateTexture(numSamplesToRender, 1, grcImage::R32F);	
			}			

			f32 peak = 0.f;
			f32 sumSq = 0;
			const f32 *dataPtr = m_Pin->GetDataBuffer()->GetBuffer();
			grcTextureLock lock;
			if(!Verifyf(m_Texture->LockRect(0,0,lock,grcsWrite), "Failed to lock pin texture"))
				return;
			
			f32 *textPtrF32 = (f32*)lock.Base;
			u32 *textPtrU32 = (u32*)lock.Base;
			s32 audioSampleIncr = kMixBufNumSamples / numSamplesToRender;
			for(s32 x = 0, audioSampleIndex = 0; x < numSamplesToRender; x++, audioSampleIndex += audioSampleIncr)
			{
				const f32 sample = dataPtr[audioSampleIndex];
				f32 normSample;
				f32 signalSample;
				if(m_Pin->GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
				{
					normSample = sample;
					signalSample = (sample*2.f)-1.f;
					if(normSample > peak)
					{
						peak = normSample;
					}
				}
				else
				{
					signalSample = sample;
					normSample = (sample+1.f)*0.5f;
					if(Abs(signalSample) > Abs(peak))
					{
						peak = signalSample;
					}
				}
							
				sumSq += signalSample*signalSample;
			
				if(supportsF32Textures)
				{
					textPtrF32[x] = normSample;
				}
				else
				{
					textPtrU32[x] = Color32(normSample,0.f,0.f,1.f).GetDeviceColor();
				}
			}

			m_Texture->UnlockRect(lock);
			if(Abs(peak) > Abs(m_PeakValue*0.9f))
			{
				m_PeakValue = peak;
			}
			else
			{
				// old peaks decay
				m_PeakValue *= 0.9f;
			}
			
			m_RMSValue = Sqrtf(sumSq/(f32)numSamplesToRender);

		}

		synthUIModulePlane *activePlane = &synthSynthesizerView::Get()->GetModulePlane();

		m_FramesSinceStaticValueChanged++;
		if(m_Pin->GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_Scrollbar.SetAlpha(GetAlpha());
			m_Scrollbar.SetShouldDraw(ShouldDraw());
			m_Scrollbar.SetTextAlpha(GetHoverFade());
			m_Scrollbar.SetUnderMouse(IsUnderMouse());
			m_Scrollbar.SetDrawReflection(g_DrawPinReflections);
			m_Scrollbar.SetTitle(m_Pin->GetName());
			m_Scrollbar.SetDrawTitle(false);
	
			m_Scrollbar.SetHighlightColour(m_Pin->GetEffectiveDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED ? g_NormalizedPinHighlight : g_SignalPinHighlight);
			
			m_Scrollbar.SetRange(m_MinStaticValue,m_MaxStaticValue);
			
			if(!m_OverridenDisplayRange)
			{
				if(m_Pin->GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
				{
					m_MinStaticValue = -1.f;
					m_MaxStaticValue = 1.f;
				}
				else
				{
					m_MinStaticValue = 0.f;
					m_MaxStaticValue = 1.f;
				}
			}
			if(m_Pin->IsOutput() || m_Pin->IsConnected())
			{
				m_Scrollbar.SetNormalizedValue(m_Pin->GetNormalizedValue(0));
			}
			else
			{
				if(m_Pin->GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
				{
					const f32 val = m_Scrollbar.GetNormalizedValue();
					m_Pin->SetStaticValue(val);				
				}
				else
				{
					m_Pin->SetStaticValue(m_Scrollbar.GetValue());
				}
			}
						
			m_PeakValue = m_RMSValue = m_Pin->GetStaticValue();
			m_TargetScale = 0.f;

			if(m_Pin->GetStaticValue() != m_LastStaticValue)
			{
				m_FramesSinceStaticValueChanged = 0;
			}
			m_LastStaticValue = m_Pin->GetStaticValue();
		}
		else if(IsUnderMouse() && !IsBeingDragged())
		{
			m_TargetScale = 1.f;


			const f32 xDelta = 1.09f;
			const f32 yDelta = 0.09f;
			if(ioKeyboard::KeyDown(KEY_ALT))
			{
				if(ioKeyboard::KeyDown(KEY_RIGHT))
				{
					m_WaveXOffset += 0.009f;
				}
				else if(ioKeyboard::KeyDown(KEY_LEFT))
				{
					m_WaveXOffset -= 0.009f;
				}
			}
			else
			{	
				if(ioKeyboard::KeyDown(KEY_RIGHT))
				{
					m_WaveXZoom = Clamp(m_WaveXZoom*xDelta,1.f,5000.f);
				}
				else if(ioKeyboard::KeyDown(KEY_LEFT))
				{
					m_WaveXZoom = Clamp(m_WaveXZoom*(1.f/xDelta),1.f,5000.f);
				}
			}
			if(ioKeyboard::KeyDown(KEY_UP))
			{
				m_WaveYZoom = Clamp(m_WaveYZoom+yDelta,0.1f,10.f);
			}
			else if(ioKeyboard::KeyDown(KEY_DOWN))
			{
				m_WaveYZoom = Clamp(m_WaveYZoom-yDelta,0.1f,10.f);
			}

			const f32 offsetLimit = (1.f/m_WaveXZoom) * (m_WaveXZoom-1.f);
			m_WaveXOffset = Clamp(m_WaveXOffset,0.f,offsetLimit);
		}
		else if(activePlane->GetAuditioningPin() == m_Pin)
		{
			m_TargetScale = 1.f;
		}
		else
		{
			m_TargetScale = 0.f;
			m_WaveXZoom = 1.f;
			m_WaveYZoom = 0.9f;
			m_WaveXOffset = 0.f;
			m_Animate = true;
		}

		// also zoom to show other end of a connection
		bool areAnyConnectedPinsZoomed = false;
		if(m_Pin->IsConnected() && m_Pin->GetOtherPin()->GetDataState()==synthPin::SYNTH_PIN_DYNAMIC && m_Pin->GetOtherPin()->GetView()->IsUnderMouse())
		{
			areAnyConnectedPinsZoomed = true;
		}
		else if(m_Pin->IsOutput())
		{
			// check all destinations
			for(atArray<synthPin*>::const_iterator iter = m_OutputDestinationPins.begin(); iter != m_OutputDestinationPins.end(); iter++)
			{
				if((*iter)->GetDataState() == synthPin::SYNTH_PIN_DYNAMIC && 
					(*iter)->GetView()->IsUnderMouse() && 
					!(*iter)->GetView()->IsZoomingDisabled())
				{
					areAnyConnectedPinsZoomed = true;
					break;
				}
			}
		}
		if(areAnyConnectedPinsZoomed)
		{
			m_TargetScale = 0.5f;
		}

		m_ShouldBeHighlighted = (IsUnderMouse()||IsBeingDragged()||areAnyConnectedPinsZoomed);

		if(m_DisableZooming)
		{
			m_TargetScale = m_OverridenTargetScale;
		}
		Approach(m_LastScale,m_TargetScale,9.f,TIME.GetElapsedTime()-TIME.GetPrevElapsedTime());

		Matrix34 labelMtx = GetMatrix();
		
		m_Label.SetMatrix(labelMtx);
		const f32 labelHeight = m_Label.ComputeHeight();
		const f32 pinHeight = ComputeHeight();
		// position slightly in front of the pin view to prevent z-fighting
		labelMtx.d -= GetMatrix().c * 0.1f;
		labelMtx.d += GetMatrix().b * (pinHeight*0.5f + labelHeight*0.5f);		

		m_Label.SetSize(0.2f);
		m_Label.SetMatrix(labelMtx);
		//formatf(m_Title, sizeof(m_Title), "%s:%u", m_Pin->GetName(), m_Pin->GetNumConnections());
		m_Label.SetString(m_Pin->GetName());
		m_Label.SetUnderMouse(IsUnderMouse());
		m_Label.SetAlpha(GetAlpha());
		m_Label.SetShouldDraw(m_ShouldDrawTitle && ShouldDraw());

		//m_ValueLabel.SetSize(0.2f * Min(m_LastScale,2.f));
		labelMtx.d = GetMatrix().d - GetMatrix().c * 0.1f;
		m_Scrollbar.SetMatrix(labelMtx);

		m_Scrollbar.SetReadOnly(m_Pin->IsOutput() || m_Pin->IsConnected() || m_Pin->GetDataState()==synthPin::SYNTH_PIN_DYNAMIC);

		labelMtx = m_Scrollbar.GetMatrix();

		m_PeakLabel.SetSize(0.1f);
		m_PeakLabel.SetMatrix(labelMtx);
		labelMtx.d -= (ComputeHeight()* 0.5f + m_PeakLabel.ComputeHeight()) * labelMtx.b;
		labelMtx.d -= labelMtx.c*0.15f;
		m_PeakLabel.SetMatrix(labelMtx);

		m_PeakLabel.SetAlpha(GetHoverFade() * GetAlpha());
		m_PeakLabel.SetShouldDraw(ShouldDraw() && !m_DrawFilled && (m_Pin->IsConnected() || m_Pin->IsOutput()));
		formatf(m_PeakLabelText, "[%g, %g]",m_HeldPeakMinValue,m_HeldPeakMaxValue);
		m_PeakLabel.SetString(m_PeakLabelText);

		if(activePlane)
		{
			bool auditioning = false;
			if(IsUnderMouse())
			{
				if(ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT)
				{
					if(activePlane->IsConnecting())
					{
						if(!m_Pin->IsConnected())
						{
							activePlane->EndConnection(m_Pin);
						}
					}
					else if(!m_DisableConnections)
					{
						// it doesn't matter if this pin is already connected; we can have one to many
						if(m_Pin->IsOutput())
						{
							if(activePlane->IsDuplicateKeyDown())
							{
								activePlane->CreateJunctionFromOutput(m_Pin);
							}
							else
							{
								activePlane->StartConnection(m_Pin);
							}
						}
						else if(activePlane->IsDuplicateKeyDown())
						{

							if(m_Pin->IsConnected() && m_Pin->GetParentModule()->GetTypeId() != SYNTH_JUNCTIONPIN)
							{
								activePlane->CreateJunctionFromInput(m_Pin);
							}
						}
					}
				}
				else if(ioMouse::GetPressedButtons()&ioMouse::MOUSE_RIGHT &&  !synthUIPlane::GetMaster()->IsGroupKeyDown())
				{
					if(m_Pin->IsConnected())
					{
						activePlane->Disconnect(m_Pin);
					}
					else
					{
						// right click again to drop a cable
						activePlane->CancelConnection();
					}
				}

				// pin auditioning
				if((activePlane->IsCapsLockOn() || ioKeyboard::KeyDown(KEY_SHIFT)) && m_Pin->GetDataState() == synthPin::SYNTH_PIN_DYNAMIC &&
					m_Pin->GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
				{
					if(!activePlane->IsAuditioning())
					{
						activePlane->SetAuditioningPin(m_Pin);
					}
					auditioning = true;
				}
			}
			if(!auditioning && activePlane->GetAuditioningPin() == m_Pin)
			{
				if(!activePlane->IsCapsLockOn())
				{
					activePlane->SetAuditioningPin(NULL);
				}
			}

		}//activePlane

		m_Scrollbar.SetDrawReflection(m_ShouldDrawReflection);
		if(m_Pin->GetDataState() == synthPin::SYNTH_PIN_STATIC || m_FramesSinceStaticValueChanged < kNumFramesToHoldValueDisplay)
		{
			m_Scrollbar.SetShouldDraw(ShouldDraw());
		}
		else
		{
			m_Scrollbar.SetShouldDraw(false);
		}
	}

	void synthPinView::PostSynthesize()
	{
		if(m_Pin->GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			Vector_4V *ptr = (Vector_4V*)m_Pin->GetDataBuffer()->GetBuffer();
			Vector_4V maxVal = V4VConstant(V_ZERO);
			Vector_4V minVal = V4VConstant(V_FLT_LARGE_8);

			for(u32 i = 0; i < m_Pin->GetDataBuffer()->GetSize(); i+=4)
			{
				maxVal = V4Max(maxVal, *ptr);
				minVal = V4Min(minVal, *ptr);
				ptr++;
			}
			const Vector_4V maxXY = V4Max(maxVal, V4Permute<Y,Y,Y,Y>(maxVal));
			const Vector_4V maxZW = V4Max(V4Permute<Z,Z,Z,Z>(maxVal), V4Permute<W,W,W,W>(maxVal));

			const Vector_4V minXY = V4Min(minVal, V4Permute<Y,Y,Y,Y>(minVal));
			const Vector_4V minZW = V4Min(V4Permute<Z,Z,Z,Z>(minVal), V4Permute<W,W,W,W>(minVal));

			m_HeldPeakMaxValue = GetX(V4Max(maxXY,maxZW));
			m_HeldPeakMinValue = GetX(V4Min(minXY,minZW));
		}
		else
		{
			m_HeldPeakMaxValue = Max(m_HeldPeakMaxValue, m_Pin->GetStaticValue());
			m_HeldPeakMinValue = Min(m_HeldPeakMinValue, m_Pin->GetStaticValue());
		}
	}

	void synthPinView::ResetPeaks()
	{
		if(m_Pin->GetDataState() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
		{
			const f32 val = m_Pin->GetNormalizedValue(0);
			m_HeldPeakMaxValue = m_HeldPeakMinValue = val;
		}
		else
		{
			const f32 val = m_Pin->GetSignalValue(0);
			m_HeldPeakMaxValue = m_HeldPeakMinValue = val;
		}
	}

	void synthPinView::OnConnection(synthPin *otherPin)
	{
		if(m_Pin->IsOutput())
		{
			Assert(m_OutputDestinationPins.Find(otherPin)==-1);
			m_OutputDestinationPins.PushAndGrow(otherPin);
		}
	}

	void synthPinView::OnDisconnection(synthPin *otherPin)
	{
		if(m_Pin->IsOutput())
		{
			Assert(m_OutputDestinationPins.Find(otherPin)>=0);
			m_OutputDestinationPins.DeleteMatches(otherPin);
		}
	}
	
	extern Color32 g_ModuleEdgeColourNormal;
	void synthPinView::Render()
	{
		// render the wave form to our custom rendertarget
		const u32 rtIndex = m_DrawFilled ? m_RTIndex : 0;
		if(m_Pin->GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			synthSynthesizerView::UnlockRenderTarget();

			
			
			if(m_DrawFilled && !m_TrailRenderTargets[0])
			{
				// create extra render target for trails
				const u32 rtSize = m_DrawLowLOD ? 32 : kMixBufNumSamples;
				m_TrailRenderTargets[0] = grcTextureFactory::GetInstance().CreateRenderTarget("__pinview_t_tgt1",
					grcrtPermanent, rtSize, rtSize, 32);
				grcTextureFactory::GetInstance().LockRenderTarget(0, m_TrailRenderTargets[0], sm_SharedDepthRT);
				GRCDEVICE.Clear(true, Color32(0,0,0),true, 1.f,false,0);
				grcTextureFactory::GetInstance().UnlockRenderTarget(0);

				m_TrailRenderTargets[1] = grcTextureFactory::GetInstance().CreateRenderTarget("__pinview_t_tgt2",
					grcrtPermanent, rtSize, rtSize, 32);
				grcTextureFactory::GetInstance().LockRenderTarget(0, m_TrailRenderTargets[1], sm_SharedDepthRT);
				GRCDEVICE.Clear(true, Color32(0,0,0),true, 1.f,false,0);
				grcTextureFactory::GetInstance().UnlockRenderTarget(0);
			}
			
			grcTextureFactory::GetInstance().LockRenderTarget(0, 
				m_DrawFilled ? m_TrailRenderTargets[rtIndex] : sm_SharedRenderTarget,
				sm_SharedDepthRT);

			GRCDEVICE.Clear(true, Color32(0,0,0),true, 1.f,false,0);

			const u32 otherRTIndex = m_DrawFilled ? (m_RTIndex+1)%2 : 0;
			Matrix34 mtx(M34_IDENTITY);
			mtx.d.y = 0.f;
			mtx.d.x = 0.f;
			mtx.d.z = 1.f;
			Mat44V oldProj(grcViewport::GetCurrent()->GetProjection());
			Mat34V oldCam(grcViewport::GetCurrent()->GetCameraMtx());
			grcViewport::GetCurrent()->SetCameraMtx(RCC_MAT34V(mtx));
			
			const f32 oldNearClip = grcViewport::GetCurrent()->GetNearClip();
			const f32 oldFarClip = grcViewport::GetCurrent()->GetFarClip();
			grcViewport::GetCurrent()->Ortho(-1.f,1.f,-1.f,1.f,0.f,1.f);
			grcWorldIdentity();
			
			SetShaderVar("FontTex",m_TrailRenderTargets[otherRTIndex]);
			SetShaderVar("DiffuseTex",m_Texture);
			SetShaderVar("g_WaveXZoom", m_WaveXZoom);
			SetShaderVar("g_WaveYZoom", m_WaveYZoom*m_DefaultYZoom);
			SetShaderVar("g_WaveXOffset", m_WaveXOffset);
			SetShaderVar("g_GhostLevel", m_DrawFilled ? 0.8f : 0.f);
			SetShaderVar("g_TextColour", !m_DrawFilled && m_Pin->GetEffectiveDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED ? g_NormalizedPinHighlight : g_SignalPinHighlight);
			
			DrawVB(m_DrawFilled ? "drawspectrum" : "drawwave");

			grcViewport::GetCurrent()->SetCameraMtx(oldCam);
			grcViewport::GetCurrent()->SetProjection(oldProj);
			grcViewport::GetCurrent()->SetNearFarClip(oldNearClip, oldFarClip);

			grcTextureFactory::GetInstance().UnlockRenderTarget(0);

			synthSynthesizerView::LockRenderTarget();
		}
		
		
		Matrix34 mat = GetMatrix();
	
		const Vector3 scalingVec(1.f + m_LastScale*sm_ZoomScale*1.5f,
			1.f + m_LastScale*sm_ZoomScale,
			1.f + m_LastScale*sm_ZoomScale);
		mat.Scale(scalingVec);
	
		// also move closer to the camera based on scale
		Matrix34 cameraMtx = RCC_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());
		mat.d += 0.2f * m_LastScale * cameraMtx.c;
		grcWorldMtx(mat);

		if(m_Pin->GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			SetShaderVar("FontTex",m_DrawFilled ? m_TrailRenderTargets[rtIndex] : sm_SharedRenderTarget);
			
			SetShaderVar("g_HighlightScale", m_ShouldBeHighlighted ? 1.f : 0.f);
			SetShaderVar("g_AlphaFade", m_Alpha);
			SetShaderVar("g_EdgeHighlightColour", g_ModuleEdgeColourNormal);
			DrawVB("drawtexturedquad_mirrored");
			
			if(g_DrawPinReflections && m_ShouldDrawReflection)
			{
				mat.d -= ComputeHeight() * mat.b * 1.f / (1.f + m_LastScale*sm_ZoomScale);
				grcWorldMtx(mat);
				DrawVB("drawtexturedrefl");
			}
				
			m_RTIndex = (m_RTIndex+1)%2;
		}
	
		synthUIView::Render();		
	}
}
#endif // __SYNTH_EDITOR
