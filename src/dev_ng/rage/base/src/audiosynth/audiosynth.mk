PROJECT = audiosynth

INCLUDES = -I ..\..\..\..\rage\stlport\STLport-5.0RC5\stlport -I .. -I ..\..\..\3rdparty\freetype-2.3.7\include
DEFINES = \

OBJS = \
	$(INTDIR)/synthesizer.obj	\
	$(INTDIR)/pin.obj	\
	$(INTDIR)/moduleserializer.obj	\
	$(INTDIR)/modulegroup.obj	\
	$(INTDIR)/modulefactory.obj	\
	$(INTDIR)/module.obj	\
	$(INTDIR)/sampleframe.obj	\
	$(INTDIR)/audiooutput.obj	\
	$(INTDIR)/subtracter.obj	\
	$(INTDIR)/multiplier.obj	\
	$(INTDIR)/mixer.obj	\
	$(INTDIR)/divider.obj	\
	$(INTDIR)/adder.obj	\
	$(INTDIR)/rescaler.obj	\
	$(INTDIR)/rectifier.obj	\
	$(INTDIR)/inverter.obj	\
	$(INTDIR)/inputsmoother.obj	\
	$(INTDIR)/hardknee.obj	\
	$(INTDIR)/gate.obj	\
	$(INTDIR)/fftmodule.obj	\
	$(INTDIR)/exp.obj	\
	$(INTDIR)/delayline.obj	\
	$(INTDIR)/clipper.obj	\
	$(INTDIR)/randomizer.obj	\
	$(INTDIR)/pwmnoise.obj	\
	$(INTDIR)/oscillator.obj	\
	$(INTDIR)/noisegenerator.obj	\
	$(INTDIR)/modalsynth.obj	\
	$(INTDIR)/dsfoscillator.obj	\
	$(INTDIR)/envelopegenerator.obj	\
	$(INTDIR)/anoscillator.obj	\
	$(INTDIR)/kiss_fftr.obj	\
	$(INTDIR)/kiss_fft.obj	\
	$(INTDIR)/statevariable.obj	\
	$(INTDIR)/pinkingfilter.obj	\
	$(INTDIR)/biquadfilter.obj	\
	$(INTDIR)/1polelpf.obj	\
	$(INTDIR)/uiscrollbar.obj	\
	$(INTDIR)/uirangeview.obj	\
	$(INTDIR)/uiplane.obj	\
	$(INTDIR)/uimenuview.obj	\
	$(INTDIR)/uimenuitem.obj	\
	$(INTDIR)/uilabel.obj	\
	$(INTDIR)/uicomponent.obj	\
	$(INTDIR)/uiview.obj	\
	$(INTDIR)/synthesizerview.obj	\
	$(INTDIR)/pinview.obj	\
	$(INTDIR)/moduleview.obj	\
	$(INTDIR)/svfilterview.obj	\
	$(INTDIR)/rescalerview.obj	\
	$(INTDIR)/randomizerview.obj	\
	$(INTDIR)/pwmnoiseview.obj	\
	$(INTDIR)/oscillatorview.obj	\
	$(INTDIR)/fftview.obj	\
	$(INTDIR)/clipperview.obj	\
	$(INTDIR)/biquadfilterview.obj	\
	$(INTDIR)/1poleview.obj	\


include ..\..\..\..\rage\build\Makefile.template

HEADERS += $(abspath embedded_waveform_$(SHADERDIR).h)

$(abspath embedded_waveform_$(SHADERDIR).h): ./waveform.fx
	echo Compiling embedded shader $< . . .
	..\..\..\..\rage\base\bin\makedep -I .. -I ..\..\..\3rdparty\freetype-2.3.7\include embedded_waveform_$(SHADERDIR).h ./waveform.fx
	call ..\..\..\..\rage\base\bin\makeembeddedshader.bat -platform $(SHADERDIR) $<

$(INTDIR)/synthesizer.obj: ./synthesizer.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/pin.obj: ./pin.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/moduleserializer.obj: ./moduleserializer.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/modulegroup.obj: ./modulegroup.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/modulefactory.obj: ./modulefactory.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/module.obj: ./module.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/sampleframe.obj: ./sampleframe.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/audiooutput.obj: ./audiooutput.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/subtracter.obj: ./subtracter.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/multiplier.obj: ./multiplier.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/mixer.obj: ./mixer.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/divider.obj: ./divider.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/adder.obj: ./adder.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/rescaler.obj: ./rescaler.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/rectifier.obj: ./rectifier.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/inverter.obj: ./inverter.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/inputsmoother.obj: ./inputsmoother.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/hardknee.obj: ./hardknee.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/gate.obj: ./gate.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/fftmodule.obj: ./fftmodule.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/exp.obj: ./exp.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/delayline.obj: ./delayline.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/clipper.obj: ./clipper.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/randomizer.obj: ./randomizer.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/pwmnoise.obj: ./pwmnoise.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/oscillator.obj: ./oscillator.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/noisegenerator.obj: ./noisegenerator.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/modalsynth.obj: ./modalsynth.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/dsfoscillator.obj: ./dsfoscillator.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/envelopegenerator.obj: ./envelopegenerator.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/anoscillator.obj: ./anoscillator.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/kiss_fftr.obj: ./kiss_fftr.c
	echo Make C compiling $(abspath $<)
	$(CC) $(CFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/kiss_fft.obj: ./kiss_fft.c
	echo Make C compiling $(abspath $<)
	$(CC) $(CFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/statevariable.obj: ./statevariable.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/pinkingfilter.obj: ./pinkingfilter.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/biquadfilter.obj: ./biquadfilter.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/1polelpf.obj: ./1polelpf.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/uiscrollbar.obj: ./uiscrollbar.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/uirangeview.obj: ./uirangeview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/uiplane.obj: ./uiplane.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/uimenuview.obj: ./uimenuview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/uimenuitem.obj: ./uimenuitem.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/uilabel.obj: ./uilabel.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/uicomponent.obj: ./uicomponent.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/uiview.obj: ./uiview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/synthesizerview.obj: ./synthesizerview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/pinview.obj: ./pinview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/moduleview.obj: ./moduleview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/svfilterview.obj: ./svfilterview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/rescalerview.obj: ./rescalerview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/randomizerview.obj: ./randomizerview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/pwmnoiseview.obj: ./pwmnoiseview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/oscillatorview.obj: ./oscillatorview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/fftview.obj: ./fftview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/clipperview.obj: ./clipperview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/biquadfilterview.obj: ./biquadfilterview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/1poleview.obj: ./1poleview.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

HEADERS: $(HEADERS)
