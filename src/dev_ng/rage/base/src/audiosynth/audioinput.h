

#ifndef SYNTH_AUDIOINPUT_H
#define SYNTH_AUDIOINPUT_H

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthAudioInput : public synthModuleBase<0,1,SYNTH_AUDIOINPUT>
	{
	public:
		SYNTH_MODULE_NAME("Audio In");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthAudioInput();

		virtual void Synthesize();

		synthSampleFrame &GetBuffer()
		{
			return m_Buffer;
		}

		virtual void SerializeState(synthModuleSerializer *serializer);	

		void SetInputId(const u32 bufferId)
		{
			m_InputId = bufferId;
		}

		u32 GetInputId() const { return m_InputId; }
		
		void SetBufferId(const u32 bufferId)
		{
			m_BufferId = bufferId;
		}
	private:

		synthSampleFrame m_Buffer;
		u32 m_InputId;
		u32 m_BufferId;
	};
}

#endif // SYNTH_AUDIOINPUT_H


