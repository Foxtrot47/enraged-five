// 
// synthcoredisasm.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
//

#ifndef AUD_SYNTHCOREDISASM_H
#define AUD_SYNTHCOREDISASM_H

#if __BANK

#include "atl/array.h"
#include "synthobjects.h"
#include "synthopcodes.h"

namespace rage
{

class synthCoreDisasm
{
public:
	

	struct DecodedOp
	{
		struct Operand
		{
			u32 Type;
			u32 Id;
		};

		u32 Offset;
		u32 SizeBytes;
		const u8 *DataPtr;
		u32 Opcode;
		const char *OpcodeName;
		atFixedArray<Operand, 16> Inputs;
		atFixedArray<Operand, 16> Outputs;
		atFixedArray<s32, 16> StateBlocks;
	};

	static void InitClass();

	bool Disassemble(const CompiledSynth *synth);

	atArray<DecodedOp> &GetDisassembly();

	u32 GetNumConstants() const { return m_NumConstants; }
	u32 GetNumBuffers() const { return m_NumBuffers; }
	u32 GetNumStateBlocks() const { return m_NumStateBlocks; }
	u32 GetNumRegisters() const { return m_NumRegisters; }
	u32 GetNumOutputs() const { return m_NumOutputs; }

	u32 GetTotalSizeBytes() const { return m_TotalSynthSizeBytes; }

	void ComputeRuntimeEstimate(float &ps3, float &xbox360) const;
	
	void DebugPrint();
	
	static bool IsReplacing(const u32 opcode);
	static const char *GetOpcodeName(const u32 opcode);
	char *FormatOperand(const DecodedOp::Operand &operand, char *buf, const size_t bufSize);

#if __ASSERT
	int SynthAssertFailed(const char *msg, const char *exp, const char *file, const int line) const;
#endif

private:
	float ReadConstantValue(const u32 constantId);
	bool DisassembleProgram(const u16 *programEntry, const u32 programSizeBytes);

	atArray<DecodedOp> m_Disassembly;

	struct InputVariable
	{
		u32 NameHash;
		float Value;
	};
	atFixedArray<InputVariable, 32> m_InputVariables;

	const u16 *m_ProgramEntry;
	const float *m_Constants;
	u32 m_ProgramSizeBytes;
	u32 m_NumConstants;
	u32 m_NumBuffers;
	u32 m_NumStateBlocks;
	u32 m_NumRegisters;
	u32 m_NumOutputs;
	u32 m_Cost;

	u32 m_TotalSynthSizeBytes;
	const char *m_SynthName;

	bool m_ShouldResolveConstantValues;
	bool m_ShouldPrintProgramBytes;

	struct synthCoreTimings
	{
		float PS3_Time;
		float Xbox360_Time;
	};
	static atRangeArray<synthCoreTimings, synthOpcodes::NUM_OP_CODES> sm_OpCodeTimings;
};

} // namespace rage
#endif // __BANK
#endif // AUD_SYNTHCOREDISASM_H
