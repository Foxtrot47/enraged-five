// 
// audiosynth/denormcancelview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_PHASERVIEW_H
#define SYNTH_PHASERVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthPhaser;
	class synthPhaserView : public synthModuleView
	{
	public:

		synthPhaserView(synthModule *module);

		virtual void Update();
		virtual bool StartDrag() const;
		virtual bool IsActive() const ;

	protected:
		virtual void Render();
		synthPhaser *GetPhaser() const { return (synthPhaser*)m_Module; }

	private:
		synthUIDropDownList m_FreqNoteSwitch;
		synthUIDropDownList m_TapSwitch;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_PHASERVIEW_H

