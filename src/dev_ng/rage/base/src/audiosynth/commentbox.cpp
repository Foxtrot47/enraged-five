// 
// audiosynth/commentbox.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "commentbox.h"

#if __SYNTH_EDITOR

namespace rage {

	synthCommentBox::synthCommentBox() : m_Color(0.2f,0.f,0.3f,1.f)
	{
		AddComponent(&m_TextBox);	
	}

	synthCommentBox::~synthCommentBox()
	{

	}

	void synthCommentBox::GetBounds(rage::Vector3 &v1, rage::Vector3 &v2) const
	{
		m_TextBox.GetBounds(v1,v2);
		// add a small border/title
		v2 += GetMatrix().b*0.2f + GetMatrix().a*0.1f;
		v1 -= GetMatrix().b*0.1f + GetMatrix().a*0.15f;
	}

	void synthCommentBox::Update()
	{
		m_TextBox.SetMatrix(GetMatrix());
		synthUIView::Update();	
	}

	void synthCommentBox::Render()
	{
		// draw background box
		Vector3 v1,v2;
		GetBounds(v1,v2);
		const Vector3 m = (v1+v2)*0.5f;
		Matrix34 bm = GetMatrix();
		bm.d = m;
		const f32 height = bm.b.Dot(v2-v1);
		const f32 width = bm.a.Dot(v2-v1);
		bm.Scale(width * 0.5f, height * 0.5f, 1.f);
		bm.d += bm.c * 0.1f;
		

		grcWorldMtx(bm);
		SetShaderVar("g_TextColour", m_Color);
		SetShaderVar("g_CurrentValue", IsUnderMouse() ? 0.8f : 0.24f);
		SetShaderVar("g_AlphaFade", GetAlpha());
		DrawVB("drawgroup");

		// draw text box
		synthUIView::Render();
	}

	bool synthCommentBox::StartDrag() const
	{
		if(m_TextBox.IsEditing())
		{
			return false;
		}
		return true;
	}

} // namespace rage

#endif // __SYNTH_EDITOR

