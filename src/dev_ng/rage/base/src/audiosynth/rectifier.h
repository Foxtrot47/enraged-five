

#ifndef SYNTH_RECTIFIER_H
#define SYNTH_RECTIFIER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthRectifier : public synthModuleBase<5, 1, SYNTH_RECTIFIER>
	{
	public:
		SYNTH_MODULE_NAME("Rectifier");

		synthRectifier();
		virtual void Synthesize();

		enum synthRectifierInputs
		{
			SIGNAL = 0,
			PRE_DC_OFFSET,
			POSITIVE_GAIN,
			NEGATIVE_GAIN,
			POST_DC_OFFSET,
			NUM_RECTIFIER_INPUTS
		};

		CompileTimeAssert((s32)NUM_RECTIFIER_INPUTS == (s32)NumInputs);

	private:
		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_RECTIFIER_H


