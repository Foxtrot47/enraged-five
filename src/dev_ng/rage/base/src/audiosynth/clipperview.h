// 
// audiosynth/clipperview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_CLIPPERVIEW_H
#define SYNTH_CLIPPERVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uidropdownlist.h"

namespace rage
{
	class grcTexture;
	class synthClipper;
	class synthClipperView : public synthModuleView
	{
	public:

		synthClipperView(synthModule *module);
		~synthClipperView();

		virtual void Update();
		virtual void Render();

	protected:

		virtual bool StartDrag() const;
		virtual u32 GetModuleHeight() const { return 3; }

	private:

		synthClipper *GetClipper() const { return (synthClipper*)m_Module; }
		synthUIDropDownList m_Mode;
		grcTexture *m_Texture;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_CLIPPERVIEW_H

