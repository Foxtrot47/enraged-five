
#include "module.h"
#include "moduleview.h"
#include "pin.h"

namespace rage
{
#if __SYNTH_EDITOR
	synthModuleView *synthModule::AllocateView()
	{
		return rage_new synthModuleView(this);
	}
#endif

	void synthModule::Serialize(synthModuleSerializer *serializer)
	{
		synthPin *pins;
		u32 numPins = 0;

		serializer->StartFields();
			SerializeState(serializer);
		serializer->FinishedFields();

		serializer->StartInputs();
			GetInputs(pins,numPins);
			for(u32 i = 0; i < numPins; i++)
			{
				serializer->SerializePin(&pins[i]);
			}
		serializer->FinishedInputs();

		serializer->StartOutputs();
			GetOutputs(pins,numPins);
			for(u32 i = 0; i < numPins; i++)
			{
				serializer->SerializePin(&pins[i]);
			}
		serializer->FinishedOutputs();
	}

	u32 synthModule::ComputeNumReusableInputBuffers()
	{
		synthPin *pins;
		u32 numPins;
		GetInputs(pins, numPins);

		u32 numReusableBuffers = 0;
		for(u32 i = 0; i < numPins; i++)
		{
			if(pins[i].GetOtherPin() && pins[i].GetOtherPin()->GetNumConnections() == 1)
			{
				numReusableBuffers++;
			}
		}

		return numReusableBuffers;
	}

	u32 synthModule::ComputeNumOutputBuffersRequired()
	{
		synthPin *pins;
		u32 numPins;
		GetInputs(pins, numPins);

		for(u32 i = 0; i < numPins; i++)
		{
			if(pins[i].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				return 1;
			}
		}
		return 0;
	}
}
