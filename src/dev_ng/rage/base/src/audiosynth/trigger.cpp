#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "trigger.h"
#include "triggerview.h"
#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	SYNTH_EDITOR_VIEW(synthTrigger);

	synthTrigger::synthTrigger() 
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Inputs[kTriggerInput].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kTriggerInput].SetStaticValue(0.f);
		m_Inputs[kTriggerThreshold].Init(this, "Threshold", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kTriggerThreshold].SetStaticValue(1.f);

		ResetProcessing();
		
		m_Mode = Latching;

		SYNTH_EDITOR_ONLY(m_TriggerRequested = false);
	}

	void synthTrigger::ResetProcessing()
	{
		m_Outputs[0].SetStaticValue(0.f);
		m_PrevValue = 0.f;
		m_WasTriggerHigh = false;
	}

	void synthTrigger::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeEnumField("Mode", m_Mode);
	}

	void synthTrigger::Synthesize()
	{
#if __SYNTH_EDITOR
		if(m_TriggerRequested)
		{
			m_Outputs[0].SetStaticValue(1.f);
			m_TriggerRequested = false;
			return;
		}
#endif

		if(m_Mode == Latching)
		{
			if(m_Inputs[kTriggerInput].GetDataState() == synthPin::SYNTH_PIN_STATIC)
			{
				const bool isTriggerHigh = Abs(m_Inputs[kTriggerInput].GetStaticValue()) >= (m_Inputs[kTriggerThreshold].GetNormalizedValue(0) - SMALL_FLOAT);
				
				if(!m_WasTriggerHigh && isTriggerHigh)
				{
					m_Outputs[0].SetStaticValue(1.f);
				}
				else
				{
					m_Outputs[0].SetStaticValue(0.f);
				}
				m_WasTriggerHigh = isTriggerHigh;
			}
			else
			{
				// dynamic input
				Vector_4V runningMask = V4VConstant(V_ZERO);
				for(u32 i = 0; i < m_Inputs[kTriggerInput].GetDataBuffer()->GetSize(); i+= 4)
				{
					const Vector_4V input = m_Inputs[kTriggerInput].GetNormalizedValueV(i);
					const Vector_4V threshold = m_Inputs[kTriggerThreshold].GetNormalizedValueV(i);
					Vector_4V mask = V4IsGreaterThanOrEqualV(input,threshold);
					runningMask = V4Or(runningMask, mask);
				}

				Vector_4V splattedMask = V4Or(V4Or(V4SplatX(runningMask),V4SplatY(runningMask)),V4Or(V4SplatZ(runningMask),V4SplatW(runningMask)));
				const bool isTriggerHigh = !(GetXi(splattedMask)==0);

				if((m_Mode != Latching || !m_WasTriggerHigh) && isTriggerHigh)
				{
					m_Outputs[0].SetStaticValue(1.f);
				}
				else
				{
					m_Outputs[0].SetStaticValue(0.f);
				}
				m_WasTriggerHigh = isTriggerHigh;
			}
		}
		else
		{
			// Difference mode - only supports static input
			const f32 currentVal = m_Inputs[kTriggerInput].GetNormalizedValue(0);
			const f32 triggerDelta = m_Inputs[kTriggerThreshold].GetNormalizedValue(0);
			const bool isTriggerHigh = (Abs(m_PrevValue-currentVal) >= triggerDelta);
			
			if(isTriggerHigh)
			{
				m_Outputs[0].SetStaticValue(1.f);
			}
			else
			{
				m_Outputs[0].SetStaticValue(0.f);
			}
			m_WasTriggerHigh = isTriggerHigh;
			m_PrevValue = currentVal;
		}
	}

	float synthTrigger::Process_Latch(const float *RESTRICT const inBuffer, const float *RESTRICT const inThresholdBuffer, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		const bool wasTriggerHigh = (GetXi(state) != 0);
		const Vector_4V *inPtr = reinterpret_cast<const Vector_4V*>(inBuffer);
		const Vector_4V *thresholdPtr = reinterpret_cast<const Vector_4V*>(inThresholdBuffer);

		Vector_4V runningMask = V4VConstant(V_ZERO);
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V input = *inPtr++;
			const Vector_4V threshold = *thresholdPtr++;
			Vector_4V mask = V4IsGreaterThanOrEqualV(input,V4Subtract(threshold, V4VConstant(V_FLT_EPSILON)));
			runningMask = V4Or(runningMask, mask);
		}

		Vector_4V splattedMask = V4Or(V4Or(V4SplatX(runningMask),V4SplatY(runningMask)),V4Or(V4SplatZ(runningMask),V4SplatW(runningMask)));
		const bool isTriggerHigh = !(GetXi(splattedMask)==0);

		float ret = 0.f;
		// In latch mode we only trigger when the state changes; ie don't continually trigger
		if(isTriggerHigh && !wasTriggerHigh)
		{
			ret = 1.f;
		}
		if(isTriggerHigh != wasTriggerHigh)
		{
			SetX(state, isTriggerHigh ? 1.f : 0.f);
		}

		return ret;
	}

	float synthTrigger::Process_Latch(const float *RESTRICT const inBuffer, Vector_4V_In threshold, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		const Vector_4V *inPtr = reinterpret_cast<const Vector_4V*>(inBuffer);
		
		Vector_4V runningMask = V4VConstant(V_ZERO);
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V input = *inPtr++;
			Vector_4V mask = V4IsGreaterThanOrEqualV(input,V4Subtract(threshold, V4VConstant(V_FLT_EPSILON)));
			runningMask = V4Or(runningMask, mask);
		}

		Vector_4V splattedMask = V4Or(V4Or(V4SplatX(runningMask),V4SplatY(runningMask)),V4Or(V4SplatZ(runningMask),V4SplatW(runningMask)));
		const bool isTriggerHigh = !(GetXi(splattedMask)==0);

		return Process_Latch(isTriggerHigh, state);
	}

	float synthTrigger::Process_Latch(const float input, const float threshold, Vec::Vector_4V_InOut state)
	{
		const bool isTriggerHigh = input >= (threshold - SMALL_FLOAT);
		return Process_Latch(isTriggerHigh, state);
	}

	float synthTrigger::Process_Latch(const bool isTriggerHigh, Vector_4V_InOut state)
	{
		const bool wasTriggerHigh = (GetXi(state) != 0);

		float ret = 0.f;
		// In latch mode we only trigger when the state changes; ie don't continually trigger
		if(isTriggerHigh && !wasTriggerHigh)
		{
			ret = 1.f;
		}
		if(isTriggerHigh != wasTriggerHigh)
		{
			SetX(state, isTriggerHigh ? 1.f : 0.f);
		}

		return ret;
	}

	float synthTrigger::Process_Diff(const float input, const float threshold, Vec::Vector_4V_InOut state)
	{
		float previousValue = GetY(state);
		const bool isTriggerHigh = (Abs(previousValue-input) >= threshold - SMALL_FLOAT);

		SetY(state, input);
		return isTriggerHigh ? 1.f : 0.f;
	}
}

#endif
