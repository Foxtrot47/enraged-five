#ifndef SYNTH_NOTCHFILTER_H
#define SYNTH_NOTCHFILTER_H

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum synthNotchFilterInputs
	{
		kNotchSignal = 0,
		kNotchGain,
		kNotchCutoff,
		kNotchQ,
		kNotchNumInputs
	};

	class synthNotchFilter : public synthModuleBase<kNotchNumInputs,1, SYNTH_NOTCH>
	{
		public:
			SYNTH_CUSTOM_MODULE_VIEW;

			synthNotchFilter();
			virtual ~synthNotchFilter(){};
			
			virtual void SerializeState(synthModuleSerializer *serializer);
						
			virtual void Synthesize();
			
			
			void SetMinFrequency(const f32 freq)
			{
				m_MinFrequency = freq;
			}
			
			void SetMaxFrequency(const f32 freq)
			{
				m_MaxFrequency = freq;
			}
			
			f32 GetMinFrequency() const { return m_MinFrequency; }
			f32 GetMaxFrequency() const { return m_MaxFrequency; }

			virtual const char *GetName() const
			{
				return "NotchFilter";
			}
		private:
		
			synthSampleFrame m_Buffer;
			
			f32 m_MinFrequency;
			f32 m_MaxFrequency;

			f32 m_x1,m_x2;
			f32 m_y1,m_y2;
		};
}

#endif // SYNTH_NOTCHFILTER_H


