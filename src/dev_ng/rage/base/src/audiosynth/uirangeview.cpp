#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uirangeview.h"

namespace rage
{
	synthUIRangeView::synthUIRangeView() : m_Width(1.f)
	{
		AddComponent(&m_MinScrollBar);
		AddComponent(&m_MaxScrollBar);
		SetNormalizedValues(0.f,1.f);

		SetDrawTitles(true);
	}

	synthUIRangeView::~synthUIRangeView()
	{

	}

	void synthUIRangeView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		Vector3 v3,v4;
		Vector3 v5,v6;
		m_MinScrollBar.GetBounds(v3,v4);
		m_MaxScrollBar.GetBounds(v5,v6);

		v1.Min(v3,v5);
		v2.Max(v4,v6);
	}

	void synthUIRangeView::Update()
	{
		m_MinScrollBar.SetWidth(m_Width);
		m_MaxScrollBar.SetWidth(m_Width);

		Matrix34 mat = GetMatrix();
		mat.d -= mat.a * m_Width * 2.f;
		m_MaxScrollBar.SetMatrix(mat);

		mat.d += mat.a * m_Width * 4.f;
		m_MinScrollBar.SetMatrix(mat);

		synthUIView::Update();
	}

	void synthUIRangeView::Render()
	{
		if(ShouldDraw())
		{
			m_MinScrollBar.SetAlpha(GetAlpha());
			m_MaxScrollBar.SetAlpha(GetAlpha());
			m_MinScrollBar.Draw();
			m_MaxScrollBar.Draw();
		}
	}
}
#endif // __SYNTH_EDITOR
