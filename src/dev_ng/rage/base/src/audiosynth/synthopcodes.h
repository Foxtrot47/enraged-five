#ifndef AUD_SYNTHOPCODES_H
#define AUD_SYNTHOPCODES_H

#if __ASSERT
#define synthAssert(x) (void)((x) || SynthAssertFailed(NULL, #x, __FILE__,__LINE__) || (__debugbreak(),0));
#define synthVerify(cond) (Likely(cond) || (SynthAssertFailed(NULL, #cond, __FILE__,__LINE__) || (__debugbreak(),0), false))
//#define synthAssert(x) Assert(x)
#define SYNTH_ASSERT_ONLY(x) x
#else
#define synthAssert(x)
#define synthVerify(cond) Likely(cond)
#define SYNTH_ASSERT_ONLY(x)
#endif

#define encode_synth_op(Opcode,NumInputs,NumOutputs) (((Opcode&0xff) << 8) | ((NumInputs&0xf) << 4) | (NumOutputs&0xf))
#define encode_synth_ref(T,I) ((encode_synth_ref_type(T) << 8) | (I&0xff))

#define encode_synth_ref_type(T) (T == 'R' ? synthOpcodes::Register : (T == 'C' ? synthOpcodes::Constant : (T == 'V' ? synthOpcodes::Variable : synthOpcodes::Buffer)))
#define decode_synth_ref_type(R) (R>>8)
#define decode_synth_ref_id(R) (R&0xff)

#define synthValidateReferenceType(R, T) synthAssert(decode_synth_ref_type(R) == encode_synth_ref_type(T))
#define synthValidateScalarReference(R) synthAssert(decode_synth_ref_type(R) == encode_synth_ref_type('R') || decode_synth_ref_type(R) ==  encode_synth_ref_type('C'))


#define IMPLEMENT_UNARY_SCALAR(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 1); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'R'); \
	const u32 outputRegister = decode_synth_ref_id(*pc++); \
	const float input = LoadScalarReference(*pc++, registers); \
	registers[outputRegister] = func(input); \
	} \
	break;

#define IMPLEMENT_UNARY_SCALAR_WITH_STATE(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 1); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'R'); \
	const u32 outputRegister = decode_synth_ref_id(*pc++); \
	const float input = LoadScalarReference(*pc++, registers); \
	registers[outputRegister] = func(input,GetStateBlock(*pc++)); \
	} \
	break; 

#define IMPLEMENT_BINARY_SCALAR(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 2); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'R'); \
	const u32 outputReg = decode_synth_ref_id(*pc++); \
	const float in0 = LoadScalarReference(*pc++, registers); \
	const float in1 = LoadScalarReference(*pc++, registers); \
	registers[outputReg] = func(in0, in1); \
	} \
	break;

#define IMPLEMENT_BINARY_SCALAR_WITH_STATE(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 2); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'R'); \
	const u32 outputReg = decode_synth_ref_id(*pc++); \
	const float in0 = LoadScalarReference(*pc++, registers); \
	const float in1 = LoadScalarReference(*pc++, registers); \
	registers[outputReg] = func(in0, in1, GetStateBlock(*pc++)); \
	} \
	break;

#define IMPLEMENT_BINARY_SCALAR_WITH_STATE_AND_CONTEXT(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 2); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'R'); \
	const u32 outputReg = decode_synth_ref_id(*pc++); \
	const float in0 = LoadScalarReference(*pc++, registers); \
	const float in1 = LoadScalarReference(*pc++, registers); \
	registers[outputReg] = func(m_Context, in0, in1, GetStateBlock(*pc++)); \
	} \
	break;

#define IMPLEMENT_TERNARY_SCALAR(opcode, func) case opcode: \
		{ \
		synthAssert(numInputs == 3); \
		synthAssert(numOutputs == 1); \
		synthValidateReferenceType(*pc, 'R'); \
		const u32 outputReg = decode_synth_ref_id(*pc++); \
		const float a = LoadScalarReference(*pc++, registers); \
		const float b = LoadScalarReference(*pc++, registers); \
		const float c = LoadScalarReference(*pc++, registers); \
		registers[outputReg] = func(a,b,c); \
		} \
		break;


// Check the second operand; could be buffer or scalar
#define IMPLEMENT_BINARY_BUFFER_COND(opcode, func) case opcode: \
		{ \
		synthAssert(numInputs == 2); \
		synthAssert(numOutputs == 1); \
		synthValidateReferenceType(*pc, 'B'); \
		const u32 outputId = decode_synth_ref_id(*pc++); \
		if(decode_synth_ref_type(*pc) == synthOpcodes::Buffer) \
			{ \
			const u32 inputId = decode_synth_ref_id(*pc++); \
			if(buffers[outputId]) \
			func(buffers[outputId], buffers[inputId], kMixBufNumSamples); \
			} \
			else \
			{ \
			const Vector_4V vecInput = V4LoadScalar32IntoSplatted(LoadScalarReference(*pc++, registers)); \
			if(buffers[outputId]) \
			func(buffers[outputId], vecInput, kMixBufNumSamples); \
			} \
		} \
		break;

#define IMPLEMENT_BINARY_BUFFER_COND_WITH_STATE(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 2); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 outputId = decode_synth_ref_id(*pc++); \
	if(decode_synth_ref_type(*pc) == synthOpcodes::Buffer) \
		{ \
		const u32 inputId = decode_synth_ref_id(*pc++); \
		if(buffers[outputId]) \
		func(buffers[outputId], buffers[inputId], GetStateBlock(*pc++), kMixBufNumSamples); \
		} \
					else \
		{ \
		const Vector_4V vecInput = V4LoadScalar32IntoSplatted(LoadScalarReference(*pc++, registers)); \
		if(buffers[outputId]) \
		func(buffers[outputId], vecInput, GetStateBlock(*pc++), kMixBufNumSamples); \
		} \
	} \
	break;

#define IMPLEMENT_BIQUAD_COEFF_2IN(opcode, func) case opcode: \
		{ \
		synthAssert(numInputs == 2); \
		synthAssert(numOutputs == 5); \
		synthValidateReferenceType(*pc,'R'); \
		const u32 reg_a0 = decode_synth_ref_id(*pc++); \
		synthValidateReferenceType(*pc,'R'); \
		const u32 reg_a1 = decode_synth_ref_id(*pc++); \
		synthValidateReferenceType(*pc,'R'); \
		const u32 reg_a2 = decode_synth_ref_id(*pc++); \
		synthValidateReferenceType(*pc,'R'); \
		const u32 reg_b1 = decode_synth_ref_id(*pc++); \
		synthValidateReferenceType(*pc,'R'); \
		const u32 reg_b2 = decode_synth_ref_id(*pc++); \
		const float in0 = LoadScalarReference(*pc++, registers); \
		const float in1 = LoadScalarReference(*pc++, registers); \
		func(in0, \
		in1, \
		registers[reg_a0], \
		registers[reg_a1], \
		registers[reg_a2], \
		registers[reg_b1], \
		registers[reg_b2]); \
		} \
		break;


// NOTE: the opcodes implemented with the following _BUFFER macros replace their input
//		i.e. num operands = (numInputs + numOutputs) - 1
//		First operand is always the input/output buffer
#define IMPLEMENT_UNARY_BUFFER(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 1); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 inputOutputId = decode_synth_ref_id(*pc++); \
	if(buffers[inputOutputId]) \
	func(buffers[inputOutputId], kMixBufNumSamples); \
	} \
	break;

#define IMPLEMENT_UNARY_BUFFER_WITH_STATE(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 1); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 inputOutputId = decode_synth_ref_id(*pc++); \
	if(buffers[inputOutputId]) \
	func(buffers[inputOutputId], GetStateBlock(*pc++), kMixBufNumSamples); \
	} \
	break;

// 1 scalar input, 1 buffer output, with single state block (eg ramp generator)
#define IMPLEMENT_UNARY_BUFFER_SCALAR_WITH_STATE(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 1); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 outputId = decode_synth_ref_id(*pc++); \
	const Vector_4V scalarV = V4LoadScalar32IntoSplatted(LoadScalarReference(*pc++, registers)); \
	if(buffers[outputId]) \
	func(buffers[outputId], scalarV, GetStateBlock(*pc++), kMixBufNumSamples); \
	} \
	break;

#define IMPLEMENT_BINARY_BUFFER(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 2); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 inputOutputBufferId = decode_synth_ref_id(*pc++); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 input1Id = decode_synth_ref_id(*pc++); \
	if(buffers[inputOutputBufferId] && buffers[input1Id]) \
	func(buffers[inputOutputBufferId], buffers[input1Id], kMixBufNumSamples); \
	} \
	break;

#define IMPLEMENT_BINARY_BUFFER_WITH_STATE(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 2); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 inputOutputBufferId = decode_synth_ref_id(*pc++); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 input1Id = decode_synth_ref_id(*pc++); \
	if(buffers[inputOutputBufferId] && buffers[input1Id]) \
	func(buffers[inputOutputBufferId], buffers[input1Id], GetStateBlock(*pc++), kMixBufNumSamples); \
	} \
	break;

#define IMPLEMENT_BINARY_BUFFER_SCALAR(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 2); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 inputOutputId = decode_synth_ref_id(*pc++); \
	const Vector_4V scalarV = V4LoadScalar32IntoSplatted(LoadScalarReference(*pc++, registers)); \
	if(buffers[inputOutputId]) \
	func(buffers[inputOutputId], scalarV, kMixBufNumSamples); \
	} \
	break;

#define IMPLEMENT_BINARY_BUFFER_SCALAR_WITH_STATE(opcode, func) case opcode: \
	{ \
	synthAssert(numInputs == 2); \
	synthAssert(numOutputs == 1); \
	synthValidateReferenceType(*pc, 'B'); \
	const u32 inputOutputId = decode_synth_ref_id(*pc++); \
	const float scalarParam = LoadScalarReference(*pc++, registers); \
	if(buffers[inputOutputId]) \
	func(buffers[inputOutputId], scalarParam, GetStateBlock(*pc++), kMixBufNumSamples); \
	} \
	break;

#define SCALAR_NOOP(x) x
#define SCALAR_NtoS(x) ((2.f * x) - 1.f)
#define SCALAR_StoN(x) ((1.f + x) * 0.5f)
#define SCALAR_SUM(x,y) (x + y)
#define SCALAR_SUBTRACT(x,y) (x - y)
#define SCALAR_MULTIPLY(x,y) (x * y)
#define SCALAR_DIVIDE(x,y) (Selectf(Abs(y) - SMALL_FLOAT, x / y, 0.f))

namespace rage
{	
	struct synthOpcodeDecoder
	{
		u32 DECLARE_BITFIELD_3(numOutputs, 4, numInputs, 4, opcode, 8);
	};

	class synthOpcodes
	{
	public:
		enum synthOpcodeIds
		{
			COPY_BUFFER = 0,
			COPY_SCALAR,

			CONVERT_BUFFER_TO_SIGNAL,
			CONVERT_SCALAR_TO_SIGNAL,
			CONVERT_BUFFER_TO_NORMALIZED,
			CONVERT_SCALAR_TO_NORMALIZED,

			SAMPLE_BUFFER,

			MULTIPLY_BUFFER_BUFFER,
			MULTIPLY_BUFFER_SCALAR,
			MULTIPLY_SCALAR_SCALAR,

			SUM_BUFFER_BUFFER,
			SUM_BUFFER_SCALAR,
			SUM_SCALAR_SCALAR,

			SUBTRACT_BUFFER_BUFFER,
			SUBTRACT_BUFFER_SCALAR,
			SUBTRACT_SCALAR_BUFFER,
			SUBTRACT_SCALAR_SCALAR,

			DIVIDE_BUFFER_BUFFER,
			DIVIDE_BUFFER_SCALAR,
			DIVIDE_SCALAR_SCALAR,

			RESCALE_BUFFER_BUFFER,
			RESCALE_BUFFER_SCALAR,
			RESCALE_SCALAR,

			HARDKNEE_BUFFER,
			HARDKNEE_SCALAR,

			NOISE,
			RANDOM,

			ABS_BUFFER,
			ABS_SCALAR,
			FLOOR_BUFFER,
			FLOOR_SCALAR,
			CEIL_BUFFER,
			CEIL_SCALAR,
			ROUND_BUFFER,
			ROUND_SCALAR,
			SIGN_BUFFER,
			SIGN_SCALAR,
			MODULUS_BUFFER,
			MODULUS_SCALAR,
			POWER_SCALAR,
			POWER_BUFFER,
			MAX_SCALAR,
			MAX_BUFFER,
			COMPRESSOR_EG,
			UNUSED_1,

			LERP_BUFFER,
			LERP_BUFFER_BUFFER,
			LERP_SCALAR,

			HARD_CLIP_BUFFER_BUFFER,
			HARD_CLIP_BUFFER_SCALAR,
			HARD_CLIP_SCALAR_SCALAR,
			SOFT_CLIP_BUFFER_BUFFER,
			SOFT_CLIP_BUFFER_SCALAR,
			SOFT_CLIP_SCALAR_SCALAR,

			ENVELOPE_FOLLOWER_BUFFER,
			ENVELOPE_FOLLOWER_SCALAR,

			BiquadCoefficients_LowPass2Pole,
			BiquadCoefficients_HighPass2Pole,
			BiquadCoefficients_BandPass,
			BiquadCoefficients_BandStop,
			BiquadCoefficients_LowPass4Pole,
			BiquadCoefficients_HighPass4Pole,
			BiquadCoefficients_PeakingEQ,

			BiquadProcess_2Pole,
			BiquadProcess_4Pole,

			OnePole_LPF_BUFFER_BUFFER,
			OnePole_LPF_BUFFER_SCALAR,
			OnePole_LPF_SCALAR,
			OnePole_HPF_BUFFER_BUFFER,
			OnePole_HPF_BUFFER_SCALAR,
			OnePole_HPF_SCALAR,

			OSC_RAMP_BUFFER_BUFFER,
			OSC_RAMP_BUFFER_SCALAR,
			OSC_RAMP_SCALAR,

			SINE_BUFFER,
			SINE_SCALAR,
			COS_BUFFER,
			COS_SCALAR,
			TRI_BUFFER,
			TRI_SCALAR,
			SQUARE_BUFFER,
			SQUARE_SCALAR, 
			SAW_BUFFER,
			SAW_SCALAR,

			TRIGGER_LATCH,

			ENV_GEN_LIN_INT_BUFFER,
			ENV_GEN_LIN_OS_BUFFER,
			ENV_GEN_LIN_RET_BUFFER,
			ENV_GEN_EXP_INT_BUFFER,
			ENV_GEN_EXP_OS_BUFFER,
			ENV_GEN_EXP_RET_BUFFER,

			TIMED_TRIGGER_INT,
			TIMED_TRIGGER_OS,
			TIMED_TRIGGER_RET,

			READ_VARIABLE,
			FINISHED_TRIGGER,

			READ_INPUT_0,
			READ_INPUT_1,
			READ_INPUT_2,
			READ_INPUT_3,
			READ_INPUT_4,
			READ_INPUT_5,
			READ_INPUT_6,
			READ_INPUT_7,

			NOTE_TO_FREQ_SCALAR,
			NOTE_TO_FREQ_BUFFER,

			SAH_STATIC_SCALAR,

			DECIMATE,
			COUNTER,
			COUNTER_TRIGGER,

			GATE_BUFFER_BUFFER,
			GATE_BUFFER_SCALAR,
			GATE_SCALAR_SCALAR,

			SMALLDELAY_FRAC,
			SMALLDELAY_STATIC,
			SMALLDELAY_FRAC_FB,					
			SMALLDELAY_STATIC_FB,

			TRIGGER_DIFF,

			RANDOMIZE_ONINIT,
			HOLD_SAMPLE,
			AW_FILTER,

			LERP_THREE_BUFFERS,


			BiquadCoefficients_LowShelf2Pole,
			BiquadCoefficients_LowShelf4Pole,
			BiquadCoefficients_HighShelf2Pole,
			BiquadCoefficients_HighShelf4Pole,

			Switch_Norm_Buffer,
			Switch_Index_Buffer,
			Switch_Lerp_Buffer,
			Switch_EqualPower_Buffer,
			Switch_Norm_Scalar,
			Switch_Index_Scalar,
			Switch_Lerp_Scalar,
			Switch_EqualPower_Scalar,

			Allpass_Static,
			Allpass_Buffer,

			NUM_OP_CODES,

			END = 0xff
		};
		CompileTimeAssert(NUM_OP_CODES <= END);

		enum synthReferenceTypes
		{
			Buffer = 0,
			Register,
			Constant,
			Variable
		};
	};	
}


#endif // AUD_SYNTHOPCODES_H
