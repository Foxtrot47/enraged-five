
#include "rectifier.h"

#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage
{

	using namespace Vec;

	synthRectifier::synthRectifier()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[SIGNAL].Init(this, "Input",synthPin::SYNTH_PIN_DATA_SIGNAL);

		m_Inputs[PRE_DC_OFFSET].Init(this, "PreDC",synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[PRE_DC_OFFSET].SetStaticValue(0.f);

		m_Inputs[NEGATIVE_GAIN].Init(this, "-ve Gain",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[NEGATIVE_GAIN].SetStaticValue(1.f);

		m_Inputs[POSITIVE_GAIN].Init(this, "+ve Gain",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[POSITIVE_GAIN].SetStaticValue(1.f);

		m_Inputs[POST_DC_OFFSET].Init(this, "PostDC",synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[POST_DC_OFFSET].SetStaticValue(0.f);
	}

	void synthRectifier::Synthesize()
	{
		Vec::Vector_4V preDCOffset = Vec::V4VConstant(V_ZERO);
		if(m_Inputs[PRE_DC_OFFSET].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			preDCOffset = m_Inputs[PRE_DC_OFFSET].GetSignalValueV(0);
		}

		Vec::Vector_4V postDCOffset = Vec::V4VConstant(V_ZERO);
		if(m_Inputs[POST_DC_OFFSET].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			postDCOffset = m_Inputs[POST_DC_OFFSET].GetSignalValueV(0);
		}

		Vec::Vector_4V negativeGain = Vec::V4VConstant(V_ONE);
		if(m_Inputs[NEGATIVE_GAIN].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			negativeGain = m_Inputs[NEGATIVE_GAIN].GetNormalizedValueV(0);
		}

		Vec::Vector_4V positiveGain = Vec::V4VConstant(V_ONE);
		if(m_Inputs[NEGATIVE_GAIN].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			positiveGain = m_Inputs[POSITIVE_GAIN].GetNormalizedValueV(0);
		}

	
		if(m_Inputs[SIGNAL].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			const f32 input = m_Inputs[SIGNAL].GetSignalValue(0) + GetX(preDCOffset);

			const f32 positiveSample = input * GetX(positiveGain);
			const f32 negativeSample = input * GetX(negativeGain);
			const f32 output = Selectf(input, positiveSample, negativeSample) + GetX(postDCOffset);
			m_Outputs[0].SetStaticValue(output);

		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			for(u32 i = 0; i < m_Inputs[SIGNAL].GetDataBuffer()->GetSize(); i += 4)
			{
				if(m_Inputs[PRE_DC_OFFSET].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
				{
					preDCOffset = m_Inputs[PRE_DC_OFFSET].GetSignalValueV(i);
				}

				if(m_Inputs[POST_DC_OFFSET].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
				{
					postDCOffset = m_Inputs[POST_DC_OFFSET].GetSignalValueV(i);
				}

				if(m_Inputs[NEGATIVE_GAIN].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
				{
					negativeGain = m_Inputs[NEGATIVE_GAIN].GetNormalizedValueV(i);
				}

				if(m_Inputs[NEGATIVE_GAIN].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
				{
					positiveGain = m_Inputs[POSITIVE_GAIN].GetNormalizedValueV(i);
				}

				const Vec::Vector_4V input = Vec::V4Add(m_Inputs[SIGNAL].GetSignalValueV(i), preDCOffset);
				const Vec::Vector_4V positiveSample = Vec::V4Scale(input, positiveGain);
				const Vec::Vector_4V negativeSample = Vec::V4Scale(input, negativeGain);
				const Vec::Vector_4V selMask = Vec::V4IsGreaterThanOrEqualV(input, Vec::V4VConstant(V_ZERO));
				const Vec::Vector_4V output = Vec::V4Add(Vec::V4SelectFT(selMask,
																negativeSample,positiveSample), postDCOffset);
				*((Vec::Vector_4V*)(&m_Outputs[0].GetDataBuffer()->GetBuffer()[i])) = output;

			}// for each sample
		}
	}//synthesize
}
#endif
