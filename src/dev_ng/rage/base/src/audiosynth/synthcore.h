// 
// audiosynth/synthcore.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
//

#ifndef AUD_SYNTHCORE_H
#define AUD_SYNTHCORE_H

#include "atl/array.h"
#include "atl/map.h"
#include "audiohardware/dspeffect.h"
#include "audiohardware/mixer.h"
#include "audiohardware/pcmsource.h"
#include "audiohardware/pcmsource_interface.h"
#include "synthcontext.h"
#include "synthdefs.h"
#include "synthopcodes.h"

#include "vectormath/vectormath.h"

namespace rage
{

struct CompiledSynth;

class synthCore
{
public:

	static void InitClass();
	static void ShutdownClass();

	synthCore()
		: m_ProgramEntry(NULL)
		, m_NumConstants(0)
		, m_NumBuffers(0)
		, m_NumRegisters(0)
		, m_NumOutputs(0)
		, m_IsFinished(false)
		, m_IsPaused(false)
		, m_IsPauseRequested(false)
	{
		BANK_ONLY(m_AssetNameHash = 0);
		BANK_ONLY(m_PeakLevelLin = 0.f);
		DEV_ONLY(m_SynthName = NULL);		
	}

	bool Init(const u32 synthNameHash);
	void Shutdown();
	void GenerateFrame(const float **inputBuffers, const atRangeArray<u16, audPcmSource::kMaxPcmSourceChannels> &outputBufferIds);
	void SkipFrame();
	void EndFrame();
	void StartPhys();
	void StopPhys();
	u32 GetNumOutputs() const { return m_NumOutputs; }

	static u32 GetCostEstimate(const u32 synthNameHash);

#if !__FINAL
	void ResetRuntimeMs() { m_RuntimeMs = 0.f; }
	float GetRuntimeMs() const { return m_RuntimeMs; }
#endif

#if !__SPU
	bool ApplyPreset(const u32 presetNameHash);
#endif

	void SetVariableValue(const u32 nameHash, const float val);

	bool IsFinished() const { return m_IsFinished; }
	
#if SYNTH_PROFILE

	struct ALIGNAS(16) synthProfileData
	{
		atRangeArray<float, synthOpcodes::NUM_OP_CODES> OpCodeTimings;
		atRangeArray<u32, synthOpcodes::NUM_OP_CODES> OpCodeCounters;
	};

	static float GetOpcodeTime(const u32 opcode) { return sm_ProfileData.OpCodeTimings[opcode]; }
	static u32 GetOpcodeCount(const u32 opcode) { return sm_ProfileData.OpCodeCounters[opcode]; }
	static synthProfileData &GetProfileData() { return sm_ProfileData; }
#endif // SYNTH_PROFILE

	ASSERT_ONLY(int SynthAssertFailed(const char *msg, const char *exp, const char *file, const int line) const);


	BANK_ONLY(u32 GetAssetNameHash() const { return m_AssetNameHash; })
	BANK_ONLY(float GetPeakLevel() const { return m_PeakLevelLin; })

	ASSERT_ONLY(static void DumpQuietAssetList());

private:
	enum { kMaxRegisters = 48 };
	enum { kMaxBuffers = 16 };
	enum { kMaxStateBlocks = 64 };
	enum { kMaxVariables = 20 };

	__forceinline float ReadConstant(const u32 index) const 
	{
		// C0 and C1 are magic numbers 0.0 an 1.0
		if(index == 0)
		{
			return 0.0f;
		}
		else if(index == 1)
		{
			return 1.0f;
		}
		else
		{
			TrapGE(index - 2, (u32)m_NumConstants);
			return GetConstants()[index - 2];
		}
	}

	float ReadVariable(const u32 index) const
	{
		return m_InputVariableValues[index];
	}

	Vec::Vector_4V_Ref GetStateBlock(const u32 index);

	const float *GetConstants() const
	{
		// byte programBytes[programSizeBytes];
		// u32 numConstants
		// float constants[];
		return (const float *)((const u8 *)GetProgramEntry() + m_ProgramSizeBytes + 4);
	}

	const u16 *GetProgramEntry() const
	{
#if __SPU
		return sm_LocalProgramEntry;
#else
		return m_ProgramEntry;
#endif
	}

	float LoadScalarReference(const u32 refVal, const atRangeArray<float,kMaxRegisters> &registers) const;

	const u16 *FetchProgram(const s32 tag) const;
	
	synthContext m_Context;

	const u16 *m_ProgramEntry;
	
	atRangeArray<u8, audPcmSource::kMaxPcmSourceChannels> m_OutputBufferMap;

	struct InputVariableMap
	{
		u32 NameHash;
		s32 Index;

		InputVariableMap() {}
		InputVariableMap(const u32 nameHash) 
			: NameHash(nameHash)
			, Index(0)
		{

		}

		bool operator==(const InputVariableMap &rhs) const
		{
			return NameHash == rhs.NameHash;
		}

		bool operator<(const InputVariableMap &rhs) const
		{
			return NameHash < rhs.NameHash;
		}
	};
	atFixedArray<InputVariableMap, kMaxVariables> m_InputVariableMap;
	atFixedArray<float, kMaxVariables> m_InputVariableValues;

	static Vec::Vector_4V sm_ZeroState;

	static int Compare(const InputVariableMap *a, const InputVariableMap *b);
	
	// Assuming here a frame buffer is atleast 128 f32s, so we can fit 32 Vec4s in each.
	CompileTimeAssert(kMixBufNumSamples >= 128);
	atRangeArray<Vec::Vector_4V*, kMaxStateBlocks / 32> m_StateBlockPtrs;
	atFixedArray<u16, kMaxStateBlocks / 32> m_StateBlockIds;	

	NOTFINAL_ONLY(float m_RuntimeMs);
	BANK_ONLY(u32 m_AssetNameHash);
	BANK_ONLY(float m_PeakLevelLin);

	// Asserts are compiled out on SPU, so make sure this __DEV rather than __ASSERT
	DEV_ONLY(const char *m_SynthName);

	u16 m_ProgramSizeBytes;
	u8 m_NumConstants;
	u8 m_NumBuffers;
	u8 m_NumStateBlocks;
	u8 m_NumRegisters;
	u8 m_NumOutputs;

	bool m_IsFinished;
	bool m_IsPaused;
	bool m_IsPauseRequested;
	// (MaxBuffers - 1) since we need at least one output buffer to be useful.
	static atRangeArray<float *, kMaxBuffers - 1> sm_WorkingBuffers;

	SYNTH_ASSERT_ONLY(static u32 sm_ProcessingOpcode);
	SYNTH_ASSERT_ONLY(static size_t sm_ProcessingOffset);

#if __ASSERT
	static atMap<u32, float> sm_AssetPeakLevels;
#endif

#if SYNTH_PROFILE
	static synthProfileData sm_ProfileData;
#endif // SYNTH_PROFILE

#if __SPU
	static u8 *sm_LocalProgramStorage;
	static const u16 *sm_LocalProgramEntry;
#endif
};

class synthCorePcmSource : public audPcmSource
{
public:
	struct Params
	{
		static const u32 CompiledSynthNameHash = 0xF99EE90E;
		static const u32 PresetNameHash = 0x17E452E8;
		static const u32 ExternalPcmSourceVariableBlock = 0xE4F4A655;
		static const u32 Bypass = 0x10F3D95C;
	};

	synthCorePcmSource() 
		: audPcmSource(AUD_PCMSOURCE_SYNTHCORE)
		, m_ExternalPcmSourceVariableBlock(NULL)
	{

	}

#if !__SPU

	bool Init(const u32 synthNameHash) { return m_Synth.Init(synthNameHash); }
	bool ApplyPreset(const u32 presetNameHash) { return m_Synth.ApplyPreset(presetNameHash); }
	
	AUD_PCMSOURCE_VIRTUAL void Shutdown() { m_Synth.Shutdown(); }
	AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 paramId, const u32 value);
	AUD_PCMSOURCE_VIRTUAL void SetParam(const u32 paramId, const f32 value);

#endif

	// audPcmSource interface
	AUD_PCMSOURCE_VIRTUAL void GenerateFrame();
	AUD_PCMSOURCE_VIRTUAL void SkipFrame();
	// audPcmSource interface
	AUD_PCMSOURCE_VIRTUAL void BeginFrame();
	AUD_PCMSOURCE_VIRTUAL void EndFrame();
	// PURPOSE
	//	Informs the generator that it is now playing on a physical voice
	AUD_PCMSOURCE_VIRTUAL void StartPhys(s32 channel);
	// PURPOSE
	//	Informs the generator that it is no longer playing on a physical voice
	AUD_PCMSOURCE_VIRTUAL void StopPhys(s32 channel);
	
	AUD_PCMSOURCE_VIRTUAL bool IsFinished() const { return m_Synth.IsFinished(); }

#if !__FINAL
	void ResetRuntimeMs() { m_Synth.ResetRuntimeMs(); }
	float GetRuntimeMs() const
	{
		return m_Synth.GetRuntimeMs();
	}

	AUD_PCMSOURCE_VIRTUAL void GetAssetInfo(audPcmSource::AssetInfo &assetInfo) const;
#endif

	const synthCore &GetSynth() const { return m_Synth; }

private:
	synthCore m_Synth;
	audPcmVariableBlock* m_ExternalPcmSourceVariableBlock;
};

class synthCoreDspEffect : public audDspEffect
{
public:

	synthCoreDspEffect() : audDspEffect(AUD_DSPEFFECT_SYNTHCORE)
	{

	}
	virtual bool Init(const u32 numChannels, const u32 channelMask);
	virtual void Shutdown();

	AUD_DECLARE_DSP_PROCESS_FUNCTION;

	virtual void SetParam(const u32 paramId, const f32 value);
	virtual void SetParam(const u32 paramId, const u32 value);

private:
	synthCore m_Synth;
	u8 m_NumInputChannels;
	u8 m_NumChannelsToProcess;
	u8 m_ChannelMask;
	bool m_IsInitialised;
	bool m_Bypass;
};

} // namespace rage

#endif // AUD_SYNTHCORE_H
