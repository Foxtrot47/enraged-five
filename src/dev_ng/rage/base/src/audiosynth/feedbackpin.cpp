#include "feedbackpin.h"

#if !SYNTH_MINIMAL_MODULES

#include "feedbackpinview.h"
#include "synthdefs.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthFeedbackPin);

	synthFeedbackPin::synthFeedbackPin()
	{
		m_Inputs[0].Init(this,"In", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Outputs[0].Init(this,"Out", synthPin::SYNTH_PIN_DATA_SIGNAL, true);

		m_ProcessInputFirst = true;
	}

	void synthFeedbackPin::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("ProcessInputFirst", m_ProcessInputFirst);
	}

	void synthFeedbackPin::Synthesize()
	{
		m_Outputs[0].SetDataFormat(m_Inputs[0].GetDataFormat());
		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_Outputs[0].SetStaticValue(m_Inputs[0].GetStaticValue());
		}
		else
		{
			m_Outputs[0].SetDataBuffer(m_Inputs[0].GetDataBuffer());
		}
	}
}
#endif
