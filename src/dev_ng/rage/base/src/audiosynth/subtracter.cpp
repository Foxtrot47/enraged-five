// 
// audiosynth/subtracter.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "subtracter.h"
#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;
	synthSubtracter::synthSubtracter()
	{
		m_Outputs[0].Init(this, "Result",synthPin::SYNTH_PIN_DATA_NORMALIZED,true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[0].Init(this, "Input1",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetStaticValue(1.f);
		m_Inputs[0].SetConvertsInternally(false);

		m_Inputs[1].Init(this, "Input2",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[1].SetStaticValue(1.f);
		m_Inputs[1].SetConvertsInternally(false);
	}

	void synthSubtracter::Synthesize()
	{
		m_Outputs[0].SetDataFormat(
			(m_Inputs[0].GetDataFormat()==synthPin::SYNTH_PIN_DATA_SIGNAL||m_Inputs[1].GetDataFormat()==synthPin::SYNTH_PIN_DATA_SIGNAL) ? 
			synthPin::SYNTH_PIN_DATA_SIGNAL :
		synthPin::SYNTH_PIN_DATA_NORMALIZED);

		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC &&
			m_Inputs[1].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// both static
			m_Outputs[0].SetStaticValue(m_Inputs[0].GetStaticValue() - m_Inputs[1].GetStaticValue());
		}
		else if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC ||
			m_Inputs[1].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// one static, one dynamic		
			if(m_Outputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
			{
				for(u32 i = 0; i < m_Buffer.GetSize(); i += 4)
				{
					*((Vector_4V*)&m_Outputs[0].GetDataBuffer()->GetBuffer()[i]) = V4Subtract(m_Inputs[0].GetSignalValueV(i), m_Inputs[1].GetSignalValueV(i));
				}
			}
			else
			{
				for(u32 i = 0; i < m_Buffer.GetSize(); i += 4)
				{
					*((Vector_4V*)&m_Outputs[0].GetDataBuffer()->GetBuffer()[i]) = V4Subtract(m_Inputs[0].GetNormalizedValueV(i), m_Inputs[1].GetNormalizedValueV(i));
				}
			}
	
			m_Outputs[0].SetDataBuffer(&m_Buffer);			
		}
		else
		{
			// both dynamic
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			f32 *buf0 = m_Inputs[0].GetDataBuffer()->GetBuffer();
			f32 *buf1 = m_Inputs[1].GetDataBuffer()->GetBuffer();
			for(u32 i = 0; i < m_Buffer.GetSize(); i += 4)
			{
				const Vector_4V in1 = *((Vector_4V*)&buf0[i]);
				const Vector_4V in2 = *((Vector_4V*)&buf1[i]);
				*((Vector_4V*)&m_Outputs[0].GetDataBuffer()->GetBuffer()[i]) = V4Subtract(in1, in2);
			}
		}
	}

} // namespace rage
