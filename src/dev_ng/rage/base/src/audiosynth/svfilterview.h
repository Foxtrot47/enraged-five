// 
// audiosynth/svfilterview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SVFILTERVIEW_H
#define SYNTH_SVFILTERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uilabel.h"
#include "uimenuview.h"
#include "uirangeview.h"

namespace rage
{
	class synthStateVariableFilter;
	class synthStateVariableFilterView : public synthModuleView
	{
	public:

		synthStateVariableFilterView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();

	private:

		synthStateVariableFilter *GetFilterModule() 
		{
			return (synthStateVariableFilter*)m_Module;
		}

		synthUIRangeView m_FreqRange;
		synthUIRangeView m_QRange;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_SVFILTERVIEW_H

