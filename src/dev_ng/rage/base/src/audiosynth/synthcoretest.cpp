//
// audiosynth/synthcoretest.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
//

#include "synthcore.h"
#include "synthcoredisasm.h"
#include "synthcoretest.h"
#include "synthobjects.h"
#include "synthesizer.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audiohardware/framebufferpool.h"
#include "audiohardware/mixer.h"
#include "audiohardware/mixing_vmath.inl"
#include "audiohardware/pcmsource.h"
#include "audiohardware/pcmsource_interface.h"
#include "audioengine/metadatamanager.h"
#include "file/stream.h"
#include "system/param.h"
#include "system/performancetimer.h"

#if RSG_BANK

namespace rage
{
XPARAM(synthcoretest);
PARAM(synthcoretimings, "[RAGE Audio] output file to write synth core profile timings");

	bool synthCoreTest::Init(const u32 synthNameHash)
	{
		m_FrameCount = 0U;
		m_RunCount = 0U;
		m_RunTimeAccum = 0.f;
		m_RunningMean = 0.f;

		m_AssetName = synthSynthesizer::GetMetadataManager().GetObjectName(synthNameHash);

		return InitSynth();
	}

	bool synthCoreTest::InitSynth()
	{
		const s32 sourceId = audDriver::GetMixer()->AllocatePcmSource_Sync(AUD_PCMSOURCE_SYNTHCORE);
		if(sourceId == -1)
		{
			audErrorf("Failed to allocate synthcore PCM Source");
			return false;
		}
		m_Synth = reinterpret_cast<synthCorePcmSource *>(audDriver::GetMixer()->GetPcmSource(sourceId));
		if(!m_Synth->Init(atStringHash(m_AssetName)))
		{
			return false;
		}

		m_Synth->SetParam(ATSTRINGHASH("Frequency", 0x31FF1BC8), 1.0f);
		m_Synth->SetParam(ATSTRINGHASH("IsReleasing", 0x491F6B3D), 0.0f);
		m_Synth->StartPhys(0);
		m_Synth->Start();
		
		return true;
	}

	void synthCoreTest::Shutdown()
	{
		// Free output buffers
		m_Synth->StopPhys(0);
		const s32 sourceId = audDriver::GetMixer()->GetPcmSourceSlotId(m_Synth);

		audPcmSourceInterface::Release(sourceId);
		audDriver::GetMixer()->FreePcmSource(sourceId);
	}
	
	// ~10 seconds
	enum {kTestLengthFrames = 1000 * 10};

	bool synthCoreTest::ValidateOutput()
	{
		m_RunTimeAccum += m_Synth->GetRuntimeMs();
		m_Synth->ResetRuntimeMs();
		m_FrameCount++;

		for(s32 bufferId = 0; bufferId < audPcmSource::kMaxPcmSourceChannels; bufferId++)
		{
			if(m_Synth->HasBuffer(bufferId))
			{
				if(!ValidateOutputBuffer(audDriver::GetMixer()->GetFrameBufferPool().GetBuffer_ReadOnly(m_Synth->GetBufferId(bufferId))))
				{
					audErrorf("After %u frames", m_FrameCount);
					return false;
				}
			}
		}

		if(m_FrameCount > 800 * 10)
		{
			// Test IsReleasing behaviour for the last 10% of runtime
			m_Synth->SetParam(ATSTRINGHASH("IsReleasing", 0x491F6B3D), 1.0f);
		}

		// Test pausing behaviour
		if(m_FrameCount > 500*10 && m_FrameCount < 600*10)
		{
			m_Synth->SetParam(ATSTRINGHASH("Frequency", 0x31FF1BC8), 0.0f);
		}
		else
		{
			m_Synth->SetParam(ATSTRINGHASH("Frequency", 0x31FF1BC8), 1.0f);
		}

		if(m_Synth->IsFinished())
		{
			if(m_FrameCount < kTestLengthFrames && m_RunCount++ < 1000)
			{
				Shutdown();
				InitSynth();
			}
		}
		return true;
	}

	bool synthCoreTest::ValidateOutputBuffer(const float *outputBuffer)
	{ 
		const float peakLevel = ComputePeakLevel<kMixBufNumSamples>(outputBuffer);
		if(peakLevel > 1.01f)
		{
			audErrorf("PeakLevel: +%.2fdB", audDriverUtil::ComputeDbVolumeFromLinear(peakLevel));
			return false;
		}
		else if(!FPIsFinite(peakLevel))
		{
			audErrorf("Non-finite peak");
			return false;
		}

		const float meanLevel = ComputeMeanLevel<kMixBufNumSamples>(outputBuffer);

		m_RunningMean = (meanLevel + (m_RunningMean * m_FrameCount)) / float(m_FrameCount+1);
		if(m_FrameCount > 40 && Abs(m_RunningMean) > 0.18f)
		{
			audErrorf("DC offset detected: %.2fdB (%f)", audDriverUtil::ComputeDbVolumeFromLinear_Precise(m_RunningMean), m_RunningMean);
			return false;
		}
		return true;
	}

	bool synthCoreTest::IsFinished() const
	{
		return (m_Synth->IsFinished() || m_FrameCount > kTestLengthFrames);
	}

	float synthCoreTest::GetTotalRuntimeMs() const
	{
		return m_RunTimeAccum;
	}

	class synthCoreTestReport
	{
	public:
		struct TestResult
		{
			const char *synthName;
			u32 numFrames;
			float totalRuntimeMs;
			float averageRuntimeMs;
			bool succeeded;
		};

		synthCoreTestReport(const char *fileName, bool header = true)
		{
			m_Stream = ASSET.Create(fileName, "");
			if(header)
				AddLine("Name,Duration_S,TotalCPU_Ms,AverageCPU_Ms");			
		}

		~synthCoreTestReport()
		{
			if(HasStream())
			{
				m_Stream->Close();
				m_Stream = NULL;
			}
		}

		void AddEntry(TestResult &r)
		{
			char lineBuf[128];
			formatf(lineBuf, "%s,%.3f,%.3f,%.3f", r.synthName,
													r.numFrames / static_cast<float>(kMixerNativeSampleRate/kMixBufNumSamples),
													r.totalRuntimeMs,
													r.averageRuntimeMs);
			AddLine(lineBuf);
		}

		void AddLine(const char *line)
		{
			if(HasStream())
			{
				char lineBuf[128];
				formatf(lineBuf, "%s\r\n", line);
				m_Stream->Write(lineBuf, istrlen(lineBuf));
			}
		}

		bool HasStream() const { return m_Stream != NULL; }

	private:

		fiStream *m_Stream;

	};

	bool synthCoreTestManager::RunTests()
	{
		audDisplayf("Starting synth asset test...");
		sysPerformanceTimer timer("All tests");
		timer.Start();

		atArray<synthCoreTestReport::TestResult> testResults;

		audDriver::GetMixer()->EnterBML();

		s32 numTested = 0;
		const audMetadataManager &metadataMgr = synthSynthesizer::GetMetadataManager();
		const u32 numMetadataObjects = metadataMgr.ComputeNumberOfObjects();
		audMetadataManager::ObjectIterator iter(metadataMgr, CompiledSynth::TYPE_ID);

		bool iteratedOverAllObjects = false;
		
		while(!iteratedOverAllObjects || m_Tests.GetCount())
		{
			const s32 newTestsToStart = m_Tests.GetMaxCount() - m_Tests.GetCount();
			for(s32 newId = 0; newId < newTestsToStart; newId++)
			{
				u32 objectIndex = iter.Next();
				if(objectIndex < numMetadataObjects)
				{
					synthCoreTest &test = m_Tests.Append();
					
					audMetadataObjectInfo objectInfo;
					if(metadataMgr.GetObjectInfoFromObjectIndex(objectIndex, objectInfo))
					{
						const char *objectName = objectInfo.GetName();
						const u32 synthNameHash = atStringHash(objectName);
						if(!test.Init(synthNameHash))
						{
							audErrorf("Failed to init synthCore instance for %s", objectName);
							audDriver::GetMixer()->ExitBML();
							return false;
						}
						numTested++;
					}
				}
				else
				{
					// we've reached the end of our asset list
					iteratedOverAllObjects = true;
					break;
				}
				
			}

			audDriver::GetMixer()->GeneratePcm();

			atArray<s32> finishedTests;

			for(s32 i = 0; i < m_Tests.GetCount(); i++)
			{
				if(!m_Tests[i].ValidateOutput())
				{
					audErrorf("ValidateOutput failed for %s", m_Tests[i].GetAssetName());
					finishedTests.Grow() = i;
				}
				else if(m_Tests[i].IsFinished())
				{
					finishedTests.Grow() = i;
				}
			}


			s32 numDeleted = 0;
			for(s32 i = 0; i < finishedTests.GetCount(); i++)
			{
				s32 correctedI = i - numDeleted;

				synthCoreTest &test = m_Tests[finishedTests[correctedI]];

				synthCoreTestReport::TestResult &r = testResults.Grow();
				r.synthName = test.GetAssetName();
				r.averageRuntimeMs = test.GetAverageRuntimeMs();
				r.totalRuntimeMs = test.GetTotalRuntimeMs();
				r.numFrames = test.GetFrameCount();

				m_Tests[finishedTests[correctedI]].Shutdown();
				m_Tests.Delete(finishedTests[correctedI]);

				numDeleted ++;
			}
		}

		audDriver::GetMixer()->ExitBML();

		audDisplayf("Finished synth tests - tested %u assets.", numTested);
		
		timer.Stop();
		audDisplayf("Test took %fS", timer.GetTimeMS() / 1000.f);


		const char *fileName = "synthtest.csv";
		PARAM_synthcoretest.Get(fileName);
		synthCoreTestReport report(fileName);

		if(report.HasStream())
		{
			for(s32 i = 0; i < testResults.GetCount(); i++)
			{
				report.AddEntry(testResults[i]);
			}
		}

#if SYNTH_PROFILE

		fileName = "synthtimings.csv";
		PARAM_synthcoretimings.Get(fileName);

		synthCoreTestReport timingReport(fileName, false);

		timingReport.AddLine("OpName,Count,Total_Ms,Average_us");

		for(u32 i = 0; i < synthOpcodes::NUM_OP_CODES; i++)
		{
			char linebuf[128];

			// mean time in uS
			float avg = 0.f;
			if(synthCore::GetOpcodeCount(i) > 0)
			{
				avg = synthCore::GetOpcodeTime(i) / (static_cast<float>(synthCore::GetOpcodeCount(i)) / 1000.f);
			}
			
			formatf(linebuf,
					"%s,%u,%f,%f",
						synthCoreDisasm::GetOpcodeName(i),
						synthCore::GetOpcodeCount(i),
						synthCore::GetOpcodeTime(i),
						avg);
			
					
			timingReport.AddLine(linebuf);
		}
#endif
		return true;
	}

} // namespace rage

#endif // RSG_BANK

