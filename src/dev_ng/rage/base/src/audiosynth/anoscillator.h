
#ifndef SYNTH_ANOSCILLATOR_H
#define SYNTH_ANOSCILLATOR_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES
#include "sampleframe.h"
#include "module.h"
#include "pin.h"

namespace rage
{
	class synthModuleView;
	class synthAnalogOscillator : public synthModule
	{
	public:
		enum synthAnalogOscillatorMode
		{
			SQUARE,
			TRIANGLE,
			SAW,
			NUM_ANOSCILLATOR_MODES
		};

		static char *ModeStrings[NUM_ANOSCILLATOR_MODES];

		synthAnalogOscillator();
		virtual ~synthAnalogOscillator();
		
		virtual void GetInputs(synthPin *&pins, u32 &numPins);
		virtual void GetOutputs(synthPin *&pins, u32 &numPins);

		virtual void SerializeState(synthModuleSerializer *UNUSED_PARAM(serializer)){}
		
		virtual synthPin &GetInput(const u32 index)
		{
			return m_Inputs[index];
		}
		
		virtual synthPin &GetOutput(const u32)
		{
			return m_Output;
		}
		
		virtual void Synthesize();

		enum synthAnalogOscillatorInputs
		{
			AMPLITUDE = 0,
			FREQUENCY,
			PHASE,
			NUM_ANOSC_INPUTS
		};
		
		void SetMinFrequency(const f32 freq)
		{
			m_MinFrequency = freq;
		}

		f32 GetMinFrequency() const
		{
			return m_MinFrequency;
		}
		
		void SetMaxFrequency(const f32 freq)
		{
			m_MaxFrequency = freq;
		}

		f32 GetMaxFrequency() const
		{
			return m_MaxFrequency;
		}
		
		void SetMode(const synthAnalogOscillatorMode mode)
		{
			m_Mode = mode;
		}
		
		synthAnalogOscillatorMode GetMode() const
		{
			return m_Mode;
		}

		virtual const char *GetName() const
		{
			return "AnalogOsc";
		}

		virtual u32 ComputeNumOutputBuffersRequired() { return 1; }

	private:

		synthAnalogOscillatorMode m_Mode;
		f32 m_Phase;
		// amplitude, frequency, (optional) phase
		synthPin m_Inputs[NUM_ANOSC_INPUTS];
		// osc * amplitude
		synthPin m_Output;
		synthSampleFrame m_Buffer;
		
		f32 m_MinFrequency;
		f32 m_MaxFrequency;

		f32 m_LastSample;
		f32 m_P;
	};
}
#endif
#endif // SYNTH_ANOSCILLATOR_H


