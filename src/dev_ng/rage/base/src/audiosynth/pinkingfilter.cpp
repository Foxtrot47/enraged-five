
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "pinkingfilter.h"
#include "pin.h"

namespace rage
{
	synthPinkingFilter::synthPinkingFilter() 
	{
		m_Outputs[0].Init(this,"Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);
		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);

		m_b0 = 0.f;
		m_b1 = 0.f;
		m_b2 = 0.f;
		m_b3 = 0.f;
		m_b4 = 0.f;
		m_b5 = 0.f;
		m_b6 = 0.f;
	}

	void synthPinkingFilter::Synthesize()
	{
		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			return;
		}

		f32 b0 = m_b0;
		f32 b1 = m_b1;
		f32 b2 = m_b2;
		f32 b3 = m_b3;
		f32 b4 = m_b4;
		f32 b5 = m_b5;
		f32 b6 = m_b6;
		for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i++)
		{			
			const f32 input = m_Inputs[0].GetSignalValue(i);
			// NOTE: these coefficients are hardcoded for 44.1khz and may require scaling
			//	to maintain accuracy at 48kHz
			b0 = 0.99886f * b0 + input * 0.0555179f;
			b1 = 0.99332f * b1 + input * 0.0750759f;
			b2 = 0.96900f * b2 + input * 0.1538520f;
			b3 = 0.86650f * b3 + input * 0.3104856f;
			b4 = 0.55000f * b4 + input * 0.5329522f;
			b5 = -0.7616f * b5 - input * 0.0168980f;
			const f32 out = b0 + b1 + b2 + b3 + b4 + b5 + b6 + input * 0.5362f;
			b6 = input * 0.115926f;
			// unity gain is at nyquist, so need to scale
			// -36dB
			const f32 gain = 0.01584893192461f;
			m_Outputs[0].GetDataBuffer()->GetBuffer()[i] = out * gain;
			
		}// for each sample

		m_b0 = b0;
		m_b1 = b1;
		m_b2 = b2;
		m_b3 = b3;
		m_b4 = b4;
		m_b5 = b5;
		m_b6 = b6;
	}//synthesize
}
#endif