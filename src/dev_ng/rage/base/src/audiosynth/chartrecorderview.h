// 
// audiosynth/chartrecorderview.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_CHARTRECORDERVIEW_H
#define SYNTH_CHARTRECORDERVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uiwavedataview.h"

namespace rage
{
    class synthChartRecorder;
    class synthChartRecorderView : public synthModuleView
    {
    public:

        synthChartRecorderView(synthModule *module);

        virtual void Update();
        virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;
        synthChartRecorder *GetRecorderModule() { return reinterpret_cast<synthChartRecorder*>(m_Module); }

    protected:
        virtual void Render();
        virtual bool OverrideBounds() const { return true; }

    private:

        synthUIWaveDataView m_WaveDataView;

        float m_ResizedWidth, m_ResizedHeight;
        float m_Width, m_Height;
        u32 m_LastWriteOffset;
        bool m_IsResizing;
    };
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_CHARTRECORDERVIEW_H

