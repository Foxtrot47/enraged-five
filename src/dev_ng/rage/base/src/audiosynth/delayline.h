
#ifndef SYNTH_DELAYLINE_H
#define SYNTH_DELAYLINE_H
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum synthDelayLineInputs
	{
		kDelayLineSignal = 0,
		kDelayLineDryMix,
		kDelayLineWetMix,
		kDelayLineDelay,
		kDelayLineFeedback,
		
		kDelayLineNumInputs
	};

	class synthDelayLine : public synthModuleBase<kDelayLineNumInputs, 1, SYNTH_DELAYLINE>
	{
		public:

			SYNTH_MODULE_NAME("DelayLine");

			synthDelayLine();
			virtual ~synthDelayLine();
			
			virtual void Synthesize();
						
			enum
			{
				MAX_DELAY_SAMPLES = 48000,
			};
			
			void SetMaxDelay(const f32 samples)
			{
				m_MaxDelaySamples = samples;
			}
			
			void SetMinDelay(const f32 samples)
			{
				m_MinDelaySamples = samples;
			}

			
		private:
			synthSampleFrame m_OutputBuffer;
			f32 *m_DelayBuffer;
			s32 m_DelayWriteIndex;
			
			f32 m_MaxDelaySamples;
			f32 m_MinDelaySamples;

			f32 m_SampleHistory[4];
		};
}
#endif
#endif // SYNTH_DELAYLINE_H



