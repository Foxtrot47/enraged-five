#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "audiohardware/mixer.h"
#include "dattorro.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	synthDattorroReverb::synthDattorroReverb() 
	{
		m_Inputs[kDattorroSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kDattorroDamping].Init(this, "Damping",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDattorroDamping].SetStaticValue(0.0005f);
		m_Inputs[kDattorroBandwidth].Init(this, "Bandwidth",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDattorroBandwidth].SetStaticValue(0.9995f);
		m_Inputs[kDattorroDecay].Init(this, "Decay",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDattorroDecay].SetStaticValue(0.5f);
		m_Inputs[kDattorroDecayDiffusion].Init(this, "DecayDiffusion",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDattorroDecayDiffusion].SetStaticValue(0.7f);
		m_Inputs[kDattorroInputDiffusion1].Init(this, "InputDiffusion1",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDattorroInputDiffusion1].SetStaticValue(0.75f);
		m_Inputs[kDattorroInputDiffusion2].Init(this, "InputDiffusion2",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDattorroInputDiffusion2].SetStaticValue(0.625f);
		m_Inputs[kDattorroInputExcursion1].Init(this, "Exc1", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDattorroInputExcursion2].Init(this, "Exc2", synthPin::SYNTH_PIN_DATA_NORMALIZED);

		m_Outputs[0].Init(this, "Out 1", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer1);

		m_Outputs[1].Init(this, "Out 2", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[1].SetDataBuffer(&m_Buffer2);

		m_Reverb2.Init();
	}

	synthDattorroReverb::~synthDattorroReverb()
	{
		
	}

	void synthDattorroReverb::Synthesize()
	{
		float *outPtr1 = m_Buffer1.GetBuffer();
		float *outPtr2 = m_Buffer2.GetBuffer();
		for(u32 i = 0; i < kMixBufNumSamples; i++)
		{
			m_Reverb1.Damping = m_Inputs[kDattorroDamping].GetNormalizedValue(i);
			m_Reverb1.Bandwidth = m_Inputs[kDattorroBandwidth].GetNormalizedValue(i);
			m_Reverb1.Decay = m_Inputs[kDattorroDecay].GetNormalizedValue(i);
			m_Reverb1.DecayDiffusion1 = m_Inputs[kDattorroDecayDiffusion].GetNormalizedValue(i);
			m_Reverb1.InputDiffusion1 = m_Inputs[kDattorroInputDiffusion1].GetNormalizedValue(i);
			m_Reverb1.InputDiffusion2 = m_Inputs[kDattorroInputDiffusion2].GetNormalizedValue(i);
			m_Reverb1.Excursion1 = m_Inputs[kDattorroInputExcursion1].GetNormalizedValue(i);
			m_Reverb1.Excursion2 = m_Inputs[kDattorroInputExcursion2].GetNormalizedValue(i);

			*outPtr1++ = m_Reverb1.Process(m_Inputs[kDattorroSignal].GetSignalValue(i));


			m_Reverb2.SetDamping(m_Inputs[kDattorroDamping].GetNormalizedValueV(i));
			m_Reverb2.SetBandwidth(m_Inputs[kDattorroBandwidth].GetNormalizedValueV(i));
			m_Reverb2.SetDecay(m_Inputs[kDattorroDecay].GetNormalizedValueV(i));
			m_Reverb2.SetDecayDiffusion1(m_Inputs[kDattorroDecayDiffusion].GetNormalizedValueV(i));
			m_Reverb2.SetInputDiffusion1(m_Inputs[kDattorroInputDiffusion1].GetNormalizedValueV(i));
			m_Reverb2.SetInputDiffusion2(m_Inputs[kDattorroInputDiffusion2].GetNormalizedValueV(i));
			m_Reverb2.SetExcursion1(m_Inputs[kDattorroInputExcursion1].GetNormalizedValue(i));
			m_Reverb2.SetExcursion2(m_Inputs[kDattorroInputExcursion2].GetNormalizedValue(i));

			Vector_4V inputSamples = V4LoadScalar32IntoSplatted(m_Inputs[kDattorroSignal].GetSignalValue(i));
			*outPtr2++ = GetX(m_Reverb2.Process(inputSamples));
		}
	}

	Vector_4V_Out revModelDattorro_V4::Process(Vec::Vector_4V_In input)
	{
		Vector_4V x = f_bw.Process(input);

		x = ap_13_14.Process(x);
		x = ap_19_20.Process(x);
		x = ap_15_16.Process(x);
		const Vector_4V earlyOut = ap_21_22.Process(x);

		float ap_23_24_excursion = m_Excursion1 * 16.f;
		const Vector_4V ap_23_24_out = ap_23_24.Process(V4Add(earlyOut, d_63.Read()), ap_23_24_excursion);

		const Vector_4V f_30_31_out = f_30_31.Process(d_30.Read());
		d_30.Write(ap_23_24_out);

		const Vector_4V d_39_out = d_39.Read();
		
		d_39.Write(ap_31_33.Process(V4Scale(m_Decay, f_30_31_out)));
		
		float ap_46_48_excursion = m_Excursion2 * 16.f;
		x = ap_46_48.Process(V4AddScaled(earlyOut, m_Decay, d_39_out), ap_46_48_excursion);
		const Vector_4V d_54_out = d_54.Read();
		d_54.Write(x);
		x = f_54_55.Process(d_54_out);
		d_63.Write(ap_55_59.Process(x));

		// outputScale = 0.6
		Vector_4V outputScale = V4VConstantSplat<0x3F19999A>();
		x = V4Scale(outputScale, d_54.ReadTap(266));
		x = V4AddScaled(x, outputScale, d_54.ReadTap(2974));
		x = V4SubtractScaled(x, outputScale, ap_55_59.ReadTap(1913));
		x = V4AddScaled(x,  outputScale, d_63.ReadTap(1996));
		x = V4SubtractScaled(x, outputScale, d_30.ReadTap(1990));
		x = V4SubtractScaled(x, outputScale, ap_31_33.ReadTap(187));
		return V4SubtractScaled(x, outputScale, d_39.ReadTap(1066));

	}

	float revModelDattorro::Process(const float input)
	{
		const float DecayDiffusion2 = Clamp(Decay + 0.15f, 0.25f, 0.5f);
		f_bw.Damping = 1.f - Bandwidth;

		ap_13_14.SetDiffusion(InputDiffusion1);
		ap_19_20.SetDiffusion(InputDiffusion1);
		ap_15_16.SetDiffusion(InputDiffusion2);
		ap_21_22.SetDiffusion(InputDiffusion2);

		ap_23_24.SetDecay(DecayDiffusion1);
		ap_23_24.SetDiffusion(DecayDiffusion1);
		ap_46_48.SetDecay(DecayDiffusion1);
		ap_46_48.SetDiffusion(DecayDiffusion1);

		f_30_31.Damping = Damping;
		f_54_55.Damping = Damping;

		ap_31_33.SetDiffusion(DecayDiffusion2);
		ap_55_59.SetDiffusion(DecayDiffusion2);


		float x = f_bw.Process(input);

		x = ap_13_14.Process(x);
		x = ap_19_20.Process(x);
		x = ap_15_16.Process(x);
		const float earlyOut = ap_21_22.Process(x);

		float ap_23_24_excursion = Excursion1 * 16.f;
		const float ap_23_24_out = ap_23_24.Process(earlyOut + d_63.Read(), ap_23_24_excursion);

		const float f_30_31_out = f_30_31.Process(d_30.Read());
		d_30.Write(ap_23_24_out);

		const float d_39_out = d_39.Read();
	
		d_39.Write(ap_31_33.Process(Decay * f_30_31_out));

		float ap_46_48_excursion = Excursion2 * 16.f;
		x = ap_46_48.Process(earlyOut + Decay * d_39_out,ap_46_48_excursion);
		const float d_54_out = d_54.Read();
		d_54.Write(x);
		x = f_54_55.Process(d_54_out);
		d_63.Write(ap_55_59.Process(x));

		x = 0.6f * d_54.ReadTap(266);
		x += 0.6f * d_54.ReadTap(2974);
		x -= 0.6f * ap_55_59.ReadTap(1913);
		x += 0.6f * d_63.ReadTap(1996);
		x -= 0.6f * d_30.ReadTap(1990);
		x -= 0.6f * ap_31_33.ReadTap(187);
		return x - (0.6f * d_39.ReadTap(1066));
	}
}

#endif // __SYNTH_EDITOR
