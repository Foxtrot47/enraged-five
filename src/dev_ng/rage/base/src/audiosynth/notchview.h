// 
// audiosynth/notchview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_NOTCHVIEW_H
#define SYNTH_NOTCHVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uilabel.h"
#include "uimenuview.h"
#include "uirangeview.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthNotchFilter;
	class synthNotchView : public synthModuleView
	{
	public:

		synthNotchView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();

		synthNotchFilter *GetFilterModule()
		{
			return (synthNotchFilter*)m_Module;
		}
	private:
		synthUIRangeView m_FreqRange;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_NOTCHVIEW_H

