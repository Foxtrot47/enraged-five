#include "fftmodule.h"

#if !SYNTH_MINIMAL_MODULES

#include "audioeffecttypes/fft.h"
#include "audiohardware/mixer.h"
#include "math/amath.h"
#include "system/memops.h"
#include "system/new.h"

#include "fftview.h"


namespace rage
{
	SYNTH_EDITOR_VIEW(synthFft);

	synthFft::synthFft()
	{
#if __SYNTH_EDITOR
		m_Outputs[0].Init(this, "Spectrum",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Outputs[0].SetDataBuffer(&m_OutBuffer);

		const u32 numIndices = 2 + (u32)Sqrtf((f32)(kMixBufNumSamples>>1));
		m_Ip = rage_new s32[numIndices];
		m_W = rage_new f32[kMixBufNumSamples>>1];

		sysMemSet(m_Ip, 0, sizeof(s32) * numIndices);
		sysMemSet(m_W, 0, sizeof(f32) * (kMixBufNumSamples>>1));
#endif
	}

	synthFft::~synthFft()
	{
#if __SYNTH_EDITOR
		delete[] m_Ip;
		delete[] m_W;
#endif
	}

	void synthFft::Synthesize()
	{
#if __SYNTH_EDITOR
		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_Outputs[0].SetStaticValue(0.f);
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_OutBuffer);
			sysMemCpy(m_Buffer.GetBuffer(), m_Inputs[0].GetDataBuffer()->GetBuffer(), sizeof(f32) * m_Buffer.GetSize());
			audFft::Rdft(m_Buffer.GetSize(), 1, m_Buffer.GetBuffer(), m_Ip, m_W);

			for(u32 i = 0 ; i < kMixBufNumSamples>>1; i++)
			{
				f32 s1 = m_Buffer.GetBuffer()[i*2];
				f32 s2 =  m_Buffer.GetBuffer()[i*2 + 1];
				f32 v = (Sqrtf(powf(s1,2.f)) + powf(s2,2.f))/(m_Buffer.GetSize()/2.f);
				m_Outputs[0].GetDataBuffer()->GetBuffer()[i*2] = m_Outputs[0].GetDataBuffer()->GetBuffer()[i*2+1] = v; 
			}
		}	
#endif // __SYNTH_EDITOR
	}
}
#endif
