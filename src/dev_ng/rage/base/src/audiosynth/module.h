
#ifndef SYNTH_MODULE_H
#define SYNTH_MODULE_H

#include "atl/array.h"
#include "modulefactory.h"
#include "moduleserializer.h"
#include "moduleview.h"
#include "pin.h"
#include "synthdefs.h"

#define SYNTH_MODULE_NAME(x) virtual const char *GetName() const { return x; }

namespace rage
{
class synthContext;
class synthModule
{
public:

	synthModule(const synthModuleId typeId) :
#if __SYNTH_EDITOR
	  m_View(NULL),
#endif
	  m_ProcessingStage(0),m_Index(~0U),m_CPUTime(0.f), m_TypeId(typeId)
	  {

	  }

	  virtual ~synthModule() 
	  {
#if __SYNTH_EDITOR
		  if(m_View) 
		  {
			  delete m_View;
			  m_View = NULL;
		  }
#endif // __SYNTH_EDITOR	
	  }

	virtual void GetInputs(synthPin *&pins, u32 &numPins) = 0;
	virtual void GetOutputs(synthPin *&pins, u32 &numPins) = 0;
	virtual synthPin &GetInput(const u32 index) = 0;
	virtual synthPin &GetOutput(const u32 index) = 0;
	virtual u32 GetMaxInputs() const = 0;
	virtual u32 GetMaxOutputs() const = 0;

	virtual void ResetProcessing() {}

	virtual void Synthesize() {};
	virtual void Synthesize(synthContext &UNUSED_PARAM(context))
	{
		Synthesize();
	}
	virtual const char *GetName() const = 0;

	// PURPOSE
	//	The module should use the supplied serializer to store/load any internal state that
	//	requires saving (min/max ranges, modes, etc).  Pin states/values are serialized automatically.
	virtual void SerializeState(synthModuleSerializer *UNUSED_PARAM(serializer)) {};


	synthModuleId GetTypeId() const { return m_TypeId; }

	void Serialize(synthModuleSerializer *serializer);


#if __SYNTH_EDITOR
	synthModuleView *GetView()
	{
		if(!HasView())
		{
			m_View = AllocateView();
		}
		return m_View;
	}

	bool HasView()
	{
		return (m_View != NULL);
	}
#endif // __SYNTH_EDITOR

	void SetProcessingStage(const s32 stage)
	{
		m_ProcessingStage = stage;
	}

	s32 GetProcessingStage() const
	{
		return m_ProcessingStage;
	}

	void SetCPUTime(const f32 timeMs)
	{
		m_CPUTime = timeMs;
	}

	f32 GetCPUTime() const
	{
		return m_CPUTime;
	}

	u32 GetIndex() const{ return m_Index; }
	void SetIndex(const u32 index) { m_Index = index; }

	// PURPOSE
	//	Returns the number of output sample frames required by this module instance
	// NOTES
	//	The default implementation returns 1 if any of the inputs are dynamic.  Modules that have
	//	other requirements (multiple outputs, dynamic output even when all inputs are static, etc) must
	//	provide an alternate implementation.
	virtual u32 ComputeNumOutputBuffersRequired();

	u32 ComputeNumReusableInputBuffers();

protected:
#if __SYNTH_EDITOR
	virtual synthModuleView *AllocateView();
	synthModuleView *m_View;
#endif // __SYNTH_EDITOR
	

private:
	u32 m_Index;
	s32 m_ProcessingStage;
	f32 m_CPUTime;
	synthModuleId m_TypeId;
};

template<const s32 numInputs, const s32 numOutputs, const synthModuleId moduleId> class synthModuleBase : public synthModule
{
public:

	enum {NumInputs = numInputs};
	enum {NumOutputs = numOutputs};
	static const synthModuleId TypeId = moduleId;

	synthModuleBase() : synthModule(moduleId) {}

	virtual ~synthModuleBase() {}
	

	void GetInputs(synthPin *&pins, u32 &numPins)
	{
		numPins = NumInputs;
		pins = &m_Inputs[0];
	}

	synthPin &GetInput(const u32 index)
	{
		Assert(NumInputs > 0 && index < NumInputs);
		return m_Inputs[index];
	}

	void GetOutputs(synthPin *&pins, u32 &numPins)
	{
		numPins = NumOutputs;
		pins = &m_Outputs[0];
	}

	synthPin &GetOutput(const u32 index)
	{
		Assert(NumOutputs > 0 && index < NumOutputs);
		return m_Outputs[index];
	}

	virtual u32 GetMaxInputs() const { return NumInputs; }
	virtual u32 GetMaxOutputs() const { return NumOutputs; }

protected:

	atRangeArray<synthPin, (NumInputs > 1 ? NumInputs : 1)> m_Inputs;
	atRangeArray<synthPin, (NumOutputs > 1 ? NumOutputs : 1)> m_Outputs;
};
	
}
#endif // SYNTH_MODULE_H
