

#ifndef SYNTH_READTIME_H
#define SYNTH_READTIME_H

#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthReadTime : public synthModuleBase<0,1,SYNTH_READTIME>
	{
	public:
		SYNTH_MODULE_NAME("ReadTime");
		
		synthReadTime();

		virtual void Synthesize(synthContext &context);

	private:

	};
}

#endif // SYNTH_READTIME_H


