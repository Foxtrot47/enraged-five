// 
// audiosynth/uihorizontallayout.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UIHORIZONTALLAYOUT_H
#define SYNTH_UIHORIZONTALLAYOUT_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uiview.h"
#include "vector/vector3.h"

namespace rage
{
	class synthUIHorizontalLayout : public synthUIView
	{

	public:

		synthUIHorizontalLayout();
		virtual ~synthUIHorizontalLayout();

		virtual void Update();
        virtual void PerformLayout();

		void Add(synthUIView *item);

		virtual bool StartDrag() const;
		virtual bool IsActive() const;

        enum Orientation
        {
            kHorizontal = 0,
            kVertical
        };
        void SetOrientation(Orientation orientation)
        {
            m_Orientation = orientation;
        }

        Orientation GetOrientation() const { return m_Orientation; }


        void SetPadding(const float padding) { m_Padding = padding; }
        float GetPadding() const { return m_Padding; }

    protected:
        void Render();

    private:
        Orientation m_Orientation;
        float m_Padding;
	};
}
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIHORIZONTALLAYOUT_H
