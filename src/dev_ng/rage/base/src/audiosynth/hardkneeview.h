// 
// audiosynth/hardkneeview.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_HARDKNEEVIEW_H
#define SYNTH_HARDKNEEVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uicurveview.h"

namespace rage
{
	class synthHardKnee;
	class synthHardKneeView : public synthModuleView, synthUICurveViewDataProvider
	{
	public:

		synthHardKneeView(synthModule *module);
		~synthHardKneeView();

		virtual void Update();
		virtual void Render();

		// synthUICurveViewDataProvider
		virtual void Evaluate(float *destBuffer, const u32 numSamples) const;

	protected:

		virtual u32 GetModuleHeight() const { return 3; }

	private:

		
		synthHardKnee *GetHardKnee() const { return (synthHardKnee*)m_Module; }
		
		synthUICurveView m_CurveView;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_HARDKNEEVIEW_H

