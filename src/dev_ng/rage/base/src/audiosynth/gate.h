#ifndef SYNTH_GATE_H
#define SYNTH_GATE_H

#include "synthdefs.h"

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum synthGateInputs
	{
		kGateInputSignal = 0,
		kGateInputThreshold,
		
		kGateNumInputs
	};

	enum synthGateOutputs
	{
		kGateOutputGatedSignal = 0,
		kGateOutputGateValue,
		
		kGateNumOutputs
	};
	class synthGate : public synthModuleBase<kGateNumInputs, kGateNumOutputs, SYNTH_GATE>
	{
	public:

		SYNTH_MODULE_NAME("Gate");

		synthGate();
		virtual void Synthesize();

		static void Process(float *inOut, const Vec::Vector_4V_In threshold, const u32 numSamples);
		static float Process(const float input, const float threshold);
		static void Process(float *inOut, const float *threshold, const u32 numSamples);
	private:

		synthSampleFrame m_GatedSignalBuffer;
		synthSampleFrame m_GateBuffer;
	};
}

#endif // SYNTH_GATE_H


