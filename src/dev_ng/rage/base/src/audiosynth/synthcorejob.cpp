// 
// audiosynth/synthcorejob.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_DISABLE_ASSERTS
#define AUD_DISABLE_ASSERTS 0
#endif

#if AUD_DISABLE_ASSERTS
#undef __ASSERT
#define __ASSERT 0
#undef ASSERT_ONLY
#define ASSERT_ONLY(x)


#undef Assert
#define Assert(x)
#undef Assertf
#define Assertf(x,...)
#undef FastAssert
#define FastAssert(x)
#undef AssertMsg
#define AssertMsg(x,msg)
#undef AssertVerify
#define AssertVerify(x) (x)
#undef Verifyf
#define Verifyf(x,fmt,...) (x)
#undef DebugAssert
#define DebugAssert(x)
#endif

#include "system/taskheader.h"

#if __SPU

#include "synthcore.h"
#include "synthcorejob.h"

#include "audiohardware/framebuffercache.h"
#include "audiohardware/mixer.h"
#include "audiohardware/syncsource.cpp"
#include "audiohardware/pcmsource.h"
#include "audiohardware/pcmsourcejob.h"
#include "audioengine/spuutil.h"

#include "math/amath.h"
#include "system/dma.h"

namespace rage 
{
	const void *synthSynthesizer::sm_UnloadingMetadataEndPtr = NULL;
	const void *synthSynthesizer::sm_UnloadingMetadataStartPtr = NULL;

	void SynthCoreJobEntry(sysTaskParameters &p)
	{
		InitScratchBuffer(p.Scratch.Data, p.Scratch.Size);

		sysDmaSmallGet(synthUtil::GetRandomSeedPtr(), (uint64_t)p.UserData[4].asPtr, 16, 5);

		synthSynthesizer::SetUnloadingRegion(p.UserData[8].asPtr, p.UserData[9].asPtr);

		// Allocate working buffers
		synthCore::InitClass();

		SYNTH_PROFILE_ONLY(sysDmaGet(&synthCore::GetProfileData(), (uint64_t)p.UserData[7].asPtr, sizeof(synthCore::synthProfileData), 5));

		sysDmaWait(1<<5);

		enum {kNumPcmBuffers = 16};
		ProcessPcmSourcePool<synthCorePcmSource, AUD_PCMSOURCE_SYNTHCORE>(p, kNumPcmBuffers);

		// Save random seed
		sysDmaSmallPut(synthUtil::GetRandomSeedPtr(), (uint64_t)p.UserData[4].asPtr, 16, 5);

		SYNTH_PROFILE_ONLY(sysDmaPut(&synthCore::GetProfileData(), (uint64_t)p.UserData[7].asPtr, sizeof(synthCore::synthProfileData), 5));

		sysDmaWait(1<<5);
	}
} // namespace rage

#endif // __SPU

using namespace rage;
void synthcorejob(sysTaskParameters &SPU_ONLY(p))
{
	SPU_ONLY(rage::SynthCoreJobEntry(p));
}
