//
// audiosynth/synthcoretest.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
//

#ifndef AUD_SYNTHCORETEST_H
#define AUD_SYNTHCORETEST_H

#if RSG_BANK

#include "math/amath.h"

namespace rage
{
class synthCorePcmSource;
struct CompiledSynth;
class synthCoreTest
{
public:

	bool Init(const u32 synthNameHash);
	bool ValidateOutput();
	void Shutdown();
	void Reset();

	const char *GetAssetName() const { return m_AssetName; }

	bool IsFinished() const;

	float GetTotalRuntimeMs() const;
	float GetAverageRuntimeMs() const
	{
		return GetTotalRuntimeMs() / static_cast<float>(Max<u32>(1,m_FrameCount));
	}

	u32 GetFrameCount() const { return m_FrameCount; }

	bool ValidateOutputBuffer(const float *outputBuffer);

private:

	bool InitSynth();
	

	synthCorePcmSource *m_Synth;
	const char *m_AssetName;
	u32 m_FrameCount;
	u32 m_RunCount;
	float m_RunTimeAccum;
	float m_RunningMean;
};

class synthCoreTestManager
{
public:

	bool RunTests();

private:

	enum {kMaxSimultaneousTests = 96};
	typedef atFixedArray<synthCoreTest, kMaxSimultaneousTests> ActiveTestList;
	ActiveTestList m_Tests;
};

}

#endif // RSG_BANK
#endif // AUD_SYNTHCORETEST_H


