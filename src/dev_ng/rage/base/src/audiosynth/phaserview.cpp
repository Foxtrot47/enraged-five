// 
// audiosynth/denormcancelview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "phaserview.h"
#include "phaser.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"

namespace rage
{

	synthPhaserView::synthPhaserView(synthModule *module) : synthModuleView(module)
	{
		m_FreqNoteSwitch.AddItem("NoteInput");
		m_FreqNoteSwitch.AddItem("FreqInput");
		AddComponent(&m_FreqNoteSwitch);
		m_FreqNoteSwitch.SetCurrentIndex(0);
		GetPhaser()->SetInputType(0);

		m_TapSwitch.AddItem("1");
		m_TapSwitch.AddItem("2");
		m_TapSwitch.AddItem("3");
		m_TapSwitch.AddItem("4");
		m_TapSwitch.AddItem("5");
		m_TapSwitch.AddItem("6");
		m_TapSwitch.AddItem("7");
		AddComponent(&m_TapSwitch);
		m_TapSwitch.SetCurrentIndex(0);
		GetPhaser()->SetTap(0);
	}

	void synthPhaserView::Update()
	{
		Matrix34 mat = GetMatrix();
		Vector3 offset = mat.b * 0.5f;
		mat.d += offset;

		m_FreqNoteSwitch.SetMatrix(mat);
		m_FreqNoteSwitch.SetAlpha(GetHoverFade());
		m_FreqNoteSwitch.SetShouldDraw(!IsMinimised());
		GetPhaser()->SetInputType (m_FreqNoteSwitch.GetCurrentIndex());

		mat.d += mat.b * offset;
		m_TapSwitch.SetMatrix(mat);
		m_TapSwitch.SetAlpha(GetHoverFade());
		m_TapSwitch.SetShouldDraw(!IsMinimised());
		GetPhaser()->SetTap(m_TapSwitch.GetCurrentIndex());
		
		synthModuleView::Update();
	}

	void synthPhaserView::Render()
	{
		synthModuleView::Render();
	}

	bool synthPhaserView::StartDrag() const
	{
		if(m_TapSwitch.IsUnderMouse() || m_TapSwitch.IsShowingMenu())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthPhaserView::IsActive() const
	{
		return (IsUnderMouse() || m_TapSwitch.IsActive());
	}
}

#endif // __SYNTH_EDITOR
