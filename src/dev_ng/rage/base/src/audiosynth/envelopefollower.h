
#ifndef SYNTH_ENVELOPEFOLLOWER_H
#define SYNTH_ENVELOPEFOLLOWER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{
	enum 
	{
		kEnvelopeFollowerSignal = 0,
		kEnvelopeFollowerAttack,
		kEnvelopeFollowerRelease,
		
		kEnvelopeFollowerNumInputs
	};
#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
	class synthEnvelopeFollower : public synthModuleBase<kEnvelopeFollowerNumInputs, 1, SYNTH_ENVELOPEFOLLOWER>
	{
	public:
		SYNTH_MODULE_NAME("EnvelopeFollower");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthEnvelopeFollower();	
		virtual void Synthesize();
		virtual void ResetProcessing();

		virtual void SerializeState(synthModuleSerializer *serializer);	

		static void Process(float *RESTRICT const inOutBuffer, const float attack, const float release, Vec::Vector_4V_InOut state, const u32 numSamples);
		static float Process(const float input, const float attack, const float release, Vec::Vector_4V_InOut state);
	
		enum EnvelopeFollowerMode
		{
			Normal = 0,
			ForceDynamic,
		};

		void SetMode(const EnvelopeFollowerMode mode) { m_Mode = mode;}
		EnvelopeFollowerMode GetMode() const { return m_Mode; }

	private:

		Vec::Vector_4V m_State;
		synthSampleFrame m_Buffer;
		f32 m_Envelope;
		EnvelopeFollowerMode m_Mode;
	};
#if __WIN32
#pragma warning(pop)
#endif
}
#endif
#endif // SYNTH_ENVELOPEFOLLOWER_H


