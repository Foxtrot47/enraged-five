// 
// audiosynth/moduleserializer.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_MODULESERIALIZER_H
#define SYNTH_MODULESERIALIZER_H

#include "synthdefs.h"
#include "synthobjects.h"

namespace rage
{
	class synthPin;
	class fiStream;

	class synthBinaryModuleReader
	{
	public:
		void Read(const ModularSynth::tModule *module);

		u32 GetNumFields() const { return m_ModuleData->numFieldValues; }
		const ModularSynth::tModule::tFieldValue *GetFields() const { return &m_ModuleData->FieldValue[0]; }
		
		u32 GetNumInputPins() const { return m_NumInputPins; }
		const ModularSynth::tModule::tInputPin *GetInputPins() const { return m_InputPins; }

		u32 GetNumOutputPins() const { return m_NumOutputPins; }
		const ModularSynth::tModule::tOutputPin *GetOutputPins() const { return m_OutputPins; }

		const ModularSynth::tModule *GetModuleData() const { return m_ModuleData; }

		u32 GetSizeBytes() const;

	private:
		const ModularSynth::tModule *m_ModuleData;		
		const ModularSynth::tModule::tInputPin *m_InputPins;
		const ModularSynth::tModule::tOutputPin *m_OutputPins;
		u8 m_NumInputPins;
		u8 m_NumOutputPins;
	};
	
	class synthBinaryReader
	{
	public:
		synthBinaryReader(const ModularSynth *const instanceData);

		u32 GetNumModules();
		void GetNextModule(synthBinaryModuleReader &moduleReader);

		u32 GetNumExposedVariables();
		const ModularSynth::tExposedVariable *GetNextVariable();

		u32 GetNumGroups();
		const ModularSynth::tModuleGroup *GetNextGroup();

		u32 GetNumComments();
		const ModularSynth::tComments *GetNextComment();

		u32 GetNumCameras();
		const ModularSynth::tCameraPosition *GetNextCamera();

		void Reset();

	private:

		const ModularSynth *m_InstanceData;
		const u8 *m_CurrentPtr;
		u32 m_CurrentIndex;
		u32 m_CurrentCount;
		enum
		{
			kModules = 0,
			kGroups,
			kVariables,
			kComments,
			kCameras
		}m_State;
	};

	class synthModuleSerializer
	{
	public:
		virtual ~synthModuleSerializer(){}

		virtual void StartModule(const char *typeName, u32 &type) = 0;
		virtual void FinishedModule() = 0;

		virtual void StartInputs() = 0;
		virtual void StartOutputs() = 0;
		virtual void SerializePin(synthPin *pin) = 0;
		virtual void FinishedInputs() = 0;
		virtual void FinishedOutputs() = 0;
		
		virtual void StartFields() = 0;

		#define SerializeEnumField(x,y) SerializeField(x,(s32&)y)
		virtual void SerializeField(const char *fieldName, u32 &field) = 0;
		virtual void SerializeField(const char *fieldName, s32 &field) = 0;
		virtual void SerializeField(const char *fieldName, f32 &field) = 0;
		virtual void SerializeField(const char *fieldName, bool &field) = 0;

		virtual void FinishedFields() = 0;

		virtual bool IsLoading() const = 0;
	
	protected:
		enum synthSerializerMode
		{
			StartSerializingInputs,
			SerializingInputs,
			FinishedSerializingInputs,
			
			StartSerializingOutputs,
			SerializingOutputs,
			FinishedSerializingOutputs,
		
			StartSerializingFields,
			SerializingFields,
			FinishedSerializingFields,
		};

	};
#if __SYNTH_EDITOR
	class synthModuleXmlOutputSerializer : public synthModuleSerializer
	{
	public:
		synthModuleXmlOutputSerializer(fiStream *stream);

		virtual void StartModule(const char *typeName, u32 &type);
		virtual void FinishedModule();

		virtual void StartInputs();
		virtual void StartOutputs();
		virtual void SerializePin(synthPin *pin);
		virtual void FinishedInputs();
		virtual void FinishedOutputs();

		virtual void StartFields();

		virtual void SerializeField(const char *fieldName, u32 &field);
		virtual void SerializeField(const char *fieldName, s32 &field);
		virtual void SerializeField(const char *fieldName, f32 &field);
		virtual void SerializeField(const char *fieldName, bool &field);

		virtual void FinishedFields();

		virtual bool IsLoading() const
		{
			return false;
		}
	
		void WriteElementValue(const char *elementName, const char *comment, const f32 val);
		void WriteElementValue(const char *elementName, const char *comment, const u32 val);
		void WriteElementValue(const char *elementName, const char *comment, const s32 val);
	private:

		void WriteElementStart(const char *elementName);
		void WriteElementEnd(const char *elementName);
		
		

		fiStream *m_Stream;
		synthSerializerMode m_Mode;
		u32 m_CurrentTabLevel;
	};
#endif // __SYNTH_EDITOR

	class synthModuleBinaryMemSerializer : public synthModuleSerializer
	{
	public:
		synthModuleBinaryMemSerializer();

		virtual void StartModule(const char *typeName, u32 &type);
		virtual void FinishedModule();

		virtual void StartInputs();
		virtual void StartOutputs();
		virtual void SerializePin(synthPin *pin);
		virtual void FinishedInputs();
		virtual void FinishedOutputs();

		virtual void StartFields();

		virtual void SerializeField(const char *fieldName, u32 &field);
		virtual void SerializeField(const char *fieldName, s32 &field);
		virtual void SerializeField(const char *fieldName, f32 &field);
		virtual void SerializeField(const char *fieldName, bool &field);

		virtual void FinishedFields();

		virtual bool IsLoading() const
		{
			return m_IsLoading;
		}

		void ResetPointer(const bool isLoading)
		{
			m_IsLoading = isLoading;
			m_Ptr = m_Storage;
		}

		void SetPointerForLoad(u8 *ptr)
		{
			m_IsLoading = true;
			m_Ptr = ptr;
		}
	private:
	
		void SerializeField(u32 &field);
		void SerializeField(f32 &field);

		u8 m_Storage[256];
		u8 *m_Ptr;
		bool m_IsLoading;
	};
}//namespace rage

#endif // SYNTH_MODULESERIALIZER_H
