// 
// audiosynth/commentbox.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOSYNTH_COMMENTBOX_H 
#define AUDIOSYNTH_COMMENTBOX_H 

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "uiview.h"
#include "uitextbox.h"

namespace rage {

	class synthCommentBox : public synthUIView
	{
	public:
		synthCommentBox();
		virtual ~synthCommentBox();

		const char *GetText() const { return m_TextBox.GetString(); }
		void SetText(const char *text) { m_TextBox.SetString(text); }

		Color32 GetColor() const { return m_Color; }
		void SetColor(const Color32 color) { m_Color = color; }

		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;
		virtual void Update();
		virtual void Render();
		virtual bool StartDrag() const;
		virtual bool IsActive() const { return false; }
		virtual bool IsGroup() const { return false; }

		bool IsEditing() const { return m_TextBox.IsEditing(); }
		void Edit() { m_TextBox.Edit(); }

	private:
		Color32 m_Color;
		synthUITextBox m_TextBox;
	};
} // namespace rage

#endif // __SYNTH_EDITOR
#endif // AUDIOSYNTH_COMMENTBOX_H 
