// 
// audiosynth/uimenuview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UIMENUVIEW_H
#define SYNTH_UIMENUVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uiview.h"

#include "atl/array.h"
#include "vector/vector3.h"


namespace rage
{
	class synthUIMenuItem;

	class synthUIMenuView : public synthUIView
	{
	public:

		synthUIMenuView();
		~synthUIMenuView();

		void AddItem(const char *title, u32 tag);
		void AddSubMenu(const char *title, synthUIMenuView *subMenu, Color32 color);

		virtual void Update();
		virtual void Render();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		void SortAlphabetically();

		bool IsActive() const
		{
			return false;
		}

		virtual bool StartDrag() const
		{
			return false;
		}

		void ClearSelection();

		bool HasSelection() const
		{
			return m_HasSelection;
		}

		u32 GetSelectedTag() const
		{
			return m_SelectedTag;
		}

		void ResetItems();

		void SetTextSize(const f32 size);

		void SetTitleForTag(const u32 tag, const char *title);

	private:
		atArray<synthUIMenuItem	*> m_Items;
		synthUIMenuView *m_ActiveSubMenu;
		u32 m_SelectedTag;
		bool m_HasSelection;
	};
}
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIMENUVIEW_H
