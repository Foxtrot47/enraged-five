
#ifndef SYNTH_FFT_H
#define SYNTH_FFT_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthModuleView;
	class synthFft : public synthModuleBase<1,1,SYNTH_FFT>
	{
	public:
		SYNTH_MODULE_NAME("Spectrum");

		synthFft();
		virtual ~synthFft();

		virtual void Synthesize();

	private:
		SYNTH_CUSTOM_MODULE_VIEW;

#if __SYNTH_EDITOR
		synthSampleFrame m_Buffer;
		synthSampleFrame m_OutBuffer;

		s32 *m_Ip;
		f32 *m_W;
#endif
	};
}
#endif
#endif // SYNTH_FFT_H


