// 
// audiosynth/envelopefollowerview.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_ENVELOPEFOLLOWERVIEW_H
#define SYNTH_ENVELOPEFOLLOWERVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uidropdownlist.h"

namespace rage
{	
	class synthEnvelopeFollower;
	class synthEnvelopeFollowerView : public synthModuleView
	{
	public:

		synthEnvelopeFollowerView(synthModule *module);
		~synthEnvelopeFollowerView();

		virtual void Update();

	protected:

		virtual bool StartDrag() const;

	private:

		synthEnvelopeFollower *GetEnvelopeFollower() const { return (synthEnvelopeFollower*)m_Module; }
		synthUIDropDownList m_Mode;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_ENVELOPEFOLLOWERVIEW_H

