// 
// audiosynth/uiplane.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "commentbox.h"
#include "uicomponent.h"
#include "uiview.h"
#include "moduleview.h"
#include "uiplane.h"
#include "pin.h"
#include "pinview.h"
#include "synthesizerview.h"
#include "synthobjects.h"
#include "uimenuview.h"
#include "uitextbox.h"
#include "inputsmoother.h"

#include "modulefactory.h"
#include "modulegroup.h"
#include "moduleserializer.h"

#include "audioengine/engineutil.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mouse.h"
#include "system/param.h"
#include "vector/geometry.h"
#include "grcore/viewport.h"

#if __WIN32PC
#include "system/xtl.h"
#endif
NOSTRIP_XPARAM(fullscreen);
namespace rage
{
	PARAM(experimentalmodules, "[AMP] Enable experimental modules");

	extern f32 g_MenuItemScaling;
	extern Color32 g_SignalWireColour;
	extern Color32 g_NormalizedWireColour;

	synthUIPlane *synthUIPlane::sm_MasterPlane = NULL;

	enum synthUIPlaneMenuOptions
	{
		MENU_OPTION_LOAD_PATCH = 10000,
		MENU_OPTION_NEW_PATCH,
		MENU_OPTION_SAVE_COPY,
		MENU_OPTION_SAVE_AS,
		MENU_OPTION_IMPORT_GROUP,
		MENU_OPTION_IMPORT_PATCH,
		MENU_OPTION_COMMENT,
		MENU_OPTION_APPLY_PRESET,
		MENU_OPTION_SAVE_PRESET,
		MENU_OPTION_TOGGLE_EXPORT,
		MENU_OPTION_TOGGLE_SAVING,

		MENU_OPTION_COMPRESSOR_MACRO,
	};

	
	bool synthUIPlane::sm_IsKeyboardLocked = false;

#define EXPERIMENTAL_MODULE(x) if(enableExperimentalModules) { x; }

	synthUIPlane::synthUIPlane()
	{
		m_IsActive = false;
		m_IsMouseFixed = false;
		m_AuditioningPin = NULL;
		m_CurrentlyBuildingGroup = NULL;
		m_HadLeftClick = m_HadLeftDoubleClick = m_HadRightClick = false;
		m_LastLeftClickTime = 0;
		m_IsInteractionDisabled = false;
		m_DraggingView = NULL;
		m_ConnectionP0 = NULL;
		m_CreationPosition.Zero();
		m_InputBoxState = InputBox_Idle;
		m_InputBox.SetShouldDraw(false);
		m_ImportGroupPatchName[0] = 0;
		m_ImportGroupListMenu.SetShouldDraw(false);

		// set up context menu
		m_ContextMenu.SetShouldDraw(false);

		const bool enableExperimentalModules = PARAM_experimentalmodules.Get();
		
		m_GenerateSubMenu.AddItem("AW_Noise",SYNTH_AWNOISEGENERATOR);
		m_GenerateSubMenu.AddItem("Digital Oscillator",SYNTH_DIGITAL_OSCILLATOR);
		EXPERIMENTAL_MODULE(m_GenerateSubMenu.AddItem("Analog Oscillator",SYNTH_ANALOGUE_OSCILLATOR));
		m_GenerateSubMenu.AddItem("EnvelopeGen", SYNTH_ENVELOPEGENERATOR);
		m_GenerateSubMenu.AddItem("Noise", SYNTH_NOISEGENERATOR);
		EXPERIMENTAL_MODULE(m_GenerateSubMenu.AddItem("PWM Noise", SYNTH_PWMNOISEGENERATOR));
		m_GenerateSubMenu.AddItem("Randomizer", SYNTH_RANDOMIZER);
		m_GenerateSubMenu.AddItem("ReadTime", SYNTH_READTIME);
		m_GenerateSubMenu.AddItem("Sample & Hold",SYNTH_SAMPLEANDHOLD);
		EXPERIMENTAL_MODULE(m_GenerateSubMenu.AddItem("Sample Player",SYNTH_SAMPLEPLAYER));
		m_GenerateSubMenu.AddItem("Sequencer", SYNTH_SEQUENCER);
		m_GenerateSubMenu.AddItem("TimedTrigger",SYNTH_TIMEDTRIGGER);
		m_GenerateSubMenu.AddItem("Trigger",SYNTH_TRIGGER);
		m_GenerateSubMenu.SetShouldDraw(false);

		
		m_TransformSubMenu.AddItem("Clipper",SYNTH_CLIPPER);
		m_TransformSubMenu.AddItem("Compressor", MENU_OPTION_COMPRESSOR_MACRO);
		m_TransformSubMenu.AddItem("Compressor_EG",SYNTH_COMPRESSOR);
		m_TransformSubMenu.AddItem("Converter",SYNTH_CONVERTER);
		m_TransformSubMenu.AddItem("Decimator",SYNTH_DECIMATOR);
		m_TransformSubMenu.AddItem("Delay Line",SYNTH_DELAYLINE);
		m_TransformSubMenu.AddItem("EnvelopeFollower", SYNTH_ENVELOPEFOLLOWER);
		m_TransformSubMenu.AddItem("Gate",SYNTH_GATE);
		m_TransformSubMenu.AddItem("Hard Knee",SYNTH_HARDKNEE);
		m_TransformSubMenu.AddItem("Inverter", SYNTH_INVERTER);
		m_TransformSubMenu.AddItem("MicroDelay",SYNTH_SMALLDELAY);
		m_TransformSubMenu.AddItem("Multitap Delay",SYNTH_MULTITAPDELAY);
		m_TransformSubMenu.AddItem("Note2Freq", SYNTH_NOTE2FREQ);
		EXPERIMENTAL_MODULE(m_TransformSubMenu.AddItem("Phaser",SYNTH_PHASER));
		EXPERIMENTAL_MODULE(m_TransformSubMenu.AddItem("PolyCurve",SYNTH_POLYCURVE));
		m_TransformSubMenu.AddItem("Rectifier",SYNTH_RECTIFIER);
		m_TransformSubMenu.AddItem("Rescaler",SYNTH_RESCALER);
		EXPERIMENTAL_MODULE(m_TransformSubMenu.AddItem("Tremolo",SYNTH_TREMOLO));
				
		m_TransformSubMenu.SetShouldDraw(false);

		m_FilterSubMenu.AddItem("1Pole Filter",SYNTH_1POLE);
		m_FilterSubMenu.AddItem("AW_Filter",SYNTH_AWFILTER);
		EXPERIMENTAL_MODULE(m_FilterSubMenu.AddItem("Allpass",SYNTH_ALLPASS));
		m_FilterSubMenu.AddItem("Biquad Filter",SYNTH_BIQUADFILTER);
		m_FilterSubMenu.AddItem("Frame Smoother",SYNTH_FRAMESMOOTHER);
		EXPERIMENTAL_MODULE(m_FilterSubMenu.AddItem("Notch",SYNTH_NOTCH));
		EXPERIMENTAL_MODULE(m_FilterSubMenu.AddItem("Pinking Filter",SYNTH_PINKINGFILTER));
		//EXPERIMENTAL_MODULE(m_FilterSubMenu.AddItem("Comb Filter",SYNTH_FVCOMBFILTER));
		EXPERIMENTAL_MODULE(m_FilterSubMenu.AddItem("Allpass Filter",SYNTH_FVALLPASSFILTER));
		m_FilterSubMenu.AddItem("State Variable",SYNTH_SVFILTER);
		m_FilterSubMenu.SetShouldDraw(false);

		m_MathSubMenu.AddItem("Abs",SYNTH_ABS);
		m_MathSubMenu.AddItem("Add",SYNTH_ADDER);
		m_MathSubMenu.AddItem("Ceiling",SYNTH_CEIL);
		m_MathSubMenu.AddItem("Counter",SYNTH_COUNTER);
		m_MathSubMenu.AddItem("Divide",SYNTH_DIVIDER);
		EXPERIMENTAL_MODULE(m_MathSubMenu.AddItem("Exponential",SYNTH_EXPONENTIAL));
		m_MathSubMenu.AddItem("Floor",SYNTH_FLOOR);
		m_MathSubMenu.AddItem("Max",SYNTH_MAX);
		m_MathSubMenu.AddItem("Min",SYNTH_MIN);
		m_MathSubMenu.AddItem("Modulus",SYNTH_MODULUS);	
		m_MathSubMenu.AddItem("Multiply",SYNTH_MULTIPLIER);
		m_MathSubMenu.AddItem("Power",SYNTH_POWER);
		m_MathSubMenu.AddItem("Round",SYNTH_ROUND);
		m_MathSubMenu.AddItem("Sign",SYNTH_SIGN);
		m_MathSubMenu.AddItem("Subtract",SYNTH_SUBTRACTER);		
		m_MathSubMenu.SetShouldDraw(false);

		m_DebugSubMenu.AddItem("ChartRecorder", SYNTH_CHARTRECORDER);
		m_DebugSubMenu.AddItem("Comment", MENU_OPTION_COMMENT);
		m_DebugSubMenu.AddItem("Spectrum",SYNTH_FFT);		
		m_DebugSubMenu.SetShouldDraw(false);

		m_IOSubMenu.AddItem("Audio In",SYNTH_AUDIOINPUT);
		m_IOSubMenu.AddItem("Audio Out",SYNTH_AUDIOOUTPUT);
		m_IOSubMenu.AddItem("Constant Value", SYNTH_CONSTANT);
		m_IOSubMenu.AddItem("Midi In", SYNTH_MIDIIN);
		m_IOSubMenu.AddItem("Midi CC", SYNTH_MIDICCIN);
		m_IOSubMenu.AddItem("Variable In", SYNTH_VARIABLEINPUT);
		EXPERIMENTAL_MODULE(m_IOSubMenu.AddItem("Variable Out", SYNTH_VARIABLEINPUT));
		m_IOSubMenu.SetShouldDraw(false);

		m_RoutingSubMenu.AddItem("Junction",SYNTH_JUNCTIONPIN);
		m_RoutingSubMenu.AddItem("Feedback", SYNTH_FEEDBACKPIN);
		m_RoutingSubMenu.AddItem("Mixer",SYNTH_MIXER);
		m_RoutingSubMenu.AddItem("ReverseSwitch",SYNTH_REVERSE_SWITCH);
		m_RoutingSubMenu.AddItem("Switch",SYNTH_SWITCH);
		m_RoutingSubMenu.SetShouldDraw(false);

		m_LoadSaveMenu.AddItem("New Patch",MENU_OPTION_NEW_PATCH);
		m_LoadSaveMenu.AddItem("Load Patch",MENU_OPTION_LOAD_PATCH);
		m_LoadSaveMenu.AddItem("Save As...",MENU_OPTION_SAVE_AS);
		m_LoadSaveMenu.AddItem("Save Copy...",MENU_OPTION_SAVE_COPY);
		m_LoadSaveMenu.AddItem("Import Group",MENU_OPTION_IMPORT_GROUP);
		m_LoadSaveMenu.AddItem("Import Patch",MENU_OPTION_IMPORT_PATCH);
		m_LoadSaveMenu.AddItem("Apply Preset",MENU_OPTION_APPLY_PRESET);
		m_LoadSaveMenu.AddItem("Save Preset",MENU_OPTION_SAVE_PRESET);
		m_LoadSaveMenu.AddItem("Toggle Export Flag",MENU_OPTION_TOGGLE_EXPORT);
		m_LoadSaveMenu.AddItem("Toggle Saving",MENU_OPTION_TOGGLE_SAVING);
		m_LoadSaveMenu.SetShouldDraw(false);

		m_ReverbMenu.AddItem("Dattorro", SYNTH_DATTORROREVERB);
		m_ReverbMenu.AddItem("Freeverb", SYNTH_FREEVERB);
		m_ReverbMenu.AddItem("MVerb", SYNTH_MVERB);
		m_ReverbMenu.AddItem("Progenitor", SYNTH_PROGENITORREVERB);
		m_ReverbMenu.SetShouldDraw(false);

		Color32 menuColour(1.f, 0.3f, 0.1f);
		
		m_ContextMenu.AddSubMenu("I/O",&m_IOSubMenu, menuColour);
		m_ContextMenu.AddSubMenu("Debug",&m_DebugSubMenu, menuColour);
		m_ContextMenu.AddSubMenu("Routing",&m_RoutingSubMenu, menuColour);		
		m_ContextMenu.AddSubMenu("Generate",&m_GenerateSubMenu, menuColour);
		m_ContextMenu.AddSubMenu("Transform",&m_TransformSubMenu, menuColour);
		m_ContextMenu.AddSubMenu("Filter",&m_FilterSubMenu, menuColour);
		m_ContextMenu.AddSubMenu("Reverb",&m_ReverbMenu, menuColour);
		m_ContextMenu.AddSubMenu("Math",&m_MathSubMenu, menuColour);		
		m_ContextMenu.AddSubMenu("Import/Export", &m_LoadSaveMenu, menuColour);

		m_MousePosition = GetMatrix().d;

		m_DisableDragGroupRemoval = false;
	}

	void synthUIPlane::Init(atArray<synthModule*> *modules, synthSynthesizer *synth)
	{
		m_Modules = modules;
		m_Synthesizer = synth;
	}

	void synthUIPlane::Shutdown()
	{
		for(atArray<synthUIViewGroup*>::iterator iter = m_Groups.begin(); iter != m_Groups.end(); iter++)
		{
			delete *iter;
		}
		for(atArray<synthCommentBox*>::iterator iter = m_CommentBoxes.begin(); iter != m_CommentBoxes.end(); iter++)
		{
			delete *iter;
		}

		/*for(atArray<synthModule*>::iterator iter = m_Modules->begin(); iter != m_Modules->end(); iter++)
		{
			if((*iter)->GetTypeId() != SYNTH_AUDIOOUTPUT)
			{
				delete (*iter)->GetView();
			}
		}*/
	}

	void synthUIPlane::ResetMouse()
	{
		m_MousePosition = GetMatrix().d;
	}

	synthUIPlane::~synthUIPlane()
	{

	}

	Matrix34 synthUIPlane::ComputeCameraMatrix()
	{
		Matrix34 mat;
		const f32 distance = 25.f;
		Vector3 camPos = GetMatrix().d - GetMatrix().c * distance; 
		Vector3 up =  GetMatrix().b;
		static f32 angle = 35.f;
		static char axis = 'x';
		up.RotateAboutAxis(DtoR * angle, axis);
		mat.LookAt(camPos, GetMatrix().d, up);
		return mat;
	}

	bool synthUIPlane::StartConnection(synthPin *p0)
	{
		if(m_ConnectionP0 == NULL)
		{
			m_ConnectionP0 = p0;
			return true;
		}
		return false;
	}

	bool synthUIPlane::EndConnection(synthPin *p1)
	{
		if(m_ConnectionP0 == NULL || p1 == NULL || m_ConnectionP0 == p1)
		{
			return false;
		}
		if(m_ConnectionP0->GetParentModule() == p1->GetParentModule())
		{
			if(m_ConnectionP0->GetParentModule()->GetTypeId() != SYNTH_JUNCTIONPIN)
			{
				CancelConnection();
			}
			return false;
		}
		if(p1->IsOutput())
		{
			// special case; if p1 is an output, and p0 is an output pin belonging to a junction,
			//connect from the other output to the junction input
			if(m_ConnectionP0->GetParentModule()->GetTypeId() == SYNTH_JUNCTIONPIN)
			{
				synthModule *junction = m_ConnectionP0->GetParentModule();
				if(!junction->GetInput(0).IsConnected())
				{
					junction->GetInput(0).Connect(*p1);
					CancelConnection();
					return true;
				}
			}
			CancelConnection();
			return false;
		}
		p1->Connect(*m_ConnectionP0);
		// Leave a cable connected to the mouse if the duplicate key is held
		if(!IsDuplicateKeyDown())
		{
			m_ConnectionP0 = NULL;
		}
		m_Synthesizer->ComputeProcessingGraph();
		return true;
	}

	void synthUIPlane::CancelConnection()
	{
		m_ConnectionP0 = NULL;
	}

	void synthUIPlane::Disconnect(synthPin *p0)
	{
		synthPin *otherPin = p0->GetOtherPin();
		p0->Disconnect();
		m_Synthesizer->ComputeProcessingGraph();
		if(!m_ConnectionP0 && otherPin)
		{
			StartConnection(otherPin);
		}

		// Special handling when disconnecting a 'StopTrigger' on an output module
		if(p0->GetParentModule()->GetTypeId() == SYNTH_AUDIOOUTPUT && 
			atStringHash(p0->GetName()) == ATSTRINGHASH("StopTrigger", 0x9D5A4A31))
		{
			p0->GetView()->SetNormalizedValue(0.f);
		}
	}

	synthUIViewGroup *synthUIPlane::CreateGroup(const char *groupName)
	{
		synthUIViewGroup *group = rage_new synthUIViewGroup();
		AddGroup(group);
		group->SetName(groupName);
		return group;
	}

	synthUIViewGroup *synthUIPlane::CreateConstantFromInput(synthPin &inputPin, const Matrix34 &mat)
	{
		synthUIViewGroup *group = CreateGroup(inputPin.GetName());

		if(inputPin.IsConnected())
		{
			inputPin.Disconnect();
		}

		synthModule *constant = synthModuleFactory::CreateModule(SYNTH_CONSTANT);
		constant->GetInput(0).SetStaticValue(inputPin.GetStaticValue());
		inputPin.Connect(constant->GetOutput(0));
		

		m_Synthesizer->AddModule(constant);
		constant->GetView()->SetMatrix(mat);
		constant->GetView()->Update();

		group->Add(constant->GetView());
		return group;

	}

	void synthUIPlane::CreateCompressorMacro()
	{
		synthUIViewGroup *compressorGroup = CreateGroup("Compressor");
		
		if(m_ActiveGroupForModuleCreation)
		{
			m_ActiveGroupForModuleCreation->Add(compressorGroup);
		}
		
		Matrix34 mat = GetMatrix();

		synthModule *junction = synthModuleFactory::CreateModule(SYNTH_JUNCTIONPIN);
		mat.d = m_CreationPosition;
		mat.d += mat.a * 18.f;
		mat.d += mat.b * 2.f;
		junction->GetView()->SetMatrix(mat);
		m_Synthesizer->AddModule(junction);
		junction->GetView()->Update();

		synthUIViewGroup *inputGroup = CreateGroup("Input");
		compressorGroup->Add(inputGroup);
		inputGroup->Add(junction->GetView());

		synthModule *compressorEG = synthModuleFactory::CreateModule(SYNTH_COMPRESSOR);	
		mat.d = m_CreationPosition;
		mat.d -= mat.b * 5.f;
		compressorEG->GetView()->SetMatrix(mat);
		compressorGroup->Add(compressorEG->GetView());
		m_Synthesizer->AddModule(compressorEG);
		compressorEG->GetView()->Update();

		synthPin *pins;
		u32 numInputs = 0;
		compressorEG->GetInputs(pins, numInputs);

		synthModule *mult = synthModuleFactory::CreateModule(SYNTH_MULTIPLIER);	
		mat.d = m_CreationPosition;
		mat.d -= mat.b * 20.f;
		mult->GetView()->SetMatrix(mat);
		compressorGroup->Add(mult->GetView());
		m_Synthesizer->AddModule(mult);
		mult->GetView()->Update();

		const float constantVertSpacing = 5.f;
		mat.d = m_CreationPosition;
		mat.d += mat.a * 15.f;
		mat.d -= mat.b * constantVertSpacing;

		synthUIViewGroup *controlGroup = CreateGroup("Controls");


		for(u32 i = 1; i < numInputs; i++)
		{
			synthUIViewGroup *group = CreateConstantFromInput(pins[i], mat);
			controlGroup->Add(group);
			mat.d -= mat.b * constantVertSpacing;
		}


		synthUIViewGroup *makupGainGroup = CreateConstantFromInput(mult->GetInput(2), mat);
		makupGainGroup->SetName("Make-up Gain");
		controlGroup->Add(makupGainGroup);

		compressorGroup->Add(controlGroup);		

		compressorEG->GetInput(0).Connect(junction->GetOutput(0));
		mult->GetInput(1).Connect(compressorEG->GetOutput(0));
		mult->GetInput(0).Connect(junction->GetOutput(0));
		
		junction = synthModuleFactory::CreateModule(SYNTH_JUNCTIONPIN);
		mat.d = m_CreationPosition;
		mat.d -= mat.a * 14.f;
		mat.d -= mat.b * 10.f;
		junction->GetView()->SetMatrix(mat);
		m_Synthesizer->AddModule(junction);
		junction->GetView()->Update();

		junction->GetInput(0).Connect(mult->GetOutput(0));

		synthUIViewGroup *outputGroup = CreateGroup("Output");
		compressorGroup->Add(outputGroup);
		outputGroup->Add(junction->GetView());

		m_DraggingView = compressorGroup;
		m_DragOffset.Zero();
	}

	void synthUIPlane::CreateJunctionAtMouse()
	{
		synthModule *junction = synthModuleFactory::CreateModule(SYNTH_JUNCTIONPIN);
		Assert(junction);

		Matrix34 mat = GetMatrix();
		mat.d = m_MousePosition;
		junction->GetView()->SetMatrix(mat);

		m_ModulesToAdd.PushAndGrow(junction);

		m_DraggingView = junction->GetView();
		m_DragOffset.Zero();

		junction->GetView()->Update();

		if(m_ConnectionP0)
		{
			EndConnection(&junction->GetInput(0));
		}

		StartConnection(&junction->GetOutput(0));

		synthUIViewGroup *group = FindGroupByPosition(m_MousePosition);
		if(group)
		{
			group->Add(junction->GetView());
		}
	}

	synthModule *synthUIPlane::CreateJunctionFromInput(synthPin *inputPin)
	{
		Assert(!inputPin->IsOutput());
		Assert(inputPin->IsConnected());

		synthModule *junction = synthModuleFactory::CreateModule(SYNTH_JUNCTIONPIN);
		Assert(junction);
		
		synthPin *outputToConnectToJunction = inputPin->GetOtherPin();
		inputPin->Disconnect();
		inputPin->Connect(junction->GetOutput(0));
		junction->GetInput(0).Connect(*outputToConnectToJunction);

		Matrix34 mat = GetMatrix();
		mat.d = m_MousePosition;

		junction->GetView()->SetMatrix(mat);		
		junction->GetView()->Update();

		// This is called from an iteration of the module list, so we can't affect that list directly
		m_ModulesToAdd.PushAndGrow(junction);
		//m_Synthesizer->AddModule(junction);

		if(inputPin->GetParentModule()->GetView()->GetGroup())
		{
			inputPin->GetParentModule()->GetView()->GetGroup()->Add(junction->GetView());
		}

		m_DraggingView = junction->GetView();
		m_DragOffset.Zero();

		return junction;
	}

	synthModule *synthUIPlane::CreateJunctionFromOutput(synthPin *outputPin)
	{
		Assert(outputPin);
		Assert(outputPin->IsOutput());
		synthModule *junction = synthModuleFactory::CreateModule(SYNTH_JUNCTIONPIN);
		Assert(junction);
		atArray<synthPin *> pinsToConnect;
		for(s32 i = 0; i < outputPin->GetView()->GetOutputDestinations().GetCount(); i++)
		{
			pinsToConnect.PushAndGrow(outputPin->GetView()->GetOutputDestinations()[i]);
		}

		for(s32 i = 0; i < pinsToConnect.GetCount(); i++)
		{
			pinsToConnect[i]->Disconnect();
			pinsToConnect[i]->Connect(junction->GetOutput(0));
		}

		junction->GetInput(0).Connect(*outputPin);

		Matrix34 mat = GetMatrix();
		mat.d = m_MousePosition;

		junction->GetView()->SetMatrix(mat);
		junction->GetView()->Update();

		// This is called from an iteration of the module list, so we can't affect that list directly
		m_ModulesToAdd.PushAndGrow(junction);
		//m_Synthesizer->AddModule(junction);

		if(outputPin->GetParentModule()->GetView()->GetGroup())
		{
			outputPin->GetParentModule()->GetView()->GetGroup()->Add(junction->GetView());
		}

		m_DraggingView = junction->GetView();
		m_DragOffset.Zero();

		return junction;
	}

	void synthUIPlane::DeleteModule(synthModule *module)
	{
		Assert(module);
		if(module->GetTypeId() == SYNTH_AUDIOOUTPUT)
		{
			if(m_Synthesizer->GetOutputIndex(module) == 0)
			{
				return;
			}
		}
		m_ModulesToRemove.PushAndGrow(module);
		
		if(module->GetTypeId() == SYNTH_JUNCTIONPIN && module->GetOutput(0).GetView()->GetOutputDestinations().GetCount() > 0)
		{
			synthPin *pinAttachedToInput = module->GetInput(0).GetOtherPin();
			if(pinAttachedToInput)
			{
				atArray<synthPin *> pinsToConnect;
				for(s32 i = 0; i < module->GetOutput(0).GetView()->GetOutputDestinations().GetCount(); i++)
				{
					pinsToConnect.PushAndGrow(module->GetOutput(0).GetView()->GetOutputDestinations()[i]);
				}
				for(s32 i = 0; i < pinsToConnect.GetCount(); i++)
				{
					synthPin *pinAttachedToOutput = pinsToConnect[i];
					Assert(pinAttachedToOutput);
					if(pinAttachedToOutput)
					{
						pinAttachedToOutput->Disconnect();
						pinAttachedToOutput->Connect(*pinAttachedToInput);
					}
				}
			}
		}
	}

	void synthUIPlane::DuplicateView(synthUIView *view)
	{
		audDisplayf("Duplicating view");

		synthUIView *newView = view->DuplicateContents(true);

		if(newView)
		{
			Matrix34 mat = GetMatrix();
			mat.d = GetMousePosition();
			newView->SetMatrix(mat);
			m_DraggingView = newView;
			m_DragOffset.Zero();

			if(view->GetGroup())
			{			
				view->GetGroup()->Add(newView);
			}

			if(newView->IsGroup())
			{
				AddGroup((synthUIViewGroup*)newView);
			}
		}
	}

	struct synthModuleIdPair
	{
		s32 oldModuleId;
		synthModule *newModule;
	};

	void synthUIPlane::ImportObjects(synthSynthesizer *tempInstance, synthUIViewGroup *parentGroup, atArray<synthUIViewGroup *> &groups, atArray<synthCommentBox*> &commentBoxes, atArray<synthModule*> &modules)
	{
		for(s32 i = 0; i < groups.GetCount(); i++)
		{
			if(!groups[i]->IsGrouped())
			{
				parentGroup->Add(groups[i]);
			}
			AddGroup(groups[i]);
		}

		for(s32 i = 0; i < commentBoxes.GetCount(); i++)
		{
			if(!commentBoxes[i]->IsGrouped())
			{
				parentGroup->Add(commentBoxes[i]);
			}
			AddCommentBox(commentBoxes[i]);
		}

		atArray<synthModule*> modulesToDelete;
		// import the new modules into the existing synth
		for(s32 i = 0; i < modules.GetCount(); i++)
		{
			synthModule *module = modules[i];
			if(!module->GetView()->IsGrouped())
			{
				parentGroup->Add(module->GetView());
			}
			if(module->GetTypeId() == SYNTH_AUDIOOUTPUT)
			{
				synthModule *junction = CreateJunctionFromInput(&module->GetInput(0));
				module->GetInput(0).Disconnect();

				junction->GetView()->SetMatrix(module->GetView()->GetMatrix());

				if(junction->GetView()->IsGrouped())
				{
					junction->GetView()->GetGroup()->Remove(junction->GetView());
				}

				synthUIViewGroup *outputGroup = rage_new synthUIViewGroup();
				outputGroup->SetName("Audio Output");
				outputGroup->Add(junction->GetView());
				AddGroup(outputGroup);
				module->GetView()->GetGroup()->Add(outputGroup);
				module->GetView()->GetGroup()->Remove(module->GetView());
			}
			else if(module->GetTypeId() == SYNTH_AUDIOINPUT)
			{
				synthModule *junction = CreateJunctionFromOutput(&module->GetOutput(0));
				junction->GetInput(0).Disconnect();

				junction->GetView()->SetMatrix(module->GetView()->GetMatrix());

				if(junction->GetView()->IsGrouped())
				{
					junction->GetView()->GetGroup()->Remove(junction->GetView());
				}

				synthUIViewGroup *inputGroup = rage_new synthUIViewGroup();
				inputGroup->SetName("Audio Input");
				inputGroup->Add(junction->GetView());
				AddGroup(inputGroup);
				module->GetView()->GetGroup()->Add(inputGroup);
				module->GetView()->GetGroup()->Remove(module->GetView());
			}
			else
			{
				modulesToDelete.Grow() = module;
				m_Synthesizer->AddModule(module);
			}
		}

		for(s32 i = 0; i < modulesToDelete.GetCount(); i++)
		{
			tempInstance->GetModules().DeleteMatches(modulesToDelete[i]);
		}
	}

	void synthUIPlane::ImportPatch()
	{ 
		if(audVerifyf(m_ImportGroup_Patch, "Importing a patch with no metadata"))
		{
			synthUIViewGroup *newGroup = rage_new synthUIViewGroup();
			newGroup->SetName(m_ImportGroupPatchName);
			
			Matrix34 creationMatrix = GetMatrix();
			creationMatrix.d = GetMousePosition();
			newGroup->SetMatrix(creationMatrix);

			synthUIObjects uiObjects;
			uiObjects.rootMatrix = creationMatrix;

			s32 newSynthId = synthSynthesizer::Create(m_ImportGroupPatchName, &uiObjects);
			if(audVerifyf(newSynthId != -1, "Failed to instantiated synth %s", m_ImportGroupPatchName))
			{
				synthSynthesizer *newSynth = static_cast<synthSynthesizer*>(audDriver::GetMixer()->GetPcmSource(newSynthId));

				ImportObjects(newSynth, newGroup, uiObjects.groups, uiObjects.commentBoxes, newSynth->GetModules());
			
				// TODO: Fix this
				// audPcmSourceInterface::Release(newSynthId);

			}

			// set up variables
			/*const u32 numExposedVariables = synthReader.GetNumExposedVariables();
			for(u32 i =0 ; i < numExposedVariables; i++)
			{
				const ModularSynth::tExposedVariable *varData = synthReader.GetNextVariable();
				Assert(newModuleList[varData->ModuleId].newModule->GetTypeId() == SYNTH_VARIABLEINPUT);
				if(newModuleList[varData->ModuleId].newModule->GetTypeId() == SYNTH_VARIABLEINPUT)
				{
					((synthVariableInput*)newModuleList[varData->ModuleId].newModule)->SetExportedKey(varData->Key);
				}
			}*/

			
			AddGroup(newGroup);

			m_DraggingView = newGroup;
			
			newGroup->Update();

			Vector3 v0,v1;
			newGroup->GetBounds(v0,v1);

			m_DragOffset = m_MousePosition - ((v0+v1)*0.5f);

			m_Synthesizer->ComputeProcessingGraph();
		}
	}

	void synthUIPlane::ImportGroup(const u32 groupIndex)
	{
		if(audVerifyf(m_ImportGroup_Patch, "Importing a group with no metadata"))
		{
			Matrix34 creationMatrix = GetMatrix();
			creationMatrix.d = GetMousePosition();
			
			synthUIObjects uiObjects;
			uiObjects.rootMatrix = creationMatrix;
			
			s32 newSynthId = synthSynthesizer::Create(m_ImportGroupPatchName, &uiObjects);

			synthUIViewGroup *importedGroup = uiObjects.groups[groupIndex];

			if(audVerifyf(newSynthId != -1, "Failed to instantiated synth %s", m_ImportGroupPatchName))
			{
				synthSynthesizer *newSynth = static_cast<synthSynthesizer*>(audDriver::GetMixer()->GetPcmSource(newSynthId));

				atArray<synthModuleView*> moduleViews;
				atArray<synthUIViewGroup*> groups;
				atArray<synthCommentBox *> commentBoxes;
				importedGroup->FindModules(moduleViews);
				importedGroup->FindGroups(groups);

				atArray<synthModule*> modules;
				for(s32 i = 0; i < moduleViews.GetCount(); i++)
				{
					modules.Grow() = moduleViews[i]->GetModule();
				}

				ImportObjects(newSynth, importedGroup, groups, commentBoxes, modules);
			
				// TODO: fix this
				//audPcmSourceInterface::Release(newSynthId);

			}

			// set up variables
			/*const u32 numExposedVariables = synthReader.GetNumExposedVariables();
			for(u32 i =0 ; i < numExposedVariables; i++)
			{
				const ModularSynth::tExposedVariable *varData = synthReader.GetNextVariable();
				Assert(newModuleList[varData->ModuleId].newModule->GetTypeId() == SYNTH_VARIABLEINPUT);
				if(newModuleList[varData->ModuleId].newModule->GetTypeId() == SYNTH_VARIABLEINPUT)
				{
					((synthVariableInput*)newModuleList[varData->ModuleId].newModule)->SetExportedKey(varData->Key);
				}
			}*/

			
			AddGroup(importedGroup);

			m_DraggingView = importedGroup;
			
			importedGroup->Update();

			Vector3 v0,v1;
			importedGroup->GetBounds(v0,v1);

			m_DragOffset = m_MousePosition - ((v0+v1)*0.5f);

			m_Synthesizer->ComputeProcessingGraph();
		}
	}

	void synthUIPlane::DuplicateGroup(synthUIViewGroup *oldGroup)
	{
		audDisplayf("Duplicating group");
		
		synthUIViewGroup *newGroup = (synthUIViewGroup*)oldGroup->DuplicateContents(true);

		if(newGroup)
		{
			Matrix34 mat = GetMatrix();
			mat.d = m_MousePosition;
			newGroup->SetMatrix(mat);
	
			m_Groups.PushAndGrow(newGroup);

			atArray<synthUIViewGroup*> groupList;
			newGroup->FindGroups(groupList);
			for(s32 i = 0 ; i < groupList.GetCount(); i++)
			{
				m_Groups.PushAndGrow(groupList[i]);
			}
			
			m_DraggingView = newGroup;
	
			newGroup->Update();
	
			Vector3 v0,v1;
			newGroup->GetBounds(v0,v1);
	
			m_DragOffset = m_MousePosition - ((v0+v1)*0.5f);
	
			m_Synthesizer->ComputeProcessingGraph();
		}
	}

	void synthUIPlane::Update()
	{
		if(IsActive())
		{
			const char *prompt = m_Synthesizer->ShouldExport() ? "Disable Export" : "Enable Export";
			m_LoadSaveMenu.SetTitleForTag(MENU_OPTION_TOGGLE_EXPORT, prompt);

			prompt = synthSynthesizerView::Get()->IsSavingEnabled() ? "Disable Saving" : "Enable Saving";
			m_LoadSaveMenu.SetTitleForTag(MENU_OPTION_TOGGLE_SAVING, prompt);

			synthUIView *viewUnderMouse = NULL;

			switch(m_InputBoxState)
			{
			case InputBox_Idle:
				m_InputBox.SetShouldDraw(false);
				break;
			case InputBox_SaveCopy_PatchName:
				m_InputBox.SetAllowAlphaChars(true);
				m_InputBox.Update();
				if(m_InputBox.HadEnter())
				{
					synthSynthesizerView::ClearModalComponent();
					SetIsKeyboardLocked(false);

					formatf(m_ImportGroupPatchName, "%s", m_InputBox.GetKeyboardBuffer());
					m_InputBox.Reset();
				
					if(strlen(m_ImportGroupPatchName))
					{
						if(synthSynthesizerView::Get()->SaveCopyAs(m_ImportGroupPatchName))
						{
							audDisplayf("Saved copy as '%s'", m_ImportGroupPatchName);
						}
						else
						{
							audErrorf("Failed to save copy as '%'", m_ImportGroupPatchName);
						}
					}
					m_InputBoxState = InputBox_Idle;				
				}
				break;
			case InputBox_SaveAs_PatchName:
				m_InputBox.SetAllowAlphaChars(true);
				m_InputBox.Update();
				if(m_InputBox.HadEnter())
				{
					synthSynthesizerView::ClearModalComponent();
					SetIsKeyboardLocked(false);

					formatf(m_ImportGroupPatchName, "%s", m_InputBox.GetKeyboardBuffer());
					m_InputBox.Reset();

					if(strlen(m_ImportGroupPatchName))
					{
						m_Synthesizer->SetName(m_ImportGroupPatchName);
					}
					
					m_InputBoxState = InputBox_Idle;				
				}
				break;
			case InputBox_NewPatch_PatchName:
				m_InputBox.SetAllowAlphaChars(true);
				m_InputBox.Update();
				if(m_InputBox.HadEnter())
				{
					synthSynthesizerView::ClearModalComponent();
					SetIsKeyboardLocked(false);

					formatf(m_ImportGroupPatchName, "%s", m_InputBox.GetKeyboardBuffer());
					m_InputBox.Reset();

					if(strlen(m_ImportGroupPatchName))
					{
						if(synthSynthesizer::FindMetadata(m_ImportGroupPatchName))
						{
							audErrorf("Patch with name \'%s\' already exists", m_ImportGroupPatchName);
						}
						else
						{
							if(synthSynthesizerView::Get()->CreateNewAsset(m_ImportGroupPatchName))
							{
								synthSynthesizerView::Get()->ChangeAsset(m_ImportGroupPatchName);
							}
						}
					}

					m_InputBoxState = InputBox_Idle;				
				}
				break;
			case InputBox_LoadPatch_ModuleName:
			case InputBox_ImportGroup_ModuleName:
			case InputBox_ImportPatch_ModuleName:
			
				m_InputBox.SetShouldDraw(true);
				m_InputBox.SetAllowAlphaChars(true);
				m_InputBox.Update();
				if(m_InputBox.HadEnter())
				{
					synthSynthesizerView::ClearModalComponent();
					SetIsKeyboardLocked(false);

					formatf(m_ImportGroupPatchName, "%s", m_InputBox.GetKeyboardBuffer());
					m_InputBox.Reset();
					m_ImportGroup_Patch = synthSynthesizer::FindMetadata(m_ImportGroupPatchName);
					if(m_ImportGroup_Patch == NULL)
					{
						audErrorf("Failed to find patch %s", m_ImportGroupPatchName);
						m_InputBoxState = InputBox_Idle;
					}
					else
					{
						if(m_InputBoxState == InputBox_ImportGroup_ModuleName)
						{
							m_InputBoxState = InputBox_ImportGroup_GroupName;

							// create dynamic menu
							m_ImportGroupListMenu.ResetItems();

							synthUIObjects uiObjects;
							Matrix34 creationMatrix = GetMatrix();
							creationMatrix.d = GetMousePosition();
							uiObjects.rootMatrix = creationMatrix;

							s32 newSynthId = synthSynthesizer::Create(m_ImportGroupPatchName, &uiObjects);
							if(newSynthId == -1)
							{
								audErrorf("Failed to create patch %s", m_ImportGroupPatchName);
								m_InputBoxState = InputBox_Idle;
								return;
							}
			
							for(s32 i = 0; i < uiObjects.groups.GetCount(); i++)
							{

								synthUIViewGroup *group = uiObjects.groups[i];
								// don't show groups that only contain one module
								if(group->GetContents().GetCount() > 1)
								{
									char buf[64];
									const s32 nameLength = (u32)strlen(group->GetName());
									if(nameLength > 12)
									{
										formatf(buf, "%.*s..%s (%u)", 
											8, group->GetName(), 
											group->GetName() + nameLength - 4,
											group->GetContents().GetCount());		
									}
									else
									{
										formatf(buf, "%s (%u)", group->GetName(), group->GetContents().GetCount());
									}
																
									m_ImportGroupListMenu.AddItem(buf, i);
								}
							}

							// TODO: fix this
							// audPcmSourceInterface::Release(newSynthId);

							Matrix34 mat = GetMatrix();
							mat.d = m_MousePosition - mat.c*0.2f;
							
							m_ImportGroupListMenu.SortAlphabetically();
							m_ImportGroupListMenu.SetMatrix(mat);
							m_ImportGroupListMenu.ClearSelection();						
							m_ImportGroupListMenu.Update();
							
						}
						else if(m_InputBoxState == InputBox_LoadPatch_ModuleName)
						{
							synthSynthesizerView::Get()->ChangeAsset(m_ImportGroupPatchName);
							m_InputBoxState = InputBox_Idle;
						}
						else
						{
							ImportPatch();
							m_InputBoxState = InputBox_Idle;
						}
					}
				}
				break;
			case InputBox_ImportGroup_GroupName:
				
				m_ImportGroupListMenu.SetShouldDraw(true);
				
				ComputeMouseCollision(m_MousePosition, &m_ImportGroupListMenu, viewUnderMouse);
				m_ImportGroupListMenu.Update();
				if(!viewUnderMouse)
				{
					if(HadLeftClick())
					{
						m_ImportGroupListMenu.SetShouldDraw(false);
						m_InputBoxState = InputBox_Idle;
					}
					else
					{
						// when the menu is on screen prevent any other components from receiving mouse interaction
						viewUnderMouse = &m_ImportGroupListMenu;
					}
				}
				
				if(m_ImportGroupListMenu.HasSelection())
				{					
					const s32 groupId = m_ImportGroupListMenu.GetSelectedTag();
					ImportGroup(groupId);

					m_ImportGroupListMenu.SetShouldDraw(false);
					m_InputBoxState = InputBox_Idle;
				}
				break;

			case InputBox_SavePreset_PresetName:
			case InputBox_ApplyPreset_PresetName:
				m_InputBox.SetShouldDraw(true);
				m_InputBox.SetAllowAlphaChars(true);
				m_InputBox.Update();
				if(m_InputBox.HadEnter())
				{
					synthSynthesizerView::ClearModalComponent();
					SetIsKeyboardLocked(false);

					const char *presetName = m_InputBox.GetKeyboardBuffer();

					if(presetName && *presetName)
					{
						if(m_InputBoxState == InputBox_SavePreset_PresetName)
						{
							synthSynthesizerView::Get()->SavePreset(presetName);
						}
						else
						{
							const Preset *preset = synthSynthesizer::FindPreset(presetName);

							if(preset)
							{
								audDisplayf("Applying preset %s", presetName);
								GetSynthesizer()->ApplyPreset(preset);
							}
							else
							{
								audErrorf("Failed to find preset %s", presetName);
							}
						}
					}

					m_InputBox.Reset();
					m_InputBoxState = InputBox_Idle;
				}
				break;
			}


			if(m_DraggingView == NULL)
			{
				m_DisableDragGroupRemoval = false;
			}
			m_HadLeftDoubleClick = m_HadLeftClick = m_HadRightClick = false;
			synthUIViewGroup *groupUnderMouse = NULL;
			const bool mouseClickThisFrame = (ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT) != 0;

			if(mouseClickThisFrame && m_LastLeftClickTime != 0)
			{
				m_HadLeftDoubleClick = true;
				m_LastLeftClickTime = 0;
			}
			else
			{
				if(mouseClickThisFrame)
				{
					m_MouseLeftClickX = ioMouse::GetX();
					m_MouseLeftClickY = ioMouse::GetY();
					m_LastLeftClickTime = audEngineUtil::GetCurrentTimeInMilliseconds();
				}
			}
			const u32 doubleClickTimeWindow = ::GetDoubleClickTime(); // 150
			
			// Moving the mouse also cancels a double click
			if(audEngineUtil::GetCurrentTimeInMilliseconds() - m_LastLeftClickTime > doubleClickTimeWindow ||
				ioMouse::GetX() != m_MouseLeftClickX || ioMouse::GetY() != m_MouseLeftClickY)
			{
				// if the mouse button is held down dont register a click
				if(m_LastLeftClickTime != 0/* && !(ioMouse::GetButtons()&ioMouse::MOUSE_LEFT)*/)
				{
					m_HadLeftClick = true;
				}
				m_LastLeftClickTime = 0;
			}
			m_HadRightClick = (ioMouse::GetPressedButtons()&ioMouse::MOUSE_RIGHT) != 0;

			float mouseX = static_cast<float>(ioMouse::GetX());
			float mouseY = static_cast<float>(ioMouse::GetY());

#if __WIN32PC && 0
			if(IsMouseFixed())
			{
				::SetCursorPos(m_MouseFixX, m_MouseFixY);
			}

			const bool drawMouse = !IsMouseFixed();		
			static bool showing = true;
			if(drawMouse != showing)
			{
				::ShowCursor(drawMouse);
				showing = drawMouse;
			}
			
#endif
			if(grcViewport::GetCurrent() && grcViewport::GetDefaultScreen())
			{
				
				// don't update mouse position if there is a modal component
				// Otherwise the view port is normalized 2d; we want the 3d mouse position
				// Should really grab 3d mouse position during render, since that's where the view port is
				// modified
				if(synthSynthesizerView::GetModalComponent() == NULL)
				{
					//mouseX =( mouseX/grcViewport::GetDefaultScreen()->GetWidth() ) * grcViewport::GetCurrent()->GetWidth();
					//mouseY =( mouseY/grcViewport::GetDefaultScreen()->GetHeight() ) * grcViewport::GetCurrent()->GetHeight();
					Vector3 mouseNear, mouseFar;
					grcViewport::GetCurrent()->ReverseTransform(mouseX,mouseY,(Vec3V&)mouseNear, (Vec3V&)mouseFar);

					float tValue = 0.f;
					Vector3 ray = mouseFar-mouseNear;
					ray.Normalize();

					Vector3 boundsMin, boundsMax;
					GetBounds(boundsMin, boundsMax);
					Vector4 plane;
					Vector3 planeNorm;

					plane.ComputePlane(boundsMin, -GetMatrix().c);
					geomSegments::CollideRayPlane(mouseNear, ray, plane, &tValue);
					m_MousePosition = mouseNear + (ray * tValue);

					// HACK
					m_MousePosition.z = 0.f;
				}
			
				m_ContextMenu.SetTextSize(0.27f * g_MenuItemScaling);

				ComputeMouseCollision(m_MousePosition, &m_ContextMenu, viewUnderMouse);
				if(m_ContextMenu.ShouldDraw() && !viewUnderMouse)
				{
					// when the menu is on screen prevent any other components from receiving mouse interaction
					viewUnderMouse = &m_ContextMenu;
				}
			
				if(m_DraggingView != NULL)
				{
					if(IsMouseFixed())
					{
						audDisplayf("Dragging while mouse is fixed: %f,%f", m_MousePosition.x,m_MousePosition.y);
					}
					// effectively using IsDown() here, rather than WasClicked
					if(ioMouse::GetButtons()&ioMouse::MOUSE_LEFT)
					{
						Matrix34 mtx = m_DraggingView->GetMatrix();
						mtx.d = m_MousePosition + m_DragOffset;
						m_DraggingView->SetMatrix(mtx);

						// make it impossible to lose the thing you are dragging
						viewUnderMouse = m_DraggingView;

						// alt-drag to add to an existing group
						if(IsGroupKeyDown() && !m_DisableDragGroupRemoval)
						{
							synthUIViewGroup *group = FindGroupByPosition(m_MousePosition);
							if(group && group != m_DraggingView && !m_DraggingView->IsGrouped())
							{	
								group->Add(m_DraggingView);
								// ensure we don't remove this module immediately
								m_DisableDragGroupRemoval = true;
							}
						}
					}
					else
					{
						m_DraggingView = NULL;
						m_DisableDragGroupRemoval = false;
					}
				}

				for(atArray<synthModule*>::iterator iter = m_Modules->begin(); iter != m_Modules->end(); iter++)
				{
					Vector3 boundsMin,boundsMax;
					synthUIView *view = (*iter)->GetView();
					ComputeMouseCollision(m_MousePosition, view, viewUnderMouse);
					if(viewUnderMouse==view)
					{
						if(m_DraggingView == NULL && ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT)
						{
							if(view->StartDrag())
							{
								// begin dragging this module
								m_DraggingView = view;
								m_DragOffset = (m_DraggingView->GetMatrix().d - m_MousePosition);
							}
						}
						// alt-drag to remove from group
						if(view->StartDrag() &&
							!m_DisableDragGroupRemoval && m_HadLeftClick && IsGroupKeyDown())
						{					
							if(view->IsGrouped())
							{
								view->GetGroup()->Remove(view);
								m_DisableDragGroupRemoval = true;
							}
						}
					}
				}
				if(viewUnderMouse)
				{
					if(viewUnderMouse->StartDrag() && 
						ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT && IsDuplicateKeyDown())
					{
						DuplicateView(viewUnderMouse);
					}
				}
				
				atArray<synthUIViewGroup *> groupsUnderMouse;

				for(atArray<synthUIViewGroup*>::iterator iter = m_Groups.begin(); iter != m_Groups.end(); iter++)
				{
					ComputeMouseCollision(m_MousePosition, *iter, viewUnderMouse);
					if(viewUnderMouse == *iter)
					{
						viewUnderMouse = NULL;
						(*iter)->SetUnderMouse(false);
						(*iter)->SetTransparent(true);
						groupsUnderMouse.PushAndGrow(*iter);
					}
					else
					{
						(*iter)->SetTransparent(false);
					}
				}

				// set the smallest group under mouse to be the group we consider under the mouse
				f32 smallestGroupMag2 = LARGE_FLOAT;
				for(atArray<synthUIViewGroup*>::iterator iter = groupsUnderMouse.begin(); iter != groupsUnderMouse.end(); iter++)
				{
					Vector3 v0,v1;
					(*iter)->GetBounds(v0,v1);
					const f32 boundsMag = (v1-v0).Mag2();
					if(boundsMag < smallestGroupMag2)
					{
						smallestGroupMag2 = boundsMag;
						viewUnderMouse = *iter;
						groupUnderMouse = *iter;
					}
				}
				if(groupsUnderMouse.GetCount()>0)
				{
					groupUnderMouse->SetUnderMouse(true);

					if(ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT && IsDuplicateKeyDown())
					{
						DuplicateGroup(groupUnderMouse);
					}
					else if(groupUnderMouse->StartDrag() &&
						!m_DisableDragGroupRemoval && m_HadLeftClick && IsGroupKeyDown())
					{
						if(groupUnderMouse->IsGrouped())
						{
							groupUnderMouse->GetGroup()->Remove(groupUnderMouse);
						}
					}
					if(m_DraggingView == NULL && ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT)
					{
						if(groupUnderMouse->StartDrag())
						{
							// begin dragging this module
							m_DraggingView = groupUnderMouse;
							m_DragOffset = (m_DraggingView->GetMatrix().d - m_MousePosition);
						}
					}
				}

				// Comment boxes
				for(atArray<synthCommentBox*>::iterator iter = m_CommentBoxes.begin(); iter != m_CommentBoxes.end(); iter++)
				{
					ComputeMouseCollision(m_MousePosition, *iter, viewUnderMouse);
					if(viewUnderMouse == *iter)
					{
						if(m_DraggingView == NULL && ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT)
						{
							if((*iter)->StartDrag())
							{
								// begin dragging this module
								m_DraggingView = (*iter);
								m_DragOffset = (m_DraggingView->GetMatrix().d - m_MousePosition);
							}
						}
					}
				}

				// Only show menu if there is no module under the group
				if(viewUnderMouse == NULL || groupUnderMouse != NULL)
				{
					if(HadRightClick())
					{
						Matrix34 mat;
						mat = GetMatrix();
						mat.d = m_MousePosition - mat.c*0.2f;
						m_CreationPosition = m_MousePosition;
						m_ContextMenu.SetMatrix(mat);
						m_ContextMenu.ClearSelection();
						m_ContextMenu.SetShouldDraw(true);
						m_ActiveGroupForModuleCreation = groupUnderMouse;
					}
				}

				m_ContextMenu.Update();

				if(m_ContextMenu.ShouldDraw())
				{
					if(m_ContextMenu.HasSelection())
					{
						m_ContextMenu.SetShouldDraw(false);

						const s32 selectedTag = m_ContextMenu.GetSelectedTag();
						switch(selectedTag)
						{
						case MENU_OPTION_LOAD_PATCH:
							if(m_InputBoxState == InputBox_Idle && synthSynthesizerView::SetModalComponent(&m_InputBox))
							{
								SetIsKeyboardLocked(true);
								m_InputBox.Reset();
								m_InputBox.SetTitle("Patch Name");
								m_InputBox.SetShouldDraw(true);
								m_InputBox.SetValue(m_ImportGroupPatchName);
								m_InputBox.Update();
								m_InputBoxState = InputBox_LoadPatch_ModuleName;
							}
							break;
						case MENU_OPTION_NEW_PATCH:
							if(m_InputBoxState == InputBox_Idle && synthSynthesizerView::SetModalComponent(&m_InputBox))
							{
								SetIsKeyboardLocked(true);
								m_InputBox.Reset();
								m_InputBox.SetTitle("Patch Name");
								m_InputBox.SetShouldDraw(true);
								m_InputBox.SetValue(m_ImportGroupPatchName);
								m_InputBox.Update();
								m_InputBoxState = InputBox_NewPatch_PatchName;
							}
							break;
						case MENU_OPTION_SAVE_COPY:
							if(m_InputBoxState == InputBox_Idle && synthSynthesizerView::SetModalComponent(&m_InputBox))
							{
								SetIsKeyboardLocked(true);
								m_InputBox.Reset();
								m_InputBox.SetTitle("New Patch Name");
								m_InputBox.SetShouldDraw(true);
								m_InputBox.SetValue(GetSynthesizer()->GetName());
								m_InputBox.Update();
								m_InputBoxState = InputBox_SaveCopy_PatchName;
							}
							break;
						case MENU_OPTION_SAVE_AS:
							if(m_InputBoxState == InputBox_Idle && synthSynthesizerView::SetModalComponent(&m_InputBox))
							{
								SetIsKeyboardLocked(true);
								m_InputBox.Reset();
								m_InputBox.SetTitle("Save As Patch Name");
								m_InputBox.SetShouldDraw(true);
								m_InputBox.SetValue(GetSynthesizer()->GetName());
								m_InputBox.Update();
								m_InputBoxState = InputBox_SaveAs_PatchName;
							}
							break;
						case MENU_OPTION_IMPORT_GROUP:
							if(m_InputBoxState == InputBox_Idle && synthSynthesizerView::SetModalComponent(&m_InputBox))
							{
								SetIsKeyboardLocked(true);
								m_InputBox.Reset();
								m_InputBox.SetTitle("Patch Name");
								m_InputBox.SetShouldDraw(true);
								m_InputBox.SetValue(m_ImportGroupPatchName);
								m_InputBox.Update();
								m_InputBoxState = InputBox_ImportGroup_ModuleName;
							}
							break;
						case MENU_OPTION_IMPORT_PATCH:
							if(m_InputBoxState == InputBox_Idle && synthSynthesizerView::SetModalComponent(&m_InputBox))
							{
								SetIsKeyboardLocked(true);
								m_InputBox.Reset();
								m_InputBox.SetTitle("Patch Name");
								m_InputBox.SetShouldDraw(true);
								m_InputBox.SetValue(m_ImportGroupPatchName);
								m_InputBox.Update();
								m_InputBoxState = InputBox_ImportPatch_ModuleName;
							}
							break;
						case MENU_OPTION_COMMENT:
							{
								synthCommentBox *commentBox = rage_new synthCommentBox();
								Matrix34 mat = GetMatrix();
								mat.d = m_CreationPosition;
								commentBox->SetMatrix(mat);
								commentBox->Update();
								m_CommentBoxes.PushAndGrow(commentBox);
							}
							break;
						case MENU_OPTION_SAVE_PRESET:
						case MENU_OPTION_APPLY_PRESET:
							if(m_InputBoxState == InputBox_Idle && synthSynthesizerView::SetModalComponent(&m_InputBox))
							{
								SetIsKeyboardLocked(true);
								m_InputBox.Reset();
								m_InputBox.SetTitle("Preset Name");
								m_InputBox.SetShouldDraw(true);
								m_InputBox.SetValue("");
								m_InputBox.Update();
								m_InputBoxState = selectedTag == MENU_OPTION_APPLY_PRESET ? InputBox_ApplyPreset_PresetName : InputBox_SavePreset_PresetName;
							}
							break;
						case MENU_OPTION_COMPRESSOR_MACRO:
							// Create a compressor using the Compressor_EG module
							CreateCompressorMacro();
							break;
						case MENU_OPTION_TOGGLE_EXPORT:
							m_Synthesizer->SetShouldExport(!m_Synthesizer->ShouldExport());
							break;
						case MENU_OPTION_TOGGLE_SAVING:
							synthSynthesizerView::Get()->SetEnableSaving(!synthSynthesizerView::Get()->IsSavingEnabled());
							break;
						default:
							{
								synthModuleId moduleId = (synthModuleId)selectedTag;
								if(moduleId != SYNTH_AUDIOOUTPUT || m_Synthesizer->GetNumOutputs() < audPcmSource::kMaxPcmSourceChannels)
								{
									synthModule *module = synthModuleFactory::CreateModule(moduleId);
									if(module)
									{
										Matrix34 mat = GetMatrix();
										mat.d = m_CreationPosition;
										module->GetView()->SetMatrix(mat);
										if(m_ActiveGroupForModuleCreation)
										{
											m_ActiveGroupForModuleCreation->Add(module->GetView());
										}
										m_Synthesizer->AddModule(module);
										module->GetView()->Update();
									}
								}
							}
						}
					}
				
					if(ioMouse::GetReleasedButtons()&ioMouse::MOUSE_LEFT)
					{
						m_ContextMenu.SetShouldDraw(false);
					}
				}
			}

			if(m_DraggingView)
			{
				m_DraggingView->SetUnderMouse(true);
			}
			else
			{
				if(!IsKeyboardLocked())
				{
					if(ioKeyboard::KeyPressed(KEY_J))
					{
						// create a new junction at the mouse
						CreateJunctionAtMouse();
					}
					else if(ioKeyboard::KeyPressed(KEY_T))
					{
						// drop a comment box
						synthCommentBox *commentBox = rage_new synthCommentBox();
						Matrix34 mat = GetMatrix();
						mat.d = m_MousePosition;
						commentBox->SetMatrix(mat);
						commentBox->Edit();
						m_CommentBoxes.PushAndGrow(commentBox);
					}
					else if(ioKeyboard::KeyPressed(KEY_SPACE))
					{
						m_Synthesizer->ResetProcessing();
					}
				}//!IsKeyboardLocked
			}
			
			// update groups
			atArray<synthUIViewGroup*> groupsToDelete;
			for(atArray<synthUIViewGroup*>::iterator iter = m_Groups.begin(); iter != m_Groups.end(); iter++)
			{
				(*iter)->Update();

				if((*iter)->ShouldBeDeleted() || (*iter)->IsEmpty())
				{
					groupsToDelete.PushAndGrow(*iter);
				}
			}

			if(m_CurrentlyBuildingGroup)
			{
				m_CurrentlyBuildingGroup->Update();
			}
			for(atArray<synthUIViewGroup*>::iterator iter = groupsToDelete.begin(); iter != groupsToDelete.end(); iter++)
			{
				if(m_DraggingView == *iter)
				{
					m_DraggingView = NULL;
				}
				if(viewUnderMouse == *iter)
				{
					viewUnderMouse = NULL;
				}
				m_Groups.DeleteMatches(*iter);
				delete *iter;
			}

			// update modules
			for(atArray<synthModule*>::iterator iter = m_Modules->begin(); iter != m_Modules->end(); iter++)
			{
				(*iter)->GetView()->Update();
			}

			bool addedModules = false;
			for(atArray<synthModule*>::iterator iter = m_ModulesToAdd.begin(); iter != m_ModulesToAdd.end(); iter++)
			{
				m_Synthesizer->AddModule(*iter);
				(*iter)->GetView()->Update();
				addedModules = true;
			}
			if(addedModules)
			{
				m_ModulesToAdd.Reset();
				m_Synthesizer->ComputeProcessingGraph();
			}

			// left click in space cancels any current connection
			if((viewUnderMouse == NULL||groupUnderMouse!=NULL) && IsConnecting() && ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT)
			{
				// safety check: make sure the mouse isn't close to any module bounds
				if(!IsMouseNearModule())
				{
					CancelConnection();
				}
			}

			m_Synthesizer->SetAuditioningPin(m_AuditioningPin);

			if(IsGroupKeyDown())
			{
				if(viewUnderMouse && HadRightClick() && viewUnderMouse->StartDrag())
				{
					if(m_CurrentlyBuildingGroup == NULL)
					{
						m_CurrentlyBuildingGroup = rage_new synthUIViewGroup();
						m_CurrentlyBuildingGroup->Update();
					}

					if(viewUnderMouse->IsGrouped())
					{
						synthUIViewGroup *oldGroup = viewUnderMouse->GetGroup();
						if(oldGroup->GetContents().GetCount() == 1)
						{
							// we can't remove this module from the group as that would leave the original group empty, so
							// instead add the modules group to the new group
							if(oldGroup->IsGrouped())
							{
								oldGroup->GetGroup()->Remove(oldGroup);
							}
							m_CurrentlyBuildingGroup->Add(oldGroup);

						}
						else
						{
							oldGroup->Remove(viewUnderMouse);
							if(!m_CurrentlyBuildingGroup->IsGrouped())
							{
								oldGroup->Add(m_CurrentlyBuildingGroup);
							}
							m_CurrentlyBuildingGroup->Add(viewUnderMouse);
						}
					}
					else
					{
						m_CurrentlyBuildingGroup->Add(viewUnderMouse);
					}
				}
			}
			else
			{
				if(m_CurrentlyBuildingGroup)
				{
					if(!m_CurrentlyBuildingGroup->IsEmpty())
					{
						m_Groups.PushAndGrow(m_CurrentlyBuildingGroup);
					}
					else
					{
						delete m_CurrentlyBuildingGroup;
					}

					m_CurrentlyBuildingGroup = NULL;
				}
			}

			bool removedModules = false;
			for(atArray<synthModule*>::iterator iter = m_ModulesToRemove.begin(); iter != m_ModulesToRemove.end(); iter++)
			{
				if(m_AuditioningPin && m_AuditioningPin->GetParentModule() == *iter)
				{
					m_AuditioningPin = NULL;
				}
				if(m_ConnectionP0 && m_ConnectionP0->GetParentModule() == *iter)
				{
					CancelConnection();
				}
				if(m_DraggingView == (*iter)->GetView())
				{
					m_DraggingView = NULL;
				}
				if(viewUnderMouse == (*iter)->GetView())
				{
					viewUnderMouse = NULL;
				}
				// remove from all groups
				for(atArray<synthUIViewGroup*>::iterator groupIter = m_Groups.begin(); groupIter != m_Groups.end(); groupIter++)
				{
					synthUIViewGroup *group = *groupIter;
					if(group->Contains((*iter)->GetView()))
					{
						group->Remove((*iter)->GetView());
					}
				}

				m_Synthesizer->RemoveModule(*iter);
				delete *iter;
				removedModules = true;
			}
			if(removedModules)
			{
				m_ModulesToRemove.Reset();
				m_Synthesizer->ComputeProcessingGraph();
			}

			// update comment boxes
			synthCommentBox *commentToDelete = NULL;
			for(atArray<synthCommentBox*>::iterator iter = m_CommentBoxes.begin(); iter != m_CommentBoxes.end(); iter++)
			{
				(*iter)->Update();
				if(!commentToDelete &&
					(*iter)->IsUnderMouse() && !(*iter)->IsEditing() && !IsKeyboardLocked() 
					&& ioKeyboard::KeyPressed(KEY_DELETE))
				{
					commentToDelete = *iter;
				}
			}
			if(commentToDelete)
			{
				m_CommentBoxes.DeleteMatches(commentToDelete);
				delete commentToDelete;
			}

		}// is active
	}

	bool synthUIPlane::IsMouseNearModule() const
	{
		for(atArray<synthModule*>::iterator iter = m_Modules->begin(); iter != m_Modules->end(); iter++)
		{
			const synthModuleView *const view = (*iter)->GetView();
			if(view->ShouldDraw())
			{
				Vector3 boxSize(1.25f,1.25f,0.25f);
				Vector3 boundsMin, boundsMax;
				view->GetBounds(boundsMin,boundsMax);

				if(geomPoints::IsPointInBox(m_MousePosition, boundsMin - boxSize, boundsMax + boxSize))
				{
					return true;
				}
			}
		}
		return false;
	}

	void synthUIPlane::SetIsMouseFixed(const bool is)
	{
		 m_IsMouseFixed = is; 

		 if(is)
		 {
			 m_DraggingView = NULL;

	 #if __WIN32PC
			 ioMouse::GetPlatformCursorPosition(&m_MouseFixX, &m_MouseFixY);
	#endif

			 ioMouse::SetAbsoluteOnly(false);
		 }
		 else
		 {
			 ioMouse::SetAbsoluteOnly(true);
			 WIN32PC_ONLY(ioMouse::SetPlatformCursorPosition(m_MouseFixX, m_MouseFixY));
		 }		 
	}

	void synthUIPlane::ComputeMouseCollision(const Vector3 &collisionPoint, synthUIView *view, synthUIView *&viewUnderMouse)
	{
		Vector3 boxSize(0.25f,0.25f,0.25f);
		//GetMatrix().Transform3x3(boxSize);
		Vector3 boundsMin, boundsMax;
		view->GetBounds(boundsMin,boundsMax);
		if(!IsMouseFixed() && viewUnderMouse == NULL && view->ShouldDraw() && geomPoints::IsPointInBox(collisionPoint, boundsMin - boxSize, boundsMax + boxSize))
		{
			viewUnderMouse = view;
			view->SetUnderMouse(true);	
		}
		else
		{
			view->SetUnderMouse(false);
		}
	}

	synthUIViewGroup *synthUIPlane::FindGroupByPosition(const Vector3 &pos) const
	{
		Vector3 boxSize(0.25f,0.25f,0.25f);
		//GetMatrix().Transform3x3(boxSize);
		Vector3 boundsMin, boundsMax;
		atArray<synthUIViewGroup*> groupsUnderMouse;
		for(atArray<synthUIViewGroup*>::const_iterator iter = m_Groups.begin(); iter != m_Groups.end(); iter++)
		{
			// Don't consider the view that's currently being dragged
			if((*iter) != m_DraggingView)
			{
				(*iter)->GetBounds(boundsMin,boundsMax);
				if(geomPoints::IsPointInBox(pos, boundsMin - boxSize, boundsMax + boxSize))
				{
					groupsUnderMouse.PushAndGrow(*iter);
				}
			}
		}

		synthUIViewGroup *groupUnderMouse = NULL;
		f32 smallestGroupMag2 = LARGE_FLOAT;
		for(atArray<synthUIViewGroup*>::iterator iter = groupsUnderMouse.begin(); iter != groupsUnderMouse.end(); iter++)
		{
			Vector3 v0,v1;
			(*iter)->GetBounds(v0,v1);
			const f32 boundsMag = (v1-v0).Mag2();
			if(boundsMag < smallestGroupMag2)
			{
				smallestGroupMag2 = boundsMag;
				groupUnderMouse = *iter;
			}
		}

		return groupUnderMouse;
	}

	void synthUIPlane::Render()
	{
		synthUIComponent::sm_NumRendered = 0;
		synthUIComponent::sm_NumTested = 0;
	
		for(atArray<synthUIViewGroup*>::iterator iter = m_Groups.begin(); iter != m_Groups.end(); iter++)
		{
			(*iter)->Draw();
		}
		
		if(m_CurrentlyBuildingGroup)
		{
			m_CurrentlyBuildingGroup->Draw();
		}

		for(atArray<synthCommentBox*>::iterator iter = m_CommentBoxes.begin(); iter != m_CommentBoxes.end(); iter++)
		{
			(*iter)->Draw();
		}

		for(atArray<synthModule*>::iterator iter = m_Modules->begin(); iter != m_Modules->end(); iter++)
		{
			((synthModuleView*)(*iter)->GetView())->DrawBackground();
		}

		for(atArray<synthModule*>::iterator iter = m_Modules->begin(); iter != m_Modules->end(); iter++)
		{	
			(*iter)->GetView()->RenderWires();
						
		}
		for(atArray<synthModule*>::iterator iter = m_Modules->begin(); iter != m_Modules->end(); iter++)
		{
			(*iter)->GetView()->Draw();
		}

		if(m_ConnectionP0)
		{
			synthModuleView::RenderWire(GetMatrix(), m_ConnectionP0->GetView()->GetMatrix().d, m_MousePosition, m_ConnectionP0->GetView()->GetPeakValue(),m_ConnectionP0->GetDataFormat()==synthPin::SYNTH_PIN_DATA_NORMALIZED?g_NormalizedWireColour:g_SignalWireColour, m_ConnectionP0->GetDataState() == synthPin::SYNTH_PIN_DYNAMIC);
		}

		for(atArray<synthModule*>::iterator iter = m_Modules->begin(); iter != m_Modules->end(); iter++)
		{
			((synthModuleView*)(*iter)->GetView())->DrawPins();
		}


		m_ContextMenu.Draw();	
		m_ImportGroupListMenu.Draw();

#if !__FINAL
		// draw mouse pointer
		if(PARAM_fullscreen.Get())
#endif
		{
			grcColor(Color32(1.f,1.f,1.f,1.f));
			grcDrawSphere(0.3f, m_MousePosition, 16, false, true);
		}
	}

	void synthUIPlane::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		v1.Set(LARGE_FLOAT,LARGE_FLOAT,LARGE_FLOAT);
		v2 = -v1;
		for(atArray<synthModule*>::iterator iter = m_Modules->begin(); iter != m_Modules->end(); iter++)
		{
			Vector3 t1,t2;
			if((*iter)->GetView()->ShouldDraw())
			{
				(*iter)->GetView()->GetBounds(t1,t2);
				v1.Set(Min(v1.x,t1.x),Min(v1.y,t1.y),Min(v1.z,t1.z));
				v2.Set(Max(v2.x,t2.x),Max(v2.y,t2.y),Max(v2.z,t2.z));
			}
		}
	}

	bool synthUIPlane::IsGroupKeyDown() const
	{
		return !IsKeyboardLocked() && ioKeyboard::KeyDown(KEY_ALT)!=0;
	}

	bool synthUIPlane::IsDuplicateKeyDown() const
	{
		return !IsKeyboardLocked() && ioKeyboard::KeyDown(KEY_CONTROL)!=0;
	}

	bool synthUIPlane::IsCapsLockOn() const
	{
#if __WIN32PC
		if((::GetKeyState(VK_CAPITAL)&1) != 0)
		{
			return true;
		}
#endif
		return false;
	}

	bool synthUIPlane::IsKeyboardLocked()
	{
		return sm_IsKeyboardLocked || synthSynthesizerView::GetModalComponent() != NULL;
	}
}

#endif // __SYNTH_EDITOR
