#ifndef SYNTH_SAMPLEPLAYER_H
#define SYNTH_SAMPLEPLAYER_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "audiohardware/waveplayer.h"

#include "sampleframe.h"
#include "synthdefs.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{

	enum synthSamplePlayerInputs
	{
		kSamplePlayerTrigger = 0,
		kSamplePlayerFrequency,
		kSamplePlayerStartOffset,
		
		kSamplePlayerNumInputs
	};

	enum synthSamplePlayerOutputs
	{
		kSamplePlayerSignal = 0,
		kSamplePlayerPlaying,
		kSamplePlayerPosition,
		
		kSamplePlayerNumOutputs
	};

	class synthSamplePlayer : 
		public synthModuleBase<kSamplePlayerNumInputs, kSamplePlayerNumOutputs, SYNTH_SAMPLEPLAYER>
	{
	public:	
		SYNTH_MODULE_NAME("SamplePlayer");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthSamplePlayer();
		~synthSamplePlayer();

		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();

		enum PlayMode
		{
			kOneShot = 0,
			kLooping,
			kNumPlayModes
		};

		void SetPlayMode(const PlayMode mode)
		{
			m_PlayMode = mode;
		}

		PlayMode GetPlayMode() const
		{
			return m_PlayMode;
		}

		const audWavePlayer *GetWavePlayer() const 
		{
			return (const audWavePlayer*)g_PcmSourcePool->GetSlotPtr(m_WavePlayerId);
		}

		void SetWave(const u32 waveSlotIndex, const u32 waveNameHash);

	private:

		synthSampleFrame m_Buffer;

		PlayMode m_PlayMode;

		s16 m_WavePlayerId;
		bool m_HasStartedPlayback;
		bool m_WasTriggerHigh;
	};

}
#endif
#endif // SYNTH_SAMPLEPLAYER_H
