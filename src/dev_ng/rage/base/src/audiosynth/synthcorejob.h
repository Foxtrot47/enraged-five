// 
// audiosynth/synthcorejob.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_SYNTHCOREJOB_H
#define AUD_SYNTHCOREJOB_H

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(synthcorejob);

#endif // AUD_SYNTHCOREJOB_H
