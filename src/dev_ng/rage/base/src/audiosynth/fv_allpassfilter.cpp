
#include "synthdefs.h"

#include "audiohardware/mixer.h"
#include "fv_allpassfilter.h"
#include "pin.h"

namespace rage
{
	using namespace Vec;

	synthFvAllpassFilter::synthFvAllpassFilter() 
	{
		// 1 second max delay for now
#if !USE_FRAC_DELAY
		m_AllpassBuffer = rage_new float[kMixerNativeSampleRate];
		m_AllpassLength = kMixerNativeSampleRate;
#endif
		m_Outputs[0].Init(this,"Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[kFvAllpassSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kFvAllpassFeedback].Init(this, "Feedback", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kFvAllpassLength].Init(this, "Length", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kFvAllpassDecay].Init(this, "Decay", synthPin::SYNTH_PIN_DATA_NORMALIZED);
	}

	synthFvAllpassFilter::~synthFvAllpassFilter()
	{
		delete[] m_AllpassBuffer;
	}

	void synthFvAllpassFilter::Synthesize()
	{

#if !USE_FRAC_DELAY
		const u32 newLength = Min<u32>(kMixerNativeSampleRate, (u32)(m_Inputs[kFvAllpassLength].GetNormalizedValue(0) * kMixerNativeSampleRate));
		if(newLength != m_AllpassLength)
		{
			m_Filter.SetBuffer(m_AllpassBuffer, newLength);
			m_AllpassLength = newLength;
		}
#endif

		float *outPtr = m_Buffer.GetBuffer();
		for(u32 i = 0; i < kMixBufNumSamples; i++)
		{
			m_Filter.SetDiffusion(m_Inputs[kFvAllpassFeedback].GetNormalizedValue(i));
			m_Filter.SetDecay(m_Inputs[kFvAllpassDecay].GetNormalizedValue(i));
#if USE_FRAC_DELAY
			m_Filter.SetLength(m_Inputs[kFvAllpassLength].GetNormalizedValue(i));
#endif
			*outPtr++ = m_Filter.Process(m_Inputs[kFvAllpassSignal].GetSignalValue(i));
		}
	}
}

