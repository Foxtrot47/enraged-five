// 
// audiosynth/inverter.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "inverter.h"

#if !SYNTH_MINIMAL_MODULES

#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	synthInverter::synthInverter()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);	
		m_Inputs[0].SetConvertsInternally(false);
	}

	void synthInverter::Synthesize()
	{
		m_Outputs[0].SetDataFormat(m_Inputs[0].GetDataFormat());

		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			const f32 input = m_Inputs[0].GetStaticValue();
			f32 output;
			if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
			{
				output = 1.f - input;
			}
			else
			{
				output = input * -1.f;
			}
			m_Outputs[0].SetStaticValue(output);
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			
			Vector_4V *RESTRICT inputBuf = (Vector_4V*)m_Inputs[0].GetDataBuffer()->GetBuffer();
			Vector_4V *RESTRICT outputBuf = (Vector_4V*)m_Buffer.GetBuffer();
			const Vector_4V one = V4VConstant(V_ONE);
			const Vector_4V negOne = V4VConstant(V_NEGONE);
			
			if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
			{
				for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize() >> 2; i++)
				{
					outputBuf[i] = V4Subtract(one, inputBuf[i]);
				}
			}
			else
			{
				for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize() >> 2; i++)
				{
					outputBuf[i] = V4Scale(negOne, inputBuf[i]);
				}
			}
		}
	}
} // namespace rage
#endif
