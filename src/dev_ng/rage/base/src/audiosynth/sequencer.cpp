
#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "sequencer.h"
#include "audiohardware/device.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/driver.h"
#include "audiohardware/mixer.h"

#if __SYNTH_EDITOR
#include "sequencerview.h"
#endif

namespace rage
{

	SYNTH_EDITOR_VIEW(synthSequencer);

	synthSequencer::synthSequencer() 
	{
		m_Inputs[kSequencerTempo].Init(this, "Tempo",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kSequencerTempo].SetStaticValue(0.f);

		m_Inputs[kSequencerDuration].Init(this, "Duration",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kSequencerDuration].SetStaticValue(0.5f);

		m_Inputs[kSequencerTrigger].Init(this, "Trigger", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kSequencerTrigger].SetStaticValue(1.f);

		m_Outputs[0].Init(this, "Step", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);

		for(s32 i = 0; i < kSequencerMaxDivisions; i++)
		{
			m_Outputs[i+1].Init(this, synthPin::sm_PinNumberStrings[i], synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
			m_Outputs[i+1].SetStaticValue(0.f);
		}

		m_MinBpm = 60.f;
		m_MaxBpm = 240.f;
		m_Divisions = 4;
		m_BeatClock = 0.f;
		m_IsActive = false;
	}

	void synthSequencer::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("MinBpm", m_MinBpm);
		serializer->SerializeField("MaxBpm", m_MaxBpm);
		serializer->SerializeField("Divisions", m_Divisions);
	}

	void synthSequencer::GetOutputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Outputs[0];
		numPins = m_Divisions + 1;
	}

	void synthSequencer::Synthesize()
	{
		const bool iskSequencerTriggered = (m_Inputs[kSequencerTrigger].GetNormalizedValue(0) >= 1.f);
		const f32 bpm = m_MinBpm + (m_MaxBpm-m_MinBpm)*m_Inputs[kSequencerTempo].GetNormalizedValue(0);
		
		const f32 beatLengthSeconds = 60.f / bpm;
		f32 beatLengthSamples = kMixerNativeSampleRate * beatLengthSeconds;

		// need to ensure beat length samples is a multiple of kMixBufNumSamples
		beatLengthSamples = kMixBufNumSamples * floorf((beatLengthSamples / kMixBufNumSamples));

		if(!m_IsActive && iskSequencerTriggered)
		{
			m_IsActive = true;
		}

		u32 onIndex = kSequencerMaxDivisions+1;
		bool noteOn = false;
		if(!m_IsActive)
		{
			m_BeatClock = 0.f;
			// all outputs are off
			onIndex = kSequencerMaxDivisions+1;
		}
		else
		{
			m_BeatClock += kMixBufNumSamples/beatLengthSamples;
			// we're running 4/4
			if(m_BeatClock >= 4.f)
			{
				m_BeatClock = 0.f;
				if(!iskSequencerTriggered)
				{
					m_IsActive = false;
				}
			}

			if(m_IsActive)
			{
				const f32 barRatio = (m_BeatClock*0.25f);
				f32 subdividedRatio = (barRatio * (f32)m_Divisions);
				onIndex = (u32)subdividedRatio;
				noteOn = subdividedRatio - floorf(subdividedRatio) <= m_Inputs[kSequencerDuration].GetNormalizedValue(0);
			}
		}
		
		for(u32 i = 0; i < kSequencerMaxDivisions; i++)
		{
			if(i == onIndex)
			{
				m_Outputs[i+1].SetStaticValue(noteOn ? 1.f : 0.f);
			}
			else
			{
				m_Outputs[i+1].SetStaticValue(0.f);
			}
		}

		if(!m_IsActive)
		{
			// the sequence plays out to completion when the kSequencerTrigger goes low, so leave the step output at the end
			m_Outputs[0].SetStaticValue(1.f);
		}
		else
		{
			m_Outputs[0].SetStaticValue((f32)onIndex / (f32)m_Divisions);
		}
	}
}
#endif

