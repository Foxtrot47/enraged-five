#include "exp.h"

#if !SYNTH_MINIMAL_MODULES

#include "math/amath.h"

namespace rage
{
	synthLinearToExponential::synthLinearToExponential()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);

		m_Inputs[kLinearToExponentialSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kLinearToExponentialExponent].Init(this, "Exponent", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kLinearToExponentialExponent].SetStaticValue(8.f);
		m_Outputs[0].SetDataBuffer(&m_Buffer);
	}

	void synthLinearToExponential::Synthesize()
	{
		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC && m_Inputs[1].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_Outputs[0].SetStaticValue((powf(m_Inputs[kLinearToExponentialExponent].GetStaticValue(), m_Inputs[kLinearToExponentialSignal].GetStaticValue())-1.f) / (m_Inputs[kLinearToExponentialExponent].GetStaticValue()-1.f));
		}
		else
		{
			f32 exponent = m_Inputs[kLinearToExponentialExponent].GetStaticValue();

			m_Outputs[0].SetDataBuffer(&m_Buffer);
			
			for(u32 i = 0; i < m_Buffer.GetSize(); i++)
			{
				f32 input = m_Inputs[kLinearToExponentialSignal].GetSignalValue(i);
				input = (input < 0.f ? 0.f : input);
				m_Outputs[0].GetDataBuffer()->GetBuffer()[i] = ((powf(exponent, input)-1.f)/(exponent-1.f)) * 2.f - 1.f;
			}
		}
	}
}
#endif

