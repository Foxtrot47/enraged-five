// 
// audiosynth/fftview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "fftview.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"

namespace rage
{

	synthFftView::synthFftView(synthModule *module) : synthModuleView(module)
	{

	}

	void synthFftView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		Vector3 inV1, inV2, outV1, outV2;
		GetModule()->GetInput(0).GetView()->GetBounds(inV1,inV2);
		GetModule()->GetOutput(0).GetView()->GetBounds(outV1,outV2);

		v1.Min(inV1,outV1);
		v2.Max(inV2, outV2);
	}

	void synthFftView::Update()
	{
		synthPinView *inputPinView = GetModule()->GetInput(0).GetView();
		synthPinView *outputPinView = GetModule()->GetOutput(0).GetView();

		outputPinView->SetDefaultYZoom(1.6f);
		outputPinView->SetDrawFilled(true);
		outputPinView->DisableZooming(true);
		outputPinView->DisableConnections(true);

		inputPinView->DisableZooming(true);

		Matrix34 mat = GetMatrix();
		mat.d += mat.a*3.f;
		inputPinView->SetMatrix(mat);

		mat = GetMatrix();
		mat.d -= mat.a*2.f;
		outputPinView->SetMatrix(mat);

		outputPinView->SetTargetScale(1.5f);

		synthModuleView::Update();

		const bool drawingSpectrum = (GetModule()->GetOutput(0).GetDataState() == synthPin::SYNTH_PIN_DYNAMIC);
		outputPinView->SetShouldDraw(drawingSpectrum);
		m_TitleLabel.SetShouldDraw(!drawingSpectrum);
	}

	bool synthFftView::StartDrag() const
	{
		return !GetModule()->GetInput(0).GetView()->IsUnderMouse();
	}
}

#endif // __SYNTH_EDITOR
