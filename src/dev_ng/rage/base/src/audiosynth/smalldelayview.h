// 
// audiosynth/smalldelayview.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SMALLDELAYVIEW_H
#define SYNTH_SMALLDELAYVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uidropdownlist.h"

namespace rage
{	
	class synthSmallDelay;
	class synthSmallDelayView : public synthModuleView
	{
	public:

		synthSmallDelayView(synthModule *module);
		~synthSmallDelayView();

		virtual void Update();
		
	protected:

		virtual bool StartDrag() const;

	private:

		synthSmallDelay *GetDelay() const { return (synthSmallDelay*)m_Module; }
		synthUIDropDownList m_Mode;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_SMALLDELAYVIEW_H

