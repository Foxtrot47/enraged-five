// 
// audiosynth/convertor.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "convertor.h"
#include "synthutil.h"

namespace rage 
{
	using namespace Vec;

	synthSignalConverter::synthSignalConverter()
	{
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);

		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[0].SetConvertsInternally(false);
	}

	void synthSignalConverter::Synthesize(synthContext &UNUSED_PARAM(context))
	{	
		if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
		{
			// output is signal
			m_Outputs[0].SetStaticValue(m_Inputs[0].GetSignalValue(0));
			m_Outputs[0].SetDataFormat(synthPin::SYNTH_PIN_DATA_SIGNAL);
		}
		else
		{
			// output is normalised
			m_Outputs[0].SetDataFormat(synthPin::SYNTH_PIN_DATA_NORMALIZED);
			m_Outputs[0].SetStaticValue(m_Inputs[0].GetNormalizedValue(0));
		}
		
		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
			{
				// output is signal
				synthUtil::ConvertBufferToSignal(m_Buffer.GetBuffer(), m_Inputs[0].GetDataBuffer()->GetBuffer(), m_Buffer.GetSize());
			}
			else
			{
				// output is normalized
				synthUtil::ConvertBufferToNormalised(m_Buffer.GetBuffer(), m_Inputs[0].GetDataBuffer()->GetBuffer(), m_Buffer.GetSize());
			}
		}
	}
} // namespace rage
