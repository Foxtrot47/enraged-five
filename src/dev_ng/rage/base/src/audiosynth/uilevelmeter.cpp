#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uilevelmeter.h"
#include "audiohardware/driverutil.h"

namespace rage
{

	synthUIBarMeter::synthUIBarMeter()
	{
		m_Level = 0.f;
		m_Width = 0.33f;
		m_Height = 1.5f;
	}

	void synthUIBarMeter::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		const float halfWidth = m_Width * 0.5f;
		const float halfHeight = m_Height * 0.5f;
		v1 = Vector3(-halfWidth, -halfHeight, 0.f);
		v2 = Vector3(halfWidth, halfHeight, 0.f);
		Matrix34 mat = GetMatrix();
		mat.Transform(v1);
		mat.Transform(v2);
	}

	void synthUIBarMeter::Update()
	{
		
	}

	void synthUIBarMeter::Render()
	{
		Matrix34 mat = GetMatrix();
		const float height = ComputeHeight();

		const float level = Clamp(m_Level, 0.f, 1.f);
		mat.d -= mat.b * 0.5f * (height - (level*m_Height));
		mat.Scale(Vector3(0.5f*m_Width, 0.5f*level*m_Height, 1.f));
		
		grcWorldMtx(mat);
		synthUIComponent::SetShaderVar("g_CurrentValue",0.f);
		DrawVB("drawbg");

		Vector3 v1,v2;
		GetBounds(v1,v2);
		grcDrawBox(v1,v2,Color32(1.f,1.f,1.f,1.f));
	}

	synthUILevelMeter::synthUILevelMeter()
	{
		m_Label.SetSize(0.15f);
		m_Layout.Add(&m_Label);
		m_Layout.Add(&m_Meter);
		m_LevelLabel.SetSize(0.15f);
		m_Layout.Add(&m_LevelLabel);

		m_Layout.SetOrientation(synthUIHorizontalLayout::kVertical);
		AddComponent(&m_Layout);

		m_FixedWidth = 1.f;
	}

	void synthUILevelMeter::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		synthUIView::GetBounds(v1,v2);
		
		const float halfWidth = m_FixedWidth * 0.5f;
		Vector3 width1 = Vector3(-halfWidth, 0.f, 0.f);
		Vector3 width2 = Vector3(halfWidth, 0.f, 0.f);
		Matrix34 mat = GetMatrix();
		mat.Transform(width1);
		mat.Transform(width2);

		v1.x = width1.x;	
		v2.x = width2.x;
	}

	void synthUILevelMeter::Update()
	{
		Matrix34 mat = GetMatrix();
		m_Layout.SetMatrix(mat);

		Color32 textColour = Color32(1.f,1.f,1.f,1.f);
		if(m_Meter.GetLevel() > 1.f)
		{
			textColour = Color32(1.f,0.f,0.f,1.f);			
		}

		m_LevelLabel.GetLabel().SetActiveTextColour(textColour);
		m_LevelLabel.GetLabel().SetInactiveTextColour(textColour);
		m_Label.GetLabel().SetActiveTextColour(textColour);
		m_Label.GetLabel().SetInactiveTextColour(textColour);

		formatf(m_LevelText, "%.2fdB", audDriverUtil::ComputeDbVolumeFromLinear_Precise(m_Meter.GetLevel()));
		m_LevelLabel.SetString(m_LevelText);
		synthUIView::Update();

	}
} // namespace rage

#endif // __SYNTH_EDITOR

