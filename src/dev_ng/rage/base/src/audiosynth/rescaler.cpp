// 
// audiosynth/rescaler.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rescaler.h"

#if !SYNTH_MINIMAL_MODULES

#include "rescalerview.h"
#include "math/amath.h"

#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	SYNTH_EDITOR_VIEW(synthRescaler);

	synthRescaler::synthRescaler()
	{
		m_Inputs[INPUT].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[INPUT].SetConvertsInternally(false);

		m_Inputs[MIN_INPUT].Init(this, "MinInput", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[MAX_INPUT].Init(this, "MaxInput", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[MIN_OUTPUT].Init(this, "MinOutput", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[MAX_OUTPUT].Init(this, "MaxOutput", synthPin::SYNTH_PIN_DATA_NORMALIZED);

		m_Inputs[MAX_INPUT].SetStaticValue(1.f);
		m_Inputs[MAX_OUTPUT].SetStaticValue(1.f);

		m_Outputs[0].Init(this,"Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
	}

	void synthRescaler::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("ClampInput", m_ClampInput);
	}

	void synthRescaler::Synthesize()
	{
		m_Outputs[0].SetDataFormat(m_Inputs[INPUT].GetDataFormat());

		bool areAllInputsStatic = true;
		for(u32 i = 0; i < NUM_RESCALER_INPUTS; i++)
		{
			if(m_Inputs[i].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				areAllInputsStatic = false;
				break;
			}
		}

		const synthPin::synthPinDataFormat inputFormat = m_Inputs[INPUT].GetDataFormat();
		m_Inputs[MIN_INPUT].SetDataFormat(inputFormat);
		m_Inputs[MAX_INPUT].SetDataFormat(inputFormat);
		m_Inputs[MIN_OUTPUT].SetDataFormat(inputFormat);
		m_Inputs[MAX_OUTPUT].SetDataFormat(inputFormat);
		m_Outputs[0].SetDataFormat(inputFormat);

		const bool clamp = m_ClampInput;
		if(areAllInputsStatic)
		{
			if(inputFormat == synthPin::SYNTH_PIN_DATA_NORMALIZED)
			{			
				const f32 input = m_Inputs[INPUT].GetNormalizedValue(0);
				const f32 minInput = m_Inputs[MIN_INPUT].GetNormalizedValue(0);
				const f32 maxInput = m_Inputs[MAX_INPUT].GetNormalizedValue(0);
				const f32 minOutput = m_Inputs[MIN_OUTPUT].GetNormalizedValue(0);
				const f32 maxOutput = m_Inputs[MAX_OUTPUT].GetNormalizedValue(0);

				const f32 inputRange = Max(SMALL_FLOAT, maxInput - minInput);
				const f32 normalizedInput = (input - minInput) / inputRange;
				const f32 clampedNormalizedInput = clamp ? Clamp(normalizedInput, 0.f, 1.f)	: normalizedInput;
				
				const f32 output = Lerp(clampedNormalizedInput, minOutput, maxOutput);
				m_Outputs[0].SetStaticValue(output);	
			}
			else
			{			
				const f32 input = m_Inputs[INPUT].GetSignalValue(0);
				const f32 minInput = m_Inputs[MIN_INPUT].GetSignalValue(0);
				const f32 maxInput = m_Inputs[MAX_INPUT].GetSignalValue(0);
				const f32 minOutput = m_Inputs[MIN_OUTPUT].GetSignalValue(0);
				const f32 maxOutput = m_Inputs[MAX_OUTPUT].GetSignalValue(0);

				const f32 inputRange = Max(SMALL_FLOAT, maxInput - minInput);	
				const f32 normalizedInput = (input - minInput) / inputRange;
				const f32 clampedNormalizedInput = clamp ? Clamp(normalizedInput, 0.f, 1.f)	: normalizedInput;
				const f32 output = Lerp(clampedNormalizedInput, minOutput, maxOutput);
				m_Outputs[0].SetStaticValue(output);			
			}
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			Vector_4V *RESTRICT outputBuf = (Vector_4V*)m_Outputs[0].GetDataBuffer()->GetBuffer();
			
			if(m_Inputs[INPUT].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
			{			
				for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
				{
					const Vector_4V input = m_Inputs[INPUT].GetNormalizedValueV(i);
					const Vector_4V minInput = m_Inputs[MIN_INPUT].GetNormalizedValueV(i);
					const Vector_4V maxInput = m_Inputs[MAX_INPUT].GetNormalizedValueV(i);
					const Vector_4V minOutput = m_Inputs[MIN_OUTPUT].GetNormalizedValueV(i);
					const Vector_4V maxOutput = m_Inputs[MAX_OUTPUT].GetNormalizedValueV(i);

					const Vector_4V inputRange = V4Subtract(maxInput, minInput);					
					const Vector_4V normalizedInput = V4InvScaleSafe(V4Subtract(input, minInput), inputRange, V4VConstant(V_ZERO));
					const Vector_4V clampedNormalizedInput = clamp ? V4Clamp(normalizedInput, V4VConstant(V_ZERO), V4VConstant(V_ONE))
																				: normalizedInput;
					const Vector_4V output = V4Lerp(clampedNormalizedInput, minOutput, maxOutput);
					*outputBuf++ = output;
				}
			}
			else
			{
				for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
				{
					const Vector_4V input = m_Inputs[INPUT].GetSignalValueV(i);
					const Vector_4V minInput = m_Inputs[MIN_INPUT].GetSignalValueV(i);
					const Vector_4V maxInput = m_Inputs[MAX_INPUT].GetSignalValueV(i);
					const Vector_4V minOutput = m_Inputs[MIN_OUTPUT].GetSignalValueV(i);
					const Vector_4V maxOutput = m_Inputs[MAX_OUTPUT].GetSignalValueV(i);

					const Vector_4V inputRange = V4Subtract(maxInput, minInput);					
					const Vector_4V normalizedInput = V4InvScaleSafe(V4Subtract(input, minInput), inputRange, V4VConstant(V_ZERO));
					const Vector_4V clampedNormalizedInput = clamp ? V4Clamp(normalizedInput, V4VConstant(V_ZERO), V4VConstant(V_ONE))
						: normalizedInput;
					const Vector_4V output = V4Lerp(clampedNormalizedInput, minOutput, maxOutput);
					*outputBuf++ = output;
				}
			}
		}
	}

	void synthRescaler::Process(float *RESTRICT const inOutBuffer, const float *RESTRICT const minInBuffer, const float *RESTRICT const maxInBuffer, const float *RESTRICT const minOutBuffer, const float *RESTRICT const maxOutBuffer, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		const Vector_4V *minInPtr = reinterpret_cast<const Vector_4V*>(minInBuffer);
		const Vector_4V *maxInPtr = reinterpret_cast<const Vector_4V*>(maxInBuffer);
		const Vector_4V *minOutPtr = reinterpret_cast<const Vector_4V*>(minOutBuffer);
		const Vector_4V *maxOutPtr = reinterpret_cast<const Vector_4V*>(maxOutBuffer);
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V input = *inOutPtr;
			const Vector_4V minInput = *minInPtr++;
			const Vector_4V maxInput = *maxInPtr++;
			const Vector_4V minOutput = *minOutPtr++;
			const Vector_4V maxOutput = *maxOutPtr++;

			const Vector_4V normalizedInput = V4Clamp(V4RangeSafe(input, minInput, maxInput, V4VConstant(V_ZERO)), V4VConstant(V_ZERO), V4VConstant(V_ONE));
			*inOutPtr++ = V4Lerp(normalizedInput, minOutput, maxOutput);
		}
	}

	void synthRescaler::Process(float *RESTRICT const inOutBuffer, const float minIn, const float maxIn, const float minOut, const float maxOut, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		const Vector_4V minInput = V4LoadScalar32IntoSplatted(minIn);
		const Vector_4V maxInput = V4LoadScalar32IntoSplatted(maxIn);
		const Vector_4V minOutput = V4LoadScalar32IntoSplatted(minOut);
		const Vector_4V maxOutput = V4LoadScalar32IntoSplatted(maxOut);
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V input = *inOutPtr;
			const Vector_4V unclampedNormInput = V4RangeSafe(input, minInput, maxInput, V4VConstant(V_ZERO));
			const Vector_4V normalizedInput = V4Clamp(unclampedNormInput, V4VConstant(V_ZERO), V4VConstant(V_ONE));
			*inOutPtr++ = V4Lerp(normalizedInput, minOutput, maxOutput);
		}
	}

	float synthRescaler::Process(const float input, const float minIn, const float maxIn, const float minOut, const float maxOut)
	{
		const float normalizedInput = ClampRange(input, minIn, maxIn);
		return Lerp(normalizedInput, minOut, maxOut);
	}

	void synthRescaler::Process(float *RESTRICT const inOutBuffer, const float minOut, const float maxOut, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		
		const Vector_4V minOutput = V4LoadScalar32IntoSplatted(minOut);
		const Vector_4V maxOutput = V4LoadScalar32IntoSplatted(maxOut);
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V normalizedInput = *inOutPtr;
			*inOutPtr++ = V4Lerp(normalizedInput, minOutput, maxOutput);
		}
	}

	void synthRescaler::Process(float *RESTRICT const inOutBuffer, float *RESTRICT const otherBuffer, const float t, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		Vector_4V *bPtr = reinterpret_cast<Vector_4V*>(otherBuffer);
		const Vector_4V interp = V4LoadScalar32IntoSplatted(t);
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V a = *inOutPtr;
			*inOutPtr++ = V4Lerp(interp, a, *bPtr++);
		}
	}

	void synthRescaler::Process(float *const inOutBuffer, const float *const inA, const float *const inB, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		const Vector_4V *aPtr = reinterpret_cast<const Vector_4V*>(inA);
		const Vector_4V *bPtr = reinterpret_cast<const Vector_4V*>(inB);
		
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V t = *inOutPtr;
			*inOutPtr++ = V4Lerp(t, *aPtr++, *bPtr++);
		}
	}
} // namespace rage
#endif
