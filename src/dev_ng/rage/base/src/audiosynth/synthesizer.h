//
// audiosynth/synthesizer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SYNTHESIZER_H
#define SYNTHESIZER_H

#include "atl/array.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/dspeffect.h"
#include "audioengine/metadatamanager.h"
#include "audiohardware/mixer.h"
#include "audiohardware/pcmsource.h"
#include "audiohardware/ringbuffer.h"
#include "system/criticalsection.h"

#include "vector/vector3.h"
#include "vector/matrix34.h"

#include "module.h"
#include "synthcontext.h"
#include "synthdefs.h"

namespace rage
{

#if __SYNTH_EDITOR
	class synthUIViewGroup;
	class synthCommentBox;
	struct synthUIObjects
	{
		Matrix34 rootMatrix;
		atArray<Vector3> cameraPositions;
		atArray<synthUIViewGroup *> groups;
		atArray<synthCommentBox *> commentBoxes;		
	};
#else
	struct synthUIObjects
	{

	};
#endif

	class synthSampleFrame;
	class synthModule;
	class synthPin;
	class synthAudioInput;
	class synthAudioOutput;
	struct ModularSynth;
	struct Preset;
	class fiStream;
	class synthSynthesizer 
#if __SYNTH_EDITOR
		: public audPcmSource
#endif
	{
	public:

		

		static bool InitClass();
		static void ShutdownClass();

		static const Preset *FindPreset(const char *name);
		static const Preset *FindPreset(const u32 nameHash);

		static audMetadataManager &GetMetadataManager()
		{
			return sm_MetadataMgr;
		}

		// PURPOSE
		//	Safely unload the specified metadata chunk, by marking it for delete and waiting for a mixer frame to pass
		static void UnloadMetadataChunk(const char *chunkName);

		// PURPOSE
		//	Returns true if this metadata object has been marked to unload
		static bool IsMetadataUnloading(const void *ptr) { return ptr >= sm_UnloadingMetadataStartPtr && ptr < sm_UnloadingMetadataEndPtr; }

		static const CompiledSynth *FindOptimizedMetadata(const u32 nameHash);
		
#if __SYNTH_EDITOR
		
		synthSynthesizer();
		~synthSynthesizer() { Shutdown(); }

		static s32 Create(const char *name, synthUIObjects *uiObjects = NULL);
		static s32 Create(const u32 nameHash, const u32 presetHash, synthUIObjects *uiObjects = NULL);

		static s32 Create(const ModularSynth *const instanceData, const Preset *const presetData, synthUIObjects *uiObjects = NULL);

		static const ModularSynth *FindMetadata(const char *name);
		static const ModularSynth *FindMetadata(const u32 nameHash);
		
		static synthModule *InstantiateModule(const synthBinaryModuleReader &moduleReader);

		atArray<synthModule*> &GetModules()
		{
			return m_Modules;
		}
		
		synthAudioOutput *GetOutput(const u32 index);
		synthAudioInput *GetInput(const u32 index);

		void ComputeProcessingGraph();

		void Serialize(fiStream *stream, const char *synthName);

		void ApplyPreset(const Preset *preset);

		
		// audPcmSource functionality
		void GenerateFrame();		
		void StartPhys(s32 channel);
		void StopPhys(s32 channel);
		void Start();
		void Stop();
		void SetParam(const u32 paramId, const f32 val);
		void SetParam(const u32 paramId, const u32 val);

		void ProcessDsp(const atRangeArray<u16, g_MaxOutputChannels> &bufferIds);

		void ResetProcessing();

		// PURPOSE
		//	Returns the headroom of the generated PCM in decibels
		// NOTES
		//	This will be subtracted from the playback volume to allow compensation for normalized
		//	assets, and used when determining the loudness of this content.
		f32 GetHeadroom() const { return 0.f; }

		// PURPOSE
		//	Returns the length of the generated PCM in samples at the native sample rate, 
		//	or -1 for infinite/unknown
		s32 GetLengthSamples() const { return -1; }

		// PURPOSE
		//	Returns true if the PCM generator will loop around and continue producing
		//	output when it runs out of samples
		bool IsLooping() const { return false; }

		// PURPOSE
		//	Returns the current playback position of the generator, in samples at the native
		//	sample rate.
		u32 GetPlayPositionSamples() const { return 0; }

		// PURPOSE
		//	Returns true if this generator has finished producing audio
		bool IsFinished() const { return m_HasFinished; }

		void Shutdown();

		f32 GetCPUTime() const
		{
			return m_CPUTime;
		}

		// PURPOSE
		//	Sets a named variable to the specified value
		// RETURNS
		//	true if the variable name was matched, otherwise false
		bool SetVariableValue(const u32 nameHash, const f32 val);

		void AddModule(synthModule *module);

		void SerializeCurrentPreset(fiStream *stream, const char *presetName);
		void RemoveModule(synthModule *module);

		void SetAuditioningPin(synthPin *pin) { m_AuditioningPin = pin; }

		void Lock()
		{
			m_GraphCS.Lock();
		}
		void Unlock()
		{
			m_GraphCS.Unlock();
		}

		const char *GetName() const { return m_Name; }

		void SetName(const char *name)
		{
			formatf(m_Name, "%s", name);
		}

		void ToggleMute() { m_Muted = !m_Muted; }
		bool IsMuted() const { return m_Muted; }

		void ToggleFrozen() { m_Frozen = !m_Frozen; }
		bool IsFrozen() const { return m_Frozen; }

		bool ShouldExport() const { return m_ShouldExport; }
		void SetShouldExport(const bool should) { m_ShouldExport = should; }

		u32 GetOutputIndex(const synthModule *module);
		u32 GetNumOutputs() const { return m_OutputIndices.GetCount(); }
#endif
		
		static void SetUnloadingRegion(const void *start, const void *end)
		{
			sm_UnloadingMetadataStartPtr = start;
			sm_UnloadingMetadataEndPtr = end;
		}

		static const void *GetUnloadingRegionStart() { return sm_UnloadingMetadataStartPtr; }
		static const void *GetUnloadingRegionEnd() { return sm_UnloadingMetadataEndPtr; }
		
	private:
#if __SYNTH_EDITOR
		void Synthesize();	

		void ComputeProcessingGraph(synthModule *module, s32 stage, atArray<const synthModule *> &visitedModules, atArray<atArray<s32>> &moduleStageOutputs);
		void AddOutputStage(synthModule *module, s32 stage, atArray<const synthModule *> &visitedModules, atArray<atArray<s32>> &moduleStageOutputs);
		bool IsGraphRecursive(synthModule *moduleToSearchFor, atArray<const synthModule *> &visitedModules);


		atArray<synthModule*> m_Modules;
		synthPin *m_AuditioningPin;
		
		char m_Name[256];


		sysCriticalSectionToken m_GraphCS;
		synthContext m_Context;

		struct synthExposedVariable
		{
			u32 key;
			u32 moduleIndex;
			f32 value;
			bool changed;
		};
		enum {kMaxSynthVariables = 16};
		atFixedArray<synthExposedVariable,kMaxSynthVariables> m_Variables;
		enum {kMaxAudioOutputs = kMaxPcmSourceChannels};
		enum {kMaxAudioInputs = 8};
		atFixedArray<u32,kMaxAudioOutputs> m_OutputIndices;
		atFixedArray<u32,kMaxAudioInputs> m_InputIndices;
		
		s32 m_NumProcessingStages;
		f32 m_CPUTime;

		
		bool m_HasFinished;
		bool m_ShouldExport;
		bool m_Frozen;
		bool m_Muted;
#endif//__SYNTH_EDITOR

		static const void *sm_UnloadingMetadataStartPtr;
		static const void *sm_UnloadingMetadataEndPtr;
		
		static audMetadataManager sm_MetadataMgr;
		
	};

#if __SYNTH_EDITOR

	class synthSynthesizerDspEffect : public audDspEffect
	{
	public:

		synthSynthesizerDspEffect(synthSynthesizer *synth) 
			: audDspEffect(AUD_DSPEFFECT_SYNTHCORE)
			, m_Synth(synth)
		{

		}
		virtual bool Init(const u32 numChannels, const u32 channelMask);
		virtual void Shutdown();

		AUD_DECLARE_DSP_PROCESS_FUNCTION;

		virtual void SetParam(const u32 paramId, const f32 value);
		virtual void SetParam(const u32 paramId, const u32 value);

	private:
		synthSynthesizer *m_Synth;
		u8 m_NumInputChannels;
		u8 m_NumChannelsToProcess;
		u8 m_ChannelMask;
		bool m_IsInitialised;
		bool m_Bypass;
	};
#endif
}

#endif // SYNTHESIZER_H

