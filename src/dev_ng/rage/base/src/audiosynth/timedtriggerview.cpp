// 
// audiosynth/triggerview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "timedtriggerview.h"
#include "timedtrigger.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"
#include "uiplane.h"

namespace rage
{
	synthTimedTriggerView::synthTimedTriggerView(synthModule *module) : synthModuleView(module)
	{
		m_ModeSwitch.AddItem("One shot");
		m_ModeSwitch.AddItem("Retrigger");
		m_ModeSwitch.AddItem("Interruptible");
		AddComponent(&m_ModeSwitch);

		m_ModeSwitch.SetCurrentIndex((u32)GetTrigger()->GetMode());
	}

	void synthTimedTriggerView::Update()
	{
		Matrix34 mtx = GetMatrix();

		// Mode label		
		mtx.d -= mtx.b*0.75f;

		m_ModeSwitch.SetMatrix(mtx);		
		m_ModeSwitch.SetAlpha(GetHoverFade());
		m_ModeSwitch.SetShouldDraw(ShouldDraw() && !IsMinimised());

		SetHideTitle(!IsMinimised() && m_ModeSwitch.IsShowingMenu());
		GetTrigger()->SetMode((TriggerMode)m_ModeSwitch.GetCurrentIndex());

		synthModuleView::Update();
	}

	bool synthTimedTriggerView::StartDrag() const
	{
		if(m_ModeSwitch.IsUnderMouse() || m_ModeSwitch.IsShowingMenu() || m_TitleLabel.IsUnderMouse())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthTimedTriggerView::IsActive() const
	{
		return (IsUnderMouse());
	}
}
#endif // __SYNTH_EDITOR

