// 
// audiosynth/uibutton.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UIBUTTON_H
#define SYNTH_UIBUTTON_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uilabel.h"
#include "uiview.h"

#include "vector/vector3.h"

namespace rage
{
	class synthUIButton : public synthUIView
	{
	public:
		synthUIButton();
		virtual ~synthUIButton();

		void SetTitle(const char *title)
		{
			formatf(m_Title,title);
			m_Label.SetString(m_Title);
		}

        void SetFixedSize(const Vector3 &halfSize) { m_IsFixedSize = true; m_FixedHalfSize = halfSize; }

		const char *GetTitle() const { return m_Title; }

		// synthUIComponent functionality
		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		void SetEdgeColour(const Color32 color)
		{
			m_EdgeColour = color;
		}

        void SetSize(const float size) { m_Size = size; }

		bool HadClick() const;

		virtual bool StartDrag() const { return false; } 
		virtual bool IsActive() const { return IsUnderMouse(); }

	protected:
		virtual void Render();
	private:

        Vector3 m_FixedHalfSize;
		char m_Title[64];
		Color32 m_EdgeColour;
		synthUILabel m_Label;
        float m_Size;

        bool m_IsFixedSize;

	};

} // namespace rage
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIMENUITEM_H
