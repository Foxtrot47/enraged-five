// 
// audiosynth/junctionview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_JUNCTIONVIEW_H
#define SYNTH_JUNCTIONVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"

namespace rage
{
	class synthJunctionPin;
	class synthJunctionPinView : public synthModuleView
	{
	public:
		synthJunctionPinView(synthModule *module);
		~synthJunctionPinView();
		virtual void Update();

		virtual bool StartDrag() const;
		void GetBounds(Vector3 &v1, Vector3 &v2) const;

	protected:
		f32 GetModuleWidth() const { return 2.f; }
		virtual void Render();
		virtual void DrawBackground() const {}
		virtual bool DefaultPinPositioning() const { return false; }
	private:

		synthJunctionPin *GetJunction() const { return (synthJunctionPin*)m_Module; }

		Vector3 m_LastPosition;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_JUNCTIONVIEW_H

