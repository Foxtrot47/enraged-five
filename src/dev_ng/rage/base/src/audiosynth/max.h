// 
// audiosynth/max.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_MAX_H
#define SYNTH_MAX_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "pin.h"

#include "modulefactory.h"

namespace rage
{
	enum
	{
		kMaxSignal = 0,
		kMaxReset,

		kNumMaxInputs
	};

	class synthMax : public synthModuleBase<kNumMaxInputs,1,SYNTH_MAX>
	{
	public:

		SYNTH_MODULE_NAME("Max");

		synthMax();

		virtual void Synthesize();
		virtual void ResetProcessing();

		enum synthMaxMode
		{
			kMaxStatic = 0,
			kMaxDynamic
		};

		void SetMode(const synthMaxMode mode) { m_Mode = mode; }
		synthMaxMode GetMode() const { return m_Mode; }

		static void Process(float *RESTRICT const inOutBuffer, const float resetTrigger, Vec::Vector_4V_InOut state, const u32 numSamples);
		static float Process(const float input, const float resetTrigger, Vec::Vector_4V_InOut state);


	private:

		synthSampleFrame m_Buffer;
		synthMaxMode m_Mode;

		f32 m_CurrentVal;
	};
}
#endif
#endif // SYNTH_MAX_H


