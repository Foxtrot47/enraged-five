// 
// audiosynth/variableinputview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "inputsmoother.h"
#include "module.h"
#include "moduleview.h"
#include "pin.h"
#include "pinview.h"
#include "synthesizerview.h"
#include "uiplane.h"
#include "variableinputview.h"

namespace rage
{
	synthVariableInputView::synthVariableInputView(synthModule *module) : synthModuleView(module)
	{
		AddComponent(&m_ExportedName);
		m_InputBox.SetShouldDraw(false);
	}

	void synthVariableInputView::Notify_SetValue()
	{
		GetVariableInput()->GetInput(0).GetView()->Notify_SetValue();
	}

	void synthVariableInputView::Update()
	{		
		Matrix34 mat = GetMatrix();
		Vector3 offset = mat.b * 0.5f;
		mat.d -= offset;
		m_ExportedName.SetMatrix(mat);
		const char *exportedName = GetVariableInput()->GetExportedKeyName();
		m_ExportedName.SetString(exportedName ? exportedName : "_variable name");
		m_ExportedName.SetSize(0.25f);

		synthModuleView::Update();

		if(m_InputBox.ShouldDraw())
		{
			m_InputBox.Update();
			if(m_InputBox.HadEnter())
			{
				if(strlen(m_InputBox.GetKeyboardBuffer()))
				{
					GetVariableInput()->SetExportedKey(m_InputBox.GetKeyboardBuffer());
				}
				synthSynthesizerView::ClearModalComponent();
				m_InputBox.SetShouldDraw(false);
			}
		}
		else if(m_ExportedName.IsUnderMouse() && synthUIPlane::GetMaster()->HadLeftDoubleClick())
		{
			if(synthSynthesizerView::SetModalComponent(&m_InputBox))
			{
				m_InputBox.Reset();
				m_InputBox.SetAllowAlphaChars(true);
				m_InputBox.SetTitle("Variable Name");
				m_InputBox.SetShouldDraw(true);
			}
		}
	}

	bool synthVariableInputView::StartDrag() const
	{
		if(m_ExportedName.IsUnderMouse())
		{
			return false;
		}
		return synthModuleView::StartDrag();
	}

	void synthVariableInputView::Render()
	{
		synthModuleView::Render();

		m_InputBox.Draw();
	}
}

#endif // __SYNTH_EDITOR
