// 
// audiosynth/audioinputview.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "audioinputview.h"
#include "audioinput.h"

#include "moduleview.h"
#include "pinview.h"
#include "uiplane.h"

#include "module.h"
#include "pin.h"

namespace rage
{
	synthAudioInputView::synthAudioInputView(synthModule *module) : synthModuleView(module)
	{
		AddComponent(&m_Layout);

		m_InputList.AddItem("Input 1");
		m_InputList.AddItem("Input 2");
		m_InputList.AddItem("Input 3");
		m_InputList.AddItem("Input 4");
		m_InputList.AddItem("Input 5");
		m_InputList.AddItem("Input 6");
		m_InputList.AddItem("Input 7");
		m_InputList.AddItem("Input 8");

		m_InputList.SetCurrentIndex(GetAudioInput()->GetInputId());
	
		m_Layout.Add(&m_InputList);
	
		m_Layout.SetOrientation(synthUIHorizontalLayout::kVertical);
		m_Layout.SetPadding(0.5f);
	}

	void synthAudioInputView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		synthModuleView::GetBounds(v1,v2);
		Vector3 v3,v4;
		m_InputList.GetBounds(v3,v4);

		v1.Min(v1,v3);
		v2.Max(v2,v4);
	}

	void synthAudioInputView::Update()
	{
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();
		mtx.d -= mtx.c * 0.1f;
		mtx.d -= mtx.b * 0.5f;
		m_Layout.SetMatrix(mtx);

		GetAudioInput()->SetInputId(m_InputList.GetCurrentIndex());

		synthModuleView::Update();
	}

	void synthAudioInputView::Render()
	{
		synthModuleView::Render();
	}

	bool synthAudioInputView::StartDrag() const
	{
		if(m_Layout.IsActive())
		{
			return false;
		}
		else if(m_InputList.IsShowingMenu())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthAudioInputView::IsActive() const
	{
		return (IsUnderMouse() || m_InputList.IsActive());
	}

}
#endif // __SYNTH_EDITOR

