

#ifndef SYNTH_JUNCTIONPIN_H
#define SYNTH_JUNCTIONPIN_H

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthJunctionPin : public synthModuleBase<1,1,SYNTH_JUNCTIONPIN>
	{
	public:
		SYNTH_MODULE_NAME("Junction");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthJunctionPin();
		
		virtual void Synthesize();

	private:

	};
}

#endif // SYNTH_JUNCTIONPIN_H


