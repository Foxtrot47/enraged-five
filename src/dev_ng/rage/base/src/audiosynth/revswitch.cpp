// 
// audiosynth/revswitch.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "revswitch.h"

#if !SYNTH_MINIMAL_MODULES

#include "revswitchview.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthReverseSwitch);

	synthReverseSwitch::synthReverseSwitch()
	{
		m_Inputs[SIGNAL].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[SIGNAL].SetStaticValue(0.f);

		m_Inputs[SWITCH].Init(this, "Value", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[SWITCH].SetStaticValue(0.f);

		for(u32 i = 0; i < kMaxSwitchOutputs; i++)
		{
			m_Outputs[i].Init(this, synthPin::sm_PinNumberStrings[i], synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
			m_Outputs[i].SetStaticValue(0.f);
		}

		for(u32 i = 0; i < kReverseSwitchNumInputs; i++)
		{
			m_Inputs[i].SetConvertsInternally(false);
		}

		m_NumSwitchOutputs = 1;
		m_IsNormalizedInput = true;
	}

	void synthReverseSwitch::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("NumSwitchOutputs", m_NumSwitchOutputs);
		serializer->SerializeField("NormalizedInput", m_IsNormalizedInput);
	}

	void synthReverseSwitch::GetOutputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Outputs[0];
		numPins = m_NumSwitchOutputs;
	}

	void synthReverseSwitch::Synthesize()
	{
		const u32 index = GetActiveOutputIndex();
		for(u32 i = 0; i < m_NumSwitchOutputs; i++)
		{
			if(i != index)
			{
				m_Outputs[i].SetStaticValue(0.f);
			}
			else
			{
				if(m_Inputs[SIGNAL].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
				{
					m_Outputs[index].SetDataBuffer(m_Inputs[SIGNAL].GetDataBuffer());
				}
				else
				{
					m_Outputs[index].SetStaticValue(m_Inputs[SIGNAL].GetStaticValue());
				}
			}
			m_Outputs[i].SetDataFormat(m_Inputs[SIGNAL].GetDataFormat());
		}
	}

	u32 synthReverseSwitch::GetActiveOutputIndex() const
	{
		u32 index = 0;
		const f32 unscaledInput = m_Inputs[SWITCH].GetNormalizedValue(0);
		if(m_IsNormalizedInput)
		{
			index = (u32)(Clamp(unscaledInput,0.f,1.f) * m_NumSwitchOutputs);
			if(index >= m_NumSwitchOutputs)
				index = m_NumSwitchOutputs-1;
		}
		else
		{
			index = (u32)unscaledInput % m_NumSwitchOutputs;
		}

		return index;
	}
}
#endif

