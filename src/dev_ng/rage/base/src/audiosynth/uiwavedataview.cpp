// 
// audiosynth/uiwavedataview.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "synthesizerview.h"
#include "uiplane.h"
#include "uiwavedataview.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "grcore/viewport.h"

namespace rage
{

synthUIWaveDataView::synthUIWaveDataView()
: m_WaveData(NULL)
, m_Texture(NULL)
, m_UpdateRegionStart(0)
, m_UpdateRegionEnd(0)
{
    
}

synthUIWaveDataView::~synthUIWaveDataView()
{
    if(m_Texture)
    {
        m_Texture->Release();
        m_Texture = NULL;
    }
}

void synthUIWaveDataView::GetBounds(Vector3 &v1, Vector3 &v2) const
{
    Vector3 halfSize(0.5f * m_Width, 0.5f * m_Height, 0.f);
    const Matrix34 mat = GetMatrix();
    v1 = mat.d - halfSize;
    v2 = mat.d + halfSize;
}

void synthUIWaveDataView::Update()
{
    if(m_WaveData)
    {
        if(m_Texture == NULL)
        {
            const u32 numSamplesToRender = 512;
            const u32 heightInPixels = 256;
			m_Texture = synthUIComponent::CreateTexture(numSamplesToRender, heightInPixels, grcImage::R32F);		
        }

        if(m_Texture)
        {
            grcTextureLock lock;
            m_Texture->LockRect(0,0,lock,grcsWrite);
            f32 *texturePtr = (f32*)lock.Base;

            s32 width = m_Texture->GetWidth();
            s32 height = m_Texture->GetHeight();

            /*s32 start = m_UpdateRegionStart, end = m_UpdateRegionEnd;

            //s32 regionSize = Abs(end - start) * 50;
            //start = (start + regionSize + m_WaveLengthSamples) % m_WaveLengthSamples;
            //end = (end + regionSize) % m_WaveLengthSamples;

            Assert(start >= 0);
            Assert(start < (s32)m_WaveLengthSamples);
            Assert(end < (s32)m_WaveLengthSamples);
            Assert(end >= 0);

            if(start > end)
            {
                RenderRegion(texturePtr, width, height, start, m_WaveLengthSamples-1);
                RenderRegion(texturePtr, width, height, 0, end);
            }
            else
            {
                RenderRegion(texturePtr, width, height, start, end);
            }
            
*/
            RenderRegion(texturePtr, width, height, 0, m_WaveLengthSamples);
            //RenderRegion(texturePtr, width, height, m_WaveLengthSamples>>1, m_WaveLengthSamples);
            m_Texture->UnlockRect(lock);

        }
    }
    synthUIView::Update();
}

void synthUIWaveDataView::RenderRegion(float *textureData, const s32 width, const s32 height, const s32 start, const s32 end) const
{
    float pixelIncremement = width / (float)m_WaveLengthSamples;

    float pixelX = start * pixelIncremement;
    s32 endPixelX = (s32)(0.5f + end * pixelIncremement);
    s32 startPixelX = (s32)pixelX;

    // erase this segment
    for(s32 x = startPixelX; x < endPixelX; x++)
    {
        for(s32 y = 0; y < height; y++)
        {
            s32 offset = x + y*width;
            if(offset < width*height)
            {
                textureData[offset] = 0.f;
            }
        }
    }

    for(s32 i = start; i < end; i++)
    {
        float sampleValue = 0.5f * (1.f + m_WaveData[i]);

        s32 sampleY = Min((s32)(round(sampleValue * height)), height-1);

        const s32 sampleX = (s32)round(pixelX);

       
        pixelX += pixelIncremement;

        s32 offset = Clamp(sampleY*width + sampleX, 0, width*height - 1);
        textureData[offset] = Min(1.f, textureData[offset] + 0.15f);
    }
}

void synthUIWaveDataView::Render()
{
    synthUIView::Render();

    Vector3 scalingVec(m_Width * 0.5f,m_Height * 0.5f,1.f);
    Matrix34 mtx = GetMatrix();
    mtx.Transform3x3(scalingVec);
    mtx.Scale(scalingVec);
    grcWorldMtx(mtx);


    SetShaderVar("FontTex",m_Texture);
    DrawVB("drawtexturedquad");
}

}//namespace rage
#endif // __SYNTH_EDITOR
