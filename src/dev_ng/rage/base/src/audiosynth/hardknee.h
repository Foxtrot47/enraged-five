
#ifndef SYNTH_HARDKNEE_H
#define SYNTH_HARDKNEE_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"
#include "vectormath/vectormath.h"

namespace rage
{
	enum synthHardKneeInputs
	{
		kHardKneeInputSignal= 0,
		kHardKneeInputKnee,
		
		kHardKneeNumInputs
	};
	class synthHardKnee : public synthModuleBase<kHardKneeNumInputs,1,SYNTH_HARDKNEE>
	{
	public:
		SYNTH_MODULE_NAME("HardKnee");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthHardKnee();
		virtual void Synthesize();

		static void Process(float *RESTRICT const inOutBuffer, const float *RESTRICT const kneeBuffer, const u32 numSamples);
		static void Process(float *RESTRICT const inOutBuffer, const Vec::Vector_4V_In knee, const u32 numSamples);
		static float Process(const float input, const float knee);
		

	private:

		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_EXP_H


