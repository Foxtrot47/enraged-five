// 
// audiosynth/randomizerview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "pinview.h"
#include "randomizerview.h"

#include "module.h"
#include "pin.h"
#include "randomizer.h"
#include "uiplane.h"

namespace rage
{

	synthRandomizerView::synthRandomizerView(synthModule *module) : synthModuleView(module)
	{
		m_Layout.SetOrientation(synthUIHorizontalLayout::kVertical);

		AddComponent(&m_Layout);

		m_QuantizeSwitch.AddItem("Normal");
		m_QuantizeSwitch.AddItem("Quantized");
		m_Layout.Add(&m_QuantizeSwitch);

		m_QuantizeSwitch.SetCurrentIndex(GetRandomizerModule()->IsQuantised() ? 1 : 0);

		m_ModeSwitch.AddItem("Triggered");
		m_ModeSwitch.AddItem("OnInit");
		m_Layout.Add(&m_ModeSwitch);

		m_ModeSwitch.SetCurrentIndex(GetRandomizerModule()->GetMode());
	}

	void synthRandomizerView::Update()
	{
		m_TitleLabel.SetUnderMouse(IsUnderMouse());
				
		Matrix34 mtx = GetMatrix();
		mtx.d -= mtx.b * 1.f;
		m_Layout.SetMatrix(mtx);
		
		m_QuantizeSwitch.SetAlpha(GetHoverFade());
		m_ModeSwitch.SetAlpha(GetHoverFade());

		GetRandomizerModule()->SetIsQuantised(m_QuantizeSwitch.GetCurrentIndex() == 1);
		GetRandomizerModule()->SetMode(static_cast<synthRandomizer::Mode>(m_ModeSwitch.GetCurrentIndex()));

		m_QuantizeSwitch.SetShouldDraw(ShouldDraw() && !IsMinimised() && !m_ModeSwitch.IsShowingMenu());
		m_ModeSwitch.SetShouldDraw(ShouldDraw() && !IsMinimised() && !m_QuantizeSwitch.IsShowingMenu());

		SetHideTitle(m_QuantizeSwitch.IsShowingMenu() || m_ModeSwitch.IsShowingMenu());
		
		synthModuleView::Update();
	}

	void synthRandomizerView::Render()
	{
		synthModuleView::Render();
	}

	bool synthRandomizerView::StartDrag() const
	{
		return !m_QuantizeSwitch.IsUnderMouse() && !m_QuantizeSwitch.IsShowingMenu() && 
			!m_ModeSwitch.IsUnderMouse() && !m_ModeSwitch.IsShowingMenu() &&
			synthModuleView::StartDrag();
	}

	bool synthRandomizerView::IsActive() const
	{
		return (IsUnderMouse());
	}


} // namespace rage
#endif // __SYNTH_EDITOR

