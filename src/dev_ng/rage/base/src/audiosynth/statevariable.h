
#ifndef SYNTH_STATEVARIABLE_H
#define SYNTH_STATEVARIABLE_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum synthSVFilterInputs
	{
		kStateVariableFilterSignal = 0,
		kStateVariableFilterGain,
		kStateVariableFilterFc,
		kStateVariableFilterQ,

		kStateVariableFilterNumInputs
	};

	enum synthSVFilterOutputs
	{
		kStateVariableFilterLowpass= 0,
		kStateVariableFilterBandpass,
		kStateVariableFilterHighpass,

		kStateVariableFilterNumOutputs
	};

	class synthStateVariableFilter : 
		public synthModuleBase<kStateVariableFilterNumInputs, kStateVariableFilterNumOutputs, SYNTH_SVFILTER>
	{
		public:
			SYNTH_MODULE_NAME("SVFilter");
			SYNTH_CUSTOM_MODULE_VIEW;

			synthStateVariableFilter();
			virtual ~synthStateVariableFilter(){};

			virtual void ResetProcessing();

			virtual void SerializeState(synthModuleSerializer *serializer);
						
			virtual void Synthesize();		
			
			void SetMinFrequency(const f32 freq)
			{
				m_MinFrequency = freq;
			}
			
			void SetMaxFrequency(const f32 freq)
			{
				m_MaxFrequency = freq;
			}

			f32 GetMaxFrequency() const{return m_MaxFrequency;}
			f32 GetMinFrequency() const{return m_MinFrequency;}
			
			void SetMinQ(const f32 val)
			{
				m_MinQ = val;
			}
			
			void SetMaxQ(const f32 val)
			{
				m_MaxQ = val;
			}
			f32 GetMaxQ() const {return m_MaxQ;}
			f32 GetMinQ() const {return m_MinQ;}
		private:
						
			synthSampleFrame m_Buffer[NumOutputs];
			
			f32 m_D1, m_D2;
			f32 m_MinFrequency;
			f32 m_MaxFrequency;
			f32 m_MinQ;
			f32 m_MaxQ;			
		};
}
#endif
#endif // SYNTH_STATEVARIABLE_H


