// 
// audiosynth/envelopefollowerview.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "envelopefollower.h"
#include "envelopefollowerview.h"

#if __SYNTH_EDITOR

namespace rage {

	synthEnvelopeFollowerView::synthEnvelopeFollowerView(synthModule *module) : synthModuleView(module)
	{
		m_Mode.AddItem("Normal");
		m_Mode.AddItem("ForceDynamic");
		AddComponent(&m_Mode);

		m_Mode.SetCurrentIndex(GetEnvelopeFollower()->GetMode());
	}

	synthEnvelopeFollowerView::~synthEnvelopeFollowerView()
	{

	}

	void synthEnvelopeFollowerView::Update()
	{
		GetEnvelopeFollower()->SetMode((synthEnvelopeFollower::EnvelopeFollowerMode)m_Mode.GetCurrentIndex());
		Matrix34 mat = GetMatrix();
		Vector3 offset = -mat.b;
		mat.d += offset;

		m_Mode.SetMatrix(mat);
		m_Mode.SetAlpha(GetHoverFade());

		synthModuleView::Update();
		m_TitleLabel.SetShouldDraw(!m_Mode.IsShowingMenu());
	}

	bool synthEnvelopeFollowerView::StartDrag() const
	{
		if(m_Mode.IsUnderMouse() || m_Mode.IsShowingMenu())
		{
			return false;
		}
		return synthModuleView::StartDrag();
	}

} // namespace rage
#endif // __SYNTH_EDITOR
