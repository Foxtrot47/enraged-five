// 
// audiosynth/uitextbox.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDIOSYNTH_UITEXTBOX_H 
#define AUDIOSYNTH_UITEXTBOX_H 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uilabel.h"
#include "atl/array.h"

namespace rage {

	class synthUITextBox : public synthUIComponent
	{
	public:

		synthUITextBox();

		void SetString(const char *str);
		const char *GetString() const { return m_TextBuffer; }
		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;
		
		bool IsEditing() const { return m_MouseMode == kEditMode; }
		void Edit() { m_WantsEdit = true; }

	protected:
		virtual void Render();

	private:
		void ComputeLabelStrings();
		s32 GetWidthInChars() const { return m_WidthInChars; }
		bool IsCharacterValid(const char c) const;

		enum MouseMode
		{
			kEditMode = 0,
			kDraggingMode,
			kResizingMode,
		};

		MouseMode m_MouseMode;

		u32 m_StringHash;

		f32 m_LineHeight;
		s32 m_WidthInChars;

		s32 m_EditIndex;

		s32 m_CaretLine;
		s32 m_CaretChar;

		char m_TextBuffer[1024];
		char m_LineBuffer[1024];
		atArray<synthUILabel*> m_Labels;

		bool m_IsEmpty;
		bool m_WantsEdit;
		bool m_FirstFrame;

	};
} // namespace rage

#endif // __SYNTH_EDITOR
#endif // AUDIOSYNTH_UITEXTBOX_H 
