#include "phaser.h"
#include "phaserview.h"
#include "synthdefs.h"

#define Undenormalize(sample) ((((*(u32*)&sample)&0x7f800000)==0)?0.0f:sample)

namespace rage
{
	using namespace Vec;

	SYNTH_EDITOR_VIEW(synthPhaser);

	synthPhaser::synthPhaser()
	{
		m_Outputs[0].Init(this,"Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[kPhaserCenter].Init(this, "Center", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kPhaserCenter].SetStaticValue(60.f);
		m_Inputs[kPhaserSpread].Init(this, "Spread",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kPhaserSpread].SetStaticValue(8.f);
		m_Inputs[kPhaserDepth].Init(this, "Depth",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kPhaserDepth].SetStaticValue(11.f);
		m_Inputs[kPhaserRate].Init(this, "Rate",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kPhaserRate].SetStaticValue(0.1f);
		m_Inputs[kPhaserMix].Init(this, "Mix",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kPhaserMix].SetStaticValue(0.5f);
		m_Inputs[kPhaserSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kPhaserSignal].SetConvertsInternally(false);

		for(int i=0; i<SYNTH_NUM_PHASER_FILTERS; ++i)
		{
			m_x1[i] = 0.f;
			m_x2[i] = 0.f;
			m_y1[i] = 0.f;
			m_y2[i] = 0.f;
		}

		m_prevLFOPhase = 0.f;
		m_tapNum = 0;
	}

	void synthPhaser::SerializeState(synthModuleSerializer* serializer)
	{
		serializer->SerializeField("InputType", m_inputType);
		serializer->SerializeField("NumFilters", m_tapNum);
	}

	void synthPhaser::Synthesize()
	{
		const f32 twoPiOverSampFreq = 0.00013089969389957473f;
		const f32 piOverSampFreq = 0.00006544984694978735f;
		const f32 oneOverTwelve = 1.f/12.f;

		m_Outputs[0].SetDataFormat(m_Inputs[0].GetDataFormat());
		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			const f32 input = m_Inputs[0].GetSignalValue(0);
			f32 output = (input && 0x7f800000 == 0) ? 0.0f : input;
			m_Outputs[0].SetStaticValue(output);
		}
		else
		{
			const synthPin::synthPinDataFormat dataFormat = m_Inputs[0].GetDataFormat();
			m_Outputs[0].SetDataFormat(dataFormat);
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			f32 LFOPhase = m_prevLFOPhase;

			f32* samples = m_Inputs[kPhaserSignal].GetDataBuffer()->GetBuffer();

			for(u32 j=0; j<m_Outputs[0].GetDataBuffer()->GetSize(); ++j)
			{	
				f32 center = m_Inputs[kPhaserCenter].GetNormalizedValue(j);		
				const f32 spread = m_Inputs[kPhaserSpread].GetNormalizedValue(j);
				const f32 depth = m_Inputs[kPhaserDepth].GetNormalizedValue(j);
				const f32 rate = m_Inputs[kPhaserRate].GetNormalizedValue(j);
				const f32 mix = FPClamp(m_Inputs[kPhaserMix].GetNormalizedValue(j), 0.0f, 1.0f);

				f32 input = *samples;
				
				LFOPhase += twoPiOverSampFreq*rate;
				while(LFOPhase > 2*PI)
					LFOPhase -= 2*PI;

				const f32 LFOValue = Sinf(LFOPhase);
				center += LFOValue*depth;

				atRangeArray<f32, SYNTH_NUM_PHASER_FILTERS> filterFreqs;
				if(m_inputType == 0) //note input
				{
					for(int i=0; i<SYNTH_NUM_PHASER_FILTERS; ++i)
					{
						f32 note = (i%2==0 ? center + (i*spread*-0.5f) : center + ((i+1)*spread*0.5f));
						filterFreqs[i] = FPClamp(powf(2.f,(note+36.3763f) * oneOverTwelve), 10.0f, 20000.0f);
					}
				}
				else //freq input
				{
					for(int i=0; i<SYNTH_NUM_PHASER_FILTERS; ++i)
					{
						if(i%2==0)
							filterFreqs[i] = FPClamp(center + (i*spread*-0.5f), 10.0f, 20000.0f); 
						else
							filterFreqs[i] = FPClamp(center + ((i+1)*spread*0.5f), 10.0f, 20000.0f); 
					}
				}

				atRangeArray<f32, SYNTH_NUM_PHASER_FILTERS> filterOutput;

				for(int i=0; i<SYNTH_NUM_PHASER_FILTERS; ++i)
				{
					const f32 tanArg = piOverSampFreq * filterFreqs[i];
					const f32 c = (tan(tanArg)-1)/(tan(tanArg)+1);
					
					f32 thisInput = i==0 ? input : filterOutput[i-1];
					const f32 output = Clamp(c*thisInput + m_x1[i] - c*m_y1[i], -1.0f, 1.0f);

					m_y2[i] = Undenormalize(m_y1[i]);
					m_y1[i] = Undenormalize(output);
					m_x2[i] = Undenormalize(m_x1[i]);
					m_x1[i] = Undenormalize(thisInput);

					filterOutput[i] = (output);
				}

				samples++;

				//m_Outputs[0].GetDataBuffer()->GetBuffer()[j] = (filterOutput[SYNTH_NUM_PHASER_FILTERS-1]*mix) + (input*(1-mix));
				m_Outputs[0].GetDataBuffer()->GetBuffer()[j] = (filterOutput[m_tapNum]*mix) + (input*(1-mix));
			}

			m_prevLFOPhase = LFOPhase;
		}
	}
}

