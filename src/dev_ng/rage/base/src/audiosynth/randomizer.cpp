// 
// audiosynth/randomizer.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "synthcontext.h"
#include "randomizer.h"
#include "audiosynth/synthutil.h"

#include "math/amath.h"
#include "randomizerview.h"


namespace rage 
{
	using namespace Vec;
	SYNTH_EDITOR_VIEW(synthRandomizer);

	synthRandomizer::synthRandomizer() 
	{
		m_IsQuantised = false;
		m_Mode = NORMAL;

		m_Inputs[kRandomizerTrigger].Init(this, "Trigger", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kRandomizerTrigger].SetStaticValue(0.f);
		m_Inputs[kRandomizerMinOutput].Init(this, "MinOutput", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kRandomizerMinOutput].SetStaticValue(0.f);
		m_Inputs[kRandomizerMaxOutput].Init(this, "MaxOutput", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kRandomizerMaxOutput].SetStaticValue(1.f);

		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);

		

		ResetProcessing();
	}

	void synthRandomizer::ResetProcessing()
	{
		m_IsFirstFrame = true;
	}

	void synthRandomizer::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("IsQuantised", m_IsQuantised);
		serializer->SerializeEnumField("Mode", m_Mode);
	}

	void synthRandomizer::Synthesize()
	{
		f32 triggerVal = m_Inputs[kRandomizerTrigger].GetNormalizedValue(0);		
		const bool isTriggerHigh = (triggerVal >= 1.f);

		if((m_Mode != ONE_SHOT && isTriggerHigh) || m_IsFirstFrame)
		{
			m_IsFirstFrame = false;
			GenerateRandomNumber();
		}
	}

	void synthRandomizer::GenerateRandomNumber()
	{
		const float min = m_Inputs[kRandomizerMinOutput].GetNormalizedValue(0);
		const float max = m_Inputs[kRandomizerMaxOutput].GetNormalizedValue(0);

		const f32 r = Lerp(0.5f * (synthUtil::Rand()+1.f), min, max);
		if(m_IsQuantised)
		{
			m_Outputs[0].SetStaticValue(static_cast<f32>(round(r)));
		}
		else
		{
			m_Outputs[0].SetStaticValue(r);
		}
	}

	float synthRandomizer::Process(const float trigger, const float min, const float max, Vector_4V_InOut state)
	{
		const bool isFirstFrame = GetYi(state) == 0;
		const bool isTriggerHigh = (trigger >= 1.f);

		SetY(state, 1.f);

		float ret;
		if(isFirstFrame || isTriggerHigh)
		{
			ret = Lerp(0.5f * (synthUtil::Rand()+1.f), min, max);
			SetX(state, ret);
		}
		else
		{
			ret = GetX(state);
		}

		return ret;
	}

	float synthRandomizer::Process_OnInit(const float min, const float max, Vector_4V_InOut state)
	{
		const bool isFirstFrame = GetYi(state) == 0;
		SetY(state, 1.f);
		
		float ret;
		if(isFirstFrame)
		{
			ret = Lerp(0.5f * (synthUtil::Rand()+1.f), min, max);
			SetX(state, ret);
		}
		else
		{
			ret = GetX(state);
		}

		return ret;
	}

} // namespace rage
#endif
