#include "audiooutput.h"
#include "synthdefs.h"
#include "synthutil.h"
#include "audiohardware/mixing_vmath.inl"
#include "system/memops.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	synthAudioOutput::synthAudioOutput() : m_Buffer(false)
	{
		m_Inputs[kAudioOutputSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kAudioOutputFinishedTrigger].Init(this, "StopTrigger", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_HasFinished = false;
		m_HasClipped = false;
		m_PeakLevel = 0.f;
		m_RMSLevel = 0.f;		
	}

	synthAudioOutput::~synthAudioOutput()
	{

	}
	void synthAudioOutput::Synthesize()
	{	
		// look for a stop trigger
		m_HasFinished = m_Inputs[kAudioOutputFinishedTrigger].GetNormalizedValue(0) >= 1.f;
		
		if(m_Buffer.HasBuffer())
		{
			if(!m_HasFinished && m_Inputs[kAudioOutputSignal].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
#if __SYNTH_EDITOR
				m_PeakLevel = Max(m_PeakLevel, ComputePeakLevel<kMixBufNumSamples>(m_Inputs[kAudioOutputSignal].GetDataBuffer()->GetBuffer()));
				m_RMSLevel = Max(m_RMSLevel, ComputeRMSLevel<kMixBufNumSamples>(m_Inputs[kAudioOutputSignal].GetDataBuffer()->GetBuffer()));

				if(!m_HasClipped)
				{
					if(m_PeakLevel > 1.f)
					{
						m_HasClipped = true;
					}
				}
#endif				
				synthUtil::CopyBuffer(m_Buffer.GetBufferV(), m_Inputs[kAudioOutputSignal].GetDataBuffer()->GetBufferV(), kMixBufNumSamples);
				HardClipBuffer<kMixBufNumSamples>(m_Buffer.GetBuffer());
			}
			else
			{
				sysMemSet(m_Buffer.GetBuffer(),0,m_Buffer.GetSize()*sizeof(f32));
			}
		}

		
	}
}

