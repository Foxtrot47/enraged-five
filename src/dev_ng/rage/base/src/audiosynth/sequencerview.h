// 
// audiosynth/sequencerview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SEQUENCERVIEW_H
#define SYNTH_SEQUENCERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uihorizontallayout.h"
#include "uilabel.h"
#include "uimenuview.h"
#include "uirangeview.h"
#include "uiscrollbar.h"

namespace rage
{
	class grcTexture;
	
	class synthSequencer;
	class synthSequencerView : public synthModuleView
	{
	public:

		synthSequencerView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;


	protected:
		virtual void Render();
		virtual u32 GetModuleHeight() const;

	private:
		
		synthSequencer *GetSequencer() const {return (synthSequencer*)m_Module; }
		
		synthUIScrollbar m_Divisions;
		synthUIRangeView m_TempoRange;
        synthUIHorizontalLayout m_Layout;

		
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_OSCILLATORVIEW_H

