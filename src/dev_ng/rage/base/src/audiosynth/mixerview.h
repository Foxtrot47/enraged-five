// 
// audiosynth/mixerview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_MIXERVIEW_H
#define SYNTH_MIXERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "mixer.h"
#include "moduleview.h"
#include "sampleframe.h"

namespace rage
{
	class synthMixer;
	class synthMixerView : public synthModuleView
	{
	public:

		synthMixerView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();

		synthMixer *GetMixer()
		{
			return (synthMixer*)m_Module;
		}
	private:

		enum {kMaxSignalInputs = synthMixer::NumInputs/2};

		atRangeArray<synthSampleFrame,kMaxSignalInputs> m_PostGainBuffers;
		atRangeArray<synthPin,kMaxSignalInputs> m_PostGainPins;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_MIXERVIEW_H

