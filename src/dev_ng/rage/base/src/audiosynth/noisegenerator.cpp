
#include "noisegenerator.h"
#include "synthcontext.h"
#include "synthutil.h"

#include "system/cache.h"
#include "vectormath/vectormath.h"
#include "vectormath/classes.h"

namespace rage
{	
	synthNoiseGenerator::synthNoiseGenerator() 
	{
		m_Inputs[0].Init(this, "Amplitude",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetStaticValue(1.f);
		
		m_Outputs[0].Init(this, "Noise",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);
	}
	
	void synthNoiseGenerator::Synthesize(synthContext &UNUSED_PARAM(context))
	{
		synthSampleFrame *output = m_Outputs[0].GetDataBuffer();

		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			Generate(output->GetBuffer(), output->GetSize());
			
			if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
			{
				synthUtil::ScaleBufferWithSignal(output->GetBuffer(), m_Inputs[0].GetDataBuffer()->GetBuffer(), output->GetSize());
			}
			else
			{
				synthUtil::ScaleBuffer(output->GetBuffer(), m_Inputs[0].GetDataBuffer()->GetBuffer(), output->GetSize());
			}			
		}
		else
		{
			const Vec::Vector_4V gain = m_Inputs[0].GetNormalizedValueV(0);
			Generate(output->GetBuffer(), output->GetSize());
			synthUtil::ScaleBuffer(output->GetBuffer(), gain, output->GetSize());
		}
	}

	void synthNoiseGenerator::Generate(float *RESTRICT destBuffer, const u32 numSamples)
	{
		Vec::Vector_4V *output = reinterpret_cast<Vec::Vector_4V*>(destBuffer);
		
		u32 count = numSamples>>5; // 32 samples (128 bytes) at a time

		while(count--)
		{
			// invalidate destination cache line
			ZeroDC(output, 0);

			*output++ = synthUtil::RandV();
			*output++ = synthUtil::RandV();
			*output++ = synthUtil::RandV();
			*output++ = synthUtil::RandV();
			*output++ = synthUtil::RandV();
			*output++ = synthUtil::RandV();
			*output++ = synthUtil::RandV();
			*output++ = synthUtil::RandV();
		}
	}
	
}
