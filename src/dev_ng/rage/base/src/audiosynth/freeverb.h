
#ifndef SYNTH_FREEVERB_H
#define SYNTH_FREEVERB_H

#include "blocks.h"
#include "synthdefs.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "audioeffecttypes/reverbeffect.h"

namespace rage
{
	enum synthFreeVerbInputs
	{
		kFreeverbSignal = 0,
		kFreeverbDamping,
		kFreeverbRoomSize,
		kFreeverbWetMix,
		kFreeverbDryMix,
		
		kFreeverbNumInputs
	};

	class synthFreeverb : public synthModuleBase<kFreeverbNumInputs, 1, SYNTH_FREEVERB>
	{
	public:

		synthFreeverb();
		virtual ~synthFreeverb();


		virtual void Synthesize();

		virtual const char *GetName() const
		{
			return "Freeverb";
		}


	private:
		
		audReverbEffect m_Reverb ;
		synthSampleFrame m_Buffer;
		
	};
}

#endif // SYNTH_FREEVERB_H


