// 
// audiosynth/junctionview.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "junctionview.h"

#if __SYNTH_EDITOR
#include "junction.h"
#include "pinview.h"
#include "uiplane.h"
#include "synthesizerview.h"
#include "input/mouse.h"

namespace rage
{
	synthJunctionPinView::synthJunctionPinView(synthModule *module) : synthModuleView(module)
	{
		m_LastPosition.Zero();
	}

	synthJunctionPinView::~synthJunctionPinView()
	{
	
	}

	void synthJunctionPinView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		GetJunction()->GetInput(0).GetView()->GetBounds(v1,v2);
	}

	void synthJunctionPinView::Update()
	{
		// override pin positioning
		Matrix34 mat = GetMatrix();
		const f32 pinWidth = 1.f;
		mat.d += mat.a * pinWidth;
		mat.Scale(0.75f, 0.75f, 1.f);
		synthPinView *inputPinView = GetJunction()->GetInput(0).GetView();
		synthPinView *outputPinView = GetJunction()->GetOutput(0).GetView();
		inputPinView->SetMatrix(mat);
		inputPinView->DisableZooming(true);
		inputPinView->SetShouldDrawTitle(false);
		inputPinView->SetReadOnly(true);
		inputPinView->SetShouldDrawReflection(false);

		if(inputPinView->IsUnderMouse())
		{
			if(ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT && !synthSynthesizerView::Get()->GetModulePlane().IsDuplicateKeyDown())
			{
                if(!synthSynthesizerView::Get()->GetModulePlane().IsConnecting())
				{
					synthSynthesizerView::Get()->GetModulePlane().StartConnection(outputPinView->GetPin());
				}
			}
		}

		Vector3 currentPosition = GetMatrix().d;
		if(synthSynthesizerView::Get()->GetModulePlane().GetDraggingView() == this && 
			synthSynthesizerView::Get()->GetModulePlane().GetConnectingStartPin() == &GetJunction()->GetOutput(0))
		{
			const f32 distMoved2 = (currentPosition-m_LastPosition).Mag2();
			if(distMoved2 > 0.05f)
			{
				synthSynthesizerView::Get()->GetModulePlane().CancelConnection();
			}
		}
		m_LastPosition = currentPosition;

		

		GetJunction()->GetOutput(0).GetView()->SetMatrix(mat);
		GetJunction()->GetOutput(0).GetView()->SetShouldDraw(false);

		synthModuleView::Update();

		m_TitleLabel.SetShouldDraw(false);
		GetJunction()->GetOutput(0).GetView()->SetShouldDraw(false);
	}

	void synthJunctionPinView::Render()
	{
		GetJunction()->GetInput(0).GetView()->Draw();
	}

	bool synthJunctionPinView::StartDrag() const
	{
		return GetJunction()->GetInput(0).GetView()->IsUnderMouse();
	}
}
#endif // __SYNTH_EDITOR
