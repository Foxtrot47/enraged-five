
#include "sampleandhold.h"
#include "sampleandholdview.h"
#include "synthcontext.h"
#include "vectormath/vectormath.h"
#include "system/cache.h"

namespace rage
{
	using namespace Vec;

	SYNTH_EDITOR_LINK_VIEW(synthSampleAndHold, synthSampleAndHoldView);

	synthSampleAndHold::synthSampleAndHold() 
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Inputs[kSampleAndHoldSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kSampleAndHoldSignal].SetStaticValue(0.f);
		m_Inputs[kSampleAndHoldTrigger].Init(this, "Trigger", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kSampleAndHoldTrigger].SetStaticValue(1.f);
		
		m_DynamicOutput = false;
		m_Outputs[0].SetStaticValue(0.f);

		ResetProcessing();
	}

	void synthSampleAndHold::ResetProcessing()
	{
		m_LastVal = 0.f;
		m_State = V4VConstant(V_ZERO);
	}

	void synthSampleAndHold::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("DynamicOutput", m_DynamicOutput);
	}

	void synthSampleAndHold::Synthesize()
	{
		// if trigger is high then re-sample input
		if(m_Inputs[kSampleAndHoldTrigger].GetNormalizedValue(0) >= 1.f)
		{
			if(m_DynamicOutput)
			{
				if(m_Inputs[kSampleAndHoldSignal].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
				{
					const Vector_4V *RESTRICT input = (Vector_4V*)m_Inputs[kSampleAndHoldSignal].GetDataBuffer()->GetBuffer();
					Vector_4V *RESTRICT output = (Vector_4V*)m_Buffer.GetBuffer();
					for(u32 i = 0; i < kMixBufNumSamples; i += 4)
					{
						*output++ = *input++;
					}
				}
				else
				{
					Vector_4V *RESTRICT output = (Vector_4V*)m_Buffer.GetBuffer();
					const Vector_4V input = V4LoadScalar32IntoSplatted(m_Inputs[kSampleAndHoldSignal].GetStaticValue());
					for(u32 i = 0; i < kMixBufNumSamples; i += 4)
					{
						*output++ = input;
					}
				}

				m_Outputs[0].SetDataBuffer(&m_Buffer);
			}
			else
			{
				if(m_Inputs[kSampleAndHoldSignal].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
				{
					m_LastVal = m_Inputs[kSampleAndHoldSignal].GetNormalizedValue(0);
				}
				else
				{
					m_LastVal = m_Inputs[kSampleAndHoldSignal].GetSignalValue(0);
				}
				m_Outputs[0].SetStaticValue(m_LastVal);
			}

			m_Outputs[0].SetDataFormat(m_Inputs[kSampleAndHoldSignal].GetDataFormat());
		}		
	}

	float synthSampleAndHold::ProcessStatic(const float input, const float trigger, Vec::Vector_4V_InOut state)
	{
		const bool isFirstFrame = GetYi(state) == 0;
		const bool isTriggerHigh = (trigger >= 1.f);

		float ret;

		SetY(state, 1.f);

		if(isFirstFrame || isTriggerHigh)
		{
			ret = input;
			SetX(state, ret);
		}
		else
		{
			ret = GetX(state);
		}

		return ret;
	}
}




