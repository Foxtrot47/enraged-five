// 
// audiosynth/awnoise.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_AWNOISE_H
#define SYNTH_AWNOISE_H

#include "synthdefs.h"

#include "module.h"
#include "modulefactory.h"
#include "sampleframe.h"

#include "vectormath/vectormath.h"

namespace rage
{
	enum synthAWNoiseGeneratorInputs
	{
		kAWNoisePulseWidth = 0,
		kAWNoisePulseWidthVariance,
		kAWNoiseAmplitude,
		
		kAWNoiseNumInputs
	};

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif

	class synthAWNoiseGenerator : public synthModuleBase<kAWNoiseNumInputs, 1, SYNTH_AWNOISEGENERATOR>
	{
	public:
		SYNTH_MODULE_NAME("AW_Noise");

		synthAWNoiseGenerator();
		virtual void Synthesize();
		virtual void ResetProcessing();

		
	private:
			
		Vec::Vector_4V m_State;
		synthSampleFrame m_Buffer;
		synthSampleFrame m_In1Buffer, m_In2Buffer;

	};

	enum synthAWFilterInputs
	{
		kAWFilterInput = 0,
		kAWFilterPulseWidth,
		kAWFilterPulseWidthVariance,
		kAWFilterAmplitude,

		kAWFilterNumInputs
	};

	class synthAWFilter : public synthModuleBase<kAWFilterNumInputs, 1, SYNTH_AWFILTER>
	{
	public:
		SYNTH_MODULE_NAME("AW_Filter");

		synthAWFilter();
		virtual void Synthesize();
		virtual void ResetProcessing();

		static void Process(float *inOutPtr, const float *pulseWidth, const float *pulseWidthVariance, Vec::Vector_4V_InOut state, const u32 numSamples);

	private:
		Vec::Vector_4V m_State;
		synthSampleFrame m_Buffer;
		synthSampleFrame m_In1Buffer, m_In2Buffer;
	};

#if __WIN32
#pragma warning(pop)
#endif

}

#endif // SYNTH_AWNOISE_H
