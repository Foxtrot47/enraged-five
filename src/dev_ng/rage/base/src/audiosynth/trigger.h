
#ifndef SYNTH_TRIGGER_H
#define SYNTH_TRIGGER_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{
	enum 
	{
		kTriggerInput = 0,
		kTriggerThreshold,
		
		kTriggerNumInputs
	};


	class synthTrigger : public synthModuleBase<kTriggerNumInputs, 1, SYNTH_TRIGGER>
	{
	public:

		SYNTH_MODULE_NAME("Trigger");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthTrigger();
		
		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();
		virtual void ResetProcessing();

		enum synthTriggerMode
		{
			Difference = 0,
			Latching,
		};

		synthTriggerMode GetMode() const { return m_Mode; }
		void SetMode(const synthTriggerMode mode) { m_Mode = mode; }

#if __SYNTH_EDITOR
		void Trigger() { m_TriggerRequested = true; }
#endif

		static float Process_Latch(const float *RESTRICT const inBuffer, const float *RESTRICT const inThresholdBuffer, Vec::Vector_4V_InOut state, const u32 numSamples);
		static float Process_Latch(const float *RESTRICT const inBuffer, Vec::Vector_4V threshold, Vec::Vector_4V_InOut state, const u32 numSamples);
		static float Process_Latch(const float input, const float threshold, Vec::Vector_4V_InOut state);
		
		static float Process_Diff(const float input, const float threshold, Vec::Vector_4V_InOut state);
	private:

		static float Process_Latch(const bool isTriggerHigh, Vec::Vector_4V_InOut state);

		f32 m_PrevValue;
		synthTriggerMode m_Mode;
		bool m_WasTriggerHigh;

		SYNTH_EDITOR_ONLY(bool m_TriggerRequested);
	};
}
#endif
#endif // SYNTH_TRIGGER_H


