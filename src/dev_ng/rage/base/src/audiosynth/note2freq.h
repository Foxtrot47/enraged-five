// 
// audiosynth/note2freq.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_NOTE2FREQ_H
#define SYNTH_NOTE2FREQ_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "synthdefs.h"

namespace rage
{
	class synthNoteToFrequency : public synthModuleBase<1,1,SYNTH_NOTE2FREQ>
	{
	public:
		SYNTH_MODULE_NAME("Note2Freq");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthNoteToFrequency();
		virtual void Synthesize();


		static float Process(const float noteInput);
		static void Process(float *RESTRICT inOut, const u32 numSamples);

	private:
		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_NOTE2FREQ_H


