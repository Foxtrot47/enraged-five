// 
// audiosynth/randomizer.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_RANDOMIZER_H
#define SYNTH_RANDOMIZER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"
#include "synthdefs.h"

#include "vectormath/vectormath.h"

namespace rage
{
	class synthContext;
	enum synthRandomizerInputs
	{
		kRandomizerTrigger = 0,
		kRandomizerMinOutput,
		kRandomizerMaxOutput,
		
		kRandomizerNumInputs
	};
	class synthRandomizer : public synthModuleBase<kRandomizerNumInputs, 1, SYNTH_RANDOMIZER>
	{
	public:	
		SYNTH_MODULE_NAME("Randomizer");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthRandomizer();

		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();
		virtual void ResetProcessing();
	
		bool IsQuantised() const { return m_IsQuantised != 0; }
		void SetIsQuantised(const bool is) { m_IsQuantised = is ? 1 : 0; }

		enum Mode
		{
			NORMAL = 0,
			ONE_SHOT,
		};
		void SetMode(const Mode mode) { m_Mode = mode; }
		Mode GetMode() const { return m_Mode; }

		static float Process(const float trigger, const float min, const float max, Vec::Vector_4V_InOut state);
		static float Process_OnInit(const float min, const float max, Vec::Vector_4V_InOut state);
	private:
		void GenerateRandomNumber();

		Mode m_Mode;
		u32 m_IsQuantised;
		bool m_IsFirstFrame;
	};
}
#endif
#endif // SYNTH_RANDOMIZER_H