
#include "hardknee.h"
#include "hardkneeview.h"
#if !SYNTH_MINIMAL_MODULES
#include "synthutil.h"
#include "audioengine/curve.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthHardKnee);

	using namespace Vec;
	synthHardKnee::synthHardKnee()
	{
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);

		m_Inputs[kHardKneeInputSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kHardKneeInputKnee].Init(this, "Knee", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kHardKneeInputKnee].SetStaticValue(0.f);
	}

	void synthHardKnee::Synthesize()
	{
		m_Outputs[0].SetDataFormat(synthPin::SYNTH_PIN_DATA_NORMALIZED);
		if(m_Inputs[kHardKneeInputSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC && m_Inputs[kHardKneeInputKnee].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// static mode			
			const f32 input = m_Inputs[kHardKneeInputSignal].GetNormalizedValue(0);
			const f32 knee = m_Inputs[kHardKneeInputKnee].GetNormalizedValue(0);
			m_Outputs[0].SetStaticValue(Process(input, knee));
		}
		else
		{
			// dynamic mode
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			if(m_Inputs[kHardKneeInputKnee].GetDataState() == synthPin::SYNTH_PIN_STATIC)
			{
				// Copy input to output so we can use an in-place process
				sysMemCpy(m_Buffer.GetBuffer(), m_Inputs[kHardKneeInputSignal].GetDataBuffer()->GetBuffer(), sizeof(float) * m_Buffer.GetSize());
				if(m_Inputs[kHardKneeInputSignal].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
				{
					synthUtil::ConvertBufferToNormalised(m_Buffer.GetBuffer(), kMixBufNumSamples);
				}
				const Vector_4V knee = m_Inputs[kHardKneeInputKnee].GetNormalizedValueV(0);
				Process(m_Buffer.GetBuffer(), knee, m_Buffer.GetSize());
			}
			else
			{
				for(u32 i = 0; i < m_Buffer.GetSize(); i++)
				{
					const f32 input = m_Inputs[kHardKneeInputSignal].GetNormalizedValue(i);
					const f32 knee = m_Inputs[kHardKneeInputKnee].GetNormalizedValue(i);
					m_Outputs[0].GetDataBuffer()->GetBuffer()[i] = Process(input, knee);
				}
			}
		}	
	}

	void synthHardKnee::Process(float *RESTRICT const inOutBuffer, const float *const kneeBuffer, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		const Vector_4V *kneePtr = reinterpret_cast<const Vector_4V*>(kneeBuffer);
		u32 count = numSamples >> 2;
		const Vector_4V half = V4VConstant(V_HALF);
		while(count--)
		{
			const Vector_4V input = *inOutPtr;
			const Vector_4V knee = *kneePtr++;
			const Vector_4V region0Ratio = V4InvScale(input, knee);
			const Vector_4V region0Output = V4Scale(half, region0Ratio);
			const Vector_4V region1Ratio = V4InvScale(V4Subtract(input, knee),V4Subtract(V4VConstant(V_ONE), knee));
			const Vector_4V region1Output = V4Add(half, V4Scale(half, region1Ratio));

			const Vector_4V inputGreaterThanKnee = V4IsGreaterThanOrEqualV(input, knee);
			*inOutPtr++ = V4Clamp(V4SelectFT(inputGreaterThanKnee, region0Output, region1Output), V4VConstant(V_ZERO), V4VConstant(V_ONE));
		}
	}

	void synthHardKnee::Process(float *RESTRICT const inOutBuffer, const Vector_4V_In knee, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		
		u32 count = numSamples >> 2;
		const Vector_4V half = V4VConstant(V_HALF);
		while(count--)
		{
			const Vector_4V input = *inOutPtr;
			
			const Vector_4V region0Ratio = V4InvScale(input, knee);
			const Vector_4V region0Output = V4Scale(half, region0Ratio);
			const Vector_4V region1Ratio = V4InvScale(V4Subtract(input, knee),V4Subtract(V4VConstant(V_ONE), knee));
			const Vector_4V region1Output = V4Add(half, V4Scale(half, region1Ratio));

			const Vector_4V inputGreaterThanKnee = V4IsGreaterThanOrEqualV(input, knee);
			*inOutPtr++ = V4Clamp(V4SelectFT(inputGreaterThanKnee, region0Output, region1Output), V4VConstant(V_ZERO), V4VConstant(V_ONE));
		}
	}

	float synthHardKnee::Process(const float input, const float knee)
	{
		// Calculate our output for each of the three sections, and then fsel all the options.		
		const f32 region0Ratio = input / knee;
		const f32 region0Output = 0.5f * region0Ratio;
		const f32 region1Ratio = (input - knee) / (1.f - knee);
		const f32 region1Output = 0.5f + 0.5f * region1Ratio;
		f32 output = Selectf(input, region0Output, 0.f);
		output     = Selectf(input-knee, region1Output, output);
		output     = Selectf(input-1.f, 1.f, output);

		return output;
	}

}
#endif
