// 
// audiosynth/oscillatorview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_OSCILLATORVIEW_H
#define SYNTH_OSCILLATORVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "dsfoscillator.h"
#include "moduleview.h"
#include "oscillator.h"
#include "uidropdownlist.h"
#include "uilabel.h"
#include "uimenuview.h"
#include "uirangeview.h"

namespace rage
{
	class grcTexture;
	class synthDigitalOscillator;
	class synthAnalogueOscillator;
	class synthOscillatorView : public synthModuleView
	{
	public:

		synthOscillatorView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();
		
	private:

		synthDigitalOscillator *GetDigitalOsc() { Assert(GetModule()->GetTypeId() == synthDigitalOscillator::TypeId); return (synthDigitalOscillator*)GetModule(); }
		synthAnalogueOscillator *GetAnalogueOsc() { Assert(GetModule()->GetTypeId() == synthAnalogueOscillator::TypeId); return (synthAnalogueOscillator*)GetModule(); }

		bool IsDigitalOsc() const { return GetModule()->GetTypeId() == synthDigitalOscillator::TypeId; }

		s32 m_LastRenderedMode;

		synthUIRangeView m_FreqRange;
		synthUIDropDownList m_ModeList;
		synthUIDropDownList m_StaticDynamicSwitch;

		grcTexture *m_Texture;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_OSCILLATORVIEW_H

