// 
// audiosynth/feedbackpinview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_FEEDBACKPINVIEW_H
#define SYNTH_FEEDBACKPINVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthFeedbackPin;
	class synthFeedbackPinView : public synthModuleView
	{
	public:

		synthFeedbackPinView(synthModule *module);

		virtual void Update();

	protected:
		
		virtual bool StartDrag() const;
	
	private:

		synthFeedbackPin *GetFeedbackPin() const { return (synthFeedbackPin*)m_Module; }
		synthUIDropDownList m_ProcessingDirection;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_FEEDBACKPINVIEW_H

