#include "modulefactory.h"

#if __SYNTH_EDITOR

#include "1polelpf.h"
#include "abs.h"
#include "adder.h"
#include "allpass.h"
#include "anoscillator.h"
#include "audioinput.h"
#include "audiooutput.h"
#include "awnoise.h"
#include "biquadfilter.h"
#include "ceil.h"
#include "chartrecorder.h"
#include "clipper.h"
#include "compressor.h"
#include "constant.h"
#include "convertor.h"
#include "counter.h"
#include "dattorro.h"
#include "decimator.h"
#include "delayline.h"
#include "divider.h"
#include "dsfoscillator.h"
#include "envelopefollower.h"
#include "envelopegenerator.h"
#include "exp.h"
#include "feedbackpin.h"
#include "fftmodule.h"
#include "framesmoother.h"
#include "floor.h"
#include "freeverb.h"
#include "fv_allpassfilter.h"
#include "fv_combfilter.h"
#include "hardknee.h"
#include "inputsmoother.h"
#include "inverter.h"
#include "gate.h"
#include "junction.h"
#include "max.h"
#include "midiccin.h"
#include "midiin.h"
#include "min.h"
#include "mixer.h"
#include "mod.h"
#include "modalsynth.h"
#include "multiplier.h"
#include "multitapdelay.h"
#include "mverb.h"
#include "notchfilter.h"
#include "note2freq.h"
#include "noisegenerator.h"
#include "oscillator.h"
#include "pinkingfilter.h"
#include "phaser.h"
#include "progenitor.h"
#include "polycurve.h"
#include "power.h"
#include "pwmnoise.h"
#include "randomizer.h"
#include "readtime.h"
#include "rectifier.h"
#include "rescaler.h"
#include "revswitch.h"
#include "round.h"
#include "sampleandhold.h"
#include "sampleplayer.h"
#include "sequencer.h"
#include "sign.h"
#include "smalldelay.h"
#include "statevariable.h"
#include "subtracter.h"
#include "switch.h"
#include "timedtrigger.h"
#include "tremolo.h"
#include "trigger.h"
#include "system/new.h"

#define SYNTH_EVAL(f,c) case c::TypeId: return rage::f<c>()
#define SYNTH_IMPLEMENT_TYPE_INVOKE(EVAL) \
	template<class RetType> RetType InvokeTypeFunction_##EVAL(const synthModuleId type, const RetType errorVal) \
	{ \
	switch(type) \
		 { \
		 SYNTH_EVAL(EVAL, synthAudioOutput); \
		 SYNTH_EVAL(EVAL, synthAdder);	 \
		 SYNTH_EVAL(EVAL, synth1PoleLPF); \
		 SYNTH_EVAL(EVAL, synthBiquadFilter);	\
		 SYNTH_EVAL(EVAL, synthClipper); \
		 SYNTH_EVAL(EVAL, synthSignalConverter); \
		 SYNTH_EVAL(EVAL, synthDelayLine);	\
		 SYNTH_EVAL(EVAL, synthDivider); \
		 SYNTH_EVAL(EVAL, synthAnalogueOscillator); \
		 SYNTH_EVAL(EVAL, synthEnvelopeGenerator); \
		 SYNTH_EVAL(EVAL, synthLinearToExponential); \
		 SYNTH_EVAL(EVAL, synthFft); \
		 SYNTH_EVAL(EVAL, synthHardKnee); \
		 SYNTH_EVAL(EVAL, synthVariableInput); \
		 SYNTH_EVAL(EVAL, synthInverter); \
		 SYNTH_EVAL(EVAL, synthGate); \
		 SYNTH_EVAL(EVAL, synthMixer); \
		 SYNTH_EVAL(EVAL, synthSamplePlayer); \
		 SYNTH_EVAL(EVAL, synthMultiplier); \
		 SYNTH_EVAL(EVAL, synthNoiseGenerator); \
		 SYNTH_EVAL(EVAL, synthPinkingFilter); \
		 SYNTH_EVAL(EVAL, synthPwmNoise); \
		 SYNTH_EVAL(EVAL, synthDigitalOscillator); \
		 SYNTH_EVAL(EVAL, synthEnvelopeFollower); \
		 SYNTH_EVAL(EVAL, synthRandomizer); \
		 SYNTH_EVAL(EVAL, synthRescaler); \
		 SYNTH_EVAL(EVAL, synthSampleAndHold); \
		 SYNTH_EVAL(EVAL, synthStateVariableFilter); \
		 SYNTH_EVAL(EVAL, synthRectifier); \
		 SYNTH_EVAL(EVAL, synthSubtracter); \
		 SYNTH_EVAL(EVAL, synthSequencer);		 \
		 SYNTH_EVAL(EVAL, synthNoteToFrequency); \
		 SYNTH_EVAL(EVAL, synthSwitch); \
		 SYNTH_EVAL(EVAL, synthPolyCurve);	 \
		 SYNTH_EVAL(EVAL, synthAWNoiseGenerator);	 \
		 SYNTH_EVAL(EVAL, synthDecimator);	 \
		 SYNTH_EVAL(EVAL, synthReverseSwitch); \
		 SYNTH_EVAL(EVAL, synthTrigger); \
		 SYNTH_EVAL(EVAL, synthAudioInput);		 \
		 SYNTH_EVAL(EVAL, synthJunctionPin);	 \
		 SYNTH_EVAL(EVAL, synthAbs); \
		 SYNTH_EVAL(EVAL, synthSign); \
		 SYNTH_EVAL(EVAL, synthFloor); \
		 SYNTH_EVAL(EVAL, synthCeil); \
		 SYNTH_EVAL(EVAL, synthRound); \
		 SYNTH_EVAL(EVAL, synthCounter); \
		 SYNTH_EVAL(EVAL, synthModulus); \
		 SYNTH_EVAL(EVAL, synthFeedbackPin); \
		 SYNTH_EVAL(EVAL, synthPower); \
		 SYNTH_EVAL(EVAL, synthFrameSmoother); \
		 SYNTH_EVAL(EVAL, synthMidiIn); \
		 SYNTH_EVAL(EVAL, synthMidiCCIn); \
		 SYNTH_EVAL(EVAL, synthTremolo); \
		 SYNTH_EVAL(EVAL, synthConstant); \
		 SYNTH_EVAL(EVAL, synthMax); \
		 SYNTH_EVAL(EVAL, synthMin); \
         SYNTH_EVAL(EVAL, synthChartRecorder); \
		 SYNTH_EVAL(EVAL, synthTimedTrigger); \
		 SYNTH_EVAL(EVAL, synthFvCombFilter); \
		 SYNTH_EVAL(EVAL, synthFvAllpassFilter); \
		 SYNTH_EVAL(EVAL, synthMultiTapDelay); \
		 SYNTH_EVAL(EVAL, synthProgenitorReverb); \
		 SYNTH_EVAL(EVAL, synthDattorroReverb); \
		 SYNTH_EVAL(EVAL, synthFreeverb); \
		 SYNTH_EVAL(EVAL, synthMVerb); \
		 SYNTH_EVAL(EVAL, synthSmallDelay); \
		 SYNTH_EVAL(EVAL, synthCompressor); \
		 SYNTH_EVAL(EVAL, synthAWFilter); \
		 SYNTH_EVAL(EVAL, synthReadTime); \
		 SYNTH_EVAL(EVAL, synthNotchFilter); \
		 SYNTH_EVAL(EVAL, synthAllpassFilter); \
		 SYNTH_EVAL(EVAL, synthPhaser); \
		} \
		return errorVal; \
	}

namespace rage
{
	template<class T> synthModule *synthInstantiateModule()
	{
		return rage_new T();
	}

	SYNTH_IMPLEMENT_TYPE_INVOKE(synthInstantiateModule);
	synthModule *synthModuleFactory::CreateModule(const synthModuleId type)
	{
		return InvokeTypeFunction_synthInstantiateModule(type, (synthModule*)NULL);
	}
	
	template<class T> u32 synthModuleSize()
	{
		return sizeof(T);
	}
	SYNTH_IMPLEMENT_TYPE_INVOKE(synthModuleSize);
	u32 synthModuleFactory::GetModuleSize(const synthModuleId type)
	{
		return InvokeTypeFunction_synthModuleSize(type, 0U);
	}
}
#endif // __SYNTH_EDITOR
