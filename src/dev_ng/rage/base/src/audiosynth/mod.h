
#ifndef SYNTH_MOD_H
#define SYNTH_MOD_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"
#include "vectormath/vectormath.h"

namespace rage
{
	class synthModulus : public synthModuleBase<2,1,SYNTH_MODULUS>
	{
	public:

		SYNTH_MODULE_NAME("Modulus");

		synthModulus();
		virtual void Synthesize();

		enum 
		{
			INPUT = 0,
			MODULO,
			NUM_MODULUS_INPUTS
		};

		static void Process(float *RESTRICT const inOutBuffer, const float *RESTRICT const moduloBuffer, const u32 numSamples);
		static void Process(float *RESTRICT const inOutBuffer, Vec::Vector_4V_In modulo, const u32 numSamples);
		static float Process(const float input, const float modulo);

	private:
		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_MOD_H


