// 
// audiosynth/oscillatorview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "audiohardware/mixer.h"
#include "dsfoscillator.h"
#include "oscillatorview.h"
#include "oscillator.h"
#include "moduleview.h"
#include "pinview.h"
#include "uidropdownlist.h"

#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"

#include "module.h"
#include "pin.h"

#include "input/mouse.h"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	synthOscillatorView::synthOscillatorView(synthModule *module) : synthModuleView(module)
	{
		AddComponent(&m_ModeList);
		AddComponent(&m_FreqRange);

		m_FreqRange.SetMinRange(0.f, 20000.f);
		m_FreqRange.SetMaxRange(0.f, 20000.f);

		m_StaticDynamicSwitch.AddItem("Static");
		m_StaticDynamicSwitch.AddItem("Dynamic");

		if(IsDigitalOsc())
		{
			for(u32 i = 0; i < GetDigitalOsc()->GetNumModes(); i++)
			{
				m_ModeList.AddItem(GetDigitalOsc()->GetModeName((synthDigitalOscillator::synthOscillatorMode)i));
			}
			m_ModeList.SetCurrentIndex((u32)GetDigitalOsc()->GetMode());

			m_FreqRange.SetNormalizedValues(GetDigitalOsc()->GetMinFrequency()/20000.f, GetDigitalOsc()->GetMaxFrequency()/20000.f);

			if(GetDigitalOsc()->IsStaticOutput())
			{
				m_StaticDynamicSwitch.SetCurrentIndex(0);
			}
			else
			{
				m_StaticDynamicSwitch.SetCurrentIndex(1);
			}
			AddComponent(&m_StaticDynamicSwitch);
		}
		else
		{
			for(u32 i = 0; i < GetAnalogueOsc()->GetNumModes(); i++)
			{
				m_ModeList.AddItem(GetAnalogueOsc()->GetModeName((synthAnalogueOscillator::synthAnalogueOscillatorMode)i));
			}
			m_ModeList.SetCurrentIndex((u32)GetAnalogueOsc()->GetMode());
			m_FreqRange.SetNormalizedValues(GetAnalogueOsc()->GetMinFrequency()/20000.f, GetAnalogueOsc()->GetMaxFrequency()/20000.f);
		}
		m_Texture = NULL;
	}

	void synthOscillatorView::Update()
	{
		// fade out title when editing
		SetTitleAlpha(IsMinimised() ? 1.f : 1.f - GetHoverFade());
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();

		// Mode label
		const f32 moduleHalfHeight = 0.5f * ComputeHeight();
		const f32 modeHeight = 0.25f;//m_ModeList.ComputeHeight();
		const Vector3 modeOffset = (mtx.b * modeHeight * 2.5f);
		mtx.d += modeOffset;

		// position the menu at the centre of the module
		m_ModeList.SetMenuOffset(-modeOffset);
		m_ModeList.SetMatrix(mtx);
		m_ModeList.SetAlpha(GetHoverFade() * GetAlpha());

		if(IsDigitalOsc())
		{
			GetDigitalOsc()->SetMode((synthDigitalOscillator::synthOscillatorMode)m_ModeList.GetCurrentIndex());
		}
		else
		{
			GetAnalogueOsc()->SetMode((synthAnalogueOscillator::synthAnalogueOscillatorMode)m_ModeList.GetCurrentIndex());
		}

		const bool isShowingMenu = m_ModeList.IsShowingMenu() || m_StaticDynamicSwitch.IsShowingMenu();

		m_ModeList.SetShouldDraw(ShouldDraw() && !IsMinimised());
		m_FreqRange.SetShouldDraw(!isShowingMenu && ShouldDraw() && !IsMinimised());
		m_TitleLabel.SetShouldDraw(!m_ModeList.IsShowingMenu() && ShouldDraw() && !IsMinimised());
		
		mtx = GetMatrix();
		const Vector3 yOffset = mtx.b * moduleHalfHeight * 0.25f;
		mtx.d -= yOffset;
		m_FreqRange.SetMatrix(mtx);
		m_FreqRange.SetAlpha(GetHoverFade() * GetAlpha());
		m_FreqRange.SetTextAlpha(GetHoverFade());
		m_FreqRange.SetMinRange(0.f, 20000.f);
		m_FreqRange.SetMaxRange(0.f, 20000.f);
		m_FreqRange.SetTitles("MinFreq", "MaxFreq");
		m_FreqRange.SetWidth(0.4f);

		mtx.d -= (mtx.b * m_FreqRange.ComputeHeight());
		m_StaticDynamicSwitch.SetMatrix(mtx);
		m_StaticDynamicSwitch.SetAlpha(GetHoverFade() * GetAlpha());		
		m_StaticDynamicSwitch.SetShouldDraw(!m_ModeList.IsShowingMenu() && ShouldDraw() && !IsMinimised());

		if(IsDigitalOsc())
		{
			if(m_StaticDynamicSwitch.GetCurrentIndex() == 0)
			{
				GetDigitalOsc()->SetStaticOutput(true);
			}
			else
			{
				GetDigitalOsc()->SetStaticOutput(false);
			}

			GetDigitalOsc()->SetMaxFrequency(m_FreqRange.GetMaxValue());
			GetDigitalOsc()->SetMinFrequency(m_FreqRange.GetMinValue());
			GetDigitalOsc()->GetInput(kDigitalOscillatorFrequency).GetView()->SetStaticViewRange(GetDigitalOsc()->GetMinFrequency(), GetDigitalOsc()->GetMaxFrequency());
		}
		else
		{
			GetAnalogueOsc()->SetMaxFrequency(m_FreqRange.GetMaxValue());
			GetAnalogueOsc()->SetMinFrequency(m_FreqRange.GetMinValue());
			GetAnalogueOsc()->GetInput(kAnalogueOscillatorFrequency).GetView()->SetStaticViewRange(GetAnalogueOsc()->GetMinFrequency(), GetAnalogueOsc()->GetMaxFrequency());
		}
		
		
		
		// render one cycle
		const u32 numSamplesToRender = 128;
		if(IsDigitalOsc())
		{
			if(!m_Texture || m_LastRenderedMode != GetDigitalOsc()->GetMode())
			{
				m_Texture = synthUIComponent::CreateTexture(numSamplesToRender, 1, grcImage::R32F);
			}			
		}
		else
		{
			if(!m_Texture || m_LastRenderedMode != GetAnalogueOsc()->GetMode())
			{
				m_Texture = synthUIComponent::CreateTexture(numSamplesToRender, 1, grcImage::R32F);
			}
		}
		grcTextureLock lock;

		if(m_Texture && m_Texture->LockRect(0,0,lock,grcsWrite))
		{
			f32 *textPtrF32 = (f32*)lock.Base;
			
			const Vector_4V oneTwoThreeFour = V4VConstant<0,0x3F800000,0x40000000,0x40400000>();
			const f32 oneOverNumSamples = 1.f / (f32)numSamplesToRender;
			const Vector_4V phaseStep = V4LoadScalar32IntoSplatted(oneOverNumSamples);
			Vector_4V phaseStepX4 = V4Scale(phaseStep, V4VConstant(V_FOUR));
			Vector_4V phase = V4Scale(phaseStep, oneTwoThreeFour);

			// frequency is based on numSamplesToRender
			const f32 effectiveFreq = 1.f / (f32(numSamplesToRender) / f32(kMixerNativeSampleRate));
			const Vector_4V freq = V4LoadScalar32IntoSplatted(effectiveFreq);
			// 0.8
			const Vector_4V scaling = V4VConstant<0x3F4CCCCC,0x3F4CCCCC,0x3F4CCCCC,0x3F4CCCCC>();
			// 0.1
			const Vector_4V offset = V4VConstant<0x3DCCCCCC,0x3DCCCCCC,0x3DCCCCCC,0x3DCCCCCC>();
			if(IsDigitalOsc())
			{
				for(u32 i = 0; i < numSamplesToRender; i +=4 )
				{
					Vector_4V samples = GetDigitalOsc()->ComputeSampleV(GetDigitalOsc()->GetMode(), phase);
					// shader needs it normalized
					*((Vector_4V*)&textPtrF32[i]) = V4Add(offset,V4Scale(scaling, Vec::V4Scale(Vec::V4Add(Vec::V4VConstant(V_ONE), samples), Vec::V4VConstant(V_HALF))));
					phase = V4Add(phase, phaseStepX4);
				}
				m_LastRenderedMode = GetDigitalOsc()->GetMode();
			}
			else
			{
				// create a temporary analogue oscillator - this is necessary since ComputeSampleV affects the internal state of the oscillator
				synthAnalogueOscillator osc;
				osc.SetMode(GetAnalogueOsc()->GetMode());
				for(u32 i = 0; i < numSamplesToRender; i +=4 )
				{
					// lets shift so that 0 phase is in the middle
					const u32 pixel = (i+(numSamplesToRender>>1)) % numSamplesToRender;
					Vector_4V samples = osc.ComputeSampleV(phase, freq);
					// shader needs it normalized
					*((Vector_4V*)&textPtrF32[pixel]) = V4Add(offset,V4Scale(scaling, Vec::V4Scale(Vec::V4Add(Vec::V4VConstant(V_ONE), samples), Vec::V4VConstant(V_HALF))));
					phase = V4Add(phase, phaseStepX4);
				}

				m_LastRenderedMode = GetAnalogueOsc()->GetMode();
			}
			
			m_Texture->UnlockRect(lock);
		}

		synthModuleView::Update();
	}

	void synthOscillatorView::Render()
	{
		synthModuleView::Render();

		Matrix34 mtx = GetMatrix();
		if(!IsMinimised())
		{
			mtx.d += mtx.b * 2.6f;
		}

		if(!m_ModeList.IsShowingMenu())
		{
			Vector3 scalingVec(2.f,0.9f,1.f);
			mtx.Transform3x3(scalingVec);
			mtx.Scale(scalingVec);
			grcWorldMtx(mtx);
			SetShaderVar("FontTex",m_Texture);
			SetShaderVar("DiffuseTex",m_Texture);
			SetShaderVar("g_WaveXZoom", 1.f);
			SetShaderVar("g_WaveYZoom", 1.f);
			SetShaderVar("g_WaveXOffset", 0.f);
			SetShaderVar("g_GhostLevel", 0.f);
			SetShaderVar("g_TextColour", Color32(0,0,255));
			SetShaderVar("g_AlphaFade", 1.f/*GetAlpha()*/);
			DrawVB("drawwave");	
		}
	}

	bool synthOscillatorView::StartDrag() const
	{
		if(m_FreqRange.IsUnderMouse() || m_ModeList.IsUnderMouse() || m_ModeList.IsShowingMenu() || m_StaticDynamicSwitch.IsUnderMouse() || m_StaticDynamicSwitch.IsShowingMenu())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthOscillatorView::IsActive() const
	{
		return (IsUnderMouse() || m_FreqRange.IsActive());
	}
}
#endif // __SYNTH_EDITOR

