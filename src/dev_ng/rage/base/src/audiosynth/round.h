// 
// audiosynth/round.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_ROUND_H
#define SYNTH_ROUND_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "pin.h"

#include "modulefactory.h"

namespace rage
{
	class synthRound : public synthModuleBase<1,1,SYNTH_ROUND>
	{
	public:
		SYNTH_MODULE_NAME("Round");
		synthRound();
		virtual void Synthesize();

		static void Process(float *RESTRICT const inOutBuffer, const u32 numSamples);
	private:
		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_ROUND_H


