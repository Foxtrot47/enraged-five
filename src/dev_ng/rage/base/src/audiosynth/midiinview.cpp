// 
// audiosynth/midiinview.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "audiohardware/driver.h"

#include "midiinview.h"
#include "midiin.h"

#include "moduleview.h"
#include "pinview.h"
#include "uiplane.h"

#include "module.h"
#include "pin.h"

namespace rage
{
	synthMidiInView::synthMidiInView(synthModule *module) : synthModuleView(module)
	{
        AddComponent(&m_Layout);

		m_ChannelList.AddItem("All Channels");
		m_ChannelList.AddItem("Channel 1");
		m_ChannelList.AddItem("Channel 2");
		m_ChannelList.AddItem("Channel 3");
		m_ChannelList.AddItem("Channel 4");
		m_ChannelList.AddItem("Channel 5");
		m_ChannelList.AddItem("Channel 6");
		m_ChannelList.AddItem("Channel 7");
		m_ChannelList.AddItem("Channel 8");
		m_ChannelList.AddItem("Channel 9");
		m_ChannelList.AddItem("Channel 10");
		m_ChannelList.AddItem("Channel 11");
		m_ChannelList.AddItem("Channel 12");
		m_ChannelList.AddItem("Channel 13");
		m_ChannelList.AddItem("Channel 14");
		m_ChannelList.AddItem("Channel 15");
		m_ChannelList.AddItem("Channel 16");
		m_ChannelList.SetCurrentIndex(GetMidiIn()->GetChannel());

        m_DeviceNameLabel.SetSize(0.2f);
        m_DeviceNameLabel.SetString(audDriver::GetMidiInput()->GetName());

		m_PolyphonySlider.SetRange(1.f, (f32)kMidiInMaxPolyphony);
		m_PolyphonySlider.SetNormalizedValue(GetMidiIn()->GetPolyphony() / (f32)kMidiInMaxPolyphony);
        m_PolyphonySlider.SetWidth(0.4f);
        m_PolyphonySlider.SetTitle("Polyphony");
        m_PolyphonySlider.SetRenderAsInteger(true);
        m_PolyphonySlider.SetDrawTitle(true);

		m_NoteSlider.SetRange(1.f,128.f);
		u32 currentNoteMask = GetMidiIn()->GetNoteMask();
		f32 noteMaskF32;
		if(currentNoteMask > 127)
		{
			noteMaskF32 = 128.f;
		}
		else
		{
			noteMaskF32 = (f32)currentNoteMask;
		}
		m_NoteSlider.SetNormalizedValue(noteMaskF32 / 128.f);
        m_NoteSlider.SetWidth(0.4f);
        m_NoteSlider.SetTitle("Note Mask");
        m_NoteSlider.SetRenderAsInteger(true);
		m_NoteSlider.SetDrawTitle(true);

        m_Layout.Add(&m_ChannelList);
        m_SliderLayout.Add(&m_PolyphonySlider);
        m_SliderLayout.Add(&m_NoteSlider);
        m_Layout.Add(&m_SliderLayout);
        m_Layout.Add(&m_DeviceNameLabel);
        m_Layout.SetOrientation(synthUIHorizontalLayout::kVertical);
        m_Layout.SetPadding(0.5f);
	}

	void synthMidiInView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		synthModuleView::GetBounds(v1,v2);
		Vector3 v3,v4;
		m_ChannelList.GetBounds(v3,v4);

		v1.Min(v1,v3);
		v2.Max(v2,v4);
	}

	void synthMidiInView::Update()
	{
		// fade out title when editing
		if(IsMinimised())
		{
			SetTitleAlpha(1.f);
		}
		else
		{
			SetTitleAlpha(1.f - GetHoverFade());
		}
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();
        mtx.d -= mtx.c * 0.1f;
        m_Layout.SetMatrix(mtx);

        m_PolyphonySlider.SetAlpha(GetHoverFade() * GetAlpha());
		m_PolyphonySlider.SetTextAlpha(GetHoverFade() * GetAlpha());
		
        m_NoteSlider.SetAlpha(GetHoverFade() * GetAlpha());
		m_NoteSlider.SetTextAlpha(GetHoverFade() * GetAlpha());
		
        m_ChannelList.SetAlpha(GetAlpha());
		m_ChannelList.SetShouldDraw(ShouldDraw() && !IsMinimised());

        
		m_DeviceNameLabel.SetAlpha(GetAlpha());
		m_DeviceNameLabel.SetShouldDraw(ShouldDraw() && !m_ChannelList.IsShowingMenu() && !IsMinimised());
		

		GetMidiIn()->SetChannel(m_ChannelList.GetCurrentIndex());
		SetHideTitle(m_ChannelList.IsShowingMenu());
		m_PolyphonySlider.SetShouldDraw(ShouldDraw() && !m_ChannelList.IsShowingMenu() && !IsMinimised());
        m_NoteSlider.SetShouldDraw(ShouldDraw() && !m_ChannelList.IsShowingMenu() && !IsMinimised());

		const u32 polyphony = (u32)m_PolyphonySlider.GetValue();
		GetMidiIn()->SetPolyphony(polyphony);

		const u32 noteMask = (u32)m_NoteSlider.GetValue();
		if(noteMask >= 128)
		{
			GetMidiIn()->SetNoteMask(0xff);
		}
		else
		{
			GetMidiIn()->SetNoteMask(noteMask);
		}

		synthModuleView::Update();
	}

	void synthMidiInView::Render()
	{
		synthModuleView::Render();
	}

	bool synthMidiInView::StartDrag() const
	{
        if(m_Layout.IsActive())
        {
            return false;
        }
		else if(m_ChannelList.IsShowingMenu())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthMidiInView::IsActive() const
	{
		return (IsUnderMouse() || m_ChannelList.IsActive() || m_PolyphonySlider.IsActive() || m_NoteSlider.IsActive());
	}

}
#endif // __SYNTH_EDITOR

