#include "synthdefs.h"

#include "audiohardware/mixer.h"
#include "freeverb.h"
#include "pin.h"

namespace rage
{
	synthFreeverb::synthFreeverb() 
	{
		m_Inputs[kFreeverbSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kFreeverbDamping].Init(this, "Damping",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kFreeverbDamping].SetStaticValue(0.0005f);
		m_Inputs[kFreeverbRoomSize].Init(this, "Room Size",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kFreeverbRoomSize].SetStaticValue(0.85f);
		m_Inputs[kFreeverbWetMix].Init(this, "Wet",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kFreeverbWetMix].SetStaticValue(1.f);
		m_Inputs[kFreeverbDryMix].Init(this, "Dry",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kFreeverbDryMix].SetStaticValue(0.f);
		
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Reverb.Init(1,1);
	}

	synthFreeverb::~synthFreeverb()
	{
		m_Reverb.Shutdown();
	}

	void synthFreeverb::Synthesize()
	{
		float *outPtr = m_Buffer.GetBuffer();

		if(m_Inputs[kFreeverbSignal].IsConnected())
		{
			float *input = m_Inputs[kFreeverbSignal].GetDataBuffer()->GetBuffer();
			sysMemCpy128(outPtr, input, sizeof(float) * kMixBufNumSamples);


			m_Reverb.SetParam(audReverbEffect::Damping, m_Inputs[kFreeverbDamping].GetNormalizedValue(0));
			m_Reverb.SetParam(audReverbEffect::RoomSize, m_Inputs[kFreeverbRoomSize].GetNormalizedValue(0));
			m_Reverb.SetParam(audReverbEffect::WetMix, m_Inputs[kFreeverbWetMix].GetNormalizedValue(0));
			m_Reverb.SetParam(audReverbEffect::DryMix, m_Inputs[kFreeverbDryMix].GetNormalizedValue(0));

			audDspEffectBuffer bufferDesc;

			bufferDesc.NumChannels = 1;
			Assign(bufferDesc.ChannelBufferIds[0], m_Buffer.GetBufferId());
			bufferDesc.ChannelBuffers[0] = m_Buffer.GetBuffer();

			m_Reverb.Process(bufferDesc);
		}	
	}
}

