
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "mixer.h"
#include "mixerview.h"
#include "string.h"

#include "math/amath.h"
#include "vectormath/vectormath.h"
#include "system/memops.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthMixer);

	using namespace Vec;
	synthMixer::synthMixer()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL,true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		for(u32 i = 0; i < NumInputs; i+=2)
		{
			m_Inputs[i].Init(this, "Signal",synthPin::SYNTH_PIN_DATA_SIGNAL);
			m_Inputs[i].SetStaticValue(0.f);
			m_Inputs[i+1].Init(this, "Gain",synthPin::SYNTH_PIN_DATA_NORMALIZED);
			m_Inputs[i+1].SetStaticValue(1.f);
		}		
	}

	void synthMixer::GetInputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Inputs[0];
		numPins = 2;
		for(u32 i = 0; i < NumInputs; i+=2)
		{
			if(m_Inputs[i].IsConnected())
			{
				numPins = 2 + i + 2;
			}
		}
		numPins = Min<u32>(NumInputs,numPins);
	}

	void synthMixer::Synthesize()
	{
		atRangeArray<u32,NumInputs/2> connectedInputs;
		u32 numConnections = 0;
		
		for(u32 i = 0; i < NumInputs; i += 2)
		{
			if(m_Inputs[i].IsConnected())
			{				
				connectedInputs[numConnections++] = i;
			}
		}
		
		Vector_4V *const output = (Vector_4V *)m_Outputs[0].GetDataBuffer()->GetBuffer();
		const u32 numSamplesToOutput = m_Outputs[0].GetDataBuffer()->GetSize();

		// We need the output buffer to start as zeros
		sysMemSet128(output, 0, numSamplesToOutput*sizeof(f32));

		for(u32 connectionIndex = 0; connectionIndex < numConnections ; connectionIndex ++)
		{
			Vector_4V *outputPtr = output;
			const u32 channelIndex = connectedInputs[connectionIndex];
			
			for(u32 sampleIndex = 0 ; sampleIndex < numSamplesToOutput; sampleIndex += 16)
			{	
				const Vector_4V inputSamples0 = m_Inputs[channelIndex].GetSignalValueV(sampleIndex+0);
				const Vector_4V gain0 = m_Inputs[channelIndex+1].GetNormalizedValueV(sampleIndex+0);
				const Vector_4V v0 = outputPtr[0];
				
								
				const Vector_4V inputSamples1 = m_Inputs[channelIndex].GetSignalValueV(sampleIndex+4);
				const Vector_4V gain1 = m_Inputs[channelIndex+1].GetNormalizedValueV(sampleIndex+4);
				const Vector_4V v1 = outputPtr[1];
				
				const Vector_4V out0 = V4AddScaled(v0, inputSamples0, gain0);

				const Vector_4V inputSamples2 = m_Inputs[channelIndex].GetSignalValueV(sampleIndex+8);
				const Vector_4V gain2 = m_Inputs[channelIndex+1].GetNormalizedValueV(sampleIndex+8);
				const Vector_4V v2 = outputPtr[2];
				const Vector_4V out1 = V4AddScaled(v1, inputSamples1, gain1);
				
				const Vector_4V inputSamples3 = m_Inputs[channelIndex].GetSignalValueV(sampleIndex+12);
				const Vector_4V gain3 = m_Inputs[channelIndex+1].GetNormalizedValueV(sampleIndex+12);
				const Vector_4V v3 = outputPtr[3];
				
				const Vector_4V out2 = V4AddScaled(v2, inputSamples2, gain2);					
				*outputPtr++ = out0;

				const Vector_4V out3 = V4AddScaled(v3, inputSamples3, gain3);
				
				
				*outputPtr++ = out1;
				*outputPtr++ = out2;
				*outputPtr++ = out3;
			}			
		}
	}
}

#endif
