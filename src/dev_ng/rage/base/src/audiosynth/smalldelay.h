// 
// smalldelay.h
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUD_SMALLDELAY_H
#define AUD_SMALLDELAY_H

#include "vectormath/vectormath.h"

#include "module.h"
#include "modulefactory.h"

namespace rage
{
	enum synthSmallDelayInputs
	{
		kSmallDelaySignal = 0,
		kSmallDelayLength,
		kSmallDelayFeedback,
		kSmallDelayNumInputs
	};
#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif

	class synthSmallDelay : public synthModuleBase<kSmallDelayNumInputs, 1, SYNTH_SMALLDELAY>
	{
	public:

		enum Mode
		{
			Fractional,
			NonInterpolating,
		};
		SYNTH_MODULE_NAME("MicroDelay");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthSmallDelay();
		virtual ~synthSmallDelay();

		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();
		virtual void ResetProcessing();

		static void Process_Frac(float *inOut, const float delayTime, Vec::Vector_4V_InOut state, const u32 numSamples);
		static void Process_Frac_Feedback(float *inOut, const float delayTime, const float feedback, Vec::Vector_4V_InOut state, const u32 numSamples);

		static void Process_NonInterp(float *inOut, const float delayTime, Vec::Vector_4V_InOut state, const u32 numSamples);
		static void Process_NonInterp_Feedback(float *inOut, const float delayTime, const float feedback, Vec::Vector_4V_InOut state, const u32 numSamples);

		void SetMode(Mode m) { m_Mode = m; }
		Mode GetMode() const { return m_Mode; }
	private:
		Vec::Vector_4V m_DelayLine;
		synthSampleFrame m_OutputBuffer;
		Mode m_Mode;
	};
#if __WIN32
#pragma warning(pop)
#endif

} // namespace rage


#endif // AUD_SMALLDELAY_H
