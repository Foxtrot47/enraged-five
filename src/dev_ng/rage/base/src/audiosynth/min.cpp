// 
// audiosynth/min.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "min.h"
#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	synthMin::synthMin()
	{
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);

		m_Inputs[kMinSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);

		m_Inputs[kMinReset].Init(this, "Reset", synthPin::SYNTH_PIN_DATA_NORMALIZED);

		m_Mode = kMinStatic;
		ResetProcessing();
	}

	void synthMin::ResetProcessing()
	{
		m_CurrentVal = 0.f;
	}

	void synthMin::Synthesize()
	{	
		if(m_Inputs[kMinReset].GetNormalizedValue(0) >= 1.f)
		{
			m_CurrentVal = 0.f;
		}

		if(m_Inputs[kMinSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_CurrentVal = Min(m_CurrentVal, m_Inputs[kMinSignal].GetNormalizedValue(0));
			m_Outputs[0].SetStaticValue(m_CurrentVal);
		}
		else
		{
			// dynamic input
			const Vector_4V *RESTRICT inputPtr = (const Vector_4V*)m_Inputs[0].GetDataBuffer()->GetBuffer();
			const u32 numSamples = m_Buffer.GetSize();

			if(m_Mode == kMinStatic)
			{
				Vector_4V currentMin = V4LoadScalar32IntoSplatted(m_CurrentVal);

				for(u32 i = 0; i < numSamples; i+=4)
				{
					Vector_4V inV = V4Abs(*inputPtr++);
					currentMin = V4Min(inV, currentMin);
				}
				// consolidate into X
				const Vector_4V splattedMin = V4Min(currentMin, V4Min(V4Min(V4SplatY(currentMin), V4SplatZ(currentMin)), V4SplatW(currentMin)));

				m_CurrentVal = GetX(splattedMin);
				m_Outputs[0].SetStaticValue(m_CurrentVal);
			}
			else
			{
				// dynamic, sample accurate, expensive mode
				m_Outputs[0].SetDataBuffer(&m_Buffer);

				Vector_4V *RESTRICT outputPtr = (Vector_4V*)m_Outputs[0].GetDataBuffer()->GetBuffer();
				Vector_4V currentMinSplatted = V4LoadScalar32IntoSplatted(m_CurrentVal);

				for(u32 i = 0; i < numSamples; i+=4)
				{
					Vector_4V inV = V4Abs(*inputPtr++);

					// need to process serially
					Vector_4V currentMinSplattedX = V4Min(V4SplatX(inV), currentMinSplatted);
					Vector_4V currentMinSplattedY = V4Min(V4SplatY(inV), currentMinSplattedX);
					Vector_4V currentMinSplattedZ = V4Min(V4SplatZ(inV), currentMinSplattedY);
					Vector_4V currentMinSplattedW = V4Min(V4SplatW(inV), currentMinSplattedZ);

					currentMinSplatted = currentMinSplattedW;

					// and recombine into non-splatted...
					Vector_4V outputXY = V4PermuteTwo<X1,X2,X1,X2>(currentMinSplattedX, currentMinSplattedY);
					Vector_4V outputZW = V4PermuteTwo<X1,X2,X1,X2>(currentMinSplattedZ, currentMinSplattedW);
					Vector_4V output = V4PermuteTwo<X1,Y1,X2,Y2>(outputXY, outputZW);

					*outputPtr++ = output;
				}

				m_CurrentVal = GetX(currentMinSplatted);
			}
		}
	}

	float synthMin::Process(const float *RESTRICT const inBuffer, const float resetTrigger, Vector_4V_InOut state, const u32 numSamples)
	{
		const Vector_4V *inPtr = reinterpret_cast<const Vector_4V*>(inBuffer);

		Vector_4V minVal = state;

		if(resetTrigger >= 1.f)
		{
			minVal = V4VConstant(V_ZERO);
		}

		u32 count = numSamples>>2;
		while(count--)
		{
			minVal = V4Min(minVal, V4Abs(*inPtr++));
		}
		// consolidate into X
		const Vector_4V splattedMin = V4Min(minVal, V4Min(V4Min(V4SplatY(minVal), V4SplatZ(minVal)), V4SplatW(minVal)));

		state = splattedMin;
		return GetX(splattedMin);
	}

	float synthMin::Process(const float input, const float resetTrigger, Vec::Vector_4V_InOut state)
	{
		float currentMin = GetX(state);
		if(resetTrigger >= 1.f)
		{
			currentMin = 0.f;
		}
		const float ret = Min(currentMin, Abs(input));
		SetX(state, ret);
		return ret;
	}
} // namespace rage
