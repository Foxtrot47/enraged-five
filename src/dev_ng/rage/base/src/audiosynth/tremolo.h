// 
// audiosynth/tremolo.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_TREMOLO_H
#define SYNTH_TREMOLO_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"

#include "sampleframe.h"
#include "module.h"
#include "pin.h"

#include "modulefactory.h"

namespace rage
{
	class synthTremolo : public synthModuleBase<4,1,SYNTH_TREMOLO>
	{
	public:

		SYNTH_MODULE_NAME("Tremolo");

		synthTremolo();
		virtual void Synthesize();

		enum synthTremoloInputs
		{
			SIGNAL,
			DEPTH,
			FREQUENCY,
			WETMIX,

			kMaxTremoloInputs
		};
		CompileTimeAssert((s32)kMaxTremoloInputs == (s32)NumInputs);
	
	private:

		synthSampleFrame m_Buffer;

		s32 m_DelayLineLength;
		s32 m_DelayLineWriteIndex;
		f32 m_DelayLineReadIndex;
		f32 m_InterpolationRate;
		f32 *m_DelayLine;
		bool m_GoingUp;
	};
}
#endif
#endif // SYNTH_TREMOLO_H

