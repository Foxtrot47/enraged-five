#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicurveview.h"
#include "uicomponent.h"

#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"

namespace rage
{

synthUICurveView::synthUICurveView()
{
	m_Texture = NULL;
	m_Provider = NULL;
	m_GridColor = Color32(1.f,1.f,1.f,1.f);
	m_CurveColor = Color32(0.f,0.4f,0.0f,1.f);
	m_GridVerticalSlices = 4;
	m_GridHorizontalSlices = 10;

	AddComponent(&m_Label);
	
	
	m_DrawGrid = true;
	m_AlwaysRefresh = false;
	m_NeedRefresh = false;
}

synthUICurveView::~synthUICurveView()
{
	if(m_Texture)
	{
		m_Texture->Release();
		m_Texture = NULL;
	}
}

void synthUICurveView::GetBounds(Vector3 &v1, Vector3 &v2) const
{
	v1 = Vector3(-1.f,-1.f,0.f);
	v2 = Vector3(1.f,1.f,0.f);
	GetMatrix().Transform(v1);
	GetMatrix().Transform(v2);
}

void synthUICurveView::Update()
{
	static char buf[128];
	formatf(buf, "(%.2f, %.2f)", GetRelMouseX(), GetRelMouseY());

	Matrix34 mat;
	mat.Identity();
	Vector3 v1,v2;
	GetBounds(v1,v2);
	mat.d = v1;
	
	m_Label.SetMatrix(mat);
	m_Label.SetString(buf);
	m_Label.SetSize(0.125f);
	m_Label.SetAlpha(GetHoverFade());

	float width,height;
	m_Label.ComputeWidthAndHeight(width, height);

	mat.d += (mat.a * (width / 2.f));
	mat.d += (mat.b * (height / 2.f));
	m_Label.SetMatrix(mat);

	synthUIView::Update();
}

void synthUICurveView::RasterizeCurve()
{
	// Draw response curve
	const u32 numSamples = 256;
	if(m_Provider && (m_AlwaysRefresh || m_NeedRefresh || m_Texture == NULL))
	{
		if(!m_Texture)
		{
			m_Texture = synthUIComponent::CreateTexture(numSamples, 1, grcImage::R32F);			
		}			

		grcTextureLock lock;

		m_Texture->LockRect(0,0,lock,grcsWrite);
		f32 *textPtrF32 = (f32*)lock.Base;

		// rasterize the evaluated curve
		m_Provider->Evaluate(textPtrF32, numSamples);		

		m_Texture->UnlockRect(lock);
	}
}


void synthUICurveView::DrawGrid(const s32 numVerticalSlices, const s32 numHorizontalSlices, const Color32 color) const
{
	Assert(numVerticalSlices >= 1);
	Assert(numHorizontalSlices >= 1);

	grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);
	grcStateBlock::SetBlendState(grcStateBlock::BS_Normal);

	Vector3 p0 = Vector3(-1.f, -1.f, 0.f), p1 = Vector3(-1.f, 1.f, 0.f);
	Vector3 offset = Vector3(2.f / (float)(numVerticalSlices), 0.f, 0.f);		
	for(s32 x = 0; x < numVerticalSlices + 1; x++)
	{
		grcDrawLine(p0, p1, color);
		p0 += offset;
		p1 += offset;
	}

	p0 = Vector3(-1.f, -1.f, 0.f);
	p1 = Vector3(1.f, -1.f, 0.f);
	offset = Vector3(0.f, 2.f / (float)(numHorizontalSlices), 0.f);
	for(s32 x = 0; x < numHorizontalSlices + 1; x++)
	{
		grcDrawLine(p0, p1, color);
		p0 += offset;
		p1 += offset;
	}
}

void synthUICurveView::Render()
{	
	if(m_DrawGrid)
	{	
		DrawGrid(m_GridVerticalSlices, m_GridHorizontalSlices, m_GridColor);
	}

	// Rasterize as part of Render() rather than Update(), so we can skip it if we're off screen.
	RasterizeCurve();

	SetShaderVar("FontTex",m_Texture);
	SetShaderVar("DiffuseTex",m_Texture);
	SetShaderVar("g_WaveXZoom", 1.f);
	SetShaderVar("g_WaveYZoom", 1.f);
	SetShaderVar("g_WaveXOffset", 0.f);
	SetShaderVar("g_GhostLevel", 0.f);
	SetShaderVar("g_TextColour", m_CurveColor);
	SetShaderVar("g_AlphaFade", GetAlpha());
	DrawVB("drawwave");

	synthUIView::Render();
}

} //namespace rage

#endif // __SYNTH_EDITOR
