// 
// audiosynth/divider.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_DIVIDER_H
#define SYNTH_DIVIDER_H
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthDivider : public synthModuleBase<2, 1, SYNTH_DIVIDER>
	{
	public:

		synthDivider();
		virtual ~synthDivider(){};

		virtual void Synthesize();

		virtual const char *GetName() const { return "Divider"; }

	private:

		synthSampleFrame m_Buffer;
	};
}
#endif

#endif // SYNTH_DIVIDER_H


