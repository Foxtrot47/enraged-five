// 
// audiosynth/randomizerview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_RANDOMIZERVIEW_H
#define SYNTH_RANDOMIZERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uidropdownlist.h"
#include "uihorizontallayout.h"

namespace rage
{
	class synthRandomizer;
	class synthRandomizerView : public synthModuleView
	{
	public:

		synthRandomizerView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();

		virtual u32 GetModuleHeight() const { return 3; }

	private:

		synthRandomizer *GetRandomizerModule()
		{
			return (synthRandomizer*)m_Module;
		}

		synthUIDropDownList m_QuantizeSwitch;
		synthUIDropDownList m_ModeSwitch;
		synthUIHorizontalLayout m_Layout;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_RANDOMIZER_VIEW

