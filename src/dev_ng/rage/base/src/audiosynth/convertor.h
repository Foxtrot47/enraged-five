// 
// audiosynth/convertor.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_CONVERTER_H
#define SYNTH_CONVERTER_H

#include "synthdefs.h"

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthSignalConverter : public synthModuleBase<1,1,SYNTH_CONVERTER>
	{
	public:
		SYNTH_MODULE_NAME("Converter");

		synthSignalConverter();
		void Synthesize(synthContext &context);

	private:

		synthSampleFrame m_Buffer;
	};
}

#endif // SYNTH_CONVERTER_H


