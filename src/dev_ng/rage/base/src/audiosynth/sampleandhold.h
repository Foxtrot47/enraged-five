
#ifndef SYNTH_SAMPLEANDHOLD_H
#define SYNTH_SAMPLEANDHOLD_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{
	enum synthSampleAndHoldInputs
	{
		kSampleAndHoldSignal,
		kSampleAndHoldTrigger,
		
		kSampleAndHoldNumInputs
	};

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif

	class synthSampleAndHold : public synthModuleBase<kSampleAndHoldNumInputs, 1, SYNTH_SAMPLEANDHOLD>
	{
	public:
		SYNTH_MODULE_NAME("Sample&Hold");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthSampleAndHold();
		virtual void Synthesize();
		virtual void SerializeState(synthModuleSerializer *serializer);		
		virtual void ResetProcessing();
		void SetDynamicOutput(const bool dynamicOutput) { m_DynamicOutput = dynamicOutput; }
		bool GetDynamicOutput() const { return m_DynamicOutput; }

		static float ProcessStatic(const float input, const float trigger, Vec::Vector_4V_InOut state);

	private:

		Vec::Vector_4V m_State;

		f32 m_LastVal;
		synthSampleFrame m_Buffer;
		bool m_DynamicOutput;
	};
}

#if __WIN32
#pragma warning(pop)
#endif

#endif
#endif // SYNTH_SAMPLEANDHOLD_H

