#if __XENON

#include "synthcorexapo.h"
#include "system/memops.h"
#include "audiohardware/channel.h"
#include "audiohardware/framebufferpool.h"

namespace rage
{

BANK_ONLY(bool g_DisableXAPOProcessing = false);

synthCoreXAPO::synthCoreXAPO()
: m_OutputBufferId(0xffff)
, m_Enabled(true)
{

}

synthCoreXAPO::~synthCoreXAPO()
{
	if(m_OutputBufferId != 0xffff)
	{
		g_FrameBufferPool->Free(m_OutputBufferId);
		m_OutputBufferId = 0xffff;
	}
}

bool synthCoreXAPO::Init()
{
	return true;
}

void synthCoreXAPO::DoProcess(const synthCoreXAPOParam& UNUSED_PARAM(params), float* __restrict pData, u32 ASSERT_ONLY(numFrames), u32 ASSERT_ONLY(numChannels))
{
	audAssertf(numFrames == kMixBufNumSamples, "Unsupported numFrames: %u", numFrames);
	audAssertf(numChannels == 1, "Unsupported number of channels: %u", numChannels);

	if(m_Enabled)
	{
		if(m_OutputBufferId == 0xffff)
		{
			m_OutputBufferId = g_FrameBufferPool->Allocate();			
		}
		const float *buffers[] = {pData};
		atRangeArray<u16, audPcmSource::kMaxPcmSourceChannels> outputBuffers;
		Assign(outputBuffers[0], m_OutputBufferId);
		Assign(outputBuffers[1], 0xffff);
		Assign(outputBuffers[2], 0xffff);
		Assign(outputBuffers[3], 0xffff);
	
		m_Synth.GenerateFrame(buffers, outputBuffers);
	
		const float *outputData = g_FrameBufferPool->GetBuffer(m_OutputBufferId);

		sysMemCpy(pData, outputData, sizeof(float) * kMixBufNumSamples);
	}
}

void synthCoreXAPO::OnSetParameters(const synthCoreXAPOParam& params)
{
	if(params.hash == Params::SynthName)
	{
		m_Synth.Init(params.u32Val);		
	}
	else if(params.hash == Params::Enabled)
	{
		m_Enabled = (params.floatVal == 1.f);

		// Free up our output buffer as soon as we're disabled
		if(!m_Enabled && m_OutputBufferId != 0xffff)
		{
			g_FrameBufferPool->Free(m_OutputBufferId);
			m_OutputBufferId = 0xffff;
		}
	}
	else
	{
		m_Synth.SetVariableValue(params.hash, params.floatVal);
	}	
}

} // namespace rage

#endif // __XENON
