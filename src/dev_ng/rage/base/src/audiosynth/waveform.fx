#pragma dcl position diffuse texcoord0 normal

#if __WIN32PC || __MAX
#define NO_SKINNING
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_xplatformtexturefetchmacros.fxh"
#include "../shaderlib/rage_blendsets.h"

BeginSampler(sampler1D,diffuseTexture,TextureSampler,DiffuseTex)
	string UIName="Waveform Data";
ContinueSampler(sampler1D,diffuseTexture,TextureSampler,DiffuseTex)
		AddressU  = CLAMP;        
		AddressV  = CLAMP;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR;
EndSampler;

BeginSampler(sampler2D,fontTexture,FontTexture,FontTex)
	string UIName="Font Image";
ContinueSampler(sampler2D,fontTexture,FontTexture,FontTex)
		AddressU  = CLAMP;        
		AddressV  = CLAMP;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR;
EndSampler;

float g_HighlightScale;
float g_TextShadowScale;
float g_WidthScalar;
float3 g_TextColour;
float3 g_TextShadowColour;
float3 g_EdgeHighlightColour;
float g_AlphaFade;
float g_CurrentValue;
float g_WaveXZoom;
float g_WaveYZoom;
float g_WaveXOffset;
float g_GhostLevel;

struct inputVertex 
{
	float3 pos	: POSITION;
	float2 tc	: TEXCOORD0;
};

struct outputVertex 
{
	float4 pos	: POSITION;
	float2 tc	: TEXCOORD0;
};

outputVertex VS_Transform(inputVertex IN) 
{
	outputVertex OUT;
	OUT.pos = mul(float4(IN.pos,1), gWorldViewProj);
	OUT.tc = IN.tc;
	return OUT;
}


outputVertex VS_AlignedQuadTransform(inputVertex IN)
{
	outputVertex OUT;
	float radiusW = 1.f;
	float radiusH = 1.f;

	float3 up		= normalize( gViewInverse[0].xyz) * radiusW; 
	float3 across	= normalize(-gViewInverse[1].xyz) * radiusH;
	float3 offset	= float3(0.0f,0.0f,0.0f);//uvs.x*up + uvs.y*across;


	offset.xyz += IN.pos.xyz;	// offset from middle of "mesh of 4-in-1's";

	float3 IN_pos;
	IN_pos.x = dot(offset, (float3)gWorld[0]);	// inverse local rotation - it will be re-applied by wvp matrix
	IN_pos.y = dot(offset, (float3)gWorld[1]);
	IN_pos.z = dot(offset, (float3)gWorld[2]);

	OUT.pos		= mul(float4(IN_pos.xyz,1), gWorldViewProj);

	OUT.tc	= IN.tc;
	
	return(OUT);
}
	

float ComputeEdgeFadeAlpha(float expScalar, float x, float y)
{
	// fade out edges to prevent aliasing
	float nearestY = max(y,1.f-y);
	float nearestX = max(x,1.f-x);
	float alpha = 1.f - pow(max(nearestX,nearestY),150 * expScalar);
	// fade out corners
	alpha -= pow(max(abs(x-y),abs(1.f-x-y)),8);
	
	return alpha;
}

float4 ComputeEdgeFadeAndHighlight(float expScalar, float highlightScale, float x, float y)
{
	float alpha = ComputeEdgeFadeAlpha(expScalar, x, y);
	// color edges on y axis
	float nearestY = max(y,1.f-y);
	float exponent = expScalar * ((highlightScale>0.5f) ? 45.f : 95.f);
	float tint = pow(nearestY,exponent);
	
	float3 result = tint * g_EdgeHighlightColour;
	float4 ret; 
	ret.xyz = result; ret.w = alpha;
	return ret;
}

float SampleWave(sampler1D waveformSampler, float x, float offset)
{
	float baseTexU = (1.f/g_WaveXZoom) * x + g_WaveXOffset;
	float v = tex1D(waveformSampler,baseTexU+offset).x;
	float unclamped = (1.f + ((1.f-v*2.f) * -g_WaveYZoom))/2.f;
	
	return min(max(unclamped,0.f), 1.f);
}

float4 RenderWave(outputVertex IN, sampler1D waveformSampler)
{
	const float3 white = float3(1.f,1.f,1.f);

	float offset = 1.f/(768.f*g_WaveXZoom);
	
	float delta;
	delta =  1.f - min(min(abs(SampleWave(waveformSampler,IN.tc.x,0)-IN.tc.y),abs(SampleWave(waveformSampler,IN.tc.x,-offset)-IN.tc.y)),abs(SampleWave(waveformSampler,IN.tc.x,offset)-IN.tc.y));

	
	float val = tex1D(waveformSampler,(1.f/g_WaveXZoom) * IN.tc.x + g_WaveXOffset).x;
	// we're clipping if val is < 0 or > 1
	float clipAmount = min(1.f,max(max(0.f, val-1.f),max(0.f, 0.f-val))*1000.f) * pow(delta, 15);
	
	float focus1Intensity = pow(delta, 45);
	float focus2Intensity = pow(delta,15)*0.8f;
	
	float3 focus1 = (g_TextColour*focus1Intensity);
	float3 focus2 = (g_TextColour*focus2Intensity);
		
	float3 clipColour = float3((clipAmount*2.f),0.f,0.f);
	
	float3 highlight = pow(delta,85)*white;
		
	float3 result = focus1 + focus2 + highlight + clipColour;
	float alpha = ComputeEdgeFadeAlpha(1.f,IN.tc.x,IN.tc.y);
	float4 ret = float4(0.f,0.f,0.f,alpha);
	ret.xyz = result;
	ret.w = alpha * (ret.x+ret.y+ret.z);
	
	return ret * g_AlphaFade;
}

float4 PS_DrawCurve(outputVertex IN) : COLOR0
{
	return RenderWave(IN, TextureSampler)*0.4 + RenderWave(IN, TextureSampler)*0.5;
}

float4 PS_DrawWave(outputVertex IN) : COLOR0
{
	return RenderWave(IN, TextureSampler);
}

float4 DrawSpectrum(outputVertex IN)
{
	const float4 white = float4(1.f,1.f,1.f,1.f);

	float expX = pow(IN.tc.x,1.5f);
	float baseTexU = (1.f/g_WaveXZoom) * expX + g_WaveXOffset;
	float val = tex1D(TextureSampler,baseTexU);
	val = 1.f - (20.f * log10(val))/-100.f;
	float delta = (val  - IN.tc.y);
	// we're not interested if this pixel is above the sampled value
	float clampedDelta = max(0,delta);
	float normalizedDelta = clampedDelta*2.f*g_WaveYZoom;

	float2 mirroredCoors = float2(1.f-IN.tc.x,1.f-IN.tc.y);
	float4 lastFrame = tex2D(FontTexture,mirroredCoors);
	float intensity = pow(normalizedDelta,15);
	float4 ret = float4(intensity*IN.tc.y*0.001f*IN.tc.x,intensity*IN.tc.y*0.001f*(1.f-IN.tc.x),intensity*0.001f,1.f) + (lastFrame * g_GhostLevel);
	return ret;
}

float4 PS_DrawSpectrum(outputVertex IN) : COLOR0
{
	return DrawSpectrum(IN);
}

float4 PS_DrawFreqResp(outputVertex IN) : COLOR0
{
	float4 spectrum = DrawSpectrum(IN);
	float alpha = ComputeEdgeFadeAlpha(1.f, IN.tc.x, IN.tc.y);
	spectrum.w = alpha * min(1.f,(spectrum.x+spectrum.y+spectrum.z)*0.5f);
	return spectrum * g_AlphaFade;
}

float4 PS_DrawTexturedQuad_Mirrored(outputVertex IN) : COLOR0
{
	float4 ret = ComputeEdgeFadeAndHighlight(1,g_HighlightScale, IN.tc.x, IN.tc.y);
	float2 texCoors = float2(1.f - IN.tc.x, 1.f - IN.tc.y);
	float4 diffuse = tex2D(FontTexture, texCoors);
	ret.xyz += diffuse.xyz;
	return ret * g_AlphaFade;
}

float4 PS_DrawTexturedQuad(outputVertex IN) : COLOR0
{
	float4 ret = ComputeEdgeFadeAndHighlight(10, g_HighlightScale, IN.tc.x, IN.tc.y);
	
	float3 offColour = float3(0.f,0.f,0.f);
	float3 onColourMid = float3(0.f,0.f,0.8f);
	float3 onColourEdge = float3(0.5f,0.5f,1.f);
	
	float edgeFactor = max(IN.tc.y, 1.f - IN.tc.y);
	float3 onColour = lerp(onColourMid, onColourEdge, pow(edgeFactor, 4));
	
	float2 rescaledTexCoors = IN.tc;
	float3 diffuse = lerp(offColour,onColour,tex2D(FontTexture, rescaledTexCoors).x);
	ret.xyz += diffuse;
	return ret * g_AlphaFade;
}

float4 PS_DrawVirtualisationDemo(outputVertex IN) : COLOR0
{
	float4 ret = tex2D(FontTexture, IN.tc) * 3.0f;
	ret.w = g_AlphaFade;
	return ret;
}

float4 PS_DrawFontChar(outputVertex IN) : COLOR0
{
	float2 translatedUV;
	translatedUV.x = lerp(g_WaveXZoom, g_WaveXZoom + 0.1f, IN.tc.x);
	translatedUV.y = lerp(g_WaveYZoom, g_WaveYZoom + 0.1f, 1.f-IN.tc.y);
	
	float4 diffuse = tex2D(FontTexture, translatedUV);
	return diffuse * g_AlphaFade;
}

float4 PS_DrawBlurredScreen(outputVertex IN) : COLOR0
{
	float2 Tex = float2(IN.tc.x, 1.f - IN.tc.y);
	
	float4 Color = 0.0f;
		
	// TODO: pass this in via shader variable
	float xOffset = 0.25f / 200.f;
	float yOffset = 0.25f / 150.f;

	Color 	=				  tex2D(FontTexture, Tex + float2(xOffset,yOffset))/8
							+ tex2D(FontTexture, Tex - float2(xOffset,yOffset))/8
							+ tex2D(FontTexture, Tex + float2(-xOffset,yOffset))/8
							+ tex2D(FontTexture, Tex + float2(xOffset,-yOffset))/8
							+ tex2D(FontTexture, Tex + float2(2.f * xOffset, 1.5f * yOffset))/8
							+ tex2D(FontTexture, Tex - float2(1.5f * xOffset, 2.f * yOffset))/8
							+ tex2D(FontTexture, Tex + float2(-1.8f * xOffset,1.9f * yOffset))/8
							+ tex2D(FontTexture, Tex + float2(1.8f * xOffset, -1.9f * yOffset))/8;

	
	float4 ColorSharp = tex2D(FontTexture, Tex);
	Color = lerp(ColorSharp,Color, 0.5f);
	
	// desaturate	
	float s = Color.x*Color.x + Color.y*Color.y + Color.z*Color.z;
    return lerp(Color,float4(s,s,s,1.f),0.85);
}

float4 PS_DrawTexturedReflection(outputVertex IN) : COLOR0
{
	float y = (1.f - IN.tc.y)*1.5f;
	float4 ret = ComputeEdgeFadeAndHighlight(1,g_HighlightScale, IN.tc.x, y);
	float2 texCoors = float2(1.f - IN.tc.x, IN.tc.y);
	float4 diffuse = tex2D(FontTexture, texCoors);
	ret.xyz += diffuse.xyz;
	float y2 = IN.tc.y*IN.tc.y;
	ret.w *= y2 * y2 * y2;
	return ret * g_AlphaFade * 0.9f;
}

float4 PS_DrawScrollbar(outputVertex IN) : COLOR0
{
	const float3 white = float3(1.f,1.f,1.f);

	float delta = max(0.f,min(1.f,1.f - abs(g_CurrentValue-IN.tc.y)));
	
	float focus1Intensity = pow(delta, 45);
	float focus2Intensity = pow(delta,15)*0.8f;
	float3 focus1 = g_TextColour * focus1Intensity;
	float3 focus2 = g_TextColour * focus2Intensity;
	float3 highlight = pow(delta,85)*white;
	
	float3 result = focus1 + focus2 + highlight;
	
	float4 ret = ComputeEdgeFadeAndHighlight(1,g_HighlightScale, IN.tc.x, IN.tc.y);
	ret.xyz += result;
	return ret * g_AlphaFade;
}

float4 PS_DrawScrollbarReflection(outputVertex IN) : COLOR0
{
	const float3 white = float3(1.f,1.f,1.f);

	float y = 1.f - IN.tc.y;
	float delta = max(0.f,min(1.f,1.f - abs(g_CurrentValue-y)));

	float focus1Intensity = pow(delta, 45);
	float focus2Intensity = pow(delta,15)*0.8f;
	float3 focus1 = g_TextColour * focus1Intensity;
	float3 focus2 = g_TextColour * focus2Intensity;
	float3 highlight = pow(delta,85)*white;
	
	float3 result = focus1 + focus2 + highlight;
	
	float4 ret = ComputeEdgeFadeAndHighlight(1,g_HighlightScale, IN.tc.x, y);
	ret.xyz += result;
	float y2 = IN.tc.y*IN.tc.y;
	ret.w *= y2*y2*y2;
	return ret * g_AlphaFade * 0.9f;
}

float4 PS_DrawAntialisedText(outputVertex IN) : COLOR0
{
	float2 tc = IN.tc;
	tc.x *= g_WidthScalar;
	float frontFaceAlpha = tex2D(FontTexture, tc).w;

	float4 frontFaceColour;
	frontFaceColour.xyz = g_TextColour;
	frontFaceColour.w = 1.f;
	// fade to gray
	float grayLevel = 0.7f*(1.f - IN.tc.y);
	frontFaceColour.xyz -= float3(grayLevel,grayLevel,grayLevel);
	float4 frontFace =  (frontFaceColour * frontFaceAlpha);
	
	float bg = float4(0,0,0,0);// ComputeEdgeFadeAndHighlight(false, IN.tc.x, IN.tc.y);

	
		
	float shadowAlpha = 0.2f * (tex2D(FontTexture,tc + float2(0.005f*g_WidthScalar,0.005f)).w +
							tex2D(FontTexture,tc + float2(-0.005f*g_WidthScalar,0.005f)).w + 
							tex2D(FontTexture,tc + float2(0.005f*g_WidthScalar,-0.005f)).w +
							tex2D(FontTexture,tc + float2(-0.005f*g_WidthScalar,-0.005f)).w +
							tex2D(FontTexture,tc).w);
								
	float4 shadow;
	shadow.xyz = g_TextShadowColour * shadowAlpha;
	shadow.w = shadowAlpha;
	shadow *= g_TextShadowScale;
	return (bg + frontFace + shadow) * g_AlphaFade;

}

float4 DrawWireElement(outputVertex IN, float freq, float ampl, float offset, float alpha)
{
	float distFromRipple = (0.5 - abs(IN.tc.x-0.5)) * 2;
	
	float distScalar = distFromRipple;//pow(distFromRipple, 2);
	float distortion = sin(offset + freq * PI * (IN.tc.x-g_WaveXOffset)) * distScalar * ampl;
	float val = 0.2f + (0.3f * (1.f + distortion));
	
	float delta = 1.f - abs(val-IN.tc.y);

	// we're clipping if val is < -1 or > 1
	float clipAlpha = pow(delta, 8);
	float clipAmount = min(1.f,max(0.f, abs(g_CurrentValue)-1.05f)*5000.f);
	
	const float3 intensityScaledWhite = float3(1.f,1.f,1.f) * abs(g_CurrentValue) * 3.f;
	const float3 clipColor = float3(1.f,0.f,0.f) * clipAmount;
		
	float focus1Intensity = pow(delta, 12);
	float3 focus1 = g_TextColour * focus1Intensity;
	float focus2Intensity = pow(delta,18)*0.8f;
	float3 focus2 = g_TextColour * focus2Intensity;
	float3 highlight = pow(delta,76)*intensityScaledWhite;
	
	float3 unclippedResult = focus1 + focus2 + highlight;
	float3 clippedResult = clipColor;
	
	float3 result = unclippedResult + (clippedResult-unclippedResult) * clipAmount;
	
	float4 ret;
	ret.xyz = result;
	
	ret.w = pow(delta,24) * alpha * max(0.4f, distFromRipple);
	
	return ret;
}

float4 DrawWireNoise(float2 texCoors, float offset, float tcXOffset)
{
	texCoors.x += tcXOffset;
	return float4(1.f,1.f,1.f,1.f) * (0.5f * pow(1.f - abs(offset-texCoors.y),17) * tex1D(TextureSampler,texCoors.x));
}

float4 PS_DrawWire(outputVertex IN) : COLOR0
{
	float4 ret = (DrawWireElement(IN, 6, 0.5, 0, 0.7) + 
			DrawWireElement(IN, 4, 0.7, PI/2, 0.5) + 
			DrawWireElement(IN, 2, 0.9, PI, 0.3) + 
			DrawWireElement(IN, 2, 1.0, (PI/4)*3, 0.2) + 
			DrawWireElement(IN, 2, 1.0, (PI/3), 0.2));
	

	float4 noise = DrawWireNoise(IN.tc,0.5f,(1.f-g_WaveXOffset)) +
					0.5f*DrawWireNoise(IN.tc,0.54f,(1.f-g_WaveXOffset) + 0.3f) +
					0.5f*DrawWireNoise(IN.tc,0.46f, (1.f-g_WaveXOffset) + 0.6f) + 
					0.4f*DrawWireNoise(IN.tc,0.48f, (1.f-g_WaveXOffset) + 0.8f) + 
					0.4f*DrawWireNoise(IN.tc,0.52f, (1.f-g_WaveXOffset) + 0.1f);

	ret = (ret*ret.w) + (noise*noise.w);
	
	// highlight background
	float highlightIntensity = pow(1 - abs(IN.tc.y-0.5),4) * g_HighlightScale;
	ret.w += highlightIntensity * 0.25;
	ret.xyz += highlightIntensity * g_TextColour;
	
	// fade out edges
	ret.w *= ComputeEdgeFadeAlpha(0.25f,IN.tc.x,IN.tc.y);
	return ret;
}

float4 PS_DrawBackground(outputVertex IN) : COLOR0
{
	float edgeAlpha = (1.f-ComputeEdgeFadeAlpha(10.0f,IN.tc.x, IN.tc.y)) * g_CurrentValue * 0.4f;
	float4 red = float4(1.f,0.f,0.f,1.f);
	
	float4 col1 = float4(0.15f,0.15f,0.15f,1.f);
	float4 col0 = float4(0.15f,0.15f,0.5f,1.f);
	return (col0 + (col1-col0)*(IN.tc.x+IN.tc.y)*0.5) + (red*edgeAlpha);
}

float4 PS_DrawGroup(outputVertex IN) : COLOR0
{
	float4 ret;
	ret.xyz = g_TextColour;
	ret.w = g_CurrentValue * ComputeEdgeFadeAlpha(2000.f, IN.tc.x, IN.tc.y);
	return ret * g_AlphaFade;
}

float4 PS_DrawModule(outputVertex IN) : COLOR0
{
	float4 ret = ComputeEdgeFadeAndHighlight(1,g_HighlightScale, IN.tc.x, IN.tc.y);
	ret.w *=  g_AlphaFade;
	return ret;
}

float4 PS_DrawMenu(outputVertex IN) : COLOR0
{
	float backgroundAlpha = (0.3f*g_HighlightScale+0.5f)*ComputeEdgeFadeAlpha(0.05f, IN.tc.x, IN.tc.y);
	float4 edgeFade = ComputeEdgeFadeAndHighlight(0.7f, g_HighlightScale, IN.tc.x, IN.tc.y) * g_AlphaFade;
	float4 background = lerp(float4(0.f,0.f,0.f,backgroundAlpha),float4(0.15f,0.0f,0.8f,backgroundAlpha),backgroundAlpha);
	return background*background.w+edgeFade;
}
	
float4 PS_DrawParticle(outputVertex IN) : COLOR0
{
	float4 ret;
	ret.xyz = g_TextColour;
	ret.w = 1.0f;
	return ret;
}

float4 PS_Draw(outputVertex IN) : COLOR0
{
	return float4(1.f,1.f,1.f,1.f);
}

#define ZB_Normal ZFunc = LessEqual; ZEnable = true; ZWriteEnable = true;
#define ZB_NoWrite ZFunc = LessEqual; ZEnable = true; ZWriteEnable = false;
#define ZB_Disabled ZEnable = false; ZWriteEnable = false;

technique drawscrollbar
{
	pass p0 
	{           
		BS_Normal
		ZB_NoWrite
		
        CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawScrollbar();
	}
}

technique drawscrollbarrefl
{
	pass p0 
	{           
		BS_Normal
		ZB_Disabled
        
		CullMode = NONE;
		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawScrollbarReflection();
	}
}

technique drawwave
{
	pass p0 
	{           
		BS_Normal
		ZB_Normal
		        
		AlphaTestEnable = true;
		AlphaRef = 10;
		AlphaFunc = Greater;
		
		CullMode = NONE;
		
		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawWave();
	}
}
technique drawcurve
{
	pass p0 
	{           
		BS_Normal
		ZB_Normal
		
		CullMode = NONE;
		
		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawCurve();
	}
}
technique drawspectrum
{
	pass p0 
	{           
		BS_Normal
		ZB_Disabled
		
		CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawSpectrum();
	}
}
technique drawfr
{
	pass p0 
	{           
		BS_Normal
		ZB_Normal
		
		CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawFreqResp();
	}
}

technique drawblurredscreen
{
	pass p0 
	{           
		BS_Normal
		ZB_Disabled
		
        CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawBlurredScreen();
	}
}

technique drawvirtualisation
{
	pass p0 
	{           
        ZB_Disabled
        CullMode = NONE;

		BS_Normal        

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawVirtualisationDemo();
	}
}

technique drawparticle
{
	pass p0
	{
		BS_Normal
		
        CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_AlignedQuadTransform();
		PixelShader  = compile PIXELSHADER PS_DrawParticle();
	}
}
	
technique drawtexturedquad
{
	pass p0 
	{           
		BS_Normal
		ZB_Disabled
		
        CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawTexturedQuad();
	}
}

technique drawtexturedquad_mirrored
{
	pass p0 
	{           
		BS_Normal
		
        CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawTexturedQuad_Mirrored();
	}
}

technique drawtexturedrefl
{
	pass p0 
	{           
		BS_Normal
		ZB_Disabled
	
		CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawTexturedReflection();
	}
}

technique drawwire
{
	pass p0 
	{           
		BS_Normal
		ZB_Disabled
		
		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawWire();
	}
}

technique drawmodule
{
	pass p0 
	{           
		BS_Normal
		ZB_Normal                                                                    

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawModule();
	}
}

technique drawmenu
{
	pass p0 
	{           
		BS_Normal
		ZB_Normal
		
        CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawMenu();
	}
}

technique drawfontchar
{
	pass p0 
	{           
		BS_Normal
		ZB_Normal
		
        CullMode = NONE;

		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawFontChar();
	}
}

technique drawbg
{
	pass p0 
	{
		CullMode = NONE;
		ZB_Disabled
		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawBackground();
	}
}

technique drawgroup
{
	pass p0 
	{	
		BS_Normal
		ZB_Disabled
		
		CullMode = NONE;
		
		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawGroup();
	}
}

technique drawantialiased
{
	pass p0 
	{           
		BS_Normal
		ZB_NoWrite
		
        CullMode = NONE;
        
		VertexShader = compile VERTEXSHADER VS_Transform();
		PixelShader  = compile PIXELSHADER PS_DrawAntialisedText();
	}
}

#else

// Configure the megashader
#define USE_SPECULAR
#define IGNORE_SPECULAR_MAP
#define USE_DEFAULT_TECHNIQUES

#include "../shaderlib/rage_megashader.fxh"

#endif

