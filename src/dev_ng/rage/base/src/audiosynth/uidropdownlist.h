// 
// audiosynth/uidropdownlist.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UIDROPDOWNLIST_H
#define SYNTH_UIDROPDOWNLIST_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uimenuview.h"
#include "uilabel.h"
#include "uiview.h"

#include "atl/array.h"
#include "vector/vector3.h"

namespace rage
{
	class synthUIDropDownList : public synthUIView
	{

	public:

		synthUIDropDownList();
		virtual ~synthUIDropDownList();

		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		virtual bool StartDrag() const { return false; }

		bool IsActive() const { return m_Label.IsUnderMouse(); }

		u32 GetCurrentIndex() const { return m_CurrentIndex; }
		void SetCurrentIndex(const u32 index) { m_CurrentIndex = index; }

		u32 AddItem(const char *itemString);

		bool IsShowingMenu() const { return m_Menu.ShouldDraw(); }

		void SetMenuOffset(const Vector3 &offset) { m_MenuOffset = offset; }

	protected:
		virtual void Render();

	private:
		atArray<const char *> m_Items;
		Vector3 m_MenuOffset;
		synthUIMenuView m_Menu;
		synthUILabel m_Label;
		u32 m_CurrentIndex;
	};
}
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIDROPDOWNLIST_H
