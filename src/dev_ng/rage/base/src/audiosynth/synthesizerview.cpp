
// 
// audiosynth/synthesizerview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#include "system/param.h"

namespace rage
{
	class synthSynthesizer;
	s32 g_SynthEditorSlotId = -1;

#if !__FINAL
	PARAM(synthname, "Name of synth object to edit");		
	PARAM(muteoutput, "Start with output muted");
#endif
}

#if __SYNTH_EDITOR
#include "moduleview.h"
#include "synthcoredisasm.h"
#include "synthesizerview.h"
#include "uicomponent.h"
#include "uilabel.h"
#include "audioengine/engine.h"
#include "audioengine/engineutil.h"
#include "audioengine/remotecontrol.h"
#include "audiohardware/driver.h"
#include "audiohardware/device.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "grcore/wrapper_d3d.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mouse.h"
#include "system/timemgr.h"

#include "uibutton.h"

namespace rage 
{

	f32 g_MenuItemScaling = 1.f;

	PARAM(readonly, "Don't send changes back to RAVE");
	PARAM(editpreset, "Name of the preset to edit");
	PARAM(applypreset, "Name of preset to apply");

	grcRenderTarget *synthSynthesizerView::m_BlurRT = NULL;
	grcRenderTarget *synthSynthesizerView::m_DepthBuffer = NULL;
	synthUIComponent *synthSynthesizerView::m_ModalComponent = NULL;
	synthSynthesizerView *synthSynthesizerView::sm_Instance = NULL;

	synthSynthesizerView::synthSynthesizerView() : m_CPUTimeLabel(840)
	{
		m_DepthBuffer = NULL;
		m_BlurRT = NULL;
		m_ModalComponent = NULL;
		m_LastSaveTime = 0;
		m_DisableSaving = false;

		Assert(sm_Instance == NULL);
		sm_Instance = this;
	}

	synthSynthesizerView::~synthSynthesizerView()
	{
		Assert(sm_Instance == this);
		sm_Instance = NULL;
	}

	void synthSynthesizerView::InitClass()
	{
		synthUIComponent::InitClass();
		synthModuleView::InitClass();
	}

	void synthSynthesizerView::ShutdownClass()
	{
		synthModuleView::ShutdownClass();
		synthUIComponent::ShutdownClass();
	}

	bool synthSynthesizerView::Init(const char *synthName)
	{
		m_WantsToChangeAsset = false;

		m_CameraInterSpeed = 0.5f;

		for(s32 i = 0; i < m_CameraPositions.size(); i++)
		{
			m_CameraPositions[i] = Vector3(0.f,0.f,-25.f);
		}

		m_SynthSlotId = -1;
		

		synthUIObjects uiObjects;
		uiObjects.rootMatrix = GetModulePlane().GetMatrix();
		
		m_SynthSlotId = synthSynthesizer::Create(synthName, &uiObjects);		
		if(m_SynthSlotId == -1)
		{
			m_SynthSlotId = synthSynthesizer::Create((const ModularSynth*)NULL, (const Preset*)NULL, &uiObjects);
			audErrorf("Failed to load synth %s (RAVE metadata compiler error?)", synthName);
			m_DisableSaving = true;
		}

		const char *presetName = "nopreset";
		if(PARAM_applypreset.Get(presetName))
		{
			audDisplayf("Applying preset %s", presetName);
			GetSynth()->ApplyPreset(synthSynthesizer::FindPreset(presetName));
		}
		for(s32 i = 0; i < uiObjects.cameraPositions.GetCount(); i++)
		{
			SetCameraPosition(i, uiObjects.cameraPositions[i]);
		}

		for(s32 i = 0; i < uiObjects.groups.GetCount(); i++)
		{
			GetModulePlane().AddGroup(uiObjects.groups[i]);
		}

		for(s32 i = 0; i < uiObjects.commentBoxes.GetCount(); i++)
		{
			GetModulePlane().AddCommentBox(uiObjects.commentBoxes[i]);
		}

		if(PARAM_readonly.Get())
		{
			m_DisableSaving = true;
		}
		Assert(m_SynthSlotId != -1);
		GetSynth()->SetName(synthName);

		if(PARAM_muteoutput.Get())
		{
			GetSynth()->ToggleMute();
		}

		g_SynthEditorSlotId = m_SynthSlotId;
		
		Matrix34 mtx;
		mtx.Identity();
	
		ioMouse::EnableGlobalButtons(true);

		// create render target for blur
		const u32 rtWidth = GRCDEVICE.GetWidth();//>>2;
		const u32 rtHeight = GRCDEVICE.GetHeight();//>>2;

		m_BlurRT = grcTextureFactory::GetInstance().CreateRenderTarget("__synthview_rt",
			grcrtPermanent, rtWidth, rtHeight, 32);
		m_DepthBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("__synthview_depth",
			grcrtDepthBuffer, rtWidth, rtHeight, 16);

		// ensure the render targets start clear
		D3D11_ONLY(GRCDEVICE.LockContext());
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_BlurRT, m_DepthBuffer);
		GRCDEVICE.Clear(true, Color32(0,0,0),true, 1.f,false,0);
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);
		D3D11_ONLY(GRCDEVICE.UnlockContext());

		m_UIPlane.Init(&GetSynth()->GetModules(), GetSynth());
		m_UIPlane.SetMatrix(mtx);
		m_UIPlane.SetActive(true);

		m_FreeCamMtx = m_LastCamMtx = m_UIPlane.ComputeCameraMatrix();

		m_FreeCamMtx.d = m_LastCamMtx.d = m_CameraPositions[0];

		synthUIPlane::SetMaster(&m_UIPlane);

		m_CurrentCPUSample = 0;
		for(s32 i = 0; i < kNumCPUSamples; i++)
		{
			m_CPUTimes[i] = 0.f;
		}
		
		return true;
	}

	void synthSynthesizerView::LockRenderTarget()
	{
		if(m_ModalComponent)
		{
			grcTextureFactory::GetInstance().LockRenderTarget(0, m_BlurRT, m_DepthBuffer);
		}
	}
	void synthSynthesizerView::UnlockRenderTarget()
	{
		if(m_ModalComponent)
		{
			grcTextureFactory::GetInstance().UnlockRenderTarget(0);
		}
	}

	void synthSynthesizerView::Update()
	{
		D3D11_ONLY(GRCDEVICE.LockContext());

		// dont synthesize while module UI is updated since pins could be connected/disconnected etc
		GetSynth()->Lock();
		m_UIPlane.Update();
		GetSynth()->Unlock();		
		
		if(m_ModalComponent == NULL && !synthUIPlane::GetMaster()->IsKeyboardLocked())
		{
			if(ioKeyboard::KeyPressed(KEY_C))
			{
				m_FreeCamMtx = m_UIPlane.ComputeCameraMatrix();
			}
			else if(ioKeyboard::KeyPressed(KEY_O))
			{
				Matrix34 outputMatrix = GetSynth()->GetModules()[0]->GetView()->GetMatrix();
				const f32 distance = 25.f;
				Vector3 camPos = outputMatrix.d - outputMatrix.c * distance; 
				m_FreeCamMtx.LookAt(camPos, outputMatrix.d, outputMatrix.b);				
			}
			else if(ioKeyboard::KeyPressed(KEY_M))
			{
				GetSynth()->ToggleMute();
			}
			else if(ioKeyboard::KeyPressed(KEY_F))
			{
				GetSynth()->ToggleFrozen();
			}

			if(ioKeyboard::KeyDown(KEY_CONTROL))
			{
				// Save camera snapshot
				if(ioKeyboard::KeyPressed(KEY_1))
				{
					m_CameraPositions[1] = m_FreeCamMtx.d;
				}
				else if(ioKeyboard::KeyPressed(KEY_2))
				{
					m_CameraPositions[2] = m_FreeCamMtx.d;
				}
				else if(ioKeyboard::KeyPressed(KEY_3))
				{
					m_CameraPositions[3] = m_FreeCamMtx.d;
				}
				else if(ioKeyboard::KeyPressed(KEY_4))
				{
					m_CameraPositions[4] = m_FreeCamMtx.d;
				}
				else if(ioKeyboard::KeyPressed(KEY_5))
				{
					m_CameraPositions[5] = m_FreeCamMtx.d;
				}
				else if(ioKeyboard::KeyPressed(KEY_6))
				{
					m_CameraPositions[6] = m_FreeCamMtx.d;
				}
				else if(ioKeyboard::KeyPressed(KEY_7))
				{
					m_CameraPositions[7] = m_FreeCamMtx.d;
				}
				else if(ioKeyboard::KeyPressed(KEY_8))
				{
					m_CameraPositions[8] = m_FreeCamMtx.d;
				}
				else if(ioKeyboard::KeyPressed(KEY_9))
				{
					m_CameraPositions[9] = m_FreeCamMtx.d;
				}
				else if(ioKeyboard::KeyPressed(KEY_0))
				{
					m_CameraPositions[10] = m_FreeCamMtx.d;
				}
			}
			else
			{
				// Go to snapshot
				if(ioKeyboard::KeyPressed(KEY_1))
				{
					m_FreeCamMtx.d = m_CameraPositions[1];
				}
				else if(ioKeyboard::KeyPressed(KEY_2))
				{
					m_FreeCamMtx.d = m_CameraPositions[2];
				}
				else if(ioKeyboard::KeyPressed(KEY_3))
				{
					m_FreeCamMtx.d = m_CameraPositions[3];
				}
				else if(ioKeyboard::KeyPressed(KEY_4))
				{
					m_FreeCamMtx.d = m_CameraPositions[4];
				}
				else if(ioKeyboard::KeyPressed(KEY_5))
				{
					m_FreeCamMtx.d = m_CameraPositions[5];
				}
				else if(ioKeyboard::KeyPressed(KEY_6))
				{
					m_FreeCamMtx.d = m_CameraPositions[6];
				}
				else if(ioKeyboard::KeyPressed(KEY_7))
				{
					m_FreeCamMtx.d = m_CameraPositions[7];
				}
				else if(ioKeyboard::KeyPressed(KEY_8))
				{
					m_FreeCamMtx.d = m_CameraPositions[8];
				}
				else if(ioKeyboard::KeyPressed(KEY_9))
				{
					m_FreeCamMtx.d = m_CameraPositions[9];
				}
				else if(ioKeyboard::KeyPressed(KEY_0))
				{
					m_FreeCamMtx.d = m_CameraPositions[10];
				}
			}
		}

		
		synthUIPlane::GetMaster()->SetDisableInteraction(m_ModalComponent != NULL);
		

#if __WIN32PC
		const bool isWindowFocused = ::GetFocus() == g_hwndMain;
#else
		const bool isWindowFocused = true;
#endif
	
		if(isWindowFocused)
		{
			if(ioMouse::GetButtons() & ioMouse::MOUSE_MIDDLE)
			{
				// translate
				static f32 mouseHorizSpeed = 0.06f;
				static f32 mouseVertSpeed = 0.06f;

				Vector3 d(0.f,0.f,-1.f);
				const f32 distanceFromPlane = d.Dot(m_LastCamMtx.d - synthUIPlane::GetMaster()->GetMousePosition());
				f32 zScaler = Lerp(Clamp(distanceFromPlane-5.f / 25.f,0.f,1.f), 1.f, 8.f);
				m_FreeCamMtx.d -= zScaler * mouseHorizSpeed * ioMouse::GetDX() * m_FreeCamMtx.a;
				m_FreeCamMtx.d -= zScaler * mouseVertSpeed * ioMouse::GetDY() * -m_FreeCamMtx.b;
			}
			
			// zoom and pan with scroll wheel
			static Vector3 mouseZoomSpeed(0.25f, 0.25f, 0.25f);
			const Vector3 deltaPos = m_UIPlane.GetMousePosition() - m_FreeCamMtx.d;
			//Vector3 dirVec = m_UIPlane.GetMousePosition() - m_LastCamMtx.d;
			//const f32 dist2 = dirVec.Mag2();
			//dirVec.Normalize();
			const Vector3 scaledDelta = mouseZoomSpeed * (f32)ioMouse::GetDZ() * deltaPos;
			// TODO: fix this so that it works in other planes
			// zooming out doesn't translate

			static f32 xLimit = 1000.f;
			static f32 yLimit = 1000.f;
			static f32 zLimit = 20.f;

			// disable zooming at edge of screen
#if __WIN32PC

#endif

			f32 zoomEnabled = 1.f;
			const bool zoomingOut = (ioMouse::GetDZ() < 0);
			Vector3 correction(
				Clamp(zoomingOut ? 0.f : scaledDelta.x, -xLimit, xLimit), 
				Clamp(zoomingOut ? 0.f : scaledDelta.y, -yLimit, yLimit),
				Clamp(zoomEnabled * scaledDelta.z, -zLimit, zLimit));
			
			/*static f32 movementLimit = 1.f;
			correction.x = Clamp(correction.x, -movementLimit, movementLimit);
			correction.y = Clamp(correction.y, -movementLimit, movementLimit);*/
			//static f32 movementLimit = 3.f;
			//correction.z = Clamp(correction.z, movementLimit*-2.f, movementLimit);
			m_FreeCamMtx.d += correction;
			m_FreeCamMtx.d.z = Min(-0.5f, m_FreeCamMtx.d.z);

#if __WIN32PC
			// Update the 2d mouse position so that the 3d position remains constant
			if(correction.Mag2() != 0.f && grcViewport::GetCurrent())
			{
				//grcViewport::GetCurrent()->SetCameraMtx(m_FreeCamMtx);
				
				grcViewport::GetCurrent()->SetCameraMtx(RCC_MAT34V(m_LastCamMtx));
				
				const Vector3 mousePos = synthUIPlane::GetMaster()->GetMousePosition();
				f32 wx,wy;
				grcViewport::GetCurrent()->Transform((Vec3V&)mousePos, wx, wy);
				

				//const f32 windowWidth = (f32)grcViewport::GetCurrent()->GetWidth();
				//const f32 windowHeight = (f32)grcViewport::GetCurrent()->GetHeight();
				//const f32 screenWidth = (f32)grcViewport::GetDefaultScreen()->GetWidth();
				//const f32 screenHeight = (f32)grcViewport::GetDefaultScreen()->GetHeight();

				//wx *= screenWidth/windowWidth;
				//wy *= screenHeight/windowHeight;

				// we have the position relative to the client area but we need to work it out relative 
				// to the window area

				RECT rect;
				::GetWindowRect(g_hwndMain, &rect);
				const s32 cyframe = ::GetSystemMetrics(SM_CYFRAME);
				const s32 titleBarSize = ::GetSystemMetrics(SM_CYCAPTION) + cyframe;
				const s32 frameWidth = ::GetSystemMetrics(SM_CXFRAME);
				const s32 windowXOffset = rect.left + frameWidth;
				const s32 windowYOffset = rect.top + titleBarSize;

				
//				audDisplayf("SM_CXFRAME: %d SM_CYFRAME: %d SM_CYCAPTION: %d SM_CXBORDER: %d SM_CYBORDER: %d SM_CXEDGE: %d SM_CYEDGE: %d", 
//					::GetSystemMetrics(SM_CXFRAME), ::GetSystemMetrics(SM_CYFRAME), ::GetSystemMetrics(SM_CYCAPTION), 
//					::GetSystemMetrics(SM_CXBORDER), 
//					::GetSystemMetrics(SM_CYBORDER), ::GetSystemMetrics(SM_CXEDGE), ::GetSystemMetrics(SM_CYEDGE));
			

				if(isWindowFocused)
				{
					::SetCursorPos(windowXOffset + (s32)wx, windowYOffset + (s32)wy);
				}

			}

			// set the window title
			const u32 timeSinceSaving = (g_AudioEngine.GetTimeInMilliseconds()-g_AudioEngine.GetRemoteControl().GetLastMessageReceiptTime());
			char titleBarString[128];
			formatf(titleBarString, "Rockstar Games Presents AMP - %s%s", GetSynth()->GetName(), timeSinceSaving>2000 ? " [UNSAVED]":"");
			::SetWindowText(g_hwndMain, titleBarString);
#endif
		}


		const CompiledSynth *compiledSynth = synthSynthesizer::FindOptimizedMetadata(atStringHash(GetSynth()->GetName()));

		m_CPUTimes[m_CurrentCPUSample] = GetSynth()->GetCPUTime();
		m_CurrentCPUSample = (m_CurrentCPUSample+1)%kNumCPUSamples;
		f32 summedTime = 0.f;
		for(s32 i = 0; i < kNumCPUSamples; i++)
		{	
			summedTime += m_CPUTimes[i];
		}
		const f32 meanTime = summedTime / (f32)kNumCPUSamples;
		const f32 realTimeRatio = meanTime / ((kMixBufNumSamples / (f32)kMixerNativeSampleRate) * 10.f);

		if(compiledSynth)
		{			
			synthCoreDisasm disasm;
			if(disasm.Disassemble(compiledSynth))
			{
				static bool hasPrinted = false;
				
				if(ioKeyboard::KeyPressed(KEY_D) || !hasPrinted)
				{
					disasm.DebugPrint();
					hasPrinted = true;
				}
				float ps3Time = 0.f;
				float xbox360Time = 0.f;
				disasm.ComputeRuntimeEstimate(ps3Time, xbox360Time);
				formatf(m_CPUTimeBuf,"%u modules: PS3: %.2fuS Xbox360: %.2fuS B:%u R:%u C:%u S:%u", GetSynth()->GetModules().GetCount(), 
								ps3Time, xbox360Time,
								disasm.GetNumBuffers(), 
								disasm.GetNumRegisters(), 
								disasm.GetNumConstants(), 
								disasm.GetNumStateBlocks());
			}
			else
			{
	
				formatf(m_CPUTimeBuf, "Compiled synth disassembly failed");
			}

		}
		else
		{
			formatf(m_CPUTimeBuf,"%u modules: CPU: %.1f%% (not exported)", GetSynth()->GetModules().GetCount(), realTimeRatio);
		}

		//formatf(m_CPUTimeBuf, "m %u c: %u r: %u t: %u", GetSynth()->GetModules().GetCount(), synthUIComponent::sm_NumTested,synthUIComponent::sm_NumRendered,	synthUILabel::GetNumTexturesAllocated());

		m_CPUTimeLabel.SetString(m_CPUTimeBuf);
		m_CPUTimeLabel.SetSize(0.15f);
		m_CPUTimeLabel.SetShouldAlwaysDraw(true);
	
		Matrix34 mtx(M34_IDENTITY);
		
		mtx.Scale(0.3f);
		//mtx.Translate(-0.78f, -0.96f, 0.f);
		mtx.Translate(0.7f, -0.96f, 0.f);
		m_CPUTimeLabel.SetMatrix(mtx);
		m_CPUTimeLabel.Update();

		mtx.Identity();
		mtx.Scale(0.5f);
		mtx.Translate(0.7f, 0.9f, 0.f);
		m_MuteLabel.SetString(GetSynth()->IsFrozen() ? "Frozen" : "Output Muted");
		m_MuteLabel.SetShouldAlwaysDraw(true);
		m_MuteLabel.SetShouldDraw(GetSynth()->IsMuted() || GetSynth()->IsFrozen());
		m_MuteLabel.SetSize(0.12f);
		m_MuteLabel.SetMatrix(mtx);
		m_MuteLabel.Update();

		if(g_AudioEngine.GetRemoteControl().IsPresent())
		{
			// Don't save if AMP doesn't have focus
			if(!m_DisableSaving && grcDevice::GetHasFocus())
			{
				// safety: don't save if there is only one module, in case something has gone wrong and we failed to load the existing
				// patch.
				if(GetSynth()->GetModules().GetCount() > 1 && m_LastSaveTime + 1000 < audEngineUtil::GetCurrentTimeInMilliseconds())
				{
					// send synth to RAVE
					SaveCopyAs(GetSynth()->GetName());
					m_LastSaveTime = audEngineUtil::GetCurrentTimeInMilliseconds();
				}
			}

			const char *editPresetName = "DEFAULT_PRESET";
			if(PARAM_editpreset.Get(editPresetName) && ioKeyboard::KeyPressed(KEY_P))
			{
				SavePreset(editPresetName);
			}
			
			if(PARAM_applypreset.Get(editPresetName) && ioKeyboard::KeyPressed(KEY_A))
			{
				audDisplayf("Applying preset %s", editPresetName);
				GetSynth()->ApplyPreset(synthSynthesizer::FindPreset(editPresetName));
			}
		}

		// store the current position in the magic slot
		SetCameraPosition(0, m_LastCamMtx.d);

		D3D11_ONLY(GRCDEVICE.UnlockContext());
	}

	bool synthSynthesizerView::SavePreset(const char *presetName)
	{
		audDisplayf("Saving current preset to %s", presetName);
		// send preset to RAVE
		char fileName[64];
		sysMemZeroBytes<sizeof(m_SaveBuffer)>(m_SaveBuffer);
		fiDeviceMemory::MakeMemoryFileName(fileName, sizeof(fileName), m_SaveBuffer, sizeof(m_SaveBuffer), false, "SaveFile");
		fiStream *stream = ASSET.Create(fileName, NULL);
		const char *headerText = "<RAVEMessage><EditObjects metadataType=\"ModularSynth\" chunkNameHash=\"1155669136\">\r\n";
		stream->Write(headerText, istrlen(headerText));
		GetSynth()->SerializeCurrentPreset(stream, presetName);
		const char *footerText = "</EditObjects></RAVEMessage>";
		stream->Write(footerText, istrlen(footerText));
		u32 length = stream->Tell();
		stream->Close();
		return g_AudioEngine.GetRemoteControl().SendXmlMessage(m_SaveBuffer,length);
	}

	bool synthSynthesizerView::SaveCopyAs(const char *synthName)
	{
		if(g_AudioEngine.GetRemoteControl().IsPresent())
		{	
			// send synth to RAVE
			char fileName[64];
			sysMemZeroBytes<sizeof(m_SaveBuffer)>(m_SaveBuffer);
			fiDeviceMemory::MakeMemoryFileName(fileName, sizeof(fileName), m_SaveBuffer, sizeof(m_SaveBuffer), false, "SaveFile");
			fiStream *stream = ASSET.Create(fileName, NULL);

			const char *headerText = "<RAVEMessage><EditObjects metadataType=\"ModularSynth\" chunkNameHash=\"1155669136\">\r\n";
			stream->Write(headerText, istrlen(headerText));
			GetSynth()->Serialize(stream, synthName);
			const char *footerText = "</EditObjects></RAVEMessage>";
			stream->Write(footerText, istrlen(footerText));
			u32 length = stream->Tell();
			stream->Close();
			return g_AudioEngine.GetRemoteControl().SendXmlMessage(m_SaveBuffer,length);
		}
		return false;
	}

	bool synthSynthesizerView::CreateNewAsset(const char *synthName)
	{
		if(g_AudioEngine.GetRemoteControl().IsPresent())
		{	
			// send synth to RAVE
			char fileName[64];
			sysMemZeroBytes<sizeof(m_SaveBuffer)>(m_SaveBuffer);
			fiDeviceMemory::MakeMemoryFileName(fileName, sizeof(fileName), m_SaveBuffer, sizeof(m_SaveBuffer), false, "SaveFile");
			fiStream *stream = ASSET.Create(fileName, NULL);

			const char *headerText = "<RAVEMessage><EditObjects metadataType=\"ModularSynth\" chunkNameHash=\"1155669136\">\r\n";
			stream->Write(headerText, istrlen(headerText));
			
			char xmlBuf[128];
			formatf(xmlBuf, "<ModularSynth name=\"%s\"/>", synthName);
			stream->Write(xmlBuf, istrlen(xmlBuf));

			const char *footerText = "</EditObjects></RAVEMessage>";
			stream->Write(footerText, istrlen(footerText));
			u32 length = stream->Tell();
			stream->Close();
			return g_AudioEngine.GetRemoteControl().SendXmlMessage(m_SaveBuffer,length);
		}
		return false;
	}

	void synthSynthesizerView::Draw()
	{
		// draw background
		Matrix34 orthoCamMtx(M34_IDENTITY);
		orthoCamMtx.d.y = 0.f;
		orthoCamMtx.d.x = 0.f;
		orthoCamMtx.d.z = 1.f;

		grcViewport *viewport = grcViewport::GetCurrent();
		if(viewport)
		{
			if(m_ModalComponent)
			{
				// draw into RT
				LockRenderTarget();
				GRCDEVICE.Clear(true, Color32(0,0,0,1),true, 1.f,false,0);
			}

			Mat44V oldProj(viewport->GetProjection());
			viewport->SetCameraMtx(RCC_MAT34V(orthoCamMtx));

			const f32 oldNearClip = grcViewport::GetCurrent()->GetNearClip();
			const f32 oldFarClip = grcViewport::GetCurrent()->GetFarClip();
			viewport->Ortho(1.f,-1.f,-1.f, 1.f,0.f,1.f);
			grcWorldIdentity();

			synthUIComponent::SetShaderVar("g_WaveXOffset", TIME.GetElapsedTime());

			static const audThreePointPiecewiseLinearCurve lastMessageTimeToEdgeColour(0.f,0.f,1.f,0.0f,2.f,1.f);
			// screen edges go red if this is read only, but only if the synth has more than 1 module (otherwise we don't try to save it)
			const f32 timeSinceLastSaveSeconds = (GetSynth()->GetModules().GetCount() <= 1 ? 0.f : 1.f)
				* (0.001f * (g_AudioEngine.GetTimeInMilliseconds()-g_AudioEngine.GetRemoteControl().GetLastMessageReceiptTime()));
			synthUIComponent::SetShaderVar("g_CurrentValue", lastMessageTimeToEdgeColour.CalculateValue(timeSinceLastSaveSeconds));

			synthUIComponent::DrawVB("drawbg");

			m_CPUTimeLabel.Draw();
			m_MuteLabel.Draw();

			// back to 3d viewport
			viewport->SetProjection(oldProj);
			viewport->SetNearFarClip(oldNearClip, oldFarClip);

			Matrix34 camMtx(M34_IDENTITY);
			if(!m_DisableCamera)
			{				
			
				// interp speed based on delta: the greater the distance between requested position
				// and smoothed position, the slower the interp (within range)
				static f32 maxInterpSpeed = 0.1f;
				static f32 minInterpSpeed = 0.06f;
				static f32 maxDistanceForInterp = 10.f;
				m_CameraInterSpeed = Lerp(Clamp((m_FreeCamMtx.d - m_LastCamMtx.d).Mag2() / (maxDistanceForInterp*maxDistanceForInterp), 0.f,1.f) , maxInterpSpeed, minInterpSpeed);

				Matrix34 newCamMtx = m_FreeCamMtx;
				// smoothly interp toward the new camera
				camMtx.Interpolate(m_LastCamMtx, newCamMtx, m_CameraInterSpeed);
				m_LastCamMtx = camMtx;
			
				viewport->SetCameraMtx(RCC_MAT34V(camMtx));

				// cheap hack: scale menu size based on  cam distance from origin along z axis
				const f32 zDist = -camMtx.d.z;
				const f32 normalizedZScale = Max((zDist - 28.f) / 30.f, 1.f);
				g_MenuItemScaling = normalizedZScale*1.6f;
			}
			
			m_UIPlane.Draw();						

			if(m_ModalComponent)
			{
				UnlockRenderTarget();

				// need to blit our blurred RT to screen
				GRCDEVICE.Clear(true, Color32(0,0,0,1),true, 1.f,false,0);
				viewport->SetCameraMtx(RCC_MAT34V(orthoCamMtx));
				viewport->Ortho(1.f,-1.f,-1.f, 1.f,0.f,1.f);
				grcWorldIdentity();

				synthUIComponent::SetShaderVar("FontTex",m_BlurRT);
				synthUIComponent::DrawVB("drawblurredscreen");

				// finally draw 2d modal component on top of the blurred screen
				m_ModalComponent->Draw();
			}
		}
	}

	void synthSynthesizerView::Shutdown()
	{
		m_UIPlane.Shutdown();

		m_BlurRT->Release();
		m_BlurRT = NULL;

		m_DepthBuffer->Release();
		m_DepthBuffer = NULL;
	}

	synthSynthesizer *synthSynthesizerView::GetSynth()
	{
		return static_cast<synthSynthesizer*>(audDriver::GetMixer()->GetPcmSource(m_SynthSlotId));
	}


	bool synthSynthesizerView::SetModalComponent(synthUIComponent *component)
	{
		Assert(component);
		synthUIPlane::GetMaster()->SetIsMouseFixed(true);
		if(!m_ModalComponent)
		{
			m_ModalComponent = component;
			return true;
		}
		return false;
	}

	void synthSynthesizerView::ClearModalComponent()
	{
		synthUIPlane::GetMaster()->SetIsMouseFixed(false);
		m_ModalComponent = NULL;
	}

} // namespace rage

#endif // __SYNTH_EDITOR


