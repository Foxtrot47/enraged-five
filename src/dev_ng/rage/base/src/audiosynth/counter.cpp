
#include "counter.h"

#if !SYNTH_MINIMAL_MODULES

namespace rage
{
	using namespace Vec;
	synthCounter::synthCounter()
	{
		m_Outputs[kCounterOutputValue].Init(this, "Value",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[kCounterOutputReset].Init(this, "Reset",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);

		m_Inputs[kCounterReset].Init(this, "Reset", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kCounterReset].SetStaticValue(0.f);
		m_Inputs[kCounterIncrement].Init(this, "Up", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kCounterIncrement].SetStaticValue(0.f);
		m_Inputs[kCounterDecrement].Init(this, "Down", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kCounterDecrement].SetStaticValue(0.f);
		m_Inputs[kCounterAutoReset].Init(this, "AutoReset", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kCounterAutoReset].SetStaticValue(0.f);

		m_Outputs[kCounterOutputValue].SetStaticValue(0.f);
		m_Outputs[kCounterOutputReset].SetStaticValue(0.f);

		ResetProcessing();
	}

	void synthCounter::ResetProcessing()
	{
		m_State1 = V4VConstant(V_ZERO);
		m_State2 = V4VConstant(V_ZERO);
	}

	void synthCounter::Synthesize()
	{
		const float reset = m_Inputs[kCounterReset].GetNormalizedValue(0);
		const float increment = m_Inputs[kCounterIncrement].GetNormalizedValue(0);
		const float decrement = m_Inputs[kCounterDecrement].GetNormalizedValue(0);
		const float autoResetThreshold = m_Inputs[kCounterAutoReset].GetNormalizedValue(0);

		float counterValue = synthCounter::Process_Counter(
			reset,
			increment,
			decrement,
			autoResetThreshold,
			m_State1
			);
		m_Outputs[kCounterOutputValue].SetStaticValue(counterValue);

		float triggerValueValue = synthCounter::Process_Trigger(
			reset,
			increment,
			decrement,
			autoResetThreshold,
			m_State2
			);
		m_Outputs[kCounterOutputReset].SetStaticValue(triggerValueValue);
	}

	float synthCounter::Process_Counter(const float reset, const float increment, const float decrement, const float autoResetThreshold, Vec::Vector_4V_InOut state)
	{
		float currentValue = GetX(state);
		const bool shouldAutoReset =  autoResetThreshold > 0.f && currentValue >= autoResetThreshold;

		if(reset >= 1.f || shouldAutoReset)
		{
			// Reset
			currentValue = 0.f;
		}
		else
		{
			if(increment >= 1.f)
			{
				currentValue += 1.f;
			}
			if(decrement >= 1.f)
			{
				currentValue -= 1.f;
			}
		}

		SetX(state, currentValue);
		return currentValue;
	}

	float synthCounter::Process_Trigger(const float reset, const float increment, const float decrement, const float autoResetThreshold, Vec::Vector_4V_InOut state)
	{
		float currentValue = GetX(state);
		float output = 0.f;
		const bool shouldAutoReset =  autoResetThreshold > 0.f && currentValue >= autoResetThreshold;

		if(reset >= 1.f || shouldAutoReset)
		{
			// Reset
			currentValue = 0.f;

			output = 1.f;
		}
		else
		{
			if(increment >= 1.f)
			{
				currentValue += 1.f;
			}
			if(decrement >= 1.f)
			{
				currentValue -= 1.f;
			}
		}

		SetX(state, currentValue);
		return output;
	}
}
#endif

