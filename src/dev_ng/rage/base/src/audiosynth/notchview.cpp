// 
// audiosynth/notchview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "notchview.h"
#include "notchfilter.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"
#include "uiplane.h"

namespace rage
{

	synthNotchView::synthNotchView(synthModule *module) : synthModuleView(module)
	{
		AddComponent(&m_FreqRange);

		const f32 maxUIVal = 20000.f;

		m_FreqRange.SetMinRange(0.f, maxUIVal);
		m_FreqRange.SetMaxRange(0.f, maxUIVal);

		m_FreqRange.SetNormalizedValues(GetFilterModule()->GetMinFrequency()/maxUIVal, GetFilterModule()->GetMaxFrequency()/maxUIVal);
	}

	void synthNotchView::Update()
	{
		// fade out title when editing
		if(IsMinimised())
		{
			SetTitleAlpha(1.f);
		}
		else
		{
			SetTitleAlpha(1.f - GetHoverFade());
		}
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();

		// Mode label
		const f32 moduleHalfHeight = 0.5f * ComputeHeight();

		mtx.d += (mtx.b * 1.f);
	
		mtx = GetMatrix();
		const Vector3 yOffset = mtx.b * moduleHalfHeight * 0.25f;
		mtx.d -= yOffset;
		m_FreqRange.SetMatrix(mtx);
		m_FreqRange.SetAlpha(GetHoverFade());
		m_FreqRange.SetTextAlpha(GetHoverFade());

		m_FreqRange.SetTitles("MinCutoff", "MaxCutoff");
		m_FreqRange.SetWidth(0.4f);
		m_FreqRange.SetShouldDraw(!IsMinimised());

		GetFilterModule()->SetMaxFrequency(m_FreqRange.GetMaxValue());
		GetFilterModule()->SetMinFrequency(m_FreqRange.GetMinValue());
		GetFilterModule()->GetInput(kNotchCutoff).GetView()->SetStaticViewRange(
			GetFilterModule()->GetMinFrequency(), GetFilterModule()->GetMaxFrequency());

		synthModuleView::Update();
	}

	void synthNotchView::Render()
	{
		synthModuleView::Render();
	}

	bool synthNotchView::StartDrag() const
	{
		if(m_FreqRange.IsUnderMouse())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthNotchView::IsActive() const
	{
		return (IsUnderMouse() || m_FreqRange.IsActive());
	}
}
#endif // __SYNTH_EDITOR

