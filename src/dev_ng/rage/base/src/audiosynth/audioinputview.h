// 
// audiosynth/audioinputview.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_AUDIOINPUTVIEW_H
#define SYNTH_AUDIOINPUTVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uihorizontallayout.h"
#include "uilabel.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthAudioInput;
	class synthAudioInputView : public synthModuleView
	{
	public:

		synthAudioInputView(synthModule *module);

		virtual void Update();
		virtual void Render();

		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;
		virtual bool IsActive() const;
	protected:

		virtual bool StartDrag() const;

	private:

		synthAudioInput *GetAudioInput() const { return (synthAudioInput*)m_Module; }

		synthUIHorizontalLayout m_Layout;
		synthUIDropDownList m_InputList;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_AUDIOINPUTVIEW_H

