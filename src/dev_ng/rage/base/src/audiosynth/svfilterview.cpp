// 
// audiosynth/pwnoiseview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "svfilterview.h"
#include "statevariable.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"

#include "input/mouse.h"

namespace rage
{

	synthStateVariableFilterView::synthStateVariableFilterView(synthModule *module) : synthModuleView(module)
	{
		AddComponent(&m_FreqRange);
		AddComponent(&m_QRange);

		const f32 minQUIValue = 0.5f;
		const f32 maxQUIValue = 10.f;
		m_QRange.SetTitles("MinQ", "MaxQ");
		m_QRange.SetMinRange(minQUIValue, maxQUIValue);
		m_QRange.SetMaxRange(minQUIValue, maxQUIValue);
		
		const f32 currentMinQ = GetFilterModule()->GetMinQ();
		const f32 currentMaxQ = GetFilterModule()->GetMaxQ();
		const f32 currentNormalizedMinQ = (currentMinQ-minQUIValue) / (maxQUIValue-minQUIValue);
		const f32 currentNormalizedMaxQ = (currentMaxQ-minQUIValue) / (maxQUIValue-minQUIValue);
		m_QRange.SetNormalizedValues(currentNormalizedMinQ,currentNormalizedMaxQ);

		const f32 maxUIFreqValue = 6500.f;
		m_FreqRange.SetTitles("MinFreq", "MaxFreq");
		m_FreqRange.SetMinRange(0.f, maxUIFreqValue);
		m_FreqRange.SetMaxRange(0.f, maxUIFreqValue);
		m_FreqRange.SetNormalizedValues(GetFilterModule()->GetMinFrequency()/maxUIFreqValue, GetFilterModule()->GetMaxFrequency()/maxUIFreqValue);
	}

	void synthStateVariableFilterView::Update()
	{
		// fade out title when editing
		SetTitleAlpha(1.f - GetHoverFade());
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();
		const f32 moduleHalfHeight = ComputeWidth()*0.5f;
		const Vector3 yOffset = mtx.b * moduleHalfHeight * 0.25f;
		mtx.d += yOffset;
		m_FreqRange.SetMatrix(mtx);
		m_FreqRange.SetAlpha(GetHoverFade());
		m_FreqRange.SetTextAlpha(GetHoverFade());
		m_FreqRange.SetWidth(0.4f);

		mtx.d -= m_FreqRange.ComputeHeight() * mtx.b * 1.5f;
		m_QRange.SetMatrix(mtx);
		m_QRange.SetAlpha(GetHoverFade());
		m_QRange.SetTextAlpha(GetHoverFade());
		m_QRange.SetWidth(0.4f);

		GetFilterModule()->SetMaxFrequency(m_FreqRange.GetMaxValue());
		GetFilterModule()->SetMinFrequency(m_FreqRange.GetMinValue());
		GetFilterModule()->GetInput(kStateVariableFilterFc).GetView()->SetStaticViewRange(
			GetFilterModule()->GetMinFrequency(), GetFilterModule()->GetMaxFrequency());

		GetFilterModule()->SetMaxQ(m_QRange.GetMaxValue());
		GetFilterModule()->SetMinQ(m_QRange.GetMinValue());
		GetFilterModule()->GetInput(kStateVariableFilterQ).GetView()->SetStaticViewRange(
			GetFilterModule()->GetMinQ(), GetFilterModule()->GetMaxQ());

		synthModuleView::Update();
	}

	void synthStateVariableFilterView::Render()
	{
		synthModuleView::Render();
	}

	bool synthStateVariableFilterView::StartDrag() const
	{
		if(m_FreqRange.IsUnderMouse()||m_QRange.IsUnderMouse())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthStateVariableFilterView::IsActive() const
	{
		return (IsUnderMouse() || m_FreqRange.IsActive() || m_QRange.IsActive());
	}
}
#endif // __SYNTH_EDITOR

