#include "audioinput.h"
#include "audiooutput.h"
#include "inputsmoother.h"
#include "synthesizer.h"
#include "synthesizerview.h"
#include "pin.h"
#include "pinview.h"
#include "module.h"
#include "moduleview.h"
#include "modulegroup.h"
#include "moduleserializer.h"
#include "synthcore.h"
#include "synthcoredisasm.h"

#include "synthobjects.h"
#include "audiohardware/device.h"
#include "audiohardware/device_asio.h"
#include "audiohardware/driver.h"
#include "audiohardware/mixer.h"
#include "audiohardware/pcmsource_interface.h"
#include "audiohardware/framebufferpool.h"
#include "audioengine/engine.h"
#include "audioengine/metadatamanager.h"
#include "audioengine/remotecontrol.h"
#include "audioengine/widgets.h"
#include "audiosoundtypes/sound.h"

#include "math/amath.h"
#include "system/memops.h"
#include "system/param.h"
#include "system/performancetimer.h"

#include "file/asset.h"
#include "file/stream.h"

#if __SYNTH_EDITOR
#include "commentbox.h"
#endif


namespace rage
{
	extern audPcmSourcePool *g_PcmSourcePool;

#if __SYNTH_EDITOR
PARAM(extraampchunks, "[AMP] Load additional data chunks, comma delimited");
#endif

#if __WIN32PC
	XPARAM(asio);
#endif
	

#if !__SPU

	const void *synthSynthesizer::sm_UnloadingMetadataEndPtr = NULL;
	const void *synthSynthesizer::sm_UnloadingMetadataStartPtr = NULL;

	audMetadataManager synthSynthesizer::sm_MetadataMgr;

	bool synthSynthesizer::InitClass()
	{
		synthCore::InitClass();
#if __BANK
		synthCoreDisasm::InitClass();
#endif

		const char *optimisedChunkName = "OPTIMISED";
#if __XENON
		const char *optimisedFileName = "update:/xbox360/audio/config/optamp.dat";
#elif __PS3
		const char *optimisedFileName = "update:/ps3/audio/config/optamp.dat";
#else
		const char *optimisedFileName = "audio:/config/optamp.dat";
#endif
		const char *baseChunkName = optimisedChunkName;
		const char *filePath = optimisedFileName;
		bool hasLoadedOptimisedChunk = true;
		
#if __SYNTH_EDITOR
		// Load both data sets
		baseChunkName = "BASE";
		filePath = "audio:/config/modsynth.dat";
		hasLoadedOptimisedChunk = false;
#endif

		if(!sm_MetadataMgr.Init("ModularSynth", filePath, audMetadataManager::External_NameTable_BankOnly, SYNTHOBJECTS_SCHEMA_VERSION, true, baseChunkName))
		{
			return false;
		}

		if(!hasLoadedOptimisedChunk)
		{
			if(!sm_MetadataMgr.LoadMetadataChunk(optimisedChunkName, optimisedFileName))
			{
				return false;
			}
		}

#if __SYNTH_EDITOR
		// Parse and load extra chunks when running with RAVE
		const char *extraChunksRO = "";
		if(sm_MetadataMgr.IsRAVEConnected() && PARAM_extraampchunks.Get(extraChunksRO) && extraChunksRO && strlen(extraChunksRO) > 0)
		{
			char *chunkList = StringDuplicate(extraChunksRO);
			const char* token = strtok(chunkList, ",");
			for (; token; token = strtok( NULL, ","))
			{
				if(!token)
				{
					break;
				}
				if(!sm_MetadataMgr.LoadMetadataChunk(token, token))
				{
					return false;
				}
			}
			StringFree(chunkList);		
		}
#endif
		return true;
	}

	void synthSynthesizer::ShutdownClass()
	{
		sm_MetadataMgr.Shutdown();
		synthCore::ShutdownClass();
	}

	void synthSynthesizer::UnloadMetadataChunk(const char *chunkName)
	{
		audAssertf(sm_UnloadingMetadataStartPtr == NULL, "Trying to unload %s with in-progress unload", chunkName);

		s32 chunkId = sm_MetadataMgr.FindChunkId(chunkName);
		if(chunkId == -1)
		{
			audErrorf("Failed to find synth data chunk %s to unload", chunkName);
			return;
		}

		if(audDriver::GetMixer())
		{
			const audMetadataChunk &chunk = sm_MetadataMgr.GetChunk(chunkId);

			audDriver::GetMixer()->EnterBML();

			// set up the unloading region to test against
			sm_UnloadingMetadataStartPtr = chunk.data;
			sm_UnloadingMetadataEndPtr = (const u8*)chunk.data + chunk.dataSize;

			// Ensure no new synths are created from this chunk
			sm_MetadataMgr.SetChunkEnabled(chunkId, false);

			// Grab the current mixer frame number
			const u32 mixerFrameWaitStarted = audDriver::GetMixer()->GetMixerTimeFrames();
			
			audDriver::GetMixer()->ExitBML();

			// wait until the next frame completes
			while(audDriver::GetMixer()->GetMixerTimeFrames() == mixerFrameWaitStarted)
			{
				sysIpcSleep(5);
				audDisplayf("Waiting for mixer frame to complete ...");
			}
		}

		// Should now be safe to unload this chunk
		sm_MetadataMgr.UnloadMetadataChunk(chunkName);

		// Clear unloading region
		sm_UnloadingMetadataEndPtr = NULL;
		sm_UnloadingMetadataStartPtr = NULL;
	}

	const Preset *synthSynthesizer::FindPreset(const char *name)
	{
		return FindPreset(atStringHash(name));
	}

	const Preset *synthSynthesizer::FindPreset(const u32 nameHash)
	{
		return nameHash == 0 ? NULL : sm_MetadataMgr.GetObject<Preset>(nameHash);
	}
#endif // !__SPU

	const CompiledSynth *synthSynthesizer::FindOptimizedMetadata(const u32 nameHash)
	{
		// Search all chunks looking for an unoptimised version of the requested object
		// Start with the most recently loaded chunk and work backwards
		for(s32 i = sm_MetadataMgr.GetNumChunks() - 1; i >= 0; i--)
		{
			audMetadataObjectInfo info;
			if(sm_MetadataMgr.GetObjectInfoFromSpecificChunk(nameHash, static_cast<u32>(i), info))
			{
				if(info.GetType() == CompiledSynth::TYPE_ID)
				{
					return info.GetObject<CompiledSynth>();
				}
			}
		}
		return NULL;
	}

#if __SYNTH_EDITOR

	const ModularSynth *synthSynthesizer::FindMetadata(const char *name)
	{
		return FindMetadata(atStringHash(name));
	}

	const ModularSynth *synthSynthesizer::FindMetadata(const u32 nameHash)
	{
		// Search all chunks looking for an unoptimised version of the requested object
		// Start with the most recently loaded chunk and work backwards
		for(s32 i = sm_MetadataMgr.GetNumChunks() - 1; i >= 0; i--)
		{
			audMetadataObjectInfo info;
			if(sm_MetadataMgr.GetObjectInfoFromSpecificChunk(nameHash, static_cast<u32>(i), info))
			{
				if(info.GetType() == ModularSynth::TYPE_ID)
				{
					return info.GetObject<ModularSynth>();
				}
			}
		}
		return NULL;
	}

	s32 synthSynthesizer::Create(const char *name, synthUIObjects *uiObjects /*= NULL*/)
	{
		return Create(FindMetadata(name), 0, uiObjects);
	}

	s32 synthSynthesizer::Create(const u32 nameHash, const u32 presetHash, synthUIObjects *uiObjects /*= NULL*/)
	{
		return Create(FindMetadata(nameHash), FindPreset(presetHash), uiObjects);
	}

	synthModule *synthSynthesizer::InstantiateModule(const synthBinaryModuleReader &moduleReader)
	{
		synthModule *module = synthModuleFactory::CreateModule((synthModuleId)moduleReader.GetModuleData()->TypeId);

		// field data
		synthModuleBinaryMemSerializer serializer;
		serializer.SetPointerForLoad((u8*)moduleReader.GetFields());
		module->SerializeState(&serializer);

#if __SYNTH_EDITOR
		Matrix34 mat = module->GetView()->GetMatrix();
		mat.d = Vector3(moduleReader.GetModuleData()->PosX,moduleReader.GetModuleData()->PosY,0.f);
		module->GetView()->SetMatrix(mat);
		module->GetView()->SetIsCollapsed(moduleReader.GetModuleData()->IsCollapsed != 0.f);
#endif
		module->SetProcessingStage(moduleReader.GetModuleData()->ProcessingStage);

		return module;
	}

	s32 synthSynthesizer::Create(const ModularSynth *const instanceData, const Preset *const presetData , synthUIObjects *SYNTH_EDITOR_ONLY(uiObjects) /*= NULL*/)
	{
		const s32 synthSlotId = audDriver::GetMixer()->AllocatePcmSource_Sync(AUD_PCMSOURCE_MODULARSYNTH);
		
		if(synthSlotId == -1)
		{
			return -1;
		}
		
		synthSynthesizer *synth = static_cast<synthSynthesizer*>(audDriver::GetMixer()->GetPcmSource(synthSlotId));
		Assert(synth);
		if(instanceData)
		{
			synthBinaryReader synthReader(instanceData);

			synth->m_ShouldExport = AUD_GET_TRISTATE_VALUE(instanceData->Flags, FLAG_ID_MODULARSYNTH_EXPORTFORGAME) == AUD_TRISTATE_TRUE;
			synth->m_NumProcessingStages = -1;
			u32 stateSize = 0;
			// first pass - create modules
			// skip module 0 as its the auto-created output module
			synthBinaryModuleReader moduleReader;
			const u32 numModules = synthReader.GetNumModules();
			if(numModules > 0)
			{
				synthReader.GetNextModule(moduleReader);
				Assert(moduleReader.GetModuleData()->TypeId == SYNTH_AUDIOOUTPUT);

				for(u32 i = 1; i < numModules; i++)
				{
					synthReader.GetNextModule(moduleReader);
					stateSize += synthModuleFactory::GetModuleSize((synthModuleId)moduleReader.GetModuleData()->TypeId);
					synthModule *module = InstantiateModule(moduleReader);
					Assert(module);
					synth->AddModule(module);
				}
			}

			synthReader.Reset();

			// second pass - set up connections
			for(u32 i = 0; i < numModules; i++)
			{
				synthModule *module = synth->GetModules()[i];
				Assert(module);

				synthReader.GetNextModule(moduleReader);

				if(module->GetTypeId() == SYNTH_AUDIOOUTPUT)
				{
					// we skipped the audio output in the first pass
					module->SetProcessingStage(moduleReader.GetModuleData()->ProcessingStage);

					synth->m_NumProcessingStages = module->GetProcessingStage();
#if __SYNTH_EDITOR
					Matrix34 mat = module->GetView()->GetMatrix();
					mat.d = Vector3(moduleReader.GetModuleData()->PosX,moduleReader.GetModuleData()->PosY,0.f);
					module->GetView()->SetMatrix(mat);
					module->GetView()->SetIsCollapsed(moduleReader.GetModuleData()->IsCollapsed != 0.f);
#endif
				}
			
				SYNTH_EDITOR_ONLY(module->GetView()->SetGroupId(moduleReader.GetModuleData()->GroupId));

				// inputs
				synthPin *pins;
				u32 numPins;			
				module->GetInputs(pins, numPins);
				// use the saved number of pins rather than the reported number of pins, since some modules
				// dynamically allow extra pins.  This is a bit risky...
				
				numPins = moduleReader.GetNumInputPins();
				for(u32 pinId = 0; pinId < numPins; pinId++)
				{
					if(pinId < module->GetMaxInputs())
					{
						if(moduleReader.GetInputPins()[pinId].OtherModule != -1)
						{
							const u32 otherPinIndex = moduleReader.GetInputPins()[pinId].OtherPin;
							synthModule *otherModule = synth->GetModules()[moduleReader.GetInputPins()[pinId].OtherModule];
							if(otherPinIndex < otherModule->GetMaxOutputs())
							{
								synthPin &otherPin = otherModule->GetOutput(otherPinIndex);
								pins[pinId].Connect(otherPin);
							}
						}
						else
						{
							pins[pinId].SetStaticValue(moduleReader.GetInputPins()[pinId].StaticValue);
						}
					}
				}
			
				// don't need to do anything about outputs				
			}

			// hook up any exposed variables
			u32 numVariables = synthReader.GetNumExposedVariables();
			for(u32 i = 0; i < numVariables; i++)
			{
				const ModularSynth::tExposedVariable *varData = synthReader.GetNextVariable();
				if(varData->ModuleId != -1 && varData->ModuleId < synth->GetModules().GetCount())
				{
					synthModule *module = synth->GetModules()[varData->ModuleId];
					Assert(module);
					Assert(module->GetTypeId() == SYNTH_VARIABLEINPUT);

					if(module->GetTypeId() == SYNTH_VARIABLEINPUT)
					{
						((synthVariableInput*)module)->SetExportedKey(varData->Key);
						if(synth->m_Variables.IsFull())
						{
							audWarningf("Synth has too many variables: %s (limit is %d)", varData->Key, synth->m_Variables.GetCount());
						}
						else
						{
							synthExposedVariable &var = synth->m_Variables.Append();
							var.key = atStringHash(varData->Key);
							var.moduleIndex = varData->ModuleId;
							var.value = module->GetInput(0).GetStaticValue();
						}						
					}	
				}
			}	

#if __SYNTH_EDITOR
			if(uiObjects)
			{
				// recreate groups
				struct groupMembership
				{
					synthUIViewGroup *groupToAdd;
					u32 groupIdToAddTo;
				};
				atArray<groupMembership> deferredGroupMembership;
				const u32 numGroups = synthReader.GetNumGroups();
				
				for(u32 i = 0; i < numGroups; i++)
				{
					const ModularSynth::tModuleGroup *groupData = synthReader.GetNextGroup();
					
					synthUIViewGroup *group = rage_new synthUIViewGroup();
					group->SetName(groupData->Name);
					group->SetColour(groupData->Colour.R,groupData->Colour.G,groupData->Colour.B);
					
					uiObjects->groups.Grow() = group;

					if(groupData->ParentGroupId != -1)
					{
						u32 groupId = (u32)groupData->ParentGroupId;

						groupMembership &m = deferredGroupMembership.Grow();
						m.groupIdToAddTo = groupId;
						m.groupToAdd = group;
					}
				}

				for(s32 i = 0; i < deferredGroupMembership.GetCount(); i++)
				{
					if(static_cast<int>(deferredGroupMembership[i].groupIdToAddTo) < uiObjects->groups.GetCount())
					{
						uiObjects->groups[deferredGroupMembership[i].groupIdToAddTo]->Add(deferredGroupMembership[i].groupToAdd);
					}
				}
				
				// hook up modules to the newly created groups
				for(u32 i = 0; i < numModules; i++)
				{
					synthModule *module = synth->GetModules()[i];
					if(module->GetView()->GetGroupId() != -1)
					{
						synthUIViewGroup *group = uiObjects->groups[module->GetView()->GetGroupId()];
						// Invalidate the cached group id; its only useful while loading
						module->GetView()->SetGroupId(-1);
						group->Add(module->GetView());
					}
				}

				// recreate comments		
				const u32 numComments = synthReader.GetNumComments();
				for(u32 i = 0; i < numComments; i++)
				{
					const ModularSynth::tComments *commentData = synthReader.GetNextComment();
					synthCommentBox *commentBox = rage_new synthCommentBox();
					commentBox->SetText(commentData->Text);
					Color32 color(commentData->Colour.R,commentData->Colour.G,commentData->Colour.B,commentData->Colour.A);
					commentBox->SetColor(color);
					Matrix34 mat = uiObjects->rootMatrix;
					mat.d.x = commentData->PosX;
					mat.d.y = commentData->PosY;
					commentBox->SetMatrix(mat);
					uiObjects->commentBoxes.Grow() = commentBox;
					if(commentData->GroupId != -1)
					{
						uiObjects->groups[commentData->GroupId]->Add(commentBox);
					}
				}

				// load cameras
				const u32 numCameras = synthReader.GetNumCameras();
				for(u32 i = 0; i < numCameras; i++)
				{
					const ModularSynth::tCameraPosition *camera = synthReader.GetNextCamera();
					const Vector3 position(camera->PosX, camera->PosY, camera->PosZ);
					uiObjects->cameraPositions.Grow() = position;
				}
			}
			
#endif
			SYNTH_EDITOR_ONLY(audDisplayf("Loaded %u modules, state size: %u", synth->GetModules().GetCount(), stateSize));
		}

		synth->ApplyPreset(presetData);
		
		return synthSlotId;
	}

	synthSynthesizer::synthSynthesizer() : audPcmSource(AUD_PCMSOURCE_MODULARSYNTH)
	{
		m_NumProcessingStages = -1;
		m_CPUTime = 0.f;
		m_HasFinished = false;
				
		// create a default output for now
		AddModule(rage_new synthAudioOutput());

		SYNTH_EDITOR_ONLY(m_AuditioningPin = NULL);
		SYNTH_EDITOR_ONLY(strcpy(m_Name, "Synth1"));
		SYNTH_EDITOR_ONLY(m_Muted = false);
		SYNTH_EDITOR_ONLY(m_Frozen = false);
		SYNTH_EDITOR_ONLY(m_ShouldExport = false);
	}

	void synthSynthesizer::Shutdown()
	{
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			u32 numInputs = 0;
			synthPin *inputs;
			(*i)->GetInputs(inputs, numInputs);
			for(u32 input = 0; input < numInputs; input++)
			{
				if(inputs[input].IsConnected())
				{
					inputs[input].Disconnect();
				}
			}
		}
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			delete *i;	
		}
	}

	void synthSynthesizer::ResetProcessing()
	{
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			(*i)->ResetProcessing();
		}

		m_Context.Reset();
	}

	void synthSynthesizer::SerializeCurrentPreset(fiStream *stream, const char *presetName)
	{
		char buf[512];
		formatf(buf, "<Preset name=\"%s\">\r\n", presetName);
		stream->Write(buf, istrlen(buf));
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			if((*i)->GetTypeId() == SYNTH_VARIABLEINPUT)
			{
				synthVariableInput *variableInput = (synthVariableInput*)(*i); 
				if(variableInput->GetExportedKeyName() != NULL)
				{
					const f32 val = variableInput->GetInput(0).GetStaticValue();
					formatf(buf, "<Variable><Name>%s</Name><Value>%f</Value></Variable>", variableInput->GetExportedKeyName(), FPIsFinite(val) ? val : 0.f);
					stream->Write(buf, istrlen(buf));
				}
			}
		}
		formatf(buf, "</Preset>");
		stream->Write(buf, istrlen(buf));
	}

	void synthSynthesizer::Serialize(fiStream *SYNTH_EDITOR_ONLY(stream), const char *SYNTH_EDITOR_ONLY(synthName))
	{		
		char buf[512];
		formatf(buf, "<ModularSynth name=\"%s\">\r\n", synthName);
		stream->Write(buf, istrlen(buf));
		
		formatf(buf, "<ExportForGame>%s</ExportForGame>", ShouldExport() ? "yes" : "no");
		stream->Write(buf, istrlen(buf));

		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			synthModuleXmlOutputSerializer serializer(stream);

			u32 typeId = (*i)->GetTypeId();
			serializer.StartModule((*i)->GetName(), typeId);
				serializer.WriteElementValue("PosX", "", (*i)->GetView()->GetMatrix().d.x);
				serializer.WriteElementValue("PosY", "", (*i)->GetView()->GetMatrix().d.y);
				serializer.WriteElementValue("PosZ", "", (*i)->GetView()->GetMatrix().d.z);
				serializer.WriteElementValue("IsCollapsed", "", (f32)(*i)->GetView()->IsCollapsed());
				serializer.WriteElementValue("ProcessingStage","",(*i)->GetProcessingStage());
				synthUIViewGroup *group = (*i)->GetView()->GetGroup();
				if(group)
				{
					serializer.WriteElementValue("GroupId", "", synthSynthesizerView::Get()->GetUIPlane().GetGroupId(group));
				}

				(*i)->Serialize(&serializer);
			serializer.FinishedModule();
		}
		for(u32 i = 0; i < synthSynthesizerView::Get()->GetModulePlane().GetNumGroups(); i++)
		{
			synthUIViewGroup *group = synthSynthesizerView::Get()->GetModulePlane().GetGroup(i);
			formatf(buf,"<ModuleGroup>\r\n");
			stream->Write(buf, istrlen(buf));
			formatf(buf,"<Name>%s</Name>\r\n", group->GetName());
			stream->Write(buf, istrlen(buf));
			const Color32 &colour = group->GetColour();
			formatf(buf,"<Colour><R>%u</R><G>%u</G><B>%u</B></Colour>\r\n", colour.GetRed(),colour.GetGreen(),colour.GetBlue());
			stream->Write(buf, istrlen(buf));

			if(group->IsGrouped())
			{
				formatf(buf, "<ParentGroupId>%d</ParentGroupId>\r\n", synthSynthesizerView::Get()->GetUIPlane().GetGroupId(group->GetGroup()));
				stream->Write(buf, istrlen(buf));
			}
			formatf(buf,"</ModuleGroup>\r\n");
			stream->Write(buf, istrlen(buf));
		}

		// exported variables
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			if((*i)->GetTypeId() == SYNTH_VARIABLEINPUT)
			{
				synthVariableInput *variableInput = (synthVariableInput*)(*i);
				if(variableInput->GetExportedKey() != 0)
				{
					formatf(buf,"<ExposedVariable><Key>%s</Key><ModuleId>%d</ModuleId></ExposedVariable>", variableInput->GetExportedKeyName(), variableInput->GetIndex());
					stream->Write(buf, istrlen(buf));
				}
			}
		}

		// comments
		for(u32 i = 0; i < synthSynthesizerView::Get()->GetModulePlane().GetNumCommentBoxes(); i++)
		{
			const synthCommentBox *comment = synthSynthesizerView::Get()->GetModulePlane().GetCommentBox(i);
			Assert(comment);

			if(strlen(comment->GetText()) == 0)
			{
				continue;
			}
			formatf(buf, "<Comments>\r\n");
			stream->Write(buf, istrlen(buf));

			const Color32 &color = comment->GetColor();
			formatf(buf,"<Colour><R>%u</R><G>%u</G><B>%u</B><A>%u</A></Colour>\r\n", color.GetRed(),color.GetGreen(),color.GetBlue(), color.GetAlpha());
			stream->Write(buf, istrlen(buf));
			formatf(buf, "<PosX>%f</PosX>\r\n", comment->GetMatrix().d.x);
			stream->Write(buf, istrlen(buf));
			formatf(buf, "<PosY>%f</PosY>\r\n", comment->GetMatrix().d.y);
			stream->Write(buf, istrlen(buf));
			formatf(buf, "<Text>%s</Text>\r\n", comment->GetText());
			stream->Write(buf, istrlen(buf));

			if(comment->IsGrouped())
			{
				formatf(buf, "<GroupId>%d</GroupId>\r\n", synthSynthesizerView::Get()->GetUIPlane().GetGroupId(comment->GetGroup()));
				stream->Write(buf, istrlen(buf));
			}

			formatf(buf, "</Comments>\r\n");
			stream->Write(buf, istrlen(buf));
		}

		// camera positions

		for(u32 i = 0; i < synthSynthesizerView::Get()->GetNumCameraPositions(); i++)
		{
			formatf(buf, "<CameraPosition>\r\n");
			stream->Write(buf, istrlen(buf));
			
			formatf(buf, "<PosX>%f</PosX>\r\n", synthSynthesizerView::Get()->GetCameraPosition(i).x);
			stream->Write(buf, istrlen(buf));
			formatf(buf, "<PosY>%f</PosY>\r\n", synthSynthesizerView::Get()->GetCameraPosition(i).y);
			stream->Write(buf, istrlen(buf));
			formatf(buf, "<PosZ>%f</PosZ>\r\n", synthSynthesizerView::Get()->GetCameraPosition(i).z);
			stream->Write(buf, istrlen(buf));
			
			formatf(buf, "</CameraPosition>\r\n");
			stream->Write(buf, istrlen(buf));
		}


		formatf(buf, "</ModularSynth>\r\n");
		stream->Write(buf, istrlen(buf));
}

	void synthSynthesizer::AddModule(synthModule *module)
	{
		Assert(module);
		SYS_CS_SYNC(m_GraphCS);
		const s32 index = m_Modules.GetCount();
		module->SetIndex(index);
		m_Modules.PushAndGrow(module);		
		if(module->GetTypeId() == SYNTH_AUDIOOUTPUT)
		{
			m_OutputIndices.Append() = (u32)index;
		}
		else if(module->GetTypeId() == SYNTH_AUDIOINPUT)
		{
			m_InputIndices.Append() = (u32)index;		
		}
	}

	void synthSynthesizer::ApplyPreset(const Preset *preset)
	{
		if(preset)
		{
			for(u32 i = 0; i < preset->numVariables; i++)
			{
				const float randomOffset = audEngineUtil::GetRandomNumberInRange(-preset->Variable[i].Variance, 
																					preset->Variable[i].Variance);
				SetVariableValue(preset->Variable[i].Name, preset->Variable[i].Value + randomOffset);
			}
		}
	}

	u32 synthSynthesizer::GetOutputIndex(const synthModule *module)
	{
		return m_OutputIndices.Find(module->GetIndex());
	}

	void synthSynthesizer::RemoveModule(synthModule *module)
	{
		SYS_CS_SYNC(m_GraphCS);

		if(module->GetTypeId() == SYNTH_AUDIOOUTPUT)
		{
			if(GetOutputIndex(module) == 0)
			{
				// dont allow the user to delete the output
				return;
			}
			else
			{
				m_OutputIndices.Delete(GetOutputIndex(module));
			}
		}
		else if(module->GetTypeId() == SYNTH_AUDIOINPUT)
		{
			m_InputIndices.Delete(module->GetIndex());
		}
		// firstly ensure this module is fully disconnected from all other modules
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			synthPin *pins;
			u32 numPins;
			(*i)->GetInputs(pins,numPins);
			for(u32 k = 0; k < numPins; k++)
			{
				if(pins[k].IsConnected() && pins[k].GetOtherPin()->GetParentModule() == module)
				{
					pins[k].Disconnect();
				}
			}
		}

#if __SYNTH_EDITOR
		if(m_AuditioningPin && m_AuditioningPin->GetParentModule() == module)
		{
			m_AuditioningPin = NULL;
		}
#endif

		// now remove it from the module list
		for(s32 i =0; i < m_Modules.GetCount(); i++)
		{
			if(m_Modules[i] == module)
			{
				m_Modules.Delete(i);
				break;
			}
		}

		// Update input/output indices
		m_OutputIndices.Reset();
		m_InputIndices.Reset();

		for(s32 i = 0; i < m_Modules.GetCount(); i++)
		{
			if(m_Modules[i]->GetTypeId() == SYNTH_AUDIOINPUT)
			{
				m_InputIndices.Append() = i;
			}
			else if(m_Modules[i]->GetTypeId() == SYNTH_AUDIOOUTPUT)
			{
				m_OutputIndices.Append() = i;
			}
		}
	}

	void synthSynthesizer::ComputeProcessingGraph()
	{
		SYS_CS_SYNC(m_GraphCS);
		u32 index = 0;
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			(*i)->SetProcessingStage(-1);
			(*i)->SetIndex(index++);
		}

		m_Variables.Reset();
		atArray<atArray<s32>> moduleStageOutputs;
		atArray<const synthModule *> visitedModules;
		moduleStageOutputs.Resize(index);
		visitedModules.Reserve(index);
						
		s32 currentStage = 0;
		synthModule *currentModule = GetOutput(0);
		
		moduleStageOutputs[currentModule->GetIndex()].Grow() = 0;
		visitedModules.Append() = currentModule;

		ComputeProcessingGraph(currentModule, currentStage, visitedModules, moduleStageOutputs);

#if __SYNTH_EDITOR
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			s32 moduleMaxStage = -1;
			for(atArray<s32>::iterator si = moduleStageOutputs[(*i)->GetIndex()].begin(); si != moduleStageOutputs[(*i)->GetIndex()].end(); si++)
			{
				moduleMaxStage = Max(moduleMaxStage, (*si));
			}

			// ensure that modules are updated in a safe order; anything not connected to anything is treated as if it is connected
			// straight to the output
			if(moduleMaxStage == -1)
			{
				u32 numDestinations = 0;
				u32 numPins;
				synthPin *pins;
				(*i)->GetOutputs(pins,numPins);
				for(u32 outputIndex = 0; outputIndex < numPins; outputIndex++)
				{
					numDestinations += (*i)->GetOutput(outputIndex).GetView()->GetOutputDestinations().GetCount();
				}

				if(numDestinations == 0)
				{
					// this pin is not connected to anything; calculate the graph in isolation
					moduleStageOutputs[(*i)->GetIndex()].Grow() = 0;
					ComputeProcessingGraph(*i, 0, visitedModules, moduleStageOutputs);
				}
			}
		}
#endif

		s32 endStage = -1;
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			s32 moduleMaxStage = -1;
			for(atArray<s32>::iterator si = moduleStageOutputs[(*i)->GetIndex()].begin(); si != moduleStageOutputs[(*i)->GetIndex()].end(); si++)
			{
				moduleMaxStage = Max(moduleMaxStage, (*si));
			}

			endStage = Max(endStage, moduleMaxStage);
		}

		m_NumProcessingStages = endStage;
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			s32 moduleProcessingStage = -1;
			for(atArray<s32>::iterator si = moduleStageOutputs[(*i)->GetIndex()].begin(); si != moduleStageOutputs[(*i)->GetIndex()].end(); si++)
			{
				moduleProcessingStage = Max(moduleProcessingStage, (*si));
			}
			if(moduleProcessingStage != -1)
			{
				(*i)->SetProcessingStage(endStage - moduleProcessingStage);
			}
			else
			{
#if __SYNTH_EDITOR
				audWarningf("Module missing from processing graph");
#endif
				(*i)->SetProcessingStage(-1);
			}
		}

		// compute buffer requirements
		/*audDisplayf("------------------------- Buffer requirements");
		for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
		{
			const u32 numOutputBuffersReqd = (*i)->ComputeNumOutputBuffersRequired();
			const u32 numReusableInputBuffers = (*i)->ComputeNumReusableInputBuffers();
			audDisplayf("Index %d: Name: %s :Processing stage(%d) numOutputBuffers: %u numReusableInputs: %u numAllocsReqd: %u", (*i)->GetIndex(), (*i)->GetName(), (*i)->GetProcessingStage(),
				numOutputBuffersReqd, numReusableInputBuffers, numOutputBuffersReqd > numReusableInputBuffers ? numOutputBuffersReqd - numReusableInputBuffers : 0);
		}*/

	}

	bool synthSynthesizer::IsGraphRecursive(synthModule *moduleToSearchFor, atArray<const synthModule *> &visitedModules)
	{
		const s32 result = visitedModules.Find(moduleToSearchFor);
		return (result != -1);
	}

	void synthSynthesizer::AddOutputStage(synthModule *module, s32 stage, atArray<const synthModule *> &visitedModules, atArray<atArray<s32>> &moduleStageOutputs)
	{
		moduleStageOutputs[module->GetIndex()].Grow() = stage;	

		synthPin *pins;
		u32 numPins = 0;
		module->GetInputs(pins, numPins);
		for(u32 i = 0; i < numPins; i++)
		{
			if(pins[i].IsConnected())
			{
				Assert(pins[i].GetOtherPin()->GetParentModule());
				if(!IsGraphRecursive(pins[i].GetOtherPin()->GetParentModule(), visitedModules))
				{
					visitedModules.Grow() = module;
					AddOutputStage(pins[i].GetOtherPin()->GetParentModule(), stage+1, visitedModules, moduleStageOutputs);
				}
			}
		}
	}
	void synthSynthesizer::ComputeProcessingGraph(synthModule *module, s32 stage, atArray<const synthModule *> &visitedModules, atArray<atArray<s32>> &moduleStageOutputs)
	{
		synthPin *pins;
		u32 numPins = 0;		

		module->GetInputs(pins, numPins);
		
		for(u32 i = 0; i < numPins; i++)
		{
			if(pins[i].IsConnected())
			{
				Assert(pins[i].GetOtherPin()->GetParentModule());
				atArray<const synthModule *> thisStageVisitedModules;
				AddOutputStage(pins[i].GetOtherPin()->GetParentModule(), stage+1, thisStageVisitedModules, moduleStageOutputs);		
			}
		}
		for(u32 i = 0; i < numPins; i++)
		{
			if(pins[i].IsConnected())
			{
				Assert(pins[i].GetOtherPin()->GetParentModule());
				if(!IsGraphRecursive(pins[i].GetOtherPin()->GetParentModule(), visitedModules))
				{
					visitedModules.Grow() = pins[i].GetOtherPin()->GetParentModule();
					ComputeProcessingGraph(pins[i].GetOtherPin()->GetParentModule(),stage+1,visitedModules,moduleStageOutputs);
				}
			}
		}

		// count number of output destinations
		module->GetOutputs(pins, numPins);
		
		for(u32 i = 0; i < numPins; i++)
		{
			u32 numConnections = 0;
			// search all module inputs for connections to this pin
			for(atArray<synthModule*>::iterator iter = m_Modules.begin(); iter != m_Modules.end(); iter++)
			{
				synthPin *inputPins;
				u32 numInputs;
				(*iter)->GetInputs(inputPins, numInputs);
				for(u32 k = 0; k < numInputs; k++)
				{
					if((*iter)->GetInput(k).GetOtherPin() == &pins[i])
					{
						numConnections++;
					}
				}
			}
			pins[i].SetNumOutputConnections(numConnections);
		}
	}

	void synthSynthesizer::Synthesize()
	{
		SYS_CS_SYNC(m_GraphCS);

		sysPerformanceTimer synthTimer("synthTimer");
		synthTimer.Start();

		// push any exposed variable values down to modules
		for(s32 i = 0; i < m_Variables.GetCount(); i++)
		{
			if(m_Variables[i].changed)
			{
				m_Variables[i].changed = false;
				Assert(m_Modules[m_Variables[i].moduleIndex]->GetTypeId() == synthVariableInput::TypeId);
				if(m_Modules[m_Variables[i].moduleIndex]->GetTypeId() == synthVariableInput::TypeId)
				{
					((synthVariableInput*)m_Modules[m_Variables[i].moduleIndex])->SetValue(m_Variables[i].value);
				}
			}
		}

		SYNTH_EDITOR_ONLY(sysPerformanceTimer moduleTimer("moduleTimer"));

		for(s32 stage = 0; stage <= m_NumProcessingStages; stage++)
		{
			for(atArray<synthModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
			{
				if((*i)->GetProcessingStage() == stage)
				{
					SYNTH_EDITOR_ONLY(moduleTimer.Start());

					(*i)->Synthesize(m_Context);
#if __SYNTH_EDITOR
					moduleTimer.Stop();
					(*i)->SetCPUTime(moduleTimer.GetTimeMS());
					moduleTimer.Reset();

					if((*i)->HasView())
					{
						(*i)->GetView()->PostSynthesize();
					}
#endif
				}
			}
		}

		m_Context.IncrFrameCount();
		synthTimer.Stop();
		m_CPUTime = synthTimer.GetTimeMS();		
	}


	/// audPcmSource functionality
	void synthSynthesizer::Start()
	{

	}

	void synthSynthesizer::Stop()
	{

	}

	void synthSynthesizer::SetParam(const u32 UNUSED_PARAM(paramId), const u32 UNUSED_PARAM(val))
	{

	}

	void synthSynthesizer::SetParam(const u32 paramId, const f32 val)
	{
		SetVariableValue(paramId, val);
	}

	synthAudioOutput *synthSynthesizer::GetOutput(const u32 index)
	{
		synthAudioOutput *out = (synthAudioOutput*)(m_Modules[m_OutputIndices[index]]);
		Assert(out->GetTypeId() == SYNTH_AUDIOOUTPUT);
		return out;
	}

	synthAudioInput *synthSynthesizer::GetInput(const u32 index)
	{
		synthAudioInput *out = (synthAudioInput*)(m_Modules[m_InputIndices[index]]);
		Assert(out->GetTypeId() == SYNTH_AUDIOINPUT);
		return out;
	}

	void synthSynthesizer::StartPhys(s32 UNUSED_PARAM(channel))
	{
		if(m_NumPhysicalClients++ == 0)
		{
			Assert(!HasBuffer(0));
			AllocateBuffers(m_OutputIndices.GetCount());
			for(s32 i = 0; i < m_OutputIndices.GetCount(); i++)
			{
				GetOutput(i)->SetOutputBufferId(GetBufferId(i));
			}
		}
	}

	void synthSynthesizer::StopPhys(s32 UNUSED_PARAM(channel))
	{
		if(--m_NumPhysicalClients == 0)
		{
			Assert(HasBuffer(0));
			FreeBuffers();
		}
	}
	
	void synthSynthesizer::GenerateFrame()
	{		
#if __ASSERT
		for(s32 i = 0; i < m_OutputIndices.GetCount(); i++)
		{
#if __SYNTH_EDITOR
			if(m_BufferIds[i] == audFrameBufferPool::InvalidId)
			{
				Assign(m_BufferIds[i], g_FrameBufferPool->Allocate());
				GetOutput(i)->SetOutputBufferId(m_BufferIds[i]);
			}
#endif
			Assert(GetOutput(i)->GetBuffer().HasBuffer());
			Assert(kMixBufNumSamples==GetOutput(i)->GetBuffer().GetSize());
		}
#endif

		if(!m_Frozen)
			Synthesize();		

#if __SYNTH_EDITOR
		synthPin *auditioningPin = m_AuditioningPin;
		if(auditioningPin)
		{
			for(s32 i = 0; i < m_OutputIndices.GetCount(); i++)
			{
				sysMemCpy128(GetOutput(i)->GetBuffer().GetBuffer(),
					auditioningPin->GetDataBuffer()->GetBuffer(),
					kMixBufNumSamples * sizeof(f32));
			}
		}

		if(m_Muted || m_Frozen)
		{
			for(s32 i = 0; i < m_OutputIndices.GetCount(); i++)
			{
				if(GetOutput(i)->GetBuffer().HasBuffer())
					sysMemSet128(GetOutput(i)->GetBuffer().GetBuffer(), 0, kMixBufNumSamples * sizeof(f32));
			}
		}
#endif

#if !__SYNTH_EDITOR	
		bool allFinished = true;
		for(atFixedArray<u32,kMaxAudioOutputs>::const_iterator iter = m_OutputIndices.begin(); iter != m_OutputIndices.end(); iter++)
		{
			if(iter)
			{
				Assert(m_Modules[*iter]->GetTypeId() == synthAudioOutput::TypeId);
				if(!((synthAudioOutput*)m_Modules[*iter])->HasFinished())
				{
					allFinished = false;
				}
			}
		}
		m_HasFinished = allFinished;
#endif
	}

	void synthSynthesizer::ProcessDsp(const atRangeArray<u16, g_MaxOutputChannels> &bufferIds)
	{		
		for(s32 i = 0; i < m_OutputIndices.GetCount(); i++)
		{
			if(m_BufferIds[i] == audFrameBufferPool::InvalidId)
			{				
				GetOutput(i)->SetOutputBufferId(bufferIds[i]);
			}

			Assert(GetOutput(i)->GetBuffer().HasBuffer());
			Assert(kMixBufNumSamples==GetOutput(i)->GetBuffer().GetSize());
		}

		for(s32 i = 0; i < m_InputIndices.GetCount(); i++)
		{
			GetInput(i)->SetBufferId(bufferIds[GetInput(i)->GetInputId()]);
		}

		Synthesize();		

#if __SYNTH_EDITOR
		synthPin *auditioningPin = m_AuditioningPin;
		if(auditioningPin)
		{
			sysMemCpy128(GetOutput(0)->GetBuffer().GetBuffer(),
				auditioningPin->GetDataBuffer()->GetBuffer(),
				kMixBufNumSamples * sizeof(f32));
		}

		if(m_Muted || m_Frozen)
		{
			for(s32 i = 0; i < m_OutputIndices.GetCount(); i++)
			{
				sysMemSet128(GetOutput(i)->GetBuffer().GetBuffer(), 0, kMixBufNumSamples * sizeof(f32));
			}
		}
#endif

#if !__SYNTH_EDITOR	
		bool allFinished = true;
		for(atFixedArray<u32,kMaxAudioOutputs>::const_iterator iter = m_OutputIndices.begin(); iter != m_OutputIndices.end(); iter++)
		{
			if(iter)
			{
				Assert(m_Modules[*iter]->GetTypeId() == synthAudioOutput::TypeId);
				if(!((synthAudioOutput*)m_Modules[*iter])->HasFinished())
				{
					allFinished = false;
				}
			}
		}
		m_HasFinished = allFinished;
#endif
	}

	bool synthSynthesizer::SetVariableValue(const u32 nameHash, const f32 val)
	{
		for(s32 i = 0; i < m_Variables.GetCount(); i++)
		{
			if(m_Variables[i].key == nameHash)
			{
				m_Variables[i].value = val;
				m_Variables[i].changed = true;
				return true;
			}
		}
		return false;
	}

	bool synthSynthesizerDspEffect::Init(const u32 numChannels, const u32 channelMask)
	{
		Assign(m_NumInputChannels, numChannels);
		m_ChannelMask = (u8)channelMask;

		m_NumChannelsToProcess = 0;
		for(u32 i=0, mask = 1; i<numChannels; i++)
		{
			if(m_ChannelMask & mask)
			{
				m_NumChannelsToProcess++;
			}
			mask <<= 1;
		}

		m_Bypass = false;

		return true;
	}

	void synthSynthesizerDspEffect::Shutdown()
	{
		
	}

	void synthSynthesizerDspEffect::SetParam(const u32 paramId, const f32 value)
	{
		m_Synth->SetVariableValue(paramId, value);
	}

	void synthSynthesizerDspEffect::SetParam(const u32 paramId, const u32 value)
	{
		switch(paramId)
		{
		case synthCorePcmSource::Params::CompiledSynthNameHash:
			
			break;
		case synthCorePcmSource::Params::PresetNameHash:
			m_Synth->ApplyPreset(synthSynthesizer::FindPreset(value));
			break;
		}

	}


	AUD_DEFINE_DSP_PROCESS_FUNCTION(synthSynthesizerDspEffect)
	{
		if (m_Bypass)
			return;

		m_Synth->Lock();
		atRangeArray<u16, g_MaxOutputChannels> outputBufferIds;

		for(u32 i = 0, j = 0, mask = 1; i < buffer.NumChannels; i++)
		{
			if(m_ChannelMask & mask)
			{
				outputBufferIds[j] = buffer.ChannelBufferIds[i];
				j++;
			}
			mask <<= 1;
		}

		m_Synth->ProcessDsp(outputBufferIds);
		m_Synth->Unlock();
	}



#endif // __SYNTH_EDITOR
}
