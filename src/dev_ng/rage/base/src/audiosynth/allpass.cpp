#include "audiohardware/mixer.h"
#include "math/amath.h"
#include "allpass.h"
#include "allpassview.h"
#include "pin.h"
#include "vectormath/vectormath.h"

//#pragma optimize("g", off)

#define Undenormalize(sample) ((((*(u32*)&sample)&0x7f800000)==0)?0.0f:sample)

namespace rage
{
	using namespace Vec;

	SYNTH_EDITOR_LINK_VIEW(synthAllpassFilter,synthAllpassFilterView);

	synthAllpassFilter::synthAllpassFilter() 
	{
		m_x1 = m_x2 = m_y1 = m_y2 = 0.f;

		m_MinFrequency = 0.f;
		m_MaxFrequency = 20000.f;
		
		m_Outputs[0].Init(this,"Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[kAllpassGain].Init(this, "Gain", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAllpassGain].SetStaticValue(1.f);
		m_Inputs[kAllpassCutoff].Init(this, "Cutoff",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAllpassCutoff].SetStaticValue(250.f);
		m_Inputs[kAllpassBandwidth].Init(this, "Bandwidth",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAllpassBandwidth].SetStaticValue(25.f);
		m_Inputs[kAllpassSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kAllpassSignal].SetConvertsInternally(false);
	}

	void synthAllpassFilter::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("MinFreq", m_MinFrequency);
		serializer->SerializeField("MaxFreq", m_MaxFrequency);
	}
	
	//From DAFX edited by Udo Zolzer
	void synthAllpassFilter::Synthesize()
	{
		//const f32 twoPiOverSampFreq = 0.00013089969389957473f;
		const f32 piOverSampFreq = 0.00006544984694978735f;
		if(m_Inputs[kAllpassSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// pass static value through
			m_Outputs[0].SetStaticValue(m_Inputs[kAllpassSignal].GetStaticValue());
			return;
		}
		else
		{
			if(m_Inputs[kAllpassCutoff].GetDataState() == synthPin::SYNTH_PIN_STATIC && m_Inputs[kAllpassBandwidth].GetDataState() == synthPin::SYNTH_PIN_STATIC)
			{
				m_Outputs[0].SetDataBuffer(&m_Buffer);

				const synthPin::synthPinDataFormat dataFormat = m_Inputs[kAllpassSignal].GetDataFormat();
				m_Outputs[0].SetDataFormat(dataFormat);
				f32* samples = m_Inputs[kAllpassSignal].GetDataBuffer()->GetBuffer();

				for(u32 i=0; i<m_Outputs[0].GetDataBuffer()->GetSize(); ++i)
				{
					f32 input = *samples;

					const f32 freq = m_Inputs[kAllpassCutoff].GetNormalizedValue(i);
					const f32 tanArg = piOverSampFreq * freq;
					const f32 c = (tan(tanArg)-1)/(tan(tanArg)+1);

					const f32 output = Clamp(c*input + m_x1 - c*m_y1, -1.0f, 1.0f);

					m_y2 = Undenormalize(m_y1);
					m_y1 = Undenormalize(output);
					m_x2 = Undenormalize(m_x1);
					m_x1 = Undenormalize(input);

					samples++;
					m_Outputs[0].GetDataBuffer()->GetBuffer()[i] = (output * m_Inputs[kAllpassGain].GetNormalizedValue(i));
				}
			}
			else
			{
				m_Outputs[0].SetDataBuffer(&m_Buffer);

				const synthPin::synthPinDataFormat dataFormat = m_Inputs[kAllpassSignal].GetDataFormat();
				m_Outputs[0].SetDataFormat(dataFormat);
				f32* samples = m_Inputs[kAllpassSignal].GetDataBuffer()->GetBuffer();

				for(u32 i=0; i<m_Outputs[0].GetDataBuffer()->GetSize(); ++i)
				{
					f32 input = *samples;

					const f32 freq = m_Inputs[kAllpassCutoff].GetNormalizedValue(i);
					const f32 tanArg = piOverSampFreq * freq;
					const f32 c = (tan(tanArg)-1)/(tan(tanArg)+1);

					const f32 output = Clamp(c*input + m_x1 - c*m_y1, -1.0f, 1.0f);

					m_y2 = Undenormalize(m_y1);
					m_y1 = Undenormalize(output);
					m_x2 = Undenormalize(m_x1);
					m_x1 = Undenormalize(input);

					samples++;
					m_Outputs[0].GetDataBuffer()->GetBuffer()[i] = (output * m_Inputs[kAllpassGain].GetNormalizedValue(i));
				}
			}
			
			m_y2 = Undenormalize(m_y2);
			m_y1 = Undenormalize(m_y1);
			m_x2 = Undenormalize(m_x2);
			m_x1 = Undenormalize(m_x1);
		}//synthesize
	}

	void synthAllpassFilter::Process(f32* inputBuffer, const float cutoff, Vector_4V_InOut state, const u32 numSamples)
	{
		const f32 piOverSampFreq = 0.00006544984694978735f;
		f32 x1 = GetX(state);
		f32 y1 = GetY(state);

		const f32 freq = cutoff;
		const f32 tanArg = piOverSampFreq * freq;
		const f32 c = (tan(tanArg)-1)/(tan(tanArg)+1);

		for(u32 i=0; i<numSamples; i++)
		{
			const f32 input = inputBuffer[i];
			const f32 output = Clamp(c*input + x1 - c*y1, -1.0f, 1.0f);

			y1 = output;
			x1 = input;

			inputBuffer[i] = output;
		};

		SetX(state, Undenormalize(x1));
		SetY(state, Undenormalize(y1));
	}

	void synthAllpassFilter::Process(f32* inputBuffer, const f32* cutoff, Vector_4V_InOut state, const u32 numSamples)
	{
		const f32 piOverSampFreq = 0.00006544984694978735f;
		f32 x1 = GetX(state);
		f32 y1 = GetY(state);

		for(u32 i=0; i<numSamples; i++)
		{
			const f32 freq = cutoff[i];
			const f32 tanArg = piOverSampFreq * freq;
			const f32 c = (tan(tanArg)-1)/(tan(tanArg)+1);
			
			const f32 input = inputBuffer[i];
			const f32 output = Clamp(c*input + x1 - c*y1, -1.0f, 1.0f);

			y1 = output;
			x1 = input;

			inputBuffer[i] = output;
		};

		SetX(state, Undenormalize(x1));
		SetY(state, Undenormalize(y1));
	}
}
