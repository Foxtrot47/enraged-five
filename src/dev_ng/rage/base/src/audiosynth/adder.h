// 
// audiosynth/adder.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_ADDER_H
#define SYNTH_ADDER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum synthAdderInputs { kMaxAdderInputs = 16};
	class synthAdder : public synthModuleBase<kMaxAdderInputs, 1, SYNTH_ADDER>
	{
	public:
		SYNTH_MODULE_NAME("Add");

		synthAdder();
		virtual void GetInputs(synthPin *&pins, u32 &numPins);
		virtual void Synthesize();

	private:
		synthSampleFrame m_Buffer;
	};
}
#endif // !SYNTH_MINIMAL_MODULES
#endif // SYNTH_ADDER_H


