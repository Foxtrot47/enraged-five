// 
// audiosynth/envelopefollower.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "envelopefollower.h"

#if !SYNTH_MINIMAL_MODULES

#include "abs.h"
#include "envelopefollowerview.h"
#include "synthutil.h"

#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "math/amath.h"
#include "vectormath/vectormath.h"

#if __XENON
#include "system/xtl.h"
#include <xnamath.h> // For XMVectorPow()
#endif


namespace rage
{
	SYNTH_EDITOR_VIEW(synthEnvelopeFollower);

	using namespace Vec;

	synthEnvelopeFollower::synthEnvelopeFollower()
	{
		m_Outputs[0].Init(this, "Envelope",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Inputs[kEnvelopeFollowerSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kEnvelopeFollowerSignal].SetStaticValue(0.f);
		m_Inputs[kEnvelopeFollowerAttack].Init(this, "Attack", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kEnvelopeFollowerAttack].SetStaticValue(0.f);
		m_Inputs[kEnvelopeFollowerRelease].Init(this, "Release", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kEnvelopeFollowerRelease].SetStaticValue(0.f);

		m_Mode = Normal;
		m_Outputs[0].SetStaticValue(0.f);
		ResetProcessing();
	}

	void synthEnvelopeFollower::ResetProcessing()
	{
		m_Envelope = 0.f;
		m_State = V4VConstant(V_ZERO);
	}

	void synthEnvelopeFollower::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeEnumField("Mode", m_Mode);
	}

	void synthEnvelopeFollower::Synthesize()
	{
		if(m_Inputs[kEnvelopeFollowerSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC && m_Mode != ForceDynamic)
		{
			const f32 input = Abs(m_Inputs[kEnvelopeFollowerSignal].GetStaticValue());

			const float output = Process(input, 
				m_Inputs[kEnvelopeFollowerAttack].GetNormalizedValue(0),
				m_Inputs[kEnvelopeFollowerRelease].GetNormalizedValue(0),
				m_State);

			m_Outputs[0].SetStaticValue(output);
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			// 48kHz
			const Vector_4V sampleRate = V4VConstant<0x473B8000,0x473B8000,0x473B8000,0x473B8000>();
			// 0.01
			const Vector_4V v0_01 = V4VConstant<0x3C23D70A,0x3C23D70A,0x3C23D70A,0x3C23D70A>();
			// 0.00001f
			const Vector_4V vSmallValue = V4VConstant<0x3727C5AC,0x3727C5AC,0x3727C5AC,0x3727C5AC>();

			Vector_4V lastSample = V4LoadScalar32IntoSplatted(m_Envelope);
							Vector_4V *RESTRICT outputBuf = (Vector_4V*)m_Outputs[0].GetDataBuffer()->GetBuffer();

			// 1e-17
			const Vector_4V denormLimit = V4VConstant<0x233877AA,0x233877AA,0x233877AA,0x233877AA>();

			if(m_Inputs[kEnvelopeFollowerAttack].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC || m_Inputs[kEnvelopeFollowerRelease].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
				{
					// prevent divide by zero
					const Vector_4V attack = V4Max(vSmallValue, m_Inputs[kEnvelopeFollowerAttack].GetNormalizedValueV(i));
					const Vector_4V release = V4Max(vSmallValue, m_Inputs[kEnvelopeFollowerRelease].GetNormalizedValueV(i));

					const Vector_4V scaledAttack = V4InvScale(V4VConstant(V_ONE), V4Scale(attack,sampleRate));
					const Vector_4V scaledRelease = V4InvScale(V4VConstant(V_ONE), V4Scale(release,sampleRate));

					const Vector_4V input =m_Inputs[kEnvelopeFollowerSignal].GetDataFormat()==synthPin::SYNTH_PIN_DATA_NORMALIZED ? 
						m_Inputs[kEnvelopeFollowerSignal].GetNormalizedValueV(i) : 
					V4Abs(m_Inputs[kEnvelopeFollowerSignal].GetSignalValueV(i));

					const Vector_4V a = audDriverUtil::V4Pow(v0_01, scaledAttack);
					const Vector_4V r = audDriverUtil::V4Pow(v0_01, scaledRelease);

					// now need to process serially since each sample depends on prev sample
					const Vector_4V factorX = V4SelectFT(V4IsGreaterThanOrEqualV(lastSample, input), a, r);
					const Vector_4V outputX = V4SplatX(V4Add(V4Scale(factorX, V4Subtract(lastSample, input)), input));

					const Vector_4V factorY = V4SplatY(V4SelectFT(V4IsGreaterThanOrEqualV(outputX, input), a, r));
					const Vector_4V outputY = V4SplatY(V4Add(V4Scale(factorY, V4Subtract(outputX, input)), input));

					const Vector_4V factorZ = V4SplatZ(V4SelectFT(V4IsGreaterThanOrEqualV(outputY, input), a, r));
					const Vector_4V outputZ = V4SplatZ(V4Add(V4Scale(factorZ, V4Subtract(outputY, input)), input));

					const Vector_4V factorW = V4SplatW(V4SelectFT(V4IsGreaterThanOrEqualV(outputZ, input), a, r));
					const Vector_4V outputW = V4SplatW(V4Add(V4Scale(factorW, V4Subtract(outputZ, input)), input));			

					const Vector_4V outputXY = V4PermuteTwo<X1,X2,X1,X2>(outputX,outputY);
					const Vector_4V outputZW = V4PermuteTwo<X1,X2,X1,X2>(outputZ,outputW);

					const Vector_4V envelopeOutput = V4PermuteTwo<X1,Y1,X2,Y2>(outputXY,outputZW);

					// prevent denormal numbers by clamping very small values to 0
					const Vector_4V isGreaterMask = V4IsGreaterThanOrEqualV(envelopeOutput, denormLimit);

					*outputBuf++ = V4And(envelopeOutput,isGreaterMask);
					lastSample = outputW;
				}// for each sample

			}
			else
			{
				// static attack and release

				// Populate output buffer with input data
				if(m_Inputs[kEnvelopeFollowerSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC)
				{
					synthUtil::HoldSample(m_Buffer.GetBuffer(), m_Inputs[kEnvelopeFollowerSignal].GetStaticValue(), kMixBufNumSamples);
				}
				else
				{
					sysMemCpy128(m_Buffer.GetBuffer(), m_Inputs[kEnvelopeFollowerSignal].GetDataBuffer()->GetBuffer(), kMixBufNumSamples * sizeof(float));
				}
				

				// Need absolute value if dealing with signal format
				if(m_Inputs[kEnvelopeFollowerSignal].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
				{
					synthAbs::Process(m_Buffer.GetBuffer(), kMixBufNumSamples);
				}

				Process(m_Buffer.GetBuffer(), 
							m_Inputs[kEnvelopeFollowerAttack].GetNormalizedValue(0), 
							m_Inputs[kEnvelopeFollowerRelease].GetNormalizedValue(0), 
							m_State, 
							kMixBufNumSamples);

			}
					
			m_Envelope = GetX(lastSample);
		}
	}

	void synthEnvelopeFollower::Process(float *RESTRICT const inOutBuffer, const float inAttack, const float inRelease, Vector_4V_InOut state, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		
		// 1/48kHz
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const Vector_4V oneOverFs = V4VConstant<0x37AEC33E,0x37AEC33E,0x37AEC33E,0x37AEC33E>();
		// 0.01
		const Vector_4V v0_01 = V4VConstant<0x3C23D70A,0x3C23D70A,0x3C23D70A,0x3C23D70A>();
		// 0.00001f
		const Vector_4V vSmallValue = V4VConstant<0x3727C5AC,0x3727C5AC,0x3727C5AC,0x3727C5AC>();

		const Vector_4V attack = V4Max(vSmallValue, V4LoadScalar32IntoSplatted(inAttack));
		const Vector_4V release = V4Max(vSmallValue, V4LoadScalar32IntoSplatted(inRelease));

		const Vector_4V scaledAttack = V4InvScale(oneOverFs, attack);
		const Vector_4V scaledRelease = V4InvScale(oneOverFs, release);

		const Vector_4V a = audDriverUtil::V4Pow(v0_01, scaledAttack);
		const Vector_4V r = audDriverUtil::V4Pow(v0_01, scaledRelease);

		// 1e-17
		const Vector_4V denormLimit = V4VConstant<0x233877AA,0x233877AA,0x233877AA,0x233877AA>();

		Vector_4V lastSample = state;
		u32 count = numSamples >> 2;
		while(count--)
		{
			const Vector_4V input = *inOutPtr;

			// now need to process serially since each sample depends on prev sample
			const Vector_4V factorX = V4SelectFT(V4IsGreaterThanOrEqualV(lastSample, input), a, r);
			const Vector_4V outputX = V4SplatX(V4AddScaled(input, factorX, V4Subtract(lastSample, input)));

			const Vector_4V factorY = V4SplatY(V4SelectFT(V4IsGreaterThanOrEqualV(outputX, input), a, r));
			const Vector_4V outputY = V4SplatY(V4AddScaled(input, factorY, V4Subtract(outputX, input)));

			const Vector_4V factorZ = V4SplatZ(V4SelectFT(V4IsGreaterThanOrEqualV(outputY, input), a, r));
			const Vector_4V outputZ = V4SplatZ(V4AddScaled(input, factorZ, V4Subtract(outputY, input)));

			const Vector_4V factorW = V4SplatW(V4SelectFT(V4IsGreaterThanOrEqualV(outputZ, input), a, r));
			const Vector_4V outputW = V4SplatW(V4AddScaled(input, factorW, V4Subtract(outputZ, input)));

			const Vector_4V outputXY = V4PermuteTwo<X1,X2,X1,X2>(outputX,outputY);
			const Vector_4V outputZW = V4PermuteTwo<X1,X2,X1,X2>(outputZ,outputW);

			const Vector_4V envelopeOutput = V4PermuteTwo<X1,Y1,X2,Y2>(outputXY,outputZW);

			// prevent denormal numbers by clamping very small values to 0
			const Vector_4V isGreaterMask = V4IsGreaterThanOrEqualV(envelopeOutput,denormLimit);

			*inOutPtr++ = V4And(envelopeOutput, isGreaterMask);
			lastSample = outputW;

		}

		state = lastSample;
	}

	float synthEnvelopeFollower::Process(const float inputSignal, const float attack, const float release, Vector_4V_InOut state)
	{
		float prevEnvelope = GetX(state);
		const f32 input = Abs(inputSignal);

		const f32 effectiveSampleRate = 1.f / (f32(kMixBufNumSamples) / f32(kMixerNativeSampleRate));
		// prevent dbz
		const f32 attackTime = Max(0.00001f, attack);
		const f32 releaseTime = Max(0.00001f, release);
		const f32 a = Powf(0.01f, 1.0f/(attackTime * effectiveSampleRate)); 
		const f32 r = Powf(0.01f, 1.0f/(releaseTime * effectiveSampleRate)); 

		// envelope >= input ? release : attack
		const f32 factor = Selectf(input - prevEnvelope, a, r);

		// don't allow output to go denormal; clamp to 0
		const f32 unclampedVal = factor * (prevEnvelope - input) + input;
		prevEnvelope = unclampedVal <= 1e-17f ? 0.f : unclampedVal;
		SetX(state, prevEnvelope);
		return prevEnvelope;
	}
}
#endif

