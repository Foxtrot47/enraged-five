// 
// audiosynth/clipper.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "clipper.h"

#if !SYNTH_MINIMAL_MODULES

#include "clipperview.h"
#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthClipper);

	using namespace Vec;

	synthClipper::synthClipper()
	{
		m_Outputs[0].Init(this,"Output", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[kClipperSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kClipperSignal].SetConvertsInternally(false);
		m_Inputs[kClipperThreshold].Init(this, "Threshold", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kClipperThreshold].SetStaticValue(0.f);
		
		m_Mode = HARD_CLIP;
	}

	void synthClipper::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeEnumField("Mode", m_Mode);
	}

	void synthClipper::Synthesize()
	{
		const Vector_4V oneAndAHalf = V4VConstant<0x3FC00000,0x3FC00000,0x3FC00000,0x3FC00000>();

		m_Outputs[0].SetDataFormat(m_Inputs[kClipperSignal].GetDataFormat());

		if(m_Inputs[kClipperSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// static version
			m_Outputs[0].SetStaticValue(ComputeValue(m_Inputs[kClipperSignal].GetStaticValue()));				
		}
		else
		{
			// dynamic version
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			f32 *outputBuffer = m_Outputs[0].GetDataBuffer()->GetBuffer();

			const Vector_4V *inputBuffer = (Vector_4V*)m_Inputs[kClipperSignal].GetDataBuffer()->GetBuffer();

			switch(m_Mode)
			{
			case HARD_CLIP:
				for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
				{
					Vector_4V input = *inputBuffer++;

					Vector_4V unscaledInput = V4Abs(input);
					Vector_4V threshold = m_Inputs[kClipperThreshold].GetNormalizedValueV(i);

					Vector_4V scaledInput = V4Clamp(V4InvScale(unscaledInput, threshold), V4VConstant(V_ZERO), V4VConstant(V_ONE));

					Vector_4V unscaledOutput = scaledInput;
					Vector_4V sign = V4SelectFT(
						V4IsGreaterThanOrEqualV(input, V4VConstant(V_ZERO)), 
						V4VConstant(V_NEGONE), 
						V4VConstant(V_ONE));

					Vector_4V output = V4Scale(sign, V4Scale(unscaledOutput, threshold));
					
					*((Vector_4V*)outputBuffer) = output;
					outputBuffer += 4;
				}
				break;
			case SOFT_CLIP_POLY:
				for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
				{
					Vector_4V input = *inputBuffer++;

					Vector_4V unscaledInput = V4Abs(input);
					Vector_4V threshold = m_Inputs[kClipperThreshold].GetNormalizedValueV(i);

					Vector_4V scaledInput = V4Clamp(V4InvScale(unscaledInput, threshold), V4VConstant(V_ZERO), V4VConstant(V_ONE));

					Vector_4V scaledInputCubed = V4Scale(scaledInput, V4Scale(scaledInput,scaledInput));
										
					Vector_4V unscaledOutput = V4Subtract(V4Scale(oneAndAHalf, scaledInput), V4Scale(V4VConstant(V_HALF), scaledInputCubed));

					Vector_4V sign = V4SelectFT(
						V4IsGreaterThanOrEqualV(input, V4VConstant(V_ZERO)), 
						V4VConstant(V_NEGONE), 
						V4VConstant(V_ONE));

					Vector_4V output = V4Scale(sign, V4Scale(unscaledOutput, threshold));

					*((Vector_4V*)outputBuffer) = output;
					outputBuffer += 4;
				}
				break;
			}
				
		}
	}

	f32 synthClipper::ComputeValue(const f32 inputValue) const
	{
		const f32 max = m_Inputs[kClipperThreshold].GetNormalizedValue(0);
		switch(m_Mode)
		{
		case HARD_CLIP:
			{	
				return HardClip(inputValue, max);
			}
		
		case SOFT_CLIP_POLY:
			{			
				return SoftClip(inputValue, max);
			}
		default:
			Assert(0);
			return 0.f;
		}
	}

	void synthClipper::HardClip(float *RESTRICT inOutBuffer, const float *thresholdBuffer, const u32 numSamples)
	{
		const Vector_4V *thresholdPtr = reinterpret_cast<const Vector_4V*>(thresholdBuffer);
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		u32 count = numSamples >> 2;
		Vector_4V minThresh = V4VConstant<0x38D1B717,0x38D1B717,0x38D1B717,0x38D1B717>(); // 0.0001
		while(count--)
		{
			Vector_4V input = *inOutPtr;

			Vector_4V unscaledInput = V4Abs(input);
			Vector_4V threshold = V4Max(minThresh, *thresholdPtr++);

			Vector_4V scaledInput = V4Clamp(V4InvScale(unscaledInput, threshold), V4VConstant(V_ZERO), V4VConstant(V_ONE));

			Vector_4V unscaledOutput = scaledInput;
			Vector_4V sign = V4SelectFT(
				V4IsGreaterThanOrEqualV(input, V4VConstant(V_ZERO)), 
				V4VConstant(V_NEGONE), 
				V4VConstant(V_ONE));

			*inOutPtr++ = V4Scale(sign, V4Scale(unscaledOutput, threshold));			
		}
	}

	void synthClipper::HardClip(float *RESTRICT inOutBuffer, Vec::Vector_4V_In threshold, const u32 numSamples)
	{

		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		Vector_4V minThresh = V4VConstant<0x38D1B717,0x38D1B717,0x38D1B717,0x38D1B717>(); // 0.0001
		threshold = V4Max(threshold, minThresh);
		u32 count = numSamples >> 2;
		while(count--)
		{
			Vector_4V input = *inOutPtr;

			Vector_4V unscaledInput = V4Abs(input);
			Vector_4V scaledInput = V4Clamp(V4InvScale(unscaledInput, threshold), V4VConstant(V_ZERO), V4VConstant(V_ONE));

			Vector_4V unscaledOutput = scaledInput;
			Vector_4V sign = V4SelectFT(
				V4IsGreaterThanOrEqualV(input, V4VConstant(V_ZERO)), 
				V4VConstant(V_NEGONE), 
				V4VConstant(V_ONE));

			*inOutPtr++ = V4Scale(sign, V4Scale(unscaledOutput, threshold));			
		}
	}

	float synthClipper::HardClip(const float inputValue, const float threshold)
	{
		const f32 unscaledInput = Abs(inputValue);
		// rescale to 0,1 based on threshold
		const f32 input = Clamp(unscaledInput / Max(0.0001f, threshold), 0.f, 1.f);
		// rescale back
		return input * threshold * Sign(inputValue);
	}

	void synthClipper::SoftClip(float *RESTRICT inOutBuffer, const float *RESTRICT thresholdBuffer, const u32 numSamples)
	{
		const Vector_4V oneAndAHalf = V4VConstant<0x3FC00000,0x3FC00000,0x3FC00000,0x3FC00000>();
		const Vector_4V *thresholdPtr = reinterpret_cast<const Vector_4V*>(thresholdBuffer);
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);

		const Vector_4V minThresh = V4VConstant<0x38D1B717,0x38D1B717,0x38D1B717,0x38D1B717>(); // 0.0001

		u32 count = numSamples >> 2;
		while(count--)
		{
			Vector_4V input = *inOutPtr;

			Vector_4V unscaledInput = V4Abs(input);
			Vector_4V threshold = V4Max(minThresh, *thresholdPtr++);

			Vector_4V scaledInput = V4Clamp(V4InvScale(unscaledInput, threshold), V4VConstant(V_ZERO), V4VConstant(V_ONE));

			Vector_4V scaledInputCubed = V4Scale(scaledInput, V4Scale(scaledInput,scaledInput));

			Vector_4V unscaledOutput = V4Subtract(V4Scale(oneAndAHalf, scaledInput), V4Scale(V4VConstant(V_HALF), scaledInputCubed));

			Vector_4V sign = V4SelectFT(
				V4IsGreaterThanOrEqualV(input, V4VConstant(V_ZERO)), 
				V4VConstant(V_NEGONE), 
				V4VConstant(V_ONE));

			*inOutPtr++ = V4Scale(sign, V4Scale(unscaledOutput, threshold));
		}
	}

	void synthClipper::SoftClip(float *RESTRICT inOutBuffer, Vector_4V_In threshold, const u32 numSamples)
	{
		const Vector_4V oneAndAHalf = V4VConstant<0x3FC00000,0x3FC00000,0x3FC00000,0x3FC00000>();
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);

		const Vector_4V minThresh = V4VConstant<0x38D1B717,0x38D1B717,0x38D1B717,0x38D1B717>(); // 0.0001
		threshold = V4Max(minThresh, threshold);

		u32 count = numSamples >> 2;
		while(count--)
		{
			Vector_4V input = *inOutPtr;

			Vector_4V unscaledInput = V4Abs(input);
			
			Vector_4V scaledInput = V4Clamp(V4InvScale(unscaledInput, threshold), V4VConstant(V_ZERO), V4VConstant(V_ONE));

			Vector_4V scaledInputCubed = V4Scale(scaledInput, V4Scale(scaledInput,scaledInput));

			Vector_4V unscaledOutput = V4Subtract(V4Scale(oneAndAHalf, scaledInput), V4Scale(V4VConstant(V_HALF), scaledInputCubed));

			Vector_4V sign = V4SelectFT(
				V4IsGreaterThanOrEqualV(input, V4VConstant(V_ZERO)), 
				V4VConstant(V_NEGONE), 
				V4VConstant(V_ONE));

			*inOutPtr++ = V4Scale(sign, V4Scale(unscaledOutput, threshold));
		}
	}

	float synthClipper::SoftClip(const float inputValue, const float threshold)
	{
		const f32 unscaledInput = Abs(inputValue);
		// rescale to 0,1 based on threshold
		const f32 input = Clamp(unscaledInput / Max(0.0001f, threshold), 0.f, 1.f);
		const f32 unscaledOutput = 1.5f*input - 0.5f*input*input*input;
		// rescale back
		return unscaledOutput * threshold * Sign(inputValue);
	}
}
#endif

