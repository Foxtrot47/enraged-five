// 
// audiosynth/hardkneeview.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "hardknee.h"
#include "hardkneeview.h"
#include "oscillator.h"
#include "pinview.h"
#include "synthutil.h"

#include "vectormath/vectormath.h"

#if __SYNTH_EDITOR

namespace rage 
{
	using namespace Vec;

	synthHardKneeView::synthHardKneeView(synthModule *module) : synthModuleView(module)
	{
		m_CurveView.SetAlwaysRefresh(true);
		
		m_CurveView.SetGridSize(10,4);
		m_CurveView.SetProvider(this);

		AddComponent(&m_CurveView);
	}

	synthHardKneeView::~synthHardKneeView()
	{
	
	}

	void synthHardKneeView::Update()
	{
		Matrix34 mtx = GetMatrix();
		if(!IsMinimised())
		{
			mtx.d += mtx.b * 1.5f;
		}

		Vector3 scalingVec(2.f,0.9f,1.f);
		mtx.Transform3x3(scalingVec);
		mtx.Scale(scalingVec);

		m_CurveView.SetMatrix(mtx);

		m_CurveView.SetDrawGrid(!IsMinimised());
		if(!IsMinimised())
		{
			u32 numInputs;
			synthPin *inputs;
			GetHardKnee()->GetInputs(inputs, numInputs);
			float gridAlpha = GetHoverFade();
			for(u32 i = 0; i < numInputs; i++)
			{
				gridAlpha = Max(gridAlpha, inputs[i].GetView()->GetHoverFade());
			}
			const Color32 color(0.75f, 0.75f, 0.75f, 0.5f * gridAlpha);

			m_CurveView.SetGridColor(color);
		}
		m_CurveView.SetAlpha(1.f);

		synthModuleView::Update();
	}

	void synthHardKneeView::Evaluate(float *destBuffer, const u32 numSamples) const
	{
		// rasterize the evaluated curve
		Vector_4V state = V4VConstant(V_ZERO);

#if !__ASSERT && __WIN32PC
		(void) numSamples;	// keep the compiler happy
#endif
		Assert(numSamples == kMixBufNumSamples);
		CompileTimeAssert(kMixBufNumSamples == 256);
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		// kMixerNativeSampleRate / kMixBufNumSamples = 187.5hz
		const Vector_4V freq = V4VConstant<0x433B8000,0x433B8000,0x433B8000,0x433B8000>();
		synthDigitalOscillator::GenerateRamp(destBuffer, freq, state, kMixBufNumSamples);

		if(GetHardKnee()->GetInput(1).GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			ALIGNAS(16) float kneeBuf[kMixBufNumSamples] ;
			sysMemCpy(kneeBuf, GetHardKnee()->GetInput(1).GetDataBuffer()->GetBuffer(), sizeof(float) * kMixBufNumSamples);
			if(GetHardKnee()->GetInput(1).GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
			{
				synthUtil::ConvertBufferToNormalised(kneeBuf, kMixBufNumSamples);
			}
			synthHardKnee::Process(destBuffer, kneeBuf, kMixBufNumSamples);
		}
		else
		{
			// Static knee
			Vector_4V knee = GetHardKnee()->GetInput(1).GetNormalizedValueV(0);
			synthHardKnee::Process(destBuffer, knee, kMixBufNumSamples);
		}
	}

	void synthHardKneeView::Render()
	{	
		synthModuleView::Render();		
	}

} // namespace rage
#endif // __SYNTH_EDITOR
