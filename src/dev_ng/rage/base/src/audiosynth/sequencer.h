// 
// audiosynth/sequencer.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SEQUENCER_H
#define SYNTH_SEQUENCER_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "module.h"
#include "modulefactory.h"
#include "pin.h"
#include "synthdefs.h"

#include "math/amath.h"

namespace rage
{
	enum {kMaxDivisions = 16};
	enum {kSequencerMaxDivisions = 16};
	enum {kSequencerNumOutputs = kSequencerMaxDivisions + 1};
	
	enum synthSequencerInputs
	{
		kSequencerTempo,
		kSequencerDuration,
		kSequencerTrigger,
		
		kSequencerNumInputs
	};

	class synthSequencer : public synthModuleBase<kSequencerNumInputs, kSequencerNumOutputs, SYNTH_SEQUENCER>
	{
	public:
		SYNTH_MODULE_NAME("Sequencer");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthSequencer();

		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void GetOutputs(synthPin *&pins, u32 &numPins);
		virtual void Synthesize();

		void SetBpmRange(const f32 min, const f32 max)
		{
			m_MinBpm = min;
			m_MaxBpm = max;
		}
		f32 GetMinBpm() const { return m_MinBpm; }
		f32 GetMaxBpm() const { return m_MaxBpm; }

		
		void SetDivisions(const u32 divisions)
		{
			m_Divisions = Min<u32>(divisions, kSequencerMaxDivisions);
		}
		u32 GetDivisions() const { return m_Divisions; }

	private:

		u32 m_Divisions;
		f32 m_MinBpm;
		f32 m_MaxBpm;
		f32 m_BeatClock;
		bool m_IsActive;
};

} // namespace rage
#endif
#endif // SYNTH_SEQUENCER_H
