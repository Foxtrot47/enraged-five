
#include "inputsmoother.h"
#include "audiohardware/mixer.h"
#include "variableinputview.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthVariableInput);
	synthVariableInput::synthVariableInput()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetStaticValue(1.f);
		m_Outputs[0].SetStaticValue(1.f);
		
		m_ExportedKey = 0;
		SYNTH_EDITOR_ONLY(m_ExportedKeyName = NULL);
	}

	synthVariableInput::~synthVariableInput()
	{
#if __SYNTH_EDITOR
		if(m_ExportedKeyName)
		{
			StringFree(m_ExportedKeyName);
		}
#endif
	}

	void synthVariableInput::SetValue(const f32 val)
	{
		m_Inputs[0].SetStaticValue(val);
		SYNTH_EDITOR_ONLY(((synthVariableInputView*)GetView())->Notify_SetValue());
	}

	void synthVariableInput::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("ExportedKey", m_ExportedKey);
	}

	void synthVariableInput::SetExportedKey(const char *keyName)
	{
		m_ExportedKey = atStringHash(keyName);
#if __SYNTH_EDITOR
		if(m_ExportedKeyName)
		{
			StringFree(m_ExportedKeyName);
		}
		m_ExportedKeyName = StringDuplicate(keyName);
#endif
	}

	void synthVariableInput::Synthesize()
	{
		m_Outputs[0].SetStaticValue(m_Inputs[0].GetNormalizedValue(0));
	}
}

