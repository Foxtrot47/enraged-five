#include "junction.h"
#include "junctionview.h"
#include "synthdefs.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthJunctionPin);

	synthJunctionPin::synthJunctionPin()
	{
		m_Inputs[0].Init(this,"In", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[0].SetConvertsInternally(false);
		m_Outputs[0].Init(this,"Out", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
	}

	void synthJunctionPin::Synthesize()
	{
		m_Outputs[0].SetDataFormat(m_Inputs[0].GetDataFormat());
		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_Outputs[0].SetStaticValue(m_Inputs[0].GetStaticValue());
		}
		else
		{
			m_Outputs[0].SetDataBuffer(m_Inputs[0].GetDataBuffer());
		}
	}
}

