#include "synthdefs.h"
#include "pin.h"
#include "pinview.h"

namespace rage
{

	const char *synthPin::sm_PinNumberStrings[16] = 
	{
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		"10",
		"11",
		"12",
		"13",
		"14",
		"15",
		"16",
	};

	synthPin::~synthPin()
	{
#if __SYNTH_EDITOR
		if(IsConnected())
		{
			Disconnect();
		}

		if(m_View)
		{
			delete m_View;
			m_View = NULL;
		}
#endif
	}


#if __SYNTH_EDITOR
	bool synthPin::AllocateView()
	{
		m_View = rage_new synthPinView(this);
		return (m_View != NULL);
	}
#endif

	void synthPin::NotifyConnection(synthPin *SYNTH_EDITOR_ONLY(otherPin))
	{
#if __SYNTH_EDITOR
		GetView()->OnConnection(otherPin);
#endif
	}

	void synthPin::NotifyDisconnection(synthPin *SYNTH_EDITOR_ONLY(otherPin))
	{
#if __SYNTH_EDITOR
        if(HasView())
        {
            GetView()->OnDisconnection(otherPin);
        }
#endif
	}
}