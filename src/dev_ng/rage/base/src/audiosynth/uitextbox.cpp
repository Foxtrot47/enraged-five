// 
// audiosynth/uitextbox.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 
#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uitextbox.h"
#include "uiplane.h"

#include "input/keyboard.h"
#include "input/keys.h"
#include "audioengine/engine.h"

#if __WIN32PC
#include "system/xtl.h"
#endif

namespace rage {

#if __WIN32PC
	extern HWND g_hwndMain;
#endif

	synthUITextBox::synthUITextBox()
	{
		m_LineHeight = 0.25f;
		m_WidthInChars = 32;
		m_MouseMode = kDraggingMode;

		m_EditIndex = 0;
		m_CaretChar = 0;
		m_CaretLine = 0;

		m_IsEmpty = true;
		m_WantsEdit = false;

		sysMemZeroBytes<sizeof(m_TextBuffer)>(m_TextBuffer);
		safecpy(m_TextBuffer, "Double-click to enter text ...");
		ComputeLabelStrings();

		m_FirstFrame = true;
	}

	void synthUITextBox::SetString(const char *str)
	{
		safecpy(m_TextBuffer, str);
		ComputeLabelStrings();
		m_IsEmpty = false;
		m_EditIndex = (s32)strlen(m_TextBuffer);
	}

	void synthUITextBox::ComputeLabelStrings()
	{
		s32 currentLine = 0;
		s32 currentChar = 0;
		const s32 widthInChars = GetWidthInChars();
		s32 textBufferIndex = 0;
		const s32 stringLength = (s32)strlen(m_TextBuffer);
		s32 lineBufferIndex = 0;
		const char *lastLinePtr = m_LineBuffer;

		while(textBufferIndex < stringLength)
		{
			// update caret
			if(textBufferIndex <= m_EditIndex)
			{
				m_CaretLine = currentLine;
				m_CaretChar = currentChar;
			}
			// time for a new line?
			if(currentChar >= widthInChars || m_TextBuffer[textBufferIndex] == '\n')
			{
				if(m_Labels.GetCount() <= currentLine)
				{
					// allocate new label
					m_Labels.Grow() = rage_new synthUILabel(m_WidthInChars * 15);
				}
				m_LineBuffer[lineBufferIndex++] = 0;

				m_Labels[currentLine]->SetString(lastLinePtr);

				lastLinePtr = m_LineBuffer + lineBufferIndex;
				currentChar = 0;
				currentLine++;
				if(m_TextBuffer[textBufferIndex] == '\n')
				{
					textBufferIndex++;
				}
			}
			else
			{
				m_LineBuffer[lineBufferIndex++] = m_TextBuffer[textBufferIndex++];
				currentChar++;
			}
		}
		if(m_EditIndex == stringLength)
		{
			m_CaretChar = currentChar;
			m_CaretLine = currentLine;
		}
		if(strlen(lastLinePtr))
		{
			if(m_Labels.GetCount() <= currentLine)
			{
				// allocate new label
				m_Labels.Grow(1) = rage_new synthUILabel(m_WidthInChars * 15);
			}
			m_LineBuffer[lineBufferIndex++] = 0;
			m_Labels[currentLine++]->SetString(lastLinePtr);
		}
		for(s32 i = currentLine; i < m_Labels.GetCount(); i++)
		{
			delete m_Labels[i];
		}
		m_Labels.Resize(currentLine);
	}

	void synthUITextBox::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		const Matrix34 &mat = GetMatrix();
		const s32 numLines = Max(1, m_Labels.GetCount());
		const f32 halfHeight = numLines * m_LineHeight * 0.5f;
		const f32 halfWidth = 1.35f;

		Vector3 offsetV = mat.a * halfWidth + mat.b * halfHeight;
		
		v1 = mat.d - offsetV;
		v2 = mat.d + offsetV;

		/*for(s32 i = 0; i < m_Labels.GetCount(); i++)
		{
			Vector3 v3,v4;
			m_Labels[i]->GetBounds(v3,v4);
			v1.Min(v1, v3);
			v2.Max(v2, v4);
		}*/
	}

	void synthUITextBox::Update()
	{

		
		if(IsUnderMouse() || m_WantsEdit)
		{
			// under mouse - double click to enter edit mode
			if(synthUIPlane::GetMaster()->HadLeftDoubleClick() || m_WantsEdit)
			{
				m_WantsEdit = false;
				if(m_MouseMode != kEditMode)
				{
					if(!synthUIPlane::IsKeyboardLocked())
					{
						synthUIPlane::SetIsKeyboardLocked(true);
						m_MouseMode = kEditMode;
						// flush keyboard buffer
						ioKeyboard::GetBufferedInput(NULL,0);
						if(m_IsEmpty)
						{
							sysMemZeroBytes<sizeof(m_TextBuffer)>(m_TextBuffer);
							m_EditIndex = 0;
							ComputeLabelStrings();
						}
					}
				}
			}
		}
		else
		{
			// not under mouse - single click to exit edit mode
			if(!m_FirstFrame && (synthUIPlane::GetMaster()->HadLeftClick() ||
				synthUIPlane::GetMaster()->HadLeftDoubleClick()))
			{
				if(m_MouseMode == kEditMode)
				{
					synthUIPlane::SetIsKeyboardLocked(false);
					if(strlen(m_TextBuffer) == 0)
					{
						sysMemZeroBytes<sizeof(m_TextBuffer)>(m_TextBuffer);
						safecpy(m_TextBuffer, "Click to enter text ...");
						ComputeLabelStrings();
						m_IsEmpty = true;
					}
					else
					{
						m_IsEmpty = false;
					}
					m_MouseMode = kDraggingMode;
				}
			}
		}
			
		m_FirstFrame = false;
		if(m_MouseMode == kEditMode)
		{
			// act on key presses to update text buffer
#if __WIN32PC
#ifndef MAPVK_VSC_TO_VK
#define MAPVK_VSC_TO_VK 1
#endif
			u8 keyboardBuffer[128];
			const s32 numKeys = ioKeyboard::GetBufferedInput((char*)keyboardBuffer, sizeof(keyboardBuffer));
			for(s32 i = 0; i < numKeys; i++)
			{
				u32 scanCode = keyboardBuffer[i];
				u32 virtualKeyCode = ::MapVirtualKey(scanCode, MAPVK_VSC_TO_VK);

				if(virtualKeyCode == VK_BACK)
				{
					if(m_EditIndex >= 1)
					{
						m_EditIndex--;
						const s32 originalLength = (s32)strlen(m_TextBuffer);
						for(s32 k = m_EditIndex; k < originalLength; k++)
						{
							m_TextBuffer[k] = m_TextBuffer[k+1];
						}
					}
				}
				else if(scanCode == KEY_HOME)
				{
					m_EditIndex = 0;
				}
				else if(scanCode == KEY_END)
				{
					m_EditIndex = (s32)strlen(m_TextBuffer);
				}
				else if(scanCode == KEY_DELETE)
				{
					const s32 originalLength = (s32)strlen(m_TextBuffer);
					if(m_EditIndex < originalLength)
					{
						for(s32 k = m_EditIndex; k < originalLength; k++)
						{
							m_TextBuffer[k] = m_TextBuffer[k+1];
						}
					}
				}
				else if(scanCode == KEY_LEFT)
				{
					if(m_EditIndex > 0)
					{
						m_EditIndex--;
					}
				}
				else if(scanCode == KEY_RIGHT)
				{
					if((u32)m_EditIndex < strlen(m_TextBuffer))
					{
						m_EditIndex++;
					}
				}
				else
				{
					char character = 0;
					if(virtualKeyCode == VK_RETURN)
					{
						character = '\n';
					}
					else if((u32)m_EditIndex < sizeof(m_TextBuffer) - 1)
					{					
						u16 c[2];
						BYTE kbState[256];
						::GetKeyboardState(&kbState[0]);
						if(::ToAscii(virtualKeyCode, scanCode, &kbState[0], (LPWORD)&c, 0))
						{
							character = (char)c[0];
						}			
					}
					// Check for printable ascii character
					if(IsCharacterValid(character))
					{
						const s32 originalLength = (s32)strlen(m_TextBuffer);
						for(s32 k = originalLength; k >= m_EditIndex; k--)
						{
							m_TextBuffer[k+1] = m_TextBuffer[k];
						}
						m_TextBuffer[m_EditIndex++] = character;
					}
				}
			}
			
#endif
		
			ComputeLabelStrings();
		}

		// set up line positions
		Vector3 v1,v2;
		GetBounds(v1,v2);
		const Matrix34 &mat = GetMatrix();
		const f32 height = mat.b.Dot(v2-v1);
		const Vector3 perLineVec = mat.b * (height / (f32)m_Labels.GetCount());
	
		Matrix34 tm = mat;
		tm.d = v2;
		for(s32 i = 0; i < m_Labels.GetCount(); i++)
		{
			m_Labels[i]->SetMatrix(tm);
			m_Labels[i]->SetSize(0.1f);
			m_Labels[i]->Update();
			
			const Vector3 pos = tm.d;
			// left justify position
			
			f32 width, height;
			m_Labels[i]->ComputeWidthAndHeight(width,height);
			tm.d -= width * 0.5f * tm.a;
			tm.d -= height * 0.5f * tm.b;
			m_Labels[i]->SetMatrix(tm);
			
			tm.d = pos;
			tm.d -= perLineVec;
		}
	}
	
	bool synthUITextBox::IsCharacterValid(const char c) const
	{
		return	c >= 32 &&
				c <= 126 &&
				c != '<' &&
				c != '>' &&
				c != '&';
	}

	void synthUITextBox::Render()
	{
		const bool blinkingOn = ((g_AudioEngine.GetTimeInMilliseconds() % 900) < 650);

		for(s32 i = 0; i < m_Labels.GetCount(); i++)
		{			
			if(i == m_CaretLine && m_MouseMode == kEditMode)
			{
				/*Vector3 v1,v2;
				m_Labels[i]->GetBounds(v1,v2);
				grcDrawBox(v1,v2,Color32(1.f,0.f,0.f,1.f));*/

				m_Labels[i]->SetDrawCaret(blinkingOn);
				m_Labels[i]->SetCaretPosition(m_CaretChar);
			}
			else
			{
				m_Labels[i]->SetDrawCaret(false);
			}
			m_Labels[i]->Draw();
		}
	}

} // namespace rage

#endif // __SYNTH_EDITOR
