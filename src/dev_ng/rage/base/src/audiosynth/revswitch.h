// 
// audiosynth/revswitch.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_REVSWITCH_H
#define SYNTH_REVSWITCH_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "synthdefs.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "atl/array.h"
#include "math/amath.h"

namespace rage
{
	class synthReverseSwitch : public synthModuleBase<2,16,SYNTH_REVERSE_SWITCH>
	{
	public:

		SYNTH_MODULE_NAME("ReverseSwitch");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthReverseSwitch();
		
		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();

		virtual void GetOutputs(synthPin *&pins, u32 &numPins);

		enum { kMaxSwitchOutputs = 16};
		enum synthRevSwitchInputs
		{
			SIGNAL = 0,
			SWITCH,

			kReverseSwitchNumInputs
		};

		CompileTimeAssert((s32)kReverseSwitchNumInputs == (s32)NumInputs);
		CompileTimeAssert((s32)kMaxSwitchOutputs == (s32)NumOutputs);

		u32 GetNumSwitchOutputs() const { return m_NumSwitchOutputs; }
		void SetNumSwitchOutputs(const u32 numValues) { m_NumSwitchOutputs = Clamp(numValues, 1U, (u32)kMaxSwitchOutputs); }

		bool IsNormalizedInput() const { return m_IsNormalizedInput; }
		void SetNormalizedInput(const bool is) { m_IsNormalizedInput = is; }

		u32 GetActiveOutputIndex() const;
	
	private:

		u32 m_NumSwitchOutputs;
		bool m_IsNormalizedInput;
	};
}
#endif
#endif // SYNTH_REVSWITCH_H



