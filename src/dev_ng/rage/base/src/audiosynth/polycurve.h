// 
// audiosynth/polycurve.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_POLYCURVE_H
#define SYNTH_POLYCURVE_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"
#include "synthdefs.h"

namespace rage
{
	class synthPolyCurve : public synthModuleBase<1,1,SYNTH_POLYCURVE>
	{
	public:
		SYNTH_MODULE_NAME("PolyCurve");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthPolyCurve();
		
		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();

		f32 EvaluatePolynomial(const f32 x) const;

		f32 *GetCoefficients() { return &m_Coefficients[0]; }
		enum {kMaxCoefficients = 8};
	private:
	
		f32 m_Coefficients[kMaxCoefficients];		
		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_POLYCURVE_H


