// 
// audiosynth/note2freq.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "note2freq.h"
#include "note2freqview.h"
#include "synthutil.h"
#include "audiohardware/driverutil.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	SYNTH_EDITOR_VIEW(synthNoteToFrequency);
	
	synthNoteToFrequency::synthNoteToFrequency()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetStaticValue(0.f);
		m_Outputs[0].SetStaticValue(0.f);
	}

	void synthNoteToFrequency::Synthesize()
	{
		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_Outputs[0].SetStaticValue(Process(m_Inputs[0].GetNormalizedValue(0)));
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			sysMemCpy(m_Buffer.GetBuffer(), m_Inputs[0].GetDataBuffer()->GetBuffer(), m_Buffer.GetSize() * sizeof(float));

			if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
			{
				synthUtil::ConvertBufferToNormalised(m_Buffer.GetBuffer(), m_Buffer.GetSize());
			}
			Process(m_Buffer.GetBuffer(), m_Buffer.GetSize());
		}
	}

	float synthNoteToFrequency::Process(const float noteInput)
	{
		return powf(2.f, (noteInput+36.3763f) * (1.f/12.f));
	}
	
	void synthNoteToFrequency::Process(float *RESTRICT inOut, const rage::u32 numSamples)
	{
		u32 count = numSamples>>2;
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOut);
		
		const Vector_4V noteOffset = V4VConstant<0x42118155,0x42118155,0x42118155,0x42118155>();
		const Vector_4V oneTwelfth = V4VConstant<0x3DAAAAAB,0x3DAAAAAB,0x3DAAAAAB,0x3DAAAAAB>();

		while(count--)
		{
			*inOutPtr = audDriverUtil::V4Exp2(V4Scale(V4Add(*inOutPtr, noteOffset), oneTwelfth));
			inOutPtr++;
		}
	}
} // namespace rage

