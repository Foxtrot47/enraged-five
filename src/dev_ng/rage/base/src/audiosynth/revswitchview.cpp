// 
// audiosynth/revswitchview.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "revswitchview.h"

#include "revswitch.h"
#include "moduleview.h"
#include "pinview.h"
#include "uiplane.h"

#include "module.h"
#include "pin.h"

namespace rage
{
	synthReverseSwitchView::synthReverseSwitchView(synthModule *module) : synthModuleView(module)
	{
		m_ModeSwitch.AddItem("0-1 Input");
		m_ModeSwitch.AddItem("0-n Input");
		AddComponent(&m_ModeSwitch);
		m_ModeSwitch.SetCurrentIndex(GetSwitch()->IsNormalizedInput() ? 0 : 1);

		AddComponent(&m_Divisions);
		m_Divisions.SetRange(1.f, (f32)synthReverseSwitch::kMaxSwitchOutputs);
		m_Divisions.SetNormalizedValue(GetSwitch()->GetNumSwitchOutputs() / (f32)synthReverseSwitch::kMaxSwitchOutputs);
	}

	void synthReverseSwitchView::Update()
	{
		// fade out title when editing
		SetTitleAlpha(1.f - GetHoverFade());
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		m_Divisions.SetShouldDraw(ShouldDraw() && !IsMinimised());

		Matrix34 mtx = GetMatrix();

		m_Divisions.SetMatrix(mtx);
		m_Divisions.SetWidth(0.4f);
		m_Divisions.SetAlpha(GetHoverFade() * GetAlpha());
		m_Divisions.SetTitle("SwitchOutputs");
		m_Divisions.SetTextAlpha(GetHoverFade() * GetAlpha());
		m_Divisions.SetRenderAsInteger(true);

		mtx.d -= mtx.b * m_Divisions.ComputeHeight() * 1.f;
		m_ModeSwitch.SetMatrix(mtx);
		m_ModeSwitch.SetAlpha(GetHoverFade() * GetAlpha());

		GetSwitch()->SetNormalizedInput(m_ModeSwitch.GetCurrentIndex() == 0);
		SetHideTitle(m_ModeSwitch.IsShowingMenu());

		const u32 divisions = (u32)m_Divisions.GetValue();
		GetSwitch()->SetNumSwitchOutputs(divisions);

		// check for any outputs that now invalid but are still connected
		for(u32 i = divisions; i < synthReverseSwitch::kMaxSwitchOutputs; i++)
		{
			atArray<synthPin *> &destinations = GetSwitch()->GetOutput(i).GetView()->GetOutputDestinations();
			for(s32 k = 0; k < destinations.GetCount(); k++)
			{
				Assert(destinations[k]->GetOtherPin() == &GetSwitch()->GetOutput(i));
				destinations[k]->Disconnect();
			}
		}

		synthModuleView::Update();
	}

	void synthReverseSwitchView::Render()
	{
		synthModuleView::Render();

		Vector3 inputPinPos = GetSwitch()->GetInput(synthReverseSwitch::SIGNAL).GetView()->GetMatrix().d;
		inputPinPos.z -= 0.1f;

		const u32 activeOutputIndex = GetSwitch()->GetActiveOutputIndex();

		Vector3 pinPos = GetSwitch()->GetOutput(activeOutputIndex).GetView()->GetMatrix().d;
		pinPos.z -= 0.1f;

		synthModuleView::RenderWire(synthUIPlane::GetMaster()->GetMatrix(), 
			inputPinPos,
			pinPos,			
			GetSwitch()->GetInput(synthReverseSwitch::SIGNAL).GetView()->GetPeakValue(),
			Color32(0.f,0.f,1.f,1.f), 
			true);

	}

	bool synthReverseSwitchView::StartDrag() const
	{
		if(m_Divisions.IsUnderMouse() || m_ModeSwitch.IsUnderMouse() || m_ModeSwitch.IsShowingMenu())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthReverseSwitchView::IsActive() const
	{
		return (IsUnderMouse() || m_Divisions.IsActive());
	}

	u32 synthReverseSwitchView::GetModuleHeight() const
	{
		return Max(4U, synthModuleView::GetModuleHeight());
	}
}
#endif // __SYNTH_EDITOR

