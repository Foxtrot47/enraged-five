// 
// audiosynth/numericinputbox.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_NUMERICINPUTBOX_H
#define SYNTH_NUMERICINPUTBOX_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uiview.h"
#include "uilabel.h"
#include "vector/vector3.h"

namespace rage
{
	class synthUITextBoxInput : public synthUIView
	{
	public:

		synthUITextBoxInput();
		virtual ~synthUITextBoxInput();

		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		
		bool IsActive() const { return ShouldDraw(); }

		void SetTitle(const char *title)
		{
			m_DrawTitle = (title != NULL);
			m_TitleLabel.SetString(title);
		}

		
		virtual bool StartDrag() const { return false; }

		bool HadEnter() const { return m_HadEnter; }
		const char *GetKeyboardBuffer() { return &m_KeyboardBuffer[0]; }
		void Reset();

		void SetValue(const char *val);

		void SetAllowAlphaChars(const bool allow) { m_AllowAlphaChars = allow; }

	protected:
		virtual void Render();

	private:

		enum {kKeyboardBufferSize = 96};
		char m_KeyboardBuffer[kKeyboardBufferSize];
		u32 m_KeyboardIndex;
		synthUILabel m_InputLabel;

		synthUILabel m_TitleLabel;
		
		Color32 m_HighlightColour;

		bool m_InputEnabled;
		bool m_HadEnter;
		bool m_DrawTitle;
		bool m_DrawReflection;
		bool m_AllowAlphaChars;
	};
};
#endif // __SYNTH_EDITOR
#endif // SYNTH_NUMERICINPUTBOX_H
