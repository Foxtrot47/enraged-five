#include "audioinput.h"
#include "audioinputview.h"
#include "synthdefs.h"

#include "audiohardware/framebufferpool.h"
#include "audiohardware/mixer.h"
#include "system/memops.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthAudioInput);

	synthAudioInput::synthAudioInput() : m_Buffer(false)
	{
		m_Outputs[0].Init(this,"Signal", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetStaticValue(0.f);
		m_InputId = 0;
		m_BufferId = 0;
	}

	void synthAudioInput::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("InputId", m_InputId);
	}

	void synthAudioInput::Synthesize()
	{
		m_Buffer.SetBufferId(m_BufferId);
		m_Outputs[0].SetDataBuffer(&m_Buffer);
	}
}

