// 
// audiosynth/polycurveview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_POLYCURVEVIEW_H
#define SYNTH_POLYCURVEVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uiscrollbar.h"

namespace rage
{
	class grcTexture;
	class synthPolyCurve;
	class synthPolyCurveView : public synthModuleView
	{
	public:

		synthPolyCurveView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();
		virtual u32 GetModuleHeight() const { return 4; }

	private:

		void ComputeCoefficients();
		bool UpdateEdit(const bool checkBoundsOnly);

		synthPolyCurve *GetPolyCurve() { return (synthPolyCurve*)m_Module; }
	
		enum {kMaxPoints = 64};
		f32 m_Points[kMaxPoints];

		

		synthUIScrollbar m_Order;
		grcTexture *m_DrawnTexture;
		grcTexture *m_FitTexture;

		s32 m_LastEditedPoint;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_POLYCURVEVIEW_H

