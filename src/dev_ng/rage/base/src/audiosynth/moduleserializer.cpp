// 
// audiosynth/moduleserializer.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "module.h"
#include "moduleserializer.h"
#include "pin.h"
#include "synthobjects.h"
#include "file/stream.h"
#include "math/amath.h" 

namespace rage {

	void synthBinaryModuleReader::Read(const ModularSynth::tModule *module)
	{
		m_ModuleData = module;
		const u32 *ptr = (const u32*)&module->FieldValue[module->numFieldValues];
		Assign(m_NumInputPins, *ptr);
		ptr++;
		m_InputPins = (const ModularSynth::tModule::tInputPin *)ptr;
		ptr = (const u32*)&m_InputPins[m_NumInputPins];
		Assign(m_NumOutputPins, *ptr);
		ptr++;
		m_OutputPins = (const ModularSynth::tModule::tOutputPin *)ptr;
	}
	
	u32 synthBinaryModuleReader::GetSizeBytes() const
	{
		return m_ModuleData->numFieldValues * sizeof(ModularSynth::tModule::tFieldValue) +
			m_NumInputPins * sizeof(ModularSynth::tModule::tInputPin) + 
			m_NumOutputPins * sizeof(ModularSynth::tModule::tOutputPin) +
			12 + // 3*4 bytes for numFields,numInputPins and numOutputPins
			28; // nasty magic number comes from:
		//rage::u32 TypeId;
		//rage::f32 IsCollapsed;
		//rage::f32 PosX;
		//rage::f32 PosY;
		//rage::u32 PresetList;
		//rage::s32 ProcessingStage;
		//rage::s32 GroupId;
	}
	
	synthBinaryReader::synthBinaryReader(const ModularSynth *const instanceData)
		: 
		m_InstanceData(instanceData),
		m_CurrentPtr(NULL),
		m_CurrentIndex(0),
		m_CurrentCount(0),
		m_State(kModules)
	{
		Reset();
	}

	u32 synthBinaryReader::GetNumModules()
	{
		Assert(m_State == kModules);
		const u32 ret = m_CurrentCount;
		if(ret == 0)
		{
			// there are no modules; move directly onto variables
			m_State = kVariables;
			m_CurrentCount = *m_CurrentPtr++;
		}
		return ret;
	}
	
	void synthBinaryReader::GetNextModule(synthBinaryModuleReader &moduleReader)
	{
		Assert(m_State == kModules);
		Assert(m_CurrentIndex < m_CurrentCount);
		
		const ModularSynth::tModule *module = (const ModularSynth::tModule*)m_CurrentPtr;

		// calculate size of module
		moduleReader.Read(module);

		m_CurrentPtr += moduleReader.GetSizeBytes();

		m_CurrentIndex++;
		if(m_CurrentIndex == m_CurrentCount)
		{
			m_State = kVariables;
			m_CurrentIndex = 0;
			m_CurrentCount = *m_CurrentPtr++;
		}
	}

	u32 synthBinaryReader::GetNumGroups()
	{
		Assert(m_State == kGroups);
		const u32 ret = m_CurrentCount;
		if(ret == 0)
		{
			// there are no groups; move directly onto comments
			m_State = kComments;
			m_CurrentCount = *m_CurrentPtr++;
		}
		return ret;
	}

	const ModularSynth::tModuleGroup *synthBinaryReader::GetNextGroup()
	{
		Assert(m_State == kGroups);
		Assert(m_CurrentIndex < m_CurrentCount);

		const ModularSynth::tModuleGroup *group = (ModularSynth::tModuleGroup *)m_CurrentPtr;
		
		const u32 sizeOfGroup = sizeof(ModularSynth::tModuleGroup);
		m_CurrentPtr += sizeOfGroup;
		m_CurrentIndex++;
		if(m_CurrentIndex == m_CurrentCount)
		{
			m_State = kComments;
			m_CurrentCount = *m_CurrentPtr++;
			m_CurrentIndex = 0;
		}
		return group;
	}

	u32 synthBinaryReader::GetNumComments()
	{
		Assert(m_State == kComments);
		const u32 ret = m_CurrentCount;
		if(ret == 0)
		{
			// there are no comments; move directly onto cameras
			m_State = kCameras;
			m_CurrentCount = *m_CurrentPtr++;
		}
		return ret;
	}

	const ModularSynth::tComments *synthBinaryReader::GetNextComment()
	{
		Assert(m_State == kComments);
		Assert(m_CurrentIndex < m_CurrentCount);

		const ModularSynth::tComments *comment = (ModularSynth::tComments *)m_CurrentPtr;

		const u32 sizeOfComment = sizeof(ModularSynth::tComments);
		
		m_CurrentPtr += sizeOfComment;
		m_CurrentIndex++;
		if(m_CurrentIndex == m_CurrentCount)
		{
			m_State = kCameras;
			m_CurrentCount = *m_CurrentPtr++;
			m_CurrentIndex = 0;
		}
		
		return comment;
	}

	u32 synthBinaryReader::GetNumCameras()
	{
		Assert(m_State == kCameras);
		const u32 ret = m_CurrentCount;
		return ret;
	}

	const ModularSynth::tCameraPosition *synthBinaryReader::GetNextCamera()
	{
		Assert(m_State == kCameras);
		Assert(m_CurrentIndex < m_CurrentCount);

		const ModularSynth::tCameraPosition *camera = (ModularSynth::tCameraPosition *)m_CurrentPtr;

		const u32 sizeOfComment = sizeof(ModularSynth::tCameraPosition);

		m_CurrentPtr += sizeOfComment;
		m_CurrentIndex++;
		
		return camera;
	}

	u32 synthBinaryReader::GetNumExposedVariables()
	{
		Assert(m_State == kVariables);
		const u32 ret = m_CurrentCount;
		if(m_CurrentCount == 0)
		{
			// there are no variables; move directly onto groups
			m_State = kGroups;
			m_CurrentCount = *m_CurrentPtr++;
		}
		return ret;
	}

	const ModularSynth::tExposedVariable *synthBinaryReader::GetNextVariable()
	{
		Assert(m_State == kVariables);
		Assert(m_CurrentIndex < m_CurrentCount);
		ModularSynth::tExposedVariable *var = (ModularSynth::tExposedVariable*)m_CurrentPtr;
		// exposed variable struct is fixed size
		m_CurrentPtr += sizeof(ModularSynth::tExposedVariable);
		m_CurrentIndex++;

		if(m_CurrentIndex == m_CurrentCount)
		{
			m_State = kGroups;
			m_CurrentCount = *m_CurrentPtr++;
			m_CurrentIndex = 0;
		}
		return var;
	}

	void synthBinaryReader::Reset()
	{
		m_CurrentPtr = (u8*)&m_InstanceData->Module[0];

		m_State = kModules;
		m_CurrentIndex = 0;
		m_CurrentCount = m_InstanceData->numModules;
	}

#if __SYNTH_EDITOR
	synthModuleXmlOutputSerializer::synthModuleXmlOutputSerializer(fiStream *stream)
	{
		m_Stream = stream;
		m_Mode = synthModuleSerializer::StartSerializingInputs;
		m_CurrentTabLevel = 0;
	}

	void synthModuleXmlOutputSerializer::StartModule(const char *typeName, u32 &type)
	{
		WriteElementStart("Module");
			WriteElementValue("TypeId", typeName, type);
		m_Mode = synthModuleSerializer::StartSerializingFields;
	}

	void synthModuleXmlOutputSerializer::FinishedModule()
	{
		Assert(m_Mode == synthModuleSerializer::FinishedSerializingOutputs);
		WriteElementEnd("Module");
	}

	void synthModuleXmlOutputSerializer::StartInputs()
	{
		Assert(m_Mode == synthModuleSerializer::FinishedSerializingFields);

		//WriteElementStart("Inputs");

		m_Mode = synthModuleSerializer::SerializingInputs;
	}

	void synthModuleXmlOutputSerializer::SerializePin(synthPin *pin)
	{
		Assert(m_Mode == synthModuleSerializer::SerializingInputs || m_Mode == synthModuleSerializer::SerializingOutputs); 
		WriteElementStart(pin->IsOutput() ? "OutputPin" : "InputPin");
			if(pin->IsConnected())
			{
				// find the pin index
				synthPin *pins;
				u32 numPins;
				if(m_Mode == synthModuleSerializer::SerializingInputs)
				{
					pin->GetOtherPin()->GetParentModule()->GetOutputs(pins,numPins);
				}
				else
				{
					pin->GetOtherPin()->GetParentModule()->GetInputs(pins,numPins);
				}
				u32 index = ~0U;
				for(u32 i = 0; i < numPins; i++)
				{
					if(&pins[i] == pin->GetOtherPin())
					{
						index = i;
						break;
					}
				}
				WriteElementValue("OtherPin", pin->GetName(), index);
				WriteElementValue("OtherModule", pin->GetName(), (s32)pin->GetOtherPin()->GetParentModule()->GetIndex());
				WriteElementValue("StaticValue", pin->GetName(), 0.f);
			}
			else
			{
				WriteElementValue("OtherPin", "NC", -1);
				WriteElementValue("OtherModule", "NC", -1);
				if(pin->IsOutput())
				{
					// static outputs shouldn't save their value, as it prevents useful diffing
					WriteElementValue("StaticValue", pin->GetName(), 0.f);
				}
				else
				{
					WriteElementValue("StaticValue", pin->GetName(), pin->GetDataState() == synthPin::SYNTH_PIN_STATIC ? pin->GetStaticValue() : 0.f);
				}
			}

		WriteElementEnd(pin->IsOutput() ? "OutputPin" : "InputPin");
	}

	void synthModuleXmlOutputSerializer::FinishedInputs()
	{
		Assert(m_Mode == synthModuleSerializer::SerializingInputs);
		//WriteElementEnd("Inputs");
		m_Mode = synthModuleSerializer::FinishedSerializingInputs;
	}

	void synthModuleXmlOutputSerializer::StartOutputs()
	{
		Assert(m_Mode == synthModuleSerializer::FinishedSerializingInputs);
		//WriteElementStart("Outputs");
		m_Mode = synthModuleSerializer::SerializingOutputs;
	}

	void synthModuleXmlOutputSerializer::FinishedOutputs()
	{
		Assert(m_Mode == synthModuleSerializer::SerializingOutputs);
		//WriteElementEnd("Outputs");
		m_Mode = synthModuleSerializer::FinishedSerializingOutputs;
	}

	void synthModuleXmlOutputSerializer::StartFields()
	{
		//WriteElementStart("Fields");
		m_Mode = synthModuleSerializer::SerializingFields;
	}

	void synthModuleXmlOutputSerializer::SerializeField(const char *fieldName, bool &field)
	{
		WriteElementStart("FieldValue");
		WriteElementValue("Value", fieldName, field ? 1U : 0U);
		WriteElementEnd("FieldValue");
	}

	void synthModuleXmlOutputSerializer::SerializeField(const char *fieldName, u32 &field)
	{
		WriteElementStart("FieldValue");
		WriteElementValue("Value", fieldName, field);
		WriteElementEnd("FieldValue");
	}

	void synthModuleXmlOutputSerializer::SerializeField(const char *fieldName, s32 &field)
	{
		WriteElementStart("FieldValue");
		WriteElementValue("Value", fieldName, field);
		WriteElementEnd("FieldValue");
	}

	void synthModuleXmlOutputSerializer::SerializeField(const char *fieldName, f32 &field)
	{
		WriteElementStart("FieldValue");
		WriteElementValue("Value", fieldName, field);
		WriteElementEnd("FieldValue");
	}

	void synthModuleXmlOutputSerializer::FinishedFields()
	{
		Assert(m_Mode == synthModuleSerializer::SerializingFields);
		//WriteElementEnd("Fields");
		m_Mode = synthModuleSerializer::FinishedSerializingFields;
	}

	void synthModuleXmlOutputSerializer::WriteElementStart(const char *elementName)
	{
		char buf[128];
		Assert(StringLength(elementName) < (s32)sizeof(buf));
		char tabs[16];

		for(u32 t = 0; t < sizeof(tabs); t++)
		{
			if(t < m_CurrentTabLevel)
				tabs[t] = '\t';
			else
				tabs[t] = 0;
		}
		formatf(buf, sizeof(buf), "%s<%s>\r\n", tabs, elementName);
		m_Stream->Write(buf,StringLength(buf));
		m_CurrentTabLevel++;
	}

	void synthModuleXmlOutputSerializer::WriteElementEnd(const char *elementName)
	{
		Assert(m_CurrentTabLevel>0);

		char buf[128];
		Assert(StringLength(elementName) < (s32)sizeof(buf));
		char tabs[16];
		
		m_CurrentTabLevel--;

		for(u32 t = 0; t < sizeof(tabs); t++)
		{
			if(t < m_CurrentTabLevel)
				tabs[t] = '\t';
			else
				tabs[t] = 0;
		}
		formatf(buf, sizeof(buf), "%s</%s>\r\n", tabs, elementName);
		m_Stream->Write(buf,StringLength(buf));
	}

	void synthModuleXmlOutputSerializer::WriteElementValue(const char *elementName,	const char *comment, const u32 val)
	{
		char buf[128];
		Assert(StringLength(elementName) < (s32)sizeof(buf));
		char tabs[16];

		for(u32 t = 0; t < sizeof(tabs); t++)
		{
			if(t < m_CurrentTabLevel)
				tabs[t] = '\t';
			else
				tabs[t] = 0;
		}
		formatf(buf, "%s<%s>%u</%s> <!-- %s -->\r\n", tabs, elementName, val, elementName, comment);
		m_Stream->Write(buf,StringLength(buf));
	}

	void synthModuleXmlOutputSerializer::WriteElementValue(const char *elementName,	const char *comment, const s32 val)
	{
		char buf[128];
		Assert(StringLength(elementName) < (s32)sizeof(buf));
		char tabs[16];

		for(u32 t = 0; t < sizeof(tabs); t++)
		{
			if(t < m_CurrentTabLevel)
				tabs[t] = '\t';
			else
				tabs[t] = 0;
		}
		formatf(buf, "%s<%s>%d</%s> <!-- %s -->\r\n", tabs, elementName, val, elementName, comment);
		m_Stream->Write(buf,StringLength(buf));
	}

	void synthModuleXmlOutputSerializer::WriteElementValue(const char *elementName,	const char *comment, const f32 val)
	{
		char buf[128];
		Assert(StringLength(elementName) < (s32)sizeof(buf));
		char tabs[16];

		for(u32 t = 0; t < sizeof(tabs); t++)
		{
			if(t < m_CurrentTabLevel)
				tabs[t] = '\t';
			else
				tabs[t] = 0;
		}
		formatf(buf, "%s<%s>%f</%s> <!-- %s -->\r\n", tabs, elementName, FPIsFinite(val) ? val : 0.f, elementName, comment);
		m_Stream->Write(buf,StringLength(buf));
	}

#endif // __SYNTH_EDITOR

	///////////////////////////////////
	synthModuleBinaryMemSerializer::synthModuleBinaryMemSerializer()
	{
		m_IsLoading = false;
		m_Ptr = m_Storage;
	}

	void synthModuleBinaryMemSerializer::StartModule(const char *UNUSED_PARAM(typename), u32 &UNUSED_PARAM(type))
	{
		
	}

	void synthModuleBinaryMemSerializer::FinishedModule()
	{
		
	}

	void synthModuleBinaryMemSerializer::StartInputs()
	{
		
	}

	void synthModuleBinaryMemSerializer::SerializePin(synthPin *pin)
	{
		// we only care about inputs
		if(!pin->IsOutput())
		{
			if(m_IsLoading)
			{
				if(*((u32*)m_Ptr) != ~0U)
				{
					pin->SetStaticValue(*((f32*)m_Ptr));
				}
			}
			else
			{
				if(pin->GetDataState() != synthPin::SYNTH_PIN_STATIC)
				{
					*((u32*)m_Ptr) = ~0U;
				}
				else
				{
					*((f32*)m_Ptr) = pin->GetStaticValue();
				}
			}
			m_Ptr += 4;
		}
	}

	void synthModuleBinaryMemSerializer::FinishedInputs()
	{
		
	}

	void synthModuleBinaryMemSerializer::StartOutputs()
	{
		
	}

	void synthModuleBinaryMemSerializer::FinishedOutputs()
	{
	
	}

	void synthModuleBinaryMemSerializer::StartFields()
	{

	}

	void synthModuleBinaryMemSerializer::SerializeField(const char *UNUSED_PARAM(fieldName), bool &field)
	{
		f32 fieldF32 = (f32)(field == true ? 1.f : 0.f);
		SerializeField(fieldF32);
		field = (fieldF32 == 1.f ? true : false);
	}

	void synthModuleBinaryMemSerializer::SerializeField(const char *UNUSED_PARAM(fieldName), u32 &field)
	{
		f32 fieldF32 = (f32)field;
		SerializeField(fieldF32);
		field = (u32)fieldF32;
	}

	void synthModuleBinaryMemSerializer::SerializeField(const char *UNUSED_PARAM(fieldName), s32 &field)
	{
		f32 fieldF32 = (f32)field;
		SerializeField(fieldF32);
		field = (s32)fieldF32;
	}

	void synthModuleBinaryMemSerializer::SerializeField(const char *UNUSED_PARAM(fieldName), f32 &field)
	{
		SerializeField(field);
	}

	void synthModuleBinaryMemSerializer::SerializeField(u32 &v)
	{
		if(m_IsLoading)
		{
			v = *((u32*)m_Ptr);
		}
		else
		{
			*((u32*)m_Ptr) = v;
		}
		m_Ptr += sizeof(u32);
	}

	void synthModuleBinaryMemSerializer::SerializeField(f32 &v)
	{
		if(m_IsLoading)
		{
			u32 temp = *((u32*)m_Ptr);
			v = *((f32*)&temp);
		}
		else
		{
			*((f32*)m_Ptr) = FPIsFinite(v) ? v : 0.f;
		}
		m_Ptr += sizeof(f32);
	}


	void synthModuleBinaryMemSerializer::FinishedFields()
	{
		
	}

} // namespace rage
