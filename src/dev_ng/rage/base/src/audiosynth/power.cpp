// 
// audiosynth/power.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#include "abs.h"
#include "power.h"
#include "sign.h"
#include "synthutil.h"
#include "audiohardware/driverutil.h"
#include "math/amath.h"
#include "math/vecmath.h"
#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	synthPower::synthPower()
	{
		m_Outputs[0].Init(this, "Result",synthPin::SYNTH_PIN_DATA_NORMALIZED,true);
		m_Outputs[0].SetStaticValue(1.f);

		m_Inputs[0].Init(this, "Input",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetStaticValue(1.f);
		m_Inputs[0].SetConvertsInternally(false);
		m_Inputs[1].Init(this, "Power",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[1].SetStaticValue(1.f);		
	}

	void synthPower::Synthesize()
	{
		m_Outputs[0].SetDataFormat(m_Inputs[0].GetDataFormat());

		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC &&
			m_Inputs[1].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// both static
			const f32 input = Max(0.f, m_Inputs[0].GetNormalizedValue(0));
			const f32 exponent = m_Inputs[1].GetNormalizedValue(0);			
			const f32 out = Powf(input,exponent);
			const f32 rescaledOut = m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED ? out : out*2.f - 1.f;
			m_Outputs[0].SetStaticValue(rescaledOut);
		}
		else
		{
			// one or both dynamic
			m_Outputs[0].SetDataBuffer(&m_Buffer);		
			Vector_4V *RESTRICT output = (Vector_4V*)m_Buffer.GetBuffer();

			if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
			{
				for(u32 i = 0; i < m_Buffer.GetSize(); i += 4)
				{
					const Vector_4V out = audDriverUtil::V4Pow(V4Max(m_Inputs[0].GetNormalizedValueV(i), V4VConstant(V_ZERO)),
													m_Inputs[1].GetNormalizedValueV(i));
					output[i>>2] = out;
				}
			}
			else
			{
				// signal input
				ALIGNAS(128) float absBuffer[kMixBufNumSamples];  // CACHE_ALIGNED???

				synthUtil::CopyBuffer((Vector_4V*)absBuffer, m_Inputs[0].GetDataBuffer()->GetBufferV(), kMixBufNumSamples);
				synthAbs::Process(absBuffer, kMixBufNumSamples);

				if(m_Inputs[1].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
				{				
					ALIGNAS(16) float expBuffer[kMixBufNumSamples] ;

					for(u32 i = 0; i < kMixBufNumSamples; i++)
					{
						expBuffer[i] = m_Inputs[1].GetNormalizedValue(i);
					}
					synthPower::Process(absBuffer, expBuffer, kMixBufNumSamples);
				}
				else
				{
					synthPower::Process(absBuffer, m_Inputs[1].GetNormalizedValueV(0), kMixBufNumSamples);
				}

				synthUtil::CopyBuffer(m_Outputs[0].GetDataBuffer()->GetBufferV(), m_Inputs[0].GetDataBuffer()->GetBufferV(), kMixBufNumSamples);
				synthSign::Process(m_Outputs[0].GetDataBuffer()->GetBuffer(), kMixBufNumSamples);
				synthUtil::ScaleBuffer(m_Outputs[0].GetDataBuffer()->GetBuffer(), absBuffer, kMixBufNumSamples);
			}
		}
	}

	void synthPower::Process(float *RESTRICT const inOutBuffer, const float *const expBuffer, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		const Vector_4V *expPtr = reinterpret_cast<const Vector_4V*>(expBuffer);
		u32 count = numSamples>>2;
		while(count--)
		{
#if RSG_CPU_INTEL 
			// avoid calling V4Pow with a negative base or with zero in both base and exponent
			// this asserts on PC so we can't zero Inf/NaNs afterwards like on other platforms
			const Vector_4V inSamples = *inOutPtr;
			const Vector_4V zeroMask = V4IsLessThanOrEqualV(inSamples, V4VConstant(V_ZERO));
			const Vector_4V outSamples = V4SelectFT(zeroMask, inSamples, V4VConstant(V_ZERO));
			const Vector_4V exp = *expPtr++;
			const Vector_4V expZeroMask = V4IsLessThanV(exp, V4VConstant(V_ZERO));
			const Vector_4V expClamped = V4SelectFT(expZeroMask, exp, V4VConstant(V_ZERO));
			*inOutPtr++ = audDriverUtil::V4Pow(outSamples, expClamped);
#else
			const Vector_4V inSamples = *inOutPtr;
			const Vector_4V outSamples = audDriverUtil::V4Pow(inSamples, *expPtr++);
			const Vector_4V zeroMask = V4IsLessThanOrEqualV(inSamples, V4VConstant(V_ZERO));
			*inOutPtr++ = V4SelectFT(zeroMask, outSamples, V4VConstant(V_ZERO));
#endif
		}
	}

	void synthPower::Process(float *const inOutBuffer, Vector_4V_In exp, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		u32 count = numSamples>>2;
		
#if RSG_CPU_INTEL
		const Vector_4V expClamped = V4SelectFT(V4IsLessThanV(exp, V4VConstant(V_ZERO)), exp, V4VConstant(V_ZERO));
#endif

		while(count--)
		{
#if RSG_CPU_INTEL 
			// avoid calling V4Pow with a negative base or with zero in both base and exponent
			// this asserts on PC so we can't zero Inf/NaNs afterwards like on other platforms
			const Vector_4V inSamples = *inOutPtr;
			const Vector_4V zeroMask = V4IsLessThanOrEqualV(inSamples, V4VConstant(V_ZERO));
			const Vector_4V outSamples = V4SelectFT(zeroMask, inSamples, V4VConstant(V_ZERO));			
			
			*inOutPtr++ = audDriverUtil::V4Pow(outSamples, expClamped);
#else
			const Vector_4V inSamples = *inOutPtr;
			const Vector_4V outSamples = audDriverUtil::V4Pow(inSamples, exp);
			const Vector_4V zeroMask = V4IsLessThanOrEqualV(inSamples, V4VConstant(V_ZERO));
			*inOutPtr++ = V4SelectFT(zeroMask, outSamples, V4VConstant(V_ZERO));
#endif
		}
	}

	float synthPower::Process(const float x, const float y)
	{
		return Powf(Max(0.f, x), y);
	}

} // namespace rage

