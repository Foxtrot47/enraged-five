// 
// audiosynth/abs.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_ABS_H
#define SYNTH_ABS_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "pin.h"

#include "modulefactory.h"

namespace rage
{
	class synthAbs : public synthModuleBase<1,1,SYNTH_ABS>
	{
	public:

		SYNTH_MODULE_NAME("Abs");

		synthAbs();

		virtual void Synthesize();

		static void Process(float *RESTRICT inOutBuffer, const u32 numSamples);
		static void Process(float *RESTRICT output, const float *RESTRICT input, const u32 numSamples);
	private:

		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_ABS_H


