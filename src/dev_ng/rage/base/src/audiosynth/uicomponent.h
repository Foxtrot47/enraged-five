// 
// audiosynth/uicomponent.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UICOMPONENT_H
#define SYNTH_UICOMPONENT_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "grcore/im.h"
#include "grcore/effect_typedefs.h"
#include "vector/vector3.h"
#include "vector/matrix34.h"

namespace rage
{
	class grmShader;
	class grcTexture;
	struct grcVertexDeclaration;
	class grcVertexBuffer;
	class grcIndexBuffer;
	class grmShader;

	class synthUIComponent
	{
	public:

		virtual ~synthUIComponent(){}

		static bool InitClass();
		static void ShutdownClass();

		synthUIComponent()
		{
			m_WorldMtx.Identity();
			m_IsUnderMouse = false;
			m_ShouldDraw = true;
			m_Alpha = 1.f;
			m_RelativeMouseX = m_RelativeMouseY = 0.f;
		}

		void Draw();

		void SetMatrix(const Matrix34 &matrix)
		{
			m_WorldMtx = matrix;
		}

		const Matrix34 &GetMatrix() const
		{
			return m_WorldMtx;
		}

		void SetUnderMouse(const bool isUnderMouse)
		{
			m_IsUnderMouse = isUnderMouse;
		}

		bool IsUnderMouse() const
		{
			return m_IsUnderMouse && ShouldDraw();
		}

		void SetRelativeMousePos(const float x, const float y)
		{
			m_RelativeMouseX = x;
			m_RelativeMouseY = y;
		}

		float GetRelMouseX() const { return m_RelativeMouseX; }
		float GetRelMouseY() const { return m_RelativeMouseY; }

		void SetShouldDraw(const bool should)
		{
			m_ShouldDraw = should;
		}

		bool ShouldDraw() const
		{
			return m_ShouldDraw;
		}

		virtual void Update() = 0;

		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const = 0;

		f32 ComputeWidth() const
		{
			Vector3 v1,v2;
			GetBounds(v1,v2);
			return GetMatrix().a.Dot(v2-v1);
		}

		f32 ComputeHeight() const
		{
			Vector3 v1,v2;
			GetBounds(v1,v2);
			return GetMatrix().b.Dot(v2-v1);
		}

		void ComputeWidthAndHeight(f32 &width, f32 &height) const
		{
			Vector3 v1,v2;
			GetBounds(v1,v2);
			const Vector3 v3 = v2-v1;
			height = GetMatrix().b.Dot(v3);
			width = GetMatrix().a.Dot(v3);
		}

		void SetAlpha(const f32 alpha)
		{
			m_Alpha = alpha;
		}

		f32 GetAlpha() const
		{
			return m_Alpha;
		}
		
		static void DrawVB(const grcEffectTechnique tech);
		static void DrawVB(const char *techName);
		static grmShader *GetShader() 
		{
			return sm_Shader;
		}
		static void SetShaderVar(const char *varName, const f32 val);
		static void SetShaderVar(const char *varName, const Color32 val);
		static void SetShaderVar(const char *varName, const grcTexture *val);

		static u32 sm_NumTested;
		static u32 sm_NumRendered;

		static grcTexture *CreateTexture(const s32 width, const s32 height, const s32 format);

	protected:
		virtual void Render() = 0;

		f32 m_Alpha;
	private:
		Matrix34 m_WorldMtx;
		float m_RelativeMouseX;
		float m_RelativeMouseY;
		bool m_IsUnderMouse;
		bool m_ShouldDraw;

		static grmShader *sm_Shader;
		static grcVertexDeclaration *sm_VertexDecl;
		static grcVertexBuffer *sm_VertexBuffer;
		static grcIndexBuffer *sm_IndexBuffer;
	};
}// namespace rage

#endif // __SYNTH_EDITOR
#endif // SYNTH_WAVEFORMVIEW_H
