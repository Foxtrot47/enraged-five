#include "synthdefs.h"
#include "timedtrigger.h"
#include "timedtriggerview.h"

#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;
	SYNTH_EDITOR_VIEW(synthTimedTrigger);

	synthTimedTrigger::synthTimedTrigger() 
	{
		m_Outputs[0].Init(this, "Trigger1",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.0f);
		m_Outputs[1].Init(this, "Trigger2",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[1].SetStaticValue(0.0f);
		m_Outputs[2].Init(this, "Trigger3",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[2].SetStaticValue(0.0f);
		m_Outputs[3].Init(this, "Trigger4",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[3].SetStaticValue(0.0f);
		m_Outputs[4].Init(this, "Finished",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[4].SetStaticValue(0.0f);
		
		
		m_Inputs[kTimedTriggerInputTrigger].Init(this, "InputTrigger", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kTimedTriggerInputTrigger].SetStaticValue(0.f);
		m_Inputs[kTimedTriggerPredelay].Init(this, "Predelay", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kTimedTriggerPredelay].SetStaticValue(0.f);

		m_Inputs[kTimedTriggerTime1].Init(this, "Time1", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kTimedTriggerTime1].SetStaticValue(0.4f);
		m_Inputs[kTimedTriggerTime2].Init(this, "Time2", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kTimedTriggerTime2].SetStaticValue(0.4f);
		m_Inputs[kTimedTriggerTime3].Init(this, "Time3", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kTimedTriggerTime3].SetStaticValue(0.4f);
		m_Inputs[kTimedTriggerTime4].Init(this, "Time4", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kTimedTriggerTime4].SetStaticValue(0.4f);
	
		m_TriggerMode = synthEnvelopeGenerator::Retrigger;

		ResetProcessing();
	}

	void synthTimedTrigger::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeEnumField("TriggerMode", m_TriggerMode);
	}

	void synthTimedTrigger::ResetProcessing()
	{
		m_State = V4VConstant(V_ZERO);
	}

	void synthTimedTrigger::Synthesize()
	{
		const float triggerInput = m_Inputs[kTimedTriggerInputTrigger].GetNormalizedValue(0);
		const float predelay = Max(0.0f, m_Inputs[kTimedTriggerPredelay].GetNormalizedValue(0));

		float inOut1 = m_Inputs[kTimedTriggerTime1].GetNormalizedValue(0);
		float inOut2 = m_Inputs[kTimedTriggerTime2].GetNormalizedValue(0);
		float inOut3 = m_Inputs[kTimedTriggerTime3].GetNormalizedValue(0);
		float inOut4 = m_Inputs[kTimedTriggerTime4].GetNormalizedValue(0);

		float finished = 1.0f;

		finished = Process(m_TriggerMode, triggerInput, predelay, m_State, inOut1, inOut2, inOut3, inOut4);
		
		m_Outputs[0].SetStaticValue(inOut1);
		m_Outputs[1].SetStaticValue(inOut2);
		m_Outputs[2].SetStaticValue(inOut3);
		m_Outputs[3].SetStaticValue(inOut4);

		m_Outputs[4].SetStaticValue(finished);
	}

	float synthTimedTrigger::Process(const TriggerMode triggerMode, const float trigger, const float predelay, Vec::Vector_4V_InOut state, float &inOut1, float &inOut2, float &inOut3, float &inOut4)
	{
		const u32 packedState = (u32)GetXi(state);

		u32 phase = packedState & 0xff;
		u32 samplesTilNextPhase = packedState >> 8;

		bool stateChangeThisFrame = samplesTilNextPhase < kMixBufNumSamples;
		
		bool finished = false;
		if(triggerMode == synthEnvelopeGenerator::Interruptible && phase != IDLE)
		{
			if(trigger >= 1.f)
			{
				// The check in the IDLE state below will catch the same trigger so there will be no delay in
				// restarting the envelope
				phase = IDLE;
			}
		}

		switch(phase)
		{
		case IDLE:
			if(trigger >= 1.0f)
			{
				phase = PREDELAY;
				samplesTilNextPhase = u32(predelay * kMixerNativeSampleRate);
				stateChangeThisFrame = samplesTilNextPhase < kMixBufNumSamples;
				// intentional fall through
			}
			else
			{
				break;
			}
		case PREDELAY:
			if(stateChangeThisFrame)
			{	
				phase = TRIGGER1_WAIT;				
			}
			else
			{
				break;
			}
		case TRIGGER1_WAIT:
			if(inOut1 >= 0.0f)
			{
				phase = TRIGGER1;
				samplesTilNextPhase = u32(inOut1 * kMixerNativeSampleRate);
				stateChangeThisFrame = samplesTilNextPhase < kMixBufNumSamples;
			}
			else
			{
				// Wait until time goes positive
				break;
			}
		case TRIGGER1:
			if(stateChangeThisFrame)
			{
				phase = TRIGGER2_WAIT;
			}
			else
			{
				break;
			}
		case TRIGGER2_WAIT:
			if(inOut2 >= 0.0f)
			{
				phase = TRIGGER2;
				samplesTilNextPhase = u32(inOut2 * kMixerNativeSampleRate);
				stateChangeThisFrame = samplesTilNextPhase < kMixBufNumSamples;
			}
			else
			{
				// Wait until time goes positive
				break;
			}
		case TRIGGER2:
			if(stateChangeThisFrame)
			{
				phase = TRIGGER3_WAIT;
			}
			else
			{
				break;
			}
		case TRIGGER3_WAIT:
			if(inOut3 >= 0.0f)
			{
				phase = TRIGGER3;
				samplesTilNextPhase = u32(inOut3 * kMixerNativeSampleRate);
				stateChangeThisFrame = samplesTilNextPhase < kMixBufNumSamples;
			}
			else
			{
				// Wait until time goes positive
				break;
			}
		case TRIGGER3:
			if(stateChangeThisFrame)
			{
				phase = TRIGGER4_WAIT;
			}
			else
			{
				break;
			}
		case TRIGGER4_WAIT:
			if(inOut4 >= 0.0f)
			{
				phase = TRIGGER4;
				samplesTilNextPhase = u32(inOut4 * kMixerNativeSampleRate);
				stateChangeThisFrame = samplesTilNextPhase < kMixBufNumSamples;
			}
			else
			{
				// Wait until time goes positive
				break;
			}
		case TRIGGER4:
			if(stateChangeThisFrame)
			{
				phase = FINISHED;
			}
			else
			{
				break;
			}
		case FINISHED:
			if(triggerMode == synthEnvelopeGenerator::Retrigger || triggerMode == synthEnvelopeGenerator::Interruptible)
			{
				phase = IDLE;
			}
			else
			{
				// Not in constant trigger mode so wait for trigger signal to go low before
				// synthEnvelopeGenerator::Retriggering	
				if(trigger < 1.f)
				{
					phase = IDLE;
				}
			}
			finished = true;
			break;
		}

		inOut1 = phase == TRIGGER1 || phase == TRIGGER1_WAIT ? 1.0f : 0.0f;
		inOut2 = phase == TRIGGER2 || phase == TRIGGER2_WAIT ? 1.0f : 0.0f;
		inOut3 = phase == TRIGGER3 || phase == TRIGGER3_WAIT ? 1.0f : 0.0f;
		inOut4 = phase == TRIGGER4 || phase == TRIGGER4_WAIT ? 1.0f : 0.0f;

		samplesTilNextPhase -= kMixBufNumSamples;

		u32 newPackedVals = (phase&0xff) | (samplesTilNextPhase<<8);
		state = V4LoadScalar32IntoSplatted(newPackedVals);

		return finished ? 1.0f : 0.0f;
	}
}

