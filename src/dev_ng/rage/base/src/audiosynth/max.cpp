// 
// audiosynth/max.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "max.h"

#if !SYNTH_MINIMAL_MODULES

#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	synthMax::synthMax()
	{
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);

		m_Inputs[kMaxSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMaxSignal].SetConvertsInternally(false);
		m_Inputs[kMaxReset].Init(this, "Reset", synthPin::SYNTH_PIN_DATA_NORMALIZED);

		m_Mode = kMaxStatic;
		ResetProcessing();
	}

	void synthMax::ResetProcessing()
	{
		m_CurrentVal = 0.f;
	}

	void synthMax::Synthesize()
	{	
		if(m_Inputs[kMaxReset].GetNormalizedValue(0) >= 1.f)
		{
			m_CurrentVal = 0.f;
		}

		m_Outputs[kMaxSignal].SetDataFormat(m_Inputs[kMaxSignal].GetDataFormat());

		if(m_Inputs[kMaxSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_CurrentVal = Max(m_CurrentVal, Abs(m_Inputs[kMaxSignal].GetStaticValue()));
			m_Outputs[0].SetStaticValue(m_CurrentVal);
		}
		else
		{
			// dynamic input
			const Vector_4V *RESTRICT inputPtr = (const Vector_4V*)m_Inputs[0].GetDataBuffer()->GetBuffer();
			const u32 numSamples = m_Buffer.GetSize();

			if(m_Mode == kMaxStatic)
			{
				Vector_4V currentMax = V4LoadScalar32IntoSplatted(m_CurrentVal);
				for(u32 i = 0; i < numSamples; i+=4)
				{
					Vector_4V inV = V4Abs(*inputPtr++);
					currentMax = V4Max(inV, currentMax);
				}
				// consolidate into X
				const Vector_4V splattedMax = V4Max(currentMax, V4Max(V4Max(V4SplatY(currentMax), V4SplatZ(currentMax)), V4SplatW(currentMax)));

				m_CurrentVal = GetX(splattedMax);
				m_Outputs[0].SetStaticValue(m_CurrentVal);
			}
			else
			{
				// dynamic, sample accurate, expensive mode
				m_Outputs[0].SetDataBuffer(&m_Buffer);
				
				Vector_4V *RESTRICT outputPtr = (Vector_4V*)m_Outputs[0].GetDataBuffer()->GetBuffer();
				Vector_4V currentMaxSplatted = V4LoadScalar32IntoSplatted(m_CurrentVal);

				for(u32 i = 0; i < numSamples; i+=4)
				{
					Vector_4V inV = V4Abs(*inputPtr++);
					
					// need to process serially
					Vector_4V currentMaxSplattedX = V4Max(V4SplatX(inV), currentMaxSplatted);
					Vector_4V currentMaxSplattedY = V4Max(V4SplatY(inV), currentMaxSplattedX);
					Vector_4V currentMaxSplattedZ = V4Max(V4SplatZ(inV), currentMaxSplattedY);
					Vector_4V currentMaxSplattedW = V4Max(V4SplatW(inV), currentMaxSplattedZ);

					currentMaxSplatted = currentMaxSplattedW;

					// and recombine into non-splatted...
					Vector_4V outputXY = V4PermuteTwo<X1,X2,X1,X2>(currentMaxSplattedX, currentMaxSplattedY);
					Vector_4V outputZW = V4PermuteTwo<X1,X2,X1,X2>(currentMaxSplattedZ, currentMaxSplattedW);
					Vector_4V output = V4PermuteTwo<X1,Y1,X2,Y2>(outputXY, outputZW);

					*outputPtr++ = output;
				}

				m_CurrentVal = GetX(currentMaxSplatted);
			}
		}
	}


	void synthMax::Process(float *RESTRICT const inOutBuffer, const float resetTrigger, Vector_4V_InOut state, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);

		Vector_4V maxVal = state;

		if(resetTrigger >= 1.f)
		{
			maxVal = V4VConstant(V_ZERO);
		}
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V inV = V4Abs(*inOutPtr);

			// need to process serially
			Vector_4V currentMaxSplattedX = V4Max(V4SplatX(inV), maxVal);
			Vector_4V currentMaxSplattedY = V4Max(V4SplatY(inV), maxVal);
			Vector_4V currentMaxSplattedZ = V4Max(V4SplatZ(inV), maxVal);
			Vector_4V currentMaxSplattedW = V4Max(V4SplatW(inV), maxVal);

			maxVal = currentMaxSplattedW;

			// and recombine into non-splatted...
			Vector_4V outputXY = V4PermuteTwo<X1,X2,X1,X2>(currentMaxSplattedX, currentMaxSplattedY);
			Vector_4V outputZW = V4PermuteTwo<X1,X2,X1,X2>(currentMaxSplattedZ, currentMaxSplattedW);
			Vector_4V output = V4PermuteTwo<X1,Y1,X2,Y2>(outputXY, outputZW);

			*inOutPtr++ = output;
		}
		
		state = maxVal;
	}

	float synthMax::Process(const float input, const float resetTrigger, Vec::Vector_4V_InOut state)
	{
		float currentMax = GetX(state);
		if(resetTrigger >= 1.f)
		{
			currentMax = 0.f;
		}
		const float ret = Max(currentMax, Abs(input));
		SetX(state, ret);
		return ret;
	}
} // namespace rage
#endif
