// 
// audiosynth/rescalerviwe.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "rescalerview.h"
#include "rescaler.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"
#include "uiplane.h"

namespace rage
{

	synthRescalerView::synthRescalerView(synthModule *module) : synthModuleView(module)
	{
		m_ModeSwitch.AddItem("Clamped");
		m_ModeSwitch.AddItem("Unclamped");
		AddComponent(&m_ModeSwitch);
		m_ModeSwitch.SetCurrentIndex(GetRescalerModule()->ShouldClampInput() ? 0 : 1);
	}

	void synthRescalerView::Update()
	{
		Matrix34 mtx = GetMatrix();

		// Mode label
		mtx.d -= (mtx.b * 1.f);
		m_ModeSwitch.SetMatrix(mtx);
		m_ModeSwitch.SetAlpha(GetHoverFade());
		m_ModeSwitch.SetShouldDraw(ShouldDraw() && !IsMinimised());
		GetRescalerModule()->SetClampInput(m_ModeSwitch.GetCurrentIndex() == 0);
		SetHideTitle(m_ModeSwitch.IsShowingMenu());
		synthModuleView::Update();
	}

	void synthRescalerView::Render()
	{
		synthModuleView::Render();
	}

	bool synthRescalerView::StartDrag() const
	{
		if(m_ModeSwitch.IsUnderMouse() || m_ModeSwitch.IsShowingMenu())
			return false;
		return synthModuleView::StartDrag();
	}

	bool synthRescalerView::IsActive() const
	{
		return (IsUnderMouse());
	}
}
#endif // __SYNTH_EDITOR

