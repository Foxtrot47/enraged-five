// 
// audiosynth/switchview.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "switchview.h"

#include "switchview.h"
#include "switch.h"
#include "moduleview.h"
#include "pinview.h"
#include "uiplane.h"

#include "module.h"
#include "pin.h"

namespace rage
{
	synthSwitchView::synthSwitchView(synthModule *module) : synthModuleView(module)
	{
		m_ModeSwitch.AddItem("0-n Input");
		m_ModeSwitch.AddItem("0-1 Input");
		m_ModeSwitch.AddItem("Linear XFade");
		m_ModeSwitch.AddItem("Equal Power XFade");
		AddComponent(&m_ModeSwitch);

		m_ModeSwitch.SetCurrentIndex(GetSwitch()->GetMode());

		AddComponent(&m_Divisions);
		m_Divisions.SetRange(1.f, (f32)synthSwitch::kMaxSwitchValues);
		m_Divisions.SetNormalizedValue(GetSwitch()->GetNumSwitchValues() / (f32)synthSwitch::kMaxSwitchValues);
	}

	void synthSwitchView::Update()
	{
		// fade out title when editing
		if(IsMinimised())
		{
			SetTitleAlpha(1.f);
		}
		else
		{
			SetTitleAlpha(1.f - GetHoverFade());
		}
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();

		m_Divisions.SetMatrix(mtx);
		m_Divisions.SetWidth(0.4f);
		m_Divisions.SetAlpha(GetHoverFade() * GetAlpha());
		m_Divisions.SetTitle("SwitchValues");
		m_Divisions.SetTextAlpha(GetHoverFade() * GetAlpha());
		m_Divisions.SetRenderAsInteger(true);

		mtx.d -= mtx.b * m_Divisions.ComputeHeight() * 1.f;
		m_ModeSwitch.SetMatrix(mtx);
		m_ModeSwitch.SetAlpha(GetHoverFade() * GetAlpha());
		m_ModeSwitch.SetShouldDraw(ShouldDraw() && !IsMinimised());
			
		GetSwitch()->SetMode((synthSwitch::synthSwitchMode)m_ModeSwitch.GetCurrentIndex());
		SetHideTitle(m_ModeSwitch.IsShowingMenu());
		m_Divisions.SetShouldDraw(ShouldDraw() && !m_ModeSwitch.IsShowingMenu() && !IsMinimised());

		const u32 divisions = (u32)m_Divisions.GetValue();
		GetSwitch()->SetNumSwitchValues(divisions);

		synthModuleView::Update();
	}

	void synthSwitchView::Render()
	{
		synthModuleView::Render();

		Vector3 outputPinPos = GetSwitch()->GetOutput(0).GetView()->GetMatrix().d;
		outputPinPos.z -= 0.1f;

		if(GetSwitch()->GetMode() == synthSwitch::kInterpInput)
		{
			const f32 input = GetSwitch()->GetInput(0).GetNormalizedValue(0);
			const f32 interp = input - floorf(input);
			const u32 activeInputIndex = 1+GetSwitch()->ComputeActiveInputIndex();

			Vector3 pinPos = GetSwitch()->GetInput(activeInputIndex).GetView()->GetMatrix().d;
			pinPos.z -= 0.1f;

			synthModuleView::RenderWire(synthUIPlane::GetMaster()->GetMatrix(), 
				pinPos,
				outputPinPos,
				GetSwitch()->GetInput(activeInputIndex).GetView()->GetPeakValue() * (1.f-interp),
				Color32(0.f,0.f,1.f,1.f), 
				true);

			if(activeInputIndex < GetSwitch()->GetNumSwitchValues() && interp > 0.f)
			{
				Vector3 pinPos = GetSwitch()->GetInput(activeInputIndex+1).GetView()->GetMatrix().d;
				pinPos.z -= 0.1f;

				synthModuleView::RenderWire(synthUIPlane::GetMaster()->GetMatrix(), 
					pinPos,
					outputPinPos,
					GetSwitch()->GetInput(activeInputIndex+1).GetView()->GetPeakValue() * interp,
					Color32(0.f,0.f,1.f,1.f), 
					true);
			}

		}
		else
		{
			const u32 activeInputIndex = 1+GetSwitch()->ComputeActiveInputIndex();

			Vector3 pinPos = GetSwitch()->GetInput(activeInputIndex).GetView()->GetMatrix().d;
			pinPos.z -= 0.1f;

			synthModuleView::RenderWire(synthUIPlane::GetMaster()->GetMatrix(), 
				pinPos,
				outputPinPos,
				GetSwitch()->GetInput(activeInputIndex).GetView()->GetPeakValue(),
				Color32(0.f,0.f,1.f,1.f), 
				true);

		}
	}

	bool synthSwitchView::StartDrag() const
	{
		if(m_Divisions.IsUnderMouse() || m_ModeSwitch.IsUnderMouse() || m_ModeSwitch.IsShowingMenu())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthSwitchView::IsActive() const
	{
		return (IsUnderMouse() || m_Divisions.IsActive());
	}

	u32 synthSwitchView::GetModuleHeight() const
	{
		return Max(4U, synthModuleView::GetModuleHeight());
	}
}
#endif // __SYNTH_EDITOR

