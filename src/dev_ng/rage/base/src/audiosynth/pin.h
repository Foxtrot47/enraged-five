#ifndef __SYNTH_PIN_H
#define __SYNTH_PIN_H

#include "sampleframe.h"
#include "synthdefs.h"
#include "string/stringhash.h"
#include "vectormath/vectormath.h"

#include <string.h>

namespace rage
{
class synthModule;
class synthPinView;

class synthPin
{
public:
	enum synthPinDataState
	{
		SYNTH_PIN_STATIC = 0,
		SYNTH_PIN_DYNAMIC,
	};
	
	enum synthPinDataFormat
	{
		SYNTH_PIN_DATA_NORMALIZED = 0,
		SYNTH_PIN_DATA_SIGNAL,
	};
	
	synthPin()
	{
		m_ParentModule = NULL;
		m_DataState = SYNTH_PIN_STATIC;
		m_DataFormat = SYNTH_PIN_DATA_NORMALIZED;
		m_OtherPin = NULL;
		m_StaticValue = 0.f;
		m_DataBuffer = NULL;
		
#if __SYNTH_EDITOR
		m_Name = NULL;
		m_View = NULL;
#endif // __SYNTH_EDITOR

		m_NumOutputConnections = 0;
		m_IsOutput = false;
		m_ConvertsInternally = true;
	}

	~synthPin();
	
	synthPinDataState GetDataState() const
	{
		if(m_OtherPin)
		{
			return m_OtherPin->GetDataState();
		}
		return m_DataState;
	}

	synthPinDataFormat GetEffectiveDataFormat() const
	{
		if(m_ConvertsInternally)
		{
			return m_DataFormat;
		}
		else
		{
			return GetDataFormat();
		}
	}
	
	bool IsConnected() const
	{
		return (m_OtherPin != NULL);
	}

	bool IsOutput() const
	{ 
		return m_IsOutput; 
	}

	void SetConvertsInternally(const bool converts)
	{
		m_ConvertsInternally = converts;
	}
	
	void SetStaticValue(const f32 val)
	{
		m_StaticValue = val;
		m_DataState = SYNTH_PIN_STATIC;
	}
	
	void Connect(synthPin &otherPin)
	{
		Assert(!IsConnected());
		m_OtherPin = &otherPin;
		otherPin.NotifyConnection(this);
	}

	void Disconnect()
	{
		Assert(IsConnected());
		Assert(m_OtherPin);
		m_OtherPin->NotifyDisconnection(this);

		m_OtherPin = NULL;
		m_DataState = SYNTH_PIN_STATIC;
	}

	void SetDataBuffer(synthSampleFrame *buffer)
	{
		Assert(!IsConnected());
		m_DataBuffer = buffer;
		m_DataState = SYNTH_PIN_DYNAMIC;
	}

	synthPinDataFormat GetDataFormat() const
	{
		if(m_OtherPin)
		{
			return m_OtherPin->GetDataFormat();
		}
		return m_DataFormat;
	}

	void SetDataFormat(const synthPinDataFormat format)
	{
		m_DataFormat = format;
	}

	synthPin *GetOtherPin() const { return m_OtherPin; }
	
	void Init(synthModule *parentModule, const char *SYNTH_EDITOR_ONLY(name), synthPinDataFormat format, const bool isOutput = false) 
	{ 
		m_ParentModule = parentModule; 
		m_DataFormat = format; 
		m_IsOutput = isOutput; 

		SYNTH_EDITOR_ONLY(m_Name = name); 
	}

	synthModule *GetParentModule() const { return m_ParentModule; }
	
	f32 GetStaticValue() const
	{
		//assert(m_DataState == SYNTH_PIN_STATIC);
		if(m_OtherPin)
		{
			return m_OtherPin->GetStaticValue();
		}
		return m_StaticValue;
	}
	
	synthSampleFrame *GetDataBuffer()
	{
		if(m_OtherPin)
		{
			return m_OtherPin->GetDataBuffer();
		}
		return m_DataBuffer;
	}
		
	f32 GetNormalizedValue(const s32 index) const
	{
		if(m_OtherPin)
		{
			return m_OtherPin->GetNormalizedValue(index);
		}
		f32 val = 0.f;
		if(m_DataState == synthPin::SYNTH_PIN_STATIC)
		{
			val = m_StaticValue;
		}
		else
		{
			val = m_DataBuffer->GetBuffer()[index];
		}
		if(m_DataFormat == SYNTH_PIN_DATA_NORMALIZED)
		{
			return val;
		}
		else
		{
			return  (1.f+val)*0.5f;
		}
	}

	inline Vec::Vector_4V GetNormalizedValueV(const s32 index) const
	{
		if(m_OtherPin)
		{
			return m_OtherPin->GetNormalizedValueV(index);
		}
		Assert((index&3)==0);
		Vec::Vector_4V val = Vec::V4VConstant(V_ZERO);
		if(m_DataState == synthPin::SYNTH_PIN_STATIC)
		{
			val = Vec::V4LoadScalar32IntoSplatted(m_StaticValue);
		}
		else
		{
			val = *((Vec::Vector_4V*)&m_DataBuffer->GetBuffer()[index]);
		}
		if(m_DataFormat == SYNTH_PIN_DATA_NORMALIZED)
		{
			return val;
		}
		else
		{
			return  Vec::V4Scale(Vec::V4Add(Vec::V4VConstant(V_ONE), val), Vec::V4VConstant(V_HALF));
		}
	}
	
	f32 GetSignalValue(const s32 index) const
	{
		if(m_OtherPin)
		{
			return m_OtherPin->GetSignalValue(index);
		}
		f32 val = 0.f;
		if(m_DataState == synthPin::SYNTH_PIN_STATIC)
		{
			val = m_StaticValue;
		}
		else
		{
			val = m_DataBuffer->GetBuffer()[index];
		}
		if(m_DataFormat == SYNTH_PIN_DATA_SIGNAL)
		{
			return val;
		}
		else
		{
			return val*2.f - 1.f;
		}
	}

	inline Vec::Vector_4V GetSignalValueV(const s32 index) const
	{
		if(m_OtherPin)
		{
			return m_OtherPin->GetSignalValueV(index);
		}
		Assert((index&3)==0);
		Vec::Vector_4V val = Vec::V4VConstant(V_ZERO);
		if(m_DataState == synthPin::SYNTH_PIN_STATIC)
		{
			val = Vec::V4LoadScalar32IntoSplatted(m_StaticValue);
		}
		else
		{
			val = *((Vec::Vector_4V*)&m_DataBuffer->GetBuffer()[index]);
		}
		if(m_DataFormat == SYNTH_PIN_DATA_SIGNAL)
		{
			return val;
		}
		else
		{
			return  Vec::V4Add(Vec::V4Scale(Vec::V4VConstant(V_TWO), val), Vec::V4VConstant(V_NEGONE));
		}
	}

	
#if __SYNTH_EDITOR 
	const char *GetName() const
	{
		return m_Name;
	}

	synthPinView *GetView()
	{
		if(!HasView())
		{
			AllocateView();
		}
		return m_View;
	}

	bool HasView() const { return (m_View != NULL); }
#endif // __SYNTH_EDITOR


	u32 GetNumConnections() const
	{
		if(m_IsOutput)
		{
			return m_NumOutputConnections;
		}
		else
		{
			return IsConnected() ? 1U : 0U;
		}
	}

	void SetNumOutputConnections(const u32 numConnections) { m_NumOutputConnections = numConnections; }

	static const char *sm_PinNumberStrings[16];

private:
	void NotifyConnection(synthPin *otherPin);
	void NotifyDisconnection(synthPin *otherPin);

	synthPinDataState m_DataState;
	synthPinDataFormat m_DataFormat;

	f32 m_StaticValue;

	synthSampleFrame *m_DataBuffer;
	synthPin *m_OtherPin;
	synthModule *m_ParentModule;

#if __SYNTH_EDITOR
	bool AllocateView();

	const char *m_Name;
	synthPinView *m_View;
#endif

	u32 m_NumOutputConnections;
	bool m_IsOutput;
	bool m_ConvertsInternally;

};
				
}

#endif // __SYNTH_PIN_H
