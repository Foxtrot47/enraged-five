// 
// smalldelay.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "smalldelay.h"
#include "smalldelayview.h"
#include "system/memops.h"
#include "vectormath/vectormath.h"

#include "math/float16.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthSmallDelay);
	using namespace Vec;
	synthSmallDelay::synthSmallDelay()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_OutputBuffer);
		sysMemSet128(m_OutputBuffer.GetBuffer(), 0, m_OutputBuffer.GetSize() * sizeof(f32));

		m_Inputs[kSmallDelaySignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);	
		m_Inputs[kSmallDelayLength].Init(this, "DelaySamples",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kSmallDelayLength].SetStaticValue(1.f);
		m_Inputs[kSmallDelayFeedback].Init(this, "Feedback",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kSmallDelayFeedback].SetStaticValue(0.f);

		m_Mode = Fractional;

		ResetProcessing();
	}

	synthSmallDelay::~synthSmallDelay()
	{
		
	}

	void synthSmallDelay::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeEnumField("Mode", m_Mode);
	}

	void synthSmallDelay::ResetProcessing()
	{
		m_DelayLine = V4VConstant(V_ZERO);
	}

	void synthSmallDelay::Synthesize()
	{
		if(m_Inputs[kSmallDelaySignal].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			sysMemCpy(m_OutputBuffer.GetBuffer(), m_Inputs[kSmallDelaySignal].GetDataBuffer()->GetBuffer(), kMixBufNumSamples * sizeof(float));
			const float delayTime = m_Inputs[kSmallDelayLength].GetNormalizedValue(0);
			
			const float feedback = m_Inputs[kSmallDelayFeedback].GetNormalizedValue(0);
			
			if(m_Mode == Fractional)
			{
				if(feedback == 0.f)
				{
					synthSmallDelay::Process_Frac(m_OutputBuffer.GetBuffer(), delayTime, m_DelayLine, kMixBufNumSamples);
				}
				else
				{
					synthSmallDelay::Process_Frac_Feedback(m_OutputBuffer.GetBuffer(),
															delayTime,
															feedback,
															m_DelayLine,
															kMixBufNumSamples);
				}
			}
			else
			{
				if(feedback == 0.f)
				{
					synthSmallDelay::Process_NonInterp(m_OutputBuffer.GetBuffer(), delayTime, m_DelayLine, kMixBufNumSamples);
				}
				else
				{
					synthSmallDelay::Process_NonInterp_Feedback(m_OutputBuffer.GetBuffer(), 
																	delayTime,
																	feedback,
																	m_DelayLine, 
																	kMixBufNumSamples);
				}
			}

			m_Outputs[0].SetDataFormat(m_Inputs[kSmallDelaySignal].GetDataFormat());
		}
	}

	void synthSmallDelay::Process_Frac(float *inOut, const float delayTimeIn, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		Vector_4V delayLine[2] = {V4Float16Vec4UnpackFromXY(state), V4Float16Vec4UnpackFromZW(state)};
		float *delayLinePtr = (float*)&delayLine[0];

		const float delayTime = Abs(delayTimeIn);
		
		const float frac = delayTime - floorf(delayTime);

		s32 writeIndex = 0;
		s32 readIndex = (s32)delayTime;
		for(u32 i = 0; i < numSamples; i++)
		{
			const float input = inOut[i];
			delayLinePtr[writeIndex & 7] = input;

			float sample0 = delayLinePtr[(writeIndex+readIndex) & 7];
			float sample1 = delayLinePtr[(writeIndex+1+readIndex) & 7];

			float out = Lerp(frac, sample0, sample1);
			inOut[i] = out;

			writeIndex++;
		}

		state = V4Float16Vec8Pack(delayLine[0], delayLine[1]);
	}

	void synthSmallDelay::Process_Frac_Feedback(float *inOut, const float delayTimeIn, const float feedback, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		Vector_4V delayLine[2] = {V4Float16Vec4UnpackFromXY(state), V4Float16Vec4UnpackFromZW(state)};
		float *delayLinePtr = (float*)&delayLine[0];

		const float delayTime = Abs(delayTimeIn);

		const float frac = delayTime - floorf(delayTime);

		s32 readIndex = (s32)delayTime;
		s32 writeIndex = 0;
		for(u32 i = 0; i < numSamples; i++)
		{
			const float input = inOut[i];
			
			float sample0 = delayLinePtr[(writeIndex+readIndex) & 7];
			float sample1 = delayLinePtr[(writeIndex+1+readIndex) & 7];

			float out = Lerp(frac, sample0, sample1);
			inOut[i] = out;
			delayLinePtr[writeIndex & 7] = input + out * feedback;

			writeIndex++;
		}

		state = V4Float16Vec8Pack(delayLine[0], delayLine[1]);
	}

	void synthSmallDelay::Process_NonInterp(float *inOut, const float delayTimeIn, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		Vector_4V delayLine[2] = {V4Float16Vec4UnpackFromXY(state), V4Float16Vec4UnpackFromZW(state)};
		float *delayLinePtr = (float*)&delayLine[0];

		const float delayTime = Abs(delayTimeIn);

		s32 readIndex = (s32)delayTime;
		s32 writeIndex = 0;
		for(u32 i = 0; i < numSamples; i++)
		{
			delayLinePtr[writeIndex&7] = inOut[i];
			inOut[i] = delayLinePtr[(writeIndex+readIndex) & 7];
			
			writeIndex++;
		}

		state = V4Float16Vec8Pack(delayLine[0], delayLine[1]);
	}

	void synthSmallDelay::Process_NonInterp_Feedback(float *inOut, const float delayTimeIn, const float feedback, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		Vector_4V delayLine[2] = {V4Float16Vec4UnpackFromXY(state), V4Float16Vec4UnpackFromZW(state)};
		float *delayLinePtr = (float*)&delayLine[0];

		const float delayTime = Abs(delayTimeIn);

		s32 writeIndex = 0;
		s32 readIndex = (s32)delayTime;
		for(u32 i = 0; i < numSamples; i++)
		{
			const float input = inOut[i];
			const float output = delayLinePtr[(writeIndex+readIndex) & 7];
			inOut[i] = output;
			delayLinePtr[writeIndex & 7] = input + output * feedback;
			writeIndex++;
		}

		state = V4Float16Vec8Pack(delayLine[0], delayLine[1]);
	}

}
