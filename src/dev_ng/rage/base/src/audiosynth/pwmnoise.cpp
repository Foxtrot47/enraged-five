// 
// audiosynth/pwmnoise.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "pwmnoise.h"
#include "pwmnoiseview.h"

namespace rage 
{
	SYNTH_EDITOR_VIEW(synthPwmNoise);

	synthPwmNoise::synthPwmNoise() 
	{
		m_Inputs[kPwmNoisePulseAmplitude].Init(this, "Pulse Ampl",synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kPwmNoisePulseAmplitude].SetStaticValue(1.f);
		m_Inputs[kPwmNoiseGain].Init(this, "kPwmNoiseGain", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kPwmNoiseGain].SetStaticValue(1.f);
		m_Inputs[kPwmNoisePulseWidth].Init(this, "Pulse Width", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kPwmNoisePulseWidth].SetStaticValue(1.f);
		m_Inputs[kPwmNoisePulseRange].Init(this, "Pulse Range", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kPwmNoisePulseRange].SetStaticValue(1.f);

		m_MaxPulseWidth = 256;
		m_MinPulseWidth = 1;		
	
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_PulseAmplitude = 1.f;
		m_PulseSamples = 0;
		m_OnOff = false;
	}

	void synthPwmNoise::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("MinWidth", m_MinPulseWidth);
		serializer->SerializeField("MaxWidth", m_MaxPulseWidth);
	}

	void synthPwmNoise::Synthesize()
	{
		const f32 minWidth = (f32)m_MinPulseWidth;
		const f32 maxWidth = (f32)m_MaxPulseWidth;
		for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i++)
		{
			if(m_PulseSamples <= 0)
			{
				// pick a new width
				const f32 widthF32 = minWidth + (m_Inputs[kPwmNoisePulseWidth].GetNormalizedValue(i)*(maxWidth-minWidth));
				m_PulseSamples = (s32)widthF32;
				// pick a new amplitude
				m_PulseAmplitude = -1.f + (m_Inputs[kPwmNoisePulseAmplitude].GetNormalizedValue(i) * m_Inputs[kPwmNoisePulseRange].GetNormalizedValue(i));
				// flip the signal
				m_OnOff = !m_OnOff;
			}
			const f32 samp = (m_OnOff ? 1.f : -1.f) * m_PulseAmplitude * m_Inputs[kPwmNoiseGain].GetNormalizedValue(i);
			m_Outputs[0].GetDataBuffer()->GetBuffer()[i] = samp;
			m_PulseSamples--;
		}

	}

} // namespace rage
#endif
