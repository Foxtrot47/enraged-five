// 
// audiosynth/sign.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "sign.h"

#if !SYNTH_MINIMAL_MODULES

#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	synthSign::synthSign()
	{
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);

		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetConvertsInternally(false);
	}

	void synthSign::Synthesize()
	{	
		m_Outputs[0].SetDataFormat(m_Inputs[0].GetDataFormat());

		f32 in = 0;
		if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
		{
			in = m_Inputs[0].GetNormalizedValue(0);
		}
		else
		{
			in = m_Inputs[0].GetSignalValue(0);
		}
		const f32 sign = in == 0.f ? 1.f : Sign(in);
		m_Outputs[0].SetStaticValue(sign);

		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			const f32 *RESTRICT const input = m_Inputs[0].GetDataBuffer()->GetBuffer();
			f32 *RESTRICT output = m_Outputs[0].GetDataBuffer()->GetBuffer();

			const u32 numSamples = m_Outputs[0].GetDataBuffer()->GetSize();
			for(u32 i = 0; i < numSamples; i+=4)
			{
				Vector_4V inV = *((Vector_4V*)&input[i]);
				Vector_4V selectMask = V4IsGreaterThanOrEqualV(V4VConstant(V_ZERO), inV);
				*((Vector_4V*)&output[i]) = V4SelectFT(selectMask, V4VConstant(V_ONE), V4VConstant(V_NEGONE));
			}
		}
	}

	void synthSign::Process(float *RESTRICT const inOutBuffer, const rage::u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		u32 count = numSamples>>2;
		while(count--)
		{
			Vector_4V selectMask = V4IsGreaterThanOrEqualV(V4VConstant(V_ZERO), *inOutPtr);
			*inOutPtr++ = V4SelectFT(selectMask, V4VConstant(V_ONE), V4VConstant(V_NEGONE));
		}
	}
} // namespace rage
#endif
