//
// audiosynth/synthutil.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef SYNTH_SYNTHUTIL_H
#define SYNTH_SYNTHUTIL_H

#include "sampleframe.h"
#include "math/vecrand.h"
#include "vectormath/vectormath.h"

namespace rage
{
	class synthUtil
	{
	public:

		static void CopyBuffer(Vec::Vector_4V *RESTRICT outPtr, const Vec::Vector_4V *RESTRICT inPtr, const u32 numSamples)
		{
#if __SPU || RSG_ORBIS
			u32 count = numSamples >> 5;
			while(count--)
			{
				*outPtr++ = *inPtr++;
				*outPtr++ = *inPtr++;
				*outPtr++ = *inPtr++;
				*outPtr++ = *inPtr++;
				*outPtr++ = *inPtr++;
				*outPtr++ = *inPtr++;
				*outPtr++ = *inPtr++;
				*outPtr++ = *inPtr++;
			}
#else
			sysMemCpy128(outPtr, inPtr, sizeof(float) * numSamples);
#endif
		}

		static void ScaleBuffer(float *RESTRICT inOutBuffer, const Vec::Vector_4V_In scalar, const u32 numSamples)
		{
			Vec::Vector_4V *vecPtr = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
			u32 count = numSamples >> 2;
			while(count--)
			{
				*vecPtr = Vec::V4Scale(*vecPtr, scalar);
				vecPtr++;
			}
		}

		static void ScaleBuffer(float *RESTRICT inOutBuffer, const float *RESTRICT scalingBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *RESTRICT vecInOutPtr = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
			const Vec::Vector_4V *RESTRICT vecScalingPtr = reinterpret_cast<const Vec::Vector_4V*>(scalingBuffer);
			u32 count = numSamples >> 2;
			while(count--)
			{
				*vecInOutPtr = Vec::V4Scale(*vecInOutPtr, *vecScalingPtr++);
				vecInOutPtr++;
			}
		}

		static void ScaleBufferWithSignal(float *RESTRICT inOutBuffer, const float *RESTRICT scalingSignalBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *RESTRICT vecInOutPtr = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
			const Vec::Vector_4V *RESTRICT vecScalingPtr = reinterpret_cast<const Vec::Vector_4V*>(scalingSignalBuffer);
			u32 count = numSamples >> 2;
			while(count--)
			{
				const Vec::Vector_4V scalar = Vec::V4Scale(Vec::V4Add(Vec::V4VConstant(V_ONE), *vecScalingPtr++), Vec::V4VConstant(V_HALF));
				*vecInOutPtr = Vec::V4Scale(*vecInOutPtr, scalar);
				vecInOutPtr++;
			}
		}

		static void ConvertBufferToSignal(float *RESTRICT inOutBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *inOutVec = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
			u32 count = numSamples>>4;
			while(count--)
			{
				*inOutVec = Vec::V4AddScaled(Vec::V4VConstant(V_NEGONE), Vec::V4VConstant(V_TWO), *inOutVec);
				inOutVec++;
				*inOutVec = Vec::V4AddScaled(Vec::V4VConstant(V_NEGONE), Vec::V4VConstant(V_TWO), *inOutVec);
				inOutVec++;
				*inOutVec = Vec::V4AddScaled(Vec::V4VConstant(V_NEGONE), Vec::V4VConstant(V_TWO), *inOutVec);
				inOutVec++;
				*inOutVec = Vec::V4AddScaled(Vec::V4VConstant(V_NEGONE), Vec::V4VConstant(V_TWO), *inOutVec);
				inOutVec++;
			}
		}

		static void ConvertBufferToSignal(float *RESTRICT outBuffer, const float *RESTRICT inBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *RESTRICT outVec = reinterpret_cast<Vec::Vector_4V*>(outBuffer);
			const Vec::Vector_4V *RESTRICT inVec = reinterpret_cast<const Vec::Vector_4V*>(inBuffer);
			u32 count = numSamples>>4;
			while(count--)
			{
				*outVec++ = Vec::V4AddScaled(Vec::V4VConstant(V_NEGONE), Vec::V4VConstant(V_TWO), *inVec++);
				*outVec++ = Vec::V4AddScaled(Vec::V4VConstant(V_NEGONE), Vec::V4VConstant(V_TWO), *inVec++);
				*outVec++ = Vec::V4AddScaled(Vec::V4VConstant(V_NEGONE), Vec::V4VConstant(V_TWO), *inVec++);
				*outVec++ = Vec::V4AddScaled(Vec::V4VConstant(V_NEGONE), Vec::V4VConstant(V_TWO), *inVec++);
			}
		}

		static void ConvertBufferToNormalised(float *RESTRICT inOutBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *inOutVec = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
			u32 count = numSamples>>4;
			while(count--)
			{
				*inOutVec = Vec::V4AddScaled(Vec::V4VConstant(V_HALF), Vec::V4VConstant(V_HALF), *inOutVec);
				inOutVec++;
				*inOutVec = Vec::V4AddScaled(Vec::V4VConstant(V_HALF), Vec::V4VConstant(V_HALF), *inOutVec);
				inOutVec++;
				*inOutVec = Vec::V4AddScaled(Vec::V4VConstant(V_HALF), Vec::V4VConstant(V_HALF), *inOutVec);
				inOutVec++;
				*inOutVec = Vec::V4AddScaled(Vec::V4VConstant(V_HALF), Vec::V4VConstant(V_HALF), *inOutVec);
				inOutVec++;
			}
		}

		static void ConvertBufferToNormalised(float *RESTRICT outBuffer, const float *RESTRICT inBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *RESTRICT outVec = reinterpret_cast<Vec::Vector_4V*>(outBuffer);
			const Vec::Vector_4V *RESTRICT inVec = reinterpret_cast<const Vec::Vector_4V*>(inBuffer);
			u32 count = numSamples>>4;
			while(count--)
			{
				*outVec++ = Vec::V4AddScaled(Vec::V4VConstant(V_HALF), Vec::V4VConstant(V_HALF), *inVec++);
				*outVec++ = Vec::V4AddScaled(Vec::V4VConstant(V_HALF), Vec::V4VConstant(V_HALF), *inVec++);
				*outVec++ = Vec::V4AddScaled(Vec::V4VConstant(V_HALF), Vec::V4VConstant(V_HALF), *inVec++);
				*outVec++ = Vec::V4AddScaled(Vec::V4VConstant(V_HALF), Vec::V4VConstant(V_HALF), *inVec++);
			}
		}

		static void Sum(float *RESTRICT inOutBuffer, const float *RESTRICT inBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *RESTRICT vecInOutPtr = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
			const Vec::Vector_4V *RESTRICT vecInBuf2 = reinterpret_cast<const Vec::Vector_4V*>(inBuffer);
			u32 count = numSamples >> 2;
			while(count--)
			{
				*vecInOutPtr = Vec::V4Add(*vecInOutPtr, *vecInBuf2++);
				vecInOutPtr++;
			}
		}

		static void Sum(float *RESTRICT inOutBuffer, Vec::Vector_4V_In inScalar, const u32 numSamples)
		{
			Vec::Vector_4V *RESTRICT vecInOutPtr = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);			
			u32 count = numSamples >> 2;
			while(count--)
			{
				*vecInOutPtr = Vec::V4Add(*vecInOutPtr, inScalar);
				vecInOutPtr++;
			}
		}

		static void Subtract(float *RESTRICT inOutBuffer, const float *RESTRICT inBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *RESTRICT vecInOutPtr = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
			const Vec::Vector_4V *RESTRICT vecInBuf2 = reinterpret_cast<const Vec::Vector_4V*>(inBuffer);
			u32 count = numSamples >> 2;
			while(count--)
			{
				*vecInOutPtr = Vec::V4Subtract(*vecInOutPtr, *vecInBuf2++);
				vecInOutPtr++;
			}
		}

		static void Subtract(float *RESTRICT inOutBuffer, Vec::Vector_4V_In inScalar, const u32 numSamples)
		{
			// (x - y) == (x + (-1*y))
			Sum(inOutBuffer, Vec::V4Scale(Vec::V4VConstant(V_NEGONE), inScalar), numSamples);
		}

		static void Subtract(Vec::Vector_4V_In inScalar, float *RESTRICT inOutBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *inOutPtr = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
			u32 count = numSamples>>2;
			while(count--)
			{
				*inOutPtr = Vec::V4Subtract(inScalar, *inOutPtr);
				inOutPtr++;
			}
		}

		static void Divide(float *RESTRICT inOutBuffer, const float *RESTRICT inBuffer, const u32 numSamples)
		{
			Vec::Vector_4V *RESTRICT vecInOutPtr = reinterpret_cast<Vec::Vector_4V*>(inOutBuffer);
			const Vec::Vector_4V *RESTRICT vecInBuf2 = reinterpret_cast<const Vec::Vector_4V*>(inBuffer);
			u32 count = numSamples >> 2;
			while(count--)
			{
				*vecInOutPtr = Vec::V4InvScaleSafe(*vecInOutPtr, *vecInBuf2++, Vec::V4VConstant(V_ZERO));
				vecInOutPtr++;
			}
		}

		static void Divide(float *RESTRICT inOutBuffer, Vec::Vector_4V_In inScalar, const u32 numSamples)
		{
			ScaleBuffer(inOutBuffer, Vec::V4InvertSafe(inScalar, Vec::V4VConstant(V_ZERO)), numSamples);
		}

		static void HoldSample(float *RESTRICT outBuffer, const float val, const u32 numSamples)
		{
			const Vec::Vector_4V valV = Vec::V4LoadScalar32IntoSplatted(val);
			Vec::Vector_4V *outPtr = reinterpret_cast<Vec::Vector_4V*>(outBuffer);
#if !__SPU
			sysMemSet128(outBuffer, 0, sizeof(float) * numSamples);
#endif
			u32 count = numSamples >> 4;
			while(count--)
			{
				*outPtr++ = valV;
				*outPtr++ = valV;
				*outPtr++ = valV;
				*outPtr++ = valV;
			}
		}

		static __forceinline Vec::Vector_4V_Out RandV()
		{
			return Vec::V4AddScaled(Vec::V4VConstant(V_NEGONE), Vec::V4VConstant(V_TWO), Vec::V4Rand(sm_RandomSeed));
		}

		static __forceinline Vec::Vector_4V_Out RandNormV()
		{
			return Vec::V4Rand(sm_RandomSeed);
		}

		static float Rand()
		{
			return Vec::GetX(RandV());
		}

		static void InitClass()
		{
			sm_RandomSeed = Vec::V4RandInit();
		}

#if __PS3
		static Vec::Vector_4V *GetRandomSeedPtr() { return &sm_RandomSeed; }
#endif
	
private:
		static Vec::Vector_4V sm_RandomSeed;
	};
}


#endif // SYNTH_SYNTHUTIL_H
