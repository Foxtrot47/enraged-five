// 
// audiosynth/pwmnoiseview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_PWMNOISEVIEW_H
#define SYNTH_PWMNOISEVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uilabel.h"
#include "uimenuview.h"
#include "uirangeview.h"

namespace rage
{
	class synthPwmNoise;
	class synthPwmNoiseView : public synthModuleView
	{
	public:

		synthPwmNoiseView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();

	private:

		synthPwmNoise *GetNoiseModule() 
		{
			return (synthPwmNoise*)m_Module;
		}

		synthUIRangeView m_WidthRange;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_PWMNOISEVIEW_H

