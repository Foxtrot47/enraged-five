
#include "synthdefs.h"

#include "audiohardware/mixer.h"
#include "fv_combfilter.h"
#include "pin.h"
#include "math/vecmath.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	synthFvCombFilter::synthFvCombFilter() 
	{
		// 1 second max delay for now
		m_CombBuffer = rage_new float[kMixerNativeSampleRate];
		m_CombLength = kMixerNativeSampleRate;

		m_Outputs[0].Init(this,"Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);
		
		m_Inputs[kFvCombSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kFvCombDamping].Init(this, "Damping", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kFvCombDamping].SetStaticValue(1.f);
		m_Inputs[kFvCombFeedback].Init(this, "Feedback",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kFvCombFeedback].SetStaticValue(0.4f);
		m_Inputs[kFvCombLength].Init(this, "Length", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kFvCombLength].SetStaticValue(0.01f);
	}

	synthFvCombFilter::~synthFvCombFilter()
	{
		delete[] m_CombBuffer;
	}

	void synthFvCombFilter::Synthesize()
	{
		const u32 newLength = Min<u32>(kMixerNativeSampleRate, (u32)(m_Inputs[kFvCombLength].GetNormalizedValue(0) * kMixerNativeSampleRate));
		if(newLength != m_CombLength)
		{
			m_Filter.SetBuffer(m_CombBuffer, newLength);
			m_CombLength = newLength;
		}

		float *outPtr = m_Buffer.GetBuffer();
		for(u32 i = 0; i < kMixBufNumSamples; i++)
		{
			m_Filter.SetDamping(m_Inputs[kFvCombDamping].GetNormalizedValue(i));
			m_Filter.SetFeedback(m_Inputs[kFvCombFeedback].GetNormalizedValue(i));

			*outPtr++ = m_Filter.Process(m_Inputs[kFvCombSignal].GetSignalValue(i));
		}
	}
}
