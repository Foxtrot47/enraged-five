#ifndef SYNTH_MIDICCIN_H
#define SYNTH_MIDICCIN_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "audiohardware/midi.h"
#include "atl/array.h"
#include "math/amath.h"

namespace rage
{

	enum synthMidiCCInOutputs
	{
		kMidiCCInValue = 0,
		kMidiCCInActivity,

		kMidiCCInNumOutputs
	};

	class synthMidiCCIn : public synthModuleBase<0, kMidiCCInNumOutputs, SYNTH_MIDICCIN>
	{
	public:

		SYNTH_MODULE_NAME("MIDI CC");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthMidiCCIn();
	
		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();

		u32 GetChannel() const { return m_Channel; }
		void SetChannel(const u32 channel) { m_Channel = Clamp<u32>(channel, 0, 16); }

		u32 GetControllerId() const { return m_ControllerId; }
		void SetControllerId(const u32 ccId) { m_ControllerId = Clamp<u32>(ccId,0,127); }

        void StartLearning() { m_IsLearning = true; }
        void StopLearning() { m_IsLearning = false; }
        bool IsLearning() const { return m_IsLearning; }

		void SetPitchBend(const bool pitchBend) { m_PitchBend = pitchBend; }
		bool GetPitchBend() const { return m_PitchBend; }

	private:

		void ProcessMessage(const audMidiMessage &message);
		
		u32 m_ControllerId;
		u32 m_Channel;

		bool m_PitchBend;
        bool m_IsLearning;
		bool m_HadActivityThisFrame;
	};
}
#endif
#endif // SYNTH_MIDIIN_H


