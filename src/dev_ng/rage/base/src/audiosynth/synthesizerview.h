// 
// audiosynth/synthesizerview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SYNTHESIZERVIEW_H
#define SYNTH_SYNTHESIZERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "atl/array.h"

#include "synthesizer.h"
#include "uilabel.h"
#include "uiplane.h"
#include "vector/matrix34.h"

namespace rage
{

	class grcTexture;
	class grcRenderTarget;

	// Temporary to aid transition
	typedef synthUIPlane synthUIModulePlane;
	typedef synthUIPlane synthUIHUDPlane;

	class synthSynthesizerView
	{
	public:
		synthSynthesizerView();
		~synthSynthesizerView();

		bool Init(const char *synthName);
		void Shutdown();
		void Update();
		void Draw();

		void DisableCamera(const bool disabled)
		{
			m_DisableCamera = disabled;
		}

		static void InitClass();
		static void ShutdownClass();

		static void UnlockRenderTarget();
		static void LockRenderTarget();

		static bool SetModalComponent(synthUIComponent *component);
		static void ClearModalComponent();
		static synthUIComponent *GetModalComponent(){ return m_ModalComponent; }

		synthSynthesizer *GetSynth();

		synthUIPlane &GetUIPlane() { return m_UIPlane; }
		synthUIPlane &GetModulePlane() { return GetUIPlane(); }
		synthUIPlane &GetHUDPlane() { return GetUIPlane(); }

		const Matrix34 &GetCameraMatrix() const { return m_FreeCamMtx; }
		void SetCameraPosition(const u32 index, const Vector3 &pos)
		{
			m_CameraPositions[index] = pos;
		}
		const Vector3 &GetCameraPosition(const u32 index) const
		{
			return m_CameraPositions[index];
		}
		u32 GetNumCameraPositions() const { return m_CameraPositions.size(); }


		static synthSynthesizerView *Get() { Assert(sm_Instance); return sm_Instance; }

		bool SavePreset(const char *presetName);

		bool WantsToChangeAsset() const { return m_WantsToChangeAsset; }
		const char *GetNewAssetName() const { return m_NewAssetName; }
		void ChangeAsset(const char *assetName)
		{
			formatf(m_NewAssetName, "%s", assetName);
			m_WantsToChangeAsset = true;
		}

		bool SaveCopyAs(const char *synthName);
		bool CreateNewAsset(const char *synthName);

		bool IsSavingEnabled() const { return !m_DisableSaving; }
		void SetEnableSaving(const bool should) { m_DisableSaving = !should; }

	private:

		atRangeArray<Vector3,11> m_CameraPositions;
		char m_SaveBuffer[1024*1024];

		s32 m_SynthSlotId;
		synthUIPlane m_UIPlane;
		Matrix34 m_FreeCamMtx;
		Matrix34 m_LastCamMtx;

		synthUILabel m_CPUTimeLabel;
		synthUILabel m_MuteLabel;
		enum numCPUSamplesEnum {kNumCPUSamples = 64};
		f32 m_CPUTimes[kNumCPUSamples];

		s32 m_CurrentCPUSample;
		char m_CPUTimeBuf[64];

		u32 m_LastSaveTime;
		f32 m_CameraInterSpeed;

		bool m_DisableCamera;
		bool m_DisableSaving;

		char m_NewAssetName[128];
		bool m_WantsToChangeAsset;
		
		static synthUIComponent *m_ModalComponent;
		static grcRenderTarget *m_BlurRT;
		static grcRenderTarget *m_DepthBuffer;
		static synthSynthesizerView *sm_Instance;
	};
}

#endif // __SYNTH_EDITOR

#endif // SYNTH_SYNTHESIZERVIEW_H
