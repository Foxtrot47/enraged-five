#ifndef SYNTH_PHASER_H
#define SYNTH_PHASER_H

#include "sampleframe.h"
#include "module.h"
#include "pin.h"

#include "modulefactory.h"

#define SYNTH_NUM_PHASER_FILTERS 7
namespace rage
{
	enum synthPhaserInputs
	{
		kPhaserSignal = 0,
		kPhaserCenter,
		kPhaserSpread,
		kPhaserDepth,
		kPhaserRate,
		kPhaserMix,
		kPhaserNumInputs
	};

	class synthPhaser : public synthModuleBase<kPhaserNumInputs,1, SYNTH_PHASER>
	{
	public:
		SYNTH_MODULE_NAME("Phaser");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthPhaser();

		virtual void SerializeState(synthModuleSerializer *serializer);

		virtual void Synthesize();

		void SetInputType(int inputType) {m_inputType = inputType;}
		void SetTap(int tapNum) {m_tapNum = tapNum;}

	private:
		synthSampleFrame m_Buffer;
		//synthSampleFrame m_Buffer1;
		//synthSampleFrame m_Buffer2;
		//synthSampleFrame m_Buffer3;

		f32 m_prevLFOPhase;

		atRangeArray<f32, SYNTH_NUM_PHASER_FILTERS> m_x1;
		atRangeArray<f32, SYNTH_NUM_PHASER_FILTERS> m_x2;
		atRangeArray<f32, SYNTH_NUM_PHASER_FILTERS> m_y1;
		atRangeArray<f32, SYNTH_NUM_PHASER_FILTERS> m_y2;

		u32 m_inputType;
		u32 m_tapNum;
	};
}

#endif // SYNTH_PHASER_H


