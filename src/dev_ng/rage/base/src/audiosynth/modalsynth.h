#ifndef SYNTH_MODALSYNTH_H
#define SYNTH_MODALSYNTH_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "pin.h"
#include "kiss_fftr.h"

#include "audioengine/curve.h"

namespace rage
{
	struct synthModeEnv
	{
		s32 numSamples;
		f32 delta;
	};
	struct synthModeDef
	{
		s32 binIndex;
		f32 startGain;
		s32 delaySamples;
		s32 holdSamples;
		s32 numEnvelopes;
		synthModeEnv envelope[4];
	};

	struct synthModeState
	{
		synthModeDef def;
		s32 curSample;
		s32 envIndex;
		f32 gain;
	};
	class synthModalSynth : public synthModule
	{
	public:	
		synthModalSynth();
		~synthModalSynth();

		virtual void GetInputs(synthPin *&pins, u32 &numPins);
		virtual void GetOutputs(synthPin *&pins, u32 &numPins);

		virtual void SerializeState(synthModuleSerializer *UNUSED_PARAM(serializer)){}

		virtual synthPin &GetInput(const u32 index)
		{
			return m_Inputs[index];
		}

		virtual synthPin &GetOutput(const u32)
		{
			return m_Output;
		}

		virtual void Synthesize();

		virtual const char *GetName() const
		{
			return "ModalSynth";
		}

		virtual u32 ComputeNumOutputBuffersRequired() { return 1; }

		enum synthModalSynthInputs
		{
			AMPLITUDE,
			RELEASE,
			FREQUENCY,
			NOISE,
			NUM_MODALSYNTH_INPUTS
		};
	private:
		void InitModes();
		// amplitude
		synthPin m_Inputs[NUM_MODALSYNTH_INPUTS];
		// noise * amplitude
		synthPin m_Output;
		kiss_fftr_cfg m_FftConfig;
		kiss_fft_cpx m_FreqData[257];
		synthSampleFrame m_Buffer;

		s32 m_NumModes;
		audCurve m_ReleaseCurve;
		synthModeState m_Modes[257];
	};
}
#endif
#endif // SYNTH_MODALSYNTH_H
