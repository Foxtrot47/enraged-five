#include "midiccin.h"

#if !SYNTH_MINIMAL_MODULES

#include "midiccinview.h"

#include "audiohardware/driver.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthMidiCCIn);

	synthMidiCCIn::synthMidiCCIn()
	{
		m_ControllerId = 0;
		m_Channel = 0;
        m_IsLearning = false;
		m_PitchBend = false;

		m_Outputs[0].Init(this, "Value", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);

		m_Outputs[1].Init(this, "Activity", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[1].SetStaticValue(0.f);
	}

	void synthMidiCCIn::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("Controller", m_ControllerId);
		serializer->SerializeField("Channel", m_Channel);
		serializer->SerializeField("PitchBend", m_PitchBend);
	}

	void synthMidiCCIn::Synthesize()
	{
		MidiMessageFunctor functor;
		functor.Reset<synthMidiCCIn, &synthMidiCCIn::ProcessMessage>(this);
		
		m_HadActivityThisFrame = false;

		SYNTH_EDITOR_ONLY(audDriver::GetMidiInput()->ProcessQueue(functor, m_Channel, audMidiIn::kDontEmptyQueue));

		if(m_HadActivityThisFrame)
		{			
			m_Outputs[kMidiCCInActivity].SetStaticValue(1.f);
		}
		else
		{
			m_Outputs[kMidiCCInActivity].SetStaticValue(0.f);
		}
	}

	void synthMidiCCIn::ProcessMessage(const audMidiMessage &message)
	{
		struct channelMessage
		{
			u8 status;
			u8 byte1;
			u8 byte2;
			u8 byte3;
		};

		const channelMessage &m = (const channelMessage&)message.Message;

		if(m_PitchBend)
		{
			enum {PitchBend = 0xE0};
			if((m.status & 0xF0) == PitchBend)
			{
				u32 val = ((m.byte2 & 0x7F)<<7) | (m.byte1 & 0x7F);
				m_HadActivityThisFrame = true;
				m_Outputs[kMidiCCInValue].SetStaticValue(val / 16384.f);
			}
		}
		else
		{
			if(m_IsLearning && m.byte1 != 0)
			{
				m_ControllerId = m.byte1;
	            m_IsLearning = false;
			}
	
			if(m.byte1 == m_ControllerId)
			{
				m_HadActivityThisFrame = true;
				m_Outputs[kMidiCCInValue].SetStaticValue(m.byte2 / 127.f);
			}
		}

		//audDisplayf("MIDI In: %s: %d message %X key %X velocity %X ", m_Device.GetName(), m.status, m.status&0xf0, m.byte1, m.byte2);
	}
}
#endif
