#ifndef SYNTH_MULTIPLIER_H
#define SYNTH_MULTIPLIER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum {kMaxMultiplierInputs = 16};
	class synthMultiplier : public synthModuleBase<kMaxMultiplierInputs,1,SYNTH_MULTIPLIER>
	{
		public:
			SYNTH_MODULE_NAME("Multiplier");
			synthMultiplier();
			virtual void GetInputs(synthPin *&pins, u32 &numPins);			
			virtual void Synthesize();
			
		private:

			synthSampleFrame m_Buffer;
		};
}
#endif
#endif // SYNTH_MULTIPLIER_H


