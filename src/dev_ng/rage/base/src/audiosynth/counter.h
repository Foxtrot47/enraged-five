
#ifndef SYNTH_COUNTER_H
#define SYNTH_COUNTER_H
#include "synthdefs.h"

#include "atl/array.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{

	enum 
	{
		kCounterReset = 0,
		kCounterIncrement,
		kCounterDecrement,
		kCounterAutoReset,
		
		kCounterNumInputs
	};

	enum
	{
		kCounterOutputValue = 0,
		kCounterOutputReset,

		kCounterNumOutputs
	};

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
	class synthCounter : public synthModuleBase<kCounterNumInputs, kCounterNumOutputs, SYNTH_COUNTER>
	{
	public:

		SYNTH_MODULE_NAME("Counter");
		synthCounter();
		virtual void Synthesize();
		virtual void ResetProcessing();

		static float Process_Counter(const float reset, const float increment, const float decrement, const float autoResetThreshold, Vec::Vector_4V_InOut state);
		static float Process_Trigger(const float reset, const float increment, const float decrement, const float autoResetThreshold, Vec::Vector_4V_InOut state);
	
	private:

		Vec::Vector_4V m_State1;
		Vec::Vector_4V m_State2;
	};
}
#if __WIN32
#pragma warning(pop)
#endif

#endif // SYNTH_COUNTER_H


