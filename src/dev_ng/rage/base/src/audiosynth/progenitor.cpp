
#include "synthdefs.h"
#if __SYNTH_EDITOR
#include "audiohardware/mixer.h"
#include "progenitor.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;
	synthProgenitorReverb::synthProgenitorReverb() 
	{
		m_Inputs[kProgenitorReverbSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);

		InitInput(kProgenitorReverbDecay1, "Decay1", m_Reverb1.Decay1);
		InitInput(kProgenitorReverbDecay2, "Decay2", m_Reverb1.Decay2);
		InitInput(kProgenitorReverbDecay3, "Decay3", m_Reverb1.Decay3);
		InitInput(kProgenitorReverbDiffusion1, "Diffusion1", m_Reverb1.Diffusion1);
		InitInput(kProgenitorReverbDiffusion2, "Diffusion2", m_Reverb1.Diffusion2);
		InitInput(kProgenitorReverbDefinition, "Definition", m_Reverb1.Definition);
		InitInput(kProgenitorReverbDecayDiffusion, "DecayDiffusion", m_Reverb1.DecayDiffusion);
		InitInput(kProgenitorReverbHFBandwidth, "HFBandwidth", m_Reverb1.HF_Bandwidth);
		InitInput(kProgenitorReverbDamping, "Damping", m_Reverb1.Damping);
		
		m_Outputs[0].Init(this, "Out 1", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer1);

		m_Outputs[1].Init(this, "Out 2", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[1].SetDataBuffer(&m_Buffer2);

		m_Reverb2.Init();
	}

	void synthProgenitorReverb::InitInput(const s32 inputIndex, const char *name, float &val)
	{
		m_Inputs[inputIndex].Init(this, name, synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[inputIndex].SetStaticValue(val);
	}

	synthProgenitorReverb::~synthProgenitorReverb()
	{

	}

	void synthProgenitorReverb::UpdateInput(const s32 index, const s32 sampleIndex, float &destVal, Vec::Vector_4V_Ref destValV)
	{
		const float val = m_Inputs[index].GetNormalizedValue(sampleIndex);
		destVal = val;
		destValV = V4LoadScalar32IntoSplatted(val);
	}

	void synthProgenitorReverb::UpdateInputs(const s32 sampleIndex)
	{
		UpdateInput(kProgenitorReverbDecay1, sampleIndex, m_Reverb1.Decay1, m_Reverb2.Decay1);
		UpdateInput(kProgenitorReverbDecay2, sampleIndex, m_Reverb1.Decay2, m_Reverb2.Decay2);
		UpdateInput(kProgenitorReverbDecay3, sampleIndex, m_Reverb1.Decay3, m_Reverb2.Decay3);
		UpdateInput(kProgenitorReverbDiffusion1, sampleIndex, m_Reverb1.Diffusion1, m_Reverb2.Diffusion1);
		UpdateInput(kProgenitorReverbDiffusion2, sampleIndex, m_Reverb1.Diffusion2, m_Reverb2.Diffusion2);
		UpdateInput(kProgenitorReverbDefinition, sampleIndex, m_Reverb1.Definition, m_Reverb2.Definition);
		UpdateInput(kProgenitorReverbDecayDiffusion, sampleIndex, m_Reverb1.DecayDiffusion, m_Reverb2.DecayDiffusion);
		UpdateInput(kProgenitorReverbHFBandwidth, sampleIndex, m_Reverb1.HF_Bandwidth, m_Reverb2.HF_Bandwidth);
		UpdateInput(kProgenitorReverbDamping, sampleIndex, m_Reverb1.Damping, m_Reverb2.Damping);		
	}

	void synthProgenitorReverb::Synthesize()
	{
		float *outPtr1 = m_Buffer1.GetBuffer();
		float *outPtr2 = m_Buffer2.GetBuffer();

		for(u32 i = 0; i < kMixBufNumSamples; i++)
		{
			UpdateInputs(i);
			float input = m_Inputs[kProgenitorReverbSignal].GetSignalValue(i);
			*outPtr1++ = m_Reverb1.Process(input,input);

			Vector_4V inV = V4LoadScalar32IntoSplatted(input);
			Vector_4V out2 = m_Reverb2.Process(inV,inV);
			*outPtr2++ = GetX(out2);
		}
	}

	float revModelProgenitor::Process(const float lInput, const float rInput)
	{
		n_15_16.SetDecay(Decay2);
		n_15_16.SetDiffusion(Diffusion2);
		n_17_18.SetDecay(Decay3);
		n_17_18.SetDiffusion(Diffusion1);

		n_24_27.SetDecay1(Decay1);
		n_24_27.SetDefinition(Definition);
		n_24_27.SetDecay2(Decay2);
		n_24_27.SetDecayDiffusion(DecayDiffusion);

		n_31_37.SetDecay1(Decay1);
		n_31_37.SetDecay2(Decay2);
		n_31_37.SetDecayDiffusion(DecayDiffusion);
		n_31_37.SetDefinition(Definition);
		n_31_37.SetChorusDry(ChorusDry);
		n_31_37.SetChorusWet(ChorusWet);
		

		n_19_20.SetDecay(Decay2);
		n_19_20.SetDiffusion(Diffusion2);
		n_21_22.SetDecay(Decay3);
		n_21_22.SetDiffusion(Diffusion1);

		n_42_45.SetDecay1(Decay1);
		n_42_45.SetDecay2(Decay2);
		n_42_45.SetDecayDiffusion(DecayDiffusion);
		n_42_45.SetDefinition(Definition);

		n_49_55.SetDecay1(Decay1);
		n_49_55.SetDecay2(Decay2);
		n_49_55.SetDefinition(Definition);
		n_49_55.SetDecayDiffusion(DecayDiffusion);
		n_49_55.SetChorusDry(ChorusDry);
		n_49_55.SetChorusWet(ChorusWet);

		dcL.SetFc(5.f);
		dcR.SetFc(5.f);


		n_59_60.Damping = HF_Bandwidth; // InputDamping
		n_64_65.Damping = HF_Bandwidth;

		n_11_12.Damping = Damping;//lfo?
		n_13_14.Damping = Damping;//-lfo?

		n_7_8.Damping = 0.875f;
		n_9_10.Damping = 0.875f;
		
		float outL = 0.f, outR = 0.f;

/*		const float n60 = n_59_60.Read();
		const float n59 = lInput * 0.812f + n60 * HF_Bandwidth;
		n_59_60.Write(n59);
		outL = n_60_61.Process(n60);

		const float n65 = n_64_65.Read();
		const float n64 = rInput * 0.812f + n65 * HF_Bandwidth;
		n_64_65.Write(n64);
		outR = n_65_66.Process(n65);
*/

		outL = n_60_61.Process(n_59_60.Process(dcL.Process(lInput)));
		outR = n_65_66.Process(n_64_65.Process(dcR.Process(rInput)));
		
		
		const float crossL = n_37_39a.Read();
		const float crossR = n_55_58.Read();

		const float bassBoost = 0.06f;
		outL += Decay1 * (crossR + bassBoost + n_9_10.Process(crossR));
		outR += Decay1 * (crossL + bassBoost + n_7_8.Process(crossL));

		
		outL = n_17_18.Process(n_16_17.Process(n_15_16.Process(n_11_12.Process(outL))));
		outR = n_21_22.Process(n_19_20.Process(n_13_14.Process(outR)));

		n_37_39a.Process(n_31_37.Process(n_27_31.Process(n_24_27.Process(outL))));
		n_49_55.Process(n_45_49.Process(n_42_45.Process(n_41_42.Process(outR))));


		const float Aout = n_27_31.ReadTap(276) * 0.938f;
		const float Bout = n_45_49.ReadTap(468)*0.438f + n_41_42.ReadTap(625)*0.938f - n_27_31.ReadTap(312) * 0.438f + n_55_58.ReadTap(8)*0.125f;
		const float Cout = n_45_49.ReadTap(24)*0.938f + n_37_39a.ReadTap(36) * 0.469f;
		const float Dout = n_27_31.ReadTap(40)*0.438f + n_23_24.Read() * 0.938f - n_45_49.ReadTap(192)*0.438f + n_37_39a.ReadTap(1572)*0.125f;

		return Aout+Bout+Cout+Dout;//?
	}

	
}

#endif // __SYNTH_EDITOR
