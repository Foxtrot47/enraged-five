// 
// audiosynth/numericinputbox.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "numericinputbox.h"

#include "audiohardware/channel.h"

#include "input/keyboard.h"
#include "input/keys.h"
#include "system/memops.h"
#include "system/xtl.h"

namespace rage {

synthUITextBoxInput::synthUITextBoxInput() : 
m_InputLabel(1536),
m_KeyboardIndex(0),
m_InputEnabled(false),
m_DrawTitle(false),
m_DrawReflection(false),
m_AllowAlphaChars(false)
{
	AddComponent(&m_TitleLabel);
	sysMemSet(m_KeyboardBuffer, 0, kKeyboardBufferSize);
	m_HighlightColour = Color32(0.f,0.f,1.f,1.f);
}

synthUITextBoxInput::~synthUITextBoxInput()
{

}

void synthUITextBoxInput::GetBounds(Vector3 &v1, Vector3 &v2) const
{
	v1 = Vector3(-1.f,-1.f,0.f);
	v2 = Vector3(1.f, 1.f, 0.f);
	GetMatrix().Transform(v1);
	GetMatrix().Transform(v2);
}

struct synthKeyMapEntry
{
	s32 scanCode;
	char character;
	char shiftCharacter;
	bool isAlphaCharacter;
};

synthKeyMapEntry g_KeyMap[] =
{
	{KEY_A, 'a','A',true},
	{KEY_B, 'b','B',true},
	{KEY_C, 'c','C',true},
	{KEY_D, 'd','D',true},
	{KEY_E, 'e','E',true},
	{KEY_F, 'f','F',true},
	{KEY_G, 'g','G',true},
	{KEY_H, 'h','H',true},
	{KEY_I, 'i','I',true},
	{KEY_J, 'j','J',true},
	{KEY_K, 'k','K',true},
	{KEY_L, 'l','L',true},
	{KEY_M, 'm','M',true},
	{KEY_N, 'n','N',true},
	{KEY_O, 'o','O',true},
	{KEY_P, 'p','P',true},
	{KEY_Q, 'q','Q',true},
	{KEY_R, 'r','R',true},
	{KEY_S, 's','S',true},
	{KEY_T, 't','T',true},
	{KEY_U, 'u','U',true},
	{KEY_V, 'v','V',true},
	{KEY_W, 'w','W',true},
	{KEY_X, 'x','X',true},
	{KEY_Y, 'y','Y',true},
	{KEY_Z, 'z','Z',true},
	{KEY_0, '0',')', false},
	{KEY_1, '1','!', false},
	{KEY_2, '2','\"', false},
	{KEY_3, '3','�', false},
	{KEY_4, '4','$', false},
	{KEY_5, '5','%', false},
	{KEY_6, '6','^', false},
	{KEY_7, '7','&', false},
	{KEY_8, '8','*', false},
	{KEY_9, '9','(', false},
	{KEY_NUMPAD0, '0',')', false},
	{KEY_NUMPAD1, '1','!', false},
	{KEY_NUMPAD2, '2','\"', false},
	{KEY_NUMPAD3, '3','�', false},
	{KEY_NUMPAD4, '4','$', false},
	{KEY_NUMPAD5, '5','%', false},
	{KEY_NUMPAD6, '6','^', false},
	{KEY_NUMPAD7, '7','&', false},
	{KEY_NUMPAD8, '8','*', false},
	{KEY_NUMPAD9, '9','(', false},
	
	{KEY_MINUS, '-', '_', false},
	{KEY_PERIOD, '.', '>', false},
	{KEY_DECIMAL, '.', '>', false},	
	{KEY_SPACE, ' ', ' ', true},
	{KEY_SLASH, '/', '?', true},
	{KEY_MULTIPLY, '*', '*', true},
	{KEY_LBRACKET, '(', '(', true},
	{KEY_RBRACKET, ')', ')', true},
	{KEY_ADD, '+', '+', true},
	{KEY_EQUALS, '=', '+', true},
	{KEY_SEMICOLON, ';', ':', true},
	{KEY_COMMA, ',', '<', true},
	{KEY_BACKSLASH, '\\', '|', true},
};

const u32 g_NumKeyMapEntries = sizeof(g_KeyMap) / sizeof(g_KeyMap[0]);

void synthUITextBoxInput::Update()
{
	synthUIView::Update();

	m_TitleLabel.SetSize(0.1f);
	m_TitleLabel.SetShouldDraw(m_DrawTitle);
	m_TitleLabel.SetUnderMouse(true);
	
	m_InputLabel.SetShouldDraw(true);
	m_InputLabel.SetSize(0.1f);

	m_TitleLabel.SetShouldAlwaysDraw(true);
	m_InputLabel.SetShouldAlwaysDraw(true);

	if(m_InputEnabled)
	{

		
		if(ioKeyboard::KeyPressed(KEY_RETURN) || ioKeyboard::KeyPressed(KEY_NUMPADENTER))
		{
			m_KeyboardBuffer[m_KeyboardIndex] = 0;
			audDisplayf("keyboard buffer: \'%s\'", m_KeyboardBuffer);
			
			m_HadEnter = true;
		}
		else if(ioKeyboard::KeyPressed(KEY_ESCAPE))
		{
			Reset();
			m_HadEnter = true;
		}
	#if __WIN32PC
		else if(ioKeyboard::KeyPressed(KEY_V) && ioKeyboard::KeyDown(KEY_CONTROL))
		{
			OpenClipboard(NULL);
			HANDLE clip0 = GetClipboardData(CF_TEXT);
			if(clip0 != INVALID_HANDLE_VALUE)
			{
				HANDLE h = GlobalLock(clip0); 
				if(h != INVALID_HANDLE_VALUE)
				{
					char* c = (char*)clip0;
					if(c)
					{
						int len = (int)strlen(c);
						for(s32 i = 0; i < len; i++)
						{
							if(m_KeyboardIndex < kKeyboardBufferSize-2)
							{
								m_KeyboardBuffer[m_KeyboardIndex++] = c[i];
							}
						}
					}
					GlobalUnlock(clip0);
					CloseClipboard();
				}
			}
		}
		else if(ioKeyboard::KeyPressed(KEY_C) && ioKeyboard::KeyDown(KEY_CONTROL))
		{		
			const size_t len = strlen(m_KeyboardBuffer) + 1;
			HGLOBAL hMem =  GlobalAlloc(GMEM_MOVEABLE, len);
			sysMemCpy(GlobalLock(hMem), m_KeyboardBuffer, len);
			GlobalUnlock(hMem);
			OpenClipboard(0);
			EmptyClipboard();
			SetClipboardData(CF_TEXT, hMem);
			CloseClipboard();
		}
	#endif
		else
		{
			const bool isShiftPressed = ioKeyboard::KeyDown(KEY_SHIFT) != 0;
			for(s32 i = 0; i < g_NumKeyMapEntries; i++)
			{
				if(m_KeyboardIndex < kKeyboardBufferSize-2 &&
					ioKeyboard::KeyPressed(g_KeyMap[i].scanCode))
				{
					if((!isShiftPressed&&!g_KeyMap[i].isAlphaCharacter) || m_AllowAlphaChars)
					{
						m_KeyboardBuffer[m_KeyboardIndex++]  = (isShiftPressed ? g_KeyMap[i].shiftCharacter : g_KeyMap[i].character); 
					}
				}
			}
			if(m_KeyboardIndex > 0 && ioKeyboard::KeyPressed(KEY_BACK))
			{
				if(ioKeyboard::KeyDown(KEY_SHIFT))
				{
					m_KeyboardIndex = 0;
					sysMemZeroBytes<sizeof(m_KeyboardBuffer)>(&m_KeyboardBuffer);
				}
				else
					m_KeyboardIndex--;
				m_KeyboardBuffer[m_KeyboardIndex] = 0;
			}
			
			m_InputLabel.SetString(m_KeyboardBuffer);
		}
	}
	m_InputEnabled = true;
}

void synthUITextBoxInput::Reset()
{
	// clear keyboard buffer
	audDisplayf("keyboard input mode");
	m_KeyboardIndex = 0;
	sysMemZeroBytes<sizeof(m_KeyboardBuffer)>(&m_KeyboardBuffer);
	m_HadEnter = false;
	m_InputEnabled = false;
}

void synthUITextBoxInput::SetValue(const char *val)
{
	strcpy(m_KeyboardBuffer, val);
	m_KeyboardIndex = (int)strlen(val);
}

void synthUITextBoxInput::Render()
{
	SetShaderVar("g_HighlightScale",1.f);
	SetShaderVar("g_AlphaFade", 1.f);
	SetShaderVar("g_TextColour", m_HighlightColour);
	SetShaderVar("g_EdgeHighlightColour", m_HighlightColour);

	Matrix34 mtx(M34_IDENTITY);
	Vector3 scalingVec = Vector3(0.5f,0.15f,1.f);
	mtx.Scale(scalingVec);
	grcWorldMtx(mtx);

	DrawVB("drawmodule");
	//if(m_DrawReflection)
	//{
	//	mtx.d += mtx.b * ComputeHeight();
	//	grcWorldMtx(mtx);
	//	DrawVB(GetShader()->LookupTechnique("drawscrollbarrefl", true));
	//}

	
	mtx.d += Vector3(0.f,0.1f,0.f);
	mtx.Scale(Vector3(1.f,3.5f,1.f));
	m_TitleLabel.SetMatrix(mtx);
	synthUIView::Render();

	mtx.d.Zero();
	m_InputLabel.SetMatrix(mtx);
	m_InputLabel.Draw();

}

} // namespace rage
#endif // __SYNTH_EDITOR
