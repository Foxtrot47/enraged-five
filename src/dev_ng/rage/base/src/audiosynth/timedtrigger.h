
#ifndef SYNTH_TIMEDTRIGGER_H
#define SYNTH_TIMEDTRIGGER_H


#include "envelopegenerator.h"
#include "synthdefs.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{
	enum
	{
		kTimedTriggerInputTrigger = 0,
		kTimedTriggerPredelay,
		kTimedTriggerTime1,
		kTimedTriggerTime2,
		kTimedTriggerTime3,
		kTimedTriggerTime4,
		
		
		kTimedTriggerNumInputs
	};

	typedef synthEnvelopeGenerator::synthEnvelopeTriggerMode TriggerMode;

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
	class synthTimedTrigger : public synthModuleBase<kTimedTriggerNumInputs, 5, SYNTH_TIMEDTRIGGER>
	{
	public:
		
		SYNTH_MODULE_NAME("TimedTrigger");
		SYNTH_CUSTOM_MODULE_VIEW;
		
		synthTimedTrigger();

		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();
		virtual void ResetProcessing();

		TriggerMode GetMode() const { return m_TriggerMode; }
		void SetMode(const TriggerMode mode) { m_TriggerMode = mode; }

		static float Process(const TriggerMode triggerMode, const float trigger, const float predelay, Vec::Vector_4V_InOut state, float &inOut1, float &inOut2, float &inOut3, float &inOut4);
	
	private:

		enum TimedTriggerState
		{
			IDLE = 0,
			PREDELAY,
			TRIGGER1_WAIT,
			TRIGGER1,
			TRIGGER2_WAIT,
			TRIGGER2,
			TRIGGER3_WAIT,
			TRIGGER3,
			TRIGGER4_WAIT,
			TRIGGER4,			
			FINISHED,
		};


		Vec::Vector_4V m_State;
		TriggerMode m_TriggerMode;
		
	};
#if __WIN32
#pragma warning(pop)
#endif
}

#endif // SYNTH_TIMEDTRIGGER_H


