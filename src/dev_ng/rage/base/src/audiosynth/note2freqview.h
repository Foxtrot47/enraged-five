// 
// audiosynth/note2freqview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_NOTE2FREQVIEW_H
#define SYNTH_NOTE2FREQVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uilabel.h"

namespace rage
{
	class synthNoteToFrequencyView : public synthModuleView
	{
	public:

		synthNoteToFrequencyView(synthModule *module);

		virtual void Update();

	protected:
		virtual u32 GetModuleHeight() const { return 2; }

	private:
		const char *ComputeNoteName(const f32 note);

		synthUILabel m_Label;
		char m_String[64];
		char m_NoteString[8];
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_NOTE2FREQVIEW_H

