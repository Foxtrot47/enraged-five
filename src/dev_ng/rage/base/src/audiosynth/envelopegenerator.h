// 
// audiosynth/envelopegenerator.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_ENVELOPEGENERATOR_H
#define SYNTH_ENVELOPEGENERATOR_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "module.h"
#include "modulefactory.h"
#include "pin.h"
#include "synthdefs.h"

#define AUD_FAST_ENVELOPE_GENERATOR 1

namespace rage
{

	enum synthEnvelopeGeneratorInputs
	{
		kEnvelopeGeneratorPredelay = 0,
		kEnvelopeGeneratorAttack,
		kEnvelopeGeneratorDecay,
		kEnvelopeGeneratorSustain,
		kEnvelopeGeneratorHold,
		kEnvelopeGeneratorRelease,
		kEnvelopeGeneratorTrigger,
		
		kEnvelopeGeneratorNumInputs
	};

	enum synthEnvelopeGeneratorOutputs
	{
		kEnvelopeGeneratorEnvelope,

		kEnvelopeGeneratorAttackActive,
		kEnvelopeGeneratorDecayActive,
		kEnvelopeGeneratorHoldActive,
		kEnvelopeGeneratorReleaseActive,

		kEnvelopeGeneratorFinished,
		
		kEnvelopeGeneratorNumOutputs
	};
#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
	class synthEnvelopeGenerator : public synthModuleBase<kEnvelopeGeneratorNumInputs, kEnvelopeGeneratorNumOutputs, SYNTH_ENVELOPEGENERATOR>
	{
	public:
		SYNTH_MODULE_NAME("EG");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthEnvelopeGenerator();
		~synthEnvelopeGenerator();

		virtual void SerializeState(synthModuleSerializer *serializer);

		virtual void Synthesize();
		virtual void ResetProcessing();

		enum synthEnvelopeReleaseType
		{
			Linear = 0,
			Exponential
		};

		enum synthEnvelopeTriggerMode
		{
			OneShot = 0,
			Retrigger,
			Interruptible
		};
		
		void SetReleaseType(const synthEnvelopeReleaseType releaseType)
		{
			m_ReleaseType = releaseType;
		}
		synthEnvelopeReleaseType GetReleaseType() const{ return m_ReleaseType; }

		void SetTriggerMode(const synthEnvelopeTriggerMode mode)
		{
			m_TriggerMode = mode;
		}

		synthEnvelopeTriggerMode GetTriggerMode() const { return m_TriggerMode; }

		static float Process(const synthEnvelopeReleaseType releaseType, synthEnvelopeTriggerMode triggerMode, 
								float *RESTRICT const outputBuffer,
								const u32 numSamples,
								Vec::Vector_4V_InOut state,
								const float predelay, 
								const float attack, 
								const float decay,
								const float sustain,
								const float hold,
								const float release,
								const float trigger
								);

	private:
		void ResetRamp(Vec::Vector_4V_In lengthSeconds);
		static u32 ComputeRamp(const float lengthSeconds, Vec::Vector_4V_InOut phaseSampleDelta);
		static void ProcessLinearRamp(float *RESTRICT outputBuffer, const u32 numSamples, Vec::Vector_4V_InOut phase, Vec::Vector_4V_In perSampleDelta);
		static void ProcessExponentialRelease(float *RESTRICT outputBuffer, Vec::Vector_4V_In releaseStartGain, const u32 numSamples);

#if !AUD_FAST_ENVELOPE_GENERATOR
		f32 m_CurrentFactor;
		f32 m_FactorSampleDelta;
		f32 m_ReleaseStartGain;
#else
		Vec::Vector_4V m_Phase;
		Vec::Vector_4V m_PhaseDelta;
		Vec::Vector_4V m_ReleaseStartGain;
#endif

		Vec::Vector_4V m_StateVec;
		Vec::Vector_4V m_TriggerState;

		synthSampleFrame m_Buffer;

		enum synthEnvelopeGeneratorState
		{
			IDLE_STATE,
			PREDELAY_STATE,
			ATTACK_STATE,
			DECAY_STATE,
			INF_HOLD_STATE,
			HOLD_STATE,
			RELEASE_STATE,
			FINISHED_STATE
		};

		u32 m_SamplesTilStateChange;


		synthEnvelopeReleaseType m_ReleaseType;
		synthEnvelopeTriggerMode m_TriggerMode;
	};
}
#if __WIN32
#pragma warning(pop)
#endif
#endif
#endif // SYNTH_PWMNOISE_H
