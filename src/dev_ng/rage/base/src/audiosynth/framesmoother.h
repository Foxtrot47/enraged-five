
#ifndef SYNTH_FRAMESMOOTHER_H
#define SYNTH_FRAMESMOOTHER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthFrameSmoother : public synthModuleBase<1,1,SYNTH_FRAMESMOOTHER>
	{
	public:

		synthFrameSmoother();
		virtual ~synthFrameSmoother(){};

		virtual const char *GetName() const
		{
			return "Frame Smoother";
		}

		virtual void Synthesize();
		virtual void ResetProcessing();

	private:

		synthSampleFrame m_Buffer;

		f32 m_LastVal;
	};
}
#endif
#endif // SYNTH_FRAMESMOOTHER_H


