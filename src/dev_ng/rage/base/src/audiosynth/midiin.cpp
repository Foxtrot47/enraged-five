
#include "midiin.h"

#if !SYNTH_MINIMAL_MODULES

#include "midiinview.h"

#include "audiohardware/driver.h"
#include "audioengine/engineutil.h"
#include "system/param.h"

namespace rage
{
	
	SYNTH_EDITOR_VIEW(synthMidiIn);

	const char * g_MidiInPinNames[] = {
		"V1 Gate","V1 Note","V1 Vel",
		"V2 Gate","V2 Note","V2 Vel",
		"V3 Gate","V3 Note","V3 Vel",
		"V4 Gate","V4 Note","V4 Vel",
		"V5 Gate","V5 Note","V5 Vel",
		"V6 Gate","V6 Note","V6 Vel",
		"V7 Gate","V7 Note","V7 Vel",
		"V8 Gate","V8 Note","V8 Vel",
		"V9 Gate","V9 Note","V9 Vel",
		"V10 Gate","V10 Note","V10 Vel",
		"V11 Gate","V11 Note","V11 Vel",
		"V12 Gate","V12 Note","V12 Vel",
		"V13 Gate","V13 Note","V13 Vel",
		"V14 Gate","V14 Note","V14 Vel",
		"V15 Gate","V15 Note","V15 Vel",
		"V16 Gate","V16 Note","V16 Vel",
	};
	

	synthMidiIn::synthMidiIn()
	{
		CompileTimeAssert(kMidiInMaxPolyphony==16);

		m_Polyphony = 1;
		m_Channel = 0;
		m_NoteMask = 0xff;

		for(u32 i = 0; i < NumOutputs; )
		{
			m_Outputs[i].SetStaticValue(0.f);
			m_Outputs[i].Init(this, g_MidiInPinNames[i], synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
			i++;
			m_Outputs[i].SetStaticValue(0.f);
			m_Outputs[i].Init(this, g_MidiInPinNames[i], synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
			i++;
			m_Outputs[i].SetStaticValue(0.f);
			m_Outputs[i].Init(this, g_MidiInPinNames[i], synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
			i++;
		}

		for(u32 i = 0; i < 128; i++)
		{
			m_NoteState[i].voiceId = 0xff;
			m_NoteState[i].velocity = 0;
		}

		for(u32 i = 0; i < kMidiInMaxPolyphony; i++)
		{
			m_VoiceTimer[i] = 0;
		}
	}

	void synthMidiIn::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("Polyphony", m_Polyphony);
		serializer->SerializeField("Channel", m_Channel);
				
		if(serializer->IsLoading())
		{
			f32 noteMaskF32;
			serializer->SerializeField("NoteMask", noteMaskF32);
			m_NoteMask = (u8)noteMaskF32;
		}
		else
		{
			f32 noteMaskF32 = (f32)m_NoteMask;
			serializer->SerializeField("NoteMask", noteMaskF32);
		}
	}

	void synthMidiIn::GetOutputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Outputs[0];
		numPins = m_Polyphony * 3;
	}

	void synthMidiIn::Synthesize()
	{
#if __SYNTH_EDITOR
		MidiMessageFunctor functor;
		functor.Reset<synthMidiIn, &synthMidiIn::ProcessMessage>(this);
		if(!audDriver::GetMidiInput()->ProcessQueue(functor, m_Channel, audMidiIn::kDontEmptyQueue))
		{
			// nothing in the queue
			return;
		}
#endif

		const u32 now = audEngineUtil::GetCurrentTimeInMilliseconds();
		for(s32 i = 127; i >= 0; i--)
		{
			if(m_NoteState[i].velocity > 0)
			{
				if(m_NoteState[i].voiceId == 0xff)
				{
					m_NoteState[i].voiceId = AllocateVoiceId();
				}
				if(m_NoteState[i].voiceId != 0xff)
				{
					u32 gatePin = m_NoteState[i].voiceId*3;
					m_Outputs[gatePin].SetStaticValue(1.f);
					m_Outputs[gatePin+1].SetStaticValue((f32)i);
					m_Outputs[gatePin+2].SetStaticValue(m_NoteState[i].velocity / (f32)0x7f);
				}
			}
			else
			{
				if(m_NoteState[i].voiceId != 0xff)
				{
					u32 gatePin = m_NoteState[i].voiceId*3;
					m_Outputs[gatePin].SetStaticValue(0.f);
					m_Outputs[gatePin+2].SetStaticValue(0.f);
					m_VoiceState.Clear(m_NoteState[i].voiceId);
					m_VoiceTimer[m_NoteState[i].voiceId] = now;
					m_NoteState[i].voiceId = 0xff;
				}
			}
		}
	}

	u8 synthMidiIn::AllocateVoiceId()
	{
		u32 numVoicesInUse = 0;
		u32 voiceToUse = ~0U;
		u32 oldestVoiceTime = ~0U;

		for(u32 i = 0; i < m_Polyphony; i++)
		{
			if(m_VoiceState.IsSet(i))
			{
				numVoicesInUse++;
			}
			else if(m_VoiceTimer[i] <= oldestVoiceTime)
			{
				voiceToUse = i;
				oldestVoiceTime = m_VoiceTimer[i];
			}
		}

		if(numVoicesInUse >= m_Polyphony)
		{
			return 0xff;
		}

		m_VoiceState.Set(voiceToUse);
		return static_cast<u8>(voiceToUse);
	}

	void synthMidiIn::ProcessMessage(const audMidiMessage &message)
	{
		struct channelMessage
		{
			u8 status;
			u8 byte1;
			u8 byte2;
			u8 byte3;
		};

		const channelMessage &m = (const channelMessage&)message.Message;
		
		const bool isNoteOn = (m.status&0xf0) == audMidiIn::kNoteOn && m.byte2 > 0;
		const bool isNoteOff = (m.status&0xf0) == audMidiIn::kNoteOff || m.byte2 == 0;

		const u32 channel = (m.status & 0xf) + 1;

		if(m_Channel == 0 || m_Channel == channel)
		{
			if(m_NoteMask == 0xff || m.byte1 == m_NoteMask)
			{
				if(isNoteOn)
				{
					m_NoteState[m.byte1].velocity = m.byte2;
				}
				else if(isNoteOff)
				{
					m_NoteState[m.byte1].velocity = 0;
				}
			}
		}
	}
}
#endif
