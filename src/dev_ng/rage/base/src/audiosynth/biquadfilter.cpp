// 
// audiosynth/biquadfilter.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "biquadfilter.h"
#include "biquadfilterview.h"
#include "audiohardware/mixer.h"
#include "math/amath.h"

#define Undenormalize(sample) ((((*(u32*)&sample)&0x7f800000)==0)?0.0f:sample)

#if RSG_CPU_X64
#define biquad_filter_freq	0.41667f
#else
#define biquad_filter_freq	0.4975f
#endif

namespace rage {
	using namespace Vec;

	SYNTH_EDITOR_VIEW(synthBiquadFilter);
#if __SYNTH_EDITOR
	const char *synthBiquadFilter::GetModeName(synthBiquadFilterModes mode) const
	{
		static const char *table[] = {
			"LowPass2Pole",
			"HighPass2Pole",
			"BandPass2Pole",
			"BandStop2Pole",
			"LowPass4Pole",
			"HighPass4Pole",
			"BandPass4Pole",
			"BandStop4Pole",
			"PeakingEQ",
			"LowShelf2Pole",
			"LowShelf4Pole",
			"HighShelf2Pole",
			"HighShelf4Pole",
			"All Pass",
			};
		return table[mode];
	}
#endif

	synthBiquadFilter::synthBiquadFilter() 
	{
		m_Inputs[kBiquadFilterSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kBiquadFilterFc].Init(this, "Cutoff", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kBiquadFilterFc].SetStaticValue(0.f);
		m_Inputs[kBiquadFilterGain].Init(this,"Gain",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kBiquadFilterGain].SetStaticValue(1.f);
		m_Inputs[kBiquadFilterResonance].Init(this,"Resonance",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kBiquadFilterResonance].SetStaticValue(0.707f);
		m_Inputs[kBiquadFilterBandwidth].Init(this, "Bandwidth", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kBiquadFilterBandwidth].SetStaticValue(1.f);
		m_Outputs[0].Init(this,"Output",synthPin::SYNTH_PIN_DATA_SIGNAL,true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_MinFrequency = 0.f;
		m_MaxFrequency = 20000.f;
		m_MinResonance = 0.07f;
		m_MaxResonance = 10.f;
		m_MinBandwidth = 1.f;
		m_MaxBandwidth = 20000.f;
		m_Mode		= LowPass2Pole;
		
		// set up identity coefficients
		m_a_0 = 1.0f;
		m_a_1 = 0.0f;
		m_a_2 = 0.0f;
		m_b_1 = 0.0f;
		m_b_2 = 0.0f;

		ResetProcessing();
	}

	void synthBiquadFilter::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("MinFreq", m_MinFrequency);
		serializer->SerializeField("MaxFreq", m_MaxFrequency);
		serializer->SerializeField("MinReso", m_MinResonance);
		serializer->SerializeField("MaxReso", m_MaxResonance);
		serializer->SerializeField("MinWidth", m_MinBandwidth);
		serializer->SerializeField("MaxWidth", m_MaxBandwidth);
		serializer->SerializeEnumField("Mode", m_Mode);
	}

	void synthBiquadFilter::ResetProcessing()
	{
		m_State = V4VConstant(V_ZERO);
	}

	void synthBiquadFilter::Synthesize()
	{
		if(m_LastMode != m_Mode)
		{
			// clear history if mode changes
			m_State = V4VConstant(V_ZERO);
			m_LastMode = m_Mode;
		}

		if(m_Inputs[kBiquadFilterSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// pass static value through
			m_Outputs[0].SetStaticValue(m_Inputs[kBiquadFilterSignal].GetStaticValue());
			return;
		}

		m_Outputs[0].SetDataBuffer(&m_Buffer);
		ComputeCoefficients(0);
		
		// Copy input into output then process replacing 
		sysMemCpy128(m_Buffer.GetBuffer(), m_Inputs[kBiquadFilterSignal].GetDataBuffer()->GetBuffer(), sizeof(float) * kMixBufNumSamples);

		if(Is4Pole())
		{
			Process_4Pole(m_Buffer.GetBuffer(), m_a_0, m_a_1, m_a_2, m_b_1, m_b_2, m_State, kMixBufNumSamples);
		}
		else //2-pole.
		{
			Process_2Pole(m_Buffer.GetBuffer(), m_a_0, m_a_1, m_a_2, m_b_1, m_b_2, m_State, kMixBufNumSamples);
		}

		// Apply gain
		Vector_4V *outPtr = m_Buffer.GetBufferV();
		for(u32 i = 0 ; i < kMixBufNumSamples; i += 4)
		{
			*outPtr = V4Scale(*outPtr, m_Inputs[kBiquadFilterGain].GetNormalizedValueV(i));
			outPtr++;
		}
	}

	void synthBiquadFilter::ComputeCoefficients(const s32 sampleIndex)
	{
		//Clamp the bandwidth to ensure we don't go below DC - and into a world of filter
		//stability pain.
		const f32 requestedCutoffFrequency = m_MinFrequency + (m_MaxFrequency-m_MinFrequency) * m_Inputs[kBiquadFilterFc].GetNormalizedValue(sampleIndex);
		// dont allow any mode other than HPF to reach 0hz
		const f32 cutoffFrequency = (m_Mode != HighPass4Pole&&m_Mode!=HighPass2Pole) ? Max(0.01f,requestedCutoffFrequency) : requestedCutoffFrequency; 
		// dont allow bandwidth to go below 1hz
		const f32 requestedBandwidth = Max(1.f,m_MinBandwidth + (m_MaxBandwidth-m_MinBandwidth) * m_Inputs[kBiquadFilterBandwidth].GetNormalizedValue(sampleIndex));

		const f32 resonance = Max(0.1f,m_MinResonance + (m_MaxResonance-m_MinResonance) * m_Inputs[kBiquadFilterResonance].GetNormalizedValue(sampleIndex));
		f32 bandwidth = Min(requestedBandwidth, cutoffFrequency * 2.0f);

		const bool is4Pole = Is4Pole();
		switch(m_Mode)
		{
		case LowPass2Pole:
		case LowPass4Pole:
			ComputeCoefficients_LowPass(is4Pole, cutoffFrequency, resonance, m_a_0, m_a_1, m_a_2, m_b_1, m_b_2);
			break;

		case HighPass2Pole:
		case HighPass4Pole:
			ComputeCoefficients_HighPass(is4Pole, cutoffFrequency, resonance, m_a_0, m_a_1, m_a_2, m_b_1, m_b_2);
			break;

		case BandPass2Pole:
		case BandPass4Pole:
			ComputeCoefficients_BandPass(cutoffFrequency, bandwidth, m_a_0, m_a_1, m_a_2, m_b_1, m_b_2);
			break;

		case BandStop2Pole:
		case BandStop4Pole:
			ComputeCoefficients_BandStop(cutoffFrequency, bandwidth,  m_a_0, m_a_1, m_a_2, m_b_1, m_b_2);
			break;

		case PeakingEQ:
			ComputeCoefficients_PeakingEQ(cutoffFrequency, bandwidth, resonance, m_a_0, m_a_1, m_a_2, m_b_1, m_b_2);
			break;

		case LowShelf4Pole:
		case LowShelf2Pole:
			ComputeCoefficients_LowShelf(is4Pole, cutoffFrequency, bandwidth, resonance, m_a_0, m_a_1, m_a_2, m_b_1, m_b_2);
			break;

		case HighShelf2Pole:
		case HighShelf4Pole:
			ComputeCoefficients_HighShelf(is4Pole, cutoffFrequency, bandwidth, resonance, m_a_0, m_a_1, m_a_2, m_b_1, m_b_2);
			break;

		case AllPass:
			ComputeCoefficients_AllPass(cutoffFrequency, resonance,  m_a_0, m_a_1, m_a_2, m_b_1, m_b_2);
			break;

		default:
			Assert(0);
		}
	}

	void synthBiquadFilter::ComputeCoefficients_LowShelf(const bool UNUSED_PARAM(is4Pole), const float unclampedFreq, const float bandwidth, const float resonance, float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
	{		
		const float fs = kMixerNativeSampleRate;
		const float clampedFreq = Clamp(unclampedFreq, 0.01f, fs * biquad_filter_freq);
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const float twoPiOverFs = 1.3089969389957471826927680763665e-4f;
		const float omega = clampedFreq * twoPiOverFs;
		
		const float clampedBandwidth = Clamp(bandwidth, SMALL_FLOAT, clampedFreq * 2.0f);
		f32 Q = clampedFreq / clampedBandwidth;
		f32 alpha = Sinf(omega) / (2.f * Q);
		f32 cs = Cosf(omega);
		
		f32 A = Max(SMALL_FLOAT, resonance);

		const float sa = 2.f*sqrt(A)*alpha;
		
		const float b0 =    A*( (A+1.f) - (A-1.f)*cs + sa );
		const float b1 =  2.f*A*( (A-1.f) - (A+1.f)*cs);
		const float b2 =    A*( (A+1.f) - (A-1.f)*cs - sa );
		const float a0 =        (A+1.f) + (A-1.f)*cs + sa;
		const float a1 =   -2.f*( (A-1.f) + (A+1.f)*cs);
		const float a2 =        (A+1.f) + (A-1.f)*cs - sa;

		// Normalize so that b0 = 1.0
		a_0 = a0 / b0;
		a_1 = a1 / b0;
		a_2 = a2 / b0;
		b_1 = b1 / b0;
		b_2 = b2 / b0;
	}

	void synthBiquadFilter::ComputeCoefficients_HighShelf(const bool UNUSED_PARAM(is4Pole), const float unclampedFreq, const float bandwidth, const float resonance, float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
	{
		const float fs = kMixerNativeSampleRate;
		const float clampedFreq = Clamp(unclampedFreq, 0.01f, fs * biquad_filter_freq);
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const float twoPiOverFs = 1.3089969389957471826927680763665e-4f;
		const float omega = clampedFreq * twoPiOverFs;
		const float clampedBandwidth = Clamp(bandwidth, SMALL_FLOAT, clampedFreq * 2.0f);
		f32 Q = clampedFreq / clampedBandwidth;
		f32 alpha = Sinf(omega) / (2.f * Q);
		f32 cs = Cosf(omega);

		f32 A = Max(SMALL_FLOAT, resonance);

		const float sa = 2.f*sqrt(A)*alpha;

		const float b0 =    A*( (A+1.f) + (A-1.f)*cs + sa );
		const float b1 = -2.f *A*( (A-1.f) + (A+1.f)*cs);
		const float b2 =    A*( (A+1.f) + (A-1.f)*cs - sa);
		const float a0 =        (A+1.f) - (A-1.f)*cs + sa;
		const float a1 =    2.f *( (A-1.f) - (A+1.f)*cs);
		const float a2 =        (A+1.f) - (A-1.f)*cs - sa;

		// Normalize so that b0 = 1.0
		a_0 = a0 / b0;
		a_1 = a1 / b0;
		a_2 = a2 / b0;
		b_1 = b1 / b0;
		b_2 = b2 / b0;
	}

	void synthBiquadFilter::ComputeCoefficients_LowPass(const bool is4Pole, const float unclampedFreq, const float resonance, float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
	{
		const float fs = kMixerNativeSampleRate;
		const float clampedFreq = Clamp(unclampedFreq, 0.01f, fs * biquad_filter_freq);
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const float twoPiOverFs = 1.3089969389957471826927680763665e-4f;
		const float omega = clampedFreq * twoPiOverFs;
		float a0 = 0.0f, a1 = 0.0f, a2 = 0.0f, b0 = 0.0f, b1 = 0.0f, b2 = 0.0f;

		//Compensate for cascaded sections.
		const float resFactor = is4Pole ? 0.5f : 1.0f;
		float cs = Cosf(omega);
		float sn = Sinf(omega);
		float alpha = sn * Sinhf(0.5f / (resonance * resFactor));

		a0 = (1.0f - cs) / 2.0f;
		a1 =  1.0f - cs;
		a2 = a0;
		b0 = 1.0f + alpha;
		b1 = -2.0f * cs;
		b2 = 1.0f - alpha;

		//Normalize so b0 = 1.0.
		a_0 = a0 / b0;
		a_1 = a1 / b0;
		a_2 = a2 / b0;
		b_1 = b1 / b0;
		b_2 = b2 / b0;
	}

	void synthBiquadFilter::ComputeCoefficients_HighPass(const bool is4Pole, const float unclampedFreq, const float resonance, float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
	{
		const float fs = kMixerNativeSampleRate;
		const float clampedFreq = Clamp(unclampedFreq, 0.01f, fs * biquad_filter_freq);
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const float twoPiOverFs = 1.3089969389957471826927680763665e-4f;
		const float omega = clampedFreq * twoPiOverFs;

		float a0 = 0.0f, a1 = 0.0f, a2 = 0.0f, b0 = 0.0f, b1 = 0.0f, b2 = 0.0f;

		//Compensate for cascaded sections.
		f32 resFactor = is4Pole ? 0.5f : 1.0f;
		f32 cs = Cosf(omega);
		f32 sn = Sinf(omega);
		f32 alpha = sn * Sinhf(0.5f / (resonance * resFactor));

		a0 = (1.0f + cs) / 2.0f;
		a1 = -1.0f - cs;
		a2 = a0;
		b0 = 1.0f + alpha;
		b1 = -2.0f * cs;
		b2 = 1.0f - alpha;

		//Normalize so b0 = 1.0.
		a_0 = a0 / b0;
		a_1 = a1 / b0;
		a_2 = a2 / b0;
		b_1 = b1 / b0;
		b_2 = b2 / b0;
	}

	void synthBiquadFilter::ComputeCoefficients_AllPass(const float unclampedFreq, const float resonance, float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
	{
		const float fs = kMixerNativeSampleRate;
		const float clampedFreq = Clamp(unclampedFreq, 0.01f, fs * biquad_filter_freq);
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const float twoPiOverFs = 1.3089969389957471826927680763665e-4f;
		const float omega = clampedFreq * twoPiOverFs;
				
		f32 cs = Cosf(omega);
		f32 sn = Sinf(omega);
		const float Q = Max(SMALL_FLOAT, resonance);

		f32 alpha = sn / 2.f*Q;

		const float b0 = 1.f - alpha;
		const float b1 = -2.f * cs;
		const float b2 = 1.f + alpha;
		const float a0 = 1.f + alpha;
		const float a1 = -2.f*cs;
		const float a2 = 1.f - alpha;

		//Normalize so b0 = 1.0.
		a_0 = a0 / b0;
		a_1 = a1 / b0;
		a_2 = a2 / b0;
		b_1 = b1 / b0;
		b_2 = b2 / b0;
	}

	void synthBiquadFilter::ComputeCoefficients_BandPass(const float unclampedFreq, const float bandwidth, float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
	{
		const float fs = kMixerNativeSampleRate;
		const float clampedFreq = Clamp(unclampedFreq, 0.01f, fs * biquad_filter_freq);
		
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const float twoPiOverFs = 1.3089969389957471826927680763665e-4f;
		const float omega = clampedFreq * twoPiOverFs;
		const float clampedBandwidth = Clamp(bandwidth, SMALL_FLOAT, clampedFreq * 2.0f);
		const float c = (1.0f / Tanf(PI * (clampedBandwidth / fs)));
		const float d = (2.0f * Cosf(omega));

		a_0 = (1.0f / (1.0f + c));
		a_1 = 0.0f;
		a_2 = -a_0;
		b_1 = -a_0 * c * d;
		b_2 =  a_0 * (c - 1.0f);
	}

	void synthBiquadFilter::ComputeCoefficients_BandStop(const float unclampedFreq, const float bandwidth, float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
	{
		const float fs = kMixerNativeSampleRate;
		const float clampedFreq = Clamp(unclampedFreq, 0.01f, fs * 0.5f);
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const float twoPiOverFs = 1.3089969389957471826927680763665e-4f;
		const float omega = clampedFreq * twoPiOverFs;
		const float d = (2.0f * Cosf(omega));
		const float clampedBandwidth = Clamp(bandwidth, SMALL_FLOAT, clampedFreq * 2.0f);
		const float c = tanf(PI * (clampedBandwidth / fs));
		
		a_0 = (1.0f / (1.0f + c));
		a_1 = -a_0 * d;
		a_2 =  a_0;
		b_1 =  a_1;
		b_2 =  a_0 * (1.0f - c);
	}

	void synthBiquadFilter::ComputeCoefficients_PeakingEQ(const float unclampedFreq, const float bandwidth, const float resonance, float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
	{
#if RSG_CPU_X64
		const float freq = Clamp(unclampedFreq, SMALL_FLOAT, 20000.f);
#else
		const float freq = Clamp(unclampedFreq, SMALL_FLOAT, 23900.f);
#endif
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const float twoPiOverFs = 1.3089969389957471826927680763665e-4f;
		const float omega = freq * twoPiOverFs;
		const float clampedBandwidth = Clamp(bandwidth, SMALL_FLOAT, freq * 2.0f);
		f32 Q = freq / clampedBandwidth;
		f32 alpha = Sinf(omega) / (2.f * Q);
		f32 cs = Cosf(omega);
		//Since we're using resonance as the gain/cut for this peak, and 0dB of resonance for the other filter
		//modes equates to -3.01dB at Fc, our desired gain/cut for this filter is actually 3.01 dB higher than 
		//the linear m_Resonance value stored in our settings structure.  That's why we multiply by 1.4142.
		f32 A = Max(SMALL_FLOAT, resonance * 1.4142f);

		const float a0 = 1.f + alpha * A;
		const float a1 = -2.f * cs;
		const float a2 = 1.f - alpha * A;
		const float b0 = 1.f + alpha / A;
		const float b1 = -2.f * cs;
		const float b2 = 1.f - alpha / A;

		// Normalize so that b0 = 1.0
		a_0 = a0 / b0;
		a_1 = a1 / b0;
		a_2 = a2 / b0;
		b_1 = b1 / b0;
		b_2 = b2 / b0;	
	}		

	void synthBiquadFilter::Process_2Pole(float *RESTRICT inOutBuffer, const float a_0, const float a_1, const float a_2, const float b_1, const float b_2, Vector_4V_InOut state, const u32 numSamples)
	{
#if 0
		// Direct form 1 version:

		float xn_1 = GetX(state);
		float xn_2 = GetY(state);
		float yn_1 = GetZ(state);
		float yn_2 = GetW(state);

		const float b_0 = 1.f;
		const float b0 = b_0 / a_0;
		const float b1 = b_1 / a_0;
		const float b2 = b_2 / a_0;
		const float a1 = a_1 / a_0;
		const float a2 = a_2 / a_0;

		float *ptr = inOutBuffer;
		for(u32 i = 0; i < numSamples; i++)
		{
			const float x = (*ptr);
			float out = b0*x + b1*xn_1 + b2*xn_2 - a1*yn_1 - a2*yn_2;

			yn_2 = yn_1;
			yn_1 = out;
			xn_2 = xn_1;
			xn_1 = x;

			*ptr++ = out;
		
		}

		SetX(state, xn_1);
		SetY(state, xn_2);
		SetZ(state, yn_1);
		SetW(state, yn_2);
#else

		static bool opt = false;
		if(!opt)
		{
			float xy_n1_1 = GetX(state);
			float xy_n2_1 = GetY(state);

			float *samples = inOutBuffer;

			for(u32 j=0; j<numSamples; ++j)
			{
				float currentSample1 = *samples;
				float newSample1 = (a_0 * currentSample1) + xy_n1_1;

				xy_n1_1 = (a_1 * currentSample1) - (b_1 * newSample1) + xy_n2_1;
				xy_n2_1 = (a_2 * currentSample1) - (b_2 * newSample1);

				*samples++ = newSample1;
			}

			SetX(state, xy_n1_1);
			SetY(state, xy_n2_1);
		}
		else	
		{
			Vector_4V xy_n1_1 = V4SplatX(state);
			Vector_4V xy_n2_1 = V4SplatY(state);	
			Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);

			const Vector_4V a0 = V4LoadScalar32IntoSplatted(a_0);
			const Vector_4V a1 = V4LoadScalar32IntoSplatted(a_1);
			const Vector_4V a2 = V4LoadScalar32IntoSplatted(a_2);
			const Vector_4V b1 = V4LoadScalar32IntoSplatted(b_1);
			const Vector_4V b2 = V4LoadScalar32IntoSplatted(b_2);

			u32 count = numSamples >> 2;
			while(count--)
			{
				const Vector_4V currentSamples = *inOutPtr;

				const Vector_4V a0x = V4Scale(a0, currentSamples);
				const Vector_4V a1x = V4Scale(a1, currentSamples);
				const Vector_4V a2x = V4Scale(a2, currentSamples);

				const Vector_4V newSampleX = V4Add(a0x, xy_n1_1);

				xy_n1_1 = V4SplatX(V4Add(V4Subtract(a1x, V4Scale(b1, newSampleX)), xy_n2_1));
				xy_n2_1 = V4SplatX(V4Subtract(a2x, V4Scale(b2, newSampleX)));

				const Vector_4V newSampleY = V4Add(a0x, xy_n1_1);
				xy_n1_1 = V4SplatY(V4Add(V4Subtract(a1x, V4Scale(b1, newSampleY)), xy_n2_1));
				xy_n2_1 = V4SplatY(V4Subtract(a2x, V4Scale(b2, newSampleY)));

				const Vector_4V newSampleZ = V4Add(a0x, xy_n1_1);
				xy_n1_1 = V4SplatZ(V4Add(V4Subtract(a1x, V4Scale(b1, newSampleZ)),xy_n2_1));
				xy_n2_1 = V4SplatZ(V4Subtract(a2x, V4Scale(b2, newSampleZ)));

				const Vector_4V newSampleW = V4Add(a0x, xy_n1_1);
				xy_n1_1 = V4SplatW(V4Add(V4Subtract(a1x, V4Scale(b1, newSampleW)),xy_n2_1));
				xy_n2_1 = V4SplatW(V4Subtract(a2x, V4Scale(b2, newSampleW)));

				Vector_4V outXY = V4PermuteTwo<X1,Y2,X1,Y2>(newSampleX, newSampleY);
				Vector_4V outZW = V4PermuteTwo<X1,Y2,X1,Y2>(newSampleZ, newSampleW);
				Vector_4V out = V4PermuteTwo<X1,Y1,X2,Y2>(outXY, outZW);

				*inOutPtr++ = out;
			}

			state = V4PermuteTwo<X1,X2,X1,X2>(xy_n1_1, xy_n2_1);
		}
#endif
	}

	void synthBiquadFilter::Process_4Pole(float *RESTRICT inOutBuffer, const float a_0, const float a_1, const float a_2, const float b_1, const float b_2, Vector_4V_InOut state, const u32 numSamples)
	{
#if 1
		f32 currentSample1, currentSample2, newSample1, newSample2;

		float xy_n1_1 = GetX(state);
		float xy_n2_1 = GetY(state);
		float xy_n1_2 = GetZ(state);
		float xy_n2_2 = GetW(state);

		for(u32 j = 0; j < numSamples; j++)
		{
			currentSample1 = *inOutBuffer;
			newSample1 = (a_0 * currentSample1) + xy_n1_1;
			currentSample2 = newSample1; //Current sample is now output of last stage.
			newSample2 = (a_0 * currentSample2) + xy_n1_2;

			xy_n1_1 = (a_1 * currentSample1) - (b_1 * newSample1) + xy_n2_1;
			xy_n1_2 = (a_1 * currentSample2) - (b_1 * newSample2) + xy_n2_2;
			xy_n2_1 = (a_2 * currentSample1) - (b_2 * newSample1);
			xy_n2_2 = (a_2 * currentSample2) - (b_2 * newSample2);

			*inOutBuffer++ = newSample2;
		}

		SetX(state, xy_n1_1);
		SetY(state, xy_n2_1);
		SetZ(state, xy_n1_2);
		SetW(state, xy_n2_2);
#else
		ALIGNAS(16) float coefficients[4]  = {a_0, a_1, a_2, b_1};

		const Vector_4V b2 = V4LoadScalar32IntoSplatted(b_2);

		Vector_4V xy_n1_1 = V4SplatX(state);
		Vector_4V xy_n2_1 = V4SplatY(state);
		Vector_4V xy_n1_2 = V4SplatZ(state);
		Vector_4V xy_n2_2 = V4SplatW(state);

		Vector_4V coefficientsV = *(Vector_4V*)coefficients;

		const Vector_4V a0 = V4SplatX(coefficientsV);
		const Vector_4V a1 = V4SplatY(coefficientsV);
		const Vector_4V a2 = V4SplatZ(coefficientsV);
		const Vector_4V b1 = V4SplatW(coefficientsV);

		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);

		u32 count = numSamples >> 2;
		while(count--)
		{
			const Vector_4V currentSamples = *inOutPtr;

			const Vector_4V a0x = V4Scale(a0, currentSamples);
			const Vector_4V a1x = V4Scale(a1, currentSamples);
			const Vector_4V a2x = V4Scale(a2, currentSamples);

			const Vector_4V newSample1X = V4Add(a0x, xy_n1_1);
			const Vector_4V newSample2X = V4AddScaled(xy_n1_2, a0, newSample1X);

			xy_n1_1 = V4SplatX(V4Add(V4Subtract(a1x, V4Scale(b1, newSample1X)), xy_n2_1));
			xy_n1_2 = V4SplatX(V4Add(V4Subtract(V4Scale(a1, newSample2X), V4Scale(b1, newSample2X)), xy_n2_2));
			xy_n2_1 = V4SplatX(V4Subtract(a2x, V4Scale(b2, newSample1X)));
			xy_n2_2 = V4SplatX(V4Subtract(V4Scale(a2, newSample2X), V4Scale(b2, newSample2X)));

			const Vector_4V newSample1Y = V4Add(a0x, xy_n1_1);
			const Vector_4V newSample2Y = V4AddScaled(xy_n1_2, a0, newSample1Y);

			xy_n1_1 = V4SplatY(V4Add(V4Subtract(a1x, V4Scale(b1, newSample1Y)), xy_n2_1));
			xy_n1_2 = V4SplatY(V4Add(V4Subtract(V4Scale(a1, newSample2Y), V4Scale(b1, newSample2Y)), xy_n2_2));
			xy_n2_1 = V4SplatY(V4Subtract(a2x, V4Scale(b2, newSample1Y)));
			xy_n2_2 = V4SplatY(V4Subtract(V4Scale(a2, newSample2Y), V4Scale(b2, newSample2Y)));


			const Vector_4V newSample1Z = V4Add(a0x, xy_n1_1);
			const Vector_4V newSample2Z = V4AddScaled(xy_n1_2, a0, newSample1Z);

			xy_n1_1 = V4SplatZ(V4Add(V4Subtract(a1x, V4Scale(b1, newSample1Z)), xy_n2_1));
			xy_n1_2 = V4SplatZ(V4Add(V4Subtract(V4Scale(a1, newSample2Z), V4Scale(b1, newSample2Z)), xy_n2_2));
			xy_n2_1 = V4SplatZ(V4Subtract(a2x, V4Scale(b2, newSample1Z)));
			xy_n2_2 = V4SplatZ(V4Subtract(V4Scale(a2, newSample2Z), V4Scale(b2, newSample2Z)));

			const Vector_4V newSample1W = V4Add(a0x, xy_n1_1);
			const Vector_4V newSample2W = V4AddScaled(xy_n1_2, a0, newSample1W);

			xy_n1_1 = V4SplatW(V4Add(V4Subtract(a1x, V4Scale(b1, newSample1W)), xy_n2_1));
			xy_n1_2 = V4SplatW(V4Add(V4Subtract(V4Scale(a1, newSample2W), V4Scale(b1, newSample2W)), xy_n2_2));
			xy_n2_1 = V4SplatW(V4Subtract(a2x, V4Scale(b2, newSample1W)));
			xy_n2_2 = V4SplatW(V4Subtract(V4Scale(a2, newSample2W), V4Scale(b2, newSample2W)));

			Vector_4V outXY = V4PermuteTwo<X1,Y2,X1,Y2>(newSample2X, newSample2Y);
			Vector_4V outZW = V4PermuteTwo<X1,Y2,X1,Y2>(newSample2Z, newSample2W);
			Vector_4V out = V4PermuteTwo<X1,Y1,X2,Y2>(outXY, outZW);

			*inOutPtr++ = out;
		}


		Vector_4V stateXY = V4PermuteTwo<X1,Y2,X1,Y2>(xy_n1_1, xy_n2_1);
		Vector_4V stateZW = V4PermuteTwo<X1,Y2,X1,Y2>(xy_n1_2, xy_n2_2);
		state = V4PermuteTwo<X1,Y1,X2,Y2>(stateXY, stateZW);
#endif
}

} // namespace rage
#endif
