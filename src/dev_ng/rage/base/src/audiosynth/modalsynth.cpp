
#include "modalsynth.h"
#include "math/random.h"
#include "system/memops.h"
#include "vectormath/vectormath.h"
#include "vectormath/classes.h"

#include "audiohardware/mixer.h"
#include "audioengine/engineutil.h"

#include "audioengine/curve.h"

#include "kiss_fftr.h"

namespace rage
{
	using namespace Vec;
	synthModalSynth::synthModalSynth() : synthModule(SYNTH_MODALSYNTH)
	{
		m_Inputs[AMPLITUDE].Init(this, "Amplitude",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[AMPLITUDE].SetStaticValue(1.f);

		m_Inputs[RELEASE].Init(this, "RTime",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[RELEASE].SetStaticValue(0.5f);

		m_Inputs[FREQUENCY].Init(this, "Fc", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[FREQUENCY].SetStaticValue(0.f);

		m_Inputs[NOISE].Init(this, "Noise", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[NOISE].SetStaticValue(0.f);

		m_Output.Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Output.SetDataBuffer(&m_Buffer);

		m_FftConfig = kiss_fftr_alloc(256, 1, NULL, NULL);
		sysMemSet(m_FreqData,0, sizeof(m_FreqData));

		m_ReleaseCurve.Init("DEFAULT_RELEASE_CURVE");
		
		sysMemSet(m_Modes, 0, sizeof(m_Modes));
		InitModes();
		// each bin is 48000/512 = 93.75hz
	}

	synthModalSynth::~synthModalSynth()
	{

	}

	void synthModalSynth::InitModes()
	{
		m_NumModes = 12;
		const s32 binOffset = (s32)(m_Inputs[FREQUENCY].GetStaticValue() * 0.5f * (257-m_NumModes));

		for(s32 i = 0; i < m_NumModes; i++)
		{
			m_Modes[i].def.binIndex = i+binOffset;
			m_Modes[i].def.holdSamples = 0;

			m_Modes[i].curSample = 0;
			m_Modes[i].envIndex = -2;
			m_Modes[i].gain = 0.f;

			m_Modes[i].curSample = m_Modes[i].def.delaySamples = 3 * 44100;
			m_Modes[i].def.startGain = audEngineUtil::GetRandomNumberInRange(0.f,1.f);
			m_Modes[i].def.numEnvelopes = 1;

			const f32 freqBasedScalar = 1.f - (i / (f32)m_NumModes);
			m_Modes[i].def.envelope[0].numSamples = (s32)(m_Inputs[RELEASE].GetStaticValue() * audEngineUtil::GetRandomNumberInRange(512.f,44100.f) * (1.f+freqBasedScalar*6.f));
			m_Modes[i].def.envelope[0].delta = 1.f/(f32)m_Modes[i].def.envelope[0].numSamples;
		}
	}
	void synthModalSynth::GetInputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Inputs[0];
		numPins = NUM_MODALSYNTH_INPUTS;
	}

	void synthModalSynth::GetOutputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Output;
		numPins = 1;
	}

	void synthModalSynth::Synthesize()
	{
		bool modesActive = false;
		const s32 samplesPerPass = 256;
		CompileTimeAssert(kMixBufNumSamples>=samplesPerPass);
		for(s32 pass = 0; pass < kMixBufNumSamples/samplesPerPass; pass++)
		{
			const f32 noiseAmount = m_Inputs[NOISE].GetNormalizedValue(pass*samplesPerPass);
			for(s32 modeIndex = 0; modeIndex < m_NumModes; modeIndex++)
			{
				if(m_Modes[modeIndex].envIndex >= 0 && m_Modes[modeIndex].envIndex < m_Modes[modeIndex].def.numEnvelopes)
				{
					// process envelope
					modesActive = true;
					m_Modes[modeIndex].gain += (m_Modes[modeIndex].def.envelope[m_Modes[modeIndex].envIndex].delta*(f32)samplesPerPass);
					m_Modes[modeIndex].curSample -= samplesPerPass;
					if(m_Modes[modeIndex].curSample<=0)
					{
						m_Modes[modeIndex].envIndex++;
						if(m_Modes[modeIndex].envIndex<m_Modes[modeIndex].def.numEnvelopes)
						{
							m_Modes[modeIndex].curSample = m_Modes[modeIndex].def.envelope[m_Modes[modeIndex].envIndex].numSamples;
						}
					}
				}
				else if(m_Modes[modeIndex].envIndex == -2)
				{
					// predelay
					modesActive = true;
					m_Modes[modeIndex].gain = 1.f;
					m_Modes[modeIndex].curSample -= samplesPerPass;
					if(m_Modes[modeIndex].curSample<=0)
					{
						m_Modes[modeIndex].envIndex++;
						m_Modes[modeIndex].curSample = m_Modes[modeIndex].def.holdSamples;
					}
				}
				else if(m_Modes[modeIndex].envIndex == -1)
				{
					// hold phase
					modesActive = true;
					m_Modes[modeIndex].gain = 1.f;//m_Modes[modeIndex].def.startGain;
					m_Modes[modeIndex].curSample -= samplesPerPass;
					if(m_Modes[modeIndex].curSample<=0)
					{
						m_Modes[modeIndex].envIndex++;
						m_Modes[modeIndex].curSample = m_Modes[modeIndex].def.envelope[0].numSamples;
						m_Modes[modeIndex].gain = 0.f;
					}
				}
				else
				{
					// inactive mode
					m_Modes[modeIndex].gain = 1.f;
				}
				const f32 r = audEngineUtil::GetRandomNumberInRange(1.f - noiseAmount,1.f + noiseAmount);
				m_FreqData[m_Modes[modeIndex].def.binIndex].i = m_ReleaseCurve.CalculateValue(m_Modes[modeIndex].gain) * m_Modes[modeIndex].def.startGain * r;
			}
			kiss_fftri(m_FftConfig, m_FreqData, &m_Output.GetDataBuffer()->GetBuffer()[pass*samplesPerPass]);

			if(!modesActive)
			{
				InitModes();
			}
		}

		
		for(u32 i = 0; i < m_Output.GetDataBuffer()->GetSize(); i+=4)
		{
			Vector_4V samples = m_Output.GetSignalValueV(i);
			Vector_4V ampl = m_Inputs[AMPLITUDE].GetNormalizedValueV(i);
			*((Vector_4V*)&m_Output.GetDataBuffer()->GetBuffer()[i]) = V4Scale(V4Scale(samples, ampl),V4VConstant(V_HALF));
		}
	}

}
