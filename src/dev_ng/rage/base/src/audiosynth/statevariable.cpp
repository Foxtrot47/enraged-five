
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "statevariable.h"
#include "svfilterview.h"
#include "sampleframe.h"
#include "math/amath.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthStateVariableFilter);

	synthStateVariableFilter::synthStateVariableFilter() 
	{
		m_MinFrequency = 0.f;
		m_MaxFrequency = 6500.f;
		m_MinQ = 0.5;
		m_MaxQ = 10.f;
						
		m_Outputs[kStateVariableFilterLowpass].Init(this, "Low Pass Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[kStateVariableFilterLowpass].SetDataBuffer(&m_Buffer[kStateVariableFilterLowpass]);
		
		m_Outputs[kStateVariableFilterBandpass].Init(this, "Band Pass Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[kStateVariableFilterBandpass].SetDataBuffer(&m_Buffer[kStateVariableFilterBandpass]);
		
		m_Outputs[kStateVariableFilterHighpass].Init(this, "High Pass Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[kStateVariableFilterHighpass].SetDataBuffer(&m_Buffer[kStateVariableFilterHighpass]);
		
		m_Inputs[kStateVariableFilterGain].Init(this, "Gain",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kStateVariableFilterGain].SetStaticValue(1.f);
		m_Inputs[kStateVariableFilterFc].Init(this, "Frequency",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kStateVariableFilterFc].SetStaticValue(1.f);
		m_Inputs[kStateVariableFilterQ].Init(this, "kStateVariableFilterQ",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kStateVariableFilterQ].SetStaticValue(0.f);
		m_Inputs[kStateVariableFilterSignal].Init(this, "Input",synthPin::SYNTH_PIN_DATA_SIGNAL);

		ResetProcessing();
	}

	void synthStateVariableFilter::ResetProcessing()
	{
		m_D1 = 0.f;
		m_D2 = 0.f;
	}
	
	void synthStateVariableFilter::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("MinFreq", m_MinFrequency);
		serializer->SerializeField("MaxFreq", m_MaxFrequency);
		serializer->SerializeField("MinQ", m_MinQ);
		serializer->SerializeField("MaxQ", m_MaxQ);
	}

	void synthStateVariableFilter::Synthesize()
	{
		f32 gain = 1.f;
		f32 q1 = 0.f;
		f32 f1 = 0.f;
		
		if(m_Inputs[kStateVariableFilterSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			return;
		}
		
		if(m_Inputs[kStateVariableFilterGain].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			gain = m_Inputs[kStateVariableFilterGain].GetStaticValue();
		}
		
		if(m_Inputs[kStateVariableFilterFc].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			const f32 freq = m_MinFrequency + m_Inputs[kStateVariableFilterFc].GetStaticValue() * (m_MaxFrequency - m_MinFrequency);
			//f1 = 2.f * M_PI * freq / 48000.f;
			f1 = 2.f * Sinf(PI * freq * g_OneOverNativeSampleRate);
		}
		if(m_Inputs[kStateVariableFilterQ].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			const f32 q = m_MinQ + m_Inputs[kStateVariableFilterQ].GetStaticValue() * (m_MaxQ - m_MinQ);
			q1 = 1.f/q;
		}
		for(u32 i = 0; i < m_Inputs[kStateVariableFilterSignal].GetDataBuffer()->GetSize(); i++)
		{
			if(m_Inputs[kStateVariableFilterFc].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				const f32 freq = m_MinFrequency + m_Inputs[kStateVariableFilterFc].GetNormalizedValue(i) * (m_MaxFrequency - m_MinFrequency);
				//f1 = 2.f * M_PI * freq / 48000.f;
				f1 = 2.f * Sinf(PI * freq * g_OneOverNativeSampleRate);
			}
			if(m_Inputs[kStateVariableFilterQ].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				const f32 q = m_MinQ + m_Inputs[kStateVariableFilterQ].GetNormalizedValue(i) * (m_MaxQ - m_MinQ);
				q1 = 1.f/q;
			}
			
			
			// where Q1 goes from 2 to 0, ie kStateVariableFilterQ goes from .5 to infinity
			const f32 denorm = 1.0e-24f;
			const f32 L = m_D2 + f1 * m_D1 - denorm;
			const f32 H = m_Inputs[kStateVariableFilterSignal].GetSignalValue(i) - L - q1*m_D1;
			const f32 B = f1 * H + m_D1;
			
			
			// store delays
			m_D1 = B;
			m_D2 = denorm + L;
			
			if(m_Inputs[kStateVariableFilterGain].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				gain = m_Inputs[kStateVariableFilterGain].GetNormalizedValue(i);
			}
			
			m_Outputs[kStateVariableFilterLowpass].GetDataBuffer()->GetBuffer()[i] = gain * L;
			m_Outputs[kStateVariableFilterBandpass].GetDataBuffer()->GetBuffer()[i] = gain * B;
			m_Outputs[kStateVariableFilterHighpass].GetDataBuffer()->GetBuffer()[i] = gain * H;
			
		}// for each sample
	}//synthesize
}

#endif
