// 
// audiosynth/note2freqview.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "note2freqview.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"

namespace rage 
{

synthNoteToFrequencyView::synthNoteToFrequencyView(synthModule *module) : synthModuleView(module)
{
	AddComponent(&m_Label);
}

void synthNoteToFrequencyView::Update()
{
	Matrix34 mtx = GetMatrix();
	mtx.d -= mtx.b;
	m_Label.SetMatrix(mtx);
	
	const f32 input = GetModule()->GetInput(0).GetNormalizedValue(0);
	const f32 output = GetModule()->GetOutput(0).GetNormalizedValue(0);

	formatf(m_String, sizeof(m_String), "%s = %.2fHz", ComputeNoteName(input), output);
	m_Label.SetString(m_String);
	m_Label.SetSize(0.275f);

	synthModuleView::Update();
}

const char *synthNoteToFrequencyView::ComputeNoteName(const f32 note)
{
	const s32 noteIndex = round(note);
	const s32 octave = noteIndex / 12;
	const s32 relNoteIndex = Clamp(noteIndex - (octave*12), 0, 11);
	static const char *noteTable[] = {"C","C#","D","D#","E","F","F#","G","G#","A","A#","B"};

	formatf(m_NoteString, sizeof(m_NoteString), "%d: %s%d", noteIndex, noteTable[relNoteIndex], octave-1);
	return m_NoteString;
}

} // namespace rage
#endif // __SYNTH_EDITOR
