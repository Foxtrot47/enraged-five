// 
// audiosynth/rescaler.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_RESCALER_H
#define SYNTH_RESCALER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to declspec(align....
#endif

#if RSG_ORBIS
#undef MAX_INPUT
#endif

namespace rage
{
	class synthRescaler : public synthModuleBase<5,1,SYNTH_RESCALER>
	{
	public:
		SYNTH_MODULE_NAME("Rescaler");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthRescaler();
		
		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();

		enum synthRescalerInputs
		{
			MIN_INPUT = 0,
			MAX_INPUT,
			INPUT,
			MIN_OUTPUT,
			MAX_OUTPUT,
			NUM_RESCALER_INPUTS
		};
		CompileTimeAssert((s32)NUM_RESCALER_INPUTS == (s32)NumInputs);
		
		void SetClampInput(const bool shouldClamp) { m_ClampInput = shouldClamp; }
		bool ShouldClampInput() const { return m_ClampInput; }

		static void Process(float *RESTRICT const inOutBuffer, 
							const float *RESTRICT const minInBuffer,
							const float *RESTRICT const maxInBuffer,
							const float *RESTRICT const minOutBuffer,
							const float *RESTRICT const maxOutBuffer,
							const u32 numSamples);

		static void Process(float *RESTRICT const inOutBuffer,
							const float minIn,
							const float maxIn,
							const float minOut,
							const float maxOut,
							const u32 numSamples);

		static void Process(float *RESTRICT const inOutBuffer,
							float *RESTRICT const otherBuffer,
							const float t,			
							const u32 numSamples);

		static void Process(float *const inOutBuffer,
							const float *const inA,
							const float *const inB,
							const u32 numSamples);

		static float Process(const float input,
							const float minIn,
							const float maxIn,
							const float minOut,
							const float maxOut);

		static void Process(float *RESTRICT const inOutBuffer, const float minOutput, const float maxOutput, const u32 numSamples);

	private:
		synthSampleFrame m_Buffer;
		bool m_ClampInput;
	};
}
#if __WIN32
#pragma warning(pop)
#endif
#endif
#endif // SYNTH_RESCALER_H


