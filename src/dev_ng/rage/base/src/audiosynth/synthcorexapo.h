#ifndef AUD_SYNTHCOREXAPO_H
#define AUD_SYNTHCOREXAPO_H

#if __XENON

#include "audioeffecttypes/rage_xapo.h"
#include "synthcore.h"

namespace rage
{
	struct synthCoreXAPOParam
	{		
		u32 hash;
		union
		{	
			float floatVal;
			u32 u32Val;
		};
	};

	class __declspec(uuid("{6A03F3B2-2870-4d37-8A16-71C413843536}"))
	synthCoreXAPO : public audXAPOBase<synthCoreXAPO, synthCoreXAPOParam>
	{
	public:
		struct Params
		{
			static const u32 SynthName = 0xA266A735;
			static const u32 Enabled = 0x5B12990F;
		};
		synthCoreXAPO();
		virtual ~synthCoreXAPO();

		virtual bool Init();
		
	private:

		void DoProcess(const synthCoreXAPOParam& params, float* __restrict pData, u32 numFrames, u32 numChannels);
		void OnSetParameters(const synthCoreXAPOParam& params);

		synthCore m_Synth;
		u32 m_OutputBufferId;
		bool m_Enabled;
	};

} // namespace rage

#endif // __XENON
#endif // AUD_SYNTHCOREXAPO_H
