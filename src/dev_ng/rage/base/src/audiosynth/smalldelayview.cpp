// 
// audiosynth/smalldelayview.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "smalldelay.h"
#include "smalldelayview.h"

#if __SYNTH_EDITOR

namespace rage {

	synthSmallDelayView::synthSmallDelayView(synthModule *module) : synthModuleView(module)
	{
		m_Mode.AddItem("Fractional");
		m_Mode.AddItem("NonInterpolating");
		AddComponent(&m_Mode);

		m_Mode.SetCurrentIndex(GetDelay()->GetMode());
	}

	synthSmallDelayView::~synthSmallDelayView()
	{
		
	}

	void synthSmallDelayView::Update()
	{
		GetDelay()->SetMode((synthSmallDelay::Mode)m_Mode.GetCurrentIndex());
		Matrix34 mat = GetMatrix();
		Vector3 offset = -mat.b;
		mat.d += offset;

		m_Mode.SetMatrix(mat);
		m_Mode.SetAlpha(GetHoverFade());

		synthModuleView::Update();
		m_TitleLabel.SetShouldDraw(!m_Mode.IsShowingMenu());
	}

	bool synthSmallDelayView::StartDrag() const
	{
		if(m_Mode.IsUnderMouse() || m_Mode.IsShowingMenu())
		{
			return false;
		}
		return synthModuleView::StartDrag();
	}

} // namespace rage
#endif // __SYNTH_EDITOR
