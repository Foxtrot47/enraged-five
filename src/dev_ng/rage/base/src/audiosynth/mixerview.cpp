// 
// audiosynth/mixerview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "mixerview.h"
#include "mixer.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"
#include "uiplane.h"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	synthMixerView::synthMixerView(synthModule *module) : synthModuleView(module)
	{
		for(u32 i = 0; i < kMaxSignalInputs; i++)
		{
			m_PostGainPins[i].Init(GetMixer(), "PostFader",synthPin::SYNTH_PIN_DATA_SIGNAL);
			AddComponent(m_PostGainPins[i].GetView());
		}
	}

	void synthMixerView::Update()
	{
		//Calculate post-gain buffers
		synthPin *inputPins;
		u32 numInputPins = 0;
		GetMixer()->GetInputs(inputPins, numInputPins);
		for(u32 i = 0; i < numInputPins; i+=2)
		{
			const u32 signalPinIndex = i>>1;
			if(inputPins[i].IsConnected())
			{
				Vector_4V *RESTRICT outputBuf = (Vector_4V*)m_PostGainBuffers[signalPinIndex].GetBuffer();
				for(u32 sample = 0; sample < m_PostGainBuffers[signalPinIndex].GetSize(); sample += 4)
				{
					Vector_4V inputSignal = inputPins[i].GetSignalValueV(sample);
					Vector_4V gain = inputPins[i+1].GetNormalizedValueV(sample);
					*outputBuf++ = V4Scale(inputSignal,gain);
				}
			}
			else
			{
				sysMemSet(m_PostGainBuffers[signalPinIndex].GetBuffer(), 0, m_PostGainBuffers[signalPinIndex].GetSize()*sizeof(f32));
			}
			m_PostGainPins[signalPinIndex].GetView()->PostSynthesize();
			m_PostGainPins[signalPinIndex].SetDataBuffer(&m_PostGainBuffers[signalPinIndex]);
		}

		synthModuleView::Update();

		for(u32 i = 0; i < kMaxSignalInputs; i++)
		{
			const u32 correspondingSignalInputIndex = i*2;
			Matrix34 mat = GetMatrix();
			// position post gain pin in the middle of the corresponding signal and gain pins
			mat.d = (inputPins[correspondingSignalInputIndex].GetView()->GetMatrix().d + inputPins[correspondingSignalInputIndex+1].GetView()->GetMatrix().d) / 2.f;
			mat.d -= mat.a * 2.f;
			mat.Scale(0.75f,0.75f,1.f);
			m_PostGainPins[i].GetView()->SetMatrix(mat);

			m_PostGainPins[i].GetView()->DisableConnections(true);
			m_PostGainPins[i].GetView()->DisableZooming(true);
			m_PostGainPins[i].GetView()->SetAlpha(GetAlpha());
			m_PostGainPins[i].GetView()->SetShouldDraw(inputPins[correspondingSignalInputIndex].IsConnected() && ShouldDraw() && !IsMinimised());
			m_PostGainPins[i].GetView()->SetShouldDrawReflection(false);
		}

		for(u32 i = numInputPins/2; i < kMaxSignalInputs; i++)
		{
			m_PostGainPins[i].GetView()->SetShouldDraw(false);
		}
	}

	void synthMixerView::Render()
	{
		synthModuleView::Render();

		Vector3 outputPinPos = GetMixer()->GetOutput(0).GetView()->GetMatrix().d;
		outputPinPos.z -= 0.1f;

		for(u32 i = 0; i < kMaxSignalInputs; i++)
		{
			if(GetMixer()->GetInput(i*2).IsConnected())
			{
				Vector3 pinPos = GetMixer()->GetInput(i*2).GetView()->GetMatrix().d;
				pinPos.z -= 0.1f;

				synthModuleView::RenderWire(synthUIPlane::GetMaster()->GetMatrix(), 
					pinPos,
					outputPinPos,
					m_PostGainPins[i].GetView()->GetPeakValue(),
					Color32(0.f,0.f,1.f,1.f), 
					true);

				// render post gain
			}
		}
		
	}

	bool synthMixerView::StartDrag() const
	{
		return synthModuleView::StartDrag();
	}

	bool synthMixerView::IsActive() const
	{
		return IsUnderMouse();
	}
}
#endif // __SYNTH_EDITOR

