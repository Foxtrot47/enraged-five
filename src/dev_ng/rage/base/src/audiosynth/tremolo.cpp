// 
// audiosynth/tremolo.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "tremolo.h"
#include "audiohardware/mixer.h"
#include "audiohardware/src_linear.h"
#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	synthTremolo::synthTremolo()
	{
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[SIGNAL].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[DEPTH].Init(this, "Depth", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[DEPTH].SetStaticValue(1.f);
		m_Inputs[FREQUENCY].Init(this, "Frequency", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[WETMIX].Init(this, "Wet Mix", synthPin::SYNTH_PIN_DATA_NORMALIZED);

		m_DelayLineWriteIndex = 0;
		m_DelayLineReadIndex = 0.f;
		m_DelayLineLength = kMixerNativeSampleRate;
		m_DelayLine = rage_aligned_new(16) f32[m_DelayLineLength];

		m_Inputs[FREQUENCY].SetStaticValue(0.01f);

		m_InterpolationRate = 1.f;
		m_GoingUp = false;

	}

	void FractionalResampleBufferSlow(const unsigned int outputSamples, const float ratio, float trailingFrac, const float *input, float *output)
	{
		f32 index = trailingFrac;
		for(u32 i = 0; i < outputSamples; i++)
		{
			u32 flooredIndex = (u32)index;
			f32 frac = index - floorf(index);
			output[i] = input[flooredIndex] +  (frac * (input[flooredIndex+1] - input[flooredIndex]));
			index = index + ratio;
		}
	}

	void synthTremolo::Synthesize()
	{		
		if(m_Inputs[SIGNAL].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{

			const f32 depth = 1.f + m_Inputs[DEPTH].GetNormalizedValue(0);
			const f32 oneOverDepth = 1.f/depth;

			const Vector_4V *RESTRICT inputPtr = (const Vector_4V *)m_Inputs[SIGNAL].GetDataBuffer()->GetBuffer();	

			// populate delay line with input data
			for(s32 i = 0; i < kMixBufNumSamples/4; i++)
			{
				*((Vector_4V*)&m_DelayLine[m_DelayLineWriteIndex]) = *inputPtr++;
				m_DelayLineWriteIndex = (m_DelayLineWriteIndex + 4) % m_DelayLineLength;
			}

			
			// interpolate output with varying rate
			const s32 numBlocks = 8;
			const s32 numSamplesPerBlock = kMixBufNumSamples / numBlocks;
			f32 *outputBuffer = m_Outputs[0].GetDataBuffer()->GetBuffer();
			u32 numSamplesSoFar = 0;

			while(numSamplesSoFar < kMixBufNumSamples)
			{
				const s32 delayLineReadIndexInt = static_cast<s32>(m_DelayLineReadIndex);
				const f32 trailingFrac = m_DelayLineReadIndex - floorf(m_DelayLineReadIndex);
				const u32 numSamplesThisTime = Min<s32>(Min<s32>(m_DelayLineLength - delayLineReadIndexInt, numSamplesPerBlock),kMixBufNumSamples - numSamplesSoFar);
				if((numSamplesThisTime&3) == 0 && (numSamplesSoFar&3) == 0 && (delayLineReadIndexInt&3) == 0)
				{
					FractionalResampleBuffer(numSamplesThisTime, 
						m_InterpolationRate, 
						trailingFrac, 
						&m_DelayLine[delayLineReadIndexInt],
						outputBuffer+numSamplesSoFar);
				}
				else
				{
					FractionalResampleBufferSlow(numSamplesThisTime, 
						m_InterpolationRate, 
						trailingFrac, 
						&m_DelayLine[delayLineReadIndexInt],
						outputBuffer+numSamplesSoFar);
				}

				numSamplesSoFar += numSamplesThisTime;
				m_DelayLineReadIndex += numSamplesThisTime * m_InterpolationRate;

				const f32 delayLineLength = (f32)m_DelayLineLength;
				while(m_DelayLineReadIndex >= delayLineLength)
				{
					m_DelayLineReadIndex -= delayLineLength;
				}

				if(m_GoingUp)
				{
					m_InterpolationRate += m_Inputs[FREQUENCY].GetNormalizedValue(numSamplesSoFar);
					if(m_InterpolationRate >= depth)
					{
						m_InterpolationRate = depth;
						m_GoingUp = false;
					}
				}
				else
				{
					m_InterpolationRate -= m_Inputs[FREQUENCY].GetNormalizedValue(numSamplesSoFar);
					if(m_InterpolationRate <= oneOverDepth)
					{
						m_InterpolationRate = oneOverDepth;
						m_GoingUp = true;
					}
				}
			}
		}
	}
} // namespace rage
#endif
