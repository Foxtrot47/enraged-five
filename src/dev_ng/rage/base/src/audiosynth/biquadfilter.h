
#ifndef SYNTH_BIQUADFILTER_H
#define SYNTH_BIQUADFILTER_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{
	enum synthBiquadFilterInputs
	{
		kBiquadFilterSignal = 0,
		kBiquadFilterGain,
		kBiquadFilterFc,
		kBiquadFilterBandwidth,
		kBiquadFilterResonance,

		kBiquadFilterNumInputs
	};

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
	class synthBiquadFilter : public synthModuleBase<kBiquadFilterNumInputs, 1, SYNTH_BIQUADFILTER>
	{
	public:

		SYNTH_CUSTOM_MODULE_VIEW;

		synthBiquadFilter();
		virtual ~synthBiquadFilter(){};

		virtual void SerializeState(synthModuleSerializer *serializer);
		virtual void Synthesize();
		virtual void ResetProcessing();

		
		void SetMinFrequency(const f32 freq)
		{
			m_MinFrequency = freq;
		}
		void SetMaxFrequency(const f32 freq)
		{
			m_MaxFrequency = freq;
		}
		f32 GetMinFrequency() const { return m_MinFrequency; }
		f32 GetMaxFrequency() const { return m_MaxFrequency; }

		void SetMinWidth(const f32 width)
		{
			m_MinBandwidth = width;
		}
		void SetMaxWidth(const f32 width)
		{
			m_MaxBandwidth = width;
		}
		f32 GetMinWidth() const { return m_MinBandwidth; }
		f32 GetMaxWidth() const { return m_MaxBandwidth; }

		void SetMinResonance(const f32 res)
		{
			m_MinResonance = res;
		}
		void SetMaxResonance(const f32 res)
		{
			m_MaxResonance = res;
		}
		f32 GetMinResonance() const { return m_MinResonance; }
		f32 GetMaxResonance() const { return m_MaxResonance; }


		virtual const char *GetName() const
		{
			return "BiquadFilter";
		}

		void GetCoefficients(f32 &a0, f32 &a1, f32 &a2, f32 &b1, f32 &b2) const
		{
			a0 = m_a_0;
			a1 = m_a_1;
			a2 = m_a_2;
			b1 = m_b_1;
			b2 = m_b_2;
		}

		enum synthBiquadFilterModes
		{
			LowPass2Pole = 0,
			HighPass2Pole,
			BandPass2Pole,
			BandStop2Pole,

			kFirst4PoleMode,
			LowPass4Pole = kFirst4PoleMode,
			HighPass4Pole,
			BandPass4Pole,
			BandStop4Pole,
			PeakingEQ,

			LowShelf2Pole,
			LowShelf4Pole,
			HighShelf2Pole,
			HighShelf4Pole,

			kNumBiquadModes,

			// Allpass isn't working yet
			AllPass,			
		};
		void SetMode(const synthBiquadFilterModes mode)
		{
			m_Mode = mode;
		}

		u32 GetNumModes()
		{
			return kNumBiquadModes;
		}

		bool Is4Pole() const{ return m_Mode>=kFirst4PoleMode && m_Mode != LowShelf2Pole && m_Mode != HighShelf2Pole && m_Mode != AllPass; }
		const char *GetModeName(synthBiquadFilterModes mode) const;

		synthBiquadFilterModes GetMode() const { return m_Mode; }

		static void Process_2Pole(float *RESTRICT const inOutBuffer, 
			const float a0, const float a1, const float a2, const float b1, const float b2, 
			Vec::Vector_4V_InOut state, const u32 numSamples);
		static void Process_4Pole(float *RESTRICT const inOutBuffer, 
			const float a0, const float a1, const float a2, const float b1, const float b2, 
			Vec::Vector_4V_InOut state, const u32 numSamples);


		static void ComputeCoefficients_LowPass2Pole(const float freq, const float resonance,
														float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
		{
			ComputeCoefficients_LowPass(false, freq, resonance, a_0, a_1, a_2, b_1, b_2);
		}
		static void ComputeCoefficients_HighPass2Pole(const float freq, const float bandwidth,
														float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
		{
			ComputeCoefficients_HighPass(false, freq, bandwidth, a_0, a_1, a_2, b_1, b_2);
		}
		static void ComputeCoefficients_LowPass4Pole(const float freq, const float resonance,
														float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
		{
			ComputeCoefficients_LowPass(true, freq, resonance, a_0, a_1, a_2, b_1, b_2);
		}
		static void ComputeCoefficients_HighPass4Pole(const float freq, const float resonance,
														float &a_0,float &a_1,float &a_2,float &b_1,float &b_2)
		{
			ComputeCoefficients_HighPass(true, freq, resonance, a_0, a_1, a_2, b_1, b_2);
		}
		
		
		static void ComputeCoefficients_PeakingEQ(const float freq, const float bandwidth, const float resonance,
														float &a_0,float &a_1,float &a_2,float &b_1,float &b_2);
		
		static void ComputeCoefficients_LowPass(const bool is4Pole, 
													const float freq, const float resonance,
													float &a_0,float &a_1,float &a_2,float &b_1,float &b_2);
		static void ComputeCoefficients_HighPass(const bool is4Pole, 
													const float freq, const float resonance,
													float &a_0,float &a_1,float &a_2,float &b_1,float &b_2);
		static void ComputeCoefficients_BandPass(const float freq, const float bandwidth, 
													float &a_0,float &a_1,float &a_2,float &b_1,float &b_2);
		static void ComputeCoefficients_BandStop(const float freq, const float bandwidth,
													float &a_0,float &a_1,float &a_2,float &b_1,float &b_2);

		static void ComputeCoefficients_LowShelf(const bool is4Pole, 
													const float freq, const float bandwidth, const float resonance,
													float &a_0,float &a_1,float &a_2,float &b_1,float &b_2);

		static void ComputeCoefficients_HighShelf(const bool is4Pole, 
													const float freq, const float bandwidth, const float resonance,
													float &a_0,float &a_1,float &a_2,float &b_1,float &b_2);

		static void ComputeCoefficients_AllPass(const float freq, const float resonance,
			float &a_0,float &a_1,float &a_2,float &b_1,float &b_2);
		
	private:

		void ComputeCoefficients(const s32 sampleIndex);

		Vec::Vector_4V m_State;

		synthSampleFrame m_Buffer;

		f32 m_MinFrequency;
		f32 m_MaxFrequency;
		f32 m_MinResonance;
		f32 m_MaxResonance;
		f32 m_MinBandwidth;
		f32 m_MaxBandwidth;

		f32 m_a_0;
		f32 m_a_1;
		f32 m_a_2;
		f32 m_b_1;
		f32 m_b_2;

		
		synthBiquadFilterModes m_Mode;
		synthBiquadFilterModes m_LastMode;
	};
}
#if __WIN32
#pragma warning(pop)
#endif
#endif // !SYNTH_MINIMAL_MODULES
#endif // SYNTH_BIQUADFILTER_H


