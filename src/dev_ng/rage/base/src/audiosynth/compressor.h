// 
// audiosynth/compressor.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_COMPRESSOR_H
#define SYNTH_COMPRESSOR_H
#include "synthdefs.h"

#include "atl/array.h"
#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"
#include "vectormath/vectormath.h"

namespace rage
{
	enum synthCompressorInputs
	{
		kCompressorSignal = 0,
		kCompressorThreshold,
		kCompressorRatio,
		kCompressorAttack,
		kCompressorRelease,

		kCompressorNumInputs
	};

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif
	class synthCompressor : public synthModuleBase<kCompressorNumInputs, 1, SYNTH_COMPRESSOR>
	{
	public:
		SYNTH_MODULE_NAME("Compressor_EG");

		synthCompressor();

		virtual void Synthesize();
		virtual void ResetProcessing();

		static void Process(float *inOutPtr, const float threshold, const float ratio, const float attack, const float release, Vec::Vector_4V_InOut state, const u32 numSamples);

	private:
		Vec::Vector_4V m_State;
		synthSampleFrame m_Buffer;

	};
}
#if __WIN32
#pragma warning(pop)
#endif

#endif // SYNTH_COMPRESSOR_H


