// 
// audiosynth/uilabel.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uilabel.h"
#include "grcore/font.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/indexbuffer.h"
#include "grcore/device.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "profile/profiler.h"
#include "string/stringhash.h"

#include "audioengine/engineutil.h"
#include "audiohardware/channel.h"

#define __SUPPORT_FT (__WIN32PC)

#if __SUPPORT_FT
#define __SUPPORT_FT_ONLY(x) x
// Freetype2 includes
#include <ft2build.h>  
#include FT_FREETYPE_H 
#else
#define __SUPPORT_FT_ONLY(x)
#endif

namespace rage
{

	EXT_PF_GROUP(SynthEditorUI);
	PF_TIMER(LabelDraw, SynthEditorUI);

#if __SUPPORT_FT
	FT_Library g_FTLibrary = NULL;
	FT_Face g_FTFace = NULL;
#endif

	bool g_TrueTypeRendering = true;

	grcVertexDeclaration *synthUILabel::sm_VertexDecl = NULL;
	grcVertexBuffer *synthUILabel::sm_VertexBuffer = NULL;
	grcIndexBuffer *synthUILabel::sm_IndexBuffer = NULL;
	u32 synthUILabel::sm_NumTexturesAllocated = 0;

	bool synthUILabel::InitClass()
	{

#if __SUPPORT_FT
		s32 error = FT_Init_FreeType(&g_FTLibrary);
		if(error) 
		{
			audErrorf("Failed to initialise Freetype library");
			return false;
		}
		Assert(g_FTLibrary);

		error = FT_New_Face(g_FTLibrary, "c:\\windows\\fonts\\arial.ttf", 0, &g_FTFace);
		if(error) 
		{
			audErrorf("Failed to load font file");
			return false;
		}
		Assert(g_FTFace);
#endif

		grcFvf fvf;
		fvf.SetPosChannel(true);
		fvf.SetTextureChannel(0,true);
		sm_VertexDecl = grmModelFactory::BuildDeclarator(&fvf, NULL, NULL, NULL);

		if(!AssertVerify(sm_VertexDecl))
		{
			return false;
		}

		sm_VertexBuffer = grcVertexBuffer::Create(4,fvf,false,false,0);
		if(!AssertVerify(sm_VertexBuffer))
		{
			return false;
		}
		grcVertexBufferEditor vertexBufferEditor(sm_VertexBuffer);

		s32 idx = 0;
		vertexBufferEditor.SetPosition(idx, Vector3(-2.f,  1.f, 0.f));
		vertexBufferEditor.SetUV(idx++,0,Vector2(1.f,0.f));

		vertexBufferEditor.SetPosition(idx, Vector3(-2.f, -1.f, 0.f));
		vertexBufferEditor.SetUV(idx++,0,Vector2(1.f,1.f));

		vertexBufferEditor.SetPosition(idx, Vector3( 2.f,  1.f, 0.f));
		vertexBufferEditor.SetUV(idx++,0,Vector2(0.f,0.f));

		vertexBufferEditor.SetPosition(idx, Vector3( 2.f, -1.f, 0.f));
		vertexBufferEditor.SetUV(idx++,0,Vector2(0.f,1.f));

		vertexBufferEditor.Unlock();

		sm_IndexBuffer = grcIndexBuffer::Create(4);
		if(!AssertVerify(sm_IndexBuffer))
		{
			return false;
		}
		u16 *lockPtr = sm_IndexBuffer->LockRW();
		if(!AssertVerify(lockPtr))
		{
			return false;
		}
		int offset = 0;
		lockPtr[offset++] = 0;
		lockPtr[offset++] = 1;
		lockPtr[offset++] = 2;
		lockPtr[offset++] = 3;

		sm_IndexBuffer->UnlockRW();

		
		return true;
	}

	void synthUILabel::ShutdownClass()
	{
#if __SUPPORT_FT
		if(g_FTFace)
		{
			FT_Done_Face(g_FTFace);
			g_FTFace = NULL;
		}
		if(g_FTLibrary)
		{
			FT_Done_FreeType(g_FTLibrary);
			g_FTLibrary = NULL;
		}
#endif

		if(sm_IndexBuffer) delete sm_IndexBuffer;
		if(sm_VertexBuffer) delete sm_VertexBuffer;
		if(sm_VertexDecl) grmModelFactory::FreeDeclarator(sm_VertexDecl);
	}

	void synthUILabel::RenderString(const char *__SUPPORT_FT_ONLY(str), f32 &__SUPPORT_FT_ONLY(horizontalScaling), const bool __SUPPORT_FT_ONLY(calcWidthOnly))
	{
#if __SUPPORT_FT
		const s32 fontSizePixels = 28;
		const s32 strLen = StringLength(str);
		s32 pen_x = 0;
		s32 previousGlyph = 0;
		
		FT_Set_Pixel_Sizes(g_FTFace,0,fontSizePixels);

		FT_GlyphSlot slot = g_FTFace->glyph;

		// compute num pixels required
		for(s32 n = 0; n < strLen; n++)
		{ 
			s32 glyphIndex = FT_Get_Char_Index(g_FTFace,str[n]);

			if(glyphIndex && previousGlyph)
			{
				FT_Vector delta; 
				FT_Get_Kerning(g_FTFace, previousGlyph, glyphIndex, FT_KERNING_DEFAULT, &delta); 
				pen_x += delta.x >> 6; 
			}

			/* load glyph image into the slot (erase previous one) */  
			FT_Load_Glyph(g_FTFace, glyphIndex, FT_LOAD_RENDER); 

			if(n == strLen-1)
			{
				// the glyph could be wider than the advance
				pen_x += Max((s32)(slot->advance.x>>6),slot->bitmap.width) + slot->bitmap_left;
			}
			else
			{
				pen_x += slot->advance.x>>6;
			}
			previousGlyph = glyphIndex;
		}


		Assert(pen_x <= m_MaxWidth);
		m_WidthScalar = pen_x / (f32)m_MaxWidth;

		if(!m_Texture)
		{
			m_Texture = synthUIComponent::CreateTexture(m_MaxWidth, 40, grcImage::A8R8G8B8);
			if(m_Texture)
				sm_NumTexturesAllocated++;
			Assert(m_Texture);
		}	


		horizontalScaling = pen_x / (f32)(fontSizePixels*2.4f);

		if(calcWidthOnly)
		{
			return;
		}

		grcTextureLock lock;

		m_Texture->LockRect(0,0,lock,grcsWrite);
		u32 *textPtrU32 = (u32*)lock.Base;
		
		const s32 MaxWidth = lock.Pitch*8/lock.BitsPerPixel;

		sysMemSet(textPtrU32,0,4*MaxWidth*40);

		pen_x = 0;
		previousGlyph = 0;

		for(s32 n = 0; n < strLen; n++)
		{ 
			s32 glyphIndex = FT_Get_Char_Index(g_FTFace,str[n]);

			if(glyphIndex && previousGlyph)
			{
				FT_Vector delta; 
				FT_Get_Kerning(g_FTFace, previousGlyph, glyphIndex, FT_KERNING_DEFAULT, &delta); 
				pen_x += delta.x >> 6; 
			}

			/* load glyph image into the slot (erase previous one) */  
			s32 error = FT_Load_Glyph(g_FTFace, glyphIndex, FT_LOAD_RENDER); 
			if(error) 
			{
				continue; /* ignore errors */
			}
			
			// now, draw to our target surface
			for(s32 x = 0; x < slot->bitmap.width; x++)
			{
				for(s32 y = 0; y < slot->bitmap.rows; y++)
				{
					s32 pixelX = x + pen_x + slot->bitmap_left;
					//Assert(pixelX>=0);
					//Assert(pixelX<pImage->GetWidth());
					
					s32 pixelY = fontSizePixels/3 + fontSizePixels - y - (fontSizePixels-slot->bitmap_top);
					//Assert(pixelY<pImage->GetHeight());
					if(pixelY>=0&&pixelY<40&&pixelX>=0&&pixelX<MaxWidth)
					{
						const u8 val = slot->bitmap.buffer[x+y*slot->bitmap.width];
						textPtrU32[pixelX + pixelY*MaxWidth] = (val|val<<8|val<<16|val<<24);
					}
				}
			}


			if(m_DrawCaret && n == m_CaretIndex)
			{
				for(s32 x = 0; x < 2; x++)
				{
					for(s32 y = 6; y < 34; y++)
					{
						textPtrU32[x+pen_x + y*MaxWidth] = ~0U;
					}
				}
			}	

			/* increment pen position */  
			pen_x += slot->advance.x >> 6;

			// special case - when caret is at the end of the string
			if(m_DrawCaret && m_CaretIndex==strLen && n == strLen-1)
			{
				for(s32 x = 0; x < 2; x++)
				{
					for(s32 y = 6; y < 34; y++)
					{
						textPtrU32[x+pen_x + y*MaxWidth] = ~0U;
					}
				}
			}

			previousGlyph = glyphIndex;
		}

		m_Texture->UnlockRect(lock);
#else
		
#endif
	}

	synthUILabel::synthUILabel(const s32 maxWidth)
	{
		m_MaxWidth = maxWidth;
		m_Texture = NULL;
		m_String = NULL;
		m_AlwaysDraw = false;
		m_NeedRender = false;
		m_HorizontalScaling = 1.f;
		m_Scaling = 1.f;
		m_ActiveTextColour = m_InactiveTextColour = Color32(1.f,1.f,1.f,1.f);
		m_ActiveShadowColour = Color32(0.2f,0.2f,5.f,1.f);
		m_InactiveShadowColour = Color32(0.f,0.f,0.15f,1.f);
		m_DrawCaret = false;
		m_CaretIndex = 0;
		m_LastTimeRendered = 0;
	}

	synthUILabel::~synthUILabel()
	{
		if(m_Texture)
		{
			m_Texture->Release();
			Assert(sm_NumTexturesAllocated >= 1);
			sm_NumTexturesAllocated--;
			m_Texture = NULL;
		}
	}

	void synthUILabel::Update()
	{
		/*if(m_Texture)
		{
			if(m_LastTimeRendered + 500 < audEngineUtil::GetCurrentTimeInMilliseconds())
			{
				m_Texture->Release();
				Assert(sm_NumTexturesAllocated >= 1);
				sm_NumTexturesAllocated--;
				m_Texture = NULL;
				m_NeedRender = true;
			}
		}*/
	}

	void synthUILabel::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		Matrix34 mtx = GetMatrix();
		Vector3 scaling = Vector3(m_HorizontalScaling,1.f,1.f);
		scaling.Scale(m_Scaling);
		mtx.Transform3x3(scaling);
		mtx.Scale(scaling);

		v1.Set(-2.f,-1.f,0.f);
		v2.Set(2.f,1.f,0.f);
		mtx.Transform(v1);
		mtx.Transform(v2);
	}

	void synthUILabel::SetString(const char *string)
	{
		const u32 stringHash = atStringHash(string);
		if(!m_String || stringHash != m_StringHash)
		{
			m_StringHash = stringHash;
			m_NeedRender = true;
		}
		m_String = string;
	}

	void synthUILabel::Render()
	{
		PF_FUNC(LabelDraw);

		const bool isVisible = (m_Alpha > 0.f);
		if(!isVisible)
			return;

		
		if(m_NeedRender)
		{
			// check screen size
			bool isOnScreenAndBig = false;
			if(grcViewport::GetCurrent())
			{
				const grcViewport *viewport = grcViewport::GetCurrent();
				Vector3 v0,v1;
				GetBounds(v0,v1);

				if(viewport->IsAABBVisible(v0,v1,viewport->GetFrustumLRTB()))
				{
					f32 x0,y0,x1,y1;

					viewport->Transform((Vec3V&)v0,x0,y0);
					viewport->Transform((Vec3V&)v1,x1,y1);
					const f32 xL = x1-x0;
					const f32 yL = y1-y0;
					const f32 screenSize = Sqrtf(xL*xL + yL*yL);

					static f32 lowLodScreenSize = 8.f;
					isOnScreenAndBig = screenSize > lowLodScreenSize;
				}
			}

			if((!m_Texture || m_AlwaysDraw || isOnScreenAndBig || IsUnderMouse()) && m_String)
			{
				RenderString(m_String, m_HorizontalScaling, false);
				m_NeedRender = false;
			}
			else
			{
				RenderString(m_String, m_HorizontalScaling, true);
			}
		}
			
		if(!m_Texture)
		{
			return;
		}
	
		m_LastTimeRendered = audEngineUtil::GetCurrentTimeInMilliseconds();
		
		Matrix34 mtx = GetMatrix();
		Vector3 scaling = Vector3(m_HorizontalScaling,1.f,1.f);
		scaling.Scale(m_Scaling);
		mtx.Transform3x3(scaling);
		mtx.Scale(scaling);

		SetShaderVar("FontTex",m_Texture);
		SetShaderVar("g_TextShadowScale", 1.f);
		SetShaderVar("g_TextColour", VEC3V_TO_VECTOR3((IsUnderMouse() ? m_ActiveTextColour : m_InactiveTextColour).GetRGB()));
		SetShaderVar("g_AlphaFade", m_Alpha);
		SetShaderVar("g_TextShadowColour", VEC3V_TO_VECTOR3((IsUnderMouse() ? m_ActiveShadowColour : m_InactiveShadowColour).GetRGB()));
		
		SetShaderVar("g_WidthScalar",m_WidthScalar);
	

		

		grcWorldMtx(mtx);
		if (GetShader()->BeginDraw(grmShader::RMC_DRAW,true, GetShader()->LookupTechnique("drawantialiased", true))) {
			GetShader()->Bind();
			GRCDEVICE.DrawIndexedPrimitive(drawTriStrip,sm_VertexDecl,*sm_VertexBuffer,*sm_IndexBuffer, sm_IndexBuffer->GetIndexCount());
			GetShader()->UnBind();
			GetShader()->EndDraw();
		}
		else
		{
			audErrorf("synthUILabel: BeginDraw failed.");
		}

	
	}
} // namespace rage
#endif // __SYNTH_EDITOR
