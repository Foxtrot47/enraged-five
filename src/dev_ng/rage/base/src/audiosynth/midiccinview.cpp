// 
// audiosynth/midiccinview.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "audiohardware/driver.h"

#include "midiccinview.h"
#include "midiccin.h"

#include "moduleview.h"
#include "pinview.h"
#include "uiplane.h"

#include "module.h"
#include "pin.h"

namespace rage
{
	synthMidiCCInView::synthMidiCCInView(synthModule *module) : synthModuleView(module), m_DeviceNameLabel(512)
	{
        m_LearnButton.SetTitle("Learn");
        m_LearnButton.SetSize(0.2f);

        m_ControllerSlider.SetRange(0.f, 127.f);
        m_ControllerSlider.SetNormalizedValue(GetMidiCCIn()->GetControllerId() / 127.f);
        m_ControllerSlider.SetWidth(0.4f);
        m_ControllerSlider.SetTitle("Controller Number");
        m_ControllerSlider.SetRenderAsInteger(true);
        m_ControllerSlider.SetDrawTitle(true);

		m_ChannelList.AddItem("All Channels");
		m_ChannelList.AddItem("Channel 1");
		m_ChannelList.AddItem("Channel 2");
		m_ChannelList.AddItem("Channel 3");
		m_ChannelList.AddItem("Channel 4");
		m_ChannelList.AddItem("Channel 5");
		m_ChannelList.AddItem("Channel 6");
		m_ChannelList.AddItem("Channel 7");
		m_ChannelList.AddItem("Channel 8");
		m_ChannelList.AddItem("Channel 9");
		m_ChannelList.AddItem("Channel 10");
		m_ChannelList.AddItem("Channel 11");
		m_ChannelList.AddItem("Channel 12");
		m_ChannelList.AddItem("Channel 13");
		m_ChannelList.AddItem("Channel 14");
		m_ChannelList.AddItem("Channel 15");
		m_ChannelList.AddItem("Channel 16");
		m_ChannelList.SetCurrentIndex(GetMidiCCIn()->GetChannel());

        m_DeviceNameLabel.SetSize(0.2f);
        m_DeviceNameLabel.SetString(audDriver::GetMidiInput()->GetName());

		m_PitchBendSwitch.AddItem("Continuous Controller");
		m_PitchBendSwitch.AddItem("Pitch-bend Controller");
		m_PitchBendSwitch.SetCurrentIndex(GetMidiCCIn()->GetPitchBend() ? 1 : 0);
		m_Layout.Add(&m_PitchBendSwitch);

        m_Layout.Add(&m_ChannelList);
        m_Layout.Add(&m_ControllerSlider);
        m_Layout.Add(&m_LearnButton);
		m_Layout.Add(&m_DeviceNameLabel);

        m_Layout.SetOrientation(synthUIHorizontalLayout::kVertical);
        m_Layout.SetPadding(0.5f);

        AddComponent(&m_Layout);

        m_WasLearning = false;
	}

	void synthMidiCCInView::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		synthModuleView::GetBounds(v1,v2);
		Vector3 v3,v4;
		m_ChannelList.GetBounds(v3,v4);

		v1.Min(v1,v3);
		v2.Max(v2,v4);
	}

	void synthMidiCCInView::Update()
	{
		// fade out title when editing
		if(IsMinimised())
		{
			SetTitleAlpha(1.f);
		}
		else
		{
			SetTitleAlpha(1.f - GetHoverFade());
		}
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();
        mtx.d -= mtx.c*0.1f;
        mtx.d -= mtx.b*0.1f;
        m_Layout.SetMatrix(mtx);

        m_ControllerSlider.SetAlpha(GetHoverFade() * GetAlpha());
		m_ControllerSlider.SetTextAlpha(GetAlpha());
		
		m_ChannelList.SetAlpha(GetAlpha());
		m_ChannelList.SetShouldDraw(ShouldDraw() && !IsMinimised());

		m_DeviceNameLabel.SetAlpha(GetAlpha());
		m_DeviceNameLabel.SetShouldDraw(ShouldDraw() && !m_PitchBendSwitch.IsShowingMenu() && !m_ChannelList.IsShowingMenu() && !IsMinimised());

        m_LearnButton.SetAlpha(GetAlpha());
        m_LearnButton.SetShouldDraw(ShouldDraw() && !GetMidiCCIn()->GetPitchBend() && !m_PitchBendSwitch.IsShowingMenu() && !m_ChannelList.IsShowingMenu() && !IsMinimised());

        if(m_LearnButton.HadClick())
        {
            if(GetMidiCCIn()->IsLearning())
            {
                GetMidiCCIn()->StopLearning();
            }
            else
            {
                GetMidiCCIn()->StartLearning();
            }
        }

        m_LearnButton.SetTitle( GetMidiCCIn()->IsLearning() ? "Learning ..." : "Learn" );

		GetMidiCCIn()->SetChannel(m_ChannelList.GetCurrentIndex());
		SetHideTitle(m_ChannelList.IsShowingMenu() || m_PitchBendSwitch.IsShowingMenu());
		m_ControllerSlider.SetShouldDraw(ShouldDraw() && !GetMidiCCIn()->GetPitchBend() && !m_PitchBendSwitch.IsShowingMenu() && !m_ChannelList.IsShowingMenu() && !IsMinimised());

		m_PitchBendSwitch.SetAlpha(GetAlpha() * GetHoverFade());
		m_PitchBendSwitch.SetShouldDraw(ShouldDraw() && !IsMinimised() && !m_ChannelList.IsShowingMenu());
		GetMidiCCIn()->SetPitchBend(m_PitchBendSwitch.GetCurrentIndex() == 1);

        if(!GetMidiCCIn()->IsLearning())
        {
            if(m_WasLearning)
            {
                m_ControllerSlider.SetNormalizedValue( GetMidiCCIn()->GetControllerId() / 127.f );
            }
            else
            {
                const u32 controllerId = (u32)m_ControllerSlider.GetValue();
		        GetMidiCCIn()->SetControllerId(controllerId);
            }
            m_WasLearning = false;
        }
        else
        {
            m_WasLearning = true;
        }

		synthModuleView::Update();
	}

	void synthMidiCCInView::Render()
	{
		synthModuleView::Render();
	}

	bool synthMidiCCInView::StartDrag() const
	{
		if(m_ControllerSlider.IsUnderMouse() || m_ChannelList.IsUnderMouse() || m_ChannelList.IsShowingMenu() || m_PitchBendSwitch.IsShowingMenu())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthMidiCCInView::IsActive() const
	{
		return (IsUnderMouse() || m_ChannelList.IsActive() || m_ControllerSlider.IsActive());
	}

}
#endif // __SYNTH_EDITOR

