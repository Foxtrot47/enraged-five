

#ifndef SYNTH_FEEDBACKPIN_H
#define SYNTH_FEEDBACKPIN_H

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "pin.h"

#include "modulefactory.h"

namespace rage
{
	class synthFeedbackPin : public synthModuleBase<1,1,SYNTH_FEEDBACKPIN>
	{
	public:
		SYNTH_MODULE_NAME("Feedback");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthFeedbackPin();

		virtual void SerializeState(synthModuleSerializer *serializer);

		virtual void Synthesize();

		bool ProcessInputFirst() const { return m_ProcessInputFirst; }
		void SetProcessInputFirst(const bool processInputFirst) { m_ProcessInputFirst = processInputFirst; }

	private:
		bool m_ProcessInputFirst;
	};
}
#endif // !SYNTH_MINIMAL_MODULES
#endif // SYNTH_FEEDBACKPIN_H


