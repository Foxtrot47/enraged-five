#ifndef SYNTH_NOISEGENERATOR_H
#define SYNTH_NOISEGENERATOR_H

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{

	class synthNoiseGenerator : public synthModuleBase<1,1,SYNTH_NOISEGENERATOR>
	{
	public:	
		SYNTH_MODULE_NAME("Noise");

		synthNoiseGenerator();	
		virtual void Synthesize(synthContext &context);		
	

		// PURPOSE
		//	Produce one buffers worth of noise, scaled by the supplied gain
		// PARAMS
		//	context - current processing context
		//	buffer - destination sample frame
		static void Generate(float *RESTRICT outputBuffer, const u32 numSamples);

	private:
	
		synthSampleFrame m_Buffer;
	};
	
}
#endif // SYNTH_NOISEGENERATOR_H
