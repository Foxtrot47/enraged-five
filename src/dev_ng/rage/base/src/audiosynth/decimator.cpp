// 
// audiosynth/decimator.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "decimator.h"

#include "synthutil.h"
#include "audiohardware/driverutil.h"
#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	synthDecimator::synthDecimator()
	{
		m_Outputs[0].Init(this,"Output", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_Inputs[kDecimatorSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kDecimatorBitDepth].Init(this, "BitDepth", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDecimatorBitDepth].SetStaticValue(1.f);
		m_Inputs[kDecimatorSampleRate].Init(this, "SampleRate", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDecimatorSampleRate].SetStaticValue(1.f);

		m_State = V4VConstant(V_ZERO);
	}

	void synthDecimator::Synthesize()
	{		
		f32 *outputBuffer = m_Outputs[0].GetDataBuffer()->GetBuffer();

		if(m_Inputs[kDecimatorSignal].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			float *inputBuffer = m_Inputs[kDecimatorSignal].GetDataBuffer()->GetBuffer();
			sysMemCpy(outputBuffer, inputBuffer, kMixBufNumSamples * sizeof(float));
			if(m_Inputs[kDecimatorSignal].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
			{
				synthUtil::ConvertBufferToSignal(outputBuffer, kMixBufNumSamples);
			}
			synthDecimator::Process(outputBuffer,
									m_Inputs[kDecimatorBitDepth].GetNormalizedValue(0),
									m_Inputs[kDecimatorSampleRate].GetNormalizedValue(0),
									m_State,
									kMixBufNumSamples);
		}
		else
		{
			sysMemSet(outputBuffer, 0, kMixBufNumSamples * sizeof(float));
		}		
	}

	void synthDecimator::Process(float *inOut, const float bitDepth, const float sampleRate, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		u32 count = numSamples >> 2;

		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOut);
		const Vector_4V bitDepthV = V4LoadScalar32IntoSplatted(bitDepth);
		
		float phase = GetX(state);
		float lastSample = GetY(state);

		// bit depth reduction
		const Vector_4V sixteen = V4VConstant<0x41800000,0x41800000,0x41800000,0x41800000>();
		Vector_4V bits = V4Scale(bitDepthV, sixteen);
		Vector_4V q = audDriverUtil::V4Exp2(bits);

		while(count--)
		{				
			// bit quantisation
			const Vector_4V input = *inOutPtr;
			const Vector_4V output = V4InvScale(V4RoundToNearestIntZero(V4Scale(input, q)),q);

			*inOutPtr++ = output;
		}

		// sample rate / aliasing

		const f32 phaseIncrement = sampleRate;
		
		if(phaseIncrement < 1.f)
		{
			for(u32 i = 0; i < numSamples; i ++)
			{	
				lastSample = phase < 1.f ? lastSample : inOut[i];
				phase -= phase < 1.f ? 0.f : 1.f;				
				phase += phaseIncrement;
				inOut[i] = lastSample;
			}
		}

		SetX(state, phase);
		SetY(state, lastSample);
	}
}


