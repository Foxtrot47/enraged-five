// 
// audiosynth/moduleview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_MODULEVIEW_H
#define SYNTH_MODULEVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uiview.h"
#include "uilabel.h"

#include "atl/array.h"
#include "vector/vector3.h"

namespace rage
{
	class synthModule;
	class synthPinView;
	class synthModuleView : public synthUIView
	{

	public:

		static bool InitClass();
		static void ShutdownClass();

		synthModuleView(synthModule *module);
		virtual ~synthModuleView();

		// synthUIComponent functionality
		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		void RenderWires();
	
		synthModule *GetModule() const { return m_Module; }

		virtual bool StartDrag() const;

		static void RenderWire(const Matrix34 &planeWorldMtx, const Vector3 &p0, const Vector3 &p1, const f32 intensity, const Color32 colour, const f32 highlightAmount = 0.1f);


		bool IsActive() const { return IsUnderMouse(); }
		bool IsMinimised() const { return m_IsMinimised; }

		virtual void PostSynthesize();

		void SetIsCollapsed(const bool is)
		{
			m_IsMinimised = is;
		}
		bool IsCollapsed() const { return m_IsMinimised; }

		virtual float GetProcessingTime() const { return m_MeanProcessingTime; }

		virtual void DrawBackground() const;
		void DrawPins() const;

        virtual void DeleteContents();
        virtual synthUIView *DuplicateContents(const bool recreateConnections);

		virtual bool IsModule() const { return true; }

	protected:
		virtual void Render();
		void SetTitleAlpha(const f32 alpha) { m_TitleAlpha = alpha; }
		void SetHideTitle(const bool hide) { m_HideTitle = hide; }

        virtual bool OverrideBounds() const { return false; }

		virtual u32 GetModuleHeight() const;
		virtual bool DefaultPinPositioning() const { return true; }
		virtual f32 GetModuleWidth() const { return 8.5f; }

		synthUILabel m_TitleLabel;
		
		synthModule *m_Module;

	private:
		synthUILabel m_CPUTimeLabel;
		enum numModuleCPUSamplesEnum {kNumCPUSamples = 10};
		f32 m_CPUTimes[kNumCPUSamples];
		s32 m_CurrentCPUSample;
		char m_CPUTimeBuf[32];

		f32 m_TitleAlpha;
		f32 m_MeanProcessingTime;

		f32 m_DragOffsetX, m_DragOffsetY;
		bool m_IsDragging;
		bool m_IsMinimised;
		bool m_HideTitle;

		static grcTexture *sm_NoiseTexture;

	};
}// namespace rage
#endif // __SYNTH_EDITOR
#endif // SYNTH_MODULEVIEW_H
