// 
// synthobjects.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
// 
// Automatically generated metadata structure definitions - do not edit.
// 

#ifndef AUD_SYNTHOBJECTS_H
#define AUD_SYNTHOBJECTS_H

#include "vector/vector3.h"
#include "vector/vector4.h"
#include "audioengine/metadataref.h"

// macros for dealing with packed tristates
#ifndef AUD_GET_TRISTATE_VALUE
	#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)
	#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) (TristateValue)((flagvar |= ((trival&0x03) << (flagid<<1))))
	namespace rage
	{
		enum TristateValue
		{
			AUD_TRISTATE_FALSE = 0,
			AUD_TRISTATE_TRUE,
			AUD_TRISTATE_UNSPECIFIED,
		}; // enum TristateValue
	} // namespace rage
#endif // !defined AUD_GET_TRISTATE_VALUE

namespace rage
{
	#define SYNTHOBJECTS_SCHEMA_VERSION 10
	
	#define SYNTHOBJECTS_METADATA_COMPILER_VERSION 2
	
	// NOTE: doesn't include abstract objects
	#define AUD_NUM_SYNTHOBJECTS 4
	
	#define AUD_DECLARE_SYNTHOBJECTS_FUNCTION(fn) void* GetAddressOf_##fn(rage::u8 classId);
	
	#define AUD_DEFINE_SYNTHOBJECTS_FUNCTION(fn) \
	void* GetAddressOf_##fn(rage::u8 classId) \
	{ \
		switch(classId) \
		{ \
			case ModularSynth::TYPE_ID: return (void*)&ModularSynth::s##fn; \
			case Preset::TYPE_ID: return (void*)&Preset::s##fn; \
			case PresetList::TYPE_ID: return (void*)&PresetList::s##fn; \
			case CompiledSynth::TYPE_ID: return (void*)&CompiledSynth::s##fn; \
			default: return NULL; \
		} \
	}
	
	// PURPOSE - Gets the type id of the parent class from the given type id
	rage::u32 gModularSynthGetBaseTypeId(const rage::u32 classId);
	
	// PURPOSE - Determines if a type inherits from another type
	bool gModularSynthIsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId);
	
	// PURPOSE - Determines if a type inherits from another type
	template<class _ObjectType>
	bool gModularSynthIsOfType(const _ObjectType* const obj, const rage::u32 baseTypeId)
	{
		return gModularSynthIsOfType(obj->ClassId, baseTypeId);
	}
	
	// disable struct alignment
	#if !__SPU
	#pragma pack(push, r1, 1)
	#endif // !__SPU
		// 
		// ModularSynth
		// 
		enum ModularSynthFlagIds
		{
			FLAG_ID_MODULARSYNTH_EXPORTFORGAME = 0,
		}; // enum ModularSynthFlagIds
		
		struct ModularSynth
		{
			static const rage::u32 TYPE_ID = 0;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			ModularSynth() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			
			static const rage::u16 MAX_MODULES = 1024;
			rage::u16 numModules;
			struct tModule
			{
				rage::u32 TypeId;
				float IsCollapsed;
				float PosX;
				float PosY;
				rage::audMetadataRef PresetList;
				rage::s32 ProcessingStage;
				rage::s32 GroupId;
				
				static const rage::u32 MAX_FIELDVALUES = 16;
				rage::u32 numFieldValues;
				struct tFieldValue
				{
					float Value;
				} FieldValue[MAX_FIELDVALUES]; // struct tFieldValue
				
				
				static const rage::u32 MAX_INPUTPINS = 32;
				rage::u32 numInputPins;
				struct tInputPin
				{
					float StaticValue;
					rage::s16 OtherPin;
					rage::s16 OtherModule;
				} InputPin[MAX_INPUTPINS]; // struct tInputPin
				
				
				static const rage::u32 MAX_OUTPUTPINS = 32;
				rage::u32 numOutputPins;
				struct tOutputPin
				{
					float StaticValue;
					rage::s16 OtherPin;
					rage::s16 OtherModule;
				} OutputPin[MAX_OUTPUTPINS]; // struct tOutputPin
				
			} Module[MAX_MODULES]; // struct tModule
			
			
			static const rage::u8 MAX_EXPOSEDVARIABLES = 128;
			rage::u8 numExposedVariables;
			struct tExposedVariable
			{
				char Key[32];
				rage::s16 ModuleId;
			} ExposedVariable[MAX_EXPOSEDVARIABLES]; // struct tExposedVariable
			
			
			static const rage::u8 MAX_MODULEGROUPS = 255;
			rage::u8 numModuleGroups;
			struct tModuleGroup
			{
				char Name[64];
				rage::s32 ParentGroupId;
				
				struct tColour
				{
					rage::u8 R;
					rage::u8 G;
					rage::u8 B;
				} Colour; // struct tColour
				
			} ModuleGroup[MAX_MODULEGROUPS]; // struct tModuleGroup
			
			
			static const rage::u8 MAX_COMMENTS = 255;
			rage::u8 numComments;
			struct tComments
			{
				
				struct tColour
				{
					rage::u8 R;
					rage::u8 G;
					rage::u8 B;
					rage::u8 A;
				} Colour; // struct tColour
				
				float PosX;
				float PosY;
				rage::s32 GroupId;
				char Text[256];
			} Comments[MAX_COMMENTS]; // struct tComments
			
			
			static const rage::u8 MAX_CAMERAPOSITIONS = 11;
			rage::u8 numCameraPositions;
			struct tCameraPosition
			{
				float PosX;
				float PosY;
				float PosZ;
			} CameraPosition[MAX_CAMERAPOSITIONS]; // struct tCameraPosition
			
		} SPU_ONLY(__attribute__((packed))); // struct ModularSynth
		
		// 
		// Preset
		// 
		
		struct Preset
		{
			static const rage::u32 TYPE_ID = 1;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			Preset() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			
			static const rage::u8 MAX_VARIABLES = 128;
			rage::u8 numVariables;
			struct tVariable
			{
				rage::u32 Name;
				float Value;
				float Variance;
			} Variable[MAX_VARIABLES]; // struct tVariable
			
		} SPU_ONLY(__attribute__((packed))); // struct Preset
		
		// 
		// PresetList
		// 
		
		struct PresetList
		{
			static const rage::u32 TYPE_ID = 2;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			PresetList() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			
			static const rage::u16 MAX_PRESETREFS = 65535;
			rage::u16 numPresetRefs;
			struct tPresetRef
			{
				rage::audMetadataRef Preset;
			} PresetRef[MAX_PRESETREFS]; // struct tPresetRef
			
		} SPU_ONLY(__attribute__((packed))); // struct PresetList
		
		// 
		// CompiledSynth
		// 
		
		struct CompiledSynth
		{
			static const rage::u32 TYPE_ID = 3;
			static const rage::u32 BASE_TYPE_ID = 0xFFFFFFFF;
			
			CompiledSynth() :
				ClassId(0xFF),
				NameTableOffset(0XFFFFFF),
				Flags(0xAAAAAAAA),
				NumBuffers(0U),
				NumRegisters(0U)
			{}
			
			// PURPOSE - Returns a pointer to the field whose name is specified by the hash
			void* GetFieldPtr(const rage::u32 fieldNameHash);
			
			rage::u32 ClassId : 8;
			rage::u32 NameTableOffset : 24;
			rage::u32 Flags;
			
			rage::u32 NumBuffers;
			rage::u32 NumRegisters;
			
			static const rage::u32 MAX_OUTPUTS = 4;
			rage::u32 numOutputs;
			struct tOutputs
			{
				rage::u8 BufferId;
			} Outputs[MAX_OUTPUTS]; // struct tOutputs
			
			rage::u32 programSizeBytes;
			rage::u32 numStateBlocks;
			rage::u32 costEstimate;
			rage::u16  Operations;
			
			static const rage::u32 MAX_CONSTANTS = 256;
			rage::u32 numConstants;
			struct tConstants
			{
				float Value;
			} Constants[MAX_CONSTANTS]; // struct tConstants
			
			
			static const rage::u32 MAX_INPUTVARIABLES = 256;
			rage::u32 numInputVariables;
			struct tInputVariables
			{
				rage::u32 Name;
				float InitialValue;
			} InputVariables[MAX_INPUTVARIABLES]; // struct tInputVariables
			
		} SPU_ONLY(__attribute__((packed))); // struct CompiledSynth
		
	// enable struct alignment
	#if !__SPU
	#pragma pack(pop, r1)
	#endif // !__SPU
} // namespace rage
#endif // AUD_SYNTHOBJECTS_H
