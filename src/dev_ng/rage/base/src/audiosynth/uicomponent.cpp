#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "pinview.h"
#include "uicomponent.h"
#include "uilabel.h"

#include "audiohardware/channel.h"
#include "grcore/device.h"
#include "grcore/image.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"

#include "profile/profiler.h"

namespace rage
{

	PF_GROUP(SynthEditorUI);
	PF_TIMER(UIComponentDraw, SynthEditorUI);
	PF_COUNTER(UIComponentCount, SynthEditorUI);

	grmShader *synthUIComponent::sm_Shader = NULL;
	grcVertexDeclaration *synthUIComponent::sm_VertexDecl = NULL;
	grcVertexBuffer *synthUIComponent::sm_VertexBuffer = NULL;
	grcIndexBuffer *synthUIComponent::sm_IndexBuffer = NULL;
	u32 synthUIComponent::sm_NumRendered = 0;
	u32 synthUIComponent::sm_NumTested = 0;

	bool synthUIComponent::InitClass()
	{
		grcFvf fvf;
		fvf.SetPosChannel(true);
		fvf.SetTextureChannel(0,true);
		sm_VertexDecl = grmModelFactory::BuildDeclarator(&fvf, NULL, NULL, NULL);

		if(!AssertVerify(sm_VertexDecl))
		{
			return false;
		}

		sm_VertexBuffer = grcVertexBuffer::Create(4,fvf,false,false,0);
		if(!AssertVerify(sm_VertexBuffer))
		{
			return false;
		}
		grcVertexBufferEditor vertexBufferEditor(sm_VertexBuffer);

		s32 idx = 0;
		
		vertexBufferEditor.SetPosition(idx, Vector3( 1.f, -1.f, 0.f));
		vertexBufferEditor.SetUV(idx++,0,Vector2(0.f,1.f));

		vertexBufferEditor.SetPosition(idx, Vector3(-1.f, -1.f, 0.f));
		vertexBufferEditor.SetUV(idx++,0,Vector2(1.f,1.f));

		vertexBufferEditor.SetPosition(idx, Vector3( 1.f,  1.f, 0.f));
		vertexBufferEditor.SetUV(idx++,0,Vector2(0.f,0.f));

		vertexBufferEditor.SetPosition(idx, Vector3(-1.f,  1.f, 0.f));
		vertexBufferEditor.SetUV(idx++,0,Vector2(1.f,0.f));		

		vertexBufferEditor.Unlock();

		sm_IndexBuffer = grcIndexBuffer::Create(4);
		if(!AssertVerify(sm_IndexBuffer))
		{
			return false;
		}
		u16 *lockPtr = sm_IndexBuffer->LockRW();
		if(!AssertVerify(lockPtr))
		{
			return false;
		}
		int offset = 0;
		lockPtr[offset++] = 0;
		lockPtr[offset++] = 1;
		lockPtr[offset++] = 2;
		lockPtr[offset++] = 3;

		sm_IndexBuffer->UnlockRW();

		sm_Shader = grmShaderFactory::GetInstance().Create();
		sm_Shader->Load("common/shaders/waveform");
		

		if(!synthUILabel::InitClass())
		{
			return false;
		}

		if(!synthPinView::InitClass())
		{
			return false;
		}

		return true;
	}

	void synthUIComponent::ShutdownClass()
	{
		synthUILabel::ShutdownClass();
		synthPinView::ShutdownClass();

		if(sm_Shader)
		{
			delete sm_Shader;
			sm_Shader = NULL;
		}
		if(sm_IndexBuffer) delete sm_IndexBuffer;
		if(sm_VertexBuffer) delete sm_VertexBuffer;
		if(sm_VertexDecl) grmModelFactory::FreeDeclarator(sm_VertexDecl);
	}

	grcTexture *synthUIComponent::CreateTexture(const s32 width, const s32 height, const s32 format)
	{
		grcImage* pImage = grcImage::Create(width, height, 1, (grcImage::Format)format, grcImage::STANDARD, 0, 0);
		grcTextureFactory::TextureCreateParams params(grcTextureFactory::TextureCreateParams::SYSTEM, grcTextureFactory::TextureCreateParams::LINEAR,grcsRead|grcsWrite);
		grcTexture *ret = grcTextureFactory::GetInstance().Create(pImage, &params);
		pImage->Release();
		return ret;
	}

	void synthUIComponent::Draw()
	{
		PF_FUNC(UIComponentDraw);
		PF_INCREMENT(UIComponentCount);
		if(m_ShouldDraw)
		{
			sm_NumTested++;
			Vector3 v1,v2;
			GetBounds(v1,v2);


			const grcViewport *viewport = grcViewport::GetCurrent();
			if(grcViewport::IsAABBVisible(VECTOR3_TO_VEC4V(v1).GetIntrin128(), 
											VECTOR3_TO_VEC4V(v2).GetIntrin128(), 
											viewport->GetFrustumLRTB()))
			{
				sm_NumRendered++;
				SetShaderVar("g_AlphaFade", GetAlpha());
				grcWorldMtx(m_WorldMtx);
				Render();
			}
			else
			{
				static u32 numCulled = 0;
				numCulled++;
			}
		}
	}

	void synthUIComponent::SetShaderVar(const char *varName, const f32 val)
	{
		GetShader()->SetVar(GetShader()->LookupVar(varName, true), val);
	}

	void synthUIComponent::SetShaderVar(const char *varName, const Color32 val)
	{
		GetShader()->SetVar(GetShader()->LookupVar(varName, true), VEC3V_TO_VECTOR3(val.GetRGB()));
	}

	void synthUIComponent::SetShaderVar(const char *varName, const grcTexture *val)
	{
		GetShader()->SetVar(GetShader()->LookupVar(varName, true), val);
	}

	void synthUIComponent::DrawVB(const char *techName)
	{
		DrawVB(GetShader()->LookupTechnique(techName,true));
	}

	void synthUIComponent::DrawVB(const grcEffectTechnique tech)
	{
		if (GetShader()->BeginDraw(grmShader::RMC_DRAW, true, tech)) {
			GetShader()->Bind();
			GRCDEVICE.DrawIndexedPrimitive(drawTriStrip,sm_VertexDecl,*sm_VertexBuffer,*sm_IndexBuffer, sm_IndexBuffer->GetIndexCount());
			GetShader()->UnBind();
			GetShader()->EndDraw();
		}
		else
		{
			audErrorf("synthUIComponent: BeginDraw failed.");
		}
	}
}
#endif // __SYNTH_EDITOR
