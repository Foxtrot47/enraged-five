#include "uihorizontallayout.h"

#if __SYNTH_EDITOR

namespace rage
{

	synthUIHorizontalLayout::synthUIHorizontalLayout()
        : m_Orientation(kHorizontal)
        , m_Padding(0.1f)
	{

	}

	synthUIHorizontalLayout::~synthUIHorizontalLayout()
	{

	}

	void synthUIHorizontalLayout::Add(synthUIView *item)
	{
		AddComponent(item);
	}

	bool synthUIHorizontalLayout::IsActive() const
	{
		for(atArray<synthUIComponent*>::const_iterator iter = GetComponents_const().begin(); iter != GetComponents_const().end(); iter++)
		{
			if(((synthUIView*)(*iter))->IsActive())
			{
				return true;
			}
		}
		return false;
	}

	bool synthUIHorizontalLayout::StartDrag() const
	{
		for(atArray<synthUIComponent*>::const_iterator iter = GetComponents_const().begin(); iter != GetComponents_const().end(); iter++)
		{
			if(!((synthUIView*)(*iter))->StartDrag())
			{
				return false;
			}
		}
		return true;
	}

	void synthUIHorizontalLayout::Update()
    {
        PerformLayout();
		synthUIView::Update();
	}

    void synthUIHorizontalLayout::PerformLayout()
    {
        Matrix34 mat = GetMatrix();

        if(m_Orientation == kHorizontal)
        {
            f32 totalWidth = 0.f;
            for(atArray<synthUIComponent*>::iterator iter = GetComponents().begin(); iter != GetComponents().end(); iter++)
            {
                (*iter)->SetMatrix(mat);
                ((synthUIView*)(*iter))->PerformLayout();
                totalWidth += (*iter)->ComputeWidth();
            }

            mat.d -= (totalWidth * 0.5f * (1.f + m_Padding)) * mat.a;

            for(atArray<synthUIComponent*>::iterator iter = GetComponents().begin(); iter != GetComponents().end(); iter++)
            {
                const float thisWidth = (*iter)->ComputeWidth();
                mat.d += 0.5f * thisWidth * mat.a * (1.f + m_Padding);
                (*iter)->SetMatrix(mat);
                mat.d += 0.5f * thisWidth * mat.a * (1.f + m_Padding);
            }
        }
        else
        {
            // Vertical orientation
            f32 totalHeight = 0.f;
            for(atArray<synthUIComponent*>::iterator iter = GetComponents().begin(); iter != GetComponents().end(); iter++)
            {
                (*iter)->SetMatrix(mat);
                ((synthUIView*)(*iter))->PerformLayout();
                totalHeight += (*iter)->ComputeHeight();
            }

            mat.d += (totalHeight * 0.5f * (1.f + m_Padding)) * mat.b;

            for(atArray<synthUIComponent*>::iterator iter = GetComponents().begin(); iter != GetComponents().end(); iter++)
            {
                const float thisHeight = (*iter)->ComputeHeight();
                mat.d -= 0.5f * thisHeight * mat.b * (1.f + m_Padding);
                (*iter)->SetMatrix(mat);
                mat.d -= 0.5f * thisHeight * mat.b * (1.f + m_Padding);
            }
        }
    }

    void synthUIHorizontalLayout::Render()
    {
        synthUIView::Render();

        /*for(atArray<synthUIComponent*>::iterator iter = GetComponents().begin(); iter != GetComponents().end(); iter++)
        {
            Vector3 v1,v2;
            (*iter)->GetBounds(v1,v2);
            grcDrawBox(v1,v2,Color32(1.f,1.f,1.f,1.f));
        }   
        */
    }
}

#endif // __SYNTH_EDITOR
