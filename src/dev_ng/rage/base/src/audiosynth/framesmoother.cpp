
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "framesmoother.h"
#include "audiohardware/mixer.h"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	synthFrameSmoother::synthFrameSmoother() 
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetStaticValue(0.f);
		m_Outputs[0].SetDataBuffer(&m_Buffer);
		
		ResetProcessing();
	}

	void synthFrameSmoother::ResetProcessing()
	{
		m_LastVal = 0.f;
	}

	void synthFrameSmoother::Synthesize()
	{
		const f32 input = m_Inputs[0].GetNormalizedValue(0);
		
		const Vector_4V oldVal = V4LoadScalar32IntoSplatted(m_LastVal);
		const Vector_4V delta = V4Subtract(V4LoadScalar32IntoSplatted(input),oldVal);
		CompileTimeAssert(kMixBufNumSamples == 256 || kMixBufNumSamples == 128);
		const Vector_4V oneOverNumSamples = V4VConstantSplat<kMixBufNumSamples == 256 ? 0x3B800000 : 0x3c000000>(); // 1/256 or 1/128
		// (0,1,2,3)
		const Vector_4V scaleStep = V4VConstant<0,0x3F800000,0x40000000,0x40400000>();

		const Vector_4V step = V4Scale(delta, oneOverNumSamples);
		const Vector_4V multStep = V4Scale(step, V4VConstant(V_FOUR));
		Vector_4V mult = V4Add(oldVal, V4Scale(step,scaleStep));

		Vector_4V *outputBuf = (Vector_4V*)m_Buffer.GetBuffer();

		for(u32 i = 0; i < m_Buffer.GetSize(); i += 4)
		{
			*outputBuf++ = mult;
			mult = V4Add(mult, multStep);
		}
		m_LastVal = GetW(mult);
	}
}
#endif

