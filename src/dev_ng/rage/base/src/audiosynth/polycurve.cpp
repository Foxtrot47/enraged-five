// 
// audiosynth/polycurve.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "polycurve.h"

#if !SYNTH_MINIMAL_MODULES

#include "polycurveview.h"
#include "synthdefs.h"

namespace rage 
{
	SYNTH_EDITOR_VIEW(synthPolyCurve);

	synthPolyCurve::synthPolyCurve()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);
		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetStaticValue(0.f);

		for(s32 i = 0; i < kMaxCoefficients; i++)
		{
			m_Coefficients[i] = 0.2f;
		}
		m_Coefficients[0] = 0.f;
	}

	void synthPolyCurve::SerializeState(synthModuleSerializer *serializer)
	{
		for(s32 i = 0; i < kMaxCoefficients; i++)
		{
			serializer->SerializeField("Coefficient", m_Coefficients[i]);
		}
	}

	f32 synthPolyCurve::EvaluatePolynomial(const f32 input) const
	{
		f32 x = input;
		f32 x_power = 1.f;
		f32 output = m_Coefficients[0];
		for (s32 i=1; i<kMaxCoefficients; i++) 
		{
			x_power *= x;
			output += (x_power * m_Coefficients[i]);
		}

		return output;
	}

	void synthPolyCurve::Synthesize()
	{
		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			m_Outputs[0].SetStaticValue(EvaluatePolynomial(m_Inputs[0].GetStaticValue()));
		}
		else
		{			

			m_Outputs[0].SetDataBuffer(&m_Buffer);

			for(u32 i = 0; i < m_Buffer.GetSize(); i++)
			{
				m_Outputs[0].GetDataBuffer()->GetBuffer()[i] = EvaluatePolynomial(m_Inputs[0].GetNormalizedValue(i)); 
			}
		}
	}

} // namespace rage
#endif
