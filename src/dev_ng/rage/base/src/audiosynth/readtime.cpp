#include "readtime.h"
#include "synthcontext.h"

namespace rage
{
	synthReadTime::synthReadTime()
	{
		m_Outputs[0].Init(this,"Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);
	}

	void synthReadTime::Synthesize(synthContext &context)
	{
		m_Outputs[0].SetStaticValue(context.GetTimeS());
	}
}

