// 
// audiosynth/adder.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "adder.h"
#include "math/amath.h"
#include "system/memops.h"
#include "vectormath/vectormath.h"

namespace rage {

	using namespace Vec;
	synthAdder::synthAdder() 
	{
		m_Outputs[0].Init(this, "Result",synthPin::SYNTH_PIN_DATA_NORMALIZED,true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		for(u32 i = 0; i < NumInputs; i++)
		{
			m_Inputs[i].Init(this, "In",synthPin::SYNTH_PIN_DATA_NORMALIZED);
			m_Inputs[i].SetStaticValue(0.f);
			m_Inputs[i].SetConvertsInternally(false);
		}
	}

	void synthAdder::GetInputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Inputs[0];
		numPins = 2;
		for(u32 i = 1; i < NumInputs; i++)
		{
			if(m_Inputs[i].IsConnected())
			{
				numPins = i+2;
			}
		}
		numPins = Min(numPins, (u32)NumInputs);
	}

	void synthAdder::Synthesize()
	{
		// see if we have any dynamic inputs
		bool hasDynamicInput = false;
		bool hasSignalInput = false;
		u32 numInputs = 2;
		for(u32 i = 0; i < NumInputs; i++)
		{
			if(m_Inputs[i].IsConnected())
			{			
				numInputs = i + 2;
			}
		}
		numInputs = Min<u32>(numInputs, NumInputs);

		for(u32 i = 0; i < numInputs; i++)
		{
			if(m_Inputs[i].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				hasDynamicInput = true;
			}
			if(m_Inputs[i].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
			{
				hasSignalInput = true;
			}
		}

		m_Outputs[0].SetDataFormat(hasSignalInput ? synthPin::SYNTH_PIN_DATA_SIGNAL : synthPin::SYNTH_PIN_DATA_NORMALIZED);

		if(!hasDynamicInput)
		{
			// all inputs are static
			f32 sum = m_Inputs[0].GetStaticValue() + m_Inputs[1].GetStaticValue();
			for(u32 i = 2; i < numInputs; i++)
			{
				sum += m_Inputs[i].GetStaticValue();
			}
			m_Outputs[0].SetStaticValue(sum);
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			if(numInputs == 2)
			{
				if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC ||
					m_Inputs[1].GetDataState() == synthPin::SYNTH_PIN_STATIC)
				{
					// one static, one dynamic
					f32 *buf;
					Vector_4V splat;
					if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
					{
						splat = V4LoadScalar32IntoSplatted(m_Inputs[0].GetStaticValue());
						buf = m_Inputs[1].GetDataBuffer()->GetBuffer();
					}
					else
					{
						splat = V4LoadScalar32IntoSplatted(m_Inputs[1].GetStaticValue());
						buf = m_Inputs[0].GetDataBuffer()->GetBuffer();
					}

					for(u32 i = 0; i < m_Buffer.GetSize(); i+=4)
					{
						const Vector_4V in = *((Vector_4V*)&buf[i]);
						*((Vector_4V*)&m_Outputs[0].GetDataBuffer()->GetBuffer()[i]) = V4Add(in, splat);
					}
				}
				else
				{
					// both dynamic
					
					f32 *buf0 = m_Inputs[0].GetDataBuffer()->GetBuffer();
					f32 *buf1 = m_Inputs[1].GetDataBuffer()->GetBuffer();
					for(u32 i = 0; i < m_Buffer.GetSize(); i+=4)
					{
						const Vector_4V in1 = *((Vector_4V*)&buf0[i]);
						const Vector_4V in2 = *((Vector_4V*)&buf1[i]);
						*((Vector_4V*)&m_Outputs[0].GetDataBuffer()->GetBuffer()[i]) = V4Add(in1, in2);
					}
				}
			}
			else
			{
				// some other number of inputs
				sysMemSet(m_Buffer.GetBuffer(), 0, sizeof(f32) * m_Buffer.GetSize());

				f32 staticSum = 0.f;
				bool haveStaticValues = false;
				for(u32 i = 0; i < numInputs; i++)
				{
					if(m_Inputs[i].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
					{
						f32 *RESTRICT buf0 = m_Inputs[i].GetDataBuffer()->GetBuffer();
						f32 *RESTRICT buf1 = m_Buffer.GetBuffer();
						for(u32 s = 0; s < m_Buffer.GetSize(); s += 4)
						{
							const Vector_4V in1 = *((Vector_4V*)&buf0[s]);
							*((Vector_4V*)&buf1[s]) = V4Add(in1, *((Vector_4V*)&buf1[s]));
						}
					}
					else
					{
						staticSum += m_Inputs[i].GetStaticValue();
						haveStaticValues = true;
					}
				}

				if(haveStaticValues)
				{
					const Vector_4V val = V4LoadScalar32IntoSplatted(staticSum);
					f32 *RESTRICT buf1 = m_Buffer.GetBuffer();
					for(u32 s = 0; s < m_Buffer.GetSize(); s += 4)
					{
						*((Vector_4V*)&buf1[s]) = V4Add(*((Vector_4V*)&buf1[s]), val);
					}
				}
			}
		}
		
	}

} // namespace rage
#endif
