// 
// audiosynth/fftview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_FFTVIEW_H
#define SYNTH_FFTVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"

namespace rage
{
	class synthFftView : public synthModuleView
	{
	public:
		
		synthFftView(synthModule *module);

		virtual void Update();

		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

	protected:
		virtual bool DefaultPinPositioning() const { return false; }
		virtual bool StartDrag() const;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_FFTVIEW_H

