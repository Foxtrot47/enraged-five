// 
// audiosynth/rescalerview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_RESCALERVIEW_H
#define SYNTH_RESCALERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uilabel.h"
#include "uimenuview.h"
#include "uirangeview.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthRescaler;
	class synthRescalerView : public synthModuleView
	{
	public:

		synthRescalerView(synthModule *module);

		virtual void Update();
		virtual bool StartDrag() const;
		virtual bool IsActive() const;

	protected:
		virtual void Render();

	private:

		synthRescaler *GetRescalerModule() 
		{
			return (synthRescaler*)m_Module;
		}

		synthUIDropDownList m_ModeSwitch;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_RESCALERVIEW_H

