// 
// audiosynth/audiooutputview.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_AUDIOOUTPUTVIEW_H
#define SYNTH_AUDIOOUTPUTVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uibutton.h"
#include "uihorizontallayout.h"
#include "uilabel.h"
#include "uilevelmeter.h"
#include "pin.h"

namespace rage
{
	class synthAudioOutput;
	class synthAudioOutputView : public synthModuleView
	{
	public:

		synthAudioOutputView(synthModule *module);
		virtual ~synthAudioOutputView();
		virtual void Update();
		virtual void Render();
		virtual void PostSynthesize();

		virtual u32 GetModuleHeight() const { return 4; }

	private:

		synthAudioOutput *GetAudioOutput() const { return (synthAudioOutput*)m_Module; }

		char m_PeakLevelString[32];

		s32 *m_Ip;
		f32 *m_W;

		synthUIHorizontalLayout m_Layout;
		synthUIHorizontalLayout m_VertLayout;

		synthUILabelView m_ClipLabel;
		synthUIButton m_MuteButton;
	
		synthUILevelMeter m_CurrentMeter;
		synthUILevelMeter m_PeakMeter;
		synthUILevelMeter m_RMSMeter;

		synthPin m_FftPin;
		synthSampleFrame m_FftBuffer;

		float m_ClipAlpha;
		float m_PeakLevel;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_AUDIOINPUTVIEW_H

