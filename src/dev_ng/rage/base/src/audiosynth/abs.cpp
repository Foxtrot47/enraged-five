// 
// audiosynth/abs.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "abs.h"

#if !SYNTH_MINIMAL_MODULES

#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	synthAbs::synthAbs()
	{
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);

		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);		
	}

	void synthAbs::Synthesize()
	{	
		m_Outputs[0].SetDataFormat(m_Inputs[0].GetDataFormat());

		if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
		{
			m_Outputs[0].SetStaticValue(Abs(m_Inputs[0].GetNormalizedValue(0)));
		}
		else
		{
			m_Outputs[0].SetStaticValue(Abs(m_Inputs[0].GetSignalValue(0)));
		}

		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			const f32 *RESTRICT const input = m_Inputs[0].GetDataBuffer()->GetBuffer();
			f32 *RESTRICT output = m_Outputs[0].GetDataBuffer()->GetBuffer();
			const u32 numSamples = m_Outputs[0].GetDataBuffer()->GetSize();

			Process(output, input, numSamples);				
		}
	}

	void synthAbs::Process(float *RESTRICT output, const float *RESTRICT input, const u32 numSamples)
	{
		u32 count = numSamples >> 2;
		const Vector_4V *inPtr = reinterpret_cast<const Vector_4V*>(input);
		Vector_4V *outPtr = reinterpret_cast<Vector_4V*>(output);
		while(count--)
		{
			*outPtr++ = V4Abs(*inPtr++);
		}
	}

	void synthAbs::Process(float *RESTRICT inOutBuffer, const u32 numSamples)
	{
		u32 count = numSamples >> 2;
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		while(count--)
		{
			*inOutPtr = V4Abs(*inOutPtr);
			inOutPtr++;
		}
	}
} // namespace rage
#endif
