// 
// audiosynth/sampleplayerview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SAMPLEPLAYERVIEW_H
#define SYNTH_SAMPLEPLAYERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "numericinputbox.h"
#include "uilabel.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthSamplePlayer;
	class grcTexture;
	class synthSamplePlayerView : public synthModuleView
	{
	public:

		synthSamplePlayerView(synthModule *module);
		~synthSamplePlayerView();
		virtual void Update();

		virtual bool StartDrag() const;

		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

	protected:
		virtual void Render();

	private:

		void UpdateWaveSelection();

		synthSamplePlayer *GetSamplePlayer() const { return (synthSamplePlayer*)m_Module; }

		synthUITextBoxInput m_InputBox;

		synthUILabel m_TotalTimeLabel;
		char m_TotalTime[64];
		
		synthUIDropDownList m_BankList;
		atMap<u32, const char *> m_BankNameMap;

		synthUILabel m_WaveNameLabel;
		char m_WaveName[32];

		bool m_EditingWaveName;

		synthUILabel m_ModeLabel;		
		grcTexture *m_Texture;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_SAMPLEPLAYERVIEW_H

