#ifndef AUD_BLOCKS_H
#define AUD_BLOCKS_H

#include "audiohardware/channel.h"
#include "audiohardware/driverdefs.h"
#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "atl/array.h"
#include "math/amath.h"
#include "system/cache.h"
#include "system/dma.h"
#include "system/memops.h"
#include "vectormath/vectormath.h"
#include "audioengine/spuutil.h"

#define REV_CHECK_RET(x) if(!x) return false

namespace rage
{
#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure padded due to alignment
#endif

	class revSimpleDelay_V8
	{
	public:

#if !__SPU
		revSimpleDelay_V8() 
			: m_Buffer(NULL)
			, m_BufferIndex(0)
			, m_BufferLength(0)
#if __PS3
			, m_LocalBuffer(NULL)
			, m_LocalBufferIndex(0)
			, m_LocalBufferLength(0)
#endif // __PS3
		{
			m_Gain = Vec::V4VConstant(V_ONE);
		}

		~revSimpleDelay_V8()
		{
			if(m_Buffer)
				delete[] m_Buffer;
		}

		bool SetLength(const s32 unalignedLengthSamples)
		{
			// Align length to a multiple of two samples, divide by two as we are storing two samples in each Vec4
			const s32 lengthSamples = unalignedLengthSamples >> 1;
			
			if(m_BufferLength != lengthSamples || m_Buffer == NULL)
			{
				if(m_Buffer)
				{
					delete[] m_Buffer;
				}
				if(lengthSamples)
				{
					m_Buffer = rage_aligned_new(16) Vec::Vector_4V[lengthSamples];
					if(!m_Buffer)
						return false;

					for(s32 i = 0; i < lengthSamples; i++)
					{
						m_Buffer[i] = Vec::V4VConstant(V_ZERO);
					}
				}

				m_BufferLength = lengthSamples;
				m_BufferIndex = 0;
			}

			return true;
		}

		bool SetLengthS(const float lengthSeconds, const float sampleRate)
		{
			audAssertf(lengthSeconds > 0.f && lengthSeconds >= (1.f / float(sampleRate)), "Invalid delay time: %f seconds", lengthSeconds);
			return SetLength(static_cast<s32>(0.5f + lengthSeconds * sampleRate));
		}

#endif // !__SPU

		bool HasBuffer() const
		{
			return m_Buffer != NULL;
		}

		void ZeroBuffer()
		{
			if (m_Buffer)
			{
				for (s32 i = 0; i < m_BufferLength; i++)
				{
					m_Buffer[i] = Vec::V4VConstant(V_ZERO);
				}
			}
		}

		void Process(Vec::Vector_4V_InOut inOut0, Vec::Vector_4V_InOut inOut1)
		{
			Vec::Vector_4V out0, out1;
			Read(out0, out1);			
			Write(inOut0, inOut1);
			inOut0 = out0;
			inOut1 = out1;
		}

		void Read(Vec::Vector_4V_Ref out0, Vec::Vector_4V_Ref out1) const
		{
			Vec::Vector_4V packedSamples;

#if __SPU
			Assert(m_LocalBuffer);
			packedSamples = m_LocalBuffer[m_LocalBufferIndex];	
#else
			XENON_ONLY(PrefetchDC(m_Buffer + m_BufferIndex + 8));
			packedSamples = m_Buffer[m_BufferIndex];
#endif
			// unpack 8 16 bit ints into 32bit ints (splitting into two vectors)
			const Vec::Vector_4V inputInts1 = Vec::V4UnpackLowSignedShort(packedSamples);
			const Vec::Vector_4V inputInts2 = Vec::V4UnpackHighSignedShort(packedSamples);

			// convert to float and scale
			out0 = Vec::V4Scale(Vec::V4IntToFloatRaw<15>(inputInts1), m_Gain);
			out1 = Vec::V4Scale(Vec::V4IntToFloatRaw<15>(inputInts2), m_Gain);
		}

		void Write(Vec::Vector_4V_In in0, Vec::Vector_4V_In in1)
		{
			Vec::Vector_4V inputInts0 = Vec::V4FloatToIntRaw<15>(in0);
			Vec::Vector_4V inputInts1 = Vec::V4FloatToIntRaw<15>(in1);

			Vec::Vector_4V outPacked = Vec::V4PackSignedIntToSignedShort(inputInts0, inputInts1);

#if __SPU
			Assert(m_LocalBuffer);
			m_LocalBuffer[m_LocalBufferIndex++] = outPacked;	
			m_LocalBufferIndex %= m_LocalBufferLength;		
			m_BufferIndex++;
#else
			m_Buffer[m_BufferIndex] = outPacked;		
			m_BufferIndex = IIncrementSaturateAndWrap(m_BufferIndex, m_BufferLength);
#endif
		}

		void SetGain(Vec::Vector_4V_In gain)
		{
			m_Gain = gain;
		}

		void SetGain(const float gain)
		{
			m_Gain = Vec::V4LoadScalar32IntoSplatted(gain);
		}

		size_t GetSizeBytes() const
		{
			return sizeof(Vec::Vector_4V) * m_BufferLength;
		}

#if __SPU
		void Prefetch(const s32 localBufferSizeSamples, const s32 tag)
		{
			m_LocalBufferLength = Min(m_BufferLength, localBufferSizeSamples >> 1); // 16bits per sample
			m_LocalBuffer = AllocateFromScratch<Vec::Vector_4V>(m_LocalBufferLength, 16, "Delay_V8");
			
			if(m_LocalBufferLength == m_BufferLength)
			{
				// We can fit the entire buffer in SPU LS
				m_LocalBufferIndex = m_BufferIndex;
				m_BufferFetchIndex = 0;
				// Grab entire buffer
				sysDmaGet(m_LocalBuffer, u32(m_Buffer), m_LocalBufferLength * sizeof(Vec::Vector_4V), tag);
			}
			else
			{
				m_LocalBufferIndex = 0;
				m_BufferFetchIndex = m_BufferIndex;

				if(m_BufferFetchIndex + m_LocalBufferLength > m_BufferLength)
				{
					// wrap-around case; need two fetches
					const s32 size0 = m_BufferLength - m_BufferFetchIndex;
					sysDmaGet(m_LocalBuffer, u32(m_Buffer + m_BufferFetchIndex), size0 * sizeof(Vec::Vector_4V), tag);
					const s32 size1 = m_LocalBufferLength - size0;
					sysDmaGet(m_LocalBuffer + size0, u32(m_Buffer), size1 * sizeof(Vec::Vector_4V), tag);
				}
				else
				{
					// grab in a single fetch
					sysDmaGet(m_LocalBuffer, u32(m_Buffer + m_BufferFetchIndex), m_LocalBufferLength * sizeof(Vec::Vector_4V), tag);
				}
			}
		}

		void Writeback(const s32 tag)
		{
			Assert(m_LocalBuffer);
	
			if(m_BufferFetchIndex + m_LocalBufferLength > m_BufferLength)
			{
				// wrap-around case; need two puts
				const s32 size0 = m_BufferLength - m_BufferFetchIndex;
				sysDmaPut(m_LocalBuffer, u32(m_Buffer + m_BufferFetchIndex), size0 * sizeof(Vec::Vector_4V), tag);
				const s32 size1 = m_LocalBufferLength - size0;
				sysDmaPut(m_LocalBuffer + size0, u32(m_Buffer), size1 * sizeof(Vec::Vector_4V), tag);
			}
			else
			{
				// write back in a single put
				sysDmaPut(m_LocalBuffer, u32(m_Buffer + m_BufferFetchIndex), m_LocalBufferLength * sizeof(Vec::Vector_4V), tag);
			}
			m_BufferIndex %= m_BufferLength;			
		}
#endif

	private:

		Vec::Vector_4V m_Gain;
		Vec::Vector_4V *m_Buffer;
		s32 m_BufferIndex;
		s32 m_BufferLength;

#if __PS3
		Vec::Vector_4V *m_LocalBuffer;
		s32 m_BufferFetchIndex;
		s32 m_LocalBufferIndex;
		s32 m_LocalBufferLength;				
#endif // __SPU
	};

	class revFractionalDelay_V8
	{
	public:

#if !__SPU
		revFractionalDelay_V8() 
			: m_Buffer(NULL)
			, m_BufferIndex(0)
			, m_BufferLength(0)
			, m_ModulationSpeed(0.f)
			, m_ModulationDepth(0.f)
			, m_Modulation(0.f)
#if __PS3
			, m_LocalBuffer(NULL)
			, m_LocalBufferIndex(0)
			, m_LocalBufferLength(0)
#endif // __PS3
		{
			m_Gain = Vec::V4VConstant(V_ONE);
		}

		~revFractionalDelay_V8()
		{
			if(m_Buffer)
				delete[] m_Buffer;
		}

		bool SetLength(const u32 unalignedLengthSamples)
		{
			// Align length to a multiple of two samples, divide by two as we are storing two samples in each Vec4
			const u32 lengthSamples = unalignedLengthSamples >> 1;

			if(m_BufferLength != lengthSamples || m_Buffer == NULL)
			{
				if(m_Buffer)
				{
					delete[] m_Buffer;
				}
				if(lengthSamples)
				{
					m_Buffer = rage_aligned_new(16) Vec::Vector_4V[lengthSamples];
					if(!m_Buffer)
						return false;

					for(u32 i = 0; i < lengthSamples; i++)
					{
						m_Buffer[i] = Vec::V4VConstant(V_ZERO);
					}
				}

				m_BufferLength = lengthSamples;
				m_BufferIndex = 0;
			}

			return true;
		}

		bool SetLengthS(const float lengthSeconds, const float sampleRate)
		{
			audAssertf(lengthSeconds > 0.f && lengthSeconds >= (1.f / float(sampleRate)), "Invalid delay time: %f seconds", lengthSeconds);
			return SetLength(static_cast<u32>(0.5f + lengthSeconds * sampleRate));
		}

		void SetModulationDepth(const float modDepth)
		{
			m_ModulationDepth = modDepth;
		}

		void SetModulationSpeed(const float modSpeed)
		{
			m_ModulationSpeed = modSpeed / 24000.f;
		}

#endif // !__SPU

		bool HasBuffer() const
		{
			return m_Buffer != NULL;
		}

		void Process(Vec::Vector_4V_InOut inOut0, Vec::Vector_4V_InOut inOut1)
		{
			Vec::Vector_4V out0, out1;
			Read(out0, out1);			
			Write(inOut0, inOut1);
			inOut0 = out0;
			inOut1 = out1;
		}

		void ReadFractional(Vec::Vector_4V_InOut inOut0, Vec::Vector_4V_InOut inOut1)
		{
			m_Modulation += m_ModulationSpeed;
			if(m_Modulation >= 1.f)
			{
				m_Modulation = 1.f;
				m_ModulationSpeed *= -1.f;
			}
			else if(m_Modulation < 0.f)
			{
				m_Modulation = 0.f;
				m_ModulationSpeed *= -1.f;
			}

			const float maxDelayTime = static_cast<f32>(m_BufferLength);
			const float delayTime0 = maxDelayTime * (1.f + m_Modulation*m_ModulationDepth);
			const float frac0 = delayTime0 - floorf(delayTime0);

			m_Modulation += m_ModulationSpeed;
			if(m_Modulation >= 1.f)
			{
				m_Modulation = 1.f;
				m_ModulationSpeed *= -1.f;
			}
			else if(m_Modulation < 0.f)
			{
				m_Modulation = 0.f;
				m_ModulationSpeed *= -1.f;
			}

			const float delayTime1 = maxDelayTime * (1.f + m_Modulation*m_ModulationDepth);
			const float frac1 = delayTime1 - floorf(delayTime1);

			const s32 sampleIndex0 = int(m_BufferIndex<<1) - (int)delayTime0;
			const s32 sampleIndex1 = int(m_BufferIndex<<1) - (int)delayTime1;

			int readIndex0 = sampleIndex0 >> 1;
			int readIndex1 = sampleIndex1 >> 1;

			Vec::Vector_4V packedSamples0;
			Vec::Vector_4V packedSamples1;
#if __SPU
			Assert(m_LocalBuffer);			 		
			packedSamples0 = sysDmaGetTypeT<Vec::Vector_4V>(u32(m_Buffer + ((readIndex0 + m_BufferLength) % m_BufferLength)), 2);
			packedSamples1 = sysDmaGetTypeT<Vec::Vector_4V>(u32(m_Buffer + ((readIndex1 + m_BufferLength) % m_BufferLength)), 2);			
#else
			packedSamples0 = m_Buffer[(readIndex0 + m_BufferLength) % m_BufferLength];
			packedSamples1 = m_Buffer[(readIndex1 + m_BufferLength) % m_BufferLength];
#endif
			
			const Vec::Vector_4V inputInts00 = Vec::V4UnpackLowSignedShort(packedSamples0);
			const Vec::Vector_4V inputInts01 = Vec::V4UnpackHighSignedShort(packedSamples0);
			
			const Vec::Vector_4V inputInts10 = Vec::V4UnpackLowSignedShort(packedSamples1);
			const Vec::Vector_4V inputInts11 = Vec::V4UnpackHighSignedShort(packedSamples1);
			
			const Vec::Vector_4V input00 = Vec::V4IntToFloatRaw<15>(inputInts00);
			const Vec::Vector_4V input01 = Vec::V4IntToFloatRaw<15>(inputInts01);
			const Vec::Vector_4V input10 = Vec::V4IntToFloatRaw<15>(inputInts10);
			const Vec::Vector_4V input11 = Vec::V4IntToFloatRaw<15>(inputInts11);
		
			const Vec::Vector_4V frac0V = Vec::V4LoadScalar32IntoSplatted(frac0);
			const Vec::Vector_4V frac1V = Vec::V4LoadScalar32IntoSplatted(frac1);
			const Vec::Vector_4V out0 = Vec::V4Scale(m_Gain, Vec::V4AddScaled(input00, frac0V, Vec::V4Subtract(input01, input00)));
			const Vec::Vector_4V out1 = Vec::V4Scale(m_Gain, Vec::V4AddScaled(input10, frac1V, Vec::V4Subtract(input11, input10)));

			inOut0 = out0;
			inOut1 = out1;
		}
		
		void Read(Vec::Vector_4V_Ref out0, Vec::Vector_4V_Ref out1) const
		{
			Vec::Vector_4V packedSamples;

#if __SPU
			Assert(m_LocalBuffer);
			packedSamples = m_LocalBuffer[m_LocalBufferIndex];	
#else
			packedSamples = m_Buffer[m_BufferIndex];
#endif
			// unpack 8 16 bit ints into 32bit ints (splitting into two vectors)
			const Vec::Vector_4V inputInts1 = Vec::V4UnpackLowSignedShort(packedSamples);
			const Vec::Vector_4V inputInts2 = Vec::V4UnpackHighSignedShort(packedSamples);

			// convert to float and scale
			out0 = Vec::V4Scale(Vec::V4IntToFloatRaw<15>(inputInts1), m_Gain);
			out1 = Vec::V4Scale(Vec::V4IntToFloatRaw<15>(inputInts2), m_Gain);
		}

		void Write(Vec::Vector_4V_In in0, Vec::Vector_4V_In in1)
		{
			Vec::Vector_4V inputInts0 = Vec::V4FloatToIntRaw<15>(in0);
			Vec::Vector_4V inputInts1 = Vec::V4FloatToIntRaw<15>(in1);

			Vec::Vector_4V outPacked = Vec::V4PackSignedIntToSignedShort(inputInts0, inputInts1);

#if __SPU
			Assert(m_LocalBuffer);
			m_LocalBuffer[m_LocalBufferIndex++] = outPacked;	
			m_LocalBufferIndex %= m_LocalBufferLength;		
			m_BufferIndex++;
#else
			m_Buffer[m_BufferIndex] = outPacked;
			m_BufferIndex++;
			m_BufferIndex %= m_BufferLength;
#endif			
		}

		void SetGain(Vec::Vector_4V_In gain)
		{
			m_Gain = gain;
		}

		void SetGain(const float gain)
		{
			m_Gain = Vec::V4LoadScalar32IntoSplatted(gain);
		}

		size_t GetSizeBytes() const
		{
			return sizeof(Vec::Vector_4V) * m_BufferLength;
		}

#if __SPU
		void Prefetch(const u32 localBufferSizeSamples, const s32 tag)
		{
			m_LocalBufferLength = Min(m_BufferLength, localBufferSizeSamples >> 1); // 16bits per sample
			m_LocalBuffer = AllocateFromScratch<Vec::Vector_4V>(m_LocalBufferLength, 16, "Delay_V8");

			if(m_LocalBufferLength == m_BufferLength)
			{
				// We can fit the entire buffer in SPU LS
				m_LocalBufferIndex = m_BufferIndex;
				m_BufferFetchIndex = 0;
				// Grab entire buffer
				sysDmaGet(m_LocalBuffer, u32(m_Buffer), m_LocalBufferLength * sizeof(Vec::Vector_4V), tag);
			}
			else
			{
				m_LocalBufferIndex = 0;
				m_BufferFetchIndex = m_BufferIndex;

				if(m_BufferFetchIndex + m_LocalBufferLength > m_BufferLength)
				{
					// wrap-around case; need two fetches
					const u32 size0 = m_BufferLength - m_BufferFetchIndex;
					sysDmaGet(m_LocalBuffer, u32(m_Buffer + m_BufferFetchIndex), size0 * sizeof(Vec::Vector_4V), tag);
					const u32 size1 = m_LocalBufferLength - size0;
					sysDmaGet(m_LocalBuffer + size0, u32(m_Buffer), size1 * sizeof(Vec::Vector_4V), tag);
				}
				else
				{
					// grab in a single fetch
					sysDmaGet(m_LocalBuffer, u32(m_Buffer + m_BufferFetchIndex), m_LocalBufferLength * sizeof(Vec::Vector_4V), tag);
				}
			}
		}

		void Writeback(const s32 tag)
		{
			Assert(m_LocalBuffer);

			if(m_BufferFetchIndex + m_LocalBufferLength > m_BufferLength)
			{
				// wrap-around case; need two puts
				const u32 size0 = m_BufferLength - m_BufferFetchIndex;
				sysDmaPut(m_LocalBuffer, u32(m_Buffer + m_BufferFetchIndex), size0 * sizeof(Vec::Vector_4V), tag);
				const u32 size1 = m_LocalBufferLength - size0;
				sysDmaPut(m_LocalBuffer + size0, u32(m_Buffer), size1 * sizeof(Vec::Vector_4V), tag);
			}
			else
			{
				// write back in a single put
				sysDmaPut(m_LocalBuffer, u32(m_Buffer + m_BufferFetchIndex), m_LocalBufferLength * sizeof(Vec::Vector_4V), tag);
			}
			m_BufferIndex %= m_BufferLength;			
		}
#endif

	private:

		Vec::Vector_4V m_Gain;
		Vec::Vector_4V *m_Buffer;
		u32 m_BufferIndex;
		u32 m_BufferLength;

		float m_ModulationSpeed;
		float m_ModulationDepth;
		float m_Modulation;

#if __PS3
		Vec::Vector_4V *m_LocalBuffer;
		u32 m_BufferFetchIndex;
		u32 m_LocalBufferIndex;
		u32 m_LocalBufferLength;				
#endif // __SPU
	};

	// PURPOSE
	//	Implements a four channel, fixed length simple delay line
	class revSimpleDelay_V4
	{
	public:
		revSimpleDelay_V4() 
			: m_Buffer(NULL)
			, m_BufferIndex(0)
			, m_BufferLength(0)
#if __PS3
			, m_LocalBuffer(NULL)
			, m_LocalBufferIndex(0)
			, m_LocalBufferLength(0)
#endif // __PS3
		{

		}

		~revSimpleDelay_V4()
		{
			if(m_Buffer)
				delete[] m_Buffer;
		}

		bool HasBuffer() const
		{
			return m_Buffer != NULL;
		}

		bool SetLength(const s32 lengthSamples)
		{
			if(m_BufferLength != lengthSamples || m_Buffer == NULL)
			{
				if(m_Buffer)
				{
					delete[] m_Buffer;
				}
				m_Buffer = rage_aligned_new(16) Vec::Vector_4V[lengthSamples];
				if(!m_Buffer)
					return false;

				for(s32 i = 0; i < lengthSamples; i++)
				{
					m_Buffer[i] = Vec::V4VConstant(V_ZERO);
				}

				m_BufferLength = lengthSamples;
				m_BufferIndex = 0;
			}
			return true;
		}

		void ZeroBuffer()
		{
			for(s32 i = 0; i < m_BufferLength; i++)
			{
				m_Buffer[i] = Vec::V4VConstant(V_ZERO);
			}
		}

		bool SetLengthS(const float lengthSeconds)
		{
			audAssertf(lengthSeconds > 0.f && lengthSeconds >= (1.f / float(kMixerNativeSampleRate)), "Invalid delay time: %f seconds", lengthSeconds);
			return SetLength(static_cast<s32>(0.5f + lengthSeconds * kMixerNativeSampleRate));
		}

		bool SetLengthS(const float lengthSeconds, const float sampleRate)
		{
			audAssertf(lengthSeconds > 0.f && lengthSeconds >= 1.f / sampleRate, "Invalid delay time: %f seconds", lengthSeconds);
			return SetLength(static_cast<s32>(0.5f + lengthSeconds * sampleRate));
		}

		Vec::Vector_4V_Out operator()(Vec::Vector_4V_In input)
		{
			return Process(input);
		}

		// PURPOSE
		//	Writes the supplied four samples into the delay line, returns four samples from the delay line
		Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
		{
			const Vec::Vector_4V output = Read();
			Write(input);
			return output;
		}

		__forceinline Vec::Vector_4V_Out Read() const
		{
#if __SPU
			Assert(m_LocalBuffer);
			Vec::Vector_4V delaySamples = m_LocalBuffer[m_LocalBufferIndex];
#else
			XENON_ONLY(PrefetchDC(m_Buffer + m_BufferIndex + 8));
			Vec::Vector_4V delaySamples = m_Buffer[m_BufferIndex];
#endif
			return delaySamples;
		}

		Vec::Vector_4V_Out ReadTap(const s32 offset) const
		{
			Assert(!__SPU);
			return m_Buffer[(m_BufferIndex+m_BufferLength-offset) % m_BufferLength];
		}

		__forceinline void Write(Vec::Vector_4V_In input)
		{
#if __SPU
			Assert(m_LocalBuffer);
			m_LocalBuffer[m_LocalBufferIndex++] = input;
#else
			m_Buffer[m_BufferIndex] = input;
			m_BufferIndex = IIncrementSaturateAndWrap(m_BufferIndex, m_BufferLength);
#endif			
		}

		size_t GetSizeBytes() const
		{
			return sizeof(Vec::Vector_4V) * m_BufferLength;
		}

#if __SPU
		void Prefetch(const s32 localBufferSizeSamples, const s32 tag)
		{
			Assert(m_Buffer);
			Assert(m_BufferLength > localBufferSizeSamples);
			m_LocalBufferLength = localBufferSizeSamples;
			m_LocalBuffer = AllocateFromScratch<Vec::Vector_4V>(m_LocalBufferLength, 16, "Delay_V4");
			m_LocalBufferIndex = 0;
			
			if(m_BufferIndex + m_LocalBufferLength > m_BufferLength)
			{
				// wrap-around case; need two fetches
				const u32 size0 = m_BufferLength - m_BufferIndex;		
				sysDmaGet(m_LocalBuffer, u32(m_Buffer + m_BufferIndex), size0 * sizeof(Vec::Vector_4V), tag);
				const u32 size1 = m_LocalBufferLength - size0;
				sysDmaGet(m_LocalBuffer + size0, u32(m_Buffer), size1 * sizeof(Vec::Vector_4V), tag);
			}
			else
			{
				// grab in a single fetch
				sysDmaGet(m_LocalBuffer, u32(m_Buffer + m_BufferIndex), m_LocalBufferLength * sizeof(Vec::Vector_4V), tag);
			}			
		}

		void Writeback(const s32 tag)
		{
			Assert(m_LocalBuffer);
			Assert(m_LocalBufferIndex == m_LocalBufferLength);

			if(m_BufferIndex + m_LocalBufferLength > m_BufferLength)
			{
				// wrap-around case; need two puts
				const u32 size0 = m_BufferLength - m_BufferIndex;
				sysDmaPut(m_LocalBuffer, u32(m_Buffer + m_BufferIndex), size0 * sizeof(Vec::Vector_4V), tag);
				const u32 size1 = m_LocalBufferLength - size0;
				sysDmaPut(m_LocalBuffer + size0, u32(m_Buffer), size1 * sizeof(Vec::Vector_4V), tag);
			}
			else
			{
				// write back in a single fetch
				sysDmaPut(m_LocalBuffer, u32(m_Buffer + m_BufferIndex), m_LocalBufferLength * sizeof(Vec::Vector_4V), tag);
			}

			m_BufferIndex += m_LocalBufferIndex;
			m_BufferIndex %= m_BufferLength;			
		}
#endif

		// V8 compatible API

		void Process(Vec::Vector_4V_InOut inOut0, Vec::Vector_4V_InOut inOut1)
		{
			inOut0 = Process(inOut0);
			inOut1 = Process(inOut1);
		}

		void Read(Vec::Vector_4V_Ref out0, Vec::Vector_4V_Ref out1) const
		{			
			out0 = m_Buffer[m_BufferIndex];
			out1 = m_Buffer[IIncrementSaturateAndWrap(m_BufferIndex, m_BufferLength)];
		}

		void Write(Vec::Vector_4V_In in0, Vec::Vector_4V_In in1)
		{
			Write(in0);
			Write(in1);
		}

	private:

		Vec::Vector_4V *m_Buffer;
		s32 m_BufferIndex;
		s32 m_BufferLength;

#if __PS3
		Vec::Vector_4V *m_LocalBuffer;
		s32 m_LocalBufferIndex;
		s32 m_LocalBufferLength;		
#endif
	};

template<u32 Length> class revSimpleDelay
{
public:
	enum {MaxLength = Length};
	revSimpleDelay()
		: m_BufIdx(0)
		, m_Length(MaxLength)
	{
		sysMemZeroBytes<sizeof(m_Buffer)>(m_Buffer);
	}

	float Process(const float input)
	{
		const float output = Read();
		Write(input);			
		return output;
	}

	float ReadTap(const u32 offset) const
	{
		return m_Buffer[(m_BufIdx+m_Length-offset) % m_Length];
	}

	float Read() const { return m_Buffer[m_BufIdx]; }
	void Write(const float input) 
	{ 
		m_Buffer[m_BufIdx] = input;
		if(++m_BufIdx >= m_Length)
		{
			m_BufIdx = 0;
		}
	}

	void SetLength(const u32 lengthSamples)
	{
		m_Length = lengthSamples;
	}

private:
	float m_Buffer[MaxLength];
	u32 m_BufIdx;
	u32 m_Length;
};

template<u32 Length> class revLinModDelay
{
public:
	enum {MaxLength = Length};
	revLinModDelay()
		: m_BufIdx(0)
	{
		sysMemZeroBytes<sizeof(m_Buffer)>(m_Buffer);
		m_Length = MaxLength;
	}

	float Process(const float input)
	{
		const float output = Read();
		Write(input);			
		return output;
	}

	float ReadTap(const float offset) const
	{
		s32 index = (s32)offset;
		const float frac = offset - floorf(offset);

		float sample0 = m_Buffer[(m_BufIdx+m_Length-index) % m_Length];
		float sample1 = m_Buffer[(m_BufIdx+1+m_Length-index) % m_Length];

		return Lerp(frac, sample0, sample1);
	}

	float Read() const { return m_Buffer[m_BufIdx]; }
	void Write(const float input) 
	{ 
		m_Buffer[m_BufIdx] = input;
		if(++m_BufIdx >= m_Length)
		{
			m_BufIdx = 0;
		}
	}

	void SetLength(const u32 lengthSamples) { m_Length = lengthSamples; }

private:

	float m_Buffer[MaxLength];
	u32 m_Length;	
	u32 m_BufIdx;
};


class revLinModDelay_V4
{
public:
	
	revLinModDelay_V4()
		: m_BufferIndex(0)
		, m_BufferLength(0)
		, m_Buffer(NULL)
	{
	
	}

	~revLinModDelay_V4()
	{
		if(m_Buffer)
			delete[] m_Buffer;
	}

	bool SetLength(const u32 lengthSamples)
	{
		if(m_Buffer)
		{
			delete[] m_Buffer;
		}
		m_Buffer = rage_aligned_new(16) Vec::Vector_4V[lengthSamples];
		m_BufferLength = lengthSamples;
	
		if(!m_Buffer)
			return false;

		for(u32 i = 0; i < lengthSamples; i++)
		{
			m_Buffer[i] = Vec::V4VConstant(V_ZERO);
		}

		return true;
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		const Vec::Vector_4V output = Read();
		Write(input);	
		return output;
	}

	Vec::Vector_4V_Out ReadTap(const float offset) const
	{
		Vec::Vector_4V offsetV = Vec::V4LoadScalar32IntoSplatted(offset);
		s32 index = (s32)offset;
		
		//frac = offset - floorf(offset);
		Vec::Vector_4V frac = Vec::V4Subtract(offsetV, Vec::V4RoundToNearestIntNegInf(offsetV));

		Vec::Vector_4V sample0 = m_Buffer[(m_BufferIndex+m_BufferLength-index) % m_BufferLength];
		Vec::Vector_4V sample1 = m_Buffer[(m_BufferIndex+1+m_BufferLength-index) % m_BufferLength];

		return Vec::V4Lerp(frac, sample0, sample1);
	}

	Vec::Vector_4V_Out Read() const { return m_Buffer[m_BufferIndex]; }
	void Write(Vec::Vector_4V_In input) 
	{ 
		m_Buffer[m_BufferIndex] = input;
		if(++m_BufferIndex >= m_BufferLength)
		{
			m_BufferIndex = 0;
		}
	}

private:
	u32 m_BufferIndex;
	u32 m_BufferLength;
	Vec::Vector_4V *m_Buffer;
};

template<u32 Length> class revAllPassFig8
{
public:
	revAllPassFig8() 
		: m_Decay(0.f)
		, m_Diffusion(0.f)
	{

	}

	void SetDecay(const float decay)
	{
		m_Decay = decay;
	}

	void SetDiffusion(const float diffusion)
	{
		m_Diffusion = diffusion;
	}

	float Process(const float input)
	{
		const float delayOut = m_Delay.Read();
		const float x = input + (delayOut * m_Diffusion);
		m_Delay.Write(x);
		return (delayOut * m_Decay) - (x * m_Diffusion);
	}

private:
	revSimpleDelay<Length> m_Delay;
	float m_Decay;
	float m_Diffusion;
};

class revAllPassFig8_V4
{
public:
	revAllPassFig8_V4()
		: m_Decay(Vec::V4VConstant(V_ZERO))
		, m_Diffusion(Vec::V4VConstant(V_ZERO))
	{
		
	}

	bool SetLength(const u32 delayLengthSamples)
	{
		return m_DelayLine.SetLength(delayLengthSamples);
	}

	void SetDecay(Vec::Vector_4V_In decayInterleaved)
	{
		m_Decay = decayInterleaved;
	}

	void SetDiffusion(Vec::Vector_4V_In diffusionInterleaved)
	{
		m_Diffusion = diffusionInterleaved;
	}

	Vec::Vector_4V_Out ReadTap(const u32 offset)
	{
		return m_DelayLine.ReadTap(offset);
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		const Vec::Vector_4V delayOut = m_DelayLine.Read();
		// x = input + (delayOut * m_Diffusion);
		Vec::Vector_4V x = Vec::V4AddScaled(input, delayOut, m_Diffusion);
		m_DelayLine.Write(x);
		//(delayOut * m_Decay) - (x * m_Diffusion);
		return Vec::V4SubtractScaled(Vec::V4Scale(delayOut, m_Decay), x, m_Diffusion);
	}

private:
	Vec::Vector_4V m_Decay;
	Vec::Vector_4V m_Diffusion;
	revSimpleDelay_V4 m_DelayLine;
};

template<u32 Length> class revAllPassFig8Mod
{
public:
	revAllPassFig8Mod() 
		: m_Decay(0.f)
		, m_Diffusion(0.f)
	{

	}

	void SetDecay(const float decay)
	{
		m_Decay = decay;
	}

	void SetDiffusion(const float diffusion)
	{
		m_Diffusion = diffusion;
	}

	float Process(const float input, const float tapOffset)
	{
		const float delayOut = m_Delay.ReadTap(tapOffset);
		const float x = input + (delayOut * m_Diffusion);
		m_Delay.Write(x);
		return (delayOut * m_Decay) - (x * m_Diffusion);
	}

private:
	revLinModDelay<Length> m_Delay;
	float m_Decay;
	float m_Diffusion;
};


class revAllPassFig8Mod_V4
{
public:
	revAllPassFig8Mod_V4() 
		: m_Decay(Vec::V4VConstant(V_ZERO))
		, m_Diffusion(Vec::V4VConstant(V_ZERO))
	{

	}

	bool SetLength(const u32 lengthSamples)
	{
		return m_Delay.SetLength(lengthSamples);
	}

	void SetDecay(Vec::Vector_4V_In decay)
	{
		m_Decay = decay;
	}

	void SetDiffusion(Vec::Vector_4V_In diffusion)
	{
		m_Diffusion = diffusion;
	}

	Vec::Vector_4V_Out Process(const Vec::Vector_4V_In input, const float tapOffset)
	{
		const Vec::Vector_4V delayOut = m_Delay.ReadTap(tapOffset);
		const Vec::Vector_4V x = Vec::V4AddScaled(input, delayOut, m_Diffusion);
		m_Delay.Write(x);
		return Vec::V4SubtractScaled(Vec::V4Scale(delayOut, m_Decay), x, m_Diffusion);
	}

private:
	Vec::Vector_4V m_Decay;
	Vec::Vector_4V m_Diffusion;
	revLinModDelay_V4 m_Delay;
};

template<u32 Length> class revAllPass
{
public:
	revAllPass() : m_Diffusion(0.f)
	{
	
	}

	void SetDiffusion(const float diffusion)
	{
		m_Diffusion = diffusion;
	}

	float Process(const float input)
	{
		// Reference: DAFX pg. 177
		// y(n) = -g.x(n) + x(n-m) + g.y(n-m)
		const float xn = input;
		const float g = m_Diffusion;
		const float az = m_Delay.Read();
		const float yn = xn * -g + az;
		m_Delay.Write(yn * g + xn);

		return yn;
	}

private:
	revSimpleDelay<Length> m_Delay;
	float m_Diffusion;

};

class revAllPass_V8
{
public:
	revAllPass_V8()
	{
		m_Diffusion = Vec::V4VConstant(V_ZERO);	
	}

	void SetDiffusion(Vec::Vector_4V_In diffusion)
	{
		m_Diffusion = diffusion;
	}

	void ZeroBuffer()
	{
		m_Delay.ZeroBuffer();
	}

#if !__SPU
	void SetLength(const u32 length)
	{
		m_Delay.SetLength(length);
	}

	void SetLengthS(const float lengthSeconds, const float sampleRate)
	{
		m_Delay.SetLengthS(lengthSeconds, sampleRate);
	}
#endif

	void Process(Vec::Vector_4V_InOut inOut0, Vec::Vector_4V_InOut inOut1)
	{
		// Reference: DAFX pg. 177
		// y(n) = -g.x(n) + x(n-m) + g.y(n-m)

		const Vec::Vector_4V g = m_Diffusion;
		const Vec::Vector_4V negG = Vec::V4Scale(Vec::V4VConstant(V_NEGONE), g);

		const Vec::Vector_4V xn0 = inOut0;
		const Vec::Vector_4V xn1 = inOut1;
		
		Vec::Vector_4V az0, az1;
		m_Delay.Read(az0, az1);
		const Vec::Vector_4V yn0 = Vec::V4AddScaled(az0, xn0, negG);
		const Vec::Vector_4V yn1 = Vec::V4AddScaled(az1, xn1, negG);

		m_Delay.Write(Vec::V4AddScaled(xn0, yn0, g),
						Vec::V4AddScaled(xn1, yn1, g));

		inOut0 = yn0;
		inOut1 = yn1;
	}

	size_t GetSizeBytes() const
	{
		return m_Delay.GetSizeBytes();
	}

#if __SPU
	void Prefetch(const u32 localSizeSamples, const s32 tag)
	{
		m_Delay.Prefetch(localSizeSamples, tag);
	}

	void Writeback(const s32 tag)
	{
		m_Delay.Writeback(tag);
	}
#endif // __SPU

private:
	Vec::Vector_4V m_Diffusion;
	revSimpleDelay_V8 m_Delay;	
};

template<u32 SampleRate> class revStateVariable_V4
{	
public:

	revStateVariable_V4()
	{			
		SetFc(Vec::V4VConstant<0x44FA0000,0x44FA0000,0x44FA0000,0x44FA0000>()); // 2000
		SetResonance(Vec::V4VConstant(V_ZERO));	
		Reset();
	}

	enum {Oversample = 4};

	Vec::Vector_4V_Out operator()(Vec::Vector_4V_In input)
	{
		return Process(input);
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		for(unsigned int i = 0; i < Oversample; i++)
		{
			m_LPF = Vec::V4AddScaled(m_LPF, m_X, m_BPF);
			// hpf = input - lpf - q * bpf
			//     = input - lpf + (-q*bpf)
			m_HPF = Vec::V4AddScaled(Vec::V4Subtract(input, m_LPF), m_NegQ, m_BPF);
			m_BPF = Vec::V4AddScaled(m_BPF, m_X, m_HPF);
		}
		return m_LPF;
	}

	void Reset()
	{
		m_LPF = m_HPF = m_BPF = Vec::V4VConstant(V_ZERO);
	}

	Vec::Vector_4V_Out GetNotch() const
	{
		return Vec::V4Add(m_LPF, m_HPF);
	}

	Vec::Vector_4V_Out GetHPF() const
	{
		return m_HPF;
	}

	Vec::Vector_4V_Out GetLPF() const
	{
		return m_LPF;
	}

	Vec::Vector_4V_Out GetBPF() const
	{
		return m_BPF;
	}

	void SetFc(Vec::Vector_4V_In fc)
	{		
		// sampleRate = (Fs * Oversample)
		CompileTimeAssert(SampleRate == 48000 || SampleRate == 24000);
		CompileTimeAssert(Oversample == 4);

		// PI * Fc / Fs
		// Fc * PI / Fs
		const Vec::Vector_4V fcScalar = SampleRate == 48000 ? Vec::V4VConstant<0x3789421E,0x3789421E,0x3789421E,0x3789421E>() // PI / 192kHz
															: Vec::V4VConstant<0x3809421E,0x3809421E,0x3809421E,0x3809421E>(); // PI / 96kHz
		m_X = Vec::V4Scale(Vec::V4VConstant(V_TWO), Vec::V4Sin(Vec::V4Scale(fcScalar, fc)));
	}

	void SetResonance(Vec::Vector_4V_In resonance)
	{
		// -1 * (2 - 2*reso)
		// = -2 + 2*reso
		m_NegQ = Vec::V4AddScaled(Vec::V4VConstant(V_NEGTWO), Vec::V4VConstant(V_TWO), resonance);
	}

private:

	Vec::Vector_4V m_NegQ;
	Vec::Vector_4V m_X;

	Vec::Vector_4V m_LPF;
	Vec::Vector_4V m_HPF;
	Vec::Vector_4V m_BPF;	
};

template<u32 Length> class revAllPassFig8A
{
public:
	revAllPassFig8A() 
		: m_Diffusion(0.f)
	{

	}

	void SetDiffusion(const float diffusion)
	{
		m_Diffusion = diffusion;
	}

	float Process(const float input)
	{
		const float delayOut = m_Delay.Read();
		const float x = input - m_Diffusion*delayOut;
		m_Delay.Write(x);
		return x * m_Diffusion + delayOut;
	}

	float ReadTap(const u32 index)
	{
		return m_Delay.ReadTap(index);
	}

private:
	revSimpleDelay<Length> m_Delay;
	float m_Diffusion;
};

class revAllPassFig8A_V4
{
public:
	revAllPassFig8A_V4()
		: m_Diffusion(Vec::V4VConstant(V_ZERO))
	{

	}

	bool SetLength(const u32 delayLengthSamples)
	{
		return m_DelayLine.SetLength(delayLengthSamples);
	}

	void SetDiffusion(Vec::Vector_4V_In diffusionInterleaved)
	{
		m_Diffusion = diffusionInterleaved;
	}

	Vec::Vector_4V_Out ReadTap(const u32 offset)
	{
		return m_DelayLine.ReadTap(offset);
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		const Vec::Vector_4V delayOut = m_DelayLine.Read();
		// x = input - (delayOut * m_Diffusion);
		Vec::Vector_4V x = Vec::V4SubtractScaled(input, delayOut, m_Diffusion);
		m_DelayLine.Write(x);
		// return x * m_Diffusion + delayOut
		return Vec::V4AddScaled(delayOut, x, m_Diffusion);
	}

private:
	Vec::Vector_4V m_Diffusion;
	revSimpleDelay_V4 m_DelayLine;
};

template<u32 Length1,u32 Length2> class revAllPassDualFig8
{
public:
	revAllPassDualFig8()
		: m_Decay1(0.f)
		, m_Decay2(0.f)
		, m_DecayDiffusion(0.f)
		, m_Definition(0.f)
	{

	}

	void SetDecay1(const float d)
	{
		m_Decay1 = d;
	}

	void SetDecay2(const float d)
	{
		m_Decay2 = d;
	}

	void SetDecayDiffusion(const float d)
	{
		m_DecayDiffusion = d;
	}

	void SetDefinition(const float d)
	{
		m_Definition = d;
	}

	float Process(const float in)
	{
		float input = in;
		const float feedback2 = m_DecayDiffusion;
		const float feedback1 = m_Definition;

		const float delay2Out = m_Delay2.Read();
		input += feedback2*delay2Out;
		float output = m_Decay2 * delay2Out - input*feedback2;		

		input += feedback1*m_Delay1.Read();
		m_Delay2.Write(m_Decay1 * m_Delay1.Read() - input*feedback1);
		m_Delay1.Write(input);

		return output;

/*
		const float delay2Out = m_Delay2.Read();
		const float x1 = input + delay2Out * m_DecayDiffusion;

		const float delay1Out = m_Delay1.Read();
		const float x2 = delay1Out * m_Definition + x1;

		const float output1 = (delay1Out * m_Decay1) - (x2 * m_Definition);

		m_Delay1.Write(x2);
		m_Delay2.Write(output1);

		return (delay2Out * m_Decay2) - (x1 * m_DecayDiffusion);	*/
	}

private:
	revSimpleDelay<Length1> m_Delay1;
	revSimpleDelay<Length2> m_Delay2;

	float m_Decay1;
	float m_Decay2;
	float m_DecayDiffusion;
	float m_Definition;
};


class revAllPassDualFig8_V4
{
public:
	revAllPassDualFig8_V4()
		: m_Decay1(Vec::V4VConstant(V_ZERO))
		, m_Decay2(Vec::V4VConstant(V_ZERO))
		, m_DecayDiffusion(Vec::V4VConstant(V_ZERO))
		, m_Definition(Vec::V4VConstant(V_ZERO))
	{

	}

	bool SetLength(const u32 lengthSamples1, const u32 lengthSamples2)
	{
		if(!m_Delay1.SetLength(lengthSamples1))
			return false;
		if(!m_Delay2.SetLength(lengthSamples2))
			return false;
		return true;
	}

	void SetDecay1(Vec::Vector_4V_In d)
	{
		m_Decay1 = d;
	}

	void SetDecay2(Vec::Vector_4V_In d)
	{
		m_Decay2 = d;
	}

	void SetDecayDiffusion(Vec::Vector_4V_In d)
	{
		m_DecayDiffusion = d;
	}

	void SetDefinition(Vec::Vector_4V_In d)
	{
		m_Definition = d;
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In in)
	{
		Vec::Vector_4V input = in;
		const Vec::Vector_4V feedback2 = m_DecayDiffusion;
		const Vec::Vector_4V feedback1 = m_Definition;

		const Vec::Vector_4V delay2Out = m_Delay2.Read();
		input = Vec::V4AddScaled(input, feedback2, delay2Out);
		Vec::Vector_4V output = Vec::V4SubtractScaled(Vec::V4Scale(m_Decay2, delay2Out), input, feedback2);

		input = Vec::V4AddScaled(input, feedback1, m_Delay1.Read());
		m_Delay2.Write(Vec::V4SubtractScaled(Vec::V4Scale(m_Decay1, m_Delay1.Read()), input, feedback1));
		m_Delay1.Write(input);

		return output;

/*
		const float delay2Out = m_Delay2.Read();
		const float x1 = input + delay2Out * m_DecayDiffusion;

		const float delay1Out = m_Delay1.Read();
		const float x2 = delay1Out * m_Definition + x1;

		const float output1 = (delay1Out * m_Decay1) - (x2 * m_Definition);

		m_Delay1.Write(x2);
		m_Delay2.Write(output1);

		return (delay2Out * m_Decay2) - (x1 * m_DecayDiffusion);	*/
	}

private:

	Vec::Vector_4V m_Decay1;
	Vec::Vector_4V  m_Decay2;
	Vec::Vector_4V  m_DecayDiffusion;
	Vec::Vector_4V  m_Definition;

	revSimpleDelay_V4 m_Delay1;
	revSimpleDelay_V4 m_Delay2;
};

class revDCBlocker
{
public:

	revDCBlocker()
		: m_A(0.f)
		, m_X1(0.f)
		, m_Y1(0.f)
	{

	}

	void SetA(const float a)
	{
		m_A = a;
	}

	void SetFc(const float fc)
	{
		const float sqrt3 = 1.73205081f;
		float _fc = 2.f*fc/(float)kMixerNativeSampleRate;
		
		const float alpha = _fc * PI;
		const float s = Sinf(alpha);
		const float c = Cosf(alpha);
		m_A = (sqrt3 - 2.f*s)/(s + sqrt3*c);
	}

	float Process(const float input)
	{
		float output = input - m_X1 + m_A * m_Y1;
		m_X1 = input;
		m_Y1 = output;

		return output;
	}
private:

	float m_A;
	float m_X1,m_Y1;
};


class revDCBlocker_V4
{
public:

	revDCBlocker_V4()
		: m_A(Vec::V4VConstant(V_ZERO))
		, m_X1(Vec::V4VConstant(V_ZERO))
		, m_Y1(Vec::V4VConstant(V_ZERO))
	{

	}

	void SetA(const Vec::Vector_4V_In a)
	{
		m_A = a;
	}

	void SetFc(const Vec::Vector_4V_In fc)
	{
		// sqrt3 = 1.73205081f;
		Vec::Vector_4V sqrt3 = Vec::V4VConstantSplat<0x3FDDB3D7>();

		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		Vec::Vector_4V twoPiOverFs = Vec::V4VConstantSplat<0x3909421E>();
		//Vec::Vector_4V _fc = 2.f*fc/(float)kMixerNativeSampleRate;

		Vec::Vector_4V alpha = Vec::V4Scale(fc, twoPiOverFs);

		Vec::Vector_4V s,c;
		Vec::V4SinAndCos(s,c,alpha);
		
		//m_A = (sqrt3 - 2.f*s)/(s + sqrt3*c);
		m_A = Vec::V4InvScale(
					Vec::V4SubtractScaled(sqrt3, Vec::V4VConstant(V_TWO), s), 
					Vec::V4AddScaled(s, sqrt3, c)
			);
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		//float output = input - m_X1 + m_A * m_Y1;
		Vec::Vector_4V output = Vec::V4AddScaled(Vec::V4Subtract(input, m_X1), m_A, m_Y1);
		
		m_X1 = input;
		m_Y1 = output;

		return output;
	}
private:

	Vec::Vector_4V m_A;
	Vec::Vector_4V m_X1,m_Y1;
};

template<u32 Length1, u32 Length2, u32 Length3, u32 Length4> class revAllPassTrioChorus
{
public:

	revAllPassTrioChorus()
		: m_DecayDiffusion(0.f)
		, m_Definition(0.f)
		, m_Decay1(0.f)
		, m_Decay2(0.f)
		, m_ChorusWet(0.f)
		, m_ChorusDry(0.f)
	{

	}

	void SetDecay1(const float d)
	{
		m_Decay1 = d;
	}

	void SetDecay2(const float d)
	{
		m_Decay2 = d;
	}

	void SetDecayDiffusion(const float d)
	{
		m_DecayDiffusion = d;
	}

	void SetDefinition(const float d)
	{
		m_Definition = d;
	}

	void SetChorusDry(const float d)
	{
		m_ChorusDry = d;
	}

	void SetChorusWet(const float d)
	{
		m_ChorusWet = d;
	}

	float Process(const float input)
	{
		const float delay4Out = m_D4.Read();
		const float x1 = input + m_DecayDiffusion * delay4Out;

		const float delay3Out = m_D3.Read();
		const float x2 = x1 + delay3Out * m_Definition;
		m_D4.Write(delay3Out*m_Decay1 - x2*m_Definition);

		const float delay0Out = m_D0.Read();
		const float delay1Out = m_D1.Read();
		m_D1.Write(delay0Out);
		m_D2.Write(delay1Out);
		const float chorusOut = m_D2.Read() * m_ChorusWet + delay1Out * m_ChorusDry;

		const float x3 = x2 + chorusOut*m_Definition;
		m_D3.Write(chorusOut * m_Decay1 - x3 * m_Definition);
		m_D0.Write(x3);

		return delay4Out * m_Decay2 - x1*m_DecayDiffusion;

	}
private:

	revSimpleDelay<Length1> m_D0;
	revSimpleDelay<Length2> m_D1;
	revSimpleDelay<1> m_D2;
	revSimpleDelay<Length3> m_D3;
	revSimpleDelay<Length4> m_D4;

	float m_DecayDiffusion;
	float m_Definition;
	float m_Decay1;
	float m_Decay2;
	float m_ChorusWet;
	float m_ChorusDry;
};


class revAllPassTrioChorus_V4
{
public:

	revAllPassTrioChorus_V4()
		: m_DecayDiffusion(Vec::V4VConstant(V_ZERO))
		, m_Definition(Vec::V4VConstant(V_ZERO))
		, m_Decay1(Vec::V4VConstant(V_ZERO))
		, m_Decay2(Vec::V4VConstant(V_ZERO))
		, m_ChorusWet(Vec::V4VConstant(V_ZERO))
		, m_ChorusDry(Vec::V4VConstant(V_ZERO))
	{

	}

	bool SetLength(const u32 length1, const u32 length2, const u32 length3, const u32 length4)
	{
		if(!m_D0.SetLength(length1))
			return false;
		if(!m_D1.SetLength(length2))
			return false;
		if(!m_D2.SetLength(1))
			return false;
		if(!m_D3.SetLength(length3))
			return false;
		if(!m_D4.SetLength(length4))
			return false;

		return true;
	}

	void SetDecay1(Vec::Vector_4V_In d)
	{
		m_Decay1 = d;
	}

	void SetDecay2(Vec::Vector_4V_In d)
	{
		m_Decay2 = d;
	}

	void SetDecayDiffusion(Vec::Vector_4V_In d)
	{
		m_DecayDiffusion = d;
	}

	void SetDefinition(Vec::Vector_4V_In d)
	{
		m_Definition = d;
	}

	void SetChorusDry(Vec::Vector_4V_In d)
	{
		m_ChorusDry = d;
	}

	void SetChorusWet(Vec::Vector_4V_In d)
	{
		m_ChorusWet = d;
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		Vec::Vector_4V delay4Out = m_D4.Read();
		Vec::Vector_4V x1 = Vec::V4AddScaled(input, m_DecayDiffusion, delay4Out);

		Vec::Vector_4V delay3Out = m_D3.Read();
		Vec::Vector_4V x2 = Vec::V4AddScaled(x1, delay3Out, m_Definition);
		m_D4.Write(Vec::V4SubtractScaled(Vec::V4Scale(delay3Out, m_Decay1), x2, m_Definition));

		Vec::Vector_4V delay0Out = m_D0.Read();
		Vec::Vector_4V delay1Out = m_D1.Read();
		m_D1.Write(delay0Out);
		m_D2.Write(delay1Out);
		Vec::Vector_4V chorusOut = Vec::V4AddScaled(Vec::V4Scale(m_D2.Read(), m_ChorusWet), delay1Out, m_ChorusDry);

		Vec::Vector_4V x3 = Vec::V4AddScaled(x2, chorusOut, m_Definition);
		m_D3.Write(Vec::V4SubtractScaled(Vec::V4Scale(chorusOut, m_Decay1), x3, m_Definition));
		m_D0.Write(x3);

		return Vec::V4SubtractScaled(Vec::V4Scale(delay4Out, m_Decay2), x1, m_DecayDiffusion);

	}
private:
	Vec::Vector_4V m_DecayDiffusion;
	Vec::Vector_4V m_Definition;
	Vec::Vector_4V m_Decay1;
	Vec::Vector_4V m_Decay2;
	Vec::Vector_4V m_ChorusWet;
	Vec::Vector_4V m_ChorusDry;

	revSimpleDelay_V4 m_D0;
	revSimpleDelay_V4 m_D1;
	revSimpleDelay_V4 m_D2;
	revSimpleDelay_V4 m_D3;
	revSimpleDelay_V4 m_D4;	
};

class revOnePole
{
public:
	revOnePole()
		: m_y(0.f)
	{

	}

	float Damping;
	float Process(const float input)
	{
		const float output = (1.f-Damping) * input + Damping * m_y;
		m_y = output;
		return output;
	}

private:

	float m_y;
};

class revOnePole_V4
{
public:
	revOnePole_V4()
		: m_a0(Vec::V4VConstant(V_ONE))
		, m_negb1(Vec::V4VConstant(V_ZERO))
		, m_y(Vec::V4VConstant(V_ZERO))
		
	{

	}

	void SetDamping(Vec::Vector_4V_In damping)
	{
		m_negb1 = Vec::V4Negate(damping);
		m_a0 = Vec::V4Subtract(Vec::V4VConstant(V_ONE), damping);
	}

	void SetFc_24kHz(Vec::Vector_4V_In fc)
	{
		// Compute coefficients
		//-2 * PI * freq / Fs
		// = (-2 * PI * 1/Fs) * freq		
		const Vec::Vector_4V freqScalar = Vec::V4VConstantSplat<0xB989421E>();

		const Vec::Vector_4V x = audDriverUtil::V4ExpE(Vec::V4Scale(fc, freqScalar));
		m_a0 = Vec::V4Subtract(Vec::V4VConstant(V_ONE), x);
		m_negb1 = x;
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		//const f32 lowpassOutput = (a0*input - b1 * prevSample);
		m_y = Vec::V4AddScaled(Vec::V4Scale(m_a0, input), m_negb1, m_y);	
		return m_y;
	}

	Vec::Vector_4V_Out operator()(Vec::Vector_4V_In input)
	{
		return Process(input);
	}

private:

	Vec::Vector_4V m_a0;
	Vec::Vector_4V m_negb1;
	Vec::Vector_4V m_y;
};

class rev1PoleLPF_V4
{
public:

	rev1PoleLPF_V4()
	{
		m_A0 = m_B1 = m_Y = Vec::V4VConstant(V_ZERO);
	}

	Vec::Vector_4V_Out operator()(Vec::Vector_4V_In input)
	{
		return Process(input);
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{		
		return (m_Y = Vec::V4Subtract(Vec::V4Scale(m_A0, input), Vec::V4Scale(m_B1, m_Y)));
	}

	void SetFc(Vec::Vector_4V_In fc)
	{
		//-2 * PI * freq / Fc
		// = -2 * PI * 1/Fc * freq
		const Vec::Vector_4V freqScalar = Vec::V4VConstant<0xB909421E,0xB909421E,0xB909421E,0xB909421E>();

		const Vec::Vector_4V x = audDriverUtil::V4ExpE(Vec::V4Scale(fc, freqScalar));
		m_A0 = Vec::V4Subtract(Vec::V4VConstant(V_ONE), x);
		m_B1 = Vec::V4Scale(x, Vec::V4VConstant(V_NEGONE));
	}

private:

	Vec::Vector_4V m_A0, m_B1;
	Vec::Vector_4V m_Y;
};

class revBiquad_V4
{
public:
	revBiquad_V4()
	{
		m_a0 = m_a1 = m_a2 = m_b1 = m_b2 = Vec::V4VConstant(V_ZERO);
		m_xy11 = m_xy21 = m_xy12 = m_xy22 = Vec::V4VConstant(V_ZERO);
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		const Vec::Vector_4V currentSample1 = input;
		const Vec::Vector_4V newSample1 = Vec::V4AddScaled(m_xy11, m_a0, currentSample1);
		const Vec::Vector_4V currentSample2 = newSample1; //Current sample is now output of last stage.
		const Vec::Vector_4V newSample2 = Vec::V4AddScaled(m_xy12,  m_a0, currentSample2);

		m_xy11 = Vec::V4Add(Vec::V4Subtract(Vec::V4Scale(m_a1, currentSample1), Vec::V4Scale(m_b1, newSample1)), m_xy21);
		m_xy12 = Vec::V4Add(Vec::V4Subtract(Vec::V4Scale(m_a1, currentSample2), Vec::V4Scale(m_b1, newSample2)), m_xy22);
		m_xy21 = Vec::V4Subtract(Vec::V4Scale(m_a2, currentSample1), Vec::V4Scale(m_b2, newSample1));
		m_xy22 = Vec::V4Subtract(Vec::V4Scale(m_a2, currentSample2), Vec::V4Scale(m_b2, newSample2));

		return newSample2;
	}

	static Vec::Vector_4V_Out Sinh(Vec::Vector_4V_In x)
	{
		// (e^2x - 1) / 2e^x

		Vec::Vector_4V a = Vec::V4Subtract(audDriverUtil::V4ExpE(Vec::V4Scale(Vec::V4VConstant(V_TWO), x)), Vec::V4VConstant(V_ONE));
		Vec::Vector_4V b = Vec::V4Scale(Vec::V4VConstant(V_TWO), audDriverUtil::V4ExpE(x));

		return Vec::V4InvScale(a, b);
	}

	void Set4PoleLPF(const float fc, const float resonance)
	{
		Set4PoleLPF(Vec::V4LoadScalar32IntoSplatted(fc), Vec::V4LoadScalar32IntoSplatted(resonance));
	}

	void Set4PoleLPF(Vec::Vector_4V_In fc, Vec::Vector_4V_In resonance)
	{		
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		
		const Vec::Vector_4V twoPiOverFs = Vec::V4VConstant<0x3909421E,0x3909421E,0x3909421E,0x3909421E>();
		const Vec::Vector_4V omega = Vec::V4Scale(fc, twoPiOverFs);
		
		Vec::Vector_4V cs;
		Vec::Vector_4V sn;
		Vec::V4SinAndCos(sn, cs, omega);
		
		// resonance * 0.5 to compensate for cascaded sections
		Vec::Vector_4V alpha = Vec::V4Scale(sn, 
										Sinh(Vec::V4InvScale(Vec::V4VConstant(V_HALF), Vec::V4Scale(resonance, Vec::V4VConstant(V_HALF))))); 
		
		Vec::Vector_4V a1 = Vec::V4Subtract(Vec::V4VConstant(V_ONE), cs);
		Vec::Vector_4V a0 = Vec::V4Scale(a1, Vec::V4VConstant(V_HALF));
		Vec::Vector_4V a2 = a0;

		Vec::Vector_4V b0 = Vec::V4Add(Vec::V4VConstant(V_ONE), alpha);
		Vec::Vector_4V b1 = Vec::V4Scale(Vec::V4VConstant(V_NEGTWO), cs);
		Vec::Vector_4V b2 = Vec::V4Subtract(Vec::V4VConstant(V_ONE), alpha);

		//Normalize so b0 = 1.0.
		m_a0 = Vec::V4InvScale(a0, b0);
		m_a1 = Vec::V4InvScale(a1, b0);
		m_a2 = Vec::V4InvScale(a2, b0);
		m_b1 = Vec::V4InvScale(b1, b0);
		m_b2 = Vec::V4InvScale(b2, b0);
	}

	void Set4PoleBPF(const float fc, const float bw)
	{
		Set4PoleBPF(Vec::V4LoadScalar32IntoSplatted(fc), Vec::V4LoadScalar32IntoSplatted(bw));
	}

	void Set4PoleBPF(Vec::Vector_4V_In fc, Vec::Vector_4V_In bandwidth)
	{
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const Vec::Vector_4V fs = Vec::V4VConstantSplat<0x473B8000>();
		const Vec::Vector_4V twoPiOverFs = Vec::V4VConstantSplat<0x3909421E>();
		const Vec::Vector_4V omega = Vec::V4Scale(fc, twoPiOverFs);
		
		const Vec::Vector_4V c = Vec::V4Invert(Vec::V4Tan(Vec::V4Scale(Vec::V4VConstant(V_PI), Vec::V4InvScale(bandwidth, fs))));
		const Vec::Vector_4V d = Vec::V4Scale(Vec::V4VConstant(V_TWO), Vec::V4Cos(omega));

		m_a0 = Vec::V4Invert(Vec::V4Add(Vec::V4VConstant(V_ONE), c));
		m_a1 = Vec::V4VConstant(V_ZERO);
		m_a2 = Vec::V4Scale(Vec::V4VConstant(V_NEGONE), m_a0);
		m_b1 = Vec::V4Scale(Vec::V4Scale(m_a2, c), d);
		m_b2 = Vec::V4Scale(m_a0,  Vec::V4Subtract(c, Vec::V4VConstant(V_ONE)));

		Assert(Vec::V4IsFiniteAll(m_a0));
		Assert(Vec::V4IsFiniteAll(m_a1));
		Assert(Vec::V4IsFiniteAll(m_a2));
		Assert(Vec::V4IsFiniteAll(m_b1));
		Assert(Vec::V4IsFiniteAll(m_b2));
	}

	void Validate() const
	{
		Assert(Vec::V4IsFiniteAll(m_xy11));
		Assert(Vec::V4IsFiniteAll(m_xy12));
		Assert(Vec::V4IsFiniteAll(m_xy21));
		Assert(Vec::V4IsFiniteAll(m_xy22));
	}

private:

	Vec::Vector_4V m_a0, m_a1, m_a2;
	Vec::Vector_4V m_b1, m_b2;
	Vec::Vector_4V m_xy11, m_xy21;
	Vec::Vector_4V m_xy12, m_xy22;
};

class revEnvFollower_V4
{
public:

	revEnvFollower_V4()
	{
		m_A = m_R = m_Prev = Vec::V4VConstant(V_ZERO);
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In inputSignal)
	{
		const Vec::Vector_4V denormLimit = Vec::V4VConstantSplat<0x233877AA>(); // 1e-17
		
		const Vec::Vector_4V input = Vec::V4Abs(inputSignal);

		// envelope >= input ? release : attack
		const Vec::Vector_4V factor = Vec::V4SelectFT(Vec::V4IsGreaterThanOrEqualV(m_Prev, input), m_A, m_R);
		const Vec::Vector_4V unclampedVal = Vec::V4AddScaled(input, factor, Vec::V4Subtract(m_Prev,  input));
		
		// don't allow output to go denormal; clamp to 0
		const Vec::Vector_4V isGreaterMask = Vec::V4IsGreaterThanOrEqualV(unclampedVal, denormLimit);
		m_Prev = Vec::V4And(unclampedVal, isGreaterMask);		

		return m_Prev;
	}

	void Validate() const
	{
		Assert(Vec::V4IsFiniteAll(m_Prev));
	}

	void SetAttackRelease(Vec::Vector_4V_In attack, Vec::Vector_4V_In release)
	{
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const Vec::Vector_4V oneOverFs = Vec::V4VConstantSplat<0x37AEC33E>();
		
		// 0.00001f
		const Vec::Vector_4V vSmallValue = Vec::V4VConstantSplat<0x3727C5AC>();
		
		const Vec::Vector_4V scaledAttack = Vec::V4InvScale(oneOverFs, Vec::V4Max(vSmallValue, attack));
		const Vec::Vector_4V scaledRelease = Vec::V4InvScale(oneOverFs, Vec::V4Max(vSmallValue, release));
		
#if 0 // V4Pow() is not very consistent across platforms
		// 0.01
		const Vec::Vector_4V v0_01 = Vec::V4VConstantSplat<0x3C23D70A>();
		m_A = audDriverUtil::V4Pow(v0_01, scaledAttack);
		m_R = audDriverUtil::V4Pow(v0_01, scaledRelease);
#else
		// Super-slow, but not called often
		Vec::SetX(m_A, ::powf(0.01f, Vec::GetX(scaledAttack)));
		Vec::SetY(m_A, ::powf(0.01f, Vec::GetY(scaledAttack)));
		Vec::SetZ(m_A, ::powf(0.01f, Vec::GetZ(scaledAttack)));
		Vec::SetW(m_A, ::powf(0.01f, Vec::GetW(scaledAttack)));

		Vec::SetX(m_R, ::powf(0.01f, Vec::GetX(scaledRelease)));
		Vec::SetY(m_R, ::powf(0.01f, Vec::GetY(scaledRelease)));
		Vec::SetZ(m_R, ::powf(0.01f, Vec::GetZ(scaledRelease)));
		Vec::SetW(m_R, ::powf(0.01f, Vec::GetW(scaledRelease)));
#endif
	}

private:

	Vec::Vector_4V m_A;
	Vec::Vector_4V m_R;
	Vec::Vector_4V m_Prev;
};

class revHalfBandAllpass
{
public:

	revHalfBandAllpass()
	{
		m_a = Vec::V4VConstant(V_ZERO);
		m_x0 = m_x1 = m_x2 = Vec::V4VConstant(V_ZERO);
		m_y0 = m_y1 = m_y2 = Vec::V4VConstant(V_ZERO);
	}

	void SetA(Vec::Vector_4V_In a)
	{
		m_a = a;
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		m_x2 = m_x1;
		m_x1 = m_x0;
		m_x0 = input;

		m_y2 = m_y1;
		m_y1 = m_y0;
		const Vec::Vector_4V output = Vec::V4AddScaled(m_x2, Vec::V4Subtract(input, m_y2), m_a);
		m_y0 = output;

		return output;
	}

private:

	Vec::Vector_4V m_a;
	Vec::Vector_4V m_x0, m_x1, m_x2;
	Vec::Vector_4V m_y0, m_y1, m_y2;
};

class rev4thOrderHalfBandFilter
{
public:
	rev4thOrderHalfBandFilter()
	{
		// 4th order; 70dB rejection (0.1 transition band)
		// 0.07986642623635751
		// 0.5453536510711322
		m_FilterA[0].SetA(Vec::V4VConstant<0x3DA39102,0x3DA39102,0x3DA39102,0x3DA39102>());
		m_FilterA[1].SetA(Vec::V4VConstant<0x3F0B9C4C,0x3F0B9C4C,0x3F0B9C4C,0x3F0B9C4C>());

		//0.28382934487410993
		//0.8344118914807379
		m_FilterB[0].SetA(Vec::V4VConstant<0x3E915214,0x3E915214,0x3E915214,0x3E915214>());
		m_FilterB[1].SetA(Vec::V4VConstant<0x3F559C05,0x3F559C05,0x3F559C05,0x3F559C05>());

		m_y = Vec::V4VConstant(V_ZERO);		
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		Vec::Vector_4V output = Vec::V4Scale(Vec::V4VConstant(V_HALF), Vec::V4Add(m_y, m_FilterA[1].Process(m_FilterA[0].Process(input))));
		m_y = m_FilterB[1].Process(m_FilterB[0].Process(input));
		return output;
	}

private:
	atRangeArray<revHalfBandAllpass, 2> m_FilterA;
	atRangeArray<revHalfBandAllpass, 2> m_FilterB;

	Vec::Vector_4V m_y;
};

class rev8thOrderHalfBandFilter
{
public:
	rev8thOrderHalfBandFilter()
	{
		// 8th order; 106dB rejection, transition band = 0.05
		//0.03583278843106211
		//0.2720401433964576
		//0.5720571972357003
		//0.827124761997324
		
		m_FilterA[0].SetA(Vec::V4VConstant<0x3D12C567,0x3D12C567,0x3D12C567,0x3D12C567>());
		m_FilterA[1].SetA(Vec::V4VConstant<0x3E8B48D8,0x3E8B48D8,0x3E8B48D8,0x3E8B48D8>());
		m_FilterA[2].SetA(Vec::V4VConstant<0x3F127257,0x3F127257,0x3F127257,0x3F127257>());
		m_FilterA[3].SetA(Vec::V4VConstant<0x3F53BE73,0x3F53BE73,0x3F53BE73,0x3F53BE73>());

		//0.1340901419430669
		//0.4243248712718685
		//0.7062921421386394
		//0.9415030941737551
		m_FilterB[0].SetA(Vec::V4VConstant<0x3E094EED,0x3E094EED,0x3E094EED,0x3E094EED>());
		m_FilterB[1].SetA(Vec::V4VConstant<0x3ED9411C,0x3ED9411C,0x3ED9411C,0x3ED9411C>());
		m_FilterB[2].SetA(Vec::V4VConstant<0x3F34CF90,0x3F34CF90,0x3F34CF90,0x3F34CF90>());
		m_FilterB[3].SetA(Vec::V4VConstant<0x3F710659,0x3F710659,0x3F710659,0x3F710659>());

		m_y = Vec::V4VConstant(V_ZERO);		
	}

	Vec::Vector_4V_Out Process(Vec::Vector_4V_In input)
	{
		Vec::Vector_4V output = Vec::V4Scale(Vec::V4VConstant(V_HALF), Vec::V4Add(m_y, m_FilterA[3].Process(m_FilterA[2].Process(m_FilterA[1].Process(m_FilterA[0].Process(input))))));
		m_y = m_FilterB[3].Process(m_FilterB[2].Process(m_FilterB[1].Process(m_FilterB[0].Process(input))));
		return output;
	}

private:
	atRangeArray<revHalfBandAllpass, 4> m_FilterA;
	atRangeArray<revHalfBandAllpass, 4> m_FilterB;

	Vec::Vector_4V m_y;
};

class itu2ndOrderFilter
{
public:
	itu2ndOrderFilter() : x_1(0.f), x_2(0.f) {}

	void SetupPrefilter()
	{
		a1 = -1.69065929318241;
		a2 = 0.73248077421585;
		b0 = 1.53512485958697;
		b1 = -2.69169618940638;
		b2 = 1.19839281085285;
	}

	void SetupRLBFilter()
	{
		a1 = -1.99004745483398;
		a2 = 0.99007225036621;
		b0 = 1.0;
		b1 = -2.0;
		b2 = 1.0;
	} 

	float Process(const float x)
	{
		// ITU-R BS.1770-1 second order filter implementation
		// see: http://webs.uvigo.es/servicios/biblioteca/uit/rec/BS/R-REC-BS.1770-1-200709-I!!PDF-E.pdf

		const double temp = x - (x_1*a1 + x_2*a2);

		const double y = temp * b0 + b1 * x_1 + b2 * x_2;
		x_2 = x_1;
		x_1 = temp;

		return (float)y;
	}

private:
	double x_1, x_2;
	double a1,a2,b0,b1,b2;
};

// PURPOSE
//	Implements a surround sound loudness meter to ITU-R BS.1770-1 spec
class audSurroundLoudnessMeter
{
public:

	enum {kMaxNumChannels = 6};
	audSurroundLoudnessMeter()
	{
		for(s32 i = 0; i < kMaxNumChannels; i++)
		{
			m_PreFilters[i].SetupPrefilter();
			m_RLBFilters[i].SetupRLBFilter();

			m_Zi[i] = 0.f;
		}

		SetWindowLength(0.4f);

		m_Loudness = 0.f;
	}

	void Process(atRangeArray<const float *, kMaxNumChannels> &buffers, const s32 numChannelsToProcess, const s32 numSamples)
	{
		for(s32 channelIndex = 0; channelIndex < numChannelsToProcess; channelIndex++)
		{
			float zi_sum = 0.f;		
			if(buffers[channelIndex])
			{
				for(s32 sampleIndex = 0; sampleIndex < numSamples; sampleIndex++)
				{
					const float x = buffers[channelIndex][sampleIndex];
					const float yi = m_RLBFilters[channelIndex].Process(m_PreFilters[channelIndex].Process(x));

					zi_sum += yi*yi;
				}
				m_DelayLines[channelIndex].Write(zi_sum);


				zi_sum = 0.f;
				for(s32 sampleIndex = 0; sampleIndex < m_WindowLength; sampleIndex++)
				{
					zi_sum += m_DelayLines[channelIndex].ReadTap(sampleIndex);
				}

				m_Zi[channelIndex] = zi_sum * (1.f / (float)(m_WindowLength * kMixBufNumSamples));
			}
		}

		float weightedChannelSum = 0.f;

		// NOTE: these are in RAGE speaker order
		// Ignore LFE?
		const float G[] = {1.f,1.f,1.41f,1.41f,1.f,0.f};
		for(s32 channelIndex = 0; channelIndex < numChannelsToProcess; channelIndex++)
		{
			weightedChannelSum += m_Zi[channelIndex] * G[channelIndex];
		}
		m_Loudness = -0.691f + 10.f * log10f(weightedChannelSum);

	}

	float GetLoudness() const { return m_Loudness; }
	float GetZi(const audRageSpeakerIds channelId) const { return m_Zi[channelId]; }

	void SetWindowLength(const float windowLengthSeconds)
	{
		// Convert window length to buffer count
		m_WindowLength = round(kMixerNativeSampleRate * windowLengthSeconds / (float)kMixBufNumSamples);
		for(u32 i = 0; i < kMaxNumChannels; i++)
		{
			m_DelayLines[i].SetLength((u32)m_WindowLength);
		}
	}

private:

	atRangeArray<revSimpleDelay<3 * kMixerNativeSampleRate / kMixBufNumSamples + 1>, kMaxNumChannels> m_DelayLines;
	atRangeArray<float, kMaxNumChannels> m_Zi;
	atRangeArray<itu2ndOrderFilter, kMaxNumChannels> m_PreFilters;
	atRangeArray<itu2ndOrderFilter, kMaxNumChannels> m_RLBFilters;

	s32 m_WindowLength;
	float m_Loudness;
};
#if __WIN32
#pragma warning(pop)
#endif
} // namespace rage
#endif // AUD_BLOCKS_H

