// 
// audiosynth/chartrecorderview.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "chartrecorder.h"
#include "chartrecorderview.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"
#include "uiplane.h"
#include "modulegroup.h"

#include "input/mouse.h"

namespace rage
{
    synthChartRecorderView::synthChartRecorderView(synthModule *module) 
        : synthModuleView(module)
        , m_Width(8.5f)
        , m_Height(4.f)
        , m_LastWriteOffset(0)
        , m_IsResizing(false)
    {
        AddComponent(&m_WaveDataView);
    }

    void synthChartRecorderView::GetBounds(Vector3 &v1, Vector3 &v2) const
    {
        Matrix34 mat = GetMatrix();
        Vector3 halfSize(m_Width * 0.5f, m_Height * 0.5f, 0.f);
        v1 = mat.d - halfSize;
        v2 = mat.d + halfSize;
    }

    void synthChartRecorderView::Update()
    {
        synthPinView *inputPinView = GetModule()->GetInput(1).GetView();
        inputPinView->DisableZooming(true);

        Matrix34 mat = GetMatrix();
        mat.d += mat.a*m_Width*0.5f;
        inputPinView->SetMatrix(mat);

        inputPinView = GetModule()->GetInput(0).GetView();
        mat.d += mat.b * 2.7f;
        inputPinView->SetMatrix(mat);

        Vector3 mousePos = synthUIPlane::GetActive()->GetMousePosition();

        GetMatrix().UnTransform(mousePos);

        const f32 relX = 1.f - Clamp(0.5f + (mousePos.x / m_Width), 0.f, 1.f);
        const f32 relY = 1.f - Clamp(0.5f + (mousePos.y / m_Height), 0.f, 1.f);

        if(!m_IsResizing && IsUnderMouse() && ioMouse::GetButtons()&ioMouse::MOUSE_LEFT && relX >= 0.75f && relY >= 0.75f)
        {
            m_ResizedHeight = m_Height;
            m_ResizedWidth = m_Width;
            m_IsResizing = true;

            synthUIPlane::GetActive()->SetIsMouseFixed(true);
        }
        else if(m_IsResizing)
        {
            if(!ioMouse::GetButtons()&ioMouse::MOUSE_LEFT)
            {
                m_IsResizing = false;
                synthUIPlane::GetActive()->SetIsMouseFixed(false);

                Vector3 offset(0.5f * (m_Width - m_ResizedWidth), 0.5f * (m_Height - m_ResizedHeight), 0);

                Matrix34 mat = GetMatrix();
                mat.d += offset;
                SetMatrix(mat);

                synthUIPlane::GetActive()->SetMousePosition(synthUIPlane::GetActive()->GetMousePosition() + offset);

                m_Width = m_ResizedWidth;
                m_Height = m_ResizedHeight;
            }
            else
            {
                m_ResizedHeight += ioMouse::GetDY() * 0.05f;
                m_ResizedWidth += ioMouse::GetDX() * 0.05f;

                m_ResizedWidth = Max(m_ResizedWidth, 5.f);
                m_ResizedHeight = Max(m_ResizedHeight, 5.f);
            }
        }
        
        m_WaveDataView.SetMatrix(GetMatrix());
        m_WaveDataView.SetSize(m_Width * 0.9f,m_Height * 0.7f);
        m_WaveDataView.SetWaveData(GetRecorderModule()->GetBuffer(), GetRecorderModule()->GetBufferLength());
        u32 currentWriteOffset = GetRecorderModule()->GetWriteOffset();
        m_WaveDataView.SetUpdateRegion(m_LastWriteOffset, currentWriteOffset);
        m_LastWriteOffset = currentWriteOffset;
        synthModuleView::Update();

        m_TitleLabel.SetShouldDraw(false);
    }
    
    void synthChartRecorderView::Render()
    {
    
        synthModuleView::Render();

        if(m_IsResizing)
        {
            Vector3 v1,v2;
            Matrix34 mat = GetMatrix();
            
            Vector3 halfSize(m_ResizedWidth * 0.5f, m_ResizedHeight * 0.5f, 0.f);
            v1 = mat.d - halfSize;
            v2 = mat.d + halfSize;

            Vector3 offset(0.5f * (m_Width - m_ResizedWidth), 0.5f * (m_Height - m_ResizedHeight), 0);

            v1 += offset;
            v2 += offset;
            grcDrawBox(v1,v2,Color32(1.f,1.f,1.0f,1.f));
        }
    }
}

#endif // __SYNTH_EDITOR
