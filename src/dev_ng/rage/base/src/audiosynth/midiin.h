
#ifndef SYNTH_MIDIIN_H
#define SYNTH_MIDIIN_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "audiohardware/midi.h"

namespace rage
{
	enum {kMidiInMaxPolyphony = 16};
	enum {kMidiInNumOutputs = kMidiInMaxPolyphony*3};
	class synthMidiIn : public synthModuleBase<0, kMidiInNumOutputs, SYNTH_MIDIIN>
	{
	public:
		SYNTH_MODULE_NAME("MIDI In");
		SYNTH_CUSTOM_MODULE_VIEW;

		synthMidiIn();
	
		virtual void SerializeState(synthModuleSerializer *serializer);

		virtual void Synthesize();

		virtual void GetOutputs(synthPin *&pins, u32 &numPins);

		u32 GetPolyphony() const {return m_Polyphony; }
		void SetPolyphony(const u32 polyphony) { m_Polyphony = Min<u32>(kMidiInMaxPolyphony,polyphony); }

		u32 GetChannel() const { return m_Channel; }
		void SetChannel(const u32 channel) { m_Channel = Clamp<u32>(channel, 0, 16); }

		u32 GetNoteMask() const { return m_NoteMask; }
		void SetNoteMask(const u32 noteMask) { Assign(m_NoteMask, noteMask); }

	private:

		u8 AllocateVoiceId();

		void ProcessMessage(const audMidiMessage &message);

		struct synthNoteState
		{
			u8 velocity;
			u8 voiceId;
		};

		atRangeArray<synthNoteState,128> m_NoteState;
		atRangeArray<u32,kMidiInMaxPolyphony> m_VoiceTimer;
		atFixedBitSet<kMidiInMaxPolyphony> m_VoiceState;
		u32 m_Polyphony;
		u32 m_Channel;
		u8 m_NoteMask;
	};
}
#endif
#endif // SYNTH_MIDIIN_H


