// 
// audiosynth/sampleframe.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SAMPLEFRAME_H
#define SYNTH_SAMPLEFRAME_H

#include "audiohardware/framebuffercache.h"
#include "audiohardware/framebufferpool.h"
#include "audiohardware/mixer.h"
#include "vectormath/vectormath.h"

namespace rage
{
	
class synthSampleFrame
{
	public:
				
		synthSampleFrame(const bool shouldAllocateBuffer = true)
		{
			if(shouldAllocateBuffer)
			{
				m_BufferId = audFrameBufferPool::InvalidId;
				Allocate();
			}
			else
			{
				m_BufferId = audFrameBufferPool::InvalidId;
				m_OwnsBuffer = false;
			}

			// For now all sample frames are the same size, however this will change when the sample
			// rate is variable
			m_NumSamples = kMixBufNumSamples;
			Assert((m_NumSamples&15) == 0);
		}

		synthSampleFrame(const u32 bufferId)
			: m_NumSamples(kMixBufNumSamples)
			, m_BufferId((u16)bufferId)			
			, m_OwnsBuffer(false)
		{
			
		}

		~synthSampleFrame()
		{
			Free();
		}

		bool Allocate()
		{
			Assert(m_BufferId == audFrameBufferPool::InvalidId);
			Assert(g_FrameBufferPool);
			const u32 bufferId = g_FrameBufferPool->Allocate();
			if(bufferId == audFrameBufferPool::InvalidId)
			{
				audErrorf("Failed to allocate sample frame buffer");
				return false;
			}
			Assign(m_BufferId, bufferId);

			
			m_OwnsBuffer = true;

			return true;
		}

		void Free()
		{
			if(m_BufferId != audFrameBufferPool::InvalidId && m_OwnsBuffer)
			{
				g_FrameBufferPool->Free(m_BufferId);
				m_BufferId = audFrameBufferPool::InvalidId;
				m_OwnsBuffer = false;
			}
		}

		f32 *GetBuffer()
		{
			return g_FrameBufferCache->GetBuffer(m_BufferId);
		}

		const f32 *GetBuffer() const
		{
			return g_FrameBufferCache->GetBuffer(m_BufferId);
		}

		Vec::Vector_4V *GetBufferV()
		{
			return (Vec::Vector_4V*)g_FrameBufferCache->GetBuffer(m_BufferId);
		}

		const Vec::Vector_4V *GetBufferV() const
		{
			return (Vec::Vector_4V*)g_FrameBufferCache->GetBuffer(m_BufferId);
		}


		u32 GetSize() const
		{
			return m_NumSamples;
		}

		void SetBufferId(const u32 bufferId)
		{
			Free();
			Assign(m_BufferId, bufferId);
			m_OwnsBuffer = false;
		}

		bool HasBuffer() const { return m_BufferId != audFrameBufferPool::InvalidId; }
		u32 GetBufferId() const { return m_BufferId; }

	private:
		u16 m_NumSamples;
		u16 m_BufferId;
		bool m_OwnsBuffer;
	};
}

#endif // SYNTH_SAMPLEFRAME_H
