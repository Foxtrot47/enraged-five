
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include <math.h>
#include "math/amath.h"
#include "audiohardware/mixer.h"
#include "oscillator.h"
#include "oscillatorview.h"
#include "rescaler.h"
#include "synthutil.h"
#include "system/cache.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

#if __SYNTH_EDITOR
	char *synthDigitalOscillator::ModeStrings[NUM_OSCILLATOR_MODES] =
	{
		"SQUARE",
		"TRI",
		"SAW",
		"RAMP",
		"SINE",
		"COSINE",
	};
	const char *synthDigitalOscillator::GetModeName(const synthOscillatorMode mode) const
	{
		return ModeStrings[mode];
	}
#endif

	SYNTH_EDITOR_LINK_VIEW(synthDigitalOscillator, synthOscillatorView);

	synthDigitalOscillator::synthDigitalOscillator() 
	{
		m_Inputs[kDigitalOscillatorAmplitude].Init(this, "Amplitude",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDigitalOscillatorAmplitude].SetStaticValue(1.f);
		m_Inputs[kDigitalOscillatorFrequency].Init(this, "Frequency",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDigitalOscillatorFrequency].SetStaticValue(0.f);
		m_Inputs[kDigitalOscillatorPhase].Init(this, "Phase",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDigitalOscillatorPhase].SetStaticValue(0.f);
		m_Inputs[kDigitalOscillatorHardSync].Init(this, "HardSync", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kDigitalOscillatorHardSync].SetStaticValue(0.f);

		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_FrequencyRange[kMinFreq] = 0.f;
		m_FrequencyRange[kMaxFreq] = 20000.f;

		m_StaticOutput = false;
		m_Mode = SINE;

		ResetProcessing();
	}

	void synthDigitalOscillator::ResetProcessing()
	{
		m_Phase = V4VConstant(V_ZERO);
		m_StateVec = V4VConstant(V_ZERO);
	}

	synthDigitalOscillator::~synthDigitalOscillator()
	{

	}

	void synthDigitalOscillator::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("MinFreq", m_FrequencyRange[kMinFreq]);
		serializer->SerializeField("MaxFreq", m_FrequencyRange[kMaxFreq]);
		serializer->SerializeEnumField("Mode", m_Mode);
		if(m_Mode >= NUM_OSCILLATOR_MODES)
		{
			audWarningf("Invalid oscillator mode: %d", m_Mode);
			m_Mode = SINE;

		}
		serializer->SerializeField("StaticOutput", m_StaticOutput);
	}

	void synthDigitalOscillator::Synthesize()
	{	
		const bool isHardSynced = (m_Inputs[kDigitalOscillatorHardSync].GetNormalizedValue(0) >= 1.f);
		if(isHardSynced)
		{
			// Reset phase			
			m_StateVec = V4VConstant(V_ZERO);
		}

		// Always produce static output if driven by static phase
		if(m_StaticOutput || (m_Inputs[kDigitalOscillatorPhase].IsConnected() && m_Inputs[kDigitalOscillatorPhase].GetDataState() == synthPin::SYNTH_PIN_STATIC))
		{
			float phase = 0.0f;

			if(m_Inputs[kDigitalOscillatorPhase].IsConnected())
			{
				phase = m_Inputs[kDigitalOscillatorPhase].GetNormalizedValue(0);
			}
			else
			{
				float freq = Lerp(m_Inputs[kDigitalOscillatorFrequency].GetNormalizedValue(0), m_FrequencyRange[kMinFreq], m_FrequencyRange[kMaxFreq]);
				phase = GenerateRamp(freq, m_StateVec);

				// TODO: support phase offset
			}

			float output = 0.0f;
			switch(m_Mode)
			{
			case SINE:
				output = Sine(phase);
				break;
			case COSINE:
				output = Cosine(phase);
				break;
			case SQUARE:
				output = Square(phase);
				break;
			case TRIANGLE:
				output = Triangle(phase);
				break;
			case SAW:
				output = Saw(phase);
				break;
			case INVERTED_SAW:
				// Convert phase to signal
				output = (phase*2.f)-1.f;
				break;
			default:
				Assert(0);
			}

			output *= m_Inputs[kDigitalOscillatorAmplitude].GetNormalizedValue(0);
			m_Outputs[0].SetStaticValue(output);
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			// First, generate a ramp signal
			if(m_Inputs[kDigitalOscillatorPhase].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				Vector_4V *outPtr = m_Buffer.GetBufferV();
				if (outPtr)
				{
					for (u32 i = 0; i < kMixBufNumSamples; i += 4)
					{
						*outPtr++ = m_Inputs[kDigitalOscillatorPhase].GetNormalizedValueV(i);
					}
				}
			}
			else
			{
				if(m_Inputs[kDigitalOscillatorFrequency].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
				{
					// Frequency modulation version - first, populate buffer with normalized frequency
					Vector_4V *outPtr = m_Buffer.GetBufferV();
					if (outPtr)
					{
						for (u32 i = 0; i < kMixBufNumSamples; i += 4)
						{
							*outPtr++ = m_Inputs[kDigitalOscillatorFrequency].GetNormalizedValueV(i);
						}
					}

					if (m_Buffer.GetBuffer())
					{
						// Lerp into Hz range
						synthRescaler::Process(m_Buffer.GetBuffer(), m_FrequencyRange[kMinFreq], m_FrequencyRange[kMaxFreq], kMixBufNumSamples);
						// Turn into normalized ramp
						GenerateRamp(m_Buffer.GetBuffer(), m_StateVec, kMixBufNumSamples);
					}
				}
				else
				{
					if (m_Buffer.GetBuffer())
					{
						const float normalizedFreq = m_Inputs[kDigitalOscillatorFrequency].GetNormalizedValue(0);
						const float freq = Lerp(normalizedFreq, m_FrequencyRange[kMinFreq], m_FrequencyRange[kMaxFreq]);
						GenerateRamp(m_Buffer.GetBuffer(), V4LoadScalar32IntoSplatted(freq), m_StateVec, kMixBufNumSamples);
					}
				}
			}
			switch(m_Mode)
			{

			case SINE:
				if (m_Buffer.GetBuffer())
				{
					Sine(m_Buffer.GetBuffer(), kMixBufNumSamples);
				}
				break;
			case COSINE:
				if (m_Buffer.GetBuffer())
				{
					Cosine(m_Buffer.GetBuffer(), kMixBufNumSamples);
				}
				break;
			case SQUARE:
				if (m_Buffer.GetBuffer())
				{
					Square(m_Buffer.GetBuffer(), kMixBufNumSamples);
				}
				break;
			case TRIANGLE:
				if (m_Buffer.GetBuffer())
				{
					Triangle(m_Buffer.GetBuffer(), kMixBufNumSamples);
				}
				break;
			case SAW:
				if (m_Buffer.GetBuffer())
				{
					Saw(m_Buffer.GetBuffer(), kMixBufNumSamples);
				}
				break;
			case INVERTED_SAW:
				if (m_Buffer.GetBuffer())
				{
					synthUtil::ConvertBufferToSignal(m_Buffer.GetBuffer(), kMixBufNumSamples);
				}
				break;
			default:
				Assert(0);

			}

			// Apply gain
			Vector_4V *outPtr = m_Buffer.GetBufferV();
			if (outPtr)
			{
				for (u32 i = 0; i < kMixBufNumSamples; i += 4)
				{
					*outPtr = V4Scale(*outPtr, m_Inputs[kDigitalOscillatorAmplitude].GetNormalizedValueV(i));
					outPtr++;
				}
			}
		}
	}

	void synthDigitalOscillator::GenerateRamp(float *RESTRICT const inOutBuffer, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		if (inOutBuffer)
		{
			Vector_4V* inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
			CompileTimeAssert(kMixerNativeSampleRate == 48000);
			const Vector_4V nyquist = V4VConstant<0x46BB8000, 0x46BB8000, 0x46BB8000, 0x46BB8000>();
			// 1/48kHz
			const Vector_4V oneOverFs = V4VConstant<0x37AEC33E, 0x37AEC33E, 0x37AEC33E, 0x37AEC33E>();

			// State contains the next four phase values to use, without overflow correction
			Vector_4V phase = state;

			u32 count = numSamples >> 2;
			while (count--)
			{
				const Vector_4V freq = V4Clamp(*inOutPtr, V4VConstant(V_ZERO), nyquist);
				// Frequency Modulation version			

				const Vector_4V phaseStep = V4Scale(freq, oneOverFs);

				Vector_4V phase0 = V4SplatW(phase);
				Vector_4V phase1 = V4Add(phase0, V4SplatX(phaseStep));
				Vector_4V phase2 = V4Add(phase1, V4SplatY(phaseStep));
				Vector_4V phase3 = V4Add(phase2, V4SplatZ(phaseStep));

				Vector_4V phase01 = V4PermuteTwo<X1, X2, X1, X2>(phase0, phase1);
				Vector_4V phase23 = V4PermuteTwo<X1, X2, X1, X2>(phase2, phase3);
				phase = V4PermuteTwo<X1, Y1, X2, Y2>(phase01, phase23);

				// Keep phase in range [0,1] by subtracting floor(phase)
				phase = V4Subtract(phase, V4RoundToNearestIntZero(phase));
				*inOutPtr++ = phase;

				phase = V4Add(phase, V4SplatW(phaseStep));
			}
			state = V4SplatW(phase);
		}
	}

	void synthDigitalOscillator::GenerateRamp(float *RESTRICT const outputBuffer, const Vec::Vector_4V_In frequency, Vec::Vector_4V_InOut state, const u32 numSamples)
	{
		if (outputBuffer)
		{
			Vector_4V* outputPtr = reinterpret_cast<Vector_4V*>(outputBuffer);
			CompileTimeAssert(kMixerNativeSampleRate == 48000);
			// 1/48kHz
			const Vector_4V oneOverFs = V4VConstant<0x37AEC33E, 0x37AEC33E, 0x37AEC33E, 0x37AEC33E>();

			// (0,1,2,3)
			const Vector_4V zeroOneTwoThree = V4VConstant<0, 0x3F800000, 0x40000000, 0x40400000>();

			const Vector_4V singleSamplePhaseStep = V4Scale(frequency, oneOverFs);
			const Vector_4V phaseStep = V4Scale(zeroOneTwoThree, singleSamplePhaseStep);
			const Vector_4V phaseStepX4 = V4Scale(V4VConstant(V_FOUR), singleSamplePhaseStep);

			// State is splatted with the next phase value to use, without overflow correction
			Vector_4V phase = V4Add(phaseStep, state);

			u32 count = numSamples >> 2;
			while (count--)
			{
				// Keep phase in range [0,1] by subtracting floor(phase)
				phase = V4Subtract(phase, V4RoundToNearestIntZero(phase));
				*outputPtr++ = phase;
				phase = V4Add(phase, phaseStepX4);
			}
			state = V4SplatX(phase);
		}
	}

	float synthDigitalOscillator::GenerateRamp(const float frequency, Vec::Vector_4V_InOut state)
	{
		const float currentPhase = GetX(state);

		const f32 effectiveSampleRate = 1.f / (float(kMixBufNumSamples) / float(kMixerNativeSampleRate));
		// clamp the frequency to the effective sample rate to prevent phase becoming >= 2.f
		const float phaseStep = Min(1.f, frequency / effectiveSampleRate);
		const float newPhaseOverflow = currentPhase + phaseStep;
		// Since our maximum phase value is < 2.f due to the clamping above we can wrap with a simple subtraction
		const float newPhase = newPhaseOverflow >= 1.f ? newPhaseOverflow - 1.f : newPhaseOverflow;

		SetX(state, newPhase);
		return currentPhase;
	}

	void synthDigitalOscillator::Sine(float *RESTRICT const inOutBuffer, const u32 numSamples)
	{
		if (inOutBuffer)
		{
			Vector_4V* inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);

			const Vector_4V negTwoPi = V4VConstant<0xC0C90FDA, 0xC0C90FDA, 0xC0C90FDA, 0xC0C90FDA>();
			const Vector_4V pi = V4VConstant(V_PI);
			u32 count = numSamples >> 2;
			while (count--)
			{
				// V4SinFast wants phase in [-PI,PI]
				// (x*2PI) - PI
				// Actually that generates 180 degree phase shift; [PI,-PI] seems to be correct
				// x*-2PI + PI
				const Vector_4V scaledPhase = V4AddScaled(pi, negTwoPi, *inOutPtr);
				*inOutPtr++ = V4SinFast(V4Clamp(scaledPhase, V4VConstant(V_NEG_PI), V4VConstant(V_PI)));
			}
		}
	}

	float synthDigitalOscillator::Sine(const float inputPhase)
	{
		return Sinf(inputPhase * 2.f * PI);
	}

	void synthDigitalOscillator::Cosine(float *RESTRICT const inOutBuffer, const u32 numSamples)
	{
		if (inOutBuffer)
		{
			const Vector_4V negTwoPi = V4VConstant<0xC0C90FDA, 0xC0C90FDA, 0xC0C90FDA, 0xC0C90FDA>();
			const Vector_4V pi = V4VConstant(V_PI);

			Vector_4V* inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
			u32 count = numSamples >> 2;
			while (count--)
			{
				// V4CosFast wants phase in [-PI,PI]
				// (x*2PI) - PI
				// Actually that generates 180 degree phase shift; [PI,-PI] seems to be correct
				// x*-2PI + PI
				const Vector_4V scaledPhase = V4AddScaled(pi, negTwoPi, *inOutPtr);
				*inOutPtr++ = V4CosFast(V4Clamp(scaledPhase, V4VConstant(V_NEG_PI), V4VConstant(V_PI)));
			}
		}
	}

	float synthDigitalOscillator::Cosine(const float inputPhase)
	{
		return Cosf(inputPhase * 2.f * PI);
	}

	void synthDigitalOscillator::Triangle(float *RESTRICT const inOutBuffer, const u32 numSamples)
	{
		if (inOutBuffer)
		{
			Vector_4V* inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
			u32 count = numSamples >> 2;
			const Vector_4V one = V4VConstant(V_ONE);
			const Vector_4V negTwo = V4VConstant(V_NEGTWO);
			//const Vector_4V negOne = V4VConstant(V_NEGONE);
			while (count--)
			{
				*inOutPtr = V4AddScaled(one, negTwo, V4Abs(V4AddScaled(one, *inOutPtr, negTwo)));
				inOutPtr++;
			}
		}
	}

	float synthDigitalOscillator::Triangle(const float inputPhase)
	{
		return 1.f + -2.f * Abs(inputPhase * -2.f + 1.f);
	}

	void synthDigitalOscillator::Square(float *RESTRICT const inOutBuffer, const u32 numSamples)
	{
		if (inOutBuffer)
		{
			Vector_4V* inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
			u32 count = numSamples >> 2;
			while (count--)
			{
				*inOutPtr = Vec::V4SelectFT(Vec::V4IsGreaterThanOrEqualV(*inOutPtr, Vec::V4VConstant(V_HALF)), V4VConstant(V_ONE), Vec::V4VConstant(V_NEGONE));
				inOutPtr++;
			}
		}
	}

	float synthDigitalOscillator::Square(const float inputPhase)
	{
		return inputPhase < 0.5f ? -1.f : 1.f;
	}

	void synthDigitalOscillator::Saw(float *RESTRICT const inOutBuffer, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		u32 count = numSamples>>2;
		while(count--)
		{
			*inOutPtr = V4Scale(V4Subtract(V4Scale(*inOutPtr,  V4VConstant(V_TWO)), V4VConstant(V_ONE)), V4VConstant(V_NEGONE));
			inOutPtr++;
		}		
	}

	float synthDigitalOscillator::Saw(const float inputPhase)
	{
		return -1.f * ((inputPhase*2.f) - 1.f);
	}
}
#endif
