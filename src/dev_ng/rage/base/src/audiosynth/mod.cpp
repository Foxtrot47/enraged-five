
#include "mod.h"

#if !SYNTH_MINIMAL_MODULES

#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	synthModulus::synthModulus()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Inputs[INPUT].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[INPUT].SetStaticValue(0.f);
		m_Inputs[MODULO].Init(this, "Modulo", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[MODULO].SetStaticValue(1.f);

		m_Outputs[0].SetStaticValue(0.f);
	}

	void synthModulus::Synthesize()
	{	
		if(m_Inputs[INPUT].GetDataState() == synthPin::SYNTH_PIN_STATIC &&
			m_Inputs[MODULO].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			const f32 input = m_Inputs[INPUT].GetStaticValue();
			const f32 modulo = m_Inputs[MODULO].GetStaticValue();
			
			m_Outputs[0].SetStaticValue(Process(input, modulo));
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);
		
			Vector_4V *RESTRICT outputBuf = (Vector_4V*)m_Outputs[0].GetDataBuffer()->GetBuffer();

			for(u32 i = 0; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
			{
				const Vector_4V input = m_Inputs[INPUT].GetNormalizedValueV(i);
				const Vector_4V modulo = V4Max(V4VConstant(V_ONE), m_Inputs[MODULO].GetNormalizedValueV(i));
				const Vector_4V divided = V4InvScale(input, modulo);

				const Vector_4V dividedRounded = V4RoundToNearestIntNegInf(divided);

				const Vector_4V remainder = V4Subtract(divided, dividedRounded);
				const Vector_4V output = V4Scale(modulo,remainder);
				*outputBuf++ = output;
			}// for each sample
		}
	}

	void synthModulus::Process(float *RESTRICT const inOutBuffer, const float *const moduloBuffer, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		const Vector_4V *moduloPtr = reinterpret_cast<const Vector_4V*>(moduloBuffer);
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V input = *inOutPtr;
			const Vector_4V modulo = V4Max(V4VConstant(V_ONE), *moduloPtr++);
			const Vector_4V divided = V4InvScale(input, modulo);
			
			const Vector_4V dividedRounded = V4RoundToNearestIntNegInf(divided);

			const Vector_4V remainder = V4Subtract(divided, dividedRounded);
			*inOutPtr++ = V4Scale(modulo,remainder);				
		}
	}

	void synthModulus::Process(float *RESTRICT const inOutBuffer, Vector_4V_In modulo, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);
		
		u32 count = numSamples>>2;
		while(count--)
		{
			const Vector_4V input = *inOutPtr;
			
			const Vector_4V divided = V4InvScale(input, modulo);
		
			const Vector_4V dividedRounded = V4RoundToNearestIntNegInf(divided);

			const Vector_4V remainder = V4Subtract(divided, dividedRounded);
			*inOutPtr++ = V4Scale(modulo,remainder);				
		}
	}

	float synthModulus::Process(const float input, const float modulo)
	{	
		const f32 divided = input / Max(1.f, modulo);
		const f32 remainder = divided - floorf(divided);
		return modulo * remainder;
	}
}
#endif
