
#ifndef SYNTH_DATTORRO_H
#define SYNTH_DATTORRO_H

#include "blocks.h"
#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	enum synthDattorroInputs
	{
		kDattorroSignal = 0,
		kDattorroDamping,
		kDattorroBandwidth,
		kDattorroDecay,
		kDattorroDecayDiffusion,
		kDattorroInputDiffusion1,
		kDattorroInputDiffusion2,

		kDattorroInputExcursion1,
		kDattorroInputExcursion2,

		kDattorroNumInputs
	};
	
	class revModelDattorro
	{

	public:

		float Damping;
		float Bandwidth;
		float InputDiffusion1;
		float InputDiffusion2;
		float DecayDiffusion1;

		float Excursion1;
		float Excursion2;
		
		float Decay;

		revModelDattorro()
		{
			Damping = 0.0005f;
			Bandwidth = 0.9995f;
			Decay = 0.5f;
			DecayDiffusion1 = 0.7f;
			
			InputDiffusion1 = 0.75f;
			InputDiffusion2 = 0.625f;

			Excursion1 = 0.f;
			Excursion2 = 0.f;
		}

		float Process(const float input);

	private:

		revAllPassFig8A<142> ap_13_14;
		revAllPassFig8A<107> ap_19_20;
		revAllPassFig8A<379> ap_15_16;
		revAllPassFig8A<2777> ap_21_22;

		revAllPassFig8Mod<672 + 16> ap_23_24;
		revAllPassFig8Mod<908 + 16> ap_46_48;

		revAllPassFig8A<1800> ap_31_33;
		revAllPassFig8A<2656> ap_55_59;

		revSimpleDelay<4453> d_30;
		revSimpleDelay<3720> d_39;
		revSimpleDelay<4217> d_54;
		revSimpleDelay<3163> d_63;

		revOnePole f_54_55;
		revOnePole f_30_31;
		revOnePole f_bw;

	};

	class revModelDattorro_V4
	{
	public:

		bool Init()
		{
			REV_CHECK_RET(ap_13_14.SetLength(142));
			REV_CHECK_RET(ap_19_20.SetLength(107));
			REV_CHECK_RET(ap_15_16.SetLength(379));
			REV_CHECK_RET(ap_21_22.SetLength(2777));

			REV_CHECK_RET(ap_23_24.SetLength(672 + 16));
			REV_CHECK_RET(ap_46_48.SetLength(908 + 16));
			
			REV_CHECK_RET(ap_31_33.SetLength(1800));
			REV_CHECK_RET(ap_55_59.SetLength(2656));

			REV_CHECK_RET(d_30.SetLength(4453));
			REV_CHECK_RET(d_39.SetLength(3720));
			REV_CHECK_RET(d_54.SetLength(4217));
			REV_CHECK_RET(d_63.SetLength(3163));

			return true;
		}

		void SetDamping(Vec::Vector_4V damping)
		{
			f_30_31.SetDamping(damping);
			f_54_55.SetDamping(damping);			
		}
		
		void SetBandwidth(Vec::Vector_4V_In bandwidth)
		{
			f_bw.SetDamping(Vec::V4Subtract(Vec::V4VConstant(V_ONE), bandwidth));
		}

		void SetInputDiffusion1(Vec::Vector_4V_In diffusion)
		{
			ap_13_14.SetDiffusion(diffusion);
			ap_19_20.SetDiffusion(diffusion);
		}

		void SetInputDiffusion2(Vec::Vector_4V_In diffusion)
		{
			ap_15_16.SetDiffusion(diffusion);
			ap_21_22.SetDiffusion(diffusion);
		}

		void SetDecayDiffusion1(Vec::Vector_4V_In diffusion)
		{
			ap_23_24.SetDecay(diffusion);
			ap_23_24.SetDiffusion(diffusion);
			ap_46_48.SetDecay(diffusion);
			ap_46_48.SetDiffusion(diffusion);
		}

		void SetDecay(Vec::Vector_4V_In decay)
		{
			m_Decay = decay;
			// decayOffset = 0.15f
			Vec::Vector_4V decayOffset = Vec::V4VConstantSplat<0x3E19999A>();
			Vec::Vector_4V DecayDiffusion2 = Vec::V4Clamp(Vec::V4Add(decay, decayOffset), Vec::V4VConstant(V_QUARTER), Vec::V4VConstant(V_HALF));
			ap_31_33.SetDiffusion(DecayDiffusion2);
			ap_55_59.SetDiffusion(DecayDiffusion2);
		}

		void SetExcursion1(const float excursion)
		{
			m_Excursion1 = excursion;
		}

		void SetExcursion2(const float excursion)
		{
			m_Excursion2 = excursion;
		}
		
		Vec::Vector_4V_Out Process(Vec::Vector_4V_In input);

	private:

		Vec::Vector_4V m_Decay;

		revAllPassFig8A_V4 ap_13_14;
		revAllPassFig8A_V4 ap_19_20;
		revAllPassFig8A_V4 ap_15_16;
		revAllPassFig8A_V4 ap_21_22;

		revAllPassFig8Mod_V4 ap_23_24;
		revAllPassFig8Mod_V4 ap_46_48;

		revAllPassFig8A_V4 ap_31_33;
		revAllPassFig8A_V4 ap_55_59;

		revSimpleDelay_V4 d_30;
		revSimpleDelay_V4 d_39;
		revSimpleDelay_V4 d_54;
		revSimpleDelay_V4 d_63;

		revOnePole_V4 f_54_55;
		revOnePole_V4 f_30_31;
		revOnePole_V4 f_bw;

		float m_Excursion1;
		float m_Excursion2;
	};

	class synthDattorroReverb : public synthModuleBase<kDattorroNumInputs,2, SYNTH_DATTORROREVERB>
	{
	public:

		synthDattorroReverb();
		virtual ~synthDattorroReverb();


		virtual void Synthesize();

		virtual const char *GetName() const
		{
			return "Dattorro Reverb";
		}


	private:

		synthSampleFrame m_Buffer1;
		synthSampleFrame m_Buffer2;
		revModelDattorro m_Reverb1;
		revModelDattorro_V4 m_Reverb2;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_DATTORRO_H


