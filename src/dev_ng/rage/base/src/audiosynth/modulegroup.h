// 
// audiosynth/modulegroup.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_MODULEGROUP_H
#define SYNTH_MODULEGROUP_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "atl/array.h"
#include "vector/matrix34.h"
#include "numericinputbox.h"
#include "uicomponent.h"
#include "uilabel.h"
#include "uiview.h"

namespace rage
{
	class synthModuleView;

class synthUIViewGroup : public synthUIView
{
public:

	synthUIViewGroup();
	~synthUIViewGroup();

	void Add(synthUIView *module);
	void Remove(synthUIView *module);
	bool Contains(synthUIView *module) const;

	const char *GetName() const { return m_NameBuf; }
	void SetName(const char *name);

	virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;
	virtual void Update();
	virtual void Render();

	const atArray<synthUIView *> &GetContents() const
	{
		return m_Contents;
	}
	virtual bool StartDrag() const;
	virtual bool IsActive() const { return false; }

	bool ShouldBeDeleted() const { return m_ShouldBeDeleted; }

	const Vector3 &GetViewOffset(const u32 i) { return m_ModuleOffsets[i]; }
	void SetViewOffset(const u32 i, const Vector3 &offset) { m_ModuleOffsets[i] = offset; }

	void SetColour(const u8 r, const u8 g, const u8 b);
	void SetColour(const Color32 colour) { m_Colour = colour; }
	const Color32 &GetColour() const { return m_Colour; }

	virtual bool IsGroup() const { return true; }

	void SetTransparent(const bool transparent)
	{
		m_IsTransparent = transparent;
	}

    bool IsEmpty() const { return m_Contents.GetCount() == 0; }

    float GetProcessingTime() const;

    virtual void DeleteContents();
	virtual synthUIView *DuplicateContents(const bool recreateConnections);

	s32 GetId() const;

	void FindModules(atArray<synthModuleView*> &moduleList);
	void FindGroups(atArray<synthUIViewGroup*> &groupList);

private:

	synthUIViewGroup *Clone() const;

	void DrawGroupBox(const Vector3 &v1, const Vector3 &v2);

	synthUITextBoxInput m_InputBox;
	enum {kMaxNameLength=128};
	char m_NameBuf[kMaxNameLength];
	char m_CPUTimeBuf[32];
	synthUILabel m_TitleLabel;
	synthUILabel m_CPUTimeLabel;
	atArray<synthUIView *> m_Contents;
	atArray<Vector3> m_ModuleOffsets;
	Color32 m_Colour;
	bool m_ShouldBeDeleted;
	bool m_ModuleUnderMouse;
	bool m_IsTransparent;

	static u32 sm_NextGroupNum;
};

}//namespace rage
#endif // __SYNTH_EDITOR
#endif // SYNTH_MODULEGROUP_H
