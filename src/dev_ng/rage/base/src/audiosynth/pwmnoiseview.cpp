// 
// audiosynth/pwnoiseview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "pwmnoiseview.h"
#include "pwmnoise.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"

#include "input/mouse.h"

namespace rage
{

	synthPwmNoiseView::synthPwmNoiseView(synthModule *module) : synthModuleView(module)
	{
		AddComponent(&m_WidthRange);
		
		const f32 minUIWidth = 1.f;
		const f32 maxUIWidth = 256.f;
		
		m_WidthRange.SetMinRange(minUIWidth, maxUIWidth);
		m_WidthRange.SetMaxRange(minUIWidth, maxUIWidth);

		const f32 currentMinWidth = (GetNoiseModule()->GetMinPulseWidth()-minUIWidth) / (maxUIWidth-minUIWidth);
		const f32 currentMaxWidth = (GetNoiseModule()->GetMaxPulseWidth()-minUIWidth) / (maxUIWidth-minUIWidth);
		m_WidthRange.SetNormalizedValues(currentMinWidth, currentMaxWidth);
	}

	void synthPwmNoiseView::Update()
	{
		// fade out title when editing
		SetTitleAlpha(1.f - GetHoverFade());
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();

		m_WidthRange.SetMatrix(mtx);
		m_WidthRange.SetAlpha(GetHoverFade() * GetAlpha());
		m_WidthRange.SetTextAlpha(GetHoverFade());
		m_WidthRange.SetTitles("MinWidth", "MaxWidth");
		m_WidthRange.SetWidth(0.4f);
		m_WidthRange.SetShouldDraw(ShouldDraw());

		GetNoiseModule()->SetMaxPulseWidth((s32)m_WidthRange.GetMaxValue());
		GetNoiseModule()->SetMinPulseWidth((s32)m_WidthRange.GetMinValue());
		GetNoiseModule()->GetInput(kPwmNoisePulseWidth).GetView()->SetStaticViewRange((f32)GetNoiseModule()->GetMinPulseWidth(), (f32)GetNoiseModule()->GetMaxPulseWidth());

		synthModuleView::Update();
	}

	void synthPwmNoiseView::Render()
	{
		synthModuleView::Render();
	}

	bool synthPwmNoiseView::StartDrag() const
	{
		if(m_WidthRange.IsUnderMouse())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthPwmNoiseView::IsActive() const
	{
		return (IsUnderMouse() || m_WidthRange.IsActive());
	}
}
#endif // __SYNTH_EDITOR

