// 
// audiosynth/oscillatorview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "audiohardware/driverutil.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "profile/profiler.h"

#include "biquadfilterview.h"
#include "biquadfilter.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"
#include "uiplane.h"
#include "audiohardware/mixer.h"

namespace rage
{
	EXT_PF_GROUP(ModuleViewUpdate);
	PF_TIMER(BiquadFilterUpdate, ModuleViewUpdate);

	synthBiquadFilterView::synthBiquadFilterView(synthModule *module) : synthModuleView(module)
	{
		m_Texture = NULL;
		AddComponent(&m_ModeList);

		for(u32 i = 0; i < GetFilterModule()->GetNumModes(); i++)
		{
			m_ModeList.AddItem(GetFilterModule()->GetModeName((synthBiquadFilter::synthBiquadFilterModes)i));
		}
		m_ModeList.SetCurrentIndex((u32)GetFilterModule()->GetMode());
		AddComponent(&m_FreqRange);

		m_FreqRange.SetMinRange(0.f, 20000.f);
		m_FreqRange.SetMaxRange(0.f, 20000.f);
		m_FreqRange.SetNormalizedValues(GetFilterModule()->GetMinFrequency()/20000.f, GetFilterModule()->GetMaxFrequency()/20000.f);
	}

	void synthBiquadFilterView::Update()
	{
		PF_FUNC(BiquadFilterUpdate);
		// fade out title when editing
		SetTitleAlpha(IsMinimised() ? 1.f : 1.f - GetHoverFade());
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();

		// Mode label
		const f32 moduleHalfHeight = 0.5f * ComputeHeight();
		const f32 modeHeight = 0.25f;//m_ModeList.ComputeHeight();
		mtx.d += (mtx.b * modeHeight * 2.5f);
		
		m_ModeList.SetMatrix(mtx);
		m_ModeList.SetAlpha(GetHoverFade());
		
		GetFilterModule()->SetMode((synthBiquadFilter::synthBiquadFilterModes)m_ModeList.GetCurrentIndex());
			
		const bool isMenuDisplayed = m_ModeList.IsShowingMenu();

		m_ModeList.SetShouldDraw(!IsMinimised() && ShouldDraw());
		m_FreqRange.SetShouldDraw(!IsMinimised() && ShouldDraw() && !isMenuDisplayed);
		m_TitleLabel.SetShouldDraw(ShouldDraw() && !isMenuDisplayed);

		mtx = GetMatrix();
		const Vector3 yOffset = mtx.b * moduleHalfHeight * 0.25f;
		mtx.d -= yOffset;
		m_FreqRange.SetMatrix(mtx);
		m_FreqRange.SetAlpha(GetHoverFade()*GetAlpha());
		m_FreqRange.SetTextAlpha(GetHoverFade()*GetAlpha());
		m_FreqRange.SetTitles("MinCutoff", "MaxCutoff");
		m_FreqRange.SetWidth(0.4f);

		GetFilterModule()->SetMaxFrequency(m_FreqRange.GetMaxValue());
		GetFilterModule()->SetMinFrequency(m_FreqRange.GetMinValue());
		GetFilterModule()->GetInput(kBiquadFilterFc).GetView()->SetStaticViewRange(GetFilterModule()->GetMinFrequency(), GetFilterModule()->GetMaxFrequency());

		synthModuleView::Update();

		// graph the frequency response of the filter
		const u32 numSamplesToRender = 256;
		if(!m_Texture)
		{
			m_Texture = synthUIComponent::CreateTexture(numSamplesToRender, 1, grcImage::R32F);
		}			

		f32 a0,a1,a2,b1,b2;
		const f32 b0 = 1.f;
		GetFilterModule()->GetCoefficients(a0,a1,a2,b1,b2);

		grcTextureLock lock;

		m_Texture->LockRect(0,0,lock, grcsWrite);
		const f32 freqStep = PI/(f32)numSamplesToRender;
		f32 w = 0.f;
		
		f32 *textPtrF32 = (f32*)lock.Base;
		for(u32 i = 0; i < numSamplesToRender; i++)
		{
			const f32 phi = square(rage::Sinf(w*0.5f));
			const f32 dbMag = 0.f - (10.f*log10(square(b0+b1+b2) - 4.f*(b0*b1 + 4.f*b0*b2 + b1*b2)*phi + 16.f*b0*b2*square(phi))
				-10.f*log10(square(a0+a1+a2) - 4.f*(a0*a1 + 4.f*a0*a2 + a1*a2)*phi + 16.f*a0*a2*square(phi)));

			f32 linMag = audDriverUtil::ComputeLinearVolumeFromDb(dbMag);
			if(GetFilterModule()->Is4Pole())
			{
				linMag *= linMag;
			}
			*textPtrF32 = 4.f * linMag;
				

			if(!FPIsFinite(*textPtrF32))
			{
				*textPtrF32 = 0.f;
			}
			
			w += freqStep;
			textPtrF32++;
		}

		m_Texture->UnlockRect(lock);		
	}

	void synthBiquadFilterView::Render()
	{
		synthModuleView::Render();

		Matrix34 mtx = GetMatrix();
		if(!IsMinimised())
		{
			mtx.d += mtx.b * 3.f;
		}

		if(!m_ModeList.IsShowingMenu())
		{
			Vector3 scalingVec(2.f,0.9f,1.f);
			mtx.Transform3x3(scalingVec);
			mtx.Scale(scalingVec);
			grcWorldMtx(mtx);

			SetShaderVar("FontTex",m_Texture);
			SetShaderVar("DiffuseTex",m_Texture);
			SetShaderVar("g_WaveXZoom", 1.f);
			SetShaderVar("g_WaveYZoom", 1.f);
			SetShaderVar("g_WaveXOffset", 0.f);
			SetShaderVar("g_GhostLevel", 0.f);
			SetShaderVar("g_AlphaFade", 1.f);
			
			DrawVB("drawfr");
		}
	}

	bool synthBiquadFilterView::StartDrag() const
	{
		if(m_FreqRange.IsUnderMouse() || m_ModeList.IsShowingMenu() || m_ModeList.IsUnderMouse())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthBiquadFilterView::IsActive() const
	{
		return (IsUnderMouse() || m_FreqRange.IsActive());
	}
}
#endif // __SYNTH_EDITOR

