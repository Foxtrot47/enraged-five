// 
// audiosynth/triggerview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_TRIGGERVIEW_H
#define SYNTH_TRIGGERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthTrigger;
	class synthTriggerView : public synthModuleView
	{
	public:

		synthTriggerView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();

		synthTrigger *GetTrigger()
		{
			return (synthTrigger*)m_Module;
		}
	private:
		synthUIDropDownList m_ModeSwitch;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_TRIGGERVIEW_H

