// 
// audiosynth/switchview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_SWITCHVIEW_H
#define SYNTH_SWITCHVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uilabel.h"
#include "uiscrollbar.h"
#include "uidropdownlist.h"

namespace rage
{

	class synthSwitch;
	class synthSwitchView : public synthModuleView
	{
	public:

		synthSwitchView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();
		virtual u32 GetModuleHeight() const;

	private:

		synthSwitch *GetSwitch() const {return (synthSwitch*)m_Module; }

		synthUIScrollbar m_Divisions;
		synthUIDropDownList m_ModeSwitch;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_SWITCHVIEW_H

