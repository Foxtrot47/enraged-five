// 
// audiosynth/uiplane.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UIPLANE_H
#define SYNTH_UIPLANE_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uiview.h"
#include "uilabel.h"
#include "uimenuview.h"
#include "numericinputbox.h"
#include "synthesizer.h"

namespace rage
{
	class synthPin;
	class synthCommentBox;
	class synthUIViewGroup;

	// PURPOSE
	//	UI container class that manages multiple views on a single 2d plane.
	//	Responsible for culling and user input processing
	class synthUIPlane : public synthUIComponent
	{
	public:

		synthUIPlane();
		virtual ~synthUIPlane();

		void Init(atArray<synthModule*> *modules, synthSynthesizer *synth);
		void Shutdown();

		// PURPOSE
		//	Sets whether this is the active plane for user interaction
		void SetActive(const bool is)
		{
			m_IsActive = is;
		}

		bool IsActive() const
		{
			return (m_IsActive);
		}

		void SetDisableInteraction(const bool disable)
		{
			m_IsInteractionDisabled = disable;
		}
		bool IsInteractionDisabled() const { return m_IsInteractionDisabled; }

		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		void DeleteModule(synthModule *module);

		bool StartConnection(synthPin *p0);
		bool EndConnection(synthPin *p1);
		void CancelConnection();
		bool IsConnecting() const{return m_ConnectionP0!=NULL;}
		const synthPin *GetConnectingStartPin() const { return m_ConnectionP0; }
		void Disconnect(synthPin *p0);

		static synthUIPlane* GetMaster() { return sm_MasterPlane; }
		static synthUIPlane* GetActive() { return GetMaster(); }
		static void SetMaster(synthUIPlane *plane) { sm_MasterPlane = plane; }

		const Vector3 &GetMousePosition()
		{
			return m_MousePosition;
		}

		void SetMousePosition(const Vector3 &mousePos)
		{
			m_MousePosition = mousePos;
		}

		bool HadLeftClick() const { return m_HadLeftClick; }
		bool HadLeftDoubleClick() const { return m_HadLeftDoubleClick; }
		bool HadRightClick() const { return m_HadRightClick; }

		bool IsAuditioning() const{ return (m_AuditioningPin!=NULL); }
		synthPin *GetAuditioningPin() const { return m_AuditioningPin; }
		void SetAuditioningPin(synthPin *pin) { m_AuditioningPin = pin; }

		void DuplicateModule(synthModule *module);

		synthModule *CreateJunctionFromOutput(synthPin *outputPin);
		synthModule *CreateJunctionFromInput(synthPin *inputPin);
		void CreateJunctionAtMouse();

		void DuplicateGroup(synthUIViewGroup *moduleGroup);
		void AddGroup(synthUIViewGroup *moduleGroup)
		{
			m_Groups.PushAndGrow(moduleGroup);
		}

		u32 GetNumGroups() const
		{
			return m_Groups.GetCount();
		}

		const synthUIViewGroup *GetGroup(const u32 index) const
		{
			return m_Groups[index];
		}

		synthUIViewGroup *GetGroup(const u32 index)
		{
			return m_Groups[index];
		}

		s32 GetGroupId(const synthUIViewGroup *group)
		{
			return m_Groups.Find(const_cast<synthUIViewGroup*>(group));
		}

		u32 GetNumCommentBoxes() const
		{
			return m_CommentBoxes.GetCount();
		}

		const synthCommentBox *GetCommentBox(const u32 index)
		{
			return m_CommentBoxes[index];
		}

		void AddCommentBox(synthCommentBox *commentBox)
		{
			m_CommentBoxes.PushAndGrow(commentBox);
		}

		synthSynthesizer *GetSynthesizer()
		{
			return m_Synthesizer;
		}

		Matrix34 ComputeCameraMatrix();

		bool IsMouseFixed() const { return m_IsMouseFixed; }
		void SetIsMouseFixed(const bool is);

		void ResetMouse();

		bool IsGroupKeyDown() const;
		bool IsDuplicateKeyDown() const;
		bool IsCapsLockOn() const;

		const synthUIView *GetDraggingView() const { return m_DraggingView; }

		static bool IsKeyboardLocked();
		static void SetIsKeyboardLocked(const bool is) { sm_IsKeyboardLocked = is; }

	protected:
		virtual void Render();

	private:

		void ImportGroup(const u32 groupIndex);
		void ImportPatch();
		void ImportObjects(synthSynthesizer *tempInstance, synthUIViewGroup *parentGroup, atArray<synthUIViewGroup *> &groups, atArray<synthCommentBox*> &commentBoxes, atArray<synthModule*> &modules);

		bool IsMouseNearModule() const;
		synthUIViewGroup *FindGroupByPosition(const Vector3 &pos) const;

		void ComputeMouseCollision(const Vector3 &collisionPoint, synthUIView *view, synthUIView *&viewUnderMouse);

		void DuplicateView(synthUIView *view);

		void CreateCompressorMacro();
		synthUIViewGroup *CreateGroup(const char *groupName);
		synthUIViewGroup *CreateConstantFromInput(synthPin &inputPin, const Matrix34 &mat);

		synthUIMenuView m_ContextMenu;
		synthUIMenuView m_GenerateSubMenu;
		synthUIMenuView m_TransformSubMenu;
		synthUIMenuView m_FilterSubMenu;
		synthUIMenuView m_MathSubMenu;
		synthUIMenuView m_DebugSubMenu;
		synthUIMenuView m_RoutingSubMenu;
		synthUIMenuView m_IOSubMenu;
		synthUIMenuView m_ReverbMenu;
		synthUIMenuView m_LoadSaveMenu;

		enum InputBoxState
		{
			InputBox_Idle = 0,
			InputBox_LoadPatch_ModuleName,
			InputBox_ImportGroup_ModuleName,
			InputBox_ImportGroup_GroupName,
			InputBox_ImportPatch_ModuleName,
			InputBox_ApplyPreset_PresetName,
			InputBox_SavePreset_PresetName,
			InputBox_SaveCopy_PatchName,
			InputBox_SaveAs_PatchName,
			InputBox_NewPatch_PatchName,
		};
		synthUITextBoxInput m_InputBox;
		InputBoxState m_InputBoxState;
		synthUIMenuView m_ImportGroupListMenu;

		char m_ImportGroupPatchName[128];
		const ModularSynth *m_ImportGroup_Patch;

		atArray<synthModule*> m_ModulesToRemove;
		atArray<synthModule*> m_ModulesToAdd;
		atArray<synthUIViewGroup*> m_Groups;
		atArray<synthCommentBox*> m_CommentBoxes;

		synthUIView *m_DraggingView;
		Vector3 m_DragOffset;
		Vector3 m_MousePosition;
		Vector3 m_CreationPosition;
		synthPin *m_ConnectionP0;

		atArray<synthModule*> *m_Modules;
		synthSynthesizer *m_Synthesizer;

		synthPin *m_AuditioningPin;

		synthUIViewGroup *m_CurrentlyBuildingGroup;
		synthUIViewGroup *m_ActiveGroupForModuleCreation;

		s32 m_MouseFixX;
		s32 m_MouseFixY;
		u32 m_LastLeftClickTime;
		s32 m_MouseLeftClickX,m_MouseLeftClickY;
		bool m_HadLeftClick;
		bool m_HadRightClick;
		bool m_HadLeftDoubleClick;
		bool m_IsActive;
		bool m_IsMouseFixed;
		bool m_IsInteractionDisabled;
		bool m_DisableDragGroupRemoval;
		static synthUIPlane *sm_MasterPlane;
		static bool sm_IsKeyboardLocked;
	};
}
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIPLANE_H
