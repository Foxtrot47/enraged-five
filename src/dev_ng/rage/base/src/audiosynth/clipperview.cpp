// 
// audiosynth/clipperview.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "clipper.h"
#include "clipperview.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

namespace rage {

	synthClipperView::synthClipperView(synthModule *module) : synthModuleView(module)
	{
		m_Texture = NULL;

		m_Mode.AddItem("Hard clip");
		m_Mode.AddItem("Soft clip");
		AddComponent(&m_Mode);

		m_Mode.SetCurrentIndex(GetClipper()->GetMode());
	}

	synthClipperView::~synthClipperView()
	{
		if(m_Texture)
		{
			m_Texture->Release();
			m_Texture = NULL;
		}
	}

	void synthClipperView::Update()
	{
		GetClipper()->SetMode((synthClipper::synthClipperMode)m_Mode.GetCurrentIndex());
		Matrix34 mat = GetMatrix();
		Vector3 offset = -mat.b;
		mat.d += offset;

		m_Mode.SetMatrix(mat);
		m_Mode.SetAlpha(GetHoverFade());

		// Draw response curve
		const bool needDraw = true;
		if(needDraw || m_Texture == NULL)
		{
			const u32 numSamplesToRender = 256;
			if(!m_Texture)
			{
				m_Texture = synthUIComponent::CreateTexture(numSamplesToRender, 1, grcImage::R32F);			
			}			

			grcTextureLock lock;

			m_Texture->LockRect(0,0,lock,grcsWrite);
			f32 *textPtrF32 = (f32*)lock.Base;

			// rasterize the evaluated curve
			f32 val = 0.f;
			const f32 timeStep = 1.f / (f32)numSamplesToRender;
			
			
			for(s32 x = 0; x < numSamplesToRender; x++)
			{
				textPtrF32[x] = GetClipper()->ComputeValue(val);
				val += timeStep;
			}
			m_Texture->UnlockRect(lock);
		}

		synthModuleView::Update();
		m_TitleLabel.SetShouldDraw(!m_Mode.IsShowingMenu());
	}

	bool synthClipperView::StartDrag() const
	{
		if(m_Mode.IsUnderMouse() || m_Mode.IsShowingMenu())
		{
			return false;
		}
		return synthModuleView::StartDrag();
	}

	void synthClipperView::Render()
	{	
		synthModuleView::Render();

		Matrix34 mtx = GetMatrix();
		if(!IsMinimised())
		{
			mtx.d += mtx.b * 1.5f;
		}

		Vector3 scalingVec(2.f,0.9f,1.f);
		mtx.Transform3x3(scalingVec);
		mtx.Scale(scalingVec);
		grcWorldMtx(mtx);

		SetShaderVar("FontTex",m_Texture);
		SetShaderVar("DiffuseTex",m_Texture);
		SetShaderVar("g_WaveXZoom", 1.f);
		SetShaderVar("g_WaveYZoom", 1.f);
		SetShaderVar("g_WaveXOffset", 0.f);
		SetShaderVar("g_GhostLevel", 0.f);
		SetShaderVar("g_TextColour", Color32(0.f,0.4f,0.0f,1.f));
		SetShaderVar("g_AlphaFade", 1.f/*GetAlpha()*/);
		DrawVB("drawwave");		
	}

} // namespace rage
#endif // __SYNTH_EDITOR
