// 
// audiosynth/egview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "audiohardware/driverutil.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"

#include "envelopegenerator.h"
#include "egview.h"
#include "moduleview.h"
#include "pinview.h"
#include "uiplane.h"

#include "module.h"
#include "pin.h"

#include "profile/profiler.h"

namespace rage
{
EXT_PF_GROUP(ModuleViewUpdate);
PF_TIMER(EnvelopeGeneratorUpdate, ModuleViewUpdate);

	synthEnvelopeGeneratorView::synthEnvelopeGeneratorView(synthModule *module) : synthModuleView(module)
	{
		AddComponent(&m_TotalTimeLabel);

		m_ReleaseType.AddItem("Lin Release");
		m_ReleaseType.AddItem("Exp Release");
		m_ReleaseType.SetCurrentIndex(GetEnvelope()->GetReleaseType());
		AddComponent(&m_ReleaseType);

		m_TriggerMode.AddItem("One Shot");
		m_TriggerMode.AddItem("Retrigger");
		m_TriggerMode.AddItem("Interruptible");
		m_TriggerMode.SetCurrentIndex(GetEnvelope()->GetTriggerMode());
		AddComponent(&m_TriggerMode);

		m_Texture = NULL;
	}

	synthEnvelopeGeneratorView::~synthEnvelopeGeneratorView()
	{
		if(m_Texture)
		{
			m_Texture->Release();
			m_Texture = NULL;
		}
	}

	void synthEnvelopeGeneratorView::Update()
	{
		PF_FUNC(EnvelopeGeneratorUpdate);

		const f32 predelayTime = Max(0.f, GetEnvelope()->GetInput(kEnvelopeGeneratorPredelay).GetNormalizedValue(0));
		const f32 attackTime = Max(0.f, GetEnvelope()->GetInput(kEnvelopeGeneratorAttack).GetNormalizedValue(0));
		const f32 decayTime = Max(0.f, GetEnvelope()->GetInput(kEnvelopeGeneratorDecay).GetNormalizedValue(0));
		const f32 sustainLevel = GetEnvelope()->GetInput(kEnvelopeGeneratorSustain).GetNormalizedValue(0);
		const f32 holdTime = Max(0.f, GetEnvelope()->GetInput(kEnvelopeGeneratorHold).GetNormalizedValue(0));
		const f32 releaseTime = Max(0.f, GetEnvelope()->GetInput(kEnvelopeGeneratorRelease).GetNormalizedValue(0));

		bool needDraw = false;

		const f32 totalTime = predelayTime + attackTime + decayTime + holdTime + releaseTime;

		s32 releaseType = GetEnvelope()->GetReleaseType();
		
		if(totalTime != m_LastTotalTime || sustainLevel != m_LastSustainLevel || releaseType != m_LastReleaseType)
		{
			needDraw = true;
		}
		
		m_LastTotalTime  = totalTime;
		m_LastSustainLevel = sustainLevel;
		m_LastReleaseType = releaseType;

		if(needDraw || m_Texture == NULL)
		{
			const u32 numSamplesToRender = 256;
			if(!m_Texture)
			{
				m_Texture = synthUIComponent::CreateTexture(numSamplesToRender, 1, grcImage::R32F);			
			}			

			grcTextureLock lock;
			
			m_Texture->LockRect(0,0,lock,grcsWrite);
			f32 *textPtrF32 = (f32*)lock.Base;
			
			// rasterize the evaluated curve
			f32 vol = 0.f;
			const f32 timeStep = totalTime / (f32)numSamplesToRender;
			f32 time = 0.f;
			enum states{kPredelay,kAttack,kDecay,kHold,kRelease}state = kPredelay;
			for(s32 x = 0; x < numSamplesToRender; x++)
			{
			
				switch(state)
				{
				case kPredelay:
					if(time >= predelayTime)
					{
						state = kAttack;
						time = 0.f;
					}
					else
					{
						time += timeStep;
						break;
					}
					
				case kAttack:
					if(time >= attackTime)
					{
						state = kDecay;
						vol = 1.f;
						time = 0.f;
					}
					else
					{
						// linear ramp to 1
						vol = time / attackTime;
						time += timeStep;
						break;
					}
					
				case kDecay:
					if(time >= decayTime)
					{
						state = kHold;
						time = 0.f;
						vol = sustainLevel;
					}
					else
					{
						vol = Lerp(time/decayTime, 1.f, sustainLevel);
						time += timeStep;
						break;
					}
					
				case kHold:
					if(time >= holdTime)
					{
						state = kRelease;
						time = 0.f;
						m_ReleaseStartGain = vol;
					}
					else
					{
						time += timeStep;
						break;
					}
					
				case kRelease:
					if(releaseTime > 0.f)
					{
						if(GetEnvelope()->GetReleaseType() == synthEnvelopeGenerator::Exponential)
						{
							const f32 horizontalScaling = 4.f;
							const f32 rescaledX = (time / releaseTime) * horizontalScaling;
							const f32 yAtOne = 0.0183156389f;//Powf(M_E, -1.0f * horizontalScaling);
							const f32 oneOverOneMinusyAtOne = 1.f / (1.f - yAtOne);
							const f32 clampedOutput = (Powf(M_E, (-1.0f * rescaledX)) - yAtOne) * oneOverOneMinusyAtOne;

							vol = clampedOutput * m_ReleaseStartGain;	
						}
						else
						{
							vol = Lerp(time/releaseTime, sustainLevel, 0.f);
						}
					}
					else
					{
						vol = 0.f;
					}
					time += timeStep;
					break;
				}
		
				textPtrF32[x] = 0.05f + vol * 0.9f;
			}
			m_Texture->UnlockRect(lock);
		}

		formatf(m_TotalTime, "Length: %.2fs%s", totalTime, GetEnvelope()->GetInput(kEnvelopeGeneratorHold).GetNormalizedValue(0) < 0.f ? " (Inf Hold)":"");
		m_TotalTimeLabel.SetString(m_TotalTime);

		m_TotalTimeLabel.SetShouldDraw(!IsMinimised());
		//m_TitleLabel.SetShouldDraw(!IsMinimised());

		Matrix34 mtx = GetMatrix();		

		mtx.d -= mtx.b * 2.f;
		m_ReleaseType.SetMatrix(mtx);
		m_ReleaseType.SetShouldDraw(!IsMinimised());
		m_ReleaseType.SetAlpha(1.f);
		GetEnvelope()->SetReleaseType((synthEnvelopeGenerator::synthEnvelopeReleaseType)m_ReleaseType.GetCurrentIndex());

		mtx.d -= mtx.b * 1.f;
		m_TriggerMode.SetMatrix(mtx);
		m_TriggerMode.SetShouldDraw(!IsMinimised());
		m_TriggerMode.SetAlpha(1.f);
		GetEnvelope()->SetTriggerMode((synthEnvelopeGenerator::synthEnvelopeTriggerMode)m_TriggerMode.GetCurrentIndex());

		mtx.d -= mtx.b * 4.5f;
		m_TotalTimeLabel.SetMatrix(mtx);
		m_TotalTimeLabel.SetSize(0.25f);
		m_TotalTimeLabel.SetAlpha(1.f);

		synthModuleView::Update();
	}

	void synthEnvelopeGeneratorView::Render()
	{	
		synthModuleView::Render();

		Matrix34 mtx = GetMatrix();
		if(!IsMinimised())
		{
			mtx.d += mtx.b * 3.f;
		}

		Vector3 scalingVec(2.f,0.9f,1.f);
		mtx.Transform3x3(scalingVec);
		mtx.Scale(scalingVec);
		grcWorldMtx(mtx);

		SetShaderVar("FontTex",m_Texture);
		SetShaderVar("DiffuseTex",m_Texture);
		SetShaderVar("g_WaveXZoom", 1.f);
		SetShaderVar("g_WaveYZoom", 1.f);
		SetShaderVar("g_WaveXOffset", 0.f);
		SetShaderVar("g_GhostLevel", 0.f);
		SetShaderVar("g_TextColour", Color32(0.f,0.4f,0.0f,1.f));
		SetShaderVar("g_AlphaFade", 1.f/*GetAlpha()*/);
		DrawVB("drawwave");		
	}

	bool synthEnvelopeGeneratorView::StartDrag() const
	{
		if(!m_TriggerMode.IsUnderMouse() && !m_TriggerMode.IsShowingMenu() && !m_ReleaseType.IsUnderMouse() && !m_ReleaseType.IsShowingMenu())
		{
			return synthModuleView::StartDrag();
		}
		return false;
	}
}

#endif // __SYNTH_EDITOR
