
#ifndef SYNTH_PROGENITOR_H
#define SYNTH_PROGENITOR_H

#include "blocks.h"
#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "audioeffecttypes/revmodelprog.h"

namespace rage
{
	enum synthProgenitorReverbInputs
	{
		kProgenitorReverbSignal = 0,
		kProgenitorReverbDecay1,
		kProgenitorReverbDecay2,
		kProgenitorReverbDecay3,
		kProgenitorReverbDiffusion1,
		kProgenitorReverbDiffusion2,
		kProgenitorReverbDecayDiffusion,
		kProgenitorReverbDefinition,
		kProgenitorReverbHFBandwidth,
		kProgenitorReverbDamping,

		kProgenitorReverbNumInputs
	};	
	
	class revModelProgenitor
	{
	public:


		float Diffusion1;
		float Diffusion2;
		float DecayDiffusion;
		float Decay1;
		float Decay2;
		float Decay3;
		float HF_Bandwidth;
		float Definition;

		float ChorusDry;
		float ChorusWet;

		float Damping;

		revModelProgenitor()
		{
			HF_Bandwidth = 0.188f;
			Decay1 = 0.938f;
			Decay2 = 0.844f;
			Decay3 = 0.906f;
			Diffusion1 = 0.312f;
			Diffusion2 = 0.375f;
			Definition = 0.25f;
			DecayDiffusion = 0.406f;

			ChorusDry = 0.781f;
			ChorusWet = 0.219f;

			Damping = 0.312f;

		}

		float Process(const float lInput, const float rInput);

	private:
		
		revDCBlocker dcL,dcR;
		// Left channel
		//revSimpleDelay<1> n_59_60;
		revSimpleDelay<1> n_60_61;
		//revSimpleDelay<1> n_9_10;
		//revSimpleDelay<1> n_11_12;

		revOnePole n_7_8; // biquad?
		revOnePole n_9_10; // biquad?
		revOnePole n_59_60;
		revOnePole n_64_65;
		revOnePole n_11_12;
		revOnePole n_13_14;

		revAllPassFig8<239> n_15_16;
		revSimpleDelay<2> n_16_17;
		revAllPassFig8<392> n_17_18;

		revSimpleDelay<1055> n_23_24;
	
		revAllPassDualFig8<1944, 612> n_24_27;
		revSimpleDelay<344> n_27_31;
		revAllPassTrioChorus<1212, 121, 816, 1264> n_31_37;
		revSimpleDelay<1572> n_37_39a;

		// right channel
		//revSimpleDelay<1> n_64_65;
		revSimpleDelay<1> n_65_66;
		//revSimpleDelay<1> n_7_8;
		//revSimpleDelay<1> n_13_14;

		revAllPassFig8<205> n_19_20;
		revSimpleDelay<1> n_20_21;
		revAllPassFig8<329> n_21_22;
		
		revSimpleDelay<625> n_40_41;
		revSimpleDelay<835> n_41_42;
		revAllPassDualFig8<368, 2032> n_42_45;
		revSimpleDelay<500> n_45_49;
		revAllPassTrioChorus<1452, 5, 688, 1340> n_49_55;
		revSimpleDelay<16> n_55_58;
	
	};

	class synthProgenitorReverb : public synthModuleBase<kProgenitorReverbNumInputs, 2, SYNTH_PROGENITORREVERB>
	{     
	public:

		synthProgenitorReverb();
		virtual ~synthProgenitorReverb();


		virtual void Synthesize();

		virtual const char *GetName() const
		{
			return "Progenitor Reverb";
		}


	private:
		void UpdateInput(const s32 index, const s32 sampleIndex, float &destVal, Vec::Vector_4V_Ref destValV);
		void InitInput(const s32 index, const char *name, float &val);
		void UpdateInputs(const s32 sampleIndex);

		revModelProgenitor m_Reverb1;
		revModelProgenitor_V4 m_Reverb2;

		synthSampleFrame m_Buffer1;
		synthSampleFrame m_Buffer2;
	};
}
#endif // __SYNTH_EDITOR
#endif // SYNTH_PROGENITOR_H


