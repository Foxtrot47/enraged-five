// 
// audiosynth/synthdefs.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTHDEFS_H
#define SYNTHDEFS_H

#define SYNTH_MINIMAL_MODULES 0

#define __SYNTH_EDITOR (__WIN32PC && __BANK && !__TOOL && 0)

#if __SYNTH_EDITOR
#define SYNTH_EDITOR_ONLY(x) x
#define SYNTH_EDITOR_VIEW(x) SYNTH_EDITOR_LINK_VIEW(x, x##View)
#define SYNTH_EDITOR_LINK_VIEW(x,y) synthModuleView *x::AllocateView() { return rage_new y(this); }
#define SYNTH_CUSTOM_MODULE_VIEW synthModuleView *AllocateView()
#else
#define SYNTH_EDITOR_ONLY(x)
#define SYNTH_EDITOR_VIEW(x)
#define SYNTH_EDITOR_LINK_VIEW(x,y)
#define SYNTH_CUSTOM_MODULE_VIEW
#endif

#define SYNTH_PROFILE (0 && !__FINAL)
#if SYNTH_PROFILE
#define SYNTH_PROFILE_ONLY(x) x
#else
#define SYNTH_PROFILE_ONLY(x)
#endif

#endif // SYNTHDEFS_H

