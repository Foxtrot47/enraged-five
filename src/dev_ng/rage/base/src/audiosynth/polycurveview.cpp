// 
// audiosynth/polycurveview.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "polycurveview.h"
#include "polycurve.h"
#include "moduleview.h"
#include "pinview.h"
#include "uiplane.h"

#include "vector/vectort.h"
#include "vector/matrixt.h"

#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"

#include "module.h"
#include "pin.h"

#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mouse.h"

namespace rage
{
	synthPolyCurveView::synthPolyCurveView(synthModule *module) : synthModuleView(module)
	{
		m_DrawnTexture = NULL;
		m_FitTexture = NULL;
		m_LastEditedPoint = -1;
		sysMemSet(m_Points, 0, sizeof(m_Points));
	}

	void synthPolyCurveView::Update()
	{
		// fade out title when editing
		SetTitleAlpha(1.f - GetHoverFade());
		m_TitleLabel.SetUnderMouse(IsUnderMouse());

		Matrix34 mtx = GetMatrix();

		// render one cycle
		const u32 numSamplesToRender = kMaxPoints;
		if(!m_DrawnTexture)
		{
			m_DrawnTexture = synthUIComponent::CreateTexture(numSamplesToRender, 1, grcImage::R32F);
		}			

		grcTextureLock lock;
		m_DrawnTexture->LockRect(0,0,lock,grcsWrite);
		f32 *textPtrF32 = (f32*)lock.Base;
		sysMemCpy(textPtrF32, m_Points, sizeof(m_Points));
		m_DrawnTexture->UnlockRect(lock);
		
		if(!m_FitTexture)
		{
			m_FitTexture = synthUIComponent::CreateTexture(numSamplesToRender, 1, grcImage::R32F);
		}			

		m_FitTexture->LockRect(0,0,lock,grcsWrite);
		textPtrF32 = (f32*)lock.Base;
		f32 phase = 0.f;
		const f32 phaseStep = 1.f / (f32)numSamplesToRender;
		f32 minY = FLT_MAX,maxY = -FLT_MAX;
		for(u32 i = 0; i < numSamplesToRender; i++)
		{
			textPtrF32[i] = GetPolyCurve()->EvaluatePolynomial(phase);
			minY = Min(textPtrF32[i],minY);
			maxY = Max(textPtrF32[i],maxY);
			phase += phaseStep;
		}
		// normalize to aid debugging
		if(maxY!=minY)
		{
			for(u32 i = 0; i < numSamplesToRender; i++)
			{
				textPtrF32[i] = (textPtrF32[i]-minY) / (maxY-minY);
			}
		}

		m_FitTexture->UnlockRect(lock);

		const bool isEditing = UpdateEdit(false);
		GetPolyCurve()->GetInput(0).GetView()->SetShouldDraw(!isEditing);
		GetPolyCurve()->GetOutput(0).GetView()->SetShouldDraw(!isEditing);
		ComputeCoefficients();
		synthModuleView::Update();
	}

	bool synthPolyCurveView::UpdateEdit(const bool checkBoundsOnly)
	{
		if(IsUnderMouse())
		{
			Vector3 mousePos = synthUIPlane::GetMaster()->GetMousePosition();

			GetMatrix().UnTransform(mousePos);
			f32 width,height;
			ComputeWidthAndHeight(width, height);

			const f32 relX = Clamp(0.5f + (mousePos.x / width), 0.f, 1.f);
			const f32 relY = 1.f - Clamp(0.5f + (mousePos.y / height), 0.f, 1.f);


			if(relX >= 0.15f && relX <= 0.85f && relY >= 0.3f && relY <= 0.7f)
			{
				if(ioMouse::GetButtons()&ioMouse::MOUSE_LEFT)
				{
					if(!checkBoundsOnly)
					{
						const f32 drawnX = Clamp((relX-0.15f) / (0.85f-0.15f),0.f,1.f);
						const f32 drawnVal = Clamp((relY - 0.3f) / (0.4f),0.f,1.f);
						
						s32 pointIndex = (s32)(drawnX * kMaxPoints);
						m_Points[pointIndex] = drawnVal;
					
						if(m_LastEditedPoint != -1)
						{
							const s32 numPoints = Abs(pointIndex - m_LastEditedPoint);
							const f32 delta = (m_Points[pointIndex]-m_Points[m_LastEditedPoint]) / (f32)numPoints;
							f32 val = m_Points[m_LastEditedPoint];
							for(s32 i = m_LastEditedPoint; i != pointIndex; i += Sign(pointIndex-m_LastEditedPoint))
							{
								m_Points[i] = val;
								val += delta;
							}
						}
						m_LastEditedPoint = pointIndex;
					}
					return true;
				}
			}			
		}
		if(!checkBoundsOnly && !ioKeyboard::KeyDown(KEY_CONTROL))
		{
			m_LastEditedPoint = -1;
		}
		return false;
	}

	//-----------------------------------------------------------------------------
	//
	// Solves A*x = b for a vector x, where A is specified by
	// the arrays U[0..m-1][0..n-1], w[0..n-1], and V[0..n-1][0..n-1]
	// as returned by svdcmp. m and n are the dimensions of A and will be
	// equal for square matrices. b[0..m-1] is the input right-hand
	// side. x[0..n-1] is the output solution vector. No input
	// quantities are destroyed, so the routine may be called
	// sequentially with different b vectors.
	//
	//-----------------------------------------------------------------------------

	void svbacksub( const MatrixT<f32> &U, const VectorT<f32> &w,
		const MatrixT<f32> &V, const VectorT<f32> &b,
		VectorT<f32> &x )
	{
		s16 m = U.GetRows();
		s16 n = U.GetCols();
		VectorT<f32> tmp(n,0.f);

		//Calculate w^-1 * U^T * b
		for( s16 j=0; j < n; ++j ) {
			f32 s = 0.0;
			if( w[j] != 0.0 ) {			//Nonzero result only if w[j] is nonzero.
				for( s16 i=0; i < m; ++i ) {
					s += U[i][j]*b[i];
				}
				s /= w[j];				//This is the divide by w[j].
			}
			tmp[j] = s;
		}

		//Now matrix multiply by V to get answer.
		
		VectorT<f32> multmp( V.GetRows(), 0.f );
		for( s16 row=0; row < V.GetRows(); ++row ) {
			f32 sum = 0.0;
			for( s16 k=0; k < V.GetCols(); ++k )  sum += V[row][k] * tmp[k];
			multmp[row] = sum;
		}

	 x = multmp;

	}


	//-----------------------------------------------------------------------------
	// Computes(a2+b2)^1/2 without destructive underflow or overflow.
	// (Used by svdcmp)
	//-----------------------------------------------------------------------------

	f32 pythag( f32 a, f32 b )
	{
		f32 absa = Abs(a);
		f32 absb = Abs(b);
		if( absa > absb )
			return absa*Sqrtf( 1.0f + square(absb/absa) );
		else
			return ( absb == 0.0f ? 0.0f : absb*Sqrtf( 1.0f + square(absa/absb) ) );
	}

	//-----------------------------------------------------------------------------
	// Singular value decomposition utility routine
	//
	// adapted from Press et al.'s routine, svdcmp
	//
	// Given a Matrix M[0..m-1][0..n-1], this routine computes its
	// singular value decomposition, A = U w VT. The matrix U replaces
	// M on output. The diagonal matrix of singular values W is output
	// as a vector w[0..n-1]. The matrix V (not the transpose VT) is
	// output as V[0..n-1][0..n-1].
	//
	//-----------------------------------------------------------------------------
	void svdecomp( MatrixT<f32> &M, VectorT<f32> &w, MatrixT<f32> &V )
	{
		s16 m = M.GetRows();
		s16 n = M.GetCols();

		//
		// Householder reduction to bidiagonal form.
		//
		VectorT<f32> rv1(n,0.f);
		f32 f, h, s, x, y, z;
		f32 g     = 0.0f;
		f32 scale = 0.0f;
		f32 anorm = 0.0f;
		s16 p = 0;
		for( s16 i=0; i < n; ++i ) {
			p = i+1;
			rv1[i] = scale*g;
			g = s = scale = 0.0;
			if( i < m ) {
				for( s16 k=i; k < m; ++k )  scale += abs(M[k][i]);
				if( scale != 0.0 ) {
					for( s16 k=i; k < m; ++k ) {
						M[k][i] /= scale;
						s       += square(M[k][i]);
					}
					f = M[i][i];
					g = -Sign(/*Sqrtf(s),*/f);
					h = f*g - s;
					M[i][i] = f-g;
					for( s16 j=p; j < n; ++j ) {
						s = 0.0;
						for( s16 k=i; k < m; ++k )  s += M[k][i]*M[k][j];
						f = s/h;
						for( s16 k=i; k < m; ++k )  M[k][j] += f*M[k][i];
					}
					for( s16 k=i; k < m; ++k ) M[k][i] *= scale;
				}
			}
			w[i] = scale*g;
			g = s = scale = 0.0;
			if( i < m  &&  i != n-1 ) {
				for( s16 k=p; k < n; ++k )  scale += Abs(M[i][k]);
				if( scale != 0.0 ) {
					for( s16 k=p; k < n; ++k ) {
						M[i][k] /= scale;
						s       += square(M[i][k]);
					}
					f = M[i][p];
					g = -Sign(/*Sqrtf(s),*/f);
					h = f*g - s;
					M[i][p] = f-g;
					for( s16 k=p; k < n; ++k )  rv1[k] = M[i][k]/h;
					for( s16 j=p; j < m; ++j ) {
						s = 0.0;
						for( s16 k=p; k < n; ++k )  s       += M[j][k]*M[i][k];
						for( s16 k=p; k < n; ++k )  M[j][k] += s*rv1[k];
					}
					for( s16 k=p; k < n; ++k )  M[i][k] *= scale;
				}
			}
			anorm = Max( anorm, Abs(w[i])+Abs(rv1[i]) );
		}

		//
		// Accumulation of right-hand transformations.
		//
		for( s16 i=n-1; i >= 0; --i ) {
			if( i < n-1 ) {
				if( g != 0.0f ) {
					for( s16 j=p; j < n; ++j )	//Double div to avoid underflow.
						V[j][i] = (M[i][j]/M[i][p])/g;
					for( s16 j=p; j < n; ++j ) {
						s = 0.0;
						for( s16 k=p; k < n; ++k )  s       += M[i][k]*V[k][j];
						for( s16 k=p; k < n; ++k )  V[k][j] += s*V[k][i];
					}
				}
				for( s16 j=p; j < n; ++j )  V[i][j] = V[j][i] = 0.0f;
			}
			V[i][i] = 1.0f;
			g       = rv1[i];
			p       = i;
		}

		//
		// Accumulation of left-hand transformations.
		//
		for( s16 i = Min(m,n)-1; i >= 0; --i ) {
			p = i+1;
			g = w[i];
			for( s16 j=p; j < n; ++j )  M[i][j] = 0.0f;
			if( g != 0.0f ) {
				g = 1.0f/g;
				for( s16 j=p; j < n; ++j ) {
					s = 0.0f;
					for( s16 k=p; k < m; ++k )  s += M[k][i]*M[k][j];
					f = (s/M[i][i])*g;
					for( s16 k=i; k < m; ++k )  M[k][j] += f*M[k][i];
				}
				for( s16 j=i; j < m; ++j )  M[j][i] *= g;
			} else {
				for( s16 j=i; j < m; ++j )  M[j][i] = 0.0f;
			}
			++M[i][i];
		}

		//
		// Diagonalization of the bidiagonal form: Loop over
		// singular values and over allowed iterations.
		//
		const s32 ITSMAX = 30;
		for( s16 k=n-1; k >= 0; --k ) {
			for( s16 its=0; its < ITSMAX; ++its ) {
				bool flag = true;
				s16 nm = 0;
				f32 c;
				for( p=k; p >= 0; --p ) {		//Test for splitting.
					nm = p-1;					//Note that rv1[0] is always zero.
					if( Abs(rv1[p])+anorm == anorm ) {
						flag = false;
						break;
					}
					if( Abs(w[nm])+anorm == anorm )  break;
				}
				if( flag ) {
					c = 0.0f;					//Cancellation of rv1[p], if p > 0.
					s = 1.0f;
					for( s16 i=p; i <= k; i++ ) {
						f      = s*rv1[i];
						rv1[i] = c*rv1[i];
						if( Abs(f)+anorm == anorm )  break;
						g    = w[i];
						h    = pythag(f,g);
						w[i] = h;
						h    = 1.0f/h;
						c    = g*h;
						s    = -f*h;
						for( s16 j=0; j < m; ++j ) {
							y        = M[j][nm];
							z        = M[j][i];
							M[j][nm] = y*c + z*s;
							M[j][i]  = z*c - y*s;
						}
					}
				}
				z = w[k];
				if( p == k ) {					//Convergence.
					if( z < 0.0f ) {				//Singular value is made nonnegative.
						w[k] = -z;
						for( s16 j=0; j < n; ++j )  V[j][k] = -V[j][k];
					}
					break;
				}
				if( its == ITSMAX-1 )
				{
					audErrorf("no convergence in ITSMAX svdsmp iterations");
					return;
				}
				x  = w[p];						//Shift from bottom 2-by-2 minor.
				nm = k - 1;
				y  = w[nm];
				g  = rv1[nm];
				h  = rv1[k];
				f  = ( (y-z)*(y+z) + (g-h)*(g+h) ) / (2.0f*h*y);
				g  = pythag( f, 1.0f );
				f  = ( (x-z)*(x+z) + h*( ( y/(f+Sign(g/*,f*/)) ) - h ) ) / x;

				//
				// Next QR transformation:
				//
				c = s = 1.0f;
				for( s16 j=p; j <= nm; ++j ) {
					s16 i  = j + 1;
					g      = rv1[i];
					y      = w[i];
					h      = s*g;
					g      = c*g;
					z      = pythag(f,h);
					rv1[j] = z;
					c      = f/z;
					s      = h/z;
					f      = x*c + g*s;
					g      = g*c - x*s;
					h      = y*s;
					y     *= c;
					for( s16 jj=0; jj < n; ++jj ) {
						x        = V[jj][j];
						z        = V[jj][i];
						V[jj][j] = x*c + z*s;
						V[jj][i] = z*c - x*s;
					}
					z    = pythag(f,h);
					w[j] = z;					//Rotation can be arbitrary if z = O.
					if( z != 0.0f ) {
						z = 1.0f/z;
						c = f*z;
						s = h*z;
					}
					f = c*g + s*y;
					x = c*y - s*g;
					for( s16 jj=0; jj < m; ++jj ) {
						y        = M[jj][j];
						z        = M[jj][i];
						M[jj][j] = y*c + z*s;
						M[jj][i] = z*c - y*s;
					}
				}
				rv1[p] = 0.0f;
				rv1[k] = f;
				w[k]   = x;
			}
		}
	}

	void ComputeInverse(MatrixT<f32> &inout)
	{
		s16 nrows = inout.GetRows();
		s16 ncols = inout.GetCols();

		if( nrows != ncols)
			return; // can't compute inverse of non-square matrix

		MatrixT<f32> U( inout ), V(nrows,ncols);
		VectorT<f32> x(nrows,0.f), w(nrows,0.f), b(nrows,0.f);


		MatrixT<f32> temp(inout);
		svdecomp( U, w, V );
		for( s16 row=0; row < nrows; ++row ) {
			b = VectorT<f32>(b.GetSize(),0.0f);
			b[row] = 1.0f;
			svbacksub( U, w, V, b, x );
			for( s16 col=0; col < ncols; ++col )
				temp[row][col] = x[col];
		}

		inout = temp;
	}

	void synthPolyCurveView::ComputeCoefficients()
	{
		//calculate the normal equations matrix and resultant vector
		const s16 porder = 7;
		MatrixT<f32> M( porder+1, porder+1 );
		VectorT<f32> b( porder+1, 0.f );
		s32 n = kMaxPoints;

		for( s16 row=0; row <= porder; ++row ) 
		{
			for( s16 col=row; col <= porder; ++col ) 
			{
				f32 sum = 0.0;
				for( long k=0; k < n; ++k )  
				{
					if( k != 0 )
					{
						// x linearly increases from 0 to 1 as k goes from 0 to n
						sum += pow((f32)k / (f32)n, row+col);
					}
				}
				M[row][col] = sum;
				if( row != col )
				{
					M[col][row] = sum;
				}
			}
			f32 sum = 0.0;
			for( long k=0; k < n; ++k )
			{
				if( k != 0 ) 
				{
					// x linearly increases from 0 to 1 as k goes from 0 to n
					sum += pow((f32)k/(f32)n,row)*m_Points[k];
				}
			}
			b[row] = sum;
		}

		//covariance matrix
		ComputeInverse(M);

		//solve for the coefficients
		VectorT<f32> coeffs( porder+1, 0.f );
//		coeffs = M * b;

		VectorT<f32> multmp( M.GetRows(), 0.f );
		for( s16 row=0; row < M.GetRows(); ++row ) {
			f32 sum = 0.0;
			for( s16 k=0; k < M.GetCols(); ++k )  sum += M[row][k] * b[k];
			coeffs[row] = sum;
		}

		f32 *runtimeCoefficients = GetPolyCurve()->GetCoefficients();
		for(s16 i = 0; i < synthPolyCurve::kMaxCoefficients; i++)
		{
			runtimeCoefficients[i] = coeffs[i];
		}

		// normalize coefficients
		f32 maxC = -FLT_MAX;
		f32 minC = FLT_MAX;
		for(s32 i = 0; i < synthPolyCurve::kMaxCoefficients; i++)
		{
			maxC = Max(maxC, runtimeCoefficients[i]);			
			minC = Min(minC, runtimeCoefficients[i]);
		}

		const f32 range = maxC-minC;
		if(range != 0.f)
		{
			for(s32 i = 0; i < synthPolyCurve::kMaxCoefficients; i++)
			{
				runtimeCoefficients[i] = (runtimeCoefficients[i]-minC) / range;
			}
		}

	}

	void synthPolyCurveView::Render()
	{
		synthModuleView::Render();

		Matrix34 mtx = GetMatrix();

		Vector3 scalingVec(3.f,2.f,1.f);
		mtx.Transform3x3(scalingVec);
		mtx.Scale(scalingVec);
		grcWorldMtx(mtx);

	//	SetShaderVar("DiffuseTex2",m_DrawnTexture);
		SetShaderVar("DiffuseTex",m_FitTexture);
		SetShaderVar("g_WaveXZoom", 1.f);
		SetShaderVar("g_WaveYZoom", 1.f);
		SetShaderVar("g_WaveXOffset", 0.f);
		SetShaderVar("g_GhostLevel", 0.f);
		SetShaderVar("g_TextColour", Color32(0,0,255));
		DrawVB("drawcurve");	

		if(IsUnderMouse())
		{
			Vector3 v1,v2;
			GetBounds(v1,v2);
			grcDrawBox(v1,v2,Color32(1.f,1.f,1.f,1.f));
		}
	}

	bool synthPolyCurveView::StartDrag() const
	{
	/*	if(UpdateEdit(true))
		{
			return false;
		}
		else
		{*/
			return synthModuleView::StartDrag();
		//}
	}

	bool synthPolyCurveView::IsActive() const
	{
		return (IsUnderMouse());// || m_FreqRange.IsActive());
	}


} // namespace rage

#endif // __SYNTH_EDITOR
