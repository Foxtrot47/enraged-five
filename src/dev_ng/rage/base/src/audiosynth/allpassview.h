// 
// audiosynth/allpassview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_ALLPASSVIEW_H
#define SYNTH_ALLPASSVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uilabel.h"
#include "uimenuview.h"
#include "uirangeview.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthAllpassFilter;
	class synthAllpassFilterView : public synthModuleView
	{
	public:

		synthAllpassFilterView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();

		synthAllpassFilter *GetFilterModule()
		{
			return (synthAllpassFilter*)m_Module;
		}
	private:
		synthUIRangeView m_FreqRange;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_ALLPASSVIEW_H

