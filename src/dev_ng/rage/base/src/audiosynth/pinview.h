// 
// audiosynth/pinview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_PINVIEW_H
#define SYNTH_PINVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uiscrollbar.h"
#include "uilabel.h"
#include "uiview.h"
#include "vector/vector3.h"

#include "audiohardware/mixer.h"

namespace rage
{
	class grcTexture;
	class synthPin;
	class grcRenderTarget;

	class synthPinView : public synthUIView
	{
	
	public:

		synthPinView(synthPin *pin);
		virtual ~synthPinView();

		static bool InitClass();
		static void ShutdownClass();

		void SetDrawFilled(const bool drawFilled)
		{
			m_DrawFilled = drawFilled;
		}

		void SetAnimate(const bool animate)
		{
			m_Animate = animate;
		}

		synthPin *GetPin()
		{
			return m_Pin;
		}
		// synthUIComponent functionality
		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;
	
		bool IsBeingDragged() const
		{
			return m_IsBeingDragged;
		}

		void SetIsBeingDragged(const bool is)
		{
			m_IsBeingDragged = is;
		}

		bool StartDrag() const { return false; }

		f32 GetPeakValue() const
		{
			return m_PeakValue;
		}

		f32 GetRMSValue() const
		{
			return m_RMSValue;
		}

		void SetStaticViewRange(const f32 min, const f32 max)
		{
			m_OverridenDisplayRange = true;
			m_MinStaticValue = min;
			m_MaxStaticValue = max;
		}

		void SetNormalizedValue(const float val)
		{
			m_Scrollbar.SetNormalizedValue(val);
		}

		void PostSynthesize();

		bool IsActive() const;

		// PURPOSE
		//	Called when the pin value has been modified externally
		void Notify_SetValue();

		void SetDefaultYZoom(const f32 defaultYZoom) { m_DefaultYZoom = defaultYZoom; }

		void OnConnection(synthPin *otherPin);
		void OnDisconnection(synthPin *otherPin);
		atArray<synthPin*> &GetOutputDestinations() { return m_OutputDestinationPins; }

		void DisableZooming(const bool disable) { m_DisableZooming = disable; }
		bool IsZoomingDisabled() const { return m_DisableZooming; }
		void SetShouldDrawTitle(const bool should) { m_ShouldDrawTitle = should; }
		void SetReadOnly(const bool readOnly) { m_Scrollbar.SetReadOnly(readOnly); }
		void SetShouldDrawReflection(const bool should) { m_ShouldDrawReflection = should; }

		void SetTargetScale(const f32 scale) { m_OverridenTargetScale = scale; }
		void DisableConnections(const bool disable) { m_DisableConnections = disable; }
		
	protected:
		virtual void Render();
		
	private:

		void ResetPeaks();

		synthPin *m_Pin;
		grcTexture *m_Texture;
		grcRenderTarget *m_TrailRenderTargets[2];

		static grcRenderTarget *sm_SharedRenderTarget;
		static grcRenderTarget *sm_SharedDepthRT;
		u32 m_RTIndex;
		synthUILabel m_Label;
		synthUIScrollbar m_Scrollbar;
		synthUILabel m_PeakLabel;

		char m_Title[32];
		char m_PeakLabelText[32];
		
		f32 m_LastScale;
		f32 m_TargetScale;
		f32 m_OverridenTargetScale;
		s32 m_LastMouseY;

		f32 m_PeakValue;
		f32 m_RMSValue;

		f32 m_HeldPeakMaxValue;
		f32 m_HeldPeakMinValue;
		f32 m_HeldPeakMaxValue_Display;
		f32 m_HeldPeakMinValue_Display;
		f32 m_TimeTilPeakDisplayUpdate;

		f32 m_WaveXZoom;
		f32 m_WaveYZoom;
		f32 m_DefaultYZoom;
		f32 m_WaveXOffset;

		f32 m_MinStaticValue;
		f32 m_MaxStaticValue;
		f32 m_LastStaticValue;


		u32 m_FramesSinceStaticValueChanged;

		atArray<synthPin *> m_OutputDestinationPins;

		bool m_DrawFilled;
		bool m_Animate;
		bool m_IsBeingDragged;
		bool m_ShouldBeHighlighted;
		bool m_DisableZooming;
		bool m_ShouldDrawTitle;
		bool m_ShouldDrawReflection;
		bool m_DisableConnections;
		bool m_OverridenDisplayRange;
		bool m_DrawLowLOD;
		bool m_IsTextureLowLOD;

		static f32 sm_ZoomScale;
	};
}// namespace rage
#endif // __SYNTH_EDITOR
#endif // SYNTH_WAVEFORMVIEW_H
