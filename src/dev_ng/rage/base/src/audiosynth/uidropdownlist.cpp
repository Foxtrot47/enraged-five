#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "input/mouse.h"
#include "uidropdownlist.h"
#include "uiplane.h"

namespace rage
{
	synthUIDropDownList::synthUIDropDownList() : m_CurrentIndex(0)
	{
		AddComponent(&m_Label);
		AddComponent(&m_Menu);
		m_Label.SetSize(0.275f);
		m_Menu.SetShouldDraw(false);
		m_Menu.SetTextSize(0.2f);
		m_MenuOffset.Zero();
	}

	synthUIDropDownList::~synthUIDropDownList()
	{

	}

	u32 synthUIDropDownList::AddItem(const char *itemString)
	{
		const u32 index = m_Items.GetCount();
		m_Items.PushAndGrow(itemString);
		m_Menu.AddItem(itemString, index);
		return index;
	}

	void synthUIDropDownList::GetBounds(Vector3 &v1, Vector3 &v2) const
	{	
		if(m_Menu.ShouldDraw())
		{
			m_Menu.GetBounds(v1,v2);
		}
		else
		{
			m_Label.GetBounds(v1,v2);
		}
	}

	void synthUIDropDownList::Update()
	{
		Matrix34 mat = GetMatrix();
		m_Label.SetMatrix(mat);
		m_Label.SetShouldDraw(ShouldDraw() && !m_Menu.ShouldDraw());
		m_Label.SetAlpha(GetAlpha());

		mat.d += m_MenuOffset;
		m_Menu.SetMatrix(mat);

		if(m_Menu.ShouldDraw())
		{
			if(m_Menu.HasSelection())
			{
				m_CurrentIndex = m_Menu.GetSelectedTag();
				m_Menu.SetShouldDraw(false);
			}
			if(!IsUnderMouse())
			{
				if(synthUIPlane::GetMaster()->HadLeftClick())
				{
					m_Menu.SetShouldDraw(false);
				}
			}
		}
		else
		{
			if(m_Label.IsUnderMouse())
			{	
				if(ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT)
				{
					m_CurrentIndex = (m_CurrentIndex + 1) % m_Items.GetCount();
				}
				else if(synthUIPlane::GetMaster()->HadRightClick())
				{
					m_Menu.ClearSelection();
					m_Menu.SetShouldDraw(true);
				}
			}
		}

		m_Label.SetString(m_Items[m_CurrentIndex]);
		synthUIView::Update();
	}

	void synthUIDropDownList::Render()
	{
		synthUIView::Render();
	}
}
#endif // __SYNTH_EDITOR
