// 
// audiosynth/feedbackpinview.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "feedbackpinview.h"
#include "feedbackpin.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"

namespace rage
{

	synthFeedbackPinView::synthFeedbackPinView(synthModule *module) : synthModuleView(module)
	{
		m_ProcessingDirection.AddItem("Input-side First");
		m_ProcessingDirection.AddItem("Output-side First");
		AddComponent(&m_ProcessingDirection);

		m_ProcessingDirection.SetCurrentIndex(GetFeedbackPin()->ProcessInputFirst() ? 0 : 1);
	}

	void synthFeedbackPinView::Update()
	{
		GetFeedbackPin()->SetProcessInputFirst(m_ProcessingDirection.GetCurrentIndex() == 0);
		Matrix34 mat = GetMatrix();
		Vector3 offset = mat.b * 0.5f;
		mat.d += offset;
		m_ProcessingDirection.SetMenuOffset(-offset);
		m_ProcessingDirection.SetMatrix(mat);
		m_ProcessingDirection.SetAlpha(GetHoverFade());
		
		synthModuleView::Update();

		m_TitleLabel.SetShouldDraw(!m_ProcessingDirection.IsShowingMenu());
	}

	bool synthFeedbackPinView::StartDrag() const
	{
		return !GetModule()->GetInput(0).GetView()->IsUnderMouse();
	}
}

#endif // __SYNTH_EDITOR
