// 
// audiosynth/uimenuitem.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UIMENUITEM_H
#define SYNTH_UIMENUITEM_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uilabel.h"

#include "atl/array.h"
#include "vector/vector3.h"

namespace rage
{
	class synthUIMenuView;
	class synthUIMenuItem : public synthUIComponent
	{
	public:
		synthUIMenuItem();
		virtual ~synthUIMenuItem();

		void SetTitle(const char *title)
		{
			formatf(m_Title,title);
			m_Label.SetString(m_Title);
		}

		void SetTag(const u32 tag)
		{
			m_Tag = tag;
		}

		u32 GetTag() const
		{
			return m_Tag;
		}

		f32 GetTextSize() const { return m_TextSize; }
		void SetTextSize(const f32 size) { m_TextSize = size; }

		void SetSubMenu(synthUIMenuView *menu)
		{
			m_SubMenu = menu;
		}

		const char *GetTitle() const { return m_Title; }

		synthUIMenuView *GetSubMenu() const
		{
			return m_SubMenu;
		}

		// synthUIComponent functionality
		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		void SetEdgeColour(const Color32 color)
		{
			m_EdgeColour = color;
		}

		static int CompareAlphabetically(synthUIMenuItem * const*  item1, synthUIMenuItem * const*  item2);

	protected:
		virtual void Render();
	private:

		char m_Title[64];
		Color32 m_EdgeColour;
		synthUIMenuView *m_SubMenu;
		synthUILabel m_Label;
		u32 m_Tag;
		f32 m_TextSize;
	};

} // namespace rage
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIMENUITEM_H
