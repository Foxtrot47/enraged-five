
#include "sampleplayer.h"
#include "sampleplayerview.h"

#if !SYNTH_MINIMAL_MODULES

#include "audioengine/engineutil.h"
#include "audiohardware/device.h"
#include "audiohardware/driver.h"
#include "audiohardware/waveref.h"
#include "audiohardware/waveslot.h"
#include "vectormath/vectormath.h"

namespace rage
{
	SYNTH_EDITOR_VIEW(synthSamplePlayer);

	synthSamplePlayer::synthSamplePlayer() : m_Buffer(false)
	{
		m_Inputs[kSamplePlayerTrigger].Init(this, "kSamplePlayerTrigger",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kSamplePlayerTrigger].SetStaticValue(0.f);

		m_Inputs[kSamplePlayerStartOffset].Init(this, "StartOffset",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kSamplePlayerStartOffset].SetStaticValue(0.f);

		m_Inputs[kSamplePlayerFrequency].Init(this, "Freq",synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kSamplePlayerFrequency].SetStaticValue(0.f);

		m_Outputs[kSamplePlayerSignal].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[kSamplePlayerSignal].SetDataBuffer(&m_Buffer);

		m_Outputs[kSamplePlayerPlaying].Init(this, "kSamplePlayerPlaying", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[kSamplePlayerPlaying].SetStaticValue(0.f);

		m_Outputs[kSamplePlayerPosition].Init(this, "kSamplePlayerPosition", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[kSamplePlayerPosition].SetStaticValue(0.f);

		m_HasStartedPlayback = false;
		m_WasTriggerHigh = false;

		Assign(m_WavePlayerId, audDriver::GetMixer()->AllocatePcmSource_Sync(AUD_PCMSOURCE_WAVEPLAYER));

		SetWave(audWaveSlot::GetWaveSlotIndex(audWaveSlot::FindLoadedBankWaveSlot(audWaveSlot::FindBankId("RESIDENT\\COLLISION"))), 
			ATSTRINGHASH("DROP_1", 0xD5D88420));
		
		m_PlayMode = kOneShot;
	}

	void synthSamplePlayer::SetWave(const u32 waveSlotIndex, const u32 waveNameHash)
	{
		audWaveRef waveRef;
		waveRef.Init(waveSlotIndex, waveNameHash);
		const_cast<audWavePlayer*>(GetWavePlayer())->SetParam(audWavePlayer::Params::WaveReference, waveRef.GetAsU32());
	}

	synthSamplePlayer::~synthSamplePlayer()
	{
		audPcmSourceInterface::Release(m_WavePlayerId);
		m_WavePlayerId = -1;
	}

	void synthSamplePlayer::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeEnumField("PlayMode",m_PlayMode);
	
		// Temporary hack - save slot/wave info
		const audWavePlayer *wavePlayer = (audWavePlayer*)g_PcmSourcePool->GetSlotPtr(m_WavePlayerId);
		u32 slotId = wavePlayer->GetWaveSlotId();
		serializer->SerializeField("SlotId", slotId);
		u32 waveNameHashLow = wavePlayer->GetWaveNameHash()&0xffff;
		u32 waveNameHashHigh = (wavePlayer->GetWaveNameHash()>>16)&0xffff;
		serializer->SerializeField("WaveNameHash_Low", waveNameHashLow);
		serializer->SerializeField("WaveNameHash_High", waveNameHashHigh);
		if(serializer->IsLoading())
		{
			const u32 waveNameHash = waveNameHashLow|(waveNameHashHigh<<16);
			if(audWaveSlot::GetWaveSlotFromIndex(slotId)->GetContainer().IsValid() &&
				audWaveSlot::GetWaveSlotFromIndex(slotId)->GetContainer().FindObject(waveNameHash) != adatContainer::InvalidId)
			{
				SetWave(slotId, waveNameHash);
			}
		}	
	}

	void synthSamplePlayer::Synthesize()
	{
		if(!g_PcmSourcePool->IsSlotInitialised(m_WavePlayerId))
		{
			m_Outputs[kSamplePlayerSignal].SetStaticValue(0.f);
			m_Outputs[kSamplePlayerPlaying].SetStaticValue(0.f);
			return;
		}

		audWavePlayer *wavePlayer = (audWavePlayer*)g_PcmSourcePool->GetSlotPtr(m_WavePlayerId);

		const bool shouldBePlaying = (m_HasStartedPlayback && m_PlayMode==kOneShot) || (m_Inputs[kSamplePlayerTrigger].GetNormalizedValue(0) >= 1.f && wavePlayer->HasValidWave());
		if(shouldBePlaying)
		{
			if(!m_HasStartedPlayback)
			{
				const f32 startOffset = m_Inputs[kSamplePlayerStartOffset].GetNormalizedValue(0);
				const u32 startOffsetSamples = (u32)(startOffset * wavePlayer->GetLengthSamples());
				wavePlayer->SetParam(audWavePlayer::Params::StartOffsetSamples, startOffsetSamples);
		
				wavePlayer->Start();
				wavePlayer->StartPhys(0);
				m_HasStartedPlayback = true;
			}

			wavePlayer->SetParam(audPcmSource::Params::FrequencyScalingFactor, m_Inputs[kSamplePlayerFrequency].GetSignalValue(0) + 1.f);

			const bool hasFinished = wavePlayer->IsFinished();
			m_Buffer.SetBufferId(wavePlayer->GetBufferId(0));

			if(hasFinished)
			{
				wavePlayer->Stop();
				wavePlayer->StopPhys(0);
				m_HasStartedPlayback = false;
				m_Outputs[kSamplePlayerPlaying].SetStaticValue(0.f);
			}
			else
			{
				m_Outputs[kSamplePlayerPlaying].SetStaticValue(1.f);
			}

			m_Outputs[kSamplePlayerPosition].SetStaticValue(wavePlayer->GetPlayPositionSamples() / (f32)wavePlayer->GetLengthSamples());

			m_Outputs[kSamplePlayerSignal].SetDataBuffer(&m_Buffer);
		}
		else
		{
			if(m_HasStartedPlayback)
			{
				wavePlayer->StopPhys(0);
				wavePlayer->Stop();
				m_HasStartedPlayback = false;
			}
			m_Outputs[kSamplePlayerSignal].SetStaticValue(0.f);
			m_Outputs[kSamplePlayerPlaying].SetStaticValue(0.f);
		}
	}

}
#endif
