
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "synthdefs.h"
#include "envelopegenerator.h"
#include "rescaler.h"
#include "synthutil.h"
#include "timedtrigger.h"

#include "audiohardware/driverutil.h"
#include "audiohardware/mixer.h"
#include "system/memops.h"
#include "vectormath/vectormath.h"

#if __SYNTH_EDITOR
#include "egview.h"
#endif

namespace rage
{
using namespace Vec;

SYNTH_EDITOR_VIEW(synthEnvelopeGenerator);

synthEnvelopeGenerator::synthEnvelopeGenerator() 
{
	m_Outputs[kEnvelopeGeneratorEnvelope].Init(this, "Envelope",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
	m_Outputs[kEnvelopeGeneratorEnvelope].SetDataBuffer(&m_Buffer);

	m_Outputs[kEnvelopeGeneratorFinished].Init(this, "Finished",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
	m_Outputs[kEnvelopeGeneratorFinished].SetStaticValue(0.f);

	m_Outputs[kEnvelopeGeneratorAttackActive].Init(this, "AttackActive",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
	m_Outputs[kEnvelopeGeneratorAttackActive].SetStaticValue(0.f);

	m_Outputs[kEnvelopeGeneratorDecayActive].Init(this, "DecayActive",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
	m_Outputs[kEnvelopeGeneratorDecayActive].SetStaticValue(0.f);

	m_Outputs[kEnvelopeGeneratorHoldActive].Init(this, "HoldActive",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
	m_Outputs[kEnvelopeGeneratorHoldActive].SetStaticValue(0.f);

	m_Outputs[kEnvelopeGeneratorReleaseActive].Init(this, "ReleaseActive",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
	m_Outputs[kEnvelopeGeneratorReleaseActive].SetStaticValue(0.f);

	m_Inputs[kEnvelopeGeneratorPredelay].Init(this, "Predelay", synthPin::SYNTH_PIN_DATA_NORMALIZED);
	m_Inputs[kEnvelopeGeneratorPredelay].SetStaticValue(0.f);

	m_Inputs[kEnvelopeGeneratorAttack].Init(this, "Attack", synthPin::SYNTH_PIN_DATA_NORMALIZED);
	m_Inputs[kEnvelopeGeneratorAttack].SetStaticValue(0.2f);

	m_Inputs[kEnvelopeGeneratorDecay].Init(this, "Decay", synthPin::SYNTH_PIN_DATA_NORMALIZED);
	m_Inputs[kEnvelopeGeneratorDecay].SetStaticValue(0.2f);

	m_Inputs[kEnvelopeGeneratorSustain].Init(this, "Sustain", synthPin::SYNTH_PIN_DATA_NORMALIZED);
	m_Inputs[kEnvelopeGeneratorSustain].SetStaticValue(0.5f);
	m_Inputs[kEnvelopeGeneratorHold].Init(this, "Hold", synthPin::SYNTH_PIN_DATA_NORMALIZED);
	m_Inputs[kEnvelopeGeneratorHold].SetStaticValue(0.5f);

	m_Inputs[kEnvelopeGeneratorRelease].Init(this, "Release", synthPin::SYNTH_PIN_DATA_NORMALIZED);
	m_Inputs[kEnvelopeGeneratorRelease].SetStaticValue(0.5f);

	m_Inputs[kEnvelopeGeneratorTrigger].Init(this, "Trigger", synthPin::SYNTH_PIN_DATA_NORMALIZED);
	m_Inputs[kEnvelopeGeneratorTrigger].SetStaticValue(0.f);
	
	m_ReleaseType = Exponential;
	m_TriggerMode = Retrigger;

	ResetProcessing();
}

synthEnvelopeGenerator::~synthEnvelopeGenerator()
{

}

void synthEnvelopeGenerator::ResetProcessing()
{
	m_StateVec = V4VConstant(V_ZERO);
	m_TriggerState = V4VConstant(V_ZERO);
}

void synthEnvelopeGenerator::SerializeState(synthModuleSerializer *serializer)
{
	serializer->SerializeEnumField("ReleaseType", m_ReleaseType);
	serializer->SerializeEnumField("TriggerMode", m_TriggerMode);
}

void synthEnvelopeGenerator::Synthesize()
{
	const float predelay = m_Inputs[kEnvelopeGeneratorPredelay].GetNormalizedValue(0);
	const float attack = m_Inputs[kEnvelopeGeneratorAttack].GetNormalizedValue(0);
	const float decay = m_Inputs[kEnvelopeGeneratorDecay].GetNormalizedValue(0);
	const float sustain = m_Inputs[kEnvelopeGeneratorSustain].GetNormalizedValue(0);
	const float hold = m_Inputs[kEnvelopeGeneratorHold].GetNormalizedValue(0);
	const float release = m_Inputs[kEnvelopeGeneratorRelease].GetNormalizedValue(0);
	const float trigger = m_Inputs[kEnvelopeGeneratorTrigger].GetNormalizedValue(0);

	
	const float finished = Process(m_ReleaseType, m_TriggerMode, m_Buffer.GetBuffer(), m_Buffer.GetSize(), m_StateVec, predelay, attack, decay, sustain, hold, release, trigger);
	m_Outputs[kEnvelopeGeneratorFinished].SetStaticValue(finished);

	float t1 = attack;
	float t2 = decay;
	float t3 = hold;
	float t4 = release;
	ASSERT_ONLY(const float triggerFinished = )
		synthTimedTrigger::Process(m_TriggerMode, trigger, predelay, m_TriggerState, t1, t2, t3, t4);
	Assert(triggerFinished == finished);
	m_Outputs[kEnvelopeGeneratorAttackActive].SetStaticValue(t1);
	m_Outputs[kEnvelopeGeneratorDecayActive].SetStaticValue(t2);
	m_Outputs[kEnvelopeGeneratorHoldActive].SetStaticValue(t3);
	m_Outputs[kEnvelopeGeneratorReleaseActive].SetStaticValue(t4);
	
}

u32 synthEnvelopeGenerator::ComputeRamp(const float lengthSeconds, Vector_4V_InOut perSampleDelta)
{
	// 48kHz
	CompileTimeAssert(kMixerNativeSampleRate == 48000);
	const Vector_4V sampleRateV = V4VConstant<0x473B8000,0x473B8000,0x473B8000,0x473B8000>();

	Vector_4V samplesTilStateChangeUnaligned = V4Max(V4VConstant(V_ZERO), V4Scale(sampleRateV, V4LoadScalar32IntoSplatted(lengthSeconds)));
	// 256 or 128
	CompileTimeAssert(kMixBufNumSamples == 256 || kMixBufNumSamples == 128);
	const Vector_4V frameLengthSamples = V4VConstantSplat<kMixBufNumSamples == 256 ? 0x43800000 : 0x43000000>();

	// align to frame boundary
	const Vector_4V samplesTilStateChange = V4Subtract(samplesTilStateChangeUnaligned, V4Modulus(samplesTilStateChangeUnaligned, frameLengthSamples));

	perSampleDelta = V4Invert(samplesTilStateChange);

	return (u32)GetX(samplesTilStateChange);
}

void synthEnvelopeGenerator::ProcessLinearRamp(float *RESTRICT outputBuffer, const u32 numSamples, Vector_4V_InOut phase, Vector_4V_In perSampleDelta)
{
	// (0,1,2,3)
	const Vector_4V scaleStep = V4VConstant<0,0x3F800000,0x40000000,0x40400000>();

	phase = V4AddScaled(phase, perSampleDelta, scaleStep);
	const Vector_4V phaseStep = V4Scale(perSampleDelta, V4VConstant(V_FOUR));

	Vector_4V *outputPtr = reinterpret_cast<Vector_4V*>(outputBuffer);
	u32 count = numSamples >> 4;
	while(count--)
	{
		const Vector_4V phase0 = phase;
		const Vector_4V phase1 = V4Add(phase, phaseStep);
		const Vector_4V phase2 = V4Add(phase, V4Scale(V4VConstant(V_TWO), phaseStep));
		const Vector_4V phase3 = V4Add(phase, V4Scale(V4VConstant(V_THREE), phaseStep));

		*outputPtr++ = phase0;
		*outputPtr++ = phase1;
		*outputPtr++ = phase2;
		*outputPtr++ = phase3;
		phase = V4Add(phase3, phaseStep);
	}
	phase = V4SplatX(phase);
}

void synthEnvelopeGenerator::ProcessExponentialRelease(float *RESTRICT outputBuffer, Vec::Vector_4V_In releaseStartGain, const u32 numSamples)
{
	Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(outputBuffer);
	

	//const f32 yAtOne = 0.0183156389f;//Powf(M_E, -1.0f * horizontalScaling);
	const Vector_4V yAtOne = V4VConstant<0x3C960AAD,0x3C960AAD,0x3C960AAD,0x3C960AAD>();
	//const f32 oneOverOneMinusyAtOne = 1.f / (1.f - yAtOne);
	//1.0186573603754641701605487662281
	const Vector_4V oneOverOneMinusyAtOne = V4VConstant<0x3F82635D,0x3F82635D,0x3F82635D,0x3F82635D>();
	const Vector_4V e = V4VConstant<0x402DF854,0x402DF854,0x402DF854,0x402DF854>();
	u32 count = numSamples >> 4;
	while(count--)
	{
		const Vector_4V phase0 = *(inOutPtr+0);
		const Vector_4V phase1 = *(inOutPtr+1);
		const Vector_4V phase2 = *(inOutPtr+2);
		const Vector_4V phase3 = *(inOutPtr+3);

		const Vector_4V rescaledX0 = V4Scale(phase0,V4VConstant(V_FOUR));
		const Vector_4V clampedOutput0 = V4Scale(oneOverOneMinusyAtOne, V4Subtract(V4Pow(e, V4Scale(V4VConstant(V_NEGONE), rescaledX0)), yAtOne));

		const Vector_4V rescaledX1 = V4Scale(phase1,V4VConstant(V_FOUR));
		const Vector_4V clampedOutput1 = V4Scale(oneOverOneMinusyAtOne, V4Subtract(V4Pow(e, V4Scale(V4VConstant(V_NEGONE), rescaledX1)), yAtOne));

		const Vector_4V rescaledX2 = V4Scale(phase2,V4VConstant(V_FOUR));
		const Vector_4V clampedOutput2 = V4Scale(oneOverOneMinusyAtOne, V4Subtract(V4Pow(e, V4Scale(V4VConstant(V_NEGONE), rescaledX2)), yAtOne));

		const Vector_4V rescaledX3 = V4Scale(phase3,V4VConstant(V_FOUR));
		const Vector_4V clampedOutput3 = V4Scale(oneOverOneMinusyAtOne, V4Subtract(V4Pow(e, V4Scale(V4VConstant(V_NEGONE), rescaledX3)), yAtOne));

		*inOutPtr++ = V4Scale(releaseStartGain, clampedOutput0);
		*inOutPtr++ = V4Scale(releaseStartGain, clampedOutput1);
		*inOutPtr++ = V4Scale(releaseStartGain, clampedOutput2);
		*inOutPtr++ = V4Scale(releaseStartGain, clampedOutput3);
	}
}

float synthEnvelopeGenerator::Process(const synthEnvelopeReleaseType releaseMode,
										const synthEnvelopeTriggerMode triggerMode,
										float *RESTRICT const outputBuffer, 
										const u32 numSamples, 
										Vec::Vector_4V_InOut state, 
										const float predelay, 
										const float attack, 
										const float decay, 
										const float sustain, 
										const float hold, 
										const float release, 
										const float trigger)
{
	
	const u32 packedVals = (u32)GetXi(state);
	// low 8 bits for state, upper 24 for samplesTilStateChange
	synthEnvelopeGeneratorState envState = (synthEnvelopeGeneratorState)(packedVals&0xff);
	u32 samplesTilStateChange = packedVals>>8;
	
	Vector_4V releaseStartGain = V4SplatY(state);
	Vector_4V phase = V4SplatZ(state);	
	Vector_4V phaseStep = V4SplatW(state);

	bool changeThisFrame = samplesTilStateChange < numSamples;
	
	bool needSilence = true;

	float finished = 0.0f;

	if(triggerMode == Interruptible && envState != IDLE_STATE)
	{
		if(trigger >= 1.f)
		{
			// The check in the IDLE state below will catch the same trigger so there will be no delay in
			// restarting the envelope
			envState = IDLE_STATE;
		}
	}

	switch(envState)
	{
	case IDLE_STATE:
		if(trigger >= 1.f)
		{
			samplesTilStateChange = u32(Max(0.f, predelay) * kMixerNativeSampleRate);
			
			envState = PREDELAY_STATE;
			changeThisFrame = samplesTilStateChange < numSamples;
			// Intentional fall-through
		}
		else
		{
			// Silent output while waiting on trigger
			break;
		}
	case PREDELAY_STATE:
		if(!changeThisFrame)
		{
			break;
		}
		else
		{
			samplesTilStateChange = ComputeRamp(attack, phaseStep);
			phase = V4VConstant(V_ZERO);

			envState = ATTACK_STATE;
			changeThisFrame = samplesTilStateChange < numSamples;
		}
		// Intentional fall-through
	case ATTACK_STATE:
		if(!changeThisFrame)
		{
			// generate linear attack curve
			ProcessLinearRamp(outputBuffer, numSamples, phase, phaseStep);
			needSilence = false;
			break;
		}
		else
		{
			samplesTilStateChange = ComputeRamp(decay, phaseStep);
			phase = V4VConstant(V_ZERO);
			envState = DECAY_STATE;
			changeThisFrame = samplesTilStateChange < numSamples;
		}
		// Intentional fall-through		
	case DECAY_STATE:
		if(!changeThisFrame)
		{
			// Generate linear (increasing) ramp, then lerp that
			ProcessLinearRamp(outputBuffer, numSamples, phase, phaseStep);
			synthRescaler::Process(outputBuffer, 1.f, sustain, numSamples);
			needSilence = false;
			break;
		}
		else
		{
			envState = INF_HOLD_STATE;
		}
		// Intentional fall-through
	case INF_HOLD_STATE:

		if(hold >= 0.f)
		{
			samplesTilStateChange = ComputeRamp(hold, phaseStep);
			phase = V4VConstant(V_ZERO);

			envState = HOLD_STATE;
			changeThisFrame = samplesTilStateChange < numSamples;
		}
		else
		{
			// Infinite hold
			synthUtil::HoldSample(outputBuffer, sustain, numSamples);
			needSilence = false;
			break;
		}
		// Intentional fall-through
	case HOLD_STATE:

		if(!changeThisFrame)
		{
			synthUtil::HoldSample(outputBuffer, sustain, numSamples);
			needSilence = false;
			break;
		}
		else
		{
			samplesTilStateChange = ComputeRamp(release, phaseStep);
			phase = V4VConstant(V_ZERO);

			envState = RELEASE_STATE;
			changeThisFrame = samplesTilStateChange < numSamples;

			releaseStartGain = V4LoadScalar32IntoSplatted(sustain);
		}
		// intentional fall-through
	case RELEASE_STATE:
		if(!changeThisFrame)
		{
			ProcessLinearRamp(outputBuffer, numSamples, phase, phaseStep);
			
			if(releaseMode == Linear)
			{
				synthRescaler::Process(outputBuffer, GetX(releaseStartGain), 0.0f, numSamples);
			}
			else
			{
				ProcessExponentialRelease(outputBuffer, releaseStartGain, numSamples);
			}
			needSilence = false;
			break;
		}
		else
		{	
			envState = FINISHED_STATE;
		}
		// Intentional fall-through
	case FINISHED_STATE:
		if(triggerMode == Retrigger || triggerMode == Interruptible)
		{
			envState = IDLE_STATE;
		}
		else
		{
			// Not in constant trigger mode so wait for trigger signal to go low before
			// retriggering	
			if(trigger < 1.f)
			{
				envState = IDLE_STATE;
			}
		}
		finished = true;
		break;
	}

	samplesTilStateChange -= numSamples;
	
	if(needSilence)
	{
#if __SPU
		sysMemSet(outputBuffer, 0, sizeof(float) * numSamples);
#else
		sysMemSet128(outputBuffer, 0, sizeof(float) * numSamples);
#endif
	}

		
		
	// low 8 bits for state, upper 24 for samplesTilStateChange
	u32 newPackedVals = (envState&0xff) | (samplesTilStateChange<<8);
	state = V4LoadScalar32IntoSplatted(newPackedVals);

	Vector_4V phaseAndStep = V4PermuteTwo<X1,X2,X1,X2>(phase,phaseStep);
	state = V4PermuteTwo<X1,Y2,Z1,W1>(state, releaseStartGain);
	state = V4PermuteTwo<X1,Y1,X2,Y2>(state, phaseAndStep);

	return finished;
}

}
#endif
