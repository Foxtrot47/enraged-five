#include "multitapdelay.h"

#include "math/amath.h"
#include "system/memops.h"

namespace rage
{	
	synthMultiTapDelay::synthMultiTapDelay()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_OutputBuffer);
		sysMemSet128(m_OutputBuffer.GetBuffer(), 0, m_OutputBuffer.GetSize() * sizeof(f32));

		m_DelayBuffer = rage_aligned_new(16) f32[MAX_DELAY_SAMPLES];
		sysMemSet(m_DelayBuffer, 0, MAX_DELAY_SAMPLES * sizeof(f32));
		
		m_Inputs[kMultitapSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kMultitapTap1].Init(this, "Tap1", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap1].SetStaticValue(1.f);
		m_Inputs[kMultitapTap1_Gain].Init(this, "Tap1_Gain", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap1_Feedback].Init(this, "Tap1_Feedback", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap2].Init(this, "Tap2", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap2].SetStaticValue(1.f);
		m_Inputs[kMultitapTap2_Gain].Init(this, "Tap2_Gain", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap2_Feedback].Init(this, "Tap2_Feedback", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap3].Init(this, "Tap3", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap3].SetStaticValue(1.f);
		m_Inputs[kMultitapTap3_Gain].Init(this, "Tap3_Gain", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap3_Feedback].Init(this, "Tap3_Feedback", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap4].Init(this, "Tap4", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap4].SetStaticValue(1.f);
		m_Inputs[kMultitapTap4_Gain].Init(this, "Tap4_Gain", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kMultitapTap4_Feedback].Init(this, "Tap4_Feedback", synthPin::SYNTH_PIN_DATA_NORMALIZED);

		m_DelayWriteIndex = 0;
		m_LengthSamples = MAX_DELAY_SAMPLES;

	}

	synthMultiTapDelay::~synthMultiTapDelay()
	{
		delete[] m_DelayBuffer;
		m_DelayBuffer = NULL;
	}

	float synthMultiTapDelay::ReadTap(const u32 offset)
	{
		return *(m_DelayBuffer + ((m_DelayWriteIndex + m_LengthSamples - offset) % m_LengthSamples));
	}

	void synthMultiTapDelay::Synthesize()
	{
		const u32 numTaps = 4;

		for(u32 i = 0; i < m_OutputBuffer.GetSize(); i++)
		{
			float output = 0.f;
			float feedback = 0.f;
			const float input = m_Inputs[kMultitapSignal].GetSignalValue(i);

			for(u32 tapIndex = 0; tapIndex < numTaps; tapIndex++)
			{
				const u32 tapOffset = (u32)(m_Inputs[kMultitapTap1 + tapIndex*3].GetNormalizedValue(i) * m_LengthSamples);
				const float tapOutput = ReadTap(tapOffset);
				output += tapOutput * m_Inputs[kMultitapTap1_Gain + tapIndex*3].GetNormalizedValue(i);
				feedback += tapOutput * m_Inputs[kMultitapTap1_Feedback + tapIndex*3].GetNormalizedValue(i);
			}

			m_OutputBuffer.GetBuffer()[i] = output;
			m_DelayBuffer[m_DelayWriteIndex] = feedback + input;
			m_DelayWriteIndex = (m_DelayWriteIndex+1) % m_LengthSamples;
		}
	}
}
