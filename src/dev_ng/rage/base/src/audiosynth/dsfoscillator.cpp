
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include <math.h>
#include "math/amath.h"
#include "audiohardware/mixer.h"
#include "dsfoscillator.h"
#include "oscillatorview.h"

#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

#if __SYNTH_EDITOR
	char *synthAnalogueOscillator::ModeStrings[NUM_OSCILLATOR_MODES] =
	{
		"IMPULSE TRAIN",
		"SQUARE",
		"SAW"
	};
	const char *synthAnalogueOscillator::GetModeName(const synthAnalogueOscillatorMode mode) const
	{
		return ModeStrings[mode];
	}
#endif
	SYNTH_EDITOR_LINK_VIEW(synthAnalogueOscillator, synthOscillatorView);

	synthAnalogueOscillator::synthAnalogueOscillator() 
	{
		m_Inputs[kAnalogueOscillatorAmplitude].Init(this, "Amplitude",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAnalogueOscillatorAmplitude].SetStaticValue(1.f);
		m_Inputs[kAnalogueOscillatorFrequency].Init(this, "Frequency",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAnalogueOscillatorFrequency].SetStaticValue(0.f);
		m_Inputs[kAnalogueOscillatorPhase].Init(this, "Phase",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAnalogueOscillatorPhase].SetStaticValue(0.f);
		m_Inputs[kAnalogueOscillatorHardSync].Init(this, "HardSync", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kAnalogueOscillatorHardSync].SetStaticValue(0.f);

		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);

		m_FrequencyRange[kMinFreq] = 0.f;
		m_FrequencyRange[kMaxFreq] = 20000.f;

		m_Mode = IMPULSE_TRAIN;
		m_Phase = V4VConstant(V_ZERO);
		m_LastOut = V4VConstant(V_ZERO);
		m_LastBlitOut = V4VConstant(V_ZERO);
	}

	synthAnalogueOscillator::~synthAnalogueOscillator()
	{

	}

	void synthAnalogueOscillator::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("MinFreq", m_FrequencyRange[kMinFreq]);
		serializer->SerializeField("MaxFreq", m_FrequencyRange[kMaxFreq]);
		serializer->SerializeEnumField("Mode", m_Mode);
	}

	void synthAnalogueOscillator::Synthesize()
	{	
		// 24000
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const Vector_4V nyquist = V4VConstant<0x46BB8000,0x46BB8000,0x46BB8000,0x46BB8000>();
		// 1/48kHz
		const Vector_4V oneOverFs = V4VConstant<0x37AEC33E,0x37AEC33E,0x37AEC33E,0x37AEC33E>();

		Vector_4V amp = V4VConstant(V_ZERO);

		const Vector_4V freqStorage = *((Vector_4V*)&m_FrequencyRange[0]);
		const Vector_4V minFreq = V4SplatX(freqStorage);
		const Vector_4V maxFreq = V4SplatY(freqStorage);
		const Vector_4V freqDelta = V4Subtract(maxFreq,minFreq);

		Vector_4V freq = V4Add(minFreq, V4Scale(freqDelta,m_Inputs[kAnalogueOscillatorFrequency].GetNormalizedValueV(0)));
		Vector_4V phaseStep =  V4Scale(freq, oneOverFs);

		Vector_4V phase = m_Phase;

		const bool isHardSynced = (m_Inputs[kAnalogueOscillatorHardSync].GetNormalizedValue(0) >= 1.f);
		if(isHardSynced)
		{
			m_Phase = phase = V4VConstant(V_ZERO);
		}
		if(isHardSynced)
		{
			// apply phase offset
			Vector_4V phaseOffset = V4SplatX(m_Inputs[kAnalogueOscillatorPhase].GetNormalizedValueV(0));

			Vector_4V phaseToUse;

			if(m_Inputs[kAnalogueOscillatorPhase].IsConnected())
			{
				// disable internal phase and use the provided phase directly
				phaseToUse = phaseOffset;
			}
			else
			{
				// phase is an offset
				phaseToUse = V4Add(phaseOffset, phase);
			}

			// need to correct any overflow - this is unfortunately necessary
			const Vector_4V phaseOffsetOverflowCorrection = V4RoundToNearestIntZero(phaseToUse);
			phaseToUse = V4Subtract(phaseToUse, phaseOffsetOverflowCorrection);

			// generate one sample
			const Vector_4V output = V4Scale(m_Inputs[kAnalogueOscillatorAmplitude].GetNormalizedValueV(0),ComputeSampleV(phaseToUse, freq));

			m_Outputs[0].SetStaticValue(GetX(output));
			// step phase on by mixbufnumsamples
			// 256 or 128
			CompileTimeAssert(kMixBufNumSamples == 256 || kMixBufNumSamples == 128);
			const Vector_4V mixBufNumSamples = V4VConstantSplat<kMixBufNumSamples == 256 ? 0x43800000 : 0x43000000>();

			if(!isHardSynced && !m_Inputs[kAnalogueOscillatorPhase].IsConnected())
			{
				phase = V4Add(phase, V4Scale(phaseStep, mixBufNumSamples));
				// overflow correction
				const Vector_4V phaseOverflowCorrection = V4RoundToNearestIntZero(phase);
				phase = V4Subtract(phase, phaseOverflowCorrection);
				m_Phase = V4SplatX(phase);
			}
		}
		else
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			bool isPhaseOffset = false;
			Vector_4V phaseOffset = V4VConstant(V_ZERO);

			if(!m_Inputs[kAnalogueOscillatorPhase].IsConnected())
			{
				// phase pin as offset
				const f32 phaseOffsetScalar = m_Inputs[kAnalogueOscillatorPhase].GetStaticValue();
				if(phaseOffsetScalar > 0.f)
				{
					phaseOffset = V4LoadScalar32IntoSplatted(phaseOffsetScalar);
					isPhaseOffset = true;
				}
			}


			for(u32 i = 0 ; i < m_Outputs[0].GetDataBuffer()->GetSize(); i += 4)
			{
				Vector_4V output = V4VConstant(V_ZERO);

				// calculate phase for these 4 samples
				if(Likely(!m_Inputs[kAnalogueOscillatorPhase].IsConnected()))
				{
					if(Unlikely(m_Inputs[kAnalogueOscillatorFrequency].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC))
					{
						// Frequency Modulation
						freq = V4Add(minFreq, V4Scale(freqDelta,m_Inputs[kAnalogueOscillatorFrequency].GetNormalizedValueV(i)));
						freq = V4Clamp(freq, V4VConstant(V_ZERO), nyquist);
						phaseStep =  V4Scale(freq, oneOverFs);
					}

					Vector_4V phase0 = V4Add(V4SplatW(phase),V4SplatX(phaseStep));
					Vector_4V phase1 = V4Add(phase0, V4SplatY(phaseStep));
					Vector_4V phase2 = V4Add(phase1, V4SplatZ(phaseStep));
					Vector_4V phase3 = V4Add(phase2, V4SplatW(phaseStep));

					Vector_4V phase01 = V4PermuteTwo<X1,X2,X1,X2>(phase0,phase1);
					Vector_4V phase23 =  V4PermuteTwo<X1,X2,X1,X2>(phase2,phase3);
					phase = V4PermuteTwo<X1,Y1,X2,Y2>(phase01,phase23);
				}
				else
				{	
					phase = m_Inputs[kAnalogueOscillatorPhase].GetNormalizedValueV(i);
				}

				// handle phase overflow
				const Vector_4V phaseOverflowCorrection = V4RoundToNearestIntZero(phase);
				phase = V4Subtract(phase, phaseOverflowCorrection);

				Vector_4V offsetPhase = phase;
				if(isPhaseOffset)
				{
					// use the phase pin to offset computed (internal) phase
					offsetPhase = V4Add(phase, phaseOffset);
					const Vector_4V offsetPhaseOverflowCorrection = V4RoundToNearestIntZero(offsetPhase);
					offsetPhase = V4Subtract(offsetPhase, offsetPhaseOverflowCorrection);
				}				

				// compute 4 sample values
				output = ComputeSampleV(offsetPhase,freq);
				amp = m_Inputs[kAnalogueOscillatorAmplitude].GetNormalizedValueV(i);
				*((Vector_4V*)&m_Outputs[0].GetDataBuffer()->GetBuffer()[i]) = V4Scale(output, amp);
			}

			m_Phase = phase;
		}
	}	

	Vector_4V synthAnalogueOscillator::ComputeSampleV(const Vec::Vector_4V_In phase, const Vec::Vector_4V_In freq)
	{
		const Vec::Vector_4V scaledPhase = Vec::V4Scale(phase,Vec::V4VConstant(V_PI));
		const Vec::Vector_4V nyquist = Vec::V4VConstant<0x46BB8000,0x46BB8000,0x46BB8000,0x46BB8000>();
		const Vec::Vector_4V m = Vec::V4AddScaled(Vec::V4VConstant(V_ONE),Vec::V4RoundToNearestIntNegInf(Vec::V4InvScale(nyquist, freq)),Vec::V4VConstant(V_TWO));
		CompileTimeAssert(kMixerNativeSampleRate == 48000);
		const Vec::Vector_4V epsilonVal = Vec::V4VConstant<0x358637BD,0x358637BD,0x358637BD,0x358637BD>();

		const Vec::Vector_4V denominator = Vec::V4SinFast(scaledPhase);
		// SinFast only works in range -PI,PI, so we'd need to wrap the phase here
		Vec::Vector_4V temp = Vec::V4Sin(Vec::V4Scale(m,scaledPhase));
		temp = Vec::V4InvScale(temp, Vec::V4Scale(m,denominator));

		const Vec::Vector_4V mask = Vec::V4IsGreaterThanV(Vec::V4Abs(denominator),epsilonVal);
		Vector_4V blitOutput = Vec::V4SelectFT(mask, Vec::V4VConstant(V_ONE), temp);
		// 0.999
		//const Vector_4V leakyIntegrator = V4VConstant<0x3F7FBE76,0x3F7FBE76,0x3F7FBE76,0x3F7FBE76>();

		Vector_4V output;

		switch(m_Mode)
		{
		case IMPULSE_TRAIN:
			{				
				// calculated above
				output = blitOutput;
			}
			break;
		case SQUARE:
			{
				//Vector_4V lastBlitOut = m_LastBlitOut;
				//Vector_4V dcbState = lastBlitOut;
				//Vector_4V lastOut = m_LastOut;
				//
				//const Vec::Vector_4V denominator = Vec::V4SinFast(scaledPhase);




				//if ( fabs( denominator )  < std::numeric_limits<StkFloat>::epsilon() ) {
				//	// Inexact comparison safely distinguishes betwen *close to zero*, and *close to PI*.
				//	if ( phase_ < 0.1f || phase_ > TWO_PI - 0.1f )
				//		lastBlitOutput_ = a_;
				//	else
				//		lastBlitOutput_ = -a_;
				//}
				//else {
				//	lastBlitOutput_ =  rage::Sinf( m_ * phase_ );
				//	lastBlitOutput_ /= p_ * denominator;
				//}

				//// serialise four samples worth

				//// lastBlitOutput_ += temp;
				//lastBlitOut = V4Add(V4SplatX(temp), lastBlitOut);

				//// Now apply DC blocker.
				////lastFrame_[0] = lastBlitOutput_ - dcbState_ + 0.999 * lastFrame_[0];
				////dcbState_ = lastBlitOutput_;
				//const Vector_4V outX = V4Subtract(lastBlitOut,V4AddScaled(dcbState, lastOut, leakyIntegrator));

				//dcbState = lastBlitOut;

				f32 yn_1 = GetX(m_LastBlitOut);
				f32 dcbState = yn_1;
				f32 lastSampleOutput = GetX(m_LastOut);

				f32 lastBlitOutput;

				f32 p = 0.5f * kMixerNativeSampleRate / GetX(freq);
				f32 a = GetX(m) / p;

				ALIGNAS(16) f32 phaseArray[4] ;
				//ALIGNAS(16) f32 freqArray[4] ;
				ALIGNAS(16) f32 outputArray[4] ;

				*((Vector_4V*)&phaseArray) = phase;
				//*((Vector_4V*)&freqArray) = freq;

				for(u32 i = 0; i < 4; i++)
				{
					f32 denom = rage::Sinf( phaseArray[i] );
					
					if ( fabs( denom )  < 0.0001f ) 
					{
						// Inexact comparison safely distinguishes between *close to zero*, and *close to PI*.
						if ( phaseArray[i] < 0.1f || phaseArray[i] > 2.f*PI - 0.1f )
							lastBlitOutput = a;
						else
							lastBlitOutput = -a;
					}
					else
					{
						lastBlitOutput =  rage::Sinf( GetX(m) * phaseArray[i] );
						lastBlitOutput /= p * denom;
					}

					lastBlitOutput += yn_1;

					// Now apply DC blocker.
					lastSampleOutput = outputArray[i] = lastBlitOutput - dcbState + 0.999f * lastSampleOutput;
					dcbState = lastBlitOutput;
				}

	
				m_LastOut = V4LoadScalar32IntoSplatted(lastSampleOutput);
				m_LastBlitOut = V4LoadScalar32IntoSplatted(lastBlitOutput);

				output = *(Vector_4V*)&outputArray;

			}
			break;
		default:
			output = V4VConstant(V_ZERO);
			break;
		}

		return output;
	}
}

#endif
