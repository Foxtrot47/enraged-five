// 
// audiosynth/midiccinview.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_MIDICCINVIEW_H
#define SYNTH_MIDICCINVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "moduleview.h"
#include "uibutton.h"
#include "uihorizontallayout.h"
#include "uilabel.h"
#include "uiscrollbar.h"
#include "uidropdownlist.h"

namespace rage
{

	class synthMidiCCIn;
	class synthMidiCCInView : public synthModuleView
	{
	public:

		synthMidiCCInView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

        virtual u32 GetModuleHeight() const
        {
            return 3U;
        }

	protected:
		virtual void Render();

	private:

		synthMidiCCIn *GetMidiCCIn() const {return (synthMidiCCIn*)m_Module; }

		
		synthUIScrollbar m_ControllerSlider;
		synthUIDropDownList m_ChannelList;
		synthUILabelView m_DeviceNameLabel;
        synthUIButton m_LearnButton;
        synthUIHorizontalLayout m_Layout;
		synthUIDropDownList m_PitchBendSwitch;
        bool m_WasLearning;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_MIDICCINVIEW_H

