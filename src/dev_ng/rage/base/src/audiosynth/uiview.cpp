#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uiview.h"
#include "modulegroup.h"
#include "synthesizerview.h"
#include "uiplane.h"
#include "uicomponent.h"
#include "uiplane.h"
#include "vector/geometry.h"
#include "system/timemgr.h"

namespace rage
{
    synthUIView::~synthUIView()
    {
        if(IsGrouped())
        {
            GetGroup()->Remove(this);
        }
    }

	void synthUIView::Update()
	{
		Approach(m_MouseHoverFade, ((IsUnderMouse() || IsActive()) && !DisableInteraction() ? 1.f : 0.f), 4.f, TIME.GetElapsedTime()-TIME.GetPrevElapsedTime());
		
		Vector3 mousePos = synthUIPlane::GetActive()->GetMousePosition();

		bool underMouse = false;
		for(atArray<synthUIComponent*>::iterator i = m_Components.begin(); i != m_Components.end(); i++)
		{
			Vector3 v1,v2;
			(*i)->GetBounds(v1,v2);
			const Matrix34 &mat = (*i)->GetMatrix();
			Vector3 absFwd = mat.c;
			absFwd.Abs();
			v1 -= absFwd;
			v2 += absFwd;

			if((*i)->ShouldDraw() && !underMouse && geomPoints::IsPointInBox(mousePos,v1,v2))
			{
				underMouse = true;
				(*i)->SetUnderMouse(IsUnderMouse());

				const float relX = 1.f - ((mousePos.x - v1.x) / (v2.x - v1.x));
				const float relY = (mousePos.y - v1.y) / (v2.y - v1.y);
				(*i)->SetRelativeMousePos(relX,relY);
			}
			else
			{
				(*i)->SetUnderMouse(false);
			}

			(*i)->Update();
		}
	}

	void synthUIView::Render()
	{
		for(atArray<synthUIComponent*>::iterator i = m_Components.begin(); i != m_Components.end(); i++)
		{
			(*i)->Draw();
		}
	}

    void synthUIView::GetBounds(Vector3 &v1, Vector3 &v2) const
    {
        if(GetComponents_const().size() == 0)
        {
            v1.Zero();
            v2.Zero();
        }
        else
        {
            v1.Set(LARGE_FLOAT);
            v2.Set(-LARGE_FLOAT);
    
            for(atArray<synthUIComponent*>::const_iterator iter = GetComponents_const().begin(); iter != GetComponents_const().end(); iter++)
            {
                Vector3 v3,v4;
                (*iter)->GetBounds(v3,v4);
                v1.Min(v1,v3);
                v2.Max(v2,v4);
            }
        }
    }

	void synthUIView::AddToGroup(synthUIViewGroup *group) 
	{ 
		Assert(m_Group == NULL); 
		m_Group = group; 
	}

	void synthUIView::RemoveFromGroup(const synthUIViewGroup *ASSERT_ONLY(group)) 
	{ 
		Assert(m_Group == group);
		m_Group = NULL;
	}

	synthUIViewGroup *synthUIView::GetGroup() const
	{
		return m_Group;
	}
}
#endif // __SYNTH_EDITOR
