
#include "abs.h"
#include "gate.h"
#include "synthutil.h"

#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	synthGate::synthGate()
	{
		m_Outputs[kGateOutputGatedSignal].Init(this,"GatedSignal", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[kGateOutputGateValue].Init(this, "GateVal", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);

		m_Inputs[kGateInputSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kGateInputThreshold].Init(this, "Threshold", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kGateInputThreshold].SetStaticValue(0.f);
		m_Outputs[kGateOutputGatedSignal].SetDataBuffer(&m_GatedSignalBuffer);
		m_Outputs[kGateOutputGateValue].SetDataBuffer(&m_GateBuffer);
	}

	void synthGate::Synthesize()
	{

		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			const float input = m_Inputs[kGateInputSignal].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED ?
																					m_Inputs[kGateInputSignal].GetStaticValue() : 
																					Abs(m_Inputs[kGateInputSignal].GetStaticValue());

			const float gateVal = Process(input, m_Inputs[kGateInputThreshold].GetNormalizedValue(0));

			m_Outputs[kGateOutputGateValue].SetStaticValue(gateVal);
			m_Outputs[kGateOutputGatedSignal].SetStaticValue(gateVal * m_Inputs[kGateInputSignal].GetStaticValue());
			m_Outputs[kGateOutputGatedSignal].SetDataFormat(m_Inputs[kGateOutputGatedSignal].GetDataFormat());
		}
		else
		{
			m_Outputs[kGateOutputGateValue].SetDataBuffer(&m_GateBuffer);
			sysMemCpy(m_GateBuffer.GetBuffer(), m_Inputs[kGateInputSignal].GetDataBuffer()->GetBuffer(), kMixBufNumSamples * sizeof(float));
			if(m_Inputs[kGateInputSignal].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
			{
				synthAbs::Process(m_GateBuffer.GetBuffer(), kMixBufNumSamples);
			}

			if(m_Inputs[kGateInputThreshold].GetDataState() == synthPin::SYNTH_PIN_STATIC)
			{
				Process(
					m_GateBuffer.GetBuffer(),
					m_Inputs[kGateInputThreshold].GetNormalizedValueV(0), 
					kMixBufNumSamples
					);
			}
			else
			{
				ALIGNAS(16) float tempBuf[kMixBufNumSamples] ;
				sysMemCpy(tempBuf, m_Inputs[kGateInputThreshold].GetDataBuffer()->GetBuffer(), kMixBufNumSamples * sizeof(float));

				if(m_Inputs[kGateInputThreshold].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
				{
					synthUtil::ConvertBufferToNormalised(tempBuf, kMixBufNumSamples);
				}
				Process(m_GateBuffer.GetBuffer(), tempBuf, kMixBufNumSamples);
			}

			m_Outputs[kGateOutputGatedSignal].SetDataBuffer(&m_GatedSignalBuffer);
			m_Outputs[kGateOutputGatedSignal].SetDataFormat(m_Inputs[kGateInputSignal].GetDataFormat());

			sysMemCpy(m_GatedSignalBuffer.GetBuffer(), m_GateBuffer.GetBuffer(), sizeof(float) * kMixBufNumSamples);

			synthUtil::ScaleBuffer(m_GatedSignalBuffer.GetBuffer(), 
									m_Inputs[kGateInputSignal].GetDataBuffer()->GetBuffer(), 
									kMixBufNumSamples);
		}

		/*if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_STATIC && m_Inputs[1].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			if(Likely(m_Inputs[kGateOutputGatedSignal].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL))
			{
				const f32 gateValue = Abs(m_Inputs[kGateInputSignal].GetSignalValue(0)) >= m_Inputs[kGateInputThreshold].GetNormalizedValue(0) ? 1.f : 0.f;
				m_Outputs[kGateOutputGateValue].SetStaticValue(gateValue);
				m_Outputs[kGateOutputGatedSignal].SetStaticValue(gateValue * m_Inputs[kGateInputSignal].GetSignalValue(0));
				m_Outputs[kGateOutputGatedSignal].SetDataFormat(synthPin::SYNTH_PIN_DATA_SIGNAL);
			}
			else
			{
				const f32 gateValue = m_Inputs[kGateInputSignal].GetNormalizedValue(0) >= m_Inputs[kGateInputThreshold].GetNormalizedValue(0) ? 1.f : 0.f;
				m_Outputs[kGateOutputGateValue].SetStaticValue(gateValue);
				m_Outputs[kGateOutputGatedSignal].SetStaticValue(gateValue * m_Inputs[kGateInputSignal].GetNormalizedValue(0));
				m_Outputs[kGateOutputGatedSignal].SetDataFormat(synthPin::SYNTH_PIN_DATA_NORMALIZED);
			}			
		}
		else
		{
			m_Outputs[kGateOutputGateValue].SetDataBuffer(&m_GateBuffer);
			m_Outputs[kGateOutputGatedSignal].SetDataBuffer(&m_GatedSignalBuffer);

			// vectorized gate

			f32 *signalOutput = m_Outputs[kGateOutputGatedSignal].GetDataBuffer()->GetBuffer();
			f32 *gateOutput = m_Outputs[kGateOutputGateValue].GetDataBuffer()->GetBuffer();

			if(Likely(m_Inputs[kGateOutputGatedSignal].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL))
			{
				m_Outputs[kGateOutputGatedSignal].SetDataFormat(synthPin::SYNTH_PIN_DATA_SIGNAL);
				for(u32 i = 0; i < m_Outputs[kGateOutputGateValue].GetDataBuffer()->GetSize(); i += 4)
				{
					const Vector_4V inputV = m_Inputs[kGateInputSignal].GetSignalValueV(i);
					const Vector_4V thresholdV = m_Inputs[kGateInputThreshold].GetNormalizedValueV(i);

					const Vector_4V gateMask = V4IsGreaterThanOrEqualV(V4Abs(inputV),thresholdV);
					const Vector_4V gateValue = V4SelectFT(gateMask, V4VConstant(V_ZERO), V4VConstant(V_ONE));

					*((Vector_4V*)signalOutput) = V4Scale(gateValue, inputV);
					*((Vector_4V*)gateOutput) = gateValue;

					signalOutput += 4;
					gateOutput += 4;
				}
			}
			else
			{
				m_Outputs[kGateOutputGatedSignal].SetDataFormat(synthPin::SYNTH_PIN_DATA_NORMALIZED);
				for(u32 i = 0; i < m_Outputs[kGateOutputGateValue].GetDataBuffer()->GetSize(); i += 4)
				{
					const Vector_4V inputV = m_Inputs[kGateInputSignal].GetNormalizedValueV(i);
					const Vector_4V thresholdV = m_Inputs[kGateInputThreshold].GetNormalizedValueV(i);

					const Vector_4V gateMask = V4IsGreaterThanOrEqualV(inputV,thresholdV);
					const Vector_4V gateValue = V4SelectFT(gateMask, V4VConstant(V_ZERO), V4VConstant(V_ONE));

					*((Vector_4V*)signalOutput) = V4Scale(gateValue, inputV);
					*((Vector_4V*)gateOutput) = gateValue;

					signalOutput += 4;
					gateOutput += 4;
				}
			}
		}*/
	}

	void synthGate::Process(float *inOut, const float *threshold, const u32 numSamples)
	{
		u32 count = numSamples>>2;
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOut);
		const Vector_4V *thresholdPtr = reinterpret_cast<const Vector_4V*>(threshold);

		while(count--)
		{
			const Vector_4V inputV = *inOutPtr;
			const Vector_4V thresholdV = *thresholdPtr++;

			const Vector_4V gateMask = V4IsGreaterThanOrEqualV(inputV,thresholdV);
			const Vector_4V gateValue = V4SelectFT(gateMask, V4VConstant(V_ZERO), V4VConstant(V_ONE));

			*inOutPtr++ = gateValue;
		}
	}

	float synthGate::Process(const float input, const float threshold)
	{
		return input >= threshold ? 1.f : 0.f;
	}

	void synthGate::Process(float *inOut, const Vec::Vector_4V_In thresholdV, const u32 numSamples)
	{
		u32 count = numSamples>>2;
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOut);
		
		while(count--)
		{
			const Vector_4V inputV = *inOutPtr;

			const Vector_4V gateMask = V4IsGreaterThanOrEqualV(inputV,thresholdV);
			const Vector_4V gateValue = V4SelectFT(gateMask, V4VConstant(V_ZERO), V4VConstant(V_ONE));

			*inOutPtr++ = gateValue;
		}
	}

} // namespace rage



