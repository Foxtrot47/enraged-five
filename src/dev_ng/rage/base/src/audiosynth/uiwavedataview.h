// 
// audiosynth/uiwavedataview.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UIWAVEDATAVIEW_H
#define SYNTH_UIWAVEDATAVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uiview.h"
#include "uilabel.h"
#include "vector/vector3.h"

namespace rage
{
    class grcTexture;
    class synthUIWaveDataView : public synthUIView
    {
    public:

        synthUIWaveDataView();
        virtual ~synthUIWaveDataView();

        virtual void Update();
        virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

        virtual bool IsActive() const { return IsUnderMouse(); }

        virtual bool StartDrag() const { return false; }


        void SetWaveData(const float *waveData, const u32 lengthSamples)
        {
            m_WaveData = waveData;
            m_WaveLengthSamples = lengthSamples;
        }

        void SetSize(const float width, const float height)
        {
            m_Width = width;
            m_Height = height;
        }

        void SetUpdateRegion(const s32 startSample, const s32 endSample)
        {
            m_UpdateRegionStart = startSample;
            m_UpdateRegionEnd = endSample;
        }

    protected:
        virtual void Render();

    private:

        void RenderRegion(float *textureData, const s32 width, const s32 height, const s32 start, const s32 end) const;
        void DrawLine(s32 x0, s32 y0, const s32 x1, const s32 y1, u32 *destBuffer, const s32 width, const s32 height) const;

        const float *m_WaveData;
        u32 m_WaveLengthSamples;
        s32 m_UpdateRegionStart, m_UpdateRegionEnd;
        grcTexture *m_Texture;

        float m_Width;
        float m_Height;

    };
}
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIWAVEDATAVIEW_H
