// 
// audiosynth/biquadfilterview.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_BIQUADFILTERVIEW_H
#define SYNTH_BIQUADFILTERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uidropdownlist.h"
#include "uilabel.h"
#include "uirangeview.h"

namespace rage
{
	class grcTexture;
	class synthBiquadFilter;
	class synthBiquadFilterView : public synthModuleView
	{
	public:

		synthBiquadFilterView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:
		virtual void Render();
		synthBiquadFilter *GetFilterModule() { return (synthBiquadFilter*)m_Module; }

	private:
		synthUIDropDownList m_ModeList;
		synthUIRangeView m_FreqRange;
		grcTexture *m_Texture;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_BIQUADFILTERVIEW_H

