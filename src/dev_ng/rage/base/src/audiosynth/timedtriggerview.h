// 
// audiosynth/timedtriggerview.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_TIMEDTRIGGERVIEW_H
#define SYNTH_TIMEDTRIGGERVIEW_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uidropdownlist.h"

namespace rage
{
	class synthTimedTrigger;
	class synthTimedTriggerView : public synthModuleView
	{
	public:

		synthTimedTriggerView(synthModule *module);

		virtual void Update();

		virtual bool StartDrag() const;

		virtual bool IsActive() const;

	protected:

		synthTimedTrigger *GetTrigger()
		{
			return (synthTimedTrigger*)m_Module;
		}
	private:
		synthUIDropDownList m_ModeSwitch;
	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_TIMEDTRIGGERVIEW_H

