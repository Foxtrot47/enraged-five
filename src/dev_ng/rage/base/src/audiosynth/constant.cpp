// 
// audiosynth/constant.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "constant.h"

#if !SYNTH_MINIMAL_MODULES

namespace rage 
{

	synthConstant::synthConstant()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetStaticValue(1.f);
		m_Outputs[0].SetStaticValue(1.f);
	}

	void synthConstant::Synthesize()
	{
		m_Outputs[0].SetStaticValue(m_Inputs[0].GetNormalizedValue(0));
	}

} // namespace rage
#endif
