// 
// audiosynth/synthcontext.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_SYNTHCONTEXT_H
#define SYNTH_SYNTHCONTEXT_H

#include "audiohardware/mixer.h"

namespace rage
{
	
	class synthContext
	{
	public:

		synthContext() : m_FramesProcessed(0)
		{

		}

		float GetTimeS() const
		{ 
			return m_FramesProcessed * g_SecondsPerMixBuffer;
		}

		void IncrFrameCount() { m_FramesProcessed++; }
		void Reset() { m_FramesProcessed = 0; }

	private:

		u32 m_FramesProcessed;
	};
}



#endif // SYNTH_SYNTHCONTEXT_H

