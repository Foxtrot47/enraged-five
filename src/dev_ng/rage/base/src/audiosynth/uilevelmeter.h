// 
// audiosynth/uilevelmeter.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_UILEVELMETER_H
#define SYNTH_UILEVELMETER_H

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uicomponent.h"
#include "uihorizontallayout.h"
#include "uilabel.h"
#include "uiview.h"

namespace rage
{
	class synthUIBarMeter : public synthUIView
	{
	public:

		synthUIBarMeter();
		virtual ~synthUIBarMeter(){}

		void SetLevel(const float level) { m_Level = level; }
		float GetLevel() const { return m_Level; }

		virtual void Update();
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;

		virtual bool StartDrag() const { return false; }
		virtual bool IsActive() const { return IsUnderMouse(); }

		void SetWidth(const float width) { m_Width = width; }
		void SetHeight(const float height) { m_Height = height; }
	
	protected:

		virtual void Render();

	private:
		float m_Level;
		float m_Width;
		float m_Height;		
	};

	class synthUILevelMeter : public synthUIView
	{
	public:
		synthUILevelMeter();
		virtual ~synthUILevelMeter(){}

		void SetTitle(const char *title)
		{
			formatf(m_Title,title);
			m_Label.SetString(m_Title);
		}
		const char *GetTitle() const { return m_Title; }

		// synthUIComponent functionality
		virtual void Update();
		
		void SetFixedWidth(const float width) { m_FixedWidth = width; }

		bool HadClick() const;

		void SetLevel(const float level) { m_Meter.SetLevel(level); }

		virtual bool StartDrag() const { return false; } 
		virtual bool IsActive() const { return IsUnderMouse(); }
		virtual void GetBounds(Vector3 &v1, Vector3 &v2) const;
		
	private:

		char m_Title[64];
		char m_LevelText[64];
		synthUIHorizontalLayout m_Layout;
		synthUIBarMeter m_Meter;
		synthUILabelView m_Label;
		synthUILabelView m_LevelLabel;
		float m_FixedWidth;
	};

} // namespace rage
#endif // __SYNTH_EDITOR
#endif // SYNTH_UIMENUITEM_H
