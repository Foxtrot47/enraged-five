#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "uimenuitem.h"
#include "grmodel/shader.h"
#include "uimenuview.h"
#include "vector/geometry.h"
#include "uiplane.h"
namespace rage
{
	synthUIMenuItem::synthUIMenuItem() : m_Label(450)
	{
		m_SubMenu = NULL;
		m_EdgeColour = Color32(0.f,0.2f,1.f,1.f);
		m_TextSize = 0.27f;
	}

	synthUIMenuItem::~synthUIMenuItem()
	{

	}

	void synthUIMenuItem::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		v1 = Vector3(-1.f,-1.f,0.f);
		v2 = Vector3(1.f,1.f,0.f);
		const f32 width = m_Label.ComputeWidth();
		Vector3 scalingVec = Vector3(width,m_Label.ComputeHeight() * 0.5f,1.f);
		// fix width
		scalingVec.x *= ((m_TextSize*10.f)/width);
		
		Matrix34 mat = GetMatrix();
		mat.Transform3x3(scalingVec);
		mat.Scale(scalingVec);
		mat.Transform(v1);
		mat.Transform(v2);


/*		const Matrix34 &mat = GetMatrix();
		m_Label.GetBounds(v1,v2);
		// add a small border
		const f32 borderWidth = 0.05f;
		v1 -= mat.a * borderWidth;
		v1 -= mat.b * borderWidth;
		v2 += mat.a * borderWidth;
		v2 += mat.b * borderWidth;
*/
	}

	void synthUIMenuItem::Update()
	{
		m_Label.SetSize(m_TextSize);
		Matrix34 mat = GetMatrix();
		//mat.d -= mat.c*0.01f;
		m_Label.SetMatrix(mat);
		m_Label.SetUnderMouse(IsUnderMouse()||(m_SubMenu&&m_SubMenu->ShouldDraw()));
		if(m_SubMenu)
		{
            m_SubMenu->SetTextSize(m_TextSize);
			if(!ShouldDraw())
			{
				m_SubMenu->SetShouldDraw(false);
			}
			
			Vector3 mousePos = synthUIPlane::GetActive()->GetMousePosition();
			//Vector3 boundsMin, boundsMax;
			//GetBounds(boundsMin, boundsMax);

			//mousePos.x = boundsMin.x + mousePos.x * (boundsMax.x-boundsMin.x);
			//mousePos.y = boundsMin.y + mousePos.y * (boundsMax.y-boundsMin.y);
			
			Vector3 v1,v2;
			m_SubMenu->GetBounds(v1,v2);
			const Matrix34 &mat = m_SubMenu->GetMatrix();
			Vector3 absFwd = mat.c;
			absFwd.Abs();
			v1 -= absFwd;
			v2 += absFwd;

			if(m_SubMenu->ShouldDraw() && geomPoints::IsPointInBox(mousePos,v1,v2))
			{
				m_SubMenu->SetUnderMouse(true);
			}
			else
			{
				m_SubMenu->SetUnderMouse(false);
			}
			m_SubMenu->Update();
		}
	}

	void synthUIMenuItem::Render()
	{
        float width, height;
        ComputeWidthAndHeight(width, height);
		Matrix34 bm = GetMatrix();
		bm.Scale(width * 0.5f, height * 0.5f, 1.f);
		
		grcWorldMtx(bm);
		const bool isHighlighted = (IsUnderMouse()||(m_SubMenu&&m_SubMenu->ShouldDraw()));
		SetShaderVar("g_HighlightScale", isHighlighted ? 1.f : 0.f);
		SetShaderVar("g_AlphaFade", m_Alpha*0.65f);
		SetShaderVar("g_EdgeHighlightColour", m_EdgeColour);

		DrawVB("drawmenu");
		m_Label.Draw();

		if(m_SubMenu)
		{
			m_SubMenu->Draw();
		}

	}

	int synthUIMenuItem::CompareAlphabetically(synthUIMenuItem *const * item1, synthUIMenuItem *const * item2)
	{
		return stricmp((*item1)->GetTitle(), (*item2)->GetTitle());
	}

}
#endif // __SYNTH_EDITOR
