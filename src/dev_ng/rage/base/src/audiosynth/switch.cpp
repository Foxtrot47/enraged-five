// 
// audiosynth/switch.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#include "switch.h"
#include "switchview.h"
#include "synthutil.h"
#include "vectormath/vectormath.h"

namespace rage
{
	using namespace Vec;

	SYNTH_EDITOR_VIEW(synthSwitch);

	synthSwitch::synthSwitch()
	{
		m_Outputs[0].Init(this, "Output",synthPin::SYNTH_PIN_DATA_NORMALIZED, true);

		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[0].SetStaticValue(0.f);
		for(u32 i = 1; i < kMaxSwitchValues + 1; i++)
		{
			m_Inputs[i].Init(this, synthPin::sm_PinNumberStrings[i-1], synthPin::SYNTH_PIN_DATA_NORMALIZED);
			m_Inputs[i].SetStaticValue(0.f);
			m_Inputs[i].SetConvertsInternally(false);
		}
		
		m_Outputs[0].SetStaticValue(0.f);

		m_NumSwitchValues = 2;
		m_Mode = kNormalizedInput;
	}

	void synthSwitch::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("NumSwitchValues", m_NumSwitchValues);
		serializer->SerializeEnumField("Mode", m_Mode);
	}

	void synthSwitch::GetInputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Inputs[0];
		numPins = m_NumSwitchValues + 1;
	}

	void synthSwitch::Synthesize()
	{
		bool hasDynamicInputs = false;
		bool hasSignalInputs = false;
		for(u32 i = 1; i < 1+m_NumSwitchValues; i++)
		{
			if(m_Inputs[i].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				hasDynamicInputs = true;
			}
			if(m_Inputs[i].GetDataFormat() == synthPin::SYNTH_PIN_DATA_SIGNAL)
			{
				hasSignalInputs = true;
			}
		}

		const float t = m_Inputs[0].GetNormalizedValue(0);

		m_Outputs[0].SetDataFormat(hasSignalInputs ? synthPin::SYNTH_PIN_DATA_SIGNAL : synthPin::SYNTH_PIN_DATA_NORMALIZED);

		if(hasDynamicInputs)
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);
			
			atFixedArray<const float *, 16> inputs;
			// Prepare input buffers
			for(u32 i = 0; i < m_NumSwitchValues; i++)
			{
				for(u32 s = 0; s < kMixBufNumSamples; s++)
				{
					if(hasSignalInputs)
					{
						m_InputBuffers[i].GetBuffer()[s] = m_Inputs[i+1].GetSignalValue(s);
					}
					else
					{
						m_InputBuffers[i].GetBuffer()[s] = m_Inputs[i+1].GetNormalizedValue(s);
					}
				}
				inputs.Append() = m_InputBuffers[i].GetBuffer();
			}

			switch(m_Mode)
			{
			case kNormalizedInput:
				Process_Norm(m_Buffer.GetBuffer(), inputs, t, kMixBufNumSamples);
				break;
			case kRescaledInput:
				Process_Index(m_Buffer.GetBuffer(), inputs, t, kMixBufNumSamples);
				break;
			case kInterpInput:
				Process_Lerp(m_Buffer.GetBuffer(), inputs, t, kMixBufNumSamples);
				break;
			case kEqualPowerXFade:
				Process_EqualPower(m_Buffer.GetBuffer(), inputs, t, kMixBufNumSamples);
				break;
			}

		}		
		else
		{
			// Static mode
			atFixedArray<float, 16> inputs;
			for(u32 i = 0; i < m_NumSwitchValues; i++)
			{
				if(hasSignalInputs)
				{
					inputs.Append() = m_Inputs[i+1].GetSignalValue(0);
				}
				else
				{
					inputs.Append() = m_Inputs[i+1].GetNormalizedValue(0);
				}
			}
			

			float outVal = 0.f;
			switch(m_Mode)
			{
			case kNormalizedInput:
				outVal = Process_Norm(t, inputs);
				break;
			case kRescaledInput:
				outVal = Process_Index(t, inputs);
				break;
			case kInterpInput:
				outVal = Process_Lerp(t, inputs);
				break;
			case kEqualPowerXFade:
				outVal = Process_EqualPower(t, inputs);
				break;
			}

			m_Outputs[0].SetStaticValue(outVal);
		}

	}

	u32 synthSwitch::ComputeActiveInputIndex() const
	{
		const f32 unscaledInput = m_Inputs[0].GetNormalizedValue(0);

		u32 index = 0;
		if(m_Mode == kNormalizedInput)
		{
			index = (u32)(Clamp(unscaledInput,0.f,1.f) * m_NumSwitchValues);
			if(index >= m_NumSwitchValues)
				index = m_NumSwitchValues-1;
		}
		else
		{
			index = (u32)unscaledInput % m_NumSwitchValues;
		}

		return index;
	}

	void synthSwitch::Process_Norm(float *inOut, const atFixedArray<const float *, 16> &otherInputs, const float t, const u32 numSamples)
	{
		const u32 numValues = otherInputs.GetCount();

		u32 index = (u32)(Clamp(t,0.f,1.f) * numValues);
		if(index >= numValues)
		{
			index = numValues - 1;
		}

		if(inOut != otherInputs[index])
		{
			synthUtil::CopyBuffer(reinterpret_cast<Vector_4V*>(inOut), reinterpret_cast<const Vector_4V*>(otherInputs[index]), numSamples);
		}
	}

	void synthSwitch::Process_Index(float *inOut, const atFixedArray<const float *, 16> &otherInputs, const float t, const u32 numSamples)
	{
		const u32 numValues = otherInputs.GetCount();

		u32 index = (u32)t % numValues;
		
		if(inOut != otherInputs[index])
		{
			synthUtil::CopyBuffer(reinterpret_cast<Vector_4V*>(inOut), reinterpret_cast<const Vector_4V*>(otherInputs[index]), numSamples);
		}
	}

	void synthSwitch::Process_Lerp(float *inOut, const atFixedArray<const float *, 16> &otherInputs, const float t, const u32 numSamples)
	{
		const u32 numValues = otherInputs.GetCount();
		const u32 index0 = (u32)t % numValues;
		const f32 interpFactor = t - floorf(t);

		if(index0 >= numValues - 1 || interpFactor == 0.0f)
		{
			if(otherInputs[index0] != inOut)
			{
				synthUtil::CopyBuffer(reinterpret_cast<Vector_4V*>(inOut), reinterpret_cast<const Vector_4V*>(otherInputs[index0]), numSamples);
			}
		}
		else
		{
			Vector_4V interp = V4LoadScalar32IntoSplatted(interpFactor);
	
			const Vector_4V *aPtr = reinterpret_cast<const Vector_4V*>(otherInputs[index0]);
			const Vector_4V *bPtr = reinterpret_cast<const Vector_4V*>(otherInputs[index0+1]);
			Vector_4V *outPtr = reinterpret_cast<Vector_4V*>(inOut);
			u32 count = numSamples>>2;
			while(count--)
			{
				*outPtr++ = V4Lerp(interp, *aPtr++, *bPtr++);
			}
		}
	}

	void synthSwitch::Process_EqualPower(float *inOut, const atFixedArray<const float *, 16> &otherInputs, const float t, const u32 numSamples)
	{
		const u32 numValues = otherInputs.GetCount();
		const u32 index0 = (u32)t % numValues;
		const f32 interpFactor = t - floorf(t);

		if(index0 >= numValues - 1 || interpFactor == 0.0f)
		{
			if(otherInputs[index0] != inOut)
			{
				synthUtil::CopyBuffer(reinterpret_cast<Vector_4V*>(inOut), reinterpret_cast<const Vector_4V*>(otherInputs[index0]), numSamples);
			}
		}
		else
		{
			const Vector_4V interp = V4LoadScalar32IntoSplatted(interpFactor);
			const Vector_4V angle = V4Scale(interp, V4VConstant(V_PI_OVER_TWO));
			
			Vector_4V linVol1, linVol2;
			V4SinAndCos(linVol2, linVol1, angle);
						
			const Vector_4V *aPtr = reinterpret_cast<const Vector_4V*>(otherInputs[index0]);
			const Vector_4V *bPtr = reinterpret_cast<const Vector_4V*>(otherInputs[index0+1]);
			Vector_4V *outPtr = reinterpret_cast<Vector_4V*>(inOut);
			u32 count = numSamples>>2;
			while(count--)
			{
				*outPtr++ = V4AddScaled(V4Scale(*aPtr++, linVol1), *bPtr++, linVol2);
			}
		}
	}

	float synthSwitch::Process_Norm(const float t, const atFixedArray<float, 16> &inputs)
	{
		const u32 numValues = inputs.GetCount();

		u32 index = (u32)(Clamp(t,0.f,1.f) * numValues);
		if(index >= numValues)
		{
			index = numValues - 1;
		}
		return inputs[index];
	}

	float synthSwitch::Process_Index(const float t, const atFixedArray<float, 16> &inputs)
	{
		const u32 numValues = inputs.GetCount();
		u32 index = (u32)t % numValues;
		return inputs[index];		
	}

	float synthSwitch::Process_Lerp(const float t, const atFixedArray<float, 16> &inputs)
	{
		const u32 numValues = inputs.GetCount();
		const u32 index0 = (u32)t % numValues;
		const f32 interpFactor = t - floorf(t);

		if(index0 >= numValues - 1 || interpFactor == 0.0f)
		{
			return inputs[index0];
		}
		else
		{
			return Lerp(interpFactor, inputs[index0], inputs[index0+1]);
		}
	}

	float synthSwitch::Process_EqualPower(const float t, const atFixedArray<float, 16> &inputs)
	{
		const u32 numValues = inputs.GetCount();
		const u32 index0 = (u32)t % numValues;
		const f32 interpFactor = t - floorf(t);

		if(index0 >= numValues - 1 || interpFactor == 0.0f)
		{
			return inputs[index0];
		}
		else
		{
			const float angle = interpFactor * PI * 0.5f;
			const float linVol2 = Sinf(angle);
			const float linVol1 = Cosf(angle);
			return inputs[index0]*linVol1 + inputs[index0+1]*linVol2;
		}
	}
}

