// 
// audiosynth/ceil.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "ceil.h"

#if !SYNTH_MINIMAL_MODULES

#include "math/amath.h"
#include "vectormath/vectormath.h"

namespace rage 
{
	using namespace Vec;

	synthCeil::synthCeil()
	{
		m_Outputs[0].Init(this, "Output", synthPin::SYNTH_PIN_DATA_NORMALIZED, true);
		m_Outputs[0].SetStaticValue(0.f);

		m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_NORMALIZED);		
	}

	void synthCeil::Synthesize()
	{	
		m_Outputs[0].SetDataFormat(m_Inputs[0].GetDataFormat());

		if(m_Inputs[0].GetDataFormat() == synthPin::SYNTH_PIN_DATA_NORMALIZED)
		{
			m_Outputs[0].SetStaticValue(ceilf(m_Inputs[0].GetNormalizedValue(0)));
		}
		else
		{
			m_Outputs[0].SetStaticValue(ceilf(m_Inputs[0].GetSignalValue(0)));
		}

		if(m_Inputs[0].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
		{
			m_Outputs[0].SetDataBuffer(&m_Buffer);

			const f32 *RESTRICT const input = m_Inputs[0].GetDataBuffer()->GetBuffer();
			f32 *RESTRICT output = m_Outputs[0].GetDataBuffer()->GetBuffer();

			const u32 numSamples = m_Outputs[0].GetDataBuffer()->GetSize();

			Process(output, input, numSamples);
		}
	}

	void synthCeil::Process(float *RESTRICT outBuffer, const float *RESTRICT inBuffer, const u32 numSamples)
	{
		const Vector_4V *inPtr = reinterpret_cast<const Vector_4V*>(inBuffer);
		Vector_4V *outPtr = reinterpret_cast<Vector_4V*>(outBuffer);
		
		u32 count = numSamples >> 4;
		while(count--)
		{
			*outPtr++ = V4RoundToNearestIntPosInf(*inPtr++);
			*outPtr++ = V4RoundToNearestIntPosInf(*inPtr++);
			*outPtr++ = V4RoundToNearestIntPosInf(*inPtr++);
			*outPtr++ = V4RoundToNearestIntPosInf(*inPtr++);
		}
	}

	void synthCeil::Process(float *RESTRICT inOutBuffer, const u32 numSamples)
	{
		Vector_4V *inOutPtr = reinterpret_cast<Vector_4V*>(inOutBuffer);

		u32 count = numSamples >> 4;
		while(count--)
		{
			*inOutPtr = V4RoundToNearestIntPosInf(*inOutPtr);
			inOutPtr++;
			*inOutPtr = V4RoundToNearestIntPosInf(*inOutPtr);
			inOutPtr++;
			*inOutPtr = V4RoundToNearestIntPosInf(*inOutPtr);
			inOutPtr++;
			*inOutPtr = V4RoundToNearestIntPosInf(*inOutPtr);
			inOutPtr++;
		}
	}

} // namespace rage
#endif
