// 
// audiosynth/sequencerview.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"
#if __SYNTH_EDITOR && !SYNTH_MINIMAL_MODULES

#include "sequencerview.h"
#include "sequencer.h"
#include "moduleview.h"
#include "pinview.h"

#include "module.h"
#include "pin.h"

namespace rage
{
	synthSequencerView::synthSequencerView(synthModule *module) : synthModuleView(module)
	{
		m_Layout.Add(&m_Divisions);
        m_Layout.Add(&m_TempoRange);
		
        m_Layout.SetOrientation(synthUIHorizontalLayout::kVertical);
        m_Layout.SetPadding(0.5f);

        AddComponent(&m_Layout);

		const f32 maxUIVal = 320.f;
		m_TempoRange.SetMinRange(0.f, maxUIVal);
		m_TempoRange.SetMaxRange(0.f, maxUIVal);
        m_TempoRange.SetTitles("MinBPM", "MaxBPM");
        m_TempoRange.SetWidth(0.4f);
        m_TempoRange.SetDrawTitles(true);
		
		m_TempoRange.SetNormalizedValues(GetSequencer()->GetMinBpm()/maxUIVal, GetSequencer()->GetMaxBpm()/maxUIVal);
		m_Divisions.SetRange(1, kSequencerMaxDivisions);
		m_Divisions.SetNormalizedValue(GetSequencer()->GetDivisions() / (f32)kSequencerMaxDivisions);
        m_Divisions.SetWidth(0.4f);
        m_Divisions.SetTitle("Divisions");
        m_Divisions.SetDrawTitle(true);
        m_Divisions.SetRenderAsInteger(true);
    }

	void synthSequencerView::Update()
	{
		// fade out title when editing
        if(IsMinimised())
        {
            SetTitleAlpha(1.f);
        }
        else
        {
            SetTitleAlpha(1.f - GetHoverFade());
        }
		m_TitleLabel.SetUnderMouse(IsUnderMouse());
		
		m_TempoRange.SetShouldDraw(ShouldDraw() && !IsMinimised());
		m_Divisions.SetShouldDraw(ShouldDraw() && !IsMinimised());

		m_Layout.SetMatrix(GetMatrix());
		
		m_Divisions.SetAlpha(GetHoverFade() * GetAlpha());
		m_Divisions.SetTextAlpha(GetHoverFade() * GetAlpha());

		m_TempoRange.SetAlpha(GetHoverFade() * GetAlpha());
		m_TempoRange.SetTextAlpha(GetHoverFade());
		
		const s32 divisions = (s32)m_Divisions.GetValue();
		GetSequencer()->SetDivisions(divisions);

		GetSequencer()->SetBpmRange(m_TempoRange.GetMinValue(),m_TempoRange.GetMaxValue());
		GetSequencer()->GetInput(kSequencerTempo).GetView()->SetStaticViewRange(GetSequencer()->GetMinBpm(), GetSequencer()->GetMaxBpm());

		// check for any outputs that now invalid but are still connected
		u32 numOutputs = 0;
		synthPin *pins;
		GetSequencer()->GetOutputs(pins,numOutputs);
		// kMaxDivisions+1 to account for the step output pin
		for(u32 i = numOutputs; i < kSequencerMaxDivisions+1; i++)
		{
			atArray<synthPin *> &destinations = GetSequencer()->GetOutput(i).GetView()->GetOutputDestinations();
			for(s32 k = 0; k < destinations.GetCount(); k++)
			{
				Assert(destinations[k]->GetOtherPin() == &GetSequencer()->GetOutput(i));
				destinations[k]->Disconnect();
			}
		}

		synthModuleView::Update();
	}

	void synthSequencerView::Render()
	{
		synthModuleView::Render();
	}

	u32 synthSequencerView::GetModuleHeight() const
	{
		return Max(synthModuleView::GetModuleHeight(), 6U);
	}

	bool synthSequencerView::StartDrag() const
	{
		if(m_TempoRange.IsUnderMouse() || m_Divisions.IsUnderMouse())
		{
			return false;
		}
		else
		{
			return synthModuleView::StartDrag();
		}
	}

	bool synthSequencerView::IsActive() const
	{
		return (IsUnderMouse() || m_TempoRange.IsActive() || m_Divisions.IsActive());
	}
}
#endif // __SYNTH_EDITOR

