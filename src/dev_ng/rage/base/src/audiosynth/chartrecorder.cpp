
#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "chartrecorder.h"
#include "chartrecorderview.h"
#include "audiohardware/mixer.h"
#include "vectormath/vectormath.h"

namespace rage
{
    using namespace Vec;
    SYNTH_EDITOR_VIEW(synthChartRecorder);

    synthChartRecorder::synthChartRecorder()
    {
#if __SYNTH_EDITOR
        m_Inputs[0].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
        m_Inputs[1].Init(this, "Record", synthPin::SYNTH_PIN_DATA_NORMALIZED);
        m_Inputs[1].SetStaticValue(1.f);

        m_Length = m_AllocatedLength = kMixerNativeSampleRate * sizeof(f32) * 2;
        m_Buffer = rage_aligned_new(16) float[m_AllocatedLength];
        sysMemSet(m_Buffer,0,sizeof(float) * m_AllocatedLength);

        m_WriteOffsetSamples = 0;
#endif
    }

    synthChartRecorder::~synthChartRecorder()
    {
#if __SYNTH_EDITOR
    delete[] m_Buffer;
#endif
    }

    void synthChartRecorder::Synthesize()
    {
#if __SYNTH_EDITOR
    if(m_Inputs[1].GetNormalizedValue(0) >= 1.f)
    {
        const s32 numSamples = kMixBufNumSamples;
        Vector_4V *destPtr = (Vector_4V*)(m_Buffer + m_WriteOffsetSamples);
        
        for(s32 i = 0; i < numSamples; i+= 4)
        {
            *destPtr++ = m_Inputs[0].GetSignalValueV(i);
        }

          m_WriteOffsetSamples = (m_WriteOffsetSamples + numSamples) % m_Length;
    }
    else
    {
        // Always start recording from 0
        m_WriteOffsetSamples = 0;
    }

#endif // __SYNTH_EDITOR
    }
}
#endif

