// 
// audiosynth/variableinputview.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SYNTH_VARIABLEINPUTVIEW_H
#define SYNTH_VARIABLEINPUTVIEW_H

#include "synthdefs.h"
#if __SYNTH_EDITOR

#include "moduleview.h"
#include "uilabel.h"
#include "numericinputbox.h"

namespace rage
{
	class synthVariableInput;
	class synthVariableInputView : public synthModuleView
	{
	public:

		synthVariableInputView(synthModule *module);

		virtual void Update();
		virtual void Render();

		void Notify_SetValue();
	protected:

		virtual bool StartDrag() const;

	private:

		synthVariableInput *GetVariableInput() const { return (synthVariableInput*)m_Module; }
		synthUILabel m_ExportedName;
		synthUITextBoxInput m_InputBox;

	};
}

#endif // __SYNTH_EDITOR
#endif // SYNTH_VARIABLEINPUTVIEW_H

