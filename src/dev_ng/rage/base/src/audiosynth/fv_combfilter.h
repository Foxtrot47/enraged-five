
#ifndef SYNTH_FV_COMBFILTER_H
#define SYNTH_FV_COMBFILTER_H

#include "synthdefs.h"

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "audioeffecttypes/combfilter.h"

namespace rage
{
	enum synthFvCombInputs
	{
		kFvCombSignal = 0,
		kFvCombLength,
		kFvCombFeedback,
		kFvCombDamping,
		kFvCombNumInputs
	};

	class synthFvCombFilter : public synthModuleBase<kFvCombNumInputs,1, SYNTH_FVCOMBFILTER>
	{
	public:

		synthFvCombFilter();
		virtual ~synthFvCombFilter();

	
		virtual void Synthesize();

		virtual const char *GetName() const
		{
			return "FV_CombFilter";
		}

	
	private:

		synthSampleFrame m_Buffer;
		audCombFilter m_Filter;

		u32 m_CombLength;
		float *m_CombBuffer;
	};
}

#endif // SYNTH_FV_COMBFILTER_H


