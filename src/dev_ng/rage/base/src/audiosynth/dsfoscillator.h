
#ifndef SYNTH_DSFOSCILLATOR_H
#define SYNTH_DSFOSCILLATOR_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "audiohardware/mixer.h"

#include "synthdefs.h"

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{
	class synthModuleView;

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif

	enum synthAnalogueOscillatorInputs
	{
		kAnalogueOscillatorAmplitude = 0,
		kAnalogueOscillatorFrequency,
		kAnalogueOscillatorPhase,
		kAnalogueOscillatorHardSync,
		
		kAnalogueOscillatorNumInputs
	};

	class synthAnalogueOscillator : 
		public synthModuleBase<kAnalogueOscillatorNumInputs, 1, SYNTH_ANALOGUE_OSCILLATOR>
	{
	public:
		SYNTH_MODULE_NAME("AnalogueOsc");
		SYNTH_CUSTOM_MODULE_VIEW;

		enum synthAnalogueOscillatorMode
		{
			IMPULSE_TRAIN = 0,
			SQUARE,
			SAW,			
			NUM_OSCILLATOR_MODES
		};

#if __SYNTH_EDITOR
		static char *ModeStrings[NUM_OSCILLATOR_MODES];
		u32 GetNumModes() const
		{
			return NUM_OSCILLATOR_MODES;
		}

		const char *GetModeName(const synthAnalogueOscillatorMode mode) const;
#endif

		synthAnalogueOscillator();
		virtual ~synthAnalogueOscillator();

		virtual void SerializeState(synthModuleSerializer *serializer);

		virtual void Synthesize();

		void SetMinFrequency(const f32 freq)
		{
			m_FrequencyRange[kMinFreq] = freq;
		}

		f32 GetMinFrequency() const
		{
			return m_FrequencyRange[kMinFreq];
		}

		void SetMaxFrequency(const f32 freq)
		{
			m_FrequencyRange[kMaxFreq] = freq;
		}

		f32 GetMaxFrequency() const
		{
			return m_FrequencyRange[kMaxFreq];
		}

		void SetMode(const synthAnalogueOscillatorMode mode)
		{
			m_Mode = mode;
		}

		synthAnalogueOscillatorMode GetMode() const
		{
			return m_Mode;
		}

		Vec::Vector_4V ComputeSampleV(const Vec::Vector_4V_In phase, const Vec::Vector_4V_In freq);

	private:
		
		enum {kMinFreq = 0,kMaxFreq};

		Vec::Vector_4V m_Phase;
		Vec::Vector_4V m_LastOut;
		Vec::Vector_4V m_LastBlitOut;
		ALIGNAS(16) f32 m_FrequencyRange[4] ;


		synthAnalogueOscillatorMode m_Mode;

		synthSampleFrame m_Buffer;

	};
#if __WIN32
#pragma warning(pop)
#endif

}
#endif
#endif // SYNTH_DSFOSCILLATOR_H


