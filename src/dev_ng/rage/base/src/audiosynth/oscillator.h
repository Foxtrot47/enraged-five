
#ifndef SYNTH_OSCILLATOR_H
#define SYNTH_OSCILLATOR_H

#include "audiohardware/mixer.h"

#include "synthdefs.h"

#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectormath.h"

namespace rage
{
	class synthModuleView;

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4324) // structure was padded due to __declspec(align())
#endif

	enum synthDigitalOscillatorInputs
	{
		kDigitalOscillatorAmplitude = 0,
		kDigitalOscillatorFrequency,
		kDigitalOscillatorPhase,
		kDigitalOscillatorHardSync,
		
		kDigitalOscillatorNumInputs
	};

	class synthDigitalOscillator : 
		public synthModuleBase<kDigitalOscillatorNumInputs, 1, SYNTH_DIGITAL_OSCILLATOR>
	{
	public:
		SYNTH_MODULE_NAME("Oscillator");
		SYNTH_CUSTOM_MODULE_VIEW;

		enum synthOscillatorMode
		{
			SQUARE = 0,
			TRIANGLE,
			SAW,
			INVERTED_SAW,
			SINE,
			COSINE,
			NUM_OSCILLATOR_MODES
		};

#if __SYNTH_EDITOR
		static char *ModeStrings[NUM_OSCILLATOR_MODES];
		u32 GetNumModes() const
		{
			return NUM_OSCILLATOR_MODES;
		}
		const char *GetModeName(const synthOscillatorMode mode) const;
#endif

		synthDigitalOscillator();
		virtual ~synthDigitalOscillator();
		
		virtual void SerializeState(synthModuleSerializer *serializer);	
		virtual void Synthesize();
		virtual void ResetProcessing();


		void SetMinFrequency(const f32 freq)
		{
			m_FrequencyRange[kMinFreq] = freq;
		}

		f32 GetMinFrequency() const
		{
			return m_FrequencyRange[kMinFreq];
		}
		
		void SetMaxFrequency(const f32 freq)
		{
			m_FrequencyRange[kMaxFreq] = freq;
		}

		f32 GetMaxFrequency() const
		{
			return m_FrequencyRange[kMaxFreq];
		}
		
		void SetMode(const synthOscillatorMode mode)
		{
			m_Mode = mode;
		}
		
		synthOscillatorMode GetMode() const
		{
			return m_Mode;
		}

		void SetStaticOutput(const bool staticOutput)
		{
			m_StaticOutput = staticOutput;
		}

		bool IsStaticOutput() const{ return m_StaticOutput; }

		__forceinline Vec::Vector_4V_Out ComputeSampleV(const synthOscillatorMode mode, const Vec::Vector_4V_In phase)
		{
			Vec::Vector_4V one = Vec::V4VConstant(V_ONE);
			Vec::Vector_4V two = Vec::V4VConstant(V_TWO);

			Vec::Vector_4V output;
			switch(mode)
			{
			case SQUARE:
				output = Vec::V4SelectFT(Vec::V4IsGreaterThanOrEqualV(phase, Vec::V4VConstant(V_HALF)), one, Vec::V4VConstant(V_NEGONE));
				break;
			case SINE:
				output = Vec::V4SinFast(Vec::V4CanonicalizeAngle(Vec::V4Scale(phase, Vec::V4VConstant(V_TWO_PI))));
				break;
			case COSINE:
				output = Vec::V4CosFast(Vec::V4CanonicalizeAngle(Vec::V4Scale(phase, Vec::V4VConstant(V_TWO_PI))));
				break;
			case TRIANGLE:
				{
					const Vec::Vector_4V lessThanHalf = Vec::V4Scale(phase,two);
					// 1 -  (2*(phase-0.5))
					const Vec::Vector_4V greaterThanHalf = Vec::V4Subtract(one,Vec::V4Scale(Vec::V4Subtract(phase,Vec::V4VConstant(V_HALF)),two));

					const Vec::Vector_4V selectMask = Vec::V4IsLessThanV(phase, Vec::V4VConstant(V_HALF));
					// 1 - (2 * val)
					output = Vec::V4Scale(Vec::V4VConstant(V_NEGONE),Vec::V4Subtract(one,
						Vec::V4Scale(two,Vec::V4SelectFT(selectMask, greaterThanHalf, lessThanHalf))));
				}
				break;
			case SAW:
				output = Vec::V4Scale(Vec::V4Subtract(Vec::V4Scale(phase,two), one), Vec::V4VConstant(V_NEGONE));
				break;
			case INVERTED_SAW:
				output = Vec::V4Subtract(Vec::V4Scale(phase,two), one);
				break;
			default:
				output = Vec::V4VConstant(V_ZERO);
				break;
			}

			return output;
		}


		// PURPOSE
		//	Generate a normalized [0,1] ramp at the specified frequency
		// PARAMS
		//	inOutBuffer: frequency (hz) input, contains ramp oscillator output
		//	state: persistent state (phase)
		//	numSamples: number of samples to generate
		static void GenerateRamp(float *RESTRICT const inOutBuffer, Vec::Vector_4V_InOut state, const u32 numSamples);
		// PURPOSE
		//	Generate a normalized [0,1] ramp at the specified frequency
		// PARAMS
		//	outputBuffer
		//	frequency (hz)
		//	state: persistent state (phase)
		//	numSamples: number of samples to generate
		static void GenerateRamp(float *RESTRICT const outputBuffer, const Vec::Vector_4V_In frequency, Vec::Vector_4V_InOut state, const u32 numSamples);
		// PURPOSE
		//	Generate a normalized [0,1] ramp at the specified frequency
		// PARAMS
		//	frequency in hertz
		//	state: persistent state (phase)
		// RETURNS
		//	ramp value
		static float GenerateRamp(const float frequency, Vec::Vector_4V_InOut state);


		// PURPOSE
		//	Applies a sine transform to the specified buffer
		// PARAMS
		//	inOutBuffer: [0,1] phase values as input, [-1,1] signal values as output
		static void Sine(float *RESTRICT const inOutBuffer, const u32 numSamples);
		static float Sine(const float inputPhase);
		static void Cosine(float *RESTRICT const inOutBuffer, const u32 numSamples);
		static float Cosine(const float inputPhase);
		static void Square(float *RESTRICT const inOutBuffer, const u32 numSamples);
		static float Square(const float inputPhase);
		static void Triangle(float *RESTRICT const inOutBuffer, const u32 numSamples);
		static float Triangle(const float inputPhase);
		static void Saw(float *RESTRICT const inOutBuffer, const u32 numSamples);
		static float Saw(const float inputPhase);


	private:
		enum {kMinFreq = 0,kMaxFreq};

		Vec::Vector_4V m_StateVec;

		Vec::Vector_4V m_Phase;
		ALIGNAS(16) f32 m_FrequencyRange[4] ;
		
		synthOscillatorMode m_Mode;
	
		synthSampleFrame m_Buffer;		

		bool m_StaticOutput;
	};
#if __WIN32
#pragma warning(pop)
#endif

}
#endif // !SYNTH_MINIMAL_MODULES
#endif // SYNTH_OSCILLATOR_H


