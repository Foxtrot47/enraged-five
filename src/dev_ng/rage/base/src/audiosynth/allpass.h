#ifndef SYNTH_ALLPASS_H
#define SYNTH_ALLPASS_H

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

#include "vectormath/vectortypes.h"

namespace rage
{
	enum synthAllpassFilterInputs
	{
		kAllpassSignal = 0,
		kAllpassGain,
		kAllpassCutoff,
		kAllpassBandwidth,
		kAllpassNumInputs
	};

	class synthAllpassFilter : public synthModuleBase<kAllpassNumInputs,1, SYNTH_ALLPASS>
	{
		public:
			SYNTH_CUSTOM_MODULE_VIEW;

			synthAllpassFilter();
			virtual ~synthAllpassFilter(){};
			
			virtual void SerializeState(synthModuleSerializer *serializer);
						
			virtual void Synthesize();

			static void Process(f32* inputBuffer, const float cutoff, Vec::Vector_4V_InOut state, const u32 numSamples);
			static void Process(f32* inputBuffer, const f32* cutoff, Vec::Vector_4V_InOut state, const u32 numSamples);

			void SetMinFrequency(const f32 freq)
			{
				m_MinFrequency = freq;
			}
			
			void SetMaxFrequency(const f32 freq)
			{
				m_MaxFrequency = freq;
			}
			
			f32 GetMinFrequency() const { return m_MinFrequency; }
			f32 GetMaxFrequency() const { return m_MaxFrequency; }

			virtual const char *GetName() const
			{
				return "AllPassFilter";
			}

		private:
		
			synthSampleFrame m_Buffer;
			
			f32 m_MinFrequency;
			f32 m_MaxFrequency;

			f32 m_x1,m_x2;
			f32 m_y1,m_y2;
		};
}

#endif // SYNTH_ALLPASS_H


