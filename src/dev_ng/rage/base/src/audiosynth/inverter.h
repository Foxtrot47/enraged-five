// 
// audiosynth/inverter.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


#ifndef SYNTH_INVERTER_H
#define SYNTH_INVERTER_H

#include "synthdefs.h"
#if !SYNTH_MINIMAL_MODULES

#include "sampleframe.h"
#include "module.h"
#include "modulefactory.h"
#include "pin.h"

namespace rage
{
	class synthInverter : public synthModuleBase<1,1,SYNTH_INVERTER>
	{
	public:
		SYNTH_MODULE_NAME("Inverter");

		synthInverter();
		virtual void Synthesize();

	private:
		synthSampleFrame m_Buffer;
	};
}
#endif
#endif // SYNTH_INVERTER_H


