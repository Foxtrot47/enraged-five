// 
// audiosynth/uiscrollbar.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "synthdefs.h"

#if __SYNTH_EDITOR

#include "synthesizerview.h"
#include "uiscrollbar.h"
#include "uiplane.h"

#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"

#include "grcore/viewport.h"
#include "system/timemgr.h"
#include "input/keys.h"
#include "input/keyboard.h"
#include "input/mouse.h"

namespace rage
{
	synthUIScrollbar::synthUIScrollbar() : 
	m_CurrentValue(0.f), 
		m_MinValue(0.f), 
		m_MaxValue(1.f), 
		m_TextAlpha(0.f), 
		m_Width(1.f),
		m_IsBeingDragged(false),
		m_DrawTitle(false),
		m_DrawReflection(false),
		m_ReadOnly(false),
		m_RenderAsInteger(false)
	{
		AddComponent(&m_ValueLabel);
		AddComponent(&m_TitleLabel);
		m_InputBox.SetShouldDraw(false);
		m_HighlightColour = Color32(0.f,0.f,1.f,1.f);
	}

	synthUIScrollbar::~synthUIScrollbar()
	{
		if(m_IsBeingDragged)
		{
			synthUIPlane::GetMaster()->SetIsMouseFixed(false);
		}
	}

	void synthUIScrollbar::GetBounds(Vector3 &v1, Vector3 &v2) const
	{
		v1 = Vector3(-1.f,-1.f,0.f);
		v2 = Vector3(1.f, 1.f, 0.f);
		GetMatrix().Transform(v1);
		GetMatrix().Transform(v2);
	}

	void synthUIScrollbar::Update()
	{
		synthUIView::Update();

		if(!m_ReadOnly)
		{
			if(m_IsBeingDragged)
			{
				if(ioMouse::GetButtons()&ioMouse::MOUSE_LEFT)
				{
					const f32 keyboardScalar = ioKeyboard::KeyDown(KEY_SHIFT) ? 1.f : 
						ioKeyboard::KeyDown(KEY_CONTROL) ? 0.05f : 0.5f;

					const f32 scaledDelta = 0.003f * -ioMouse::GetDY() * keyboardScalar;
					const f32 currentValue = m_CurrentValue + scaledDelta;
					if(ioKeyboard::KeyDown(KEY_ALT)|| m_CurrentValue < 0.f || m_CurrentValue > 1.f)
					{
						m_CurrentValue = currentValue;
					}
					else
					{
						m_CurrentValue = Clamp(currentValue ,0.f,1.f);
					}
				}
				else
				{
					m_IsBeingDragged = false;
					synthUIPlane::GetMaster()->SetIsMouseFixed(false);
				}
			}
			if(IsUnderMouse() && ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT)
			{
				m_IsBeingDragged = true;
				synthUIPlane::GetMaster()->SetIsMouseFixed(true);
				m_LastMouseY = ioMouse::GetY();
			}

			if(IsUnderMouse())
			{
				const f32 scaledDelta = 1.f/(m_MaxValue-m_MinValue);
				const f32 keyboardDelta = ioKeyboard::KeyDown(KEY_SHIFT) ? 0.05f : 
					ioKeyboard::KeyDown(KEY_CONTROL) ? Min(scaledDelta, 0.00075f) : 0.001f;

				const f32 currentValue = m_CurrentValue;

				f32 delta = 0.f;

				if(ioKeyboard::KeyDown(KEY_CONTROL))
				{
					// fine control - one delta per press, rather than per frame
					if(ioKeyboard::KeyPressed(KEY_DOWN))
					{
						delta = -keyboardDelta;
					}
					else if(ioKeyboard::KeyPressed(KEY_UP))
					{
						delta = keyboardDelta;						
					}
				}
				else
				{
					if(ioKeyboard::KeyDown(KEY_DOWN))
					{
						delta = -keyboardDelta;
					}
					else if(ioKeyboard::KeyDown(KEY_UP))
					{
						delta = keyboardDelta;
					}
				}

				if(delta != 0.f)
				{
					if(ioKeyboard::KeyDown(KEY_ALT) || m_CurrentValue <0.f || m_CurrentValue > 1.f)
					{
						m_CurrentValue = currentValue + delta;
					}
					else
					{
						m_CurrentValue = Clamp(currentValue + delta,0.f,1.f);
					}
				}
				
			}
		}
		if(m_RenderAsInteger)
		{	
			formatf(m_ValueString,sizeof(m_ValueString),"%d", (s32)GetValue());
		}
		else
		{
			formatf(m_ValueString,sizeof(m_ValueString),"%g", GetValue());
		}
		m_ValueLabel.SetString(m_ValueString);
		//m_ValueLabel.SetUnderMouse(IsBeingDragged());

        const f32 pinHeight = ComputeHeight();


		if(m_DrawTitle)
		{	
			Matrix34 labelMtx = GetMatrix();
			
            m_TitleLabel.SetMatrix(labelMtx);
            const f32 labelHeight = m_TitleLabel.ComputeHeight();
			// position slightly in front of the pin view to prevent z-fighting
            labelMtx.d -= labelMtx.c * 0.1f;
			labelMtx.d += labelMtx.b * (pinHeight*0.5f + labelHeight*0.5f);
			m_TitleLabel.SetMatrix(labelMtx);
			m_TitleLabel.SetSize(0.2f);
			m_TitleLabel.SetAlpha(GetAlpha() * m_TextAlpha);
		}
		Matrix34 labelMtx = GetMatrix();
		
		m_ValueLabel.SetMatrix(labelMtx);
		const f32 labelHeight = m_ValueLabel.ComputeHeight();
		// position slightly in front of the pin view to prevent z-fighting
		labelMtx.d -= GetMatrix().c * 0.1f;
		labelMtx.d -= GetMatrix().b * (pinHeight*0.5f + labelHeight*0.15f);
		
		m_ValueLabel.SetSize(0.2f);
		m_ValueLabel.SetMatrix(labelMtx);
		m_ValueLabel.SetAlpha(GetAlpha() * m_TextAlpha);
		m_ValueLabel.SetShouldDraw(ShouldDraw() && m_ValueLabel.GetAlpha()>0.f);

		m_TitleLabel.SetShouldDraw(m_DrawTitle);

		if(m_InputBox.ShouldDraw())
		{
			m_InputBox.Update();
			if(m_InputBox.HadEnter())
			{
				f32 inputVal;
				if(sscanf(m_InputBox.GetKeyboardBuffer(), "%f", &inputVal) == 1)
				{
					// prevent divide by zero
					if(m_MinValue!=m_MaxValue)
					{
						SetNormalizedValue((inputVal - m_MinValue) / (m_MaxValue-m_MinValue));
					}
				}
				m_InputBox.SetShouldDraw(false);
				synthSynthesizerView::ClearModalComponent();
			}
		}
		else
		{
			if(IsUnderMouse() && 
				(synthUIPlane::GetMaster()->HadLeftDoubleClick() || ioKeyboard::KeyPressed(KEY_RETURN) || ioKeyboard::KeyPressed(KEY_NUMPADENTER))
					&& !m_ReadOnly)
			{
				if(m_IsBeingDragged)
				{
					m_IsBeingDragged = false;
					synthUIPlane::GetMaster()->SetIsMouseFixed(false);
				}

				if(synthSynthesizerView::SetModalComponent(&m_InputBox))
				{
					m_InputBox.Reset();
					//char buf[32];
					//formatf(buf, sizeof(buf), "%.2f", GetValue());
					//m_InputBox.SetValue(buf);

					if(m_TitleLabel.GetString() != NULL)
					{
						m_InputBox.SetTitle(m_TitleLabel.GetString());
					}
					m_InputBox.SetShouldDraw(true);
					m_InputBox.Update();
				}
			}
		}
	}

	void synthUIScrollbar::SetReadOnly(const bool readOnly)
	{
		m_ReadOnly = readOnly;
		if(readOnly && m_IsBeingDragged)
		{
			m_IsBeingDragged = false;
			synthUIPlane::GetMaster()->SetIsMouseFixed(false);
		}
	}
	void synthUIScrollbar::Render()
	{
		SetShaderVar("g_HighlightScale",IsUnderMouse()||IsBeingDragged() ? 1.f : 0.f);
		SetShaderVar("g_AlphaFade", m_Alpha);
		SetShaderVar("g_CurrentValue", m_CurrentValue);
		SetShaderVar("g_TextColour", m_HighlightColour);
		SetShaderVar("g_EdgeHighlightColour", m_HighlightColour);
		
		Matrix34 mtx = GetMatrix();
		Vector3 scalingVec = Vector3(m_Width,1.f,1.f);
		mtx.Transform3x3(scalingVec);
		mtx.Scale(scalingVec);
		grcWorldMtx(mtx);

		DrawVB(GetShader()->LookupTechnique("drawscrollbar", true));
		if(m_DrawReflection)
		{
			mtx.d -= mtx.b * ComputeHeight();
			grcWorldMtx(mtx);
			DrawVB(GetShader()->LookupTechnique("drawscrollbarrefl", true));
		}

		synthUIView::Render();
	}

}//namespace rage
#endif // __SYNTH_EDITOR
