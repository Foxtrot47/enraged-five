#include "audiohardware/mixer.h"
#include "math/amath.h"
#include "notchfilter.h"
#include "notchview.h"
#include "pin.h"
#include "vectormath/vectormath.h"

#define Undenormalize(sample) ((((*(u32*)&sample)&0x7f800000)==0)?0.0f:sample)

namespace rage
{
	using namespace Vec;

	SYNTH_EDITOR_LINK_VIEW(synthNotchFilter,synthNotchView);

	synthNotchFilter::synthNotchFilter() 
	{
		m_x1 = m_x2 = m_y1 = m_y2 = 0.f;

		m_MinFrequency = 0.f;
		m_MaxFrequency = 20000.f;
		
		m_Outputs[0].Init(this,"Output",synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Outputs[0].SetDataBuffer(&m_Buffer);
		
		m_Inputs[kNotchGain].Init(this, "Gain", synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kNotchGain].SetStaticValue(1.f);
		m_Inputs[kNotchCutoff].Init(this, "Cutoff",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kNotchCutoff].SetStaticValue(1.f);
		m_Inputs[kNotchQ].Init(this, "Q",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[kNotchQ].SetStaticValue(0.8f);
		m_Inputs[kNotchSignal].Init(this, "Input", synthPin::SYNTH_PIN_DATA_SIGNAL);
		m_Inputs[kNotchSignal].SetConvertsInternally(false);
	}

	void synthNotchFilter::SerializeState(synthModuleSerializer *serializer)
	{
		serializer->SerializeField("MinFreq", m_MinFrequency);
		serializer->SerializeField("MaxFreq", m_MaxFrequency);
	}
	
	//From http://www.musicdsp.org/showArchiveComment.php?ArchiveID=35
	void synthNotchFilter::Synthesize()
	{
		const f32 twoPiOverSampFreq = 0.00013089969389957473f;
		if(m_Inputs[kNotchSignal].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			// pass static value through
			m_Outputs[0].SetStaticValue(m_Inputs[kNotchSignal].GetStaticValue());
			return;
		}
		else
		{
			if(m_Inputs[kNotchCutoff].GetDataState() == synthPin::SYNTH_PIN_STATIC && m_Inputs[kNotchQ].GetDataState() == synthPin::SYNTH_PIN_STATIC)
			{
				m_Outputs[0].SetDataBuffer(&m_Buffer);

				const synthPin::synthPinDataFormat dataFormat = m_Inputs[kNotchSignal].GetDataFormat();
				m_Outputs[0].SetDataFormat(dataFormat);
				f32* samples = m_Inputs[kNotchSignal].GetDataBuffer()->GetBuffer();

				const f32 freq = m_Inputs[kNotchCutoff].GetNormalizedValue(0);
				const f32 Q = Clamp(m_Inputs[kNotchQ].GetNormalizedValue(0), 0.0f, 1.0f);

				const f32 x = cos(freq * twoPiOverSampFreq);
				const f32 a0 = (pow(1-Q, 2)/(2.f*(abs(x)+1.f))) + Q;
				const f32 a1 = -2.f * x * a0;
				const f32 a2 = a0;
				const f32 b1 = -2.f * x * Q;
				const f32 b2 = Q * Q;

				if(m_Inputs[kNotchGain].GetDataState() == synthPin::SYNTH_PIN_STATIC)
				{
					f32 gain = m_Inputs[kNotchGain].GetNormalizedValue(0);
					for(u32 j=0; j<m_Outputs[0].GetDataBuffer()->GetSize(); ++j)
					{
						f32 input = *samples;

						const f32 output = Clamp(a0*input + a1*m_x1 + a2*m_x2 - b1*m_y1 - b2*m_y2, -1.0f, 1.0f);

						m_y2 = m_y1;
						m_y1 = output;
						m_x2 = m_x1;
						m_x1 = input;

						samples++;
						m_Outputs[0].GetDataBuffer()->GetBuffer()[j] = (output * gain);
					}
				}
				else
				{
					for(u32 j=0; j<m_Outputs[0].GetDataBuffer()->GetSize(); ++j)
					{
						f32 input = *samples;

						const f32 output = Clamp(a0*input + a1*m_x1 + a2*m_x2 - b1*m_y1 - b2*m_y2, -1.0f, 1.0f);

						m_y2 = m_y1;
						m_y1 = output;
						m_x2 = m_x1;
						m_x1 = input;

						samples++;
						m_Outputs[0].GetDataBuffer()->GetBuffer()[j] = (output * m_Inputs[kNotchGain].GetNormalizedValue(j));
					}
				}
			}
			else
			{
				m_Outputs[0].SetDataBuffer(&m_Buffer);

				const synthPin::synthPinDataFormat dataFormat = m_Inputs[kNotchSignal].GetDataFormat();
				m_Outputs[0].SetDataFormat(dataFormat);
				f32* samples = m_Inputs[kNotchSignal].GetDataBuffer()->GetBuffer();

				for(u32 j=0; j<m_Outputs[0].GetDataBuffer()->GetSize(); ++j)
				{
					const f32 freq = m_Inputs[kNotchCutoff].GetNormalizedValue(j);
					const f32 Q = Clamp(m_Inputs[kNotchQ].GetNormalizedValue(j), 0.0f, 1.0f);

					const f32 x = cos(freq * twoPiOverSampFreq);
					const f32 a0 = (pow(1-Q, 2)/(2.f*(abs(x)+1.f))) + Q;
					const f32 a1 = -2.f * x * a0;
					const f32 a2 = a0;
					const f32 b1 = -2.f * x * Q;
					const f32 b2 = Q * Q;

					f32 input = *samples;

					const f32 output = Clamp(a0*input + a1*m_x1 + a2*m_x2 - b1*m_y1 - b2*m_y2, -1.0f, 1.0f);

					m_y2 = m_y1;
					m_y1 = output;
					m_x2 = m_x1;
					m_x1 = input;

					samples++;
					m_Outputs[0].GetDataBuffer()->GetBuffer()[j] = (output * m_Inputs[kNotchGain].GetNormalizedValue(j));
				}
			}

			m_y2 = Undenormalize(m_y2);
			m_y1 = Undenormalize(m_y1);
			m_x2 = Undenormalize(m_x2);
			m_x1 = Undenormalize(m_x1);	
		}//synthesize
	}
}
