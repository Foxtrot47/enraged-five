
#include "math/amath.h"
#include "anoscillator.h"
//#include "oscillatorview.h"

namespace rage
{
	char *synthAnalogOscillator::ModeStrings[NUM_ANOSCILLATOR_MODES] =
	{
		"SQUARE",
		"TRI",
		"SAW"
	};

	synthAnalogOscillator::synthAnalogOscillator() : synthModule(SYNTH_ANOSCILLATOR)
	{
		m_Inputs[AMPLITUDE].Init(this, "Amplitude",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[AMPLITUDE].SetStaticValue(1.f);
		m_Inputs[FREQUENCY].Init(this, "Frequency",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[FREQUENCY].SetStaticValue(0.f);
		m_Inputs[PHASE].Init(this, "Phase",synthPin::SYNTH_PIN_DATA_NORMALIZED);
		m_Inputs[PHASE].SetStaticValue(0.f);
		
		m_Output.Init(this, "Output", synthPin::SYNTH_PIN_DATA_SIGNAL, true);
		m_Output.SetDataBuffer(&m_Buffer);
		
		m_MinFrequency = 0.f;
		m_MaxFrequency = 20000.f;
		
		m_Mode = SQUARE;
		m_Phase = 0.f;

		m_LastSample = 0.f;
		m_P = 0.f;
	}
	
	synthAnalogOscillator::~synthAnalogOscillator()
	{
		
	}

	/*synthModuleView *synthOscillator::AllocateView()
	{
		return rage_new synthOscillatorView(this);
	}*/
	
	void synthAnalogOscillator::GetInputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Inputs[0];
		numPins = NUM_ANOSC_INPUTS;
	}
	
	void synthAnalogOscillator::GetOutputs(synthPin *&pins, u32 &numPins)
	{
		pins = &m_Output;
		numPins = 1;
	}
	
	void synthAnalogOscillator::Synthesize()
	{
		// 48,000 samples per second
		// phaseStep = freq / 48000
		
		f32 phaseStep = 0.f;
		f32 amp = 0.f;
		f32 freq = 0.f;
		
		f32 dc = 0.f;
		f32 dp = 1.1f;
		f32 pmax = 0.f;

		const f32 leak = 0.995f;

		if(m_Inputs[FREQUENCY].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			freq = m_MinFrequency + ((m_MaxFrequency - m_MinFrequency) * m_Inputs[FREQUENCY].GetStaticValue());
			phaseStep =  freq / 48000.f;

			pmax = 0.5f * 48000.f / freq;
			dc = -0.498f/pmax;
		}
		
			
		if(m_Inputs[AMPLITUDE].GetDataState() == synthPin::SYNTH_PIN_STATIC)
		{
			amp = m_Inputs[AMPLITUDE].GetStaticValue();
		}
		
		for(u32 i = 0 ; i < m_Output.GetDataBuffer()->GetSize(); i++)
		{
			f32 output = 0.f;

			if(m_Inputs[PHASE].IsConnected())
			{
				m_Phase = m_Inputs[PHASE].GetNormalizedValue(i);
			}
			else
			{		
				if(m_Inputs[FREQUENCY].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
				{
					freq = m_MinFrequency + ((m_MaxFrequency - m_MinFrequency) * m_Inputs[FREQUENCY].GetNormalizedValue(i));
					phaseStep =  freq / 48000.f;

					pmax = 0.5f * 48000.f / freq;
					dc = -0.498f/pmax;
				}
			}
			if(m_Inputs[AMPLITUDE].GetDataState() == synthPin::SYNTH_PIN_DYNAMIC)
			{
				amp = m_Inputs[AMPLITUDE].GetNormalizedValue(i);
			}
			
			switch(m_Mode)
			{
				case SQUARE:
				case TRIANGLE:
					{
					/*	f32 period = 48000.f/freq;
						f32 m=2*(s32)(period/2)+1.0f;
						f32 k=(s32)(k0*period);
						f32 g=0.99f/(period*(k/period)*(1-k/period));
						f32 t;
						float bpblit;
						float square=0.0f;
						float triangle=0.0f;
						for(t=1;t<=period;t++)
						{
							bpblit=rage::Sinf(PI*(t+1)*m/period)/(m*rage::Sinf(PI*(t+1)/period));
							bpblit-=rage::Sinf(PI*(t+k)*m/period)/(m*rage::Sinf(PI*(t+k)/period));
							if(m_Mode == BANDLIMITED_TRIANGLE)
							{
								square+=bpblit;
								output = square;
							}
							else
							{
								triangle+=g*(square+k/period);
								output = triangle;
							}
						}
						square=0;
						triangle=0;*/

						output = 0.f;
					}
					break;
				case SAW:
					{
						m_P += dp;
						if(m_P < 0.0f)
						{
							m_P = -m_P;
							dp = -dp;
						}
						else if(m_P > pmax)
						{
							m_P = pmax + pmax - m_P;
							dp = -dp;
						}

						const f32 x = Max(0.00001f,PI * m_P);
						output = m_LastSample = leak*m_LastSample + dc + Sinf(x)/x;
					}
					break;
				default:
					break;
			}

			if(!m_Inputs[PHASE].IsConnected())
			{
				m_Phase += phaseStep;
				while(m_Phase > 1.f)
				{
					m_Phase -= 1.f;
				}
			}
				
			m_Output.GetDataBuffer()->GetBuffer()[i] = output * amp;
		}
	}
	
}

