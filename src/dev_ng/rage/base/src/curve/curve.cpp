//
// curve/curve.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "curve.h"

#if ENABLE_UNUSED_CURVE_CODE

namespace rage {

cvCurveMgrBase* cvCurveMgrBase::sm_Inst = 0;

} // namespace rage

#endif // ENABLE_UNUSED_CURVE_CODE
