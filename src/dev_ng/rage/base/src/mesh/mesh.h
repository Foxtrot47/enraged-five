//
// mesh/mesh.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef MESH_MESH_H
#define MESH_MESH_H

#include "mesh_config.h"


#include "mesh/array.h"
#include "atl/queue.h"
#include "mesh/attribute.h"
#include "vector/color32.h"
#include "vector/matrix34.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "atl/bitset.h"
#include "atl/ptr.h"
#include "string/string.h"
#include "shaderlib/rage_constants.h"

namespace rage {

struct mshSerializer;
class fiBaseTokenizer;
class fiStream;

const int mshMaxTexCoordSets = 8;
const int mshMaxTanBiSets = 2;
const int mshMaxMatricesPerMaterial = 256;	// intentionally decoupled from SKINNING_COUNT, which is now a per-platform limit.
const int mshMaxFloatBlindData = 4;			// this needs to be a multiple of 4
const int mshMaxBlendTargets = 256;

class mshMesh;

#if MESH_LIBRARY

// If you need to add or reorder values here, keep in mind that the values are tabled in
// a few places: Serialize(mshSerializer&,mshPrimType) and mshRendererVgl::Begin
// Furthermore, we test if a primitive needs triangulation by checking against > mshTRIANGLES.

// Enumerant used to define the type of a particular mshPrimitive
enum mshPrimType { 
	mshPOINTS,		// Each index in primitive defines a point (min 1)
	mshLINES,		// Indices in primitive definite independent line segments (min 2, multiple of 2)
	mshTRIANGLES,	// Indices in primitive define a mesh of independent triangles (min 3, multiple of 3)
	mshTRISTRIP,	// Indices in primitive cumulatively define a normal-winding tristrip (min 3)
	mshTRISTRIP2,	// Indices in primitive cumulatively define a reverse-winding tristrip (min 3)
	mshQUADS,		// Indices in primitive define a mesh of independent quads (min 4, multiple of 4, in tristrip order).  If fourth index is zero, quad is a triangle.
	mshPOLYGON,		// Indices in primitive cumulative define a closed, convex polygon (min 3, as a trifan)
	FORCE_32 = 0x7FFFFFFF 
};

// Enumerant used to define the meaning of each entry in the vertex float blind data array.
// Add new ID's to the end of the list as necessary
enum mshVertexFloatBlindID
{
	mshVtxBlindUNASSIGNED = 0,
	mshVtxBlindPROJECTSPECIFIC1 = 1,
	mshVtxBlindPROJECTSPECIFIC2 = 2,
	mshVtxBlindPROJECTSPECIFIC3 = 3,
	mshVtxBlindPROJECTSPECIFIC4 = 4,
	mshVtxBlindPROJECTSPECIFIC5 = 5,
	mshVtxBlindPROJECTSPECIFIC6 = 6,
	mshVtxBlindPROJECTSPECIFIC7 = 7,
	mshVtxBlindPROJECTSPECIFIC8 = 8,
	mshVtxBlindCLOTHPIN = 9,
	mshVtxBlindCLOTH_WEIGHT = 10,
	mshVtxBlindCLOTHVTXGROUP = 11,
	mshVtxBlindCLOTHBOUNDID = 12,
	mshVtxBlindCLOTHPINTYPE = 13,
	mshVtxBlindCLOTHVTXGROUPTYPE = 14,
	mshVtxBlindCLOTH_INFLATION_SCALE = 15,
	mshVtxBlindCLOTH_CUSTOM_EDGE = 16,
	mshVtxBlindCLOTHOFFSETZ = 17,
	mshVtxBlindHAIRVECTORX = 18,
	mshVtxBlindHAIRVECTORY = 19,
	mshVtxBlindHAIRVECTORZ = 20,
	mshVtxBlindCLOTHPINRADIUS = 21,
	mshVtxBlindCLOTHPINRAMP = 22,
	mshVtxBlindCLOTHEDGECOMPRESSION = 23,
	mshVtxBlindCLOTHBENDSPRINGLENGTH = 24,
	mshVtxBlindCLOTHBENDSPRINGSTRENGTH = 25,
	mshVtxBlindCLOTH_MAPPING_THRESHOLD = 26,
	mshVtxBlindCLOTH_ERROR_THRESHOLD = 27,

	mshVtxBlindCLOTHPINRADIUSEXTRA0 = 30,
	mshVtxBlindCLOTHPINRADIUSEXTRA1 = 31,
	mshVtxBlindCLOTHPINRADIUSEXTRA2 = 32,
	mshVtxBlindCLOTHPINRADIUSEXTRA3 = 33,
	mshVtxBlindCLOTHPINRADIUSEXTRA4 = 34,
	mshVtxBlindCLOTHPINRADIUSEXTRA5 = 35,
	mshVtxBlindCLOTHPINRADIUSEXTRA6 = 36,
	mshVtxBlindCLOTHPINRADIUSEXTRA7 = 37,
	mshVtxBlindCLOTHPINRADIUSEXTRA8 = 38,
	mshVtxBlindCLOTHPINRADIUSEXTRA9 = 39,
};

inline mshPrimType mshFlipWinding(mshPrimType t) {
	return t==mshTRISTRIP? mshTRISTRIP2 : t==mshTRISTRIP2? mshTRISTRIP : t;
}

class mshMesh;

// Pure virtual renderer object.  Subclass this to provide rendering facilities
class mshRenderer {
public:
	mshRenderer() : Wireframe(false) { }
	virtual ~mshRenderer() { }

	// Bind material associated with attribute list
	virtual void Material(const mshArray<mshAttribute> &) const = 0;
	// Begin a primitive (like glBegin)
	virtual void Begin(mshPrimType,int count) const = 0;
	// Define a new current normal (like glNormal3f)
	virtual void Normal(float,float,float) const = 0;
	// Define a new current vertex color (like glColor4f)
	virtual void Color(float,float,float,float) const = 0;
	// Define a new current base texture coordinate (like glTexCoord2f)
	virtual void TexCoord0(float,float) const = 0;
	// Define a new current second-pass texture coordinate
	virtual void TexCoord1(float,float) const = 0;
	// Define a new vertex and sends it to renderer (like glVertex3f)
	virtual void Vertex(float,float,float) const = 0;
	// End a primitive (like glEnd)
	virtual void End() const = 0;

	void Vertex(const Vector3 &V) const { Vertex(V.x,V.y,V.z); }
	void Normal(const Vector3 &V) const { Normal(V.x,V.y,V.z); }
	void Color(const Vector4 &V) const { Color(V.x,V.y,V.z,V.w); }
	void TexCoord0(const Vector2 &V) const { TexCoord0(V.x,V.y); }
	void TexCoord1(const Vector2 &V) const { TexCoord1(V.x,V.y); }

	bool Wireframe;
};

const int mshMaxMatricesPerVertex = 4;

// A binding contains references to up to four matrices and
// four blend weights.  The weights always add up to 1.0f.
// If you have fewer than four matrices, set the unused weights 
// to zero and the indices will be ignored.
// <FLAG Component>
struct mshBinding {
	mshRangeArray<int,mshMaxMatricesPerVertex> Mtx;	// Up to four matrix indices
	mshRangeArray<float,mshMaxMatricesPerVertex> Wgt;			// Always adds up to 1.0
	bool IsPassThrough;
	u8 pad[3];
	
	mshBinding& operator=(const mshBinding b)
	{
		for( int i = 0; i < mshMaxMatricesPerVertex; i++ )
		{
			Mtx[i] = b.Mtx[i];
			Wgt[i] = b.Wgt[i];
		}

		IsPassThrough = b.IsPassThrough;
		return *this;
	}

	bool operator==(const mshBinding &that) const {
		for (int i=0; i<mshMaxMatricesPerVertex; i++)
			if (Mtx[i] != that.Mtx[i] || Wgt[i] != that.Wgt[i] || IsPassThrough != that.IsPassThrough)
				return false;
		return true;
	}
	void Reset() { 
		for (int i=0; i<mshMaxMatricesPerVertex; i++) {
			Mtx[i] = 0;
			Wgt[i] = i? 0.0f : 1.0f;
			IsPassThrough = false;
		}
	}
	template <class _T> void Init(_T& weights,int count,int maxMatrices = 3);
	void Serialize(mshSerializer &S);
};


/*
PURPOSE
Given an array of full skin influences, locate the top ones and
initialize the binding structure with them.
PARAMS
weights - Input array of influence weights, one per bone
count - Number of bones
maxMatrices - Max number of matrices per vert (should be 1-mshMaxMatricesPerVertex)
*/
template <class _T> void mshBinding::Init(_T& weights,int count,int maxMatrices) 
{
	Reset();
	FastAssert(maxMatrices >= 1 && maxMatrices <= mshMaxMatricesPerVertex);
	// The assert below is no longer valid as the mshRangeArray index has been upped from an unsigned char to an int
//	Assertf(count <= 255, "%d matrices is more than the upper limit of 255 matrices that can effect the vertices of one mesh", count); // We currently use a u8 for the matrix indices
	atBitSet used(count);
	for (int i=0; i<maxMatrices; i++) {
		int mtx = -1;
		float largest = 0;
		for (int j=0; j<count; j++) {
			if (!used.IsSet(j) && weights[j] > largest) {
				mtx = j;
				largest = weights[j];
			}
		}
		if (mtx == -1)	// no more nonzero weights found
			break;
		used.Set(mtx);
		Mtx[i] = mtx;
		Wgt[i] = largest;
		IsPassThrough = false;
	}
	// Renormalize all weights to add up to 1.0 again
	float sum = 0.0f;
	for (int i=0; i<mshMaxMatricesPerVertex; i++)
		sum += Wgt[i];
	if (sum) {
		sum = 1.0f / sum;
		for (int i=0; i<mshMaxMatricesPerVertex; i++)
			Wgt[i] *= sum;
	}
#if __ASSERT
	// Make sure the sort worked.
	for (int i=1; i<mshMaxMatricesPerVertex; i++)
		FastAssert(Wgt[i-1] >= Wgt[i]);
#endif
}

struct mshTangentBinormal {
	Vector3 T;
	Vector3 B;
	bool IsClose(const mshTangentBinormal &that,float tol) const {
		return T.IsClose(that.T,tol) && B.IsClose(that.B,tol);
	}
	bool operator==(const mshTangentBinormal& that) const
	{
		return ((T == that.T) && (B == that.B));
	}
};

struct mshMaterial;

// A list of indices of the same type defining one or more shapes.
// <FLAG Component>
struct mshPrimitive {
	mshPrimType Type;
	mshIndex Priority;
	mshArray<mshIndex> Idx;
	bool Truncate(mshPrimitive &first,mshPrimitive &second,int count) const;
	void RemoveDegenerates(const mshMaterial &M,mshPrimitive &output) const;
	bool Triangulate(const mshMaterial &M,mshPrimitive &output,const Vector3 &boxMin) const;
	bool Tristrip(const mshMaterial &M,mshArray<mshPrimitive> &output) const;
	bool Quadrify(const mshMaterial &M,mshPrimitive &output) const;
	void Draw(const mshMaterial &M,const mshRenderer &R) const;
	void DrawSkinned(const mshMaterial &M,const mshRenderer &R,const Matrix34 *matrices) const;
	void DrawNormals(const mshMaterial &M, const mshRenderer &R, Color32 &c, float length) const;
	void DrawTangents(const mshMaterial &M, const mshRenderer &R, Color32 &c, float length) const;
	void DrawBinormals(const mshMaterial &M, const mshRenderer &R, Color32 &c, float length, bool shaderCompute) const;
	int GetSubPrimitiveCount() const;
	void Serialize(mshSerializer &S);
	int GetTriangleCount() const;

	mshPrimitive& operator=(const mshPrimitive& p)
	{
		Type = p.Type;
		Priority = p.Priority;
		Idx.Resize(p.Idx.GetCount());
		for( int i = 0; i < p.Idx.GetCount(); i++ )
			Idx[i] = p.Idx[i];
		return *this;
	}
};

struct mshBlendTargetDelta {
	Vector4		Position;
	Vector4		Normal;
	Vector4		Tangent;
	Vector4		BiNormal;

	bool operator==(const mshBlendTargetDelta& that) const
	{
		return (Position == that.Position &&
				Normal == that.Normal && 
				Tangent == that.Tangent &&
				BiNormal == that.BiNormal);
	}
};

// PURPOSE:
//	The vertex structure for a mesh object.	
// NOTES:
//	Here's what an example vertex looks like in an ascii .mesh file:
//		-26 -5.8745117 47.999512 / 1 0 0 / 0 1 0 / 1 1 1 1 2 0.703125 0.9375 0 0 / 1 1 0 0 0 0 -1 / 0 0 0 0 0 
//  Here's a description of the elements in the vertex:
//		POSITION / BINDING / NORMAL / CPV,TEXCOUNT,TEXTURE UVS / TAN_BI_COUNT, TANGENTS AND BINORMALS / FLOAT BLIND DATA COUNT, FLOAT BLIND DATAS
struct mshVertex {
	static u8 mshMeshVersion;
	static u8 mshBlendTargetCount;

	// SIZE OF THIS STRUCTURE MUST BE MULTIPLE OF FOUR BYTES
	mshVertex() : Pos(0,0,0), Nrm(0,0,0), Cpv(1,1,1,1), Cpv2(1,1,1,1), Cpv3(1,1,1,1), TexCount(0), TanBiCount(0), BlendTargetDeltas(0) { 
		for (int i=0; i<mshMaxTexCoordSets; i++) {
			Tex[i].Zero();
		}
		for (int i=0; i<mshMaxTanBiSets; i++) {
			TanBi[i].T.Zero();
			TanBi[i].B.Zero();
		}
		for (int i=0; i<mshMaxFloatBlindData; i++) {
			FloatBlindData[i] = 0.0f;
		}

		FloatBlindDataCount=0;
		Binding.Reset();
	}

	~mshVertex()
	{
		if( BlendTargetDeltas )
			delete[] BlendTargetDeltas;
	}

	mshVertex& operator=(const mshVertex& v);
	bool operator==(const mshVertex& v) const;

	Vector3 Pos;
	mshBinding Binding;
	Vector3 Nrm;
	Vector4 Cpv, Cpv2, Cpv3;
	int TexCount, TanBiCount;
	mshRangeArray<Vector2,mshMaxTexCoordSets> Tex;
	mshRangeArray<mshTangentBinormal,mshMaxTanBiSets> TanBi;

	int FloatBlindDataCount;
	mshRangeArray<float, mshMaxFloatBlindData> FloatBlindData;

	mshBlendTargetDelta* BlendTargetDeltas;
	//mshRangeArray<mshBlendTargetDelta, mshMaxBlendTargets> BlendTargetDeltas;

	void Serialize(mshSerializer &S);
};

union mshChannelData {
	s32 s;
	u32 u;
	float f;
};

class mshChannel {
public:
	mshChannel() : Name(0), Data(NULL), VertexCount(0), FieldCount(0) { }
	~mshChannel() { delete[] Data; }
	mshChannel(const mshChannel &that) { CopyFrom(that); }
	mshChannel& operator=(const mshChannel &that) { delete[] Data; CopyFrom(that); return *this; }
	void Init(u32 name,int vertexCount,int fieldCount) {
		delete[] Data;
		Name = name;
		Data = rage_new mshChannelData[vertexCount * fieldCount];
		VertexCount = vertexCount;
		FieldCount = fieldCount;
	}
	u32 GetName() const { return Name; }
	int GetVertexCount() const { return VertexCount; }
	int GetFieldCount() const { return FieldCount; }
	mshChannelData& Access(int v,int f) { 
		FastAssert(v>=0&&v<VertexCount);
		FastAssert(f>=0&&f<FieldCount);
		return Data[v*FieldCount+f];
	}
	const mshChannelData& Access(int v,int f) const { 
		FastAssert(v>=0&&v<VertexCount);
		FastAssert(f>=0&&f<FieldCount);
		return Data[v*FieldCount+f];
	}
	void Resize(int newVertexCount) {
		mshChannelData *newData = rage_new mshChannelData[newVertexCount * FieldCount];
		int toCopy = VertexCount;
		if (toCopy > newVertexCount)
			toCopy = newVertexCount;
		toCopy *= FieldCount;
		for (int i=0; i<toCopy; i++)
			newData[i] = Data[i];
		delete[] Data;
		Data = newData;
        VertexCount = newVertexCount;
	}
	void Serialize(mshSerializer&);
private:
	void CopyFrom(const mshChannel &that) {
		Name = that.Name;
		VertexCount = that.VertexCount;
		FieldCount = that.FieldCount;
		int count = VertexCount * FieldCount;
		Data = rage_new mshChannelData[count];
		for (int i=0; i<count; i++)
			Data[i] = that.Data[i];
	}
	u32 Name;
	mshChannelData *Data;
	int VertexCount, FieldCount;
};

// A container class which associates a list of shading attributes with a list of primitives.
// <FLAG Component>
struct mshMaterial {
	mshMaterial() : Priority(32), TexSetCount(0), TanBiSetCount(0) { }
	ConstString Name;
	mshArray<mshPrimitive> Prim;
	mshArray<mshAttribute> Attr;
	mshArray<mshChannel> Channels;
#if !__TOOL
private:
#endif
	mshArray<mshVertex> Verts;
public:
	mshMaterial& operator=(const mshMaterial& m);

	void SetVertexCount(int c) { Verts.Resize(c); }
	int GetVertexCount() const { return Verts.GetCount(); }

	void SetPos(int i,const Vector3 &v) { Verts[i].Pos=v; }
	const Vector3& GetPos(int i) const { return Verts[i].Pos; }

	void SetNormal(int i,const Vector3 &v) { Verts[i].Nrm=v; }
	const Vector3& GetNormal(int i) const { return Verts[i].Nrm; }

	void SetBinding(int i,const mshBinding &v) { Verts[i].Binding=v; }
	const mshBinding& GetBinding(int i) const { return Verts[i].Binding; }

	void SetTexCoord(int i,int j,const Vector2 &v) { Verts[i].Tex[j]=v; }
	const Vector2& GetTexCoord(int i,int j) const { return Verts[i].Tex[j]; }

	void SetTangent(int i,int j,const Vector3 &v) { Verts[i].TanBi[j].T=v; }
	const Vector3& GetTangent(int i,int j) const { return Verts[i].TanBi[j].T; }

	void SetBinormal(int i,int j,const Vector3 &v) { Verts[i].TanBi[j].B=v; }
	const Vector3& GetBinormal(int i,int j) const { return Verts[i].TanBi[j].B; }

	void SetCpv(int i,const Vector4 &v) { Verts[i].Cpv=v; }
	const Vector4& GetCpv(int i) const { return Verts[i].Cpv; }

	void SetCpv2(int i,const Vector4 &v) { Verts[i].Cpv2=v; }
	const Vector4& GetCpv2(int i) const { return Verts[i].Cpv2; }

	void SetCpv3(int i,const Vector4 &v) { Verts[i].Cpv3=v; }
	const Vector4& GetCpv3(int i) const { return Verts[i].Cpv3; }

	void SetBlendTargetDelta(int vertIndex, int targetIndex, const mshBlendTargetDelta& delta)	{ Verts[vertIndex].BlendTargetDeltas[targetIndex] = delta; }
	const mshBlendTargetDelta& GetBlendTargetDelta(int vertIndex, int targetIndex) const		{ return Verts[vertIndex].BlendTargetDeltas[targetIndex]; }

	// Use these as a last resort
	void SetVertex(int i,const mshVertex &v) { Verts[i]=v; }
	const mshVertex& GetVertex(int i) const { return Verts[i]; }

	bool IsSingleBoneSkinned() const;

	void AddChannel(u32 name,int fieldCount) {
		Channels.Append().Init(name,GetVertexCount(),fieldCount);
	}

	void DeleteChannel(int index) {
		Channels.Delete(index);
	}

	int FindChannel(u32 name) const {
		for (int i=0; i<Channels.GetCount(); i++)
			if (Channels[i].GetName() == name)
				return i;
		return -1;	
	}

	mshChannel& GetChannel(int i) { FastAssert(Channels[i].GetVertexCount()==GetVertexCount()); return Channels[i]; }
	const mshChannel& GetChannel(int i) const { FastAssert(Channels[i].GetVertexCount()==GetVertexCount()); return Channels[i]; }

	int GetChannelCount() const { return Channels.GetCount(); }

	int GetRawChannelData(int vert, mshChannelData* outData);

	mshIndex Priority;
	int TexSetCount, TanBiSetCount;

	int AddVertex(
		const Vector3 &pos,
		const mshBinding &binding,
		const Vector3 &nrm,
		const Vector4 &cpv,
		const Vector4 &cpv2,
		const Vector4 &cpv3,
		int tcCount,
		const Vector2 *tcs,
		int fbdCount = 0,
		const float *fbd = NULL);

	int AddVertex(const mshVertex& vertex);

	void Draw(const mshRenderer &R) const;
	void DrawSkinned(const mshRenderer &R,const Matrix34 *matrices) const;
	void DrawNormals(const mshRenderer &R, Color32 &c, float length) const;
	void DrawTangents(const mshRenderer &R, Color32 &c, float length) const;
	void DrawBinormals(const mshRenderer &R, Color32 &c, float length, bool shaderCompute) const;
	bool Triangulate();
	bool Tristrip();
	void CacheOptimize();
	void ReorderVertices();
	int Weld();

	int WeldWithoutQuantize();

	void ComputeNormals();
	void ComputeTanBi(int set);
	// New method found on the net -- currently in R&D phase
	void ComputeTanBi2(int set);

	void ResetVerts();
	void RemoveDegenerates();
	void RemoveUnreferenced();
	void Reverse();
	void Serialize(mshSerializer &S);
	int GetTriangleCount() const;

	bool Merge(const mshMaterial & that);
	void Packetize(mshArray<mshMaterial> &newMtls,int finalVtxCount,int finalMtxCount) const;
	
	void BackfaceCull(const Vector3 &eyePoint);

	void CopyNongeometricData(mshMaterial& dest) const;

	// For read-only iteration over all triangles
	struct TriangleIterator {
		void GetVertIndices(int& a, int& b, int& c);
		int m_CurrPrim;
		int m_CurrTri;
		mshMaterial* m_Material;

		TriangleIterator& operator++ ();
		bool operator==(const TriangleIterator& other) const;
		bool operator!=(const TriangleIterator& other) const {
			return !(*this == other);
		}
	};

	TriangleIterator BeginTriangles();
	TriangleIterator EndTriangles();

	//
	bool operator ==( const mshMaterial & that ) 					{ return ( !stricmp( Name, that.Name ) && Priority == that.Priority ) ; }

	bool SameNongeometricData( mshMaterial & that );
};


// Mesh class which is heavily indexed; it contains a list of materials, which in turn contain
// a list of primitives.  Each primitive is defined in terms of indices specified in this mesh
// class.  Each index references an adjunct, and an adjunct in turn references distinct positions,
// normals, colors, and texture coordinates (collectively referred to as channels).  The mesh class 
// provides operations for adding and deleting channels, as well as welding and flattening the
// indexing.
// <FLAG Component>
class mshMesh {
	friend struct mshMaterial;
	friend struct mshPrimitive;
	friend struct mshEdgeMap;
public:
	mshMesh(bool skinned = false);

	mshMesh& operator=(const mshMesh& m);

	void Concatenate(const mshMesh& m);

	void Reset();
	void MakeSkinned();
	void MakeRigid();

	void Draw(const mshRenderer &R) const;
	void DrawSkinned(const mshRenderer &R,const Matrix34 *matrices) const;
	void DrawNormals(const mshRenderer &R, Color32 &c, float length) const;
	void DrawTangents(const mshRenderer &R, Color32 &c, float length) const;
	// Set shaderCompute to true if you want to see the binormals used by the default rage shaders
	void DrawBinormals(const mshRenderer &R, Color32 &c, float length, bool shaderCompute=false) const;
	void Serialize(mshSerializer &S);

	mshIndex AddOffset(const Vector3 &V);

	int Weld();

	// PURPOSE: Similar to Weld, but doesn't quantize verts first. Instead it runs "approximately equal" checks on the
	// verts. This should prevent issues where nearby verts fall on either side of a quantization boundary, but
	// will run slower than regular Weld. Uses full dimensionality of verts to test for approx. equality.
	int WeldWithoutQuantize();

	int GetTriangleCount() const;
	int GetVertexCount() const;

	void MergeSameMaterials();
	void CleanModel();
	void Triangulate();
	void Tristrip();
	void CacheOptimize();
	void ReorderVertices();
	void RemoveDegenerates();
	void RemoveUnreferenced();
	void Reverse();
	// PURPOSE: normalizes colors so that they're within 0.0-1.0.
	void NormalizeColors();

	// PURPOSE: Snaps all verts in the mesh within tolerance of plane onto the plane
	void SnapToPlane(const Vector4& plane, float tolerance);

	// PURPOSE: Clears all attributes on a mesh except for position (cpv, normal, tex coords, tangent binormals, bindings all reset)
	void ClearVtxAttributes();

	void ComputeBoundingBox(Vector3 &outMin,Vector3 &outMax) const;

	void ComputeNormals();
	void ComputeTanBi(int set);

	int GetMtlCount() const { return Mtl.GetCount(); }
	const mshMaterial& GetMtl(int i) const { return Mtl[i]; }
	mshMaterial& GetMtl(int i) { return Mtl[i]; }

	void AddMtl(const mshMaterial &mtl) { Mtl.Append() = mtl; }
	mshMaterial& NewMtl() { return Mtl.Append(); }

	void BackfaceCull(const Vector3 &eyePoint);

	bool IsSkinned() const { return Skinned != 0; }
	bool IsSingleBoneSkinned() const;
	int GetMatrixCount() const { return Offset.GetCount(); }
	const Vector3& GetOffset(int i) const { return Offset[i]; }

	void MakeBoneRelative();

	void SetBoundSphere(const Vector4 &v) { Sphere = v; }
	const Vector4 &GetBoundSphere() const { return Sphere; }
	void SetBoundBox(const Matrix34 &mtx,const Vector3 &size) {
		BoxMatrix = mtx;
		BoxSize = size;
	}
	const Matrix34 &GetBoundBoxMatrix() const { return BoxMatrix; }
	const Vector3 &GetBoundBoxSize() const { return BoxSize; }

	mshVertexFloatBlindID GetFloatBlindID(int idx) const {return (mshVertexFloatBlindID)FloatBlindDataIds[idx]; }
	void SetFloatBlindID(int idx, mshVertexFloatBlindID id) { FloatBlindDataIds[idx] = (int)id; }
	int GetNumFloatBlindChannels() const { return mshMaxFloatBlindData; }

	void SetBlendTargetCount(int count)		{ BlendTargetCount = (u8)count; mshVertex::mshBlendTargetCount = BlendTargetCount; }
	int GetBlendTargetCount() const			{ return (int)BlendTargetCount; }

	void SetBlendTargetName(int targetIndex, const char* name) { BlendTargetNames[targetIndex] = ConstString(name); }
	const char* GetBlendTargetName(int targetIndex) const { return BlendTargetNames[targetIndex]; }

	void Packetize(int (*cb)(const mshMesh &mesh,const mshMaterial &mtl,void *closure),void *closure,int maxMtxCount = 0);

protected:
	mshArray <mshMaterial> Mtl;		// Material and primitives
	mshArray <Vector3> Offset;		// Global offset for converting bone-relative to model-relative
	Vector4 Sphere;					// optimized bound sphere
	Matrix34 BoxMatrix;				// optimized bound box (position and orientation only)
	Vector3 BoxSize;				// optimized bound box size
	u8 Skinned, BlendTargetCount, pad1, pad2;

	mshRangeArray<int, mshMaxFloatBlindData> FloatBlindDataIds;
	mshRangeArray<ConstString, mshMaxBlendTargets> BlendTargetNames;
};

void Serialize(mshSerializer &S,mshMesh &v);

#endif		// MESH_LIBRARY

}	// namespace rage

#endif
