###############################################################################
#                 Build a CSV file of all .mesh files given a directory
###############################################################################

use strict;
use Cwd;
use mesh;
use Data::Dumper;

my $dir = "t:\\agent\\assets\\"; # default path, if none is given
#$dir = "T:\\agent\\assets\\level\\dc_dist\\DC_chemco_180\\";

if ($ARGV[0])
{
      $dir = $ARGV[0];
}
else
{
      print "USAGE : findNearQuads.pl topLevelDirectoryNameOfMeshFiles\n\n";
      print "EXAMPLE : findNearQuads.pl t:\\agent\\assets\\\n";
      exit;
}

if (!-e $dir)
{
      print "Directory ($dir) is not found!\n";
      exit;
}

print "Grabbing all .mesh files, recursively in $dir...\nPlease be patient...\n";
my $oldDir = cwd();	# save off the original current working dir
chdir($dir);	 	
my $command = 'for /r %F in (*.mesh) do @echo %F %~nF %~dF %~pF';
my $res = `$command`;
my @files = split(/\n/, $res);
chdir ($oldDir);	# restore original directory

my $total = @files;
my $counter=0;
open (OUTFILE, ">look.csv");

      my @titles;
      push (@titles,"TotalPrimitives");
      push (@titles,"TriangleCount");
      push (@titles,"MinTristripLength");
      push (@titles,"AverageTristripLength");
      push (@titles,"MaxTristripLength");
      push (@titles,"TotalNearMappedQuads");
      push (@titles,"PercentOfQuadsWhichAreNearMapped");
      push (@titles,"TotalFullMappedQuads");
      push (@titles,"PercentOfQuadsWhichAreFullMapped");
      push (@titles,"TotalOverMappedQuads");
      push (@titles,"PercentOfQuadsWhichAreOverMapped");
      push (@titles,"TotalMaterial");
      push (@titles,"TotalPos");
      push (@titles,"TotalPosSkin");
      push (@titles,"TotalNrm");
      push (@titles,"TotalCpv");
      push (@titles,"TotalTex0");
      push (@titles,"TotalTex1");
      push (@titles,"TotalAdj");
      push (@titles,"TotalPck");
      push (@titles,"TotalMissingPck");
      push (@titles,"StripHistogram");
      push (@titles,"MaterialNameList");
      push (@titles,"MaterialHistogram");
      push (@titles,"PrimTypeHistogram");
      push (@titles,"error");
      push (@titles,"warning");
      push (@titles,"status");

###############################################################################
#              print out the CSV header titles
###############################################################################

print OUTFILE "FullPath,Path,BaseName,";
foreach my $item (@titles)
{
      print OUTFILE "\"$item\",";
}
print OUTFILE "\n";

###############################################################################
#             Jam thru each .mesh file and output a line 
#              in the excel file
###############################################################################

foreach my $line (@files)
{
	$line =~ m/^(.+\.mesh)\s+(.+)\s+(.:)\s+(.+)$/i;

	my $fullFileName = $1;
	my $baseName = $2;
	my $drive = $3;
	my $path = $4;

	$counter++;
        if (1) #$fullFileName =~ m/polySurface6969/i) 
        {
               print "($counter of $total)\n$fullFileName\n";
               my $meshObject = new mesh();
               $meshObject->loadFile($fullFileName);

               my $statHashPtr = $meshObject->generateStatHash();
               my $pckFile = "$fullFileName.pck";
               if (-e "$fullFileName.pck")
               {
                     print "PCK found\n";
                     my $filesize = -s $pckFile;
                     print "FILE SIZE ($filesize)\n";
                     $statHashPtr->{TotalPck} = $filesize;
               }
               else
               {
                     if ($statHashPtr->{TotalMissingPck} eq undef) 
                     {
                           $statHashPtr->{TotalMissingPck} = 0;
                     }
                     $statHashPtr->{TotalMissingPck}++;
                     print "PCK not found\n";
               }
               print OUTFILE "$fullFileName,file:\\\\$drive$path,$baseName,";
               foreach my $item (@titles)
               {
                     my $value = $statHashPtr->{$item};
                     my $itemType = ref $statHashPtr->{$item};
                     if ($itemType =~ m/HASH/) 
                     {
                           my $histHashPtr = $statHashPtr->{$item};
                           my @keys = sort(keys %$histHashPtr);
                           $value="";
                           foreach my $len (@keys)
                           {
                                 my $count = $histHashPtr->{$len};
                                 $value .= "[$len:$count] ";          
                           }
                     }
                     if ($itemType =~ m/ARRAY/) 
                     {
                           $value="";
                           # print out Material Name List
                           my $materialNameListPtr = $statHashPtr->{$item};
                           foreach my $matName (@$materialNameListPtr)
                           {
                                 $matName =~ s/\[//ig;
                                 $matName =~ s/]//ig;
                                 $value .="[$matName] ";
                           }
                     }
                     print OUTFILE "$value,";
               }
               print OUTFILE "\n";
               $meshObject = undef;
         }
}

print "Total Files ($counter)\n";
close (OUTFILE);
exit;

