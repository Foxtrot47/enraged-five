//
// mesh/array.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef MESH_ARRAY_H
#define MESH_ARRAY_H

#include "system/new.h"

namespace rage {

// broken out so that it can be selectively disabled separately if it's a time sink
#ifndef mshArrayAssert
#define mshArrayAssert(x)	FastAssert(x)
#endif

#if __WIN32
#pragma warning(disable: 4505)                          //warning: unreferenced local function has been removed
#pragma warning(disable: 4710)                          //warning: function not inlined 
#endif

typedef int mshIndex;
const mshIndex mshUndefined = 0x7FFFFFFF;

/*
	mshArray is identical to atArray except that its maximum size is 2G elements instead of 64K elements
	on PC builds.  This allows us to handle *very* large meshes if necessary (such as when generating
	spatial subdivision trees for large levels).  It also has a larger allocation step.
*/
template <class T,int allocStep = 256>		
class mshArray {
	CompileTimeAssert((allocStep & -allocStep) == allocStep);	// allocStep MUST be a power of two
public:
	/* Default constructor.  Array is left totally empty */
	mshArray() : m_Elements(0), m_Count(0), m_AllocCount(0) { }

	/* Intialize array allocation to specified size, but array still contains zero elements
		PARAMS: baseCount - Number of entries to preallocate */
	mshArray(int baseCount) : m_Elements(rage_new T[baseCount]), m_Count(0), m_AllocCount((mshIndex) baseCount) { }

	/* Intended for classes that need resource compiler support */
	mshArray(bool initialize) {if (initialize) {m_Elements=0; m_Count=0; m_AllocCount=0;}}

	/* Default destructor */
	~mshArray() { Kill(); }

	/* Construct an array from another array.  Original array is unchanged.
		PARAMS: that - Array to copy initial data from */
	mshArray(const mshArray& that) { CopyFrom(that); }

	/* Copy an array from another array.  Other array is unchanged, this array has its contents replaced.
		PARAMS: that - Array to copy data from */
	mshArray& operator=(const mshArray& that) { if (this != &that) { Kill(); CopyFrom(that); } return *this; }

	/* Assume ownership of an array without doing a copy, destroying the other array in the process.
		PARAMS: that - Array to assume control of.  The array is left empty.
		NOTES: Useful when doing batch delete operations */
	void Assume(mshArray& that) {
		if (this != &that) { 
			Kill(); 
			m_Elements = that.m_Elements; m_Count = that.m_Count; m_AllocCount = that.m_AllocCount;
			that.m_Elements = 0; that.m_Count = that.m_AllocCount = 0;
		}
	}

	/* Access array, with range checking */
	T& operator[](int index) { mshArrayAssert(index>=0&&index<m_Count); return m_Elements[index]; }

	/* Access array, with range checking */
	const T& operator[](int index) const { mshArrayAssert(index>=0&&index<m_Count); return m_Elements[index]; }

	/* RETURNS: Number of valid items in the array.  Not necessarily the same as the number of entries allocated */
	int GetCount() const { return m_Count; }

	/* RETURNS: Number of valid items in the array.  Not necessarily the same as the number of entries allocated */
	int operator() () const { return m_Count; }

	/* RETURNS: Number of items preallocated in the array.  Not necessarily the same as the number of items in the array*/
	int GetAllocCount() const { return m_AllocCount; } // Current Capacity

	/* Initializes array with a given size
		PARAMS: count - New size for array */
	void Init(int count) {
		FastAssert((count >= 0) && (count < mshUndefined));
		if (m_AllocCount != count) {
			delete[] m_Elements;
			m_Elements = rage_new T[count];
		}
		m_AllocCount = m_Count = (mshIndex) count;
	}

	/* Initializes array to a given value and a given size
		PARAMS: t - Item to initialize array with
				count - New size for array */
	void Init(const T& t,int count) {
		Init(count);
		for (int i=0; i<count; i++)
			m_Elements[i] = t;
	}

	/* Clears array and reclaims its storage   PARAMS: New preallocation size, defaults to zero */
	void Reset(int size = 0) {
		if(m_AllocCount)
		{
			delete[] m_Elements;
			m_Elements = 0; 
			m_Count = 0;
		}
		if (size)
			m_Elements = rage_new T[m_AllocCount = (mshIndex) size];
		else
			m_AllocCount = 0;
	}

	/* Resizes an array, preserving its content. 
		PARAMS: newCount - New number of elements in the array
				newAllocCount - New number of elements to allow growth for the array
		RETURNS: Pointer to raw storage for the array
		NOTES: Both the count and the preallocated size are set to newCount.
			If newCount is smaller than the number of elements previously
			in the array, the extra entries are lost.  The array is copied
			if the size is reduced or not enough space exists for rage_new growth.
		SEE ALSO: Resize */
	T* Reallocate(int newCount, int newAllocCount = 0) {
		T* newArray = 0;
		bool bCopy = false;
		if ((newCount > 0)  || (newAllocCount > 0))
		{
			if ((newCount > m_AllocCount) || (newCount < m_Count))
			{
				newArray = rage_new T[(newCount < newAllocCount) ? newAllocCount : newCount];
				bCopy = true;
			}
		}
		const int minCount = newCount < m_Count? newCount : m_Count;
		if (bCopy)
		{
			for (int i=0; i<minCount; i++)
				newArray[i] = m_Elements[i];
		}
		FastAssert(newCount < mshUndefined);
		m_Count	= (mshIndex) minCount;
		if (bCopy || (newCount == 0))
		{
			m_AllocCount = (mshIndex)(((newCount < newAllocCount) && (newCount != 0)) ? newAllocCount : newCount);
			delete[] m_Elements;
			return m_Elements = newArray;
		}
		return m_Elements;
	}

	/* Resizes an array, preserving its content
		PARAMS: newCount - New number of elements in the array
		NOTES: If you are shrinking the array, the array is not copied.
	*/
	void Resize(int newCount, int newAllocCount = 0) {
		FastAssert((newCount >= 0) && (newCount < mshUndefined));
		//if (newCount > m_Count) 
			Reallocate(newCount, newAllocCount);
		m_Count = (mshIndex) newCount;
	}

	void GrowBy(int delta) {
		if (m_AllocCount < m_Count + delta)
			Reallocate(m_Count + delta);
	}

	/* RETURNS: Reference to a newly appended array element. */
	T& Append() { 
		if (m_Count == m_AllocCount) 
			Reallocate(m_AllocCount + allocStep); 
		return m_Elements[m_Count++];
	}

	/* Adds two elements at once
		PARAMS: a, b - Elements to add */
	void Append2(const T& a,const T& b) {
		if(m_Count + 2 >= m_AllocCount)
			Reallocate(m_AllocCount + allocStep);
		m_Elements[m_Count] = a;
		m_Elements[m_Count+1] = b;
		m_Count += 2;
	}

	/* Adds three elements at once
		PARAMS: a, b, c - Elements to add */
	void Append3(const T& a,const T& b,const T& c) {
		if(m_Count + 3 >= m_AllocCount)
			Reallocate(m_AllocCount + allocStep);
		m_Elements[m_Count] = a;
		m_Elements[m_Count+1] = b;
		m_Elements[m_Count+2] = c;
		m_Count += 3;
	}

	/* Adds four elements at once
		PARAMS: a, b, c, d - Elements to add */
	void Append4(const T& a,const T& b,const T& c,const T& d) {
		if(m_Count + 4 >= m_AllocCount)
			Reallocate(m_AllocCount + allocStep);
		m_Elements[m_Count] = a;
		m_Elements[m_Count+1] = b;
		m_Elements[m_Count+2] = c;
		m_Elements[m_Count+3] = d;
		m_Count += 4;
	}

	/* Inserts an empty slot into an existing array
		PARAMS: beforeIndex - Array index to insert an empty slot before
		RETURNS: Reference to the empty slot
		NOTES: The slot is not really empty.  It will contain whatever
			array data happened to be there before. */
	T& Insert(int beforeIndex) {
		if (m_Count == m_AllocCount)
			Reallocate(m_AllocCount + allocStep);
		mshArrayAssert(beforeIndex>=0 && beforeIndex <= m_Count);
		for (int i=m_Count; i>beforeIndex; i--)
			m_Elements[i] = m_Elements[i-1];
		++m_Count;
		return m_Elements[beforeIndex];
	}

	/* Deletes array element at specified index
		PARAMS: index - Index of array element to delete */
	void Delete(int index) {
		mshArrayAssert(index>=0 && index < m_Count);
		for (int i=index; i<m_Count-1; i++)
			m_Elements[i] = m_Elements[i+1];
		--m_Count;
	}

	/* Access array, with range checking, ensuring existence of index
		PARAMS: index - index of array element to access/add if necessary
		NOTES: this can introduce a div and mod every access in the worst case, shouldn't happen in practice */
	T& SafeAccess(int index) {
		mshArrayAssert(index>=0);
		if(m_AllocCount <= index)
		{
#if __WIN32
			div_t q = div(index, allocStep);
			Reallocate( m_AllocCount + ((allocStep * (q.quot)) + allocStep - (q.rem)) );
#else
			Reallocate( m_AllocCount + ((allocStep * (index / allocStep)) + allocStep - (index % allocStep)) );
#endif
		}
		if(m_Count <= index)
			m_Count = (mshIndex) (index + 1);
		return m_Elements[index];
	}

protected:
	/* Internal copy helper.  Allocates array of same size and copies elements.
		PARAMS: that - Array to copy */
	void CopyFrom(const mshArray &that) {
		m_AllocCount = m_Count = that.m_Count;
		if(m_AllocCount)
			m_Elements = rage_new T[m_AllocCount]; //lint !e672 possible memory leak in assignment to pointer
		else
			m_Elements = NULL;
		for (int i=0; i<m_Count; i++)
			m_Elements[i] = that.m_Elements[i];
	}
	/* Internal cleanup function.  Reclaims all heap memory associated with elements */
	void Kill() { if(m_AllocCount) delete[] m_Elements; }
	/* The raw array of elements */
	T* m_Elements;
	mshIndex m_Count, /* Number of valid items in the array */
		m_AllocCount;		/* Total number of entries allocated in the array.  Always greater than or equal to m_Count. */
};

template <class T,int size>
class mshRangeArray {
public:
	T& operator[](int index) {
		mshArrayAssert(index>=0&&index<size);
		return m_Elements[index];
	}
	const T& operator[](int index) const {
		mshArrayAssert(index>=0&&index<size);
		return m_Elements[index];
	}
	static int GetCount() {return size;}

private:
	T m_Elements[size];
};

}	// namespace rage

#endif
