//
// mesh/material.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mesh.h"

#if MESH_LIBRARY

#include "serialize.h"

#include "vector/geometry.h"

#if __TOOL || __RESOURCECOMPILER
#include <set>
#endif

#define USE_TOOTLE ((__RESOURCECOMPILER || __TOOL || __WIN32PC) && 0)

#if USE_TOOTLE
#include "../../../3rdParty/amd_tootle_2_0/include/tootlelib.h"
#include "system/param.h"
//#include "grcore/config.h"

#if 0 //__D3D
	#if RSG_CPU_X64
		#if __OPTIMIZED
			#pragma comment(lib, "../../../3rdParty/amd_tootle_2_0/lib/TootleStatic_MT64.lib")
		#else
			#pragma comment(lib, "../../../3rdParty/amd_tootle_2_0/lib/TootleStatic_MT_d64.lib")
		#endif  // __OPTIMIZED
	#else
		#if __OPTIMIZED
			#pragma comment(lib, "../../../3rdParty/amd_tootle_2_0/lib/TootleStatic_MT.lib")
		#else
			#pragma comment(lib, "../../../3rdParty/amd_tootle_2_0/lib/TootleStatic_MT_d.lib")
		#endif  // __OPTIMIZED
	#endif // RSG_CPU_X64
#else
	#if RSG_CPU_X64
		#if __OPTIMIZED
			#pragma comment(lib, "../../../3rdParty/amd_tootle_2_0/lib/TootleSoftwareOnlyStatic_MT64.lib")
		#else
			#pragma comment(lib, "../../../3rdParty/amd_tootle_2_0/lib/TootleSoftwareOnlyStatic_MT_d64.lib")
		#endif  // __OPTIMIZED
	#else
		#if __OPTIMIZED
			#pragma comment(lib, "../../../3rdParty/amd_tootle_2_0/lib/TootleSoftwareOnlyStatic_MT.lib")
		#else
			#pragma comment(lib, "../../../3rdParty/amd_tootle_2_0/lib/TootleSoftwareOnlyStatic_MT_d.lib")
		#endif  // __OPTIMIZED
	#endif // RSG_CPU_X64
#endif // __D3D

extern __THREAD int RAGE_LOG_DISABLE;

PARAM(usetootle, "[RAGE] Use AMD Tootle to optimize meshes");
#endif // USE_TOOTLE

namespace rage {

u8 mshVertex::mshMeshVersion = 0;
u8 mshVertex::mshBlendTargetCount = 0;

mshVertex& mshVertex::operator=(const mshVertex& v)
{
	sysMemCpy(this, &v, sizeof(mshVertex));
	/*
	Pos = v.Pos;
	Binding = v.Binding;
	Nrm = v.Nrm;
	Cpv = v.Cpv;
	TexCount = v.TexCount;
	TanBiCount = v.TanBiCount;

	for( int i = 0; i < mshMaxTexCoordSets; i++ )
	{
		Tex[i] = v.Tex[i];
	}

	for( int i = 0; i < mshMaxTanBiSets; i++ )
	{
		TanBi[i] = v.TanBi[i];
	}

	FloatBlindDataCount = v.FloatBlindDataCount;
	for( int i = 0; i < mshMaxFloatBlindData; i++ )
	{
		FloatBlindData[i] = v.FloatBlindData[i];
	}
	*/
	if( v.BlendTargetDeltas )
	{
		BlendTargetDeltas = rage_new mshBlendTargetDelta[mshBlendTargetCount];

		for( int i = 0; i < mshBlendTargetCount; i++ )
		{
			BlendTargetDeltas[i] = v.BlendTargetDeltas[i];
		}
	}

	return *this;
}

bool mshVertex::operator==(const mshVertex& v) const
{
	if( Pos != v.Pos || !(Binding == v.Binding) || Nrm != v.Nrm || 
		Cpv != v.Cpv || Cpv2 != v.Cpv2 || Cpv3 != v.Cpv3 || TexCount != v.TexCount || TanBiCount != v.TanBiCount || 
		FloatBlindDataCount != v.FloatBlindDataCount )
		return false;

	for( int i = 0; i < TexCount; i++ )
	{
		if( Tex[i] != v.Tex[i] )
			return false;
	}

	for( int i = 0; i < TanBiCount; i++ )
	{
		if( !(TanBi[i] == v.TanBi[i]) )
			return false;
	}
	
	for( int i = 0; i < FloatBlindDataCount; i++ )
	{
		if( FloatBlindData[i] != v.FloatBlindData[i] )
			return false;
	}

	if( BlendTargetDeltas )
	{
		for( int i = 0; i < mshBlendTargetCount; i++ )
		{
			if( !(BlendTargetDeltas[i] == v.BlendTargetDeltas[i]) )
				return false;
		}
	}
	return true;
}

mshMaterial& mshMaterial::operator=(const mshMaterial& m)
{
	Name = m.Name;
	Prim.Resize(m.Prim.GetCount());
	for( int i = 0; i < m.Prim.GetCount(); i++ )
	{
		Prim[i] = m.Prim[i];
	}
	Attr.Resize(m.Attr.GetCount());
	for( int i = 0; i < m.Attr.GetCount(); i++ )
	{
		Attr[i] = m.Attr[i];
	}
	Channels.Resize(m.Channels.GetCount());
	for( int i = 0; i < m.Channels.GetCount(); i++ )
	{
		Channels[i] = m.Channels[i];
	}
	Verts.Resize(m.Verts.GetCount());
	for( int i = 0; i < m.Verts.GetCount(); i++ )
	{
		Verts[i] = m.Verts[i];
	}
	Priority = m.Priority;
	TexSetCount = m.TexSetCount;
	TanBiSetCount = m.TanBiSetCount;

	return *this;
}

/* 
	PURPOSE
		Binds the material object and then renders all primitives associated with that material
	PARAMS
		R - Renderer object 
*/
void mshMaterial::Draw(const mshRenderer &R) const {
	for (int i=0; i<Prim.GetCount(); i++)
		Prim[i].Draw(*this,R);
}


void mshMaterial::BackfaceCull(const Vector3 &eyept) {
	Assert(Prim.GetCount() == 1 && Prim[0].Type == mshTRIANGLES);
	mshArray<mshIndex> newArray;
	const mshArray<mshIndex> &idx = Prim[0].Idx;
	for (int i=0; i<idx.GetCount(); i+= 3) {
		const Vector3 &V1 = Verts[idx[i+0]].Pos;
		const Vector3 &V2 = Verts[idx[i+1]].Pos;
		const Vector3 &V3 = Verts[idx[i+2]].Pos;
		Vector3 A, B, C, E;
		A.Subtract(V1,V2);
		B.Subtract(V1,V3);
		E.Subtract(V1,eyept);
		E.Normalize();
		C.Cross(A,B);
		C.Normalize();
		if (C.Dot(E) < 0.0f) {
			newArray.Append() = idx[i+0];
			newArray.Append() = idx[i+1];
			newArray.Append() = idx[i+2];
		}
	}
	Prim[0].Idx.Assume(newArray);
}


/* 
	PURPOSE
		Binds the material object and then renders all primitives associated with that material
	PARAMS
		R - Renderer object 
*/
void mshMaterial::DrawSkinned(const mshRenderer &R,const Matrix34 *matrices) const {
	for (int i=0; i<Prim.GetCount(); i++)
		Prim[i].DrawSkinned(*this,R,matrices);
}


/* 
	PURPOSE
		Renders all primitive normals associated with that material
	PARAMS
		R - Renderer object 
		c - Color to use
		length - length of the vector 
*/
void mshMaterial::DrawNormals(const mshRenderer &R, Color32 &c, float length) const {
	for (int i=0; i<Prim.GetCount(); i++)
		Prim[i].DrawNormals(*this,R, c, length);
}

/* 
	PURPOSE
		Renders all primitive tangents associated with that material
	PARAMS
		R - Renderer object 
		c - Color to use
		length - length of the vector 
*/
void mshMaterial::DrawTangents(const mshRenderer &R, Color32 &c, float length) const {
	for (int i=0; i<Prim.GetCount(); i++)
		Prim[i].DrawTangents(*this,R, c, length);
}

/* 
	PURPOSE
		Renders all primitive binormals associated with that material
	PARAMS
		R - Renderer object 
		c - Color to use
		length - length of the vector 
		shaderCompute - compute the binormal in a method similar to what rage shaders do
*/
void mshMaterial::DrawBinormals(const mshRenderer &R, Color32 &c, float length, bool shaderCompute) const 
{
	for (int i=0; i<Prim.GetCount(); i++)
		Prim[i].DrawBinormals(*this,R, c, length, shaderCompute);
}





/*
	PURPOSE
		Converts all primitives (if possible) into independent triangle list, one per
		each priority level present in the input.  Actually, it only checks if the priority
		of the current and previous primitves are different, so four primitives of
		priority 0, 1, 0, 1 would produce four outputs, not just two.
		If there are non-triangulatable primitives in the list, the conversion fails.
	PARAMS
		M - Mesh containing this material
	RETURNS
		True if conversion was successful, else false (and material is left unchanged)
*/
bool mshMaterial::Triangulate() {
	if (!Prim.GetCount())
		return false;
	mshArray<mshPrimitive> newPrim;
	mshPrimitive *output = 0;
	int lastPrio = -1;
	Vector3 boxMin(1E10f,1E10f,1E10f);
	for (int i=0; i<Verts.GetCount(); i++)
		boxMin.Min(boxMin, Verts[i].Pos);
	for (int i=0; i<Prim.GetCount(); i++) {
		if (Prim[i].Priority != lastPrio) {
			lastPrio = Prim[i].Priority;
			output = &newPrim.Append();
		}
		if (!Prim[i].Triangulate(*this,*output,boxMin))
			return false;
	}
	Prim.Assume(newPrim);
	return true;		
}



/*
	PURPOSE
		Converts all primitives (if possible) into one or more tristrips.
		If there are non-triangulatable primitives in the list, the conversion fails.
	PARAMS
		M - Mesh containing this material
	RETURNS
		True if conversion was successful, else false (and material is left unchanged)
*/
bool mshMaterial::Tristrip() {
	if (!Prim.GetCount())
		return false;
	mshArray<mshPrimitive> output;
	for (int i=0; i<Prim.GetCount(); i++)
		if (!Prim[i].Tristrip(*this,output))
			return false;
	Prim.Assume(output);
	return true;
}

// Huge thanks to Tom Forsyth for sharing this:
// http://home.comcast.net/~tom_forsyth/papers/fast_vert_cache_opt.html
const float FindVertexScore_CacheDecayPower = 1.5f;
const float FindVertexScore_LastTriScore = 0.75f;
const float FindVertexScore_ValenceBoostScale = 2.0f;
const float FindVertexScore_ValenceBoostPower = 0.5f;
const int MaxSizeVertexCache = 32;

struct vcache_vertex_data {
	vcache_vertex_data() : CacheTag(-1), CurrentScore(0), NumActiveTris(0) { }

	void AddTri(int ti) {
		++NumActiveTris;
		TriIndices.Append() = ti;
	}

	int CacheTag;	// position in the modelled cache (-1 if it is not in the cache)
	float CurrentScore;
	int NumActiveTris;	// number of triangles not yet added that use it
	mshArray<mshIndex> TriIndices;		// TotalTriangles worth of indices
};

struct vcache_triangle {
	int WasAdded;			// nonzero if already added
	float TriangleScore;	// score; sum of the scores of the three vertices
	mshIndex Idx[3];				// vertex indices of the triangle
};

float FindVertexScore ( vcache_vertex_data *VertexData )
{
	if ( VertexData->NumActiveTris == 0 )
	{
		// No tri needs this vertex!
		return -1.0f;
	}

	float Score = 0.0f;
	int CachePosition = VertexData->CacheTag;
	if ( CachePosition < 0 )
	{
		// Vertex is not in FIFO cache - no score.
	}
	else
	{
		if ( CachePosition < 3 )
		{
			// This vertex was used in the last triangle,
			// so it has a fixed score, whichever of the three
			// it's in. Otherwise, you can get very different
			// answers depending on whether you add
			// the triangle 1,2,3 or 3,1,2 - which is silly.
			Score = FindVertexScore_LastTriScore;
		}
		else
		{
			Assert ( CachePosition < MaxSizeVertexCache );
			// Points for being high in the cache.
			const float Scaler = 1.0f / ( MaxSizeVertexCache - 3 );
			Score = 1.0f - ( CachePosition - 3 ) * Scaler;
			Score = powf ( Score, FindVertexScore_CacheDecayPower );
		}
	}

	// Bonus points for having a low number of tris still to
	// use the vert, so we get rid of lone verts quickly.
	float ValenceBoost = powf ( (float) VertexData->NumActiveTris,
		-FindVertexScore_ValenceBoostPower );
	Score += FindVertexScore_ValenceBoostScale * ValenceBoost;

	return Score;
}

void mshMaterial::CacheOptimize() {
	Assert(Prim.GetCount() == 1 && Prim[0].Type == mshTRIANGLES);

#if USE_TOOTLE
	if (PARAM_usetootle.Get())
	{
		static const int kCacheSize = 4;
		int indexCount = Prim[0].Idx.GetCount();

		AssertVerify(ASSERT_ONLY(TOOTLE_OK == ) TootleInit());

#if 0
		static const int kNumViewports = 10;
		float viewports[3*kNumViewports];
		//srand(0);
		for (int i = 0; i < kNumViewports; ++i)
		{
			Vector3 randUnit;
			randUnit.SetX(static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2.0f - 1.0f);
			randUnit.SetY(static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2.0f - 1.0f);
			randUnit.SetZ(static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2.0f - 1.0f);
			randUnit.Normalize(randUnit);
			viewports[i*3] = randUnit.GetX();
			viewports[i*3+1] = randUnit.GetY();
			viewports[i*3+2] = randUnit.GetZ();
		}

#if 1
		++RAGE_LOG_DISABLE;
		AssertVerify(ASSERT_ONLY(TOOTLE_OK == ) TootleOptimize(
			&Verts[0],
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			Verts.GetCount(),
			indexCount/3,
			sizeof(mshVertex),
			kCacheSize,
			viewports,
			kNumViewports,
			TOOTLE_CCW,
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			NULL,
			TOOTLE_VCACHE_TIPSY,
			TOOTLE_OVERDRAW_FAST));
		--RAGE_LOG_DISABLE;
#else
		unsigned int numClusters;
		unsigned int* clusters = rage_new unsigned int[indexCount/3+1];

		++RAGE_LOG_DISABLE;
		AssertVerify(ASSERT_ONLY(TOOTLE_OK == ) TootleFastOptimizeVCacheAndClusterMesh(
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			indexCount/3,
			Verts.GetCount(),
			kCacheSize,
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			clusters,
			&numClusters,
			TOOTLE_DEFAULT_ALPHA));

		AssertVerify(ASSERT_ONLY(TOOTLE_OK == ) TootleOptimizeOverdraw(
			&Verts[0],
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			Verts.GetCount(),
			indexCount/3,
			sizeof(mshVertex),
			viewports,
			kNumViewports,
			TOOTLE_CCW,
			clusters,
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			NULL,
			TOOTLE_OVERDRAW_FAST));
		--RAGE_LOG_DISABLE;

		delete [] clusters;
#endif // 0
#else
		++RAGE_LOG_DISABLE;
		AssertVerify(ASSERT_ONLY(TOOTLE_OK == ) TootleOptimizeVCache(
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			indexCount/3,
			Verts.GetCount(),
			kCacheSize,
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			NULL,
			TOOTLE_VCACHE_TIPSY));
		--RAGE_LOG_DISABLE;
#endif // 0

#if 1
		++RAGE_LOG_DISABLE;
		AssertVerify(ASSERT_ONLY(TOOTLE_OK == ) TootleOptimizeVertexMemory(
			&Verts[0], 
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			Verts.GetCount(),
			indexCount/3,
			sizeof(mshVertex),
			&Verts[0], 
			reinterpret_cast<unsigned int*>(&(Prim[0].Idx[0])),
			NULL));
		--RAGE_LOG_DISABLE;
#endif // 0

		TootleCleanup();

		return;
	}
#endif // USE_TOOTLE

	// init the vertex array
	mshArray<vcache_vertex_data> vdata;
	vdata.Init(Verts.GetCount());

	// init the tri array and track which triangles are used by which vertices
	mshArray<vcache_triangle> tdata(Prim[0].Idx.GetCount()/3);
	for (int i=0, ti=0; i<Prim[0].Idx.GetCount(); i+=3, ti++) {
		vcache_triangle &tri = tdata.Append();
		tri.WasAdded = false;
		tri.TriangleScore = 0;
		for (int j=0; j<3; j++)
			vdata[tri.Idx[j] = Prim[0].Idx[i+j]].AddTri(ti);
	}

	// compute the vertex scores
	for (int i=0; i<vdata.GetCount(); i++)
		vdata[i].CurrentScore = FindVertexScore(&vdata[i]);

	// compute the initial triangle scores
	for (int i=0; i<tdata.GetCount(); i++)
		tdata[i].TriangleScore = vdata[tdata[i].Idx[0]].CurrentScore + vdata[tdata[i].Idx[1]].CurrentScore + vdata[tdata[i].Idx[2]].CurrentScore;

	atFixedArray<mshIndex,MaxSizeVertexCache + 3> vcache;		// extra room so array never has to grow
	mshArray<mshIndex> newIndices(Prim[0].Idx.GetCount());

	int bestTri = -1;
	float bestScore = -2.0f;
	for (int i=0; i<tdata.GetCount(); i++) {
		// Rescan all suitable remaining triangles if necessary
		if (bestScore < 0.1f) {
			for (int j=0; j<tdata.GetCount(); j++) {
				if (tdata[j].WasAdded)
					continue;
				if (bestScore < tdata[j].TriangleScore) {
					bestTri = j;
					bestScore = tdata[j].TriangleScore;
				}
			}
		}
		Assert(bestTri != -1);

		// add the triangle to the draw list
		vcache_triangle &bt = tdata[bestTri];
		bt.WasAdded = true;
		bt.TriangleScore = -1.0f;
		for (int k=0; k<3; k++) {
			int vidx = bt.Idx[k];
			vcache_vertex_data &v = vdata[vidx];
			// Reduce valence of the vertex
			v.NumActiveTris--;
			Assert(v.NumActiveTris >= 0);
			// Add the vertex index to the output trilist
			newIndices.Append() = vidx;

			// If it was already in the cache, delete it
			if (v.CacheTag != -1)
				vcache.Delete(v.CacheTag);
			// Reinsert vertex at head of cache
			vcache.Insert(0) = vidx;
			// Revalidate all cache tags
			for (int m=0; m<vcache.GetCount(); m++)
				vdata[vcache[m]].CacheTag = m;
		}

		// update all vertices in the cache
		for (int k=0; k<vcache.GetCount(); k++) {
			int vidx = vcache[k];
			vcache_vertex_data &v = vdata[vidx];
			// update tag to reflect new position (which may no longer be in the cache!)
			v.CacheTag = k < MaxSizeVertexCache? k : -1;
			v.CurrentScore = FindVertexScore(&v);
		}

		// reset best triangle
		bestTri = -1;
		bestScore = -2.0f;

		// update all triangle scores associated with vertices in the cache
		// this is a separate pass because we need all vertex scores stabilized first.
		for (int k=0; k<vcache.GetCount(); k++) {
			int vidx = vcache[k];
			vcache_vertex_data &v = vdata[vidx];
			for (int t=0; t<v.TriIndices.GetCount(); t++) {
				vcache_triangle &tri = tdata[v.TriIndices[t]];
				// ignore triangle if it is already gone (we set its score to an impossibly low number already when we pulled it)
				if (tri.WasAdded)
					continue;
				// recompute triangle score
				tri.TriangleScore = vdata[tri.Idx[0]].CurrentScore + vdata[tri.Idx[1]].CurrentScore + vdata[tri.Idx[2]].CurrentScore;
				// ..and see if it's the best one for next iteration
				if (bestScore < tri.TriangleScore) {
					bestTri = v.TriIndices[t];
					bestScore = tri.TriangleScore;
				}
			}
		}

		// remove excess vertices from the cache
		// any excess have already had their bookkeeping properly performed.
		while (vcache.GetCount() > MaxSizeVertexCache)
			vcache.Pop();
	}

#if __ASSERT
	for (int i=0; i<vdata.GetCount(); i++)
		Assert(vdata[i].NumActiveTris == 0);
	for (int i=0; i<tdata.GetCount(); i++)
		Assert(tdata[i].WasAdded);
#endif

	// copy the reordered vertex list over.
	Assert(newIndices.GetCount() == Prim[0].Idx.GetCount());
	Prim[0].Idx.Assume(newIndices);
}

void mshMaterial::ReorderVertices() {
	int vCount = Verts.GetCount();
	mshArray<int> remap(vCount);
	for (int i=0; i<vCount; i++)
		remap.Append() = -1;		// unused
	mshArray<mshVertex> newVerts(vCount);
	for (int i=0; i<Prim.GetCount(); i++) {
		for (int j=0; j<Prim[i].Idx.GetCount(); j++) {
			mshIndex &idx = Prim[i].Idx[j];
			if (remap[idx] == -1) {
				remap[idx] = newVerts.GetCount();
				newVerts.Append() = Verts[idx];
			}
			idx = remap[idx];
		}
	}
	Assert(Verts.GetCount() >= newVerts.GetCount());
	Verts.Assume(newVerts);

	//Reorder the material channel data to reflect the changes made to the vertex ordering
	int channelCount = Channels.GetCount();
	for(int i=0; i<channelCount; i++)
	{
		mshChannel& origChannel = Channels[i];
		Assert(origChannel.GetVertexCount() >= Verts.GetCount());

		mshChannel remapChannel;
		remapChannel.Init(origChannel.GetName(), Verts.GetCount(), origChannel.GetFieldCount());

		//Iterate over the vertices and the contained fields, remapping the data to the correct indicies
		for(int j=0; j<origChannel.GetVertexCount(); j++)
		{
			for(int k=0; k<origChannel.GetFieldCount(); k++)
			{
				//Remap the channel data based on the remapping indicies stored during the vertex remap process
				if(remap[j] != -1)
					remapChannel.Access( remap[j], k ) = origChannel.Access( j, k );
			}
		}

		//Copy the remapped data back over the original channel data
		Channels[i] = remapChannel;
	}

}



/*
	PURPOSE
		Remove all vertex data but keep the material information.  
		Deallocates memory used by vertex data.
	PARAMS
		None
*/
void mshMaterial::ResetVerts()
{
	Verts.Reset();
}

/*
	PURPOSE
		Remove all degenerate primitives from all primitives in this material
		If a particular primitive resolves to no indices (because it consisted
		entirely of degenerate subprimitives) then that primitive is deleted
		from the material.
	PARAMS
		M - Mesh containing this material
*/
void mshMaterial::RemoveDegenerates() {
	for (int i=0; i<Prim.GetCount(); i++) {
		mshPrimitive output;
		Prim[i].RemoveDegenerates(*this,output);
		if (output.Idx.GetCount())
		{
			Prim[i].Idx.Reset();
			Prim[i] = output;
		} else {
			Prim.Delete(i);
			--i;
		}
	}
}

/*
PURPOSE
Remove all unreferenced vertices and remap indices in all primitives appropriately
*/

void mshMaterial::RemoveUnreferenced()
{
	atBitSet used(Verts.GetCount());
	int i, j;
	for (i=0; i<Prim.GetCount(); i++) {
		mshPrimitive &P = Prim[i];
		for (j=0; j<P.Idx.GetCount(); j++)
			used.Set(P.Idx[j]);
	}

	mshArray <mshIndex> remap;
	remap.Init(mshUndefined,Verts.GetCount());
	int bias = 0;

	for (i=0; i<Verts.GetCount(); i++) {
		if (used.IsSet(i)) {
			// account for any items before us that have already been deleted
			remap[i] = (mshIndex) (i - bias);
		}
		else
			++bias;			// all future entries get shuffled left more
	}

	if (!bias)
		return;

	mshArray <mshVertex> output(Verts.GetCount() - bias);

	// Update primitives
	for (i=0; i<Prim.GetCount(); i++) {
		mshPrimitive &P = Prim[i];
		for (j=0; j<P.Idx.GetCount(); j++)
			P.Idx[j] = remap[P.Idx[j]];
	}

	// Create output array
	int next = 0;
	for (i=0; i<Verts.GetCount(); i++) {
		if (remap[i] == next) {
			output.Append() = Verts[i];
			++next;
		}
	}
	Assert(output.GetCount() + bias == i);
	Verts.Assume(output);
}


/*
PURPOSE
Reverse the draw order
*/

void mshMaterial::Reverse()
{
	for (int i=0; i<Prim.GetCount(); i++) {
		mshPrimitive &P = Prim[i];
		switch (P.Type)
		{
		case mshPOLYGON:
		case mshTRIANGLES:
		case mshQUADS:
			{
				for (int j=0; j<P.Idx.GetCount()/2; j++) {
					int index = (P.Idx.GetCount()-1) - j;
					mshIndex temp = P.Idx[j];
					P.Idx[j] = P.Idx[index];
					P.Idx[index] = temp;
				}

			}
			break;
		case mshTRISTRIP:
		case mshTRISTRIP2:
			{
				for (int j=0; j<P.Idx.GetCount()-1; j++) {
					int index = j+1;
					mshIndex temp = P.Idx[j];
					P.Idx[j] = P.Idx[index];
					P.Idx[index] = temp;
				}

			}
			break;

		default:
			AssertMsg(0 , "Unsupported type");
		}		
	}
}

/*
	PURPOSE
		This appends all the primitives from the "from" material to this material
	PARAMS
		from - mesh to merge primitives from
*/
bool  mshMaterial::Merge( const mshMaterial & from )
{
	// Make sure channel data is compatible
	if (Channels.GetCount() != from.Channels.GetCount()) {
		Warningf("Different number of channels in materials, cannot merge");
		return false;
	}

	for (int i=0; i<Channels.GetCount(); i++) {
		if (Channels[i].GetName() != from.Channels[i].GetName()) {
			Warningf("Channel names don't match");
			return false;
		}
		if (Channels[i].GetFieldCount() != from.Channels[i].GetFieldCount()) {
			Warningf("Channel field counts don't match");
			return false;
		}
	}

	// Make sure attributes (not likely used) are compatible)
	/* if (Attr != from.Attr) {
		Warningf("Cannot merge material with different attributes");
		return false;
	} */
	/*
	if (Verts.GetCount() + from.Verts.GetCount() >= 65535) {
		Displayf("Cannot merge material, too many resulting vertices");
		return false;
	}
	*/
	int idxCount = 0;
	const int EXTRA_VERTS_FOR_BREAKS = 2;
	for (int i=0; i<Prim.GetCount(); i++)
		idxCount += Prim[i].Idx.GetCount() + EXTRA_VERTS_FOR_BREAKS;
	mshPrimitive &final = Prim[Prim.GetCount()-1];
	const mshPrimitive &first = from.Prim[0];
	mshPrimType expected = (final.Idx.GetCount() & 1)? mshFlipWinding(final.Type) : final.Type;
	bool needDegenerate = expected != first.Type;
	if (needDegenerate)
		++idxCount;
	for (int i=0; i<from.Prim.GetCount(); i++)
		idxCount += from.Prim[i].Idx.GetCount() + EXTRA_VERTS_FOR_BREAKS;
	/*
	if (idxCount >= 65535) {
		Displayf("Cannot merge material, too many resulting indices");
		return false;
	}
	*/

	int offset = Verts.GetCount();

	if (Verts.GetCount() + from.Verts.GetCount() > 65535)
	{
		return false;
	}

	// Add the vertices of the source material into the current material
	Verts.Reallocate(Verts.GetCount() + from.Verts.GetCount());
	for (int i=0; i<from.Verts.GetCount(); i++)
		Verts.Append() = from.Verts[i];

	// Add the channel data of the source material into the current material
	for (int i=0; i<Channels.GetCount(); i++) {
		int channelOffset = Channels[i].GetVertexCount();
		int fieldCount = Channels[i].GetFieldCount();
		Channels[i].Resize(Channels[i].GetVertexCount() + from.Channels[i].GetVertexCount());
		for (int j=0; j<from.Channels[i].GetVertexCount(); j++) {
			for (int k=0; k<fieldCount; k++)
				Channels[i].Access(channelOffset+j,k) = from.Channels[i].Access(j,k);
		}
	}

	// Preserve winding if necessary when merging materials.
	if (needDegenerate) {
		mshIndex last = final.Idx[final.Idx.GetCount()-1];
		final.Idx.Append() = last;
	}

	Prim.Reallocate(Prim.GetCount() + from.Prim.GetCount());
	// Add the primitives as well and adjust their indices.
	for (int i=0; i<from.Prim.GetCount(); i++) {
		mshPrimitive &p = Prim.Append() = from.Prim[i];
		for (int j=0; j<p.Idx.GetCount(); j++)
			p.Idx[j] += offset;
	}

	return true;
}

/*
	PURPOSE
		Packetize a material into an output array
*/
void mshMaterial::Packetize(mshArray<mshMaterial> &newMtls,int finalVtxCount,int finalMtxCount) const
{
	int baseTri = 0;

	// Make sure it's triangles
	Assert(Prim.GetCount() == 1 && Prim[0].Type == mshTRIANGLES);

	const mshPrimitive &srcTris = Prim[0];

	// start pulling primitives out, adding new vertices until we fill up a packet
	int *remap = rage_new int[GetVertexCount()];
	while (baseTri < srcTris.Idx.GetCount()) 
	{
		// Enough for 256 matrices, the max we support.
		atFixedBitSet<256> mtxUsed;
		mtxUsed.Reset();

		// Clear remapping array for this packet
		memset(remap,-1,sizeof(int) * GetVertexCount());
		int vertexCount = 0;

		// Create a new material and an empty primitive and enough verts to avoid too much resizing
		mshMaterial &dest = newMtls.Append();
		dest.Name = Name;
		dest.Prim.Append();
		dest.Prim[0].Type = mshTRIANGLES;
		dest.Prim[0].Priority = srcTris.Priority;
		dest.Verts.Reallocate(finalVtxCount);
		// A rough guess to get started.
		dest.Prim[0].Idx.Reallocate(finalVtxCount*6);

		// Keep going until we're out of triangles or the next triangle could overflow the packet
		// (the packet sizes are large enough it's no big deal if we duplicate a few vertices)
		int mtxCur = 0;
		while (baseTri < srcTris.Idx.GetCount() && vertexCount + 3 < finalVtxCount) 
		{
			// A single triangle can reference up to 3 * mshMaxMatricesPerVertex; unlikely,
			// but we need to be robust.  If it's remotely possible this triangle could fill the
			// packet, we have to do the math.
			if (finalMtxCount && mtxCur + (3 * mshMaxMatricesPerVertex) > finalMtxCount)
			{
				atFixedBitSet<256> mtxNew(mtxUsed);
				int newMatrices = 0;

				for (int j=0; j<3; j++)
				{
					mshIndex idx = srcTris.Idx[baseTri+j];
					const mshBinding &b = Verts[idx].Binding;
					if( b.IsPassThrough == false )
					{
						for (int k=0; k<mshMaxMatricesPerVertex; k++)
						{
#if __RESOURCECOMPILER
							if(b.Mtx[k] >= mtxNew.GetNumBits())
							{
								Errorf("Vertex %d trying to set bit on mshMaterial matrix bitset that's higher than allowed: %d >= %d!", idx, b.Mtx[k], mtxNew.GetNumBits());
								continue;
							}
#endif
							if (b.Wgt[k] && !mtxNew.GetAndSet(b.Mtx[k]))
								++newMatrices;
						}
					}
				}

				// Too many matrices, abort this packet.
				if (mtxCur + newMatrices > finalMtxCount)
					break;
			}

			// For each vertex referenced by the primitive, add it if it's not already there
			for (int j=0; j<3; j++)
			{
				mshIndex idx = srcTris.Idx[baseTri+j];
				if (remap[idx] == -1)
				{
					remap[idx] = vertexCount++;
					dest.Verts.Append() = Verts[idx];
					// Track the newly added matrices
					if (finalMtxCount)
					{
						const mshBinding &b = Verts[idx].Binding;
						if( b.IsPassThrough == false )
						{
							for (int k=0; k<mshMaxMatricesPerVertex; k++)
							{
#if __RESOURCECOMPILER
								if(b.Mtx[k] >= mtxUsed.GetNumBits())
								{
									Errorf("Vertex %d trying to set bit on mshMaterial matrix bitset that's higher than allowed: %d >= %d!", idx, b.Mtx[k], mtxUsed.GetNumBits());
									continue;
								}
#endif
								if (b.Wgt[k] && !mtxUsed.GetAndSet(b.Mtx[k]))
									++mtxCur;
							}
						}
					}
				}
				dest.Prim[0].Idx.Append() = remap[idx];

				if (Verts[idx].TexCount > dest.TexSetCount)
					dest.TexSetCount = Verts[idx].TexCount;
				if (Verts[idx].TanBiCount > dest.TanBiSetCount)
					dest.TanBiSetCount = Verts[idx].TanBiCount;
			}

			baseTri += 3;
		}

		// Copy channel data over
		dest.Channels.Resize(Channels.GetCount());
		for (int i=0; i<Channels.GetCount(); i++) 
		{
			int fieldCount = Channels[i].GetFieldCount();
			const int destChannelVertCount = Channels[i].GetVertexCount();
			dest.Channels[i].Init(Channels[i].GetName(),destChannelVertCount,fieldCount);
			for (int j=0; j<destChannelVertCount; j++) 
				for (int k=0; k<fieldCount; k++)
					dest.Channels[i].Access(j,k) = Channels[i].Access(j,k);
		}
	}
	delete[] remap;
}

/*
	RETURNS
		Cumulative triangle count in all primitives in this material
*/
int mshMaterial::GetTriangleCount() const {
	int result = 0;
	for (int i=0; i<Prim.GetCount(); i++)
		result += Prim[i].GetTriangleCount();
	return result;
}


void Serialize(mshSerializer &S,mshPrimitive &v) { v.Serialize(S); }
void Serialize(mshSerializer &S,mshVertex &v) { v.Serialize(S); }

inline int PackElement(float f) {
	if (f < -1) f = -1;
	else if (f > 1) f = 1;
	return int(f * 256) & 1023;
}

inline float UnpackElement(int i) {
	// sign extend
	i = (i << 22) >> 22;
	return i / 256.0f;
}

void SerializeNrm(mshSerializer &S,Vector3 &v,bool /*ASSERT_ONLY(quantized)*/) {
	// Versions prior to 12 would pack normals to S1.8, which isn't great when you're using 10:11:11 formats!
	if (S.T.IsBinary() && mshVertex::mshMeshVersion <= 11) {
		if (S.Writing) {
			int packed = (PackElement(v.x) << 20) | (PackElement(v.y) << 10) | PackElement(v.z);
			::rage::Serialize(S,packed);
		}
		else {
			int packed = 0;
			::rage::Serialize(S,packed);
			v.x = UnpackElement(packed >> 20);
			v.y = UnpackElement((packed >> 10) & 1023);
			v.z = UnpackElement(packed & 1023);
		}
	}
	else
		::rage::Serialize(S,v);
}

void mshVertex::Serialize(mshSerializer &S) 
{
	::rage::Serialize(S,Pos);
	S.Separator("/");
	Binding.Serialize(S);
	S.Separator("/");
	SerializeNrm(S,Nrm,true);
	S.Separator("/");
	::rage::Serialize(S,Cpv);
	if( mshVertex::mshMeshVersion >= 10 )
	{
		S.Separator("/");
		::rage::Serialize(S,Cpv2);
		S.Separator("/");
		if( mshVertex::mshMeshVersion >= 11 )
		{
			::rage::Serialize(S,Cpv3);
			S.Separator("/");
		}
	}
	::rage::Serialize(S,TexCount);
	for (int i=0; i<TexCount; i++)
	{
		::rage::Serialize(S,Tex[i]);
	}
	S.Separator("/");
	::rage::Serialize(S,TanBiCount);

	for (int i=0; i<TanBiCount; i++) 
	{
		SerializeNrm(S,TanBi[i].T,false);
		SerializeNrm(S,TanBi[i].B,false);
	}
	
	if(mshVertex::mshMeshVersion > 6)
	{
		S.Separator("/");
		::rage::Serialize(S, FloatBlindDataCount);
		for (int i=0; i<mshMaxFloatBlindData; i++) 
			::rage::Serialize(S,FloatBlindData[i]);
	}

	if( mshVertex::mshMeshVersion >= 9 )
	{
		S.Separator("/");

		if( mshVertex::mshBlendTargetCount > 0 )
		{
			// Allocate space for target deltas when reading
			if( !S.Writing )
			{
				BlendTargetDeltas = rage_new mshBlendTargetDelta[mshVertex::mshBlendTargetCount];
			}

			if ( BlendTargetDeltas )
			{
				for( int i = 0; i < mshVertex::mshBlendTargetCount; i++ )
				{
					::rage::Serialize(S, BlendTargetDeltas[i].Position);
					::rage::Serialize(S, BlendTargetDeltas[i].Normal);
					::rage::Serialize(S, BlendTargetDeltas[i].Tangent);
					::rage::Serialize(S, BlendTargetDeltas[i].BiNormal);
				}
			}
		}
	}
}

void Serialize(mshSerializer&S,mshChannel &c) {
	c.Serialize(S);
}

void mshChannel::Serialize(mshSerializer &S) {
	::rage::Serialize(S,Name);
	S.Separator(":");
	::rage::Serialize(S,VertexCount);
	::rage::Serialize(S,FieldCount);
	S.BeginBlock();
	int count = VertexCount * FieldCount;
	if (!S.Writing) {
		delete[] Data;
		Data = rage_new mshChannelData[count];
	}
	for (int i=0; i<count; i++)
		::rage::Serialize(S,Data[i].u);
	S.EndBlock();
}

/*
	PURPOSE
		Internal use only; implements serialization for mshMaterial objects
	PARAMS
		S - Serializer object to use
*/
void mshMaterial::Serialize(mshSerializer &S) {
	S.BeginBlock();
	SERIALIZE(S,Name);
	SERIALIZE(S,Priority);
	SERIALIZE(S,Prim);
	SERIALIZE(S,Verts);
	if (!S.Writing) {
		for (int i=0; i<Verts.GetCount(); i++) {
			if (Verts[i].TexCount > TexSetCount)
				TexSetCount = Verts[i].TexCount;
			if (Verts[i].TanBiCount > TanBiSetCount)
				TanBiSetCount = Verts[i].TanBiCount;
		}
	}
	if (mshVertex::mshMeshVersion >= 8) {
		SERIALIZE(S,Channels);
	}
	S.EndBlock();
}



int mshMaterial::AddVertex(
		const Vector3 &pos,
		const mshBinding &binding,
		const Vector3 &nrm,
		const Vector4 &cpv,
		const Vector4 &cpv2,
		const Vector4 &cpv3,
		int tcCount,
		const Vector2 *tcs,
		int fbdCount,
		const float *fbd)
{
	int result = Verts.GetCount();
	mshVertex &V = Verts.Append();
	V.Pos = pos;
	V.Binding = binding;
	V.Nrm = nrm;
	Assert(nrm.x >= -1 && nrm.x <= 1);
	Assert(nrm.y >= -1 && nrm.y <= 1);
	Assert(nrm.z >= -1 && nrm.z <= 1);
	V.Cpv = cpv;
	V.Cpv2 = cpv2;
	V.Cpv3 = cpv3;
	V.TexCount = tcCount;
	if (tcCount > TexSetCount)
		TexSetCount = tcCount;
	for (int i=0; i<tcCount; i++)
	{
		V.Tex[i] = tcs[i];
	}
	
	V.FloatBlindDataCount = fbdCount;
	if(fbdCount && fbd)
	{
		for(int i=0;
			((i<fbdCount) && (i<mshMaxFloatBlindData));
			i++)
		{
			V.FloatBlindData[i] = fbd[i];
		}
	}

	if( mshVertex::mshBlendTargetCount )
	{
		V.BlendTargetDeltas = rage_new mshBlendTargetDelta[mshVertex::mshBlendTargetCount];
		for( int i = 0; i < mshVertex::mshBlendTargetCount; i++ )
		{
			V.BlendTargetDeltas[i].Position.Zero();
			V.BlendTargetDeltas[i].Normal.Zero();
			V.BlendTargetDeltas[i].Tangent.Zero();
			V.BlendTargetDeltas[i].BiNormal.Zero();
		}
	}
	return result;
}


int mshMaterial::AddVertex(const mshVertex& vertex)
{
	int result = Verts.GetCount();
	mshVertex &V = Verts.Append();
	V = vertex;
	return result;
}


/*
	PURPOSE:
		Compute the smooth normals of all the verticies in this material
*/
void mshMaterial::ComputeNormals()
{
	atArray<Vector3, 0, unsigned int> tempNormals;
	
	tempNormals.Resize(Verts.GetCount());
	for( int i = 0; i < Verts.GetCount(); i++ )
		tempNormals[i].Zero();

    TriangleIterator iter = BeginTriangles();
	TriangleIterator end = EndTriangles();
	while( iter != end )
	{
		int a, b, c;
		iter.GetVertIndices(a, b, c);

		Vector3 edge0 = Verts[b].Pos - Verts[a].Pos;
		Vector3 edge1 = Verts[c].Pos - Verts[a].Pos;
		Vector3 vNrm;
		vNrm.Cross(edge0, edge1);
		tempNormals[a].Add(vNrm);
		tempNormals[b].Add(vNrm);
		tempNormals[c].Add(vNrm);
		++iter;
	}

	for( int i = 0; i < tempNormals.GetCount(); i++ )
	{
		tempNormals[i].Normalize();
		Verts[i].Nrm = tempNormals[i];
	}
}


/*
	PURPOSE
		Utility function which computes the tangent vector for a single
		texture-mapped triangle.  The final tangent vector for a vertex should
		be the average of all of the tangent vectors of every triangle that
		references that vertex.  Need to watch out for sign flips due to
		texturing discontinuities.
	NOTES
		Taken directly from "Estimating a Tangent Vector for Bump Mapping" at 
		http:www.magic-software.com
*/
static void ComputeTangent(const Vector3 &P0,const Vector3 &P1,const Vector3 &P2,
					float u0,float v0,
					float u1,float v1,
					float u2,float v2,
					Vector3 &dPdu) {
	float denom = ((v1 - v0) * (u2 - u0)) - ((v2 - v0) * (u1 - u0));
	if (denom)
		dPdu = (((P2 - P0) * (v1 - v0)) - ((P1 - P0) * (v2 - v0))) / denom;
	else
		dPdu.Zero();
}


/*
	PURPOSE
		Utility function which computes the binormal vector for a single
		texture-mapped triangle.  The final binormal vector for a vertex should
		be the average of all of the binormal vectors of every triangle that
		references that vertex.  Need to watch out for sign flips due to
		texturing discontinuities.
	NOTES
		Derived from ComputeTangent by swapping all u's and v's.
		Hopefully this is valid.
*/
static void ComputeBinormal(const Vector3 &P0,const Vector3 &P1,const Vector3 &P2,
					float u0,float v0,
					float u1,float v1,
					float u2,float v2,
					Vector3 &dPdv) {
	float denom = ((u1 - u0) * (v2 - v0)) - ((u2 - u0) * (v1 - v0));
	if (denom)
		dPdv = (((P2 - P0) * (u1 - u0)) - ((P1 - P0) * (u2 - u0))) / denom;
	else
		dPdv.Zero();
}

void mshMaterial::ComputeTanBi2(int set) 
{
	// Displayf("%d %d this = %p Name = %s", __LINE__, iNoOfFunctionCalls, this, (const char*)this->Name);
	// Pulled code from here: http://www.terathon.com/code/tangent.html
	// Message thread describing it here:  http://www.opengl.org/discussion_boards/ubb/Forum3/HTML/011349.html
	Assert(set < mshMaxTanBiSets);
	const int vcount = Verts.GetCount();
	mshArray<Vector3> tempTangents;
	tempTangents.Init(vcount * 2);
	Vector3 *tan1 = &tempTangents[0];
	Vector3 *tan2 = tan1 + vcount;
	
	if (set+1>TanBiSetCount)
		TanBiSetCount=set+1;
	for (int i=0; i<vcount; i++) {
		if (Verts[i].TanBiCount < set+1)
			Verts[i].TanBiCount = set+1;
		tan1[i].Zero();
		tan2[i].Zero();
	}

	for (int j=0; j<Prim.GetCount(); j++) 
	{
		const mshPrimitive &p = Prim[j];
		Assert(p.Type == mshTRIANGLES);
		for (int k=0; k<p.Idx.GetCount(); k+=3) {
			const mshVertex &a0 = Verts[p.Idx[k]];
			const mshVertex &a1 = Verts[p.Idx[k+1]];
			const mshVertex &a2 = Verts[p.Idx[k+2]];
			
			float x1 = a1.Pos.x - a0.Pos.x;	
			float x2 = a2.Pos.x - a0.Pos.x;
			float y1 = a1.Pos.y - a0.Pos.y;
			float y2 = a2.Pos.y - a0.Pos.y;
			float z1 = a1.Pos.z - a0.Pos.z;
			float z2 = a2.Pos.z - a0.Pos.z;
			
			float s1 = a1.Tex[set].x - a0.Tex[set].x;
			float s2 = a2.Tex[set].x - a0.Tex[set].x;
			float t1 = a1.Tex[set].y - a0.Tex[set].y;
			float t2 = a2.Tex[set].y - a0.Tex[set].y;
			
			float divisor = (s1 * t2 - s2 * t1);
			// NOTE: What should we do when UV's are overlapping?  Currently, remove it's contribution
			if ( divisor == 0 ) {
// NOTE: Uncomment this if you want warning spew for invalid tangent based UV's
/*
				Warningf("Triangle has bad UV's -- this may effect normal mapping\n"
							"\t\t UV coords0: %f,  %f\n"
							"\t\t UV coords1: %f,  %f\n"
							"\t\t UV coords1: %f,  %f\n", 
							a0.Tex[set].x, a0.Tex[set].y, a1.Tex[set].x, a1.Tex[set].y, a2.Tex[set].x, a2.Tex[set].y);
*/
			}
			float r = (divisor == 0) ? 0.f : 1.f / divisor;
			Vector3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
			Vector3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);
			
			tan1[p.Idx[k]] += sdir;
			tan1[p.Idx[k+1]] += sdir;
			tan1[p.Idx[k+2]] += sdir;
			
			tan2[p.Idx[k]] += tdir;
			tan2[p.Idx[k+1]] += tdir;
			tan2[p.Idx[k+2]] += tdir;
		}
	}
	for (int i = 0; i < vcount; ++i) 
	{
		const Vector3 &n = Verts[i].Nrm;
		const Vector3 &t = (tan1[i].IsZero()) ? ((Abs(M34_IDENTITY.b.Dot(n)) < 1.0f / M_SQRT2) ? M34_IDENTITY.b : M34_IDENTITY.a) : tan1[i];
		
#if !__TOOL
		if ( tan1[i].IsZero() ) {
// NOTE: Uncomment this if you want warning spew for invalid tangent based UV's
			Warningf("Forcing tangent space for vert %d -- may break normalmapping", i);
		}
#endif
		
		// Gram-Schmidth orthogonalize
		Vector3 &tangent = Verts[i].TanBi[set].T;
		float ndt = n.Dot(t);
		tangent.Scale(n, -ndt);
		tangent.Add(t);
		tangent.Normalize();
		
		if ( tangent.Mag() < 0.1f ) {
// NOTE: Uncomment this if you want warning spew for invalid tangent based UV's
			Warningf("Tangent space is invalid for vert %d", i);

			if (Abs(n.x)<=Abs(n.y) || Abs(n.x)<=Abs(n.z))
			{
				tangent.Cross(n,Vector3(1,0,0));
			}
			else if (Abs(n.y)<=Abs(n.x) || Abs(n.y)<=Abs(n.z))
			{
				tangent.Cross(n,Vector3(0,1,0));				
			}
			else
			{
				tangent.Cross(n,Vector3(0,0,1));							
			}

		}
		
		// Determine binormal
		Vector3 &binormal = Verts[i].TanBi[set].B;
		binormal.Cross(n, t);
		// See if we need to flip the sign
		if ( binormal.Dot(tan2[i]) < 0.f ) {
			binormal.Scale(-1.f);
		}
		binormal.Normalize();
	}
}

void mshMaterial::ComputeTanBi(int set) {
	Assert(set < mshMaxTanBiSets);
	mshArray<float> counts;
	int vcount = Verts.GetCount();
	counts.Init(vcount);
	if (set+1>TanBiSetCount)
		TanBiSetCount=set+1;
	for (int i=0; i<vcount; i++) {
		Verts[i].TanBi[set].T.Zero();
		Verts[i].TanBi[set].B.Zero();
		if (Verts[i].TanBiCount < set+1)
			Verts[i].TanBiCount = set+1;
		counts[i] = 0.0f;
	}
	for (int j=0; j<Prim.GetCount(); j++) {
		const mshPrimitive &p = Prim[j];
		Assert(p.Type == mshTRIANGLES);
		for (int k=0; k<p.Idx.GetCount(); k+=3) {
			const mshVertex &a0 = Verts[p.Idx[k]];
			const mshVertex &a1 = Verts[p.Idx[k+1]];
			const mshVertex &a2 = Verts[p.Idx[k+2]];
			Vector3 tn, bi;
			ComputeTangent((a0.Pos),(a1.Pos),(a2.Pos),
				(a0.Tex[set]).x,(a0.Tex[set]).y,
				(a1.Tex[set]).x,(a1.Tex[set]).y,
				(a2.Tex[set]).x,(a2.Tex[set]).y,
				tn);
			ComputeBinormal((a0.Pos),(a1.Pos),(a2.Pos),
				(a0.Tex[set]).x,(a0.Tex[set]).y,
				(a1.Tex[set]).x,(a1.Tex[set]).y,
				(a2.Tex[set]).x,(a2.Tex[set]).y,
				bi);
			Verts[p.Idx[k]].TanBi[set].T += tn;
			Verts[p.Idx[k]].TanBi[set].B += bi;
			++counts[p.Idx[k]];

			Verts[p.Idx[k+1]].TanBi[set].T += tn;
			Verts[p.Idx[k+1]].TanBi[set].B += bi;
			++counts[p.Idx[k+1]];

			Verts[p.Idx[k+2]].TanBi[set].T += tn;
			Verts[p.Idx[k+2]].TanBi[set].B += bi;
			++counts[p.Idx[k+2]];
		}
	}
	for (int i=0; i<vcount; i++) {
		Verts[i].TanBi[set].T /= counts[i];
		Verts[i].TanBi[set].T.Normalize();
		Verts[i].TanBi[set].B /= counts[i];
		Verts[i].TanBi[set].B.Normalize();
	}
}


inline void mshQuantize(float &v,float scale) {
	v = (v * scale);
	if (v >= 0)
		v = floorf(v + 0.5f);
	else
		v = ceilf(v - 0.5f);
	v /= scale;
}


inline void mshQuantize(Vector2 &v,float scale) {
	mshQuantize(v.x,scale);
	mshQuantize(v.y,scale);
}


inline void mshQuantize(Vector3 &v,float scale) {
	mshQuantize(v.x,scale);
	mshQuantize(v.y,scale);
	mshQuantize(v.z,scale);
}


inline void mshQuantize(Vector4 &v,float scale) {
	mshQuantize(v.x,scale);
	mshQuantize(v.y,scale);
	mshQuantize(v.z,scale);
	mshQuantize(v.w,scale);
}


#define LAME	0

class Welder {
public:
	void SetVertexArray(const mshVertex *array,int count) {
		sm_Base = array;
		sm_Count = count;
	}

	int Build(int *remap);

	static const Vector3& GetPos(int i) {
		return sm_Base[i].Pos;
	}

	static bool AreEqual(int i,int j) {
		return (sm_Base[i] == sm_Base[j]);
	}
private:
	static const mshVertex *sm_Base;
	static int sm_Count;
};

const mshVertex *Welder::sm_Base;
int Welder::sm_Count;

const float PosScale = 2048.0f;
// const float InvPosScale = 1.0f / PosScale;
const float NrmScale = 256.0f;
const float CpvScale = 256.0f;
const float TexScale = 65536.0f;

#if __TOOL || __RESOURCECOMPILER
const float WgtScale = 1024.0f;
const float BinormalScale = 1024.0f;
#endif

#if LAME	// O(N**2) version for validation
// LAME version: On polySurface1480 (house.mb) it merged 923 vertices in 10.37 seconds
int Welder::Build(int *remap) {
	int i;
	int merged = 0;
	int bias = 0;
	for (i=0; i<sm_Count; i++)
		remap[i] = i;
	for (i=0; i<sm_Count; i++) {
		if (remap[i] == i) {	// not already remapped?
			// account for any items before us that have already been deleted
			remap[i] = (i - bias);

			// search for later items that duplicate us (but only if they
			// have not already been merged)
			for (int j=i+1; j<sm_Count; j++) {
				if (remap[j] == j && AreEqual(i,j)) {
					// i and j are the same.
					remap[j] = remap[i];
					++merged;
				}
			}
		}
		else
			++bias;			// all future entries get shuffled left more
	}
	return merged;

}
#else
struct WelderCell {
	WelderCell() : Vertex(0,0,0) {
		for (int i=0; i<8; i++)
			Kids[i] = NULL;
	}
	~WelderCell() {
		for (int i=0; i<8; i++)
			if (Kids[i])
				delete Kids[i];
	}
	int Insert(const Vector3 &pos,int index,const Vector3 &corner,float size);

	Vector3 Vertex;
	mshArray<int,8> Indices;
	WelderCell *Kids[8];
};
int WelderCell::Insert(const Vector3 &pos,int index,const Vector3 &corner,float size) {
	// Cell is empty?
	if (Indices.GetCount() == 0) {
		Vertex = pos;
		Indices.Append() = index;
		return index;
	}
	// We belong in this cell?
	else if (Vertex == pos) {
		// Are we a duplicate of somebody?
		for (int i=0; i<Indices.GetCount(); i++) {
			if (Welder::AreEqual(Indices[i],index))
				return Indices[i];
		}
		Indices.Append() = index;
		return index;
	}
	// Cell is full, and we are not duplicate, so dispatch to child.
	else {
		size *= 0.5f;
		float xSplit = corner.x + size;
		float ySplit = corner.y + size;
		float zSplit = corner.z + size;
		int xCell = pos.x >= xSplit? 1 : 0;
		int yCell = pos.y >= ySplit? 2 : 0;
		int zCell = pos.z >= zSplit? 4 : 0;
		int cell = xCell + yCell + zCell;
		if (!Kids[cell])
			Kids[cell] = rage_new WelderCell;
		return Kids[cell]->Insert(pos,index,
			Vector3(xCell?xSplit:corner.x,yCell?ySplit:corner.y,zCell?zSplit:corner.z),size);
	}
}
int Welder::Build(int *remap) {
	// Build the empty octree and determine its extrema
	WelderCell *root = rage_new WelderCell;
	// MUST be exact powers of two; grow to largest power of two necessary
	Vector3 corner(-1,-1,-1);
	float size = 2;
	for (int i=0; i<sm_Count; i++) {
		const Vector3 &v = GetPos(i);
		float ax = fabsf(v.x);
		float ay = fabsf(v.y);
		float az = fabsf(v.z);
		if (ay > ax) ax = ay;
		if (az > ax) ax = az;
		while (-ax < corner.x) {
			corner.Scale(2.0f);
			size *= 2.0f;
		}
	}
	// Displayf("Welder::Build - octree is (%f,%f,%f) size %f",corner.x,corner.y,corner.z,size);
	int merged = 0;
	// Locate duplicates in first pass over data
	for (int i=0; i<sm_Count; i++) {
		remap[i] = root->Insert(GetPos(i),i,corner,size);
		if (remap[i] != i)
			++merged;
	}
	// If there were merge operations, collapse the remap array
	if (merged) {
		int bias = 0;
		for (int i=0; i<sm_Count; i++) {
			// If we ourselves were a duplicate, all others past us shift down one
			if (remap[i] != i) {
				remap[i] = remap[remap[i]];
				++bias;
			}
			// Otherwise we were not a duplicate, so just compensate for duplicates below us
			else
				remap[i] -= bias;
		}
		Assert(merged==bias);
	}
	delete root;
	return merged;
}
#endif

int mshMaterial::Weld() {
	// Quantize first.
	int vcount = Verts.GetCount();

	// Make sure we have some verts to weld
	if (vcount==0)
	{
		return 0;
	}

	for (int i=0; i<vcount; i++) {
		mshQuantize(Verts[i].Pos,PosScale);
		mshQuantize(Verts[i].Nrm,NrmScale);
		mshQuantize(Verts[i].Cpv,CpvScale);
		mshQuantize(Verts[i].Cpv2,CpvScale);
		mshQuantize(Verts[i].Cpv3,CpvScale);
		for (int j=0; j<Verts[i].TexCount; j++)
			mshQuantize(Verts[i].Tex[j],TexScale);
	}

	int *remap = rage_new int[vcount];
	Welder W;
	W.SetVertexArray(&Verts[0],vcount);
	int welded = W.Build(remap);
	if (welded) 
	{
		int newSize = vcount - welded;
		
		mshArray<mshVertex> newVerts;
		newVerts.Resize(newSize);
		
		for (int i=0; i<Prim.GetCount(); i++)
			for (int j=0; j<Prim[i].Idx.GetCount(); j++)
				Prim[i].Idx[j] = remap[Prim[i].Idx[j]];

		for (int i=0; i<Verts.GetCount(); i++)
			newVerts[remap[i]] = Verts[i];

		Verts.Assume(newVerts);

		//Remap all of the mesh channels -- this assumes that the
		//channel data values wouldn't effect the welding. If differing
		//channel values are set on vertices that otherwise would have been
		//welded, then data could be lost here...
		for(int chnIdx=0; chnIdx<Channels.GetCount(); chnIdx++)
		{
			mshChannel remChn;
			mshChannel& oldChn = Channels[chnIdx];
			remChn.Init(oldChn.GetName(), newSize, oldChn.GetFieldCount());

			for(int i=0; i<oldChn.GetVertexCount(); i++)
			{
				for(int j=0; j<oldChn.GetFieldCount(); j++)
					remChn.Access(remap[i],j) = oldChn.Access(i,j);
			}
			Channels[chnIdx] = remChn;
		}//End for(int chnIdx=0...
	}
	delete[] remap;
	return welded;
}


#if __TOOL || __RESOURCECOMPILER
// Similar to welder, but doesn't use quantization
class NQWelder {
public:
	void SetVertexArray(const mshVertex *array,int count) {
		sm_Base = array;
		sm_Count = count;
	}

	static const Vector3& GetPos(int i) {
		return sm_Base[i].Pos;
	}

	// Check that all the attributes of a vertex are nearly equal, within some set of tolerance ranges
	static bool AreNearEqual(int aindex,int bindex) {
		int i;
		const mshVertex& a = sm_Base[aindex];
		const mshVertex& b = sm_Base[bindex];

		// These are sorted in rough order from most to least varying
		
		// Check positional equality
		if (!a.Pos.IsClose(b.Pos, 1.0f / PosScale)) {
			return false;
		}

		// OK, positions are the same, check tex coords
		if (a.TexCount != b.TexCount) {
			return false;
		}
		for (i = 0; i < a.TexCount; i++) {
			if (!a.Tex[i].IsClose(b.Tex[i], 1.0f / TexScale)) {
				return false;
			}
		}

		// Now check normals and CPVs
		if (!a.Nrm.IsClose(b.Nrm, 1.0f / NrmScale)) {
			return false;
		}
#if __RESOURCECOMPILER
		if (!a.Cpv.IsEqual(b.Cpv)) {
#else
		if (!a.Cpv.IsClose(b.Cpv, 1.0f / CpvScale)) {
#endif
			return false;
		}
#if __RESOURCECOMPILER
		if (!a.Cpv2.IsEqual(b.Cpv2)) {
#else
		if (!a.Cpv2.IsClose(b.Cpv2, 1.0f / CpvScale)) {
#endif
			return false;
		}
#if __RESOURCECOMPILER
		if (!a.Cpv3.IsEqual(b.Cpv3)) {
#else
		if (!a.Cpv3.IsClose(b.Cpv3, 1.0f / CpvScale)) {	
#endif
			return false;
		}

		// Maybe could early-out here, because if the stuff above is all the same, would the verts really
		// differ in the attributes below?

		// Finally, check tangent binormals and skeleton binding
		if (a.TanBiCount != b.TanBiCount) {
			return false;
		}
		for (i = 0; i < a.TanBiCount; i++) {
			if (!a.TanBi[i].IsClose(b.TanBi[i], 1.0f / BinormalScale)) {
				return false;
			}
		}

		for(i = 0; i < mshMaxMatricesPerVertex; i++) {
			if (a.Binding.Mtx[i] != b.Binding.Mtx[i]) {
				return false;
			}
			if (fabs(a.Binding.Wgt[i] - b.Binding.Wgt[i]) >= 1.0f / WgtScale) {
				return false;
			}
		}

		return true;
	}

#define CHECK_INEQUAL(x,y) 	if ((x) != (y)) return (x) < (y);

#define CHECK_INEQUAL_XYZ(a, b) \
	CHECK_INEQUAL((a).x, (b).x) \
	CHECK_INEQUAL((a).y, (b).y) \
	CHECK_INEQUAL((a).z, (b).z) \
// END

#define CHECK_INEQUAL_XYZW(a, b) \
	CHECK_INEQUAL((a).x, (b).x) \
	CHECK_INEQUAL((a).y, (b).y) \
	CHECK_INEQUAL((a).z, (b).z) \
	CHECK_INEQUAL((a).w, (b).w) \
// END

	struct VertLess {
		bool operator()(int aindex, int bindex) const {
			if (AreNearEqual(aindex,bindex)) {
				return false;
			}
			const mshVertex& a = sm_Base[aindex];
			const mshVertex& b = sm_Base[bindex];

			int i;

			CHECK_INEQUAL_XYZ(a.Pos, b.Pos);
			CHECK_INEQUAL(a.TexCount, b.TexCount);
			for(i = 0; i < a.TexCount; i++) {
				CHECK_INEQUAL(a.Tex[i].x, b.Tex[i].x);
				CHECK_INEQUAL(a.Tex[i].y, b.Tex[i].y);
			}
			CHECK_INEQUAL_XYZ(a.Nrm, b.Nrm);
			CHECK_INEQUAL_XYZW(a.Cpv, b.Cpv);
			CHECK_INEQUAL_XYZW(a.Cpv2, b.Cpv2);
			CHECK_INEQUAL_XYZW(a.Cpv3, b.Cpv3);

			CHECK_INEQUAL(a.TanBiCount, b.TanBiCount);
			for(i = 0; i < a.TanBiCount; i++) {
				CHECK_INEQUAL_XYZ(a.TanBi[i].B, b.TanBi[i].B);
				CHECK_INEQUAL_XYZ(a.TanBi[i].T, b.TanBi[i].T);
			}

			for(i = 0; i < mshMaxMatricesPerVertex; i++) {
				CHECK_INEQUAL(a.Binding.Mtx[i], b.Binding.Mtx[i]);
				CHECK_INEQUAL(a.Binding.Wgt[i], b.Binding.Wgt[i]);
			}
			return false;
		}
	};

	typedef std::set<int, VertLess> VertIdxSet;

	int Build(int *remap) {
		VertIdxSet verts;

		int merged = 0;
		for(int i = 0; i < sm_Count; i++) {
			VertIdxSet::const_iterator iter = verts.find(i);
			if (iter == verts.end()) {
				verts.insert(i);
				remap[i] = i;
			}
			else {
				remap[i] = *iter;
				++merged;
			}
		}

		if (merged) {
			int bias = 0;
			for (int i=0; i<sm_Count; i++) {
				// If we ourselves were a duplicate, all others past us shift down one
				if (remap[i] != i) {
					remap[i] = remap[remap[i]];
					++bias;
				}
				// Otherwise we were not a duplicate, so just compensate for duplicates below us
				else
					remap[i] -= bias;
			}
			Assert(merged==bias);
		}

		return merged;
	}

private:
	static const mshVertex *sm_Base;
	static int sm_Count;
};

const mshVertex* NQWelder::sm_Base;
int NQWelder::sm_Count;

int mshMaterial::WeldWithoutQuantize() {
	int vcount = Verts.GetCount();

	if (vcount==0)
	{
		return 0;
	}

	int *remap = rage_new int[Verts.GetCount()];

	NQWelder W;
	W.SetVertexArray(&Verts[0], vcount);

	int welded = W.Build(remap);
	if (welded) 
	{
		int newSize = vcount - welded;
		mshArray<mshVertex> newVerts;
		newVerts.Resize(newSize);
		for (int i=0; i<Prim.GetCount(); i++)
			for (int j=0; j<Prim[i].Idx.GetCount(); j++)
				Prim[i].Idx[j] = remap[Prim[i].Idx[j]];
		for (int i=0; i<Verts.GetCount(); i++)
			newVerts[remap[i]] = Verts[i];
		Verts.Assume(newVerts);

		//Remap all of the mesh channels -- this assumes that the
		//channel data values wouldn't effect the welding. If differing
		//channel values are set on vertices that otherwise would have been
		//welded, then data could be lost here...
		for(int chnIdx=0; chnIdx<Channels.GetCount(); chnIdx++)
		{
			mshChannel remChn;
			mshChannel& oldChn = Channels[chnIdx];
			remChn.Init(oldChn.GetName(), newSize, oldChn.GetFieldCount());

			for(int i=0; i<oldChn.GetVertexCount(); i++)
			{
				for(int j=0; j<oldChn.GetFieldCount(); j++)
					remChn.Access(remap[i],j) = oldChn.Access(i,j);
			}
			Channels[chnIdx] = remChn;
		}//End for(int chnIdx=0...
	}
	delete [] remap;
	return welded;
}
#endif	// __TOOL

void mshMaterial::CopyNongeometricData(mshMaterial& dest) const
{
	// delete any current data in dest
	dest.Prim.Reset();
	dest.Verts.Reset();
	dest.Attr.Reset();
	dest.Channels.Reset();

	dest.Name = Name;
	dest.Priority = Priority;
	dest.TexSetCount = TexSetCount;
	dest.TanBiSetCount = TanBiSetCount;

	dest.Attr.Resize(Attr.GetCount());
	for(int i = 0; i < Attr.GetCount(); i++) {
		dest.Attr[i] = Attr[i];
	}

	dest.Channels.Resize(GetChannelCount());
	for(int i = 0; i < GetChannelCount(); i++) {
		dest.Channels[i].Init(Channels[i].GetName(), 0, Channels[i].GetFieldCount());
	}
}

bool mshMaterial::SameNongeometricData(mshMaterial& that) {
	if (Priority != that.Priority) {
		return false;
	}
	if (TexSetCount != that.TexSetCount || TanBiSetCount != that.TanBiSetCount) {
		return false;
	}

	if (Attr.GetCount() != that.Attr.GetCount()) {
		return false;
	}

	// should sort the attribute lists?
	for(int i = 0; i < Attr.GetCount(); i++) {
		if (stricmp(Attr[i].GetName(), that.Attr[i].GetName())) {
			return false;
		}
		if (stricmp(Attr[i].GetValue(), that.Attr[i].GetValue())) {
			return false;
		}
	}

	if (Channels.GetCount() != that.Channels.GetCount()) {
		return false;
	}

	// sort the channel lists?
	for(int i = 0; i < Channels.GetCount(); i++) {
		if (Channels[i].GetName() != that.Channels[i].GetName()) {
			return false;
		}
		if (Channels[i].GetFieldCount() != that.Channels[i].GetFieldCount()) {
			return false;
		}
	}

	return true;
}

int mshMaterial::GetRawChannelData(int vert, mshChannelData* outData)
{
	int size = 0;
	for(int i = 0; i < Channels.GetCount(); i++) {
		size += Channels[i].GetFieldCount();
	}

	outData = rage_new mshChannelData[size];

	int element = 0;
	for(int i = 0; i < Channels.GetCount(); i++) {
		for(int j = 0; j != Channels[i].GetFieldCount(); j++) {
			outData[element] = Channels[i].Access(vert, j);
			element++;
		}
	}
	Assert(element == size);
	return element;
}

mshMaterial::TriangleIterator mshMaterial::BeginTriangles()
{
	mshMaterial::TriangleIterator iter;
	iter.m_Material = Prim.GetCount() > 0 ? this : NULL;
	iter.m_CurrPrim = 0;
	iter.m_CurrTri = 0;
	return iter;
}

mshMaterial::TriangleIterator mshMaterial::EndTriangles()
{
	mshMaterial::TriangleIterator iter;
	iter.m_Material = NULL;
	iter.m_CurrPrim = 0;
	iter.m_CurrTri = 0;
	return iter;
}

bool mshMaterial::TriangleIterator::operator==(const mshMaterial::TriangleIterator& other) const {
	if (m_Material == NULL) {
		return m_Material == other.m_Material;
	}
	return m_Material == other.m_Material &&
		m_CurrPrim == other.m_CurrPrim && 
		m_CurrTri == other.m_CurrTri;
}

void mshMaterial::TriangleIterator::GetVertIndices(int& a, int& b, int& c) {
	Assert(m_Material);
	mshPrimitive& prim = m_Material->Prim[m_CurrPrim];

	switch(prim.Type) {
	case mshTRIANGLES:
		{
			int triNum = m_CurrTri * 3;
			a = prim.Idx[triNum+0];
			b = prim.Idx[triNum+1];
			c = prim.Idx[triNum+2];
		}
		return;
	case mshPOLYGON:
		{
			a = prim.Idx[0];
			b = prim.Idx[m_CurrTri+1];
			c = prim.Idx[m_CurrTri+2];
		}
		return;
	case mshTRISTRIP:
		{
			a = prim.Idx[m_CurrTri];
			if ((m_CurrTri & 0x1) == 0) { // even triangles
				b = prim.Idx[m_CurrTri+1];
				c = prim.Idx[m_CurrTri+2];
			}
			else { // odd triangles
				b = prim.Idx[m_CurrTri+2];
				c = prim.Idx[m_CurrTri+1];
			}
		}
		return;
	case mshTRISTRIP2:
		{
			a = prim.Idx[m_CurrTri];
			if ((m_CurrTri & 0x1) == 1) { // odd triangles
				b = prim.Idx[m_CurrTri+1];
				c = prim.Idx[m_CurrTri+2];
			}
			else { // even triangles
				b = prim.Idx[m_CurrTri+2];
				c = prim.Idx[m_CurrTri+1];
			}
		}
		return;
	case mshQUADS:
	default:
		a = b = c = -1;
		AssertMsg(0 , "Primitive type not handled");
	}
}

mshMaterial::TriangleIterator& mshMaterial::TriangleIterator::operator ++ () {
	if (!m_Material) {
		return *this;
	}

	mshPrimitive& prim = m_Material->Prim[m_CurrPrim];

	m_CurrTri++;
	if (m_CurrTri >= prim.GetTriangleCount()) {
		m_CurrTri = 0;
		m_CurrPrim++;
		if (m_CurrPrim >= m_Material->Prim.GetCount()) {
			m_CurrPrim = 0;
			m_Material = NULL;
		}
	}
	return *this;
}

void mshBinding::Serialize(mshSerializer &S) {
	if (!S.Writing)
	{
		Reset();
	}
	for (int i=0; i<mshMaxMatricesPerVertex; i++) 
	{
		rage::Serialize(S,Wgt[i]);
		if (!Wgt[i])
		{
			break;
		}
		if (S.T.IsBinary()) 
		{
			// copy in and out so serialization works either way

			u8 temp = (u8) Mtx[i];
			rage::Serialize(S,temp);
			Mtx[i] = temp;
		}
		else
		{
			rage::Serialize(S,Mtx[i]);
		}
	}
}


}	// namespace rage

#endif
