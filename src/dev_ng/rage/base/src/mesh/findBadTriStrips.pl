# given a directory, it will search recursively for all .mesh files.
# then it will see which mesh file has bad tristripping, for whatever reason.

use strict;
use Cwd;

#######################################################3
# no longer in use.. please use
# findNearQuads.pl to build the look.csv
# then  buildMeshReport.pl  to build the html pages
#########################################################

print "This code is no longer in use.\n";
print "Please use  findNearQuads.pl to build the look.csv file\n";
print "then buildMeshReport.pl to build the html pages\n";
print "Friday, March 19, 2004 - Robert Suh\n";
exit;



my $dir = "t:\\agent\\assets\\"; # default path, if none is given
if ($ARGV[0])
{
	$dir = $ARGV[0];
}

if (!-e $dir)
{
	print "Directory ($dir) is not found!\n";
	exit;
}

print "Grabbing all .mesh files, recursively in $dir...\nPlease be patient...\n";
my $oldDir = cwd();	# save off the original current working dir
chdir($dir);	 	
my $command = 'for /r %F in (*.mesh) do @echo %F %~nF %~dF %~pF';
my $res = `$command`;
my @files = split(/\n/, $res);
chdir ($oldDir);	# restore original directory

my $total = @files;
my $counter=0;
open (OUTFILE, ">look.csv");


foreach my $line (@files)
{
	$line =~ m/^(.+\.mesh)\s+(.+)\s+(.:)\s+(.+)$/i;

	my $fullFileName = $1;
	my $baseName = $2;
	my $drive = $3;
	my $path = $4;

	$counter++;
	print "($counter of $total)\n";
	my ($stripCount, $avg, $matCount) = processFile($fullFileName);
	$avg = sprintf('%5.03f', $avg);
	my $ratio = $stripCount/$avg;
	print OUTFILE "$fullFileName,file:\\\\$drive$path,$baseName,$avg,$ratio,$stripCount,$matCount\n";

}

print "Total Files ($counter)\n";
close (OUTFILE);

exit;


sub processFile ($)
{
	my $filename = shift;

	print "FILE ($filename)\n";
	open (FILE, "$filename");
	my @data = <FILE>;
	close (FILE);				

	my $insideMaterialTag = 0;
	my $lastType = undef;
	my $stripCount=0;
	my $stripLenAccum=0;
	my $materialCount=0;
	foreach (@data)
	{
		if ($_ =~ m/mtl\s+\d+/i)
		{
			$insideMaterialTag=1;
		   # print "INside ($_)\n";
		}
		if ($insideMaterialTag)
		{
   			if ($_ =~ m/Name\s\"(.+)\"/i)
			{
				#print "Material Name = ($1)\n";	
				$materialCount++;
			}
			if ($_ =~ m/Type\s+(.+)/i)
			{
			  #  print "TYPE ($1)\n";
				$lastType = $1;
			}
			if ($_ =~ m/Idx\s+(\d+)/i)
			{
				#print "    $lastType : ($1)\n";
				$stripLenAccum += $1;
				$stripCount++;
			}
		}
	}
	my $average = $stripLenAccum / $stripCount;
	print "Average Strips Len ($average)\n";
	return ($stripCount, $average, $materialCount);
	@data = undef;

}

