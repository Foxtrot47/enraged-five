//
// mesh/serialize.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "serialize.h"
#include "string/string.h"

namespace rage {

void mshSerializer::BeginValue(const char *tag) {
	if (Writing) {
		T.StartLine();
		if (tag) {
			T.PutDelimiter(tag);
			T.PutDelimiter(" ");
		}
	}
	else if (tag)
		T.GetDelimiter(tag);
}

void mshSerializer::EndValue() {
	if (Writing)
		T.EndLine();
}

void mshSerializer::BeginBlock() {
	if (Writing) {
		T.StartLine();
		T.PutDelimiter("{");
		T.EndLine();
		T.Indent(1);
	}
	else
		T.GetDelimiter("{");
}

void mshSerializer::EndBlock() {
	if (Writing) {
		T.Indent(-1);
		T.StartLine();
		T.PutDelimiter("}");
		T.EndLine();
	}
	else
		T.GetDelimiter("}");
}

void mshSerializer::Separator(const char *sep) {
	if (Writing) {
		T.PutDelimiter(sep);
		T.PutDelimiter(" ");
	}
	else
		T.GetDelimiter(sep);
}

void Serialize(mshSerializer &S,bool &v) {
	if (S.Writing)
		S.T.PutByte(v);
	else
		v = S.T.GetByte() != 0;
}

void Serialize(mshSerializer &S,mshIndex &v) {
	if (S.Writing) {
		S.T.Put((int)v);
	}
	else {
		v = (mshIndex) S.T.GetInt();
	}
}

void Serialize(mshSerializer &S,unsigned char &v) {
	if (S.Writing)
		S.T.PutByte(v);
	else
		v = (u8) S.T.GetByte();
}

void Serialize(mshSerializer &S,float &v) {
	if (S.Writing)
		S.T.Put(v);
	else
		v = S.T.GetFloat();
}

void Serialize(mshSerializer &S,u32 &v) {
	if (S.Writing)
		S.T.Put((int)v);
	else
		v = (u32)S.T.GetInt();
}

void Serialize(mshSerializer &S,Vector2 &v) {
	if (S.Writing) {
		S.T.Put(v.x);
		S.T.Put(v.y);
	}
	else
		S.T.GetVector(v);
}

void Serialize(mshSerializer &S,Vector3 &v) {
	if (S.Writing) {
		S.T.Put(v.x);
		S.T.Put(v.y);
		S.T.Put(v.z);
	}
	else
		S.T.GetVector(v);
}

void Serialize(mshSerializer &S,Vector4 &v) {
	if (S.Writing) {
		S.T.Put(v.x);
		S.T.Put(v.y);
		S.T.Put(v.z);
		S.T.Put(v.w);
	}
	else
		S.T.GetVector(v);
}

void Serialize(mshSerializer &S,const char *&v) {
	if (S.Writing) {
		if (S.T.IsBinary()) {
			S.T.PutShort((int)strlen(v)+1);
			S.T.Put(v);
		}
		else
			S.T.PutStr("\"%s\" ",v);
	}
	else {
		char temp[256];
		if (S.T.IsBinary()) {
			int len = S.T.GetShort();
			for (int i=0; i<len; i++)
				temp[i] = (char)S.T.GetByte();
		}
		else
			S.T.GetToken(temp,sizeof(temp));
		v = StringDuplicate(temp);
	}
}

void Serialize(mshSerializer &S,ConstString &v) {
	if (S.Writing) {
		if (S.T.IsBinary()) {
			S.T.PutShort((int)strlen(v)+1);
			S.T.Put((const char*)v);
		}
		else
			S.T.PutStr("\"%s\" ",v.m_String);
	}
	else {
		char temp[256];
		if (S.T.IsBinary()) {
			int len = S.T.GetShort();
			for (int i=0; i<len; i++)
				temp[i] = (char)S.T.GetByte();
		}
		else
			S.T.GetToken(temp,sizeof(temp));
		v = temp;
	}
}

}	// namespace rage
