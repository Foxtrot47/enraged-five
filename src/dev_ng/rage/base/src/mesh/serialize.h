//
// mesh/serialize.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef MESH_SERIALIZE_H
#define MESH_SERIALIZE_H

#include "file/stream.h"
#include "mesh/array.h"
#include "file/asset.h"
#include "file/token.h"
#include "string/string.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "atl/ptr.h"

namespace rage {

/*
PURPOSE
	This class implements a serializer object which allows simplified loading and saving code.

NOTES
	Here are following requirements for this class:

	* ASCII and Binary formats are both supported (implies no XML)
	* Can support existing basic classes (implies global overloaded functions)
	* Supports existing array classes
	* Backward and forward compatibile during reads (like datParser)

	Items fall into three classes: intrinsics, arrays, and structures.  All classes
	are tagged when they appear within a structure, but not when they are the
	fundamental element of an array.

	An intrinsic is either a scalar or a simple type like Vector3.  Intrinsics always
	parse to the end of the current line in text files.

	An array is zero or more instances of any of the three classes.  Arrays contain
	an element count in addition to their instance list.  All arrays are handled
	by the global template Serialize function.

	A structure is a collection of named items.

	Any serializable type must implement a global function with the signature
	"void Serialize(mshSerializer &S,TYPENAME &v)".  If the TYPENAME represents
	a struct or class type, that function may choose to forward the request to 
	a member function to avoid breaking encapsulation.

<FLAG Component>
*/
struct mshSerializer {
	mshSerializer(fiBaseTokenizer &t,bool w) : T(t), Writing(w) { }
	fiBaseTokenizer &T;
	bool Writing;
	void BeginValue(const char *tag);
	void EndValue();
	void BeginBlock();
	void EndBlock();
	void Separator(const char *sep);
};

void Serialize(mshSerializer &S,bool &v);
void Serialize(mshSerializer &S,mshIndex &v);
void Serialize(mshSerializer &S,unsigned char &v);
void Serialize(mshSerializer &S,float &v);
void Serialize(mshSerializer &S,u32 &v);
void Serialize(mshSerializer &S,Vector2 &v);
void Serialize(mshSerializer &S,Vector3 &v);
void Serialize(mshSerializer &S,Vector4 &v);
void Serialize(mshSerializer &S,const char *&v);
void Serialize(mshSerializer &S,ConstString &v);

template <class T> void Serialize(mshSerializer &S,mshArray<T> &v) {
	if (S.Writing) {
		S.T.Put(v.GetCount());
		if (v.GetCount()) {
			S.T.PutDelimiter("{ ");
			if (sizeof(T) <= sizeof(int)) {
				const int perLine = 15;
				if (v.GetCount() >= perLine) {
					S.T.Indent(1);
					S.T.EndLine();
					S.T.StartLine();
				}
				for (int i=0; i<v.GetCount(); i++) {
					Serialize(S,v[i]);
					if (v.GetCount() >= perLine && (i % perLine) == perLine-1) {
						S.T.EndLine();
						S.T.StartLine();
					}
				}
				if (v.GetCount() >= perLine) {
					S.T.Indent(-1);
					S.T.EndLine();
					S.T.StartLine();
				}
				S.T.PutDelimiter("}");
			}
			else {
				S.T.EndLine();
				S.T.Indent(1);
				for (int i=0; i<v.GetCount(); i++) {
					S.T.StartLine();
					Serialize(S,v[i]);
					S.T.EndLine();
				}
				S.T.Indent(-1);
				S.T.StartLine();
				S.T.PutDelimiter("}");
				S.T.EndLine();
			}
		}
	}
	else {
		int c = S.T.GetInt();
		v.Reset(c);
		if (c) {
			S.T.GetDelimiter("{");
			while (c--)
				Serialize(S,v.Append());
			S.T.GetDelimiter("}");
		}
	}
}

template <class T> void Serialize(mshSerializer &S,atPtr<T> &v) {
	if (!S.Writing) {
		if (S.T.IsBinary()) {
			int type;
			rage::Serialize(S,type);
			v = T::Create(type);
		}
		else {
			char buf[32];
			S.T.GetToken(buf,sizeof(buf));
			v = T::Create(buf);
		}
	}
	else {
		if (S.T.IsBinary()) {
			int type = v->GetType();
			rage::Serialize(S,type);
		}
		else {
			S.T.PutStr("%s ",v->GetTypeName());
		}
	}
	Serialize(S,*v);
}

template <class T> bool SerializeToAsciiFile(const char *filename,const T& serializable) {
	fiStream *stream = fiStream::Create(filename);
	if (!stream)
		return false;
	stream->PutCh(13);
	stream->PutCh(10);
	fiAsciiTokenizer t;
	mshSerializer serializer(t,true);
	t.Init(filename,stream);
	rage::Serialize(serializer,const_cast<T&>(serializable));
	stream->Close();
	return true;
}

template <class T> bool SerializeToBinaryFile(const char *filename,const T& serializable) {
	fiStream *stream = fiStream::Create(filename);
	if (!stream)
		return false;
	stream->PutCh(26);
	fiBinTokenizer t;
	mshSerializer serializer(t,true);
	t.Init(filename,stream);
	rage::Serialize(serializer,const_cast<T&>(serializable));
	stream->Close();
	return true;
}

template <class T> bool SerializeFromFile(const char *filename,T& serializable,const char *ext) {
	//Stream *stream = Stream::Open(filename);
	fiSafeStream stream(fiStream::PreLoad(ASSET.Open(filename,ext)));
	if (!stream)
		return false;
	int ch = stream->GetCh();
	if (ch == 26) {
		fiBinTokenizer t;
		t.Init(filename,stream);
		mshSerializer serializer(t,false);
		rage::Serialize(serializer,serializable);
	}
	else {
		// Handle files we didn't write cleanly.
		if (ch != 13)
			stream->Seek(0);
		fiAsciiTokenizer t;
		t.Init(filename,stream);
		mshSerializer serializer(t,false);
		rage::Serialize(serializer,serializable);
	}
	//stream->Close();
	return true;
}


template <class T> bool SerializeFromStream(fiStream *stream,T& serializable) {
	if (!stream)
		return false;
	fiAsciiTokenizer t;
	t.Init(stream->GetName(),stream);
	mshSerializer serializer(t,false);
	rage::Serialize(serializer,serializable);
	return true;
}


#define SERIALIZE(S,tag) do { S.BeginValue(#tag); rage::Serialize(S,tag); S.EndValue(); } while (0)
#define SERIALIZET(S,name,field) do { S.BeginValue(name); rage::Serialize(S,field); S.EndValue(); } while (0)

}	// namespace rage

#endif
