# Asset Assistant!
# daily reporting on a project's assets.
                                                                                  
# directory report                                                               
# sub-directory reports (asset_assistant)
use strict;
use Data::Dumper;
use rsGraph;

my @winger = ( "|", "/", "-", "\\" );
my $wingerCounter=0;


my $outputPath = "c:\\agentReport\\";

if ($ARGV[0])
{
	$outputPath = $ARGV[0];
}
else
{
   print "USAGE   : buildMeshReport.pl directory2OutputHTMLFiles\n";
   print "\n\n";
   print "Example : buildMeshReport.pl c:\\agentReport\\\n";
   exit;
}
if (-e $outputPath) 
{
   # directory already exists..
}
else
{
   # make new directory
   print "Directory ($outputPath) does not exist.. creating new dir\n";
   system("mkdir $outputPath");
}




open (FILE, "look.csv");
my @data = <FILE>;
close (FILE);

###############################################################################
#           strip off the first CSV line to obtain HEADER titles
###############################################################################
my $titles = shift (@data);   # dump off the header
chomp($titles);
my @titleList = split(/,/,$titles);
my @newTitles;
foreach my $xxx (@titleList)
{
   $xxx =~ s/\"//ig;
   push (@newTitles, $xxx);
}
@titleList = @newTitles;


my %masterNode;

my $counter=0;
my $total = @data;
print "Reading in .CSV file...\n";
print "========================================================================\n";
print "     Reading CSV File\n";
print "========================================================================\n";
foreach my $line (@data)
{        
   $counter++;
   if ($counter> 10) 
   {
     # last;      # uncomment this for testing.. it will bail out after 200 entries
   }

   my @array = split(/,/, $line);
   my $fullFileName = $array[0];
   #print "$fullFileName\n";
   print "\r$counter of $total";
   my @nodes = split(/\\/, $fullFileName);
   placeInNode(\%masterNode, \@nodes, $line);
}
print "\n";
print "========================================================================\n";
print "     Processing Started\n";
print "========================================================================\n";
processNodes(\%masterNode);
print "\n";
print "========================================================================\n";
print "     Processing Ended\n";
print "========================================================================\n";

print "========================================================================\n";
print "     Building HTML Pages\n";
print "========================================================================\n";
my @linkBack = ("index");
printReport ("Asset Report", \%masterNode,\@linkBack);
print "========================================================================\n";
print "     Done.\n";
print "========================================================================\n";

my $indexPage = "$outputPath\\index.html";
open (FILE, ">$indexPage");
my @list = getFirstPages(\%masterNode);
foreach (@list)
{
   print FILE "<a href=\"$_\">Goto Main at ($_)</a><br>";
}
close (FILE);

`start $indexPage`;

sub getFirstPages($) 
{
   my $hashPtr = shift;

   my @out;

   my $firstPage = $hashPtr->{__PageFileName};
   if ($firstPage eq undef) 
   {
      my @totalChildrenList = sort(keys (%$hashPtr));
      my $totalChildrenCount = @totalChildrenList;
      foreach my $childName (@totalChildrenList)
      {
         if ($childName =~ m/^__/i) 
         {
         }
         else
         {
            my @list = getFirstPages($hashPtr->{$childName});
            push (@out, @list);
         }
      }
   }
   else
   {
      push (@out , $firstPage);
   }
   return @out;

}

#$masterNode{__PageTitle} = "index";
#$masterNode{__PageFileName} = "index.html";


###############################################################################
#                 end
###############################################################################

sub placeInNode($$) # ptr to masterHashNode, $line 
{
   my $NodeHashPtr = shift;
   my $nodeListPtr = shift;
   my $payLoad = shift;
                                             
   my $nodeName = shift (@$nodeListPtr);
   if ($nodeName eq undef) 
   {
      $NodeHashPtr->{__Payload} = $payLoad;
      return;
   }
   if ($NodeHashPtr->{$nodeName} eq undef) 
   {
      my %hash;                  
      $NodeHashPtr->{$nodeName} = \%hash;
      my $hashPtr = $NodeHashPtr->{$nodeName};
   }
      # add it..
      my $childNodePtr = $NodeHashPtr->{$nodeName};
      my $name = $nodeName;
      #$name =~ s/[^A-Za-z0-9_]//ig;
      #$name = "p_".$name;
      $childNodePtr->{__PageTitle} = $name;
      $childNodePtr->{__Parent} = $NodeHashPtr;
      my $postPath = buildPostPath($childNodePtr,$name);
      my $dir = buildSubDir($postPath);
      #print "DIR ($dir\\$postPath)\n";
      $childNodePtr->{__PageFileName} = "$dir\\$postPath.html";
      placeInNode ($childNodePtr, $nodeListPtr, $payLoad);
}


# pass in link pack array ptr
sub buildPrefix($) 
{
   my $arrayPtr = shift;
   my $out = "unique";
   foreach my $item (@$arrayPtr)
   {
      #remove anything that might fuck up a filename
      $item =~ s/[^A-Za-z0-9_]//ig;
      $out .= "_".$item."_";
   }
   $out =~ s/^_+//ig;
   return $out;
}

sub buildPrePath($)  # given a node, it will give ya a PREFIX FILENAME
{
   my $nodePtr = shift;
   my $out;
   my @outList;

   my $parentNodePtr = $nodePtr->{__Parent};
   while(1)
   {
      if ($parentNodePtr ne undef) 
      {
         my $title = $parentNodePtr->{__PageTitle};
         $title =~ s/[^A-Za-z0-9_]//ig;
         push (@outList, "$title");
      }
      else
      {
         last;
      }
      $parentNodePtr = $parentNodePtr->{__Parent};
   }
   @outList = reverse(@outList);
   $out = join('_', @outList);
   $out =~ s/^_+//ig;
   return $out;
}

sub buildPostPath($$) # given a nodePtr, and a new Entity
{
   my $nodePtr = shift;
   my $title   = shift;
   my $prePath = buildPrePath($nodePtr);
   $title =~ s/[^A-Za-z0-9_]//ig;
   my $out = "$prePath"."_"."$title";
   $out =~ s/^_+//ig;
   return $out;
}

sub buildSubDir($) 
{
   my $keycode = shift;

   my $crc = crc32($keycode);

#   print "CRC ($crc)\n";
   my $code1 = substr($crc, 0,3);
   my $code2 = substr($crc, 3,3);
   my $code3 = substr($crc, 6,3);

#   print "($code1) ($code2) ($code3)\n";

   my $relDir = "reportData\\$code1\\$code2\\$code3\\";
   my $absDir = "$outputPath\\$relDir";

   if (-e $absDir) 
   {
     # print "Already Exists!\n";
   }
   else
   {
      system ("md $absDir");
   }

   #print "ABS ($absDir)\n";

   return ($absDir);
}


###############################################################################
#                 Print Report
###############################################################################
sub printReport($$$$)    
{  
   my $title = shift;
   my $NodeHashPtr = shift;
   my $parentLinkBackArrayPtr = shift;

   my $prefix = buildPrefix($parentLinkBackArrayPtr);

   my %triangleCountHash;
   my %materialHash;
   my %primCountHash;
   my %nearMappedHash;
   my %fullMappedHash;
   my %overMappedHash;

   my %node2FileName;

   drawWinger();


   if ($NodeHashPtr->{__Info} ne undef) 
   {
      #########################################################################
      #        Grab all children info for Graphs
      #########################################################################
      my @totalChildrenList = sort(keys (%$NodeHashPtr));
      my $totalChildrenCount = @totalChildrenList;
      foreach my $childName (@totalChildrenList)
      {
         if ($childName =~ m/^__/i) 
         {
         }
         else
         {
            if ($NodeHashPtr->{$childName}->{__Info} ne undef) 
            {
               $node2FileName{$childName} = $NodeHashPtr->{$childName}->{__PageFileName};
               my $matCount = $NodeHashPtr->{$childName}->{__Info}->{TotalMaterial};
               $materialHash{"$childName"} = $matCount;
               my $primCount = $NodeHashPtr->{$childName}->{__Info}->{TotalPrimitives};
               $primCountHash{"$childName"} = $primCount;
               my $triangleCount = $NodeHashPtr->{$childName}->{__Info}->{TriangleCount};
               $triangleCountHash{"$childName"} = $triangleCount;
               my $nearCount = $NodeHashPtr->{$childName}->{__Info}->{TotalNearMappedQuads};
               $nearMappedHash{"$childName"} = $nearCount;
               my $fullCount = $NodeHashPtr->{$childName}->{__Info}->{TotalFullMappedQuads};
               $fullMappedHash{"$childName"} = $fullCount;
               my $overCount = $NodeHashPtr->{$childName}->{__Info}->{TotalOverMappedQuads};
               $overMappedHash{"$childName"} = $overCount;
            }
         }
      }
   }

   my @childrenPtrList = sort(keys (%$NodeHashPtr));
   my %parentInfoHash;
   foreach my $childNodeName (@childrenPtrList)
   {
      if ($childNodeName =~ m/^__/i)
      {
      }
      else
      {
            my $type =  ref $NodeHashPtr;
            if ($type eq "HASH") 
            {
               my $childNodePtr = $NodeHashPtr->{$childNodeName};
               my $type = ref $childNodePtr;
               if ($type eq "HASH") 
               {
                  push (@$parentLinkBackArrayPtr, "$title");
                  printReport($childNodeName,$childNodePtr,$parentLinkBackArrayPtr);
                  pop (@$parentLinkBackArrayPtr);
               }
            }
         
      }
   }

   push (@$parentLinkBackArrayPtr, "$title");

   $title =~ s/[^A-Za-z0-9_]//ig;

   my $fname = $NodeHashPtr->{__PageFileName};
  # my $pageFileName = "$outputPath\\$fname";
   my $pageFileName  = "$fname";
   open(FILEOUT, ">$pageFileName");
   my $perlScript = getPerlScript();
   print FILEOUT '<html><head><title>Asset Report - '.$title.'</title>'.$perlScript.'</head><body>\n';
   print FILEOUT boxOpen("#000000", "#FFFFFF");

   print FILEOUT showTitle("Report on <b><i>$title</i></b>\n");


   ############################################################################
   #              Print Top Level Navigator
   ############################################################################


   my $markerPtr = $NodeHashPtr;
   my @revList;
   my $parentNode = $markerPtr;
   while(1)
   {
      last if ($parentNode eq undef);
      my $pageTitle = $parentNode->{__PageTitle};
      my $pageFileName = $parentNode->{__PageFileName};
     # print "PAGE ($pageFileName)\n";
      if ($pageTitle) 
      {
         my $str =  "<a href=\"$pageFileName\">[ $pageTitle ]</a> &gt; ";
         push (@revList, $str);
      }
      $parentNode = $parentNode->{__Parent};
   }                       
   @revList = reverse(@revList);
   print FILEOUT boxOpen("#8888FF","#DDDDFF");
   foreach (@revList)
   {
      print FILEOUT "$_";
   }
   print FILEOUT boxClose();
   
   
   print FILEOUT "<BR>";



   ############################################################################
   #           Print Information of the NOde
   ############################################################################
   my $infoHash = $NodeHashPtr->{__Info};
   
   if ($infoHash->{FullPath} ne undef) 
   {
      my $path = $infoHash->{FullPath};
      print FILEOUT "Goto <a href=\"$path\">$path</a><br><br>";
   }
   print FILEOUT '<form name="myform">';
   if ($infoHash->{FullPath} ne undef) 
   {
      print FILEOUT '<INPUT TYPE="button" VALUE="View Tristrips" onClick=openTristripViewer(\''.$infoHash->{FullPath}.'\')>';
   }
   if ($infoHash->{Path} ne undef) 
   {
      my $path = $infoHash->{Path};
      $path =~ s/\\$//ig;
      $path =~ s/^file://ig;
      $path =~ s/^\\\\//ig;
      print FILEOUT '<INPUT TYPE="button" VALUE="Goto Dir :" onClick=systemStart(\''.$path.'\')>'.$path.'<br>';
   }
   print FILEOUT '</form>';                                    


   
   print FILEOUT boxOpen("#FFFF88","#FFFFEE");
   if ($infoHash ne undef) 
   {
      my @keys = sort( keys (%$infoHash));
      foreach (@keys)
      {
         my $value = $infoHash->{$_};
         print FILEOUT "[<b>$_</b>] = [$value]<br>";
      }
   }
   print FILEOUT boxClose();
   ############################################################################

   $title=~ s/[^A-Za-z0-9_]//ig;

   my $pageFileName = $NodeHashPtr->{__PageFileName};

   if ($pageFileName ne undef) 
   {
      my $output = drawGraph(\%triangleCountHash, \%node2FileName, "$pageFileName"."_graph_tricount",   "Triangle Counts",         "Object", "Total Triangles Used");
      print FILEOUT  $output;                     
      my $output = drawGraph(\%nearMappedHash, \%node2FileName, "$pageFileName"."_graph_nearmapped", "Near Mapped Quad Counts", "Object", "Near Mapped Quads Used");
      print FILEOUT  $output;                     
      my $output = drawGraph(\%fullMappedHash, \%node2FileName, "$pageFileName"."_graph_fullmapped", "Full Mapped Quad Counts", "Object", "Full Mapped Quads Used");
      print FILEOUT  $output;                     
      my $output = drawGraph(\%overMappedHash, \%node2FileName, "$pageFileName"."_graph_overmapped", "Over Mapped Quad Counts", "Object", "Over Mapped Quads Used");
      print FILEOUT  $output;   
   }

   my $infoHash = $NodeHashPtr->{__Info};
   if ($pageFileName ne undef) 
   {
      if ($infoHash ne undef) 
      {
         if ($infoHash->{StripHistogram} ne undef) 
         {
            my $hist = $infoHash->{StripHistogram};
            my @list = split(/\s+/, $hist);
            my %histoHash;
            foreach (@list)
            {
               m/\[(\d+):(\d+)\]/i;
               my $stripLen = $1;
               my $totalStrips = $2;
               $histoHash{ $stripLen} = $totalStrips;
            }
            my $output = drawGraph(\%histoHash, \%node2FileName,"$pageFileName"."_graph_histograph", "Strip Length Histogram", "Strip Length", "Total Primitive Count");
            print FILEOUT  $output;   
         }
         if ($infoHash->{MaterialHistogram} ne undef) 
         {
            my $hist = $infoHash->{MaterialHistogram};
            my @list = split(/\s+/, $hist);
            my %histoHash;
            foreach (@list)
            {
               m/\[(.+?):(\d+)\]/i;
               my $stripLen = $1;
               my $totalStrips = $2;
               $histoHash{$stripLen} = $totalStrips;
            }
            my $output = drawGraph(\%histoHash, \%node2FileName,"$pageFileName"."_graph_material_histograph", "Material Frequency Histogram", "Material", "Total Primitives Using it");
            print FILEOUT  $output;   
         }
         if ($infoHash->{PrimTypeHistogram} ne undef) 
         {
            my $hist = $infoHash->{PrimTypeHistogram};
            my @list = split(/\s+/, $hist);
            my %histoHash;
            foreach (@list)
            {
               m/\[(.+?):(\d+)\]/i;
               my $stripLen = $1;
               my $totalStrips = $2;
               $histoHash{"$stripLen"} = $totalStrips;
            }
            my $output = drawGraph(\%histoHash, \%node2FileName,"$pageFileName"."_graph_primtype_histograph", "Prim Type Histogram", "Primitive Type", "Total Primitives Using it");
            print FILEOUT  $output;   
         }
      }
   }

   my $prefix = buildPrefix($parentLinkBackArrayPtr);

   ############################################################################
   #                 Display Child Links
   ############################################################################
   my $counter=0;

   my %lcChildHash;
   foreach my $childNodeName (@childrenPtrList)
   {
      if ($childNodeName =~ m/^__/i) 
      {
      }
      else
      {
         my $lower = lc($childNodeName);
         $lcChildHash{$lower} = $childNodeName;      
      }
   }
   my @childListNames = sort (keys %lcChildHash);
   my $totalChildren = @childListNames;

   if ($totalChildren > 0) 
   {
      print FILEOUT boxOpen("#88FF88", "#EEFFEE");
      my $maxPerColumn = $totalChildren / 2;
      print FILEOUT "<font size=5>Total Children = $totalChildren</font><br>";
      print FILEOUT "<table width=100".'%'."border=1><tr><td>";
      my $tdopen=1;

      foreach my $childNodeNameLC (@childListNames)
      {
         my $childNodeName = $lcChildHash{$childNodeNameLC};
         $counter++;
         if ($counter > $maxPerColumn) 
         {
            $counter=0;
            if ($tdopen eq 1) 
            {
               print FILEOUT "</td>";
               $tdopen = 0;
            }
         }
         if ($childNodeName =~ m/^__/i) 
         {
         }
         else
         {
            my $type =  ref $NodeHashPtr;
            if ($type eq "HASH") 
            {
               my $childNodePtr = $NodeHashPtr->{$childNodeName};
               my $fname = $childNodePtr->{__PageFileName};
               if ($tdopen == 0) 
               {
                  print FILEOUT "<td>";
                  $tdopen=1;
               }
               print FILEOUT "<a href=\"$fname\">$childNodeName</a><br>";
            }
         }
      }
      if ($tdopen == 1) 
      {
         print FILEOUT "</td>";
      }
      print FILEOUT "</tr></table>\n";
      print FILEOUT boxClose();
   }





   print FILEOUT boxClose();
   print FILEOUT footer();
   print FILEOUT '</body></html>';
   close(FILEOUT);

   pop (@$parentLinkBackArrayPtr);

}

my $globalGraph=undef;

sub drawGraph($$$$$$) 
{
   my $masterHashPtr = shift;
   my $node2FileNameHashPtr = shift;
   my $title = shift;
   my $chartTitle = shift;
   my $chartXLabel = shift;
   my $chartYLabel = shift;

 #  print "TITLE FOR GRAPH IN ($title)\n";
   my $output = "\n\n\n";
   #return $output;

    

   my $checksum = hashChecksum($masterHashPtr);
   #return $output;
                                 
   my @triangleCountList = keys (%$masterHashPtr);
   my $triangleCount = @triangleCountList;
   if (($triangleCount>0))
   {
      if (1) 
      {
       #  print "Making Graph! with ($triangleCount) children\n";
         my $sortFlag = 0; # default, sort by value
         if ($chartTitle =~ m/strip length histogram/i) 
         {
            $sortFlag=1;   # no by key
         }
         $title =~ s/\./_/ig;
         my $filename = "$title"."-$checksum".".gif";
         #print "FILENAME ($filename) for GRAPH\n";
         #my $dir = buildSubDir($filename);
         #$filename = "$dir\\$filename";
        # print "FINAL GRPAH FILENAME ($filename)\n";
                              
         if (-e $filename) 
         {
            # graph already exists..
         }
         else
         {
            # build the graph
            my $graph = new rsGraph();
            $graph->setupDataHash($masterHashPtr, $sortFlag);
            $graph->setChartType_BarClustered();
            $graph->setChartTitle("$chartTitle");
            $graph->setupXAxis("$chartXLabel");
            $graph->setupYAxis("$chartYLabel");
            $graph->exportGif($filename);  
            $graph->quit();   
            $graph = undef;
         }

         $output .= "<table><tr><td>";
         $output .= "<img src=\"$filename\">";
         $output .= "</td><td>";

         # build top ten offender list
         my @keyList = sort {
            $masterHashPtr->{$b} <=> $masterHashPtr->{$a}
         } keys %$masterHashPtr;

         $output .= "<table><tr><td>";
         $output .= "<h1>Top 10</h1>";
         $output .= "</td></tr><tr><td>";


         $output .= "<table>";
         my $counter=0;
         $output .= "<tr><td> </td><td>$chartXLabel</td><td>$chartYLabel</td></tr>";
         foreach (@keyList)
         {
            $counter++;
            my $value = $masterHashPtr->{$_};
            my $filename = $node2FileNameHashPtr->{$_}; 
            if ($filename ne undef) 
            {
               $output .= "<tr bgcolor=#DDFFFF><td>$counter</td><td><a href=\"$filename\">$_</a></td><td>$value</td></tr>";
            }
            else
            {
               $output .= "<tr bgcolor=#DDFFFF><td>$counter</td><td>$_</td><td>$value</td></tr>";
            }

            if ($counter>=10) 
            {
               last;
            }
         }
         $output .= "</table>\n";

         my $filenamex = "$title"."_complete_list".".html";
     #    print "FILENAME FOR COMPLETE ($filenamex)\n";
        # my $dir = buildSubDir($filenamex);
        # $filenamex = "$dir\\$filenamex";
      #   print "FILENAME COMPLETE ($filenamex)\n";
         open (FILE, ">$filenamex");
         # build top 1000 list html file
         print FILE boxOpen("#000000","#FFFFFF");
         print FILE showTitle("Complete list of <b><i>$chartTitle</i></b>");
         print FILE boxOpen("#000000","#FFFFFF");
         print FILE "<table border=0 width=100".'%'.">";
         print FILE "<tr bgcolor=#DDDDFF><td> </td><td>$chartXLabel</td><td>$chartYLabel</td></tr>";
         my $counter=0;
         foreach my $finger (@keyList)
         {
            my $color = "#EEFFEE";
            if ($counter & 1) 
            {
               $color = "#DDFFDD";               
            }
            $counter++;
            my $value = $masterHashPtr->{$finger};
            my $filenamey = $node2FileNameHashPtr->{$finger}; 
            if ($filenamey ne undef) 
            {
               print FILE "<tr bgcolor=$color><td><b>$counter</b></td><td>$finger</td><td>$value</td></tr>";
            }
            else
            {
               print FILE "<tr bgcolor=$color><td><b>$counter</b></td><td>$finger</td><td>$value</td></tr>";

            }
         }
         print FILE "</table>";
         print FILE boxClose();
         print FILE boxClose();
         close (FILE);

         $output .= "<a href=\"$filenamex\">See Complete List</a><br>";
         $output .= "</td></tr></table>";
         $output .= "</td></tr></table>";
         $output .= "<hr>\n\n";
      }
   }
   return $output;
}
                     

sub processNodes($)  # pass in the node.. it will recurse down.. and process leaves
{
   my $NodeHashPtr = shift;

   drawWinger();
               
   if ($NodeHashPtr->{__Payload} ne undef) 
   {
      # we are at the leaf with data..
      my $hPtr = convert2Hash($NodeHashPtr->{__Payload});
      $NodeHashPtr->{__Info} = $hPtr;
      return $hPtr;  #// return the information hash;
   }
   
   my @childrenPtrList = keys (%$NodeHashPtr);

   my %parentInfoHash;
   foreach my $childNodeName (@childrenPtrList)
   {
      if ($childNodeName =~ m/^__Parent/i) #$childNodeName ne "Payload") 
      {
      }
      else
      {
         my $type =  ref $NodeHashPtr;
         if ($type eq "HASH") 
         {
            my $childNodePtr = $NodeHashPtr->{$childNodeName};
            if (ref $childNodePtr eq "HASH") 
            {
               my $infoHashPtr = processNodes($childNodePtr);
               %parentInfoHash = combineInfo(\%parentInfoHash, $infoHashPtr);
            }
            
         }
      }
   }
   $NodeHashPtr->{__Info} = \%parentInfoHash;
   return \%parentInfoHash;
}

sub combineInfo($$) # parent hash ptr, info hash ptr 
{
   my $parentHashPtr = shift;
   my $infoHashPtr   = shift;

   my @accumTitles = ( "TotalFullMappedQuads", "TotalNearMappedQuads", "TotalOverMappedQuads","TotalPck","TotalMissingPck","TotalAdj","TotalTex0", "TotalTex1","TriangleCount", "TotalPos", "TotalPosSkin","TotalCpv", "TotalMaterial", "TotalPrimitives");

   foreach (@accumTitles)
   {
      if ($infoHashPtr->{$_} ne undef) 
      {
         if ($parentHashPtr->{$_} eq undef) 
         {
            $parentHashPtr->{$_} = 0;
         }
        # print "COMBINE ($_)\n";
         $parentHashPtr->{$_} += $infoHashPtr->{$_};
      }
   }

   my @simpleStringCombiners = ("MaterialNameList", "error", "warning", "status");
   foreach (@simpleStringCombiners)
   {
      if ($infoHashPtr->{$_} ne undef) 
      {
         # we gound a material names list
         if ($parentHashPtr->{$_} eq undef) 
         {
            $parentHashPtr->{$_} = "";
         }
         my $info = $infoHashPtr->{$_}; # read in the material list
         $parentHashPtr->{$_} .= " $info ";
         $info = $parentHashPtr->{$_};
         $info =~ s/^\s+//ig;
         $info =~ s/\s+$//ig;
         $info =~ s/\s+/ /ig;
         my @array = split(/\s+/, $info);
         my %hash;
         foreach (@array)
         {
            $hash{$_} = 1;
         }
         my @array = sort( keys (%hash));
         $info = join(' ', @array);
         $parentHashPtr->{$_} = $info;
         # kill duplicates
      }
   }

   my @stringCombiners = ("StripHistogram", "PrimTypeHistogram", "MaterialHistogram");
   # string combiners
   foreach (@stringCombiners)
   {
      if ($infoHashPtr->{$_} ne undef) 
      {
         if ($parentHashPtr->{$_} eq undef) 
         {
            $parentHashPtr->{$_} = "";
         }
         my $info = $infoHashPtr->{$_};
         $parentHashPtr->{$_} .= " $info ";
         my $out = $parentHashPtr->{$_};
         $out =~ s/^\s+//ig;
         $out =~ s/\s+$//ig;
         $out =~ s/\s+/ /ig;

         my @list = split(/\s+/, $out);
         my %histoHash;

         foreach (@list)
         {
            m/\[(.+?):(\d+)\]/i;
            my $stripLen = $1;
            my $totalStrips = $2;
            if ($histoHash{$stripLen} eq undef) 
            {
               $histoHash{$stripLen} = 0;
            }
            $histoHash{ $stripLen} += $totalStrips;
         }
         my @list = keys %histoHash;
         $out ="";
         foreach (@list)
         {
            my $value = $histoHash{$_};
            $out .=" [$_:$value] ";
         }
         $out =~ s/^\s+//ig;
         $out =~ s/\s+$//ig;
         $out =~ s/\s+/ /ig;

         $parentHashPtr->{$_} = $out;
      }
   }

   return %$parentHashPtr;
}

sub convert2Hash($) # converts the excel line to a hash 
{
   my $line = shift;
   chomp($line);
   my @array = split(/,/,$line);
   my $count = @array;
   my %hash;
   my $counter;
   foreach my $data (@array)
   {
      my $title = $titleList[$counter];
      if ($title eq undef) 
      {
         $title = "UNKNOWN";
      }
      $hash{$title} = $data;
      $counter++;
   }
   return \%hash;
}

sub boxOpen($$)
{
   my $color1 = shift;
   my $color2 = shift;

   my $out;
   $out .= "\n<table bgcolor=$color1 border=0 cellspacing=0 cellpadding=0 width=100".'%'."><tr><td>";
   $out .= "<table cellpadding=3 cellspacing=1 border=0 width=100".'%'."><tr><td bgcolor=$color2>";
   $out .= "<font face=\"Arial\">";
   return $out;
}
sub boxClose() 
{
   my $out;
   $out .= "</font>";
   $out .= "</td></tr></table>";
   $out .= "</td></tr></table>\n";
   return $out;
}


sub getTime() 
{
   my $timeInSecs = time();
   # Construct the time stamp
   my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = 
           localtime($timeInSecs);


   my ($MonthStr) =
             qw (Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)[$mon];

   $year += 1900;

   my $timestamp = sprintf( "%02d-%s-%d %02d:%02d:%02d",
           $mday, $MonthStr, $year, $hour, $min, $sec );
   return $timestamp;
}

sub showTitle($) 
{
   my $title = shift;
   my $out;
   $out .= boxOpen("#FFFFFF", "#FFFFFF");
   $out .= "<font size=5 face=\"Arial\">$title</font>";
   $out .= "</td><td align=right>";
   my $dateCreated =  getTime() ;
   $out .= "Date Created : <i>$dateCreated</i>";
   $out .= boxClose();           
   return $out;
}

sub footer() 
{
   my $out;
   $out .= boxOpen("#FFFFFF", "#FFFFFF");
   $out .= "<center>";
   $out .= "&copy;2004 Rockstar<br>";
   $out .= "Please send all requests, bugs, suggestions regarding this report to<br><b>robert_suh\@RockstarSanDiego.com";

   $out .= "</center>";

   $out .= boxClose();
}


use String::CRC32;
sub hashChecksum($) 
{
   my $hashPtr = shift;

   my $out;
   my @keyList = keys (%$hashPtr);
   my @valuelist = values( %$hashPtr);
   $out = "@keyList @valuelist";
   my $crc = crc32($out);
   return $crc;
}

sub getPerlScript() 
{
   my $ss = '
<SCRIPT LANGUAGE="PerlScript">
sub openTristripViewer($) 
{
   my $filename = shift;
   if (-e $filename) 
   {
      my $cmd = \'\\\\\\\\Zen\\projects\\age\\tools\\rsTools\\viewTristrips.exe \'.$filename;
     # $window->document->write($cmd);
      system($cmd);
   }
   else
   {
      $window->document->write("ERROR : File ($filename) is not found!");
   }
}
sub systemStart($) 
{
   my $input = shift;
   system("start $input");
}
</SCRIPT>
';
   return $ss;
}

sub drawWinger() 
{
   $wingerCounter++;
   my $winger = $winger[($wingerCounter>>4)&3];
   print "\r($winger)";
}











