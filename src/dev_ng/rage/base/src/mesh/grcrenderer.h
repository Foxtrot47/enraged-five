//
// mesh/grcrenderer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef MESH_GRCRENDERER_H
#define MESH_GRCRENDERER_H

#include "mesh/mesh.h"
#include "grcore/im.h"


namespace rage {

static grcDrawMode msh2grc_dm[] = { drawPoints, drawLines, drawTris, drawTriStrip, drawTriStrip, drawTriStrip, drawTriFan, drawTriAdj };
CompileTimeAssert(NELEM(msh2grc_dm) == drawModesTotal);
/*
	PURPOSE:
		Implement mesh rendering using grcore immediate mode API.
	<FLAG Component>
*/

#if MESH_LIBRARY

class mshRendererGrc : public mshRenderer {
	void Material(const mshArray<mshAttribute> &) const { }
	// Begin a primitive (like glBegin)
	void Begin(mshPrimType type,int count) const {
		grcBegin(msh2grc_dm[(int)type],count);
	}
	// Define a new current normal (like glNormal3f)
	void Normal(float x,float y,float z) const { grcNormal3f(x,y,z); }
	// Define a new current vertex color (like glColor4f)
	void Color(float r,float g,float b,float a) const { grcColor4f(r,g,b,a); }
	// Define a new current base texture coordinate (like glTexCoord2f)
	void TexCoord0(float s,float t) const { grcTexCoord2f(s,t); }
	// Define a new current second-pass texture coordinate
	void TexCoord1(float,float) const { }
	// Define a new vertex and sends it to renderer (like glVertex3f)
	void Vertex(float x,float y,float z) const { grcVertex3f(x,y,z); }
	// End a primitive (like glEnd)
	void End() const { grcEnd(); }
};

#endif // MESH_LIBRARY

}	// namespace rage

#endif // MESH_GRCRENDERER_H
