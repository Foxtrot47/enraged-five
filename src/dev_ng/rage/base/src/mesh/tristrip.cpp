//
// mesh/tristrip.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mesh.h"
#include "serialize.h"

#include "core/output.h"
#include "data/assetcfg.h"


int main(int argc,char **argv) {
	rage_new datAssetManagerFlat;

	for (int i=1; i<argc; i++) {
		mshMesh MESH;
		if (SerializeFromFile(argv[i],MESH,"mesh")) {
			Displayf("Tristripping '%s'...",argv[i]);
			MESH.Tristrip();
			SerializeToAsciiFile(argv[i],MESH);
		}
		else
			Errorf("Unable to load '%s'",argv[i]);
	}
}
