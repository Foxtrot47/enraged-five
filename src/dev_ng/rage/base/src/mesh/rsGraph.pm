use strict;

package rsGraph;  # simple GIF Graph generator

use Win32::OLE qw( with in );
use Win32::OLE::Const "Microsoft Graph";


sub new() 
{
   my $packageName   = shift;
   my $this = {};       # allocate a hash table to hold class variables
   #$this->{primaryKey} = "ID";      # Primary Key FIELD Name
   
   my %ChartOptions = (
        width  =>  640,
	height  =>  600,
	haslegend  =>  0,  # no legend
	type  =>  xlBarClustered, #xl3DLine,
	perspective  =>  30,
	rotation  =>  20,
	autoscaling  =>  1,
	rightangleaxes  =>  1,
	title  =>  "Your Title Goes Here!",
   );
   
   my $ChartApp = new Win32::OLE( "MSGraph.Application", "Quit" ) ||
	die "Cannot create object\n";
   $ChartApp->{Visible} = 0; #dont show the app when its launching
   my $DataSheet = $ChartApp->DataSheet();
   my $Chart = $ChartApp->Chart();
   foreach my $Option ( keys( %ChartOptions ) )
   {
	$Chart->{$Option} = $ChartOptions{$Option};
   }
   $this->{ChartApp}  = $ChartApp;
   $this->{DataSheet} = $DataSheet;
   $this->{Chart}      = $Chart;

   bless $this;
   return $this;
}

sub setChartTitle($) 
{
   my $this = shift;
   my $title = shift;

   my $Chart = $this->{Chart};

   $Chart->{HasTitle} = 1;
   $Chart->{ChartTitle}->{Text} = $title;
   $Chart->{ChartTitle}->{Font}->{Name} = "Tahoma";
   $Chart->{ChartTitle}->{Font}->{Size} = 18;
   $Chart->{ChartTitle}->{Left} = 0;


}

#Bar xlBarClustered 
#Stacked Bar xlBarStacked 
#Line xlLine 
#3d Line xl3DLine 
#Area xlAreaStacked 
#3D Area xl3DAreaStacked 
#Column xColumnClustered 
#3D Column xl3DColumn 
#Pie xlPie 
#3D Pie xl3DPie 
sub setChartType_ColumnClustered() 
{
   my $this = shift;
   $this->setChartType(xlColumnClustered);
}

sub setChartType_3DPie() 
{
   my $this = shift;
   $this->setChartType(xl3DPie);
}

sub setChartType_Pie() 
{
   my $this = shift;
   $this->setChartType(xlPie);
}


sub setChartType_3DColumn() 
{
   my $this = shift;
   $this->setChartType(xl3DColumn);
}



sub setChartType_3DAreaStacked() 
{
   my $this = shift;
   $this->setChartType(xl3DAreaStacked);
}

sub setChartType_BarClustered() 
{
   my $this = shift;
   $this->setChartType(xlBarClustered);
}
sub setChartType_BarStacked() 
{
   my $this = shift;
   $this->setChartType(xlBarStacked);
}
sub setChartType_Line() 
{
   my $this = shift;
   $this->setChartType(xlLine);
}
sub setChartType_3Dline() 
{
   my $this = shift;
   $this->setChartType(xl3DLine);
}
sub setChartType_AreaStacked() 
{
   my $this = shift;
   $this->setChartType(xlAreaStacked);
}




sub setChartType($) 
{
   my $this = shift;
   my $chartType = shift;
   my $Chart = $this->{Chart};
   $Chart->{type} = $chartType;
}




sub setupDataHash($$)  # pass in a hash ptr
{
   my $this = shift;
   my $hashPtr = shift;
   my $sortFlag = shift;
   my $DataSheet = $this->{DataSheet};
   my @keyList =  keys (%$hashPtr);
   my( @CELLS ) = ( 'a'..'zzz' );

   if ($sortFlag eq 0) # sort by hash value
   {
      @keyList = sort {
                  $hashPtr->{$b} <=> $hashPtr->{$a}
      } keys %$hashPtr;
   }
   else
   {
      # sort by NUMERIC 
      @keyList = sort {
                  $a <=> $b
      } keys %$hashPtr;
   }


   my $counter=0;
   foreach my $keyname(@keyList)
   {
      $DataSheet->Range("$CELLS[$counter]0")->{Value} = $keyname;
      $DataSheet->Range("$CELLS[$counter]1")->{Value} = $hashPtr->{$keyname};
      $counter++;      
   }
}

sub setupYAxis($) 
{
   my $this = shift;
   my $label = shift;
   my $Chart = $this->{Chart};
   # Configure the Y axis.
   if( my $Axis = $Chart->Axes( xlValue ) )
   {
       # print Dumper $Axis;
	$Axis->{HasMajorGridlines} = 1;
        $Axis->{HasTitle} = 1;            
        with( $Axis->{AxisTitle}->{Font},
               Name => "Tahoma",
               Bold => 1,
               Size => 10,
               Italic => 1);
        $Axis->{AxisTitle}->{Caption} = "$label";
        $Axis->{AxisTitle}->{Orientation} = 90 ; #xlVertical;



	$Axis->{MajorGridlines}->{Border}->{Weight} = 1;
	$Axis->{MajorGridlines}->{Border}->{ColorIndex} = 48;
	$Axis->{MajorGridlines}->{Border}->{LineStyle} = xlContinuous;
	with( $Chart->Axes( xlValue )->{TickLabels}->{Font},
		Name  =>  "Tahoma",
		Bold  =>  0,    
                Size  => 10,
		Italic  =>  0
	);
   }

}

sub cleanup() 
{
   my $this = shift;
   return;
   my $Chart = $this->{Chart};
   my $iIndex=0;
   my $iTotal = $Chart->SeriesCollection( 1 )->Points()->{Count};
   my $PrevText  = "";
   foreach my $Point (in( $Chart->SeriesCollection( 1 )->Points()))
   {
	my $Percent = int( ++$iIndex * 100 / $iTotal );
	my $Text = $Point->{DataLabel}->{Text};
	$Point->{MarkerStyle} = xlMarkerStyleDot;
	$Point->{DataLabel}->{Font}->{Background} = xlBackgroundOpaque;
	$Point->{DataLabel}->{Top} -= 12;
	$Point->{HasDataLabel} = 0 if( $Text eq $PrevText );
	$PrevText = $Text;
	print "\rFormatting: $Percent%";
   }
   print "\n";
}

sub setupXAxis($) 
{
   my $this = shift;
   my $label = shift;

   my $Chart = $this->{Chart};

   #print "inside ($label)\n";
   if( my $Axis = $Chart->Axes( xlCategory ) )
   {
      #print "asdasdd\n";
      $Axis->{HasTitle} = 1;            
      with( $Axis->{AxisTitle}->{Font},
           Name => "Tahoma",
           Bold => 1,
           Size=>10,
           Italic => 1);
        $Axis->{AxisTitle}->{Caption} = "$label";

	$Axis->{HasMajorGridlines} = 0;
        $Axis->{AxisTitle} = "My Title";
	$Axis->{TickLabels}->{orientation} = xlUpward;
	with( $Axis->{TickLabels}->{Font},
		Name  =>  "Tahoma",
		Bold  =>  0,
                Size => 10,
		Italic  =>  0
   	);
   }

}

sub exportGif($) 
{
   my $this = shift;
   my $filename = shift;
   $this->cleanup();
   my $Chart = $this->{Chart};
   print "Writing out Chart Gif ($filename)\n";
   $Chart->Export( $filename, "GIF", 0 );
}

sub quit() 
{
   my $this = shift;
   my $Application = $this->{ChartApp};
   $Application->Quit();
}







1;
