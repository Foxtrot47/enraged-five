//
// mesh/attribute.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "attribute.h"
#include "serialize.h"

#include <stdio.h>

using namespace rage;

mshAttribute* mshAttribute::Set(const char *name,float value) {
	char buffer[32];
	sprintf(buffer,"%f",value);
	Name = name;
	Value = buffer;
	return this;
}


void mshAttribute::Serialize(mshSerializer &S) {
	S.BeginBlock();
	SERIALIZE(S,Name);
	SERIALIZE(S,Value);
	S.EndBlock();
}

void Serialize(mshSerializer &S,mshAttribute &v) {
	v.Serialize(S);
}


