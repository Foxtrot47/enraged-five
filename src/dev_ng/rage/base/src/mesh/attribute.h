//
// mesh/attribute.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef MESH_ATTRIBUTE_H
#define MESH_ATTRIBUTE_H

#include "atl/array.h"
#include "string/string.h"

namespace rage {

struct mshSerializer;

// A simple name and value pair used for storing generic shader and node attributes
class mshAttribute {
protected:
	void Copy(const mshAttribute &that) {
		Name = that.Name;
		Value = StringDuplicate(that.Value.m_String);
	}
	void Kill() {
		StringFree(Value.m_String);
	}
	
public:
	mshAttribute(const mshAttribute &that) { Copy(that); }
	mshAttribute() : Value(0) { }

	mshAttribute* Set(const char *name,float value); 

	mshAttribute* Set(const char *name,const char *value) {
		Name = name;
		Value = value;
		return this;
	}

	const char *GetName() const { return Name; }

	const char *GetValue() { return Value; }

	void Serialize(mshSerializer &S);

	mshAttribute& operator=(const mshAttribute &that) {
		Kill();
		Copy(that);
		return *this;
	}

protected:
	ConstString Name;
	ConstString Value;
};

void Serialize(mshSerializer &S,mshAttribute &v);

}	// namespace rage

#endif
