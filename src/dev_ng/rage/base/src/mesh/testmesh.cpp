//
// mesh/testmesh.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#define CHECKLEAKS

#include "mesh.h"
#include "bsp.h"
#include "serialize.h"
#include "vglrenderer.h"

#include "gfx/simple.h"
#include "core/ipc.h"
#include "data/args.h"
#include "data/main.h"
#include "devcam/polarcam.h"

#if __BANK
#include "bank/bank.h"
#include "bank/bkmgr.h"
#endif

int Main() {
	ageInit("//zen/projects/age/assets/gfx");

	ageBeginGfx();

	devPolarCam PC;
	PC.Init(10);

	bool drawOrig = false, drawNew = false, drawBsp = true, drawNormals = false;
	int bspLevel = 0;
#if __BANK
	PIPE.AddWidgets(BANKMGR.CreateBank("Pipe"));
	bkBank &B = BANKMGR.CreateBank("Test");
	B.AddToggle("Draw Orig",&drawOrig);
	B.AddToggle("Draw New",&drawNew);
	B.AddToggle("Draw BSP",&drawBsp);
	B.AddSlider("BSP Draw Level",&bspLevel,0,1000,1);
	B.AddToggle("Draw Normals",&drawNormals);
#endif

//	mshNode SCENE;
	mshMesh MESH;

	if (!MESH.LoadMod("//zen/projects/age/assets/gfx/largesphere"))
	// if (!MESH.LoadMod("//zoo/projects/rb/assets/current/levels/konoko_gym/konoko_gym_konoko_gym"))
	// if (!SCENE.LoadTree("c:/rb/art/char/bug/anim/bug_anim_ref.mesh"))
		return 0;

	
#if 0
	{
		datBinTokenizer T;
		SafeStream stream(Stream::Open("c:/test.mesh"));
		T.Init("test",stream);
		mshSerializer S(T,false);
		MESH.Serialize(S);
	}
#endif

	MESH.CleanModel();

	MESH.Triangulate();

	mshBspNode BSP;
	BSP.FromMesh(MESH);

	MESH.CleanModel();

	unsigned ms = ipcTime();
	const mshBspNode *ROOT = BSP.MakeTree(0.01f);
	ms = ipcTime() - ms;
	Displayf("%d ms for BSP generation",ms);

	mshMesh MESH2;
	ROOT->ToMesh(MESH2,MESH);

	{
		datAsciiTokenizer T;
		SafeStream stream(Stream::Create("c:/testout.mesh"));
		T.Init("test",stream);
		mshSerializer S(T,true);
		MESH.Serialize(S);
	}

	MESH.SaveMod("c:/testout.mod",false,84,0);

	mshRenderer &R = *rage_new mshRendererVgl;

	do {
		ageBeginFrame();
		PC.Update();
		RSTATE.SetCamera(PC.GetWorldMatrix());
		RSTATE.SetIdentity();

		if (drawOrig)
			MESH.Draw(R);
		if (drawNew)
			MESH2.Draw(R);
		if (drawNormals)
			MESH.DrawNormals(R);
		if (drawBsp)
			ROOT->Draw(Vector4(1,1,1,1),R,bspLevel);

		ageEndFrame();
#if __PS2 && defined(CHECKLEAKS)
		break;
#endif
	} while (!ageExit());

	delete &R;
	
	delete ROOT;

	ageEndGfx();

	return 0;
}
