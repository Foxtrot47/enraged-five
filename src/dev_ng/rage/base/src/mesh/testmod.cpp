//
// mesh/testmod.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "data/assetcfg.h"

int main(int argc,const char **argv) {
	if (argc > 2) {
		rage_new datAssetManagerFlat;

		mshMesh MESH, MESH2;
		if (!MESH.LoadMod(argv[1]))
			return 1;
		SerializeToAsciiFile("c:\\test.mesh",MESH);
		SerializeFromFile("c:\\test.mesh",MESH2);
		SerializeToAsciiFile("c:\\test2.mesh",MESH2);
		return !MESH.SaveMod(argv[2],false,84,0,false);
	}
	else
		return 1;
}
