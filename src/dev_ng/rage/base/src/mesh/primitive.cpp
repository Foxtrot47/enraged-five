//
// mesh/primitive.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mesh.h"
#include "serialize.h"
#include "xbstrip.h"

#if MESH_LIBRARY

namespace rage {

/*
	PURPOSE
		Draws a single primitive
	PARAMS
		M - Mesh containing this primitive
		R - Renderer object
	NOTES
		Does no state management.  Certain primitives consist of more than one triangle. 
*/
void mshPrimitive::Draw(const mshMaterial &M,const mshRenderer &R) const {
	if (R.Wireframe && Type == mshTRIANGLES) {
		R.Color(Vector4(1,1,1,1));
		for (int i=0; i<Idx.GetCount()-2; i+=3) {
			R.Begin(mshLINES,6);
			R.Vertex(M.GetVertex(Idx[i+0]).Pos);
			R.Vertex(M.GetVertex(Idx[i+1]).Pos);
			R.Vertex(M.GetVertex(Idx[i+1]).Pos);
			R.Vertex(M.GetVertex(Idx[i+2]).Pos);
			R.Vertex(M.GetVertex(Idx[i+2]).Pos);
			R.Vertex(M.GetVertex(Idx[i+0]).Pos);
			R.End();

		}
		for (int i=0; i<M.GetVertexCount(); i++) {
			R.Color(Vector4(1,0,0,1));
			R.Begin(mshPOINTS,1);
			R.Vertex(M.GetVertex(i).Pos);
			R.End();
		}
	}
	else {
		R.Begin(Type, Idx.GetCount());
		for (int i=0; i<Idx.GetCount(); i++)  {
			const mshVertex &A = M.GetVertex(Idx[i]);
			R.Normal(A.Nrm);
			R.Color(A.Cpv);
			if (A.TexCount)
				R.TexCoord0(A.Tex[0]);
			if (A.TexCount > 1)
				R.TexCoord1(A.Tex[1]);
			R.Vertex(A.Pos);
		}
		R.End();
	}
}


/*
	PURPOSE
		Draws a single primitive
	PARAMS
		M - Mesh containing this primitive
		R - Renderer object
		matrices - skinning matrices
	NOTES
		Does no state management.  Certain primitives consist of more than one triangle. 
*/
void mshPrimitive::DrawSkinned(const mshMaterial &M,const mshRenderer &R,const Matrix34 *matrices) const {
	R.Begin(Type, Idx.GetCount());
	for (int i=0; i<Idx.GetCount(); i++)  {
		const mshVertex &A = M.GetVertex(Idx[i]);
		R.Normal(A.Nrm);
		R.Color(A.Cpv);
		if (A.TexCount)
			R.TexCoord0(A.Tex[0]);
		if (A.TexCount > 1)
			R.TexCoord1(A.Tex[1]);

		Vector3 Final(0,0,0);
		Vector3 V;
		if( A.Binding.IsPassThrough )
		{
			V.Dot(A.Pos,matrices[0]);
			Final.Add(V);
		}
		else
		{
			for (int i=0; i<mshMaxMatricesPerVertex; i++) {
				Vector3 V;
				V.Dot(A.Pos,matrices[A.Binding.Mtx[i]]);
				Final.AddScaled(V,A.Binding.Wgt[i]);
			}
		}
		R.Vertex(Final);
	}
	R.End();
}


/*
	PURPOSE
		Renders all normals associated with a primitive
	PARAMS
		M - Mesh containing this primitive
		R - Renderer object
		R - Renderer object 
		c - Color to use
		length - length of the vector 
		shaderCompute - compute the binormal in a method similar to what rage shaders do
*/
void mshPrimitive::DrawNormals(const mshMaterial &M,const mshRenderer &R, Color32 &c, float length) const {
	Vector4 startColor(c.GetRedf(), c.GetGreenf(), c.GetBluef(), c.GetAlphaf());
	Vector4 endColor(startColor);
	startColor.Scale(0.3f);
	startColor.w = endColor.w;
	
	Vector3 offset;
	for (int i=0; i<Idx.GetCount(); i++) {
		const mshVertex &A = M.GetVertex(Idx[i]);
		R.Begin(mshLINES,2);
		R.Color(startColor);
		R.Vertex(A.Pos);
		R.Color(endColor);
		offset.Scale(A.Nrm, length);
		R.Vertex(A.Pos + offset);
		R.End();
	}
}

/*
	PURPOSE
		Renders all tangents associated with a primitive
	PARAMS
		M - Mesh containing this primitive
		R - Renderer object
		R - Renderer object 
		c - Color to use
		length - length of the vector 
*/
void mshPrimitive::DrawTangents(const mshMaterial &M,const mshRenderer &R, Color32 &c, float length) const {
	Vector4 startColor(c.GetRedf(), c.GetGreenf(), c.GetBluef(), c.GetAlphaf());
	Vector4 endColor(startColor);
	startColor.Scale(0.3f);
	startColor.w = endColor.w;
	
	Vector3 offset;
	for (int i=0; i<Idx.GetCount(); i++) {
		const mshVertex &A = M.GetVertex(Idx[i]);
		for (int j = 0; j < A.TanBiCount; ++j) {
			R.Begin(mshLINES,2);
			R.Color(startColor);
			R.Vertex(A.Pos);
			R.Color(endColor);
			offset.Scale(A.TanBi[j].T, length);
			R.Vertex(A.Pos + offset);
			R.End();
		}
	}
}

/*
	PURPOSE
		Renders all binormals associated with a primitive
	PARAMS
		M - Mesh containing this primitive
		R - Renderer object
		R - Renderer object 
		c - Color to use
		length - length of the vector 
		shaderCompute - compute the binormal in a method similar to what rage shaders do
*/
void mshPrimitive::DrawBinormals(const mshMaterial &M,const mshRenderer &R, Color32 &c, float length, bool shaderCompute) const {
	Vector4 startColor(c.GetRedf(), c.GetGreenf(), c.GetBluef(), c.GetAlphaf());
	Vector4 endColor(startColor);
	startColor.Scale(0.3f);
	startColor.w = endColor.w;
	
	Vector3 offset;
	for (int i=0; i<Idx.GetCount(); i++) {
		const mshVertex &A = M.GetVertex(Idx[i]);
		for (int j = 0; j < A.TanBiCount; ++j) {
			R.Begin(mshLINES,2);
			R.Color(startColor);
			R.Vertex(A.Pos);
			R.Color(endColor);
			if ( shaderCompute ) {
				offset.Cross(A.TanBi[j].T, A.Nrm);
				offset.Normalize();
				// Grab the dominant term & use it for sign comparison
				Vector3 txnAbs;
				txnAbs.Abs(offset);
				Vector3 bi(A.TanBi[j].B);
				float testA, testB;
				if ( txnAbs.x > txnAbs.y ) {
					if ( txnAbs.x > txnAbs.z ) {
						testA = offset.x;
						testB = bi.x;
					}
					else {
						testA = offset.z;
						testB = bi.z;
					}
				}
				else if ( txnAbs.y > txnAbs.z ) {
					testA = offset.y;
					testB = bi.y;
				}
				else {
					testA = offset.z;
					testB = bi.z;
				}
				// Finally, do the comparison
				if ( (testA >= 0.f && testB >= 0) || (testA < 0.f && testB < 0.f) ) {
					offset.Scale(length);
				}
				else {
					offset.Scale(-length);
				}
			}
			else {
				offset.Scale(A.TanBi[j].B, length);
			}
			R.Vertex(A.Pos + offset);
			R.End();
		}
	}
}
							   


/*
	PURPOSE
		Remove degenerate primitives based on index.
	PARAMS
		M - Mesh containing this primitive
		output - Output primitive, always valid.
	NOTES
		In order for this function to be most effective, you need to run
		a WeldPos first because testing is done based on exact position,
		output may end up being an empty primitive
		on return even if the function succeeded (all primitives may be
		degenerate)
*/
void mshPrimitive::RemoveDegenerates(const mshMaterial &M,mshPrimitive &output) const {
	output.Type = Type;
	output.Priority = Priority;
	output.Idx.Reset(Idx.GetCount());
	if (Type == mshPOINTS) {
		output.Idx = Idx;
	}
	else if (Type == mshLINES) {
		for (int i=0; i<Idx.GetCount()-2; i+=2)
			if (M.GetPos(Idx[i]) != M.GetPos(Idx[i+1]))
				output.Idx.Append2(Idx[i],Idx[i+1]);
	}
	else if (Type == mshTRISTRIP || Type == mshTRISTRIP2) {
		// Interesting dilemma -- degenerates are okay in tristrips in some situations?
		output.Idx = Idx;
	}
	else if (Type == mshTRIANGLES) {
		for (int i=0; i<Idx.GetCount()-2; i+=3) {
			const Vector3 &p0 = M.GetPos(Idx[i+0]);
			const Vector3 &p1 = M.GetPos(Idx[i+1]);
			const Vector3 &p2 = M.GetPos(Idx[i+2]);
			if (p0 != p1 && p0 != p2 && p1 != p2) {
				Vector4 plane;
				plane.ComputePlane(p0,p1,p2);
				if (plane.Mag2())
					output.Idx.Append3(Idx[i+0],Idx[i+1],Idx[i+2]);
			}
		}
	}
	else if (Type == mshQUADS) {
		Assert(0);
#if 0
		// Not the fastest way to do this, but catches everything
		mshPrimitive temp, temp2;
		AssertVerify(Triangulate(M,temp));
		temp.RemoveDegenerates(M,temp2);
		AssertVerify(temp2.Quadrify(M,output));
#endif
	}
	else if (Type == mshPOLYGON) {
		int i;
		for (i=0; i<Idx.GetCount()-1; i++)
			if (M.GetPos(Idx[i]) != M.GetPos(Idx[i+1]))
				output.Idx.Append() = Idx[i];
		// output last vertex unless it was the same as the first
		if (output.Idx.GetCount() && output.Idx[0] != Idx[i])
			output.Idx.Append() = Idx[i];
		// If we didn't get enough for a complete poly, nuke it.
		if (output.Idx.GetCount() < 3)
			output.Idx.Reset();
	}
}


static void TriangulateQuad(const mshMaterial &mtl,mshPrimitive &output,mshIndex i0,mshIndex i1,mshIndex i2,mshIndex i3,const Vector3 &boxMin) {
	// TODO: Check planarity first
	// Inputs are in trifan, not tristrip, order.
	float s0 = mtl.GetPos(i0).Dist2(boxMin);
	float s1 = mtl.GetPos(i1).Dist2(boxMin);
	float s2 = mtl.GetPos(i2).Dist2(boxMin);
	float s3 = mtl.GetPos(i3).Dist2(boxMin);

	// 0 +----------+ 1
	//   |          |
	//   |          |
	//   |          |
	//   |          |
	// 3 +----------+ 2
	// s0 closest, make it not on the shared edge
	if (s0 <= s1 && s0 <= s2 && s0 <= s3) {
		output.Idx.Append3(i0,i1,i3);
		output.Idx.Append3(i1,i2,i3);
	}
	// s1 closest, make it not on the shared edge
	else if (s1 <= s0 && s1 <= s2 && s1 <= s3) {
		output.Idx.Append3(i1,i2,i0);
		output.Idx.Append3(i2,i3,i0);
	}
	// s2 closest, make it not on the shared edge
	else if (s2 <= s0 && s2 <= s1 && s2 <= s3) {
		output.Idx.Append3(i2,i3,i1);
		output.Idx.Append3(i3,i0,i1);
	}
	// s3 closest
	else {
		output.Idx.Append3(i3,i0,i2);
		output.Idx.Append3(i0,i1,i2);
	}
}


/* 
	PURPOSE
		Converts current primitive into a triangle mesh, if possible
	PARAMS
		M - Mesh object containing this primitive (not currently used)
		output - Output primitive.  Triangles are appended to this.
	RETURNS
		True if conversion was successful (output is valid), or else
		false if conversion failed
	NOTES
		Winding order is always preserved.
*/
bool mshPrimitive::Triangulate(const mshMaterial &mtl,mshPrimitive &output,const Vector3 &boxMin) const {
	output.Type = mshTRIANGLES;
	output.Priority = Priority;

	if (Type == mshTRIANGLES) {
		for (int i=0; i<Idx.GetCount(); i++)
			output.Idx.Append() = Idx[i];
		return true;
	}
	else if (Type == mshTRISTRIP || Type == mshTRISTRIP2) {
		bool winding = (Type == mshTRISTRIP2);
		for (int i=2; i<Idx.GetCount(); i++) {
			if (winding)
				output.Idx.Append2(Idx[i-1],Idx[i-2]);
			else
				output.Idx.Append2(Idx[i-2],Idx[i-1]);
			output.Idx.Append() = Idx[i];
			winding = !winding;
		}
		return true;
	}
	else if (Type == mshQUADS) {
		Assert(0);
		for (int i=0; i<Idx.GetCount(); i+=4) {
			if (Idx[i+3])
				TriangulateQuad(mtl,output,Idx[i],Idx[i+1],Idx[i+3],Idx[i+2],boxMin);
			else
				output.Idx.Append3(Idx[i],Idx[i+1],Idx[i+2]);
		}
		return true;
	}
	else if (Type == mshPOLYGON) {
		if (Idx.GetCount() == 4)
			TriangulateQuad(mtl,output,Idx[0],Idx[1],Idx[2],Idx[3],boxMin);
		else {
			for (int i=2; i<Idx.GetCount(); i++)
				output.Idx.Append3(Idx[0],Idx[i-1],Idx[i]);
		}
		return true;
	}
	else
		return false;
}


/*
	PURPOSE
		Convert primitive into quad list, if possible
	PARAMS
		M - Mesh object containing this primitive
		output - Output primitive.  Primitives are appended to this.
	RETURNS
		True if primitive could be converted to quads, else false
	NOTES
		Unless primitive is already QUADS, it will internally attempt
		to triangulate the primitive first.
*/
#if 0
bool mshPrimitive::Quadrify(const mshMaterial &mtl,mshPrimitive &output) const {
	output.Type = mshQUADS;
	output.Priority = Priority;

	if (Type == mshQUADS) {
		output = *this;
		return true;
	}

	mshPrimitive temp;
	if (Triangulate(mtl,temp)) {
		// Build edge connectivity list
		// For each edge, determine angle between the two triangles to see if that edge can be a quad
		// Elimate any candidates that are collinear by angle
		// Sort candidates by number shared edges
		// +--+--+ so that we don't accidentally make a quad
		// |\ |\ | out of the "middle" two triangles (leaving two
		// | \| \| tris behind, for three quads total) when we could
		// +--+--+ have just made two quads.
		Assert(0 && "NIY!");
		return true;
	}
	return false;
}
#endif


/*
	PURPOSE
		Splits a single tristrip primitive into two tristrips, where the first one has
		exactly count vertices.
	PARAMS
		first - First primitive to output; will contain at most count vertices
		second - Second primitive to output, contains whatever didn't make it into first
		count - Max number of vertices in first primitive
	RETURNS
		True on success (and first and second are valid), else false on failure.
	NOTES
		Function will fail if primitive is not a tristrip with at least four vertices in it,
		or count is equal to the number of vertices already in the tristrip.  If count is
		odd, second tristrip will be of opposite winding type of first one.
*/
bool mshPrimitive::Truncate(mshPrimitive &first,mshPrimitive &second,int count) const {
	if (Type != mshTRISTRIP && Type != mshTRISTRIP2)
		return false;
	if (count >= Idx.GetCount())
		return false;
	first.Type = Type;
	first.Priority = Priority;
	first.Idx.Reset(count);
	int i;
	for (i=0; i<count; i++)
		first.Idx.Append() = Idx[i];
	second.Type = (count & 1)? mshFlipWinding(Type) : Type;
	second.Priority = Priority;
	second.Idx.Reset(Idx.GetCount()-count+2);
	for (i=count-2; i<Idx.GetCount(); i++)
		second.Idx.Append() = Idx[i];
	return true;
}



/*
	PURPOSE
		Generate tristrips from specified primitive.
	PARAMS
		M - Mesh containing this primitive
		output - Array of primitives to output.  This list is appended to by each call.
			All primitives output will be either TRISTRIP or TRISTRIP2.
	RETURNS
		True if conversion succeeded, or false if current primitive was not TRIANGLES
*/
bool mshPrimitive::Tristrip(const mshMaterial &mtl,mshArray<mshPrimitive> &output) const {
	if (Type != mshTRIANGLES || mtl.GetVertexCount() > 65534)
		return false;

	WORD *indices = NULL;

	// TODO: This won't work with over 64k verts per material!
	int idxCount = Idx.GetCount();
	WORD *triangles = rage_new WORD[idxCount];
	for (int i=0; i<idxCount; i++) 
		triangles[i] = (WORD) Idx[i];

	DWORD count = Stripify(idxCount/3,triangles,NULL,&indices,(OPTIMIZE_FOR_INDICES) | OUTPUT_TRISTRIP);

	delete[] triangles;

	int currentLength = 0;
	mshPrimitive *P = 0;

	for (DWORD j=0; j<count; j++) {
		if (currentLength == 0) {
			// Detect degenerate inserted to maintain winding
			if (indices[j] == indices[j+1])
				++j;
			P = &output.Append();
			P->Type = (j&1)? mshTRISTRIP2 : mshTRISTRIP;
			P->Priority = Priority;
		}
		P->Idx.Append() = indices[j];
		++currentLength;
		// Detect breaks since we can encode them cheaply on PS2 and GameCube.
		// We'll just end up re-inserting them again on PC/Xbox
		if (j<count-3 && indices[j]==indices[j+1] && indices[j+2]==indices[j+3]) {
			currentLength = 0;
			j += 2;
		}
	}

	delete[] indices;

	return true;
}


/*
	RETURNS
		Return the number of sub-primitives in a primitive.
	NOTES
		A sub-primitive is typically a single point, line segment,
		or triangle.  An exception is made for quads, where each
		quad or triangle counts as a single sub-primitive (otherwise
		the code would be forced to scan the entire index list to
		determine which entries were quads and which were triangles)
*/
int mshPrimitive::GetSubPrimitiveCount() const {
	int i = Idx();
	switch (Type) {
	case mshPOINTS:
		return i;
	case mshLINES:
		return i >> 1;
	case mshTRISTRIP:
	case mshTRISTRIP2:
		return i - 2;
	case mshTRIANGLES:
		return i / 3;
	case mshQUADS:	
		return i >> 2;
	case mshPOLYGON:
		return i - 2;
	default:
		return 0;
	}
}


void Serialize(mshSerializer &S,mshPrimType &t) {
	static const char *lut[] = { "POINTS","LINES","TRIANGLES","TRISTRIP","TRISTRIP2","QUADS","POLYGON" };
	if (S.Writing) {
		if (S.T.IsBinary())
			S.T.PutByte((int)t);
		else
			S.T.PutDelimiter(lut[(int)t]);
	}
	else {
		if (S.T.IsBinary())
			t = (mshPrimType) S.T.GetByte();
		else {
			char buf[32];
			S.T.GetToken(buf,sizeof(buf));
			for (int i=0; i<(int)(sizeof(lut)/sizeof(lut[0])); i++)
				if (!stricmp(buf,lut[i])) {
					t = (mshPrimType) i;
					return;
				}
			t = mshPOINTS;
		}
	}
}

/*
	PURPOSE
		Internal use only; implements serialization for mshPrimitive objects
	PARAMS
		S - Serializer object to use
*/
void mshPrimitive::Serialize(mshSerializer &S) {
	S.BeginBlock();
	SERIALIZE(S,Type);
	SERIALIZE(S,Priority);
	SERIALIZE(S,Idx);
	S.EndBlock();
}



/*
	RETURNS
		Returns triangle count in a primitive
*/
int mshPrimitive::GetTriangleCount() const {
	if (Type == mshTRIANGLES)
		return Idx.GetCount() / 3;
	else if (Type == mshQUADS)
		return Idx.GetCount() / 2;
	else if (Type == mshTRISTRIP || Type == mshTRISTRIP2)
		return Idx.GetCount() - 2;
	else
		return 0;
}

}	// namespace rage

#endif
