//
// mesh/bsp.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if 0

#include "bsp.h"

using namespace rage;

/*
	PURPOSE
		Initialize a mshBspVertex object as a linearly interpolated set
		between two other vertices
	PARAMS
		t - Lerp value (from A=0 to B=1)
		A - Start vertex
		B - Finish vertex
	NOTES
		Both vertices must have the same material index
*/
void mshBspVertex::Lerp(float t,const mshBspVertex &A,const mshBspVertex &B) {
	Pos.Lerp(t,A.Pos,B.Pos);
	Assert(A.Mtl == B.Mtl);
	Mtl = A.Mtl;
	Cpv.Lerp(t,A.Cpv,B.Cpv);
	Tex0.Lerp(t,A.Tex0,B.Tex0);
	Tex1.Lerp(t,A.Tex1,B.Tex1);
}


/*
	PURPOSE
		Initialize a mshBspVertex object as the point on the supplied cut plane
		between the two supplied vertices
	PARAMS
		plane - Plane equation of split plane
		A - Start vertex
		B - Finish vertex
	NOTES
		Behavior is undefined if plane is not between A and B.  Resuling data
		point is positioned on plane.
*/
void mshBspVertex::Cut(const Vector4 &plane,const mshBspVertex &A,const mshBspVertex &B) {
	float a = plane.DistanceToPlane(A.Pos);
	float b = plane.DistanceToPlane(B.Pos);
	Lerp(a/(a-b),A,B);
}


/*
	PURPOSE
		Default constructor
*/
mshBspNode::mshBspNode() {
	Neg = Pos = 0;
	Plane.Zero();
}


/*
	PURPOSE
		Destructor
*/
mshBspNode::~mshBspNode() {
	delete Neg;
	delete Pos;
}


/*
	PURPOSE
		Given a cut plane and a tolerance, break the input bsp node
		into multiple output bsp nodes
	PARAMS
		neg - Output bsp node containing negative halfspace.
		pos - Output bsp node containing positive halfspace
		cop - Output bsp node containing coplanar halfspace
		plane - Cut plane
		tol - Tolerance.  Any vertex at least this close to plane is considered on the plane itself
	NOTES
		All outputs are appended to, not replaced.
*/
void mshBspNode::Cut(mshBspNode &neg,mshBspNode &pos,mshBspNode &cop,const Vector4 &plane,float tol) const {

	// Classify all indices in primitive
	mshArray<s8> signs(Vtx.GetCount());
	int i;
	for (i=0; i<Vtx.GetCount(); i++) {
		float dist = plane.DistanceToPlane(Vtx[i].Pos);
		signs.Append() = (dist < -tol)? (s8)-1 : (dist > tol)? (s8)1 : (s8)0;
	}

	for (i=0; i<Vtx.GetCount()-2; i+=3) {
		bool splitAB = signs[i]*signs[i+1] == -1;
		bool splitBC = signs[i+1]*signs[i+2] == -1;
		bool splitCA = signs[i+2]*signs[i] == -1;
			
		if (splitAB) {
			mshBspVertex adjAB(plane,Vtx[i],Vtx[i+1]);
			if (splitBC) {		// AB+BC, B forms triangle, AC forms quad
				mshBspVertex adjBC(plane,Vtx[i+1],Vtx[i+2]);
				mshArray<mshBspVertex> &B = signs[i+1]<0? neg.Vtx : pos.Vtx;
				mshArray<mshBspVertex> &AC = signs[i+1]<0? pos.Vtx : neg.Vtx;
				B.Append3(Vtx[i+1],adjBC,adjAB);
				AC.Append3(Vtx[i],adjAB,adjBC);
				AC.Append3(Vtx[i],adjBC,Vtx[i+2]);
			}
			else if (splitCA) {	// AB+CA, A forms triangle, BC forms quad
				mshBspVertex adjCA(plane,Vtx[i+2],Vtx[i]);
				mshArray<mshBspVertex> &A = signs[i]<0? neg.Vtx : pos.Vtx;
				mshArray<mshBspVertex> &BC = signs[i]<0? pos.Vtx : neg.Vtx;
				A.Append3(Vtx[i],adjAB,adjCA);
				BC.Append3(Vtx[i+1],Vtx[i+2],adjCA);
				BC.Append3(Vtx[i+1],adjCA,adjAB);
			}
			else {				// C is on plane, A and B form triangles
				mshArray<mshBspVertex> &A = signs[i]<0? neg.Vtx : pos.Vtx;
				mshArray<mshBspVertex> &B = signs[i]<0? pos.Vtx : neg.Vtx;
				A.Append3(Vtx[i],adjAB,Vtx[i+2]);
				B.Append3(Vtx[i+1],Vtx[i+2],adjAB);
			}
		}
		else if (splitBC) {	
			mshBspVertex adjBC(plane,Vtx[i+1],Vtx[i+2]);
			if (splitCA) {	// BC+CA, C forms triangle, AB forms quad
				mshBspVertex adjCA(plane,Vtx[i+2],Vtx[i]);
				mshArray<mshBspVertex> &C = signs[i+2]<0? neg.Vtx : pos.Vtx;
				mshArray<mshBspVertex> &AB = signs[i+2]<0? pos.Vtx : neg.Vtx;
				C.Append3(Vtx[i+2],adjCA,adjBC);
				AB.Append3(Vtx[i],Vtx[i+1],adjBC);
				AB.Append3(Vtx[i],adjBC,adjCA);
			}
			else {			// A is on plane, B and C form triangles
				mshArray<mshBspVertex> &B = signs[i+1]<0? neg.Vtx : pos.Vtx;
				mshArray<mshBspVertex> &C = signs[i+1]<0? pos.Vtx : neg.Vtx;
				B.Append3(Vtx[i+1],adjBC,Vtx[i]);
				C.Append3(Vtx[i+2],Vtx[i],adjBC);
			}
		}
		else if (splitCA) {	// B is on plane, A and C form triangles
			mshBspVertex adjCA(plane,Vtx[i+2],Vtx[i]);
			mshArray<mshBspVertex> &A = signs[i]<0? neg.Vtx : pos.Vtx;
			mshArray<mshBspVertex> &C = signs[i]<0? pos.Vtx : neg.Vtx;
			A.Append3(Vtx[i],Vtx[i+1],adjCA);
			C.Append3(Vtx[i+2],adjCA,Vtx[i+1]);
		}
		else {				// No split required
			if (signs[i]<0 || signs[i+1]<0 || signs[i+2]<0)
				neg.Vtx.Append3(Vtx[i],Vtx[i+1],Vtx[i+2]);
			else if (signs[i]>0 || signs[i+1]>0 || signs[i+2]>0)
				pos.Vtx.Append3(Vtx[i],Vtx[i+1],Vtx[i+2]);
			else
				cop.Vtx.Append3(Vtx[i],Vtx[i+1],Vtx[i+2]);
		}
	}
}



/*
	PURPOSE
		Given a bsp node and a cut plane, determine the number of surfaces that would end up in the
		negative halfspace, positive halfspace, or coplanar space.
	PARAMS
		neg - Output count of surfaces in the negative halfspace
		pos - Output count of surfaces in the positive halfspace
		cop - Output count of surfaces in coplanar halfspace (will be at least
			one if plane was computed from one of the input surfaces)
		split - Output count of number of surfaces that had to be split
		plane - Cut plane
		tol - Tolerance.  Any vertex at least this close to plane is considered on the plane itself
	NOTES
		Any surface that is split will also count as both a neg and pos surface
*/
void mshBspNode::Classify(int &neg,int &pos,int &cop,int &split,const Vector4 &plane,float tol) const {
	neg = pos = cop = split = 0;

	// Classify all indices in primitive
	mshArray<s8> signs(Vtx.GetCount());
	int i;
	for (i=0; i<Vtx.GetCount(); i++) {
		float dist = plane.DistanceToPlane(Vtx[i].Pos);
		signs.Append() = (dist < -tol)? (s8)-1 : (dist > tol)? (s8)1 : (s8)0;
	}

	for (i=0; i<Vtx.GetCount()-2; i+=3) {
		bool splitAB = signs[i]*signs[i+1] == -1;
		bool splitBC = signs[i+1]*signs[i+2] == -1;
		bool splitCA = signs[i+2]*signs[i] == -1;

		if (splitAB || splitBC || splitCA) {
			++neg;
			++pos;
			++split;
		}
		else {				// No split required
			if (signs[i]<0 || signs[i+1]<0 || signs[i+2]<0)
				++neg;
			else if (signs[i]>0 || signs[i+1]>0 || signs[i+2]>0)
				++pos;
			else
				++cop;
		}
	}
}


/*
	PURPOSE
		Given a bsp node and a cut plane, produce a score for the suitability of
		that plane (lower values are better)
	PARAMS
		plane - Cut plane
		tol - Tolerance.  Any vertex at least this close to plane is considered on the plane itself
		convex - Flag which is forced to false if both halfspaces contain polys
	RETURNS
		Score for cut plane, with lower values being better.
	NOTES
		Factors affecting score:
		- How far out of balance resulting tree is counts against us
		- Number of splits we produce counts against us
		- Number of coplanar tris counts in our favor
	TODO
		Determine appropriate weights for each sub-term
*/
int mshBspNode::Classify(const Vector4 &plane,float tol,bool &convex) const {
	int neg, pos, cop, split;
	Classify(neg,pos,cop,split,plane,tol);
	if (neg && pos)
		convex = false;
	return abs(neg - pos) + 5 * split - 2 * cop;
}


static int s_NodeCount = 1;


/*
	PURPOSE
		Given a mshBspNode which contains a list of surfaces,
		recursively partition it into a BSP tree.
	PARAMS
		tol - Tolerance for "point versus plane" check.
	RETURNS
		Pointer to root of resulting BSP tree
*/
const mshBspNode* mshBspNode::MakeTree(float tol) const {
	if (Vtx.GetCount() == 0)
		return 0;

	int bestScore = 0x7FFFFFFF;
	// int bestIdx = -1;
	Vector4 bestPlane;
	int i;
	bool convex = true;

	// Locate best candidate split plane
	for (i=0; i<Vtx.GetCount()-2; i+=3) {
		Vector4 plane;
		plane.ComputePlane(Vtx[i].Pos,Vtx[i+1].Pos,Vtx[i+2].Pos);
		if (plane.Mag2()) {
			int thisScore = Classify(plane,tol,convex);
			if (thisScore < bestScore) {
				bestPlane = plane;
				bestScore = thisScore;
				// bestIdx = i;
			}
		}
		else {
			Vtx[i].Pos.Print("Degenerate:\n0 ",true);
			Vtx[i+1].Pos.Print("1 ",true);
			Vtx[i+2].Pos.Print("2 ",true);
		}
	}

	++s_NodeCount;

	if (convex) {
		mshBspNode *newNode = rage_new mshBspNode;
		newNode->Vtx = Vtx;
		return newNode;
	}
	else {
		// Perform split
		mshBspNode neg, pos, cop;
		Cut(neg, pos, cop, bestPlane, tol);

		Assert(cop.Vtx.GetCount());

		mshBspNode *newNode = rage_new mshBspNode;
		newNode->Vtx.Assume(cop.Vtx);
		newNode->Neg = neg.MakeTree(tol);
		newNode->Pos = pos.MakeTree(tol);
		newNode->Plane = bestPlane;
		return newNode;
	}
}


/*
	PURPOSE
		Initialize a mshBspNode from a mesh object.
	PARAMS
		M - Mesh to initialize mshBspNode from
*/
void mshBspNode::FromMesh(const mshMesh &M) {
	Plane.Zero();
	Neg = Pos = 0;

	int i, j, k;
	for (i=0; i<M.GetMtlCount(); i++) {
		for (j=0; j<M.GetMtl(i).Prim.GetCount(); j++) {
			const mshPrimitive &P = M.GetMtl(i).Prim[j];
			Assert(P.Type == mshTRIANGLES);
			for (k=0; k<P.Idx.GetCount(); k++) {
				const mshAdjunct &A = M.GetAdj(P.Idx[k]);
				mshBspVertex &V = Vtx.Append();
				V.Pos = M.GetPos(A.P);
				V.Mtl = i;
				if (M.GetCpvCount())
					V.Cpv = M.GetCpv(A.C0);
				else
					V.Cpv.Zero();
				if (M.GetTexCount(0))
					V.Tex0 = M.GetTex(0,A.T[0]);
				else
					V.Tex0.Zero();
				if (M.GetTexCount(1))
					V.Tex1 = M.GetTex(1,A.T[1]);
				else
					V.Tex1.Zero();
			}
		}
	}
}



/*
	PURPOSE
		Renders BSP recusrively using supplied color and renderer object
	PARAMS
		color - Draw color for coplanar polys
		R - renderer object
		level - recursion level; subtrees are drawn only when level is nonzero
	NOTES
		Negative halfspace is drawn with red tint of draw color.
		Positive halfspace is drawn with green tint of draw color.
*/
void mshBspNode::Draw(const Vector4 &color,mshRenderer &R,int level) const {
	const float atten = 0.8f;
	R.Color(color);
	R.Begin(mshTRIANGLES,Vtx.GetCount());
	for (int i=0; i<Vtx.GetCount(); i++)
		R.Vertex(Vtx[i].Pos);
	R.End();

	if (Neg && level)
		Neg->Draw(Vector4(color.x,color.y * atten,color.z * atten,color.w),R,level-1);
	if (Pos && level)
		Pos->Draw(Vector4(color.x * atten,color.y,color.z * atten,color.w),R,level-1);
}


/*
	PURPOSE
		Construct a mshMesh object from a mshBspNode tree
	PARAMS
		M - output mesh
		origMesh - original mesh, used for material assignments
*/
void mshBspNode::ToMesh(mshMesh &M,const mshMesh &origMesh) const {
	Displayf("%d nodes in last tree",s_NodeCount);
	s_NodeCount = 1;
	M.Reset();
	for (int i=0; i<origMesh.GetMtlCount(); i++) {
		M.AddMtl(origMesh.GetMtl(i));
		M.GetMtl(i).Prim.Reset();
	}

	ToMeshRecurse(M);

	M.CleanModel();
	M.Tristrip();
}



void mshBspNode::ToMeshRecurse(mshMesh &M) const {
	int i;
	for (i=0; i<Vtx.GetCount()-2; i+=3) {
		mshArray<mshPrimitive> &PA = M.GetMtl(Vtx[i].Mtl).Prim;
		mshPrimitive &P = PA.GetCount()? PA[0] : PA.Append();
		P.Type = mshTRIANGLES;
		P.Priority = 0;
		P.Idx.Append3(
			M.AddAdj(Vtx[i].Pos,ORIGIN,Vtx[i].Cpv,Vtx[i].Tex0,Vtx[i].Tex1),
			M.AddAdj(Vtx[i+1].Pos,ORIGIN,Vtx[i+1].Cpv,Vtx[i+1].Tex0,Vtx[i+1].Tex1),
			M.AddAdj(Vtx[i+2].Pos,ORIGIN,Vtx[i+2].Cpv,Vtx[i+2].Tex0,Vtx[i+2].Tex1));
	}

	if (Neg)
		Neg->ToMeshRecurse(M);
	if (Pos)
		Pos->ToMeshRecurse(M);
}

#endif
