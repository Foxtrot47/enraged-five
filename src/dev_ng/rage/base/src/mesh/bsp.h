//
// mesh/bsp.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef MESH_BSP_H
#define MESH_BSP_H

#include "mesh/mesh.h"

namespace rage {

class mshMesh;

/*
	PURPOSE
		All possible data associated with a vertex in a BSP tree.  Material index is stored
		with each vertex to simplify split operations, but the assumption is that any given
		triangle consists of vertices with the same material
*/
struct mshBspVertex {
	mshBspVertex() { }
	mshBspVertex(const Vector4 &plane,const mshBspVertex &A,const mshBspVertex &B) { Cut(plane,A,B); }
	
	Vector3 Pos;
	int Mtl;
	Vector4 Cpv;
	Vector2 Tex0, Tex1;

	void Lerp(float t,const mshBspVertex &A,const mshBspVertex &B);
	void Cut(const Vector4 &plane,const mshBspVertex &A,const mshBspVertex &B);
};


/*
	PURPOSE
		Stores a BSP tree structure as an array of triangles which are contained within the
		specified cut plane, along with pointers to the halfspaces on each side of the cut
		plane.  The tree generate stops once we reach a convex area, so this is suitable for
		point-in-space containment problems.
*/
struct mshBspNode {
	mshBspNode();
	~mshBspNode();
	
	void FromMesh(const mshMesh &M);
	void ToMesh(mshMesh &M,const mshMesh &origMesh) const;

	void Cut(mshBspNode &neg,mshBspNode &pos,mshBspNode &cop,const Vector4 &plane,float tol) const;
	void Classify(int &neg,int &pos,int &cop,int &split,const Vector4 &plane,float tol) const;
	int Classify(const Vector4 &plane,float tol,bool &convex) const;
	void Draw(const Vector4 &color,mshRenderer &R,int level) const;
	
	const mshBspNode *MakeTree(float tol = 0.1f) const;

	mshArray<mshBspVertex> Vtx;		// Will be a multiple of three
	const mshBspNode *Neg, *Pos;
	Vector4 Plane;
private:
	void ToMeshRecurse(mshMesh &M) const;
};

}	// namespace rage

#endif
