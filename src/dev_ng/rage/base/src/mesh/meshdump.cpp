//
// mesh/meshdump.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "node.h"
#include "serialize.h"
#include "core/stream.h"
#include "data/token.h"

void main(int /*argc*/,const char **argv) {
	SafeStream F(Stream::Open(argv[1]));

	datTokenizer T;
	T.Init(argv[1],F);

	mshSerializer S(T,false);

	mshNode ROOT;
	ROOT.Serialize(S);

	ROOT.Print(0);
}
