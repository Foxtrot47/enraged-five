//
// mesh/mesh.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "mesh.h"
#include "serialize.h"
#include "file/stream.h"
#include "atl/array.h"
#include "atl/queue.h"
#include "vector/geometry.h"
#include "vector/quaternion.h"

#if MESH_LIBRARY

namespace rage {


mshMesh::mshMesh(bool skinned) {
	BlendTargetCount = 0;
	Skinned = skinned;
	Sphere.Zero();
	BoxMatrix.Identity();
	BoxSize.Zero();
	for (int i=0; i<mshMaxFloatBlindData; i++) 
		FloatBlindDataIds[i] = (int)mshVtxBlindUNASSIGNED;
}

// Magic Software, Inc.
// http://www.magic-software.com
// Copyright (c) 2000-2002.  All Rights Reserved
//
// Source code from Magic Software is supplied under the terms of a license
// agreement and may not be copied or disclosed except in accordance with the
// terms of that agreement.  The various license agreements may be found at
// the Magic Software web site.  This file is subject to the license
//
// FREE SOURCE CODE
// http://www.magic-software.com/License/free.pdf
void ComputeVertices(const Matrix34& m_akAxis, const Vector3& vExtents, Vector3 akVertex[8])
{
    Vector3 akEAxis[3] =
    {
        m_akAxis.a * vExtents.x,
        m_akAxis.b * vExtents.y,
        m_akAxis.c * vExtents.z
    };

    akVertex[0] = m_akAxis.d - akEAxis[0] - akEAxis[1] - akEAxis[2];
    akVertex[1] = m_akAxis.d + akEAxis[0] - akEAxis[1] - akEAxis[2];
    akVertex[2] = m_akAxis.d + akEAxis[0] + akEAxis[1] - akEAxis[2];
    akVertex[3] = m_akAxis.d - akEAxis[0] + akEAxis[1] - akEAxis[2];
    akVertex[4] = m_akAxis.d - akEAxis[0] - akEAxis[1] + akEAxis[2];
    akVertex[5] = m_akAxis.d + akEAxis[0] - akEAxis[1] + akEAxis[2];
    akVertex[6] = m_akAxis.d + akEAxis[0] + akEAxis[1] + akEAxis[2];
    akVertex[7] = m_akAxis.d - akEAxis[0] + akEAxis[1] + akEAxis[2];
}

void GetExtents(const Vector3 akVertex[8], Vector3 &vMin, Vector3 &vMax)
{
	for (u32 uIndex = 0; uIndex < 8; uIndex++)
	{
		// Max
		vMax.x = std::max(akVertex[uIndex].x, vMax.x);
		vMax.y = std::max(akVertex[uIndex].y, vMax.y);
		vMax.z = std::max(akVertex[uIndex].z, vMax.z);
		// Min
		vMin.x = std::min(akVertex[uIndex].x, vMin.x);
		vMin.y = std::min(akVertex[uIndex].y, vMin.y);
		vMin.z = std::min(akVertex[uIndex].z, vMin.z);
	}
}

// TODO -- use spdSphere::MergeSpheres
Vector4 MergeSpheres(const Vector4 &rkSphere0, const Vector4 &rkSphere1)
{
    Vector4 kCDiff = rkSphere1 - rkSphere0; 
	kCDiff.w = 0.0f;
    float fLSqr = kCDiff.Mag2();
    float fRDiff = rkSphere1.w - rkSphere0.w;
    float fRDiffSqr = fRDiff*fRDiff;

    if ( fRDiffSqr >= fLSqr )
        return ( fRDiff >= 0.0f ? rkSphere1 : rkSphere0 );

    float fLength = sqrtf(fLSqr);
    const float fTolerance = 1e-06f;
    Vector4 kSphere;

    if ( fLength > fTolerance )
    {
        float fCoeff = (fLength + fRDiff)/(2.0f*fLength);
        kSphere = kCDiff;
		kSphere *= fCoeff;
		kSphere += rkSphere0;
    }
    else
    {
        kSphere = rkSphere0;
    }

    kSphere.w = 0.5f*(fLength + rkSphere0.w +
        rkSphere1.w);

    return kSphere;
}

//----------------------------------------------------------------------------
void MergeBoxes(Matrix34 &kBox, Vector3 &kBoxExtents, 
				const Matrix34 &rkBox0, const Vector3 &rkBoxExtents0,
				const Matrix34 &rkBox1, const Vector3 &rkBoxExtents1)
{
	// Assume Identity Orientation
	kBox.Identity();
	Vector3 vMin, vMax;
	vMin.Set(FLT_MAX, FLT_MAX, FLT_MAX);
	vMax.Set(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	Vector3 akVertex[8];
	ComputeVertices(rkBox0, rkBoxExtents0, akVertex);
	GetExtents(akVertex, vMin, vMax);
	ComputeVertices(rkBox1, rkBoxExtents1, akVertex);
	GetExtents(akVertex, vMin, vMax);
	kBoxExtents = vMax - vMin;
	kBoxExtents /= 2.0f;
	kBox.d = (vMax + vMin);
	kBox.d /= 2.0f;
}

/* 
	PURPOSE
		Copy constructor
	PARAMS
		m - the object to copy
*/
mshMesh& mshMesh::operator=(const mshMesh& m)
{
	Mtl.Resize(m.Mtl.GetCount());
	for( int i = 0; i < m.Mtl.GetCount(); i++ )
	{
		Mtl[i] = m.Mtl[i];
	}

	Offset.Resize(m.Offset.GetCount());
	for( int i = 0; i < m.Offset.GetCount(); i++ )
	{
		Offset[i] = m.Offset[i];
	}

	Sphere = m.Sphere;
	BoxMatrix = m.BoxMatrix;
	BoxSize = m.BoxSize;
	Skinned = m.Skinned;
	BlendTargetCount = m.BlendTargetCount;

	for( int i = 0; i < mshMaxFloatBlindData; i++ )
	{
		FloatBlindDataIds[i] = m.FloatBlindDataIds[i];
	}

	return *this;
}

/* 
	PURPOSE
		Concatenate mesh
	PARAMS
		m - the object to concatenate
*/
void mshMesh::Concatenate(const mshMesh& m)
{
	const u32 uGrowBy = 2;
	int iOriginalCount = Mtl.GetCount();
	Assert(Skinned == m.Skinned);
	Assert(BlendTargetCount == m.BlendTargetCount);
	for( int i = 0; i < mshMaxFloatBlindData; i++ )
	{
		Assert(FloatBlindDataIds[i] == m.FloatBlindDataIds[i]);
	}

	Mtl.Resize(m.Mtl.GetCount() + iOriginalCount, uGrowBy * (m.Mtl.GetCount() + iOriginalCount));
	for( int i = iOriginalCount; i < (iOriginalCount + m.Mtl.GetCount()); i++ )
	{
		Mtl[i] = m.Mtl[i - iOriginalCount];
	}

	iOriginalCount = Offset.GetCount();
	Offset.Resize(m.Offset.GetCount() + iOriginalCount, uGrowBy * (m.Offset.GetCount() + iOriginalCount));
	for( int i = iOriginalCount; i < (iOriginalCount + m.Offset.GetCount()); i++ )
	{
		Offset[i] = m.Offset[i - iOriginalCount];
	}

	Sphere = MergeSpheres(m.Sphere, Sphere);

	Matrix34 mTemp = BoxMatrix;
	Vector3  vTemp = BoxSize;
	MergeBoxes(BoxMatrix, BoxSize, mTemp, vTemp, m.BoxMatrix, m.BoxSize);


}

/* 
	PURPOSE
		Renders all materials (and therefore all primitives) in the mesh object
	PARAMS
		R - Renderer object 
*/
void mshMesh::Draw(const mshRenderer &R) const {
	for (int i=0; i<Mtl(); i++)
		Mtl[i].Draw(R);
}


void mshMesh::BackfaceCull(const Vector3 &eyept) {
	for (int i=0; i<Mtl(); i++)
		Mtl[i].BackfaceCull(eyept);
}


/* 
	PURPOSE
		Renders all materials (and therefore all primitives) in the mesh object
	PARAMS
		R - Renderer object 
*/
void mshMesh::DrawSkinned(const mshRenderer &R,const Matrix34 *matrices) const {
	for (int i=0; i<Mtl(); i++)
		Mtl[i].DrawSkinned(R,matrices);
}



/* 
	PURPOSE
		Draws all normals in the mesh
	PARAMS
		R - Renderer object
		c - Color to use
		length - length of the vector 
*/
void mshMesh::DrawNormals(const mshRenderer &R, Color32 &c, float length) const {
	for (int i=0; i<Mtl(); i++)
		Mtl[i].DrawNormals(R, c, length);
}

/* 
	PURPOSE
		Draws all tangents in the mesh
	PARAMS
		R - Renderer object 
		c - Color to use
		length - length of the vector 
*/
void mshMesh::DrawTangents(const mshRenderer &R, Color32 &c, float length) const {
	for (int i=0; i<Mtl(); i++)
		Mtl[i].DrawTangents(R, c, length);
}

/* 
	PURPOSE
		Draws all binromals in the mesh
	PARAMS
		R - Renderer object 
		c - Color to use
		length - length of the vector 
		shaderCompute - whether to use the computed binromal that default rage shaders use
*/
void mshMesh::DrawBinormals(const mshRenderer &R, Color32 &c, float length, bool shaderCompute) const {
	for (int i=0; i<Mtl(); i++)
		Mtl[i].DrawBinormals(R, c, length, shaderCompute);
}


/* 
	PURPOSE
		Adds a bone offset to the Offset array of the mesh object.
	PARAMS
		V - offset to add
	RETURNS
		Zero-based index of the rage_new data
*/
mshIndex mshMesh::AddOffset(const Vector3 &V) {
	Offset.Append() = V;
	return (mshIndex) (Offset.GetCount() - 1);
}


/*
	PURPOSE
		Resets the contents of the mesh to empty (and non-skinned)
*/
void mshMesh::Reset() {
	Mtl.Reset();
	Offset.Reset();
	Skinned = false;
}


/* 
	PURPOSE
		Convert a mesh's vertices to skinned vertices which have four matrix bindings and weights.
	NOTES
		The weight is initialized to be fully attached to matrix zero.  If the mesh is already
		skinned, the function does nothing.
*/
void mshMesh::MakeSkinned() {
	Assert(Mtl.GetCount() == 0);
	Skinned = true;
}


/*
	PURPOSE
		Convert a mesh's vertices to unskinned vertices by throwing out the matrix bindings and weights.
	NOTES
		If the mesh is already unskinned, the function does nothing.
*/
void mshMesh::MakeRigid() {
	Assert(Mtl.GetCount() == 0);
	Skinned = false;
}


/*
	PURPOSE
		Triangulate all materials in a mesh
*/
void mshMesh::Triangulate() {

	for (int i=0; i<Mtl.GetCount(); i++) {
		Mtl[i].Triangulate();
	}
}


/*
	PURPOSE
		Tristrip all materials in a mesh
*/
void mshMesh::Tristrip() {
	for (int i=0; i<Mtl.GetCount(); i++) {
		Mtl[i].Tristrip();
	}
}


void mshMesh::CacheOptimize() {
	for (int i=0; i<Mtl.GetCount(); i++) {
		Mtl[i].CacheOptimize();
	}
}


void mshMesh::ReorderVertices() {
	for (int i=0; i<Mtl.GetCount(); i++) {
		Mtl[i].ReorderVertices();
	}
}


/*
	PURPOSE
		Remove degenerates in all primitives
*/
void mshMesh::RemoveDegenerates() {
	for (int i=0; i<Mtl.GetCount(); i++)
	{
		Mtl[i].RemoveDegenerates();
	}
	for (int i=0; i<Mtl.GetCount(); i++)
	{
		if( Mtl[i].Prim.GetCount() == 0 )
		{
			Mtl.Delete(i);
			--i;
		}
	}
}

/*
PURPOSE
Remove unreferenced vertices
*/
void mshMesh::RemoveUnreferenced() {
	for (int i=0; i<Mtl.GetCount(); i++)
	{
		Mtl[i].RemoveUnreferenced();
	}
}

/*
PURPOSE
Reverse the draw order used mostly to handle negative scale
*/
void mshMesh::Reverse() {
	for (int i=0; i<Mtl.GetCount(); i++)
	{
		Mtl[i].Reverse();
	}
}

void mshMesh::NormalizeColors()
{
	float maximum=1.0f;

	// find maximum:
	for (int i=0;i<Mtl.GetCount();i++)
	{
		for (int j=0;j<Mtl[i].GetVertexCount();j++)
		{
			const Vector4& cpv=Mtl[i].GetCpv(j);			
			if (cpv.x>maximum)
				maximum=cpv.x;
			if (cpv.y>maximum)
				maximum=cpv.y;
			if (cpv.z>maximum)
				maximum=cpv.z;
		}
	}


	// scale based on maximum:
	if (maximum>1.0f)
	{
		float maxInv=1.0f/maximum;
		for (int i=0;i<Mtl.GetCount();i++)
		{
			for (int j=0;j<Mtl[i].GetVertexCount();j++)
			{
				Vector4 cpv=Mtl[i].GetCpv(j);
				cpv.Set(Clamp(cpv.x*maxInv,0.0f,1.0f),Clamp(cpv.y*maxInv,0.0f,1.0f),Clamp(cpv.z*maxInv,0.0f,1.0f),1.0f);
				Mtl[i].SetCpv(j,cpv);
			}
		}
	}
}


/*
	PURPOSE
		Merges multiple instances of the same material into a single material
*/
void mshMesh::MergeSameMaterials()
{
	mshArray<mshMaterial> newMtls(Mtl.GetCount());
	atBitSet used(Mtl.GetCount());
	used.Reset();
	for (int i=0; i<Mtl.GetCount(); i++) 
	{
		if (!used.IsSet(i)) 
		{
			used.Set(i);
			// bool merged = false;
			for (int j=i+1; j<Mtl.GetCount(); j++) 
			{
				if (!used.IsSet(j)) 
				{
					if (Mtl[i].Name == Mtl[j].Name && Mtl[i].Merge(Mtl[j])) 
					{
						// merged = true;
						used.Set(j);
					}
				}
			}
			newMtls.Append() = Mtl[i];
		}
	}

	Mtl.Assume(newMtls);
}


/*
	PURPOSE
		Break a mesh up into smaller pieces based upon criteria from a user-defined callback.
*/
void mshMesh::Packetize(int (*cb)(const mshMesh &mesh,const mshMaterial &mtl,void *closure),void *closure,int maxMtxCount)
{
	mshArray<mshMaterial> newMtls;
	for (int i=0; i<Mtl.GetCount(); i++)
	{
		int finalCount = cb(*this, Mtl[i], closure);
		bool overSkinLimit = false;

		if (IsSkinned())
		{
			atFixedBitSet<256> mtxUsed;
			int totalMtxUsed = 0;
			const mshMaterial &mtl = Mtl[i];
			// Make sure it's triangles
			Assert(mtl.Prim.GetCount() == 1 && mtl.Prim[0].Type == mshTRIANGLES);

			for (int j=0; j<mtl.GetVertexCount(); j++)
			{
				const mshBinding &b = mtl.GetVertex(j).Binding;
				if( b.IsPassThrough == false )
				{
					for (int k=0; k<mshMaxMatricesPerVertex; k++)
					{
#if __RESOURCECOMPILER
						if(b.Mtx[k] >= mtxUsed.GetNumBits())
						{
							Errorf("Vertex %d trying to set bit on mshMesh matrix bitset that's higher than allowed: %d >= %d!", j, b.Mtx[k], mtxUsed.GetNumBits());
							continue;
						}
#endif
						if (b.Wgt[k] && !mtxUsed.GetAndSet(b.Mtx[k]))
							++totalMtxUsed;
					}
				}
			}
			overSkinLimit = (totalMtxUsed > maxMtxCount);
		}

		if (finalCount < Mtl[i].GetVertexCount() || overSkinLimit)
			Mtl[i].Packetize(newMtls,finalCount,maxMtxCount);
		else
			newMtls.Append() = Mtl[i];
	}

	// Displayf("Input mesh had %d materials, output now has %d",Mtl.GetCount(),newMtls.GetCount());

	Mtl.Assume(newMtls);
}


/*
	PURPOSE
		Convenience function which cleans a mesh (assuming it's going to be
		used as a model) by doing a weld, triangulation, degenerate removal,
		unreferenced channel and adjunct removal.  You will probably want
		to call mshMesh::Tristrip yourself afterward if the mesh is going
		to be rendered.
	PARAMS
		quantizeAndWeld - if true, mesh normal, CPV, and texture coordinate
			values will be quantized, and all values and adjuncts will
			be welded.
	NOTES
		The operations are done in a specific order; welding is done first,
		which may expose degenerates.  Removing degenerates may cause some
		adjuncts to go completely unreferenced, which in turn may cause
		channel data to go completely unreferenced.
*/
void mshMesh::CleanModel() {
	Weld();
	Triangulate();
	RemoveDegenerates();
}


/*
	RETURNS
		Cumulative triangle count in the mesh
*/
int mshMesh::GetTriangleCount() const {
	int result = 0;
	for (int i=0; i<Mtl.GetCount(); i++)
		result += Mtl[i].GetTriangleCount();
	return result;
}

/*
RETURNS
Cumulative vertex count in the mesh
*/
int mshMesh::GetVertexCount() const {
	int result = 0;
	for (int i=0; i<Mtl.GetCount(); i++)
		result += Mtl[i].GetVertexCount();
	return result;
}



void Serialize(mshSerializer &S,mshMaterial &v) { v.Serialize(S); }
void Serialize(mshSerializer &S,mshTangentBinormal &v) {
	Serialize(S,v.T);
	Serialize(S,v.B);
}

// Turn this off once we no longer support old mesh formats.
#define COMPATIBLE	1

#if COMPATIBLE
void Serialize(mshSerializer &S,u16 &v) {
	if (S.Writing) {
		S.T.PutShort(v);
	}
	else {
		v = (u16) S.T.GetShort();
	}
}
struct mshAdjunct {
	u16 P, N, TB0, C0, T0, T1;
	void Serialize(mshSerializer &S);
};
struct mshTB
{
	Vector3 T;
	Vector3 B;
	void Serialize(mshSerializer &S);
};
class mshSkin: public Vector3 {
public:
	bool IsClose(const mshSkin &S,float tol) const {
		return Vector3::IsClose(S,tol) && Bind == S.Bind;
	}
	mshBinding Bind;
	void Serialize(mshSerializer &S);
};

struct mshOldPrimitive {
	mshPrimType Type;
	u16 Priority;
	mshArray<u16> Idx;
	void Serialize(mshSerializer &S);
};
struct mshOldMtl {
	ConstString Name;
	u16 Priority;
	mshArray<mshOldPrimitive> Prim;
	void Serialize(mshSerializer &S);
};
enum {
	HAS_NORMAL = 1, HAS_TB0 = 2, HAS_CPV = 4, HAS_TEX0 = 8, HAS_TEX1 = 16
};
static int s_Channels;
void mshAdjunct::Serialize(mshSerializer &S) {
	SERIALIZE(S,P);
	if (s_Channels & HAS_NORMAL) SERIALIZE(S,N);
	if (s_Channels & HAS_TB0) SERIALIZE(S,TB0);
	if (s_Channels & HAS_CPV) SERIALIZE(S,C0);
	if (s_Channels & HAS_TEX0) SERIALIZE(S,T0);
	if (s_Channels & HAS_TEX1) SERIALIZE(S,T1);
}
void mshTB::Serialize(mshSerializer &S) {
	rage::Serialize(S, T);
	rage::Serialize(S, B);
}
void Serialize(mshSerializer &S,mshTB &v) { v.Serialize(S); }
void Serialize(mshSerializer &S,mshAdjunct &a) { a.Serialize(S); }
void Serialize(mshSerializer &S,mshOldPrimitive &v) { v.Serialize(S); }
extern void Serialize(mshSerializer &S,mshPrimType &t);
void mshOldMtl::Serialize(mshSerializer &S) {
	S.BeginBlock();
	SERIALIZE(S,Name);
	SERIALIZE(S,Priority);
	SERIALIZE(S,Prim);
	S.EndBlock();
}
void mshOldPrimitive::Serialize(mshSerializer &S) {
	S.BeginBlock();
	SERIALIZE(S,Type);
	SERIALIZE(S,Priority);
	SERIALIZE(S,Idx);
	S.EndBlock();
}
void Serialize(mshSerializer &S,mshSkin &v) {
	S.BeginValue(0);
	rage::Serialize(S,v.x);
	rage::Serialize(S,v.y);
	rage::Serialize(S,v.z);
	// Newer formats stop storing after a weight of zero and can tolerate mshMaxMatricesPerVertex changing.
	// This is a legacy format that always stores all 4 weights, so intentionally don't use the constant.
	for (int i=0; i<4 /*mshMaxMatricesPerVertex*/; i++) {
		u8 temp = (u8) v.Bind.Mtx[i];
		Serialize(S,temp);
		v.Bind.Mtx[i] = temp;
	}
	Serialize(S,v.Bind.Wgt[0]);
	Serialize(S,v.Bind.Wgt[1]);
	Serialize(S,v.Bind.Wgt[2]);
	Serialize(S,v.Bind.Wgt[3]);
	S.EndValue();
}
void Serialize(mshSerializer &S,mshOldMtl &m) { m.Serialize(S); }
void Serialize(mshSerializer &S,mshSkin &v);
#endif


/*
	PURPOSE
		Writes the mesh information out to supplied stream object
	PARAMS
		S - stream to write mesh to
		indent - number of spaces to indent each line (useful when saving out meshes within larger files)
*/
void mshMesh::Serialize(mshSerializer &S) {
#if COMPATIBLE
	u8 Version = 0;
	if (!S.Writing) {
		if (S.T.IsBinary() || S.T.CheckToken("Version"))
			Version = (u8) S.T.GetByte();
	}
	else {
		Version = 12;
		SERIALIZE(S,Version);
	}

	if( Version < 12 )
	{
		Warningf("Loading mesh file version %d, latest version is 12.", Version);
	}
	if( Version > 12 )
	{
		Errorf("Loading mesh file version %d, which is not supported (max is 12)",Version);
	}
	
	S.BeginBlock();

	// Detect binary files that are the original version with no version header.
	if (!S.Writing && S.T.IsBinary() && Version < 2) {
		Skinned = Version;
		Version = 0;
	}
	else {
		SERIALIZE(S,Skinned);
	}
	if (Version < 5) {
		// Displayf("Version %d mesh file found!",Version);
		AssertMsg(Version == 0 || Version == 2,"Sorry, only legacy V0 is supported.");
		mshArray<mshSkin> PosSkin;
		mshArray<Vector3> Pos;
		mshArray<Vector3> Nrm;
		mshArray<mshTB> TanBi0;
		mshArray<Vector4> Cpv;
		mshArray<Vector2> Tex0, Tex1;
		mshArray<mshAdjunct> Adj;
		mshArray<mshOldMtl> Mtl;
		SERIALIZE(S,PosSkin);
		SERIALIZE(S,Pos);
		SERIALIZE(S,Nrm);
		if( Version == 2 )
		{
			SERIALIZE(S,TanBi0);
		}
		SERIALIZE(S,Cpv);
		SERIALIZE(S,Tex0);
		SERIALIZE(S,Tex1);
		s_Channels = 0;
		if (Nrm.GetCount()) s_Channels |= HAS_NORMAL;
		if (TanBi0.GetCount()) s_Channels |= HAS_TB0;
		if (Cpv.GetCount()) s_Channels |= HAS_CPV;
		if (Tex0.GetCount()) s_Channels |= HAS_TEX0;
		if (Tex1.GetCount()) s_Channels |= HAS_TEX1;
		SERIALIZE(S,Adj);
		SERIALIZE(S,Mtl);
		mshBinding dummy;
		dummy.Reset();
		Vector3 dummyN(0,0,0);
		Vector4 dummyCPV(1,1,1,1);
		for (int i=0; i<Mtl.GetCount(); i++) 
		{
			mshMaterial &dest = NewMtl();
			const mshOldMtl &src = Mtl[i];
			dest.Name = src.Name;
			dest.Priority = src.Priority;
			for (int j=0; j<src.Prim.GetCount(); j++) 
			{
				const mshOldPrimitive &srcP = src.Prim[j];
				mshPrimitive &destP = dest.Prim.Append();
				destP.Type = srcP.Type;
				destP.Priority = srcP.Priority;
				for (int k=0; k<srcP.Idx.GetCount(); k++) 
				{
					const mshAdjunct &A = Adj[srcP.Idx[k]];
					Vector2 tcs[2];
					if (Tex0.GetCount())
					{
						tcs[0] = Tex0[A.T0];
					}
					else
					{
						tcs[0].Zero();
					}
					if (Tex1.GetCount())
					{
						tcs[1] = Tex1[A.T1];
					}
					else
					{
						tcs[1].Zero();
					}
					int newVertex = dest.AddVertex(Skinned? PosSkin[A.P] : Pos[A.P],
						Skinned? PosSkin[A.P].Bind : dummy,
						Nrm.GetCount()? Nrm[A.N] : dummyN,
						Cpv.GetCount()? Cpv[A.C0] : dummyCPV,
						dummyCPV,	// no old data supports second cpv set
						dummyCPV,	// no old data supports third cpv set
						Tex1.GetCount()? 2 : Tex0.GetCount()? 1 : 0,tcs);
					destP.Idx.Append() = newVertex;
				}
			}
		}
		// This model is gonna be a mess, so clean it now.
		CleanModel();
		Tristrip();
	}
	else 
#else
	#error "It's not safe to turn off COMPATIBLE, the code in this block has severely rotted."
	u8 Version = 255;
	SERIALIZE(S,Version);

	S.BeginBlock();
	SERIALIZE(S,Skinned);
#endif
	{
		if (Version >= 9)
		{
			SERIALIZE(S, BlendTargetCount);
			mshVertex::mshBlendTargetCount = BlendTargetCount;
		}
		mshVertex::mshMeshVersion = Version;
		SERIALIZE(S,Mtl);
	}
	SERIALIZE(S,Offset);
	if (Version >= 6) {
		SERIALIZE(S,Sphere);
		SERIALIZE(S,BoxMatrix.a);
		SERIALIZE(S,BoxMatrix.b);
		SERIALIZE(S,BoxMatrix.c);
		SERIALIZE(S,BoxMatrix.d);
		SERIALIZE(S,BoxSize);
	}
	if (Version >= 7)
	{
		for (int i=0; i<mshMaxFloatBlindData; i++) 
			::rage::Serialize(S,FloatBlindDataIds[i]);
	}
	if (Version >= 9)
	{
		for( int i = 0; i < BlendTargetCount; i++ )
			::rage::Serialize(S, BlendTargetNames[i]);
	}

	S.EndBlock();

	// hack to force skinned model to unskinned
	// Skinned = false;
	// Offset.Reset();
}


void Serialize(mshSerializer &S,mshMesh &v) {
	v.Serialize(S);
}



/*
	PURPOSE
		Converts a model-relative model to a bone-relative model by subtracting out
		the bone offsets.  Does *not* modify the skin weights to be cumulative instead
		of global, that needs to be done by lower-level code.
*/
void mshMesh::MakeBoneRelative() {
	if (!Skinned || !Offset.GetCount())
		return;

	for (int i=0; i<Mtl.GetCount(); i++)
		for (int j=0; j<Mtl[i].GetVertexCount(); j++) {
			if( Mtl[i].GetBinding(j).IsPassThrough == false )
			{
				Vector3 v = Mtl[i].GetPos(j);
				v.Subtract(Offset[Mtl[i].GetBinding(j).Mtx[0]]);
				Mtl[i].SetPos(j,v);
			}
		}
}

/*
	PURPOSE
		Compute smooth vertex normals for each vertex in the mesh
*/
void mshMesh::ComputeNormals() {
	for (int i=0; i<Mtl.GetCount(); i++)
		Mtl[i].ComputeNormals();
}


/*
	PURPOSE
		Compute tangent and binormal (TanS0 and TanT0 in our nomenclature)
		for each vertex in the mesh.
*/
void mshMesh::ComputeTanBi(int set) 
{
	for (int i=0; i<Mtl.GetCount(); i++)
		Mtl[i].ComputeTanBi2(set);
}


int mshMesh::Weld() {
	int result = 0;
	for (int i=0; i<Mtl.GetCount(); i++)
		result += Mtl[i].Weld();
	return result;
}

#if __TOOL || __RESOURCECOMPILER
int mshMesh::WeldWithoutQuantize() {
	int result = 0;
	for (int i=0; i<Mtl.GetCount(); i++)
		result += Mtl[i].WeldWithoutQuantize();
	return result;
}
#endif

void mshMesh::ComputeBoundingBox(Vector3 &outMin,Vector3 &outMax) const {
	outMin.Set(FLT_MAX, FLT_MAX, FLT_MAX);
	outMax.Set(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	for (int i=0; i<Mtl.GetCount(); i++) {
		for (int j=0; j<Mtl[i].GetVertexCount(); j++) {
			outMin.Min(outMin, Mtl[i].GetPos(j));
			outMax.Max(outMax, Mtl[i].GetPos(j));
		}
	}
}

void mshMesh::SnapToPlane(const Vector4& plane, float tolerance) {
	for(int i = 0; i < Mtl.GetCount(); i++) {
		for (int j = 0; j < Mtl[i].GetVertexCount(); j++) {

			const Vector3& pos = Mtl[i].GetPos(j);
			float dist = plane.DistanceToPlane(pos);

			if (fabs(dist) < tolerance) {
				Vector3 planeNormal;
				plane.GetVector3(planeNormal);
				planeNormal.Scale(dist);
				Vector3 newPos = pos - planeNormal;
				Mtl[i].SetPos(j,newPos);
			}
		}
	}
}

void mshMesh::ClearVtxAttributes() {
	for(int i = 0; i < Mtl.GetCount(); i++) {
		for(int j = 0; j < Mtl[i].GetVertexCount(); j++) {
			mshVertex vtx = Mtl[i].GetVertex(j);
			vtx.Cpv.Set(1.0f);
			vtx.Cpv2.Set(1.0f);
			vtx.Cpv3.Set(1.0f);
			vtx.Binding.Reset();
			int k;
			for(k = 0; k < vtx.TexCount; k++) {
				vtx.Tex[k].Zero();
			}
			vtx.TexCount = 0;
			for(k = 0; k < vtx.TanBiCount; k++) {
				vtx.TanBi[k].B.Zero();
				vtx.TanBi[k].T.Zero();
			}
			vtx.TanBiCount = 0;
			vtx.Nrm.Set(0.0f, 1.0f, 0.0f);
			Mtl[i].SetVertex(j,vtx);
		}
	}
}

bool mshMaterial::IsSingleBoneSkinned() const {
	for(int j = 0; j < GetVertexCount(); j++) {
		if ((GetVertex(j).Binding.IsPassThrough == false) && (GetVertex(j).Binding.Wgt[0] < 0.95f))
			return false;
	}
	return true;
}

bool mshMesh::IsSingleBoneSkinned() const {
	// If it's not skinned at all, it's not single-bone skinned
	if (!Skinned)
		return false;
	for(int i = 0; i < Mtl.GetCount(); i++) {
		if (!Mtl[i].IsSingleBoneSkinned())
			return false;
	}
	
	// All vertices have at least 95% weighting in the first vertex.
	return true;
}

}	// namespace rage

#endif	// MESH_LIBRARY
