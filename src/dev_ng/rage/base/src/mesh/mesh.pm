# Loading and Examining ASCII .MESH FILES
# This Module allows loading .MESH file, both Binary and ASCII.
# actually if its a binary mesh file, it will be converted to ascii.
# Once it is loaded, it will stick all the data in a perl Hash 
# which can be traversed normally.
# by Robert Suh

use strict;
package mesh;

sub new() 
{
   my $packageName   = shift;
   my $this = {};       # allocate a hash table to hold class variables
   bless $this;
   return $this;
}

###############################################################################
#                 LOAD .MESH file
#
# my $meshObject = new mesh();
# $meshObject->loadFile('c:\agent\assets\rob.mesh');
#
# binary and ascii mesh files are accepted.
###############################################################################

# just like load, but performs the super shadow, and statistics checks.
sub loadFileCache($) 
{
   my $this = shift;
   my $filename = shift;

}

sub loadFile($) 
{
   my $this = shift;
   my $filename = shift;
   open (FILE, $filename);
   my @data = <FILE>;
   close (FILE);     

   $this->{Pos}     = $this->ripSimple("Pos", \@data);
   $this->{Cpv}     = $this->ripSimple("Cpv", \@data);
   $this->{Nrm}     = $this->ripSimple("Nrm", \@data);
   $this->{Tex0}    = $this->ripSimple("Tex0", \@data);
   $this->{Tex1}    = $this->ripSimple("Tex1", \@data);
   $this->{Skinned} = $this->ripSimple("Skinned", \@data);
   $this->{PosSkin} = $this->ripSimple("PosSkin", \@data);
   $this->{Offset}  = $this->ripSimple("Offset", \@data);
   $this->{Adj}     = $this->ripAdj(\@data);
   $this->{Mtl}     = $this->ripMtl(\@data);
}

sub ripMtl($) 
{
   my $this = shift;
   my $tag = "Mtl";
   my $arrayPtr = shift;
   my $inside = 0;
   my @output;
   my %hash;
   my $indent=0;

   my @materialList;
   my %materialHash;
   my $materialHashPtr = undef;
   my $primHashPtr=undef;

   my $lastPush = 0;

   #print "ripping material\n";

   foreach my $line (@$arrayPtr)
   {
      $line = $this->stringClean($line);
      if ($inside==1) 
      {
         if ($line =~ m/Idx\s+\d+/i) 
         {
            #print "[$indent] INDEX ($line)\n";
            $line =~ m/{(.+)}/ig;
            my $indices = $1;
            $indices = $this->stringClean($indices);
            my @indexArray = split(/\s+/, $indices);
            $primHashPtr->{Idx} = \@indexArray;
            if ($materialHashPtr->{Prim} eq undef) 
            {
               my @array;
               $materialHashPtr->{Prim} = \@array;
            }
            my $arrayPtr = $materialHashPtr->{Prim};
            push (@$arrayPtr, $primHashPtr);
            $primHashPtr = undef;
         }
         if ($line =~ m/}/i) 
         {
            $indent--;
             #print "INDENT OUT ($indent)\n";
            if ($indent<0) 
            {
               $inside=0;
            }
         }
         else
         {
            $line = $this->stringClean($line);
            if (1)#(($indent == 1) || ($indent==3)) 
            {
               #print "[$indent] $line\n";
            }
            if ($indent==1) 
            {
               my @list = split(/\s+/, $line);
               my $indexName = $list[0];
               my $value     = $list[1];
               $value =~ s/"//ig;
             # print "[[$indent]] ($indexName) = ($value)\n";

               if ($indexName =~ m/Name/i) 
               {
                  $materialHashPtr = undef;
                 # print "Clearning materialHahsPtr\n";
               }
               if ($materialHashPtr eq undef) 
               {
                  my %hash;
                  $materialHashPtr = \%hash;
                  $lastPush++;
               }
               if ($indexName =~ m/Prim/i) 
               {
                 # print "BUMP-------------------------------------------------\n";
                  #print Dumper $materialHashPtr;
                  push (@materialList, $materialHashPtr);
                  $lastPush--;
                  #$materialHashPtr = undef;
               }
               else
               {
                  if ($indexName ne undef) 
                  {
                     $materialHashPtr->{$indexName} = $value;
                  }

               }
            }
            if ($indent==3) 
            {
               my @list = split(/\s+/, $line);
               my $indexName = $list[0];
               my $value     = $list[1];
               $value =~ s/"//ig;
               #print "[[$indent]] ($indexName) = ($value)\n";
               if ($primHashPtr eq undef) 
               {
                  my %hash;
                  $primHashPtr = \%hash;
               }
               $primHashPtr->{$indexName} = $value;

            }
         }
      }
      if ($inside) 
      {
         if ($line =~ m/{/) 
         {
            $indent++;
           # print "INDENT IN ($indent)\n";
         }
      }
      if ($line=~ m/$tag\s+(\d+)\s+{/i) 
      {
         if ($1 == "0") 
         {
            my @out;
            return (@out); #empty;
         }
         $inside = 1;
      }
   }

   if ($lastPush>0) 
   {
      #print "LAST PUSH ($lastPush)\n";
      #print Dumper $materialHashPtr;
      push (@materialList, $materialHashPtr);
   }


   my $total = @materialList;
  # print "($tag) = ($total)\n";
   use Data::Dumper;
   my $counter=0;
   foreach my $materialHashPtr (@materialList)
   {
      my $primListPtr = $materialHashPtr->{Prim};
      foreach my $primPtr (@$primListPtr)
      {
   #      print Dumper $primPtr;
         $counter++;
      }
   }
#   my $hashPtr = $materialList[0];
#   print Dumper $hashPtr;
   
#   print "COUNTER ($counter)\n";
   
   return \@materialList;

}

sub ripAdj($) 
{
   my $this = shift;
   my $tag = "Adj";
   my $arrayPtr = shift;
   my $inside = 0;
   my @output;
   my $hashPtr = undef;

   foreach my $line (@$arrayPtr)
   {
      if ($inside==1) 
      {
         if ($line =~ m/\s+}/i) 
         {
            $inside=0;
         }
         else
         {
         #   print $line;
            $line = $this->stringClean($line);
            my @list = split(/\s+/, $line);
            my $indexName = $list[0];
            my $value     = $list[1];
          #  print "($indexName) = ($value)\n";
            if ($hashPtr eq undef) 
            {
               my %hash;
               $hashPtr = \%hash;
            }
            $hashPtr->{$indexName} = $value;
            if ($indexName =~ m/T0/i) 
            {
               push (@output, $hashPtr);
               $hashPtr=undef;
            }
         }
      }
      if ($line=~ m/$tag\s+(\d+)\s+{/i) 
      {
         if ($1 == "0") 
         {
            my @out;
            return (@out); #empty;
         }
         $inside = 1;
      }
   }

   my $total = @output;
   #print "($tag) = ($total)\n";
   return \@output;

}

sub ripSimple($$) 
{
   my $this = shift;
   my $tag = shift;
   my $arrayPtr = shift;
   my $inside = 0;
   my @output;
   foreach my $line (@$arrayPtr)
   {
      if ($inside==1) 
      {
         if ($line =~ m/\s+}/i) 
         {
            $inside=0;
         }
         else
         {
         #   print $line;
            chomp($line);
            $line = $this->stringClean($line);
            push (@output, $line);
         }
      }
      if ($line=~ m/$tag\s+(\d+)\s+{/i) 
      {
         if ($1 == "0") 
         {
            my @out;
            return (@out); #empty;
         }
         $inside = 1;
      }
   }

   my $total = @output;
   #print "($tag) = ($total)\n";
   return \@output;
}

sub stringClean($) 
{
   my $this = shift;
   my $string = shift;

   chomp($string);
   $string =~ s/^\s+//ig;
   $string =~ s/\s+$//ig;
   $string =~ s/\s+/ /ig;
   return $string;
}

sub getPosSkinList() 
{
   my $this = shift;
   my $posSkinListPtr = $this->{PosSkin};
   return @$posSkinListPtr;
}
sub getPosSkinTotal() 
{
   my $this = shift;
   my @list = $this->getPosSkinList();
   my $total = @list;
   return $total;
}


sub getPosList() 
{
   my $this = shift;
   my $posListPtr = $this->{Pos};
   return @$posListPtr;
}

sub getPosTotal() 
{
   my $this = shift;
   my @list = $this->getPosList();
   my $total = @list;
   return $total;
}
sub getNrmList() 
{
   my $this = shift;
   my $nrmListPtr = $this->{Nrm};
   return @$nrmListPtr;
}
sub getNrmTotal() 
{
   my $this = shift;
   my @list = $this->getNrmList();
   my $total = @list;
   return $total;
}
sub getCpvList() 
{
   my $this = shift;
   my $cpvListPtr = $this->{Cpv};
   return @$cpvListPtr;
}
sub getCpvTotal() 
{
   my $this = shift;
   my @list = $this->getCpvList();
   my $total = @list;
   return $total;
}

sub getTex0List() 
{
   my $this = shift;
   my $tex0ListPtr = $this->{Tex0};
   return @$tex0ListPtr;
}
sub getTex0Total() 
{
   my $this = shift;
   my @list = $this->getTex0List();
   my $total = @list;
   return $total;
}
sub getTex1List() 
{
   my $this = shift;
   my $tex1ListPtr = $this->{Tex1};
   return @$tex1ListPtr;
}
sub getTex1Total() 
{
   my $this = shift;
   my @list = $this->getTex1List();
   my $total = @list;
   return $total;
}

sub getAdjList() 
{
   my $this = shift;
   my $adjListPtr = $this->{Adj};
   return @$adjListPtr;
}
sub getAdjTotal() 
{
   my $this = shift;
   my @list = $this->getAdjList();
   my $total = @list;
   return $total;
}

sub getMaterialList() 
{
   my $this = shift;
   my $materialListPtr = $this->{Mtl};
   return @$materialListPtr;
}
sub getMaterialTotal() 
{
   my $this = shift;
   my @list = $this->getMaterialList();
   my $total = @list;
   return $total;
}

sub getMaterialNames() 
{
   my $this = shift;
   my @list = $this->getMaterialList();
   my $totalMaterials = $this->getMaterialTotal();
   #print "-------- TOTAL ($totalMaterials)\n";
   my @nameList;
   foreach my $materialPtr (@list)
   {
      my $name = $materialPtr->{Name};
      #print "MATERIAL NAME ($name)\n";
      push (@nameList, $name);
    #  print Dumper $materialPtr;
   }
   return @nameList;
}

sub createStripLengthHistogram() 
{
   my $this = shift;
   my %hash;
   my @materialList = $this->getMaterialList();
   foreach my $materialPtr (@materialList)
   {
      my $primListPtr = $materialPtr->{Prim};
      my @list = @$primListPtr;
      my $total = @list;
     # print "TOtal Prims ($total)\n";
      foreach my $primHashPtr (@list)
      {
        # print Dumper $primHashPtr;
         my $indexListPtr = $primHashPtr->{Idx};
         my @idxList = @$indexListPtr;
         my $total = @idxList;
        # print "Total Indices in prim ($total)\n";
         if ($hash{$total} eq undef) 
         {
            $hash{$total} = 1;   
         }
         else
         {
            $hash{$total} ++;
         }
      }
   }
   #print Dumper %hash;
   my @keyList = keys %hash;
   my $primCount=0;
   my $primMin = undef;
   my $primMax = undef;
   foreach my $primLength (@keyList)
   {
      my $pc = $hash{$primLength};
      if ($primMin eq undef) 
      {
         $primMin = $primLength;
         $primMax = $primLength;
      }
      if ($primLength < $primMin) 
      {
         $primMin = $primLength;
      }
      if ($primLength > $primMax) 
      {
         $primMax = $primLength;
      }
      $primCount += $pc;
   }
   #print "PRIM COUNT ($primCount)\n";
   return ($primCount, $primMin, $primMax, \%hash);
   
}


# pass in two 2d points, returns the distance
sub distance2d($$) 
{
   my $this = shift;
   my $point1 = shift;
   my $point2 = shift;

   my @array1 = split(/\s+/, $point1);
   my @array2 = split(/\s+/, $point2);

   my $deltaA = $array1[0] - $array2[0];
   my $deltaB = $array1[1] - $array2[1];

   my $distance = sqrt( ($deltaA * $deltaA) + ($deltaB * $deltaB));

   return $distance;
}
# pass in two 3d points, returns the distance
sub distance3d($$) 
{
}

sub findAlmostFaceMappedQuads() 
{
   my $this = shift;
   my $tex0ListPtr = $this->{Tex0};
   my $adjListPtr  = $this->{Adj};
   my @materialList = $this->getMaterialList();
   my $nearMappedCount=0;
   my $fullMappedCount=0;
   my $overMappedCount=0;
   foreach my $materialHashPtr (@materialList)
   {
      my $primListPtr = $materialHashPtr->{Prim};
      foreach my $primHashPtr (@$primListPtr)
      {
         my $indexListPtr = $primHashPtr->{Idx};
         my $totalIndices = @$indexListPtr;

         my $lastPoint=undef;
         if ($totalIndices == 4) 
         {
            #print "QUAD-\n";
            my @texList;
            foreach my $index (@$indexListPtr)
            {
               my $adjunctPtr = $adjListPtr->[$index];
               my $tex0Index  = $adjunctPtr->{T0};
               my $tex        = $tex0ListPtr->[$tex0Index];
             #  print "INDEX [$index] tex0Index [$tex0Index] [$tex]\n";
               push (@texList, $tex);
            }
            my $distance1 = $this->distance2d( $texList[0], $texList[1]);
            my $distance2 = $this->distance2d( $texList[0], $texList[2]);
            my $distance3 = $this->distance2d( $texList[2], $texList[3]);
            my $distance4 = $this->distance2d( $texList[1], $texList[3]);
            #print "DIST ($distance1) ($distance2) ($distance3) ($distance4)\n";
            if (($distance1 > 0.9) && ($distance1 < 1.0)) 
            {
               if (($distance2 > 0.9) && ($distance2 < 1.0)) 
               {
                  if (($distance3 > 0.9) && ($distance3 < 1.0)) 
                  {
                     if (($distance4 > 0.9) && ($distance4 < 1.0)) 
                     {
                     #   print "LOOKY!\n";
                        $nearMappedCount++;
                     }
                  }
               }
            }
            if (($distance1 > 1.0) && ($distance3 > 1.0)) 
            {
               if ($distance2 == $distance4) 
               {
                  $overMappedCount++;
               }
            }
            else
            {
               if (($distance2 > 1.0) && ($distance4 > 1.0)) 
               {
                     if ($distance1 == $distance3) 
                     {
                        $overMappedCount++;
                     }
               }
            }

            if (($distance1 == 1.0)) 
            {
               if (($distance2 == 1.0)) 
               {
                  if (($distance3 == 1.0)) 
                  {
                     if (($distance4 == 1.0)) 
                     {
                     #   print "LOOKY!\n";
                        $fullMappedCount++;
                     }
                  }
               }
            }

         }
      }
   }
   return ($nearMappedCount, $fullMappedCount, $overMappedCount);
}

# {  #14 => 23 }
sub getPrimsPerMaterial() 
{
   my $this = shift;
   my %hash;
   my @materialList = $this->getMaterialList();
   foreach my $materialHashPtr (@materialList)
   {
      my $materialName = $materialHashPtr->{Name};
      #print Dumper $materialHashPtr;
      my $primListPtr = $materialHashPtr->{Prim};
      my $totalPrims = @$primListPtr;
      if ($hash{$materialName} eq undef) 
      {
         $hash{$materialName} =0;
      }
      $hash{$materialName}++;
   }
   return %hash;
}
# returns an array, ACCUMULATED INDICES, TOTAL PRIMS, AVERAGE INDICES PER PRIM
sub calculatePrimData() 
{
   my $this = shift;
   my $accumulator = 0;
   my $counter=0;
   my $triangle_count=0;
   my @materialList = $this->getMaterialList();
   foreach my $materialHashPtr (@materialList)
   {
      my $primListPtr = $materialHashPtr->{Prim};
      foreach my $primHashPtr (@$primListPtr)
      {
         my $indexListPtr= $primHashPtr->{Idx};
         my $totalIndices = @$indexListPtr;
         $triangle_count+= ($totalIndices-2);
         $accumulator += $totalIndices;
         $counter ++;
      }
   }
   if ($counter==0) 
   {
      $counter=1;
   }
   my $average = $accumulator/$counter;
  # print "Average Length ($average) ($counter) ($triangle_count)\n";
   return ($accumulator, $counter, $average, $triangle_count);
}

sub printSimpleReport() 
{
   my $this = shift;
   #print "Total Pos (".$this->getPosTotal().")\n";
   #print "Total Nrm (".$this->getNrmTotal().")\n";
   #print "Total Cpv (".$this->getCpvTotal().")\n";
   my ($accumulator, $counter, $average, $triangle_count) = $this->calculatePrimData();
   my $looky  = $this->findAlmostFaceMappedQuads();
   #print "Total ALMOST Quad ($looky)\n";
   my $percent;
   if ($looky > 0) 
   {
      $percent = ($looky / $counter)*100;
      $percent = sprintf("%3.2f", $percent);
      #print "$looky Quads and [$percent]\% of primitives are NEAR face mapped quads! fix me!\n";
   }
   return ($accumulator, $counter, $average, $triangle_count, $looky, $percent);
}

###############################################################################
#                 Messages On Nodes
###############################################################################

sub addStatus($) 
{
   my $this = shift;
   my $message = shift;
   if ($this->{status} eq undef) 
   {
      my @array;
      $this->{status} = \@array;
   }
   my $arrayPtr = $this->{status};
   push (@$arrayPtr, $message);

   print "$message\n";

}

sub addError($) 
{
   my $this = shift;
   my $message = shift;
   if ($this->{error} eq undef) 
   {
      my @array;
      $this->{error} = \@array;
   }
   my $arrayPtr = $this->{error};
   push (@$arrayPtr, $message);
   $this->addStatus("!ERROR! $message");
}

sub addWarning($) 
{
   my $this = shift;
   my $message = shift;
   if ($this->{warning} eq undef) 
   {
      my @array;
      $this->{warning} = \@array;
   }
   my $arrayPtr = $this->{warning};
   push (@$arrayPtr, $message);
   $this->addStatus("!WARNING! $message");
}

# given a mesh, it will give out a hash of primtypes
# mshTriangle => 3
# mshTristrip => 10
# mshTristrip2=> 33

sub getPrimTypeStats() 
{
   my $this = shift;
   my $counter=0;
   my %hash;
   my @materialList = $this->getMaterialList();
   foreach my $materialHashPtr (@materialList)
   {
      my $primListPtr = $materialHashPtr->{Prim};
      foreach my $primHashPtr (@$primListPtr)
      {
         my $indexListPtr = $primHashPtr->{Idx};
         my $primType     = $primHashPtr->{Type};
         my $totalIndices = @$indexListPtr;
         if ($hash{$primType} eq undef) 
         {
            $hash{$primType} = 0;
         }
         $hash{$primType}++;
      }
   }
   #print Dumper %hash;
   return %hash;
}

# there should NEVER be any duplicates!

###############################################################################
#                 Scan mesh data for duplicates..
###############################################################################


sub searchForDuplicates() 
{
   my $this = shift;
   my @list = $this->getPosList();
   if (@list) 
   {
      my %hash;
      my $count=0;
      foreach (@list)
      {
         if ($hash{$_} eq undef) 
         {
            $hash{$_} = 1;
         }
         else
         {
            # duplicate found!
            $hash{$_}++;
            $count++;
         }
      }
      if ($count > 0) 
      {
         $this->addWarning("Duplicate Position Entries found");
      }
   }
   my @list = $this->getCpvList();
   if (@list)
   {
      my %hash;
      my $count=0;
      foreach (@list)
      {
         if ($hash{$_} eq undef) 
         {
            $hash{$_} = 1;
         }
         else
         {
            # duplicate found!
            $hash{$_}++;
            $count++;
         }
      }
      if ($count > 0) 
      {
         $this->addWarning("Duplicate CPV Entries found");
      }
   }
   my @list = $this->getNrmList();
   if (@list)
   {
      my %hash;
      my $count=0;
      foreach (@list)
      {
         if ($hash{$_} eq undef) 
         {
            $hash{$_} = 1;
         }
         else
         {
            # duplicate found!
            $hash{$_}++;
            $count++;
         }
      }
      if ($count > 0) 
      {
         $this->addWarning("Duplicate Normal Entries found");
      }
   }
   my @list = $this->getTex0List();
   if (@list) 
   {
      my %hash;
      my $count=0;
      foreach (@list)
      {
         if ($hash{$_} eq undef) 
         {
            $hash{$_} = 1;
         }
         else
         {
            # duplicate found!
            $hash{$_}++;
            $count++;
         }
      }
      if ($count > 0) 
      {
         $this->addWarning("Duplicate Texture0 Entries found");
      }
   }
   my @list = $this->getTex1List();
   if (@list)
   {
      my %hash;
      my $count=0;
      foreach (@list)
      {
         if ($hash{$_} eq undef) 
         {
            $hash{$_} = 1;
         }
         else
         {
            # duplicate found!
            $hash{$_}++;
            $count++;
         }
      }
      if ($count > 0) 
      {
         $this->addWarning("Duplicate Texture1 Entries found");
      }
   }
}

sub generateStatHash()
{
   my $this = shift;
   my %hash;
   $hash{TotalPosSkin} = $this->getPosSkinTotal();
   $hash{TotalPos} = $this->getPosTotal();
   $hash{TotalNrm} = $this->getNrmTotal();
   $hash{TotalCpv} = $this->getCpvTotal();
   $hash{TotalAdj} = $this->getAdjTotal();
   $hash{TotalTex0}= $this->getTex0Total();
   $hash{TotalTex1}= $this->getTex1Total();
   my ($accumulator, $counter, $average, $triangle_count) = $this->calculatePrimData();
   $hash{TriangleCount} = $triangle_count;
   $hash{AverageTristripLength} = $average;

   my ($nearMappedQuadCount, $fullMappedQuadCount, $overMappedQuadCount)  = $this->findAlmostFaceMappedQuads();
   #print "Total ALMOST Quad ($nearMappedQuadCount)\n";
   my $percent=0;
   if ($nearMappedQuadCount > 0) 
   {
      $percent = ($nearMappedQuadCount / $counter)*100;
      $percent = sprintf("%3.2f", $percent);
     # print "$nearMappedQuadCount Quads and [$percent]\% of primitives are NEAR face mapped quads! fix me!\n";
   }
   $hash{TotalNearMappedQuads} = $nearMappedQuadCount;
   $hash{TotalFullMappedQuads} = $fullMappedQuadCount;
   $hash{TotalOverMappedQuads} = $overMappedQuadCount;
   $hash{PercentOfQuadsWhichAreNearMapped} = $percent;
   my $percent=0;
   if ($fullMappedQuadCount > 0) 
   {
      $percent = ($fullMappedQuadCount / $counter)*100;
      $percent = sprintf("%3.2f", $percent);
      #print "$fullMappedQuadCount Quads and [$percent]\% of primitives are FULLY face mapped quads! fix me!\n";
   }
   $hash{PercentOfQuadsWhichAreFullMapped} = $percent;
                        
   my $percent=0;
   if ($overMappedQuadCount > 0) 
   {
      $percent = ($overMappedQuadCount / $counter)*100;
      $percent = sprintf("%3.2f", $percent);
     # print "$overMappedQuadCount Quads and [$percent]\% of primitives are OVER face mapped quads! fix me!\n";
   }
   $hash{PercentOfQuadsWhichAreOverMapped} = $percent;

   my ($primCount, $primMin, $primMax, $histogramHashPtr) = $this->createStripLengthHistogram();

   $hash{TotalPrimitives} = $primCount;
   $hash{MinTristripLength} = $primMin;
   $hash{MaxTristripLength} = $primMax;
   $hash{StripHistogram}  = $histogramHashPtr;

   my @list = $this->getMaterialNames();
   $hash{MaterialNameList} = \@list;
   $hash{TotalMaterial} = $this->getMaterialTotal();

   $this->searchForDuplicates();

   $hash{warning} = $this->{warning};
   $hash{error}   = $this->{error};
   $hash{status}  = $this->{status};

   my %primTypeHash = $this->getPrimTypeStats();
   $hash{PrimTypeHistogram} = \%primTypeHash;

   my %primsPerMaterialHash= $this->getPrimsPerMaterial();
   $hash{MaterialHistogram} = \%primsPerMaterialHash;

   return \%hash;
}





###############################################################################
1;

