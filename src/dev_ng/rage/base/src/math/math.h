//
// math/math.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef MATH_MATH_H
#define MATH_MATH_H

#include "channel.h"

#include "nan.h"
#include "constants.h"

#include "amath.h"
#include "simplemath.h"			
#include "random.h"

#include "float16.h"
#include "intrinsics.h"
#include "altivec2.h"
#include "vecmath.h"
#include "vecrand.h"

#endif // MATH_MATH_H