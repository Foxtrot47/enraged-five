Project RageGraphics
RootDirectory ..\..\..\..\..\

ParserIgnore

IncludePath ..\..;$(RAGE_DIR)\3rdParty
Directory {
	..\..\output
	..\..\crmetadata
	..\..\crskeleton
	..\..\edge
	..\..\grblendshapes
	..\..\grcore
	..\..\grmodel
	..\..\grprofile
	..\..\input
	..\..\jpeg
	..\..\mesh
	..\..\rmcore
	..\..\shaderlib
	..\..\spatialdata
}

Libraries {
	%RAGE_DIR%\base\src\rage_lib_psc\rage_lib_psc
}
