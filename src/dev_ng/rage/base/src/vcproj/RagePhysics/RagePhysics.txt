Project RagePhysics
RootDirectory ..\..\..\..\..\

ParserIgnore

IncludePath ..\..
Directory {
	..\..\curve
	..\..\physics
	..\..\pharticulated
	..\..\phbound
	..\..\phbullet
	..\..\phcore
	..\..\pheffects
	..\..\phsolver
}

Libraries {
	%RAGE_DIR%\base\src\rage_lib_psc\rage_lib_psc
}
