
#ifndef __UNITYBUILD
# define __UNITYBUILD
#endif //
#include "forceinclude/_unity_prologue.h"
#include "../../../audioasiolib/asio.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioasiolib/asiodrivers.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioasiolib/asiolist.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audiodata/container.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audiodata/containerbuilder.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/ambisonics.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/category.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/categorymanager.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/categorycontroller.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/categorycontrollermgr.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/compressedvolume.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/controller.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/curve.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/curverepository.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/dynamicmixmgr.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/dynamixdefs.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/engine.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/engineconfig.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/engineutil.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/entityvariablecache.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/environment.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/environmentgroup.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/environment_game.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/math.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/metadatamanager.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/soundfactory.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/soundmanager.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/soundmanager_update.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/soundpool.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/soundset.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/speechdefs.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/tempomap.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/tracker.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/remotecontrol.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/remotedebug.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/requestedsettings.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/widgets.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/ambisonicoptimizerjob.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/categoryupdatejob.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/categorycontrollerjob.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/environmentupdatejob.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../../audioengine/soundupdatejob.cpp"
#include "forceinclude/_unity_epilogue.h"
