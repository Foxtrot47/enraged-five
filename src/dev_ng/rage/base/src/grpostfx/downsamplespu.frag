#include "system/codefrag_spu.h"
#include "grpostfx/downsample.cpp"

SPUFRAG_DECL(void, downsamplespu, EdgePostTileInfo*);
SPUFRAG_IMPL(void, downsamplespu, EdgePostTileInfo* tileInfo)
{
	edgePostMain(tileInfo);
}
