// 
// grpostfx/postfx.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Post-processing effects
//
//  
// 

#include "postfx.h"

#include "atl/array.h"
#include "atl/string.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/button.h"
#include "bank/color.h"
#include "bank/combo.h"
#include "bank/slider.h"
#include "diag/tracker.h"
#include "file/asset.h"
#include "grcore/device.h"
#include "grcore/im.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "grcore/effect_values.h"
#include "grmodel/shader.h"
#include "grmodel/shaderfx.h"
#include "grmodel/shadergroup.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "rmcore/drawable.h"
#include "shaderlib/rage_constants.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "grprofile/pix.h"


#if __PPU
#include "grcore/wrapper_gcm.h"
#include "grcore/texturegcm.h"
#endif // __PPU

#if __XENON
#include "grcore/texturexenon.h"
#include "embedded_rage_postfx_fxl_final.h"
#elif __PPU
#include "embedded_rage_postfx_psn.h"
#elif __WIN32PC
#include "embedded_rage_postfx_win32_30.h"
#endif

#if __XENON
#include "system/xtl.h"
#include "system/d3d9.h"
#endif

// PostFX is designed to be tweaked at the project level and trying to get all of the permutations
// of variables used or not on the different platforms was getting out of hand.
#define LOOKUPVAR(name)	LookupVar(name,false)


using namespace rage;

grPostFX *grPostFX::sm_instance = NULL;

static Color32 white(1.0f,1.0f,1.0f,1.0f);


const char *grPostFX::sm_DefaultTexturePath = NULL;

// this is the default now
#define Z_BUFFERDEPTH 1

#define DEVELOPERONLY 0

PARAM(hdr, "[grpostfx] Set the whole rendering pipeline to 10-bit fp on 360 and 16-bit on PC");

#if __PPU
PARAM(edgepost, "[grpostfx] Use EDGE Post");
namespace rage
{
	XPARAM(srgb);
	XPARAM(bb720pwidth);
	XPARAM(bb720pheight);
} // namespace rage
#endif

#if __WIN32PC
XPARAM(ati);
bool grPostFX::m_RenderTargetActive = false;
#endif
grPostFX::grcPPPPreset::grcPPPPreset()
{
#if __PPU
	// for testing purposes
	m_BrightPassThreshold = 0.2f;
	m_BrightPassOffset = 0.07f;
	m_BrightPassThresholdTwo = 0.2f;
	m_BrightPassOffsetTwo = 0.07f;
#else
	m_BrightPassThreshold = 0.2f;
	m_BrightPassOffset = 0.07f;
	m_BrightPassThresholdTwo = 0.2f;
	m_BrightPassOffsetTwo = 0.07f;
#endif

	m_MiddleGray = 0.18f;
	m_MiddleGrayTwo = 0.18f;

	m_fAdapt = 2.0f;

	// correct gamma for the equation below
#if __WIN32PC
	m_fGamma = 1.0f/2.4f;
#else
	m_fGamma = 1.0f/2.4f;
#endif

	m_ClearColor = Vector4(0.0f, 0.0f, 1.0f, 1.0f);
	m_ColorCorrect = Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	m_ConstAdd = Vector4(0.0f,0.0f,0.0f,0.0f);	
	m_deSat = 1.0f;	
	m_deSatTwo = 1.0f;	
	m_Scale = Vector3(1.0f, 0.0f, 0.0f);
	m_fWhite = 1.0f;
	m_fBrightPassWhite = 1.0f;
	m_fBrightPassWhiteTwo = 1.0f;
	m_fContrast = 0.0f;
	m_fContrastTwo = 0.0f;
	m_iNumOfFramesInLumHistory = 16;

	// police cam parameters
	m_fGrainIntensity = 0.02f;
	m_fInterlaceIntensity = 2.5f;
	m_fTimeFrame = 1.0f;
	m_fInterference = 0.002f;

	// RDR2 parameters
	m_fDeath = 0.0f;
	m_fDeathDrip = 0.0f;
	m_fDeathBlood = 0.0f;
	m_fDeathColor = Vector4(0.7f, 0.0f, 0.0f, 1.0f);

	// limit light adaption to a certain value range
	m_fLowerLimitAdaption = 0.15f;
	m_fHigherLimitAdaption = 0.8f;

	// fog parameters
	m_FogMinColor = Vector4(1.0f, 1.0f, 1.0f, 0.0f);
	m_FogMaxColor = Vector4(0.5f, 0.5f, 0.5f, 0.8f);
	m_FogParams =   Vector4(10.0f, 0.0f, 0.2f, 10000.0f);
	m_BrightPassParams = Vector4(0.0f, 0.9f, 0.0f, 0.0f);
	m_ParticleFogParams =   Vector4(0.0f, 0.0f, 30.0f, 0.3f);

	// some initial DOF parameters
	m_DofParamters.Set(0.05f, 6.0f, 0.0f, 0.1f);

#if __BANK
	m_iChooseTechnique = 0;
#endif // __BANK

	m_CurrentEffect = NULL;

	// 25-tap Gauss filter
	m_fBloomIntensity = 1.0f;

	m_BlueShiftColor.Set(1.05f, 0.97f, 1.27f,0.0f);
	m_BlueShiftParams.Set(0.0f, 1.0f, 0.0f, 0.0f);

	m_StreakSamples = 3.0f;
	for(int i=0; i<MAX_STREAK_ITERATIONS*4;i++)
	{
		m_StreakAttenuation[i] = 0.9f;
	}
	m_StreakIterations = 3;
	m_BloomIterations = 6;
	m_BloomIntensity = 1.0f/4.0f;
	m_StreakIntensity = 1.0;

	m_ShowRenderTargets = false;
	m_SwitchPoliceCam = false;
	m_NumStreaks = 2;
	m_DistortionFreq = 20.0f;
	m_DistortionScale = 0.0f;
	m_DistortionRoll = 0.0f;
	
	m_BlurEffectTintColor.Set(1.0f, 1.0f, 1.0f);
	m_BlurEffectTintFade = 0.0f;
	m_BlurEffectDesatScale = 0.0f;

#if RAGE_SSAO
#if __XENON
	m_EnableSSAO = true;
#else
	m_EnableSSAO = false;
#endif // __XENON
	m_SSAOTweakParams = Vector4(5.0f, 3.0f, 37.0f, 4.0f);
#endif // RAGE_SSAO
}

const char *grPostFX::grcPPPPreset::GetTechniqueName() const
{
	if (m_CurrentEffect)
		return m_CurrentEffect->GetTechniqueName();

	return "invalid";
}

void grPostFX::grcPPPPreset::GetChooseTechniqueName(char * buffer, const int bufferSize, const grPostFXEffect *postFXEffect) const {

	if (postFXEffect)
		safecpy(buffer, postFXEffect->GetTechniqueName(), bufferSize);
	else
		safecpy(buffer, "invalid", bufferSize);
}

#define FORCE_COLORONLY_POST_EFFECT_TECHNIQUE 0

void grPostFX::grcPPPPreset::SetChooseTechnique(const char * chooseTechniqueName) {
	int effectCount = GRPOSTFX->GetEffectCount();

#if FORCE_COLORONLY_POST_EFFECT_TECHNIQUE
	if(1)
	{
	    const int forced_effect = 0;
#if __BANK
		m_iChooseTechnique = forced_effect;
#endif // __BANK
		m_CurrentEffect = GRPOSTFX->GetEffect(forced_effect);
		return;
	}
#endif


	for (int i=0; i<effectCount; i++) {
		grPostFXEffect *effect = GRPOSTFX->GetEffect(i);

		if (effect) {
			if (strcmp(chooseTechniqueName, effect->GetTechniqueName())==0) {
				m_CurrentEffect = effect;
#if __BANK
				m_iChooseTechnique = i;
#endif // __BANK
				return;
			}
		}
	}

	Assertf(false, "Could not identify postfx technique '%s'", chooseTechniqueName);

	// If we did not find one pick no post processing
	m_CurrentEffect = GRPOSTFX->GetDefaultEffect();

#if __BANK
	m_iChooseTechnique = 0;
#endif // __BANK
}

grPostFX::grPostFX() : m_SeedBrightPassCallback(0), m_SeedFogPassCallback(0), m_PersistentBrightPassCallback(0)
{
	m_PostFXShader = NULL; // post fx shader

	m_PPPPresets = NULL;
	memset(m_PPPStack, 0, sizeof(m_PPPStack));


#if __PPU
	m_AdaptedLuminanceId = grcevNONE;
#endif // __PPU
	m_TexelSizeId = grcevNONE;
	m_ElapsedTimeId = grcevNONE;
	m_FramesID = grcevNONE;
	m_LambdaId = grcevNONE;								
	m_BrightPassValuesId = grcevNONE;
	m_WhiteId = grcevNONE; 
	m_ColorCorrectId = grcevNONE;
	m_ConstAddId = grcevNONE;
	m_deSatId = grcevNONE;
	m_ContrastId = grcevNONE;
	m_ScaleId = grcevNONE;
	m_horzTapOffsId = grcevNONE;
	m_vertTapOffsId = grcevNONE;
	m_TexelWeightId = grcevNONE;
	m_BrightPassParamsId = grcevNONE;
	m_BackBuffer = NULL;
	m_DepthBuffer = NULL;

#if __WIN32PC
	m_FSCopy = NULL;
	m_NeedToCapture = false;
	m_RenderTargetActive = false;
#endif

#if __XENON
	m_DepthBuffer360FromTiling = NULL;
	m_ColorBuffer360FromTiling = NULL;
	m_PredicatedTiling = false;
#endif

	m_DepthBuffer = NULL;
#if __PPU
	m_MRTBuffer = NULL;
#endif

	// Post processing pipeline render target handles
	m_QuarterRT = NULL;
	m_SecondaryFinalOutputRT = NULL;
	m_DepthBufferRT = NULL;
	m_128x128RT = NULL;
	m_64x64RT = NULL;
	m_16x16RT = NULL;
	m_4x4RT = NULL;
	m_LumHistoryRT = NULL;
	m_CurrentAdaptedLum1x1RT = NULL;
#if __WIN32PC
	m_LastAdaptedLum1x1RT = NULL;
#endif // __WIN32PC
	m_BrightPassRT = NULL;
	m_PersistentBrightPassRT = NULL;
	m_PrevPersistentBrightPassRT = NULL;
	m_ScratchPadRT = NULL;
	m_ScratchPadRT2 = NULL;
	m_FogMapRT = NULL;
	m_GaussRT = NULL;
	m_ScreenShot = NULL;

	m_RandomMap = NULL;
	m_LightGlowMap = NULL;
	m_Counter = 0;

	m_showGammaControl = true;		// off by default

#if __XENON
	// sets distribution of hardware shader units between the vertex and pixel shader to default
	m_CurrentPixelThreads = -1;
#endif

#if __BANK
	m_techniqueSpecificWidgets.Reserve( 15 );
	m_pBank = NULL;
#endif

	m_stacked = 0;

	m_fRHO = 3.0f;

	m_DOFBlurType = dof5tap;
	m_Effects.Reserve(16);
	RegisterDefaultEffects();
}

void grPostFX::InitShaders(const char *path)
{	
	RAGE_TRACK(grPostFx);

	m_PPPPresets = CreatePPPPreset();

	for (int x=0; x<PPP_STACK_SIZE; x++)
	{
		m_PPPStack[x] = CreatePPPPreset();
	}

	if (path!=NULL)
		ASSET.PushFolder(path);

	//
	// Create post fx shader	
	//
	m_PostFXShader = grmShaderFactory::GetInstance().Create();
	m_PostFXShader->Load("rage_postfx");

	// get render target sample stages handles
	m_FullResMapId = m_PostFXShader->LOOKUPVAR("FullResMap");
	m_RenderMapId = m_PostFXShader->LOOKUPVAR("RenderMap");
	m_RenderMapBilinearId = m_PostFXShader->LOOKUPVAR("RenderBilinearMap");
	m_RenderMapAnisoId = m_PostFXShader->LOOKUPVAR("RenderAnisoMap");
#if __PPU
	m_RenderMapGaussId = m_PostFXShader->LOOKUPVAR("RenderGaussMap");
	m_RenderMapQuincunxId= m_PostFXShader->LOOKUPVAR("RenderQuincunxMap");
#endif
	m_ToneMapId = m_PostFXShader->LOOKUPVAR("ToneMap");
	m_AdaptedLuminanceMapId = m_PostFXShader->LOOKUPVAR("AdaptedLuminanceMap");

#if __XENON
	m_VertexTextureId = m_PostFXShader->LOOKUPVAR("VertexTex");
#endif

	m_NoiseId = m_PostFXShader->LOOKUPVAR("NoiseTex");
	m_LightGlowMapId = m_PostFXShader->LOOKUPVAR("LightGlowTex");
	m_DepthMapId = m_PostFXShader->LOOKUPVAR("DepthMap");

	m_StarStreakPatternId[0] = m_PostFXShader->LOOKUPVAR("StreakMap0");
	m_StarStreakPatternId[1] = m_PostFXShader->LOOKUPVAR("StreakMap1");
	m_StarStreakPatternId[2] = m_PostFXShader->LOOKUPVAR("StreakMap2");
	m_StarStreakPatternId[3] = m_PostFXShader->LOOKUPVAR("StreakMap3");
	m_FogMapId = m_PostFXShader->LOOKUPVAR("FogMap");
	m_FogCardTextureId = m_PostFXShader->LOOKUPVAR("FogTexture");

	m_ProjInverseTransposeId = m_PostFXShader->LOOKUPVAR("WorldViewProjInverse");
	
	// Screen Distortion filter
	m_DistortionFreqId 	 = m_PostFXShader->LOOKUPVAR("distortionFreq");
	m_DistortionScaleId	 = m_PostFXShader->LOOKUPVAR("distortionScale");
	m_DistortionRollId 	 = m_PostFXShader->LOOKUPVAR("distortionRoll");

 	// get variable handles
 	// handle for the variable that sets the exposure value
	m_ExposureId = m_PostFXShader->LOOKUPVAR("Exposure");
#if __PPU
	m_AdaptedLuminanceId = m_PostFXShader->LOOKUPVAR("AdaptedLuminance");
#endif // __PPU
	// handle for the variable that provides the size of one texel
	m_TexelSizeId = m_PostFXShader->LOOKUPVAR("TexelSize");
	// this is for the luminance history
	// m_TexCoorId = m_PostFXShader->LOOKUPVAR("TexCoord");
	// handle for the variable that holds the values to decelerate the adaption process
	m_ElapsedTimeId = m_PostFXShader->LOOKUPVAR("ElapsedTime");		
	// handle to the number of frames that are stored to keep track of luminance changes
	m_FramesID = m_PostFXShader->LOOKUPVAR("Frames");		
	// lambda value for light adaptation
	m_LambdaId = m_PostFXShader->LOOKUPVAR("Lambda");								

	// handle for the variable that holds bright pass related values like so:
	// BrightPassValues.x = threshold value for bright pass
	// BrightPassValues.y =  offset value for bright pass
	// BrightPassValues.z = the middle gray key value. 
	// This value is used in the bright pass filter and the tone mapping stage
	// BrightPassValues.w = White value, this is used to minimize loss of contrast when tone mapping a low-dynamic range
	m_BrightPassValuesId = m_PostFXShader->LOOKUPVAR("BrightPassValues");		
	m_WhiteId = m_PostFXShader->LOOKUPVAR("White");	

	// handle to color correction variable in shader
	m_ColorCorrectId = m_PostFXShader->LOOKUPVAR("ColorCorrect");	
	// handle to color correction variable in shader
	m_ConstAddId = m_PostFXShader->LOOKUPVAR("ConstAdd");	
	// handle to color correction variable in shader
	m_deSatId = m_PostFXShader->LOOKUPVAR("deSat");
	// handle to contrast value in the shader
	m_ContrastId = m_PostFXShader->LOOKUPVAR("Contrast");
	// handle for the radial blur parameter
	m_ScaleId = m_PostFXShader->LOOKUPVAR("Scale");	
	// lower limit for light adaption
	m_LowerLimitAdaptionId = m_PostFXShader->LOOKUPVAR("LowerLimitAdaption");	
	// upper limit for light adaption
	m_HigherLimitAdaptionId = m_PostFXShader->LOOKUPVAR("HigherLimitAdaption");	
	// Standard bloom intensity
	m_BloomIntensity = m_PostFXShader->LOOKUPVAR("IntensityBloom");

	//
	// Depth of Field
	//
	m_DofParamsId = m_PostFXShader->LOOKUPVAR("DofParams"); 
	m_NearFarClipPlaneQId  = m_PostFXShader->LOOKUPVAR("NearFarClipPlaneQ");

	// Police Camera
	// film grain parameters
	m_fIntensityGrainId = m_PostFXShader->LOOKUPVAR("intensityGrain");
	m_fInterferenceId = m_PostFXShader->LOOKUPVAR("interference");
	m_fInterlaceIntensityId = m_PostFXShader->LOOKUPVAR("intensityInterlace");
	// gamma control
	m_fGammaId = m_PostFXShader->LOOKUPVAR("Gamma");

	// RDR2 parameters
	m_DeathId = m_PostFXShader->LOOKUPVAR("DeathMag");
	m_DeathDripId = m_PostFXShader->LOOKUPVAR("DeathDrip");
	m_DeathBloodId = m_PostFXShader->LOOKUPVAR("DeathBlood");
	m_DeathColorId = m_PostFXShader->LOOKUPVAR("DeathColor");
	m_DeathTexId = m_PostFXShader->LOOKUPVAR("DeathTex");

	// TO DO: multiple splats
	m_DeathSplatId[0] = m_PostFXShader->LOOKUPVAR("DeathSplat1");

	// Fog / handles to different fog parameters
	m_FogMinColorId = m_PostFXShader->LOOKUPVAR("FogMinColor");	
	m_FogMaxColorId = m_PostFXShader->LOOKUPVAR("FogMaxColor");	
	m_FogParamsId = m_PostFXShader->LOOKUPVAR("FogParams");	
	m_ParticleFogParamsId = m_PostFXShader->LOOKUPVAR("ParticleFogParams");	

	// color blue shift at night
	m_BlueShiftParamsId = m_PostFXShader->LOOKUPVAR("BlueShiftParams");	
	m_BlueShiftColorId = m_PostFXShader->LOOKUPVAR("BlueShiftColor");	

	// light streak
	m_LightStreakDirectionId = m_PostFXShader->LOOKUPVAR("LightStreakDirection");	
	m_LightStreakWeightsId = m_PostFXShader->LOOKUPVAR("streakWeights");	
	m_StreakParamsId = m_PostFXShader->LOOKUPVAR("StreakParams");

	// Lookup techniques directly from the shader to avoid requiring a ton of technique groups
	// retrieve handles for post-processing effects techniques

	//
	// no post processing
	//
	m_OnlyColorFiltersTechnique = m_PostFXShader->LookupTechnique("OnlyColorFilters");		
	m_CopyTechnique = m_PostFXShader->LookupTechnique("CopyRT");
	m_SimpleCopyTechnique = m_PostFXShader->LookupTechnique("SimpleCopy");

#if __XENON
	// xenon needs a special copy for 32bit float rendertargets (only red channel is valid)
	m_CopyRedTechnique = m_PostFXShader->LookupTechnique("CopyRedRT");	
#endif

	m_SeedBrightPassTechnique = m_PostFXShader->LookupTechnique("SeedRT");	
	m_SeedBrightPassNoZTechnique = m_PostFXShader->LookupTechnique("SeedRTNoZ");	
	m_FogCardTechnique = m_PostFXShader->LookupTechnique("FogCard");	
	m_CopyAlphaTechnique = m_PostFXShader->LookupTechnique("CopyAlphaRT");
	m_CopyColorScaleTechnique = m_PostFXShader->LookupTechnique("ColorScaleCopyRT");
	m_ToneDownTechnique = m_PostFXShader->LookupTechnique("ToneDownBrightPass");

	// preview to visualize the blur factor for depth of field
	m_CopyBlurFactorTechnique = m_PostFXShader->LookupTechnique("CopyBlurFactor");

	// convert from RGB 12-bit to L16uv
	m_Convert12bitToL16uvTechnique = m_PostFXShader->LookupTechnique("Convert12bitToL16uv");

	//
	// High Dynamic Range
	//
	// technique that just copies 1:1 render targets
	m_InitialDownsampleTechnique = m_PostFXShader->LookupTechnique("InitialDownSampleRT");
	m_DownSampleTechnique = m_PostFXShader->LookupTechnique("DownSampleRT");

	// EDGE post uses linear depth for DOF and motion blur
	m_DepthDownSampleTechnique = m_PostFXShader->LookupTechnique("DepthDownsample");

	// technique that is used to average the luminance values during scale with a 3x3 pattern
	m_SampleAverageLuminanceTechnique = m_PostFXShader->LookupTechnique("SampleAverageLuminance");

	// technique that is used to average the luminance values during scale with a 4x4 pattern
	m_ReSampleAverageLuminanceTechnique = m_PostFXShader->LookupTechnique("ReSampleAverageLuminance");  
	// technique that is used to average the luminance values during scale and uses the exponent result with a 4x4 pattern
	m_ReSampleAverageLuminanceExpTechnique = m_PostFXShader->LookupTechnique("ReSampleAverageLuminanceExp");
	// technique that calculates the adapted luminance
	m_CalculateAdaptedLuminanceTechnique = m_PostFXShader->LookupTechnique("CalculateAdaptedLuminance");

	// Bloom filter by Masake Kawase
	m_KawaseBloomTechnique = m_PostFXShader->LookupTechnique("KawaseBloomFilter");
	// Streak filter by Masake Kawase
	m_KawaseStreakTechnique = m_PostFXShader->LookupTechnique("KawaseStreakFilter");
	m_BrightPassParamsId = m_PostFXShader->LOOKUPVAR("brightPassParams");

	// 25-tap Gauss filter
	// Gauss blur horizontal texture coordinate offsets
	m_horzTapOffsId = m_PostFXShader->LOOKUPVAR("horzTapOffs");
	// Gauss blur vertical texture coordinate offsets
	m_vertTapOffsId = m_PostFXShader->LOOKUPVAR("vertTapOffs");	
	// texel weight
	m_TexelWeightId = m_PostFXShader->LOOKUPVAR("TexelWeight");	


	// techniques for 360-optimized two pass Gauss filter
	m_GaussXTech = m_PostFXShader->LookupTechnique("GaussX");
	m_GaussYTech = m_PostFXShader->LookupTechnique("GaussY");

	// techniques for 25-tap two pass Gauss filter
	m_GaussX5Tech = m_PostFXShader->LookupTechnique("GaussX_5");
	m_GaussY5Tech = m_PostFXShader->LookupTechnique("GaussY_5");


	// techniques for 25-tap two pass Gauss filter
	m_GaussX25Tech = m_PostFXShader->LookupTechnique("GaussX_25");
	m_GaussY25Tech = m_PostFXShader->LookupTechnique("GaussY_25");

	// techniques for 5-tap two pass Gauss filter
	m_GaussX5Tech = m_PostFXShader->LookupTechnique("GaussX_5");
	m_GaussY5Tech = m_PostFXShader->LookupTechnique("GaussY_5");

	// techniques for 17-tap Gauss filter
	m_GaussX17Tech = m_PostFXShader->LookupTechnique("GaussX_17");
	m_GaussY17Tech = m_PostFXShader->LookupTechnique("GaussY_17");

#if __PPU
	// PS3 specific filter techniques
	m_PS3QuincunxFilterTech = m_PostFXShader->LookupTechnique("PS3QuincunxFilter");
	m_PS3GaussFilterTech = m_PostFXShader->LookupTechnique("PS3GaussFilter");
#endif

	// technique that uses shaders that apply a bright pass to the target render target
	m_BrightPassTechnique = m_PostFXShader->LookupTechnique("BrightPass");
	// technique that holds the shaders for tone mapping and the final screen blit
	m_ToneMappingTechnique = m_PostFXShader->LookupTechnique("ToneMapping");

	// combined HDR + DOF
	m_ToneMapDOFTechnique = m_PostFXShader->LookupTechnique("ToneMapDOF");

	// combined HDR + DOF + Fog
	m_ToneMapDOFFogTechnique = m_PostFXShader->LookupTechnique("ToneMapDOFFog");

	// combined HDR + Fog
	m_ToneMapFogTechnique = m_PostFXShader->LookupTechnique("ToneMapFog");

	// combined HDR + Fog + Distort
	m_ToneMapFogDistortTechnique = m_PostFXShader->LookupTechnique("ToneMapFogDistort");

	// RDR2 death effect!
	m_RDR2DeathTechnique = m_PostFXShader->LookupTechnique("RDR2Death");

	m_BlurEffectTintColorId = m_PostFXShader->LOOKUPVAR("blurEffectTintColor");
	m_BlurEffectTintFadeId = m_PostFXShader->LOOKUPVAR("blurEffectTintFade");
	m_BlurEffectDesatScaleId = m_PostFXShader->LOOKUPVAR("blurEffectDesatScale");

	// Just normal Fog
	m_FogTechnique = m_PostFXShader->LookupTechnique("Fog");

	// Just Fog that uses the alpha blending to save a resolve and allow it to be easily moved in a frame
	m_BlendedFogTechnique = m_PostFXShader->LookupTechnique("BlendedHeightFog");

	// combined HDR + Fog + Police Cam
	m_ToneMapFogPoliceCamTechnique = m_PostFXShader->LookupTechnique("ToneMapFogPoliceCam");

#if RAGE_SSAO
	m_SSAODownscaleTechnique = m_PostFXShader->LookupTechnique("SSAODownscale");
	m_SSAOMainTechnique = m_PostFXShader->LookupTechnique("SSAOMain");
	m_SSAOBlurXTechnique = m_PostFXShader->LookupTechnique("SSAOBlurX");
	m_SSAOBlurYTechnique = m_PostFXShader->LookupTechnique("SSAOBlurY");
	m_SSAOUpscaleTechnique = m_PostFXShader->LookupTechnique("SSAOUpscale");
	m_SSAOProjectionParamsId = m_PostFXShader->LOOKUPVAR("SSAOProjectionParams");
	m_SSAOMapId = m_PostFXShader->LOOKUPVAR("SSAOMap");
	m_SSAOTweakParamsId = m_PostFXShader->LOOKUPVAR("SSAOTweakParams");
#endif // RAGE_SSAO
	m_ScreenSizeId = m_PostFXShader->LOOKUPVAR("gScreenSize");

	// load a volume texture for the PoliceCam effect
	if (path!=NULL)
		ASSET.PopFolder();

	if (sm_DefaultTexturePath)
	{
		// If the game requested a particular path for ppp textures, use that.
		ASSET.PushFolder(sm_DefaultTexturePath);
	}
	else
	{
		// get default asset path -> root of asset directory
		ASSET.PushFolder(ASSET.GetPath());
	}

	// load noise look-up texture for the old TV effect in Sepia
#if __PPU
#define RANDOM_TEXTURE_NAME		"Random3D_PS3"
#define RANDOM_TEXTURE_NAME_EXT	"Random3D_PS3.dds"
#else
#define RANDOM_TEXTURE_NAME		"Random3D_Xenon"
#define RANDOM_TEXTURE_NAME_EXT	"Random3D_Xenon.dds"
#endif

	if ( ASSET.Exists( RANDOM_TEXTURE_NAME , "dds" ) )
	{
		m_RandomMap = grcTextureFactory::GetInstance().Create(RANDOM_TEXTURE_NAME_EXT);
	}

	if ( ASSET.Exists( "lightglow", "dds" ) )
	{
		m_LightGlowMap = grcTextureFactory::GetInstance().Create("lightglow.dds");
	}
	grcTextureFactory::GetInstance().RegisterTextureReference("__noisemap", m_RandomMap);
	grcTextureFactory::GetInstance().RegisterTextureReference("__lightglowmap", m_LightGlowMap);

	// Loaded by game
	m_DeathMap = NULL;
	for (int cnt = 0; cnt < NUM_BLOOD_SPLATTER_TEXTURES; cnt++)
	{
		m_DeathSplat[cnt] = NULL;
	}

	ASSET.PopFolder();
}

void grPostFX::CreateRenderTargets(int iWidth, int iHeight)
{	
	RAGE_TRACK(grPostFx);
	RAGE_TRACK(grPostFxRenderTargets);

	// set render target descriptions
	grcTextureFactory::CreateParams params;
	grcTextureFactory::CreateParams depthparams;
	params.Multisample = 0;
	params.HasParent = true;
	params.Parent = NULL; 
	params.UseFloat = true;

	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();


// for the PC we need to create a color and a depth buffer ... as long as we can not read from the back buffer and the depth buffer here
#if __WIN32PC
	// 
	GRCDEVICE.GetMultiSample(*(u32*)&depthparams.Multisample, depthparams.MultisampleQuality);
	if (depthparams.Multisample) 
		depthparams.CreateAABuffer = true;

	params.Multisample = depthparams.Multisample;
	params.MultisampleQuality = depthparams.MultisampleQuality;
	params.CreateAABuffer = depthparams.CreateAABuffer;

	m_DepthBufferRT = textureFactory.CreateRenderTarget("FullScreen Z-Buffer", grcrtDepthBuffer, iWidth, iHeight, 32,&depthparams);

	if(PARAM_ati.Get())
	{
		// needs to be implemented for ATI X1 cards
		params.Multisample = 0;
		params.Format = grctfA2B10G10R10ATI;
	}
	else
		params.Format = grctfA16B16G16R16F;

	m_FSCopy = textureFactory.CreateRenderTarget("FullScreenCopy", grcrtPermanent, iWidth, iHeight, 64, &params);

	params.Multisample = 0;
#endif

// if we are on a 360, we can pick the 7e3 / 16-bit fp rendertarget / texture combination
#if __XENON
	#if LOW_PRECISION_INTERMEDIATE_TARGETS
	params.Format = grctfA8R8G8B8;
	#else
	params.Format = grctfA2B10G10R10;
	#endif
	params.UseFloat = true;
#elif __PPU && HACK_MC4
#if 1
	{
		if (GRCDEVICE.GetWidth() == 1280 && GRCDEVICE.GetHeight() == 720)
		{
			int backBufferWidth = 1280;
			int backBufferHeight = 720;
			PARAM_bb720pwidth.Get(backBufferWidth);
			PARAM_bb720pheight.Get(backBufferHeight);
			Assert(backBufferWidth == iWidth && backBufferWidth <= 1280);
			Assert(backBufferHeight == iHeight && backBufferHeight <= 720);

			if (backBufferWidth < 1280 && backBufferHeight < 720)
			{
				params.Format = grctfA8R8G8B8;
				params.IsSRGB = PARAM_srgb.Get();
				m_ResolveBuffer = textureFactory.CreateRenderTarget("ResolveBuffer", grcrtPermanent, iWidth, iHeight, 32, &params);
				params.IsSRGB = false;
			}
			else
			{
				m_ResolveBuffer = NULL;
			}
		}
		else
		{
			m_ResolveBuffer = NULL;
		}
	}
#else
	m_ResolveBuffer = NULL;
#endif // 0
#elif __WIN32PC
		if(PARAM_ati.Get())
		{
			// needs to be implemented for ATI X1 cards
			params.Multisample = 0;
			params.Format = grctfA2B10G10R10ATI;
		}
		else
			params.Format = grctfA16B16G16R16F;
#endif

	//
	// for 360 only
	// here are a few performance measurements from a test environment 
	// that does not exist in code anymore but might be interesting anyway
	//
	// if you divide by 3: downsampling 0.649 / blur: 0.267
	// if you divide by 4: downsampling 0.554 / blur: 0.154
	// if you divide by 5: downsampling 0.7 / blur: 0.1
	// if you divide by 6: downsampling 2.566 / blur: 0.04
	// if you specify 288 / 160 (close to a divide through 4.5): downsampling 0.738 / blur: 0.123
	// if you specify 320 / 160: downsampling 0.71 / blur: 0.136
	// seems like dividing through 4 is the sweetspot ... what a surprise

	int iQuarterWidth = 0;
	int iQuarterHeight = 0;

	// quarter size render target
	if(iWidth < 1280 && iHeight < 720 && 0) // && 0 = RDR2.  Always use downsized buffers!
	{
		// if it is a smaller render target .. just use the same size for the quarter render buffer for now ...
		// we will need more fine grain control here in the future
		iQuarterWidth = iWidth;
		iQuarterHeight = iHeight;
	}
	else
	{
		iQuarterWidth = iWidth / 2;
		iQuarterHeight = iHeight / 2;
	}

	int iSmallWidth = iQuarterWidth / 2;
	int iSmallHeight = iQuarterHeight / 2;

	// scaled down render target used by many other effects
#if __XENON
	params.Multisample = 1;
#endif
#if __PPU
	// please note that 64-bit fp render targets do not support multi-sampling and also do not support hardware Quincunx or Gauss filtering
	params.Multisample = 0;
	//params.InTiledMemory = true;
#endif
	m_QuarterRT = textureFactory.CreateRenderTarget("1/4 Screen", grcrtPermanent, iQuarterWidth, iQuarterHeight, 32, &params);

	// scratch quarter render target
	m_ScratchQuarterRT = textureFactory.CreateRenderTarget("1/4 Scratch Screen", grcrtPermanent, iQuarterWidth, iQuarterHeight, 32, &params);

	
#if LOW_PRECISION_INTERMEDIATE_TARGETS
	m_QuarterMotionBlurRT = m_QuarterRT;
	m_QuarterStreakRT = m_ScratchQuarterRT;
#else

#if __XENON
	params.Format = grctfA8R8G8B8;
	params.Format = grctfA2B10G10R10;
#endif // __XENON
#endif // LOW_PRECISION_INTERMEDIATE_TARGETS

	// Bloom effect: Gauss blur render target
	m_GaussRT = textureFactory.CreateRenderTarget("Gauss RT", grcrtPermanent, iSmallWidth, iSmallHeight, 32, &params);

	// used in the first pass for Gauss blur and later for other effects
	m_ScratchPadRT = textureFactory.CreateRenderTarget("Scratch pad RT", grcrtPermanent, iSmallWidth, iSmallHeight, 32, &params);

#if __XENON
	// if there is an active rendertarget memory pool,turn it off
	// the following two targets are persistent and cannot overlap with other target's memory
	grcRenderTargetMemPool * memPool = grcRenderTargetMemPool::Deactivate();
#endif

#if __XENON
	params.Format = grctfA8R8G8B8;
#endif	

	m_FogMapRT = textureFactory.CreateRenderTarget("Fog map RT", grcrtPermanent, iSmallWidth/2, iSmallHeight/2, 32, &params);

#if PERSISTENT_BRIGHTPASS
	m_PersistentBrightPassRT = textureFactory.CreateRenderTarget("Bright Pass", grcrtPermanent, iQuarterWidth, iQuarterHeight, 32, &params);
#if __XENON
	m_PrevPersistentBrightPassRT = m_PersistentBrightPassRT;
#else
	m_PrevPersistentBrightPassRT = textureFactory.CreateRenderTarget("PrevBright Pass", grcrtPermanent, iQuarterWidth, iQuarterHeight, 32, &params);
#endif
#endif

#if __XENON
	params.Format = grctfA2B10G10R10;
#endif	

	// result of bright pass filter goes in here
	m_BrightPassRT = textureFactory.CreateRenderTarget("Bright Pass", grcrtPermanent, iSmallWidth, iSmallHeight, 32, &params);

#if __XENON
	// reactivate for the rest of the targets
	if (memPool)
		memPool->grcRenderTargetMemPool::Reactivate();
#endif

#if __PPU
	params.Multisample = 0;
	//params.InTiledMemory = false;
#endif

	// this should work now on 360, PS3 and PC
	params.Format = grctfR32F;

	//
	// the following render targets are used to measure the luminance of the scene
	// 
	// first pass luminance measure goes into a 128x128 32-bit fp render target
	m_128x128RT = textureFactory.CreateRenderTarget("128x128 RT", grcrtPermanent, 128, 128, 32, &params);

	// second pass luminance measure goes into a 64x64 32-bit fp render target
	m_64x64RT = textureFactory.CreateRenderTarget("64x64 RT", grcrtPermanent, 64, 64, 32, &params);

	// third pass luminance measure goes into a 16x16 32-bit fp render target
	m_16x16RT = textureFactory.CreateRenderTarget("16x16 RT", grcrtPermanent, 16, 16, 32, &params);

	// fourth pass luminance measure goes into a 4x4 32-bit fp render target
	m_4x4RT = textureFactory.CreateRenderTarget("4x4 RT", grcrtPermanent, 4, 4, 32, &params);

#if __XENON
	// if there is an active rendertarget memory pool,turn it off
	// the following two targets are persistent and cannot overlap with other target's memory
	memPool = grcRenderTargetMemPool::Deactivate();
#endif

	// fifth pass luminance measure goes into a 4x4 32-bit fp render target
	// luminance history ... this render target holds the last 16 frames of average luminance values
	m_LumHistoryRT = textureFactory.CreateRenderTarget("1x1 RT", grcrtPermanent, 4, 4, 32, &params);

#if __PPU
	// We want our 1x1 tone mapping render targets to be efficiently readable by the PPU so stick them in host memory
	params.InLocalMemory = false;
#endif // __PPU

	// temporary luminance result into a 1x1 32-bit fp render target
	m_CurrentAdaptedLum1x1RT = textureFactory.CreateRenderTarget("Current Adapted Lum 1x1 RT", grcrtPermanent, 1, 1, 32, &params);

#if __XENON
	// reactiveate pool for subsequent targets
	if (memPool)
		memPool->grcRenderTargetMemPool::Reactivate();
#endif

#if RAGE_SSAO
	{
		grcTextureFactory::CreateParams params;
		params.Format = grctfA8R8G8B8;
		m_SSAOPingPongRT[0] = textureFactory.CreateRenderTarget("SSAO Ping Pong RT #0", grcrtPermanent, 512, 256, 32);
		m_SSAOPingPongRT[1] = textureFactory.CreateRenderTarget("SSAO Ping Pong RT #1", grcrtPermanent, 512, 256, 32);
	}
#endif // RAGE_SSAO

#if __WIN32PC
	// the PC / PS3 can not read and write into a render target at the same time, that's why we need some temporary storage here
	m_LastAdaptedLum1x1RT = textureFactory.CreateRenderTarget("Last Adapted Lum 1x1 RT", grcrtPermanent, 1, 1, 32, &params);
#endif // __WIN32PC

#if __PPU
	// Clear to zero so we don't get crazy feedback from unitialised values
	grcTextureLock lock;
	m_CurrentAdaptedLum1x1RT->LockRect(0, 0, lock);
	*reinterpret_cast<float*>(lock.Base) = 0.0f;
	m_CurrentAdaptedLum1x1RT->UnlockRect(lock);
#endif // __PPU
}

void grPostFX::BindPCRenderTarget(bool WIN32PC_ONLY(clear))
{
#if __WIN32PC
	Assert(!m_RenderTargetActive && "grPostFX render target Bind request when already active");

	// on PCs, we will render to a rendertarget so we have easy access to the screen contents
	grcTextureFactory::GetInstance().LockRenderTarget(0, m_FSCopy, m_DepthBufferRT);
	if (clear)
	{
		GRCDEVICE.Clear(true,Color32(GetClearColor()), true,1.f,false);
	}
	m_RenderTargetActive = true;
#endif
}

// PC-only 
void grPostFX::ReleasePCRenderTarget()
{
#if __WIN32PC
	// unlock it when done with rendering ... 
	Assert(m_RenderTargetActive && "grPostFX render target release request when not active");
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);	
	m_RenderTargetActive = false;
#endif
}

// ... let the music play
void grPostFX::Process()
{
	BeginPostProcess();
	ProcessPostProcess();
	EndPostProcess();
}

// workhorse render to render target blit function
//
void grPostFX::BlitToRT(grcRenderTarget * target,		// target render target
						const grcTexture * texture,			// source render target
						grcEffectTechnique technique,	// technique in FX file
						grcEffectVar textureId,			// texture sampler
						const Color32 * color,			// vertex color | is that still used anywhere????
						float x1,						// x1 - Destination base x
						float y1,						// y1 - Destination base y
						float x2,						// x2 - Destination opposite-corner x
						float y2						// y2 - Destination opposite-corner y
#if __XENON
						,const int exponentbias)
#else
						)
#endif		
{
//	int dstWidth, dstHeight;

	// vertex color to white ... is there a reason?
	if (color == NULL) 
		color = &white;

	// use the default workhorse texture sampler stages
	if(!textureId)
		textureId = m_RenderMapId;

	if(target)
	{
		grcTextureFactory::GetInstance().LockRenderTarget(0, target, NULL);
	}

	m_PostFXShader->SetVar(m_ScreenSizeId, Vector4((float)GRCDEVICE.GetWidth(), (float)GRCDEVICE.GetHeight(), 1.0f/(float)GRCDEVICE.GetWidth(), 1.0f/(float)GRCDEVICE.GetHeight()));

	// set the chosen render target sampler stages; default is m_RenderMapId

	m_PostFXShader->SetVar(textureId, texture);

	if ( texture )
	{
		// provide the size of the target or source render target to the shader
		m_PostFXShader->SetVar(m_TexelSizeId, Vector4(1.0f/float(texture->GetWidth()),1.0f/float(texture->GetHeight()),(float)texture->GetWidth(), (float)texture->GetHeight()));
	}

	// blit
	m_PostFXShader->TWODBlit(
							x1,				// x1 - Destination base x
							y1,				// y1 - Destination base y
							x2,				// x2 - Destination opposite-corner x
							y2,				// y2 - Destination opposite-corner y
							0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
							0.0f,			// u1 - Source texture base u (in normalized texels)
							0.0f,			// v1 - Source texture base v (in normalized texels)
							1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
							1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
							*color,			// color - vertex color ... for OpenGL style grcDrawQuadf
							technique);

	//	release texture again
	m_PostFXShader->SetVar(textureId, (grcTexture*)grcTexture::None);

	// if there is a target render target other then the full screen, we need to unlock it
	// - Realize(): resolves the render target
	// - Device::SetRenderTarget() to previous render target | does that mean it always sets the render target back? Only makes sense when I want to get back to full screen??
	// - releases previous render target
	// - Device::SetDepthStencilSurface() to previous depth and stencil buffer | see above
	// - sets size
	if (target)
	{
#if __XENON

		m_ClearParams.ColorExpBias = exponentbias;
		grcTextureFactory::GetInstance().UnlockRenderTarget(0, &m_ClearParams);	
#elif __WIN32PC || __PPU
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);	
#endif
	}
}

// setup work for post processing
void grPostFX::BeginPostProcess()
{

#if Z_BUFFERDEPTH

	PIXBegin(0, "Grab Z buffer");

#if __PPU || __XENON
	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
#endif // __PPU || __XENON

	// grab depth buffer here
#if __XENON
	if (m_DepthBuffer360FromTiling == NULL)
	{
		// tiled rendering is off, so we need to resolve the depthbuffer ourselves
		m_DepthBuffer = textureFactory.GetBackBufferDepth(true);
	}
	else
	{
		// tiled rendering is on, so we must not resolve again, as EndTiledRendering already did
		m_DepthBuffer = m_DepthBuffer360FromTiling;
	}
#elif __PPU
	m_DepthBuffer = textureFactory.GetBackBufferDepth();
#endif

	PIXEnd();

#endif // Z_BUFFERDEPTH

	// sets all render states and alpha blending states
	if (PARAM_hdr.Get())
	{
		grcEffect::SetDefaultRenderState( grcersHIGHPRECISIONBLENDENABLE, true);
	}

	grcState::SetState( grcsHalfPixelOffset, true );

	grcState::SetDepthTest(false);
	grcState::SetDepthWrite(false);
	grcState::SetAlphaBlend(false);
	grcState::SetAlphaTest(false);
	

	//
	// this is the setup path for HDR + DOF effect
	//
	GrabBackBuffer();
#if __PS3
	PushBackBuffer();
#endif
}


void grPostFX::BrightPass()
{
			PIXBegin(0, "Bright-Pass Filter");

#if __XENON
			m_PostFXShader->SetVar(m_VertexTextureId, m_CurrentAdaptedLum1x1RT); 		
#else
	#if __PPU
			m_PostFXShader->SetVar(m_AdaptedLuminanceId, m_AdaptedLuminance);
	#else
			m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, m_CurrentAdaptedLum1x1RT); 		
	#endif
#endif
			

			BlitToRT(m_BrightPassRT, m_GaussRT, m_BrightPassTechnique);

#if __XENON
			m_PostFXShader->SetVar(m_VertexTextureId, (grcTexture*)grcTexture::None);
#else
			m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, (grcTexture*)grcTexture::None);
#endif
			PIXEnd();
}


void grPostFX::LoadDeathTexture(char *filename, char **splatFilenames)
{
	if (filename)
	{
		m_DeathMap = grcTextureFactory::GetInstance().Create(filename);
	}
	if (splatFilenames)
	{
		// TO DO: support multiple splats
		if (splatFilenames[0])
		{
			m_DeathSplat[0] = grcTextureFactory::GetInstance().Create(splatFilenames[0]);
		}
	}
}

void grPostFX::RDR2DeathPass()
{
	PIXBegin(0, "RDR2 death pass");

	m_PostFXShader->SetVar(m_DeathTexId, m_DeathMap);
	m_PostFXShader->SetVar(m_DeathSplatId[0], m_DeathSplat[0]);
	BlitToRT(NULL, m_BackBuffer, m_RDR2DeathTechnique, m_FullResMapId);
	m_PostFXShader->SetVar(m_DeathTexId, (grcTexture*)grcTexture::None);
	m_PostFXShader->SetVar(m_DeathSplatId[0], (grcTexture*)grcTexture::None);

	PIXEnd();
}

void grPostFX::ToneMappingSetup()
{
#if RAGE_SSAO
	SSAO();
#endif // RAGE_SSAO

	if(!m_PPPPresets->m_SwitchPoliceCam)
	{
		Vector4 Temp(m_PPPPresets->m_BrightPassThreshold, m_PPPPresets->m_BrightPassOffset, m_PPPPresets->m_MiddleGray, m_PPPPresets->m_fBrightPassWhite * m_PPPPresets->m_fBrightPassWhite);
		m_PostFXShader->SetVar(m_BrightPassValuesId, Temp);
	}
	else
	{
		Vector4 Temp(m_PPPPresets->m_BrightPassThresholdTwo, m_PPPPresets->m_BrightPassOffsetTwo, m_PPPPresets->m_MiddleGrayTwo, m_PPPPresets->m_fBrightPassWhiteTwo * m_PPPPresets->m_fBrightPassWhiteTwo);
		m_PostFXShader->SetVar(m_BrightPassValuesId, Temp);
	}

	m_PostFXShader->SetVar(m_WhiteId, m_PPPPresets->m_fWhite * m_PPPPresets->m_fWhite);

	m_PostFXShader->SetVar(m_LowerLimitAdaptionId, m_PPPPresets->m_fLowerLimitAdaption);

	m_PostFXShader->SetVar(m_HigherLimitAdaptionId, m_PPPPresets->m_fHigherLimitAdaption);

	//
	// this is the source for most effects
	//
	// scale down to a 1/4 render target size
	PIXBegin(0, "Down Sample to 1/4 size");
#if RAGE_ENCODEOPAQUECOLOR
	BlitToRT(m_QuarterRT, m_BackBuffer, m_InitialDownsampleTechnique, m_FullResMapId);
#else
	BlitToRT(m_QuarterRT, m_BackBuffer, m_InitialDownsampleTechnique);
#endif // RAGE_ENCODEOPAQUECOLOR
	PIXEnd();

	//
	// scale down to a 1/16 render target size
	//
	PIXBegin(0, "Downsample to 1/16 size and blur");
	BlitToRT(m_GaussRT, m_QuarterRT, m_DownSampleTechnique);
	PIXEnd();

	//
	// measure luminance of the full screen
	//
	// scale down to 128x128 of render target size and convert to luminance
	PIXBegin(0, "Measure and apply Adaption");
	m_PostFXShader->SetVar(m_TexelSizeId, Vector4(1.0f / static_cast<float>(m_GaussRT->GetWidth()), 1.0f / static_cast<float>(m_GaussRT->GetHeight()), static_cast<float>(m_GaussRT->GetWidth()), static_cast<float>(m_GaussRT->GetHeight())));
	BlitToRT(m_128x128RT, m_GaussRT, m_SampleAverageLuminanceTechnique, m_ToneMapId);

	// scale down luminance to 64x64
	m_PostFXShader->SetVar(m_TexelSizeId, Vector4(1.0f / static_cast<float>(m_128x128RT->GetWidth()), 1.0f / static_cast<float>(m_128x128RT->GetHeight()), static_cast<float>(m_128x128RT->GetWidth()), static_cast<float>(m_128x128RT->GetHeight())));
	BlitToRT(m_64x64RT, m_128x128RT, m_ReSampleAverageLuminanceTechnique, m_ToneMapId);

	// scale down luminance to 16x16
	m_PostFXShader->SetVar(m_TexelSizeId, Vector4(1.0f / static_cast<float>(m_64x64RT->GetWidth()), 1.0f / static_cast<float>(m_64x64RT->GetHeight()), static_cast<float>(m_64x64RT->GetWidth()), static_cast<float>(m_64x64RT->GetHeight())));
	BlitToRT(m_16x16RT, m_64x64RT, m_ReSampleAverageLuminanceTechnique, m_ToneMapId);

	// scale down luminance to 4x4
	m_PostFXShader->SetVar(m_TexelSizeId, Vector4(1.0f / static_cast<float>(m_16x16RT->GetWidth()), 
							1.0f / static_cast<float>(m_16x16RT->GetHeight()), static_cast<float>(m_16x16RT->GetWidth()), 
							static_cast<float>(m_16x16RT->GetHeight())));
	BlitToRT(m_4x4RT, m_16x16RT, m_ReSampleAverageLuminanceTechnique, m_ToneMapId);

	//
	// we are storing a history of the last 16 frames in the 1x1 render targets
	// to access these render targets we need to generate texture coordinates for each frame
	// 
	// get size of one texel in texture coordinates
	Vector2 TexelSize = Vector2(1.0f / m_LumHistoryRT->GetWidth(), 1.0f / static_cast<float>(m_LumHistoryRT->GetHeight()));
	Vector4 TexelSize4 = Vector4(TexelSize.x, TexelSize.y, static_cast<float>(m_LumHistoryRT->GetWidth()), static_cast<float>(m_LumHistoryRT->GetHeight()));
	
	static int i = 0;
	static int w = 0;

	// how many texels did we write so far
	static int Counter = 1;

	// size of the render target is a limit
	int NumOfColums = m_LumHistoryRT->GetWidth();
	int NumOfRows = m_LumHistoryRT->GetHeight();

	// if the user wants the history only to catch a restricted number of frames 
	if(Counter < m_PPPPresets->m_iNumOfFramesInLumHistory)
	{
		// run a whole row in x direction
		if(i < (NumOfColums - 1.0))
		{
			i++;
			Counter++;
		}
		else
		{
			Counter++;

			// count up to the next row
			w++;
			
			// start running through this row
			i = 0;

			// if we are at the last row start from scratch
			if(!(w < NumOfRows))
				w = 0;
		}
	}
	else
	{
		Counter = 1;
		i = 0;
		w= 0;
	}

	Vector2 TexCoord = Vector2(i * TexelSize.x, w * TexelSize.y);
	// m_PostFXShader->SetVar(m_TexCoorId, Vector2(TexCoord.x, TexCoord.y));
	m_PostFXShader->SetVar(m_TexelSizeId, TexelSize4);

	//
	// ... and we also have to generate pixel coordinates to write the data
	// 
	// render always in only one pixel of the 16 pixels of the render target
	//
	// range conversion from 0..1 to -1..1
	// this is the center of the pixel then
	// texcoord(0, 0) == pixelcenter(-0.75, 0.75)
	// texcoord(0.25, 0.0) == pixelcenter(-0.25, 0.75) 
	// texcoord(0.5, 0.0) == pixelcenter(0.25, 0.75)
	// texcoord(0.75, 0.0) == pixelcenter(0.75, 0.75)
	//
	// X*2 - 0.75
	// -Y*2 - 0.75
	// 
	Vector2 BlitPixelCenter = Vector2(TexCoord.x * 2.0f - 0.75f, -(TexCoord.y * 2.0f - 0.75f)); 

	// get upper right and lower left corner
	// add 1/2 pixel up and 1/2 pixel left
	Vector2 BlitPixelUpperRightCorner = BlitPixelCenter + (TexelSize);
	Vector2 BlitPixelLowerLeftCorner = BlitPixelCenter - (TexelSize);

	// lock the lum history function
	grcTextureFactory::GetInstance().LockRenderTarget(0, m_LumHistoryRT, NULL);

	// this is actually a way to work around the fact that the XBOX uses EDRAM
	// what we do here is blit back the old texture to EDRAM and then blit the one pixel we want to 
	// change in there with the next blit call
#if __XENON
	// set the chosen render target sampler stages; default is m_RenderMapId
	m_PostFXShader->SetVar(m_ToneMapId, m_LumHistoryRT);

	// blit
	m_PostFXShader->TWODBlit(
							-1.0,				// x1 - Destination base x
							1.0,				// y1 - Destination base y
							1.0,				// x2 - Destination opposite-corner x
							-1.0,				// y2 - Destination opposite-corner y
							0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
							0.0f,			// u1 - Source texture base u (in normalized texels)
							0.0f,			// v1 - Source texture base v (in normalized texels)
							1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
							1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
							white,			// color - vertex color ... for OpenGL style grcDrawQuadf
							m_CopyTechnique);

	//	release texture again
	m_PostFXShader->SetVar(m_ToneMapId, (grcTexture*)grcTexture::None);
#endif
	// provide the size of the target or source render target to the shader
	m_PostFXShader->SetVar(m_TexelSizeId, Vector4(1.0f/float(m_4x4RT->GetWidth()),
							1.0f/float(m_4x4RT->GetHeight()),(float)m_4x4RT->GetWidth(), 
							(float)m_4x4RT->GetHeight()));

	// set the chosen render target sampler stages; default is m_RenderMapId
	m_PostFXShader->SetVar(m_ToneMapId, m_4x4RT);

	// blit
	m_PostFXShader->TWODBlit(
							BlitPixelUpperRightCorner.x,				// x1 - Destination base x
							BlitPixelUpperRightCorner.y,				// y1 - Destination base y
							BlitPixelLowerLeftCorner.x,				// x2 - Destination opposite-corner x
							BlitPixelLowerLeftCorner.y,				// y2 - Destination opposite-corner y
							0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
							0.0f,			// u1 - Source texture base u (in normalized texels)
							0.0f,			// v1 - Source texture base v (in normalized texels)
							1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
							1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
							white,			// color - vertex color ... for OpenGL style grcDrawQuadf
							m_ReSampleAverageLuminanceExpTechnique);

	//	release texture again
	m_PostFXShader->SetVar(m_ToneMapId, (grcTexture*)grcTexture::None);


	// if there is a target render target other then the full screen, we need to unlock it
	// - Realize(): resolves the render target
	// - Device::SetRenderTarget() to previous render target | does that mean it always sets the render target back? Only makes sense when I want to get back to full screen??
	// - releases previous render target
	// - Device::SetDepthStencilSurface() to previous depth and stencil buffer | see above
	// - sets size
#if __XENON
//	m_ClearParams.ResolveExpBias = 0;
	m_ClearParams.ColorExpBias = 0;
	grcTextureFactory::GetInstance().UnlockRenderTarget(0, &m_ClearParams);	
#elif __PPU || __WIN32PC
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);	
#endif

	//
	// adapt luminance to simulate light adaption of the human visual system
	//
	m_PostFXShader->SetVar(m_ElapsedTimeId, TIME.GetSeconds());
	m_PostFXShader->SetVar(m_FramesID, (float)m_PPPPresets->m_iNumOfFramesInLumHistory);
	m_PostFXShader->SetVar(m_LambdaId, logf(2.0f) / m_PPPPresets->m_fAdapt);

#if __WIN32PC
	// the PC / PS3 can not read and write in the same render target in one pass
	// that's why we need a temporary pointer to a render target to swap

	// copy temp luminance value into swap RT
	grcRenderTarget* swapRT = m_LastAdaptedLum1x1RT;

	// copy current luminance value into temp RT
	m_LastAdaptedLum1x1RT = m_CurrentAdaptedLum1x1RT;

	// copy into current luminance value the old ones
	m_CurrentAdaptedLum1x1RT = swapRT;

	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, m_LastAdaptedLum1x1RT); 		
	BlitToRT(m_CurrentAdaptedLum1x1RT, m_LumHistoryRT, m_CalculateAdaptedLuminanceTechnique, m_ToneMapId);
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, (grcTexture*)grcTexture::None);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, m_CurrentAdaptedLum1x1RT); 		
	BlitToRT(m_CurrentAdaptedLum1x1RT, m_LumHistoryRT, m_CalculateAdaptedLuminanceTechnique, m_ToneMapId);
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, (grcTexture*)grcTexture::None);
#endif

#if __PPU
	// Lock and read the current luminance
	grcTextureLock lock;
	m_CurrentAdaptedLum1x1RT->LockRect(0, 0, lock);
	m_AdaptedLuminance = *reinterpret_cast<float*>(lock.Base);
	m_CurrentAdaptedLum1x1RT->UnlockRect(lock);
#endif // __PPU

	PIXEnd();
}

void grPostFX::SetupDOF()
{
	//
	// grab all values necessary from widgets
	//
	Vector4 temp;
	temp.Set(m_PPPPresets->m_DofParamters);

	// Put blur range in an exponent that is more tune-friendly
	temp.x *= temp.x * temp.x;
	m_PostFXShader->SetVar(m_DofParamsId, temp);

	// 360 / PS3 path with Z-buffer support
#if Z_BUFFERDEPTH
	//
	// for the Z-buffer support get near and far clipping plane from the viewport
	//
	// What we do here is transform the Z value in the Z buffer from NDC to worldviewproj = camera space
	// To achieve this, we implement the following formula in the pixel shader
	// z = -(n * f) / (z_p * (f - n) - f)
	//
	// Q = Zf / Zf - Zn
	// z = Zn * Q / Zd - Q
	// this value is positive because we have an inverted z axis ... so the z axis is opengl style while the rest
	// is D3D style
	// whereas Zd is the value coming from the depth buffer
	const grcViewport *vp = grcViewport::GetCurrent();

	// get near and far clipping plane
	// to send it down to the pixel shader
	Vector3 PlanesAndQ(vp->GetNearClip(), vp->GetFarClip(), vp->GetFarClip() / (vp->GetFarClip() - vp->GetNearClip()));
	m_PostFXShader->SetVar(m_NearFarClipPlaneQId, PlanesAndQ);
#endif
}


#if NICEGAUSS
//-----------------------------------------------------------------------------
// Name: GaussianDistribution
// Desc: Helper function for CreateTexCoordNTexelWeights function to compute the 
//       2 parameter Gaussian distribution using the given standard deviation
//       rho
//		 This might be optimized by using a look-up table
//-----------------------------------------------------------------------------
float GaussianDistribution( float x, float y, float rho )
{
	float g = 1.0f / sqrtf( 2.0f * PI * rho * rho );
	g *= expf( -(x * x + y * y)/(2 * rho * rho));

	return g;
}

//-----------------------------------------------------------------------------
// Name: CreateTexCoordNTexelWeights
// Desc: Get the texture coordinate offsets to be used inside the Bloom
//       pixel shader.
//-----------------------------------------------------------------------------
void CreateTexCoordNTexelWeightsGauss(
									  int dwRenderTargetSize,
									  float fTexCoordOffset[15],
									  Vector4* vTexelWeight,
									  float fDeviation,
									  float fMultiplier
									  )
{
	int i=0;

	// width or height depending on which Gauss filter is applied
	// the vertical or horizontal filter
	float tu = 1.0f / (float)dwRenderTargetSize; 

	// Fill the center texel
	float weight = fMultiplier * GaussianDistribution( 0, 0, fDeviation );
	vTexelWeight[0] = Vector4( weight, weight, weight, 1.0f );

	fTexCoordOffset[0] = 0.0f;

	// Fill the first half
	for( i=1; i < 8; i++ )
	{
		// Get the Gaussian intensity for this offset
		weight = fMultiplier * GaussianDistribution( (float)i, 0, fDeviation );
		fTexCoordOffset[i] = i * tu;

		vTexelWeight[i] = Vector4( weight, weight, weight, 1.0f );
	}

	// Mirror to the second half
	for( i=8; i < 15; i++ )
	{
		vTexelWeight[i] = vTexelWeight[i-7];
		fTexCoordOffset[i] = -fTexCoordOffset[i-7];
	}
}
#endif

void grPostFX::PushBackBuffer()
{
#if __PS3
	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();

	grcRenderTarget* defaultDepthBuffer;

	defaultDepthBuffer = textureFactory.GetFrontBufferDepth(false);
	textureFactory.PushDefaultRenderTarget(grcmrtColor0, m_BackBuffer);
	textureFactory.PushDefaultRenderTarget(grcmrtDepthStencil, defaultDepthBuffer);
	for (u32 mrt = grcmrtColor1; mrt < grcmrtColorCount; ++mrt)
	{
		textureFactory.PushDefaultRenderTarget(static_cast<grcMrt>(mrt), NULL);
	}
	textureFactory.BindDefaultRenderTargets();
#endif
}

void grPostFX::PopBackBuffer()
{
#if __PS3
	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();

	for (u32 mrt = grcmrtColor1; mrt < grcmrtColorCount; ++mrt)
	{
		textureFactory.PopDefaultRenderTarget(static_cast<grcMrt>(mrt));
	}
	textureFactory.PopDefaultRenderTarget(grcmrtDepthStencil);
	textureFactory.PopDefaultRenderTarget(grcmrtColor0);
#endif // __PS3
}

//------------------------------------------------------------------------------
// Resolves the back buffer.
//
grcRenderTarget* grPostFX::GrabBackBuffer(bool forceResolve)
{
	if (!forceResolve && m_BackBuffer)
		return m_BackBuffer;

	PIXBegin(0, "Grab back buffer");

#if __XENON || __PS3
	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
#endif // __XENON || __PS3

#if __WIN32PC
	m_BackBuffer = m_FSCopy;
#endif

#if __XENON
	if (m_PredicatedTiling)
	{
		grcResolveFlags resolveFlags;
		resolveFlags.ClearColor = false;
		resolveFlags.ClearDepthStencil = false;
		GRCDEVICE.SaveBackBuffer(grcTextureFactory::GetInstance().GetBackBuffer(false), NULL, &resolveFlags);
		m_BackBuffer = (grcTextureFactory::GetInstance().GetBackBuffer(false));
	}
	else
	{
		m_BackBuffer = (grcRenderTarget*)textureFactory.GetBackBuffer();
	}
#endif

#if __PS3
	// grcRenderTarget* defaultDepthBuffer;

	{
		// GRCDEVICE.SetWaitFlip();

		GRCDEVICE.MsaaResolveBackToFrontBuffer();
		m_BackBuffer = textureFactory.GetFrontBuffer();
		/* defaultDepthBuffer = */ textureFactory.GetFrontBufferDepth(false);
	}

#endif // __PS3

	PIXEnd();

	return m_BackBuffer;
}

//------------------------------------------------------------------------------
// Resolves the depth buffer.
//
grcRenderTarget* grPostFX::GrabDepthBuffer(bool forceResolve)
{
	if (!forceResolve && m_DepthBuffer)
		return m_DepthBuffer;

	PIXBegin(0, "Grab Z buffer");

#if __PS3 || __XENON
	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
#endif

#if __XENON
	if (m_PredicatedTiling)
	{
		grcResolveFlags resolveFlags;
		resolveFlags.ClearColor = false;
		resolveFlags.ClearDepthStencil = false;
		GRCDEVICE.SaveBackBuffer(NULL, grcTextureFactory::GetInstance().GetBackBufferDepth(false), &resolveFlags);
		m_DepthBuffer = (grcTextureFactory::GetInstance().GetBackBufferDepth(false));
	}
	else
	{
		m_DepthBuffer = textureFactory.GetBackBufferDepth(true);
	}
#elif __PS3
	m_DepthBuffer = textureFactory.GetBackBufferDepth();
#endif

	PIXEnd();

	return m_DepthBuffer;
}

//------------------------------------------------------------------------------
void grPostFX::SetBackBuffer(grcRenderTarget* backBuffer)
{
	m_BackBuffer = backBuffer;
}

//------------------------------------------------------------------------------
void grPostFX::SetDepthBuffer(grcRenderTarget* depthBuffer)
{
	m_DepthBuffer = depthBuffer;
}

//------------------------------------------------------------------------------
//	PURPOSE
//		Tells postfx when we're in a predicated tiling draw.
//
#if __XENON
void grPostFX::SetPredicatedTiling(bool on)
{
	m_PredicatedTiling = on;
}
#endif


void grPostFX::MotionBlur()
{
}

void grPostFX::ProcessPostProcess()
{
	PIXBegin(0, "Post-Processing Pipeline");

#if __WIN32PC
	// need to close our "catch all" render target on the PC and start running the post-processing pipeline
	if (m_RenderTargetActive)
		grPostFX::ReleasePCRenderTarget();
#endif

	// if we are scaling up during post, use linear...
	u32 savedMinFiler = grcEffect::GetDefaultSamplerState(grcessMINFILTER);	
	u32 savedMagFiler = grcEffect::GetDefaultSamplerState(grcessMAGFILTER);
	grcEffect::SetDefaultSamplerState(grcessMINFILTER,(GRCDEVICE.GetHeight() > m_BackBuffer->GetHeight()) ? grcSSV::TEXF_LINEAR : grcSSV::TEXF_POINT);
	grcEffect::SetDefaultSamplerState(grcessMAGFILTER,(GRCDEVICE.GetHeight() > m_BackBuffer->GetHeight()) ? grcSSV::TEXF_LINEAR : grcSSV::TEXF_POINT);

#if __XENON
	int oldGRPAlloc = 0;
	if (GRCGPRALLOCATION)
	{
		GRCGPRALLOCATION->GetGPRAllocation();

		// set the pixel shader gprs to 80 -> vertex shader can only use 48 gprs
		GRCGPRALLOCATION->SetGPRAllocation(80);
	}
#endif

	// set gamma for all of this
	m_PostFXShader->SetVar(m_fGammaId, m_PPPPresets->m_fGamma);

	m_PostFXShader->SetVar(m_ColorCorrectId, m_PPPPresets->m_ColorCorrect);
	m_PostFXShader->SetVar(m_ConstAddId, m_PPPPresets->m_ConstAdd);
	m_PostFXShader->SetVar(m_deSatId, m_PPPPresets->m_deSat);
	m_PostFXShader->SetVar(m_ContrastId, m_PPPPresets->m_fContrast);
	m_PostFXShader->SetVar(m_BlueShiftParamsId, m_PPPPresets->m_BlueShiftParams);
	m_PostFXShader->SetVar(m_BlueShiftColorId, m_PPPPresets->m_BlueShiftColor);

	// Call the actual implementation of the PPP effect
	if (!m_PPPPresets->m_CurrentEffect)
	{
		m_PPPPresets->m_CurrentEffect = GetDefaultEffect();
#if __BANK
		m_PPPPresets->m_iChooseTechnique = 0;
#endif // __BANK
	}

//	m_PPPPresets->m_CurrentEffect->PostProcess();


	m_PPPPresets->m_CurrentEffect->Process(this);
 
	if(m_PPPPresets->m_ShowRenderTargets)
		ShowRenderTargets();
	grcEffect::SetDefaultSamplerState(grcessMINFILTER,savedMinFiler);
	grcEffect::SetDefaultSamplerState(grcessMAGFILTER,savedMagFiler);

#if __XENON
	if (GRCGPRALLOCATION)
	{
		// set the pixel shader threads to 112 -> vertex shader can only use 16 threads
		GRCGPRALLOCATION->SetGPRAllocation(oldGRPAlloc);
	}
#endif
	PIXEnd();
}

#if RAGE_SSAO
void grPostFX::SSAO()
{
	PIXBegin(0, "SSAO");

	if (!m_PPPPresets->m_EnableSSAO)
	{
		grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
		textureFactory.LockRenderTarget(0, m_SSAOPingPongRT[1], NULL);
		GRCDEVICE.Clear(true, Color32(0xff, 0xff, 0xff, 0xff), false, 1.0f, false, 0x0);
		textureFactory.UnlockRenderTarget(0);
		m_PostFXShader->SetVar(m_SSAOMapId, m_SSAOPingPongRT[1]);

		PIXEnd();
		return;
	}

	const grcViewport *vp = grcViewport::GetCurrent();
	float h=vp->GetTanHFOV();
	float v=vp->GetTanVFOV();
	float n=vp->GetNearClip();
	float f=vp->GetFarClip();
#if __XENON
	float Q = f/(f-n);
	float P = 1.0f/(-n*Q);
	float QPmP = Q*P-P;	
	Vector4 projParams=Vector4(h, v, P, QPmP);
#else
	float Q = f/(f-n);
	float P = 1.0f/(-n*Q);
	float QP = Q*P;
	Vector4 projParams=Vector4(h, v, P, QP);
#endif // __XENON
	m_PostFXShader->SetVar(m_SSAOProjectionParamsId, projParams);
	m_PostFXShader->SetVar(m_SSAOTweakParamsId, m_PPPPresets->m_SSAOTweakParams);
	PIXBegin(0, "SSAO - Downscale");
	BlitToRT(m_SSAOPingPongRT[0], m_DepthBuffer, m_SSAODownscaleTechnique, m_DepthMapId);
	PIXEnd();
	PIXBegin(0, "SSAO - Main");
	BlitToRT(m_SSAOPingPongRT[1], m_SSAOPingPongRT[0], m_SSAOMainTechnique, m_SSAOMapId);
	PIXEnd();
	PIXBegin(0, "SSAO - Blur X");
	BlitToRT(m_SSAOPingPongRT[0], m_SSAOPingPongRT[1], m_SSAOBlurXTechnique, m_SSAOMapId);
	PIXEnd();
	PIXBegin(0, "SSAO - Blur Y");
	BlitToRT(m_SSAOPingPongRT[1], m_SSAOPingPongRT[0], m_SSAOBlurYTechnique, m_SSAOMapId);
	PIXEnd();
	PIXBegin(0, "SSAO - Upscale");
	m_PostFXShader->SetVar(m_FullResMapId, m_BackBuffer);
	BlitToRT(m_BackBuffer, m_SSAOPingPongRT[1], m_SSAOUpscaleTechnique, m_RenderMapBilinearId);
	PIXEnd();

	PIXEnd();
}
#endif // RAGE_SSAO

template<class T>
bool IsValidPtr(T* ptr )
{
	return ( ptr != (T*)0 ) && ( ptr != (T*)0xCDCDCDCD );
}

// PURPOSE 
// : Renders the fog out as a separate pass so that it merged with transparencies correctly
//
// TODO : set it so that it points to the screen rendertarget
// TODO : make sure depth is set correctly
void grPostFX::RenderFogPass( const grcTexture* depthBuffer )
{

	Assert( IsValidPtr( depthBuffer ));

	// pass down the inverse FullCompositeMtx so that the PS can compute 
	// worldspace coordinates for height based fog.
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
	Mat44V modelViewInv;
	InvertFull(modelViewInv, grcViewport::GetCurrent()->GetFullCompositeMtx());

	m_PostFXShader->SetVar(m_ProjInverseTransposeId, RCC_MATRIX44(modelViewInv));

	// pass down fog parameters.
	m_PostFXShader->SetVar(m_FogMinColorId, m_PPPPresets->m_FogMinColor);
	m_PostFXShader->SetVar(m_FogMaxColorId, m_PPPPresets->m_FogMaxColor);
	m_PostFXShader->SetVar(m_FogParamsId, m_PPPPresets->m_FogParams);
	m_PostFXShader->SetVar(m_ParticleFogParamsId, m_PPPPresets->m_ParticleFogParams);

	
	PIXBegin(0, "Blended Fog Pass");
	// set depth buffer
	m_PostFXShader->SetVar(m_DepthMapId, depthBuffer); 	
	// the result of the light adaption
#if __XENON
	m_PostFXShader->SetVar(m_VertexTextureId, m_CurrentAdaptedLum1x1RT); 		
#else
#if __PPU
	m_PostFXShader->SetVar(m_AdaptedLuminanceId, m_AdaptedLuminance);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, m_CurrentAdaptedLum1x1RT); 		
#endif
#endif

	BlitToRT(NULL, depthBuffer,  m_BlendedFogTechnique, m_DepthMapId);


#if __XENON
	m_PostFXShader->SetVar(m_VertexTextureId, (grcTexture*)grcTexture::None);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, (grcTexture*)grcTexture::None); 		
#endif
	m_PostFXShader->SetVar(m_RenderMapBilinearId, (grcTexture*)grcTexture::None);
	m_PostFXShader->SetVar(m_RenderMapAnisoId, (grcTexture*)grcTexture::None);
	m_PostFXShader->SetVar(m_RenderMapId, (grcTexture*)grcTexture::None); 		
	PIXEnd();
}
// release all setup stuff
void grPostFX::EndPostProcess()
{
#if __PPU && HACK_MC4
	if (m_ResolveBuffer)
	{
		grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
		BlitToRT(textureFactory.GetFrontBuffer(), m_ResolveBuffer, m_DownSampleTechnique);
	}
#endif // __PPU && HACK_MC4
	
	grcState::Default();	

	if (PARAM_hdr.Get())
	{
		grcEffect::SetDefaultRenderState( grcersHIGHPRECISIONBLENDENABLE, false);
	}
	grcState::SetState( grcsHalfPixelOffset, false );

#if __PS3
	PopBackBuffer();
#endif

	// This forces a resolve next frame
	m_DepthBuffer = NULL;
	m_BackBuffer = NULL;
}

//static functions
void grPostFX::Init()
{
	Assert(sm_instance == NULL);
	sm_instance = rage_new grPostFX;
}

void grPostFX::Terminate()
{
	Assert(sm_instance);
	delete sm_instance;
	sm_instance = NULL;
}

// shows a render target as a quad on the screen for debugging purposes
// should be relative to the screen size
void grPostFX::ShowRT(grcTexture * texture, 
					  grcEffectTechnique technique,					// technique in FX file
					  float DestX, float DestY, float SizeX, float SizeY)								
{
	Color32 white(1.0f,1.0f,1.0f,1.0f);

	// set the chosen render target sampler stages; default is m_RenderMapId
	m_PostFXShader->SetVar(m_ToneMapId, texture);

	// provide the size of the target or source render target to the shader
	m_PostFXShader->SetVar(m_TexelSizeId, Vector4(1.0f/float(texture->GetWidth()),1.0f/float(texture->GetHeight()),(float)texture->GetWidth(), (float)texture->GetHeight()));

	// blit
	m_PostFXShader->TWODBlit(DestX,			// x1 - Destination base x
							DestY,			// y1 - Destination base y
							SizeX,			// x2 - Destination opposite-corner x
							SizeY,			// y1 - Destination opposite-corner y
							0.1f,							// z - Destination z value.  Note that the z value is expected to be in 0..1 space
							0.0f,							// u1 - Source texture base u (in normalized texels)
							0.0f,							// v1 - Source texture base v (in normalized texels)
							1.0f,							// u2 - Source texture opposite-corner u (in normalized texels)
							1.0f,							// v2 - Source texture opposite-corner v (in normalized texels)
							(Color32)white,				// color - reference to Packed color value
							technique);

	//	release texture again
	m_PostFXShader->SetVar(m_ToneMapId, (grcTexture*)grcTexture::None);
}

//
// Post-Processing Pipeline Visual Debugger
//
// shows render targets for debugging purposes
// is relative to the screen size
void grPostFX::ShowRenderTargets()
{
	// first get screen size
	float PositionUpperLeftCornerX = 0.1f;
	float PositionUpperLeftCornerY = 0.1777f;
	const float OriginX = (PositionUpperLeftCornerX) - (1.0f);
	const float OriginY =  (1.0f) - (PositionUpperLeftCornerY);

	// the preview window follows the screen ratio ... how cool is that?
	const float RectWidth = 0.20f;
	const float RectHeight = 0.20f;

	ShowRT(m_QuarterRT, 
		m_CopyTechnique,  
		OriginX,	// top left x
		OriginY,	// top left y 
		OriginX + RectWidth,	// bottom right x
		OriginY - RectHeight);	// bottom right y

	ShowRT(m_128x128RT, 
#if __XENON
		m_CopyRedTechnique,  
#else
		m_CopyTechnique,  
#endif
		OriginX,		// top left x
		OriginY - RectHeight,	// top left y 
		OriginX + RectWidth,			// bottom right x
		OriginY - 2 * RectHeight);	// bottom right y

	ShowRT(m_64x64RT, 
#if __XENON
		m_CopyRedTechnique,  
#else
		m_CopyTechnique,  
#endif
		OriginX,		// top left x
		OriginY - 2 *RectHeight,	// top left y 
		OriginX + RectWidth,			// bottom right x
		OriginY - 3 * RectHeight);	// bottom right y

	ShowRT(m_16x16RT, 
#if __XENON
		m_CopyRedTechnique,  
#else
		m_CopyTechnique,  
#endif
		OriginX,	// top left x
		OriginY - 3 * RectHeight,	// top left y 
		OriginX + RectWidth, // bottom right x
		OriginY - 4 * RectHeight);	// bottom right y

	ShowRT(m_4x4RT, 
#if __XENON
		m_CopyRedTechnique,  
#else
		m_CopyTechnique,  
#endif
		OriginX,	// top left x
		OriginY - 4 * RectHeight,	// top left y 
		OriginX + RectWidth, // bottom right x
		OriginY - 5 * RectHeight);	// bottom right y

	ShowRT(m_BrightPassRT, 
		m_CopyTechnique,  
		OriginX + RectWidth,	// top left x
		OriginY - RectHeight,	// top left y 
		OriginX + 2 * RectWidth, // bottom right x
		OriginY - 2 * RectHeight);	// bottom right y

	ShowRT(m_GaussRT, 
		m_CopyTechnique,  
		OriginX + RectWidth,	// top left x
		OriginY - 2 * RectHeight,	// top left y 
		OriginX + 2 * RectWidth, // bottom right x
		OriginY - 3 * RectHeight);	// bottom right y

	ShowRT(m_LumHistoryRT, 
#if __XENON
		m_CopyRedTechnique,  
#else
		m_CopyTechnique,  
#endif
		OriginX,	// top left x
		OriginY - 5 * RectHeight,	// top left y 
		OriginX + RectWidth, // bottom right x
		OriginY - 6 * RectHeight);	// bottom right y

	ShowRT(m_CurrentAdaptedLum1x1RT, 
#if __XENON
		m_CopyRedTechnique,  
#else
		m_CopyTechnique,  
#endif
		OriginX,	// top left x
		OriginY - 6 * RectHeight,	// top left y 
		OriginX + RectWidth, // bottom right x
		OriginY - 7 * RectHeight);	// bottom right y
#if __PPU
	ShowRT(m_DepthBuffer, m_CopyBlurFactorTechnique,
		OriginX + RectWidth,	// top left x
		OriginY - 4 * RectHeight,	// top left y 
		OriginX + 2 * RectWidth, // bottom right x
		OriginY - 5 * RectHeight);	// bottom right y
#elif __XENON
	ShowRT(m_DepthBuffer, m_CopyTechnique,
		OriginX + RectWidth,	// top left x
		OriginY - 4 * RectHeight,	// top left y 
		OriginX + 2 * RectWidth, // bottom right x
		OriginY - 5 * RectHeight);	// bottom right y
#endif
}

grPostFX::~grPostFX()
{
	while (m_Effects.GetCount())
		delete m_Effects.Pop();
	delete m_PPPPresets;

	for (int x=0; x<PPP_STACK_SIZE; x++)
		delete m_PPPStack[x];

	if(m_PersistentBrightPassCallback) delete m_PersistentBrightPassCallback;
	if(m_SeedBrightPassCallback) delete m_SeedBrightPassCallback;
	if(m_SeedFogPassCallback) delete m_SeedFogPassCallback;
	delete m_PostFXShader;
}

void grPostFX::DeleteRenderTargets()
{
#if __WIN32PC
	if (m_FSCopy)
	{
		m_FSCopy->Release();
		m_FSCopy = NULL;	
	}
	if (m_DepthBufferRT)
	{
		m_DepthBufferRT->Release();
		m_DepthBufferRT = NULL;	
	}
#endif
#if __PPU
	if (m_MRTBuffer)
	{
		m_MRTBuffer->Release();
		m_MRTBuffer = NULL;	
	}
#endif

#if RAGE_SSAO
	for (u32 i = 0; i < NELEM(m_SSAOPingPongRT); ++i)
	{
		if (m_SSAOPingPongRT[i])
		{
			m_SSAOPingPongRT[i]->Release();
			m_SSAOPingPongRT[i] = NULL;
		}
	}
#endif // RAGE_SSAO

	if ( m_FogMapRT )
	{
		m_FogMapRT->Release();
		m_FogMapRT = 0;
	}
	if (m_QuarterRT)
	{
		m_QuarterRT->Release();
		m_QuarterRT = NULL;	
	}
	if (m_ScratchQuarterRT)
	{
		m_ScratchQuarterRT->Release();
		m_ScratchQuarterRT = NULL;	
	}

	m_SecondaryFinalOutputRT = NULL;		// we don't own this render target
	if (m_128x128RT)
	{
		m_128x128RT->Release();
		m_128x128RT = NULL;	
	}

	if (m_64x64RT)
	{
		m_64x64RT->Release();
		m_64x64RT = NULL;	
	}

	if (m_16x16RT)
	{
		m_16x16RT->Release();
		m_16x16RT = NULL;	
	}
	if (m_4x4RT)
	{
		m_4x4RT->Release();
		m_4x4RT = NULL;	
	}
	if (m_LumHistoryRT)
	{
		m_LumHistoryRT->Release();
		m_LumHistoryRT = NULL;	
	}
	if (m_CurrentAdaptedLum1x1RT)
	{
		m_CurrentAdaptedLum1x1RT->Release();
		m_CurrentAdaptedLum1x1RT = NULL;	
	}
#if __WIN32PC
	if (m_LastAdaptedLum1x1RT)
	{
		m_LastAdaptedLum1x1RT->Release();
		m_LastAdaptedLum1x1RT = NULL;	
	}
#endif // __WIN32PC
	if (m_BrightPassRT)
	{
		m_BrightPassRT->Release();
		m_BrightPassRT = NULL;	
	}

	if (m_ScratchPadRT)
	{
		m_ScratchPadRT->Release();
		m_ScratchPadRT = NULL;	
	}

	if (m_QuarterRT)
	{
		m_QuarterRT->Release();
		m_QuarterRT = NULL;	
	}

	if (m_GaussRT)
	{
		m_GaussRT->Release();
		m_GaussRT = NULL;	
	}

	if (m_ScreenShot)
	{
		m_ScreenShot->Release();
		m_ScreenShot = NULL;
	}

	if (m_RandomMap)
	{
		m_RandomMap->Release();
		m_RandomMap = NULL;
	}
	if (m_LightGlowMap)
	{
		m_LightGlowMap->Release();
		m_LightGlowMap = NULL;
	}
#if PERSISTENT_BRIGHTPASS
	if (m_PersistentBrightPassRT)
	{
		m_PersistentBrightPassRT->Release();
		m_PersistentBrightPassRT = NULL;
	}
	if (m_PrevPersistentBrightPassRT)
	{
#if !__XENON
		m_PrevPersistentBrightPassRT->Release();
#endif
		m_PrevPersistentBrightPassRT = NULL;
	}
#endif
}

bool grPostFX::ParsePPPPreset(grcPPPPreset *pPPPPreset, parTreeNode *pRootNode) const
{

	// 	parTreeNode *pExposureNode = pRootNode->FindChildWithName(GRCPPPPRESET_EXPOSURE_PARAM);
	// 	if(pExposureNode)
	// 		sscanf(pExposureNode->GetData(), "%f", &pPPPPreset->m_Exposure);

	parTreeNode *pBrightPassThresholdNode = pRootNode->FindChildWithName(GRCPPPPRESET_BRIGHTPASSTHRESHOLD_PARAM);
	if(pBrightPassThresholdNode)
		sscanf(pBrightPassThresholdNode->GetData(), "%f", &pPPPPreset->m_BrightPassThreshold);

	parTreeNode *pBrightPassOffsetNode = pRootNode->FindChildWithName(GRCPPPPRESET_BRIGHTPASSOFFSET_PARAM);
	if(pBrightPassOffsetNode)
		sscanf(pBrightPassOffsetNode->GetData(), "%f", &pPPPPreset->m_BrightPassOffset);

	parTreeNode *pBrightPassWhite = pRootNode->FindChildWithName(GRCPPPPRESET_BRIGHTPASSWHITE_PARAM);
	if(pBrightPassWhite)
		sscanf(pBrightPassWhite->GetData(), "%f", &pPPPPreset->m_fBrightPassWhite);

	parTreeNode *pMiddleGrayNode = pRootNode->FindChildWithName(GRCPPPPRESET_MIDDLEGRAY_PARAM);
	if(pMiddleGrayNode)
		sscanf(pMiddleGrayNode->GetData(), "%f", &pPPPPreset->m_MiddleGray);

	parTreeNode *pBrightPassThresholdNodeTwo = pRootNode->FindChildWithName(GRCPPPPRESET_BRIGHTPASSTHRESHOLDTWO_PARAM);
	if(pBrightPassThresholdNodeTwo)
		sscanf(pBrightPassThresholdNodeTwo->GetData(), "%f", &pPPPPreset->m_BrightPassThresholdTwo);

	parTreeNode *pBrightPassOffsetNodeTwo = pRootNode->FindChildWithName(GRCPPPPRESET_BRIGHTPASSOFFSETTWO_PARAM);
	if(pBrightPassOffsetNodeTwo)
		sscanf(pBrightPassOffsetNodeTwo->GetData(), "%f", &pPPPPreset->m_BrightPassOffsetTwo);

	parTreeNode *pBrightPassWhiteTwo = pRootNode->FindChildWithName(GRCPPPPRESET_BRIGHTPASSWHITETWO_PARAM);
	if(pBrightPassWhiteTwo)
		sscanf(pBrightPassWhiteTwo->GetData(), "%f", &pPPPPreset->m_fBrightPassWhiteTwo);

	parTreeNode *pNumOfFramesInLumHistory = pRootNode->FindChildWithName(GRCPPPPRESET_NUMOFFRAMESINLUMHISTORY_PARAM);
	if(pNumOfFramesInLumHistory)
		sscanf(pNumOfFramesInLumHistory->GetData(), "%d", &pPPPPreset->m_iNumOfFramesInLumHistory);

	parTreeNode *pMiddleGrayNodeTwo = pRootNode->FindChildWithName(GRCPPPPRESET_MIDDLEGRAYTWO_PARAM);
	if(pMiddleGrayNodeTwo)
		sscanf(pMiddleGrayNodeTwo->GetData(), "%f", &pPPPPreset->m_MiddleGrayTwo);

	parTreeNode *pAdaptNode = pRootNode->FindChildWithName(GRCPPPPRESET_ADAPT_PARAM);
	if(pAdaptNode)
		sscanf(pAdaptNode->GetData(), "%f", &pPPPPreset->m_fAdapt);

	parTreeNode *pBloomIntensityNode = pRootNode->FindChildWithName(GRCPPPPRESET_BLOOMINTENSITY_PARAM);
	if(pBloomIntensityNode)
		sscanf(pBloomIntensityNode->GetData(), "%f", &pPPPPreset->m_fBloomIntensity);

	parTreeNode *pClearColorNode = pRootNode->FindChildWithName(GRCPPPPRESET_CLEARCOLOR_PARAM);
	if(pClearColorNode)
		pPPPPreset->m_ClearColor = *(vector_cast<Vector4*>(pClearColorNode->GetData()));

	parTreeNode *pColorCorrectNode = pRootNode->FindChildWithName(GRCPPPPRESET_COLORCORRECT_PARAM);
	if(pColorCorrectNode)
		pPPPPreset->m_ColorCorrect = *(vector_cast<Vector4*>(pColorCorrectNode->GetData()));

	parTreeNode *pColorAddNode = pRootNode->FindChildWithName(GRCPPPPRESET_COLORADD_PARAM);
	if(pColorAddNode)
		pPPPPreset->m_ConstAdd = *(vector_cast<Vector4*>(pColorAddNode->GetData()));

	parTreeNode *pSat = pRootNode->FindChildWithName(GRCPPPPRESET_SAT_PARAM);
	if(pSat)
		sscanf(pSat->GetData(), "%f", &pPPPPreset->m_deSat);

	parTreeNode *pSatTwo = pRootNode->FindChildWithName(GRCPPPPRESET_SATTWO_PARAM);
	if(pSatTwo)
		sscanf(pSatTwo->GetData(), "%f", &pPPPPreset->m_deSatTwo);

	parTreeNode *pScale = pRootNode->FindChildWithName(GRCPPPPRESET_SCALE_PARAM);
	if(pScale)
		pPPPPreset->m_Scale = *(vector_cast<Vector3*>(pScale->GetData()));

	parTreeNode *pWhite = pRootNode->FindChildWithName(GRCPPPPRESET_RHO_PARAM);
	if(pWhite)
		sscanf(pWhite->GetData(), "%f", &pPPPPreset->m_fWhite);

	parTreeNode *pContrast = pRootNode->FindChildWithName(GRCPPPPRESET_CONTRAST_PARAM);
	if(pContrast)
		sscanf(pContrast->GetData(), "%f", &pPPPPreset->m_fContrast);

	parTreeNode *pContrastTwo = pRootNode->FindChildWithName(GRCPPPPRESET_CONTRASTTWO_PARAM);
	if(pContrastTwo)
		sscanf(pContrastTwo->GetData(), "%f", &pPPPPreset->m_fContrastTwo);

	parTreeNode *pFrameSharpness = pRootNode->FindChildWithName(GRCPPPPRESET_GRAININTENSITY_PARAM);
	if(pFrameSharpness)
		sscanf(pFrameSharpness->GetData(), "%f", &pPPPPreset->m_fGrainIntensity);

	parTreeNode *pInterlaceIntensity = pRootNode->FindChildWithName(GRCPPPPRESET_INTERLACEINTENSITY_PARAM);
	if(pInterlaceIntensity)
		sscanf(pInterlaceIntensity->GetData(), "%f", &pPPPPreset->m_fInterlaceIntensity);

	parTreeNode *pTimeFrame = pRootNode->FindChildWithName(GRCPPPPRESET_TIMEFRAME_PARAM);
	if(pTimeFrame)
		sscanf(pTimeFrame->GetData(), "%f", &pPPPPreset->m_fTimeFrame);

	parTreeNode *pInterference = pRootNode->FindChildWithName(GRCPPPPRESET_INTERFERENCE_PARAM);
	if(pInterference)
		sscanf(pInterference->GetData(), "%f", &pPPPPreset->m_fInterference);

	if ( m_showGammaControl )
	{
		parTreeNode *pGamma = pRootNode->FindChildWithName(GRCPPPPRESET_GAMMA_PARAM);
		if(pGamma)
			sscanf(pGamma->GetData(), "%f", &pPPPPreset->m_fGamma);
	}

	parTreeNode *pLowerLimitAdaption = pRootNode->FindChildWithName(GRCPPPPRESET_LOWERLIMITADAPTION_PARAM);
	if(pLowerLimitAdaption)
		sscanf(pLowerLimitAdaption->GetData(), "%f", &pPPPPreset->m_fLowerLimitAdaption);

	parTreeNode *pHigherLimitAdaption = pRootNode->FindChildWithName(GRCPPPPRESET_HIGHERLIMITADAPTION_PARAM);
	if(pHigherLimitAdaption)
		sscanf(pHigherLimitAdaption->GetData(), "%f", &pPPPPreset->m_fHigherLimitAdaption);

	parTreeNode *pChooseTechnique = pRootNode->FindChildWithName(GRCPPPPRESET_CHOOSETECHNIQUE_PARAM);
	if(pChooseTechnique) {
		char chooseTechniqueName[128];
		sscanf(pChooseTechnique->GetData(), "%s", chooseTechniqueName);
		pPPPPreset->SetChooseTechnique(chooseTechniqueName);

		sscanf(pChooseTechnique->GetData(), "%d", (int*)&pPPPPreset->m_iChooseTechnique);
	}

	parTreeNode *pDofParamters = pRootNode->FindChildWithName(GRCPPPPRESET_DOFPARAMETERS_PARAM);
	if(pDofParamters)
		pPPPPreset->m_DofParamters = *(vector_cast<Vector4*>(pDofParamters->GetData()));

	parTreeNode *pFogMinColor = pRootNode->FindChildWithName(GRCPPPPRESET_FOGMINCOLOR_PARAM);
	if(pFogMinColor)
		pPPPPreset->m_FogMinColor = *(vector_cast<Vector4*>(pFogMinColor->GetData()));

	parTreeNode *pFogMaxColor = pRootNode->FindChildWithName(GRCPPPPRESET_FOGMAXCOLOR_PARAM);
	if(pFogMaxColor)
		pPPPPreset->m_FogMaxColor = *(vector_cast<Vector4*>(pFogMaxColor->GetData()));

	parTreeNode *pFogParams = pRootNode->FindChildWithName(GRCPPPPRESET_FOGPARAMETERS_PARAM);
	if(pFogParams)
		pPPPPreset->m_FogParams = *(vector_cast<Vector4*>(pFogParams->GetData()));

	parTreeNode *pBrightPassParams = pRootNode->FindChildWithName(GRCPPPPRESET_BRIGHTPASSPARAMETERS_PARAM);
	if(pBrightPassParams)
		pPPPPreset->m_BrightPassParams = *(vector_cast<Vector4*>(pBrightPassParams->GetData()));

	parTreeNode *pTemp = pRootNode->FindChildWithName(GRCPPPPRESET_STREAKSAMPLES_PARAM);
	if(pTemp)
		sscanf(pTemp->GetData(), "%f", &pPPPPreset->m_StreakSamples);

	pTemp = pRootNode->FindChildWithName(GRCPPPPRESET_STREAKITERATIONS_PARAM);
	if(pTemp)
		sscanf(pTemp->GetData(), "%d", &pPPPPreset->m_StreakIterations);

	pTemp = pRootNode->FindChildWithName(GRCPPPPRESET_KAW_BLOOMITERATIONS_PARAM);
	if(pTemp)
		sscanf(pTemp->GetData(), "%d", &pPPPPreset->m_BloomIterations);

	pTemp = pRootNode->FindChildWithName(GRCPPPPRESET_KAW_BLOOMINTENSITY_PARAM);
	if(pTemp)
		sscanf(pTemp->GetData(), "%f", &pPPPPreset->m_BloomIntensity);

	pTemp = pRootNode->FindChildWithName(GRCPPPPRESET_STREAKINTENSITY_PARAM);
	if(pTemp)
		sscanf(pTemp->GetData(), "%f", &pPPPPreset->m_StreakIntensity);

	parTreeNode *pShowRenderTargets = pRootNode->FindChildWithName(GRCPPPPRESET_SHOWRENDERTARGETS_PARAM);

	Assert(pShowRenderTargets);
	if(strcmp(pShowRenderTargets->GetData(), "true")==0)
		pPPPPreset->m_ShowRenderTargets = true;
	else
		pPPPPreset->m_ShowRenderTargets = false;

	parTreeNode *pShowPoliceCamera = pRootNode->FindChildWithName(GRCPPPPRESET_SWITCHPOLICECAMERA_PARAM);

	if(pShowPoliceCamera)
	{
		if(strcmp(pShowPoliceCamera->GetData(), "true")==0)
			pPPPPreset->m_SwitchPoliceCam = true;
		else
			pPPPPreset->m_SwitchPoliceCam = false;
	}

	pTemp = pRootNode->FindChildWithName(GRCPPPPRESET_DOF_BLURTYPE_PARAM);
	if (pTemp)
		sscanf(pTemp->GetData(), "%u", (unsigned int*)&m_DOFBlurType);

	parTreeNode *pBlueShiftColor = pRootNode->FindChildWithName(GRCPPPPRESET_BLUESHIFTCOLOR_PARAM);
	if (pBlueShiftColor)
		pPPPPreset->m_BlueShiftColor = *(vector_cast<Vector4*>(pBlueShiftColor->GetData()));

	parTreeNode *pBlueShiftParam = pRootNode->FindChildWithName(GRCPPPPRESET_BLUESHIFTPARAMS_PARAM);
	if (pBlueShiftParam)
		pPPPPreset->m_BlueShiftParams = *(vector_cast<Vector4*>(pBlueShiftParam->GetData()));

	return true;
}

bool grPostFX::LoadPPPPreset(grcPPPPreset *pPPPPreset, const char* filename) const
{
	Assert(pPPPPreset);
	Assert(filename);

	//Load the XML "ppp" file
	parTree* pTree = PARSER.LoadTree(filename, "");
	if(!pTree)
		return false;

	parTreeNode *pRootNode = pTree->GetRoot();
	if(strcmp(pRootNode->GetElement().GetName(), GRCPPPPRESET_PPP_STRUCTURE) != 0)
	{
		Errorf("Failed to load post-processing preset file '%s', file is not a valid post-processing preset file.", filename);
		return false;
	}

	/* int presetVesion;
	parAttribute* pVersionAttr = pRootNode->GetElement().FindAttribute(GRCPPPPRESET_VERSION_ATTR);
	if(!pVersionAttr)
		presetVesion = 1;
	else
	{
		pVersionAttr->ConvertToInt();
		presetVesion = pVersionAttr->GetIntValue();
	} */

	bool result = ParsePPPPreset(pPPPPreset, pRootNode);

	delete pTree;

	// adickinson [12/8/2005 16:16] changed from false to true for consistency
	return result;	
}

#if __BANK

void grPostFX::LoadPPPPresetBankCbk()
{
	char selFileName[256];
	memset( selFileName, 0, 256 );

	if ( BANKMGR.OpenFile(selFileName,256,"*.ppp", false, "Post-Processing Pipeline Presets (*.ppp)") )
	{
		LoadPPPPreset( selFileName );
	}
}
#endif // __BANK

bool grPostFX::StorePPPPreset(const char* filename) const
{
	Assert(filename);

	parTree* pTree = rage_new parTree();
	parTreeNode *pRootNode = pTree->CreateRoot();
	parElement &rootElm = pRootNode->GetElement();
	rootElm.SetName(GRCPPPPRESET_PPP_STRUCTURE);
	rootElm.AddAttribute(GRCPPPPRESET_VERSION_ATTR, GRCPPPPRESET_VERSION, false, false);
	pTree->SetRoot(pRootNode);

	WritePPPPreset(pRootNode);

	bool bSaveRes = PARSER.SaveTree(filename,"",pTree,parManager::XML);
	if(!bSaveRes)
		Errorf("Failed to save post-processing preset file '%s'.",filename);

	delete pTree;

	return bSaveRes;
}



bool grPostFX::WritePPPPreset(parTreeNode *pRootNode) const
{
	char tmpBuf[255];

	parTreeNode *pBrightPassThreshold = rage_new parTreeNode();
	pBrightPassThreshold->GetElement().SetName(GRCPPPPRESET_BRIGHTPASSTHRESHOLD_PARAM);
	pBrightPassThreshold->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_BrightPassThreshold);
	pBrightPassThreshold->SetData(tmpBuf, (int)strlen(tmpBuf));
	pBrightPassThreshold->AppendAsChildOf(pRootNode);

	parTreeNode *pBrightPassOffset = rage_new parTreeNode();
	pBrightPassOffset->GetElement().SetName(GRCPPPPRESET_BRIGHTPASSOFFSET_PARAM);
	pBrightPassOffset->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_BrightPassOffset);
	pBrightPassOffset->SetData(tmpBuf, (int)strlen(tmpBuf));
	pBrightPassOffset->AppendAsChildOf(pRootNode);

	parTreeNode *pBrightPassWhite = rage_new parTreeNode();
	pBrightPassWhite->GetElement().SetName(GRCPPPPRESET_BRIGHTPASSWHITE_PARAM);
	pBrightPassWhite->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fBrightPassWhite);
	pBrightPassWhite->SetData(tmpBuf, (int)strlen(tmpBuf));
	pBrightPassWhite->AppendAsChildOf(pRootNode);


	parTreeNode *pMiddleGray = rage_new parTreeNode();
	pMiddleGray->GetElement().SetName(GRCPPPPRESET_MIDDLEGRAY_PARAM);
	pMiddleGray->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_MiddleGray);
	pMiddleGray->SetData(tmpBuf, (int)strlen(tmpBuf));
	pMiddleGray->AppendAsChildOf(pRootNode);

	parTreeNode *pBrightPassThresholdTwo = rage_new parTreeNode();
	pBrightPassThresholdTwo->GetElement().SetName(GRCPPPPRESET_BRIGHTPASSTHRESHOLDTWO_PARAM);
	pBrightPassThresholdTwo->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_BrightPassThresholdTwo);
	pBrightPassThresholdTwo->SetData(tmpBuf, (int)strlen(tmpBuf));
	pBrightPassThresholdTwo->AppendAsChildOf(pRootNode);

	parTreeNode *pBrightPassOffsetTwo = rage_new parTreeNode();
	pBrightPassOffsetTwo->GetElement().SetName(GRCPPPPRESET_BRIGHTPASSOFFSETTWO_PARAM);
	pBrightPassOffsetTwo->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_BrightPassOffsetTwo);
	pBrightPassOffsetTwo->SetData(tmpBuf, (int)strlen(tmpBuf));
	pBrightPassOffsetTwo->AppendAsChildOf(pRootNode);

	parTreeNode *pBrightPassWhiteTwo = rage_new parTreeNode();
	pBrightPassWhiteTwo->GetElement().SetName(GRCPPPPRESET_BRIGHTPASSWHITETWO_PARAM);
	pBrightPassWhiteTwo->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fBrightPassWhiteTwo);
	pBrightPassWhiteTwo->SetData(tmpBuf, (int)strlen(tmpBuf));
	pBrightPassWhiteTwo->AppendAsChildOf(pRootNode);

	parTreeNode *pNumOfFramesInLumHistory = rage_new parTreeNode();
	pNumOfFramesInLumHistory->GetElement().SetName(GRCPPPPRESET_NUMOFFRAMESINLUMHISTORY_PARAM);
	pNumOfFramesInLumHistory->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%d", m_PPPPresets->m_iNumOfFramesInLumHistory);
	pNumOfFramesInLumHistory->SetData(tmpBuf, (int)strlen(tmpBuf));
	pNumOfFramesInLumHistory->AppendAsChildOf(pRootNode);


	parTreeNode *pMiddleGrayTwo = rage_new parTreeNode();
	pMiddleGrayTwo->GetElement().SetName(GRCPPPPRESET_MIDDLEGRAYTWO_PARAM);
	pMiddleGrayTwo->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_MiddleGrayTwo);
	pMiddleGrayTwo->SetData(tmpBuf, (int)strlen(tmpBuf));
	pMiddleGrayTwo->AppendAsChildOf(pRootNode);

	parTreeNode *pfAdapt = rage_new parTreeNode();
	pfAdapt->GetElement().SetName(GRCPPPPRESET_ADAPT_PARAM);
	pfAdapt->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fAdapt);
	pfAdapt->SetData(tmpBuf, (int)strlen(tmpBuf));
	pfAdapt->AppendAsChildOf(pRootNode);

	parTreeNode *pBloomIntensity = rage_new parTreeNode();
	pBloomIntensity->GetElement().SetName(GRCPPPPRESET_BLOOMINTENSITY_PARAM);
	pBloomIntensity->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fBloomIntensity);
	pBloomIntensity->SetData(tmpBuf, (int)strlen(tmpBuf));
	pBloomIntensity->AppendAsChildOf(pRootNode);

	parTreeNode *pClearColor = rage_new parTreeNode();
	pClearColor->GetElement().SetName(GRCPPPPRESET_CLEARCOLOR_PARAM);
	pClearColor->GetElement().AddAttribute("content", "vector4_array", false, false);
	pClearColor->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_ClearColor), sizeof(Vector4));
	pClearColor->AppendAsChildOf(pRootNode);

	parTreeNode *pColorCorrect = rage_new parTreeNode();
	pColorCorrect->GetElement().SetName(GRCPPPPRESET_COLORCORRECT_PARAM);
	pColorCorrect->GetElement().AddAttribute("content", "vector4_array", false, false);
	pColorCorrect->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_ColorCorrect), sizeof(Vector4));
	pColorCorrect->AppendAsChildOf(pRootNode);

	parTreeNode *pConstAdd = rage_new parTreeNode();
	pConstAdd->GetElement().SetName(GRCPPPPRESET_COLORADD_PARAM);
	pConstAdd->GetElement().AddAttribute("content", "vector4_array", false, false);
	pConstAdd->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_ConstAdd), sizeof(Vector4));
	pConstAdd->AppendAsChildOf(pRootNode);

	parTreeNode *pdeSat = rage_new parTreeNode();
	pdeSat->GetElement().SetName(GRCPPPPRESET_SAT_PARAM);
	pdeSat->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_deSat);
	pdeSat->SetData(tmpBuf, (int)strlen(tmpBuf));
	pdeSat->AppendAsChildOf(pRootNode);

	parTreeNode *pdeSatTwo = rage_new parTreeNode();
	pdeSatTwo->GetElement().SetName(GRCPPPPRESET_SATTWO_PARAM);
	pdeSatTwo->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_deSatTwo);
	pdeSatTwo->SetData(tmpBuf, (int)strlen(tmpBuf));
	pdeSatTwo->AppendAsChildOf(pRootNode);

	parTreeNode *pScale = rage_new parTreeNode();
	pScale->GetElement().SetName(GRCPPPPRESET_SCALE_PARAM);
	pScale->GetElement().AddAttribute("content", "vector3_array", false, false);
	pScale->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_Scale), sizeof(Vector3));
	pScale->AppendAsChildOf(pRootNode);

	parTreeNode *pWhite = rage_new parTreeNode();
	pWhite->GetElement().SetName(GRCPPPPRESET_RHO_PARAM);
	pWhite->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fWhite);
	pWhite->SetData(tmpBuf, (int)strlen(tmpBuf));
	pWhite->AppendAsChildOf(pRootNode);

	parTreeNode *pContrast = rage_new parTreeNode();
	pContrast->GetElement().SetName(GRCPPPPRESET_CONTRAST_PARAM);
	pContrast->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fContrast);
	pContrast->SetData(tmpBuf, (int)strlen(tmpBuf));
	pContrast->AppendAsChildOf(pRootNode);

	parTreeNode *pContrastTwo = rage_new parTreeNode();
	pContrastTwo->GetElement().SetName(GRCPPPPRESET_CONTRASTTWO_PARAM);
	pContrastTwo->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fContrastTwo);
	pContrastTwo->SetData(tmpBuf, (int)strlen(tmpBuf));
	pContrastTwo->AppendAsChildOf(pRootNode);

	parTreeNode *pFrameSharpness = rage_new parTreeNode();
	pFrameSharpness->GetElement().SetName(GRCPPPPRESET_GRAININTENSITY_PARAM);
	pFrameSharpness->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fGrainIntensity);
	pFrameSharpness->SetData(tmpBuf, (int)strlen(tmpBuf));
	pFrameSharpness->AppendAsChildOf(pRootNode);

	parTreeNode *pInterlaceIntensity = rage_new parTreeNode();
	pInterlaceIntensity->GetElement().SetName(GRCPPPPRESET_INTERLACEINTENSITY_PARAM);
	pInterlaceIntensity->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fInterlaceIntensity);
	pInterlaceIntensity->SetData(tmpBuf, (int)strlen(tmpBuf));
	pInterlaceIntensity->AppendAsChildOf(pRootNode);

	parTreeNode *pTimeFrame = rage_new parTreeNode();
	pTimeFrame->GetElement().SetName(GRCPPPPRESET_TIMEFRAME_PARAM);
	pTimeFrame->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fTimeFrame);
	pTimeFrame->SetData(tmpBuf, (int)strlen(tmpBuf));
	pTimeFrame->AppendAsChildOf(pRootNode);

	if ( m_showGammaControl )
	{
		parTreeNode *pGamma = rage_new parTreeNode();
		pGamma->GetElement().SetName(GRCPPPPRESET_GAMMA_PARAM);
		pGamma->GetElement().AddAttribute("content", "ascii", false, false);
		sprintf(tmpBuf,"%f", m_PPPPresets->m_fGamma);
		pGamma->SetData(tmpBuf, (int)strlen(tmpBuf));
		pGamma->AppendAsChildOf(pRootNode);
	}


	parTreeNode *pLowerLimitAdaption = rage_new parTreeNode();
	pLowerLimitAdaption->GetElement().SetName(GRCPPPPRESET_LOWERLIMITADAPTION_PARAM);
	pLowerLimitAdaption->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fLowerLimitAdaption);
	pLowerLimitAdaption->SetData(tmpBuf, (int)strlen(tmpBuf));
	pLowerLimitAdaption->AppendAsChildOf(pRootNode);

	parTreeNode *pHigherLimitAdaption = rage_new parTreeNode();
	pHigherLimitAdaption->GetElement().SetName(GRCPPPPRESET_HIGHERLIMITADAPTION_PARAM);
	pHigherLimitAdaption->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fHigherLimitAdaption);
	pHigherLimitAdaption->SetData(tmpBuf, (int)strlen(tmpBuf));
	pHigherLimitAdaption->AppendAsChildOf(pRootNode);

	parTreeNode *pInterference = rage_new parTreeNode();
	pInterference->GetElement().SetName(GRCPPPPRESET_INTERFERENCE_PARAM);
	pInterference->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_fInterference);
	pInterference->SetData(tmpBuf, (int)strlen(tmpBuf));
	pInterference->AppendAsChildOf(pRootNode);

	parTreeNode *pChooseTechnique = rage_new parTreeNode();
	pChooseTechnique->GetElement().SetName(GRCPPPPRESET_CHOOSETECHNIQUE_PARAM);
	pChooseTechnique->GetElement().AddAttribute("content", "ascii", false, false);
	m_PPPPresets->GetChooseTechniqueName(tmpBuf, sizeof(tmpBuf), m_PPPPresets->m_CurrentEffect);
	pChooseTechnique->SetData(tmpBuf, (int)strlen(tmpBuf));
	pChooseTechnique->AppendAsChildOf(pRootNode);

	parTreeNode *pDofParamters = rage_new parTreeNode();
	pDofParamters->GetElement().SetName(GRCPPPPRESET_DOFPARAMETERS_PARAM);
	pDofParamters->GetElement().AddAttribute("content", "vector4_array", false, false);
	pDofParamters->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_DofParamters), sizeof(Vector4));
	pDofParamters->AppendAsChildOf(pRootNode);

	parTreeNode *pFogMinColor = rage_new parTreeNode();
	pFogMinColor->GetElement().SetName(GRCPPPPRESET_FOGMINCOLOR_PARAM);
	pFogMinColor->GetElement().AddAttribute("content", "vector4_array", false, false);
	pFogMinColor->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_FogMinColor), sizeof(Vector4));
	pFogMinColor->AppendAsChildOf(pRootNode);

	parTreeNode *pFogMaxColor = rage_new parTreeNode();
	pFogMaxColor->GetElement().SetName(GRCPPPPRESET_FOGMAXCOLOR_PARAM);
	pFogMaxColor->GetElement().AddAttribute("content", "vector4_array", false, false);
	pFogMaxColor->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_FogMaxColor), sizeof(Vector4));
	pFogMaxColor->AppendAsChildOf(pRootNode);

	parTreeNode *pFogParams = rage_new parTreeNode();
	pFogParams->GetElement().SetName(GRCPPPPRESET_FOGPARAMETERS_PARAM);
	pFogParams->GetElement().AddAttribute("content", "vector4_array", false, false);
	pFogParams->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_FogParams), sizeof(Vector4));
	pFogParams->AppendAsChildOf(pRootNode);

	parTreeNode *pBrightPassParams = rage_new parTreeNode();
	pBrightPassParams->GetElement().SetName(GRCPPPPRESET_BRIGHTPASSPARAMETERS_PARAM);
	pBrightPassParams->GetElement().AddAttribute("content", "vector4_array", false, false);
	pBrightPassParams->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_BrightPassParams), sizeof(Vector4));
	pBrightPassParams->AppendAsChildOf(pRootNode);

	parTreeNode *pTemp = rage_new parTreeNode();
	pTemp->GetElement().SetName(GRCPPPPRESET_STREAKSAMPLES_PARAM);
	pTemp->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_StreakSamples);
	pTemp->SetData(tmpBuf, (int)strlen(tmpBuf));
	pTemp->AppendAsChildOf(pRootNode);

	pTemp = rage_new parTreeNode();
	pTemp->GetElement().SetName(GRCPPPPRESET_STREAKITERATIONS_PARAM);
	pTemp->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%d", m_PPPPresets->m_StreakIterations);
	pTemp->SetData(tmpBuf, (int)strlen(tmpBuf));
	pTemp->AppendAsChildOf(pRootNode);

	pTemp = rage_new parTreeNode();
	pTemp->GetElement().SetName(GRCPPPPRESET_KAW_BLOOMITERATIONS_PARAM);
	pTemp->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%d", m_PPPPresets->m_BloomIterations);
	pTemp->SetData(tmpBuf, (int)strlen(tmpBuf));
	pTemp->AppendAsChildOf(pRootNode);

	pTemp = rage_new parTreeNode();
	pTemp->GetElement().SetName(GRCPPPPRESET_KAW_BLOOMINTENSITY_PARAM);
	pTemp->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_BloomIntensity);
	pTemp->SetData(tmpBuf, (int)strlen(tmpBuf));
	pTemp->AppendAsChildOf(pRootNode);

	pTemp = rage_new parTreeNode();
	pTemp->GetElement().SetName(GRCPPPPRESET_STREAKINTENSITY_PARAM);
	pTemp->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf,"%f", m_PPPPresets->m_StreakIntensity);
	pTemp->SetData(tmpBuf, (int)strlen(tmpBuf));
	pTemp->AppendAsChildOf(pRootNode);

	parTreeNode *pShowRenderTargets = rage_new parTreeNode();
	pShowRenderTargets->GetElement().SetName(GRCPPPPRESET_SHOWRENDERTARGETS_PARAM);
	pShowRenderTargets->GetElement().AddAttribute("content", "ascii", false ,false);
	if(m_PPPPresets->m_ShowRenderTargets)
		pShowRenderTargets->SetData("true", 4);
	else
		pShowRenderTargets->SetData("false", 5);
	pShowRenderTargets->AppendAsChildOf(pRootNode);

	parTreeNode *pShowPoliceCamera = rage_new parTreeNode();
	pShowPoliceCamera->GetElement().SetName(GRCPPPPRESET_SWITCHPOLICECAMERA_PARAM);
	pShowPoliceCamera->GetElement().AddAttribute("content", "ascii", false ,false);
	if(m_PPPPresets->m_SwitchPoliceCam)
		pShowPoliceCamera->SetData("true", 4);
	else
		pShowPoliceCamera->SetData("false", 5);
	pShowPoliceCamera->AppendAsChildOf(pRootNode);

// 	if (m_saveDOFTypeWithPresets) 
// 	{ 
// 		parTreeNode *pDOFBlurType = rage_new parTreeNode(); 
// 		pDOFBlurType->GetElement().SetName(GRCPPPPRESET_DOF_BLURTYPE_PARAM); 
// 		pDOFBlurType->GetElement().AddAttribute("content", "ascii", false, false); 
// 		sprintf(tmpBuf, "%u", m_DOFBlurType); 
// 		pDOFBlurType->SetData(tmpBuf, (int)strlen(tmpBuf)); 
// 		pDOFBlurType->AppendAsChildOf(pRootNode); 
// 	} 	   

	parTreeNode *pBlueShiftColor = rage_new parTreeNode(); 
	pBlueShiftColor->GetElement().SetName(GRCPPPPRESET_BLUESHIFTCOLOR_PARAM); 
	pBlueShiftColor->GetElement().AddAttribute("content", "vector4_array", false, false); 
	pBlueShiftColor->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_BlueShiftColor), sizeof(Vector4)); 
	pBlueShiftColor->AppendAsChildOf(pRootNode); 
	   
	parTreeNode *pBlueShiftParams = rage_new parTreeNode(); 
	pBlueShiftParams->GetElement().SetName(GRCPPPPRESET_BLUESHIFTPARAMS_PARAM); 
	pBlueShiftParams->GetElement().AddAttribute("content", "vector4_array", false, false); 
	pBlueShiftParams->SetData(reinterpret_cast<const char*>(&m_PPPPresets->m_BlueShiftParams), sizeof(Vector4)); 
	pBlueShiftParams->AppendAsChildOf(pRootNode); 

	return true;
}

#if __BANK
void grPostFX::SavePPPPresetBankCbk()
{
	char selFileName[256];
	memset( selFileName, 0, 256 );

	if ( BANKMGR.OpenFile(selFileName,256,"*.ppp", true, "PPP Presets (*.ppp)") )
	{
		SavePPPPreset( selFileName );
	}
}

void grPostFX::UpdateTechniqueSpecificWidgets()
{
	m_PPPPresets->m_CurrentEffect = m_Effects[m_PPPPresets->m_iChooseTechnique];
	if ( m_pBank )
	{
		AddTechniqueSpecificWidgets( *m_pBank );
	}
}

const char* grPostFX::sm_BrightPassBlurNames[] =
{
	"None",
	"5-tap",
	"9-tap",
	"17-tap",
	"double 9",
	"double 17"
};

void grPostFX::AddWidgets( bkBank &bk )
{
	m_pBank = &bk;

	bk.AddToggle("Allow game to override settings", &m_allowGameOverride, NullCB, "When disabled does not let game code override postfx settings, so you can tune all these widgets by hand.");

	bk.AddCombo("Effects", &m_PPPPresets->m_iChooseTechnique, m_Effects.GetCount(), m_ChooseTechniqueNames, 
		datCallback(MFA(grPostFX::UpdateTechniqueSpecificWidgets),this), "Pre-defined post-processing techniques." );
	bk.AddToggle("Visual Render Target Debugger", &m_PPPPresets->m_ShowRenderTargets, NullCB, "Show the different stages of the post-process rendering pipeline." );

	if ( bkRemotePacket::IsConnectedToRag() )
	{
		bk.AddColor("Clear Color",reinterpret_cast<Vector3*>(&m_PPPPresets->m_ClearColor), NullCB, "Global color correction." );
		bk.AddColor("Color Correction",reinterpret_cast<Vector3*>(&m_PPPPresets->m_ColorCorrect), NullCB, "Global color correction." );
		bk.AddColor("Color Addition",reinterpret_cast<Vector3*>(&m_PPPPresets->m_ConstAdd), NullCB, "Global color additive." );	}
	else
	{
		bk.AddSlider("Clear Color Red Channel", &m_PPPPresets->m_ClearColor.x, 0.0f, 1.0f, 0.01f, NullCB, "Global color correction (Red)." );
		bk.AddSlider("Clear Color Green Channel", &m_PPPPresets->m_ClearColor.y, 0.0f, 1.0f, 0.01f, NullCB, "Global color correction (Green)." );
		bk.AddSlider("Clear Color Blue Channel", &m_PPPPresets->m_ClearColor.z, 0.0f, 1.0f, 0.01f, NullCB, "Global color correction (Blue)." );

		bk.AddSlider("Color Correction Red Channel", &m_PPPPresets->m_ColorCorrect.x, 0.0f, 1.0f, 0.01f, NullCB, "Global color correction (Red)." );
		bk.AddSlider("Color Correction Green Channel", &m_PPPPresets->m_ColorCorrect.y, 0.0f, 1.0f, 0.01f, NullCB, "Global color correction (Green)." );
		bk.AddSlider("Color Correction Blue Channel", &m_PPPPresets->m_ColorCorrect.z, 0.0f, 1.0f, 0.01f, NullCB, "Global color correction (Blue)." );

		bk.AddSlider("Color Addition Red Channel", &m_PPPPresets->m_ConstAdd.x, 0.0f, 1.0f, 0.01f, NullCB, "Global color additive (Red)." );
		bk.AddSlider("Color Addition Green Channel", &m_PPPPresets->m_ConstAdd.y, 0.0f, 1.0f, 0.01f, NullCB, "Global color additive (Green)." );
		bk.AddSlider("Color Addition Blue Channel", &m_PPPPresets->m_ConstAdd.z, 0.0f, 1.0f, 0.01f, NullCB, "Global color additive (Blue)." );
	}

	bk.AddSlider( "Saturation", &m_PPPPresets->m_deSat, 0.0f, 1.0f, 0.01f, NullCB, "Color saturation." );
	bk.AddSlider( "Contrast", &m_PPPPresets->m_fContrast, -2.0f, 2.0f, 0.01f, NullCB, "Color contrast." );
	bk.PushGroup("Blue Shift Parameters");
		bk.AddSlider("Blue shift scale", &m_PPPPresets->m_BlueShiftParams.x, 0.0f, 10.0f, 0.01f, NullCB, "Blue shift Scale" );
		bk.AddSlider("Blue shift offset", &m_PPPPresets->m_BlueShiftParams.y, -10.0f, 10.0f, 0.01f, NullCB, "Blue shift Offset" );
		bk.AddSlider("Blue shift light adaption scale", &m_PPPPresets->m_BlueShiftParams.z, -10.0f, 10.0f, 0.01f, NullCB, "Blue shift Offset" );
		bk.AddSlider("Blue shift color", &m_PPPPresets->m_BlueShiftColor, 0.0f, 2.0f, 0.01f, NullCB, "Blue shift Offset" );
	bk.PopGroup();

	if ( m_showGammaControl )
	{
		bk.AddSlider( "Gamma 0.416667 == 1/2.4", &m_PPPPresets->m_fGamma, 0.0f, 2.0f, 0.01f, NullCB, "Gamma control" );
	}

#if RAGE_SSAO
	bk.PushGroup("SSAO");
	bk.AddToggle("Enable", &m_PPPPresets->m_EnableSSAO);
	bk.AddSlider("Intensity", &m_PPPPresets->m_SSAOTweakParams.x, 0.0f, 100.0f, 1.0f);
	bk.AddSlider("Min Blur Disc Size", &m_PPPPresets->m_SSAOTweakParams.y, 0.0f, 100.0f, 0.1f);
	bk.AddSlider("Max Blur Disc Size", &m_PPPPresets->m_SSAOTweakParams.z, 0.0f, 100.0f, 0.1f);
	bk.AddSlider("Max Blur Disc Size Depth", &m_PPPPresets->m_SSAOTweakParams.w, 0.0f, 100.0f, 0.5f);
	bk.PopGroup();
#endif // RAGE_SSAO

	m_techniqueSpecificWidgets.Resize(0);
	AddTechniqueSpecificWidgets( bk );
}

void grPostFX::AddTechniqueSpecificWidgets( bkBank &bk )
{
	while ( m_techniqueSpecificWidgets.GetCount() > 0 )
	{
		bk.Remove( *(m_techniqueSpecificWidgets.Pop()) );
	}    

	if (m_PPPPresets->m_CurrentEffect)
	{
		m_PPPPresets->m_CurrentEffect->AddWidgets(this, bk);
	}


	m_techniqueSpecificWidgets.Push( bk.AddButton("Save Post-Processing Presets", datCallback(MFA(grPostFX::SavePPPPresetBankCbk),this)) );
	m_techniqueSpecificWidgets.Push( bk.AddButton("Load Post-Processing Presets", datCallback(MFA(grPostFX::LoadPPPPresetBankCbk),this)) );
}

void grPostFX::AddWidgetsBrightPass(bkBank &bk)
{

	m_techniqueSpecificWidgets.Push( bk.PushGroup( "Bright Pass", true, "Removes dark areas from the scene." ) );
	{
		bk.AddSlider("Threshold", &m_PPPPresets->m_BrightPassThreshold, 0.0f, 4.0f, 0.01f, NullCB, "Threshold is subtracted from luminance." );
		bk.AddSlider("Offset", &m_PPPPresets->m_BrightPassOffset, 0.0f, 4.0f, 0.01f, NullCB, "Offset is Luminance /= (BrightPassOffset + Luminance)." );
		bk.AddSlider("Bright Pass White", &m_PPPPresets->m_fBrightPassWhite, 0.0f, 15.0f, 0.1f, NullCB, "Minimze loss of contrast if tone mapping low dynamic range images." );        
	}
	bk.PopGroup();
}

void grPostFX::AddWidgetsToneMapping(bkBank &bk)
{
	m_techniqueSpecificWidgets.Push(bk.PushGroup("Light Adaption"));
	bk.AddSlider("Speed", &m_PPPPresets->m_fAdapt, 0.0f, 5.0f, 0.01f,NullCB,"Speed at which we simulate the human eye adjusting to changes in brightness.");
	bk.AddSlider( "Lower limit", &m_PPPPresets->m_fLowerLimitAdaption, 0.0f, 2.0f, 0.01f, NullCB, "Lower limit light adaption" );
	bk.AddSlider( "Higher limit", &m_PPPPresets->m_fHigherLimitAdaption, 0.0f, 2.0f, 0.01f, NullCB, "Higher limit light adaption" );
	bk.AddSlider( "Num. of Frames in Lum. History", &m_PPPPresets->m_iNumOfFramesInLumHistory, 1, 16, 1, NullCB, "number of frames where we track luminance history" );
	bk.PopGroup();

	m_techniqueSpecificWidgets.Push(bk.PushGroup("Luminance adjustment"));
	bk.AddSlider("Intensity Bloom", &m_PPPPresets->m_fBloomIntensity, 0.0f, 10.0f, 0.1f, NullCB, "Intensity of blurring." );
	bk.AddSlider("Middle Gray Value", &m_PPPPresets->m_MiddleGray, 0.0f, 1.0f, 0.001f, NullCB, "Subjective middle brightness of the scene." );
	bk.AddSlider("White", &m_PPPPresets->m_fWhite, 0.0f, 15.0f, 0.1f, NullCB, "Minimze loss of contrast if tone mapping low dynamic range images." );        
	bk.PopGroup();
}


void grPostFX::AddWidgetsDOF(bkBank &bk)
{
	static const char* DOFname[max_dof_filters] = 
	{
		"double 17-tap",
		"9-tap", 
		"5-tap",
		"none",

	};
	m_techniqueSpecificWidgets.Push(bk.PushGroup("Depth of Field"));

	bk.AddCombo("DOF blur kernel", (int*)&m_DOFBlurType, max_dof_filters, DOFname, NullCB, "Which DOF blur to use.");
	bk.AddSlider("dof focal distance (meters)",&m_PPPPresets->m_DofParamters[1],0.0f,10000.0f,0.1f,NullCB,"Distance of center of focus.");
	bk.AddSlider("dof blur range (%)",&m_PPPPresets->m_DofParamters[0],0.0f,1.0f,0.01f,NullCB,"Rate at which focus goes from focused to blurry.");
	bk.AddSlider("dof cutoff (meters)",&m_PPPPresets->m_DofParamters[2],0.0f,10000.0f,0.1f,NullCB,"Objects farther than this distance will be clear, so we can un-fog the sky.");
	bk.AddSlider("dof smoothing",&m_PPPPresets->m_DofParamters[3],0.0f,0.999f,0.01f,NullCB,"Seed to smoothstep");
	bk.PopGroup();
}

void grPostFX::AddWidgetsFog(bkBank &bk)
{
	m_techniqueSpecificWidgets.Push( bk.PushGroup( "Fog tuning" ) );
	if ( bkRemotePacket::IsConnectedToRag() )
	{
		{
			bk.AddColor("Min Fog Color",vector_cast<Vector3*>(&m_PPPPresets->m_FogMinColor), NullCB, "Top fog color" );
			bk.AddColor("Max Fog Color",vector_cast<Vector3*>(&m_PPPPresets->m_FogMaxColor), NullCB, "Bottom fog color" );
		}			
	}
	else
	{
		bk.AddSlider("Min Fog Color Red Channel", &m_PPPPresets->m_FogMinColor.x, 0.0f, 1.0f, 0.01f, NullCB, "Min fog color (Red)." );
		bk.AddSlider("Min Fog Color Green Channel", &m_PPPPresets->m_FogMinColor.y, 0.0f, 1.0f, 0.01f, NullCB, "Min fog color (Green)." );
		bk.AddSlider("Min Fog Color Blue Channel", &m_PPPPresets->m_FogMinColor.z, 0.0f, 1.0f, 0.01f, NullCB, "Min fog color (Blue)." );
		bk.AddSlider("Min Fog Opacity", &m_PPPPresets->m_FogMinColor.w, 0.0f, 1.0f, 0.01f, NullCB, "Min fog Opacity." );

		bk.AddSlider("Max Fog Color Red Channel", &m_PPPPresets->m_FogMaxColor.x, 0.0f, 1.0f, 0.01f, NullCB, "Max fog color (Red)." );
		bk.AddSlider("Max Fog Color Green Channel", &m_PPPPresets->m_FogMaxColor.y, 0.0f, 1.0f, 0.01f, NullCB, "Max fog color (Green)." );
		bk.AddSlider("Max Fog Color Blue Channel", &m_PPPPresets->m_FogMaxColor.z, 0.0f, 1.0f, 0.01f, NullCB, "Max fog color (Blue)." );
	}
	bk.PopGroup();
}

void grPostFX::AddWidgetsFogTypeA(bkBank &bk)
{
	m_techniqueSpecificWidgets.Push( bk.PushGroup( "Fog tuning - Type A" ) );

	bk.AddSlider("Max Fog Opacity", &m_PPPPresets->m_FogMaxColor.w, 0.0f, 1.0f, 0.01f, NullCB, "Max fog Opacity." );
	bk.AddSlider("Fog top height", &m_PPPPresets->m_FogParams.x, -1000.0f, 1000.0f, 0.2f, NullCB, "Top fog height" );
	bk.AddSlider("Fog near Z", &m_PPPPresets->m_FogParams.z, -1000.0f, 1000.0f, 1.0f, NullCB, "Fog near Z" );
	bk.AddSlider("Fog far Z", &m_PPPPresets->m_FogParams.w, 0.0f, 10000.0f, 1.0f, NullCB, "Fog far Z" );

	bk.PopGroup();
}

void grPostFX::AddWidgetsFogTypeB(bkBank &bk)
{
	m_techniqueSpecificWidgets.Push( bk.PushGroup( "Fog tuning - Type B" ) );

	bk.AddSlider("Fog top height", &m_PPPPresets->m_FogParams.x, -1000.0f, 1000.0f, 0.2f, NullCB, "Fog top" );
	bk.AddSlider("Fog bottom height", &m_PPPPresets->m_FogParams.y, -1000.0f, 1000.0f, 0.2f, NullCB, "Fog bottom" ); 
	bk.AddSlider("Fog density", &m_PPPPresets->m_FogParams.z, 0.0f, 100.0f, 0.0001f, NullCB, "Fog density" );
	bk.AddSlider("Final Fog Opacity", &m_PPPPresets->m_FogMaxColor.w, 0.0f, 1.0f, 0.01f, NullCB, "Max fog Opacity." );

	bk.AddSlider("Color Blend Start", &m_PPPPresets->m_FogParams.y, 0.0f, 1.0f, 0.01f, NullCB, "Max fog Opacity." );
	bk.AddSlider("Color Blend End", &m_PPPPresets->m_FogMinColor.w, 0.0f, 1.0f, 0.01f, NullCB, "Max fog Opacity." );

	bk.AddSlider("Fog End", &m_PPPPresets->m_FogParams.w, 0.0f, 1000000.0f, 1.0f, NullCB, "Fog End" );

	bk.PopGroup();
}

#endif // __BANK

bool grPostFX::SavePPPPreset( const char *pFileName ) const
{
	// make sure the proper extension is there...
	char pppFileName[RAGE_MAX_PATH];
	safecpy(pppFileName, pFileName);

	const char* ext = fiAssetManager::FindExtensionInPath((const char*)pppFileName);
	if ( !ext )
	{
		safecat(pppFileName, ".ppp");
	}

	//Save the camera preset data
	return StorePPPPreset( pppFileName );
}

bool grPostFX::LoadPPPPreset( const char *pFileName )
{
	//Load the selected post-processing pipeline preset file
	grcPPPPreset *pPPPPreset = CreatePPPPreset();
	bool success = LoadPPPPreset( pPPPPreset, pFileName );    
	if ( success )
	{
		memcpy( m_PPPPresets, pPPPPreset, GetPPPPresetSize() );
#if __BANK
		UpdateTechniqueSpecificWidgets();
#endif // __BANK
	}

	delete pPPPPreset;

	return success;
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Pushes the stack of presets down and copies the current PPP settings to
//	the top of the stack.
//
bool grPostFX::PushPPPPresets()
{
	bool success = true;

	if (m_stacked < PPP_STACK_SIZE)
	{
		m_stacked++;

		for (u8 i = (m_stacked - 1); i > 0; i--)
		{
			memcpy(m_PPPStack[i], m_PPPStack[i - 1], GetPPPPresetSize());
		}

		memcpy(m_PPPStack[0], m_PPPPresets, GetPPPPresetSize());
	}
	else
		success = false;

	return success;
}


////////////////////////////////////////////////////////////////////////////////
//
//	PURPOSE
//		Overwrites the current PPP settings with those from the top of the stack.
//	Moves everything else up one in the stack.
//
bool grPostFX::PopPPPPresets()
{
	bool success = true;

	if (m_stacked > 0)
	{
		memcpy(m_PPPPresets, m_PPPStack[0], GetPPPPresetSize());

		for (u8 i = 0; i < m_stacked; i++)
		{
			memcpy(m_PPPStack[i], m_PPPStack[i + 1], GetPPPPresetSize());
		}

		m_stacked--;
	}
	else
		success = false;

	return success;
}


rage::atString grPostFX::SavePPPPresetFunc( void *pObj, const char *pFilePath )
{
	if ( ((grPostFX *)pObj)->SavePPPPreset(pFilePath) )
	{
		atString pppFileName(pFilePath);
		const char* ext = fiAssetManager::FindExtensionInPath((const char*)pppFileName);
		if ( !ext )
		{
			pppFileName += ".ppp";
		}

		return pppFileName;
	}

	return atString("");
}




// Our PPP effects (TODO: These should go into a separate .cpp)
grPostFX::grPostFXEffectNone::grPostFXEffectNone(grPostFX *postFX)
: grPostFXEffect(postFX)
{
	m_Name = "Color Filters Only (PC, 360, PS3)";
	m_TechniqueName = "Color Filters Only";
}

void grPostFX::grPostFXEffectNone::Process(grPostFX *postFX)
{
	postFX->ProcessNone();
}

void grPostFX::ProcessNone()
{
	BlitToRT(NULL, m_BackBuffer, m_OnlyColorFiltersTechnique, m_FullResMapId);
}

grPostFX::grPostFXEffectToneMapping::grPostFXEffectToneMapping(grPostFX *postFX)
: grPostFXEffect(postFX)
{
	m_Name = "HDR (PC, 360, PS3)";
	m_TechniqueName = "HDR";
}

#if __BANK
void grPostFX::grPostFXEffectToneMapping::AddWidgets(grPostFX *postFX, bkBank &bk)
{
	postFX->AddWidgetsBrightPass(bk);
	postFX->AddWidgetsToneMapping(bk);
}

#endif // __BANK


void grPostFX::grPostFXEffectToneMapping::Process(grPostFX *postFX)
{
	postFX->ProcessToneMapping();
}

void grPostFX::ProcessToneMapping()
{
	ToneMappingSetup();

	//
	// apply the bright pass filter
	//
	BrightPass();

#if __WIN32PC || __PPU
	// here comes some additional blur
	PIXBegin(0, "Blur like hell with 9 tap filters :-)...");
	BlitToRT(m_ScratchPadRT, m_BrightPassRT, m_GaussXTech, m_RenderMapBilinearId);
	BlitToRT(m_GaussRT, m_ScratchPadRT, m_GaussYTech, m_RenderMapBilinearId);
	PIXEnd();
#endif

	PIXBegin(0, "Tone Mapping");
	// the result of the light adaption
#if __XENON
	m_PostFXShader->SetVar(m_VertexTextureId, m_CurrentAdaptedLum1x1RT); 		
#else
#if __PPU
	m_PostFXShader->SetVar(m_AdaptedLuminanceId, m_AdaptedLuminance);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, m_CurrentAdaptedLum1x1RT); 		
#endif
#endif
	// blurred 1/16 image for HDR ... note it is anisotropic filtered
#if __WIN32PC || __PPU
	m_PostFXShader->SetVar(m_RenderMapAnisoId, m_GaussRT); 	
#else
	m_PostFXShader->SetVar(m_RenderMapAnisoId, m_BrightPassRT); 	
#endif
	BlitToRT(NULL, m_BackBuffer, m_ToneMappingTechnique, m_FullResMapId);

	PIXEnd();

	MotionBlur();

	// this is used to make screenshots easily
	if (m_SecondaryFinalOutputRT)
	{
		if (m_SecondaryFinalOutputRT->GetWidth() <= m_QuarterRT->GetWidth())
			BlitToRT(m_SecondaryFinalOutputRT, m_QuarterRT, m_ToneMappingTechnique, m_FullResMapId);
		else
			BlitToRT(m_SecondaryFinalOutputRT, m_BackBuffer, m_ToneMappingTechnique, m_FullResMapId);
	}

	m_PostFXShader->SetVar(m_RenderMapAnisoId, (grcTexture*)grcTexture::None);
#if __XENON
	m_PostFXShader->SetVar(m_VertexTextureId, (grcTexture*)grcTexture::None);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, (grcTexture*)grcTexture::None); 		
#endif
}

grPostFX::grPostFXEffectToneMappingDepthOfField::grPostFXEffectToneMappingDepthOfField(grPostFX *postFX)
: grPostFXEffect(postFX)
{
	m_Name = "HDR + Depth of Field (360, PS3)";
	m_TechniqueName = "HDRDepthofField";
}

#if __BANK

void grPostFX::grPostFXEffectToneMappingDepthOfField::AddWidgets(grPostFX *postFX, bkBank &bk)
{
	postFX->AddWidgetsBrightPass(bk);
	postFX->AddWidgetsToneMapping(bk);
	postFX->AddWidgetsDOF(bk);
}

#endif // __BANK

void grPostFX::grPostFXEffectToneMappingDepthOfField::Process(grPostFX *postFX)
{
	postFX->ProcessToneMappingDepthOfField();
}

void grPostFX::ProcessToneMappingDepthOfField()
{
	SetupDOF();
	ToneMappingSetup();

	//
	// apply the bright pass filter
	//
	BrightPass();

	// blur the quarter render target for DOF
	PIXBegin(0, "Blur with two 5 tap filters :-)...");
	BlitToRT(m_ScratchQuarterRT, m_QuarterRT, m_GaussX5Tech, m_RenderMapBilinearId);
	BlitToRT(m_QuarterRT, m_ScratchQuarterRT, m_GaussY5Tech, m_RenderMapBilinearId);
	PIXEnd();

	PIXBegin(0, "Tone Mapping + DOF");
	// set depth buffer
	m_PostFXShader->SetVar(m_DepthMapId, m_DepthBuffer); 	
	// the result of the light adaption
#if __XENON
	m_PostFXShader->SetVar(m_VertexTextureId, m_CurrentAdaptedLum1x1RT); 		
#else
#if __PPU
	m_PostFXShader->SetVar(m_AdaptedLuminanceId, m_AdaptedLuminance);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, m_CurrentAdaptedLum1x1RT); 		
#endif
#endif

	// blurred 1/16 image for HDR ... note it is anisotropic filtered
	m_PostFXShader->SetVar(m_RenderMapAnisoId, m_BrightPassRT); 

	// quarter sized image for DOF
	m_PostFXShader->SetVar(m_RenderMapBilinearId, m_QuarterRT);
	BlitToRT(NULL, m_BackBuffer, m_ToneMapDOFTechnique, m_FullResMapId);
#if __XENON
	m_PostFXShader->SetVar(m_VertexTextureId, (grcTexture*)grcTexture::None);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, (grcTexture*)grcTexture::None); 		
#endif
	m_PostFXShader->SetVar(m_RenderMapBilinearId, (grcTexture*)grcTexture::None);
	m_PostFXShader->SetVar(m_RenderMapAnisoId, (grcTexture*)grcTexture::None);
	m_PostFXShader->SetVar(m_RenderMapId, (grcTexture*)grcTexture::None); 		
	PIXEnd();
}

grPostFX::grPostFXEffectToneMappingDepthOfFieldGaussFog::grPostFXEffectToneMappingDepthOfFieldGaussFog(grPostFX *postFX)
: grPostFXEffect(postFX)
{
	m_Name = "HDR + Depth of Field Gauss Fog (360 only)";
	m_TechniqueName = "HDRDepthofFieldGaussFog";
}

#if __BANK

void grPostFX::grPostFXEffectToneMappingDepthOfFieldGaussFog::AddWidgets(grPostFX *postFX, bkBank &bk)
{
	postFX->AddWidgetsBrightPass(bk);
	postFX->AddWidgetsToneMapping(bk);
	postFX->AddWidgetsDOF(bk);
	postFX->AddWidgetsFog(bk);
	postFX->AddWidgetsFogTypeA(bk);
}

#endif // __BANK

void grPostFX::grPostFXEffectToneMappingDepthOfFieldGaussFog::Process(grPostFX *postFX)
{
	postFX->ProcessToneMappingDepthOfFieldGaussFog();
}

void grPostFX::ProcessToneMappingDepthOfFieldGaussFog()
{
	SetupDOF();
	ToneMappingSetup();
	//
	// apply the bright pass filter
	//

	// pass down the inverse FullCompositeMtx so that the PS can compute 
	// worldspace coordinates for height based fog.
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
	Mat44V modelViewInv;
	InvertFull(modelViewInv, grcViewport::GetCurrent()->GetFullCompositeMtx());

	m_PostFXShader->SetVar(m_ProjInverseTransposeId, RCC_MATRIX44(modelViewInv));

	// pass down fog parameters.
	m_PostFXShader->SetVar(m_FogMinColorId, m_PPPPresets->m_FogMinColor);
	m_PostFXShader->SetVar(m_FogMaxColorId, m_PPPPresets->m_FogMaxColor);
	m_PostFXShader->SetVar(m_FogParamsId, m_PPPPresets->m_FogParams);
	m_PostFXShader->SetVar(m_ParticleFogParamsId, m_PPPPresets->m_ParticleFogParams);

#define OPTGAUSS 0
#if OPTGAUSS
	// here comes some additional blur
	PIXBegin(0, "9 tap 360 optimized filters :-)...");
	BlitToRT(m_ScratchPadRT, m_BrightPassRT, m_GaussXTech, m_RenderMapBilinearId);
	BlitToRT(m_BrightPassRT, m_ScratchPadRT, m_GaussYTech, m_RenderMapBilinearId);
	PIXEnd();

#endif

	PIXBegin(0, "Tone Mapping + DOF");
	// set depth buffer
	m_PostFXShader->SetVar(m_DepthMapId, m_DepthBuffer); 	
	// the result of the light adaption
#if __XENON
	m_PostFXShader->SetVar(m_VertexTextureId, m_CurrentAdaptedLum1x1RT); 		
#else
#if __PPU
	m_PostFXShader->SetVar(m_AdaptedLuminanceId, m_AdaptedLuminance);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, m_CurrentAdaptedLum1x1RT); 		
#endif
#endif
	// blurred 1/16 image for HDR ... note it is anisotropic filtered
	m_PostFXShader->SetVar(m_RenderMapAnisoId, m_BrightPassRT); 	
	// quarter sized image for DOF
	m_PostFXShader->SetVar(m_RenderMapBilinearId, m_QuarterRT);
	BlitToRT(NULL, m_BackBuffer, m_ToneMapDOFFogTechnique, m_FullResMapId);
#if __XENON
	m_PostFXShader->SetVar(m_VertexTextureId, (grcTexture*)grcTexture::None);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, (grcTexture*)grcTexture::None); 		
#endif
	m_PostFXShader->SetVar(m_RenderMapBilinearId, (grcTexture*)grcTexture::None);
	m_PostFXShader->SetVar(m_RenderMapAnisoId, (grcTexture*)grcTexture::None);
	m_PostFXShader->SetVar(m_RenderMapId, (grcTexture*)grcTexture::None); 		
	PIXEnd();
}



grPostFX::grPostFXEffectFog::grPostFXEffectFog(grPostFX *postFX)
	: grPostFXEffect(postFX)
{
	m_Name = "Fog (360 only)";
	m_TechniqueName = "Fog";
}

#if __BANK

void grPostFX::grPostFXEffectFog::AddWidgets(grPostFX *postFX, bkBank &bk)
{
	postFX->AddWidgetsFog(bk);
	postFX->AddWidgetsFogTypeB(bk);
}

#endif // __BANK

void grPostFX::grPostFXEffectFog::Process(grPostFX *postFX)
{
	postFX->ProcessFog();
}

void grPostFX::ProcessFog()
{
	// pass down the inverse FullCompositeMtx so that the PS can compute 
	// worldspace coordinates for height based fog.
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);

	// pass down the inverse FullCompositeMtx so that the PS can compute 
	// worldspace coordinates for height based fog.
	Mat44V modelViewInv;
	InvertFull(modelViewInv, grcViewport::GetCurrent()->GetFullCompositeMtx());
	m_PostFXShader->SetVar(m_ProjInverseTransposeId, RCC_MATRIX44(modelViewInv));

	// pass down fog parameters.

	m_PostFXShader->SetVar(m_FogMaxColorId, m_PPPPresets->m_FogMaxColor);

	Vector4 fogVals = m_PPPPresets->m_FogParams;

	m_PPPPresets->m_FogMinColor.w = Max( m_PPPPresets->m_FogParams.y + 0.000001f , m_PPPPresets->m_FogMinColor.w);
	float scale = 1.0f/( m_PPPPresets->m_FogMinColor.w -  m_PPPPresets->m_FogParams.y );
	float offset = - ( m_PPPPresets->m_FogMinColor.w +  m_PPPPresets->m_FogParams.y ) / 2.0f ;//+ 0.5f;
	offset = offset * scale + 0.5f;

	fogVals.y = scale;
	m_PostFXShader->SetVar(m_FogParamsId, fogVals);

	fogVals = m_PPPPresets->m_FogMinColor;
	fogVals.w = offset;
	m_PostFXShader->SetVar(m_FogMinColorId, fogVals );


	// here comes some additional blur to make it cooler at nigh

	PIXBegin(0, "PPP Fog");
	// set depth buffer
	m_PostFXShader->SetVar(m_DepthMapId, m_DepthBuffer); 	
	// the result of the light adaption

	BlitToRT(NULL, m_BackBuffer, m_FogTechnique, m_FullResMapId);

#if __XENON
	m_PostFXShader->SetVar(m_VertexTextureId, (grcTexture*)grcTexture::None);
#else
	m_PostFXShader->SetVar(m_AdaptedLuminanceMapId, (grcTexture*)grcTexture::None); 		
#endif
	m_PostFXShader->SetVar(m_RenderMapBilinearId, (grcTexture*)grcTexture::None);
	m_PostFXShader->SetVar(m_RenderMapAnisoId, (grcTexture*)grcTexture::None);
	m_PostFXShader->SetVar(m_RenderMapId, (grcTexture*)grcTexture::None); 		
	PIXEnd();
}

void grPostFX::SetTechnique(const char *name)
{
	m_PPPPresets->SetChooseTechnique(name);
}

void grPostFX::SetDefaultTechnique()
{
#if __BANK
	m_PPPPresets->m_iChooseTechnique = 0;
#endif // __BANK
	m_PPPPresets->m_CurrentEffect = GetDefaultEffect();
}

void grPostFX::RegisterEffect(grPostFXEffect *effect)
{
#if __BANK
	Assert(m_Effects.GetCount() < MaxTechniques);
	m_ChooseTechniqueNames[m_Effects.GetCount()] = effect->GetTechniqueName();
#endif // __BANK

	m_Effects.Append() = effect;
}

void grPostFX::RegisterDefaultEffects()
{
	RegisterEffect(rage_new grPostFXEffectNone(this));
	RegisterEffect(rage_new grPostFXEffectToneMapping(this));
	RegisterEffect(rage_new grPostFXEffectToneMappingDepthOfField(this));
	RegisterEffect(rage_new grPostFXEffectToneMappingDepthOfFieldGaussFog(this));
	RegisterEffect(rage_new grPostFXEffectFog(this));
#if __PS3
	if (PARAM_edgepost.Get())
	{
		RegisterEffect(rage_new grPostFXEdgeEffect(this));
	}
#endif // __PS3
}

grPostFX::grcPPPPreset *grPostFX::CreatePPPPreset()
{
	return rage_new grcPPPPreset();
}

int grPostFX::GetPPPPresetSize() const
{
	return sizeof(grcPPPPreset);
}
