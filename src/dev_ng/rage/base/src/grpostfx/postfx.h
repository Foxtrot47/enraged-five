
// 
// grpostfx/postfx.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Post-processing interface
//	Use this class to render any post-processing effects in your game
// 
// 
#ifndef __GRPOSTFX_H_
#define __GRPOSTFX_H_

#include "postfx_cfg.h"
#include "atl/array.h"
#include "bank/widget.h"
#include "data/base.h"
#include "data/callback.h"
#include "grcore/effect.h"
#include "vector/color32.h"
#include "vector/vector4.h"
#include "vector/matrix44.h"
#include "grcore/texture.h"
#include "grcore/gprallocation.h"
#include "grmodel/shader.h"
#include "shaderlib/rage_constants.h"
#if __PS3
#include "grpostfx/postparams.h"
#include "edge/post/edgepost.h" // EdgePostWorkload definition
#include <sdk_version.h>

struct EdgePostProcessStage;
struct EdgePostImage;
#endif // __PS3

#define MAX_STAR_STREAKS 4
#define MAX_STREAK_ITERATIONS 4

#define NUM_BLOOD_SPLATTER_TEXTURES 2

#define RAGE_SSAO (0)

namespace rage
{
	class grcRenderTarget;
	class grmShaderGroup;
	class grmShaderFactory;
	class grmShaderFX;
	class grcTexture;
	class grcTextureReference;
	class grPostFX;
	class bkBank;	
	class Color32;
	class atString;
	class parTreeNode;
	template <class _T,int _Align,class _CounterType> class atArray;

	// Post-Processing Pipeline
	enum dofFilter
	{
		dof17tap,
		dof9tap,
		dof5tap,
		dofnone,
		max_dof_filters
	};

#define PPP_STACK_SIZE 3

	//-----------------------------------------------------------------
	//Define the XML syntax used in the camera preset ".camset" files
#define GRCPPPPRESET_VERSION				"1"

#define GRCPPPPRESET_STRUCTURE				"grcppppreset"
#define GRCPPPPRESET_VERSION_ATTR			"version"
#define GRCPPPPRESET_PPP_STRUCTURE		"post-processing"

// #define GRCPPPPRESET_EXPOSURE_PARAM			"exposure"
#define GRCPPPPRESET_BRIGHTPASSTHRESHOLD_PARAM "brightpassthreshold"
#define GRCPPPPRESET_BRIGHTPASSOFFSET_PARAM "brightpassoffset"
#define GRCPPPPRESET_BRIGHTPASSWHITE_PARAM "brightpasswhite"
#define GRCPPPPRESET_MIDDLEGRAY_PARAM "middlegray"
#define GRCPPPPRESET_BRIGHTPASSTHRESHOLDTWO_PARAM "brightpassthresholdtwo"
#define GRCPPPPRESET_BRIGHTPASSOFFSETTWO_PARAM "brightpassoffsettwo"
#define GRCPPPPRESET_BRIGHTPASSWHITETWO_PARAM "brightpasswhitetwo"
#define	GRCPPPPRESET_NUMOFFRAMESINLUMHISTORY_PARAM "numofframesinlumhistory"
#define GRCPPPPRESET_MIDDLEGRAYTWO_PARAM "middlegraytwo"
#define GRCPPPPRESET_ADAPT_PARAM "adapt"
#define GRCPPPPRESET_BLOOMINTENSITY_PARAM "bloomintensity"
#define GRCPPPPRESET_CLEARCOLOR_PARAM "clearcolor"
#define GRCPPPPRESET_COLORCORRECT_PARAM "colorcorrect"
#define GRCPPPPRESET_COLORADD_PARAM "coloradd"
#define GRCPPPPRESET_SAT_PARAM "saturation"
#define GRCPPPPRESET_SATTWO_PARAM "saturationpolicecam"
#define GRCPPPPRESET_SCALE_PARAM "scale"
#define GRCPPPPRESET_RHO_PARAM "rho"
#define GRCPPPPRESET_CONTRAST_PARAM "contrast"
#define GRCPPPPRESET_CONTRASTTWO_PARAM "contrastpolicecam"
#define GRCPPPPRESET_GRAININTENSITY_PARAM "grainintensity"
#define GRCPPPPRESET_INTERLACEINTENSITY_PARAM "interlaceintensity"
#define GRCPPPPRESET_FRAMESHAPE_PARAM "frameshape"
#define GRCPPPPRESET_FRAMELIMIT_PARAM "framelimit"
#define GRCPPPPRESET_TIMEFRAME_PARAM "timeframe"
#define GRCPPPPRESET_INTERFERENCE_PARAM "interference"
#define GRCPPPPRESET_CHOOSETECHNIQUE_PARAM "choosetechnique"
#define GRCPPPPRESET_DOFPARAMETERS_PARAM "dofparameters"
#define GRCPPPPRESET_SHOWRENDERTARGETS_PARAM "showrendertargets"
#define GRCPPPPRESET_SWITCHPOLICECAMERA_PARAM "switchpolicecamera"
#define GRCPPPPRESET_GAMMA_PARAM "gamma"
#define GRCPPPPRESET_LOWERLIMITADAPTION_PARAM "lowerlimitlightadaption"
#define GRCPPPPRESET_HIGHERLIMITADAPTION_PARAM "higherlimitlightadaption"
#define GRCPPPPRESET_FOGMINCOLOR_PARAM "fogmincolor"
#define GRCPPPPRESET_FOGMAXCOLOR_PARAM "fogmaxcolor"
#define GRCPPPPRESET_FOGPARAMETERS_PARAM "fogparameters"
#define GRCPPPPRESET_BRIGHTPASSPARAMETERS_PARAM "brightpassparameters"
#define GRCPPPPRESET_DOF_BLURTYPE_PARAM "dofblurtype" 
#define GRCPPPPRESET_BLUESHIFTCOLOR_PARAM "blueshiftcolor" 
#define GRCPPPPRESET_BLUESHIFTPARAMS_PARAM "blueshiftparams" 


	// light streak and associated kawase bloom
#define GRCPPPPRESET_STREAKSAMPLES_PARAM "m_StreakSamples"
#define GRCPPPPRESET_STREAKITERATIONS_PARAM "m_StreakIterations"
#define GRCPPPPRESET_KAW_BLOOMITERATIONS_PARAM "m_BloomIterations"
#define GRCPPPPRESET_KAW_BLOOMINTENSITY_PARAM "m_BloomIntensity"
#define GRCPPPPRESET_STREAKINTENSITY_PARAM "m_StreakIntensity"


	class grPostFXEffect
	{
	public:
		grPostFXEffect(grPostFX * /*postFX*/) {};

		virtual ~grPostFXEffect() {};

		virtual void AddWidgets(grPostFX* /*postFX*/, bkBank &/*bank*/) {};

		virtual void Process(grPostFX* postFX) = 0;

		const char *GetName() const				{ return m_Name; }
		const char *GetTechniqueName() const	{ return m_TechniqueName; }

	protected:
		const char *m_Name;				// Friendly name for this effect
		const char *m_TechniqueName;	// Technique name
	};




	class grPostFX : public datBase
	{
		friend class grPostFXEdgeEffect;

	public:
		struct grcPPPPreset;
		grPostFX();
		virtual ~grPostFX();

		//
		// PURPOSE
		//	Loads needed shaders and shader variables.
		// PARAMS
		//	path - defaults to NULL - if NULL, will use the currently set path
		//
		virtual void InitShaders(const char *path=NULL); 

		// bind render target on the PC
		void BindPCRenderTarget(bool clear = false);

		// on PC release render target again
		void ReleasePCRenderTarget();

		// this call executes whatever you have chosen
		void Process(); 

		// this call executes at the beginning of the frame
		void BeginFrame();

		// Register( a PPP effect
		void RegisterEffect(grPostFXEffect *effect);

		// this is used to create the render targets and delete them
		// this is useful to adjust for window size changes
		void CreateRenderTargets(int iWidth, int iHeight);
		virtual void DeleteRenderTargets(void);

		// enable of disbale dof effects. By default they are on. Use -nodof cammand line to disbale them all together...
		void EnableDOF(bool on_off);

		// DOF parameters: 
		// first parameter is focus and the second range
		virtual void SetDofParams(const Vector4 &params) {if (m_allowGameOverride) m_PPPPresets->m_DofParamters = params;}
		void SetDofFocalPlaneDist( float d ) {if (m_allowGameOverride) m_PPPPresets->m_DofParamters.y=d;} // 'd' is distance to Focal Plane
		const Vector4 &GetDofParams() const { return m_PPPPresets->m_DofParamters; }

		void SetDofKernal(dofFilter newKernal) {m_DOFBlurType = newKernal;}

		// get/set bright pass threshold of HDR bright-pass filter
		float GetBrightPassThreshold() const { return m_PPPPresets->m_BrightPassThreshold; }
		void SetBrightPassThreshold(float f) { if (m_allowGameOverride) m_PPPPresets->m_BrightPassThreshold = f; }

		// get/set bright pass offset of HDR bright-pass filter
		float GetBrightPassOffset() const { return m_PPPPresets->m_BrightPassOffset; }
		void SetBrightPassOffset(float f) {if (m_allowGameOverride)  m_PPPPresets->m_BrightPassOffset = f; }

		// get/set middle gray of HDR 
		float GetMiddleGray() const { return m_PPPPresets->m_MiddleGray; }
		void SetMiddleGray(float f) { if (m_allowGameOverride) m_PPPPresets->m_MiddleGray = f; }

		// get/set bright pass threshold of HDR bright-pass filter
		float GetBrightPassThresholdTwo() const { return m_PPPPresets->m_BrightPassThresholdTwo; }
		void SetBrightPassThresholdTwo(float f) { if (m_allowGameOverride) m_PPPPresets->m_BrightPassThresholdTwo = f; }

		// get/set bright pass offset of HDR bright-pass filter
		float GetBrightPassOffsetTwo() const { return m_PPPPresets->m_BrightPassOffsetTwo; }
		void SetBrightPassOffsetTwo(float f) { if (m_allowGameOverride) m_PPPPresets->m_BrightPassOffsetTwo = f; }

		// get/set middle gray of HDR 
		float GetMiddleGrayTwo() const { return m_PPPPresets->m_MiddleGrayTwo; }
		void SetMiddleGrayTwo(float f) { if (m_allowGameOverride) m_PPPPresets->m_MiddleGrayTwo = f; }

		// get/set White value for tone mapping
		int GetNumOfFramesInLumHistory() const { return m_PPPPresets->m_iNumOfFramesInLumHistory; }
		void SetNumOfFramesInLumHistory(int i) { if (m_allowGameOverride) m_PPPPresets->m_iNumOfFramesInLumHistory = i; }

		// get/set White value for tone mapping
		float GetBrightPassWhiteTwo() const { return m_PPPPresets->m_fBrightPassWhiteTwo; }
		void SetBrightPassWhiteTwo(float f) { if (m_allowGameOverride) m_PPPPresets->m_fBrightPassWhiteTwo = f; }

		// get/set adaption level of light adaption 
		float GetAdapt() const { return m_PPPPresets->m_fAdapt; }
		void SetAdapt(float f) { if (m_allowGameOverride) m_PPPPresets->m_fAdapt = f; }

		// get/set bloom intensity of HDR Gauss filter 
		float GetBloomIntensity() const { return m_PPPPresets->m_fBloomIntensity; }
		void SetBloomIntensity(float f) { if (m_allowGameOverride) m_PPPPresets->m_fBloomIntensity = f; }

		// color correction in post-processing pipeline
		const Vector4 &GetClearColor() const { return m_PPPPresets->m_ClearColor; }
		void SetClearColor(float r, float g, float b, float a) { if (m_allowGameOverride) m_PPPPresets->m_ClearColor.Set(r,g,b,a); }
		void SetClearColor(const Vector4 &Color) { if (m_allowGameOverride) m_PPPPresets->m_ClearColor.Set(Color); }

		// color correction in post-processing pipeline
		const Vector4 &GetColorCorrect() const { return m_PPPPresets->m_ColorCorrect; }
		void SetColorCorrect(float r, float g, float b, float a) { if (m_allowGameOverride) m_PPPPresets->m_ColorCorrect.Set(r,g,b,a); }
		void SetColorCorrect(const Vector4 &Color) { if (m_allowGameOverride) m_PPPPresets->m_ColorCorrect.Set(Color); }

		// add color in post-processing pipeline
		const Vector4 &GetConstAdd() const { return m_PPPPresets->m_ConstAdd; }
		void SetConstAdd(float r, float g, float b, float a) { if (m_allowGameOverride) m_PPPPresets->m_ConstAdd.Set(r,g,b,a); }
		void SetConstAdd(const Vector4 &Color) { if (m_allowGameOverride) m_PPPPresets->m_ConstAdd.Set(Color); }

		// radial blur filter parameters
		const Vector3 &GetScale() const { return m_PPPPresets->m_Scale; }
		void SetScale(float x, float y, float z) { if (m_allowGameOverride) m_PPPPresets->m_Scale.Set(x, y, z); }
		void SetScale(const Vector3 &Scale) { if (m_allowGameOverride) m_PPPPresets->m_Scale.Set(Scale); }

		// get/set White value for tone mapping
		float GetWhite() const { return m_PPPPresets->m_fWhite; }
		void SetWhite(float f) { if (m_allowGameOverride) m_PPPPresets->m_fWhite = f; }

		// get/set White value for tone mapping
		float GetBrightPassWhite() const { return m_PPPPresets->m_fBrightPassWhite; }
		void SetBrightPassWhite(float f) { if (m_allowGameOverride) m_PPPPresets->m_fBrightPassWhite = f; }

		// get/set visual render target debugger
		bool GetShowRenderTargetFlag() const { return m_PPPPresets->m_ShowRenderTargets; }
		void SetShowRenderTargetFlag(bool b) { if (m_allowGameOverride) m_PPPPresets->m_ShowRenderTargets = b; }

		// get/set visual render target debugger
		bool GetSwitchPoliceCameraFlag() const { return m_PPPPresets->m_SwitchPoliceCam; }
		void SetSwitchPoliceCameraFlag(bool b) { if (m_allowGameOverride) m_PPPPresets->m_SwitchPoliceCam = b; }

		// get/set light adaption
		bool GetLightAdaptionFlag() const { return m_PPPPresets->m_bLightAdaption; }
		void SetLightAdaptionFlag(bool b) { if (m_allowGameOverride) m_PPPPresets->m_bLightAdaption = b; }

		// get/set contrast
		float GetContrast() const { return m_PPPPresets->m_fContrast; }
		void SetContrast(float f) { if (m_allowGameOverride) m_PPPPresets->m_fContrast = f; }

		// get/set contrast
		float GetContrastTwo() const { return m_PPPPresets->m_fContrastTwo; }
		void SetContrastTwo(float f) { if (m_allowGameOverride) m_PPPPresets->m_fContrastTwo = f; }

		// get/set saturation
		float GetSaturation() const {return m_PPPPresets->m_deSat; }
		void SetSaturation(float f) {if (m_allowGameOverride) m_PPPPresets->m_deSat = f;}

		// get/set saturation
		float GetSaturationTwo() const {return m_PPPPresets->m_deSatTwo; }
		void SetSaturationTwo(float f) {if (m_allowGameOverride) m_PPPPresets->m_deSatTwo = f;}

		// get and set technique
 		int GetPPTechnique() const {return m_PPPPresets->m_iChooseTechnique; }
 		void SetPPTechnique(int i){m_PPPPresets->m_iChooseTechnique = i;}

		//
		// film grain parameters
		//
		// get and set sharpness
		float GetFrameSharpness() const {return m_PPPPresets->m_fGrainIntensity; }
		void SetFrameSharpness(float f){if (m_allowGameOverride) m_PPPPresets->m_fGrainIntensity = f;}

		float GetInterlaceIntensity() const {return m_PPPPresets->m_fInterlaceIntensity; }
		void SetInterlaceIntensity(float f){if (m_allowGameOverride) m_PPPPresets->m_fInterlaceIntensity = f;}

		// get and set time limit
		float GetTimeFrame() const {return m_PPPPresets->m_fTimeFrame; }
		void SetTimeFrame(float f){if (m_allowGameOverride) m_PPPPresets->m_fTimeFrame = f;}

		// get and set interference value
		float GetInterference() const {return m_PPPPresets->m_fInterference; }
		void SetInterference(float f){if (m_allowGameOverride) m_PPPPresets->m_fInterference = f;}

		// get and set gamma value
		float GetGamma() const {return m_PPPPresets->m_fGamma; }
		void SetGamma(float f){if (m_allowGameOverride) m_PPPPresets->m_fGamma = f;}

		// get and set lower limit value
		float GetLowerLimitLightAdaption() const {return m_PPPPresets->m_fLowerLimitAdaption; }
		void SetLowerLimitLightAdaption(float f){if (m_allowGameOverride) m_PPPPresets->m_fLowerLimitAdaption = f;}

		// get and set lower limit value
		float GetHigherLimitLightAdaption() const {return m_PPPPresets->m_fHigherLimitAdaption; }
		void SetHigherLimitLightAdaption(float f){if (m_allowGameOverride) m_PPPPresets->m_fHigherLimitAdaption = f;}

		rage::grcRenderTarget *GetFogMapRt() {return m_FogMapRT;}

		// get and set fog parameters: x=topheight, y=bottomheight, z=density
		void SetFogParams(const Vector4 &params)	{if (m_allowGameOverride) m_PPPPresets->m_FogParams = params;}
		const Vector4 &GetFogParams() const { return m_PPPPresets->m_FogParams; }

		// get and set fog parameters: x=topheight, y=bottomheight, z=density
		void SetParticleFogParams(const Vector4 &params)	
		{
			if (m_allowGameOverride)
			{
				m_PPPPresets->m_ParticleFogParams = params;
				m_PostFXShader->SetVar(m_ParticleFogParamsId, m_PPPPresets->m_ParticleFogParams);
			}
		}
		const Vector4 &GetParticleFogParams() const { return m_PPPPresets->m_ParticleFogParams; }

		// get and set minimum fog color
		void SetFogMinColor(const Vector4 &color)	{if (m_allowGameOverride) m_PPPPresets->m_FogMinColor =color;}
		const Vector4 &GetFogMinColor() const { return m_PPPPresets->m_FogMinColor; }

		// get and set maximum fog color
		void SetFogMaxColor(const Vector4 &color)	{if (m_allowGameOverride) m_PPPPresets->m_FogMaxColor =  color;}
		const Vector4 &GetFogMaxColor() const { return m_PPPPresets->m_FogMaxColor; }

		// get and set blue shift color
		void SetBlueShiftColor(const Vector4 &color)	{if (m_allowGameOverride) m_PPPPresets->m_BlueShiftColor =  color;}
		const Vector4 &GetBlueShiftColor() const { return m_PPPPresets->m_BlueShiftColor; }

		// get and set blue shift scale, how quickly does the blue shift ramp up?
		float GetBlueShiftScale() const {return m_PPPPresets->m_BlueShiftParams.x; }
		void SetBlueShiftScale(float f){if (m_allowGameOverride) m_PPPPresets->m_BlueShiftParams.x = f;}

		// get and set blue shift offset, set point of luminance at which blue shift starts kicking in.
		float GetBlueShiftOffset() const {return m_PPPPresets->m_BlueShiftParams.y; }
		void SetBlueShiftOffset(float f){if (m_allowGameOverride) m_PPPPresets->m_BlueShiftParams.y = f;}

		// get and set blue shift light adaption scale, how much does the current exposure as adjusted by the PPP affect the blue shift?
		float GetBlueShiftLightAdaptionScale() const {return m_PPPPresets->m_BlueShiftParams.z; }
		void SetBlueShiftLightAdaptionScale(float f){if (m_allowGameOverride) m_PPPPresets->m_BlueShiftParams.z = f;}

		// get/set streak enable
		bool GetStreakEnabled() const { return m_PPPPresets->m_StreakEnabled; }
		void SetStreakEnabled(bool f) { if (m_allowGameOverride) m_PPPPresets->m_StreakEnabled = f; }

		// get/set streak samples
		float GetStreakSamples() const { return m_PPPPresets->m_StreakSamples; }
		void SetStreakSamples(float f) { if (m_allowGameOverride) m_PPPPresets->m_StreakSamples = f; }

		// get/set number of streaks. IMPORTANT currently works with 2 streaks (times 2, they are bidirectional.)
		int GetNumStreaks() const { return m_PPPPresets->m_NumStreaks; }
		void SetNumStreaks(int f) { if (m_allowGameOverride) m_PPPPresets->m_NumStreaks = f; }

		// get/set streak Iterations
		int GetStreakIterations() const { return m_PPPPresets->m_StreakIterations; }
		void SetStreakIterations(int f) { if (m_allowGameOverride) m_PPPPresets->m_StreakIterations = f; }

		// get/set Bloom Iterations
		int GetStreakBloomIterations() const { return m_PPPPresets->m_BloomIterations; }
		void SetStreakBloomIterations(int f) { if (m_allowGameOverride) m_PPPPresets->m_BloomIterations = f; }

		// get/set streak Intensity
		float GetStreakIntensity() const { return m_PPPPresets->m_StreakIntensity; }
		void SetStreakIntensity(float f) { if (m_allowGameOverride) m_PPPPresets->m_StreakIntensity = f; }

		// get/set Bloom Intensity
		float GetStreakBloomIntensity() const { return m_PPPPresets->m_BloomIntensity; }
		void SetStreakBloomIntensity(float f) { if (m_allowGameOverride) m_PPPPresets->m_BloomIntensity = f; }

		// get/set distortion frequency
		float GetDistortionFreq() const { return m_PPPPresets->m_DistortionFreq; }
		void SetDistortionFreq(float f) { if (m_allowGameOverride) m_PPPPresets->m_DistortionFreq = f; }

		// get/set distortion roll
		float GetDistortionRoll() const { return m_PPPPresets->m_DistortionRoll; }
		void SetDistortionRoll(float f) { if (m_allowGameOverride) m_PPPPresets->m_DistortionRoll = f; }

		// get/set distortion scale (intensity)
		float GetDistortionScale() const { return m_PPPPresets->m_DistortionScale; }
		void SetDistortionScale(float f) { if (m_allowGameOverride) m_PPPPresets->m_DistortionScale = f; }

		const Vector3 &GetBlurEffecTintColor() const {return m_PPPPresets->m_BlurEffectTintColor;}
		void SetBlurEffectTintColor(const Vector3 &v) {if (m_allowGameOverride) m_PPPPresets->m_BlurEffectTintColor = v;}

		float GetBlurEffecTintFade() const {return m_PPPPresets->m_BlurEffectTintFade;}
		void SetBlurEffectTintFade(float f) {if (m_allowGameOverride) m_PPPPresets->m_BlurEffectTintFade = f;}

		float GetBlurEffecDesatScale() const {return m_PPPPresets->m_BlurEffectDesatScale;}
		void SetBlurEffectDesatScale(float f) {if (m_allowGameOverride) m_PPPPresets->m_BlurEffectDesatScale = f;}
		
		// get and set minimum fog color
		void SetBrightPassParams(const Vector4 &params)	{if (m_allowGameOverride) m_PPPPresets->m_BrightPassParams =params;}
		const Vector4 &GetBrightPassParams() const { return m_PPPPresets->m_BrightPassParams; }

		// get and set the whole preset lot.
		grcPPPPreset& GetPPPPresets() { return *m_PPPPresets; }
		virtual void SetPPPPresets(grcPPPPreset &preset) { if (m_allowGameOverride) memcpy(m_PPPPresets, &preset, GetPPPPresetSize()); }

//		const char *GetPPTechniqueName() const	{ return m_PPPPresets->GetTechniqueName(); }
		void SetPPTechniqueName(const char *name)	{ if (m_allowGameOverride) m_PPPPresets->SetChooseTechnique(name); }
		
		// allows stack behavior for post-effects.
		bool PushPPPPresets();
		bool PopPPPPresets();

		void SetGammaEditable( bool val )		{ m_showGammaControl = val; }

		// return a pointer the a texture of the back buffer
		// NOTE: on the PC you need to call grPostFX:UnlockRenderTarget() before calling this function.
		grcTexture * GetBackBuffer()			{ return m_BackBuffer; }

		// allow the game to request that we do the final ppp render into a second render target (as well as the main back buffer) - can be lower resolution than the main buffer.  Set to NULL to turn off this feature (off by default)
		void SetSecondaryFinalRenderTarget(grcRenderTarget * pTarget){ 	m_SecondaryFinalOutputRT = pTarget; }

#if __XENON
		void SetDepthBufferFromTiling(grcRenderTarget * depthBuffer) { m_DepthBuffer360FromTiling = depthBuffer; }
		void SetColorBufferFromTiling(grcRenderTarget * colorBuffer) { m_ColorBuffer360FromTiling = colorBuffer; }
		grcRenderTarget	* GetDepthBuffer() { return m_DepthBuffer; }
		void SetPredicatedTiling(bool on);
#endif
		// functions to pre-seed the bright pass buffer with things that receive the star light streak
		struct SeedBrightPassParams 
		{
			float x1,y1,x2,y2;
			float z;
			float u1,v1,u2,v2;
			Color32 color;
			grcEffectTechnique tech;
			float intensity;
		};

		grmShader* PostFXShader() { return m_PostFXShader; }

		void SetSeedBrightPassIntensity(float f)
		{
			if (m_allowGameOverride)
			{
				m_PPPPresets->m_StreakParams.y = f;
				m_PostFXShader->SetVar(m_StreakParamsId, m_PPPPresets->m_StreakParams);
			}
		}

		void SeedBrightPassRT(SeedBrightPassParams &params) 
		{
			if (m_allowGameOverride)
			{
				m_PPPPresets->m_StreakParams.x = 1.0f/2.0f;
				m_PPPPresets->m_StreakParams.y = params.intensity;
				m_PostFXShader->SetVar(m_StreakParamsId, m_PPPPresets->m_StreakParams);
				m_PostFXShader->SetVar(m_LightGlowMapId, m_LightGlowMap); 		
				m_PostFXShader->TWODBlit(params.x1,params.y1,params.x2,params.y2,params.z,params.u1,params.v1,params.u2,params.v2,params.color,params.tech);
			}
		}

		grcEffectTechnique	GetSeedBrightPassTechnique() { return m_SeedBrightPassTechnique; }
		grcEffectTechnique	GetSeedBrightPassNoZTechnique() { return m_SeedBrightPassNoZTechnique; }
		grcEffectTechnique	GetFogCardTechnique() { return m_FogCardTechnique; }
		grcRenderTarget* GetBrightPassRT() { return m_BrightPassRT; }

		void SetVelocityMap(grcTexture *tex) {m_VelocityMapTexture = tex;}

		void SetFogCardTexture(grcTexture *tex) { m_PostFXShader->SetVar(m_FogCardTextureId, tex); }
		void SetSeedFogPassCallback(datCallback callback) 
		{ 
			if (callback.GetType() != datCallback::NULL_CALLBACK) {
				if(!m_SeedFogPassCallback)
					m_SeedFogPassCallback = rage_new datCallback;
				*m_SeedFogPassCallback = callback;
			}
		}

		void SetSeedBrightPassCallback(datCallback callback) 
		{ 
			if (callback.GetType() != datCallback::NULL_CALLBACK) {
				if(!m_SeedBrightPassCallback)
					m_SeedBrightPassCallback = rage_new datCallback;
				*m_SeedBrightPassCallback = callback;
			}
		}

		void SetPersistentBrightPassCallback(datCallback callback) 
		{ 
			if (callback.GetType() != datCallback::NULL_CALLBACK) {
				if(!m_PersistentBrightPassCallback)
					m_PersistentBrightPassCallback = rage_new datCallback;
				*m_PersistentBrightPassCallback = callback;
			}
		}

		// Allows game to pass its own back/depth buffers.  If this happens, postfx won't resolve these buffers manually.

		void SetBackBuffer(grcRenderTarget*);
		void SetDepthBuffer(grcRenderTarget*);
		grcRenderTarget* GrabBackBuffer(bool forceResolve = false);

		void PushBackBuffer();
		void PopBackBuffer();


		grcRenderTarget* GrabDepthBuffer(bool forceResolve = false);

		void RenderFogPass( const grcTexture* depthBuffer );

		void LoadDeathTexture(char*, char**);

		int GetEffectCount() const					{ return m_Effects.GetCount(); }
		grPostFXEffect *GetEffect(int index) const	{ return m_Effects[index]; }
		grPostFXEffect *GetDefaultEffect() const	{ return m_Effects[0]; }
		void SetDefaultTechnique();
		void SetTechnique(const char *name);
		// static functions
		// keep track of the instance
		static void Init();
		static void Terminate();
		static grPostFX *GetInstance() {return sm_instance;}

		static void SetTexturePath(const char *texturePath) { sm_DefaultTexturePath = texturePath; }

#if __BANK
		enum
		{
			MaxTechniques = 16	// Max number of techniques we can have
		};

		bkBank *m_pBank;
		atArray<bkWidget*> m_techniqueSpecificWidgets;

		const char* m_ChooseTechniqueNames[MaxTechniques];


		void AddWidgets(bkBank &bk);

		void AddWidgetsBrightPass(bkBank &bk);
		void AddWidgetsToneMapping(bkBank &bk);
		void AddWidgetsDOF(bkBank &bk);
		void AddWidgetsFog(bkBank &bk);
		void AddWidgetsFogTypeA(bkBank &bk);
		void AddWidgetsFogTypeB(bkBank &bk);
		void AddWidgetsLightStreaks(bkBank &bk);

	private:
		void AddTechniqueSpecificWidgets( bkBank &bk );

#endif // __BANK


	protected:
		void ProcessNone();
		void ProcessToneMapping();
		void ProcessToneMappingDepthOfField();
		void ProcessToneMappingDepthOfFieldGaussFog();
		void ProcessFog();

		bool	m_showGammaControl;

		virtual void BeginPostProcess();
		virtual void ProcessPostProcess();
		virtual void EndPostProcess();
		// Runs bright pass filter. Shared by HDR effects.
		virtual void BrightPass();		  
		
		virtual void BrightPassPre() {};
		virtual void BrightPassMain() {};
		virtual void BrightPassPost() {};		
		void RDR2DeathPass();

		// Sets up things needed for tone mapping.
		void ToneMappingSetup();

		// Sets up things needed for depth of field.
		void SetupDOF();

		void MotionBlur();
		void MC4MotionBlur();

#if RAGE_SSAO
		void SSAO();
#endif // RAGE_SSAO

		// blits from render target to render target
		void BlitToRT(grcRenderTarget * target,		// target render target
			const grcTexture * texture,			// source render target
			grcEffectTechnique technique,				// technique in FX file
			grcEffectVar textureId = grcevNONE,			// texture sampler
			//				  bool useTextureSize=false,	// texture size from source render target instead of target render target
			const Color32 * color = NULL,					// vertex color | is that still used anywhere????
			float x1 = -1.0,								// x1 - Destination base x
			float y1 = 1.0,									// y1 - Destination base y
			float x2 = 1.0,									// x2 - Destination opposite-corner x
			float y2 = -1.0								// y2 - Destination opposite-corner y
#if __XENON
			,const int exponentbias = 0);
#else
			);
#endif						

		// for debugging purposes show all the render targets involved
		void ShowRenderTargets();

		// shows a render target as a quad on the screen for debugging purposes
		void ShowRT(grcTexture * texture, 
			grcEffectTechnique technique,					// technique in FX file
			float DestX, float DestY, float SizeX, float SizeY);

#if __WIN32PC
		grcRenderTarget *m_FSCopy;
		static __THREAD bool m_RenderTargetActive;
#endif

		grcRenderTarget *m_ExposedRT;					// render target that holds the exposed full screen image
		grcRenderTarget *m_QuarterRT;					// render target that holds a 1/4 copy of the exposed full screen image
		grcRenderTarget *m_QuarterMotionBlurRT;
		grcRenderTarget *m_QuarterStreakRT;
		grcRenderTarget *m_ScratchQuarterRT;					// render target that holds a 1/4 copy of the exposed full screen image
		grcRenderTarget *m_SecondaryFinalOutputRT;			// render target that holds a 1/4 copy of the final (color corrected ppp image)
		grcRenderTarget *m_128x128RT;					// render target that holds luminance values of the m_QuarterRT
		grcRenderTarget *m_64x64RT;						// scaled down version of m_128x128RT
		grcRenderTarget *m_16x16RT;						// scaled down version of m_64x64RT
		grcRenderTarget *m_4x4RT;						// scaled down version of m_16x16RT
		grcRenderTarget *m_LumHistoryRT;						// scaled down version of m_4x4RT: holds in one texel a luminance value for the whole scene
		grcRenderTarget *m_CurrentAdaptedLum1x1RT;		// stores a 1 texel luminance value temporarily between passes
#if __WIN32PC
		grcRenderTarget *m_LastAdaptedLum1x1RT;			// stores a 1 texel luminance value temporarily between passes
#endif // __WIN32PC
		grcRenderTarget *m_BrightPassRT;				// stores the result of the bright-pass filter
		grcRenderTarget *m_PersistentBrightPassRT;		// stores the previous result of the bright-pass filter (to do light trails, etc.)
		grcRenderTarget *m_PrevPersistentBrightPassRT;	// stores the previous result of the bright-pass filter (to do light trails, etc.)
		grcRenderTarget *m_ScratchPadRT;				// stores Gauss blur in X direction and later other effects
		grcRenderTarget *m_ScratchPadRT2;
		grcRenderTarget *m_GaussRT;						// stores Gauss blur in Y direction
		grcRenderTarget *m_FogMapRT;					// stores accumulated fog texture
		//grcRenderTarget *m_StarStreakPattern[MAX_STAR_STREAKS/2 + 2];		// only need half streak buffers, as streaks grow in 2 directions. But then we want 2 smaller ones for the glow part.
		datCallback		*m_SeedFogPassCallback;
		datCallback		*m_SeedBrightPassCallback;
		datCallback		*m_PersistentBrightPassCallback;

		grcRenderTarget *m_DepthBufferRT;
		grcRenderTarget	*m_DepthBuffer;					// 360 can grab depth buffer as a texture
		grcRenderTarget	*m_BackBuffer;					// stores the back buffer
		grcRenderTarget	*m_FrontBuffer;					// points to the front buffer
#if __PPU
		grcRenderTarget	*m_MRTBuffer;					// points to the MRT buffer

#if HACK_MC4
		grcRenderTarget	*m_ResolveBuffer;
#endif // HACK_MC4
#endif
		grcTexture		*m_RandomMap;					// noise map for the old TV effect
		grcTexture		*m_LightGlowMap;				// texture for drawing light glow seeds 

		grcTexture		*m_VelocityMapTexture;

		// RDR2
		grcTexture		*m_DeathMap;					// Deathy texture
		grcTexture		*m_DeathSplat[NUM_BLOOD_SPLATTER_TEXTURES];
		dofFilter		m_DOFBlurType;
	   
#if RAGE_SSAO
		grcRenderTarget	*m_SSAOPingPongRT[2];
#endif // RAGE_SSAO

#if __XENON
		grcRenderTarget *m_DepthBuffer360FromTiling;			// Required by DOF when tiled rendering is on
		grcRenderTarget *m_ColorBuffer360FromTiling;			// if the app has already resolved the color buffer, we don't need to grabe it again...
 		bool m_PredicatedTiling;
#endif

		bool			m_NeedToCapture;	// flag to keep track of the screen capture for screen transitions
		grcRenderTarget *m_ScreenShot;		// holds the screen capture for screen transitions

		grmShader		*m_PostFXShader;	// post fx shader

		// handles for the render target sampler
		// in the tone mapping stage we will end up so far with four texture stages at the same time
		// - m_FullResMapId
		// - m_RenderMapId
		// - m_RenderMapBilinearId	// bilinear filtered version of the render sampler states above
		// - m_AdaptedLuminanceMapId
		grcEffectVar	m_FullResMapId;				// handle for the full screen render target sampler stages
		grcEffectVar	m_RenderMapId;				// handle for workhorse render target sampler stages
		grcEffectVar	m_RenderMapBilinearId;		// handle for bilinear filtered workhorse render target sampler stages
		grcEffectVar	m_RenderMapAnisoId;			// handle for anisotropic filtered workhorse render target sampler stage
#if __PPU
		// filter kernel is [1 2 1][2 4 2][1 2 1] * 1/16
		grcEffectVar	m_RenderMapGaussId;			// handle for gauss filtered render target
		// filter kernel is [1 4 1] [1 0 1] * 1/8
		grcEffectVar	m_RenderMapQuincunxId;		// handle for gauss filtered render target
#endif
		grcEffectVar	m_ToneMapId;				// handle for tone map render target sampler  stages (uses only point sampling)
		grcEffectVar	m_AdaptedLuminanceMapId;	// handle for temporary 1 texel render target sampler stages
		grcEffectVar	m_VertexTextureId;			// handle for vertex texture
		grcEffectVar	m_NoiseId;					// handle for the texture sampler states for the volume noise texture
		grcEffectVar	m_LightGlowMapId;			// handle for the light glow texture
		grcEffectVar	m_DepthMapId;				// handle for specific depth map sampler states
		grcEffectVar	m_FogMapId;
		grcEffectVar	m_StarStreakPatternId[MAX_STAR_STREAKS];
		grcEffectVar	m_FogCardTextureId;			// fog card to draw particle fog
		grcEffectVar	m_ProjInverseTransposeId;	// handle for InverseProjection matrix to go from screen back to world space.

		grcEffectVar	m_DistortionFreqId;
		grcEffectVar	m_DistortionScaleId;
		grcEffectVar	m_DistortionRollId;
// 		dofFilter		m_DOFBlurType;

		// RDR2
		grcEffectVar	m_DeathTexId;				// Death texture
		grcEffectVar	m_DeathSplatId[NUM_BLOOD_SPLATTER_TEXTURES];	
		
	public:
		// values for the shader variables
		struct grcPPPPreset
		{
			grcPPPPreset();

			void GetChooseTechniqueName(char * buffer, const int bufferSize, const grPostFXEffect *postFXEffect) const;
			const char *GetTechniqueName() const;
			void SetChooseTechnique(const char * chooseTechniqueName);

			Vector4		    m_ClearColor;				// color for color correction
			Vector4		    m_ColorCorrect;				// color for color correction
			Vector4			m_ConstAdd;					// add a color for color correction
			Vector4			m_DofParamters;
			Vector3			m_Scale;					// scaling parameter for radial blur
			// 			float			m_Exposure;					// variable that holds the exposure value
			float			m_BrightPassThreshold;		// threshold value for bright pass
			float			m_BrightPassOffset;			// offset value for bright pass
			float			m_MiddleGray;				// The middle gray key value. This value is used in the bright pass filter and the tone mapping stage

			// specific effects for the police cam
			float			m_BrightPassThresholdTwo;	// threshold value for bright pass
			float			m_BrightPassOffsetTwo;		// offset value for bright pass
			float			m_MiddleGrayTwo;			// The middle gray key value. This value is used in the bright pass filter and the tone mapping stage
			float			m_fBrightPassWhiteTwo;		// dedicated white value for the bright pass
			int				m_iNumOfFramesInLumHistory;	// the user can determine how many luminance values from the last frames are kept 

			float			m_fAdapt;					// adaption variable for simulating the bright/dark adaption
			float			m_fBloomIntensity;			// holds value for bloom intensity.
			float			m_deSat;					// de-saturate color by lerping between color and monochrome
			float			m_deSatTwo;					// Police Camera: de-saturate color by lerping between color and monochrome
			float			m_fWhite;					// this is used to minimize loss of contrast when tone mapping a low-dynamic range
			float			m_fBrightPassWhite;			// dedicated white value for the bright pass
			float			m_fContrast;				// contrast
			float			m_fContrastTwo;				// Police Camera: contrast
			Vector4			m_BlueShiftColor;			// Night blue shift color
			Vector4			m_BlueShiftParams;			// Night blue shift parameters r=scale, g=offset, b=light adaption value scale
 			int				m_iChooseTechnique;			// Technique index 

			grPostFXEffect *m_CurrentEffect;			// The effect to use
			bool			m_bLightAdaption;			// switches light adaption on and off
			float			m_fGrainIntensity;			// film grain effect: intensity
			float			m_fInterlaceIntensity;			// film grain effect: intensity
			float			m_fTimeFrame;				// film grain effect: time frame for going though the volume texture
			float			m_fInterference;			// interference due to offsetting texture look-ups

			bool			m_SwitchPoliceCam;
			float			m_fGamma;					// gamma control
			float			m_fLowerLimitAdaption;		// lower limit of the light adaption process
			float			m_fHigherLimitAdaption;		// higher limit of the light adaption process
			bool			m_ShowRenderTargets;

			// fog parameters
			Vector4			m_FogMinColor;				// Color at the top of fog
			Vector4			m_FogMaxColor;				// Color at the bottom of fog
			Vector4			m_FogParams;				// Fog parameters, x=top height in meters, y=bottom height in meters, z=fogdensity.
			Vector4			m_BrightPassParams;			// Bright pass parameters, x=current kawase bloom pass, y=persistent fog tone down scaler
			Vector4			m_ParticleFogParams;		// Fog parameters, x=top height in meters, y=bottom height in meters, z=fogdensity.
			Vector4			m_StreakParams;				// streak parameters
			float			m_StreakAttenuation[MAX_STREAK_ITERATIONS*4];
			float			m_StreakSamples;
			int				m_StreakIterations;			// number of iterations we are ping ponging in between
			int				m_BloomIterations;			// number of iterations we are ping ponging in between
			float			m_BloomIntensity;			// how much do we preserve between samples? (4 samples taken in shader, so should be 1.0/4.0)
			float			m_StreakIntensity;			// how much do we blend the streak into the frame buffer?
			int				m_NumStreaks;
			bool			m_StreakEnabled;

			float 			m_DistortionFreq;
			float 			m_DistortionScale;
			float 			m_DistortionRoll;

			Vector3			m_BlurEffectTintColor;
			float			m_BlurEffectTintFade;
			float			m_BlurEffectDesatScale;

			// RDR2 parameters
			float			m_fDeath;					// Death effect magnitude
			float			m_fDeathDrip;
			float			m_fDeathBlood;
			Vector4			m_fDeathColor;
#if RAGE_SSAO
			Vector4			m_SSAOTweakParams;
			bool			m_EnableSSAO;
#endif // RAGE_SSAO
		};

		// would need to be added to the structure above
		float	m_fRHO;						// rho value for the Gauss bloom filter

	protected:
		grcPPPPreset *m_PPPPresets;
		grcPPPPreset *m_PPPStack[PPP_STACK_SIZE];
		u8			 m_stacked;

		virtual grcPPPPreset *CreatePPPPreset();
		virtual int GetPPPPresetSize() const;

		virtual bool StorePPPPreset(const char* filename) const;
		virtual bool WritePPPPreset(parTreeNode *pRootNode) const;

		virtual bool LoadPPPPreset(grcPPPPreset *pPPPPreset, const char* filename) const;
		virtual bool ParsePPPPreset(grcPPPPreset *pPPPPreset, parTreeNode *pRootNode) const;

		void RegisterDefaultEffects();

#if __BANK
		// technique-specific widgets
		void UpdateTechniqueSpecificWidgets();

		// store widget data
		void LoadPPPPresetBankCbk();
		void SavePPPPresetBankCbk();
#endif  // __BANK

	public:

		bool SavePPPPreset( const char *pFileName ) const;
		virtual bool LoadPPPPreset( const char *pFileName );

		static bool LoadPPPPresetFunc( void *pObj, const char *pFilePath ) { return ((grPostFX *)pObj)->LoadPPPPreset(pFilePath); }
		static rage::atString SavePPPPresetFunc( void *pObj, const char *pFilePath );

	protected:
		class grPostFXEffectNone : public grPostFXEffect
		{
		public:
			grPostFXEffectNone(grPostFX *postFX);
			virtual void Process(grPostFX *postFX);
		};

		//
		// all SPU based PostFX come here
		//
#if __PS3

		// color filters on the SPU
		class grPostFXEdgeEffect : public grPostFXEffect
		{
		public:
			grPostFXEdgeEffect(grPostFX *postFX);
			
#if __BANK
			virtual void AddWidgets(grPostFX *postFX, bkBank &bk);
#endif // __BANK

			virtual void Process(grPostFX *postFX);

		private:
			static void RsxUserHandler(const u32 cause);
			void StartSpuPostprocessing();
			void SetupStage(EdgePostProcessStage* pStage, const EdgePostImage* pImages, u32 numImages, void* pJobCode, u32 jobCodeSize);
			void SetupDownsample(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa);
			void SetupDownsampleFloat(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa);
			void SetupUpsample(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa);
			void SetupUpsampleFloat(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa);
			void SetupHorizontalGauss(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa, float* weights);
			void SetupHorizontalGaussFloat(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa, float* weights);
			void SetupVerticalGauss(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa, float* weights);
			void SetupVerticalGaussFloat(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa, float* weights);
			void SetupBloomCaptureStage(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa);
			void SetupNearFuzziness(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa);
			void SetupFarFuzziness(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* nearEa, void* dstEa);
			void SetupDepthOfField(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* depthEa, void* dstEa);
			void SetupMotionBlur(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* motion, void* dstEa);
			void SetupAddSat(EdgePostProcessStage* pStage, u32 width, u32 height, void* src0, void* src1, void* dstEa);
			void SetupAddSatModulateBloom(EdgePostProcessStage* pStage, u32 width, u32 height, void* src0, void* src1, void* dstEa);
			void SetupIlr(EdgePostProcessStage* pStage, u32 width, u32 height, void* src, void* dstEa);

			static const u32		kMaxStages = 256;
			uint8_t*				m_PostprocessArena;		// postprocessing work area ( pixel buffers ... )
			uint8_t*				m_LocalToMainImage;		// RSX writes source image here
			uint8_t*				m_DepthImage;			// RSX writes depth here
			u32						m_LocalToMainOffset;	// RSX writes source image here (RSX offset )
			u32						m_DepthOffset;			// RSX writes depth here (RSX offset )
			u32						m_PostLabelValue;
			u32						m_NumStages;			// number of postprocessing stages
			EdgePostProcessStage	m_Stages[kMaxStages];	// array of stages
			EdgePostWorkload		m_Workload;				// workload descriptor
			PostParams				m_PostParams;			// postprocessing parameters
			// gauss weights
			static float			sm_BloomWeights[4];		// = { 0.3125f, 0.234375f, 0.09375f, 0.015625f};
			static float			sm_DoubleWeights[4];	// = { 0.3125f * 2.f, 0.234375f * 2.f, 0.09375f * 2.f, 0.015625f * 2.f};
		};

#endif



		class grPostFXEffectToneMapping : public grPostFXEffect
		{
		public:
			grPostFXEffectToneMapping(grPostFX *postFX);
#if __BANK
			virtual void AddWidgets(grPostFX *postFX, bkBank &bk);
#endif // __BANK
			virtual void Process(grPostFX *postFX);

		private:
#if RAGE_SSAO
			bool m_EnableSSAO;
#endif // RAGE_SSAO
		};

		class grPostFXEffectToneMappingDepthOfField : public grPostFXEffect
		{
		public:
			grPostFXEffectToneMappingDepthOfField(grPostFX *postFX);
#if __BANK
			virtual void AddWidgets(grPostFX *postFX, bkBank &bk);
#endif // __BANK
			virtual void Process(grPostFX *postFX);
		};

		class grPostFXEffectToneMappingDepthOfFieldGaussFog : public grPostFXEffect
		{
		public:
			grPostFXEffectToneMappingDepthOfFieldGaussFog(grPostFX *postFX);
#if __BANK
			virtual void AddWidgets(grPostFX *postFX, bkBank &bk);
#endif // __BANK
			virtual void Process(grPostFX *postFX);
		};

		class grPostFXEffectFog : public grPostFXEffect
		{
		public:
			grPostFXEffectFog(grPostFX *postFX);
#if __BANK
			virtual void AddWidgets(grPostFX *postFX, bkBank &bk);
#endif // __BANK
			virtual void Process(grPostFX *postFX);
		};




		atArray<grPostFXEffect *>	m_Effects;					// All registered PPP effects

		float						m_Counter;

#if __PPU
		float						m_AdaptedLuminance;
		grcEffectVar				m_AdaptedLuminanceId;		// handle for the variable that sets the scene adapted luminance
#endif // __PPU

		// handles for shader variables
 		grcEffectVar				m_ExposureId;				// handle for the variable that sets the exposure value
		grcEffectVar				m_TexelSizeId;				// handle for the variable that provides the size of one texel
		grcEffectVar				m_TexCoorId;				// handle for the variable that provides the size of one texel
		grcEffectVar				m_ElapsedTimeId;			// handle for the variable that holds the values to decelerate the adaption process
		grcEffectVar				m_LambdaId;								
		grcEffectVar				m_BrightPassValuesId;		// handle for the variable that holds bright pass related values. (BrightPassValues.x = threshold, BrightPassValues.y =  offset, 
		grcEffectVar				m_WhiteId;					// handle to tone mapping white value
		// BrightPassValues.z = middle gray key value,  BrightPassValues.w = White value)
		grcEffectVar				m_FramesID;					// handle to the number of frames that are stored to keep track of luminance changes


		grcEffectVar				m_ColorCorrectId;			// handle to hold variable for color correction
		grcEffectVar				m_ConstAddId;				// handle to hold variable for color addition
		grcEffectVar				m_deSatId;					// handle to hold desaturation value
		grcEffectVar				m_ContrastId;				// handle to hold contrast value
		grcEffectVar				m_ScaleId;					// handle for the radial blur parameter
		grcEffectVar				m_NearFarClipPlaneQId;		// handle for the near and far clipping plane and the Q parameter for the depth of field calculation
		grcEffectVar				m_DofParamsId;				// handle for DOF
		grcEffectVar				m_horzTapOffsId;			// handle for Gauss blur horizontal texture coordinate offsets
		grcEffectVar				m_vertTapOffsId;			// handle for Gauss blur vertical texture coordinate offsets
		grcEffectVar				m_TexelWeightId;			// handle for Gauss texel weight
		grcEffectVar				m_fIntensityGrainId;		// handle for the intensity parameter of the film grain effect
		grcEffectVar				m_fInterlaceIntensityId;	// handle for the intensity of the interlace effect in the police camera
		grcEffectVar				m_fTimeFrameId;				// handle for time frame in which the third channel of the volume texture is gone through
		grcEffectVar				m_fInterferenceId;			// handle for interference in the film grain effect
		grcEffectVar				m_fGammaId;					// handle for gamma control
		grcEffectVar				m_LowerLimitAdaptionId;		// handle for value that is used to this is used to minimize loss of contrast when tone mapping a low-dynamic range
		grcEffectVar				m_HigherLimitAdaptionId;	// handle for value that is used to this is used to minimize loss of contrast when tone mapping a low-dynamic range
		grcEffectVar				m_BloomIntensity;			// handle for standard bloom intensity (non-streak)

		// fog parameters 
		grcEffectVar				m_FogMinColorId;			// handle to hold minimum fog color
		grcEffectVar				m_FogMaxColorId;			// handle to hold maximum fog color
		grcEffectVar				m_FogParamsId;				// handle to hold fog parameters
		grcEffectVar				m_ParticleFogParamsId;				// handle to hold fog parameters

		// RDR2 parameters
		grcEffectVar				m_DeathId;
		grcEffectVar				m_DeathDripId;
		grcEffectVar				m_DeathBloodId;
		grcEffectVar				m_DeathColorId;

		// Blue shift
		grcEffectVar				m_BlueShiftColorId;			// handle to hold blue shift color
		grcEffectVar				m_BlueShiftParamsId;		// handle to hold blue shift parameters (offset / scale)
		// Light streak
		grcEffectVar				m_LightStreakDirectionId;	// handle to hold Light Streak direction
		grcEffectVar				m_StreakParamsId;			// handle to hold Light Streak other parameters 
		grcEffectVar				m_LightStreakWeightsId;
		grcEffectVar				m_BrightPassParamsId;		// handle to hold bright pass params, r = iterations for Masake Kawase's filter, g = tone down scale for persisten bright pass

		// handles for techniques
		grcEffectTechnique				m_SimpleCopyTechnique;
		grcEffectTechnique				m_CopyTechnique;						// technique that just copies 1:1 render targets
		grcEffectTechnique				m_CopyRedTechnique;						// technique that just copies the red channel across all the other channel
		grcEffectTechnique				m_CopyAlphaTechnique;					// technique that just copies alpha grcEffectTechniqueo a render target
		grcEffectTechnique				m_CopyColorScaleTechnique;				// technique that just copies render targets and scales each color by a constant 
		grcEffectTechnique				m_SeedBrightPassTechnique;				// technique that just copies seeds the bright pass render target for star streaks
		grcEffectTechnique				m_SeedBrightPassNoZTechnique;
		grcEffectTechnique				m_FogCardTechnique;					// technique that just copies seeds the bright pass render target for star streaks
		grcEffectTechnique				m_ToneDownTechnique;						// technique to tone down persistent bright pass

#if __PPU
		grcEffectTechnique				m_PS3QuincunxFilterTech;				// technique that runs a Quincunx filter on the PS3 while blitting
		grcEffectTechnique				m_PS3GaussFilterTech;					// technique that runs a Gauss filter on the PS3 while blitting
#endif

		grcEffectTechnique				m_SampleAverageLuminanceTechnique;		// technique that is used to average the luminance values during scale with a 3x3 pattern
		grcEffectTechnique				m_ReSampleAverageLuminanceTechnique;		// technique that is used to average the luminance values during scale with a 4x4 pattern
		grcEffectTechnique				m_ReSampleAverageLuminanceExpTechnique;		// technique that is used to average the luminance values during scale and uses the exponent result with a 4x4 pattern
		grcEffectTechnique				m_CalculateAdaptedLuminanceTechnique;		// technique that calculates the adapted luminance
		grcEffectTechnique				m_GaussXTech;
		grcEffectTechnique				m_GaussYTech;
		grcEffectTechnique				m_GaussX25Tech;							// 25-tap Gauss filter ... very smooth, but expensive
		grcEffectTechnique				m_GaussY25Tech;
		grcEffectTechnique				m_GaussX5Tech;							// 5-tap Gauss filter probably a simulated 12 tap?  ... very smooth, but expensive
		grcEffectTechnique				m_GaussY5Tech;
		grcEffectTechnique				m_GaussX17Tech;
		grcEffectTechnique				m_GaussY17Tech;
		grcEffectTechnique				m_BrightPassTechnique;					// technique that uses shaders that apply a bright pass to the target render target
		grcEffectTechnique				m_ToneMappingTechnique;					// technique that holds the shaders for tone mapping and the final screen blit
		grcEffectTechnique				m_PoliceCamTechnique;				
		grcEffectTechnique				m_ToneMapDOFTechnique;					// technique that holds tone mapping + Depth of Field
		grcEffectTechnique				m_ToneMapDOFFogTechnique;				// technique that holds tone mapping + Depth of Field + Fog
		grcEffectTechnique				m_ToneMapFogTechnique;					// technique that holds tone mapping + Fog
		grcEffectTechnique				m_ToneMapFogDistortTechnique;			// technique that holds tone mapping + Fog + Distort
		grcEffectTechnique				m_ToneMapFogPoliceCamTechnique;		
		grcEffectTechnique				m_OnlyColorFiltersTechnique;			// technique that holds only the color filters: used when post-processing is switched off
		grcEffectTechnique				m_InitialDownsampleTechnique;
		grcEffectTechnique				m_DownSampleTechnique;					// technique that down samples to 1/4 render target
		grcEffectTechnique				m_DownSampleMotionBlurTechnique;		// technique that down samples and copies velocity map magnitude
		grcEffectTechnique				m_DepthDownSampleTechnique;				// technique that down samples for the depth filter
		grcEffectTechnique				m_BrightGaussPassTechnique;				// combined bright and gauss pass ...
		grcEffectTechnique				m_DownSampleNBlurTechnique;				// downsample and blur at the same time
		grcEffectTechnique				m_FogTechnique;							// simple fog technique
		grcEffectTechnique				m_CopyBlurFactorTechnique;				// preview to visualize the blur factor for depth of field
		grcEffectTechnique				m_KawaseBloomTechnique;					// technique that applies a glare filter to imitate the bloom effect of the optical never of the eye
		grcEffectTechnique				m_Convert12bitToL16uvTechnique;			// technique used to copy from the 12-bit RGB value stored in a MRT to the L16uv value
		grcEffectTechnique				m_KawaseStreakTechnique;				// technique that applies a glare filter to imitate the bloom effect of the optical never of the eye
		grcEffectTechnique				m_RDR2DeathTechnique;					// RDR2 death effect
		grcEffectTechnique				m_BlendedFogTechnique;					// technique to apply fog with it's own separate pass

#if MC4_MOTIONBLUR
		grcEffectTechnique				m_MC4MotionBlurCombine;
		grcEffectTechnique				m_MotionBlurStreakTechnique;
		grcEffectVar					m_QuarterMapId;
#endif // MC4_MOTIONBLUR


#if RAGE_SSAO
		grcEffectTechnique				m_SSAODownscaleTechnique;
		grcEffectTechnique				m_SSAOMainTechnique;
		grcEffectTechnique				m_SSAOBlurXTechnique;
		grcEffectTechnique				m_SSAOBlurYTechnique;
		grcEffectTechnique				m_SSAOUpscaleTechnique;
		grcEffectVar					m_SSAOProjectionParamsId;
		grcEffectVar					m_SSAOMapId;
		grcEffectVar					m_SSAOTweakParamsId;
#endif // RAGE_SSAO
		grcEffectVar					m_ScreenSizeId;

		grcEffectVar					m_VelocityMapId;
		grcEffectVar					m_BlurEffectTintColorId;
		grcEffectVar					m_BlurEffectTintFadeId;
		grcEffectVar					m_BlurEffectDesatScaleId;

		static grPostFX *sm_instance;			// instance of post processing effects

#if __XENON
		// stores the number of pixel threads currently used by the 360
		int m_CurrentPixelThreads;
#endif

#if __XENON
		// stores the number of pixel threads currently used by the 360
		grcResolveFlags m_ClearParams;
#endif

		static const char* sm_BrightPassBlurNames[];

		static const char *sm_DefaultTexturePath;

		bool m_allowGameOverride;												// Junctioned to a toggle widget.  When false prevents game code from overriding postfx settings, so you can tune them by hand in RAG.
		bool m_saveDOFTypeWithPresets;		// (RAG widget) When true, saves the DOF blur kernel with the preset
	};

#define GRPOSTFX (rage::grPostFX::GetInstance())
}	// name space range

#endif // __GRPOSTFX_H_
