#define NICEGAUSS 0

// this is based on "A Steerable Streak Filter" by Chris Oat in ShaderX3
#define KAWASE_STREAK 0
#define BLUESHIFT 0
#define PERSISTENT_BRIGHTPASS 0
#define LOW_PRECISION_INTERMEDIATE_TARGETS 0
#define MC4_MOTIONBLUR 0
