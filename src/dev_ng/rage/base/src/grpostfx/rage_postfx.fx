// -------------------------------------------------------------
// RAGE Post-Processing Pipeline
// 
// -------------------------------------------------------------
#include "postfx_cfg.h"

#pragma dcl position texcoord0

#define NO_SKINNING
#include "../shaderlib/rage_common.fxh"
#include "../shaderlib/rage_hdr.fxh"
#include "../shaderlib/rage_alternativecolorspaces.fxh"
#include "../shaderlib/rage_xplatformtexturefetchmacros.fxh"

// floating-point depth buffer support on the 360 needs -fpz
#if __PS3
#define FLOAT half
#define FLOAT2 half2
#define FLOAT3 half3
#define FLOAT4 half4
#define FLOAT3x3 half3x3
#else
#define FLOAT float
#define FLOAT2 float2
#define FLOAT3 float3
#define FLOAT4 float4
#define FLOAT3x3 float3x3
#endif
// fog values
FLOAT4			FogMinColor;				// Color at minimum density of fog
FLOAT4			FogMaxColor;				// Color at maximum density of fog
FLOAT4			FogParams;					// Top height in meters of fog (fog color = m_FogTopColor)
FLOAT4			ParticleFogParams;
// blue shift
FLOAT4			BlueShiftColor;			// Blue shift color
FLOAT4			BlueShiftParams;		// r = scale, g = offset, b = light adaption value scale

// light adaptation history
FLOAT2 TexCoord;

// light streak
FLOAT4 LightStreakDirection;			// rg = uv direction, b = bloomIntensity, w = streakSamples
#define bloomIntensity LightStreakDirection.b
#define streakSamples LightStreakDirection.w
FLOAT4 streakWeights;			// 

FLOAT4 StreakParams = FLOAT4(1.0f/3.0f, 1.0f,0.0f,0.0f);
#define ColorScale StreakParams.r
#define SeedIntensity StreakParams.g
#define streakIntensity StreakParams.b


float4x4 WorldViewProjInverse;

// set exposure level
// FLOAT    Exposure;

// this holds the elapsed time 
FLOAT     ElapsedTime;
// the number of frames the user chose to keep track of the adapted luminance values
float	  Frames;
FLOAT     Lambda;

// this is were the values for the bright pass are stored
FLOAT4 BrightPassValues; 

#define BrightPassThreshold		BrightPassValues.x
#define BrightPassOffset		BrightPassValues.y
#define MiddleGray				BrightPassValues.z
#define BrightPassWhite			BrightPassValues.w

FLOAT White;

// Bloom multiplier
float	IntensityBloom;

FLOAT4 brightPassParams;

// Kawase's Bloom
#define fIteration brightPassParams.r
#define tonedownspeed brightPassParams.g

// 25-tap Gauss filter
FLOAT2	horzTapOffs[7];		// Gauss blur horizontal texture coordinate offsets
FLOAT2	vertTapOffs[7];		// Gauss blur vertical texture coordinate offsets
FLOAT4	TexelWeight[16];	// texel weight


// luminance values            
const FLOAT3 LUMINANCE  = FLOAT3(0.2125f, 0.7154f, 0.0721f);


// color correction and de-saturation value
FLOAT deSat = 1.0f;
FLOAT4 ConstAdd;
FLOAT4 ColorCorrect;
FLOAT Contrast= 1.0f;

// for radial blur
FLOAT3 Scale;

// DOF
FLOAT4 DofParams;
FLOAT3 NearFarClipPlaneQ;

#if RAGE_MOTIONBLUR && !RAGE_VELOCITYBUFFER
// motion blur
float4x4 gPrevViewProj;
float4x4 gViewProjInverse;
#endif // RAGE_MOTIONBLUR && !RAGE_VELOCITYBUFFER

// maximum CoC radius and diameter in pixels
FLOAT2 vMaxCoC = FLOAT2(5.0f,10.0f);

// scale factor for maximum CoC size on low res. image
FLOAT radiusScale = 0.3f;

// Police Camera
FLOAT intensityGrain;
FLOAT intensityInterlace;
FLOAT frameShape;
FLOAT frameLimit;
FLOAT interference;

// RDR2
FLOAT DeathMag;
FLOAT DeathDrip;
FLOAT DeathBlood;
FLOAT4 DeathColor;

// gamma control
FLOAT Gamma;

// light adaption control
FLOAT LowerLimitAdaption;
FLOAT HigherLimitAdaption;

// distortion controls
float distortionFreq;
float distortionScale;
float distortionRoll;

FLOAT3 blurEffectTintColor = half3(0.0f, 0.0f, 0.0f);
FLOAT blurEffectTintFade = 0.0f;
FLOAT blurEffectDesatScale = 0.0f;

float4 SSAOProjectionParams;
float4 SSAOTweakParams = { 5.0f, 3.0f, 37.0f, 4.0f };
#define SSAOIntensity SSAOTweakParams.x
#define SSAOMinBlurDiscSize SSAOTweakParams.y
#define SSAOMaxBlurDiscSize SSAOTweakParams.z
#define SSAOMaxBlurDiscSizeDepth SSAOTweakParams.w

float4 gScreenSize;

BeginSampler(sampler2D,SSAOMap,SSAOMapSampler,SSAOMap)
ContinueSampler(sampler2D,SSAOMap,SSAOMapSampler,SSAOMap)
	MIN_POINT_MAG_POINT_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginSampler(sampler2D,FullResMap,FullResMapSampler,FullResMap)
ContinueSampler(sampler2D,FullResMap,FullResMapSampler,FullResMap)
	MIN_POINT_MAG_POINT_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;


BeginSampler(sampler2D,RenderMap,RenderMapSampler,RenderMap)
ContinueSampler(sampler2D,RenderMap,RenderMapSampler,RenderMap)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

#if __PS3
// those sampler states are only available on the PS3
BeginSampler(sampler2D,RenderGaussMap,RenderMapGaussSampler,RenderGaussMap)
ContinueSampler(sampler2D,RenderGaussMap,RenderMapGaussSampler,RenderGaussMap)
   MinFilter = GAUSSIAN;
   MagFilter = GAUSSIAN;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginSampler(sampler2D,RenderQuincunxMap,RenderMapQuincunxSampler,RenderQuincunxMap)
ContinueSampler(sampler2D,RenderQuincunxMap,RenderMapQuincunxSampler,RenderQuincunxMap)
   MinFilter = QUINCUNX;
   MagFilter = QUINCUNX;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;
#endif

#if __XENON
BeginSampler(sampler2D, VertexTexture, VertexTextureSampler, VertexTex)
    int UIHidden = 1;
ContinueSampler(sampler2D,VertexTexture, VertexTextureSampler,VertexTex)
   MinFilter = POINT;
   MagFilter = POINT;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

// even simpler 
// Even simpler passthrough vertex shader - this one assumes positions are pretransformed
//	Good for screen space blits.
// this time with four channels in the texture coordinate register
rageVertexOutputPassThrough4TexOnly VS_ragePassThroughNoXform4TexOnly(rageVertexInput IN)
{
 	rageVertexOutputPassThrough4TexOnly OUT;
	OUT.pos = FLOAT4(IN.pos, 1.0f);
	
	// this is a one channel texture
#if __XENON
	float luminance = clamp(tex2Dlod(VertexTextureSampler, FLOAT4(IN.texCoord0,0.0f,0.0f)).r, LowerLimitAdaption, HigherLimitAdaption);
#else
	float luminance = clamp(h1tex2D(VertexTextureSampler, IN.texCoord0), LowerLimitAdaption, HigherLimitAdaption);
#endif
	
    OUT.texCoord0 = float4(IN.texCoord0, 1.0f, 1.0f);
    OUT.texCoord1 = float4(luminance.xxx, 1.0f);

    return OUT;
}

// -------------------------------------------------------------
// Vertex shader that passes the current screen position as TEXCOORD2, useful for mapping back into worldspace.
// this is currently for the 360 only
// -------------------------------------------------------------
rageVertexOutputPassThrough4TexOnly VS_ragePassThroughNoXform4TexAndScrPos(rageVertexInput IN)
{
 	rageVertexOutputPassThrough4TexOnly OUT =(rageVertexOutputPassThrough4TexOnly)0;
	OUT.pos = FLOAT4(IN.pos, 1.0f);
	
#if __XENON
	float luminance = clamp(tex2Dlod(VertexTextureSampler, FLOAT4(IN.texCoord0,0.0f,0.0f)).r, LowerLimitAdaption, HigherLimitAdaption);
#else
	float luminance = clamp(h1tex2D(VertexTextureSampler, IN.texCoord0), LowerLimitAdaption, HigherLimitAdaption);
#endif

	FLOAT2 halfscres = TexelSize.zw * 0.5f;
    OUT.texCoord0 = float4(IN.texCoord0.x, IN.texCoord0.y, 
						   IN.pos.x * halfscres.x + halfscres.x,
						 - IN.pos.y * halfscres.y + halfscres.y);
						 
	// transport the luminance value to the pixel shader
	// the last two values hold the signed pos value that is used to fetch a 3D noise map
	FLOAT2 SignedPos = sign(IN.pos.xy);

    OUT.texCoord1 = FLOAT4(luminance, 0.0f, SignedPos.x, SignedPos.y);

    return OUT;
}
#endif

BeginSampler(sampler2D,RenderBilinearMap,RenderMapBilinearSampler,RenderBilinearMap)
ContinueSampler(sampler2D,RenderBilinearMap,RenderMapBilinearSampler,RenderBilinearMap)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginSampler(sampler2D,RenderAnisoMap,RenderMapAnisoSampler,RenderAnisoMap)
ContinueSampler(sampler2D,RenderAnisoMap,RenderMapAnisoSampler,RenderAnisoMap)
	MIN_ANISO_MAG_ANISO_MIP_NONE;
    AddressU      = CLAMP;
    AddressV      = CLAMP;
    MaxAnisotropy = 16;
EndSampler;

BeginSampler(sampler2D,DepthMap,DepthMapSampler,DepthMap)
ContinueSampler(sampler2D,DepthMap,DepthMapSampler,DepthMap)
	MIN_POINT_MAG_POINT_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginSampler(sampler2D,ToneMap,ToneMapSampler,ToneMap)
ContinueSampler(sampler2D,ToneMap,ToneMapSampler,ToneMap)
	MIN_POINT_MAG_POINT_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginSampler(sampler2D,AdaptedLuminanceMap,AdaptedLuminanceMapSampler,AdaptedLuminanceMap)
ContinueSampler(sampler2D,AdaptedLuminanceMap,AdaptedLuminanceMapSampler,AdaptedLuminanceMap)
	MIN_POINT_MAG_POINT_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

#if RAGE_MOTIONBLUR || MC4_MOTIONBLUR
#if RAGE_VELOCITYBUFFER || MC4_MOTIONBLUR
BeginSampler(sampler2D,VelocityMap,VelocityMapSampler,VelocityMap)
ContinueSampler(sampler2D,VelocityMap,VelocityMapSampler,VelocityMap)
	MIN_POINT_MAG_POINT_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;
#endif // RAGE_VELOCITYBUFFER

BeginSampler(sampler2D,QuarterMap,QuarterMapSampler,QuarterMap)
ContinueSampler(sampler2D,QuarterMap,QuarterMapSampler,QuarterMap)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginSampler(sampler2D,JitterMap,JitterMapSampler,JitterMap)
ContinueSampler(sampler2D,JitterMap,JitterMapSampler,JitterMap)
	MIN_POINT_MAG_POINT_MIP_NONE;
   AddressU  = WRAP;
   AddressV  = WRAP;
EndSampler;
#endif // RAGE_MOTIONBLUR

#if !__PSP2
BeginSampler(sampler3D,NoiseTexture,NoiseSampler,NoiseTex)
	string UIName="__noisemap";
    string ResourceName = "__noisemap";
ContinueSampler(sampler3D,NoiseTexture,NoiseSampler,NoiseTex)
	AddressU = WRAP;
	AddressV = WRAP;
	AddressW = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;
#else
BeginSampler(sampler2D,NoiseTexture,NoiseSampler,NoiseTex)
	string UIName="__noisemap";
    string ResourceName = "__noisemap";
ContinueSampler(sampler3D,NoiseTexture,NoiseSampler,NoiseTex)
	AddressU = WRAP;
	AddressV = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;
#define tex3D(sampler, float3) tex2D((sampler), (float3).xy)
#endif

BeginSampler(sampler2D,LightGlowTex,LightGlowTexSampler,LightGlowTex)
	string UIName="__lightglowmap";
    string ResourceName = "__lightglowmap";
ContinueSampler(sampler2D,LightGlowTex,LightGlowTexSampler,LightGlowTex)
	AddressU = CLAMP;
	AddressV = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
EndSampler;



BeginSampler(sampler2D,StreakMap0,StreakMap0Sampler,StreakMap0)
ContinueSampler(sampler2D,StreakMap0,StreakMap0Sampler,StreakMap0)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = WRAP;
   AddressV  = WRAP;
EndSampler;

BeginSampler(sampler2D,StreakMap1,StreakMap1Sampler,StreakMap1)
ContinueSampler(sampler2D,StreakMap1,StreakMap1Sampler,StreakMap1)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginSampler(sampler2D,StreakMap2,StreakMap2Sampler,StreakMap2)
ContinueSampler(sampler2D,StreakMap2,StreakMap2Sampler,StreakMap2)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;
/*
BeginSampler(sampler2D,StreakMap3,StreakMap3Sampler,StreakMap3)
ContinueSampler(sampler2D,StreakMap3,StreakMap3Sampler,StreakMap3)
   MinFilter = LINEAR;
   MagFilter = LINEAR;
   MipFilter = NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;
*/
#pragma sampler 0

BeginSampler(sampler2D,FogTexture,FogTextureSampler,FogTexture)
ContinueSampler(sampler2D,FogTexture,FogTextureSampler,FogTexture)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = WRAP;
   AddressV  = WRAP;
EndSampler;

BeginSampler(sampler2D,FogMap,FogMapSampler,FogMap)
ContinueSampler(sampler2D,FogMap,FogMapSampler,FogMap)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

// RDR2

BeginSampler(sampler2D, DeathTexture, DeathSampler, DeathTex)
ContinueSampler(sampler2D, DeathTexture, DeathSampler, DeathTex)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginSampler(sampler2D, DeathSplatTexture1, DeathSplatSampler1, DeathSplat1)
ContinueSampler(sampler2D, DeathSplatTexture1, DeathSplatSampler1, DeathSplat1)
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;


#if !__XENON
FLOAT AdaptedLuminance;
rageVertexOutputPassThroughTexOnly VS_ragePassThroughNoXformToneMapping(rageVertexInput IN)
{
 	rageVertexOutputPassThroughTexOnly OUT;
	OUT.pos = FLOAT4(IN.pos, 1.0f);
    OUT.texCoord0.xy = IN.texCoord0;
#if __PS3
    OUT.texCoord0.z = clamp(AdaptedLuminance, LowerLimitAdaption, HigherLimitAdaption);
    OUT.texCoord0.w = 0;
#else
	OUT.texCoord0.zw = 0.0f.xx;
#endif

    return OUT;
}
#endif // !__XENON

// -------------------------------------------------------------
// PSCopyRT
// - plain copy
// -------------------------------------------------------------
FLOAT4 PSCopyRT( rageVertexOutputPassThroughTexOnly IN): COLOR
{
     return h4tex2D(ToneMapSampler, IN.texCoord0.xy);
}

float4 PSDepthDownsample(rageVertexOutputPassThroughTexOnly IN, out float oDepth : DEPTH) : COLOR
{
	// sample 4 times
	float dx = TexelSize.x;
	float dy = TexelSize.y;
	float d0 = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy + float2(0, 0)).r;
	float d1 = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy + float2(dx, 0)).r;
	float d2 = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy + float2(0, dy)).r;
	float d3 = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy + float2(dx, dy)).r;

	// downsample depth (take closer to camera)
	oDepth = min(min(d0, d1), min(d2, d3));
	return 1.0f.xxxx;
}

// -------------------------------------------------------------
// PSCopyRedRT
// - plain copy of red channel
// -------------------------------------------------------------
FLOAT4 PSCopyRedRT( rageVertexOutputPassThroughTexOnly IN): COLOR
{
     return float4(h4tex2D(ToneMapSampler, IN.texCoord0.xy).x, h4tex2D(ToneMapSampler, IN.texCoord0.xy).x, h4tex2D(ToneMapSampler, IN.texCoord0.xy).x, 1.0);
}

// -------------------------------------------------------------
// PSCombineBrightPassRT
// - Combine light streaks and bright pass
// -------------------------------------------------------------
FLOAT4 PSCombineBrightPassRT( rageVertexOutputPassThroughTexOnly IN): COLOR
{
     FLOAT4 cntstreak = 
     h4tex2D(StreakMap0Sampler, IN.texCoord0.xy) + 
     h4tex2D(StreakMap1Sampler, IN.texCoord0.xy); 
     
     FLOAT4 ofsstreak = _H4Tex2DOffset(StreakMap0Sampler, IN.texCoord0.xy, FLOAT2(  1.0f,  0.0f )) + 
     _H4Tex2DOffset(StreakMap0Sampler, IN.texCoord0.xy, FLOAT2(  -1.0f,  0.0f )) + 
     _H4Tex2DOffset(StreakMap0Sampler, IN.texCoord0.xy, FLOAT2(  0.0f,  1.0f )) + 
     _H4Tex2DOffset(StreakMap0Sampler, IN.texCoord0.xy, FLOAT2(  0.0f,  -1.0f )) + 
     _H4Tex2DOffset(StreakMap1Sampler, IN.texCoord0.xy, FLOAT2(  1.0f,  0.0f )) + 
     _H4Tex2DOffset(StreakMap1Sampler, IN.texCoord0.xy, FLOAT2(  -1.0f,  0.0f )) + 
     _H4Tex2DOffset(StreakMap1Sampler, IN.texCoord0.xy, FLOAT2(  0.0f,  1.0f )) + 
     _H4Tex2DOffset(StreakMap1Sampler, IN.texCoord0.xy, FLOAT2(  0.0f,  -1.0f ));
     FLOAT4 streak = (ofsstreak * 0.125f + cntstreak * 0.25f) * streakIntensity;
     return streak +
     h4tex2D(RenderMapBilinearSampler, IN.texCoord0.xy) + 
     h4tex2D(StreakMap2Sampler, IN.texCoord0.xy)
     ;
}

// -------------------------------------------------------------
// PSCopyAlphaRT
// - plain alpha copy
// -------------------------------------------------------------
FLOAT4 PSCopyAlphaRT( rageVertexOutputPassThroughTexOnly IN): COLOR
{
     return h4tex2D(RenderMapSampler, IN.texCoord0.xy).a;
}

//--------------------------------------------------------------
// Convert from two MRT render targets that offer RGB 12-bit 
// resolution to L16uv color format
//--------------------------------------------------------------
FLOAT4 PSConvertRGB12bitToL16uv( rageVertexOutputPassThroughTexOnly IN): COLOR
{
	FLOAT4 Low = h4tex2D(RenderMapSampler, IN.texCoord0.xy);
	FLOAT4 High = h4tex2D(FullResMapSampler, IN.texCoord0.xy);

	// store 
	return FLOAT4(High.xyz * VALUE_RANGE + Low.xyz * (VALUE_RANGE/FOUR_BIT_RANGE), Low.w);
}

//-----------------------------------------------------------------------------
// Pixel Shader: PSScaleBuffer
// Desc: Perform a high-pass filter on the source texture and scale down.
//-----------------------------------------------------------------------------
FLOAT4 PSScaleBuffer( rageVertexOutputPassThroughTexOnly IN ) : COLOR0
{
#if __XENON
	return h4tex2D(RenderMapSampler, IN.texCoord0.xy);
#elif __PS3 || __WIN32PC || __PSP2
	FLOAT4 result = h4tex2D(RenderMapSampler, IN.texCoord0.xy);
	
	if (isinf(dot(result,result)) || isnan(dot(result,result)))
		result = 0.0f;
	
	return result;
#endif		
}

#if MC4_MOTIONBLUR
FLOAT4 PSScaleMotionBlur( rageVertexOutputPassThroughTexOnly IN ) : COLOR0
{
	half4 velMap = tex2D(VelocityMapSampler, IN.texCoord0.xy);
	return FLOAT4(h4tex2D(RenderMapSampler, IN.texCoord0.xy).xyz/** Exposure*/, velMap.z);
}
#endif

FLOAT4 PSInitialScaleBuffer( rageVertexOutputPassThroughTexOnly IN ) : COLOR0
{
#if RAGE_ENCODEOPAQUECOLOR
	// **** Need to really do some kind of manual bilinear here ****
	return FLOAT4(rageDecodeOpaqueColor(h4tex2D(FullResMapSampler, IN.texCoord0.xy))/* * Exposure*/, 1);
#else
	#if __XENON
		return h4tex2D(RenderMapSampler, IN.texCoord0.xy);
	#elif __PS3 || __WIN32PC || __PSP2
		FLOAT4 result = h4tex2D(RenderMapSampler, IN.texCoord0.xy);
		
		if (isinf(dot(result,result)) || isnan(dot(result,result)))
			result = 0.0f;
		
		return result;
	#endif		
#endif
}

#if __PS3
// 
// the following filters are PS3 specific
// they utilize PS3-only hardware supported filters
//
//-----------------------------------------------------------------------------
// Pixel Shader: PSScaleBuffer
// Desc: Perform a high-pass filter on the source texture and scale down.
//		 filter kernel is [1 4 1] [1 0 1] * 1/8
//-----------------------------------------------------------------------------
FLOAT4 PSScaleBuffer_QUINCUNX( rageVertexOutputPassThroughTexOnly IN ) : COLOR0
{
	//return h4tex2D( RenderMapQuincunxSampler, IN.texCoord0.xy);
	FLOAT4 result = h4tex2D(RenderMapQuincunxSampler, IN.texCoord0.xy);
	
	if (isinf(dot(result,result)) || isnan(dot(result,result)))
		result = 0.0f;
	
	return result;
}

//-----------------------------------------------------------------------------
// Pixel Shader: PSGaussBlurBuffer
// Desc: Perform a PS3-specific Gauss filter while bliting
//		 filter kernel is [1 2 1][2 4 2][1 2 1] * 1/16
//-----------------------------------------------------------------------------
FLOAT4 PSGaussBlurBuffer( rageVertexOutputPassThroughTexOnly IN ) : COLOR0
{
	FLOAT4 result = h4tex2D(RenderMapGaussSampler, IN.texCoord0.xy);
	
	if (isinf(dot(result,result)) || isnan(dot(result,result)))
		result = 0.0f;
	
	return result;
}

#endif

//-----------------------------------------------------------------------------
// Pixel Shader: PSScaleBuffer
// Desc: Perform a high-pass filter on the source texture and scale down.
//-----------------------------------------------------------------------------

FLOAT4 PSScaleBuffer_16tap( rageVertexOutputPassThroughTexOnly IN ) : COLOR0
{
	FLOAT4 outColor = 0.0f;
	
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(1.5f , -1.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(1.5f , -0.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(1.5f ,  0.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(1.5f ,  1.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(0.5f , -1.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(0.5f , -0.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(0.5f ,  0.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(0.5f ,  1.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f ,-1.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f ,-0.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f , 0.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f ,  1.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f , -1.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f , -0.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f ,  0.5f) );
	outColor += _H4Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f ,  1.5f) );

	return outColor * 1.0f/16.0f/* * Exposure */; 
}

// -------------------------------------------------------------
// 1st pass of 4 passes to measure luminance
// texture is fetched in a 3x3 pattern
// mimics a 3x3 filter with a 4 bilinear texture look ups
// -------------------------------------------------------------
FLOAT4 PSSampleAverageLuminance(rageVertexOutputPassThroughTexOnly IN): COLOR                
{
    // used to prevent black pixels
    const FLOAT Delta = 0.0001f;    
    
    FLOAT fLogLumSum = 0.0f;
    fLogLumSum += rageLog(dot(_Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.0f, -1.0f)).rgb, LUMINANCE)+ Delta);
    fLogLumSum += rageLog(dot(_Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.0f,  0.0f)).rgb, LUMINANCE)+ Delta);
    fLogLumSum += rageLog(dot(_Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.0f,  1.0f)).rgb, LUMINANCE)+ Delta);
    fLogLumSum += rageLog(dot(_Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.0f,  -1.0f)).rgb, LUMINANCE)+ Delta);
    fLogLumSum += rageLog(dot(tex2D( ToneMapSampler, IN.texCoord0.xy).rgb, LUMINANCE)+ Delta);
    fLogLumSum += rageLog(dot(_Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.0f,   1.0f)).rgb, LUMINANCE)+ Delta);
    fLogLumSum += rageLog(dot(_Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.0f,  -1.0f)).rgb, LUMINANCE)+ Delta);
    fLogLumSum += rageLog(dot(_Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.0f,   0.0f)).rgb, LUMINANCE)+ Delta);
    fLogLumSum += rageLog(dot(_Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.0f,   1.0f)).rgb, LUMINANCE)+ Delta);
    
    // Divide the sum to complete the average
    return fLogLumSum * 0.111111f;
}


// -------------------------------------------------------------
//
// -------------------------------------------------------------
FLOAT4 PSReSampleAverageLuminance(rageVertexOutputPassThroughTexOnly IN): COLOR                
{
    FLOAT fSum = 0.0f;

	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.5f , -1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.5f , -0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.5f ,  0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.5f ,  1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.5f , -1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.5f , -0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.5f ,  0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.5f ,  1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f ,-1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f ,-0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f , 0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f ,  1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f , -1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f , -0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f ,  0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f ,  1.5f) ).x;

    // Divide the sum to complete the average
    return fSum /= 16.0f;
}

// -------------------------------------------------------------
// 4th pass of 4 passes to measure luminance
//
// -------------------------------------------------------------
FLOAT4 PSReSampleAverageLuminanceExp(rageVertexOutputPassThroughTexOnly IN): COLOR                
{
    FLOAT fSum = 0.0f;
    
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.5f , -1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.5f , -0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.5f ,  0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(1.5f ,  1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.5f , -1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.5f , -0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.5f ,  0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(0.5f ,  1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f ,-1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f ,-0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f , 0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-0.5f ,  1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f , -1.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f , -0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f ,  0.5f) ).x;
	fSum += _Tex2DOffset( ToneMapSampler, IN.texCoord0.xy, FLOAT2(-1.5f ,  1.5f) ).x;

    // Divide the sum to complete the average
    return fSum = exp(fSum / 16.0f);
    
//     return Exposure;
}

float2 tap16[16] = {float2(-1.5f , -1.5f),
	                float2(-0.5f , -1.5f),
                    float2( 0.5f , -1.5f),
                    float2( 1.5f , -1.5f),
					float2(-1.5f , -0.5f),
					float2(-0.5f , -0.5f),
					float2( 0.5f , -0.5f),
					float2( 1.5f , -0.5f),
					float2(-1.5f , 0.5f),
					float2(-0.5f , 0.5f),
					float2( 0.5f , 0.5f),
					float2( 1.5f , 0.5f),
					float2(-1.5f , 1.5f),
					float2(-0.5f , 1.5f),
					float2( 0.5f , 1.5f),
					float2( 1.5f , 1.5f)};

// -------------------------------------------------------------
// Mimics adaption process of human visual system
// This simulates the light adaptation that occurs when moving from a 
//
// The AdaptedLuminanceMapSampler holds the texture of the previous pass 
// and the ToneMapSampler the texture of the current pass.
// -------------------------------------------------------------
FLOAT4 PSCalculateAdaptedLuminance(rageVertexOutputPassThroughTexOnly IN): COLOR                
{
#if __PS3
	float AdaptedLum = IN.texCoord0.z;
#else
	// end result of this function from the previous frame
	float AdaptedLum = tex2D( AdaptedLuminanceMapSampler, FLOAT2(0.0f, 0.0f)).x;
#endif

	int zGreater = 0;	
	float Lum = 0;
	
	// check the number of pixels the user chose in the 4x4 render target
	// if they are all bigger than the adapted luminance value or if they are all smaller than the
	// adapted luminance value -> kick off the light adaptation otherwise just keep the adapted luminance
	// value around
#if __XENON	
	for(int i = 0; i < Frames; i++)
	{
		Lum = tex2D( ToneMapSampler, float2(0.5f, 0.5f) + TexelSize.xy * tap16[i] ).x;
		zGreater += (AdaptedLum < Lum);        
	}

	if(zGreater == Frames || zGreater == 0)
	{
#endif	
		// m_pToneMap[0]
//		float CurrentLum = tex2D( ToneMapSampler, TexCoord).x;
		float CurrentLum = tex2D( ToneMapSampler, float2(0.0f, 0.0f)).x;

		// Stores the result in the 1x1 texture to be used in the next pass. 
		// To decelerate the adaption process, the term with the ragePow() intrinsic
		// is used. 
		//return AdaptedLum + (CurrentLum - AdaptedLum) * ( 1 - ragePow( 0.98f, Lambda * ElapsedTime) ); 

		// framerate independent light adaption
		// Exponential decay is always in the form Ae^-Lambda*t.
		// Lambda is related to FLOAT-life using
		// T1/2 = ln(2) / Lambda 
		// Lambda  = ln(2) / T1/2
		// I simplify the whole thing by choosing Lambda from a slider
		// let t be elapsed time
		return AdaptedLum + (CurrentLum - AdaptedLum) * (1 - exp(-Lambda * ElapsedTime));  
#if __XENON	
	      
	}
	
	// if not every value in the 4x4 render target is smaller or bigger than the currently adapted luminance value
	// so there are values that go in different directions
	// -> do not adapt and keep whatever is in AdaptedLum
	else
		return AdaptedLum;			
#endif		
}

// -------------------------------------------------------------
// tone mapping function
// used by several of the pixel shaders below
// -------------------------------------------------------------
FLOAT3 ToneMapping1(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN, 
		#else
			rageVertexOutputPassThroughTexOnly IN,
		#endif
		FLOAT3 FullScreenImage, FLOAT3 AdaptedLum)
{
	// RGB -> XYZ conversion
	// http://www.w3.org/Graphics/Color/sRGB
	// The official sRGB to XYZ conversion matrix is (following ITU-R BT.709)
	// 0.4125 0.3576 0.1805
	// 0.2126 0.7152 0.0722
	// 0.0193 0.1192 0.9505	
	const FLOAT3x3 RGB2XYZ = {0.5141364, 0.3238786,  0.16036376,
							 0.265068,  0.67023428, 0.06409157,
							 0.0241188, 0.1228178,  0.84442666};				            
	// Make sure none of XYZ can be zero
	// This prevents temp from ever being zero, or Yxy from ever being zero        
	float3 XYZ = mul(RGB2XYZ, FullScreenImage.rgb) + float3(0.000001f, 0.000001f, 0.000001f);	
	
	// XYZ -> Yxy conversion
	float3 Yxy;
	Yxy.r = XYZ.g;									// copy luminance Y

	float temp = dot(FLOAT3(1.0,1.0,1.0), XYZ.rgb); 
	//Yxy.g = XYZ.r / temp;  							// x = X / (X + Y + Z)
	//Yxy.b  = XYZ.g / temp; 							// y = X / (X + Y + Z)	
	Yxy.gb = XYZ.rg / temp;
	
    // (Lp) Map average luminance to the middlegrey zone by scaling pixel luminance
    // raise the value range a bit ...
	float Lp = Yxy.r * MiddleGray / (AdaptedLum.x + 0.001f);    

	// (Ld) Scale all luminance within a displayable range of 0 to 1
	Yxy.r = (Lp  * (1.0f + Lp/White))/(1.0f + Lp);	

	// Yxy -> XYZ conversion
	XYZ.r = Yxy.r * Yxy.g / Yxy.b;               // X = Y * x / y
	XYZ.g = Yxy.r;                                // copy luminance Y
	XYZ.b = Yxy.r * (1 - Yxy.g - Yxy.b) / Yxy.b;  // Z = Y * (1-x-y) / y

	// XYZ -> RGB conversion
	// The official XYZ to sRGB conversion matrix is (following ITU-R BT.709)
	//  3.2410 -1.5374 -0.4986
	// -0.9692  1.8760  0.0416
	//  0.0556 -0.2040  1.0570		
	const float3x3 XYZ2RGB  = { 2.5651,-1.1665,-0.3986,
							  -1.0217, 1.9777, 0.0439, 
							   0.0753, -0.2543, 1.1892};
#if 1
	// Night Tone mapper: for scotopic images
	// reduces brightness and contrast, desaturates the image, adds a blue shift and reduces visual acuity 
	// 
	// Pseudo: Y * (1.33 * (1+((Y+Z)/X)-1.68)
	float V = XYZ.g *(1.33*(1.0+((XYZ.g+XYZ.b) / XYZ.r)-1.68));
	float3 NightColor = V * BlueShiftColor.rgb;
	return FullScreenImage.rgb = lerp(NightColor, mul(XYZ2RGB, XYZ), saturate(Yxy.r * BlueShiftParams.r + (1.0 - AdaptedLum.x) * BlueShiftParams.b + BlueShiftParams.g));
#else    
   	return FullScreenImage.rgb = mul(XYZ2RGB, XYZ);	
#endif
}


// -------------------------------------------------------------
// Bright pass filter
// The bright-pass filter removes everything from the scene except lights and
// bright reflections
// -------------------------------------------------------------
FLOAT4 PSBrightPass(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
): COLOR                
{    
    // 1/4 image 
    FLOAT4 Sample = _Tex2DOffset( RenderMapSampler, IN.texCoord0.xy, FLOAT2(0.0f, 0.0f) );
    
    // Current adaption level of luminance (1x1 texture)
#if __XENON    
    float AdaptedLum = IN.texCoord1.r;
#else
	float AdaptedLum = _Tex2DOffset( AdaptedLuminanceMapSampler, FLOAT2(0.0f, 0.0f), FLOAT2(0.0f, 0.0f)).x;    
#endif	
    
    // clamps light adaption
    AdaptedLum = clamp(AdaptedLum, LowerLimitAdaption, HigherLimitAdaption);
    
	// RGB -> XYZ conversion
	const FLOAT3x3 RGB2XYZ = {0.5141364, 0.3238786,  0.16036376,
							 0.265068,  0.67023428, 0.06409157,
							 0.0241188, 0.1228178,  0.84442666};				                    
	float3 XYZ = mul(RGB2XYZ, Sample.rgb);	
	
	// XYZ -> Yxy conversion
	float3 Yxy;
	Yxy.r = XYZ.g;                            // copy luminance Y
	
	// prevent division by zero to keep the compiler happy ... strange
	float temp = dot(FLOAT3(1.0,1.0,1.0), XYZ.rgb) + 0.000001f; 
	//Yxy.g = XYZ.r / temp; // x = X / (X + Y + Z)
	//Yxy.b  = XYZ.g / temp; // y = X / (X + Y + Z)	
	Yxy.gb = XYZ.rg / temp;
	
    // (Lp) Map average luminance to the middlegrey zone by scaling pixel luminance
    // raise the value range a bit ...
	float Lp = Yxy.r * MiddleGray / (AdaptedLum + 0.001f);    

	// (Ld) Scale all luminance within a displayable range of 0 to 1
	// new bright pass white value
	Yxy.r = (Lp  * (1.0f + Lp/(BrightPassWhite))) ;	

	Yxy.r -= BrightPassThreshold;
	
	Yxy.r = max(Yxy.r, 0.0f);
    Yxy.r /= (BrightPassOffset + Yxy.r);

	// Yxy -> XYZ conversion
	XYZ.r = Yxy.r * Yxy.g / Yxy. b;               // X = Y * x / y
	XYZ.g = Yxy.r;                                // copy luminance Y
	XYZ.b = Yxy.r * (1 - Yxy.g - Yxy.b) / Yxy.b;  // Z = Y * (1-x-y) / y

	// XYZ -> RGB conversion
	const float3x3 XYZ2RGB  = { 2.5651,-1.1665,-0.3986,
							  -1.0217, 1.9777, 0.0439, 
							   0.0753, -0.2543, 1.1892};
	Sample.rgb = mul(XYZ2RGB, XYZ);	    
    return saturate(Sample);
}

// -------------------------------------------------------------
// Tone Down Bright pass filter
// The bright-pass filter gets toned down by its stored alpha values.
// -------------------------------------------------------------
FLOAT4 PSToneDownBrightPass(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
): COLOR                
{    
    FLOAT4 Sample = h4tex2D( RenderMapSampler, IN.texCoord0.xy);
	Sample *= tonedownspeed;      
	Sample -= 0.004;      
    return Sample;
}

// -------------------------------------------------------------
// PSBlurry
// Note: this is my DOY filter kernel 
// emulates a 36-tap filter with 9 bilinear filtered fetches
// -------------------------------------------------------------
FLOAT4 PSBlurry(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
    FLOAT4 Color = 0.0f;
    
    // 36-tap filter emulated with 9 texture fetches
    // ... kind of Gauss for poor people like me :-)
    // the weights are used to make the filter look more round
    // think of a 36-tap filter
    // x x x x x x
    // x x x x x x
    // x x x x x x 
    // x x x x x x
    // x x x x x x
    // x x x x x x
    // the sampling points of this filter are lying in-between each quad
    // to give the whole thing a round appearance, "corner" samples are given less importance
    // these are the actual 9-taps we fetch
    //  x   x   x
    //
    //  x   x   x
    // 
    //  x   x   x
    // think of the center, upper middle, left middle, right middle and lower middle samples
    // as a star. These get 0.1f as weight. The corner "samples" get 0.071f as a weight.
    // the center point gets 0.25f 
	Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 2.0f,  2.0f)) * 0.071f;
	Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 2.0f,  0.0f)) * 0.1f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 2.0f, -2.0f)) * 0.071f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f,  2.0f)) * 0.1f;
    Color += h4tex2D( RenderMapBilinearSampler, IN.texCoord0.xy) * 0.25f;
	Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f, -2.0f)) * 0.1f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(-2.0f,  2.0f)) * 0.071f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(-2.0f,  0.0f)) * 0.1f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(-2.0f, -2.0f)) * 0.071f;
	
    return Color;
}


// -------------------------------------------------------------
// PSGaussX
// Note: 360 friendly very good looking Gauss blur
// -------------------------------------------------------------
FLOAT4 PSGaussX(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
    FLOAT4 Color = 0.0f;
    
    // this is a 360.0f friendly Gauss-kind-of filter
    Color  = h4tex2D( RenderMapBilinearSampler, IN.texCoord0.xy) * 0.25f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -4.0f,  0.0f )) * 0.05f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -3.0f,  0.0f )) * 0.08f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -2.0f,  0.0f )) * 0.1f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -1.0f,  0.0f )) * 0.15f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  1.0f,  0.0f )) * 0.15f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  2.0f,  0.0f )) * 0.1f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  3.0f,  0.0f )) * 0.08f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  4.0f,  0.0f )) * 0.05f;

    return Color;
}

// -------------------------------------------------------------
// PSGaussY
// Note: 360 friendly very good looking Gauss blur
// -------------------------------------------------------------
FLOAT4 PSGaussY(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
    FLOAT4 Color = 0.0f;
    
    // this is a 360.0f friendly Gauss-kind-of filter
    Color  = h4tex2D( RenderMapBilinearSampler, IN.texCoord0.xy) * 0.25f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  0.0f, -4.0f )) * 0.05f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  0.0f, -3.0f )) * 0.08f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  0.0f, -2.0f )) * 0.1f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  0.0f, -1.0f )) * 0.15f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  0.0f,  1.0f )) * 0.15f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  0.0f,  2.0f )) * 0.1f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  0.0f,  3.0f )) * 0.08f;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  0.0f,  4.0f )) * 0.05f;

    return Color;
}

// -------------------------------------------------------------
// PSGaussX5
// Note: Like PSGaussX, but less blur and less taps.
// -------------------------------------------------------------
half4 PSGaussX5(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
    half4 Color = (half4)0.0f;
    
    // this is a 360.0f friendly Gauss-kind-of filter
    Color  = (half4)(h4tex2D( RenderMapBilinearSampler, IN.texCoord0.xy) * 0.334f);
    Color += (half4)(_H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, half2( -2.0f,  0.0f )) * 0.133f);
    Color += (half4)(_H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, half2( -1.0f,  0.0f )) * 0.2f);
    Color += (half4)(_H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, half2(  1.0f,  0.0f )) * 0.2f);
    Color += (half4)(_H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, half2(  2.0f,  0.0f )) * 0.133f);

    return Color;
}


// -------------------------------------------------------------
// PSGaussY5
// Note: Like PSGaussY, but less blur and less taps.
// -------------------------------------------------------------
half4 PSGaussY5(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
    half4 Color = (half4)0.0f;
    
    // this is a 360.0f friendly Gauss-kind-of filter
    Color  = (half4)(h4tex2D( RenderMapBilinearSampler, IN.texCoord0.xy) * 0.334f);
    Color += (half4)(_H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, half2( 0.0f,  -2.0f )) * 0.133f);
    Color += (half4)(_H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, half2( 0.0f,  -1.0f )) * 0.2f);
    Color += (half4)(_H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, half2( 0.0f,  1.0f )) * 0.2f);
    Color += (half4)(_H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, half2( 0.0f,  2.0f )) * 0.133f);

    return Color;
}


// -------------------------------------------------------------
// PSGaussX17
// -------------------------------------------------------------
half4 PSGaussX17(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
    FLOAT4 Color = 0.0f;

    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -7.5f,  0.0f )) * 0.00457891;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -7.0f,  0.0f )) * 0.007549346;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -6.0f,  0.0f )) * 0.012446767;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -5.0f,  0.0f )) * 0.02052125;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -4.0f,  0.0f )) * 0.033833821;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -3.0f,  0.0f )) * 0.05578254;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -2.0f,  0.0f )) * 0.09196986;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( -1.0f,  0.0f )) * 0.151632665;
    Color += h4tex2D( RenderMapBilinearSampler, IN.texCoord0.xy) * 0.25;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  1.0f,  0.0f )) * 0.151632665;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  2.0f,  0.0f )) * 0.09196986;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  3.0f,  0.0f )) * 0.05578254;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  4.0f,  0.0f )) * 0.033833821;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  5.0f,  0.0f )) * 0.02052125;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  6.0f,  0.0f )) * 0.012446767;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  7.0f,  0.0f )) * 0.007549346;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(  7.5f,  0.0f )) * 0.00457891;

    return (half4)Color;
}


// -------------------------------------------------------------
// PSGaussY17
// -------------------------------------------------------------
half4 PSGaussY17(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
    FLOAT4 Color = 0.0f;

    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f, -7.5f)) * 0.00457891;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f, -7.0f)) * 0.007549346;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f, -6.0f)) * 0.012446767;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f, -5.0f)) * 0.02052125;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f, -4.0f)) * 0.033833821;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f, -3.0f)) * 0.05578254;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f, -2.0f)) * 0.09196986;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f, -1.0f)) * 0.151632665;
    Color += h4tex2D( RenderMapBilinearSampler, IN.texCoord0.xy) * 0.25;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f,  1.0f)) * 0.151632665;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f,  2.0f)) * 0.09196986;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f,  3.0f)) * 0.05578254;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f,  4.0f)) * 0.033833821;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f,  5.0f)) * 0.02052125;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f,  6.0f)) * 0.012446767;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f,  7.0f)) * 0.007549346;
    Color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2( 0.0f,  7.5f)) * 0.00457891;

    return (half4)Color;
}


// -------------------------------------------------------------
// Gauss horizontal filter
// -------------------------------------------------------------
FLOAT4 PSGaussX_25(rageVertexOutputPassThroughTexOnly IN) : COLOR
{
	FLOAT4 ColorSum = 0.0f;	
	FLOAT2 Tex = IN.texCoord0.xy;

	// sample inner taps
	ColorSum = h4tex2D(RenderMapSampler, Tex) * TexelWeight[0];
	ColorSum += h4tex2D(RenderMapSampler, Tex + horzTapOffs[1]) * TexelWeight[1];	
	ColorSum += h4tex2D(RenderMapSampler, Tex - horzTapOffs[1]) * TexelWeight[1];
	ColorSum += h4tex2D(RenderMapSampler, Tex + horzTapOffs[2]) * TexelWeight[2];
	ColorSum += h4tex2D(RenderMapSampler, Tex - horzTapOffs[2]) * TexelWeight[2];			
	ColorSum += h4tex2D(RenderMapSampler, Tex + horzTapOffs[3]) * TexelWeight[3];	
	ColorSum += h4tex2D(RenderMapSampler, Tex - horzTapOffs[3]) * TexelWeight[3];	
	
	// sample outer taps
	ColorSum += h4tex2D(RenderMapSampler, Tex + horzTapOffs[4]) * TexelWeight[4];
	ColorSum += h4tex2D(RenderMapSampler, Tex - horzTapOffs[4]) * TexelWeight[4];
	ColorSum += h4tex2D(RenderMapSampler, Tex + horzTapOffs[5]) * TexelWeight[5];
	ColorSum += h4tex2D(RenderMapSampler, Tex - horzTapOffs[5]) * TexelWeight[5];
	ColorSum += h4tex2D(RenderMapSampler, Tex + horzTapOffs[6]) * TexelWeight[6];
	ColorSum += h4tex2D(RenderMapSampler, Tex - horzTapOffs[6]) * TexelWeight[6];	

	return ColorSum;				
}


// -------------------------------------------------------------
// Gauss vertical filter
// -------------------------------------------------------------
FLOAT4 PSGaussY_25(rageVertexOutputPassThroughTexOnly IN) : COLOR
{
	FLOAT4 ColorSum = 0.0f;
	FLOAT2 Tex = IN.texCoord0.xy;
	
	ColorSum = h4tex2D(RenderMapSampler, Tex) * TexelWeight[0];
	ColorSum += h4tex2D(RenderMapSampler, Tex + vertTapOffs[1]) * TexelWeight[1];	
	ColorSum += h4tex2D(RenderMapSampler, Tex - vertTapOffs[1]) * TexelWeight[1];
	ColorSum += h4tex2D(RenderMapSampler, Tex + vertTapOffs[2]) * TexelWeight[2];
	ColorSum += h4tex2D(RenderMapSampler, Tex - vertTapOffs[2]) * TexelWeight[2];			
	ColorSum += h4tex2D(RenderMapSampler, Tex + vertTapOffs[3]) * TexelWeight[3];	
	ColorSum += h4tex2D(RenderMapSampler, Tex - vertTapOffs[3]) * TexelWeight[3];	

	// sample outer taps
	ColorSum += h4tex2D(RenderMapSampler, Tex + vertTapOffs[4]) * TexelWeight[4];
	ColorSum += h4tex2D(RenderMapSampler, Tex - vertTapOffs[4]) * TexelWeight[4];
	ColorSum += h4tex2D(RenderMapSampler, Tex + vertTapOffs[5]) * TexelWeight[5];
	ColorSum += h4tex2D(RenderMapSampler, Tex - vertTapOffs[5]) * TexelWeight[5];
	ColorSum += h4tex2D(RenderMapSampler, Tex + vertTapOffs[6]) * TexelWeight[6];
	ColorSum += h4tex2D(RenderMapSampler, Tex - vertTapOffs[6]) * TexelWeight[6];	
	
	return ColorSum;				
}

// -------------------------------------------------------------
// Kawase HDR Bloom Filter
// -------------------------------------------------------------
FLOAT4 KawaseBloomFilter1(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
	FLOAT2 Tex = IN.texCoord0.xy;

	FLOAT2 halfTexelSize = TexelSize.xy / 2.0f;
	FLOAT2 dUV = (halfTexelSize .xy * fIteration);// + halfTexelSize.xy;
	// sample top left
	FLOAT2 TopLeft = FLOAT2(Tex.x - dUV.x, Tex.y - dUV.y); 
	
	// sample top right
	FLOAT2 TopRight = FLOAT2(Tex.x + dUV.x, Tex.y - dUV.y);
	
	// sample bottom right
	FLOAT2 BottomRight = FLOAT2(Tex.x + dUV.x, Tex.y + dUV.y);
	
	// sample bottom left
	FLOAT2 BottomLeft = FLOAT2(Tex.x - dUV.x, Tex.y + dUV.y);

	FLOAT4 addedBuffer = 0.0f;
	
	// sample top left
	addedBuffer = h4tex2D(RenderMapBilinearSampler, TopLeft);
	
	// sample top right
	addedBuffer += h4tex2D(RenderMapBilinearSampler, TopRight);
	
	// sample bottom right
	addedBuffer += h4tex2D(RenderMapBilinearSampler, BottomRight);
	
	// sample bottom left
	addedBuffer += h4tex2D(RenderMapBilinearSampler, BottomLeft);
	
	// average
	return addedBuffer * bloomIntensity;
}

// -------------------------------------------------------------
// Kawase light streak filter
// Streaks light in direction passed in
// -------------------------------------------------------------
FLOAT4 KawaseStreakFilter1(	rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
	// Streak direction is a 2D vector in image space
	FLOAT2 texCoordSample= IN.texCoord0.xy;
	
	// Scale and accumulate
	FLOAT4 cOut= saturate(streakWeights.x) * h4tex2D(RenderMapSampler, texCoordSample);
	
	// Streak direction is a 2D vector in image space
	texCoordSample= (LightStreakDirection.rg * StreakParams.w * TexelSize.xy );	
	// Scale and accumulate
	cOut+= saturate(streakWeights.y) * h4tex2D(RenderMapSampler, IN.texCoord0.xy +texCoordSample);	
	// Scale and accumulate
	cOut+= saturate(streakWeights.y) * h4tex2D(RenderMapSampler, IN.texCoord0.xy -texCoordSample);
	// Streak direction is a 2D vector in image space
	
	texCoordSample += texCoordSample ;	
	// Scale and accumulate
	cOut+= saturate(streakWeights.z) * h4tex2D(RenderMapSampler, IN.texCoord0.xy +texCoordSample);	
	// Scale and accumulate
	cOut+= saturate(streakWeights.z) * h4tex2D(RenderMapSampler, IN.texCoord0.xy -texCoordSample);
	// Streak direction is a 2D vector in image space
	
	texCoordSample += texCoordSample ;	
	// Scale and accumulate
	cOut+= saturate(streakWeights.w) * h4tex2D(RenderMapSampler, IN.texCoord0.xy +texCoordSample);	
	// Scale and accumulate
	cOut+= saturate(streakWeights.w) * h4tex2D(RenderMapSampler, IN.texCoord0.xy -texCoordSample);
	
	return saturate(cOut);
}

// -------------------------------------------------------------
// correct color and desaturate image
//
// used by several of the pixel shaders below
// -------------------------------------------------------------
FLOAT4 ColorFilters(FLOAT3 Color)
{
    // color correction
    Color = saturate(Color * ColorCorrect.xyz * 2.0f  + ConstAdd.xyz);
    
    // monochrome
    FLOAT dotColor = dot(Color,LUMINANCE);
    
    // saturation
    FLOAT3 ret = lerp(dotColor.xxx, Color, deSat);
	
	// contrast
	//FLOAT3 Cont = (ret - 0.5f) * Contrast + 0.5; 
	FLOAT3 Cont = saturate(ret - Contrast * (ret - 1.0f) * ret *(ret - 0.5f));

	//
	// this should be the official way to do it :-)
	// http://www.w3.org/Graphics/Color/sRGB.html
	//
	// the right Gamma value to convert from gamma 1.0 to 2.2 is 1.0 / 2.4 = 0.41666666666666666666666666666667
	// return FLOAT4((Cont <= 0.00304) ? Cont * 12.92 : (1.055f * ragePow(Cont, Gamma) - 0.055f), 1.0f); 
	return FLOAT4((Cont <= 0.00304) ? Cont * 12.92 : (1.055f * ragePow(Cont, Gamma) - 0.055f), 1.0f);
}    

// -------------------------------------------------------------
// PSColorFilters
// - sets color filters
// -------------------------------------------------------------
FLOAT4 PSColorFilters( rageVertexOutputPassThroughTexOnly IN): COLOR
{
	//return ColorFilters(rageDecodeOpaqueColor(tex2D(RenderMapSampler, IN.texCoord0.xy)));
	// Point sampled
	return ColorFilters(rageDecodeOpaqueColor(tex2D(FullResMapSampler, IN.texCoord0.xy)));
	//float4 rColor = tex2D(FullResMapSampler, IN.texCoord0.xy);
	//return rColor;
}

//
// -------------------------------------------------------------
// tone mapping stage
// -------------------------------------------------------------
//
FLOAT4 PSToneMap(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
#if __XENON    
    float AdaptedLum = IN.texCoord1.r;
#else
#if __PS3
	float AdaptedLum = IN.texCoord0.z;
#else
	float AdaptedLum = tex2D( AdaptedLuminanceMapSampler, FLOAT2(0.0f, 0.0f)).x;
#endif
#endif	

#if __WIN32PC
    // clamps light adaption
    AdaptedLum = clamp(AdaptedLum, LowerLimitAdaption, HigherLimitAdaption);
#endif // __WIN32PC

	FLOAT3 FullScreenImage = rageDecodeOpaqueColor(h4tex2D(FullResMapSampler, IN.texCoord0.xy));
	FLOAT3 BlurredImage = h3tex2D(RenderMapAnisoSampler, IN.texCoord0.xy);
#if RAGE_ENCODEOPAQUECOLOR
	return ColorFilters(FullScreenImage + BlurredImage.rgb);
#else
	return ColorFilters(ToneMapping1(IN, FullScreenImage, AdaptedLum) + BlurredImage.rgb);
#endif
}

//
// special vertex shader for the sepia / old TV effect
//
// Even simpler passthrough vertex shader - this one assumes positions are pretransformed
//	Good for screen space blits.
rageVertexOutputPassThrough VS_ragePassThroughNoXformSepia(rageVertexInput IN)
{
	rageVertexOutputPassThrough OUT;
	OUT.pos = FLOAT4(IN.pos, 1.0f);
    OUT.diffuse = IN.diffuse;
    OUT.texCoord0 = IN.texCoord0;
    OUT.normal = FLOAT3(0.0f.xx, -1.0f);	// Force normal to face camera
    OUT.texCoord1 = sign(IN.pos.xy);

    return OUT;
}

/*
// -------------------------------------------------------------
// Sepia filter
// To performa a sepia conversion, we need to transform our RGB
// sample from the input image into YIQ space, process it, 
// and then transform it back to RGB space
// This shader covers an optimized way to do this
// -------------------------------------------------------------
FLOAT4 PSSepia(rageVertexOutputPassThrough IN) : COLOR0
{
    FLOAT Y;
    FLOAT4 c, OrigColor;
    FLOAT3 IntensityConverter={0.299, 0.587, 0.114};
    FLOAT3 SepiaConvert={0.191, -0.054, -0.221};

    FLOAT4 FullScreenImage = h4tex2D(FullResMapSampler, IN.texCoord0);
    
    // get intensity value (Y part of YIQ)
    Y = dot(IntensityConverter, FullScreenImage.rgb);

	// 
	// film grain
	//	        
	// Define a frame shape
	FLOAT f = (1 - IN.texCoord1.x * IN.texCoord1.x) * (1 - IN.texCoord1.y * IN.texCoord1.y);
	FLOAT frame = saturate(frameSharpness * (ragePow(f, frameShape) - frameLimit));

	// noise ... just a texture filled witf rand()
	FLOAT rand = tex3D(NoiseSampler, FLOAT3(1.5f * IN.texCoord1, ElapsedTime)).x - 0.2;
	
	// Combine frame, sepia image and interference
	return ColorFilters(frame * (interference * rand + (Y + SepiaConvert)));    
}
*/


float4 PSRDR2Death(rageVertexOutputPassThrough IN) : COLOR0
{
	float4 result;
    float3 redTint = {0.7f, 0.0f, 0.0f};
    float3 IntensityConverter={0.299, 0.587, 0.114};
	float2 screenCoord = float2(IN.texCoord0.x, IN.texCoord0.y);

	// to do: give splatters their own UV's
	float2 newUV = IN.texCoord0;
	float3 splat1 = tex2D(DeathSplatSampler1, newUV).xyz;
	float pull1 = saturate(splat1.g - (1.0f - DeathDrip));
	pull1 = saturate(pull1 * 500.0f) * (1.0f - splat1.g);
	newUV.y -= (pull1 * splat1.b);
	splat1 = tex2D(DeathSplatSampler1, newUV).xyz;
//	pull1 = (pull1 != 0.0f) * (1.0f - pull1);
//	float pull1 = saturate((splat1.w != 0.0f) * (1.0f - splat1.w) - (1.0f - DeathDrip));

// TINT

	// Zoom the red tint, to make apparent peripheral motion
	redTint = DeathColor.xyz;
	float scaleFactor = 1.0f + (DeathMag) * 0.2f;
	screenCoord = (screenCoord - 0.5f) * scaleFactor + 0.5f;
	float4 bloodColor = tex2D(DeathSampler, screenCoord);
    float tintOpacity = saturate(bloodColor.w - 1.0f + DeathMag);

// SPLATTER

    float dropStrength = splat1.x * DeathBlood;


	// Warp the framebuffer
    float4 FullScreenImage;
	float warp = sin(dropStrength * 3.1415) * 0.05f;
	newUV = IN.texCoord0;
	newUV.x += warp * DeathBlood;
	newUV.y += warp * DeathBlood;
	FullScreenImage = tex2D(FullResMapSampler, newUV);


	// As the player approaches death, tint the screen red
	FullScreenImage = saturate(FullScreenImage);
    float Y = dot(IntensityConverter, FullScreenImage.rgb);
	float3 deathTint = redTint * 0.5f + redTint * Y * 0.5f;

	// Blend between the screen and the red tint
	FullScreenImage.xyz = ((FullScreenImage.xyz * (1.0f - tintOpacity)) + (deathTint * tintOpacity));


// FINAL BLEND

	// Blend between the screen and the dripping blood
	result.xyz = ((FullScreenImage.xyz * (1.0f - dropStrength)) + (bloodColor.xyz * dropStrength));
	result.w = 1.0f;

	return result;
}

//
// Police Camera Effect
// - a median filter with an offset of 2 instead of 1 is used to average color values
// - a "kind-of-interlace" effect is used to mimic interlacing artefacts
//
FLOAT3 FindMedian(FLOAT3 RGB, FLOAT3 RGB2, FLOAT3 RGB3)
{
	// vectorised median filter ... this would be worth an article :-)
   return (RGB < RGB2)? ((RGB2 < RGB3) ? RGB2 : max(RGB,RGB3)) : ((RGB < RGB3)? RGB : max(RGB2, RGB3));
} 

FLOAT3 PoliceCam(sampler2D TextureSampler, FLOAT4 Texcoord, FLOAT2 TexCoord1) : COLOR0
{
	// noise ... just a texture filled with rand()
	// this texture is only a L8 with 64x64x64 
	FLOAT rand = tex3D(NoiseSampler, FLOAT3(1.5f * TexCoord1.x + ElapsedTime*1.234,1.5f * TexCoord1.y + ElapsedTime* 1.678, ElapsedTime)).x - 0.2;

    FLOAT3 MedianColor[2];
    
    // a "real" median filter has 9 taps oriented very uniformly like this
    // x x x
    // x x x
    // x x x
    // this filter has only 6 taps oriented like this
    // x 0 0 0 x
    // 0 x 0 x 0
    // x 0 0 0 x
    MedianColor[0] = FindMedian(_H4Tex2DOffset( TextureSampler, Texcoord.xy + interference * rand, FLOAT2(-2.0f,	-2.0f)).rgb, 
								_H4Tex2DOffset( TextureSampler, Texcoord.xy + interference * rand, FLOAT2(-1.0f,	 0.0f)).rgb, 
								_H4Tex2DOffset( TextureSampler, Texcoord.xy + interference * rand, FLOAT2(-2.0f,	 2.0f)).rgb);  
    MedianColor[1] = FindMedian(_H4Tex2DOffset( TextureSampler, Texcoord.xy + interference * rand, FLOAT2(2.0f, -2.0f)).rgb, 
								_H4Tex2DOffset( TextureSampler, Texcoord.xy + interference * rand, FLOAT2(1.0f,  0.0f)).rgb, 
								_H4Tex2DOffset( TextureSampler, Texcoord.xy + interference * rand, FLOAT2(2.0f,  2.0f)).rgb);  
								
    FLOAT3 Color;  
    Color = (MedianColor[0] + MedianColor[1]) / 2.0f;  

    // get every second line
    // Texcoord.w holds the unnormalized screen coordinates in y direction e.g. 0..719
    FLOAT fraction = frac(Texcoord.w * 0.5f); // fraction is 0.0f or 1 for eacf line
    
    // mimic interlaced rendering
    if(fraction)
       return (intensityGrain * rand + Color);
    else
	   // every second line is without film grain and a bit brighter
       return (Color * intensityInterlace);
}

//
// GetBlurFactor()
//
FLOAT GetBlurFactor(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN, 
		#else
			rageVertexOutputPassThroughTexOnly IN,
		#endif
		sampler2D DepthMap		
)
{
	FLOAT CamDepth = 0;

	float currDepth = rageTexDepth2D(DepthMap, IN.texCoord0.xy).r;
#if __XENON
	// Q = Zf / Zf - Zn
	// z = -Zn * Q / Zd - Q
	CamDepth = -(NearFarClipPlaneQ[0] * NearFarClipPlaneQ[2]) / ((1 - currDepth) - NearFarClipPlaneQ[2]);
#elif __PS3
	// Q = Zf / Zf - Zn
	// z = -Zn * Q / Zd - Q
	CamDepth = -(NearFarClipPlaneQ[0] * NearFarClipPlaneQ[2]) / (currDepth - NearFarClipPlaneQ[2]);
#endif	

	// DofParams.x - near blur plane
	// DofParams.y - far blur plane
	//FLOAT BlurFactor = saturate(abs(CamDepth - DofParams.x)/(DofParams.y));
	
	// Range * abs(Focus - CamDepth)
	FLOAT BlurFactor = saturate(DofParams.x * abs(DofParams.y - CamDepth));
	BlurFactor = smoothstep(DofParams.w, 1.0f, CamDepth > DofParams.z ? 0.0f : BlurFactor);

	return BlurFactor;
}


// -------------------------------------------------------------
// PSCopyBlurFactor
//
// preview to visualize the blur factor for depth of field
// -------------------------------------------------------------
FLOAT4 PSCopyBlurFactor(		
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
): COLOR
{
	// we need to convert the depth buffer values to use them
	return GetBlurFactor(IN, DepthMapSampler);
}

// -------------------------------------------------------------
// Combined tone mapping and DOF effect
// this is the cheap one ...
// -------------------------------------------------------------
FLOAT3 DOFToneMapped(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN 
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
)
{
    FLOAT3 FullScreenImage = h3tex2D(FullResMapSampler, IN.texCoord0.xy);
    
    // blurred image for HDR
    FLOAT3 BlurredImage = h3tex2D(RenderMapAnisoSampler, IN.texCoord0.xy);
    BlurredImage *= IntensityBloom;
    
    // Depth of Field image
    FLOAT3 DOFImage = h3tex2D(RenderMapBilinearSampler, IN.texCoord0.xy);
	
	// blur feactor for depth of field effect
	FLOAT BlurFactor = GetBlurFactor(IN, DepthMapSampler);
	
	// gets adaption luminance value from the vertex shader
	// to save some performance we just fetcf the 1x1 texture in the vertex shader 
	// and send down the values to the pixel shader
#if __XENON    
    float AdaptedLum = IN.texCoord1.r;
#else
#if __PS3
	float AdaptedLum = IN.texCoord0.z;
#else
	float AdaptedLum = tex2D( AdaptedLuminanceMapSampler, FLOAT2(0.0f, 0.0f)).x;
#endif
#endif	

	//
	// tone mapping
	//   
#if __WIN32PC
    // clamps light adaption
    AdaptedLum = clamp(AdaptedLum, LowerLimitAdaption, HigherLimitAdaption);
#endif // __WIN32PC
    
	return ToneMapping1(IN, lerp(FullScreenImage, DOFImage, BlurFactor), AdaptedLum) + BlurredImage;
}


// -------------------------------------------------------------
// Height based fog. 
// -------------------------------------------------------------

FLOAT3 FogHeightBased(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN, 
		#else
			rageVertexOutputPassThroughTexOnly IN,
		#endif
			FLOAT3 color,
			float currDepth)
{
// lame workaround ... it is just supported on XENON only
#if __XENON	
#if PERSISTENT_BRIGHTPASS
    color += _H4Tex2DOffset( RenderMapBilinearSampler, IN.texCoord0.xy, FLOAT2(0.0f, 0.0f) ).rgb;
#endif    

// these defines are to make the code below more readable.
#define fogtop FogParams.x
#define fogbottom FogParams.y
#define fognearz FogParams.z
#define fogfarz FogParams.w
#define CamPosY gViewInverse[3].y
#define CamPos gViewInverse[3].xyz
	
	FLOAT3 screenPos = FLOAT3(IN.texCoord0.zw, 1.0f - currDepth);
	FLOAT4 worldPos4 = mul(FLOAT4(screenPos, 1.0f), WorldViewProjInverse);
	worldPos4.xyz /= worldPos4.w;
	FLOAT3 ray = worldPos4.xyz - gViewInverse[3].xyz;
	FLOAT lenray = length(ray);
	FLOAT infog = 1.0;	
	FLOAT abovefog = 0.0;
	if(CamPosY > fogtop)
	{
		if(worldPos4.y >= fogtop)
		{
			infog = 0.0;
			abovefog = 1.0f;
		}
		else
		{
			infog = saturate((fogtop - worldPos4.y) / (CamPosY - worldPos4.y));
			abovefog = 1.0 - infog;
		}
	}
	else 
	{
		if(worldPos4.y <= fogtop)
		{
			infog = 1.0;
		}
		else
		{
			infog = saturate((fogtop-CamPosY) / (worldPos4.y - CamPosY));
			abovefog = 1.0 - infog;
		}
	}
			
	FLOAT3 startpos = worldPos4.xyz + ray * abovefog;
	ray = ray * infog;
	FLOAT3 endpos = worldPos4.xyz + ray;
	
	FLOAT startweight = saturate((startpos.y - fogbottom)/(fogtop - fogbottom));
	FLOAT4 blendfogcolstart = lerp(FogMaxColor, FogMinColor, startweight);
	
	FLOAT endweight = saturate((endpos.y - fogbottom)/(fogtop - fogbottom));
	FLOAT4 blendfogcolend = lerp(FogMaxColor, FogMinColor, endweight);
	
	FLOAT4 fogcol = FogMinColor * abovefog + (blendfogcolstart + blendfogcolend) * 0.5 * infog;
	
	FLOAT density = (lenray - fognearz) / (fogfarz-fognearz); //TODO: Implement fog near and fog far plane
	density = min(saturate(fogcol.w * density), fogcol.w);
#define FOGMAP 1
#if !FOGMAP
	fogcol.xyz = lerp(color, fogcol.xyz, density);
	return fogcol;
#else	
	FLOAT fogdepth = saturate(h1tex2D(FogMapSampler, IN.texCoord0.xy)*ParticleFogParams.w);
	FLOAT fogmaplerp = min(fogdepth*ParticleFogParams.z, lenray + ParticleFogParams.y)/ParticleFogParams.z ;
	return lerp(fogcol, FogMaxColor, fogmaplerp);
#endif	
#else
	return 0.0f.xxx;
#endif		
}


FLOAT4 FogHeightBasedUsingAlpha(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif 
			 )
{
// lame workaround ... it is just supported on XENON only
#if __XENON	

// these defines are to make the code below more readable.
#define fogtop FogParams.x
#define fogbottom FogParams.y
#define fognearz FogParams.z
#define fogfarz FogParams.w
#define CamPosY gViewInverse[3].y
#define CamPos gViewInverse[3].xyz
	
#if __XENON
	// fetcf depth buffer
	float currDepth = 1.0f - rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy).r;
#elif __PS3
	float currDepth = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy).r;
#endif	

	FLOAT3 screenPos = FLOAT3(IN.texCoord0.zw,  currDepth);
	FLOAT4 worldPos4 = mul(FLOAT4(screenPos, 1.0f), WorldViewProjInverse);
	worldPos4.xyz /= worldPos4.w;

	FLOAT3 ray = worldPos4.xyz - gViewInverse[3].xyz;
	FLOAT lenray = length(ray);
	FLOAT infog = 1.0;	
	FLOAT abovefog = 0.0;

	if(CamPosY > fogtop)
	{
		if(worldPos4.y >= fogtop)
		{
			infog = 0.0;
			abovefog = 1.0f;
		}
		else
		{
			infog = saturate((fogtop - worldPos4.y) / (CamPosY - worldPos4.y));
			abovefog = 1.0 - infog;
		}
	}
	else 
	{
		if(worldPos4.y <= fogtop)
		{
			infog = 1.0;
		}
		else
		{
			infog = saturate((fogtop-CamPosY) / (worldPos4.y - CamPosY));
			abovefog = 1.0 - infog;
		}
	}
		


	FLOAT3 startpos = worldPos4.xyz + ray * abovefog;
	ray = ray * infog;
	FLOAT3 endpos = worldPos4.xyz + ray;
	
	FLOAT startweight = saturate((startpos.y - fogbottom)/(fogtop - fogbottom));
	FLOAT4 blendfogcolstart = lerp(FogMaxColor, FogMinColor, startweight);
	
	FLOAT endweight = saturate((endpos.y - fogbottom)/(fogtop - fogbottom));
	FLOAT4 blendfogcolend = lerp(FogMaxColor, FogMinColor, endweight);
	
	FLOAT4 fogcol = FogMinColor * abovefog + (blendfogcolstart + blendfogcolend) * 0.5 * infog;
	
	FLOAT density = (lenray - fognearz) / (fogfarz-fognearz); //TODO: Implement fog near and fog far plane
	density = min(saturate(fogcol.w * density), fogcol.w);
	return FLOAT4( fogcol.xyz, density);
	
#else
	return 0.0f.xxxx;
#endif		
}

FLOAT3 FogDepthBased(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN, 
		#else
			rageVertexOutputPassThroughTexOnly IN,
		#endif
			FLOAT3 color,
			float currDepth)
{
// lame workaround ... it is just supported on XENON only
#if __XENON	
// these defines are to make the code below more readable.
#define fogtop FogParams.x
#define fogbottom FogParams.y
#define fogdensity FogParams.z
#define fogend FogParams.w

	FLOAT fogmin = FogMaxColor.w;
	FLOAT colScale = fogbottom;
	FLOAT colOffset = FogMinColor.w;
	
	FLOAT lerpval = 0;
	
	FLOAT3 screenPos = FLOAT3(IN.texCoord0.zw, 1.0f - currDepth);
	FLOAT4 worldPos4 = mul(FLOAT4(screenPos, 1.0f), WorldViewProjInverse);
	worldPos4.xyz /= worldPos4.w;
	FLOAT3 fogvec = gViewInverse[3].xyz - worldPos4.xyz;

	FLOAT dist = dot( fogvec, gViewInverse[2].xyz );

	lerpval = saturate(fogdensity *  ( dist - fogtop )) ;
	lerpval = dist > fogend  ? 0.0f : lerpval;
	lerpval = min( lerpval, fogmin );

	FLOAT clerpVal = saturate( lerpval * colScale + colOffset );
	FogMaxColor.xyz = lerp( FogMinColor.xyz, FogMaxColor.xyz, clerpVal );
	return lerp(color.xyz,FogMaxColor.xyz, lerpval);
#else
	return 1.0f.xxx;
#endif		
}

//
// -------------------------------------------------------------
// tone mapping + depth of field
// -------------------------------------------------------------
//
FLOAT4 PSToneMapDOF(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
    return (ColorFilters(DOFToneMapped(IN)));
}

//
// -------------------------------------------------------------
// tone mapping + depth of field + depth based fog
// tone mapping + depth of field + height based fog
// -------------------------------------------------------------
//

FLOAT4 PSToneMapDOFFog(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
	float currDepth = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy).r;
    return ColorFilters(FogDepthBased(IN,DOFToneMapped(IN),currDepth));
}


//
// -------------------------------------------------------------
// MC guys, use this function!
// -------------------------------------------------------------
//

FLOAT4 PSToneMapDOFHeightFog(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
	float currDepth = h1tex2D(DepthMapSampler, IN.texCoord0.xy);
    return ColorFilters(FogHeightBased(IN,DOFToneMapped(IN),currDepth));
}



// -------------------------------------------------------------
// Separated Hieght Fog using alpha to blend in values
// -------------------------------------------------------------
//

FLOAT4 PSBlendedHeightFog(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
    return FogHeightBasedUsingAlpha(IN);
}



//
// -------------------------------------------------------------
// tone mapping + height based fog
// -------------------------------------------------------------
//

FLOAT4 PSToneMapFog(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
    FLOAT3 FullScreenImage = h3tex2D(FullResMapSampler, IN.texCoord0.xy);
    
    // blurred image for HDR
    FLOAT3 BlurredImage = h3tex2D(RenderMapAnisoSampler, IN.texCoord0.xy);
    
	float currDepth = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy).r;
	
	// gets adaption luminance value from the vertex shader
	// to save some performance we just fetch the 1x1 texture in the vertex shader 
	// and send down the values to the pixel shader
#if __XENON    
    float AdaptedLum = IN.texCoord1.r;
#else
#if __PS3
	float AdaptedLum = IN.texCoord0.z;
#else
	float AdaptedLum = tex2D( AdaptedLuminanceMapSampler, FLOAT2(0.0f, 0.0f)).x;
#endif
#endif	

	//
	// tone mapping
	// 
#if __WIN32PC
    // clamps light adaption
    AdaptedLum = clamp(AdaptedLum, LowerLimitAdaption, HigherLimitAdaption);
#endif // __WIN32PC
    
    // Tone map the full screen image
    return ColorFilters(ToneMapping1(IN, FogHeightBased(IN, FullScreenImage + BlurredImage, currDepth), AdaptedLum) );
}

//
// -------------------------------------------------------------
// tone mapping + height based fog + Police Cam
// -------------------------------------------------------------
//

FLOAT4 PSToneMapFogPoliceCam(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
    // blurred image for HDR
    FLOAT3 BlurredImage = h3tex2D(RenderMapAnisoSampler, IN.texCoord0.xy);
    
    FLOAT3 FullScreenImage = 0;

#if __XENON
	// this is where the police cam effect comes in
    FullScreenImage = PoliceCam(FullResMapSampler, IN.texCoord0, IN.texCoord1.zw);
#endif    
    
	float currDepth = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy).r;
	
	// gets adaption luminance value from the vertex shader
	// to save some performance we just fetch the 1x1 texture in the vertex shader 
	// and send down the values to the pixel shader
#if __XENON    
    float3 AdaptedLum = IN.texCoord1.rgb;
#else
#if __PS3
	float AdaptedLum = IN.texCoord0.z;
#else
	float AdaptedLum = tex2D( AdaptedLuminanceMapSampler, FLOAT2(0.0f, 0.0f)).x;
#endif
#endif	

	//
	// tone mapping
	// 
#if __WIN32PC
    // clamps light adaption
    AdaptedLum = clamp(AdaptedLum, LowerLimitAdaption, HigherLimitAdaption);
#endif // __WIN32PC
    
    // Tone map the full screen image
    return ColorFilters(ToneMapping1(IN, FogHeightBased(IN, FullScreenImage + BlurredImage, currDepth), AdaptedLum) );
}

//
// -------------------------------------------------------------
// straight fog which should be extended to volumetric fog in future.
// -------------------------------------------------------------
//
FLOAT4 PSFog(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
	FLOAT3 col = h4tex2D(FullResMapSampler, IN.texCoord0.xy).xyz; 
	float currDepth = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy).r;

    return ColorFilters( FogDepthBased(IN, col, currDepth) ) ;
}



struct passVertexColor {
    FLOAT4 pos			: DX_POS;
    FLOAT4 diffuse		: COLOR;
#if __PS3
    FLOAT4 texCoord0	: TEXCOORD0;
#else
	FLOAT3 texCoord0	: TEXCOORD0;
#endif
	FLOAT4 screenPos	: TEXCOORD5;
};

FLOAT4	CalcScreenSpacePosition	(FLOAT4 Position)
{
	FLOAT4	ScreenPosition;

	ScreenPosition.x = Position.x * 0.5f + Position.w * 0.5f;
	ScreenPosition.y = Position.w * 0.5f - Position.y * 0.5f;
	ScreenPosition.z = Position.w;
	ScreenPosition.w = Position.w;

	return(ScreenPosition);
}

passVertexColor VS_PassColorThrough(rageVertexInput IN)
{
 	passVertexColor OUT;
	OUT.pos = FLOAT4(IN.pos, 1.0f);
    OUT.diffuse = IN.diffuse;
    OUT.texCoord0.xy = IN.texCoord0;
    OUT.texCoord0.z = IN.pos.z;
#if __PS3
    OUT.texCoord0.w = clamp(AdaptedLuminance, LowerLimitAdaption, HigherLimitAdaption);
#endif // __PS3
	OUT.screenPos = CalcScreenSpacePosition(OUT.pos);

    return OUT;
}


passVertexColor VS_SmoothClipColor(rageVertexInput IN)
{
 	passVertexColor OUT;
	OUT.pos = FLOAT4(IN.pos, 1.0f);
	FLOAT opacity = saturate(10.0f - IN.pos.x * 10.0f);
	opacity *= saturate(10.0f - IN.pos.y * 10.0f);
	opacity *= saturate(10.0f + IN.pos.x * 10.0f);
	opacity *= saturate(10.0f + IN.pos.y * 10.0f);
    OUT.diffuse = IN.diffuse * opacity;
    OUT.texCoord0.xy = IN.texCoord0;
    OUT.texCoord0.z = IN.pos.z;
#if __PS3
    OUT.texCoord0.w = 0;
#endif // __PS3
	OUT.screenPos = CalcScreenSpacePosition(OUT.pos);

    return OUT;
}


FLOAT4 PSFogCard( passVertexColor IN): COLOR
{
		FLOAT x = IN.texCoord0.x * 2.0f - 1.0f;
		FLOAT y = IN.texCoord0.y * 2.0f - 1.0f;
		FLOAT len = x*x + y*y;
		len = 1.0-saturate(sqrt(len));
		return h4tex2D(FogTextureSampler, IN.texCoord0.xy + ParticleFogParams.xy) * len * SeedIntensity;
}


// -------------------------------------------------------------
// PSSeedRT
// - Seed the streak rendertarget witf Z respected.
// -------------------------------------------------------------
FLOAT4 PSSeedRTNoZ( passVertexColor IN): COLOR
{
	IN.diffuse.xyz *= tex2D(LightGlowTexSampler, IN.texCoord0.xy).xyz*SeedIntensity;
	return IN.diffuse;
}

FLOAT4 PSSeedRT( passVertexColor IN): COLOR
{
	FLOAT ZBuf = tex2Dproj( DepthMapSampler, IN.screenPos).x;
	if(IN.texCoord0.z > ZBuf)
	{
		IN.diffuse.xyz *= tex2D(LightGlowTexSampler, IN.texCoord0.xy).xyz*SeedIntensity;
		return IN.diffuse;
	}
	else
	{
		return 0.0f.xxxx;
	}
}


//
// -------------------------------------------------------------
// distortion stage
// -------------------------------------------------------------
//
FLOAT4 PSToneMapFogDistort(
		#if __XENON
			rageVertexOutputPassThrough4TexOnly IN
		#else
			rageVertexOutputPassThroughTexOnly IN
		#endif
) : COLOR0
{
	// Some signed noise for the distortion effect
	float noisy = tex3D(NoiseSampler, float3(0, 0.5 * IN.texCoord0.y, 0.1 * ElapsedTime)).r - 0.5;

	// Repeat a 1 - x^2 (0 < x < 1) curve and roll it with sinus.
	float dst = frac(IN.texCoord0.y * distortionFreq + distortionRoll * sin(ElapsedTime));
	dst *= (1 - dst);
	// Make sure distortion is highest in the center of the image
	dst /= 1 + distortionScale * abs(IN.texCoord0.y);

	// ... and finally distort
	IN.texCoord0.x += distortionScale * noisy * dst;	   
	   
    FLOAT3 FullScreenImage = h3tex2D(FullResMapSampler, IN.texCoord0.xy);
    
    // blurred image for HDR
    FLOAT3 BlurredImage = h3tex2D(RenderMapAnisoSampler, IN.texCoord0.xy);
    
	float currDepth = rageTexDepth2D(DepthMapSampler, IN.texCoord0.xy).r;
	
	// gets adaption luminance value from the vertex shader
	// to save some performance we just fetch the 1x1 texture in the vertex shader 
	// and send down the values to the pixel shader
#if __XENON    
    float AdaptedLum = IN.texCoord1.r;
#else
#if __PS3
	float AdaptedLum = IN.texCoord0.z;
#else
	float AdaptedLum = tex2D( AdaptedLuminanceMapSampler, FLOAT2(0.0f, 0.0f)).x;
#endif
#endif	

	//
	// tone mapping
	// 
#if __WIN32PC
    // clamps light adaption
    AdaptedLum = clamp(AdaptedLum, LowerLimitAdaption, HigherLimitAdaption);
#endif // __WIN32PC
    
	// Tone map the full screen image


	float4 image = ColorFilters(ToneMapping1(IN, FogHeightBased(IN, FullScreenImage + BlurredImage, currDepth), AdaptedLum) );
	image = lerp(image, image.zxyw, (noisy+0.5f) * distortionScale);

	// Combine frame, distorted image and interference
	return image;
}

#if RAGE_MOTIONBLUR
#if 0
float3 MotionBlur2(float2 tex, float depth, float lengthScale )
{
	float2 spos1 = motionBlurGetOldScreenPos( tex, depth );
	float2 moVec =( spos1.xy-tex.xy ) * lengthScale;  // usaully lengthScale is one
	float maxLen = 32.0f/720.0f;
	float moLen = max( length(moVec), maxLen );
	moVec *= maxLen / moLen;  // Does the clampling here to save on texture cache trashing 

#if __XENON 
	float3 res = 0;

	// do hidden clamping
	// this allows for motion blur when it goes behind an object

	float3 dRange = float3( 0.2, 0.5,0.9);

	float4 d = float4( tex2D( DepthMapSampler, tex.xy ).x,   // 4 depth samples for occlusion 
		tex2D( DepthMapSampler, tex.xy + moVec * dRange.x).x, 
		tex2D( DepthMapSampler, tex.xy + moVec * dRange.y).x,
		tex2D( DepthMapSampler, tex.xy + moVec * dRange.z).x);

	float range = 100.0f;

	float3 blend = saturate( ( d.xxx - d.yzw ) * range + 1.0f );

	moVec *= dot( blend.xyz,dRange) /1.6f; //  <-- Shorten if occluded by objects

	float3 gradH =float3( moVec.x , moVec.y, 0.0f );
	float3 gradV =float3( 1.0f/720, 0.0, 0.0f );

	tex.xy += moVec * 0.25f;

	asm
	{
		setGradientH gradH
		setGradientV gradV
		tfetch2D res.xyz, tex.xy, FullResMapSampler,
		UseComputedLOD=true, UseRegisterGradients=true, AnisoFilter=max8to1
	};

	return res.xyz;

#elif __PS3
	// do hidden clamping
	// this allows for motion blur when it goes behind an object

	float3 dRange = float3( 0.2, 0.5,0.9);

	float4 d = float4( tex2D( DepthMapSampler, tex.xy ).x,   // 4 depth samples for occlusion 
		texDepth2D( DepthMapSampler, tex.xy + moVec * dRange.x).x, 
		texDepth2D( DepthMapSampler, tex.xy + moVec * dRange.y).x,
		texDepth2D( DepthMapSampler, tex.xy + moVec * dRange.z).x);

	float range = 100.0f;

	float3 blend = saturate( ( d.xxx - d.yzw ) * range + 1.0f );

	moVec *= dot( blend.xyz,dRange) /1.6f;  // <-- Shorten if occluded by objects

	float3 gradH =float3( moVec.x , moVec.y, 0.0f );
	float3 gradV =float3( 1.0f/720, 0.0, 0.0f );

	tex.xy += moVec * 0.25f;

	return tex2Dgrad(FullResMapSampler, tex, gradH, gradV);
#else
	return h3tex2D(FullResMapSampler, tex);
#endif
}
#endif // 0

half2 GetVelocity(rageVertexOutputPassThroughTexOnly IN)
{
#if RAGE_VELOCITYBUFFER
	float2 velocity = h2tex2D(VelocityMapSampler, IN.texCoord0.xy);
	velocity.y = -velocity.y; // Why flip? Who knows?
#else
	float2 currPosProj = IN.texCoord0.xy * 2.0f - 1.0f;
#if __PS3
	float depth = texDepth2D(DepthMapSampler, IN.texCoord0.xy).r;
#else
	float depth = 1.0f - h1tex2D(DepthMapSampler, IN.texCoord0.xy);
#endif // __PS3
	float4 worldPos = mul(float4(currPosProj, depth, 1), gViewProjInverse);
	worldPos /= worldPos.w;
	float4 prevPosProj = mul(worldPos, gPrevViewProj);
	float2 velocity = (currPosProj - prevPosProj.xy / prevPosProj.w) * 0.5f * gMotionBlurScalar;
#endif
	return velocity;
}

void AddMotionBlurSample(inout float4 average, half2 texCoords)
{
	half3 sample = h3tex2D(QuarterMapSampler, texCoords);
	average += float4(sample, 1);
}

half4 PSMotionBlur(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
	// How many samples should be use to blur per pixel
	// We blur backwards and forwards, so a value of 8
	// for instance implied 4 forwards and 4 backwards
	static const int numSamples = 8;
	half2 velocity = GetVelocity(IN);
	half2 velocityStep = velocity / numSamples;
	float speed = length(velocity);
	float3 baseSample = h3tex2D(FullResMapSampler, IN.texCoord0.xy);
	float4 sampleAverage = float4(baseSample.rgb, 1);
	
	// Look up into our jitter texture using this 'magic' formula
	static const half2 JitterOffset = { 58.164f, 47.13f };
	const half2 jitterLookup = IN.texCoord0.xy * JitterOffset + (baseSample.rg * 8.0f);
	half jitterScalar = h1tex2D(JitterMapSampler, jitterLookup) - 0.5f;
	IN.texCoord0.xy += velocityStep * jitterScalar;
	
#if __XENON
	[unroll]
#endif // __XENON
	for (int i = 0; i < numSamples / 2; ++i)
	{
		half2 currentVelocityStep = velocityStep * (i + 1);
		AddMotionBlurSample(sampleAverage, IN.texCoord0.xy + currentVelocityStep);
		AddMotionBlurSample(sampleAverage, IN.texCoord0.xy - currentVelocityStep);
	}
	
	// Magic number for controlling the interpolation between the motion blurred
	// quarter size RT and the single full res (non-motion blurred) lookup
	static const float speedLerpScalar = 100.0f;
	return float4(lerp(baseSample, sampleAverage.rgb / sampleAverage.w, min(1.0f, speed * speedLerpScalar)), 1);
}
#endif // RAGE_MOTIONBLUR

#if MC4_MOTIONBLUR

half4 GetVelocityMC4(float2 texCoord)
{
	//float2 offset = {1.0f / 1280.0f, 1.0f / 720.0f};
	half4 temp = tex2D(VelocityMapSampler, texCoord.xy);// - (offset * 0.5f));
	return temp;
	//return half3(temp.x, temp.y, temp.z);// + temp.w) * 0.5f);
}


void AddMotionBlurSample(inout half4 average, half2 texCoords)
{
	half3 sample = h3tex2D(QuarterMapSampler, texCoords);
	average += half4(sample, 1.0f);
}

half4 PSMotionBlurCombineMC4(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
	float3 baseSample = h3tex2D(FullResMapSampler, IN.texCoord0.xy);
	float4 blur = h4tex2D(QuarterMapSampler, IN.texCoord0.xy);

	return float4(lerp(baseSample.xyz, blur.xyz, saturate(blur.a*4.0f)), 1);
}


half4 PSStreakMotionBlur(rageVertexOutputPassThroughTexOnly IN) : COLOR0
{
	static const int numSamples = 8;
	half4 velocity = GetVelocityMC4(IN.texCoord0.xy);
	velocity.xy = velocity.xy * 2.0f - 1.0f;
	//velocity.xy /= 38.0f;
	//half velMag = velocity.z;
	half velMag = velocity.z;
	//velMag = ragePow(velMag, 1.25f);
	velocity.xy *= (velMag / 38.0f);

	//return float4(velocity.xy, 0.0f, 1.0f);
	//return float4(tex2D(VelocityMapSampler, IN.texCoord0.xy).rgb, 1.0f);

	//velocity.y = -velocity.y;
	half2 velocityStep = velocity.xy / numSamples;
	//float speed = length(velocity.xyz);
	half3 baseSample = h3tex2D(FullResMapSampler, IN.texCoord0.xy);
	half4 sampleAverage = float4(baseSample.rgb, 1);
	half dofVal = velocity.w;


#if __XENON
	[unroll]
#endif
	for (int i = 0; i < numSamples / 2; ++i)
	{
		half2 currentVelocityStep = velocityStep * (i + 1);
		AddMotionBlurSample(sampleAverage, IN.texCoord0.xy + currentVelocityStep);
		AddMotionBlurSample(sampleAverage, IN.texCoord0.xy - currentVelocityStep);
	}

	half3 outColor = sampleAverage.rgb / sampleAverage.w;

	half2 normScreenPos = IN.texCoord0.xy;
	half2 vecToCenter = normScreenPos - 0.5f;
	half distToCenter = ragePow(length(vecToCenter) * 2.25f, 0.8f);

	half3 desat = dot(outColor.rgb,float3(0.3f,0.59f,0.11f));
	outColor = lerp(outColor, desat, distToCenter*blurEffectDesatScale);


	half3 tinted = outColor.rgb * blurEffectTintColor;
	outColor.rgb = lerp(outColor.rgb, tinted.rgb, distToCenter * blurEffectTintFade);

	return half4(outColor.rgb, saturate(velMag + dofVal));
}
#endif

struct vertexOutputSSAO
{
	float4	pos			: DX_POS;
	float2	texCoord0	: TEXCOORD0;		
};

vertexOutputSSAO VS_screenTransformSSAO(float4 pos : POSITION0)
{
    vertexOutputSSAO OUT;
	OUT.pos	= float4(pos.xy,0,1);
	OUT.texCoord0 = pos.xy*0.5f+0.5f;
#if __XENON	
	OUT.texCoord0.xy+=gScreenSize.zw*0.5f;
	OUT.texCoord0.y=1.0f-OUT.texCoord0.y;
#else
	OUT.texCoord0.y=1.0f-OUT.texCoord0.y;
#endif
	return(OUT);
}

float ooScaleDepthSample(float depthSample)
{
#if __XENON
	float ood = depthSample*SSAOProjectionParams.z+SSAOProjectionParams.w;
#else
	float ood = depthSample*SSAOProjectionParams.z-SSAOProjectionParams.w;
#endif
	return ood;
}

#if __PS3
float4 PS_SSAODownscale(vertexOutputSSAO IN) : COLOR
{
	float2 off=gScreenSize.zw;

	float dsamp0 = rageTexDepth2D(DepthMapSampler, IN.texCoord0).x;
	float dsamp1 = rageTexDepth2D(DepthMapSampler, IN.texCoord0+float2(off.x,0)).x;
	float dsamp2 = rageTexDepth2D(DepthMapSampler, IN.texCoord0+off.xy).x;
	float dsamp3 = rageTexDepth2D(DepthMapSampler, IN.texCoord0+float2(0,off.y)).x;

	float depth=1.0f/ooScaleDepthSample(dsamp0);
	float d0=floor(depth)/256.0f;
	float d1=floor((depth-d0*256.0f)*256.0f)/256.0f;
	float d2=floor(((depth-d0*256.0f)-d1)*256.0f*256.0f)/256.0f;

	return float4(1.0f, d0, d1, d2);
}
#else
float4 PS_SSAODownscale(vertexOutputSSAO IN) : COLOR
{
	float2 tex = IN.texCoord0;

	float2 off=gScreenSize.zw;

	float4 dsamps0;

#if __PS3 || 1
	float oodepth=ooScaleDepthSample(rageTexDepth2D(DepthMapSampler, tex+off*float2(0,0)).x);
#else
	dsamps0.x = rageTexDepth2D(DepthMapSampler, tex+off*float2(0,0)).x;
	dsamps0.y = rageTexDepth2D(DepthMapSampler, tex+off*float2(1,0)).x;
	dsamps0.z = rageTexDepth2D(DepthMapSampler, tex+off*float2(1,1)).x;
	dsamps0.w = rageTexDepth2D(DepthMapSampler, tex+off*float2(0,1)).x;
	float oodepth=ooScaleDepthSample(max(max(dsamps0.x, dsamps0.y),max(dsamps0.z, dsamps0.w)));
#endif

#if __XENON
	float depth=(1.0f/oodepth);

	float exp=floor(clamp(log2(abs(depth))+64,0,128));
	float rangemin=ragePow(2,exp-64);
	float mant=(abs(depth)-rangemin)/rangemin;

	float oot=256.0f/255.0f;
	float m1=floor(min(mant*oot,1.0f)*255.0f)/255.0f;
	float m2=floor(min(frac(mant*256.0f)*oot,1.0f)*255.0f)/255.0f;

	return float4(1.0, m1, exp/255.0f, m2);
#else
	float depth=abs(1.0f/oodepth);
	float d0=floor(depth)/256.0f;
	float d1=floor((depth-d0*256.0f)*256.0f)/256.0f;
	float d2=floor(((depth-d0*256.0f)-d1)*256.0f*256.0f)/256.0f;
	return float4(1.0, d0, d1, d2);
#endif
}
#endif // __PS3

float4 PS_SSAOMain(vertexOutputSSAO IN) : COLOR
{
	float2 tex = IN.texCoord0;

#if __XENON
	float m=(255.0f+1.0f/(256*255));
	float2 mscale=float2(m/256.0f, m/(256.0f*256.0f));
#endif

	float4 dsamp=tex2D(SSAOMapSampler,tex).xyzw;
#if __XENON
	float depth=-ragePow(2,(dsamp.z*m)-64)*(1.0f+dot(dsamp.yw,mscale));
#else
	float depth=-dot(dsamp,float4(0,256.0f, 1.0f, 1.0f/256.0f));
#endif
	float oodepth=1.0f/depth;

#if __XENON
	float2 ntex0=tex+(gScreenSize.zw*float2(-2.0f,-2.0f));
#else
	float2 ntex0=tex+(gScreenSize.zw*float2(-2.0f,2.0f));
#endif
	float4 nsamp0=tex2D(SSAOMapSampler,ntex0).xyzw;
#if __XENON
	float ndepth0=-ragePow(2,(nsamp0.z*m)-64)*(1.0f+dot(nsamp0.yw,mscale));
#else
	float ndepth0=-dot(nsamp0,float4(0,256.0f, 1.0f, 1.0f/256.0f));
#endif

#if __XENON
	float2 ntex1=tex+(gScreenSize.zw*float2(2.0f,2.0f));
#else
	float2 ntex1=tex+(gScreenSize.zw*float2(2.0f,-2.0f));
#endif
	float4 nsamp1=tex2D(SSAOMapSampler,ntex1).xyzw;
#if __XENON
	float ndepth1=-ragePow(2,(nsamp1.z*m)-64)*(1.0f+dot(nsamp1.yw,mscale));
#else
	float ndepth1=-dot(nsamp1,float4(0,256.0f, 1.0f, 1.0f/256.0f));
#endif

	float angle=dot(IN.texCoord0*gScreenSize.xy*0.5f, float2(9.0f, 29.814f));//+angle_offset;

	float2 scx;
	sincos(angle, scx.x, scx.y);

#if __XENON
	float2 tScale=gScreenSize.zw*2.0f*(SSAOMinBlurDiscSize+SSAOMaxBlurDiscSize*saturate(-SSAOMaxBlurDiscSizeDepth*oodepth));
#else
	float2 tScale=gScreenSize.zw*(2.0f*(SSAOMinBlurDiscSize+SSAOMaxBlurDiscSize*abs(SSAOMaxBlurDiscSizeDepth*oodepth)));
#endif
	float2 tex0 = tex+float2( scx.y,-scx.x)*(1.00f*tScale);
	float2 tex1 = tex+float2( scx.x, scx.y)*(0.75f*tScale);
	float2 tex2 = tex+float2(-scx.y, scx.x)*(0.50f*tScale);
	float2 tex3 = tex+float2(-scx.x,-scx.y)*(0.25f*tScale);

	float4 dsamps;
	float4 dsamp0=tex2D(SSAOMapSampler,tex0).xyzw;
	float4 dsamp1=tex2D(SSAOMapSampler,tex1).xyzw;
	float4 dsamp2=tex2D(SSAOMapSampler,tex2).xyzw;
	float4 dsamp3=tex2D(SSAOMapSampler,tex3).xyzw;

#if __XENON
	dsamps.x=-ragePow(2,(dsamp0.z*m)-64)*(1.0f+dot(dsamp0.yw,mscale));
	dsamps.y=-ragePow(2,(dsamp1.z*m)-64)*(1.0f+dot(dsamp1.yw,mscale));
	dsamps.z=-ragePow(2,(dsamp2.z*m)-64)*(1.0f+dot(dsamp2.yw,mscale));
	dsamps.w=-ragePow(2,(dsamp3.z*m)-64)*(1.0f+dot(dsamp3.yw,mscale));
#else
	dsamps.x=-dot(dsamp0,float4(0,256.0f, 1.0f, 1.0f/256.0f));
	dsamps.y=-dot(dsamp1,float4(0,256.0f, 1.0f, 1.0f/256.0f));
	dsamps.z=-dot(dsamp2,float4(0,256.0f, 1.0f, 1.0f/256.0f));
	dsamps.w=-dot(dsamp3,float4(0,256.0f, 1.0f, 1.0f/256.0f));
#endif

	float4 tdepths=dsamps*oodepth;

	float3 rPos=float3((tex-0.5f)*SSAOProjectionParams.xy,0.5f);
	float3 dir0=(float3((tex0-0.5f)*SSAOProjectionParams.xy,0.5f)*tdepths.x)-rPos;
	float3 dir1=(float3((tex1-0.5f)*SSAOProjectionParams.xy,0.5f)*tdepths.y)-rPos;
	float3 dir2=(float3((tex2-0.5f)*SSAOProjectionParams.xy,0.5f)*tdepths.z)-rPos;
	float3 dir3=(float3((tex3-0.5f)*SSAOProjectionParams.xy,0.5f)*tdepths.w)-rPos;
	float3 nPos=rPos*depth;

	float3 rPos0=float3((ntex0-0.5f)*SSAOProjectionParams.xy,0.5f);
	float3 nPos0=rPos0*ndepth0;

	float3 rPos1=float3((ntex1-0.5f)*SSAOProjectionParams.xy,0.5f);
	float3 nPos1=rPos1*ndepth1;

	float3 normT = normalize(cross(ddx(nPos.xyz),ddy(nPos.xyz)));
	float3 norm0 = normalize(cross(ddx(nPos0.xyz),ddy(nPos0.xyz)));
	float3 norm1 = normalize(cross(ddx(nPos1.xyz),ddy(nPos1.xyz)));

#if __XENON
	float3 norm=-normalize(normT+norm0+norm1);
#else
	float3 norm=normalize(normT+norm0+norm1);
#endif

	float4 len=float4(length(dir0), length(dir1), length(dir2), length(dir3));

	dir0=dir0/len.x;
	dir1=dir1/len.y;
	dir2=dir2/len.z;
	dir3=dir3/len.w;

	float4 ndot=float4(dot(dir0, norm), dot(dir1, norm), dot(dir2, norm), dot(dir3, norm));

	float num=dot(saturate(max((len*-depth*2.0f),(1.0-ndot))),0.25f.xxxx);

#if __PS3
	return float4(ragePow(num,SSAOIntensity), dsamp.yzw);
#else
	return float4(ragePow(num,SSAOIntensity), dsamp.yzw);
#endif
}

float4 PS_SSAOBlurX(vertexOutputSSAO IN) : COLOR
{
	float num = 0.0f;

	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x - 4.0*TexelSize.x, IN.texCoord0.y)).x * 1.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x - 3.0*TexelSize.x, IN.texCoord0.y)).x * 2.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x - 2.0*TexelSize.x, IN.texCoord0.y)).x * 3.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x - TexelSize.x, IN.texCoord0.y)).x * 4.0/25.0;
	float4 sample = tex2D(SSAOMapSampler, IN.texCoord0);
	num += sample.x * 5.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x + TexelSize.x, IN.texCoord0.y)).x * 4.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x + 2.0*TexelSize.x, IN.texCoord0.y)).x * 3.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x + 3.0*TexelSize.x, IN.texCoord0.y)).x * 2.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x + 4.0*TexelSize.x, IN.texCoord0.y)).x * 1.0/25.0;

#if __PS3
	return sample; // no blur
#else
	return float4(num, sample.yzw);
#endif // __PS3
}

float4 PS_SSAOBlurY(vertexOutputSSAO IN) : COLOR
{
	float num = 0.0f;

	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x, IN.texCoord0.y - 4.0*TexelSize.y)).x * 1.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x, IN.texCoord0.y - 3.0*TexelSize.y)).x * 2.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x, IN.texCoord0.y - 2.0*TexelSize.y)).x * 3.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x, IN.texCoord0.y - TexelSize.y)).x * 4.0/25.0;
	float4 sample = tex2D(SSAOMapSampler, IN.texCoord0);
	num += sample.x * 5.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x, IN.texCoord0.y + TexelSize.y)).x * 4.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x, IN.texCoord0.y + 2.0*TexelSize.y)).x * 3.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x, IN.texCoord0.y + 3.0*TexelSize.y)).x * 2.0/25.0;
	num += tex2D(SSAOMapSampler, float2(IN.texCoord0.x, IN.texCoord0.y + 4.0*TexelSize.y)).x * 1.0/25.0;

#if __PS3
	return sample;
#else
	return float4(num, sample.yzw);
#endif // __PS3
}

float4 PS_SSAOUpscale(vertexOutputSSAO IN) : COLOR
{
#if 1
	float ssao = tex2D(RenderMapBilinearSampler,IN.texCoord0.xy).x;
#else
	float2 ss=gScreenSize.zw*4.0f;

	float mini_occ=tex2D(RenderMapBilinearSampler,IN.texCoord0.xy).x;
	float mini_occ0=tex2D(RenderMapBilinearSampler,IN.texCoord0.xy+float2(-1.5f,-1.5f)*ss).x;
	float mini_occ1=tex2D(RenderMapBilinearSampler,IN.texCoord0.xy+float2( 1.5f,-1.5f)*ss).x;
	float mini_occ2=tex2D(RenderMapBilinearSampler,IN.texCoord0.xy+float2(-1.5f, 1.5f)*ss).x;
	float mini_occ3=tex2D(RenderMapBilinearSampler,IN.texCoord0.xy+float2( 1.5f, 1.5f)*ss).x;

	float ssao = (mini_occ+mini_occ0+mini_occ1+mini_occ2+mini_occ3)/5.0f;
#endif // 0
//return float4(ssao.xxx, 1);
	float4 fullRes = tex2D(FullResMapSampler, IN.texCoord0.xy);
	fullRes.rgb *= ssao;
	return fullRes;
}

technique Convert12bitToL16uv
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSConvertRGB12bitToL16uv();
    }
}

technique CopyRT
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSCopyRT();
    }
}

technique CopyRedRT
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSCopyRedRT();
    }
}

technique SeedRT
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = true;
		BlendOp = ADD;
		SrcBlend = ONE;
        DestBlend = ONE;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_PassColorThrough();
        PixelShader  = compile PIXELSHADER PSSeedRT();
    }
}

technique SeedRTNoZ
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = true;
		BlendOp = ADD;
		SrcBlend = ONE;
        DestBlend = ONE;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_PassColorThrough();
        PixelShader  = compile PIXELSHADER PSSeedRTNoZ();
    }
}

technique FogCard
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = true;
		ZWriteEnable = false;
        AlphaBlendEnable = true;
        AlphaTestEnable = false;
        SrcBlend = One;
        DestBlend = One;
    
        VertexShader = compile VERTEXSHADER VS_SmoothClipColor();
        PixelShader  = compile PIXELSHADER PSFogCard();
    }
}

technique ColorScaleCopyRT
{
    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSCombineBrightPassRT();
    }
}

technique CopyAlphaRT
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSCopyAlphaRT();
    }
}

technique CopyBlurFactor
{

    pass P0
    {
    
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSCopyBlurFactor();
    }
}

technique OnlyColorFilters
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSColorFilters();
    }
}

#if __PS3
technique PS3QuincunxFilter
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSScaleBuffer_QUINCUNX();
    }
}

technique PS3GaussFilter
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussBlurBuffer();
    }
}
#endif

technique DownSampleRT
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSScaleBuffer();
    }
}

#if MC4_MOTIONBLUR
technique DownSampleMotionBlur
{

    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSScaleMotionBlur();
    }
}
#endif

technique InitialDownSampleRT
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSInitialScaleBuffer();
    }
}

technique SampleAverageLuminance
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSSampleAverageLuminance();
    }
}

technique ReSampleAverageLuminance
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSReSampleAverageLuminance();
    }
}

technique ReSampleAverageLuminanceExp
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSReSampleAverageLuminanceExp();
    }
}

technique CalculateAdaptedLuminance
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

#if __PS3
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformToneMapping();
#else
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
#endif
        PixelShader  = compile PIXELSHADER PSCalculateAdaptedLuminance();
    }
}

technique BrightPass
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexOnly();
#else
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
#endif        
        PixelShader  = compile PIXELSHADER PSBrightPass();
    }
}

technique ToneDownBrightPass
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexOnly();
#else
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
#endif        
        PixelShader  = compile PIXELSHADER PSToneDownBrightPass();
    }
}

technique GaussX
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussX();
    }
}

technique GaussY
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussY();
    }
}

technique GaussX_5
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussX5();
    }
}


technique GaussY_5
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussY5();
    }
}

technique GaussX_17
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussX17();
    }
}

technique GaussY_17
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussY17();
    }
}

technique ToneMapping
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexOnly();
#else
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformToneMapping();
#endif        
        PixelShader  = compile PIXELSHADER PSToneMap();
    }
}

technique ToneMapDOF
{
    pass P0
    {
		CullMode = NONE;
   		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;

#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexOnly();
#else
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformToneMapping();
#endif        
        PixelShader  = compile PIXELSHADER PSToneMapDOF();
    }
}

technique ToneMapDOFFog
{
    pass P0
    {
		CullMode = NONE;
   		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;
#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexAndScrPos();
#else 
		// this is a dummy for now
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformToneMapping();       
#endif        
        PixelShader  = compile PIXELSHADER PSToneMapDOFFog();
    }
}

technique ToneMapFog
{
    pass P0
    {
		CullMode = NONE;
   		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;

#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexAndScrPos();
#else 
		// this is a dummy for now
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformToneMapping();       
#endif        
        PixelShader  = compile PIXELSHADER PSToneMapFog();
    }
}

technique ToneMapFogDistort
{
    pass P0
    {
		CullMode = NONE;
   		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;

#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexAndScrPos();
#else 
		// this is a dummy for now
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformToneMapping();       
#endif        
        PixelShader  = compile PIXELSHADER PSToneMapFogDistort();
    }
}


technique ToneMapFogPoliceCam
{
    pass P0
    {
		CullMode = NONE;
   		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;

#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexAndScrPos();
#else 
		// this is a dummy for now
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformToneMapping();       
#endif        
        PixelShader  = compile PIXELSHADER PSToneMapFogPoliceCam();
    }
}

technique Fog
 {
    pass P0
    {
		CullMode = NONE;
   		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;

#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexAndScrPos();
#else 
		// this is a dummy for now
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();       
#endif        
        PixelShader  = compile PIXELSHADER PSFog();
    }
}


technique BlendedHeightFog
 {
    pass P0
    {
		CullMode = NONE;
   		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = true;

        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;

#if __XENON
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXform4TexAndScrPos();
#else 
		// this is a dummy for now
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();       
#endif        
        PixelShader  = compile PIXELSHADER PSBlendedHeightFog();
    }
}

technique KawaseBloomFilter
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER KawaseBloomFilter1();
    }
}

technique KawaseStreakFilter
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER KawaseStreakFilter1();
    }
}


technique GaussX_25
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussX_25();
    }
}

technique GaussY_25
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSGaussY_25();
    }
}

technique draw
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSCopyRT();
    }
}

technique RDR2Death
{
	pass P0
	{
		CullMode = NONE;
   		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformSepia();
        PixelShader  = compile PIXELSHADER PSRDR2Death();
	}
}

#if MC4_MOTIONBLUR
technique StreakMotionBlur
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSStreakMotionBlur();
    }
}

technique MotionBlurCombine
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
		VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSMotionBlurCombineMC4();
    }
}
#endif

#if RAGE_MOTIONBLUR
technique MotionBlur
{
    pass P0
    {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
    
		VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSMotionBlur();
    }
}
#endif // RAGE_MOTIONBLUR

technique SSAODownscale
{
	pass p0
	{
		VertexShader = compile VERTEXSHADER VS_screenTransformSSAO();
		PixelShader = compile PIXELSHADER PS_SSAODownscale();
	}
}

technique SSAOMain
{
	pass p0
	{
		VertexShader = compile VERTEXSHADER VS_screenTransformSSAO();
		PixelShader = compile PIXELSHADER PS_SSAOMain();
	}
}

technique SSAOBlurX
{
	pass p0
	{
		VertexShader = compile VERTEXSHADER VS_screenTransformSSAO();
		PixelShader = compile PIXELSHADER PS_SSAOBlurX();
	}
}

technique SSAOBlurY
{
	pass p0
	{
		VertexShader = compile VERTEXSHADER VS_screenTransformSSAO();
		PixelShader = compile PIXELSHADER PS_SSAOBlurY();
	}
}

technique SSAOUpscale
{
	pass p0
	{
		VertexShader = compile VERTEXSHADER VS_screenTransformSSAO();
		PixelShader = compile PIXELSHADER PS_SSAOUpscale();
	}
}

technique SimpleCopy
{
    pass P0
    {
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSCopyRT();
    }
}

technique DepthDownsample
{
    pass P0
    {
		ZWriteEnable = true;
#if 1
		ZEnable = true;
		ZFunc = Always;
#else
		ZEnable = false;
#endif // 0
        VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
        PixelShader  = compile PIXELSHADER PSDepthDownsample();
    }
}
