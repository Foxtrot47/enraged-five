// 
// grpostfx/edgepost_task.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#if __PPU
#include "edge/post/edgepost_ppu.h"
#include "grcore/viewport.h"
#include "grcore/wrapper_gcm.h"
#include "grpostfx/edgepostjobheader.h"
#include "grpostfx/postfx.h"
#include "system/codefrag_spu.h"
#include "system/taskheader.h"

#include <cell/spurs.h>

//////////////////////////////////////////////////////
/// NUMBER OF SPU EDGEPOST CAN USE
#define EDGEPOST_SPU_NUM				2

extern char SPURS_TASK_START(edgepostspu)[];
DECLARE_FRAG_INTERFACE(bloomcapturespu);
DECLARE_FRAG_INTERFACE(dofspu);
DECLARE_FRAG_INTERFACE(downsamplespu);
DECLARE_FRAG_INTERFACE(fuzzinessspu);
DECLARE_FRAG_INTERFACE(horizontalgaussspu);
DECLARE_FRAG_INTERFACE(ilrspu);
DECLARE_FRAG_INTERFACE(motionblurspu);
DECLARE_FRAG_INTERFACE(ropspu);
DECLARE_FRAG_INTERFACE(upsamplespu);
DECLARE_FRAG_INTERFACE(verticalgaussspu);

namespace rage
{

extern CellSpurs* g_Spurs;

static CellSpursTaskset* s_TaskSet;
static CellSpursTaskId s_TaskIds[EDGEPOST_SPU_NUM];
static CellSpursTaskAttribute s_TaskAttributes[EDGEPOST_SPU_NUM];
static CellSpursTaskArgument s_TaskArguments[EDGEPOST_SPU_NUM];
static CellSpursTaskSaveConfig s_TaskSaveConfigs[EDGEPOST_SPU_NUM];
static u32 s_CompletionCounter  = EDGEPOST_SPU_NUM;
static u32 s_EdgePostLabelIndex;

static const CellSpursTaskLsPattern kPostTaskLsPattern = { { 0x00000000, 0x00000000, 0x00000000, 0x00000001 } }; // last 2K for stack

// Gauss filter weights
float grPostFX::grPostFXEdgeEffect::sm_BloomWeights[4] = { 0.3125f, 0.234375f, 0.09375f, 0.015625f};
float grPostFX::grPostFXEdgeEffect::sm_DoubleWeights[4] = { 0.3125f * 2.f, 0.234375f * 2.f, 0.09375f * 2.f, 0.015625f * 2.f};

static inline u32 align(const u32 value, u32 alignment)
{
	return (value + (alignment - 1)) & ~(alignment - 1);
}

static uint8_t* s_NotifyArea = 0;

void grPostFX::grPostFXEdgeEffect::RsxUserHandler(const u32 cause)
{
	// kick Edge Post
	grPostFXEdgeEffect* pEffect = (grPostFXEdgeEffect*)cause;
	pEffect->StartSpuPostprocessing();
}

grcRenderTarget* s_ColorScratch;
grcRenderTarget* s_DepthScratch;

grPostFX::grPostFXEdgeEffect::grPostFXEdgeEffect(grPostFX *postFX)
: grPostFXEffect(postFX)
{
	m_Name = "EDGE Post";
	m_TechniqueName = "EDGE Post";

	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();

	const grcRenderTarget* frontBuffer = textureFactory.GetFrontBuffer();
	const u32 width = frontBuffer->GetWidth();
	const u32 height = frontBuffer->GetHeight();
	const u32 width2 = width / 2;
	const u32 height2 = height / 2;
	const u32 width4 = width2 / 2;
	const u32 height4 = height2 / 2;
	const u32 width8 = width4 / 2;
	const u32 height8 = height4 / 2;
	const u32 width16 = width8 / 2;
	const u32 height16 = height8 / 2;

	grcTextureFactory::CreateParams createParams;
	createParams.Format = grctfA8R8G8B8;
	createParams.InTiledMemory = true;
	createParams.EnableCompression = false;
	createParams.InLocalMemory = true;
	s_ColorScratch = textureFactory.CreateRenderTarget("EDGE Post Color Scratch", grcrtPermanent, width2, height2, 32, &createParams);

	createParams.Format = grctfD24S8;
	s_DepthScratch = textureFactory.CreateRenderTarget("EDGE Post Depth Scratch", grcrtDepthBuffer, width2, height2, 32, &createParams);

	// calculate host memory requirements
	u32 halfResImageSize = align(width2 * height2 * 4, 128);

	// calculate arena size
	u32 arenaSize = 
		halfResImageSize +			// half resolution color buffer
		halfResImageSize +			// half resolution depth + motion buffer
		halfResImageSize +			// tmp0
		halfResImageSize;			// tmp1

	const u32 arenaAlignment = 1024*1024;
	Printf("Allocating SPU Postprocessing arena size in host memory : %d (~ %dMB)\n", arenaSize, (arenaSize + arenaAlignment - 1) / arenaAlignment);

	// align to 1MB
	arenaSize = align(arenaSize, arenaAlignment);

	// Allocate and map XDR memory
	m_PostprocessArena = rage_aligned_new (arenaAlignment) uint8_t[arenaSize];
	ASSERT_ONLY(int ret); /*= cellGcmMapMainMemory(m_PostprocessArena, arenaSize, &m_LocalToMainOffset);
	Assert(ret == CELL_OK);*/

	// Allocate images inside the arena
	uint8_t* pData = m_PostprocessArena;

	// color buffer
	m_LocalToMainImage = pData; pData += halfResImageSize;

	// depth buffer + motion
	m_DepthImage = pData; pData += halfResImageSize;

	// temporary buffers
	uint8_t* pTmp0 = pData; pData += halfResImageSize;
	uint8_t* pTmp1 = pData; pData += halfResImageSize;

	// Get RSX offsets
	m_LocalToMainOffset = gcm::MainOffset(m_LocalToMainImage);
	m_DepthOffset = gcm::MainOffset(m_DepthImage);

	// Setup stages
	memset(&m_Stages[0], 0, sizeof(m_Stages));
	EdgePostProcessStage* pStage = &m_Stages[0];

	//////////////////////////////////////////////////////////////////////////
	/// DEPTH OF FIELD

	// calculate near fuzziness
	SetupNearFuzziness(pStage++,		width2, height2,	m_DepthImage, pTmp0);

	// blur near fuzziness a bit to smooth out near dof edges
	SetupDownsampleFloat(pStage++,		width2, height2,	pTmp0, pTmp1);
	SetupHorizontalGaussFloat(pStage++,	width4, height4,   pTmp1, pTmp0, sm_DoubleWeights);
	SetupVerticalGaussFloat(pStage++,	width4, height4,   pTmp0, pTmp1, sm_DoubleWeights);
	SetupUpsampleFloat(pStage++,		width4, height4,	pTmp1, pTmp0);

	// calculate far fuzziness and combine with near fuzziness
	SetupFarFuzziness(pStage++,			width2, height2,	m_DepthImage, pTmp0, pTmp1);

	// Calculate Depth of field
	SetupDepthOfField(pStage++,			width2, height2,	m_LocalToMainImage, pTmp1, pTmp0);

#define USE_MOTIONBLUR (0)
#if USE_MOTIONBLUR
	//////////////////////////////////////////////////////////////////////////
	//// MOTION BLUR

	// Motion blur
	SetupMotionBlur(pStage++,			width2, height2,	pTmp0, m_DepthImage, m_LocalToMainImage);
#endif // USE_MOTIONBLUR
	//////////////////////////////////////////////////////////////////////////
	//// BLOOM

	// downsample to quarter res
	uint8_t* pTmp0_4[4];
	uint8_t* pTmp1_4[4];
	pTmp0_4[0] = pTmp0;
	pTmp1_4[0] = pTmp1;
	for (int i=1; i<4; ++i) 
	{
		pTmp0_4[i] = pTmp0_4[i-1] + (width4 * height4 * 4);
		pTmp1_4[i] = pTmp1_4[i-1] + (width4 * height4 * 4);
	}

	// downsample to 1/4 and capture bloom color
	SetupDownsample(pStage++,			width2, height2,	USE_MOTIONBLUR ? m_LocalToMainImage : pTmp0, pTmp1_4[0]);
	SetupBloomCaptureStage(pStage++,	width4, height4,	pTmp1_4[0], pTmp1_4[1]);

	// Blur and downsample to 1/8
	SetupHorizontalGauss(pStage++,		width4, height4,	pTmp1_4[1], pTmp1_4[0], sm_BloomWeights);
	SetupVerticalGauss(pStage++,		width4, height4,	pTmp1_4[0], pTmp1_4[1], sm_BloomWeights);
	SetupDownsample(pStage++,			width4, height4,	pTmp1_4[1], pTmp0_4[0]);

	// Blur and downsample to 1/16
	SetupHorizontalGauss(pStage++,		width8, height8,	pTmp0_4[0], pTmp0_4[1], sm_BloomWeights);
	SetupVerticalGauss(pStage++,		width8, height8,	pTmp0_4[1], pTmp0_4[0], sm_BloomWeights);
	SetupDownsample(pStage++,			width8, height8,	pTmp0_4[0], pTmp0_4[2]);

	// Extra blur 1/8 buffer
	SetupHorizontalGauss(pStage++,		width8, height8,	pTmp0_4[0], pTmp0_4[1], sm_BloomWeights);
	SetupVerticalGauss(pStage++,		width8, height8,	pTmp0_4[1], pTmp0_4[0], sm_BloomWeights);

	// Blur 1/16 buffer
	for (int k = 0; k < 3; ++k)
	{
		SetupHorizontalGauss(pStage++,	width16, height16,	pTmp0_4[2], pTmp0_4[3], sm_BloomWeights);
		SetupVerticalGauss(pStage++,	width16, height16,	pTmp0_4[3], pTmp0_4[2], sm_BloomWeights);
	}

	// Modulate upper levels
// 	SetupConstModulate(pStage++,		width16, height16,	pTmp0_4[2], 1.4f, pTmp0_4[2]);
// 	SetupConstModulate(pStage++,		width8, height8,	pTmp0_4[0], 1.3f, pTmp0_4[0]);
// 	SetupConstModulate(pStage++,		width4, height4,	pTmp1_4[1], 1.2f, pTmp1_4[1]);

	// Upsample to 1/8 and add
	SetupUpsample(pStage++,				width16, height16,	pTmp0_4[2], pTmp0_4[3]);
	SetupAddSat(pStage++,				width8, height8,	pTmp0_4[3], pTmp0_4[0], pTmp0_4[0]);

	// Upsample to 1/4 and add
	SetupUpsample(pStage++,				width8, height8,	pTmp0_4[0], pTmp0_4[1]);
	SetupAddSat(pStage++,				width4, height4,	pTmp0_4[1], pTmp1_4[1], pTmp1_4[1]);

#if 1
	// Downsample to 1/8
	SetupDownsample(pStage++,			width8, height8,	pTmp1_4[0], pTmp0_4[0]);

	// Internal lens reflection
	SetupIlr(pStage++,					width8, height8,	pTmp0_4[0], pTmp0_4[0]);

	// Blur ILR
	for (int k = 0; k < 2; ++k)
	{
		SetupHorizontalGauss(pStage++,	width8, height8,	pTmp0_4[0], pTmp0_4[1], sm_BloomWeights);
		SetupVerticalGauss(pStage++,	width8, height8,	pTmp0_4[1], pTmp0_4[0], sm_BloomWeights);
	}

	// Upsample and add to bloom
	SetupUpsample(pStage++,				width8, height8,	pTmp0_4[0], pTmp0_4[1]);
	SetupAddSat(pStage++,				width4, height4,	pTmp0_4[1], pTmp1_4[1], pTmp1_4[1]);
#endif // 0

	// Upsample to 1/2 and integrate
	SetupUpsample(pStage++,				width4, height4,	pTmp1_4[1], pTmp0);
	SetupAddSatModulateBloom(pStage++,	width2, height2,	m_LocalToMainImage, pTmp0, m_LocalToMainImage);

	// Count number of stages
	m_NumStages = pStage - &m_Stages[0];
	Assert(m_NumStages <= kMaxStages);

	// Set some default parameters values
	m_PostParams.m_bloomStrength = .4f;
	m_PostParams.m_exposureLevel = .8f;
	m_PostParams.m_minLuminance = .4f;
	m_PostParams.m_maxLuminance = .7f;
	m_PostParams.m_nearFuzzy = .0f;
	m_PostParams.m_nearSharp = .0f;
	m_PostParams.m_farSharp = .025;
	m_PostParams.m_farFuzzy = 0.06f;
	m_PostParams.m_maxFuzziness = 1.f;
	m_PostParams.m_motionScaler = 2.0f;

	// Spurs priority
	uint8_t prios[8] = { 1, 0, 0, 0, 0, 0, 0, 0};
	for (int j = 1; j < EDGEPOST_SPU_NUM; ++j)
		prios[j] = 1;

	// Create a taskset
#if 1
	s_TaskSet = (CellSpursTaskset*)rage_aligned_new (CELL_SPURS_TASKSET_ALIGN) u8[CELL_SPURS_TASKSET_CLASS1_SIZE];
#else
	static u8 taskSetBuffer[CELL_SPURS_TASKSET_CLASS1_SIZE] ALIGNED(CELL_SPURS_TASKSET_ALIGN);
	s_TaskSet = reinterpret_cast<CellSpursTaskset*>(taskSetBuffer);
#endif // 0

	// Initialize it
	CellSpursTasksetAttribute attributeTaskset;
	ASSERT_ONLY(ret =) cellSpursTasksetAttributeInitialize (&attributeTaskset, (uint64_t)0, prios, EDGEPOST_SPU_NUM);
	Assert(ret == CELL_OK);

	ASSERT_ONLY(ret =) cellSpursTasksetAttributeSetName(&attributeTaskset, "EDGE Post");
	Assert(ret == CELL_OK);

	ASSERT_ONLY(ret =) cellSpursTasksetAttributeSetTasksetSize(&attributeTaskset, CELL_SPURS_TASKSET_CLASS1_SIZE);
	Assert(ret == CELL_OK);

	ASSERT_ONLY(ret =) cellSpursCreateTasksetWithAttribute(g_Spurs, s_TaskSet, &attributeTaskset);
	Assert(ret == CELL_OK);

	// Create spurs tasks
	for (int t = 0; t < EDGEPOST_SPU_NUM; ++t)
	{
		// context save area
#if 1
		cellSpursTaskGetContextSaveAreaSize(&s_TaskSaveConfigs[t].sizeContext, &kPostTaskLsPattern);
		//Printf("s_TaskSaveConfigs[t=%d].sizeContext=%d\n", t, s_TaskSaveConfigs[t].sizeContext);
		void *context_save_buffer = rage_aligned_new (128) u8[s_TaskSaveConfigs[t].sizeContext];
#else
		const u32 sizeContext = CELL_SPURS_TASK_EXECUTION_CONTEXT_SIZE + 1*2048;
		s_TaskSaveConfigs[t].sizeContext = sizeContext;
		static u8 contextBuffer[EDGEPOST_SPU_NUM][sizeContext] ;
		void *context_save_buffer = contextBuffer[t];
#endif // 0

		s_TaskSaveConfigs[t].eaContext = context_save_buffer;
		s_TaskSaveConfigs[t].lsPattern = &kPostTaskLsPattern;

		// argument
		s_TaskArguments[t].u32[0] = (u32)&m_Workload;
		s_TaskArguments[t].u32[1] = (u32)&s_CompletionCounter;

		// task
		ASSERT_ONLY(ret =) cellSpursTaskAttributeInitialize(&s_TaskAttributes[t], SPURS_TASK_START(edgepostspu), &s_TaskSaveConfigs[t], &s_TaskArguments[t]);
		Assert(ret == CELL_OK);

		ASSERT_ONLY(ret =) cellSpursCreateTaskWithAttribute(s_TaskSet, &s_TaskIds[t], &s_TaskAttributes[t]);
		Assert(ret == CELL_OK);
	}

	// initialize rsx label value
	s_EdgePostLabelIndex = gcm::RsxSemaphoreRegistrar::Allocate();
	volatile u32 *label = cellGcmGetLabelAddress(s_EdgePostLabelIndex);
	*label = m_PostLabelValue = 0;

	// set RSX user handler
	cellGcmSetUserHandler(RsxUserHandler);

	// setup notify area
	s_NotifyArea = rage_aligned_new (1024*1024) u8[1024];
	ASSERT_ONLY(ret =) cellGcmMapEaIoAddressWithFlags(s_NotifyArea, CELL_GCM_NOTIFY_IO_ADDRESS_BASE, 1024*1024, CELL_GCM_IOMAP_FLAG_STRICT_ORDERING);
	Assert(ret == CELL_OK);

	// set a non zero value for the first time
	CellGcmNotifyData* pNotifyData = cellGcmGetNotifyDataAddress(0);
	pNotifyData->zero = 0xdeadbeef;
}

void grPostFX::grPostFXEdgeEffect::SetupStage(EdgePostProcessStage* pStage, const EdgePostImage* pImages, u32 numImages, void* pJobCode, u32 jobCodeSize)
{
	edgePostSetupStage(pStage, pImages, numImages, pJobCode, jobCodeSize, (jobCodeSize + 15) & ~15, &m_PostParams, sizeof(m_PostParams));
}

void grPostFX::grPostFXEdgeEffect::SetupDownsample(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width / 2, height / 2, 4),
		EdgePostImage(kEdgePostInputTile, srcEa, width, height, 4),
	};
	SetupStage(pStage, images, 2, (void*)FRAG_INTERFACE_START(downsamplespu), (u32)FRAG_INTERFACE_SIZE(downsamplespu));
	pStage->user0 = POST_DOWNSAMPLE_8;
}

void grPostFX::grPostFXEdgeEffect::SetupDownsampleFloat(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa)
{
	SetupDownsample(pStage, width, height, srcEa, dstEa);
	pStage->user0 = POST_DOWNSAMPLE_F;
}

void grPostFX::grPostFXEdgeEffect::SetupUpsample(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width * 2, height * 2, 4),
		EdgePostImage(kEdgePostInputTile, srcEa, width, height, 4, 4, 1),
	};
	SetupStage(pStage, images, 2, (void*)FRAG_INTERFACE_START(upsamplespu), (u32)FRAG_INTERFACE_SIZE(upsamplespu));
	pStage->user0 = POST_UPSAMPLE_8;
}

void grPostFX::grPostFXEdgeEffect::SetupUpsampleFloat(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa)
{
	SetupUpsample(pStage, width, height, srcEa, dstEa);
	pStage->user0 = POST_UPSAMPLE_F;
}

void grPostFX::grPostFXEdgeEffect::SetupHorizontalGauss(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa, float* weights)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, srcEa, width, height, 4, 4, 0),
	};
	SetupStage(pStage, images, 2, (void*)FRAG_INTERFACE_START(horizontalgaussspu), (u32)FRAG_INTERFACE_SIZE(horizontalgaussspu));
	pStage->user0 = POST_GAUSS_8;
	pStage->userDataF[0] = weights[0];
	pStage->userDataF[1] = weights[1];
	pStage->userDataF[2] = weights[2];
	pStage->userDataF[3] = weights[3];
}

void grPostFX::grPostFXEdgeEffect::SetupHorizontalGaussFloat(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa, float* weights)
{
	SetupHorizontalGauss(pStage, width, height, srcEa, dstEa, weights);
	pStage->user0 = POST_GAUSS_F;
}

void grPostFX::grPostFXEdgeEffect::SetupVerticalGauss(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa, float* weights)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, srcEa, width, height, 4, 0, 3),
	};
	SetupStage(pStage, images, 2, (void*)FRAG_INTERFACE_START(verticalgaussspu), (u32)FRAG_INTERFACE_SIZE(verticalgaussspu));
	pStage->user0 = POST_GAUSS_8;
	pStage->userDataF[0] = weights[0];
	pStage->userDataF[1] = weights[1];
	pStage->userDataF[2] = weights[2];
	pStage->userDataF[3] = weights[3];
}

void grPostFX::grPostFXEdgeEffect::SetupVerticalGaussFloat(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa, float* weights)
{
	SetupVerticalGauss(pStage, width, height, srcEa, dstEa, weights);
	pStage->user0 = POST_GAUSS_F;
}

void grPostFX::grPostFXEdgeEffect::SetupBloomCaptureStage(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, srcEa, width, height, 4),
	};
	SetupStage(pStage, images, 2, (void*)FRAG_INTERFACE_START(bloomcapturespu), (u32)FRAG_INTERFACE_SIZE(bloomcapturespu));
}

void grPostFX::grPostFXEdgeEffect::SetupNearFuzziness(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, srcEa, width, height, 4),
	};
	SetupStage(pStage, images, 2, (void*)FRAG_INTERFACE_START(fuzzinessspu), (u32)FRAG_INTERFACE_SIZE(fuzzinessspu));
	pStage->user0 = POST_CALC_NEAR_FUZZINESS;
}

void grPostFX::grPostFXEdgeEffect::SetupFarFuzziness(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* nearEa, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, srcEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, nearEa, width, height, 4),
	};
	SetupStage(pStage, images, 3, (void*)FRAG_INTERFACE_START(fuzzinessspu), (u32)FRAG_INTERFACE_SIZE(fuzzinessspu));
	pStage->user0 = POST_CALC_FAR_FUZZINESS;
}

void grPostFX::grPostFXEdgeEffect::SetupDepthOfField(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* depthEa, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, srcEa, width, height, 4, 8, 7, 4),
		EdgePostImage(kEdgePostInputTile, depthEa, width, height, 4, 8, 7),
	};
	SetupStage(pStage, images, 3, (void*)FRAG_INTERFACE_START(dofspu), (u32)FRAG_INTERFACE_SIZE(dofspu));
}

void grPostFX::grPostFXEdgeEffect::SetupMotionBlur(EdgePostProcessStage* pStage, u32 width, u32 height, void* srcEa, void* motion, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, srcEa, width, height, 4, 16, 16, 4),
		EdgePostImage(kEdgePostInputTile, motion, width, height, 4),
	};
	SetupStage(pStage, images, 3, (void*)FRAG_INTERFACE_START(motionblurspu), (u32)FRAG_INTERFACE_SIZE(motionblurspu));
}

void grPostFX::grPostFXEdgeEffect::SetupAddSat(EdgePostProcessStage* pStage, u32 width, u32 height, void* src0, void* src1, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, src0, width, height, 4),
		EdgePostImage(kEdgePostInputTile, src1, width, height, 4),
	};
	SetupStage(pStage, images, 3, (void*)FRAG_INTERFACE_START(ropspu), (u32)FRAG_INTERFACE_SIZE(ropspu));
	pStage->user0 = POST_ROP_ADDSAT;
}

void grPostFX::grPostFXEdgeEffect::SetupIlr(EdgePostProcessStage* pStage, u32 width, u32 height, void* src, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, src, width, height, 4),
	};
	SetupStage(pStage, images, 2, (void*)FRAG_INTERFACE_START(ilrspu), (u32)FRAG_INTERFACE_SIZE(ilrspu));
}

void grPostFX::grPostFXEdgeEffect::SetupAddSatModulateBloom(EdgePostProcessStage* pStage, u32 width, u32 height, void* src0, void* src1, void* dstEa)
{
	EdgePostImage images[] =
	{
		EdgePostImage(kEdgePostOutputTile, dstEa, width, height, 4),
		EdgePostImage(kEdgePostInputTile, src0, width, height, 4),
		EdgePostImage(kEdgePostInputTile, src1, width, height, 4),
	};
	SetupStage(pStage, images, 3, (void*)FRAG_INTERFACE_START(ropspu), (u32)FRAG_INTERFACE_SIZE(ropspu));
	pStage->user0 = POST_ROP_PREMULTIPLY_ADDSAT;
}

#if __BANK
void grPostFX::grPostFXEdgeEffect::AddWidgets(grPostFX * /*postFX*/, bkBank &bk)
{
	bk.AddSlider("Bloom strength", &m_PostParams.m_bloomStrength, 0.0f, 100.0f, 0.002f);
	bk.AddSlider("Bloom exposure level", &m_PostParams.m_exposureLevel, 0.0f, 100.0f, 0.002f);
	bk.AddSlider("Bloom min luminance", &m_PostParams.m_minLuminance, 0.0f, 100.0f, 0.002f);
	bk.AddSlider("Bloom max luminance", &m_PostParams.m_maxLuminance, 0.0f, 100.0f, 0.002f);
	bk.AddSlider("Dof near fuzzy", &m_PostParams.m_nearFuzzy, 0.0f, 1.0f, 0.002f);
	bk.AddSlider("Dof near sharp", &m_PostParams.m_nearSharp, 0.00001f, 1.0f, 0.002f);
	bk.AddSlider("Dof far sharp", &m_PostParams.m_farSharp, 0.0f, 1.1f, 0.002f);
	bk.AddSlider("Dof far fuzzy", &m_PostParams.m_farFuzzy, 0.0f, 1.1f, 0.002f);
	bk.AddSlider("Dof max fuzziness", &m_PostParams.m_maxFuzziness, 0.0f, 1.0f, 0.002f);
	bk.AddSlider("Motion blur scaler", &m_PostParams.m_motionScaler, 0.0f, 100.0f, 0.002f);
}
#endif // __BANK

void grPostFX::grPostFXEdgeEffect::StartSpuPostprocessing()
{
	// wait notification data
	volatile CellGcmNotifyData* pNotifyData = cellGcmGetNotifyDataAddress( 0);
	while( pNotifyData->zero != 0 )
	{
		sys_timer_usleep(30);
	}

	// reset notification data
	pNotifyData->zero = 0xdeadbeef;

	// make sure every edgepost task has ended
	while (s_CompletionCounter != EDGEPOST_SPU_NUM)
	{
		sys_timer_usleep(30);
	}

	// reset completion counter
	s_CompletionCounter = 0;

	// reset sync values for current frame
	m_Stages[m_NumStages - 1].flags |= EDGE_POST_WRITE_RSX_LABEL;
	m_Stages[m_NumStages - 1].rsxLabelAddress = (u32)cellGcmGetLabelAddress(s_EdgePostLabelIndex);
	m_Stages[m_NumStages - 1].rsxLabelValue = ++m_PostLabelValue;

	// setup camera
	m_PostParams.m_nearPlane = grcViewport::GetCurrent()->GetNearClip();
	m_PostParams.m_farPlane = grcViewport::GetCurrent()->GetFarClip();

	// keep values sensible
	m_PostParams.m_nearSharp = m_PostParams.m_nearSharp < m_PostParams.m_nearFuzzy ? m_PostParams.m_nearFuzzy : m_PostParams.m_nearSharp;
	m_PostParams.m_farSharp = m_PostParams.m_farSharp < m_PostParams.m_nearSharp ? m_PostParams.m_nearSharp : m_PostParams.m_farSharp;
	m_PostParams.m_farFuzzy = m_PostParams.m_farFuzzy < m_PostParams.m_farSharp ? m_PostParams.m_farSharp : m_PostParams.m_farFuzzy;

	// start SPU work
	edgePostInitializeWorkload(&m_Workload, &m_Stages[0], m_NumStages);

	// Send task signals
	for (int t = 0; t < EDGEPOST_SPU_NUM; ++t)
		cellSpursSendSignal(s_TaskSet, s_TaskIds[t]);
}

void grPostFX::grPostFXEdgeEffect::Process(grPostFX* postFX)
{
	// RSX wait for spu post processing to end
	cellGcmSetWaitLabel(GCM_CONTEXT, s_EdgePostLabelIndex, m_PostLabelValue);

	// Do this first since we want to keep the number of 2D <-> 3D RSX mode switches
	// to a minimum
	if (GRCDEVICE.GetMSAA())
	{
		GRCDEVICE.MsaaResolveBackToFrontBuffer();
	}

	grcTextureFactory& textureFactory = grcTextureFactory::GetInstance();
	grcRenderTarget* frontBuffer = textureFactory.GetFrontBuffer();
	CellGcmTexture* frontBufferGcmTex = reinterpret_cast<CellGcmTexture*>(frontBuffer->GetTexturePtr());
	CellGcmTexture* colorScratchGcmTex = reinterpret_cast<CellGcmTexture*>(s_ColorScratch->GetTexturePtr());
	CellGcmTexture* depthScratchGcmTex = reinterpret_cast<CellGcmTexture*>(s_DepthScratch->GetTexturePtr());

	// setup default states
	cellGcmSetBlendEnable(GCM_CONTEXT, CELL_GCM_FALSE);
	cellGcmSetBlendEnableMrt(GCM_CONTEXT, CELL_GCM_FALSE, CELL_GCM_FALSE, CELL_GCM_FALSE);
	cellGcmSetDepthTestEnable(GCM_CONTEXT, CELL_GCM_FALSE);
	cellGcmSetCullFaceEnable(GCM_CONTEXT, CELL_GCM_FALSE);
	cellGcmSetDepthMask(GCM_CONTEXT, CELL_GCM_FALSE);
	cellGcmSetColorMask(GCM_CONTEXT, CELL_GCM_COLOR_MASK_R | CELL_GCM_COLOR_MASK_G | CELL_GCM_COLOR_MASK_B | CELL_GCM_COLOR_MASK_A);
	cellGcmSetColorMaskMrt(GCM_CONTEXT, 0);

	//////////////////////////////////////////////////////////////////////////
	/// Move SPU results into VRAM and composite with original frame

	const u32 width = frontBuffer->GetWidth();
	const u32 height = frontBuffer->GetHeight();
	const u32 width2 = width / 2;
	const u32 height2 = height / 2;
	const u32 pitch2 = width2 * 4;

	// Move accumulator to VRAM (~0.06 ms)
	cellGcmSetTransferImage(GCM_CONTEXT, CELL_GCM_TRANSFER_MAIN_TO_LOCAL,
		colorScratchGcmTex->offset, colorScratchGcmTex->pitch, 0, 0, // destination
		m_LocalToMainOffset, pitch2, 0, 0, // source
		width2, height2, 4);

	//////////////////////////////////////////////////////////////////////////
	/// Move current frame into Host Memory for SPU input

	// Downsample depth/motion buffer (VRAM -> VRAM) (~0.5 ms)
	grcRenderTarget* depthBuffer = textureFactory.GetBackBufferDepth();
	u8 depthFormat = GRCDEVICE.PatchShadowToDepthBuffer(depthBuffer, false);
	postFX->BlitToRT(s_DepthScratch, depthBuffer, postFX->m_DepthDownSampleTechnique, postFX->m_DepthMapId);
	GRCDEVICE.PatchDepthToShadowBuffer(depthBuffer, depthFormat, false);

	// Move downsampled buffer to HOST (~ 0.11 ms)
	cellGcmSetTransferImage(GCM_CONTEXT, CELL_GCM_TRANSFER_LOCAL_TO_MAIN,
		m_DepthOffset, pitch2, 0, 0, // destination
		depthScratchGcmTex->offset, depthScratchGcmTex->pitch, 0, 0, // source
		width2, height2, 4);

	// setup scaling parameters (source)
	CellGcmTransferScale scale;
	scale.conversion = CELL_GCM_TRANSFER_CONVERSION_TRUNCATE;
	scale.format = CELL_GCM_TRANSFER_SCALE_FORMAT_A8R8G8B8;
	scale.operation = CELL_GCM_TRANSFER_OPERATION_SRCCOPY;
	scale.clipX = 0;
	scale.clipY = 0;
	scale.outX = 0;
	scale.outY = 0;
	scale.outW = scale.clipW = width2;
	scale.outH = scale.clipH = height2;
	scale.ratioX = cellGcmGetFixedSint32((float)frontBufferGcmTex->width / (float)width2);
	scale.ratioY = cellGcmGetFixedSint32((float)frontBufferGcmTex->height / (float)height2);
	scale.inW = frontBufferGcmTex->width;
	scale.inH = frontBufferGcmTex->height;
	scale.pitch = frontBufferGcmTex->pitch;
	// CellGcmTransferScale.origin can be set to either CELL_GCM_TRANSFER_ORIGIN_CENTER or
	// CELL_GCM_TRANSFER_ORIGIN_CORNER. Never use CELL_GCM_TRANSFER_ORIGIN_CORNER, It is
	// two time slower. No idea why.!
	scale.origin = CELL_GCM_TRANSFER_ORIGIN_CENTER;
	scale.interp = CELL_GCM_TRANSFER_INTERPOLATOR_FOH;
	scale.inX = cellGcmGetFixedUint16(0.5f);
	scale.inY = cellGcmGetFixedUint16(0.5f);
	scale.offset = frontBufferGcmTex->offset;

	// setup surface parameters (destination)
	CellGcmTransferSurface surface;
	surface.format = CELL_GCM_TRANSFER_SURFACE_FORMAT_A8R8G8B8;
	surface.pitch = pitch2;
	surface.offset = m_LocalToMainOffset;

	// Copy/Downsample color buffer (~ 0.24 ms)
	cellGcmSetTransferScaleMode(GCM_CONTEXT, CELL_GCM_TRANSFER_LOCAL_TO_MAIN, CELL_GCM_TRANSFER_SURFACE);
	cellGcmSetTransferScaleSurface(GCM_CONTEXT, &scale, &surface);

	// set transfer location back to local (as per gcm manual)
	cellGcmSetTransferLocation(GCM_CONTEXT, CELL_GCM_LOCATION_LOCAL);

	// setup notification stuff
	cellGcmSetNotifyIndex(GCM_CONTEXT, 0);
	cellGcmSetNotify(GCM_CONTEXT);
	cellGcmSetUserCommand(GCM_CONTEXT, (const u32)this);

	// Blend onto (previous) color buffer (~0.5 ms)
	cellGcmSetBlendEnable(GCM_CONTEXT, CELL_GCM_TRUE);
	cellGcmSetBlendEquation(GCM_CONTEXT, CELL_GCM_FUNC_ADD, CELL_GCM_FUNC_ADD);
	cellGcmSetBlendFunc(GCM_CONTEXT, CELL_GCM_ONE, CELL_GCM_ONE_MINUS_SRC_ALPHA, CELL_GCM_ONE, CELL_GCM_ONE);
	postFX->BlitToRT(frontBuffer, s_ColorScratch, postFX->m_SimpleCopyTechnique, postFX->m_ToneMapId);
	cellGcmSetBlendEnable(GCM_CONTEXT, CELL_GCM_FALSE);
}

} // namespace rage

#endif // __PPU
