#include "system/codefrag_spu.h"
#include "grpostfx/upsample.cpp"

SPUFRAG_DECL(void, upsamplespu, EdgePostTileInfo*);
SPUFRAG_IMPL(void, upsamplespu, EdgePostTileInfo* tileInfo)
{
	edgePostMain(tileInfo);
}
