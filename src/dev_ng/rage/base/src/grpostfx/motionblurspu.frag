#include "system/codefrag_spu.h"
#include "grpostfx/motionblur.cpp"

SPUFRAG_DECL(void, motionblurspu, EdgePostTileInfo*);
SPUFRAG_IMPL(void, motionblurspu, EdgePostTileInfo* tileInfo)
{
	edgePostMain(tileInfo);
}
