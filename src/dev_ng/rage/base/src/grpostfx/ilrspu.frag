#include "system/codefrag_spu.h"
#include "grpostfx/ilr.cpp"

SPUFRAG_DECL(void, ilrspu, EdgePostTileInfo*);
SPUFRAG_IMPL(void, ilrspu, EdgePostTileInfo* tileInfo)
{
	edgePostMain(tileInfo);
}
