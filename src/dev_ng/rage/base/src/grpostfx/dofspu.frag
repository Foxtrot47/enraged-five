#include "system/codefrag_spu.h"
#include "grpostfx/dof.cpp"
#include "edge/post/edgepost_dof_c.cpp"

SPUFRAG_DECL(void, dofspu, EdgePostTileInfo*);
SPUFRAG_IMPL(void, dofspu, EdgePostTileInfo* tileInfo)
{
	edgePostMain(tileInfo);
}
