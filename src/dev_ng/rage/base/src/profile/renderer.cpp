#include "renderer.h"

#include "math/amath.h"
#include "string/string.h"
#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/new.h"
#include "system/pix.h"

#if RSG_PROFILE_RENDERER

#if !defined(_MSC_VER) || _MSC_VER >= 1700

namespace rage {

sysCriticalSectionToken s_HandlerTable_CS;

static pfRenderInterface s_null_interface;
static pfRenderFactory s_null_factory;
static pfMemoryInterface s_memory_interface;

pfRenderInterface * g_pfRenderInterface = &s_null_interface;
pfRenderFactory * g_pfRenderFactory = &s_null_factory;
pfMemoryInterface * g_pfMemoryInterface = &s_memory_interface;

DEFINE_THREAD_PTR(debugDraw::BatchInterface, g_debugDraw);
DEFINE_THREAD_PTR(debugDraw::BatchInterface, g_debugDraw2D);
DEFINE_THREAD_PTR(debugDraw::BatchInterface, g_debugDraw3D);
DEFINE_THREAD_VAR(s8, g_debugDrawStack);


void pfRenderInterface::SetWorldMatrix(Mat34V_In) {
}

void pfRenderInterface::SetCameraMatrix(Mat34V_In) {
}

void pfRenderInterface::SetProjectionMatrix(Mat44V_In) {
}

void pfRenderInterface::SetViewport(const pfViewport&) {
}

void *pfRenderInterface::Begin(pfDrawType,unsigned,pfVertexType) {
	return NULL;
}

void pfRenderInterface::End() {
}

void pfRenderInterface::BeginFrame() {
}

void pfRenderInterface::EndFrame() {
}

void pfRenderInterface::PushDepthStencilState() {
}

void pfRenderInterface::PopDepthStencilState() {
}

void pfRenderInterface::SetDepthStencilState(pfDepthStencilState) {
}

void pfRenderInterface::PushRasterizerState() {
}

void pfRenderInterface::PopRasterizerState() {
}

void pfRenderInterface::SetRasterizerState(pfRasterizerState) {
}

void pfRenderInterface::PushBlendState() {
}

void pfRenderInterface::PopBlendState() {
}

void pfRenderInterface::SetBlendState(pfBlendState) {
}

void pfRenderInterface::DrawString(pfFont*,float,float,float,float,unsigned,const char *,unsigned) {
}

void pfRenderInterface::SetTexture(pfTexture*) {
}

void pfRenderInterface::DrawIndexedPrimtive(pfIndexedPrimitive *) {
}

void pfRenderInterface::SetConstantColor(unsigned) {
}

pfIndexedPrimitive* pfRenderFactory::CreateIndexedPrimitive(pfDrawType,const void *,size_t,pfVertexType,const u16 *,size_t) {
	return NULL;
}

pfTexture* pfRenderFactory::CreateTexture(const char *) {
	return NULL;
}

unsigned pfRenderFactory::GetScreenWidth() const {
	return 0;
}

unsigned pfRenderFactory::GetScreenHeight() const {
	return 0;
}

unsigned pfRenderFactory::GetFontHeight(pfFont*) const {
	return 0;
}

unsigned pfRenderFactory::GetStringWidth(pfFont*,const char*,size_t) const {
	return 0;
}

void* pfMemoryInterface::Allocate(size_t bytes) {
	return sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL)->Allocate(bytes,16);
}

void pfMemoryInterface::Free(void *ptr) {
	sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL)->Free(ptr);
}

namespace debugDraw {

bool Paused;
pfFont *FontProportional, *FontFixed, *FontTiny;

BatchInterface::BatchInterface(pfRenderInterface *ri) {
	m_FramesToLive = 0;
	m_World = m_Camera = Mat34V(V_IDENTITY);
	m_Projection = m_View = m_ViewProj = m_InverseViewProj = Mat44V(V_IDENTITY);
	m_Viewport.Set(0,0,(float)g_pfRenderFactory->GetScreenWidth(),(float)g_pfRenderFactory->GetScreenHeight());
	m_WorldStackCount = m_CameraStackCount = m_ProjectionStackCount = m_ViewportStackCount = 0;
	m_Color = ~0U;
	TrapZ(FontProportional != 0);
	m_Font = FontProportional;
	m_NormalX = m_NormalY = m_NormalZ = 0.0f;
	m_TexCoordS = m_TexCoordT = 0.0f;
	m_Current = m_Stop = NULL;
	m_Vertex3f = NULL;
	m_RenderInterface = ri;
	m_IsImmediate = false;
}

void BatchInterface::UpdateInternalMatrices() {
	InvertTransformOrtho((Mat34V&)m_View, m_Camera);
	Multiply(m_ViewProj, m_Projection, m_View);
	InvertFull(m_InverseViewProj, m_ViewProj);
}

static CallbackHandler s_Handlers[256];
static HandlerIndex s_NextHandler;

Header* BatchInterface::AddCommand(HandlerIndex &slot,CallbackHandler handler,u8 byteParam,size_t paramSize) {
	Header *h = m_Cursor.AddCommand(slot,handler,byteParam,paramSize);
	if (!paramSize && m_IsImmediate) {
		s_Handlers[h->index](this,h);
		m_Cursor.m_PageOffset--;
	}
	return h;
}

void BatchInterface::AddCommandAndPayload(HandlerIndex &index,CallbackHandler handler,u8 byteParam,size_t paramSize,const void *params) {
	unsigned origOffset = m_Cursor.m_PageOffset;
	if (Header *h = m_Cursor.AddCommand(index,handler,byteParam,paramSize)) {
		memcpy(h+1, params, paramSize);
		if (m_IsImmediate) {
			s_Handlers[h->index](this,h);
			m_Cursor.m_PageOffset = origOffset;
		}
	}
}

Header* BatchCursor::AddCommand(HandlerIndex &index,CallbackHandler handler,u8 byteParam,size_t paramSize) {
	u32 numSlots = (u32)(1 + (paramSize + 3) / 4);
	TrapGE(numSlots,Page::MaxCount-1);
	// Insufficient room in current page (or no page started yet)?
	if (m_PageOffset + numSlots > m_PageSize) {
		// Add a terminating command to previous page.
		if (m_PageOffset)
			m_CurrentPage->header[m_PageOffset].index = 0;

		// Attempt to allocate a new page
		void *result = g_pfMemoryInterface->Allocate(sizeof(Page));
		if (!result)
			return NULL;

		// Chain previous page (or head of list) to this new page; reserve enough space for a pointer at the start of this page
		if (m_CurrentPage)
			m_CurrentPage->next = (Page*)result;
		else
			m_FirstPage = (Page*)result;
		m_CurrentPage = (Page*)result;
		m_CurrentPage->next = 0;

		m_PageOffset = 0;
		// Always leave one extra command so we can terminate the list
		m_PageSize = Page::MaxCount-1;
	}

	if (!index) {
		SYS_CS_SYNC(s_HandlerTable_CS);
		// Re-check the value after getting the critsec in case two threads got here at the same time.
		if (!index) {
			if (!++s_NextHandler)
				Quitf("Too many handlers, make HandlerIndex a bigger type");
			s_Handlers[index = s_NextHandler] = handler;
		}
	}

	Header *h = m_CurrentPage->header + m_PageOffset;
	h->index = index;
	h->byteParam = byteParam;
	Assign(h->paramCount,numSlots-1);

	m_PageOffset += numSlots;

	return h;
}

void BatchCursor::Flush() {
	Page *p = m_FirstPage;
	while (p) {
		Page *nextP = p->next;
		g_pfMemoryInterface->Free(p);
		p = nextP;
	}

	m_FirstPage = m_CurrentPage = NULL;
	m_PageOffset = m_PageSize = 0;
}


void BatchCursor::Process(BatchInterface *batch,BatchCursor *next) {
	Page *p = m_FirstPage;

	// Make sure that if there's a list, it's terminated.
	if (m_CurrentPage)
		m_CurrentPage->header[m_PageOffset].index = 0;

	// Clear the list now if we're not using it again
	if (next) {
		m_FirstPage = m_CurrentPage = NULL;
		m_PageOffset = m_PageSize = 0;
	}

	while (p) {
		Page *nextP = p->next;
		Header *h = p->header;

		// Process all commands in this page, stopping at NUL terminator.
		while (h->index) {
			// Handle the command
			s_Handlers[h->index](batch,h);

			// If this should persist for longer than a frame, copy it to the next frame now.
			// Do this after invoking the handler so that it can adjust the framesToLive if it wants.
			if (next && batch->m_FramesToLive) {
				if (Header *h2 = next->AddCommand(h->index,NULL,h->byteParam,h->paramCount*4))
					memcpy(h2+1, h+1, h->paramCount*4);
			}

			h += 1 + h->paramCount;
		}

		// Free this page
		if (next)
			g_pfMemoryInterface->Free(p);
		// Chain to next pag�e
		p = nextP;
	}
}

void BatchInterface::Process(BatchCursor *next) {
	PF_CPU_AUTO_TAG("BatchInterface::Process (Debug rendering)");
	PF_GPU_AUTO_TAG("BatchInterface::Process (Debug rendering)");

	m_FramesToLive = 0;

	// Make sure the constant color is set to a known good value since it's lazy-stated.
	m_RenderInterface->SetConstantColor(0);
	m_RenderInterface->SetConstantColor(~0U);
	m_RenderInterface->BeginFrame();

	m_Persistent.Process(this,next);
	m_Cursor.Process(this,next);

	AssertMsg(!m_WorldStackCount,"Somebody left a world matrix on the stack");
	AssertMsg(!m_CameraStackCount,"Somebody left a camera matrix on the stack");
	AssertMsg(!m_ProjectionStackCount,"Somebody left a projection matrix on the stack");
	m_WorldStackCount = m_CameraStackCount = m_ProjectionStackCount = 0;

	m_RenderInterface->EndFrame();
}

void BatchInterface::Flush() {
	m_Cursor.Flush();
	m_WorldStackCount = m_CameraStackCount = m_ProjectionStackCount = 0;
}


static VertexHandler s_VertexHandlers[8] = {
	&BatchInterface::Vertex3f_Pos,
	&BatchInterface::Vertex3f_PosColor,
	&BatchInterface::Vertex3f_PosNormal,
	&BatchInterface::Vertex3f_PosColorNormal,
	&BatchInterface::Vertex3f_PosTexCoord,
	&BatchInterface::Vertex3f_PosColorTexCoord,
	&BatchInterface::Vertex3f_PosNormalTexCoord,
	&BatchInterface::Vertex3f_PosColorNormalTexCoord,
};


#define IMPL_CALLBACK(_name,_type,_size) \
static void _name(BatchInterface *b,Header *h) { \
	if (void *v = b->m_RenderInterface->Begin((pfDrawType)h->byteParam,h->paramCount*4/_size,_type)) { \
		memcpy(v,h+1,h->paramCount * 4); \
		b->m_RenderInterface->End(); \
	} \
}

IMPL_CALLBACK(Callback_Pos,pfPos,12)
IMPL_CALLBACK(Callback_PosColor,pfPosColor,16)
IMPL_CALLBACK(Callback_PosNormal,pfPosNormal,24)
IMPL_CALLBACK(Callback_PosColorNormal,pfPosColorNormal,28)
IMPL_CALLBACK(Callback_PosTexCoord,pfPosTexCoord,20)
IMPL_CALLBACK(Callback_PosColorTexCoord,pfPosColorTexCoord,24)
IMPL_CALLBACK(Callback_PosNormalTexCoord,pfPosNormalTexCoord,32)
IMPL_CALLBACK(Callback_PosColorNormalTexCoord,pfPosColorNormalTexCoord,36)

u8 BatchInterface::sm_VertexSizes[]= { 12,16,24,28,20,24,32,36 };

#undef IMPL_CALLBACK

static CallbackHandler s_CallbackHandlers[8] = {
	Callback_Pos,Callback_PosColor,
	Callback_PosNormal,Callback_PosColorNormal,
	Callback_PosTexCoord,Callback_PosColorTexCoord,
	Callback_PosNormalTexCoord,Callback_PosColorNormalTexCoord,
};

void BatchInterface::Begin(pfDrawType dt,unsigned vc,pfVertexType vt) {
	static u8 indices[8];
	if (m_IsImmediate)
		m_Current = (float*) m_RenderInterface->Begin(dt,vc,vt);
	else
		m_Current = (float*) (AddCommand(indices[vt],s_CallbackHandlers[vt],(u8)dt,vc * sm_VertexSizes[vt])+1);
	m_Vertex3f = s_VertexHandlers[vt];
	if (m_Current)
		m_Stop = m_Current + vc * (sm_VertexSizes[vt] / 4);
	else
		m_Stop = NULL;
}


void BatchInterface::Vertex3f_Pos(float x,float y,float z) {
	if (m_Current < m_Stop) {
		m_Current[0] = x;
		m_Current[1] = y;
		m_Current[2] = z;
		m_Current += 3;
	}
}

void BatchInterface::Vertex3f_PosColor(float x,float y,float z) {
	if (m_Current < m_Stop) {
		m_Current[0] = x;
		m_Current[1] = y;
		m_Current[2] = z;
		*(unsigned*)(m_Current+3) = m_Color;
		m_Current += 4;
	}
}

void BatchInterface::Vertex3f_PosNormal(float x,float y,float z) {
	if (m_Current < m_Stop) {
		m_Current[0] = x;
		m_Current[1] = y;
		m_Current[2] = z;
		m_Current[3] = m_NormalX;
		m_Current[4] = m_NormalY;
		m_Current[5] = m_NormalZ;
		m_Current += 6;
	}
}

void BatchInterface::Vertex3f_PosColorNormal(float x,float y,float z) {
	if (m_Current < m_Stop) {
		m_Current[0] = x;
		m_Current[1] = y;
		m_Current[2] = z;
		*(unsigned*)(m_Current+3) = m_Color;
		m_Current[4] = m_NormalX;
		m_Current[5] = m_NormalY;
		m_Current[6] = m_NormalZ;
		m_Current += 7;
	}
}

void BatchInterface::Vertex3f_PosTexCoord(float x,float y,float z) {
	if (m_Current < m_Stop) {
		m_Current[0] = x;
		m_Current[1] = y;
		m_Current[2] = z;
		m_Current[3] = m_TexCoordS;
		m_Current[4] = m_TexCoordT;
		m_Current += 5;
	}
}

void BatchInterface::Vertex3f_PosColorTexCoord(float x,float y,float z) {
	if (m_Current < m_Stop) {
		m_Current[0] = x;
		m_Current[1] = y;
		m_Current[2] = z;
		*(unsigned*)(m_Current+3) = m_Color;
		m_Current[4] = m_TexCoordS;
		m_Current[5] = m_TexCoordT;
		m_Current += 6;
	}
}

void BatchInterface::Vertex3f_PosNormalTexCoord(float x,float y,float z) {
	if (m_Current < m_Stop) {
		m_Current[0] = x;
		m_Current[1] = y;
		m_Current[2] = z;
		m_Current[3] = m_NormalX;
		m_Current[4] = m_NormalY;
		m_Current[5] = m_NormalZ;
		m_Current[6] = m_TexCoordS;
		m_Current[7] = m_TexCoordT;
		m_Current += 8;
	}
}

void BatchInterface::Vertex3f_PosColorNormalTexCoord(float x,float y,float z) {
	if (m_Current < m_Stop) {
		m_Current[0] = x;
		m_Current[1] = y;
		m_Current[2] = z;
		*(unsigned*)(m_Current+3) = m_Color;
		m_Current[4] = m_NormalX;
		m_Current[5] = m_NormalY;
		m_Current[6] = m_NormalZ;
		m_Current[7] = m_TexCoordS;
		m_Current[8] = m_TexCoordS;
		m_Current += 9;
	}
}

}	// debugDraw

}	// rage

#endif	// DevStudio 2012 or better

#endif	// RSG_PROFILE_RENDERER
