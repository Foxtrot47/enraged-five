//
// profile/log.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PROFILE_LOG_H
#define PROFILE_LOG_H

/////////////////////////////////////////////////////////////////
// external defines

#include "diag/stats.h"

#if __STATS

namespace rage {

class bkBank;
class pfTimer;
class fiStream;
class pfPage;
class pfValue;
class bkCombo;


/////////////////////////////////////////////////////////////////
// pfLogMgr
// PURPOSE
//   Manager for the LOG views.  Controls selecting the page to view
//   and passing it to the viewer when needed.
//
// <FLAG Component>
//
class pfLogMgr
{
public:
	static void Init ();										// class init
	static void Shutdown ();									// class shutdown

	static void Enable(bool);

	// page selection
	static pfPage *NextPage ();								// select the next page from the pfProfiler
	static pfPage *PrevPage ();								// select the previous page from the pfProfiler
	static void SetPage (pfPage *page);						// set the current page
	static void SetPageByName (const char *pageName);		// set the current page using the name to search

	static void Update (bool paused=false);						// collect/write data
	static void WriteAnnotation (const char * string);			// output an annotation to the open sink (public)
	static void Flush();

	// output functions
	static void SetFile (const char * filename);				// set the filename for file output (lazy file opening)
	static void OpenFile (const char * filename);				// open an output file
	static void CloseFile ();									// close the currently open file

	#if __BANK
	static void LogCB ();
	static void AddWidgets (bkBank & bank);						// add control widgets
	#endif

	static void SetUseCustomElements(bool b);
	static void SetCustomTimers(atArray<pfTimer*>* timers);
	static void SetCustomValues(atArray<pfValue*>* values);
	static void SetWriteHeader(bool b);

	enum {MaxFilenameDisplayed=64};								// length of the filename to display
	enum {MaxFilename=256};										// maximum length of stored filename for lazy opening

protected:
	static bool Enabled;										// is the log class enabled?

	static pfPage * CurrentPage;								// the page currently being displayed
	static char CurrentPageName[];								// the name of the current page

	static bool UseCustomLog;
	static atArray<pfTimer*>* CustomTimers;
	static atArray<pfValue*>* CustomValues;

	static bool OutputActive;									// does there exist a valid output object
	static bool FileWaitingToOpen;								// is there a filename set but not opened?
	static char WaitingFileName[];								// the name of the output file waiting to be opened
	static char CurrentFileNameDisplay[];						// the name of the output file
	static fiStream * CurrentStream;								// the current stream being written to

	static void WriteHeader ();									// output data column header info
	static void WriteData ();									// output data snapshot from current page
	static bool ShouldWriteHeader;								// the page contents have changed, so write a header

	static void WriteSample (float sample);						// write a single data point
	static void WriteEOL ();									// write EOL

	// Page Selection Combo Box
#if __BANK
	static void SelectPageComboCb ();							// select the page from the combo box
	static int sm_ComboPageSelection;							// current page selection in combo box
	static bkCombo * sm_SelectionPageCombo;						// combo box for page selection
#endif
};

}	// namespace rage

#endif // __STATS

#endif // !PROFILE_EKG_H
