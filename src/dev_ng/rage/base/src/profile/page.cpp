//
// profile/page.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "bank/bank.h"
#include "string/string.h"
#include "profile/group.h"
#include "profile/pfpacket.h"
#include "profile/page.h"
#include "profile/profiler.h"

using namespace rage;


#if __STATS

pfPage::pfPage (const char * name)
{
	m_NumGroups = 0;

	Assert(name);
	strncpy(m_Name,name,kNameMaxLen-1);
	m_Name[kNameMaxLen-1] = 0;

#if __BANK
	m_id = pfPacket::GetNewId();
	m_refCount = 0;

	AddToRagProfiler();
#endif

	m_TreeNode.SetKey(m_Name);
	m_TreeNode.SetData(this);
	GetRageProfiler().RegisterPage(m_TreeNode);

	isActive = true;
}


pfPage::~pfPage ()
{
#if __BANK
	if ( pfPacket::IsConnected() )
	{
		pfPacket packet;
		packet.Begin( pfPacket::DESTROY_PAGE, 0, 0 );
		packet.Write_u32( m_id );
		packet.Send();
	}

	for ( int i = 0; i < m_NumGroups; ++i )
	{
		m_Groups[i]->RemovePage( this );
	}
#endif

	GetRageProfiler().UnregisterPage(m_TreeNode);
}


void pfPage::AddGroup (pfGroup * group)
{
	if (m_NumGroups<kMaxGroups)
	{
		m_Groups[m_NumGroups++] = group;
	}

#if __BANK
	group->AddPage( this );
#endif
}


#if __BANK
#include <stdio.h>

void pfPage::AddWidgets (bkBank & bank)
{
	char temp[32 + kNameMaxLen];

	sprintf(temp,"page - %s",m_Name);
	bank.PushGroup(temp,false);

	for (int i=0; i<m_NumGroups; i++)
	{
		m_Groups[i]->AddWidgets(bank);
	}

	bank.PopGroup();
}

void pfPage::AddTraceWidgets(bkBank& bank)
{
	char temp[32 + kNameMaxLen];
	formatf(temp, 32 + kNameMaxLen, "page - %s", m_Name);
	bank.PushGroup(temp, false);

	for(int i = 0; i < m_NumGroups; i++)
	{
		m_Groups[i]->AddTraceWidgets(bank);
	}
	bank.PopGroup();
}

void pfPage::AddToRagProfiler()
{
	if ( pfPacket::IsConnected() )
	{
		pfPacket packet;
		packet.Begin( pfPacket::CREATE_PAGE, 0, 0 );
		packet.Write_u32( m_id );
		packet.Write_const_char( m_Name );
		packet.Send();

		for ( int i = 0; i < m_NumGroups; ++i )
		{
			m_Groups[i]->AddToRagProfiler();

			pfPacket linkPacket;
			linkPacket.Begin( pfPacket::LINK_GROUP_TO_PAGE, 0, 0 );
			linkPacket.Write_u32( m_Groups[i]->GetId() );
			linkPacket.Write_u32( GetId() );
			linkPacket.Send();
		}
	}
}
#endif


pfGroup * pfPage::GetNextGroup (const pfGroup * curGroup)
{
	Assert(kMaxGroups>=m_NumGroups);

	if (curGroup==NULL)
	{
		if (m_NumGroups>=0)
		{
			return m_Groups[0];
		}
		else
		{
			return NULL;
		}
	}

	for (int i=0; i<m_NumGroups; i++)
	{
		if (m_Groups[i]==curGroup)
		{
			if (i<m_NumGroups-1)
			{
				return m_Groups[i+1];
			}
			else
			{
				return NULL;
			}
		}
	}

	return NULL;
}

//==============================================================

pfPageLink::pfPageLink(pfPage * page, pfGroup * group)
{
	page->AddGroup( group );

#if __BANK
	if ( pfPacket::IsConnected() )
	{
		pfPacket packet;
		packet.Begin( pfPacket::LINK_GROUP_TO_PAGE, 0, 0 );
		packet.Write_u32( group->GetId() );
		packet.Write_u32( page->GetId() );
		packet.Send();
	}
#endif
}

#endif
