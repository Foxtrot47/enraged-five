#include "cputrace.h"
#if RAGETRACE
#include "cellschedulertrace.h"
#if __PPU
#include <sys/ppu_thread.h>
#endif
#include "vector/colors.h"
#include "grcore/im.h"

#include <stdio.h>

namespace rage {

bool g_pfEnableCpuTrace = true;
__THREAD pfCpuThreadTrace* g_pfCpuThreadTrace = 0;

//////////////////////////////////////////////////////////////////////////

pfCpuThreadTraceInitialiser::pfCpuThreadTraceInitialiser(const char* name, u32 sizeKb, u32 depth)
:	m_RootId(name, Color32(0))
,	m_sizeKb(sizeKb)
,	m_depth(depth)
,	m_rpCpuTrace(g_pfCpuThreadTrace)
{
#if __PPU
	// need PPU thread id to correlate against cell scheduler
	sys_ppu_thread_t id;
	sys_ppu_thread_get_id(&id);
	m_ThreadId = id;
#endif	
	RageTrace().AddInitialiser(this);
}
pfCpuThreadTraceInitialiser::~pfCpuThreadTraceInitialiser()
{
	RageTrace().RemoveInitialiser(this);
}
void pfCpuThreadTraceInitialiser::Enable()
{
	RageTrace().AddTrace(m_rpCpuTrace = rage_new pfCpuThreadTrace(&m_RootId, m_sizeKb*1024, m_ThreadId, m_depth));
}
void pfCpuThreadTraceInitialiser::Disable()
{
	delete m_rpCpuTrace;
	m_rpCpuTrace = 0;
}

//////////////////////////////////////////////////////////////////////////

pfCpuThreadTrace::pfCpuThreadTrace(pfTraceCounterId* rootId, u32 bufsize, u32 threadid, u32 numrows)
:	pfBufferedTrace(rootId->m_Name, bufsize, rage_new pfCpuTraceCounter(rootId))
,	m_ThreadId(threadid)
{
	m_NumRows = numrows + 1;
	m_LastActive = 0;
}

void pfCpuThreadTrace::AdvanceTo(u32 timebase)
{
#if __PPU
	bool active = g_pfCellSchedulerTrace ? g_pfCellSchedulerTrace->IsThreadActive(m_ThreadId) : true;
#else
	bool active = true;
#endif
	if (!active)
	{
		if (m_LastActive)
		{
			// render the busy time bar
			RageTrace().RenderBar(m_LastActive, timebase, Color_green, 0);
			m_LastActive = 0;
		}
		// update the active counter's idle count
		if (m_ActiveCounter)
			static_cast<pfCpuTraceCounter*>(m_ActiveCounter)->m_IdleCount += timebase - m_LastTimeBase;
	}
	else if (!m_LastActive)
	{
		m_LastActive = timebase|1;
	}
	m_LastTimeBase = timebase;
	pfBufferedTrace::AdvanceTo(timebase);
}

//////////////////////////////////////////////////////////////////////////

void pfCpuTraceCounter::OnPush(u32 timebase)
{
	pfTraceCounter::OnPush(timebase);
	m_IdleCount = 0;
}

void pfCpuTraceCounter::OnPop(u32 timebase)
{
	double idle = double(m_IdleCount) * sysTimer::GetTicksToMicroseconds();
	m_IdleTime.Add(idle);	

	if (m_Parent)
		static_cast<pfCpuTraceCounter*>(m_Parent)->m_IdleCount += m_IdleCount;
	s32 delta = timebase - m_StartTime - m_IdleCount;
	if (delta > 0)
	{
		double t = double(delta) * sysTimer::GetTicksToMicroseconds();
		m_Counter.Add(t);
		m_Id->m_Counter.Add(t);
	}
}

void pfCpuTraceCounter::Dump()
{
// 	float pct = PercentageOfParent();
// 	if (pct < 1.0f || m_Time.Sum() < 50*32)
// 		return;
// 	float scale = sysTimer::GetTicksToMicroseconds() * (1.0f / 32.0f);
// 	int busy = (int)((m_Time.Sum() - m_IdleTime.Sum()) * scale);	
// 	int idle = (int)(m_IdleTime.Sum() * scale);
// 	const char* spaces = "                                            ";
// 	Displayf("%*.*s[%5.2f%%][%8i][%8i][%6.1f] : %s", m_Layer, m_Layer, spaces, 
// 		pct, busy, idle, m_Time.NumSamples() * (1.0f / 32.0f), (const char*)m_Id);
// 	DumpChildren();
}

void pfCpuTraceCounter::Reset()
{
	m_IdleTime.Reset();
	pfTraceCounter::Reset();
}

void pfCpuTraceCounter::EndFrame()
{
	m_IdleTime.EndFrame();
	pfTraceCounter::EndFrame();
}

void pfCpuTraceCounter::DisplayStats(float x, float y)
{
	pfCpuTraceCounter* parent = static_cast<pfCpuTraceCounter*>(m_Parent);
	float parentbusy = parent ? parent->m_Counter.FrameTime() : 0;
	float parentidle = parent ? parent->m_IdleTime.FrameTime() : 0;
	float parenttotal = parentbusy + parentidle;
	float busy = m_Counter.FrameTime();
	float idle = m_IdleTime.FrameTime();
	float total = busy + idle;
	float busyfrac = total ? busy / total : 0.0f;
	float pct = parenttotal ? total * 100.0f / parenttotal : 100.0f;
	char timestr[128];
	sprintf(timestr, "%6i %6i %6.2f %6.1f%% %s", (int)busy, (int)idle, 
		m_Counter.FrameCalls(), pct, m_Id->m_Name);
	Color32 col = m_Id->m_Color;
	col.SetAlpha(255);
	RageTrace().RenderQuad(x, y, x + 8, y + 8, col);
	x += 12;
	RageTrace().RenderQuad(x, y, x + pct * busyfrac, y + 8, col);	
	RageTrace().RenderQuad(x + pct * busyfrac, y + 6, x + pct, y + 8, col);	
	grcDraw2dText(x + 92, y, timestr);	
}

} // namespace rage
#endif // RAGETRACE
