// 
// profile/pfpacket.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PROFILE_PFPACKET_H
#define PROFILE_PFPACKET_H

#include "diag/stats.h"
#include "system/namedpipe.h"
#include "system/pipepacket.h"

#if __BANK && __STATS

namespace rage
{

class sysCompressionFactory;

#define USE_PFPACKET_SEMA 0

class pfPacket : public sysPipePacket
{
public:
	pfPacket();

	enum ECommands
	{
		COUNT,
		CREATE_PAGE,
		DESTROY_PAGE,
		CREATE_GROUP,
		DESTROY_GROUP,		
		CREATE_TIMER,
		DESTROY_TIMER,
		CREATE_VALUE,
		DESTROY_VALUE,
		CREATE_ARRAY,
		DESTROY_ARRAY,
		CREATE_ARRAY_VALUE,
		DESTROY_ARRAY_VALUE,
		LINK_GROUP_TO_PAGE,
		UPDATE_TIMER,
		UPDATE_VALUE,
		PAGE_REF_COUNT
	};

	virtual void Send();

	static bool Connect( bool waitForPipe );

	static bool IsConnected();

	static void Close();

	static bool HaveDataToSend() { return sm_writeBufferLength > 0; }

	static void ReceivePackets();

	static void SendPackets();

	static u32 GetNewId();

private:
	static void ResetMessageBuffer();

	static sysCompressionFactory* sm_pCompressionFactory;
	enum { MAX_WRITE_BUFFER_LENGTH = 8 * 1024 };
	static u8 sm_writeBuffer[MAX_WRITE_BUFFER_LENGTH];
	static int sm_writeBufferLength;
	static sysNamedPipe sm_pipe;
	static u32 sm_messageCount;
	static bool sm_isConnected;
	static u32 sm_id;

#if USE_PFPACKET_SEMA
	static sysIpcMutex sm_mutex;
#endif
};

}

#endif // __BANK && __STATS

#endif // PROFILE_PFPACKET_H
