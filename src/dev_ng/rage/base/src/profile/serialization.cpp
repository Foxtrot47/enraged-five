#include "settings.h"

#if USE_PROFILER

#include "file/asset.h"
#include "system/stl_wrapper.h"

#include "common.h"
#include "serialization.h"
#include "system/memory.h"

namespace Profiler
{
	#define DEFAULT_SERIALIZATION stream.Write( (const rage::u8*)&val, (rage::u32)sizeof(val) )

	OutputDataStream &operator << ( OutputDataStream &stream, const char* val )
	{
		rage::u32 length = val == nullptr ? 0 : (rage::u32)strlen(val);
		stream << length;

		if (length > 0)
		{
			stream.Write((const rage::u8*)val, length);
		}
		return stream;
	}

	OutputDataStream &operator << ( OutputDataStream &stream, rage::s8 val )
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream &operator << ( OutputDataStream &stream, rage::u8 val )
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream &operator << (OutputDataStream &stream, rage::s16 val)
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream &operator << (OutputDataStream &stream, rage::u16 val)
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream &operator << ( OutputDataStream &stream, rage::s32 val )
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream &operator << ( OutputDataStream &stream, rage::s64 val )
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream &operator << ( OutputDataStream &stream, char val )
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream & operator<<(OutputDataStream &stream, rage::u64 val)
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream & operator<<(OutputDataStream &stream, rage::u32 val)
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream & operator<<(OutputDataStream &stream, float val)
	{
		DEFAULT_SERIALIZATION;
		return stream;
	}

	OutputDataStream & operator<<(OutputDataStream &stream, const rage::Vec3V& val)
	{
		return stream << val.GetXf() << val.GetYf() << val.GetZf();
	}

	OutputDataStream & operator<<(OutputDataStream &stream, const rage::stlString& val)
	{
		stream << (rage::u32)val.size();
		if (!val.empty())
			stream.Write((const rage::u8*)&val[0], (rage::u32)(sizeof(val[0]) * val.size()));
		return stream;
	}

	OutputDataStream & operator<<(OutputDataStream &stream, const rage::stlWstring& val)
	{
		size_t count = val.size() * sizeof(wchar_t);
		stream << (rage::u32)count;
		if (!val.empty())
			stream.Write((const rage::u8*)(&val[0]), (rage::u32)count);
		return stream;
	}

	OutputDataStream & operator<<(OutputDataStream &stream, const Image& val)
	{
		rage::s32 fileSize = 0;
		rage::u8* imageBuffer = nullptr;

		if (rage::fiStream* imageStream = rage::ASSET.Open(val.path, ""))
		{
			rage::sysMemAutoUseDebugMemory debugMem;

			fileSize = imageStream->Size();
			if (fileSize >= 0)
			{
				imageBuffer = rage_new rage::u8[fileSize];	

				imageStream->Read(imageBuffer, fileSize);
				imageStream->Close();
			}
		}

		stream << fileSize;

		if (imageBuffer)
		{
			stream.Write(imageBuffer, fileSize);

			rage::sysMemAutoUseDebugMemory debugMem;
			delete[] imageBuffer;
		}
		
		return stream;
	}
}

#endif //USE_PROFILER
