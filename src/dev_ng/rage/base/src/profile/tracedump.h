// 
// profile/tracedump.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PROFILE_TRACEDUMP_H
#define PROFILE_TRACEDUMP_H

#include "input/pad.h"

namespace rage
{

/*	PURPOSE
		A convenience wrapper around the XTraceStartRecording and XTraceStopRecording functions in the
		Xbox360 XDK.  Tracing is triggered by a debug button on the controller.

		To use the class, simply create an instance.  Then call Start right before executing the code 
		you want to profile then call Stop immediately after the code you want to profile.  You can also
		optionally call Break to break into the debugger after the trace is finished.

		At runtime, when you want to trace the block of code, press the debug button you have associated
		to the trace dump.  This is R1 on controller 0 by default.

		Example:
			pfTraceDump trace;
			trace.Start();
			< slow code here >
			trace.Stop();
			trace.Break();

		This code will run normaly until you press the debug button.  For the frame that you pressed the debug button,
		it will execute a trace and dump the results to the specified trace file.  The results can be parsed
		and read with tracedump in the xdk.  You can also use xenon_trace_dump.bat in rage\base\bin to do the copy
		work for you.
*/
class pfTraceDump
{
public:
	pfTraceDump();
	~pfTraceDump();

	//	PURPOSE:  Start tracing if the trace button is pressed
	void Start() const;

	//	PURPOSE: Stop tracing if the trace button is pressed
	void Stop() const;

	//	PURPOSE: Break into the debugger if the trace button is pressed
	void Break() const;

	//	PURPOSE: Set the trace button
	//	PARAMS:
	//		traceButton - The debug button to use for tracing.  This defaults to ioPad::R1
    void SetTraceButton(unsigned int traceButton);

	//	PURPOSE: Set the trace output file
	//	PARAMS:
	//		traceFile - the hardware path of the trace output file. This defaults to "game:\\trace.bin"
	void SetTraceFile(const char* traceFile);

	//	PURPOSE: Get the trace button
	//	RETURNS: An unsigned int representing the button that will execute a trace
	unsigned int GetTraceButton() const;

	//	PURPOSE: Get the trace file
	//	RETURNS: A null terminated string containing the trace output file
	const char*	GetTraceFile() const;

protected:
	unsigned int	m_TraceButton;
	const char*		m_TraceFile;
};

// Cross platform version of pfTraceDump that doesn't rely on button presses
// For Xenon it saves the trace results to a .bin file on the xenon's harddrive,
// for PS3 it will trigger a capture if the game was run with Tuner.
class pfTraceDump2
{
public:
	static void Start();

	static void Stop();

	//	PURPOSE: Set the trace output file
	//	PARAMS:
	//		traceFile - the hardware path of the trace output file. This defaults to "game:\\trace.bin"
	static void SetTraceFile(const char* traceFile);

	//	PURPOSE: Get the trace file
	//	RETURNS: A null terminated string containing the trace output file
	static const char*	GetTraceFile();

protected:
	static const char* m_TraceFile;
};


} // namespace rage

#endif // PROFILE_TRACEDUMP_H
