# Converts from a simple format, t/Page/Group/Timer or v/Page/Group/Timer (allowing '#' comments and blank lines)
# to the XML format we use for loading

import sys
inFile = file(sys.argv[1], "r")
outFile = file(sys.argv[2], "w")

outFile.write("<CustomElements>\n")
for line in inFile:
	sline = line.strip()
	if len(sline) == 0 or sline[0] == '#':
		continue
	items = sline.split("/")
	if items[0] == "t":
		outFile.write("<Item><Page>%s</Page><Group>%s</Group><Timer>%s</Timer></Item>\n" % (items[1], items[2], items[3]))
	else:
		outFile.write("<Item><Page>%s</Page><Group>%s</Group><Value>%s</Value></Item>\n" % (items[1], items[2], items[3]))
outFile.write("<CustomElements>\n")
outFile.close()

