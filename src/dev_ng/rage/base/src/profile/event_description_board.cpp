#include "settings.h"

#if USE_PROFILER

//Rage
#include "system/criticalsection.h"


#include "event_description_board.h"
#include "event.h"

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SetEventFlag( int index, rage::u8 mask, bool flag )
{ 
	EventDescriptionBoard& board = EventDescriptionBoard::Get();
	
	SYS_CS_SYNC( board.sm_csBoardToken );
	
	auto& events = board.GetEvents();
	if (!rockyVerifyf(index < (int)events.size(), "Invalid EventDescription index"))
		return;

	if (index < 0)
	{
		for (int i = 0; i < events.GetCount(); ++i)
			events[i]->flags = (events[i]->flags & ~mask) | (flag ? mask : 0);
	} else
	{
		events[index]->flags = (events[index]->flags & ~mask) | (flag ? mask : 0);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool HasSamplingEvents()
{
	EventDescriptionBoard& board = EventDescriptionBoard::Get();
	
	SYS_CS_SYNC( board.sm_csBoardToken );

	auto& events = board.GetEvents();
	for (int i = 0; i < events.GetCount(); ++i)
		if (events[i]->IsSampling() && !events[i]->IsResource())
			return true;

	return false;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER
