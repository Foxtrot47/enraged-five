#include "cellspurstrace.h"
#include "system/timer.h"

#if __PPU && RAGETRACE

namespace rage {

pfSpuTotalTrace* g_pfSpuTotals = 0;
// pfSpuTrace* g_pfSpuTrace[6]  = {0,0,0,0,0,0};

void pfSpuTrace::OnEndFrame()
{
	// update SPU totals	
	UpdateTotals(Root(), g_pfSpuTotals->Root());
}

void pfSpuTrace::UpdateTotals(pfTraceCounter* spu, pfTraceCounter* total)
{
	total->m_Counter.m_Frame.Add(spu->m_Counter.m_Frame);
	for(atMap<pfTraceCounterId*, pfTraceCounter*>::Iterator iter = spu->m_Children.CreateIterator();
		iter; ++iter)
	{
		UpdateTotals(*iter, total->Child(iter.GetKey()));
	}	
}

// should probably reinstate some of this to deal with spurs tasks:

// pfCellSpursTrace::pfCellSpursTrace(CellSpurs* spurs, u32 firstSpu, u32 numSpus, u32 maxTracePacketsPerSpu)
// :	m_Spurs(spurs)
// ,	m_SpuTraces(0, numSpus)
// {
// 	u32 spursTraceBufferSize = maxTracePacketsPerSpu * numSpus * sizeof(CellSpursTracePacket) + sizeof(CellSpursTraceInfo);
// 	m_SpursTraceBuffer = (CellSpursTraceInfo*)memalign(256, spursTraceBufferSize);
// 	memset(m_SpursTraceBuffer, 0xCD, spursTraceBufferSize);
// 	cellSpursTraceInitialize(spurs, m_SpursTraceBuffer, 
// 		spursTraceBufferSize, CELL_SPURS_TRACE_MODE_FLAG_WRAP_BUFFER);
// 	cellSpursTraceStart(spurs);
// 	CellSpursTracePacket* buf = (CellSpursTracePacket*)(m_SpursTraceBuffer + 1);
// 	for(u32 i=0; i<numSpus; ++i)
// 	{
// 		char name[32];
// 		sprintf(name, "SPU%i", i + firstSpu);
// 		pfCellSpuTrace* trace = rage_new pfCellSpuTrace(name, i, buf, maxTracePacketsPerSpu);
// 		m_SpuTraces.Push(trace);
// 		RageTrace().AddTrace(trace);
// 		buf += maxTracePacketsPerSpu;
// 	}
// }
// 
// pfCellSpursTrace::~pfCellSpursTrace()
// {	
// 	for(atArray<pfCellSpuTrace*>::iterator it = m_SpuTraces.begin(); it != m_SpuTraces.end(); ++it)
// 		delete *it;
// 	cellSpursTraceStop(m_Spurs);
// 	free(m_SpursTraceBuffer);
// }
// 
// pfCellSpuTrace::pfCellSpuTrace(const char* name, u32 spu, CellSpursTracePacket* buffer, u32 maxTracePackets)
// :	pfTrace(name, new pfTraceCounter(&m_RootId))
// ,	m_Spu(spu)
// ,	m_Buf(buffer)
// ,	m_BufEnd(buffer + maxTracePackets)
// ,	m_BufRead(buffer)
// ,	m_RootId(name, Color32(0))
// {		
// 	m_NumRows = 2;
// }
// 
// u32 pfCellSpuTrace::GetNextSample()
// {
// 	if (m_BufRead->header.spu != 0xCD)
// 		return m_BufRead->header.time|1;
// 	return 0;
// }
// 
// void pfCellSpuTrace::AdvanceTo(u32 timebase)
// {
// 	pfTrace::AdvanceTo(timebase);
// 
// 	while (m_BufRead->header.spu != 0xCD)
// 	{
// 		Assert(m_BufRead->header.spu == m_Spu);
// 		if (s32(timebase - m_BufRead->header.time) < 0)
// 			break;		
// 
// 		switch (m_BufRead->header.tag)
// 		{
// 		case CELL_SPURS_TRACE_TAG_JOB:
// 			{
// 				if (m_ActiveJob)
// 				{
// 					PopCounter(m_BufRead->header.time);
// 					m_ActiveJob = 0;
// 				}
// 				CellSpursJob256* job = (CellSpursJob256*)m_BufRead->data.job.jobDescriptor;
// 				if (job)
// 				{
// 					const char* name = (const char*)job->workArea.userData[24];
// 					if (name)
// 					{
// 						//m_JobStart = m_BufRead->header.time;					
// 						m_ActiveJob = name;
// 						PushCounter(m_BufRead->header.time, &RageTrace().IdFromName(name));
// 					}
// 				}
// 			}
// 			break;
// 		case CELL_SPURS_TRACE_TAG_GUID:
// 			if (m_ActiveJob)
//  			{
// 				PopCounter(m_BufRead->header.time);
//  				m_ActiveJob = 0;
//  			}
// 			break;
// 
// 		case CELL_SPURS_TRACE_TAG_TASK:
// 			switch(m_BufRead->data.task.incident)
// 			{
// 			case CELL_SPURS_TRACE_TASK_DISPATCH:
// 				if (m_ActiveJob)
// 				{
// 					PopCounter(m_BufRead->header.time);
// 					m_ActiveJob = 0;
// 				}
// 				m_ActiveJob = "SPU Tasks"; // :TODO: get proper task name here
// 				PushCounter(m_BufRead->header.time, &RageTrace().IdFromName(m_ActiveJob));
// 				break;
// 
// 			case CELL_SPURS_TRACE_TASK_WAIT:
// 			case CELL_SPURS_TRACE_TASK_YIELD:
// 			case CELL_SPURS_TRACE_TASK_EXIT:
// 				if (m_ActiveJob)
// 				{
// 					PopCounter(m_BufRead->header.time);
// 					m_ActiveJob = 0;
// 				}
// 				break;
// 			}
// 			break;
// 		}
// 		m_BufRead->header.spu = 0xCD;
// 		if (++m_BufRead == m_BufEnd)
// 			m_BufRead = m_Buf;
// 	}
// }

} // namespace rage

#endif // __PPU && RAGETRACE
