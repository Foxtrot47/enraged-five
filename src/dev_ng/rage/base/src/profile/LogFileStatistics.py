import sys, csv, math
inFile = csv.reader(file(sys.argv[1], "rb"))

headers = inFile.next()
dataSets = [[] for x in headers]

for data in inFile:
	for i in range(len(data)):
		if len(data[i]) > 0:
			dataSets[i].append(float(data[i]))

def GetStats(dataset):
	total = sum(dataset)
	total2 = sum([x*x for x in dataset])
	n = len(dataset)
	mean = total/n
	stddev = math.sqrt((total2 - total*mean)/(n-1))
	return (min(dataset), max(dataset), mean, stddev)

outFile = csv.writer(file(sys.argv[2], "wb"))
outFile.writerow(["Timer", "Mean", "Min", "Max", "Stddev"])

for i in range(len(data)):
	if len(dataSets[i]) > 0:
		theMin, theMax, theMean, theStddev = GetStats(dataSets[i])
		outFile.writerow([headers[i], theMean, theMin, theMax, theStddev])
