#ifndef PROFILE_CPU_TRACE_H
#define PROFILE_CPU_TRACE_H

#include "trace.h"

#if __SPU
#include "cellspurstrace.h"
#else
#if RAGETRACE

#include "system/tls.h"

namespace rage {

// per-thread CPU trace

class pfCpuThreadTrace : public pfBufferedTrace
{
public:
	pfCpuThreadTrace(pfTraceCounterId* rootId, u32 bufsize, u32 threadid, u32 numrows = 1);

	void	AdvanceTo(u32 timebase);

private:	
	u32		m_LastActive;
	u32		m_LastTimeBase;
	u32		m_ThreadId;
};

// cpu trace counter that tracks thread idle/busy time

class pfCpuTraceCounter : public pfTraceCounter
{
public:
	pfCpuTraceCounter(pfTraceCounterId* id = 0) : pfTraceCounter(id) {}

	pfTraceCounter* New() {return rage_new pfCpuTraceCounter;}
	void Dump();
	void Reset();
	void EndFrame();	
	void DisplayStats(float x, float y);

	void OnPush(u32 timebase);
	void OnPop(u32 timebase);

	u32					m_IdleCount;
	pfFrameStatCounter	m_IdleTime;
};

extern __THREAD pfCpuThreadTrace* g_pfCpuThreadTrace;

class pfCpuTraceScope
{
public:
	pfCpuTraceScope(pfTraceCounterId* id) 
	{
		if (g_pfCpuThreadTrace)
			g_pfCpuThreadTrace->Push(id);
	}
	~pfCpuTraceScope() 
	{
		if (g_pfCpuThreadTrace)
			g_pfCpuThreadTrace->Pop();
	}
};

extern bool g_pfEnableCpuTrace;

class pfCpuThreadTraceInitialiser : public pfTraceInitialiser
{
public:
	pfCpuThreadTraceInitialiser(const char* name, u32 sizeKb, u32 depth);
	~pfCpuThreadTraceInitialiser();
	void Enable();
	void Disable();

private:
	pfTraceCounterId	m_RootId;
	u32					m_sizeKb;
	u32					m_depth;
	pfCpuThreadTrace*&	m_rpCpuTrace;
	u32					m_ThreadId;
};

} // namespace rage

#define RAGETRACE_INITTHREAD(name, sizeKb, depth) \
	static ::rage::pfCpuThreadTraceInitialiser threadTrace(name, sizeKb, depth);	
#define _RAGETRACE_PUSH(id) do {if (::rage::g_pfCpuThreadTrace) ::rage::g_pfCpuThreadTrace->Push(&id);} while (0)
#define _RAGETRACE_START(id) do {if (::rage::g_pfCpuThreadTrace) ::rage::g_pfCpuThreadTrace->Push(&id, true);} while (0)
#define _RAGETRACE_SCOPE(id) ::rage::pfCpuTraceScope MacroJoin(_ragetrace_,__LINE__)(&id)
#define RAGETRACE_POP() do {if (::rage::g_pfCpuThreadTrace) ::rage::g_pfCpuThreadTrace->Pop();} while (0)

#else

#define RAGETRACE_INITTHREAD(name, sizeKb, depth)
#define _RAGETRACE_PUSH(id)
#define _RAGETRACE_START(id)
#define _RAGETRACE_SCOPE(id)
#define RAGETRACE_POP()

#endif // RAGETRACE

#define RAGETRACE_PUSH(id) _RAGETRACE_PUSH(_ragetrace_##id)
#define RAGETRACE_START(id) _RAGETRACE_START(_ragetrace_##id)
#define RAGETRACE_SCOPE(id) _RAGETRACE_SCOPE(_ragetrace_##id)
#define RAGETRACE_PUSHNAME(name)  _RAGETRACE_PUSH(::rage::RageTrace().IdFromName(name))
#define RAGETRACE_STARTNAME(name) _RAGETRACE_START(::rage::RageTrace().IdFromName(name))
#define RAGETRACE_SCOPENAME(name) _RAGETRACE_SCOPE(::rage::RageTrace().IdFromName(name))
#endif

#endif // PROFILE_CPU_TRACE_H
