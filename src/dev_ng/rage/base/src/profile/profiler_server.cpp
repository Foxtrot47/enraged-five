#include "settings.h"

#if USE_PROFILER

//rage
// #include "compression/compression.h"
#include "file/asset.h"
#include "file/tcpip.h"
#include "math/amath.h"
#include "system/criticalsection.h"
#include "system/magicnumber.h"
#include "system/param.h"

#include "common.h"
#include "profiler_server.h"

PARAM(rockyZip, "[Rocky] Turns on compression for Rocky captures");

using namespace rage;

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct FileHeader
{
	static const u16 VERSION = 1;
	static const u32 MAGIC_WORD = MAKE_MAGIC_NUMBER('R', 'K', 'Y', '1');

	u32 magic;
	u16 version;
	u16 flags;

	enum Flags
	{
		ZIP_PACKED = 0x1,
	};

	FileHeader(bool isZip) : magic(MAGIC_WORD), version(VERSION), flags(0)
	{
		if (isZip)
		{
			flags |= ZIP_PACKED;
		}
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Server::Server(const AppDescription& applicationDesc, int port, int range) : listenSocket(fiHandleInvalid), clientSocket(fiHandleInvalid), compressorState(nullptr), outputBuffer(), desc(applicationDesc), port(port), range(range), useCompression(false)
{
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Server::Update()
{
	SYS_CS_SYNC(csServer);

	if (!UpdateConnection())
		return;

	Flush();

	int length = -1;
	while ( (length = rage::fiDeviceTcpIp::GetInstance().Read(clientSocket, inputBuffer, BUFFER_SIZE ) ) > 0 )
	{
		networkStream.Write(inputBuffer, length);
	}

	while (IMessage *message = MessageFactory::Get().Create(networkStream))
	{
		messages.Push(message);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* Server::PopMessage()
{
	return messages.Pop();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct DataResponse
{
	rage::u32 version;
	rage::u32 size;
	rage::u16 responseType;
	rage::u16 applicationID;

	DataResponse(rage::u16 t, rage::u16 appID, rage::u32 s, rage::u32 v) : version(v), size(s), applicationID(appID), responseType(t) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Server::SendData(const void* data, size_t size)
{
	if (!fiIsValidHandle(clientSocket) && localStream == nullptr)
		return false;

	SYS_CS_SYNC(csServer);

	rockyFatalAssertf(bytesRequiredToSend >= size, "Protocol is broken! Sending more than expcted!");
	bytesRequiredToSend -= size;

	if (outputBuffer.GetCapacity() < MAX_ALLOWED_BUFFER_SIZE)
	{
		USE_ROCKY_MEMORY;
		// Reserve, not Reserve extend should be fine, this should only be called once when the buffer is empty).
		outputBuffer.Reserve(MAX_ALLOWED_BUFFER_SIZE);
	}

	if (outputBuffer.GetCount() + size > (rage::u32)outputBuffer.GetCapacity())
		FlushBuffer();

	for (rage::u32 index = 0; index < size;)
	{
		rage::u32 maxDataAllowed = outputBuffer.GetCapacity() - outputBuffer.GetCount();
		rage::u32 maxDataPresent = (rage::u32)size - index;
		rage::u32 dataSize = Min(maxDataPresent, maxDataAllowed);

		rage::u8* toWrite = outputBuffer.GetElements() + outputBuffer.GetCount();
		outputBuffer.ResizeGrow(outputBuffer.GetCount() + dataSize);
		sysMemCpy(toWrite, (rage::u8*)data + index, dataSize);

		index += dataSize;

		if (outputBuffer.GetCount() == outputBuffer.GetCapacity())
			FlushBuffer();
	}

	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Server::SendData(OutputDataStream& stream)
{
	SYS_CS_SYNC(csServer);
	rage::sysMemAutoUseDebugMemory debugMem;
	return SendData(stream.GetData(), stream.Length());
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Server::SendHeader(rage::u16 type, size_t dataSize)
{
	if (!fiIsValidHandle(clientSocket) && localStream == nullptr)
		return false;

	SYS_CS_SYNC(csServer);
	DataResponse response(type, desc.applicationID, (rage::u32)dataSize, desc.protocolVersion);

	rockyFatalAssertf(bytesRequiredToSend == 0, "Protocol is broken! Still expecting %" I64FMT "u bytes to send!", (u64)bytesRequiredToSend);
	bytesRequiredToSend = dataSize + sizeof(response);

	return SendData(&response, sizeof(response));
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Server::Send(rage::u16 type, OutputDataStream& stream)
{
	SYS_CS_SYNC(csServer);

	rage::sysMemAutoUseDebugMemory debugMem;

	if (!SendHeader(type, stream.Length()))
		return false;

	return SendData(stream.GetData(), stream.Length());
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Server::FlushBuffer()
{
	PROFILE;
	if (!outputBuffer.empty())
	{
		if (localStream != nullptr)
		{
			/*if (useCompression)
			{
				USE_ROCKY_MEMORY;

				rage::CompressorBase* compressor = rage::CompressorFactory::GetCompressor(rage::COMPRESSOR_ZLIB);
				compressorState->debugName = "Rocky";

				if (compressBuffer.GetCapacity() < outputBuffer.GetCapacity())
				{
					compressBuffer.ResizeGrow(outputBuffer.GetCapacity());
				}

				compressor->DeflateReset(compressorState, rage::CompressorBase::FOCUS_BEST_SPEED, 0);

				compressorState->avail_in = outputBuffer.GetCount();
				compressorState->next_in = outputBuffer.GetElements();
				compressorState->total_in = 0;

				while (compressorState->avail_in)
				{
					compressorState->avail_out = compressBuffer.GetCount();
					compressorState->next_out = compressBuffer.GetElements();
					compressorState->total_out = 0;

					if (compressor->Deflate(compressorState))
					{
						u32 chunkSize = (u32)(compressorState->total_out);
						localStream->Write(&chunkSize, sizeof(chunkSize));

						u32 uncompressedSize = outputBuffer.GetCount();
						localStream->Write(&uncompressedSize, sizeof(uncompressedSize));

						localStream->Write(compressBuffer.GetElements(), chunkSize);
					}
				}
			}
			else
			{*/
				localStream->Write(outputBuffer.GetElements(), (int)outputBuffer.GetCount());
			//}

			localStream->Flush();
		}

		if (fiIsValidHandle(clientSocket))
		{
			PROFILER_TAG("Size", outputBuffer.GetCount());
			rage::fiDeviceTcpIp::GetInstance().SafeWrite(clientSocket, outputBuffer.GetElements(), (int)outputBuffer.GetCount());
		}

		outputBuffer.ResetCount();
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Server::Flush()
{
	SYS_CS_SYNC(csServer);
	FlushBuffer();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define MAX_READ_CHUNK_SIZE		(512 << 10)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Server::UpdateConnection()
{
	SYS_CS_SYNC(csServer);

	if (!fiIsValidHandle(listenSocket))
	{
		for (int currentPort = port; currentPort < port + range; ++currentPort)
		{
#if RSG_PC
			const char* ipAddress = rage::fiDeviceTcpIp::GetInstance().GetLocalHost();
#else
			const char* ipAddress = nullptr;
#endif

			listenSocket = rage::fiDeviceTcpIp::GetInstance().Listen(currentPort, 8, ipAddress);
			if (fiIsValidHandle(listenSocket))
			{
				rockyDisplayf("Starting Rocky Profiling Server [%s:%d]", ipAddress ? ipAddress : "localhost", currentPort);
				rage::fiDeviceTcpIp::SetBlocking(listenSocket, false);
				break;
			}
		}
	}

	if (!fiIsValidHandle(listenSocket))
		return false;

	fiHandle socket = rage::fiDeviceTcpIp::GetInstance().Pickup(listenSocket);
	if (fiIsValidHandle(socket))
	{
		if (fiIsValidHandle(clientSocket))
			rage::fiDeviceTcpIp::GetInstance().Close(clientSocket);

		rockyDisplayf("Established a new connection!");

		clientSocket = socket;
		//rage::fiDeviceTcpIp::GetInstance().SetNoDelay(clientSocket, true);
		//rage::fiDeviceTcpIp::GetInstance().SetReadWriteBufferSize(clientSocket, MAX_READ_CHUNK_SIZE, MAX_READ_CHUNK_SIZE);
		rage::fiDeviceTcpIp::SetBlocking(clientSocket, false);
	}

	return fiIsValidHandle(clientSocket);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Server::SetLocalMode(const char* fileName)
{
	if (localStream != nullptr)
	{
		localStream->Close();
		localStream = nullptr;

		//if (compressorState)
		//{
		//	rage::CompressorBase* const compressor = rage::CompressorFactory::GetCompressor(rage::COMPRESSOR_ZLIB);
		//	compressor->DeflateEnd(compressorState);
		//	compressor->DestroyCompressorState(compressorState);
		//	compressorState = nullptr;
		//}
	}

	if (fileName != nullptr)
	{
		if ((localStream = rage::ASSET.Create(fileName, "prof")) != nullptr)
		{
			// useCompression = PARAM_rockyZip.Get();
			FileHeader header(false);
			localStream->Write(&header, sizeof(FileHeader));

			//rage::CompressorBase* const compressor = rage::CompressorFactory::GetCompressor(rage::COMPRESSOR_ZLIB);
			//if (compressorState)
			//{
			//	compressor->DeflateReset(compressorState, rage::CompressorBase::FOCUS_BEST_SPEED, 0);
			//}
			//else
			//{
			//	compressorState = compressor->CreateCompressorState();
			//	compressor->DeflateInit(compressorState, rage::CompressorBase::FOCUS_BEST_SPEED, 0);
			//}
		}
		else
		{
			rockyErrorf("Rocky capture could not be saved to file %s", fileName);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Server::IsLocalMode() const
{
	return localStream != nullptr;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Server::SetListenPort(int listenPort)
{
	port = listenPort;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Server::Shutdown()
{
	SetLocalMode(nullptr);

	if (fiIsValidHandle(clientSocket))
	{
		rage::fiDeviceTcpIp::GetInstance().Close(clientSocket);
		clientSocket = fiHandleInvalid;
	}

	if (fiIsValidHandle(listenSocket))
	{
		rage::fiDeviceTcpIp::GetInstance().Close(listenSocket);
		listenSocket = fiHandleInvalid;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Server::~Server()
{
	Shutdown();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct MessageHeader
{
	rage::u32 mark;
	rage::u32 length;

	static const rage::u32 MESSAGE_MARK = 0xB50FB50F;
	bool IsValid() const { return mark == MESSAGE_MARK; }

	MessageHeader() : mark(0), length(0) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
MessageFactory& MessageFactory::Get()
{
	static MessageFactory instance;
	return instance;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* MessageFactory::Create(InputDataStream& str)
{
	MessageHeader header;
	if (!str.Read(header))
		return nullptr;

	if (!header.IsValid())
		return nullptr;

	size_t length = str.Length();

	if (!str.CanRead(header.length))
		return nullptr;

	rage::u16 applicationID = (u16)-1;
	rage::u16 messageType = (u16)-1;

	str >> applicationID;
	str >> messageType;

	MessageCreateFunction* fn = factory.Access((applicationID << 16) | messageType);

	rockyFatalAssertf(fn != nullptr, "Unknown message type! Most probably you need to update your code!");

	if (fn != nullptr)
	{
		IMessage* result = (*fn)(str);

		if (header.length + str.Length() != length)
		{
			rockyFatalAssertf(false, "Message Stream is corrupted! Invalid Protocol?");
			return nullptr;
		}

		return result;
	}

	return nullptr;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER
