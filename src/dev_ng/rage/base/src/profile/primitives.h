#ifndef PROFILE_PRIMITIVES_H
#define PROFILE_PRIMITIVES_H

#include "renderer.h"
#include "vectormath/classes.h"
#include "vector/color32.h"

#if RSG_PROFILE_RENDERER

/*
	Rules for functions in this file:
	1. If a function accepts a world matrix, it preserves the current world matrix before switching to the specified world matrix.
	2. Most function overloads that accept Vec3V's convert them to call the Mat34V version, and so therefore are subject to rule 1.  The primary exception is DrawLine.
	3. The only function that modifies current color state is debugDraw::Color*.  Other functions that accept a color directly do not modify current color state.
	4. If you need to call a function that requires a color and you want it to inherit the current color state, just pass in debugDraw::GetCurrentColor().
*/

namespace rage {

struct pfColor {
	pfColor(Color32 c) : m_Color(c.GetColor()) { }
	pfColor(u32 c) : m_Color(c) { }
	operator unsigned() const {
		return m_Color; 
	}
	void SetRed(int c) {
		c = (c<0)? 0 : (c>255)? 255 : c;
		((Color32&)m_Color).SetRed(c);
	}
	void SetGreen(int c) {
		c = (c<0)? 0 : (c>255)? 255 : c;
		((Color32&)m_Color).SetGreen(c);
	}
	void SetBlue(int c) {
		c = (c<0)? 0 : (c>255)? 255 : c;
		((Color32&)m_Color).SetBlue(c);
	}
	void SetAlpha(int c) {
		c = (c<0)? 0 : (c>255)? 255 : c;
		((Color32&)m_Color).SetAlpha(c);
	}
	int GetRed() const {
		return ((Color32&)m_Color).GetRed();
	}
	int GetGreen() const {
		return ((Color32&)m_Color).GetGreen();
	}
	int GetBlue() const {
		return ((Color32&)m_Color).GetBlue();
	}
	int GetAlpha() const {
		return ((Color32&)m_Color).GetAlpha();
	}
	unsigned m_Color;
};
CompileTimeAssert(sizeof(pfColor) == sizeof(Color32));

const pfColor pfColor_white(0xFFFFFFFF);

namespace debugDraw {

// Here are the basic batch render functions.

inline unsigned Rescale(float x) {
	if (x <= 0.0f)
		return 0;
	else if (x >= 1.0f)
		return 255;
	else
		return (unsigned)((x * 255.0f) + 0.5f);
}

inline void Color4u(unsigned r,unsigned g,unsigned b,unsigned a) {
	((Color32&)g_debugDraw->m_Color).Set(r,g,b,a);
}

inline void Color3u(unsigned r,unsigned g,unsigned b) {
	Color4u(r,g,b,255);
}

inline void Color3f(float r,float g,float b) {
	Color3u(Rescale(r),Rescale(g),Rescale(b));
}

inline void Color3fv(const float *v) {
	Color3f(v[0],v[1],v[2]);
}

inline void Color3f(Vec3V_In v) {
	Color3fv((float*)&v);
}

inline void Color4f(float r,float g,float b,float a) {
	Color4u(Rescale(r),Rescale(g),Rescale(b),Rescale(a));
}

inline void Color4fv(const float *v) {
	Color4f(v[0],v[1],v[2],v[3]);
}

inline void Color4f(Vec4V_In v) {
	Color4fv((float*)&v);
}

/* inline void Color(unsigned u) {
	g_debugDraw->m_Color = u;
} */

inline void Color(pfColor c) {
	g_debugDraw->m_Color = c.m_Color;
}

inline pfColor GetCurrentColor() {
	return pfColor(g_debugDraw->m_Color);
}

inline void TexCoord2f(float s,float t) {
	g_debugDraw->m_TexCoordS = s;
	g_debugDraw->m_TexCoordT = t;
}

inline void TexCoord2fv(const float *v) {
	TexCoord2f(v[0],v[1]);
}

inline void Normal3f(float x,float y,float z) {
	g_debugDraw->m_NormalX = x;
	g_debugDraw->m_NormalY = y;
	g_debugDraw->m_NormalZ = z;
}

inline void Normal3fv(const float *v) {
	Normal3f(v[0],v[1],v[2]);
}

inline void Normal3f(Vec3V_In v) {
	Normal3fv((float*)&v);
}

inline void Vertex3f(float x,float y,float z) {
	(g_debugDraw->*(g_debugDraw->m_Vertex3f))(x,y,z);
}

inline void Vertex3fv(const float *v) {
	Vertex3f(v[0],v[1],v[2]);
}

inline void Vertex3f(Vec3V_In v) {
	Vertex3fv((float*)&v);
}

inline void Vertex2fv(const float *v) {
	Vertex3f(v[0],v[1],0.0f);
}

inline void Vertex2f(float x,float y) {
	Vertex3f(x,y,0);
}

inline void Vertex2f(Vec2V_In v) {
	Vertex2fv((float*)&v);
}

void SetLifetime(unsigned ftl);

inline void Begin(pfDrawType dt,unsigned vc,pfVertexType vt) {
	g_debugDraw->Begin(dt,vc,vt);
}

inline void End() {
	g_debugDraw->End();
}

inline unsigned GetScreenWidth() {
	return g_pfRenderFactory->GetScreenWidth();
}

inline unsigned GetScreenHeight() {
	return g_pfRenderFactory->GetScreenHeight();
}

inline unsigned GetFontHeight() {
	return g_pfRenderFactory->GetFontHeight(g_debugDraw->m_Font);
}

inline unsigned GetStringWidth(const char *s,size_t len = 0) {
	if (!len) len = strlen(s);
	return g_pfRenderFactory->GetStringWidth(g_debugDraw->m_Font,s,len);
}

inline pfFont *GetCurrentFont() {
	return g_debugDraw->m_Font;
}

inline pfFont *SetCurrentFont(pfFont *f) {
	pfFont *prev = g_debugDraw->m_Font;
	g_debugDraw->m_Font = f;
	return prev;
}

extern void DrawString(float x,float y,float z,float scale,pfColor rgba,const char *string,unsigned len);
inline void Draw2dText(float x,float y,pfColor rgba,const char *string) {
	DrawString(x,y,0,1.0f,rgba,string,ustrlen(string));
}
PRINTF_LIKE_N(6) extern void DrawStringf(float x,float y,float z,float scale,pfColor rgba,const char *fmt,...);
extern bool DrawLabel(Vec3V_In pos,int xOffset,int yOffset,pfColor rgba,const char *string,unsigned len);
inline bool DrawLabel(Vec3V_In pos,pfColor rgba,const char *string,unsigned len) {
	return DrawLabel(pos,0,0,rgba,string,len);
}

PRINTF_LIKE_N(3) extern bool DrawLabelf(Vec3V_In pos,pfColor rgba,const char *fmt,...);
PRINTF_LIKE_N(5) extern bool DrawLabelf(Vec3V_In pos,int xOffset,int yOffset,pfColor rgba,const char *fmt,...);
// Returns true if point is front halfspace.  outNormPos contains normalized (0..1) screen position (which may be all or partially off screen)
extern bool Project(Vec3V_InOut outNormPos,Vec3V_In pos,Mat34V_In camera,Mat44V_In proj);
inline bool Project(Vec3V_InOut outNormPos,Vec3V_In pos) {
	return Project(outNormPos,pos,g_debugDraw->m_Camera,g_debugDraw->m_Projection);
}
// Convert incoming normalized (0..1) screen position to two world space points (always ignores current world matrix, just respects camera and projection)
extern void UnProject(float xNorm,float yNorm,Vec3V_InOut outNear,Vec3V_InOut outFar);

extern void SetWorldMatrix(Mat34V_In);
extern void SetWorldIdentity();
extern void MultiplyWorldMatrix(Mat34V_In);
extern void PushWorldMatrix();
extern void PopWorldMatrix();
extern void PushProjectionMatrix();
extern void SetProjectionMatrix(Mat44V_In);
inline Mat44V_Out GetProjectionMatrix() {
	return g_debugDraw->m_Projection;
}
extern void PopProjectionMatrix();
extern void PushCameraMatrix();
extern void SetCameraMatrix(Mat34V_In);
inline Mat34V_Out GetCameraMatrix() {
	return g_debugDraw->m_Camera;
}
inline Vec3V_Out GetCameraPosition() {
	return g_debugDraw->m_Camera.GetCol3();
}
extern void PopCameraMatrix();

extern void PushDepthStencilState();
extern void PopDepthStencilState();
extern void SetDepthStencilState(pfDepthStencilState dss);
inline void PushDepthStencilState(pfDepthStencilState dss) {
	PushDepthStencilState();
	SetDepthStencilState(dss);
}
extern void PushRasterizerState();
extern void PopRasterizerState();
extern void SetRasterizerState(pfRasterizerState rs);
inline void PushRasterizerState(pfRasterizerState rs) {
	PushRasterizerState();
	SetRasterizerState(rs);
}

extern void PushBlendState();
extern void PopBlendState();
extern void SetBlendState(pfBlendState rs);
inline void PushBlendState(pfBlendState rs) {
	PushBlendState();
	SetBlendState(rs);
}

extern void PushViewport();
extern void SetViewport(const pfViewport &v);
inline void SetViewport(float x,float y,float w,float h,float zMin=0.0f,float zMax=1.0f) {
	pfViewport v;
	v.Set(x,y,w,h,zMin,zMax);
	SetViewport(v);
}
inline const pfViewport& GetViewport() {
	return g_debugDraw->m_Viewport;
}
extern void PopViewport();

extern void SetTexture(pfTexture*);
extern void DrawLine(const float *p1,const float *p2,pfColor c1,pfColor c2);
inline void DrawLine(Vec3V_In p1,Vec3V_In p2,pfColor c) {
	DrawLine((float*)&p1,(float*)&p2,c,c);
}
inline void DrawLine(Vec3V_In p1,Vec3V_In p2,pfColor c1,pfColor c2) {
	DrawLine((float*)&p1,(float*)&p2,c1,c2);
}

// In an effort to have fewer surprises with regard to state leaks or inconsistent behavior:
// Draw... functions typically have three versions.
// The first draws a "unit" version with the current world matrix.
// The second and third draw a scaled version with the supplied matrix (the world matrix is not affected).  They differ in whether a single component or three components are specified.
// Color is not specified; it is always inherited from current color state.
// Primitives are always wireframe unless they have "Solid" in their name; no flags in the parameter list to control this any longer.

// Draws a box (all edges length 1.0f) centered around the origin; use the world matrix to position it properly.
extern void DrawBox(pfColor color);
// Draws a box with specified scale using supplied matrix (which is pushed and popped to preserve a previous matrix)
extern void DrawBox(Vec3V_In scale,Mat34V_In mtx,pfColor color);
inline void DrawBox(float edgeLength,Mat34V_In mtx,pfColor color) {
	DrawBox(Vec3V(ScalarV(edgeLength)),mtx,color);
}
inline void DrawBox(Vec3V_In mini,Vec3V_In maxi,pfColor color) {	// For drawing AABB's
	Vec3V mid = (maxi - mini) * Vec3V(V_HALF);
	Mat34V m34;
	Mat34VFromTranslation(m34,mini + mid);
	DrawBox(mid, m34, color);
}

// Draws a solid box (all edges length 1.0f) centered around the origin; use the world matrix to position it properly.
extern void DrawSolidBox(pfColor color);
// Draws a box with specified scale using supplied matrix (which is pushed and popped to preserve a previous matrix)
extern void DrawSolidBox(Vec3V_In scale,Mat34V_In mtx,pfColor color);
inline void DrawSolidBox(float edgeLength,Mat34V_In mtx,pfColor color) {
	DrawSolidBox(Vec3V(ScalarV(edgeLength)),mtx,color);
}
inline void DrawSolidBox(Vec3V_In mini,Vec3V_In maxi,pfColor color) {	// For drawing AABB's
	Vec3V mid = (maxi - mini) * Vec3V(V_HALF);
	Mat34V m34;
	Mat34VFromTranslation(m34,mini + mid);
	DrawSolidBox(mid, m34, color);
}


// Draws a unit sphere centered around the origin; use the world matrix to position it properly, scale it into an ellipsoid
extern void DrawSphere(pfColor color);
// Draws a sphere with specified scale using supplied matrix (which is pushed and popped to preserve a previous matrix)
extern void DrawSphere(Vec3V_In scale,Mat34V_In mtx,pfColor color);
inline void DrawSphere(float radius,Mat34V_In mtx,pfColor color) {
	DrawSphere(Vec3V(ScalarV(radius)),mtx,color);
}
inline void DrawSphere(float radius,Vec3V_In pos,pfColor color) {
	Mat34V m34;
	Mat34VFromTranslation(m34,pos);
	DrawSphere(Vec3V(ScalarV(radius)),m34,color);
}

// Draws a unit solid sphere centered around the origin; use the world matrix to position it properly, scale it into an ellipsoid
extern void DrawSolidSphere(pfColor color);
// Draws a solid sphere with specified scale using supplied matrix (which is pushed and popped to preserve a previous matrix)
extern void DrawSolidSphere(Vec3V_In scale,Mat34V_In mtx,pfColor color);
inline void DrawSolidSphere(float radius,Mat34V_In mtx,pfColor color) {
	DrawSolidSphere(Vec3V(ScalarV(radius)),mtx,color);
}
inline void DrawSolidSphere(float radius,Vec3V_In pos,pfColor color) {
	Mat34V m34;
	Mat34VFromTranslation(m34,pos);
	DrawSolidSphere(Vec3V(ScalarV(radius)),m34,color);
}
inline void DrawEitherSphere(float radius,Vec3V_In pos,pfColor color,bool solid) {
	if (solid)
		DrawSolidSphere(radius,pos,color);
	else
		DrawSphere(radius,pos,color);
}
inline void DrawEitherSphere(Vec3V_In scale,Mat34V_In mtx,pfColor color,bool solid) {
	if (solid)
		DrawSolidSphere(scale,mtx,color);
	else
		DrawSphere(scale,mtx,color);
}

extern void DrawAxis();
extern void DrawAxis(Vec3V_In scale,Mat34V_In mtx);
inline void DrawAxis(float scale,Mat34V_In mtx) {
	DrawAxis(Vec3V(ScalarV(scale)),mtx);
}

extern void DrawFrustum(pfColor color);
extern void DrawFrustum(Vec3V_In scale,Mat34V_In mtx,pfColor color);

extern void DrawSphericalCone();

// Capsules are always Y-up to match our physics engine; use the matrix to orient it accordingly.
extern void DrawTaperedCapsule(float height,float radiusBot,float radiusTop,Mat34V_In mtx,pfColor color);
extern void DrawSolidTaperedCapsule(float height,float radiusBot,float radiusTop,Mat34V_In mtx,pfColor color);

inline void DrawCapsule(float height,float radius,Mat34V_In mtx,pfColor color) {
	DrawTaperedCapsule(height,radius,radius,mtx,color);
}
inline void DrawSolidCapsule(float height,float radius,Mat34V_In mtx,pfColor color) {
	DrawSolidTaperedCapsule(height,radius,radius,mtx,color);
}

extern void DrawTaperedCylinder(float height,float radiusBot,float radiusTop,Mat34V_In mtx,pfColor color);
extern void DrawSolidTaperedCylinder(float height,float radiusBot,float radiusTop,Mat34V_In mtx,pfColor color);
extern void DrawCylinder(float height,float radius,Mat34V_In mtx,pfColor color);
extern void DrawSolidCylinder(float height,float radius,Mat34V_In mtx,pfColor color);
extern void DrawSolidCappedCylinder(float height,float radius,Mat34V_In mtx,pfColor color);

// Just fucking kill me.
extern void DrawSpiral(Vec3V_In start, Vec3V_In  end, pfColor color, float startRadius, float endRadius, float revolutionsPerMeter, float initialPhase = 0.0f, float arrowLength = 0.0f, int steps=12);
extern void DrawArc(float r, Vec3V_In center, Vec3V_In  axisX, Vec3V_In axisY, pfColor color, float beginAngle, float endAngle, int steps=11);
extern void DrawArc(pfColor color, float beginAngle, float endAngle, int steps=11);
extern void DrawCircle(pfColor color);
extern void DrawCircle(float r, Vec3V_In center, Vec3V_In axisX, Vec3V_In axisY,pfColor color);
extern void DrawSolidCircle(pfColor color);
extern void DrawSolidCircle(float r, Vec3V_In center, Vec3V_In axisX, Vec3V_In axisY,pfColor color);
extern void DrawArrowOrtho(Vec3V_In vStartPos, Vec3V_In  vEndPos, pfColor color, Vec3V_In vCameraPosition, float fArrowheadLength=0.8f, float fArrowheadBaseHalfLength=0.2f);
inline void DrawCircleAxes(float r,Vec3V_In center,pfColor color) { // This is used often enough it may be worth "macroifying"
	DrawCircle(r,center,Vec3V(V_X_AXIS_WONE),Vec3V(V_Y_AXIS_WONE),color);
	DrawCircle(r,center,Vec3V(V_X_AXIS_WONE),Vec3V(V_Z_AXIS_WONE),color);
	DrawCircle(r,center,Vec3V(V_Y_AXIS_WONE),Vec3V(V_Z_AXIS_WONE),color);
}
inline void DrawEitherCircle(float r, Vec3V_In center, Vec3V_In axisX, Vec3V_In axisY,pfColor color,const bool solid) {
	if (solid)
		DrawSolidCircle(r,center,axisX,axisY,color);
	else
		DrawCircle(r,center,axisX,axisY,color);
}

extern void PushDefaultScreen();	// Sets screen-sized ortho projection matrix and sets camera matrix to identity; similar to PUSH_DEFAULT_SCREEN macro
extern void PopDefaultScreen();		// Restores previous projection and camera matrices.

extern void PushGpuMarker(const char *tag);
extern void PopGpuMarker();

// Begin 2D rendering, and remember that we're doing 2D now (LSB set)
inline void Start2D() {
	g_debugDraw = g_debugDraw2D;
	g_debugDrawStack = (g_debugDrawStack << 1) | 1;
	FastAssert(g_debugDrawStack>=0);	// Catch stack overflow (s8 becomes negative)
}

// Pop the stack, and restore 3D if it was previously active
inline void End2D() {
	g_debugDrawStack = g_debugDrawStack >> 1;
	if (!(g_debugDrawStack & 1))
		g_debugDraw = g_debugDraw3D;
	FastAssert(g_debugDrawStack>=2);	// Catch stack underflow (0x2 sentinel should always be there)
}

// Begin 3D rendering, and remember that we're doing 3D now (LSB clear)
inline void Start3D() {
	g_debugDraw = g_debugDraw3D;
	g_debugDrawStack = g_debugDrawStack << 1;
	FastAssert(g_debugDrawStack>=0);	// Catch stack overflow (s8 becomes negative)
}

// Pop the stack, and restore 2D if it was previously active
inline void End3D() {
	g_debugDrawStack = g_debugDrawStack >> 1;
	if (g_debugDrawStack & 1)
		g_debugDraw = g_debugDraw2D;
	FastAssert(g_debugDrawStack>=2);	// Catch stack underflow (0x2 sentinel should always be there)
}

inline void Start2D(int framesToLive) {
	Start2D();
	if (framesToLive != 1)
		SetLifetime(framesToLive);
}

inline void End2D(int framesToLive) {
	if (framesToLive != 1)
		SetLifetime(0);
	End2D();
}

inline void Start3D(int framesToLive) {
	Start3D();
	if (framesToLive != 1)
		SetLifetime(framesToLive);
}

inline void End3D(int framesToLive) {
	if (framesToLive != 1)
		SetLifetime(0);
	End3D();
}

inline void InitThread() {
#if !RSG_FINAL
	if (!g_pfRenderInterface)	// Thread started before the render interface was initialized?
		__debugbreak();
#endif
	g_debugDraw2D = rage_new debugDraw::BatchInterface(g_pfRenderInterface);
    auto bi = rage_new debugDraw::BatchInterface(g_pfRenderInterface);
    g_debugDraw = bi;
    g_debugDraw3D = bi;
	ASSERT_ONLY(g_debugDrawStack = 0x2);
}

inline void ShutdownThread() {
	delete g_debugDraw2D;
	delete g_debugDraw3D;
    g_debugDraw2D = NULL;
    g_debugDraw3D = NULL;
    g_debugDraw = NULL;
}

}	// namespace debugDraw


}	// namespace rage

#endif	// RSG_PROFILE_RENDERER

#endif	 // PROFILE_PRIMITIVES_H
