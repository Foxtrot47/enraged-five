#ifndef PROFILER_MESSAGE_H
#define PROFILER_MESSAGE_H

#if USE_PROFILER

#include "common.h"
#include "serialization.h"
#include "profiler_server.h"

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RockyApp
{
	static const rage::u16 ID = 0;
	static void RegisterMessages();
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RockyResponse
{
	enum Type : rage::u16
	{
		FrameDescriptionBoard = 0,		// DescriptionBoard for Instrumental Frames
		EventFrame = 1,					// Instrumental Data
		SamplingFrame = 2,				// Sampling Data
		NullFrame = 3,					// Last Fame Mark
		ReportProgress = 4,				// Report Current Progress
		Handshake = 5,					// Handshake Response
		LEGACY_VariablesPack = 6,		// Pack with Variables
		SynchronizationData = 7,		// Synchronization Data for the thread
		TagsPack = 8,					// Pack of tags
		CallstackDescriptionBoard = 9,	// DescriptionBoard with resolved function addresses
		CallstackPack = 10,				// Pack of CallStacks
		HWEventsDescriptionBoard = 11,	// Description Board with events
		HWEventsPack = 12,				// Pack of Hardware Events
		LiveDataPack = 13,				// Pack with live data
		ResourcePack = 14,				// Pack with draw data
        NetworkBandwidthLive = 15,      // One frame worth of network bandwidth data streamed to the profiler
        NetworkBandwidthPack = 16,      // Pack with network bandwidth data (all frames included)
		ThreadDescriptionBoard = 17,	// Pack with ThreadDescription names
		PageFaultsPack = 18,			// Path with Page Faults
	};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RockyMessage 
{
	enum Type : rage::u16
	{
		Start,
		Stop,
		TurnSampling,
		StartMode,
		StartModeExt,
		TurnLive,
		Cancel,
		SetMaxCaptureFrameLimit,
		SetRootPassword,
		COUNT,
	};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RockyPlatform
{
	enum Type : rage::u32
	{
		PC,
		PS4,
		XOne,
		Linux,
		Network,
		Other,
	};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct StartMessage : public Message<RockyMessage::Start, RockyApp::ID>
{
	static IMessage* Create(InputDataStream&);
	virtual void Apply() override;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct StartModeMessage : public Message<RockyMessage::StartMode, RockyApp::ID>
{
	rage::u32 mode;
	static IMessage* Create(InputDataStream&);
	virtual void Apply() override;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct StartModeExtMessage : public Message<RockyMessage::StartModeExt, RockyApp::ID>
{
	rage::u32 mode;
	rage::u32 samplingFrequency;
	rage::u32 scriptSamplingFrequency;

	static IMessage* Create(InputDataStream&);
	virtual void Apply() override;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct StopMessage : public Message<RockyMessage::Stop, RockyApp::ID>
{
	static IMessage* Create(InputDataStream&);
	virtual void Apply() override;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct TurnSamplingMessage : public Message<RockyMessage::TurnSampling, RockyApp::ID>
{
	rage::s32 index;
	byte isSampling;

	static IMessage* Create(InputDataStream& stream);
	virtual void Apply() override;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct TurnLiveMessage : public Message<RockyMessage::TurnLive, RockyApp::ID>
{
	rage::s32 index;
	byte isLive;

	static IMessage* Create(InputDataStream& stream);
	virtual void Apply() override;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct CancelMessage : public Message<RockyMessage::Cancel, RockyApp::ID>
{
	static IMessage* Create(InputDataStream&);
	virtual void Apply() override;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct SetMaxCaptureFrameLimitMessage : public Message<RockyMessage::SetMaxCaptureFrameLimit, RockyApp::ID>
{
	rage::u32 frameLimit;

	static IMessage* Create(InputDataStream&);
	virtual void Apply() override;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct SetRootPasswordMessage : public Message<RockyMessage::SetRootPassword, RockyApp::ID>
{
	RockyPlatform::Type platform;
	char password[256];
	
	static IMessage* Create(InputDataStream&);
	virtual void Apply() override;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER

#endif //PROFILER_MESSAGE_H
