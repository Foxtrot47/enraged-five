#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#if USE_PROFILER

#include "common.h"
#include <sstream>
#include "memory_pool.h"
#include "atl/array.h"
#include "atl/vector.h"
#include "math/amath.h"
#include "rocky.h"
#include "string/string.h"
#include "system/stl_wrapper.h"
#include "vectormath/vec3v.h"

namespace Profiler
{
	struct ImageJPG;

	class DataStream
	{
		typedef rage::atVector<rage::u8, 0, rage::u32> DataVector;
		DataVector data;
		rage::u32 position;

		static const rage::u32 MIN_CAPACITY = 4 * 1024;
	public:
		DataStream() : position(0) {}

		rage::u32 Length() const { return data.GetCount() - position; }
		bool IsEmpty() const { return Length() == 0; }

		bool CanRead() const { return Length() > 0; }
		bool CanRead(int count) const { return count <= (int)Length(); }

		const rage::u8* GetData() const
		{
			return !IsEmpty() ? &data[position] : nullptr;
		}

		const char* GetString() const
		{
			return (const char*)GetData();
		}

		template<class T>
		bool Read(T& item)
		{
			return Read((rage::u8*)&item, (rage::u32)sizeof(T));
		}

		template<class T>
		bool ReadString(T& item)
		{
			return Read((rage::u8*)&item, (rage::u32)sizeof(T));
		}

		template<size_t size>
		bool ReadString(char(&buffer)[size])
		{
			rage::u32 length = 0;
			if (Read(length))
			{
				rockyFatalAssertf(length < size, "Output buffer is too small to handle incoming data: needed=%u, allocated=%" SIZETFMT "u", length, size);
				Read((rage::u8*)buffer, length);
				buffer[length] = 0;
				return true;
			}
			return false;
		}


		bool Read(rage::u8* buffer, rage::u32 size)
		{
			if (Length() < size)
				return false;
			
			using namespace rage; // for sysMemCpy
			sysMemCpy(buffer, &data[position], size);
			position += size;

			return true;
		}

		bool Write(const rage::u8* buffer, rage::u32 size)
		{
			if (size == 0)
				return true;

			using namespace rage; // for sysMemCpy

			int availableSpace = data.GetCapacity() - data.GetCount();
			if (size > (rage::u32)availableSpace)
			{
				rage::u32 count = Length() + size;
				DataVector updData(count, rage::Max(4 * count / 3, MIN_CAPACITY));

				rage::u32 size = Length();

				if (size > 0)
					sysMemCpy(&updData[0], &data[position], size);

				data.Swap(updData);
				position = 0;
			}
			else
			{
				data.Resize(data.GetCount() + size);
			}

			int count = data.GetCount();
			sysMemCpy(&data[count - size], buffer, size);
			return true;
		}

		void Reset()
		{
			position = 0;
			data.Resize(0);
		}
	};

	class OutputDataStream : public DataStream
	{
	public:
		static OutputDataStream& GetEmptyStream()
		{
			static OutputDataStream Empty;
			return Empty;
		}

		// It is important to make private inheritance in order to avoid collision with default operator implementation
		friend OutputDataStream &operator << ( OutputDataStream &stream, const char* val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, rage::s8 val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, rage::u8 val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, rage::s16 val);
		friend OutputDataStream &operator << ( OutputDataStream &stream, rage::u16 val);
		friend OutputDataStream &operator << ( OutputDataStream &stream, rage::s32 val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, rage::u32 val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, rage::s64 val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, rage::u64 val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, float val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, char val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, const rage::stlString& val );
		friend OutputDataStream &operator << ( OutputDataStream &stream, const rage::stlWstring& val );
		friend OutputDataStream &operator << (OutputDataStream &stream, const rage::Vec3V& val);
		friend OutputDataStream &operator << (OutputDataStream &stream, const Image& val);
	};

	template<class T>
	OutputDataStream& operator<<(OutputDataStream &stream, const rage::atVector<T>& val)
	{
		stream << (rage::u32)val.GetCount();

		for (int i = 0; i < val.GetCount(); ++i)
			stream << val[i];

		return stream;
	}

	template<class T, int Alignment, class CounterType>
	OutputDataStream& operator<<(OutputDataStream &stream, const rage::atVector<T, Alignment, CounterType>& val)
	{
		stream << (rage::u32)val.GetCount();

		for (int i = 0; i < val.GetCount(); ++i)
			stream << val[i];

		return stream;
	}

	template<class T, int Alignment, class CounterType>
	OutputDataStream& operator<<(OutputDataStream &stream, const rage::atArray<T, Alignment, CounterType>& val)
	{
		stream << (rage::u32)val.GetCount();

		for (int i = 0; i < val.GetCount(); ++i)
			stream << val[i];

		return stream;
	}

	template<class T, uint N>
	OutputDataStream& operator<<(OutputDataStream &stream, const MemoryPool<T, N>& val)
	{
		stream << (rage::u32)val.Size();

		val.ForEach([&](const T& data) -> bool
		{
			stream << data;
			return true;
		});

		return stream;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class InputDataStream : public DataStream {};

	template<class T>
	InputDataStream & operator>>( InputDataStream &stream, T& val )
	{
		stream.Read( val );
		return stream;
	}



}

#endif //USE_PROFILER

#endif //SERIALIZATION_H
