//
// profile/cellschedulertrace.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "cellschedulertrace.h"
#if __PPU && RAGETRACE
#include <cell/perf/performance.h>

#pragma comment(lib,"perf")

namespace rage {

pfCellSchedulerTrace* g_pfCellSchedulerTrace = 0;

pfCellSchedulerTrace::pfCellSchedulerTrace()
:	pfTrace("CellScheduler")
,	m_PpuSchedTraceBuf(0)
,	m_ReadPointer(0)
{
	m_NumRows = 0;
	m_HwThreads[0] = 0;
	m_HwThreads[1] = 0;

	ASSERT_ONLY(int ret =) cellPerfAddLv2OSTrace(
		CELL_PERF_LV2_TRACE_TYPE_PPU_SCHEDULER,
		SCHED_TRACE_BUF_SIZE, 
		CELL_PERF_LV2_TRACE_MODE_OVERWRITE,
		&m_PpuSchedTraceBuf);
	Assert(CELL_OK == ret);	

	cellPerfStart();
}

pfCellSchedulerTrace::~pfCellSchedulerTrace()
{
	cellPerfDeleteLv2OSTrace(CELL_PERF_LV2_TRACE_TYPE_PPU_SCHEDULER);
	cellPerfStop();
}

struct ppuLv2TraceHeader
{
	u8 tag;
	u8 length;
	u16 hwthreadid;
	u32 timebase;
};
struct ppuLv2SchedulerTrace
{
	u32 threadid;
	u8 incident;
	u8 reserved;
	u16 priority;
};

u32 pfCellSchedulerTrace::GetNextSample()
{	
	u32 result = 0;
	CellPerfLv2TraceHeader* ppuSchedulerTrace = (CellPerfLv2TraceHeader*)m_PpuSchedTraceBuf;
	while (m_ReadPointer != ppuSchedulerTrace->writePointer)
	{
		u32* schedtrace = (u32*)(ppuSchedulerTrace + 1) + m_ReadPointer;
		ppuLv2TraceHeader* tracehdr = (ppuLv2TraceHeader*)schedtrace;
		if (tracehdr->tag == CELL_PERF_LV2_TRACE_TYPE_PPU_SCHEDULER)
		{
			result = tracehdr->timebase | 1;
			break;
		}
		m_ReadPointer += 2 + tracehdr->length;
		m_ReadPointer %= (SCHED_TRACE_BUF_SIZE - sizeof(CellPerfLv2TraceHeader)) / 4;
	}	
	return result;
}

// NOTE: important that pfCellSchedulerTrace is processed AFTER all the 
// pfCpuThreadTrace's so that they get correct information about whether
// they were idle or not

void pfCellSchedulerTrace::AdvanceTo(u32 timebase)
{	
	CellPerfLv2TraceHeader* ppuSchedulerTrace = (CellPerfLv2TraceHeader*)m_PpuSchedTraceBuf;
	while (m_ReadPointer != ppuSchedulerTrace->writePointer)
	{
		u32* schedtrace = (u32*)(ppuSchedulerTrace + 1) + m_ReadPointer;
		ppuLv2TraceHeader* tracehdr = (ppuLv2TraceHeader*)schedtrace;
		if (tracehdr->tag == CELL_PERF_LV2_TRACE_TYPE_PPU_SCHEDULER)
		{
			if (s32(timebase - tracehdr->timebase) < 0)
				return;

			// update hardware thread states
			ppuLv2SchedulerTrace* schedtrc = (ppuLv2SchedulerTrace*)(tracehdr + 1);
			m_HwThreads[tracehdr->hwthreadid] = schedtrc->threadid;
		}
		m_ReadPointer += 2 + tracehdr->length;
		m_ReadPointer %= (SCHED_TRACE_BUF_SIZE - sizeof(CellPerfLv2TraceHeader)) / 4;
	}
}

} // namespace rage

#endif // __PPU && RAGETRACE
