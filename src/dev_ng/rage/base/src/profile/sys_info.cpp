#include "sys_info.h"
#include "system/timer.h"
#include "system/memops.h"

#if defined (_MSC_VER)
#define CPUID(INFO, ID) __cpuid(INFO, ID)
#include <intrin.h> 
#elif defined(__clang__)
# if RSG_CPU_X64
# include <cpuid.h>
#if defined(ORBIS_SDK_VERSION) && ORBIS_SDK_VERSION>1700
# define CPUID(INFO, ID) __cpuid(ID, INFO[0], INFO[1], INFO[2], INFO[3])
#else
# define CPUID(INFO, ID) __get_cpuid(ID, (unsigned int*)&INFO[0], (unsigned int*)&INFO[1], (unsigned int*)&INFO[2], (unsigned int*)&INFO[3])
#endif
# elif RSG_CPU_ARM
# endif
#else
#error Unknown platform!
#endif

#if RSG_WIN32
#include "system/xtl.h"
#endif

namespace rage 
{

static sysCpuInfo g_cpuInfo;

bool InitCpuInfo()
{
#if RSG_CPU_X64
	sysMemSet(&g_cpuInfo, 0, sizeof(g_cpuInfo));

	int cpuInfo[4] = { -1 };

	// Vendor
	CPUID(cpuInfo, 0);
	int cpuLevel = cpuInfo[0];
	(void)cpuLevel;
	*(u32*)(&g_cpuInfo.m_VendorName[0]) = cpuInfo[1];
	*(u32*)(&g_cpuInfo.m_VendorName[4]) = cpuInfo[3];
	*(u32*)(&g_cpuInfo.m_VendorName[8]) = cpuInfo[2];
	if (!strcmp(g_cpuInfo.m_VendorName, "GenuineIntel"))
		g_cpuInfo.m_Vendor = sysCpuInfo::CPU_INTEL;
	else if (!strcmp(g_cpuInfo.m_VendorName, "AuthenticAMD"))
		g_cpuInfo.m_Vendor = sysCpuInfo::CPU_AMD;
	else
		g_cpuInfo.m_Vendor = sysCpuInfo::CPU_OTHER;

	// Model
	CPUID(cpuInfo, 0x80000000);
	unsigned nExIds = cpuInfo[0];
	for (unsigned i = 0x80000000; i <= nExIds; ++i)
	{
		CPUID(cpuInfo, i);
		if (i == 0x80000002)
			sysMemCpy(g_cpuInfo.m_Model, cpuInfo, sizeof(cpuInfo));
		else if (i == 0x80000003)
			sysMemCpy(g_cpuInfo.m_Model + 16, cpuInfo, sizeof(cpuInfo));
		else if (i == 0x80000004)
			sysMemCpy(g_cpuInfo.m_Model + 32, cpuInfo, sizeof(cpuInfo));
	}

	// Cache Size
	// http://softpixel.com/~cwright/programming/simd/cpuid.php
	switch (g_cpuInfo.m_Vendor)
	{
	case sysCpuInfo::CPU_INTEL:
		CPUID(cpuInfo, 1);
		g_cpuInfo.m_CacheLineSize = u8(cpuInfo[1] >> 8) * 8;
		break;

	case sysCpuInfo::CPU_AMD:
		CPUID(cpuInfo, 0x80000005);
		g_cpuInfo.m_CacheLineSize = u8(cpuInfo[2]);
		break;

	case sysCpuInfo::CPU_OTHER:
		g_cpuInfo.m_CacheLineSize = 64;
		break;
	}

	// Frequency
#if RSG_WIN32
	LARGE_INTEGER cpuFrequency;
	QueryPerformanceFrequency(&cpuFrequency);
	g_cpuInfo.m_Frequency = cpuFrequency.QuadPart * (RSG_PC ? 1000 : 1);
#elif RSG_ORBIS
	g_cpuInfo.m_Frequency = sceKernelGetTscFrequency();
#else
	if (cpuLevel >= 0x16)
	{
		CPUID(cpuInfo, 0x16);
		//CPUID.16h.EAX = Processor Base Frequency (in MHz)
		//CPUID.16h.EBX = Maximum Frequency(in MHz)
		//CPUID.16h.ECX = Bus(Reference) Frequency(in MHz)
		g_cpuInfo.m_Frequency = u64(cpuInfo[0]) * 1000000ull;
	}
#endif

	// AVX
	CPUID(cpuInfo, 1);
	g_cpuInfo.m_isAvxSupported = (cpuInfo[2] & (1 << 28)) != 0;
#endif
	return true;
}

const sysCpuInfo& sysGetCpuInfo()
{
	static bool init = InitCpuInfo();
	(void)init;
	return g_cpuInfo;
}

#if defined(__clang__) && defined(RSG_SCARLETT) && RSG_SCARLETT && defined(__cpuid)
// /me slow claps MS. Can't (currently as of clang compiler module verison 12) include cpuid.h and intrin.h due to the clash of __cpuid being declared differently
// Due to unity, intrin.h is included via lz4.c, so undefine __cpuid
#undef __cpuid
#endif

} // namespace rage