//
// profile/group.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "group.h"

#include "bank/bank.h"
#include "string/string.h"
#include "profile/element.h"
#include "profile/page.h"
#include "profile/pfpacket.h"
#include "profile/profiler.h"
#include "system/threadtype.h"
#include "math/amath.h"

using namespace rage;

#if __ASSERT && !__TOOL
#define IS_MAIN_THREAD()	AssertMsg(sysThreadType::IsBeforeMainThreadInit() || sysThreadType::IsUpdateThread(), "Manipulating a pfGroup from outside the main thread? B*1010276")
#else
#define IS_MAIN_THREAD()
#endif

#if __STATS

///////////////////////////////////////////////////////////////////

pfGroup::pfGroup (const char * name, bool initiallyActive)
		: Active(initiallyActive)
{
	TimerList = NULL;
	ValueList = NULL;

	strncpy(Name,name,NameMaxLen-1);
	Name[NameMaxLen-1] = '\0';

#if __BANK
	m_id = pfPacket::GetNewId();

	memset( m_pages, 0, sizeof( pfPage *) * kMaxPages );

	AddToRagProfiler();
#endif

	GetRageProfiler().RegisterGroup(*this);
}

pfGroup::~pfGroup()
{
#if __BANK
	if ( pfPacket::IsConnected() )
	{
		pfPacket packet;
		packet.Begin( pfPacket::DESTROY_GROUP, 0, 0 );
		packet.Write_u32( m_id );
		packet.Send();
	}
#endif
}

void pfGroup::AddTimer (pfTimer * timer)
{
	IS_MAIN_THREAD();
	if (TimerList==NULL)
	{
		TimerList = timer;
	}
	else
	{
		TimerList->AppendAtEnd(timer);
	}
}


void pfGroup::AddValue (pfValue * value)
{
	IS_MAIN_THREAD();
	if (ValueList==NULL)
	{
		ValueList = value;
	}
	else
	{
		ValueList->AppendAtEnd(value);
	}
}


pfTimer * pfGroup::GetNextTimer (pfTimer * curTimer)
{
	if (curTimer==NULL)
	{
		return TimerList;
	}
	else
	{
		return curTimer->GetNext();
	}
}


pfValue * pfGroup::GetNextValue (pfValue * curValue)
{
	if (curValue==NULL)
	{
		return ValueList;
	}
	else
	{
		return curValue->GetNext();
	}
}


#if __BANK
#include <stdio.h>

void pfGroup::AddWidgets (bkBank & bank)
{
	char temp[32 + NameMaxLen];

	sprintf(temp,"group %s - enabled",Name);
	bank.AddToggle(temp,&Active);

	sprintf(temp,"group %s",Name);
	bank.PushGroup(temp,false);

	pfTimer * timer = TimerList;

	while (timer!=NULL)
	{
		timer->AddWidgets(bank);
		timer = timer->GetNext();
	}

	pfValue * value = ValueList;

	while (value != NULL)
	{
		value->AddWidgets(bank);
		value = value->GetNext();
	}

	bank.PopGroup();
}

void pfGroup::AddTraceWidgets(bkBank& bank)
{
	if (TimerList == NULL)
	{
		return;
	}

	char temp[32 + NameMaxLen];
	formatf(temp,32+NameMaxLen,"group %s",Name);
	bank.PushGroup(temp,false);
	pfTimer * timer = TimerList;

	while (timer!=NULL)
	{
		timer->AddTraceWidgets(bank);
		timer = timer->GetNext();
	}
	bank.PopGroup();
}

void pfGroup::AddToRagProfiler()
{
	if ( pfPacket::IsConnected() )
	{
		pfPacket packet;
		packet.Begin( pfPacket::CREATE_GROUP, 0, 0 );
		packet.Write_u32( m_id );
		packet.Write_const_char( Name );
		packet.Send();

		pfTimer *timer = TimerList;
		while ( timer != NULL )
		{
			timer->AddToRagProfiler( *this );
			timer = timer->GetNext();
		}

		pfValue *value = ValueList;
		while ( value != NULL )
		{
			value->AddToRagProfiler( *this );
			value = value->GetNext();
		}
	}
}

bool pfGroup::IsVisible() const
{
	for ( int i = 0; i < kMaxPages; ++i )
	{
		if ( (m_pages[i] != NULL) && m_pages[i]->IsVisible() )
		{
			return true;
		}
	}

	return false;
}

void pfGroup::AddPage( pfPage * page )
{
	IS_MAIN_THREAD();
	ASSERT_ONLY(bool addedPage = false);
	for ( int i = 0; i < kMaxPages; ++i )
	{
		if ( m_pages[i] == NULL )
		{			
			m_pages[i] = page;

			ASSERT_ONLY(addedPage = true);
			break;
		}
	}

	AssertMsg( addedPage , "The size of kMaxPages needs to be increased." );
}

void pfGroup::RemovePage( pfPage * page )
{
	IS_MAIN_THREAD();
	for ( int i = 0; i < kMaxPages; ++i )
	{
		if ( m_pages[i] == page )
		{
			m_pages[i] = NULL;
			break;
		}
	}
}
#endif

void pfGroup::Init ()
{
	int		count;
	
	pfTimer * timer = TimerList;
	count = 0;
	while (timer!=NULL)
	{
		timer = timer->GetNext();
		count++;
	}
	
	timer = TimerList;
	while (timer!=NULL)
	{
		if(!count)
		{
			break;
		}
		
		timer->Init();

		timer = timer->GetNext();
		count--;
	}
	
	pfValue * value = ValueList;
	count = 0;
	while (value!=NULL)
	{
		value = value->GetNext();
		count++;
	}
	
	value = ValueList;
	while (value!=NULL)
	{
		if(!count)
		{
			break;
		}
		
		value->Init();

		value = value->GetNext();
		count--;
	}
}

void pfGroup::Reset ()
{
	IS_MAIN_THREAD();
	pfTimer * timer = TimerList;

	while (timer!=NULL)
	{
		timer->Reset();
		timer = timer->GetNext();
	}
}


void pfGroup::BeginFrame ()
{
	IS_MAIN_THREAD();
	pfTimer * timer = TimerList;

	while (timer!=NULL)
	{
		timer->BeginFrame();
		timer = timer->GetNext();
	}

	// some values (counters) need a BeginFrame call
	pfValue * value = ValueList;
	while (value!=NULL)
	{
		value->BeginFrame();
		value = value->GetNext();
	}
}


void pfGroup::FlipElapsedTime()
{
	IS_MAIN_THREAD();
	pfTimer * timer = TimerList;
	while (timer!=NULL)
	{
		timer->FlipElapsedTime();
		timer = timer->GetNext();
	}
}


void pfGroup::EndFrame ()
{
	IS_MAIN_THREAD();
	pfTimer * timer = TimerList;

	while (timer!=NULL)
	{
		timer->EndFrame();
		timer = timer->GetNext();
	}

	pfValue * value = ValueList;
	while (value!=NULL)
	{
		value->EndFrame();
		value = value->GetNext();
	}
}


void pfGroup::DebugView () const
{
	Displayf("Group (%p) '%s'",this,Name);	

	pfTimer * timer = TimerList;

	while (timer!=NULL)
	{
		Displayf("  Timer '%s' (%p): time (%p) %f",timer->GetName(),this,timer,timer->GetFrameTime());
		timer = timer->GetNext();
	}

	pfValue * value = ValueList;

	while (value!=NULL)
	{
		char displayString[64];
		value->WriteToString(displayString,64);

		const char *name;
		s32 index;
		name = value->GetName(index);
		if(index >= 0)
		//if(value->NumValues() > 1)
		{
			Displayf("  Value '%s_%d' (%p): value (%p) %s",name,index,this,value,displayString);
		}
		else
		{
			Displayf("  Value '%s' (%p): value (%p) %s",name,this,value,displayString);
		}

		value = value->GetNext();
	}
}




#endif
