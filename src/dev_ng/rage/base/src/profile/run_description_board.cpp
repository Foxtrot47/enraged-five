#include "settings.h"

#if USE_PROFILER

#include "run_description_board.h"

#include "string/string.h"
#include "string/stringbuilder.h"
#include "system/param.h"

#include "sys_info.h"

using namespace rage;

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RunDescriptionBoard::Add(const char* name, const char* value)
{
	for (Entry& e : entries)
	{
		if (e.name == name)
		{
			e.value = value;
			return;
		}
	}

	Entry entry;
	entry.name = name;
	entry.value = value;
	entries.PushAndGrow(entry);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RunDescriptionBoard::RunDescriptionBoard() : entries(0, 64)
{
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RunDescriptionBoard& RunDescriptionBoard::Update()
{
	Add("Application Path", sysParam::GetProgramName());

	atStringBuilder builder;

	for (int i = 0; i < sysParam::GetArgCount(); ++i)
	{
		builder.Append(sysParam::GetArg(i));
		builder.Append('\n');
	}

	Add("Command Line", builder.ToString());

	const sysCpuInfo& cpuInfo = sysGetCpuInfo();
	if (cpuInfo.m_Frequency)
	{
		char buffer[256] = { 0 };
		formatf(buffer, "%.3f GHz", float(cpuInfo.m_Frequency / 1000000000.0));
		Add("CPU Frequency", buffer);
	}
	Add("CPU Model", cpuInfo.m_Model);

	return *this;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RunDescriptionBoard& RunDescriptionBoard::Get()
{
	static RunDescriptionBoard board;
	if (board.entries.empty())
		board.Update();
	return board;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator << (OutputDataStream& stream, const RunDescriptionBoard::Entry& ob)
{
	return stream << ob.name << ob.value;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator << (OutputDataStream& stream, const RunDescriptionBoard& ob)
{
	return stream << ob.entries;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

#endif //USE_PROFILER
