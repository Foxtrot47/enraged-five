#include "profile/settings.h"

#if RSG_LINUX

#include "tracer_linux.h"

#include "file/device.h"
#include "file/stream.h"
#include "string/stringutil.h"

#include <cstdio>
#include <cstdlib>

#define TRACE_PATH "/sys/kernel/debug/tracing/"
#define TRACE_BUFFER_SIZE_KB	(32 << 10)

namespace rage
{
//////////////////////////////////////////////////////////////////////////
FTrace g_FTrace;
//////////////////////////////////////////////////////////////////////////
bool FTrace::SetVariable(const char* name, const char* value)
{
	char cmd[256] = { 0 };
	formatf(cmd, "echo %s > " TRACE_PATH "%s", value, name);
	return ExecSudo(cmd);
}

bool FTrace::SetVariable(const char* name, int value)
{
	char cmd[256] = { 0 };
	formatf(cmd, "echo %d > " TRACE_PATH "%s", value, name);
	return ExecSudo(cmd);
}

bool FTrace::SetVariable(const char* name, bool value)
{
	return SetVariable(name, value ? "1" : "0");
}

//////////////////////////////////////////////////////////////////////////
FTrace::FTrace() : m_RootPwd("")
{

}

bool FTrace::SetRootPassword(const char* pwd)
{
	m_RootPwd = pwd;
	return true;
}

bool FTrace::EnableEvent(ftrace::event_type type, bool enabled /*= true*/)
{
	switch (type)
	{
	case ftrace::EVENT_SCHED_SWITCH:
		return SetVariable("events/sched/sched_switch/enable", enabled);
		break;
	case ftrace::EVENT_SCHED_WAKEUP:
		return SetVariable("events/sched/sched_wakeup/enable", enabled);
		break;
	default:
		break;
	}
	return false;
}

bool FTrace::Start()
{
	// Stop tracing in case we've crashed before
	SetVariable("tracing_on", false);
	// Clear output
	SetVariable("trace", "");
	// VS TODO: Support binary output format
	SetVariable("options/bin", false);
	// Raw format is missing some of the critical bits
	SetVariable("options/raw", false);
	// Disable re-sched info
	SetVariable("options/irq-info", false);
	// Setting timer to 'mono' to match sysTimer::GetTick() - clock_gettime(CLOCK_MONOTONIC, &res);
	SetVariable("trace_clock", "mono");
	// Setting buffer size
	SetVariable("buffer_size_kb", TRACE_BUFFER_SIZE_KB);

	return SetVariable("tracing_on", true);
}

bool FTrace::Stop()
{
	return SetVariable("tracing_on", false);
}

bool FTrace::Parse(ftrace::event_callback cb, TimeScope scope)
{
	char cmd[256] = { 0 };
	formatf(cmd, "echo \'%s\' | sudo -S sh -c \'cat " TRACE_PATH "trace\'", m_RootPwd.c_str());

	if (FILE* pipe = popen(cmd, "r"))
	{
		char buffer[512] = { 0 };

		while (fgets(buffer, sizeof(buffer), pipe)) {
			ProcessEvent(buffer, cb, scope);
		}

		pclose(pipe);
		return true;
	}
	return false;
}

struct Parser
{
	const char* cursor;
	const char* text;
	const char* end;

	Parser(const char* t) : text(t), cursor(t), end(text + strlen(text)) {}

	int SkipWhitespaces()
	{
		int count = 0;
		while (isspace(*cursor))
			count += Skip(1);
		return count;
	}

	int SkipDigits()
	{
		int count = 0;
		while (isdigit(*cursor))
			count += Skip(1);
		return count;
	}

	int Skip(int count)
	{
		Assert(cursor + count <= end);
		cursor += count;
		return count;
	}

	int Skip(const char token)
	{
		int count = 0;
		while (*cursor == token)
			count += Skip(1);
		return count;
	}

	int Skip(const char* token)
	{
		return StringStartsWith(cursor, token) ? Skip((int)strlen(token)) : 0;
	}

	int Read(u16& val)
	{
		u32 temp = 0;
		int res = Read(temp);
		Assign(val, temp);
		return res;
	}

	int Read(s32& val)
	{
		val = atoi(cursor);
		int count = SkipWhitespaces() + Skip('-');
		while (isdigit(*cursor))
			count += Skip(1);
		return count;
	}

	int Read(u32& val)
	{
		val = strtoul(cursor, nullptr, 10);
		int count = SkipWhitespaces();
		while (isdigit(*cursor))
			count += Skip(1);
		return count;
	}

	int Find(const char* text)
	{
		const char* ptr = strstr(cursor, text);
		return ptr ? (ptr - cursor) : -1;
	}

	template<size_t _Size>
	int ReadFixedString(char (&dest)[_Size], bool skipWhitespaces = true)
	{
		int whitespaceCount = skipWhitespaces ? SkipWhitespaces() : 0;
		Assert(whitespaceCount < _Size);
		int length = (int)_Size - whitespaceCount;
		strncpy_s(dest, _Size, cursor, length);
		Skip(length);
		return _Size;
	}

	template<size_t _Size>
	int ReadString(char (&dest)[_Size], int maxLength)
	{
		int length = Min(maxLength, (int)_Size - 1);
		strncpy_s(dest, _Size, cursor, length);
		Skip(length);
		return length;
	}

	template<size_t _Size>
	int ReadString(char(&dest)[_Size], const char* delimiter)
	{
		int length = Find(delimiter);
		return ReadString(dest, length);
	}
};

bool FTrace::ProcessEvent(const char* buffer, ftrace::event_callback cb, TimeScope scope)
{
	Parser parser(buffer);

	if (parser.Skip('#'))
		return false;

	// [Example]  systemd-journal-396   [007] 125479.900521:

	u32 timeInt = 0, timeFrac = 0;
	ftrace::event_header header = { 0 };
	parser.ReadFixedString(header.task, true);
	parser.Skip("-");
	parser.Read(header.pid);
	parser.SkipWhitespaces();
	parser.Skip('[');
	parser.Read(header.cpuid);
	parser.Skip(']');
	parser.SkipWhitespaces();
	parser.Read(timeInt);
	parser.Skip('.');
	parser.Read(timeFrac);
	header.timestamp = ((s64)timeInt * 1000000 + (s64)timeFrac) * 1000;

	if (header.timestamp < scope.m_Start || scope.m_Finish > header.timestamp)
		return false;
	
	parser.Skip(':');
	parser.SkipWhitespaces();

	if (parser.Skip("sched_switch:"))
	{
		// [Example] sched_switch: prev_comm=systemd-journal prev_pid=396 prev_prio=120 prev_state=R+ ==> next_comm=in:imuxsock next_pid=1399 next_prio=120

		ftrace::sched_switch ev;
		
		parser.Skip(" prev_comm=");
		parser.ReadString(ev.prev_comm, " prev_pid=");
		
		parser.Skip(" prev_pid=");
		parser.Read(ev.prev_pid);

		parser.Skip(" prev_prio=");
		parser.Read(ev.prev_prio);
		
		parser.Skip(" prev_state=");
		char temp[16];
		parser.ReadString(temp, " ==>");

		parser.Skip(" ==>");

		parser.Skip(" next_comm=");
		parser.ReadString(ev.next_comm, " next_pid=");

		parser.Skip(" next_pid=");
		parser.Read(ev.next_pid);

		parser.Skip(" next_prio=");
		parser.Read(ev.next_prio);

		cb(header, ev);
	}
	 
	return true;
}

bool FTrace::ExecSudo(const char* cmd)
{
	char buffer[256] = { 0 };
	formatf(buffer, "echo \'%s\' | sudo -S sh -c \'%s\' 2> /dev/null", m_RootPwd.c_str(), cmd);
	return Exec(buffer);
}

bool FTrace::Exec(const char* cmd)
{
	int result = system(cmd);
	return result == 0;
}

//////////////////////////////////////////////////////////////////////////
FTrace& FTrace::Get()
{
	return g_FTrace;
}
//////////////////////////////////////////////////////////////////////////
}

#endif // RSG_LINUX

