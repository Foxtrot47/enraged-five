#ifndef STREAM_PROFILER_H
#define STREAM_PROFILER_H

#include "settings.h"

#if USE_PROFILER

#include "common.h"
#include "serialization.h"

// Rage
#include "file/device.h"

#define USE_PROFILER_IO (__DEVPROF && USE_PROFILER_NORMAL & 0)

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if USE_PROFILER_IO
struct DeviceProfiler : public rage::fiDeviceProfiler
{
	EventDescription* readDescription;
	EventDescription* writeDescription;

	struct Desc
	{
		TagDescription* tag;
		EventDescription* event;
	};
	rage::atMap<fiHandle, Desc> openedHandles;

	DeviceProfiler();
	~DeviceProfiler();

	void StartEvent(fiHandle handle, const Profiler::EventDescription* desc, size_t size);
	void FinishEvent(fiHandle handle);

	const Desc* RegisterHandle(fiHandle handle, const char* file);

	virtual void OnRegister(fiHandle handle, const char* file) override;
	virtual void OnOpen(fiHandle handle, const char *file) override;
	virtual void OnRead(fiHandle handle, size_t size, rage::u64 offset) override;
	virtual void OnReadInternal(fiHandle handle, size_t size, rage::u64 offset) override;
	virtual void OnReadEnd(fiHandle handle, size_t size) override;
	virtual void OnWrite(fiHandle handle, size_t size) override;
	virtual void OnWriteEnd(fiHandle handle) override;
	virtual void OnSeek(fiHandle handle, rage::u64 offset) override;
	virtual void OnClose(fiHandle handle) override;

	virtual bool OnPushEvent(Profiler::EventDescription* desc) override;
	virtual void OnPopEvent() override;
};
#endif // (__DEVPROF && USE_PROFILER_BASIC)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER_IO

#endif //STREAM_PROFILER_H