#ifndef SYM_ENGINE_H
#define SYM_ENGINE_H

#if USE_PROFILER

#include "common.h"
#include "rocky.h"

#include "system/xtl.h"

#include <cstring>

#include "atl/array.h"
#include "atl/map.h"
#include "atl/vector.h"
#include "system/stl_wrapper.h"

#include "serialization.h"

#define ROCKY_USE_DBG_HELP 0

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct Symbol
{
	rage::u64 address;
	rage::u64 offset;
	rage::stlWstring module;
	rage::stlWstring file;
	rage::stlWstring function;
	rage::u32		 line;
	Symbol() : line(0), offset(0), address(0) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct Module
{
	rage::stlWstring name;
	void* baseAddress;
	size_t size;

	Module() : baseAddress(0), size(0) {}
	Module(const rage::stlWstring& moduleName, void* moduleBaseAddress, size_t moduleSize) : name(moduleName), baseAddress(moduleBaseAddress), size(moduleSize) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ProcessInfo
{
	rage::atString m_Name;
	rage::u32 m_PID;
	rage::u64 m_WorkingSetSize;
	rage::u64 m_CommitSize;
	ProcessInfo() : m_PID(0), m_WorkingSetSize(0), m_CommitSize(0) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const Module& ob);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct CallStackBuffer
{
	rage::u64 data[MAX_CALLSTACK_DEPTH];
	void Clear();
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef rage::atMap<rage::u64, Symbol*> SymbolCache;
typedef rage::atArray<ProcessInfo> ProcessList;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class SymEngine
{
	SymbolCache* cache;

	bool isInitialized;

	rage::atVector<Module> loadedModules;

#if ROCKY_USE_DBG_HELP
	HANDLE hProcess;
	bool needRestorePreviousSettings;
	DWORD previousOptions;
	static const size_t MAX_SEARCH_PATH_LENGTH = 2048;
	wchar_t previousSearchPath[MAX_SEARCH_PATH_LENGTH];
#endif
public:
	SymEngine();
	~SymEngine();

	void Init();
	void Close();
	void Clear();

	// Get Module from the list
	const Module* GetModule(rage::u64 dwAddress);
	// Get the list of registered Modules
	const rage::atVector<Module>& GetModules() const;
	// Get the list of registered Modules
	bool GetProcesses(ProcessList& out) const;
	// Get Symbol from PDB file
	const Symbol* ResolveSymbol(rage::u64 dwAddress);
	// Get or Create symbol
	Symbol* GetOrCreateSymbol(rage::u64 dwAddress);

#if ROCKY_USE_DBG_HELP
	// Collects Callstack
	int GetCallstack(volatile const HANDLE& hThread, CONTEXT& context, CallStackBuffer& callstack);
#endif
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER

#endif //SYM_ENGINE_H
