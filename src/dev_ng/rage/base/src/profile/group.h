//
// profile/group.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

//
// Groups collect profile elements together.
// Groups, in turn, are managed by the pfProfiler 
// class and are placed into pfPage's for display.
//

#ifndef PROFILE_GROUP_H
#define PROFILE_GROUP_H

/////////////////////////////////////////////////////////////////
// external defines

#include "diag/stats.h"

#if !__STATS

///////////////////////////////////////////////////////////////////
// empty macro definitions for !__STATS

#define PF_GROUP(x)
#define PF_GROUP_OFF(x)
#define EXT_PF_GROUP(x)

#else // __STATS

#include "atl/array.h"
#include "profile/element.h"

namespace rage {

///////////////////////////////////////////////////////////////////
// includes and external class defines

class bkBank;
class pfPage;
class pfValue;

///////////////////////////////////////////////////////////////////
// macros to declare profile groups

// declare a profile group
#define PF_GROUP(name)											::rage::pfGroup PFGROUP_##name(#name)

// declare a profile group that is initially disabled
#define PF_GROUP_OFF(name)										::rage::pfGroup PFGROUP_##name(#name, false)

// declare an extern profile group
#define EXT_PF_GROUP(name)										extern ::rage::pfGroup PFGROUP_##name


///////////////////////////////////////////////////////////////////
// pfGroup
// PURPOSE
//   Holds a list of timers and values (elements).  These elements 
//   are owned and managed by the group.
//
// <FLAG Component>
//
class pfGroup
{
public:
	pfGroup (const char * name, bool initiallyActive = true);// create, name, and register a new group
	~pfGroup();

	const char * GetName ()	const							{return Name;}
	bool GetActive () const									{return Active;}

	void AddTimer (pfTimer * timer);						// add a new timer to this group (only)
	void AddValue (pfValue * value);						// add a new value (int) to this group (only)

	pfTimer * GetNextTimer (pfTimer * curTimer);			// get the next timer, or NULL if end, or first if NULL
	pfValue * GetNextValue (pfValue * curValue);			// get the next value, or NULL if end, or first if NULL

	void Init ();											// do any final initialization on the groups
	void Reset ();											// reset all the contained elements
	void BeginFrame ();										// pass BeginFrame signal to elements
	void EndFrame ();										// pass EndFrame signal to elements
	void FlipElapsedTime();

	void DebugView () const;								// display a simple debug output of the group's elements

#if __BANK
	void AddWidgets (bkBank & bank);						// add this group and its elements to the provided bank
	void AddTraceWidgets(bkBank& bank);						// 
	void AddToRagProfiler();								// if connected, add to Rag Profiler
	u32 GetId() const										{ return m_id; }
	bool IsVisible() const;
	void AddPage( pfPage * page );
	void RemovePage( pfPage * page );
#endif

protected:
	bool Active;											// is this group active

	enum {NameMaxLen=48};
	char Name[NameMaxLen];

	pfTimer * TimerList;									// a list of the timers in the group
	pfValue * ValueList;									// a list of the values in the group

	enum {kMaxPages=16};

	BANK_ONLY( u32 m_id; )
	BANK_ONLY( pfPage * m_pages[kMaxPages]; )
};

///////////////////////////////////////////////////////////////////

}	// namespace rage

#endif	// __STATS

#endif  // PROFILE_GROUP_H
