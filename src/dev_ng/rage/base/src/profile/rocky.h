#ifndef PROFILER_H
#define PROFILER_H

#include "settings.h"

#if OVERRIDE_ROCKY
#include "rocky_override.h"
#endif

#if USE_PROFILER
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atl/array.h"
#include "atl/atfixedstring.h"
#include "atl/string.h"
#include "system/ipc.h"
#include "system/interlocked.h"
#include "system/timer.h"
#include "system/tls.h"
#include "memory_pool.h"
#include "vectormath/vec3v.h"
#include "file/file_config.h"

// For Razor
#include "system/pix.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define ROCKY_CONCAT_IMPL(x, y) x##y
#define ROCKY_CONCAT(x, y) ROCKY_CONCAT_IMPL(x, y)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define ROCKY_FUNC __FUNCTION__
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define ROCKY_PUSH_POP_PANIC_CHECK RSG_DEV
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define USE_ROCKY_LIVE (USE_PROFILER_NORMAL)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace rage { class Vec3V; }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace Profiler
{
	// Source: http://msdn.microsoft.com/en-us/library/system.windows.media.colors(v=vs.110).aspx
	// Image:  http://i.msdn.microsoft.com/dynimg/IC24340.png
	struct Color
	{
		enum Enum : rage::u32
		{
			Null = 0x00000000,
			AliceBlue = 0xFFF0F8FF,
			AntiqueWhite = 0xFFFAEBD7,
			Aqua = 0xFF00FFFF,
			Aquamarine = 0xFF7FFFD4,
			Azure = 0xFFF0FFFF,
			Beige = 0xFFF5F5DC,
			Bisque = 0xFFFFE4C4,
			Black = 0xFF000000,
			BlanchedAlmond = 0xFFFFEBCD,
			Blue = 0xFF0000FF,
			BlueViolet = 0xFF8A2BE2,
			Brown = 0xFFA52A2A,
			BurlyWood = 0xFFDEB887,
			CadetBlue = 0xFF5F9EA0,
			Chartreuse = 0xFF7FFF00,
			Chocolate = 0xFFD2691E,
			Coral = 0xFFFF7F50,
			CornflowerBlue = 0xFF6495ED,
			Cornsilk = 0xFFFFF8DC,
			Crimson = 0xFFDC143C,
			Cyan = 0xFF00FFFF,
			DarkBlue = 0xFF00008B,
			DarkCyan = 0xFF008B8B,
			DarkGoldenRod = 0xFFB8860B,
			DarkGray = 0xFFA9A9A9,
			DarkGreen = 0xFF006400,
			DarkKhaki = 0xFFBDB76B,
			DarkMagenta = 0xFF8B008B,
			DarkOliveGreen = 0xFF556B2F,
			DarkOrange = 0xFFFF8C00,
			DarkOrchid = 0xFF9932CC,
			DarkRed = 0xFF8B0000,
			DarkSalmon = 0xFFE9967A,
			DarkSeaGreen = 0xFF8FBC8F,
			DarkSlateBlue = 0xFF483D8B,
			DarkSlateGray = 0xFF2F4F4F,
			DarkTurquoise = 0xFF00CED1,
			DarkViolet = 0xFF9400D3,
			DeepPink = 0xFFFF1493,
			DeepSkyBlue = 0xFF00BFFF,
			DimGray = 0xFF696969,
			DodgerBlue = 0xFF1E90FF,
			FireBrick = 0xFFB22222,
			FloralWhite = 0xFFFFFAF0,
			ForestGreen = 0xFF228B22,
			Fuchsia = 0xFFFF00FF,
			Gainsboro = 0xFFDCDCDC,
			GhostWhite = 0xFFF8F8FF,
			Gold = 0xFFFFD700,
			GoldenRod = 0xFFDAA520,
			Gray = 0xFF808080,
			Green = 0xFF008000,
			GreenYellow = 0xFFADFF2F,
			HoneyDew = 0xFFF0FFF0,
			HotPink = 0xFFFF69B4,
			IndianRed = 0xFFCD5C5C,
			Indigo = 0xFF4B0082,
			Ivory = 0xFFFFFFF0,
			Khaki = 0xFFF0E68C,
			Lavender = 0xFFE6E6FA,
			LavenderBlush = 0xFFFFF0F5,
			LawnGreen = 0xFF7CFC00,
			LemonChiffon = 0xFFFFFACD,
			LightBlue = 0xFFADD8E6,
			LightCoral = 0xFFF08080,
			LightCyan = 0xFFE0FFFF,
			LightGoldenRodYellow = 0xFFFAFAD2,
			LightGray = 0xFFD3D3D3,
			LightGreen = 0xFF90EE90,
			LightPink = 0xFFFFB6C1,
			LightSalmon = 0xFFFFA07A,
			LightSeaGreen = 0xFF20B2AA,
			LightSkyBlue = 0xFF87CEFA,
			LightSlateGray = 0xFF778899,
			LightSteelBlue = 0xFFB0C4DE,
			LightYellow = 0xFFFFFFE0,
			Lime = 0xFF00FF00,
			LimeGreen = 0xFF32CD32,
			Linen = 0xFFFAF0E6,
			Magenta = 0xFFFF00FF,
			Maroon = 0xFF800000,
			MediumAquaMarine = 0xFF66CDAA,
			MediumBlue = 0xFF0000CD,
			MediumOrchid = 0xFFBA55D3,
			MediumPurple = 0xFF9370DB,
			MediumSeaGreen = 0xFF3CB371,
			MediumSlateBlue = 0xFF7B68EE,
			MediumSpringGreen = 0xFF00FA9A,
			MediumTurquoise = 0xFF48D1CC,
			MediumVioletRed = 0xFFC71585,
			MidnightBlue = 0xFF191970,
			MintCream = 0xFFF5FFFA,
			MistyRose = 0xFFFFE4E1,
			Moccasin = 0xFFFFE4B5,
			NavajoWhite = 0xFFFFDEAD,
			Navy = 0xFF000080,
			OldLace = 0xFFFDF5E6,
			Olive = 0xFF808000,
			OliveDrab = 0xFF6B8E23,
			Orange = 0xFFFFA500,
			OrangeRed = 0xFFFF4500,
			Orchid = 0xFFDA70D6,
			PaleGoldenRod = 0xFFEEE8AA,
			PaleGreen = 0xFF98FB98,
			PaleTurquoise = 0xFFAFEEEE,
			PaleVioletRed = 0xFFDB7093,
			PapayaWhip = 0xFFFFEFD5,
			PeachPuff = 0xFFFFDAB9,
			Peru = 0xFFCD853F,
			Pink = 0xFFFFC0CB,
			Plum = 0xFFDDA0DD,
			PowderBlue = 0xFFB0E0E6,
			Purple = 0xFF800080,
			Red = 0xFFFF0000,
			RosyBrown = 0xFFBC8F8F,
			RoyalBlue = 0xFF4169E1,
			SaddleBrown = 0xFF8B4513,
			Salmon = 0xFFFA8072,
			SandyBrown = 0xFFF4A460,
			SeaGreen = 0xFF2E8B57,
			SeaShell = 0xFFFFF5EE,
			Sienna = 0xFFA0522D,
			Silver = 0xFFC0C0C0,
			SkyBlue = 0xFF87CEEB,
			SlateBlue = 0xFF6A5ACD,
			SlateGray = 0xFF708090,
			Snow = 0xFFFFFAFA,
			SpringGreen = 0xFF00FF7F,
			SteelBlue = 0xFF4682B4,
			Tan = 0xFFD2B48C,
			Teal = 0xFF008080,
			Thistle = 0xFFD8BFD8,
			Tomato = 0xFFFF6347,
			Turquoise = 0xFF40E0D0,
			Violet = 0xFFEE82EE,
			Wheat = 0xFFF5DEB3,
			White = 0xFFFFFFFF,
			WhiteSmoke = 0xFFF5F5F5,
			Yellow = 0xFFFFFF00,
			YellowGreen = 0xFF9ACD32,
		};
	};
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct Filter
	{
		// NOTE: Please talk to Vadim Slyusarev before modifying this enum.
		//		 Filters are very high-level performance categories which directly reflect performance budgets.
		//		 https://hub.rockstargames.com/display/CODE/Performance+Budgets
		enum Type : rage::u32
		{
			Null,

			// Main/Dependency
			AI,
			Physics,
			Network,	
			Streaming,	
			WorldAndPopulation,
			Script,
			Camera,
			ProcessAfterMovement,
			Render,
			BuildDrawList,
			SafeMode,
			Replay,
			Audio,	
			Visibility,

			Animation,
			AnimationPrePhysics,
			AnimationMidPhysics,
			AnimationPreRender,

			Graphics,

			Loco,
			Cloth,
			VFX,
			Security,
			Debug,	
			UI,	
			Other,

			Internal,

			SubRender,

			// GPU
			GPU_Scene,
			GPU_ReflectionMap,
			GPU_Lighting,
			GPU_CascadeShadows,
			GPU_SkyFogClouds,
			GPU_PostFXUI,
			GPU_IdleTime,
			GPU_Debug,
			GPU_Other,

			Count,
		};

		static const char* ToString(Filter::Type type)
		{
			static const char* toString[Count] =
			{
				"Null",
				"AI",
				"Physics",
				"Network",	
				"Streaming",	
				"World and Population",
				"Script",
				"Camera",
				"Process After Movement",
				"Render",
				"Build Draw List",
				"SafeMode",
				"Replay",
				"Audio",	
				"Visibility",
				"Animation",
				"Animation (PrePhysics)",
				"Animation (MidPhysics)",
				"Animation (PreRender)",
				"Graphics",
				"Loco",
				"Cloth",
				"VFX",
				"Security",
				"Debug",	
				"UI",	
				"Other",
				"Internal",
				"SubRender",
				"GPU Scene",
				"GPU ReflectionMap",
				"GPU Lighting",
				"GPU CascadeShadows",
				"GPU SkyFogClouds",
				"GPU PostFXUI",
				"GPU Other",
			};
			
			CompileTimeAssert(Filter::Count == (sizeof(toString) / (sizeof(toString)[0])));
			return toString[type];
		}
	};

	#define MAKE_CATEGORY(FILTER, COLOR) (((rage::u64)FILTER << (32)) | (COLOR))

	struct Category
	{
		rage::u32 color;
		Filter::Type filter;
		Category() : color(Color::Null), filter(Filter::Null) {}
		Category(rage::u64 packed) : color(rage::u32(packed)) 
		{
			filter = (Filter::Type)(packed >> 32);
		}

		bool IsNull() const
		{
			return color == Color::Null && filter == Filter::Null;
		}

		rage::u64 Pack() const
		{
			return MAKE_CATEGORY(filter, color);
		}

		static const rage::u64 PackedNull = 0;
	};

	typedef rage::u64 PackedCategory;

	CompileTimeAssert(sizeof(Category) == sizeof(PackedCategory));

	struct CategoryColor
	{
		// Packing as u64 to be able to be able to change only header for new categories
		#define CATEGORY(NAME, FILTER, COLOR) static const PackedCategory NAME = MAKE_CATEGORY(FILTER, COLOR);

		// None
		CATEGORY(None, Filter::Other, Color::Null);

		// Audio
		CATEGORY(Audio, Filter::Audio, Color::Pink);
		CATEGORY(Sound, Filter::Audio, Color::Pink);

		// AI
		CATEGORY(AI, Filter::AI, 0xFFFF9CE7);
		CATEGORY(AIFullUpdate, Filter::AI, Color::HotPink);
		CATEGORY(AILambda, Filter::AI, 0xFFB2FFB2);
		CATEGORY(AILambdaFullUpdate, Filter::AI, 0xFF7FE27F);
		CATEGORY(AIFollowRoute, Filter::AI, 0xFF9966FF);
		CATEGORY(AILoco, Filter::AI, 0xFF33FFB2);
		CATEGORY(AIPhaseChanged, Filter::AI, Color::DarkOrange);
		CATEGORY(Pathfinding, Filter::AI, Color::LawnGreen);

		// Animation
		CATEGORY(Animation, Filter::Animation, Color::SkyBlue);
		CATEGORY(AnimationPrePhysics, Filter::AnimationPrePhysics, Color::SkyBlue);
		CATEGORY(AnimationMidPhysics, Filter::AnimationMidPhysics, Color::SkyBlue);
		CATEGORY(AnimationPreRender, Filter::AnimationPreRender, Color::SkyBlue);
		CATEGORY(AnimationOther, Filter::Animation, Color::LightBlue);

		// Loco
		CATEGORY(Loco, Filter::Loco, Color::SpringGreen);

		// Cloth
		CATEGORY(Cloth, Filter::Cloth, Color::SeaGreen);

		// Network
		CATEGORY(Network, Filter::Network, Color::Magenta);

		// Population
		CATEGORY(Population, Filter::WorldAndPopulation, Color::MediumTurquoise);
		CATEGORY(MetaPed, Filter::WorldAndPopulation, Color::LimeGreen);

		// Physics
		CATEGORY(Physics, Filter::Physics, Color::LawnGreen);
		CATEGORY(IntegratePosition, Filter::Physics, Color::SteelBlue);		

		// SafeMode
		CATEGORY(SafeMode, Filter::SafeMode, Color::BurlyWood);

		// Physics
		CATEGORY(ProcessAfterMovement, Filter::ProcessAfterMovement, Color::DodgerBlue);

		// Render
		CATEGORY(Lighting, Filter::Other, Color::Khaki);
		CATEGORY(Render, Filter::Render, Color::BurlyWood);
		CATEGORY(BuildDrawList, Filter::BuildDrawList, Color::BurlyWood);
		CATEGORY(Graphics, Filter::Graphics, Color::BurlyWood);
		CATEGORY(SubRender, Filter::SubRender, Color::BurlyWood);


		// Loading
		CATEGORY(Loading, Filter::Streaming, Color::Gold);
		CATEGORY(Resources, Filter::Streaming, Color::Gold);

		// Update
		CATEGORY(InstanceUpdate, Filter::Other, Color::SkyBlue);
		CATEGORY(SceneUpdate, Filter::Other, Color::DodgerBlue);

		// Script
		CATEGORY(Script, Filter::Script, Color::Thistle);

		// UI
		CATEGORY(UI, Filter::UI, Color::Bisque);

		// Security
		CATEGORY(Security, Filter::Security, Color::LightSalmon);

		// Replay
		CATEGORY(Replay, Filter::Replay, Color::SlateGray);
		CATEGORY(ReplayAnim, Filter::Replay, Color::SlateBlue);

		// Visibility
		CATEGORY(Visibility, Filter::Visibility, Color::PapayaWhip);
		CATEGORY(VisibilityWait, Filter::Visibility, Color::Tomato);
		CATEGORY(Attachments, Filter::Visibility, Color::DodgerBlue);
		CATEGORY(PreRender, Filter::Visibility, Color::BurlyWood);

		// FX
		CATEGORY(VisualEffects, Filter::VFX, Color::Sienna);

		// Camera
		CATEGORY(Camera, Filter::Camera, Color::DodgerBlue);

		// Internal
		CATEGORY(Empty, Filter::Internal, Color::White);
		CATEGORY(Wait, Filter::Internal, Color::Tomato);

		// Other
		CATEGORY(Other, Filter::Other, Color::Plum);

		// Debug
		CATEGORY(Debug, Filter::Debug, Color::Black);
		CATEGORY(Log, Filter::Debug, Color::DeepPink);
		CATEGORY(Tools, Filter::Debug, Color::Gold);
		CATEGORY(DebugRender, Filter::Debug, Color::BurlyWood);

		// GPU
		CATEGORY(GPU_Scene, Filter::GPU_Scene, Color::DodgerBlue);
		CATEGORY(GPU_ReflectionMap, Filter::GPU_ReflectionMap, Color::DeepPink);
		CATEGORY(GPU_Lighting, Filter::GPU_Lighting, Color::Khaki);
		CATEGORY(GPU_CascadeShadows, Filter::GPU_CascadeShadows, Color::DarkGray);
		CATEGORY(GPU_SkyFogClouds, Filter::GPU_SkyFogClouds, Color::SkyBlue);
		CATEGORY(GPU_PostFXUI, Filter::GPU_PostFXUI, Color::Sienna);
		CATEGORY(GPU_IdleTime, Filter::GPU_IdleTime, Color::Purple);
		CATEGORY(GPU_Debug, Filter::GPU_Debug, Color::Yellow);
		CATEGORY(GPU_Other, Filter::GPU_Other, Color::Plum);
	};
}

namespace rage { class Vec3V; }

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ProcessDescription
{
	rage::atString processName;
	rage::atString commandLine;
	rage::u64 uniqueKey;
	rage::u32 processId;
	ProcessDescription(rage::u64 key, rage::u32 pid, const char* name, const char* cmdLine) : uniqueKey(key), processId(pid), processName(name), commandLine(cmdLine) {}
	ProcessDescription() : uniqueKey((rage::u64)-1), processId((rage::u32)-1) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ThreadDescription
{
	// Thread ID of the thread
	rage::sysIpcCurrentThreadId threadId;

	// SystemThread ID of the thread (matches system thread ID: on Orbis it is not equal to threadId)
	rage::u64 systemThreadId;

	rage::atFixedString<64> name;
	rage::u32 maxDepth;
	rage::s32 priority;
	rage::u32 threadMask;
	rage::u32 processId;
	rage::u64 baseAddress;

	bool registered;

	ThreadDescription(const char* threadName, rage::u32 depth = 1, rage::s32 threadPriority = 0, rage::sysIpcCurrentThreadId threadId = sysIpcCurrentThreadIdInvalid, rage::u32 mask = 0, rage::u32 pid = (rage::u32)-1);
	ThreadDescription();

	bool IsReal() const;

	static bool Register(const char* threadName = "MainThread", rage::u32 depth = 1);
	static bool Unregister(rage::sysIpcCurrentThreadId threadId);
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EventDescription;
struct Frame;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct GPUFrame
{
	rage::u32 m_Frame;
	rage::u64 m_Present;
	rage::u64 m_Queued;
	rage::u64 m_Complete;
	rage::u64 m_Processing;
	rage::u64 m_VSync;
	rage::u64 m_Flip;

	GPUFrame() {}
	GPUFrame(rage::u32 frame, rage::u64 present, rage::u64 queued, rage::u64 complete, rage::u64 processing, rage::u64 vsync, rage::u64 flip)
		: m_Frame(frame), m_Present(present), m_Queued(queued), m_Complete(complete), m_Processing(processing), m_VSync(vsync), m_Flip(flip) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void NextFrame();
void NextGpuFrame(rage::u32 frameNumber, rage::u64 timestamp);
void NextGpuFlip(const GPUFrame& gpuFrame);
void AddInternalTags();
const char* GenerateCaptureName();
void SetListenPort(int port);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
enum FrameType
{
	FRAME_CPU,
	FRAME_GPU,
	FRAME_VSYNC,
	FRAME_COUNT,
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
EventDescription* GetMainFrameEventDescription(FrameType type);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct Timestamp
{
	rage::u32 start;

	static const rage::u32 Invalid = 0xFFFFFFFF;
#if RSG_PC
	static const rage::u32 TimePrecisionCut = 0;
#else
	static const rage::u32 TimePrecisionCut = 8;
#endif

	static ROCKY_INLINE rage::u32 Pack(rage::u64 time)
	{
		return (rage::u32)(time >> TimePrecisionCut);
	}

	static ROCKY_INLINE rage::u64 Unpack(rage::u32 time)
	{
		return rage::u64(time) << TimePrecisionCut;
	}

	static ROCKY_INLINE float ToMs(rage::u32 packedDuration)
	{
		return ((rage::u64)packedDuration << TimePrecisionCut) * rage::sysTimer::GetTicksToMilliseconds();
	}

	ROCKY_INLINE void Start() { start = Pack(rage::sysTimer::GetTicks()); }
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EventTime : public Timestamp
{
	rage::u32 finish;

	ROCKY_INLINE EventTime() {}
	ROCKY_INLINE EventTime(rage::u32 begin, rage::u32 end) { start = begin; finish = end; }

	ROCKY_INLINE void Stop() { finish = Pack(rage::sysTimer::GetTicks()); }
	ROCKY_INLINE void Start() { Timestamp::Start(); finish = Timestamp::Invalid; }

	// Cold Functions
	ROCKY_INLINE bool IsIntersect(Timestamp other) const { return start <= other.start && other.start <= finish; }
	ROCKY_INLINE bool IsValid() const { return start <= finish && finish != Timestamp::Invalid; }
	ROCKY_INLINE bool IsFinished() const { return finish != Timestamp::Invalid; }
	ROCKY_INLINE bool operator<(const EventTime& other) const { return start < other.start ? true : (finish < other.finish); }
	ROCKY_INLINE bool operator==(const EventTime& other) const { return start == other.start && finish == other.finish; }

	rage::u32 CalcIntersect(EventTime other) const;

	static const size_t SerializationSize = sizeof(rage::u32) * 2; // Timestamp x2
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EventData : public EventTime
{
	const EventDescription* description;

	EventData() {}
	EventData(const EventDescription* desc, rage::u32 eventStart, rage::u32 eventFinish) : description(desc), EventTime(eventStart, eventFinish) {}

	static const size_t SerializationSize = EventTime::SerializationSize + sizeof(rage::u32); // + index
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const int ROCKY_HARDWARE_EVENT_COUNT = 4;
void GetHWCounters(rage::u64 counters[ROCKY_HARDWARE_EVENT_COUNT]);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct LiveEventData : EventData
{
	EventData* child;
	rage::u64 hwEvents[ROCKY_HARDWARE_EVENT_COUNT];

	void StartLive();
	void StopLive();
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TDescription>
class DescriptionBoard;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct TagDescription
{
	enum Flags : rage::u8
	{
		IS_LIVE = 1 << 0,
	};

	char* name;
	rage::u32 index;
	rage::u8 flags;

	bool IsGroup() const { 	return strchr(name, '\\') != nullptr; }

	static TagDescription* Create(const char* tagName, rage::u8 flags = 0);
	static TagDescription* CreateShared(const char* eventName);
protected:
	friend class DescriptionBoard<TagDescription>;

	void Init(const char* tagName);
	TagDescription(rage::u32 descriptionIndex);
	~TagDescription();
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EventDescription : TagDescription
{
	const char* file;
	rage::u32 line;
	Category category;

	float budget;

	enum Flags : rage::u8
	{
		IS_SAMPLING = 1 << 0,
		IS_LIVE		= 1 << 1,
		IS_RESOURCE = 1 << 2,
	};

	rage::u8 flags;

	ROCKY_INLINE bool IsSampling() const { return (flags & IS_SAMPLING) > 0; }
	ROCKY_INLINE bool IsLive() const { return (flags & IS_LIVE) > 0; }
	ROCKY_INLINE bool IsResource() const { return (flags & IS_RESOURCE) > 0; }

	static EventDescription* Create(const char* eventName, const char* fileName = nullptr, const unsigned long fileLine = 0, const PackedCategory cat = Category::PackedNull, rage::u8 defaultFlags = 0, float eventBudget = 0.0f);
	static EventDescription* CreateShared(const char* eventName);
private:
	friend class DescriptionBoard<EventDescription>;
	EventDescription(rage::u32 descriptionIndex);
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct Tag
{
	const TagDescription* description;
	Timestamp time;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TData>
struct DataTag : public Tag
{
	TData data;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<>
struct DataTag<void> : public Tag {};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct Mode
{
	enum Type : rage::u32
	{
		OFF							= 0x0,
		INSTRUMENTATION_CATEGORIES	= (1 << 0),
		INSTRUMENTATION_EVENTS		= (1 << 1),
		INSTRUMENTATION				= (INSTRUMENTATION_CATEGORIES | INSTRUMENTATION_EVENTS),
		SAMPLING					= (1 << 2),
		TAGS						= (1 << 3),
		AUTOSAMPLING				= (1 << 4),
		SWITCH_CONTEXT				= (1 << 5),
		IO							= (1 << 6),
		GPU							= (1 << 7),
		END_SCREENSHOT				= (1 << 8),
		SCRIPTS						= (1 << 9),
		MEMORY						= (1 << 10),
		HW_COUNTERS					= (1 << 11),
		LIVE						= (1 << 12),
		MT_RESOURCE					= (1 << 13),
		STATS_DRAWCALLS				= (1 << 14),
		GPU_CAPTURE					= (1 << 15),
		VIDEO_CAPTURE				= (1 << 16),
		OTHER_PROCESSES				= (1 << 17),
		COMPACT						= (1 << 18),
        NETWORK_BANDWIDTH_CAPTURE   = (1 << 19),
		LIVE_GUI					= (1 << 20),
		PAGE_FAULTS					= (1 << 21),
		SYS_CALLS					= (1 << 22),
		SWITCH_CONTEXT_CALLSTACKS	= (1 << 23),
		LOGGING						= (1 << 24),
		UNUSED_1					= (1 << 25),
		SCRIPTS_AUTOSAMPLING		= (1 << 26),

		DEFAULT 					= 0xFFFFFFFF & ~SCRIPTS & ~SAMPLING & ~MEMORY & ~HW_COUNTERS & ~LIVE & ~MT_RESOURCE & ~STATS_DRAWCALLS & ~GPU_CAPTURE & ~VIDEO_CAPTURE & ~COMPACT & ~NETWORK_BANDWIDTH_CAPTURE & ~SYS_CALLS & ~SWITCH_CONTEXT_CALLSTACKS & ~LOGGING,
		DEFAULT_CATEGORIES			= INSTRUMENTATION_CATEGORIES | SWITCH_CONTEXT | END_SCREENSHOT | IO | GPU | LIVE_GUI,
		DEFAULT_SMOKETEST			= INSTRUMENTATION | SWITCH_CONTEXT | END_SCREENSHOT | IO | GPU | OTHER_PROCESSES | AUTOSAMPLING | TAGS,
		DEFAULT_STARTUP				= INSTRUMENTATION_CATEGORIES | IO | SWITCH_CONTEXT | AUTOSAMPLING | LIVE_GUI,
		FULL						= 0xFFFFFFFF & ~SAMPLING & ~LIVE & ~COMPACT,

		ALL							= 0xFFFFFFFF
	};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AttachCallStack(rage::u32 mode, rage::u32 timestamp);
void AttachCallStack(rage::u32 mode, rage::u32 timestamp, size_t* addresses, size_t count);
size_t* CreateCallStack(rage::u32 mode, rage::u32 timestamp);
void AttachCaptureInfo(const char* name, const char* value);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
enum ControlState
{
	ROCKY_START,
	ROCKY_STOP,
};
typedef void (*CaptureControlCallback)(ControlState state, rage::u32 mode);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RegisterCaptureControlCallback(CaptureControlCallback callback);
void UnRegisterCaptureControlCallback(CaptureControlCallback callback);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
enum EventState : rage::u8
{
	ROCKY_EVENT_START,
	ROCKY_EVENT_STOP,
};
typedef void(*CaptureCategoryCallback)(const EventDescription* desc, rage::u64 timestamps, EventState state);
void RegisterCaptureCategoryCallback(CaptureCategoryCallback callback, rage::sysIpcCurrentThreadId threadId = sysIpcCurrentThreadIdInvalid);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ScriptSymbol
{
	rage::atFixedString<128> m_Name;
	rage::atFixedString<256> m_File;
	rage::u32 m_Line;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef bool(*ResolveScriptSymbolCallback)(size_t address, ScriptSymbol& outSymbol);
void RegisterResolveScriptSymbolCallback(ResolveScriptSymbolCallback callback);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct LiveData
{
	const EventDescription* description;
	float duration;
	LiveData() : description(nullptr), duration(0.0f) {}
	LiveData(const EventDescription* desc, float dur) : description(desc), duration(dur) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef void(*LiveDataCallback)(rage::atArray<LiveData>& liveData);
void RegisterLiveDataCallback(LiveDataCallback callback);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct PageFaultStats
{
	float m_DurationMs;
	int m_Count;
	void Add(float duration) { m_DurationMs += duration; m_Count += 1; }
	void Reset() { m_DurationMs = 0.0f; m_Count = 0; }
	PageFaultStats() { Reset(); }
};
PageFaultStats GetPageFaultStats();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct Image
{
	const char* path;
	Image() {}
	Image(const char* filePath) : path(filePath) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const rage::u32 MAX_CALLSTACK_DEPTH = 32;
struct CallStackEntry
{
	size_t callstack[MAX_CALLSTACK_DEPTH];
	Timestamp time;
	rage::u32 mode;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if USE_PROFILER_BANDWIDTH_TRACKING
struct NetworkBandwidthEntry
{
    static const rage::u32 MAX_BANDWIDTH_DATA = 10000;
    rage::u32 length;
    rage::u8 data[MAX_BANDWIDTH_DATA];
    rage::u32 frameCount;
};
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ResourceMode 
{
	enum Type : rage::u8
	{
		NONE		= 0,	
		READ		= (1 << 0),
		WRITE		= (1 << 1),
		SHARED		= (1 << 2),
	};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ResourceData
{
	const EventDescription* source;
	const EventDescription* description;
	Timestamp time;
	rage::u8 mode;
	//[Padding] 3 bytes
};
//////////////////////////////////////////////////////////////////////////
struct Drawcall 
{
	const EventDescription* renderPhase;
	const TagDescription* instanceName;
	const TagDescription* shaderName;
	rage::Vec3V position;
	rage::u64 entityID;
	rage::u64 gpuStart;
	rage::u64 gpuFinish;
	rage::u32 primitiveCount;
	rage::u32 instanceCount;
	rage::u8 drawcallType; 
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef MemoryPool<EventData, 1024 * 32> EventBuffer;
typedef MemoryPool<EventData, 1024> GPUBuffer;
#if USE_PROFILER_BANDWIDTH_TRACKING
typedef MemoryPool<NetworkBandwidthEntry, 1024> NetworkBandwidthBuffer;
#endif
typedef MemoryPool<CallStackEntry, 4096> CallStackEntryBuffer;
const rage::u32 MAX_START_STOP_STACK_SIZE = 64;
typedef char RockyShortString[32];
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef DataTag<void> TagVoid;
typedef DataTag<float> TagFloat;
typedef DataTag<rage::u32> TagU32;
typedef DataTag<rage::s32> TagS32;
typedef DataTag<rage::u64> TagU64;
typedef DataTag<rage::Vec3V> TagVec3;
typedef DataTag<RockyShortString> TagString;
typedef DataTag<Image> TagImage;
typedef DataTag<Drawcall> TagDrawcall;
#if RSG_IOS
typedef DataTag<size_t> TagSizet;
#endif //RSG_IOS
typedef MemoryPool<TagVoid, 8 * 1024> TagBuffer;
typedef MemoryPool<TagFloat, 2 * 1024> TagFloatBuffer;
typedef MemoryPool<TagU32, 2 * 1024> TagU32Buffer;
typedef MemoryPool<TagS32, 2 * 1024> TagS32Buffer;
typedef MemoryPool<TagU64, 2 * 1024> TagU64Buffer;
typedef MemoryPool<TagVec3, 2 * 1024> TagVec3Buffer;
typedef MemoryPool<TagString, 64> TagStringBuffer;
typedef MemoryPool<TagImage, 4> TagImageBuffer;
typedef MemoryPool<TagDrawcall, 2048> TagDrawcallBuffer;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef MemoryPool<LiveEventData, 2048> LiveEventBuffer;
typedef MemoryPool<TagString, 16> LiveTagBuffer;
const rage::u32 LIVE_EVENT_BUFFER_COUNT = 3;
const rage::u32 LIVE_EVENT_FRAME_LATENCY = LIVE_EVENT_BUFFER_COUNT - 1;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef MemoryPool<ResourceData, 1024> ResourceBuffer;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void tagcpy(TagString&dest, const char *src);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EventStorage
{
	// Hot section! Be very carefull with the order!
	rage::u32 mode;
	
	// Some padding here, but we need to keep "mode" at the top

	// Main event collection buffer
	EventBuffer eventBuffer;

	// Careful with the order of the fields - it's organized in a way to minimize the number of cache misses
	TagBuffer tags;
	TagU64Buffer tagsU64;
	TagS32Buffer tagsS32;
	TagU32Buffer tagsU32;
	TagFloatBuffer tagsFloat;
	TagVec3Buffer tagsVec3;
	TagStringBuffer tagsString;
	TagImageBuffer tagsImage;
	TagDrawcallBuffer tagsDrawcall;

	// Start/Stop stack to support old style Push/Pop macros
	rage::atFixedArray<EventData*, MAX_START_STOP_STACK_SIZE> startStopStack;

	CaptureCategoryCallback captureCategoryCallback;

	CallStackEntryBuffer callstackBuffer;

	// Main GPU collection buffer
	GPUBuffer gpuBuffer;

	uint samplingCounter;

	rage::u8 index;

	rage::u8 liveBufferIndex;

	struct LiveBuffer
	{
		LiveEventBuffer events;
		LiveTagBuffer tags;
	};

	LiveBuffer liveBuffer[LIVE_EVENT_BUFFER_COUNT];

#if USE_PROFILER_FOR_MT_RESOURCES
	ResourceBuffer resourceUsageBuffer;
	ResourceBuffer resourceSignalBuffer;
#endif

	void SwapLiveBuffer() { liveBufferIndex = (liveBufferIndex + 1) % LIVE_EVENT_BUFFER_COUNT; }
	LiveBuffer& GetLiveBackBuffer() { return liveBuffer[(liveBufferIndex + 1) % LIVE_EVENT_BUFFER_COUNT]; }

	EventStorage() : mode(Mode::OFF), captureCategoryCallback(nullptr), samplingCounter(0), liveBufferIndex(0) {}

	ROCKY_INLINE EventData& NextEvent() { return eventBuffer.Add(); }
	ROCKY_INLINE LiveEventData& NextLiveEvent() { return liveBuffer[liveBufferIndex].events.Add(); }
	ROCKY_INLINE TagString& NextLiveTag() { return liveBuffer[liveBufferIndex].tags.Add(); }

	template<class TTag>
	ROCKY_INLINE TTag& NextTag();

	template<class T>
	ROCKY_INLINE void AddTag(const TagDescription* desc, rage::u32 timestamp, T value)
	{
		DataTag<T>& tag = NextTag<DataTag<T>>();
		tag.description = desc;
		tag.time.start = timestamp;
		tag.data = value;
	}

	ROCKY_INLINE EventData& AddEvent(const EventDescription* desc, rage::u32 start, rage::u32 finish)
	{
		EventData& data = NextEvent();
		data.description = desc;
		data.start = start;
		data.finish = finish;

#if USE_ROCKY_LIVE
		if (mode & Mode::LIVE)
		{
			if (desc && desc->IsLive())
			{
				LiveEventData& liveData = NextLiveEvent();
				liveData.description = desc;
				liveData.start = start;
				liveData.finish = finish;
				liveData.child = &data;
			}
		}
#endif
		return data;
	}

	// Free all temporary memory
	void Clear(bool preserveContent);

	// Reserves minimum required memory required for each tag
	void ReserveMinimumMemory();

	void ClearEvents(bool preserveContent);
	void ClearTags(bool preserveContent);
	void ClearCallstacks(bool preserveContent);
	void ClearLiveBuffers(bool preserveContent);
	void ClearResources(bool preserveContent);
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<> ROCKY_INLINE TagVoid& EventStorage::NextTag<TagVoid>() { return tags.Add(); }
template<> ROCKY_INLINE TagFloat& EventStorage::NextTag<TagFloat>() { return tagsFloat.Add(); }
template<> ROCKY_INLINE TagU32& EventStorage::NextTag<TagU32>() { return tagsU32.Add(); }
template<> ROCKY_INLINE TagS32& EventStorage::NextTag<TagS32>() { return tagsS32.Add(); }
template<> ROCKY_INLINE TagU64& EventStorage::NextTag<TagU64>() { return tagsU64.Add(); }
template<> ROCKY_INLINE TagVec3& EventStorage::NextTag<TagVec3>() { return tagsVec3.Add(); }
template<> ROCKY_INLINE TagString& EventStorage::NextTag<TagString>() { return tagsString.Add(); }
template<> ROCKY_INLINE TagImage& EventStorage::NextTag<TagImage>() { return tagsImage.Add(); }
template<> ROCKY_INLINE TagDrawcall& EventStorage::NextTag<TagDrawcall>() { return tagsDrawcall.Add(); }
#if RSG_IOS
template<> ROCKY_INLINE TagSizet& EventStorage::NextTag<TagSizet>()
{
	TagU64& t = tagsU64.Add();
	return *(TagSizet*)&t;
}
#endif //RSG_IOS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct TLS
{
	static __THREAD EventStorage* storage; // Active Frame (is used as buffer)
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<rage::u32 EVENT_MASK>
struct EventBase
{
	EventData* data;

	static ROCKY_INLINE EventData* Start(const EventDescription* description)
	{
		EventStorage* storage = TLS::storage;
		if (!storage || !(storage->mode & EVENT_MASK))
			return nullptr;

		return StartInternal(storage, description);
	}

	static ROCKY_INLINE void Stop(EventData* eventData)
	{
		// If event has already been stopped, it has been overwritten.
		if (!eventData || eventData->IsFinished())
			return;

		EventStorage* storage = TLS::storage;
		if (storage && storage->mode != Mode::OFF)
		{
			eventData->Stop();

#if USE_PROFILER_NORMAL
			if (storage->mode & (Mode::LIVE | Mode::COMPACT))
				StopLiveOrCompact(storage, eventData);

			if (EVENT_MASK == Mode::INSTRUMENTATION_CATEGORIES)
				if (CaptureCategoryCallback cb = storage->captureCategoryCallback)
					cb(eventData->description, Timestamp::Unpack(eventData->start), ROCKY_EVENT_STOP);
#endif
		}
	}

	ROCKY_INLINE EventBase( const EventDescription* description )
	{
		data = Start(description);
	}

	ROCKY_INLINE ~EventBase()
	{
		Stop(data);
	}

	// Time threshold for the compact mode and callstack mode
	static rage::u32 TimeThreshold;

private:
	static ROCKY_NOINLINE EventData* StartInternal(EventStorage* storage, const EventDescription* description)
	{
		EventData* data = &storage->NextEvent();
		data->description = description;
		data->Start();
		
	#if USE_ROCKY_LIVE
		if (storage->mode & Mode::LIVE)
		{
			if (description && description->IsLive())
			{
				LiveEventData& liveData = storage->NextLiveEvent();
				// Mark event as not complete
				liveData.Start();
				liveData.description = description;
				liveData.child = data;
				data = &liveData;
				liveData.StartLive();
			}
		}

		if (EVENT_MASK == Mode::INSTRUMENTATION_CATEGORIES)
			if (CaptureCategoryCallback cb = storage->captureCategoryCallback)
				cb(description, Timestamp::Unpack(data->start), ROCKY_EVENT_START);
	#endif

		return data;
	}

	static ROCKY_NOINLINE void StopLiveOrCompact(EventStorage* storage, EventData* eventData)
	{
		if (storage->mode & (Mode::LIVE))
		{
			if (eventData->description && eventData->description->IsLive())
			{
				LiveEventData& liveData = *(LiveEventData*)eventData;
				liveData.StopLive();
				if (liveData.child)
					liveData.child->finish = liveData.finish;
			}
		}
		else if (storage->mode & (Mode::COMPACT))
		{
			if (eventData->finish - eventData->start < TimeThreshold)
			{
				storage->eventBuffer.TryPop(eventData);
			}
		}
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef EventBase<Mode::INSTRUMENTATION_EVENTS> Event; 
typedef EventBase<Mode::INSTRUMENTATION_CATEGORIES> CategoryEvent; 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct StackedEvent
{
	static ROCKY_INLINE void StackPush(const EventDescription* description)
	{
		if (EventStorage* storage = TLS::storage)
		{
			if (storage->mode & Mode::INSTRUMENTATION_EVENTS)
			{
				storage->startStopStack.Push(Event::Start(description));
			}
		}
	}

	static ROCKY_INLINE void StackPushForce(const EventDescription* description)
	{
		if (EventStorage* storage = TLS::storage)
		{
			storage->startStopStack.Push(EventBase<Mode::ALL>::Start(description));
		}
	}

	static ROCKY_INLINE void StackPop(const EventDescription* description = nullptr)
	{
		(void)description;

		if (EventStorage* storage = TLS::storage)
		{
			if (storage->mode & Mode::INSTRUMENTATION_EVENTS)
			{
				if (!storage->startStopStack.empty())
				{
					EventData* eventData = storage->startStopStack.Pop();
#if ROCKY_PUSH_POP_PANIC_CHECK
					if (description != nullptr && description != eventData->description && !eventData->IsFinished())
					{
						FatalAssertf(strcmp(description->name, eventData->description->name) == 0, "Profiler macros PUSH/POP mismatch %s != %s", eventData->description->name, description->name);
					}
#endif
					Event::Stop(eventData);
				}
			}
		}
	}

	static ROCKY_INLINE void StackPopForce()
	{
		if (EventStorage* storage = TLS::storage)
		{
			if (!storage->startStopStack.empty())
			{
				EventData* desc = storage->startStopStack.Pop();
				EventBase<Mode::ALL>::Stop(desc);
			}
		}
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<rage::u32 TMode>
struct EventWithCallstack
{
	const EventDescription* desc;
	Timestamp startTime;

	ROCKY_INLINE EventWithCallstack( const EventDescription* description )
	{
		EventStorage* storage = TLS::storage;
		if (!storage || (storage->mode & TMode) != TMode)
		{
			desc = nullptr;
		}
		else
		{
			desc = description;
			startTime.Start();
		}
	}

	ROCKY_INLINE ~EventWithCallstack()
	{
		if (desc)
		{
			if (EventStorage* storage = TLS::storage)
			{
				Timestamp finishTime;
				finishTime.Start();

				if (finishTime.start - startTime.start > EventBase<TMode>::TimeThreshold)
				{
					EventData& data = storage->NextEvent();
					data.description = desc;
					data.start = startTime.start;
					data.finish = finishTime.start;

					AttachCallStack(TMode, data.finish);
				}
			}
		}	
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct GPUEvent
{
	static void AddEvent(const EventDescription* descrtiption, rage::u64 start, rage::u64 finish);
	static void AddIdle(const EventDescription* descrtiption, rage::u64 start, rage::u64 finish);
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if USE_PROFILER_FOR_MT_RESOURCES
ROCKY_INLINE void AttachResourceUsage(EventDescription* source, EventDescription* resource, rage::u8 accessMode, rage::u32 timestamp)
{
	EventStorage* storage = TLS::storage;
	if (resource && storage && (storage->mode & Mode::MT_RESOURCE) != 0)
	{
		ResourceData& data = storage->resourceUsageBuffer.Add();
		data.source = source;
		data.description = resource;
		data.time.start = timestamp;
		data.mode = accessMode;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ROCKY_INLINE void SignalResourceUsage(EventDescription* desc, rage::u8 accessMode)
{
	EventStorage* storage = TLS::storage;
	if (desc && storage && (storage->mode & Mode::MT_RESOURCE) != 0)
	{
		Timestamp timestamp;
		timestamp.Start();

		ResourceData& data = storage->resourceSignalBuffer.Add();
		data.description = desc;
		data.source = nullptr;
		data.time.start = timestamp.start;
		data.mode = accessMode;

		AttachCallStack(Mode::MT_RESOURCE, timestamp.start);
	}
}
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AttachMsg(const TagDescription* desc, const char* msg);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TVal>
ROCKY_INLINE void AttachTag(const TagDescription* desc, TVal val)
{
	// VS TODO: can we remove this check from here?
	if (desc)
	{
		if (EventStorage* storage = TLS::storage)
		{
			DataTag<TVal>& tag = storage->NextTag<DataTag<TVal>>();
			tag.description = desc;
			tag.data = val;
			tag.time.Start();
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TVal>
ROCKY_INLINE void AttachTag(const TagDescription* desc, TVal val, Profiler::Timestamp timestamp)
{
	// VS TODO: can we remove this check from here?
	if (desc)
	{
		if (EventStorage* storage = TLS::storage)
		{
			DataTag<TVal>& tag = storage->NextTag<DataTag<TVal>>();
			tag.description = desc;
			tag.data = val;
			tag.time = timestamp;
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<>
ROCKY_INLINE void AttachTag(const TagDescription* desc, const char* val)
{
	// VS TODO: can we remove this check from here?
	if (desc)
	{
		if (EventStorage* storage = TLS::storage)
		{
			DataTag<RockyShortString>& tag = storage->NextTag<DataTag<RockyShortString>>();
			tag.description = desc;
			tagcpy(tag, val ? val : "NULL");
			tag.time.Start();
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<>
ROCKY_INLINE void AttachTag(const TagDescription* desc, char* val)
{
	AttachTag(desc, (const char*)val);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ROCKY_INLINE void AttachTag(const TagDescription* desc)
{
	// VS TODO: can we remove this check from here?
	if (desc)
	{
		if (EventStorage* storage = TLS::storage)
		{
			DataTag<void>& tag = storage->NextTag<DataTag<void>>();
			tag.description = desc;
			tag.time.Start();
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ROCKY_INLINE void AttachLiveTag(const TagDescription* desc, const char* val)
{
	AttachTag(desc, val);
	if (EventStorage* storage = TLS::storage)
	{
		if (storage->mode & Mode::LIVE)
		{
			TagString& tag = storage->NextLiveTag();
			tag.description = desc;
			tagcpy(tag, val);
			tag.time.Start();
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ROCKY_INLINE bool IsCollectingTags()
{
	EventStorage* storage = TLS::storage;
	if (!storage)
		return false;

	return (storage->mode & Mode::TAGS) != 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ROCKY_INLINE bool IsCollectingEvents(Mode::Type mode = Mode::INSTRUMENTATION_EVENTS)
{
	EventStorage* storage = TLS::storage;
	if (!storage)
		return false;

	return (storage->mode & mode) != 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rage::u32 GetActiveMode();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef bool (*ScreenshotCallback)(const char*);
void RegisterScreenshotCallback(ScreenshotCallback cb);
void NotifyScreenshotReady(const char* name, bool isOk);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
EventStorage* GetGPUEventStorage();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class App
{
public:
	App();
	~App();

	// Starts a Rocky capture in the specified mode
	static void StartCapture(rage::u32 mode = Mode::DEFAULT);

	// Stops the active Rocky capture and saves it to the specified location.
	// If filename == NULL - generates the name automatically
	static void StopCapture(const char* filename = nullptr, bool force = false);

	// Stops the active Rocky capture and saves it to the specified location.
	static void SaveCaptureAfterHang(const char* filename);

	// Set callstack sampling frequency
	static void SetSamplingFrequency(int frequency);

	// Get callstack sampling frequency
	static int GetSamplingFrequency();

	// Set script callstack sampling frequency
	static void SetScriptSamplingFrequency(int frequency);

	// Get script callstack sampling frequency
	static int GetScriptSamplingFrequency();
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "system/interlocked.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace Profiler
{
	class ThreadToken
	{
	public:
		ThreadToken(const char* frameName, const int depth)
		{
			Profiler::ThreadDescription::Register(frameName, depth);
			m_ThreadId = rage::sysIpcGetCurrentThreadId();
		}

		~ThreadToken()
		{
			Profiler::ThreadDescription::Unregister(m_ThreadId);
		}

	private:
		rage::sysIpcCurrentThreadId m_ThreadId;
	};

	struct CommonDescriptions
	{
		static void Init();
		static const Profiler::EventDescription* s_operatorNewDescription;
		static const Profiler::EventDescription* s_operatorDeleteDescription;
	};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CHECK_CATEGORY_TYPE(CATEGORY) static_assert(sizeof(CATEGORY) == sizeof(Profiler::PackedCategory), "Use Profiler::CategoryColor::XXX instead of Profiler::Color::XXX or use MAKE_CATEGORY macro to combine a special one")
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif //ROCKY_LEVEL

//////////////////////////////////////////////////////////////////////////
// ===============     ROCKY_LEVEL_MAIN_FRAME   ========================//
//////////////////////////////////////////////////////////////////////////
#if USE_PROFILER_FRAME
// Macro for the MainThread																									 //
// Use it only for your main thread once!																					 //
#define PROFILER_FRAME(FRAME_NAME)	static bool ROCKY_CONCAT(autoregisterThread, __LINE__) = Profiler::ThreadDescription::Register(FRAME_NAME, 2); \
									(void)ROCKY_CONCAT(autoregisterThread, __LINE__);										\
									Profiler::NextFrame();	 \
									Profiler::CategoryEvent ROCKY_CONCAT(autogenerated_event_, __LINE__)(Profiler::GetMainFrameEventDescription(Profiler::FRAME_CPU));

// Profiler macro for the APP																								 //
// Turns on profiling in the constructor and turns it off in the destructor													 //
#define PROFILER_APP(APP_NAME)		Profiler::App	profilerApp;						\
									PROFILER_FRAME(APP_NAME);							\

// Macro for additional threads																								 //
// Use it to include your thread into rocky capture. Creates a token that registers the thread in its constructor,
//	and destroys it in the destructor (when losing scope). The use of a fixed name here is intentional,
//	as a thread should not be registered twice in the same scope.
#define PROFILER_THREAD(FRAME_NAME, DEPTH) Profiler::ThreadToken __profiler_token(FRAME_NAME, DEPTH); 

// Manual registration of a profiler thread. This thread will be tracked for the lifetime of the application,
//	unless Unregister() is called with its thread id. Useful for threads that are not managed directly by
//	our code and thus not applicable to the scoped tokens used in PROFILER_THREAD.
#define PROFILER_REGISTER_THREAD(FRAME_NAME, DEPTH) Profiler::ThreadDescription::Register(FRAME_NAME, DEPTH); 

#elif (!OVERRIDE_ROCKY)
#define PROFILER_FRAME(FRAME_NAME)
#define PROFILER_APP(APP_NAME)
#define PROFILER_THREAD(FRAME_NAME, DEPTH)
#define PROFILER_REGISTER_THREAD(FRAME_NAME, DEPTH)
#endif //USE_PROFILER_FRAME



//////////////////////////////////////////////////////////////////////////
// ===============     ROCKY_LEVEL_BASIC   =======================//
//////////////////////////////////////////////////////////////////////////
#if USE_PROFILER_BASIC
// Category is a special type of macro for high-level blocks of work (e.g. Animation, AI, Physics, Rendering)				  //
// This type of events has an attached color and are placed into the ThreadView												  //
// (Depth is usually limited to 1-2 levels, so it's pointless to have deep trees made from Categories)						  //
// Also use it as root marker for all the Dependency Jobs																	  //
// Example: PROFILER_CATEGORY("UpdateAI", Profiler::CategoryColor::AI)														  //
#define PROFILER_CATEGORY(NAME, CATEGORY)	CHECK_CATEGORY_TYPE(CATEGORY); \
											static Profiler::EventDescription* ROCKY_CONCAT(autogenerated_description_, __LINE__) = nullptr; \
											if (Unlikely(!ROCKY_CONCAT(autogenerated_description_, __LINE__))) ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::Create( NAME, __FILE__, __LINE__, CATEGORY ); \
											Profiler::CategoryEvent ROCKY_CONCAT(autogenerated_event_, __LINE__)( (ROCKY_CONCAT(autogenerated_description_, __LINE__)) );

#define PROFILER_CATEGORY_NORAZOR(NAME, CATEGORY)	CHECK_CATEGORY_TYPE(CATEGORY); \
													static Profiler::EventDescription* ROCKY_CONCAT(autogenerated_description_, __LINE__) = nullptr; \
													if (Unlikely(!ROCKY_CONCAT(autogenerated_description_, __LINE__))) ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::Create( NAME, __FILE__, __LINE__, CATEGORY ); \
													Profiler::CategoryEvent ROCKY_CONCAT(autogenerated_event_, __LINE__)( (ROCKY_CONCAT(autogenerated_description_, __LINE__)) ); 

// Scoped custom high-level category																						 //
#define PROFILER_CUSTOM_CATEGORY(DESCRIPTION) Profiler::CategoryEvent ROCKY_CONCAT(autogenerated_event_, __LINE__)(DESCRIPTION); //

// Creates a single EventDescription for 																					 //
#define PROFILER_DESCRIPTION(NAME, CATEGORY) Profiler::EventDescription::Create( NAME, __FILE__, __LINE__, CATEGORY )		 //

// GPU events																															 // GPU Events
#define PROFILER_GPU_EVENT(DESCRIPTION, START, FINISH)	Profiler::GPUEvent::AddEvent(DESCRIPTION, START, FINISH);
#define PROFILER_GPU_IDLE(DESCRIPTION, START, FINISH)	Profiler::GPUEvent::AddIdle(DESCRIPTION, START, FINISH);
#define PROFILER_GPU_FRAME(FRAME, TIMESTAMP)			Profiler::NextGpuFrame(FRAME, TIMESTAMP);
#define PROFILER_GPU_FLIP(GPU_FRAME_STRUCT)				Profiler::NextGpuFlip(GPU_FRAME_STRUCT);
#elif (!OVERRIDE_ROCKY)
#define PROFILER_CATEGORY(NAME, CATEGORY)
#define PROFILER_CATEGORY_NORAZOR(NAME, CATEGORY)
#define PROFILER_DESCRIPTION(NAME, CATEGORY)
#define PROFILER_CUSTOM_CATEGORY(DESCRIPTION)
#define PROFILER_GPU_EVENT(DESCRIPTION, START, FINISH)
#define PROFILER_GPU_IDLE(DESCRIPTION, START, FINISH)
#define PROFILER_GPU_FRAME(FRAME, TIMESTAMP)
#define PROFILER_GPU_FLIP(GPU_FRAME_STRUCT)
#endif //USE_PROFILER_BASIC



//////////////////////////////////////////////////////////////////////////
// ===============     ROCKY_LEVEL_NORMAL		========================//
//////////////////////////////////////////////////////////////////////////
#if USE_PROFILER_NORMAL
// Basic scoped event with user-defined name																				  //
// This macro is useful for overriding function name or to mark different scopes inside one function						  //
// Example: PROFILER_EVENT("FunctionName")																					  //
#define PROFILER_EVENT(NAME) static Profiler::EventDescription* ROCKY_CONCAT(autogenerated_description_, __LINE__) = nullptr;	\
							 if (Unlikely(!ROCKY_CONCAT(autogenerated_description_, __LINE__))) ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::Create( NAME, __FILE__, __LINE__ );  \
							 Profiler::Event ROCKY_CONCAT(autogenerated_event_, __LINE__)( (ROCKY_CONCAT(autogenerated_description_, __LINE__)) );

// Macro for wrapping a single-line call with Rocky counter
// Note:
//		CMD inside this macto IS NOT COMPILED OUT IN FINAL!
// Example:
//		PROFILER_INLINE_EVENT(m_settings.Init(initMode));
#define PROFILER_INLINE_EVENT(CMD)  PROFILER_CUSTOM_EVENT_PUSH(#CMD); CMD; PROFILER_CUSTOM_EVENT_POP(#CMD);


// Basic scoped event with the current function name																		  //
// Use it in 95% of your cases																								  //
// Automatically grabs current scope function's name																		  //
// PROFILE = PROFILER_EVENT(AUTOGENERATE_FUNCTION_NAME)																		  //
#define PROFILE				PROFILER_EVENT(ROCKY_FUNC)																		  //


// Budgeted performance scope-event																							 //
// The same as PROFILER_EVENT but with specified Budget																		 //
#define PROFILER_EVENT_BUDGETED(NAME, BUDGET)	static Profiler::EventDescription* ROCKY_CONCAT(autogenerated_description_, __LINE__) = nullptr;	\
												if (Unlikely(!ROCKY_CONCAT(autogenerated_description_, __LINE__))) ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::Create( NAME, __FILE__, __LINE__, Profiler::CategoryColor::None, 0, BUDGET );  \
												Profiler::CategoryEvent ROCKY_CONCAT(autogenerated_event_, __LINE__)( (ROCKY_CONCAT(autogenerated_description_, __LINE__)) );														

// Attach additional data as the context of the scope																		  //
// Supported types: float, u32, s32, u64, Vec3V, ShortString - char[32]														  //
// Example: PROFILER_TAG("Position", pPed->GetTransform().GetPosition());													  //
#define PROFILER_TAG(NAME, VALUE)	static Profiler::TagDescription* volatile ROCKY_CONCAT(autogenerated_tag_, __LINE__) = nullptr;	\
									if (Unlikely(!ROCKY_CONCAT(autogenerated_tag_, __LINE__))) ROCKY_CONCAT(autogenerated_tag_, __LINE__) = Profiler::TagDescription::Create( NAME ); \
									if (Profiler::IsCollectingTags()) Profiler::AttachTag(ROCKY_CONCAT(autogenerated_tag_, __LINE__), VALUE); 

// Scoped event for any synchronization/wait block of the code																 //
#define PROFILER_WAIT(NAME) static Profiler::EventDescription* volatile ROCKY_CONCAT(autogenerated_description_, __LINE__) = nullptr; \
							if (Unlikely(!ROCKY_CONCAT(autogenerated_description_, __LINE__))) ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::Create( NAME, __FILE__, __LINE__, Profiler::CategoryColor::Empty ); \
							Profiler::EventWithCallstack<Profiler::Mode::SWITCH_CONTEXT | Profiler::Mode::INSTRUMENTATION_EVENTS> ROCKY_CONCAT(autogenerated_event_, __LINE__)( (ROCKY_CONCAT(autogenerated_description_, __LINE__)) ); 

//
#define PROFILER_CUSTOM_WAIT(DESCRIPTION) Profiler::EventWithCallstack<Profiler::Mode::SWITCH_CONTEXT | Profiler::Mode::INSTRUMENTATION_EVENTS> ROCKY_CONCAT(autogenerated_event_, __LINE__)(DESCRIPTION);

// Attach a callstack for each call of the function
// Usefull for tracking down the usage of the function
#define PROFILER_EVENT_WITH_CALLSTACK(NAME)  static Profiler::EventDescription* volatile ROCKY_CONCAT(autogenerated_description_, __LINE__) = nullptr;	\
											 if (Unlikely(!ROCKY_CONCAT(autogenerated_description_, __LINE__))) ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::Create( NAME, __FILE__, __LINE__ );  \
											 Profiler::EventWithCallstack<Profiler::Mode::INSTRUMENTATION_EVENTS> ROCKY_CONCAT(autogenerated_event_, __LINE__)( (ROCKY_CONCAT(autogenerated_description_, __LINE__)) );

// Create a single EventDescription wrapped inside a lambda - can be used as an inlined function parameter
// e.g. CallSomeFunction([]{ DoStuff(); }, PROFILER_LAMBDA_DESCRIPTION("DoStuff"));
#define PROFILER_LAMBDA_DESCRIPTION(NAME)	[]()->Profiler::EventDescription* {																			\
												static Profiler::EventDescription* desc = nullptr;														\
												return (desc != nullptr) ? desc : desc = Profiler::EventDescription::Create(NAME, __FILE__, __LINE__);	\
											}()


// Scoped event with pre-created Profiler::EventDescription pointer															 //
// Useful for custom generated/pre-created profiling descriptions															 //
// Usually used for custom sub-system support																				 //
#define PROFILER_CUSTOM_EVENT(DESCRIPTION) Profiler::Event ROCKY_CONCAT(autogenerated_event_, __LINE__)(DESCRIPTION);		 //

// Scoped event with shared string name																						 //
#define PROFILER_STRING_EVENT(NAME) Profiler::EventDescription* ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::CreateShared( NAME );  \
									Profiler::Event ROCKY_CONCAT(autogenerated_event_, __LINE__)( (ROCKY_CONCAT(autogenerated_description_, __LINE__)) );

// Support for old PUSH/POP paradigm																						 //
#define PROFILER_CUSTOM_EVENT_PUSH(NAME)	static Profiler::EventDescription* volatile ROCKY_CONCAT(autogenerated_description_, __LINE__) = nullptr;	\
											if (Unlikely(!ROCKY_CONCAT(autogenerated_description_, __LINE__))) ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::Create( NAME, __FILE__, __LINE__ );  \
											Profiler::StackedEvent::StackPush((ROCKY_CONCAT(autogenerated_description_, __LINE__)));	
																															 //
// Support for old PUSH/POP paradigm with Budget																			 //
#define PROFILER_CUSTOM_EVENT_PUSH_BUDGETED(NAME, BUDGET)	static Profiler::EventDescription* volatile ROCKY_CONCAT(autogenerated_description_, __LINE__) = nullptr;	\
															if (Unlikely(!ROCKY_CONCAT(autogenerated_description_, __LINE__))) ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::Create( NAME, __FILE__, __LINE__, Profiler::CategoryColor::None, 0, BUDGET );  \
															Profiler::StackedEvent::StackPush((ROCKY_CONCAT(autogenerated_description_, __LINE__)));	
																															 //
// Support for old PUSH/POP paradigm																						 //
#if ROCKY_PUSH_POP_PANIC_CHECK																								 //
#define PROFILER_CUSTOM_EVENT_POP(NAME)			static Profiler::EventDescription* volatile ROCKY_CONCAT(autogenerated_description_, __LINE__) = nullptr;	\
												if (Unlikely(!ROCKY_CONCAT(autogenerated_description_, __LINE__))) ROCKY_CONCAT(autogenerated_description_, __LINE__) = Profiler::EventDescription::CreateShared( NAME );  \
												Profiler::StackedEvent::StackPop(ROCKY_CONCAT(autogenerated_description_, __LINE__));										 //
#else																														 //
#define PROFILER_CUSTOM_EVENT_POP(NAME)			Profiler::StackedEvent::StackPop();											 //
#endif //ROCKY_PUSH_POP_PANIC_CHECK																													 //


// Tag with predefined																										 //
#define PROFILER_CUSTOM_TAG(DESCRIPTION) if (Profiler::IsCollectingTags()) Profiler::AttachTag(DESCRIPTION);													 //

// Tag with CONST tag name (creates a tag once with the specified name)
#define PROFILER_CONST_STRING_TAG(NAME) if (Profiler::IsCollectingTags()) { \
											static const Profiler::TagDescription* ROCKY_CONCAT(autogenerated_event_, __LINE__) = Profiler::TagDescription::CreateShared( NAME ); \
											Profiler::AttachTag(ROCKY_CONCAT(autogenerated_event_, __LINE__)); \
										} 

// Adds a Live tag visible on the main live chart (e.g. Mission Start, Cutscene start, Black screen etc.)
#define PROFILER_LIVE_TAG(DESCRIPTION, STRING_VALUE)	if (Profiler::IsCollectingEvents((Profiler::Mode::Type)(Profiler::Mode::LIVE | Profiler::Mode::TAGS))) Profiler::AttachLiveTag(DESCRIPTION, STRING_VALUE);

// Memory Allocation events																									 //
#define PROFILER_MEMORY_ALLOC_EVENT(ALLOC_SIZE)		Profiler::EventWithCallstack<Profiler::Mode::MEMORY> ROCKY_CONCAT(autogenerated_event_, __LINE__)( Profiler::CommonDescriptions::s_operatorNewDescription ); 
#define PROFILER_MEMORY_FREE_EVENT					Profiler::EventWithCallstack<Profiler::Mode::MEMORY> ROCKY_CONCAT(autogenerated_event_, __LINE__)( Profiler::CommonDescriptions::s_operatorDeleteDescription ); 

#elif (!OVERRIDE_ROCKY)
#define PROFILER_EVENT(NAME)
#define PROFILER_INLINE_EVENT(CMD) CMD;
#define PROFILE
#define PROFILER_TAG(NAME, VALUE)
#define PROFILER_EVENT_BUDGETED(NAME, BUDGET)
#define PROFILER_WAIT(NAME)
#define PROFILER_CUSTOM_WAIT(DESCRIPTION)
#define PROFILER_EVENT_WITH_CALLSTACK(NAME)
#define PROFILER_LAMBDA_DESCRIPTION(NAME)
#define PROFILER_CUSTOM_EVENT(DESCRIPTION)
#define PROFILER_STRING_EVENT(NAME)
#define PROFILER_CUSTOM_EVENT_PUSH(NAME)
#define PROFILER_CUSTOM_EVENT_PUSH_BUDGETED(NAME, BUDGET)
#define PROFILER_CUSTOM_EVENT_POP(NAME)
#define PROFILER_CUSTOM_TAG(DESCRIPTION)
#define PROFILER_CONST_STRING_TAG(NAME)
#define PROFILER_LIVE_TAG(DESCRIPTION, STRING_VALUE)
#define PROFILER_MEMORY_ALLOC_EVENT(ALLOC_SIZE)
#define PROFILER_MEMORY_FREE_EVENT
#endif //USE_PROFILER_NORMAL
																															  //

#if USE_PROFILER_FOR_MT_RESOURCES
#define PROFILER_SIGNAL_RESOURCE(DESCRIPTION, MODE) Profiler::SignalResourceUsage(DESCRIPTION, MODE);
#else
#define PROFILER_SIGNAL_RESOURCE(DESCRIPTION, MODE)
#endif

#endif //PROFILER_H