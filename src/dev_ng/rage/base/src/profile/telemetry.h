#ifndef PROFILE_TELEMETRY_H
#define PROFILE_TELEMETRY_H

#define USE_TELEMETRY	(!RSG_PS3 && !__FINAL && !__RESOURCECOMPILER)

#include "system/tls.h"

enum ProfileZoneType
{
	PZONE_NORMAL,
	PZONE_STALL,
	PZONE_IDLE
};

enum MessageType
{
	MSGT_LOG,
	MSGT_WARNING,
	MSGT_ERROR,
};

enum LockResultType
{
	LRT_SUCCESS = 0,
	LRT_FAILED	= 1,
	LRT_TIMEOUT = 2
};

#if USE_TELEMETRY

#define TELEMETRY_ONLY(x) x

#define TELEMETRY_SET_THREAD_NAME(threadId, nameFormat, ...)                     \
	do {                                                                         \
		const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.SetThreadName) {                                                    \
	        c.SetThreadName(&threadId, nameFormat, ##__VA_ARGS__);                \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_SET_THREAD_NAME

#define TELEMETRY_ALLOC(ptr, size, file, line, nameFormat, ...)                  \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.Alloc) {                                                            \
	        c.Alloc(ptr, size, file, line, nameFormat, ##__VA_ARGS__);           \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_ALLOC

#define TELEMETRY_FREE(ptr, file, line)                                          \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.Free) {                                                             \
	        c.Free(ptr, file, line);                                             \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_FREE

#define TELEMETRY_START_ZONE(zoneType, file, line, nameFormat, ...)              \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.StartZone) {                                                        \
		c.StartZone(zoneType, file, line, nameFormat ##__VA_ARGS__);             \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_START_ZONE

#define TELEMETRY_END_ZONE(file, line)                                           \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.EndZone) {                                                          \
		c.EndZone(file, line);													 \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_END_ZONE

#define TELEMETRY_TRY_LOCK(handle, file, line, nameFormat, ...)					 \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.TryLock) {															 \
		c.TryLock(handle, file, line, nameFormat ##__VA_ARGS__);                 \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_TRY_LOCK

#define TELEMETRY_END_TRY_LOCK(eResult,handle, file, line)						 \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.EndTryLock) {														 \
		c.EndTryLock(eResult,handle, file, line);								 \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_TRY_LOCK

#define TELEMETRY_LOCK(handle, file, line, nameFormat, ...)						 \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.Lock) {															 \
		c.Lock(handle, file, line, nameFormat ##__VA_ARGS__);                    \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_LOCK

#define TELEMETRY_UNLOCK(handle, file, line, nameFormat, ...)					 \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.Unlock) {															 \
		c.Unlock(handle, file, line, nameFormat ##__VA_ARGS__);                  \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_UNLOCK

#define TELEMETRY_PLOT_MEMORY(bytes, nameFormat, ...)                            \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.PlotMemory) {                                                       \
		    c.PlotMemory(bytes, nameFormat, ##__VA_ARGS__);                      \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_PLOT_MEMORY

#define TELEMETRY_PLOT_MS(milliseconds, nameFormat, ...)                         \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.PlotMilliseconds) {                                                 \
	        c.PlotMilliseconds(milliseconds, nameFormat, ##__VA_ARGS__);         \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_PLOT_MS

#define TELEMETRY_PLOT_FLOAT(value, nameFormat, ...)                             \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.PlotFloat) {                                                        \
	        c.PlotFloat(value, nameFormat, ##__VA_ARGS__);                       \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_PLOT_FLOAT

#define TELEMETRY_ADD_BLOB(blobBuffer, pluginName, nameFormat, ...)              \
	do {                                                                         \
	    const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
	    if(c.AddBlob) {                                                          \
	        c.AddBlob(blobBuffer, pluginName, nameFormat, ##__VA_ARGS__);        \
	    }                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_ADD_BLOB

#define TELEMETRY_MESSAGE_LOG(nameFormat, ...)                                   \
	do {                                                                         \
		const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
		if(c.Message) {                                                          \
			c.Message(MSGT_LOG, nameFormat, ##__VA_ARGS__);                      \
		}                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_MESSAGE_LOG

#define TELEMETRY_MESSAGE_WARNING(nameFormat, ...)                               \
	do {                                                                         \
		const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
		if(c.Message) {                                                          \
			c.Message(MSGT_WARNING, nameFormat, ##__VA_ARGS__);                  \
		}                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_MESSAGE_LOG

#define TELEMETRY_MESSAGE_ERROR(nameFormat, ...)                                 \
	do {                                                                         \
		const ::rage::TelemetryCallbacks & c = ::rage::GetTelemetryCallbacks();  \
		if(c.Message) {                                                          \
			c.Message(MSGT_ERROR, nameFormat, ##__VA_ARGS__);                    \
		}                                                                        \
	} while(0)                                                                   \
	// TELEMETRY_MESSAGE_LOG


namespace rage
{
////////////////////////////////////////////////////////////
class TelemetryCallbacks;
class  CBlobBuffer;

////////////////////////////////////////////////////////////
const TelemetryCallbacks & GetTelemetryCallbacks();
void SetTelemetryCallbacks(const TelemetryCallbacks & callbacks);

////////////////////////////////////////////////////////////
class TelemetryCallbacks
{
public:
	void (*SetThreadName)(void* pThreadId, const char * nameFormat, ...);

	void (*Alloc)(const void * ptr, u32 size, const char * file, unsigned int line, const char * nameFormat, ...);
	void (*Free)(const void * ptr, const char * file, unsigned int line);

	void (*StartZone)(ProfileZoneType zoneType, const char * file, unsigned int line, const char * nameFormat, ...);
	void (*EndZone)(const char * file, unsigned int line);

	void (*TryLock)(const void* ptr, const char * file, unsigned int line, const char * nameFormat, ...);
	void (*EndTryLock)(LockResultType eResult, const void* ptr, const char * file, unsigned int line);

	void (*Lock)(const void* ptr, const char * file, unsigned int line, const char * nameFormat, ...);
	void (*Unlock)(const void* ptr, const char * file, unsigned int line, const char * nameFormat, ...);

	void (*PlotMemory)(u32 value, const char * nameFormat, ...);
	void (*PlotMilliseconds)(float value, const char * nameFormat, ...);
	void (*PlotFloat)(float value, const char * nameFormat, ...);

	void (*AddBlob)(const CBlobBuffer & blob, const char * pluginName, const char * nameFormat, ...);

	void (*Message)(u32 flags, const char * nameFormat, ...);
};

////////////////////////////////////////////////////////////
class CBlobBuffer
{
public:
	CBlobBuffer(const char * blobType)
	: m_BufferPosition(0)
	{
		// endianness check
		WriteU32(0x01234567);

		// blob type can be used to control versioning
		// and support multiple blob types in a single plugin
		// eg. "gputimings:0", "gputimings:1", "something:6"
		WriteString(blobType);
	}

	inline const void * GetData() const
	{
		return m_Buffer;
	}

	inline int GetDataSizeInBytes() const
	{
		return m_BufferPosition;
	}

	inline int WriteU32(u32 value)
	{
		return Write<u32>(value);
	}
	inline int WriteFloat(float value)
	{
		return Write<float>(value);
	}
	inline int WriteString(const char * value)
	{
		unsigned int startingBufferPosition = m_BufferPosition;
		while(*value != '\0' && m_BufferPosition != BUFFER_SIZE)
		{
			m_Buffer[m_BufferPosition] = *value;
			++m_BufferPosition;
			++value;
		}
		if(m_BufferPosition == BUFFER_SIZE)
		{
			m_BufferPosition = startingBufferPosition;
		}
		else
		{
			m_Buffer[m_BufferPosition] = '\0';
			++m_BufferPosition;
		}
		return m_BufferPosition - startingBufferPosition;
	}
private:
	template<typename T> inline int Write(T value)
	{
		if(BUFFER_SIZE - m_BufferPosition < sizeof(T))
		{
			return 0;
		}

		union
		{
			T value;
			u8  bytes[sizeof(T)];
		} valueBytes;

		valueBytes.value = value;
		for(unsigned int i = 0; i < sizeof(T); ++i)
		{
			m_Buffer[m_BufferPosition++] = valueBytes.bytes[i];
		}

		return sizeof(T);
	}
private:
	static const unsigned int BUFFER_SIZE = 16 * 1024;
private:
	unsigned int m_BufferPosition;
	u8 m_Buffer[BUFFER_SIZE];
};

////////////////////////////////////////////////////////////
extern __THREAD const char * g_TrackingFile;
extern __THREAD unsigned int g_TrackingLine;

#define TRACKING__FILE__ TrackingFile(__FILE__)
#define TRACKING__LINE__ TrackingLine(__LINE__)

inline const char* TrackingFile(const char * file)
{
	return g_TrackingFile != NULL ? g_TrackingFile : file;
}

inline unsigned int TrackingLine(unsigned int line)
{
	return g_TrackingLine != 0xFFFFFFFF ? g_TrackingLine : line;
}

////////////////////////////////////////////////////////////
class CTelemetryTracking
{
public:
	CTelemetryTracking(const char * trackingFile, const unsigned int trackingLine)
		: m_PrevTrackingFile(g_TrackingFile)
		, m_PrevTrackingLine(g_TrackingLine)
		{
			g_TrackingFile = trackingFile;
			g_TrackingLine = trackingLine;
		}
	~CTelemetryTracking()
		{
			g_TrackingFile = m_PrevTrackingFile;
			g_TrackingLine = m_PrevTrackingLine;
		}
private:
	const char * const m_PrevTrackingFile;
	const unsigned int m_PrevTrackingLine;
};

}

#else

#define TELEMETRY_ONLY(x)

#define TELEMETRY_SET_THREAD_NAME(...)
#define TELEMETRY_ALLOC(...)
#define TELEMETRY_FREE(...)
#define TELEMETRY_START_ZONE(...)
#define TELEMETRY_END_ZONE(...)
#define TELEMETRY_TRY_LOCK(...)
#define TELEMETRY_END_TRY_LOCK(...)
#define TELEMETRY_LOCK(...)
#define TELEMETRY_UNLOCK(...)
#define TELEMETRY_PLOT_MEMORY(...)
#define TELEMETRY_PLOT_MS(...)
#define TELEMETRY_PLOT_FLOAT(...)
#define TELEMETRY_MESSAGE_LOG(...)
#define TELEMETRY_MESSAGE_WARNING(...)
#define TELEMETRY_MESSAGE_ERROR(...)

#endif

#endif