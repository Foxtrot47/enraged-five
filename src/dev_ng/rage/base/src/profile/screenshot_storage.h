#ifndef SCREENSHOT_STORAGE_H
#define SCREENSHOT_STORAGE_H

#include "settings.h"

#if USE_PROFILER

#include "atl/array.h"
#include "data/growbuffer.h"
#include "file/limits.h"
#include "system/criticalsection.h"

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class ScreenshotStorage
{
public:
	struct Slot
	{
		rage::datGrowBuffer buffer;
		char name[rage::RAGE_MAX_PATH];
		bool isReady;
		Slot();
	};

	// PURPOSE: Queries a new screenshot
	// RETURNS: A new slot with the memory file name and allocated space for the screenshot
	const Slot* QueryScreenshot();

	// PURPOSE: Checks whether all the requests are completed
	bool IsReady() const;

	// PURPOSE: Cleans all the temporary memory allocated for the screenshots
	void Reset(bool force = false);

	// PURPOSE: Notification for screenshot request completion
	bool MakeReady(const char* name);

	// PURPOSE: Destructor
	~ScreenshotStorage();

private:
	mutable rage::sysCriticalSectionToken cs;
	rage::atArray<Slot*, 16> slots;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER
#endif //PROFILER_CORE_H
