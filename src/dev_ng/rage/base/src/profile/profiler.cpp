//
// profile/profiler.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

//==============================================================
// external defines

#include "diag/stats.h"

#include "profiler.h"

#if __STATS
#include "group.h"
#include "log.h"
#include "pfpacket.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/button.h"
#include "data/callback.h"
#include "file/tcpip.h"
#include "grcore/setup.h"

#if !__RESOURCECOMPILER
	#include "input/pad.h"
	#include "input/keyboard.h"
	#include "input/keys.h"
	#include "input/mapper.h"
	#include "input/mouse.h"
#endif	//	!__RESOURCECOMPILER

#include "parser/manager.h"
#include "profile/element.h"
#include "string/stringutil.h"
#include "system/param.h"
#include "vector/vector3.h"


using namespace rage;

//==============================================================
// pfProfiler

ASSERT_ONLY(int pfProfiler::ms_GroupOverflowCount = 0);

pfProfiler::pfProfiler ()
	: m_Enabled(true)
	, m_Initialized(false)
	, m_NumGroups(0)
{
#if __BANK
	m_Bank = NULL;
#endif
}


pfProfiler::~pfProfiler ()
{
	Assert(m_PageTree.IsEmpty());
}


void pfProfiler::RegisterPage (pfPageNode & pageNode)
{
	Assert(!pageNode.IsAttached());
	AssertVerify(m_PageTree.AddNode(pageNode));
}


void pfProfiler::UnregisterPage (pfPageNode & pageNode)
{
	m_PageTree.RemoveNode(pageNode);
}


void pfProfiler::RegisterGroup (pfGroup & group)
{
	if (m_NumGroups<kMaxGroups)
	{
		m_Groups[m_NumGroups] = &group;
		m_NumGroups++;
	}
	ASSERT_ONLY(else { ms_GroupOverflowCount ++; })
}

PARAM(noprofile,"[profile] Disable profiler module");
PARAM(tunerannotation,"[grcore] Enable Tuner annotation");
PARAM(customlog,"[profile] Load a custom set of timers and values to log from the specified file");
PARAM(logfile, "[profile] Sets the name of the logfile (defaults to profiler_log.csv)");
PARAM(profilerrag,"[profile] Enable Rag Profiler Plugin");
bool g_EnableTunerAnnotation = false;

void pfProfiler::Init (bool BANK_ONLY(createWidgets))
{
	Assertf(ms_GroupOverflowCount == 0, "pfProfiler::Init()...Too many profiler groups, increase kMaxGroups to %d.", kMaxGroups + ms_GroupOverflowCount);

    g_EnableTunerAnnotation = PARAM_tunerannotation.Get();

	if (!m_Initialized)
	{
		if (PARAM_noprofile.Get())
		{
			m_Enabled = false;
		}
		else if ( PARAM_profilerrag.Get() )
		{
#if __BANK
			pfPacket::Connect( true );
#endif
		}

		const char* logFileName = "c:\\profiler_log.csv";
		PARAM_logfile.Get(logFileName);
		pfLogMgr::SetFile(logFileName);

		for (int i=0; i<m_NumGroups; i++)
		{
			m_Groups[i]->Init();
		}

		const char* customFileName = NULL;
		if (PARAM_customlog.Get(customFileName) && customFileName != NULL && customFileName[0] != '\0')
		{
			CustomElements elts = LoadCustomElementList(customFileName);
			pfLogMgr::SetCustomTimers(elts.m_Timers);
			pfLogMgr::SetCustomValues(elts.m_Values);
			pfLogMgr::SetUseCustomElements(true);
		}

#if __BANK
		if(createWidgets)
		{
			m_Bank = &BANKMGR.CreateBank("rage - Profile Stats");
			AddCreateWidgetButton(*m_Bank);
		}

		// create initial stats for Rag Profiler
		if ( pfPacket::IsConnected() )
		{
			const pfPageNode * curNode = m_PageTree.FindMinimumNode();
			while ( curNode )
			{
				curNode->GetData()->AddToRagProfiler();
				curNode = m_PageTree.FindSuccessorNode( *curNode );
			}
		}
#endif
		m_Initialized = true;
	}
	else
	{
		Warningf("pfProfiler:Init while already initialized");
	}
}


void pfProfiler::Shutdown ()
{
	if (m_Initialized)
	{
		pfLogMgr::Shutdown();
#if __BANK
		if ( pfPacket::IsConnected() )
		{
			pfPacket::Close();
		}
#endif
	}
	else
	{
		Warningf("pfProfiler::Shutdown - but not initialized");
	}

#if __BANK
	if(m_Bank)
	{
		BANKMGR.DestroyBank(*m_Bank);
		m_Bank = NULL;
	}
#endif

	m_Initialized = false;
}

pfTimer* pfProfiler::FindTimer(const char* pageName, const char* groupName, const char* timerName)
{
	pfPage** page = m_PageTree.Access(pageName);
	if (page)
	{
		for(int i = 0; i < (*page)->GetNumGroups(); i++)
		{
			if (!stricmp(groupName, (*page)->GetGroup(i)->GetName()))
			{
				pfGroup* group = (*page)->GetGroup(i);
				pfTimer* timer = group->GetNextTimer(NULL);
				while(timer)
				{
					if (!stricmp(timerName, timer->GetName()))
					{
						return timer;
					}
					timer = group->GetNextTimer(timer);
				}
				return NULL;
			}
		}
	}
	return NULL;
}


pfValue* pfProfiler::FindValue(const char* pageName, const char* groupName, const char* valueName)
{
	pfPage** page = m_PageTree.Access(pageName);
	if (page)
	{
		for(int i = 0; i < (*page)->GetNumGroups(); i++)
		{
			if (!stricmp(groupName, (*page)->GetGroup(i)->GetName()))
			{
				pfGroup* group = (*page)->GetGroup(i);
				pfValue* value = group->GetNextValue(NULL);
				while(value)
				{
					int ignored = 0;
					if (!stricmp(valueName, value->GetName(ignored)))
					{
						return value;
					}
					value = group->GetNextValue(value);
				}
				return NULL;
			}
		}
	}
	return NULL;
}

pfProfiler::CustomElements pfProfiler::LoadCustomElementList(const char* filename)
{
	parTree* tree = PARSER.LoadTree(filename, "elts");

	CustomElements elts;
	elts.m_Timers = NULL;
	elts.m_Values = NULL;

	if (!tree) 
	{
		return elts;
	}

	parTreeNode* root = tree->GetRoot();
	for(parTreeNode::ChildNodeIterator kid = root->BeginChildren(); kid != root->EndChildren(); ++kid)
	{
		const char* page = NULL;
		const char* group = NULL;
		const char* timer = NULL;
		const char* value = NULL;
		(*kid)->FindValueFromPath("Page", page);
		(*kid)->FindValueFromPath("Group", group);
		(*kid)->FindValueFromPath("Timer", timer);
		(*kid)->FindValueFromPath("Value", value);
		if (page && group)
		{
			if (timer)
			{
				pfTimer* t = FindTimer(page, group, timer);
				if (t)
				{
					if (!elts.m_Timers)
					{
						elts.m_Timers = rage_new atArray<pfTimer*>;
					}
					elts.m_Timers->PushAndGrow(t);
				}
			}
			else if (value)
			{
				pfValue* val = FindValue(page, group, value);
				if (val)
				{
					if (!elts.m_Values)
					{
						elts.m_Values = rage_new atArray<pfValue*>;
					}
					elts.m_Values->PushAndGrow(val);
				}
			}
		}
	}

	delete tree;

	return elts;
}



#if __BANK


void pfProfiler::AddCreateWidgetButton(bkBank& bank)
{
	bank.AddButton("Create Profile Stats Widgets", datCallback( MFA1(pfProfiler::AddWidgets), &GetRageProfiler(), &bank));
}

void pfProfiler::AddWidgets (bkBank& bank)
{
	// destroy first widget which is the create button
	bkWidget* pWidget = BANKMGR.FindWidget("rage - Profile Stats/Create Profile Stats Widgets");
	if(pWidget == NULL)
		return;
	pWidget->Destroy();

	bank.AddButton( "Print Network Stats", datCallback( CFA(fiDeviceTcpIp::PrintNetStats) ) );

	bank.AddToggle("Profiler - enabled",&m_Enabled);

	if (m_OnCreateWidgets.IsBound())
	{
		m_OnCreateWidgets(bank);
	}
		
	pfLogMgr::AddWidgets(bank);

	// add pages to the bank
	const pfPageNode * curNode = m_PageTree.FindMinimumNode();
	while (curNode)
	{
		curNode->GetData()->AddWidgets(bank);
		curNode = m_PageTree.FindSuccessorNode(*curNode);
	}

#if __XENON
	bank.PushGroup("Save Trace for Timer", false);
	// add pages to the bank
	curNode = m_PageTree.FindMinimumNode();
	while (curNode)
	{
		curNode->GetData()->AddTraceWidgets(bank);
		curNode = m_PageTree.FindSuccessorNode(*curNode);
	}
	bank.PopGroup();
#endif
}
#endif



pfPage * pfProfiler::GetNextPage (const pfPage * curPage, bool onlyActivePages)
{
	const pfPageNode * nextNode;

	do
	{
		if (curPage==NULL)
		{
			nextNode = m_PageTree.FindMinimumNode();
		}
		else
		{
			const pfPageNode * curNode = m_PageTree.FindNode(curPage->GetName());
			Assert(curNode);
			nextNode = m_PageTree.FindSuccessorNode(*curNode);
		}
		
		if(nextNode)
		{
			//see if this page is active, and if we care about that state...
			if(!onlyActivePages || (nextNode->GetData() && nextNode->GetData()->GetIsActive()))
			{
				break;
			}
			else
			{	// Without this, the function could enter an infinite loop if
				// an inactive page was encountered. /FF
				curPage = nextNode->GetData();
			}
		}
	} while(nextNode);

	return (nextNode ? nextNode->GetData() : NULL);
}


pfPage * pfProfiler::GetPrevPage (const pfPage * curPage, bool onlyActivePages)
{
	const pfPageNode * prevNode;

	do
	{
		if (curPage==NULL)
		{
			prevNode = m_PageTree.FindMaximumNode();
		}
		else
		{
			const pfPageNode * curNode = m_PageTree.FindNode(curPage->GetName());
			Assert(curNode);
			prevNode = m_PageTree.FindPredecessorNode(*curNode);
		}

		if(prevNode)
		{
			//see if this page is active, and if we care about that state...
			if(!onlyActivePages || (prevNode->GetData() && prevNode->GetData()->GetIsActive()))
			{
				break;
			}
			else
			{	// Without this, the function could enter an infinite loop if
				// an inactive page was encountered. /FF
				curPage = prevNode->GetData();
			}
		}
	}while(prevNode);

	return (prevNode ? prevNode->GetData() : NULL);
}


pfPage * pfProfiler::GetPageByName (const char * pageName)
{
	const pfPageNode * node = m_PageTree.FindNode(pageName);
	if (node)
	{
		return node->GetData();
	}
	else
	{
		return NULL;
	}
}


void pfProfiler::Reset ()
{
	for (int i=0; i<m_NumGroups; i++)
	{
		m_Groups[i]->Reset();
	}
}


void pfProfiler::FlipElapsedTimes()
{
	for(int i=0; i<m_NumGroups; i++)
	{
		m_Groups[i]->FlipElapsedTime();
	}
}



#endif // __STATS


//==============================================================
// GetAGEProfiler() - See declaration.

rage::pfProfiler & rage::GetRageProfiler()
{
	static pfProfiler sProfiler;
	return sProfiler;
}

