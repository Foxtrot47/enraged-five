#ifndef RUN_DESCRIPTION_BOARD_H
#define RUN_DESCRIPTION_BOARD_H

#if USE_PROFILER

#include "common.h"
#include "serialization.h"
#include "atl/string.h"
#include "atl/vector.h"

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class RunDescriptionBoard
{
	RunDescriptionBoard();
public:
	struct Entry
	{
		rage::atString name;
		rage::atString value;
	};

	RunDescriptionBoard& Update();
	void Add(const char* name, const char* value);

	rage::atVector<Entry> entries;
	static RunDescriptionBoard& Get();
};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator << (OutputDataStream& stream, const RunDescriptionBoard& ob);
OutputDataStream& operator << (OutputDataStream& stream, const RunDescriptionBoard::Entry& ob);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER

#endif //RUN_DESCRIPTION_BOARD_H