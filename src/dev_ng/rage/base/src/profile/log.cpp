//
// profile/log.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

/////////////////////////////////////////////////////////////////
// external defines

#include "diag/stats.h"

#if __STATS

#include "bank/bank.h"
#include "file/asset.h"
#include "file/stream.h"
#include "string/string.h"
#include "math/amath.h"
#include "profile/profiler.h"
#include "profile/log.h"
#include "bank/combo.h"

using namespace rage;

/////////////////////////////////////////////////////////////////
// pfLogMgr

// static variables
bool pfLogMgr::Enabled = false;
bool pfLogMgr::OutputActive = false;
pfPage * pfLogMgr::CurrentPage = NULL;
char pfLogMgr::CurrentPageName[pfPage::kNameMaxLen] = "*no page*";
bool pfLogMgr::UseCustomLog = false;
atArray<pfTimer*>* pfLogMgr::CustomTimers = NULL;
atArray<pfValue*>* pfLogMgr::CustomValues = NULL;
fiStream * pfLogMgr::CurrentStream = NULL;
bool pfLogMgr::FileWaitingToOpen = false;
char pfLogMgr::WaitingFileName[MaxFilename] = "";
char pfLogMgr::CurrentFileNameDisplay[MaxFilenameDisplayed] = "*no file*";
bool pfLogMgr::ShouldWriteHeader = false;
#if __BANK
int pfLogMgr::sm_ComboPageSelection = 0;
bkCombo * pfLogMgr::sm_SelectionPageCombo = NULL;
#endif	// __BANK

void pfLogMgr::Enable(bool on)
{
	if (!Enabled && on)
	{
		ShouldWriteHeader = true;
	}
	Enabled=on;
}

// member functions
pfPage *pfLogMgr::NextPage ()
{
	SetPage(GetRageProfiler().GetNextPage(CurrentPage));
	return CurrentPage;
}


pfPage *pfLogMgr::PrevPage ()
{
	SetPage(GetRageProfiler().GetPrevPage(CurrentPage));
	return CurrentPage;
}


void pfLogMgr::SetPage (pfPage *page)
{
	ShouldWriteHeader = true;

	CurrentPage = page;

	if (CurrentPage)
	{
		Displayf("pfLogMgr - page to log '%s'",CurrentPage->GetName());
		strcpy(CurrentPageName,CurrentPage->GetName());
	}
	else
	{
		Displayf("pfLogMgr - no page to log");
		strcpy(CurrentPageName,"*no page*");
	}

	// Sometimes redundant, it sets the current selection whether from combo box or hot keys.
#if __BANK
	if ( sm_SelectionPageCombo != NULL )
	{
		sm_ComboPageSelection = sm_SelectionPageCombo->GetStringIndex(CurrentPageName);
	}
#endif	// __BANK
}

void pfLogMgr::SetPageByName (const char *pageName)
{
	SetPage(GetRageProfiler().GetPageByName(pageName));
}

void pfLogMgr::Init ()
{
}


void pfLogMgr::Shutdown ()
{
	CloseFile();

	Enabled = false;
	OutputActive = false;
	CurrentPage = NULL;
	CurrentStream = NULL;
	ShouldWriteHeader = false;
}


#if __BANK
void pfLogMgr::LogCB()
{
	if(Enabled)
	{
		ShouldWriteHeader = true;
	}
}

void pfLogMgr::AddWidgets (bkBank & bank)
{
	char temp[128] = "Log - enabled";

	bank.AddToggle(temp,&Enabled,datCallback(CFA(pfLogMgr::LogCB)));
	bank.AddText("Current Log",CurrentFileNameDisplay,pfLogMgr::MaxFilenameDisplayed,true);

	// Add a combo box with the active page names
	int totalPageCount = GetRageProfiler().GetNumGroups() + 1;
	const char** comboNames = rage_new const char*[totalPageCount];
	comboNames[0] = "*no page*";
	int activePageCount = 1;
	pfPage *currentPage = NULL;
	for (int i = 0; i < totalPageCount; ++i)
	{
		currentPage = GetRageProfiler().GetNextPage(currentPage);
		if (currentPage)
		{
			comboNames[activePageCount] = currentPage->GetName();
		}
		else
		{ // No more active pages
			break;
		}

		++activePageCount;
	}

	sm_SelectionPageCombo = bank.AddCombo("Log - page", &sm_ComboPageSelection, activePageCount, comboNames, datCallback(CFA(pfLogMgr::SelectPageComboCb)), "Select the Log page to be active");

	delete [] comboNames;
}

void pfLogMgr::SelectPageComboCb ()
{
	if (sm_SelectionPageCombo == NULL)
	{
		return;
	}

	const char* pageName = sm_SelectionPageCombo->GetString(sm_ComboPageSelection);
	if (pageName == NULL)
	{
		return;
	}

	if (strcmp(pageName, "*no page*") == 0)
	{
		SetPage(NULL);
		return;
	}

	pfPage *page = GetRageProfiler().GetPageByName(pageName);
	if (page == NULL)
	{
		return;
	}

	SetPage(page);
}
#endif


void pfLogMgr::Update (bool paused)
{
	if (Enabled && !paused && (CurrentPage || UseCustomLog))
	{
		if (FileWaitingToOpen)
		{
			OpenFile(WaitingFileName);
			FileWaitingToOpen = false;
			strcpy(WaitingFileName,"");
		}

		if (OutputActive)
		{
			WriteData();
		}
	}
}


void pfLogMgr::SetFile (const char * filename)
{
	//are we the same file?
	if(CurrentStream)
	{
		if (!strcmp(filename, CurrentStream->GetName()))
		{
			return;
		}
	}

	CloseFile();
	OutputActive = false;
	CurrentStream = NULL;

	if (filename==NULL)
	{
		strcpy(WaitingFileName,"");
		FileWaitingToOpen = false;
		return;
	}
	else
	{
		strncpy(WaitingFileName,filename,MaxFilename);
		FileWaitingToOpen = true;
	}
}


void pfLogMgr::OpenFile (const char * filename)
{
	CloseFile();

	if (filename==NULL)
	{
		CurrentStream = NULL;
		strcpy(CurrentFileNameDisplay,"*NULL*");
		OutputActive = false;
		return;
	}

	CurrentStream = fiStream::Create(filename);

	if (CurrentStream)
	{
		OutputActive = true;
		strncpy(CurrentFileNameDisplay,filename,MaxFilenameDisplayed);
	}
	else
	{
		Errorf("pfLogMgr::OpenFile - couldn't open profile stream '%s'",filename);
		OutputActive = false;
		strcpy(CurrentFileNameDisplay,"*open failed*");
	}
}


void pfLogMgr::CloseFile ()
{
	if (CurrentStream)
	{
		CurrentStream->Close();
		CurrentStream = NULL;
	}
	OutputActive = false;
}


void pfLogMgr::WriteHeader ()
{
	Assert(Enabled && OutputActive && CurrentPage);

	// stream output
	Assert(CurrentStream);

	pfGroup * group;
	pfTimer * timer;
	pfValue * value;

	if (UseCustomLog)
	{
		if (CustomTimers)
		{
			for (int i = 0; i < CustomTimers->GetCount(); i++)
			{
				timer = (*CustomTimers)[i];
				if (timer->GetActive())
				{
					fprintf(CurrentStream,"%s,",timer->GetName());
				}
			}
		}
		if (CustomValues)
		{
			for (int i = 0; i < CustomValues->GetCount(); i++)
			{
				value = (*CustomValues)[i];
				if (value->GetActive())
				{
					const char	*name;
					s32			index;
					name = value->GetName(index);
					if(index >= 0)
					{
						fprintf(CurrentStream,"%s_%d,",name,index);
					}
					else
					{
						fprintf(CurrentStream,"%s,",name);
					}
				}
			}
		}
	}
	else
	{
		for (group = CurrentPage->GetNextGroup(NULL); group != NULL; group = CurrentPage->GetNextGroup(group))
		{
			for (timer = group->GetNextTimer(NULL); timer != NULL; timer = group->GetNextTimer(timer))
			{
				if (timer->GetActive())
				{
					fprintf(CurrentStream,"%s,",timer->GetName());
				}
			}

			for (value = group->GetNextValue(NULL); value != NULL; value = group->GetNextValue(value))
			{
				if (value->GetActive())
				{
					const char	*name;
					s32			index;
					name = value->GetName(index);
					if(index >= 0)
					{
						fprintf(CurrentStream,"%s_%d,",name,index);
					}
					else
					{
						fprintf(CurrentStream,"%s,",name);
					}
				}
			}
		}
	}

	fprintf(CurrentStream,"\r\n");

	ShouldWriteHeader = false;
}


void pfLogMgr::WriteSample (float sample)
{
	Assert(CurrentStream);
	fprintf(CurrentStream,"%f,",sample);
}


void pfLogMgr::WriteEOL ()
{
	Assert(CurrentStream);
	fprintf(CurrentStream,"\r\n");
}


void pfLogMgr::WriteAnnotation (const char * text)
{
	Assert(CurrentStream);
	fprintf(CurrentStream,"%s\r\n", text);
}

void pfLogMgr::Flush()
{
	Assert(CurrentStream);
	CurrentStream->Flush();
}

void pfLogMgr::WriteData ()
{
	Assert(Enabled && OutputActive && CurrentPage);

	if (ShouldWriteHeader)
	{
		WriteHeader();
	}

	pfGroup * group;
	pfTimer * timer;
	pfValue * value;

	if (UseCustomLog)
	{
		if (CustomTimers)
		{
			for (int i = 0; i < CustomTimers->GetCount(); i++)
			{
				timer = (*CustomTimers)[i];
				if (timer->GetActive())
				{
					WriteSample(timer->GetFrameTime()*1000000);
				}
			}
		}
		if (CustomValues)
		{
			for(int i = 0; i < CustomValues->GetCount(); i++)
			{
				value = (*CustomValues)[i];
				if (value->GetActive())
				{
					WriteSample(value->GetAsFloat());
				}
			}
		}
	}
	else
	{
		for (group = CurrentPage->GetNextGroup(NULL); group != NULL; group = CurrentPage->GetNextGroup(group))
		{
			for (timer = group->GetNextTimer(NULL); timer != NULL; timer = group->GetNextTimer(timer))
			{
				if (timer->GetActive())
				{
					WriteSample(timer->GetFrameTime()*1000000);
				}
			}

			for (value = group->GetNextValue(NULL); value != NULL; value = group->GetNextValue(value))
			{
				if (value->GetActive())
				{
					WriteSample(value->GetAsFloat());
				}
			}
		}
	}

	WriteEOL();
}

void pfLogMgr::SetUseCustomElements(bool b)
{
	UseCustomLog = b;
}

void pfLogMgr::SetCustomTimers(atArray<pfTimer*>* timers)
{
	CustomTimers = timers;
}

void pfLogMgr::SetCustomValues(atArray<pfValue*>* values)
{
	CustomValues = values;
}

void pfLogMgr::SetWriteHeader(bool b)
{
	ShouldWriteHeader = b;
}

#endif	// __STATS

