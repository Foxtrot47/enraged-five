//
// profile/testdraw.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

//
// PURPOSE:
//	This application is a tester for for the pfDraw* draw batching system.
//

// includes
#include "devcam/roamcam.h"
#include "grcore/im.h"
#include "grcore/setup.h"
#include "grcore/state.h"
#include "grcore/viewport.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "math/random.h"
#include "grprofile/drawmanager.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/colors.h"

using namespace rage;

//=============================================================================
// The draw static variables.

PFD_DECLARE_GROUP(Group_1);
PFD_DECLARE_SUBGROUP(SubGroup_1,Group_1);
PFD_DECLARE_ITEM(Item1,Color_blue,Group_1);
PFD_DECLARE_ITEM(Item2,Color_yellow,SubGroup_1);


//=============================================================================
// Main

mthRandom gRand;

void DrawBall(const Vector3 & center)
{
	for(int i=0; i<50; i++)
	{
		grcBegin(drawLines,4);
		for(int j=0; j<2; j++)
		{
			float a, b, c;
			Vector3 v;
			a = gRand.GetRanged(-5.0f,5.0f);
			b = gRand.GetRanged(-5.0f,5.0f);
			c = gRand.GetRanged(-5.0f,5.0f);
			v.Set(a,b,c);
			v.Normalize();
			v.Add(center);
			grcVertex3f(v);
			a = gRand.GetRanged(-5.0f,5.0f);
			b = gRand.GetRanged(-5.0f,5.0f);
			c = gRand.GetRanged(-5.0f,5.0f);
			v.Set(a,b,c);
			v.Normalize();
			v.Add(center);
			grcVertex3f(v);
		}
		grcEnd();
	}
}


void CreateDestroyTest(bool PF_DRAW_ONLY(init), bool PF_DRAW_ONLY(createBank))
{
#if __PFDRAW
	pfDrawManager test("TestManager");
	if(init)
	{
		test.Init(100,createBank);
		test.Shutdown();
	}
#endif
}


int Main()
{
	grcSetup setup;
	setup.Init(RAGE_ASSET_ROOT,"Rage Profile Draw Tester");

	setup.BeginGfx(true);
	setup.CreateDefaultFactories();

	CreateDestroyTest(false,false);
	CreateDestroyTest(true,false);
	CreateDestroyTest(true,true);

#if __PFDRAW
	GetRageProfileDraw().Init(20000);
	GetRageProfileDraw().SetEnabled(true);
#endif

	CreateDestroyTest(false,false);
	CreateDestroyTest(true,false);
	CreateDestroyTest(true,true);

	dcamRoamCam camera;
	camera.GetWorldMtx().d.y = 10.0f;
	camera.SetForceLookDown(true);

	bool quit = false;

	bool enableItem1 = true;
	bool enableItem2 = true;

	int cycleItem1 = 100;
	int cycleItem2 = 30;

	int count = 0;

	PFD_GROUP_ENABLE(Group_1,true);
	PFD_GROUP_ENABLE(SubGroup_1,true);

	do
	{
		count++;

		// Update
		setup.BeginUpdate();
		
		camera.Update(TIME.GetSeconds());

		if(count%cycleItem1==0)
		{
			enableItem1 = !enableItem1;
			PFD_ITEM_ENABLE(Item1,enableItem1);
		}

		if(count%cycleItem2==0)
		{
			enableItem2 = !enableItem2;
			PFD_ITEM_ENABLE(Item2,enableItem2);
		}

#if __PFDRAW
		if(PFD_Item1.Begin())
		{
			grcColor3f(0.0f,0.0f,1.0f);
			DrawBall(Vector3(0.0f,0.0f,0.0f));
			PFD_Item1.End();
		}

		if(PFD_Item2.Begin())
		{
			grcColor3f(0.5f,0.5f,0.5f);
			DrawBall(Vector3(3.0f,2.0f,1.0f));
			PFD_Item2.End();
		}
#endif

		setup.EndUpdate();

		// Draw
		setup.BeginDraw();

		grcViewport viewport;
		grcViewport::SetCurrent(&viewport);

		grcViewport::SetCurrentCameraMtx(camera.GetWorldMtx());

		grcLightState::SetEnabled(false);
		grcState::SetAlphaBlend(true);
		grcState::SetDepthTest(false);
		grcState::SetBlendSet(grcbsNormal);
		grcBindTexture(NULL);
 
		grcViewport::SetCurrentWorldIdentity();
		grcColor3f(0.8f,0.3f,0.3f);
		DrawBall(Vector3(-2.0f,-1.0f,0.0f));

#if __PFDRAW
		GetRageProfileDraw().Render();
#endif

		setup.EndDraw();
	} 
	while (!quit && !setup.WantExit());

#if __PFDRAW
	GetRageProfileDraw().Shutdown();
#endif

	setup.DestroyFactories();

	setup.EndGfx();

	setup.Shutdown();

	return 0;
}

// aiveh/testvehnav
