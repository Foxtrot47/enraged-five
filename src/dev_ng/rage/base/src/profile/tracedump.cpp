// 
// profile/tracedump.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "profile/tracedump.h"
#include "system/param.h"
#include "system/xtl.h"

#if __XENON && !__FINAL
#include <tracerecording.h>
#include <pmcpbsetup.h>
#pragma comment (lib, "tracerecording.lib")
#pragma comment (lib, "libpmcpb.lib")
#elif __PS3 && !__FINAL
#include <stdint.h>
#include <sn/libsntuner.h>
#endif

PARAM(tracesize, "Size of the trace buffer (360 only)");

namespace rage
{

pfTraceDump::pfTraceDump()
{
	m_TraceButton = ioPad::R1;
	m_TraceFile = "game:\\trace.bin";
}

pfTraceDump::~pfTraceDump()
{
}

void pfTraceDump::Start() const
{
#if __XENON && __DEBUGBUTTONS
	if (ioPad::GetPad(0).GetPressedDebugButtons() & m_TraceButton ) 
	{
		XTraceStartRecording(m_TraceFile);
	}
#endif
}

void pfTraceDump::Stop() const
{
#if __XENON && __DEBUGBUTTONS
	if (ioPad::GetPad(0).GetPressedDebugButtons() & m_TraceButton ) 
	{
		XTraceStopRecording();
	}
#endif
}

void pfTraceDump::Break() const
{
#if __XENON && __DEBUGBUTTONS
	if (ioPad::GetPad(0).GetPressedDebugButtons() & m_TraceButton ) 
	{
		DebugBreak();
	}
#endif
}

void pfTraceDump::SetTraceButton(unsigned int traceButton)
{
	m_TraceButton = traceButton;
}

void pfTraceDump::SetTraceFile(const char* traceFile)
{
	m_TraceFile = traceFile;
}

unsigned int pfTraceDump::GetTraceButton() const
{
	return m_TraceButton;
}

const char*	pfTraceDump::GetTraceFile() const
{
	return m_TraceFile;
}

///////////////////////////////////////////////////////////////////////////
// pfTraceDump2

const char* pfTraceDump2::m_TraceFile = "game:\\trace.pix2";

#if __PS3 && !__FINAL
 __attribute__((noinline))
void TraceDumpCaptureFn()
 {
	 __nop();
	 __nop();
	 __nop();
	 __nop();
	 __nop();
	 __nop();
	 __nop();
	 __nop();

}
#endif

void pfTraceDump2::Start()
{
	Warningf("Starting Trace");
#if __XENON && !__FINAL
	u32 traceSize = 10000000;
	PARAM_tracesize.Get(traceSize);
	XTraceSetBufferSize(traceSize);
	XTraceStartRecording(m_TraceFile);
#elif __PS3 && !__FINAL
	snStartCapture();
	TraceDumpCaptureFn();
#endif
}

void pfTraceDump2::Stop()
{
#if __XENON && !__FINAL
	XTraceStopRecording();
#elif __PS3 && !__FINAL
	TraceDumpCaptureFn();
	snStopCapture();
	TraceDumpCaptureFn();
	TraceDumpCaptureFn();
#endif

	Warningf("Stopping Trace");
}


void pfTraceDump2::SetTraceFile(const char* traceFile)
{
	m_TraceFile = traceFile;
}

const char*	pfTraceDump2::GetTraceFile()
{
	return m_TraceFile;
}


} // namespace rage
