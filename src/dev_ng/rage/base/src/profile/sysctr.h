//
// profile/sysctr.h
//
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//

// This file contains some macros to help annotate low level code with counters useful
// for profiling, similar to how hardware would provide counters for cache misses,
// instructions executed, etc. These can be used to keep track of operations such as
// memory allocations, string hash computations, etc, and are meant to hook into profiling
// tools to capture the data.
//
// Note that the counters are kept in thread-local storage, otherwise, they would be much less
// useful in multithreaded applications.
//
// All this is controlled by the PF_SYSTEM_COUNTERS #define, if that's not set, all related
// code will compile out completely.

#ifndef PROFILE_SYSCTR_H
#define PROFILE_SYSCTR_H

#include "diag/stats.h"
#include "system/tls.h"

// PURPOSE:	Main #define for controlling whether these counters should be compiled in or not.
// NOTE:	Since we use thread-local space here, it's potentially problematic for mobile platforms
//			that don't fully support that, hence the RSG_EMULATE_TLS here.
#define PF_SYSTEM_COUNTERS ((__STATS) && !(RSG_EMULATE_TLS) && !(__GAMETOOL))

#if PF_SYSTEM_COUNTERS

// PURPOSE:	Increase the system counter x by one.
#define PF_SYSTEM_COUNTER_INCREASE(x) ::rage::pfSystemCounters::x++

// PURPOSE:	Define a system counter, x.
#define PF_SYSTEM_COUNTER(x) namespace rage { namespace pfSystemCounters { __THREAD u64 x; } }

// PURPOSE: Retrieve the current value of system counter x.
#define PF_SYSTEM_COUNTER_GET(x) ::rage::pfSystemCounters::x

// PURPOSE:	External declaration of a system counter, x.
#define EXT_PF_SYSTEM_COUNTER(x) namespace rage { namespace pfSystemCounters { extern __THREAD u64 x; } }

#else	// PF_SYSTEM_COUNTERS

#define PF_SYSTEM_COUNTER_INCREASE(x)
#define PF_SYSTEM_COUNTER(x)
#define EXT_PF_SYSTEM_COUNTER(x)

#endif	// PF_SYSTEM_COUNTERS

#endif // PROFILE_SYSCTR_H

// End of file 'profile/sysctr.h'
