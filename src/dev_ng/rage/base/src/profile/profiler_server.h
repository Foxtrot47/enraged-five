#ifndef PROFILER_SERVER_H
#define PROFILER_SERVER_H

#if USE_PROFILER

#include "atl/array.h"
#include "atl/queue.h"
#include "file/handle.h"
#include "file/stream.h"
#include "atl/map.h"
#include "serialization.h"
#include "system/criticalsection.h"
#include "system/lockfreering.h"

namespace rage
{
class CompressorState;
}

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class IMessage
{
public:
	virtual void Apply() = 0;
	virtual ~IMessage() {}

	static IMessage* Create( InputDataStream& str );
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<rage::u16 MESSAGE_TYPE, rage::u16 APPLICATION_ID>
class Message : public IMessage
{
	enum { id = (MESSAGE_TYPE | (APPLICATION_ID << 16)) };
public:
	static rage::u32 GetMessageType() { return id; }
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct AppDescription
{
	rage::u32 protocolVersion;
	rage::u16 applicationID;

	AppDescription(rage::u16 appID, rage::u32 version) : applicationID(appID), protocolVersion(version) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ServerLocker;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Server
{
	friend struct ServerLocker;

	InputDataStream networkStream;

	static const int BUFFER_SIZE = 1024;
	rage::u8 inputBuffer[BUFFER_SIZE];

	fiHandle listenSocket;
	fiHandle clientSocket;

	rage::fiStream* localStream;
	rage::CompressorState* compressorState;

	AppDescription desc;

	int port;
	int range;

	bool useCompression;

	size_t bytesRequiredToSend;

	rage::sysCriticalSectionToken csServer;

	static const rage::u32 MAX_ALLOWED_BUFFER_SIZE = 2 * 1024 * 1024;
	rage::atArray<rage::u8, 0, rage::u32> outputBuffer;
	rage::atArray<rage::u8, 0, rage::u32> compressBuffer;

	rage::sysLockFreeRing<IMessage, 32> messages;

	bool UpdateConnection();
	void FlushBuffer();

public:
	Server(const AppDescription& applicationDesc, int port, int range = 3);
	~Server();

	bool SendHeader(rage::u16 type, size_t size);
	bool SendData(const void* data, size_t size);
	bool SendData(OutputDataStream& stream);

	bool Send(rage::u16 type, OutputDataStream& stream = OutputDataStream::GetEmptyStream());

	void SetLocalMode(const char* fileName);
	bool IsLocalMode() const;

	void SetListenPort(int listenPort);

	void Flush();
	void Update();

	IMessage* PopMessage();

	void Shutdown();
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ServerLocker
{
	Server& server;
	ServerLocker(Server& s) : server(s) { s.csServer.Lock(); }
	~ServerLocker() { server.csServer.Unlock(); }
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class MessageFactory
{
	typedef IMessage* (*MessageCreateFunction)(InputDataStream& str);
	rage::atMap<rage::u32, MessageCreateFunction> factory;

	MessageFactory() {}
public:
	static MessageFactory& Get();

	template<class T>
	bool RegisterMessage()
	{
		rage::sysMemAutoUseDebugMemory debugMem;
		factory.Insert(T::GetMessageType(), T::Create);
		return true;
	}

	int GetCount() const 
	{
		return factory.GetNumUsed();
	}

	IMessage* Create(InputDataStream& str);
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define REGISTER_ROCKY_MESSAGE(MessageType) MessageFactory::Get().RegisterMessage<MessageType>();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

#endif //USE_PROFILER

#endif //PROFILER_SERVER_H
