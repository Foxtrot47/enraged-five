// 
// profile/pfpacket.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "pfpacket.h"
#include "page.h"
#include "profiler.h"

#include "bank/packet.h"
#include "data/datcompressiondata.h"
#include "file/ficompressiondata.h"
#include "file/tcpip.h"
#include "system/compressionfactory.h"
#include "system/ipc.h"
#include "system/spinlock.h"

#if __BANK && __STATS

namespace rage {

#define PROFILER_REMOTE_PORT_OFFSET (bkRemotePacket::SOCKET_OFFSET_USER5 + 40)

sysCompressionFactory* pfPacket::sm_pCompressionFactory = NULL;
u8 pfPacket::sm_writeBuffer[pfPacket::MAX_WRITE_BUFFER_LENGTH];
int pfPacket::sm_writeBufferLength = 0;
sysNamedPipe pfPacket::sm_pipe;
u32 pfPacket::sm_messageCount = 0;
bool pfPacket::sm_isConnected = false;
u32 pfPacket::sm_id = 0;

#if USE_PFPACKET_SEMA
sysIpcMutex pfPacket::sm_mutex;
#endif

pfPacket::pfPacket()
	: sysPipePacket(sm_pipe)
{
}

void pfPacket::Send()
{
	int length = m_Length + sysPipePacket::HEADER_SIZE;
	if ( length + sm_writeBufferLength >= MAX_WRITE_BUFFER_LENGTH )
	{
		pfPacket::SendPackets();
	}

	SwapHeaderBytes();
	memcpy( &sm_writeBuffer[sm_writeBufferLength], &m_Length, length );
	sm_writeBufferLength += length;
}

bool pfPacket::Connect( bool waitForPipe )
{
	if ( sm_pCompressionFactory == NULL )
	{
		sm_pCompressionFactory = rage_new sysCompressionFactory();
		sm_pCompressionFactory->RegisterCompressionData( rage_new fiCompressionData );
		sm_pCompressionFactory->RegisterCompressionData( rage_new datCompressionData );
	}

	ResetMessageBuffer();

#if USE_PFPACKET_SEMA
	sm_mutex = sysIpcCreateMutex();
#endif

	// connect bank pipe:

	bool result = false;

	const char* addr = bkRemotePacket::GetRagSocketAddress();

	// wait if asked to:
	int port = bkRemotePacket::GetRagSocketPort( PROFILER_REMOTE_PORT_OFFSET );

	Displayf( "Connecting to Rag Profiler port %d... ", port );
	int retryCount = waitForPipe ? 8 : 0;
	result = false;
	do 
	{
		result = sm_pipe.Open( addr, port );
		if ( !result )
		{
			--retryCount;
			sysIpcSleep(100);
		}
	} 
	while( retryCount && !result );

	if ( result )
	{
		sm_isConnected = true;
		Displayf( "Connected." );
	}
	else
	{
		sm_isConnected = false;
		Errorf( "Unable to connect.  You may have an older version of the Profiler Rag plugin." );
	}

	return result;
}

bool pfPacket::IsConnected()
{
	return sm_isConnected;
}

void pfPacket::Close()
{
#if USE_PFPACKET_SEMA
	sysIpcLockMutex( sm_mutex );
#endif

	if ( sm_pCompressionFactory )
	{
		delete sm_pCompressionFactory;
		sm_pCompressionFactory = NULL;
	}

	sm_pipe.Close();

#if USE_PFPACKET_SEMA
	sysIpcUnlockMutex( sm_mutex );
#endif
}

void pfPacket::ReceivePackets()
{
	pfPacket p;
	while ( sm_pipe.Read(&(p.m_Length),1) == 1 ) 
	{
		sm_pipe.SafeRead( 1 + (char*)(&(p.m_Length)), sysPipePacket::HEADER_SIZE - 1 );
		p.SwapHeaderBytes();

		if (p.m_Length)
		{
			sm_pipe.SafeRead(p.m_Storage,p.m_Length);
		}

		if ( p.GetCommand() == PAGE_REF_COUNT )
		{
			p.Begin();
			u32 id = p.GetGuid();
			int count = p.Read_s32();
			p.End();

			// loop over all of the pages and find the id
			pfPage *page = GetRageProfiler().GetNextPage( NULL, false );
			while ( page )
			{
				if ( page->GetId() == id )
				{
					page->SetRefCount( count );
					break;
				}

				page = GetRageProfiler().GetNextPage( page, false );
			}
		}
	}
}

void pfPacket::SendPackets()
{
#if USE_PFPACKET_SEMA
	sysIpcLockMutex( sm_mutex );
#endif

	if ( sm_writeBufferLength > 0 )
	{
		// use temp mem
		sysMemStartTemp();

		// compress the buffer
		u32 compressedSize;
		u8* compressedStream = sm_pCompressionFactory->CompressData( datCompressionData::GetStaticHeaderPrefix(), 
			sm_writeBuffer, sm_writeBufferLength, compressedSize );

		//Displayf( "Compressed %d to %d...", sm_writeBufferLength, compressedSize );

		// now send the entire thing
		if ( compressedStream && sm_pipe.IsValid() )
		{
			sm_pipe.SafeWrite( compressedStream, compressedSize );
		}

		ResetMessageBuffer();		

		// cleanup
		delete compressedStream;

		sysMemEndTemp();
	}

#if USE_PFPACKET_SEMA
	sysIpcUnlockMutex( sm_mutex );
#endif
}

u32 pfPacket::GetNewId()
{
	return ++sm_id;
}

void pfPacket::ResetMessageBuffer()
{
	memset( sm_writeBuffer, 0, MAX_WRITE_BUFFER_LENGTH );
	sm_writeBufferLength = 0;

	pfPacket p;
	p.Begin( pfPacket::COUNT, 0, 0 );
	p.Write_u32( sm_messageCount );
	p.Send();

	++sm_messageCount;
}

}

#endif // __BANK && __STATS
