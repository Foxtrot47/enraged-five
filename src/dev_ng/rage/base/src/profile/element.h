//
// profile/element.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

//
// These are the datatypes used by pfProfiler.
// They are collected together by the pfGroup class.
//

#ifndef PROFILE_ELEMENT_H
#define PROFILE_ELEMENT_H

/////////////////////////////////////////////////////////////////
// external defines

#if __BANK && !__SPU
#include <typeinfo>
#endif

#include "data/base.h"
#include "diag/stats.h"
#include "profile/pfpacket.h"
#include "system/timer.h"
#include "trace.h"

#if !__STATS

/////////////////////////////////////////////////////////////////
// empty macro definitions for !__STATS

#define PF_TIMER(name,group)
#define PF_TIMER_OFF(name,group)
#define PF_VALUE_INT(name,group)
#define PF_VALUE_INT_OFF(name,group)
#define PF_VALUE_U16(name,group)
#define PF_VALUE_U16_OFF(name,group)
#define PF_VALUE_FLOAT(name,group)
#define PF_VALUE_FLOAT_OFF(name,group)
#define PF_VALUE_TYPE(type,count,name,group)
#define PF_COUNTER(name,group)
#define PF_COUNTER_CUMULATIVE(name,group)
#define PF_COUNTER_FLOAT(name,group)
#define PF_COUNTER_CUMULATIVE_FLOAT(name,group)
#define EXT_PF_TIMER(name)
#define EXT_PF_VALUE_INT(name)
#define EXT_PF_VALUE_U16(name)
#define EXT_PF_VALUE_FLOAT(name)
#define EXT_PF_COUNTER(name)
#define EXT_PF_COUNTER_CUMULATIVE(name)
#define EXT_PF_COUNTER_FLOAT(name)
#define EXT_PF_COUNTER_CUMULATIVE_FLOAT(name)
#define EXT_PF_VALUE_TYPE_ARRAY(type, count, name)
#define PF_START(name)
#define PF_STOP(name)
#define PF_FUNC(name)
#define PF_UNFUNC(name)
#define PF_SET(name,valueOrPointer)
#define PF_SETA(name,valueOrPointer,element)
#define PF_INCREMENT(name)
#define PF_INCREMENTBY(name,delta)
#define PF_SETTIMER(name,value)
#define PF_READ_TIME(name)				(CompileTimeAssert) // Not available in non-__STATS builds
#define PF_READ_TIME_MS(name)			(CompileTimeAssert) // Not available in non-__STATS builds

#else // __STATS

namespace rage {

class pfGroup;
class bkBank;

///////////////////////////////////////////////////////////////////
// macros for declaring and accessing element variables

// declare an element
#define PF_TIMER(name,group)									::rage::pfTimer PFTIMER_##name(#name,PFGROUP_##group,true)
#define PF_TIMER_OFF(name,group)								::rage::pfTimer PFTIMER_##name(#name,PFGROUP_##group,false)
#define PF_VALUE_INT(name,group)								::rage::pfValueT<int> PFVALUE_##name(#name,PFGROUP_##group, true)
#define PF_VALUE_INT_OFF(name,group)							::rage::pfValueT<int> PFVALUE_##name(#name,PFGROUP_##group, false)
#define PF_VALUE_U16(name,group)								::rage::pfValueT< ::rage::u16> PFVALUE_##name(#name,PFGROUP_##group,true)
#define PF_VALUE_U16_OFF(name,group)							::rage::pfValueT< ::rage::u16> PFVALUE_##name(#name,PFGROUP_##group,false)
#define PF_VALUE_FLOAT(name,group)								::rage::pfValueT<float> PFVALUE_##name(#name,PFGROUP_##group,true)
#define PF_VALUE_FLOAT_OFF(name,group)							::rage::pfValueT<float> PFVALUE_##name(#name,PFGROUP_##group,false)

//#define PF_VALUE_TYPE_ARRAY(type,count,name,group)				pfValueTA<type,count> PFVALUE_##name(#name,&PFGROUP_##group)

#define PF_VALUE_TYPE_ARRAY(type,count,name,group)				::rage::pfValuesTA<type,count> PFVALUE_##name(#name,&PFGROUP_##group,true)

#define PF_COUNTER(name,group)									::rage::pfCounter<int> PFCOUNTER_##name(#name,PFGROUP_##group,true)
#define PF_COUNTER_CUMULATIVE(name,group)						::rage::pfCounter<int> PFCOUNTER_##name(#name,PFGROUP_##group,false)

#define PF_COUNTER_FLOAT(name,group)							::rage::pfCounter<float> PFCOUNTER_##name(#name,PFGROUP_##group,true)
#define PF_COUNTER_CUMULATIVE_FLOAT(name,group)					::rage::pfCounter<float> PFCOUNTER_##name(#name,PFGROUP_##group,false)

// declare an extern element
#define EXT_PF_TIMER(name)										extern ::rage::pfTimer PFTIMER_##name
#define EXT_PF_VALUE_INT(name)									extern ::rage::pfValueT<int> PFVALUE_##name
#define EXT_PF_VALUE_U16(name)									extern ::rage::pfValueT< ::rage::u16> PFVALUE_##name
#define EXT_PF_VALUE_FLOAT(name)								extern ::rage::pfValueT<float> PFVALUE_##name
#define EXT_PF_COUNTER(name)									extern ::rage::pfCounter<int> PFCOUNTER_##name
#define EXT_PF_COUNTER_CUMULATIVE(name)							extern ::rage::pfCounter<int> PFCOUNTER_##name
#define EXT_PF_COUNTER_FLOAT(name)								extern ::rage::pfCounter<float> PFCOUNTER_##name
#define EXT_PF_COUNTER_CUMULATIVE_FLOAT(name)					extern ::rage::pfCounter<float> PFCOUNTER_##name
#define EXT_PF_VALUE_TYPE_ARRAY(type, count, name)				extern ::rage::pfValuesTA<type, count> PFVALUE_##name

// variable access
#define PF_TIMER_VAR(name)										PFTIMER_##name
#define PF_VALUE_VAR(name)										PFVALUE_##name
#define PF_COUNTER_VAR(name)									PFCOUNTER_##name

// element control
#define PF_START(name)											PFTIMER_##name.Start()
#define PF_STOP(name)											PFTIMER_##name.Stop()
#define PF_FUNC(name)											::rage::pfTimerFunc PFTIMER_FUNC##name(PFTIMER_##name)
#define PF_UNFUNC(name)											::rage::pfTimerUnFunc PFTIMER_UNFUNC##name(PFTIMER_##name)
#define PF_CANCEL(name)											PFTIMER_##name.Cancel()
#define PF_SETTIMER(name,value)									PFTIMER_##name.Set(value)
#define PF_SET(name,valueOrPointer)								PFVALUE_##name.Set(valueOrPointer)

#define PF_SETA(name,valueOrPointer,element)					PFVALUE_##name.GetValue(element)->Set(valueOrPointer)

#define PF_INCREMENT(name)										PFCOUNTER_##name.Increment()
#define PF_INCREMENTBY(name,delta)								PFCOUNTER_##name.IncrementBy(delta)

// element query
#define PF_READ_TIME(name)										PFTIMER_##name.GetFrameTime()
#define PF_READ_TIME_MS(name)									(PFTIMER_##name.GetFrameTime() * 1000.0f)


//=============================================================================
// pfTimer
// PURPOSE
//   A profile element that keeps track of time.  A timer can be started and
//   stopped multiple times in a frame.  It remembers the average time
//   per frame and the peak time in a frame over some windowed period.
//
// <FLAG Component>
//
class pfTimer : public datBase
{
public:
	pfTimer (const char *name, pfGroup &group, bool active);	// name, reset, and register the timer
	virtual ~pfTimer();

	enum {NameMaxLen=32};										// the maximum name length;
	const char * GetName () const								{return Name;}

	void Reset ();												// reset all values
	void BeginFrame ();											// signal new frame
	void EndFrame ();											// signal end of frame
	inline void FlipElapsedTime()
	{
		StoredElapsedFrameTime = UElapsedFrameTime * sysTimer::GetTicksToSeconds();
	}
	void Start ();												// start the timer (new call)
	void Set (float value);										// set the timer to a specified value
	void Stop ();												// stop the timer
	void Cancel ();												// cancel the timer (don't count time from start call)
	bool GetActive () const										{FastAssert(IsInit);return Active;}
	bool GetInit () const										{return IsInit;}
	virtual void Init ()										{IsInit = true;}

	// accessors
	int GetCalls () const										{return Calls;}
	float GetFrameTime () const;

	float GetAverageTime () const;
	float GetAverageCalls () const								{return AverageCalls;}
	static float GetAverageAlpha ()								{return AverageAlpha;}
	static void SetAverageAlpha (float alpha)					{AverageAlpha = alpha;}

	float GetPeakTime () const;
	int GetPeakCalls () const									{return PeakCalls;}
	static int GetPeakWindow ()									{return PeakWindow;}
	static void SetPeakWindow (int peakWindow)					{PeakWindow = peakWindow;}

	void StatsfFormatted () const;								// display this timer to the statsf screen

#if __BANK
	void AddWidgets (bkBank & bank);							// add the timer to the bank
	void AddTraceWidgets (bkBank& bank);						// adds a trace button to the bank
	void AddToRagProfiler( pfGroup & group );					// if connected, add to Rag Profiler
	bool IsVisible() const;
#endif

	pfTimer * GetNext ()										{return Next;}
	void SetNext (pfTimer * next)								{Next = next;}
	void AppendAtEnd (pfTimer * list);							// append list to the end of the list this is in

	enum { ELAPSED_TIME, COUNTER0, COUNTER1, MODE_COUNT };
	static int GetMode()										{return Mode;}
	static void NextMode()										{if (++Mode==MODE_COUNT) Mode=0;}

	static utimer_t GetCounter();
	static float GetScale();

	void BeginTrace();

	// Hooks that the graphics library can use to intercept starts and stops
	typedef void (*TimerEventCb)(pfTimer*);
	static TimerEventCb	sm_OnStart;
	static TimerEventCb sm_OnStop;

protected:

	bool Active;												// should this timer record/display/etc.
	bool IsInit;												// set after ::Init has been called.
	bool IsRunning;												// keep track of whether it's currently running

	pfTimer * Next;												// the next timer in a singly-linked list
	BANK_ONLY( pfGroup * Group; )

	char Name[NameMaxLen];										// the name of the timer

	utimer_t MyElapsedFrameTimeValue;
	utimer_t MyCounter0Value;
	utimer_t MyCounter1Value;

	utimer_t UElapsedFrameTime;									// cumulative time for frame
	float StoredElapsedFrameTime;								// cumulative time for frame used for rendering purposes
	float Counter0Value;
	float Counter1Value;

#if __BANK
	int PrevCalls;
	utimer_t UPrevElapsedFrameTime;
#endif

	int Calls;													// number of calls this frame

	float AverageElapsedFrameTime;								// EWMA (exponentially weighted moving average) time per frame
	float AverageCounter0Value;
	float AverageCounter1Value;

	float AverageCalls;											// EWMA calls per frame
	static float AverageAlpha;									// the decay constant for the EWMA

	float PeakElapsedFrameTime;									// peak time in the last PeakWindow frames
	float PeakCounter0Value;
	float PeakCounter1Value;

	int PeakCalls;												// peak calls per frame in same window
	static int PeakWindow;										// window size for above values

	static int Mode;

	static pfTimer* sm_TraceTimer;								// The timer we want to run a trace on

	RAGETRACE_ONLY(pfTraceCounterId m_CounterId;)

	BANK_ONLY(u32 m_id;)
};


//=============================================================================
// pfTimerFunc
// PURPOSE
//   A pfTimerFunc is a helper object that starts a pfTimer on construction
//   and stops it on destruction.  This class is a convenient way to ensure
//   that a timer is running while a particular scope (e.g. function) is active.
//   This is particularly useful when a function that is to be timed has
//   multiple exit points.
//
class pfTimerFunc
{
public:
	pfTimerFunc(pfTimer & timer);
	~pfTimerFunc();

private:
	pfTimer * Timer;
};


//=============================================================================
// pfTimerUnFunc
// PURPOSE
//   A pfTimerUnFunc is a helper object that stops a pfTimer on construction
//   and starts it on destruction.  This class is a convenient way to ensure
//   that a timer is not running while a particular scope (e.g. function) is active.
//   This is particularly useful when a function that is to be skipped has
//   multiple exit points.
//
class pfTimerUnFunc
{
public:
	pfTimerUnFunc(pfTimer & timer);
	~pfTimerUnFunc();

private:
	pfTimer * Timer;
};


//=============================================================================
// pfValue
// PURPOSE
//   A base class for profile value elements (e.g. ints, floats, vectors, colors, etc).
//
// <FLAG Component>
//
class pfValue
{
public:
	pfValue (const char * name, pfGroup & group, bool active, s32 index = -1); // create and register with the static pfProfiler
	virtual ~pfValue();

	virtual void WriteToString (char * string, int maxLen) = 0;	// write the value into a string

	bool GetActive() const;
	void SetActive(bool value);

	enum {NameMaxLen=32};

	const char * GetName(s32 &index) const;
	
	void ChangeName(const char* newName);

	virtual void BeginFrame ();

	virtual void EndFrame ();

	virtual void Init();

	pfValue * GetNext();

	void SetNext(pfValue * next);

	// PURPOSE: Append input list to the end of the list this value is in.
	void AppendAtEnd (pfValue * list);
	
	enum eGetFloatReturns
	{
		GET_AS_FLOAT_RETURNS_CUR,								// GetAsFloat returns the current value
		GET_AS_FLOAT_RETURNS_AVERAGE,							// GetAsFloat returns the current average
		GET_AS_FLOAT_RETURNS_PEAK, 								// GetAsFloat returns the current decaying peak
		MAX_GET_AS_FLOAT_RETURNS								//last value
	};
	
	virtual float GetAsFloat (eGetFloatReturns retVal = MAX_GET_AS_FLOAT_RETURNS) const;	// map the value to a float for certain uses - if you override, you will need to duplicate all the peak/average decay stuff...
	float GetAverage() const									{return m_AverageValue;} //used for sorting
	
	//hack to allow arrays with a minimum of fuss...
	virtual u32 NumValues() const								{return 1;}
	virtual pfValue *GetValue(u32 /*element*/)					{FastAssert(m_IsInit);FastAssert(NumValues() == 1);return this;}
	bool GetInit()const											{return m_IsInit;}	

	void SetPeakDecayAlpha(f32 peakDecayAlpha)					{m_PeakDecayAlpha = peakDecayAlpha;}
	void SetPeakDecayTicks(u32 peakDecayTicks)					{sm_PeakDecayTicks = peakDecayTicks;}
	void SetAverageBlendAlpha(f32 averageBlendAlpha)			{m_AverageBlendAlpha = averageBlendAlpha;}
	void SetGetAsFloatReturns(eGetFloatReturns getFloatReturns)	{m_GetFloatReturns = getFloatReturns;}

#if __BANK
	void AddWidgets (bkBank & bank);							// add the timer to the bank
	void AddToRagProfiler( pfGroup & group );					// if connected, add to Rag Profiler
	u32 GetId() const											{ return m_id; }
	bool IsVisible() const;
#endif

protected:

	virtual float GetAsFloatReal () const = 0;					// this version gets the unadulterated value, so that the massages values to be returned...
#if __BANK
	virtual float GetPrevAsFloatReal () const = 0;
#endif

	//=========================================================================
	// Data

	// PURPOSE: The name of the value.
	char m_Name[NameMaxLen];

	// PURPOSE: If this is part of an array, which index, so it can be properly named, else negative
	s32 m_Index;

	// PURPOSE: Should this value record, display, etc.?
	bool m_Active;

	// PURPOSE: Has this value been initialized?  Set after ::Init has been called.
	bool m_IsInit;

	// PURPOSE: Current moving average value - an exponentially weighted moving average (EWMA) value.
	f32 m_AverageValue;

	// PURPOSE: Current peak value - may or may not decay.
	f32 m_CurPeak;

	// PURPOSE: Number of ticks after a new peak to wait before start decaying.
	static u32 sm_PeakDecayTicks;

	// PURPOSE: When 0, the peak decays.
	u32 m_CurPeakDecayTicks;

	// PURPOSE: Alpha (multiplier) to decay current peak with each frame.
	f32 m_PeakDecayAlpha;

	// PURPOSE: Alpha (multiplier) to decay current average with each frame -- new average = new value * (1-a) + old average * a;
	f32 m_AverageBlendAlpha;

	// PURPOSE: Controlls how this value (by default) returns itself as a float (i.e. average, peak, current, etc)
	eGetFloatReturns m_GetFloatReturns;

	// PURPOSE: The next value in a singly-linked list.
	pfValue * m_Next;

	// PURPOSE: The group that this belongs to
	BANK_ONLY( pfGroup * m_Group; )

	BANK_ONLY( u32 m_id; )
};


//=============================================================================
// PURPOSE
//   Template for various profile value elements the value may either be set 
//   explicitly or be a pointer to the value.
//
// <FLAG Component>
//
template <class T> class pfValueT : public pfValue
{
public:
	pfValueT (const char * name, pfGroup & group, bool active, s32 index = -1); // create and register with the static pfProfiler

	virtual const T GetValue ()									{return (Indirect?*ValuePtr:Value);}
	virtual float GetAsFloatReal () const						{return (float)(Indirect?*ValuePtr:Value);}
#if __BANK
	virtual float GetPrevAsFloatReal () const						{return (float)(PrevValue);}
#endif
	virtual void Set (const T & value)
	{
#if __BANK
		PrevValue = Value; 
#endif
		Indirect=false;		
		Value=value;
	}
	
	virtual void Set (const T * valuePtr)
	{
#if __BANK
		PrevValue = *ValuePtr;
#endif
		Indirect=true; 
		ValuePtr=valuePtr;
	}

	virtual pfValueT<T> *GetValue(u32 /*element*/)				{FastAssert(NumValues() == 1);return this;}

	bool GetIndirect () const									{ return Indirect; }
	void WriteToString (char * string, int maxLen);				// write the value into a string

protected:
	bool Indirect;												// is it a value or pointer to a value
	union
	{
		T Value;												// the data as a value
		const T * ValuePtr;										// the data location
	};

#if __BANK
	T PrevValue;
#endif
};


//this exists so we can provide arguments to an array...
class ArrayInitHelperValueTA
{
public:
	const char	*Name;
	pfGroup		*Group;
	void		*ArrayBase;
};

template <class T> class pfValueTA;
template <class T> class ArrayInitHelperValueTADummy
{
public:
	ArrayInitHelperValueTADummy<T>(const char *name, pfGroup * group, pfValueTA<T> *arrayBase, ArrayInitHelperValueTA *Helper)
	{
		//one interesting question arises in how we might support arrays of arrays - we would need to cache the previous value somewhere and there restore it...
		
		Helper->Name = name;
		Helper->Group = group;
		Helper->ArrayBase = arrayBase;
	}
};

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4355) //we use the 'this' pointer in the initializer list, which should be safe, but the compiler complains...
#endif
template <class T> class pfValueTA : public pfValueT<T>
{
public:
	static ArrayInitHelperValueTA	arrayHelper;
	
	pfValueTA<T> () : pfValueT<T>(arrayHelper.Name, *arrayHelper.Group, arrayHelper.ArrayBase ? (this - (pfValueTA<T> *)arrayHelper.ArrayBase) : -1)
	{
		//the constructor is not used - the argument initialization does the real work here...
	}
};
#if __WIN32
#pragma warning(pop)
#endif

template <class T> ArrayInitHelperValueTA	pfValueTA<T>::arrayHelper;

template <class T, u32 numElements = 1> class pfValuesTA
{
protected:
	ArrayInitHelperValueTADummy<T>		helperDummy;
	
	pfValueTA<T>	Values[numElements];
public: 
	
	pfValuesTA (const char * name, pfGroup * group) : 
	  helperDummy(name, group, (numElements > 1) ? &Values[0] : 0, &pfValueTA<T>::arrayHelper)
	{

	}

	~pfValuesTA()
	{

	}
	
	/*virtual*/ u32 NumValues() const
	{
		return numElements;
	}
	
	/*virtual*/ pfValueT<T> *GetValue(u32 element)
	{
		FastAssert(element < numElements);
		
		return &Values[element];
	}
};

///////////////////////////////////////////////////////////////////
// pfValue: template members

namespace pfElementWrite
{
	// polymorphic functions for writing values to a string
	void Write (char * string, int maxLen, int i);
	void Write (char * string, int maxLen, float i);
};

template <class T> pfValueT<T>::pfValueT (const char * name, pfGroup & group, bool active, s32 index) : pfValue (name,group,active,index)
{
	Indirect=false;
	Value=0;
}

template <class T> void pfValueT<T>::WriteToString (char * string, int maxLen)
{
	pfElementWrite::Write(string,maxLen,GetValue());
}


//=============================================================================
// pfCounter:
// PURPOSE
//    A pfValueT subclass that stores a counter variable.  The Increment
//    function increments that counter, and it is optionally cleared
//    on each frame.
//
// <FLAG Component>
//
template<class T> class pfCounter : public pfValueT<T>
{
public:
	pfCounter (const char *name, pfGroup &group, bool frameClear);	// create and register with the static pfProfiler

	void Increment ()												{ CurrentValue++; }
	void IncrementBy (const T delta)								{ CurrentValue += delta; }
	void Clear ()													{ CurrentValue = 0; }
	void BeginFrame ()												{ pfValueT<T>::Value = CurrentValue; if (ClearEachFrame) Clear(); }

protected:
	bool ClearEachFrame;											// should the value be cleared each frame?
	T CurrentValue;
};


//=============================================================================
// Implementations

inline bool pfValue::GetActive() const
{
	FastAssert(m_IsInit);
	return m_Active;
}

inline void pfValue::SetActive(bool value)
{
	FastAssert(m_IsInit);
	m_Active = value;
}

inline const char * pfValue::GetName(s32 &index) const
{
	index = m_Index;
	return m_Name;
}

inline void pfValue::Init()
{
	m_IsInit = true;
}

inline pfValue * pfValue::GetNext()
{
	return m_Next;
}

inline void pfValue::SetNext(pfValue * next)
{
	m_Next = next;
}


template <class T> pfCounter<T>::pfCounter (const char * name, pfGroup &group, bool frameClear)
  : pfValueT<T> (name,group,true), ClearEachFrame(frameClear), CurrentValue()
{
	pfValue::SetGetAsFloatReturns(pfValue::GET_AS_FLOAT_RETURNS_CUR);
}

}	// namespace rage


///////////////////////////////////////////////////////////////////

#endif	// __STATS

#endif  // PROFILE_ELEMENT_H
