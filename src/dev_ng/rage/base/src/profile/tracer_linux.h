#ifndef TRACER_LINUX_H
#define TRACER_LINUX_H

#if RSG_LINUX
#include "atl/string.h"


namespace ftrace
{
	enum event_type : unsigned short
	{
		EVENT_NONE		   = 0,
		EVENT_SCHED_SWITCH = 256,
		EVENT_SCHED_WAKEUP = 258,
	};

	struct event_header
	{
		// PURPOSE: Timestamp when the event occurred
		int64_t timestamp;
		// PURPOSE: Process ID
		pid_t pid;
		// PURPOSE: CPU Core ID
		unsigned short cpuid;
		// PURPOSE: Task Name
		char task[16];
	};

	struct event_base
	{
		unsigned short common_type;
		unsigned char common_flags;
		unsigned char common_preempt_count;
		int common_pid;
		event_base(unsigned short type) : common_type(type) {}
	};

	// cat /sys/kernel/debug/tracing/events/sched/sched_switch
	struct sched_switch : event_base {
		char prev_comm[16];
		pid_t prev_pid;
		int prev_prio;
		long prev_state;
		char next_comm[16];
		pid_t next_pid;
		int next_prio;
		sched_switch() : event_base(EVENT_SCHED_SWITCH) {}
	};

	typedef void(*event_callback)(const ftrace::event_header& header, const ftrace::event_base& data);
}


namespace rage
{
	struct TimeScope
	{
		s64 m_Start {0};
		s64 m_Finish {INT64_MAX};
	};

	class FTrace
	{
		atString m_RootPwd;


		bool ProcessEvent(const char* buffer, ftrace::event_callback cb, TimeScope scope);
	public:
		FTrace();

		bool SetRootPassword(const char* pwd);
		bool EnableEvent(ftrace::event_type type, bool enabled = true);
		bool SetVariable(const char* name, bool value);
		bool SetVariable(const char* name, int value);
		bool SetVariable(const char* name, const char* value);

		bool Start();
		bool Stop();

		bool Parse(ftrace::event_callback cb, TimeScope scope);

		bool ExecSudo(const char* cmd);
		bool Exec(const char* cmd);

		static FTrace& Get();
	};
}

#endif //RSG_LINUX
#endif //TRACER_LINUX_H