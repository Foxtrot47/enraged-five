#include "settings.h" 

#if USE_PROFILER

#include "file/device.h"
#include "system/nelem.h"

#include "common.h"
#include "memory_pool.h"
#include "screenshot_storage.h"

using namespace rage;

namespace Profiler
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static const unsigned DEFAULT_BUFFER_GROW_SIZE = 256 << 10; // 256 Kb
static const unsigned PREALLOCATE_BUFFER_SIZE = 2048 << 10; // 2 Mb
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const ScreenshotStorage::Slot* ScreenshotStorage::QueryScreenshot()
{
	SYS_CS_SYNC(cs);
	USE_ROCKY_MEMORY;

	Slot* slot = rage_new Slot();

	if (slot)
		slots.PushAndGrow(slot);
	
	return slot;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool ScreenshotStorage::IsReady() const
{
	SYS_CS_SYNC(cs);

	for (const Slot* slot : slots)
		if (!slot->isReady)
			return false;

	return true;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ScreenshotStorage::Reset(bool force)
{
	SYS_CS_SYNC(cs);
	for (int i = slots.GetCount() - 1; i >= 0; --i)
	{
		if (slots[i]->isReady || force)
		{
			delete slots[i];
			slots.DeleteFast(i);
		}
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool ScreenshotStorage::MakeReady(const char* name)
{
	SYS_CS_SYNC(cs);

	for (Slot* slot : slots)
	{
		if (!strncmp(slot->name, name, RAGE_MAX_PATH))
		{
			slot->isReady = true;
			return true;
		}
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ScreenshotStorage::~ScreenshotStorage()
{
	Reset();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ScreenshotStorage::Slot::Slot() : isReady(false)
{
	buffer.Init(&GetProfilerAllocator(), datGrowBuffer::Flags::NULL_TERMINATE, DEFAULT_BUFFER_GROW_SIZE);

	// Let's try to allocate memory
	if (buffer.Preallocate(PREALLOCATE_BUFFER_SIZE))
	{
		// Using GrowBuffer if we have enough memory
		fiDevice::MakeGrowBufferFileName(name, NELEM(name), &buffer);
	}
	else
	{
		// We don't have enough available memory to complete the operation
		// Let's fallback to the temporary HDD copy of the screenshot
		strcpy(name, "t:/RockyScreenshot.jpg");
	}
		
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
#endif //USE_PROFILER
