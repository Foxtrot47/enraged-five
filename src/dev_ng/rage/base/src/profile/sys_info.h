#ifndef SYS_INFO_H
#define SYS_INFO_H

namespace rage 
{
	// PURPOSE: CPU info
	struct sysCpuInfo
	{
		enum Vendor
		{
			CPU_INTEL,
			CPU_AMD,
			CPU_OTHER,
		};

		// PURPOPSE: Full model of the processor
		// EXAMPLE:	 Intel(R) Core(TM) i5-2500 CPU @ 3.30GHz
		char m_Model[64];

		// PURPOSE: Vendor name
		// EXAMPLE: GenuineIntel or AuthenticAMD
		char m_VendorName[16];

		// PURPOSE: Type of the processor
		Vendor m_Vendor;

		// PURPOSE: CPU Frequency
		u64 m_Frequency;

		// PURPOSE: Size of the cache line
		// EXAMPLE: 64
		u32 m_CacheLineSize;

		// PURPOSE: AVX
		bool m_isAvxSupported;
	};

	// PURPOSE: Returns information about the current processor
	const sysCpuInfo& sysGetCpuInfo();
}
#endif // SYS_INFO_H
