
#ifndef PROFILE_STALLDETECTION_H
#define PROFILE_STALLDETECTION_H


#define STALL_DETECTION (__XENON && !__NO_OUTPUT)


namespace rage
{
class pfStallDetection
{
public:
#if STALL_DETECTION
	pfStallDetection();

	void BeginFrame();

	void EndFrame();

private:
	// Print a callstack if we exceeded our alotted time
	void CheckStall(float fTotalTimeMS);

	// Print callback for the stack trace
	static void DisplayLine(u32 addr,const char *sym,u32 offset);

	// Main entry point for the stall detection thread
	static void StallDetectionWorker(void *ptr);


	// Number of encountered stalls
	int m_StallCount;

	// True if we enabled the stall detection at the beginning of this frame
	bool m_StallDetectThisFrame;


#else // STALL_DETECTION

	inline void BeginFrame() {}
	inline void EndFrame() {}

#endif // STALL_DETECTION
};


} // namespace rage

#endif // PROFILE_STALLDETECTION_H
