//
// profile/element.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "element.h"

#include "bank/bank.h"
#include "diag/tracker.h"
#include "file/limits.h"
#include "file/stream.h"
#include "math/amath.h"
#include "profile/group.h"
#include "profile/pfpacket.h"
#include "string/string.h"
#include "cputrace.h"

#if __PPU
extern "C" {
int snPushMarker(const char *pText);
int snPopMarker();
}
extern bool g_EnableTunerAnnotation;
#endif

using namespace rage;

#if __STATS

#if __XENON && !__FINAL
#include "system/xtl.h"
#include <tracerecording.h>
#pragma comment (lib, "tracerecording.lib")
#endif

float pfTimer::AverageAlpha;									// the decay constant for the EWMA
int pfTimer::PeakWindow;										// window size for above values

int pfTimer::Mode = pfTimer::ELAPSED_TIME;

pfTimer* pfTimer::sm_TraceTimer = NULL;

void pfTimer_NoOp(pfTimer*) {}

pfTimer::TimerEventCb pfTimer::sm_OnStart = pfTimer_NoOp;
pfTimer::TimerEventCb pfTimer::sm_OnStop = pfTimer_NoOp;

///////////////////////////////////////////////////////////////////
// pfTimer

pfTimer::pfTimer (const char * name, pfGroup & group, bool active)
RAGETRACE_ONLY( : m_CounterId(name))
{
	RAGE_TRACK(pfTimer);

	Reset();

	strncpy(Name,name,NameMaxLen-1);
	Name[NameMaxLen-1] = '\0';

	Active = active;

	Next = NULL;
#if __BANK
	Group = &group;
#endif

	IsInit = false;

	IsRunning = false;

#if __BANK
	m_id = pfPacket::GetNewId();

	AddToRagProfiler( group );
#endif

	group.AddTimer(this);
}


pfTimer::~pfTimer()
{
#if __BANK
	if ( pfPacket::IsConnected() )
	{
		pfPacket packet;
		packet.Begin( pfPacket::DESTROY_TIMER, 0, 0 );
		packet.Write_u32( m_id );
		packet.Send();
	}
#endif
}

#if __BANK
void pfTimer::AddWidgets (bkBank & bank)
{
	char temp[32 + NameMaxLen];

	sprintf(temp,"timer - %s",Name);

	bank.AddToggle(temp,&Active);
}

void pfTimer::AddTraceWidgets (bkBank & bank)
{
	char temp[32 + NameMaxLen];

	sprintf(temp,"Trace %s",Name);

	bank.AddButton(temp, datCallback(MFA(pfTimer::BeginTrace), this));
}

void pfTimer::AddToRagProfiler( pfGroup & group )
{
	if ( pfPacket::IsConnected() )
	{
		pfPacket packet;
		packet.Begin( pfPacket::CREATE_TIMER, 0, 0 );
		packet.Write_u32( m_id );
		packet.Write_u32( group.GetId() );
		packet.Write_const_char( Name );
		packet.Send();
	}
}

bool pfTimer::IsVisible() const
{
	return Group->IsVisible();
}

void pfTimer::BeginTrace()
{
	sm_TraceTimer = this;
}
#endif


void pfTimer::Reset ()
{
	AverageElapsedFrameTime = AverageCounter0Value = AverageCounter1Value = 0.0f;
	PeakElapsedFrameTime = PeakCounter0Value = PeakCounter1Value = 0.0f;
	AverageCalls = 0.0f;
	PeakCalls = 0;
	StoredElapsedFrameTime = 0.0f;
	IsRunning = false;
}

void pfTimer::BeginFrame ()
{
	MyElapsedFrameTimeValue = sysTimer::GetTicks();
	MyCounter0Value = 0;
	MyCounter1Value = 0;
	
	UElapsedFrameTime = 0;
	Counter0Value = 0.0f;
	Counter1Value = 0.0f;

	Calls = 0;
}


void pfTimer::EndFrame ()
{
	if (IsRunning)
		Stop();

	const float alpha = 0.98f;

	const float ElapsedFrameTime = UElapsedFrameTime * sysTimer::GetTicksToSeconds();
	if ( AverageElapsedFrameTime == 0.0f )
	{
		AverageElapsedFrameTime = ElapsedFrameTime;		
	}
	else
	{
		AverageElapsedFrameTime = alpha * AverageElapsedFrameTime + (1.0f - alpha) * ElapsedFrameTime;		
	}

	if ( AverageCounter0Value == 0.0f )
	{
		AverageCounter0Value = Counter0Value;
	}
	else
	{
		AverageCounter0Value = alpha * AverageCounter0Value + (1.0f - alpha) * Counter0Value;
	}

	if ( AverageCounter1Value == 0.0f )
	{
		AverageCounter1Value = Counter1Value;
	}
	else
	{
		AverageCounter1Value = alpha * AverageCounter1Value + (1.0f - alpha) * Counter1Value;
	}

	PeakElapsedFrameTime = (ElapsedFrameTime > PeakElapsedFrameTime) ? ElapsedFrameTime : PeakElapsedFrameTime;
	PeakCounter0Value = (Counter0Value > PeakCounter0Value) ? Counter0Value : PeakCounter0Value;
	PeakCounter1Value = (Counter1Value > PeakCounter1Value) ? Counter1Value : PeakCounter1Value;

	if ( AverageCalls == 0.0f )
	{
		AverageCalls = (float)Calls;
	}
	else
	{
		AverageCalls = alpha * AverageCalls + (1.0f - alpha) * (float)Calls;
	}

	PeakCalls = (Calls > PeakCalls) ? Calls : PeakCalls;

#if __BANK
	if ( pfPacket::IsConnected() && IsVisible()
		&& ((UElapsedFrameTime != UPrevElapsedFrameTime) || (Calls != PrevCalls)) )
	{
		pfPacket packet;
		packet.Begin( pfPacket::UPDATE_TIMER, 0, 0 );
		packet.Write_u32( m_id );
		
		packet.Write_float( ElapsedFrameTime );
		packet.Write_float( Counter0Value );
		packet.Write_float( Counter1Value );
		
		packet.Write_float( AverageElapsedFrameTime );
		packet.Write_float( AverageCounter0Value );
		packet.Write_float( AverageCounter1Value );
		
		packet.Write_float( PeakElapsedFrameTime );
		packet.Write_float( PeakCounter0Value );
		packet.Write_float( PeakCounter1Value );
		
		packet.Write_s32( Calls );
		packet.Write_float( AverageCalls );
		packet.Write_s32( PeakCalls );
		packet.Send();
	}
#endif
}


utimer_t pfTimer::GetCounter()
{
	switch (Mode)
	{
	case ELAPSED_TIME:
		return sysTimer::GetTicks();
	default:
		return 0;
	}
}


float pfTimer::GetScale()
{
	switch (Mode)
	{
	case ELAPSED_TIME:
		return sysTimer::GetTicksToSeconds();
	default:
		return 1.0f / 1E6f;
	}
}

void pfTimer::Start ()
{
	IsRunning = true;

	MyElapsedFrameTimeValue = sysTimer::GetTicks();
	MyCounter0Value = 0;
	MyCounter1Value = 0;

	pfTimer::sm_OnStart(this);

	// 	This will fuck up because the trace can't handle multiple markers with the same name
	//	_RAGETRACE_PUSH(m_CounterId);
}

void pfTimer::Set(float value)
{
#if __BANK
	PrevCalls = Calls;
	UPrevElapsedFrameTime = UElapsedFrameTime;
#endif

	Calls++;
	
	UElapsedFrameTime = (utimer_t)(value / sysTimer::GetTicksToSeconds());

	// not sure if these should be set
	Counter0Value = value;
	Counter1Value = value;
}


void pfTimer::Stop ()
{
	// 	This will fuck up because the trace can't handle multiple markers with the same name
	//	RAGETRACE_POP();

	pfTimer::sm_OnStop(this);

#if __BANK
	PrevCalls = Calls;
	UPrevElapsedFrameTime = UElapsedFrameTime;
#endif

	Calls++;

	UElapsedFrameTime += (sysTimer::GetTicks() - MyElapsedFrameTimeValue);
//	Counter0Value += (0 - MyCounter1Value) * (1.0f / 1E6f);
//	Counter1Value += (0 - MyCounter0Value) * (1.0f / 1E6f);

	IsRunning = false;
}


void pfTimer::Cancel ()
{
}

float pfTimer::GetFrameTime () const
{
	switch ( Mode )
	{
	case ELAPSED_TIME:
//		return ElapsedFrameTime;
		return StoredElapsedFrameTime;

	case COUNTER0:
		return Counter0Value;

	case COUNTER1:
		return Counter1Value;
	}

	return 0.0f;
}

float pfTimer::GetAverageTime() const
{
	switch ( Mode )
	{
	case ELAPSED_TIME:
		return AverageElapsedFrameTime;

	case COUNTER0:
		return AverageCounter0Value;

	case COUNTER1:
		return AverageCounter1Value;
	}

	return 0.0f;
}

float pfTimer::GetPeakTime() const
{
	switch ( Mode )
	{
	case ELAPSED_TIME:
		return PeakElapsedFrameTime;

	case COUNTER0:
		return PeakCounter0Value;

	case COUNTER1:
		return PeakCounter1Value;
	}

	return 0.0f;
}


void pfTimer::AppendAtEnd (pfTimer * list)
{
	if (list!=NULL)
	{
		pfTimer * end = this;

		while (end->Next != NULL)
		{
			end = end->Next;
		};

		end->Next = list;
	}
}
				   
				   
///////////////////////////////////////////////////////////////////
// pfTimerFunc

pfTimerFunc::pfTimerFunc(pfTimer& timer) :
	Timer(&timer)
{
	Timer->Start();
}


pfTimerFunc::~pfTimerFunc()
{
	Timer->Stop();
}


///////////////////////////////////////////////////////////////////
// pfTimerUnFunc

pfTimerUnFunc::pfTimerUnFunc(pfTimer& timer) :
Timer(&timer)
{
	Timer->Stop();
}


pfTimerUnFunc::~pfTimerUnFunc()
{
	Timer->Start();
}


///////////////////////////////////////////////////////////////////

u32 pfValue::sm_PeakDecayTicks = 10;


pfValue::pfValue (const char * name, pfGroup & group, bool active, s32 index)
{
	strncpy(m_Name,name,NameMaxLen-1);
	m_Name[NameMaxLen-1] = '\0';

	m_Active = active;
	m_Index = index;

	m_Next = NULL;

#if __BANK
	m_Group = &group;
#endif

	m_IsInit = false;

#if __BANK
	m_id = pfPacket::GetNewId();

	// if we're the first value of an array, consume an id (it will become the array's unique id)
	if ( m_Index == 0 )
	{
		m_id = pfPacket::GetNewId();
	}

	AddToRagProfiler( group );
#endif

	group.AddValue(this);

	m_AverageValue = 0;

	m_GetFloatReturns = GET_AS_FLOAT_RETURNS_CUR;
	//m_GetFloatReturns = GET_AS_FLOAT_RETURNS_AVERAGE;
	//m_GetFloatReturns = GET_AS_FLOAT_RETURNS_PEAK;

	m_CurPeak = 0;
	m_CurPeakDecayTicks = 0;
	m_PeakDecayAlpha = .98f;
	m_AverageBlendAlpha = .98f;
}


pfValue::~pfValue()
{
#if __BANK
	if ( pfPacket::IsConnected() )
	{
		pfPacket packet;
		packet.Begin( (m_Index == -1) ? pfPacket::DESTROY_VALUE : pfPacket::DESTROY_ARRAY_VALUE, 0, 0 );
		packet.Write_u32( m_id );
		packet.Send();
	}
#endif
}


float pfValue::GetAsFloat (eGetFloatReturns retVal) const
{
	if(retVal >= MAX_GET_AS_FLOAT_RETURNS)
	{
		retVal = m_GetFloatReturns;
	}
	
	switch(retVal)
	{
	case GET_AS_FLOAT_RETURNS_CUR:
		return GetAsFloatReal();
	
	case GET_AS_FLOAT_RETURNS_AVERAGE:
		return m_AverageValue;
	
	case GET_AS_FLOAT_RETURNS_PEAK:
		return m_CurPeak;
	
	default:
		Assert(0);
		return GetAsFloatReal();
	}
}

void pfValue::ChangeName(const char* newName)
{
	strncpy(m_Name, newName, NameMaxLen);
	m_Name[NameMaxLen-1] = '\0';
}

void pfValue::BeginFrame()
{
}

void pfValue::EndFrame()
{
#if __BANK
	float prevAvg = m_AverageValue;
	float prevPeak = m_CurPeak;
#endif

	//determine the current average...
	m_AverageValue = m_AverageBlendAlpha * m_AverageValue + (1.0f - m_AverageBlendAlpha) * GetAsFloatReal();

	//determine if we have a new peak...
	if(GetAsFloatReal() >= m_CurPeak)
	{
		//new peak...
		m_CurPeak = GetAsFloatReal();
		m_CurPeakDecayTicks = sm_PeakDecayTicks;
	}
	else
	{
		//see about the decay...
		if(m_CurPeakDecayTicks)
		{
			m_CurPeakDecayTicks--;
		}
		else
		{
			//do the decay towards zero...
			m_CurPeak = m_PeakDecayAlpha * m_CurPeak;
		}
	}

#if __BANK
	if ( pfPacket::IsConnected() && IsVisible()
		&& ((GetAsFloatReal() != GetPrevAsFloatReal()) || (prevAvg != m_AverageValue) || (prevPeak != m_CurPeak)) )
	{
		pfPacket packet;
		packet.Begin( pfPacket::UPDATE_VALUE, 0, 0 );
		packet.Write_u32( m_id );
		packet.Write_float( GetAsFloatReal() );
		packet.Write_float( m_AverageValue );
		packet.Write_float( m_CurPeak );
		packet.Send();
	}
#endif
}

#if __BANK
void pfValue::AddWidgets (bkBank & bank)
{
	char temp[32 + NameMaxLen];

	sprintf(temp,"value - %s",m_Name);

	bank.AddToggle(temp,&m_Active);
}

void pfValue::AddToRagProfiler( pfGroup & group )
{
	if ( pfPacket::IsConnected() )
	{
		pfPacket packet;
		if ( m_Index == -1 )
		{
			packet.Begin( pfPacket::CREATE_VALUE, 0, 0 );
			packet.Write_u32( m_id );
			packet.Write_u32( group.GetId() );
			packet.Write_const_char( m_Name );
			packet.Send();
		}
		else
		{
			// create the containing array
			if ( m_Index == 0 )
			{
				pfPacket arrayPacket;
				arrayPacket.Begin( pfPacket::CREATE_ARRAY, 0, 0 );
				arrayPacket.Write_u32( m_id - 1 );
				arrayPacket.Write_u32( group.GetId() );
				arrayPacket.Write_const_char( m_Name );
				arrayPacket.Send();
			}

			packet.Begin( pfPacket::CREATE_ARRAY_VALUE, 0, 0 );
			packet.Write_u32( m_id );
			packet.Write_u32( m_id - m_Index - 1 );	// the id of the array
			
			char buf[NameMaxLen + 16];
			sprintf( buf, "%s_%d", m_Name, m_Index );

			packet.Write_const_char( buf );
			packet.Send();
		}		
	}
}

bool pfValue::IsVisible() const
{
	return m_Group->IsVisible();
}
#endif



void pfElementWrite::Write (char * string, int /*maxLen*/, int i)
{
	// TODO: should pay attention to max length
	sprintf(string,"%d",i);
}


void pfElementWrite::Write (char * string, int /*maxLen*/, float f)
{
	// TODO: should pay attention to max length
	sprintf(string,"%f",f);
}


void pfValue::AppendAtEnd (pfValue * list)
{
	if (list!=NULL)
	{
		pfValue * end = this;

		while (end->m_Next != NULL)
		{
			end = end->m_Next;
		};

		end->m_Next = list;
	}
}

#endif	// __BANK

