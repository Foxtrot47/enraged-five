#include "stream_profiler.h"

#if USE_PROFILER

#include "rocky_memory.h"
#include "rocky.h"
#include "core.h"

using namespace rage;

namespace Profiler
{
#if USE_PROFILER_IO
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static rage::sysCriticalSectionToken s_deviceProfilerToken;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DeviceProfiler::DeviceProfiler()
{
	readDescription = Profiler::EventDescription::Create("IO::Read");
	writeDescription = Profiler::EventDescription::Create("IO::Write");
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DeviceProfiler::~DeviceProfiler()
{
	USE_ROCKY_MEMORY;
	openedHandles.Reset();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const DeviceProfiler::Desc* DeviceProfiler::RegisterHandle(fiHandle handle, const char* file)
{
	TagDescription* tag = Profiler::TagDescription::CreateShared(file);
	EventDescription* event = Profiler::EventDescription::CreateShared(file);
	
	if (event)
		event->category = Profiler::CategoryColor::Resources;

	DeviceProfiler::Desc desc = { tag, event };

	USE_ROCKY_MEMORY;
	SYS_CS_SYNC(s_deviceProfilerToken);
	return &(openedHandles.Insert(handle, desc).data);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnRegister(fiHandle handle, const char * file)
{
	SYS_CS_SYNC(s_deviceProfilerToken);
	if (openedHandles.Access(handle) == nullptr)
		RegisterHandle(handle, file);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnOpen(fiHandle handle, const char *file)
{
	SYS_CS_SYNC(s_deviceProfilerToken);
	const DeviceProfiler::Desc* desc = RegisterHandle(handle, file);
	PROFILER_CUSTOM_TAG(desc->tag);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnRead(fiHandle /*handle*/, size_t /*size*/, rage::u64 /*offset*/)
{
	PROFILER_CUSTOM_EVENT_PUSH("fiDevice::Read");
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnReadInternal(fiHandle handle, size_t size, rage::u64 /*offset*/)
{
	StartEvent(handle, readDescription, size);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnReadEnd(fiHandle handle, size_t /*size*/)
{
	FinishEvent(handle);
	PROFILER_CUSTOM_EVENT_POP(nullptr);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnWrite(fiHandle handle, size_t size)
{
	PROFILER_CUSTOM_EVENT_PUSH("fiDevice::Write");
	StartEvent(handle, writeDescription, size);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnWriteEnd(fiHandle handle)
{
	FinishEvent(handle);
	PROFILER_CUSTOM_EVENT_POP(nullptr);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnSeek(fiHandle /*handle*/, rage::u64 /*offset*/)
{

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnClose(fiHandle handle)
{
	USE_ROCKY_MEMORY;
	SYS_CS_SYNC(s_deviceProfilerToken);
	openedHandles.Delete(handle);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EventStorageScope
{
	EventStorage* previous;
	EventStorageScope(EventStorage* current)
	{
		previous = TLS::storage;
		TLS::storage = current;
	}

	~EventStorageScope()
	{
		TLS::storage = previous;
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::StartEvent(fiHandle handle, const EventDescription* eventDesc, size_t size)
{
	SYS_CS_SYNC(s_deviceProfilerToken);

	const Desc* desc = openedHandles.Access(handle);
	if (desc == nullptr)
	{
		char name[256];
		sprintf(name, "0x%llx", (unsigned long long)handle);
		desc = RegisterHandle(handle, name);
	}

	if (ThreadEntry* entry = Core::Get().GetIOThreadEntry())
	{
		if (entry->storage.mode & Mode::IO)
		{
			// Executing thread
			PROFILER_CUSTOM_TAG(desc->tag);

			// IO thread
			EventStorageScope scope(&entry->storage);
			Profiler::StackedEvent::StackPushForce(eventDesc);
			Profiler::StackedEvent::StackPushForce(desc->event);
			PROFILER_CUSTOM_TAG(desc->tag);
			PROFILER_TAG("Size", size);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::FinishEvent(fiHandle handle)
{
	SYS_CS_SYNC(s_deviceProfilerToken);

	if (openedHandles.Access(handle))
	{
		if (ThreadEntry* entry = Core::Get().GetIOThreadEntry())
		{
			if (entry->storage.mode & Mode::IO)
			{
				EventStorageScope scope(&entry->storage);
				Profiler::StackedEvent::StackPopForce();
				Profiler::StackedEvent::StackPopForce();
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool DeviceProfiler::OnPushEvent(Profiler::EventDescription* desc)
{
	if (ThreadEntry* entry = Core::Get().GetIOThreadEntry())
	{
		if (entry->storage.mode & Mode::IO)
		{
			EventStorageScope scope(&entry->storage);
			Profiler::StackedEvent::StackPushForce(desc);
			return true;
		}
	}
	return false;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DeviceProfiler::OnPopEvent()
{
	if (ThreadEntry* entry = Core::Get().GetIOThreadEntry())
	{
		if (entry->storage.mode & Mode::IO)
		{
			EventStorageScope scope(&entry->storage);
			Profiler::StackedEvent::StackPopForce();
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif // USE_PROFILER_IO
}

#endif //USE_PROFILER
