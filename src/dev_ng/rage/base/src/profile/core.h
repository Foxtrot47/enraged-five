#ifndef PROFILER_CORE_H
#define PROFILER_CORE_H

#include "settings.h"

#if USE_PROFILER

//rage include
#include "atl/dlist.h"
#include "atl/vector.h"
#include "atl/string.h"
#include "diag/channel.h"
#include "math/amath.h"
#include "rocky_message.h"
#include "system/criticalsection.h"
#include "system/dependency.h"
#include "system/ipc.h"

#include "event.h"
#include "serialization.h"
#include "sampler.h"
#include "memory_pool.h"
#include "event_tracer.h"
#include "profiler_server.h"
#include "screenshot_storage.h"
#include "stream_profiler.h"
#include <map>

namespace rage
{
	class RsonWriter;
}

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ScopeHeader
{
	rage::u32 boardNumber;
	rage::u32 threadNumber;
	EventTime event;

	ScopeHeader();
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator << ( OutputDataStream& stream, const ScopeHeader& ob);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TArray>
void ReserveExtraCapacityForEventBuffer(TArray& arr, rage::u32 extraCount)
{
	if (arr.GetCount() + (int)extraCount <= arr.GetCapacity())
		return;

	rage::u32 extraReserveCount = rage::Max((rage::u32)extraCount, (rage::u32)4 * 1024);
	arr.ReserveExtend(extraReserveCount);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef MemoryPool<EventData, 1024> ScopeEventBuffer;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ScopeData
{
	ScopeHeader header;
	ScopeEventBuffer events;

	void AddEvent(const EventData& data)
	{
		if (data.description != nullptr)
		{
			events.Push(data);
		}
	}

	void InitRootEvent(const EventData& data)
	{
		header.event = data;
		AddEvent(data);
	}

	void Send();
	void Clear(bool preserveMemory = true);
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct SwitchContextMap;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator << ( OutputDataStream& stream, const ScopeData& ob);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ThreadEntry
{
	ROCKY_ALIGN_CACHE EventStorage storage;
	ThreadDescription description;
	EventStorage** threadTLS;

	ThreadEntry(const ThreadDescription& desc, EventStorage** tls) : description(desc), threadTLS(tls) {}
	void Activate(rage::u32 mode);
	void Clear();
	void SortEvents();
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct CounterBase
{
	float minTime;
	float avgTime;
	float maxTime;
	float sumTime;
	float count;
	bool IsValid() const { return (count > SMALL_FLOAT) && (fabs(avgTime) > SMALL_FLOAT); }
	CounterBase() : minTime(FLT_MAX), avgTime(0.0f), maxTime(0.0f), sumTime(0.0f), count(0.0f) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TDescription>
struct Counter : public CounterBase
{
	const TDescription* description;
	Counter() : description(nullptr) {}
	Counter(const TDescription* desc) : description(desc) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct CategoryCounter : public CounterBase
{
	Filter::Type category;
	CategoryCounter() : category(Filter::Type::Null) {}
	CategoryCounter(Filter::Type cat) : category(cat) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef Counter<EventDescription> EventCounter;
typedef Counter<TagDescription> TagCounter;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TDescription>
struct CounterGroup
{
	rage::atString name;
	rage::atArray<Counter<TDescription> > counters;
	rage::atArray<CategoryCounter> categoryCounters;
	CounterGroup() {}
	CounterGroup(const char* groupName) : name(groupName) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef CounterGroup<EventDescription> EventCounterGroup;
typedef CounterGroup<TagDescription> TagCounterGroup;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ThreadCollectionGroup
{
	ThreadEntry* threadEntry;
	bool subtractWaiting;
	ThreadCollectionGroup(ThreadEntry* entry, bool noWaiting = false) : threadEntry(entry), subtractWaiting(noWaiting) {}
	ThreadCollectionGroup() : threadEntry(nullptr), subtractWaiting(false) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct Frame
{
	EventTime event;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RockyThreadFilter
{
	bool m_Initialized;
	rage::atArray<rage::atString> m_Threads;
	void Init();
	void Add(const char* name);
	bool IsOK(const ThreadDescription& desc);
	RockyThreadFilter() : m_Initialized(0) { }
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static rage::u32 GetCurrentProcessId()
{
#if __POSIX || RSG_SCE || RSG_LINUX
	return getpid();
#elif RSG_WIN32
	return ::GetCurrentProcessId();
#else
#error "not yet implemented on this platform"
#endif
}

class Core
{
	friend class App;

	rage::sysIpcCurrentThreadId mainThreadID;

	rage::atVector<ThreadEntry*> threads;

	rage::atVector<ThreadDescription> threadDescriptions;
	rage::atVector<ProcessDescription> processDescriptions;

	ThreadEntry* gpuThreadEntry;
	ThreadEntry* gpuVSyncThreadEntry;
	ThreadEntry* ioThreadEntry;

	rage::s64 progressReportedLastTimestampMS;
	int samplingFrequency;
	int scriptSamplingFrequency;
	int frameLimit;

	rage::u32 currentBoardNumber;

	rage::atVector<Frame, 0, rage::u32> frames[FRAME_COUNT];

	Server server;

	rage::sysCriticalSectionToken m_csCallbacksToken;
	ScreenshotCallback screenshotCallback;
	LiveDataCallback liveDataCallback;
	rage::atArray<CaptureControlCallback> captureControlCallbacks;

	rage::atArray<LiveData> liveDataBuffer;

	rage::sysDependency liveDependency;
	//rage::sysCountdownEvent liveDepEvent;

	rage::sysDependency updateDependency;
	volatile bool updateDependencyRunning;

	rage::atString outputFileName;

#if USE_PROFILER_IO
	DeviceProfiler deviceProfiler;
#endif //USE_PROFILER_IO

	ScreenshotStorage screenshotStorage;

	RockyThreadFilter threadFilter;

	void UpdateEvents();
	void Update(bool singleThreaded = true);
	void UpdateLive(bool singleThreaded = true);
	void DoUpdateLive(rage::u32 frameIdx);

	Core();
	~Core();

	static Core notThreadSafeInstance;

	void DumpCapturingProgress();
	void SendHandshakeResponse(EventTracer::Status status);

	// Serialize and send frames
	void DumpCallstacks(rage::u32 boardNumber);

	// Serialize and send switch contexts
	void DumpSwitchContexts(rage::u32 boardNumber, SynchronizationBuffer& switchContexts);

	// Serialize and send hardware events
	void DumpHardwareEvents(rage::u32 boardNumber);
	void DumpHardwareDescriptionBoard(rage::u32 boardNumber);

	// Serialize and send page faults data
	void DumpPageFaults(rage::u32 boardNumber);

	// Serialize and send description board
	rage::u32 DumpDescriptionBoard(EventTime interval, rage::u32 mode);

	// Cleanup all unregistered threads, removing from the threads array.
	void CleanupThreads();

	// Serialize and send all the data
	bool Dump();

	// Serialize and send frames
	bool DumpFrames(rage::u32 mode, bool singleThreaded);
	
	// Merges all the GPU events in one thread
	bool MergeGPUEvents();

	// Returns number of fully collected frames
	int GetNumCollectedFrames(FrameType type) const;

	// Returns number of fully collected frames
	rage::u32 GetFirstValidFrameTime(FrameType type) const;

	// Serialize and send sampling data
	bool DumpSamplingData();

	// Current mode
	rage::u32 currentMode;
	rage::u32 pendingMode;
	
	struct DumpRequest
	{
		rage::u32 dumpMode;
		bool isSinglethreaded;
		DumpRequest();
		void Reset();
	};
	DumpRequest dumpRequest;

	rage::u32 lastUsedMode;

	// Attached screenshot to the current frame
	void AttachScreenshot(EventStorage* storage);

	// Generate Budgets
	bool GenerateCounterGroup(EventCounterGroup& group, const rage::atArray<ThreadCollectionGroup>& threads, const SwitchContextMap& switchContexts, FrameType frameType) const;

	// Updates Debug Input
	void UpdateInput();

#if USE_PROFILER_BANDWIDTH_TRACKING
    NetworkBandwidthBuffer m_NetworkBandwidthBuffer;
    NetworkBandwidthEntry* m_NetworkBandwidthLastBufferEntry;

    void DumpNetworkBandwidth(rage::u32 boardNumber);
#endif

    void CleanupNetworkBandwidth(bool bPreseverMemory);

	void DumpThreadDescriptions(rage::u32 boardNumber);

public:

#if USE_PROFILER_BANDWIDTH_TRACKING
    void AttachNetworkBandwidthData(rage::u32 len, const rage::u8* data);
#endif

    // Returns the next board number
    rage::u32 GetNextBoardNumber() const { return currentBoardNumber + 1; }

    // Returns the current board number
    rage::u32 GetCurrentBoardNumber() const { return currentBoardNumber; }

    bool IsCurrentModeLive() const;

	// Returns previous mode
	rage::u32 Activate(rage::u32 mode);
	
	// Controls sampling routine
	Sampler sampler;

	// Threads Synchronization token
	mutable rage::sysCriticalSectionToken m_csThreadsToken;

	// Synchronization token
	rage::sysCriticalSectionToken m_csCoreToken;

	// Returns thread collection
	const rage::atVector<ThreadEntry*>& GetThreads() const;

	// Returns GPU thread entry
	ThreadEntry* GetGPUThreadEntry() const;

	// Returns IO thread entry
	ThreadEntry* GetIOThreadEntry() const;

	// Starts sampling process
	void StartSampling();

	// Starts live session
	void StartLive(rage::u32 mode);

	// Asynchronous live update
	bool UpdateLiveAsync(rage::u32 frameIdx);

	// Asynchronous network update
	bool UpdateAsync();

	// Serialize and send current profiling progress
	void DumpProgress(const char* message = "");

	// Too much time from last report
	bool IsTimeToReportProgress() const;

	// Requests full dump of the data
	void RequestDump(rage::u32 mode, bool singleThreaded = false);

	// Serialize all the current state to the file
	const char* DumpToFile(const char* fileName, bool force = false, bool singleThreaded = false);

	// Current Mode
	rage::u32 GetMode() const { return currentMode; }

	// Registers thread and create EventStorage
	bool RegisterThread(const ThreadDescription& description);

	// Unregisters a thread. If the mode is OFF, it will be removed immediately.
	//	Otherwise, it will be removed after the next Stop message.
	bool UnregisterThread(rage::sysIpcCurrentThreadId threadId);

	// Registers thread description only (cleaned after capture is done)
	bool RegisterThreadDescription(const ThreadDescription& description);

	// Registers process description only (cleaned after capture is done)
	bool RegisterProcessDescription(const ProcessDescription& description);

	// Get current Server
	Server& GetServer() { return server; }

	// Set screenshot callback
	void RegisterScreenshotCallback(ScreenshotCallback cb);

	// Notify screenshot ready
	void NotifyScreenshotReady(const char* name, bool isOk);

	// Set livedata callback
	void RegisterLiveDataCallback(LiveDataCallback cb);

	// Releases all the resources used by the core
	void Shutdown();

	// Starts Rocky capture on next frame
	void StartCapture(rage::u32 mode = Mode::DEFAULT);

	// Stops Rocky capture
	rage::u32 StopCapture();

	// Generate Budgets
	struct BudgetSettings
	{
		// PURPOSE: Discard all the event below specified threshold in milliseconds
		float m_Threshold;
		// PURPOSE: Sort by category, then by time
		bool m_Sort;
		// PURPOSE: Use total time instead of average
		bool m_UseTotalTime;
		BudgetSettings() : m_Threshold(0.0f), m_Sort(false), m_UseTotalTime(false) {}
	};

	bool GenerateBudgets(rage::RsonWriter& rw, const BudgetSettings& settings);
	bool GenerateBudgets(rage::atArray<EventCounterGroup>& budgets, const BudgetSettings& settings);
	bool GenerateBudgets(rage::atArray<TagCounterGroup>& budgets, const BudgetSettings& settings);

	// Set AutoSampling frequency
	void SetSamplingFrequency(int frequency);
	int GetSamplingFrequency() const;

	void SetScriptSamplingFrequency(int frequency);
	int GetScriptSamplingFrequency() const;

	// Set connection port
	void SetListenPort(int port);

	// Set Maximum number of frames to collect
	void SetMaxCaptureFrameLimit(int limit);

	// Set Root Password for the target device
	void SetRootPassword(RockyPlatform::Type platform, const char* password);
	void SetEncodedRootPassword(RockyPlatform::Type platform, const char* password);

	// Registers a callback for capture start
	void RegisterCaptureControlCallback(CaptureControlCallback cb);
	
	// UnRegisters a callback for capture start
	void UnRegisterCaptureControlCallback(CaptureControlCallback cb);

	// Register custom callback for each category
	void RegisterCaptureCategoryCallback(CaptureCategoryCallback callback, rage::sysIpcCurrentThreadId threadId);

	// Increments next GPU frame
	void NextGpuFrame(rage::u32 frameNumber, rage::u64 timestamp);

	// Appends next GPU Flip Event
	void NextGpuFlip(const GPUFrame& gpuFrame);

	// Generates the list of Rocky telemetry keywords
	bool GetTelemetryKeywords(rage::atArray<rage::atString>& keywords);

	// NOT Thread Safe singleton (performance)
	static ROCKY_INLINE Core& Get() { return notThreadSafeInstance; }

	// Main Update Function
	static void NextFrame() { Get().Update(); }

	// Get Active ThreadID
	static ROCKY_INLINE rage::sysIpcCurrentThreadId GetThreadID() { return Get().mainThreadID; }
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER

#endif //PROFILER_CORE_H
