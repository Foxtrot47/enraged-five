#ifndef SAMPLER_H
#define SAMPLER_H

#if USE_PROFILER

#include "sym_engine.h"

#include "atl/vector.h"
#include "system/criticalsection.h"
#include "system/xtl.h"

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ThreadEntry;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Sampler
{
	MemoryPool<CallStackBuffer, 1024> callstacks;
	rage::atVector<ThreadEntry*> targetThreads;
	ResolveScriptSymbolCallback resolveScriptSymbolCb;
public:	
	SymEngine symEngine;

	Sampler();
	~Sampler();

	void RegisterResolveScriptSymbolCallback(ResolveScriptSymbolCallback callback);
		
	bool IsActive() const;

	void StartSampling(const rage::atVector<ThreadEntry*>& threads);
	bool StopSampling();

	CallStackBuffer* TryAppendCallstack(rage::u64 threadID, rage::u64 timestamp);

	size_t GetCollectedCount() const;
	OutputDataStream& Serialize(OutputDataStream& stream);

	typedef rage::atVector<const Symbol*, 0, rage::u32> SymbolArray;
	void ResolveAddresses(const rage::stlUnorderedSet<rage::u64>& addresses, SymbolArray& symbols);
	void ResolveScriptAddresses(const rage::stlUnorderedSet<rage::u64>& addresses, SymbolArray& symbols);
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& os, const Symbol * const symbol);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER

#endif //SAMPLER_H
