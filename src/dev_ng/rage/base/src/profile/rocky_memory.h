#ifndef ROCKY_MEMORY_H
#define ROCKY_MEMORY_H


#if USE_PROFILER
#include "system/memory.h"

#if !__RESOURCECOMPILER
#define USE_ROCKY_MEMORY USE_DEBUG_MEMORY()
#else
namespace Profiler
{
	rage::sysMemAllocator& GetRockyDebugAllocator(); 
}

#define USE_ROCKY_MEMORY rage::sysMemAutoUseAllocator useAllocator(Profiler::GetRockyDebugAllocator())
#endif


#endif //USE_PROFILER

#endif //ROCKY_MEMORY_H