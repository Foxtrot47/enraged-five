#include "primitives.h"
#include "system/criticalsection.h"
#include "system/pix.h"

#if RSG_PROFILE_RENDERER

#if !defined(_MSC_VER) || _MSC_VER >= 1700

namespace rage {

namespace debugDraw {

static pfIndexedPrimitive *s_Primitives[256];
static HandlerIndex primitiveIndex;
static u8 s_NextPrimitive;

static void primitiveHandler(BatchInterface *b,Header *h) {
	b->m_RenderInterface->SetConstantColor(*(unsigned*)(h+1));
	b->m_RenderInterface->DrawIndexedPrimtive(s_Primitives[h->byteParam]);
}

#define IMPLEMENT_PRIMITIVE_HANDLER(_ctor,_color) \
	static u8 index; \
	if (!index) { \
		SYS_CS_SYNC(g_ThreadSafeInit_CS); \
		if (!index) { \
			if (!++s_NextPrimitive) \
				Quitf("Too many registered indexed primtives"); \
			s_Primitives[index = s_NextPrimitive] = _ctor; \
		} \
	} \
	g_debugDraw->AddCommandAndPayload(primitiveIndex,primitiveHandler,index,sizeof(pfColor),&color);

#define IMPLEMENT_SCALED_DRAW(_func) \
	PushWorldMatrix(); \
	Mat34V scaleMtx, finalMtx; \
	Mat34VFromScale(scaleMtx, scale, Vec3V(V_ZERO)); \
	Transform(finalMtx, mtx, scaleMtx); \
	SetWorldMatrix(finalMtx); \
	_func; \
	PopWorldMatrix();


pfIndexedPrimitive* CreateBox(float s) {
	const float x1 = -s, y1 = -s, z1 = -s;
	const float x2 = +s, y2 = +s, z2 = +s;
	float vertices[24] = {
		x1,y1,z1,
		x2,y1,z1,
		x2,y2,z1,
		x1,y2,z1,
		x1,y1,z2,
		x2,y1,z2,
		x2,y2,z2,
		x1,y2,z2,
	};
	static u16 indices[24] = {
		0,1, 1,2, 2,3, 3,0,
		4,5, 5,6, 6,7, 7,4,
		0,4, 1,5, 2,6, 3,7,
	};
	return g_pfRenderFactory->CreateIndexedPrimitive(pfLines,vertices,24,pfPos,indices,24);
}

pfIndexedPrimitive* CreateSolidBox(float s) {
	const float x1 = -s, y1 = -s, z1 = -s;
	const float x2 = +s, y2 = +s, z2 = +s;
	float vertices[] = {
		x1,y1,z1,0,0,-1,
		x2,y1,z1,0,0,-1,
		x2,y2,z1,0,0,-1,
		x1,y2,z1,0,0,-1,

		x1,y1,z2,0,0,+1,
		x1,y2,z2,0,0,+1,
		x2,y2,z2,0,0,+1,
		x2,y1,z2,0,0,+1,

		x2,y1,z2,+1,0,0,
		x2,y2,z2,+1,0,0,
		x2,y2,z1,+1,0,0,
		x2,y1,z1,+1,0,0,

		x1,y1,z1,-1,0,0,
		x1,y2,z1,-1,0,0,
		x1,y2,z2,-1,0,0,
		x1,y1,x2,-1,0,0,

		x1,y2,z2,0,+1,0,
		x1,y2,z1,0,+1,0,
		x2,y2,z1,0,+1,0,
		x2,y2,z2,0,+1,0,

		x1,y1,z1,0,-1,0,
		x1,y1,z2,0,-1,0,
		x2,y1,z2,0,-1,0,
		x2,y1,z1,0,-1,0,
	};
	static u16 indices[] = {
		3,2,1,3,1,0,
		7,6,5,7,5,4,
		11,10,9,11,9,8,
		15,14,13,15,13,12,
		19,18,17,19,17,16,
		23,22,21,23,21,20,
	};
	return g_pfRenderFactory->CreateIndexedPrimitive(pfTriangles,vertices,sizeof(vertices)/sizeof(vertices[0]),pfPosNormal,indices,sizeof(indices)/sizeof(indices[0]));
}

pfIndexedPrimitive* CreateSphere(float radius,unsigned longSteps,unsigned latSteps,unsigned longSkip,unsigned latSkip) {
	// Generate the vertices
	struct v3 { float x,y,z; };
	v3* v = Alloca(v3,longSteps*latSteps), *vp = v;
	for (unsigned zi=0; zi<latSteps; zi++) {
		float zt = (PI * zi) / (latSteps-1);
		float z = cosf(zt) * radius, sz = sinf(zt) * radius;
		for (unsigned ti=0; ti<longSteps; ti++) {
			float t = ti * PI*2 / (longSteps-1);
			vp->x = cosf(t) * sz;
			vp->z = sinf(t) * sz;
			vp->y = z;
			++vp;
		}
	}
	// Generate the indices (there's actually some extra slop here because longitude lines don't wrap)
	u16* i = Alloca(u16,longSteps*latSteps*2*2), *ip = i;
	// Latitude lines (full circles, even at the poles for now to keep the code simple)
	for (unsigned zi=0; zi<latSteps; zi+=latSkip) {
		for (unsigned ti=0; ti<longSteps; ti++) {
			*ip++ = (u16)(zi*longSteps + ti);
			*ip++ = (u16)(zi*longSteps + ((ti+1)%longSteps));
		}
	}
	// Longitude lines
	for (unsigned zi=0; zi<latSteps-1; zi++) {
		for (unsigned ti=0; ti<longSteps; ti+=longSkip) {
			*ip++ = (u16)(zi*longSteps + ti);
			*ip++ = (u16)(((zi+1)*longSteps) + ti);
		}
	}

	// Create the indexed primitive (depending on longSkip and latSkip we may produce far fewer indices that we allocated for)
	return g_pfRenderFactory->CreateIndexedPrimitive(pfLines,&v->x,vp-v,pfPos,i,ip-i);
}

pfIndexedPrimitive* CreateSolidSphere(float radius,unsigned longSteps,unsigned latSteps) {
	// Generate the vertices
	struct v3n3 { float x,y,z,nx,ny,nz; };
	v3n3* v = Alloca(v3n3,longSteps*latSteps), *vp = v;
	for (unsigned zi=0; zi<latSteps; zi++) {
		float zt = (PI * zi) / (latSteps-1);
		float z = cosf(zt) * radius, sz = sinf(zt) * radius;
		for (unsigned ti=0; ti<longSteps; ti++) {
			float t = ti * PI*2 / (longSteps-1);
			vp->x = cosf(t) * sz;
			vp->y = z;
			vp->z = sinf(t) * sz;
			vp->nx = cosf(t);
			vp->ny = cosf(zt);
			vp->nz = sinf(t);
			++vp;
		}
	}
	// Generate the indices (there's actually some extra slop here because longitude lines don't wrap)
	u16* i = Alloca(u16,longSteps*latSteps*6), *ip = i;
	// Latitude lines (full circles, even at the poles for now to keep the code simple)
	for (unsigned zi=0; zi<latSteps-1; zi+=1) {
		for (unsigned ti=0; ti<longSteps; ti++) {
			u16 i0 = (u16)(zi*longSteps + ti);
			u16 i1 = (u16)(zi*longSteps + ((ti+1)%longSteps));
			u16 i2 = (u16)((zi+1)*longSteps + ((ti+1)%longSteps));
			u16 i3 = (u16)((zi+1)*longSteps + ti);
			ip[0] = i3;
			ip[1] = i2;
			ip[2] = i1;
			ip[3] = i3;
			ip[4] = i1;
			ip[5] = i0;
			ip+=6;
		}
	}

	// Create the indexed primitive (depending on longSkip and latSkip we may produce far fewer indices that we allocated for)
	return g_pfRenderFactory->CreateIndexedPrimitive(pfTriangles,&v->x,vp-v,pfPosNormal,i,ip-i);
}



pfIndexedPrimitive* CreateSphericalCone(float radius,unsigned longSteps,unsigned latSteps,unsigned longSkip,unsigned latSkip) {
	// Generate the vertices
	struct v3 { float x,y,z; };
	v3* v = Alloca(v3,longSteps*latSteps), *vp = v;
	for (unsigned zi=0; zi<latSteps; zi++) {
		float z = ((float)zi/(latSteps-1))*radius, sz = z;
		for (unsigned ti=0; ti<longSteps; ti++) {
			float t = ti * PI*2 / (longSteps-1);
			vp->x = cosf(t) * sz;
			vp->y = sinf(t) * sz;
			vp->z = z;
			++vp;
		}
	}
	// Generate the indices (there's actually some extra slop here because longitude lines don't wrap)
	u16* i = Alloca(u16,longSteps*latSteps*2*2), *ip = i;
	// Latitude lines (full circles, even at the poles for now to keep the code simple)
	for (unsigned zi=0; zi<latSteps;) {
		for (unsigned ti=0; ti<longSteps; ti++) {
			*ip++ = (u16)(zi*longSteps + ti);
			*ip++ = (u16)(zi*longSteps + ((ti+1)%longSteps));
		}
		zi+=latSkip;
		if (zi == latSteps)
			--zi;
	}
	// Longitude lines
	for (unsigned ti=0; ti<longSteps; ti+=longSkip) {
		*ip++ = (u16)(ti);
		*ip++ = (u16)(((latSteps-1)*longSteps) + ti);
	}

	// Create the indexed primitive (depending on longSkip and latSkip we may produce far fewer indices that we allocated for)
	return g_pfRenderFactory->CreateIndexedPrimitive(pfLines,&v->x,vp-v,pfPos,i,ip-i);
}


pfIndexedPrimitive* CreateFrustum(float scale) {
	struct v3 { float x,y,z; };
	v3 v[5] = {{scale, scale, -scale}, {scale,-scale,-scale}, {-scale,-scale,-scale}, {-scale,scale,-scale}, {0,0,0} };
	static u16 i[16] = { 0,1, 1,2, 2,3, 3,0, 4,0, 4,1, 4,2, 4,3 };
	return g_pfRenderFactory->CreateIndexedPrimitive(pfLines,&v[0].x,5,pfPos,i,16);
}

pfIndexedPrimitive* CreateCircle(float r,unsigned steps) {
	struct v3 { float x,y,z; };
	v3 *v = Alloca(v3,steps);
	u16 *idx = Alloca(u16,steps+1);
	for (unsigned i=0; i<steps; i++) {
		float t = 2 * PI * i / (float)steps;
		v[i].x = cosf(t) * r;
		v[i].y = sinf(t) * r;
		v[i].z = 0.0f;
		Assign(idx[i], i);
	}
	idx[steps] = 0;
	return g_pfRenderFactory->CreateIndexedPrimitive(pfLineStrip,v,steps,pfPos,idx,steps+1);
}

pfIndexedPrimitive* CreateSolidCircle(float r,unsigned steps) {
	struct v3 { float x,y,z; };
	v3 *v = Alloca(v3,steps+1);
	u16 *idx = Alloca(u16,steps*3);
	for (unsigned i=0; i<steps; i++) {
		float t = 2 * PI * i / (float)steps;
		v[i].x = cosf(t) * r;
		v[i].y = sinf(t) * r;
		v[i].z = 0.0f;
		Assign(idx[i*3+0], steps);
		Assign(idx[i*3+1], i);
		Assign(idx[i*3+2], (i+1)%steps);
	}
	v[steps].x = v[steps].y = v[steps].z = 0.0f;
	return g_pfRenderFactory->CreateIndexedPrimitive(pfLineStrip,v,steps+1,pfPos,idx,steps*3);
}

pfIndexedPrimitive* CreateCylinder(float radius,float height,unsigned steps) {
	struct v3 { float x,y,z; };
	v3 *v = Alloca(v3,steps*2);
	u16 *idx = Alloca(u16,steps*6);

	unsigned bottom = 0, top = steps*2, middle = steps*4;
	for (unsigned i=0; i<steps; i++) {
		float t = 2 * PI * i / (float)steps;
		v[i].x = v[i+steps].x = cosf(t) * radius;
		v[i].z = v[i+steps].z = sinf(t) * radius;
		v[i].y = height * -0.5f;
		v[i+steps].y = height * 0.5f;
		Assign(idx[bottom+i*2+0], i);		// Bottom circle
		Assign(idx[bottom+i*2+1], i+1==steps?0:i+1);		// Bottom circle
		Assign(idx[top+i*2+0], i+steps);	// Top circle
		Assign(idx[top+i*2+1], i+1==steps?steps:i+steps+1);	// Top circle
		Assign(idx[middle+i*2+0], i);// Line from bottom...
		Assign(idx[middle+i*2+1], i+steps);	// ...to top
	}

	return g_pfRenderFactory->CreateIndexedPrimitive(pfLines,v,steps*2,pfPos,idx,steps*6);
}


pfIndexedPrimitive* CreateSolidCylinder(float radius,float height,unsigned steps) {
	struct v3n3 { float x,y,z,nx,ny,nz; };
	v3n3 *v = Alloca(v3n3,steps*2);
	u16 *idx = Alloca(u16,steps*6), *ip = idx;

	for (unsigned i=0; i<steps; i++) {
		float t = 2 * PI * i / (float)steps;
		float x = cosf(t);
		float z = sinf(t);
		float yb = height * -0.5f;
		float yt = height * 0.5f;
		// Bottom side
		v[i].x = x * radius;
		v[i].y = yb;
		v[i].z = z * radius;
		v[i].nx = x;
		v[i].ny = 0;
		v[i].nz = z;
		// Top side
		v[i+steps].x = x * radius;
		v[i+steps].y = yt;
		v[i+steps].z = z * radius;
		v[i+steps].nx = x;
		v[i+steps].ny = 0;
		v[i+steps].nz = z;
	}
	// Sides
	for (unsigned i=0; i<steps; i++) {
		unsigned ni = i+1==steps? 0 : i+1;
		unsigned i0 = i, i1 = steps+i, i2 = steps+ni, i3 = ni;
		Assign(*ip++, i0);
		Assign(*ip++, i1);
		Assign(*ip++, i2);
		Assign(*ip++, i0);
		Assign(*ip++, i2);
		Assign(*ip++, i3);
	}

	return g_pfRenderFactory->CreateIndexedPrimitive(pfTriangles,v,steps*2,pfPosNormal,idx,ip-idx);
}


pfIndexedPrimitive* CreateSolidCappedCylinder(float radius,float height,unsigned steps) {
	struct v3n3 { float x,y,z,nx,ny,nz; };
	v3n3 *v = Alloca(v3n3,steps*4);
	u16 *idx = Alloca(u16,steps*12), *ip = idx;	// steps*2 is an upper bound, we get the exact math right in the call

	for (unsigned i=0; i<steps; i++) {
		float t = 2 * PI * i / (float)steps;
		float x = cosf(t);
		float z = sinf(t);
		float yb = height * -0.5f;
		float yt = height * 0.5f;
		// Bottom cap
		v[i].x = x * radius;
		v[i].y = yb;
		v[i].z = z * radius;
		v[i].nx = 0;
		v[i].ny = -1.0f;
		v[i].nz = 0;
		// Top cap
		v[i+steps].x = x * radius;
		v[i+steps].y = yt;
		v[i+steps].z = z * radius;
		v[i+steps].nx = 0;
		v[i+steps].ny = +1.0f;
		v[i+steps].nz = 0;
		// Bottom side
		v[i+steps*2].x = x * radius;
		v[i+steps*2].y = yb;
		v[i+steps*2].z = z * radius;
		v[i+steps*2].nx = x;
		v[i+steps*2].ny = 0;
		v[i+steps*2].nz = z;
		// Top side
		v[i+steps*3].x = x * radius;
		v[i+steps*3].y = yt;
		v[i+steps*3].z = z * radius;
		v[i+steps*3].nx = x;
		v[i+steps*3].ny = 0;
		v[i+steps*3].nz = z;
	}
	// Bottom endcap
	for (unsigned i=2; i<steps; i++) {
		Assign(*ip++, 0);
		Assign(*ip++, i-1);
		Assign(*ip++, i);
	}
	// Top endcap
	for (unsigned i=2; i<steps; i++) {
		Assign(*ip++, steps);
		Assign(*ip++, steps+i);
		Assign(*ip++, steps+i-1);
	}
	// Sides
	for (unsigned i=0; i<steps; i++) {
		unsigned ni = i+1==steps? 0 : i+1;
		unsigned i0 = steps*2+i, i1 = steps*3+i, i2 = steps*3+ni, i3 = steps*2+ni;
		Assign(*ip++, i0);
		Assign(*ip++, i1);
		Assign(*ip++, i2);
		Assign(*ip++, i0);
		Assign(*ip++, i2);
		Assign(*ip++, i3);
	}

	return g_pfRenderFactory->CreateIndexedPrimitive(pfTriangles,v,steps*4,pfPosNormal,idx,ip-idx);
}


void SetLifetime(unsigned ftl) {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *h) {
		b->m_FramesToLive = h->byteParam;
		// Modify the header so that when this is preserved it will end up one smaller on next frame
		if (h->byteParam!=255)
			--(h->byteParam);
	};
	g_debugDraw->AddCommand(index,handler,ftl>254?254:(u8)ftl,0);
}

void SetWorldIdentity() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		b->m_World = Mat34V(V_IDENTITY);
		b->m_RenderInterface->SetWorldMatrix(b->m_World);
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void SetWorldMatrix(Mat34V_In m34) {
	if (IsEqualAll(m34.GetCol0(), Vec3V(V_X_AXIS_WZERO)) && IsEqualAll(m34.GetCol1(), Vec3V(V_Y_AXIS_WZERO)) && IsEqualAll(m34.GetCol2(), Vec3V(V_Z_AXIS_WZERO))) {
		if (IsEqualAll(m34.GetCol3(),Vec3V(V_ZERO)))
			SetWorldIdentity();
		else {
			static HandlerIndex index;
			auto handler = [](BatchInterface *b,Header *h) {
				Vec3V trans(V_ZERO);
				memcpy(&trans,h+1,12);
				Mat34VFromTranslation(b->m_World, trans);
				b->m_RenderInterface->SetWorldMatrix(b->m_World);
			};
			g_debugDraw->AddCommandAndPayload(index,handler,0,12,&m34.GetCol3ConstRef());
		}
	}
	else {
		static HandlerIndex index;
		auto handler = [](BatchInterface *b,Header *h) {
			memcpy(&b->m_World,(h+1),sizeof(Mat34V));
			b->m_RenderInterface->SetWorldMatrix(b->m_World);
		};
		// TODO: Could save four words here by ignoring the .w channel.
		g_debugDraw->AddCommandAndPayload(index,handler,0,sizeof(m34),&m34);
	}
}

void PushWorldMatrix() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		if (b->m_WorldStackCount < BatchInterface::MaxDepth)
			b->m_WorldStack[b->m_WorldStackCount++] = b->m_World;
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void MultiplyWorldMatrix(Mat34V_In m34) {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *h) {
		Mat34V other;
		memcpy(&other,h+1,sizeof(other));
		Transform(b->m_World, other);
		b->m_RenderInterface->SetWorldMatrix(b->m_World);
	};
	g_debugDraw->AddCommandAndPayload(index,handler,0,sizeof(m34),&m34);
}

void PopWorldMatrix() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		if (b->m_WorldStackCount) {
			b->m_World = b->m_WorldStack[--(b->m_WorldStackCount)];
			b->m_RenderInterface->SetWorldMatrix(b->m_World);
		}
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void PushCameraMatrix() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		if (b->m_CameraStackCount < BatchInterface::MaxDepth)
			b->m_CameraStack[b->m_CameraStackCount++] = b->m_Camera;
	};
	// TODO: Immediate mode stack too?
	g_debugDraw->AddCommand(index,handler,0,0);
}

void SetCameraMatrix(Mat34V_In m34) {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *h) {
		memcpy(&b->m_Camera,(h+1),sizeof(Mat34V));
		b->UpdateInternalMatrices();
		b->m_RenderInterface->SetCameraMatrix(b->m_Camera);
	};
	// Need this to take effect immediately so that culling and orienting to viewport both work.
	g_debugDraw->m_Camera = m34;
	// TODO: Could save four words here by ignoring the .w channel.
	g_debugDraw->AddCommandAndPayload(index,handler,0,sizeof(m34),&m34);
}

void PopCameraMatrix() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		if (b->m_CameraStackCount) {
			b->m_Camera = b->m_CameraStack[--(b->m_CameraStackCount)];
			b->UpdateInternalMatrices();
			b->m_RenderInterface->SetCameraMatrix(b->m_Camera);
		}
	};
	// TODO: Immediate mode stack too?
	g_debugDraw->AddCommand(index,handler,0,0);
}

void PushProjectionMatrix() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		if (b->m_ProjectionStackCount < BatchInterface::MaxDepth)
			b->m_ProjectionStack[b->m_ProjectionStackCount++] = b->m_Projection;
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void SetProjectionMatrix(Mat44V_In m44) {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *h) {
		memcpy(&b->m_Projection,(h+1),sizeof(Mat44V));
		b->UpdateInternalMatrices();
		b->m_RenderInterface->SetProjectionMatrix(b->m_Projection);
	};
	// Need this to take effect immediately so that culling and orienting to viewport both work.
	g_debugDraw->m_Projection = m44;
	// TODO: Could save four words here by ignoring the .w channel.
	Assert(IsFiniteAll(m44));
	g_debugDraw->AddCommandAndPayload(index,handler,0,sizeof(m44),&m44);
}

void PopProjectionMatrix() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		if (b->m_ProjectionStackCount) {
			b->m_Projection = b->m_ProjectionStack[--(b->m_ProjectionStackCount)];
			b->UpdateInternalMatrices();
			b->m_RenderInterface->SetProjectionMatrix(b->m_Projection);
		}
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void PushViewport() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		if (b->m_ViewportStackCount < BatchInterface::MaxDepth)
			b->m_ViewportStack[b->m_ViewportStackCount++] = b->m_Viewport;
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void SetViewport(const pfViewport &v) {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *h) {
		memcpy(&b->m_Viewport,(h+1),sizeof(pfViewport));
		b->m_RenderInterface->SetViewport(b->m_Viewport);
	};
	g_debugDraw->AddCommandAndPayload(index,handler,0,sizeof(v),&v);
}

void PopViewport() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		if (b->m_ViewportStackCount) {
			b->m_Viewport = b->m_ViewportStack[--(b->m_ViewportStackCount)];
			b->m_RenderInterface->SetViewport(b->m_Viewport);
		}
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void PushDepthStencilState() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		b->m_RenderInterface->PushDepthStencilState();
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void PopDepthStencilState() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		b->m_RenderInterface->PopDepthStencilState();
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void SetDepthStencilState(pfDepthStencilState dss) {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *h) {
		b->m_RenderInterface->SetDepthStencilState(static_cast<pfDepthStencilState>(h->byteParam));
	};
	g_debugDraw->AddCommand(index,handler,(u8)dss,0);
}


void PushRasterizerState() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		b->m_RenderInterface->PushRasterizerState();
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void PopRasterizerState() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		b->m_RenderInterface->PopRasterizerState();
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void SetRasterizerState(pfRasterizerState rs) {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *h) {
		b->m_RenderInterface->SetRasterizerState(static_cast<pfRasterizerState>(h->byteParam));
	};
	g_debugDraw->AddCommand(index,handler,(u8)rs,0);
}


void PushBlendState() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		b->m_RenderInterface->PushBlendState();
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void PopBlendState() {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *) {
		b->m_RenderInterface->PopBlendState();
	};
	g_debugDraw->AddCommand(index,handler,0,0);
}

void SetBlendState(pfBlendState bs) {
	static HandlerIndex index;
	auto handler = [](BatchInterface *b,Header *h) {
		b->m_RenderInterface->SetBlendState(static_cast<pfBlendState>(h->byteParam));
	};
	g_debugDraw->AddCommand(index,handler,(u8)bs,0);
}


void DrawLine(const float *p1,const float *p2,pfColor c1,pfColor c2) {
	static HandlerIndex index;
	struct params { float p1[3]; unsigned c1; float p2[3]; unsigned c2; };
	auto handler = [](BatchInterface *b,Header *h) {
		void *r = b->m_RenderInterface->Begin(pfLines, 2, pfPosColor);
		if (r) {
			memcpy(r, h+1, sizeof(params));
			b->m_RenderInterface->End();
		}
	};
	params p = { { p1[0], p1[1], p1[2] }, c1, { p2[0], p2[1], p2[2] }, c2 };
	g_debugDraw->AddCommandAndPayload(index,handler,0,sizeof(params),&p);
}

void DrawBox(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateBox(0.5f),color);
}

void DrawBox(Vec3V_In scale,Mat34V_In mtx,pfColor color) {
	IMPLEMENT_SCALED_DRAW(DrawBox(color));
}

void DrawSolidBox(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateSolidBox(0.5f),color);
}

void DrawSolidBox(Vec3V_In scale,Mat34V_In mtx,pfColor color) {
	IMPLEMENT_SCALED_DRAW(DrawSolidBox(color));
}

void DrawSphere(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateSphere(1.0f,64,32,4,4),color);
}
	
void DrawSphere(Vec3V_In scale,Mat34V_In mtx,pfColor color) {
	IMPLEMENT_SCALED_DRAW(DrawSphere(color));
}

void DrawSolidSphere(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateSolidSphere(1.0f,32,16),color);
}

void DrawSolidSphere(Vec3V_In scale,Mat34V_In mtx,pfColor color) {
	IMPLEMENT_SCALED_DRAW(DrawSolidSphere(color));
}

void DrawAxis() {
	DrawLine(Vec3V(V_ZERO),Vec3V(V_X_AXIS_WZERO),Color32(255,0,0));
	DrawLine(Vec3V(V_ZERO),Vec3V(V_Y_AXIS_WZERO),Color32(0,255,0));
	DrawLine(Vec3V(V_ZERO),Vec3V(V_Z_AXIS_WZERO),Color32(0,0,255));
}

void DrawAxis(Vec3V_In scale,Mat34V_In mtx) {
	IMPLEMENT_SCALED_DRAW(DrawAxis());
}

void DrawSphericalCone(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateSphericalCone(1.0f,64,32,4,4),color);
}

void DrawFrustum(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateFrustum(0.5f),color);
}

void DrawFrustum(Vec3V_In scale,Mat34V_In mtx,pfColor color) {
	IMPLEMENT_SCALED_DRAW(DrawFrustum(color));
}

void DrawCircle(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateCircle(1.0f,64),color);
}

void DrawSolidCircle(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateSolidCircle(1.0f,64),color);
}

void DrawCircle(float r, Vec3V_In center, Vec3V_In axisX, Vec3V_In axisY,pfColor color) {
	Vec3V scale(r,r,r);
	Mat34V mtx(axisX, axisY, Cross(axisX,axisY), center);
	IMPLEMENT_SCALED_DRAW(DrawCircle(color));
}

void DrawSolidCircle(float r, Vec3V_In center, Vec3V_In axisX, Vec3V_In axisY,pfColor color) {
	Vec3V scale(r,r,r);
	Mat34V mtx(axisX, axisY, Cross(axisX,axisY), center);
	IMPLEMENT_SCALED_DRAW(DrawSolidCircle(color));
}

void DrawCylinder(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateCylinder(1.0f,1.0f,32),color);
}

void DrawCylinder(float height,float radius,Mat34V_In mtx,pfColor color) {
	Vec3V scale(radius,height,radius);
	IMPLEMENT_SCALED_DRAW(DrawCylinder(color));
}

void DrawSolidCylinder(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateSolidCylinder(1.0f,1.0f,32),color);
}

void DrawSolidCylinder(float height,float radius,Mat34V_In mtx,pfColor color) {
	Vec3V scale(radius,height,radius);
	IMPLEMENT_SCALED_DRAW(DrawSolidCylinder(color));
}

void DrawSolidCappedCylinder(pfColor color) {
	IMPLEMENT_PRIMITIVE_HANDLER(debugDraw::CreateSolidCappedCylinder(1.0f,1.0f,32),color);
}

void DrawSolidCappedCylinder(float height,float radius,Mat34V_In mtx,pfColor color) {
	Vec3V scale(radius,height,radius);
	IMPLEMENT_SCALED_DRAW(DrawSolidCappedCylinder(color));
}

void DrawArc(pfColor color, float beginAngle,float endAngle,int steps) {
	static HandlerIndex index;
	struct params { unsigned color; float beginAngle, deltaAngle; };
	auto handler = [](BatchInterface *b,Header *h) {
		params &p = *(params*)(h+1);
		b->m_RenderInterface->SetConstantColor(p.color);
		float *v = (float*) b->m_RenderInterface->Begin(pfLineStrip,h->byteParam,pfPos);
		for (int i=0; i < h->byteParam; i++,v+=3) {
			float t = p.beginAngle + p.deltaAngle / (h->byteParam-1);
			v[0] = cosf(t);
			v[1] = sinf(t);
			v[2] = 0.0f;
		}
		b->m_RenderInterface->End();
	};
	params p = { color, beginAngle, endAngle-beginAngle };
	Assert(steps>1 && steps<=255);
	g_debugDraw->AddCommandAndPayload(index,handler,(u8)steps,sizeof(p),&p);
}

void DrawArc(float r, Vec3V_In center, Vec3V_In axisX, Vec3V_In axisY, pfColor color, float beginAngle, float endAngle, int steps) {
	Vec3V scale(r,r,r);
	Mat34V mtx(axisX, axisY, Cross(axisX,axisY), center);
	IMPLEMENT_SCALED_DRAW(DrawArc(color,beginAngle,endAngle,steps));
}

#undef IMPLEMENT_PRIMITIVE_HANDLER

#undef IMPLEMENT_SCALED_DRAW

void PushDefaultScreen() {
	PushCameraMatrix();
	SetCameraMatrix(Mat34V(V_IDENTITY));
	PushProjectionMatrix();
	Mat44V ortho;
	MakeOrthoLH(ortho,0,(float)g_pfRenderFactory->GetScreenWidth(),(float)g_pfRenderFactory->GetScreenHeight(),0,0.0f,1.0f);
	SetProjectionMatrix(ortho);
	PushWorldMatrix();
	SetWorldIdentity();
}

void PopDefaultScreen() {
	PopWorldMatrix();
	PopProjectionMatrix();
	PopCameraMatrix();
}

void SetTexture(pfTexture *tex) {
	auto handler = [](BatchInterface *b,Header *h) {
		b->m_RenderInterface->SetTexture(h->byteParam? *(pfTexture**)(h+1) : 0);
	};
	static HandlerIndex index;
	if (tex)
		g_debugDraw->AddCommandAndPayload(index,handler,1,sizeof(pfTexture*),&tex);
	else
		g_debugDraw->AddCommand(index,handler,0,0);
}

bool Project(Vec3V_InOut outNormPos,Vec3V_In pos,Mat34V_In camera,Mat44V_In proj) {
	Vec4V dest = Multiply(proj,Vec4V(UnTransformOrtho(camera,pos),ScalarV(V_ONE)));
	if (dest.GetWf() > 0) {
		ScalarV oow(1.0f / dest.GetWf());
		Vec3V sc(0.5f,-0.5f,0.5f);
		outNormPos = dest.GetXYZ() * oow * sc + Vec3V(V_HALF);
		return true;
	}
	else
		return false;
}

static inline Vec3V_Out ProjectiveTransform(Mat44V_In mtx,Vec4V_In in) {
	Vec4V result = Multiply(mtx,in);
	return result.GetXYZ() / result.GetW();
}

void UnProject(float xNorm,float yNorm,Vec3V_InOut outNear,Vec3V_InOut outFar) {
	// Convert from 0..1 to -1..+1
	xNorm = (xNorm - 0.5f) * 2.0f;
	yNorm = (yNorm - 0.5f) * 2.0f;
	outNear = ProjectiveTransform(g_debugDraw->m_InverseViewProj,Vec4V(xNorm,yNorm,0.0f,1.0f));
	outFar = ProjectiveTransform(g_debugDraw->m_InverseViewProj,Vec4V(xNorm,yNorm,1.0f,1.0f));
}

void DrawString(float x,float y,float z,float scale,pfColor rgba,const char *string,unsigned len) {
	struct params { pfFont *font; float x, y, z, scale; unsigned rgba, len; char payload[0]; };
	auto handler = [](BatchInterface *b,Header *h) {
		params &p = *(params*)(h+1);
		b->m_RenderInterface->DrawString(p.font,p.x,p.y,p.z,p.scale,p.rgba,p.payload,p.len);
	};
	static HandlerIndex index;
	if (g_debugDraw->m_IsImmediate)
		g_debugDraw->m_RenderInterface->DrawString(g_debugDraw->m_Font,x,y,z,scale,rgba,string,len);
	// Round string up to multiple of four bytes but include space for the \0
	else if (Header *h = g_debugDraw->AddCommand(index,handler,0,sizeof(params) + ((len + 4)&~3))) {
		params &p = *(params*)(h+1);
		p.font = g_debugDraw->m_Font;
		p.x = x; p.y = y; p.z = z; p.scale = scale; p.rgba = rgba; p.len = len;
		memcpy(p.payload,string,len);
		p.payload[len] = 0;
	}
}

void DrawStringf(float x,float y,float z,float scale,pfColor rgba,const char *fmt,...) {
	va_list args;
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,fmt,args);
	DrawString(x,y,z,scale,rgba,buf,ustrlen(buf));
	va_end(args);
}

bool DrawLabel(Vec3V_In pos,int xOffset,int yOffset,pfColor rgba,const char *string,unsigned len) {
	Vec3V normPos;
	if (Project(normPos,pos)) {
		DrawString(normPos.GetXf() * GetScreenWidth() + xOffset,normPos.GetYf() * GetScreenHeight() + yOffset,normPos.GetZf(),1.0f,rgba,string,len);
		return true;
	}
	else
		return false;
}

bool DrawLabelf(Vec3V_In pos,pfColor rgba,const char *fmt,...) {
	va_list args;
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,fmt,args);
	va_end(args);
	return DrawLabel(pos,rgba,buf,ustrlen(buf));
}

bool DrawLabelf(Vec3V_In pos,int xOffset,int yOffset,pfColor rgba,const char *fmt,...) {
	va_list args;
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,fmt,args);
	va_end(args);
	return DrawLabel(pos,xOffset,yOffset,rgba,buf,ustrlen(buf));
}

void DrawTaperedCylinder(float height,float radiusBot,float radiusTop,Mat34V_In mtx,pfColor color) {
	// The non-tapered version is a canned primitive which can be rendered more efficiently.
	if (radiusBot == radiusTop) {
		DrawCylinder(height,radiusBot,mtx,color);
		return;
	}

	static HandlerIndex index;
	struct params { float halfHeight, radiusBot, radiusTop; unsigned color; Mat34V mtx; };
	auto handler = [](BatchInterface *b,Header *h) {
		params &p = *(params*)(h+1);
		b->m_RenderInterface->SetWorldMatrix(p.mtx);
		b->m_RenderInterface->SetConstantColor(p.color);
		float *v = (float*) b->m_RenderInterface->Begin(pfLines,h->byteParam*2,pfPos);
		for (int i=0; i < h->byteParam; i++,v+=6) {
			float t = 2 * PI * i / (h->byteParam);
			float x = cosf(t), z = sinf(t);
			v[0] = x * p.radiusBot;
			v[1] = -p.halfHeight;
			v[2] = z * p.radiusBot;
			v[3] = x * p.radiusTop;
			v[4] = +p.halfHeight;
			v[5] = z * p.radiusTop;
		}
		b->m_RenderInterface->End();
	};
	params p = { height*0.5f,radiusBot,radiusTop,color,mtx };
	g_debugDraw->AddCommandAndPayload(index,handler,32,sizeof(p),&p);
}

void DrawSolidTaperedCylinder(float height,float radiusBot,float radiusTop,Mat34V_In mtx,pfColor color) {
	// The non-tapered version is a canned primitive which can be rendered more efficiently.
	if (radiusBot == radiusTop) {
		DrawSolidCylinder(height,radiusBot,mtx,color);
		return;
	}

	// This cylinder is not currently capped since we expect it to be mostly used by capsules.
	static HandlerIndex index;
	struct params { float halfHeight, radiusBot, radiusTop; unsigned color; Mat34V mtx; };
	auto handler = [](BatchInterface *b,Header *h) {
		params &p = *(params*)(h+1);
		b->m_RenderInterface->SetWorldMatrix(p.mtx);
		b->m_RenderInterface->SetConstantColor(p.color);
		struct v3n3 { float x,y,z,nx,ny,nz; };
		v3n3 *v = (v3n3*) b->m_RenderInterface->Begin(pfTriangleStrip,h->byteParam*2+2,pfPosNormal);
		// Note the normals aren't quite right, this assumes there isn't an extreme amount of tapering.
		// This does avoid a discontinuity here where the cylinder meets the sphere cap in a capsule though.
		for (int i=0; i < h->byteParam; i++,v+=2) {
			float t = 2 * PI * i / (h->byteParam);
			float x = cosf(t), z = sinf(t);
			v[0].x = x * p.radiusBot;
			v[0].y = -p.halfHeight;
			v[0].z = z * p.radiusBot;
			v[0].nx = x;
			v[0].ny = 0.0f;
			v[0].nz = z;
			v[1].x = x * p.radiusTop;
			v[1].y = +p.halfHeight;
			v[1].z = z * p.radiusTop;
			v[1].nx = x;
			v[1].ny = 0.0f;
			v[1].nz = z;
		}
		v[0].x = p.radiusBot;
		v[0].y = -p.halfHeight;
		v[0].z = 0;
		v[0].nx = 1.0f;
		v[0].ny = 0.0f;
		v[0].nz = 0.0f;
		v[1].x = p.radiusTop;
		v[1].y = +p.halfHeight;
		v[1].z = 0;
		v[1].nx = 1.0f;
		v[1].ny = 0.0f;
		v[1].nz = 0.0f;
		b->m_RenderInterface->End();
	};
	params p = { height*0.5f,radiusBot,radiusTop,color,mtx };
	g_debugDraw->AddCommandAndPayload(index,handler,32,sizeof(p),&p);
}

void DrawTaperedCapsule(float height,float radiusBot,float radiusTop,Mat34V_In mtx,pfColor color) {
	Mat34V topMtx = mtx, botMtx = mtx;
	DrawTaperedCylinder(height,radiusBot,radiusTop,mtx,color);
	botMtx.SetCol3(AddScaled(botMtx.GetCol3(),mtx.GetCol1(),ScalarV(height*-0.5f)));
	topMtx.SetCol3(AddScaled(topMtx.GetCol3(),mtx.GetCol1(),ScalarV(height*0.5f)));
	DrawSphere(radiusBot,botMtx,color);
	DrawSphere(radiusTop,topMtx,color);
}

void DrawSolidTaperedCapsule(float height,float radiusBot,float radiusTop,Mat34V_In mtx,pfColor color) {
	DrawSolidTaperedCylinder(height,radiusBot,radiusTop,mtx,color);
	Mat34V topMtx = mtx, botMtx = mtx;
	botMtx.SetCol3(AddScaled(botMtx.GetCol3(),mtx.GetCol1(),ScalarV(height*-0.5f)));
	topMtx.SetCol3(AddScaled(topMtx.GetCol3(),mtx.GetCol1(),ScalarV(height*0.5f)));
	DrawSolidSphere(radiusBot,botMtx,color);
	DrawSolidSphere(radiusTop,topMtx,color);
}

void DrawSpiral(Vec3V_In start, Vec3V_In  end, pfColor color, float startRadius, float endRadius, float revolutionsPerMeter, float initialPhase, float arrowLength, int steps) {
	(void)start;
	(void)end;
	(void)color;
	(void)startRadius;
	(void)endRadius;
	(void)revolutionsPerMeter;
	(void)initialPhase;
	(void)arrowLength;
	(void)steps;
}

void DrawArrowOrtho(Vec3V_In vStartPos, Vec3V_In vEndPos, pfColor color, Vec3V_In vCameraPosition, float fArrowheadLength/* =0.8f */, float fArrowheadBaseHalfLength/* =0.2f */) {
#if 1
	(void)vStartPos;
	(void)vEndPos;
	(void)color;
	(void)vCameraPosition;
	(void)fArrowheadLength;
	(void)fArrowheadBaseHalfLength;
#else
	// get camera position
	if (vStartPos != vEndPos) {
		// draw arrowhead orthogonal to the camera
		Vec3V vLineNormal = NormalizeFastSafe(vEndPos - vStartPos);
		
		Vec3V vAdjustedEnd = vEndPos;
		vAdjustedEnd -= fArrowheadLength*vLineNormal;

		Vec3V vBaseLocation = vAdjustedEnd;
		Vec3V vHeadLocation = vBaseLocation + vLineNormal*fArrowheadLength;

		// determine normal for base of arrowhead which is orthogonal to the view direction
		Vec3V vBaseVect;
		vBaseVect = NormalizeFastSafe(Cross( vLineNormal, vCameraPosition - vBaseLocation ));

		Vec3V vBaseEnd1 = vBaseLocation + vBaseVect*fArrowheadBaseHalfLength;
		Vec3V vBaseEnd2 = vBaseLocation - vBaseVect*fArrowheadBaseHalfLength;

		DrawLine( vStartPos, vBaseLocation, Color );	// arrow body
		DrawLine( vBaseEnd1, vBaseEnd2, Color );		// arrowhead base
		DrawLine( vBaseEnd1, vHeadLocation, Color );	// arrowhead base end #1 to point
		DrawLine( vBaseEnd2, vHeadLocation, Color );	// arrowhead base end #2 to point
	}
#endif
}


void PushGpuMarker(const char *PIX_TAGGING_ONLY(tag)) {
#if ENABLE_PIX_TAGGING
	static HandlerIndex index;
	auto handler = [](BatchInterface *,Header *h) {
		PF_PUSH_GPU_MARKER((const char*)(h+1));
	};
	g_debugDraw->AddCommandAndPayload(index,handler,0,strlen(tag)+1,tag);
#endif
}

void PopGpuMarker() {
#if ENABLE_PIX_TAGGING
	static HandlerIndex index;
	auto handler = [](BatchInterface*,Header*) {
		PF_POP_GPU_MARKER();
	};
	g_debugDraw->AddCommand(index,handler,0,0);
#endif
}

}	// debugDraw

}	// rage

#endif	// DevStudio 2012 or better

#endif	// RSG_PROFILE_RENDERER
