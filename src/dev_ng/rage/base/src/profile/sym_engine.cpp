#include "settings.h"

#if USE_PROFILER

#include "sym_engine.h"
#include "file/handle.h"
#include "math/amath.h"

#if RSG_SCE
#include <kernel.h>
#include <sceerror.h>
#include <libdbg.h>
#endif

#if (RSG_ORBIS && defined(ORBIS_SDK_VERSION) && ORBIS_SDK_VERSION>1700)
#define SCE_SUPPORTS_DEBUG_MODULES 1
#else
#define SCE_SUPPORTS_DEBUG_MODULES 0
#endif



#if RSG_DURANGO
#include <system/xtl.h>
//#include <psapi.h>
//#include <processthreadsapi.h>
#pragma pack(push,8)
typedef struct _MODULEINFO {
	LPVOID lpBaseOfDll;
	DWORD SizeOfImage;
	LPVOID EntryPoint;
} MODULEINFO, *LPMODULEINFO;
#pragma pack(pop)
#define EnumProcessModulesEx        K32EnumProcessModulesEx
#define GetModuleInformation        K32GetModuleInformation
#define GetModuleFileNameExA        K32GetModuleFileNameExA
extern "C" DWORD WINAPI K32EnumProcessModulesEx(HANDLE hProcess, HMODULE *lphModule, DWORD cb, LPDWORD lpcbNeeded, DWORD dwFilterFlag);
extern "C" DWORD WINAPI K32GetModuleInformation(HANDLE hProcess, HMODULE hModule, LPMODULEINFO lpmodinfo, DWORD cb);
extern "C" DWORD WINAPI K32GetModuleFileNameExA(HANDLE hProcess, HMODULE hModule, LPSTR lpFilename, DWORD nSize);
extern "C" WINBASEAPI HANDLE WINAPI OpenProcess(DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwProcessId);
#pragma comment(lib,"toolhelpx.lib")
#endif //RSG_DURANGO

#if RSG_PC
#include <psapi.h>
#endif

#if ROCKY_USE_DBG_HELP
#define DBGHELP_TRANSLATE_TCHAR 1
#include "system/dbghelp.h"
#pragma comment( lib, "DbgHelp.Lib" )
#endif


#include "system/stack.h"
#include "system/nelem.h"


using namespace rage;

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void ReportLastError()
//{
//	LPVOID lpMsgBuf;
//	DWORD dw = GetLastError();
//
//	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
//								NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
//								(LPTSTR)&lpMsgBuf, 0, NULL);
//
//	MessageBox(NULL, (LPCTSTR)lpMsgBuf, TEXT("Error"), MB_OK);
//	LocalFree(lpMsgBuf);
//}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SymEngine::SymEngine() : isInitialized(false)
#if ROCKY_USE_DBG_HELP
	, hProcess(GetCurrentProcess()), needRestorePreviousSettings(false), previousOptions(0)
#endif
{
	USE_ROCKY_MEMORY;
	cache = rage_new SymbolCache();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SymEngine::~SymEngine()
{
	Close();

	USE_ROCKY_MEMORY;

	Clear();

	delete cache;
	cache = nullptr;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SymEngine::Clear()
{
	if (cache)
	{
		USE_ROCKY_MEMORY;
		for (SymbolCache::Iterator it = cache->CreateIterator(); !it.AtEnd(); it.Next())
			delete it.GetData();

		cache->Reset();
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Symbol* SymEngine::GetOrCreateSymbol(rage::u64 dwAddress)
{
	USE_ROCKY_MEMORY;

	Symbol* symbol = nullptr;

	if (Symbol** target = cache->Access(dwAddress))
	{
		symbol = *target;
	}
	else
	{
		symbol = rage_new Symbol();
		cache->Insert(dwAddress, symbol);
	}

	return symbol;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const Symbol* SymEngine::ResolveSymbol(rage::u64 dwAddress)
{
	if (dwAddress == 0)
		return nullptr;

	USE_ROCKY_MEMORY;

	Symbol* symbol = GetOrCreateSymbol(dwAddress);

	if (symbol->address != 0)
		return symbol;

	if (!isInitialized)
		return nullptr;

	symbol->address = dwAddress;

#if ROCKY_USE_DBG_HELP
	// Module Name
	IMAGEHLP_MODULEW64 moduleInfo;
	memset(&moduleInfo, 0, sizeof(IMAGEHLP_MODULEW64));
	moduleInfo.SizeOfStruct = sizeof(moduleInfo);
	if (SymGetModuleInfoW64(hProcess, dwAddress, &moduleInfo))
	{
		symbol->module = moduleInfo.ImageName;
	}


	// FileName and Line
	IMAGEHLP_LINEW64 lineInfo;
	memset(&lineInfo, 0, sizeof(IMAGEHLP_LINEW64));
	lineInfo.SizeOfStruct = sizeof(lineInfo);
	DWORD dwDisp;
	if (SymGetLineFromAddrW64(hProcess, dwAddress, &dwDisp, &lineInfo))
	{
		symbol->file = lineInfo.FileName;
		symbol->line = lineInfo.LineNumber;
	}

	const size_t length = (sizeof(SYMBOL_INFOW) + MAX_SYM_NAME*sizeof(WCHAR) + sizeof(ULONG64) - 1) / sizeof(ULONG64) + 1;

	// Function Name
	ULONG64 buffer[length];
	PSYMBOL_INFOW dbgSymbol = (PSYMBOL_INFOW)buffer;
	memset(dbgSymbol, 0, sizeof(buffer));
	dbgSymbol->SizeOfStruct = sizeof(SYMBOL_INFOW);
	dbgSymbol->MaxNameLen = MAX_SYM_NAME;
	if (SymFromAddrW(hProcess, dwAddress, &symbol->offset, dbgSymbol))
	{
		symbol->function.resize(dbgSymbol->NameLen);
		memcpy(&symbol->function[0], &dbgSymbol->Name[0], sizeof(WCHAR) * dbgSymbol->NameLen);
	}
#else
	char nameBuff[256];
	rage::sysStack::ParseMapFileSymbol(nameBuff, NELEM(nameBuff), symbol->address, true);

	rage::stlString name = nameBuff;
	symbol->function = rage::stlWstring(name.begin(), name.end());

	if (const Module* module = GetModule(symbol->address))
	{
		symbol->module = module->name;
		symbol->offset = symbol->address - (rage::u64)module->baseAddress;
	}
#endif

	return symbol;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// const char* USER_SYMBOL_SEARCH_PATH = "http://msdl.microsoft.com/download/symbols";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if ROCKY_USE_DBG_HELP
BOOL CALLBACK EnumModules( LPCWSTR moduleName, DWORD64 baseOfDll, PVOID /*userContext*/ )
{
	HMODULE hMod;
	BOOL res = GetModuleHandleExW(GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, moduleName, &hMod);

	if(res != 0)
	{
		IMAGEHLP_MODULEW64 info;
		info.SizeOfStruct = sizeof(info);
		res = SymGetModuleInfoW64(GetCurrentProcess(), baseOfDll, &info);

		if(res != 0)
		{
			SymLoadModuleExW(GetCurrentProcess(), NULL, info.ImageName, info.ModuleName, info.BaseOfImage, info.ImageSize, NULL, 0);
		}
	}

	return TRUE;
}
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SymEngine::Init()
{
	if (!isInitialized)
	{
#if (ROCKY_USE_DBG_HELP)
		previousOptions = SymGetOptions();

		memset(previousSearchPath, 0, MAX_SEARCH_PATH_LENGTH);
		SymGetSearchPathW(hProcess, previousSearchPath, MAX_SEARCH_PATH_LENGTH);

		//SymSetOptions(SymGetOptions() | SYMOPT_LOAD_LINES | SYMOPT_DEFERRED_LOADS | SYMOPT_UNDNAME | SYMOPT_INCLUDE_32BIT_MODULES | SYMOPT_LOAD_ANYTHING);
		SymSetOptions(SymGetOptions() | SYMOPT_LOAD_LINES);
		if (!SymInitialize(hProcess, NULL, TRUE))
		{
			needRestorePreviousSettings = true;
			SymCleanup(hProcess);

			if (SymInitialize(hProcess, NULL, TRUE))
				isInitialized = true;
		}
		else
		{
			isInitialized = true;
		}

		if (isInitialized)
		{
			SymRefreshModuleList(hProcess);
			SymEnumerateModulesW64(hProcess, EnumModules, nullptr);
		}
#endif

#if RSG_SCE
		SceKernelModule modules[256];
		size_t numLoaded = 0;
	#if SCE_ORBIS_SDK_VERSION >= 0x05500000u
		sceDbgGetModuleList(modules, NELEM(modules), &numLoaded);
	#else
		sceKernelGetModuleList(modules, NELEM(modules), &numLoaded);
	#endif
		loadedModules.Resize(numLoaded);
		for(size_t i=0; i<numLoaded; ++i)
		{
			Module& module = loadedModules[i];
#if SCE_SUPPORTS_DEBUG_MODULES
			SceDbgModuleInfo info;
			info.size = sizeof(info);

			if(sceDbgGetModuleInfo(modules[i], &info) == SCE_OK)
			{
				rage::stlString name = info.name;
				module.name = rage::stlWstring(name.begin(), name.end());
				module.baseAddress = info.segmentInfo[0].baseAddr;
				module.size = info.segmentInfo[0].size;
			}
#else
			module.name = L"UnknownModule";
#endif

		}

#elif (RSG_DURANGO || RSG_PC)
		HANDLE processHandle = GetCurrentProcess();
		HMODULE modules[256];
		DWORD modulesSize = 0;
		EnumProcessModulesEx(processHandle, modules, sizeof(modules), &modulesSize, 0);

		int moduleCount = modulesSize / sizeof(HMODULE);
		loadedModules.resize(moduleCount);

		for (int i = 0; i < moduleCount; ++i)
		{
			MODULEINFO info = { 0 };
			if (GetModuleInformation(processHandle, modules[i], &info, sizeof(MODULEINFO)))
			{
				char name[MAX_PATH] = "UnknownModule";
				GetModuleFileNameExA(processHandle, modules[i], name, MAX_PATH);

				Module& module = loadedModules[i];
				rage::stlString stringName = name;
				module.name = rage::stlWstring(stringName.begin(), stringName.end());
				module.baseAddress = info.lpBaseOfDll;
				module.size = info.SizeOfImage;
			}
		}
#endif

		rage::sysStack::OpenSymbolFile();
		isInitialized = true;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SymEngine::Close()
{
#if ROCKY_USE_DBG_HELP
	if (isInitialized)
	{
		SymCleanup(hProcess);
		isInitialized = false;
	}

	if (needRestorePreviousSettings)
	{
		HANDLE currentProcess = GetCurrentProcess();

		SymSetOptions(previousOptions);
		SymSetSearchPathW(currentProcess, previousSearchPath);
		SymInitialize(currentProcess, NULL, TRUE);

		needRestorePreviousSettings = false;
	}
#endif

	rage::sysStack::CloseSymbolFile();
	isInitialized = false;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const Module* SymEngine::GetModule(rage::u64 dwAddress)
{
	for (int i = 0; i < loadedModules.GetCount(); ++i)
	{
		if ((rage::u64)loadedModules[i].baseAddress <= dwAddress && dwAddress < ((rage::u64)loadedModules[i].baseAddress + loadedModules[i].size))
			return &loadedModules[i];
	}

	return nullptr;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const rage::atVector<Profiler::Module>& SymEngine::GetModules() const
{
	return loadedModules;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool SymEngine::GetProcesses(ProcessList& out) const
{
#if (RSG_PC)
	DWORD processPIDs[1024] = { 0 };
	DWORD processSize = 0;

	EnumProcesses(processPIDs, sizeof(processPIDs), &processSize);
	int processCount = processSize / sizeof(DWORD);

	out.Reserve(processCount);

	for (int i = 0; i < processCount; ++i)
	{
		if (processPIDs[i])
		{
			HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processPIDs[i]);
			if (hProcess != NULL)
			{
				ProcessInfo& info = out.Append();

				HMODULE hMod;
				DWORD cbNeeded;

				char name[256] = "<Empty>";
				if (EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
				{
					GetModuleBaseNameA(hProcess, hMod, name, sizeof(name));
				}
				info.m_Name = name;

				PROCESS_MEMORY_COUNTERS oMemoryInfo = { 0 };
				GetProcessMemoryInfo(hProcess, &oMemoryInfo, sizeof(oMemoryInfo));
				info.m_WorkingSetSize = oMemoryInfo.WorkingSetSize;
				info.m_CommitSize = oMemoryInfo.PagefileUsage;

				info.m_PID = processPIDs[i];
			}
			CloseHandle(hProcess);
		}
	}

	return true;
#else
	(void)out;
	return false;
#endif
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if ROCKY_USE_DBG_HELP
int SymEngine::GetCallstack(volatile const HANDLE& hThread, CONTEXT& context, CallStackBuffer& callstack) 
{
	// We can't initialize dbghelp.dll here => http://microsoft.public.windbg.narkive.com/G2WkSt2k/stackwalk64-performance-problems
	// Otherwise it will be 5x times slower
	// Init();

	STACKFRAME64 stackFrame;
	memset(&stackFrame, 0, sizeof(STACKFRAME64));
	DWORD machineType;

	stackFrame.AddrPC.Mode = AddrModeFlat;
	stackFrame.AddrFrame.Mode = AddrModeFlat;
	stackFrame.AddrStack.Mode = AddrModeFlat;

#ifdef _M_IX86
	machineType = IMAGE_FILE_MACHINE_I386;
	stackFrame.AddrPC.Offset = context.Eip;
	stackFrame.AddrFrame.Offset = context.Ebp;
	stackFrame.AddrStack.Offset = context.Esp;
#elif _M_X64
	machineType = IMAGE_FILE_MACHINE_AMD64;
	stackFrame.AddrPC.Offset = context.Rip;
	stackFrame.AddrFrame.Offset = context.Rsp;
	stackFrame.AddrStack.Offset = context.Rsp;
#elif _M_IA64
	machineType = IMAGE_FILE_MACHINE_IA64;
	stackFrame.AddrPC.Offset = context.StIIP;
	stackFrame.AddrFrame.Offset = context.IntSp;
	stackFrame.AddrStack.Offset = context.IntSp;
	stackFrame.AddrBStore.Offset = context.RsBSP;
	stackFrame.AddrBStore.Mode = AddrModeFlat;
#else
#error "Platform not supported!"
#endif

	static const rage::u32 CALLSTACK_BUFFER_SIZE = 64;
	rage::u64 buffer[CALLSTACK_BUFFER_SIZE];

	rage::u32 count = 0;

	buffer[count++] = stackFrame.AddrPC.Offset;

	while (fiIsValidHandle(hThread) && StackWalk64(machineType, GetCurrentProcess(), hThread, &stackFrame, &context, nullptr, SymFunctionTableAccess64, SymGetModuleBase64, nullptr) )
	{
		DWORD64 dwAddress = stackFrame.AddrPC.Offset;
		if (!dwAddress)
			break;

		if (count == CALLSTACK_BUFFER_SIZE)
			return 0; // Too long callstack - possible error, let's skip it

		if (buffer[count - 1] == dwAddress)
			continue;

		buffer[count++] = dwAddress;
	}

	rage::u32 trimmedCount = Min(MAX_CALLSTACK_DEPTH, count);

	callstack.Clear();
	sysMemCpy(callstack.data, &buffer[count - trimmedCount], sizeof(rage::u64) * trimmedCount);

	return fiIsValidHandle(hThread) ? trimmedCount : 0;
}
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CallStackBuffer::Clear()
{
	sysMemSet(data, 0, sizeof(rage::u64) * MAX_CALLSTACK_DEPTH);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Profiler::OutputDataStream& operator<<(OutputDataStream& stream, const Module& ob)
{
	return stream << ob.name.c_str() << (rage::u64)ob.baseAddress << (rage::u64)ob.size;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER
