#ifndef PROFILER_COMMON_H
#define PROFILER_COMMON_H

#if USE_PROFILER

#include "types.h"
#include "diag/channel.h"
#include "rocky_memory.h"

// MEMORY //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define ROCKY_CACHE_LINE_SIZE 64
#define ROCKY_ALIGN_CACHE ALIGNAS(ROCKY_CACHE_LINE_SIZE)
#define ROCKY_INLINE __forceinline

#if RSG_ORBIS
#define ROCKY_NOINLINE __attribute__ ((__noinline__))
#else
#define ROCKY_NOINLINE __declspec(noinline)
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// LOGGING /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RAGE_DECLARE_CHANNEL(Rocky);

#define rockyAssert(cond)							RAGE_ASSERT(Rocky,cond)
#define rockyAssertf(cond,fmt,...)					RAGE_ASSERTF(Rocky,cond,fmt,##__VA_ARGS__)
#define rockyAssert2(cond)							RAGE_ASSERT2(Rocky,cond)
#define rockyAssertf2(cond,fmt,...)					RAGE_ASSERTF2(Rocky,cond,fmt,##__VA_ARGS__)
#define rockyAssert3(cond)							RAGE_ASSERT3(Rocky,cond)
#define rockyAssertf3(cond,fmt,...)					RAGE_ASSERTF3(Rocky,cond,fmt,##__VA_ARGS__)
#define rockyFatalAssertf(cond,fmt,...)				RAGE_FATALASSERTF(Rocky,cond,fmt,##__VA_ARGS__)
#define rockyVerify(cond)							RAGE_VERIFY(Rocky,cond)
#define rockyVerifyf(cond,fmt,...)					RAGE_VERIFYF(Rocky,cond,fmt,##__VA_ARGS__)
#define rockyVerify2(cond)							RAGE_VERIFY2(Rocky,cond)
#define rockyVerifyf2(cond,fmt,...)					RAGE_VERIFYF2(Rocky,cond,fmt,##__VA_ARGS__)
#define rockyVerify3(cond)							RAGE_VERIFY3(Rocky,cond)
#define rockyVerifyf3(cond,fmt,...)					RAGE_VERIFYF3(Rocky,cond,fmt,##__VA_ARGS__)
#define rockyErrorf(fmt,...)						RAGE_ERRORF(Rocky,fmt,##__VA_ARGS__)
#define rockyWarningf(fmt,...)						RAGE_WARNINGF(Rocky,fmt,##__VA_ARGS__)
#define rockyDisplayf(fmt,...)						RAGE_DISPLAYF(Rocky,fmt,##__VA_ARGS__)


#define ROCKY_UNUSED(arg) {arg;}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif

#endif //PROFILER_COMMON_H
