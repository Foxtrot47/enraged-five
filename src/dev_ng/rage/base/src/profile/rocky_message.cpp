#include "settings.h"

#if USE_PROFILER

#include "common.h"
#include "core.h"
#include "event.h"
#include "rocky_message.h"
#include "profiler_server.h"
#include "event_description_board.h"

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void StartMessage::Apply()
{
	rage::u32 mode = HasSamplingEvents() ? Mode::SAMPLING : Mode::DEFAULT;
	Core::Get().Activate(mode);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* StartMessage::Create(InputDataStream&)
{
	USE_ROCKY_MEMORY;
	return rage_new StartMessage();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void StartModeMessage::Apply()
{
	Core::Get().Activate(HasSamplingEvents() ? Mode::SAMPLING : mode);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* StartModeMessage::Create(InputDataStream& stream)
{
	USE_ROCKY_MEMORY;
	StartModeMessage* msg = rage_new StartModeMessage();
	stream >> msg->mode;
	return msg;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void StartModeExtMessage::Apply()
{
	Core::Get().SetSamplingFrequency(samplingFrequency);
	Core::Get().SetScriptSamplingFrequency(scriptSamplingFrequency);
	Core::Get().Activate(HasSamplingEvents() ? Mode::SAMPLING : mode);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* StartModeExtMessage::Create(InputDataStream& stream)
{
	USE_ROCKY_MEMORY;
	StartModeExtMessage* msg = rage_new StartModeExtMessage();
	stream >> msg->mode;
	stream >> msg->samplingFrequency;
	stream >> msg->scriptSamplingFrequency;
	return msg;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void StopMessage::Apply()
{
	Core& core = Core::Get();

	if (core.GetMode() != Mode::OFF)
	{
		rage::u32 mode = core.Activate(Mode::OFF);
		core.RequestDump(mode & ~Mode::LIVE); // Strip LIVE parameter from the mode
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* StopMessage::Create(InputDataStream&)
{
	USE_ROCKY_MEMORY;
	return rage_new StopMessage();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* TurnSamplingMessage::Create( InputDataStream& stream )
{
	USE_ROCKY_MEMORY;
	TurnSamplingMessage* msg = rage_new TurnSamplingMessage();
	stream >> msg->index;
	stream >> msg->isSampling;
	return msg;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void TurnSamplingMessage::Apply()
{
	SetEventFlag(index, EventDescription::IS_SAMPLING, isSampling != 0);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* TurnLiveMessage::Create(InputDataStream& stream)
{
	USE_ROCKY_MEMORY;
	TurnLiveMessage* msg = rage_new TurnLiveMessage();
	stream >> msg->index;
	stream >> msg->isLive;
	return msg;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void TurnLiveMessage::Apply()
{
	SetEventFlag(index, EventDescription::IS_LIVE, isLive != 0);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* CancelMessage::Create(InputDataStream&)
{
	USE_ROCKY_MEMORY;
	return rage_new CancelMessage();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CancelMessage::Apply()
{
	Core& core = Core::Get();
	core.Activate(Mode::OFF);
	core.RequestDump(Mode::OFF);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* SetMaxCaptureFrameLimitMessage::Create(InputDataStream& stream)
{
	USE_ROCKY_MEMORY;
	SetMaxCaptureFrameLimitMessage* msg = rage_new SetMaxCaptureFrameLimitMessage();
	stream >> msg->frameLimit;
	return msg;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SetMaxCaptureFrameLimitMessage::Apply()
{
	Core::Get().SetMaxCaptureFrameLimit(frameLimit);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMessage* SetRootPasswordMessage::Create(InputDataStream& stream)
{
	USE_ROCKY_MEMORY;
	SetRootPasswordMessage* msg = rage_new SetRootPasswordMessage();
	stream >> msg->platform;
	stream.ReadString(msg->password);
	return msg;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SetRootPasswordMessage::Apply()
{
	Core::Get().SetRootPassword(platform, password);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RockyApp::RegisterMessages()
{
	static bool init = false;
	if (!init)
	{
		init = true;
		USE_ROCKY_MEMORY;
		REGISTER_ROCKY_MESSAGE(StartMessage);
		REGISTER_ROCKY_MESSAGE(StopMessage);
		REGISTER_ROCKY_MESSAGE(TurnSamplingMessage);
		REGISTER_ROCKY_MESSAGE(StartModeMessage);
		REGISTER_ROCKY_MESSAGE(StartModeExtMessage);
		REGISTER_ROCKY_MESSAGE(TurnLiveMessage);
		REGISTER_ROCKY_MESSAGE(CancelMessage);
		REGISTER_ROCKY_MESSAGE(SetMaxCaptureFrameLimitMessage);
		REGISTER_ROCKY_MESSAGE(SetRootPasswordMessage);
		rockyFatalAssertf(MessageFactory::Get().GetCount() == RockyMessage::COUNT, "Don't forget to register the message in the factory!");
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER
