#include "trace.h"
#if RAGETRACE
#include "system/timer.h"
#include "vector/colors.h"
#include "cellschedulertrace.h"
#include "cputrace.h"
#include "grcore/device.h"
#include "grcore/font.h"
#include "grcore/viewport.h"
#include "grcore/im.h"
#include <algorithm>
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "diag/tracker.h"
#include "system/param.h"
#if __PPU
#include "cellspurstrace.h"
#include "gcmtrace.h"
#endif
#include "atl/map.h"
#include "string/stringhash.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "file/stream.h"

namespace rage {

// helper for batching up draw operations
class pfVertexBuffer
{
public:
	pfVertexBuffer();
	~pfVertexBuffer();

	void	Begin();
	void	End();
	void	Quad(float x1, float y1, float x2, float y2, Color32 color);
	void	Vertex(float x, float y, Color32 color);

private:
	void	Flush();

	struct pfVtx
	{
		float x, y, z;
		//u32 color;
		Color32 color;
	};
	pfVtx*					m_VtxBegin;
	pfVtx*					m_VtxEnd;
	pfVtx*					m_Vtx;
//	grcVertexDeclaration*	m_VtxDcl;
};

pfVertexBuffer::pfVertexBuffer()
{
	/* grcVertexElement e[2];
	e[0].type = grcVertexElement::grcvetPosition;
	e[0].size = 12;
	e[0].format = grcFvf::grcdsFloat3;
	e[1].type = grcVertexElement::grcvetColor;
	e[1].size = 4;
	e[1].format = grcFvf::grcdsColor;
	m_VtxDcl = GRCDEVICE.CreateVertexDeclaration(e,2); */

	u32 verts = GRCDEVICE.GetVerticesMaximumBatchSize(drawQuads, sizeof(pfVtx));
	verts &= ~3;
	m_VtxBegin = rage_new pfVtx[verts];
	m_VtxEnd = m_VtxBegin + verts;
}

pfVertexBuffer::~pfVertexBuffer()
{
	delete [] m_VtxBegin;
	// m_VtxDcl->Release();
}

void pfVertexBuffer::Begin()
{
	m_Vtx = m_VtxBegin;
}

void pfVertexBuffer::End()
{
	Flush();
}

void pfVertexBuffer::Flush()
{
	if (m_Vtx != m_VtxBegin)
	{
#if 1
		grcBegin(drawQuads, (int)(m_Vtx - m_VtxBegin));
		for(pfVtx* vtx = m_VtxBegin; vtx != m_Vtx; ++vtx)
			grcVertex(vtx->x, vtx->y, vtx->z, 0.0f, 0.0f, 0.0f, vtx->color, 0.0f, 0.0f);
		grcEnd();
#else
		GRCDEVICE.SetVertexDeclaration(m_VtxDcl);
		void* verts = GRCDEVICE.BeginVertices(drawQuads, m_Vtx - m_VtxBegin, sizeof(pfVtx));
		if ( verts)
		{
			size_t size = (size_t) (((u8*)m_Vtx - (u8*)m_VtxBegin);
			memcpy(verts, m_VtxBegin, size);
			GRCDEVICE.EndVertices(((char *) verts) + size);
		}
#endif
		m_Vtx = m_VtxBegin;
	}
}

void pfVertexBuffer::Vertex(float x, float y, Color32 color)
{
	m_Vtx->x = x; m_Vtx->y = y; m_Vtx->z = 0;
	m_Vtx->color = color; //.GetDeviceColor();
	if (++m_Vtx == m_VtxEnd)
		Flush();
}

void pfVertexBuffer::Quad(float x1, float y1, float x2, float y2, Color32 color)
{
	Vertex(x1, y1, color);
	Vertex(x1, y2, color);
	Vertex(x2, y2, color);
	Vertex(x2, y1, color);
}

//////////////////////////////////////////////////////////////////////////

PARAM(trace,		"[profile] Enable profile trace");
#if __PPU
PARAM(traceidle,	"[profile] Enable CELL PPU scheduler trace to monitor thread idle time (slows down update considerably!)");
PARAM(tracespurs,	"[profile] Enable CELL SPURS trace");
PARAM(tracegcm,		"[profile] Enable GCM trace");

static uint32_t s_IdFromName(const char *taskName)
{
	return (uint32_t) &RageTrace().IdFromName(taskName);
}

extern uint32_t (*g_pIdFromName)(const char*);
#endif

pfTraceManager::pfTraceManager()
:	m_Initialised(false)
,	m_Enabled(false)
,	m_Traces(0, 32)
,	m_Inits(0, 32)
,	m_VtxBuf(0)
{
	m_FsQPosWrite = 0;
	m_FsQPosRead = 5;
	m_SampleFrames = 4;
	m_ShowWholeRun = false;
	m_ShowLegend = true;
	m_ShowTopCounters = 10;
	m_ActiveTopCounter = 0;

#if __PPU
	g_pIdFromName = s_IdFromName;
	printf("[STARTUP] Patched in RAGETRACE support to SPU task system\n");
#endif
}

//static fiHandle s_StatsFile;
static float m_YPos;

void pfTraceManager::Init()
{
	USE_DEBUG_MEMORY();

	Assert(!m_Initialised);
	m_Initialised = true;
#if __BANK
	m_Bank = &BANKMGR.RegisterBank("rage - Profile Trace",
		datCallback(MFA1(pfTraceManager::AddWidgets), this, 0, true));
#endif
	m_VtxBuf = rage_new pfVertexBuffer;
	m_NumFrames = 8;
	m_Scale = sysTimer::GetTicksToSeconds() * (GRCDEVICE.GetWidth() - 350) * 60.0f / m_NumFrames;
	m_ScaleTimeStart = (u64)sysTimer::GetTicks();

	m_InitCountDown = 15;
	m_EnableToggle = PARAM_trace.Get();
#if __PPU
	// cpu thread traces require a cell scheduler trace to determine idle time on psn
	// NOTE: this slows down the update considerably
	m_EnableCellScheduler = PARAM_traceidle.Get();
	m_EnableSpursTrace = PARAM_tracespurs.Get();
	m_EnableGcmTrace = PARAM_tracegcm.Get();
	// turn on trace if either cell trace enabled
	m_EnableToggle |= m_EnableCellScheduler | m_EnableSpursTrace | m_EnableGcmTrace;
#endif		

//	s_StatsFile = fiDevice::Open("tracestats.csv", false);
// 	if (!s_StatsFile)
// 		s_StatsFile = fiDevice::Create("tracestats.csv");
// 	fseek(s_StatsFile, 0, SEEK_END);
}

void pfTraceManager::Shutdown()
{
	delete m_VtxBuf;
}

void pfTraceManager::Reset()
{
	m_InitCountDown = 2;
}

#if __BANK
void pfTraceManager::AddWidgets(bkBank& bank)
{
	bank.AddToggle("Profiler trace - enabled", &m_EnableToggle);
	bank.AddToggle("CELL Scheduler trace (SLOW) - enabled", &m_EnableCellScheduler);
	bank.AddToggle("SPURS trace - enabled", &m_EnableSpursTrace);
	bank.AddToggle("GCM trace - enabled", &m_EnableGcmTrace);

	bank.AddToggle("Show hierarchy/legend", &m_ShowLegend);
	bank.AddSlider("Show top counters", &m_ShowTopCounters, 1, 100, 1);
	bank.AddToggle("Show whole run average", &m_ShowWholeRun);
	bank.AddSlider("Sample interval", &m_SampleFrames, 1, 100, 1);
	bank.AddButton("Reset stats", datCallback(MFA(pfTraceManager::Reset), this));

	bank.AddButton("Write to file", datCallback(MFA(pfTraceManager::WriteToFile), this));	
}
#endif

static int SortCounters(pfTraceCounterId* const* lhs, pfTraceCounterId* const* rhs)
{
	// don't put transparent bars at the top of the list
	int alpha = (*lhs)->m_Color.GetAlpha() - (*rhs)->m_Color.GetAlpha();
	if (alpha < 0)
		return 1;
	else if (alpha > 0)
		return -1;
	
	double diff = (*lhs)->m_Counter.FrameTime() - (*rhs)->m_Counter.FrameTime();
	if (diff < 0)
		return 1;
	else if (diff > 0)
		return -1;
	return 0;
}

void pfTraceManager::WriteToFile()
{
	sysCriticalSection cs(m_Cs);
	fiStream* fh = fiStream::Open("x:\\tracestats.csv");
	if (!fh)
		fh = fiStream::Create("x:\\tracestats.csv");
	if (fh)
	{		
		fseek(fh, 0, SEEK_END);
		
		m_Counters.QSort(0, m_Counters.size(), &SortCounters);

		fprintf(fh, "Name, Mean time per frame, Std err, Lower95, Upper95, "
					"Mean calls per frame, Std err, Lower95, Upper95\n");
		for(atArray<pfTraceCounterId*>::iterator it = m_Counters.begin(); it != m_Counters.end(); ++it)
		{
			pfTraceCounterId& counter = **it;
			fprintf(fh, "%s, %f, %f, %f, %f, %f, %f, %f, %f\n", counter.m_Name,
				counter.m_Counter.m_TotalFrameTime.Mean(), 
				counter.m_Counter.m_TotalFrameTime.StandardDeviation(),
				counter.m_Counter.m_TotalFrameTime.Lower95thPercentile(),
				counter.m_Counter.m_TotalFrameTime.Upper95thPercentile(),
				counter.m_Counter.m_TotalFrameCalls.Mean(), 
				counter.m_Counter.m_TotalFrameCalls.StandardDeviation(),
				counter.m_Counter.m_TotalFrameCalls.Lower95thPercentile(),
				counter.m_Counter.m_TotalFrameCalls.Upper95thPercentile());
		}
		fprintf(fh, "\n\n");

		for(atArray<pfTrace*>::iterator it = m_Traces.begin(); it != m_Traces.end(); ++it)
		{
			pfTrace& trace= **it;
			trace.WriteToFile(fh);
		}

		fh->Close();
	}
}

void pfTraceManager::AddTrace(pfTrace* trace)
{
	m_Traces.Push(trace);
}

void pfTraceManager::RemoveTrace(pfTrace* trace)
{
	m_Traces.DeleteMatches(trace);
}

void pfTraceManager::AddInitialiser(pfTraceInitialiser* init)
{
	m_Inits.Push(init);
}
void pfTraceManager::RemoveInitialiser(pfTraceInitialiser* init)
{
	m_Inits.DeleteMatches(init);
}

void pfTraceManager::AddCounter(pfTraceCounterId* counter)
{
	sysCriticalSection cs(m_Cs);
	m_Counters.PushAndGrow(counter);
}

void pfTraceManager::RemoveCounter(pfTraceCounterId* counter)
{
	sysCriticalSection cs(m_Cs);
	m_Counters.DeleteMatches(counter);	
}

void pfTraceManager::SetFrameStart()
{
	m_FsQ[m_FsQPosWrite] = (u32)sysTimer::GetTicks();
	m_FsQPosWrite = (m_FsQPosWrite + 1) & 7;
}

void pfTraceManager::Update()
{
	if (m_Enabled && !m_EnableToggle)
	{
		m_Enabled = false;
#if __PPU 
		// these traces can slow the game down so get rid
		delete g_pfCellSchedulerTrace;
		g_pfCellSchedulerTrace = 0;
		for(int i=0; i<6; ++i)
		{
			delete g_pfSpuTrace[i];
			g_pfSpuTrace[i] = 0;
		}
		delete g_pfSpuTotals;
		g_pfSpuTotals = 0;
		delete g_pfGcmTrace;
		g_pfGcmTrace = 0;
#endif
		for(int i=0; i<m_Inits.GetCount(); ++i)
			m_Inits[i]->Disable();
	}
	else if (!m_Enabled && m_EnableToggle)
	{
		m_Enabled = true;
		for(int i=0; i<m_Inits.GetCount(); ++i)
			m_Inits[i]->Enable();
	}

	if (!m_Enabled)
		return;

#if __PPU
	// toggle cell scheduler
	if (m_EnableCellScheduler && !g_pfCellSchedulerTrace)
	{
		m_Traces.Insert(0) = g_pfCellSchedulerTrace = rage_new pfCellSchedulerTrace;
	}
	else if (!m_EnableCellScheduler && g_pfCellSchedulerTrace)
	{
		delete g_pfCellSchedulerTrace;
		g_pfCellSchedulerTrace = 0;
	}
	// toggle spurs trace
	if (m_EnableSpursTrace && !g_pfSpuTrace[0])
	{
		AddTrace(g_pfSpuTrace[0] = rage_new pfSpuTrace("SPU0", 256*1024));
		AddTrace(g_pfSpuTrace[1] = rage_new pfSpuTrace("SPU1", 256*1024));
		AddTrace(g_pfSpuTrace[2] = rage_new pfSpuTrace("SPU2", 256*1024));
		AddTrace(g_pfSpuTrace[3] = rage_new pfSpuTrace("SPU3", 256*1024));
		AddTrace(g_pfSpuTrace[4] = rage_new pfSpuTrace("SPU4", 256*1024));
		AddTrace(g_pfSpuTrace[5] = rage_new pfSpuTrace("SPU5", 256*1024));
		AddTrace(g_pfSpuTotals = rage_new pfSpuTotalTrace("SPU Total"));
	}
	else if (!m_EnableSpursTrace && g_pfSpuTrace[0])
	{
		for(int i=0; i<6; ++i)
		{
			delete g_pfSpuTrace[i];
			g_pfSpuTrace[i] = 0;
		}
		delete g_pfSpuTotals;
		g_pfSpuTotals = 0;
	}
	// toggle gcm trace
	if (m_EnableGcmTrace && !g_pfGcmTrace)
	{
		AddTrace(g_pfGcmTrace = rage_new pfGcmTrace);
	}
	else if (!m_EnableGcmTrace && g_pfGcmTrace)
	{
		delete g_pfGcmTrace;
		g_pfGcmTrace = 0;
	}
#endif

	PUSH_DEFAULT_SCREEN();

	m_FrameStart = m_FrameEnd;
	m_FrameEnd = m_FsQ[m_FsQPosRead];
	m_FsQPosRead = (m_FsQPosRead + 1) & 7;
	
	if (!(++m_FrameCount & 31))
	{
		u64 t = (u64)sysTimer::GetTicks();
		u64 elapsed = t - m_ScaleTimeStart;
		m_NumFrames = (u32)ceilf(elapsed * sysTimer::GetTicksToSeconds() * 30.0f / 32 - .1f) * 2;
		m_NumFrames = Clamp(m_NumFrames, 2u, 16u);
		m_Scale = sysTimer::GetTicksToSeconds() * (GRCDEVICE.GetWidth() - 350) * 60.0f / m_NumFrames;
		m_ScaleTimeStart = t;
	}

	int numRows = 0;
	for(atArray<pfTrace*>::iterator it = m_Traces.begin(); it != m_Traces.end(); ++it)
	{
		pfTrace& trace= **it;
		if (trace.NumRows())
			numRows += trace.NumRows() + 1;
	}
	m_YPos = (float)(GRCDEVICE.GetHeight() - 50 - 4 * numRows);

	// render background
	m_VtxBuf->Begin();
	RenderQuad(42.f, m_YPos-8, GRCDEVICE.GetWidth()-42.f, m_YPos+(numRows+2)*4.f, Color32(1.f,1.f,1.f,.5f));
	m_VtxBuf->End();
	// draw 60 hz lines
	grcBegin(drawLines, (m_NumFrames + 1) * 2);
	float x = 200.0f;
	float dx = m_Scale / (sysTimer::GetTicksToSeconds() * 60.0f);
	float y1 = m_YPos-4;
	float y2 = y1 + (numRows + 2) * 4;
	for(u32 i=0; i<=m_NumFrames; ++i, x+=dx)
	{
		grcVertex(x, y1, 0, 0.0f, 0.0f, 0.0f, Color_black, 0.0f, 0.0f);
		grcVertex(x, y2, 0, 0.0f, 0.0f, 0.0f, Color_black, 0.0f, 0.0f);
	}
	grcEnd();

	m_VtxBuf->Begin();

	u32 nextsample = 0;
	m_Row = 0;
	for(atArray<pfTrace*>::iterator it = m_Traces.begin(); it != m_Traces.end(); ++it)
	{
		pfTrace& trace= **it;
		trace.BeginFrame();
		trace.AdvanceTo(m_FrameStart);
		u32 tb = trace.GetNextSample();
		if (tb && (!nextsample || s32(nextsample - tb) > 0))
			nextsample = tb;
		if (trace.NumRows())
		{
			grcColor(Color_black);
			grcDraw2dText(50.0f, m_YPos + m_Row * 4, trace.Name());
			m_Row += trace.NumRows() + 1;
		}
	}	

	u32 totrows = m_Row;
	while (nextsample && s32(nextsample - m_FrameEnd) < 0)
	{
		u32 sample = nextsample;
		nextsample = 0;
		m_Row = totrows;
		// going in reverse to ensure cell thread scheduler processed last
		for(atArray<pfTrace*>::reverse_iterator it = m_Traces.rbegin(); it != m_Traces.rend(); ++it)
		{
			pfTrace& trace= **it;
			if (trace.NumRows())
				m_Row -= trace.NumRows() + 1;
			trace.AdvanceTo(sample);
			u32 tb = trace.GetNextSample();
			//Assert(s32(nextsample - sample) > 0);
			if (tb && (!nextsample || s32(nextsample - tb) > 0))
				nextsample = tb;
		}		
	}

	m_Row = 0;
	for(atArray<pfTrace*>::iterator it = m_Traces.begin(); it != m_Traces.end(); ++it)
	{
		pfTrace& trace= **it;
		if (trace.NumRows())
			m_Row += trace.NumRows() + 1;
		trace.EndFrame(m_FrameEnd);
	}

	float y = 100.0f;

	if (m_ShowLegend)
	{
		// show the active trace legend and overall stats
		static s32 m_ActiveTrace = 0;
		if (m_ActiveTrace >= m_Traces.size())
			m_ActiveTrace = 0;
		if (KEYBOARD.KeyPressed(KEY_NUMPAD9) && m_ActiveTrace > 0)
			--m_ActiveTrace;
		if (KEYBOARD.KeyPressed(KEY_NUMPAD3) && m_ActiveTrace < m_Traces.size() - 1)
			++m_ActiveTrace;
		if (m_ActiveTrace >= 0 && m_ActiveTrace < m_Traces.size())
			m_Traces[m_ActiveTrace]->DisplayLegend(y);
	}

	{
		sysCriticalSection cs(m_Cs);

		u32 i = 0;
		int cursorIdx = 0;
		if (!m_ShowLegend && m_ShowTopCounters)
			m_Counters.QSort(0, m_Counters.size(), &SortCounters);

		for(atArray<pfTraceCounterId*>::iterator it = m_Counters.begin(); it != m_Counters.end(); ++it)
		{
			pfTraceCounterId& counter = **it;
			counter.m_Counter.EndFrame();
			if (!m_ShowLegend && i < m_ShowTopCounters)
			{
				if (&counter == m_ActiveTopCounter)
				{
					cursorIdx = (int)(it - m_Counters.begin());
					grcColor(Color_red);
				}
				else
				{
					grcColor(Color_white);
				}
				int time = (int)counter.m_Counter.FrameTime();
				char timestr[128];
				sprintf(timestr, "%6i %6.2f %s", time, counter.m_Counter.FrameCalls(), counter.m_Name);
				RenderQuad(88, y, 96, y+8, counter.m_Color);
				grcDraw2dText(100, y, timestr);
				y += 12;
				if (&counter == m_ActiveTopCounter)
				{
					for(atArray<pfTraceCounter*>::iterator it = counter.m_Instances.begin();
						it != counter.m_Instances.end(); ++it)
					{
						pfTraceCounter& inst = **it;
						int time = (int)inst.m_Counter.FrameTime();
						sprintf(timestr, "%6i %6.2f %s", time, inst.m_Counter.FrameCalls(), inst.m_Parent->m_Id->m_Name);
						RenderQuad(100, y, 108, y+8, inst.m_Id->m_Color);
						grcDraw2dText(112, y, timestr);
						y += 12;
						if (y >= m_YPos)
							break;
					}
				}
				if (y >= m_YPos)
					break;
				++i;
			}
		}
		if (!m_ShowLegend)
		{
			if (KEYBOARD.KeyPressed(KEY_NUMPAD8) && cursorIdx > 0)
				--cursorIdx;		
			if (KEYBOARD.KeyPressed(KEY_NUMPAD2) && cursorIdx < m_Counters.size() - 1)
				++cursorIdx;
			if (cursorIdx >= 0 && cursorIdx < m_Counters.size())
				m_ActiveTopCounter = m_Counters[cursorIdx];
		}
		if (m_InitCountDown && !--m_InitCountDown)
		{
			for(atArray<pfTrace*>::iterator it = m_Traces.begin(); it != m_Traces.end(); ++it)
			{
				pfTrace& trace= **it;
				trace.Reset();
			}
			for(atArray<pfTraceCounterId*>::iterator it = m_Counters.begin(); it != m_Counters.end(); ++it)
			{
				pfTraceCounterId& counter = **it;
				counter.m_Counter.Reset();
			}
		}	
	}

	m_VtxBuf->End();

	POP_DEFAULT_SCREEN();
}

static int SortChildren(pfTraceCounter* const* lhs, pfTraceCounter* const* rhs)
{
	float diff = (*lhs)->SortKey() - (*rhs)->SortKey();
	if (diff < 0)
		return 1;
	else if (diff > 0)
		return -1;
	return 0;
}

void pfTrace::DisplayLegend(float y)
{
	if (!m_CursorItem)
	{
		if (!m_Root)
			return;
		m_DisplayRoot = m_Root;
		m_CursorItem = m_Root;
	}	

	// gather parents
	pfTraceCounter* hierarchy[32];
	pfTraceCounter** pH = hierarchy;
	pfTraceCounter* pParent = m_DisplayRoot;
	for(; pParent; pParent = pParent->m_Parent)
		*pH++ = pParent;

	// display parents and self
	float x = 50.0f;
	while (pH != hierarchy)
	{
		pfTraceCounter* pCounter = *--pH; 
		grcColor(Color_white);
		pCounter->DisplayStats(x, y);
		y += 12; x += 12;		
	}

	// display children
	atArray<pfTraceCounter*> children(0, m_DisplayRoot->m_Children.GetNumUsed());
	for(atMap<pfTraceCounterId*, pfTraceCounter*>::Iterator it = m_DisplayRoot->m_Children.CreateIterator(); it; ++it)
		children.Push(*it);
	children.QSort(0, children.size(), &SortChildren);
	s32 cursorIdx = 0;
	for(atArray<pfTraceCounter*>::iterator it = children.begin(); it != children.end(); ++it)
	{
		if (*it == m_CursorItem)
		{
			grcColor(Color_red);
			cursorIdx = (int)(it - children.begin());
		}
		else
		{
			grcColor(Color_white);
		}
		(*it)->DisplayStats(x, y);		
		y += 12;
		if (y >= m_YPos)
			break;
	}

	// process input
	if (KEYBOARD.KeyPressed(KEY_NUMPAD8) && cursorIdx > 0)
		--cursorIdx;		
	if (KEYBOARD.KeyPressed(KEY_NUMPAD2) && cursorIdx < children.size() - 1)
		++cursorIdx;
	if (cursorIdx < children.size())
		m_CursorItem = children[cursorIdx];

	if (KEYBOARD.KeyPressed(KEY_NUMPAD6) && m_CursorItem && m_CursorItem->m_Children.GetNumUsed())
		m_DisplayRoot = m_CursorItem;
	if (KEYBOARD.KeyPressed(KEY_NUMPAD4) && m_DisplayRoot->m_Parent)
	{
		m_CursorItem = m_DisplayRoot;
		m_DisplayRoot = m_DisplayRoot->m_Parent;	
	}		
}

float pfTraceCounter::PercentageOfParent() const
{
	if (!m_Parent || !m_Parent->m_Counter.FrameTime())
		return 100.0f;
	return ((u64)m_Counter.FrameTime() * 10000 / (u64)m_Parent->m_Counter.FrameTime()) * .01f;	
}

void pfTraceCounter::DisplayStats(float x, float y)
{
	float pct = PercentageOfParent();
	int time = (int)m_Counter.FrameTime();
	char timestr[128];
	sprintf(timestr, "%6i %6.2f %6.1f%% %s", time, m_Counter.FrameCalls(), pct, m_Id->m_Name);
	Color32 col = m_Id->m_Color;
	col.SetAlpha(255);
	RageTrace().RenderQuad(x, y, x + 8, y + 8, col);
	RageTrace().RenderQuad(x + 12, y, x + 12 + pct, y + 8, col);
	grcDraw2dText(x + 104, y, timestr);
}

void pfTraceManager::RenderBar(u32 start, u32 end, Color32 color, u32 layer)
{
	if (s32(start - m_FrameStart) < 0)
		start = m_FrameStart;
	if (s32(m_FrameEnd - end) < 0)
		end = m_FrameEnd;
	if (s32(end - start) <= 0)
		return;

	float x1 = (start - m_FrameStart) * m_Scale + 200;
	float x2 = (end - m_FrameStart) * m_Scale + 200;
	float y1 = m_YPos + (m_Row + layer) * 4.0f;
	float y2 = y1 + 4;
	m_VtxBuf->Quad(x1, y1, x2, y2, color);
}

void pfTraceManager::RenderQuad(float x1, float y1, float x2, float y2, Color32 color)
{
	m_VtxBuf->Quad(x1, y1, x2, y2, color);
}

#if RAGE_TRACKING
	extern __THREAD int g_TrackerDepth;
#endif // RAGE_TRACKING

namespace {
	pfTraceCounterId* allocTraceCounter(const char* name, Color32 color)
	{
		return rage_new pfTraceCounterId(name, color);
	}
}

pfTraceCounterId* (*gAllocTraceCounterFunc)(const char*, Color32) = allocTraceCounter;

pfTraceCounterId& pfTraceManager::IdFromName(const char* name)
{
	sysCriticalSection cs(m_NameCs);
	sysMemAllocator& oldAlloc = sysMemAllocator::GetCurrent();
	sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());

#if RAGE_TRACKING
	++g_TrackerDepth;
#endif // RAGE_TRACKING

	static atMap<const char*, pfTraceCounterId*> s_CounterIds;
	pfTraceCounterId*& id = s_CounterIds[name];
	if (!id)
	{
		RAGE_TRACK(pfTraceManagerName);

		u32 hash = atStringHash(name);
		id = gAllocTraceCounterFunc(name, BarColor(hash));
	}

#if RAGE_TRACKING
	--g_TrackerDepth;
#endif // RAGE_TRACKING

	sysMemAllocator::SetCurrent(oldAlloc);
	return *id;
}

Color32 pfTraceManager::BarColor(u32 i) const
{
	static const Color32 s_BarColors[] = {
		Color_black,
		Color_red,
		Color_green,
		Color_yellow,
		Color_blue,
		Color_magenta,
		Color_cyan,
		Color_white,	
		Color_grey,
		Color_orange,
		Color32(0.5f,0.0f,0.0f),
		Color32(0.0f,0.5f,0.0f),
		Color32(0.5f,0.5f,0.0f),
		Color32(0.0f,0.0f,0.5f),
		Color32(0.5f,0.0f,0.5f),
		Color32(0.0f,0.5f,0.5f)
	};
	return s_BarColors[i % (sizeof(s_BarColors) / sizeof(s_BarColors[0]))];
}

pfTraceManager& RageTrace()
{
	static pfTraceManager trace;
	return trace;
}

//////////////////////////////////////////////////////////////////////////

pfTrace::pfTrace(const char* name, pfTraceCounter* root)
:	m_Root(root)
,	m_ActiveCounter(root)
,	m_NumRows(2)
,	m_Frame(0)
,	m_CursorItem(0)
,	m_DisplayRoot(root)
{
	strcpy(m_Name, name);
}

pfTrace::~pfTrace()
{
	delete m_Root;
	RageTrace().RemoveTrace(this);
}

void pfTrace::BeginFrame()
{	
}

void pfTrace::AdvanceTo(u32)
{
}

void pfTrace::PushCounter(u32 timebase, pfTraceCounterId* id)
{
	if (!m_ActiveCounter)
	{
		m_ActiveCounter = m_Root;
		m_Root->OnPush(timebase);
		if (id == m_Root->m_Id)
			return;
	}
	m_ActiveCounter = m_ActiveCounter->Child(id);
	m_ActiveCounter->OnPush(timebase);
}

void pfTrace::RenderBar(u32 endtime)
{
	RageTrace().RenderBar(m_ActiveCounter->m_StartTime, endtime, 
		m_ActiveCounter->m_Id->m_Color, 1); //m_ActiveCounter->m_Layer);
}

void pfTrace::PopCounter(u32 timebase)
{
	if (!m_ActiveCounter)
		return;
	if (m_NumRows && m_ActiveCounter->m_Parent == m_DisplayRoot) //m_ActiveCounter->m_Layer < m_NumRows)
		RenderBar(timebase);
	m_ActiveCounter->OnPop(timebase);
	m_ActiveCounter = m_ActiveCounter->m_Parent;
}

void pfTrace::Reset()
{
	if (m_Root)
		m_Root->Reset();
}

void pfTrace::EndFrame(u32 timebase)
{
	#define STACK_SIZE 64
	pfTraceCounterId* stack[STACK_SIZE];
	pfTraceCounterId** pStack = stack;
	
	for(pfTraceCounter* ctr = m_ActiveCounter; ctr; ctr = ctr->m_Parent)
	{
		// this messes up the count & length of call measurements .. 
		*pStack++ = ctr->m_Id;
		PopCounter(timebase);
		Assertf(pStack - &stack[0] < STACK_SIZE,"out of stack space, this is bad.");
	}

	OnEndFrame();

	if (m_Root)
		m_Root->EndFrame();

	while (pStack != stack)
		PushCounter(timebase, *--pStack);
}

void pfTrace::WriteToFile(fiStream* fh)
{
	if (m_Root)
		m_Root->WriteToFile(fh);
}

//////////////////////////////////////////////////////////////////////////

pfBufferedTrace::pfBufferedTrace(const char* name, u32 bufsize, pfTraceCounter* root)
:	pfTrace(name, root)
,	m_AutoPop(false)
{
	//m_Stack = 0;
	bufsize = (bufsize / 4) & ~1;
#if __PPU
	// use system memory as this is debug only stuff..
	m_Buf = (u32*)malloc(bufsize * 4);
#else
	m_Buf = rage_new u32[bufsize];
#endif
	m_BufEnd = m_Buf + bufsize;
	m_BufRead = m_Buf;
	m_BufWrite = m_Buf;
	memset(m_Buf, 0, bufsize*4);
}

pfBufferedTrace::~pfBufferedTrace()
{
#if __PPU
	free(m_Buf);
#else
	delete [] m_Buf;
#endif	
}

u32 pfBufferedTrace::GetNextSample()
{
	return m_BufRead[0];
}

void pfBufferedTrace::AdvanceTo(u32 timebase)
{
	while (m_BufRead[0] && s32(timebase - m_BufRead[0]) >= 0)
	{
		u32 time = m_BufRead[0];
		pfTraceCounterId* id = (pfTraceCounterId*)m_BufRead[1];
		if (id)
			PushCounter(time, id);
		else
			PopCounter(time);
		m_BufRead[0] = 0;
		m_BufRead[1] = 0;
		m_BufRead += 2;
		if (m_BufRead == m_BufEnd)
			m_BufRead = m_Buf;
	}
}

//////////////////////////////////////////////////////////////////////////

pfTraceCounterId::pfTraceCounterId(const char* name)
{
	RAGE_TRACK(pfTraceCounterId);

	RageTrace().AddCounter(this);
	Init(name);
}
pfTraceCounterId::pfTraceCounterId(const char* name, Color32 color)
{
	RAGE_TRACK(pfTraceCounterId);
	RageTrace().AddCounter(this);
	Init(name, color);
}

void pfTraceCounterId::Init(const char* name)
{
	u32 hash = atStringHash(name);
	Init(name, RageTrace().BarColor(hash));
}

pfTraceCounterId::~pfTraceCounterId()
{
	RageTrace().RemoveCounter(this);	
}

void pfTraceCounterId::Init(const char* name, Color32 color)
{
	strncpy(m_Name, name, sizeof(m_Name) - 1);
	m_Color = color;
}

pfTraceCounter::~pfTraceCounter()
{
	m_Id->m_Instances.DeleteMatches(this);
	for(atMap<pfTraceCounterId*, pfTraceCounter*>::Iterator it = m_Children.CreateIterator(); it; ++it)
		delete *it;
}

pfTraceCounter* pfTraceCounter::Child(pfTraceCounterId* id)
{
	pfTraceCounter*& child = m_Children[id];
	if (!child)
	{
		child = New();
		child->m_Parent = this;
		child->m_Layer = m_Layer + 1;
		child->m_Id = id;
		id->m_Instances.PushAndGrow(child);
	}
	return child;
}

void pfTraceCounter::OnPush(u32 timebase)
{
	m_StartTime = timebase;
}

void pfTraceCounter::OnPop(u32 timebase)
{
	s32 delta = timebase - m_StartTime;
	if (delta > 0)
	{
		double t = double(delta) * sysTimer::GetTicksToMicroseconds();
		m_Counter.Add(t);
		m_Id->m_Counter.Add(t);
	}
}

void pfTraceCounter::EndFrame()
{	
	m_Counter.EndFrame();
	for(atMap<pfTraceCounterId*, pfTraceCounter*>::Iterator it = m_Children.CreateIterator(); it; ++it)
		(*it)->EndFrame();
}

void pfTraceCounter::Reset()
{	
	m_Counter.Reset();
	for(atMap<pfTraceCounterId*, pfTraceCounter*>::Iterator it = m_Children.CreateIterator(); it; ++it)
		(*it)->Reset();
}

void pfTraceCounter::Dump()
{
// 	float pct = PercentageOfParent();	
// 	if (pct < .1f)
// 		return;
// 	float scale = sysTimer::GetTicksToMicroseconds() * (1.0f / 32.0f);
// 	int time = (int)(m_Time.Sum() * scale);
// 	const char* spaces = "                                            ";
// 	Displayf("%*.*s[%5.2f%%][%8i][%6.1f] : %s", m_Layer, m_Layer, spaces, 
// 		pct, time, m_Time.NumSamples() * (1.0f / 32.0f), m_Id->m_Name);
// 	DumpChildren();
}

float pfTraceCounter::SortKey() const
{
	return (float)m_Id->m_Counter.m_TotalFrameTime.Mean();
}

void pfTraceCounter::DumpChildren()
{
	atArray<pfTraceCounter*> children(0, m_Children.GetNumUsed());
	for(atMap<pfTraceCounterId*, pfTraceCounter*>::Iterator it = m_Children.CreateIterator(); it; ++it)
		children.Push(*it);
	children.QSort(0, children.size(), &SortChildren);
	for(atArray<pfTraceCounter*>::iterator it = children.begin(); it != children.end(); ++it)
		(*it)->Dump();
}

void pfTraceCounter::WriteToFile(fiStream* fh)
{
	fprintf(fh, "%s, %s, %f, %f, %f, %f, %f, %f, %f, %f\n", 
		m_Id->m_Name, 
		m_Parent ? m_Parent->m_Id->m_Name : "",
		m_Counter.m_TotalFrameTime.Mean(), 
		m_Counter.m_TotalFrameTime.StandardDeviation(),
		m_Counter.m_TotalFrameTime.Lower95thPercentile(),
		m_Counter.m_TotalFrameTime.Upper95thPercentile(),
		m_Counter.m_TotalFrameCalls.Mean(), 
		m_Counter.m_TotalFrameCalls.StandardDeviation(),
		m_Counter.m_TotalFrameCalls.Lower95thPercentile(),
		m_Counter.m_TotalFrameCalls.Upper95thPercentile());		
	atArray<pfTraceCounter*> children(0, m_Children.GetNumUsed());
	for(atMap<pfTraceCounterId*, pfTraceCounter*>::Iterator it = m_Children.CreateIterator(); it; ++it)
		children.Push(*it);
	children.QSort(0, children.size(), &SortChildren);
	for(atArray<pfTraceCounter*>::iterator it = children.begin(); it != children.end(); ++it)
		(*it)->WriteToFile(fh);
}

void pfFrameStatCounter::Reset()
{
	m_FrameTime.Reset();
	m_FrameCalls.Reset();
	m_TotalFrameTime.Reset();
	m_TotalFrameCalls.Reset();
	m_Frame.Reset();
}

void pfFrameStatCounter::EndFrame()
{
	m_FrameTime.Add(m_Frame.Sum());
	m_FrameCalls.Add((double)m_Frame.NumSamples());
	m_TotalFrameTime.Add(m_Frame.Sum());
	m_TotalFrameCalls.Add((double)m_Frame.NumSamples());
	if (m_FrameTime.NumSamples() >= RageTrace().SampleFrames())
	{
		m_AvgPerFrameTime = (float)m_FrameTime.Mean();
		m_AvgPerFrameCalls = (float)m_FrameCalls.Mean();
		m_FrameTime.Reset();
		m_FrameCalls.Reset();
	}
	m_Frame.Reset();
}

} // namespace rage

#endif // RAGETRACE
