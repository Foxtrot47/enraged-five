#include "settings.h"

#if USE_PROFILER

#include "core.h"
#include "event_tracer.h"
#include "string/unicode.h"
#include "system/eventtracingforwindows.h"
#include "system/memops.h"
#include "system/param.h"

PARAM(rockySamplingFrequency, "[Rocky] Overrides Rocky auto-sampling frequency (1000 by default)");

#if RSG_SCE
#include <perf.h>
#include <unistd.h>
#if RSG_PROSPERO
#include <libsysmodule.h>
#pragma comment(lib,"ScePerf_nosubmission_stub_weak")
#pragma comment(lib,"SceSysmodule_stub_weak")
#else
#pragma comment(lib,"ScePerf_stub_weak")
#endif
#endif

#if (RSG_PC || RSG_XBOX)

// PURPOSE: Allows to limit events only to the currently active list of threads.
//			Disabled by default due to expensive hash_map search for each event.
//			Usually enabled in some cases which require longer sessions with very limited list of threads.
#define LIMIT_ACTIVE_THREADS (0)

#define INITGUID  // Causes definition of SystemTraceControlGuid in evntrace.h.
#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#include <wmistr.h>
#include <evntrace.h>
#include <evntcons.h>
#pragma warning(pop)
#endif

#if defined(RSG_GDK) && RSG_GDK
#pragma warning(push)
#pragma warning(disable:4668)    // 'DBG' is not defined as a preprocessor macro, replacing with '0' for '#if/#elif'
#include <pix.h>
#pragma warning(pop)
#include "durango_etw_secret_api.h"
#pragma comment(lib, "pixevt.lib")
#elif RSG_DURANGO
#pragma warning(push)
#pragma warning(disable:4668)    // 'DBG' is not defined as a preprocessor macro, replacing with '0' for '#if/#elif'
#include <pix.h>
#pragma warning(pop)
#include "durango_etw_secret_api.h"
#pragma comment(lib, "toolhelpx.lib")
#pragma comment(lib, "PIXEvt.lib")
#endif

#if RSG_LINUX
#include "tracer_linux.h"
#endif 

using namespace rage;

#if ETW_SUPPORTED
const EtwGuid SampledProfileGuid(0xce1dbfb4, 0x137e, 0x4da6, 0x87b0, 0x3f59aa102cbcuLL);
#endif

namespace Profiler
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class EventTracerProcessor
	{
#if ETW_SUPPORTED
		EtwSession* etwSession;
#endif

#if RSG_PC
		void AdjustPrivileges();
#endif

#if RSG_SCE
		rage::atVector<ThreadEntry*> threads;

		int ParseTrace(void* pBuffer, size_t bufSize);
		rage::u32 sessionPID;
		int sessionTraceID;
		void* sessionTraceBuffer;
		void* sessionTempBuffer;

		rage::sysIpcEvent workerFinishEvent;
#endif

		rage::sysIpcThreadId processThreadHandle;
		static void ProcessEvents(void* parameter);
	public:
		EventTracerProcessor();
		~EventTracerProcessor();

		EventTracer::Status Start(EventTracer::Mode mode);
		bool Stop();
		
		bool Flush(Mode::Type mode, EventTime scope = EventTime());

		EventTracer::Mode currentMode;
		rage::stlUnorderedSet<rage::u64> activeThreads;
		rage::atRangeArray<rage::u8, 128> activeCores;
		rage::u64 mainThread;

		PageFaultStats pageFaultStats;
	};
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	EventTracerProcessor gEventTracerProcessor;
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void OnSwitchContextEvent(rage::u32 oldThreadID, rage::u32 newThreadID, rage::u64 timestamp, rage::u16 oldThreadPriority, rage::u16 newThreadPriority, rage::u8 reason, rage::u8 cpuID)
	{
		SwitchContextData& event = EventTracer::Get().switchContextEvents.Add();
		event.timestamp = Timestamp::Pack(timestamp);
		event.oldThreadID = oldThreadID;
		event.newThreadID = newThreadID;
		event.oldThreadPriority = oldThreadPriority;
		event.newThreadPriority = newThreadPriority;
		event.cpuID = cpuID;
		event.reason = reason;

		if (gEventTracerProcessor.currentMode & EventTracer::MODE_LIVE)
		{
			EventTracer::Get().GetFrontSynchronizationBuffer().Add() = event;
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void OnStackWalkEvent(rage::u64 threadID, rage::u64 timestamp, rage::u64* callstack, rage::u8 count, u32 mode)
	{
		StackWalkData& event = EventTracer::Get().stackwalkEvents.Add();
		event.entry.time.start = Timestamp::Pack(timestamp);
		event.entry.mode = mode;
		event.threadID = threadID;

		sysMemSet(&event.entry.callstack, 0, MAX_CALLSTACK_DEPTH * sizeof(rage::u64));

		rage::u8 trimmedCount = Min((rage::u8)MAX_CALLSTACK_DEPTH, count);
		sysMemCpy(event.entry.callstack, callstack + count - trimmedCount, trimmedCount * sizeof(rage::u64));
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void OnPageFaultEvent(rage::u64 threadID, rage::u64 start, rage::u64 finish, rage::u64 virtualAddress, rage::u64 readOffset, rage::u32 byteCount)
	{
		if (gEventTracerProcessor.currentMode & EventTracer::MODE_PAGE_FAULTS)
		{
			if (gEventTracerProcessor.activeThreads.find(threadID) != gEventTracerProcessor.activeThreads.end())
			{
				PageFaultData& pageFault = EventTracer::Get().pageFaultEvents.Add();
				pageFault.start = Timestamp::Pack(start);
				pageFault.finish = Timestamp::Pack(finish);
				pageFault.threadID = threadID;
				pageFault.virtualAddress = virtualAddress;
				pageFault.readOffset = readOffset;
				pageFault.byteCount = byteCount;

				const float durationMs = (finish - start) * sysTimer::GetTicksToMilliseconds();
				gEventTracerProcessor.pageFaultStats.Add(durationMs);
			}
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void OnHardwareEvent(rage::u64 threadID, rage::u64 timestamp, const rage::u64* values, rage::u8 count)
	{
		if (gEventTracerProcessor.currentMode & EventTracer::MODE_HW_EVENTS)
		{
			HardwareEventData& event = EventTracer::Get().hardwareEvents.Add();
			event.time.start = Timestamp::Pack(timestamp);
			event.threadID = threadID;
			sysMemCpy(event.values, values, count * sizeof(u64));
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if ETW_SUPPORTED
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	const rage::s8 SWITCH_CONTEXT_REASON_ENUM_SHIFT = 13;
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct CSwitch
	{
		rage::u32 NewThreadId;
		rage::u32 OldThreadId;
		int8  NewThreadPriority;
		int8  OldThreadPriority;
		uint8  PreviousCState;
		int8  SpareByte;
		int8  OldThreadWaitReason;
		int8  OldThreadWaitMode;
		int8  OldThreadState;
		int8  OldThreadWaitIdealProcessor;
		rage::u32 NewThreadWaitTime;
		rage::u32 Reserved;

		static const u8 OPCODE = 36;
	};
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct SampledProfile
	{
		rage::u64 InstructionPointer;
		rage::u32 ThreadId;
		rage::u32 Count;

		static const u8 OPCODE = 46;
	};
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct SysCallEnter
	{
		rage::u64 SysCallAddress;

		static const byte OPCODE = 51;
	};
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct PCMCounterProfile
	{
		static const u8 OPCODE = 47;
	};
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct StackWalk_Event
	{
		rage::u64 EventTimeStamp;
		rage::u32 StackProcess;
		rage::u32 StackThread;
		rage::u64 Stack0;

		static const byte OPCODE = 32;
		static const EtwGuid GUID;
	};
	const EtwGuid StackWalk_Event::GUID = EtwGuid(0xdef2fe46UL, 0x7bd6, 0x4b80, 0xbd94, 0xf57fe20d0ce3ULL);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct PageFault_V2
	{
		static const EtwGuid GUID;
	};
	const EtwGuid PageFault_V2::GUID = EtwGuid(0x3d6fa8d3UL, 0xfe05, 0x11d0, 0x9dda, 0x00c04fd7ba7cULL);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct PageFault_HardFault : public PageFault_V2
	{
		rage::u64 InitialTime;
		rage::u64 ReadOffset;
		rage::u64 VirtualAddress;
		rage::u64 FileObject;
		rage::u32 TThreadId;
		rage::u32 ByteCount;

		static const byte OPCODE = 32;
	};
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct Process_TypeGroup1 {
		rage::u64 UniqueProcessKey;
		rage::u32 ProcessId;
		rage::u32 ParentId;
		rage::u32 SessionId;
		rage::s32 ExitStatus;
		rage::u64 DirectoryTableBase;
		rage::u32 Flags;
		// SID UserSID;				// variable size
		// string ImageFileName;	// null terminated
		// wstring CommandLine;		// null terminated

#if !ETW_TDH_SUPPORTED
		rage::u8 UserSID[28];
		char ImageFileName[1];
#endif

		static const EtwGuid GUID;
	};
	const EtwGuid Process_TypeGroup1::GUID = EtwGuid(0x3d6fa8d0UL, 0xfe05, 0x11d0, 0x9dda, 0x00c04fd7ba7cULL);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct Thread_TypeGroup1
	{
		rage::u32 ProcessId;
		rage::u32 TThreadId;
		rage::u64 StackBase;
		rage::u64 StackLimit;
		rage::u64 UserStackBase;
		rage::u64 UserStackLimit;
		rage::u64 Affinity;
		rage::u64 Win32StartAddr;
		rage::u64 TebBase;
		rage::u32 SubProcessTag;
		rage::u8 BasePriority;
		rage::u8 PagePriority;
		rage::u8 IoPriority;
		rage::u8 ThreadFlags;
		static const EtwGuid GUID;
	};
	const EtwGuid Thread_TypeGroup1::GUID = EtwGuid(0x3d6fa8d1UL, 0xfe05, 0x11d0, 0x9dda, 0x00c04fd7ba7cULL);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rage::u32 g_CurrentProcessId = GetCurrentProcessId();
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void __stdcall OnRecordEvent(const EtwEventRecord* eventRecord)
	{
		USES_CONVERSION;
		USE_ROCKY_MEMORY;

		EtwOpcode opcode = eventRecord->EventHeader.EventDescriptor.Opcode;

		switch ((u8)opcode)
		{
		// Switch Context events
		case CSwitch::OPCODE:
			if (sizeof(CSwitch) == eventRecord->UserDataLength)
			{
				CSwitch* pSwitchEvent = (CSwitch*)eventRecord->UserData;
				OnSwitchContextEvent(pSwitchEvent->OldThreadId,
					pSwitchEvent->NewThreadId,
					eventRecord->EventHeader.TimeStamp,
					pSwitchEvent->OldThreadPriority,
					pSwitchEvent->NewThreadPriority,
					(rage::u8)(pSwitchEvent->OldThreadWaitReason + SWITCH_CONTEXT_REASON_ENUM_SHIFT),
					(rage::u8)eventRecord->BufferContext.ProcessorIndex);

#if LIMIT_ACTIVE_THREADS
				//if (gEventTracerProcessor.activeThreads[pSwitchEvent->OldThreadId])
				if (gEventTracerProcessor.mainThread == pSwitchEvent->OldThreadId)
					gEventTracerProcessor.activeCores[eventRecord->BufferContext.ProcessorNumber] = 0;

				//if (gEventTracerProcessor.activeThreads[pSwitchEvent->NewThreadId])
				if (gEventTracerProcessor.mainThread == pSwitchEvent->NewThreadId)
					gEventTracerProcessor.activeCores[eventRecord->BufferContext.ProcessorNumber] = 1;
#endif
			}
			break;

		// Stack Walk Events
		case StackWalk_Event::OPCODE: // +case PageFault_HardFault::OPCODE:
			{
				if (eventRecord->UserData && eventRecord->UserDataLength > 0)
				{
					if (eventRecord->EventHeader.ProviderId == StackWalk_Event::GUID)
					{
						StackWalk_Event* pStackWalkEvent = (StackWalk_Event*)eventRecord->UserData;

					#if LIMIT_ACTIVE_THREADS
						if (gEventTracerProcessor.activeCores[eventRecord->BufferContext.ProcessorNumber])
					#endif
						{
							rage::u32 count = 1 + (eventRecord->UserDataLength - sizeof(StackWalk_Event)) / 8;

							if (count && pStackWalkEvent->StackThread != 0)
							{
								OnStackWalkEvent(pStackWalkEvent->StackThread,
									pStackWalkEvent->EventTimeStamp,
									&pStackWalkEvent->Stack0,
									(rage::u8)count,
									Mode::AUTOSAMPLING);
							}
						}
					}
					else if (eventRecord->EventHeader.ProviderId == PageFault_HardFault::GUID)
					{
						PageFault_HardFault* pHadFaultEvent = (PageFault_HardFault*)eventRecord->UserData;

						OnPageFaultEvent(pHadFaultEvent->TThreadId, 
							pHadFaultEvent->InitialTime, 
							eventRecord->EventHeader.TimeStamp, 
							pHadFaultEvent->VirtualAddress, 
							pHadFaultEvent->ReadOffset, 
							pHadFaultEvent->ByteCount);
					}
				}
			}
			break;

		// Default case - we have do expensive GUID comparison in this case to identify event
		default:
			{
				//EtwEventMetadata* md = EtwEventMetadata::Create(eventRecord);
				//md->PrintEvent(eventRecord, [](const char *OUTPUT_ONLY(str)) {rockyDisplayf("%s", str); });

				if (eventRecord->EventHeader.ProviderId == Process_TypeGroup1::GUID)
				{
					if (opcode == EtwOpcode::START || opcode == EtwOpcode::DC_START)
					{
						Process_TypeGroup1* pProcessEvent = (Process_TypeGroup1*)eventRecord->UserData;


#if ETW_TDH_SUPPORTED
						char fileName[256] = { 0 };
						wchar_t wCommandLine[1024] = { 0 };
						EtwGetProperty(fileName, sizeof(fileName), eventRecord, L"ImageFileName");
						EtwGetProperty(wCommandLine, sizeof(wCommandLine), eventRecord, L"CommandLine");
#else
						const char* fileName = pProcessEvent->ImageFileName;
						const wchar_t* wCommandLine = (const wchar_t*)(fileName + strlen(fileName) + 1);
#endif
						
						Core::Get().RegisterProcessDescription(ProcessDescription(
							pProcessEvent->UniqueProcessKey, 
							pProcessEvent->ProcessId, 
							fileName, 
							W2A((char16*)wCommandLine)
						));
					}
				}

				if (eventRecord->EventHeader.ProviderId == Thread_TypeGroup1::GUID)
				{
					if (opcode == EtwOpcode::START || opcode == EtwOpcode::DC_START)
					{
						Thread_TypeGroup1* pThreadEvent = (Thread_TypeGroup1*)eventRecord->UserData;

						ThreadDescription desc;
						desc.systemThreadId = pThreadEvent->TThreadId;
						desc.processId = pThreadEvent->ProcessId;
						desc.priority = pThreadEvent->BasePriority;
						desc.baseAddress = pThreadEvent->Win32StartAddr;
						Core::Get().RegisterThreadDescription(desc);
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	const TRACEHANDLE INVALID_TRACEHANDLE = (TRACEHANDLE)-1;
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif

#if RSG_LINUX
	struct PIDCache
	{
		atSet<u32> m_Cache;
		void Reset()
		{
			m_Cache.Reset();
		}
		bool Add(u32 pid)
		{
			return m_Cache.Insert(pid);
		}
	};
	PIDCache g_pidCache;
	
	void OnRecordEvent(const ftrace::event_header& header, const ftrace::event_base& data)
	{
		switch (data.common_type)
		{
		case ftrace::EVENT_SCHED_SWITCH:
			const ftrace::sched_switch& ev = (const ftrace::sched_switch&)data;
			OnSwitchContextEvent(ev.prev_pid,
				ev.next_pid,
				header.timestamp,
				ev.prev_prio,
				ev.next_prio,
				0, // VS TODO: FIX ME!
				header.cpuid);

			// Registering a new thread if needed
			if (g_pidCache.Add(ev.next_pid))
			{
				ThreadDescription desc;
				desc.name = ev.next_comm;
				desc.systemThreadId = ev.next_pid;
				desc.processId = ev.next_pid;
				desc.priority = ev.next_prio;
				desc.baseAddress = 0;
				Core::Get().RegisterThreadDescription(desc);
			}

			break;
		}
	}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void EventTracerProcessor::ProcessEvents(void* parameter)
	{
		//PROFILER_THREAD("Rocky::EventTracerProcessor", 1);

		EventTracerProcessor* tracer = (EventTracerProcessor*)parameter;
		(void)tracer;

#if ETW_SUPPORTED
		if (EtwSession* session = tracer->etwSession)
			session->Process();
#endif

#if RSG_SCE
		uint8_t* sessionLastPosition = (uint8_t*)tracer->sessionTraceBuffer;

		void* sessionTraceBuffer = tracer->sessionTraceBuffer;
		void* sessionTempBuffer = tracer->sessionTempBuffer;

		while (!sysIpcPollEvent(tracer->workerFinishEvent))
		{
			rage::sysIpcSleep(5);

			PROFILER_CATEGORY("ProcessEvents", Profiler::CategoryColor::Debug);

			int ret = 0;
			uint8_t* pStartRead = NULL;
			uint8_t* pEndRead = NULL;
			size_t streamSize = 0;

			// Parse Name Trace
			ScePerfTraceInfo traceInfo;

			// Parse Main Trace
			ret = scePerfTraceGetInfo(tracer->sessionTraceID, &traceInfo);
			if (!rockyVerifyf(ret == SCE_OK, "scePerfTraceGetInfo failed"))
			{
				//Can't just drop out, sysIpcDeleteEvent on prospero expects the event to be in a reset state
				sysIpcWaitEvent( tracer->workerFinishEvent );
				break;
			}

			// We don't need to clean memory - it will be overridden anyway
			//memset(tracer->sessionTempBuffer, 0, traceInfo.size);

			if (traceInfo.flag == SCE_PERF_TRACE_OVERWRITE)
			{
				pStartRead = sessionLastPosition;
				pEndRead = (uint8_t*)sessionTraceBuffer + traceInfo.offset;
			}
			else
			{
				pStartRead = sessionLastPosition;
				pEndRead = (uint8_t*)sessionTraceBuffer + traceInfo.size;
			}
			sessionLastPosition = pEndRead;

			// Unwrap the buffer if it's overflowed
			if (pEndRead < pStartRead)
			{
				// Copy from the startRead to the end of the buffer
				// then copy from the beginning until the endRead
				size_t startReadToEndBuffer = ((uint8_t*)sessionTraceBuffer + traceInfo.size) - pStartRead;

				memcpy(sessionTempBuffer, pStartRead, startReadToEndBuffer);
				memcpy((uint8_t*)sessionTempBuffer + startReadToEndBuffer, sessionTraceBuffer, pEndRead - (uint8_t*)sessionTraceBuffer);

				streamSize = traceInfo.size - (pStartRead - pEndRead);
			}
			else
			{
				streamSize = pEndRead - pStartRead;
				memcpy(sessionTempBuffer, pStartRead, streamSize);
			}

			ret = tracer->ParseTrace(sessionTempBuffer, streamSize);
			if (!rockyVerifyf(ret == SCE_OK, "parseTrace failed"))
			{
				//Can't just drop out, sysIpcDeleteEvent on prospero expects the event to be in a reset state
				sysIpcWaitEvent( tracer->workerFinishEvent );
				break;
			}
		}
#endif
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PC_SAMPLE_RATE				(SCE_PERF_TRACE_PS_FREQ_MAX/2)
#define PC_AUTOSAMPLE_RATE			(1000)
#define MAIN_TRACE_BUF_SIZE			(32 * 1024 * 1024)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	EventTracerProcessor::EventTracerProcessor()
		: currentMode(EventTracer::MODE_NONE)
#if ETW_SUPPORTED
		, etwSession(nullptr)
#endif
#if RSG_SCE
		, workerFinishEvent(0)
		, sessionPID(0)
		, sessionTraceID(0)
		, sessionTraceBuffer(0)
		, sessionTempBuffer(0)
#endif
		, processThreadHandle(sysIpcThreadIdInvalid)
	{
	}

#if RSG_SCE

	// Documentation:
	// %SCE_ORBIS_SDK%\..\documentation\pdf\en\SDK_doc\Optimization\Performance_Optimization_e\CPU_Analysis_Using_Hardware_Counter_e.pdf

	// Verified core performance counter events
#define SCE_PERF_PMC_EVENT_RETIRED_SSE_AVX_OPERATIONS 0x3
#define SCE_PERF_PMC_EVENT_RETIRED_SSE_AVX_OPS 0xA
#define SCE_PERF_PMC_EVENT_RETIRED_X87_FLOATING_POINT_OPERATIONS 0x11
#define SCE_PERF_PMC_EVENT_MOQ_REPLAY 0x25
#define SCE_PERF_PMC_EVENT_CANCELED_STORE_TO_LOAD_FORWARD_OPERATIONS 0x2A
#define SCE_PERF_PMC_EVENT_DATA_CACHE_REFILLS_FROM_L2_OR_NORTHBRIDGE 0x42
#define SCE_PERF_PMC_EVENT_DATA_CACHE_REFILLS_FROM_THE_NORTHBRIDGE 0x43
#define SCE_PERF_PMC_EVENT_L1_DTLB_MISS_AND_L2_DTLB_HIT 0x45
#define SCE_PERF_PMC_EVENT_MISALIGNED_ACCESSES 0x47
#define SCE_PERF_PMC_EVENT_PREFETCH_INSTRUCTIONS_DISPATCHED 0x4B
#define SCE_PERF_PMC_EVENT_L1_DTLB_HIT 0x4D
#define SCE_PERF_PMC_EVENT_L1_DTLB_RELOAD_LATENCY 0x4E
#define SCE_PERF_PMC_EVENT_INEFFECTIVE_SOFTWARE_PREFETCHES 0x52
#define SCE_PERF_PMC_EVENT_COMMAND_RELATED_TO_UNCACHEABLE_MEMORY_AND_IO 0x61
#define SCE_PERF_PMC_EVENT_MEMORY_REQUESTS_BY_TYPE 0x65
#define SCE_PERF_PMC_EVENT_MAB_REQUESTS 0x68
#define SCE_PERF_PMC_EVENT_MAB_WAIT_CYCLES 0x69
#define SCE_PERF_PMC_EVENT_DATA_CACHE_ACCESSES 0x40
#define SCE_PERF_PMC_EVENT_DATA_CACHE_MISSES 0x41
#define SCE_PERF_PMC_EVENT_DTLB_MISS 0x46
#define SCE_PERF_PMC_EVENT_MAB_WAIT_CYCLES 0x69
#define SCE_PERF_PMC_EVENT_CPU_CLOCKS_NOT_HALTED 0x76 
#define SCE_PERF_PMC_EVENT_RETIRED_INSTRUCTIONS 0xC0
#define SCE_PERF_PMC_EVENT_DC_HARDWARE_PREFETCH_ALLOCATES 0x4F
#define SCE_PERF_PMC_EVENT_INSTRUCTION_CACHE_MISSES 0x81
#define SCE_PERF_PMC_EVENT_INSTRUCTION_CACHE_FETCHES 0x80
#define SCE_PERF_PMC_EVENT_ITLB_MISS 0x85
#define SCE_PERF_PMC_EVENT_INSTRUCTION_FETCH_STALL 0x87
#define SCE_PERF_PMC_EVENT_INSTRUCTION_FETCH_STALLS 0x8D
#define SCE_PERF_PMC_EVENT_ITLB_RELOAD_STALLS 0x95
#define SCE_PERF_PMC_EVENT_NO_OP_DISPATCHED_CYCLE 0xA2
#define SCE_PERF_PMC_EVENT_DISPATCH_STALLS_GROUP_ONE 0xA3
#define SCE_PERF_PMC_EVENT_DISPATCH_STALLS_GROUP_TWO 0xA4
#define SCE_PERF_PMC_EVENT_DISPATCH_STALLS_GROUP_THREE 0xA5

// Unverified core performance counter events
#define SCE_PERF_PMC_EVENT_DISPATCHED_FPU_OPERATIONS 0x0
#define SCE_PERF_PMC_EVENT_RETIRED_MMX_FP_INSTRUCTIONS 0xCB
#define SCE_PERF_PMC_EVENT_FP_SCHEDULER_EMPTY 0x1
#define SCE_PERF_PMC_EVENT_DISPATCHED_FAST_FLAG_FPU_OPERATIONS 0x2
#define SCE_PERF_PMC_EVENT_RETIRED_SERIALIZING_OPS 0x5
#define SCE_PERF_PMC_EVENT_SEGMENT_REGISTER_LOADS 0x20
#define SCE_PERF_PMC_EVENT_PIPELINE_RESTART_DUE_TO_SELF_MODIFYING_CODE 0x21
#define SCE_PERF_PMC_EVENT_PIPELINE_RESTART_DUE_TO_PROBE_HIT 0x22
#define SCE_PERF_PMC_EVENT_LOCKED_OPERATIONS 0x24
#define SCE_PERF_PMC_EVENT_RETIRED_CLFLUSH_INSTRUCTIONS 0x26
#define SCE_PERF_PMC_EVENT_RETIRED_CPUID_INSTRUCTIONS 0x27
#define SCE_PERF_PMC_EVENT_LS_DISPATCH 0x29
#define SCE_PERF_PMC_EVENT_DATA_CACHE_LINES_EVICTED 0x44
#define SCE_PERF_PMC_EVENT_DCACHE_MISSES_BY_LOCKED_INSTRUCTIONS 0x4C
#define SCE_PERF_PMC_EVENT_GLOBAL_PAGE_INVALIDATIONS 0x54
#define SCE_PERF_PMC_EVENT_TLB_INVALIDATES_CAUSED_BY_INVLPG_AND_INVLPGA 0x55
#define SCE_PERF_PMC_EVENT_COMMAND_RELATED_TO_READ_BLOCK_OPERATIONS 0x62
#define SCE_PERF_PMC_EVENT_COMMAND_RELATED_TO_CHANGE_TO_DIRTY_OPERATIONS 0x63
#define SCE_PERF_PMC_EVENT_DATA_CACHE_PREFETCHES 0x67
#define SCE_PERF_PMC_EVENT_SYSTEM_RESPONSE_BY_COHERENCE_STATE 0x6C
#define SCE_PERF_PMC_EVENT_DATA_WRITTEN_TO_SYSTEM 0x6D
#define SCE_PERF_PMC_EVENT_PROBE_HITS 0x73
#define SCE_PERF_PMC_EVENT_CACHE_CROSS_INVALIDATES 0x75
#define SCE_PERF_PMC_EVENT_TLB_FLUSH_EVENTS 0x78
#define SCE_PERF_PMC_EVENT_GLOBAL_PAGE_INVALIDATIONS 0x54
#define SCE_PERF_PMC_EVENT_PDC_MISS 0x162
#define SCE_PERF_PMC_EVENT_PAGE_TABLE_WALKER_0 0x16E
#define SCE_PERF_PMC_EVENT_PAGE_TABLE_WALKER_1 0x16F
#define SCE_PERF_PMC_EVENT_INSTRUCTION_CACHE_REFILLS_FROM_L2 0x82
#define SCE_PERF_PMC_EVENT_INSTRUCTION_CACHE_REFILLS_FROM_SYSTEM 0x83
#define SCE_PERF_PMC_EVENT_L1_ITLB_MISS_L2_ITLB_HIT 0x84
#define SCE_PERF_PMC_EVENT_RETURN_STACK_HITS 0x88
#define SCE_PERF_PMC_EVENT_RETURN_STACK_OVERFLOWS 0x89
#define SCE_PERF_PMC_EVENT_INSTRUCTION_CACHE_VICTIMS 0x8B
#define SCE_PERF_PMC_EVENT_INSTRUCTION_CACHE_LINES_INVALIDATED 0x8C
#define SCE_PERF_PMC_EVENT_ITLB_RELOADS 0x99
#define SCE_PERF_PMC_EVENT_ITLB_RELOADS_ABORTED 0x9A
#define SCE_PERF_PMC_EVENT_RETIRED_INDIRECT_BRANCH_INFO 0x19A
#define SCE_PERF_PMC_EVENT_RETIRED_UOPS 0xC1
#define SCE_PERF_PMC_EVENT_RETIRED_BRANCH_INSTRUCTIONS 0xC2
#define SCE_PERF_PMC_EVENT_RETIRED_MISPREDICTED_BRANCH_INSTRUCTIONS 0xC3
#define SCE_PERF_PMC_EVENT_RETIRED_TAKEN_BRANCH_INSTRUCTIONS 0xC4
#define SCE_PERF_PMC_EVENT_RETIRED_TAKEN_BRANCH_INSTRUCTIONS_MISPREDICTED 0xC5
#define SCE_PERF_PMC_EVENT_RETIRED_FAR_CONTROL_TRANSFERS 0xC6
#define SCE_PERF_PMC_EVENT_RETIRED_BRANCH_RESYNCS 0xC7
#define SCE_PERF_PMC_EVENT_RETIRED_NEAR_RETURNS 0xC8
#define SCE_PERF_PMC_EVENT_RETIRED_NEAR_RETURNS_MISPREDICTED 0xC9
#define SCE_PERF_PMC_EVENT_RETIRED_MISPREDICTED_TAKEN_BRANCH_INSTRUCTIONS_DUE_TO_TARGET_MISMATCH 0xCA
#define SCE_PERF_PMC_EVENT_RETIRED_MMX_FP_INSTRUCTIONS 0xCB
#define SCE_PERF_PMC_EVENT_INTERRUPTS_MASKED_CYCLES 0xCD
#define SCE_PERF_PMC_EVENT_INTERRUPTS_TAKEN 0xCF
#define SCE_PERF_PMC_EVENT_INTERRUPTS_MASKED_CYCLES_WITH_INTERRUPT_PENDING 0xCE
#define SCE_PERF_PMC_EVENT_INTERRUPTS_MASKED_CYCLES 0xCD
#define SCE_PERF_PMC_EVENT_INTERRUPTS_TAKEN 0xCF
#define SCE_PERF_PMC_EVENT_FPU_EXCEPTIONS 0xDB
#define SCE_PERF_PMC_EVENT_BREAKPOINT_MATCHES_0 0xDC
#define SCE_PERF_PMC_EVENT_BREAKPOINT_MATCHES_1 0xDD
#define SCE_PERF_PMC_EVENT_BREAKPOINT_MATCHES_2 0xDE
#define SCE_PERF_PMC_EVENT_BREAKPOINT_MATCHES_3 0xDF
#define SCE_PERF_PMC_EVENT_TAGGED_IBS_OPS 0x1CF
#define SCE_PERF_PMC_EVENT_TAGGED_IBS_OPS_RETIRED 0x1D0


namespace Perf 
{
	HWEventDescription CPUClocksNotIdle("Work Cycles", 
										"The number of clocks that the CPU is not in a halted state", 
										HWCounterType::TOTAL_CYCLES, 
										Profiler::Color::LimeGreen,
										SCE_PERF_PMC_EVENT_CPU_CLOCKS_NOT_HALTED, 0, 1);

	HWEventDescription InstructionCacheMissWaitCycles(	"ICache Miss", 
														"Instruction Cache Miss Wait Cycles", 
														HWCounterType::WAIT_CYCLES, 
														Profiler::Color::Gold,
														SCE_PERF_PMC_EVENT_MAB_WAIT_CYCLES, 0xC, 1);

	HWEventDescription DataCacheMissWaitCycles(	"DCache Miss", 
												"Data Cache Miss Wait Cycles",
												HWCounterType::WAIT_CYCLES, 
												Profiler::Color::Tan,
												SCE_PERF_PMC_EVENT_MAB_WAIT_CYCLES, 0xB, 1);

	HWEventDescription BranchMisprediction("Branch Miss", 
											"Branch Misprediction Wait Cycles",
											HWCounterType::WAIT_CYCLES, 
											Profiler::Color::Coral,
											SCE_PERF_PMC_EVENT_RETIRED_MISPREDICTED_BRANCH_INSTRUCTIONS, 0, 14);
}

bool SetHWCounters(const rage::atArray<const HWEventDescription*>& counters, const atVector<ThreadEntry*>& threads)
{
	int resOpen = scePerfPmcOpen();
	if (resOpen != SCE_OK)
		return false;

	for (int i = 0; i < threads.GetCount(); ++i)
	{
		if (threads[i]->description.IsReal())
		{
			ScePthread threadID = (ScePthread)threads[i]->description.threadId;

			int resStop = scePerfPmcStop(threadID);
			if (resStop)
				return false;

			int resReset = scePerfPmcReset(threadID);
			if (resReset)
				return false;

			for (int counterIndex = 0; counterIndex < counters.GetCount(); ++counterIndex)
			{
				int res = scePerfPmcSelectEvent(threadID, counterIndex, counters[counterIndex]->code);
				if (res)
					return false;
			}

			int resStart = scePerfPmcStart(threadID);
			if (resStart)
				return false;
		}
	}

	return true;
}
#endif

#if ETW_SUPPORTED

#define XB1_PERF_PMC_EVENT_Reserved						0	//  Reserved.
#define XB1_PERF_PMC_EVENT_RetiredSseAvx				1	//  The number of SSE/AVX operations retired.
#define XB1_PERF_PMC_EVENT_DCacheAccesses				2	//  The number of accesses to the L1 data cache for load and store references.
#define XB1_PERF_PMC_EVENT_DCacheMisses					3	//  The number of L1 data cache references which miss the data cache.
#define XB1_PERF_PMC_EVENT_DCacheRefills				4	//  The number of L1 data cache refills satisfied from the L2 cache (and/or the northbridge).
#define XB1_PERF_PMC_EVENT_L1DTLBMissL2DTLBHit			5	//  The number of L1 data cache accesses that miss in the L1 DTLB and hit in the L2 DTLB.
#define XB1_PERF_PMC_EVENT_DtlbMiss						6	//  The number of L1 data cache accesses that miss in both the L1 and L2 DTLBs.
#define XB1_PERF_PMC_EVENT_L1DtlbHit					7	//  The number of L1 data cache accesses that hit in the L1 DTLB.
#define XB1_PERF_PMC_EVENT_L1ITLBMissL2ITLBHit			8	//  The number of instruction fetches that miss in the L1 ITLB but hit in the L2 ITLB.
#define XB1_PERF_PMC_EVENT_ItlbMiss						9	//  The number of instruction fetches that miss in the 4K ITLB and 2M ITLB.
#define XB1_PERF_PMC_EVENT_ItlbInstructionFetchHits		10	//  The number of instruction fetches that hit in the 4K ITLB and 2M ITLB.
#define XB1_PERF_PMC_EVENT_MisalignedAccess				11	//  The number of L1 data cache accesses that are misaligned. Misaligned accesses incur at least an extra cache access and an extra cycle of latency on reads.
#define XB1_PERF_PMC_EVENT_IneffectiveSWPrefetches		12	//  The number of software prefetches that do not cause an actual L1 data cache refill.
#define XB1_PERF_PMC_EVENT_CpuClkNotHalted				13	//  The number of clocks that the CPU is not in a halted state.
#define XB1_PERF_PMC_EVENT_RetiredInstructions			14	//  The number of instructions retired (execution completed and architectural state updated). This count includes exceptions and interrupts.
#define XB1_PERF_PMC_EVENT_BranchInstructions			15	//  The number of branch instructions retired. This includes all types of architectural control flow changes, including exceptions and interrupts.
#define XB1_PERF_PMC_EVENT_MispredictedBranch			16	//  The number of branch instructions retired, of any type, that were not correctly predicted in either target or direction. This includes those for which prediction is not attempted (far control transfers, exceptions and interrupts), and excludes resyncs.
#define XB1_PERF_PMC_EVENT_RetiredTakenBranch			17	//  The number of taken branches that were retired. This includes all types of architectural control flow changes, including exceptions and interrupts, and excludes resyncs.
#define XB1_PERF_PMC_EVENT_RetiredFarSyscall			18	//  The number of far syscalls retired.
#define XB1_PERF_PMC_EVENT_RetiredNearReturns			19	//  The number of near return instructions retired.
#define XB1_PERF_PMC_EVENT_RetiredReturnsMispredicted	20	//  A near return instruction was retired that mispredicted in either target or direction.
#define XB1_PERF_PMC_EVENT_MispredictedTakenBranch		21	//  A taken branch instruction was retired that mispredicted in target address (but not in direction).
#define XB1_PERF_PMC_EVENT_MmxFPInstructions			22	//  A floating point (x87, MMX, or SSE) instruction was retired.
#define XB1_PERF_PMC_EVENT_RetiredFPInstructions		23	//  The number of SSE/AVX operations retired.
#define XB1_PERF_PMC_EVENT_RetiredMmxInstructions		24	//  The number of MMX operations retired.
#define XB1_PERF_PMC_EVENT_RetiredSseInstructions		25	//  The number of SSE operations retired.
#define XB1_PERF_PMC_EVENT_InstructionFetchStalls		26	//  The number cycles that the instruction fetch engine is stalled.
#define XB1_PERF_PMC_EVENT_DataCachePrefetches			27	//  L1 data cache prefetches.
#define XB1_PERF_PMC_EVENT_DataCacheReadSize			28	//  The number of L1 data cache reads.
#define XB1_PERF_PMC_EVENT_DataCacheWriteSize			29	//  The number of L1 data cache writes.
#define XB1_PERF_PMC_EVENT_ITLBReloadStalls				30	//  The number of cycles when the fetch engine is stalled for an ITLB reload.


namespace Perf
{
	HWEventDescription DataCacheAccesses("DCacheAccesses",
		"The number of accesses to the L1 data cache for load and store references.",
		HWCounterType::DEFAULT,
		Profiler::Color::LimeGreen,
		XB1_PERF_PMC_EVENT_DCacheAccesses, 0, 1);

	HWEventDescription DataCacheMisses("DCacheMisses",
		"The number of L1 data cache references which miss the data cache.",
		HWCounterType::DEFAULT,
		Profiler::Color::Gold,
		XB1_PERF_PMC_EVENT_DCacheMisses, 0, 1);

	HWEventDescription DtlbMiss("DtlbMiss",
		"The number of L1 data cache accesses that miss in both the L1 and L2 DTLBs.",
		HWCounterType::DEFAULT,
		Profiler::Color::Gold,
		XB1_PERF_PMC_EVENT_DtlbMiss, 0, 1);

	HWEventDescription ItlbMiss("ItlbMiss",
		"The number of instruction fetches that miss in the 4K ITLB and 2M ITLB.",
		HWCounterType::DEFAULT,
		Profiler::Color::Tan,
		XB1_PERF_PMC_EVENT_ItlbMiss, 0, 1);
}

rage::u32 GetHWCounterCode(const rage::atArray<const HWEventDescription*>& counters, int index)
{
	return index < counters.GetCount() ? (u32)counters[index]->code : 0;
}

bool SetHWCounters(const rage::atArray<const HWEventDescription*>& counters)
{
#if RSG_DURANGO
	return S_OK == ConfigurePMCs(
		GetHWCounterCode(counters, 0), 
		GetHWCounterCode(counters, 1), 
		GetHWCounterCode(counters, 2), 
		GetHWCounterCode(counters, 3)
	);
#else
	// VS TODO: This API is not supported yet on PC
	//			Could be potentially added with one of the next Windows SDK
	(void)counters;
	return false;
#endif
}

#endif


void GetHWCounters(rage::u64 counters[ROCKY_HARDWARE_EVENT_COUNT])
{
	(void)counters;
#if RSG_DURANGO || (defined(RSG_GDK) && RSG_GDK)
	if (IsCollectingTags())
	{
#if RSG_CLANG
		// GDK Clang can't link readpmc so we can't call GetPMCValue
		// as of GDKX 210600
		counters[0] = 0;
		counters[1] = 0;
		counters[2] = 0;
		counters[3] = 0;
#else
		GetPMCValue(0, &counters[0]);
		GetPMCValue(1, &counters[1]);
		GetPMCValue(2, &counters[2]);
		GetPMCValue(3, &counters[3]);
#endif
	}
#endif
}

#if (RSG_PC)
void EventTracerProcessor::AdjustPrivileges()
{
	// No privilege? Ok, let's add the privilege!
	HANDLE token = 0;
	if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &token))
	{
		TOKEN_PRIVILEGES tokenPrivileges;
		sysMemSet(&tokenPrivileges, 0, sizeof(tokenPrivileges));
		tokenPrivileges.PrivilegeCount = 1;
		tokenPrivileges.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		LookupPrivilegeValue(NULL, SE_SYSTEM_PROFILE_NAME, &tokenPrivileges.Privileges[0].Luid);

		AdjustTokenPrivileges(token, FALSE, &tokenPrivileges, 0, (PTOKEN_PRIVILEGES)NULL, 0);
		CloseHandle(token);
	}
}
#endif


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if ETW_SUPPORTED
const int START_TRACE_ATTEMPTS_NUMBER = 6;
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
EventTracer::Status EventTracerProcessor::Start(EventTracer::Mode mode)
{
	const int supportedModes = (EventTracer::MODE_SWITCH_CONTEXT | EventTracer::MODE_AUTOSAMPLING | EventTracer::MODE_SAMPLING | EventTracer::MODE_HW_EVENTS | EventTracer::MODE_PAGE_FAULTS);

	if (currentMode == EventTracer::MODE_NONE && (mode & supportedModes) != 0)
	{
		USE_ROCKY_MEMORY;
		bool needToCreateWorkerThread = false;

#if RSG_PC
		AdjustPrivileges();
#endif

#if ETW_SUPPORTED
		u32 kernelSessionFlags = 0;

		if (mode & (EventTracer::MODE_PROCESS))
		{
			kernelSessionFlags |= EVENT_TRACE_FLAG_PROCESS;
			kernelSessionFlags |= EVENT_TRACE_FLAG_THREAD;
		}

		if (mode & (EventTracer::MODE_SWITCH_CONTEXT | EventTracer::MODE_AUTOSAMPLING | EventTracer::MODE_HW_EVENTS | EventTracer::MODE_PAGE_FAULTS))
			needToCreateWorkerThread = true;

		int collectCallstacks = mode & (EventTracer::MODE_AUTOSAMPLING | EventTracer::MODE_SAMPLING | EventTracer::MODE_HW_EVENTS);

		if (mode & EventTracer::MODE_SWITCH_CONTEXT)
			kernelSessionFlags |= EVENT_TRACE_FLAG_CSWITCH;

		if (mode & (EventTracer::MODE_PAGE_FAULTS))
			kernelSessionFlags |= EVENT_TRACE_FLAG_MEMORY_HARD_FAULTS;

		if (mode & (EventTracer::MODE_SYS_CALLS))
			kernelSessionFlags |= EVENT_TRACE_FLAG_SYSTEMCALL;

		if (collectCallstacks)
			kernelSessionFlags |= EVENT_TRACE_FLAG_PROFILE;

		etwSession = EtwSession::CreateKernelSession(kernelSessionFlags);
		if (!etwSession)
			return EventTracer::Status::ETW_FAILED;

		atArray<EtwClassicEventId> sampleIDs;

		if (collectCallstacks)
		{
			EtwClassicEventId sampleEventID;
			sampleEventID.EventGuid = SampledProfileGuid;
			sampleEventID.Type = SampledProfile::OPCODE;
			sampleIDs.PushAndGrow(sampleEventID);

			EtwSession::SetKernelSamplingFrequency(EventTracer::GetSamplingFrequency());
		}

		if (mode & (EventTracer::MODE_PAGE_FAULTS))
		{
			EtwClassicEventId pageFaultEventID;
			pageFaultEventID.EventGuid = PageFault_V2::GUID;
			pageFaultEventID.Type = PageFault_HardFault::OPCODE;
			sampleIDs.PushAndGrow(pageFaultEventID);
		}

		if (mode & (EventTracer::MODE_SYS_CALLS))
		{
			EtwClassicEventId syscallEventID;
			syscallEventID.EventGuid = SampledProfileGuid;
			syscallEventID.Type = SysCallEnter::OPCODE;
			sampleIDs.PushAndGrow(syscallEventID);
		}

		if (mode & (EventTracer::MODE_SWITCH_CONTEXT_CALLSTACKS))
		{
			EtwClassicEventId switchEventID;
			switchEventID.EventGuid = Thread_TypeGroup1::GUID;
			switchEventID.Type = CSwitch::OPCODE;
			sampleIDs.PushAndGrow(switchEventID);
		}

		if (!sampleIDs.empty())
			etwSession->EnableStackTrace(sampleIDs.GetElements(), sampleIDs.GetCount());

	#if RSG_DURANGO
		if ((mode & EventTracer::MODE_HW_EVENTS) && (mode & EventTracer::MODE_LIVE))
		{
			const HWEventDescription* hwEvents[] = {
				&Perf::DataCacheAccesses,
				&Perf::DataCacheMisses,
				&Perf::DtlbMiss,
				&Perf::ItlbMiss,
			};

			atArray<const HWEventDescription*>& descs = EventTracer::Get().hardwareEventDescriptions;
			descs.Reset();

			for (int i = 0; i < NELEM(hwEvents); ++i)
				descs.PushAndGrow(hwEvents[i]);

			SetHWCounters(descs);
		}
	#endif

		for (const ThreadEntry* entry : Core::Get().GetThreads())
		{
			rage::u64 threadID = entry->description.systemThreadId;
			if (threadID != (rage::u64)sysIpcThreadIdInvalid)
				activeThreads.insert(threadID);
		}

		activeCores.SetZero();

		mainThread = (rage::u64)sysIpcGetSystemThreadId();

		etwSession->Start(OnRecordEvent);
#endif

#if RSG_SCE
		threads = Core::Get().GetThreads();

		int ret = SCE_OK;
		needToCreateWorkerThread = true;
		workerFinishEvent = rage::sysIpcCreateEvent();

		sessionPID = getpid();

		sessionTempBuffer = RockyAllocate(MAIN_TRACE_BUF_SIZE);
		if (sessionTempBuffer == nullptr)
			return EventTracer::Status::ETW_FAILED;

		sysMemSet(sessionTempBuffer, 0, MAIN_TRACE_BUF_SIZE);

#if RSG_PROSPERO
		ret = sceSysmoduleLoadModule(SCE_SYSMODULE_PERF);
		if (ret != SCE_OK)
			return EventTracer::Status::SCE_CREATE_FAILED;
#endif

		// Main trace setup
		ret = scePerfTraceCreate(MAIN_TRACE_BUF_SIZE, &sessionTraceID, &sessionTraceBuffer);
		if (ret != SCE_OK)
			return EventTracer::Status::SCE_CREATE_FAILED;

		ret = scePerfTraceEnable(sessionTraceID, SCE_PERF_TRACE_NAME_SCHEDULER, 0);
		if (ret != SCE_OK)
			return EventTracer::Status::SCE_START_FAILED;

		if (mode & EventTracer::MODE_SWITCH_CONTEXT)
		{
			ret = scePerfTraceEnable(sessionTraceID, SCE_PERF_TRACE_SCHEDULER, 0);
			if (ret != SCE_OK)
				return EventTracer::Status::SCE_ENABLE_SWITCH_CONTEXT_FAILED;
		}

		if (mode & (EventTracer::MODE_SAMPLING | EventTracer::MODE_AUTOSAMPLING | EventTracer::MODE_HW_EVENTS))
		{
			u8 backtrace = 0; // OFF
			int sampleRate = EventTracer::GetSamplingFrequency();
			if (mode & EventTracer::MODE_SAMPLING)
				sampleRate = PC_SAMPLE_RATE;
			backtrace = MAX_CALLSTACK_DEPTH - 1;

			u8 pmcMask = 0x0;

			atArray<const HWEventDescription*>& descs = EventTracer::Get().hardwareEventDescriptions;
			descs.Reset();

			if (mode & EventTracer::MODE_HW_EVENTS)
			{
				const HWEventDescription* defaultEvents[] = {
					&Perf::CPUClocksNotIdle,
					&Perf::InstructionCacheMissWaitCycles,
					&Perf::DataCacheMissWaitCycles,
					&Perf::BranchMisprediction,
				};

				for (int i = 0; i < NELEM(defaultEvents); ++i)
					descs.PushAndGrow(defaultEvents[i]);

				SetHWCounters(descs, threads);

				if ((mode & EventTracer::MODE_LIVE) == 0)
					pmcMask = (1 << descs.GetCount()) - 1;
			}

			// DLH
			// Need to explicitly init here due to compiler wError in Prospero 2.00
			// error : nested designators are a C99 extension [-Werror,-Wc99-designator]
			ScePerfTraceEnableMode pcSampleMode;
			pcSampleMode.pcmode.frequency = sampleRate;
			pcSampleMode.pcmode.backtrace = backtrace;
			pcSampleMode.pcmode.pmc_mask = pmcMask;

			ret = scePerfTraceEnable(sessionTraceID, SCE_PERF_TRACE_PC_SAMPLING, pcSampleMode.mode);
			if (ret != SCE_OK)
				return EventTracer::Status::SCE_ENABLE_SAMPLING_FAILED;
		}

		// Start traces
		ret = scePerfTraceStart(sessionTraceID, SCE_PERF_TRACE_OVERWRITE);
		if (ret != SCE_OK)
			return EventTracer::Status::SCE_START_FAILED;
#endif

#if RSG_LINUX
		FTrace& tracer = FTrace::Get();

		if (mode & EventTracer::MODE_SWITCH_CONTEXT)
			tracer.EnableEvent(ftrace::EVENT_SCHED_SWITCH);
		
		g_pidCache.Reset();
		tracer.Start();
#endif

		currentMode = mode;

		if (needToCreateWorkerThread)
		{
			rage::sysMemAutoUseAllocator allocator(rage::sysMemAllocator::GetMaster());
			processThreadHandle = sysIpcCreateThread(ProcessEvents, this, 16 * 1024, rage::PRIO_LOWEST, "[Rocky] EventTracer");
		}
	}

	return EventTracer::Status::OK;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool EventTracerProcessor::Stop()
{
	if (currentMode != EventTracer::MODE_NONE)
	{
		USE_ROCKY_MEMORY;
		bool result = true;

	#if ETW_SUPPORTED
		if (etwSession)
		{
			etwSession->Flush();
			etwSession->Destroy();
			etwSession = nullptr;
		}
		activeThreads.reset();
	#endif

	#if RSG_SCE
		sysIpcSetEvent(workerFinishEvent);
		result &= sysIpcWaitThreadExit(processThreadHandle);
		processThreadHandle = sysIpcThreadIdInvalid;
		sysIpcDeleteEvent(workerFinishEvent);
		workerFinishEvent = 0;

		// Trace Shutdown
		int ret = scePerfTraceStop(sessionTraceID);
		if (!rockyVerifyf(ret == SCE_OK, "scePerfTraceStop failed"))
			result = false;
		
		ret = scePerfTraceDelete(sessionTraceID);
		if (!rockyVerifyf(ret == SCE_OK, "scePerfTraceDelete failed"))
			result = false;

		RockyFree(sessionTempBuffer);
		sessionTempBuffer = 0;
		sessionTraceID = 0;
	#endif

	#if RSG_LINUX
		FTrace::Get().Stop();
		g_pidCache.Reset();
	#endif

		if (processThreadHandle != sysIpcThreadIdInvalid)
		{
			result &= sysIpcWaitThreadExit(processThreadHandle);
			processThreadHandle = sysIpcThreadIdInvalid;
		}

		activeThreads.clear();
		currentMode = EventTracer::MODE_NONE;
		
		return true;
	}

	return false;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool EventTracerProcessor::Flush(Mode::Type mode, EventTime scope)
{
	if (mode == Mode::OFF)
		return false;

	bool result = true;

#if RSG_LINUX
	TimeScope ftraceScope = { (s64)Timestamp::Unpack(scope.start), (s64)Timestamp::Unpack(scope.finish) };
	result = FTrace::Get().Parse(OnRecordEvent, ftraceScope);
#endif

	(void)scope;

	return result;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
EventTracerProcessor::~EventTracerProcessor()
{
	Stop();
	Flush(Mode::OFF);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if RSG_SCE
int EventTracerProcessor::ParseTrace(void* pBuffer, size_t bufSize)
{
	if (pBuffer == 0 || bufSize == 0)
		return -1;

	uint32_t* pTraceBufferStart	= (uint32_t*)pBuffer;
	uint32_t* pTraceBufferNext	= pTraceBufferStart;
	uint32_t* pTraceBufferEnd	= pTraceBufferStart + bufSize / sizeof(uint32_t);

	if (pTraceBufferStart >= pTraceBufferEnd)
		return -1;

	// Get the time of the first packet
	ScePerfTraceHeader* pPkt = (ScePerfTraceHeader*)pTraceBufferNext;

	while (pTraceBufferNext < pTraceBufferEnd)
	{
		pPkt = (ScePerfTraceHeader*)pTraceBufferNext;
		pTraceBufferNext = pTraceBufferNext + (pPkt->size / sizeof(uint32_t));

		if (pTraceBufferNext > pTraceBufferEnd)
			return -1;

		if( pPkt->size == 0 || pPkt->size % sizeof(uint32_t) != 0)
			return -1;

		switch (pPkt->type)
		{
		case SCE_PERF_TRACE_PC_SAMPLING:
			{
				const ScePerfTracePcSampleEvent* pPcSamplePkt = (ScePerfTracePcSampleEvent*)pPkt;

				rage::u64 threadID = pPcSamplePkt->header.id;
				rage::u64 timestamp = pPcSamplePkt->header.time;

				if (pPcSamplePkt->pid == sessionPID)
				{
					// Callstack
					if (pPcSamplePkt->number_backtrace > 0)
					{
						rage::u8 count = pPcSamplePkt->number_backtrace;

						if (count > 0)
						{
							OnStackWalkEvent(threadID, timestamp, (rage::u64*)&pPcSamplePkt->rip, count + 1, Mode::AUTOSAMPLING);
						}
					}

					// Hardware Counters
					if (pPcSamplePkt->number_perf_counters)
					{
						OnHardwareEvent(threadID, timestamp, &pPcSamplePkt->perf_counter[0], pPcSamplePkt->number_perf_counters);
					}
				}
			}
			break;


		case SCE_PERF_TRACE_SCHEDULER:
			{
				const ScePerfTraceSchedulerEvent* pSchedulerPkt = (ScePerfTraceSchedulerEvent*)pPkt;

				OnSwitchContextEvent(pSchedulerPkt->header.id,
									 pSchedulerPkt->tid_on,
									 pSchedulerPkt->header.time,
									 pSchedulerPkt->active_priority_off,
									 pSchedulerPkt->active_priority_on,
									 (rage::u8)(pSchedulerPkt->incident),
									 pSchedulerPkt->cpu_id);
			}
			break;

		case SCE_PERF_TRACE_NAME_SCHEDULER:
			{
				const ScePerfTraceNameSchedulerEvent* pSchedulerPkt = (ScePerfTraceNameSchedulerEvent*)pPkt;

				ThreadDescription threadDesc(pSchedulerPkt->szName);
				threadDesc.systemThreadId = pSchedulerPkt->id;
				Core::Get().RegisterThreadDescription(threadDesc);
			}
			break;

		default:
			break;
		}
	}

	return SCE_OK;
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const SwitchContextData& ob)
{
	return stream << ob.timestamp << ob.oldThreadID << ob.newThreadID << ob.cpuID << ob.reason << ob.oldThreadPriority << ob.newThreadPriority;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const HardwareEventData& ob)
{
	stream.Write((u8*)&ob, sizeof(ob));
	return stream;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const HWEventDescription& ob)
{
	return stream << ob.name << ob.description << ob.color << ob.counterMask << ob.code << ob.cycleMultiplier ;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
EventTracer::Status EventTracer::Start(EventTracer::Mode mode)
{
	return gEventTracerProcessor.Start(mode);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool EventTracer::Stop()
{
	return gEventTracerProcessor.Stop();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool EventTracer::Flush(Profiler::Mode::Type mode, EventTime scope)
{
	return gEventTracerProcessor.Flush(mode, scope);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Profiler::EventTracer::Mode EventTracer::GetCurrentMode() const
{
	return gEventTracerProcessor.currentMode;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PageFaultStats EventTracer::GetPageFaultStats() const
{
	return gEventTracerProcessor.pageFaultStats;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void LiveEventData::StartLive()
{
#if RSG_SCE
	for (int i = 0; i < ROCKY_HARDWARE_EVENT_COUNT; ++i)
		hwEvents[i] = scePerfPmcGetCounterSelf(i);
#endif
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void LiveEventData::StopLive()
{
#if RSG_SCE
	for (int i = 0; i < ROCKY_HARDWARE_EVENT_COUNT; ++i)
		hwEvents[i] = scePerfPmcGetCounterSelf(i) - hwEvents[i];
#endif
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
EventTracer& EventTracer::Get()
{
	static EventTracer controller;
	return controller;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int EventTracer::GetSamplingFrequency()
{
	int sampleRate = Core::Get().GetSamplingFrequency();
	PARAM_rockySamplingFrequency.Get(sampleRate);
	return sampleRate;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void EventTracer::Clear(bool preserveMemory)
{
	switchContextEvents.Clear(preserveMemory);
	gpuSwitchContextEvents.Clear(preserveMemory);
	stackwalkEvents.Clear(preserveMemory);
	hardwareEvents.Clear(preserveMemory);
	pageFaultEvents.Clear(preserveMemory);

	for (int i = 0; i < LIVE_EVENT_BUFFER_COUNT; ++i)
		liveSwitchContextEvents[i].Clear(preserveMemory);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER
