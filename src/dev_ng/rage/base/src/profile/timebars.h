//
// profile/timebars.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#ifndef PROFILE_TIMEBARS_H
#define PROFILE_TIMEBARS_H

// Rage headers
#include "atl/array.h"
#include "atl/map.h"
#include "profile/telemetry.h"
#include "system/criticalsection.h"
#include "system/timer.h"

namespace rage {

#define RAGE_TIMEBARS	((!__FINAL || __FINAL_LOGGING) && !__PROFILE && !__TOOL)

#if RAGE_TIMEBARS
#define RAGE_TIMEBARS_ONLY(x)	x
#else
#define RAGE_TIMEBARS_ONLY(x)
#endif 

#if RAGE_TIMEBARS

#define INCLUDE_DETAIL_TIMEBARS		(__DEV || __BANK)			// Omit minor timebars outside of Debug and Beta
#if INCLUDE_DETAIL_TIMEBARS
#define DETAIL_TIMEBARS_ONLY(x)	x
#else
#define DETAIL_TIMEBARS_ONLY(x)
#endif 

// These are all callback functions for manipulating timebars. RageCore callbacks are all no-ops, but RageGraphics will install the "real" versions of the functions
namespace pfTimebarFuncs {
	typedef void (*StartCb)				(uptr /*dummy*/, const char* pName, const char * file, unsigned int line, bool detail, float budget, ProfileZoneType zoneType);
	typedef void (*PushCb)				(uptr /*dummy*/, const char* pName, const char * file, unsigned int line, bool detail, float budget, ProfileZoneType zoneType);
	typedef void (*PopCb)				(uptr /*dummy*/, int detail);

	typedef void (*StartStartupBarCb)	(uptr /*dummy*/, const char *pName, const char * file, unsigned int line, bool detail, float budget, ProfileZoneType zoneType);
	typedef void (*PushStartupBarCb)	(uptr /*dummy*/, const char *pName, const char * file, unsigned int line, bool detail, float budget, ProfileZoneType zoneType);
	typedef void (*PopStartupBarCb)		(uptr /*dummy*/, int detail);

	typedef void (*PushMarkerCb)		(const char* pName, int id);
	typedef void (*PopMarkerCb)			(int id);

	extern StartCb				g_StartCb;
	extern PushCb				g_PushCb;
	extern PopCb				g_PopCb;
	extern StartStartupBarCb	g_StartStartupBarCb;
	extern PushStartupBarCb		g_PushStartupBarCb;
	extern PopStartupBarCb		g_PopStartupBarCb;
	extern PushMarkerCb			g_PushMarkerCb;
	extern PopMarkerCb			g_PopMarkerCb;
}

class	pfAutoPushTimer
{
public:
	pfAutoPushTimer(const char* name, bool detail, float budget = 0.0f ) : m_Detail(detail) {::rage::pfTimebarFuncs::g_PushCb(0, name, __FILE__, __LINE__, detail, budget, PZONE_NORMAL); }
	~pfAutoPushTimer() {::rage::pfTimebarFuncs::g_PopCb(0, m_Detail ? 1 : 0); }

	bool m_Detail;
};

class	pfAutoMarker
{
public:
	pfAutoMarker(const char* name, int id) : m_Id(id) { ::rage::pfTimebarFuncs::g_PushMarkerCb(name, id); }
	~pfAutoMarker() { ::rage::pfTimebarFuncs::g_PopMarkerCb(m_Id); }

private:
	int m_Id;
};


#define PF_START_TIMEBAR(name)						::rage::pfTimebarFuncs::g_StartCb(0, name, __FILE__, __LINE__, false, 0.0f, PZONE_NORMAL)
#define PF_START_TIMEBAR_IDLE(name)					::rage::pfTimebarFuncs::g_StartCb(0, name, __FILE__, __LINE__, false, 0.0f, PZONE_IDLE)
#define PF_START_TIMEBAR_BUDGETED(name, budget)		::rage::pfTimebarFuncs::g_StartCb(0, name, __FILE__, __LINE__, false, budget, PZONE_NORMAL)
#define PF_PUSH_TIMEBAR(name)						::rage::pfTimebarFuncs::g_PushCb(0, name, __FILE__, __LINE__, false, 0.0f, PZONE_NORMAL)
#define PF_PUSH_TIMEBAR_BUDGETED(name, budget)		::rage::pfTimebarFuncs::g_PushCb(0, name, __FILE__, __LINE__, false, budget, PZONE_NORMAL)
#define PF_PUSH_TIMEBAR_IDLE(name)					::rage::pfTimebarFuncs::g_PushCb(0, name, __FILE__, __LINE__, false, 0.0f, PZONE_IDLE)
#define PF_POP_TIMEBAR()							::rage::pfTimebarFuncs::g_PopCb(0, 0)
#define PF_AUTO_PUSH_TIMEBAR(name)					::rage::pfAutoPushTimer localPushTimer(name,false)
#define PF_AUTO_PUSH_TIMEBAR_BUDGETED(name, budget)	::rage::pfAutoPushTimer localPushTimer(name,false, budget)

#define PF_START_STARTUPBAR(name)					::rage::pfTimebarFuncs::g_StartStartupBarCb(0, name, __FILE__, __LINE__, false, 0.0f, PZONE_NORMAL)
#define PF_PUSH_STARTUPBAR(name)					::rage::pfTimebarFuncs::g_PushStartupBarCb(0, name, __FILE__, __LINE__, false, 0.0f, PZONE_NORMAL)
#define PF_POP_STARTUPBAR()							::rage::pfTimebarFuncs::g_PopStartupBarCb(0, 0)

#if INCLUDE_DETAIL_TIMEBARS
#define PF_PUSH_TIMEBAR_DETAIL(name)				::rage::pfTimebarFuncs::g_PushCb(0, name, __FILE__, __LINE__, true, 0.0f, PZONE_NORMAL)
#define PF_START_TIMEBAR_DETAIL(name)				::rage::pfTimebarFuncs::g_StartCb(0, name, __FILE__, __LINE__, true, 0.0f, PZONE_NORMAL)
#define PF_POP_TIMEBAR_DETAIL()						::rage::pfTimebarFuncs::g_PopCb(0, 1)
#define PF_AUTO_PUSH_DETAIL(name)					::rage::pfAutoPushTimer localPushTimer(name,true)
#else // INCLUDE_DETAIL_TIMEBARS
#define PF_PUSH_TIMEBAR_DETAIL(name)
#define PF_START_TIMEBAR_DETAIL(name)
#define PF_POP_TIMEBAR_DETAIL()
#define PF_AUTO_PUSH_DETAIL(name)
#endif // INCLUDE_DETAIL_TIMEBARS

#else // !RAGE_TIMEBARS

#define PF_START_TIMEBAR(name)
#define PF_START_TIMEBAR_IDLE(name)
#define PF_START_TIMEBAR_BUDGETED(name, budget)
#define PF_START_TIMEBAR_DETAIL(name)
#define PF_PUSH_TIMEBAR(name)
#define PF_PUSH_TIMEBAR_BUDGETED(name, budget)
#define PF_PUSH_TIMEBAR_IDLE(name)
#define PF_PUSH_TIMEBAR_DETAIL(name)
#define PF_POP_TIMEBAR()	
#define PF_POP_TIMEBAR_DETAIL()	
#define PF_AUTO_PUSH_TIMEBAR(name)
#define PF_AUTO_PUSH_TIMEBAR_BUDGETED(name, budget)
#define PF_AUTO_PUSH_DETAIL(name)

#define PF_START_STARTUPBAR(name)					
#define PF_PUSH_STARTUPBAR(name)					
#define PF_POP_STARTUPBAR()		

#endif	// RAGE_TIMEBARS

#if __PS3
#if !__FINAL
	PS3_ONLY(void pfDummySync() __attribute__((noinline));)
#else
	inline void pfDummySync() {}
#endif
#endif // __PS3

} // namespace rage


#endif //! INC_TIMEBARS_H_
