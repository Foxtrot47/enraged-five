//
// profile/profiler.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

//
// The profiler class is the static interface to the
// profile system.
//

#ifndef PROFILE_PROFILER_H
#define PROFILE_PROFILER_H


//==============================================================
// external defines

#include "atl/bintree.h"
#include "diag/stats.h"
#include "data/base.h"
#include "grcore/viewport.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"

namespace rage {

#if !__STATS

#define PF_STATS_VIEW(x)									// empty macro for !__STATS

// empty profiler class to obviate #if __STATS checks
class pfProfiler
{
public:
	static void Init (bool =true)							{ }
	static void Shutdown ()									{ }
	static void Reset ()									{ }
};

#else // __STATS

class pfEKGMgr;
class pfStatsView;


//=============================================================================
// pfProfiler
// PURPOSE
//   Manages profile groups (which in turn contain pages).
class pfProfiler : public datBase
{
public:
	// PURPOSE: constructor
	pfProfiler ();

	// PURPOSE: destructor
	~pfProfiler ();

	// all post static declaration initialization
	void Init (bool createWidgets=true);

	// undo things done in Init
	void Shutdown ();

	// register a new group
	void RegisterGroup (pfGroup & group);

	// register a page (collection of groups)
	void RegisterPage (pfPageNode & pageNode);

	// unregister a page
	void UnregisterPage (pfPageNode & pageNode);

	// flip elapsed times at the end of the frame
	void FlipElapsedTimes();

	bool IsInitialized() { return m_Initialized; }

	// reset all the elements and groups
	void Reset ();

	// return the next active page (NULL if end, first if NULL)
	pfPage * GetNextPage (const pfPage * curPage, bool onlyActivePages = true);

	// return the previous active page (NULL if start, last if NULL)
	pfPage * GetPrevPage (const pfPage * curPage, bool onlyActivePages = true);

	// is the profiler enabled?
	bool m_Enabled;

	pfTimer* FindTimer(const char* pageName, const char* groupName, const char* timerName);

	pfValue* FindValue(const char* pageName, const char* groupName, const char* valueName);

	struct CustomElements
	{
		atArray<pfTimer*>* m_Timers;
		atArray<pfValue*>* m_Values;
	};

	CustomElements LoadCustomElementList(const char* filename);

#if __BANK
	// add the groups, and elements to the bank (part of Init)
	void AddCreateWidgetButton(bkBank& bank);
	void AddWidgets (bkBank& bank);
	atDelegate<void (bkBank&)> m_OnCreateWidgets;
#endif

	// return number of profiler groups
	int GetNumGroups() { return m_NumGroups; }
	pfGroup* GetGroup(int i) { return m_Groups[i]; }

	// get profiler page by string name, or NULL if it doesn't exist.
	pfPage * GetPageByName (const char * pageName);

	// PURPOSE: Old-system text "stats" pages of the profiler pages
	enum {kMaxStatsViews = (RSG_PC || RSG_DURANGO || RSG_ORBIS) ? 255 : 160 };

protected:
	// PURPOSE: Has the profiler been initialized?  widgets added, views registered, etc.
	bool m_Initialized;

	// PURPOSE: Binary tree to store all pages (sorted by name)
	atBinTree<const char *, pfPage *> m_PageTree;

	// PURPOSE: Maximum number of groups managed by the profiler (groups contain sets of elements (timers/values))
	enum {kMaxGroups = 255};

	// PURPOSE: Actual number of groups
	int m_NumGroups;

	// PURPOSE: Pointers to the groups
	atRangeArray<pfGroup *, kMaxGroups> m_Groups;

#if __BANK
	bkBank * m_Bank;
#endif

private:
	ASSERT_ONLY(static	int ms_GroupOverflowCount);
};


#endif	// __STATS


//==============================================================
// GetRageProfiler
//
// PURPOSE:
//	This function simply makes available the static profiler, and,
//	by using a function instead of a static variable directly,
//	assures that the profiler is initialized before any use.
//	See C++ FAQ Lite: 10.13
//

pfProfiler & GetRageProfiler();

}	// namespace rage
#endif  // PROFILE_PROFILER_H
