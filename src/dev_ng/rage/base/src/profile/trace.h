//// profile/trace.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef PROFILE_TRACE_H
#define PROFILE_TRACE_H

#define RAGETRACE (!__FINAL && !__PROFILE && !__64BIT && !__WIN32PC && __DEV )

#if RAGETRACE && !__SPU
#include "atl/array.h"
#include "atl/map.h"
#include "system/timer.h"
#include "data/base.h"
#include "vector/color32.h"
#include "system/ipc.h"
#include "system/criticalsection.h"

namespace rage {

class bkBank;
class pfTrace;
class pfVertexBuffer;
class pfTraceCounterId;
class fiStream;
class pfTraceInitialiser;

class pfTraceManager : public datBase
{
public:	
	void				Init();
	void				Shutdown();

	void				SetFrameStart();
	void				AddInitialiser(pfTraceInitialiser* init);
	void				RemoveInitialiser(pfTraceInitialiser* init);
	void				AddTrace(pfTrace* trace);
	void				AddCounter(pfTraceCounterId* counter);
	void				RemoveCounter(pfTraceCounterId* counter);
	void				RemoveTrace(pfTrace* trace);
	void				Update();
	pfTraceCounterId&	IdFromName(const char* name);
	Color32				BarColor(u32 i) const;
	const char*			BarColorName(u32 i) const;
	void				RenderBar(u32 start, u32 end, Color32 id, u32 layer = 0);
	void				RenderQuad(float x1, float y1, float x2, float y2, Color32 color);
	u32					Frame() const {return m_FrameCount;}
	u32					SampleFrames() const {return m_SampleFrames;}
	bool				ShowWholeRun() const {return m_ShowWholeRun;}
	void				Reset();
	void				WriteToFile();

private:
	friend pfTraceManager& RageTrace();
	pfTraceManager();

	bool				m_Initialised;
	u32					m_InitCountDown;
	bool				m_EnableToggle;
	bool				m_Enabled;
	bool				m_EnableCellScheduler;
	bool				m_EnableSpursTrace;
	bool				m_EnableGcmTrace;
	pfVertexBuffer*		m_VtxBuf;
	u32					m_FsQPosRead;
	u32					m_FsQPosWrite;
	u32					m_FsQ[8];
	u32					m_FrameStart;
	u32					m_FrameEnd;
	u32					m_Row;
	u32					m_NumFrames;
	float				m_Scale;
	u64					m_ScaleTimeStart;
	u32					m_FrameCount;
	bool				m_ShowLegend;
	u32					m_ShowTopCounters;
	pfTraceCounterId*	m_ActiveTopCounter;
	bool				m_ShowWholeRun;		
	u32					m_SampleFrames;
	sysCriticalSectionToken	m_NameCs;
	sysCriticalSectionToken m_Cs;

	atArray<pfTrace*>	m_Traces;	
	atArray<pfTraceInitialiser*> m_Inits;	
	atArray<pfTraceCounterId*>	m_Counters;
#if __BANK
	void				AddWidgets(bkBank& bank);
	bkBank*				m_Bank;
#endif
};

pfTraceManager& RageTrace();

//////////////////////////////////////////////////////////////////////////

class pfTraceCounter;

class pfTrace
{
public:
	pfTrace(const char* name, pfTraceCounter* root = 0);
	virtual ~pfTrace();

	virtual void	BeginFrame();
	virtual void	EndFrame(u32 timebase);
	void			Reset();
	virtual u32		GetNextSample() = 0;
	virtual void	AdvanceTo(u32 timebase);
	u32				NumRows() const {return m_NumRows;}
	const char*		Name() const {return m_Name;}
	virtual void	RenderBar(u32 endtime);
	void			DisplayLegend(float y);
	void			WriteToFile(fiStream* fh);
	pfTraceCounter* Root() const {return m_Root;}

protected:
	virtual void	OnEndFrame() {}
	void			PushCounter(u32 timebase, pfTraceCounterId* id);
	void			PopCounter(u32 timebase);

	char			m_Name[64];
	u32				m_NumRows;
	pfTraceCounter*	m_Root;
	pfTraceCounter*	m_ActiveCounter;
	pfTraceCounter*	m_DisplayRoot;
	pfTraceCounter*	m_CursorItem;
	u32				m_Frame;
};

class pfTraceInitialiser
{
public:
	virtual ~pfTraceInitialiser() {}
	virtual void Enable() = 0;
	virtual void Disable() = 0;
};

//////////////////////////////////////////////////////////////////////////
// stat counter for tracking mean & standard deviation
// useful percentile stuff for normally distributed data

class pfStatCounter
{
public:
	pfStatCounter() {Reset();}
	void Reset()
	{
		m_NumSamples = 0;
		m_Sum = m_SumOfSquares = 0.0;
	}
	void Add(double sample)
	{
		++m_NumSamples;
		m_Sum += sample;
		m_SumOfSquares += sample * sample;
	}
	void Add(const pfStatCounter& rhs)
	{
		m_NumSamples += rhs.m_NumSamples;
		m_Sum += rhs.m_Sum;
		m_SumOfSquares += rhs.m_SumOfSquares;
	}
	double Variance() const
	{
		return (m_NumSamples * m_SumOfSquares - m_Sum * m_Sum) / 
				(m_NumSamples * (m_NumSamples - 1));
	}
	u64 NumSamples() const {return m_NumSamples;}
	double Sum() const {return m_Sum;}
	double SumOfSquares() const {return m_SumOfSquares;}
	double Mean() const {return m_Sum / m_NumSamples;}
	double StandardDeviation() const {return sqrt(Variance());}

	// normal distribution:
	double LowerQuartile() const {return Mean() - .6798 * StandardDeviation();}
	double UpperQuartile() const {return Mean() + .6798 * StandardDeviation();}
	double Lower95thPercentile() const {return Mean() - 1.96 * StandardDeviation();}
	double Upper95thPercentile() const {return Mean() + 1.96 * StandardDeviation();}

private:
	u64		m_NumSamples;
	double	m_Sum;
	double	m_SumOfSquares;
};

class pfFrameStatCounter
{
public:
	void Add(double sample) 
	{
		m_Frame.Add(sample);
		//m_Total.Add(sample);
	}
	void Reset();
	void EndFrame(); 
	float FrameTime() const
	{
		if (RageTrace().ShowWholeRun())
			return (float)m_TotalFrameTime.Mean();
		else
			return m_AvgPerFrameTime;
	}
	float FrameCalls() const
	{
		if (RageTrace().ShowWholeRun())
			return (float)m_TotalFrameCalls.Mean();
		else
			return m_AvgPerFrameCalls;
	}

	pfStatCounter m_Frame;
	pfStatCounter m_FrameTime;
	pfStatCounter m_FrameCalls;	
	//pfStatCounter m_Total;
	pfStatCounter m_TotalFrameTime;
	pfStatCounter m_TotalFrameCalls;	
	float		  m_AvgPerFrameTime;
	float		  m_AvgPerFrameCalls;
};

//////////////////////////////////////////////////////////////////////////
// hierarchical counter that totals up the time spent in different
// marked regions of a trace

class pfTraceCounter
{
public:
	pfTraceCounter(pfTraceCounterId* id = 0)
	:	m_Parent(0)
	,	m_Id(id)
	,	m_Layer(0)
	{}
	virtual ~pfTraceCounter();

	virtual void Reset();
	virtual void EndFrame();
	pfTraceCounter* Child(pfTraceCounterId* id);
	virtual pfTraceCounter* New() {return rage_new pfTraceCounter;}
	virtual void Dump();
	virtual void DumpChildren();
	virtual void DisplayStats(float x, float y);
	virtual void WriteToFile(fiStream* handle);

	virtual void OnPush(u32 timebase);
	virtual void OnPop(u32 timebase);

	virtual float SortKey() const;
	float PercentageOfParent() const;
	
	pfTraceCounter*					m_Parent;
	pfTraceCounterId*				m_Id;	
	u32								m_Layer;
	u32								m_StartTime;
	pfFrameStatCounter				m_Counter;

	atMap<pfTraceCounterId*, pfTraceCounter*> m_Children;
};

//////////////////////////////////////////////////////////////////////////
// identifier for an individual counter within a trace

class pfTraceCounterId
{
public:
	pfTraceCounterId(const char* name = "");
	pfTraceCounterId(const char* name, Color32 color);
	~pfTraceCounterId();
	void Init(const char* name);
	void Init(const char* name, Color32 color);

	char						m_Name[64];
	Color32						m_Color;
	pfFrameStatCounter			m_Counter;	
	atArray<pfTraceCounter*>	m_Instances;
};

//////////////////////////////////////////////////////////////////////////

class pfBufferedTrace : public pfTrace
{
public:
	pfBufferedTrace(const char* name, u32 bufsize, pfTraceCounter* root);
	~pfBufferedTrace();

	virtual u32		GetNextSample();
	virtual void	AdvanceTo(u32 timebase);

	void			StartFrame();
	void			Push(pfTraceCounterId* identifier, bool autopop = false);
	void			Pop();
	//u32				Stack() const {return m_Stack - m_AutoPop;}

protected:
	void			Store(pfTraceCounterId* identifier, u32 timebase);

	bool			m_AutoPop;
	u32*			m_Buf;
	u32*			m_BufEnd;
	u32*			m_BufWrite;
	u32*			m_BufRead;
	//u32				m_Stack;
};

inline void pfBufferedTrace::Push(pfTraceCounterId* identifier, bool autopop)
{
	u32 t = (u32)sysTimer::GetTicks();
	if (m_AutoPop)
		Store(0, t);
	Store(identifier, t);
	m_AutoPop = autopop;
}

inline void pfBufferedTrace::Pop()
{
	Push(NULL);
}

inline void pfBufferedTrace::Store(pfTraceCounterId* identifier, u32 timebase)
{
	m_BufWrite[0] = timebase|1;
	m_BufWrite[1] = (u32)identifier;
	m_BufWrite += 2;
	if (m_BufWrite == m_BufEnd)
		m_BufWrite = m_Buf;
}

} // namespace rage

#define RAGETRACE_INIT() ::rage::RageTrace().Init()
#define RAGETRACE_FRAMESTART() ::rage::RageTrace().SetFrameStart()
#define RAGETRACE_UPDATE() ::rage::RageTrace().Update()
#define RAGETRACE_DECL(name) ::rage::pfTraceCounterId _ragetrace_##name(#name)
#define RAGETRACE_SHUTDOWN() ::rage::RageTrace().Shutdown()

#else

#define RAGETRACE_INIT()
#define RAGETRACE_FRAMESTART()
#define RAGETRACE_UPDATE()
#define RAGETRACE_DECL(name)
#define RAGETRACE_SHUTDOWN()

#endif // RAGETRACE && !__SPU

#if RAGETRACE
namespace rage {class pfTraceCounterId;} // for spu
#define RAGETRACE_ONLY(x) x
#else
#define RAGETRACE_ONLY(x)
#endif

#endif // PROFILE_TRACE_H
