#ifndef EVENT_DESCRIPTION_BOARD_H
#define EVENT_DESCRIPTION_BOARD_H

#if USE_PROFILER

#include "common.h"
#include "serialization.h"

#include "atl/binmap.h"
// #include "atl/hashmap.h"
#include "atl/vector.h"
#include "atl/map.h"
#include "math/amath.h"
#include "system/criticalsection.h"
#include "system/memory.h"

using namespace rage;

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EventDescription;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TDescription>
class DescriptionBoard
{
public:
	typedef rage::atVector<TDescription*, 0, rage::u32> Board;

private:
	Board board;
	rage::atMap<rage::u64, TDescription*> shared;

	static volatile rage::u32 sm_recursiveGuard;
public:
	static rage::sysCriticalSectionToken sm_csBoardToken;

	TDescription* CreateDescription(const char* name)
	{
		// Memory is not ready yet
		if (!rage::sysMemAllocator::IsCurrentSet())
			return nullptr;

		USE_ROCKY_MEMORY;
		SYS_CS_SYNC(sm_csBoardToken);

		TDescription* desc = rage_new TDescription((rage::u32)board.size());
		board.PushAndGrow(desc, rage::Max(16, board.GetCapacity()));
		desc->Init(name);

		return desc;
	}

	TDescription* CreateSharedDescription(const char* name)
	{
		if (name == nullptr || strlen(name) == 0)
			return nullptr;

		USE_ROCKY_MEMORY;
		SYS_CS_SYNC(sm_csBoardToken);

		u64 hash = (u64)atStringHash(name); // atStringHash64(name); No 64 bit string hash function

		TDescription** item = shared.Access(hash);
		if (item == NULL)
		{
			TDescription* desc = rage_new TDescription((rage::u32)board.size());
			board.PushAndGrow(desc, rage::Max(16, board.GetCapacity()));
			desc->Init(name);

			shared.Insert(hash, desc);

			return desc;
		}
		
		FatalAssertf(stricmp(name, (*item)->name) == 0, "[Rocky] Found different tags with the same hash: %s [0x%" I64FMT "x] <=> %s [0x%" I64FMT "x]! Please report this bug to v.slyusarev!", name, hash, (*item)->name, (u64)atStringHash((*item)->name));

		return *item;
	}

	static DescriptionBoard<TDescription>& Get()
	{
		static volatile DescriptionBoard<TDescription>* instance;

		while (instance == nullptr)
		{
			if (rage::sysInterlockedCompareExchange(&sm_recursiveGuard, 1, 0) != 1)
				continue;

			
			static DescriptionBoard<TDescription> result;
			instance = &result;

			while (rage::sysInterlockedCompareExchange(&sm_recursiveGuard, 0, 1) != 0) {}
		}

		return *((DescriptionBoard<TDescription>*)instance);
	}

	const Board& GetEvents() const
	{
		return board;
	}

	template<class Func>
	void ForEach(Func func) const
	{
		SYS_CS_SYNC(sm_csBoardToken);

		for (int i = 0; i < board.GetCount(); ++i)
			func(board[i]);
	}

	void Shutdown()
	{
		USE_ROCKY_MEMORY;
		SYS_CS_SYNC( sm_csBoardToken );

		for (int i = 0; i < board.GetCount(); ++i)
			delete board[i];

		board.clear();
		shared.Reset();
	}

	~DescriptionBoard()
	{
		Shutdown();
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TDescription>
rage::sysCriticalSectionToken DescriptionBoard<TDescription>::sm_csBoardToken;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TDescription>
volatile rage::u32 DescriptionBoard<TDescription>::sm_recursiveGuard = 0;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TDescription>
OutputDataStream& operator << ( OutputDataStream& stream, const DescriptionBoard<TDescription>& ob)
{
	SYS_CS_SYNC( ob.sm_csBoardToken );

	const auto& events = ob.GetEvents();

	stream << (rage::u32)events.size();

	for (int i = 0; i < events.GetCount(); ++i)
		stream << *(events[i]);

	return stream;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SetEventFlag(int index, rage::u8 mask, bool flag);
bool HasSamplingEvents();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef DescriptionBoard<EventDescription> EventDescriptionBoard;
typedef DescriptionBoard<TagDescription> TagDescriptionBoard;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER

#endif //EVENT_DESCRIPTION_BOARD_H