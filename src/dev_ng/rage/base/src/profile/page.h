//
// profile/page.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

//
// A page contains a collection of groups for display,
// logging, (or transmission, etc.)  A group may exist
// in many different pages.
//

#ifndef PROFILE_PAGE_H
#define PROFILE_PAGE_H

//==============================================================
// external defines

#include "atl/bintree.h"
#include "diag/stats.h"


#if !__STATS

//==============================================================
// empy macro definitions for !__STATS

#define PF_PAGE(x,y)											// empty macro for !__STATS
#define EXT_PF_PAGE(x)											// empty macro for !__STATS
#define PF_LINK(x,y)											// empty macro for !__STATS

#else // __STATS

//==============================================================
// includes and external class defines

#include "data/base.h"

namespace rage {

class bkBank;
class pfProfiler;
class pfGroup;
class pfElement;
class pfPage;

//==============================================================
// macros to declare profile pages

// declare and define a pfPage
#define PF_PAGE(name,description)								::rage::pfPage PFPAGE_##name (description)

// declare an extern pfPage
#define EXT_PF_PAGE(name)										extern ::rage::pfPage PFPAGE_##name

// link a group into a page
//two ##'s in the same symbol seem to be not supported?
//#define PF_LINK(pagename,groupname)								pfPageLink PFPAGELINK_##pagename_##groupname (&PFPAGE_##pagename,&PFGROUP_##groupname)
#define PF_LINK(pagename,groupname)								::rage::pfPageLink PFPAGELINK_##groupname (&PFPAGE_##pagename,&PFGROUP_##groupname)


//==============================================================
// pfPageNode

typedef atBinTree<const char *, pfPage *>::Node pfPageNode;


//==============================================================
// pfPage
// PURPOSE
//   The page class contains a list of groups that are to be
//   displayed together.  The page is only a logical collection
//   and the groups are still managed directly by the pfProfiler class.
//
// <FLAG Component>
//
class pfPage
{
public:
	// PURPOSE: constructor
	pfPage (const char * description);

	// PURPOSE: destructor
	~pfPage ();

	void AddGroup (pfGroup * group);							// add a group to the Groups
	const char * GetName () const								{return m_Name;}

	bool GetIsActive() const									{return isActive;};
	void SetIsActive(bool isActive)								{this->isActive = isActive;};

	int GetNumGroups ()											{return m_NumGroups;}
	pfGroup * GetGroup (int i)									{FastAssert(i>=0 && i<m_NumGroups); return m_Groups[i];}

	pfGroup * GetNextGroup (const pfGroup * curGroup);			// return the next group, or NULL if end, or first group if NULL

	enum {kNameMaxLen=128};										// the maximum length of the page name
	enum {kMaxGroups=16};										// the maximum number of groups

#if __BANK
	void AddWidgets (bkBank & bank);							// add control widgets
	void AddTraceWidgets (bkBank& bank);						// add widgets for generating trace data
	void AddToRagProfiler();									// if connected, add to Rag Profiler
	u32 GetId() const											{ return m_id; }
	bool IsVisible() const										{ return m_refCount > 0; }
	void SetRefCount( int count )								{ m_refCount = count; }
#endif

protected:
	int m_NumGroups;											// the number of groups on this page
	pfGroup * m_Groups[kMaxGroups];								// the list of groups contained in this page
	char m_Name[kNameMaxLen];									// a textual description of the page
	pfPageNode m_TreeNode;										// a node for storing this page in a tree
	bool isActive;												// a flag that allows pages to go inactive

	BANK_ONLY( u32 m_id; )
	BANK_ONLY( int m_refCount; )
};


//==============================================================
// pfPageLink

class pfPageLink
{
public:
	pfPageLink (pfPage * page, pfGroup * group);
};

}	// namespace rage

//==============================================================

#endif	// __STATS

#endif  // PROFILE_PAGE_H
