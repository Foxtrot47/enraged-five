#ifndef DURANGO_ETW_H
#define DURANGO_ETW_H

#if (RSG_XDK || RSG_GDK) && !RSG_FINAL && !RSG_TOOL

#if RSG_GDK
#include <evntprov.h>
#endif

#if RSG_XDK

//////////////////////////////////////////////////////////////////////////
// This code is not presented in XDK. But all the functions that we
// need are exported and available through XDK libraries.
//
// All what we need to do is to declare all the structures and functions
// in the proper way, so the linker will take care of it.
//
// Hope that Microsoft will fix this problem soon and we will be able to
// access this code directly through the header.
//
// Source: hidden part from $(DurangoXDK)/$(XDKEDITION)/xdk/Include/shared/evntrace.h
//////////////////////////////////////////////////////////////////////////
#ifndef WMIAPI
#ifndef MIDL_PASS
#ifdef _WMI_SOURCE_
#define WMIAPI __stdcall
#else
#define WMIAPI DECLSPEC_IMPORT __stdcall
#endif // _WMI_SOURCE
#endif // MIDL_PASS

#endif // WMIAPI

#include <guiddef.h>

#if defined(_NTDDK_) || defined(_NTIFS_) || defined(_WMIKM_)

#define _EVNTRACE_KERNEL_MODE

#endif

#if !defined(_EVNTRACE_KERNEL_MODE)

#include <wmistr.h>

#endif

//
// EventTraceGuid is used to identify a event tracing session
//
DEFINE_GUID ( /* 68fdd900-4a3e-11d1-84f4-0000f80464e3 */
			 EventTraceGuid,
			 0x68fdd900,
			 0x4a3e,
			 0x11d1,
			 0x84, 0xf4, 0x00, 0x00, 0xf8, 0x04, 0x64, 0xe3
			 );

//
// SystemTraceControlGuid. Used to specify event tracing for kernel
//
DEFINE_GUID ( /* 9e814aad-3204-11d2-9a82-006008a86939 */
			 SystemTraceControlGuid,
			 0x9e814aad,
			 0x3204,
			 0x11d2,
			 0x9a, 0x82, 0x00, 0x60, 0x08, 0xa8, 0x69, 0x39
			 );

//
// EventTraceConfigGuid. Used to report system configuration records
//
DEFINE_GUID ( /* 01853a65-418f-4f36-aefc-dc0f1d2fd235 */
			 EventTraceConfigGuid,
			 0x01853a65,
			 0x418f,
			 0x4f36,
			 0xae, 0xfc, 0xdc, 0x0f, 0x1d, 0x2f, 0xd2, 0x35
			 );

//
// DefaultTraceSecurityGuid. Specifies the default event tracing security
//
DEFINE_GUID ( /* 0811c1af-7a07-4a06-82ed-869455cdf713 */
			 DefaultTraceSecurityGuid,
			 0x0811c1af,
			 0x7a07,
			 0x4a06,
			 0x82, 0xed, 0x86, 0x94, 0x55, 0xcd, 0xf7, 0x13
			 );

#define KERNEL_LOGGER_NAMEW         L"NT Kernel Logger"
#define GLOBAL_LOGGER_NAMEW         L"GlobalLogger"
#define EVENT_LOGGER_NAMEW          L"EventLog"
#define DIAG_LOGGER_NAMEW           L"DiagLog"

#define KERNEL_LOGGER_NAMEA         "NT Kernel Logger"
#define GLOBAL_LOGGER_NAMEA         "GlobalLogger"
#define EVENT_LOGGER_NAMEA          "EventLog"
#define DIAG_LOGGER_NAMEA           "DiagLog"

#define MAX_MOF_FIELDS                      16  // Limit of USE_MOF_PTR fields

#ifndef _TRACEHANDLE_DEFINED
#define _TRACEHANDLE_DEFINED
typedef ULONG64 TRACEHANDLE, *PTRACEHANDLE;
#endif

//types for event data going to System Event Logger
#define SYSTEM_EVENT_TYPE                        1

//
// predefined generic event types (0x00 to 0x09 reserved).
//

#define EVENT_TRACE_TYPE_INFO               0x00  // Info or point event
#define EVENT_TRACE_TYPE_START              0x01  // Start event
#define EVENT_TRACE_TYPE_END                0x02  // End event
#define EVENT_TRACE_TYPE_STOP               0x02  // Stop event (WinEvent compatible)
#define EVENT_TRACE_TYPE_DC_START           0x03  // Collection start marker
#define EVENT_TRACE_TYPE_DC_END             0x04  // Collection end marker
#define EVENT_TRACE_TYPE_EXTENSION          0x05  // Extension/continuation
#define EVENT_TRACE_TYPE_REPLY              0x06  // Reply event
#define EVENT_TRACE_TYPE_DEQUEUE            0x07  // De-queue event
#define EVENT_TRACE_TYPE_RESUME             0x07  // Resume event (WinEvent compatible)
#define EVENT_TRACE_TYPE_CHECKPOINT         0x08  // Generic checkpoint event
#define EVENT_TRACE_TYPE_SUSPEND            0x08  // Suspend event (WinEvent compatible)
#define EVENT_TRACE_TYPE_WINEVT_SEND        0x09  // Send Event (WinEvent compatible)
#define EVENT_TRACE_TYPE_WINEVT_RECEIVE     0XF0  // Receive Event (WinEvent compatible)

//
// Predefined Event Tracing Levels for Software/Debug Tracing
//
//
// Trace Level is UCHAR and passed in through the EnableLevel parameter
// in EnableTrace API. It is retrieved by the provider using the
// GetTraceEnableLevel macro.It should be interpreted as an integer value
// to mean everything at or below that level will be traced.
//
// Here are the possible Levels.
//

#define TRACE_LEVEL_NONE        0   // Tracing is not on
#define TRACE_LEVEL_CRITICAL    1   // Abnormal exit or termination
#define TRACE_LEVEL_FATAL       1   // Deprecated name for Abnormal exit or termination
#define TRACE_LEVEL_ERROR       2   // Severe errors that need logging
#define TRACE_LEVEL_WARNING     3   // Warnings such as allocation failure
#define TRACE_LEVEL_INFORMATION 4   // Includes non-error cases(e.g.,Entry-Exit)
#define TRACE_LEVEL_VERBOSE     5   // Detailed traces from intermediate steps
#define TRACE_LEVEL_RESERVED6   6
#define TRACE_LEVEL_RESERVED7   7
#define TRACE_LEVEL_RESERVED8   8
#define TRACE_LEVEL_RESERVED9   9


//
// Event types for Process & Threads
//

#define EVENT_TRACE_TYPE_LOAD                  0x0A      // Load image

//
// Event types for IO subsystem
//

#define EVENT_TRACE_TYPE_IO_READ               0x0A
#define EVENT_TRACE_TYPE_IO_WRITE              0x0B
#define EVENT_TRACE_TYPE_IO_READ_INIT          0x0C
#define EVENT_TRACE_TYPE_IO_WRITE_INIT         0x0D
#define EVENT_TRACE_TYPE_IO_FLUSH              0x0E
#define EVENT_TRACE_TYPE_IO_FLUSH_INIT         0x0F


//
// Event types for Memory subsystem
//

#define EVENT_TRACE_TYPE_MM_TF                 0x0A      // Transition fault
#define EVENT_TRACE_TYPE_MM_DZF                0x0B      // Demand Zero fault
#define EVENT_TRACE_TYPE_MM_COW                0x0C      // Copy on Write
#define EVENT_TRACE_TYPE_MM_GPF                0x0D      // Guard Page fault
#define EVENT_TRACE_TYPE_MM_HPF                0x0E      // Hard page fault
#define EVENT_TRACE_TYPE_MM_AV                 0x0F      // Access violation

//
// Event types for Network subsystem, all protocols
//

#define EVENT_TRACE_TYPE_SEND                  0x0A     // Send
#define EVENT_TRACE_TYPE_RECEIVE               0x0B     // Receive
#define EVENT_TRACE_TYPE_CONNECT               0x0C     // Connect
#define EVENT_TRACE_TYPE_DISCONNECT            0x0D     // Disconnect
#define EVENT_TRACE_TYPE_RETRANSMIT            0x0E     // ReTransmit
#define EVENT_TRACE_TYPE_ACCEPT                0x0F     // Accept
#define EVENT_TRACE_TYPE_RECONNECT             0x10     // ReConnect
#define EVENT_TRACE_TYPE_CONNFAIL              0x11     // Fail
#define EVENT_TRACE_TYPE_COPY_TCP              0x12     // Copy in PendData
#define EVENT_TRACE_TYPE_COPY_ARP              0x13     // NDIS_STATUS_RESOURCES Copy
#define EVENT_TRACE_TYPE_ACKFULL               0x14     // A full data ACK
#define EVENT_TRACE_TYPE_ACKPART               0x15     // A Partial data ACK
#define EVENT_TRACE_TYPE_ACKDUP                0x16     // A Duplicate data ACK


//
// Event Types for the Header (to handle internal event headers)
//

#define EVENT_TRACE_TYPE_GUIDMAP                0x0A
#define EVENT_TRACE_TYPE_CONFIG                 0x0B
#define EVENT_TRACE_TYPE_SIDINFO                0x0C
#define EVENT_TRACE_TYPE_SECURITY               0x0D
#define EVENT_TRACE_TYPE_DBGID_RSDS             0x40

//
// Event Types for Registry subsystem
//

#define EVENT_TRACE_TYPE_REGCREATE                  0x0A     // NtCreateKey
#define EVENT_TRACE_TYPE_REGOPEN                    0x0B     // NtOpenKey
#define EVENT_TRACE_TYPE_REGDELETE                  0x0C     // NtDeleteKey
#define EVENT_TRACE_TYPE_REGQUERY                   0x0D     // NtQueryKey
#define EVENT_TRACE_TYPE_REGSETVALUE                0x0E     // NtSetValueKey
#define EVENT_TRACE_TYPE_REGDELETEVALUE             0x0F     // NtDeleteValueKey
#define EVENT_TRACE_TYPE_REGQUERYVALUE              0x10     // NtQueryValueKey
#define EVENT_TRACE_TYPE_REGENUMERATEKEY            0x11     // NtEnumerateKey
#define EVENT_TRACE_TYPE_REGENUMERATEVALUEKEY       0x12     // NtEnumerateValueKey
#define EVENT_TRACE_TYPE_REGQUERYMULTIPLEVALUE      0x13     // NtQueryMultipleValueKey
#define EVENT_TRACE_TYPE_REGSETINFORMATION          0x14     // NtSetInformationKey
#define EVENT_TRACE_TYPE_REGFLUSH                   0x15     // NtFlushKey
#define EVENT_TRACE_TYPE_REGKCBCREATE               0x16     // KcbCreate
#define EVENT_TRACE_TYPE_REGKCBDELETE               0x17     // KcbDelete
#define EVENT_TRACE_TYPE_REGKCBRUNDOWNBEGIN         0x18     // KcbRundownBegin
#define EVENT_TRACE_TYPE_REGKCBRUNDOWNEND           0x19     // KcbRundownEnd
#define EVENT_TRACE_TYPE_REGVIRTUALIZE              0x1A     // VirtualizeKey
#define EVENT_TRACE_TYPE_REGCLOSE                   0x1B     // NtClose (KeyObject)
#define EVENT_TRACE_TYPE_REGSETSECURITY             0x1C     // SetSecurityDescriptor (KeyObject)
#define EVENT_TRACE_TYPE_REGQUERYSECURITY           0x1D     // QuerySecurityDescriptor (KeyObject)
#define EVENT_TRACE_TYPE_REGCOMMIT                  0x1E     // CmKtmNotification (TRANSACTION_NOTIFY_COMMIT)
#define EVENT_TRACE_TYPE_REGPREPARE                 0x1F     // CmKtmNotification (TRANSACTION_NOTIFY_PREPARE)
#define EVENT_TRACE_TYPE_REGROLLBACK                0x20     // CmKtmNotification (TRANSACTION_NOTIFY_ROLLBACK)
#define EVENT_TRACE_TYPE_REGMOUNTHIVE               0x21     // NtLoadKey variations + system hives

//
// Event types for system configuration records
//
#define EVENT_TRACE_TYPE_CONFIG_CPU             0x0A     // CPU Configuration
#define EVENT_TRACE_TYPE_CONFIG_PHYSICALDISK    0x0B     // Physical Disk Configuration
#define EVENT_TRACE_TYPE_CONFIG_LOGICALDISK     0x0C     // Logical Disk Configuration
#define EVENT_TRACE_TYPE_CONFIG_NIC             0x0D     // NIC Configuration
#define EVENT_TRACE_TYPE_CONFIG_VIDEO           0x0E     // Video Adapter Configuration
#define EVENT_TRACE_TYPE_CONFIG_SERVICES        0x0F     // Active Services
#define EVENT_TRACE_TYPE_CONFIG_POWER           0x10     // ACPI Configuration
#define EVENT_TRACE_TYPE_CONFIG_NETINFO         0x11     // Networking Configuration
#define EVENT_TRACE_TYPE_CONFIG_OPTICALMEDIA    0x12     // Optical Media Configuration

#define EVENT_TRACE_TYPE_CONFIG_IRQ             0x15     // IRQ assigned to devices
#define EVENT_TRACE_TYPE_CONFIG_PNP             0x16     // PnP device info
#define EVENT_TRACE_TYPE_CONFIG_IDECHANNEL      0x17     // Primary/Secondary IDE channel Configuration
#define EVENT_TRACE_TYPE_CONFIG_NUMANODE        0x18     // Numa configuration
#define EVENT_TRACE_TYPE_CONFIG_PLATFORM        0x19     // Platform Configuration
#define EVENT_TRACE_TYPE_CONFIG_PROCESSORGROUP  0x1A     // Processor Group Configuration
#define EVENT_TRACE_TYPE_CONFIG_PROCESSORNUMBER 0x1B     // ProcessorIndex -> ProcNumber mapping
#define EVENT_TRACE_TYPE_CONFIG_DPI             0x1C     // Display DPI Configuration

//
// Event types for Optical IO subsystem
//

#define EVENT_TRACE_TYPE_OPTICAL_IO_READ        0x37
#define EVENT_TRACE_TYPE_OPTICAL_IO_WRITE       0x38
#define EVENT_TRACE_TYPE_OPTICAL_IO_FLUSH       0x39
#define EVENT_TRACE_TYPE_OPTICAL_IO_READ_INIT   0x3a
#define EVENT_TRACE_TYPE_OPTICAL_IO_WRITE_INIT  0x3b
#define EVENT_TRACE_TYPE_OPTICAL_IO_FLUSH_INIT  0x3c

//
// Event types for Filter Manager
//

#define EVENT_TRACE_TYPE_FLT_PREOP_INIT         0x60   // Minifilter preop initiation
#define EVENT_TRACE_TYPE_FLT_POSTOP_INIT        0x61   // Minifilter postop initiation
#define EVENT_TRACE_TYPE_FLT_PREOP_COMPLETION   0x62   // Minifilter preop completion
#define EVENT_TRACE_TYPE_FLT_POSTOP_COMPLETION  0x63   // Minifilter postop completion
#define EVENT_TRACE_TYPE_FLT_PREOP_FAILURE      0x64   // Minifilter failed preop
#define EVENT_TRACE_TYPE_FLT_POSTOP_FAILURE     0x65   // Minifilter failed postop

//
// Enable flags for Kernel Events
//
#define EVENT_TRACE_FLAG_PROCESS            0x00000001  // process start & end
#define EVENT_TRACE_FLAG_THREAD             0x00000002  // thread start & end
#define EVENT_TRACE_FLAG_IMAGE_LOAD         0x00000004  // image load

#define EVENT_TRACE_FLAG_DISK_IO            0x00000100  // physical disk IO
#define EVENT_TRACE_FLAG_DISK_FILE_IO       0x00000200  // requires disk IO

#define EVENT_TRACE_FLAG_MEMORY_PAGE_FAULTS 0x00001000  // all page faults
#define EVENT_TRACE_FLAG_MEMORY_HARD_FAULTS 0x00002000  // hard faults only

#define EVENT_TRACE_FLAG_NETWORK_TCPIP      0x00010000  // tcpip send & receive

#define EVENT_TRACE_FLAG_REGISTRY           0x00020000  // registry calls
#define EVENT_TRACE_FLAG_DBGPRINT           0x00040000  // DbgPrint(ex) Calls

//
// Enable flags for Kernel Events on Vista and above
//
#define EVENT_TRACE_FLAG_PROCESS_COUNTERS   0x00000008  // process perf counters
#define EVENT_TRACE_FLAG_CSWITCH            0x00000010  // context switches
#define EVENT_TRACE_FLAG_DPC                0x00000020  // deffered procedure calls
#define EVENT_TRACE_FLAG_INTERRUPT          0x00000040  // interrupts
#define EVENT_TRACE_FLAG_SYSTEMCALL         0x00000080  // system calls

#define EVENT_TRACE_FLAG_DISK_IO_INIT       0x00000400  // physical disk IO initiation
#define EVENT_TRACE_FLAG_ALPC               0x00100000  // ALPC traces
#define EVENT_TRACE_FLAG_SPLIT_IO           0x00200000  // split io traces (VolumeManager)

#define EVENT_TRACE_FLAG_DRIVER             0x00800000  // driver delays
#define EVENT_TRACE_FLAG_PROFILE            0x01000000  // sample based profiling
#define EVENT_TRACE_FLAG_FILE_IO            0x02000000  // file IO
#define EVENT_TRACE_FLAG_FILE_IO_INIT       0x04000000  // file IO initiation

#define EVENT_TRACE_FLAG_PMC_PROFILE		0x80000000	// sample based profiling (PMC) - NOT CONFIRMED!

//
// Enable flags for Kernel Events on Win7 and above
//
#define EVENT_TRACE_FLAG_DISPATCHER         0x00000800  // scheduler (ReadyThread)
#define EVENT_TRACE_FLAG_VIRTUAL_ALLOC      0x00004000  // VM operations

//
// Enable flags for Kernel Events on Win8 and above
//
#define EVENT_TRACE_FLAG_VAMAP              0x00008000  // map/unmap (excluding images)
#define EVENT_TRACE_FLAG_NO_SYSCONFIG       0x10000000  // Do not do sys config rundown

//
// Pre-defined Enable flags for everybody else
//
#define EVENT_TRACE_FLAG_EXTENSION          0x80000000  // Indicates more flags
#define EVENT_TRACE_FLAG_FORWARD_WMI        0x40000000  // Can forward to WMI
#define EVENT_TRACE_FLAG_ENABLE_RESERVE     0x20000000  // Reserved

//
// Logger Mode flags
//
#define EVENT_TRACE_FILE_MODE_NONE          0x00000000  // Logfile is off
#define EVENT_TRACE_FILE_MODE_SEQUENTIAL    0x00000001  // Log sequentially
#define EVENT_TRACE_FILE_MODE_CIRCULAR      0x00000002  // Log in circular manner
#define EVENT_TRACE_FILE_MODE_APPEND        0x00000004  // Append sequential log

#define EVENT_ENABLE_PROPERTY_SID                   0x00000001
#define EVENT_ENABLE_PROPERTY_TS_ID                 0x00000002
#define EVENT_ENABLE_PROPERTY_STACK_TRACE           0x00000004
#define EVENT_ENABLE_PROPERTY_PSM_KEY               0x00000008
#define EVENT_ENABLE_PROPERTY_IGNORE_KEYWORD_0      0x00000010
#define EVENT_ENABLE_PROPERTY_PROVIDER_GROUP        0x00000020
#define EVENT_ENABLE_PROPERTY_ENABLE_KEYWORD_0      0x00000040
#define EVENT_ENABLE_PROPERTY_PROCESS_START_KEY     0x00000080
#define EVENT_ENABLE_PROPERTY_EVENT_KEY             0x00000100
#define EVENT_ENABLE_PROPERTY_EXCLUDE_INPRIVATE     0x00000200

#define EVENT_TRACE_REAL_TIME_MODE          0x00000100  // Real time mode on
#define EVENT_TRACE_DELAY_OPEN_FILE_MODE    0x00000200  // Delay opening file
#define EVENT_TRACE_BUFFERING_MODE          0x00000400  // Buffering mode only
#define EVENT_TRACE_PRIVATE_LOGGER_MODE     0x00000800  // Process Private Logger
#define EVENT_TRACE_ADD_HEADER_MODE         0x00001000  // Add a logfile header

#define EVENT_TRACE_USE_GLOBAL_SEQUENCE     0x00004000  // Use global sequence no.
#define EVENT_TRACE_USE_LOCAL_SEQUENCE      0x00008000  // Use local sequence no.

#define EVENT_TRACE_RELOG_MODE              0x00010000  // Relogger

#define EVENT_TRACE_USE_PAGED_MEMORY        0x01000000  // Use pageable buffers

//
// Logger Mode flags on XP and above
//

#define EVENT_TRACE_FILE_MODE_NEWFILE       0x00000008  // Auto-switch log file
#define EVENT_TRACE_FILE_MODE_PREALLOCATE   0x00000020  // Pre-allocate mode

//
// Logger Mode flags on Vista and above
//

#define EVENT_TRACE_NONSTOPPABLE_MODE       0x00000040  // Session cannot be stopped (Autologger only)
#define EVENT_TRACE_SECURE_MODE             0x00000080  // Secure session
#define EVENT_TRACE_USE_KBYTES_FOR_SIZE     0x00002000  // Use KBytes as file size unit
#define EVENT_TRACE_PRIVATE_IN_PROC         0x00020000  // In process private logger
#define EVENT_TRACE_MODE_RESERVED           0x00100000  // Reserved bit, used to signal Heap/Critsec tracing

//
// Logger Mode flags on Win7 and above
//

#define EVENT_TRACE_NO_PER_PROCESSOR_BUFFERING 0x10000000  // Use this for low frequency sessions.

//
// Logger Mode flags on Win8 and above
//

#define EVENT_TRACE_SYSTEM_LOGGER_MODE      0x02000000  // Receive events from SystemTraceProvider
#define EVENT_TRACE_ADDTO_TRIAGE_DUMP       0x80000000  // Add ETW buffers to triage dumps
#define EVENT_TRACE_STOP_ON_HYBRID_SHUTDOWN 0x00400000  // Stop on hybrid shutdown
#define EVENT_TRACE_PERSIST_ON_HYBRID_SHUTDOWN 0x00800000 // Persist on hybrid shutdown

//
// ControlTrace Codes
//

#define EVENT_TRACE_CONTROL_QUERY           0
#define EVENT_TRACE_CONTROL_STOP            1
#define EVENT_TRACE_CONTROL_UPDATE          2

//
// Flush ControlTrace Codes for XP and above
//

#define EVENT_TRACE_CONTROL_FLUSH           3       // Flushes all the buffers

//
// Flags used by WMI Trace Message
// Note that the order or value of these flags should NOT be changed as they are processed
// in this order.
//
#define TRACE_MESSAGE_SEQUENCE              1  // Message should include a sequence number
#define TRACE_MESSAGE_GUID                  2  // Message includes a GUID
#define TRACE_MESSAGE_COMPONENTID           4  // Message has no GUID, Component ID instead
#define TRACE_MESSAGE_TIMESTAMP             8  // Message includes a timestamp
#define TRACE_MESSAGE_PERFORMANCE_TIMESTAMP 16 // *Obsolete* Clock type is controlled by the logger
#define TRACE_MESSAGE_SYSTEMINFO            32 // Message includes system information TID,PID

//
// Vista flags set by system to indicate provider pointer size.
//

#define TRACE_MESSAGE_POINTER32         0x0040   // Message logged by 32 bit provider
#define TRACE_MESSAGE_POINTER64         0x0080   // Message logged by 64 bit provider

#define TRACE_MESSAGE_FLAG_MASK         0xFFFF   // Only the lower 16 bits of flags are placed in the message
// those above 16 bits are reserved for local processing
//
// Maximum size allowed for a single TraceMessage message.
//
// N.B. This limit was increased from 8K to 64K in Win8.
//

#define TRACE_MESSAGE_MAXIMUM_SIZE   (64 * 1024)

//
// Flags to indicate to consumer which fields
// in the EVENT_TRACE_HEADER are valid
//

#define EVENT_TRACE_USE_PROCTIME          0x0001    // ProcessorTime field is valid
#define EVENT_TRACE_USE_NOCPUTIME         0x0002    // No Kernel/User/Processor Times

//
// TRACE_HEADER_FLAG values are used in the Flags field of EVENT_TRACE_HEADER
// structure while calling into TraceEvent API
//

#define TRACE_HEADER_FLAG_USE_TIMESTAMP     0x00000200
#define TRACE_HEADER_FLAG_TRACED_GUID       0x00020000 // denotes a trace
#define TRACE_HEADER_FLAG_LOG_WNODE         0x00040000 // request to log Wnode
#define TRACE_HEADER_FLAG_USE_GUID_PTR      0x00080000 // Guid is actually a pointer
#define TRACE_HEADER_FLAG_USE_MOF_PTR       0x00100000 // MOF data are dereferenced

#define ENABLE_TRACE_PARAMETERS_VERSION      1
#define ENABLE_TRACE_PARAMETERS_VERSION_2    2

// This struct is _ENABLE_TRACE_PARAMETERS in the Windows SDK, but that conflicts with the XB1 XDK.
typedef struct _ENABLE_TRACE_PARAMETERS_V2 {
    ULONG                    Version;
    ULONG                    EnableProperty;
    ULONG                    ControlFlags;
    GUID                     SourceId;
    PEVENT_FILTER_DESCRIPTOR EnableFilterDesc;
    ULONG                    FilterDescCount;
} ENABLE_TRACE_PARAMETERS_V2, *PENABLE_TRACE_PARAMETERS_V2;

//
// Trace header for all legacy events.
//

typedef struct _EVENT_TRACE_HEADER {        // overlays WNODE_HEADER
	USHORT          Size;                   // Size of entire record
	union {
		USHORT      FieldTypeFlags;         // Indicates valid fields
		struct {
			UCHAR   HeaderType;             // Header type - internal use only
			UCHAR   MarkerFlags;            // Marker - internal use only
		} DUMMYSTRUCTNAME;
	} DUMMYUNIONNAME;
	union {
		ULONG       Version;
		struct {
			UCHAR   Type;                   // event type
			UCHAR   Level;                  // trace instrumentation level
			USHORT  Version;                // version of trace record
		} Class;
	} DUMMYUNIONNAME2;
	ULONG           ThreadId;               // Thread Id
	ULONG           ProcessId;              // Process Id
	LARGE_INTEGER   TimeStamp;              // time when event happens
	union {
		GUID        Guid;                   // Guid that identifies event
		ULONGLONG   GuidPtr;                // use with WNODE_FLAG_USE_GUID_PTR
	} DUMMYUNIONNAME3;
	union {
		struct {
			ULONG   KernelTime;             // Kernel Mode CPU ticks
			ULONG   UserTime;               // User mode CPU ticks
		} DUMMYSTRUCTNAME;
		ULONG64     ProcessorTime;          // Processor Clock
		struct {
			ULONG   ClientContext;          // Reserved
			ULONG   Flags;                  // Event Flags
		} DUMMYSTRUCTNAME2;
	} DUMMYUNIONNAME4;
} EVENT_TRACE_HEADER, *PEVENT_TRACE_HEADER;

//
// This header is used to trace and track transaction co-relations
//
typedef struct _EVENT_INSTANCE_HEADER {
	USHORT          Size;
	union {
		USHORT      FieldTypeFlags;     // Indicates valid fields
		struct {
			UCHAR   HeaderType;         // Header type - internal use only
			UCHAR   MarkerFlags;        // Marker - internal use only
		} DUMMYSTRUCTNAME;
	} DUMMYUNIONNAME;
	union {
		ULONG       Version;
		struct {
			UCHAR   Type;
			UCHAR   Level;
			USHORT  Version;
		} Class;
	} DUMMYUNIONNAME2;
	ULONG           ThreadId;
	ULONG           ProcessId;
	LARGE_INTEGER   TimeStamp;
	ULONGLONG       RegHandle;
	ULONG           InstanceId;
	ULONG           ParentInstanceId;
	union {
		struct {
			ULONG   KernelTime;             // Kernel Mode CPU ticks
			ULONG   UserTime;               // User mode CPU ticks
		} DUMMYSTRUCTNAME;
		ULONG64     ProcessorTime;          // Processor Clock
		struct {
			ULONG   EventId;                // Event ID
			ULONG   Flags;                  // Trace header Flags
		} DUMMYSTRUCTNAME2;
	} DUMMYUNIONNAME3;
	ULONGLONG       ParentRegHandle;
} EVENT_INSTANCE_HEADER, *PEVENT_INSTANCE_HEADER;

//
// Following are structures and macros for use with USE_MOF_PTR
//

// Trace data types
#define ETW_NULL_TYPE_VALUE                 0
#define ETW_OBJECT_TYPE_VALUE               1
#define ETW_STRING_TYPE_VALUE               2
#define ETW_SBYTE_TYPE_VALUE                3
#define ETW_BYTE_TYPE_VALUE                 4
#define ETW_INT16_TYPE_VALUE                5
#define ETW_UINT16_TYPE_VALUE               6
#define ETW_INT32_TYPE_VALUE                7
#define ETW_UINT32_TYPE_VALUE               8
#define ETW_INT64_TYPE_VALUE                9
#define ETW_UINT64_TYPE_VALUE               10
#define ETW_CHAR_TYPE_VALUE                 11
#define ETW_SINGLE_TYPE_VALUE               12
#define ETW_DOUBLE_TYPE_VALUE               13
#define ETW_BOOLEAN_TYPE_VALUE              14
#define ETW_DECIMAL_TYPE_VALUE              15
// Extended types
#define ETW_GUID_TYPE_VALUE                 101
#define ETW_ASCIICHAR_TYPE_VALUE            102
#define ETW_ASCIISTRING_TYPE_VALUE          103
#define ETW_COUNTED_STRING_TYPE_VALUE       104
#define ETW_POINTER_TYPE_VALUE              105
#define ETW_SIZET_TYPE_VALUE                106
#define ETW_HIDDEN_TYPE_VALUE               107
#define ETW_BOOL_TYPE_VALUE                 108
#define ETW_COUNTED_ANSISTRING_TYPE_VALUE   109
#define ETW_REVERSED_COUNTED_STRING_TYPE_VALUE 110
#define ETW_REVERSED_COUNTED_ANSISTRING_TYPE_VALUE 111
#define ETW_NON_NULL_TERMINATED_STRING_TYPE_VALUE 112
#define ETW_REDUCED_ANSISTRING_TYPE_VALUE   113
#define ETW_REDUCED_STRING_TYPE_VALUE       114
#define ETW_SID_TYPE_VALUE                  115
#define ETW_VARIANT_TYPE_VALUE              116
#define ETW_PTVECTOR_TYPE_VALUE             117
#define ETW_WMITIME_TYPE_VALUE              118
#define ETW_DATETIME_TYPE_VALUE             119
#define ETW_REFRENCE_TYPE_VALUE             120


#define DEFINE_TRACE_MOF_FIELD(MOF, ptr, length, type) \
	(MOF)->DataPtr  = (ULONG64)(ULONG_PTR) ptr; \
	(MOF)->Length   = (ULONG) length; \
	(MOF)->DataType = (ULONG) type;

typedef struct _MOF_FIELD {
	ULONG64     DataPtr;    // Pointer to the field. Up to 64-bits only
	ULONG       Length;     // Length of the MOF field
	ULONG       DataType;   // Type of data
} MOF_FIELD, *PMOF_FIELD;

#if !defined(_EVNTRACE_KERNEL_MODE) || defined(_WMIKM_)

//
// This is the header for every logfile. The memory for LoggerName
// and LogFileName must be contiguous adjacent to this structure
// Allows both user-mode and kernel-mode to understand the header.
//
// TRACE_LOGFILE_HEADER32 and TRACE_LOGFILE_HEADER64 structures
// are also provided to simplify cross platform decoding of the
// header event.
//

typedef struct _TRACE_LOGFILE_HEADER {
	ULONG           BufferSize;         // Logger buffer size in Kbytes
	union {
		ULONG       Version;            // Logger version
		struct {
			UCHAR   MajorVersion;
			UCHAR   MinorVersion;
			UCHAR   SubVersion;
			UCHAR   SubMinorVersion;
		} VersionDetail;
	} DUMMYUNIONNAME;
	ULONG           ProviderVersion;    // defaults to NT version
	ULONG           NumberOfProcessors; // Number of Processors
	LARGE_INTEGER   EndTime;            // Time when logger stops
	ULONG           TimerResolution;    // assumes timer is constant!!!
	ULONG           MaximumFileSize;    // Maximum in Mbytes
	ULONG           LogFileMode;        // specify logfile mode
	ULONG           BuffersWritten;     // used to file start of Circular File
	union {
		GUID LogInstanceGuid;           // For RealTime Buffer Delivery
		struct {
			ULONG   StartBuffers;       // Count of buffers written at start.
			ULONG   PointerSize;        // Size of pointer type in bits
			ULONG   EventsLost;         // Events losts during log session
			ULONG   CpuSpeedInMHz;      // Cpu Speed in MHz
		} DUMMYSTRUCTNAME;
	} DUMMYUNIONNAME2;
#if defined(_WMIKM_)
	PWCHAR          LoggerName;
	PWCHAR          LogFileName;
	RTL_TIME_ZONE_INFORMATION TimeZone;
#else
	LPWSTR          LoggerName;
	LPWSTR          LogFileName;
	TIME_ZONE_INFORMATION TimeZone;
#endif
	LARGE_INTEGER   BootTime;
	LARGE_INTEGER   PerfFreq;           // Reserved
	LARGE_INTEGER   StartTime;          // Reserved
	ULONG           ReservedFlags;      // ClockType
	ULONG           BuffersLost;
} TRACE_LOGFILE_HEADER, *PTRACE_LOGFILE_HEADER;

typedef struct _TRACE_LOGFILE_HEADER32 {
	ULONG           BufferSize;         // Logger buffer size in Kbytes
	union {
		ULONG       Version;            // Logger version
		struct {
			UCHAR   MajorVersion;
			UCHAR   MinorVersion;
			UCHAR   SubVersion;
			UCHAR   SubMinorVersion;
		} VersionDetail;
	};
	ULONG           ProviderVersion;    // defaults to NT version
	ULONG           NumberOfProcessors; // Number of Processors
	LARGE_INTEGER   EndTime;            // Time when logger stops
	ULONG           TimerResolution;    // assumes timer is constant!!!
	ULONG           MaximumFileSize;    // Maximum in Mbytes
	ULONG           LogFileMode;        // specify logfile mode
	ULONG           BuffersWritten;     // used to file start of Circular File
	union {
		GUID LogInstanceGuid;           // For RealTime Buffer Delivery
		struct {
			ULONG   StartBuffers;       // Count of buffers written at start.
			ULONG   PointerSize;        // Size of pointer type in bits
			ULONG   EventsLost;         // Events losts during log session
			ULONG   CpuSpeedInMHz;      // Cpu Speed in MHz
		};
	};
#if defined(_WMIKM_)
	ULONG32         LoggerName;
	ULONG32         LogFileName;
	RTL_TIME_ZONE_INFORMATION TimeZone;
#else
	ULONG32         LoggerName;
	ULONG32         LogFileName;
	TIME_ZONE_INFORMATION TimeZone;
#endif
	LARGE_INTEGER   BootTime;
	LARGE_INTEGER   PerfFreq;           // Reserved
	LARGE_INTEGER   StartTime;          // Reserved
	ULONG           ReservedFlags;      // ClockType
	ULONG           BuffersLost;
} TRACE_LOGFILE_HEADER32, *PTRACE_LOGFILE_HEADER32;

typedef struct _TRACE_LOGFILE_HEADER64 {
	ULONG           BufferSize;         // Logger buffer size in Kbytes
	union {
		ULONG       Version;            // Logger version
		struct {
			UCHAR   MajorVersion;
			UCHAR   MinorVersion;
			UCHAR   SubVersion;
			UCHAR   SubMinorVersion;
		} VersionDetail;
	};
	ULONG           ProviderVersion;    // defaults to NT version
	ULONG           NumberOfProcessors; // Number of Processors
	LARGE_INTEGER   EndTime;            // Time when logger stops
	ULONG           TimerResolution;    // assumes timer is constant!!!
	ULONG           MaximumFileSize;    // Maximum in Mbytes
	ULONG           LogFileMode;        // specify logfile mode
	ULONG           BuffersWritten;     // used to file start of Circular File
	union {
		GUID LogInstanceGuid;           // For RealTime Buffer Delivery
		struct {
			ULONG   StartBuffers;       // Count of buffers written at start.
			ULONG   PointerSize;        // Size of pointer type in bits
			ULONG   EventsLost;         // Events losts during log session
			ULONG   CpuSpeedInMHz;      // Cpu Speed in MHz
		};
	};
#if defined(_WMIKM_)
	ULONG64         LoggerName;
	ULONG64         LogFileName;
	RTL_TIME_ZONE_INFORMATION TimeZone;
#else
	ULONG64         LoggerName;
	ULONG64         LogFileName;
	TIME_ZONE_INFORMATION TimeZone;
#endif
	LARGE_INTEGER   BootTime;
	LARGE_INTEGER   PerfFreq;           // Reserved
	LARGE_INTEGER   StartTime;          // Reserved
	ULONG           ReservedFlags;      // ClockType
	ULONG           BuffersLost;
} TRACE_LOGFILE_HEADER64, *PTRACE_LOGFILE_HEADER64;

#endif // !_EVNTRACE_KERNEL_MODE || _WMIKM_

//
// Instance Information to track parent child relationship of Instances.
//

typedef struct EVENT_INSTANCE_INFO {
	HANDLE      RegHandle;
	ULONG       InstanceId;
} EVENT_INSTANCE_INFO, *PEVENT_INSTANCE_INFO;

#if !defined(_EVNTRACE_KERNEL_MODE)

//
// Structures that have UNICODE and ANSI versions are defined here
//

//
// Logger configuration and running statistics. This structure is used
// by user-mode callers, such as PDH library
//

typedef struct _EVENT_TRACE_PROPERTIES {
	WNODE_HEADER Wnode;
	//
	// data provided by caller
	ULONG BufferSize;                   // buffer size for logging (kbytes)
	ULONG MinimumBuffers;               // minimum to preallocate
	ULONG MaximumBuffers;               // maximum buffers allowed
	ULONG MaximumFileSize;              // maximum logfile size (in MBytes)
	ULONG LogFileMode;                  // sequential, circular
	ULONG FlushTimer;                   // buffer flush timer, in seconds
	ULONG EnableFlags;                  // trace enable flags
	LONG  AgeLimit;                     // unused

	// data returned to caller
	ULONG NumberOfBuffers;              // no of buffers in use
	ULONG FreeBuffers;                  // no of buffers free
	ULONG EventsLost;                   // event records lost
	ULONG BuffersWritten;               // no of buffers written to file
	ULONG LogBuffersLost;               // no of logfile write failures
	ULONG RealTimeBuffersLost;          // no of rt delivery failures
	HANDLE LoggerThreadId;              // thread id of Logger
	ULONG LogFileNameOffset;            // Offset to LogFileName
	ULONG LoggerNameOffset;             // Offset to LoggerName
} EVENT_TRACE_PROPERTIES, *PEVENT_TRACE_PROPERTIES;

//
// Data Provider structures
//
// Used by RegisterTraceGuids()
//
typedef struct  _TRACE_GUID_REGISTRATION {
	LPCGUID Guid;            // Guid of data block being registered or updated.
	HANDLE RegHandle;        // Guid Registration Handle is returned.
} TRACE_GUID_REGISTRATION, *PTRACE_GUID_REGISTRATION;

//
// Data consumer structures
//

#endif // !_EVNTRACE_KERNEL_MODE

typedef struct _TRACE_GUID_PROPERTIES {
	GUID    Guid;
	ULONG   GuidType;
	ULONG   LoggerId;
	ULONG   EnableLevel;
	ULONG   EnableFlags;
	BOOLEAN IsEnable;
} TRACE_GUID_PROPERTIES, *PTRACE_GUID_PROPERTIES;

#ifndef ETW_BUFFER_CONTEXT_DEF
#define ETW_BUFFER_CONTEXT_DEF
typedef struct _ETW_BUFFER_CONTEXT {
	union {
		struct {
			UCHAR ProcessorNumber;
			UCHAR Alignment;
		} DUMMYSTRUCTNAME;
		USHORT ProcessorIndex;
	} DUMMYUNIONNAME;
	USHORT  LoggerId;
} ETW_BUFFER_CONTEXT, *PETW_BUFFER_CONTEXT;
#endif

//
// Provider Information Flags used on Vista and above.
//
#define TRACE_PROVIDER_FLAG_LEGACY     (0x00000001)
#define TRACE_PROVIDER_FLAG_PRE_ENABLE (0x00000002)

//
// Enable Information for Provider Instance
// Used on Vista and above
//
typedef struct _TRACE_ENABLE_INFO {

	ULONG       IsEnabled;
	UCHAR       Level;
	UCHAR       Reserved1;
	USHORT      LoggerId;
	ULONG       EnableProperty;
	ULONG       Reserved2;
	ULONGLONG   MatchAnyKeyword;
	ULONGLONG   MatchAllKeyword;

} TRACE_ENABLE_INFO, *PTRACE_ENABLE_INFO;

//
// Instance Information for Provider
// Used on Vista and above
//
typedef struct _TRACE_PROVIDER_INSTANCE_INFO {

	ULONG NextOffset;
	ULONG EnableCount;
	ULONG Pid;
	ULONG Flags;

} TRACE_PROVIDER_INSTANCE_INFO, *PTRACE_PROVIDER_INSTANCE_INFO;

//
// GUID Information Used on Vista and above
//
typedef struct _TRACE_GUID_INFO {
	ULONG InstanceCount;
	ULONG Reserved;
} TRACE_GUID_INFO, *PTRACE_GUID_INFO;

typedef struct _PROFILE_SOURCE_INFO {
	ULONG NextEntryOffset;
	ULONG Source;
	ULONG MinInterval;
	ULONG MaxInterval;
	ULONG64 Reserved;
	WCHAR Description[ANYSIZE_ARRAY];
} PROFILE_SOURCE_INFO, *PPROFILE_SOURCE_INFO;

//
// An EVENT_TRACE consists of a fixed header (EVENT_TRACE_HEADER) and
// optionally a variable portion pointed to by MofData. The datablock
// layout of the variable portion is unknown to the Logger and must
// be obtained from WBEM CIMOM database.
//

typedef struct _EVENT_TRACE {
	EVENT_TRACE_HEADER      Header;             // Event trace header
	ULONG                   InstanceId;         // Instance Id of this event
	ULONG                   ParentInstanceId;   // Parent Instance Id.
	GUID                    ParentGuid;         // Parent Guid;
	PVOID                   MofData;            // Pointer to Variable Data
	ULONG                   MofLength;          // Variable Datablock Length
	union {
		ULONG               ClientContext;
		ETW_BUFFER_CONTEXT  BufferContext;
	} DUMMYUNIONNAME;
} EVENT_TRACE, *PEVENT_TRACE;

#define EVENT_CONTROL_CODE_DISABLE_PROVIDER 0
#define EVENT_CONTROL_CODE_ENABLE_PROVIDER  1
#define EVENT_CONTROL_CODE_CAPTURE_STATE    2

#endif // RSG_XDK

typedef struct _EVENT_RECORD
	EVENT_RECORD, *PEVENT_RECORD;

typedef struct _EVENT_TRACE_LOGFILEW
	EVENT_TRACE_LOGFILEW, *PEVENT_TRACE_LOGFILEW;

typedef struct _EVENT_TRACE_LOGFILEA
	EVENT_TRACE_LOGFILEA, *PEVENT_TRACE_LOGFILEA;

typedef ULONG (WINAPI * PEVENT_TRACE_BUFFER_CALLBACKW)
	(PEVENT_TRACE_LOGFILEW Logfile);

typedef ULONG (WINAPI * PEVENT_TRACE_BUFFER_CALLBACKA)
	(PEVENT_TRACE_LOGFILEA Logfile);

typedef VOID (WINAPI *PEVENT_CALLBACK)( PEVENT_TRACE pEvent );

typedef VOID (WINAPI *PEVENT_RECORD_CALLBACK) (PEVENT_RECORD EventRecord);


struct _EVENT_TRACE_LOGFILEW {
	LPWSTR                  LogFileName;      // Logfile Name
	LPWSTR                  LoggerName;       // LoggerName
	LONGLONG                CurrentTime;      // timestamp of last event
	ULONG                   BuffersRead;      // buffers read to date
	union {
		// Mode of the logfile
		ULONG               LogFileMode;
		// Processing flags used on Vista and above
		ULONG               ProcessTraceMode;
	} DUMMYUNIONNAME;
	EVENT_TRACE             CurrentEvent;     // Current Event from this stream.
	TRACE_LOGFILE_HEADER    LogfileHeader;    // logfile header structure
	PEVENT_TRACE_BUFFER_CALLBACKW             // callback before each buffer
		BufferCallback;   // is read
	//
	// following variables are filled for BufferCallback.
	//
	ULONG                   BufferSize;
	ULONG                   Filled;
	ULONG                   EventsLost;
	//
	// following needs to be propaged to each buffer
	//
	union {
		// Callback with EVENT_TRACE
		PEVENT_CALLBACK         EventCallback;
		// Callback with EVENT_RECORD on Vista and above
		PEVENT_RECORD_CALLBACK  EventRecordCallback;
	} DUMMYUNIONNAME2;

	ULONG                   IsKernelTrace;    // TRUE for kernel logfile

	PVOID                   Context;          // reserved for internal use
};

struct _EVENT_TRACE_LOGFILEA {
	LPSTR                   LogFileName;      // Logfile Name
	LPSTR                   LoggerName;       // LoggerName
	LONGLONG                CurrentTime;      // timestamp of last event
	ULONG                   BuffersRead;      // buffers read to date
	union {
		ULONG               LogFileMode;      // Mode of the logfile
		ULONG               ProcessTraceMode; // Processing flags
	} DUMMYUNIONNAME;
	EVENT_TRACE             CurrentEvent;     // Current Event from this stream
	TRACE_LOGFILE_HEADER    LogfileHeader;    // logfile header structure
	PEVENT_TRACE_BUFFER_CALLBACKA             // callback before each buffer
		BufferCallback;   // is read

	//
	// following variables are filled for BufferCallback.
	//
	ULONG                   BufferSize;
	ULONG                   Filled;
	ULONG                   EventsLost;
	//
	// following needs to be propaged to each buffer
	//
	union {
		PEVENT_CALLBACK         EventCallback;  // callback for every event
		PEVENT_RECORD_CALLBACK  EventRecordCallback;
	} DUMMYUNIONNAME2;


	ULONG                   IsKernelTrace;  // TRUE for kernel logfile

	PVOID                   Context;        // reserved for internal use
};
//////////////////////////////////////////////////////////////////////////
#define PEVENT_TRACE_BUFFER_CALLBACK    PEVENT_TRACE_BUFFER_CALLBACKW
#define EVENT_TRACE_LOGFILE             EVENT_TRACE_LOGFILEW
#define PEVENT_TRACE_LOGFILE            PEVENT_TRACE_LOGFILEW
#define KERNEL_LOGGER_NAME              KERNEL_LOGGER_NAMEW
#define GLOBAL_LOGGER_NAME              GLOBAL_LOGGER_NAMEW
#define EVENT_LOGGER_NAME               EVENT_LOGGER_NAMEW
//////////////////////////////////////////////////////////////////////////

#if RSG_XDK

//////////////////////////////////////////////////////////////////////////
// This code is not presented in XDK
// Source: <evntprov.h> from Windows SDK
//////////////////////////////////////////////////////////////////////////
typedef struct _EVENT_DESCRIPTOR {

	USHORT      Id;
	UCHAR       Version;
	UCHAR       Channel;
	UCHAR       Level;
	UCHAR       Opcode;
	USHORT      Task;
	ULONGLONG   Keyword;

} EVENT_DESCRIPTOR, *PEVENT_DESCRIPTOR;

typedef const EVENT_DESCRIPTOR *PCEVENT_DESCRIPTOR;

#define MAX_EVENT_FILTER_EVENT_ID_COUNT      (64) /*
    The maximum number of event IDs in an event ID or stackwalk filter.
    Used with EVENT_FILTER_DESCRIPTOR of type EVENT_FILTER_TYPE_EVENT_ID or
    EVENT_FILTER_TYPE_STACKWALK. */

/*
EVENT_FILTER_TYPE values for the Type field of EVENT_FILTER_DESCRIPTOR.
*/
#define EVENT_FILTER_TYPE_NONE               (0x00000000)
#define EVENT_FILTER_TYPE_SCHEMATIZED        (0x80000000) // Provider-side.
#define EVENT_FILTER_TYPE_SYSTEM_FLAGS       (0x80000001) // Internal use only.
#define EVENT_FILTER_TYPE_TRACEHANDLE        (0x80000002) // Initiate rundown.
#define EVENT_FILTER_TYPE_PID                (0x80000004) // Process ID.
#define EVENT_FILTER_TYPE_EXECUTABLE_NAME    (0x80000008) // EXE file name.
#define EVENT_FILTER_TYPE_PACKAGE_ID         (0x80000010) // Package ID.
#define EVENT_FILTER_TYPE_PACKAGE_APP_ID     (0x80000020) // Package Relative App Id (PRAID).
#define EVENT_FILTER_TYPE_PAYLOAD            (0x80000100) // TDH payload filter.
#define EVENT_FILTER_TYPE_EVENT_ID           (0x80000200) // Event IDs.
#define EVENT_FILTER_TYPE_EVENT_NAME         (0x80000400) // Event name (TraceLogging only).
#define EVENT_FILTER_TYPE_STACKWALK          (0x80001000) // Event IDs for stack.
#define EVENT_FILTER_TYPE_STACKWALK_NAME     (0x80002000) // Event name for stack (TraceLogging only).
#define EVENT_FILTER_TYPE_STACKWALK_LEVEL_KW (0x80004000) // Filter stack collection by level and keyword.

/*
EVENT_FILTER_EVENT_ID is used to pass EventId filter for
stack walk filters.
*/
typedef struct _EVENT_FILTER_EVENT_ID {
    BOOLEAN FilterIn;
    UCHAR Reserved;
    USHORT Count;
    USHORT Events[ANYSIZE_ARRAY];
} EVENT_FILTER_EVENT_ID, *PEVENT_FILTER_EVENT_ID;
//////////////////////////////////////////////////////////////////////////

#endif // RSG_XDK

//////////////////////////////////////////////////////////////////////////
// This code is not presented in XDK or GDK
// Source: <tdh.h> from Windows SDK
//////////////////////////////////////////////////////////////////////////
/*
InType provides basic information about the raw encoding of the data in the
field of an ETW event. An event field's InType tells the event decoder how to
determine the size of the field. In the case that a field's OutType is
NULL/unspecified/unrecognized, the InType also provides a default OutType for
the data (an OutType refines how the data should be interpreted). For example,
InType = INT32 indicates that the field's data is 4 bytes in length. If the
field's OutType is NULL/unspecified/unrecognized, the InType of INT32 also
provides the default OutType, TDH_OUTTYPE_INT, indicating that the field's data
should be interpreted as a Win32 INT value.

Note that there are multiple ways for the size of a field to be determined.

- Some InTypes have a fixed size. For example, InType UINT16 is always 2 bytes.
  For these fields, the length property of the EVENT_PROPERTY_INFO structure
  can be ignored by decoders.
- Some InTypes support deriving the size from the data content. For example,
  the size of a COUNTEDSTRING field is determined by reading the first 2 bytes
  of the data, which contain the size of the remaining string. For these
  fields, the length property of the EVENT_PROPERTY_INFO structure must be
  ignored.
- Some InTypes use the Flags and length properties of the EVENT_PROPERTY_INFO
  structure associated with the field. Details on how to do this are provided
  for each type.

For ETW InType values, the corresponding default OutType and the list of
applicable OutTypes can be found in winmeta.xml. For legacy WBEM InType values
(i.e. values not defined in winmeta.xml), the details for each InType are
included below.
*/
enum _TDH_IN_TYPE {
    TDH_INTYPE_NULL, /* Invalid InType value. */
    TDH_INTYPE_UNICODESTRING, /*
        Field size depends on the Flags and length fields of the corresponding
        EVENT_PROPERTY_INFO structure (epi) as follows:
        - If ((epi.Flags & PropertyParamLength) != 0), the
          epi.lengthPropertyIndex field contains the index of the property that
          contains the number of WCHARs in the string.
        - Else if ((epi.Flags & PropertyLength) != 0 || epi.length != 0), the
          epi.length field contains number of WCHARs in the string.
        - Else the string is nul-terminated (terminated by (WCHAR)0).
        Note that some event providers do not correctly nul-terminate the last
        string field in the event. While this is technically invalid, event
        decoders may silently tolerate such behavior instead of rejecting the
        event as invalid. */
    TDH_INTYPE_ANSISTRING, /*
        Field size depends on the Flags and length fields of the corresponding
        EVENT_PROPERTY_INFO structure (epi) as follows:
        - If ((epi.Flags & PropertyParamLength) != 0), the
          epi.lengthPropertyIndex field contains the index of the property that
          contains the number of BYTEs in the string.
        - Else if ((epi.Flags & PropertyLength) != 0 || epi.length != 0), the
          epi.length field contains number of BYTEs in the string.
        - Else the string is nul-terminated (terminated by (CHAR)0).
        Note that some event providers do not correctly nul-terminate the last
        string field in the event. While this is technically invalid, event
        decoders may silently tolerate such behavior instead of rejecting the
        event as invalid. */
    TDH_INTYPE_INT8,    /* Field size is 1 byte. */
    TDH_INTYPE_UINT8,   /* Field size is 1 byte. */
    TDH_INTYPE_INT16,   /* Field size is 2 bytes. */
    TDH_INTYPE_UINT16,  /* Field size is 2 bytes. */
    TDH_INTYPE_INT32,   /* Field size is 4 bytes. */
    TDH_INTYPE_UINT32,  /* Field size is 4 bytes. */
    TDH_INTYPE_INT64,   /* Field size is 8 bytes. */
    TDH_INTYPE_UINT64,  /* Field size is 8 bytes. */
    TDH_INTYPE_FLOAT,   /* Field size is 4 bytes. */
    TDH_INTYPE_DOUBLE,  /* Field size is 8 bytes. */
    TDH_INTYPE_BOOLEAN, /* Field size is 4 bytes. */
    TDH_INTYPE_BINARY, /*
        Field size depends on the OutType, Flags, and length fields of the
        corresponding EVENT_PROPERTY_INFO structure (epi) as follows:
        - If ((epi.Flags & PropertyParamLength) != 0), the
          epi.lengthPropertyIndex field contains the index of the property that
          contains the number of BYTEs in the field.
        - Else if ((epi.Flags & PropertyLength) != 0 || epi.length != 0), the
          epi.length field contains number of BYTEs in the field.
        - Else if (epi.OutType == IPV6), the field size is 16 bytes.
        - Else the field is incorrectly encoded. */
    TDH_INTYPE_GUID, /* Field size is 16 bytes. */
    TDH_INTYPE_POINTER, /*
        Field size depends on the eventRecord.EventHeader.Flags value. If the
        EVENT_HEADER_FLAG_32_BIT_HEADER flag is set, the field size is 4 bytes.
        If the EVENT_HEADER_FLAG_64_BIT_HEADER flag is set, the field size is 8
        bytes. Default OutType is HEXINT64. Other usable OutTypes include
        CODE_POINTER, LONG, UNSIGNEDLONG.
        */
    TDH_INTYPE_FILETIME,   /* Field size is 8 bytes. */
    TDH_INTYPE_SYSTEMTIME, /* Field size is 16 bytes. */
    TDH_INTYPE_SID, /*
        Field size is determined by reading the first few bytes of the field
        value to determine the number of relative IDs. */
    TDH_INTYPE_HEXINT32, /* Field size is 4 bytes. */
    TDH_INTYPE_HEXINT64, /* Field size is 8 bytes. */
    TDH_INTYPE_MANIFEST_COUNTEDSTRING, /*
        Supported in Windows 2018 Fall Update or later. This is the same as
        TDH_INTYPE_COUNTEDSTRING, but can be used in manifests.
        Field contains a little-endian 16-bit bytecount followed by a WCHAR
        (16-bit character) string. Default OutType is STRING. Other usable
        OutTypes include XML, JSON. Field size is determined by reading the
        first two bytes of the payload, which are then interpreted as a
        little-endian 16-bit integer which gives the number of additional bytes
        (not characters) in the field. */
    TDH_INTYPE_MANIFEST_COUNTEDANSISTRING, /*
        Supported in Windows 2018 Fall Update or later. This is the same as
        TDH_INTYPE_COUNTEDANSISTRING, but can be used in manifests.
        Field contains a little-endian 16-bit bytecount followed by a CHAR
        (8-bit character) string. Default OutType is STRING. Other usable
        OutTypes include XML, JSON, UTF8. Field size is determined by reading
        the first two bytes of the payload, which are then interpreted as a
        little-endian 16-bit integer which gives the number of additional bytes
        (not characters) in the field. */
    TDH_INTYPE_RESERVED24,
    TDH_INTYPE_MANIFEST_COUNTEDBINARY, /*
        Supported in Windows 2018 Fall Update or later.
        Field contains a little-endian 16-bit bytecount followed by binary
        data. Default OutType is HEXBINARY. Other usable
        OutTypes include IPV6, SOCKETADDRESS, PKCS7_WITH_TYPE_INFO. Field size
        is determined by reading the first two bytes of the payload, which are
        then interpreted as a little-endian 16-bit integer which gives the
        number of additional bytes in the field. */

    // End of winmeta intypes.
    // Start of TDH intypes for WBEM. These types cannot be used in manifests.

    TDH_INTYPE_COUNTEDSTRING = 300, /*
        Field contains a little-endian 16-bit bytecount followed by a WCHAR
        (16-bit character) string. Default OutType is STRING. Other usable
        OutTypes include XML, JSON. Field size is determined by reading the
        first two bytes of the payload, which are then interpreted as a
        little-endian 16-bit integer which gives the number of additional bytes
        (not characters) in the field. */
    TDH_INTYPE_COUNTEDANSISTRING, /*
        Field contains a little-endian 16-bit bytecount followed by a CHAR
        (8-bit character) string. Default OutType is STRING. Other usable
        OutTypes include XML, JSON, UTF8. Field size is determined by reading
        the first two bytes of the payload, which are then interpreted as a
        little-endian 16-bit integer which gives the number of additional bytes
        (not characters) in the field. */
    TDH_INTYPE_REVERSEDCOUNTEDSTRING, /*
        Deprecated. Prefer TDH_INTYPE_COUNTEDSTRING.
        Field contains a big-endian 16-bit bytecount followed by a WCHAR
        (16-bit little-endian character) string. Default OutType is STRING.
        Other usable OutTypes include XML, JSON. Field size is determined by
        reading the first two bytes of the payload, which are then interpreted
        as a big-endian 16-bit integer which gives the number of additional
        bytes (not characters) in the field. */
    TDH_INTYPE_REVERSEDCOUNTEDANSISTRING, /*
        Deprecated. Prefer TDH_INTYPE_COUNTEDANSISTRING.
        Field contains a big-endian 16-bit bytecount followed by a CHAR (8-bit
        character) string. Default OutType is STRING. Other usable OutTypes
        include XML, JSON, UTF8. Field size is determined by reading the first
        two bytes of the payload, which are then interpreted as a big-endian
        16-bit integer which gives the number of additional bytes in the
        field. */
    TDH_INTYPE_NONNULLTERMINATEDSTRING, /*
        Deprecated. Prefer TDH_INTYPE_COUNTEDSTRING.
        Field contains a WCHAR (16-bit character) string. Default OutType is
        STRING. Other usable OutTypes include XML, JSON. Field size is the
        remaining bytes of data in the event. */
    TDH_INTYPE_NONNULLTERMINATEDANSISTRING, /*
        Deprecated. Prefer TDH_INTYPE_COUNTEDANSISTRING.
        Field contains a CHAR (8-bit character) string. Default OutType is
        STRING. Other usable OutTypes include XML, JSON, UTF8. Field size is
        the remaining bytes of data in the event. */
    TDH_INTYPE_UNICODECHAR, /*
        Deprecated. Prefer TDH_INTYPE_UINT16 with TDH_OUTTYPE_STRING.
        Field contains a WCHAR (16-bit character) value. Default OutType is
        STRING. Field size is 2 bytes. */
    TDH_INTYPE_ANSICHAR, /*
        Deprecated. Prefer TDH_INTYPE_UINT8 with TDH_OUTTYPE_STRING.
        Field contains a CHAR (8-bit character) value. Default OutType is
        STRING. Field size is 1 byte. */
    TDH_INTYPE_SIZET, /*
        Deprecated. Prefer TDH_INTYPE_POINTER with TDH_OUTTYPE_UNSIGNEDLONG.
        Field contains a SIZE_T (UINT_PTR) value. Default OutType is HEXINT64.
        Field size depends on the eventRecord.EventHeader.Flags value. If the
        EVENT_HEADER_FLAG_32_BIT_HEADER flag is set, the field size is 4 bytes.
        If the EVENT_HEADER_FLAG_64_BIT_HEADER flag is set, the field size is
        8 bytes. */
    TDH_INTYPE_HEXDUMP, /*
        Deprecated. Prefer TDH_INTYPE_BINARY.
        Field contains binary data. Default OutType is HEXBINARY. Field size is
        determined by reading the first four bytes of the payload, which are
        then interpreted as a little-endian UINT32 which gives the number of
        additional bytes in the field. */
    TDH_INTYPE_WBEMSID /*
        Deprecated. Prefer TDH_INTYPE_SID.
        Field contains an SE_TOKEN_USER (security identifier) value. Default
        OutType is STRING (i.e. the SID will be converted to a string during
        decoding using ConvertSidToStringSid or equivalent). Field size is
        determined by reading the first few bytes of the field value to
        determine the number of relative IDs. Because the SE_TOKEN_USER
        structure includes pointers, decoding this structure requires accurate
        knowledge of the event provider's pointer size (i.e. from
        eventRecord.EventHeader.Flags). */
};

/*
OutType describes how to interpret a field's data. If a field's OutType is
not specified in the manifest, it defaults to TDH_OUTTYPE_NULL. If the field's
OutType is NULL, decoding should use the default OutType associated with the
field's InType.

Not all combinations of InType and OutType are valid, and event decoding tools
will only recognize a small set of InType+OutType combinations. If an
InType+OutType combination is not recognized by a decoder, the decoder should
use the default OutType associated with the field's InType (i.e. the decoder
should behave as if the OutType were NULL).
*/
enum _TDH_OUT_TYPE {
    TDH_OUTTYPE_NULL, /*
        Default OutType value. If a field's OutType is set to this value, the
        decoder should determine the default OutType corresponding to the
        field's InType and use that OutType when decoding the field. */
    TDH_OUTTYPE_STRING, /*
        Implied by the STRING, CHAR, and SID InType values. Applicable to the
        INT8, UINT8, UINT16 InType values. Specifies that the field should be
        decoded as text. Decoding depends on the InType. For INT8, UINT8, and
        ANSISTRING InTypes, the data is decoded using the ANSI code page of the
        event provider. For UINT16 and UNICODESTRING InTypes, the data is
        decoded as UTF-16LE. For SID InTypes, the data is decoded using
        ConvertSidToStringSid or equivalent. */
    TDH_OUTTYPE_DATETIME, /*
        Implied by the FILETIME and SYSTEMTIME InType values. Data is decoded
        as a date/time. FILETIME is decoded as a 64-bit integer representing
        the number of 100-nanosecond intervals since January 1, 1601.
        SYSTEMTIME is decoded as the Win32 SYSTEMTIME structure. In both cases,
        the time zone must be determined using other methods. (FILETIME is
        usually but not always UTC.) */
    TDH_OUTTYPE_BYTE, /*
        Implied by the INT8 InType value. Data is decoded as a signed integer. */
    TDH_OUTTYPE_UNSIGNEDBYTE, /*
        Implied by the UINT8 InType value. Data is decoded as an unsigned
        integer. */
    TDH_OUTTYPE_SHORT, /*
        Implied by the INT16 InType value. Data is decoded as a signed
        little-endian integer. */
    TDH_OUTTYPE_UNSIGNEDSHORT, /*
        Implied by the UINT16 InType value. Data is decoded as an unsigned
        little-endian integer. */
    TDH_OUTTYPE_INT, /*
        Implied by the INT32 InType value. Data is decoded as a signed
        little-endian integer. */
    TDH_OUTTYPE_UNSIGNEDINT, /*
        Implied by the UINT32 InType value. Data is decoded as an unsigned
        little-endian integer. */
    TDH_OUTTYPE_LONG, /*
        Implied by the INT64 InType value. Applicable to the INT32 InType value
        (i.e. to distinguish between the C data types "long int" and "int").
        Data is decoded as a signed little-endian integer. */
    TDH_OUTTYPE_UNSIGNEDLONG, /*
        Implied by the UINT64 InType value. Applicable to the UINT32 InType
        value (i.e. to distinguish between the C data types "long int" and
        "int"). Data is decoded as an unsigned little-endian integer. */
    TDH_OUTTYPE_FLOAT, /*
        Implied by the FLOAT InType value. Data is decoded as a
        single-precision floating-point number. */
    TDH_OUTTYPE_DOUBLE, /*
        Implied by the DOUBLE InType value. Data is decoded as a
        double-precision floating-point number. */
    TDH_OUTTYPE_BOOLEAN, /*
        Implied by the BOOL InType value. Applicable to the UINT8 InType value.
        Data is decoded as a Boolean (false if zero, true if non-zero). */
    TDH_OUTTYPE_GUID, /*
        Implied by the GUID InType value. Data is decoded as a GUID. */
    TDH_OUTTYPE_HEXBINARY, /*
        Not commonly used. Implied by the BINARY and HEXDUMP InType values. */
    TDH_OUTTYPE_HEXINT8, /*
        Specifies that the field should be formatted as a hexadecimal integer.
        Applicable to the UINT8 InType value. */
    TDH_OUTTYPE_HEXINT16, /*
        Specifies that the field should be formatted as a hexadecimal integer.
        Applicable to the UINT16 InType value. */
    TDH_OUTTYPE_HEXINT32, /*
        Not commonly used. Implied by the HEXINT32 InType value. Applicable to
        the UINT32 InType value. */
    TDH_OUTTYPE_HEXINT64, /*
        Not commonly used. Implied by the HEXINT64 InType value. Applicable to
        the UINT64 InType value. */
    TDH_OUTTYPE_PID, /*
        Specifies that the field is a process identifier. Applicable to the
        UINT32 InType value. */
    TDH_OUTTYPE_TID, /*
        Specifies that the field is a thread identifier. Applicable to the
        UINT32 InType value. */
    TDH_OUTTYPE_PORT, /*
        Specifies that the field is an Internet Protocol port number, specified
        in network byte order (big-endian). Applicable to the UINT16 InType
        value. */
    TDH_OUTTYPE_IPV4, /*
        Specifies that the field is an Internet Protocol V4 address. Applicable
        to the UINT32 InType value. */
    TDH_OUTTYPE_IPV6, /*
        Specifies that the field is an Internet Protocol V6 address. Applicable
        to the BINARY InType value. If the length of a field is unspecified in
        the EVENT_PROPERTY_INFO but the field's InType is BINARY and its
        OutType is IPV6, the field's length should be assumed to be 16 bytes. */
    TDH_OUTTYPE_SOCKETADDRESS, /*
        Specifies that the field is a SOCKADDR structure. Applicable to the
        BINARY InType value. Note that different address types have different
        sizes. */
    TDH_OUTTYPE_CIMDATETIME, /*
        Not commonly used. */
    TDH_OUTTYPE_ETWTIME, /*
        Not commonly used. Applicable to the UINT32 InType value. */
    TDH_OUTTYPE_XML, /*
        Specifies that the field should be treated as XML text. Applicable to
        the *STRING InType values. When this OutType is used, decoders should
        use standard XML decoding rules (i.e. assume a Unicode encoding unless
        the document specifies a different encoding in its encoding
        attribute). */
    TDH_OUTTYPE_ERRORCODE, /*
        Not commonly used. Specifies that the field is an error code of
        some type. Applicable to the UINT32 InType value. */
    TDH_OUTTYPE_WIN32ERROR, /*
        Specifies that the field is a Win32 error code. Applicable to the
        UINT32 and HEXINT32 InType values. */
    TDH_OUTTYPE_NTSTATUS, /*
        Specifies that the field is an NTSTATUS code. Applicable to the UINT32
        and HEXINT32 InType values. */
    TDH_OUTTYPE_HRESULT, /*
        Specifies that the field is an HRESULT error code. Applicable to the
        INT32 InType value. */
    TDH_OUTTYPE_CULTURE_INSENSITIVE_DATETIME, /*
        Specifies that a date/time value should be formatted in a
        locale-invariant format. Applicable to the FILETIME and SYSTEMTIME
        InType values. */
    TDH_OUTTYPE_JSON, /*
        Specifies that the field should be treated as JSON text. Applicable to
        the *STRING InType values. When this OutType is used with the ANSI
        string InType values, decoders should decode the data as UTF-8. */
    TDH_OUTTYPE_UTF8, /*
        Specifies that the field should be treated as UTF-8 text. Applicable to
        the *ANSISTRING InType values. */
    TDH_OUTTYPE_PKCS7_WITH_TYPE_INFO, /*
        Specifies that the field should be treated as a PKCS#7 message (e.g.
        encrypted and/or signed). Applicable to the BINARY InType value. One
        or more bytes of TraceLogging-compatible type information (providing
        the type of the inner content) may optionally be appended immediately
        after the PKCS#7 message. For example, the byte 0x01
        (TlgInUNICODESTRING = 0x01) might be appended to indicate that the
        inner content is to be interpreted as InType = UNICODESTRING; the bytes
        0x82 0x22 (TlgInANSISTRING + TlgInChain = 0x82, TlgOutJSON = 0x22)
        might be appended to indicate that the inner content is to be
        interpreted as InType = ANSISTRING, OutType = JSON. */
    TDH_OUTTYPE_CODE_POINTER, /*
        Specifies that the field should be treated as an address that can
        potentially be decoded into a symbol name. Applicable to InTypes
        UInt32, UInt64, HexInt32, HexInt64, and Pointer. */

    // End of winmeta outtypes.
    // Start of TDH outtypes for WBEM.

    TDH_OUTTYPE_REDUCEDSTRING = 300, /*
        Not commonly used. */
    TDH_OUTTYPE_NOPRINT /*
        Not commonly used. Specifies that the field should not be shown in the
        output of the decoding tool. This might be applied to a Count or a
        Length field. Applicable to all InType values. Most decoders ignore
        this value. */
};

typedef struct _PROVIDER_EVENT_INFO
{
	ULONG NumberOfEvents;
	ULONG Reserved;
	_Field_size_(NumberOfEvents) EVENT_DESCRIPTOR EventDescriptorsArray[ANYSIZE_ARRAY];

} PROVIDER_EVENT_INFO;
typedef PROVIDER_EVENT_INFO *PPROVIDER_EVENT_INFO;

//
// Payload filtering definitions
//

//
// TDH_PAYLOADFIELD_OPERATORs are used to build Payload filters.
//
//    BETWEEN uses a closed interval: [LowerBound <= FieldValue <= UpperBound].
//    Floating-point comparisons are not supported.
//    String comparisons are case-sensitive.
//    Values are converted based on the manifest field type.
//

typedef enum _PAYLOAD_OPERATOR {

    //
    // For integers, comparison can be one of:
    //

    PAYLOADFIELD_EQ = 0,
    PAYLOADFIELD_NE = 1,
    PAYLOADFIELD_LE = 2,
    PAYLOADFIELD_GT = 3,
    PAYLOADFIELD_LT = 4,
    PAYLOADFIELD_GE = 5,
    PAYLOADFIELD_BETWEEN = 6,        // Two values: lower/upper bounds
    PAYLOADFIELD_NOTBETWEEN = 7,     // Two values: lower/upper bounds
    PAYLOADFIELD_MODULO = 8,         // For periodically sampling a field

    //
    // For strings:
    //

    PAYLOADFIELD_CONTAINS      = 20, // Substring identical to Value
    PAYLOADFIELD_DOESNTCONTAIN = 21, // No substring identical to Value

    //
    // For strings or other non-integer values
    //

    PAYLOADFIELD_IS    = 30,         // Field is identical to Value
    PAYLOADFIELD_ISNOT = 31,         // Field is NOT identical to Value
    PAYLOADFIELD_INVALID = 32
} PAYLOAD_OPERATOR;

typedef struct _PAYLOAD_FILTER_PREDICATE
{
	LPWSTR FieldName;
	USHORT CompareOp;    // PAYLOAD_OPERATOR
	LPWSTR Value;        // One or two values (i.e., two for BETWEEN operations)

} PAYLOAD_FILTER_PREDICATE, *PPAYLOAD_FILTER_PREDICATE;

#define MAX_PAYLOAD_PREDICATES 8

typedef enum _PROPERTY_FLAGS
{
   PropertyStruct        = 0x1,      // Type is struct.
   PropertyParamLength   = 0x2,      // Length field is index of param with length.
   PropertyParamCount    = 0x4,      // Count field is index of param with count.
   PropertyWBEMXmlFragment = 0x8,    // WBEM extension flag for property.
   PropertyParamFixedLength = 0x10,  // Length of the parameter is fixed.
   PropertyParamFixedCount = 0x20,   // Count of the parameter is fixed.
   PropertyHasTags       = 0x40,     // The Tags field has been initialized.
   PropertyHasCustomSchema = 0x80,   // Type is described with a custom schema.
} PROPERTY_FLAGS;

typedef struct _EVENT_PROPERTY_INFO {
    PROPERTY_FLAGS Flags;
    ULONG NameOffset;
    union {
        struct _nonStructType {
            USHORT InType;
            USHORT OutType;
            ULONG MapNameOffset;
        } nonStructType;
        struct _structType {
            USHORT StructStartIndex;
            USHORT NumOfStructMembers;
            ULONG padding;
        } structType;
        struct _customSchemaType {
            USHORT padding2;
            USHORT OutType;
            ULONG CustomSchemaOffset;
        } customSchemaType;
    };
    union {
        USHORT count;
        USHORT countPropertyIndex;
    };
    union {
        USHORT length;
        USHORT lengthPropertyIndex;
    };
    union {
        ULONG Reserved;
        struct {
            ULONG Tags : 28;
        };
    };
} EVENT_PROPERTY_INFO;
typedef EVENT_PROPERTY_INFO *PEVENT_PROPERTY_INFO;

typedef enum _DECODING_SOURCE {
    DecodingSourceXMLFile,
    DecodingSourceWbem,
    DecodingSourceWPP,
    DecodingSourceTlg,
    DecodingSourceMax
} DECODING_SOURCE;

/*
Values used in the TRACE_EVENT_INFO Flags field.
*/
typedef enum _TEMPLATE_FLAGS
{
    TEMPLATE_EVENT_DATA = 1, // Used when custom xml is not specified.
    TEMPLATE_USER_DATA = 2,  // Used when custom xml is specified.
    TEMPLATE_CONTROL_GUID = 4 // EventGuid contains the manifest control GUID.
} TEMPLATE_FLAGS;

typedef struct _TRACE_EVENT_INFO {

    GUID ProviderGuid; /* The meaning of this field depends on DecodingSource.
        - XMLFile: ProviderGuid contains the decode GUID (the provider GUID of
          the manifest).
        - Wbem: If EventGuid is GUID_NULL, ProviderGuid contains the decode
          GUID. Otherwise, ProviderGuid contains the control GUID.
        - WPP: ProviderGuid is not used (always GUID_NULL).
        - Tlg: ProviderGuid contains the control GUID. */

    GUID EventGuid; /* The meaning of this field depends on DecodingSource.
        - XMLFile: If the provider specifies a controlGuid, EventGuid contains
          the controlGuid and Flags contains TEMPLATE_CONTROL_GUID. Otherwise,
          if the event's Task specifies an eventGUID, EventGuid contains the
          eventGUID. Otherwise, EventGuid is GUID_NULL.
        - Wbem: If EventGuid is not GUID_NULL, it is the decode GUID.
        - WPP: EventGuid contains the decode GUID.
        - Tlg: EventGuid is not used (always GUID_NULL). */

    EVENT_DESCRIPTOR EventDescriptor;
    DECODING_SOURCE DecodingSource;
    ULONG ProviderNameOffset;
    ULONG LevelNameOffset;
    ULONG ChannelNameOffset;
    ULONG KeywordsNameOffset;

    ULONG TaskNameOffset; /* Meaning of this field depends on DecodingSource.
        - XMLFile: The offset to the name of the associated task.
        - Wbem: The offset to the name of the event.
        - WPP: Not used.
        - Tlg: The offset to the name of the event. */

    ULONG OpcodeNameOffset;
    ULONG EventMessageOffset;
    ULONG ProviderMessageOffset;
    ULONG BinaryXMLOffset;
    ULONG BinaryXMLSize;

    union {
        ULONG EventNameOffset; /* Event name for manifest-based events.
            This field is valid only if DecodingSource is set to
            DecodingSourceXMLFile or DecodingSourceTlg.

            EventNameOffset contains the offset from the beginning of this
            structure to a nul-terminated Unicode string that contains the
            event's name.

            This field will be 0 if the event does not have an assigned name or
            if this event is decoded on a system that does not support decoding
            manifest event names. Event name decoding is supported on Windows
            10 Fall Creators Update (2017) and later. */

        ULONG ActivityIDNameOffset; /* Activity ID name for WBEM events.
            This field is valid only if DecodingSource is set to
            DecodingSourceWbem.

            ActivityIDNameOffset contains the offset from the beginning of this
            structure to a nul-terminated Unicode string that contains the
            property name of the activity identifier in the MOF class. */
    };

    union {
        ULONG EventAttributesOffset; /* Attributes for manifest-based events.
            This field is valid only if DecodingSource is set to
            DecodingSourceXMLFile.

            EventAttributesOffset contains the offset from the beginning of
            this structure to a nul-terminated Unicode string that contains a
            semicolon-separated list of name=value attributes associated with
            the event.

            This field will be 0 if the event does not have attributes or if
            this event is decoded on a system that does not support decoding
            manifest event attributes. Attribute decoding is supported on
            Windows 10 Fall Creators Update (2017) and later.

            Defined attributes include:
            FILE=Filename of source code associated with event;
            LINE=Line number of source code associated with event;
            COL=Column of source code associated with event;
            FUNC=Function name associated with event;
            MJ=Major component associated with event;
            MN=Minor component associated with event.

            Values containing semicolons or double-quotes should be quoted
            using double-quotes. Double-quotes within the value should be
            doubled. Example string:
            FILE=source.cpp;LINE=123;MJ="Value; ""Quoted""" */

        ULONG RelatedActivityIDNameOffset; /* Related activity ID name (WBEM).
            This field is valid only if DecodingSource is set to
            DecodingSourceWbem.

            RelatedActivityIDNameOffset contains the offset from the beginning
            of this structure to a nul-terminated Unicode string that contains
            the property name of the related activity identifier in the MOF
            class. */
    };

    ULONG PropertyCount;
    _Field_range_(0, PropertyCount) ULONG TopLevelPropertyCount;
    union {
        TEMPLATE_FLAGS Flags;
        struct {
            ULONG Reserved : 4; // TEMPLATE_FLAGS values
            ULONG Tags : 28;
        };
    };
    _Field_size_(PropertyCount) EVENT_PROPERTY_INFO EventPropertyInfoArray[ANYSIZE_ARRAY];
} TRACE_EVENT_INFO;
typedef TRACE_EVENT_INFO *PTRACE_EVENT_INFO;

typedef _Return_type_success_(return == ERROR_SUCCESS) ULONG TDHSTATUS;

typedef enum _TDH_CONTEXT_TYPE {
    TDH_CONTEXT_WPP_TMFFILE, /* LPCWSTR path to the TMF file for a WPP event. */
    TDH_CONTEXT_WPP_TMFSEARCHPATH, /* LPCWSTR semicolon-separated list of
        directories to search for the TMF file for a WPP event. Only files
        with the name [ProviderId].TMF will be found during the search. */
    TDH_CONTEXT_WPP_GMT, /* Integer value. If set to 1, the TdhGetWppProperty
        and TdhGetWppMessage functions will format a WPP event's timestamp in
        GMT. By default, the timestamp is formatted in local time. */
    TDH_CONTEXT_POINTERSIZE, /* Integer value, set to 4 or 8. Used when
        decoding POINTER or SIZE_T fields on WPP events that do not set a
        pointer size in the event header. If the event does not set a pointer
        size in the event header and this context is not set, the decoder will
        use the pointer size of the current process. */
    TDH_CONTEXT_PDB_PATH, /* LPCWSTR semicolon-separated list of PDB files
        to be search for decoding information when decoding an event using
        TdhGetWppProperty or TdhGetWppMessage. (Not used by TdhGetProperty
        or TdhGetEventInformation.) */
    TDH_CONTEXT_MAXIMUM
} TDH_CONTEXT_TYPE;

/*
Decoding configuration parameters used with TdhGetDecodingParameter,
TdhSetDecodingParameter, TdhGetEventInformation, TdhGetProperty,
TdhGetPropertySize, and TdhEnumerateProviderFilters.

Note that the TDH_CONTEXT_WPP_GMT and TDH_CONTEXT_PDB_PATH parameter types are
only used by TdhGetDecodingParameter and TdhSetDecodingParameter. They are
ignored by TdhGetEventInformation and TdhGetProperty.
*/
typedef struct _TDH_CONTEXT {
    ULONGLONG ParameterValue; /* For GMT or POINTERSIZE, directly stores the
        parameter's integer value. For other types, stores an LPCWSTR pointing
        to a nul-terminated string with the parameter value. */
    TDH_CONTEXT_TYPE ParameterType;
    ULONG ParameterSize; /* Reserved. Set to 0. */
} TDH_CONTEXT;
typedef TDH_CONTEXT *PTDH_CONTEXT;

typedef struct _EVENT_MAP_ENTRY {
    ULONG OutputOffset;
    union {
        ULONG Value;        // For ULONG value (valuemap and bitmap).
        ULONG InputOffset;  // For String value (patternmap or valuemap in WBEM).
    };
} EVENT_MAP_ENTRY;
typedef EVENT_MAP_ENTRY *PEVENT_MAP_ENTRY;

typedef enum _MAP_FLAGS {
    EVENTMAP_INFO_FLAG_MANIFEST_VALUEMAP = 0x1,
    EVENTMAP_INFO_FLAG_MANIFEST_BITMAP = 0x2,
    EVENTMAP_INFO_FLAG_MANIFEST_PATTERNMAP = 0x4,
    EVENTMAP_INFO_FLAG_WBEM_VALUEMAP = 0x8,
    EVENTMAP_INFO_FLAG_WBEM_BITMAP = 0x10,
    EVENTMAP_INFO_FLAG_WBEM_FLAG = 0x20,
    EVENTMAP_INFO_FLAG_WBEM_NO_MAP = 0x40
} MAP_FLAGS;

typedef enum _MAP_VALUETYPE {
    EVENTMAP_ENTRY_VALUETYPE_ULONG,
    EVENTMAP_ENTRY_VALUETYPE_STRING
}  MAP_VALUETYPE;

typedef struct _EVENT_MAP_INFO {
    ULONG NameOffset;
    MAP_FLAGS Flag;
    ULONG EntryCount;
    union {
        MAP_VALUETYPE MapEntryValueType;
        ULONG FormatStringOffset;
    };
    _Field_size_(EntryCount) EVENT_MAP_ENTRY MapEntryArray[ANYSIZE_ARRAY];
} EVENT_MAP_INFO;
typedef EVENT_MAP_INFO *PEVENT_MAP_INFO;
//////////////////////////////////////////////////////////////////////////

#if RSG_XDK
//////////////////////////////////////////////////////////////////////////
// This code is not presented in XDK
// Source: <evntprov.h> from Windows SDK
//////////////////////////////////////////////////////////////////////////
/*
EVENT_FILTER_DESCRIPTOR describes a filter data item for EnableTraceEx2.
*/
typedef struct _EVENT_FILTER_DESCRIPTOR {

	ULONGLONG   Ptr;  // Pointer to filter data. Set to (ULONGLONG)(ULONG_PTR)pData.
	ULONG       Size; // Size of filter data in bytes.
	ULONG       Type; // EVENT_FILTER_TYPE value.

} EVENT_FILTER_DESCRIPTOR, *PEVENT_FILTER_DESCRIPTOR;
//////////////////////////////////////////////////////////////////////////
#endif // RSG_XDK

//////////////////////////////////////////////////////////////////////////
// This code is not presented in XDK or GDK
// Source: <evntcons.h> from Windows SDK
//////////////////////////////////////////////////////////////////////////
#define EVENT_HEADER_EXT_TYPE_RELATED_ACTIVITYID   0x0001
#define EVENT_HEADER_EXT_TYPE_SID                  0x0002
#define EVENT_HEADER_EXT_TYPE_TS_ID                0x0003
#define EVENT_HEADER_EXT_TYPE_INSTANCE_INFO        0x0004
#define EVENT_HEADER_EXT_TYPE_STACK_TRACE32        0x0005
#define EVENT_HEADER_EXT_TYPE_STACK_TRACE64        0x0006
#define EVENT_HEADER_EXT_TYPE_PEBS_INDEX           0x0007
#define EVENT_HEADER_EXT_TYPE_PMC_COUNTERS         0x0008
#define EVENT_HEADER_EXT_TYPE_PSM_KEY              0x0009
#define EVENT_HEADER_EXT_TYPE_EVENT_KEY            0x000A
#define EVENT_HEADER_EXT_TYPE_EVENT_SCHEMA_TL      0x000B
#define EVENT_HEADER_EXT_TYPE_PROV_TRAITS          0x000C
#define EVENT_HEADER_EXT_TYPE_PROCESS_START_KEY    0x000D
#define EVENT_HEADER_EXT_TYPE_CONTROL_GUID         0x000E
#define EVENT_HEADER_EXT_TYPE_QPC_DELTA            0x000F
#define EVENT_HEADER_EXT_TYPE_MAX                  0x0010

typedef struct _EVENT_HEADER {

	USHORT              Size;                   // Event Size
	USHORT              HeaderType;             // Header Type
	USHORT              Flags;                  // Flags
	USHORT              EventProperty;          // User given event property
	ULONG               ThreadId;               // Thread Id
	ULONG               ProcessId;              // Process Id
	LARGE_INTEGER       TimeStamp;              // Event Timestamp
	GUID                ProviderId;             // Provider Id
	EVENT_DESCRIPTOR    EventDescriptor;        // Event Descriptor
	union {
		struct {
			ULONG       KernelTime;             // Kernel Mode CPU ticks
			ULONG       UserTime;               // User mode CPU ticks
		} DUMMYSTRUCTNAME;
		ULONG64         ProcessorTime;          // Processor Clock 
		// for private session events
	} DUMMYUNIONNAME;
	GUID                ActivityId;             // Activity Id

} EVENT_HEADER, *PEVENT_HEADER;

typedef struct _EVENT_HEADER_EXTENDED_DATA_ITEM {

	USHORT      Reserved1;                      // Reserved for internal use
	USHORT      ExtType;                        // Extended info type 
	struct {
		USHORT  Linkage             :  1;       // Indicates additional extended 
		// data item
		USHORT  Reserved2           : 15;
	};
	USHORT      DataSize;                       // Size of extended info data
	ULONGLONG   DataPtr;                        // Pointer to extended info data

} EVENT_HEADER_EXTENDED_DATA_ITEM, *PEVENT_HEADER_EXTENDED_DATA_ITEM;

typedef struct _EVENT_RECORD {

	EVENT_HEADER        EventHeader;            // Event header
	ETW_BUFFER_CONTEXT  BufferContext;          // Buffer context
	USHORT              ExtendedDataCount;      // Number of extended
	// data items
	USHORT              UserDataLength;         // User data length
	PEVENT_HEADER_EXTENDED_DATA_ITEM            // Pointer to an array of 
		ExtendedData;           // extended data items                                               
	PVOID               UserData;               // Pointer to user data
	PVOID               UserContext;            // Context from OpenTrace
} EVENT_RECORD, *PEVENT_RECORD;
//////////////////////////////////////////////////////////////////////////
#define PROCESS_TRACE_MODE_REAL_TIME                0x00000100
#define PROCESS_TRACE_MODE_RAW_TIMESTAMP            0x00001000
#define PROCESS_TRACE_MODE_EVENT_RECORD             0x10000000
//////////////////////////////////////////////////////////////////////////

# if RSG_XDK

//////////////////////////////////////////////////////////////////////////
// This functions is not presented in XDK headers, but they are 
// exported to the library, so we can declare them here and link to the 
// library implementation.	
// Source: <evntrace.h> from Windows SDK
//////////////////////////////////////////////////////////////////////////
EXTERN_C
ULONG
WMIAPI
EnableTraceEx2 (
    _In_ TRACEHANDLE TraceHandle,
    _In_ LPCGUID ProviderId,
    _In_ ULONG ControlCode,
    _In_ UCHAR Level,
    _In_ ULONGLONG MatchAnyKeyword,
    _In_ ULONGLONG MatchAllKeyword,
    _In_ ULONG Timeout,
    _In_opt_ PENABLE_TRACE_PARAMETERS_V2 EnableParameters
    );

EXTERN_C
	ULONG
	WMIAPI
	StartTraceW (
	_Out_ PTRACEHANDLE TraceHandle,
	_In_ LPCWSTR InstanceName,
	_Inout_ PEVENT_TRACE_PROPERTIES Properties
	);

EXTERN_C
	ULONG
	WMIAPI
	ControlTraceW (
	_In_ TRACEHANDLE TraceHandle,
	_In_opt_ LPCWSTR InstanceName,
	_Inout_ PEVENT_TRACE_PROPERTIES Properties,
	_In_ ULONG ControlCode
	);

EXTERN_C
	ULONG
	WMIAPI
	TraceSetInformation (
	_In_ TRACEHANDLE SessionHandle,
	_In_ TRACE_INFO_CLASS InformationClass,
	_In_reads_bytes_(InformationLength) PVOID TraceInformation,
	_In_ ULONG InformationLength
	);

EXTERN_C
	ULONG
	WMIAPI
	TraceQueryInformation (
	_In_ TRACEHANDLE SessionHandle,
	_In_ TRACE_INFO_CLASS InformationClass,
	_Out_writes_bytes_(InformationLength) PVOID TraceInformation,
	_In_ ULONG InformationLength,
	_Out_opt_ PULONG ReturnLength
	);
//////////////////////////////////////////////////////////////////////////
#define RegisterTraceGuids      RegisterTraceGuidsW
#define StartTrace              StartTraceW
#define ControlTrace            ControlTraceW
#define StopTrace               StopTraceW
#define QueryTrace              QueryTraceW
#define UpdateTrace             UpdateTraceW
#define FlushTrace              FlushTraceW
#define QueryAllTraces          QueryAllTracesW
//////////////////////////////////////////////////////////////////////////

#endif // RSG_XDK

EXTERN_C
ULONG
WMIAPI
ProcessTrace(
	_In_reads_(HandleCount) PTRACEHANDLE HandleArray,
	_In_ ULONG HandleCount,
	_In_opt_ LPFILETIME StartTime,
	_In_opt_ LPFILETIME EndTime
);

EXTERN_C
TRACEHANDLE
WMIAPI
OpenTraceW(
	_Inout_ PEVENT_TRACE_LOGFILEW Logfile
);

EXTERN_C
ULONG
WMIAPI
CloseTrace(
	_In_ TRACEHANDLE TraceHandle
);

#define OpenTrace               OpenTraceW

#endif // ((RSG_XDK || RSG_GDK) && !RSG_FINAL && !RSG_TOOL)

#endif //DURANGO_ETW_H
