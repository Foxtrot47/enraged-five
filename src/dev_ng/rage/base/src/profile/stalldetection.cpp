
#include "stalldetection.h"

#include "atl/array.h"
#include "atl/map.h"
#include "system/ipc.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/stack.h"
#include "system/timemgr.h"
#include "system/timer.h"

#if __XENON
#include "system/xtl.h"
#include <xbdm.h>
#endif



namespace rage {

#if STALL_DETECTION

#define FORCE_STALLS_NO_COMMANDLINE 0
PARAM(stalls, "Monitor stalls greater than param value");

// Information about our worker thread
static sysIpcCurrentThreadId sThreadId = sysIpcCurrentThreadIdInvalid;
static sysIpcThreadId sStallThread = sysIpcThreadIdInvalid;

// If true, we encountered a stall during this frame
static volatile bool sbStalledStack = false;

// Default # of ms threshold until a frame is considered stalled
static int s_StallWaitTime = 60;



struct StallInfo
{
	u32 m_iCount;
	u32 m_iIndex;

	StallInfo()
		:m_iCount(0)
		,m_iIndex(0)
	{	}

	StallInfo(int iIndex)
		:m_iCount(1)
		,m_iIndex(iIndex)
	{	}

	StallInfo(const StallInfo& rOther)
		:m_iCount(rOther.m_iCount)
		,m_iIndex(rOther.m_iIndex)
	{	}
};

static atMap<u32, StallInfo> sStallInfos;
static u32 s_StallStackTrace[32];
static sysTimer s_StallTimer;

// The update thread will signal this to tell the stall detection worker that a frame has started
sysIpcSema s_StallStartSema;

// The update thread will signal this to tell the stall detectionw worker that a frame has ended
sysIpcSema s_StallGateSema;



void pfStallDetection::StallDetectionWorker( void* /*data*/ )
{
	while (true)	//DEBUG only, not intended for real game!
	{
		//Wait for the main thread to start its loop and signal this thread
		sysIpcWaitSemaTimed(s_StallStartSema, sysIpcInfinite);

		//Kicked here by main thread passing the start, hang out for 30ms, capture the stack
		if (!sysIpcWaitSemaTimed(s_StallGateSema,s_StallWaitTime))
		{
#if !__FINAL
			DmSuspendThread((u32)sThreadId);
			sbStalledStack = true;	//Main thread will pick up on this at the end of the main loop and print out the stack
			memset(s_StallStackTrace, 0, sizeof(s_StallStackTrace));

			sysStack::CaptureStackTrace((u32)sThreadId, s_StallStackTrace, 32, 2);	//Cast is used in sysStack
			DmResumeThread((u32)sThreadId);
#endif
		}
	}
}

void pfStallDetection::DisplayLine(u32 addr,const char *sym,u32 offset) {
	Printf("\t%8x - %s+%x\n",addr,sym,offset);
}

void pfStallDetection::CheckStall(float fTotalTimeMS)
{
	if (sbStalledStack)
	{
		u32 uStack = 0;
		for (int i=0 ; i<32 ; i++)
		{
			uStack ^= uStack>>1;
			uStack ^= s_StallStackTrace[i];
		}

		StallInfo *pFound = sStallInfos.Access(uStack);
		Printf("---------------------------------------------\n");
		if (pFound)
		{
			Printf("%d[=%d (count %d)]\t\tLong frame detected @ frame %d\n", m_StallCount, pFound->m_iIndex, pFound->m_iCount, TIME.GetFrameCount());
		}
		else
		{
			Printf("%d\t\tLong frame detected @ frame %d\n", m_StallCount, TIME.GetFrameCount());
		}
		Printf("\t\t\tTotalTime was: %f\n", fTotalTimeMS);
		if(pFound)
		{
			Printf("Same stack as number %d (count is %d)\n", pFound->m_iIndex, pFound->m_iCount);
			Printf("---------------------------------------------\n");
			pFound->m_iCount+=1;
		}
		else
		{
			Printf("\t\t\tMain thread location at %dms follows\n", s_StallWaitTime);
			Printf("\t\t\tTotal unique stalls at: %d\n", sStallInfos.GetNumUsed());
			Printf("---------------------------------------------\n");
			NOTFINAL_ONLY(sysStack::PrintCapturedStackTrace(s_StallStackTrace, NELEM(s_StallStackTrace), DisplayLine));
			Printf("---------------------------------------------\n");
			StallInfo info(m_StallCount);
			sStallInfos[uStack] = info;
		}

		sbStalledStack = false;
		++m_StallCount;
	}
}



pfStallDetection::pfStallDetection()
: m_StallCount(0)
, m_StallDetectThisFrame(false)
{
}


void pfStallDetection::BeginFrame()
{
	s_StallTimer.Reset();

	if (PARAM_stalls.Get(s_StallWaitTime) || FORCE_STALLS_NO_COMMANDLINE)
	{
		//We'll need to check for stall detection at the end.
		sbStalledStack = false;

		// Don't call BeginFrame twice without EndFrame inbetween.
		Assert(!m_StallDetectThisFrame);
		m_StallDetectThisFrame = true;

		// If this is the first time we're running, create all the necessary IPC tools.
		if (sStallThread == sysIpcThreadIdInvalid)
		{
			s_StallStartSema = sysIpcCreateSema(false);
			s_StallGateSema = sysIpcCreateSema(false);
			sThreadId = sysIpcGetCurrentThreadId();
			sStallThread = sysIpcCreateThread(StallDetectionWorker, NULL, 32768, PRIO_ABOVE_NORMAL, "[RAGE] Stall Detection Thread");
		}

		//Signal the start semaphore
		sysIpcSignalSema(s_StallStartSema);
	}
}

void pfStallDetection::EndFrame()
{
	if (m_StallDetectThisFrame)
	{
		if (!sbStalledStack)
		{
			//Signal the gate semaphore
			sysIpcSignalSema(s_StallGateSema);
		}
		else
		{
			CheckStall(s_StallTimer.GetMsTime());
		}

		m_StallDetectThisFrame = false;
	}
}

#endif // STALL_DETECTION

} // namespace rage
