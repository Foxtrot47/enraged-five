#ifndef MEMORY_POOL_H
#define MEMORY_POOL_H

#if USE_PROFILER

#include "common.h"
#include "system/memory.h"
#include "system/new.h"

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void* RockyAllocate(size_t size);
void  RockyFree(const void* ptr);
rage::sysMemAllocator& GetProfilerAllocator();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct MemoryChunkBase
{
	MemoryChunkBase* next;
	MemoryChunkBase* prev;
	MemoryChunkBase() : next(0), prev(0) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class MemoryPoolBase
{
protected:
	MemoryChunkBase* root;
	MemoryChunkBase* chunk;
	uint index;
	uint chunkItemCount;
	size_t chunkTotalSize;

	bool isLooped;

	void Init(uint itemCount, size_t chunkSize)
	{
		index = itemCount;
		chunkItemCount = itemCount;
		chunkTotalSize = chunkSize;
	}
public:
	MemoryPoolBase() : root(0), index(0), chunk(0), isLooped(false), chunkItemCount(0), chunkTotalSize(0) {}
	~MemoryPoolBase() { Clear(false); }

	ROCKY_INLINE bool IsEmpty() const
	{
		return root == 0 || (root == chunk && index == 0);
	}

	// PURPOSE: Resets looped pool
	ROCKY_INLINE void ResetIsLooped()
	{
		isLooped = false;
	}

	// PURPOSE: Quick clear - doesn't call destructors
	//			Use only for POD types
	ROCKY_NOINLINE void Clear(bool preserveMemory = true)
	{
		isLooped = false;

		if (!preserveMemory)
		{
			if (root)
			{
				MemoryChunkBase* next = root;

				while (next != nullptr)
				{
					MemoryChunkBase* toDelete = next;
					next = toDelete->next;
					RockyFree(toDelete);
				}

				chunk = root = 0;
				index = chunkItemCount;
			}
		}
		else
		{
			index = 0;
			chunk = root;
		}
	}

	// PURPOSE: Try to allocate and append a new chunk
	//			Returns false if we are running out of memory
	ROCKY_NOINLINE bool TryAppendChunk()
	{
		// If we are already looped - we are short on memory, let's skip the allocation
		void* ptr = !isLooped ? RockyAllocate(chunkTotalSize) : nullptr;

		if (!ptr)
			return false;

		MemoryChunkBase* newChunk = rage_placement_new(ptr) MemoryChunkBase();

		MemoryChunkBase* lastChunk = chunk;
		while (lastChunk && lastChunk->next)
			lastChunk = lastChunk->next;

		if (lastChunk)
		{
			// Append the new chunk to the end of the list
			lastChunk->next = newChunk;
			newChunk->prev = lastChunk;
		}
		else
		{
			// First chunk - we need to do the full initialization
			root = newChunk;
			chunk = newChunk;
			index = 0;
		}

		return true;
	}

	ROCKY_NOINLINE bool AddChunk()
	{
		if (!chunk || !chunk->next)
		{
			if (TryAppendChunk())
			{
				// Check whether we have an extra link
				// If next == nullptr - it means that we have only one chunk currently in the list
				// So let's return success
				if (chunk->next == nullptr)
					return true;
			}
			else
			{
				isLooped = true;

				// No root? Nothing we could do about this
				if (root == nullptr || chunk == nullptr)
					return false;

				// We have only one chunk in the list - flag success
				if (root == chunk)
					return true;

				// Re-link the root chunk to the end of the list
				MemoryChunkBase* newChunk = root;
				root = root->next;
				root->prev = nullptr;

				chunk->next = newChunk;
				newChunk->prev = chunk;
				newChunk->next = nullptr;
			}
		}

		// Adjusting current chunk to the next link
		chunk = chunk->next;

		return true;
	}

	ROCKY_NOINLINE size_t Size() const
	{
		size_t count = 0;

		for (const MemoryChunkBase* it = root; it != chunk; it = it->next)
			count += chunkItemCount;

		return chunk ? (count + index) : count;
	}

	ROCKY_NOINLINE void Reserve(int count)
	{
		int chunkCount = 0;

		for (const MemoryChunkBase* it = root; it != nullptr; it = it->next)
			++chunkCount;

		int requestCount = (count + chunkItemCount - 1) / chunkItemCount;

		for (int i = chunkCount + 1; i <= requestCount; ++i)
			if (!TryAppendChunk())
				break;
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T, uint SIZE>
struct MemoryChunk : public MemoryChunkBase
{
	T data[SIZE];
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T, uint SIZE = 16>
class MemoryPool : public MemoryPoolBase
{
	typedef MemoryChunk<T, SIZE> Chunk;

	T fallback;

	ROCKY_INLINE Chunk* GetChunk() { return (Chunk*)chunk; }
	ROCKY_INLINE Chunk* GetRoot() { return (Chunk*)root; }
	ROCKY_INLINE const Chunk* GetChunk() const { return (Chunk*)chunk; }
	ROCKY_INLINE const Chunk* GetRoot() const { return (Chunk*)root; }
public:
	MemoryPool()
	{
		Init(SIZE, sizeof(Chunk));
	}

	ROCKY_INLINE T& Add()
	{
		if (GetChunk() != nullptr && index < SIZE)
			return GetChunk()->data[index++];

		if (AddChunk())
		{
			index = 0;
			return GetChunk()->data[index++];
		}

		return fallback;
	}

	ROCKY_INLINE void Push(const T& data)
	{
		Add() = data;
	}

	ROCKY_INLINE bool TryPop(const T* pData)
	{
		if (index > 0 && (&(GetChunk()->data[index - 1]) == pData))
		{
			if (--index == 0)
			{
				if (chunk->prev != nullptr)
				{
					chunk = chunk->prev;
					index = SIZE;
				}
			}
			return true;
		}
		return false;
	}

	static uint GetChunkSize()
	{
		return SIZE;
	}

	template<class Func>
	void ForEach(Func func) const
	{
		for (const Chunk* it = GetRoot(); it != GetChunk(); it = (Chunk*)(it->next))
			for (uint i = 0; i < SIZE; ++i)
				if (!func(it->data[i]))
					return;

		if (const Chunk* ch = GetChunk())
			for (uint i = 0; i < index; ++i)
				if (!func(ch->data[i]))
					return;
	}

	template<class Func>
	void ForEach(Func func)
	{
		for (Chunk* it = GetRoot(); it != GetChunk(); it = (Chunk*)(it->next))
			for (uint i = 0; i < SIZE; ++i)
				if (!func(it->data[i]))
					return;

		if (Chunk* ch = GetChunk())
			for (uint i = 0; i < index; ++i)
				if (!func(ch->data[i]))
					return;
	}

	template<class Func>
	void ForEachChunk(Func func) const
	{
		for (const Chunk* it = GetRoot(); it != GetChunk(); it = (Chunk*)(it->next))
			func(it->data, SIZE);

		if (GetChunk() && index > 0)
			func(GetChunk()->data, index);
	}

	template<class Func>
	void ForEachChunk(Func func)
	{
		for (Chunk* it = GetRoot(); it != GetChunk(); it = (Chunk*)(it->next))
			func(it->data, SIZE);

		if (GetChunk() && index > 0)
			func(GetChunk()->data, index);
	}

	void ToArray(T* destination) const
	{
		uint curIndex = 0;

		for (const Chunk* it = GetRoot(); it != GetChunk(); it = (const Chunk*)(it->next))
		{
			memcpy(&destination[curIndex], it->data, sizeof(T) * SIZE);
			curIndex += SIZE;
		}

		if (GetChunk() && index > 0)
		{
			memcpy(&destination[curIndex], GetChunk()->data, sizeof(T) * index);
		}
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER

#endif //MEMORY_POOL_H
