#ifndef PROFILER_SETTINGS_H
#define PROFILER_SETTINGS_H

#include "file/file_config.h"

//////////////////////////////////////////////////////////////////////////
// PURPOSE: Change this value to 1 to compile out Rocky in all configs.	
//			Use this define for temporary disabling Rocky to measure it's overhead.
#define DISABLE_ROCKY 0
#define OVERRIDE_ROCKY 0
//////////////////////////////////////////////////////////////////////////

// PROFILING LEVELS
#define ROCKY_LEVEL_NONE			0
#define ROCKY_LEVEL_FRAME			1 // Collects only MAIN FRAME
#define ROCKY_LEVEL_BASIC			2 // Collects only PROFILER_CATEGORY macros (Budgets)
#define ROCKY_LEVEL_NORMAL			3 // Collects all PROFILE, PROFILER_EVENT and TAG macros (default mode for all of the configs)
#define ROCKY_LEVEL_DETAILED		4 // Collects all the detailed macros


#if (DISABLE_ROCKY || OVERRIDE_ROCKY || __FINAL || __TOOL || __GAMETOOL || __RESOURCECOMPILER)
#define USE_PROFILER						0
#define DEBUG_CPU_GPU_TIME_SYNC				0
#else
#define USE_PROFILER						(RSG_PC ? ROCKY_LEVEL_NORMAL : 0)
#define DEBUG_CPU_GPU_TIME_SYNC				0
#endif

#define USE_PROFILER_FRAME		(USE_PROFILER >= ROCKY_LEVEL_FRAME)
#define USE_PROFILER_BASIC		(USE_PROFILER >= ROCKY_LEVEL_BASIC)
#define USE_PROFILER_NORMAL		(USE_PROFILER >= ROCKY_LEVEL_NORMAL)
#define USE_PROFILER_DETAILED	(USE_PROFILER >= ROCKY_LEVEL_DETAILED)

#if USE_PROFILER
#define PROFILER_ONLY(...)   __VA_ARGS__
#define USE_PROFILER_BANDWIDTH_TRACKING 1
#else
#define PROFILER_ONLY(...)
#define USE_PROFILER_BANDWIDTH_TRACKING 0
#endif

#if USE_PROFILER_BANDWIDTH_TRACKING
#define PROFILER_BANDWIDTH_TRACKING_ONLY(...)   __VA_ARGS__
#else
#define PROFILER_BANDWIDTH_TRACKING_ONLY(...)
#endif

#if USE_PROFILER_BASIC
#define PROFILER_BASIC_ONLY(...)   __VA_ARGS__
#else
#define PROFILER_BASIC_ONLY(...)
#endif

#if USE_PROFILER_NORMAL
#define PROFILER_NORMAL_ONLY(...)   __VA_ARGS__
#else
#define PROFILER_NORMAL_ONLY(...)
#endif

#if USE_PROFILER_DETAILED
#define PROFILER_DETAILED_ONLY(...)   __VA_ARGS__
#else
#define PROFILER_DETAILED_ONLY(...)
#endif


// PURPOSE: Collects MT Resource usage
#define USE_PROFILER_FOR_MT_RESOURCES (USE_PROFILER && RSG_BANK)

//////////////////////////////////////////////////////////////////////////
#endif
