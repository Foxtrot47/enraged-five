#ifndef EVENT_TRACER_H
#define EVENT_TRACER_H

#if USE_PROFILER 

#include "atl/array.h"
#include "atl/string.h"
#include "types.h"
#include "memory_pool.h"
#include "serialization.h"

#include "system/ipc.h"

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma pack(push)
#pragma pack(1)
struct SwitchContextData
{
	rage::u32 timestamp;
	rage::u32 oldThreadID;
	rage::u32 newThreadID;

	rage::u16 oldThreadPriority;
	rage::u16 newThreadPriority;

	rage::u8 cpuID;
	rage::u8 reason;

	SwitchContextData() {}
	SwitchContextData(rage::u32 time, rage::u32 prevThreadID, rage::u32 currThreadID, rage::u8 switchReason) :
		timestamp(time),
		oldThreadID(prevThreadID),
		newThreadID(currThreadID),
		oldThreadPriority(0),
		newThreadPriority(0),
		cpuID((rage::u8)-1),
		reason(switchReason)
	{}
};
#pragma pack(pop)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct StackWalkData
{
	CallStackEntry entry;
	rage::u64 threadID;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma pack(push)
#pragma pack(1)
struct HardwareEventData
{
	rage::u64 threadID;
	Timestamp time;
	rage::u64 values[ROCKY_HARDWARE_EVENT_COUNT];
};
struct PageFaultData : public EventTime
{
	rage::u64 threadID;
	rage::u64 virtualAddress;
	rage::u64 readOffset;
	rage::u32 byteCount;
};
#pragma pack(pop)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
enum HWCounterType
{
	DEFAULT			= 0,
	TOTAL_CYCLES	= (1 << 0),
	WAIT_CYCLES		= (1 << 1),
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct HWEventDescription
{
	rage::atString name;
	rage::atString description;

	rage::u32 cycleMultiplier;
	rage::u32 counterMask;
	rage::u32 color;

	struct UnpackedCode
	{
		rage::u64 EventSelect0 : 8;
		rage::u64 UnitMask : 8;
		rage::u64 ZeroPadding0 : 16;
		rage::u64 EventSelect1 : 4;
		rage::u64 ZeroPadding1 : 28;
	};

	union
	{
		rage::u64 code;
		UnpackedCode unpackedCode;
	};
	
	void Set(rage::u64 c, rage::u64 unitMask)
	{
		unpackedCode.EventSelect0 = c & 0xFF;
		unpackedCode.UnitMask = unitMask;
		unpackedCode.ZeroPadding0 = 0;
		unpackedCode.EventSelect1 = (c >> 8) & 0xF;
		unpackedCode.ZeroPadding1 = 0;
	}

	HWEventDescription(const char* eventName, const char* desc, rage::u32 counterType, rage::u32 counterColor, rage::u64 c, rage::u64 unitMask = 0, rage::u32 cycleMul = 0) 
		: name(eventName), description(desc), cycleMultiplier(cycleMul), counterMask(counterType), color(counterColor)
	{
		Set(c, unitMask);
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const SwitchContextData& ob);
OutputDataStream& operator<<(OutputDataStream& stream, const HardwareEventData& ob);
OutputDataStream& operator<<(OutputDataStream& stream, const HWEventDescription& ob);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef MemoryPool<SwitchContextData, 1024 * 128> SynchronizationBuffer;
typedef MemoryPool<StackWalkData, 1024 * 8> StackWalkBuffer;
typedef MemoryPool<PageFaultData, 1024 * 8> PageFaultBuffer;
typedef MemoryPool<HardwareEventData, 1024 * 8> HardwareEventsBuffer;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EventTracer
{
	enum Status
	{
		OK = 0,
		ETW_ERROR_ALREADY_EXISTS = 1,
		ETW_ERROR_ACCESS_DENIED = 2,
		ETW_FAILED = 3,
		NOT_SUPPORTED_YET = 4,

		SCE_CREATE_FAILED = 5,
		SCE_ENABLE_SWITCH_CONTEXT_FAILED = 6,
		SCE_ENABLE_SAMPLING_FAILED = 7,
		SCE_START_FAILED = 8,

	};

	enum Mode
	{
		MODE_NONE			= 0,
		MODE_SAMPLING		= (1 << 0),
		MODE_SWITCH_CONTEXT = (1 << 1),
		MODE_AUTOSAMPLING	= (1 << 2),
		MODE_HW_EVENTS		= (1 << 3),
		MODE_LIVE			= (1 << 4),
		MODE_PROCESS		= (1 << 5),
		MODE_PAGE_FAULTS	= (1 << 6),
		MODE_SYS_CALLS		= (1 << 7),
		MODE_SWITCH_CONTEXT_CALLSTACKS = (1 << 8),

		MODE_FULL = 0xFFFFFFFF,
	};


	SynchronizationBuffer liveSwitchContextEvents[LIVE_EVENT_BUFFER_COUNT];
	SynchronizationBuffer switchContextEvents;
	SynchronizationBuffer gpuSwitchContextEvents;

	StackWalkBuffer stackwalkEvents;
	HardwareEventsBuffer hardwareEvents;
	PageFaultBuffer pageFaultEvents;

	rage::atArray<const HWEventDescription*> hardwareEventDescriptions;

	volatile rage::u8 liveBufferIndex;
	ROCKY_INLINE SynchronizationBuffer& GetFrontSynchronizationBuffer() { return liveSwitchContextEvents[liveBufferIndex]; }
	ROCKY_INLINE SynchronizationBuffer& GetBackSynchronizationBuffer() { return liveSwitchContextEvents[(liveBufferIndex + 1) % LIVE_EVENT_BUFFER_COUNT]; }
	void SwapLiveSynchronizationBuffer() { liveBufferIndex = (liveBufferIndex + 1) % LIVE_EVENT_BUFFER_COUNT; }

	Status Start(EventTracer::Mode mode);
	bool Stop();
	bool Flush(Profiler::Mode::Type mode, EventTime scope);
	void Clear(bool preserveMemory = true);

	void StartLive(LiveEventData& data);
	void StopLive(LiveEventData& data);

	Mode GetCurrentMode() const;

	PageFaultStats GetPageFaultStats() const;

	static int GetSamplingFrequency();

	static EventTracer& Get();
private:
	EventTracer() : liveBufferIndex(0) {};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct SwitchContextCustomReason
{
	enum Type
	{
		IdleGPU = 128,
	};
};

struct SwitchContextCustomCPUID
{
	enum Type
	{
		GPU = 128,
	};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void OnSwitchContextEvent(rage::u32 oldThreadID, rage::u32 newThreadID, rage::u64 timestamp, rage::u16 oldThreadPriority, rage::u16 newThreadPriority, rage::u8 reason, rage::u8 cpuID);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER
#endif //EVENT_TRACER_H