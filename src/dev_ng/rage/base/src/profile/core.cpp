#include "settings.h" 

#if USE_PROFILER

#include "common.h"
#include "core.h"
#include "event.h"
#include "profiler_server.h"
#include "serialization.h"
#include "tracer_linux.h"
#include "event_description_board.h"
#include "run_description_board.h"

#include "diag/channel.h"
#include "data/base64.h"
#include "data/rson.h"
#include "input/pad.h"
#include "file/asset.h"
#include "file/device.h"
#include "math/amath.h"
#include "parser/tree.h"
#include "parser/manager.h"
#include "string/stringbuilder.h"
#include "string/stringutil.h"
#include "system/cache.h"
#include "system/dependencyscheduler.h"
#include "system/ipc.h"
#include "system/hangdetect.h"
#include "system/memmanager.h"
#include "system/param.h"
#include "system/service.h"
#include "system/threadregistry.h"
#include "system/simpleallocator.h"
#include "system/stack.h"
#include "system/stl_wrapper.h"
#include "system/threadtype.h"
//#include "system/countdownevent.h"
#include "vectormath/vec3v.h"
#include "file/default_paths.h"

//#include "file/paths.h"

#include <iomanip>
#include <regex>

#if RSG_ORBIS
#include <pthread.h>
#endif

#if RSG_LINUX
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <unistd.h>
#endif

#include "system/xtl.h"

#if RSG_PC
#include <psapi.h>
#endif

RAGE_DEFINE_CHANNEL(Rocky);

using namespace rage;

PARAM(rockyPort, "[Rocky] Bind port to use for profiler connections (default 31313)");
PARAM(rockyMemory, "[Rocky] Size of memory allocated for Rocky Profiler (MB)");
PARAM(rockyON, "[Rocky] Turns on Rocky automatically at startup");
PARAM(rockyFrameLimit, "[Rocky] Sets maximum number of frames in a capture (default no limit)");
PARAM(rockyOutputFile, "[Rocky] Output name for the Rocky Capture");
PARAM(rockyOutputFolder, "[Rocky] Output folder for the Rocky Capture");
PARAM(rockyThreads, "[Rocky] List of thread names to activate");
PARAM(rockySingleThreadUpdate, "[Rocky] Forces single-threaded update");
PARAM(rockySmoketest, "[Rocky] Enables automation mode");
PARAM(rockyScriptSamplingFrequency, "[Rocky] Sets Rocky script sampling frequency (5000 by default)");

PARAM(rockyTelemetryStartup, "[Rocky] Turns on Rocky telemetry at startup");
PARAM(rockyTelemetryKeywords, "[Rocky] Adds specified keywords to the Rocky telemetry");

PARAM(rockyPwd, "[Rocky] Root password from the target device");


#define USE_EVENT_TRACER (USE_PROFILER_BASIC)

// GTA5 compat for RDR button masking
namespace rage
{
	class eIoPad
	{
	public:
		enum ePadButtonMask
		{
			L2 = rage::ioPad::L2,
			R2 = rage::ioPad::R2,
			L1 = rage::ioPad::L1,
			R1 = rage::ioPad::R1,
			RUP = rage::ioPad::RUP,
			RRIGHT = rage::ioPad::RRIGHT,
			RDOWN = rage::ioPad::RDOWN,
			RLEFT = rage::ioPad::RLEFT,
			SELECT = rage::ioPad::SELECT,
			L3 = rage::ioPad::L3,
			R3 = rage::ioPad::R3,
			START = rage::ioPad::START,
			UP = rage::ioPad::LUP,
			LRIGHT = rage::ioPad::LRIGHT,
			LDOWN = rage::ioPad::LDOWN,
			LLEFT = rage::ioPad::LLEFT,
			TOUCH = rage::ioPad::TOUCH,
		};
	};
}

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static const short DEFAULT_PORT = 31313;
static const rage::u32 NETWORK_PROTOCOL_VERSION = 29;
static const AppDescription ROCKY_PROFILER_APP(RockyApp::ID, NETWORK_PROTOCOL_VERSION);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static const u32 ROCKY_START_STOP_PAD_MASK = (ioPad::LDOWN | ioPad::LRIGHT);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RockyMuter
{
	EventStorage* activeStorage;
	RockyMuter()
	{
		activeStorage = TLS::storage;
		TLS::storage = nullptr;
	}
	~RockyMuter()
	{
		TLS::storage = activeStorage;
	}
};
#define ROCKY_MUTE RockyMuter muter##__LINE__;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static const int PROFILER_HEAP_SIZE_MB = 64;
static const int PROFILER_HEAP_STARTUP_SIZE_MB = 512;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static rage::sysCriticalSectionToken s_memoryToken;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool IsCapturingStartup()
{
	return PARAM_rockyON.Get() || PARAM_rockyTelemetryStartup.Get();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
size_t GetProfilerHeapSize()
{
	int size = IsCapturingStartup() ? PROFILER_HEAP_STARTUP_SIZE_MB : PROFILER_HEAP_SIZE_MB;

	if (PARAM_rockyMemory.IsInitialized())
		PARAM_rockyMemory.Get(size);

	return (size_t)(size * 1024 * 1024);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __RESOURCECOMPILER
rage::sysMemAllocator& GetRockyDebugAllocator()
{
	static rage::sysMemSimpleAllocator allocator((int)GetProfilerHeapSize(), rage::sysMemSimpleAllocator::HEAP_DEBUG, false);
	return allocator;
}
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class RockyAllocator : public sysMemAllocator
{
	atArray<sysMemAllocator*> allocators;
	volatile size_t allocated;
	volatile size_t allocationsCount;
	volatile size_t allocationsOverhead;

	size_t heapSizeLimit;
public:
	RockyAllocator() : allocated(0), allocationsCount(0), allocationsOverhead(0), heapSizeLimit(GetProfilerHeapSize()) {}

	virtual void* TryAllocate(size_t size,size_t align,int heapIndex = 0) override
	{
		bool canAllocate = (size + allocated) < heapSizeLimit;
		if (!canAllocate)
			return nullptr;

		for (int i = 0; i < allocators.GetCount(); ++i)
		{
			sysMemAllocator* allocator = allocators[i];
			void* result = allocator->TryAllocate(size, align, heapIndex);
			if (result != nullptr)
			{
				sysInterlockedAdd(&allocated, size);
				sysInterlockedAdd(&allocationsCount, 1);
				return result;
			}
		}
		return nullptr;
	}

	virtual void* Allocate(size_t size, size_t align, int heapIndex = 0) override
	{
		return TryAllocate(size, align, heapIndex);
	}

	virtual void ROCKY_NOINLINE Free(const void *ptr) override
	{
		for (int i = 0; i < allocators.GetCount(); ++i)
		{
			sysMemAllocator* allocator = allocators[i];
			if (allocator->IsValidPointer(ptr))
			{
				sysMemAutoUseAllocator use(*allocator);
				size_t size = allocator->GetSize(ptr);
				allocator->Free(ptr);

				sysInterlockedAdd(&allocated, -(s64)size);
				return;
			}
		}
		rockyFatalAssertf(false, "Unknown de-allocation!");
	}

	virtual size_t GetMemoryUsed(int /*bucket = -1*/) override
	{
		return allocated;
	}

	virtual size_t GetMemoryAvailable() override
	{
		size_t available = 0;

		for (int i = 0; i < allocators.GetCount(); ++i)
			available += allocators[i]->GetMemoryAvailable();

		size_t maxAvailable = GetProfilerHeapSize() - GetMemoryUsed(0);

		return Min(available, maxAvailable);
	}

	size_t GetHeapSize() const override
	{
		return heapSizeLimit;
	}

	void Add(sysMemAllocator* allocator)
	{
		if (allocator != nullptr)
			allocators.PushAndGrow(allocator);
	}

	void AddOverhead(size_t ticks)
	{
		sysInterlockedAdd(&allocationsOverhead, ticks);
	}

	size_t GetAllocationsCount() const
	{
		return allocationsCount;
	}

	float GetAllocationsOverheadMs() const
	{
		return allocationsOverhead * rage::sysTimer::GetTicksToMilliseconds();
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static RockyAllocator* pAllocator = nullptr;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rage::sysMemAllocator& GetProfilerAllocator()
{
	SYS_CS_SYNC(s_memoryToken);

#if !__RESOURCECOMPILER
	if (pAllocator == nullptr)
	{
		USE_ROCKY_MEMORY;

		pAllocator = rage_new RockyAllocator();

#if (RSG_DURANGO || RSG_ORBIS)	
		// Adding additional memory for startup captures first
		// This allows to allocate as much memory as possible for startup captures within hemlis heap before carving chunks out of debug heap
		if (IsCapturingStartup())
		{
			// HEMLIS is PHYS_MEM_GARLIC_WRITECOMBINE, it's much slower for CPU access
			pAllocator->Add(MEMMANAGER.GetHemlisAllocator());
		}
#endif

		// Adding debug memory allocator
		pAllocator->Add(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL));

		// Test that allocator is working
		void* ptr = RockyAllocate(16);
		RockyFree(ptr);
	}

	return  *pAllocator;
#else
	return GetRockyDebugAllocator();
#endif
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void* RockyAllocate(size_t size)
{
	size_t start = rage::sysTimer::GetTicks();

	void* result = nullptr;
	// Limit memory for not rockyON builds
	{
		ROCKY_MUTE; // Mute all the events during allocation
		result = GetProfilerAllocator().Allocate(size, ROCKY_CACHE_LINE_SIZE);
	}

	size_t finish = rage::sysTimer::GetTicks();
	if (pAllocator)
	{
		pAllocator->AddOverhead(finish - start);
	}

	return result;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RockyFree(const void* ptr)
{
	ROCKY_MUTE; // Mute all the events during free
	GetProfilerAllocator().Free(ptr);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RockyShutdownMemory()
{
	SYS_CS_SYNC(s_memoryToken);
	if (pAllocator)
	{
		delete pAllocator;
		pAllocator = nullptr;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RockyMemoryStats
{
	size_t used;
	size_t total;

	RockyMemoryStats(size_t usedMemory, size_t totalMemory) : used(usedMemory), total(totalMemory) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RockyMemoryStats GetRockyMemoryStats()
{
	return RockyMemoryStats(GetProfilerAllocator().GetMemoryUsed(), GetProfilerAllocator().GetMemoryAvailable() + GetProfilerAllocator().GetMemoryUsed());
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct FilterDescriptionBoard {};
OutputDataStream & operator<<(OutputDataStream &stream, FilterDescriptionBoard);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<> rage::u32 EventBase<Mode::SWITCH_CONTEXT | Mode::INSTRUMENTATION_EVENTS>::TimeThreshold = 0;
template<> rage::u32 EventBase<Mode::MEMORY>::TimeThreshold = 0;
template<> rage::u32 EventBase<Mode::INSTRUMENTATION_EVENTS>::TimeThreshold = 0;
template<> rage::u32 EventBase<Mode::INSTRUMENTATION_CATEGORIES>::TimeThreshold = 0;
template<> rage::u32 EventBase<Mode::ALL>::TimeThreshold = 0;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ThreadDescription::ThreadDescription(const char* threadName, rage::u32 depth /*= 1*/, rage::s32 threadPriority /*= 0*/, rage::sysIpcCurrentThreadId id /*= sysIpcCurrentThreadIdInvalid*/, rage::u32 mask, rage::u32 pid)
	: maxDepth(depth), priority(threadPriority), threadId(id), systemThreadId((rage::u64)id), threadMask(mask), registered(true), processId(pid), baseAddress(0)
{
	name = threadName;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ThreadDescription::ThreadDescription()
	: maxDepth(1), priority(0), threadId(sysIpcCurrentThreadIdInvalid), systemThreadId((rage::u64)-1), threadMask(0), registered(false), processId((rage::u32)-1), baseAddress(0)
{

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::DumpProgress(const char* message)
{
	progressReportedLastTimestampMS = rage::sysTimer::GetSystemMsTime();

	if (server.IsLocalMode())
	{
		rockyDisplayf("%s", message);
	}
	else
	{
		OutputDataStream stream;
		stream << message;
		server.Send(RockyResponse::ReportProgress, stream);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool IsSleepDescription(const Profiler::EventDescription* desc)
{
	return desc->category.color == Color::White;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static const float MergeEventsThresholdMS = 5.0;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ThreadProcessingData
{
	ScopeData scope;
	EventTime timeSlice;
	ThreadEntry* entry;
	rage::sysDependency dependency;
	// rage::sysCountdownEvent* finishEvent;
	volatile rage::u32* eventsProcessed;

	void Process();
	void ProcessChunk(const EventData* &rootEvent, EventData* chunk, uint count);

	static bool AsyncDependency(const sysDependency& dep)
	{
#if defined(RDR_STYLE_DEPENDENCY) && RDR_STYLE_DEPENDENCY
		ThreadProcessingData* data = dep.GetPtr<0, ThreadProcessingData>();

#else
		ThreadProcessingData* data = (ThreadProcessingData* )dep.m_Params[0].m_AsPtr;
#endif
		Assertf(data, "Missing data bundle for Rocky threaded workload");
		data->Process();
		return true;
	}

	void Launch()
	{
#if defined(RDR_STYLE_DEPENDENCY) && RDR_STYLE_DEPENDENCY
		dependency.Init(AsyncDependency);
		dependency.Set<0>(this);
		dependency.SetPriority(sysBaseDependency::kPriorityCritical);
		dependency.SetCost(sysBaseDependency::kCostHigh);
#else
		dependency.Init(&ThreadProcessingData::AsyncDependency);
		dependency.m_Params[0].m_AsPtr = this;
		dependency.m_Priority = sysDependency::kPriorityCritical;
#endif
		sysDependencyScheduler::Insert(&dependency);
	}

	ThreadProcessingData() : eventsProcessed(nullptr), entry(nullptr)/*, finishEvent(nullptr)*/ {}
};

u32 OffsetTime(u32 time, u32 origin)
{
	return time - origin;
}

void ROCKY_NOINLINE ThreadProcessingData::ProcessChunk(const EventData* &rootEvent, EventData* chunk, uint count)
{
	// Adding origin for overlapping captures
	u32 origin = timeSlice.start;

	for (uint index = 0; index < count; ++index)
	{
		EventData& data = chunk[index];
		PrefetchDC(&data);

		if (data.finish == Timestamp::Invalid)
			data.finish = timeSlice.finish;

		if (OffsetTime(data.finish, origin) > OffsetTime(data.start, origin) && OffsetTime(data.start, origin) >= OffsetTime(timeSlice.start, origin) && OffsetTime(timeSlice.finish, origin) >= OffsetTime(data.finish, origin))
		{
			if (!rootEvent)
			{
				rootEvent = &data;
				scope.InitRootEvent(*rootEvent);
			}
			else if (OffsetTime(rootEvent->finish, origin) < OffsetTime(data.finish, origin))
			{
				// Let's try to merge small events together in a batches of 1ms of work
				// Sleep root events will get their own scope.
				if (Timestamp::ToMs(data.finish - scope.header.event.start) < MergeEventsThresholdMS
					&& !IsSleepDescription(data.description)
					&& !IsSleepDescription(rootEvent->description))
				{
					scope.AddEvent(data);
					scope.header.event.finish = data.finish;
				}
				else
				{
					scope.Send();
					scope.InitRootEvent(data);
				}

				rootEvent = &data;
			}
			else
			{
				scope.AddEvent(data);
			}
		}
	}

	rage::sysInterlockedAdd(eventsProcessed, count);
}

void ROCKY_NOINLINE ThreadProcessingData::Process()
{
	rage::sysMemAutoUseDebugMemory debugMem;

	EventStorage& storage = entry->storage;


	// Events
	if (!storage.eventBuffer.IsEmpty())
	{
		const EventData* rootEvent = nullptr;

		storage.eventBuffer.ForEachChunk([this, &rootEvent](EventData* chunk, uint count)
		{
			ProcessChunk(rootEvent, chunk, count);
		});
		scope.Send();

		scope.Clear(false);
		storage.ClearEvents(false);

		if (!storage.tags.IsEmpty() || !storage.tagsFloat.IsEmpty() || !storage.tagsU32.IsEmpty() ||
			!storage.tagsS32.IsEmpty() || !storage.tagsU64.IsEmpty() || !storage.tagsVec3.IsEmpty()
			|| !storage.tagsImage.IsEmpty() || !storage.tagsDrawcall.IsEmpty() || !storage.tagsString.IsEmpty())
		{
			OutputDataStream tagStream;
			tagStream << scope.header.boardNumber << scope.header.threadNumber;
			tagStream << storage.tags
				<< storage.tagsFloat
				<< storage.tagsU32
				<< storage.tagsS32
				<< storage.tagsU64
				<< storage.tagsVec3
				<< storage.tagsImage
				<< storage.tagsDrawcall
				<< storage.tagsString;
			Core::Get().GetServer().Send(RockyResponse::TagsPack, tagStream);
		}
		storage.ClearTags(false);
	}

#if USE_PROFILER_FOR_MT_RESOURCES
	// Resources
	if (!storage.resourceUsageBuffer.IsEmpty() || !storage.resourceSignalBuffer.IsEmpty())
	{
		OutputDataStream resourceStream;
		resourceStream << scope.header.boardNumber << scope.header.threadNumber;
		resourceStream << storage.resourceUsageBuffer << storage.resourceSignalBuffer;
		Core::Get().GetServer().Send(RockyResponse::ResourcePack, resourceStream);
		storage.ClearResources(false);
	}
#endif
	// finishEvent->Dec();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if USE_PROFILER_BANDWIDTH_TRACKING

void Core::AttachNetworkBandwidthData(rage::u32 len, const rage::u8* data)
{
    if (currentMode & Mode::NETWORK_BANDWIDTH_CAPTURE)
    {
        rockyFatalAssertf(sysThreadType::IsUpdateThread(), "m_NetworkBandwidthBuffer is not thread safe");
        if (len > 0 && rockyVerifyf(len < NetworkBandwidthEntry::MAX_BANDWIDTH_DATA, "AttachNetworkBandwidth storage too small. Trying to add data with len: %u. We might want to increase MAX_BANDWIDTH_DATA", len))
        {
            if (m_NetworkBandwidthLastBufferEntry != nullptr)
            {
                rage::u32 availableSpace = NetworkBandwidthEntry::MAX_BANDWIDTH_DATA - m_NetworkBandwidthLastBufferEntry->length;
                if (availableSpace > len)
                {
                    memcpy(&m_NetworkBandwidthLastBufferEntry->data[m_NetworkBandwidthLastBufferEntry->length], data, len);
                    m_NetworkBandwidthLastBufferEntry->length += len;
                    m_NetworkBandwidthLastBufferEntry->frameCount++;
                    return;
                }
            }

            NetworkBandwidthEntry& entry = m_NetworkBandwidthBuffer.Add();
            entry.length = len;
            entry.frameCount = 1;
            memcpy(entry.data, data, len);
            m_NetworkBandwidthLastBufferEntry = &entry;
        }
    }
}

void Core::DumpNetworkBandwidth(rage::u32 boardNumber)
{
    SYS_CS_SYNC(m_csThreadsToken);
    
    USE_ROCKY_MEMORY;
    DumpProgress("Collecting Network Bandwidth...");
    server.Flush();
    {
        DumpProgress("Sending Network Bandwidth...");
        server.Flush();
        if (!m_NetworkBandwidthBuffer.IsEmpty())
        {
            OutputDataStream netBandwidthBuffersStream;
            netBandwidthBuffersStream << boardNumber;
            u32 usedSpace = 0;
            u32 wastedSpace = 0;
            u32 frameCount = 0;
            m_NetworkBandwidthBuffer.ForEach([&](const NetworkBandwidthEntry& entry) -> bool
            {
                rockyAssert(entry.length > 0);
                frameCount += entry.frameCount;
                usedSpace += entry.length;
                wastedSpace += NetworkBandwidthEntry::MAX_BANDWIDTH_DATA - entry.length;
                netBandwidthBuffersStream.Write(entry.data, entry.length);
                return true;
            });
            u32 totalSpace = usedSpace + wastedSpace;
            if (totalSpace > 0)
            {
                rockyDisplayf("DumpNetworkBandwidth --> frame count: %u (average frame count per buffer entry: %f), data sent: %u bytes, wasted space: %u bytes (%f%% of total data (%u bytes)", frameCount, frameCount / (float)m_NetworkBandwidthBuffer.Size(), usedSpace, wastedSpace, wastedSpace / (float)totalSpace, totalSpace);
            }
            // We might want to split these into multiple packs if they become too big - not a problem at the moment even for 20 minutes captures
            server.Send(RockyResponse::NetworkBandwidthPack, netBandwidthBuffersStream);
            server.Flush();
        }
    }
}

#endif // USE_PROFILER_BANDWIDTH_TRACKING

void Core::CleanupNetworkBandwidth(
#if USE_PROFILER_BANDWIDTH_TRACKING
    bool bPreseverMemory
#else
    bool UNUSED_PARAM(bPreserveMemory)
#endif
)
{
#if USE_PROFILER_BANDWIDTH_TRACKING
    m_NetworkBandwidthBuffer.Clear(bPreseverMemory);
    m_NetworkBandwidthLastBufferEntry = nullptr;
#endif 
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::DumpThreadDescriptions(rage::u32 boardNumber)
{
	DumpProgress("Dumping Thread Descriptions...");
	OutputDataStream threadDescStream;
	threadDescStream << boardNumber;
	threadDescStream << processDescriptions;
	threadDescStream << threadDescriptions;
	server.Send(RockyResponse::ThreadDescriptionBoard, threadDescStream);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::DumpCallstacks(rage::u32 boardNumber)
{
	SYS_CS_SYNC(m_csThreadsToken);

	DumpProgress("Collecting Callstacks...");

	StackWalkBuffer& stackwalkBuffer = EventTracer::Get().stackwalkEvents;

	typedef rage::atMap<rage::u64, ThreadEntry*> ThreadMap;
	ThreadMap threadMap;
	for (int threadIndex = 0; threadIndex < threads.GetCount(); ++threadIndex)
	{
		threads[threadIndex]->storage.callstackBuffer.ResetIsLooped();
		threadMap.Insert(threads[threadIndex]->description.systemThreadId, threads[threadIndex]);
	}

	// Collect all the not registered threads from the current process
	rage::atArray<ThreadEntry*> threadMisc;
	rage::u32 currentProcessID = GetCurrentProcessId();
	for (int descIndex = 0; descIndex < threadDescriptions.GetCount(); ++descIndex)
	{
		const ThreadDescription& threadDesc = threadDescriptions[descIndex];
		if ((threadDesc.processId == currentProcessID) && (threadMap.Access(threadDesc.systemThreadId) == nullptr))
		{
			ThreadEntry* threadEntry = rage_aligned_new(ROCKY_CACHE_LINE_SIZE) ThreadEntry(threadDesc, nullptr);
			threadMisc.PushAndGrow(threadEntry);
			threadMap.Insert(threadDesc.systemThreadId, threadEntry);
		}
	}

	stackwalkBuffer.ForEach([&](const StackWalkData& event) -> bool
	{
		if (ThreadEntry** entry = threadMap.Access((rage::u64)event.threadID))
		{
			CallStackEntry& callstack = (*entry)->storage.callstackBuffer.Add();
			callstack = event.entry;
		}
		return true;
	});
	stackwalkBuffer.Clear(false);

	{
		DumpProgress("Resolving Symbols...");
		rage::stlUnorderedSet<rage::u64> codeAddresses;
		rage::stlUnorderedSet<rage::u64> scriptAddresses;

		for (int threadIndex = 0; threadIndex < threads.GetCount(); ++threadIndex)
		{
			threads[threadIndex]->storage.callstackBuffer.ForEach([&](const CallStackEntry& entry) -> bool
			{
				for (int i = 0; i < MAX_CALLSTACK_DEPTH; ++i)
				{
					if (!entry.callstack[i])
						break;

					if ((entry.mode & Mode::SCRIPTS_AUTOSAMPLING) == 0)
						codeAddresses.insert(entry.callstack[i]);
					else
						scriptAddresses.insert(entry.callstack[i]);
				}
				return true;
			});
		}

		Sampler::SymbolArray symbols;

		DumpProgress("Resolving C++ Symbols...");
		sampler.ResolveAddresses(codeAddresses, symbols);

		DumpProgress("Resolving C# Symbols...");
		sampler.ResolveScriptAddresses(scriptAddresses, symbols);

		DumpProgress("Sending Symbols...");

		OutputDataStream callstacksBoardStream;
		callstacksBoardStream << boardNumber;
		callstacksBoardStream << rage::sysParam::GetProgramName();
		callstacksBoardStream << sampler.symEngine.GetModules();
		callstacksBoardStream << symbols;
		server.Send(RockyResponse::CallstackDescriptionBoard, callstacksBoardStream);
		server.Flush();
		sampler.symEngine.Clear();
	}

	{
		DumpProgress("Sending Callstacks...");
		for (ThreadMap::Iterator it = threadMap.CreateIterator(); !it.AtEnd(); it.Next())
		{
			const CallStackEntryBuffer& callstacks = it.GetData()->storage.callstackBuffer;
			if (callstacks.IsEmpty())
				continue;

			OutputDataStream callstacksStream;
			callstacksStream << boardNumber << it.GetKey() << callstacks;
			server.Send(RockyResponse::CallstackPack, callstacksStream);
		}

		server.Flush();
	}

	// Cleaning temporary storages
	for (ThreadEntry* entry : threadMisc)
		delete entry;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::DumpSwitchContexts(rage::u32 boardNumber, SynchronizationBuffer& switchContexts)
{
	rage::u32 eventsCount = (rage::u32)switchContexts.Size();
	server.SendHeader(RockyResponse::SynchronizationData, sizeof(boardNumber) + sizeof(eventsCount) + eventsCount * sizeof(SwitchContextData));
	server.SendData(&boardNumber, sizeof(boardNumber));
	server.SendData(&eventsCount, sizeof(eventsCount));

	switchContexts.ForEachChunk([&](const SwitchContextData* data, uint count)
	{
		OutputDataStream synchronizationStream;
		for (uint i = 0; i < count; ++i)
			synchronizationStream << data[i];

		server.SendData(synchronizationStream);
	});

	switchContexts.Clear(true);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::DumpHardwareEvents(rage::u32 boardNumber)
{
	const atArray<const HWEventDescription*>& descriptions = EventTracer::Get().hardwareEventDescriptions;
	int descriptionsCount = descriptions.GetCount();
	DumpHardwareDescriptionBoard(boardNumber);

	DumpProgress("Sending Hardware Events Data...");
	HardwareEventsBuffer& hardwareEvents = EventTracer::Get().hardwareEvents;
	rage::u32 eventsCount = (rage::u32)hardwareEvents.Size();
	int samplingFrequency = EventTracer::GetSamplingFrequency();
	rage::u32 dataSize = eventsCount * (sizeof(rage::u64) + sizeof(rage::u32) + sizeof(rage::u64) * descriptionsCount); //ThreadID + Time + Count * Value
	server.SendHeader(RockyResponse::HWEventsPack, sizeof(boardNumber) + sizeof(samplingFrequency) + sizeof(descriptionsCount) + sizeof(eventsCount) + dataSize);
	server.SendData(&boardNumber, sizeof(boardNumber));
	server.SendData(&samplingFrequency, sizeof(samplingFrequency));
	server.SendData(&descriptionsCount, sizeof(descriptionsCount));
	server.SendData(&eventsCount, sizeof(eventsCount));

	hardwareEvents.ForEachChunk([&](const HardwareEventData* data, uint count)
	{
		OutputDataStream hardwareEventsStream;
		for (uint eventIndex = 0; eventIndex < count; ++eventIndex)
		{
			hardwareEventsStream << data[eventIndex].threadID;
			hardwareEventsStream << data[eventIndex].time;
			for (int valueIndex = 0; valueIndex < descriptionsCount; ++valueIndex)
				hardwareEventsStream << data[eventIndex].values[valueIndex];
		}

		server.SendData(hardwareEventsStream);
	});

	hardwareEvents.Clear(false);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::DumpHardwareDescriptionBoard(rage::u32 boardNumber)
{
	DumpProgress("Sending Hardware Events Description...");
	const atArray<const HWEventDescription*>& descriptions = EventTracer::Get().hardwareEventDescriptions;
	OutputDataStream hardwareEventsDescriptionStream;
	hardwareEventsDescriptionStream << boardNumber;
	hardwareEventsDescriptionStream << descriptions.GetCount();
	for (int i = 0; i < descriptions.GetCount(); ++i)
		hardwareEventsDescriptionStream << *descriptions[i];
	server.Send(RockyResponse::HWEventsDescriptionBoard, hardwareEventsDescriptionStream);
	server.Flush();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::DumpPageFaults(rage::u32 boardNumber)
{
	PageFaultBuffer& pageFaultEvents = EventTracer::Get().pageFaultEvents;

	if (pageFaultEvents.IsEmpty())
		return;

	DumpProgress("Sending Page Faults ...");

	rage::u32 eventsCount = (rage::u32)pageFaultEvents.Size();

	rage::u32 dataSize = eventsCount * sizeof(PageFaultData);
	server.SendHeader(RockyResponse::PageFaultsPack, sizeof(boardNumber) + sizeof(eventsCount) + dataSize);
	server.SendData(&boardNumber, sizeof(boardNumber));
	server.SendData(&eventsCount, sizeof(eventsCount));

	pageFaultEvents.ForEachChunk([&](const PageFaultData* data, uint count)
	{
		OutputDataStream pageFaultStream;
		pageFaultStream.Write((rage::u8*)data, sizeof(PageFaultData) * count);
		server.SendData(pageFaultStream);
	});

	pageFaultEvents.Clear(false);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int Core::GetNumCollectedFrames(FrameType type) const
{
	int index = 0;

	// Get the last valid frame
	for (int i = frames[type].GetCount() - 1; i >= 0; --i)
	{
		if (frames[type][i].event.finish != Timestamp::Invalid)
		{
			index = i + 1;
			break;
		}
	}

	return Max(0, type == FRAME_CPU ? index : index - 2);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static rage::u32 GetMaxValidTimestamp(rage::u32 a, rage::u32 b)
{
	rage::u32 result;
	if (a != Timestamp::Invalid && b != Timestamp::Invalid)
	{
		result = Max(a, b);
	}
	else if (a != Timestamp::Invalid)
	{
		result = a;
	}
	else
	{
		result = b;
	}
	return result;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rage::u32 Core::GetFirstValidFrameTime(FrameType type) const
{
	SYS_CS_SYNC(m_csThreadsToken);
	rage::u32 startTime = Timestamp::Invalid;
	bool hasOverlap = false;

	for (int i = 0; i < threads.GetCount(); ++i)
	{
		if ((type == FRAME_CPU && threads[i]->description.threadId == mainThreadID) || (type == FRAME_GPU && threads[i] == gpuThreadEntry))
		{
			EventDescription* mainFrameDescription = GetMainFrameEventDescription(type);

			threads[i]->storage.eventBuffer.ForEach([&](const EventData& data) -> bool
			{
				if (data.description == mainFrameDescription)
				{
					if (data.start < startTime)
					{
						if (startTime != Timestamp::Invalid)
							hasOverlap = true;

						startTime = data.start;
					}
				}
				return true;
			});
		}
	}

	return hasOverlap ? startTime : GetMaxValidTimestamp(frames[type].GetCount() > 0 ? frames[type][0].event.start : Timestamp::Invalid, startTime);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::MergeGPUEvents()
{
	USE_ROCKY_MEMORY;

	DumpProgress("Merging GPU Events...");

	SYS_CS_SYNC(m_csThreadsToken);

	u32 eventCount = 0;

	for (int i = 0; i < threads.GetCount(); ++i)
	{
		eventCount += (u32)threads[i]->storage.gpuBuffer.Size();
	}

	if (eventCount > 0)
	{
		atArray<EventData, 0, u32> gpuEvents;
		gpuEvents.Reserve(eventCount);

		for (int i = 0; i < threads.GetCount(); ++i)
		{
			ThreadEntry* entry = threads[i];

			if (!entry->storage.gpuBuffer.IsEmpty())
			{
				entry->storage.gpuBuffer.ForEach([&](const EventData& ev) -> bool
				{
					gpuEvents.Push(ev);
					return true;
				});

				entry->storage.gpuBuffer.Clear(false);
			}
		}

		gpuEvents.QSort(0, -1, [](const EventData* a, const EventData* b) -> int
		{
			if (a->start != b->start)
				return (a->start - b->start);

			if (a->finish != b->finish)
				return (b->finish - a->finish);

			if (a->description->budget > b->description->budget)
				return -1;
			else if (a->description->budget < b->description->budget)
				return 1;

			return 0;
		});

		gpuThreadEntry->storage.eventBuffer.Clear(true);
		gpuThreadEntry->storage.eventBuffer.Reserve(gpuEvents.GetCount());
		for (int i = 0; i < gpuEvents.GetCount(); i++)
			gpuThreadEntry->storage.NextEvent() = gpuEvents[i];
	}

	gpuVSyncThreadEntry->SortEvents();

	return eventCount > 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::DumpFrames(rage::u32 mode, bool singleThreaded)
{
	SYS_CS_SYNC(m_csThreadsToken);

	int frameCount = GetNumCollectedFrames(FRAME_CPU);

	if (!frameCount || threads.empty())
		return false;

	USE_ROCKY_MEMORY;

	rage::sysTimer dumpTimer;

	// VS: Temporary switching to non-realtime scheduler during shader initialization - causes hangs with realtime scheduler
	//	   Google is aware of the problem and they've already prepared a fix.
	//	   ETA for the new kernel rollout - end on November 2019.
#if RSG_GGP
	bool bResetRealtimePriority = sysThreadRegistry::SetDefaultSchedulerThreadPolicy(sysIpcSchedPolicy::POLICY_SCHED_OTHER);
#endif

	DumpProgress("Collecting Frame Events...");

	MergeGPUEvents();

	EventTime timeSlice[FRAME_COUNT];

	for (int i = 0; i < FRAME_COUNT; ++i)
	{
		int collectedCount = GetNumCollectedFrames((FrameType)i);

		timeSlice[i].start = GetFirstValidFrameTime((FrameType)i);
		timeSlice[i].finish = collectedCount > 0 ? frames[i][collectedCount-1].event.finish : Timestamp::Invalid;
	}

	if (!timeSlice[FRAME_GPU].IsValid())
		timeSlice[FRAME_GPU] = timeSlice[FRAME_CPU];

	rage::u32 boardNumber = DumpDescriptionBoard(timeSlice[FRAME_CPU], mode & ~Mode::LIVE);

	// EventFrames
	rage::atArray<ThreadProcessingData> taskArray;
	taskArray.Reserve(threads.GetCount());

	// rage::sysCountdownEvent completeEvent;

	rage::u32 eventTotal = 0;
	volatile rage::u32 eventProcessed = 0;

	for (int i = 0; i < threads.GetCount(); ++i)
	{
		ThreadEntry* entry = threads[i];
		if (!entry->storage.eventBuffer.IsEmpty())
		{
			ThreadProcessingData& task = taskArray.Append();
			task.entry = threads[i];
			task.timeSlice = timeSlice[(entry == gpuThreadEntry) ? FRAME_GPU : FRAME_CPU];
			task.scope.header.boardNumber = (rage::u32)boardNumber;
			task.scope.header.threadNumber = (rage::u32)i;
			task.eventsProcessed = &eventProcessed;
			// task.finishEvent = &completeEvent;
			eventTotal += (rage::u32)task.entry->storage.eventBuffer.Size();
			// completeEvent.Inc();
		}
	}

	for (int i = 0; i < taskArray.GetCount(); ++i)
	{
		if (singleThreaded)
			taskArray[i].Process();
		else
			taskArray[i].Launch();
	}

	int numIterations = 0;
	while (eventProcessed < eventTotal)
	{
		// Report progress only every 100ms (10ms x 10)
		while ((eventProcessed < eventTotal) && (numIterations++ % 10 != 0))
			sysIpcSleep(10);
		
		char msg[256];
		rage::u32 percent = (rage::u32)(100.0 * (double)eventProcessed / (double)eventTotal);
		sprintf(msg, "Processing Events %d%%", percent);
		DumpProgress(msg);
		HANG_DETECT_TICK();
	}

	// Check that we've completed all the tasks
	// completeEvent.Wait();

	// if (!singleThreaded)
	//	for (int i = 0; i < taskArray.GetCount(); ++i)
	//		taskArray[i].Wait(); // Add an event to wait on.

	server.Flush();

#if USE_PROFILER_BANDWIDTH_TRACKING
    DumpNetworkBandwidth(boardNumber);
#endif

#if USE_EVENT_TRACER
	DumpProgress("Flushing Event Tracer...");
	EventTracer::Get().Flush((Mode::Type)mode, timeSlice[FRAME_CPU]);
#endif

	DumpThreadDescriptions(boardNumber);

#if USE_EVENT_TRACER
	// Callstacks
	DumpProgress("Sending Callstacks Data...");
	DumpCallstacks(boardNumber);

	// Synchronization
	DumpProgress("Sending Synchronization Data...");
	EventTracer::Get().gpuSwitchContextEvents.ForEach([](SwitchContextData& data)->bool { EventTracer::Get().switchContextEvents.Push(data); return true; });
	DumpSwitchContexts(boardNumber, EventTracer::Get().switchContextEvents);

	// hardware events
	DumpProgress("Sending Hardware Counters...");
	DumpHardwareEvents(boardNumber);

	// page faults
	DumpPageFaults(boardNumber);
#endif

	server.Flush();

	rockyDisplayf("Finished saving the capture in %.3fs!", dumpTimer.GetTime());

#if RSG_GGP
	if (bResetRealtimePriority)
		sysThreadRegistry::SetDefaultSchedulerThreadPolicy(sysIpcSchedPolicy::POLICY_SCHED_RR);
#endif

	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::DumpSamplingData()
{
	if (sampler.GetCollectedCount() > 0)
	{
		USE_ROCKY_MEMORY;

		DumpProgress("Collecting Sampling Events...");

		OutputDataStream stream;
		sampler.Serialize(stream);

		DumpProgress("Sending Message With Sampling Data...");
		server.Send(RockyResponse::SamplingFrame, stream);

		return true;
	}
	return false;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::SetListenPort(int port)
{
	server.SetListenPort(port);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::SetSamplingFrequency(int frequency)
{
	samplingFrequency = frequency;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::SetScriptSamplingFrequency(int frequency)
{
	scriptSamplingFrequency = frequency;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::SetMaxCaptureFrameLimit(int limit)
{
	frameLimit = limit;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::SetRootPassword(RockyPlatform::Type ASSERT_ONLY(platform), const char* LINUX_ONLY(password))
{
	rockyFatalAssertf(platform == RockyPlatform::Linux, "Rocky doesn't support root password for any other platform than Linux");
#if RSG_LINUX
	FTrace::Get().SetRootPassword(password);
#endif
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::SetEncodedRootPassword(RockyPlatform::Type platform, const char* password)
{
	char decodedPass[256] = { 0 };
	u32 length = 0;
	if (datBase64::Decode(password, NELEM(decodedPass) - 1, (u8*)decodedPass, &length))
	{
		SetRootPassword(platform, decodedPass);
	}
	else
	{
		rockyFatalAssertf(false, "Failed to decode root password!");
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int Core::GetSamplingFrequency() const
{
	return samplingFrequency;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int Core::GetScriptSamplingFrequency() const
{
	return scriptSamplingFrequency;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool UpdateLiveDependency(const rage::sysDependency&);
bool UpdateAsyncDependency(const rage::sysDependency&);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const sysIpcCurrentThreadId sysIpcNullThreadId = (sysIpcCurrentThreadId)0;
const sysIpcCurrentThreadId sysIpcGpuThreadId = (sysIpcCurrentThreadId)-2;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool ThreadDescription::IsReal() const
{
	return (threadId != sysIpcCurrentThreadIdInvalid) && (threadId != sysIpcGpuThreadId);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Core::Core()
	: mainThreadID(sysIpcCurrentThreadIdInvalid)
	, currentMode(Mode::OFF)
	, pendingMode(Mode::OFF)
	, lastUsedMode(Mode::OFF)
	, progressReportedLastTimestampMS(0)
	, screenshotCallback(nullptr)
	, liveDataCallback(nullptr)
	, server(ROCKY_PROFILER_APP, DEFAULT_PORT)
	, samplingFrequency(1000)
	, scriptSamplingFrequency(5000)
	, frameLimit(0)
	, updateDependencyRunning(false)
{
	// randomize startup board number
	currentBoardNumber = rand();

	// Registering it automatically in the storage
	RegisterThread(ThreadDescription("GPU", 1, 0, sysIpcGpuThreadId, sysThreadType::THREAD_TYPE_RENDER));
	RegisterThread(ThreadDescription("VSync", 1));
	RegisterThread(ThreadDescription("I/O", 1));

	// Modifying thread settings to avoid conflicts with the main thread
	SYS_CS_SYNC( m_csThreadsToken );

	gpuThreadEntry = threads[0];
	gpuThreadEntry->threadTLS = NULL;

	gpuVSyncThreadEntry = threads[1];
	gpuVSyncThreadEntry->threadTLS = NULL;

	ioThreadEntry = threads[2];
	ioThreadEntry->threadTLS = NULL;

	RockyApp::RegisterMessages();

#if defined(RDR_STYLE_DEPENDENCY) && RDR_STYLE_DEPENDENCY
	liveDependency.Init(UpdateLiveDependency);
	liveDependency.SetPriority(rage::sysBaseDependency::kPriorityLow);

	updateDependency.Init(UpdateAsyncDependency);
	updateDependency.SetPriority(rage::sysBaseDependency::kPriorityLow);
	updateDependency.SetScheduler(sysBaseDependency::kSchedulerLazy);
#else
	liveDependency.Init(&UpdateLiveDependency);
	liveDependency.m_Priority = sysDependency::kPriorityLow;

	updateDependency.Init(&UpdateAsyncDependency);
	updateDependency.m_Priority = sysDependency::kPriorityLow;
#endif

	if (PARAM_rockyScriptSamplingFrequency.Get())
		PARAM_rockyScriptSamplingFrequency.Get(scriptSamplingFrequency);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static const int ROCKY_CS_UPDATE_TIMEOUT_MS = 5000;
static const Profiler::TagDescription* g_RockySystemTag = Profiler::TagDescription::CreateShared("Game\\Rocky");
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::Update(bool singleThreaded)
{
	USE_ROCKY_MEMORY;

	// bool locked = m_csCoreToken.TimedLockUsec(ROCKY_CS_UPDATE_TIMEOUT_MS * 1000);

	SYS_CS_SYNC( m_csCoreToken );
	
	if (PARAM_rockySingleThreadUpdate.Get())
		singleThreaded = true;

	// Wait for live dependency from the previous frame to finish
	// liveDependency.Wait();

	if (currentMode != Mode::OFF)
	{
		if (frames[FRAME_CPU].GetCount())
		{
			if (EventStorage* storage = TLS::storage)
				if (storage->mode & Mode::INSTRUMENTATION_EVENTS)
					while (!storage->startStopStack.empty())
						Event::Stop(storage->startStopStack.Pop());

			frames[FRAME_CPU].back().event.Stop();

			if (!frames[FRAME_CPU].back().event.IsValid())
			{
				PROFILER_LIVE_TAG(g_RockySystemTag, "Timer Reset! Ignore this spike!")
			}
		}

		if (currentMode & Mode::VIDEO_CAPTURE)
			if (EventStorage* storage = TLS::storage)
				AttachScreenshot(storage);
			
		if (IsTimeToReportProgress())
			DumpCapturingProgress();

		if (currentMode & Mode::LIVE)
		{
			UpdateLive(singleThreaded);
		}
		else if (frameLimit > 0 && frames[FRAME_CPU].GetCount() >= frameLimit)
		{
			if (IsCapturingStartup())
				DumpToFile(nullptr, true, singleThreaded);
			else
				RequestDump(Activate(Mode::OFF), singleThreaded);
		}
	}

	// If we have a pending dump request - we need to process it now
	if (dumpRequest.dumpMode != Mode::OFF)
		Dump();
	if (PARAM_rockyTelemetryStartup.Get() && currentMode == Mode::OFF)
	{
		pendingMode = Mode::DEFAULT_STARTUP;
		threadFilter.Add("MainThread");
		threadFilter.Add("Dependency");
		threadFilter.Add("I/O");
	}

	if (PARAM_rockyON.Get() && currentMode == Mode::OFF)
	{
		pendingMode = Mode::DEFAULT_STARTUP | Mode::INSTRUMENTATION_EVENTS | Mode::COMPACT | Mode::OTHER_PROCESSES | Mode::TAGS /*| Mode::SCRIPTS*/;
	}
	UpdateInput();

	if (pendingMode != currentMode)
	{
		u32 prevMode = Activate(pendingMode);
		if (prevMode != Mode::OFF)
			RequestDump(prevMode, singleThreaded);
	}
		
	UpdateEvents();

	if (currentMode != Mode::OFF)
	{
		ROCKY_MUTE;
		frames[FRAME_CPU].PushAndGrow(Frame(), 128);
		frames[FRAME_CPU].back().event.Start();
	}

	// if (locked)
		// m_csCoreToken.Unlock();

	// Check whether we've finished the previous update
	if (!updateDependencyRunning)
	{
		ROCKY_MUTE;
		updateDependencyRunning = true;
		sysDependencyScheduler::Insert(&updateDependency);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
TagDescription* GetFrameTagDescription()
{
	static TagDescription* frameTag = nullptr;
	if (frameTag == nullptr)
		frameTag = TagDescription::CreateShared("Frame");
	return frameTag;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::NextGpuFrame(rage::u32 frameNumber, rage::u64 timestamp)
{
	if (IsCollectingEvents(Mode::GPU))
	{
		USE_ROCKY_MEMORY;
		rage::u32 packedTime = Timestamp::Pack(timestamp);

		if (frames[FRAME_GPU].GetCount())
		{
			Frame& frame = frames[FRAME_GPU].back();
			frame.event.finish = packedTime;

			if (EventStorage* storage = TLS::storage)
			{
				storage->gpuBuffer.Push(EventData(GetMainFrameEventDescription(FRAME_GPU), frame.event.start, frame.event.finish));
			}

			gpuThreadEntry->storage.AddTag(GetFrameTagDescription(), packedTime, frameNumber);
		}
			
		frames[FRAME_GPU].PushAndGrow(Frame());
		frames[FRAME_GPU].back().event.start = packedTime + 1; // +1 to avoid overlapping with the previous frame
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::NextGpuFlip(const GPUFrame& gpuFrame)
{
	if (IsCollectingEvents(Mode::GPU))
	{
		USE_ROCKY_MEMORY;
		rage::u32 currentFlipTime = Timestamp::Pack(gpuFrame.m_Flip);
		rage::u32 prevFlipTime = Timestamp::Invalid;

		if (frames[FRAME_VSYNC].GetCount())
		{
			prevFlipTime = frames[FRAME_VSYNC].back().event.finish;
		}

		frames[FRAME_VSYNC].PushAndGrow(Frame());
		frames[FRAME_VSYNC].back().event = EventTime(prevFlipTime, currentFlipTime);

		if (currentFlipTime != prevFlipTime)
		{
			u32 startTimestamp = prevFlipTime + 1; // +1 to avoid overlapping with the previous frame

			// GPU Flip event
			gpuVSyncThreadEntry->storage.AddEvent(
				GetMainFrameEventDescription(FRAME_VSYNC), 
				startTimestamp,
				currentFlipTime
			);

			// Not all platforms provide this information.
			if (gpuFrame.m_Complete > 0)
			{
				// Present processing. Frame completed, waiting for command queue to reach the Present/Flip command.
				static EventDescription* presentProcessingEvent = EventDescription::Create("Waiting for Present Cmd", __FILE__, __LINE__, CategoryColor::None);
				gpuVSyncThreadEntry->storage.AddEvent(
					presentProcessingEvent,
					Timestamp::Pack(gpuFrame.m_Complete),
					Timestamp::Pack(gpuFrame.m_Processing)
				);
			}

			// Present command processed, wait for next Vsync to flip.
			static EventDescription* waitVSyncEvent = EventDescription::Create("Wait for Vsync", __FILE__, __LINE__, CategoryColor::Wait);
			gpuVSyncThreadEntry->storage.AddEvent(
				waitVSyncEvent, 
				Timestamp::Pack(gpuFrame.m_Processing) + 1,
				currentFlipTime
			);

			// Mark the time Vsync occurs with a tiny event so it's visible on the timeline, if provided.
			if (gpuFrame.m_VSync > 0)
			{
				static EventDescription* vSyncMarkerEvent = EventDescription::Create("VSync", __FILE__, __LINE__, CategoryColor::Wait);
				gpuVSyncThreadEntry->storage.AddEvent(
					vSyncMarkerEvent,
					Timestamp::Pack(gpuFrame.m_VSync),
					Timestamp::Pack(gpuFrame.m_VSync) + 1
				);
			}
		
			// Frame Tag
			gpuVSyncThreadEntry->storage.AddTag(
				GetFrameTagDescription(), 
				startTimestamp,
				gpuFrame.m_Frame
			);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::UpdateInput()
{
	// Check that full combo is pressed
	// Trigger if last button of the combo is pressed
	if ((ioPad::GetPad(0).GetButtons() & ROCKY_START_STOP_PAD_MASK) == ROCKY_START_STOP_PAD_MASK &&
		(ioPad::GetPad(0).GetPressedButtons() & ROCKY_START_STOP_PAD_MASK) != 0)
	{
		if (currentMode != Mode::OFF)
			pendingMode = Mode::OFF;
		else if (lastUsedMode != Mode::OFF)
			pendingMode = lastUsedMode;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma pack(push)
#pragma pack(1)
struct PackedLiveEventData
{
	EventTime time;
	rage::u64 hwEvents[ROCKY_HARDWARE_EVENT_COUNT];
	rage::u32 descriptionIndex;

	PackedLiveEventData(const LiveEventData& data) : time(data.start, data.finish), descriptionIndex(data.description->index)
	{
		if (!time.IsValid())
			time.finish = time.start;

		if (!data.description->IsLive())
			descriptionIndex = (rage::u32)-1;

		sysMemCpy(hwEvents, data.hwEvents, sizeof(rage::u64) * ROCKY_HARDWARE_EVENT_COUNT);
	}
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct PackedLiveTagData
{
	Timestamp time;
	rage::u32 descriptionIndex;
	RockyShortString value;

	PackedLiveTagData(const TagString& data) : time(data.time), descriptionIndex(data.description->index)
	{
		strcpy_s(value, 32, data.data);
	}
};
#pragma pack(pop)

bool UpdateLiveDependency(const rage::sysDependency& dep)
{
	PROFILER_CATEGORY("Rocky::UpdateLive", Profiler::CategoryColor::Debug);
	return Core::Get().UpdateLiveAsync(dep.m_Params[0].m_AsUInt);
}

bool UpdateAsyncDependency(const rage::sysDependency&)
{
	PROFILER_CATEGORY("Rocky::UpdateAsync", Profiler::CategoryColor::Debug);
	return Core::Get().UpdateAsync();
}

bool AppendToLiveBuffer(atArray<LiveData>& liveData, const LiveEventData& liveEvent)
{
	if (!liveEvent.IsValid())
		return false;

	float duration = Timestamp::ToMs(liveEvent.finish - liveEvent.start);

	// Linear search here - only few events are active at the moment
	for (int i = 0; i < liveData.GetCount(); ++i)
	{
		if (liveData[i].description == liveEvent.description)
		{
			liveData[i].duration += duration;
			return true;
		}
	}

	liveData.PushAndGrow(LiveData(liveEvent.description, duration));

	return false;
}

bool Core::UpdateAsync()
{
	// override the listen port if necessary
	// NOTE: we have to do this here because Core is statically initialised before the arguments are processed
	int port;
	if (PARAM_rockyPort.Get(port))
	{
		server.SetListenPort((short)port);
	}

	server.Update();

	updateDependencyRunning = false;

	return true;
}

bool Core::UpdateLiveAsync(u32 frameIdx)
{
	SYS_CS_SYNC(m_csThreadsToken);
	DoUpdateLive(frameIdx);
	// liveDepEvent.Dec();
	return true;
}

void Core::DoUpdateLive(u32 frameIdx)
{
	USE_ROCKY_MEMORY;

	bool updateGUI = (currentMode & Mode::LIVE_GUI) != 0;

	//=== Thread Events  ===//
	//=== Packing Scheme ===//
	//  u32: BoardNumber    //
	//  u32: FrameIndex     //
	//2xu32: FrameWindow	//
	//  u32: ThreadCount    //
	//  ForEachThread       //
	//  u32: ThreadIndex    //
	//  u32: EventCount     //
	//  ptr: LiveEventData  //
	//////////////////////////

	if (updateGUI)
	{
		// Calculate Output size
		size_t responseSize = sizeof(currentBoardNumber) + sizeof(u32) + sizeof(u32) + sizeof(EventTime);
		u32 threadCount = 0;

		for (ThreadEntry* entry : threads)
		{
			EventStorage::LiveBuffer& buffer = entry->storage.GetLiveBackBuffer();
			if (!buffer.events.IsEmpty())
			{
				threadCount += 1;
				responseSize += sizeof(rage::u32) + sizeof(rage::u32); // ThreadIndex + Event Count
				responseSize += buffer.events.Size() * sizeof(PackedLiveEventData);

				responseSize += sizeof(rage::u32); // Tag Count
				responseSize += buffer.tags.Size() * sizeof(PackedLiveTagData);
			}
		}

		PROFILER_TAG("ResponseSize", responseSize);

		// Send Data
		server.SendHeader(RockyResponse::LiveDataPack, responseSize);
		server.SendData(&currentBoardNumber, sizeof(currentBoardNumber));
		server.SendData(&frameIdx, sizeof(frameIdx));
		server.SendData(&frames[FRAME_CPU][frameIdx].event, sizeof(frames[FRAME_CPU][frameIdx].event));
		server.SendData(&threadCount, sizeof(threadCount));
	}

	liveDataBuffer.ResetCount();

	for (int threadIndex = 0; threadIndex < threads.GetCount(); ++threadIndex)
	{
		ThreadEntry* entry = threads[threadIndex];

		EventStorage::LiveBuffer& buffer = entry->storage.GetLiveBackBuffer();
		if (!buffer.events.IsEmpty())
		{
			rage::u32 eventCount = (rage::u32)buffer.events.Size();

			if (updateGUI)
				server.SendData(&threadIndex, sizeof(threadIndex));

			// Events
			if (updateGUI)
				server.SendData(&eventCount, sizeof(eventCount));

			buffer.events.ForEach([&](const LiveEventData& data)
			{
				PackedLiveEventData packedData(data);
	
				if (updateGUI)
					server.SendData(&packedData, sizeof(packedData));

				AppendToLiveBuffer(liveDataBuffer, data);

				return true;
			});
			buffer.events.Clear(true);

			// Tags
			if (updateGUI)
			{
				rage::u32 tagsCount = (rage::u32)buffer.tags.Size();
				server.SendData(&tagsCount, sizeof(tagsCount));
				buffer.tags.ForEach([&](const TagString& data)
				{
					PackedLiveTagData packedTag(data);
					server.SendData(&packedTag, sizeof(packedTag));
					return true;
				});
			}
			buffer.tags.Clear(true);
		}
	}

#if USE_EVENT_TRACER
	// Switch Contexts
	if ((currentMode & Mode::SWITCH_CONTEXT) != 0 && updateGUI)
	{
		PROFILER_EVENT("Core::DoUpdateLive_DumpSwitchContexts");

		SynchronizationBuffer& buffer = EventTracer::Get().GetBackSynchronizationBuffer();
		PROFILER_TAG("SwitchContextSize", buffer.Size() * sizeof(SwitchContextData));

		DumpSwitchContexts(currentBoardNumber, buffer);
	}
#endif

#if USE_PROFILER_BANDWIDTH_TRACKING
    if (!m_NetworkBandwidthBuffer.IsEmpty())
    {
        USE_ROCKY_MEMORY;
        m_NetworkBandwidthBuffer.ForEach([&](const NetworkBandwidthEntry& entry) -> bool
        {
            if (entry.length > 0)
            {
                OutputDataStream netBandwidthBuffersStream;
                netBandwidthBuffersStream.Write(entry.data, entry.length);
                server.Send(RockyResponse::NetworkBandwidthLive, netBandwidthBuffersStream);
            }
            return true;
        });
        CleanupNetworkBandwidth(true);
    }
#endif

	if (updateGUI)
	{
		PROFILER_EVENT("Core::DoUpdateLive_Flush");
		server.Flush();
	}

	if (liveDataCallback != nullptr)
		liveDataCallback(liveDataBuffer);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::UpdateLive(bool singleThreaded)
{
	SYS_CS_SYNC(m_csThreadsToken);

	for (ThreadEntry* entry : threads)
	{
		entry->storage.SwapLiveBuffer();
	}

	if (currentMode & Mode::SWITCH_CONTEXT)
	{
		EventTracer::Get().SwapLiveSynchronizationBuffer();
	}

	// NB: When UpdateLive() is called, all events in frames[FRAME_CPU] have completed.
	if (frames[FRAME_CPU].GetCount() >= LIVE_EVENT_FRAME_LATENCY)
	{
		if (singleThreaded)
		{
			ROCKY_MUTE;
			DoUpdateLive(frames[FRAME_CPU].GetCount() - LIVE_EVENT_FRAME_LATENCY);
		}
		else
		{
			ROCKY_MUTE;
			//liveDepEvent.Inc();
			liveDependency.m_Params[0].m_AsUInt = frames[FRAME_CPU].GetCount() - LIVE_EVENT_FRAME_LATENCY;
			rage::sysDependencyScheduler::Insert(&liveDependency);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::UpdateEvents()
{
	if (mainThreadID == sysIpcCurrentThreadIdInvalid)
		mainThreadID = rage::sysIpcGetCurrentThreadId();

	while (IMessage* msg = server.PopMessage())
	{
		msg->Apply();
		delete msg;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::StartSampling()
{
	SYS_CS_SYNC(m_csThreadsToken);
	sampler.StartSampling(threads);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::StartLive(rage::u32 mode)
{
	DumpDescriptionBoard(EventTime(0, 0xFFFFFFFF), mode);
	DumpHardwareDescriptionBoard(currentBoardNumber);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static const float AUTOMATIC_CALLSTACK_COLLECTION_THRESHOLD_MS = 0.015f;
static const float COMPACT_COLLECTION_THRESHOLD_MS = 0.03f;

bool Core::IsCurrentModeLive() const
{
    return (currentMode & Mode::LIVE) != 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AddInternalRunInfo()
{
	ProcessList processes;
	if (Core::Get().sampler.symEngine.GetProcesses(processes))
	{
		rage::atStringBuilder builder;
		builder.Append("Name\tPID\tWorkingSet(Bytes)\tCommitSize(Bytes)\n");
		for (int i = 0; i < processes.GetCount(); ++i)
		{
			const ProcessInfo& info = processes[i];
			char buf[512];
			formatf(buf, "%s\t%d\t%" I64FMT "u\t%" I64FMT "u\n", info.m_Name.c_str(), info.m_PID, info.m_WorkingSetSize, info.m_CommitSize);
			builder.Append(buf);
		}
		Profiler::AttachCaptureInfo("Processes", builder.ToString());
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rage::u32 Core::Activate(rage::u32 mode)
{
	if (currentMode != mode)
	{
		USE_ROCKY_MEMORY;

		EventStorage* storage = TLS::storage;
		(void)storage;

		if (mode != Mode::OFF) // Storing profiling session start time
		{
			rage::u64 currentFrequency = (rage::u64)(1.0f / rage::sysTimer::GetTicksToSeconds());
			rage::u64 ticksMS = (currentFrequency / 1000);
			EventBase<Mode::SWITCH_CONTEXT | Mode::INSTRUMENTATION_EVENTS>::TimeThreshold = (rage::u32)(((rage::u64)(ticksMS * AUTOMATIC_CALLSTACK_COLLECTION_THRESHOLD_MS)) >> Timestamp::TimePrecisionCut);

			if (mode & Mode::COMPACT)
			{
				rage::u32 threshold = (rage::u32)(((rage::u64)(ticksMS * COMPACT_COLLECTION_THRESHOLD_MS)) >> Timestamp::TimePrecisionCut);
				EventBase<Mode::INSTRUMENTATION_EVENTS>::TimeThreshold = threshold;
				EventBase<Mode::INSTRUMENTATION_CATEGORIES>::TimeThreshold = threshold;
			}

			CommonDescriptions::Init();

			// Clean all the screenshots
			// We can't use force clean here as we could still have pending screenshot requests in fly
			screenshotStorage.Reset();

			{
				SYS_CS_SYNC(m_csCallbacksToken);
				for (int i = 0; i < captureControlCallbacks.GetCount(); ++i)
					captureControlCallbacks[i](ROCKY_START, mode);
			}
		}

		// Activate Threads
		{
			SYS_CS_SYNC(m_csThreadsToken);
			for (int i = 0; i < threads.GetCount(); ++i)
				if (threadFilter.IsOK(threads[i]->description))
					threads[i]->Activate(mode);
		}

#if USE_PROFILER_BANDWIDTH_TRACKING
        if (mode != Mode::OFF)
        {
            CleanupNetworkBandwidth(false);
        }
#endif // USE_PROFILER_BANDWIDTH_TRACKING

		if (mode != Mode::OFF)
		{
			if (mode & Mode::IO)
			{
			#if USE_PROFILER_IO
				rage::fiDevice::SetDeviceProfiler(&deviceProfiler);// , rage::fiDeviceProfilerMgr::PROFILER_PERF);
			#endif
			}

			const char* rootPwd = nullptr;
			if (PARAM_rockyPwd.Get(rootPwd))
			{
				SetEncodedRootPassword(RockyPlatform::Linux, rootPwd);
			}
			
			EventTracer::Status status = EventTracer::OK;

#if USE_EVENT_TRACER
			int tracerMode = EventTracer::MODE_NONE;

			if (mode & Mode::OTHER_PROCESSES)
				tracerMode |= EventTracer::MODE_PROCESS;

			if (mode & Mode::SWITCH_CONTEXT)
				tracerMode |= EventTracer::MODE_SWITCH_CONTEXT;

			if (mode & Mode::AUTOSAMPLING)
				tracerMode |= EventTracer::MODE_AUTOSAMPLING;

			if (mode & Mode::PAGE_FAULTS)
				tracerMode |= EventTracer::MODE_PAGE_FAULTS;

			if (mode & Mode::SYS_CALLS)
				tracerMode |= EventTracer::MODE_SYS_CALLS;

			if (mode & Mode::SWITCH_CONTEXT_CALLSTACKS)
				tracerMode |= EventTracer::MODE_SWITCH_CONTEXT_CALLSTACKS;

			if (mode & Mode::HW_COUNTERS)
				tracerMode |= EventTracer::MODE_HW_EVENTS;

			if (mode & Mode::LIVE)
				tracerMode |= EventTracer::MODE_LIVE;

			{
				SYS_CS_SYNC(m_csThreadsToken);

				// Reset all the thread descriptions
				threadDescriptions.Reset();
				processDescriptions.Reset();

				EventTracer::Get().Clear(false);
				status = EventTracer::Get().Start((EventTracer::Mode)tracerMode);
			}
		#endif

			if (mode & Mode::LIVE)
			{
				Core::Get().StartLive(mode);
			}

			int numFrames = 0;
			if (PARAM_rockyFrameLimit.Get(numFrames))
				SetMaxCaptureFrameLimit(numFrames);
				
			SendHandshakeResponse(status);
		}
		else
		{
			if (currentMode & Mode::IO)
			{
			#if USE_PROFILER_IO
				static rage::fiDeviceProfiler s_dummy;
				rage::fiDevice::SetDeviceProfiler(&s_dummy);// , rage::fiDeviceProfilerMgr::PROFILER_PERF);
			#endif //USE_PROFILER_IO
			}

			#if USE_EVENT_TRACER
				EventTracer::Get().Stop();
			#endif

			if (currentMode & Mode::SAMPLING)
				sampler.StopSampling();

			if (PARAM_rockyON.Get())
				PARAM_rockyON.Set(NULL);

			if (PARAM_rockyTelemetryStartup.Get())
				PARAM_rockyTelemetryStartup.Set(NULL);

			if (currentMode & Mode::END_SCREENSHOT)
			{
				AttachScreenshot(storage);
			}

			
			AddInternalRunInfo();

			{
				SYS_CS_SYNC(m_csCallbacksToken);
				for (int i = 0; i < captureControlCallbacks.GetCount(); ++i)
					captureControlCallbacks[i](ROCKY_STOP, currentMode);
			}
		}

		rage::u32 previous = currentMode;
		currentMode = mode;
		pendingMode = mode;

		if (mode != Mode::OFF)
			lastUsedMode = mode;

		return previous;
	}

	return currentMode;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::DumpCapturingProgress()
{
	ROCKY_MUTE;

	rage::atStringBuilder builder;

	if (currentMode != Mode::OFF)
	{
		char buf[256];
		formatf(buf, "Capturing Frame %d\n", (rage::u32)frames[FRAME_CPU].GetCount());
		builder.Append(buf);

		RockyMemoryStats stats = GetRockyMemoryStats();

		Timestamp timestamp;
		timestamp.Start();

		float timerReset = Timestamp::ToMs(((u32)-1) - timestamp.start) / 1000.0f;
		float maxReset = Timestamp::ToMs((u32)-1) / 1000.0f;

		formatf(buf, "Memory %.1f Mb (%d%%)\nTimer Reset in %.1f seconds (%.1f)\n", (stats.used >> 10) / 1024.0f, (int)((100.0f * stats.used) / stats.total), timerReset, maxReset);
		builder.Append(buf);
	}

	if (currentMode == Mode::SAMPLING)
	{
		char buf[64];
		formatf(buf, "Sample Count %d\n", (rage::u32)sampler.GetCollectedCount());
		builder.Append(buf);
	}

	DumpProgress(builder.ToString());
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::IsTimeToReportProgress() const
{
	rage::u32 delayMS = (currentMode & Mode::LIVE_GUI) != 0 ? 500 : 5000;
	return rage::sysTimer::GetSystemMsTime() > (progressReportedLastTimestampMS + delayMS);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::RequestDump(rage::u32 mode, bool singleThreaded)
{
	rockyAssert(currentMode == Mode::OFF);
	dumpRequest.dumpMode = mode;
	dumpRequest.isSinglethreaded = singleThreaded;
	Dump();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::Dump()
{
	u32 mode = dumpRequest.dumpMode;
	bool singleThreaded = dumpRequest.isSinglethreaded;

	if (mode != Mode::OFF && !screenshotStorage.IsReady())
		return false;

	bool hasSomeData = GetNumCollectedFrames(FRAME_CPU) || sampler.GetCollectedCount();
	
	if (hasSomeData)
	{
		if (!outputFileName.IsNull())
			server.SetLocalMode(outputFileName.c_str());

		if (mode & Mode::INSTRUMENTATION || mode & Mode::NETWORK_BANDWIDTH_CAPTURE)
			DumpFrames(mode, singleThreaded);

		if (mode & Mode::SAMPLING)
			DumpSamplingData();

		server.Send(RockyResponse::NullFrame, OutputDataStream::GetEmptyStream());
	}

	screenshotStorage.Reset();

	server.SetLocalMode(NULL);

	threadDescriptions.Resize(0);
	processDescriptions.Resize(0);

	for (int i = 0; i < NELEM(frames); ++i)
		frames[i].Resize(0);

	EventTracer::Get().Clear(false);
	outputFileName.Clear();

	CleanupThreads();

    CleanupNetworkBandwidth(false);

	dumpRequest.Reset();

	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rage::u32 Core::DumpDescriptionBoard(EventTime interval, rage::u32 mode)
{
	SYS_CS_SYNC(m_csThreadsToken);

	rage::u32 mainThreadIndex = 0;
	for (int i = 0; i < threads.GetCount(); ++i)
		if (threads[i]->description.threadId == mainThreadID)
			mainThreadIndex = (rage::u32)i;

	// FrameDescription Board
	OutputDataStream boardStream;

    currentBoardNumber = GetNextBoardNumber();
	boardStream << currentBoardNumber;
	boardStream << (rage::s64)(1.0f / rage::sysTimerConsts::TicksToSeconds);
	boardStream << (rage::u64)interval.start;
	boardStream << Timestamp::TimePrecisionCut;
	boardStream << interval;
	boardStream << threads; 
	boardStream << mainThreadIndex;
	boardStream << DescriptionBoard<EventDescription>::Get();
	boardStream << DescriptionBoard<TagDescription>::Get();
	boardStream << RunDescriptionBoard::Get();
	boardStream << FilterDescriptionBoard();
	boardStream << mode;
	boardStream << (rage::u32)GetCurrentProcessId();
	boardStream << (rage::u32)0;// processDescriptions;
	boardStream << (rage::u32)0;//threadDescriptions;

	server.Send(RockyResponse::FrameDescriptionBoard, boardStream);

	return currentBoardNumber;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const char* Core::DumpToFile(const char* fileName, bool force, bool singleThreaded)
{
	USE_ROCKY_MEMORY;

	rockyFatalAssertf(singleThreaded || sysThreadType::IsUpdateThread(), "Core::DumpToFile should be called on the MainThread!");

	char fullPath[256] = { 0 };

	const char* folder = nullptr;
	if (PARAM_rockyOutputFolder.Get(folder))
	{
		strcat_s(fullPath, sizeof(fullPath), folder);
	}

	if ((fileName == nullptr || strlen(fileName) == 0) && !PARAM_rockyOutputFile.Get(fileName))
	{
		fileName = GenerateCaptureName();
	}

	strcat_s(fullPath, sizeof(fullPath), fileName);

	outputFileName = fullPath;

	if (GetMode() == Mode::OFF || force)
	{
		u32 prevMode = Activate(Mode::OFF);
		RequestDump(prevMode ? prevMode : Mode::FULL, singleThreaded);
	}
	else
	{
		// Wait for the current frame to complete
		pendingMode = Mode::OFF;
	}

	return fileName;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::SendHandshakeResponse(EventTracer::Status status)
{
	OutputDataStream stream;
	stream << (rage::u32)status;
	server.Send(RockyResponse::Handshake, stream);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::RegisterThread(const ThreadDescription& description)
{
	USE_ROCKY_MEMORY;
	SYS_CS_SYNC(m_csThreadsToken);

	ThreadEntry* entry = nullptr; 

	// Note: We have only few threads in the list
	//       It's cheaper to iterate than to maintain a dictionary
	if (description.threadId != sysIpcCurrentThreadIdInvalid)
		for (ThreadEntry* pThread : threads)
			if (pThread->description.threadId == description.threadId)
				entry = pThread;

	if (entry == nullptr)
	{
		entry = rage_aligned_new(ROCKY_CACHE_LINE_SIZE) ThreadEntry(description, description.IsReal() ? &TLS::storage : nullptr);
		threads.push_back(entry);
	}

	if (currentMode != Mode::OFF)
		if (threadFilter.IsOK(entry->description))
			entry->Activate(currentMode);

	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static volatile bool g_IsCoreShutdown = false;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::UnregisterThread(rage::sysIpcCurrentThreadId threadId)
{
	if (g_IsCoreShutdown)
		return true;

	USE_DEBUG_MEMORY();
	SYS_CS_SYNC(m_csThreadsToken);

	bool bValidThreadId = false;

	// Reverse iterate through the threads, as its more likely a temporary thread lives at the end of the list.
	for (int i = threads.GetCount() - 1; i >= 0; i--)
	{
		ThreadEntry* t = threads[i];

		// Use the thread ID as a lookup
		if (t->description.threadId == threadId)
		{
			// If we're currently OFF, the thread can be immediately removed from the list
			if (GetMode() == Mode::OFF)
			{
				threads.Delete(i);
				delete t;
			}
			else
			{
				// Otherwise, mark it as unregistered for cleanup after the current operation.
				t->description.registered = false;
			}

			// Flag the thread Id as valid and exit.
			bValidThreadId = true;
			break;
		}
	}

	// Assert and return false if we were unable to match the thread id.
	rockyAssertf(bValidThreadId, "Unregistering unknown thread id");
	return bValidThreadId;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::RegisterThreadDescription(const ThreadDescription& description)
{
	USE_ROCKY_MEMORY;

	for (const ThreadDescription& desc : threadDescriptions)
		if (desc.systemThreadId == description.systemThreadId && description.systemThreadId != (rage::u64) - 1)
			return false;

	threadDescriptions.PushAndGrow(description, 256);
	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::RegisterProcessDescription(const ProcessDescription& description)
{
	USE_ROCKY_MEMORY;
	processDescriptions.PushAndGrow(description, 256);
	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::CleanupThreads()
{
	USE_ROCKY_MEMORY;
	SYS_CS_SYNC(m_csThreadsToken);

	// Reverse iterate through the threads list, as we will be removing unregistered threads.
	for (int i = threads.GetCount() - 1; i >= 0; i--)
	{
		ThreadEntry* t = threads[i];
		if (!t->description.registered)
		{
			threads.Delete(i);
			delete t;
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::RegisterLiveDataCallback(LiveDataCallback cb)
{
	liveDataCallback = cb;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::RegisterScreenshotCallback(ScreenshotCallback cb)
{
	screenshotCallback = cb;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::NotifyScreenshotReady(const char* name, bool /*isOk*/)
{
	screenshotStorage.MakeReady(name);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Core::~Core()
{
	Shutdown();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::Shutdown()
{
	USE_ROCKY_MEMORY;

	g_IsCoreShutdown = true;

	server.Shutdown();

	Activate(Mode::OFF);

	// Cleanup Threads
	{
		SYS_CS_SYNC(m_csThreadsToken);
		for (int i = 0; i < threads.GetCount(); ++i)
			delete threads[i];

		threads.Reset();
	}

    CleanupNetworkBandwidth(false);

	DescriptionBoard<EventDescription>::Get().Shutdown();
	DescriptionBoard<TagDescription>::Get().Shutdown();	

	EventTracer::Get().Stop();

	RockyShutdownMemory();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::StartCapture(rage::u32 mode /*= Mode::INSTRUMENTATION*/)
{
	pendingMode = mode;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rage::u32 Core::StopCapture()
{
	return Activate(Mode::OFF);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const rage::atVector<ThreadEntry*>& Core::GetThreads() const
{
	return threads;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ThreadEntry* Core::GetGPUThreadEntry() const
{
	return gpuThreadEntry;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ThreadEntry* Core::GetIOThreadEntry() const
{
	return ioThreadEntry;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__THREAD EventStorage* TLS::storage = nullptr;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Core Core::notThreadSafeInstance;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream & operator<<(OutputDataStream &stream, FilterDescriptionBoard)
{
	stream << Filter::Count;
	for (int i = 0; i < Filter::Count; ++i)
		stream << Filter::ToString((Filter::Type)i);
	return stream;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ScopeHeader::ScopeHeader() : threadNumber(0), boardNumber(0)
{
	event.start = 0;
	event.finish = 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const ThreadDescription& description)
{
	return stream << (rage::u64)(description.systemThreadId) << description.name.c_str() << description.maxDepth << description.priority << description.threadMask << description.processId << description.baseAddress;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const ProcessDescription& description)
{
	return stream << description.uniqueKey << description.processId << description.processName.c_str() << description.commandLine.c_str();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const ThreadEntry* entry)
{
	return stream << entry->description;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const CallStackEntry& entry)
{
	stream << entry.time;
	stream << entry.mode;

	rage::u8 count = 0;
	for (count = 0; count < MAX_CALLSTACK_DEPTH; ++count)
		if (entry.callstack[count]==0)
			break;

	stream << count;
	for (int i = 0; i < count; ++i)
		stream << (rage::u64)entry.callstack[i];

	return stream;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const ResourceData& data)
{
	return stream << data.time << (data.source != nullptr ? data.source->index : (u32)-1) << data.description->index << (u8)data.mode;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& stream, const Drawcall& data)
{
	return stream << (data.renderPhase ? data.renderPhase->index : (rage::u32) - 1) 
				  << (data.instanceName ? data.instanceName->index : (rage::u32)-1)
				  << (data.shaderName ? data.shaderName->index : (rage::u32)-1)
				  << data.primitiveCount << data.instanceCount 
				  << data.position << data.entityID << data.drawcallType;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void NextFrame()
{
	return Core::NextFrame();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void NextGpuFrame(rage::u32 frameNumber, rage::u64 timestamp)
{
	return Core::Get().NextGpuFrame(frameNumber, timestamp);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void NextGpuFlip(const GPUFrame& gpuFrame)
{
	return Core::Get().NextGpuFlip(gpuFrame);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
EventDescription* GetMainFrameEventDescription(FrameType type)
{
	static EventDescription* cpuFrame = EventDescription::Create("CPU Frame", __FILE__, __LINE__, CategoryColor::None, EventDescription::IS_LIVE, 33.0f);
	static EventDescription* gpuFrame = EventDescription::Create("GPU Frame", __FILE__, __LINE__, CategoryColor::None);
	static EventDescription* gpuFlip = EventDescription::Create("GPU Flip", __FILE__, __LINE__, CategoryColor::None);

	static EventDescription* frames[FRAME_COUNT] = { cpuFrame, gpuFrame, gpuFlip };

	return frames[type];
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AddInternalTags()
{
	if (TLS::storage == nullptr)
		return;

	if (TLS::storage->mode & Mode::INSTRUMENTATION)
	{
		static Profiler::TagDescription* rockyMemoryTag = Profiler::TagDescription::Create("Memory\\Rocky Memory (Kb)");
		Profiler::AttachTag(rockyMemoryTag , GetProfilerAllocator().GetMemoryUsed() >> 10);

		static Profiler::TagDescription* rockyTotalMemoryTag = Profiler::TagDescription::Create("Memory\\Rocky Memory [Total] (Kb)");
		Profiler::AttachTag(rockyTotalMemoryTag, GetProfilerAllocator().GetHeapSize() >> 10);

		static Profiler::TagDescription* rockyAllocationsCountTag = Profiler::TagDescription::Create("Memory\\Rocky Allocations Count");
		Profiler::AttachTag(rockyAllocationsCountTag, pAllocator->GetAllocationsCount());

		static Profiler::TagDescription* rockyAllocationsOverheadTag = Profiler::TagDescription::Create("Memory\\Rocky Allocations Overhead (ms)");
		Profiler::AttachTag(rockyAllocationsOverheadTag, pAllocator->GetAllocationsOverheadMs());


#if RSG_PC
		#define ATTACH_SYS_MEM_TAG(NAME, VALUE) { static Profiler::TagDescription* tag = Profiler::TagDescription::Create(NAME); Profiler::AttachTag(tag, VALUE); }

		MEMORYSTATUSEX statex = { 0 };
		statex.dwLength = sizeof(statex);
		GlobalMemoryStatusEx(&statex);

		ATTACH_SYS_MEM_TAG("System Memory\\Memory Used (%)", (rage::u32)(statex.dwMemoryLoad));
		ATTACH_SYS_MEM_TAG("System Memory\\Physical Total (Kb)", (rage::u32)(statex.ullTotalPhys >> 10ull));
		ATTACH_SYS_MEM_TAG("System Memory\\Physical Free (Kb)", (rage::u32)(statex.ullAvailPhys >> 10ull));
		ATTACH_SYS_MEM_TAG("System Memory\\PageFile Total (Kb)", (rage::u32)(statex.ullTotalPageFile >> 10ull));
		ATTACH_SYS_MEM_TAG("System Memory\\PageFile Free (Kb)", (rage::u32)(statex.ullAvailPageFile >> 10ull));
		ATTACH_SYS_MEM_TAG("System Memory\\Virtual Total (Kb)", (rage::u32)(statex.ullTotalVirtual >> 10ull));
		ATTACH_SYS_MEM_TAG("System Memory\\Virtual Free (Kb)", (rage::u32)(statex.ullAvailVirtual >> 10ull));
		ATTACH_SYS_MEM_TAG("System Memory\\Extended Free (Kb)", (rage::u32)(statex.ullAvailExtendedVirtual >> 10ull));

		PROCESS_MEMORY_COUNTERS counters = { 0 };
		GetProcessMemoryInfo(GetCurrentProcess(), &counters, sizeof(counters));

		ATTACH_SYS_MEM_TAG("Process Memory\\Page Fault Count", (rage::u32)(counters.PageFaultCount));
		ATTACH_SYS_MEM_TAG("Process Memory\\Peak Working Set Size (Kb)", (rage::u32)(counters.PeakWorkingSetSize >> 10ull));
		ATTACH_SYS_MEM_TAG("Process Memory\\Working Set Size (Kb)", (rage::u32)(counters.WorkingSetSize >> 10ull));
		ATTACH_SYS_MEM_TAG("Process Memory\\Quota Peak Paged Pool Usage (Kb)", (rage::u32)(counters.QuotaPeakPagedPoolUsage >> 10ull));
		ATTACH_SYS_MEM_TAG("Process Memory\\Quota Paged Pool Usage (Kb)", (rage::u32)(counters.QuotaPagedPoolUsage >> 10ull));
		ATTACH_SYS_MEM_TAG("Process Memory\\Quota Peak Non-Paged Pool Usage (Kb)", (rage::u32)(counters.QuotaPeakNonPagedPoolUsage >> 10ull));
		ATTACH_SYS_MEM_TAG("Process Memory\\Quota Non-Paged Pool Usage (Kb)", (rage::u32)(counters.QuotaNonPagedPoolUsage >> 10ull));
		ATTACH_SYS_MEM_TAG("Process Memory\\Peak Pagefile Usage (Kb)", (rage::u32)(counters.PeakPagefileUsage >> 10ull));
		ATTACH_SYS_MEM_TAG("Process Memory\\Pagefile Usage (Kb)", (rage::u32)(counters.PagefileUsage >> 10ull));

		#undef ATTACH_SYS_MEM_TAG
#endif
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AttachMsg(const TagDescription* desc, const char* msg)
{
	AttachTag(desc, msg);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool ThreadDescription::Register(const char* threadName /*= "MainThread"*/, rage::u32 depth)
{
	// Initialize thread ID
	rage::sysIpcSetCurrentThreadId();

	ThreadDescription description(	threadName, 
									depth,
									rage::sysIpcGetCurrentThreadPriority(), 
									rage::sysIpcGetCurrentThreadId(),
									sysThreadType::GetCurrentThreadType()	);

	description.systemThreadId = (u64)rage::sysIpcGetCurrentThreadId();

	return Core::Get().RegisterThread(description);
}

bool ThreadDescription::Unregister(rage::sysIpcCurrentThreadId threadId)
{
	return Core::Get().UnregisterThread(threadId);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// const int LIVE_MODE_RESERVED_EVENT_COUNT = 2048;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void UpdateThreadPriority(ThreadDescription& desc)
{
	(void)desc;
	return;
// TODO: Fix priority

//	sysIpcThreadId threadId = (sysIpcThreadId)desc.threadId;
//	(void)threadId;
//
//#if (RSG_PC || RSG_XBOX)
//	HANDLE handle = ::OpenThread(THREAD_QUERY_INFORMATION, false, (DWORD)((u64)threadId));
//	threadId = (sysIpcThreadId)handle;
//#endif
//
//	sysIpcPriority priority = sysIpcGetThreadPriority(threadId);
//
//#if (RSG_PC || RSG_XBOX)
//	::CloseHandle(handle);
//#endif
//
//	desc.priority = priority;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ThreadEntry::Activate(rage::u32 mode)
{
	if (!description.registered)
		return;

	storage.mode = mode;

	if (mode == Mode::OFF)
	{
		if (threadTLS != nullptr)
			*threadTLS = nullptr;
	}
	else
	{
		if ((mode & Mode::LIVE) != 0)
		{
			storage.Clear(false);
			
			// Reserve memory for live events in advance
			for (int i = 0; i < LIVE_EVENT_BUFFER_COUNT; ++i)
			{
				storage.liveBuffer[i].events.Reserve(LiveEventBuffer::GetChunkSize());
				storage.liveBuffer[i].tags.Reserve(LiveTagBuffer::GetChunkSize());
			}
		}
		else
		{
			storage.Clear(false);
		}
        
		// Reserving minimum memory to be able to catch every type of tag
		storage.ReserveMinimumMemory();

		// Update thread priority
		if (description.IsReal())
			UpdateThreadPriority(description);
	
		if (threadTLS != nullptr)
			*threadTLS = &storage;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ThreadEntry::Clear()
{
	storage.Clear(false);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ThreadEntry::SortEvents()
{
	USE_ROCKY_MEMORY;

	int count = (int)storage.eventBuffer.Size();
	if (count == 0)
		return;

	rage::atArray<EventData,0,u32> buffer(count, count);
	storage.eventBuffer.ToArray(&buffer[0]);
	buffer.QSort(0, -1, [](const EventData* a, const EventData* b) -> int
	{
		if (a->start != b->start)
			return (a->start - b->start);

		if (a->finish != b->finish)
			return (b->finish - a->finish);

		if (a->description->budget > b->description->budget)
			return -1;
		else if (a->description->budget < b->description->budget)
			return 1;

		return 0;
	});

	storage.eventBuffer.Clear(true);
	for (int i = 0; i < count; ++i)
		storage.NextEvent() = buffer[i];
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ScopeData::Send()
{
	if (!events.IsEmpty())
	{
		Server& server = Core::Get().GetServer();

		size_t size = sizeof(header);
		size += sizeof(rage::u32) + events.Size() * EventData::SerializationSize; // index + size

		// Locking the server to prevent packet interleaving
		ServerLocker lock(server);

		if (!server.SendHeader(RockyResponse::EventFrame, size))
			return;

		server.SendData(&header, sizeof(header));
		
		OutputDataStream frameStream;

		int eventCount = (int)events.Size();
		server.SendData(&eventCount, sizeof(eventCount));

		events.ForEachChunk([&](const EventData* chunk, uint count) -> bool
		{
			for (uint i = 0; i < count; ++i)
				frameStream << chunk[i];

			server.SendData(frameStream);
			frameStream.Reset();
			return true;
		});
	}

	Clear();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ScopeData::Clear(bool preserveMemory)
{
	events.Clear(preserveMemory);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::AttachScreenshot(EventStorage* storage)
{
	if (storage != nullptr && screenshotCallback != nullptr)
	{
		if (const ScreenshotStorage::Slot* slot = screenshotStorage.QueryScreenshot())
		{
			if (screenshotCallback(slot->name))
			{
				static Profiler::TagDescription* desc = Profiler::TagDescription::Create("Frame\\Screenshot");
				TagImage& tag = storage->tagsImage.Add();
				tag.time.Start();
				tag.description = desc;
				tag.data = Image(slot->name);
			}
			else
			{
				// Failed to take a screenshot, let's mark for removal
				screenshotStorage.MakeReady(slot->name);
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct EventDataNode
{
	const EventData* data;
	rage::u32 childrenSum;
	rage::u32 childrenWait;
	rage::u32 GetSelfDuration() const { return data->finish - data->start - childrenSum; }
	EventDataNode(const EventData* d) : data(d), childrenSum(0), childrenWait(0) {}
	EventDataNode() : data(nullptr), childrenSum(0), childrenWait(0) {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct CounterAccumulator
{
	struct Item
	{
		rage::u32 time;
		rage::u32 count;

		Item() : time(0), count(0) {}
	};

	atVector<Item> frames;

	void Add(rage::u32 frame, rage::u32 t)
	{
		while ((rage::u32)frames.GetCount() <= frame)
			frames.PushAndGrow(Item(), 128);

		frames[frame].time += t;
		frames[frame].count += 1;
	}

	bool Get(int frameCount, float& min, float& avg, float& max, float& sum, float& avgCount, CounterAccumulator* categoryCounter=nullptr)
	{
		min = FLT_MAX;
		max = -FLT_MAX;
		sum = 0.0f;
		avg = 0.0f;
		avgCount = 0.0f;

		for (int i = 0; i < Min(frames.GetCount(), frameCount); ++i)
		{
			const Item& frame = frames[i];
			if (frame.count)
			{
				if (categoryCounter)
					categoryCounter->Add(i, frame.time);

				float time = Timestamp::ToMs(frame.time);
				min = Min(min, time);
				max = Max(max, time);
				sum += time;
				avgCount += (float)frame.count;
			}
		}

		if (frameCount)
		{
			avg = sum / frameCount;
			avgCount = avgCount / frameCount;
			return true;
		}

		return frameCount > 0;
	}

	CounterAccumulator() {}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef atMap<const EventDescription*, CounterAccumulator> CountersMap;
typedef atMap<Filter::Type, CounterAccumulator> CategoryCountersMap;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct SwitchContextMap
{
	typedef atVector<EventTime, 0, rage::u32> EventTimeBuffer;
	atMap<u32, EventTimeBuffer> threadWait;

	// PURPOSE: Loads Synchronization intervals into the map
	void Load(SynchronizationBuffer& buffer);

	// PURPOSE: Calculates Wait time on for the specified interval
	u32 GetWaitTime(u32 threadID, EventTime interval) const;

private:
	// PURPOSE: Calculates partition index to find the closest index for selected timestamp in sorted array of intervals
	static int BinaryPartition(const EventTimeBuffer& elements, const u32 timestamp);
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SwitchContextMap::Load(SynchronizationBuffer& buffer)
{
	USE_ROCKY_MEMORY;

	buffer.ForEach([&](const SwitchContextData& data) -> bool
	{
		EventTimeBuffer& oldThread = threadWait[data.oldThreadID];
		EventTimeBuffer& newThread = threadWait[data.newThreadID];
		oldThread.PushAndGrow(EventTime(data.timestamp, 0xFFFFFFFF), Max(16, oldThread.GetCount()));
		if (!newThread.empty())
			newThread.back().finish = data.timestamp;
		return true;
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u32 SwitchContextMap::GetWaitTime(u32 threadID, EventTime interval) const
{
	u32 totalWait = 0;

	if (const EventTimeBuffer* buffer = threadWait.Access(threadID))
	{
		int start = BinaryPartition(*buffer, interval.start);
		int finish = BinaryPartition(*buffer, interval.finish);

		for (int i = start; i <= finish && 0 <= i && i < buffer->GetCount(); ++i)
		{
			u32 wait = interval.CalcIntersect(buffer->at(i));
			totalWait += wait;
		}
	}

	return totalWait;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int SwitchContextMap::BinaryPartition(const EventTimeBuffer& elements, const u32 timestamp)
{
	int low = 0;
	int high = elements.GetCount() - 1;

	while (low < high) {
		int mid = (low + high) >> 1;

		if (timestamp > elements[mid].finish)
			low = mid + 1;
		else
			high = mid;
	}
	return high;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::GenerateCounterGroup(EventCounterGroup& group, const rage::atArray<ThreadCollectionGroup>& threads, const SwitchContextMap& switchContexts, FrameType frameType) const
{
	SYS_CS_SYNC(m_csThreadsToken);

	CountersMap countersMap;
	CategoryCountersMap categoryCountersMap;

	for (auto& cat : group.categoryCounters)
		categoryCountersMap.Insert(cat.category);

	int frameCount = GetNumCollectedFrames(frameType);

	if (frameCount == 0)
		return false;

	EventTime window;
	window.start = GetFirstValidFrameTime(frameType);
	window.finish = frames[frameType][frameCount - 1].event.finish;

	const atVector<Frame, 0, rage::u32>& frameList = frames[frameType];

	rage::u32 startFrame = 0;

	while (startFrame < (rage::u32)frameList.GetCount() && frameList[startFrame].event.finish < window.start)
		startFrame++;

	for (const ThreadCollectionGroup& thread : threads)
	{
		const ThreadEntry* entry = thread.threadEntry;
		const bool subtractWaiting = thread.subtractWaiting;
		const u32 threadID = (u32)(entry->description.systemThreadId);

		rage::atArray<EventDataNode> stack(0, 16);


		rage::u32 currentFrame = 0;

		entry->storage.eventBuffer.ForEach([&](const EventData& data) -> bool
		{
			if (data.description != nullptr)
			{
				rage::u32 color = data.description->category.color;
				if ((color != Profiler::Color::Null && color != Profiler::Color::White) &&						// Is color not null and not synchronization
					(	   data.start >= window.start
						&& data.finish <= window.finish
						&& data.start < data.finish))	// Is inside captured window
				{
					while (!stack.empty() && stack.back().data->finish < data.finish)
					{
						const EventDataNode& parent = stack.Pop();

						while ((startFrame + currentFrame) < (rage::u32)frameList.GetCount() && frameList[startFrame + currentFrame].event.finish < parent.data->finish)
							++currentFrame;

						u32 totalTime = parent.data->finish - parent.data->start;
						u32 waitTime = subtractWaiting ? switchContexts.GetWaitTime(threadID, *parent.data) : 0;
						
						u32 duration = (totalTime - waitTime) - (parent.childrenSum - parent.childrenWait);

						countersMap[parent.data->description].Add(currentFrame, duration);
					}

					if (!stack.empty())
					{
						const u32 waitTime = subtractWaiting ? switchContexts.GetWaitTime(threadID, data) : 0;
						const u32 durationTime = data.finish - data.start;
						
						stack.back().childrenSum += durationTime;
						stack.back().childrenWait += waitTime;
					}

					EventDataNode node(&data);
					stack.PushAndGrow(node);
				}
			}
			return true;
		});

		while (!stack.empty())
		{
			const EventDataNode& parent = stack.Pop();

			while ((startFrame + currentFrame) < (rage::u32)frameList.GetCount() && frameList[startFrame + currentFrame].event.finish < parent.data->finish)
				++currentFrame;

			countersMap[parent.data->description].Add(currentFrame, parent.GetSelfDuration());
		}
	}

	u32 numFrames = frameCount - startFrame;

	for (CountersMap::Iterator it = countersMap.CreateIterator(); !it.AtEnd(); it.Next())
	{
		const EventDescription* pDesc = it.GetKey();
		CounterAccumulator* pCatCounter = categoryCountersMap.Access(pDesc->category.filter);

		EventCounter counter(pDesc);
		if (it.GetData().Get(numFrames, counter.minTime, counter.avgTime, counter.maxTime, counter.sumTime, counter.count, pCatCounter))
		{
			group.counters.PushAndGrow(counter);
		}	
	}

	for (auto& catCounter : group.categoryCounters)
	{
		CounterAccumulator* pCatCounterAccum = categoryCountersMap.Access(catCounter.category);

		pCatCounterAccum->Get(numFrames, catCounter.minTime, catCounter.avgTime, catCounter.maxTime, catCounter.sumTime, catCounter.count);
	}

	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::GenerateBudgets(rage::atArray<EventCounterGroup>& budgets, const BudgetSettings& settings)
{
	SYS_CS_SYNC(m_csThreadsToken);

	// Merging all the GPU events together
	MergeGPUEvents();

	SwitchContextMap switchContextMap;
	switchContextMap.Load(EventTracer::Get().switchContextEvents);

	const char* budgetsPath = RS_TOOLSROOT"/bin/RockyProfiler/PerformanceBudgetsTotal";

#if RSG_LINUX
	if (g_SysService.IsCloud())
		budgetsPath = "/mnt/developer/PerformanceBudgets";
#endif

	rage::parTree* tree = PARSER.LoadTree(budgetsPath, "xml");
	if (!rockyVerifyf(tree, "Can't find PerformanceBudgets.xml!"))
		return false;

	for (rage::parTreeNode::ChildNodeIterator it = tree->GetRoot()->BeginChildren(); it != tree->GetRoot()->EndChildren(); ++it)
	{
		parTreeNode* currentNode = *it;

		FrameType frameType = FRAME_CPU;
		if (parTreeNode* isGpuNode = currentNode->FindChildWithName("IsGPU"))
		{
			if (strstr(isGpuNode->GetData(), "true") != nullptr)
				frameType = FRAME_GPU;
		}

		if (parTreeNode* nameNode = currentNode->FindChildWithName("Name"))
		{
			if (parTreeNode* threadsNode = currentNode->FindChildWithName("Threads"))
			{
				atArray<ThreadCollectionGroup> entries;

				for (parTreeNode::ChildNodeIterator threadIt = threadsNode->BeginChildren(); threadIt != threadsNode->EndChildren(); ++threadIt)
				{
					parTreeNode* threadNameNode = (*threadIt)->FindChildWithName("Name");
					const char* threadNameMask = threadNameNode->GetData();

					parTreeNode* subtractWaitingNode = (*threadIt)->FindChildWithName("SubstractWaiting"); 
					bool subtractWaiting = subtractWaitingNode != nullptr && strstr(subtractWaitingNode->GetData(), "true") != nullptr;

					std::regex rgx(threadNameMask);
					std::smatch match;

					for (ThreadEntry* entry : threads)
					{
						std::string threadName = entry->description.name.c_str();
						if (std::regex_search(threadName, match, rgx))
							entries.PushAndGrow(ThreadCollectionGroup(entry, subtractWaiting));
					}
				}

				EventCounterGroup group(nameNode->GetData());

				// Populate budget categories.
				if (parTreeNode* threadsNode = currentNode->FindChildWithName("Budgets"))
				{
					for (parTreeNode::ChildNodeIterator threadIt = threadsNode->BeginChildren(); threadIt != threadsNode->EndChildren(); ++threadIt)
					{
						parTreeNode* categoryNameNode = (*threadIt)->FindChildWithName("Name");
						const char* categoryName = categoryNameNode->GetData();

						if (categoryName)
						{
							for (u32 i = 0; i < Filter::Type::Count; ++i)
							{
								if (strcmp(categoryName, Filter::ToString((Filter::Type)i)) == 0)
								{
									group.categoryCounters.PushAndGrow((Filter::Type)i);
									break;
								}
							}
						}
					}
				}

				if (GenerateCounterGroup(group, entries, switchContextMap, frameType))
				{
					if (settings.m_Sort && group.counters.GetCount() > 0)
					{
						if (settings.m_UseTotalTime)
							std::sort(&group.counters[0], &group.counters[0] + group.counters.GetCount(),
								[](const EventCounter& a, const EventCounter& b) { return a.sumTime > b.sumTime; }
							);
						else
							std::sort(&group.counters[0], &group.counters[0] + group.counters.GetCount(),
								[](const EventCounter& a, const EventCounter& b) { return a.avgTime > b.avgTime; }
							);
					}
					budgets.PushAndGrow(group);
				}
			}
		}
	}

	delete tree;

	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const char JSONDisallowedSymbols[] = { ',', '#', '<', '>', '*', '&', '?' };
bool IsAllowedJSONSymbol(char c)
{
	for (int i = 0; i < NELEM(JSONDisallowedSymbols); ++i)
		if (c == JSONDisallowedSymbols[i])
			return false;
	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::GenerateBudgets(rage::RsonWriter& rw, const BudgetSettings& settings)
{
	atArray<Profiler::CounterGroup<Profiler::EventDescription>> groups;
	BudgetSettings adjustedSettings = settings;
	adjustedSettings.m_Sort = true;

	Profiler::Core::Get().GenerateBudgets(groups, adjustedSettings);

	char name[RAGE_MAX_PATH] = { 0 };

	rw.Begin("rocky", nullptr);
	for (Profiler::CounterGroup<Profiler::EventDescription>& group : groups)
	{
		rw.BeginArray(group.name.c_str(), nullptr);
		{
			for (Profiler::EventCounter& counter : group.counters)
			{
				float time = settings.m_UseTotalTime ? counter.sumTime : counter.avgTime;

				// Drop out everything below 10 ms
				if (time > settings.m_Threshold)
				{
					strcpy_s(name, RAGE_MAX_PATH, counter.description->name);
					char* str = name;
					while (*str)
					{
						if (!IsAllowedJSONSymbol(*str))
							*str = '@';
						++str;
					}

					rw.Begin(nullptr, nullptr);
					rw.WriteString("name", name);
					rw.WriteString("category", Profiler::Filter::ToString(counter.description->category.filter));
					rw.WriteFloat("time", time);
					rw.End();
				}
			}
		}
		rw.End();
	}
	rw.End();
	return !groups.empty();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TTag>
TagCounter& operator+=(TagCounter& counter, const TTag& tag)
{
	float data = (float)tag.data;
	counter.count += 1.0f;
	counter.avgTime += data;
	counter.maxTime = Max(data, counter.maxTime);
	counter.minTime = Min(data, counter.minTime);
	return counter;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef rage::atMap<const TagDescription*, TagCounter> TagCountersMap;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class TTagStorage, class TTag>
void PopulateCountersMap(TagCountersMap& countersMap, const TTagStorage& storage, const EventTime window)
{
	storage.ForEach([&](const TTag& tag)
	{
		if (window.IsIntersect(tag.time))
			if (TagCounter* counter = countersMap.Access(tag.description))
				*counter += tag;

		return true;
	});
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::GenerateBudgets(rage::atArray<TagCounterGroup>& budgets, const BudgetSettings& /*settings*/)
{
	int frameCount = GetNumCollectedFrames(FRAME_CPU);
	if (frameCount == 0)
		return false;

	EventTime window;
	window.start = GetFirstValidFrameTime(FRAME_CPU);
	window.finish = frames[FRAME_CPU][frameCount - 1].event.finish;

	const rage::atVector<TagDescription*,0,rage::u32>& descriptions = TagDescriptionBoard::Get().GetEvents();

	TagCountersMap countersMap;

	for (int i = 0; i < descriptions.GetCount(); ++i)
	{
		const TagDescription* desc = descriptions[i];
		if (desc->IsGroup())
			countersMap.Insert(desc, TagCounter(desc));
	}

	for (const ThreadEntry* entry : threads)
	{
		PopulateCountersMap<TagFloatBuffer, TagFloat>(countersMap, entry->storage.tagsFloat, window);
		PopulateCountersMap<TagU32Buffer, TagU32>(countersMap, entry->storage.tagsU32, window);
		PopulateCountersMap<TagS32Buffer, TagS32>(countersMap, entry->storage.tagsS32, window);
		PopulateCountersMap<TagU64Buffer, TagU64>(countersMap, entry->storage.tagsU64, window);
	}

	char groupNameBuffer[256];
	typedef atMap<atString, TagCounterGroup*> GroupMap;
	GroupMap groups;
	for (TagCountersMap::Iterator it = countersMap.CreateIterator(); !it.AtEnd(); it.Next())
	{
		TagCounter& counter = it.GetData();
		if (!counter.IsValid())
			continue;

		const TagDescription* desc = it.GetKey();
		const char* del = strchr(desc->name, '\\');
		if (del == nullptr)
			continue;

		strncpy_s(groupNameBuffer, 256, desc->name, (int)(del - desc->name));
		atString groupName(groupNameBuffer);

		TagCounterGroup* group = nullptr;
		if (TagCounterGroup** groupPtr = groups.Access(groupName))
		{
			group = *groupPtr;
		}
		else
		{
			group = rage_new TagCounterGroup(groupName.c_str());
			groups.Insert(groupName, group);
		}
		
		// Normalize total time => avg time
		counter.avgTime /= counter.count;

		group->counters.PushAndGrow(counter, 128);
	}

	for (GroupMap::Iterator it = groups.CreateIterator(); !it.AtEnd(); it.Next())
	{
		budgets.PushAndGrow(*(it.GetData()));
		delete it.GetData();
	}

	return groups.GetNumUsed() > 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Core::GetTelemetryKeywords(rage::atArray<rage::atString>& keywords)
{
	const char *keywordNames[32] = { 0 };
	char keywordsBuffer[1024] = { 0 };

	int keywordCount = PARAM_rockyTelemetryKeywords.GetArray(keywordNames, NELEM(keywordNames), keywordsBuffer, NELEM(keywordsBuffer));
	if (keywordCount)
	{
		// keywords.ReserveExtend((u32)keywordCount);
		for (int i = 0; i < keywordCount; ++i)
			keywords.PushAndGrow(rage::atString(keywordNames[i]));
		return true;
	}
	return false;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::RegisterCaptureControlCallback(CaptureControlCallback cb)
{
	SYS_CS_SYNC(m_csCallbacksToken);
	rockyFatalAssertf(captureControlCallbacks.Find(cb) == -1, "Can't register the same callback twice!");
	captureControlCallbacks.PushAndGrow(cb);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::UnRegisterCaptureControlCallback(CaptureControlCallback cb)
{
	SYS_CS_SYNC(m_csCallbacksToken);
	int index = captureControlCallbacks.Find(cb);
	rockyFatalAssertf(index != -1, "Can't unregister the same callback twice!");
	captureControlCallbacks.DeleteFast(index);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::RegisterCaptureCategoryCallback(CaptureCategoryCallback callback, rage::sysIpcCurrentThreadId threadId /*= sysIpcCurrentThreadIdInvalid*/)
{
	SYS_CS_SYNC(m_csThreadsToken);
	for (ThreadEntry* thread : threads)
	{
		if (thread->description.threadId == threadId || threadId == sysIpcCurrentThreadIdInvalid)
		{
			thread->storage.captureCategoryCallback = callback;
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RegisterScreenshotCallback(ScreenshotCallback cb)
{
	Core::Get().RegisterScreenshotCallback(cb);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void NotifyScreenshotReady(const char* name, bool isOk)
{
	Core::Get().NotifyScreenshotReady(name, isOk);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RegisterLiveDataCallback(LiveDataCallback cb)
{
	Core::Get().RegisterLiveDataCallback(cb);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PageFaultStats GetPageFaultStats()
{
	return EventTracer::Get().GetPageFaultStats();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
EventStorage* GetGPUEventStorage()
{
	ThreadEntry* gpuThread = Core::Get().GetGPUThreadEntry();
	if (!gpuThread)
		return nullptr;

	EventStorage& storage = gpuThread->storage;

	return storage.mode & Mode::GPU ? &storage : nullptr;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void EventStorage::Clear(bool preserveContent)
{
	ClearEvents(preserveContent);
	ClearTags(preserveContent);
	ClearCallstacks(preserveContent);
	ClearLiveBuffers(preserveContent);
	ClearResources(preserveContent);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void EventStorage::ClearEvents(bool preserveContent)
{
	USE_ROCKY_MEMORY;
	samplingCounter = 0;
	eventBuffer.Clear(preserveContent);
	startStopStack.Reset();
	gpuBuffer.Clear(preserveContent);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define FOREACH_TAG_BUFFER(func)		tags.func;\
										tagsFloat.func; \
										tagsS32.func; \
										tagsU32.func; \
										tagsU64.func; \
										tagsVec3.func; \
										tagsString.func; \
										tagsImage.func; \
										tagsDrawcall.func;


void EventStorage::ClearTags(bool preserveContent)
{
	USE_ROCKY_MEMORY;
	FOREACH_TAG_BUFFER(Clear(preserveContent));
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void EventStorage::ReserveMinimumMemory()
{
	USE_ROCKY_MEMORY;
	eventBuffer.Reserve(1);
	callstackBuffer.Reserve(1);
	FOREACH_TAG_BUFFER(Reserve(1));
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void EventStorage::ClearResources(bool preserveContent)
{
#if USE_PROFILER_FOR_MT_RESOURCES
	resourceUsageBuffer.Clear(preserveContent);
	resourceSignalBuffer.Clear(preserveContent);
#else
	(void)preserveContent;
#endif
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void EventStorage::ClearLiveBuffers(bool preserveContent)
{
	for (int i = 0; i < LIVE_EVENT_BUFFER_COUNT; ++i)
	{
		liveBuffer[i].events.Clear(preserveContent);
		liveBuffer[i].tags.Clear(preserveContent);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void EventStorage::ClearCallstacks(bool preserveContent)
{
	callstackBuffer.Clear(preserveContent);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ROCKY_NOINLINE void AttachCallStack(rage::u32 mode, rage::u32 timestamp)
{
	if (EventStorage* storage = TLS::storage)
	{
		if (storage->mode & Mode::INSTRUMENTATION)
		{
			// Mute rocky to avoid deadlock
			ROCKY_MUTE;

			CallStackEntry& entry = storage->callstackBuffer.Add();

			// Store time of the event
			entry.time.start = timestamp;
			entry.mode = mode;

			// Capture current callstack
			rage::sysStack::CaptureStackTrace(entry.callstack, MAX_CALLSTACK_DEPTH, 2);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ROCKY_NOINLINE void AttachCallStack(rage::u32 mode, rage::u32 timestamp, size_t* addresses, size_t count)
{
	if (EventStorage* storage = TLS::storage)
	{
		if (storage->mode & mode)
		{
			// Mute rocky to avoid deadlock (memory allocation on storage growth)
			ROCKY_MUTE;

			CallStackEntry& entry = storage->callstackBuffer.Add();

			// Store time of the event
			entry.time.start = timestamp;
			entry.mode = mode;

			// Trim the callstack if it is longer than MAX_CALLSTACK_DEPTH
			size_t depth = Min(count, (size_t)MAX_CALLSTACK_DEPTH);
			sysMemCpy(entry.callstack, addresses, depth * sizeof(size_t));

			if (depth < MAX_CALLSTACK_DEPTH)
				entry.callstack[depth] = 0;
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ROCKY_NOINLINE size_t* CreateCallStack(rage::u32 mode, rage::u32 timestamp)
{
	if (EventStorage* storage = TLS::storage)
	{
		if (storage->mode & mode)
		{
			CallStackEntry& entry = storage->callstackBuffer.Add();
			entry.time.start = timestamp;
			entry.mode = mode;

			return entry.callstack;
		}
	}

	return nullptr;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AttachCaptureInfo(const char* name, const char* value)
{
	if (!StringNullOrEmpty(value))
		RunDescriptionBoard::Get().Add(name, value);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RegisterCaptureControlCallback(CaptureControlCallback callback)
{
	Core::Get().RegisterCaptureControlCallback(callback);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void UnRegisterCaptureControlCallback(CaptureControlCallback callback)
{
	Core::Get().UnRegisterCaptureControlCallback(callback);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RegisterCaptureCategoryCallback(CaptureCategoryCallback callback, rage::sysIpcCurrentThreadId threadId /*= sysIpcCurrentThreadIdInvalid*/)
{
	Core::Get().RegisterCaptureCategoryCallback(callback, threadId);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RegisterResolveScriptSymbolCallback(ResolveScriptSymbolCallback callback)
{
	Core::Get().sampler.RegisterResolveScriptSymbolCallback(callback);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
App::App()
{
	// Start a new frame for the APP
	Core::NextFrame();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
App::~App()
{
	NextFrame();
	SaveCaptureAfterHang(nullptr);

	// Release all the resources
	Core::Get().Shutdown();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void App::StartCapture(rage::u32 mode)
{
	rockyFatalAssertf(sysThreadType::IsUpdateThread(), "App::StartCapture should be called on the MainThread!");
	Core::Get().StartCapture(mode);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void App::StopCapture(const char* filename /*= nullptr*/, bool force)
{
	const bool singleThreaded = false;
	Core::Get().DumpToFile(filename, force, singleThreaded);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void App::SaveCaptureAfterHang(const char* filename)
{
	const bool singleThreaded = true;
	Core::Get().Update(singleThreaded);
	const bool force = true;
	Core::Get().DumpToFile(filename, force, singleThreaded);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void App::SetSamplingFrequency(int frequency)
{
	Core::Get().SetSamplingFrequency(frequency);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int App::GetSamplingFrequency()
{
	return Core::Get().GetSamplingFrequency();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int App::GetScriptSamplingFrequency()
{
	return Core::Get().GetScriptSamplingFrequency();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void App::SetScriptSamplingFrequency(int frequency)
{
	Core::Get().SetScriptSamplingFrequency(frequency);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const EventDescription* CommonDescriptions::s_operatorNewDescription = nullptr;
const EventDescription* CommonDescriptions::s_operatorDeleteDescription = nullptr;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CommonDescriptions::Init()
{
	if (!s_operatorNewDescription)
		s_operatorNewDescription = Profiler::EventDescription::CreateShared("operator_new");

	if (!s_operatorDeleteDescription)
		s_operatorDeleteDescription = Profiler::EventDescription::CreateShared("operator_delete");
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rage::u32 GetActiveMode()
{
	return Core::Get().GetMode();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rage::u32 EventTime::CalcIntersect(EventTime other) const 
{ 
	rage::u32 left = Max(start, other.start); 
	rage::u32 right = Min(finish, other.finish); 
	return (right > left) ? right - left : 0; 
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GPUEvent::AddEvent(const EventDescription* description, rage::u64 start, rage::u64 finish)
{
	EventStorage* storage = TLS::storage;
	if (storage && (storage->mode & Mode::GPU) != 0)
	{
		EventData& data = storage->gpuBuffer.Add();
		data.description = description;
		data.start = Timestamp::Pack(start);
		data.finish = Timestamp::Pack(finish);

		if (description && description->IsLive())
		{
			LiveEventData& liveData = storage->NextLiveEvent();
			// Mark event as not complete
			liveData.start = data.start;
			liveData.finish = data.finish;
			liveData.description = description;
			liveData.child = &data;
		}
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static const rage::u8 GPU_IDLE_SWITCH_CONTEXT_REASON = 128;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u32 ConvertThreadID(sysIpcCurrentThreadId id)
{
	return static_cast<u32>((u64)id);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GPUEvent::AddIdle(const EventDescription* description, rage::u64 start, rage::u64 finish)
{
	EventStorage* storage = TLS::storage;
	if (storage && (storage->mode & Mode::GPU) != 0)
	{
		GPUEvent::AddEvent(description, start, finish);
		
		SynchronizationBuffer& gpuBuffer = EventTracer::Get().gpuSwitchContextEvents;
		gpuBuffer.Push(SwitchContextData(Timestamp::Pack(start), ConvertThreadID(sysIpcGpuThreadId), ConvertThreadID(sysIpcNullThreadId), GPU_IDLE_SWITCH_CONTEXT_REASON));
		gpuBuffer.Push(SwitchContextData(Timestamp::Pack(finish), ConvertThreadID(sysIpcNullThreadId), ConvertThreadID(sysIpcGpuThreadId), GPU_IDLE_SWITCH_CONTEXT_REASON));
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const char* GenerateCaptureName()
{
	static char name[RAGE_MAX_PATH];
	fiDevice::SystemTime time;
	fiDevice::GetLocalSystemTime(time);
	const char* appName = rage::fiAssetManager::FileName(sysParam::GetProgramName());
	formatf(name, "%s(%d.%02d.%02dT%02d-%02d-%02d.%03d)", appName, time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);
	return name;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SetListenPort(int port)
{
	Core::Get().SetListenPort(port);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void tagcpy(TagString&tagString, const char *src)
{
	char *dest = tagString.data;
	unsigned remain = sizeof(RockyShortString);
	while (*src && --remain)
		*dest++ = *src++;
	*dest = 0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Core::DumpRequest::DumpRequest()
{
	Reset();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Core::DumpRequest::Reset()
{
	dumpMode = Mode::OFF;
	isSinglethreaded = false;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RockyThreadFilter::Init()
{
	USE_ROCKY_MEMORY;

	const char *threadNames[32] = {0};
	char threadsBuffer[1024] = {0};

	int threadsCount = PARAM_rockyThreads.GetArray(threadNames, NELEM(threadNames), threadsBuffer, NELEM(threadsBuffer));
	if (threadsCount)
	{
		// m_Threads.ReserveExtend((u32)threadsCount);
		for (int i = 0; i < threadsCount; ++i)
			m_Threads.PushAndGrow(rage::atString(threadNames[i]));
	}
	m_Initialized = true;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RockyThreadFilter::Add(const char* name)
{
	USE_ROCKY_MEMORY;
	m_Threads.PushAndGrow(rage::atString(name));
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RockyThreadFilter::IsOK(const ThreadDescription& desc)
{
	if (!m_Initialized)
		Init();

	if (m_Threads.GetCount() == 0)
		return true;

	for (const rage::atString& thread : m_Threads)
		if (stristr(desc.name, thread.c_str()))
			return true;

	return false;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER
