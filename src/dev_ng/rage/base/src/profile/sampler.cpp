#include "settings.h"

#if USE_PROFILER

#include "common.h"
#include "core.h"
#include "event.h"
#include "sampler.h"
#include "serialization.h"

#include "sym_engine.h"


#include "file/handle.h"
#include "system/ipc.h"
#include "system/param.h"
#include "system/stl_wrapper.h"

namespace Profiler
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct CallStackTreeNode
{
	rage::u64 dwArddress;
	rage::u32 invokeCount;

	rage::stlList<CallStackTreeNode> children;

	CallStackTreeNode()									: dwArddress(0), invokeCount(0) {}
	CallStackTreeNode(rage::u64 address)	: dwArddress(address), invokeCount(0) {} 

	bool Merge(const CallStackBuffer& callstack, int index)
	{
		++invokeCount;
		if (index == 0)
			return true;

		// I suppose, that usually sampling function has only several children.. so linear search will be fast enough
		rage::u64 address = callstack.data[index];
		for (auto it = children.begin(); it != children.end(); ++it)
			if (it->dwArddress == address)
				return it->Merge(callstack, index - 1);

		// Didn't find node => create one
		children.push_back(CallStackTreeNode(address));
		return children.back().Merge(callstack, index - 1); 
	}

	void CollectAddresses(rage::stlUnorderedSet<rage::u64>& addresses) const
	{
		addresses.insert(dwArddress);
		for (auto it = children.begin(); it != children.end(); ++it)
			it->CollectAddresses(addresses);
	}

	OutputDataStream& Serialize(OutputDataStream& stream) const
	{
		stream << (rage::u64)dwArddress << invokeCount;

		stream << (rage::u32)children.size();
		for (auto it = children.begin(); it != children.end(); ++it)
			it->Serialize(stream);

		return stream;
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Sampler::StopSampling()
{
	if (!IsActive())
		return false;

	targetThreads.clear();

	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CallStackBuffer* Sampler::TryAppendCallstack(rage::u64 threadID, rage::u64 timestamp)
{
	rage::u32 time = Timestamp::Pack(timestamp);
	
	for (int i = 0; i < targetThreads.size(); ++i)
	{
		if (targetThreads[i]->description.systemThreadId == threadID)
		{
			EventStorage& storage = targetThreads[i]->storage;

			bool toAdd = false;

			storage.eventBuffer.ForEachChunk([&](const EventData* data, uint count)
			{
				if (count > 0 && time <= data[count - 1].finish && data[0].start <= time && !toAdd)
				{
					// Binary search for event
					uint left = 0;
					uint right = count - 1;

					while (left != right)
					{
						uint mid = (left + right + 1) / 2;

						if (data[mid].start <= time)
							left = mid;
						else
							right = mid - 1;
					}

					if (data[left].start <= time && time <= data[left].finish)
						toAdd = true;
				}
			});

			return toAdd ? &callstacks.Add() : 0;
		}
	}

	return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Sampler::Sampler() : resolveScriptSymbolCb(nullptr)
{
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Sampler::~Sampler()
{
	StopSampling();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Sampler::RegisterResolveScriptSymbolCallback(ResolveScriptSymbolCallback callback)
{
	resolveScriptSymbolCb = callback;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Sampler::StartSampling(const rage::atVector<ThreadEntry*>& threads)
{
	if (IsActive())
		StopSampling();

	targetThreads = threads;

	callstacks.Clear(false);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool Sampler::IsActive() const
{
	return !targetThreads.empty();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& operator<<(OutputDataStream& os, const Symbol * const symbol)
{
	if (!rockyVerifyf(symbol, "Can't serialize NULL symbol!"))
		return os;

	return os << symbol->address << symbol->offset << symbol->module << symbol->function << symbol->file << symbol->line;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OutputDataStream& Sampler::Serialize(OutputDataStream& stream)
{
	rockyFatalAssertf(!IsActive(), "Can't serialize active Sampler!");

	if (IsActive())
		return stream;

	rage::u32 count = (rage::u32)callstacks.Size();
	stream << count;

	if (count == 0)
		return stream;

	CallStackTreeNode tree;

	Core::Get().DumpProgress("Merging CallStacks...");

	callstacks.ForEach([&](const CallStackBuffer& buffer) -> bool
	{
		int count = 0;
		while (count < MAX_CALLSTACK_DEPTH && buffer.data[count] != 0)
			++count;

		if (count > 0)
			tree.Merge(buffer, count - 1);

		return true;
	});

	rage::stlUnorderedSet<rage::u64> addresses;
	tree.CollectAddresses(addresses);

	Core::Get().DumpProgress("Resolving Symbols...");

	SymbolArray symbols;
	ResolveAddresses(addresses, symbols);

	stream << rage::sysParam::GetProgramName();
	stream << symEngine.GetModules();
	stream << symbols;

	tree.Serialize(stream);

	callstacks.Clear(false);

	// Clear temporary data for dbghelp.dll (http://microsoft.public.windbg.narkive.com/G2WkSt2k/stackwalk64-performance-problems)
	//symEngine.Close();

	return stream;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Sampler::ResolveAddresses(const rage::stlUnorderedSet<rage::u64>& addresses, SymbolArray& symbols)
{
	symEngine.Init();

	symbols.Reserve((rage::u32)addresses.size());

	for (auto it = addresses.begin(); it != addresses.end(); ++it)
		if (const Symbol* symbol = symEngine.ResolveSymbol(*it))
			symbols.push_back(symbol);

	symEngine.Close();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Sampler::ResolveScriptAddresses(const rage::stlUnorderedSet<rage::u64>& addresses, SymbolArray& symbols)
{
	if (resolveScriptSymbolCb == nullptr || addresses.size() == 0)
		return;

	//symbols.ReserveExtend((rage::u32)addresses.size());

	ScriptSymbol scriptSymbol;

	for (auto it = addresses.begin(); it != addresses.end(); ++it)
	{
		if (Symbol* symbol = symEngine.GetOrCreateSymbol(*it))
		{
			if (symbol->address == 0)
			{
				symbol->address = *it;

				if (resolveScriptSymbolCb(*it, scriptSymbol))
				{
					symbol->function = rage::stlWstring(scriptSymbol.m_Name.c_str(), scriptSymbol.m_Name.c_str() + scriptSymbol.m_Name.GetLength());
					symbol->file = rage::stlWstring(scriptSymbol.m_File.c_str(), scriptSymbol.m_File.c_str() + scriptSymbol.m_File.GetLength());
					symbol->line = scriptSymbol.m_Line;
				}
			}
			symbols.PushAndGrow(symbol);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
size_t Sampler::GetCollectedCount() const
{
	return callstacks.Size();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

#endif //USE_PROFILER
