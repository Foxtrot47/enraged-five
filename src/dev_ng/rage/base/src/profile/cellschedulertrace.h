//
// profile/cellschedulertrace.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef PROFILE_CELL_SCHEDULER_TRACE_H
#define PROFILE_CELL_SCHEDULER_TRACE_H

#include "trace.h"

#if __PPU && RAGETRACE

namespace rage {

// wraps the Cell Lv2 OS thread scheduler trace to determine
// which threads are active on the PPU at any time during the frame

class pfCpuTrace;

class pfCellSchedulerTrace : public pfTrace
{
public:
	pfCellSchedulerTrace();
	~pfCellSchedulerTrace();

	u32			GetNextSample();
	void		AdvanceTo(u32 timebase);

	bool		IsThreadActive(u32 thread) const;

private:
	static const u32 SCHED_TRACE_BUF_SIZE = 4 * 1024 * 1024;

	u32*		m_PpuSchedTraceBuf;
	u32			m_HwThreads[2];
	u32			m_ReadPointer;
};

inline bool	pfCellSchedulerTrace::IsThreadActive(u32 thread) const
{
	return m_HwThreads[0] == thread || m_HwThreads[1] == thread;
}

extern pfCellSchedulerTrace* g_pfCellSchedulerTrace;

} // namespace rage

#endif // __PPU && RAGETRACE

#endif 
