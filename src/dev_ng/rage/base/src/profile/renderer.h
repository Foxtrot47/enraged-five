#ifndef PROFILE_RENDERER_H
#define PROFILE_RENDERER_H

#include "system/tls.h"
#include "diag/trap.h"
#include "vectormath/classes.h"
#include "vector/color32.h"

#include <string.h>		// for memcpy

#ifdef _MSC_VER
#pragma warning(disable:4480)
#pragma warning(disable:4481)
#endif


#define RSG_PROFILE_RENDERER (0)
#define REMAP_TO_debugdraw (0)

#if RSG_PROFILE_RENDERER

namespace rage {

// Here is the external render interface to be implemented by the graphics library.

// Values chosen to match a subset of sga::DrawType.
enum pfDrawType {
	pfPoints = 1,
	pfLines = 2,
	pfLineStrip = 3,
	pfTriangles = 4,
	pfTriangleStrip = 5,
	pfTriangleFan = 6,	// this won't work for indexed primitives
	// pfLineLoop = 18,	// grcore doesn't support this so don't expose it yet
	// pfQuads = 19,	// intentionally leave out quads since indexed quads are difficult to emulate on PC
};

// Bit 0 is color; bit 1 is normal; bit 2 is texture coordinates.
enum pfVertexType {
	pfPos, 
	pfPosColor, 
	pfPosNormal,
	pfPosColorNormal,
	pfPosTexCoord,
	pfPosColorTexCoord, 
	pfPosNormalTexCoord,
	pfPosColorNormalTexCoord,
};

enum pfDepthStencilState {
	pfDSS_DepthOn_StencilOff,
	pfDSS_DepthOff_StencilOff,
	pfDSS_IgnoreDepth = pfDSS_DepthOff_StencilOff,
	pfDSS_DepthOn_StencilOn,
	pfDSS_InverseDepthOn_StencilOn,
	pfDSS_DepthTestOnlyGE_StencilOff
};

enum pfRasterizerState {
	pfRS_Default,
	pfRS_NoBackfaceCull
};

enum pfBlendState {
	pfBS_Default,
	pfBS_Normal,
	pfBS_CompositeAlpha,
	pfBS_Additive,
};

struct pfViewport {
	float x, y, width, height, minDepth, maxDepth;
	void Set(float _x,float _y,float _w,float  _h,float _zMin=0.0f,float _zMax=0.0f) {
		x=_x; y=_y; width=_w; height=_h; minDepth=_zMin; maxDepth=_zMax;
	}
};

class pfTexture;
class pfFont;
struct pfIndexedPrimitive;

class pfRenderInterface {
public:
	virtual ~pfRenderInterface() { }
	// Sets a transform matrix
	virtual void SetWorldMatrix(Mat34V_In world);
	virtual void SetCameraMatrix(Mat34V_In camera);
	virtual void SetProjectionMatrix(Mat44V_In proj);
	virtual void SetViewport(const pfViewport &vp);
	// Begins a render primitive
	virtual void* Begin(pfDrawType,unsigned,pfVertexType);
	// Ends a render primitive
	virtual void End();
	virtual void PushDepthStencilState();
	virtual void PopDepthStencilState();
	virtual void SetDepthStencilState(pfDepthStencilState);

	virtual void PushRasterizerState();
	virtual void PopRasterizerState();
	virtual void SetRasterizerState(pfRasterizerState);

	virtual void PushBlendState();
	virtual void PopBlendState();
	virtual void SetBlendState(pfBlendState);

	// Display 2D text
	virtual void DrawString(pfFont*,float x,float y,float z,float scale,unsigned rgba,const char *string,unsigned len);

	virtual void BeginFrame();
	virtual void EndFrame();

	// Renders an indexed primitive using the current world matrix.
	virtual void DrawIndexedPrimtive(pfIndexedPrimitive *);
	// Specifies the constant color when using a format without color (pfPos, pfPosNormal, etc)
	virtual void SetConstantColor(u32 color);

	virtual void SetTexture(pfTexture *tex);
};

class pfRenderFactory {
public:
	virtual ~pfRenderFactory() { }
	// Creates an indexed primitive with no color channel.
	virtual pfIndexedPrimitive* CreateIndexedPrimitive(pfDrawType dt,const void *vertices,size_t vertexCount,pfVertexType vt,const u16 *indices,size_t indexCount);
	virtual pfTexture* CreateTexture(const char *name);		// Or you can simply cast your grcTexture or sga::Texture to a pfTexture.

	virtual unsigned GetScreenWidth() const;
	virtual unsigned GetScreenHeight() const;
	virtual unsigned GetFontHeight(pfFont*) const;
	virtual unsigned GetStringWidth(pfFont*,const char *string,size_t len) const;
};

class pfMemoryInterface {
public:
	virtual void *Allocate(size_t bytes);
	virtual void Free(void *ptr);
};

extern pfRenderInterface *g_pfRenderInterface;
extern pfRenderFactory *g_pfRenderFactory;
extern pfMemoryInterface *g_pfMemoryInterface;

extern bool g_HDRUIUseColorAdjustment;

// Here is the batched render interface layered on top of the basic render interface.
// This is what clients actually use.

namespace debugDraw {

extern pfFont *FontFixed, *FontProportional, *FontTiny;


const int BeginMax = 8192;

// Create a sphere primitive (Z-up) of given radius; this can become an ellipsoid with proper scaling in the world matrix
extern pfIndexedPrimitive* CreateSphere(float radius,unsigned longSteps,unsigned latSteps,unsigned longSkip,unsigned latSkip);
// Creates a unit spherical cone looking down the ... who knows
extern pfIndexedPrimitive* CreateSphericalCone(unsigned longSteps,unsigned latSteps);
// Creates a box with corners (s,s,s) and (-s,-s,-s)
extern pfIndexedPrimitive* CreateBox(float s);
extern pfIndexedPrimitive* CreateCircle(float r,unsigned steps);

extern bool Paused;

struct Header;
class BatchInterface;
typedef u8 HandlerIndex;

struct Header {
	HandlerIndex index;
	u8 byteParam;
	u16 paramCount;
};

CompileTimeAssertSize(Header,4,4);

struct Page {
	Page *next, *prev;
	static const unsigned MaxCount = (16384-2*sizeof(void*))/sizeof(Header);
	Header header[MaxCount];
};

CompileTimeAssertSize(Page,16384,16384);

typedef void (BatchInterface::*VertexHandler)(float,float,float);
typedef void (*CallbackHandler)(BatchInterface*,Header*);
class BatchInterface;

class BatchCursor {
	friend class BatchInterface;
public:
	BatchCursor() {
		m_CurrentPage = m_FirstPage = NULL;
		m_PageOffset = m_PageSize = 0;
	}
	void Flush();
	Header* AddCommand(HandlerIndex &index,CallbackHandler handler,u8 byteParam,size_t paramSize);
	void Process(BatchInterface *batch,BatchCursor *next);
private:
	Page *m_CurrentPage, *m_FirstPage;
	unsigned m_PageOffset, m_PageSize;
};

/*
	This class is intentionally not thread-safe, for performance reasons.  Every thread should have its own separate BatchInterface.
	Furthermore, higher-level code is expected to double-buffer each thread's BatchInterface, switching to a fresh BatchInterface
	prior to either directly calling Process on the old one or placing that thread's BatchInterface into a work queue to be processed
	by the render thread.
*/
class BatchInterface {
public:
	BatchInterface(pfRenderInterface *ri);

	Header* AddCommand(HandlerIndex &slot,CallbackHandler handler,u8 byteParam,size_t paramSize);
	void AddCommandAndPayload(HandlerIndex &slot,CallbackHandler handler,u8 byteParam,size_t paramSize,const void *params);

	// If next is NULL, we play this back without freeing anything.  Otherwise we clear this list and
	// any persistent rendering is added to 'next'.  Just pass in 'this' for the simple single-threaded case in sample code.
	void Process(BatchCursor *next);
	// This is only suitable for single-threaded use.
	void Process() {
		Process(&m_Cursor);
	}

	void Flush();

	void Begin(pfDrawType dt,unsigned vc,pfVertexType vt = pfPosColor);
	void End() {
		if (m_IsImmediate)
			m_RenderInterface->End();
	}

	static const unsigned MaxDepth = 4;
	Mat34V m_World, m_WorldStack[MaxDepth], m_Camera, m_CameraStack[MaxDepth];
	Mat44V m_Projection, m_ProjectionStack[MaxDepth];
	pfViewport m_Viewport, m_ViewportStack[MaxDepth];
	unsigned m_WorldStackCount, m_CameraStackCount, m_ProjectionStackCount, m_ViewportStackCount;
	unsigned m_FramesToLive;
	VertexHandler m_Vertex3f;

	void BeginFrame() {
		m_RenderInterface->BeginFrame();
	}

	void EndFrame() {
		m_RenderInterface->EndFrame();
	}

	void Vertex3f_Pos(float,float,float);
	void Vertex3f_PosColor(float,float,float);
	void Vertex3f_PosNormal(float,float,float);
	void Vertex3f_PosColorNormal(float,float,float);
	void Vertex3f_PosTexCoord(float,float,float);
	void Vertex3f_PosColorTexCoord(float,float,float);
	void Vertex3f_PosNormalTexCoord(float,float,float);
	void Vertex3f_PosColorNormalTexCoord(float,float,float);

	unsigned m_Color;
	pfFont *m_Font;
	float m_NormalX, m_NormalY, m_NormalZ, m_TexCoordS, m_TexCoordT;
	float *m_Current, *m_Stop;
	static u8 sm_VertexSizes[8];
	bool m_IsImmediate;

	pfRenderInterface *m_RenderInterface;

	void UpdateInternalMatrices();
	Mat44V m_View, m_ViewProj, m_InverseViewProj;

	BatchCursor m_Cursor, m_Persistent;
};

}	// namespace debugDraw

extern DECLARE_THREAD_PTR(debugDraw::BatchInterface, g_debugDraw);
extern DECLARE_THREAD_PTR(debugDraw::BatchInterface, g_debugDraw2D);
extern DECLARE_THREAD_PTR(debugDraw::BatchInterface, g_debugDraw3D);
extern DECLARE_THREAD_VAR(s8,g_debugDrawStack);	// Bit stack; 1=2D, 0=3D was previously active; Initialized with 0x2 in assert builds to catch underflow and overflow

}	// namespace rage

#endif	// RSG_PROFILE_RENDERER

#endif	 // PROFILE_RENDERER_H
