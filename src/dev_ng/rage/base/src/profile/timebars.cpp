//
// profile/timebars.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "timebars.h"

#if __PS3 && !__FINAL
namespace rage 
{
	void pfDummySync()
	{
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
	}
}
#endif // __PS3 && !__FINAL

#if RAGE_TIMEBARS
namespace rage {
	namespace pfTimebarFuncs {

		void StartCb_NoOp			(uptr /*dummy*/, const char*, const char *, unsigned int, bool, float, ProfileZoneType ) {}
		void PushCb_NoOp			(uptr /*dummy*/, const char*, const char *, unsigned int, bool, float, ProfileZoneType ) {}
		void PopCb_NoOp				(uptr /*dummy*/, int ) {}
		void StartStartupBarCb_NoOp	(uptr /*dummy*/, const char*, const char *, unsigned int, bool, float, ProfileZoneType ) {}
		void PushStartupBarCb_NoOp	(uptr /*dummy*/, const char*, const char *, unsigned int, bool, float, ProfileZoneType ) {}
		void PopStartupBarCb_NoOp	(uptr /*dummy*/, int ) {}
		void PushMarkerCb_NoOp		(const char*, int) {}
		void PopMarkerCb_NoOp		(int ) {}

		StartCb				g_StartCb			= StartCb_NoOp;
		PushCb				g_PushCb			= PushCb_NoOp;
		PopCb				g_PopCb				= PopCb_NoOp;
		StartStartupBarCb	g_StartStartupBarCb = StartStartupBarCb_NoOp;
		PushStartupBarCb	g_PushStartupBarCb	= PushStartupBarCb_NoOp;
		PopStartupBarCb		g_PopStartupBarCb	= PopStartupBarCb_NoOp;
		PushMarkerCb		g_PushMarkerCb		= PushMarkerCb_NoOp;
		PopMarkerCb			g_PopMarkerCb		= PopMarkerCb_NoOp;

	}   // namespace pfTimebarFuncs
}	// namespace rage
#endif // RAGE_TIMEBARS



