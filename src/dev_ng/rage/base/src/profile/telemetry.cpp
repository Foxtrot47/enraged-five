#include "profile/telemetry.h"

#include "bank/bkmgr.h"
#include "file/tcpip.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/new.h"
#include "system/memory.h"
#include "system/simpleallocator.h"

#if USE_TELEMETRY

namespace rage
{
////////////////////////////////////////////////////////////////////////////////////////////////////
__THREAD const char * g_TrackingFile = NULL;
__THREAD unsigned int g_TrackingLine = 0xFFFFFFFF;

////////////////////////////////////////////////////////////////////////////////////////////////////
TelemetryCallbacks g_TelemetryCallbacks = { NULL };

////////////////////////////////////////////////////////////////////////////////////////////////////

const TelemetryCallbacks & GetTelemetryCallbacks()
{
	return g_TelemetryCallbacks;
}

void SetTelemetryCallbacks(const TelemetryCallbacks & callbacks)
{
	g_TelemetryCallbacks = callbacks;
}

}

#endif // USE_TELEMETRY