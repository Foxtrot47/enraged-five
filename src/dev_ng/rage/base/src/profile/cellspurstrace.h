//
// profile/cellspurstrace.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef PROFILE_CELL_SPURS_TRACE_H
#define PROFILE_CELL_SPURS_TRACE_H

#include "trace.h"

#if __PPU && RAGETRACE
#include <cell/spurs.h>

namespace rage {

class pfCellSpuTrace;

// manage a set of SPURS traces
// class pfCellSpursTrace 
// {
// public:
// 	pfCellSpursTrace(CellSpurs* spurs, u32 firstSpu, u32 numSpus, u32 maxTracePacketsPerSpu);
// 	~pfCellSpursTrace();
// 
// private:
// 	CellSpurs*					m_Spurs;
// 	CellSpursTraceInfo*			m_SpursTraceBuffer;
// 	atArray<pfCellSpuTrace*>	m_SpuTraces;
// };
// 
// // manage a single SPU trace
// class pfCellSpuTrace : public pfTrace
// {
// public:
// 	pfCellSpuTrace(const char* name, u32 spu, CellSpursTracePacket* pBuffer, u32 maxTracePackets);
// 
// 	u32		GetNextSample();
// 	void	AdvanceTo(u32 timebase);
// 
// private:
// 	u32						m_Spu;
// 	CellSpursTracePacket*	m_Buf;
// 	CellSpursTracePacket*	m_BufEnd;
// 	CellSpursTracePacket*	m_BufRead;
// 	const char*				m_ActiveJob;
// 	pfTraceCounterId		m_RootId;
// };

class pfSpuTrace : public pfBufferedTrace
{
public:
	pfSpuTrace(const char* name, int bufsize)
	:	pfBufferedTrace(name, bufsize, rage_new pfTraceCounter(&m_RootId))
	,	m_RootId(name, Color32(0))
	{}
	virtual void OnEndFrame();
private:
	void UpdateTotals(pfTraceCounter* spu, pfTraceCounter* total);
	pfTraceCounterId	m_RootId;	
};

class pfSpuTotalTrace : public pfTrace
{
public:
	pfSpuTotalTrace(const char* name)
	:	pfTrace(name, rage_new pfTraceCounter(&m_RootId))
	,	m_RootId(name, Color32(0))
	{
		m_NumRows = 0; // no time bars, just text
	}
	virtual u32 GetNextSample() {return 0;}
private:
	pfTraceCounterId m_RootId;
};

extern pfSpuTrace* g_pfSpuTrace[6];
extern pfSpuTotalTrace* g_pfSpuTotals;

} // namespace rage

#endif // __PPU && RAGETRACE

#if __SPU 
#if RAGETRACE
#include "system/dma.h"
#include <cell/dma.h>
#include <cell/spurs/common.h>

namespace rage {

class pfTraceCounterId;

class pfSpuTrace
{
public:
	pfSpuTrace() : m_traceAddr(0) {}
	pfSpuTrace(u32 traceArrayAddr)
	{
		Init(traceArrayAddr);
	}

	void Init(u32 traceArrayAddr)
	{
		if (!traceArrayAddr)
			return;

		u32 spuId = cellSpursGetCurrentSpuId();
		m_traceAddr = cellDmaGetUint32(traceArrayAddr + spuId * 4, 0, 0, 0);
		if (!m_traceAddr)
			return;
		m_traceAddr += 0x60;
		cellDmaGet(&m_bufInfo, m_traceAddr, sizeof(m_bufInfo), 0, 0, 0);
		cellDmaWaitTagStatusAll(1);		
		m_AutoPop = false;
	}

	void Finish(u32 tag, bool addWait = true)
	{
		if (!m_traceAddr)
			return;
		cellDmaSmallPut(&m_bufInfo.m_BufWrite, m_traceAddr + 8, 4, tag, 0, 0);
		if( addWait )
		{
			cellDmaWaitTagStatusAll(1U<<tag);
		}
		m_traceAddr = 0;
	}

	void Push(pfTraceCounterId* traceid, bool autopop = false)
	{
		if (!m_traceAddr)
			return;
		u32 t = ~spu_readch(SPU_RdDec);
		if (m_AutoPop)
			Store(0, t);
		Store(traceid, t);
		m_AutoPop = autopop;
	}

	void Pop()
	{
		Push(0);
	}
	
private:
	void Store(pfTraceCounterId* identifier, u32 timebase)
	{		
		u64 writeAddr = (u64)m_bufInfo.m_BufWrite;
		m_bufInfo.m_BufWrite += 2;
		if (m_bufInfo.m_BufWrite == m_bufInfo.m_BufEnd)
			m_bufInfo.m_BufWrite = m_bufInfo.m_Buf;
		cellDmaPutUint64(((u64)identifier)|(((u64)(timebase|1))<<32), writeAddr, 0, 0, 0);
	}

	struct
	{
		u32* m_Buf;
		u32* m_BufEnd;
		u32* m_BufWrite;
		u32* m_BufRead;
	}  m_bufInfo;
	u32		m_traceAddr;
	bool	m_AutoPop;
};

extern pfSpuTrace* g_pfSpuTrace;

class pfSpuTraceScope
{
public:
	pfSpuTraceScope(pfTraceCounterId* id) 
	{
		if (g_pfSpuTrace)
			g_pfSpuTrace->Push(id);
	}
	~pfSpuTraceScope() 
	{
		if (g_pfSpuTrace)
			g_pfSpuTrace->Pop();
	}
};

} // namespace rage

#define RAGETRACE_PUSH(id) if (::rage::g_pfSpuTrace) ::rage::g_pfSpuTrace->Push(id)
#define RAGETRACE_START(id) if (::rage::g_pfSpuTrace) ::rage::g_pfSpuTrace->Push(id, true)
#define RAGETRACE_POP() if (::rage::g_pfSpuTrace) ::rage::g_pfSpuTrace->Pop()
#define RAGETRACE_SCOPE(id) ::rage::pfSpuTraceScope _ragetrace_##__LINE__(id)

#else // RAGETRACE

namespace rage {

class pfSpuTrace
{
public:
	pfSpuTrace() {}
	pfSpuTrace(u32) {}
	void Init(u32) {}
	void Finish(u32) {}
	void Push(u32) {}
	void Pop() {}
};

} // namespace rage

#define RAGETRACE_PUSH(id)
#define RAGETRACE_START(id)
#define RAGETRACE_POP() 
#define RAGETRACE_SCOPE(id) 

#endif // RAGETRACE

#endif // __SPU

#endif // PROFILE_CELL_SPURS_TRACE_H
