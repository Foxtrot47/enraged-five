#ifndef _DIAGERRORCODES_H_
#define _DIAGERRORCODES_H_

#if __WIN32PC
#include "atl/map.h"
#include "diag/errorcodes.h"
#include "system/language.h"

namespace rage
{

class diagErrorCodes
{
private:
	static bool					m_bLoaded;
	static bool					m_bLoading;
	static bool					m_bShowExtraReturnCodeNumberInHex;
	static sysLanguage			m_currentLanguage;
	static s32					m_ExtraReturnCodeNumber;
	static atMap<u32, wchar_t*>	m_ErrorCodes;
	static atMap<u32, wchar_t*>	m_ErrorMessages;

public:
	static bool		LoadLanguageFile();

	static sysLanguage		GetCurrentLanguage() { return m_currentLanguage; }

	static wchar_t*	GetErrorCodeString(FatalErrorCode errorCode);
	static wchar_t*	GetDefaultErrorCodeString();
	static wchar_t*	GetErrorMessageString(FatalErrorCode errorCode);
	static wchar_t*	GetDefaultErrorMessageString();

	static bool		IsErrorCodeValid(u32 errorCode) { return m_ErrorCodes.Access(errorCode) != 0; }
	static bool		IsErrorCodesLoaded() { return m_bLoaded; }

	static void		SetExtraReturnCodeNumber(s32 extraReturnCodeNumber, bool bShowInHex = true) { m_ExtraReturnCodeNumber = extraReturnCodeNumber; m_bShowExtraReturnCodeNumberInHex = bShowInHex; }
	static s32		GetExtraReturnCodeNumber() { return m_ExtraReturnCodeNumber; }
	static bool		ShowExtraReturnCodeNumberInHex() { return m_bShowExtraReturnCodeNumberInHex; }

	// Convert a Win32 error from GetLastError() to a char* string. Formats into the buffer provided,
	// and returns pBuffer to allow the string to be passed to another function more easily.
	static char* Win32ErrorToString(int error, char* pBuffer, size_t bufferLen);
};

} // namespace rage

#endif // __WIN32PC
#endif // _DIAGERRORCODES_H_


