#undef __ASSERT
#undef ASSERT_ONLY
#undef Assert
#undef FastAssert
#undef AssertMsgf
#undef AssertMsg

#define __ASSERT			1
#define ASSERT_ONLY(x)			x
#define Assert(x)			(void)(x)
#define FastAssert(x)		(void)(x)
#define AssertMsgf(x,args)	(void)(x)
#define AssertMsg(x,msg)	(void)(x)

