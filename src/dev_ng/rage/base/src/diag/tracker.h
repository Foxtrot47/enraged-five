// 
// diag/tracker.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DIAG_TRACKER_H
#define DIAG_TRACKER_H

#include <stddef.h>		// for size_t

#include "system/tls.h"	// for __THREAD macro

#define RAGE_TRACKING			(!__FINAL && !__SPU)

#if RAGE_TRACKING

#define RAGE_TRACKING_ONLY(...)	__VA_ARGS__ 

// Creates temporary, scoped section name from the identifier provided
#define RAGE_TRACK(x)			    ::rage::diagTrackerHelper track_##x(#x)

// Helper macro for RAGE_CREATE_TRACKER_VARIABLE_UNIQUE
#define RAGE_CREATE_TRACKER_VARIABLE(x,y)			::rage::diagTrackerHelper track_##y(x)

// Helper macro for RAGE_TRACK_NAME
#define RAGE_CREATE_TRACKER_VARIABLE_UNIQUE(x,y)	RAGE_CREATE_TRACKER_VARIABLE(x,y)

#if (!__PPU)
// Creates a temporary, scoped section name from the null-terminates string provided
#define RAGE_TRACK_NAME(x)							RAGE_CREATE_TRACKER_VARIABLE_UNIQUE(x,__COUNTER__)
#else
// Creates a temporary, scoped section name from the null-terminates string provided
#define RAGE_TRACK_NAME(x)							RAGE_CREATE_TRACKER_VARIABLE_UNIQUE(x,__LINE__)
#endif

// Pops the current section name and pushes the identifier provided
#define RAGE_NEW_TRACK(x)		    ::rage::diagTrackerHelper::SwitchTo(#x)

#if HACK_RDR2
#define RAGE_NEW_TRACK_BUCKET(x)	::rage::diagTrackerHelper::SwitchTo(#x); ::rage::sysMemUseMemoryBucket::SwitchTo(TRACK_MEMORY_BUCKET_##x)
#define RAGE_TRACK_BUCKET(x)		::rage::diagTrackerHelper track_##x(#x); ::rage::sysMemUseMemoryBucket bucket_track_##x(TRACK_MEMORY_BUCKET_##x)
#endif

// Generates a report
#define RAGE_TRACK_REPORT()		    if (::rage::diagTracker::GetCurrent()) ::rage::diagTracker::GetCurrent()->Report()

// Generates a report, then sets TrackDeallocations
#define RAGE_TRACK_REPORT_FREES(b) \
	if (::rage::diagTracker::GetCurrent()) \
	{ \
	::rage::diagTracker::GetCurrent()->Report(); \
	::rage::diagTracker::GetCurrent()->TrackDeallocations(b); \
	}

// Generates a report with the given null-terminating string as its name
#define RAGE_TRACK_REPORT_NAME(x)	if (::rage::diagTracker::GetCurrent()) ::rage::diagTracker::GetCurrent()->Report(x)

// Generates a report with the given null-terminating string as its name, then sets TrackDeallocations
#define RAGE_TRACK_REPORT_NAME_FREES(x,b) \
	if (::rage::diagTracker::GetCurrent()) \
	{ \
		::rage::diagTracker::GetCurrent()->Report(x); \
		::rage::diagTracker::GetCurrent()->TrackDeallocations(b); \
	}

// Tallies an allocation on new
// This macro is NOT multi-statement safe and it also evaluates its input more than once.
#define RAGE_TRACKING_TALLY(result,heapType) \
	/*if (!::rage::g_TrackerDepth) __debugbreak();*/ \
	AssertMsg(::rage::g_TrackerDepth, "Untracked memory allocation! Please add a RAGE_TRACK here."); \
	if (result && ::rage::diagTracker::GetCurrent() && ::rage::sysMemAllocator::GetCurrent().IsTallied() ) \
		::rage::diagTracker::GetCurrent()->Tally(result,::rage::sysMemAllocator::GetCurrent().GetSizeWithOverhead(result),heapType); \

// Untallies an allocation on delete
// This macro is NOT multi-statement safe and it also evaluates its input more than once.
#define RAGE_TRACKING_UNTALLY(ptr) \
	if (ptr && ::rage::diagTracker::GetCurrent() && ::rage::sysMemAllocator::GetCurrent().IsTallied()) \
		::rage::diagTracker::GetCurrent()->UnTally(ptr,::rage::sysMemAllocator::GetCurrent().GetSizeWithOverhead(ptr))

// Adds additiional information about an allocation that can only be retrieved from macro new
// This macro is NOT multi-statement safe and it also evaluates its input more than once.
#define RAGE_TRACKING_INFO(ptr,size,typeName,file,line) \
    if (ptr && ::rage::diagTracker::GetCurrent() && ::rage::sysMemAllocator::GetCurrent().IsTallied()) \
        ::rage::diagTracker::GetCurrent()->AddInfo(ptr,size,typeName,file,line);

#else
#define RAGE_TRACKING_ONLY(...)
#define RAGE_TRACK(x)
#define RAGE_CREATE_TRACKER_VARIABLE(x,y)
#define RAGE_CREATE_TRACKER_VARIABLE_UNIQUE(x,y)
#define RAGE_TRACK_NAME(x)
#define RAGE_NEW_TRACK(x)
#if HACK_RDR2
#define RAGE_TRACK_BUCKET(x)
#define RAGE_NEW_TRACK_BUCKET(x)
#endif
#define RAGE_TRACK_REPORT()
#define RAGE_TRACK_REPORT_FREES(b)
#define RAGE_TRACK_REPORT_NAME(x)
#define RAGE_TRACK_REPORT_NAME_FREES(x,b)
#define RAGE_TRACKING_TALLY(result,heapType)
#define RAGE_TRACKING_UNTALLY(ptr)
#define RAGE_TRACKING_INFO(ptr,size,typeName,file,line)
#endif

#if RAGE_TRACKING

namespace rage 
{

extern __THREAD int g_TrackerDepth;

class diagTracker 
{
public:
	// These are additional bits that can be added to the memory bucket
	enum {
		// If set, this block of memory can be defragged
		MEMBUCKETFLAG_CAN_DEFRAG = (1 << 30),

		// If set, this block of memory cannot be defragged due to stream locking
		MEMBUCKETFLAG_LOCKED = (1 << 29),

		// If set, this block of memory cannot be defragged due to external allocation
		MEMBUCKETFLAG_EXTERNAL = (1 << 28),

		// If set, this block of memory was moved during defragmentation
		MEMBUCKETFLAG_MOVED = (1 << 27),

		// This is the mask that extracts the flags
		MEMBUCKET_FLAGSMASK = 0xffff0000,

		// This is the mask that denotes the actual memory bucket
		MEMBUCKET_BUCKETMASK = 0xffff,
	};
	diagTracker();
	virtual ~diagTracker();

    // PURPOSE: Add a section name to the stack
    // PARAMS:
    //  name - name of the section
	virtual void Push( const char* name ) = 0;

    // PURPOSE: Pops the top section name off the stack
	virtual void Pop() = 0;

    // PURPOSE: Tallies an allocation on new, etc.
    // PARAMS:
    //  object - address of the allocation
    //  size - size of the object, with overhead
    //  heapType - identifies which heap the object was allocated on
	virtual void Tally( void* object, size_t size, int heapType ) = 0;	// used by new, etc

    // PURPOSE: Untallies an allocation on delete, etc.
    // PARAMS:
    //  object - address of the de-allocation
    //  size - size of object
    // RETURNS:
    // NOTES:
	virtual bool UnTally( void* object, size_t size ) = 0;		// used by delete

	// PURPOSE: Updates a block to indicate that it can (or cannot) be defragmented.
	// PARAMS:
	//  object - address of the allocation
	//  isDefraggable - true if it can be defragged, false otherwise.
	virtual void MarkDefragmentable(void * /*addr*/, bool /*true/false*/) {}
	
	virtual void MarkLocked(void * /*addr*/, bool /*true/false*/) {}

	virtual void MarkExternal(void * /*addr*/, bool /*true/false*/) {}

	virtual void MarkMoved(void * /*addr*/, bool /*true/false*/) {}

    // PURPOSE: Adds additional information about a Tally that can only be retrieved through macro new
    // PARAMS:
    //  object - address of the allocation
    //  size - size of the object created without overhead
    //  typeName - string name of the object's type
    //  file - file in which the allocation took place
    //  line - line in file on which the allocation took place
    virtual void AddInfo( void* object, size_t size, const char* typeName, const char* file ,int line ) = 0;    // used by macro new

    // PURPOSE: Generates a report
    // PARAMS:
    //  tag - name of the report
    //  largeObject - maximum number of objects to report about
	virtual void Report(const char *tag = 0,size_t largeObject = 512 * 1024) = 0;

	// PURPOSE: Generates a detailed report on the client side (if a client is attached).
	// PARAMS:
	//  filename - Filename to save the report to. Some implementations could extract the directory
	//             portion of this parameter and store multiple files in it.
	virtual void FullReport(const char * /*filename*/) {}

	// PURPOSE: Send version information to the client.
	// PARAMS:
	//   buildVersion - String that identifies the current title version (like "v297.1").
	//   packageType - Any string to identify the package type ("Disc build", "installremote", etc)
	virtual void SetVersionInfo(const char * /*buildVersion*/, const char * /*packageType*/) {}

	// PURPOSE: 
	//  Enable or disable tracking of detailed deallocation information.
	// PARAMS:
	//  enable - true to remember deallocations, false to try to save memory.
	// NOTES:
	//  This function is mainly for the diagTrackerRemote class to help balance the Rag Memory Tracker's
	//  functionality versus its memory usage.
	//  The best way to use this function is to call Report() followed by TrackDeallocations(true), then later call
	//  Report() and TrackDeallocations(false).  This will provide a clear picture for the Report, and use
	//  the least amount of memory.
	virtual void TrackDeallocations( bool /*enable*/ ) {}

	// PURPOSE:
	//  Indicate to the tracking system what target the memory information is coming from
	// PARAMS:
	//  platform - the name of the platform (Win32, Xenon, PS3, etc)
	virtual void SetTargetPlatform( const char* /*platform*/ ) {}

    // PURPOSE: Called once per frame, this will do any updating that needs to be done on a regular interval
    virtual void Update() {}

	// PURPOSE: Set an address to be more verbose about
	virtual void TrackAddr(void *) { }

	// PURPOSE: Inform the tracker about a heap
	virtual void InitHeap(const char* /*name*/, void* /*begin*/, size_t /*size*/) { }

    // PURPOSE: Retrieves the current diagTracker object
    // RETURNS: current diagTracker
	static diagTracker* GetCurrent() { return sm_Current; }

    // PURPOSE: Sets the current diagTracker object
    // PARAMS:
    //  current - the diagTracker that we want to use
    // RETURNS:
    //  the previous diagTracker
	static diagTracker* SetCurrent(diagTracker *current) {
		diagTracker *prev = sm_Current;
		sm_Current = current;
		return prev;
	}

protected:
	static diagTracker *sm_Current;
};

class diagTrackerHelper 
{
public:
    // PURPOSE: Creates a scoped section name on the diagTracker stack
    // PARAMS:
    //  name - name of the section
	__forceinline diagTrackerHelper(const char *name) 
    {
		++g_TrackerDepth;
		if (diagTracker::GetCurrent()) {
				diagTracker::GetCurrent()->Push(name); 
		}
	}

    // PURPOSE: Pops the current section name on the diagTracker stack and pushes the new name
    // PARAMS:
    //  name - name of the new section
	static __forceinline void SwitchTo(const char *name) 
    {
		if (diagTracker::GetCurrent()) {
			diagTracker::GetCurrent()->Pop();
			diagTracker::GetCurrent()->Push(name); 
		}
	}

    // PURPOSE: Removes the scoped section name from the diagTracker stack
	~diagTrackerHelper() 
    { 
		if (diagTracker::GetCurrent()) 
			diagTracker::GetCurrent()->Pop(); 
		--g_TrackerDepth;
	}
};

}	// namespace rage

#endif	// RAGE_TRACKING

#endif	// DIAG_TRACKER_H
