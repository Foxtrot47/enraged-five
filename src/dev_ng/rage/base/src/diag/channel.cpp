// 
// diag/channel.cpp 
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

// For intended use, see grcore/channel.h; rage/build/makechannel.bat can be used to bootstrap a new channel file.

#include "bank/bkmgr.h"
#include "channel.h"
#include "diag/tracker.h"
#include "diag/diagerrorcodes.h"
#include "output.h"			// for legacy crap only
#include "string/stringutil.h"
#include "system/bootmgr.h"
#include "system/criticalsection.h"
#include "system/exception.h"
#include "system/interlocked.h"
#include "system/ipc.h"
#include "system/membarrier.h"
#include "system/memops.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/stack.h"
#include "system/timer.h"
#include "system/tinyheap.h"
#include "system/tmcommands.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "file/device.h"
#include "file/remote.h"
#include "file/stream.h"
#include "file/tcpip.h"
#include "grcore/device.h"

#include <stdio.h>
#include <time.h>

// The toplevel wrappers all need a CHANNEL_MESSAGE_SIZE buffer.
// diagLogDefault needs a CHANNEL_MESSAGE_SIZE2 buffer.
// diagPrintf needs a CHANNEL_MESSAGE_SIZE buffer.
// on all platforms but PS3, diagPrintCallback needs another CHANNEL_MESSAGE_SIZE buffer.
// So on PS3, we need about CHANNEL_MESSAGE_SIZE*2 + CHANNEL_MESSAGE_SIZE2 bytes of stack space,
// and on Xenon we need CHANNEL_MESSAGE_SIZE*3 + CHANNEL_MESSAGE_SIZE2 bytes of stack space.
// (This is currently 1.75k on PS3 and 2.25k on Xenon)
// This doesn't count additional space consumed by any callbacks or system-level functions.
// Note also that the remote packet size for file writes (for file logging) is another 4k.
// So a thread's stack needs to have at least 8k of slop if it's going to do any tty output or assertions.

extern __THREAD int RAGE_LOG_DISABLE;

const char *diagAssertTitle;

#if __BANK
#include "bank/bank.h"
#endif

#if __WIN32PC
#include "email.h"
#endif

#if __PS3
#include <sys/tty.h>
#include <stdlib.h>
#elif __WIN32
#include "system/xtl.h"
#endif

#if __WIN32PC
#include "system/wndproc.h"
#include "diag/errorcodes.h"
#endif

#if RSG_ORBIS
#include <np.h>
#endif

PARAM(diagTerminateIgnoreDebugger, "Calls to diagTerminate will not trigger a __debugbreak.");
DEV_ONLY(PARAM(rfsRemoteTty, "Send TTY to systray as well"));
PARAM(bugAsserts, "Log bugs to bugstar");
PARAM(breakoncontext, "Break when a particular string appears in a context message");
#if REDIRECT_TTY
PARAM(redirectTTY, "Redirect TTY output to the specified TCP IP:port");
#endif // REDIRECT_TTY
PARAM(noassertfocus,"[logging] Do not make asserts steal the window focus");
PARAM(messageBoxTimeout, "[logging] Enable message box timeout that chooses default option once timer expires: supports =0 to =15");

#define DEFAULT_MESSAGEBOX_TIMEOUT 3
static int s_messageBoxTimeout = DEFAULT_MESSAGEBOX_TIMEOUT;  

NOSTRIP_FINAL_LOGGING_PARAM(ttyframeprefix, "Include a time and frame count on the TTY");

RAGE_DEFINE_CHANNEL(art)

namespace rage { bool g_IsStaticInitializationFinished; }

#if EXCEPTION_HANDLING
#	define SPIN_IF_HAS_EXCEPTION_BEEN_THROWN() while (sysException::HasBeenThrown()) sysIpcSleep(1000);
#else
#	define SPIN_IF_HAS_EXCEPTION_BEEN_THROWN() (void) 0
#endif


void diagTerminate()
{
	rage::fiStream::DumpOpenFiles();
#if !__NO_OUTPUT
	rage::diagChannel::CloseLogFile();
#endif

#if EXCEPTION_HANDLING
#if !__WIN32PC
	// don't exit if there has been an exception thrown, or we may truncate the output
	while (rage::sysException::HasBeenThrown())
	{
		rage::sysIpcSleep(1000);
	}
#endif
#endif

	// Make sure all output is flushed, including printfs. 
	fflush(0);

#if !__FINAL 
	if (!PARAM_diagTerminateIgnoreDebugger.Get() && rage::sysBootManager::IsDebuggerPresent())
#endif // !__FINAL
	{
		__debugbreak();
	}

#if RSG_PS3 || RSG_ORBIS
	_Exit(1);
#elif __XENON
	XLaunchNewImage( XLAUNCH_KEYWORD_DEFAULT_APP, 0 );
#elif RSG_PC
	if (rage::diagTerminateCallback)
	{
		rage::diagTerminateCallback(NULL);
	}

	// Throw an exception so the MS error reporting window comes up and we can get a crash dump
	*(int*)0 = 0;
	ExitProcess(1);
#endif
}

void diagTerminate(rage::FatalErrorCode WIN32PC_ONLY(errorCode), bool WIN32PC_ONLY(silent))
{
#if RSG_PC
	if (rage::diagErrorCodes::IsErrorCodeValid(errorCode) || !rage::diagErrorCodes::IsErrorCodesLoaded())
	{
		const size_t TITLE_STRING_LENGTH = 256;
		const size_t ERROR_STRING_LENGTH = 512;
		wchar_t wTitleString[TITLE_STRING_LENGTH] = {0};
		wchar_t wErrorString[ERROR_STRING_LENGTH] = {0};

		wcsncpy_s(wTitleString, _countof(wTitleString), rage::diagErrorCodes::GetErrorCodeString(errorCode), _TRUNCATE);

		wchar_t* w_errorString = rage::diagErrorCodes::GetErrorMessageString(errorCode);

		if (w_errorString)
		{
			wcsncpy_s(wErrorString, _countof(wErrorString), w_errorString, _TRUNCATE);

			int returnCodeNumber = rage::diagErrorCodes::GetExtraReturnCodeNumber();
			if (returnCodeNumber != 0)
			{
				const size_t EXTRA_RETURN_CODE_STRING_LENGTH = 64;
				wchar_t wExtraReturnCodeString[EXTRA_RETURN_CODE_STRING_LENGTH] = {0};
				int radix = 10;

				if (rage::diagErrorCodes::ShowExtraReturnCodeNumberInHex())
				{
					radix = 16;
					wcsncat_s(wErrorString, _countof(wErrorString), L" 0x", _TRUNCATE);
				}
				else
				{
					wcsncat_s(wErrorString, _countof(wErrorString), L" ", _TRUNCATE);
				}

				_itow_s(returnCodeNumber, wExtraReturnCodeString, _countof(wExtraReturnCodeString), radix );
				_wcsupr(wExtraReturnCodeString);
				wcsncat_s(wErrorString, _countof(wErrorString), wExtraReturnCodeString, _TRUNCATE);
			}
		}


#if BACKTRACE_ENABLED
		USES_CONVERSION;
		const char* uErrorString = WIDE_TO_UTF8((const rage::char16*)wErrorString);
		const char* uTitleString = WIDE_TO_UTF8((const rage::char16*)wTitleString);
		char errorCodeHex[11];
		rage::formatf(errorCodeHex, "0x%08X", errorCode);
		rage::sysException::SetAnnotation("error_code", errorCodeHex);
		rage::sysException::SetAnnotation("error_string", uErrorString);
		rage::sysException::SetAnnotation("error_title", uTitleString);
#endif

		// If we have a handle to a window at this point, parent the message box to it.
		//if (rage::g_hwndMain != NULL)
		//{
		//	// Minimize the window so you can't see the game and the message box is front and center.
		//	ShowWindow(rage::g_hwndMain, SW_MINIMIZE);
		//}

		MessageBoxW(NULL, wErrorString, wTitleString, MB_OK | MB_ICONERROR | MB_SETFOREGROUND | MB_TOPMOST);


		if (rage::diagTerminateCallback)
		{
			char mb_errorMessage[2048];
			wcstombs(mb_errorMessage, wErrorString, 2048);
			mb_errorMessage[2047] = '\0'; 
			rage::diagTerminateCallback(mb_errorMessage);
		}

		if (silent)
		{
			// Terminate process silently. Do not trigger a crash dump.
			rage::fiStream::DumpOpenFiles();
			TerminateProcess(GetCurrentProcess(), 1);
		}
	}
#endif // RSG_PC
	diagTerminate();
}


namespace rage
{

#if RSG_PC
	static void diagTerminateCallbackNone(const char*) { }
	void (*diagTerminateCallback)(const char* error) = diagTerminateCallbackNone;
#endif // RSG_PC

} // namespace rage


#if !__NO_OUTPUT

namespace rage {

PARAM(Channel_all,"[ Output Channels ] Set the minimum file log, tty, and popups level for 'Channel' and any subchannels (fatal, error, warning, display, debug1, debug2, debug3)");
PARAM(Channel_log,"[ Output Channels ] Set the minimum file log level for 'Channel' and any subchannels (fatal, error, warning, display, debug1, debug2, debug3)");
PARAM(Channel_tty,"[ Output Channels ] Set the minimum tty level for 'Channel' and any subchannels (fatal, error, warning, display, debug1, debug2, debug3)");
PARAM(Channel_popups,"[ Output Channels ] Set the minimum popups level for 'Channel' and any subchannels (fatal, error, warning, display, debug1, debug2, debug3)");
PARAM(Channel_rag,"[ Output Channels ] Create a rag output pane for this 'Channel'");
diagChannel diagChannel::ALL(NULL,"[ALL] ","ALL");

PARAM(usevsoutput,"[diag] Use Visual-Studio style output (with file/line numbers)");
PARAM(noquitonassert,"[diag] Don't allow asserts to quit the program");
PARAM(logtimestamps,"[diag] Output the timestamp with each line when logging to a file");

#if __WIN32PC
PARAM(emailasserts,     "[diag] Email address to send all asserts to");
PARAM(emailassertsfrom, "[diag] Email address to send all asserts from");
#endif

#if __BANK
bkBank* diagChannel::sm_pBank = NULL;
#endif //__BANK

bool diagChannel::sm_UseThreadPrefix = true;
bool diagChannel::sm_startNewFile = false;

diagChannel* diagChannel::First;

int diagChannel::sm_systemAssertDate = 0;

diagChannel::diagChannel(diagChannel *parent,const char *tag,const char *paramTag,
						 diagSeverity fileLevel,diagSeverity ttyLevel,diagSeverity popupLevel,diagChannelPosix posixPrefix) 
						 : Parent(parent), Tag(tag), ParamTag(paramTag), Next(First), PosixPrefix(posixPrefix)
{
	if (parent)
		ParentCount = parent->ParentCount + 1;
	else
		ParentCount = 0;
	Assign(FileLevel,fileLevel);
	Assign(TtyLevel,ttyLevel);
	Assign(PopupLevel,popupLevel);
	UpdateMaxLevel();
	Assign(MaxLevel,fileLevel);		// For now...
	First = this;
	Assign(OriginalFileLevel, 0xFF);
	Assign(OriginalTtyLevel, 0xFF);

	// inherit parent channel posix specification
	if (PosixPrefix == CHANNEL_POSIX_UNSPECIFIED)
	{
		if(parent)
			PosixPrefix = parent->PosixPrefix;
		else
			PosixPrefix = CHANNEL_POSIX_OFF;
	}

	// and apply our posix setting to all children with an unspecified setting
	for (diagChannel *i=First; i; i=i->Next)
	{
		if (i->Parent == this)
		{
			if(i->PosixPrefix == CHANNEL_POSIX_UNSPECIFIED)
			{
				i->PosixPrefix = PosixPrefix;
			}
		}
	}
}

void diagChannel::UpdateMaxLevel()
{
	// Figure out the highest (most verbose) level we need to support.
	// This allows us to suppress a call that will never do anything before even entering diagLogf.
	MaxLevel = FileLevel;
	if (MaxLevel < TtyLevel)
		MaxLevel = TtyLevel;
	if (MaxLevel < PopupLevel)
		MaxLevel = PopupLevel;
}

const int maxIgnore = 128;
static u64 s_ignoredCodes[maxIgnore];
static int s_ignoredCodeCount;

#if __ASSERT
void diagChannel::ClearIgnoredAsserts()
{
	s_ignoredCodeCount = 0;
	memset(s_ignoredCodes, sizeof(s_ignoredCodes), 0);

	Warningf("CLEARED IGNORED ASSERTS");
}
#endif // ASSERT

void diagChannel::overrideSeverity(diagSeverity fileOverrideLevel, diagSeverity ttyOverrideLevel)
{
	Assertf(OriginalFileLevel == 0xFF, "calling overrideSeverity without having resetSeverity");
	Assertf(OriginalTtyLevel == 0xFF, "calling overrideSeverity without having resetSeverity");
	Assign(OriginalFileLevel,FileLevel);
	Assign(FileLevel, fileOverrideLevel);
	Assign(OriginalTtyLevel,TtyLevel);
	Assign(TtyLevel, ttyOverrideLevel);
	UpdateMaxLevel();
}

void diagChannel::resetSeverity()
{
	Assertf(OriginalFileLevel != 0xFF, "calling resetSeverity without having overrideSeverity");
	Assertf(OriginalTtyLevel != 0xFF, "calling resetSeverity without having overrideSeverity");
	Assign(FileLevel, OriginalFileLevel);
	Assign(TtyLevel, OriginalTtyLevel);
	Assign(OriginalFileLevel, 0xFF);
	Assign(OriginalTtyLevel, 0xFF);
	UpdateMaxLevel();
}

#if __BANK
void diagChannel::WidgetCallback(void *channel)
{
	((diagChannel*)channel)->UpdateMaxLevel();
}

static void WidgetCreateOutputWindowCallback(void *channel)
{
	BANKMGR.CreateOutputWindow(((diagChannel*)channel)->ParamTag, "NamedColor:Black");
}

void diagChannel::AddWidgets(bkBank &bank) {
	if (!*ParamTag)
		return;
	static const char *items[] = { "Fatal", "Asserts", "Errors", "Warnings", "Display", "Debug1", "Debug2", "Debug3" };
	int numKids = 0;
	for (diagChannel *i=First; i; i=i->Next)
		if (i->Parent == this)
			++numKids;
	if (numKids) {
		char buf[64];
		bank.PushGroup(formatf(buf,"%s (%d subchannel%s)",ParamTag,numKids,numKids>1?"s":""),false);
	}
	else
		bank.PushGroup(ParamTag,false);
	bank.AddCombo("Tty spew",&TtyLevel,8,items,datCallback(diagChannel::WidgetCallback,this));
	bank.AddCombo("Popups",&PopupLevel,8,items,datCallback(diagChannel::WidgetCallback,this));
	bank.AddCombo("File logging",&FileLevel,8,items,datCallback(diagChannel::WidgetCallback,this));
	bank.AddButton("Create output window",datCallback(WidgetCreateOutputWindowCallback,this));
	// Add immediate descendants to ourselves.
	for (diagChannel *i=First; i; i=i->Next) {
		if (i->Parent == this)
			i->AddWidgets(bank);
	}
	bank.PopGroup();
}

void diagChannel::InitWidgets(bkBank &bank) {
	bank.AddButton("Start new log file", &diagChannel::StartNewLogFile);
	for (diagChannel *i = First; i; i = i->Next)
		if (i->Parent == &diagChannel::ALL)
			i->AddWidgets(bank);
}

void diagChannel::InitOutputWindows() 
{
	int argc = sysParam::GetArgCount();
	char **argv = sysParam::GetArgArray();
	for (int i=1; i<argc; i++) 
	{
		const char *arg = argv[i];
		if (arg[0]!='-')	// Note that sysParam::Init already fixes up em dashes and other such things
			continue;
		else
			++arg;
		const char *match = strstr(arg,"_rag");
		if (match)
		{
			for (diagChannel *j = First; j; j = j->Next)
			{
				// Make sure we match the entire parameter tag
				if (!strnicmp(j->ParamTag,arg,match - arg) && strlen(j->ParamTag) == (size_t)(match - arg))
				{
					BANKMGR.CreateOutputWindow(j->ParamTag,"NamedColor:Black");
					break;
				}
			}
		}
	}
}

#if __ASSERT
int diagChannel::DetectBlockedPopups(bool detectAll)
{
	if (detectAll)
	{
		int blocked = 0;

		for (diagChannel* i = First; i; i = i->Next)
		{
			if (i->PopupLevel < DIAG_SEVERITY_ASSERT)
			{
				++blocked;
			}
		}

		return blocked;
	}

	int channelsPerUpdate = 20;
	static int totalChannelsVisited = 0;
	static diagChannel *i = NULL;
	static int noPopupsFound = 0;
	static int noPopupsBuffered = 0;
	int channelsVisited = 0;

	if(i == NULL)
	{
		i = First;
		totalChannelsVisited = 0;

		//show the buffered result from the past complete run
		noPopupsBuffered = noPopupsFound;
		noPopupsFound = 0;
	}

	for (; i && channelsVisited < channelsPerUpdate; i = i->Next)
	{
		++channelsVisited;
		++totalChannelsVisited;
		//Displayf("DetectNoPopups level = %d Found = %d Buffer = %d ASSERT = %d visit:%d total:%d tag:%s paramtag:%s ", i->PopupLevel, noPopupsFound, noPopupsBuffered, DIAG_SEVERITY_ASSERT, channelsVisited, totalChannelsVisited, i->Tag, i->ParamTag);
		if (i->PopupLevel < DIAG_SEVERITY_ASSERT)
		{
			++noPopupsFound;
		}
	}

	return noPopupsBuffered;
}
#endif

void diagChannel::SendAllSubChannels()
{
	for (diagChannel *i = First; i; i = i->Next)
	{
		if (i->Parent == &diagChannel::ALL)
		{
			i->SendSubChannels();
		}
	}
}

void diagChannel::SendSubChannels()
{
	for (diagChannel *i=First; i; i=i->Next)
	{
		if (i->Parent == this)
		{
			BANKMGR.SendSubChannelInfo(ParamTag, i->ParamTag);
			i->SendSubChannels();
		}
	}
}
#endif

static int string_to_spew_level(const char *p) 
{
	if (!strcasecmp(p,"fatal"))
		return DIAG_SEVERITY_FATAL;
	else if (!strcasecmp(p,"assert") || !strcasecmp(p, "asserts"))
		return DIAG_SEVERITY_ASSERT;
	else if (!strcasecmp(p,"error") || !strcasecmp(p, "errors"))
		return DIAG_SEVERITY_ERROR;
	else if (!strcasecmp(p,"warning") || !strcasecmp(p, "warnings"))
		return DIAG_SEVERITY_WARNING;
	else if (!strcasecmp(p,"display"))
		return DIAG_SEVERITY_DISPLAY;
	else if (!strcasecmp(p,"debug1"))
		return DIAG_SEVERITY_DEBUG1;
	else if (!strcasecmp(p,"debug2"))
		return DIAG_SEVERITY_DEBUG2;
	else if (!strcasecmp(p,"debug3"))
		return DIAG_SEVERITY_DEBUG3;
	else {
		Warningf("Unknown spew level string '%s', defaulting to 'display'",p);
		return DIAG_SEVERITY_DISPLAY;
	}
}

bool diagChannel::HasParent(diagChannel *parent) const
{
	return Parent? (Parent==parent || Parent->HasParent(parent)) : false;
}

#if REDIRECT_TTY
// 16-bit commands, all other values are treated as string length.
enum
{
	CMD_WAIT            = 0,
	// string length values [1..CMD_MAX_STRING_LEN]
	CMD_MAX_STRING_LEN  = 0xfffd,
	CMD_FLUSH           = 0xfffe,
	CMD_BUF_WRAP        = 0xffff,
};
ALIGNAS(2) static char s_TtyBuffer[1024 << 10];     // align(2) for the u16 sizes embedded at even offsets
static volatile u64 s_TtyBufferPut = 0;             // [0..31] index, [32..63] sequence counter
static volatile u64 s_TtyBufferGet = 0;             // [0..31] index, [32..63] sequence counter
static sysIpcSema s_TtyBufferSema;
static char s_TtyBufferIpaddr[16];
static int s_TtyBufferPort;
static sysIpcCurrentThreadId s_TtyBufferThreadId = sysIpcCurrentThreadIdInvalid;
static bool s_TtyBufferEnable = false;
bool s_allowOutputDebugString = true;
static __THREAD bool s_DontDoDiagPrintCallback = false;

static void NativeOutputString(const char *str)
{
	#if RSG_PC || RSG_DURANGO
		OutputDebugString(str);
	#elif RSG_ORBIS
		printf("%s", str);
	#else
		#error Unhandled platform
	#endif
}

template<class VOIDPTR>
static void TtySendReceive(int (*func)(fiHandle,VOIDPTR,int), fiHandle *sock, VOIDPTR data, size_t size)
{
	const u32 MIN_SPAM_TIME_MS = 10000;
	u32 lastConnectMsgTimeMs = sysTimer::GetSystemMsTime()-MIN_SPAM_TIME_MS;
	u32 lastDisconnectMsgTimeMs = lastConnectMsgTimeMs;

	char *p = (char*)data;
	while (size)
	{
		if (!fiIsValidHandle(*sock))
		{
			for (;;)
			{
				u32 t = sysTimer::GetSystemMsTime();
				bool spam = false;
				if (t-lastConnectMsgTimeMs >= MIN_SPAM_TIME_MS)
				{
					NativeOutputString("Connecting to remote TTY host.\n");
					lastConnectMsgTimeMs = t;
					spam = true;
				}
				*sock = fiDeviceTcpIp::Connect(s_TtyBufferIpaddr, s_TtyBufferPort);
				if (fiIsValidHandle(*sock))
				{
					break;
				}
				if (spam)
				{
					NativeOutputString("Remote TTY connection failed.\n");
				}
				sysIpcSleep(100);
			}
		}
		int result = func(*sock, p, (int)size);
		if (result < 0)
		{
			u32 t = sysTimer::GetSystemMsTime();
			if (t-lastDisconnectMsgTimeMs >= MIN_SPAM_TIME_MS)
			{
				NativeOutputString("Remote TTY error, restarting connection.\n");
				lastDisconnectMsgTimeMs = t;
			}
			fiDeviceTcpIp::GetInstance().Close(*sock);
			*sock = fiHandleInvalid;
		}
		else
		{
			p += result;
			Assert(result <= (int)size);
			size -= result;
		}
	}
}

static void TtySend(fiHandle *sock, const void *data, size_t size)
{
	TtySendReceive(fiDeviceTcpIp::StaticWrite, sock, data, size);
}

static void TtyReceive(fiHandle *sock, void *data, size_t size)
{
	TtySendReceive(fiDeviceTcpIp::StaticRead, sock, data, size);
}

static void TtyBufferThread(void*)
{
	s_TtyBufferThreadId = sysIpcGetCurrentThreadId();

	fiHandle sock = fiHandleInvalid;

#	if __ASSERT
		// Coverity complains about an assert having a side effect if reading a
		// volatile variable within an assert, so explicitly load it outside the
		// assert first.
		const u64 ttyBufferGetForAssert = s_TtyBufferGet;
		FatalAssert(ttyBufferGetForAssert == 0);
#	endif
	u64 g = 0;
	unsigned semWaitCount = 1;
	for (;;)
	{
		// While unlikely, it is possible for semWaitCount to be zero here when
		// the thread wakes up using semWaitCount==1 and the ring buffer
		// immediately wraps.
		if (semWaitCount)
		{
			sysIpcWaitSema(s_TtyBufferSema, semWaitCount);
			semWaitCount = 0;
		}

		u64 gidx = g & 0x00000000ffffffffull;
		u64 pidx;
#		if __ASSERT
			const u64 ttyBufferPutForAssert = s_TtyBufferPut;
			FatalAssert((ttyBufferPutForAssert&0x00000000ffffffffull) != gidx);
#		endif

		u64 txBegin = gidx;
		u64 txEnd = gidx;
		bool flush = false;

		// Determine how much data can be sent in a single packet.
		do
		{
			// Spin until the length is filled in.  Length is set to CMD_WAIT(0)
			// until the TTY string has been copied into the ring buffer.
			char *const ptr = s_TtyBuffer + gidx;
			size_t len;
			for (;;)
			{
				len = *(u16*)ptr;
				if (len != CMD_WAIT)
				{
					break;
				}
				sysIpcSleep(1); // NB: Don't sleep(0) or yield()!
			}

			// Read barrier to ensure the non-zero length is read before the
			// string is read.
			sysMemReadBarrier();

			// Special case commands.
			if (len > CMD_MAX_STRING_LEN)
			{
				if (len == CMD_FLUSH)
				{
					txEnd = gidx;
					*(u16*)ptr = CMD_WAIT;
					FatalAssert(gidx+2 <= NELEM(s_TtyBuffer));
					g += 0x0000000100000002ull;
					gidx += 2;
					if (gidx >= NELEM(s_TtyBuffer))
					{
						FatalAssert(gidx == NELEM(s_TtyBuffer));
						g &= 0xffffffff00000000ull;
						gidx = 0;
					}
					flush = true;
					++semWaitCount;
					break;
				}
				else if (len == CMD_BUF_WRAP)
				{
					txEnd = gidx;
					*(u16*)ptr = CMD_WAIT;
					g = (g + 0x0000000100000000ull) & 0xffffffff00000000ull;
					gidx = 0;
					break;
				}
			}

			else
			{
				++semWaitCount;

				FatalAssert(gidx+len <= NELEM(s_TtyBuffer));
				g += 0x0000000100000000ull + len;
				gidx += len;
				txEnd = gidx;
				if (gidx >= NELEM(s_TtyBuffer))
				{
					FatalAssert(gidx == NELEM(s_TtyBuffer));
					g &= 0xffffffff00000000ull;
					gidx = 0;
					break;
				}
			}

			pidx = s_TtyBufferPut & 0x00000000ffffffffull;
		}
		while (gidx != pidx);

		const u64 memsetBegin = txBegin;
		const u64 memsetEnd   = txEnd;

		TtySend(&sock, s_TtyBuffer+((size_t)(txBegin)), (size_t)(txEnd-txBegin));

		if (flush)
		{
			flush = false;

			char txPacket[6];
			const u16 hdr = CMD_FLUSH;
			const u32 seq = (u32)(g>>32);
			sysMemCpy(txPacket,   &hdr, 2);
			sysMemCpy(txPacket+2, &seq, 4);

			char rxPacket[6];

			do
			{
				TtySend(&sock, txPacket, sizeof(txPacket));
				TtyReceive(&sock, rxPacket, sizeof(rxPacket));
			}
			while (memcmp(txPacket, rxPacket, sizeof(txPacket)) != 0);
			CompileTimeAssert(sizeof(txPacket) == sizeof(rxPacket));
		}

		CompileTimeAssert(CMD_WAIT == 0);
		sysMemSet(s_TtyBuffer+((size_t)(memsetBegin)), 0, (size_t)(memsetEnd-memsetBegin));
		sysMemWriteBarrier();
		s_TtyBufferGet = g;
	}
}

static void *TtyBufferAllocate(size_t size)
{
	FatalAssert(size <= CMD_MAX_STRING_LEN);
	FatalAssert((size&1) == 0);
	FatalAssert(size <= NELEM(s_TtyBuffer));
	u64 p = s_TtyBufferPut;
	for (;;)
	{
		// Determine where in the buffer we want to attempt to allocate space
		// for copying the string.
		u64 p2 = p + 0x0000000100000000ull + size;  // value to try CAS with s_TtyBufferPut
		char *alloc = NULL;                         // address to copy string
		FatalAssert(((uptr)p&1) == 0);
		u64 g = s_TtyBufferGet;
		u64 pidx  = p  & 0x00000000ffffffffull;
		u64 gidx  = g  & 0x00000000ffffffffull;
		u64 p2idx = p2 & 0x00000000ffffffffull;
		// Buffer not empty, and get not wrapped around after put ?
		if (pidx < gidx)
		{
			// Is there space before the get ?
			if (p2idx < gidx)
			{
				alloc = s_TtyBuffer + pidx;
			}
		}
		// Buffer empty, or wrapped ?
		else
		{
			// Space before reaching end of buffer ?
			if (p2idx < NELEM(s_TtyBuffer))
			{
				alloc = s_TtyBuffer + pidx;
			}
			// Space filling up to exactly the end of the buffer ?
			else if (p2idx == NELEM(s_TtyBuffer))
			{
				// Wrap put around, provided that won't then empty the buffer.
				if (gidx != 0)
				{
					p2 &= 0xffffffff00000000ull;
					alloc = s_TtyBuffer + pidx;
				}
			}
			// No space, need to wrap ?
			else
			{
				// Space at the start ?
				if (size < gidx)
				{
					p2 = (p2 & 0xffffffff00000000ull) | size;
					alloc = s_TtyBuffer;
				}
			}
		}

		// If there was room in the buffer, try to do the allocation
		if (alloc)
		{
			u64 p3 = sysInterlockedCompareExchange(&s_TtyBufferPut, p2, p);
			if (p != p3)
			{
				// CAS failed, no need to sleep, just try again
				p = p3;
				continue;
			}

			// If the allocation wrapped, then add marker for that
			if (alloc != s_TtyBuffer+pidx)
			{
				FatalAssert(alloc == s_TtyBuffer);
				FatalAssert(*(u16*)(s_TtyBuffer+pidx) == CMD_WAIT);
				*(u16*)(s_TtyBuffer+pidx) = CMD_BUF_WRAP;
			}

			FatalAssert(*(u16*)alloc == CMD_WAIT);
			return alloc;
		}

		// Buffer is currently full, special case handling for calling from the
		// worker thread to prevent a deadlock.
		else if (sysIpcGetCurrentThreadId() == s_TtyBufferThreadId)
		{
			return NULL;
		}

		// Buffer full on any other thread
		else
		{
			sysIpcSleep(1); // NB: Don't sleep(0) or yield()!
			p = s_TtyBufferPut;
		}
	}
}

// OutputDebugString is very slow on Durango when KD is connected (which is used
// by R*TM).  So instead, place the string into a buffer and use a worker thread
// for the OutputDebugString call.  This unfortunately means that the ordering
// with OutputDebugString calls from libraries or the OS will not be correct.
// Have asked Microsoft at
// https://forums.xboxlive.com/AnswerPage.aspx?qid=073358a8-ac77-4a61-8ea9-bdbac63929ee&tgt=1
// if they have any better solutions.
static void BufferedOutputDebugString(const char *str, size_t len)
{
	const size_t size = sizeof(u16) + (len|1) + sizeof(char);
	char *const alloc = (char*)TtyBufferAllocate(size);
	if (alloc)
	{
		sysMemCpy(alloc+sizeof(u16), str, len+1);
		// _After_ the string has been copied, then update the string size.
		sysMemWriteBarrier();
		*(u16*)alloc = (u16)size;
		sysIpcSignalSema(s_TtyBufferSema);
	}
	else
	{
		Assert(sysIpcGetCurrentThreadId() == s_TtyBufferThreadId);
		NativeOutputString(str);
	}
}

static void BufferedOutputFlush()
{
	void *const alloc = TtyBufferAllocate(2);
	if (alloc)
	{
		*(u16*)alloc = (u16)CMD_FLUSH;
		sysIpcSignalSema(s_TtyBufferSema);
	}
	else
	{
		Assert(sysIpcGetCurrentThreadId() == s_TtyBufferThreadId);
		// Not really much we can do in this case.
	}
}

bool diagChannel::RedirectOutput(const char *ipaddrPort, u32 timeoutMs)
{
	Assert(!s_TtyBufferEnable);

	s_TtyBufferSema = sysIpcCreateSema(0);

	unsigned i;
	for (i=0; i<NELEM(s_TtyBufferIpaddr)-1; ++i)
	{
		char c = ipaddrPort[i];
		if (c == ':')
		{
			break;
		}
		Assert(c != '\0');
		s_TtyBufferIpaddr[i] = c;
	}
	s_TtyBufferIpaddr[i] = '\0';
	++i;
	s_TtyBufferPort = atoi(ipaddrPort+i);

	if (sysIpcCreateThread(TtyBufferThread, NULL, 0x1000, PRIO_ABOVE_NORMAL, "TtyBufferThread") == sysIpcThreadIdInvalid)
	{
		Quitf("Failed to create TtyBufferThread");
	}

	Displayf("Redirecting TTY to %s\n", ipaddrPort);

	s_TtyBufferEnable = true;

	const bool redirected = FlushLogFile(timeoutMs);
	s_TtyBufferEnable = redirected;

	Displayf(redirected ? "... Succeeded\n" : "... Failed\n");

	return redirected;
}

#endif // REDIRECT_TTY

void diagChannel::InitClass()
{
	InitSystemDateAssert();
	InitArgs();
}

void diagChannel::InitArgs() {
	// In order to speed this function up (since it's now called when new script channels are added) the
	// algorithm is simpler... parameters are processed in the order encountered, and any modification to
	// a channel will immediately update all of its child channels.
	int argc = sysParam::GetArgCount();
	char **argv = sysParam::GetArgArray();
	// Dummy test to keep these from dead stripping so they show up in -help
	if (PARAM_Channel_all.Get() && PARAM_Channel_log.Get() && PARAM_Channel_tty.Get() && PARAM_Channel_popups.Get() && PARAM_Channel_rag.Get())
		PARAM_Channel_all.Set(0);
	for (int i=1; i<argc; i++) 
	{
		const char *arg = argv[i];
		if (arg[0]!='-')	// Note that sysParam::Init already fixes up em dashes and other such things
			continue;
		else
			++arg;
		const char *match = NULL;
		int mask = 0;
		if ((match = stristr(arg,"_log=")) != NULL)
			mask = 1;
		else if ((match = stristr(arg,"_tty=")) != NULL)
			mask = 2;
		else if ((match = stristr(arg,"_popup=")) != NULL || (match=stristr(arg,"_popups=")) != NULL)
			mask = 4;
		else if ((match = stristr(arg,"_all=")) != NULL)
			mask = 7;
		else
			continue;
		for (diagChannel *j = First; j; j = j->Next)
		{
			// Make sure we match the entire parameter tag
			if (!strnicmp(j->ParamTag,arg,match - arg) && strlen(j->ParamTag) == (size_t)(match - arg))
			{
				int thisLevel = string_to_spew_level(strchr(match,'=')+1);
				if (mask & 1) Assign(j->FileLevel,thisLevel);
				if (mask & 2) Assign(j->TtyLevel,thisLevel);
				if ((mask & 4) && (thisLevel <= DIAG_SEVERITY_ERROR)) Assign(j->PopupLevel,thisLevel);
				j->UpdateMaxLevel();
				for (diagChannel *k = First; k; k = k->Next)
				{
					if (k->HasParent(j))
					{
						if (mask & 1) Assign(k->FileLevel,thisLevel);
						if (mask & 2) Assign(k->TtyLevel,thisLevel);
						if ((mask & 4) && (thisLevel <= DIAG_SEVERITY_ERROR)) Assign(k->PopupLevel,thisLevel);
						k->UpdateMaxLevel();
					}
				}
			}
		}
	}
}

//
// Used in RAGE_ASSERT_BEFORE definitions. Checks if the system date is before the specified date. If it's the same date or after
// the assertion fails. Uses UTC/GMT time.
//
void diagChannel::InitSystemDateAssert()
{
	time_t rawtime;

	time( &rawtime );

	tm* pTime = gmtime( &rawtime );

	sm_systemAssertDate = RAGE_ASSERT_BEFORE_DATE( pTime->tm_year + 1900, pTime->tm_mon + 1, pTime->tm_mday );
}

PARAM(nopopups,"[diag] Disable all use of popups (optional: add =abort, =retry, or =ignore, default is ignore)");
PARAM(noquits,"[diag] Disable any attempt to quit (dangerous!)");
PARAM(notopmostasserts,"[diag] Do not make asserts top-most.");

static const char s_Colors[DIAG_SEVERITY_COUNT][6] =
{
	TRed,
	TRed,
	TRed,
	TYellow,
	"",
	TBlue,
	TBlue,
	TBlue
};

static const char *s_Prefixes[DIAG_SEVERITY_COUNT] = 
{
	"Fatal Error: ",
	"Error: ",
	"Error: ",
	"Warning: ",
	"",
	"Debug1: ",
	"Debug2: ",
	"Debug3: "
};

// MSBuild wants to see spaces before the colons.
static const char *s_VsPrefixes[DIAG_SEVERITY_COUNT] = 
{
	"Fatal Error : ",
	"Error : ",
	"Error : ",
	"Warning : ",
	"",
	"",
	"",
	""
};

static sysCriticalSectionToken logToken;
sysCriticalSectionToken diagLogfToken;
static sysIpcThreadId loggerThreadId = sysIpcThreadIdInvalid;
static sysIpcSema loggerThreadSemaphore = NULL;
static volatile int loggerThreadRefCount;
static fiStream *logStream;
static bool s_output = true;
static char s_logfileName[RAGE_MAX_PATH];
static u64 s_logfileSize;
static int s_logfileNumber;
static const u64 LOG_FILE_SIZE_INCREMENT =  1024 * 1024;
static u64 s_maxLogFileSize = 300 * LOG_FILE_SIZE_INCREMENT; // keep default as it was
static sysIpcCurrentThreadId s_loggerThreadId;

// Fixed array of pointers to void (void) functions. If you want to add another callback, increase the size of the array.
static atFixedArray<void(*)(), 1> s_newFileCallbacks;
static sysCriticalSectionToken callbackArrayToken;

bool diagChannel::AddNewFileCallback(void (*newFileCallback)())
{
	SYS_CS_SYNC(callbackArrayToken);

	// Do not add a callback to the array if it's already in there.
	if (s_newFileCallbacks.Find(newFileCallback) == -1)
	{
		if (s_newFileCallbacks.GetCount() < s_newFileCallbacks.GetMaxCount())
		{
			s_newFileCallbacks.Push(newFileCallback);
			return true;
		}

		Displayf("%s :: Array of on-new-file callbacks is full (capacity: %u). Increase the capacity.", 
			__FUNCTION__,
			s_newFileCallbacks.GetMaxCount());
	}

	return false;
}

static void CallNewFileCallbacks()
{
	SYS_CS_SYNC(callbackArrayToken);

	for (int i = 0; i < s_newFileCallbacks.GetCount(); ++i)
	{
		Assert(s_newFileCallbacks[i] != NULL);

		s_newFileCallbacks[i]();
	}
}

void diagChannel::LoggerThread(void*)
{
	// This thread
	s_loggerThreadId = sysIpcGetCurrentThreadId();
	while (loggerThreadRefCount)
	{
		// This normally times out; only the file close code ever signals this, specifically to speed things up.
		sysIpcWaitSemaTimed(loggerThreadSemaphore, 1000); 

		// If we can't get the lock right now, don't worry about it, just flush later.
		// Avoids a race condition with diagCloseLogFile owning the critsec already.
		if (logToken.TryLock())
		{
			if(logStream)
			{
				logStream->Flush();
				s_logfileSize = logStream->Size64();
				// If the log file has gone over a certain size, start a new file.
				if (s_logfileNumber != -1 && (s_logfileSize >= s_maxLogFileSize || sm_startNewFile == true))
				{
					sm_startNewFile = false;
					// If the log file doesn't have an extension, we cannot break it up.
					char *ext = strrchr(s_logfileName,'.');
					if (ext)
					{
						// If we already added _### before, truncate to that
						if (s_logfileNumber)
							*strrchr(s_logfileName,'_') = 0;
						else	// Otherwise truncate to the extension
							*ext = 0;

						++s_logfileNumber;
						char newName[RAGE_MAX_PATH];
						formatf(newName,"%s_%d.%s",s_logfileName,s_logfileNumber,ext+1);
						safecpy(s_logfileName, newName);
						logStream->Close();
						logStream = fiStream::Create(s_logfileName);
						s_logfileSize = 0;

						CallNewFileCallbacks();
					}
					else
						s_logfileNumber = -1;
				}
			}
			logToken.Unlock();
		}
	}
}


bool diagChannel::FlushLogFile(u32 timeoutMs)
{
	SYS_CS_SYNC(logToken);
	if (logStream)
		logStream->Flush();

#if REDIRECT_TTY
	if (s_TtyBufferEnable)
	{
		BufferedOutputFlush();

		const bool infinite = (timeoutMs == 0);
		do
		{
			if (((s_TtyBufferPut^s_TtyBufferGet)&0xffffffff) == 0)
				return true;
			sysIpcSleep(1);
		}
		while (--timeoutMs || infinite);
		return false;
	}
#endif // REDIRECT_TTY

	return true;
}

void diagChannel::StartNewLogFile()
{
	sm_startNewFile = true;
}

bool diagChannel::OpenLogFile(const char *name)
{
	sysCriticalSection diagSync(diagLogfToken);
	SYS_CS_SYNC(logToken);
	if (logStream)
		logStream->Write("OPENING NEW LOG FILE, SO ",25);
	CloseLogFile();

	logStream = fiStream::Create(name);
	s_logfileSize = 0;
	Assertf(logStream,"OpenLogFile(%s) failed?",name);

	safecpy(s_logfileName, name);

#if RSG_DURANGO || RSG_ORBIS
	int cpuAffinity = 5;
#else
	int cpuAffinity = 0;
#endif

	if (logStream)
	{
		if (loggerThreadRefCount++ == 0)
		{
			loggerThreadSemaphore = sysIpcCreateSema(0);
			loggerThreadId = sysIpcCreateThread(LoggerThread,NULL,8192,PRIO_ABOVE_NORMAL,"[RAGE] Logfile", cpuAffinity, "RageLogfile");
		}
	}

	return logStream != NULL;
}

bool diagChannel::TryLockDiagLogfToken()
{
	return diagLogfToken.TryLock();
}

void diagChannel::UnlockDiagLogfToken()
{
	diagLogfToken.Unlock();
}

void diagChannel::CloseLogFile()
{
	sysCriticalSection diagSync(diagLogfToken);
	SYS_CS_SYNC(logToken);
	if (logStream)
	{
		logStream->Write("CLOSING LOG FILE BY USER REQUEST\r\n",34);	// hope I counted that right.
		logStream->Close();
		logStream = NULL;

		if (!--loggerThreadRefCount)
		{
			if ( loggerThreadId != sysIpcThreadIdInvalid )
			{
				sysIpcSignalSema(loggerThreadSemaphore);
				sysIpcWaitThreadExit(loggerThreadId);
				loggerThreadId = sysIpcThreadIdInvalid;
				sysIpcDeleteSema(loggerThreadSemaphore);
				loggerThreadSemaphore = NULL;
			}
		}
	}
	s_logfileName[0] = '\0';
}

int diagChannel::GetLogFileNumber()
{
	return s_logfileNumber;
}

const char* diagChannel::GetLogFileName()
{
	return s_logfileName;
}

int diagChannel::GetLogFileSize()
{
	SYS_CS_SYNC(logToken);
	if (logStream)
	{
		return logStream->Size();
	}
	return 0;
}

void diagChannel::SetLogFileMaxSize(u32 megabytes)
{
	// Ensure that each logfile's maximum size is no less than LOG_FILE_SIZE_INCREMENT(=1MB) in size.
	s_maxLogFileSize = megabytes > 0U ? megabytes * LOG_FILE_SIZE_INCREMENT : LOG_FILE_SIZE_INCREMENT;
}

void diagChannel::SetOutput(bool enable)
{
	if(s_output != enable)
	{
		int thisLevel = enable ? DIAG_SEVERITY_DISPLAY : DIAG_SEVERITY_FATAL;
		for (diagChannel *i = First; i; i = i->Next)
		{
			Assign(i->TtyLevel,thisLevel);
			Assign(i->FileLevel,thisLevel);
			i->UpdateMaxLevel();
		}
	}
	s_output = enable;
}

bool diagChannel::GetOutput()
{
	return s_output;
}

static u64 s_PosixMarkTimeMs = 0; //Last time we marked time.
static u64 s_PosixBaseTimeMs = 0; //The time we marked.
static u64 s_ServerPosixTime = 0; //The posix time we get from the server
static u64 s_ServerSyncTime = 0;  //The system time at which we recorded the Server Posix Time

void diagChannel::SetPosixTime(u64 posixTime)
{
	// record the posix time the server sent us, along with the system time at
	// which we recorded the server time. This is used in GetPosixTime().
	s_ServerPosixTime = posixTime;
	s_ServerSyncTime = sysTimer::GetSystemMsTime();

	// update our cached time
	s_PosixMarkTimeMs = 0;
	GetPosixTime();
}

u64 diagChannel::GetPosixTime()
{
	const u64 curTimeMs = sysTimer::GetSystemMsTime();

	if(s_ServerPosixTime > 0)
	{
		// we have server time
		s64 elapsedMs = curTimeMs - s_ServerSyncTime;
		return s_ServerPosixTime + (elapsedMs / 1000);
	}

	static const u64 REFRESH_INTERVAL_MS  = 60*1000;

	//Refresh the posix base time every REFRESH_INTERVAL seconds.
	if(0 == s_PosixMarkTimeMs || (curTimeMs - s_PosixMarkTimeMs >= REFRESH_INTERVAL_MS))
	{
		u64 posixTimeSec;

#if RSG_DURANGO
		//Difference between the Windows epoch (1601-01-01) and the Posix epoch (1970-01-01).
		static const u64 DELTA_EPOCH_SECONDS = 11644473600UL;

		//Get the time in 100ns units since the Windows epoch.
		FILETIME fileTime;
		GetSystemTimeAsFileTime(&fileTime);

		posixTimeSec = (u64(fileTime.dwHighDateTime) << 32) | u64(fileTime.dwLowDateTime);
		posixTimeSec /= (1000*1000*1000)/100;  //Convert to seconds
		posixTimeSec -= DELTA_EPOCH_SECONDS;   //Convert to Posix epoch
#elif RSG_ORBIS
		//Difference between the NP epoch (0001-01-01) and the Posix epoch (1970-01-01).
		static const u64 DELTA_EPOCH_SECONDS = 62135596800UL;

		SceRtcTick ticks;
		int err = sceRtcGetCurrentNetworkTick(&ticks);

		if(err < 0)
		{
			// use local ticks
			err = sceRtcGetCurrentTick(&ticks);
		}

		if(err >= 0)
		{
			posixTimeSec = ticks.tick;
			posixTimeSec /= (1000*1000);   //Convert to seconds
			posixTimeSec -= DELTA_EPOCH_SECONDS;
		}
		else
		{
			// we haven't received the current posix time from the server yet,
			// so fall back to whatever the user's clock is set to
			posixTimeSec = time(NULL);
		}
#else
		// we haven't received the current posix time from the server yet,
		// so fall back to whatever the user's clock is set to
		posixTimeSec = time(NULL);
#endif

		s_PosixBaseTimeMs = posixTimeSec * 1000;
		s_PosixMarkTimeMs = curTimeMs;
	}

	return (s_PosixBaseTimeMs + (curTimeMs - s_PosixMarkTimeMs))/1000;
}

#if __WIN32PC
static sysCriticalSectionToken printToken;
#endif

#if __WIN32 || RSG_ORBIS
static size_t StripColorization(char *dest,size_t destSize,const char *src)
{
	char *d = dest;
	char *const dlast = d+destSize-1;
	while (*src && d<dlast)
	{
		if (*src == '\033')
		{
			if (!src[1] || !src[2] || !src[3])
				++src;
			else if (src[3]=='m')
				src += 4;
			else if (src[4]=='m')
				src += 5;
			else
				++src;
		}
		else
			*d++ = *src++;
	}
	*d = 0;
	return d-dest;
}
#endif

void diagPrintDefault(const char *msg)
{
#if __WIN32PC

	// All escape sequences used at start of line are five characters long.
	char temp[CHANNEL_MESSAGE_SIZE];
	size_t len = StripColorization(temp,sizeof(temp),msg);
	if (s_TtyBufferEnable)
		BufferedOutputDebugString(temp, len);
	else
	{
		SYS_CS_SYNC(printToken);

# if !defined(_M_X64)
		unsigned short oldfpstate;
		_asm fnstcw [oldfpstate];
# endif

		OutputDebugString(temp);
		while (*msg) 
		{
			if (*msg == '\033')
			{
				fflush(stdout);
				if (msg[1] == '[' && msg[2] && (msg[3]=='m' || (msg[3] && msg[4] == 'm'))) 
				{
					if (msg[2] == '0')
					{
						SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
						msg += 4;
					}
					else
					{
						WORD r = msg[3] & 1? FOREGROUND_RED : 0;
						WORD g = msg[3] & 2? FOREGROUND_GREEN : 0;
						WORD b = msg[3] & 4? FOREGROUND_BLUE : 0;
						SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),r|g|b|FOREGROUND_INTENSITY);
						msg += 5;
					}
				}
				else
					++msg;
			}
			else
			{
				// don't output the bell character to TTY. It causes system beeps when outputting binary data.
				if((*msg == '\a'))
				{
					*msg++;
				}
				else
				{
					putchar(*msg++);
				}
			}
		}

		fflush(stdout);

# if !defined(_M_X64)
		_asm fldcw [oldfpstate]
# endif
	}

#elif __XENON
	char temp[CHANNEL_MESSAGE_SIZE];
	StripColorization(temp,sizeof(temp),msg);
	OutputDebugString(temp);
#elif __PPU
	unsigned pWriteLen;
	/* 0=PPU, 1=PPU stderr, 2=SPU, 3=USER1, 4=USER2, ... 15=USER13 */
	int channel = (msg[0]=='[' && msg[1]=='S' && msg[2]=='P' && msg[3]=='U' && msg[5]==']')? SYS_TTYP2 : SYS_TTYP0;
	sys_tty_write(channel,msg,strlen(msg),&pWriteLen);
#elif __PSP2 || RSG_ORBIS
	/// for now...
	char temp[CHANNEL_MESSAGE_SIZE];
	size_t len = StripColorization(temp,sizeof(temp),msg);
	if (s_TtyBufferEnable)
		BufferedOutputDebugString(temp, len);
	else
		printf("%s", temp);
#elif RSG_DURANGO
	char temp[CHANNEL_MESSAGE_SIZE];
	size_t len = StripColorization(temp,sizeof(temp),msg);
	if (s_TtyBufferEnable)
		BufferedOutputDebugString(temp, len);
#if REDIRECT_TTY
	else if(!s_allowOutputDebugString && !sysBootManager::IsDebuggerPresent())
	{
		// On Durango, disabled OutputDebugString if no debugger is present. This prevents QA from
		// starting R*TM after the game has booted to caputre TTY - which causes slowdowns and audio
		// stutters due to kernel debugger.
		// s_allowOutputDebugString will be true initially, until R*TM setup code executes, so we
		// don't lose any initial TTY output.
	}
#endif // REDIRECT_TTY
	else
		OutputDebugString(temp);
#endif
}

void diagPrintFlush()
{
	fflush(stdout);
}

void (*diagPrintCallback)(const char*) = diagPrintDefault;

void diagPrint(const char* buffer)
{
    diagPrint(buffer, (int)strlen(buffer));
}

void diagPrintLn(const char* buffer)
{
    diagPrintLn(buffer, (int)strlen(buffer));
}

void diagPrint(const char* buffer, const int bufLen)
{
    static const int MAX_CHARS  = 255;
    char buf[MAX_CHARS+1];
    for(int i = 0; i < (int)bufLen; i += MAX_CHARS)
    {
        int len;
        if(i+MAX_CHARS <= bufLen)
        {
            len = MAX_CHARS;
        }
        else
        {
            len = bufLen - i;
        }
        memcpy(buf, &buffer[i], len);
        buf[len] = '\0';

	#if REDIRECT_TTY
		if(s_DontDoDiagPrintCallback)
		{
			diagPrintDefault(buf);
		}
		else
	#endif // REDIRECT_TTY
		{
			diagPrintCallback(buf);
		}
    }
}

void diagPrintLn(const char* buffer, const int bufLen)
{
    diagPrint(buffer, bufLen);

#if REDIRECT_TTY
	if(s_DontDoDiagPrintCallback)
	{
		diagPrintDefault("\n");
	}
	else
#endif // REDIRECT_TTY
	{
		diagPrintCallback("\n");
	}
}

void diagLoggedPrint(const char *buffer)
{
    diagLoggedPrint(buffer, (int)strlen(buffer));
}

void diagLoggedPrintLn(const char *buffer)
{
    diagLoggedPrintLn(buffer, (int)strlen(buffer));
}

void diagLoggedPrint(const char *buffer, const int bufLen)
{
	{
		SYS_CS_SYNC(logToken);

		if (logStream)
		{
		    logStream->Write(buffer,bufLen);
        }
	}

    diagPrint(buffer, bufLen);
}

void diagLoggedPrintLn(const char *buffer, const int bufLen)
{
	{
		SYS_CS_SYNC(logToken);

		if (logStream)
		{
		    logStream->Write(buffer,bufLen);
			logStream->Write("\n",1);
        }
	}

    diagPrintLn(buffer, bufLen);
}

void diagPrintf(const char *fmt,...)
{
	va_list args;
	va_start(args,fmt);
	char buffer[CHANNEL_MESSAGE_SIZE];
	vformatf(buffer,sizeof(buffer),fmt,args);
	va_end(args);

#if REDIRECT_TTY
	if(s_DontDoDiagPrintCallback)
	{
		diagPrintDefault(buffer);
	}
	else
#endif // REDIRECT_TTY
	{
		diagPrintCallback(buffer);
	}
}

void diagLoggedPrintf(const char *fmt,...)
{
	va_list args;
	va_start(args,fmt);
	char buffer[CHANNEL_MESSAGE_SIZE];
	vformatf(buffer,sizeof(buffer),fmt,args);
	va_end(args);

	{
		SYS_CS_SYNC(logToken);

		if (logStream)
		{
			// Special case; if string is terminated with newline, convert to CR/LF on output.
			// Don't bother with internal cases yet, diagLoggedPrintf isn't used that often.
			int bufLen = StringLength(buffer);
			if (bufLen && buffer[bufLen-1] == '\n') 
			{
				logStream->Write(buffer,bufLen-1);
				logStream->Write("\r\n",2);
			}
			else
				logStream->Write(buffer,bufLen);
		}
	}

	diagPrintCallback(buffer);
}

// Higher-level code is expected to override this.
const char *diagBuildString = "RAGE-" RAGE_RELEASE_STRING;

static bool diagExternalCallbackNone(const diagChannel&,diagSeverity&,const char*,int,const char*)
{
	return true;		// Allow popups.  This function can also use diagLoggedPrintf to display extra context.
}

static int diagLogBugCallbackNone(const char*,const char*,u32,int dialogReturned)
{
	return dialogReturned;
}

// This wrapper is required to call sysStack::PrintStackTrace, because PrintStackTrace takes an argument and
// therefore doesn't match the signature of the application-settable callback.
void diagDefaultPrintStackTrace()
{
#if !__FINAL || __FINAL_LOGGING
	sysStack::PrintStackTrace();
#endif // !!__FINAL || __FINAL_LOGGING
}

unsigned diagDefaultGetGameFrame()
{
	return 0;
}

static void diagDefaultLongExecutionTime(const unsigned, const unsigned, const unsigned, const char*, const char*)
{

}

bool (*diagExternalCallback)(const diagChannel&,diagSeverity&,const char*,int,const char*) = diagExternalCallbackNone;
void (*diagPrintStackTrace)() = diagDefaultPrintStackTrace;
int (*diagLogBugCallback)(const char*,const char*,u32,int) = diagLogBugCallbackNone;
unsigned (*diagGetGameFrame)() = diagDefaultGetGameFrame;
void (*diagLongExecutionCallback)(const unsigned, const unsigned, const unsigned, const char*, const char*) = diagDefaultLongExecutionTime;

static int s_LogsByType[DIAG_SEVERITY_COUNT];

struct Message
{
	Message *Prev;
	const char *Fmt, *Arg1, *Arg2;
	const char *(*Callback)(void *user1,void *user2);
	const char *GetMsg(char *buf,size_t bufSize)
	{
		if (Callback)
			formatf(buf,bufSize,Fmt,(*Callback)((void*)Arg1,(void*)Arg2));
		else
			formatf(buf,bufSize,Fmt,Arg1,Arg2);
		return buf;
	}
};
static __THREAD Message *s_TopContext;

unsigned diagGetAdjustedSystemTime()
{
    static unsigned baseTime;

	if (!baseTime)
		baseTime = sysTimer::GetSystemMsTime();

    return (sysTimer::GetSystemMsTime() - baseTime);
}

bool diagLogDefault(const diagChannel &channel,diagSeverity severity,const char *file,int line,const char *fmt, va_list args)
{
	if (!s_output)
		return true;

	// lightweight log profiling
	unsigned functionTopTime = sysTimer::GetSystemMsTime();

	++s_LogsByType[severity];

	bool logToFile = severity <= channel.FileLevel;
	bool logToTty = severity <= channel.TtyLevel;
	bool logToPopup = severity <= channel.PopupLevel;
	bool usePrefix = diagChannel::GetUseThreadPrefix();
	bool posixPrefix = channel.PosixPrefix == CHANNEL_POSIX_ON;

	// Figure out if this message was previously ignored, and abort immediately
	// in that case -- no callstack, no logging to file or tty.
	u64 ignoreCode = (u64)(size_t)file << 32 | line;

	if (logToPopup)
	{
		for (int i=0; i<s_ignoredCodeCount; i++)
			if (s_ignoredCodes[i] == ignoreCode)
				return true;
	}

	char posixPrefixStr[12]; // 12 is enough to hold 10 digits for the posix time, the ':' separator and the NULL terminating character
	posixPrefixStr[0] = 0;
	if(posixPrefix)
	{
		formatf(posixPrefixStr, ":%010u", diagChannel::GetPosixTime());
	}

	unsigned relativeTime = diagGetAdjustedSystemTime();
	unsigned gameFrame = diagGetGameFrame();

	// format message. Do it after the ignore code check to save time
	char msg[CHANNEL_MESSAGE_SIZE];
	vformatf(msg,sizeof(msg),fmt,args);

	// Stack backtraces are pretty expensive, so only do the work once
#if !__FINAL || __FINAL_LOGGING
	if ((logToTty || logToFile) && severity <= channel.PopupLevel) {
		diagPrintf(TPurple "%s" TNorm "\n", diagAssertTitle? diagAssertTitle : sysParam::GetProgramName());
		diagPrintStackTrace();
	}
#endif

	static char spaces[33] = "                                ";
	int indent = 30;
	char msgBuf[128];

	if (logToFile)
	{
		SYS_CS_SYNC(logToken);
		if (logStream)
		{
			if (PARAM_usevsoutput.Get() && line)
			{
				if(usePrefix)
				{
					fprintf(logStream,"%s(%d): [%08u:%08u%s] %s%s%s",file,line,relativeTime,gameFrame,posixPrefixStr,s_Prefixes[severity],g_CurrentThreadName,channel.Tag);
				}
				fprintf(logStream,"%s\r\n",msg);
				if (s_TopContext && severity <= DIAG_SEVERITY_WARNING)
				{
					Message* i = s_TopContext;
					while(i)
					{
						if(usePrefix)
						{
							fprintf(logStream, "%s(%d): [%08u:%08u%s] %s%s%s", file,line, relativeTime,gameFrame,posixPrefixStr,s_Prefixes[severity],g_CurrentThreadName,channel.Tag);
						}
						fprintf(logStream, "%sfrom %s\r\n",spaces+indent,i->GetMsg(msgBuf,sizeof(msgBuf)));
						i = i->Prev;
						if (indent)
						{
							indent -= 2;
						}
					}
				}
			}
			else
			{
				if(PARAM_logtimestamps.Get())
				{
					if(usePrefix)
					{
						time_t secs = time(NULL);
						struct tm* t = localtime(&secs);
						fprintf(logStream,"[%u/%u %u:%02u:%02u%s] %s%s%s",t->tm_mon + 1, t->tm_mday, ((t->tm_hour % 12) == 0) ? 12 : (t->tm_hour % 12), t->tm_min, t->tm_sec, (t->tm_hour < 12) ? "am" : "pm",s_Prefixes[severity],g_CurrentThreadName,channel.Tag);
					}
				}
				else
				{
					if(usePrefix)
					{
						fprintf(logStream,"[%08u:%08u%s] %s%s%s",relativeTime,gameFrame, posixPrefixStr, s_Prefixes[severity],g_CurrentThreadName,channel.Tag);
					}
				}

				fprintf(logStream,"%s\r\n", msg);
				if (s_TopContext && severity <= DIAG_SEVERITY_WARNING)
				{
					Message* i = s_TopContext;
					while(i)
					{
						if (usePrefix)
						{
							fprintf(logStream, "[%08u:%08u%s] %s%s%s", relativeTime,gameFrame,posixPrefixStr,s_Prefixes[severity],g_CurrentThreadName,channel.Tag);
						}
						fprintf(logStream, "%sfrom %s\r\n",spaces+indent,i->GetMsg(msgBuf,sizeof(msgBuf)));
						i = i->Prev;
						if (indent)
						{
							indent -= 2;
						}
					}
				}
			}
			if (severity <= DIAG_SEVERITY_ASSERT)
				logStream->Flush();
		}
	}

	if (logToTty
#if REDIRECT_TTY
			|| s_TtyBufferEnable
#endif // REDIRECT_TTY
		)
	{
#if __DEV
		if (PARAM_rfsRemoteTty.Get())
			fiRemoteTty(msg);
#endif // __DEV

		char singleContext[128];
		singleContext[0] = 0;
		if (s_TopContext && severity <= DIAG_SEVERITY_WARNING && !s_TopContext->Prev)
			formatf(singleContext," (from %s)",s_TopContext->GetMsg(msgBuf,sizeof(msgBuf)));

#if REDIRECT_TTY
		// Don't call diagPrintCallback(), since that would push the text through to RAG. Instead,
		// directly call diagPrintDefault(), which sends it over the TCP pipe to R*TM.
		if (!logToTty)
		{
			s_DontDoDiagPrintCallback = true;
		}
#endif // REDIRECT_TTY

		// Note the code is intentionally calling diagPrintf only once.
		if (PARAM_usevsoutput.Get() && line)
		{
			if (PARAM_ttyframeprefix.Get())
			{
				if(usePrefix)
				{
					diagPrintf("%s%s(%d): [%08u:%08u%s] %s%s%s%s%s%s\n",s_Colors[severity],file,line,relativeTime,gameFrame,posixPrefixStr,s_VsPrefixes[severity],g_CurrentThreadName,channel.Tag,msg,singleContext,severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
				}
				else
				{
					diagPrintf("[%08u:%08u] %s%s%s\n",relativeTime,gameFrame,msg,singleContext,severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
				}
			}
			else
			{
				if(usePrefix)
				{
					diagPrintf("%s%s(%d): %s%s%s%s%s%s\n",s_Colors[severity],file,line,s_VsPrefixes[severity],g_CurrentThreadName,channel.Tag,msg,singleContext,severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
				}
				else
				{
					diagPrintf("%s%s%s\n",msg,singleContext,severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
				}
			}
		}
		else
		{
			if (PARAM_ttyframeprefix.Get())
			{
				if(usePrefix)
				{
					diagPrintf("%s[%08u:%08u%s] %s%s%s%s%s%s\n",s_Colors[severity],relativeTime,gameFrame,posixPrefixStr,s_Prefixes[severity],g_CurrentThreadName,channel.Tag,msg,singleContext,severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
				}
				else
				{
					diagPrintf("[%08u:%08u] %s%s%s\n",relativeTime,gameFrame,msg,singleContext,severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
				}
			}
			else
			{
				if(usePrefix)
				{
					diagPrintf("%s%s%s%s%s%s%s\n",s_Colors[severity],s_Prefixes[severity],g_CurrentThreadName,channel.Tag,msg,singleContext,severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
				}
				else
				{
					diagPrintf("%s%s%s\n",msg,singleContext,severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
				}
			}
		}
		if (s_TopContext && severity <= DIAG_SEVERITY_WARNING && s_TopContext->Prev)
		{
			Message* i = s_TopContext;
			while(i)
			{
				if (PARAM_usevsoutput.Get() && line)
				{
					if (PARAM_ttyframeprefix.Get())
					{
						if (usePrefix)
						{
							diagPrintf("%s%s(%d): [%08u:%08u%s] %s%s%s%sfrom %s%s\n",s_Colors[severity],file,line,relativeTime,gameFrame,posixPrefixStr,s_VsPrefixes[severity],g_CurrentThreadName,channel.Tag,spaces + indent,i->GetMsg(msgBuf,sizeof(msgBuf)),severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
						}
						else
						{
							diagPrintf("                    %sfrom %s%s\n",spaces + indent,i->GetMsg(msgBuf,sizeof(msgBuf)),severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
						}
					}
					else
					{
						if (usePrefix)
						{
							diagPrintf("%s%s(%d): %s%s%s%sfrom %s%s\n",s_Colors[severity],file,line,s_VsPrefixes[severity],g_CurrentThreadName,channel.Tag,spaces + indent,i->GetMsg(msgBuf,sizeof(msgBuf)),severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
						}
						else
						{
							diagPrintf("%sfrom %s%s\n",spaces + indent,i->GetMsg(msgBuf,sizeof(msgBuf)),severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
						}
					}
				}
				else
				{
					if (PARAM_ttyframeprefix.Get())
					{
						if (usePrefix)
						{
							diagPrintf("%s[%08u:%08u%s] %s%s%s%sfrom %s%s\n",s_Colors[severity],relativeTime,gameFrame,posixPrefixStr,s_Prefixes[severity],g_CurrentThreadName,channel.Tag,spaces+indent,i->GetMsg(msgBuf,sizeof(msgBuf)),severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
						}
						else
						{
							diagPrintf("                    %sfrom %s%s\n",spaces+indent,i->GetMsg(msgBuf,sizeof(msgBuf)),severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
						}
					}
					else
					{
						if (usePrefix)
						{
							diagPrintf("%s%s%s%s%sfrom %s%s\n",s_Colors[severity],s_Prefixes[severity],g_CurrentThreadName,channel.Tag,spaces+indent,i->GetMsg(msgBuf,sizeof(msgBuf)),severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
						}
						else
						{
							diagPrintf("%sfrom %s%s\n",spaces+indent,i->GetMsg(msgBuf,sizeof(msgBuf)),severity!=DIAG_SEVERITY_DISPLAY?TNorm:"");
						}
					}
				}
					i = i->Prev;
				if (indent)
				{
					indent -= 2;
				}
			}
		}

#if REDIRECT_TTY
		s_DontDoDiagPrintCallback = false;
#endif // REDIRECT_TTY
	}

#if __WIN32PC
	if (severity == DIAG_SEVERITY_FATAL && PARAM_emailasserts.Get())
	{
		const char *recipient = NULL, *sender = NULL;
		if (PARAM_emailasserts.Get(recipient) && *recipient && PARAM_emailassertsfrom.Get(sender) && *sender)
		{
			char text2[CHANNEL_MESSAGE_SIZE2];
			formatf(text2,sizeof(text2),"Program: %s\nFile:    %s\nLine:    %d\n\n%s\n",diagAssertTitle? diagAssertTitle : sysParam::GetProgramName(),file,line,msg);
			SendSystemEmail(sender, recipient, "Fatal Error Email", text2);
		}
	}
#endif

#if !__FINAL || __FINAL_LOGGING
	if (diagExternalCallback(channel,severity,file,line,msg) && logToPopup)
	{
		unsigned flags, defaultIfNoServer, topmostAddon;
		const char *insns;

		// If we don't have PARAMs, we can't assert.
		if (!sysParam::IsInitialized())
		{
			diagPrintf("%s: %s\n", file, msg);
			__debugbreak();
		}

		topmostAddon = (PARAM_notopmostasserts.Get()) ? 0 : MB_TOPMOST | MB_SETFOREGROUND;
		topmostAddon |= (PARAM_noassertfocus.Get()) ? MB_NOFOCUS : 0;

		if (severity == DIAG_SEVERITY_FATAL) 
		{
			flags = MB_OK | MB_ICONERROR | topmostAddon;
			defaultIfNoServer = IDOK;
			insns = "Press Ok to abort the program.  It is not safe to continue.";
		}
		else
		{
			if (!PARAM_noquitonassert.Get())
			{
				flags = MB_ABORTRETRYIGNORE | MB_ICONQUESTION | topmostAddon | MB_DEFBUTTON3;
				defaultIfNoServer = IDIGNORE;
				const char *answer;
				if (PARAM_nopopups.Get(answer))
				{
					if (!stricmp(answer,"abort"))
						defaultIfNoServer = IDABORT;
					else if (!stricmp(answer,"retry"))
						defaultIfNoServer = IDRETRY;
				}
				else
				{
					defaultIfNoServer = IDIGNORE;
				}
				insns = "Press Abort to stop the program.\nPress Retry to continue onward but halt on this message if it happens again.\nPress Ignore to continue onward and ignore this in the future.";
			}
			else
			{
				flags = MB_OK | MB_ICONQUESTION | topmostAddon | MB_DEFBUTTON1;
				defaultIfNoServer = IDOK;
				insns = "Press OK to continue onward and ignore this in the future.";
			}
		}
		char text[CHANNEL_MESSAGE_SIZE2];
		if (line)
			formatf(text,sizeof(text),"%s(%d): %s%s%s\n\n",file,line,channel.Tag,s_Prefixes[severity],msg);
		else
			formatf(text,sizeof(text),"%s%s%s\n\n",channel.Tag,s_Prefixes[severity],msg);
		Message *m = s_TopContext;
		if (m)
		{
			while (m)
			{
				safecat(text,"...from ");
				safecat(text,(const char*)(m->GetMsg(msgBuf,sizeof(msgBuf))));
				safecat(text,"\n");
				m = m->Prev;
			}
			safecat(text,"\n");
		}
		safecat(text,insns);

		unsigned result = 0;

		// Prepare additional messagebox options
		unsigned options = 0;
		if(PARAM_messageBoxTimeout.Get())
		{
			PARAM_messageBoxTimeout.Get(s_messageBoxTimeout);
			unsigned timeout = s_messageBoxTimeout;

			if(timeout > MBO_TIMEOUT)
				timeout = MBO_TIMEOUT;

			options |= timeout & MBO_TIMEOUT;
		}
		options |= MBO_ASSERTKEYBOARD;
		// Add other options here...		

#if !__FINAL
		if (PARAM_bugAsserts.Get())
		{
			char buffer[CHANNEL_MESSAGE_SIZE2 + 256];
			formatf(buffer, "Abort\nRetry\nIgnore\nRequest AssertBot Popup\n\r%s", text);

			// specify that we are using a custom messagebox
			flags |= MB_XMESSAGEBOX;

			// Convert the default to the proper custom result.
			if (defaultIfNoServer == IDABORT)
				defaultIfNoServer = 1;
			else if (defaultIfNoServer == IDRETRY)
				defaultIfNoServer = 2;
			else if (defaultIfNoServer == IDIGNORE)
				defaultIfNoServer = 3;
			else if (defaultIfNoServer == IDCUSTOM4)
				defaultIfNoServer = 4;
			else
				defaultIfNoServer = 3;

			result = fiRemoteShowMessageBox(buffer, diagAssertTitle ? diagAssertTitle : sysParam::GetProgramName(), flags, defaultIfNoServer, options);

			// convert custom messagebox result flags back to assert flags
			if (result == 1)
				result = IDABORT;
			else if (result == 2)
				result = IDRETRY;
			else if (result == 3)
				result = IDIGNORE;
			else if (result == 4)
				result = IDCUSTOM4;
			else
				result = IDIGNORE;

			// Construct summary 
			char summary[256];
			formatf(summary,sizeof(summary),"%s%s%s",channel.Tag,s_Prefixes[severity],msg);
			// construct detailed summary
			if (line)
				formatf(text,sizeof(text),"%s(%d): %s%s%s\n",file,line,channel.Tag,s_Prefixes[severity],msg);
			else
				formatf(text,sizeof(text),"%s%s%s\n",channel.Tag,s_Prefixes[severity],msg);
			Message *m = s_TopContext;
			if (m)
			{
				safecat(text,"\n");
				while (m)
				{
					safecat(text,"...from ");
					safecat(text,(const char*)(m->GetMsg(msgBuf,sizeof(msgBuf))));
					safecat(text,"\n");
					m = m->Prev;
				}
			}

			// construct hash
			u32 hashId = atPartialStringHash(fmt);
			if(s_TopContext)
				hashId = atPartialStringHash(s_TopContext->GetMsg(msgBuf,sizeof(msgBuf)), hashId);
			// Call the callback that is used for logging/updated the associated assert bug
			// (ID_CUSTOM4 indicates that they wish to view the bug)
			result = diagLogBugCallback(summary, text, hashId, result);
		}
		else
		{
			result = fiRemoteShowMessageBox(text,diagAssertTitle?diagAssertTitle:sysParam::GetProgramName(),flags,defaultIfNoServer,options);
		}
#endif
		// User chose abort (possibly no choice, if fatal)
		if (!PARAM_noquitonassert.Get() && (result == IDOK || result == IDABORT))
		{
#		  if SYSTMCMD_ENABLE
			// If not running R*TM, return and let the game break or crash on
			// the __debugbreak.  If R*TM is running, continue on so we hit the
			// sysTmCmdQuitf().
			if (!PARAM_rockstartargetmanager.Get() || severity != DIAG_SEVERITY_FATAL)
#		  endif
				return false;
		}
		else if ((result == IDOK || result == IDIGNORE) && s_ignoredCodeCount < maxIgnore)
			s_ignoredCodes[s_ignoredCodeCount++] = ignoreCode;
	}
#else
	// special compile hack for debugging final builds
	OUTPUT_ONLY((void)sizeof(CHANNEL_MESSAGE_SIZE2));
#endif

	// Fatal errors thunk to diagTerminate right now.
	if (severity == DIAG_SEVERITY_FATAL && !sysBootManager::IsDebuggerPresent() && !PARAM_noquits.Get())
	{
		SPIN_IF_HAS_EXCEPTION_BEEN_THROWN();
#if __FINAL && HACK_RDR2 && !__NO_OUTPUT
		*(int*)-1=-1;
#elif SYSTMCMD_ENABLE
		sysTmCmdQuitf(msg);
#else
		diagTerminate();
#endif
	}

	static const unsigned LONG_FUNCTION_TIME = 500;
	unsigned functionExecutionTime = (sysTimer::GetSystemMsTime() - functionTopTime);
	if(functionExecutionTime > LONG_FUNCTION_TIME)
	{
		diagLongExecutionCallback(functionExecutionTime, relativeTime, gameFrame, channel.Tag, msg);
	}

	return severity != DIAG_SEVERITY_FATAL || PARAM_noquits.Get();
}

bool (*diagLogCallback)(const diagChannel &channel,diagSeverity severity,const char *file,int line,const char *fmt, va_list args) = diagLogDefault;

void diagSetLongExecutionCallback(diagLongExecutionCallbackType callback)
{
	diagLongExecutionCallback = callback;
}

bool diagLogf(const diagChannel &channel,diagSeverity severity,const char *file,int line,const char *fmt,...)
{
	if (sysIpcGetCurrentThreadId() == s_loggerThreadId)
		return false;
	SYS_CS_SYNC(diagLogfToken);
	va_list args;
	va_start(args,fmt);
	bool rt = diagLogCallback(channel,severity,file,line,fmt,args);
	va_end(args);
	return rt;
}

// Identical to diagLogf2 but the severity is implicit to save an instruction on every call.
bool diagLogf_Assert(const diagChannel &channel,const char *file,int line,const char *fmt,...)
{
	if (sysIpcGetCurrentThreadId() == s_loggerThreadId)
		return false;
	SYS_CS_SYNC(diagLogfToken);
	va_list args;
	va_start(args,fmt);
	bool rt = diagLogCallback(channel,DIAG_SEVERITY_ASSERT,file,line,fmt,args);
	va_end(args);
	return rt;
}

// Cheaper version doesn't accept __FILE__ or __LINE__ and has no return value to test
void diagLogf2(const diagChannel &channel,diagSeverity severity,const char *fmt,...)
{
	if (sysIpcGetCurrentThreadId() == s_loggerThreadId)
		return;
	SYS_CS_SYNC(diagLogfToken);
	va_list args;
	va_start(args,fmt);
	diagLogCallback(channel,severity,"",0,fmt,args);
	va_end(args);
}

void diagErrorf(const diagChannel &channel,const char *fmt,...)
{
	if (channel.MaxLevel < DIAG_SEVERITY_ERROR)
		return;
	if (sysIpcGetCurrentThreadId() == s_loggerThreadId)
		return;
	SYS_CS_SYNC(diagLogfToken);
	va_list args;
	va_start(args,fmt);
	bool rt = diagLogCallback(channel,DIAG_SEVERITY_ERROR,"",0,fmt,args);
	va_end(args);

	//if error popups are enabled for this channel, make the abort button work
	if(!rt && channel.PopupLevel <= DIAG_SEVERITY_ERROR)
	{
		__debugbreak();
	}
}

void diagWarningf(const diagChannel &channel,const char *fmt,...)
{
	if (channel.MaxLevel < DIAG_SEVERITY_WARNING)
		return;
	if (sysIpcGetCurrentThreadId() == s_loggerThreadId)
		return;
	SYS_CS_SYNC(diagLogfToken);
	va_list args;
	va_start(args,fmt);
	diagLogCallback(channel,DIAG_SEVERITY_WARNING,"",0,fmt,args);
	va_end(args);
}

void diagDisplayf(const diagChannel &channel,const char *fmt,...)
{
	if (channel.MaxLevel < DIAG_SEVERITY_DISPLAY)
		return;
	if (sysIpcGetCurrentThreadId() == s_loggerThreadId)
		return;
	SYS_CS_SYNC(diagLogfToken);
	va_list args;
	va_start(args,fmt);
	diagLogCallback(channel,DIAG_SEVERITY_DISPLAY,"",0,fmt,args);
	va_end(args);
}


#if __BANK
__THREAD Message *s_diagPrivateHeap = NULL;
const int diagPrivateHeapSize = 32;

void diagContextMessage::Push(const char *fmt,const char *a1,const char *a2,const char* (*cb)(void*,void*))
{
	// Allocate a private context stack on first use by a thread
	if (!s_diagPrivateHeap) {
		RAGE_TRACK(diagContextMessage);
		s_diagPrivateHeap = rage_new Message[diagPrivateHeapSize];
	}

	Message *newMsg = s_TopContext? s_TopContext + 1 : s_diagPrivateHeap;
	if (newMsg == s_diagPrivateHeap + diagPrivateHeapSize)
		Quitf("MessageCommon - too many contexts pushed");

	newMsg->Fmt = fmt;
	newMsg->Arg1 = a1;
	newMsg->Arg2 = a2;
	newMsg->Callback = cb;

	const char* contextSubstr = NULL;
	if (PARAM_breakoncontext.Get(contextSubstr))
	{
		char buffer[CHANNEL_MESSAGE_SIZE];
		const char* result = NULL;
		if (cb)
		{
			result = cb((void*)a1, (void*)a2);
		}
		else
		{
			formatf(buffer, fmt, a1, a2);
			result = &buffer[0];
		}

		if (result && stristr(result, contextSubstr))
		{
			Warningf("Found -breakoncontext context %s", result);
			__debugbreak();
		}
	}

	// Update this thread's stack
	newMsg->Prev = s_TopContext;
	s_TopContext = newMsg;
}

diagContextMessage::diagContextMessage(const char* fmt) 
{
	Push(fmt,NULL,NULL,NULL);
}

diagContextMessage::diagContextMessage(const char* fmt,const char *a1) 
{
	Push(fmt,a1,NULL,NULL);
}

diagContextMessage::diagContextMessage(const char* fmt,const char *a1,const char *a2) 
{
	Push(fmt,a1,a2,NULL);
}

diagContextMessage::diagContextMessage(const char* fmt,const char *(*cb)(void *,void *),void *user1,void *user2) 
{
	Push(fmt,(const char*)user1,(const char*)user2,cb);
}

void diagContextMessage::Pop()
{
	s_TopContext = s_TopContext->Prev;
}

diagContextMessage::~diagContextMessage() 
{ 
	Pop();
}

void diagContextMessage::PerThreadShutdown()
{
	if (s_diagPrivateHeap) {
		delete[] s_diagPrivateHeap;
		s_diagPrivateHeap = NULL;
	}
}

void diagContextMessage::DisplayContext(const char *prefix)
{
	char msgBuf[128];
	Message *i = s_TopContext;
	static char spaces[33] = "                                ";
	int indent = 30;
	while (i)
	{
		diagLoggedPrintf("%s%sfrom %s\n",prefix,spaces + indent,i->GetMsg(msgBuf,sizeof(msgBuf)));
		i = i->Prev;
		if (indent)
			indent -= 2;
	}
}

const char *diagContextMessage::GetTopMessage(char *buf,size_t siz)
{
	return s_TopContext? s_TopContext->GetMsg(buf,siz) : NULL;
}
#endif

//// LEGACY CODE SUPPORT

}

void Printf(const char * fmt,...)		// This is a duplicate of diagLoggedPrintf.
{
	using namespace rage;

	if (!s_output)
		return;

	va_list args;
	va_start(args,fmt);
	char buffer[CHANNEL_MESSAGE_SIZE];
	vformatf(buffer,sizeof(buffer),fmt,args);
	va_end(args);

	{
		SYS_CS_SYNC(logToken);
		if (logStream)
		{
			logStream->Write(buffer,StringLength(buffer));
		}
#if REDIRECT_TTY
		if (s_TtyBufferEnable)
		{
			BufferedOutputDebugString(buffer, StringLength(buffer));
		}
#endif // REDIRECT_TTY
	}

#if REDIRECT_TTY
	if(s_DontDoDiagPrintCallback)
	{
		diagPrintDefault(buffer);
	}
	else
#endif // REDIRECT_TTY
	{
		diagPrintCallback(buffer);
	}
}

namespace rage { 

diagChannel Channel_Legacy(&diagChannel::ALL,"","legacy",DIAG_SEVERITY_DEBUG1,DIAG_SEVERITY_DISPLAY,DIAG_SEVERITY_ASSERT);

static unsigned s_OutputMask = diagOutput::OUTPUT_FATALS | diagOutput::OUTPUT_ERRORS | diagOutput::OUTPUT_WARNINGS | diagOutput::OUTPUT_DISPLAYS;

#define IMPLEMENT_LEGACY_FUNCTION(entryName,severity) \
void entryName(const char *fmt,...) { 	\
	if (DIAG_SEVERITY_##severity == DIAG_SEVERITY_FATAL) s_output = true; \
	if (!(s_output && (s_OutputMask & diagOutput::OUTPUT_##severity##S))) return; \
	/* Scoped here so to unlock diagLogfToken before */ \
	/* triggering an exception for DIAG_SEVERITY_FATAL */ \
	{ \
		if (sysIpcGetCurrentThreadId() == s_loggerThreadId) \
			return; \
		SYS_CS_SYNC(diagLogfToken); \
		va_list args; \
		va_start(args,fmt); \
		diagLogCallback(Channel_Legacy,DIAG_SEVERITY_##severity,__FILE__,0,fmt,args); \
		va_end(args); \
	} \
	if (DIAG_SEVERITY_##severity == DIAG_SEVERITY_FATAL) \
	{ \
		static bool s_recursive/*=false*/; \
		if (!s_recursive) \
		{ \
			s_recursive = true; \
			diagChannel::FlushLogFile(); \
		} \
		SPIN_IF_HAS_EXCEPTION_BEEN_THROWN(); \
		__debugbreak(); \
	} \
}

#define IMPLEMENT_LEGACY_FUNCTION_DEBUGF(N) \
void Debugf##N(int level,const char *fmt,...) { 	\
	if (!(s_output && (s_OutputMask & diagOutput::OUTPUT_DISPLAYS))) return; \
	if (level >= N) \
	{ \
		if (sysIpcGetCurrentThreadId() == s_loggerThreadId) \
			return; \
		SYS_CS_SYNC(diagLogfToken); \
		va_list args; \
		va_start(args,fmt); \
		diagLogCallback(Channel_Legacy,DIAG_SEVERITY_DISPLAY,__FILE__,0,fmt,args); \
		va_end(args); \
	} \
}

}

using namespace rage;

IMPLEMENT_LEGACY_FUNCTION(Errorf,ERROR)

IMPLEMENT_LEGACY_FUNCTION(Warningf,WARNING)

IMPLEMENT_LEGACY_FUNCTION(Displayf,DISPLAY)

IMPLEMENT_LEGACY_FUNCTION_DEBUGF(1)

IMPLEMENT_LEGACY_FUNCTION_DEBUGF(2)

IMPLEMENT_LEGACY_FUNCTION_DEBUGF(3)

void Quitf(const char *fmt,...)
{
	s_output = true;

	if (!(s_output && (s_OutputMask & diagOutput::OUTPUT_FATALS))) return;
	/* Scoped here so to unlock diagLogfToken before */
	/* triggering an exception for DIAG_SEVERITY_FATAL */
	{
		SYS_CS_SYNC(diagLogfToken);
		va_list args;
		va_start(args,fmt);
		diagLogCallback(Channel_Legacy,DIAG_SEVERITY_FATAL,__FILE__,0,fmt,args);
		va_end(args);
	}

	static bool s_recursive/*=false*/;
	if (!s_recursive)
	{
		s_recursive = true;
		diagChannel::FlushLogFile();
	}
	SPIN_IF_HAS_EXCEPTION_BEEN_THROWN();
	__debugbreak();
}

void Quitf(int,const char *fmt,...)
{
	s_output = true;

	if (!(s_output && (s_OutputMask & diagOutput::OUTPUT_FATALS))) return;
	/* Scoped here so to unlock diagLogfToken before */
	/* triggering an exception for DIAG_SEVERITY_FATAL */
	{
		SYS_CS_SYNC(diagLogfToken);
		va_list args;
		va_start(args,fmt);
		diagLogCallback(Channel_Legacy,DIAG_SEVERITY_FATAL,__FILE__,0,fmt,args);
		va_end(args);
	}

	static bool s_recursive/*=false*/;
	if (!s_recursive)
	{
		s_recursive = true;
		diagChannel::FlushLogFile();
	}
	SPIN_IF_HAS_EXCEPTION_BEEN_THROWN();
	__debugbreak();
}

namespace rage {

diagChannel Channel_Assert(0,"","",DIAG_SEVERITY_DEBUG1,DIAG_SEVERITY_DISPLAY,DIAG_SEVERITY_ASSERT);

bool diagWindowClosed;

// Old-style Assert, Assertf, and AssertMsg all go through this function.
int diagAssertHelper(const char *file,int line,const char *fmt,...)
{
#if RSG_PC && !__TOOL
	if(diagWindowClosed)
		return 1;
#endif


	SYS_CS_SYNC(diagLogfToken);
	va_list args;
	va_start(args,fmt);
	bool rt = diagLogCallback(Channel_Assert,DIAG_SEVERITY_ASSERT,file,line,fmt,args);
	va_end(args);
	return rt;
}

int diagAssertHelper2(const char *file,int line,const char *msg)
{
	return diagAssertHelper(file,line,"%s",msg);
}

void diagOutput::UseVSFormatOutput(bool NOTFINAL_ONLY(flag))
{
#if !__FINAL
	if (flag)
		PARAM_usevsoutput.Set("");
	else
		PARAM_usevsoutput.Set(NULL);
#endif
}

unsigned diagOutput::GetOutputMask()
{
	return s_OutputMask;
}

void diagOutput::SetOutputMask(unsigned newMask)
{
	s_OutputMask = newMask;
}

int diagOutput::GetErrorCount()
{
	return s_LogsByType[DIAG_SEVERITY_ERROR];
}

int diagOutput::GetWarningCount()
{
	return s_LogsByType[DIAG_SEVERITY_WARNING];
}

void diagOutput::DisablePopUpErrors()
{
#if !__FINAL
	PARAM_nopopups.Set("");
#endif
}

void diagOutput::DisablePopUpQuits()
{
#if !__FINAL
	PARAM_nopopups.Set("");
#endif
}

} // namespace rage

#else		// __NO_OUTPUT

namespace rage { bool diagWindowClosed; }

#if RSG_PC
#if __RGSC_DLL
void QuitNoOutput(int /*errorCode*/)
{

}
#else
void QuitNoOutput(int errorCode)
{
	diagTerminate(static_cast<rage::FatalErrorCode>(errorCode),false);
}
#endif // __RGSC_DLL
#endif // RSG_PC

#endif		// __NO_OUTPUT

#undef SPIN_IF_HAS_EXCEPTION_BEEN_THROWN


#if __BANK
// Logging Bank

void diagChannel::InitBank()
{
	// Create the Logging bank.
	sm_pBank = &BANKMGR.CreateBank("Logging", 0, 0, false); 

	if(!Verifyf(sm_pBank, "Failed to create Logging bank!"))
		return;

	sm_pBank->AddButton("Force New Log File", &diagChannel::Bank_ForceNewLogFile);
}

void diagChannel::Bank_ForceNewLogFile()
{
	sm_startNewFile = true;
}
#endif // __BANK
