// 
// parser/restconsole.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_RESTCONSOLE_H
#define PARSER_RESTCONSOLE_H

#if __BANK

namespace rage
{
	void restAddConsoleCommand();
}

#endif // __BANK

#endif
