// 
// parser/restconsole.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "restconsole.h"

#include "restcommand.h"
#include "restservice.h"

#include "bank/console.h"
#include "data/growbuffer.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/limits.h"
#include "file/stream.h"

#if __BANK

using namespace rage;

// PURPOSE: Decodes a URI in-place
//'RETURNS: The new length of the string, or -1 on error


void RestProcessorResponse(restStatus::Enum status, restCommand command, datGrowBuffer* buf, bkConsole::OutputFunctor output)
{
	char statusBuf[32];
	formatf(statusBuf, "Status: %d\n", status);

	output(statusBuf);
	if (command.m_OutputStream)
	{
		// Make sure all data is written to the grow buffer
		command.m_OutputStream->Flush();
	}
	if (buf && buf->GetBuffer())
	{
		buf->Append("\0", 1);
		output(reinterpret_cast<char*>(buf->GetBuffer()));
	}
}

void RestProcessor(const char** argv, int argc, bkConsole::OutputFunctor output)
{
	restCommand command;

	if (argc < 2)
	{
		RestProcessorResponse(restStatus::REST_BAD_REQUEST, command, NULL, output);
		output("Usage: rest [GET|POST|PUT|DELETE] path/to/resource ...\n");
		return;
	}

	// Fill out the parts of the command. If we get an error at any point, send back a BAD_REQUEST error code
	if (!strcmp(argv[0], "GET"))
	{
		command.m_Verb = restCommand::REST_GET;
	}
	else if (!strcmp(argv[0], "POST"))
	{
		command.m_Verb = restCommand::REST_POST;
	}
	else if (!strcmp(argv[0], "PUT"))
	{
		command.m_Verb = restCommand::REST_PUT;
	}
	else if (!strcmp(argv[0], "DELETE"))
	{
		command.m_Verb = restCommand::REST_DELETE;
	}
	else
	{	
		RestProcessorResponse(restStatus::REST_BAD_REQUEST, command, NULL, output);
		return;
	}


	bool validUri = command.SetUri(argv[1]);

	if (!validUri)
	{
		RestProcessorResponse(restStatus::REST_BAD_REQUEST, command, NULL, output);
		return;
	}

	datGrowBuffer gb;
	char gbName[RAGE_MAX_PATH];
	fiDevice::MakeGrowBufferFileName(gbName, RAGE_MAX_PATH, &gb);

	command.m_OutputStream = ASSET.Create(gbName, "");

	// Merge the data back together 
	atString data;
	for(int i = 2; i < argc; i++)
	{
		data += argv[i];
		data += " ";
	}

	command.m_Data = data.c_str();


	restStatus::Enum status = REST.ProcessCommand(command);

	RestProcessorResponse(status, command, &gb, output);
}

void rage::restAddConsoleCommand()
{
	bkConsole::CommandFunctor fn;
	fn.Reset<RestProcessor>();
	bkConsole::AddCommand("rest", fn);
}

#endif // __BANK
