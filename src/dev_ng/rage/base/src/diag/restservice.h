// 
// parser/restservice.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_RESTSERVICE_H 
#define PARSER_RESTSERVICE_H 

#include "atl/array.h"
#include "atl/delegate.h"

#include "restcommand.h"

#include "diag/channel.h"

// DOM-IGNORE-BEGIN
RAGE_DECLARE_CHANNEL(REST)

#define restAssertf(cond,fmt,...)			RAGE_ASSERTF(REST,cond,fmt,##__VA_ARGS__)
#define restFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(REST,cond,fmt,##__VA_ARGS__)
#define restVerifyf(cond,fmt,...)			RAGE_VERIFYF(REST,cond,fmt,##__VA_ARGS__)
#define restErrorf(fmt,...)					RAGE_ERRORF(REST,fmt,##__VA_ARGS__)
#define restWarningf(fmt,...)				RAGE_WARNINGF(REST,fmt,##__VA_ARGS__)
#define restDisplayf(fmt,...)				RAGE_DISPLAYF(REST,fmt,##__VA_ARGS__)
#define restDebugf1(fmt,...)					RAGE_DEBUGF1(REST,fmt,##__VA_ARGS__)
#define restDebugf2(fmt,...)					RAGE_DEBUGF2(REST,fmt,##__VA_ARGS__)
#define restDebugf3(fmt,...)					RAGE_DEBUGF3(REST,fmt,##__VA_ARGS__)
#define restLogf(severity,fmt,...)			RAGE_LOGF(REST,severity,fmt,##__VA_ARGS__)
#define restCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,REST,severity,fmt,##__VA_ARGS__)
// DOM-IGNORE-END

#define RAGE_USE_WEB_SERVER (1 && (RSG_BANK /*|| RSG_TOOL*/) && (RSG_PC || RSG_XENON || RSG_PPU || RSG_DURANGO || RSG_ORBIS))

#if __BANK

namespace rage
{
	class restCommand;
	class restServiceDirectory;


	////////////////////////////////////////////////////////////////////////


	// PURPOSE: A restService is something that knows how to process restCommands sent by
	// various tools to the game. The commands identify the service by name, so each service is
	// responsible for seeing if it was the target for a command or if one of its sub-services was,
	// and then processing the command 
	class restService 
	{
	public:
		restService(const char* name);
		virtual ~restService();

		virtual restStatus::Enum ProcessCommand(restCommand& command) = 0;

		virtual const char* GetServiceType() = 0;

		const char* GetName() const {return m_Name;}

		void PrintFullName(fiStream* stream);

		// PURPOSE: When getting the directory listing for a service, the directory itself prints:
		// <Item href="svcName" serviceType="SvcType"
		//  Then we call this function and clients can add additional service details
		// Then we print "/>"
		virtual void PrintExtraDirInfo(fiStream* /*stream*/) { };

		restServiceDirectory* GetParent() {return m_Parent;}

	protected:
		friend class restServiceDirectory;

		const char*					m_Name;
		restServiceDirectory*		m_Parent;
	};



	////////////////////////////////////////////////////////////////////////


	// PURPOSE: A container for other restServices. The only native functionality it has is to list
	// its children.
	class restServiceDirectory : public restService
	{
	public:
		restServiceDirectory(const char* name);
		virtual ~restServiceDirectory();

		virtual restStatus::Enum ProcessCommand(restCommand& command);

		void AddChild(restService& svc);
		void RemoveChild(restService& svc);

		restService* FindChild(const char* name);

		int NumChildren() { return m_Children.GetCount(); }

		virtual const char* GetServiceType() { return "ServiceDirectory"; }

	protected:
		restStatus::Enum ProcessMyCommand(restCommand& command);
		restStatus::Enum ListChildren(restCommand& command);

	private:
		atArray<restService*>	m_Children;
	};


	////////////////////////////////////////////////////////////////////////


	// PURPOSE: The top level manager for rest services. 
	// TODO: Do we even need this or would a couple of free functions suffice?
	class restServiceManager
	{
	public:
		void Init();
		void Shutdown();

		void Update();

#if RAGE_USE_WEB_SERVER
		const char* GetWebServerUri();
#else
		const char* GetWebServerUri() { return ""; }
#endif

		restStatus::Enum ProcessCommand(restCommand& command);

		inline restServiceDirectory* GetRootService();

		restService* FindService(atArray<atString>& pathComponents, int numComponentsToUse);
		restService* FindService(const char* path);

		restServiceDirectory* CreateDirectories(atArray<atString>& pathComponents, int numComponentsToUse);
		restServiceDirectory* CreateDirectories(const char* path);

		void RemoveAndDeleteService(const char* path);

	private:
		restServiceDirectory* m_Root;
	};

	extern restServiceManager REST;


	////////////////////////////////////////////////////////////////////////


	// PURPOSE: A basic service with callbacks for each operation

	typedef atDelegate<restStatus::Enum (restCommand& command)> restSimpleCb;

	class restSimpleService : public restService
	{
	public:

		restSimpleService(const char* name);

		virtual restStatus::Enum ProcessCommand(restCommand& command);

		virtual const char* GetServiceType() { return "Simple"; }

		restSimpleCb m_OnGet;
		restSimpleCb m_OnPost; 
		// Can add PUT and DELETE later if necessary
	};

	restService* restRegisterSimpleService(const char* pathToService, restSimpleCb onGet, restSimpleCb onPost = restSimpleCb());


	////////////////////////////////////////////////////////////////////////


#if 0 // Just for testing - not official bank service
	class restServiceBank : public restService
	{
	public:
		restServiceBank(const char* name);
		virtual ~restServiceBank() {}

		virtual restStatus::Enum ProcessCommand(restCommand& command);
	}
#endif



	////////////////////////////////////////////////////////////////////////
	class restServiceFileAccess : public restService
	{
	public:
		restServiceFileAccess(const char* name, const char* fileRoot);
		virtual ~restServiceFileAccess();

		virtual restStatus::Enum ProcessCommand(restCommand& command);

		virtual const char* GetServiceType() { return "FileAccess"; }
	protected:
		atString m_FileRoot;
	};

	restServiceDirectory* restServiceManager::GetRootService()
	{
		return m_Root;
	}

} // namespace rage

#endif // __BANK

#endif // PARSER_RESTSERVICE_H
