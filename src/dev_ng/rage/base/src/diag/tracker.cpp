// 
// diag/tracker.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tracker.h"

#if RAGE_TRACKING

namespace rage {

// Initialize this to zero to enable breakpoints on any untracked memory allocation.
#if __WIN32PC
	__THREAD int g_TrackerDepth = 1;	// PC doesn't have tracking by default
#else // __WIN32PC
	__THREAD int g_TrackerDepth = 0;	// Everybody else does
#endif // __WIN32PC

diagTracker* diagTracker::sm_Current;

diagTracker::diagTracker() {
}


diagTracker::~diagTracker() {
}

}

#endif	// RAGE_TRACKING
