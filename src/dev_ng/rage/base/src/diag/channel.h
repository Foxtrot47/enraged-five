// 
// diag/channel.h 
// 
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DIAG_CHANNEL_H 
#define DIAG_CHANNEL_H 

#include <stdarg.h>
#include "diag/errorcodes.h"
/******************************************
Using the channel macros:

In one file that's included by all the others in your module add the following code, replacing
'grc' with your module's prefix and 'Graphics' with the channel name. If you're creating a 
subchanel, use the RAGE_DECLARE_SUBCHANNEL macro instead.


#include "diag/channel.h"

// DOM-IGNORE-BEGIN
RAGE_DECLARE_CHANNEL(Graphics)

#define grcAssertf(cond,fmt,...)					RAGE_ASSERTF(Graphics,cond,fmt,##__VA_ARGS__)
#define grcFatalAssertf(cond,fmt,...)				RAGE_FATALASSERTF(Graphics,cond,fmt,##__VA_ARGS__)
#define grcAssertBefore(year,month,day)				RAGE_ASSERT_BEFORE(Graphics,year,month,day)
#define grcAssertBeforef(year,month,day,fmt,...)	RAGE_ASSERT_BEFOREF(Graphics,year,month,day,fmt,##__VA_ARGS__)
#define grcVerifyf(cond,fmt,...)					RAGE_VERIFYF(Graphics,cond,fmt,##__VA_ARGS__)
#define grcErrorf(fmt,...)							RAGE_ERRORF(Graphics,fmt,##__VA_ARGS__)
#define grcWarningf(fmt,...)						RAGE_WARNINGF(Graphics,fmt,##__VA_ARGS__)
#define grcDisplayf(fmt,...)						RAGE_DISPLAYF(Graphics,fmt,##__VA_ARGS__)
#define grcDebugf1(fmt,...)							RAGE_DEBUGF1(Graphics,fmt,##__VA_ARGS__)
#define grcDebugf2(fmt,...)							RAGE_DEBUGF2(Graphics,fmt,##__VA_ARGS__)
#define grcDebugf3(fmt,...)							RAGE_DEBUGF3(Graphics,fmt,##__VA_ARGS__)
#define grcLogf(severity,fmt,...)					RAGE_LOGF(Graphics,severity,fmt,##__VA_ARGS__)
#define grcCondLogf(cond,severity,fmt,...)			RAGE_CONDLOGF(cond,Graphics,severity,fmt,##__VA_ARGS__)
// DOM-IGNORE-END


Then in one .cpp in your module add

#include "system/param.h"

RAGE_DEFINE_CHANNEL(Graphics)


********************************************

Converting old source code:

If you have old source code you want to channel, a batchsub script can save some time converting
your output calls over. Create a text file (say grcchannelsubs.txt) containing something like this:

Warningf            grcWarningf
Errorf              grcErrorf
Printf              grcDisplayf
Displayf            grcDisplayf
AssertMsgf          grcAssertf
Assert              grcAssertf
Assertf             grcAssertf
Verifyf             grcVerifyf
AssertMsg           grcAssertf
AssertVerify        grcVerifyf

Then run "rage\base\bin\batchsubtree.bat grcchannelsubs.txt" in the directory you want to convert over.

You may have to add some descriptions to what were formerly descriptionless Assert() and
AssertVerify() calls.

As usual for running batchsub you should look over the changes it made.

********************************************/

extern void diagTerminate();
extern void diagTerminate(rage::FatalErrorCode errorCode, bool silent);

// Temporary buffers for string assembly
enum { CHANNEL_MESSAGE_SIZE = 512 };
// Larger version which contains a bunch of additional boilerplate text (assertion text and email)
enum { CHANNEL_MESSAGE_SIZE2 = CHANNEL_MESSAGE_SIZE + 256 };

// DO NOT CHANGE! We need to decrease the size of Xenon Beta for GTA5
#define DISABLE_DEBUG_CHANNELS (__XENON && __DEV)

// Redirect TTY output to a socket? Mainly used with R*TM
#define REDIRECT_TTY (1)

#if DISABLE_DEBUG_CHANNELS
static void DisableChannel(...){}
#endif

#define RAGE_ASSERT_BEFORE_DATE(year,month,day)			(((((year)*12)+(month))*31)+(day))

namespace rage {

class fiStream;
class bkBank;

// SEVERITY_ERROR is #define in windows.h, so we need to add gratuitous prefixes.
enum diagSeverity { DIAG_SEVERITY_FATAL, DIAG_SEVERITY_ASSERT, DIAG_SEVERITY_ERROR, DIAG_SEVERITY_WARNING, DIAG_SEVERITY_DISPLAY, DIAG_SEVERITY_DEBUG1, DIAG_SEVERITY_DEBUG2, DIAG_SEVERITY_DEBUG3, DIAG_SEVERITY_COUNT };

enum diagChannelPosix
{
	CHANNEL_POSIX_UNSPECIFIED = 0,	// use default (off) or inherit parent
	CHANNEL_POSIX_OFF,				// expressly off
	CHANNEL_POSIX_ON,				// expressly on
};

#define RAGE_CAT2(a, b)     RAGE_CAT2_I(a, b)
#define RAGE_CAT2_I(a, b)   a ## b

# if __NO_OUTPUT		// __NO_OUTPUT already assumes !__ASSERT.

inline void diagUnused(const char*,...) { }

#define diagPrint(...)              do {} while(false)
#define diagLoggedPrint(...)        do {} while(false)
#define diagPrintf(...)				do {} while(false)		// This goes directly to the TTY
#define diagPrintLnf(...)           do {} while(false)      // This goes directly to the TTY
#define diagLoggedPrintf(...)		do {} while(false)		// This goes to both the TTY and any file log (for exceptions and stack backtraces)

#define RAGE_DEFINE_CHANNEL(tag,...)	// channelName, fileLevel, ttyLevel, popupLevel
#define RAGE_DECLARE_CHANNEL(tag)
#define RAGE_DEFINE_SUBCHANNEL(parent,tag,...)
#define RAGE_DECLARE_SUBCHANNEL(parent,tag)
#define RAGE_ASSERT(tag,cond)		do {} while(false)
#define RAGE_ASSERTF(tag,cond,...)  do {} while(false)
#define RAGE_FATALASSERTF(tag,cond,...)	do {} while(false)
#define RAGE_ASSERT_BEFORE(tag,year,month,day)			do {} while(false)
#define RAGE_ASSERT_BEFOREF(tag,year,month,day,fmt,...)	do {} while(false)
#define RAGE_VERIFY(tag,cond)		Likely(cond)
#define RAGE_VERIFYF(tag,cond,...)	Likely(cond)
#define RAGE_PC_FATAL_SILENT(errCode) diagTerminate(errCode, true)
#define RAGE_ERRORF(tag,...)		do {} while(false)
#define RAGE_DISPLAYF(tag,...)		do {} while(false)
#define RAGE_WARNINGF(tag,...)		do {} while(false)
#define RAGE_DEBUGF1(tag,...)		do {} while(false)
#define RAGE_DEBUGF2(tag,...)		do {} while(false)
#define RAGE_DEBUGF3(tag,...)		do {} while(false)
#define RAGE_LOGF(tag,severity,fmt,...)		do {if(severity == ::rage::DIAG_SEVERITY_FATAL) {diagTerminate();}} while(false)
#define RAGE_CONDLOGF(cond,tag,severity,fmt,...) do {if (Unlikely(severity == ::rage::DIAG_SEVERITY_FATAL && (cond))) {diagTerminate();}} while(false)

# elif __SPU

#define RAGE_DEFINE_CHANNEL(tag,...)	// channelName, fileLevel, ttyLevel, popupLevel
#define RAGE_DECLARE_CHANNEL(tag)
#define RAGE_DEFINE_SUBCHANNEL(parent,tag,...)
#define RAGE_DECLARE_SUBCHANNEL(parent,tag)
#define RAGE_ASSERT(tag,cond)		Assert(cond)
#define RAGE_ASSERTF(tag,cond,...)  Assertf(cond,##__VA_ARGS__)
#define RAGE_FATALASSERTF(tag,cond,...)	Assertf(cond,##__VA_ARGS__)
#define RAGE_ASSERT_BEFORE(tag,year,month,day)			do {} while(false)			
#define RAGE_ASSERT_BEFOREF(tag,year,month,day,fmt,...)	do {} while(false)
#define RAGE_VERIFY(tag,cond)		AssertVerify(cond)
#define RAGE_VERIFYF(tag,cond,...)	Verifyf(cond,##__VA_ARGS__)
#define RAGE_ERRORF(tag,...)		Errorf("[" #tag "] " __VA_ARGS__)
#define RAGE_DISPLAYF(tag,...)		Displayf("[" #tag "] " __VA_ARGS__)
#define RAGE_WARNINGF(tag,...)		Warningf("[" #tag "] " __VA_ARGS__)
#define RAGE_DEBUGF1(tag,...)		Displayf("[" #tag "] " __VA_ARGS__)
#define RAGE_DEBUGF2(tag,...)		Displayf("[" #tag "] " __VA_ARGS__)
#define RAGE_DEBUGF3(tag,...)		Displayf("[" #tag "] " __VA_ARGS__)
#define RAGE_LOGF(tag,severity,fmt,...)		do {Displayf("[" #tag "] " fmt, ##__VA_ARGS__); if(severity == ::rage::DIAG_SEVERITY_FATAL) {diagTerminate();}} while(false)
#define RAGE_CONDLOGF(cond,tag,severity,fmt,...) do {if (Unlikely(severity == ::rage::DIAG_SEVERITY_FATAL && (cond))) {Displayf("[" #tag "] " fmt, ##__VA_ARGS__); diagTerminate();}} while(false)

#else 

#if __ASSERT
#define RAGE_ASSERT(tag,cond)							((void)((Likely(cond)||diagLogf_Assert(RAGE_CAT2(Channel_,tag),__FILE__,__LINE__,"Assert(%s) FAILED",#cond)||(__debugbreak(),0))))
#define RAGE_ASSERTF(tag,cond,fmt,...)					((void)((Likely(cond)||diagLogf_Assert(RAGE_CAT2(Channel_,tag),__FILE__,__LINE__,"Assertf(%s) FAILED: " fmt,#cond,##__VA_ARGS__)||(__debugbreak(),0))))
#define RAGE_FATALASSERTF(tag,cond,fmt,...)				((void)((Likely(cond)||diagLogf(RAGE_CAT2(Channel_,tag),::rage::DIAG_SEVERITY_FATAL,__FILE__,__LINE__,"Assertf(%s) FAILED: " fmt,#cond,##__VA_ARGS__)||(__debugbreak(),0))))
#define RAGE_ASSERT_BEFORE(tag,year,month,day)			((void)(Likely(::diagChannel::IsSystemDateBefore(RAGE_ASSERT_BEFORE_DATE(year,month,day)))||diagLogf_Assert(RAGE_CAT2(Channel_,tag),__FILE__,__LINE__,"Assert system date before %.4d/%.2d/%.2d FAILED",year,month,day)||(__debugbreak(),0)))
#define RAGE_ASSERT_BEFOREF(tag,year,month,day,fmt,...)	((void)(Likely(::diagChannel::IsSystemDateBefore(RAGE_ASSERT_BEFORE_DATE(year,month,day)))||diagLogf_Assert(RAGE_CAT2(Channel_,tag),__FILE__,__LINE__,"Assert system date before %.4d/%.2d/%.2d FAILED.\nMessage: " fmt,year,month,day,##__VA_ARGS__)||(__debugbreak(),0)))
#define RAGE_VERIFY(tag,cond)							(Likely(cond)||((diagLogf_Assert(RAGE_CAT2(Channel_,tag),__FILE__,__LINE__,"Verify(%s) FAILED",#cond)||(__debugbreak(),0)),false))
#define RAGE_VERIFYF(tag,cond,fmt,...)					(Likely(cond)||((diagLogf_Assert(RAGE_CAT2(Channel_,tag),__FILE__,__LINE__,"Verifyf(%s) FAILED: " fmt,#cond,##__VA_ARGS__)||(__debugbreak(),0)),false))
#else
#define RAGE_ASSERT(tag,cond)							do {} while(false)
#define RAGE_ASSERTF(tag,cond,fmt,...)					do {} while(false)
#define RAGE_FATALASSERTF(tag,cond,fmt,...)				do {} while(false)
#define RAGE_ASSERT_BEFORE(tag,year,month,day)			do {} while(false)
#define RAGE_ASSERT_BEFOREF(tag,year,month,day,fmt,...)	do {} while(false)
#define RAGE_VERIFY(tag,cond)							Likely(cond)
#define RAGE_VERIFYF(tag,cond,fmt,...)					Likely(cond)
#endif

class diagChannel 
{
public:
	diagChannel(diagChannel *parent,const char *tag,const char *paramTag,diagSeverity fileLevel = DIAG_SEVERITY_DEBUG1,diagSeverity ttyLevel = DIAG_SEVERITY_DISPLAY,diagSeverity popupLevel = DIAG_SEVERITY_ASSERT,diagChannelPosix posixPrefix = CHANNEL_POSIX_UNSPECIFIED);
	u8 FileLevel,		// Highest severity that will still be logged to a file (default is SEVERITY_DEBUG1) (usually <= TtyLevel)
		TtyLevel,		// Highest severity that will still be logged to tty (default is SEVERITY_DISPLAY) (usually <= PopupLevel)
		PopupLevel,		// Highest severity that will still result in a popup message (default is SEVERITY_ERROR)
		MaxLevel;		// Largest of FileLevel, TtyLevel, and PopupLevel, which we use to cull the message entirely (and its formatf overhead)
	u8 ParentCount;	// Number of parents (could easily be u8)
	u8 OriginalFileLevel;   //stores the original file severity level when setting an override
	u8 OriginalTtyLevel;   //stores the original tty severity level when setting an override
	diagChannelPosix PosixPrefix;
#if __BANK
	static void InitWidgets(bkBank &bank);
	static void WidgetCallback(void *channel);
	static void InitOutputWindows();
	static void SendAllSubChannels();

	static void InitBank();
	static void Bank_ForceNewLogFile();
#endif
#if __ASSERT
	static void ClearIgnoredAsserts();
	static int DetectBlockedPopups(bool detectAll);
#endif 
	static void InitClass();
	static void InitArgs();
	const char *Tag, *ParamTag;
	bool HasParent(diagChannel *parent) const;

	static bool OpenLogFile(const char *name);
	static void CloseLogFile();
	static int GetLogFileSize();
	static void SetLogFileMaxSize(u32 megabytes);
	static const char* GetLogFileName();
	static int GetLogFileNumber();
	static bool FlushLogFile(u32 timeoutMs=1000);
	static void StartNewLogFile();
	// Add a function to call when a new log file is opened. Don't do any actual logging inside the callback because
	// it will be called from within the logger thread.
	static bool AddNewFileCallback(void (*newFileCallback)());

#if REDIRECT_TTY
	static bool RedirectOutput(const char *ipaddrPort, u32 timeoutMs);
#endif

	static void SetOutput(bool enable);
	static bool GetOutput();

	static void SetPosixTime(u64 posixTime);
	static u64 GetPosixTime();

	static diagChannel ALL;		// Magic parent channel for everything.
	diagChannel *GetParent() const { return Parent; }
	diagChannel *GetNext() const { return Next; }

	static diagChannel *GetFirst() { return First; }

	// PURPOSE
	//	Control whether or not the current thread's name/channel is prefixed to the output.
	//	Intended for use in controlled situations where the thread & channel information is
	//	presented in some other manner and where the prefix would interfere with the output.
	// RETURNS
	//	The previous value.
	static bool UseThreadPrefix(bool mode) { bool old = sm_UseThreadPrefix ; sm_UseThreadPrefix = mode; return old; }
	static bool GetUseThreadPrefix() { return sm_UseThreadPrefix; }

	static bool IsSystemDateBefore( int date ) { return sm_systemAssertDate < date; }

	void overrideSeverity(diagSeverity fileOverrideLevel, diagSeverity ttyOverrideLevel);
	void resetSeverity();
	static bool TryLockDiagLogfToken();
	static void UnlockDiagLogfToken();
private:
	void UpdateMaxLevel();
	void AddWidgets(bkBank&);
#if __BANK
	void SendSubChannels();

	static bkBank* sm_pBank;
#endif
	diagChannel *Parent;
	diagChannel *Next;
	static diagChannel *First;
	static bool sm_UseThreadPrefix;
	static bool sm_startNewFile;
	static int	sm_systemAssertDate;
	static void LoggerThread(void*);
	static void InitSystemDateAssert();
};

extern const char *diagBuildString;		// Expected to be overridden by higher-level code

// User callback; should call diagLoggedPrintf to display any additional context depending on supplied severity.
// Should return true to allow default handler to display a popup if necessary.  Callback could display a dialog box
// itself instead and return false.  It can also adjust the severity up or down to force execution to break.
// This is a simpler alternative to replacing diagLogDefault (below) entirely.
extern bool (*diagExternalCallback)(const diagChannel&,diagSeverity&,const char*,int,const char*);

// User callback; prints the current call stack. The default simply calls sysStack::PrintStackTrace, but applications
// that require more specialized call stack logging can override this to format the output as needed or turn it off entirely,
// which might be useful if the stack data is displayed as part of other diagnostic data shown by the app specific
// error handlers.
extern void (*diagPrintStackTrace)();

// The default function diag functions use to dump the stack (diagPrintStackTrace refers to this by default).
extern void diagDefaultPrintStackTrace();

// User callback: return the game frame.
extern unsigned (*diagGetGameFrame)();

// User callback; set to be notified of long execution times in diagLogDefault
extern void (*diagLongExecutionCallback)(const unsigned, const unsigned, const unsigned, const char*, const char*);

//Simple, basic Print variants.  There's no limit on string length.
extern void diagPrint(const char* buffer);
extern void diagPrintLn(const char* buffer);
extern void diagPrint(const char* buffer, const int bufLen);
extern void diagPrintLn(const char* buffer, const int bufLen);
extern void diagLoggedPrint(const char* buffer);
extern void diagLoggedPrintLn(const char* buffer);
extern void diagLoggedPrint(const char* buffer, const int bufLen);
extern void diagLoggedPrintLn(const char* buffer, const int bufLen);

// This is what diagLogf uses to get text to the tty.  This is also what stack traceback code ultimately uses
// so we avoid all sorts of recursion madness when trying to do tracebacks during tracebacks.
// Low-level string output formatter (use by diagLogf itself, and stack traceback code)
extern void diagPrintf(const char *fmt,...) PRINTF_LIKE_N(1);
extern void diagPrintLnf(const char *fmt,...) PRINTF_LIKE_N(1);
extern void diagLoggedPrintf(const char *fmt,...) PRINTF_LIKE_N(1);
extern void diagPrintDefault(const char *msg);
extern void diagPrintFlush();
extern void (*diagPrintCallback)(const char *msg);

// This returns an adjusted system time for channel output logging, so timestamps always start at 0
extern unsigned diagGetAdjustedSystemTime();

// Returns false on abort, else true (so that RAGE_ASSERTF and RAGE_VERIFYF can halt on the actual line of the error)
extern bool diagLogf(const diagChannel &channel,diagSeverity severity,const char *file,int line,const char *fmt,...) PRINTF_LIKE_N(5);
extern bool diagLogf_Assert(const diagChannel &channel,const char *file,int line,const char *fmt,...) PRINTF_LIKE_N(4);
extern void diagLogf2(const diagChannel &channel,diagSeverity severity,const char *fmt,...) PRINTF_LIKE_N(3);
extern void diagErrorf(const diagChannel &channel,const char *fmt,...) PRINTF_LIKE_N(2);
extern void diagWarningf(const diagChannel &channel,const char *fmt,...) PRINTF_LIKE_N(2);
extern void diagDisplayf(const diagChannel &channel,const char *fmt,...) PRINTF_LIKE_N(2);
extern bool diagLogDefault(const diagChannel &channel,diagSeverity severity,const char *file,int line,const char *fmt, va_list args);
typedef bool (*diagLogCallbackType)(const diagChannel &channel,diagSeverity severity,const char *file,int line,const char *fmt, va_list args);
extern diagLogCallbackType diagLogCallback;

typedef void (*diagLongExecutionCallbackType)(const unsigned, const unsigned, const unsigned, const char*, const char*);
extern void diagSetLongExecutionCallback(diagLongExecutionCallbackType callback);

// Callback for logging a bug
extern int (*diagLogBugCallback)(const char* summary,const char* details,u32 assertId,int defaultIfNoServer);

#define diagVerifyfHelper(cond,channel,fmt,...)				(Likely(cond)||((diagLogf_Assert(channel,__FILE__,__LINE__,"Verifyf(%s) FAILED: " fmt,#cond,##__VA_ARGS__)||(__debugbreak(),0)),false))
#define diagAssertfHelper(cond,channel,fmt,...)				((void)((Likely(cond)||diagLogf_Assert(channel,__FILE__,__LINE__,"Assertf(%s) FAILED: " fmt,#cond,##__VA_ARGS__)||(__debugbreak(),0))))
#define diagCondLogfHelper(cond,channel,severity,fmt,...)	do { if (cond) {diagLogf2(channel,severity,fmt,##__VA_ARGS__);} } while (0)
#define diagLogfHelper(channel,severity,fmt,...)			diagCondLogfHelper((severity) <= (channel).MaxLevel,channel,severity,fmt,##__VA_ARGS__)

#define RAGE_DEFINE_CHANNEL(tag,...)			::rage::diagChannel Channel_##tag(&::rage::diagChannel::ALL,"[" #tag "] ", #tag, ## __VA_ARGS__);
#define RAGE_DECLARE_CHANNEL(tag)				extern ::rage::diagChannel Channel_##tag;
#define RAGE_DEFINE_SUBCHANNEL(parent,tag,...)	::rage::diagChannel Channel_##parent##_##tag(&Channel_##parent,"[" #parent "_" #tag "] ", #parent "_" #tag, ## __VA_ARGS__);
#define RAGE_DECLARE_SUBCHANNEL(parent,tag)		extern ::rage::diagChannel Channel_##parent##_##tag;

/*
    The definition of RAGE_LOG_FMT can be overridden to include more information
    in channel output.  For example, when added to a cpp file the following will
    prepend extra debug info to all channel output:

    #undef RAGE_LOG_FMT
    #define RAGE_LOG_FMT(fmt)\
        "%s: "fmt, this->GetExtraDebugInfo()
*/

#define RAGE_ERRORF(tag,fmt,...)		diagErrorf(RAGE_CAT2(Channel_,tag),RAGE_LOG_FMT(fmt),##__VA_ARGS__)
#define RAGE_WARNINGF(tag,fmt,...)		diagWarningf(RAGE_CAT2(Channel_,tag),RAGE_LOG_FMT(fmt),##__VA_ARGS__)
#define RAGE_DISPLAYF(tag,fmt,...)		diagDisplayf(RAGE_CAT2(Channel_,tag),RAGE_LOG_FMT(fmt),##__VA_ARGS__)
#if DISABLE_DEBUG_CHANNELS
#define RAGE_DEBUGF1(tag,...)			DisableChannel(__VA_ARGS__)
#define RAGE_DEBUGF2(tag,...)			DisableChannel(__VA_ARGS__)
#define RAGE_DEBUGF3(tag,...)			DisableChannel(__VA_ARGS__)
#else
#define RAGE_DEBUGF1(tag,fmt,...)		diagLogfHelper(RAGE_CAT2(Channel_,tag),::rage::DIAG_SEVERITY_DEBUG1,RAGE_LOG_FMT(fmt),##__VA_ARGS__)
#define RAGE_DEBUGF2(tag,fmt,...)		diagLogfHelper(RAGE_CAT2(Channel_,tag),::rage::DIAG_SEVERITY_DEBUG2,RAGE_LOG_FMT(fmt),##__VA_ARGS__)
#define RAGE_DEBUGF3(tag,fmt,...)		diagLogfHelper(RAGE_CAT2(Channel_,tag),::rage::DIAG_SEVERITY_DEBUG3,RAGE_LOG_FMT(fmt),##__VA_ARGS__)
#endif
#define RAGE_LOGF(tag,severity,fmt,...)	diagLogfHelper(RAGE_CAT2(Channel_,tag),severity,RAGE_LOG_FMT(fmt),##__VA_ARGS__)
#define RAGE_CONDLOGF(cond,tag,severity,fmt,...) \
										diagCondLogfHelper((cond) && ((severity) <= Channel_##tag.MaxLevel),RAGE_CAT2(Channel_,tag),severity,RAGE_LOG_FMT(fmt),##__VA_ARGS__)
#define RAGE_PC_FATAL_SILENT(errCode)	diagTerminate(errCode, true)
#endif	// !__NO_OUTPUT

#if RSG_PC
// User callback; notify the game that we are going to terminate abnormally.
extern void (*diagTerminateCallback)(const char* data);
#endif //RSG_PC

//These are "override-able" macros in that they can be overridden to write
//to a configurable channel.
//
//Within a .cpp file define __rage_channel to be the name of the channel
//to which these macros should write.
//
//For example:
//
//  RAGE_DECLARE_CHANNEL(rline);
//  RAGE_DEFINE_SUBCHANNEL(rline, session)
//  #define __rage_channel rline_session
//
//All of the following macros will then write to the rline_session channel.
//
//*** DO NOT define __rage_channel in a .h file ***
//
#define rageDebugf1(fmt, ...)   RAGE_DEBUGF1(__rage_channel, fmt, ##__VA_ARGS__)
#define rageDebugf2(fmt, ...)   RAGE_DEBUGF2(__rage_channel, fmt, ##__VA_ARGS__)
#define rageDebugf3(fmt, ...)   RAGE_DEBUGF3(__rage_channel, fmt, ##__VA_ARGS__)
#define rageDisplayf(fmt, ...)  RAGE_DISPLAYF(__rage_channel, fmt, ##__VA_ARGS__)
#define rageWarningf(fmt, ...)  RAGE_WARNINGF(__rage_channel, fmt, ##__VA_ARGS__)
#define rageErrorf(fmt, ...)    RAGE_ERRORF(__rage_channel, fmt, ##__VA_ARGS__)
#define rageAssert(cond)        RAGE_ASSERT(__rage_channel, cond)
#define rageVerify(cond)        RAGE_VERIFY(__rage_channel, cond)
#define rageAssertf(cond, fmt, ...)     RAGE_ASSERTF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#define rageVerifyf(cond, fmt, ...)     RAGE_VERIFYF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#define rageLogf(severity, fmt, ...)    RAGE_LOGF(__rage_channel, severity, fmt, ##__VA_ARGS__)
#define rageCondLogf(cond, severity, fmt, ...)  RAGE_CONDLOGF(__rage_channel, cond, severity, fmt, ##__VA_ARGS__)

} // namespace rage

#endif // DIAG_CHANNEL_H 

//This is deliberately outside of the #include guards
//to support unity builds.  We're effectively resetting
//the definition of RAGE_LOG_FMT after every cpp file is compiled.
#ifdef RAGE_LOG_FMT
#undef RAGE_LOG_FMT
#endif
#define RAGE_LOG_FMT_DEFAULT(fmt)   fmt
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT
