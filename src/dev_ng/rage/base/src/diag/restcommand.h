// 
// parser/restcommand.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_RESTCOMMAND_H 
#define PARSER_RESTCOMMAND_H 

#include "atl/array.h"
#include "atl/string.h"
#include "parsercore/attribute.h"

//#if __BANK

namespace rage
{
	class fiStream;

	// PURPOSE: This is a subset of the HTTP status codes, based on what our four verbs can return
	namespace restStatus
	{
		enum Enum
		{
			REST_OK					= 200,	// Generic 'OK' status
			REST_CREATED			= 201,	// The new resource specified by the URI was created
			REST_OK_NO_CONTENT		= 204,	// 'OK', nothing needs to be returned
			REST_BAD_REQUEST		= 400,	// The URI or the new data were badly formed
            REST_FORBIDDEN          = 403,  // The operation you attempted is not allowed
			REST_NOT_FOUND			= 404,	// The URI points to a non-existent resource
			REST_METHOD_NOT_ALLOWED	= 405,	// The indicated resource doesn't support this operation
			REST_INTERNAL_ERROR		= 500,	// Some error happened on the game side.

			// Special purpose error codes - normally restServices won't respond with these
			REST_LENGTH_REQUIRED	= 411,  // The request needs to specify a Content-Length
		};

		const char* GetStatusName(Enum e);

		inline bool IsOk(Enum e) { return ((int)e / 100) == 2; }
		inline bool IsClientError(Enum e) { return ((int)e / 100) == 4; }
		inline bool IsServerError(Enum e) { return ((int)e / 100) == 5; }
	}

	class restCommand
	{
	public:
		// PURPOSE: These are all the actions that a restCommand can represent.
		// NOTES: The verbs are taken from the description of REST-over-http.  
		// See here: http://en.wikipedia.org/wiki/Representational_State_Transfer
		// Each service can decide what these specific verbs actually _do_ to the 
		// resource that's identified with the URI, but the actions need to follow
		// a few constraints, listed with each verb below.
		// Note too, "idempotent" roughly means the same command can be applied >1 
		// times in a row without altering the game state after the first time.
		// These enums use individual bits for convenience functions (easy to test if a 
		// request is in the right set of verbs) but any given command only has
		// one single verb.
		enum Verb
		{
			REST_GET		= BIT(0),	// Queries data, does not alter game state
			REST_POST		= BIT(1),	// Modifies data.
			REST_PUT		= BIT(2),	// Modifies data. Idempotent. 
			REST_DELETE		= BIT(3),	// Deletes data. Idempotent.
		};

		restCommand();

		bool Init(Verb verb, const char* uri, fiStream* outputStream, const char* data = NULL);

		// PURPOSE: Gets the 'current' path component that a service should be examining, or an offset from the current one.
		inline const char*	GetCurrentPathComponent(int offset = 0);

		// RETURNS: True if the current path component is one off the end of the list (don't 'Get' it!)
		inline bool			IsAtEndOfPath();

		// PURPOSE: Moves to the next path component
		inline void			NextPathComponent() { m_UriIndex ++; }

		// PURPOSE: Sets the URI and updates the URI components, URI index, and m_Query based on the original URI
		bool				SetUri(const char* newUri);

		// PURPOSE: Helper method to report that only some verbs are allowed
		void				ReportErrorMethodNotAllowed(int allowed = 0, const char* message = "");

		void				ReportError(const char* message = "");

		// PURPOSE: The action this command should take
		Verb				m_Verb;

		// PURPOSE: The original (unparsed) URI, possibly including a scheme name ("http://") and a query ("...?start=3&stop=10")
		const char*			m_OriginalUri;

		// PURPOSE: A NULL terminated UTF8 encoded block of data that could represent arguments to the command
		const char*			m_Data;

		// PURPOSE: The hierarchical components of the URI. This may include the scheme name ("http:") but not the query ("...?start=3&stop=10")
		atArray<atString>	m_UriComponents;

		// PURPOSE: The 'current' position in the URI Components array
		int					m_UriIndex;

		// PURPOSE: All the query parameters, split up into name=value pairs. 
		parAttributeList	m_Query;

		// PURPOSE: A stream to write any output to. 
		fiStream*			m_OutputStream;

		// PURPOSE: The mime type describing the format your response is in.
		// NOTES:
		//		Common choices might be:
		//		text/plain - generic ASCII text
		//		text/html - HTML that should be rendered by the browser
		//		application/xml - generic XML
		//		image/jpeg - a JPEG
		//		application/vnd.rockstargames.<foo>+xml - a custom XML-based file format
		//		application/atom+xml - an AtomPub document (a "feed" of some sort)
		//		application/octet-stream - arbitrary binary data
		//		application/json - Javascript Object Notation data.
		const char*			m_OutputType;
	};


	const char* restCommand::GetCurrentPathComponent( int offset /*= 0*/ )
	{
		return m_UriComponents[m_UriIndex + offset].c_str();
	}

	bool restCommand::IsAtEndOfPath()
	{
		return m_UriIndex == m_UriComponents.GetCount();
	}

}

//#endif // __BANK

#endif // PARSER_RESTCOMMAND_H
