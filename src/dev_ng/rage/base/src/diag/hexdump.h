// 
// diag/hexdump.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DIAG_HEXDUMP_H
#define DIAG_HEXDUMP_H

// This code is designed to be directly included where necessary.
// The reason is because you may need a different "output" function
// depending on whether it's thread-safe or not.
#ifndef Printf
#include "output.h"
#endif

namespace rage {

//	PURPOSE
//		Prints a block of data as hexadecimal values
//	PARAMS
//		v - Points to the block of data to write out
//		ct - The number of bytes to write
//	NOTES
//		Output format is:
//		00000001: 12 34 56 78 90 ab cd ef 12 34 56 78 90 ab cd ef .4Vx.....4Vx....
// <FLAG Component>
static void diagHexDump(const void *v,int ct,bool addr = false) {
        unsigned char *b = (unsigned char*)v;

	int i;
	int j;

        for (i=0; i<ct; i+=16) {
                int pl = (ct - i); if (pl > 16) pl = 16;
				if (addr)
					Printf("%p: ",b+i);
				else
					Printf("%08x: ",i);
                for (j=0; j<pl; j++)
                        Printf("%02x ",b[i+j]);
                for (;j<16; j++)
                        Printf("   ");
                for (j=0; j<pl; j++)
                        Printf("%c",b[i+j]>=32&&b[i+j]<127?b[i+j]:'.');
                for (;j<16; j++)
                        Printf(" ");
                Printf("\n");
        }
}

}	// namespace rage

#endif // DIAG_HEXDUMP_H
