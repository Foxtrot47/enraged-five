//
// diag/output.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DIAG_OUTPUT_H
#define DIAG_OUTPUT_H

#include <stddef.h>

namespace rage {

#ifndef BIT
#define BIT(n)	(1<<(n))
#endif

#define TBlack		"\033[30m"  // Black Text
#define TRed		"\033[31m"  // Red Text
#define TGreen		"\033[32m"  // Green Text
#define TYellow		"\033[33m"  // Yellow Text
#define TBlue		"\033[34m"  // Blue Text
#define TPurple		"\033[35m"  // Purple Text
#define TCyan		"\033[36m"  // Cyan Text

#define TIRed		"\033[41m"  // Inversed Red Text
#define TIGreen		"\033[42m\033[30m"  // Inversed Green Text
#define TIYellow	"\033[43m\033[30m"  // Inversed Yellow Text
#define TIBlue		"\033[44m"  // Inversed Blue Text
#define TIPurple	"\033[45m"  // Inversed Purple Text
#define TICyan		"\033[46m"  // Inversed Cyan Text

#define TNorm		"\033[0m"		// Normal Text

#define BOOL_TO_STRING(condition) ((condition) ? "T" : "F")

//PURPOSE
//  Evaluates to the name of the current function, analogous to
//  the way __FILE__ evaluates to the name of the current file.
//  Most (but not all) compilers define this.
#if !defined(__FUNCTION__)
# if defined(__FUNC__)
#  define __FUNCTION__      __FUNC__
# elif defined(__PRETTY_FUNCTION__)
#  define __FUNCTION__      __PRETTY_FUNCTION__
# elif defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901)
#  define __FUNCTION__      __func__
# elif defined(__GNUC__)
#  define __FUNCTION__      __PRETTY_FUNCTION__
# else
#  define __FUNCTION__      "*unknown function*"
# endif
#endif

class diagOutput
{
public:
	static void UseVSFormatOutput(bool = true);
	static void DisablePopUpQuits();
	static void DisablePopUpErrors();
	enum { OUTPUT_FATALS = BIT(0), OUTPUT_ERRORS = BIT(1), OUTPUT_WARNINGS = BIT(2), OUTPUT_DISPLAYS = BIT(3),
			OUTPUT_ALL = (OUTPUT_FATALS|OUTPUT_ERRORS|OUTPUT_WARNINGS|OUTPUT_DISPLAYS)};
	static void SetOutputMask(unsigned mask);
	static unsigned GetOutputMask();
	static int GetWarningCount();
	static int GetErrorCount();
};

enum { OUTPUT_FATAL, OUTPUT_ERROR, OUTPUT_WARNING, OUTPUT_DISPLAY };		// These shadow DIAG_SEVERITY_... definitions.

#if __BANK

class diagContextMessage
{
public:
	diagContextMessage(const char*);
	diagContextMessage(const char*,const char*);
	diagContextMessage(const char*,const char*,const char*);
	diagContextMessage(const char*,const char *(*callback)(void *user1,void *user2),void *user1,void *user2);
	static void DisplayContext(const char *prefix);
	static const char *GetTopMessage(char *msgBuf,size_t msgBufSize);
	static void PerThreadShutdown();
	~diagContextMessage();

	static void Push(const char *fmt,const char *a1,const char *a2,const char* (*cb)(void*,void*));
	static void Pop();
};

#define DIAG_CONTEXT_MESSAGE(fmt,...)	rage::diagContextMessage mess(fmt,##__VA_ARGS__)
#define DIAG_CONTEXT_CLEANUP()			rage::diagContextMessage::PerThreadShutdown()

#else	// __NO_OUTPUT

#define DIAG_CONTEXT_MESSAGE(fmt,...)
#define DIAG_CONTEXT_CLEANUP()

#endif  //__NO_OUTPUT

} // namespace rage

#endif
