#ifndef DIAG_TRACKER_REMOTE_2_H
#define DIAG_TRACKER_REMOTE_2_H

#include "tracker.h"
#include "system/criticalsection.h"
#include "atl/array.h"
#include "system/messagequeue.h"

#if RAGE_TRACKING

namespace rage 
{

class diagTrackerRemote2 : public diagTracker 
{
public:
	enum
	{
		PROTOCOL_VERSION = 9,
	};

	struct ClientInfo
	{
		u32 m_ProtocolVersion;
		char m_SymbolFile[256];
		char m_Platform[32];			// Platform string, such as Xbox 360, PS3, etc
		char m_Configuration[32];		// Configuration type, such as Debug, BankRelease, etc
		u16 m_PointerSize;				// sizeof(void *) - 4 or 8
		char m_PlatformIdentifier;		// This is g_sysPlatform
		u64 m_MainSize;					// Durango has a (size_t) main offset for proper symbol lookup
		char m_Pad;

		ClientInfo()
		{
			sysMemSet(this, 0, sizeof(ClientInfo));
		}
	};

	struct VersionInfo
	{
		char m_BuildVersion[32];		// Build version ("v297.1", etc)
		char m_PackageType[32];			// Package type (disc build, installremote, etc)

		VersionInfo()
		{
			m_BuildVersion[0] = 0;
			m_PackageType[0] = 0;
		}
	};

    diagTrackerRemote2(const bool startWorker, int customPort = 0);
    ~diagTrackerRemote2();

    virtual void Push(const char*);
    virtual void Pop();
    virtual void Tally(void*,size_t,int);	// used by new, etc
    virtual bool UnTally(void*,size_t);		// used by delete
    virtual void AddInfo(void*,size_t,const char*,const char*,int);    // used by macro new
    virtual void Report(const char *tag,size_t); 
	virtual void TrackDeallocations( bool enable );
	virtual void SetTargetPlatform( const char* platform );
	
	// Flags
	virtual void MarkFlags(void *addr, int flags, bool value);
	virtual void MarkDefragmentable(void *addr, bool value) {MarkFlags(addr, MEMBUCKETFLAG_CAN_DEFRAG, value);}
	virtual void MarkLocked(void *addr, bool value) {MarkFlags(addr, MEMBUCKETFLAG_LOCKED, value);}
	virtual void MarkExternal(void *addr, bool value) {MarkFlags(addr, MEMBUCKETFLAG_EXTERNAL, value);}
	virtual void MarkMoved(void *addr, bool value) {MarkFlags(addr, MEMBUCKETFLAG_MOVED, value);}
	
	virtual void FullReport(const char * filename);
	virtual void InitHeap(const char* name, void* begin, size_t size);

    virtual void Update();  // compresses and sends buffered messages
    void RequestDump();
	virtual void SetVersionInfo(const char *buildVersion, const char *packageType);

	void StartNetworkWorker();

private:
	static void WorkerStub(void* param);
	void Worker();
	void Send(int packetType, const void* data, int dataSize, bool dynamic = false);
	void Flush();

	enum 
	{
#if RSG_ORBIS
		NUM_BUFFERS = 3, // 2 buffers filled during init causing deadlock, bumped to 3 on Orbis
#elif RSG_CPU_X64        // PC and Durango need 4 buffers
		NUM_BUFFERS = 4,
#else
		NUM_BUFFERS = 3,
#endif
		BUF_SIZE = 100000
	};
	typedef atFixedArray<u32, BUF_SIZE>	Buffer;
	int										m_CustomPort;
	Buffer									m_Buffer[NUM_BUFFERS];
	Buffer*									m_CurBuf;
	sysCriticalSectionToken					m_Cs;
	sysMessageQueue<Buffer*, NUM_BUFFERS>	m_Empty;
	sysMessageQueue<Buffer*, NUM_BUFFERS>	m_Full;
};

} // namespace rage

#endif	// RAGE_TRACKING

#endif // DIAG_TRACKER_REMOTE_2_H
