#include "tracker_remote2.h"

#if RAGE_TRACKING
#include "math/amath.h"
#include "file/tcpip.h"
#include "file/winsock.h"
#include "string/string.h"
#include "system/bootmgr.h"
#include "system/endian.h"
#include "system/memops.h"
#include "system/stack.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/platform.h"
#include "system/timer.h"
#include "system/memory.h"
#include "system/platform.h"
#include <string.h>
#if __PS3
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#elif __XENON
#include "system/xtl.h"
#include <xbdm.h>
#else
#include "system/xtl.h"
#endif

#define USE_DEJA_STYLE_BROADCAST (0) 

XPARAM(symfilepath);

#if RSG_DURANGO
extern "C" int main();
#elif RSG_ORBIS
extern int main(int argc,char **argv);
#elif RSG_PC
extern "C" IMAGE_DOS_HEADER __ImageBase;
#endif


namespace rage {

enum memPacketType
{
	MV_INIT = 1,
	MV_NEWHEAP,
	MV_ALLOC,
	MV_FREE,
	MV_PUSH,
	MV_POP,
	MV_FRAME,
	MV_REQUESTDUMP,
	MV_ADD_FLAGS,
	MV_CLEAR_FLAGS,
	MV_FULL_REPORT,
	MV_VERSION_INFO,
};

diagTrackerRemote2::diagTrackerRemote2(const bool startWorker, int customPort)
{
	m_CustomPort = customPort;

	if ( startWorker )
		StartNetworkWorker();

	ClientInfo clientInfo;

#if RSG_DURANGO || RSG_ORBIS
	clientInfo.m_MainSize = (size_t) main;
#elif RSG_PC
	clientInfo.m_MainSize = (size_t) &__ImageBase;
#endif

	m_CurBuf = 0;
	for(int i=0; i<NELEM(m_Buffer); ++i)
		m_Empty.Push(&m_Buffer[i]);

	clientInfo.m_ProtocolVersion = sysEndian::NtoL((u32) PROTOCOL_VERSION);
#if __PS3 || RSG_ORBIS
	char *ext;
	safecpy(clientInfo.m_SymbolFile,*getargv());
	ext = strrchr(clientInfo.m_SymbolFile,'.');
	if (ext)
		strcpy(ext,".cmp");
#elif __XENON
	DM_XBE xbe;
	DmGetXbeInfo(NULL, &xbe);
	safecpy(clientInfo.m_SymbolFile, xbe.LaunchPath);
	char* ext = strrchr(clientInfo.m_SymbolFile,'.');
	if (ext)
		strcpy(ext,".cmp");
#elif RSG_DURANGO
	char *cmdLine = GetCommandLine();

	if (cmdLine)
	{
		if (*cmdLine == '\"')
			cmdLine++;

		if (*cmdLine == 'G')
			cmdLine += 3;

		// HACK: Until we upload the Durango *.cmp file to the devkit
		sprintf(clientInfo.m_SymbolFile, "%%RS_CODEBRANCH%%\\game\\VS_Project\\Durango\\Layout\\Image\\Loose\\%s", cmdLine);

		// Get only the first argument, i.e. the exe name.
		if (strchr(clientInfo.m_SymbolFile, ' '))
			*strchr(clientInfo.m_SymbolFile, ' ') = 0;

		if (strrchr(clientInfo.m_SymbolFile,'.'))
			strcpy(strrchr(clientInfo.m_SymbolFile,'.'),".cmp");
		else
			strcat(clientInfo.m_SymbolFile,".cmp");
	}

#elif __WIN32PC // __XENON
	// sysStack::InitClass() hasn't been called yet, we'll have to figure out the
	// location of the symbol file ourselves.
//	safecpy(clientInfo.m_SymbolFile, sysStack::GetSymbolFilename());
	const char *pCustomSymFilePath = 0;

	if (PARAM_symfilepath.Get(pCustomSymFilePath))
	{
		safecpy(clientInfo.m_SymbolFile, pCustomSymFilePath);
	}
	else
	{
		const char *cmdLine = GetCommandLine();

		if (cmdLine)
		{
			if (*cmdLine == '\"')
				cmdLine++;

			safecpy(clientInfo.m_SymbolFile, cmdLine);

			// Get only the first argument, i.e. the exe name.
			if (strchr(clientInfo.m_SymbolFile, ' '))
			{
				*strchr(clientInfo.m_SymbolFile, ' ') = 0;
			}

			if (strrchr(clientInfo.m_SymbolFile,'.'))
				strcpy(strrchr(clientInfo.m_SymbolFile,'.'),".cmp");
			else
				strcat(clientInfo.m_SymbolFile,".cmp");
		}
	}
#endif // !__XENON

	// TODO: We should really have a place somewhere in RAGE that gives us those strings.
#if !__OPTIMIZED
	safecpy(clientInfo.m_Configuration, "Debug");
#elif __DEV
	safecpy(clientInfo.m_Configuration, "Beta");
#elif __BANK
	safecpy(clientInfo.m_Configuration, "BankRelease");
#else
	safecpy(clientInfo.m_Configuration, "Release");
#endif

#if __XENON
	safecpy(clientInfo.m_Platform, "Xbox 360");
#elif __PS3
	safecpy(clientInfo.m_Platform, "PS3");
#elif __PSP2
	safecpy(clientInfo.m_Platform, "PSP2");
#elif RSG_ORBIS
	safecpy(clientInfo.m_Platform, "PS4");
#elif RSG_DURANGO
	safecpy(clientInfo.m_Platform, "XBox One");
#elif __64BIT
	safecpy(clientInfo.m_Platform, "x64");
#else
	safecpy(clientInfo.m_Platform, "Win32");
#endif

	clientInfo.m_PointerSize = sysEndian::NtoL((u16) sizeof(void *));

#if RSG_DURANGO
	clientInfo.m_PlatformIdentifier = platform::DURANGO;
#else
	clientInfo.m_PlatformIdentifier = g_sysPlatform;
#endif	

	Send(MV_INIT, &clientInfo, sizeof(clientInfo));
}

void diagTrackerRemote2::StartNetworkWorker()
{
	sysIpcCreateThread(WorkerStub, this, 4096, PRIO_LOWEST, "diagTrackerRemote2");
}

void diagTrackerRemote2::WorkerStub(void* param)
{
	static_cast<diagTrackerRemote2*>(param)->Worker();
}

void diagTrackerRemote2::Worker()
{
	int port = (m_CustomPort) ? m_CustomPort : 8798;

	fiHandle handle = fiHandleInvalid;
	bool isGoodConnection = true;
	while (isGoodConnection)
	{
		Buffer& buf = *m_Full.Pop();	
		if (!fiIsValidHandle(handle))
		{
			Displayf("Connecting to Memory Tracker port %d...", port);
			InitWinSock();

#if USE_DEJA_STYLE_BROADCAST
			// was playing around with connecting the same way as deja, i.e. braodcast
			// a UDP packet to the whole subnet and wait for the tracker to connect to us.
			// it works but it's a bit messy
			fiHandle listener = fiDeviceTcpIp::Listen(port, 1);
			struct sockaddr_in si_other;
			int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			memset(&si_other, 0, sizeof(si_other));
			si_other.sin_family = AF_INET;
			si_other.sin_port = htons((u16)port);
			si_other.sin_addr.s_addr = htonl(INADDR_BROADCAST);
			int val = 1;
			setsockopt(s, SOL_SOCKET, SO_BROADCAST, (char*)&val, sizeof(val));
#if __XENON
			u_long nonblocking = 1;
			XSocketIOCTLSocket((SOCKET)listener, FIONBIO, &nonblocking);
#elif __WIN32
			u_long nonblocking = 1;
			ioctlsocket((SOCKET)listener, FIONBIO, &nonblocking);
#else // !__WIN32
			setsockopt((int)listener, SOL_SOCKET, SO_NBIO, (char*)&val, sizeof(val));
#endif // !__WIN32
			char* msg = "memtracker";
			for(int retry = 0; retry < 10; ++retry)
			{
				sendto(s, msg, strlen(msg)+1, 0, (sockaddr*)&si_other, sizeof(si_other));
				handle = fiDeviceTcpIp::Pickup(listener);
				if (fiIsValidHandle(handle))
					break;
				sysIpcSleep(100);
			} 
#else // USE_DEJA_STYLE_BROADCAST

#if __WIN32PC
			// PC currently has inconsistent behavior - it needs NULL to get the socket
			// MemVisualize is using; GetLocalHost() would return the loopback device, which
			// is not the one MemVisualize binds to.
			const char *localHost = NULL;
#else // __WIN32PC
			const char *localHost = fiDeviceTcpIp::GetLocalHost();
#endif // __WIN32PC

			for(int retry = 0; retry < 10; ++retry)
			{
				handle = fiDeviceTcpIp::Connect(localHost, port);
				if (fiIsValidHandle(handle))
					break;
				sysIpcSleep(100);
			} 
#endif // USE_DEJA_STYLE_BROADCAST
			if (fiIsValidHandle(handle))
			{
				Displayf("Connected.");
			}
			else
			{
				Errorf("Unable to connect.");
				diagTracker::SetCurrent(0);
				//break;
			}
		}
		if (fiIsValidHandle(handle))
			isGoodConnection = fiDeviceTcpIp::GetInstance().SafeWrite(handle, buf.GetElements(), buf.GetCount() * 4);
		buf.Reset();
		m_Empty.Push(&buf);
	}

	Errorf("Aborted connection to memory tracker - discarding all subsequent packages");

	// Keep flushing the buffers so the main thread doesn't stall.
	while (true)
	{
		Buffer& buf = *m_Full.Pop();	
		buf.Reset();
		m_Empty.Push(&buf);
	}
}
  
diagTrackerRemote2::~diagTrackerRemote2()
{
	ShutdownWinSock();
}

struct memPacketInitHeap
{
	size_t m_Begin, m_Size;
	char m_Name[32];
};
void diagTrackerRemote2::InitHeap(const char* name, void* begin, size_t size)
{
	memPacketInitHeap heap;
	heap.m_Begin = sysEndian::NtoL((size_t)begin);
	heap.m_Size  = sysEndian::NtoL((size_t)size);
	sysMemCpy(heap.m_Name, name, 32);
	Send(MV_NEWHEAP, &heap, sizeof(memPacketInitHeap));
}

#define STACK_NAME_SIZE 63 

struct sStackEntry
{
	char m_Name[STACK_NAME_SIZE];
	u8   m_CallstackLevel;
};

static __THREAD int sm_SentStackPos = 0;
static __THREAD int sm_StackPos = 0;
static __THREAD sStackEntry sm_Stack[64];

static int CaptureStack(size_t * buf, u32 count)
{
	size_t stack[128];
	sysStack::CaptureStackTrace(stack, 128, 2);
	int i;
	for(i=0; i<128 && stack[i]; ++i) {}
	u32 level = i;
	if (level > count)
		level = count;
	for(--i; i >= 0 && count; --i, --count)
		*buf++ = sysEndian::NtoL(stack[i]);
	for(; count; --count)
		*buf++ = 0;
	return level;
}

void diagTrackerRemote2::Push(const char* name)
{
	Assert(sm_StackPos < 64);
	size_t temp[128];
	sm_Stack[sm_StackPos].m_CallstackLevel = (u8)CaptureStack(temp, 128);
		
	if (!name)
		name = "NULL";

	size_t size = strlen(name); 
	size = Min((size_t)(STACK_NAME_SIZE - 1), size); // Avoid buffer overruns, Warning : this could truncate names
	sysMemCpy(sm_Stack[sm_StackPos].m_Name, name, size);
	sm_Stack[sm_StackPos].m_Name[size] = 0;
	sm_StackPos++;
}

void diagTrackerRemote2::Pop()
{
	Assert(sm_StackPos > 0);
	if (sm_SentStackPos > --sm_StackPos)
	{
		--sm_SentStackPos;
		Send(MV_POP, 0, 0);
	}
}

struct memPacketAlloc
{
	size_t m_Begin, m_Size;
	u32 m_Bucket;
	u32 m_Pad;
	size_t m_Callstack[128];
};
void diagTrackerRemote2::Tally(void* addr, size_t size, int)
{
	memPacketAlloc alloc;
	alloc.m_Begin = sysEndian::NtoL((size_t)addr);
	alloc.m_Size = sysEndian::NtoL((size_t)size);
	alloc.m_Bucket = sysEndian::NtoL((u32)sysMemCurrentMemoryBucket);
	u32 depth = rage::Min(128,CaptureStack(alloc.m_Callstack, 128));	

	for(int& s = sm_SentStackPos ;s < sm_StackPos; ++s)
		Send(MV_PUSH, &sm_Stack[s], sizeof(sStackEntry));
	Send(MV_ALLOC, &alloc, sizeof(size_t) * (2 + depth) + (sizeof(u32) * 2), true); // DYNAMIC
}

struct memPacketFree
{
	size_t m_Begin;
	size_t m_Callstack[128];
};
bool diagTrackerRemote2::UnTally(void* addr, size_t)
{
	memPacketFree free;
	free.m_Begin = sysEndian::NtoL((size_t)addr);
	u32 depth = rage::Min(128,CaptureStack(free.m_Callstack, 128));
	Send(MV_FREE, &free, sizeof(size_t) * (1 + depth), true); // DYNAMIC
	return true;
}

struct memPacketMarkFlags
{
	size_t m_Begin, m_Flags;	
};

void diagTrackerRemote2::MarkFlags(void *addr, int flags, bool value)
{
	memPacketMarkFlags packet;

	packet.m_Begin = sysEndian::NtoL((size_t)addr);
	packet.m_Flags = sysEndian::NtoL((size_t)flags);
	Send(value ? MV_ADD_FLAGS : MV_CLEAR_FLAGS, &packet, sizeof(packet));
}

void diagTrackerRemote2::FullReport(const char *filename)
{
	char fullFilename[RAGE_MAX_PATH];
	safecpy(fullFilename, filename);

	Send(MV_FULL_REPORT, fullFilename, sizeof(fullFilename));
}

void diagTrackerRemote2::AddInfo(void*,size_t,const char*,const char*,int) {}
void diagTrackerRemote2::Report(const char* /*tag*/,size_t) {}
void diagTrackerRemote2::TrackDeallocations( bool /*enable*/ ) {}
void diagTrackerRemote2::SetTargetPlatform( const char* /*platform*/ ) {}

void diagTrackerRemote2::Update()
{
	Send(MV_FRAME, 0, 0);
	Flush();
}

void diagTrackerRemote2::RequestDump()
{
	Send(MV_REQUESTDUMP,0,0);
}

void diagTrackerRemote2::Flush()
{
	SYS_CS_SYNC(m_Cs);
	if (m_CurBuf)
	{
		m_Full.Push(m_CurBuf);
		m_CurBuf = 0;
	}
}

void diagTrackerRemote2::SetVersionInfo(const char *buildVersion, const char *packageType)
{
	VersionInfo versionInfo;

	safecpy(versionInfo.m_BuildVersion, buildVersion);
	safecpy(versionInfo.m_PackageType, packageType);

	Send(MV_VERSION_INFO, &versionInfo, sizeof(VersionInfo));
}

void diagTrackerRemote2::Send(int packetType, const void* data, int dataSize, bool dynamic)
{
	FastAssert(!(dataSize&3));
	dataSize >>= 2;
	u32 header = sysEndian::NtoL(packetType|(((u32)(size_t)sysIpcGetCurrentThreadId())<<8));
	SYS_CS_SYNC(m_Cs);
	if (m_CurBuf && m_CurBuf->GetCount() + 1 + int(dynamic) + dataSize > m_CurBuf->GetMaxCount())
		Flush();

	if (!m_CurBuf)
	{
		HANG_DETECT_SAVEZONE_ENTER();
		static const unsigned WARN_EVERY_N_SECONDS = 5;
		unsigned stall = 0;
		while (!m_Empty.PopWait(m_CurBuf, WARN_EVERY_N_SECONDS*1000))
		{
			stall += WARN_EVERY_N_SECONDS;
			Warningf("Stalled %u seconds waiting for Memvisualize network output buffer space", stall);
		}
		HANG_DETECT_SAVEZONE_EXIT("diagTrackerRemote2::Send");
	}

	m_CurBuf->Push(header);
	const u32* src = static_cast<const u32*>(data);
	const u32 size = sysEndian::NtoL((u32)dataSize);
	if (dynamic) m_CurBuf->Push(size);
	while (dataSize--)
		m_CurBuf->Push(*src++);
}

} // namespace rage

#endif // !__FINAL
