// 
// diag/pedantic.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "pedantic.h"

#include "system/param.h"

namespace rage {

// Default pedantic value is 0 on __DEV builds, 1 on !__DEV builds, and 2 on __FINAL builds.
// int diagPedantic = !__DEV + __FINAL;

}
