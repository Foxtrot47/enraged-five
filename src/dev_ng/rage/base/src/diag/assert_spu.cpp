// 
// diag/assert_spu.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include <stdio.h>
#include <stdlib.h>

namespace rage {

static char s_AssertText[128];

int AssertText(const char *fmt,...)
{
	va_list args;
	va_start(args,fmt);
	vsnprintf(s_AssertText,sizeof(s_AssertText),fmt,args);
	va_end(args);
	return 0;
}

int AssertFailed(const char * exp, const char * file,int line) 
{
	if (s_AssertText[0])
		Errorf("%s",s_AssertText);
	Quitf("Assert(%s) failed in [%s, line %d].",exp,file,line);
	return 0;
}

}	// namespace rage
