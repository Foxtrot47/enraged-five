// email.cpp 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 

#if __WIN32PC

#include "email.h"
#include "string/string.h"
#include "system/param.h"
#include "system/xtl.h"
#include <stdio.h>

namespace rage
{

#define EMAIL_TEMP_FILENAME "c:\\_rageemailtemp%d.txt"
#define EMAIL_LOG_FILENAME  "c:\\_rageemaillog.txt"

PARAM(smtpserver, "[diag] SMTP server to use when emailing");
PARAM(emailerpath, "[diag] Path to rageemailer.exe");

bool 
SendEmail(const char* sender, 
          const char* recipient, 
          const char* subject, 
          const char* text)
{
    Assert(sender && recipient && subject && text);

    //Note that we create random filenames so processes emailing simultaneously
    //don't trash each others' tempfiles.  (This has actually occurred.)
    char filename[64];
    formatf(filename, sizeof(filename), EMAIL_TEMP_FILENAME, rand());

    FILE* f = fopen(filename, "w");

    if(!f)
    {
        return false;
    }

    fwrite(text, strlen(text), 1, f);

    fclose(f);

    bool result = SendEmailUsingFile(sender, recipient, subject, filename);

    char buf[256];
    formatf(buf, sizeof(buf), "del %s", filename);
    system(buf);

    return result;
}

bool 
SendEmailUsingFile(const char* sender, 
                   const char* recipient, 
                   const char* subject, 
                   const char* filename)
{
    Assert(sender && recipient && subject && filename);

    const char* smtpserver = 0;
    PARAM_smtpserver.Get(smtpserver);

    bool useServer = (smtpserver && strlen(smtpserver));

    const char* emailerpath = 0;
    PARAM_emailerpath.Get(emailerpath);

    unsigned pathLen = emailerpath ? (unsigned) strlen(emailerpath) : 0;
    char cmdline[512];

    if(pathLen)
    {
		bool pathEndsInSlash = ((emailerpath[pathLen]-1) == '/') || 
			((emailerpath[pathLen]-1) == '\\');

        formatf(cmdline, sizeof(cmdline),
                "%s%srageEmailer -to %s -from %s -subject \"%s\" -body %s %s%s >> " EMAIL_LOG_FILENAME, 
                emailerpath,
                pathEndsInSlash ? "" : "/",
                recipient, 
                sender, 
                subject,
                filename,
                useServer ? "-server " : "",
                useServer ? smtpserver : "");
    }
    else
    {
        formatf(cmdline, sizeof(cmdline),
                "rageEmailer -to %s -from %s -subject \"%s\" -body %s %s%s >> " EMAIL_LOG_FILENAME, 
                recipient, 
                sender, 
                subject,
                filename,
                useServer ? "-server " : "",
                useServer ? smtpserver : "");
    }

    system(cmdline);
    
    return true;
}

bool 
SendSystemEmail(const char* sender,
                const char* recipient,
                const char* subject, 
                const char* text)
{
    Assert(recipient && subject && text);

    //Open temp file for email
    char filename[64];
    formatf(filename, sizeof(filename), EMAIL_TEMP_FILENAME, rand());

    FILE* f = fopen(filename, "w");

    if(!f)
    {
        return false;
    }

    bool isRunningAsService = (0 == getenv("USERNAME"));

    char buf[1024]; 
    memset(buf, sizeof(buf), 0);

    //Write HTML header
    formatf(buf, sizeof(buf), "<html><head><title>%s</title></head><body><font face=\"Arial\" size=2><h1>%s</h1><p>\n", subject, subject);
	fwrite(buf, strlen(buf), 1, f);

    //Write program info to aid debugging
    formatf(buf, sizeof(buf), "<table><tr width=100% bgcolor=#FFEEEE><td>\n");
	fwrite(buf, strlen(buf), 1, f);
	formatf(buf, sizeof(buf), "<tr width=100%><td bgcolor=#EEFFEE>Computer</td><td bgcolor=#EEEEFF>%s</td></tr>\n", getenv("COMPUTERNAME"));
	fwrite(buf, strlen(buf), 1, f);

    if(!isRunningAsService)
    {
        formatf(buf, sizeof(buf), "<tr width=100%><td bgcolor=#EEFFEE>Username</td><td bgcolor=#EEEEFF>%s</td></tr>\n", getenv("USERNAME"));
	    fwrite(buf, strlen(buf), 1, f);
    }

	formatf(buf, sizeof(buf), "<tr width=100%><td bgcolor=#EEFFEE>Command Line</td><td bgcolor=#EEEEFF>\n");
	fwrite(buf, strlen(buf), 1, f);
	for(int i=0; i<sysParam::GetArgCount(); i++)
	{
		formatf(buf, sizeof(buf), " %s", sysParam::GetArg(i));
		fwrite(buf, strlen(buf), 1, f);
	}

    //NOTE: Windows services run out of the WINDOWS\System directory, which 
    //      isn't much use displaying and can sometimes cause odd errors.
    if(!isRunningAsService)
    {
	    formatf(buf, sizeof(buf), "<tr width=100%><td bgcolor=#EEFFEE>File Info</td><td bgcolor=#EEEEFF><pre>\n");
	    fwrite(buf, strlen(buf), 1, f);
        fclose(f);

	    formatf(buf, sizeof(buf), "dir %s >> %s", sysParam::GetProgramName(), filename);
	    system(buf);

	    f = fopen(filename, "a");
	    formatf(buf, sizeof(buf), "</pre></td></tr>\n");
	    fwrite(buf, strlen(buf), 1, f);
    }

    formatf(buf, sizeof(buf), "</table><p>\n");
	fwrite(buf, strlen(buf), 1, f);

    //Write body text
	formatf(buf, sizeof(buf), "<table><tr width=100%><td bgcolor=#F0F0F0><pre>\n");
	fwrite(buf, strlen(buf), 1, f);

	fwrite(text, strlen(text), 1, f);

	formatf(buf, sizeof(buf), "</pre></td></tr></table><p>\n");
	fwrite(buf,strlen(buf), 1, f);

	fclose(f);

    //Send the mail
    bool result = SendEmailUsingFile(sender, recipient, subject, filename);

    //Delete the temp file
    formatf(buf, sizeof(buf), "del %s", filename);
    system(buf);

    return result;
}

};

#endif //__WIN32PC
