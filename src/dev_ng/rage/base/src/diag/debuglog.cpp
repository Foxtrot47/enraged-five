//
// diag/debuglog.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "debuglog.h"

#if __DEBUGLOG
#include "output.h"
#include "assert.h"

#include "file/stream.h"
#include "math/amath.h"
#include "system/memops.h"
#include "system/param.h"
#include "system/stack.h"
#include "system/timer.h"

using namespace rage;

PARAM(debuglog, "file to write the debuglog to");
PARAM(debuglogslow, "enable slow debug logging");
PARAM(debuglogmask, "enable slow debug logging");

namespace rage 
{

// A file to write debug output to
__THREAD fiStream *gDebugLogFile = NULL;
__THREAD int gDebugLogDisabledCount = 0;
u32 gDebugLogMask = 0xFFFFFFFF;

static const int sStackDepth = 63; // must be less than 256 bytes
static u32 sLastStack[sStackDepth + 1];

static int sDataBytes = 0;
static int sStackBytes = 0;
static int sHeaderBytes = 0;
static int sErrorsReported = 0;

static bool sbSwab = false;

static float sCPUTime = 0;
static float sIOTime = 0;

// Remembers the first parameter of diagDebugLogInit
static bool sDebugReplay;
static bool sbForceFatal = false;

// The name of the log to open in diagDebugLogInit
#if __XENON
static const char s_dbName[] = "game:\\debug.log";
#else
static const char s_dbName[] = "t:\\debug.log";
#endif


static void diagDumpStackTraceback(u32 *stack,int stored) {

	sysStack::OpenSymbolFile();

	for (int i = stored - 1; i >= 0; i--)
	{
		char buf[128];
		u32 addr = stack[i];
		if (sbSwab)
			addr = ((addr >> 24) | ((addr >> 8) & 0xFF00) | ((addr << 8) & 0xFF0000) | (addr << 24));
		int offset = sysStack::ParseMapFileSymbol(buf,128,addr);
		Displayf("%02d. %x %s+%d",i,addr,buf,offset);
	}

	sysStack::CloseSymbolFile();
}


void diagDebugLogInit(bool bReplay, bool abortOnFailure) {
	diagDebugLogShutdown();

	gDebugLogMask = PARAM_debuglogslow.Get() ? 0xFFFFFFFF : (diagDebugLogPhysics | diagDebugLogAmbient | diagDebugLogTrigger | diagDebugLogVehicle | diagDebugLogAI | diagDebugLogMisc );
	PARAM_debuglogmask.Get(gDebugLogMask);

	const char* file = s_dbName;
	PARAM_debuglog.Get(file);

	if (!gDebugLogFile)
		gDebugLogFile = bReplay ? fiStream::Open(file) : fiStream::Create(file);
	memset(sLastStack, 0, sizeof(sLastStack));
	sDebugReplay = bReplay;
	if (abortOnFailure && !gDebugLogFile)
		Quitf("Unable to open debug log file");
	if (sDebugReplay)
		sErrorsReported = 0;
	else
		sStackBytes = sDataBytes = sHeaderBytes = 0;

	if (gDebugLogFile)
	{
		static const u32 magic = 'DDLI', revmagic = 'ILDD';
		u32 tmp;

		if (sDebugReplay)
		{
			gDebugLogFile->Read(&tmp, 4);
			if (tmp == revmagic)
				sbSwab = true;
			else 
				Assert(tmp == magic);
		}
		else
		{
			gDebugLogFile->Write(&magic, 4);
		}
	}
}


void diagDebugLogShutdown() {
	if (gDebugLogFile) {
		gDebugLogFile->Close();
		if (!sDebugReplay)
			Printf("diagDebugLog wrote %dk (header %dk, data %dk, stack %dk, CPU time %.2fs, IO time %.2fs)\n", 
			(sStackBytes + sDataBytes + sHeaderBytes) / 1024,
			sHeaderBytes / 1024, sDataBytes / 1024, sStackBytes / 1024,
			sCPUTime / 1000.f, sIOTime / 1000.f);
		gDebugLogFile = 0;
	}
}


static void diagHexDump(const void *v,int ct) {
	unsigned char *b = (unsigned char*)v;

	for (int i=0; i<ct; i+=16) {
		int pl = (ct - i); if (pl > 16) pl = 16;
		Printf("%08x: ",i);
		int j;
		for (j=0; j<pl; j++)
			Printf("%02x ",b[i+j]);
		for (;j<16; j++)
			Printf("   ");
		for (j=0; j<pl; j++)
			Printf("%c",b[i+j]>=32&&b[i+j]<127?b[i+j]:'.');
		for (;j<16; j++)
			Printf(" ");
		Printf("\n");
	}
}


static void diagFloatDump(const void *v,int ct) {
	float *f = (float*)v;

	ct/=sizeof(float);
	for (int i=0; i<ct; i+=3) {
		int pl = (ct - i); if (pl > 3) pl = 3;
		Printf("%08x: ",i*4);
		int j;
		for (j=0; j<pl; j++)
		{
			u32 tmp;
			sysMemCpy(&tmp, &f[i+j], sizeof(float));
			if (sbSwab)
				tmp = ((tmp >> 24) | ((tmp >> 8) & 0xFF00) | ((tmp << 8) & 0xFF0000) | (tmp << 24));
			Printf("%f ",IntToFloatBitwise(tmp));
		}
		for (;j<(int)sizeof(float); j++)
			Printf("   ");
		Printf("\n");
	}
}

struct diagDebugLogHeader
{
	int ftag;
	u16 flen;
	u8 fstackCommon, fstackLength;
};

void diagDebugLogRaw(int tag, const void* data, int len, bool fatal, u32* stack, int stackLength)
{
	sysTimer timer;

	if (sDebugReplay) {
		diagDebugLogHeader head;

		int offset = gDebugLogFile->Tell();

		char fbuffer[256];
		gDebugLogFile->Read(&head,sizeof(head));
		Assert(head.flen < (int)sizeof(fbuffer));

		gDebugLogFile->Read(fbuffer,head.flen);
		gDebugLogFile->Read((char*)(sLastStack) + head.fstackCommon, (head.fstackLength << 2) - head.fstackCommon);

		if (stackLength == head.fstackLength && tag == head.ftag && len == head.flen && 
			memcmp(data,fbuffer,len) == 0 && memcmp(stack,sLastStack,(head.fstackLength << 2)) == 0)
			return;

		Errorf("DEBUGLOG MISMATCH");

		for(int i = 0;i<4;i++)
		{
			if (((char *)&tag)[i]==0)
				((char *)&tag)[i]=32;
			if (((char *)&head.ftag)[i]==0)
				((char *)&head.ftag)[i]=32;
		}

		if (tag == head.ftag)
		{
#if __BE
			Displayf("\nTAG: (%c%c%c%c) OFFSET: %d", ((char *)&head.ftag)[0], ((char *)&head.ftag)[1], ((char *)&head.ftag)[2], ((char *)&head.ftag)[3], offset);
#else
			Displayf("\nTAG: (%c%c%c%c) OFFSET: %d", ((char *)&head.ftag)[3], ((char *)&head.ftag)[2], ((char *)&head.ftag)[1], ((char *)&head.ftag)[0], offset);
#endif
		}
		else
		{
#if __BE
		Displayf("\nTAG: (%c%c%c%c) FTAG: (%c%c%c%c) OFFSET: %d", 
			((char *)&tag)[0], ((char *)&tag)[1], ((char *)&tag)[2], ((char *)&tag)[3], 
			((char *)&head.ftag)[0], ((char *)&head.ftag)[1], ((char *)&head.ftag)[2], ((char *)&head.ftag)[3], 
			offset);
#else
		Displayf("\nTAG: (%c%c%c%c) FTAG: (%c%c%c%c) OFFSET: %d", 
			((char *)&tag)[3], ((char *)&tag)[2], ((char *)&tag)[1], ((char *)&tag)[0], 
			((char *)&head.ftag)[3], ((char *)&head.ftag)[2], ((char *)&head.ftag)[1], ((char *)&head.ftag)[0], 
			offset);
#endif
		}

		if (len == head.flen && !memcmp(data,fbuffer,len))
		{
			Displayf("\nDATA: (%d bytes)", len); 
			diagHexDump(data,len); 
			diagFloatDump(data,len);
		}
		else
		{
			Displayf("\nCURRENT DATA: (%d bytes)", len); 
			diagHexDump(data,len); 
			diagFloatDump(data,len);

			Displayf("\nSTORED DATA: (%d bytes)", head.flen); 
			diagHexDump(fbuffer,head.flen);
			diagFloatDump(fbuffer,head.flen);
		}

		if (stackLength == head.fstackLength && !memcmp(stack,sLastStack,(head.fstackLength << 2)))
		{ 
			Displayf("\nSTACK (%d entries):", stackLength); 
			diagDumpStackTraceback(stack,stackLength); 
		}
		else
		{
			Displayf("\nCURRENT STACK (%d entries):", stackLength); 
			diagDumpStackTraceback(stack,stackLength); 
			Displayf("\nSTORED STACK: (%d entries)", head.fstackLength);
			diagDumpStackTraceback(sLastStack,head.fstackLength); 
		}

		Printf("\n");

		if(fatal || sbForceFatal) {
			Assert("diagDebugLog mismatch, aborting." && 0);
			if (++sErrorsReported == 5)
				diagDebugLogShutdown();
		}
		else {
			Displayf("diagDebugLog mismatch, continuing.");
		}
	}
	else {
		// count number of repeat entries from last stack
		int stackCommon = 0;
		for ( ; stackCommon < stackLength && stack[stackCommon] == sLastStack[stackCommon]; stackCommon++ ) { }

		u32 finalEntry = stack[stackCommon];
		u32 lastFinalEntry = sLastStack[stackCommon];

		// count number of repeat bytes in final stack entry
		if (stackCommon < stackLength)
		{
			// change units to bytes
			stackCommon <<= 2;

			if ((finalEntry & 0xFF000000) == (lastFinalEntry & 0xFF000000)) stackCommon++;
			if ((finalEntry & 0xFFFF0000) == (lastFinalEntry & 0xFFFF0000)) stackCommon++;
			if ((finalEntry & 0xFFFFFF00) == (lastFinalEntry & 0xFFFFFF00)) stackCommon++;
		}
		else
		{
			// change units to bytes
			stackCommon <<= 2;
		}

		diagDebugLogHeader head;
		head.ftag = tag;
		Assign(head.flen, len);
		Assign(head.fstackCommon, stackCommon);
		Assign(head.fstackLength, stackLength);

		sCPUTime += timer.GetMsTime();
		timer.Reset();
		gDebugLogFile->Write(&head,sizeof(head));
		gDebugLogFile->Write(data,len);
		gDebugLogFile->Write((char*)stack + stackCommon, (stackLength << 2) - stackCommon);
		sIOTime += timer.GetMsTime();
		timer.Reset();

		sHeaderBytes += sizeof(head);
		sDataBytes += len;
		sStackBytes += (stackLength << 2) - stackCommon;

		sysMemCpy((char*)sLastStack + stackCommon, (char*)stack + stackCommon, (stackLength << 2) - stackCommon);
		sCPUTime += timer.GetMsTime();
	}
}

void diagDebugLogRaw(int tag, const void * data, int len, bool fatal, int extraStackSkip) 
{
	if (!gDebugLogFile || gDebugLogDisabledCount)
		return;

	sysTimer timer;

	u32 stack[sStackDepth + 1];
	sysStack::CaptureStackTrace(stack, sStackDepth + 1, extraStackSkip + 2);
	int stackLength = 0;
	while (stack[stackLength])
		stackLength++;

	// reverse stack
	for (int i = 0; i < (stackLength >> 1); i++)
	{
		int tmp = stack[i];
		stack[i] = stack[stackLength - i - 1];
		stack[stackLength - i - 1] = tmp;
	}

	sCPUTime += timer.GetMsTime();

	diagDebugLogRaw(tag, data, len, fatal, stack, stackLength);
}

void diagDebugLogSPUBuffer(diagDebugLogSubsystem subsys, const char* addr, int size)
{
	if (!diagDebugLogIsActive(subsys))
		return;

	diagDebugLogRaw('SPsz', &size, sizeof(size));

	while (size > 5)
	{
		int tag = 0;
		u32 stack = 0;
		u8 shortLen;

		for(int i = 0; i < 4; i++)
			((char *)&tag)[i] = *addr++, size--;
		shortLen = *addr++, size--;

		Assert(shortLen <= size);

		diagDebugLogRaw(tag, addr, shortLen, true, &stack, 0);

		addr += shortLen, size -= shortLen;
	}

	Assert(size == 0);
}

void diagDebugLogDump(int start, int stop, bool callstacks)
{
	if (!gDebugLogFile)
		return;

	int offset;

	while ((offset = gDebugLogFile->Tell()) <= stop)
	{
		diagDebugLogHeader head;

		char fbuffer[256];
		if (gDebugLogFile->Read(&head,sizeof(head)) != sizeof(head))
			return;

		if (sbSwab)
		{
			head.ftag = ((head.ftag >> 24) | ((head.ftag >> 8) & 0xFF00) | ((head.ftag << 8) & 0xFF0000) | (head.ftag << 24));
			head.flen = ((head.flen >> 8) | (head.flen<< 8));
		}

		Assert(head.flen < (int)sizeof(fbuffer));

		if (gDebugLogFile->Read(fbuffer,head.flen) != head.flen)
			return;
		if (gDebugLogFile->Read((char*)(sLastStack) + head.fstackCommon, (head.fstackLength << 2) - head.fstackCommon) != (head.fstackLength << 2) - head.fstackCommon)
			return;

		if (offset < start)
			continue;

		for(int i = 0;i<4;i++)
			if (((char *)&head.ftag)[i]==0)
				((char *)&head.ftag)[i]=32;

#if __BE
		Displayf("\nTAG: (%c%c%c%c) OFFSET: %d", ((char *)&head.ftag)[0], ((char *)&head.ftag)[1], ((char *)&head.ftag)[2], ((char *)&head.ftag)[3], offset);
#else
		Displayf("\nTAG: (%c%c%c%c) OFFSET: %d", ((char *)&head.ftag)[3], ((char *)&head.ftag)[2], ((char *)&head.ftag)[1], ((char *)&head.ftag)[0], offset);
#endif

		Displayf("\nDATA: (%d bytes)", head.flen); 
		diagHexDump(fbuffer,head.flen);
		diagFloatDump(fbuffer,head.flen);

		if (callstacks)
		{
			Displayf("\nSTACK: (%d entries)", head.fstackLength);
			diagDumpStackTraceback(sLastStack,head.fstackLength); 
		}

		Printf("\n");
	}
}

} // namespace rage

#endif	// __DEBUGLOG




#if __DEBUGLOG_SPU

namespace rage
{
	
char *gDebugLogFile = 0;
bool gDebugLogDisabled = false;

u16 gDebugLogMaxSize = 0;
u16 *gDebugLogCurrentSize = 0;


void diagDebugLogRaw (int tag, const void * data, int len, bool UNUSED_PARAM(fatal), int UNUSED_PARAM(extraStackSkip))
{
	//Printf("SPUDEBUGLOG tag (%c%c%c%c) len %d\n", ((char *)&tag)[0], ((char *)&tag)[1], ((char *)&tag)[2], ((char *)&tag)[3], len);

	if (gDebugLogFile == 0 || gDebugLogDisabled || gDebugLogCurrentSize == 0)
		return;

	for(int i = 0; i < 4; i++)
		if (((char *)&tag)[i] == 0)
			((char *)&tag)[i] = 32;

	u8 shortLen = u8(len);
	if (shortLen != len)
	{
		Errorf("SPUDEBUGLOG data is larger than max size: %d > 255 tag (%c%c%c%c)", len,
			((char *)&tag)[0], ((char *)&tag)[1], ((char *)&tag)[2], ((char *)&tag)[3]);
		return;
	}

	if (*gDebugLogCurrentSize >= gDebugLogMaxSize || u32(gDebugLogMaxSize - *gDebugLogCurrentSize) < sizeof(tag) + sizeof(shortLen) + shortLen)
	{
		Errorf("SPUDEBUGLOG out of space: %u + %" SIZETFMT "u > %u tag (%c%c%c%c)",
			*gDebugLogCurrentSize, sizeof(tag) + sizeof(u8) + len, gDebugLogMaxSize,
			((char *)&tag)[0], ((char *)&tag)[1], ((char *)&tag)[2], ((char *)&tag)[3]);
		return;
	}

	for(int i = 0; i < 4; i++)
		gDebugLogFile[(*gDebugLogCurrentSize)++] = ((char *)&tag)[i];
	gDebugLogFile[(*gDebugLogCurrentSize)++] = shortLen;
	for(int i = 0; i < shortLen; i++)
		gDebugLogFile[(*gDebugLogCurrentSize)++] = ((char *)data)[i];
}


void diagDebugLogSPUInit(char* buffer, u16* startSize, u16 maxSize)
{
	gDebugLogFile = buffer;
	gDebugLogMaxSize = maxSize;
	gDebugLogCurrentSize = startSize;
	//Printf("SPUDEBUGLOG start: %p %u %u\n", gDebugLogFile, gDebugLogMaxSize, gDebugLogCurrentSize ? *gDebugLogCurrentSize : 0);
}


void diagDebugLogSPUShutdown()
{
	//Printf("SPUDEBUGLOG stop: %p %u %u\n", gDebugLogFile, gDebugLogMaxSize, gDebugLogCurrentSize ? *gDebugLogCurrentSize : 0);
	gDebugLogFile = 0;
	gDebugLogCurrentSize = 0;
}


} // namespace rage

#endif // __DEBUGLOG_SPU




#if __DEBUGLOG || __DEBUGLOG_SPU

#include "vector/matrix34.h"

namespace rage
{

void diagDebugLogRaw (int tag, const Matrix34 * data, bool fatal)
{
	float packed[12];

	packed[0] = data->a.x;
	packed[1] = data->a.y;
	packed[2] = data->a.z;
	packed[3] = data->b.x;
	packed[4] = data->b.y;
	packed[5] = data->b.z;
	packed[6] = data->c.x;
	packed[7] = data->c.y;
	packed[8] = data->c.z;
	packed[9] = data->d.x;
	packed[10] = data->d.y;
	packed[11] = data->d.z;

	diagDebugLogRaw(tag,packed,sizeof(packed),fatal,1);
}

} // namespace rage

#endif // __DEBUGLOG || __DEBUGLOG_SPU
