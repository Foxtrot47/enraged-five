#if __WIN32PC
#include "diag/diagerrorcodes.h"
#include "diag/errorcodes.h"
#include "string/stringhash.h"
#include "file/asset.h"
#include "system/param.h"
#include "system/service.h"

#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(error: 4668)

using namespace rage;

enum ErrorCodeFileLineType
{
	LINE_TYPE_DEFAULT	= 0,
	LINE_TYPE_LABEL,
	LINE_TYPE_TEXT,
	LINE_TYPE_MAX
};

bool diagErrorCodes::m_bLoaded = false;
bool diagErrorCodes::m_bLoading = false;
bool diagErrorCodes::m_bShowExtraReturnCodeNumberInHex = true;
sysLanguage	diagErrorCodes::m_currentLanguage = LANGUAGE_ENGLISH;
s32 diagErrorCodes::m_ExtraReturnCodeNumber = 0;
atMap<u32, wchar_t*> diagErrorCodes::m_ErrorCodes;
atMap<u32, wchar_t*> diagErrorCodes::m_ErrorMessages;

wchar_t russianError[] = {0x0022,0x041d,0x0435,0x0438,0x0441,0x043f,0x0440,0x0430,0x0432,0x0438,0x043c,0x0430,0x044f,0x0020,0x043e,0x0448,0x0438,0x0431,0x043a,0x0430,0x0020,0x002d,0x0020,0x041f,0x043e,0x0436,0x0430,0x043b,0x0443,0x0439,0x0441,0x0442,0x0430,0x002c,0x0020,0x043f,0x0435,0x0440,0x0435,0x0437,0x0430,0x043f,0x0443,0x0441,0x0442,0x0438,0x0442,0x0435,0x0020,0x0438,0x0433,0x0440,0x0443,0x0022, 0x0000};
wchar_t japaneseError[] = {0x56de,0x5fa9,0x4e0d,0x80fd,0x306a,0x30a8,0x30e9,0x30fc,0xff1a,0x30b2,0x30fc,0x30e0,0x3092,0x518d,0x8d77,0x52d5,0x3057,0x3066,0x304f,0x3060,0x3055,0x3044,0x3002,0x0000};

#if !__TOOL && !__RESOURCECOMPILER
NOSTRIP_PC_PARAM(language, "Override system language (specify first letter, two-letter code, or full language name)");
#endif

#if !__FINAL
namespace rage
{
XPARAM(update);
}
#endif // !__FINAL

bool diagErrorCodes::LoadLanguageFile()
{
	m_ErrorCodes.Kill();
	m_ErrorMessages.Kill();
	
	sysLanguage language = g_SysService.GetSystemLanguage();
	m_currentLanguage = language;

	char filename[32];

	// TODO: Mexican

	switch(language)
	{
	case LANGUAGE_CHINESE_TRADITIONAL:
		strcpy(filename, "chinese.txt");
		break;
	case LANGUAGE_CHINESE_SIMPLIFIED:
#if RSG_ORBIS || RSG_DURANGO
        strcpy(filename, "chinese.txt");
#else
        strcpy(filename, "chinesesimp.txt");
#endif // RSG_ORBIS || RSG_DURANGO
		break;
	case LANGUAGE_FRENCH:
		strcpy(filename, "french.txt");
		break;
	case LANGUAGE_GERMAN:
		strcpy(filename, "german.txt");
		break;
	case LANGUAGE_ITALIAN:
		strcpy(filename, "italian.txt");
		break;
	case LANGUAGE_JAPANESE:
		strcpy(filename, "japanese.txt");
		break;
	case LANGUAGE_KOREAN:
		strcpy(filename, "korean.txt");
		break;
	case LANGUAGE_MEXICAN:
		strcpy(filename, "mexican.txt");
		break;
	case LANGUAGE_PORTUGUESE:
		strcpy(filename, "portuguese.txt");
		break;
	case LANGUAGE_POLISH:
		strcpy(filename, "polish.txt");
		break;
	case LANGUAGE_RUSSIAN:
		strcpy(filename, "russian.txt");
		break;
	case LANGUAGE_SPANISH:
		strcpy(filename, "spanish.txt");
		break;
	default:
		strcpy(filename, "american.txt");
	}

	char	fileLocationRelative[1024];
	wchar_t w_fileLocation[512];
	FILE* fp = NULL;

	// This used to be "cc=UNICODE" but this defaults to UTF-16 if no BOM char exists (see https://msdn.microsoft.com/en-us/library/yeby3zcb.aspx).
	// ccs=UTF-8 will default to UTF-8 if no BOM exists (which is not common in UTF-8 files but more likely in multi-byte encodings such as UTF-16.
	#define ERR_CODE_FILE_ENCODING L"UTF-8"

#if !__FINAL
	const char* pUpdateFolder;
	if (PARAM_update.Get(pUpdateFolder))
	{
		sprintf(fileLocationRelative, "%sx64/data/errorcodes/%s", pUpdateFolder, filename);
		mbstowcs(w_fileLocation, fileLocationRelative, 1024);
		fp = _wfopen(w_fileLocation, L"r, ccs=" ERR_CODE_FILE_ENCODING);
	}
	
	if (fp == NULL)
	{
		sprintf(fileLocationRelative, "update/x64/data/errorcodes/%s", filename);
		mbstowcs(w_fileLocation, fileLocationRelative, 1024);
		fp = _wfopen(w_fileLocation, L"r, ccs=" ERR_CODE_FILE_ENCODING);
	}
	
	if (fp == NULL)
	{
		sprintf(fileLocationRelative, "X:/gta5/titleupdate/dev_ng/x64/data/errorcodes/%s", filename);
		mbstowcs(w_fileLocation, fileLocationRelative, 1024);
		fp = _wfopen(w_fileLocation, L"r, ccs=" ERR_CODE_FILE_ENCODING);
	}

	if (fp == NULL)
	{
		sprintf(fileLocationRelative, "x64/data/errorcodes/%s", filename);
		mbstowcs(w_fileLocation, fileLocationRelative, 1024);
		fp = _wfopen(w_fileLocation, L"r, ccs=" ERR_CODE_FILE_ENCODING);
	}

	if (fp == NULL)
	{
		// TODO: Re-enable this Quitf when we have fatal error code files properly loading in packaged builds
		//Quitf("Unable to load PC Fatal Error Code file!");
	}
#else
	sprintf(fileLocationRelative, "update/x64/data/errorcodes/%s", filename);
	mbstowcs(w_fileLocation, fileLocationRelative, 1024);
	fp = _wfopen(w_fileLocation, L"r, ccs=" ERR_CODE_FILE_ENCODING);

	if (fp == NULL)
	{
		sprintf(fileLocationRelative, "x64/data/errorcodes/%s", filename);
		mbstowcs(w_fileLocation, fileLocationRelative, 1024);

		fp = _wfopen(w_fileLocation, L"r, ccs=" ERR_CODE_FILE_ENCODING);
	}

	if (fp == NULL)
	{
		sprintf(fileLocationRelative, "X:/gta5/build/dev_ng/x64/data/errorcodes/%s", filename);
		mbstowcs(w_fileLocation, fileLocationRelative, 1024);

		fp = _wfopen(w_fileLocation, L"r, ccs=" ERR_CODE_FILE_ENCODING);
	}
#endif // !__FINAL

	if (!fp)
	{
		m_bLoaded = false;
		return m_bLoaded;
	}

	ErrorCodeFileLineType lastValidLineType = LINE_TYPE_DEFAULT;

	wchar_t line[2048];
	wchar_t tempErrorCode[128];
	wchar_t tempErrorMessage[1024];
	char mb_errorCode[128];
	u32 hashCode = 0;
	wchar_t sep[] = L"[]";

	m_bLoading = true;

	while(fgetws(line, sizeof(line), fp) != NULL)
	{
		if (line[0] == L'{' || line[0] == L'\n')
			continue;

		if (line[0] == L'[')
		{
			if (lastValidLineType == LINE_TYPE_DEFAULT || lastValidLineType == LINE_TYPE_TEXT)
			{
				lastValidLineType = LINE_TYPE_LABEL;

				// Get the error code
				wchar_t* token = wcstok(line, sep);
				wcscpy_s(tempErrorCode, 128, token);		
				wcstombs(mb_errorCode, token, 64);
				size_t errorCodeStrLen = wcslen(tempErrorCode);
				wchar_t* errorCode = rage_new wchar_t[errorCodeStrLen + 1];
				wcscpy_s(errorCode, errorCodeStrLen + 1, tempErrorCode);

				hashCode = atStringHash(mb_errorCode);
				m_ErrorCodes.Insert(hashCode, errorCode);
			}
			else
			{
				Quitf(ERR_DEFAULT,"Error loading error code files");
			}
		}
		else
		{
			if (lastValidLineType == LINE_TYPE_LABEL)
			{
				lastValidLineType = LINE_TYPE_TEXT;

				// Get the error string
				wcscpy_s(tempErrorMessage, 1024, line);
				size_t errorMessageStrLen = wcslen(tempErrorMessage);
				wchar_t* errorMessage = rage_new wchar_t[errorMessageStrLen + 1];
				wcscpy_s(errorMessage, errorMessageStrLen + 1, tempErrorMessage);

				m_ErrorMessages.Insert(hashCode, errorMessage);
			}
			else
			{
				Quitf(ERR_DEFAULT,"Error loading error code files");
			}
		}
	}

	fclose(fp);

	m_bLoading = false;
	m_bLoaded = true;
	return m_bLoaded;
}

wchar_t* diagErrorCodes::GetErrorCodeString(FatalErrorCode errorCode)
{
	if (!m_bLoaded)
		return GetDefaultErrorCodeString();

	wchar_t** errorString = m_ErrorCodes.Access(static_cast<u32>(errorCode));

	if (!errorString)
		return GetDefaultErrorCodeString();
	else if (wcslen(*errorString) == 0)
		return GetDefaultErrorCodeString();

	return *errorString;
}

wchar_t* diagErrorCodes::GetDefaultErrorCodeString()
{
	// TODO: Localize

	switch(m_currentLanguage)
	{
	case LANGUAGE_FRENCH:
		//return L"Erreur irrécupérable - Veuillez relancer le jeu.";
	case LANGUAGE_ITALIAN:
		//return L"Errore irrecuperabile - riavvia il gioco.";
	case LANGUAGE_GERMAN:
		//return L"Nicht zu behebender Fehler - Bitte das Spiel neu starten";
	case LANGUAGE_SPANISH:
		//return L"Error irrecuperable: reinicia el juego.";
	case LANGUAGE_RUSSIAN:
		//return (wchar_t*)&russianError[0];
	case LANGUAGE_JAPANESE:
		//return (wchar_t*)&japaneseError[0];
	default:
		return L"Fatal Error";
	}
}

wchar_t* diagErrorCodes::GetErrorMessageString(FatalErrorCode errorCode)
{
	if (m_bLoading)
	{
		return L"Corrupted Error Code File - Please reinstall the game.";
	}
	else if (!m_bLoaded)
	{
		return GetDefaultErrorMessageString();
	}
	
	wchar_t** errorString = m_ErrorMessages.Access(static_cast<u32>(errorCode));

	if (!errorString)
		return GetDefaultErrorMessageString();
	else if (wcslen(*errorString) == 0)
		return GetDefaultErrorMessageString();

	return *errorString;
}

wchar_t* diagErrorCodes::GetDefaultErrorMessageString()
{
	// TODO: Localize

	switch(m_currentLanguage)
	{
	case LANGUAGE_FRENCH:
		return L"Erreur irrécupérable - Veuillez relancer le jeu.";
	case LANGUAGE_ITALIAN:
		return L"Errore irrecuperabile - riavvia il gioco.";
	case LANGUAGE_GERMAN:
		return L"Nicht zu behebender Fehler - Bitte das Spiel neu starten";
	case LANGUAGE_SPANISH:
		return L"Error irrecuperable: reinicia el juego.";
	case LANGUAGE_RUSSIAN:
			return (wchar_t*)&russianError[0];
 	case LANGUAGE_JAPANESE:
 			return (wchar_t*)&japaneseError[0];
	default:
		return L"Unrecoverable fault - Please restart the game";
	}
}

char* diagErrorCodes::Win32ErrorToString(int error, char* pBuffer, size_t bufferLen)
{
	DWORD ret = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), pBuffer, (DWORD)bufferLen, NULL);
	if(ret == 0)
	{
		formatf(pBuffer, bufferLen, "Unknown error");
		return pBuffer;
	}

	// The returned string usually has newlines in it - strip them out
	char* pNewline = strchr(pBuffer, '\r');
	if(pNewline)
	{
		while((*pNewline == '\r' || *pNewline == '\n') && pNewline != pBuffer)
			*pNewline-- = 0;
	}
	return pBuffer;
}

#endif // __WIN32PC

