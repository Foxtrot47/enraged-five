//
// diag/memstats.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// PURPOSE:
//		This file contains a number of functions for creating a log
//		of memory usage and loading times for a game.
// TODO - turn these functions into a class.


#ifndef DIAG_MEMSTATS_H
#define DIAG_MEMSTATS_H

#include "stats.h"

namespace rage {

#if __STATS
class fiStream;

// handle to memory logging file handle, if open
extern fiStream *g_LogMemoryStream;

// handle to load logging file handle, if open
extern fiStream *g_LogLoadStream;

//	PURPOSE
//		Call this at start of initialization to open memory usage logfile.
//  PARAMS
//      filename - The name of the logfile to open
//	NOTES
//		Logfile will be created only if running with -memstats.
//		Run with -memstats=something.txt to override given filename.
//		<CODE>
//		// heirarchical memory logging example
//		diagLogMemoryMessage("Loading cars");
//		diagLogMemoryPush();
//		
//			// <allocate shared data>
//			diagLogMemory("Car shared data");
//		
//			foreach name
//			{
//				diagLogMemoryMessage("Loading Car %s",name);
//				diagLogMemoryPush();
//				{
//					// <load model>
//					diagLogMemory("Model")
//		
//					// <load bounds>
//					diagLogMemory("Bounds")
//		
//					// <init particles>
//					diagLogMemory("Particles")
//				}
//				diagLogMemoryPop();
//				diagLogMemory("Car %s subtotal",name)
//			}
//		
//			// <allocated more stuff>
//			diagLogMemory("More shared data");
//		
//		diagLogMemoryPop();
//		diagLogMemory("All Cars total");
//		</CODE>
//   SEE ALSO
//		diagLogMemoryFlush, diagLogMemoryClose, diagLogMemory, diagLog
//   <FLAG Component>
void diagLogMemoryOpen(const char *filename = "memstats.txt");

//	PURPOSE
//		Call this at start of initialization to open load time logfile.
//	NOTES
//		Logfile will be created only if running with -loadstats.
//		Run with -loadstats=something.txt to override given filename.
//  PARAMS
//      filename - The name to give the logfile
//  SEE ALSO:
//		diagLogLoadClose
void diagLogLoadOpen(const char *filename = "loadstats.txt");

//	PURPOSE
//		Call this when done loading but not done logging.
//	SEE ALSO
//		diagLogLoadClose()
//		diagLogMemoryClose()
void diagLogMemoryFlush();

//	PURPOSE
//		Call this when done logging memory.
//	SEE ALSO
//		diagLogLoadClose()
void diagLogMemoryClose();

//	PURPOSE
//		Call this when done logging load times.
//	SEE ALSO
//		diagLogMemoryClose()
void diagLogLoadClose();

//	PURPOSE
//		Print message to logfiles.
//  PARAMS
//      message - The message to print to the logfile
//	NOTES
//		Only writes to memory log file if diagLogMemoryOpen() was called and the application was run with -memstats.
//		Only writes to load time log file if diagLogLoadOpen() was called and the application was run with -loadstats.
//		This compiles out if __STATS is disabled.
//  <FLAG Component>
void diagLogMemoryMessage(const char *message, ...);

//	PURPOSE
//		Print message and memory allocated to logfile since last diagLogMemory() call.
//		Print load time passed since last diagLogMemory() call as well.
//  PARAMS
//      message - The message to print to the logfile
//	NOTES
//		Only logs memory messages if diagLogMemoryOpen() was called and the application was run with -memstats.
//		Only logs load times if diagLogLoadOpen() was called and the application was run with -loadstats.
//		This compiles out if __STATS is disabled.
//  <FLAG Component>
void diagLogMemory(const char *message, ...);


//	PURPOSE
//			Print message and total memory allocated to memory stats logfile.
//			Also print message and total load time to load time logfile.
//  PARAMS
//      message - The message to print to the logfile
//	NOTES
//			Only logs memory messages if diagLogMemoryOpen() was called and the application was run with -memstats.
//			Only logs load times if diagLogLoadOpen() was called and the application was run with -loadstats.
//			This compiles out if __STATS is disabled.
//  <FLAG Component>
void diagLogMemoryTotal(const char *message, ...);


//	PURPOSE
//		Increase indent for both memory logging and for load time logging.
//  SEE ALSO:
//		diagLogMemoryPop
//	NOTES
//		This function does not actually write anything to the log.  If you have any
//		allocations before it, make sure you call diagLogMemory() first.  In other words,
//		don't allocate then push: you should allocate, then log, *then* push.
//		This compiles out if __STATS is disabled.
void diagLogMemoryPush();


//	PURPOSE
//		Decreases indent for both memory logging and for load time logging.
//	SEE ALSO:
//		diagLogMemoryPush
//	NOTES
//		This function does not actually write anything to the log.  If you have any
//		allocations before it, make sure you call diagLogMemory() first.  In other words,
//		don't allocate then pop: you should allocate, then log, *then* pop.
//		This compiles out if __STATS is disabled.
void diagLogMemoryPop();

//	PURPOSE
//		This is just like calling diagLogMemoryMessage() and then diagLogMemoryPush().
//  PARAMS
//      message - The message to print to the logfile
//	SEE ALSO
//		diagLogMemoryMessage()
//		diagLogMemoryPush()
//		diagLogMemoryEndSection()
//	NOTES
//		Only logs memory messages if diagLogMemoryOpen() was called and the application was run with -memstats.
//		Only logs load times if diagLogLoadOpen() was called and the application was run with -loadstats.
//		This compiles out if __STATS is disabled.
//  <FLAG Component>
void diagLogMemoryStartSection(const char *message, ...);


//	PURPOSE
//		This is just like calling diagLogMemoryPop() and then diagLogMemory().
//  PARAMS
//      message - The message to print to the logfile
//	SEE ALSO
//		diagLogMemoryPop()
//		diagLogMemory()
//		diagLogMemoryStartSection()
//	NOTES
//		Only logs memory messages if diagLogMemoryOpen() was called and the application was run with -memstats.
//		Only logs load times if diagLogLoadOpen() was called and the application was run with -loadstats.
//		This compiles out if __STATS is disabled.
//  <FLAG Component>
void diagLogMemoryEndSection(const char *message, ...);



#else

inline void diagLogMemoryOpen(const char *UNUSED_PARAM(filename) = "") {}
inline void diagLogLoadOpen(const char *UNUSED_PARAM(filename) = "") {}
inline void diagLogMemoryFlush() {}
inline void diagLogMemoryClose() {}
inline void diagLogLoadClose() {}
inline void diagLogMemoryPush() {}
inline void diagLogMemoryPop() {}

#if __WIN32
	#pragma warning(disable: 4710)	// "function 'blah' not inlined" message
	inline void diagLogMemoryMessage(const char *UNUSED_PARAM(message), ...) {}
	inline void diagLogMemory(const char *UNUSED_PARAM(message), ...) {}
	inline void diagLogMemoryTotal(const char *UNUSED_PARAM(message), ...) {}
	inline void diagLogMemoryStartSection(const char *UNUSED_PARAM(message), ...) {}
	inline void diagLogMemoryEndSection(const char *UNUSED_PARAM(message), ...) {}
#else
	#ifdef __GNUC__
		#define diagLogMemoryMessage(args...)
		#define diagLogMemory(args...)
		#define diagLogMemoryTotal(args...)
		#define diagLogMemoryStartSection(args...)
		#define diagLogMemoryEndSection(args...)
	#else
		#define diagLogMemoryMessage
		#define diagLogMemory
		#define diagLogMemoryTotal
		#define diagLogMemoryStartSection
		#define diagLogMemoryEndSection
	#endif
#endif

#endif

} // namespace rage

#endif

