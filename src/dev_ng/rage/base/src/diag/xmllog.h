//
// diag/xmllog.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

//	PURPOSE:
//		This file contains an xml log which is for outputing xml to files or screen
//		for later parsing by tools such as xslt, excel, access etc.
//

#ifndef DIAG_XMLLOG_H
#define DIAG_XMLLOG_H

#include "atl/string.h"
namespace rage 
{

	// PURPOSE : simple log that outputs in xml 
	//
	class XmlLog
	{	

		int					m_tabCount;
	public:
		XmlLog() : m_tabCount(0) {}
		virtual ~XmlLog()  {}
		virtual void  StartTag( const char* tagname );
		virtual void  EndTag( const char* tagname, bool useTabs = true );
		virtual void  WriteVal( const char* val );
		virtual void  WriteVal( int val );
		virtual void  WriteVal( float f ) ;
		virtual void  WriteHeader(float XmlVersion);

		virtual void  WriteBytes( const char*  str, int size ) = 0;

		template<class T>
		void Write( const char* tag, T val )
		{
			StartTag(tag);
			XMLLogWriteValue( *this,val );
			EndTag(tag, false );
		}
		void GroupStart(  const char* tag )
		{
			StartTag(tag);
			WriteVal("\n");
		}
		void GroupEnd(  const char* tag )
		{
			EndTag(tag, true);
		}

		void NewLine() { WriteBytes("\n",1); }
	};


	template<class T>
	void XMLLogWriteValue( XmlLog& log, T str )
	{
		log.WriteVal( str );
	}
	inline void XMLLogWriteValue( XmlLog& log, u32 v )
	{
		log.WriteVal( (int)v );
	}
	inline void XMLLogWriteValue( XmlLog& log, bool v )
	{
		const char* text = v ? "true" : "false";
		log.WriteVal( text );
	}	
	

	class fiStream;
	class fiAsciiTokenizer;

	// PURPOSE : outputs to a file using the tokenizer
	//
	class XmlFileLog : public XmlLog
	{
		fiStream*			m_stream;
	public:
		XmlFileLog( const char* name, const char* ext, bool writeXmlHeader = false, float XmlVersion = 1.0f, bool append = false  );
		virtual ~XmlFileLog();
		virtual void  WriteBytes( const char* str, int size  );
	};

	class XmlStdOutLog : public XmlLog
	{
	public:
		XmlStdOutLog( bool writeXmlHeader = false, float XmlVersion = 1.0f );
		virtual void  WriteBytes( const char* str, int size  );
	};

	class XmlStringLog : public XmlLog
	{
		
		atString		m_string;
	public:
		
		virtual void  WriteBytes( const char* str, int /*size*/ ) 
		{ 
			m_string += str; 
		}
		const char* ToString() { return m_string.c_str(); }
	};

XmlLog&	XmlStdLog( bool writeXmlHeader = false, float XmlVersion = 1.0f );

}	// namespace rage

#endif
