// 
// diag/seh.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DIAG_SEH_H
#define DIAG_SEH_H

/*  Macros for structured error handling

    Structed error handling is intended to look like exception handling,
    but its real benefit is in being able to handle errors in a structured
    way while keeping code as readable as possible.

    Contrast the following two pieces of code which host a network session.
    The first doesn't use seh and has 7 levels of indentation, making it
    somewhat difficult to follow.

    The second uses seh and has one level of indentation making it much easier
    follow.

//---------------------------------------
//Doesn't use SEH
//---------------------------------------
bool success = false;
if(!this->IsDormant())
{
    if(!this->IsPending())
    {
        if(localOwner.IsSignedIn())
        {
            bool hasPrivs = true;

            if(RL_NETMODE_ONLINE == netMode)
            {
                hasPrivs = rlGamerInfo::HasMultiplayerPrivileges(localOwner.GetLocalIndex());

                if(!hasPrivs)
                {
                    rlError("Gamer:\"%s\" does not have multiplayer privileges",
                            localOwner.GetName()));
                }
            }

            bool hasNilAttrs = false;
            if(hasPrivs)
            {
                for(int i = 0; i < (int) attrs.GetCount(); ++i)
                {
                    if(!attrs.GetValueByIndex(i))
                    {
                        rlError("Session attribute %d is nil", i);
                        hasNilAttrs = true;
                        break;
                    }
                }

                if(!hasNilAttrs)
                {
                    CmdHost* cmd = rlAlloc<CmdHost>();

                    if(!AssertVerify(cmd))
                    {
                        rlError("Error allocating CmdHost");
                    }
                    else
                    {
                        new(cmd) CmdHost(this,
                                            localOwner,
                                            netMode,
                                            numPublicSlots,
                                            numPrivateSlots,
                                            attrs,
                                            createFlags,
                                            netHw,
                                            lanListenPort,
                                            status);

                        success = this->DoCmd(cmd);

                        if(!success)
                        {
                            rlDelete(cmd);
                        }
                    }
                }
            }
        }
    }

    return success;
}

//---------------------------------------
//Uses SEH
//---------------------------------------
bool success = false;
CmdHost* cmd = NULL;

rtry
{
    rverify(this->IsDormant(), catchall,);
    rverify(!this->IsPending(), catchall,);
    rverify(localOwner.IsSignedIn(), catchall,);

    if(RL_NETMODE_ONLINE == netMode)
    {
        rcheck(rlGamerInfo::HasMultiplayerPrivileges(localOwner.GetLocalIndex()),
                catchall,
                rlError("Gamer:\"%s\" does not have multiplayer privileges",
                        localOwner.GetName()));
    }

    for(int i = 0; i < (int) attrs.GetCount(); ++i)
    {
        rverify(attrs.GetValueByIndex(i),
                catchall,
                rlError("Session attribute %d is nil", i));
    }

    cmd = rlAlloc<CmdHost>();
    rverify(cmd, catchall, rlError("Error allocating CmdHost"));

    new(cmd) CmdHost(this,
                        localOwner,
                        netMode,
                        numPublicSlots,
                        numPrivateSlots,
                        attrs,
                        createFlags,
                        netHw,
                        lanListenPort,
                        status);

    rcheck(this->DoCmd(cmd), catchall,);

    success = true;
}
rcatchall
{
    if(cmd){rlDelete(cmd);}
}

return success;

-------------------------------------------------------------------------------

Following is an example of multiple rcatch() clauses

Foo* foo = NULL;
Bar* bar = NULL;
Baz* baz = NULL;

rtry
{
    rcheck(InitSystem(), catchall, rlError("Error initializing system"));

    foo = new Foo();
    rcheck(foo, catch(noFoo), rlError("Error allocating foo"));

    bar = new Bar(foo);
    rcheck(bar, catch(noBar), rlError("Error allocating bar"));

    baz = new Baz(bar);
    rcheck(baz, catch(noBaz), rlError("Error allocating baz"));
}
rcatch(noBaz)
{
    delete bar;
    bar = NULL;
    rthrow(noBar,);
}
rcatch(noBar)
{
    delete foo;
    foo = NULL;
    rthrow(noFoo,)
}
rcatch(noFoo)
{
    rthrow(catchall,)
}
rcatchall
{
    ShutdownSystem();
}

*/

#ifdef __SNC__
#define SNC_PRAGMA(x)	_Pragma(x)
#else
#define SNC_PRAGMA(x)
#endif

#define rtry	SNC_PRAGMA("diag_suppress=129")

//Handles an error by performing action then jumping to dest.
//The destination is defined on a subsequent line by rcatch(dest).
//Action is typically an error printing statement.
//Example:
//rtry
//{
//    if(!someCondition)
//    {
//        throw(foo, Print("someCondition was false"));
//    }
//}
//rcatch(foo)
//{
//    HandleError();
//}
#define rthrow(dest, action) do{action;goto rexcept_##dest;}while(0);

//Checks a condition and performs an rthrow if the condition is false.
//Example:
//rtry
//{
//    rcheck(someCondition, foo, Print("someCondition was false"));
//}
//rcatch(foo)
//{
//    HandleError();
//}
#define rcheck(cond, dest, action)\
    do{if(!(cond)){rthrow(dest, action);}}while(0);

//Checks a condition and asserts and performs an rthrow if the
//condition is false.
//Example:
//rtry
//{
//    rverify(someCondition, foo, Print("someCondition was false"));
//}
//rcatch(foo)
//{
//    HandleError();
//}
#if __ASSERT || __NO_OUTPUT
#define rverify(cond, dest, action)\
    do{if(!(cond)){do{action;}while(0); AssertMsg(false, #cond); rthrow(dest,);}}while(0);
#else
// if asserts are disabled but output is enabled, output the condition that failed
#define rverify(cond, dest, action)\
	do{if(!(cond)){do{action;}while(0); Errorf("rverify(%s) failed", #cond); rthrow(dest,);}}while(0);
#endif

//Defines the destination for an rthrow.  See above examples.
#define rcatch(label)\
    while(0)\
    rexcept_##label: SNC_PRAGMA("diag_default=129")

//Convenience macro for defining the catchall destination for throws.
//Example:
//rtry
//{
//    rverify(someCondition, catchall, Print("someCondition was false"));
//}
//rcatchall
//{
//    HandleError();
//}
#define rcatchall rcatch(catchall)

//A simple checks on a condition to see if it is valid, and throws to the catchall block.
//	Equivalent to calling rcheck(cond, catchall, );
//Example:
//rtry
//{
//    rcheckall(someCondition);
//}
//rcatchall
//{
//    HandleError();
//}
#define rcheckall(cond) rcheck(cond, catchall, )

//A simple checks on a condition to see if it is valid, and throws to the catchall block with an assert.
//	Equivalent to calling rverify(cond, catchall, );
//Example:
//rtry
//{
//    rverifyall(someCondition);
//}
//rcatchall
//{
//    HandleError();
//}
#define rverifyall(cond) rverify(cond, catchall, );

#endif  //DIAG_SEH_H
