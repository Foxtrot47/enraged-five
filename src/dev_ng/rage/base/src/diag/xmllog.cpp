//
// diag/xmllog.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "xmllog.h"

#include "output.h"
#include "assert.h"

#include "file/stream.h"
#include "file/asset.h"
#include "file/token.h"
#include "math/amath.h"
#include "system/codecheck.h"

using namespace rage;

XmlStdOutLog	g_StdLog;

XmlLog&	rage::XmlStdLog(bool writeXmlHeader /* = false */, float XmlVersion /* = 1.0f */ )
{
	if (writeXmlHeader)
	{
		g_StdLog.WriteHeader(XmlVersion);
	}
	return g_StdLog;
}
void XmlLog::WriteHeader(float XmlVersion)
{
	static char buffer[96];  
	int num = sprintf(buffer, "<?xml version=\"%.1f\"?>\n", XmlVersion);
	WriteBytes(buffer, num );
}

void  XmlLog::WriteVal( const char* val )
{
	WriteBytes( val , StringLength(val) );
}
void  XmlLog::WriteVal( int val )
{
	char str[64];
	int num=sprintf(str,"%d", val);
	WriteBytes(str,num);
}
void  XmlLog::WriteVal( float val )
{
	char str[64];
	int num=sprintf(str,"%.8g", val);
	WriteBytes(str,num);
}
void  XmlLog::StartTag( const char* tagname )
{
	char tabs[]="\t\t\t\t\t\t\t\t";
	tabs[m_tabCount] ='\0';

	char str[1024];
	int num = sprintf(str, "%s<%s>",tabs,tagname );
	WriteBytes(str,num);

	m_tabCount++;
	m_tabCount = Min( m_tabCount, 7);

	Assert( m_tabCount >= 0 );

}
void  XmlLog::EndTag( const char* tagname , bool useTabs)
{
	char tabs[]="\t\t\t\t\t\t\t\t";
	char str[1024];
	--m_tabCount;
	int num;

	if ( useTabs)
	{
		m_tabCount = Max( m_tabCount, 0);
		tabs[m_tabCount] ='\0';

		num= sprintf(str,"%s</%s>\n",tabs,tagname );
	}
	else
	{
		num= sprintf(str,"</%s>\n",tagname );
	}
	WriteBytes(str,num);
}
// File Log ---------------------------
XmlFileLog::XmlFileLog( const char* name, const char* ext, bool writeXmlHeader /* = false */, float XmlVersion /* = 1.0f */, bool append )
{
	if ( append )
	{
		m_stream = ASSET.Open( name, ext, false, false );
		Assert( !writeXmlHeader );
		if ( m_stream) m_stream->Seek( m_stream->Size() );
	}
	else
	{
		m_stream =ASSET.Create( name, ext) ;
	}
	if (writeXmlHeader)
	{
		WriteHeader(XmlVersion);
	}
}
XmlFileLog::~XmlFileLog()
{
	if ( m_stream ) m_stream->Close();
}
void XmlFileLog::WriteBytes( const char* str, int size )
{
	if ( m_stream ) m_stream->Write( str, size );
}

// Std Out Log ---------------------------

XmlStdOutLog::XmlStdOutLog(bool writeXmlHeader /* = false */, float XmlVersion /* = 1.0f */)
{
	if (writeXmlHeader)
	{
		WriteHeader(XmlVersion);
	}
}
void XmlStdOutLog::WriteBytes( const char* OUTPUT_ONLY(str), int )
{
	OUTPUT_ONLY(printf("%s",str));
}

