//
// diag/assert.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "diag/channel.h"
#include "system/bootmgr.h"
#include "system/param.h"

#if __ASSERT

PARAM(debugassertson,"Enable DebugAssert's");

namespace rage {

bool NIY = true;		// Set this to false to see all "not implemented yet" assertions.

int DebugAssertFailed(void) {
	static bool debugAssertsOn=PARAM_debugassertson.Get();
	if (debugAssertsOn && sysBootManager::IsDebuggerPresent())
	{
		Warningf("Debug Assert Failed - breaking...");
		__debugbreak();
		Warningf("continuing after Debug Assert Failed");
	}
	return false;
}


bool ArrayAssertFailed(int line) {
	return diagAssertHelper("base/src/atl/array.h",line,"array bounds exception") != 0 || (diagTerminate(),0);
}

int FastAssertFailed(void) {
	return diagAssertHelper("fastassert",0,"FastAssert Exception") != 0 || (diagTerminate(),0);
}

} // namespace rage

#if RAGE_MINIMAL_ASSERTS

void assertCheck(struct assertData* ad,int cond) {
	if (!cond && !::rage::diagAssertHelper2(ad->file,ad->line,ad->cond))
		__debugbreak();
}

void assertCheck(struct assertData* ad,unsigned cond) {
	if (!cond && !::rage::diagAssertHelper2(ad->file,ad->line,ad->cond))
		__debugbreak();
}

void assertCheck(struct assertData* ad,unsigned long cond) {
	if (!cond && !::rage::diagAssertHelper2(ad->file,ad->line,ad->cond))
		__debugbreak();
}

void assertCheck(struct assertData* ad,unsigned long long cond) {
	if (!cond && !::rage::diagAssertHelper2(ad->file,ad->line,ad->cond))
		__debugbreak();
}

void assertCheck(struct assertData* ad,const void *cond) {
	if (!cond && !::rage::diagAssertHelper2(ad->file,ad->line,ad->cond))
		__debugbreak();
}

void assertCheck(struct assertData* ad,float cond) {
	if (!cond && !::rage::diagAssertHelper2(ad->file,ad->line,ad->cond))
		__debugbreak();
}

#endif

#endif		// __ASSERT
