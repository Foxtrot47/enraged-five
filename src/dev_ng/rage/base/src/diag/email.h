// email.h
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 

#ifndef EMAIL_H
#define EMAIL_H

namespace rage
{

//NOTES
//  The email functions use the following cmdline parameters:
//  smtpserver  - SMTP server to use.  If not specified, rageEmailer default is used.

//PURPOSE
//  Sends an email.
//PARAMETERS
//  sender      - Sender email address, in name@domain format
//  recipient   - Recipient email address, in name@domain format
//  subject     - Subject line to display
//  text        - Body text
//RETURNS
//  False if error, true otherwise.  Because an external programs (rageemailer)
//  is used to send the mail, a true result does not guarantee a successful send.
bool SendEmail(const char* sender, 
               const char* recipient, 
               const char* subject, 
               const char* text);

//PURPOSE
//  Sends an email, using specified file as body text.
//PARAMETERS
//  sender      - Sender email address, in name@domain format
//  recipient   - Recipient email address, in name@domain format
//  subject     - Subject line to display
//  filename    - Name of text file whose contents are used as body text
//RETURNS
//  False if error, true otherwise.  Because an external programs (rageemailer)
//  is used to send the mail, a true result does not guarantee a successful send.
bool SendEmailUsingFile(const char* sender, 
                        const char* recipient, 
                        const char* subject, 
                        const char* filename);

//PURPOSE
//  Sends an HTML-formatted email that includes system identification
//  and debugging info along with the body text.
//PARAMETERS
//  sender      - Sender email address, in name@domain format
//  recipient   - Recipient email address, in name@domain format
//  subject     - Subject line to display
//  text        - Body text
//RETURNS
//  False if error, true otherwise.  Because an external programs (rageemailer)
//  is used to send the mail, a true result does not guarantee a successful send.
bool SendSystemEmail(const char* sender,
                     const char* recipient,
                     const char* subject, 
                     const char* text);

}; //namespace rage

#endif //EMAIL_H
