// 
// parser/restservice.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "restservice.h"

#include "civetweb.h"
#include "restcommand.h"

#include "data/growbuffer.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/stream.h"
#include "file/tcpip.h"
#include "file/winsock.h"
#include "system/ipc.h"
#include "system/lockfreering.h"
#include "system/param.h"
#include "system/timer.h"

RAGE_DEFINE_CHANNEL(REST)

using namespace rage;

PARAM(webserverOn, "[diag] Enable the REST interface web server");
PARAM(httpPort, "[diag] The port(s) that the debug web server listens on (default: 7890)");
PARAM(httpNumThreads, "[diag] the max number of threads to listed for rest requests on (default: 4)");
PARAM(httpThrottle, "[diag] Throttle the rest responses. Simple use is \"-httpThrottle=*=10k\" See the civentweb user manual for more options");

#if __BANK

#if RAGE_USE_WEB_SERVER
void InitWebServer();
void ShutdownWebServer();
void UpdateWebServer();
namespace rage
{
	char g_RestWebServerUri[128] = "";
}
#endif

restServiceManager rage::REST;

restService::restService( const char* name )
{
	m_Name = StringDuplicate(name);
	m_Parent = NULL;
}

restService::~restService()
{
	restServiceDirectory* parentDir = dynamic_cast<restServiceDirectory*>(m_Parent);
	if (parentDir)
	{
		parentDir->RemoveChild(*this);
	}
	StringFree(m_Name);
}

void restService::PrintFullName(fiStream* stream)
{
	if (m_Parent)
	{
		m_Parent->PrintFullName(stream);
	}
	fprintf(stream, "%s/", m_Name);
}

restServiceDirectory::restServiceDirectory( const char* name )
: restService(name)
{
}

restServiceDirectory::~restServiceDirectory()
{
	for(int i = 0; i < m_Children.GetCount(); i++)
	{
		delete m_Children[i];
	}
}

restService* restServiceDirectory::FindChild(const char* name)
{
	for(int i = 0; i < m_Children.GetCount(); i++) 
	{
		const char* kidName = m_Children[i]->GetName();
		if (kidName && !stricmp(kidName, name))
		{
			return m_Children[i];
		}
	}
	return NULL;
}

restStatus::Enum restServiceDirectory::ProcessCommand( restCommand& command )
{
	if (command.IsAtEndOfPath())
	{
		return ProcessMyCommand(command);
	}

	// else check for a child with a matching name, and forward the command on to that child.

	const char* pathComp = command.GetCurrentPathComponent();

	restService* kid = FindChild(pathComp);

	if (kid)
	{
		command.NextPathComponent();
		return kid->ProcessCommand(command);
	}
	return restStatus::REST_NOT_FOUND;
}

void restServiceDirectory::AddChild( restService& svc )
{
	m_Children.PushAndGrow(&svc);
	svc.m_Parent = this;
}

void restServiceDirectory::RemoveChild( restService& svc )
{
	svc.m_Parent = NULL;
	m_Children.DeleteMatches(&svc);
}

restStatus::Enum rage::restServiceDirectory::ProcessMyCommand( restCommand& command )
{
	switch(command.m_Verb)
	{
	case restCommand::REST_GET:
		return ListChildren(command);
	default:
		command.ReportErrorMethodNotAllowed(restCommand::REST_GET);
		return restStatus::REST_METHOD_NOT_ALLOWED;
	}
}

restStatus::Enum restServiceDirectory::ListChildren( restCommand& command )
{
	fiStream* out = command.m_OutputStream;

	fprintf(out, "<restServiceDirectory>\n");
	for(int i = 0; i < m_Children.GetCount(); i++)
	{
		fprintf(out, "\t<Item href=\"./%s\" serviceType=\"%s\" ",  m_Children[i]->GetName(), m_Children[i]->GetServiceType());
		m_Children[i]->PrintExtraDirInfo(out);
		fprintf(out, " />\n");
	}
	fprintf(out, "</restServiceDirectory>\n");

	return restStatus::REST_OK;
}

void restServiceManager::Init()
{
	m_Root = rage_new restServiceDirectory("");
#if RAGE_USE_WEB_SERVER
	InitWebServer();
#endif
}

#if RAGE_USE_WEB_SERVER
const char* restServiceManager::GetWebServerUri()
{
	return g_RestWebServerUri;
}
#endif

void restServiceManager::Shutdown()
{
#if RAGE_USE_WEB_SERVER
	ShutdownWebServer();
#endif
	delete m_Root;
}

void restServiceManager::Update()
{
#if RAGE_USE_WEB_SERVER
	UpdateWebServer();
#endif
}

restStatus::Enum restServiceManager::ProcessCommand(restCommand& command)
{
	FastAssert(m_Root);


#if !__NO_OUTPUT
	sysTimer timer;
	const char* verbName = "";
	switch(command.m_Verb)
	{
	case restCommand::REST_DELETE: verbName = "DELETE"; break;
	case restCommand::REST_GET: verbName = "GET"; break;
	case restCommand::REST_POST: verbName = "POST"; break;
	case restCommand::REST_PUT: verbName = "PUT"; break;
	}
	restDebugf2("Processing REST '%s' command for URL %s", verbName, command.m_OriginalUri);
	restDebugf3("%s", command.m_Data);
#endif

	restStatus::Enum status = m_Root->ProcessCommand(command);

#if !__NO_OUTPUT
	restDebugf2("Status: %d Time: %fms", (int)status, timer.GetMsTime());
#endif

	return status;
}

restServiceDirectory* restServiceManager::CreateDirectories(atArray<atString>& paths, int numComponentsToUse)
{
	restServiceDirectory* parentDir = m_Root;
	for(int i = 0; i < numComponentsToUse; i++)
	{
		const char* pathComp = paths[i].c_str();
		restService* kidSvc = parentDir->FindChild(pathComp);
		restServiceDirectory* kidSvcDir = NULL;
		if (!kidSvc) 
		{
			// Kid doesn't exist, create it
			kidSvcDir = rage_new restServiceDirectory(pathComp);
			parentDir->AddChild(*kidSvcDir);
		}
		else
		{
			kidSvcDir = dynamic_cast<restServiceDirectory*>(kidSvc);
			if (!restVerifyf(kidSvcDir, "Can't create subdirectories because %s already exists and isn't a restServiceDirectory", pathComp))
			{
				return NULL;
			}
		}

		parentDir = kidSvcDir;
	}

	return parentDir;
}

restServiceDirectory* restServiceManager::CreateDirectories(const char* dirPath)
{
	atArray<atString> pathComps;
	atString path(dirPath);
	path.Split(pathComps, '/');

	return CreateDirectories(pathComps, pathComps.GetCount());
}

restService* restServiceManager::FindService(atArray<atString>& paths, int numComponentsToUse)
{
	restServiceDirectory* parentDir = m_Root;
	for(int i = 0; i < numComponentsToUse; i++)
	{
		const char* pathComp = paths[i].c_str();
		restService* kidSvc = parentDir->FindChild(pathComp);
		restServiceDirectory* kidSvcDir = NULL;
		if (!kidSvc) 
		{
			return NULL;
		}
		else if (i == numComponentsToUse-1)
		{
			return kidSvc;
		}
		else // it needs to be a directory
		{
			kidSvcDir = dynamic_cast<restServiceDirectory*>(kidSvc);
			if (!restVerifyf(kidSvcDir, "Can't traverse through non-directories (path was %s)", pathComp))
			{
				return NULL;
			}
		}

		parentDir = kidSvcDir;
	}

	return NULL;
}

restService* restServiceManager::FindService(const char* svcPath)
{
	atArray<atString> pathComps;
	atString path(svcPath);
	path.Split(pathComps, '/');

	return FindService(pathComps, pathComps.GetCount());
}

void restServiceManager::RemoveAndDeleteService(const char* path)
{
	restService* svc = FindService(path);

	if (!svc)
	{
		restErrorf("Couldn't find service to delete");
		return;
	}

	restServiceDirectory* parent = svc->GetParent();
	bool done = false;
	while(!done)
	{
		parent->RemoveChild(*svc);
		delete svc;

		// Also delete empty directories
		if (parent->NumChildren() == 0)
		{
			svc = parent;
			parent = parent->GetParent();
		}
		else
		{
			done = true;
		}
	}
}


restSimpleService::restSimpleService(const char* name) : restService(name)
{
}

restStatus::Enum restSimpleService::ProcessCommand(restCommand& command)
{
	if (!command.IsAtEndOfPath())
	{
		command.ReportError("URL was to a sub-resource of a simple service. Not allowed.");
		return restStatus::REST_NOT_FOUND;
	}

	switch(command.m_Verb)
	{
	case restCommand::REST_GET:
		if (m_OnGet.IsBound())
		{
			return m_OnGet(command);
		}
		break;
	case restCommand::REST_POST:
		if (m_OnPost.IsBound())
		{
			return m_OnPost(command);
		}
		break;
	default:
		// Do nothing, report method not found error
		break;
	}
	command.ReportErrorMethodNotAllowed(
		(m_OnGet.IsBound() ? restCommand::REST_GET : 0) |
		(m_OnPost.IsBound() ? restCommand::REST_POST : 0),
		"Method not allowed for simple service");
	return restStatus::REST_METHOD_NOT_ALLOWED;
}

namespace rage
{
restService* restRegisterSimpleService(const char* pathToService, restSimpleCb onGet, restSimpleCb onPost /* = restSimpleService::CommandCb */)
{
	atArray<atString> pathComps;
	atString path(pathToService);
	path.Split(pathComps, '/');

	restServiceDirectory* parentDir = REST.CreateDirectories(pathComps, pathComps.GetCount() - 1);

	if (!parentDir)
	{
		restErrorf("Couldn't find or create parent directory for path %s", pathToService);
		return NULL;
	}

	restSimpleService* svc = rage_new restSimpleService(pathComps.Top().c_str());

	svc->m_OnGet = onGet;
	svc->m_OnPost = onPost;

	parentDir->AddChild(*svc);

	return svc;
}
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
// Web Server Code
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

#if RAGE_USE_WEB_SERVER

// Used for creating a restCommand on a worker thread and executing it on the main thread.
struct MarshalledRestCommand
{
	restCommand m_Command;
	sysIpcSema m_Sema;
	restStatus::Enum m_Status;

	MarshalledRestCommand() 
	{
		m_Sema = sysIpcCreateSema(0);
		m_Status = restStatus::REST_INTERNAL_ERROR;
	}

	~MarshalledRestCommand()
	{
		sysIpcDeleteSema(m_Sema);
	}
};

namespace {
	const int MAX_WEB_WORKER_THREADS = 64; // This is just the size of the ring buffer, not the actual number of threads.
	const char* MAX_WEB_WORKER_THREADS_STR = "64";
	mg_context* g_WebServer = NULL;
	struct mg_callbacks* g_WebServerCallbacks = NULL;
	sysLockFreeRing<MarshalledRestCommand, MAX_WEB_WORKER_THREADS> g_WebServerCommandQueue;
}

int mg_log_message_callback(const struct mg_connection *, const char *message)
{
	restDebugf1("%s", message);
	return 1;
}

void mg_log_request_callback(const struct mg_connection * conn, int reply_status_code)
{
	mg_request_info * ri = mg_get_request_info(const_cast<struct mg_connection*>(conn));
	restDebugf1("From=%d.%d.%d.%d:%d %s %s Result=%d Time=%.3fms", 
		(int)((ri->remote_ip >> 24) & 0xFF),
		(int)((ri->remote_ip >> 16) & 0xFF),
		(int)((ri->remote_ip >> 8) & 0xFF),
		(int)((ri->remote_ip >> 0) & 0xFF),
		ri->remote_port, 
		ri->request_method, ri->uri, 
		reply_status_code, (float)reinterpret_cast<ptrdiff_t>(ri->conn_data) * 0.001);
}

int mg_rest_callback(struct mg_connection *conn)
{
	// TODO: Decide when to handle a REST request and when to let the server handle things. Or should we use
	// mg_set_request_handler to add URIs that should get rest-ed?

	mg_request_info * request_info = mg_get_request_info(conn);

#if !__NO_OUTPUT
	sysTimer timer;
	u32 inAddr = request_info->remote_ip;
	s32 inPort = request_info->remote_port;
	restDebugf2("Received HTTP REST request from %d.%d.%d.%d:%d", 
		(inAddr >> 24), 
		((inAddr >> 16) & 0xFF), 
		((inAddr >> 8) & 0xFF),
		(inAddr & 0xFF),
		inPort);
#endif

	// Build up a REST command
	MarshalledRestCommand cmd;
	restCommand::Verb verb = (restCommand::Verb)0;

	if (!strcmp(request_info->request_method, "PUT"))
	{
		verb = restCommand::REST_PUT;
	}
	else if (!strcmp(request_info->request_method, "GET"))
	{
		verb = restCommand::REST_GET;
	}
	else if (!strcmp(request_info->request_method, "POST"))
	{
		verb = restCommand::REST_POST;
	}
	else if (!strcmp(request_info->request_method, "DELETE"))
	{
		verb = restCommand::REST_DELETE;
	}

	if (verb == 0)
	{
		send_http_error(conn, restStatus::REST_METHOD_NOT_ALLOWED, restStatus::GetStatusName(restStatus::REST_METHOD_NOT_ALLOWED), 
			"%s not allowed for REST services", request_info->request_method);
		return 1;
	}

	char* messageBody = NULL;
	if (verb == restCommand::REST_PUT || verb == restCommand::REST_POST)
	{
		int len = mg_get_content_length(conn);
		if (len == -1)
		{
			// Do nothing, NULL messageBody is OK here. Could be all the data is in the URI
		}
		else
		{
			messageBody = rage_new char[len + 1];
			sysMemSet(messageBody, 0x0, len + 1);
			int readBytes = mg_read(conn, messageBody, len);

			if (readBytes != len)
			{
				send_http_error(conn, restStatus::REST_BAD_REQUEST, restStatus::GetStatusName(restStatus::REST_BAD_REQUEST),
					"Content-Length and actual length of content did not match");
				delete messageBody;
				return 1;
			}
		}
	}

	datGrowBuffer gb;
	gb.Init(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL), datGrowBuffer::NULL_TERMINATE);
	gb.Preallocate(4 * 1024);

	char gbName[RAGE_MAX_PATH];

	fiDevice::MakeGrowBufferFileName(gbName, RAGE_MAX_PATH, &gb);

	fiStream* stream = ASSET.Create(gbName, "");

	// HACK undocumented civetweb info:
	// request_info->uri and request_info->query_string are really the same string - civetweb just puts a '\0' in place of the '?'
	// in the original URI. So undo that change to pass the full URI to cmd.Init()
	if (request_info->query_string)
	{
		restAssertf(request_info->uri + strlen(request_info->uri) + 1 == request_info->query_string, "URI and query string weren't adjacent in memory");
		const_cast<char*>(request_info->query_string)[-1] = '?';
	}

	if (!cmd.m_Command.Init(verb, request_info->uri, stream, messageBody))
	{
		send_http_error(conn, restStatus::REST_BAD_REQUEST, restStatus::GetStatusName(restStatus::REST_BAD_REQUEST), 
			"Couldn't decode the URL into a REST service request");
		delete messageBody;
		return 1;
	}

	// Add to the queue. The main thread will execute the command at some point in the future
	if (!g_WebServerCommandQueue.Push(&cmd))
	{
		send_http_error(conn, restStatus::REST_INTERNAL_ERROR, restStatus::GetStatusName(restStatus::REST_INTERNAL_ERROR),
			"Couldn't queue the REST command for execution. Is the main thread blocked? Not enough room in the ring buffer?");
		delete messageBody;
		return 1;
	}
	sysIpcWaitSema(cmd.m_Sema); // Wait here. Main thread signals after it's done with this command
	restStatus::Enum status = cmd.m_Status;

	cmd.m_Command.m_OutputStream->Flush();
	cmd.m_Command.m_OutputStream->Close();
	
	const char* outputBuffer = gb.Length() ? (char*)gb.GetBuffer() : "";

	if (!restStatus::IsOk(status))
	{
		send_http_error(conn, status, restStatus::GetStatusName(status), "%s", outputBuffer);
		delete messageBody;
		return 1;
	}

	mg_printf(conn,
		"HTTP/1.1 %d %s\r\n"
		"Content-Type: %s\r\n"
		"Content-Length: %d\r\n"
		"Connection: %s\r\n"
		"Access-Control-Allow-Origin: *\r\n"
		"\r\n",
		status, restStatus::GetStatusName(status),
		cmd.m_Command.m_OutputType, gb.Length(), "keep-alive"); // Change "close" to "keep-alive" when we support it.

	if (gb.Length())
	{
		mg_write(conn, gb.GetBuffer(), gb.Length());
	}

	mg_set_reply_status_code(conn, (int)cmd.m_Status); // Used for logging purposes
	reinterpret_cast<ptrdiff_t&>(request_info->conn_data) = (u32)timer.GetUsTime();

	delete messageBody;
	return 1;
}


void InitWebServer()
{
	if (!PARAM_webserverOn.Get())
	{
		return;
	}

	InitWinSock(sysParam::GetArgCount(), sysParam::GetArgArray());

	const char* listeningPort = "7890";
	PARAM_httpPort.Get(listeningPort);

	const char* numThreads = "4";
	PARAM_httpNumThreads.Get(numThreads);
	if (!Verifyf(atoi(numThreads) <= MAX_WEB_WORKER_THREADS, "Max number of http threads is %d", MAX_WEB_WORKER_THREADS))
	{
		numThreads = MAX_WEB_WORKER_THREADS_STR;
	}

	const char* throttle = "*=-1";
	PARAM_httpThrottle.Get(throttle);

	{
		USE_DEBUG_MEMORY();
		g_WebServerCallbacks = rage_new struct mg_callbacks;
		sysMemSet(g_WebServerCallbacks, 0, sizeof(struct mg_callbacks)); // NULL out all the pointers
		g_WebServerCallbacks->begin_request = mg_rest_callback;
		g_WebServerCallbacks->log_message = mg_log_message_callback;
		g_WebServerCallbacks->end_request = mg_log_request_callback;
	}

	const char *options[] = {
		"document_root", "/",
		"listening_ports", listeningPort,
		"enable_keep_alive", "yes",
		"num_threads", numThreads,
		"throttle", throttle,
		NULL
	};
	

	g_WebServer = mg_start(g_WebServerCallbacks, NULL, options);
	if (g_WebServer)
	{
		formatf(g_RestWebServerUri, "http://%s:%s/", fiDeviceTcpIp::GetLocalIpAddrName(), listeningPort);

		restDisplayf("Started debug web server, available at %s", g_RestWebServerUri);
	}
	else
	{
		restErrorf("Error initializing debug web server");
	}
}

void ShutdownWebServer()
{
	if (g_WebServer)
		mg_stop(g_WebServer);

	{
		USE_DEBUG_MEMORY();
		delete g_WebServerCallbacks;
	}

	ShutdownWinSock();
}

void UpdateWebServer()
{
	while(1)
	{
		MarshalledRestCommand* cmd = g_WebServerCommandQueue.Pop();
		if (cmd)
		{
			cmd->m_Status = REST.ProcessCommand(cmd->m_Command);
			sysIpcSignalSema(cmd->m_Sema);
		}
		else
		{
			break;
		}
	}
}

#endif // RAGE_USE_WEB_SERVER

#if 0 // Just for testing - not official bank service
restServiceBank::restServiceBank( const char* name )
: restService(name)
{
}

restStatus::Enum restServiceBank::ProcessCommand( restCommand& command )
{
	if (command.IsAtEndOfPath())
	{
		return restStatus::REST_NOT_FOUND;
	}

	// rebuild the bank path from the URI components
	atString uriPath;
	for(int i = command.m_UriIndex; i < command.m_UriComponents.GetCount(); i++)
	{
		uriPath += command.m_UriComponents[i];
		if (i < command.m_UriComponents.GetCount()-1)
		{
			uriPath += "/";
		}
	}

	bkWidget* widget = BANKMGR.FindWidget(uriPath.c_str());

	if (!widget)
	{
		return restStatus::REST_NOT_FOUND;
	}

	switch(command.m_Verb)
	{
	case restCommand::GET:
		{
			char buf[RAGE_MAX_PATH];
			widget->GetStringRepr(buf, RAGE_MAX_PATH);
			fputs(buf, command.m_OutputStream);
			return restStatus::REST_OK;
		}
	case restCommand::POST:
		{
			widget->SetStringRepr(command.m_Data);
			return restStatus::REST_OK_NO_CONTENT;
		}
	}

}
#endif

restServiceFileAccess::restServiceFileAccess( const char* name, const char* fileRoot )
: restService(name)
{
	m_FileRoot = fileRoot;
}

restServiceFileAccess::~restServiceFileAccess()
{
}

static const char* sm_ExtnToMimeTypeMap[][2] = 
{
	// Common rage types
	{"txt",			"text/plain"},
	{"xml",			"text/xml"},
	{"meta",		"text/xml"},

	// Common web types
	{"html",		"text/html"},
	{"htm",			"text/html"},
	{"css",			"text/css"},
	{"js",			"application/x-javascript"},
	{"gif",			"image/gif"},
	{"jpg",			"image/jpeg"},
	{"jpeg",		"image/jpeg"},
	{"png",			"image/png"},
	{"svg",			"image/svg+xml"},
	{"xslt",		"application/xml"},
	{NULL, NULL}
};

restStatus::Enum restServiceFileAccess::ProcessCommand( restCommand& command )
{
	// rebuild file path from URI components
	atString uriPath(m_FileRoot);

	for(int i = command.m_UriIndex; i < command.m_UriComponents.GetCount(); i++)
	{
		uriPath += command.m_UriComponents[i];
		if (i < command.m_UriComponents.GetCount()-1)
		{
			uriPath += "/";
		}
	}

	if (command.m_Verb != restCommand::REST_GET)
	{
		command.ReportErrorMethodNotAllowed(restCommand::REST_GET, "Method not allowed for file access service");
		return restStatus::REST_METHOD_NOT_ALLOWED;
	}

	fiStream* stream = ASSET.Open(uriPath.c_str(), "");

	if (stream)
	{
		// set output type based on extension?
		const char* extn = ASSET.FindExtensionInPath(uriPath.c_str());
		const char* mimeType = "text/plain";

		if (extn)
		{
			extn++; // skip the '.'
			for(int i = 0; sm_ExtnToMimeTypeMap[i][0] != NULL; i++)
			{
				if (!strcmp(sm_ExtnToMimeTypeMap[i][0], extn))
				{
					mimeType = sm_ExtnToMimeTypeMap[i][1];
					break;
				}
			}
		}
		command.m_OutputType = mimeType;

		char buf[2048];
		int bytesRead;

		do
		{
			bytesRead = stream->Read(buf, 2048);
			command.m_OutputStream->Write(buf, bytesRead);
		}
		while(bytesRead > 0);

		stream->Close();
		return restStatus::REST_OK;
	}
	else
	{
		return restStatus::REST_NOT_FOUND;
	}
}

#endif // __BANK
