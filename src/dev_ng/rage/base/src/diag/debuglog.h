//
// diag/debuglog.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

//	PURPOSE:
//		This file contains functions for generating a debug log containing arbitrary data,
//		and checking a previously generated log against the current run of the game.
//  TODO: Turn these functions into a class.

#ifndef DIAG_DEBUGLOG_H
#define DIAG_DEBUGLOG_H

#include "system/tls.h"

namespace rage {

class fiStream;
class Vector3;
class Matrix34;

// PURPOSE: Control a few compile options that are all necessary for testing replay.
// NOTES:	This must always be submitted as 0, only turn it on locally for replay tests.
#define	TEST_REPLAY		0

// Off by default.  Please do NOT commit code with this enabled,
// we've accidentally shipped several games with extra overhead in
// them because of this.
#define __DEBUGLOG		(HACK_MC4 && __BANK || TEST_REPLAY && !__SPU)
#define __DEBUGLOG_SPU	(HACK_MC4 && __SPU && (__BANK || TEST_REPLAY))
#define __DEBUGLOG_PPU	(HACK_MC4 && __PPU && (__BANK || TEST_REPLAY))
#define __DEBUGLOG_PS3	(__DEBUGLOG_SPU || __DEBUGLOG_PPU)

#if __DEBUGLOG
#define DEBUGLOG_ONLY(x) x
#else
#define DEBUGLOG_ONLY(x)
#endif

#if __DEBUGLOG_SPU
#define DEBUGLOG_SPU_ONLY(x) x
#else
#define DEBUGLOG_SPU_ONLY(x)
#endif

#if __DEBUGLOG_PPU
#define DEBUGLOG_PPU_ONLY(x) x
#else
#define DEBUGLOG_PPU_ONLY(x)
#endif

#if __DEBUGLOG_PS3
#define DEBUGLOG_PS3_ONLY(x) x
#else
#define DEBUGLOG_PS3_ONLY(x)
#endif

enum diagDebugLogSubsystem
{
	diagDebugLogPhysics		= 0x00000001,
	diagDebugLogPhysicsSlow	= 0x00000002,
	diagDebugLogAmbient		= 0x00000004,
	diagDebugLogAmbientSlow	= 0x00000008,
	diagDebugLogTrigger		= 0x00000010,
	diagDebugLogTriggerSlow	= 0x00000020,
	diagDebugLogVehicle		= 0x00000040,
	diagDebugLogVehicleSlow	= 0x00000080,
	diagDebugLogAI			= 0x00000100,
	diagDebugLogAISlow		= 0x00000200,
	diagDebugLogMisc		= 0x00000400,
};

#if __DEBUGLOG
extern __THREAD fiStream *gDebugLogFile;
extern __THREAD int gDebugLogDisabledCount;
extern u32 gDebugLogMask;
#elif __DEBUGLOG_SPU
extern char *gDebugLogFile;
static const int gDebugLogDisabledCount = 0;
static const u32 gDebugLogMask = 0xFFFFFFFF;
#endif


#if __DEBUGLOG

// PURPOSE: Initializes the debug log (the log is saved to C:\debug.log)
// PARAMS
//		bReplay - if true, opens an existing debug log and verifies that the current run's output matches the previous run.
//		abortOnFailure - not used
// SEE ALSO:
//		diagDebugLogShutdown, diagDebugLog
//	<FLAG Component>
void diagDebugLogInit(bool bReplay, bool abortOnFailure=false);

// PURPOSE: Closes the debug log
// RETURNS: Number of bytes written to the log
// SEE ALSO:
//		diagDebugLogInit, diagDebugLog
//	<FLAG Component>
void diagDebugLogShutdown();

#else // !__DEBUGLOG

inline void diagDebugLogInit(bool, bool) { }
inline void diagDebugLogShutdown() { }

#endif // !__DEBUGLOG

#if __DEBUGLOG_SPU
void diagDebugLogSPUInit(char* buffer, u16* currentSize, u16 maxSize);
void diagDebugLogSPUShutdown();
#else
inline void diagDebugLogSPUInit(char*, u16*, u16) { }
inline void diagDebugLogSPUShutdown() { }
#endif


#if __DEBUGLOG || __DEBUGLOG_SPU

// PURPOSE: Writes debugging info to the log
// PARAMS
//		tag - an identifier for this bit of debugging data
//		data - the data to write to the file
//		len - how much data to write
//		fatal - if true and a replay mismatch is found, halts program execution
// SEE ALSO:
//		diagDebugLog
void diagDebugLogRaw (int tag, const void * data, int len, bool fatal=true, int extraStackSkip = 0);

// PURPOSE: Writes out a Matrix34 to the debug log ignoring the w components of the contained vectors
void diagDebugLogRaw (int tag, const Matrix34 * data, bool fatal);

// PURPOSE: Returns true if the debug log is active.
__forceinline bool diagDebugLogIsActive(diagDebugLogSubsystem subsys) { return Unlikely(gDebugLogFile && !gDebugLogDisabledCount && (gDebugLogMask & subsys)); }

// PURPOSE: Writes debugging into to the log
// PARAMS:
//		T - (implicit) type of data to write
//		tag - an identifier for this bit of debugging data
//		data - the data to write
//		fata - if true and a replay mismatch is found, halts program execution
//	<FLAG Component>
template<class T> __forceinline void diagDebugLog (diagDebugLogSubsystem subsys, int tag, const T * data, bool fatal=true) { if (diagDebugLogIsActive(subsys)) diagDebugLogRaw(tag,data,sizeof(T),fatal); }

// PURPOSE: Writes out a Vector3 to the debug log ignoring the w component
__forceinline void diagDebugLog (diagDebugLogSubsystem subsys, int tag, const Vector3 * data, bool fatal=true)  { if (diagDebugLogIsActive(subsys)) diagDebugLogRaw(tag,data,3 * sizeof(float),fatal); }

// PURPOSE: Writes out a Matrix34 to the debug log ignoring the w components of the contained vectors
__forceinline void diagDebugLog (diagDebugLogSubsystem subsys, int tag, const Matrix34 * data, bool fatal=true) { if (diagDebugLogIsActive(subsys)) diagDebugLogRaw(tag, data, fatal); }

// PURPOSE: Writes out a Vec3V to the debug log ignoring the w component
__forceinline void diagDebugLog (diagDebugLogSubsystem subsys, int tag, const Vec3V * data, bool fatal=true)  { if (diagDebugLogIsActive(subsys)) diagDebugLogRaw(tag,data,3 * sizeof(float),fatal); }

// PURPOSE: Writes out a Mat34V to the debug log ignoring the w components of the contained vectors
__forceinline void diagDebugLog (diagDebugLogSubsystem subsys, int tag, const Mat34V * data, bool fatal=true) { if (diagDebugLogIsActive(subsys)) diagDebugLogRaw(tag, reinterpret_cast<const Matrix34*>(data), fatal); }

// PURPOSE: Writes out a tag without any data to the debug log
__forceinline void diagDebugLogTag (diagDebugLogSubsystem subsys, int tag, bool fatal=true) { if (diagDebugLogIsActive(subsys)) diagDebugLogRaw(tag, &tag, 0, fatal); }

#else

template<class T> __forceinline void diagDebugLog (diagDebugLogSubsystem, int, const T *, int = true) { }

#endif


#if __DEBUGLOG

// PURPOSE: Dump the entire log file starting from the specified file offset with callstacks optionally included
void diagDebugLogDump(int start, int stop, bool callstacks);

// PURPOSE: Temporarily disables the debug log, useful to ignore sections with asynchronous execution
__forceinline void diagDebugLogSetDisabled(bool bDisabled) { if (bDisabled) gDebugLogDisabledCount++; else gDebugLogDisabledCount--; Assert(gDebugLogDisabledCount>=0); }

// PURPOSE: Set the debug log mask
__forceinline void diagDebugLogSetMask(u32 newMask) { gDebugLogMask = newMask; }

// PURPOSE: Record the log data returned from an SPU
void diagDebugLogSPUBuffer(diagDebugLogSubsystem subsys, const char* addr, int size);

#else

__forceinline void diagDebugLogDump(int, int, bool) { }
__forceinline void diagDebugLogSetDisabled(bool) { }
__forceinline void diagDebugLogSetMask(u32) { }
__forceinline void diagDebugLogSPUBuffer(char*, int) { }

#endif


}	// namespace rage

#endif
