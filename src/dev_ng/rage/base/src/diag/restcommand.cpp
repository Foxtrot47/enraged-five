// 
// parser/restcommand.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "restcommand.h"

#include "file/stream.h"
#include "net/http.h"

//#if __BANK

using namespace rage;

const char* restStatus::GetStatusName(Enum e)
{
	// Names taken from RFC 2616: http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
	switch(e)
	{
	case REST_OK: return "OK";
	case REST_CREATED: return "Created";
	case REST_OK_NO_CONTENT: return "No Content";
	case REST_BAD_REQUEST: return "Bad Request";
    case REST_FORBIDDEN: return "Forbidden";
	case REST_NOT_FOUND: return "Not Found";
	case REST_METHOD_NOT_ALLOWED: return "Method Not Allowed";
	case REST_LENGTH_REQUIRED: return "Length Required";
	case REST_INTERNAL_ERROR: return "Server Error";
	}
	return NULL;
}

restCommand::restCommand()
: m_Verb(REST_GET)
, m_OriginalUri(NULL)
, m_Data(NULL)
, m_UriIndex(0)
, m_OutputStream(NULL)
, m_OutputType("application/xml")
{
}

bool restCommand::Init(Verb verb, const char* uri, fiStream* outputStream, const char* data /* = NULL */)
{
	m_Verb = verb;
	m_OutputStream = outputStream;
	m_Data = data;
	bool uriOk = SetUri(uri);
	return uriOk;
}

int UriDecode(char* uri)
{
	if (strchr(uri, '%'))
	{
		Warningf(" '%%' isn't recognised in restCommand::UriDecode()- someone needs to implement this!");
	}
	return (int)strlen(uri);
	/* Here's what I want to do , but UriDecode is in the rage net library and this is in rage core
	u32 newLen = strlen(uri);
	u32 numConsumed = 0;

	// TODO: Either make this not depend on the net library, or move it into the net library!
	bool success = netHttpRequest::UrlDecode(uri, &newLen, uri, newLen, &numConsumed);
	return success ? (int)newLen : -1;
	*/
}

bool ParseUriQuery( char* queryUrl, restCommand &command ) 
{
	char* startOfPair = &queryUrl[0];

	while (startOfPair && *startOfPair)
	{
		// Get the next '&', NULL it out
		char* nextPair = strchr(startOfPair, '&');
		if (nextPair)
		{
			*nextPair = '\0';
			nextPair++;
		}

		char* namePos = startOfPair;

		// now check for an '=' in the substring
		char* valuePos = strchr(startOfPair, '=');
		if (valuePos)
		{
			*valuePos = '\0';
			valuePos++;
		}

		if (namePos && *namePos)
		{
			if (UriDecode(namePos) == -1)
			{
				return false;
			}
			if (valuePos && UriDecode(valuePos) == -1)
			{
				return false;
			}
			command.m_Query.AddAttribute(namePos, valuePos);
		}
		else if (valuePos)
		{
			return false; // Got a "=1234" unnamed attribute
		}

		startOfPair = nextPair;
	}
	return true;
}

bool ParseUriComponents(restCommand& command)
{
	atString uriString(command.m_OriginalUri);
	uriString.Split(command.m_UriComponents, '/', true);
	command.m_UriIndex = 0;

	if (command.m_UriComponents.GetCount() == 0)
	{
		return true;
	}

	// if it starts with http://foo/svc... skip over the 'http' bit and the 'foo' bit
	if (command.m_UriComponents[0] == "http:")
	{
		if (command.m_UriComponents.GetCount() < 2)
		{
			return false;
		}
		command.m_UriIndex = 2;
		// Skip 2 elements because it splits out like this: http: // foo / svc...
	}

	atString& lastComponent = command.m_UriComponents[command.m_UriComponents.GetCount()-1];
	int startOfQuery = lastComponent.IndexOf('?');

	// Sample: ......./foo?xasd=qwer&ydf=&zsdf&wds=1234

	if (startOfQuery > -1)
	{
		int queryLen = lastComponent.GetLength() - startOfQuery;
		if (queryLen >= 1024)
		{
			return false; // Query part is too long
		}

		char queryBuf[1024]; 
		safecpy(queryBuf, lastComponent.c_str() + startOfQuery + 1);

		lastComponent.Truncate(startOfQuery);

		if (!ParseUriQuery(queryBuf, command))
		{
			return false;
		}

		// Special case for queries at the root level... We had one component in m_UriComponents but didn't really want it.
		// E.g. "/?x=1" or "http://10.0.0.1/?x=1"
		if (startOfQuery == 0 && (command.m_UriComponents.GetCount() - 1) == command.m_UriIndex)
		{
			command.m_UriIndex++;
		}

	}

	for(int i = 0; i < command.m_UriComponents.GetCount(); i++)
	{
		int newLen = UriDecode(const_cast<char*>(command.m_UriComponents[i].c_str()));
		command.m_UriComponents[i].Truncate(newLen);
	}

	return true;
}

bool restCommand::SetUri( const char* newUri )
{
	m_OriginalUri = newUri;

	return ParseUriComponents(*this);
}

void restCommand::ReportError(const char* message /* = "" */)
{
	fprintf(m_OutputStream, "<error>%s</error>", message ? message : "");
	m_OutputType = "application/xml";
}

void restCommand::ReportErrorMethodNotAllowed(int allowed /* = 0 */, const char* message /* = */ )
{
	fprintf(m_OutputStream, "Allow: ");
	if (allowed & REST_GET)
	{
		fprintf(m_OutputStream, "GET, ");
	}
	if (allowed & REST_POST)
	{
		fprintf(m_OutputStream, "POST, ");
	}
	if (allowed & REST_PUT)
	{
		fprintf(m_OutputStream, "PUT, ");
	}
	if (allowed & REST_DELETE)
	{
		fprintf(m_OutputStream, "DELETE, ");
	}
	fprintf(m_OutputStream, "\n");
	ReportError(message);
}

//#endif // __BANK
