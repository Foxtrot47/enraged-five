// 
// diag/printf_spu.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if __SPU

#include <stdio.h>
#include <stdarg.h>

#undef std

extern "C" {

struct __output_vars {
	int __count;
	char *__sprintf_ptr;
	size_t __sprintf_len;
	int __fill, __width, __scale;
	bool __left;
};


static void __sprintf(int ch,__output_vars *v) {
	if (v->__sprintf_len != 1) {
		*(v->__sprintf_ptr)++ = ch;
		++(v->__count);
		if (v->__sprintf_len)
			--(v->__sprintf_len);
	}
}



static void __itoa(int val,int base,void (*pr)(int,__output_vars *v),__output_vars *v) {
	char stack[64];
	int sp = 0;
	bool negative = false;
	if (val < 0) {
		negative = true;
		val = -val;
	}
	if (!val)
		stack[sp++] = '0';
	else 
		while (val) {
			stack[sp++] = '0' + (val % base);
			val /= base;		// integer division rounds toward zero, even for negative numbers.
		}
	v->__width -= negative;
	v->__width -= sp;
	if (!v->__left) while (v->__width-- > 0)
		pr(v->__fill,v);
	if (negative)
		pr('-',v);
	while (sp)
		pr(stack[--sp],v);
	if (v->__left) while (v->__width-- > 0)
		pr(v->__fill,v);
}

static void __ltoa(long val,int base,void (*pr)(int,__output_vars*),__output_vars *v) {
	char stack[64];
	int sp = 0;
	bool negative = false;
	if (val < 0) {
		negative = true;
		val = -val;
	}
	if (!val)
		stack[sp++] = '0';
	else 
		while (val) {
			stack[sp++] = '0' + (val % base);
			val /= base;		// integer division rounds toward zero, even for negative numbers.
		}
	v->__width -= negative;
	v->__width -= sp;
	if (!v->__left) while (v->__width-- > 0)
		pr(v->__fill,v);
	if (negative)
		pr('-',v);
	while (sp)
		pr(stack[--sp],v);
	if (v->__left) while (v->__width-- > 0)
		pr(v->__fill,v);
}

static char hex[] = "0123456789abcdef";
static char HEX[] = "0123456789ABCDEF";

static void __utoa(unsigned val,unsigned base,void (*pr)(int,__output_vars *),char *hex,__output_vars *v) {
	char stack[64];
	int sp = 0;
	if (!val)
		stack[sp++] = '0';
	else
		while (val) {
			stack[sp++] = hex[val % base];
			val /= base;
		}
	v->__width -= sp;
	if (!v->__left) while (v->__width-- > 0)
		pr(v->__fill,v);
	while (sp)
		pr(stack[--sp],v);
	if (v->__left) while (v->__width-- > 0)
		pr(v->__fill,v);
}

static void __ultoa(unsigned long val,unsigned base,void (*pr)(int,__output_vars *),__output_vars *v) {
	char stack[64];
	int sp = 0;
	if (!val)
		stack[sp++] = '0';
	else
		while (val) {
			stack[sp++] = hex[val % base];
			val /= base;
		}
	v->__width -= sp;
	if (!v->__left) while (v->__width-- > 0)
		pr(v->__fill,v);
	while (sp)
		pr(stack[--sp],v);
	if (v->__left) while (v->__width-- > 0)
		pr(v->__fill,v);
}

static void __pr_string(const char*s,void (*pr)(int,__output_vars*),__output_vars* v) { while (*s) pr(*s++,v); }

static void __pr_pad(int i,void (*pr)(int,__output_vars*),__output_vars *v) { while(i-- > 0)pr(' ',v); }

static void __ftoa(float val,void (*pr)(int,__output_vars*),__output_vars *v) {
	union {
		float f;
		unsigned u;
	} tmp;
	tmp.f = val;
	if ((tmp.u & 0x7f800000) == 0x7f800000) {
		if (tmp.u & 0x7fffff)
			__pr_string("NaN",pr,v);
		else
			__pr_string("Inf",pr,v);
		return;
	}

	if (v->__scale)
		v->__width -= v->__scale + 1;
	if (val > -1.0f && val < 0) {	// special case - lose sign when rounding
		while (v->__width-- > 2)
			pr(v->__fill,v);
		pr('-',v); pr('0',v);
	}
	else
		__ltoa((long)val,10,pr,v);
	if (v->__scale) {
		if (val < 0) val = -val;
		val -= (float) (long) val;
		pr('.',v);
		v->__width = v->__scale;
		v->__fill = '0';
		while (v->__scale--)
			val *= 10.0f;
		__ltoa((long)val,10,pr,v);
	}
}

static int __output(const char *fmt, va_list args, void (*pr)(int,__output_vars*),char *ptr,int len,__output_vars *v) {
	v->__count = 0;
	v->__sprintf_ptr = ptr;
	v->__sprintf_len = len;

	while (*fmt) {
		if (*fmt == '%') {
			++fmt;
			if (*fmt == '-') {
				++fmt;
				v->__left = true;
			}
			else
				v->__left = false;
			if (*fmt == '0') {
				v->__fill = '0';
				++fmt;
			}
			else
				v->__fill = 32;
			v->__width = 0;
			if (*fmt == '*') {
				v->__width = va_arg(args,int);
				++fmt;
			}
			else {
				while (*fmt >= '0' && *fmt <= '9')
					v->__width = 10 * v->__width + (*fmt++ - '0');
			}

			v->__scale = 0;
			if (*fmt == '.') {
				++fmt;
				if (*fmt == '*') {
					v->__scale = va_arg(args,int);
					++fmt;
				}
				else {
					while (*fmt >= '0' && *fmt <= '9')
						v->__scale = 10 * v->__scale + (*fmt++ - '0');
				}
			}
			else
				v->__scale = 6;	// printf
			switch (*fmt) {
			case 'b':
				__itoa(va_arg(args, int),2,pr,v);
				break;
			case 'c':
				pr(va_arg(args, int),v);
				break;
			case 'd':
			case 'i':
				__itoa(va_arg(args, int),10,pr,v);
				break;
			case 'l':
				if (fmt[1] == 'x') {
					++fmt;
					__ultoa(va_arg(args,unsigned long),16,pr,v);
				}
				else if (fmt[1] == 'b') {
					++fmt;
					__ultoa(va_arg(args,unsigned long),2,pr,v);
				}
				else
					__ltoa(va_arg(args, long),10,pr,v);
				break;
			case 'f':
				__ftoa((float) va_arg(args, double), pr, v);
				break;
			case 'e':
			case 'g':
				__ftoa((float) va_arg(args, double), pr, v);
				break;
			case 'o':
				__utoa(va_arg(args, int),8,pr,hex,v);
				break;
			case 'u':
				if (fmt[1] == 'l') {
					++fmt;
					__ultoa(va_arg(args, unsigned long),10,pr,v);
				}
				else
					__utoa(va_arg(args, unsigned),10,pr,hex,v);
				break;
			case 'p':
				v->__fill = '0';
				v->__width = 8;
			case 'x':
				__utoa(va_arg(args, int),16,pr,hex,v);
				break;
			case 'X':
				__utoa(va_arg(args, int),16,pr,HEX,v);
				break;
			case 's': {
				char *s = va_arg(args, char*);
				int len = 0; if (s) for(;s[len];len++);
				v->__fill = v->__width - (s ? len : 6);  // strlen("(null)") == 6;
				if(!v->__left) 
					__pr_pad(v->__fill, pr, v);
				__pr_string(s? s : "(null)",pr,v);
				if(v->__left)
					__pr_pad(v->__fill, pr, v);
				}
				break;
			case 'S': {
				unsigned short *s = va_arg(args, unsigned short *);
				int len = 0; if (s) for(;s[len];len++);
				v->__fill = v->__width - (s ? len : 6); // strlen("(null)") == 6;
				if(!v->__left)
					__pr_pad(v->__fill, pr, v);
				if(!s)
					__pr_string("(null)",pr,v);
				else
				{
					while(*s)
					{
						pr((char)*s,v);
						s++;
					}
				}
				if(v->__left)
					__pr_pad(v->__fill, pr, v);
				}
				break;
			default:
				pr(*fmt,v);
				break;
			}
			++fmt;
		}
		else
			pr(*fmt++,v);
	}
	return v->__count;
}

int std::sprintf(char * dest,const char* fmt,...) {
	va_list args;
	va_start(args,fmt);
	return vsprintf(dest,fmt,args);
}

int std::vsprintf(char *dest,const char *fmt,va_list args) {
	__output_vars v;
	int count = __output(fmt, args, __sprintf, dest, 0, &v);
	dest[count] = 0;
	return count;
}


int std::vsnprintf(char *dest,size_t len,const char *fmt,va_list args) {
	__output_vars v;
	int count = __output(fmt, args, __sprintf, dest, len, &v);
	dest[count] = 0;
	return count;
}


int std::snprintf(char *dest,size_t len,const char* fmt,...) {
	va_list args;
	va_start(args,fmt);
	return vsnprintf(dest,len,fmt,args);
}


}	// extern "C"

#endif		// __SPU

