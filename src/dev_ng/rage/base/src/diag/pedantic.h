// 
// diag/pedantic.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef DIAG_PEDANTIC_H
#define DIAG_PEDANTIC_H

namespace rage {

// TODO: Consider making this const int on __FINAL builds.
// extern int diagPedantic;

}

#endif	// DIAG_PEDANTIC_H
