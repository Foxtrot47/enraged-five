//
// diag/stats.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef DIAG_STATS_H
#define DIAG_STATS_H

#include "file/file_config.h"

#ifndef __STATS
// PURPOSE
//		The __STATS macro is used to conditionally compile memory usage code
//		from memstats.h, as well as profiling code from the profile module.
#define __STATS ((__DEV && !__XENON) && !__TOOL && !__SPU && !__RGSC_DLL)

#if __STATS
#define STATS_ONLY(x)	x
#else
#define STATS_ONLY(x)
#endif
#endif

#endif // DIAG_STATS_H
