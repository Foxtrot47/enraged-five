// 
// diag/trap.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DIAG_TRAP_H 
#define DIAG_TRAP_H 

#if __XENON
#include <ppcintrinsics.h>
#endif

#if __SPU
#include <spu_intrinsics.h>
#endif

namespace rage {

// For best results, if one of the parameters is a compile-time constant, it should be the second parameter.
// Admiral Ackbar would be unamused.

#define TrapZ(x)	TrapEQ((x),0)
#define TrapNZ(x)	TrapNE((x),0)

#if __FINAL || __PROFILE || defined(DISABLE_TRAPS) || (__XENON && __DEV && !defined(ENABLE_TRAPS_IN_THIS_FILE))

#define TRAP_ONLY(x)

#define ENABLE_TRAPS	0

#define TrapEQ(a,b)
#define TrapNE(a,b)
#define TrapGE(a,b)
#define TrapGT(a,b)
#define TrapLE(a,b)
#define TrapLT(a,b)

#elif defined(CONVERT_TRAPS_TO_ASSERTIONS)

#define TRAP_ONLY(x)	ASSERT_ONLY(x)

#define ENABLE_TRAPS	__ASSERT

#define TrapEQ(a,b)		Assertf(!((a)==(b)),"%d == %d",(a),(b))
#define TrapNE(a,b)		Assertf(!((a)!=(b)),"%d != %d",(a),(b))
#define TrapGE(a,b)		Assertf(!((a)>=(b)),"%d >= %d",(a),(b))
#define TrapGT(a,b)		Assertf(!((a)>(b)),"%d > %d",(a),(b))
#define TrapLE(a,b)		Assertf(!((a)<=(b)),"%d <= %d",(a),(b))
#define TrapLT(a,b)		Assertf(!((a)<(b)),"%d < %d",(a),(b))

#elif __SPU

#define TRAP_ONLY(x)	x

#define ENABLE_TRAPS	1

__forceinline void TrapEQ(int a,int b) {
	spu_hcmpeq(a,b);
}
__forceinline void TrapNE(int a,int b) {
	// if a>b or b>a, halt...
	spu_hcmpgt(a,b);
	spu_hcmpgt(b,a);
}

__forceinline void TrapGE(int a,int b) {
	spu_hcmpgt(a,b-1);
}
__forceinline void TrapGE(unsigned a,unsigned b) {
	spu_hcmpgt(a,b-1);
}

__forceinline void TrapGT(int a,int b) {
	spu_hcmpgt(a,b);
}
__forceinline void TrapGT(unsigned a,unsigned b) {
	spu_hcmpgt(a,b);
}

__forceinline void TrapLE(int a,int b) {
	spu_hcmpgt(b,a-1);
}
__forceinline void TrapLE(unsigned a,unsigned b) {
	spu_hcmpgt(b,a-1);
}

__forceinline void TrapLT(int a,int b) {
	spu_hcmpgt(b,a);
}
__forceinline void TrapLT(unsigned a,unsigned b) {
	spu_hcmpgt(b,a);
}


#elif __XENON && 0	// These traps just hang the debugger

#define TRAP_ONLY(x)	x

#define ENABLE_TRAPS	1

__forceinline void TrapEQ(int a,int b) {
	__tweq(a, b);
}

__forceinline void TrapNE(int a,int b) {
	__twne(a, b);
}

__forceinline void TrapGE(int a,int b) {
	__twge(a, b);
}
__forceinline void TrapGE(unsigned a,unsigned b) {
	__twlge(a, b);
}

__forceinline void TrapGT(int a,int b) {
	__twgt(a, b);
}
__forceinline void TrapGT(unsigned a,unsigned b) {
	__twlgt(a, b);
}

__forceinline void TrapLE(int a,int b) {
	__twle(a, b);
}
__forceinline void TrapLE(unsigned a,unsigned b) {
	__twlle(a, b);
}

__forceinline void TrapLT(int a,int b) {
	__twlt(a, b);
}
__forceinline void TrapLT(unsigned a,unsigned b) {
	__twllt(a, b);
}

#elif  __XENON

#define TRAP_ONLY(x)	x

#define ENABLE_TRAPS	1

// This will generate the trap in a single insn most of the time, since the immediate is only 16 bits
// and we're using a value that should already be in a register; should become sw r??,0xD1E(r0)
#define __builtin_trap2(a)	(*(int*)0xD1E = (a))

__forceinline void TrapEQ(int a,int b) { if (a==b) __builtin_trap2(a); }

__forceinline void TrapNE(int a,int b) { if (a!=b) __builtin_trap2(a); }

__forceinline void TrapGE(int a,int b) { if (a>=b) __builtin_trap2(a); }
__forceinline void TrapGE(unsigned a,unsigned b) { if (a>=b) __builtin_trap2(a); }

__forceinline void TrapGT(int a,int b) { if (a>b) __builtin_trap2(a); }
__forceinline void TrapGT(unsigned a,unsigned b) { if (a>b) __builtin_trap2(a); }

__forceinline void TrapLE(int a,int b) { if (a<=b) __builtin_trap2(a); }
__forceinline void TrapLE(unsigned a,unsigned b) { if (a<=b) __builtin_trap2(a); }

__forceinline void TrapLT(int a,int b) { if (a<b) __builtin_trap2(a); }
__forceinline void TrapLT(unsigned a,unsigned b) { if (a<b) __builtin_trap2(a); }

#else	// !__XENON; note that SNC generates conditional traps where possible.

#define TRAP_ONLY(x)	x

#define ENABLE_TRAPS	1

__forceinline void TrapEQ(int a,int b) { if (a==b) __builtin_trap(); }

__forceinline void TrapNE(int a,int b) { if (a!=b) __builtin_trap(); }

__forceinline void TrapGE(int a,int b) { if (a>=b) __builtin_trap(); }
__forceinline void TrapGE(unsigned a,unsigned b) { if (a>=b) __builtin_trap(); }

__forceinline void TrapGT(int a,int b) { if (a>b) __builtin_trap(); }
__forceinline void TrapGT(unsigned a,unsigned b) { if (a>b) __builtin_trap(); }

__forceinline void TrapLE(int a,int b) { if (a<=b) __builtin_trap(); }
__forceinline void TrapLE(unsigned a,unsigned b) { if (a<=b) __builtin_trap(); }

__forceinline void TrapLT(int a,int b) { if (a<b) __builtin_trap(); }
__forceinline void TrapLT(unsigned a,unsigned b) { if (a<b) __builtin_trap(); }

# if __64BIT
#undef TrapZ
#undef TrapNZ
__forceinline void TrapZ(int a) { if (a==0) __builtin_trap(); }
__forceinline void TrapZ(u32 a) { if (a==0) __builtin_trap(); }
__forceinline void TrapZ(u64 a) { if (a==0) __builtin_trap(); }
__forceinline void TrapNZ(int a) { if (a!=0) __builtin_trap(); }
__forceinline void TrapNZ(u32 a) { if (a!=0) __builtin_trap(); }
__forceinline void TrapNZ(u64 a) { if (a!=0) __builtin_trap(); }
__forceinline void TrapGE(u64 a,u64 b) { if (a>=b) __builtin_trap(); }
__forceinline void TrapGT(u64 a,u64 b) { if (a>b) __builtin_trap(); }
__forceinline void TrapLE(u64 a,u64 b) { if (a<=b) __builtin_trap(); }
__forceinline void TrapLT(u64 a,u64 b) { if (a<b) __builtin_trap(); }
# endif // sizeof...

#endif

} // namespace rage

#endif // DIAG_TRAP_H 
