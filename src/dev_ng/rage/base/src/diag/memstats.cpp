//
// diag/memstats.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include <stdarg.h>
#include "memstats.h"
#include "assert.h"
#include "output.h"
#include "stats.h"

#include "file/stream.h"
#include "string/string.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/timer.h"

using namespace rage;


#if __STATS
// handle to memory logging file handle, if open
fiStream *rage::g_LogMemoryStream = NULL;

// handle to load logging file handle, if open
fiStream *rage::g_LogLoadStream = NULL;
#endif

PARAM(memstats,"[memory] Enable hierarchical logging of memory usage.  Supply an optional parameter to specify filename.");
PARAM(loadstats,"[memory] Enable hierarchical logging of load times.  Supply an optional parameter to specify filename.");
PARAM(debugMemstats,"[memory] Print memory logging information to standard output.  Supply an optional parameter to increase logging detail.");

#if __STATS

const int NUM_LEVELS=10;
const int NUM_BUCKETS=16;

static int g_LogMemorySubtotals[NUM_LEVELS];
static int g_LogMemorySubtotalBuckets[NUM_LEVELS][NUM_BUCKETS];
static int g_LogMemoryIndent=0;

static float g_LogLoadSubtotals[NUM_LEVELS];

// The indent number of pushes not popped yet
static int g_LogLoadIndent=0;

static int g_Width=30;
static const int MAX_BUFFER_SIZE=256;

// Controls memstats output, 1 if output is desired, otherwise 0
static int s_DebugMemstats=0;

const char *g_MemoryBucketNames[NUM_BUCKETS] =
{
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};


static const char *getBucketName(int i)
{
	static const char *defaultName[NUM_BUCKETS] =
	{
		"[00]", "[01]", "[02]", "[03]", "[04]", "[05]", "[06]", "[07]",
		"[08]", "[09]", "[10]", "[11]", "[12]", "[13]", "[14]", "[15]",
	};
	if( g_MemoryBucketNames[i] )
		return g_MemoryBucketNames[i];
	else
		return defaultName[i];
}


static void indentMemory()
{
	for (int i=0;i<g_LogMemoryIndent;i++)
		fprintf(g_LogMemoryStream,"    ");
}



static void indentLoad()
{
	for (int i=0;i<g_LogLoadIndent;i++)
		fprintf(g_LogLoadStream,"    ");
}



// copy message into buffer, and add padding up to g_Width spaces
// truncate message if necessary to fit
void copyPadded(char buffer[MAX_BUFFER_SIZE],const char *message)
{
	bool done=false;
	for (int i=0;i<g_Width;i++)
	{
		if (message[i]=='\0')
			done=true;
		if (done)
			buffer[i]=' ';
		else
			buffer[i]=message[i];
	}
}



static float elapsedTime()
{
	static unsigned initialTime=0;
	if (initialTime==0)
		initialTime=sysTimer::GetSystemMsTime();
	unsigned currentTime=sysTimer::GetSystemMsTime();
	if (currentTime<initialTime)
		Errorf("currentTime<initialTime");
	return float(currentTime-initialTime)/1000.f;
}


/*
PURPOSE
	Called internally to get usage stats; will check if a resource
	build is in progress and use those numbers instead if appropriate.
*/
int LogMemoryUsed(int bucket)
{
	return (int) sysMemAllocator::GetCurrent().GetMemoryUsed(bucket);
}


void rage::diagLogMemoryOpen(const char *filename)
{
	AssertMsg(!g_LogMemoryStream , "LogMemoryOpen: Already has a stream open");

	if (PARAM_debugMemstats.Get())
	{
		s_DebugMemstats=1;
		PARAM_debugMemstats.Get(s_DebugMemstats);
	}
	else
		s_DebugMemstats=0;

	if (PARAM_memstats.Get())
	{
		const char *temp=NULL;
		if (PARAM_memstats.Get(temp) && temp[0])
			filename=temp;

		g_LogMemoryStream = fiStream::Create(filename);

		if (!g_LogMemoryStream)
			Errorf("Could not create %s",filename);
	}

	g_LogMemoryIndent=0;
	g_LogMemorySubtotals[g_LogMemoryIndent]=LogMemoryUsed(-1);
	for (int bucket=0;bucket<NUM_BUCKETS;bucket++)
		g_LogMemorySubtotalBuckets[g_LogMemoryIndent][bucket]=LogMemoryUsed(bucket);
}


void rage::diagLogLoadOpen(const char *filename)
{
	AssertMsg(!g_LogLoadStream , "LogLoadOpen: Already has a stream open");

	if (PARAM_loadstats.Get())
	{
		const char *temp=NULL;
		if (PARAM_loadstats.Get(temp) && temp[0])
			filename=temp;

		g_LogLoadStream = fiStream::Create(filename);

		if (!g_LogLoadStream)
			Errorf("Could not create %s",filename);
	}

	g_LogLoadIndent=0;
	g_LogLoadSubtotals[g_LogLoadIndent]=elapsedTime();
}



void rage::diagLogMemoryFlush()
{
	if (g_LogMemoryStream)
		g_LogMemoryStream->Flush();

	if (g_LogLoadStream)
		g_LogLoadStream->Flush();
}



void rage::diagLogMemoryClose()
{
	if (g_LogMemoryStream)
		g_LogMemoryStream->Close();

	g_LogMemoryStream=NULL;
}



void rage::diagLogLoadClose()
{
	if (g_LogLoadStream)
		g_LogLoadStream->Close();

	g_LogLoadStream=NULL;
}



void rage::diagLogMemoryMessage(const char *message, ...)
{
	if (g_LogMemoryStream || g_LogLoadStream)
	{
		const int bufSize = 128;
		char buf[bufSize];
		va_list args;
		va_start(args,message);
		vsprintf(buf,message,args);
		if (g_LogMemoryStream)
		{
			indentMemory();
			AssertMsg((StringLength(buf)<bufSize) , "LogMemoryMessage Overflow");
			fprintf(g_LogMemoryStream,"%s\r\n",buf);
		}
		if (g_LogLoadStream)
		{
			indentLoad();
			AssertMsg((StringLength(buf)<bufSize) , "LogMemoryMessage Overflow");
			fprintf(g_LogLoadStream,"%s\r\n",buf);
		}
		va_end(args);

		if (g_LogMemoryIndent<s_DebugMemstats)
			Debugf1(s_DebugMemstats,"\t>>> %s <<<",buf);
	}
}

void rage::diagLogMemory(const char *msg, ...)
{
	if (!g_LogMemoryStream && !g_LogLoadStream)
		return;

	va_list args;
	va_start(args,msg);
	char message[MAX_BUFFER_SIZE];
	vsprintf(message,msg,args);
	va_end(args);

	if (g_LogMemoryStream)
	{
		Assert(message[0]!='\r');
		Assert(message[0]!='\n');
		Assert(message[0]!=' ');

		char buffer[MAX_BUFFER_SIZE];
		copyPadded(buffer,message);

		int bytesTotal=LogMemoryUsed(-1);
		int bytes=bytesTotal-g_LogMemorySubtotals[g_LogMemoryIndent];
		float kilobytes=float(bytes)/1024.0f;

		if (bytes<1024 && bytes>-1024)
			sprintf(&buffer[g_Width],"%7d B",bytes);
		else if (kilobytes>=1024.0f || kilobytes<=-1024.0f)
			sprintf(&buffer[g_Width],"%7.3f M",kilobytes/1024.0f);
		else
			sprintf(&buffer[g_Width],"%7.2f K",kilobytes);

		if (g_LogMemoryIndent<s_DebugMemstats)
			Debugf1(s_DebugMemstats,"\t>>> %s <<<",buffer);

		indentMemory();
		fprintf(g_LogMemoryStream,buffer);

		g_LogMemorySubtotals[g_LogMemoryIndent]=bytesTotal;

		if (sysMemAllocator::GetCurrent().HasMemoryBuckets())
		{
			char details[MAX_BUFFER_SIZE];
			strcpy(details,"      (");
			bool showDetails=false;
			bool printPlus=false;
			bool printSpacePlus=false;

			g_LogMemorySubtotals[g_LogMemoryIndent]=bytesTotal;

			for (int bucket=0;bucket<NUM_BUCKETS;bucket++)
			{
				int bytesTotal=LogMemoryUsed(bucket);
				int bytes=bytesTotal-g_LogMemorySubtotalBuckets[g_LogMemoryIndent][bucket];
				float kilobytes=float(bytes)/1024.0f;

				if (bytes>0)
				{
					// don't print '+' before first bucket
					if (printPlus)
					{
						strcat(details,"+ ");
						printSpacePlus=true;
						printPlus=false;
					}
					else if (printSpacePlus)
						strcat(details," + ");
					else
					{
						if (bucket==0)
							printPlus=true;
						else
							printSpacePlus=true;
					}

					// only show details if there's data outside the default bucket
					if (bucket>0)
						showDetails=true;

					if (bytes<1024 && bytes>-1024)
						sprintf(&details[StringLength(details)],"%dB %s",bytes,getBucketName(bucket));
					else if (kilobytes>=1024.0f || kilobytes<=-1024.0f)
						sprintf(&details[StringLength(details)],"%.3fM %s",kilobytes/1024.0f,getBucketName(bucket));
					else
						sprintf(&details[StringLength(details)],"%.2fK %s",kilobytes,getBucketName(bucket));
				}

				g_LogMemorySubtotalBuckets[g_LogMemoryIndent][bucket]=bytesTotal;
			}

			strcat(details,")");

			AssertMsg(( StringLength(details) < MAX_BUFFER_SIZE ) , "Overflowed details string" );

			if (showDetails)
				fprintf(g_LogMemoryStream,details);
		}

		fprintf(g_LogMemoryStream,"\r\n");
	}

	if (g_LogLoadStream)
	{
		Assert(message[0]!='\r');
		Assert(message[0]!='\n');
		Assert(message[0]!=' ');

		char buffer[MAX_BUFFER_SIZE];
		copyPadded(buffer,message);

		float timeTotal=elapsedTime();
		float time=timeTotal-g_LogLoadSubtotals[g_LogLoadIndent];

		sprintf(&buffer[g_Width],"%7.3f sec\r\n",time);

		indentLoad();
		fprintf(g_LogLoadStream,buffer);

		g_LogLoadSubtotals[g_LogLoadIndent]=timeTotal;
	}
}


void rage::diagLogMemoryTotal(const char *msg, ...)
{
	if (!g_LogMemoryStream && !g_LogLoadStream)
		return;

	char message[MAX_BUFFER_SIZE];

	va_list args;
	va_start(args,msg);
	vsprintf(message,msg,args);
	va_end(args);

	if (g_LogMemoryStream)
	{
		indentMemory();
		int bytes=LogMemoryUsed(-1);

		char buffer[MAX_BUFFER_SIZE];
		copyPadded(buffer,message);

		fprintf(g_LogMemoryStream,"%s%7.3f M\r\n",buffer,float(bytes)/1048576.0f);

		if (g_LogMemoryIndent<s_DebugMemstats)
			Debugf1(s_DebugMemstats,"\t>>> %s%7.3f M\r\n <<<",message,float(bytes)/1048576.0f);
	}

	if (g_LogLoadStream)
	{
		indentLoad();

		char buffer[MAX_BUFFER_SIZE];
		copyPadded(buffer,message);

		float time=elapsedTime();
		fprintf(g_LogLoadStream,"%s%7.3f sec\r\n",buffer,time);
	}
}


void rage::diagLogMemoryPop()
{
	if (g_LogMemoryStream)
	{
		Assert(g_LogMemoryIndent>0);
		g_LogMemoryIndent--;
	}

	if (g_LogLoadStream)
	{
		Assert(g_LogLoadIndent>0);
		g_LogLoadIndent--;
	}
}


void rage::diagLogMemoryPush()
{
	if (g_LogMemoryStream)
	{
		g_LogMemoryIndent++;
		g_LogMemorySubtotals[g_LogMemoryIndent]=LogMemoryUsed(-1);

		for (int bucket=0;bucket<NUM_BUCKETS;bucket++)
			g_LogMemorySubtotalBuckets[g_LogMemoryIndent][bucket]=LogMemoryUsed(bucket);
	}

	if (g_LogLoadStream)
	{
		g_LogLoadIndent++;
		g_LogLoadSubtotals[g_LogLoadIndent]=elapsedTime();
	}
}



void rage::diagLogMemoryStartSection(const char *message, ...)
{
	if (g_LogMemoryStream || g_LogLoadStream)
	{
		va_list args;
		va_start(args,message);
		char buffer[MAX_BUFFER_SIZE];
		vsprintf(buffer,message,args);
		diagLogMemoryMessage(buffer);
		va_end(args);
	}
	diagLogMemoryPush();
}


void rage::diagLogMemoryEndSection(const char *message, ...)
{
	diagLogMemoryPop();
	if (g_LogMemoryStream || g_LogLoadStream)
	{
		va_list args;
		va_start(args,message);
		char buffer[MAX_BUFFER_SIZE];
		vsprintf(buffer,message,args);
		diagLogMemory(buffer);
		va_end(args);
	}
}



#endif

