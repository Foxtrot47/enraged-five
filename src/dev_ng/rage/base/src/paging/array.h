// 
// paging/array.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PAGING_ARRAY_H
#define PAGING_ARRAY_H

#include "data/struct.h"
#include "diag/trap.h"
#include "paging/base.h"

#include <string.h>

namespace rage {

/*	An array of data that can live in defragmentable memory.  Always created zero-initialized, and no constructor
	or destructor is (currently) ever invoked on the contained array.  This could be revisited if necessary. */
template <class _Type> class pgArray : public pgBase
{
public:
	// Solely to get the virtual pointer correct so we don't crash on destruction.
	pgArray<_Type>(size_t c) : m_Count(c), pad(0) { }

	pgArray<_Type>(datResource&) { }

	static size_t ComputeSize(size_t count)
	{
		return sizeof(pgArray<_Type>) + count * sizeof(_Type);
	}

	static pgArray<_Type>* Create(size_t count) 
	{
		pgArray<_Type> *result = (pgArray<_Type>*) rage_new char[ComputeSize(count)];
		rage_placement_new(result) pgArray<_Type>(count);
		memset(result->m_Elements, 0, count * sizeof(_Type));
		return result;
	}

	size_t GetCount() const { return m_Count; }

	// Not including operator[] here is intentional; pgArrays are generally already pointers,
	// so you end up having to use the weird pObj->operator[](value) syntax anyway.
	const _Type& GetElement(unsigned idx) const { TrapGE(idx,m_Count); return m_Elements[idx]; }

	_Type& GetElement(unsigned idx) { TrapGE(idx,(unsigned int)m_Count); return m_Elements[idx]; }

	const _Type* GetElements() const { return this? m_Elements : 0; }

	_Type* GetElements() { return this? m_Elements : 0; }

	IMPLEMENT_PLACE_INLINE(pgArray<_Type>)

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		// datswap all of the elements in the array
		for (int i=0; i<(int)(m_Count); i++) {
			::rage::datSwapper(m_Elements[i]);
		}

		pgBase::DeclareStruct(s);
		STRUCT_BEGIN(pgArray<_Type>);
		STRUCT_FIELD(m_Count);
		STRUCT_IGNORE(pad);
		STRUCT_PAD_TO_ALIGNMENT(pad, __alignof(_Type));	   // assumes that pgArray<_Type> has the same alignment as _Type, because of the 0-length array
		// Note: m_Elements is ignored here, since it should have 0 size (and we swapped all the elements earlier)
		STRUCT_END();
	}
#endif

private:
	size_t m_Count, pad;
	_Type m_Elements[0];
};

}	// namespace rage

#endif	// PAGING_Array_H

