// 
// paging/dictionary.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

// Hack to avoid LNK4221
// "This object file does not define any previously undefined public symbols, so it will not be used by any link operation that consumes this library"
#if !__WIN32PC || __ASSERT

#include "paging/dictionary.h"

#if __ASSERT
rage::sysIpcCurrentThreadId rage::pgDictionaryBase::sm_ListOwnerThread = sysIpcCurrentThreadIdInvalid;
#endif

#endif
