// 
// paging/base.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

/*	Defragmentation system theory of operation.

	Anything that may be defragmentable must derive from pgBase.  There are two different defragmentation methods available, with
	different tradeoffs.

	The first method relies on using [SAFE_]ADD_KNOWN_REF and [SAFE_]REMOVE_KNOWN_REF and UPDATE_KNOWN_REF whenever we have a
	pointer to a potentially defragmentable object.  The template classes pgOwner (pointer to defragmentable memory that we own,
	or in particular are responsible for resource constructing) and pgRef (pointer to defragmentable memory we do not own)
	manage calling ADD/REMOVE/UPDATE_KNOWN_REF appropriately.

	These pointers are just normal pointers, but the system tracks their location; any time we add a known reference, we record
	the address of the pointer in a pool.  Every pointer to this object needs its own pool entry, so this method can consume a
	lot of pool space when an object is heavily referenced.  However, we have a performance advantage because all of the pointers
	to this defragmentable object are just normal pointers, which improves SPU interoperability for starters.

	The second method relies on a single global table of pointers to defragmentable objects.  All actual references to the
	defragmentable object are really just either the index of the pointer in the global table, or a pointer to that pointer.
	This method requires an extra level of indirection on any access, but it has the distinct advantage of allowing any number
	of pointers to the same defragmentable memory block with no additional storage.  The pgHandle template and pgHandleIndex
	templates manage the details here.

	When memory is being defragmented, higher level code does the following:
	1. Copies memory blocks from original to new location
	2. Calls pgBase::PatchAllReferences with the array of memory blocks that were copied
	3. Calls the resource constructor (with the defragmentation flag set) on the toplevel resource that had
	   one or more memory blocks moved.

   Step three handles all of the usual internal pointers within a resource to other parts of that resource.  Step two is
   the tricky part -- for every block being moved, we need to quickly identify any pointers affected by memory copy.

   For tracked pointers (the first method above, pgOwner/pgReg), we need to handle all of these cases:
   1. Normal pointer to defragmentable memory
   2. Defragmentable pointer to normal memory
   3. Defragmentable pointer to defragmentable memory

   The original implementation simply walked the entire pointer table and tried to fix up both the address of the pointer
   (in case the memory block containing the pointer was being moved) and the address being pointed to (in case the memory
   block the pointer was pointing at moved).  While this worked (we think), it was pretty slow.  The new implementation
   allows each tracked pointer to be on up to two lists.  The first list is grouped by address of the pointer, and the
   second list is grouped by the contents (address being pointed at) of the pointer.  For the first and second cases
   immediately above, the pointer will actually only be on one of the two lists.

   The double indirect pointers (the second method above, pgHandle/pgHandleInedx) are a bit simpler.  In those cases
   they are only ever on a single linked list based on the contents of the pointer; the pointer itself is in a single
   global table in undefragmentable memory, so only case 1 above applies.
*/

#define NO_datResourcePatch_HACK

#include "base.h"
#include "handle.h"
#include "ref.h"
#include "data/resourceheader.h"
#include "data/safestruct.h"
#include "data/struct.h"
#include "math/intrinsics.h"
#include "paging/rscbuilder.h"
#include "system/buddyheap.h"
#include "system/buddyallocator.h"
#include "system/memmanager.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/cache.h"
#include "system/criticalsection_spu.h"
#include "system/endian.h"
#include "system/multiallocator.h"
#include "system/tinyheap.h"
#include "system/tasklog.h"
#include "system/param.h"
#include "system/platform.h"
#include "system/stack.h"
#include "system/virtualallocator.h"
#include "diag/channel.h"
#include "diag/output.h"
#include "diag/trap.h"
#include "paging/knownrefpool.h"
#include "atl/map.h"

#include <algorithm>

#if __PPU
#include <sys/sys_fixed_addr.h>
#endif

#if !__FINAL
#if __PS3
#include <sys/dbg.h>
#else
#include "system/xtl.h"
#endif
#endif

#if RSG_ORBIS 
#include <kernel.h>
#endif


#define pgDisplayf(fmt,...)		//  Displayf
#define BASE_TASKLOG_ONLY(x)	// TASKLOG_ONLY(x)

#define NO_REFERENCE_TRACKING	(__RESOURCECOMPILER || __TOOL)

// ====================== Platform Dependent =========================
// THIS ONE IS JUST FOR TEXTURES AND RENDER TARGETS
extern const rage::u32 pgMaxHandles = (__WIN32PC || __64BIT || RSG_ORBIS) ? 40000 : 8000;
// THIS ONE IS FOR EVERYTHING ELSE
extern const rage::u32 pgMaxHandles2 = (__WIN32PC || __64BIT || RSG_ORBIS) ? 32767 : 6000;
// ====================== Platform Dependent =========================

rage::pgBase *g_pgHandleArray[pgMaxHandles] ALIGNED(128);		// For better DMA performance
rage::pgBase *g_pgHandleArray2[pgMaxHandles2] ALIGNED(128);		// For better DMA performance
#if PGHANDLE_REF_COUNT
	rage::u16 g_pgHandleRefCountArray[pgMaxHandles];  // in .bss so this will be automatically zeroed for us
#endif


namespace rage {

#if !HAS_PADDED_POINTERS
CompileTimeAssertSize(datBase,4,8);
CompileTimeAssertSize(pgBase,8,16);		// If this changes, code in base_spu.cpp will need to be updated
CompileTimeAssertSize(pgBaseRefCounted,12,24);
#endif

#if ENABLE_KNOWN_REFERENCES
sysCriticalSectionToken s_KnownRefToken;
pgBaseKnownReferencePool s_KnownReferencePool;
#endif

u32 pgBase::sm_MapSize;

// Setting this to something smaller than 12 (a 4k page) is probably pointless.
// We could set it to something larger and we'd walk fewer lists while patching large blocks,
// but we'd have to do additional testing to make sure the pointer really is in the active range,
// and moving items between lists (instead of just moving the entire lists like we do now) would
// be a lot more work.  The main reason, then, to make it larger, is to save a bit of memory for 
// the head pointers.
const int TrackShift = __PS3? 12 : 13;
const size_t TrackStep = 1 << TrackShift;

#if ENABLE_KNOWN_REFERENCES

#if RSG_PC

#if USE_SPARSE_MEMORY
static sysMemSparseAllocator *s_TrackedHeap;
#else
static sysMemGrowBuddyAllocator *s_TrackedHeap;
static char *s_TrackStart = (char*) 4096, *s_TrackEnd = (char*)8192;
static size_t s_MemoryReferenceCount = (0x7FFFFFFF) >> TrackShift;
#endif

void pgBase::SetTrackedHeap(sysMemAllocator &trackedHeap)
{
#if USE_SPARSE_MEMORY
	s_TrackedHeap = dynamic_cast<sysMemSparseBuddyAllocator&>(trackedHeap).GetInternalAllocator();
#else
	s_TrackedHeap = dynamic_cast<sysMemGrowBuddyAllocator*>(&trackedHeap);
	s_TrackStart = (char*)s_TrackedHeap->GetHeapBase();
	s_TrackEnd = s_TrackStart + 0x7FFFFFFF;
	s_MemoryReferenceCount = (s_TrackEnd - s_TrackStart) >> TrackShift;
	TrapGE(s_MemoryReferenceCount,(size_t)sysBuddyHeap::c_INVALID);
#endif
}
	
bool pgBase::IsTrackedAddress(void *ptr)
{
	//Assert(s_TrackedHeap != NULL);
	if (s_TrackedHeap == NULL)
		return false;

	return s_TrackedHeap->IsValidPointer(ptr);
}

#else

// No pointers are tracked by default.
static char *s_TrackStart = (char*) 4096, *s_TrackEnd = (char*)8192;
static size_t s_MemoryReferenceCount;

void pgBase::SetTrackedHeap(sysMemAllocator &trackedHeap)
{
	// Tell game systems where the defragmentable heap lives.
	s_TrackStart = (char*) trackedHeap.GetHeapBase();
	s_TrackEnd = s_TrackStart + trackedHeap.GetHeapSize();
	s_MemoryReferenceCount = (s_TrackEnd - s_TrackStart) >> TrackShift;
	TrapGE(s_MemoryReferenceCount,(size_t)sysBuddyHeap::c_INVALID);
}

bool pgBase::IsTrackedAddress(void *ptr)
{
	return (char*)ptr >= s_TrackStart && (char*)ptr < s_TrackEnd;
}

#endif

#endif	// ENABLE_KNOWN_REFERENCES


#if __DEV
pgRef<pgBase> g_WatchedBaseObject;
#endif

NOTFINAL_ONLY(PARAM(defragcheck,"Enable expensive defrag runtime checks"));

// #pragma option O=0

#if ENABLE_KNOWN_REFERENCES
static inline bool IsKnownReference(pgBaseNode *node)
{
	return s_KnownReferencePool.Contains(node);
}
#else
static inline bool IsKnownReference(pgBaseNode *)
{
	return false;
}
#endif

static /*inline*/ pgBasePageMap* IsPageMap(pgBaseNode *node)
{
	if (node)
		if (!IsKnownReference(node))
			if (static_cast<pgBasePageMap*>(node)->Subtype==0)
				return static_cast<pgBasePageMap*>(node);
				
	return NULL;
}

static inline pgBaseMetaData* IsMetaData(pgBaseNode *node)
{
	return node && !IsKnownReference(node) && static_cast<pgBasePageMap*>(node)->Subtype!=0? static_cast<pgBaseMetaData*>(node) : 0;
}

/*
	If an object is a toplevel resource, the pgBasePageMap will always be the first item in the list.
	Either way, zero or more pgBaseMetaData objects will be next.
	Anything after that will be pgBaseKnownReferences, which are never resourced.
*/
pgBase::pgBase() {
	// All nodes are now kept in external pools allocated at runtime, so no fixups are necessary here.
#if !ENABLE_KNOWN_REFERENCES
	if (datResource_sm_Current && m_FirstNode) {
		datResource_sm_Current->PointerFixup(m_FirstNode);
		// We're not making this defragmentable, but we still need to pack the map information into the structure.
		MakeDefragmentable(datResource_sm_Current->GetMap(),false);

#if RSG_ORBIS
		// Change physical chunks to be GARLIC WB and read-only
		const datResourceMap &map = datResource_sm_Current->GetMap();
		sysMemVirtualAllocator *const va = sysMemVirtualAllocator::sm_Instance;
		for (int i=map.VirtualCount; i<map.VirtualCount+map.PhysicalCount; i++) {
			void *const ptr = map.Chunks[i].DestAddr;
			va->SetMemTypeKeepContents(ptr,va->GetSize(ptr),MEMTYPE_CPU_RO|MEMTYPE_CPU_WB|MEMTYPE_GPU_RO|MEMTYPE_GPU_GARLIC);
		}
#endif
	}
	else
		m_FirstNode = NULL;
#elif NO_REFERENCE_TRACKING
	// No longer need a sentinel value here, it will be rewritten directly by resourcing code if it really is toplevel resource.
	m_FirstNode = NULL;
#else
	if (datResource_sm_Current) {

		// If we are doing defrag, then another thread could be modifying the
		// linked list pointed to by m_FirstNode, so need to lock the
		// s_KnownRefToken critsec.  If we are doing the object construction for
		// the first time, then nothing else should have access to it yet, so we
		// can skip the critsec lock.
		if (datResource_IsDefragmentation)
			s_KnownRefToken.Lock();

		if (!IsKnownReference(m_FirstNode))
			datResource_sm_Current->PointerFixup(m_FirstNode);

		// Make sure it's in one of the address ranges we expect; the range check is only for old resources.
		AssertMsg((size_t)m_FirstNode != ~0U,"Old pagemap resource detected, you REALLY need new assets (will crash now...)");

		pgBaseNode **pNext = &m_FirstNode;
		pgBasePageMap *pm = IsPageMap(m_FirstNode);
		if (pm) {
			if (!datResource_IsDefragmentation) {
				// Make sure enough space was allocated for it.
				Assert(pm->VirtualCount + pm->PhysicalCount >= datResource_sm_Current->GetMap().VirtualCount + datResource_sm_Current->GetMap().PhysicalCount);

				const datResourceMap &map = datResource_sm_Current->GetMap();
				Assert(map.Chunks[0].DestAddr == this);
				MakeDefragmentable(map,false);
			}
			pNext = &m_FirstNode->Next;
		}

		// Fix up chain of metadata items, if any, stopping at the first known reference.
		while (*pNext && !IsKnownReference(*pNext)) {
			datResource_sm_Current->PointerFixup(*pNext);
			pgBaseMetaData *md = IsMetaData(*pNext);

			pgBaseMetaDataType *type = pgBaseMetaDataType::Lookup(md->Type);
			if (!type)
				Quitf("Unknown type %x during meta data defragmentation!",md->Type);
			else
				type->Place(md,*datResource_sm_Current);
			pNext = &md->Next;
		}

		if (datResource_IsDefragmentation)
			s_KnownRefToken.Unlock();
	}
	else	// Not a resource construction, so make sure the pointer is properly initialized to zero.
		m_FirstNode = NULL;
#endif
}


void pgBase::Destroy() {
#if !NO_REFERENCE_TRACKING
# if ENABLE_KNOWN_REFERENCES
	BASE_TASKLOG_ONLY(s_TaskLog.Log('~pgB',(u32)this));
	ClearAllKnownRefs();
# endif

	// By now, if there's anything left the front of the chain will be the page map.
	if (IsPageMap(m_FirstNode)) {
		datResourceMap map;
		RegenerateMap(map);
		FreeMemory(map);
		//m_FirstNode = NULL;
	}
#endif
	// This cannot be here until the handle index itself lives in pgBase!
	// pgHandleBase::Unregister(this);
}


void pgBase::FreeMemory(const datResourceMap &map) 
{
	sysMemAllocator* pAllocator;

#if RESOURCE_HEADER
	if (map.IsOptimized())
	{
		pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL);

		for (u32 i = 0; i < map.VirtualCount; ++i)
		{
			void* ptr = map.Chunks[i].DestAddr;
			sysMemManager::GetInstance().DeleteNodeInfo(ptr);
			IF_DEBUG_MEMORY_FILL_N(sysMemSet(ptr, 0xEE, map.Chunks[i].Size), DMF_RESOURCE);
			pAllocator->Free(ptr);
		}		
	}
	else
#endif
	{
		pAllocator = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);

#if !ENABLE_KNOWN_REFERENCES
		if (pAllocator->SupportsAllocateMap()) {
			pAllocator->FreeMap(map);
			return;
		}
#endif

		for (u32 i=0; i<map.VirtualCount; i++) 
		{
#if !__FINAL
			// Make sure memory is totally nuked.  Note that we're stomping on the memory pointed to
			// by "this" but we don't reference anything else after this.
			if (IsMemoryTracked(map.Chunks[i].DestAddr,map.Chunks[i].Size))
				Quitf("Trying to FreeMemory tracked memory, chunk at %p, base at %p", map.Chunks[i].DestAddr, map.VirtualBase);

#if RESOURCE_HEADER
			Assert(!sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL)->IsValidPointer(map.Chunks[i].DestAddr));
#endif
			IF_DEBUG_MEMORY_FILL_N(sysMemSet(map.Chunks[i].DestAddr, 0xEE, map.Chunks[i].Size), DMF_RESOURCE);

			// Break glass in case of 0xEE Quitf
			// Displayf("  pgBase::~pgBase deleting page %d/%d from %p-%p",i,map.VirtualCount,map.Chunks[i].DestAddr,(char*)map.Chunks[i].DestAddr+map.Chunks[i].Size-1);
#endif
#if USE_SPARSE_MEMORY
			// Free the root virtual allocation last below (hack for sparse buddy allocator)
			if (i)
#endif
				pAllocator->Free(map.Chunks[i].DestAddr);
		}
	}	
	
	pAllocator = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
	for (u32 i=map.VirtualCount; i< u32(map.VirtualCount)
#if !FREE_PHYSICAL_RESOURCES
		+ u32(map.PhysicalCount)
#endif // !FREE_PHYSICAL_RESOURCES		
		; i++) 
	{
		pAllocator->Free(map.Chunks[i].DestAddr);
	}

#if USE_SPARSE_MEMORY
	// If there was any virtual memory (always the case, but let's be sure) then free the root allocation last.
	// DeferredFree causes the memory supplied to not actually be freed until the *next* allocator call.
	// This should be safe to do on all configurations but I want to minimize chances of breakage.
	if (map.VirtualCount)
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->DeferredFree(map.Chunks[0].DestAddr);
#endif
}

void pgBase::Validate(u32 NOTFINAL_ONLY(userData)) {
#if !__FINAL
	// By now, if there's anything left the front of the chain will be the page map.
	// If known references are disabled, the first node is always the page map if it's nonzero
#if ENABLE_KNOWN_REFERENCES
	if (IsPageMap(m_FirstNode)) 
#else
	if (m_FirstNode)
#endif
	{
		datResourceMap map;
		RegenerateMap(map);
		char debugName[128];

		u32 chunkCount = map.VirtualCount + map.PhysicalCount;
		for (u32 i=0; i<chunkCount; i++) {
			if (!sysMemAllocator::GetMaster().GetSize(map.Chunks[i].DestAddr))
				Quitf("Memory chunk #%d %p for resource %p (%s) has become invalid - it may have been freed", i, map.Chunks[i].DestAddr, this, GetDebugName(debugName, sizeof(debugName)));

			u32 foundUserData = sysMemAllocator::GetMaster().GetUserData(map.Chunks[i].DestAddr);
			if (foundUserData != userData)
				Quitf("Memory chunk #%d %p for resource %p (%s) has mismatching userdata - it's supposed to be %d, but we got %d", i, map.Chunks[i].DestAddr,
					this, GetDebugName(debugName, sizeof(debugName)), userData, foundUserData);
		}
	}
#endif // !__FINAL
}

void pgBase::MakeDefragmentable(const datResourceMap &map,bool needUnlock) 
{
	pgBasePageMap *pm = IsPageMap(m_FirstNode);
	Assertf(pm,"Attempting to defragment something (%p->%p) that isn't a toplevel resource.  This will probably crash later.",this,m_FirstNode);

	Assert(map.Chunks[map.RootVirtualChunk].DestAddr == map.VirtualBase);
	pm->VirtualCount = map.VirtualCount;
	pm->PhysicalCount = map.PhysicalCount;
	u32 mapCount = map.VirtualCount + map.PhysicalCount;
	pm->RootVirtualChunk = map.RootVirtualChunk;
	pm->Subtype = 0;

	u32 i = 0;

#if RESOURCE_HEADER
	if (map.IsOptimized())
	{
		if (needUnlock)
			sysMemAllocator::GetCurrent().UnlockBlock(map.Chunks[i].DestAddr);

		// Compress the relevant map information into a single u32.
		pm->PageInfo[i] = (size_t) map.Chunks[i].DestAddr;
		i = 1;
	}
#endif

	for (; i<mapCount; i++) 
	{
		if (needUnlock)
			sysMemAllocator::GetCurrent().UnlockBlock(map.Chunks[i].DestAddr);
		// Compress the relevant map information into a single u32.
		size_t addr = (size_t) map.Chunks[i].DestAddr;
		size_t leafSize = ((i < map.VirtualCount)?g_rscVirtualLeafSize:g_rscPhysicalLeafSize);
		u32 sizeLog2 = _FloorLog2((int)(map.Chunks[i].Size / leafSize));
		// Trap, don't assert, so we don't get strangely hosed in release builds again.
		TrapGE(sizeLog2,16U);
		TrapNZ(addr & sizeLog2);
		pm->PageInfo[i] = addr | sizeLog2;
	}
}

void pgBase::RegenerateMap(datResourceMap &outMap) const {
	pgBasePageMap *pm = IsPageMap(m_FirstNode);

#if !__NO_OUTPUT
	if (!pm)
	{
		Errorf("RegenerateMap called on invalid ptr %p, m_FirstNode=%p", this, m_FirstNode);
#if __TASKLOG
		s_TaskLog.Dump();
#endif // __TASKLOG

		Errorf("Allocator size of this pointer: %d", (u32) sysMemAllocator::GetMaster().GetSize(this));
		Errorf("User data for this pointer: %x", sysMemAllocator::GetMaster().GetUserData(this));
		sysMemAllocator::DebugPrintAllocInfo(this);
		Quitf("Trying to generate a map from a pointer that is not the top-level pointer of a resource. TTY has more.");
	}
#endif // !__NO_OUTPUT

	outMap.VirtualCount = pm->VirtualCount;
	outMap.PhysicalCount = pm->PhysicalCount;
	outMap.RootVirtualChunk = pm->RootVirtualChunk;

	u32 i=0;

#if RESOURCE_HEADER
	if (outMap.VirtualCount == 1)
	{
		void* ptr = (void*) (pm->PageInfo[i] & ~15);
		sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL);

		if (pAllocator->IsValidPointer(ptr))
		{			
			outMap.Chunks[0].DestAddr = outMap.Chunks[0].SrcAddr = ptr;
			outMap.Chunks[0].Size = pAllocator->GetSize(ptr);			
			i = 1;
		}
	}
#endif

	for (; i<outMap.VirtualCount; i++) 
	{
		outMap.Chunks[i].DestAddr = outMap.Chunks[i].SrcAddr = (void*) (pm->PageInfo[i] & ~15);
		outMap.Chunks[i].Size = g_rscVirtualLeafSize << (pm->PageInfo[i]&0xF);
	}
#if !FREE_PHYSICAL_RESOURCES
	for (u32 j=outMap.VirtualCount; j<u32(outMap.VirtualCount+outMap.PhysicalCount); j++) 
	{
		outMap.Chunks[j].DestAddr = outMap.Chunks[j].SrcAddr = (void*) (pm->PageInfo[j] & ~15);
		outMap.Chunks[j].Size = g_rscPhysicalLeafSize << (pm->PageInfo[j]&0xF);
	}
#else	
	outMap.PhysicalCount = 0; 
#endif // !FREE_PHYSICAL_RESOURCES	

	// hann, this will fuck up as soon as we move to the new cookie system.
	outMap.VirtualBase = outMap.Chunks[outMap.RootVirtualChunk].DestAddr;
}


#define ENABLE_MEMORY_PROTECT (!__FINAL && !__PSP2)

#if ENABLE_MEMORY_PROTECT
bool g_DisableReadOnly; 	// So crash report can let us know if it worked

void ProtectRange(void *ptr,size_t size,bool readOnly) {
	TrapNZ((size_t)ptr & 4095);
#if __PS3
	while (size >= 4096) {
		int err = sys_dbg_mat_set_condition((sys_addr_t)ptr, readOnly? SYS_DBG_MAT_WRITE : SYS_DBG_MAT_TRANSPARENT);
		if (err != CELL_OK) {
			// Errorf("sys_dbg_mat_set_condition failed, code %08x, addr %p - make sure *both* 'Enable lv2 exception handler' and 'Enable MAT' are set in ps3tm?",err,ptr);
			g_DisableReadOnly = true;
			return;
		}
		ptr = (void*)((char*)ptr + 4096);
		size -= 4096;
	}
#elif __XENON
	XPhysicalProtect(ptr,size,readOnly?PAGE_READONLY:PAGE_READWRITE);
#elif __WIN32
	DWORD oldProtect;
	VirtualProtect(ptr,size,readOnly?PAGE_READONLY:PAGE_READWRITE,&oldProtect);
#endif
}


void pgBase::MakeReadOnly(bool flag) const {
	if (!m_FirstNode || IsKnownReference(m_FirstNode) || g_DisableReadOnly)
		return;

#if __XENON
	if (!XGetSmallPagesOverride()) {
		Errorf("pgBase::MakeReadOnly -- need to enable XEnableSmallPagesOverride call in main.h");
		g_DisableReadOnly = true;
		return;
	}
#endif
	datResourceMap map;
	RegenerateMap(map);
	u32 count = map.VirtualCount;
#if !__PS3 && !FREE_PHYSICAL_RESOURCES		
		count+= map.PhysicalCount;
#endif // !__PS3 && !FREE_PHYSICAL_RESOURCES

	for (u32 i=0; i<count; i++)
		ProtectRange(map.Chunks[i].DestAddr,map.Chunks[i].Size,flag);
}
#endif


int pgBase::MapContainsPointer(void *ptr) const
{
	datResourceMap temp;
	RegenerateMap(temp);

	for (int i=0; i<temp.VirtualCount
#if !FREE_PHYSICAL_RESOURCES		
		+ temp.PhysicalCount
#endif // !FREE_PHYSICAL_RESOURCES		
		; i++)
	{
		char *start = (char*)temp.Chunks[i].DestAddr;
		u32 size = (u32) temp.Chunks[i].Size;
		if (ptr >= start && ptr < start+size)
			return i;
	}
	return -1;
}

#if __RESOURCECOMPILER
bool g_ReadiedBuild;
#endif

#if __DECLARESTRUCT
void pgBase::DeclareStruct(class datTypeStruct &s) {
#if __RESOURCECOMPILER
	AssertMsg(g_ReadiedBuild,"You forgot to call pgRscBuilder::ReadyBuild before DeclareStruct.");
#endif
	// We need to swap any pagemap or attached data.
	datTypeStruct t;
	if (IsPageMap(m_FirstNode))
		IsPageMap(m_FirstNode)->DeclareStruct(t);
	else if (IsMetaData(m_FirstNode))
		IsMetaData(m_FirstNode)->DeclareStruct(t);
	else
		AssertMsg(!m_FirstNode,"Invalid m_FirstNode in DeclareStruct");
	STRUCT_BEGIN(pgBase);
	STRUCT_FIELD_VP(m_FirstNode);
	STRUCT_END();
}

void pgBaseRefCounted::DeclareStruct(class datTypeStruct &s) {
	pgBase::DeclareStruct(s);
	STRUCT_BEGIN(pgBaseRefCounted);
	STRUCT_FIELD(m_RefCount);
	STRUCT_END();
}

void pgBasePageMap::DeclareStruct(class datTypeStruct &s) {
	// Make sure the metadata chain is declared as well
	if (Next) {
		pgBaseMetaData *md = IsMetaData(Next);
		Assert(md);
		datTypeStruct t;
		pgBaseMetaDataType::Lookup(md->Type)->DeclareStruct(md,t);
	}
	STRUCT_BEGIN(pgBasePageMap);
	STRUCT_FIELD_VP(Next);
	STRUCT_FIELD(VirtualCount);
	STRUCT_FIELD(PhysicalCount);
	STRUCT_FIELD(RootVirtualChunk);
	STRUCT_FIELD(Subtype);
	STRUCT_IGNORE(Pad);
	STRUCT_END();
}

void pgBaseMetaData::DeclareStruct(class datTypeStruct &s) {
	// Declare our successor, if any
	if (Next) {
		pgBaseMetaData *md = IsMetaData(Next);
		Assert(md);
		datTypeStruct t;
		pgBaseMetaDataType::Lookup(md->Type)->DeclareStruct(md,t);
	}

	STRUCT_BEGIN(pgBaseMetaData);
	STRUCT_FIELD_VP(Next);
	STRUCT_FIELD(Type);
	STRUCT_PADDING64(Pad);
	STRUCT_END();
}
#endif

#if TRACK_REFERENCE_BACKTRACE && ENABLE_KNOWN_REFERENCES
static void rbt_display(size_t OUTPUT_ONLY(addr),const char *OUTPUT_ONLY(sym),size_t OUTPUT_ONLY(offset)) {
	diagLoggedPrintf(TCyan "  %8" SIZETFMT "x - %s+%" SIZETFMT "x" TCyan "\n",addr,sym,offset);
}
#define PRINT_STACK_TRACE(bt)	sysStack::PrintRegisteredBacktrace(bt,rbt_display)
#else
#define PRINT_STACK_TRACE(bt)
#endif

#if ENABLE_KNOWN_REFERENCES

#if USE_SPARSE_MEMORY

class pgMemoryIndexToRefIndex
{
public:
	void Init()
	{
		memset(m_Arrays,0,sizeof(m_Arrays));
	}

	void Shutdown()
	{
		for (size_t i=0; i<sysMemSparseAllocator::MaxPages; i++)
			if (m_Arrays[i])
			{
				sysMemVirtualFree(m_Arrays[i]);
				m_Arrays[i] = NULL;
			}
	}

	static char *GetNext(size_t &iterator)
	{
		CompileTimeAssert(sysMemSparseAllocator::PagesPerBlock < 65536);


		// Are we at the end of a previous subheap?  If so, jump to next
		if ((iterator & 65535) == (sysMemSparseAllocator::GetSubHeapSize() >> TrackShift))
			iterator = (iterator & ~65535) + 65536;
		else
			++iterator;

		// Are we at the end of all heaps?
		if ((iterator >> 16) == s_TrackedHeap->GetSubHeapCount())
			return NULL;

		return s_TrackedHeap->GetSubHeapBase(iterator >> 16) + ((iterator & 65535) << TrackShift);
	}

	size_t GetHeapIndex(const void *self)
	{
		size_t c = s_TrackedHeap->GetSubHeapCount();
		for (size_t i=0; i<c; i++)
			if ((char*)self >= s_TrackedHeap->GetSubHeapBase(i) && (char*)self < s_TrackedHeap->GetSubHeapBase(i) + sysMemSparseAllocator::GetSubHeapSize())
			{
				if (!m_Arrays[i])
				{
					m_Arrays[i] = (sysBuddyNodeIdx*) sysMemVirtualAllocate(sysMemSparseAllocator::PagesPerBlock * sizeof(sysBuddyNodeIdx));
					memset(m_Arrays[i],0xFF,sysMemSparseAllocator::PagesPerBlock * sizeof(sysBuddyNodeIdx));
				}
				return i;
			}

		return sysMemSparseAllocator::MaxPages;
	}

	size_t GetSubMemoryIndex(const void *self,size_t heapIndex)
	{
		size_t result = ((char*)self - s_TrackedHeap->GetSubHeapBase(heapIndex)) >> TrackShift;
		TrapGE(result,sysMemSparseAllocator::PagesPerBlock);
		return result;
	}

	sysBuddyNodeIdx GetHeadLink(const void *self)
	{
		size_t hi = GetHeapIndex(self);
		if (hi == sysMemSparseAllocator::MaxPages)
			return sysBuddyHeap::c_NONE;
		else
			return m_Arrays[hi][GetSubMemoryIndex(self,hi)];
	}

	bool IsValidPointer(const void *self)
	{
		return s_TrackedHeap->IsValidPointer(self);
	}

	sysBuddyNodeIdx MoveHeadLink(const void *from,const void *to)
	{
		// Relocate the linked list from source address to destination address
		size_t fromHeapIndex = GetHeapIndex(from);
		size_t toHeapIndex = GetHeapIndex(to);
		TrapGE(fromHeapIndex,sysMemSparseAllocator::MaxPages);
		TrapGE(toHeapIndex,sysMemSparseAllocator::MaxPages);
		size_t fromIndex = GetSubMemoryIndex(from,fromHeapIndex);
		size_t toIndex = GetSubMemoryIndex(to,toHeapIndex);
		sysBuddyNodeIdx r = m_Arrays[fromHeapIndex][fromIndex];
		Assert(m_Arrays[toHeapIndex][toIndex] == sysBuddyHeap::c_NONE);
		m_Arrays[toHeapIndex][toIndex] = r;
		m_Arrays[fromHeapIndex][fromIndex] = sysBuddyHeap::c_NONE;
		return r;
	}

	void AddSelfLink(const void *self,pgBaseKnownReference_Link *linkPool,ptrdiff_t r)
	{
		size_t headHeapIndex = GetHeapIndex(self);
		if (headHeapIndex != sysMemSparseAllocator::MaxPages)
		{
			size_t headIndex = GetSubMemoryIndex(self,headHeapIndex);
			Assert(linkPool[r] >= sysBuddyHeap::c_INVALID);
			linkPool[r] = m_Arrays[headHeapIndex][headIndex];
			Assign(m_Arrays[headHeapIndex][headIndex],(sysBuddyNodeIdx)r);
		}
		else	// Not really tracked
			linkPool[r] = sysBuddyHeap::c_INVALID;
	}

	void RemoveSelfLink(const void *self,pgBaseKnownReference_Link *linkPool,ptrdiff_t r)
	{
		if (linkPool[r] != sysBuddyHeap::c_INVALID)
		{
			size_t heapIndex = GetHeapIndex(self);
			sysBuddyNodeIdx *i = &m_Arrays[heapIndex][GetSubMemoryIndex(self,heapIndex)];
			while (*i != (sysBuddyNodeIdx)r)
			{
#if !__FINAL
				if (*i == sysBuddyHeap::c_NONE)
				{
					PRINT_STACK_TRACE(s_KnownReferencePool.GetPool_Backtraces()[r]);
					Quitf("Pointer %p not on list we expected, changed by higher-level code?",self);
				}
#endif
				i = &linkPool[*i];
			}

			*i = linkPool[r];
			linkPool[r] = sysBuddyHeap::c_NONE;		// mark node free, only for sanity checking
		}
	}
private:
	sysBuddyNodeIdx *m_Arrays[sysMemSparseAllocator::MaxPages];
};
#else

#if !__RESOURCECOMPILER
CompileTimeAssert(g_rscVirtualLeafSize == TrackStep);
#endif

class pgMemoryIndexToRefIndex
{
public:
	void Init() 
	{ 
		m_Array = rage_new sysBuddyNodeIdx[s_MemoryReferenceCount]; 
		memset(m_Array, 0xFF, s_MemoryReferenceCount * sizeof(sysBuddyNodeIdx));
	}

	void Shutdown() 
	{
		delete[] m_Array; 
		m_Array = NULL;
	}

	static char *GetNext(size_t &iterator)
	{
		if (iterator == s_MemoryReferenceCount)
			return NULL;
		else
			return s_TrackStart + (iterator++ << TrackShift);
	}

	size_t GetMemoryIndex(const void *self)
	{
		return ((char*)self - s_TrackStart) >> TrackShift;
	}

	sysBuddyNodeIdx GetHeadLink(const void *self)
	{
		size_t i = GetMemoryIndex(self);
		return i < s_MemoryReferenceCount? m_Array[i] : sysBuddyHeap::c_NONE;
	}

	bool IsValidPointer(const void *self)
	{
		return GetMemoryIndex(self) < s_MemoryReferenceCount;
	}

	sysBuddyNodeIdx MoveHeadLink(const void *from,const void *to)
	{
		// Relocate the linked list from source address to destination address
		size_t fromIndex = GetMemoryIndex(from);
		size_t toIndex = GetMemoryIndex(to);
		TrapGE(fromIndex,s_MemoryReferenceCount);
		TrapGE(toIndex,s_MemoryReferenceCount);
		sysBuddyNodeIdx r = m_Array[fromIndex];
#if !__FINAL
		if (!Verifyf(m_Array[toIndex] == sysBuddyHeap::c_NONE,"Previous head link[%" SIZETFMT "u] is %x, should be sysBuddyHeap::c_NONE",toIndex,m_Array[toIndex]))
		{
			Errorf("=== BEGIN REALLY IMPORTANT INFO ===");
			Errorf("Previous head link at %p info (this might tell you what was there before)",to);
			sysMemAllocator::DebugPrintAllocInfo(to);

			Errorf("Current head link at %p info (this might tell you what was being moved)",from);
			sysMemAllocator::DebugPrintAllocInfo(from);
			Errorf("=== EDN REALLY IMPORTANT INFO ===");

			Quitf("Please paste at least text between == REALLY IMPORTANT INFO == into the bug.");
		}
#endif
		m_Array[toIndex] = r;
		m_Array[fromIndex] = sysBuddyHeap::c_NONE;
		return r;
	}

	void AddSelfLink(const void *self,pgBaseKnownReference_Link *linkPool,ptrdiff_t r)
	{
		size_t headIndex = GetMemoryIndex(self);
		Assert(linkPool[r] >= sysBuddyHeap::c_INVALID);
		if (headIndex < s_MemoryReferenceCount)
		{
			linkPool[r] = m_Array[headIndex];
			Assign(m_Array[headIndex],(sysBuddyNodeIdx)r);
		}
		else // Not really tracked
		{
			linkPool[r] = sysBuddyHeap::c_INVALID;
		}
	}

	void RemoveSelfLink(const void *self,pgBaseKnownReference_Link *linkPool,ptrdiff_t r)
	{
		if (linkPool[r] != sysBuddyHeap::c_INVALID)
		{
			sysBuddyNodeIdx *i = &m_Array[GetMemoryIndex(self)];
			while (*i != (sysBuddyNodeIdx)r)
			{
#if !__FINAL
				if (*i == sysBuddyHeap::c_NONE)
				{
					PRINT_STACK_TRACE(s_KnownReferencePool.GetPool_Backtraces()[r]);
					Quitf("Pointer %p not on list we expected, changed by higher-level code?",self);
				}
#endif
				i = &linkPool[*i];
			}

			*i = linkPool[r];
			linkPool[r] = sysBuddyHeap::c_NONE;		// mark node free, only for sanity checking
		}
	}

private:
	sysBuddyNodeIdx *m_Array;
};

#endif

pgMemoryIndexToRefIndex s_KnownRefHead_ByAddress, s_KnownRefHead_ByContents;
pgMemoryIndexToRefIndex s_pgHandleLinkHeads;
pgMemoryIndexToRefIndex s_pgHandleLinkHeads2;
u32 g_pgHandleFirstFree = 1, g_pgHandlesUsed, g_pgPeakHandlesUsed;
u32 g_pgHandleFirstFree2 = 1, g_pgHandlesUsed2, g_pgPeakHandlesUsed2;
pgBaseKnownReference_Link s_pgHandleLinkArray[pgMaxHandles];
pgBaseKnownReference_Link s_pgHandleLinkArray2[pgMaxHandles];
sysCriticalSectionToken pgHandleToken;

#if __ASSERT


bool pgBase::ValidateAllReferences()
{
	SYS_CS_SYNC(s_KnownRefToken);

	bool result = true;

	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
	pgBaseKnownReference_Link *linkPool_byAddress = s_KnownReferencePool.GetPool_Links_ByAddress();
	pgBaseKnownReference_Link *linkPool_byContents = s_KnownReferencePool.GetPool_Links_ByContents();
#if TRACK_REFERENCE_BACKTRACE
	pgBaseKnownReference_Backtrace *tracePool = s_KnownReferencePool.GetPool_Backtraces();
#endif
	int totalVisited = 0, totalExpected = 0;
	u32 knownReferencePoolSize = s_KnownReferencePool.GetSize();
	for (u32 i=0; i<knownReferencePoolSize; i++) 
	{
		if (refPool[i])
			++totalExpected;
	}
	// Make sure the number of entries being used actually matches what we expect
	Assert(totalExpected == s_KnownReferencePool.GetUsed());
	if (totalExpected != s_KnownReferencePool.GetUsed())
	{
		Errorf("Expected %u nonzero pointers but pool claims there are %u",totalExpected,s_KnownReferencePool.GetUsed());
		result = false;
	}

	size_t iterator=0;
	for (char *src = pgMemoryIndexToRefIndex::GetNext(iterator); src; src = pgMemoryIndexToRefIndex::GetNext(iterator))
	{
		sysBuddyNodeIdx r = s_KnownRefHead_ByAddress.GetHeadLink(src);
		while (r != sysBuddyHeap::c_NONE)
		{
			if ((char*)refPool[r] < src || (char*)refPool[r] >= src + TrackStep)
			{
				PRINT_STACK_TRACE(tracePool[r]);
				Errorf("Tracked pointer by address %p is on wrong list (should be on one for %p)",refPool[r],src);
				result = false;
			}
			// Count the visit now only if we know we're not on the contents list
			if (linkPool_byContents[r] == sysBuddyHeap::c_INVALID)
				++totalVisited;
			r = linkPool_byAddress[r];
		}

		r = s_KnownRefHead_ByContents.GetHeadLink(src);
		while (r != sysBuddyHeap::c_NONE)
		{
			if (!refPool[r] || (char*)*refPool[r] < src || (char*)*refPool[r] >= src + TrackStep)
			{
				PRINT_STACK_TRACE(tracePool[r]);
				Errorf("Tracked pointer by contents @%p->%p is on wrong list (should be on one for %p)",refPool[r],*refPool[r],src);
				result = false;
			}
			++totalVisited;
			r = linkPool_byContents[r];
		}
	}
	if (totalVisited != totalExpected)
	{
		Errorf("Expected %u pointers but only saw %u on linked lists?",totalExpected,totalVisited);
		result = false;
	}
	return result;
}

#endif	// __ASSERT

#if !__FINAL

bool pgBase::IsMemoryTracked(void *dest,size_t size)
{
	SYS_CS_SYNC(s_KnownRefToken);
	char *start = (char*)((size_t)dest & ~(TrackStep-1));
	char *stop = (char*)((size_t)(start + size - 1) & ~(TrackStep-1));
	bool result = false;
	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
	pgBaseKnownReference_Link *linkPool_byAddress = s_KnownReferencePool.GetPool_Links_ByAddress();
	pgBaseKnownReference_Link *linkPool_byContents = s_KnownReferencePool.GetPool_Links_ByContents();
#if TRACK_REFERENCE_BACKTRACE
	pgBaseKnownReference_Backtrace *tracePool = s_KnownReferencePool.GetPool_Backtraces();
#endif
	// Untracked memory ranges are never considered tracked
	if (s_KnownRefHead_ByAddress.IsValidPointer(start))
	{
		while (start <= stop)
		{
			// If any linked list head is nonempty, there's memory being tracked.
			if (s_KnownRefHead_ByAddress.GetHeadLink(start) != sysBuddyHeap::c_NONE)
			{
				Errorf("One or more tracked pointers by address in memory range %p,%p",start,stop);
				sysBuddyNodeIdx i = s_KnownRefHead_ByAddress.GetHeadLink(start);
				while (i != sysBuddyHeap::c_NONE)
				{
					PRINT_STACK_TRACE(tracePool[i]);
					Errorf("Pointer at %p, pointing to %p",refPool[i],*refPool[i]);
					i = linkPool_byAddress[i];
				}
				result = true;
			}
			if (s_KnownRefHead_ByContents.GetHeadLink(start) != sysBuddyHeap::c_NONE)
			{
				Errorf("One or more tracked pointers by contents in memory range %p,%p",start,stop);
				sysBuddyNodeIdx i = s_KnownRefHead_ByContents.GetHeadLink(start);
				while (i != sysBuddyHeap::c_NONE)
				{
					PRINT_STACK_TRACE(tracePool[i]);
					Errorf("Pointer at %p, pointing to %p",refPool[i],*refPool[i]);
					i = linkPool_byContents[i];
				}
				result = true;
			}
			if (s_pgHandleLinkHeads.GetHeadLink(start) != sysBuddyHeap::c_NONE)
			{
				Errorf("One or more unfreed texture pgHandles in memory range %p,%p (missing call to pgHandleBase::Unregister)",start,stop);
				for (sysBuddyNodeIdx i = s_pgHandleLinkHeads.GetHeadLink(start); i != sysBuddyHeap::c_NONE; i = s_pgHandleLinkArray[i]) {
					Errorf("Handle slot %d @%p, pointing at %p",i,&g_pgHandleArray[i],g_pgHandleArray[i]);
				}
				AssertMsg(false,"Caught a missing pgHandle, but I'll be nice and not crash the game.");
				// result = true;
			}
			if (s_pgHandleLinkHeads2.GetHeadLink(start) != sysBuddyHeap::c_NONE)
			{
				Errorf("One or more unfreed non-texture pgHandle2s in memory range %p,%p (missing call to pgHandle2Base::Unregister)",start,stop);
				for (sysBuddyNodeIdx i = s_pgHandleLinkHeads2.GetHeadLink(start); i != sysBuddyHeap::c_NONE; i = s_pgHandleLinkArray2[i]) {
					Errorf("Handle slot %d @%p, pointing at %p",i,&g_pgHandleArray2[i],g_pgHandleArray2[i]);
				}
				AssertMsg(false,"Caught a missing pgHandle2, but I'll be nice and not crash the game.");
				// result = true;
			}
			start += TrackStep;
		}
	}
	return result;
}

#endif	// !__FINAL

struct datResourcePatch {
	datResourcePatch(const datResourceMap &map);

	template <class _Ptr> void PointerFixup(_Ptr& ptr) const
	{
		for (int i=0; i<m_Count; i++)
			if (ptr >= m_SrcStart[i] && ptr < m_SrcEnd[i]) {
				ptr = (_Ptr)((char*)ptr + m_Fixup[i]);
				return;
			}
	}

private:
	void* m_SrcStart[datResourceChunk::MAX_CHUNKS];
	void* m_SrcEnd[datResourceChunk::MAX_CHUNKS];
	ptrdiff_t m_Fixup[datResourceChunk::MAX_CHUNKS];
	int	m_Count;
};


datResourcePatch::datResourcePatch(const datResourceMap &map) : m_Count(0) 
{
	for (int i = 0; i != map.VirtualCount + map.PhysicalCount; ++i) {
		if (map.Chunks[i].SrcAddr != map.Chunks[i].DestAddr) {
			m_SrcStart[m_Count] = map.Chunks[i].SrcAddr;
			m_SrcEnd[m_Count] = (char*)map.Chunks[i].SrcAddr + map.Chunks[i].Size;
			m_Fixup[m_Count] = (char*)map.Chunks[i].DestAddr - (char*)map.Chunks[i].SrcAddr;
			m_Count++;
		}
	}
}

#if BASE_DEBUG_NAME
void DumpPgHandleArray(bool verbose)
{
	SYS_CS_SYNC(s_KnownRefToken);		// take it again explicitly in case we are called externally
	char buffer[256];
	atMap<const char*,u32> counts;
	for (int i=1; i<pgMaxHandles; i++) {
		pgBase *b = g_pgHandleArray[i];
		if ((size_t)b > pgMaxHandles) {
			if (verbose) Displayf("%4d. %s %s",i,GetRttiName(b),b->GetDebugName(buffer,sizeof(buffer)));
			counts[GetRttiName(b)]++;
		}
		else if (verbose)
			Displayf("%4d. EMPTY SLOT",i);
	}
	for (int i=1; i<pgMaxHandles2; i++) {
		pgBase *b = g_pgHandleArray2[i];
		if ((size_t)b > pgMaxHandles2) {
			if (verbose) Displayf("%4d. %s %s",i,GetRttiName(b),b->GetDebugName(buffer,sizeof(buffer)));
			counts[GetRttiName(b)]++;
		}
		else if (verbose)
			Displayf("%4d. EMPTY SLOT",i);
	}
	for (atMap<const char*,u32>::Iterator i=counts.CreateIterator(); !i.AtEnd(); i.Next()) {
		Displayf("DumpPgHandleArray - Type %s - %d instances",i.GetKey(),i.GetData());
	}
	Displayf("DumpPgHandleArray - %d/%d/%d textures, %d/%d/%d drawables (current/peak/max)",
		g_pgHandlesUsed,g_pgPeakHandlesUsed,pgMaxHandles,
		g_pgHandlesUsed2,g_pgPeakHandlesUsed2,pgMaxHandles2);
}
#endif

#define WARN_DUPLICATE_TEXTURES	(__ASSERT && !__RESOURCECOMPILER && 0)

#if WARN_DUPLICATE_TEXTURES
atMap<u32,u32> s_DuplicateMap;
static inline bool IgnoredTextureHash(u32 h) {
	return h == ATSTRINGHASH("image",0xd58c8637);
}
#endif

u32 pgHandleBase::RegisterIndexInternal(pgBase *obj)
{
	SYS_CS_SYNC(s_KnownRefToken);
	u32 h = obj->GetHandleIndex();
	if (h)
		return h;

	if (g_pgHandleFirstFree == pgMaxHandles) {
#if BASE_DEBUG_NAME
		DumpPgHandleArray(true);
#endif
		Quitf("pgHandle array is full, raise pgMaxHandles in base.cpp (see TTY for object vptrs and names)");
	}

#if WARN_DUPLICATE_TEXTURES
	char buffer[256];
	const char *bp = obj->GetDebugName(buffer,sizeof(buffer));
	if (bp) {
		u32 hash = atStringHash(bp);
		if (!IgnoredTextureHash(hash)) {
			u32 *slot = s_DuplicateMap.Access(hash);
			if (slot) {
				// Render targets tend to double-register for some reason.  So let the first duplicate go by.
				// If we want to eliminate render targets, we could call GetRttiName but that may have a
				// more significant performance impact
				Assertf(*slot == 0 || obj->ExpectDuplicatesByName(),"pgHandleBase::RegisterIndexInternal(%s) already here %d times",bp,*slot);
				++(*slot);
			}
			else
				s_DuplicateMap.Insert(hash,1);
		}
	}
#endif

	h = g_pgHandleFirstFree;
	Assertf(h < pgMaxHandles,"Invalid pgHandleBase on free list, %x",h);
	g_pgHandleFirstFree = (u32)(size_t)g_pgHandleArray[h];
	g_pgHandleArray[h] = obj;
	obj->SetHandleIndex(h);	  
	Assertf(obj->GetHandleIndex() == h,"obj->SetHandleIndex truncated input?");
	if (++g_pgHandlesUsed > g_pgPeakHandlesUsed) {
		g_pgPeakHandlesUsed = g_pgHandlesUsed;
		if (!(g_pgPeakHandlesUsed & 255))
			Displayf("pgHandleBase::RegisterIndex - new peak %u",g_pgPeakHandlesUsed);
	}

#	if PGHANDLE_REF_COUNT
		// Should already be zero, but force to prevent future false positive errors
		Assert(!g_pgHandleRefCountArray[h]);
		g_pgHandleRefCountArray[h] = 0;
#	endif

	s_pgHandleLinkHeads.AddSelfLink(obj,s_pgHandleLinkArray,h);
	return h;
}

u32 pgHandleBase2::RegisterIndexInternal(pgBase *obj)
{
	SYS_CS_SYNC(s_KnownRefToken);
	u32 h = obj->GetHandleIndex();
	if (h)
		return h;

	if (g_pgHandleFirstFree2 == pgMaxHandles2) {
#if BASE_DEBUG_NAME
		DumpPgHandleArray(true);
#endif
		Quitf("pgHandle2 array is full, raise pgMaxHandles2 in base.cpp (see TTY for object vptrs and names)");
	}

	h = g_pgHandleFirstFree2;
	Assertf(h < pgMaxHandles2,"Invalid pgHandleBase on free list, %x",h);
	g_pgHandleFirstFree2 = (u32)(size_t)g_pgHandleArray2[h];
	g_pgHandleArray2[h] = obj;
	obj->SetHandleIndex(h);	  
	Assertf(obj->GetHandleIndex() == h,"obj->SetHandleIndex truncated input?");
	if (++g_pgHandlesUsed2 > g_pgPeakHandlesUsed2) {
		g_pgPeakHandlesUsed2 = g_pgHandlesUsed2;
		if (!(g_pgPeakHandlesUsed2 & 255))
			Displayf("pgHandleBase2::RegisterIndex - new peak %u",g_pgPeakHandlesUsed2);
	}

	s_pgHandleLinkHeads2.AddSelfLink(obj,s_pgHandleLinkArray2,h);
	return h;
}

static void CheckForDanglingPgHandles(pgBase* PGHANDLE_REF_COUNT_ONLY(owner), u32 PGHANDLE_REF_COUNT_ONLY(h))
{
#	if PGHANDLE_REF_COUNT
	 	if (Unlikely(h && g_pgHandleRefCountArray[h])) {
			char buffer[256];
	 		Errorf("pgBase \"%s\" (%p) being unregisterred, while still referenced by %u pgHandles.  Handle index 0x%04x.",
				owner->GetDebugName(buffer,sizeof(buffer)), owner, g_pgHandleRefCountArray[h], h);
			g_pgHandleRefCountArray[h] = 0;
			sysMemAllocator::DebugPrintAllocInfo(owner);

			Displayf("");
			Displayf("Scanning memory for dangling pgHandles...");
	 		Assert(dynamic_cast<sysMemMultiAllocator*>(&sysMemAllocator::GetMaster()));
	 		sysMemMultiAllocator *const master = static_cast<sysMemMultiAllocator*>(&sysMemAllocator::GetMaster());
	 		const unsigned numAllocators = master->GetNumAllocators();

			// The same allocator can be registerred more that once with the
			// master multi allocator.  So we do some basic testing to avoid
			// duplicates.  If we fill this array, then we may process
			// duplicates, but that isn't the end of the world.
			sysMemAllocator *alreadyChecked[8] = {NULL};
			unsigned alreadyCheckedOldest = 0;

	 		for (unsigned allocIdx=0; allocIdx<numAllocators; ++allocIdx) {
	 			sysMemAllocator *const alloc = master->GetAllocator(allocIdx);
	 			if (alloc) {

					// Bail out of duplicate allocators
					for (unsigned i=0; i<NELEM(alreadyChecked); ++i) {
						if (alreadyChecked[i] == alloc) {
							goto nextAllocator;
						}
					}
					alreadyChecked[alreadyCheckedOldest] = alloc;
					++alreadyCheckedOldest;
					alreadyCheckedOldest = (alreadyCheckedOldest<NELEM(alreadyChecked)) ? alreadyCheckedOldest : 0;

					// Scan the memory controlled by the allocator
					const uptr heapBase = (uptr)alloc->GetHeapBase();
#					if __PS3
						if (heapBase - RSX_FB_BASE_ADDR < 0x10000000)
							continue;
#					endif
	 				pgBase ***ppp = (pgBase***)((heapBase+(sizeof(void*)-1))&~(sizeof(void*)-1));
					const size_t heapSize = alloc->GetHeapSize();
	 				pgBase ***const end = (pgBase***)((heapBase+heapSize)&~(sizeof(void*)-1));
	 				while (ppp < end) {
	 					if (Unlikely(*ppp - g_pgHandleArray == (ptrdiff_t)h)) {
							Displayf("");
	 						Displayf("Found possible dangling pgHandle at %p", ppp);
							sysMemAllocator::DebugPrintAllocInfo(ppp);
	 					}
	 					++ppp;
	 				}
	 			}

				nextAllocator:;
	 		}

			Displayf("");
	 		__debugbreak();
	 	}
#	endif
}

void pgHandleBase::Unregister(pgBase *owner)
{
	if (owner && owner->GetHandleIndex()) {
		SYS_CS_SYNC(s_KnownRefToken);
		u32 h = owner->GetHandleIndex();
		Assertf(h < pgMaxHandles,"Invalid pgHandleBase in Unregister, %x",h);
		Assert(g_pgHandleArray[h] == (void*)owner);

#if WARN_DUPLICATE_TEXTURES
		char buffer[256];
		const char *bp = owner->GetDebugName(buffer,sizeof(buffer));
		if (bp) {
			u32 hash = atStringHash(bp);
			if (!IgnoredTextureHash(hash)) {
				u32 *slot = s_DuplicateMap.Access(hash);
				if (Verifyf(slot && *slot,"%s %s - %p is null or points at a zero",GetRttiName(owner),bp,slot))
					(*slot)--;
			}
		}
#endif

		CheckForDanglingPgHandles(owner,h);
		g_pgHandleArray[h] = (pgBase*)(size_t)g_pgHandleFirstFree;
		s_pgHandleLinkHeads.RemoveSelfLink(owner,s_pgHandleLinkArray,h);
		g_pgHandleFirstFree = h;
		owner->SetHandleIndex(0);
		--g_pgHandlesUsed;
	}
}

void pgHandleBase2::Unregister(pgBase *owner)
{
	if (owner && owner->GetHandleIndex()) {
		SYS_CS_SYNC(s_KnownRefToken);
		u32 h = owner->GetHandleIndex();
		Assertf(h < pgMaxHandles2,"Invalid pgHandleBase in Unregister, %x",h);
		Assert(g_pgHandleArray2[h] == (void*)owner);
		// CheckForDanglingPgHandles(owner,h);
		g_pgHandleArray2[h] = (pgBase*)(size_t)g_pgHandleFirstFree2;
		s_pgHandleLinkHeads2.RemoveSelfLink(owner,s_pgHandleLinkArray2,h);
		g_pgHandleFirstFree2 = h;
		owner->SetHandleIndex(0);
		--g_pgHandlesUsed2;
	}
}


void pgBase::PatchAllReferences(const datResourceMap &map)
{
	SYS_CS_SYNC(s_KnownRefToken);

	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
	pgBaseKnownReference_Link *linkPool_byAddress = s_KnownReferencePool.GetPool_Links_ByAddress();
	pgBaseKnownReference_Link *linkPool_byContents = s_KnownReferencePool.GetPool_Links_ByContents();
#if TRACK_REFERENCE_BACKTRACE
	pgBaseKnownReference_Backtrace *tracePool = s_KnownReferencePool.GetPool_Backtraces();
#endif
#if __ASSERT
	bool failed = false;
#endif

	// Fix up the locations of the pointers first, so that when we go to fix up the contents
	// later on we're always working on the final copy.  Needs to be in two passes because
	// otherwise we might see stale contents in the case where there is more than one block.
	for (int i=0, count=map.VirtualCount; i<count; i++)
	{
		// Skip blocks that didn't move.
		ptrdiff_t fixup = (char*)map.Chunks[i].DestAddr - (char*)map.Chunks[i].SrcAddr;
		if (!fixup)
			continue;

		char *srcPtr = (char*)map.Chunks[i].SrcAddr;
		char *destPtr = (char*)map.Chunks[i].DestAddr;
		for (size_t blockCount = map.Chunks[i].Size >> TrackShift; blockCount; blockCount--,srcPtr += TrackStep,destPtr += TrackStep)
		{
			sysBuddyNodeIdx r = s_KnownRefHead_ByAddress.MoveHeadLink(srcPtr,destPtr);
			// Walk the list of tracked pointers that live on this page of memory
			while (r != sysBuddyHeap::c_NONE)
			{
#if __ASSERT
				if ((char*)refPool[r] < srcPtr || (char*)refPool[r] >= srcPtr + TrackStep)
				{
					failed = true;
					PRINT_STACK_TRACE(tracePool[r]);
					Errorf("Tracked pointer by address %p not in expected range %p,%p",refPool[r],srcPtr,srcPtr + TrackStep-1);
				}
				else
#endif
				{
					// Displayf("Patching tracked pointer by address from %p to %p",refPool[r],(char*)refPool[r] + fixup);
					refPool[r] = (void**)((char*)refPool[r] + fixup);
				}
				r = linkPool_byAddress[r];
			}
		}
	}

	// Now do all the contents of pointers (including the double indirect table)
	for (int i=0, count=map.VirtualCount; i<count; i++)
	{
		// Skip blocks that didn't move.
		ptrdiff_t fixup = (char*)map.Chunks[i].DestAddr - (char*)map.Chunks[i].SrcAddr;
		if (!fixup)
			continue;

		char *srcPtr = (char*)map.Chunks[i].SrcAddr;
		char *destPtr = (char*)map.Chunks[i].DestAddr;
		for (size_t blockCount = map.Chunks[i].Size >> TrackShift; blockCount; blockCount--,srcPtr += TrackStep,destPtr += TrackStep)
		{
			// Relocate the linked list from source address to destination address
			sysBuddyNodeIdx r = s_KnownRefHead_ByContents.MoveHeadLink(srcPtr,destPtr);
			// Walk the list of tracked pointers that point to this page of memory
			while (r != sysBuddyHeap::c_NONE)
			{
#if __ASSERT
				if ((char*)*refPool[r] < srcPtr || (char*)*refPool[r] >= srcPtr + TrackStep)
				{
					failed = true;
					PRINT_STACK_TRACE(tracePool[r]);
					Errorf("Tracked pointer by contents %p not in expected range %p,%p",*refPool[r],srcPtr,srcPtr + TrackStep-1);
				}
				else
#endif
				{
					// Displayf("Patching tracked pointer by contents from %p to %p",*refPool[r],(char*)*refPool[r] + fixup);
					*refPool[r] = (void*)((char*)*refPool[r] + fixup);
				}
				r = linkPool_byContents[r];
			}

			// Relocate the linked list from source address to destination address
			r = s_pgHandleLinkHeads.MoveHeadLink(srcPtr,destPtr);
			// Walk the list of double indirect pointers that point to this page of memory
			while (r != sysBuddyHeap::c_NONE)
			{
				Assert((char*)g_pgHandleArray[r] >= srcPtr && (char*)g_pgHandleArray[r] < srcPtr + TrackStep);
				g_pgHandleArray[r] = (pgBase*)((char*)g_pgHandleArray[r] + fixup);
				r = s_pgHandleLinkArray[r];
			}

			// Relocate the second linked list from source address to destination address
			r = s_pgHandleLinkHeads2.MoveHeadLink(srcPtr,destPtr);
			// Walk the list of double indirect pointers that point to this page of memory
			while (r != sysBuddyHeap::c_NONE)
			{
				Assert((char*)g_pgHandleArray2[r] >= srcPtr && (char*)g_pgHandleArray2[r] < srcPtr + TrackStep);
				g_pgHandleArray2[r] = (pgBase*)((char*)g_pgHandleArray2[r] + fixup);
				r = s_pgHandleLinkArray2[r];
			}
		}
	}
	AssertMsg(!failed,"One or more tracking failures, see TTY for details");
}


#endif	// ENABLE_KNOWN_REFERENCES



#if __DEV
void pgBase::DebugDisplayReferencing()
{
	if (g_WatchedBaseObject != this)
	{
		g_WatchedBaseObject = this;
		Displayf("Watching references to %p", this);
	}
}
#endif

u32 pgBase::GetHandleIndex() const
{
	return 0;
}


void pgBase::SetHandleIndex(u32)
{
	Quitf(ERR_GEN_PAGE_1,"SetHandleIndex not implemented for this class");
}


#if NO_REFERENCE_TRACKING && ENABLE_KNOWN_REFERENCES

void pgBase::AddKnownRef(void**) const { }

void pgBase::RemoveKnownRef(void**) const { }

void pgBase::RemoveRefIfKnown(void**) const { }

void pgBase::DumpRefs() { }

void pgBase::DumpUniqueRefs() { }

#elif ENABLE_KNOWN_REFERENCES	// !NO_REFERENCE_TRACKING

#if !__FINAL
void pgBase::DumpRefs() {
	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
#if TRACK_REFERENCE_BACKTRACE
	pgBaseKnownReference_Backtrace *tracePool = s_KnownReferencePool.GetPool_Backtraces();
#endif
	for (u32 i=0; i<s_KnownReferencePool.GetSize(); i++) {
		if (refPool[i]) {
			Displayf("[%5u] Ref %p *Ref %p",
				i,refPool[i],*refPool[i]);
			PRINT_STACK_TRACE(tracePool[i]);
		}
	}
#if __TASKLOG
	s_TaskLog.Dump();
#endif
}

void pgBase::DumpUniqueRefs() {
	typedef atMap<u16, u32> UniqueMap;
	UniqueMap uniques;

#if TRACK_REFERENCE_BACKTRACE
	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
	pgBaseKnownReference_Backtrace *tracePool = s_KnownReferencePool.GetPool_Backtraces();

	for (u32 i=0; i<s_KnownReferencePool.GetSize(); i++) {
		if (refPool[i]) {
			uniques[tracePool[i]]++;
		}
	}

	UniqueMap::Iterator entry = uniques.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		u32 hits = entry.GetData(); /// do something with it
		Displayf(">> Callstack with %d hits << ", hits);
		PRINT_STACK_TRACE(entry.GetKey());
		Displayf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	}
#endif // TRACK_REFERENCE_BACKTRACE

#if __TASKLOG
	s_TaskLog.Dump();
#endif
}
#endif

void pgBase::AddKnownRef(void** ppReference) const
{
	SYS_CS_SYNC(s_KnownRefToken);

	Assertf(!datResource_IsDefragmentation,"Calling AddKnownRef during defragmentation, probably not what you intended.");

	Assertf(IsTrackedAddress(*ppReference), "Adding reference to pointer %p which isn't in virtual resource heap, kinda wasteful.",*ppReference);

	Assertf(!IsRefKnown(ppReference), "pgBase::AddKnownRef: Ref is already known to this object!");

	// Call a (harmless) virtual function to make sure we've actually been constructed by now.
	// Actually, known references CAN get added before the object is constructed during resource fixups.
	// IsSafeToDelete();

#if !__FINAL
	if (s_KnownReferencePool.IsFull())
	{
		ASSERT_ONLY(pgBase::ValidateAllReferences());
		pgBase::DumpUniqueRefs();

		Quitf("KnownReference pool exhausted, raise KnownReferencePoolSize in knownrefpool.h to something larger than %u (or delete your data\\*_cache_?.dat file, but send your TTY log to Klaas first)", s_KnownReferencePool.GetSize());
	}
#endif

	ptrdiff_t newRef = s_KnownReferencePool.New();

	pgBaseNode *nodePool = s_KnownReferencePool.GetPool_Nodes();
	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
#if TRACK_REFERENCE_BACKTRACE
	pgBaseKnownReference_Backtrace *tracePool = s_KnownReferencePool.GetPool_Backtraces();
#endif

	refPool[newRef] = ppReference;

#if TRACK_REFERENCE_BACKTRACE
	tracePool[newRef] = sysStack::RegisterBacktrace();
#endif

	s_KnownRefHead_ByAddress.AddSelfLink(ppReference,s_KnownReferencePool.GetPool_Links_ByAddress(),newRef);
	s_KnownRefHead_ByContents.AddSelfLink(*ppReference,s_KnownReferencePool.GetPool_Links_ByContents(),newRef);

	/// PRINT_STACK_TRACE(tracePool[newRef]);
	/// Displayf("callstack id is %u",tracePool[newRef]);

	BASE_TASKLOG_ONLY(s_TaskLog.Log('PAKR',(u32)this,(u32)ppReference,0));

	// Skip a page map if there is one (note, this may not actually happen in practice since most resources are wrapped by dictionaries)
	pgBaseNode **head = &m_FirstNode;
	while (*head && !IsKnownReference(*head))
		head = &(*head)->Next;

	pgDisplayf("adding new known ref @%x (%p->%p)",newRef,ppReference,*ppReference);
#if __DEV
	if (g_WatchedBaseObject == this)
	{
		PRINT_STACK_TRACE(tracePool[newRef]);
		Displayf("adding new known ref @%" PTRDIFFTFMT "x (%p->%p)",newRef,ppReference,*ppReference);
	}
#endif
	Assertf(*head != nodePool+newRef,"newly allocated ref %" PTRDIFFTFMT "d was already here (%p)?",newRef,m_FirstNode);
	nodePool[newRef].Next = *head;
	*head = nodePool+newRef;

	defragCheckAssertf(IsRefKnown(ppReference), "pgBase::AddKnownRef: Ref didn't get added to this object!");
}


void pgBase::RemoveKnownRef(void** ppReference) const
{
	SYS_CS_SYNC(s_KnownRefToken);

	AssertMsg(!datResource_IsDefragmentation,"Calling RemoveKnownRef during defragmentation, probably not what you intended.");

	BASE_TASKLOG_ONLY(s_TaskLog.Log('PRKR',(u32)this,(u32)ppReference,0));

	pgBaseNode *nodePool = s_KnownReferencePool.GetPool_Nodes();
	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
#if TRACK_REFERENCE_BACKTRACE && __ASSERT
	pgBaseKnownReference_Backtrace *tracePool = s_KnownReferencePool.GetPool_Backtraces();
#endif

	// If the first reference is a page map, skip it.
	pgBaseNode **i = &m_FirstNode;
	while (*i && !IsKnownReference(*i))
		i = &(*i)->Next;

	// Everything else is a known reference.
	while (*i) {
		pgBaseNode *ref = *i;
		ptrdiff_t r = ref - nodePool;
		// BASE_TASKLOG_ONLY(s_TaskLog.Log(' REF',(u32)ref,(u32)ref->Reference,(u32)ref));

		if (refPool[r] == ppReference) {
			// Remove this reference from the chain.
			Assertf(*i != ref->Next,"Next loop in RemoveKnownRef? %p",ref->Next);
			*i = ref->Next;

			// pgBaseNode *n = m_FirstNode; while (n) { Printf("%p->",n); n = n->Next; }
			pgDisplayf("removing known ref %u (%p->%p), next %p",r,refPool[r],*refPool[r],ref->Next);
#if __DEV
			if (g_WatchedBaseObject == this)
			{
				PRINT_STACK_TRACE(tracePool[r]);
				Displayf("removing known ref %" PTRDIFFTFMT "u (%p->%p), next %p",r,refPool[r],*refPool[r],ref->Next);
			}
#endif

			s_KnownRefHead_ByContents.RemoveSelfLink(*ppReference,s_KnownReferencePool.GetPool_Links_ByContents(),r);
			s_KnownRefHead_ByAddress.RemoveSelfLink(ppReference,s_KnownReferencePool.GetPool_Links_ByAddress(),r);

			// Mark the entry free (without affecting the pointer's contents)
			refPool[r] = NULL;

			// Remove the entry from the pool.
			s_KnownReferencePool.Delete(ref);
			return;
		}
		i = &(*i)->Next;
	}

	ASSERT_ONLY(char debugName[128];)
	Assertf(false, "pgBase::RemoveKnownRef - %p->%p Attempted to remove a reference to %p (%s) that wasn't on the list",ppReference,*ppReference, this, GetDebugName(debugName, sizeof(debugName)));
}

// This is a combination of RemoveKnownRef (above) and IsRefKnown.
void pgBase::RemoveRefIfKnown(void** ppReference) const
{
	if (!ppReference)
		return; // nothing to do here

	SYS_CS_SYNC(s_KnownRefToken);

	AssertMsg(!datResource_IsDefragmentation,"Calling RemoveKnownRef during defragmentation, probably not what you intended.");

	BASE_TASKLOG_ONLY(s_TaskLog.Log('PRKR',(u32)this,(u32)ppReference,0));

	pgBaseNode *nodePool = s_KnownReferencePool.GetPool_Nodes();
	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
#if TRACK_REFERENCE_BACKTRACE && __ASSERT
	pgBaseKnownReference_Backtrace *tracePool = s_KnownReferencePool.GetPool_Backtraces();
#endif

	// If the first reference is a page map, skip it.
	pgBaseNode **i = &m_FirstNode;
	while (*i && !IsKnownReference(*i))
		i = &(*i)->Next;

	// Everything else is a known reference.
	while (*i) {
		pgBaseNode *ref = *i;
		ptrdiff_t r = ref - nodePool;
		// BASE_TASKLOG_ONLY(s_TaskLog.Log(' REF',(u32)ref,(u32)ref->Reference,(u32)ref));

		if (refPool[r] == ppReference) {
			// Remove this reference from the chain.
			Assertf(*i != ref->Next,"Next loop in RemoveKnownRef? %p",ref->Next);
			*i = ref->Next;

			// pgBaseNode *n = m_FirstNode; while (n) { Printf("%p->",n); n = n->Next; }
			pgDisplayf("removing known ref %u (%p->%p), next %p",r,refPool[r],*refPool[r],ref->Next);
#if __DEV
			if (g_WatchedBaseObject == this)
			{
				PRINT_STACK_TRACE(tracePool[r]);
				Displayf("removing known ref %" PTRDIFFTFMT "u (%p->%p), next %p",r,refPool[r],*refPool[r],ref->Next);
			}
#endif

			s_KnownRefHead_ByContents.RemoveSelfLink(*ppReference,s_KnownReferencePool.GetPool_Links_ByContents(),r);
			s_KnownRefHead_ByAddress.RemoveSelfLink(ppReference,s_KnownReferencePool.GetPool_Links_ByAddress(),r);

			// Mark the entry free (without affecting the pointer's contents)
			refPool[r] = NULL;

			// Remove the entry from the pool.
			s_KnownReferencePool.Delete(ref);
			return;
		}
		i = &(*i)->Next;
	}
}

#endif		// !NO_REFERENCE_TRACKING


#if ENABLE_KNOWN_REFERENCES

void pgBase::ClearAllKnownRefs(bool assertIfAny) const
{
	SYS_CS_SYNC(s_KnownRefToken);

	if(!s_KnownReferencePool.IsInitialized())
	{
		// If the pool hasn't even been initialized, we can't have known references,
		// so just return instead of crashing. This can happen when shutting down static objects
		// in an application that never did anything related to these references.
		return;
	}

#if __DEV
	bool watched = (g_WatchedBaseObject == this);  
	if (watched)
	{
		g_WatchedBaseObject = NULL;
	}
#endif

	pgBaseNode *nodePool = s_KnownReferencePool.GetPool_Nodes();
	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
#if TRACK_REFERENCE_BACKTRACE
	pgBaseKnownReference_Backtrace *tracePool = s_KnownReferencePool.GetPool_Backtraces();
#endif

	// If the first reference is a page map, skip it.
	BASE_TASKLOG_ONLY(s_TaskLog.Log('CAKR',(u32)this,(u32)m_FirstNode,m_FirstNode?(u32)m_FirstNode->Next:0));
	pgDisplayf("clear all known refs - first node is %p",m_FirstNode);
	pgBaseNode **head = &m_FirstNode;
	while (*head && !IsKnownReference(*head))
		head = &(*head)->Next;

	pgBaseNode *i = *head;

	while (i) {
		ptrdiff_t r = i - nodePool;
		pgDisplayf("clearing-all known ref @%u",r);
#if __DEV
		if (watched)
		{
			PRINT_STACK_TRACE(tracePool[r]);
			Displayf("clearing-all known ref @%" PTRDIFFTFMT "u",r);
		}
#endif

		PRINT_STACK_TRACE(tracePool[r]);
		if(assertIfAny)
		{
			Warningf("pgBase::ClearAllKnownRefs - Object %p has outstanding reference at %p",this,refPool[r]);
		}
		Assert(IsKnownReference(i));

		// Displayf("nuking known ref %p @%p->%p cached %p",ref,ref->Reference,*ref->Reference,ref->CachedValue);

		s_KnownRefHead_ByContents.RemoveSelfLink(*refPool[r],s_KnownReferencePool.GetPool_Links_ByContents(),r);
		s_KnownRefHead_ByAddress.RemoveSelfLink(refPool[r],s_KnownReferencePool.GetPool_Links_ByAddress(),r);

		// Clear out the backpointer.  All we really assume about it is that it's a pointer
		// to SOMETHING in this object's owned memory.
		// TODO: If this is a root resource reference, check that the pointer is somewhere within the page map.
		*refPool[r] = NULL;

		// And mark the entry freed as well.
		refPool[r] = NULL;

		// Skip to next entry before we nuke this one.
		i = i->Next;

		// Remove the entry from the pool.
		s_KnownReferencePool.Delete(nodePool + r);
	}

#if __ASSERT
	if(assertIfAny)
	{
		char debugName[128];
		Assertf(!*head,"pgBase::ClearAllKnownRefs - Object %p (%s) still some known references, see TTY for details.  This may be a bug in higher-level code; watch out for NULL pointer dereferences later on.",this,GetDebugName(debugName, sizeof(debugName)));
	}
#endif // __ASSERT

	// Clear the head pointer, erasing the entire list
	*head = NULL;
}


bool pgBase::IsRefKnown(void** ppReference) const
{
	if (!ppReference)
		return false;

	SYS_CS_SYNC(s_KnownRefToken);

	pgBaseNode *nodePool = s_KnownReferencePool.GetPool_Nodes();
	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();

	pgBaseNode *i = m_FirstNode;
	while (i && !IsKnownReference(i))
		i = i->Next;
	while (i) {
		Assert(IsKnownReference(i));
		if (refPool[i - nodePool] == ppReference)
			return true;
		Assertf(i != i->Next,"IsRefKnown - loop in reference chain at %p",i);
		i = i->Next;
	}

	//	It was not found in the list.
	return false;
}

#endif	// ENABLE_KNOWN_REFERENCES

#if (NO_REFERENCE_TRACKING || !ENABLE_KNOWN_REFERENCES) && !__FINAL
bool pgBase::IsPointerKnown(void **)
{
	return false;
}
#elif !__FINAL
bool pgBase::IsPointerKnown(void **ppReference)
{
	if (!ppReference)
		return false;

	SYS_CS_SYNC(s_KnownRefToken);

	sysBuddyNodeIdx i = s_KnownRefHead_ByAddress.GetHeadLink(ppReference);
	pgBaseKnownReference_Reference *refPool = s_KnownReferencePool.GetPool_References();
	pgBaseKnownReference_Link *linkPool = s_KnownReferencePool.GetPool_Links_ByAddress();
	while (i != sysBuddyHeap::c_NONE)
	{
		Assert(i != sysBuddyHeap::c_INVALID);
		if (refPool[i] == ppReference)
			return true;
		i = linkPool[i];
	}
	return false;
}
#endif

#if ENABLE_KNOWN_REFERENCES

bool pgBase::IsReferenced(void) const
{
	SYS_CS_SYNC(s_KnownRefToken);

	pgBaseNode *i = m_FirstNode;
	while (i && !IsKnownReference(i))
		i = i->Next;
	return IsKnownReference(i);
}

#else

#endif

#if TABLE_SEARCH_FIXUPS

template <size_t pageSize> class TableSearchHelper {
public:
	void Init(size_t maxSize)
	{
		m_MaxPages = (maxSize + pageSize - 1) / pageSize;
		m_Lut = rage_new s8[m_MaxPages];
		m_ChunkCount = 0;
		sysMemSet(m_Lut,0xFF,m_MaxPages);
		// printf("TableSearchHelper allocated %u bytes.\n",m_MaxPages);
	}
	void Shutdown()
	{
		delete[] m_Lut;
		m_Lut = NULL;
	}
	void Set(const datResourceChunk *chunks,void* datResourceChunk::* addr,size_t base,size_t count)
	{
		if (count) 
		{
#if RESOURCE_HEADER
			if (count == 1 && sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL)->IsValidPointer(chunks[0].*addr))
			{
				m_Mini = (size_t)(chunks[base].*addr);
				m_Maxi = m_Mini + chunks[base].Size;				
				m_ChunkCount = count;

				m_Addresses[0] = m_Lut + (size_t(chunks[base].*addr) - m_Mini) / pageSize;
				const size_t size = chunks[base].Size / pageSize;
				m_Sizes[0] = size;

				sysMemSet(m_Addresses[0], base, m_Sizes[0]);
			}
			else
#endif
			{
				size_t mini = (size_t)(chunks[base].*addr);
				size_t maxi = mini + chunks[base].Size;
				for (size_t i=1; i<count; i++) {
					size_t thisMini = (size_t)(chunks[base+i].*addr);
					size_t thisMaxi = thisMini + chunks[base+i].Size;
					if (thisMini < mini)
						mini = thisMini;
					if (thisMaxi > maxi)
						maxi = thisMaxi;
				}
				m_Mini = mini;
				m_Maxi = maxi;
				TRAP_ONLY(size_t lutSize = (maxi - mini) / pageSize);
				TrapGT(lutSize,m_MaxPages);
				m_ChunkCount = count;

				for (size_t i=0; i<count; i++)
					sysMemSet(m_Addresses[i] = (m_Lut + ((size_t(chunks[base+i].*addr) - m_Mini) / pageSize)), base+i, m_Sizes[i] = (chunks[base+i].Size / pageSize));
			}
		}
		else
			m_Mini = m_Maxi = m_ChunkCount = 0;
	}
	void Reset()
	{
		// Reset the entries we touched, faster than clearing the entire table.
		for (size_t i=0; i<m_ChunkCount; i++)
			sysMemSet(m_Addresses[i], 0xFF, m_Sizes[i]);
		m_ChunkCount = 0;
	}
	int Lookup(void *address)
	{
		size_t a = (size_t)address;
		if (a >= m_Mini && a < m_Maxi)
			return m_Lut[(a - m_Mini) / pageSize];
		else
			return -1;
	}
	bool Contains(void *address)
	{
		return Lookup(address) != -1;
	}
private:
	size_t m_MaxPages;
	size_t m_Mini, m_Maxi, m_ChunkCount;
	s8 *m_Lut;
	s8 *m_Addresses[datResourceChunk::MAX_CHUNKS];
	size_t m_Sizes[datResourceChunk::MAX_CHUNKS];
};

const u8 leftMasks[8] =  { 0xFF, 0x7F, 0x3F, 0x1F, 0x0F, 0x07, 0x03, 0x01 };
const u8 rightMasks[8] = { 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE, 0xFF };
const u8 bitLut[8] =	 { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };

template <size_t pageSize> class TableSearchHelper2 {
public:
	void Init(size_t maxSize)
	{
		m_MaxPages = (((maxSize + pageSize - 1) / pageSize) + 7) >> 3;
		m_Bits = rage_new u8[m_MaxPages];
		m_ChunkCount = 0;
		sysMemSet(m_Bits,0,m_MaxPages);
		// printf("TableSearchHelper2 allocated %u bytes.\n",m_MaxPages);
	}
	void Shutdown()
	{
		delete[] m_Bits;
		m_Bits = NULL;
	}

	void SetBitsInRange(size_t start,size_t count)
	{
		// Fill in the valid masks
		size_t maskLeft = start;
		size_t maskRight = (start + count - 1);
		size_t maskLeftByte = maskLeft >> 3;
		size_t maskRightByte = maskRight >> 3;
		if (maskLeftByte == maskRightByte)
			m_Bits[maskLeftByte] |= leftMasks[maskLeft & 7] & rightMasks[maskRight & 7];
		else {
			// Left edge
			m_Bits[maskLeftByte] |= leftMasks[maskLeft & 7];
			// Fill in middle quickly
			if (maskRightByte - maskLeftByte > 1)
				sysMemSet(m_Bits+maskLeftByte+1,0xFF,maskRightByte - maskLeftByte - 1);
			// Right edge
			m_Bits[maskRightByte] |= rightMasks[maskRight & 7];
		}
	}
	void ResetBitsInRange(size_t start,size_t count)
	{
		// Fill in the valid masks
		size_t maskLeft = start;
		size_t maskRight = (start + count - 1);
		size_t maskLeftByte = maskLeft >> 3;
		size_t maskRightByte = maskRight >> 3;
		if (maskLeftByte == maskRightByte)
			m_Bits[maskLeftByte] &= ~(leftMasks[maskLeft & 7] & rightMasks[maskRight & 7]);
		else {
			// Left edge
			m_Bits[maskLeftByte] &= ~leftMasks[maskLeft & 7];
			// Fill in middle quickly
			if (maskRightByte - maskLeftByte > 1)
				sysMemSet(m_Bits+maskLeftByte+1,0x00,maskRightByte - maskLeftByte - 1);
			// Right edge
			m_Bits[maskRightByte] &= ~rightMasks[maskRight & 7];
		}
	}

	void Set(const datResourceChunk *chunks,void* datResourceChunk::* addr,size_t base,size_t count)
	{
		if (count) 
		{
#if RESOURCE_HEADER
			sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL);
			if (count == 1 && pAllocator->IsValidPointer(chunks[0].*addr))
			{
				m_Mini = (size_t)(chunks[base].*addr);
				m_Maxi = m_Mini + chunks[base].Size;			
				m_ChunkCount = count;

				SetBitsInRange(m_Offsets[0] = 0, m_Sizes[0] = 1);
			}
			else
#endif
			{
				size_t mini = (size_t)(chunks[base].*addr);
				size_t maxi = mini + chunks[base].Size;
				for (size_t i=1; i<count; i++) 
				{
					size_t thisMini = (size_t)(chunks[base+i].*addr);
					size_t thisMaxi = thisMini + chunks[base+i].Size;
					if (thisMini < mini)
						mini = thisMini;
					if (thisMaxi > maxi)
						maxi = thisMaxi;
				}
				m_Mini = mini;
				m_Maxi = maxi;
				TRAP_ONLY(size_t lutSize = (((maxi - mini) / pageSize) + 7) >> 3);
				TrapGT(lutSize,m_MaxPages);
				m_ChunkCount = count;
				for (size_t i=0; i<count; i++)
					SetBitsInRange(m_Offsets[i] = ((size_t(chunks[base+i].*addr) - m_Mini) / pageSize),m_Sizes[i] = chunks[base+i].Size / pageSize);
			}			
		}
		else
			m_Mini = m_Maxi = m_ChunkCount = 0;
	}
	void Reset()
	{
		// Reset the entries we touched, faster than clearing the entire table.
		for (size_t i=0; i<m_ChunkCount; i++)
			ResetBitsInRange(m_Offsets[i], m_Sizes[i]);
		m_ChunkCount = 0;
	}
	bool Contains(void *address)
	{
		size_t a = (size_t)address;
		if (a >= m_Mini && a < m_Maxi) {
			size_t index = (a - m_Mini) / pageSize; 
			return (m_Bits[index >> 3] & bitLut[index & 7]) != 0;
		}
		else
			return false;
	}
private:
	size_t m_MaxPages;
	size_t m_Mini, m_Maxi, m_ChunkCount;
	u8 *m_Bits;
	size_t m_Offsets[datResourceChunk::MAX_CHUNKS];
	size_t m_Sizes[datResourceChunk::MAX_CHUNKS];
};


TableSearchHelper<g_rscVirtualLeafSize> s_SearchSrcVirt;
TableSearchHelper<g_rscPhysicalLeafSize> s_SearchSrcPhys;
TableSearchHelper2<g_rscVirtualLeafSize> s_SearchDestVirt;
TableSearchHelper2<g_rscPhysicalLeafSize> s_SearchDestPhys;
#endif
#if BINARY_SEARCH_FIXUPS
struct SortByRegionAddress
{
	bool operator()( const datResourceSortedChunk& a, const datResourceSortedChunk& b ) const
	{
		// Regions cannot overlap so this test is sufficient.
		return a.Start < b.Start;
	}
};

struct SearchByAddress
{
	bool operator()( const datResourceSortedChunk& a, const datResourceSortedChunk &b ) const
	{
		// Regions may overlap.
		return (char*)a.Start + a.Size <= (char*)b.Start;
	}
};
#endif

datResource::datResource(const datResourceMap &map,const char *debugName,bool isDefrag) : m_Map(map), m_DebugName(debugName), m_WasDefrag(datResource_IsDefragmentation) {
	m_Next = datResource_sm_Current;
	datResource_sm_Current = this;
	++g_DisableInitNan;
#if ENABLE_KNOWN_REFERENCES
	datResource_IsDefragmentation = isDefrag;
#else
	(void)isDefrag;
#endif
#if __ASSERT
	int mc = map.VirtualCount+map.PhysicalCount;
	for (int i=0; i<mc; i++)
		Assertf(map.Chunks[i].SrcAddr && map.Chunks[i].Size,"Invalid resource chunk, index %d src=%p dest=%p, size=%" SIZETFMT "u",i,map.Chunks[i].SrcAddr,map.Chunks[i].DestAddr,map.Chunks[i].Size);
#endif
#if TABLE_SEARCH_FIXUPS
	if (sysThreadType::IsUpdateThread())
	{
	 	s_SearchSrcVirt.Set(map.Chunks,&datResourceChunk::SrcAddr,0,map.VirtualCount);
		s_SearchSrcPhys.Set(map.Chunks,&datResourceChunk::SrcAddr,map.VirtualCount,map.PhysicalCount);
		s_SearchDestVirt.Set(map.Chunks,&datResourceChunk::DestAddr,0,map.VirtualCount);
		s_SearchDestPhys.Set(map.Chunks,&datResourceChunk::DestAddr,map.VirtualCount,map.PhysicalCount);
	}
	else
#endif
#if BINARY_SEARCH_FIXUPS
	{
		Assign(m_MapCount, map.VirtualCount+map.PhysicalCount);
		for (int i=0; i<m_MapCount; i++) {
			Src[i].Start = map.Chunks[i].SrcAddr;
			Dest[i].Start = map.Chunks[i].DestAddr;
			BitfieldAssign(Src[i].Size,map.Chunks[i].Size);
			BitfieldAssign(Dest[i].Size,map.Chunks[i].Size);
			BitfieldAssign(Src[i].SelfIndex,i);
			BitfieldAssign(Dest[i].SelfIndex,i);
		}
		// Strictly speaking Src only needs sorting if isDefrag is true, but this code isn't a performance bottleneck.
		// The real bottleneck is in ContainsAddress and GetFixup, where even doing a binary search is getting expensive.
		std::sort(Src+0,Src+m_MapCount,SortByRegionAddress());
		std::sort(Dest+0,Dest+m_MapCount,SortByRegionAddress());
	}
#endif
}


datResource::~datResource() {
#if TABLE_SEARCH_FIXUPS
	if (sysThreadType::IsUpdateThread())
	{
		s_SearchSrcVirt.Reset();
		s_SearchSrcPhys.Reset();
		s_SearchDestVirt.Reset();
		s_SearchDestPhys.Reset();
	}
#endif
	datResource_sm_Current = m_Next;
#if ENABLE_DEFRAGMENTATION
	datResource_IsDefragmentation = m_WasDefrag;
#endif
	--g_DisableInitNan;
}


#define USE_BINARY_SEARCH_FIXUPS	(BINARY_SEARCH_FIXUPS)		// force to zero to disable it without huge rebuild if you suspect trouble

bool datResource::ContainsThisAddress(void *address) const {
#if !NO_REFERENCE_TRACKING
	// If this assert fires, it means we're attempting to manually fix
	// up a tracked pointer.  This is ultimately harmless, but is a waste
	// of time.  It's harmless because the tracked pointer has already
	// been fixed up by now to point to the new location, so the tests
	// here will never succeed.  (This is important because if they
	// did succeed, we would be modifying a tracked pointer outside of
	// the system, which would break our optimizations that track pointers
	// by their current address to accelerate PatchAllReferences)
	defragCheckAssert(!datResource_IsDefragmentation || !pgBase::IsPointerKnown((void**)address));
#endif
#if TABLE_SEARCH_FIXUPS
	if (sysThreadType::IsUpdateThread())
		return s_SearchDestVirt.Contains(address) || s_SearchDestPhys.Contains(address);
#endif
#if USE_BINARY_SEARCH_FIXUPS
	const datResourceSortedChunk *start = Dest, *end = Dest + m_MapCount;
	datResourceSortedChunk search; search.Start = address; search.Size = 1;
	const datResourceSortedChunk *i = std::lower_bound(start, end, search, SearchByAddress());
	return (i != end && address >= i->Start && (char*)address < (char*)i->Start + i->Size);
#else
	return m_Map.ContainsDest(address) != -1;
#endif
}

ptrdiff_t datResource::GetFixup(void *ptr) const {
#if TABLE_SEARCH_FIXUPS
	if (sysThreadType::IsUpdateThread())
	{
		int index;
		index = s_SearchSrcVirt.Lookup(ptr);
		if (index == -1)
			index = s_SearchSrcPhys.Lookup(ptr);
		if (index != -1)
			return (char*)m_Map.Chunks[index].DestAddr - (char*)m_Map.Chunks[index].SrcAddr;
		else if (!datResource_IsDefragmentation)
			Quitf("Resource '%s' : Invalid fixup, address 0x%p is neither virtual nor physical",m_DebugName, ptr);
		return 0;
	}
#endif
#if USE_BINARY_SEARCH_FIXUPS
	const datResourceSortedChunk *start = Src, *end = Src + m_MapCount;
	datResourceSortedChunk search; search.Start = ptr; search.Size = 1;
	const datResourceSortedChunk *i = std::lower_bound(start, end, search, SearchByAddress());
	// TODO: Could store the fixup explicitly in the array; would no longer need SelfIndex.
	if (i != end && ptr >= i->Start && (char*)ptr < (char*)i->Start + i->Size)
		return (char*)m_Map.Chunks[i->SelfIndex].DestAddr - (char*)m_Map.Chunks[i->SelfIndex].SrcAddr;
	else if (!datResource_IsDefragmentation)
	{
		Quitf(ERR_SYS_INVALIDRESOURCE_5,"Resource '%s' : Invalid fixup, address 0x%p is neither virtual nor physical",m_DebugName, ptr);
	}
	return 0;
#else
	// TODO: Some sort of cache here?  After defragmentation we have no guaranteed mapping though.
	int index = m_Map.ContainsSrc(ptr);
	if (index != -1) {
		return (char*)m_Map.Chunks[index].DestAddr - (char*)m_Map.Chunks[index].SrcAddr;
	}
	else if (!datResource_IsDefragmentation) {
		Quitf("Resource '%s' : Invalid fixup, address 0x%p is neither virtual nor physical",m_DebugName, ptr);
	}
	return 0;
#endif
}

void pgBase::InitClass(int knownRefPoolSize)
{
#if ENABLE_KNOWN_REFERENCES
	s_KnownReferencePool.Init(knownRefPoolSize);

	// The head structures depend on the size of the memory being managed, which is now abstracted.
	s_KnownRefHead_ByAddress.Init();
	s_KnownRefHead_ByContents.Init();
#else
	(void)knownRefPoolSize;
#endif

#if ENABLE_DEFRAGMENTATION
	g_pgHandleFirstFree = 1;
	// Initialize the rest of the free list.
	for (u32 i=1; i<pgMaxHandles; i++)
		g_pgHandleArray[i] = (pgBase*)(size_t)(i+1);
	g_pgHandleFirstFree2 = 1;
	// Initialize the rest of the free list.
	for (u32 i=1; i<pgMaxHandles2; i++)
		g_pgHandleArray2[i] = (pgBase*)(size_t)(i+1);

	s_pgHandleLinkHeads.Init();
	s_pgHandleLinkHeads2.Init();

	// The pools are sized based on the number of handles or references, not the address space being tracked.
	memset(s_KnownReferencePool.GetPool_Links_ByContents(),0xFF,s_KnownReferencePool.GetSize() * sizeof(pgBaseKnownReference_Link));
	memset(s_KnownReferencePool.GetPool_Links_ByAddress(),0xFF,s_KnownReferencePool.GetSize() * sizeof(pgBaseKnownReference_Link));
	memset(s_pgHandleLinkArray,0xFF,pgMaxHandles * sizeof(pgBaseKnownReference_Link));
	memset(s_pgHandleLinkArray2,0xFF,pgMaxHandles2 * sizeof(pgBaseKnownReference_Link));
#endif

#if TABLE_SEARCH_FIXUPS
	s_SearchSrcVirt.Init(sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetHeapSize());
	s_SearchSrcPhys.Init(sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetHeapSize());
	s_SearchDestVirt.Init(sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetHeapSize());
	s_SearchDestPhys.Init(sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetHeapSize());
#endif
}


void pgBase::ShutdownClass()
{
#if TABLE_SEARCH_FIXUPS
	s_SearchSrcVirt.Shutdown();
	s_SearchSrcPhys.Shutdown();
	s_SearchDestVirt.Shutdown();
	s_SearchDestPhys.Shutdown();
#endif

#if ENABLE_DEFRAGMENTATION
	s_pgHandleLinkHeads2.Shutdown();
	s_pgHandleLinkHeads.Shutdown();
#endif
#if ENABLE_KNOWN_REFERENCES
	s_KnownRefHead_ByContents.Shutdown();
	s_KnownRefHead_ByAddress.Shutdown();
	s_KnownReferencePool.Shutdown();
#endif
}



pgBaseMetaDataType* pgBaseMetaDataType::First;

pgBaseMetaDataType::pgBaseMetaDataType(u32 type) : Next(First), Type(type) { 
	First = this; 
#if __ASSERT
	// Need to make sure neither "end byte" is nonzero since either one may alias pgBasePageMap::IsMetaData depending on endianness.
	AssertMsg(((type >> 24) & 0xFF) && (type & 0xFF),"Type must be FOURCC code");
	pgBaseMetaDataType *i = Next;
	while (i) {
		Assertf(type != i->Type,"Duplicate pgBaseMetaDataType type value %x encountered.",type);
		i = i->Next;
	}
#endif	// __ASSERT
}

pgBaseMetaDataType::~pgBaseMetaDataType() {
	// Patch selves out of registration list.
	pgBaseMetaDataType **pHead = &First;
	while ((*pHead) != this)
		pHead = &(*pHead)->Next;
	*pHead = Next;
}

pgBaseMetaDataType *pgBaseMetaDataType::Lookup(u32 type) {
	pgBaseMetaDataType *i = First;
	while (i) {
		if (i->Type == type)
			break;
		else
			i = i->Next;
	}
	return i;
}

pgBaseMetaData* pgBase::LookupMetaData(u32 type) {
	pgBaseMetaData *i = IsPageMap(m_FirstNode)? IsMetaData(m_FirstNode->Next) : IsMetaData(m_FirstNode);
	while (i) {
		if (i->Type == type)
			break;
		else
			i = static_cast<pgBaseMetaData*>(i->Next);
	}
	return i;
}

bool pgBase::HasPageMap() const
{
	return IsPageMap(m_FirstNode) != NULL;
}

#if __RESOURCECOMPILER
void pgBase::AttachMetaData(pgBaseMetaData *md) {
	md->Next = m_FirstNode;
	m_FirstNode = md;
}
#endif

static const u32 pgBasePN_DiscriminatorLow = 0x11113333UL;
static const u32 pgBasePN_DiscriminatorHigh = 0x55557777UL;

pgBasePlatformNeutral::pgBasePlatformNeutral()
{
#if !__64BIT
	if (!datResource_sm_Current)
	{
		m_Pad.Words.Low = pgBasePN_DiscriminatorLow;
		m_Pad.Words.High = pgBasePN_DiscriminatorHigh;
	}
#endif
}

struct pgBaseRaw32
{
	u32 m_Vptr;
	u32 m_FirstNode;
	u32 m_PadLow;
	u32 m_PadHigh;
};

struct pgBaseRaw64
{
	u32 m_VptrLow;
	u32 m_VptrHigh;
	u32 m_FirstNodeLow;
	u32 m_FirstNodeHigh;
};

void pgBasePlatformNeutral::BeforePlace(pgBasePlatformNeutral* that, char originatingPlatform)
{
	bool needsSwap = sysGetByteSwap(originatingPlatform);
	bool was64 = sysIs64Bit(originatingPlatform);


#if __ASSERT || __64BIT
	pgBaseRaw32 thisAs32Copy = *reinterpret_cast<pgBaseRaw32*>(that);
#endif 

	// Verify that the originating platform data matches what we see in the file
#if __ASSERT
	// Check the discriminator bytes to see if we came from a 32-bit resource, and if we need byte swapping
	// Note that we can't tell if we need byte swapping on 64-bit platforms, so lets hope for the best there - that they remain 
	// little endian. We could store the ID of the originating platform and pass that in, if necessary
	// (or we could look at the vptrs... One end or the other should begin with a bunch of zeros)

	if (thisAs32Copy.m_PadLow == pgBasePN_DiscriminatorLow && thisAs32Copy.m_PadHigh == pgBasePN_DiscriminatorHigh)
	{
		// Looks like 32-bit
		Assertf(!was64 && !needsSwap, "Found 32-bit no-swap discriminator, but originating platform is '%c'", originatingPlatform);
	}
	else if (thisAs32Copy.m_PadLow == sysEndian::Swap(pgBasePN_DiscriminatorLow) && thisAs32Copy.m_PadHigh == sysEndian::Swap(pgBasePN_DiscriminatorHigh))
	{
		// Looks like 32-bit that needs byte swap
		Assertf(!was64 && needsSwap, "Found 32-bit swap discriminator, but originating platform is '%c'", originatingPlatform);
	}
	else
	{
		Assertf(was64, "Originating platform '%c' claimed to be 32 bit, but no discriminators matched", originatingPlatform);
		Assertf(!needsSwap, "Originating platform '%c' needs to be byte swapped, can't handle 64-bit swapping yet", originatingPlatform);
	}
#endif


#if __64BIT
	if (!was64)
	{
		if (needsSwap)
		{
			sysEndian::SwapMe(thisAs32Copy.m_FirstNode);
		}

		pgBaseRaw64* thisAs64 = reinterpret_cast<pgBaseRaw64*>(that);
		thisAs64->m_VptrHigh = 0x0;
		thisAs64->m_FirstNodeLow = thisAs32Copy.m_FirstNode;
		thisAs64->m_FirstNodeHigh = 0x0;
	}
#else // __64BIT
	if (was64)
	{
		pgBaseRaw64 thisAs64Copy = *reinterpret_cast<pgBaseRaw64*>(that);

		if (needsSwap)
		{
			sysEndian::SwapMe(thisAs64Copy.m_FirstNodeLow);
		}								  

		// This was a 64-bit resource, Shuffle the bytes around first
		Assertf(thisAs64Copy.m_FirstNodeHigh== 0x0, "Expected 0x0 for the high word of m_FirstNode, got 0x%08x", thisAs64Copy.m_FirstNodeHigh);
		pgBaseRaw32* thisAs32 = reinterpret_cast<pgBaseRaw32*>(that);
		thisAs32->m_FirstNode = thisAs64Copy.m_FirstNodeLow;
		thisAs32->m_PadLow = pgBasePN_DiscriminatorLow;
		thisAs32->m_PadHigh = pgBasePN_DiscriminatorHigh;
	}
	else
	{
		if (needsSwap)
		{
			pgBaseRaw32* thisAs32 = reinterpret_cast<pgBaseRaw32*>(that);
			sysEndian::SwapMe(thisAs32->m_FirstNode);
			sysEndian::SwapMe(thisAs32->m_PadLow);
			sysEndian::SwapMe(thisAs32->m_PadHigh);
		}
	}
#endif // __64BIT
	// OK everything is in the right order, now we can call constructors.
}

void pgBasePlatformNeutral::Place(pgBasePlatformNeutral* that, char originatingPlatform)
{
	BeforePlace(that, originatingPlatform);
	::new(that) pgBasePlatformNeutral();
}

#if __DECLARESTRUCT
void pgBasePlatformNeutral::DeclareStruct(datTypeStruct& s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(pgBasePlatformNeutral, pgBase)
#if !__64BIT
		SSTRUCT_FIELD(pgBasePlatformNeutral, m_Pad.Words.Low)
		SSTRUCT_FIELD(pgBasePlatformNeutral, m_Pad.Words.High)
#endif
	SSTRUCT_END(pgBasePlatformNeutral)
}
#endif // __DECLARESTRUCT


} // namespace rage
