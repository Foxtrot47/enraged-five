// 
// paging/ref.h
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PAGING_REF_H
#define PAGING_REF_H

#include "data/struct.h"
#include "paging/base.h"
#include "system/memops.h"


namespace rage {

template <class _T> struct pgRef {
	SYS_MEM_OPS_PTR_LIKE_TYPE(_T);
	pgRef() {
#if !__SPU
		if (!datResource_sm_Current)
			ptr = 0;
		else if (!datResource_IsDefragmentation && ptr) {
			datResource_sm_Current->PointerFixup(ptr);
			ADD_KNOWN_REF(ptr);
		}
		// else do nothing if it's a defrag, we handle that in PatchAllReferences... right?
#endif
	}
	explicit pgRef(_T *p) {
		ptr = p;
		SAFE_ADD_KNOWN_REF(ptr);
	}

	pgRef(const pgRef& other) {
		ptr = other.ptr;
		SAFE_ADD_KNOWN_REF(ptr);
	}

	void Release() {
		if (ptr) {
			SAFE_REMOVE_KNOWN_REF(ptr);
			ptr->Release();
			ptr = NULL;
		}
	}

	~pgRef() {
		SAFE_REMOVE_KNOWN_REF(ptr);
	}

#if __SPU
	bool IsLocalStore() const { return (unsigned)ptr < 256*1024; }
#endif
	_T& operator*() const { return *ptr; }
	_T* operator->() const { return ptr; }
	operator _T*() const { return ptr; }
	void operator=(_T* that) { 
		UPDATE_KNOWN_REF(ptr,that);
	}
	void operator=(const pgRef& that) {
		UPDATE_KNOWN_REF(ptr, that.ptr);
	}

	DECLARE_PADDED_POINTER(_T,ptr); 
};

#if __DECLARESTRUCT
template <class _T> inline void datSwapper(pgRef<_T> &ref)		{ datSwapper((char*&)(ref.ptr)); }
#endif


}

#endif		// PAGING_REF_H
