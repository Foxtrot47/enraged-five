// 
// paging/base.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PAGING_BASE_H
#define PAGING_BASE_H

#include "data/base.h"
#include "data/struct.h"
#include "system/interlocked.h"
#include "system/param.h"

#include "paging/base_spu.h"

namespace rage {

class datResource;
class pgAssetBase;
struct datResourceMap;
class sysMemAllocator;

/* 
	pgBase defines additional entry points necessary for objects
	that might be asynchronously loaded.  In a perfect world, nobody
	should have to know how they are being created or destroyed,
	but in complex systems such notifications can avoid people attempting
	to abuse resource constructors.

	Note that there is currently no callback for relocating objects.
	The system simply invokes PageOut on the original object, copies
	it to a new, possibly overlapping address, invokes the resource
	constructor at the new address, and then calls PageIn.
*/

// If set, pgBase offers a pgBase::GetDebugName() function.
#define BASE_DEBUG_NAME (1 && !__FINAL && !__SPU)


#define DECLARE_THREADED_PLACE() \
	static const bool sm_ThreadedPlace = true

// Convenience macro that hides some type-casting crap
#if ENABLE_KNOWN_REFERENCES
#define IS_REF_KNOWN(pBase)	((pBase) && (pBase)->IsRefKnown((void**)&(pBase)))
#define IS_POINTER_KNOWN(pBase)	((pBase) && (pBase)->IsPointerKnown((void**)&(pBase)))
#else
#define IS_REF_KNOWN(pBase)			false
#define IS_POINTER_KNOWN(pBase)		false
#endif

#if __ASSERT
XPARAM(defragcheck);
#define defragCheckAssert(x)			Assert(!PARAM_defragcheck.Get() || (x))
#define defragCheckAssertf(x,...)		Assertf(!PARAM_defragcheck.Get() || (x),__VA_ARGS__)
#else
#define defragCheckAssert(x)
#define defragCheckAssertf(...)
#endif

#if __SPU

#define ADD_KNOWN_REF(pBase)

#define REMOVE_KNOWN_REF(pBase)

#define REMOVE_REF_IF_KNOWN(pBase)

#define SAFE_ADD_KNOWN_REF(pBase)

#define SAFE_REMOVE_KNOWN_REF(pBase)

#define DECLARE_DUMMY_ADD_REMOVE_KNOWN_REF

#define UPDATE_KNOWN_REF(ptr,val)	((ptr) = (val))

class pgBase: public datBase {
public:
	static void AddKnownRef(uint32_t eaPool,uint32_t eaBase,uint32_t eaReference
#if TRACK_REFERENCE_BACKTRACE
		,uint16_t backtraceIndex
#endif
		);
	static void RemoveKnownRef(uint32_t eaPool,uint32_t eaBase,uint32_t eaReference);
private:
	void *m_FirstNode;
};

#else	// !__SPU

#if ENABLE_KNOWN_REFERENCES

// pBase cannot be NULL.
#define ADD_KNOWN_REF(pBase) ((pBase)->AddKnownRef((void**)&(pBase)))

// pBase cannot be NULL.
#define REMOVE_KNOWN_REF(pBase) ((pBase)->RemoveKnownRef((void**)&(pBase)))

// Checks to see if a reference is known, and removes it if found
#define REMOVE_REF_IF_KNOWN(pBase) ((pBase)->RemoveRefIfKnown((void**)&(pBase)))

// Only invokes ADD_KNOWN_REF if pBase is non-NULL and we're not defragmenting
#define SAFE_ADD_KNOWN_REF(pBase)	do { if (!datResource_IsDefragmentation && pgBase::IsTrackedAddress((void*)(pBase))) ADD_KNOWN_REF(pBase); } while (0)

// Only invokes REMOVE_KNOWN_REF if pBase is non-NULL and we're not defragmenting
#define SAFE_REMOVE_KNOWN_REF(pBase)	do { if (!datResource_IsDefragmentation && pgBase::IsTrackedAddress((void*)(pBase))) REMOVE_KNOWN_REF(pBase); } while (0)

// Only invokes REMOVE_KNOWN_REF if pBase is non-NULL and we're not defragmenting
#define SAFE_REMOVE_REF_IF_KNOWN(pBase)	do { if (!datResource_IsDefragmentation && pgBase::IsTrackedAddress((void*)(pBase))) REMOVE_REF_IF_KNOWN(pBase); } while (0)

#define DECLARE_DUMMY_ADD_REMOVE_KNOWN_REF \
	void AddKnownRef(void**) { } \
	void RemoveKnownRef(void**) { }

// Convenience macro for handling assignment; NOTE that it evaluates 'val' twice, so it shouldn't be something expensive.
// We use macros here instead of templates because we track __FILE__ and __LINE__ in ADD_KNOWN_REF.  Well, we used to anyway.
#define UPDATE_KNOWN_REF(ptr,val) do { \
	if ((ptr) != (val)) { \
	AssertMsg(!datResource_IsDefragmentation,"Calling UPDATE_KNOWN_REF during defrag probably isn't what you wanted."); \
	SAFE_REMOVE_KNOWN_REF(ptr); \
	(ptr) = (val); \
	SAFE_ADD_KNOWN_REF(ptr); \
	} } while (0)

#else

#define ADD_KNOWN_REF(pBase)

#define REMOVE_KNOWN_REF(pBase)

#define REMOVE_REF_IF_KNOWN(pBase)

#define SAFE_ADD_KNOWN_REF(pBase)

#define SAFE_REMOVE_KNOWN_REF(pBase)

#define SAFE_REMOVE_REF_IF_KNOWN(pBase)

#define DECLARE_DUMMY_ADD_REMOVE_KNOWN_REF

#define UPDATE_KNOWN_REF(ptr,val)	((ptr) = (val))

#endif

#if __BANK
#define BANK_SAFE_ADD_KNOWN_REF(pBase) SAFE_ADD_KNOWN_REF(pBase)
#define BANK_SAFE_REMOVE_KNOWN_REF(pBase) SAFE_REMOVE_KNOWN_REF(pBase)
#else
#define BANK_SAFE_ADD_KNOWN_REF(pBase) ADD_KNOWN_REF(pBase)
#define BANK_SAFE_REMOVE_KNOWN_REF(pBase) REMOVE_KNOWN_REF(pBase)
#endif


class pgBase
//#if __BANK
//	: public datBase 
//#endif
{
public:
	// PURPOSE:	Define as true in derived classes to allow placement from streaming thread
	static const bool sm_ThreadedPlace = false;

	// DO NOT BE TEMPTED TO ADD A RESOURCE CONSTRUCTOR HERE.
	// The default logic in pgBase handles fixups.
	pgBase();

	virtual ~pgBase() {Destroy();}

	// Bounds need this.  Objects being copied over intentionally do NOT inherit the original's known reference list.
	pgBase& operator=(const pgBase& ) { return *this; }

	//
	// PURPOSE
	//	Destroy the contents of the object
	//
	void Destroy();

	// check the object allocs are still valid
	void Validate(u32 userData);

#if ENABLE_KNOWN_REFERENCES
	//
	// PURPOSE
	//	Add the provided pointer to the list of pointers that need to be
	//	updated if this object is deleted.
	// PARAMS
	//	ppReference	-	The address of the reference.
	//
	void	AddKnownRef					(void** ppReference) const;

	//
	// PURPOSE
	//	Remove the provided pointer from the list of pointers that need
	//	to be updated if this object is deleted.
	// PARAMS
	//	ppReference	-	The address of the reference.
	//
	void RemoveKnownRef(void** ppReference) const;


	//
	// PURPOSE
	//	Remove the provided pointer from the list of pointers that need
	//	to be updated if this object is deleted. Checks that the ref is known
	//  before deleting it. Equivaled to IS_KNOWN_REF followed by REMOVE_KNOWN_REF
	// PARAMS
	//	ppReference	-	The address of the reference.
	//
	void RemoveRefIfKnown(void** ppReference) const;


	//
	// PURPOSE
	//	Auto-nulls all the known pointers that point to this object.
	// PARAM
	//  assertIfAny - generate an warning/assert if any references are cleared
	// NOTES
	//	This is done to prevent dangling pointers throughout the system.
	//	It is used directly by the destructor, but is also useful for
	//	objects whose storage is re-used instead of using delete/new to
	//	create a new one.
	//
	void ClearAllKnownRefs(bool assertIfAny=true) const;

	//
	// PURPOSE
	//	Determine if the ref (which should be pointing to this) is one
	//	that we are already aware of and tracking or not.
	// PARAMS
	//	ppReference	-	The address of the reference.
	// RETURNS
	//	Whether or not it is known (in the list on know refs).
	//
	bool IsRefKnown(void **ppReference) const;
#endif

#if BASE_DEBUG_NAME
	//
	// PURPOSE
	//	Return a user-friendly name that identifies this individual resource.
	//	In case of a texture, for example, this could be the texture name.
	// PARAMS
	//	buffer - Buffer which MIGHT be used to store the name in.
	//  bufferSize - Number of bytes available in "buffer".
	// RETURNS
	//	The name of this resource, guaranteed to be a non-NULL string.
	//  *KEEP IN MIND* that this returned value could be "buffer", but doesn't
	//  necessarily have to be! It might also point directly into this resource,
	//  or a string literal. As such, the return value of this function should
	//  only be considered valid until the buffer provided is no longer good,
	//  or until this resource is destroyed, whichever comes first.
	virtual const char *GetDebugName(char * /*buffer*/, size_t /*bufferSize*/) const		{ return "?"; }

#if __ASSERT
	virtual bool ExpectDuplicatesByName() const { return false; }
#endif
#endif // BASE_DEBUG_NAME


#if !__FINAL
	// PURPOSE
	//	Determine if this pointer is known by the system at all.
	static bool IsPointerKnown(void **ppReference);
#endif

	//
	// PURPOSE
	//	Determine if there are any known references in the list.
	// RETURNS
	//	Whether or not there are known references in the list.
	//
#if ENABLE_KNOWN_REFERENCES
	bool IsReferenced(void) const;
#else
	bool IsReferenced(void) const { return false; }
#endif

#if !__SPU
	void MakeDefragmentable(const datResourceMap &map,bool needUnlock = true);

	void RegenerateMap(datResourceMap &map) const;

	// PURPOSE:	Determines whether the map contains the specified pointer or not.
	// RETURNS:	Zero-based slot index of the chunk containing that pointer, or -1 if no match
	int MapContainsPointer(void *ptr) const;

	// PURPOSE:	Temporarily change the map allocation from MAX_CHUNKS to specified value.
	//			Useful when you know an object is pretty small and doesn't need the default
	//			516 byte map array.  Call with zero or no parameters to reset to MAX_CHUNKS.
	// static void SetMapSize(u32 newMapSize = 0) { sm_MapSize = newMapSize; }

	// PURPOSE:	Returns current mapsize override (zero means default, MAX_CHUNKS)
	static u32 GetMapSize() { return sm_MapSize; }

#if !__FINAL
	// PURPOSE: Makes all the memory read only.
	void MakeReadOnly(bool readOnly) const;
#endif
#endif

	// PURPOSE:	Additional fixup that needs to occur on the main thread after resource construction
	void PostPlace() {}

	// PURPOSE: For very special weird cases, not for general use.
	/* bool HasPageMap() { return m_PageMap != 0; }
	void ClearPageMap() { m_PageMap = 0; } */

	static void PatchAllReferences(const datResourceMap &patch);

#if __ASSERT
	static bool ValidateAllReferences();
#endif
#if !__FINAL && ENABLE_KNOWN_REFERENCES
	static void DumpRefs();
	static void DumpUniqueRefs();
	static bool IsMemoryTracked(void *dest,size_t size);
#else
	static bool IsMemoryTracked(void *,size_t) { return false; }
#endif

	static void InitClass(int knownRefPoolSize = -1);
	static void ShutdownClass();

#if ENABLE_KNOWN_REFERENCES
	static void SetTrackedHeap(sysMemAllocator &trackedHeap);
	static bool IsTrackedAddress(void *ptr);
#else
	static bool IsTrackedAddress(void *) { return false; }
#endif

#if __DEV
	void DebugDisplayReferencing();
#endif

	virtual u32 GetHandleIndex() const;
	virtual void SetHandleIndex(u32);

	// Returns pointer to metadata attached to this resource.
	pgBaseMetaData *LookupMetaData(u32 type);
	bool HasPageMap() const;

#if __RESOURCECOMPILER
	// Attach meta data to this object.  For now, restricted to resource generation time.
	// Works if the object is a toplevel resource or not.  Note that the meta data is
	// patched into the existing linked list, not copied.
	void AttachMetaData(pgBaseMetaData *md);
#endif

	inline bool HasFirstNode() const { return m_FirstNode != 0; }

protected:
#if __RESOURCECOMPILER
	friend class pgRscBuilder;
#endif

	// For use by pgBaseAllocated only
	pgBase(pgBasePageMap *pageMap) : m_FirstNode((pgBaseNode*)pageMap) { }

	static void FreeMemory(const datResourceMap &map);

#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif

	mutable pgBaseNode *m_FirstNode;

	static u32 sm_MapSize;
};

#endif	// !__SPU

class pgBaseRefCounted: public pgBase {
public:
	pgBaseRefCounted(unsigned initRefCount) : m_RefCount(initRefCount) { }

	pgBaseRefCounted(datResource &) { }

	~pgBaseRefCounted() { Assertf(m_RefCount == 0,"pgBaseRefCounted dtor - m_RefCount is %u, not zero.",m_RefCount); }

	void AddRef() const {
		ValidateRefCount(m_RefCount);
		sysInterlockedIncrement(&m_RefCount);
	}

	void DecRef() const {
		ValidateRefCount(m_RefCount);
		sysInterlockedDecrement(&m_RefCount);
	}

	int Release() const {
		ValidateRefCount(m_RefCount);
		int result = (int) sysInterlockedDecrement(&m_RefCount);
		if (result == 0) {
			delete this;
			return 0;
		}
		else
			return result;
	}

	int GetRefCount() const { return (int) m_RefCount; }

#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif
private:
	mutable unsigned m_RefCount;

	PAD_FOR_GCC_X64_4;
};

class pgBaseAllocated: public pgBase {
public:
	pgBaseAllocated();
	~pgBaseAllocated();

private:
	u32 m_PageMapMemory[3];
};

class pgBasePlatformNeutral : public pgBase
{
public:
#if !__64BIT
	// This makes sure that derived structures can be the same size on 32-bit and 64-bit builds (minus a couple memcpys we'll need to make)
	// AND makes sure the structure is 8-byte aligned on 32-bit builds
	union
	{
		struct { u32 Low, High; } Words ;
		u64 Full; // for alignment
	} m_Pad;
#endif

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

	static void BeforePlace(pgBasePlatformNeutral* that, char originatingPlatform); // Derived classes should store a char originating platform in their top-level class and pass it in here, before doing a placement new on the object
	// (Platform ID is not stored here since that'd add another 8b to this class)

	static void Place(pgBasePlatformNeutral* that, char originatingPlatform); 

	pgBasePlatformNeutral(); 
};

}	// namespace rage

#endif
