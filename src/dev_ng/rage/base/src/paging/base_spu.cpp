// 
// paging/base_spu.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if __PPU

extern char g_grcTexture[] = "grcTexture";
extern char g_grcInstanceData[] = "grcInstanceData";
extern char g_Base_Spu[] = __FILE__;

#elif __SPU

#include "base.h"
#include "system/criticalsection_spu.h"
#include "diag/trap.h"
#include "atl/staticpool.h"
#include "paging/knownrefpool.h"
#include "system/tasklog.h"
#include "system/ppu_symbol.h"

/// NOTE - SPU VERSION DOESN'T UNDERSTAND PAGEMAPS

// The SPU code makes some assumptions about these three objects all being the same
// size to simplify some pool access.
CompileTimeAssert(sizeof(pgBaseNode) == sizeof(pgBaseKnownReference_Reference));
CompileTimeAssert(sizeof(pgBaseNode) == sizeof(pgBaseKnownReference_CachedValue));

#if __TASKLOG
DECLARE_PPU_SYMBOL(sysTaskLog,s_TaskLog);
#endif

#define pgDisplayf(fmt,...) //  Displayf

void pgBase::RemoveKnownRef(uint32_t eaPool,uint32_t eaBaseAddr,uint32_t eaReference)
{
	TASKLOG_ONLY(sysTaskLog *taskLog = PPU_SYMBOL(s_TaskLog));

	// Skip the vptr in pgBase.
	uint32_t eaBase = eaBaseAddr + OffsetOf(pgBase, m_FirstNode);

	// DMA the pgBaseKnownReferencePool data to get access to the pool size.
	// Note: we could make this smaller by just DMA'ing pgBaseKnownReferencePool::m_Size,
	// or reuse this to eliminate the pgBaseKnownReferencePool DMA get in pgBaseKnownReferencePool::Delete().
	pgBaseKnownReferencePool _pool;
	sysDmaGetAndWait(&_pool,eaPool,sizeof(_pool),spuGetTag);
	const uint32_t knownReferencePoolSize = _pool.GetSize();
	const uint32_t knownReferencePoolSize4 = knownReferencePoolSize * 4;
#if __ASSERT
	const uint32_t knownReferencePoolSize8 = knownReferencePoolSize4 * 2;
#endif

	uint32_t eaSlot;
	while ((eaSlot = sysDmaGetUInt32(eaBase, spuGetTag)) != 0) {	// eaSlot contains address of the pgBaseKnownReference
		// See if this reference matches what we're looking for
		uint32_t eaThisReference = sysDmaGetUInt32(eaSlot + knownReferencePoolSize4, spuGetTag);

		if (eaThisReference == eaReference) {
			// Make sure the cached value matches what is really there to catch bad memory moves.
#if __ASSERT
			uint32_t CachedValue = sysDmaGetUInt32(eaSlot + knownReferencePoolSize8, spuGetTag);
			uint32_t actualValue = sysDmaGetUInt32(eaReference, spuGetTag);
			Assertf(actualValue == CachedValue,"pgBase_RemoveKnownRef - Actual value %x doesn't match last known %x",actualValue,CachedValue);
			pgDisplayf("SPU RemoveKnownRef %x (%x->%x)",eaSlot,eaReference,actualValue);
#endif
			// Patch this entry out of the reference list
			uint32_t Next = sysDmaGetUInt32(eaSlot, spuGetTag);		// get location of next reference in chain
			sysDmaPutUInt32(Next, eaBase, spuGetTag);

			// NULL out the tracked reference so that PatchAllReferences doesn't get confused
			sysDmaPutUInt32(0, eaSlot + knownReferencePoolSize4, spuGetTag);

			// Free the pool entry
			pgBaseKnownReferencePool::Delete(eaPool, eaSlot);

			TASKLOG_ONLY(taskLog->Log('SRKR',eaBaseAddr,eaReference,0));

			// And we're done.
			return;
		}

		// Visit next item in list
		eaBase = eaSlot;
	}
	TASKLOG_ONLY(taskLog->Log('srkr',eaBaseAddr,eaReference,0));
	Quitf("HALT: pgBase_RemoveKnownRef - Unknown reference %x not in ref chain for %x?",eaReference,eaBaseAddr);
}

void pgBase::AddKnownRef(uint32_t eaPool,uint32_t eaBase,uint32_t eaReference
#if TRACK_REFERENCE_BACKTRACE
						 ,u16 backtrace
#endif
						 )
{
	Assert(eaBase);
	Assert(eaReference);
	TASKLOG_ONLY(sysTaskLog *taskLog = PPU_SYMBOL(s_TaskLog));

	uint32_t eaBacktrace;
	uint32_t knownReferencePoolSize;
	uint32_t eaNewRef = pgBaseKnownReferencePool::New(eaPool,eaBacktrace,knownReferencePoolSize);

	// Retrieve the "m_FirstSlot" entry from the pgBase object being updated
	uint32_t eaFirstSlot = sysDmaGetUInt32(eaBase + OffsetOf(pgBase, m_FirstNode), spuGetTag);

	// Fill in the pgBaseKnownReference entry.
	uint32_t Next = eaFirstSlot;
	sysDmaPutUInt32(Next,eaNewRef,spuGetTag);
	sysDmaPutUInt32(eaReference, eaNewRef + knownReferencePoolSize * 4,spuGetTag);
	uint32_t CachedValue = sysDmaGetUInt32(eaReference, spuGetTag);
	sysDmaPutUInt32(CachedValue, eaNewRef + knownReferencePoolSize * 8,spuGetTag);
	pgDisplayf("SPU AddKnownRef %x (%x->%x)",eaNewRef,eaReference,CachedValue);
	TASKLOG_ONLY(taskLog->Log('SAKR',eaBase,eaReference,0));
#if TRACK_REFERENCE_BACKTRACE
	sysDmaPutUInt16(backtrace, eaBacktrace, spuGetTag);
#endif

	// Update m_FirstSlot.
	sysDmaPutUInt32(eaNewRef, eaBase + OffsetOf(pgBase,m_FirstNode), spuGetTag);

	// TODO: Ought to make sure we skipped the hidden resource map.  That would only happen if a texture
	// was somehow a toplevel resource.
}

#endif
