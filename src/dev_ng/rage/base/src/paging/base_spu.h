
// paging/base_spu.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PAGING_BASE_SPU_H
#define PAGING_BASE_SPU_H

#include <stddef.h>

#include "data/struct.h"

namespace rage {

class pgBase;

struct pgBaseNode {
	pgBaseNode *Next;
};

#define TRACK_REFERENCE_BACKTRACE	(__ASSERT)

typedef void **pgBaseKnownReference_Reference;

#if RSG_PC || RSG_DURANGO || RSG_ORBIS
typedef u32 pgBaseKnownReference_Link;
#else
typedef u16 pgBaseKnownReference_Link;
#endif

#if TRACK_REFERENCE_BACKTRACE
// 256 unique backtraces probably isn't enough.
typedef u16 pgBaseKnownReference_Backtrace;
#endif

struct pgBasePageMap: public pgBaseNode {
	u8 VirtualCount, PhysicalCount, RootVirtualChunk;
	u8 Subtype;	// Nonzero if this is really metadata (aliases part of pgBaseMetaData::Type)
	u32 Pad;		// This is now just a pad word that can be eliminated if we feel like breaking all resources.
	size_t PageInfo[0];
#if __DECLARESTRUCT
	void DeclareStruct(class datTypeStruct &s);
#endif
};


// Subclass this and register a pgBaseMetaDataType handler below to support it.
struct pgBaseMetaData: public pgBaseNode {
	pgBaseMetaData(datResource&) { }
	pgBaseMetaData(u32 type) : Type(type) { Next = 0; }
	u32 Type;		
	datPadding64(4,Pad)
#if __DECLARESTRUCT
	// This is intentionally not virtual, but it uses pgBaseMetaDataType to do runtime virtual support.
	void DeclareStruct(class datTypeStruct &s);
#endif
};

// Each new data data should subclass this class with a unique Type passed to the parent ctor
struct pgBaseMetaDataType {
	pgBaseMetaDataType(u32 type);
	virtual ~pgBaseMetaDataType();

	pgBaseMetaDataType *Next;
	u32 Type;
	virtual void Place(pgBaseMetaData*,datResource&) = 0;
#if __DECLARESTRUCT
	virtual void DeclareStruct(pgBaseMetaData *,datTypeStruct&) = 0;
#endif

	static pgBaseMetaDataType *First;
	static pgBaseMetaDataType *Lookup(u32 type);
};

} // namespace rage

#endif
