// 
// paginging/streaming_channel.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PAGING_CHANNEL_H 
#define PAGING_CHANNEL_H 

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(paging)

#define pagingAssert(cond)						RAGE_ASSERT(paging,cond)
#define pagingAssertf(cond,fmt,...)				RAGE_ASSERTF(paging,cond,fmt,##__VA_ARGS__)
#define pagingFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(paging,cond,fmt,##__VA_ARGS__)
#define pagingVerify(cond)						RAGE_VERIFY(paging,cond)
#define pagingVerifyf(cond,fmt,...)				RAGE_VERIFYF(paging,cond,fmt,##__VA_ARGS__)
#define pagingErrorf(fmt,...)					RAGE_ERRORF(paging,fmt,##__VA_ARGS__)
#define pagingWarningf(fmt,...)					RAGE_WARNINGF(paging,fmt,##__VA_ARGS__)
#define pagingDisplayf(fmt,...)					RAGE_DISPLAYF(paging,fmt,##__VA_ARGS__)
#define pagingDebugf1(fmt,...)					RAGE_DEBUGF1(paging,fmt,##__VA_ARGS__)
#define pagingDebugf2(fmt,...)					RAGE_DEBUGF2(paging,fmt,##__VA_ARGS__)
#define pagingDebugf3(fmt,...)					RAGE_DEBUGF3(paging,fmt,##__VA_ARGS__)
#define pagingLogf(severity,fmt,...)			RAGE_LOGF(paging,severity,fmt,##__VA_ARGS__)


RAGE_DECLARE_SUBCHANNEL(paging, asset)

#define pagingAssetAssert(cond)						RAGE_ASSERT(paging_asset,cond)
#define pagingAssetAssertf(cond,fmt,...)			RAGE_ASSERTF(paging_asset,cond,fmt,##__VA_ARGS__)
#define pagingAssetFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(paging_asset,cond,fmt,##__VA_ARGS__)
#define pagingAssetVerify(cond)						RAGE_VERIFY(paging_asset,cond)
#define pagingAssetVerifyf(cond,fmt,...)			RAGE_VERIFYF(paging_asset,cond,fmt,##__VA_ARGS__)
#define pagingAssetErrorf(fmt,...)					RAGE_ERRORF(paging_asset,fmt,##__VA_ARGS__)
#define pagingAssetWarningf(fmt,...)				RAGE_WARNINGF(paging_asset,fmt,##__VA_ARGS__)
#define pagingAssetDisplayf(fmt,...)				RAGE_DISPLAYF(paging_asset,fmt,##__VA_ARGS__)
#define pagingAssetDebugf1(fmt,...)					RAGE_DEBUGF1(paging_asset,fmt,##__VA_ARGS__)
#define pagingAssetDebugf2(fmt,...)					RAGE_DEBUGF2(paging_asset,fmt,##__VA_ARGS__)
#define pagingAssetDebugf3(fmt,...)					RAGE_DEBUGF3(paging_asset,fmt,##__VA_ARGS__)
#define pagingAssetLogf(severity,fmt,...)			RAGE_LOGF(paging_asset,severity,fmt,##__VA_ARGS__)

#define PAGING_OPTIMISATIONS_OFF	0

#if PAGING_OPTIMISATIONS_OFF
#define PAGING_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else
#define PAGING_OPTIMISATIONS()
#endif	//PAGING_OPTIMISATIONS_OFF

#endif // PAGING_CHANNEL_H 
