//
// paging/rscbuilder.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PAGING_RSCBUILDER_H
#define PAGING_RSCBUILDER_H

#include "streamer.h"
#include "base.h"

#include "data/resource.h"
#include "data/resourceheader.h"
#include "data/struct.h"
#include "system/buddyallocator_config.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "grcore/config.h"

#include <string.h>	// for memmove

#define ONE_PASS_BUILD		1

#if FREE_PHYSICAL_RESOURCES
	#define CLEANUP(x) Cleanup(x)
#else
	#define CLEANUP(x)
#endif

#if !__SPU

namespace rage {
	
class pgBase;

struct pgStreamerRequest;

struct pgStreamerInfo {
    u32 FileHandle;
	void *ReadHandle;
    sysIpcSema Sema;
	datResourceInfo Info;
};

/*
	The pgRscBuilder class manages creation of resource heaps, which are raw
	binary dumps of program data that can be reloaded and relocated for later use.
	Most of these functions are not called directly, rather being abstracted
	behind pgPaging functions.
*/
class pgRscBuilder {
public:
	static const int LAST_PASS = !ONE_PASS_BUILD;		// for two-pass resourcing.  Set to zero for single-pass resourcing.

	/*	PURPOSE
			Returns true if any BeginBuild is in progress */
#if __RESOURCECOMPILER
	static bool IsBuilding();

	/*	PURPOSE
		Returns true if any BeginBuild first pass is in progress. */
	static bool IsBuildingFirstPass();

	/*	PURPOSE
			Returns true if any BeginBuild second pass is in progress. */
	static bool IsBuildingLastPass();
#else
	static bool IsBuilding() { return false; }
#endif

	/*  PURPOSE
			Compute the size of a node in the resource buddy heap it would take to
			store a certain number of bytes.
		PARAMS
			size - The amount of bytes to calculate the node size for
			physicalHeap - Whether this allocation is for the virtual or physical heap
		RETURNS
			The size of the allocation in the buddy heap that would be done.
			This will be at least the minimum size of a buddy heap allocation, and
			typically a power of two (except in PS3's physical heap).
	*/
	static size_t ComputeLeafSize(size_t size, bool physicalHeap);

	static bool Exists(const char* filename, const char* ext);

	static char* ConstructName(char *dest,int destSize,const char *path,const char *ext,bool forSave = false);

	/* PURPOSE
		Provide a name to be used during the build process for reporting purposes. */
	static void SetBuildName(const char* name);

	/* PURPOSE
		Return a name for the resource being constructed. */
	static const char* GetBuildName();


	/*	PURPOSE
			Set virtual and physical leaf and page sizes for builds. These need 
			to match the sizes used by the game. During resource construction 
			dynamic values are used. Game runtime uses constant values.  Will
			configure for the current target platform (g_sysPlatform)
		*/
#if  __RESOURCECOMPILER
	static void ConfigureBuild();

	/*	PURPOSE
			Begins a resorce build operation; a build must be in progress for
			any BeginCaptures to work.  Builds will nest if necessary, which
			is used for model caching while building page files.  When a build
			is in progress, all operator new calls are redirected to the
			internal grow-only heap.  Any operator delete calls will display
			a warning message because the memory will not be reclaimed.  If you
			need to allocate a temporary object that you want to reclaim and
			not be stored on the resource heap, call datMemoryStartUseTemporary
			to restore the heap and datMemoryEndUseTemporary to pop back to the
			resource heap; these calls will nest properly as well.
		PARAMS
			secondPass - True if this is the second build pass.  On a second pass we will
				automatically fill in appropriate chunk sizes if they were specified as
				zero.  Code may choose to detect which pass is in progress; the first pass
				is only used for determining memory allocation sizes, so you can save time
				by skipping swizzling, etc.
			heapSize - Maximum size of the resource object heap
			placement - Forced address to construct heap at (defaults to allocating
				ourselves and freeing it in EndBuild)
			virtualChunkSize - Maximum size of any single virtual allocation; if nonzero, we will
				introduce fillable gaps as necessary to make sure that allocations
				never span a multiple of this boundary.
			physicalChunkSize - Maximum size of any single physical allocation; if nonzero, we will
				introduce fillable gaps as necessary to make sure that allocations
				never span a multiple of this boundary.
			nodeCount - Maximum number of allocations to support.  If zero (the
				default) we assume one node per 256 bytes in the heap.  Some
				code uses much smaller allocations and will need to be adjusted.
		NOTES
			At least one BeginBuild must be in progress for a BeginCapture to
			work; streaming is implemented by marking off a section of the resource
			heap during the capture and copying it into a page buffer that is
			then eventually flushed to disc.
		SEE ALSO
			BeginCapture, SaveBuild, EndBuild, datMemoryStartUseTemporary,
			datMemoryEndUseTemporary
		*/
	static void BeginBuild(bool secondPass,int heapSize,size_t virtualChunkSize = 0,size_t physicalChunkSize = 0,int nodeCount = 0);

	/*	PURPOSE
			Saves the current resource heap out to disc; its original address
			is written out so that when the object is read back in it can be
			properly relocated.  You also supply an arbitrary version number that
			is checked at load time so that you can easily invalidate old data
			by modifying your version number.
		PARAMS
			heapName - Name of the file to create
			ext - Extension; first character must be '#', replaced by platform id char
			version - Arbitrary version number to associate with this data.
			typeInfo - For cross-platform builds only; describes the object being written
				so that we can do endian-swap, etc, as necessary before writing it out.
			size - Size of the file that was created (optional).
			encrypted - True if resource heap should be encrypted
		RETURNS
			True if file could be saved, else false.
		NOTES
			You must call SaveBuild *before* EndBuild.
			*/
	static bool SaveBuild(const char *heapName,const char *ext,int version, size_t* size = NULL,bool encypted = false);

	/*	PURPOSE
			Ends the current resource build, restoring normal heap operation
			if this was the last build on the build stack. 
		PARAMS
			toplevelPointer - The first object you allocated after BeginBuild.
				This is used to guarantee that there were no unexpected gaps.
			*/
	static void EndBuild(pgBase *toplevelPointer);

	/*	PURPOSE
			Call this function on your last pass prior to calling any DeclareStruct functions. */
	static void ReadyBuild(pgBase *toplevelPointer);

	/*  PURPOSE
	Specify a size at which a physical allocation will be turned into a virtual allocation.
	NOTE: Doing this on the PS3 is not a good idea.

	PARAMS
	threshold - Any physical allocation with a size equal or smaller to this threshold will
	be converted to a virtual allocation.
	*/
#endif	// __RESOURCECOMPILER

	/*	PURPOSE
			Loads a build saved by SaveBuild back into memory, returning the
			address of the heap and the original address the heap was built
			at, along with its size.
		PARAMS
			name - Name of the resource file to load
			ext - Extension; first character must be '#', replaced by platform id char
			version - Version number to expect; the build will not load if
				this number doesn't match.
			outMap - Receives mapping information (for multiple resource chunks, and split resources)
			outHdr - Receives the original resource header
		RETURNS
			Address of the object if file was found and the version matched.
			This is identical to map.VirtualPages[0].
		NOTES
			Higher-level code will typically do something like this to make
			the resulting object usable:
			<CODE>
				datResourceMap map;
				datResourceHeader hdr;
				T* t = (T*) LoadBuild(name,version,map,hdr);
				datResource rsc(map, hdr, debugName);
				t->Place(t,rsc);
			</CODE> */
	static void* LoadBuild(const char *name,const char *ext,int version,datResourceMap &map,datResourceInfo &outHdr);

	/*	PURPOSE
			Begins an asynchronous streaming operation of a resource built
			by SaveBuild.  Note that there's no version checking here since we're
			generally not in a position to rebuild it anyway.  Not ideal.
		PARAMS
			filename - Name of the resource file to load
			ext - Extension; first character must be '#', replaced by platform id char
			version - Version number to expect in the resource header
			outInfo - Structure that is filled out if the call is successful; this
				object must stay in scope until the asynchronous operation is
				completed.  
			map - On input, if valid, that existing memory
				is used to satisfy the streaming request.
		RETURNS
			The address of the block of memory allocated for the load.
		NOTES
			Multiple BeginStream operations can be in progress as long as
			unique info structures are passed in each time; they will complete
			in the order initiated.  The maximum number is limited by the
			input FIFO size of datStreamer::Read.
		SEE ALSO
			EndStream */
	static void* BeginStream(const char *filename,const char *ext,int version,pgStreamerInfo &outInfo,datResourceMap &map);

	/*	PURPOSE
			Opens a streamer handle associated with a resource, fills out the
			pgStreamerInfo structure, and returns.
		PARAMS
			filename - Name of the resource file to load, as per BeginStream
			ext - Extension; first character must be '#', replaced by platform id char
			version - Version number to expect in the resource header
			outInfo - Structure that is filled out if the call is successful; this
				object must stay in scope until the asynchronous operation is
				completed.
		RETURNS
			True on success, false if unable to find the file
		NOTES
			BeginStream is implemented by calling OpenStream and then ContinueStream.
			This version of OpenStream is implemented in terms of its overloaded version.
	*/
	static bool OpenStream(const char *filename,const char *ext,int version,pgStreamerInfo &outInfo);

	/*	PURPOSE
			Opens a streamer handle associated with a resource, fills out the
			pgStreamerInfo structure, and returns.
		PARAMS
			outInfo - Structure that is filled out if the call is successful; this
				object must stay in scope until the asynchronous operation is
				completed.
			handle - Streaming handle
		RETURNS
			True on success, false if handle is invalid
	*/
	static bool OpenStream(pgStreamerInfo &outInfo,u32 handle);

	/*	PURPOSE
			Schedule a stream operation previously prepared by OpenStream.
		PARAMS
			inoutInfo - pgStreamerInfo filled by a successful call to OpenStream
			map - If valid, this memory is used.  Otherwise, it is validated first
				(ie suitable memory is allocated)
			callback - callback function to invoke when operation is completed.  If not
				specified, we create a semaphore internally and block on that.
			userData - userdata for the callback
			rscVirtualHeapIndex - heap index that should be used for resource virtual memory if no streamable is provided
		RETURNS
			Pointer to memory buffer that will contain data on success, false on failure.
			The memory and semaphore are not left allocated on failure.  If you pass
			in a custom callback, you should not use EndStream.
	*/
	static void *ContinueStream(pgStreamerInfo &inoutInfo,datResourceMap &map,pgStreamerCallback  = NULL,void *userData = NULL,u32 flags = 0,float prio = 0.0f);


	/*	PURPOSE
			Generate a map file based on a resource info structure.
		PARAMS
			info - Resource info structure encoding memory sizes
			map - Resource map which is populated with the correct sizes for each chunk.
	*/
	static void GenerateMap(datResourceInfo info, datResourceMap &map);


	/*	PURPOSE
			Allocate memory for a build, returning its address
		PARAMS
			size - Amount of memory to allocate	
		RETURNS
			NULL on failure */
	static void* AllocateMemory(size_t size,bool isPhysical);

	/*	PURPOSE
			Copy memory correctly depending on whether it is virtual or physical.
			Used by defragmentation.  Would have been called CopyMemory but that's a macro
			under Win32, sigh.
		PARAMS
			dest - Destination for copy
			src - Source for copy
			bytes - Number of bytes to copy
			isPhysical - True if memory is physical
		NOTES
			Results are UNDEFINED if memory range overlaps. */
	static void DuplicateMemory(void *dest,const void *src,size_t bytes,bool isPhysical);

	/*	PURPOSE
			Allocate the necessary memory to create a valid set of destination 
			memory areas for the map, returning false on failure.
		PARAMS
			map - Map object to fill out
		RETURNS
			True on success, false on failure.
	*/
	static bool AllocateMap(datResourceMap &map);

	/*	PURPOSE
			Validate that a map object is sufficiently larger to contain the
			supplied resource.  If the map is constructed but not initialized,
			it will allocate the necessary memory, returning false on failure.
			If the map is initialized, it will return false if the amount of
			memory is not sufficient.
	PARAMS
			name - Debug name for error messages
			map - Map object to fill out
			hdr - Resource header to use to determine size requirements
	RETURNS
			True on success, false on failure.
	*/
	static bool ValidateMap(const char *name,datResourceMap &map,datResourceInfo hdr);

	/*	PURPOSE
			Free memory allocated by BeginBuild or ContinueStream.  Normal only necessary during
			error recovery becuase the memory is freed by normal destructors once it has been created.
		PARAMS
			map - Structure to fix
	*/
	static void FreeMemory(const datResourceMap &map);

	/*	PURPOSE
			Frees memory allocated via AllocateMemory */
	static void FreeMemory(void *ptr,bool isPhysical);

	/*	PURPOSE
			Polls an asynchronous streaming operation to see if it's complete.
		PARAMS
			inoutInfo - Structure that was filled out by the originating BeginStream call.
			block - True to block until completion, false to poll and return.
			close - True to close the streaming handle when done
		RETURNS
			True if the operation completed.  Always true if block is true.
		NOTES
			You must have a completed EndStream for every BeginStream or else
			the system will leak datStreamer handles and eventually run out.
			There is currently no way to cancel an operation once it begins. 
			Higher-level code must run Place to make the object usable.	*/
	static bool EndStream(pgStreamerInfo &inoutInfo,bool block,bool close);

	/*	PURPOSE
			Cancels a pending streaming operation, freeing all associated
			resources as if EndStream had completed.  You are responsible for
			freeing the memory yourself after calling pgStreamer::Drain!
		PARAMS
			inoutInfo - Structure that was filled out by the originating BeginStream call.
			close - True to close the streaming handle when done
	*/
	static void CancelStream(pgStreamerInfo &inoutInfo,bool close);

	/*	PURPOSE
			Frees the physical portion of the heap under __WIN32PC once the resource
			constructor has finished, since D3D copies everything we need.  On all
			builds, it resets the map object to be empty since once construction has
			completed, the memory manager owns the memory and can reclaim it normally.
	*/
	static void Cleanup(datResourceMap & map, bool freeVirtual = false);

	/*	PURPOSE
			Specify a Callback function used by VerifyMap, to select an allocator pair offset based on the paged files name
	
		PARAMS	
			callback - is a pointer to a function that takes the name of the paged files and returns whether to use the fixed resource allocator instead of the
			normal resource allocators
		NOTE
			you need to set up a fixed allocator pair in before including main.h (see main.h for more info)
	*/
	static void SetFixedAllocatorCheckCallback(bool (*callback)(const char *, datResourceInfo) );

	//	PURPOSE
	//		Appends a version number to the resource extension. This can simplify versioning of resources.
	//
	static void AddVersionNumberToExtension( bool val = true );

	static void IsPgBase(pgBase *) {};

	// PURPOSE:	Load a resource heap from disc and construct it
	// PARAMS	t - Reference to pointer that will contain the toplevel heap address
	//			name - Name of the resource heap to load
	//			ext - extension to add.  The extension should be exactly three characters long,
	//				and the first character is replaced with a platform-specific value
	//				(w for __WIN32PC, x for Xenon, p for PSP, c (cell) for PSN).
	//			version - Version number to expect
	// RETURNS:	True if heap was loaded and placed, false if file not found or version mismatch
	template <class T> static void Load(T* &t,const char *name,const char *ext,int version) {
		datResourceMap map;
		datResourceInfo hdr;
		t = (T*) LoadBuild(name,ext,version,map,hdr);
//		IsPgBase(t);
		datResource rsc(map,name);
		pgBase *b = t;			// catch things that aren't subclasses off of pgBase
		if (b) 
		{
			t->Place(t,rsc);
			t->PostPlace();
		}

		CLEANUP(map);
	}

	template <class T> static void LoadDefragmentable(T* &t,const char *name,const char *ext,int version) {
		datResourceMap map;
		datResourceInfo hdr;
		t = (T*) LoadBuild(name,ext,version,map,hdr);
		datResource rsc(map,name);
		pgBase *b = t;			// catch things that aren't subclasses off of pgBase
		if (b) {
			t->Place(t,rsc);
			t->PostPlace();
			t->MakeDefragmentable(map);	// <- T should be derived from pgBase.
		}
		CLEANUP(map);
	}

	template <class T, typename U> static void Load(T* &t,const char *name,const char *ext,int version,U &u) {
		datResourceMap map;
		datResourceInfo hdr;
		t = (T*) LoadBuild(name,ext,version,map,hdr);
//		IsPgBase(t);
		datResource rsc(map,name);
		pgBase *b = t;			// catch things that aren't subclasses off of pgBase
		if (b) 
		{
			t->Place(t,rsc,u);
			t->PostPlace();
		}
		CLEANUP(map);
	}

	template <class T, typename U, typename V> static void Load(T* &t,const char *name,const char *ext,int version,U &u,V &v) {
		datResourceMap map;
		datResourceInfo hdr;
		t = (T*) LoadBuild(name,ext,version,map,hdr);
//		IsPgBase(t);
		datResource rsc(map,name);
		pgBase *b = t;			// catch things that aren't subclasses off of pgBase
		if (b) 
		{
			t->Place(t,rsc,u,v);
			t->PostPlace();
		}
		CLEANUP(map);
	}

	template <class T, typename U, typename V, typename W> static void Load(T* &t,const char *name,const char *ext,int version,U &u,V &v,W &w) {
		datResourceMap map;
		datResourceInfo hdr;
		t = (T*) LoadBuild(name,ext,version,map,hdr);
//		IsPgBase(t);
		datResource rsc(map,name);
		pgBase *b = t;			// catch things that aren't subclasses off of pgBase
		if (b) 
		{
			t->Place(t,rsc,u,v,w);
			t->PostPlace();
		}
		CLEANUP(map);
	}

	static void Delete(pgBase* t) {
		delete t;
	}

	template <class T> static int Release(T* t) {
		int count = 0;
		pgBase *b = t;			// catch things that aren't subclasses off of pgBase
		if (b)
			count = t->Release();
		return count;
	}

	template <class T> static bool EndStream(T* &t,pgStreamerInfo &info,datResourceMap &map,bool block,bool close,const char *debugName) {
		if (EndStream(info,block,close)) {
			t = (T*) map.GetVirtualBase();
			datResource rsc(map,debugName);
			t->Place(t,rsc);
			t->PostPlace();
			CLEANUP(map);
			return true;
		}
		else
			return false;
	}

	template <class T> static bool EndStreamDefragmentable(T* &t,pgStreamerInfo &info,datResourceMap &map,bool block,bool close,const char *debugName) {
		if (EndStream(info,block,close)) {
			t = (T*) map.GetVirtualBase();
			datResource rsc(map,debugName);
			t->Place(t,rsc);
			t->PostPlace();
			t->MakeDefragmentable(map);
			CLEANUP(map);
			return true;
		}
		else
			return false;
	}

	// PURPOSE : Gets the currently allocated memory size for a resource
	static void GetSize( size_t& vSize, size_t & pSize );

	template <class T> static void PlaceStream(T* &t, const datResourceInfo&, datResourceMap &map, const char *debugName, bool isDefrag = false, bool bClearMap = true) {
		t = (T*) map.GetVirtualBase();
		datResource rsc(map,debugName,isDefrag);
		t->Place(t,rsc);
		t->PostPlace();
		if (bClearMap)
		{
			CLEANUP(map);
			map.Reset();
		}
	}

	template <class T, typename U> static void PlaceStream(T* &t, const datResourceInfo&, datResourceMap &map, const char *debugName,U &u, bool isDefrag = false) {
		t = (T*) map.GetVirtualBase();
		datResource rsc(map,debugName,isDefrag);
		t->Place(t,rsc,u);
		t->PostPlace();
		CLEANUP(map);
		map.Reset();
	}

	template <class T, typename U, typename V> static void PlaceStream(T* &t, const datResourceInfo&, datResourceMap &map, const char *debugName,U &u,V &v, bool isDefrag = false) {
		t = (T*) map.GetVirtualBase();
		datResource rsc(map,debugName,isDefrag);
		t->Place(t,rsc,u,v);
		t->PostPlace();
		CLEANUP(map);
		map.Reset();
	}

	template <class T, typename U, typename V, typename W> static void PlaceStream(T* &t, const datResourceInfo&, datResourceMap &map, const char *debugName, U &u,V &v, W &w, bool isDefrag = false) {
		t = (T*) map.GetVirtualBase();
		datResource rsc(map,debugName,isDefrag);
		t->Place(t,rsc,u,v,w);
		t->PostPlace();
		CLEANUP(map);
		map.Reset();
	}

#if !__FINAL
	// PURPOSE: Returns true if the last failed allocation made by AllocateMemory() was physical,
	// false if it was virtual.
	static bool WasLastFailedAllocationPhysical();

	// PURPOSE: Returns the size of the last chunk that AllocateMemory() unsuccessfully tried to
	// allocate.
	static size_t GetLastFailedAllocationSize();
#endif // !__FINAL
};

// Largest allocations seen during most recent resource heap build.
extern size_t g_LargestVirtualAllocation, g_LargestPhysicalAllocation;

}	// namespace rage

#endif		// !__SPU

#endif
