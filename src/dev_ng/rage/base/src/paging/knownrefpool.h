// 
// paging/knownrefpool_config.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PAGING_KNOWNREFPOOL_H
#define PAGING_KNOWNREFPOOL_H

#include "paging/knownrefpool_config.h"
#include "paging/base_spu.h"

#include <string.h>

#if ENABLE_KNOWN_REFERENCES

namespace rage {

// NOTE: This is only used by RAGE samples.  To get the one used by project-level code, see PGKNOWNREFPOOLSIZE in Core\main.cpp.
const unsigned DefaultKnownReferencePoolSize = 0x4000;
CompileTimeAssert((DefaultKnownReferencePoolSize & 31) == 0);		// must be a multiple of 32 for bitfield

class pgBaseKnownReferencePool
{
public:

#if !__SPU
	pgBaseKnownReferencePool()
	{
		m_FirstFree = m_Pool = 0;
		m_Used = m_PeakUsed = 0;
		m_Size = 0;
	}

	void Init(int sz = -1)
	{
		m_Size = sz >= 0 ? sz : DefaultKnownReferencePoolSize;

		Assert((m_Size & 31) == 0);		// must be a multiple of 32 for bitfield

#if TRACK_REFERENCE_BACKTRACE
		const u32 poolSizeInBytes = m_Size * (sizeof(pgBaseNode) + sizeof(pgBaseKnownReference_Reference) + sizeof(pgBaseKnownReference_Link)*2 + sizeof(pgBaseKnownReference_Backtrace));
#else
		const u32 poolSizeInBytes = m_Size * (sizeof(pgBaseNode) + sizeof(pgBaseKnownReference_Reference) + sizeof(pgBaseKnownReference_Link)*2);
#endif

		m_Pool = (pgBaseNode*) rage_aligned_new(128) char[poolSizeInBytes];
		memset(m_Pool, 0, poolSizeInBytes);
		for (size_t i=0; i<m_Size-1; i++)
			m_Pool[i].Next = m_Pool + i + 1;
		m_FirstFree = m_Pool;
	}

	void Shutdown()
	{
		delete[] m_Pool;
		m_Pool = NULL;
		m_FirstFree = NULL;
	}

	bool IsFull() const { return !m_FirstFree; }
	bool IsInitialized() const { return m_Pool != NULL; }

	ptrdiff_t New()
	{
		pgBaseNode *result = m_FirstFree;
		if (!result)
			Quitf("KnownReference pool exhausted, raise KnownReferencePoolSize in knownrefpool.h to something larger than %u (or delete your data\\*_cache_?.dat file, but send your TTY log to Klaas first)", GetSize());
		m_FirstFree = m_FirstFree->Next;
		// Displayf("New returns %x",result);
		if (++m_Used > m_PeakUsed) 
			m_PeakUsed = m_Used;
		ptrdiff_t bit = result - m_Pool;
		return bit;
	}
	void Delete(pgBaseNode *ptr)
	{
		--m_Used;
		// Displayf("Deleting %p",ptr);
		Assert(ptr >= m_Pool && ptr - m_Pool < (int)GetSize());
		ptr->Next = m_FirstFree;
		m_FirstFree = ptr;
	}
	pgBaseNode* GetPool_Nodes()
	{
		Assert(m_Pool);
		return m_Pool;
	}
	pgBaseKnownReference_Reference* GetPool_References()
	{
		return (pgBaseKnownReference_Reference*)(GetPool_Nodes() + m_Size);
	}
	pgBaseKnownReference_Link* GetPool_Links_ByAddress()
	{
		return (pgBaseKnownReference_Link*)(GetPool_References() + m_Size);
	}
	pgBaseKnownReference_Link* GetPool_Links_ByContents()
	{
		return (pgBaseKnownReference_Link*)(GetPool_Links_ByAddress() + m_Size);
	}
#if TRACK_REFERENCE_BACKTRACE
	pgBaseKnownReference_Backtrace* GetPool_Backtraces()
	{
		return (pgBaseKnownReference_Backtrace*)(GetPool_Links_ByContents() + m_Size);
	}
#endif

	bool Contains(pgBaseNode *ptr)
	{
		return m_Pool && ptr >= m_Pool && ptr < m_Pool + m_Size;
	}
	int GetUsed() const { return m_Used; }

#else	// __SPU
	static uint32_t New(uint32_t eaPoolStruct,uint32_t &eaBacktrace, uint32_t &sizeOut)
	{
		pgBaseKnownReferencePool _this;
		sysDmaGetAndWait(&_this,eaPoolStruct,sizeof(_this),spuGetTag);
		// Allocate a slot by reading the first free pointer.
		pgBaseNode *newSlot = _this.m_FirstFree;
		if (!newSlot)
			Quitf("KnownReference pool exhausted, raise KnownReferencePoolSize in knownrefpool.h to something larger than %u", _this.m_Size);
		Assertf(newSlot >= _this.m_Pool && newSlot < _this.m_Pool + _this.m_Size,"Invalid slot %p",newSlot);

		// Read the "next" pointer out of the first free slot, and write that into first free pointer again
		pgBaseNode *eaNext = (pgBaseNode*)sysDmaGetUInt32((uint32_t)newSlot, spuGetTag);
		_this.m_FirstFree = eaNext;

		if (++_this.m_Used > _this.m_PeakUsed)
			_this.m_PeakUsed = _this.m_Used;

#if TRACK_REFERENCE_BACKTRACE
		eaBacktrace = (uint32_t)_this.m_Pool + 12 * _this.m_Size + (newSlot - _this.m_Pool) * sizeof(pgBaseKnownReference_Backtrace);
#endif
		sizeOut = _this.m_Size;

		sysDmaPutAndWait(&_this,eaPoolStruct,sizeof(_this),spuGetTag);

		// Displayf("New returns %x",eaSlot);
		return (uint32_t) newSlot;
	}
	static void Delete(uint32_t eaPoolStruct,uint32_t eaSlot)
	{
		pgBaseKnownReferencePool _this;
		sysDmaGetAndWait(&_this,eaPoolStruct,sizeof(_this),spuGetTag);

		--_this.m_Used;
		Assertf((pgBaseNode*)eaSlot >= _this.m_Pool && (pgBaseNode*)eaSlot < _this.m_Pool + _this.m_Size,"Invalid slot %x",eaSlot);
		// Displayf("Deleting %x",eaSlot);

		// This slot now points to the previous first entry in the pool
		sysDmaPutUInt32((uint32_t)_this.m_FirstFree, eaSlot, spuGetTag);

		_this.m_FirstFree = (pgBaseNode*)eaSlot;
		sysDmaPutAndWait(&_this,eaPoolStruct,sizeof(_this),spuGetTag);
	}
#endif
	unsigned int GetSize() const
	{
		return m_Size;
	}

private:
	// These allocations are large enough that we know they'll always be sixteen-byte-aligned.
	// The sub-pools are in the following relative order:
	// pgBaseNode, pgBaseKnownReference_Reference, pgBaseKnownReference_CachedValue, and optionally pgBaseKnownReference_Backtrace
	pgBaseNode *m_Pool, *m_FirstFree;
	int m_Used, m_PeakUsed;
	u32 m_Size;
} ;

}		// namespace rage

#endif	// ENABLE_KNOWN_REFERENCES

#endif	// PAGING_KNOWNREFPOOL_H
