// 
// paging/handle.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PAGING_HANDLE_H
#define PAGING_HANDLE_H

#include "data/struct.h"
#include "paging/base.h"
#include "system/criticalsection.h"
#include "system/interlocked.h"
#include "system/memops.h"
#include "system/ppu_symbol.h"


// Disabled for __TOOL, since things like grcTextureHandle do weird things.
#define PGHANDLE_REF_COUNT              (0 && __DEV && !__TOOL)

#if PGHANDLE_REF_COUNT
#	define PGHANDLE_REF_COUNT_ONLY(...)     __VA_ARGS__
#else
#	define PGHANDLE_REF_COUNT_ONLY(...)
#endif

// Change pgHandleIndex to pgHandle to make debugging (a tiny bit) easier.
#define PGHANDLEINDEX_IS_A_PGHANDLE     (1 && PGHANDLE_REF_COUNT)


// Cannot use namespaces for ppu symbols exported to the spu, so create an alias in the global namespace
extern rage::pgBase *g_pgHandleArray[];
extern rage::pgBase *g_pgHandleArray2[];
#if PGHANDLE_REF_COUNT
	extern rage::u16 g_pgHandleRefCountArray[];
#endif

#if __SPU
	DECLARE_PPU_SYMBOL(rage::pgBase*, g_pgHandleArray);
#	if PGHANDLE_REF_COUNT
		DECLARE_PPU_SYMBOL(rage::u16, g_pgHandleRefCountArray);
#	endif
#endif


namespace rage {

#if ENABLE_DEFRAGMENTATION

#if __SPU

template <class _T> struct pgHandleUnionable
{
	_T **ptr;
};

template <class _T> struct pgHandle: public pgHandleUnionable<_T>
{
};

#else

extern u32 pgHandleFirstFree;
extern u32 pgHandleFirstFree2;

struct pgHandleBase {
	static u32 RegisterIndexInternal(pgBase *obj);

	static u32 RegisterIndex(pgBase *obj) {
		if (!obj)
			return 0;
		u32 h = obj->GetHandleIndex();
		if (h)
			return h;
		else
			return RegisterIndexInternal(obj);
	}

	static pgBase** Register(pgBase *obj) {
		u32 h = RegisterIndex(obj);
		return h? (g_pgHandleArray + h) : NULL;
	}

	static void Unregister(pgBase *owner);
};

struct pgHandleBase2 {
	static u32 RegisterIndexInternal(pgBase *obj);

	static u32 RegisterIndex(pgBase *obj) {
		if (!obj)
			return 0;
		u32 h = obj->GetHandleIndex();
		if (h)
			return h;
		else
			return RegisterIndexInternal(obj);
	}

	static pgBase** Register(pgBase *obj) {
		u32 h = RegisterIndex(obj);
		return h? (g_pgHandleArray2 + h) : NULL;
	}

	static void Unregister(pgBase *owner);
};


// pgHandle, but with no constructors or destructor, so that this can be used inside of a union.
// Higher level code needs to manually initialize ptr, and call IncRef and DecRef where appropriate.
// Use with extreme caution only.
template <class _T> struct pgHandleUnionable: public pgHandleBase {
	SYS_MEM_OPS_PTR_LIKE_TYPE(_T);
	// If any of these calls result in an invalid pointer that looks like a small integer (u16), you're attempting
	// to dereference an object that has already been deleted (more to the point, the object's destructor
	// has correctly called pgHandleBase::Unregister on itself).
	_T& operator*() const { return **ptr; }
	_T* operator->() const { return ptr? *ptr : NULL; }
	operator _T*() const { return ptr? *ptr : NULL; }

#if PGHANDLE_REF_COUNT
	void IncRef(_T **p) const {
		if (p) {
			uptr h = (pgBase**)p - g_pgHandleArray;
			// While we could use sysInterlockedIncrement_NoWrapping here, this
			// is debug only code, and it since we are trying to catch ref
			// counting bugs, sysInterlockedIncrement_NoWrapping would then mean
			// any results after the first detected error cannot be trusted.
			ASSERT_ONLY(u16 updated =) sysInterlockedIncrement(g_pgHandleRefCountArray + h);
			Assertf(updated, "pgHandle overflow handle 0x%" SIZETFMT "x", h);
		}
	}
	void DecRef(_T **p) const {
		if (p) {
			uptr h = (pgBase**)p - g_pgHandleArray;
			ASSERT_ONLY(u16 updated =) sysInterlockedDecrement(g_pgHandleRefCountArray + h);
			Assertf((u16)(updated+1), "pgHandle underflow handle 0x%" SIZETFMT "x", h);
		}
	}
#else
	__forceinline void IncRef(_T**) const {}
	__forceinline void DecRef(_T**) const {}
#endif
	__forceinline void IncRef() const { IncRef(ptr); }
	__forceinline void DecRef() const { DecRef(ptr); }

	void operator=(_T* that) {
		_T **const oldPtr = ptr;
		ptr = (_T**)Register(that);
		IncRef();
		DecRef(oldPtr);
	}

	_T **ptr;
};

template <class _T> struct pgHandle: public pgHandleUnionable<_T> {
#if PGHANDLE_REF_COUNT
	pgHandle() { pgHandleUnionable<_T>::ptr=NULL; }
	pgHandle(const pgHandle& that) { pgHandleUnionable<_T>::ptr=that.ptr; pgHandleUnionable<_T>::IncRef(); }
	pgHandle& operator=(const pgHandle& that) { that.IncRef(); pgHandleUnionable<_T>::DecRef(); pgHandleUnionable<_T>::ptr=that.ptr; return *this; }
	~pgHandle() { pgHandleUnionable<_T>::DecRef(); pgHandleUnionable<_T>::ptr=NULL; }
#endif
	void operator=(_T* that) { pgHandleUnionable<_T>::operator=(that); }
};

template <class _T> struct pgHandle2: public pgHandleBase2 {
	SYS_MEM_OPS_PTR_LIKE_TYPE(_T);
	// If any of these calls result in an invalid pointer that looks like a small integer (u16), you're attempting
	// to dereference an object that has already been deleted (more to the point, the object's destructor
	// has correctly called pgHandleBase::Unregister on itself).
	_T& operator*() const { return **ptr; }
	_T* operator->() const { return ptr? *ptr : NULL; }
	operator _T*() const { return ptr? *ptr : NULL; }

	void operator=(_T* that) { ptr = (_T**)Register(that); }
	_T **ptr;
};

// This version is slightly less performant but takes less space.
// The type pgHandleIndexReal should only be used directly when stored in
// serialized data, and cannot allow pgHandleIndex to be changed to a pgHandle
// with PGHANDLEINDEX_IS_A_PGHANDLE.
template <class _T> struct pgHandleIndexReal {
	SYS_MEM_OPS_PTR_LIKE_TYPE(_T);
	_T& operator*() const { return *(_T*)g_pgHandleArray[index]; }
	_T* operator->() const { return (_T*)g_pgHandleArray[index]; }
	operator _T*() const { return (_T*)g_pgHandleArray[index]; }

#if PGHANDLE_REF_COUNT
	void IncRef(u16 i) const {
		if (i) {
			ASSERT_ONLY(u16 updated =) sysInterlockedIncrement(g_pgHandleRefCountArray + i);
			Assertf(updated, "pgIndex overflow handle 0x%04x", i);
		}
	}
	void DecRef(u16 i) const {
		if (i) {
			ASSERT_ONLY(u16 updated =) sysInterlockedDecrement(g_pgHandleRefCountArray + i);
			Assertf((u16)(updated+1), "pgIndex underflow handle 0x%04x", i);
		}
	}
	void IncRef() const { IncRef(index); }
	void DecRef() const { DecRef(index); }
	pgHandleIndexReal(): index(0) {}
	pgHandleIndexReal(const pgHandleIndexReal& that) { index=that.index; IncRef(); }
	pgHandleIndexReal& operator=(const pgHandleIndexReal& that) { that.IncRef(); DecRef(); index=that.index; return *this; }
	~pgHandleIndexReal() { DecRef(); index=0; }
#else
	__forceinline void IncRef(u16=0) const {}
	__forceinline void DecRef(u16=0) const {}
#endif

#if !__SPU
	void operator=(_T*that) { u16 oldIndex=index; Assign(index,pgHandleBase::RegisterIndex(that)); IncRef(); DecRef(oldIndex); }
	void operator=(const _T &that) { that.IncRef(); DecRef(); index = that.index; }
#endif
	u16 index;
};

#if PGHANDLEINDEX_IS_A_PGHANDLE
	template <class _T> struct pgHandleIndex: public pgHandle<_T>
	{
		void operator=(_T* that) { pgHandle<_T>::operator=(that); }
	};
#else
	template <class _T> struct pgHandleIndex: public pgHandleIndexReal<_T>
	{
		void operator=(_T* that) { pgHandleIndexReal<_T>::operator=(that); }
	};
#endif


#if __DECLARESTRUCT
template <class _T> inline void datSwapper(pgHandle<_T> &ref)		{ if (ref.ptr) ref.ptr = (_T**)*ref.ptr; datSwapper((char*&)(ref.ptr)); }
#endif

#endif		// __SPU

#else		// !ENABLE_DEFRAGMENTATION

template <class _T> struct pgHandleUnionable
{
	_T *ptr;
};

template <class _T> struct pgHandle: public pgHandleUnionable<_T>
{
};

#endif

}
// namespace rage


#endif		// PAGING_HANDLE_H
