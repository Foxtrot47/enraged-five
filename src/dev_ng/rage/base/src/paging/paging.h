//
// paging/paging.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PAGING_PAGING_H
#define PAGING_PAGING_H

#error "Use paging/rscbuilder.h instead.  You can mostly just change pgPaging:: to pgRscBuilder::.  You may have to add #ext parameters too."

#endif
