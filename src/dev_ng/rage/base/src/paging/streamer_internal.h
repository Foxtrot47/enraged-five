// 
// paging/streamer_internal.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PAGING_STREAMER_INTERNAL_H 
#define PAGING_STREAMER_INTERNAL_H 

#include "file/handle.h"
#include "system/criticalsection.h"
#include "system/ipc.h"
#include "system/interlocked.h"
#include "system/param.h"
#include "system/new.h"
#include "system/timer.h"
#include "atl/bitset.h"

namespace rage {

class fiDevice;


// PURPOSE: Lock-Free bounded FIFO container
template<typename T, u32 SIZE>
class pgLockFreeQueue
{
	// PURPOSE: size must be a power of 2
	CompileTimeAssert(0 == (SIZE & (SIZE-1)));

public:
	// PURPOSE: Constructor
	pgLockFreeQueue();

	// PURPOSE: Push a new node
	// PURPOSE: true if succeed
	bool Push(T node);

	// PURPOSE: Pop an existing node
	// RETURN: NULL if the stack is empty
	void Pop(T &node);

	// PURPOSE: Return true if ring is empty
	// NOTE: Fast version not 100% reliable
	bool IsEmpty() const;

private:
	static const u32 sm_MaxRingEntriesMask = (SIZE - 1);

	volatile u32 m_Head;
	volatile u32 m_Tail;
	u32 m_Sequence[SIZE];
	T m_Entries[SIZE];
};

////////////////////////////////////////////////////////////////////////////////

template<typename T, u32 SIZE>
pgLockFreeQueue<T, SIZE>::pgLockFreeQueue()
: m_Head(0)
, m_Tail(0)
{
	for(u32 i=0; i<SIZE; ++i)
	{
		m_Sequence[i] = i;
	}
}

////////////////////////////////////////////////////////////////////////////////

template<typename T, u32 SIZE>
void pgLockFreeQueue<T, SIZE>::Pop(T &ret )
{
	u32 tail = m_Tail;
	for(;;)
	{
		u32 seq = m_Sequence[tail&sm_MaxRingEntriesMask];
		int diff = seq - (tail+1);
		if(diff == 0)
		{
			u32 tail2 = sysInterlockedCompareExchange(&m_Tail, tail+1, tail);
			if(tail == tail2)
			{
				break;
			}
			tail = tail2;
		}
		else if(diff < 0)
		{
			Quitf(ERR_STR_FAILURE_5, "The stream queue looks to be corrupted somehow");
		}
		else
		{
			tail = m_Tail;
		}
	}

	ret = m_Entries[tail&sm_MaxRingEntriesMask];
	sysInterlockedExchange(&m_Sequence[tail&sm_MaxRingEntriesMask], tail+sm_MaxRingEntriesMask+1);
}

////////////////////////////////////////////////////////////////////////////////

template<typename T, u32 SIZE>
bool pgLockFreeQueue<T, SIZE>::Push(T node)
{
	u32 head = m_Head;
	for(;;)
	{
		u32 seq = m_Sequence[head&sm_MaxRingEntriesMask];
		int diff = seq - head;
		if(diff == 0)
		{
			u32 head2 = sysInterlockedCompareExchange(&m_Head, head+1, head);
			if(head == head2)
			{		
				break;
			}
			head = head2;
		}
		else if(diff < 0)
		{
			Assertf(false, "pgLockFreeQueue is out of free nodes! Max: %u", SIZE);
			return false;
		}
		else
		{
			head = m_Head;
		}
	}

	sysInterlockedExchange((u32*)&m_Entries[head&sm_MaxRingEntriesMask], (u32)node);
	sysInterlockedExchange(&m_Sequence[head&sm_MaxRingEntriesMask], head+1);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

template<typename T, u32 SIZE>
bool pgLockFreeQueue<T, SIZE>::IsEmpty() const
{
	return m_Head == m_Tail;
}

// Simple thread-safe small allocation manager.
// Even though we never Allocate from more than one thread or Free from more
// that one thread, we can still Allocate and Free at the same time and need
// an internally consistent result.
template <u32 _Size> class pgBitList {

public:
#if !__WIN32PC || __64BIT
	typedef u64 BitMaskType;
	enum { MaxRequestCount = sizeof(BitMaskType)<<3 };
#else // !__WIN32PC || __64BIT
	typedef u32 BitMaskType;
	enum { MaxRequestCount = 32 };
#endif // !__WIN32PC || __64BIT
private:

	volatile BitMaskType Available;
public:
	// Init the free bitlist to all available.
	void Init() {
		Available = (~(BitMaskType)0) >> (MaxRequestCount - _Size);
	}
	bool IsFull() const {
		return !Available;
	}
	u32 GetSize() const { 
		return _Size; 
	}
	// Locate an available bit via atomic operations.
	u32 Allocate() {
		u32 bit;
		BitMaskType test, mask;
		do {
			test = Available;
			if (!test)	// nothing available?
				return GetSize();
			bit = 0, mask = 1;
			// find the bit:
			while (!(test & mask)) {
				++bit;
				mask<<=1;
			}
		// Keep trying until the write succeeds.
		} while (sysInterlockedCompareExchange(&Available,test & ~mask,test) != test);
		return bit;
	}
	void Free(BitMaskType idx) {
		idx = ((BitMaskType) 1ULL) << idx;
		Assertf(!(idx & Available),"Double-free in pgBitList?");
		sysInterlockedOr(&Available,idx);
	}
};

/* PURPOSE:  This type represents a simple 16-bit index and can be used as such (and safely passed by value). In certain builds (currently __BANK),
 * it will add a random value to the upper 16 bits whenever SetNewIndex() is called.
 *
 * Comparisons will consider the upper 16 bits, so two pgGenerationedIndex objects are only identical if the index and the seed value match.
 *
 * GetIndex() can be used to retrieve only the index itself, i.e. the lower 16 bits.
 *
 * In non-BANK builds, this object is identical with a u32 (obviously only the lower 16 bits should be used, but this is not enforced).
 *
 * NOTE that this class is not thread-safe - SetNewIndex will read and write from the shared static variable sm_NextGeneration.
 */
struct pgGenerationedIndex {

#if __BANK
	// PURPOSE: Assign a new index value. This will assign a new random seed value to the upper 16 bits.
	void SetNewIndex(u32 index)			{ FastAssert((index & 0xffff0000) == 0); m_Value = index | sm_NextGeneration; sm_NextGeneration += 0x10000; }

	// PURPOSE: Retrieve the index value that was set via SetNewIndex. The seed value will be stripped from the result.
	u32 GetIndex() const				{ return m_Value & 0xffff; }

#else // __BANK

	void SetNewIndex(u32 index)			{ m_Value = index; }

	u32 GetIndex() const				{ return m_Value; }
#endif // __BANK

	bool operator ==(const pgGenerationedIndex &r) const	{ return m_Value == r.m_Value; }

	bool operator ==(u32 r) const							{ return m_Value == r; }

	pgGenerationedIndex &operator =(u32 value)		{ m_Value = value; return *this; }

	pgGenerationedIndex &operator =(const pgGenerationedIndex &r)		{ m_Value = r.m_Value; return *this; }

	operator int() const									{ return (int) m_Value; }



private:
	u32 m_Value;								// The index value in the lower 16 bits (and the seed value in the upper 16 bits in BANK builds)

#if __BANK
	static u32 sm_NextGeneration;				// Next generation value to use - only the upper 16 bits are used
#endif // __BANK
};

// Simple lock-free queue; assumes only one thread is inserting into the queue
// and one thread is removing from the queue at any point in time; only the
// count variable is thread-safe via interlocked operations.
template <class _Type>
class pgQueue {
public:
	pgQueue() : m_Data(NULL), m_Head(0), m_Tail(0), m_Count(0), m_Size(0) { }

	void Init(unsigned size) {
		if( size )
		{
			m_Data = rage_new _Type[m_Size = size];
			memset( m_Data, 0xff, sizeof( _Type ) * size );
		}
	}

	void Shutdown() {
		delete[] m_Data;
		m_Data = NULL;
		m_Head = m_Tail = m_Count = m_Size = 0;
	}

	bool IsEmpty() const { 
		return m_Count == 0; 
	}

	_Type *BeginEnqueue() {
		SYS_CS_SYNC(m_Cs);
		if (m_Count == m_Size)
		{
			return NULL;
		}
		_Type *ret = m_Data + m_Head;
		//incrementing head here to allow multiple BeginEnqueue
		if( ++m_Head == m_Size )
			m_Head = 0;
		return ret;
	}
	
	void EndEnqueue()
	{
		SYS_CS_SYNC(m_Cs);
		++m_Count;
	}

	// If this returns non-null, you must call RemoveIndex and EndDequeue when done.
	// It's safe to call BeginDequeue multiple times before calling EndDequeue, and to call
	// RemoveIndex in a different order than the order in which elements where popped off
	// via BeginDequeue.
	const _Type *BeginDequeue() {
		SYS_CS_SYNC(m_Cs);
		if( !m_Count )
		{
			return NULL;
		}
		_Type *result = m_Data + m_Tail;
		if( *result == ~0U )
		{
			Quitf(ERR_STR_FAILURE_8, "Calling BeginDequeue when tail is invalid." );
		}
		--m_Count;
		return result;
	}
	void EndDequeue() {
		SYS_CS_SYNC(m_Cs);

		m_Data[m_Tail] = ~0U;
		if (++m_Tail == m_Size)
			m_Tail = 0;
	}

#if __BANK
	// Iterators for debugging use.
	unsigned GetCount() const {
		return m_Count;
	}
	const _Type &operator[](unsigned idx) const {
		return m_Data[(m_Head + m_Size - 1 - idx) % m_Size];
	}
#endif // __BANK

private:
	sysCriticalSectionToken m_Cs;
	_Type *m_Data;				// Array of elements
	unsigned m_Head;			// Head index - source thread will put new elements here. Modified by source thread.
	unsigned m_Tail;			// Tail index - that's the oldest element from the sink thread. Modified by sink thread.
	unsigned m_Count;			// Active elements, i.e. total number of elements that have not been removed by sink thread yet. Modified by both threads.
	unsigned m_Size;			// Size of the array
};

// Helper class for managing overlapped reads during decompression.
// TODO: Establish the overlapped reads actually do anything, and play with different values of MAX_READ.
class pgReadData {
public:
	// We used fixed-sizes buffers to allow them to be shared across instances of readers.
	static const unsigned MAX_READ = 524288;
	static const int MAX_REQUEST_COUNT = 64;

	static void Worker(void*);

	// PURPOSE:	Init the worker thread with specified priority, cpu and title.
	void Init(sysIpcPriority priority,int cpu,const char *title, int selfIndex, int readBuffers);

	// PURPOSE:	Request worker thread to exit and wait until it does.
	void Shutdown();

	// PURPOSE: Update the thread priority of the reader's thread.
	void SetThreadPriority(sysIpcPriority priority);

	// PURPOSE:	Request an asynchronous read.
#if !RSG_FINAL && RSG_PC
	void Request(const fiDevice *device,fiHandle handle,u64 offset,int count,u32 sortkey,bool uncached, u32 cancelKey, u32 selfIndex, const char *debugName, bool closeWhenFinished, char *destAddr = NULL, const u32 encryption = 0, const bool compressed = false);
#else
	void Request(const fiDevice *device,fiHandle handle,u64 offset,int count,u32 sortkey,bool uncached, u32 cancelKey, u32 selfIndex, const char *debugName, bool closeWhenFinished, char *destAddr = NULL);
#endif

	// PURPOSE: True if we can request reading of another chunk of data without blocking, i.e. there is an unused
	// read buffer available.
	bool CanRead() const	{ return m_SlotsAvailable != 0; }
	bool AnyFreeDecompBuffers() const { return !m_ReadBufferAllocations.IsFull(); }

	// PURPOSE: True if there is data that has been streamed in and can be processed.
	bool HasDataAvailable();

	// PURPOSE: Returns the current "active" chunk, i.e. the least recent chunk that was streamed in that has been
	// made active via ProcessChunk. Returns NULL if there is no active chunk.
	char *GetActiveChunk(int index);

	// PURPOSE: Returns the size of the current "active" chunk, i.e. the least recent chunk that was streamed in that
	// has been made active via ProcessChunk. Returns 0 if there is no active chunk.
	int GetActiveChunkSize(int index);

	// PURPOSE: Verify that the currently active chunk (made active via ProcessChunk) was read using this ID - will
	// assert if this is not the case. Doesn't do anything if there is no active chunk.
	void VerifyCurrentReadId(int index, u32 readId);

	// PURPOSE: Release interest in the least recent chunk of data read so the buffer can be recycled
	// and used for new data. It's safe to call this function when there was no active chunk (i.e. when ProcessChunk
	// hasn't been called) - the function will simply return in that case.
	void FreeActiveChunk(int index);

	// PURPOSE: If we don't already have an active chunk, block until more data is available and make it the active chunk.
	// When calling this function while a chunk is already active, the function will just return.
	int ProcessChunk();

	enum {
		READ_BUFFERS_OPTICAL = 3,
		READ_BUFFERS_HDD = 24,
	};

	// Gets the buffer pointed to given the index from GetAvailableBuffer()
	char *GetBuffer( int index )
	{
		Assertf( index >= 0 && index < m_TotalSlotCount, "Bad index for read buffer" );
		return m_ReadBuffers[index];
	}

	// Gets a temporary decomp buffer from READ_BUFFERS_HDD pre-allocated slots
	int GetAvailableBuffer()
	{
		if( m_ReadBufferAllocations.IsFull() )
		{
			return -1;
		}
		return m_TotalSlotCount - (pgBitList<32>::MaxRequestCount - m_ReadBufferAllocations.Allocate());
	}
	
	//Puts the temporary decomp buffer into the freelist
	void ReleaseBuffer( int index )
	{
		Assertf( index >= 0 && index < m_TotalSlotCount, "Trying to free an invalid buffer %d", index );
		m_ReadBufferAllocations.Free( (pgBitList<32>::MaxRequestCount - m_TotalSlotCount) + index);
		#if PGSTREAMER_DEBUG
			--m_CurrentDecompBuffersUsed;
		#endif
	}
	
	int GetBufferIndex( int requestIndex ) const
	{
		pgReadRequest &rq = m_ReadRequests[requestIndex];
		return rq.m_BufferIndex;
	}
	
	void ClearBufferIndex( int requestIndex )
	{
		pgReadRequest &rq = m_ReadRequests[requestIndex];
		rq.m_BufferIndex = -1;
	}

	u32 TotalSlots() const
	{
		return (u32)m_TotalSlotCount;
	}
private:
	struct pgReadRequest {
		const fiDevice *m_Device;
		fiHandle m_Handle;
		u64 m_Offset;
		char *m_Dest;
		int m_BufferIndex;
		int m_Count;
		u32 m_SortKey;

#if PGSTREAMER_DEBUG
		u32 m_readId;
		u32 m_SelfIndex;
		char m_DebugName[128];
#if RSG_PC && !RSG_FINAL
		u32 m_Encrypted;		//temporary debug for a strange read bug 2352375
		bool m_head;			//temporary debug for a strange read bug 2352375
#endif
#endif // PGSTREAMER_DEBUG

		bool m_Uncached;
		bool m_CloseWhenFinished;
	};

	bool ProcessRequest();
	int m_NextToStream;				// next slot the reader will process, used by reader thread only
	pgLockFreeQueue<int, MAX_REQUEST_COUNT> m_BeingProcessedByStreamer;

	// Number of chunks that have been streamed in and can be consumed. Goes up once a chunk is
	// fully streamed in, goes down once a chunk is freed up via FreeActiveChunk().
	// Accessed by both threads.
	u32 m_ChunksAvailable;

	// Number of slots that are currently not in use and can be used for requests. Used by streamer thread only.
	int m_SlotsAvailable;

	// Total number of slots
	int m_TotalSlotCount;

#if PGSTREAMER_DEBUG
	u32 m_SelfIndex;

	// Minimum value for "slots available".
	int m_MinSlotsAvailable;

	// Wonky average of "slots available".
	float m_AvgSlotsAvailable;
#endif // PGSTREAMER_DEBUG

	// If true, we currently grabbed a chunk to be processed via ProcessChunk. This will be the active chunk (and can be
	// queried via GetActiveChunk() and friends) until FreeActiveChunk() is called. The active chunk is always
	// the one specified in m_OldestUnprocessed.
	bool m_HasActiveChunk;
	
	pgReadRequest *m_ReadRequests;
	char **m_ReadBuffers;
	int *m_ReadCount;		// This is 0 if there is no data, or the data has been freed up
	pgBitList<pgBitList<32>::MaxRequestCount> m_ReadBufferAllocations;	//bits to say what m_ReadBuffers are available
	#if PGSTREAMER_DEBUG
		u32 m_CurrentDecompBuffersUsed;
	#endif
	sysIpcSema m_Start, m_Finish;
	sysIpcThreadId m_Thread;
	pgLockFreeQueue<int, MAX_REQUEST_COUNT> m_FinishedBufferIndex;

#if !__RESOURCECOMPILER && !__TOOL
	friend u32 CrashStreamer(pgReadRequest &r);
#endif	//	!__RESOURCECOMPILER && !__TOOL
};

// Time stamp of the last time we initiated or finished an operation on the specified drive type.
// Use by the disk cache handler to know when to back off on the copying.  This is zero if we know
// an operation is in progress, and the timestamp of the last completed disk access if not.
extern volatile utimer_t pgLastActivity[pgStreamer::DEVICE_COUNT];
extern sysIpcMutex pgIsActive[pgStreamer::DEVICE_COUNT];

} // namespace rage

#endif // PAGING_STREAMER_INTERNAL_H 
