//
// paging/dictionary.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//
#ifndef PAGING_DICTIONARY_H
#define PAGING_DICTIONARY_H

#include "atl/array_struct.h"
#include "paging/base.h"
#include "data/resource.h"
#include "data/struct.h"
#include "string/stringhash.h"
#include "system/interlocked.h"
#include "system/ipc.h"
#include "system/memmanager.h"
#include "system/tasklog.h"

#define ENABLE_MTPLACEMENT_CODE   1

#if ENABLE_MTPLACEMENT_CODE
#define DICTIONARY_THREAD __THREAD
#else
#define DICTIONARY_THREAD
#endif

namespace rage {

	class pgDictionaryBase: public pgBase {
	public:
		// PURPOSE: Specify the thread that is allowed to access the pgDictionary linked list.
		// PARAMS:  tid - thread id
		static inline void SetListOwnerThread(sysIpcCurrentThreadId ASSERT_ONLY(tid))
		{
			ASSERT_ONLY(sm_ListOwnerThread = tid;)
		}

	protected:
		// PURPOSE: Assert that the current thread is the current owner of the pgDictionary linked list access.
		static inline void AssertIsListOwnerThread()
		{
#if !ENABLE_MTPLACEMENT_CODE
			FastAssert(g_CurrentThreadId == sm_ListOwnerThread);
#endif
		}

	private:
		ASSERT_ONLY(static sysIpcCurrentThreadId sm_ListOwnerThread;)
	};

	template <class _T> class pgDictionary: public pgDictionaryBase {
	public:
		// PURPOSE:	Constructor
		// PARAMS:	memberCount - Number of entries in the dictionary
		pgDictionary(int memberCount) : m_Parent(NULL), m_RefCount(1) {
			m_Codes.Reserve(memberCount);
			m_Entries.Reserve(memberCount);
		}

		// PURPOSE:	Resource constructor
		pgDictionary(datResource &rsc) : m_Codes(rsc), m_Entries(rsc,true) { 
			SAFE_ADD_KNOWN_REF(m_Parent);
		}

		// PURPOSE: Destructor
		~pgDictionary() 
		{
			SetParent(NULL);
			for (int i=0; i<m_Entries.GetCount(); i++)
			{
				if (m_Entries[i]) 
				{
#if RESOURCE_HEADER
					sysMemManager::GetInstance().DeleteNodeInfo(m_Entries[i]);
#endif
#if __ASSERT
					ASSERT_ONLY(int count =) m_Entries[i]->Release();
					char buffer[128];
					Assertf(!count,"Dangling reference to dictionary (element %p (%s), index %d, count %d)", (_T *) m_Entries[i], GetElementName(buffer, sizeof(buffer), (_T *) m_Entries[i]), i, count);
#else
					m_Entries[i]->Release();
#endif
				}
			}
		}

#if RESOURCE_HEADER
		void* GetLastTexturePointer() const
		{
			void* last = NULL;
			void* ptr;

			for (int i = 0; i < m_Codes.GetCount(); ++i)
			{
				ptr = reinterpret_cast<void*>(reinterpret_cast<size_t>(&m_Codes[i]) + sizeof(u32));
				if (ptr > last)
					last = ptr;
			}

			for (int i = 0; i < m_Entries.GetCount(); ++i)
			{
				_T* pT = m_Entries[i];				
				ptr = reinterpret_cast<void*>(reinterpret_cast<size_t>(pT) + sizeof(_T));
				if (ptr > last)
					last = ptr;
			}

			ptr = reinterpret_cast<void*>(reinterpret_cast<size_t>(*(void**) &m_Entries) + (m_Entries.GetCount() * sizeof(datOwner<_T>)));
			if (ptr > last)
				last = ptr;	

			pgBaseNode** pNext = &m_FirstNode;
			ptr = *pNext;
			while (ptr)
			{
				if (ptr > last)
					last = reinterpret_cast<void*>(reinterpret_cast<size_t>(ptr) + 0x20);

				pNext = &m_FirstNode->Next;
				ptr = *pNext;
			}

			return last;
		}

		void SetUserData(u32 data)
		{
			for (int i = 0; i < m_Entries.GetCount(); ++i)
				sysMemManager::GetInstance().SetUserData(m_Entries[i], data);
		}
#endif

		// PURPOSE:	Returns currently active dictionary of this type
		static pgDictionary<_T> *GetCurrent() {
			AssertIsListOwnerThread();
			return sm_CurrentDict;
		}

		// PURPOSE: Sets currently active dictionary of this type, returns previously active one.
		static pgDictionary<_T> *SetCurrent(pgDictionary<_T> *newDict) {
			AssertIsListOwnerThread();
			TASKLOG("pgDict::Set Old:0x%x New:0x%x Thread:0x%x", (size_t)sm_CurrentDict, (size_t)newDict, (size_t)(sysIpcGetCurrentThreadId()));
			pgDictionary<_T> *prev = sm_CurrentDict;
			sm_CurrentDict = newDict;
			if (newDict)
				newDict->AddRef();
			if (prev)
				prev->Release();
			return prev;
		}

		// PURPOSE: Returns parent dictionary (may be null).  We go up the parent chain during
		//			a lookup operation until we get a match, allowing you to implement a simple
		//			hierarchy of shared objects.
		// NOTES:	Since dictionaries only know their parent, not their children, multiple
		//			dictionaries can share the same parent chain.
		pgDictionary<_T> *GetParent() const {
			AssertIsListOwnerThread();
			return m_Parent;
		}

		// PURPOSE:	Sets the parent of this dictionary object.
		void SetParent(pgDictionary<_T> *parent) 
		{
			AssertIsListOwnerThread();
			if (parent != m_Parent) 
			{
				if (m_Parent) 
				{
#if RESOURCE_HEADER
					SAFE_REMOVE_KNOWN_REF(m_Parent);
#else
					REMOVE_KNOWN_REF(m_Parent);
#endif
					m_Parent->Release();
				}
				
				m_Parent = parent;
				if (m_Parent) 
				{
					m_Parent->AddRef();
					SAFE_ADD_KNOWN_REF(m_Parent);
				}
			}
		}

		// PURPOSE: Inserts self into the current dictionary chain.
		void Push() {
			TASKLOG("pgDict::Push Old:0x%x New:0x%x Thread:0x%x", (size_t)sm_CurrentDict, (size_t)this, (size_t)(sysIpcGetCurrentThreadId()));
			AssertIsListOwnerThread();
			FastAssert(!m_Parent);
			m_Parent = sm_CurrentDict;
			SAFE_ADD_KNOWN_REF(m_Parent);
			sm_CurrentDict = this;
		}

		// PURPOSE: Pops self out of the current dictionary chain.
		// NOTES:	Since dictionaries are unaware of their children, there is no
		//			protection against disconnecting any child dictionaries from
		//			the list.
		void Pop() {
			TASKLOG("pgDict::Pop Old:0x%x New:0x%x Thread:0x%x", (size_t)sm_CurrentDict, (size_t)this->m_Parent, (size_t)(sysIpcGetCurrentThreadId()));
			AssertIsListOwnerThread();
			FastAssert(sm_CurrentDict == this);
			sm_CurrentDict = m_Parent;
			SAFE_REMOVE_KNOWN_REF(m_Parent);
			m_Parent = NULL;
		}

		// PURPOSE: Removes this item from the dictionary chain.
		// NOTES: Only works on list-shaped dict. chains, not tree-shaped ones.
		//        Does an O(N) list traversal. 
		void RemoveFromList()
		{
			AssertIsListOwnerThread();
			if (!sm_CurrentDict)
			{
				return;
			}
			else if (this == sm_CurrentDict)
			{
				Pop();
			}
			else
			{
				pgDictionary<_T>* curr = sm_CurrentDict;
				pgDictionary<_T>* next = curr->m_Parent;
				while(next && next != this)
				{
					curr = curr->m_Parent;
					next = curr->m_Parent;
				}
				if (next == this)
				{
					SAFE_REMOVE_KNOWN_REF(curr->m_Parent);
					curr->m_Parent = m_Parent;
					SAFE_ADD_KNOWN_REF(curr->m_Parent);

					SAFE_REMOVE_KNOWN_REF(m_Parent);
					m_Parent = NULL;
				}
			}
		}

		// PURPOSE:	Increases reference count
		void AddRef() {
			sysInterlockedIncrement((u32 *) &m_RefCount);
		}

		// PURPOSE:	Decreases reference count
		int Release() const {
			int refCount = sysInterlockedDecrement((u32 *) &m_RefCount);
			if (!refCount) {
				delete this;
				return 0;
			}
			else
				return refCount;
		}

		// PURPOSE:	Looks up an object by hashcode
		// PARAMS:	code - named object to look up
		// RETURNS:	Pointer to object, or NULL if not found
		_T* Lookup(u32 code) const {
			// There are some safe uses of this function from threads other than
			// sm_ListOwnerThread, so we cannot AssertIsListOwnerThread here.
			// Be very careful when using from another thread, generally
			// LookupLocal is what you want.
			const pgDictionary *current = this;
			do {
				Assert(current != NULL);
				int idx = current->m_Codes.BinarySearch(code);
				if (idx != -1)
					return current->m_Entries[idx];
				current = current->m_Parent;
			} while (current);
			return NULL;
		}

		// PURPOSE:	Looks up an object by hashcode (only in current dictionary)
		// PARAMS:	code - named object to look up
		// RETURNS:	Pointer to object, or NULL if not found
		_T* LookupLocal(u32 code) const {
			int idx = m_Codes.BinarySearch(code);
			if (idx != -1)
				return m_Entries[idx];
			else
				return NULL;
		}

		// PURPOSE:	Looks up an object by hashcode (only in current dictionary)
		// PARAMS:	code - named object to look up
		// RETURNS:	Index of object (suitable for passing into GetEntry) or -1 if not found.
		int LookupLocalIndex(u32 code) const {
			return m_Codes.BinarySearch(code);
		}

		// PURPOSE: Abstracts the underlying hashcode computation
		static u32 ComputeHash(const char *name) {
			return atStringHash(name);
		}

		// PURPOSE:	Looks up an object by name (computed hashcode)
		// PARAMS:	name - named object to look up
		// RETURNS:	Pointer to object, or NULL if not found
		_T* Lookup(const char *name) const {
			return Lookup(ComputeHash(name));
		}

		// PURPOSE:	Looks up an object by name (computed hashcode)
		// PARAMS:	name - named object to look up
		// RETURNS:	Pointer to object, or NULL if not found
		_T* LookupLocal(const char *name) const {
			return LookupLocal(ComputeHash(name));
		}

		// Callback typedef
		typedef bool (*DictionaryCB)(_T&, u32, void* data);

		// PURPOSE:	Execute a specified callback on every item in this dictionary
		// PARAMS:	cb - callback function
		//			data - user-specified callback
		// RETURNS:	True if all callback succeeded (or dictionary was empty), else false
		//			if one of the callbacks failed.
		bool ForAll(DictionaryCB cb, void* data) {
			bool result = true;
			for (int i=0; i<m_Entries.GetCount(); i++)
				if ((result = cb(*m_Entries[i], m_Codes[i], data)) == false)
					break;
			return result;
		}

		// PURPOSE:	Add an entry to the dictionary.  Intended only for use during
		//			initial dictionary construction.
		// PARAMS:	name - name of the object
		//			data - pointer to the object to associate with this data
		// RETURNS:	True on success, false if there was a hash collision (usually from duplicate name)
		bool AddEntry(u32 code,_T *data) {
			int i;
			for (i=0; i<m_Codes.GetCount(); i++) {
				if (m_Codes[i] == code)
					return false;
				else if (m_Codes[i] > code)
					break;
			}
			m_Codes.Insert(i);
			m_Codes[i] = code;
			m_Entries.Insert(i);
			m_Entries[i] = data;
			return true;
		}

		// PURPOSE:	Add an entry to the dictionary.  Intended only for use during
		//			initial dictionary construction.
		// PARAMS:	name - name of the object
		//			data - pointer to the object to associate with this data
		// RETURNS:	True on success, false if there was a hash collision (usually from duplicate name)
		bool AddEntry(const char *name,_T *data) {
			u32 code = ComputeHash(name);
			return AddEntry(code,data);
		}

#if !__FINAL
		// PURPOSE:	Add an entry to the dictionary, dynamically after construction ..
		bool AddNewEntry(const char *name,_T *data) {
			// Make sure we have enough capacity in the array, resizing if necessary
			m_Codes.Grow(); // add to the end
			m_Entries.Grow();
			m_Codes.Pop();	// remove from the end
			m_Entries.Pop();
			return AddEntry(name, data); // now insert into the sorted position
		}

		// PURPOSE: Deletes an entry from the dictionary
		// PARAMS: The entry name to search for and delete
		// RETURNS: true if the item was found and deleted
		// NOTES: Does not free the storage for this entry, unreference it, or anything like that.
		bool DeleteEntry(const char* name) {
			int index = LookupLocalIndex(ComputeHash(name));
			if (index >= 0)
			{
				m_Codes.Delete(index);
				m_Entries.Delete(index);
				return true;
			}
			return false;
		}
#endif // !__FINAL

		// PURPOSE:	Resource constructor helper
		static void Place(void *that,datResource &rsc) {
			::new (that) pgDictionary<_T>(rsc);
		}

#if __DECLARESTRUCT
		// PURPOSE:	Declare our fields for offline resource serialization
		void DeclareStruct(datTypeStruct &s) {
			pgBase::DeclareStruct(s);
			STRUCT_BEGIN(pgDictionary<_T>);
			STRUCT_IGNORE(m_Parent);
			STRUCT_FIELD(m_RefCount);
			STRUCT_PADDING64(m_Padding);
			STRUCT_FIELD(m_Codes);
			STRUCT_FIELD(m_Entries);
			STRUCT_END();
		}
#endif

		// RETURNS: Reference count
		int GetRefCount() const {
			return m_RefCount;
		}

		// RETURNS: Number of items in the dictionary
		int GetCount() const {
			return m_Codes.GetCount();
		}

		// RETURNS: Hash code at this ordinal
		u32 GetCode(u32 idx) const {
			return m_Codes[idx];
		}

		// RETURNS: Entry at this ordinal
		_T *GetEntry(u32 idx) const {
			return m_Entries[idx];
		}

		// PURPOSE: Set the entry to a new value. NOTE that this is an unsafe function since
		// it does not do ANY reference counting - it will NOT call Release() on the previous
		// entry and it will NOT call AddRef() or anything on the new one. Use at your own
		// risk.
		void SetEntryUnsafe(u32 idx, _T *entry) {
			m_Entries[idx] = entry;
		}

#if BASE_DEBUG_NAME
		static const char *GetElementName(char *buffer, size_t bufferSize, const pgBase *obj) 				{ return obj->GetDebugName(buffer, bufferSize); }
		static const char *GetElementName(char * /*buffer*/, size_t /*bufferSize*/, const void * /*ptr*/) 	{ return ""; }
#else // BASE_DEBUG_NAME
		static const char *GetElementName(char * /*buffer*/, size_t /*bufferSize*/, const void * /*ptr*/) 	{ return ""; }
#endif // BASE_DEBUG_NAME

	private:
		static DICTIONARY_THREAD pgDictionary<_T> *sm_CurrentDict;
		DECLARE_PADDED_POINTER(pgDictionary<_T>,m_Parent);
		mutable int m_RefCount;
		datPadding64(4,m_Padding);
		atArray<u32> m_Codes;
		atArray< datOwner<_T> > m_Entries;
	};


	// Note, these two classes are EVIL C++ VOODOO.
	// The idea is that we need a specific texture list to be pushed to the stack
	// at the time we call the resource constructor for m_Models
	// (at which time texture pointers will be fixed up)
	// So to accomplish this, we have a dummy object whose sole purpose is to push
	// a texture list in it's c'tor, and then one that pops a list in it's c'tor.
	// We put these around the model dict so their c'tors are run right before and
	// after the models' resource c'tor.

	template <class _T> struct pgDictionaryPusher
	{
		pgDictionaryPusher() { }
		explicit pgDictionaryPusher(pgDictionary<_T>* dict) { if (dict) dict->Push(); }

#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&s) 
		{ 
			STRUCT_BEGIN(pgDictionaryPusher);
			STRUCT_IGNORE(m_Padding);
			STRUCT_END();
		}
#endif

		datPadding<4> m_Padding;
	};

	template <class _T> struct pgDictionaryPopper
	{
		pgDictionaryPopper() { }
		explicit pgDictionaryPopper(pgDictionary<_T>* dict)	{ if (dict) dict->Pop(); }

#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&s) 
		{ 
			STRUCT_BEGIN(pgDictionaryPopper);
			STRUCT_IGNORE(m_Padding);
			STRUCT_END();
		}
#endif

		datPadding<4> m_Padding;
	};

}	// namespace rage

#endif	// PAGING_DICTIONARY_H
