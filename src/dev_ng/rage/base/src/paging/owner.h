// 
// paging/owner.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PAGING_OWNER_H
#define PAGING_OWNER_H

#include "data/struct.h"
#include "paging/base.h"

namespace rage {

/* Owned defragmentable pointer class. */
template <class _T> struct pgOwner {
	pgOwner() { 
#if !__SPU
		if (!datResource_sm_Current) 
			ptr = 0; 
		else if (ptr) {
			if (!datResource_IsDefragmentation || !pgBase::IsTrackedAddress(ptr)) {
				datResource::Place(ptr); 
#if ENABLE_KNOWN_REFERENCES
				if (pgBase::IsTrackedAddress(ptr))
					ADD_KNOWN_REF(ptr);		// We already do the same tests SAFE_ADD_KNOWN_REF does.
#endif
			}
			else {	// Pointer is already fixed up before now
				Assert(IS_POINTER_KNOWN(ptr));
				ptr->Place(ptr,*datResource_sm_Current);
			}
		}
#endif
	}

	~pgOwner() {
#if ENABLE_KNOWN_REFERENCES
		if (ptr)
			SAFE_REMOVE_KNOWN_REF(ptr);
#endif
	}

	void Release() {
		if (ptr) {
			SAFE_REMOVE_KNOWN_REF(ptr);
			ptr->Release();
			ptr = NULL;
		}
	}

#if __SPU
	bool IsLocalStore() const { return (unsigned)ptr < 256*1024; }
#endif
	_T& operator*() const { return *ptr; }
	_T* operator->() const { return ptr; }
	operator _T*() const { return ptr; }
	void operator=(_T* that) { 
		UPDATE_KNOWN_REF(ptr, that);
	}
	void operator=(const pgOwner& that) {
		UPDATE_KNOWN_REF(ptr, that.ptr);
	}

	DECLARE_PADDED_POINTER(_T,ptr); 
};

#if __DECLARESTRUCT
template <class _T> inline void datSwapper(pgOwner<_T> &ref)	{ if (ref.ptr) datSwapper(*ref.ptr); datSwapper((char*&)(ref.ptr)); }
#endif

}	// namespace rage

#endif	// PAGING_OWNER_H