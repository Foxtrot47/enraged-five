//
// paging/rscbuilder.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#include "rscbuilder.h"

#include "paging/streamer.h"

#include "atl/array.h"
#include "data/aes.h"
#include "data/marker.h"
#include "data/struct.h"
#include "data/resourceheader.h"
#include "diag/tracker.h"
#include "file/asset.h"
#include "file/device.h"
#include "grcore/config.h"
#include "math/amath.h"
#include "math/intrinsics.h"
#include "system/cache.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/multiallocator.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "system/memmanager.h"
#if ENABLE_BUDDY_ALLOCATOR
#include "system/buddyallocator.h"
#else
#include "system/virtualallocator.h"
#endif
#include "system/platform.h"
#include "system/timer.h"

#if __RESOURCECOMPILER
#include "system/xcompress_settings.h"
#include "system/xtl.h"
#include "xcompress_7776.h"
#if RSG_CPU_X64
#pragma comment(lib,"xcompress_x64_2008.lib")
#elif RSG_CPU_X86 && (!defined(__RGSC_DLL) || !__RGSC_DLL)
#pragma comment(lib,"xcompress_2008.lib")
#endif

#include "zlib/zlib.h"

#include <algorithm>
#include "system/rsg_algorithm.h" //for lower bound
#endif


namespace rage {

#if __XENON && ENABLE_DEBUG_HEAP
extern size_t g_sysExtraStreamingMemory;
#endif

#if !__FINAL
// True if the last failed allocation was physical, false if physical.
static bool s_WasLastFailedAllocationPhysical;

// Size of the last unsuccessful allocation attempt in AllocateMemory().
static size_t s_LastFailedAllocationSize;
#endif // !__FINAL


bool pgRscBuilder::Exists(const char* filename, const char* ext)
{
	char finalname[RAGE_MAX_PATH];
	pgRscBuilder::ConstructName(finalname, sizeof(finalname), filename, ext);
	return ASSET.Exists(finalname, "");
}

char* pgRscBuilder::ConstructName(char *dest,int destSize,const char *path,const char *ext,bool forSave ) {
	char extBuf[16];
	char resolvedPath[RAGE_MAX_PATH];

	// Replace platform spec anywhere else it may appear in the path, useful for directory names.
	char *s = resolvedPath;

	while (*path) {
		if (*path == '#')
			*(s++) = g_sysPlatform;
		else
			*(s++) = *path;
		path++;
	}
	*s = 0;

	if (ext)
	{
		extBuf[0] = g_sysPlatform;
		AssertMsg(ext[0] == '#' || ext[0] == g_sysPlatform,"Extension should start with '#', replaced by platform id");
		safecpy(extBuf+1,ext+1,sizeof(extBuf)-1);
		if (forSave)
			ASSET.FullWritePath(dest,destSize,resolvedPath,extBuf);
		else
			ASSET.FullReadPath(dest,destSize,resolvedPath,extBuf);
	}
	else
	{
		if (forSave)
			ASSET.FullWritePath(dest,destSize,resolvedPath,"");
		else
			ASSET.FullReadPath(dest,destSize,resolvedPath,"");
	}

	return dest;
}



void* pgRscBuilder::BeginStream(const char *filename,const char *ext,int version,pgStreamerInfo &outInfo,datResourceMap &map) {
	if (!OpenStream(filename,ext,version,outInfo))
		return NULL;
	void *result = ContinueStream(outInfo,map,NULL,NULL);
	if (!result) {
		pgStreamer::Close(outInfo.FileHandle);
		outInfo.FileHandle = pgStreamer::Error;
		Errorf("ContinueStream(%s.%s) failed, probably out of memory.",filename,ext);
	}
	return result;
}


bool pgRscBuilder::OpenStream(const char *filename,const char *ext,int version,pgStreamerInfo &outInfo) {
	char buffer[RAGE_MAX_PATH];
	ConstructName(buffer,sizeof(buffer),filename,ext);
	return OpenStream(outInfo,pgStreamer::Open(buffer,NULL,version));
}


bool pgRscBuilder::OpenStream(pgStreamerInfo &outInfo,u32 fileHandle) {
	outInfo.FileHandle = fileHandle;
	outInfo.ReadHandle = NULL;
	if (outInfo.FileHandle == pgStreamer::Error)
		return false;
	else {
		outInfo.Info = pgStreamer::GetResourceInfo(outInfo.FileHandle);
		return true;
	}
}


void *pgRscBuilder::AllocateMemory(size_t size,bool isPhysical) {
	void *result = sysMemAllocator::GetCurrent().RAGE_LOG_ALLOCATE_HEAP(ComputeLeafSize(size, isPhysical),128,isPhysical? MEMTYPE_RESOURCE_PHYSICAL : MEMTYPE_RESOURCE_VIRTUAL);

#if !__FINAL
	if (!result) {
		s_LastFailedAllocationSize = ComputeLeafSize(size, isPhysical);
		s_WasLastFailedAllocationPhysical = isPhysical;
	}
#endif // !__FINAL

	return result;
}

void pgRscBuilder::DuplicateMemory(void *dest,const void *src,size_t bytes,bool isPhysical) {
	memcpy(dest, src, bytes);
	if (isPhysical)
		WritebackDC(dest, (int)bytes);
}


void pgRscBuilder::FreeMemory(void *ptr,bool isPhysical) {
	sysMemAllocator::GetCurrent().GetAllocator(isPhysical? MEMTYPE_RESOURCE_PHYSICAL : MEMTYPE_RESOURCE_VIRTUAL)->Free(ptr); 
}


void pgRscBuilder::FreeMemory(const datResourceMap &map) {
	if (sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->SupportsAllocateMap()) {
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->FreeMap(map);
		return;
	}

	int i = map.VirtualCount
#if 1 // !FREE_PHYSICAL_RESOURCES		
		+ map.PhysicalCount
#endif // !FREE_PHYSICAL_RESOURCES		
		;

	while (i)
	{
		i--;
	//	Displayf("FreeMemory() - 0x%p - 0x%p (0x%08x)", map.Chunks[i].Addr, (char*)map.Chunks[i].Addr + map.Chunks[i].Size,map.Chunks[i].Size);
		//FreeMemory(map.Chunks[i].DestAddr,i>=map.VirtualCount);
		if (map.Chunks[i].DestAddr)
		{
			sysMemAllocator *allocator = sysMemAllocator::GetCurrent().GetPointerOwner(map.Chunks[i].DestAddr);
#if !__FINAL
			if (!allocator)
				Quitf("pgRscBuilder::FreeMemory doesn't know who owns the memory at 0x%p", map.Chunks[i].DestAddr);
#endif
			allocator->Free(map.Chunks[i].DestAddr); 
		}
	}
}


void rscbuilder_signal_callback(void *userArg,void * /*dest*/,u32 /*offset*/,u32 /*amtRead*/) {
	sysIpcSignalSema((sysIpcSema)userArg);
}


static void* ContinueFailure(pgStreamerInfo &info) {
	if (info.Sema) {
		sysIpcDeleteSema(info.Sema);
		info.Sema = 0;
	}
	return NULL;
}

void* pgRscBuilder::ContinueStream(pgStreamerInfo &info,datResourceMap &map,pgStreamerCallback callback,void *userData, u32 flags,float prio) {

	if (!callback) {
		callback = rscbuilder_signal_callback;
		userData = (void*)(info.Sema = sysIpcCreateSema(false));
		Assert(info.Sema);
	}
	else
		info.Sema = 0;

	bool needsAllocation = map.VirtualCount == 0;
	if (!ValidateMap(pgStreamer::GetFullName(info.FileHandle),map,info.Info)) {
		return ContinueFailure(info);
	}

	// We can now do the read all at once.
	// TODO: The offset "sizeof(datResourceFileHeader)" depends on whether we strip the resource header
	// in archives or not.  For now we could just leave them in, it's only twelve bytes
	info.ReadHandle = pgStreamer::Read(info.FileHandle,map.Chunks,map.PhysicalCount+map.VirtualCount,
		sizeof(datResourceFileHeader),
		callback,userData,pgStreamer::HARDDRIVE,flags,map.GetVirtualBase(),prio);
	if (!info.ReadHandle) {
		if (needsAllocation)
			FreeMemory(map);
		return ContinueFailure(info);
	}
	// Return the base address of virtual memory.
	return map.GetVirtualBase();
}


bool pgRscBuilder::EndStream(pgStreamerInfo &info,bool block,bool close) {
	Assert(info.Sema);
	if (!block) {
		if (!sysIpcPollSema(info.Sema))
			return false;
	}
	else
		sysIpcWaitSema(info.Sema);
	sysIpcDeleteSema(info.Sema);
	info.Sema = 0;
	info.ReadHandle = NULL;
	if (close) {
		pgStreamer::Close(info.FileHandle);
		info.FileHandle = pgStreamer::Error;
	}
	return true;
}

void pgRscBuilder::CancelStream(pgStreamerInfo &info,bool close) {
	if (info.ReadHandle) {
		pgStreamer::Cancel(info.ReadHandle);
		info.ReadHandle = NULL;
		sysIpcDeleteSema(info.Sema);
		info.Sema = 0;
		if (close) {
			pgStreamer::Close(info.FileHandle);
			info.FileHandle = pgStreamer::Error;
		}
	}
}

void pgRscBuilder::Cleanup(datResourceMap & map, bool freeVirtual)
{
#if __D3D9
	freeVirtual;
	map;
#else // __D3D9
#if __WIN32PC
	sysMemAllowResourceAlloc++;

	while (map.PhysicalCount)
	{
		int iIndex = map.VirtualCount + --map.PhysicalCount;
		void *ptr = map.Chunks[iIndex].DestAddr;
#if __DEV
		memset(ptr, 0xFE, map.Chunks[iIndex].Size);
#endif // __DEV
		map.Chunks[iIndex].DestAddr = NULL;
		FreeMemory(ptr,true);
	}
	sysMemAllowResourceAlloc--;
#endif
	if (freeVirtual)
	{
		sysMemAllowResourceAlloc++;
	
		while (map.VirtualCount)
		{
			void *ptr = map.Chunks[--map.VirtualCount].DestAddr;
			FreeMemory(ptr,false);
		}
		sysMemAllowResourceAlloc--;
	}
#endif // __D3D9
}

void pgRscBuilder::GenerateMap(datResourceInfo info, datResourceMap &map) {
	info.GenerateMap(map);
}

bool pgRscBuilder::AllocateMap(datResourceMap &map)
{
	if (sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->SupportsAllocateMap())
		return sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->AllocateMap(map);

#if RESOURCE_HEADER
	if (map.CanOptimize() && map.IsRPF())
	{
		Assert(!map.PhysicalCount);
		sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL);
		map.Chunks[0].DestAddr = pAllocator->Allocate(map.Chunks[0].Size, 16);
		
		if (!map.Chunks[0].DestAddr)
		{
			// If header allocation fails, default to the resource heap
			map.Chunks[0].DestAddr = AllocateMemory(map.Chunks[0].Size, false);
			if (!map.Chunks[0].DestAddr) 
				return false;	
		}

		map.Chunks[0].SrcAddr = (char*) datResourceFileHeader::c_FIXED_VIRTUAL_BASE;
	}
#if __XENON && ENABLE_DEBUG_HEAP
	else if (g_sysExtraStreamingMemory > 0 && map.IsScript())
	{
		Assert(!map.PhysicalCount);
		sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL);

		for (size_t i = 0; i < map.VirtualCount; ++i) 
		{
			map.Chunks[i].DestAddr = pAllocator->Allocate(map.Chunks[i].Size, 16);

			if (!map.Chunks[i].DestAddr) 
				Quitf("Failed to allocate virtual memory of size %d from Debug heap!", map.Chunks[i].Size);
		}

		map.VirtualBase = map.Chunks[map.RootVirtualChunk].DestAddr;

		return true;	
	}
#endif
	else
#endif
	{
		for (size_t i=0; i<map.VirtualCount; ++i) 
		{
			map.Chunks[i].DestAddr = AllocateMemory(map.Chunks[i].Size,false);

			if (!map.Chunks[i].DestAddr) 
			{
				WIN32PC_ONLY(Warningf("Failed to allocation virtual memory of size %d Largest Free Block %d Available %d", map.Chunks[i].Size, sysMemAllocator::GetCurrent().GetAllocator(false? MEMTYPE_RESOURCE_PHYSICAL : MEMTYPE_RESOURCE_VIRTUAL)->GetLargestAvailableBlock(), map.Chunks[i].Size, sysMemAllocator::GetCurrent().GetAllocator(false? MEMTYPE_RESOURCE_PHYSICAL : MEMTYPE_RESOURCE_VIRTUAL)->GetMemoryAvailable());)
				while (i--) 
				{
					FreeMemory(map.Chunks[i].DestAddr,false);	
					map.Chunks[i].DestAddr = NULL;
				}

				return false;
			}
		}
	}
	
	map.VirtualBase = map.Chunks[map.RootVirtualChunk].DestAddr;

	for (size_t i=map.VirtualCount; i<(size_t)(map.VirtualCount+map.PhysicalCount); ++i) 
	{
		map.Chunks[i].DestAddr = AllocateMemory(map.Chunks[i].Size,true);
		if (!map.Chunks[i].DestAddr) 
		{
			WIN32PC_ONLY(Warningf("Failed to allocation physical memory of size %d Largest Free Block %d Available %d", map.Chunks[i].Size, sysMemAllocator::GetCurrent().GetAllocator(true? MEMTYPE_RESOURCE_PHYSICAL : MEMTYPE_RESOURCE_VIRTUAL)->GetLargestAvailableBlock(), map.Chunks[i].Size, sysMemAllocator::GetCurrent().GetAllocator(true ? MEMTYPE_RESOURCE_PHYSICAL : MEMTYPE_RESOURCE_VIRTUAL)->GetMemoryAvailable());)
			while (i--) 
			{
				FreeMemory(map.Chunks[i].DestAddr,i>=map.VirtualCount);
				map.Chunks[i].DestAddr = NULL;	
			}
			return false;
		}
	}

	return true;
}

bool pgRscBuilder::ValidateMap(const char *
#if RAGE_TRACKING
							   name
#endif
							   , datResourceMap &map,datResourceInfo hdr)
{
#if RAGE_TRACKING
	RAGE_TRACK(Resources);
	diagTrackerHelper helper(name);
#endif

	GenerateMap(hdr, map);
	return AllocateMap(map);
}


void* pgRscBuilder::LoadBuild(const char *name,const char *ext,int version,datResourceMap &map,datResourceInfo &outHdr)
{
	char buffer[RAGE_MAX_PATH];
	ConstructName(buffer,sizeof(buffer),name,ext);

	// This assert is annoying for rage samples.
	// Assertf(0, "pgRscBuilder::LoadBuild deprecated.  Use streaming LoadObject function for '%s'",buffer);

	pgStreamerInfo info;

	bool needInit = !pgStreamer::IsInitialized();
	if (needInit)
		pgStreamer::InitClass();

	void *result = NULL;

	RAGE_OBJECT_INIT(result);
	RAGE_OBJECT_LABEL(result, "%s", buffer);

	{
		RAGE_SCOPE_OBJECT(result);

	if (BeginStream(name,ext,version,info,map)) {
		EndStream(info,true,true);
		outHdr = info.Info;
		result = map.GetVirtualBase();
	}
	}

	RAGE_OBJECT_KILL(result);

	//if (needInit)
	//	pgStreamer::ShutdownClass();

	return result;
}

#if ENABLE_BUDDY_ALLOCATOR
size_t pgRscBuilder::ComputeLeafSize(size_t size, bool physicalHeap) 
{
	int heap = (physicalHeap) ? MEMTYPE_RESOURCE_PHYSICAL : MEMTYPE_RESOURCE_VIRTUAL;
	sysMemAllocator* allocator = sysMemAllocator::GetCurrent().GetAllocator(heap);
	Assert(allocator);
	Assert(dynamic_cast<sysMemBuddyAllocator*>(allocator));
	size_t leafSize = static_cast<sysMemBuddyAllocator*>(allocator)->GetLeafSize();

	return sysMemBuddyAllocator::ComputeNodeSize(size,  leafSize);
}
#else
size_t pgRscBuilder::ComputeLeafSize(size_t size, bool /*physicalHeap*/) 
{
	return sysMemVirtualAllocator::ComputeAllocationSize(size);
}
#endif

#if !__FINAL
bool pgRscBuilder::WasLastFailedAllocationPhysical()
{
	return s_WasLastFailedAllocationPhysical;
}

size_t pgRscBuilder::GetLastFailedAllocationSize()
{
	return s_LastFailedAllocationSize;
}
#endif // !__FINAL

const u32 pgBaseMetaDataDebugNameTypeCode = 'DbgN';

struct pgBaseMetaDataDebugName: public pgBaseMetaData 
{
	pgBaseMetaDataDebugName(datResource &rsc) : pgBaseMetaData(rsc) { rsc.PointerFixup(Name); }
	pgBaseMetaDataDebugName(const char *name) : pgBaseMetaData(pgBaseMetaDataDebugNameTypeCode), Name(StringDuplicate(name)) { }
	const char *Name;
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&s) {
		// This also takes care of the linked list.
		pgBaseMetaData::DeclareStruct(s);
		STRUCT_BEGIN(pgBaseMetaDataDebugName);
		STRUCT_FIELD(Name);
		STRUCT_END();
	}
#endif
};

struct pgBaseMetaDataDebugNameType: public pgBaseMetaDataType {
	pgBaseMetaDataDebugNameType() : pgBaseMetaDataType(pgBaseMetaDataDebugNameTypeCode) { }
	void Place(pgBaseMetaData* that,datResource &rsc) {
		rage_placement_new(that) pgBaseMetaDataDebugName(rsc);
	}
#if __DECLARESTRUCT
	void DeclareStruct(pgBaseMetaData *that,datTypeStruct&s) {
		// Need explicit downcast here so we invoke the correct function.
		static_cast<pgBaseMetaDataDebugName*>(that)->DeclareStruct(s);
	}
#endif
} DebugNameType;

#if __RESOURCECOMPILER

PARAM(savealloctrace,"[rscbuilder] Save a memory allocation trace with each resource heap.");
PARAM(fastCompression,"[rscbuilder] Use fast zlib compression, even on Xenon");
PARAM(RSCDebugLevel,"set the rscbuilder debug spew level");

int g_rscDebug = 1;

static const char* s_BuildName = NULL;

// Initialize to values suitable for WIN32 since the resource compiler is based on that platform
size_t g_rscVirtualLeafSize = g_rscVirtualLeafSize_WIN32; 
size_t g_rscPhysicalLeafSize = g_rscPhysicalLeafSize_WIN32;
int ATL_ARRAY_MIN_ALIGN = 16;

static fiStream *s_AllocTrace;
static bool s_SavedBuild = true;	// in case they never called BeginBuild/SaveBuild/EndBuild.

static class rscAllocator: public sysMemMultiAllocator {
public:
	void* Allocate(size_t siz, size_t align,int heapIndex) {
		void *result = sysMemMultiAllocator::Allocate(siz,align,heapIndex);
		if (s_AllocTrace)
			fprintf(s_AllocTrace,"A,%u,%u,%u,%p\r\n",siz,align,heapIndex,result);
		/* if (heapIndex == 2)
			Displayf("%d,%6x,%d,%p",s_IsSecondPass+1,siz,heapIndex,result); */
		if (!result)
			Quitf("Resource heap overflow.  You tried to allocate %u B from heap %u.", siz, heapIndex);
		return result;
	}
	void Free(const void *ptr) {
		if (ptr && s_AllocTrace && !s_SavedBuild)
		{
			size_t siz = sysMemMultiAllocator::GetSize(ptr);
			fprintf(s_AllocTrace,"F,%u,%p\r\n", siz, ptr);
		}
		sysMemMultiAllocator::Free(ptr);
	}

	bool IsBuildingResource() const { return true; }

	bool IsRootResourceAllocation() const {
		return GetAllocator(MEMTYPE_GAME_VIRTUAL)->IsRootResourceAllocation();
	}
} *s_rscAllocator;

PARAM(uncompressed,"[rscbuilder] Disable resource heap compression (on by default now)");

static bool s_InBuild;
extern bool g_ReadiedBuild;
static sysMemAllocator *s_prevAllocator;
size_t g_LargestVirtualAllocation, g_LargestPhysicalAllocation;

bool pgRscBuilder::IsBuilding()
{
	return s_rscAllocator != NULL;
}

bool pgRscBuilder::IsBuildingFirstPass()
{
	return IsBuilding();
}

bool pgRscBuilder::IsBuildingLastPass() 
{
	return IsBuilding();
}

void pgRscBuilder::SetBuildName(const char* name)
{
	s_BuildName = name;
}

const char* pgRscBuilder::GetBuildName()
{
	return s_BuildName?s_BuildName:"<unknown>";
}

struct datAllocation
{
	u8 *SrcAddr, *DestAddr;
	u32 Size, AlignMask : 16, Bucket: 15, Touched : 1;
	u32 Offset;
	int AllocId;
};

struct Bucket
{
	u8 *Base;
	size_t Size, Offset;
	u32 Shift;
	u32 SplitAllocation;
	u32 AllocCount;
};

struct CompareAllocationBySourceAddr
{
	bool operator()( const datAllocation& a, const datAllocation& b ) const
	{
		return a.SrcAddr < b.SrcAddr;
	}
};

struct SearchAllocationBySourceAddr
{
	bool operator()( const datAllocation& a, const void *b) const
	{
		// Return true if source range 'a' excludes b
		return a.SrcAddr + a.Size <= b;
	}
};

struct CompareAllocationBySize 
{
	bool operator()( const datAllocation& a, const datAllocation& b ) const
	{
		return a.Size > b.Size || ((a.Size == b.Size) && (a.AlignMask > b.AlignMask));	// sort larger size first; break ties by larger alignment
	}
};

#if __OPTIMIZED
#define rscDebugf3(...)
#else
#define rscDebugf3(...) Debugf3(g_rscDebug,__VA_ARGS__)
#endif

class singlePassAllocator: public sysMemAllocator
{
public:
	singlePassAllocator(bool isVirtual,size_t heapSize,size_t leafSize,size_t nodeCount)
	{
		if (!nodeCount)
			nodeCount = heapSize / 256;

		m_bIsVirtual = isVirtual;
		m_HeapSize = heapSize;
		m_HeapOffset = 0;
		m_AllocCount = 0;
		m_AllocMax = nodeCount;
		m_LeafSize = leafSize;

		m_SrcHeapBase = (u8*)sysMemVirtualAllocate(heapSize);
		// We can have a resource heap that is very full that ends up "expanding" slightly due to internal fragmentation
		// after we assign things to buckets.  In order to prevent a crash, simply allocate 25% more space on the destination side to account for this.
		m_DestHeapBase = (u8*) sysMemVirtualAllocate((heapSize * 5) / 4);
#if !__OPTIMIZED
		memset(m_DestHeapBase, 0xFE, heapSize);		// fill with garbage
#endif
		m_Allocations = (datAllocation*) sysMemVirtualAllocate(nodeCount * sizeof(datAllocation));
		m_BucketCount = 0;
		(u32&)m_Sizes = 0;
	}

	~singlePassAllocator()
	{
		sysMemVirtualFree(m_Allocations);
		sysMemVirtualFree(m_DestHeapBase);
		if (m_SrcHeapBase)
			sysMemVirtualFree(m_SrcHeapBase);
	}

	datAllocation *GetAllocData(const void *ptr) const
	{
		datAllocation *end = m_Allocations + m_AllocCount;
		// for (size_t i=0; i<m_AllocCount-1; i++) Assert(m_Allocations[i].SrcAddr < m_Allocations[i+1].SrcAddr);
		datAllocation *result = std::lower_boundRSG(m_Allocations, end, ptr, SearchAllocationBySourceAddr());
		if (result == end)
			Quitf("GetAllocData(%p) failed!",ptr);
		Assertf(ptr >= result->SrcAddr && ptr < result->SrcAddr + result->Size,"%p != (%u/%u)%p,%p",ptr,result-m_Allocations,m_AllocCount,result->SrcAddr,result->SrcAddr+result->Size);
		return result;
	}

	void *Allocate(size_t size,size_t align,int /* heapIndex = 0 */)
	{
#if !__FINAL
		if (++sm_MemAllocId == sysMemAllocator::sm_BreakOnAlloc)
			__debugbreak();
#endif

		if (!size)
			++size;	// annoying, but otherwise we run the risk of breaking too much code.
		if (!align)
			align = 16;

		if (m_AllocCount == m_AllocMax)
			Quitf("Too many nodes in heap");

		datAllocation &d = m_Allocations[m_AllocCount++];

		// Round allocation up to alignment size
		size_t alignMask = align - 1;
		size = (size + alignMask) & ~alignMask;
		// Align the starting address to allocation appropriately
		// Note that we intentionally always allocate slop here (the +1) so we can flag attempts
		// to store a pointer past the end of a block, something that we didn't detect before and
		// in fact could fail on rare occasions at the end of a bucket.
		m_HeapOffset = (m_HeapOffset + alignMask + 1) & ~alignMask;
		d.SrcAddr = m_SrcHeapBase + m_HeapOffset;
		d.DestAddr = 0;
		d.Size = size;
		d.AlignMask = alignMask;
		d.Touched = false;
		d.AllocId = sm_MemAllocId;
		m_HeapOffset += size;
		m_LargestAllocation = 0;
		rscDebugf3("Alloc size %u align %u",d.Size,d.AlignMask);
		if (m_HeapOffset > m_HeapSize)
			Quitf("Construction heap overflow, need at least %u bytes",m_HeapOffset);

		return d.SrcAddr;
	}

	void Resize(const void * ptr,size_t newSmallerSize) 
	{
		// This is trivial in single-pass system because we wait as long
		// as possible to do any work!
		datAllocation *d = GetAllocData(ptr);
		d->Size = newSmallerSize;
	}

	void Free(const void *ptr)
	{
		AssertMsg(!ptr, "Attempting to free from a resource heap that is still being constructed.  Memory will be wasted within the resource heap.");
		if (ptr)
		{
			// Mark it touched to avoid a fatal error later
			datAllocation *d = GetAllocData(ptr);
			d->Touched = true;
		}
	}

	void* GetHeapBase() const { return g_ReadiedBuild? m_DestHeapBase : m_SrcHeapBase; }

	bool IsRootResourceAllocation() const { return m_AllocCount == 1; }

	size_t GetMemoryUsed(int) { return m_HeapOffset; }

	size_t GetMemoryAvailable() { return m_HeapSize - m_HeapOffset; }

	bool IsValidPointer(const void *ptr) const 
	{
		return ptr >= GetHeapBase() && ptr < (char*)GetHeapBase() + m_HeapOffset;
	}

	bool GetFinalOffset(u8 *ptr,size_t &off) const
	{
		if (ptr >= m_SrcHeapBase && ptr < m_SrcHeapBase + m_HeapOffset)
		{
			datAllocation *d = GetAllocData(ptr);
			if (ptr >= d->SrcAddr + d->Size)
				Quitf("Code is attempting to store a pointer past the end of a memory block");
			off = (ptr - d->SrcAddr) + (d->DestAddr - m_DestHeapBase);
			d->Touched = true;
			return true;
		}
		else
			return false;
	}

	void CopyHeap()
	{
		for (size_t i=0; i<m_AllocCount; i++)
		{
			if (m_Allocations[i].Touched)
			{
#if !__OPTIMIZED
				// Make sure there are no weird overlaps
				for (size_t j=0; j<m_Allocations[i].Size; j++)
					Assert(m_Allocations[i].DestAddr[j] == 0xFE);
#endif
				memcpy(m_Allocations[i].DestAddr, m_Allocations[i].SrcAddr, m_Allocations[i].Size);
				rscDebugf3("Copy %u byte allocation from %p to %p",m_Allocations[i].Size,m_Allocations[i].SrcAddr,m_Allocations[i].DestAddr);
			}
			else if (m_Allocations[i].Size)
				Quitf("Pointer in resource heap never got relocated, probably a leak @%p -breakonalloc=%d",m_Allocations[i].SrcAddr,m_Allocations[i].AllocId);
		}
		sysMemVirtualFree(m_SrcHeapBase);
		m_SrcHeapBase = NULL;
	}

	size_t GetLargestAllocation() const { return m_LargestAllocation; }

	// http://aggregate.org/MAGIC/
	static inline u32 CeilPow2(u32 x)
	{ 
		--x; x |= x >> 1; x |= x >> 2; x |= x >> 4; x |= x >> 8; x |= x >> 16; return x+1; 
	}

	// http://aggregate.org/MAGIC/
	static inline u32 CountOnes(u32 x)
	{
		x -= ((x >> 1) & 0x55555555);
		x = (((x >> 2) & 0x33333333) + (x & 0x33333333));
		x = (((x >> 4) + x) & 0x0f0f0f0f);
		x += (x >> 8);
		x += (x >> 16);
		return(x & 0x0000003f);
	}

	// http://aggregate.org/MAGIC/
	static inline u32 Log2ofPow2(u32 x)	// Input is a power of two
	{
		return CountOnes(x-1);
	}

	size_t PackIntoBuckets(size_t baseAlloc,size_t bucketLimit)
	{
		size_t result = 0;
		m_BucketCount = 0;

		static const u32 bucketMax[9] = { 
			(1<<RESOURCE_16N_BITS)-1, (1<<RESOURCE_8N_BITS)-1, (1<<RESOURCE_4N_BITS)-1, (1<<RESOURCE_2N_BITS)-1, 
			(1<<RESOURCE_N_BITS)-1, 1, 1, 1, 1 };

		memset(&m_BucketCounts[0],0,sizeof(m_BucketCounts));

		// Assign each allocation to a bucket
		for (size_t a=0; a<m_AllocCount; a++)
		{
			// Find the first bucket that will hold this allocation.  The buckets are always
			// allocated such that the current bucket is no larger than the previous one since
			// all allocations are sorted by size ahead of time.  We want to find the first
			// bucket that fits so that the last bucket tends to have the most space free.
			datAllocation &d = m_Allocations[a];
			size_t b = 0;
			for (b=0; b<m_BucketCount; b++)
			{
				size_t newOffset = ((m_Buckets[b].Offset + d.AlignMask) & ~d.AlignMask) + d.Size;
				if (newOffset <= m_Buckets[b].Size)
					break;
			}

			// Need to allocate a new bucket?
			if (b == m_BucketCount)
			{
				// If we've already used too many buckets, stop now.
				if (m_BucketCount == bucketLimit)
					return false;

				// First allocation is a special case, where we might have to make sure the virtual root
				// and the real largest allocation (including its alignment) both fit.  Fortunately we've
				// already done the work to figure that out.
				size_t bucketSize = !a? m_LargestAllocation : d.Size;
				
				// Compute the number of leaves we'll need for the bucket.  Note that since allocations
				// are sorted by descending size, a new bucket will never be larger than a previously
				// allocated bucket.
				size_t leafCount = (bucketSize + m_LeafSize - 1) / m_LeafSize;
				
				// Round the number of leaves up to a power of two since that's how we allocate bucket memory
				leafCount = CeilPow2(leafCount);

				// If the size of the bucket we're trying to add is over the limit for this size, stop now.
				if (leafCount < baseAlloc)
					leafCount = baseAlloc;

				// Figure out which size bucket to bump
				u32 countIdx = (Log2ofPow2(baseAlloc) - Log2ofPow2(leafCount)) + 4;
				// Too many buckets of this size, try again.
				if (m_BucketCounts[countIdx] == bucketMax[countIdx])
					return false;
				m_BucketCounts[countIdx]++;

				size_t newSize = leafCount * m_LeafSize;
				Assert(!m_BucketCount || m_Buckets[m_BucketCount-1].Size >= newSize);
				Bucket &newBucket = m_Buckets[m_BucketCount++];
				newBucket.Offset = 0;
				result += newSize;
				newBucket.Size = newSize;
				newBucket.Shift = countIdx;
				newBucket.SplitAllocation = ~0U;
				newBucket.AllocCount = 0;
			}
			d.Offset = ((m_Buckets[b].Offset + d.AlignMask) & ~d.AlignMask);
			rscDebugf3("Assign alloc %u to bucket %u offset %u size %u",a,b,d.Offset,d.Size);

			size_t halfSize = m_Buckets[b].Size >> 1;
			// If the bucket already had something in it, but it was no more than half full, and this allocation 
			// would make the bucket more than half full, remember its index because this is where we might
			// split the bucket in the future.
			if (m_Buckets[b].AllocCount && m_Buckets[b].Offset <= halfSize && d.Offset + d.Size > halfSize)
				m_Buckets[b].SplitAllocation = a;

			// Assign the current allocation to the current bucket.
			m_Buckets[b].Offset = d.Offset + d.Size;
			++m_Buckets[b].AllocCount;
			d.Bucket = b;
		}

		const u32 maxShift = (u32)m_BucketCounts.GetMaxCount()-1;
		for (;;) {
			u32 lastIdx = m_BucketCount-1;
			Bucket &lastBucket = m_Buckets[lastIdx];
			// If the last bucket has an allocation that spans the halfway point, and the bucket is less than three quarters full, 
			// and it's possible to make a bucket both half size *and* quarter size, and we're not already at the bucket limit...
			if (lastBucket.SplitAllocation != ~0U && lastBucket.Offset <= ((lastBucket.Size*3)/4) && lastBucket.Shift < maxShift-1 && baseAlloc>2 && m_BucketCount < bucketLimit)
			{
				size_t a, newOffset = 0, newCount = 0;
				size_t newSize = lastBucket.Size >> 2;
				// Scan the rest of the allocations, looking for ones in this bucket, and see if they will fit in a quarter-sized bucket.
				for (a=lastBucket.SplitAllocation; a<m_AllocCount; a++)
				{
					datAllocation &d = m_Allocations[a];
					if (d.Bucket == lastIdx)
					{
						++newCount;
						newOffset = ((newOffset + d.AlignMask) & ~d.AlignMask) + d.Size;
						if (newOffset > newSize)
							break;
					}
				}
				// Overflowed the potential new bucket, nothing else to do
				if (a != m_AllocCount)
					break;

				// Split is definitely possible... so go back over the allocations again and really do the work.
				newOffset = 0;
				u32 newSplit = ~0U;
				lastBucket.Offset = m_Allocations[lastBucket.SplitAllocation].Offset;
				for (a=lastBucket.SplitAllocation; a<m_AllocCount; a++)
				{
					datAllocation &d = m_Allocations[a];
					if (d.Bucket == lastIdx)
					{
						d.Bucket = lastIdx+1;
						// Recompute the split allocation for a future iteration
						d.Offset = (newOffset + d.AlignMask) & ~d.AlignMask;
						if (a!=lastBucket.SplitAllocation && newOffset <= (newSize>>1) && d.Offset + d.Size > (newSize>>1))
							newSplit = a;
						newOffset = d.Offset + d.Size;
					}
				}

				// Update the size and shift of the (newly-resized) previously last bucket.
				lastBucket.Size >>= 1;
				Assert(lastBucket.Offset <= lastBucket.Size);
				result -= lastBucket.Size >> 1;
				m_BucketCounts[lastBucket.Shift]--;
				lastBucket.Shift++;
				Assert(m_BucketCounts[lastBucket.Shift] == 0);
				m_BucketCounts[lastBucket.Shift]++;
				baseAlloc >>= 2;		// need to account for both half and quarter-sized buckets
				lastBucket.SplitAllocation = ~0U;	// not strictly necessary but get it right
				Assert(lastBucket.AllocCount > newCount);	// make sure we don't somehow underflow
				lastBucket.AllocCount -= newCount;	// not used, but might as well keep it correct

				// Fill in the details on the new last bucket.
				Bucket &newLast = m_Buckets[m_BucketCount++];
				Assert(newOffset <= newSize);
				newLast.Offset = newOffset;
				newLast.Size = newSize;
				newLast.Shift = lastBucket.Shift + 1;
				Assert(m_BucketCounts[newLast.Shift] == 0);
				m_BucketCounts[newLast.Shift]++;
				newLast.SplitAllocation = newSplit;
				newLast.AllocCount = newCount;
				// ...and loop back to the top and try the whole thing again!
			}
			else	// not possible to split any further
				break;
		}

		// While the last bucket is less than half full, cut it in half.
		Bucket &lastBucket = m_Buckets[m_BucketCount-1];
		while (lastBucket.Offset <= (lastBucket.Size>>1) && lastBucket.Shift < maxShift && baseAlloc>1)
		{
			lastBucket.Size >>= 1;
			result -= lastBucket.Size;
			m_BucketCounts[lastBucket.Shift]--;
			lastBucket.Shift++;
			Assert(m_BucketCounts[lastBucket.Shift] == 0);
			m_BucketCounts[lastBucket.Shift]++;
			baseAlloc >>= 1;
		}

		return result;
	}

	size_t ReadyBuild(size_t bucketLimit)
	{
		Assert(bucketLimit <= datResourceChunk::MAX_CHUNKS);

		if (!m_AllocCount)	// m_Sizes is already zero.
			return 0;

		// Sort all allocations by size (leaving root allocation, if any, intact)
		// Note that the root allocation is typically pretty small, but we generally never have
		// strict alignment on the virtual heap anyway (exception: virtual memory textures, which are
		// pretty rare and not worth complicating the implementation)
		std::sort(m_Allocations + m_bIsVirtual, m_Allocations + m_AllocCount, CompareAllocationBySize());

		// Find the largest allocation (virtual root is a special case, we need to make sure it and the
		// real largest allocation both are accounted for)
		size_t largestAllocBytes = (m_bIsVirtual && m_AllocCount > 1)
			? (((m_Allocations[0].Size + m_Allocations[1].AlignMask) & ~m_Allocations[1].AlignMask) + m_Allocations[1].Size) 
			: m_Allocations[0].Size;
		m_LargestAllocation = largestAllocBytes;
		size_t largestAlloc = CeilPow2((largestAllocBytes + m_LeafSize - 1) / m_LeafSize);
		// Pick a sane largest allocation size otherwise our base allocation size might underflow.
		// This also keeps things working when there is a very large number of small allocations.
		// Note that largestAlloc is always a power of two (not a shift count), so baseAlloc is as well.
		if (largestAlloc < 16)
			largestAlloc = 16;

		size_t baseAlloc = 0, used = 0;
		u32 iterationCount = 0;
		// Now determine a suitable base allocation.  Our initial guess will be one sixteenth of the largest
		// allocation since that's the largest "big page" we support.  We'll keep trying indefinitely with
		// larger bucket sizes, although each pass will likely incur increasingly more internal fragmentation.
		// We try >>4, >>3, >>2, >>1, >>0, <<1, <<2, etc, until we get a match.
		do {
			baseAlloc = (largestAlloc << iterationCount) >> 4;
			++iterationCount;
		} while ((used = PackIntoBuckets(baseAlloc,bucketLimit)) == 0);

		// At this point, baseAlloc is the integer multiple of the platform's root page size.  It is always
		// a power of two. Now that we know we have a working result, try a bit harder to reduce fragmentation, but never
		// inflate the bucket size above a nominal value (64k/83k base page size on PS3, 64k on 360).
		// Note that with a base base size of (say) 64k we can still have buckets up to 16 times that, 1024k.
		// PS3 has a 4k page size, other platforms use 8k, hence the different multipliers here.
		// These are the correct numbers (and the used2 < used should be <= if we want to maximize root page size) (for future reference)
		/// const size_t maxAlloc = (g_sysPlatform != platform::PS3)? 8 : 16;
		// These are the incorrect numbers that still kinda work (too late to change for gta5).  They favor 32k on 360 and 16k on PS3.
		const size_t maxAlloc = (g_sysPlatform != platform::PS3)? 3 : 4;
		while (baseAlloc < maxAlloc)
		{
			size_t used2;
			if ((used2=PackIntoBuckets(baseAlloc*2,bucketLimit))!=0 && used2 < used)
			{
				baseAlloc*=2;
				Debugf1(g_rscDebug,"%s %s: bucketSize %u, %u<%u",s_BuildName,m_bIsVirtual?"virtual":"physical",baseAlloc*m_LeafSize,used2,used);
				used = used2;
			}
			else // Redo last good run.
			{
				PackIntoBuckets(baseAlloc,bucketLimit);
				break;
			}
		}

		m_Sizes.LeafShift = Log2ofPow2(baseAlloc); Assert(m_Sizes.LeafShift == Log2ofPow2(baseAlloc));
		m_Sizes.Head16Count = m_BucketCounts[0]; Assert(m_Sizes.Head16Count == m_BucketCounts[0]);
		m_Sizes.Head8Count = m_BucketCounts[1]; Assert(m_Sizes.Head8Count == m_BucketCounts[1]);
		m_Sizes.Head4Count = m_BucketCounts[2]; Assert(m_Sizes.Head4Count == m_BucketCounts[2]);
		m_Sizes.Head2Count = m_BucketCounts[3]; Assert(m_Sizes.Head2Count == m_BucketCounts[3]);
		m_Sizes.BaseCount = m_BucketCounts[4]; Assert(m_Sizes.BaseCount == m_BucketCounts[4]);
		m_Sizes.HasTail2 = m_BucketCounts[5]; Assert(m_Sizes.HasTail2 == m_BucketCounts[5]);
		m_Sizes.HasTail4 = m_BucketCounts[6]; Assert(m_Sizes.HasTail4 == m_BucketCounts[6]);
		m_Sizes.HasTail8 = m_BucketCounts[7]; Assert(m_Sizes.HasTail8 == m_BucketCounts[7]);
		m_Sizes.HasTail16 = m_BucketCounts[8]; Assert(m_Sizes.HasTail16 == m_BucketCounts[8]);
		m_Sizes.Version = 0;
		// Make sure we didn't produce an impossible size progression
		Assert(m_Sizes.LeafShift > 0 || (!m_Sizes.HasTail2 && !m_Sizes.HasTail4 && !m_Sizes.HasTail8 && !m_Sizes.HasTail16));
		Assert(m_Sizes.LeafShift > 1 || (!m_Sizes.HasTail4 && !m_Sizes.HasTail8 && !m_Sizes.HasTail16));
		Assert(m_Sizes.LeafShift > 2 || (!m_Sizes.HasTail8 && !m_Sizes.HasTail16));
		Assert(m_Sizes.LeafShift > 3 || !m_Sizes.HasTail16);

		// Assign linear base addresses for each bucket
		u8 *next = m_DestHeapBase;
		for (size_t b=0; b<m_BucketCount; b++)
		{
			m_Buckets[b].Base = next;
			next += m_Buckets[b].Size;
		}
		// Make sure the addresses we allocated are the expected size.
		Assertf((size_t)(next - m_DestHeapBase) == m_Sizes.GetSize(m_LeafSize),"Bucket assignment - %d != %u",next - m_DestHeapBase,m_Sizes.GetSize(m_LeafSize));

		// Re-sort allocations back into source address order after they've been modified.
		std::sort(m_Allocations, m_Allocations + m_AllocCount, CompareAllocationBySourceAddr());

		// Assign final addresses to every allocation
		for (size_t a=0; a<m_AllocCount; a++)
		{
			datAllocation &d = m_Allocations[a];
			TrapGE(d.Bucket, m_BucketCount);
			d.DestAddr = m_Buckets[d.Bucket].Base + d.Offset;
			Assert(d.Offset + d.Size <= m_Buckets[d.Bucket].Size);
			rscDebugf3("Allocation %4u src %p dest %p(%u)",a,d.SrcAddr,d.DestAddr,d.Offset);
		}

		return m_BucketCount;
	}

	void DumpBuckets(const char *tag) const {
		size_t baseAlloc = 1 << m_Sizes.LeafShift;
		size_t offset = 0, size = 0;
		for (size_t b=0; b<m_BucketCount; b++)
		{
			Debugf1(g_rscDebug,"STAT: %s %s: Bucket %u size %u/%u (%u%% free)",tag,m_bIsVirtual?"virtual":"physical",b,m_Buckets[b].Offset,m_Buckets[b].Size,(m_Buckets[b].Size-m_Buckets[b].Offset)*100/m_Buckets[b].Size);
			size += m_Buckets[b].Size;
			offset += m_Buckets[b].Offset;
		}
		if (m_BucketCount)
			Debugf1(g_rscDebug,"STAT: %s %s: base bucket %u, %u/%u, %u%% fragmented",tag,m_bIsVirtual?"virtual":"physical",baseAlloc*m_LeafSize,offset,size,(size-offset)*100/size);
	}

	datResourceInfo::Sizes GetSizes() const { return m_Sizes; }

	u32 GetUnpackedSize() const {
		u32 unpackedSize = 0;
		if (m_BucketCount) {
			for (u32 i=0; i<m_BucketCount-1; i++)
				unpackedSize += m_Buckets[i].Size;
			unpackedSize += m_Buckets[m_BucketCount-1].Offset;
		}
		//Displayf("Unpacked size of %s is %u",s_BuildName,unpackedSize);
		return unpackedSize;
	}

private:
	u8 *m_SrcHeapBase, *m_DestHeapBase;
	size_t m_HeapSize, m_HeapOffset, m_AllocCount, m_AllocMax, m_LeafSize, m_LargestAllocation;
	datAllocation *m_Allocations;
	bool m_bIsVirtual;
	datResourceInfo::Sizes m_Sizes;
	atRangeArray<Bucket,datResourceChunk::MAX_CHUNKS> m_Buckets;
	atRangeArray<u32,9> m_BucketCounts;
	u32 m_BucketCount;

	static int sm_MemAllocId;
};

int singlePassAllocator::sm_MemAllocId = 1000000;

static singlePassAllocator *s_virtualAllocator, *s_physicalAllocator;

void datRelocatePointer(char *& ptr)
{
#if !__OPTIMIZED
	char *orig = ptr;
#endif
	size_t offset = 0;
	if (s_virtualAllocator->GetFinalOffset((u8*&)ptr,offset))
		ptr = (char*)datResourceFileHeader::c_FIXED_VIRTUAL_BASE + offset;
	else if (s_physicalAllocator->GetFinalOffset((u8*&)ptr,offset))
		ptr = (char*)datResourceFileHeader::c_FIXED_PHYSICAL_BASE + offset;
	else if (ptr)
		Quitf("Invalid pointer %p passed to datRelocatePointer",ptr);
	rscDebugf3("datRelocatePointer: %p -> %p",orig,ptr);
}

void pgRscBuilder::ConfigureBuild()
{
	if (g_sysPlatform == platform::PS3)
	{
		g_rscVirtualLeafSize = g_rscVirtualLeafSize_PS3;
		g_rscPhysicalLeafSize = g_rscPhysicalLeafSize_PS3;
	}
	else
	{
		g_rscVirtualLeafSize = g_rscVirtualLeafSize_WIN32;
		g_rscPhysicalLeafSize = g_rscPhysicalLeafSize_WIN32;
	}
}

void pgRscBuilder::BeginBuild(bool /*secondPass*/,int heapSize,size_t /*virtualChunkSize*/,size_t /*physicalChunkSize*/,int nodeCount) 
{
	PARAM_RSCDebugLevel.Get(g_rscDebug);

	ConfigureBuild();

	g_ReadiedBuild = false;
	s_SavedBuild = false;
	s_prevAllocator = &sysMemAllocator::GetCurrent();
	s_rscAllocator = rage_new rscAllocator;

	s_virtualAllocator = rage_new singlePassAllocator(true, heapSize, g_rscVirtualLeafSize, nodeCount);
	s_physicalAllocator = rage_new singlePassAllocator(false, heapSize, g_rscPhysicalLeafSize, nodeCount);
	s_rscAllocator->AddAllocator(*s_virtualAllocator);
	s_rscAllocator->AddAllocator(*s_virtualAllocator);
	s_rscAllocator->AddAllocator(*s_physicalAllocator);
	s_rscAllocator->AddAllocator(*s_physicalAllocator);

	sysMemAllocator::SetCurrent(*s_rscAllocator);
	sysMemAllocator::SetContainer(*s_rscAllocator);

	if (PARAM_savealloctrace.Get()) {
		if (s_AllocTrace)
			s_AllocTrace->Close();

		const char *name = NULL;
		PARAM_savealloctrace.GetParameter( name );

		if (name)
		{
			char path[256];
			fiAssetManager::RemoveNameFromPath(path, 256, name);

			char basename[256];
			fiAssetManager::RemoveExtensionFromPath(basename, 256, fiAssetManager::FileName(name));

			//add one because we have the "." in the returned char*
			const char* ext = fiAssetManager::FindExtensionInPath(name)+1;

			ASSET.PushFolder(path);
			s_AllocTrace = ASSET.Create(basename,ext);
			ASSET.PopFolder();
		}
		else
		{
			s_AllocTrace = ASSET.Create("beginBuild","txt");
		}
	}
}

void pgRscBuilder::ReadyBuild(pgBase* toplevelPointer) 
{
	g_ReadiedBuild = true;

	// Could easily be a runtime switch instead.
#if !__OPTIMIZED && 0
	pgBaseMetaDataDebugName *debugName = rage_new pgBaseMetaDataDebugName(s_BuildName);
	toplevelPointer->AttachMetaData(debugName);
#endif

	// Allocate the page map for its worst-possible size first so that we know we've reserved enough
	// address space for any possible final size.
	pgBasePageMap *pm = (pgBasePageMap*) s_virtualAllocator->Allocate(sizeof(pgBasePageMap) + sizeof(size_t) * datResourceChunk::MAX_CHUNKS, 16, 0);
	// Patch any existing data in now; pagemap is always first if it's present.
	pm->Next = toplevelPointer->m_FirstNode;
	toplevelPointer->m_FirstNode = pm;

	// Now shrink it to the smallest possible size so we can iterate on best fit.
	s_virtualAllocator->Resize(pm, sizeof(pgBasePageMap) + sizeof(size_t));

	size_t allocatedBuckets = 0;
	for (;;)
	{
		size_t virtualBuckets = 0, physicalBuckets = 0;

		// Virtual can't hog more than half the buckets, and physical gets whatever is left
		// unless of course there are no physical allocations.
		if (s_physicalAllocator->GetMemoryUsed(-1))
		{
			virtualBuckets = s_virtualAllocator->ReadyBuild(datResourceChunk::MAX_CHUNKS/2);
			physicalBuckets = s_physicalAllocator->ReadyBuild(datResourceChunk::MAX_CHUNKS - virtualBuckets);
			// If we didn't use many physical buckets and potentially could have used more virtual
			// buckets, repack the virtual memory again
			if (virtualBuckets >= datResourceChunk::MAX_CHUNKS/4 && physicalBuckets <= datResourceChunk::MAX_CHUNKS/4)
				virtualBuckets = s_virtualAllocator->ReadyBuild(datResourceChunk::MAX_CHUNKS - physicalBuckets);
		}
		else
			virtualBuckets = s_virtualAllocator->ReadyBuild(datResourceChunk::MAX_CHUNKS);

		// This shouldn't repeat more than once except in really rare situations where the base map
		// allocation caused another bucket to be created, but we will handle that properly.
		if (allocatedBuckets < (virtualBuckets + physicalBuckets))
		{
			allocatedBuckets = virtualBuckets + physicalBuckets;
			u32 baseMapSize = sizeof(pgBasePageMap) + sizeof(size_t) * allocatedBuckets;
			s_virtualAllocator->Resize(pm, baseMapSize);
			Assign(pm->VirtualCount,virtualBuckets);
			Assign(pm->PhysicalCount,physicalBuckets);
			Assign(pm->RootVirtualChunk,0);
			pm->Subtype = 0;
			pm->Pad = 0;
		}
		else
			break;
	}

	// Make sure the topmost heap address gets assigned a new address too
	// Note that DeclareStruct hasn't actually been called yet.
	datRelocatePointer((void*&)toplevelPointer);
}

void pgRscBuilder::EndBuild(pgBase * /*toplevelPointer*/) 
{
	s_SavedBuild = true;	// Should already be true anyway, just do this in case they didn't save for some reason

	g_LargestVirtualAllocation = s_virtualAllocator->GetLargestAllocation();
	g_LargestPhysicalAllocation = s_physicalAllocator->GetLargestAllocation();

	sysMemAllocator::SetCurrent(*s_prevAllocator);
	sysMemAllocator::SetContainer(*s_prevAllocator);
	s_prevAllocator = NULL;

	delete s_rscAllocator;
	s_rscAllocator = NULL;

	delete s_physicalAllocator;
	s_physicalAllocator = NULL;
	delete s_virtualAllocator;
	s_virtualAllocator = NULL;
}

void pgRscBuilder::GetSize( size_t& vSize, size_t & pSize )
{
	Assert(g_ReadiedBuild);	// not valid until this is set
	vSize = s_virtualAllocator->GetSizes().GetSize(g_rscVirtualLeafSize);
	pSize = s_physicalAllocator->GetSizes().GetSize(g_rscPhysicalLeafSize);
}

PARAM(uncompressedresources,"Emit uncompressed resources (like for viseme compiler, use with caution)");

bool pgRscBuilder::SaveBuild(const char *heapName,const char *heapExt,int version,size_t* size,bool encrypted) 
{
	if (!g_ReadiedBuild)
		Quitf("Called SaveBuild without calling ReadyBuild");

	if ((version & 255) == 0)
		Quitf("Version number (%d) modulo 256 cannot be zero.",version);

	s_SavedBuild = true;

	// Copy both resource heaps to final locations (all addresses already assigned during ReadyBuild)
	// We also free the source heaps at this time to catch stale references.
	s_virtualAllocator->CopyHeap();
	s_physicalAllocator->CopyHeap();

	char heapBuf[RAGE_MAX_PATH];
	ConstructName(heapBuf,sizeof(heapBuf),heapName,heapExt,true);
	Assert(version);

	if (s_AllocTrace) {
		s_AllocTrace->Close();
		s_AllocTrace = NULL;
		char srcFile[RAGE_MAX_PATH], destFile[RAGE_MAX_PATH];

		const char *name = NULL;
		PARAM_savealloctrace.GetParameter( name );
		if (name)
		{
			char basename[256];
			fiAssetManager::RemoveExtensionFromPath(basename, 256, fiAssetManager::FileName(name));

			//add one because we have the "." in the returned char*
			const char* ext = fiAssetManager::FindExtensionInPath(name)+1;

			ASSET.FullWritePath(srcFile,sizeof(srcFile),basename,ext);
		}
		else
		{
			ASSET.FullWritePath(srcFile,sizeof(srcFile),"beginBuild","txt");
		}
		strcpy(destFile,heapBuf);
		strcat(destFile,".txt");
		if (!fiDevice::CopySingleFile(srcFile, destFile))
			Warningf("Unable to copy trace file from %s to %s",srcFile,destFile);
	}

	s_virtualAllocator->DumpBuckets(ASSET.FileName(heapBuf));
	s_physicalAllocator->DumpBuckets(ASSET.FileName(heapBuf));

	// size_t virtualTop = s_virtualAllocator->GetSizes().GetSize(g_rscVirtualLeafSize);
	// size_t physicalTop = s_physicalAllocator->GetSizes().GetSize(g_rscPhysicalLeafSize);
	// Debugf1(g_rscDebug,"%s: Virtual %uk, physical %uk",heapName,virtualTop>>10,physicalTop>>10);

	datResourceFileHeader hdr;
	memset(&hdr,0,sizeof(hdr));

	u32 headerSize = sizeof(datResourceFileHeader);
	hdr.Magic = datResourceFileHeader::c_MAGIC;
	hdr.Version = version;
	hdr.Info.Virtual = s_virtualAllocator->GetSizes();
	hdr.Info.Physical = s_physicalAllocator->GetSizes();
	
	sysMemStartTemp();

	u8 *dest = NULL;
	u32 compSize, origSize, unpackedVirtSize = s_virtualAllocator->GetUnpackedSize();

	size_t totalSize = (hdr.Info.GetVirtualSize() + hdr.Info.GetPhysicalSize()) * 2;
	if (totalSize < 4096)
		totalSize = 4096;	// guarantee a certain minimum working size in case the file is very small.
	dest = (u8*) sysMemVirtualAllocate(totalSize);
#if __64BIT
	const bool useZlib = true;
#else
	bool useZlib = g_sysPlatform != platform::XENON || PARAM_fastCompression.Get();
#endif

	if (useZlib) {
		z_stream c_stream;
		memset(&c_stream,0,sizeof(c_stream));

		int compressionQuality = PARAM_fastCompression.Get()? Z_BEST_SPEED : Z_BEST_COMPRESSION;
		if (deflateInit2(&c_stream, compressionQuality, Z_DEFLATED,-MAX_WBITS,MAX_MEM_LEVEL,Z_DEFAULT_STRATEGY) < 0)
			Quitf("Error in deflateInit");
		c_stream.next_in = (Bytef*)s_virtualAllocator->GetHeapBase();
		c_stream.avail_in = (uInt) hdr.Info.GetVirtualSize();
		c_stream.next_out = dest;
		c_stream.avail_out = (uInt) totalSize;
		if (deflate(&c_stream, Z_NO_FLUSH) < 0)
			Quitf("Error in deflate (virtual)");
		if (c_stream.avail_in)
			Quitf("deflate didn't consume all input (virtual)?");
		c_stream.next_in = (Bytef*)s_physicalAllocator->GetHeapBase();
		c_stream.avail_in = (uInt) hdr.Info.GetPhysicalSize();
		if (hdr.Info.GetPhysicalSize()) {
			if (deflate(&c_stream, Z_NO_FLUSH) < 0)
				Quitf("Error in deflate (physical)");
		}
		if (c_stream.avail_in)
			Quitf("deflate didn't consume all input (physical)?");

		if (deflate(&c_stream, Z_FINISH) < 0)
			Quitf("Error in final deflate");

		deflateEnd(&c_stream);

		compSize = (u32) (totalSize - c_stream.avail_out);
		origSize = hdr.Info.GetVirtualSize()+hdr.Info.GetPhysicalSize();
		if (origSize<100) origSize = 100;
		Debugf2(g_rscDebug,"Compressed to %d%% of its original size",(int)(compSize/(origSize/100)));
	}
#if !__64BIT
	else {
		char *tempBuild = (char*) sysMemVirtualAllocate(hdr.Info.GetVirtualSize() + hdr.Info.GetPhysicalSize());
		memcpy(tempBuild, s_virtualAllocator->GetHeapBase(),hdr.Info.GetVirtualSize());
		memcpy(tempBuild + hdr.Info.GetVirtualSize(),s_physicalAllocator->GetHeapBase(),hdr.Info.GetPhysicalSize());

		XMEMCOMPRESSION_CONTEXT ctxt = NULL;
		XMEMCODEC_PARAMETERS_LZX params = XCOMPRESS_SETTINGS;
		if (FAILED(XMemCreateCompressionContext(XMEMCODEC_LZX, &params, XMEMCOMPRESS_STREAM, &ctxt)))
			Quitf("CreateCompressionContext failed");

		SIZE_T inSize = hdr.Info.GetVirtualSize() + hdr.Info.GetPhysicalSize();
		SIZE_T outSize = totalSize;
		if (FAILED(XMemCompress(ctxt,dest,&outSize,tempBuild,inSize)))
			Quitf("XMemCompress failed.");
		compSize = outSize;

		XMemDestroyCompressionContext(ctxt);

		sysMemVirtualFree(tempBuild);

		origSize = hdr.Info.GetVirtualSize()+hdr.Info.GetPhysicalSize();
		if (origSize<100) origSize = 100;
		Debugf2(g_rscDebug,"** XCompressed to %d%% of its original size",(int)compSize/(origSize/100));
	}
#endif

	// Remember the actual sizes before we potentially destroy them during byte swapping.
	hdr.Info.SetVersion(hdr.Version);
	datSwapper(hdr.Magic);
	datSwapper(hdr.Version);
	datSwapper((u32&)hdr.Info.Virtual);
	datSwapper((u32&)hdr.Info.Physical);

	// Save out actual data (clearing read-only bit automatically)
	ASSET.CreateLeadingPath(heapBuf);

	ASSET.ClearAttributes(heapBuf,"",FILE_ATTRIBUTE_READONLY);
	fiStream *S = fiStream::Create(heapBuf);

	size_t sizeTally = 0;
	if (S) {
		// Dump the heap (compressed if it was an improvement)
		sizeTally = S->Write(&hdr,headerSize);
		if (encrypted) {
			AES aes;
#if RSG_CPU_X64
			if (g_sysPlatform == platform::WIN64PC || g_sysPlatform == platform::ORBIS || g_sysPlatform == platform::DURANGO)
			{
				unsigned int transformitSelector = 0;
				transformitSelector = atStringHash(ASSET.FileName(heapBuf));
				if (PARAM_uncompressedresources.Get()) 
					transformitSelector += headerSize+unpackedVirtSize;
				else
					transformitSelector += headerSize+compSize;
				transformitSelector = transformitSelector % TFIT_NUM_KEYS;
				aes.Encrypt(aes.GetKeyId(), transformitSelector, dest, compSize);
			}
			else
#endif
			{
				aes.Encrypt(dest,compSize);
			}
		}
		if (PARAM_uncompressedresources.Get()) {
			sizeTally += S->Write(s_virtualAllocator->GetHeapBase(),unpackedVirtSize);
			Assert(sizeTally == headerSize + unpackedVirtSize);
			/*
			sizeTally += S->Write(s_virtualAllocator->GetHeapBase(),hdr.Info.GetVirtualSize());
			sizeTally += S->Write(s_physicalAllocator->GetHeapBase(),hdr.Info.GetPhysicalSize());
			*/
		}
		else {
			sizeTally += S->Write(dest,compSize);
			Assert(sizeTally == headerSize + compSize);
		}
		S->Close();

		if (size)
			*size = sizeTally;

		sysMemVirtualFree(dest);
		sysMemEndTemp();

		return true;
	}
	else {
		sysMemVirtualFree(dest);
		sysMemEndTemp();

		if (size)
			*size = 0;

		Errorf("Unable to create resource heap '%s'",heapName);
		return false;
	}
}

#endif // __RESOURCECOMPILER

}			// namespace rage
