//
// paging/streamer.cpp
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//

#include "streamer.h"
#include "streamer_internal.h"
#include "paging_channel.h"

#include "atl/queue.h"
#include "data/aes.h"
#include "data/marker.h"
#include "diag/output.h"
#include "diag/tracker.h"
#include "file/asset.h"
#if __XENON
#include "file/device_installer.h"
#endif
#include "file/diskcache.h"
#include "file/packfile.h"
#include "fwmaths/random.h"
#include "grprofile/pix.h"
#include "math/amath.h"
#include "math/intrinsics.h"
#include "profile/cputrace.h"
#include "system/bootmgr.h"
#include "system/cache.h"
#include "system/ipc.h"
#include "system/interlocked.h"
#include "system/timemgr.h"
#include "system/param.h"
#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/platform.h"
#include "string/string.h"
#include "data/resourceheader.h"
#include "zlib/inflateClient.h"

#if !__FINAL
#include "system/stack.h"
#endif

#include <cstdlib>


#if __PS3
#include <sys/sys_fixed_addr.h>
#include <sn/libsntuner.h>
#endif

#if RSG_ORBIS
#include <perf.h>
#endif

RAGE_DEFINE_CHANNEL(paging)

PAGING_OPTIMISATIONS()

extern __THREAD int RAGE_LOG_DISABLE;


// DO NOT ENABLE THIS AND COMMIT!
#define STREAMER_TUNER_PROFILING	(0 && RSG_ORBIS)
#define ENABLE_RAW_STREAMER			(1)

#if STREAMER_TUNER_PROFILING
	#define STREAMER_START_MARKER(_fmt, ...)			\
		do {											\
			char str[128];								\
			formatf(str, _fmt, ##__VA_ARGS__);			\
			sceRazorCpuPushMarker(str, 					\
				SCE_RAZOR_COLOR_GREEN, 					\
				SCE_RAZOR_MARKER_DISABLE_HUD);			\
		} while (false)

	#define STREAMER_END_MARKER()		sceRazorCpuPopMarker()

#else // STREAMER_TUNER_PROFILING
#define STREAMER_START_MARKER(_fmt, ...)
#define STREAMER_END_MARKER()
#endif // STREAMER_TUNER_PROFILING


namespace rage {

#if __PS3
	extern bool g_IsExiting;
#endif

extern const int MaxPackfiles;

#if __BANK
u32 pgGenerationedIndex::sm_NextGeneration;
#endif // __BANK


static bool IsValidHandle(pgStreamer::Handle handle,const char *OUTPUT_ONLY(func)) {
	if (handle == pgStreamer::Error) {
		Warningf("pgStreamer::%s - Ignoring invalid Error handle.",func);
		return false;
	}

	// Keep the 128+32 in sync with MaxPackfiles in packfile.cpp ... sigh.
	bool valid = (fiCollection::GetCollectionIndex(handle) < MaxPackfiles) && fiCollection::GetCollection(handle);
#if __ASSERT
	Assertf(valid,"pgStreamer::%s - Invalid handle %x.",func,handle);
#else
	if (!valid) {
		Errorf("pgStreamer::%s - Invalid handle %x.",func,handle);
		NOTFINAL_ONLY(sysStack::PrintStackTrace());
	}
#endif
	return valid;
}

struct pgStreamerChunk {
	void *DestAddr;
	size_t Size;
};

struct pgStreamerRequest {
	volatile int m_ChunkCount;
	pgStreamer::Handle m_Handle;
	u32 m_Offset;
	u32 m_Encrypted;
	u32 m_Size;
	u32 m_nameHash;

#if __BANK
	pgGenerationedIndex m_GenIndex;
#endif // __BANK

	float m_Priority;
	u32 m_Requested;
	u8 m_QueueId;
	bool m_Uncached, m_Critical, pad1;

	pgStreamerCallback m_Callback;
	void *m_UserArg;
	void *m_DestAddr;
	u32 m_CancelKey;
	int m_RequestIndex;
	size_t m_DataLeft;

	const fiCollection *m_Device;

#if PGSTREAMER_DEBUG
	bool m_bPushedStartMarker;
	char m_DebugName[128];
	u32 m_StartTime;
	u32 m_DeflateTime;
	u32 m_ReadTime;
	size_t m_ReadSize;
#endif // PGSTREAMER_DEBUG

	pgStreamerChunk m_Chunks[datResourceChunk::MAX_CHUNKS]; // 128*8 == 1K
};

struct pgProcessJob {
	enum {
		DECOMPRESS,
		COPY_VRAM,
		FINALIZE,
		DUMMY,
	} m_Type;

	pgStreamerRequest *m_Request;

	union {
		// For DECOMPRESS
		struct {
			size_t m_CurrentOffset;			// Offset in the current chunk
			size_t m_FirstReadOffset;
			size_t m_CompressedSize;
			size_t m_UncompressedSize;
			int m_CurrentChunk;				// Current chunk inside the output map
			int m_ChunksInBatch;			// Number of chunks currently being decompressed
			zlibInflateState m_Zstream;
			bool m_Compressed;
			bool m_FirstCall;

		} m_Decompress;

		// For COPY_VRAM
		struct {
			size_t m_Size;					// Number of bytes to DMA
			void *m_TargetData;				// Target address on PPU

		} m_CopyVram;
	};
};



static sysCriticalSectionToken s_StreamerToken, s_CallbackToken;

#if !__FINAL
PARAM(singlequeue,"[Streamer] Force a single streaming work queue (should be slower, but guarantees requests always arrive in order issued)");
PARAM(logstreaming,"[Streamer] Log all file access to a file (with names and offsets)");
PARAM(slowstreaming,"[Streamer] Artificially slow all streaming down; lower is slower (1=glacial, 13=blu-ray, 14=xbox dvd)");
int pgStreamingSlowdown;
fiStream *pgStreamingLog;
sysCriticalSectionToken pgStreamingLogToken;
#endif

static void StreamerYield()
{
	_mm_pause();
}

void pgReadData::Init(sysIpcPriority priority,int cpu,const char *title, int selfIndex, int readBuffers) 
{
	m_Start = sysIpcCreateSema(false); 
	m_Finish = sysIpcCreateSema(false); 

	m_ReadRequests = rage_new pgReadRequest[readBuffers];
	m_ReadBuffers = rage_new char *[readBuffers];
	m_ReadCount = rage_new int[readBuffers];
	m_ReadBufferAllocations.Init();
	Assert( (u32)readBuffers <= m_ReadBufferAllocations.GetSize() );
	//Pre-allocate unused space
	for(u32 i=0; i<m_ReadBufferAllocations.GetSize()-readBuffers; ++i)
	{
		m_ReadBufferAllocations.Allocate();
	}

	for (int x=0; x<readBuffers; x++)
	{
		m_ReadBuffers[x] = rage_aligned_new(128) char[MAX_READ];
		m_ReadCount[x] = 0;
	}
	m_NextToStream = 0;
	m_ChunksAvailable = 0;

#if PGSTREAMER_DEBUG
	m_SelfIndex = (u32) selfIndex;
	m_MinSlotsAvailable = readBuffers;
	m_AvgSlotsAvailable = (float) readBuffers;
	m_CurrentDecompBuffersUsed = 0;
#endif // PGSTREAMER_DEBUG

	m_SlotsAvailable = readBuffers;
	m_TotalSlotCount = readBuffers;
	m_HasActiveChunk = false;

	const bool bIsOptical = (selfIndex == pgStreamer::OPTICAL);
	m_Thread = sysIpcCreateThread(Worker,this,sysIpcMinThreadStackSize,priority,title,cpu, bIsOptical ? "RageDvdReader" : "RageHddReader");

	sysMemSet(m_ReadRequests, 0, sizeof(pgReadRequest) * readBuffers);

	(void) selfIndex;
}

#if !RSG_FINAL && RSG_PC
void pgReadData::Request(const fiDevice *device,fiHandle handle,u64 offset,int count,u32 sortkey,bool uncached, u32 STREAMDEBUG_ONLY(cancelKey), u32 STREAMDEBUG_ONLY(selfIndex), const char *STREAMDEBUG_ONLY(debugName), bool closeWhenFinished, char *destAddr, const u32 encryption, const bool head) {
#else
void pgReadData::Request(const fiDevice *device,fiHandle handle,u64 offset,int count,u32 sortkey,bool uncached, u32 STREAMDEBUG_ONLY(cancelKey), u32 STREAMDEBUG_ONLY(selfIndex), const char *STREAMDEBUG_ONLY(debugName), bool closeWhenFinished, char *destAddr) {
#endif
	Assert(count <= MAX_READ || destAddr);
	Assert(m_SlotsAvailable > 0);
	sysInterlockedDecrement( (u32 *)&m_SlotsAvailable );

#if PGSTREAMER_DEBUG
	m_MinSlotsAvailable = Min(m_MinSlotsAvailable, m_SlotsAvailable);
	m_AvgSlotsAvailable = m_AvgSlotsAvailable * 0.95f + (float) m_SlotsAvailable * 0.05f;
#endif // PGSTREAMER_DEBUG

	int requestIndex;
	{
		SYS_CS_SYNC( s_StreamerToken );
		requestIndex = m_NextToStream;
		++m_NextToStream %= m_TotalSlotCount;
	}
	pgReadRequest &r = m_ReadRequests[requestIndex];

#if !__FINAL
	pagingDebugf2("Placing request into slot %d - offset %" I64FMT "d, count %d, readId %d, close=%d", requestIndex, offset, count, cancelKey, (int) closeWhenFinished);
#endif // !__FINAL

	Assert(r.m_Device == NULL);		// There's already a request here
	Assert((s64) offset >= 0);

	r.m_Device = device;
	r.m_Handle = handle;
	r.m_Offset = offset;
	r.m_Count = count;
	r.m_Uncached = uncached;
	r.m_SortKey = sortkey;
	r.m_CloseWhenFinished = closeWhenFinished;
	r.m_Dest = destAddr;
#if RSG_PC && !RSG_FINAL
	r.m_Encrypted = encryption; //temporary debug for 2352375
	r.m_head = head;			//temporary debug for 2352375	
#endif
#if PGSTREAMER_DEBUG
	r.m_readId = cancelKey;
	r.m_SelfIndex = selfIndex;
	if( debugName )
	{
		safecpy( r.m_DebugName, debugName );
	}

	if (Unlikely(!destAddr && count>MAX_READ)) {
		Quitf("Read request \"%s\" is too large (0x%08x bytes).  Must either specify a destination address or be no larger than 0x%08x bytes", debugName, count, MAX_READ);
	}
#endif // PGSTREAMER_DEBUG

	// Displayf("Request(%x) of %d bytes",this,count);

	Assert( m_ReadCount[requestIndex] == 0 );	// Writing into something still being read from

	// Comment this marker out if you want to measure latency between the signal and
	// the reader thread picking it up.
	//STREAMER_START_MARKER("Dev %d SGL - %dKB in %d", m_SelfIndex, count / 1024, m_Bounce);
	m_BeingProcessedByStreamer.Push( requestIndex );
	sysIpcSignalSema(m_Start);
	//STREAMER_END_MARKER();
}

bool pgReadData::HasDataAvailable() {
	return m_ChunksAvailable != 0;
}

int pgReadData::ProcessChunk() {
	int ret = -1;
	if (!m_HasActiveChunk) {
		pagingDebugf3("Not currently processing a chunk, wait for data to become available");

		sysIpcWaitSema(m_Finish);
		m_FinishedBufferIndex.Pop( ret );
		Assert(m_ChunksAvailable > 0);
		pagingDebugf3("Next chunk is ready, %d (%s)", ret, m_ReadRequests[ret].m_DebugName);
		m_HasActiveChunk = true;
	}
	return ret;
}

// PURPOSE: Returns the least recent chunk that was read.
char *pgReadData::GetActiveChunk(int requestIndex) {
	FatalAssert(m_HasActiveChunk);
	FatalAssertf( requestIndex >= 0, "requestIndex is negative, that is not correct, nor good" );
	pgReadRequest &r = m_ReadRequests[requestIndex];
	FatalAssertf( r.m_BufferIndex >= 0, "r.m_BufferIndex is negative, that is not correct, nor good" );
	FatalAssert( m_ReadCount[requestIndex] != 0 );	// There is no last free chunk
	pagingDebugf3( "Accessing read buffer for slot %d (size %d)", requestIndex, m_ReadCount[requestIndex] );
	return m_ReadBuffers[r.m_BufferIndex];
}

void pgReadData::VerifyCurrentReadId(int ASSERT_ONLY(requestIndex), u32 ASSERT_ONLY(readId)) {
	FatalAssert(m_HasActiveChunk);
	FatalAssert(m_ReadCount[requestIndex] != 0);	// There is no last free chunk
	ASSERT_ONLY((void)readId);
	PGDEBUG_ONLY( FatalAssertf( m_ReadRequests[requestIndex].m_readId == readId, "Expected readId %d at slot %d, got %d", readId, requestIndex, m_ReadRequests[requestIndex].m_readId ) );
}


// PURPOSE: Amount of data read in the least recent chunk.
int pgReadData::GetActiveChunkSize(int requestIndex) {
	if (!m_HasActiveChunk) {
		return 0;
	}

	FatalAssertf( requestIndex >= 0, "requestIndex is negative, that is not correct, nor good" );
	FatalAssert(m_ReadCount[requestIndex] != 0);	// There is no last free chunk
	return m_ReadCount[requestIndex];
}

// PURPOSE: Release interest in the least recent chunk of data read so the buffer can be recycled
// and used for new data. It's safe to call this function when there was no active chunk (i.e. when ProcessChunk
// hasn't been called) - the function will simply return in that case.
void pgReadData::FreeActiveChunk(int requestIndex) {
	if (m_HasActiveChunk) {
		if( requestIndex < 0 )
		{
			Quitf( ERR_STR_FAILURE_9, "requestIndex is negative." );
		}
		FatalAssert( m_ReadCount[requestIndex] != 0 );	// There is no last free chunk
		m_ReadCount[requestIndex] = 0;
		pgReadRequest &r = m_ReadRequests[requestIndex];
		if( r.m_BufferIndex >= 0 )
		{
			ReleaseBuffer( r.m_BufferIndex );
			r.m_BufferIndex = -1;
		}
		sysInterlockedDecrement(&m_ChunksAvailable);
		sysInterlockedIncrement( (u32 *)&m_SlotsAvailable );
		m_HasActiveChunk = false;


		pagingDebugf3("Freeing up last %s, new oldest=%d, chunks available=%d", r.m_DebugName, requestIndex, m_ChunksAvailable);
	} else {
		pagingDebugf3("Had been processing empty chunk");
	}
}

#if !__FINAL
PARAM(emulateoptical,"Emulate optical drive behavior (default is current platform, or use ps3 or xenon to override)");
enum { NONE, PS3, XENON } s_EmulationMode;
#endif

__THREAD bool tls_Uncached;

// Helper function which tracks emulated LSN and can simulate slow reads.
#if __FINAL
static int pgReadBulk(const fiDevice *device,fiHandle handle,u64 offset,void *dest,int count,bool uncached,u32 /*sortkey*/)
#else
static int pgReadBulk(const fiDevice *device,fiHandle handle,u64 offset,void *dest,int count,bool uncached,u32 sortkey,bool critical = false,u32 requestTime = 0,const char* rpfname = "",const char *filename = "") 
#endif
{
	int result;
	tls_Uncached = uncached;
#if !__FINAL
	// static u32 readLsn;
	if (pgStreamingLog) {
		u32 start = sysTimer::GetSystemMsTime();
		result = device->ReadBulk(handle,offset,dest,count);

		if (requestTime) {
			u32 end = sysTimer::GetSystemMsTime();
			SYS_CS_SYNC(pgStreamingLogToken);
			fprintf(pgStreamingLog,"%9u,prequest,%9u,%5u,%5u,%s,%s,%5u,%s\r\n",start,sortkey + u32(offset >> 11),(count + 2047) >> 11,end - start,rpfname,filename,end - requestTime,end - requestTime > (critical? 700U : 1500U)? "LATE" : "");
		}
	}
	else if (s_EmulationMode == PS3 && !(sortkey & fiDevice::HARDDRIVE_LSN)) {
		static u32 readLsn;
		u32 lsn = sortkey + u32(offset >> 11);
		u32 delta = lsn > readLsn? lsn - readLsn : readLsn - lsn;
		readLsn = sortkey + u32((offset + count) >> 11);
		// These are PS3 numbers:
		// For seek distances below 250, the time seems to be about N/5 in milliseconds.
		// Above 250, about 50ms up until 100,000 sectors, where it tops out at 75ms.  Roughly (N / 4000) + 50.
		// Beyond that, it ramps up linearly from there to about 300ms at 3,000,000 sectors.  Roughly (N / 14000) + 70;
		u32 delay = delta <= 250? delta / 5 : delta <= 100000? ((delta / 4000) + 50) : ((delta / 14000) + 70);
		// We can read about 15 sectors per millisecond.  We approximate this by 16.
		delay += (count >> (11 + 4));
		// Sanity checking clamp.
		if (delay > 350)
			delay = 350;
		u32 actual = sysTimer::GetSystemMsTime();
		result = device->ReadBulk(handle,offset,dest,count);
		actual = sysTimer::GetSystemMsTime() - actual;
		if (actual < delay)
			sysIpcSleep(delay - actual);
	}
	else if (s_EmulationMode == XENON && !(sortkey & fiDevice::HARDDRIVE_LSN)) {
		// Rough approximation of behavior on XENON, based on "Streamlining Game Loading" in docs.
		static u32 readLsn;
		const u32 layer1 = 1783936;
		u32 readLayer = readLsn >= layer1? 1 : 0;
		u32 lsn = sortkey + u32(offset >> 11);
		u32 layer = (lsn >= layer1? 1 : 0);
		u32 delta;
		u32 delay = 0;
		if (readLayer != layer) {
			// overhead for the layer switch
			delay = 75;
			// lsns increase from the outer rim to the inner rim on layer1, opposite of layer0.
			delta = lsn > readLsn? (layer1 + layer1 - lsn) - readLsn : (layer1 + layer1 - readLsn) - lsn;
		}
		else
			delta = lsn > readLsn? lsn - readLsn : readLsn - lsn;
		delay += delta / (layer1 / 200);
		// Average seek (1/3rd stroke) time 110 ms typical, 140 ms maximum 
		// Full stroke seek time 180 ms typical, 240 ms maximum 
		// Inner diameter thruput is about 6.8M/sec, outer diameter, 15M/sec.  Should account for that.
		// Although inner thruput seems like about 3 sectors per millisecond.
		delay += (count >> (11 + 4));		// this is totally wrong
		// Sanity checking clamp.
		if (delay > 300)
			delay = 300;
		u32 actual = sysTimer::GetSystemMsTime();
		result = device->ReadBulk(handle,offset,dest,count);
		actual = sysTimer::GetSystemMsTime() - actual;
		if (actual < delay)
			sysIpcSleep(delay - actual);
		readLsn = lsn;
	}
	else
#endif
	{
		result = device->ReadBulk(handle,offset,dest,count);
		NOTFINAL_ONLY(if (pgStreamingSlowdown) sysIpcSleep(count >> pgStreamingSlowdown));
	}
	tls_Uncached = false;
	return result;
}

#if !__RESOURCECOMPILER && !__TOOL
extern u32 GetStrIndexFromHandle(const pgStreamer::Handle & handle);
u32 CrashStreamer(pgReadData::pgReadRequest &r);
#endif	//	!__RESOURCECOMPILER && !__TOOL

#if __BANK
bool pgStreamer::force_streamer_crash = false;
#endif
sysObfuscated<int> pgStreamer::sm_tamperCrash;

bool pgReadData::ProcessRequest() {
	if (!sysIpcPollSema(m_Start))
	{
#if PGSTREAMER_DEBUG
		if (pgStreamer::sm_IdleCallback)
			pgStreamer::sm_IdleCallback(m_SelfIndex);
#endif // PGSTREAMER_DEBUG

		sysIpcWaitSema(m_Start);
	}

	// Pick up the next request.
	int requestIndex;
	m_BeingProcessedByStreamer.Pop( requestIndex );
	pgReadRequest &r = m_ReadRequests[requestIndex];

	// If everything is 0, this is a dummy request from the disk cache.
	if (!r.m_Device && !r.m_Handle && !r.m_Offset && !r.m_Count)
	{
		Assert(false);
		return false;
	}

	PIXBegin(0, "Reader_ProcessRequest");

#if PGSTREAMER_DEBUG
	u32 readId = r.m_readId;
	const char *debugName = r.m_DebugName;
	if (pgStreamer::GetReadBulkCallback())
		pgStreamer::GetReadBulkCallback()(r.m_Offset, r.m_Count, r.m_SelfIndex, readId);
#elif RSG_PC || !__NO_OUTPUT // PGSTREAMER_DEBUG
	u32 readId = 0;
	const char *debugName = "";
#endif // PGSTREAMER_DEBUG

	void *destAddr = r.m_Dest;
	if( !destAddr )
	{
		int bufferBits = -1;
		//Wait for a decomp buffer to become free
		NOTFINAL_ONLY( bool waited = false; )
		do
		{
			bufferBits = GetAvailableBuffer();
			if( bufferBits < 0 )
			{
				//NB in gen8 the amount of read requests matches the amount of decomp buffers, so this isn't hit
				#if !RSG_FINAL
					if( !waited )
					{
						waited = true;
						sysInterlockedIncrement((u32*)&pgStreamer::sm_WaitingForBuffer);
					}
				#endif
				StreamerYield();
			}
		} while( bufferBits == -1 );
		#if !RSG_FINAL
			if( waited )
			{
				sysInterlockedDecrement((u32*)&pgStreamer::sm_WaitingForBuffer);
			}
		#endif

		destAddr = GetBuffer( bufferBits );
		r.m_Dest = (char *)destAddr;
		r.m_BufferIndex = bufferBits;
	}
	else
	{
		r.m_BufferIndex = -1;
	}

#if !__FINAL
	pagingDebugf2("Processing request - streaming offset %" I64FMT "d, %d bytes into buffer slot %d (%p) - readId=%d", r.m_Offset,
		r.m_Count, r.m_BufferIndex, destAddr, r.m_readId);
#endif // !__FINAL

	STREAMER_START_MARKER("ReadBulk %dKB (Dev %d) %s", r.m_Count / 1024, r.m_SelfIndex, r.m_DebugName);
	int count = pgReadBulk(r.m_Device,r.m_Handle,r.m_Offset,destAddr,r.m_Count,r.m_Uncached,r.m_SortKey);
	if (count != r.m_Count) {
		//try again
		count = pgReadBulk(r.m_Device,r.m_Handle,r.m_Offset,destAddr,r.m_Count,r.m_Uncached,r.m_SortKey);
	}
	STREAMER_END_MARKER();

#if !RSG_FINAL && RSG_PC
	if(!r.m_Encrypted && r.m_head)
	{
		char * pData = (char*)destAddr;
		if(pData[0] == 0 && pData[1] == 0)
		{
			//Quitf(0, "first two blocks are zero, see bugstar 2352375");
		}
	}
#endif

	if (count != r.m_Count BANK_ONLY(|| pgStreamer::force_streamer_crash)) {
#if __XENON
        bool onHdd = (r.m_SortKey & fiDevice::HARDDRIVE_LSN) != 0;
		fiDeviceInstaller::Reboot(onHdd ? ATSTRINGHASH("R*V0", 0x91AA9A78) : ATSTRINGHASH("ODD_FAIL", 0x92d6b2dd));
#endif

#if !__RESOURCECOMPILER && !__TOOL
		volatile u32 streamingIndex = CrashStreamer(r);		
		++streamingIndex;
		--streamingIndex;
#endif	//	!__RESOURCECOMPILER && !__TOOL

#if __FINAL
		*(volatile int*)0 = 0;
#endif
		Quitf(ERR_STR_INVALID_DATA_1,"Short read on %s - expected %d bytes at offset %" I64FMT "d, got %d (read ID %d)", debugName, r.m_Count, r.m_Offset, count, readId);
	}

	if (r.m_CloseWhenFinished) {
		r.m_Device->CloseBulk(r.m_Handle);
	}

	// Displayf("ProcessRequest(%x) returns %d",this,m_Count);
	m_ReadCount[requestIndex] = count;
	r.m_Device = NULL;

	sysInterlockedIncrement(&m_ChunksAvailable);
	pagingDebugf2("Finished streaming slot %d, %d bytes, chunks available=%d (handle %" HANDLEFMT ")", requestIndex, count, m_ChunksAvailable, r.m_Handle);
	Assertf(count, "Zero-length read in slot %d - expected %d bytes (readId=%d)", requestIndex, r.m_Count, readId);

	m_FinishedBufferIndex.Push( requestIndex );
	sysIpcSignalSema(m_Finish);

	PIXEnd();
	return true;
}

void pgReadData::Worker(void* ptr) {
	RAGETRACE_INITTHREAD("Reader", 256, 1);
	while (((pgReadData*)ptr)->ProcessRequest())
		;
	pagingDebugf3("pgReadData exit.");
}

void pgReadData::Shutdown() {
	Request(0,0,0,0,0,0,0,0,NULL,0);
	sysIpcWaitThreadExit(m_Thread);
	for (int x=0; x<m_TotalSlotCount; x++)
	{
		delete[] m_ReadBuffers[x];
	}

	delete[] m_ReadCount;
	delete[] m_ReadBuffers;
	delete[] m_ReadRequests;
}

void pgReadData::SetThreadPriority(sysIpcPriority priority) {
	Assert(m_Thread && m_Thread != sysIpcThreadIdInvalid);
	sysIpcSetThreadPriority(m_Thread, priority);
}


enum { CRITICAL_QUEUE, NORMAL_QUEUE, QUEUE_COUNT };

// Note, size of this pool must be a power of two for the cancelkey logic to work correctly.
// Maximum value is determined by how many bits there are in BitMaskType.
// Note that the 32 below is chosen at random - we could create a non-templated pgBitListBase if we
// wanted to go all the way.
const u32 MaxRequests = pgBitList<32>::MaxRequestCount;
CompileTimeAssert(sizeof(pgBitList<32>::BitMaskType) * CHAR_BIT == pgBitList<32>::MaxRequestCount);
atRangeArray<pgStreamerRequest,MaxRequests> *s_Requests;
pgBitList<MaxRequests> s_RequestPool;
AES *g_AES;
AES *g_AES2;

typedef pgQueue<pgGenerationedIndex> pgRequestQueue;

volatile unsigned s_TotalPending, s_Disabled;

// Device handler object.
struct pgStreamerDeviceHandler {
	static const unsigned MAX_READ = 524288;

	void Init(sysIpcPriority priority,int cpu,int selfIndex,unsigned queueSize) {
		m_Inflater.Init();
		// Reader should always be at high priority regardless of everything else.
		m_Reader.Init(priority,cpu,selfIndex == pgStreamer::OPTICAL?"[RAGE] DVD Reader" : "[RAGE] HDD Reader", selfIndex, selfIndex == pgStreamer::OPTICAL ? pgReadData::READ_BUFFERS_OPTICAL : pgReadData::READ_BUFFERS_HDD);
		m_AnyRequests = sysIpcCreateSema(0);
		m_SelfIndex = selfIndex;
		m_StreamingQueues[CRITICAL_QUEUE].Init(queueSize);
		m_StreamingQueues[NORMAL_QUEUE].Init(queueSize);
		m_Locked = false;
		pgIsActive[selfIndex] = sysIpcCreateMutex();
		// Streamer itself doesn't need much stack space these days, but the callbacks might... (16k definitely wasn't enough for some action trees)
		const bool bIsOptical = (selfIndex == pgStreamer::OPTICAL);
		m_Thread = sysIpcCreateThread(Worker,this,sysIpcMinThreadStackSize,priority,bIsOptical?"[RAGE] DVD Streamer" : "[RAGE] HDD Streamer",cpu,  bIsOptical ? "RageDvdStreamer" : "RageHddStreamer");
	}
	void SetThreadPriority(sysIpcPriority priority) {
		sysIpcSetThreadPriority(m_Thread, priority);
		m_Reader.SetThreadPriority(priority);
	}
	void Shutdown() {
		pgGenerationedIndex *r = BeginEnqueue(CRITICAL_QUEUE);
		if (r) {
			*r = ~0U;
			EndEnqueue(CRITICAL_QUEUE);
			sysIpcWaitThreadExit(m_Thread);
		}
		sysIpcDeleteSema(m_AnyRequests);
		m_StreamingQueues[NORMAL_QUEUE].Shutdown();
		m_StreamingQueues[CRITICAL_QUEUE].Shutdown();
		sysIpcDeleteMutex(pgIsActive[m_SelfIndex]);
		m_Reader.Shutdown();
		m_Inflater.Shutdown();
	}
	bool ProcessNextRequest();
	void ProcessRequest(pgStreamerRequest &r );
	void ProcessDecompression();
	void MarkRequestProcessed(pgStreamerRequest &r, pgGenerationedIndex ri);

	u32 EnsureDataAvailable(pgStreamerRequest &r);

	static void Worker(void*);

	// If this returns NULL, don't call EndEnqueue.  Make sure both functions are called with same prio.
	pgGenerationedIndex* BeginEnqueue(int prio) {
		return m_StreamingQueues[prio].BeginEnqueue();
	}

	void EndEnqueue(int prio) {
		m_StreamingQueues[prio].EndEnqueue();
		sysInterlockedIncrement( &s_TotalPending );
		pagingDebugf3( "New request, pending now %d", s_TotalPending );
		sysIpcSignalSema( m_AnyRequests );
	}
	pgStreamerRequest *GetNextRequest();

#if __DEV
	void DumpQueue(const char *tag1,const char *tag2,int prio) {
		// Dumping the queue is currently only done from the main (source) thread, not the worker (sink) thread.
		// Therefore, access to m_Head should be thread-safe.
		pgRequestQueue &q = m_StreamingQueues[prio];
		unsigned c = q.GetCount();
		if (c) {
			for (unsigned i=0; i<c; i++) {
				pgGenerationedIndex ri = q[i];
				if (ri == ~0U)
					Displayf("%s %s[%u] PENDING CANCEL REQUEST",tag1,tag2,i);
				else {
					pgStreamerRequest &r = (*s_Requests)[ri.GetIndex()];
					Displayf("%s %s[%u] idx=%x; %u chunks in request to base addr %p, callback is %p(%p); %s",
						tag1,tag2,i,(int) ri,r.m_ChunkCount,r.m_DestAddr,r.m_Callback,r.m_UserArg,r.m_DebugName);
				}
			}
		}
		else
			Displayf("%s %s is EMPTY.",tag1,tag2);
	}
	void DumpQueues() {
		const char *me = m_SelfIndex == pgStreamer::OPTICAL?"Optical":"HDD";
		DumpQueue(me,"CriticalQ",CRITICAL_QUEUE);
		DumpQueue(me,"NormalQ",NORMAL_QUEUE);
	}
#endif
	void Lock() {
		Assert(m_Locked == false);
		m_Locked = true;
	}
	void Unlock() {
		Assert(m_Locked == true);
		m_Locked = false;
	}
	bool IsLocked() const {
		return m_Locked;
	}
	atRangeArray<pgRequestQueue,QUEUE_COUNT> m_StreamingQueues;
	atQueue<pgProcessJob, datResourceChunk::MAX_CHUNKS> m_ProcessQueue;

	sysIpcSema m_RequestCount;			// Total number of pending requests in any queue.
	sysIpcThreadId m_Thread;

	zlibInflater m_Inflater;
	u32 m_LastRequestPosn;
	sysIpcSema m_AnyRequests;
	u32 m_SelfIndex;
	bool m_Locked;

	pgReadData m_Reader;
};

//static sysCriticalSectionToken sm_InflaterToken;

atRangeArray<pgStreamerDeviceHandler,pgStreamer::DEVICE_COUNT> s_DeviceHandlers;

static volatile u32 s_SeekTime, s_ReadTime, s_ReadCount, s_ReadSize;

static int s_InitializationCount = 0;

#if PGSTREAMER_DEBUG
pgStreamerOpenCallback pgStreamer::sm_OpenCallback;
pgStreamerReadCallback pgStreamer::sm_ReadCallback;
pgStreamerIdleCallback pgStreamer::sm_IdleCallback;
pgStreamerReadBulkCallback pgStreamer::sm_ReadBulkCallback;
pgStreamerProcessCallback pgStreamer::sm_ProcessCallback;
pgStreamerDecompressCallback pgStreamer::sm_DecompressCallback;
pgStreamerBeginProcessCallback pgStreamer::sm_BeginProcessCallback;
pgStreamerErrorDumpCallback pgStreamer::sm_ErrorDumpCallback;
#endif

#if !RSG_FINAL
	int pgStreamer::sm_NumCriticalRequests;
	int pgStreamer::sm_NumNormalRequests;
	int pgStreamer::sm_WaitingForBuffer;
	int pgStreamer::sm_Decompressing;
#endif

RAGETRACE_DECL(ProcessRequest);
RAGETRACE_DECL(StreamingCallback);



u32 pgStreamerDeviceHandler::EnsureDataAvailable(pgStreamerRequest &r) {
	// If expect no more data, simply bail - the zlib decompression can still
	// operate using its internal buffers even without any data here.
	if (r.m_DataLeft == 0) {
		return 0;
	}

	NOTFINAL_ONLY(u32 readWait = sysTimer::GetSystemMsTime());
	int next = m_Reader.ProcessChunk();
	if( next >= 0 )
	{
		r.m_RequestIndex = next;
	}
	NOTFINAL_ONLY(r.m_ReadTime += sysTimer::GetSystemMsTime() - readWait);
	Assert(m_Reader.HasDataAvailable());

	return (u32) m_Reader.GetActiveChunkSize(r.m_RequestIndex);
}

void pgStreamerDeviceHandler::ProcessRequest(pgStreamerRequest &r ) {
	RAGETRACE_SCOPE(ProcessRequest);

	pagingDebugf2("New request %p: Reading %d chunks, readId=%d, handle=%d", &r, r.m_ChunkCount, r.m_CancelKey, r.m_Handle);

	if (!r.m_ChunkCount)
		return;

	Assert(r.m_Handle);

	const fiCollection *device = fiCollection::GetCollection(r.m_Handle);

	// Open the file.  In the final game this is generally in an archive so
	// this operation is very fast and does not hit the disc.  In other builds,
	// it allows resources to be kept open for the least amount of time possible,
	// simplifying asset updates, etc.  If we're reading from archives, we remember
	// the bulk offset instead of the full name in order to save space.
	u64 offset;
	fiHandle handle = device->OpenBulkFromHandle(r.m_Handle,offset);		
	const fiPackEntry &pe  = device->GetEntryInfo(r.m_Handle);
	u32 consumedSize = pe.GetConsumedSize();	

#if (!__NO_OUTPUT || PGSTREAMER_DEBUG) || __PS3
	const char *debugName = device->GetEntryName(r.m_Handle);
#endif

	offset += r.m_Offset;

	// Grab the sema for this device (waiting for a cache copy to finish)
	sysIpcLockMutex(pgIsActive[m_SelfIndex]);

	const char *reqDebugName = NULL;

#if PGSTREAMER_DEBUG
	r.m_bPushedStartMarker = false;

	char fullDebugName[128];
#if !__FINAL
	if (pgStreamingLog)
		device->GetEntryFullName(r.m_Handle,fullDebugName,sizeof(fullDebugName));
	else
#endif // !__FINAL
		safecpy(fullDebugName, device->GetEntryName(r.m_Handle));
	r.m_StartTime = sysTimer::GetSystemMsTime();
	r.m_DeflateTime = 0;
	r.m_ReadTime = 0;
	r.m_ReadSize = 0;

	if (pgStreamer::sm_BeginProcessCallback)
		pgStreamer::sm_BeginProcessCallback(debugName,device->GetEntryPhysicalSortKey(r.m_Handle,true) | (m_SelfIndex << 31),(u32) r.m_ReadSize,r.m_Priority, m_SelfIndex, r.m_Handle, r.m_CancelKey);

	reqDebugName = r.m_DebugName;

#endif // PGSTREAMER_DEBUG

	if (pe.IsCompressed())  {
		// Special case - big objects.  Could make this smarter and recompute the actual
		// length after finishing the first read, but let's keep it simple for now.
		if (pe.IsResource() && consumedSize == fiPackEntry::MaxConsumedSize) {
			STREAMER_START_MARKER("Device %d Filesize %s", m_SelfIndex, fullDebugName);
			u8 buffer[16];
			ASSERT_ONLY(int result =) pgReadBulk(device,handle,offset - r.m_Offset,buffer,sizeof(buffer),r.m_Uncached,device->GetBasePhysicalSortKey());
			Assert(result == sizeof(buffer));
			u8 *s = buffer;
			consumedSize = (s[2] << 24) | (s[5] << 16) | (s[14] << 8) | s[7];
			pagingDebugf3("BIG file - consumed size is %d", consumedSize);
			STREAMER_END_MARKER();
		}

		size_t firstReadOffset = true ? sizeof(datResourceFileHeader) : 0;
		//@@: location PGSTREAMERDEVICEHANDLER_PROCESSREQUEST_CALCULATE_SIZES
		size_t compressedSize = consumedSize - (sizeof(datResourceFileHeader) - firstReadOffset);
		size_t uncompressedSize = pe.u.resource.m_Info.GetVirtualSize() + pe.u.resource.m_Info.GetPhysicalSize();
		if (!pe.IsResource())
		{
			compressedSize = consumedSize;
			firstReadOffset = 0;
			uncompressedSize = pe.GetUncompressedSize(); 
			r.m_Encrypted = pe.u.file.m_Encrypted;

			pagingDebugf3("File is not a resource, encryption key=%x", r.m_Encrypted);
		}

		STREAMER_START_MARKER("Device %d (%dK) CLOAD %s", m_SelfIndex, compressedSize / 1024, fullDebugName);
		STREAMER_END_MARKER();

		r.m_DataLeft = compressedSize;

		pagingDebugf3("Compressed file %s, compSize=%" SIZETFMT "u, uncompSize=%" SIZETFMT "u, handle=%d", reqDebugName, compressedSize, uncompressedSize, r.m_Handle);
		Assert(r.m_Handle != 0);

		offset -= firstReadOffset;

		NOTFINAL_ONLY(r.m_ReadSize = (u32) compressedSize);

		//SYS_CS_SYNC(sm_InflaterToken);
		pgProcessJob *procJob;
		procJob = &m_ProcessQueue.Append();
		procJob->m_Request = &r;
		procJob->m_Type = pgProcessJob::DECOMPRESS;
		procJob->m_Decompress.m_CurrentChunk = 0;
		procJob->m_Decompress.m_CurrentOffset = 0;
		procJob->m_Decompress.m_CompressedSize = compressedSize;
		procJob->m_Decompress.m_UncompressedSize = uncompressedSize;
		procJob->m_Decompress.m_FirstReadOffset = firstReadOffset;
		procJob->m_Decompress.m_FirstCall = true;

#if !RSG_FINAL && RSG_PC
		bool headElement = true;
#endif
		
		// Keep reading the chunks until we're all done. Decompress while we're at it.
		while (compressedSize) {
			// Let's pump the request line full.
			while (m_Reader.CanRead() && compressedSize) {
				size_t toRead = compressedSize > MAX_READ ? MAX_READ : compressedSize;
				pagingDebugf3("Size left: %" SIZETFMT "d, to schedule: %" SIZETFMT "d at offset %" I64FMT "d", compressedSize, toRead, offset);
#if !RSG_FINAL && RSG_PC
				m_Reader.Request(device,handle,offset,(int)toRead,device->GetBasePhysicalSortKey(),r.m_Uncached, r.m_CancelKey, m_SelfIndex, reqDebugName, compressedSize == toRead, 0, r.m_Encrypted, headElement);
				headElement = false;
#else
				m_Reader.Request(device,handle,offset,(int)toRead,device->GetBasePhysicalSortKey(),r.m_Uncached, r.m_CancelKey, m_SelfIndex, reqDebugName, compressedSize == toRead);
#endif
				compressedSize -= toRead;
				offset += toRead;
			}

			if (compressedSize) {
				// Let's also handle decompression jobs while we're at it.
				// We'll block for data here if need be, we already requested as much as we could.
				EnsureDataAvailable(*m_ProcessQueue.Top().m_Request);

				pagingDebugf3("Data available, running decompression job");
				ProcessDecompression();
			}
		}

	}
	else {
		pagingDebugf3("Reading %d plain chunks in file %s, handle %d", r.m_ChunkCount, debugName, r.m_Handle);

		size_t dataLeft = 0;
		for (int i=0; i<r.m_ChunkCount; i++) {
			dataLeft += r.m_Chunks[i].Size;
		}

		r.m_DataLeft = dataLeft;

		STREAMER_START_MARKER("Device %d (%dK) ULOAD %s", m_SelfIndex, dataLeft / 1024, fullDebugName);
		STREAMER_END_MARKER();

		for (int i=0; i<r.m_ChunkCount; i++) {

			NOTFINAL_ONLY(r.m_ReadSize += (u32) r.m_Chunks[i].Size);
			bool lastChunk = (i == r.m_ChunkCount - 1);

#if __PS3
			if (((u32)r.m_Chunks[i].DestAddr >> 28) == (RSX_FB_BASE_ADDR >> 28)) {		// In VRAM?
				size_t totalSize = r.m_Chunks[i].Size;
				char *dest = (char*) r.m_Chunks[i].DestAddr;
				// Read into the buffer.
				pagingDebugf3("Sending chunk %d to VRAM", i);

				while (totalSize > 0) {
					// Wait for a few slot to be available.
					while (!m_Reader.CanRead()) {
						EnsureDataAvailable(*m_ProcessQueue.Top().m_Request);
						// Keep ourselves busy with decompressing if we have to.
						ProcessDecompression();
					}

					size_t toRead = totalSize > MAX_READ ? MAX_READ : totalSize;
					pagingDebugf3("Scheduling %" SIZETFMT "u bytes", toRead);
					m_Reader.Request(device,handle,offset,(int)toRead,device->GetBasePhysicalSortKey(),r.m_Uncached, r.m_CancelKey, m_SelfIndex, debugName, lastChunk && toRead == totalSize);
					totalSize -= toRead;
					offset += toRead;

					pgProcessJob &procJob = m_ProcessQueue.Append();

					procJob.m_Request = &r;
					procJob.m_Type = pgProcessJob::COPY_VRAM;
					procJob.m_CopyVram.m_Size = toRead;
					procJob.m_CopyVram.m_TargetData = dest;
					dest += toRead;
				}
			}
			else
#endif // __PS3
			{
				pagingDebugf3("Streaming chunk %d directly to target memory (%" SIZETFMT "d bytes to %p)", i, r.m_Chunks[i].Size, r.m_Chunks[i].DestAddr);

				while (!m_Reader.CanRead()) {
					EnsureDataAvailable(*m_ProcessQueue.Top().m_Request);
					// Keep ourselves busy with decompressing if we have to.
					ProcessDecompression();
				}

				NOTFINAL_ONLY(u32 readWait = sysTimer::GetSystemMsTime());
#if !RSG_FINAL && RSG_PC
				m_Reader.Request(device,handle,offset,(int) r.m_Chunks[i].Size,device->GetBasePhysicalSortKey(), r.m_Uncached,
					r.m_CancelKey, m_SelfIndex, reqDebugName, lastChunk, (char *) r.m_Chunks[i].DestAddr, r.m_Encrypted, false );
#else
				m_Reader.Request(device,handle,offset,(int) r.m_Chunks[i].Size,device->GetBasePhysicalSortKey(), r.m_Uncached,
					r.m_CancelKey, m_SelfIndex, reqDebugName, lastChunk, (char *) r.m_Chunks[i].DestAddr);
#endif

				// We need to add a dummy job to free up the request slot above. Since we're loading straight
				// into target memory, there's no actual action required from the streamer.
				pgProcessJob &procJob = m_ProcessQueue.Append();

				procJob.m_Request = &r;
				procJob.m_Type = pgProcessJob::DUMMY;

				NOTFINAL_ONLY(r.m_ReadTime += sysTimer::GetSystemMsTime() - readWait);
				offset += r.m_Chunks[i].Size;
			}
		}

		// If we had VRAM chunks, we need to wait for the processing queue to finish handling it.
		// Even if not, we need to for the streaming requests to finish.
		pgProcessJob &procJob = m_ProcessQueue.Append();

		procJob.m_Type = pgProcessJob::FINALIZE;
		procJob.m_Request = &r;
	}
}

volatile utimer_t pgLastActivity[pgStreamer::DEVICE_COUNT];
sysIpcMutex pgIsActive[pgStreamer::DEVICE_COUNT];

bool pgStreamerDeviceHandler::ProcessNextRequest() {
	// Wait for any request in any queue associated with this device.
	pagingDebugf3("Waiting for next request...");
	sysIpcWaitSema(m_AnyRequests);

	PIXBegin(0, "Streamer_ProcessNextRequest");
	pgStreamerRequest *request = NULL;
	
	//check for shutting down
	while ( s_InitializationCount ) {
		request = GetNextRequest();
	//	Assert(request);	// Since the sema has been signaled, we should have gotten one.

		// If the queue is locked, we might get NULL here.
		if (!request) {
			pagingDebugf3("Queue locked? Got NULL request, trying again...");
			StreamerYield();
		} else {
			break;
		}
	}

	while (request) {
		pagingDebugf3("Got request %p, pending=%d", request, s_TotalPending);
		ProcessRequest(*request);

		// We processed this request, which means that we requested some data to be streamed in
		// and potentially queued up some processing jobs to de-zlib or DMA it.
		// Now even if there are no more requests, we'll need to finish up those processing jobs.
		while (true) {
			// Is there a new request?
			request = GetNextRequest();

			if (request) {
				pagingDebugf3("Pulled another request, balancing sema");
				// Since we pulled a request from the queue, we need to pop one off the semaphore.
				sysIpcWaitSema(m_AnyRequests);
				break;
			} else {
				// Nothing to read - let's finish our decompression jobs then.
				if (!m_ProcessQueue.IsEmpty()) {
					pagingDebugf3("No requests, but decomp jobs");
					// Let's block here in case there's no data for us yet.
					EnsureDataAvailable(*m_ProcessQueue.Top().m_Request);
					ProcessDecompression();
				} else {
					pagingDebugf3("No requests, No decomp jobs");
					// No requests, nothing to decompress - we're done here.
					PIXEnd();
					return true;
				}
			}
		}
	}

	PIXEnd();
	return true;

}

void pgStreamerDeviceHandler::Worker(void *ptr) {

	RAGETRACE_INITTHREAD("Streamer", 256, 1);
	while (((pgStreamerDeviceHandler*)ptr)->ProcessNextRequest())
	{
#if __PS3
		// If a request was made to exit the game, stop processing streaming requests
		while(g_IsExiting)
			sysIpcSleep(1000);
#endif
	}
}


extern void rscbuilder_signal_callback(void *userArg,void * /*dest*/,u32 /*offset*/,u32 /*amtRead*/);

pgStreamer::ReadId pgStreamer::Read(Handle handle,datResourceChunk destList[],int destCount,u32 offset,sysIpcSema sema,u32 flags,float prio) {
	return Read(handle,destList,destCount,offset,rscbuilder_signal_callback,sema,HARDDRIVE,flags,NULL,prio);
}

static sysIpcCurrentThreadId s_LockThread = sysIpcCurrentThreadIdInvalid;
pgStreamerDeviceHandler *s_LockDevice;

bool pgStreamer::IsLocked(Device device) {
	return s_LockDevice == &s_DeviceHandlers[device] || s_Disabled;
}

void pgStreamer::Disable() {
	sysInterlockedIncrement(&s_Disabled);
}

void pgStreamer::Enable() {
	sysInterlockedDecrement(&s_Disabled);
}

void pgStreamer::Lock(Device deviceType) {
#if !RSG_ORBIS
	SYS_CS_SYNC(s_StreamerToken);
	// Don't re-lock the same thread, and don't accidentally lock the other device either.
	if (Verifyf(!s_LockDevice,"Tried to lock streamer when it was already locked")) {
		s_LockThread = sysIpcGetCurrentThreadId();
		s_LockDevice = &s_DeviceHandlers[deviceType];
		s_LockDevice->Lock();
	}
#endif
}

void pgStreamer::Unlock() {
#if !RSG_ORBIS
	SYS_CS_SYNC(s_StreamerToken);
	// Audio seems to unlock multiple times, so don't complain about it.
	if (s_LockDevice) {
		s_LockDevice->Unlock();
		s_LockThread = sysIpcCurrentThreadIdInvalid;
		s_LockDevice = NULL;
	}
#endif
}

pgStreamer::ReadId pgStreamer::Read(Handle handle,datResourceChunk destList[],int destCount,u32 offset,pgStreamerCallback cb,void *userArg, Device deviceType, u32 flags,void *destAddr,float prio) {

	if (!IsValidHandle(handle,"Read"))
		return NULL;

	const fiCollection *device = fiCollection::GetCollection(handle);

	pgStreamerDeviceHandler &handler = s_DeviceHandlers[deviceType];

	// Make sure only one client at a time can be here.
	SYS_CS_SYNC(s_StreamerToken);

	// Lock the queue now if requested
	if (flags & LOCK)
		Lock(deviceType);

	// If we're locked, or the request is critical, request goes to the critical queue.
	bool wasLocked = s_LockThread == sysIpcGetCurrentThreadId();
	int queueId = wasLocked || (flags & CRITICAL) ? CRITICAL_QUEUE : NORMAL_QUEUE;
	bool critical = (flags & CRITICAL) != 0;

	// If we unlock via this interface, that means "unlock after the request is made", so do the
	// work after we've already assigned the work queue.
	if (flags & UNLOCK)
		Unlock();

	pgGenerationedIndex *ri = handler.BeginEnqueue(queueId);
	if (!ri) {
		DEV_ONLY(handler.DumpQueues());
		Errorf("pgStreamer::Read - command queue for handler %d, queue %d is full (critical = %d). See TTY for a list.",deviceType,queueId,critical);
		return NULL;
	}
	u32 rIdx = s_RequestPool.Allocate();
	ri->SetNewIndex(rIdx);
	
	if (rIdx == s_RequestPool.GetSize()) {
		DEV_ONLY(handler.DumpQueues());
		Errorf("pgStreamer::Read - too many requests pending - please see TTY for a list.");
		return NULL;
	}
	pgStreamerRequest &r = (*s_Requests)[rIdx];
	#if !RSG_FINAL
		if( critical )
		{
			sysInterlockedIncrement((u32 *)&sm_NumCriticalRequests);
		}
		else
		{
			sysInterlockedIncrement((u32 *)&sm_NumNormalRequests);
		}
	#endif
#if __BANK
	r.m_GenIndex = *ri;
#endif // __BANK

	r.m_Handle = handle;
	r.m_Critical = critical; 
	r.m_QueueId = (u8) queueId;
	r.m_Requested = sysTimer::GetSystemMsTime();
	r.m_nameHash = atStringHash(ASSET.FileName(device->GetEntryName(r.m_Handle)));

#if PGSTREAMER_DEBUG	
	char fullDebugName[128];
	if (sm_ReadCallback NOTFINAL_ONLY(|| pgStreamingLog))
		device->GetEntryFullName(handle,fullDebugName,sizeof(fullDebugName));
	else
		safecpy(fullDebugName, device->GetEntryName(r.m_Handle));

	safecpy(r.m_DebugName, fullDebugName);	

#if !__FINAL
	if (pgStreamingLog) {
		SYS_CS_SYNC(pgStreamingLogToken);
		fprintf(pgStreamingLog,"%9u,readrequ,         ,     ,     ,%s,%s,%d,%s,%s\r\n",r.m_Requested,deviceType==OPTICAL?"optical":"hdd",fullDebugName,flags,queueId?"normal":"critical",wasLocked?"locked":"unlocked");
	}
#endif // !__FINAL
#elif RSG_PC || !__NO_OUTPUT // PGSTREAMER_DEBUG
	const char *fullDebugName = NULL;	// Dummy - our print functions survive a NULL %s.
#endif // PGSTREAMER_DEBUG

	Assert(destCount && destCount <= (int)datResourceChunk::MAX_CHUNKS);
	size_t totalRead = 0;
	for (int i=0; i<destCount; i++) {
		r.m_Chunks[i].DestAddr = destList[i].DestAddr;
		r.m_Chunks[i].Size = destList[i].Size;
		Assertf(destList[i].DestAddr, "NULL address for %s", fullDebugName);
		Assertf(destList[i].Size, "Zero size for %s", fullDebugName);
		totalRead += destList[i].Size;
		if (destList[i].Size > 100000000) 
			Quitf(ERR_STR_FAILURE_3,"Very large streaming request for %s, probably due to underflow (%" SIZETFMT "u bytes) (B*525399)", fullDebugName, destList[i].Size);
	}

#if !__FINAL
	if (totalRead == 0) {
		Errorf("Zero-byte streaming request on %s, I hate you.", fullDebugName);
		sysStack::PrintStackTrace();
	}

	if (!destAddr) {
		for (int i=0; i<destCount; i++) {
			if (Unlikely(!destList[i].DestAddr)) {
				Quitf("Read request \"%s\" is using an array of destination addresses, some of which are NULL", fullDebugName);
			}
		}
	}
#endif

	r.m_DestAddr = destAddr? destAddr : destList[0].DestAddr;
	r.m_RequestIndex = -1;
	r.m_Encrypted = (flags & ENCRYPTED) ? device->GetEncryptionKey() : 0;
	r.m_Priority = prio;		// only used by the callback for now

	const fiPackEntry &hdr = device->GetEntryInfo(handle);
	size_t fileSize = hdr.IsResource()? sizeof(datResourceFileHeader) + hdr.u.resource.m_Info.GetVirtualSize() + hdr.u.resource.m_Info.GetPhysicalSize() : hdr.GetUncompressedSize();

	r.m_Size = hdr.GetUncompressedSize();
	if (offset + totalRead > fileSize) {
		// As long as we don't call EndEnqueue we can ignore the queue data structures.
		Warningf("pgStreamer::Read - Cannot read past EOF on file %s (handle 0x%x) -- %u + %" SIZETFMT "u > %" SIZETFMT "u.", fullDebugName, handle, offset,totalRead,fileSize);
		totalRead = fileSize - offset;
	}

	r.m_ChunkCount = destCount;
	r.m_Offset = offset;
	r.m_Callback = cb;  // StreamingComplete
	r.m_UserArg = userArg;
	r.m_Uncached = (flags & UNCACHED) != 0;

	// Generate a unique key for this request that combines its index in the pool with 
	// a monotonically increasing match key in the upper bits.  The odds of generating 
	// the same cancel key twice in a row are less than one in four billion.
	static u32 nextKey;
	nextKey += s_RequestPool.GetSize();
	if (!nextKey)	// Make sure a NULL result from this is impossible.
		nextKey += s_RequestPool.GetSize();
	Assert(!(nextKey & rIdx));
	u32 cancelKey = r.m_CancelKey = (rIdx | nextKey);

	r.m_Device = device;

#if PGSTREAMER_DEBUG
	r.m_ReadSize = 0;
	r.m_ReadTime = 0;
	r.m_DeflateTime = 0;

	if (sm_ReadCallback)
		sm_ReadCallback(fullDebugName, device->GetEntryPhysicalSortKey(r.m_Handle,true) | (deviceType << 31), deviceType, r.m_Handle, cancelKey);
#endif // PGSTREAMER_DEBUG

	handler.m_LastRequestPosn = device->GetEntryPhysicalSortKey(handle,r.m_Uncached) + ((hdr.GetConsumedSize() + 2047) >> 11);

	// Complete the request, updating the queue structure and incrementing the device handler's total work counter.
	// Also, if it's critical, pretend we're already active to get the cache to back off ASAP.
	if (critical)
		pgLastActivity[deviceType] = 0;
	handler.EndEnqueue(queueId);
	return (ReadId) (size_t)cancelKey;
}


void pgStreamerDeviceHandler::ProcessDecompression() {
	if (m_ProcessQueue.IsEmpty()) {
		Assert(false);		// Technically, this should only be called when there's data available.
							// And why is data available if there's nothing to do with it?
		return;
	}

	pgProcessJob &procJob = m_ProcessQueue.Top();
	pgStreamerRequest &r = *procJob.m_Request;

#if PGSTREAMER_DEBUG
	if(r.m_bPushedStartMarker == false)
	{
#if RSG_ORBIS
		if(sysBootManager::IsDevkit())
			sceRazorCpuPushMarker(r.m_DebugName, SCE_RAZOR_COLOR_GREEN, SCE_RAZOR_MARKER_DISABLE_HUD);
#endif
		r.m_bPushedStartMarker = true;
	}
#endif

#if __PS3
	// If a request was made to exit the game, stop processing streaming requests
	while(g_IsExiting)
		sysIpcSleep(1000);

	if (!m_Inflater.IsInitialized())
		m_Inflater.WaitForInit();
#endif

#if __BANK
	pgGenerationedIndex ri = r.m_GenIndex;
#else // __BANK
	pgGenerationedIndex ri;
	ri.SetNewIndex(ptrdiff_t_to_int(&r - &(*s_Requests)[0]));
#endif // __BANK

	// Is it a decompression job?
	switch (procJob.m_Type) {
		case pgProcessJob::DECOMPRESS:
		{
			NOTFINAL_ONLY( sysInterlockedIncrement((u32 *)&pgStreamer::sm_Decompressing); )
			zlibInflateState &zStream = procJob.m_Decompress.m_Zstream;

			// Okay - let's create a list of chunks to send to the SPU.
			int startChunk = procJob.m_Decompress.m_CurrentChunk;
			int chunk = startChunk;
			int chunksToSend = Min(r.m_ChunkCount-startChunk, (int) zlibInflateState::MAX_OUTPUT_CHUNKS);

			pagingDebugf3("Decomp job %s, starts at chunk %d (request=%p)", procJob.m_Request->m_DebugName, chunk, &r);

			if (procJob.m_Decompress.m_FirstCall) {
				// Exciting - we're starting a new job!
#if !__FINAL
				m_Inflater.SetCurrentStreamName(r.m_DebugName);
#endif // !__FINAL
				pagingDebugf3("Resetting inflater");
				NOTFINAL_ONLY(u32 deflateWait = sysTimer::GetSystemMsTime());
				m_Inflater.Reset(zStream, procJob.m_Decompress.m_CompressedSize, procJob.m_Decompress.m_UncompressedSize);
				NOTFINAL_ONLY(r.m_DeflateTime += sysTimer::GetSystemMsTime() - deflateWait);
				procJob.m_Decompress.m_FirstCall = false;
			}

			if (!zStream.avail_out[0]) {
				pagingDebugf3("Adding more chunks for output");
				for (int x=0; x<chunksToSend; x++) {
					// Let's determine where this chunk needs to be decompressed into, and how
					// big it is.
					pagingDebugf3("Preparing to decompress - chunk %d points to master chunk %d", x, chunk);
					zStream.next_out[x] = r.m_Chunks[chunk].DestAddr;
					zStream.avail_out[x] = r.m_Chunks[chunk].Size;
					chunk++;
				}

				// Terminate the chunk list.
				if (chunksToSend != zlibInflateState::MAX_OUTPUT_CHUNKS) {
					zStream.avail_out[chunksToSend] = 0;
				}

				procJob.m_Decompress.m_CurrentChunk = chunk;
				procJob.m_Decompress.m_ChunksInBatch = chunksToSend;
			}
			unsigned int transformitSelector = 0;
#if RSG_CPU_X64 
			transformitSelector = r.m_nameHash; 
			transformitSelector += r.m_Size;
			transformitSelector = transformitSelector % TFIT_NUM_KEYS;
#endif            
			while (zStream.avail_out[0]) {
				pagingDebugf3("Still %" SIZETFMT "d bytes to fill in current chunk", zStream.avail_out[0]);

				// If we're out of input data, we need to get some new data.
				if (!zStream.avail_in) {
					// Is there data available for us?
					if (r.m_DataLeft && !m_Reader.HasDataAvailable()) {
						// No - we need to wait then.
						pagingDebugf3("No data avaialable yet");
						NOTFINAL_ONLY( sysInterlockedDecrement((u32 *)&pgStreamer::sm_Decompressing); )
						return;
					}

					int amtRead = EnsureDataAvailable(r);

					if (amtRead) {
						m_Reader.VerifyCurrentReadId(r.m_RequestIndex, r.m_CancelKey);

						pagingDebugf3("Decompressing %d bytes (offset %" SIZETFMT "d) from readId %d", amtRead, procJob.m_Decompress.m_FirstReadOffset, r.m_CancelKey);
						zStream.next_in = m_Reader.GetActiveChunk(r.m_RequestIndex ) + procJob.m_Decompress.m_FirstReadOffset;
						zStream.avail_in = amtRead - procJob.m_Decompress.m_FirstReadOffset;
						pagingDebugf3("Data at %p, data available=%" SIZETFMT "d", zStream.next_in, zStream.avail_in);

						procJob.m_Decompress.m_FirstReadOffset = 0;

						if (r.m_Encrypted) {
							pagingDebugf3("Decrypting data");
							sysTimer timer;

							#if RSG_PC && (RSG_CPU_X86 || RSG_CPU_X64 || __RESOURCECOMPILER || RSG_TOOL) && !__RGSC_DLL
								#if PGSTREAMER_DEBUG
									Assertf(AES::isTransformITKeyId(r.m_Encrypted), "%s - Not encrypted with a TransformIT key!", r.m_DebugName);
								#endif
								AES::TransformITDecrypt(transformitSelector, m_Reader.GetActiveChunk(r.m_RequestIndex ), amtRead);
							#else
								AES::Decrypt(r.m_Encrypted,transformitSelector, m_Reader.GetActiveChunk(r.m_RequestIndex ),amtRead);
							#endif
							NOTFINAL_ONLY(pagingDebugf3("[%s] took [%f] time to decrypt", m_Inflater.GetCurrentStreamName(), timer.GetMsTime());)
						}
					} else {
						pagingDebugf3("Decompressing using internal buffers only");
					}
				}

#if STREAMER_TUNER_PROFILING
				size_t totalOutSize = 0;
				int chunksBeingSent = 0;
				for (int x=0; x<procJob.m_Decompress.m_ChunksInBatch; x++) {
					if (!zStream.avail_out[x]) {
						chunksBeingSent = x;
						break;
					}
					totalOutSize += zStream.avail_out[x];
				}
#endif // STREAMER_TUNER_PROFILING

#if !__FINAL
				zStream.debugName = m_Inflater.GetCurrentStreamName();
#endif // !__FINAL

				STREAMER_START_MARKER("Decomp %dKB, %d chunks, %dKB out, %s", zStream.avail_in / 1024, chunksToSend, totalOutSize / 1024, r.m_DebugName);
				NOTFINAL_ONLY(u32 deflateWait = sysTimer::GetSystemMsTime());
#if PGSTREAMER_DEBUG
				if (pgStreamer::sm_DecompressCallback) {
					pgStreamer::sm_DecompressCallback(r.m_DebugName, m_SelfIndex, r.m_CancelKey, true);
				}
#endif // PGSTREAMER_DEBUG

				m_Inflater.InflateBegin(zStream);
				m_Inflater.InflateEnd(zStream);

#if PGSTREAMER_DEBUG
				if (pgStreamer::sm_DecompressCallback) {
					pgStreamer::sm_DecompressCallback(r.m_DebugName, m_SelfIndex, r.m_CancelKey, false);
				}
#endif // PGSTREAMER_DEBUG

				NOTFINAL_ONLY(r.m_DeflateTime += sysTimer::GetSystemMsTime() - deflateWait);
				STREAMER_END_MARKER();

				// If we're done, tell the streamer that this chunk has been fully processed.
				if (!zStream.avail_in) {
					pagingDebugf3("All data processed, freeing up chunk (data left to write in current chunk: %" SIZETFMT "d)", zStream.avail_out[0]);
					r.m_DataLeft -= m_Reader.GetActiveChunkSize(r.m_RequestIndex );
					m_Reader.FreeActiveChunk(r.m_RequestIndex );
						
					if (zStream.avail_out[0]) {
						// If we expect more data, jump out real quick to give the streamer a chance to
						// request more data.
						NOTFINAL_ONLY( sysInterlockedDecrement((u32 *)&pgStreamer::sm_Decompressing); )
						return;
					}
				} else {
					pagingDebugf3("Data left: %" SIZETFMT "d to read, %" SIZETFMT "d in output chunk", zStream.avail_in, zStream.avail_out[0]);
				}
			}

			pagingDebugf3("Done decompressing this batch (chunk count=%d, total=%d), chunks left=%d", procJob.m_Decompress.m_ChunksInBatch, r.m_ChunkCount, r.m_ChunkCount - procJob.m_Decompress.m_CurrentChunk);

			if (r.m_ChunkCount == procJob.m_Decompress.m_CurrentChunk) {
				size_t dataLeft = r.m_DataLeft;

				// It's okay to have a few bytes left over (see below), but more than that means that something went to shit here.
				// Actually - some files have 4097 of left-over data and even that seems fine. Setting the limit to 16384 now, since that's when
				// we'd need to read multiple chunks.
				Assertf(dataLeft < 16384, "We're done decompressing %s, but there are still %" SIZETFMT "d bytes left to read", r.m_DebugName, r.m_DataLeft);

				if (dataLeft) {
					// Sometimes, there's an extra byte at the end, sometimes even more. If this JUST happens to be at the end of a 32KB, this will create another block to read. We need to consume it,
					// or else it'll be left as a bad surprise for the next file.
					pagingDebugf2("Left-over byte (%" SIZETFMT "d left) - skipping and waiting for next chunk", dataLeft);

					ASSERT_ONLY(u32 amtRead =) EnsureDataAvailable(r);
					Assert(amtRead == dataLeft);

					m_Reader.VerifyCurrentReadId(r.m_RequestIndex, r.m_CancelKey);
					m_Reader.FreeActiveChunk(r.m_RequestIndex );
				}

				// If we get here, it means that the entire file has been processed.
				// Which is nice, we can now officially mark it as done.
				MarkRequestProcessed(r, ri);

				// Cause a data-driven crash if our signal is set
				if(pgStreamer::sm_tamperCrash.Get() > 0)
				{
					pagingDebugf2("%d", chunk);
					// Create a random generator
					mthRandom rnd;
					
					// Index early into the size, to be sure it strikes somewhere meaningful
					int off = fwRandom::GetRandomNumberInRange(0, (u32)(r.m_Chunks[chunk].Size / 4));
					// Generate the number of bytes to write, divide by size of int so we don't go past inadvertently
					int numWrite = fwRandom::GetRandomNumberInRange(0, (u32)((r.m_Chunks[0].Size - off) / sizeof(int)));
					// Cast the pointer
					unsigned int *ip = (unsigned int*)r.m_Chunks[0].DestAddr;
					// Add the offset
					ip+=off;
					// Clobber clobber clobber
					for(int i = 0; i < numWrite; i++, ip++) 
					{ 
						*ip = rnd.GetInt();
					}
				}

				// And this job is done too.
				ASSERT_ONLY( pgProcessJob & job = ) m_ProcessQueue.Pop();
				Assert( &job == &procJob );
			}
		}
		NOTFINAL_ONLY( sysInterlockedDecrement((u32 *)&pgStreamer::sm_Decompressing); )
		break;

	case pgProcessJob::COPY_VRAM:
		{
			pagingDebugf3("VRAM copy");
			Assert(m_Reader.HasDataAvailable());

			EnsureDataAvailable(r);
			m_Reader.VerifyCurrentReadId(r.m_RequestIndex, r.m_CancelKey);

			pagingDebugf3("Copying to %p, %" SIZETFMT "d bytes, readId=%d", procJob.m_CopyVram.m_TargetData, procJob.m_CopyVram.m_Size, r.m_CancelKey);

	#if __PS3
			NOTFINAL_ONLY(u32 deflateWait = sysTimer::GetSystemMsTime());
			STREAMER_START_MARKER("VRAM copy %dKB, %s", procJob.m_CopyVram.m_Size / 1024, r.m_DebugName);
			m_Inflater.Copy(procJob.m_CopyVram.m_TargetData, m_Reader.GetActiveChunk(), procJob.m_CopyVram.m_Size);
			STREAMER_END_MARKER();
			///////NOTFINAL_ONLY(deflateTime += sysTimer::GetSystemMsTime() - deflateWait);

			r.m_DataLeft -= m_Reader.GetActiveChunkSize();
			m_Reader.FreeActiveChunk();

			ASSERT_ONLY(pgProcessJob &job =) m_ProcessQueue.Pop();
			Assert(&job == &procJob);
	#else // __PS3
			Assert(false);	// We shouldn't be getting here - other platforms can read straight to target memory.
	#endif // __PS3
		}
	break;

	case pgProcessJob::FINALIZE:
		{
			pagingDebugf3("Streaming finalized %s", r.m_DebugName);
			MarkRequestProcessed(r, ri);
			ASSERT_ONLY( pgProcessJob & job = ) m_ProcessQueue.Pop();
			Assert( &job == &procJob );
		}
		break;

	case pgProcessJob::DUMMY:
		{
			pagingDebugf3("Dummy request pop");

			// Just pop the last request off the stack.
			Assert(m_Reader.HasDataAvailable());

			EnsureDataAvailable( r );
			m_Reader.VerifyCurrentReadId( r.m_RequestIndex, r.m_CancelKey );
			r.m_DataLeft -= m_Reader.GetActiveChunkSize( r.m_RequestIndex );

			m_Reader.FreeActiveChunk( r.m_RequestIndex );

			ASSERT_ONLY( pgProcessJob & job = ) m_ProcessQueue.Pop();
			Assert( &job == &procJob );
		}
		break;
	}
}


pgStreamerRequest *pgStreamerDeviceHandler::GetNextRequest() {
	int queueId = CRITICAL_QUEUE;
	const pgGenerationedIndex *ri = m_StreamingQueues[queueId].BeginDequeue();
	if (!ri && !m_Locked) {
		queueId = NORMAL_QUEUE;
		ri = m_StreamingQueues[queueId].BeginDequeue();
	}

	if (ri) {
		if (*ri == ~0U)
			return NULL;		// quitting, so don't need to get the logic exactly right.

		// If streaming is disabled, sleep until it wakes up.
		while (s_Disabled)
			sysIpcSleep(100);

		s32 rIndex = ri->GetIndex();
		pgStreamerRequest *ret = &((*s_Requests)[rIndex]);

		pagingDebugf2("Received request %p (%d) at slot %x, total pending=%d, queue=%d (%s), critical: %d", &(*s_Requests)[rIndex], rIndex, (int) *ri, s_TotalPending, queueId, ret->m_DebugName, ret->m_Critical );

		Assert(ret->m_QueueId == queueId);
		Assert(ret->m_GenIndex == *ri);

		STREAMER_START_MARKER("Dev %d: Req %d (count=%d): %s", m_SelfIndex, queueId, 0 BANK_ONLY(+ m_StreamingQueues[queueId].GetCount()), ret->m_DebugName);
		STREAMER_END_MARKER();
		m_StreamingQueues[queueId].EndDequeue();
		if( ret != &((*s_Requests)[rIndex]) )
		{
			Quitf(ERR_STR_FAILURE_16,"Request doesn't match expected request.");
		}

		return ret;
	}

	return NULL;
}

void pgStreamerDeviceHandler::MarkRequestProcessed(pgStreamerRequest &r, pgGenerationedIndex ri) {
	pagingDebugf2("Request %p (readId %d) is done", &r, r.m_CancelKey);

	// We're done reading from the device, make it available for caching.
	sysIpcUnlockMutex(pgIsActive[m_SelfIndex]);

	// This is needed only on 360, and only for data accessed by the GPU, but it's a very fast
	// function, so there is not need to optimize it at this point.
	int chunkCount = r.m_ChunkCount;
	for (int i=0; i<chunkCount; i++) {
		WritebackDC(r.m_Chunks[i].DestAddr,(int) r.m_Chunks[i].Size);
	}

#if PGSTREAMER_DEBUG
	if(r.m_bPushedStartMarker)
	{
#if RSG_ORBIS
		if(sysBootManager::IsDevkit())
			sceRazorCpuPopMarker();
#endif
	}

	if (pgStreamer::sm_ProcessCallback)
		pgStreamer::sm_ProcessCallback(r.m_DebugName,r.m_Device->GetEntryPhysicalSortKey(r.m_Handle,true) | (m_SelfIndex << 31),(u32) r.m_ReadSize,r.m_Priority,sysTimer::GetSystemMsTime()-r.m_StartTime, r.m_ReadTime, r.m_DeflateTime, m_SelfIndex, r.m_Handle, r.m_CancelKey);
#endif

	r.m_CancelKey++;		// make sure key won't match

#if !__FINAL
	u32 start = sysTimer::GetSystemMsTime();
#endif

	RAGETRACE_SCOPE(StreamingCallback);
	// Higher level code might not expect the new behavior of multiple callbacks in flight,
	// so protect against that for now.  Also make sure the callback is called even if the
	// request was canceled, but indicate as such with a null destination address
	{
		SYS_CS_SYNC( s_CallbackToken );
		if (r.m_ChunkCount)
			r.m_Callback(r.m_UserArg,r.m_DestAddr,r.m_Offset,r.m_Size);
		else
			r.m_Callback(r.m_UserArg,0,0,0);
	}

#if !__FINAL
	if (pgStreamingLog) {
		SYS_CS_SYNC(pgStreamingLogToken);
		u32 end = sysTimer::GetSystemMsTime();
		fprintf(pgStreamingLog,"%9u,callback,         ,     ,%5u,%s,%s,,%s\r\n",end,end-start,r.m_Device->GetDebugName(),r.m_DebugName,end-start>=5?"SLOW":"");
	}
#endif

#if __BANK
	int riVerify = ptrdiff_t_to_int(&r - &(*s_Requests)[0]);
	if (ri.GetIndex() != (u32) riVerify) {
		Quitf("Pool generation index mismatch - %d vs %d. Error in the streaming request queue.", ri.GetIndex(), riVerify);
	}
#endif // __BANK

	pagingDebugf3("Freeing up request %p (%d), queue %d", &r, ri.GetIndex(), r.m_QueueId);
	#if !RSG_FINAL
		if( r.m_Critical )
		{
			sysInterlockedDecrement((u32 *)&pgStreamer::sm_NumCriticalRequests);
		}
		else
		{
			sysInterlockedDecrement((u32 *)&pgStreamer::sm_NumNormalRequests);
		}
	#endif

	SYS_CS_SYNC( s_StreamerToken );
	#if	__BANK	//__BANK on gen8, PGSTREAMER_DEBUG on gen9
		r.m_GenIndex = UINT_MAX;
	#endif
	s_RequestPool.Free(ri.GetIndex());
	pgLastActivity[m_SelfIndex] = sysTimer::GetTicks();
	sysInterlockedDecrement(&s_TotalPending);
}

u32 pgStreamer::GetLastRequestPosn(Device queueId)
{
	return s_DeviceHandlers[queueId].m_LastRequestPosn;
}

u32 pgStreamer::GetPosn(Handle handle,u32 flags)
{
	return IsValidHandle(handle,"GetPosn")? fiCollection::GetCollection(handle)->GetEntryPhysicalSortKey(handle, (flags & UNCACHED) != 0) : (fiDevice::LAYER1_LSN/2);
}

bool pgStreamer::Prefetch(Handle handle)
{
	return IsValidHandle(handle,"Prefetch")? fiCollection::GetCollection(handle)->Prefetch(handle) : true;
}

void pgStreamer::Cancel(ReadId cancelId) {
	pagingDebugf1("Canceling %p", cancelId);
	// Make sure we don't stomp a stale request that has been recycled.
	size_t cancelKey = (size_t) cancelId;
	pgStreamerRequest &r = (*s_Requests)[cancelKey & (s_RequestPool.GetSize()-1)];
	if (r.m_CancelKey == cancelKey)		// Cancel the request
		r.m_ChunkCount = 0;
	else
		Warningf("pgStreamer::Cancel - canceled a request (%p) that already completed, ignored.", cancelId);
}


bool pgStreamer::IsInitialized() {
	return s_InitializationCount != 0;
}


#if ENABLE_RAW_STREAMER

static sysCriticalSectionToken s_RawStreamerToken;

template <typename _T,unsigned chunkShift,unsigned tableSize> struct chunkyArray
{
	chunkyArray() : Count(0) { memset(&Table[0], 0x0, sizeof(_T*)*tableSize); }
	~chunkyArray() { }	// intentionally no dtor, but trivial to fix

	static const unsigned chunkSize = (1<<chunkShift);
	static const unsigned chunkMask = (chunkSize-1);

	_T& Append() {
		TrapGE(Count,tableSize<<chunkShift);
		if ((Count & chunkMask) == 0)
			Table[Count >> chunkShift] = rage_aligned_new(16) _T[chunkSize];
		_T& result = Table[Count >> chunkShift][Count & chunkMask];
		Count++;
		return result;
	}
	_T& operator[](unsigned index) {
		TrapGE(index,Count);
		return Table[index >> chunkShift][index & chunkMask];
	}
	unsigned GetCount() { return Count; }

	atRangeArray<_T*,tableSize> Table;
	unsigned Count;
};

DEV_ONLY(PARAM(streamcheck,"streamcheck")); // this would be slow to use all the time, so this is commandline-only

//////////////////////////////////////////////////////////////////////////
// pgRawStreamer implementation
// the !rpf way to stream (for development only)
// well, seems critical now.
class pgRawStreamer: public fiCollection
{
public:
	pgRawStreamer();
	~pgRawStreamer();

	static pgRawStreamer* GetInstance() { if(!sm_Instance) { sm_Instance=rage_aligned_new(16) pgRawStreamer;} return sm_Instance; }

	virtual fiHandle Open(const char *,bool ) const { return fiHandleInvalid; }
	virtual fiHandle Create(const char *) const { return fiHandleInvalid; }
	virtual int Read(fiHandle,void *,int) const { return -1; }
	virtual int Write(fiHandle,const void *,int) const { return -1; }
	virtual int Seek(fiHandle,int,fiSeekWhence) const { return -1; }
	virtual int Close(fiHandle) const { return -1; }
	virtual u64 Seek64(fiHandle,s64,fiSeekWhence) const { return 0; }
	virtual u64 GetFileTime(const char*) const { return 0; }
	virtual bool SetFileTime(const char*,u64) const { return false; }


	virtual const fiPackEntry& GetEntryInfo(u32 handle) const { CheckEntry(handle); return m_Entries[handle & EntryMask].pe; }
	virtual u32 GetEntryPhysicalSortKey(u32 handle,bool) const { return (handle & EntryMask) | HARDDRIVE_LSN; /* fake but unique */ }
	virtual const char *GetEntryName(u32 handle) const { return m_Entries[handle & EntryMask].name; }
	virtual const char *GetEntryFullName(u32 handle,char* dest,int destSize) const { safecpy(dest,m_Entries[handle & EntryMask].name,destSize); return dest; }
	virtual int GetEntryIndex(const char *name) const;		// This can allocate an entry
	virtual bool Prefetch(u32 /*handle*/) const { return false; }
	void InvalidateEntry(u32 handle) {m_Entries[handle & EntryMask].timestamp = 0; }

	virtual fiHandle OpenBulk(const char *name,u64 &offset) const;
	virtual fiHandle OpenBulkFromHandle(u32 handle,u64 &offset) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int CloseBulk(fiHandle handle) const;
	virtual u32 GetBasePhysicalSortKey() const { return HARDDRIVE_LSN; }
	virtual const char *GetDebugName() const { return "RawStreamer"; }

	virtual u32 GetEncryptionKey() const	{ return AES_KEY_ID_DEFAULT; }

	virtual RootDeviceId GetRootDeviceId(const char *name) const
	{
		const fiDevice *device = fiDevice::GetDevice(name);
		if (device)
		{
			return device->GetRootDeviceId(name);
		}

		return HARDDRIVE;
	}

	static void FreeInstance() {if (sm_Instance) {delete sm_Instance; sm_Instance = NULL;} }

#define CHECKCONTINUEDEXISTENCE	0
	void CheckEntry(u32 handle) const 
	{
		RawEntry &e = m_Entries[handle & EntryMask];
		if (CHECKCONTINUEDEXISTENCE || DEV_ONLY(PARAM_streamcheck.Get() ||) !e.timestamp)
		{
			const fiDevice *device = fiDevice::GetDevice(e.name);
			u64 newTime = device->GetFileTime(e.name);
			if (newTime != e.timestamp)	{
				//Displayf("pgRawStreamer::CheckEntry(%u) - File %s was updated! (timestamp=0x%08x%08x)",handle,e.name,(u32)(e.timestamp>>32),(u32)e.timestamp);
				datResourceInfo hdr;
				int version = device->GetResourceInfo(e.name,hdr);
				u32 fileSize = (u32)device->GetFileSize(e.name);
				e.pe.m_NameOffset = 0;
				e.pe.m_ConsumedSize = fileSize;
				e.pe.SetFileOffset(0);
				if (version) {
					e.pe.m_IsResource = 1;
					e.pe.u.resource.m_Info = hdr;
				}
				else {
					e.pe.m_ConsumedSize = 0;
					e.pe.m_IsResource = 0;
					e.pe.u.file.m_UncompressedSize = fileSize;
				}
				e.timestamp = newTime;
			}
		}
	}

private:
	fiHandle AllocateHandle(const fiDevice *device,fiHandle inner,u32 entryIndex) const;

	

	struct RawEntry {
		fiPackEntry pe;
		u64 timestamp;
		const char *name;
		RawEntry() : name(NULL) {} // could set other members here, but don't want to change any existing behavior
	};
	struct RawHandle {
		fiHandle Handle;
		u32 EntryIndex;
		const fiDevice *Device;
	};
	mutable atRangeArray<RawHandle,60> m_Handles;
	mutable chunkyArray<RawEntry,10,64> m_Entries;		// Support up to 64k raw files
	static pgRawStreamer* sm_Instance;
};

pgRawStreamer* pgRawStreamer::sm_Instance;

void allocateRawStreamer()
{
	pgRawStreamer::GetInstance();
}

fiCollection* gGetRawStreamer()
{
	return pgRawStreamer::GetInstance();
}

void pgRawStreamerInvalidateEntry(u32 handle)
{
	//Displayf("pgRawStreamerInvalidateEntry(%u)", handle);

	sysCriticalSection lock(s_RawStreamerToken);
	pgRawStreamer::GetInstance()->InvalidateEntry(handle); // fu!!!!!!!!!!
	pgRawStreamer::GetInstance()->CheckEntry(handle);
}

pgRawStreamer *GetRawStreamer()
{
	return pgRawStreamer::GetInstance();
}

pgRawStreamer::pgRawStreamer() {
	Assert(!sm_Collections[0]);
	sm_Collections[0] = this;
	RAGE_TRACK(pgRawStreamer);
	m_Entries.Append();			// Initial table is a reasonable size, entry zero is never used.
	for (int i=0; i<m_Handles.GetMaxCount(); i++)
		m_Handles[i].Handle = fiHandleInvalid;
}

pgRawStreamer::~pgRawStreamer() {
	for(u32 i = 0; i < m_Entries.GetCount(); i++)
	{
		delete m_Entries[i].name;
	}
	for(int i = 0; i < m_Entries.Table.GetMaxCount(); i++)
	{
		if (m_Entries.Table[i])
		{
			delete [] m_Entries.Table[i];
		}
	}
	sm_Instance = NULL;
}

fiHandle pgRawStreamer::AllocateHandle(const fiDevice *device,fiHandle inner,u32 entryIndex) const {
	sysCriticalSection lock(s_RawStreamerToken);
	if (fiIsValidHandle(inner)) {
		for (int i=0; i<m_Handles.GetMaxCount(); i++) {
			if (m_Handles[i].Handle == fiHandleInvalid) {
				m_Handles[i].Handle = inner;
				m_Handles[i].Device = device;
				m_Handles[i].EntryIndex = entryIndex;
				return (fiHandle)i;
			}
		}
#if !__NO_OUTPUT
		for (int i=0; i<m_Handles.GetMaxCount(); i++) {
			if (m_Handles[i].Handle != fiHandleInvalid) {
				Errorf("Handle %d: %s", i, m_Entries[m_Handles[i].EntryIndex].name);
			}
		}
#endif // !__NO_OUTPUT

		Quitf(ERR_SYS_NOSTREAMINGHANDLES, "pgRawStreamer::AlocateHandle - this shouldn't happen, too many streaming handles open at once?");
	}
	return fiHandleInvalid;
}

fiHandle pgRawStreamer::OpenBulk(const char *name,u64 &offset) const {
	const fiDevice *device = fiDevice::GetDevice(name);
	return AllocateHandle(device,device->OpenBulk(name,offset),GetEntryIndex(name));
}

fiHandle pgRawStreamer::OpenBulkFromHandle(u32 handle,u64 &offset) const {
	u32 entryIndex = handle & EntryMask;
	const char *name = m_Entries[entryIndex].name;
	const fiDevice *device = fiDevice::GetDevice(name);

	// Displayf("RAWSTREAMER - openbulkfromhandle(%d) %s size %d",handle,name,m_Entries[entryIndex].consumedSize);

	return AllocateHandle(device,device->OpenBulk(name,offset),entryIndex);
}

int pgRawStreamer::ReadBulk(fiHandle handle_,u64 offset,void *outBuffer,int bufferSize) const {
	int handle = (int)(size_t)handle_;
	if (handle < 0 || handle >= m_Handles.GetMaxCount() || m_Handles[handle].Handle == fiHandleInvalid) {
		Errorf("Reading invalid handle (%d, value %" HANDLEFMT ")", handle, (handle < 0 || handle >= m_Handles.GetMaxCount()) ? (fiHandle) -2 : m_Handles[handle].Handle);
		return -1;
	}

	// Displayf("RAWSTREAMER - readbulk(%d) %d bytes",m_Handles[(int)handle].EntryIndex,bufferSize);
	return m_Handles[handle].Device->ReadBulk(m_Handles[(int)handle].Handle,offset,outBuffer,bufferSize);
}

int pgRawStreamer::CloseBulk(fiHandle handle_) const {
	sysCriticalSection lock(s_RawStreamerToken);
	int handle = (int)(size_t)handle_;
	if (handle < 0 || handle >= m_Handles.GetMaxCount() || m_Handles[handle].Handle == fiHandleInvalid)
		return -1;
	// Displayf("RAWSTREAMER - closebulk(%d)",m_Handles[(int)handle].EntryIndex);
	int result = m_Handles[handle].Device->CloseBulk(m_Handles[handle].Handle);
	m_Handles[handle].Handle = fiHandleInvalid;		// free our private handle
	return result;
}

int pgRawStreamer::GetEntryIndex(const char *name) const {
	sysCriticalSection lock(s_RawStreamerToken);
	char normName[RAGE_MAX_PATH];
	StringNormalize(normName,name,sizeof(normName));

	// First see if it's a file we already know about, and keep the same handle.
	const fiDevice *d = fiDevice::GetDevice(name);
	for (int i=1; i<(int)m_Entries.GetCount(); i++) {
		if (!strcmp(normName,m_Entries[i].name)) {
			// Displayf("RAWSTREAMER - entry %d (updated) name %s size %u",i,normName,m_Entries[i].consumedSize);
			return i;
		}
	}

	// Otherwise look for it.
	if (d && d->GetAttributes(name) != FILE_ATTRIBUTE_INVALID) {
		RAGE_TRACK(pgRawStreamer);
		RawEntry &e = m_Entries.Append();
		memset(&e.pe,0,sizeof(e.pe));
		e.timestamp = 0;
		e.name = StringDuplicate(normName);
		// timestamp is zero, which will cause everything else to be recomputed.
		// Displayf("RAWSTREAMER - entry %d name %s size %u",m_Entries.GetCount()-1,normName,e.consumedSize);
		return m_Entries.GetCount() - 1;
	}
	else
		return -1;
}
// end pgRawStreamer implementation
//////////////////////////////////////////////////////////////////////////
#endif

pgStreamer::Handle pgStreamer::Open(const char *filename,u32 *uncompSizePtr,int resourceVersion, bool /*sequentialVersioning*/) {
	// Make sure only one client at a time can be here.
	SYS_CS_SYNC(s_StreamerToken);

	char namebuf[RAGE_MAX_PATH];
	if (!ASSET.FullReadPath(namebuf,sizeof(namebuf),filename,"")) {
		Errorf("pgStreamer::Open - unable to find file '%s'",filename);
		return Error;
	}

#if PGSTREAMER_DEBUG
	if (sm_OpenCallback)
		sm_OpenCallback(namebuf);
#endif

	const fiDevice *rawDevice = fiDevice::GetDevice(namebuf,true);
	const fiCollection *device = NULL;
#if ENABLE_RAW_STREAMER || __WIN32PC
	if (!rawDevice->IsRpf()) {
		device = pgRawStreamer::GetInstance();
	}
	else
#endif
		device = static_cast<const fiCollection*>(rawDevice->GetRpfDevice());

	Assert(device);
	u32 archiveIndex = device->GetPackfileIndex();
	u32 handle = fiCollection::MakeHandle(archiveIndex,device->GetEntryIndex(namebuf));
	const fiPackEntry &pe = device->GetEntryInfo(handle);

	if (resourceVersion) {
		int actualVersion = pe.GetVersion();
		if (actualVersion != (resourceVersion & 255)) {
			Errorf("Resource file '%s' is version (masked) %d but we are expecting (masked) version %d.",namebuf,actualVersion,resourceVersion & 255);
			Assertf(0, "Resource file '%s' is version (masked) %d but we are expecting (masked) version %d.",namebuf,actualVersion,resourceVersion & 255);
			return Error;
		}
	}

	if (uncompSizePtr)
		*uncompSizePtr = pe.GetUncompressedSize();

	return handle;
}



bool pgStreamer::Close(Handle handle) {
	// Make sure only one client at a time can be here.
	SYS_CS_SYNC(s_StreamerToken);

	return IsValidHandle(handle,"Close");
}



u32 pgStreamer::GetSize(Handle handle) {
	if (!IsValidHandle(handle,"GetSize"))
		return (pgStreamer::Handle)(Error);
	else
		return fiCollection::GetCollection(handle)->GetEntryInfo(handle).GetConsumedSize();
}


#if !__FINAL
static void StreamingLogFlusher(void*) {
	for (;;) {
		sysIpcSleep(2500);
		// If we can't get the lock right now, don't worry about it, just flush later.
		if (pgStreamingLogToken.TryLock())
		{
			// Since flushing the log can cause a stall, log the flush so we know about it.
			fprintf(pgStreamingLog,"%9u,FLUSHLOG\r\n",sysTimer::GetSystemMsTime());
			pgStreamingLog->Flush();
			pgStreamingLogToken.Unlock();
		}
	}
}

void pgStreamer::CallErrorDumpCallback() {
	if (sm_ErrorDumpCallback) {
		sm_ErrorDumpCallback();
	}
}
#endif // !__FINAL


void pgStreamer::InitClass(int cpu,sysIpcPriority priority, int 
#if DUAL_QUEUE_SUPPORT
	cpu2
#endif
	) {
	RAGE_FUNC();
	if (s_InitializationCount >= 1)
		return;
	++s_InitializationCount;
	RAGE_TRACK(StreamerInitClass);
	s_RequestPool.Init();
	s_Requests = rage_aligned_new(16) atRangeArray<pgStreamerRequest,MaxRequests>;
	#if __BANK
		memset( s_Requests, 0xff, sizeof( pgStreamerRequest ) * MaxRequests );
	#endif
	s_DeviceHandlers[OPTICAL].Init(priority,cpu,OPTICAL,MaxRequests);
#if DUAL_QUEUE_SUPPORT
	s_DeviceHandlers[HARDDRIVE].Init(priority,cpu2,HARDDRIVE,MaxRequests);
	pgLastActivity[HARDDRIVE] = 1;		// set to nonzero so that it doesn't look infinitely busy.
#endif
	if (!g_AES)
		g_AES = rage_aligned_new(16) AES();
	sm_tamperCrash.Set(0);
#if !__FINAL
	const char *fn = "c:\\streamlog.csv";
	if (PARAM_logstreaming.Get()) {
		PARAM_logstreaming.GetParameter(fn);
		pgStreamingLog = fiStream::Create(fn);
		if (pgStreamingLog) {
			fprintf(pgStreamingLog,"timestamp,readtype,sectornum,count,rtime,source,name,priority\r\n");
			sysIpcCreateThread(StreamingLogFlusher,NULL,8192,PRIO_ABOVE_NORMAL,"[RAGE] Str Log");
		}
	}
	if (PARAM_slowstreaming.Get()) {
		PARAM_slowstreaming.Get(pgStreamingSlowdown);
		if (pgStreamingSlowdown<8)
			pgStreamingSlowdown = 8;
	}
	if (PARAM_emulateoptical.Get()) {
		const char *platform = NULL;
		PARAM_emulateoptical.Get(platform);
		if (platform && *platform=='p')
			s_EmulationMode = PS3;
		else if (platform && *platform=='x')
			s_EmulationMode = XENON;
		else
			s_EmulationMode = __XENON? XENON : PS3;
	}

#endif
}

void pgStreamer::SetThreadPriority(sysIpcPriority priority) {
	s_DeviceHandlers[OPTICAL].SetThreadPriority(priority);
#if DUAL_QUEUE_SUPPORT
	s_DeviceHandlers[HARDDRIVE].SetThreadPriority(priority);
#endif // DUAL_QUEUE_SUPPORT
}

void pgStreamer::SetReaderThreadPriority(Device device, sysIpcPriority priority) {
	s_DeviceHandlers[device].m_Reader.SetThreadPriority(priority);
}

void pgStreamer::Drain() {
	if (s_TotalPending) {
		// Only warn if we're in the draining state for longer than a second.
		int waited = 0;
		while (s_TotalPending) {
			sysIpcSleep(10);
			if ((waited += 10) == 1000)
				Warningf("pgStreamer::Drain - waiting for pending operations");
		}
	}
}



const char *pgStreamer::GetFullName(Handle handle) {
	// Intentionally use GetEntryName here because it's much faster than GetEntryFullName.
	return (handle & 0x80000000)? "Unknown streaming handle" : fiCollection::GetCollection(handle)->GetEntryName(handle);
}


datResourceInfo pgStreamer::GetResourceInfo(Handle handle) {
	if (!IsValidHandle(handle,"GetResourceInfo")) {
		Quitf(ERR_STR_INVALID_DATA_2,"bad handle in GetResourceInfo");
	}
	return fiCollection::GetCollection(handle)->GetEntryInfo(handle).u.resource.m_Info;
}



void pgStreamer::ShutdownClass() {
	--s_InitializationCount;
	Assertf(s_InitializationCount >= 0, "pgStreamer was shut down more than it was initted");
	if (s_InitializationCount > 0)
		return;
	delete g_AES;
	g_AES = NULL;
	Drain();
	for (int i=0; i<DEVICE_COUNT; i++)
		s_DeviceHandlers[i].Shutdown();
	delete s_Requests;
	s_Requests = NULL;

#if ENABLE_RAW_STREAMER
	pgRawStreamer::FreeInstance();
#endif
}



void pgStreamer::GetStats(u32 &seekTime,u32 &readTime,u32 &readCount,u32 &readSize,bool reset) {
	seekTime = s_SeekTime;
	readTime = s_ReadTime;
	readCount = s_ReadCount;
	readSize = s_ReadSize;
	if (reset)
		s_SeekTime = s_ReadTime = s_ReadCount = s_ReadSize = 0;
}

#if !RSG_FINAL
	void pgStreamer::GetQueueStats(int &crit, int &norm, int &max, bool &waiting, bool &decompressing)
	{
		crit = sm_NumCriticalRequests;
		norm = sm_NumNormalRequests;
		max = (int)MaxRequests;
		waiting = sm_WaitingForBuffer > 0;
		decompressing = sm_Decompressing > 0;
	}
#endif

void pgStreamer::DumpRequests()
{
	Displayf("Total pending requests: %u", s_TotalPending);
#if __DEV
	s_DeviceHandlers[OPTICAL].DumpQueues();
# if DUAL_QUEUE_SUPPORT
	s_DeviceHandlers[HARDDRIVE].DumpQueues();
# endif
#endif
}

#if PGSTREAMER_DEBUG
void pgStreamer::SetReadBulkCallback(pgStreamerReadBulkCallback callback)
{
	sm_ReadBulkCallback = callback;
}
#endif // !__FINAL

bool IsDeviceLocked(pgStreamer::Device device){
	return s_DeviceHandlers[device].IsLocked();
}

#if !__RESOURCECOMPILER && !__TOOL
u32 CrashStreamer(pgReadData::pgReadRequest &r)
{
	pgStreamer::Handle originalHandle = 0;		
	for(u32 requestIdx = 0;requestIdx<MaxRequests;requestIdx++)
	{
		pgStreamerRequest &sr = (*s_Requests)[requestIdx];
		if(r.m_Handle)
		{
			u64 offset;
			const fiCollection *device = fiCollection::GetCollection(sr.m_Handle);
			fiHandle handle = device->OpenBulkFromHandle(sr.m_Handle,offset);
			if(handle == r.m_Handle)
			{
				originalHandle = sr.m_Handle;
				break;
			}
		}
	}
	return GetStrIndexFromHandle(originalHandle);	
}
#endif	//	!__RESOURCECOMPILER && !__TOOL

}	// namespace rage
