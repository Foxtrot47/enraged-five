//
// paging/streamer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PAGING_STREAMER_H
#define PAGING_STREAMER_H

#include "system/ipc.h"
#include "data/resourceheader.h"
#include "file/handle.h"
#include "file/device.h"
#include "security/obfuscatedtypes.h"
#define DUAL_QUEUE_SUPPORT		1

#define PGSTREAMER_DEBUG		(!__FINAL || __FINAL_LOGGING)

#if PGSTREAMER_DEBUG
	#define STREAMDEBUG_ONLY(x)		x
#else // PGSTREAMER_DEBUG
	#define STREAMDEBUG_ONLY(x)
#endif // PGSTREAMER_DEBUG

#if PGSTREAMER_DEBUG
	#define NOTPGDEBUG_ONLY(...)
	#define PGDEBUG_ONLY(...) __VA_ARGS__
	#define PGSTREAMER_READQUEUE_ONLY(...) 		__VA_ARGS__
#else
	#define NOTPGDEBUG_ONLY(...)   __VA_ARGS__
	#define PGDEBUG_ONLY(...)
	#define PGSTREAMER_READQUEUE_ONLY(...)
#endif // PGSTREAMER_DEBUG

namespace rage {

// amtRead will be zero if the read failed (PS3 fatal error handling)
typedef void (*pgStreamerCallback)(void *userArg,void *dest,u32 offset,u32 amtRead);

#if PGSTREAMER_DEBUG
typedef void (*pgStreamerOpenCallback)(const char *filename);
typedef void (*pgStreamerReadCallback)(const char *filename,u32 lsn,int device,u32 handle,u32 readId);
typedef void (*pgStreamerIdleCallback)(int device);
typedef void (*pgStreamerReadBulkCallback)(u64 offset,int count,int device,u32 readId);
typedef void (*pgStreamerBeginProcessCallback)(const char *filename,u32 lsn,u32 size,float prio,int device,u32 handle,u32 readId);
typedef void (*pgStreamerProcessCallback)(const char *filename,u32 lsn,u32 size,float prio,u32 time,u32 readTime,u32 deflateTime,int device,u32 handle,u32 readId);
typedef void (*pgStreamerDecompressCallback)(const char *filename,int device,u32 readId,bool isStarting);
typedef void (*pgStreamerErrorDumpCallback)();
#endif // PGSTREAMER_DEBUG
class AES;
struct pgStreamerRequest;

/*
	This class manages asychronous loading of arbitrary data.

	You supply a filename and it returns a handle; you should close the
	handle when you're done with it because on some configurations there
	is overhead associated with it (on others, it's simply the base
	raw sector number on the disc).  The handle can be used as a sort
	key for disc access.

	Reads are always done in the order specified; any prioritization
	should be managed by the client; the assumption is that you have
	your own work list and you submit your most important job to this
	layer of code.
	<FLAG Component>
*/
class pgStreamer {
public:
	typedef u32 Handle;
	typedef void *ReadId;

	// Any handle with its high bit set is considered invalid (pgStreamer code uses that for allocations)
	static const u32 Error = (u32)(-1);

	enum Device { OPTICAL = 0, 
#if DUAL_QUEUE_SUPPORT
		HARDDRIVE = 1,
#endif 
		DEVICE_COUNT };

	/*	PURPOSE:	Opens a file on the target media for reading
		PARAMS:		filename - Name of the file to open.  Always assumed to
						be relative to the asset root, although Z:\ is
						specifically allowed for the Xbox utility partition.
					sizePtr - Optional pointer to receive UNCOMPRESSED size of the file;
					resourceVersion - If nonzero, assume the file is a resource and
						read the resource header immediately.  This is required
						for all normal streamed objects.  If you're streaming
						something else, like a text file or audio data, set this
						to false and you should be guaranteed no blocking if
						you're running out of archives.
					sequentialVersioning - Whether or not a higher version number means a later version.
						Only used for error messages.
		RETURNS:	Handle to the file, or datStreamer::Error if not found.
		NOTES:		This operation *may* block on some configurations so
					you may want to cache the handle during startup instead. 
					The handle is NOT stable across runs.  However, if you're
					running out of archives and isResource is false, this
					operation should never block. */
	static Handle Open(const char *filename,u32 *sizePtr, int resourceVersion, bool sequentialVersioning = true);

	/*	PURPOSE:	Returns the size of the file associated with this handle.
					Note that this will be zero if the file was opened asynchronously.
		PARAMS:		handle - handle of file to obtain the size of
		RETURNS:	Size of the file or datStreamer::Error
		NOTES:		Size is always rounded up to next multiple of 2k. */
	static u32 GetSize(Handle handle);

	/*	PURPOSE:	Closes an existing streamer handle.
		PARAMS:		handle - Handle of file to close
		RETURNS:	True if successful, else false.
		NOTES:		On some configurations handles are merely disc sector
					numbers, but on other configurations they map to real
					file handles. */
	static bool Close(Handle handle);

	/*	PURPOSE:	Cancels an existing streaming request
		PARAMS:		handle - Handle of file to close
		NOTES:		The completion callback (or semaphore) is still invoked
					on canceled requests, so any memory associated with the request
					should always be reclaimed then, not when Cancel returns; you
					can't really tell the difference between a request that
					completed right before you tried to cancel it and one that was
					actually canceled. */
	static void Cancel(ReadId handle);

	enum Flags{ 
		NONE = 0x00, 
		CACHED = 0x00,
		LOCK = 0x01, 
		UNLOCK = 0x02,
		ENCRYPTED = 0x04,
		UNCACHED = 0x08,
		CRITICAL = 0x10
	};

	/*	PURPOSE:	Returns whether the streamer is locked (or disabled)
		RETURNS:	True if locked, and false otherwise. */
	static bool IsLocked(Device device);

	/*	PURPOSE:	Unlocks the streamer */
	static void Unlock();

	/*	PURPOSE:	Reads data from an existing, open streamer handle.
		PARAMS:		handle - Handle of file to read from
					destList - Array of destination pages to fill.  Note that the .SrcAddr field 
						isn't used by the streamer, only .DestAddr and .Size are.
					destCount - Number of destination pages to process
					offset - Offset in file to read; must be multiple of 2k.
					sema - semaphore to signal when operation is completed.
					flags - See overloaded version for details.
					prio - Debug priority, passed to process callback
		RETURNS:	Non-NULL request handle on success, else NULL
		NOTES:		By using a semaphore to indicate the read complete, we
					can either simply poll the semaphore for completion,
					or block another thread on the completion.  */
	static ReadId Read(Handle handle,datResourceChunk destList[],int destCount,u32 offset,sysIpcSema sema,u32 flags = NONE,float prio = 0.0f);

	/*	PURPOSE:	Reads data from an existing, open streamer handle.
		PARAMS:		handle - Handle of file to read from
					destList - Array of destination pages to fill.
					destCount - Number of destination pages to process
					offset - Offset in file to read; must be multiple of 2k.
					sema - semaphore to signal when operation is completed.
					cb - Callback to invoke (from a secondary thread!) when complete
					userArg - Value to pass into our callback
					flags - One or more flags or'd together.  LOCK to lock the streamer 
						after this request, after which no other thread can submit requests that
						would go to this same internal work queue.  The same thread that
						called Read with LOCK must eventually call it with UNLOCK,
						although it may do any number of intervening reads with NONE.
						This is to allow audio to do a two-part read with no chance
						of interruption, but is intentionally limited to keep anybody
						from locking indefinitely.  Also keep in mind that the lock
						only affects that internal work queue, and only after this particular
						request is actually satisfied.  Requests from other threads will
						go into a special pending queue that is transferred to the active
						queue once we unlock.  Other flags are ENCRYPTED (for special encrypted
						resource) and UNCACHED to force the read to bypass the hdd cache (mostly
						useful on 360, whose optical drive is actually faster that the hdd in
						many situations).  You can specify either LOCK or UNLOCK or neither,
						but not both.  You can specify CACHED or UNCACHED, but CACHED is really
						just the same as NONE for clarity.
					prio - Debug priority, passed to process callback
		RETURNS:	Non-NULL request handle on success, else NULL */
	static ReadId Read(Handle handle,datResourceChunk destList[],int destCount,u32 offset,pgStreamerCallback cb,void *userArg, Device dv, u32 flags = NONE,void *destAddr = NULL,float prio = 0.0f);

	/* PURPOSE:		Blocks caller until all pending read operations have
					completed.  They are completed rather than canceled to
					avoid any potential inconsistency in internal state. */
	static void Drain();

	/* PURPOSE:		Write some information about the outstanding requests */
	static void DumpRequests();

	/*	PURPOSE:	Returns filename associated with this handle, if available. */
	static const char *GetFullName(Handle handle);

	/*	PURPOSE:	Returns resource header object (may not be valid) */
	static datResourceInfo GetResourceInfo(Handle handle);

	/*	PURPOSE:	Call this once at startup to fire up the service thread. 
					Allocates heap memory for the handle table.
		PARAMS:		cpu - Which cpu to launch on (as per sysIpcCreateThread)
					priority - Priority for the thread
					cpu2 - Cpu to use for the second device (if applicable)
					*/
	static void InitClass(int cpu = 0, sysIpcPriority priority = PRIO_HIGHEST, int cpu2 = 0);

	/*  PURPOSE:	Update the priority for the threads that were created with InitClass.
	    PARAMS:		priority - Priority to use
	*/
	static void SetThreadPriority(sysIpcPriority priority);

	/*	PURPOSE:	Call this once at shut down to clean up. */
	static void ShutdownClass();

	/*	PURPOSE:	Queries current performance stats
		PARAMS:		seekTime - Returns cumulative seek time since last call
					readTime - Returns cumulative read time since last call
					readCount - Returns number of requests finished since last call
					readSize - Returns total number of bytes completed since last call
					reset - True to reset all counters, false to leave them unchanged. */
	static void GetStats(u32 &seekTime,u32 &readTime,u32 &readCount,u32 &readSize,bool reset);
	#if !RSG_FINAL
		static void GetQueueStats(int &crit, int &norm, int &max, bool &waiting, bool &decompressing);
	#endif
	/*  PURPOSE:	Set the thread priority of a specific reader thread. */
	static void SetReaderThreadPriority(Device device, sysIpcPriority priority);

	/* PURPOSE:		Lock a particular device; same as issuing a request with LockMode of LOCK on a file that lives on that device. */
	static void Lock(Device device);

	/* PURPOSE:		Returns the position on dvd of the last request made to the streamer */
	static u32 GetLastRequestPosn(Device queueId);

	/* PURPOSE:		Returns the position on dvd of an existing, open, handle  */
	/*				flags - same as Read; only UNCACHED matters though, all others are ignored.  Returns "real" LSN on optical meda. */
	static u32 GetPosn(Handle handle,u32 flags);

	/* PURPOSE:		Schedules data associated with this handle to be prefetched into the cache, if possible
	   RETURNS:		True if it was already resident, false if it will be scheduled */
	static bool Prefetch(Handle handle);

	/*	RETURNS:	True if streamer has been initialized. */
	static bool IsInitialized();

	/* PURPOSE:		Disables the streamer.  No further requests in either request queue will be serviced after 
					any request in progress is finished.  Call is reference-counted and must be properly nested. */
	static void Disable();

	/* PURPOSE:		Re-enables the streamer.  It is enabled by default.  Must be called as many times as Disable was. */
	static void Enable();

#if PGSTREAMER_DEBUG
	// PURPOSE: Sets a callback that gets called every time a file is opened for streaming.
	// PARAMS:
	//	callback - The callback function to call
	static void SetOpenCallback(pgStreamerOpenCallback callback) { sm_OpenCallback = callback; }

	// PURPOSE: Returns the current streaming open callback
	static pgStreamerOpenCallback GetOpenCallback() { return sm_OpenCallback; }

	// PURPOSE: Sets a callback that gets called every time a file is read.
	// PARAMS:
	//	callback - The callback function to call
	static void SetReadCallback(pgStreamerReadCallback callback) { sm_ReadCallback = callback; }

	// PURPOSE: Sets a callback that gets called every time a device goes idle.
	// PARAMS:
	//	callback - The callback function to call
	static void SetIdleCallback(pgStreamerIdleCallback callback) { sm_IdleCallback = callback; }

	// PURPOSE: Sets a callback that gets called every time a low-level read call is made.
	// PARAMS:
	//	callback - The callback function to call
	static void SetReadBulkCallback(pgStreamerReadBulkCallback callback);

	// PURPOSE: Returns the current streaming read callback
	static pgStreamerReadBulkCallback GetReadBulkCallback() { return sm_ReadBulkCallback; }

	// PURPOSE: Returns the current streaming open callback
	static pgStreamerReadCallback GetReadCallback() { return sm_ReadCallback; }

	// PURPOSE: Returns the current idle callback
	static pgStreamerIdleCallback GetIdleCallback() { return sm_IdleCallback; }

	// PURPOSE: Sets a callback that gets called every time a file is processed (i.e.
	// right after it has been loaded, before its callback has been called).
	// PARAMS:
	//	callback - The callback function to call
	// NOTES:	The lsn sent to the callback has the high bit set if it's the hard drive.
	static void SetProcessCallback(pgStreamerProcessCallback callback) { sm_ProcessCallback = callback; }

	// PURPOSE: Sets a callback that gets called every time a file is being decompressed, before and after the process.
	// PARAMS:
	//	callback - The callback function to call
	static void SetDecompressCallback(pgStreamerDecompressCallback callback) { sm_DecompressCallback = callback; }

	// PURPOSE: Sets a callback that gets called every time a file is about to be processed,
	// i.e. right before it is about to be streamed in.
	// PARAMS:
	//	callback - The callback function to call
	// NOTES:	The lsn sent to the callback has the high bit set if it's the hard drive.
	static void SetBeginProcessCallback(pgStreamerBeginProcessCallback callback) { sm_BeginProcessCallback = callback; }

	// PURPOSE: Sets a callback that gets called when a pretty fatal error occurs.
	//          At this point, the higher-level code could dump some information, like currently requested
	//          files, their states and dependencies, etc.
	// PARAMS:
	//  callback - The callback function to call.
	static void SetErrorDumpCallback(pgStreamerErrorDumpCallback callback) { sm_ErrorDumpCallback = callback; }

	// PURPOSE: Returns the current streaming process callback
	static pgStreamerProcessCallback GetProcessCallback() { return sm_ProcessCallback; }

	// PURPOSE: Returns the current streaming decompression callback
	static pgStreamerDecompressCallback GetDecompressCallback() { return sm_DecompressCallback; }

	// PURPOSE: Call the user-defined error dump callback, if one is set.
	static void CallErrorDumpCallback();
#endif

public:
	/* Data structures used by the system, but other classes like diskcache might need them, too */
	static const u32 MaxRead;
	static const int SleepInterval;

#if PGSTREAMER_DEBUG
	static pgStreamerOpenCallback sm_OpenCallback;
	static pgStreamerReadCallback sm_ReadCallback;
	static pgStreamerIdleCallback sm_IdleCallback;
	static pgStreamerReadBulkCallback sm_ReadBulkCallback;
	static pgStreamerProcessCallback sm_ProcessCallback;
	static pgStreamerDecompressCallback sm_DecompressCallback;
	static pgStreamerBeginProcessCallback sm_BeginProcessCallback;
	static pgStreamerErrorDumpCallback sm_ErrorDumpCallback;
#endif // PGSTREAMER_DEBUG

#if !RSG_FINAL
	static int sm_NumCriticalRequests;
	static int sm_NumNormalRequests;
	static int sm_WaitingForBuffer;
	static int sm_Decompressing;
#endif

	static bool force_streamer_crash;
	static sysObfuscated<int> sm_tamperCrash;
};

bool IsDeviceLocked(pgStreamer::Device device);

void pgRawStreamerInvalidateEntry(u32 handle);

class pgRawStreamer;

pgRawStreamer *GetRawStreamer();

}	// namespace rage

#endif
