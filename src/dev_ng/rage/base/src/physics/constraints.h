//
// physics/constraints.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef PHYSICS_CONSTRAINTS_H
#define PHYSICS_CONSTRAINTS_H

#include "constraintcylindrical.h"
#include "constraintdistance.h"
#include "constraintfixed.h"
#include "constraintfixedrotation.h"
#include "constrainthalfspace.h"
#include "constrainthinge.h"
#include "constraintprismatic.h"
#include "constraintrotation.h"
#include "constraintspherical.h"

#endif	// PHYSICS_CONSTRAINTS_H
