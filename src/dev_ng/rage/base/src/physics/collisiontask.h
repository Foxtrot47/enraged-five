#ifndef PHYSICS_COLLISIONTASK_H
#define PHYSICS_COLLISIONTASK_H

struct SpuMidPhaseTaskDesc;

namespace rage {
int	HandleMidphaseTask(SpuMidPhaseTaskDesc& taskDesc);
}	// namespace rage

#endif // PHYSICS_COLLISIONTASK_H
