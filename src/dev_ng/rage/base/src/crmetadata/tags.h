// 
// crmetadata/tags.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 


#ifndef CRMETADATA_TAGS_H
#define CRMETADATA_TAGS_H

#include "tag.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Manages a time line of tags  
// Performs containment, sorting, searching and editing operations.
class crTags
{
public:

	// PURPOSE: Default constructor
	crTags();

	// PURPOSE: Copy constructor
	crTags(const crTags&);

	// PURPOSE: Assignment operator
	crTags& operator=(const crTags& other);

	// PURPOSE: Resource constructor
	crTags(datResource&);

	// PURPOSE: Destructor
	~crTags();

	// PURPOSE: Placement
	DECLARE_PLACE(crTags);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Free/release dynamic resources
	void Shutdown();

	// PURPOSE: Destroy tags known references
	void Destroy();

	// PURPOSE: Clone
	crTags* Clone() const;

#if CR_DEV
	// PURPOSE: Serialization
	void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	void Dump(crDumpOutput& output) const;
#endif // CR_DEV

	// PURPOSE: Has tags
	// RETURNS: true - tag(s) are present, false - no tags contained
	bool HasTags() const;

	// PURPOSE: Has tags with property name
	// PARAMS: name - tag/property name to search for
	// RETURNS: true - tag(s) with name are present, false - no matching tags found
	// NOTES: Available on resourced tags, as name is first hashed and key is then compared.
	bool HasTags(const char* name) const;

	// PURPOSE: Has tags with property key
	// PARAMS: key - tag/property hash key to search for
	// RETURNS: true - tag(s) with hash key are present, false - no matching tags found
	bool HasTags(crTag::Key key) const;

	// PURPOSE: Count the total number of tags
	// RETURNS: Total number of tags contained
	int GetNumTags() const;

	// PURPOSE: Count number of tags with property name
	// PARAMS: name - tag/property name to search for
	// RETURNS: Number of tags found with matching name
	// NOTES: Available on resourced tags, as name is first hashed and key is then compared.
	int GetNumTags(const char* name) const;

	// PURPOSE: Get number of tags with property key
	// PARAMS: key - tag/property hash key to search for
	// RETURNS: Number of tags found with matching hash key
	int GetNumTags(crTag::Key key) const;

	// PURPOSE: Get tag by index
	// PARAMS: index - tag index [0..numTags-1]
	// NOTES: Tag indices are volatile, they may change with any operation
	// that triggers a call to SortTags (i.e. adding/removing/editing a tag etc)
	const crTag* GetTag(int index) const;
	crTag* GetTag(int index);

	// PURPOSE: Find the index for a tag
	// PARAMS: tag - reference to the tag to search for
	// RETURNS: tag index [0..numTags-1], or -1 if no tag was found
	// NOTES: Tag indices are volatile, they may change with any operation
	// that triggers a call to SortTags (i.e. adding/removing/editing a tag etc)
	int FindTag(const crTag& tag) const;

	// PURPOSE:  Adds a new tag to container
	// PARAMS: tag - new tag to add (will be cloned)
	// RETURNS: pointer to the newly added tag
	// NOTES: The caller remains responsible for any deallocation of the passed in tag, 
	// while container assumes responsibility for the newly cloned copy.
	crTag* AddTag(const crTag& tag);

	// PURPOSE:  Adds a new tag to container, but only if unique
	// PARAMS: tag - new tag to add (will be cloned)
	// RETURNS: pointer to the newly added tag
	// NOTES: The caller remains responsible for any deallocation of the passed in tag, 
	// while container assumes responsibility for the newly cloned copy (if one is created).
	// Uniqueness of tag is found by comparing both tag timing and property contents.
	// If both match exactly, existing tag returned instead of adding a duplicate.
	crTag* AddUniqueTag(const crTag& tag);

	// PURPOSE: Delete tag
	// PARAMS: tag - tag to remove
	// RETURNS: true - if tag found and deleted, false - failed to find tag
	bool RemoveTag(crTag& tag);

	// PURPOSE: Delete all tags with property name
	// PARAMS: name - tag/property name to search for
	// RETURNS: true - tag(s) found and deleted, false - failed to find any tags
	// NOTES: Available on resourced tags, as name is first hashed and key is then compared.
	bool RemoveAllTags(const char* name);

	// PURPOSE: Delete all tags with property key
	// PARAMS: key - tag/property hash key to search for
	// RETURNS: true - tag(s) found and deleted, false - failed to find any tags
	bool RemoveAllTags(crTag::Key key);

	// PURPOSE: Delete all tags
	// RETURNS: true - tag(s) deleted, false - no tags to delete
	bool RemoveAllTags();


	// PURPOSE: Tag callback handler (const)
	class ConstTagsCallback
	{
	public:

		// PURPOSE: Constructor
		ConstTagsCallback();

		// PURPOSE: Destructor
		virtual ~ConstTagsCallback();

		// PURPOSE: Const callback, override to handle
		// RETURNS: true - to continue callbacks, false - to terminate iteration
		virtual bool ConstCallback(const crTag&) = 0;
	};

	// PURPOSE: Tag callback handler (non-const)
	class TagsCallback
	{
	public:

		// PURPOSE: Constructor
		TagsCallback();

		// PURPOSE: Destructor
		virtual ~TagsCallback();

		// PURPOSE: Non-const callback, override to handle
		// RETURNS: true - to continue callbacks, false - to terminate iteration
		virtual bool Callback(crTag&) = 0;
	};


	// PURPOSE: Iterate all tags with matching names (const and non-const versions)
	// PARAM: name - name to search tags properties for
	// cb - tag callback object (derive to specialize)
	// RETURNS: true - all tags iterated, false - iteration terminated early
	// NOTES: Available on resourced tags, as name is first hashed and key is then compared.
	bool ForAllTags(const char* name, ConstTagsCallback& cb) const;
	bool ForAllTags(const char* name, TagsCallback& cb);

	// PURPOSE: Iterate all tags with matching keys (const and non-const versions)
	// PARAM: key - hash key to search tag properties for
	// cb - tag callback object (derive to specialize)
	// RETURNS: true - all tags iterated, false - iteration terminated early
	bool ForAllTags(crTag::Key key, ConstTagsCallback& cb) const;
	bool ForAllTags(crTag::Key key, TagsCallback& cb);

	// PURPOSE: Iterate all tags (const and non-const versions)
	// PARAM: cb - tag callback object (derive to specialize)
	// RETURNS: true - all tags iterated, false - iteration terminated early
	bool ForAllTags(ConstTagsCallback& cb) const;
	bool ForAllTags(TagsCallback& cb);


	// PURPOSE: Resort the tags to maintain internal interval ordering
	// NOTES: Must be called if tag intervals are modified by functions 
	// external to this manager, or results will be undefined.
	void SortTags();


	// PURPOSE: Copies tags from other tag container (destroying existing ones)
	// PARAMS: otherTags - other tag container to copy tags from
	// NOTES: Destructive to existing tags in the container, 
	// they are removed and replaced with copies of the other tags.
	void CopyTags(const crTags& otherTags);


protected:

	// PURPOSE: Internal function, check for tags in advance
	void CalcHasTags();

	// PURPOSE: Internal function, serializes old tag/property format
	void SerializeTagLegacy(datSerialize& s, crTag& tag);

private:

	atArray< datOwner<crTag> > m_Tags;
	bool m_HasBlockTags;
	datPadding<7> m_Padding;

	template<typename T> friend class crTagIteratorBase;
};

////////////////////////////////////////////////////////////////////////////////

extern __THREAD int s_TagsSerializeVersion;

////////////////////////////////////////////////////////////////////////////////

inline bool crTags::HasTags() const
{
	return m_Tags.GetCount() > 0;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTags::HasTags(const char* name) const
{
	return HasTags(crTag::CalcKey(name));
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTags::HasTags(crTag::Key key) const
{
	if(key == crTag::CalcKey("Block", 0xE433D77D))
	{
		return m_HasBlockTags;
	}

	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		const crTag& tag = *m_Tags[i];
		if(tag.GetKey() == key)
		{
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline int crTags::GetNumTags() const
{
	return m_Tags.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline int crTags::GetNumTags(const char* name) const
{
	return GetNumTags(crTag::CalcKey(name));
}

////////////////////////////////////////////////////////////////////////////////

inline const crTag* crTags::GetTag(int index) const
{
	return m_Tags[index];
}

////////////////////////////////////////////////////////////////////////////////

inline crTag* crTags::GetTag(int index)
{
	return m_Tags[index];
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTags::RemoveAllTags(const char* name)
{
	return RemoveAllTags(crTag::CalcKey(name));
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTags::ForAllTags(const char* name, ConstTagsCallback& cb) const
{
	return ForAllTags(crTag::CalcKey(name), cb);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTags::ForAllTags(const char* name, TagsCallback& cb)
{
	return ForAllTags(crTag::CalcKey(name), cb);
}

////////////////////////////////////////////////////////////////////////////////

} //namespace rage

#endif // CRMETADATA_TAGS_H



