// 
// crmetadata/properties.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CRMETADATA_PROPERTIES_H 
#define CRMETADATA_PROPERTIES_H 

#include "property.h"

#include "atl/map.h"
#include "string/stringhash.h"

namespace rage 
{

class crDumpOutput;
class crProperty;
class datSerialize;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Manages a collection of properties (timeless metadata).  
// Performs containment, efficient retrieval and editing operations, plus serialization.
class crProperties
{
public:

	// PURPOSE: Default constructor
	crProperties();

	// PURPOSE: Resource constructor
	crProperties(datResource&);

	// PURPOSE: Copy constructor
	crProperties(const crProperties& other);

	// PURPOSE: Assignment operator
	crProperties& operator=(const crProperties& other);

	// PURPOSE: Destructor
	~crProperties();


	// PURPOSE: Placement
	DECLARE_PLACE(crProperties);

	// PURPOSE: Off line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Free/release dynamic resources
	void Shutdown();

	// PURPOSE: Destroy properties known references
	void Destroy();

#if CR_DEV
	// PURPOSE: Allocates and loads a properties from file
	// PARAMS: 
	// filename - properties file name
	// RETURNS: pointer to newly constructed properties, or NULL if load failed.
	static crProperties* AllocateAndLoad(const char* filename);

	// PURPOSE: Save properties in independent .properties file
	// PARAMS: 
	// filename - properties file name
	// RETURNS: true - success, false - failed to save
	bool Save(const char* filename);

	// PURPOSE: Load properties from independent .properties file
	// PARAMS: 
	// filename - properties file name
	// RETURNS: true - success, false - failed to load
	bool Load(const char* filename);

	// PURPOSE: Serialization
	void Serialize(datSerialize&);

	// PURPOSE: Dump properties to output, for debugging
	void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Clone
	crProperties* Clone() const;

	// PURPOSE: Convert property name to property key
	static crProperty::Key CalcKey(const char* propertyName);

	// PURPOSE: Find property (timeless metadata) by name
	// PARAMS: propertyName - string containing property name
	// RETURNS: const pointer to property (NULL if no matching property found)
	// NOTES: Available on resources, hashes name to retrieve by hash key match
	// Performance is poorer than getting attribute directly using a hash key.
	const crProperty* FindProperty(const char* propertyName) const;

	// PURPOSE: Find property (timeless metadata) by name
	// PARAMS: propertyName - string containing property name
	// RETURNS: pointer to property (NULL if no matching property found)
	// NOTES: Available on resources, hashes name to retrieve by hash key match
	// Performance is poorer than getting attribute directly using a hash key.
	crProperty* FindProperty(const char* propertyName);

	// PURPOSE: Find property (timeless metadata) by key
	// PARAMS: propertyKey - hash of property name
	// RETURNS: const pointer to property (NULL if no matching property found)
	// NOTES: Better performance than getting attribute via name
	const crProperty* FindProperty(crProperty::Key propertyKey) const;

	// PURPOSE: Find property (timeless metadata) by key
	// PARAMS: propertyKey - hash of property name
	// RETURNS: pointer to property (NULL if no matching property found)
	// NOTES: Better performance than getting attribute via name
	crProperty* FindProperty(crProperty::Key propertyKey);

	// PURPOSE: Find if property present by name
	// PARAMS: propertyName - string containing property name
	// RETURNS: true - if property present, false - if not found
	// NOTES: Available on resources, hashes name to retrieve by hash key match
	// Performance is poorer than getting attribute directly using a hash key.
	bool HasProperty(const char* propertyName) const;

	// PURPOSE: Find if property present by key
	// PARAMS: propertyKey - hash of property name
	// RETURNS: true - if property present, false - if not found
	// NOTES: Better performance than getting attribute via name
	bool HasProperty(crProperty::Key propertyKey) const;


	// PURPOSE: Add property
	// PARAMS: prop - property to add (will be cloned and copy will be added, 
	// caller still responsible for any destruction required on the original)
	// NOTES: If property with that name already exists, it will be replaced with 
	// new property.  
	// Warning; Properties are hashed internally, this can lead to hash collisions.
	void AddProperty(const crProperty& prop);

	// PURPOSE: Remove property by name
	// PARAMS: propertyName - string containing property name
	// RETURNS: true - if property found and deleted, false - if not found
	bool RemoveProperty(const char* propertyName);

	// PURPOSE: Remove property by key
	// PARAMS: propertyKey - hash of property name
	// RETURNS: true - if property found and deleted, false - if not found
	bool RemoveProperty(crProperty::Key propertyKey);

	// PURPOSE: Remove properties by string (with wild cards)
	// PARAMS: propertyNameWithWildcards - string containing property name (supports * and ? wild cards)
	// RETURNS: number of properties found and deleted
	// NOTES: Only available with non-resourced properties.  Names are discarded during
	// resourcing, so partial wild card based matching no longer possible after this.
	int RemoveProperties(const char* propertyNameWithWildcards);

	// PURPOSE: Remove all properties
	void RemoveAllProperties();


	// PURPOSE: Property callback handler (const)
	class ConstPropertiesCallback
	{
	public:

		// PURPOSE: Constructor
		ConstPropertiesCallback();

		// PURPOSE: Destructor
		virtual ~ConstPropertiesCallback();

		// PURPOSE: Const callback, override to handle
		// RETURNS: true - to continue callbacks, false - to terminate iteration
		virtual bool ConstCallback(const crProperty&) = 0;
	};

	// PURPOSE: Property callback handler (non-const)
	class PropertiesCallback
	{
	public:

		// PURPOSE: Constructor
		PropertiesCallback();

		// PURPOSE: Destructor
		virtual ~PropertiesCallback();

		// PURPOSE: Non-const callback, override to handle
		// RETURNS: true - to continue callbacks, false - to terminate iteration
		virtual bool Callback(crProperty&) = 0;
	};

	// PRUPOSE: Iterate all the properties, const version
	// PARAM: cb - const property callback object (derive to specialize)
	// RETURNS: true - all properties iterated, false - iteration terminated early
	bool ForAllProperties(ConstPropertiesCallback& cb) const;

	// PRUPOSE: Iterate all the properties, non-const version
	// PARAM: cb - non-const property callback object (derive to specialize)
	// RETURNS: true - all properties iterated, false - iteration terminated early
	bool ForAllProperties(PropertiesCallback& cb);


	// PURPOSE: Find properties by string (with wild cards), const version
	// PARAMS: propertyName - string containing property name (supports * and ? wild cards)
	// cb - const property callback object (derive to specialize)
	// RETURNS: number of properties found
	// NOTES: Only available with non-resourced properties.  Names are discarded during
	// resourcing, so partial wild card based matching no longer possible after this.
	int FindProperties(const char* propertyNameWithWildcards, ConstPropertiesCallback& cb) const;

	// PURPOSE: Find properties by string (with wild cards), non-const version
	// PARAMS: propertyName - string containing property name (supports * and ? wild cards)
	// cb - non-const property callback object (derive to specialize)
	// RETURNS: number of properties found
	// NOTES: Only available with non-resourced properties.  Names are discarded during
	// resourcing, so partial wild card based matching no longer possible after this.
	int FindProperties(const char* propertyNameWithWildcards, PropertiesCallback& cb);


	// PURPOSE: Get property by index, const version
	// RETURNS: pointer to property
	// NOTES: Performance of iteration by index is *extremely* poor - prefer ForAllProperties.
	const crProperty* FindPropertyByIndex(int) const;

	// PURPOSE: Get property by index, non-const version
	// RETURNS: pointer to property
	// NOTES: Performance of iteration by index is *extremely* poor - prefer ForAllProperties.
	crProperty* FindPropertyByIndex(int);

	// PURPOSE: Get property name by index
	// RETURNS: property name string
	// NOTES: Performance of iteration by index is *extremely* poor - prefer ForAllProperties.
	const char* FindPropertyNameByIndex(int) const;

	// PURPOSE: Get number of properties
	// NOTES: Expensive operation, iterates all properties to calculate total count
	int GetNumProperties() const;

	// PURPOSE: Copy properties manager
	void CopyProperties(const crProperties& other);

protected:

	// PURPOSE: Update a property (internal use only)
	void UpdatePropertyInternal(crProperty& prop, crProperty::Key oldKey);

	// PURPOSE: Internal function for serializing old property format
	bool SerializePropertyLegacy(datSerialize&, crProperty& prop);

	// PURPOSE: Resource fix up function
	static void FixupDataFunc(datResource &rsc, datOwner<crProperty>&);

private:

	typedef atMap<u32, datOwner<crProperty> > PropertyMap;
	PropertyMap m_Properties;

	friend class crClip;
	friend class crProperty;
};

////////////////////////////////////////////////////////////////////////////////

extern __THREAD int s_PropertiesSerializeVersion;

////////////////////////////////////////////////////////////////////////////////

inline crProperty::Key crProperties::CalcKey(const char* propertyName)
{
	Assert(propertyName && propertyName[0]);

	return crProperty::Key(atStringHash(propertyName));
}

////////////////////////////////////////////////////////////////////////////////

inline const crProperty* crProperties::FindProperty(const char* propertyName) const
{
	return FindProperty(CalcKey(propertyName));
}

////////////////////////////////////////////////////////////////////////////////

inline crProperty* crProperties::FindProperty(const char* propertyName)
{
	return FindProperty(CalcKey(propertyName));
}

////////////////////////////////////////////////////////////////////////////////

inline const crProperty* crProperties::FindProperty(crProperty::Key propertyKey) const
{
	const datOwner<crProperty>* pprop = m_Properties.Access(u32(propertyKey));
	return pprop?*pprop:NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline crProperty* crProperties::FindProperty(crProperty::Key propertyKey)
{
	datOwner<crProperty>* pprop = m_Properties.Access(u32(propertyKey));
	return pprop?*pprop:NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crProperties::HasProperty(const char* propertyName) const
{
	return HasProperty(CalcKey(propertyName));
}

////////////////////////////////////////////////////////////////////////////////

inline bool crProperties::HasProperty(crProperty::Key propertyKey) const
{
	return FindProperty(propertyKey) != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crProperties::RemoveProperty(const char* propertyName)
{
	return RemoveProperty(CalcKey(propertyName));
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMETADATA_PROPERTIES_H 
