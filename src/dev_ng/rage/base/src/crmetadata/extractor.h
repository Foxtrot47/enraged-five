// 
// crmetadata/metadataextractor.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "atl/array.h"
#include "atl/functor.h"
#include "cranimation/animation_config.h"

#if CR_DEV

namespace rage 
{

class crMetadataExtractor
{
public:
	// PURPOSE: Functor used for serialization
	typedef Functor1<datSerialize&> Functor; 

	// PURPOSE: Default constructor
	crMetadataExtractor();

	// PURPOSE: Default destructor
	~crMetadataExtractor();

	// PURPOSE: Add callback to the extractor
	// PARAM: 
	// functor - class method used for serialization
	// markerBegin - string of the begin marker
	// markerEnd - string of the end marker
	void Add(const Functor& functor, const char* markerBegin, const char* markerEnd);

	// PURPOSE: Load metadata from file
	// PARAM: filename - pathname with extension to load
	bool Load(const char* filename);

	// PURPOSE: Save metadata into file
	// PARAM: filename - pathname with extension to save
	bool Save(const char* filename);

private:
	// PURPOSE: Seek stream to marker
	// RETURNS: true if the marker has been found
	static bool FindMarker(class fiStream* f, const char* marker, u32 size);

	u8* m_Padding;
	u32 m_SizeBegin;
	u32 m_SizeEnd;

	struct Callback
	{
		Functor m_Functor;
		const char* m_MarkerBegin;
		const char* m_MarkerEnd;
		u32 m_SeekBegin;
		u32 m_SeekEnd;
	};
	atArray<Callback> m_Callbacks;
};

} // namespace rage

#endif // CR_DEV
