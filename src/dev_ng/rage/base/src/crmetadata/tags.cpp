// 
// crmetadata/tags.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "tags.h"

#include "tag.h"
#include "dumpoutput.h"
#include "propertyattributes.h"

#include "atl/array_struct.h"
#include "data/safestruct.h"
#include "data/serialize.h"
#include "math/simplemath.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crTags::crTags()
: m_HasBlockTags(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crTags::crTags(const crTags& other)
{
	CopyTags(other);
}

////////////////////////////////////////////////////////////////////////////////

crTags& crTags::operator=(const crTags& other)
{
	CopyTags(other);

	return *this;
}

////////////////////////////////////////////////////////////////////////////////

crTags::crTags(datResource& rsc)
: m_Tags(rsc, true)
{
	// BEGIN TEMP - UNTIL NEXT RESOURCE CHANGE GOES THROUGH
	CalcHasTags();
	// END TEMP - UNTIL NEXT RESOURCE CHANGE GOES THROUGH
}

////////////////////////////////////////////////////////////////////////////////

crTags::~crTags()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crTags);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crTags::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crTags)
	SSTRUCT_FIELD(crTags, m_Tags)
	SSTRUCT_FIELD(crTags, m_HasBlockTags)
	SSTRUCT_IGNORE(crTags, m_Padding)
	SSTRUCT_END(crTags)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crTags::Shutdown()
{
	RemoveAllTags();
}

////////////////////////////////////////////////////////////////////////////////

class DestroyTagsCallback : public crTags::TagsCallback
{
public:
	virtual bool Callback(crTag& tag)
	{
		tag.Destroy();
		return true;
	}
};

////////////////////////////////////////////////////////////////////////////////

void crTags::Destroy()
{
	DestroyTagsCallback destroyTagsCb;
	ForAllTags(destroyTagsCb);
}

////////////////////////////////////////////////////////////////////////////////

crTags* crTags::Clone() const
{
	return rage_new crTags(*this);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
int tagsVersions[] = 
{
	6, // 10-MAY-10 - new property based tags
	5, // 04-FEB-09 - added track number
	4, // 03-FEB-09 - added new loop tag
	3, // 100708 - removed tag comments, streamlined flags, added generic tags (float, int, vector3/4, quaternion, matrix34)
	2, // 100208 - added interval mid, order, tag magics, focus blocks
	1, // 090506 - first tag file version 
	0,
};

////////////////////////////////////////////////////////////////////////////////

void crTags::Serialize(datSerialize& s)
{
	const int latestVersion = tagsVersions[0];

	int version = latestVersion;
	s << datLabel("TagVersion:") << version << datNewLine;

	if(version > latestVersion)
	{
		Errorf("crTags::Serialize - attempting to load version '%d' (only support up to '%d')", version, latestVersion);
		return;
	}

	s_TagsSerializeVersion = version;

	int numTags = m_Tags.GetCount();
	s << datLabel("NumTags:") << numTags << datNewLine;

	if(s.IsRead())
	{
		RemoveAllTags();
		m_Tags.Resize(numTags);
	}

	for(int i=0; i<m_Tags.GetCount(); ++i)
	{
		if(s.IsRead())
		{
			m_Tags[i] = rage_new crTag;
		}
		crTag* tag = m_Tags[i];

		if(s_TagsSerializeVersion < 6)
		{
			SerializeTagLegacy(s, *tag);
		}
		else
		{
			s << *tag;
		}

		if(s.IsRead())
		{
			tag->m_Tags = this;
		}
	}

	if(s.IsRead())
	{
		CalcHasTags();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crTags::Dump(crDumpOutput& output) const
{
	const int numTags = m_Tags.GetCount();
	output.Outputf(1, "numtags", "%d", numTags);

	for(int i=0; i<numTags; ++i)
	{
		const crTag& tag = *m_Tags[i];
		output.Outputf(1, "tag", i, "%f -> %f '%s' #%d", tag.GetStart(), tag.GetEnd(), tag.GetName(), u32(tag.GetKey()));

		tag.Dump(output);
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

int crTags::GetNumTags(crTag::Key key) const
{
	int tagCount = 0;

	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		const crTag& tag = *m_Tags[i];
		if(tag.GetKey() == key)
		{
			tagCount++;
		}
	}
	return tagCount;
}

////////////////////////////////////////////////////////////////////////////////

int crTags::FindTag(const crTag& tag) const
{
	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		if(m_Tags[i] == &tag)
		{
			return i;
		}
	}
	return -1;
}

////////////////////////////////////////////////////////////////////////////////

crTag* crTags::AddTag(const crTag& tag)
{
	crTag* newTag = m_Tags.Grow() = tag.Clone();
	newTag->m_Tags = this;

	SortTags();

	return newTag;
}

////////////////////////////////////////////////////////////////////////////////

crTag* crTags::AddUniqueTag(const crTag& tag)
{
	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		// checks for possible duplicates (in both tag timing and property contents)
		if(IsClose(m_Tags[i]->m_Start, tag.m_Start) && IsClose(m_Tags[i]->m_End, tag.m_End) && m_Tags[i]->GetProperty() == tag.GetProperty())
		{
			// found exact duplicate, so no need to add - just return existing tag
			return m_Tags[i];
		}
	}
	
	// no duplicates, so add new tag
	return AddTag(tag);
}

////////////////////////////////////////////////////////////////////////////////

bool crTags::RemoveTag(crTag& tag)
{
	const int numTags = m_Tags.GetCount();
	for(int i=numTags-1; i>=0; --i)
	{
		if(m_Tags[i] == &tag)
		{
			delete m_Tags[i];
			m_Tags.Delete(i);
			return true;
		}
	}
	CalcHasTags();
	
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crTags::RemoveAllTags(crTag::Key key)
{
	const int numTags = m_Tags.GetCount();
	for(int i=numTags-1; i>=0; --i)
	{
		if(m_Tags[i]->GetKey() == key)
		{
			delete m_Tags[i];
			m_Tags.DeleteFast(i);
		}
	}

	SortTags();
	CalcHasTags();

	return (m_Tags.GetCount() < numTags);
}

////////////////////////////////////////////////////////////////////////////////

bool crTags::RemoveAllTags()
{
	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		delete m_Tags[i];
	}
	m_Tags.Reset();
	m_HasBlockTags = false;

	return (numTags > 0);
}

////////////////////////////////////////////////////////////////////////////////

crTags::ConstTagsCallback::ConstTagsCallback()
{
}

////////////////////////////////////////////////////////////////////////////////

crTags::ConstTagsCallback::~ConstTagsCallback()
{
}

////////////////////////////////////////////////////////////////////////////////

crTags::TagsCallback::TagsCallback()
{
}

////////////////////////////////////////////////////////////////////////////////

crTags::TagsCallback::~TagsCallback()
{
}

////////////////////////////////////////////////////////////////////////////////

bool crTags::ForAllTags(crTag::Key key, ConstTagsCallback& cb) const
{
	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		const crTag& tag = *m_Tags[i];
		if(tag.GetKey() == key)
		{
			if(!cb.ConstCallback(tag))
			{
				return false;
			}
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crTags::ForAllTags(crTag::Key key, TagsCallback& cb)
{
	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		crTag& tag = *m_Tags[i];
		if(tag.GetKey() == key)
		{
			if(!cb.Callback(tag))
			{
				return false;
			}
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crTags::ForAllTags(ConstTagsCallback& cb) const
{
	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		const crTag& tag = *m_Tags[i];
		if(!cb.ConstCallback(tag))
		{
			return false;
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crTags::ForAllTags(TagsCallback& cb)
{
	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		crTag& tag = *m_Tags[i];
		if(!cb.Callback(tag))
		{
			return false;
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

int TagCompare(const datOwner<crTag>* tag0, const datOwner<crTag>* tag1)
{
	return (**tag0 < **tag1) ? -1 : ((**tag0 == **tag1) ? 0 : 1);
}

////////////////////////////////////////////////////////////////////////////////

void crTags::SortTags()
{
	m_Tags.QSort(0, -1, TagCompare);
}

////////////////////////////////////////////////////////////////////////////////

void crTags::CopyTags(const crTags& other)
{
	RemoveAllTags();

	const int numTags = other.m_Tags.GetCount();
	m_Tags.Resize(numTags);

	for(int i=0; i<numTags; ++i)
	{
		m_Tags[i] = other.m_Tags[i]->Clone();
	}

	m_HasBlockTags = other.m_HasBlockTags;
}

////////////////////////////////////////////////////////////////////////////////

void crTags::CalcHasTags()
{
	m_HasBlockTags = false;

	const int numTags = m_Tags.GetCount();
	for(int i=0; i<numTags; ++i)
	{
		const crTag& tag = *m_Tags[i];
		if(tag.GetKey() == crTag::CalcKey("Block", 0xE433D77D))
		{
			m_HasBlockTags = true;
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crTags::SerializeTagLegacy(datSerialize& s, crTag& tag)
{
	Assert(s.IsRead());

	int tagType = 0;
	s << datLabel("TagType:") << tagType << datNewLine;

	if(s_TagsSerializeVersion < 3)
	{
		sysMemStartTemp();
		{
			atString junkComment;
			s << datLabel("Comment:") << junkComment << datNewLine;
		}
		sysMemEndTemp();
	}

	s << datLabel("Interval:");

	float start = 0.f;
	float end = 0.f;

	s << start;
	s << end;

	float mid = (start<=end)?((start+end)*0.5f):((start+end+1.f)*0.5f);
	if(s_TagsSerializeVersion >= 2)
	{
		s << mid;

		if(s_TagsSerializeVersion >= 3)
		{
			u16 flags16 = 0;
			s << flags16;
		}
		else
		{
			u32 flags32 = 0;
			s << flags32;
		}
	}
	else if(s.IsRead())
	{
		bool isEmpty;
		s << isEmpty;
	}
	s << datNewLine;

	u8 trackNumber = 0;
	if (s_TagsSerializeVersion >= 5)
	{
		s << datLabel("Track:") << trackNumber << datNewLine;
	}

	sysMemStartTemp();
	{
		crProperty prop;

		s << datLabel("{") << datNewLine;
		switch(tagType)
		{
		case 1: // kTagTypeLabel
			{
				atString label;
				s << datLabel("Label:") << label << datNewLine;

				crPropertyAttributeString attribString;
				attribString.GetString() = label;
				attribString.SetName("Label");

				prop.AddAttribute(attribString);
				prop.SetName("Label");
			}
			break;

		case 2: // kTagTypeBlock:
			{
				bool focus = false;
				if(s_TagsSerializeVersion >= 2)
				{
					s << focus;
				}

				crPropertyAttributeBool attribFocus;
				attribFocus.GetBool() = focus;
				attribFocus.SetName("Focus");

				prop.AddAttribute(attribFocus);

				if(IsClose(end, mid))
				{
					crPropertyAttributeFloat attribDivision;
					attribDivision.GetFloat() = 1.f;
					attribDivision.SetName("Division");

					prop.AddAttribute(attribDivision);
				}

				prop.SetName("Block");
			}
			break;

		case 16: // kTagTypeLoop
			{
				u32 loopId = 0;
				s << loopId;			

				crPropertyAttributeInt attribInt;
				attribInt.GetInt() = int(loopId);
				attribInt.SetName("Loop");

				prop.AddAttribute(attribInt);
				prop.SetName("LoopId");
			}
			break;

		case 8: // kTagTypeGenericFloat
		case 9: // kTagTypeGenericInt
		case 10: // kTagTypeGenericVector3
		case 11: // kTagTypeGenericVector4
		case 12: // kTagTypeGenericQuaternion
		case 13: // kTagTypeGenericMatrix34
		case 14: // kTagTypeGenericString
			{
				u32 genericType = 0;
				s << datLabel("GenericType:") << genericType << datNewLine;

				crPropertyAttributeInt attribSubType;
				attribSubType.GetInt() = int(genericType);
				attribSubType.SetName("SubType");

				prop.AddAttribute(attribSubType);

				switch(tagType)
				{
				case 8: // kTagTypeGenericFloat
					{
						float f = 0.f;
						s << f;

						crPropertyAttributeFloat attribFloat;
						attribFloat.GetFloat() = f;
						attribFloat.SetName("Float");
					
						prop.AddAttribute(attribFloat);
						prop.SetName("GenericFloat");
					}
					break;

				case 9: // kTagTypeGenericInt
					{
						int i = 0;
						s << i;

						crPropertyAttributeInt attribInt;
						attribInt.GetInt() = i;
						attribInt.SetName("Int");

						prop.AddAttribute(attribInt);
						prop.SetName("GenericInt");
					}
					break;

				case 10: // kTagTypeGenericVector3
					{
						Vec3V v;
						s << v;

						crPropertyAttributeVector3 attribVector3;
						attribVector3.GetVector3() = v;
						attribVector3.SetName("Vector3");

						prop.AddAttribute(attribVector3);
						prop.SetName("GenericVector3");
					}
					break;

				case 11: // kTagTypeGenericVector4
					{
						Vec4V v4;
						s << v4;

						crPropertyAttributeVector4 attribVector4;
						attribVector4.GetVector4() = v4;
						attribVector4.SetName("Vector4");

						prop.AddAttribute(attribVector4);
						prop.SetName("GenericVector4");
					}
					break;

				case 12: // kTagTypeGenericQuaternion
					{
						QuatV q;
						s << q;

						crPropertyAttributeQuaternion attribQuaternion;
						attribQuaternion.GetQuaternion() = q;
						attribQuaternion.SetName("Quaternion");

						prop.AddAttribute(attribQuaternion);
						prop.SetName("Quaternion");
					}
					break;

				case 13: // kTagTypeGenericMatrix34
					{
						Mat34V mtx;
						s << mtx;

						crPropertyAttributeMatrix34 attribMatrix34;
						attribMatrix34.GetMatrix34() = mtx;
						attribMatrix34.SetName("Matrix34");

						prop.AddAttribute(attribMatrix34);
						prop.SetName("GenericMatrix34");
					}
					break;

				case 14: // kTagTypeGenericString
					{
						atString str;
						s << str;

						crPropertyAttributeString attribString;
						attribString.GetString() = str;
						attribString.SetName("String");

						prop.AddAttribute(attribString);
						prop.SetName("GenericString");
					}
					break;
				}
			}
			break;

		case 0: // kTagTypeNone
		case 3: // kTagTypeAnchor
		case 4: // kTagTypeTurn
		case 5: // kTagTypeEvent
		case 6: // kTagTypeSnapshot
		case 7: // kTagTypeUnknown
		case 15: // kTagTypeGenericData
		case 17: // kTagTypeGenericContainer
		default:
			Errorf("crTags::SerailizeTagLegacy - unsupported legacy tag type (%d), unable to convert to new format", tagType);
			break;
		}
		s << datLabel("}") << datNewLine << datNewLine;

		sysMemEndTemp();
		prop.CalcSignature();
		tag.GetProperty() = prop;
		sysMemStartTemp();

		tag.SetStart(start);
		tag.SetEnd(end);
	}
	sysMemEndTemp();
}

////////////////////////////////////////////////////////////////////////////////

__THREAD int s_TagsSerializeVersion = 0;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
