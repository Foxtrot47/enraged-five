// 
// crmetadata/tag.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#ifndef CRMETADATA_TAG_H
#define CRMETADATA_TAG_H

#include "property.h"

#include "cranimation/animation_config.h"
#include "paging/base.h"
#include "math/simplemath.h"


namespace rage
{

class crDumpOutput;
class crTags;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Tags are light weight phase based wrapper for properties,
// enabling clips (and parameterized motion) to be marked up with
// temporal based metadata.
class crTag : public crProperty
{
public:

	// PURPOSE: Constructor
	crTag();

	// PURPOSE: Copy constructor
	crTag(const crTag&);

	// PURPOSE: Resource constructor
	crTag(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Placement
	DECLARE_PLACE(crTag);

	// PURPOSE: Clone the tag
	crTag* Clone() const;

	// PURPOSE: Destructor
	~crTag();

	// PURPOSE: Shutdown, free/release all dynamic resources
	void Shutdown();

#if CR_DEV
	// PURPOSE: Serialize a tag
	void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Tag/property hash key
	typedef u32 Key;

	// PURPOSE: Get property contained within the tag, const version
	// RETURNS: const reference to property
	const crProperty& GetProperty() const;

	// PURPOSE: Get property contained within the tag, non-const version
	// RETURNS: const reference to property
	crProperty& GetProperty();


	// PURPOSE: Get tag start phase
	// RETURNS: tag start phase (0..1)	
	float GetStart() const;

	// PURPOSE: Get tag end phase
	// RETURNS: tag end phase (0..1)
	// NOTES: For tags without duration, end == start
	float GetEnd() const;

	// PURPOSE: Get tag mid phase
	// RETURNS: tag mid phase (0..1)
	// NOTES: Calculated as halfway between start and end (handles wrapping tags)
	float GetMid() const; 


	// PURPOSE: Set tag start phase
	// PARAMS: startPhase - new start phase
	// NOTES: If start phase > end phase, tag is assumed to wrap at 1.0
	// Triggers a SortTags on any parent crTags object
	void SetStart(float startPhase);

	// PURPOSE: Set tag end phase
	// PARAMS: endPhase - new end phase
	// NOTES: If start phase > end phase, tag is assumed to wrap at 1.0
	// Triggers a SortTags on any parent crTags object
	void SetEnd(float endPhase);

	// PURPOSE: Set both tag start and end phase
	// PARAMS: 
	// startPhase - new start phase
	// endPhase - new end phase
	// NOTES: If start phase > end phase, tag is assumed to wrap at 1.0
	// Triggers a SortTags on any parent crTags object
	void SetStartEnd(float startPhase, float endPhase);


	// PURPOSE: Does tag have duration (or is it instant)?
	// RETURNS: true - tag has duration > 0, false - tag has no duration (i.e. start == end)
	bool HasDuration() const;

	// PURPOSE: Get duration
	// RETURNS: tag duration in phase (0..1)
	// NOTES: Handles looping tags correctly
	float GetDuration() const;

	// PURPOSE: Does tag contain phase value
	// PARAMS: phase - phase value to test
	// RETURNS: true - tag contains phase, false - phase falls outside tag
	// NOTES: Handles looping tags correctly
	bool Contains(float phase) const;

	// PURPOSE: Interpolate between start and end of tag
	// PARAMS: fract - percentage between start and end
	// RETURNS: phase at percentage point between start and end
	// NOTES: Handles looping tags correctly
	float Lerp(float fract) const;

	// PURPOSE: Translate tag (shifts start and end by phase specified)
	// PARAMS: translatePhase - phase to translate tag by
	// NOTES: Handles looping tags correctly
	// Triggers a SortTags on any parent crTags object
	void Translate(float translatePhase);


	// PURPOSE: Does this tag occur before another?
	// RETURNS: true - if tag occurs before other tag
	// NOTES: Compares start phases only.
	bool operator<(const crTag&) const;

	// PURPOSE: Does this tag occur after another?
	// RETURNS: true - if tag occurs after other tag
	// NOTES: Compares start phases only.
	bool operator>(const crTag&) const;

	// PURPOSE: Does this tag occur at the same phase range as another?
	// RETURNS: true - if tag occurs a same phase range as other tag
	// NOTES: This does not compare the contents when considering equality
	// Compares start phases only.
	bool operator==(const crTag&) const;


	// PURPOSE: Init class
	static void InitClass();

	// PURPOSE: Shutdown class
	static void ShutdownClass();


private:
	float m_Start;
	float m_End;

	datRef<crTags> m_Tags;

	static bool sm_InitClassCalled;

	friend class crTags;
};

////////////////////////////////////////////////////////////////////////////////

inline const crProperty& crTag::GetProperty() const
{
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline crProperty& crTag::GetProperty()
{
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline float crTag::GetStart() const
{
	return m_Start;
}

////////////////////////////////////////////////////////////////////////////////

inline float crTag::GetEnd() const
{
	return m_End;
}

////////////////////////////////////////////////////////////////////////////////

inline float crTag::GetMid() const 
{
	if(m_Start < m_End)
	{
		return (m_Start + m_End) * 0.5f;
	}
	else if(m_End > m_Start)
	{
		float mid = (m_Start + m_End + 1.f) * 0.5f; 
		if(mid > 1.f)
		{
			mid -= floorf(mid);
		}
		return mid;
	}
	return m_Start;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTag::HasDuration() const
{
	return (m_Start != m_End);
}

////////////////////////////////////////////////////////////////////////////////

inline float crTag::GetDuration() const
{
	if(m_Start <= m_End)
	{
		return m_End - m_Start;
	}
	else
	{
		return m_End - m_Start + 1.f;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTag::Contains(float phase) const
{
	if(m_Start < m_End)
	{
		return (phase >= m_Start) && (phase <= m_End);
	}
	else if(m_End > m_Start)
	{
		return (phase <= m_End) || (phase >= m_Start);
	}
	return phase == m_Start;
}

////////////////////////////////////////////////////////////////////////////////

inline float crTag::Lerp(float fract) const
{
	float result = m_Start*(1.f-fract) + m_End * fract;
	if(m_End < m_Start)
	{
		result += fract;
	}
	if(result < 0.f || result > 1.f)
	{
		result -= floorf(result);
	}
	return result;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTag::operator<(const crTag& other) const
{
	return m_Start < other.m_Start;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTag::operator>(const crTag& other) const
{
	return m_Start > other.m_Start;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crTag::operator==(const crTag& other) const
{
	return m_Start == other.m_Start;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMETADATA_TAG_H


