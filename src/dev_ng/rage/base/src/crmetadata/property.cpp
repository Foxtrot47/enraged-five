// 
// crmetadata/property.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "property.h"

#include "dumpoutput.h"
#include "properties.h"
#include "propertyattributes.h"

#include "atl/array_struct.h"
#include "data/safestruct.h"
#include "paging/rscbuilder.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "string/stringutil.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crProperty::crProperty()
: m_Properties(NULL)
, m_Signature(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crProperty::crProperty(const crProperty& other)
{
	Copy(other);
}

////////////////////////////////////////////////////////////////////////////////

crProperty::crProperty(datResource& rsc)
: m_Name(rsc)
, m_Attributes(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crProperty::DeclareStruct(datTypeStruct& s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(crProperty, pgBase)
	SSTRUCT_FIELD(crProperty, m_Name)
	SSTRUCT_FIELD(crProperty, m_Attributes)
	SSTRUCT_FIELD(crProperty, m_Properties)
	SSTRUCT_FIELD(crProperty, m_Signature)
	SSTRUCT_END(crProperty)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crProperty);

////////////////////////////////////////////////////////////////////////////////

crProperty* crProperty::Clone() const
{
	return rage_new crProperty(*this);
}

////////////////////////////////////////////////////////////////////////////////

crProperty::~crProperty()
{
	Shutdown();

#if ENABLE_KNOWN_REFERENCES
	ClearAllKnownRefs(false);
#endif
}

////////////////////////////////////////////////////////////////////////////////

void crProperty::Shutdown()
{
	m_Name.Shutdown();
	RemoveAllAttributes();
}

////////////////////////////////////////////////////////////////////////////////

int QSortPropertyAttrubuteFunc(const datOwner<crPropertyAttribute>* attrib0, const datOwner<crPropertyAttribute>* attrib1)
{
	return (*attrib1)->GetKey() - (*attrib0)->GetKey();
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
int propertyVersions[] = 
{
	2, // DA 11-MAR-11 - attributes in sorted order
	1, // JM 03-APR-10 - new property structure
//	0,
};

////////////////////////////////////////////////////////////////////////////////

static const int s_AttributeSortVersion = 1;

////////////////////////////////////////////////////////////////////////////////

__THREAD int s_PropertySerializeVersion = 0;

////////////////////////////////////////////////////////////////////////////////

void crProperty::Serialize(datSerialize& s)
{
	const int latestVersion = propertyVersions[0];

	int version = latestVersion;
	s << datLabel("PropertyVersion:") << version << datNewLine;

	bool supportedVersion = false;

	for (int i=0; i<sizeof(propertyVersions); ++i)
	{
		if (version == propertyVersions[i])
		{
			supportedVersion = true;
		}
	}

	if(!supportedVersion)
	{
		Errorf("crProperty::Serialize - attempting to load version '%d' (only support up to '%d')", version, latestVersion);
		return;
	}

	s_PropertySerializeVersion = version;

	s << m_Name;

	int numAttributes = m_Attributes.GetCount();
	s << numAttributes;

	bool rscLoad = s.IsRead() && pgRscBuilder::IsBuilding() && (&sysMemAllocator::GetCurrent() != &sysMemAllocator::GetMaster()); 

	if(s.IsRead())
	{
		// copy all attributes
		sysMemStartTemp();
		atArray<crPropertyAttribute*> attributes;
		for(int i=0; i < numAttributes; ++i)
		{
			int attributeType = int(crPropertyAttribute::kTypeNone);
			s << attributeType;

			crPropertyAttribute* attribute = crPropertyAttribute::Allocate(crPropertyAttribute::eType(attributeType));
			s << *attribute;

			if(!rscLoad || StringWildcardCompare("*DO_NOT_RESOURCE", attribute->GetName()))
			{
				attributes.Grow() = attribute;
			}
			else
			{
				delete attribute;
			}
		}
		sysMemEndTemp();

		// create attribute array
		numAttributes = attributes.GetCount();
		m_Attributes.Resize(numAttributes);
		for(int i=0; i < numAttributes; i++)
		{
			m_Attributes[i] = attributes[i]->Clone();
		}

		// delete temporary attributes
		sysMemStartTemp();
		for(int i=0; i < numAttributes; ++i)
		{
			delete attributes[i];
		}
		attributes.Reset();
		sysMemEndTemp();
	}
	else
	{
		for(int i=0; i < numAttributes; ++i)
		{
			int attributeType = m_Attributes[i]->GetType();
			s << attributeType;
			s << *m_Attributes[i];
		}
	}

	if (version == s_AttributeSortVersion)
	{
		m_Attributes.QSort(0, m_Attributes.GetCount(), QSortPropertyAttrubuteFunc);
	}

	if(s.IsRead())
	{
		CalcSignature();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crProperty::Dump(crDumpOutput& output) const
{
	output.Outputf(1, "propertyname", "'%s'", GetName());
	output.Outputf(1, "propertykey", "%08x", u32(GetKey()));

	const int numAttributes = m_Attributes.GetCount();
	output.Outputf(1, "numattributes", "%d", numAttributes);

	for(int i=0; i<numAttributes; ++i)
	{
		const crPropertyAttribute& attrib = *m_Attributes[i];

		output.Outputf(2, "attribute", i, "'%s' #%08x type '%s' %d", attrib.GetName(), u32(attrib.GetKey()), attrib.GetTypeName(), int(attrib.GetType()));
		attrib.Dump(output);
	}	
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crProperty::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		crPropertyAttribute::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crProperty::ShutdownClass()
{
	crPropertyAttribute::ShutdownClass();

	sm_InitClassCalled = false;	
}

////////////////////////////////////////////////////////////////////////////////

void crProperty::SetName(const char* name)
{
	Key oldKey = GetKey();

	m_Name.SetString(name);

	if(m_Properties)
	{
		m_Properties->UpdatePropertyInternal(*this, oldKey);
	}
}

////////////////////////////////////////////////////////////////////////////////

const crPropertyAttribute* crProperty::GetAttribute(u32 key) const
{
	const int numAttributes = m_Attributes.GetCount();
	for(int i=0; i<numAttributes; ++i)
	{
		if(key == m_Attributes[i]->GetKey())
		{
			return m_Attributes[i];
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute* crProperty::GetAttribute(u32 key)
{
	const int numAttributes = m_Attributes.GetCount();
	for(int i=0; i<numAttributes; ++i)
	{
		if(key == m_Attributes[i]->GetKey())
		{
			return m_Attributes[i];
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crProperty::ForAllAttributes(ConstPropertyAttributeCallback cb, void* data) const
{
	const int numAttributes = m_Attributes.GetCount();
	for(int i=0; i<numAttributes; ++i)
	{
		if(!cb(*m_Attributes[i], data))
		{
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crProperty::AddAttribute(const crPropertyAttribute& attribute)
{
	const int numAttributes = m_Attributes.GetCount();
	for(int i=0; i<numAttributes; ++i)
	{
		if(attribute.GetKey() == m_Attributes[i]->GetKey())
		{
			delete m_Attributes[i];
			m_Attributes[i] = attribute.Clone();
			return false;
		} 
	}

	m_Attributes.Grow() = attribute.Clone();
	m_Attributes.QSort(0, m_Attributes.GetCount(), QSortPropertyAttrubuteFunc);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crProperty::RemoveAttribute(u32 key)
{
	const int numAttributes = m_Attributes.GetCount();

	for(int i=0; i<numAttributes; ++i)
	{
		if(key == m_Attributes[i]->GetKey())
		{
			delete m_Attributes[i];
			m_Attributes.Delete(i);
			return true;
		}
	}	
	
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crProperty::RemoveAllAttributes()
{
	const int numAttributes = m_Attributes.GetCount();
	for(int i=0; i<numAttributes; ++i)
	{
		delete m_Attributes[i];
	}
	m_Attributes.Reset();

	return (numAttributes > 0);
}

////////////////////////////////////////////////////////////////////////////////

bool crProperty::operator==(const crProperty& other) const
{
	if(m_Name.GetKey() != other.m_Name.GetKey())
	{
		return false;
	}

	if(m_Attributes.GetCount() != other.m_Attributes.GetCount())
	{
		return false;
	}

	const int numAttributes = m_Attributes.GetCount();
	for(int i=0; i<numAttributes; ++i)
	{
		if(*m_Attributes[i] != *other.m_Attributes[i])
		{
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

crProperty::StringKey::StringKey()
: m_String(NULL)
, m_Key(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crProperty::StringKey::StringKey(const StringKey& other)
{
	bool rscLoad = pgRscBuilder::IsBuilding() && (&sysMemAllocator::GetCurrent() != &sysMemAllocator::GetMaster());

	if(!rscLoad)
	{
		m_String = StringDuplicate(other.m_String.ptr);
	}
	m_Key = other.m_Key;
}

////////////////////////////////////////////////////////////////////////////////

crProperty::StringKey::StringKey(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crProperty::StringKey::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crProperty::StringKey)
	SSTRUCT_FIELD(crProperty::StringKey, m_String)
	SSTRUCT_FIELD(crProperty::StringKey, m_Key)
	SSTRUCT_END(crProperty::StringKey)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crProperty::StringKey);

////////////////////////////////////////////////////////////////////////////////

crProperty::StringKey::~StringKey()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crProperty::StringKey::Shutdown()
{
	if(m_String)
	{
		delete [] m_String;
		m_String = NULL;
	}
	m_Key = 0;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crProperty::StringKey::Serialize(datSerialize& s)
{
	const bool rscLoad = s.IsRead() && pgRscBuilder::IsBuilding() && (&sysMemAllocator::GetCurrent() != &sysMemAllocator::GetMaster()); 

	if(rscLoad)
	{
		sysMemStartTemp();
	}
	s << datString(reinterpret_cast<char**>(&m_String), NULL);
	if(rscLoad)
	{
		if(m_String)
		{
			delete [] m_String;
			m_String = NULL;
		}
		sysMemEndTemp();
	}
	s << m_Key;
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

const char* crProperty::StringKey::GetString() const
{
	return m_String;	
}

////////////////////////////////////////////////////////////////////////////////

void crProperty::StringKey::SetString(const char* str)
{
	if(m_String)
	{
		delete [] m_String;
	}
	m_String = StringDuplicate(str);
	m_Key = CalcKey(m_String);
}

////////////////////////////////////////////////////////////////////////////////

u32 crProperty::StringKey::GetKey() const
{
	return m_Key;
}

////////////////////////////////////////////////////////////////////////////////

u32 crProperty::StringKey::CalcKey(const char* str)
{
	return atStringHash(str);
}

////////////////////////////////////////////////////////////////////////////////

crProperty::StringKey& crProperty::StringKey::operator=(const crProperty::StringKey& other)
{
	Shutdown();

	bool rscLoad = pgRscBuilder::IsBuilding() && &sysMemAllocator::GetCurrent() != &sysMemAllocator::GetMaster();

	if(!rscLoad)
	{
		m_String = StringDuplicate(other.m_String.ptr);
	}
	m_Key = other.m_Key;

	return *this;
}

////////////////////////////////////////////////////////////////////////////////

void crProperty::Copy(const crProperty& other)
{
	m_Name = other.m_Name;
	m_Attributes.Resize(other.m_Attributes.GetCount());

	for(int i=0; i<other.m_Attributes.GetCount(); ++i)
	{
		m_Attributes[i] = other.m_Attributes[i]?other.m_Attributes[i]->Clone():NULL;
	}

	m_Properties = NULL;
	m_Signature = other.m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

void crProperty::CalcSignature()
{
	u32 signature = m_Name.GetKey();

	const int numAttributes = m_Attributes.GetCount();
	for(int i=0; i<numAttributes; ++i)
	{
		u32 val = m_Attributes[i]->CalcSignature();
		signature = atDataHash(reinterpret_cast<const u32*>(&val), sizeof(u32), signature);
	}
	m_Signature = signature;
}

////////////////////////////////////////////////////////////////////////////////

bool crProperty::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////


} // namespace rage
