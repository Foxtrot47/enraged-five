// 
// crmetadata/propertyattributes.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CRMETADATA_PROPERTYATTRIBUTES_H 
#define CRMETADATA_PROPERTYATTRIBUTES_H 

#include "property.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "atl/hashstring.h"
#include "atl/string.h"
#include "cranimation/animation_config.h"
#include "data/resource.h"
#include "data/serialize.h"
#include "data/struct.h"
#include "vectormath/classes.h"

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Base property attribute class, derive to implement 
class crPropertyAttribute
{
public:

	// PURPOSE: Enumeration of different built in derived property attribute types
	enum eType
	{
		kTypeNone,

		kTypeFloat,
		kTypeInt,
		kTypeBool,
		kTypeString,
		kTypeBitSet,
		kTypeVector3,
		kTypeVector4,
		kTypeQuaternion,
		kTypeMatrix34,
		kTypeSituation,
		kTypeData,
		kTypeHashString,
		// RESERVED - for more Rage specific property attribute types only

		// this must follow the list of built in property attribute types
		kTypeNum,

		// custom property attribute types *must* have a unique id >= kPropertyAttributeTypeCustom
		kTypeCustom = 0x80,
	};

	// PURPOSE: Constructor
	// PARAMS: type - derived constructor must pass in property attribute type identifier
	// (see crPropertyAttribute::eType enumeration for built in property attribute types)
	crPropertyAttribute(eType type=kTypeNone);

	// PURPOSE: Resource constructor
	crPropertyAttribute(datResource&);

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Placement
	static void Place(void*, datResource&);

	// PURPOSE: Fixes up derived type from pointer to base class only
	static void VirtualConstructFromPtr(crPropertyAttribute* base, datResource&);

	// PURPOSE: Clone the property
	virtual crPropertyAttribute* Clone() const = 0;

	// PURPOSE: Destructor
	virtual ~crPropertyAttribute();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Property attribute factory
	// PARAMS: property attribute type identifier
	// (see crPropertyAttribute::eType enumeration for built in property attribute types)
	// RETURNS: pointer to newly created property attribute, or NULL if property attribute type unknown
	static crPropertyAttribute* Allocate(eType type);

#if CR_DEV
	// PURPOSE: Serialize a property attribute
	// NOTES: Derived classes will need to implement custom file serialization here
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;

	// PURPOSE: Get property attribute type name
	// RETURNS: const pointer to property attribute type name (ie string version of class name)
	const char* GetTypeName() const;
#endif // CR_DEV

	// PURPOSE: Get property attribute name
	// RETURNS: property attribute name (or NULL if none)
	// NOTE: Not available on resources.  
	// Names are discarded at resource generation time, only hash key retained.
	// SEE ALSO: GetKey()
	const char* GetName() const;

	// PURPOSE: Get property attribute name key
	// RETURNS: hash key of property attribute name (zero if none)
	// NOTE: Available on resources (even though name is discarded)
	u32 GetKey() const;

	// PURPOSE: Set property attribute name
	// PARAMS: name - new property attribute name
	// NOTES: Automatically updates hash key.
	void SetName(const char* name);

	// PURPOSE: Get property attribute type
	// RETURNS: property attribute type identifier
	// (see crPropertyAttribute::eType enumeration for built in property attribute types)
	eType GetType() const;

	
	// PURPOSE: Equality comparison
	// NOTES: Compares keys, types, and if equal, values of derived types
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Inequality comparison
	bool operator!=(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;


	// PURPOSE: Init class
	static void InitClass();

	// PURPOSE: Shutdown class
	static void ShutdownClass();

	// PURPOSE: Definition of property attribute allocation call
	typedef crPropertyAttribute* AllocateFn();

	// PURPOSE: Definition of property attribute placement call
	typedef void PlaceFn(crPropertyAttribute*, datResource&);

	// PURPOSE: Property attribute type information
	struct TypeInfo
	{
	public:

		// PURPOSE: Constructor
		// PARAMS: type - property attribute type identifier 
		// (see crPropertyAttribute::eType enumeration for built in property attribute types)
		// name - property attribute name string
		// allocateFn - property attribute allocation call
		// placeFn - property attribute resource placement call
		TypeInfo(eType type, const char* name, AllocateFn* allocateFn, PlaceFn* placeFn);

		// PURPOSE: Destructor
		~TypeInfo();

		// PURPOSE: Registration call
		void Register() const;

		// PURPOSE: Get property attribute type identifier
		// RETURNS: property attribute type identifier 
		// (see crPropertyAttribute::ePropertyAttributeType enumeration for built in property attribute types)
		eType GetType() const;

		// PURPOSE: Get property attribute name string
		// RETURNS: string of property attribute name
		const char* GetName() const;

		// PURPOSE: Get property attribute allocation function
		// RETURNS: property attribute allocation function pointer
		AllocateFn* GetAllocateFn() const;

		// PURPOSE: Get property attribute resource placement function
		// RETURNS: property attribute resource placement function pointer
		PlaceFn* GetPlaceFn() const;

	private:
		eType m_Type;
		const char* m_Name;
		AllocateFn* m_AllocateFn;
		PlaceFn* m_PlaceFn;		
	};

	// PURPOSE: Get info about a property attribute type
	// PARAMS: type - property attribute type identifier 
	// (see crPropertyAttribute::eType enumeration for built in property attribute types)
	// RETURNS: const pointer to property attribute type info structure (may be NULL if property attribute type unknown)
	static const TypeInfo* FindTypeInfo(eType type);

	// PURPOSE: Get info about this property attribute
	// RETURNS: const reference to property attribute type info structure about this property attribute
	virtual const TypeInfo& GetTypeInfo() const = 0;

	// PURPOSE: Declare functions necessary to register a new property attribute type
	#define CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(__typename) \
		static crPropertyAttribute* AllocatePropertyAttribute(); \
		static void PlacePropertyAttribute(crPropertyAttribute* base, datResource& rsc); \
		virtual crPropertyAttribute* Clone() const; \
		static void InitClass(); \
		virtual const TypeInfo& GetTypeInfo() const; \
		static const crPropertyAttribute::TypeInfo sm_TypeInfo;

	// PURPOSE: Implement functions necessary to register a new property attribute type
	#define CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(__typename, __typeid) \
		crPropertyAttribute* __typename::AllocatePropertyAttribute() \
		{ \
			__typename* propAttrib = rage_new __typename; \
			return propAttrib; \
		} \
		void __typename::PlacePropertyAttribute(crPropertyAttribute* base, datResource& rsc) \
		{ \
			::new (base) __typename(rsc); \
		} \
		crPropertyAttribute* __typename::Clone() const \
		{ \
			return rage_new __typename(*this); \
		} \
		void __typename::InitClass() \
		{ \
			sm_TypeInfo.Register(); \
		} \
		const crPropertyAttribute::TypeInfo& __typename::GetTypeInfo() const \
		{ \
			return sm_TypeInfo; \
		} \
		const crPropertyAttribute::TypeInfo __typename::sm_TypeInfo(__typeid, CR_DEV ? #__typename :  NULL, AllocatePropertyAttribute, PlacePropertyAttribute);

protected:

	// PURPOSE: Register a new property attribute type (only call from TypeInfo::Register)
	// PARAMS: property type info (must be global/class static, persist for entire execution)
	static void RegisterTypeInfo(const TypeInfo& info);

private:
	eType m_Type;
	crProperty::StringKey m_Name;

	static atArray<const TypeInfo*> sm_TypeInfos;

	static bool sm_InitClassCalled;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Template access to property attribute value
// Checks pointer and type of property attribute, provides convenient accessors
template<typename _AttributeType, typename _ValueType>
class crPropertyAttributeAccessor
{
public:

	// PURPOSE: Constructor
	// PARAMS: pointer to attribute object (retrieved from crProperty), can be NULL
	crPropertyAttributeAccessor(const crPropertyAttribute* attribute);

	// PURPOSE: Is attribute valid?
	// NOTES: Checks for non-NULL and that attribute type matched expected type
	bool Valid() const;

	// PURPOSE: Does attribute value match input?
	bool Compare(const _ValueType&) const;

	// PURPOSE: Get attribute value, if valid
	// PARAMS: out - variable to copy value to (will be unchanged if not valid)
	// RETURNS: true - value retrieved, false - not valid, variable unchanged
	bool Get(_ValueType& out) const;

	// PURPOSE: Get pointer to attribute value, if valid
	// RETURNS: pointer to attribute value if valid, otherwise NULL
	const _ValueType* GetPtr() const;

protected:
	const crPropertyAttribute* m_Attribute;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Float property attribute
class crPropertyAttributeFloat : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeFloat();

	// PURPOSE: Resource constructor
	crPropertyAttributeFloat(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeFloat();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeFloat);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get float (const)
	float GetFloat() const;

	// PURPOSE: Get float (non-const)
	float& GetFloat();

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	const float& GetValue() const;

#if CR_DEV
	// PURPOSE: Set float
	void SetFloat(float value);
#endif // CR_DEV

private:
	float m_Float;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Integer property attribute
class crPropertyAttributeInt : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeInt();

	// PURPOSE: Resource constructor
	crPropertyAttributeInt(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeInt();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeInt);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get integer (const)
	int GetInt() const;

	// PURPOSE: Get integer (non-const)
	int& GetInt();

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	const int& GetValue() const;

private:
	int m_Int;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Boolean property attribute
class crPropertyAttributeBool : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeBool();

	// PURPOSE: Resource constructor
	crPropertyAttributeBool(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeBool();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeBool);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get boolean (const)
	bool GetBool() const;

	// PURPOSE: Get boolean (non-const)
	bool& GetBool();

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	const bool& GetValue() const;

private:
	bool m_Bool;
	datPadding<3> m_Padding;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: String property attribute
class crPropertyAttributeString : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeString();

	// PURPOSE: Resource constructor
	crPropertyAttributeString(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeString();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeString);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get string (const)
	const atString& GetString() const;

	// PURPOSE: Get string (non-const)
	atString& GetString(); 

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	const atString& GetValue() const;

private:
	atString m_String;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Bit set property attribute
class crPropertyAttributeBitSet : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeBitSet();

	// PURPOSE: Resource constructor
	crPropertyAttributeBitSet(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeBitSet();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeBitSet);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get bit set (const)
	const atBitSet& GetBitSet() const;

	// PURPOSE: Get bit set (non-const)
	atBitSet& GetBitSet(); 

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	const atBitSet& GetValue() const;

private:
	atBitSet m_BitSet;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Vector3 property attribute
class crPropertyAttributeVector3 : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeVector3();

	// PURPOSE: Resource constructor
	crPropertyAttributeVector3(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeVector3();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeVector3);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get vector3 (const)
	Vec3V_ConstRef GetVector3() const;

	// PURPOSE: Get vector3 (non-const)
	Vec3V_Ref GetVector3();

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	Vec3V_ConstRef GetValue() const;

private:
	Vec3V m_Vector3;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Vector4 property attribute
class crPropertyAttributeVector4 : public crPropertyAttribute
{	
public:

	// PURPOSE: Default constructor
	crPropertyAttributeVector4();

	// PURPOSE: Resource constructor
	crPropertyAttributeVector4(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeVector4();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeVector4);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get vector4 (const)
	Vec4V_ConstRef GetVector4() const;

	// PURPOSE: Get vector4 (non-const)
	Vec4V_Ref GetVector4();

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	Vec4V_ConstRef GetValue() const;

private:
	Vec4V m_Vector4;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Quaternion property attribute
class crPropertyAttributeQuaternion : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeQuaternion();

	// PURPOSE: Resource constructor
	crPropertyAttributeQuaternion(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeQuaternion();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeQuaternion);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get quaternion (const)
	QuatV_ConstRef GetQuaternion() const;

	// PURPOSE: Get quaternion (non-const)
	QuatV_Ref GetQuaternion();

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	QuatV_ConstRef GetValue() const;

private:
	QuatV m_Quaternion;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Matrix34 property attribute
class crPropertyAttributeMatrix34 : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeMatrix34();
	
	// PURPOSE: Resource constructor
	crPropertyAttributeMatrix34(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeMatrix34();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeMatrix34);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get matrix34 (const)
	Mat34V_ConstRef GetMatrix34() const;

	// PURPOSE: Get matrix34 (non-const)
	Mat34V_Ref GetMatrix34();

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	Mat34V_ConstRef GetValue() const;

private:
	Mat34V m_Matrix34;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Situation property attribute
class crPropertyAttributeSituation : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeSituation();

	// PURPOSE: Resource constructor
	crPropertyAttributeSituation(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeSituation();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeSituation);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get situation (const)
	TransformV_ConstRef GetSituation() const;

	// PURPOSE: Get situation (non-const)
	TransformV_Ref GetSituation();

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	TransformV_ConstRef GetValue() const;

private:
	TransformV m_Situation;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Data property attribute
class crPropertyAttributeData : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeData();

	// PURPOSE: Resource constructor
	crPropertyAttributeData(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeData();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeData);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get data (const)
	const atArray<u8>& GetData() const;

	// PURPOSE: Get data (non-const)
	atArray<u8>& GetData();

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	const atArray<u8>& GetValue() const;

private:
	atArray<u8> m_Data;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Hash string property attribute
class crPropertyAttributeHashString : public crPropertyAttribute
{
public:

	// PURPOSE: Default constructor
	crPropertyAttributeHashString();

	// PURPOSE: Resource constructor
	crPropertyAttributeHashString(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crPropertyAttributeHashString();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Type registration
	CR_DECLARE_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeHashString);

#if CR_DEV
	// PURPOSE: Serialization override
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Equality comparison
	virtual bool operator==(const crPropertyAttribute&) const;

	// PURPOSE: Calculate signature
	virtual u32 CalcSignature() const;

	// PURPOSE: Get hash string (const)
	const atHashString& GetHashString() const;

	// PURPOSE: Get hash string (non-const)
	atHashString& GetHashString(); 

	// PURPOSE: Get value by reference (with standardized name for use in templates)
	const atHashString& GetValue() const;

private:
	atHashString m_HashString;
};

////////////////////////////////////////////////////////////////////////////////

template<typename _AttributeType, typename _ValueType>
crPropertyAttributeAccessor<_AttributeType, _ValueType>::crPropertyAttributeAccessor(const crPropertyAttribute* attribute)
: m_Attribute(attribute)
{
}

////////////////////////////////////////////////////////////////////////////////

template<typename _AttributeType, typename _ValueType>
bool crPropertyAttributeAccessor<_AttributeType, _ValueType>::Valid() const
{
	if(m_Attribute)
	{
		if(m_Attribute->GetType() == _AttributeType::sm_TypeInfo.GetType())
		{
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _AttributeType, typename _ValueType>
bool crPropertyAttributeAccessor<_AttributeType, _ValueType>::Compare(const _ValueType& input) const
{
	if(Valid())
	{
		return (static_cast<const _AttributeType*>(m_Attribute)->GetValue() == input);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _AttributeType, typename _ValueType>
bool crPropertyAttributeAccessor<_AttributeType, _ValueType>::Get(_ValueType& out) const
{
	if(Valid())
	{
		out = static_cast<const _AttributeType*>(m_Attribute)->GetValue();
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _AttributeType, typename _ValueType>
const _ValueType* crPropertyAttributeAccessor<_AttributeType, _ValueType>::GetPtr() const
{
	if(Valid())
	{
		return &static_cast<const _AttributeType*>(m_Attribute)->GetValue();
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

typedef crPropertyAttributeAccessor<crPropertyAttributeFloat, float> crPropertyAttributeFloatAccessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeInt, int> crPropertyAttributeIntAccessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeBool, bool> crPropertyAttributeBoolAccessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeString, atString> crPropertyAttributeStringAccessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeBitSet, atBitSet> crPropertyAttributeBitSetAccessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeVector3, Vec3V> crPropertyAttributeVector3Accessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeVector4, Vec4V> crPropertyAttributeVector4Accessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeQuaternion, QuatV> crPropertyAttributeQuaternionAccessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeMatrix34, Mat34V> crPropertyAttributeMatrix34Accessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeSituation, TransformV> crPropertyAttributeSituationAccessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeData, atArray<u8> > crPropertyAttributeDataAccessor;
typedef crPropertyAttributeAccessor<crPropertyAttributeHashString, atHashString> crPropertyAttributeHashStringAccessor;

////////////////////////////////////////////////////////////////////////////////

extern __THREAD int s_PropertyAttributeSerializeVersion;

////////////////////////////////////////////////////////////////////////////////

inline const char* crPropertyAttribute::GetName() const
{
	return m_Name.GetString();
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crPropertyAttribute::GetKey() const
{
	return m_Name.GetKey();
}

////////////////////////////////////////////////////////////////////////////////

inline void crPropertyAttribute::SetName(const char* name)
{
	m_Name.SetString(name);
}

////////////////////////////////////////////////////////////////////////////////

inline crPropertyAttribute::eType crPropertyAttribute::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crPropertyAttribute::operator!=(const crPropertyAttribute& other) const
{
	return !(*this == other);
}

////////////////////////////////////////////////////////////////////////////////

inline float crPropertyAttributeFloat::GetFloat() const
{
	return m_Float;
}

////////////////////////////////////////////////////////////////////////////////

inline float& crPropertyAttributeFloat::GetFloat()
{
	return m_Float;
}

////////////////////////////////////////////////////////////////////////////////

inline const float& crPropertyAttributeFloat::GetValue() const
{
	return m_Float;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
inline void crPropertyAttributeFloat::SetFloat(float value)
{
	m_Float = value;
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

inline int crPropertyAttributeInt::GetInt() const
{
	return m_Int;
}

////////////////////////////////////////////////////////////////////////////////

inline int& crPropertyAttributeInt::GetInt()
{
	return m_Int;
}

////////////////////////////////////////////////////////////////////////////////

inline const int& crPropertyAttributeInt::GetValue() const
{
	return m_Int;
}

////////////////////////////////////////////////////////////////////////////////

inline bool& crPropertyAttributeBool::GetBool()
{
	return m_Bool;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crPropertyAttributeBool::GetBool() const
{
	return m_Bool;
}

////////////////////////////////////////////////////////////////////////////////

inline const bool& crPropertyAttributeBool::GetValue() const
{
	return m_Bool;
}

////////////////////////////////////////////////////////////////////////////////

inline const atString& crPropertyAttributeString::GetString() const
{
	return m_String;
}

////////////////////////////////////////////////////////////////////////////////

inline atString& crPropertyAttributeString::GetString()
{
	return m_String;
}

////////////////////////////////////////////////////////////////////////////////

inline const atString& crPropertyAttributeString::GetValue() const
{
	return m_String;
}

////////////////////////////////////////////////////////////////////////////////

inline const atBitSet& crPropertyAttributeBitSet::GetBitSet() const
{
	return m_BitSet;
}

////////////////////////////////////////////////////////////////////////////////

inline atBitSet& crPropertyAttributeBitSet::GetBitSet()
{
	return m_BitSet;
}

////////////////////////////////////////////////////////////////////////////////

inline const atBitSet& crPropertyAttributeBitSet::GetValue() const
{
	return m_BitSet;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec3V_ConstRef crPropertyAttributeVector3::GetVector3() const
{
	return m_Vector3;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec3V_Ref crPropertyAttributeVector3::GetVector3()
{
	return m_Vector3;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec3V_ConstRef crPropertyAttributeVector3::GetValue() const
{
	return m_Vector3;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec4V_ConstRef crPropertyAttributeVector4::GetVector4() const
{
	return m_Vector4;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec4V_Ref crPropertyAttributeVector4::GetVector4()
{
	return m_Vector4;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec4V_ConstRef crPropertyAttributeVector4::GetValue() const
{
	return m_Vector4;
}

////////////////////////////////////////////////////////////////////////////////

inline QuatV_ConstRef crPropertyAttributeQuaternion::GetQuaternion() const
{
	return m_Quaternion;
}

////////////////////////////////////////////////////////////////////////////////

inline QuatV_Ref crPropertyAttributeQuaternion::GetQuaternion()
{
	return m_Quaternion;
}

////////////////////////////////////////////////////////////////////////////////

inline QuatV_ConstRef crPropertyAttributeQuaternion::GetValue() const
{
	return m_Quaternion;
}

////////////////////////////////////////////////////////////////////////////////

inline Mat34V_ConstRef crPropertyAttributeMatrix34::GetMatrix34() const
{
	return m_Matrix34;
}

////////////////////////////////////////////////////////////////////////////////

inline Mat34V_Ref crPropertyAttributeMatrix34::GetMatrix34()
{
	return m_Matrix34;
}

////////////////////////////////////////////////////////////////////////////////

inline Mat34V_ConstRef crPropertyAttributeMatrix34::GetValue() const
{
	return m_Matrix34;
}

////////////////////////////////////////////////////////////////////////////////

inline TransformV_ConstRef crPropertyAttributeSituation::GetSituation() const
{
	return m_Situation;
}

////////////////////////////////////////////////////////////////////////////////

inline TransformV_Ref crPropertyAttributeSituation::GetSituation()
{
	return m_Situation;
}

////////////////////////////////////////////////////////////////////////////////

inline TransformV_ConstRef crPropertyAttributeSituation::GetValue() const
{
	return m_Situation;
}

////////////////////////////////////////////////////////////////////////////////

inline const atArray<u8>& crPropertyAttributeData::GetData() const
{
	return m_Data;
}

////////////////////////////////////////////////////////////////////////////////

inline atArray<u8>& crPropertyAttributeData::GetData()
{
	return m_Data;
}

////////////////////////////////////////////////////////////////////////////////

inline const atArray<u8>& crPropertyAttributeData::GetValue() const
{
	return m_Data;
}

////////////////////////////////////////////////////////////////////////////////

inline const atHashString& crPropertyAttributeHashString::GetHashString() const
{
	return m_HashString;
}

////////////////////////////////////////////////////////////////////////////////

inline atHashString& crPropertyAttributeHashString::GetHashString()
{
	return m_HashString;
}

////////////////////////////////////////////////////////////////////////////////

inline const atHashString& crPropertyAttributeHashString::GetValue() const
{
	return m_HashString;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMETADATA_PROPERTYATTRIBUTES_H 

