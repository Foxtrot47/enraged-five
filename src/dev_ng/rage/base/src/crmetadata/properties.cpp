// 
// crmetadata/properties.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "properties.h"

#include "dumpoutput.h"
#include "property.h"
#include "propertyattributes.h"

#include "atl/map_struct.h"
#include "data/safestruct.h"
#include "file/asset.h"
#include "file/serialize.h"
#include "file/stream.h"
#include "paging/rscbuilder.h"
#include "string/string.h"
#include "string/stringutil.h"
#include "system/memory.h"

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

crProperties::crProperties()
{
}

////////////////////////////////////////////////////////////////////////////////

crProperties::crProperties(datResource& rsc)
: m_Properties(rsc, NULL, &crProperties::FixupDataFunc)
{
}

////////////////////////////////////////////////////////////////////////////////

crProperties::crProperties(const crProperties& other)
{
	CopyProperties(other);
}

////////////////////////////////////////////////////////////////////////////////

crProperties& crProperties::operator=(const crProperties& other)
{
	CopyProperties(other);	
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

crProperties::~crProperties()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crProperties::FixupDataFunc(datResource &rsc, datOwner<crProperty>& data)
{
	data.Place(&data, rsc);
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crProperties);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crProperties::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crProperties)
	SSTRUCT_FIELD(crProperties, m_Properties)
	SSTRUCT_END(crProperties)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crProperties::Shutdown()
{
	RemoveAllProperties();
}

////////////////////////////////////////////////////////////////////////////////

class DestroyPropertiesCallback : public crProperties::PropertiesCallback
{
public:
	virtual bool Callback(crProperty& prop)
	{
		prop.Destroy();
		return true;
	}
};

////////////////////////////////////////////////////////////////////////////////

void crProperties::Destroy()
{
	DestroyPropertiesCallback destroyPropsCb;
	ForAllProperties(destroyPropsCb);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
int propertiesVersions[] = 
{
	3, // 13-DEC-11 - change situation serialization
	2, // 04-MAY-10 - new property file format
	1, // 15-JAN-09 - first property file version 
	0,
};

////////////////////////////////////////////////////////////////////////////////

crProperties* crProperties::AllocateAndLoad(const char* filename)
{
	crProperties* properties = rage_new crProperties;
	if(!properties->Load(filename))
	{
		Errorf("crProperties::AllocateAndLoad - failed to load '%s'", filename);
		delete properties;
		return NULL;
	}

	return properties;
}

////////////////////////////////////////////////////////////////////////////////

bool crProperties::Load(const char* filename)
{
	fiStream* f = ASSET.Open(filename, "properties", false, true);
	if(!f)
	{
		Errorf("crProperties - failed to open file '%s' for reading", filename);
		return false;
	}

	if(!fiSerializeFrom(f, *this))
	{
		Errorf("crProperties - failed to serialize file '%s' for reading", filename);
		return false;
	}

	f->Close();
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crProperties::Save(const char* filename)
{
	fiStream* f = ASSET.Create(filename, "properties");
	if(!f)
	{
		Errorf("crProperties - failed to open file '%s' for writing", filename);
		return false;
	}

	if(!fiSerializeTo(f, *this, false))
	{
		Errorf("crProperties - failed to serialize file '%s' for writing", filename);
		return false;
	}

	f->Close();
	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crProperties::Serialize(datSerialize& s)
{
	const int latestVersion = propertiesVersions[0];

	int version = latestVersion;
	s << datLabel("PropertyVersion:") << version << datNewLine;

	if(version > latestVersion)
	{
		Errorf("crProperties - attempting to load version '%d' (only support up to '%d')", version, latestVersion);
		return;
	}

	s_PropertiesSerializeVersion = version;

	// serialize properties
	int numProperties = 0;
	if(!s.IsRead())
	{
		numProperties = GetNumProperties();
	}
	s << datLabel("NumProperties:") << numProperties << datNewLine;

	bool rscLoad = s.IsRead() && pgRscBuilder::IsBuilding() && (&sysMemAllocator::GetCurrent() != &sysMemAllocator::GetMaster()); 

	if(s.IsRead())
	{
		// copy all properties
		sysMemStartTemp();
		atArray<crProperty*> properties;
		for(int i=0; i<numProperties; ++i)
		{
			crProperty* prop = rage_new crProperty;
			if(s_PropertiesSerializeVersion < 2)
			{
				SerializePropertyLegacy(s, *prop);
			}
			else
			{
				s << *prop;
			}
			
			if(!rscLoad || StringWildcardCompare("*DO_NOT_RESOURCE", prop->GetName()))
			{
				properties.Grow() = prop;
			}
			else
			{
				delete prop;
			}
		}
		sysMemEndTemp();

		// create property map
		numProperties = properties.GetCount();
		m_Properties.Create(u16(numProperties));
		for(int i=0; i < numProperties; i++)
		{
			crProperty* cloned = properties[i]->Clone();
			m_Properties.Insert(cloned->GetKey(), cloned);
		}

		// delete temporary properties
		sysMemStartTemp();
		for(int i=0; i<numProperties; ++i)
		{
			delete properties[i];
		}
		properties.Reset();
		sysMemEndTemp();
	}
	else
	{
		for(int i=0; i<numProperties; ++i)
		{
			s << *FindPropertyByIndex(i);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crProperties::Dump(crDumpOutput& output) const
{
	output.Outputf(1, "numproperties", "%d", GetNumProperties());
	output.Outputf(1, "",""); // basic formatting, readable

	int i=0;
	PropertyMap::ConstIterator it = m_Properties.CreateIterator();
	for(it.Start(); !it.AtEnd(); it.Next(), ++i)
	{
		const crProperty& prop = *it.GetData();

		output.Outputf(1, "property", i, "'%s' #%08x", prop.GetName(), u32(prop.GetKey()));
		prop.Dump(output);
		output.Outputf(1, "",""); // basic formatting, readable
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crProperties::SerializePropertyLegacy(datSerialize& s, crProperty& prop)
{
	const int maxPropNameLen = 256;
	char propName[maxPropNameLen];

	s << datLabel("PropertyName:") << datString(propName, maxPropNameLen) << datNewLine;

	prop.SetName(propName);

	int propType = 0;

	s << datLabel("PropertyType:") << propType << datNewLine;

	s << datLabel("{") << datNewLine;

	sysMemStartTemp();
	switch(propType)
	{
	case 1: // kPropertyTypeFloat
		{
			float f;
			s << datLabel("Float:") << f << datNewLine;

			crPropertyAttributeFloat attribFloat;
			attribFloat.SetName(propName);
			attribFloat.GetFloat() = f;

			sysMemEndTemp();
			prop.AddAttribute(attribFloat);
			sysMemStartTemp();
		}
		break;

	case 2: // kPropertyTypeInt
		{
			int i;
			s << datLabel("Int:") << i << datNewLine;

			crPropertyAttributeInt attribInt;
			attribInt.SetName(propName);
			attribInt.GetInt() = i;

			sysMemEndTemp();
			prop.AddAttribute(attribInt);
			sysMemStartTemp();
		}
		break;

	case 3: // kPropertyTypeVector3
		{
			Vec3V v;
			s << datLabel("Vector3:") << v << datNewLine;

			crPropertyAttributeVector3 attribVector3;
			attribVector3.SetName(propName);
			attribVector3.GetVector3() = v;

			sysMemEndTemp();
			prop.AddAttribute(attribVector3);
			sysMemStartTemp();
		}
		break;

	case 4: // kPropertyTypeQuaternion
		{
			QuatV q;
			s << datLabel("Quaternion:") << q << datNewLine;

			crPropertyAttributeQuaternion attribQuat;
			attribQuat.SetName(propName);
			attribQuat.GetQuaternion() = q;

			sysMemEndTemp();
			prop.AddAttribute(attribQuat);
			sysMemStartTemp();

		}
		break;

	case 5: // kPropertyTypeMatrix34
		{
			Mat34V mtx;
			s << datLabel("Matrix34:") << mtx << datNewLine;

			crPropertyAttributeMatrix34 attribMatrix34;
			attribMatrix34.SetName(propName);
			attribMatrix34.GetMatrix34() = mtx;

			sysMemEndTemp();
			prop.AddAttribute(attribMatrix34);
			sysMemStartTemp();
		}
		break;

	case 6: // kPropertyTypeString
		{
			atString str;
			s << datLabel("String:") << str << datNewLine;

			crPropertyAttributeString attribString;
			attribString.SetName(propName);
			attribString.GetString() = str;

			sysMemEndTemp();
			prop.AddAttribute(attribString);
			sysMemStartTemp();
		}
		break;

	case 7: // kPropertyTypeVector4
		{
			Vec4V v;
			s << datLabel("Vector4:") << v << datNewLine;

			crPropertyAttributeVector4 attribVector4;
			attribVector4.SetName(propName);
			attribVector4.GetVector4() = v;

			sysMemEndTemp();
			prop.AddAttribute(attribVector4);
			sysMemStartTemp();
		}
		break;

	case 8: // kPropertyTypeData
	case 9: // kPropertyTypeContainer
		// unsupported property types - assumed never actually used
		Assert(0);
		break;

	case 0: // kPropertyTypeNone
	default:
		// bad property type
		Assert(0);
		break;
	}
	sysMemEndTemp();
	prop.CalcSignature();

	s << datLabel("}") << datNewLine << datNewLine;

	return true;
}

////////////////////////////////////////////////////////////////////////////////

__THREAD int s_PropertiesSerializeVersion = 0;
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crProperties* crProperties::Clone() const
{
	return rage_new crProperties(*this);
}

////////////////////////////////////////////////////////////////////////////////

void crProperties::AddProperty(const crProperty& prop)
{
	crProperty::Key propKey = prop.GetKey();
	Assert(propKey);

	// remove any existing property with same name
	if(HasProperty(propKey))
	{
		RemoveProperty(propKey);
	}

	crProperty* newProp = prop.Clone();
	newProp->m_Properties = this;

	m_Properties.Insert(propKey, newProp);
}

////////////////////////////////////////////////////////////////////////////////

bool crProperties::RemoveProperty(crProperty::Key propertyKey)
{
	datOwner<crProperty>* prop = m_Properties.Access(propertyKey);
	if(prop && *prop)
	{
		delete *prop;
		*prop = NULL;
	}

	return m_Properties.Delete(u32(propertyKey));
}

////////////////////////////////////////////////////////////////////////////////

class RemovePropertiesCallback : public crProperties::PropertiesCallback
{
public:

	RemovePropertiesCallback(crProperties& properties) 
		: m_Properties(&properties)
	{
	}

	virtual ~RemovePropertiesCallback()
	{
	}

	virtual bool Callback(crProperty& prop)
	{
		m_Properties->RemoveProperty(prop.GetKey());
		return true;
	}

	crProperties* m_Properties;
};

////////////////////////////////////////////////////////////////////////////////

int crProperties::RemoveProperties(const char* propertyNameWithWildcards)
{
	RemovePropertiesCallback removePropsCb(*this);
	return FindProperties(propertyNameWithWildcards, removePropsCb);
}

////////////////////////////////////////////////////////////////////////////////

class DeletePropertiesCallback : public crProperties::PropertiesCallback
{
public:

	DeletePropertiesCallback(crProperties& properties) 
		: m_Properties(&properties)
	{
	}

	virtual ~DeletePropertiesCallback()
	{
	}

	virtual bool Callback(crProperty& prop)
	{
		delete &prop;
		return true;
	}

	crProperties* m_Properties;
};

////////////////////////////////////////////////////////////////////////////////

void crProperties::RemoveAllProperties()
{
	DeletePropertiesCallback deletePropsCb(*this);
	ForAllProperties(deletePropsCb);

	m_Properties.Kill();
}

////////////////////////////////////////////////////////////////////////////////

crProperties::ConstPropertiesCallback::ConstPropertiesCallback()
{
}

////////////////////////////////////////////////////////////////////////////////

crProperties::ConstPropertiesCallback::~ConstPropertiesCallback()
{
}

////////////////////////////////////////////////////////////////////////////////

crProperties::PropertiesCallback::PropertiesCallback()
{
}

////////////////////////////////////////////////////////////////////////////////

crProperties::PropertiesCallback::~PropertiesCallback()
{
}

////////////////////////////////////////////////////////////////////////////////

bool crProperties::ForAllProperties(ConstPropertiesCallback& cb) const
{
	PropertyMap::ConstIterator it = m_Properties.CreateIterator();
	while(it)
	{
		const crProperty& prop = *it.GetData();
		it.Next();
		if(!cb.ConstCallback(prop))
		{
			break;
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crProperties::ForAllProperties(PropertiesCallback& cb)
{
	PropertyMap::Iterator it = m_Properties.CreateIterator();
	while(it)
	{
		crProperty& prop = *it.GetData();
		it.Next();
		if(!cb.Callback(prop))
		{
			return false;
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

int crProperties::FindProperties(const char* propertyNameWithWildcards, ConstPropertiesCallback& cb) const
{
	int num = 0;

	PropertyMap::ConstIterator it = m_Properties.CreateIterator();
	while(it)
	{
		const crProperty& prop = *it.GetData();
		it.Next();
		if(!StringWildcardCompare(propertyNameWithWildcards, prop.GetName()))
		{
			num++;
			if(!cb.ConstCallback(prop))
			{
				break;
			}
		}
	}
	return num;
}

////////////////////////////////////////////////////////////////////////////////

int crProperties::FindProperties(const char* propertyNameWithWildcards, PropertiesCallback& cb)
{
	int num = 0;

	PropertyMap::Iterator it = m_Properties.CreateIterator();
	while(it)
	{
		crProperty& prop = *it.GetData();
		it.Next();
		if(!StringWildcardCompare(propertyNameWithWildcards, prop.GetName()))
		{
			num++;
			if(!cb.Callback(prop))
			{
				break;
			}
		}
	}
	return num;
}

////////////////////////////////////////////////////////////////////////////////

const crProperty* crProperties::FindPropertyByIndex(int idx) const
{
	Assert(idx < GetNumProperties());

	PropertyMap::ConstIterator it = m_Properties.CreateIterator();
	for(int i=0; i<idx; ++i)
	{
		it.Next();
	}

	return it.GetData();
}

////////////////////////////////////////////////////////////////////////////////

const char* crProperties::FindPropertyNameByIndex(int idx) const
{
	Assert(idx < GetNumProperties());

	PropertyMap::ConstIterator it = m_Properties.CreateIterator();
	for(int i=0; i<idx; ++i)
	{
		it.Next();
	}

	const crProperty* prop = it.GetData();
	return prop?prop->GetName():NULL;
}

////////////////////////////////////////////////////////////////////////////////

crProperty* crProperties::FindPropertyByIndex(int idx)
{
	Assert(idx < GetNumProperties());

	PropertyMap::Iterator it = m_Properties.CreateIterator();
	for(int i=0; i<idx; ++i)
	{
		it.Next();
	}

	return it.GetData();
}

////////////////////////////////////////////////////////////////////////////////

int crProperties::GetNumProperties() const
{
	int num = 0;
	PropertyMap::ConstIterator it = m_Properties.CreateIterator();
	while(it)
	{
		num++;
		it.Next();
	}
	return num;
}

////////////////////////////////////////////////////////////////////////////////

void crProperties::CopyProperties(const crProperties& other)
{
	for(int i=0; i<other.GetNumProperties(); ++i)
	{
		AddProperty(*other.FindPropertyByIndex(i));
	}
}

////////////////////////////////////////////////////////////////////////////////

void crProperties::UpdatePropertyInternal(crProperty& prop, crProperty::Key oldKey)
{
	m_Properties.Delete(oldKey);

	crProperty::Key propKey = prop.GetKey();
	if(HasProperty(propKey))
	{
		RemoveProperty(propKey);
	}

	m_Properties.Insert(prop.GetKey(), &prop);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

