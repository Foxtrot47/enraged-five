// 
// crmetadata/tagiterators.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CRMETADATA_TAGITERATORS_H 
#define CRMETADATA_TAGITERATORS_H 

#include "tags.h"
#include "propertyattributes.h"

#include "math/simplemath.h"

#define USE_TAG_ITERATOR_FILTER_CACHE (1)

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Base tag iterator
// Base class, defines interface for derived iterators
template <typename _T>
class crTagIteratorBase
{
protected:

	// PURPOSE: Base constructor
	crTagIteratorBase(const crTags& tags);

protected:

	// PURPOSE: Internal initialization
	void Init();

	// PURPOSE: Internal get current tag
	const crTag* GetCurrent() const;

	// PURPOSE: Internal get next tag
	const crTag* GetNext();

	// PURPOSE: Internal get prev tag
	const crTag* GetPrev();
  
	// PURPOSE: Internal direct access to tags
	const crTags& GetTags();

public:

	// PURPOSE: Advance the iterator
	crTagIteratorBase<_T>& operator++();

	// PURPOSE: Retreat the iterator
	crTagIteratorBase<_T>& operator--();

	// PURPOSE: Get the current tag
	const crTag* operator*() const;

	// PURPOSE: Reset iterator to beginning
	const crTag* Begin();

	// PURPOSE: Reset iterator to ending
	const crTag* End();

	// PURPOSE: Does this iterator have any tags?
	// NOTES: Much better performance than checking Count() == 0
	bool Empty();

	// PURPOSE: Count of iterator tags
	u32 Count();

	// PURPOSE: return the current tag index 
	int GetCurrentIndex() const { return m_TagIndex; }

private:
	const crTags* m_Tags;
	int m_TagIndex;
	int m_TagBasis;
	int m_TagBegin;
	int m_TagCount;

#if USE_TAG_ITERATOR_FILTER_CACHE
	u32 m_FilterCacheValid;
	u32 m_FilterCache;
#endif // USE_TAG_ITERATOR_FILTER_CACHE
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Base tag iterator with phase support
// Base class, adds support for phase offset/range for derived iterators
template <typename _T, bool inclusive=true>
class crTagIteratorBasePhase : public crTagIteratorBase<_T>
{
protected:

	// PURPOSE: Base constructor
	crTagIteratorBasePhase(const crTags& tags);

	// PURPOSE: Base constructor with phase offset
	crTagIteratorBasePhase(const crTags& tags, float startPhase);

	// PURPOSE: Base constructor with phase range
	crTagIteratorBasePhase(const crTags& tags, float startPhase, float endPhase);

protected:

	// PURPOSE: Filter override
	bool Filter(const crTag& tag);

	// PURPOSE: Basis override
	int CalcBasis();

protected:

	float m_StartPhase;
	float m_EndPhase;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Derived tag iterator
// Iterates tags by phase offset/range and/or tag key (inclusive or exclusive)
template <bool inclusive=true>
class crTagIteratorKey : public crTagIteratorBasePhase<crTagIteratorKey<inclusive>, inclusive>
{
public:

	// PURPOSE: Iterate tags
	crTagIteratorKey(const crTags& tags);

	// PURPOSE: Iterate tags with phase offset
	crTagIteratorKey(const crTags& tags, float startPhase);

	// PURPOSE: Iterate tags with phase range
	crTagIteratorKey(const crTags& tags, float startPhase, float endPhase);

	// PURPOSE: Iterate tags with tag key
	crTagIteratorKey(const crTags& tags, crTag::Key key);

	// PURPOSE: Iterate tags with phase offset and tag key
	crTagIteratorKey(const crTags& tags, float startPhase, crTag::Key key);

	// PURPOSE: Iterate tags with phase range and tag key
	crTagIteratorKey(const crTags& tags, float startPhase, float endPhase, crTag::Key key);

protected:

	// PURPOSE: Filter override
	bool Filter(const crTag& tag);

protected:
	crTag::Key m_Key;

	friend class crTagIteratorBase<crTagIteratorKey<inclusive> >;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Default tag iterator
// Iterates tags by phase offset/range inclusively and/or tag key
typedef crTagIteratorKey<true> crTagIterator;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Attribute property tag iterator
// Iterates tags by phase offset/range with matching tag and attribute key
template <bool inclusive=true>
class crTagIteratorPropertyAttribute : public crTagIteratorBasePhase<crTagIteratorPropertyAttribute<inclusive>, inclusive>
{
public:

	// PURPOSE: Iterate tags with tag and attribute keys
	crTagIteratorPropertyAttribute(const crTags& tags, crTag::Key propertyKey, crProperty::Key attributeKey);

	// PURPOSE: Iterate tags with phase offset, tag and attribute keys
	crTagIteratorPropertyAttribute(const crTags& tags, float startPhase, crTag::Key propertyKey, crProperty::Key attributeKey);

	// PURPOSE: Iterate tags with phase range, tag and attribute keys
	crTagIteratorPropertyAttribute(const crTags& tags, float startPhase, float endPhase, crTag::Key propertyKey, crProperty::Key attributeKey);

protected:

	// PURPOSE: Filter override
	bool Filter(const crTag& tag);

protected:
	crTag::Key m_PropertyKey;
	crProperty::Key m_AttributeKey;

	friend class crTagIteratorBase<crTagIteratorPropertyAttribute<inclusive> >;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Attribute property tag iterator
// Iterates tags by phase offset/range with matching tag and attribute key
template<typename _AttributeType, typename _ValueType, bool inclusive=true>
class crTagIteratorPropertyAttributeValue : public crTagIteratorBasePhase<crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>, inclusive>
{
public:

	// PURPOSE: Iterate tags with tag and attribute keys
	crTagIteratorPropertyAttributeValue(const crTags& tags, crTag::Key propertyKey, crProperty::Key attributeKey, _ValueType val);

	// PURPOSE: Iterate tags with phase offset, tag and attribute keys
	crTagIteratorPropertyAttributeValue(const crTags& tags, float startPhase, crTag::Key propertyKey, crProperty::Key attributeKey, _ValueType val);

	// PURPOSE: Iterate tags with phase range, tag and attribute keys
	crTagIteratorPropertyAttributeValue(const crTags& tags, float startPhase, float endPhase, crTag::Key propertyKey, crProperty::Key attributeKey, _ValueType val);

protected:

	// PURPOSE: Filter override
	bool Filter(const crTag& tag);

protected:
	crTag::Key m_PropertyKey;
	crProperty::Key m_AttributeKey;
	_ValueType m_Value;

	friend class crTagIteratorBase<crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive> >;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Multiple key tag iterator
// Iterates tags by phase offset/range and/or multiple tag keys
template <int maxKeys=8, bool inclusive=true>
class crTagIteratorMultipleKeys : public crTagIteratorBasePhase<crTagIteratorMultipleKeys<maxKeys, inclusive>, inclusive>
{
public:

	// PURPOSE: Iterate tags
	crTagIteratorMultipleKeys(const crTags& tags);

	// PURPOSE: Iterate tags with phase range
	crTagIteratorMultipleKeys(const crTags& tags, float startPhase);

	// PURPOSE: Iterate tags with phase range
	crTagIteratorMultipleKeys(const crTags& tags, float startPhase, float endPhase);

	// PURPOSE: Add key
	bool AddKey(crTag::Key key);

protected:

	// PURPOSE: Filter override
	bool Filter(const crTag& tag);

protected:
	atFixedArray<crTag::Key, maxKeys> m_Keys;

	friend class crTagIteratorBase<crTagIteratorMultipleKeys<maxKeys, inclusive> >;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Match property tag iterator
// Iterates tags by phase offset/range and/or matching properties
template <int maxProperties=4, bool inclusive=true>
class crTagIteratorMatchProperty : public crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>
{
public:

	// PURPOSE: Iterate tags
	crTagIteratorMatchProperty(const crTags& tags);

	// PURPOSE: Iterate tags with phase range
	crTagIteratorMatchProperty(const crTags& tags, float startPhase);

	// PURPOSE: Iterate tags with phase range
	crTagIteratorMatchProperty(const crTags& tags, float startPhase, float endPhase);

	// PURPOSE: Destructor
	~crTagIteratorMatchProperty();

	// PURPOSE: Add property key/value
	bool AddProperty(const crProperty& property);
	
	// PURPOSE: Serialize
	void Serialize(datSerialize&);

protected:

	// PURPOSE: Filter override
	bool Filter(const crTag& tag);

protected:
	atFixedArray<crProperty*, maxProperties> m_Properties;

	friend class crTagIteratorBase<crTagIteratorMatchProperty<maxProperties, inclusive> >;
};

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline crTagIteratorBase<_T>::crTagIteratorBase(const crTags& tags)
: m_Tags(&tags)
, m_TagIndex(-1)
, m_TagBasis(0)
, m_TagBegin(-1)
, m_TagCount(-1)
#if USE_TAG_ITERATOR_FILTER_CACHE
, m_FilterCacheValid(0)
, m_FilterCache(0)
#endif // USE_TAG_ITERATOR_FILTER_CACHE
{
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline void crTagIteratorBase<_T>::Init()
{
	_T& t = *(_T*)(this);
	m_TagBasis = t.CalcBasis();

	m_TagIndex = -1;
#if USE_TAG_ITERATOR_FILTER_CACHE
	m_FilterCacheValid = 0;
	m_FilterCache = 0;
#endif // USE_TAG_ITERATOR_FILTER_CACHE
	m_TagCount = -1;

	GetNext();

	m_TagBegin = m_TagIndex;
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline const crTag* crTagIteratorBase<_T>::GetCurrent() const
{
	const int numTags = m_Tags->m_Tags.GetCount();
	return InRange(m_TagIndex, 0, numTags-1)?m_Tags->m_Tags[(m_TagIndex+m_TagBasis)%numTags]:NULL;
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
__forceinline const crTag* crTagIteratorBase<_T>::GetNext()
{
	const int numTags = m_Tags->m_Tags.GetCount();
	const int tagBasis = m_TagBasis;
	const crTag*const* tags = reinterpret_cast<const crTag*const*>(m_Tags->m_Tags.GetElements());

	int tagIndex = m_TagIndex+1;
	u32 filterCacheValid = m_FilterCacheValid, filterCache = m_FilterCache;

	for(; tagIndex<numTags; ++tagIndex)
	{
#if USE_TAG_ITERATOR_FILTER_CACHE
		const u32 tb = (tagIndex<32)?(1<<tagIndex):0;
		if(filterCacheValid&tb)
		{
			if(filterCache&tb)
			{
				break;
			}
			continue;
		}
		filterCacheValid |= tb;
#endif // USE_TAG_ITERATOR_FILTER_CACHE

		_T& t = *(_T*)(this);
		if(t.Filter(*tags[(tagIndex+tagBasis)%numTags]))
		{
#if USE_TAG_ITERATOR_FILTER_CACHE
			filterCache |= tb;
#endif // USE_TAG_ITERATOR_FILTER_CACHE
			break;
		}
	}

	m_TagIndex = tagIndex;
	m_FilterCacheValid = filterCacheValid;
	m_FilterCache = filterCache;
	return InRange(tagIndex, 0, numTags-1)?tags[(tagIndex+tagBasis)%numTags]:NULL;
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline const crTag* crTagIteratorBase<_T>::GetPrev()
{
	const int numTags = m_Tags->m_Tags.GetCount();
	for(m_TagIndex=(m_TagIndex-1); m_TagIndex>=m_TagBegin; --m_TagIndex)
	{
#if USE_TAG_ITERATOR_FILTER_CACHE
		const u32 tb = (m_TagIndex<32)?(1<<m_TagIndex):0;
		if(m_FilterCacheValid&tb)
		{
			if(m_FilterCache&tb)
			{
				break;
			}
			continue;
		}
		m_FilterCacheValid |= tb;
#endif // USE_TAG_ITERATOR_FILTER_CACHE
		_T& t = *(_T*)(this);
		if(t.Filter(*m_Tags->m_Tags[(m_TagIndex+m_TagBasis)%numTags]))
		{
#if USE_TAG_ITERATOR_FILTER_CACHE
			m_FilterCache |= tb;
#endif // USE_TAG_ITERATOR_FILTER_CACHE
			break;
		}
	}

	return GetCurrent();
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline const crTags& crTagIteratorBase<_T>::GetTags()
{
	return *m_Tags;
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline crTagIteratorBase<_T>& crTagIteratorBase<_T>::operator++()
{
	GetNext();
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline crTagIteratorBase<_T>& crTagIteratorBase<_T>::operator--()
{
	GetPrev();
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline const crTag* crTagIteratorBase<_T>::operator*() const
{
	return GetCurrent();
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline const crTag* crTagIteratorBase<_T>::Begin()
{
	m_TagIndex = m_TagBegin;
	return GetCurrent();
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline const crTag* crTagIteratorBase<_T>::End()
{
	m_TagIndex = m_Tags->m_Tags.GetCount();
	return GetPrev();
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline bool crTagIteratorBase<_T>::Empty()
{
	const int numTags = m_Tags->m_Tags.GetCount();
	return m_TagBegin == numTags;
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T>
inline u32 crTagIteratorBase<_T>::Count()
{
	if(m_TagCount < 0)
	{
		const int numTags = m_Tags->m_Tags.GetCount();
		if(m_TagBegin < numTags)
		{
			m_TagCount = 1;
			for(int ti=(m_TagBegin+1); ti<numTags; ++ti)
			{
#if USE_TAG_ITERATOR_FILTER_CACHE
				const u32 tb = (ti<32)?(1<<ti):0;
				if(m_FilterCacheValid&tb)
				{
					if(m_FilterCache&tb)
					{
						m_TagCount++;
					}
					continue;
				}
				m_FilterCacheValid |= tb;
#endif // USE_TAG_ITERATOR_FILTER_CACHE
				_T& t = *(_T*)(this);
				if(t.Filter(*m_Tags->m_Tags[(ti+m_TagBasis)%numTags]))
				{
#if USE_TAG_ITERATOR_FILTER_CACHE
					m_FilterCache |= tb;
#endif // USE_TAG_ITERATOR_FILTER_CACHE
					m_TagCount++;
				}
			}		
		}
		else
		{
			m_TagCount = 0;
		}
	}
	return m_TagCount;
}

////////////////////////////////////////////////////////////////////////////////

template <typename _T, bool inclusive>
inline crTagIteratorBasePhase<_T, inclusive>::crTagIteratorBasePhase(const crTags& tags)
: crTagIteratorBase<_T>(tags)
, m_StartPhase(-1.f)
, m_EndPhase(-1.f)
{	
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T, bool inclusive>
inline crTagIteratorBasePhase<_T, inclusive>::crTagIteratorBasePhase(const crTags& tags, float startPhase)
: crTagIteratorBase<_T>(tags)
, m_StartPhase(startPhase)
, m_EndPhase(-1.f)
{
	Assert(InRange(startPhase, 0.f, 1.f));
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T, bool inclusive>
inline crTagIteratorBasePhase<_T, inclusive>::crTagIteratorBasePhase(const crTags& tags, float startPhase, float endPhase)
: crTagIteratorBase<_T>(tags)
, m_StartPhase(startPhase)
, m_EndPhase(endPhase)
{
	Assert(InRange(startPhase, 0.f, 1.f));
	Assert(InRange(endPhase, 0.f, 1.f));
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T, bool inclusive>
__forceinline bool crTagIteratorBasePhase<_T, inclusive>::Filter(const crTag& tag)
{
	// only filtering a phase range, if end phase >= 0.0
	if(m_EndPhase >= 0.f)
	{
		// check for special case of start == 0.0, end == 1.0
		if(m_EndPhase == 1.f && m_StartPhase == 0.f)
		{
			return true;
		}

		float tagStart = tag.GetStart();
		float tagEnd = tag.GetEnd();

		float phaseStart = m_StartPhase;
		float phaseEnd = m_EndPhase;

		// unwind tag and/or phase wraps
		if(tagEnd < tagStart)
		{
			tagEnd += 1.f;

			if(phaseEnd < tagStart)
			{
				phaseStart += 1.f;
				phaseEnd += 1.f;
			}
		}
		if(phaseEnd < phaseStart)
		{
			phaseEnd += 1.f;

			if(tagEnd < phaseStart)
			{
				tagStart += 1.f;
				tagEnd += 1.f;
			}
		}

		// check for exclusion
		if(inclusive || (m_EndPhase == 1.f))
		{
			if(((tagStart < phaseStart) || (tagStart > phaseEnd)) &&
				((tagEnd < phaseStart) || (tagEnd > phaseEnd)) && 
				((phaseStart < tagStart) || (phaseStart > tagEnd)) &&
				((phaseEnd < tagStart) || (phaseEnd > tagEnd)))
			{
				return false;
			}
		}
		else
		{
			if(((tagStart < phaseStart) || (tagStart >= phaseEnd)) &&
				((tagEnd < phaseStart) || (tagEnd >= phaseEnd)) && 
				((phaseStart < tagStart) || (phaseStart >= tagEnd)) &&
				((phaseEnd < tagStart) || (phaseEnd >= tagEnd)))
			{
				return false;
			}

		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T, bool inclusive>
inline int crTagIteratorBasePhase<_T, inclusive>::CalcBasis()
{
	// if start phase not 0.0 (or -1.0) then need to calculate offset (basis) into tag array
	if(m_StartPhase > 0.f)
	{
		const crTags& tags = crTagIteratorBase<_T>::GetTags();
		const int numTags = tags.GetNumTags();
		for(int i=0; i<numTags; ++i)
		{
			const crTag& tag = *tags.GetTag(i);
			if(tag.GetStart() >= m_StartPhase)
			{
				return i;
			}
		}
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline crTagIteratorKey<inclusive>::crTagIteratorKey(const crTags& tags)
: crTagIteratorBasePhase<crTagIteratorKey<inclusive> >(tags)
, m_Key(0)
{
	crTagIteratorBasePhase<crTagIteratorKey<inclusive> >::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline crTagIteratorKey<inclusive>::crTagIteratorKey(const crTags& tags, float startPhase)
: crTagIteratorBasePhase<crTagIteratorKey<inclusive> >(tags, startPhase)
, m_Key(0)
{
	crTagIteratorBasePhase<crTagIteratorKey<inclusive> >::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline crTagIteratorKey<inclusive>::crTagIteratorKey(const crTags& tags, float startPhase, float endPhase)
: crTagIteratorBasePhase<crTagIteratorKey<inclusive>, inclusive>(tags, startPhase, endPhase)
, m_Key(0)
{
	crTagIteratorBasePhase<crTagIteratorKey<inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline crTagIteratorKey<inclusive>::crTagIteratorKey(const crTags& tags, crTag::Key key)
: crTagIteratorBasePhase<crTagIteratorKey<inclusive>, inclusive>(tags)
, m_Key(key)
{
	Assert(key);
	m_Key = key;
	crTagIteratorBasePhase<crTagIteratorKey<inclusive> >::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline crTagIteratorKey<inclusive>::crTagIteratorKey(const crTags& tags, float startPhase, crTag::Key key)
: crTagIteratorBasePhase<crTagIteratorKey<inclusive>, inclusive>(tags, startPhase)
, m_Key(key)
{
	Assert(key);
	crTagIteratorBasePhase<crTagIteratorKey<inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline crTagIteratorKey<inclusive>::crTagIteratorKey(const crTags& tags, float startPhase, float endPhase, crTag::Key key)
: crTagIteratorBasePhase<crTagIteratorKey<inclusive>, inclusive>(tags, startPhase, endPhase)
, m_Key(key)
{
	Assert(key);
	crTagIteratorBasePhase<crTagIteratorKey<inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
__forceinline bool crTagIteratorKey<inclusive>::Filter(const crTag& tag)
{
	if(!m_Key || tag.GetKey() == m_Key)
	{
		return crTagIteratorBasePhase<crTagIteratorKey<inclusive>, inclusive>::Filter(tag);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline crTagIteratorPropertyAttribute<inclusive>::crTagIteratorPropertyAttribute(const crTags& tags, crTag::Key propertyKey, crProperty::Key attributeKey)
: crTagIteratorBasePhase<crTagIteratorPropertyAttribute<inclusive>, inclusive>(tags)
, m_PropertyKey(propertyKey)
, m_AttributeKey(attributeKey)
{
	crTagIteratorBasePhase<crTagIteratorPropertyAttribute<inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline crTagIteratorPropertyAttribute<inclusive>::crTagIteratorPropertyAttribute(const crTags& tags, float startPhase, crTag::Key propertyKey, crProperty::Key attributeKey)
: crTagIteratorBasePhase<crTagIteratorPropertyAttribute<inclusive>, inclusive>(tags, startPhase)
, m_PropertyKey(propertyKey)
, m_AttributeKey(attributeKey)
{
	crTagIteratorBasePhase<crTagIteratorPropertyAttribute<inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline crTagIteratorPropertyAttribute<inclusive>::crTagIteratorPropertyAttribute(const crTags& tags, float startPhase, float endPhase, crTag::Key propertyKey, crProperty::Key attributeKey)
: crTagIteratorBasePhase<crTagIteratorPropertyAttribute<inclusive>, inclusive>(tags, startPhase, endPhase)
, m_PropertyKey(propertyKey)
, m_AttributeKey(attributeKey)
{
	crTagIteratorBasePhase<crTagIteratorPropertyAttribute<inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<bool inclusive>
inline bool crTagIteratorPropertyAttribute<inclusive>::Filter(const crTag& tag)
{
	if(crTagIteratorBasePhase<crTagIteratorPropertyAttribute<inclusive>, inclusive>::Filter(tag))
	{
		if(m_PropertyKey) 
		{
			if(tag.GetKey() != m_PropertyKey)
			{
				return false;
			}
		}
		if(m_AttributeKey)
		{
			if(!tag.GetProperty().GetAttribute(m_AttributeKey))
			{
				return false;
			}
		}
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _AttributeType, typename _ValueType, bool inclusive>
inline crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>::crTagIteratorPropertyAttributeValue(const crTags& tags, crTag::Key propertyKey, crProperty::Key attributeKey, _ValueType val)
: crTagIteratorBasePhase<crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>, inclusive>(tags)
, m_PropertyKey(propertyKey)
, m_AttributeKey(attributeKey)
, m_Value(val)
{
	crTagIteratorBasePhase<crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<typename _AttributeType, typename _ValueType, bool inclusive>
inline crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>::crTagIteratorPropertyAttributeValue(const crTags& tags, float startPhase, crTag::Key propertyKey, crProperty::Key attributeKey, _ValueType val)
: crTagIteratorBasePhase<crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>, inclusive>(tags, startPhase)
, m_PropertyKey(propertyKey)
, m_AttributeKey(attributeKey)
, m_Value(val)
{
	crTagIteratorBasePhase<crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<typename _AttributeType, typename _ValueType, bool inclusive>
inline crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>::crTagIteratorPropertyAttributeValue(const crTags& tags, float startPhase, float endPhase, crTag::Key propertyKey, crProperty::Key attributeKey, _ValueType val)
: crTagIteratorBasePhase<crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>, inclusive>(tags, startPhase, endPhase)
, m_PropertyKey(propertyKey)
, m_AttributeKey(attributeKey)
, m_Value(val)
{
	crTagIteratorBasePhase<crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<typename _AttributeType, typename _ValueType, bool inclusive>
inline bool crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>::Filter(const crTag& tag)
{
	if(crTagIteratorBasePhase<crTagIteratorPropertyAttributeValue<_AttributeType, _ValueType, inclusive>, inclusive>::Filter(tag))
	{
		if(m_PropertyKey) 
		{
			if(tag.GetKey() != m_PropertyKey)
			{
				return false;
			}
		}
		if(m_AttributeKey)
		{
			crPropertyAttributeAccessor<_AttributeType, _ValueType> accessor(tag.GetProperty().GetAttribute(m_AttributeKey));
			return accessor.Compare(m_Value);
		}
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template<int maxKeys, bool inclusive> 
inline crTagIteratorMultipleKeys<maxKeys, inclusive>::crTagIteratorMultipleKeys(const crTags& tags)
: crTagIteratorBasePhase<crTagIteratorMultipleKeys<maxKeys, inclusive>, inclusive>(tags)
{
	crTagIteratorBasePhase<crTagIteratorMultipleKeys<maxKeys, inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<int maxKeys, bool inclusive>
inline crTagIteratorMultipleKeys<maxKeys, inclusive>::crTagIteratorMultipleKeys(const crTags& tags, float startPhase)
: crTagIteratorBasePhase<crTagIteratorMultipleKeys<maxKeys, inclusive>, inclusive>(tags, startPhase)
{
	crTagIteratorBasePhase<crTagIteratorMultipleKeys<maxKeys, inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<int maxKeys, bool inclusive>
inline crTagIteratorMultipleKeys<maxKeys, inclusive>::crTagIteratorMultipleKeys(const crTags& tags, float startPhase, float endPhase)
: crTagIteratorBasePhase<crTagIteratorMultipleKeys<maxKeys, inclusive>, inclusive>(tags, startPhase, endPhase)
{
	crTagIteratorBasePhase<crTagIteratorMultipleKeys<maxKeys, inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template<int maxKeys, bool inclusive>
inline bool crTagIteratorMultipleKeys<maxKeys, inclusive>::AddKey(crTag::Key key)
{
	if(m_Keys.GetCount() < maxKeys)
	{
		m_Keys.Append() = key;
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

template<int maxKeys, bool inclusive>
inline bool crTagIteratorMultipleKeys<maxKeys, inclusive>::Filter(const crTag& tag)
{
	if(crTagIteratorBasePhase<crTagIteratorMultipleKeys<maxKeys, inclusive>, inclusive>::Filter(tag))
	{
		const int numKeys = m_Keys.GetCount();
		if(numKeys > 0) 
		{
			for(int i=0; i<numKeys; ++i)
			{
				if(m_Keys[i] == tag.GetKey())
				{
					return true;
				}
			}
			return false;
		}
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template <int maxProperties, bool inclusive>
inline crTagIteratorMatchProperty<maxProperties, inclusive>::crTagIteratorMatchProperty(const crTags& tags)
: crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>(tags)
{
	crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template <int maxProperties, bool inclusive>
inline crTagIteratorMatchProperty<maxProperties, inclusive>::crTagIteratorMatchProperty(const crTags& tags, float startPhase)
: crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>(tags, startPhase)
{
	crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template <int maxProperties, bool inclusive>
inline crTagIteratorMatchProperty<maxProperties, inclusive>::crTagIteratorMatchProperty(const crTags& tags, float startPhase, float endPhase)
: crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>(tags, startPhase, endPhase)
{
	crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>::Init();
}

////////////////////////////////////////////////////////////////////////////////

template <int maxProperties, bool inclusive>
inline crTagIteratorMatchProperty<maxProperties, inclusive>::~crTagIteratorMatchProperty()
{
#if !HACK_GTA4
	for (int i=0; i<m_Properties.GetCount(); ++i)
	{
		delete m_Properties[i];
	}
#endif // !HACK_GTA4
	m_Properties.Reset();
}

////////////////////////////////////////////////////////////////////////////////

template <int maxProperties, bool inclusive>
inline bool crTagIteratorMatchProperty<maxProperties, inclusive>::AddProperty(const crProperty& property)
{
	if(m_Properties.GetCount() < maxProperties)
	{
#if HACK_GTA4
		m_Properties.Append() = const_cast<crProperty*>(&property);
#else // HACK_GTA4
		m_Properties.Append() = property.Clone();
#endif // HACK_GTA4
		crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>::Init();		
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

extern int g_crTagIteratorMatchPropertyVersions[];

////////////////////////////////////////////////////////////////////////////////

template <int maxProperties, bool inclusive>
void crTagIteratorMatchProperty<maxProperties, inclusive>::Serialize(datSerialize& s)
{
	const int latest = g_crTagIteratorMatchPropertyVersions[0];

	int version = latest;
	s << datLabel("IteratorVersion:") << version << datNewLine;

	if (version > latest) 
	{
		Errorf("crTagIteratorMatchProperty::Serialize - attempting to load version '%d' (only support up to '%d')", version , latest);
		return;
	}

	int numProperties = m_Properties.GetCount();
	s << numProperties;

	if (s.IsRead() && numProperties >= m_Properties.GetMaxCount())
	{
		Errorf("crTagIteratorMatchProperty::Serialize - attempting to load '%d' properties (only support up to '%d')", numProperties, m_Properties.GetMaxCount());
		return;
	}

	for (int i=0; i<numProperties; ++i)
	{
		if (s.IsRead())
		{
			m_Properties[i] = rage_new crProperty;
		}
		s << *m_Properties[i];
	}

	// ugh, these are now in the base class (but they're only protected)
	s << crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>::m_StartPhase;
	s << crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>::m_EndPhase;
}

////////////////////////////////////////////////////////////////////////////////

template <int maxProperties, bool inclusive>
inline bool crTagIteratorMatchProperty<maxProperties, inclusive>::Filter(const crTag& tag)
{
	if(crTagIteratorBasePhase<crTagIteratorMatchProperty<maxProperties, inclusive>, inclusive>::Filter(tag))
	{
		const int numProps = m_Properties.GetCount();
		if(numProps > 0) 
		{
			for(int i=0; i<numProps; ++i)
			{
				// Call operator ==(). Compares keys attributes (including their keys, types and values)
				if(*m_Properties[i] == tag.GetProperty())
				{
					return true;
				}
			}
			return false;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMETADATA_TAGITERATORS_H 
