// 
// crmetadata/tagiterators.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "tagiterators.h"

namespace rage 
{
	int g_crTagIteratorMatchPropertyVersions[] =
	{
		0, // DA 08-FEB-11 - initial version
	};
} // namespace rage
