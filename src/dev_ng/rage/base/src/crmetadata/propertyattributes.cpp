// 
// crmetadata/propertyattributes.cpp 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "propertyattributes.h"
#include "properties.h"

#include "dumpoutput.h"

#include "atl/array_struct.h"
#include "data/safestruct.h"
#include "math/amath.h"

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute::crPropertyAttribute(eType type)
{
	Assert(type != kTypeNone);
	Assert(!datResource_IsDefragmentation);  // TEMPORARY ASSERT - TRYING TO CATCH BUG 184231
	m_Type = type;
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute::crPropertyAttribute(datResource& rsc)
: m_Name(rsc)
{
	Assert(m_Type != kTypeNone);  // TEMPORARY ASSERT - TRYING TO CATCH BUG 184231
	Assert(m_Type != kTypeNum);  // TEMPORARY ASSERT - STILL TRYING TO CATCH BUG 184231
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crPropertyAttribute::eType);
void crPropertyAttribute::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttribute, m_Type)
	SSTRUCT_FIELD(crPropertyAttribute, m_Name)
	SSTRUCT_END(crPropertyAttribute)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttribute::Place(void *that, datResource& rsc)
{
	Assert(((crPropertyAttribute*)that)->GetType() != kTypeNone);  // TEMPORARY ASSERT - TRYING TO CATCH BUG 184231
	Assert(((crPropertyAttribute*)that)->GetType() != kTypeNum);   // TEMPORARY ASSERT - STILL TRYING TO CATCH BUG 184231
	const TypeInfo* info = FindTypeInfo(((crPropertyAttribute*)that)->GetType());
	Assert(info);

	PlaceFn* placeFn = info->GetPlaceFn();
	placeFn((crPropertyAttribute*)that, rsc);
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttribute::VirtualConstructFromPtr(crPropertyAttribute* base, datResource& rsc)
{
	Assert(base);
	base->Place(base, rsc);
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute::~crPropertyAttribute()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttribute::Shutdown()
{
	Assert(!datResource_IsDefragmentation);  // TEMPORARY ASSERT - STILL TRYING TO CATCH BUG 184231
	m_Type = kTypeNum;
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute* crPropertyAttribute::Allocate(eType type)
{
	const TypeInfo* info = FindTypeInfo(type);
	if(info)
	{
		AllocateFn* allocateFn = info->GetAllocateFn();
		return allocateFn();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttribute::Serialize(datSerialize& s)
{
	int type = int(m_Type);
	s << type;
	if(s.IsRead())
	{
		m_Type = eType(type);
	}
	s << m_Name;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttribute::Dump(crDumpOutput& output) const
{
	output.Outputf(3, "attributename", "'%s'", GetName());
	output.Outputf(3, "attributekey", "%08x", GetKey());
	output.Outputf(3, "attributetype", "'%s' %d", GetTypeName(), int(GetType()));
}

////////////////////////////////////////////////////////////////////////////////

const char* crPropertyAttribute::GetTypeName() const
{
	const TypeInfo* info = FindTypeInfo(GetType());
	if(info)
	{
		return info->GetName();
	}

	return "UNKNOWN_PROPERTY_ATTRIBUTE_TYPE";
}
#endif //CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttribute::operator==(const crPropertyAttribute& other) const
{
	if(m_Name.GetKey() != other.m_Name.GetKey())
	{
		return false;
	}
	if(m_Type != other.m_Type)
	{
		return false;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttribute::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		sm_TypeInfos.Resize(kTypeNum);
		for(int i=0; i<kTypeNum; ++i)
		{
			sm_TypeInfos[i] = NULL;
		}

		// register all built in property attribute types
		crPropertyAttributeFloat::InitClass();
		crPropertyAttributeInt::InitClass();
		crPropertyAttributeBool::InitClass();
		crPropertyAttributeString::InitClass();
		crPropertyAttributeBitSet::InitClass();
		crPropertyAttributeVector3::InitClass();
		crPropertyAttributeVector4::InitClass();
		crPropertyAttributeQuaternion::InitClass();
		crPropertyAttributeMatrix34::InitClass();
		crPropertyAttributeSituation::InitClass();
		crPropertyAttributeData::InitClass();
		crPropertyAttributeHashString::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttribute::ShutdownClass()
{
	sm_TypeInfos.Reset();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttribute::CalcSignature() const
{
	return m_Name.GetKey();
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute::TypeInfo::TypeInfo(eType type, const char* name, AllocateFn* allocateFn, PlaceFn* placeFn)
: m_Type(type)
, m_Name(name)
, m_AllocateFn(allocateFn)
, m_PlaceFn(placeFn)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute::TypeInfo::~TypeInfo()
{
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttribute::TypeInfo::Register() const
{
	crPropertyAttribute::RegisterTypeInfo(*this);
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute::eType crPropertyAttribute::TypeInfo::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

const char* crPropertyAttribute::TypeInfo::GetName() const
{
	return m_Name;
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute::AllocateFn* crPropertyAttribute::TypeInfo::GetAllocateFn() const
{
	return m_AllocateFn;
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttribute::PlaceFn* crPropertyAttribute::TypeInfo::GetPlaceFn() const
{
	return m_PlaceFn;
}

////////////////////////////////////////////////////////////////////////////////

const crPropertyAttribute::TypeInfo* crPropertyAttribute::FindTypeInfo(crPropertyAttribute::eType type)
{
	if(type < kTypeNum)
	{
		Assert(type > kTypeNone);
		Assert(sm_TypeInfos.GetCount() > 0);
		Assert(type < sm_TypeInfos.GetCount());

		return sm_TypeInfos[type];
	}
	else
	{
		const int numProperties = sm_TypeInfos.GetCount();
		for(int i=kTypeNum; i<numProperties; ++i)
		{
			if(sm_TypeInfos[i])
			{
				if(sm_TypeInfos[i]->GetType() == type)
				{
					return sm_TypeInfos[i];
				}
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttribute::RegisterTypeInfo(const crPropertyAttribute::TypeInfo& info)
{
	Assert(sm_TypeInfos.GetCount() > 0);

	if(info.GetType() < kTypeNum)
	{
		Assert(info.GetType() < sm_TypeInfos.GetCount());
		Assert(sm_TypeInfos[info.GetType()] == NULL);

		sm_TypeInfos[info.GetType()] = &info;
	}
	else
	{
		Assert(info.GetType() >= kTypeCustom);
		Assert(!FindTypeInfo(info.GetType()));

		sm_TypeInfos.Grow() = &info;
	}
}

////////////////////////////////////////////////////////////////////////////////

atArray<const crPropertyAttribute::TypeInfo*> crPropertyAttribute::sm_TypeInfos;

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttribute::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeFloat::crPropertyAttributeFloat()
: crPropertyAttribute(kTypeFloat)
, m_Float(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeFloat::crPropertyAttributeFloat(datResource& rsc)
: crPropertyAttribute(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeFloat::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeFloat, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeFloat, m_Float)
	SSTRUCT_END(crPropertyAttributeFloat)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeFloat::~crPropertyAttributeFloat()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeFloat::Shutdown()
{
	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeFloat, crPropertyAttribute::kTypeFloat);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeFloat::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);
	s << m_Float;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeFloat::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	output.Outputf(3, "float", "%f", m_Float);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeFloat::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeFloat& otherFloat = static_cast<const crPropertyAttributeFloat&>(other);
		return (m_Float == otherFloat.m_Float);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeFloat::CalcSignature() const
{
	return atDataHash(reinterpret_cast<const u32*>(&m_Float), sizeof(f32), crPropertyAttribute::CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeInt::crPropertyAttributeInt()
: crPropertyAttribute(kTypeInt)
, m_Int(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeInt::crPropertyAttributeInt(datResource& rsc)
: crPropertyAttribute(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeInt::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeInt, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeInt, m_Int)
	SSTRUCT_END(crPropertyAttributeInt)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeInt::~crPropertyAttributeInt()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeInt::Shutdown()
{
	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeInt, crPropertyAttribute::kTypeInt);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeInt::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);
	s << m_Int;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeInt::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	output.Outputf(3, "integer", "%d", m_Int);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeInt::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeInt& otherInt = static_cast<const crPropertyAttributeInt&>(other);
		return (m_Int == otherInt.m_Int);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeInt::CalcSignature() const
{
	return atDataHash(reinterpret_cast<const u32*>(&m_Int), sizeof(s32), crPropertyAttribute::CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeBool::crPropertyAttributeBool()
: crPropertyAttribute(kTypeBool)
, m_Bool(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeBool::crPropertyAttributeBool(datResource& rsc)
: crPropertyAttribute(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeBool::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeBool, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeBool, m_Bool)
	SSTRUCT_IGNORE(crPropertyAttributeBool, m_Padding)
	SSTRUCT_END(crPropertyAttributeBool)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeBool::~crPropertyAttributeBool()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeBool::Shutdown()
{
	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeBool, crPropertyAttribute::kTypeBool);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeBool::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);
	s << m_Bool;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeBool::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	output.Outputf(3, "bool", "%s", (m_Bool?"true":"false"));
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeBool::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeBool& otherBool = static_cast<const crPropertyAttributeBool&>(other);
		return (m_Bool == otherBool.m_Bool);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeBool::CalcSignature() const
{
	return atDataHash(reinterpret_cast<const char*>(&m_Bool), sizeof(u8), crPropertyAttribute::CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeString::crPropertyAttributeString()
: crPropertyAttribute(crPropertyAttribute::kTypeString)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeString::crPropertyAttributeString(datResource& rsc)
: crPropertyAttribute(rsc)
, m_String(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeString::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeString, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeString, m_String)
	SSTRUCT_END(crPropertyAttributeString)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeString::~crPropertyAttributeString()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeString::Shutdown()
{
	m_String.Reset();

	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeString, crPropertyAttribute::kTypeString);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeString::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);

	s << m_String;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeString::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	output.Outputf(3, "string", "%s", m_String.c_str());
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeString::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeString& otherString = static_cast<const crPropertyAttributeString&>(other);
		return (m_String == otherString.m_String);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeString::CalcSignature() const
{
	const char* cstr = m_String.c_str();
	return cstr ? atStringHash(cstr, crPropertyAttribute::CalcSignature()) : 0;
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeBitSet::crPropertyAttributeBitSet()
: crPropertyAttribute(crPropertyAttribute::kTypeBitSet)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeBitSet::crPropertyAttributeBitSet(datResource& rsc)
: crPropertyAttribute(rsc)
, m_BitSet(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeBitSet::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeBitSet, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeBitSet, m_BitSet)
	SSTRUCT_END(crPropertyAttributeBitSet)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeBitSet::~crPropertyAttributeBitSet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeBitSet::Shutdown()
{
	m_BitSet.Reset();

	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeBitSet, crPropertyAttribute::kTypeBitSet);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeBitSet::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);

	int numBits = m_BitSet.GetNumBits();
	s << numBits;
	if(s.IsRead())
	{
		m_BitSet.Init(numBits);
	}

	for(int i=0; i<numBits; ++i)
	{
		bool bit = m_BitSet.IsSet(i);
		s << bit;
		if(s.IsRead())
		{
			m_BitSet.Set(i, bit);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeBitSet::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	const u32 numBits = u32(m_BitSet.GetNumBits());
	output.Outputf(3, "numbits", "%d", numBits);
	
	atString bits;
	for(u32 i=0; i<numBits;)
	{
		u32 bitword = 0;
		for(u32 b=0; b<32 && i<numBits; ++b, ++i)
		{
			if(m_BitSet.IsSet(i))
			{
				bitword |= (1<<b);
			}
		}

		char buf[16];
		formatf(buf, "%08x ", bitword);

		bits += buf;
	}

	output.Outputf(3, "bitset", "%s", bits.c_str());
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeBitSet::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeBitSet& otherBitSet = static_cast<const crPropertyAttributeBitSet&>(other);
		if(m_BitSet.GetNumBits() == otherBitSet.m_BitSet.GetNumBits())
		{
			const int numBits = m_BitSet.GetNumBits();
			for(int i=0; i<numBits; ++i)
			{
				if(m_BitSet.IsSet(i) != otherBitSet.m_BitSet.IsSet(i))
				{
					return false;
				}
			}
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeBitSet::CalcSignature() const
{
	u32 signature = crPropertyAttribute::CalcSignature();
	const int numBits = m_BitSet.GetNumBits();
	for(int i=0; i<numBits; ++i)
	{
		u32 isSet = m_BitSet.IsSet(i);
		signature = atDataHash(reinterpret_cast<const u32*>(&isSet), sizeof(u32), signature);
	}
	return signature;
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeVector3::crPropertyAttributeVector3()
: crPropertyAttribute(crPropertyAttribute::kTypeVector3)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeVector3::crPropertyAttributeVector3(datResource& rsc)
: crPropertyAttribute(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeVector3::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeVector3, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeVector3, m_Vector3)
	SSTRUCT_END(crPropertyAttributeVector3)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeVector3::~crPropertyAttributeVector3()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeVector3::Shutdown()
{
	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeVector3, crPropertyAttribute::kTypeVector3);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeVector3::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);

	s << m_Vector3;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeVector3::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	output.Outputf(3, "vector3", "x %f y %f z %f", m_Vector3[0], m_Vector3[1], m_Vector3[2]);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeVector3::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeVector3& otherVector3 = static_cast<const crPropertyAttributeVector3&>(other);
		return IsEqualAll(m_Vector3, otherVector3.m_Vector3)!=0;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeVector3::CalcSignature() const
{
	return atDataHash(reinterpret_cast<const u32*>(&m_Vector3), 3*sizeof(f32), crPropertyAttribute::CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeVector4::crPropertyAttributeVector4()
: crPropertyAttribute(crPropertyAttribute::kTypeVector4)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeVector4::crPropertyAttributeVector4(datResource& rsc)
: crPropertyAttribute(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeVector4::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeVector4, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeVector4, m_Vector4)
	SSTRUCT_END(crPropertyAttributeVector4)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeVector4::~crPropertyAttributeVector4()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeVector4::Shutdown()
{
	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeVector4, crPropertyAttribute::kTypeVector4);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeVector4::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);

	s << m_Vector4;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeVector4::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	output.Outputf(3, "vector4", "x %f y %f z %f w %f", m_Vector4[0], m_Vector4[1], m_Vector4[2], m_Vector4[3]);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeVector4::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeVector4& otherVector4 = static_cast<const crPropertyAttributeVector4&>(other);
		return IsEqualAll(m_Vector4, otherVector4.m_Vector4)!=0;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeVector4::CalcSignature() const
{
	return atDataHash(reinterpret_cast<const u32*>(&m_Vector4), 4*sizeof(f32), crPropertyAttribute::CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeQuaternion::crPropertyAttributeQuaternion()
: crPropertyAttribute(crPropertyAttribute::kTypeQuaternion)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeQuaternion::crPropertyAttributeQuaternion(datResource& rsc)
: crPropertyAttribute(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeQuaternion::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeQuaternion, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeQuaternion, m_Quaternion)
	SSTRUCT_END(crPropertyAttributeQuaternion)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeQuaternion::~crPropertyAttributeQuaternion()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeQuaternion::Shutdown()
{
	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeQuaternion, crPropertyAttribute::kTypeQuaternion);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeQuaternion::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);

	s << m_Quaternion;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeQuaternion::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	output.Outputf(3, "quaternion", "x %f y %f z %f w %f", m_Quaternion[0], m_Quaternion[1], m_Quaternion[2], m_Quaternion[3]);

	Vec3V e = QuatVToEulersXYZ(m_Quaternion);
	e *= ScalarVFromF32(RtoD);
	output.Outputf(3, "(as euler)", "x %f y %f z %f", e[0], e[1], e[2]);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeQuaternion::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeQuaternion& otherQuaternion = static_cast<const crPropertyAttributeQuaternion&>(other);
		return IsEqualAll(m_Quaternion, otherQuaternion.m_Quaternion)!=0;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeQuaternion::CalcSignature() const
{
	return atDataHash(reinterpret_cast<const u32*>(&m_Quaternion), 4*sizeof(f32), crPropertyAttribute::CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeMatrix34::crPropertyAttributeMatrix34()
: crPropertyAttribute(crPropertyAttribute::kTypeMatrix34)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeMatrix34::crPropertyAttributeMatrix34(datResource& rsc)
: crPropertyAttribute(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeMatrix34::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeMatrix34, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeMatrix34, m_Matrix34)
	SSTRUCT_END(crPropertyAttributeMatrix34)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeMatrix34::~crPropertyAttributeMatrix34()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeMatrix34::Shutdown()
{
	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeMatrix34, crPropertyAttribute::kTypeMatrix34);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeMatrix34::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);

	s << m_Matrix34;
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeMatrix34::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	output.Outputf(3, "matrix3x4[0]", "x %f y %f z %f", m_Matrix34.GetCol0()[0], m_Matrix34.GetCol0()[1], m_Matrix34.GetCol0()[2]);
	output.Outputf(3, "matrix3x4[1]", "x %f y %f z %f", m_Matrix34.GetCol1()[0], m_Matrix34.GetCol1()[1], m_Matrix34.GetCol1()[2]);
	output.Outputf(3, "matrix3x4[2]", "x %f y %f z %f", m_Matrix34.GetCol2()[0], m_Matrix34.GetCol2()[1], m_Matrix34.GetCol2()[2]);
	output.Outputf(3, "matrix3x4[3]", "x %f y %f z %f", m_Matrix34.GetCol3()[0], m_Matrix34.GetCol3()[1], m_Matrix34.GetCol3()[2]);

	Vec3V e = QuatVToEulersXYZ(QuatVFromMat33V(m_Matrix34.GetMat33()));
	e *= ScalarVFromF32(RtoD);
	output.Outputf(3, "(as euler)", "x %f y %f z %f", e[0], e[1], e[2]);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeMatrix34::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeMatrix34& otherMatrix34 = static_cast<const crPropertyAttributeMatrix34&>(other);
		return IsEqualAll(m_Matrix34, otherMatrix34.m_Matrix34)!=0;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeMatrix34::CalcSignature() const
{
	Vec3V col0=m_Matrix34.GetCol0(), col1=m_Matrix34.GetCol1(), col2=m_Matrix34.GetCol2(), col3=m_Matrix34.GetCol3();
	u32 signature = crPropertyAttribute::CalcSignature();
	signature = atDataHash(reinterpret_cast<const u32*>(&col0), 3*sizeof(f32), signature);
	signature = atDataHash(reinterpret_cast<const u32*>(&col1), 3*sizeof(f32), signature);
	signature = atDataHash(reinterpret_cast<const u32*>(&col2), 3*sizeof(f32), signature);
	signature = atDataHash(reinterpret_cast<const u32*>(&col3), 3*sizeof(f32), signature);
	return signature;
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeSituation::crPropertyAttributeSituation()
: crPropertyAttribute(crPropertyAttribute::kTypeSituation)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeSituation::crPropertyAttributeSituation(datResource& rsc)
: crPropertyAttribute(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeSituation::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeSituation, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeSituation, m_Situation)
	SSTRUCT_END(crPropertyAttributeSituation)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeSituation::~crPropertyAttributeSituation()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeSituation::Shutdown()
{
	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeSituation, crPropertyAttribute::kTypeSituation);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeSituation::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);

	if(s_PropertiesSerializeVersion >= 3)
	{
		s << m_Situation;
	}
	else
	{
		Vec3V pos = m_Situation.GetPosition();
		s << pos;
		if(s.IsRead())
		{
			m_Situation.SetPosition(pos);
		}

		QuatV orient = m_Situation.GetRotation();
		s << orient;
		if(s.IsRead())
		{
			m_Situation.SetRotation(orient);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeSituation::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	Vec3V pos = m_Situation.GetPosition();
	QuatV rot = m_Situation.GetRotation();

	output.Outputf(3, "situation pos", "x %f y %f z %f", pos[0], pos[1], pos[2]);
	output.Outputf(3, "situation orient", "x %f y %f z %f w %f", rot[0], rot[1], rot[2], rot[3]);

	Vec3V e = QuatVToEulersXYZ(rot);
	e *= ScalarVFromF32(RtoD);
	output.Outputf(3, "(as euler)", "x %f y %f z %f", e[0], e[1], e[2]);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeSituation::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		return IsEqualIntAll(m_Situation, static_cast<const crPropertyAttributeSituation&>(other).m_Situation) != 0;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeSituation::CalcSignature() const
{
	return atDataHash(reinterpret_cast<const u32*>(&m_Situation), 7*sizeof(f32), crPropertyAttribute::CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeData::crPropertyAttributeData()
: crPropertyAttribute(crPropertyAttribute::kTypeData)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeData::crPropertyAttributeData(datResource& rsc)
: crPropertyAttribute(rsc)
, m_Data(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeData::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeData, crPropertyAttribute)
	SSTRUCT_FIELD(crPropertyAttributeData, m_Data)
	SSTRUCT_END(crPropertyAttributeData)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeData::~crPropertyAttributeData()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeData::Shutdown()
{
	m_Data.Reset();

	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeData, crPropertyAttribute::kTypeData);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeData::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);

	int numBytes = m_Data.GetCount();
	s << numBytes;
	if(s.IsRead())
	{
		m_Data.Resize(numBytes);
	}

	for(int i=0; i<numBytes; ++i)
	{
		u8 byte = m_Data[i];
		s << byte;
		if(s.IsRead())
		{
			m_Data[i] = byte;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeData::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	const int numBytes = m_Data.GetCount();
	output.Outputf(3, "numbytes", "%d", numBytes);

	for(int i=0; i<numBytes;)
	{
		atString bytes;
		for(u32 b=0; b<16 && i<numBytes; ++b, ++i)
		{
			char buf[8];
			formatf(buf, "%02x ", u32(m_Data[i]));

			bytes += buf;
		}

		output.Outputf(3, "data", (i&(~31)), "%s", bytes.c_str());
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeData::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeData& otherData = static_cast<const crPropertyAttributeData&>(other);
		if(m_Data.GetCount() == otherData.m_Data.GetCount())
		{
			const int numBytes = m_Data.GetCount();
			for(int i=0; i<numBytes; ++i)
			{
				if(m_Data[i] != otherData.m_Data[i])
				{
					return false;
				}
			}
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeData::CalcSignature() const
{
	return atDataHash(reinterpret_cast<const char*>(m_Data.GetElements()), m_Data.GetCount()*sizeof(u8), crPropertyAttribute::CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeHashString::crPropertyAttributeHashString()
: crPropertyAttribute(crPropertyAttribute::kTypeHashString)
{
}

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeHashString::crPropertyAttributeHashString(datResource& rsc)
: crPropertyAttribute(rsc)
, m_HashString(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crPropertyAttributeHashString::DeclareStruct(datTypeStruct& s)
{
	crPropertyAttribute::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crPropertyAttributeHashString, crPropertyAttribute)
	SSTRUCT_FIELD_AS(crPropertyAttributeHashString, m_HashString, u32)
	SSTRUCT_END(crPropertyAttributeHashString)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crPropertyAttributeHashString::~crPropertyAttributeHashString()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeHashString::Shutdown()
{
	m_HashString.Clear();

	crPropertyAttribute::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_PROPERTY_ATTRIBUTE_TYPE(crPropertyAttributeHashString, crPropertyAttribute::kTypeHashString);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crPropertyAttributeHashString::Serialize(datSerialize& s)
{
	crPropertyAttribute::Serialize(s);

	atString str;
	if(!s.IsRead())
	{
		str = m_HashString.GetCStr();
	}

	sysMemStartTemp();
	s << str;
	sysMemEndTemp();

	if(s.IsRead())
	{
		m_HashString.SetFromString(str.c_str());

		sysMemStartTemp();
		str.Clear();
		sysMemEndTemp();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crPropertyAttributeHashString::Dump(crDumpOutput& output) const
{
	crPropertyAttribute::Dump(output);

	output.Outputf(3, "hash string", "%s", m_HashString.GetCStr());
	output.Outputf(3, "hash id", "%x", m_HashString.GetHash());
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crPropertyAttributeHashString::operator==(const crPropertyAttribute& other) const
{
	if(crPropertyAttribute::operator==(other))
	{
		const crPropertyAttributeHashString& otherHashString = static_cast<const crPropertyAttributeHashString&>(other);
		return (m_HashString == otherHashString.m_HashString);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crPropertyAttributeHashString::CalcSignature() const
{
	return m_HashString.GetHash();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
