// 
// crmetadata/dumpoutput.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "dumpoutput.h"

#if CR_DEV

#include "file/asset.h"
#include "file/device.h"
#include "math/amath.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crDumpOutput::crDumpOutput()
: m_Verbosity(2)
, m_Level(0)
, m_First(true)
{

}

////////////////////////////////////////////////////////////////////////////////

crDumpOutput::~crDumpOutput()
{

}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutput::Outputf(int verbosity, const char* field, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	InternalOutputf(verbosity, field, fmt, args);

	va_end(args);
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutput::Outputf(int verbosity, const char* field, int element, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	InternalOutputf(verbosity, field, element, fmt, args);

	va_end(args);
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutput::Outputf(int verbosity, const char* field, int element, int subelement, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	InternalOutputf(verbosity, field, element, subelement, fmt, args);

	va_end(args);
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutput::SetVerbosity(int verbosity)
{
	m_Verbosity = verbosity;
}

////////////////////////////////////////////////////////////////////////////////

int crDumpOutput::GetVerbosity() const
{
	return m_Verbosity;
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutput::PushLevel(const char*)
{
	m_Level++;
	m_First = true;
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutput::PopLevel()
{
	m_Level--;
}

////////////////////////////////////////////////////////////////////////////////

crDumpOutputStdIo::crDumpOutputStdIo()
{
}

////////////////////////////////////////////////////////////////////////////////

crDumpOutputStdIo::~crDumpOutputStdIo()
{
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputStdIo::InternalOutputf(int verbosity, const char* field, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		PrivateOutputf(field, fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputStdIo::InternalOutputf(int verbosity, const char* field, int element, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		char elementBuf[64];
		formatf(elementBuf, "[%d]", element);

		atString fieldElement;
		fieldElement = field;
		fieldElement += elementBuf;

		PrivateOutputf(fieldElement.c_str(), fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputStdIo::InternalOutputf(int verbosity, const char* field, int element, int subelement, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		char elementBuf[64];
		formatf(elementBuf, "[%d][%d]", element, subelement);

		atString fieldElement;
		fieldElement = field;
		fieldElement += elementBuf;

		PrivateOutputf(fieldElement.c_str(), fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputStdIo::PrivateOutputf(const char* field, const char *fmt, va_list& args)
{
	const int bufSize = 512;
	char buf[bufSize];

	// indent
	for(int i=0; i<m_Level; ++i)
	{
		if(m_First && i==(m_Level-1))
		{
			buf[i*2] = '+';
		}
		else
		{
			buf[i*2] = '|';
		}
		buf[i*2+1] = ' ';
	}
	buf[m_Level*2] = '\0';

	strcat(buf, field);
	strcat(buf, " ");

	if(m_Level == 0)
	{
		const int padding = 20 - Max(int(strlen(field)), 19);
		const int buflen = int(strlen(buf));

		for(int i=0; i<padding; ++i)
		{
			buf[buflen+i] = ' ';
		}
		buf[buflen+padding] = '\0';
	}

	// convert variable arguments into buffer and output
	if(fmt)
	{
		const int buflen = int(strlen(buf));
		vformatf(buf+buflen, bufSize-buflen, fmt, args);
	}

	Displayf("%s", buf);

	m_First = false;
}

////////////////////////////////////////////////////////////////////////////////

crDumpOutputString::crDumpOutputString()
{

}

////////////////////////////////////////////////////////////////////////////////

crDumpOutputString::~crDumpOutputString()
{

}

////////////////////////////////////////////////////////////////////////////////

const atString& crDumpOutputString::GetString() const
{
	return m_String;
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputString::PrintString() const
{
	const char* ch = m_String.c_str();
	while(*ch)
	{
		const int maxLineLen = 254;
		char lineBuf[maxLineLen+1];

		int i=0;
		for(; i<maxLineLen && *ch; ++i)
		{
			if(*ch == '\n')
			{
				++ch;
				break;
			}

			lineBuf[i] = *(ch++);
		}

		lineBuf[i] = '\0';
		Printf("%s\n", lineBuf);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputString::InternalOutputf(int verbosity, const char* field, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		PrivateOutputf(field, fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputString::InternalOutputf(int verbosity, const char* field, int element, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		char elementBuf[64];
		formatf(elementBuf, "%d", element);

		atString fieldElement;
		fieldElement = field;
		fieldElement += elementBuf;

		PrivateOutputf(fieldElement.c_str(), fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputString::InternalOutputf(int verbosity, const char* field, int element, int subelement, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		char elementBuf[64];
		formatf(elementBuf, "%d_%d", element, subelement);

		atString fieldElement;
		fieldElement = field;
		fieldElement += elementBuf;

		PrivateOutputf(fieldElement.c_str(), fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputString::PrivateOutputf(const char* field, const char *fmt, va_list& args)
{
	// indent
	for(int i=0; i<m_Level; ++i)
	{
		if(m_First && i==(m_Level-1))
		{
			m_String += "+";
		}
		else
		{
			m_String += "|";
		}
	}

	m_String += field;
	m_String += ' ';
	
	if(m_Level == 0)
	{
		const int padding = 20 - int(strlen(field));
		for(int i=0; i<padding; ++i)
		{
			m_String += ' ';
		}
	}

	// convert variable arguments into buffer and output
	const int bufSize = 256;
	char buf[bufSize];
	if(fmt)
	{
		vformatf(buf, bufSize, fmt, args);
	}
	else
	{
		buf[0] = '\0';
	}

	m_String += buf;
	m_String += '\n';

	m_First = false;
}

////////////////////////////////////////////////////////////////////////////////

crDumpOutputGrowBuffer::crDumpOutputGrowBuffer()
{
	m_Buffer.Init(NULL, datGrowBuffer::NULL_TERMINATE);

	char name[RAGE_MAX_PATH];
	fiDevice::MakeGrowBufferFileName(name, RAGE_MAX_PATH, &m_Buffer);

	m_Stream = ASSET.Create(name, "");
}

////////////////////////////////////////////////////////////////////////////////

crDumpOutputGrowBuffer::~crDumpOutputGrowBuffer()
{
	m_Stream->Close();
}

////////////////////////////////////////////////////////////////////////////////

const char* crDumpOutputGrowBuffer::GetBuffer() const
{
	m_Stream->Flush();

	return reinterpret_cast<const char*>(m_Buffer.GetBuffer());;
}

////////////////////////////////////////////////////////////////////////////////

unsigned crDumpOutputGrowBuffer::GetLength() const
{
	m_Stream->Flush();

	return m_Buffer.Length();
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputGrowBuffer::PrintBuffer() const
{
	const char* ch = GetBuffer();
	while(*ch)
	{
		const int maxLineLen = 254;
		char lineBuf[maxLineLen+1];

		int i=0;
		for(; i<maxLineLen && *ch; ++i)
		{
			if(*ch == '\n')
			{
				++ch;
				break;
			}

			lineBuf[i] = *(ch++);
		}

		lineBuf[i] = '\0';
		Printf("%s\n", lineBuf);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputGrowBuffer::InternalOutputf(int verbosity, const char* field, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		PrivateOutputf(field, fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputGrowBuffer::InternalOutputf(int verbosity, const char* field, int element, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		char elementBuf[64];
		formatf(elementBuf, "%d", element);

		atString fieldElement;
		fieldElement = field;
		fieldElement += elementBuf;

		PrivateOutputf(fieldElement.c_str(), fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputGrowBuffer::InternalOutputf(int verbosity, const char* field, int element, int subelement, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		char elementBuf[64];
		formatf(elementBuf, "%d_%d", element, subelement);

		atString fieldElement;
		fieldElement = field;
		fieldElement += elementBuf;

		PrivateOutputf(fieldElement.c_str(), fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputGrowBuffer::PrivateOutputf(const char* field, const char *fmt, va_list& args)
{
	// indent
	for(int i=0; i<m_Level; ++i)
	{
		if(m_First && i==(m_Level-1))
		{
			m_Stream->Write("+", 2);
		}
		else
		{
			m_Stream->Write("|", 2);
		}
	}

	m_Stream->Write(field, (int) strlen(field));
	m_Stream->Write(" ", 1);

	if(m_Level == 0)
	{
		const int padding = 20 - int(strlen(field));
		for(int i=0; i<padding; ++i)
		{
			m_Stream->Write(" ", 1);
		}
	}

	// convert variable arguments into buffer and output
	const int bufSize = 256;
	char buf[bufSize];
	if(fmt)
	{
		vformatf(buf, bufSize, fmt, args);
	}
	else
	{
		buf[0] = '\0';
	}

	m_Stream->Write(buf, (int) strlen(buf));
	m_Stream->Write("\n", 1);

	m_First = false;
}

////////////////////////////////////////////////////////////////////////////////

crDumpOutputXml::crDumpOutputXml()
{

}

////////////////////////////////////////////////////////////////////////////////

crDumpOutputXml::~crDumpOutputXml()
{

}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputXml::PushLevel(const char* name)
{
	m_Levels.Grow() = name;

	for(int i=0; i<m_Level; ++i)
	{
		Printf("  ");
	}
	Printf("<%s>\n", m_Levels.Top().c_str());

	crDumpOutput::PushLevel(name);
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputXml::PopLevel()
{
	crDumpOutput::PopLevel();

	for(int i=0; i<m_Level; ++i)
	{
		Printf("  ");
	}
	Printf("</%s>\n", m_Levels.Top().c_str());

	m_Levels.Pop();
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputXml::InternalOutputf(int verbosity, const char* field, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		PrivateOutputf(field, fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputXml::InternalOutputf(int verbosity, const char* field, int element, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		char elementBuf[64];
		formatf(elementBuf, "[%d]", element);

		atString fieldElement;
		fieldElement = field;
		fieldElement += elementBuf;

		PrivateOutputf(fieldElement.c_str(), fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputXml::InternalOutputf(int verbosity, const char* field, int element, int subelement, const char* fmt, va_list& args)
{
	if(verbosity <= m_Verbosity)
	{
		char elementBuf[64];
		formatf(elementBuf, "[%d][%d]", element, subelement);

		atString fieldElement;
		fieldElement = field;
		fieldElement += elementBuf;

		PrivateOutputf(fieldElement.c_str(), fmt, args);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crDumpOutputXml::PrivateOutputf(const char* field, const char *fmt, va_list& args)
{
	// indent
	for(int i=0; i<m_Level; ++i)
	{
		Printf("  ");
	}
	Printf("<%s>", field);

	// convert variable arguments into buffer and output
	const int bufSize = 256;
	char buf[bufSize];
	if(fmt)
	{
		vformatf(buf, bufSize, fmt, args);
	}
	else
	{
		buf[0] = '\0';
	}
	Printf("%s", buf);

	Printf("</%s>\n", field);

	m_First = false;
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CR_DEV
