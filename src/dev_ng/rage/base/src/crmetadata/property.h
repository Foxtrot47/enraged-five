// 
// crmetadata/property.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CRMETADATA_PROPERTY_H
#define CRMETADATA_PROPERTY_H

#include "atl/array.h"
#include "paging/base.h"
#include "cranimation/animation_config.h"

namespace rage
{

class crDumpOutput;
class crProperties;
class crPropertyAttribute;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Meta data for animation system
// Single property consisting of multiple attributes
class crProperty : public pgBase
{
public:

	// PURPOSE: Constructor
	crProperty();

	// PURPOSE: Copy constructor
	crProperty(const crProperty&);

	// PURPOSE: Resource constructor
	crProperty(datResource&);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PRUPOSE: Placement
	DECLARE_PLACE(crProperty);

	// PURPOSE: Clone the property
	crProperty* Clone() const;

	// PURPOSE: Destructor
	~crProperty();

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();

#if CR_DEV
	// PURPOSE: Serialization
	void Serialize(datSerialize&);

	// PURPOSE: Dump output, for debugging
	void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Init class
	static void InitClass();

	// PURPOSE: Shutdown class
	static void ShutdownClass();


	// PURPOSE: Property hash key
	typedef u32 Key;

	// PURPOSE: Convert property name to property key
	// PARAMS: name - name to generate hash key for
	// RETURNS: hash key
	static Key CalcKey(const char* name);

	// PURPOSE: Verify that a property key is valid
	// PARAMS: name - name, hash - hash key
	// RETURNS: hash key
	static Key CalcKey(const char* name, u32 hash);

	// PURPOSE: Get property name
	// RETURNS: property name (or NULL if none)
	// NOTE: Not available on resources.  
	// Names are discarded at resource generation time, only key retained.
	// SEE ALSO: GetKey()
	const char* GetName() const;

	// PURPOSE: Get property name key
	// RETURNS: key of property name (zero if none)
	// NOTE: Available on resources (even though name is discarded)
	u32 GetKey() const;

	// PURPOSE: Set property name
	// PARAMS: name - new property name
	// NOTES: Automatically updates key.
	void SetName(const char* name);


	// PURPOSE: Get number of attributes
	// RETURNS: number of attributes on this property
	int GetNumAttributes() const;

	// PURPOSE: Get attribute by name, const version
	// PARAMS: name - name of attribute to retrieve
	// RETURNS: const pointer to attribute (or NULL if not found)
	// NOTES: Available on resources, hashes name to retrieve by key match
	// Performance is poorer than getting attribute directly using a key.
	const crPropertyAttribute* GetAttribute(const char* name) const;

	// PURPOSE: Get attribute by name, non const version
	// PARAMS: name - name of attribute to retrieve
	// RETURNS: non const pointer to attribute (or NULL if not found)
	// NOTES: Available on resources, hashes name to retrieve by key match
	// Performance is poorer than getting attribute directly using a key.
	crPropertyAttribute* GetAttribute(const char* name);

	// PURPOSE: Get attribute by key, const version
	// PARAMS: key - hash key of attribute to retrieve (see StringKey::CalcKey)
	// RETURNS: const pointer to attribute (or NULL if not found)
	// NOTES: Higher performance than getting attribute using a string name.
	const crPropertyAttribute* GetAttribute(u32 key) const;

	// PURPOSE: Get attribute by key, non const version
	// PARAMS: key - hash key of attribute to retrieve (see StringKey::CalcKey)
	// RETURNS: non const pointer to attribute (or NULL if not found)
	// NOTES: Higher performance than getting attribute using a string name.
	crPropertyAttribute* GetAttribute(u32 key);

	// PURPOSE: Get attribute by index, const version
	// PARAMS: idx - attribute index [0..numAttrib-1]
	// RETURNS: pointer to attribute
	// NOTES: Performance/ordering not guaranteed, prefer ForAllAttributes.
	const crPropertyAttribute* GetAttributeByIndex(u32 idx) const;

	// PURPOSE: Get attribute by index, non-const version
	// PARAMS: idx - attribute index [0..numAttrib-1]
	// RETURNS: pointer to attribute
	// NOTES: Performance/ordering not guaranteed, prefer ForAllAttributes.
	crPropertyAttribute* GetAttributeByIndex(u32 idx);


	// PURPOSE: Const callback typedef
	typedef bool (*ConstPropertyAttributeCallback)(const crPropertyAttribute& attribute, void* data);

	// PURPOSE:	Execute a specified callback on every attribute in this property
	// PARAMS:
	// cb - const callback function
	// data - user-specified callback data
	// RETURNS:	true - all callbacks succeeded (or no attributes found), false - callback failed
	// NOTE: No order for the attributes is guaranteed (or should be relied upon) 
	bool ForAllAttributes(ConstPropertyAttributeCallback cb, void* data) const;


	// PURPOSE: Add a new attribute
	// PARAM: attribute - new attribute to add (note; a copy will be taken and added, 
	// the caller remains responsible for original attribute passed in by parameter)
	// RETURNS: true - added, false - added (but replaced existing attribute of same name)
	// NOTES: Attributes have unique names, can't have two attributes with same name 
	// within the same property.  Adding a property with a duplicate name will result
	// in the destruction of the original property.
	bool AddAttribute(const crPropertyAttribute& attribute);

	// PURPOSE: Remove an attribute by name
	// PARAMS: name - name of attribute to remove
	// RETURNS: true - attribute found and removed, false - failed to find attribute
	bool RemoveAttribute(const char* name);

	// PURPOSE: Remove an attribute by key
	// PARAMS: key - hash key of attribute to remove (see StringKey::CalcKey)
	// RETURNS: true - attribute found and removed, false - failed to find attribute
	bool RemoveAttribute(u32 key);

	// PURPOSE: Remove all attributes
	// RETURNS: true - attribute(s) removed, false - no attributes to remove
	bool RemoveAllAttributes();


	// PURPOSE: Equality comparison
	// NOTES: Compares keys, attributes (including their keys, types and values)
	bool operator==(const crProperty&) const;

	// PURPOSE: Inequality comparison
	// NOTES: Compares keys, attributes (including their keys, types and values)
	bool operator!=(const crProperty&) const;

	
	// PURPOSE: Assignment operator
	crProperty& operator=(const crProperty&);

	// PURPOSE: Get signature
	u32 GetSignature() const;

	// PURPOSE: Calculate signature
	void CalcSignature();

	// PURPOSE: String hash
	class StringKey 
	{
	public:

		// PURPOSE: Default constructor
		StringKey();

		// PURPOSE: Copy constructor
		StringKey(const StringKey&);

		// PURPOSE: Resource constructor
		StringKey(datResource&);

		// PURPOSE: Resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		// PURPOSE: Placement
		DECLARE_PLACE(StringKey);

		// PURPOSE: Destructor
		~StringKey();

		// PURPOSE: Shutdown
		void Shutdown();

#if CR_DEV
		// PURPOSE: Serialization
		void Serialize(datSerialize&);
#endif // CR_DEV

		// PURPOSE: Get string (only available in tools, not in runtime resources)
		// RETURNS: string - if available (or NULL)
		// NOTES: Strings are discarded at resource time to save space, only hashes are retained
		const char* GetString() const;

		// PURPOSE: Set string (only available in tools, not in runtime resources)
		// PARAM: str - new string
		// NOTES: Strings are discarded at resource time to save space, only hashes are retained
		void SetString(const char* str);

		// PURPOSE: Get string key
		// RETURNS: key - case insensitive, 0 returned for NULL strings
		u32 GetKey() const;

		// PURPOSE: Calculate key, given string
		// RETURNS: key - case insensitive, 0 returned for NULL strings
		static u32 CalcKey(const char* str);

		// PURPOSE: Assignment operator
		StringKey& operator=(const StringKey&);

	private:

		datRef<char> m_String;
		u32 m_Key;
	};

protected:

	// PURPOSE: Internal function, copy property
	void Copy(const crProperty& other);

private:

	StringKey m_Name;

	atArray<datOwner<crPropertyAttribute> > m_Attributes;

	datRef<crProperties> m_Properties;

	u32 m_Signature;

	PAD_FOR_GCC_X64_4;
	
	static bool sm_InitClassCalled;

	friend class crProperties;
};

////////////////////////////////////////////////////////////////////////////////

inline crProperty::Key crProperty::CalcKey(const char* name)
{
	return Key(StringKey::CalcKey(name));
}

////////////////////////////////////////////////////////////////////////////////

inline crProperty::Key crProperty::CalcKey(const char* ASSERT_ONLY(name), u32 hash)
{
	FastAssert(StringKey::CalcKey(name) == hash);
	return Key(hash);
}

////////////////////////////////////////////////////////////////////////////////

inline const char* crProperty::GetName() const
{
	return m_Name.GetString();
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crProperty::GetKey() const
{
	return m_Name.GetKey();
}

////////////////////////////////////////////////////////////////////////////////

inline int crProperty::GetNumAttributes() const
{
	return m_Attributes.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline const crPropertyAttribute* crProperty::GetAttribute(const char* name) const
{
	return GetAttribute(StringKey::CalcKey(name));
}

////////////////////////////////////////////////////////////////////////////////

inline crPropertyAttribute* crProperty::GetAttribute(const char* name)
{
	return GetAttribute(StringKey::CalcKey(name));
}

////////////////////////////////////////////////////////////////////////////////

inline const crPropertyAttribute* crProperty::GetAttributeByIndex(u32 idx) const
{
	return m_Attributes[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline crPropertyAttribute* crProperty::GetAttributeByIndex(u32 idx)
{
	return m_Attributes[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline bool crProperty::RemoveAttribute(const char* name)
{
	return RemoveAttribute(StringKey::CalcKey(name));
}

////////////////////////////////////////////////////////////////////////////////

inline bool crProperty::operator!=(const crProperty& other) const
{
	return !(*this == other);
}

////////////////////////////////////////////////////////////////////////////////

inline crProperty& crProperty::operator=(const crProperty& other)
{
	Copy(other);
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crProperty::GetSignature() const
{
	return m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMETADATA_PROPERTY_H
