// 
// crmetadata/tag.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tag.h"

#include "tags.h"
#include "dumpoutput.h"

#include "data/safestruct.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crTag::crTag()
: m_Start(0.f)
, m_End(0.f)
, m_Tags(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crTag::crTag(const crTag& other)
: crProperty(other)
, m_Start(other.m_Start)
, m_End(other.m_End)
, m_Tags(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crTag::crTag(datResource& rsc)
: crProperty(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crTag::DeclareStruct(datTypeStruct& s)
{
	crProperty::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(crTag, crProperty)
	SSTRUCT_FIELD(crTag, m_Start)
	SSTRUCT_FIELD(crTag, m_End)
	SSTRUCT_FIELD(crTag, m_Tags)
	SSTRUCT_END(crTag)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crTag);

////////////////////////////////////////////////////////////////////////////////

crTag* crTag::Clone() const
{
	return rage_new crTag(*this);
}

////////////////////////////////////////////////////////////////////////////////

crTag::~crTag()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crTag::Shutdown()
{
	m_Start = m_End = 0.f;
	m_Tags = NULL;

	crProperty::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
int tagVersions[] = 
{
	1, // JM 07-APR-10 - new tag structure
	0,
};

////////////////////////////////////////////////////////////////////////////////

void crTag::Serialize(datSerialize& s)
{
	const int latestVersion = tagVersions[0];

	int version = latestVersion;
	s << datLabel("TagVersion:") << version << datNewLine;

	if(version > latestVersion)
	{
		Errorf("crTag::Serialize - attempting to load version '%d' (only support up to '%d')", version, latestVersion);
		return;
	}

	s << m_Start;
	s << m_End;
	 
	crProperty::Serialize(s);
}

////////////////////////////////////////////////////////////////////////////////

void crTag::Dump(crDumpOutput& output) const
{
	output.Outputf(1, "tagstart", "%f", m_Start);
	output.Outputf(1, "tagend", "%f", m_End);

	crProperty::Dump(output);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crTag::SetStart(float startPhase)
{
	m_Start = startPhase;

	if(m_Tags)
	{
		m_Tags->SortTags();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crTag::SetEnd(float endPhase)
{
	m_End = endPhase;

	if(m_Tags)
	{
		m_Tags->SortTags();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crTag::SetStartEnd(float startPhase, float endPhase)
{
	m_Start = startPhase;
	m_End = endPhase;

	if(m_Tags)
	{
		m_Tags->SortTags();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crTag::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		crProperty::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crTag::Translate(float translatePhase)
{
	if(!IsNearZero(translatePhase))
	{
		float startPhase = m_Start + translatePhase;
		float endPhase = m_End + translatePhase;

		if(startPhase < 0.f || startPhase > 1.f)
		{
			startPhase -= floorf(startPhase);
		}
		if(endPhase < 0.f || endPhase > 1.f)
		{
			endPhase -= floorf(endPhase);
		}

		SetStartEnd(startPhase, endPhase);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crTag::ShutdownClass()
{
	if(sm_InitClassCalled)
	{
		sm_InitClassCalled = false;

		crProperty::ShutdownClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crTag::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
