// 
// crmetadata/dumpoutput.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CRMETADATA_DUMPOUTPUT_H 
#define CRMETADATA_DUMPOUTPUT_H 


#include "atl/string.h"
#include "data/growbuffer.h"
#include "cranimation/animation_config.h"

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

class fiStream;

#if CR_DEV
// PURPOSE: Dump output base class
class crDumpOutput
{
public:
	
	// PURPOSE: Constructor
	crDumpOutput();

	// PURPOSE: Destructor
	virtual ~crDumpOutput();


	// PURPOSE: Output text (will be properly formatted and indented automatically)
	// PARAM: 
	// verbosity - verbosity level [0..3]
	// field - describe field 
	// fmt - variable argument format
	void Outputf(int verbosity, const char* field, const char* fmt, ...);

	// PURPOSE: Output element text (will be properly formatted and indented automatically)
	// PARAM: 
	// verbosity - verbosity level [0..3]
	// field - describe field 
	// element - element index
	// fmt - variable argument format
	void Outputf(int verbosity, const char* field, int element, const char* fmt, ...);

	// PURPOSE: Output sub-element text (will be properly formatted and indented automatically)
	// PARAM: 
	// verbosity - verbosity level [0..3]
	// field - describe field 
	// element, subelement - element and subelement index
	// fmt - variable argument format
	void Outputf(int verbosity, const char* field, int element, int subelement, const char* fmt, ...);


	// PURPOSE: Set verbosity
	// PARAM: verbosity - verbosity level [0..3]
	void SetVerbosity(int verbosity);

	// PURPOSE: Get verbosity
	// RETURNS: verbosity level [0..3]
	int GetVerbosity() const;


	// PURPOSE: Push a new level
	// PARAMS: name - optional level name
	virtual void PushLevel(const char* name);

	// PURPOSE: Pop a level
	virtual void PopLevel();

protected:

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, const char* fmt, va_list& args) = 0;

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, const char* fmt, va_list& args) = 0;

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, int subelement, const char* fmt, va_list& args) = 0;

protected:

	int m_Verbosity;
	int m_Level;
	bool m_First;

	friend class crmtDumperOutput;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Dump output to string
class crDumpOutputStdIo : public crDumpOutput
{
public:

	// PURPOSE: Constructor
	crDumpOutputStdIo();

	// PURPOSE: Destructor
	virtual ~crDumpOutputStdIo();

protected:

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, const char* fmt, va_list& args);

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, const char* fmt, va_list& args);

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, int subelement, const char* fmt, va_list& args);

private:

	// PURPOSE: Internal text output handler
	void PrivateOutputf(const char* field, const char *fmt, va_list& args);
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Dump output to string
class crDumpOutputString : public crDumpOutput
{
public:

	// PURPOSE: Constructor
	crDumpOutputString();

	// PURPOSE: Destructor
	virtual ~crDumpOutputString();

	// PURPOSE: Get dump output string
	const atString& GetString() const;

	// PURPOSE: Print string to standard output
	void PrintString() const;

protected:

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, const char* fmt, va_list& args);

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, const char* fmt, va_list& args);

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, int subelement, const char* fmt, va_list& args);

private:

	// PURPOSE: Internal text output handler
	void PrivateOutputf(const char* field, const char *fmt, va_list& args);

	atString m_String;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Dump output to a grow buffer
class crDumpOutputGrowBuffer : public crDumpOutput
{
public:

	// PURPOSE: Constructor
	crDumpOutputGrowBuffer();

	// PURPOSE: Destructor
	virtual ~crDumpOutputGrowBuffer();

	// PURPOSE: Get dump output string
	const char* GetBuffer() const;

	// PURPOSE: Get dump output string length
	unsigned GetLength() const;

	// PURPOSE: Print string to standard output
	void PrintBuffer() const;

protected:

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, const char* fmt, va_list& args);

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, const char* fmt, va_list& args);

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, int subelement, const char* fmt, va_list& args);

private:

	// PURPOSE: Internal text output handler
	void PrivateOutputf(const char* field, const char *fmt, va_list& args);

	datGrowBuffer m_Buffer;
	fiStream* m_Stream;
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Dump output to string
class crDumpOutputXml : public crDumpOutput
{
public:

	// PURPOSE: Constructor
	crDumpOutputXml();

	// PURPOSE: Destructor
	virtual ~crDumpOutputXml();

	// PURPOSE: Push level override
	virtual void PushLevel(const char* name);

	// PURPOSE: Pop level override
	virtual void PopLevel();

protected:

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, const char* fmt, va_list& args);

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, const char* fmt, va_list& args);

	// PURPOSE: Internal output function, override and implement in derived version
	virtual void InternalOutputf(int verbosity, const char* field, int element, int subelement, const char* fmt, va_list& args);

private:

	// PURPOSE: Internal text output handler
	void PrivateOutputf(const char* field, const char *fmt, va_list& args);

	atArray<atString> m_Levels;
};
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CRMETADATA_DUMPOUTPUT_H 
