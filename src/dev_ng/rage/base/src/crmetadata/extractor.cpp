// 
// crmetadata/extractor.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "extractor.h"

#include "file/serialize.h"

#if CR_DEV

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crMetadataExtractor::crMetadataExtractor()
: m_Padding(NULL)
, m_SizeBegin(0)
, m_SizeEnd(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crMetadataExtractor::~crMetadataExtractor()
{
	delete m_Padding;
}

////////////////////////////////////////////////////////////////////////////////

void crMetadataExtractor::Add(const Functor& functor, const char* markerBegin, const char* markerEnd)
{
	Callback& callback = m_Callbacks.Grow();
	callback.m_Functor = functor;
	callback.m_MarkerBegin = markerBegin;
	callback.m_MarkerEnd = markerEnd;
}

////////////////////////////////////////////////////////////////////////////////

bool crMetadataExtractor::Load(const char* filename)
{
	fiStream* f = fiStream::Open(filename);
	if(!f)
	{
		Errorf("crProperties - failed to open file '%s' for reading", filename);
		return false;
	}

	int magic;
	f->ReadInt(&magic, 1);

	// load metadata
	fiSerialize ser(f, true, true);
	for(int i=0; i < m_Callbacks.GetCount(); ++i)
	{
		Callback& callback = m_Callbacks[i];
		u32 beginLength = (u32) strlen(callback.m_MarkerBegin);
		u32 endLength = (u32) strlen(callback.m_MarkerEnd);

		if(!FindMarker(f, callback.m_MarkerBegin, beginLength))
		{
			return false;
		}
		callback.m_SeekBegin = f->Tell() - beginLength;

		callback.m_Functor(ser);

		if(!FindMarker(f, callback.m_MarkerEnd, endLength))
		{
			return false;
		}
		callback.m_SeekEnd = f->Tell();
	}

	// read begin and end paddings
	if(m_Callbacks.GetCount() != 0)
	{
		Callback& first = m_Callbacks[0];
		Callback& last = m_Callbacks[m_Callbacks.GetCount()-1];

		delete m_Padding;
		m_SizeBegin = first.m_SeekBegin;
		m_SizeEnd = f->Size() - last.m_SeekEnd;
		m_Padding = rage_new u8[m_SizeBegin + m_SizeEnd];

		f->Seek(0);
		f->Read(m_Padding, m_SizeBegin);
		f->Seek(last.m_SeekEnd);
		f->Read(&m_Padding[m_SizeBegin], m_SizeEnd);
	}

	f->Close();
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crMetadataExtractor::Save(const char* filename)
{
	fiStream* f = fiStream::Create(filename);
	if(!f)
	{
		Errorf("crMetadataExtractor - failed to open file '%s' for writing", filename);
		return false;
	}

	// write begin padding
	fiSerialize ser(f, false, true);
	if(m_Padding)
	{
		f->Seek(0);
		f->Write(m_Padding, m_SizeBegin);
	}

	// write metadata
	for(int i=0; i < m_Callbacks.GetCount(); ++i)
	{
		Callback& callback = m_Callbacks[i];
		
		f->Write(callback.m_MarkerBegin, (int) strlen(callback.m_MarkerBegin));

		callback.m_Functor(ser);

		f->Write(callback.m_MarkerEnd, (int) strlen(callback.m_MarkerEnd));
	}

	// write end padding
	if(m_Padding)
	{
		f->Write(&m_Padding[m_SizeBegin], m_SizeEnd);
	}

	f->Close();
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crMetadataExtractor::FindMarker(fiStream* f, const char* marker, u32 size)
{
	u32 idx = 0;
	u8 ch;
	while(f->Read(&ch,1) !=0 )
	{
		if(ch == marker[idx])
		{
			++idx;
			if(idx == size)
			{
				return true;
			}
		}
		else
		{
			idx = ch == marker[0] ? 1 : 0;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
