// 
// parser/membermapinterface.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBERMAPINTERFACE_H
#define PARSER_MEMBERMAPINTERFACE_H

#include "psofaketypes.h"

#include "atl/array.h"
#include "atl/binmap.h"
#include "atl/map.h"

namespace rage {
	class parMember;

//////////////////////////////////////////////////////////////////////////
class parMemberMapInterface
{
public:
	        parMemberMapInterface    () {}
    virtual ~parMemberMapInterface   () {}

    // PURPOSE: A function that pre-allocates the storage for the elements contained in the map.
    // PARAMS:
    //		size - The number of elements the map will contain.
    virtual void Reserve(int size)=0;

    // PURPOSE: A function that inserts a key into a map then returns the address to the data element to be filled in
    // PARAMS:
    //		keyValue - The address of the key to insert into the map. (NOTE: will be cast to _KeyType in function)
    virtual parPtrToStructure InsertKey(void* keyValue)=0;

    // PURPOSE: A function that allows operations to be performed after all things have been inserted into the map
    //              (i.e. sorting)
    virtual void  PostInsert() {}

    // PURPOSE: A function that resets a Map
    virtual void Reset()=0;

    // PURPOSE: A function that returns a pointer to the data 
    //              using the key provided to look it up
    // PARAMS:
    //		keyValue - The address of the key to insert into the map. (NOTE: will be cast to _KeyType in function)
    virtual parPtrToStructure GetDataPtr (void *keyValue)=0;

    // PURPOSE: A function that removed a key/value pair from the map returns true if deleted else false
    // PARAMS:
    //		keyValue - The address of the key to insert into the map. (NOTE: will be cast to _KeyType in function)
    //      dataNeedsDelete - if the data in the map should be deleted
    virtual bool Remove (void *keyValue, bool dataNeedsDelete)=0;
};

//////////////////////////////////////////////////////////////////////////
// Base implementation for atMap types
template<typename _KeyType, typename _DataType>
class parMemberMapInterface_atMapBase : public parMemberMapInterface
{
public:
    typedef atMap<_KeyType, _DataType> MapType;

            parMemberMapInterface_atMapBase    (MapType & map);
    virtual ~parMemberMapInterface_atMapBase   (){}

    virtual void    Reserve     (int size);
    virtual parPtrToStructure   InsertKey   (void* keyValue);
    virtual void    Reset       ();
    virtual parPtrToStructure   GetDataPtr  (void *keyValue);

protected:
    MapType*m_Map;
};

//////////////////////////////////////////////////////////////////////////
// Specialization for non-pointer data
template<typename _KeyType, typename _DataType>
class parMemberMapInterface_atMap : public parMemberMapInterface_atMapBase<_KeyType, _DataType>
{
public:
	typedef atMap<_KeyType, _DataType> MapType;

	parMemberMapInterface_atMap    (MapType & map) : parMemberMapInterface_atMapBase<_KeyType, _DataType>(map) {}
	virtual ~parMemberMapInterface_atMap   (){}

	virtual bool    Remove      (void *keyValue, bool dataNeedsDelete);
};

//////////////////////////////////////////////////////////////////////////
// Specialization for pointer data
template<typename _KeyType, typename _DataType>
class parMemberMapInterface_atMap<_KeyType, _DataType*> : public parMemberMapInterface_atMapBase<_KeyType, _DataType*>
{
public:
	typedef atMap<_KeyType, _DataType*> MapType;

	parMemberMapInterface_atMap    (MapType & map) : parMemberMapInterface_atMapBase<_KeyType, _DataType*>(map) {}
	virtual ~parMemberMapInterface_atMap   (){}

	virtual bool    Remove      (void *keyValue, bool dataNeedsDelete);
};


//////////////////////////////////////////////////////////////////////////
// Base implementation for atBinaryMap types
class parMemberAtBinaryMapGenericInterface : public parMemberMapInterface
{
public:
	parMemberAtBinaryMapGenericInterface(parMemberMap& member, void* instanceData);
	
	char*	GetArrayElement(int i);
    virtual void    Reserve     (int size);
    virtual parPtrToStructure   InsertKey   (void* keyValue);
    virtual void    PostInsert  ();
    virtual void    Reset       ();
    virtual parPtrToStructure   GetDataPtr  (void *keyValue);
	virtual bool    Remove      (void *keyValue, bool dataNeedsDelete);

	size_t GetArrayElementSize() { return m_ElementSize; }
	size_t GetCount() { return m_MapData->m_Data.m_Count; }

protected:
	parFake::AtBinMap* m_MapData;
	parMember*		m_KeyMember;
	parMember*		m_DataMember;
	size_t			m_KeySize;
	size_t			m_DataOffset;  // keysize + padding
	size_t			m_DataSize;
	size_t			m_ElementSize; // keysize + padding + datasize + padding
	int (*m_ComparisonFunction)(const void*, const void*); // Used for qsort and bsearch calls
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// parMemberMapInterface_atMap ///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename _KeyType, typename _DataType>
parMemberMapInterface_atMapBase<_KeyType,_DataType>::parMemberMapInterface_atMapBase (MapType &map) 
{
    m_Map = &map;
}

template<typename _KeyType, typename _DataType>
void parMemberMapInterface_atMapBase<_KeyType,_DataType>::Reserve(int /*size*/)
{
}

template<typename _KeyType, typename _DataType>
parPtrToStructure parMemberMapInterface_atMapBase<_KeyType,_DataType>::InsertKey(void* keyValue)
{
    parAssertf(m_Map, "InsertKey atMap is invalid.");
    parAssertf(keyValue, "InsertKey keyValue is invalid param.");

    _KeyType key = *(_KeyType*)keyValue;
    m_Map->Insert(key);
    return reinterpret_cast<parPtrToStructure>(m_Map->Access(key));// This is really a pointer-to-member, but I know the metadata for the member has offset=0, so it's safe to cast to pointer-to-structure
}

template<typename _KeyType, typename _DataType>
void parMemberMapInterface_atMapBase<_KeyType,_DataType>::Reset()
{
    parAssertf(m_Map, "Reset atMap is invalid.");
    m_Map->Reset();
}

template<typename _KeyType, typename _DataType>
parPtrToStructure parMemberMapInterface_atMapBase<_KeyType,_DataType>::GetDataPtr(void* keyValue)
{
    parAssertf(m_Map, "GetDataPtr atMap is invalid.");
    parAssertf(keyValue, "GetDataPtr keyValue is invalid param.");

    _KeyType key = *(_KeyType*)keyValue;
    return reinterpret_cast<parPtrToStructure>(m_Map->Access(key)); // This is really a pointer-to-member, but I know the metadata for the member has offset=0, so it's safe to cast to pointer-to-structure
}

// Specialized version of Remove for non-pointer types - doesn't try to delete the data
template<typename _KeyType, typename _DataType>
bool parMemberMapInterface_atMap<_KeyType,_DataType>::Remove (void *keyValue, bool /*dataNeedsDelete*/)
{
    parAssertf(this->m_Map, "Remove atMap is invalid.");
    parAssertf(keyValue, "Remove keyValue is invalid param.");
    _KeyType key = *(_KeyType*)keyValue;
    _DataType* data = (_DataType*)this->m_Map->Access(key);
    if (!data)
    {
        return false;
    }

    this->m_Map->Delete(key);

    return true;
}

// Specialized version of Remove for pointer types - may delete the data
template<typename _KeyType, typename _DataType>
bool parMemberMapInterface_atMap<_KeyType,_DataType*>::Remove (void *keyValue, bool dataNeedsDelete)
{
	parAssertf(this->m_Map, "Remove atMap is invalid.");
	parAssertf(keyValue, "Remove keyValue is invalid param.");
	_KeyType key = *(_KeyType*)keyValue;
	_DataType** data = (_DataType**)this->m_Map->Access(key);
	if (!data)
	{
		return false;
	}

	if (dataNeedsDelete)
	{
		delete (*data);
	}
	this->m_Map->Delete(key);

	return true;
}

}

#endif // PARSER_MEMBERMAPINTERFACE_H
