// 
// parser/membervectordata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MEMBERVECTORDATA_H 
#define PARSER_MEMBERVECTORDATA_H 

#include "memberdata.h"

#include "parser/macros.h"
#include "data/callback.h"

namespace rage {


namespace parMemberVectorSubType
{
	enum Enum
	{
		SUBTYPE_NORMAL,
		SUBTYPE_COLOR,
	};
}

// PURPOSE: All of the data that a parMemberSimple needs to describe the member.
struct parMemberVectorData {
	enum TypeFlags
	{
		FLAG_HIGH_PRECISION
	};

	STANDARD_PARMEMBER_DATA_CONTENTS;
	float m_InitX, m_InitY, m_InitZ, m_InitW; // Initial values. (not a Vector4 for alignment reasons)
#if __BANK
	const char* m_Description;		// Text description of the member. Used for tooltips in the bank
	float m_Min;					// Minimum value of the member. For vectors, the minimum for all components.
	float m_Max;					// Maximum value of the member. For vectors, the maximum for all components.
	float m_Step;					// The smallest delta when manipulating the widgets.
	datCallback* m_WidgetCb;
#endif
	void Init();
	void PreLoad(parTreeNode*) {Init();}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_SIMPLE_PARSABLE;
#endif
};

} // namespace rage

#endif // PARSER_MEMBERVECTORDATA_H 
