// 
// parser/memberstruct.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBERSTRUCT_H
#define PARSER_MEMBERSTRUCT_H

#include "member.h"

#include "memberstructdata.h"

namespace rage {

class parStructure;

// PURPOSE: A parMember subclass for describing structures, including both nested structures and owning pointers to base classes.
class parMemberStruct : public parMember
{
	friend class parInstanceVisitor;
	friend class parMemberArray;

public:
	typedef parMemberStructSubType::Enum SubType;
	typedef parMemberStructData Data;

	parMemberStruct() {}
	explicit parMemberStruct(Data& data) : parMember(&data) {}

	virtual void ReadTreeNode(parTreeNode* node, parPtrToStructure data) const;

	// PURPOSE: If this is metadata for a pointer, creates a new instance of the pointed-to object. 
	//   (using the type information either from 'node' or from 'typeHash'). Pointer or struct though,
	//   the return value will be the address of the sub-structure.
	// PARAMS:
	//		node - a tree node containing the type information for the new structure (in the form of a type="" attribute). Pass NULL to use the typeHash instead
	//		typeHash - the atLiteralStringHash of the type name for the created structure - ignored if node is non-NULL.
	//		outCreatedStruct - the parStructure description of the type of object that was created
	parPtrToStructure CreateNewData(parTreeNode* node, u32 typeHash, parStructure** outCreatedStruct = NULL) const;

	// RETURNS: True if this is a pointer which can point to a derived type
	bool CanDerive() const {return GetTrait(GetSubtype(), TRAIT_CAN_DERIVE);}

	// RETURNS: True if this structure/pointer must refer to parsable data
	bool IsParsableData() const {return GetTrait(GetSubtype(), TRAIT_PARSABLE_DATA);}

	// RETURNS: True if this pointer is one of the external pointer types
	bool IsExternalPointer() const {return GetTrait(GetSubtype(), TRAIT_EXTERNAL_POINTER);}

	// RETURNS: True if this pointer is a "smart pointer", and can't be represented with a _Type* C-style pointer
	bool IsSmartPointer() const {return GetTrait(GetSubtype(), TRAIT_SMART_POINTER);}

	// RETURNS: True if this pointer/structure owns the data it refers to
	bool IsOwner() const {return GetTrait(GetSubtype(), TRAIT_OWNER); }

	// RETURNS: True if this POINTER owns the data it refers to
	bool IsOwnerPointer() const {return GetTrait(GetSubtype(), TRAIT_OWNER_POINTER); }

	// RETURNS: The declared type of data that this pointer/structure contains
	parStructure* GetBaseStructure() const;

	// RETURNS: The actual type of data that an instance of this pointer/structure contains
	// PARAMS: structureAddress - the address of the structure you are trying to find the type of
	// NOT the address of the structure that contains this member variable!
	parStructure* GetConcreteStructure(parConstPtrToStructure structureAddress) const;

	// PURPOSE:
	//	Sets the value of the pointer using a name.
	// NOTES:
	//	Only works for external_named pointers
	void SetFromString(parPtrToStructure containingStructureAddr, const char* name) const;

	virtual size_t GetSize() const;
	virtual size_t FindAlign() const;

	parPtrToStructure GetObjAddr(parPtrToStructure containingStructureAddr) const;
	parConstPtrToStructure GetObjAddr(parConstPtrToStructure containingStructureAddr) const;

	// PURPOSE: 
	//  Sets the value of a pointer, for owning pointers only. Doesn't do any memory management.
	void SetObjAddr(parPtrToStructure containingStructureAddr, parPtrToStructure newPtrValue) const;

public:
	// Various traits for each of the subtypes. For descriptions see the 
	enum SubTypeTrait
	{
		TRAIT_CAN_DERIVE,
		TRAIT_PARSABLE_DATA,
		TRAIT_EXTERNAL_POINTER,
		TRAIT_SMART_POINTER,
		TRAIT_OWNER,
		TRAIT_OWNER_POINTER,
	};

	static bool GetTrait(SubType subtype, SubTypeTrait trait);


	STANDARD_PARMEMBER_DATA_FUNCS_DECL(parMemberStruct);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_PARSABLE;
#endif
};

// This can't be defined until the derived class is.
inline parMemberStruct*	parMember::AsStructOrPointer()	{ return parMemberType::IsStructOrPointer(GetType()) ? smart_cast<parMemberStruct*>(this) : NULL; }

}

#endif
