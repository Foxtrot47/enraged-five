// 
// parser/membersimple.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBERSIMPLE_H
#define PARSER_MEMBERSIMPLE_H

#include "member.h"

#include "membersimpledata.h"

#include "data/callback.h"

namespace rage {

// PURPOSE: A parMember subclass for describing simple data types like int, float, bool, etc.
class parMemberSimple : public parMember
{
public:
	typedef parMemberSimpleSubType::Enum SubType;
	typedef parMemberSimpleData Data;

	parMemberSimple() {}
	explicit parMemberSimple(Data& data) : parMember(&data) {}
	virtual ~parMemberSimple();

	virtual void ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const;

	void SetFromString(parPtrToStructure containingStructureAddr, const char* name) const;

	virtual size_t GetSize() const;

public:

	STANDARD_PARMEMBER_DATA_FUNCS_DECL(parMemberSimple);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_PARSABLE;
#endif
};

// This can't be defined until the derived class is.
inline parMemberSimple*	parMember::AsSimple()		{ return parMemberType::IsSimple(GetType()) ? smart_cast<parMemberSimple*>(this) : NULL; }

}

#endif
