// 
// parser/psorscbuilder.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "psorscbuilder.h"

#include "manager.h"
#include "memberdata.h"
#include "optimisations.h"
#include "psobyteswap.h"
#include "psoconsts.h"
#include "psodata.h"
#include "psofile.h"
#include "psoschema.h"

#include "atl/array.h"
#include "atl/map.h"
#include "data/aes.h"
#include "diag/output.h"
#include "file/asset.h"
#include "math/simplemath.h"
#include "paging/rscbuilder.h"
#include "system/endian.h"
#include "system/memops.h"

PARSER_OPTIMISATIONS();

const int STRUCTARRAY_SPLIT_SIZE = 16 * 1024;

using namespace rage;


psoRscBuilderStructSchema::psoRscBuilderStructSchema(psoRscBuilder* builder)
: m_Builder(builder)
, m_UnderConstruction(true)
, m_AutoPack(false)
{
}

psoRscBuilderStructSchema::~psoRscBuilderStructSchema()
{
}

void psoRscBuilderStructSchema::ComputeOffsetAndUpdateSize(psoRscMemberSchemaData& mem, ptrdiff_t offset)
{
	if (psoConstants::IsReserved(mem.m_NameHash))
	{
		return;
	}

	FastAssert((m_AutoPack && offset < 0) ||
		(!m_AutoPack && offset >= 0)); // If you choose to use autopack, ALL members have to be autopacked.

	size_t size = 0;
	size_t align = 0;
	FindMemberTraits(mem, size, align);

	if (m_AutoPack)
	{
		// Round up to alignment
		offset = AlignPow2(m_Structure.m_Size, align);
		mem.m_Offset = (u32)offset;
		m_Structure.m_Size = ptrdiff_t_to_int(offset + size);
	}
	else
	{
		mem.m_Offset = (u32)offset;
	}

	m_Structure.m_AlignPower = Max(m_Structure.m_AlignPower, psoRscMemberSchemaData::FindAlignPower(align));

	FastAssert(offset >= 0 && 
		offset < psoSchemaMemberData::MAX_OFFSET && 
		((u32)offset < m_Structure.m_Size || (u32)offset == m_Structure.m_Size && size == 0));  // special case for 0-byte structures at the end of a class

	return;
}

ptrdiff_t psoRscBuilderStructSchema::AddPadding(size_t pad, size_t align)
{
	FastAssert(m_AutoPack);

	ptrdiff_t offset = AlignPow2(m_Structure.m_Size, align);
	m_Structure.m_Size += ptrdiff_t_to_int(offset + pad);

	return offset;
}

void psoRscBuilderStructSchema::FindMemberTraits(const psoRscMemberSchemaData& mem, size_t& size, size_t& align)
{
	psoType type = mem.GetType();
	size = type.GetSize();
	align = type.GetAlign();

	if (size != 0)
	{
		return;
	}

	// This code is so similar to psoSchemaCatalog::FindMemberSize, but they find the referenced data in different ways. 
	// Would be nice if they could share code though.
	switch(type.GetEnum())
	{
	case psoType::TYPE_STRUCT:
		{
			psoRscBuilderStructSchema* subStruct = m_Builder->FindStructSchema(atLiteralHashValue(mem.EnumOrStruct.m_ReferentHash));
			if (parVerifyf(subStruct, "Couldn't find struct for hash 0x%08x", mem.EnumOrStruct.m_ReferentHash))
			{
				size = subStruct->m_Structure.m_Size;
				align = (size_t)(1U << subStruct->m_Structure.m_AlignPower);
				return;
			}
		}
		break;
	case psoType::TYPE_STRING_MEMBER:
		align = sizeof(char);
		size = sizeof(char) * mem.String.m_Count;
		return;
	case psoType::TYPE_WIDE_STRING_MEMBER:
		align = sizeof(char16);
		size = sizeof(char16) * mem.String.m_Count;
		return;
	case psoType::TYPE_ARRAY_MEMBER:
		{
			psoRscMemberSchemaData& eltMem = m_Members[mem.Array.m_ContentsIndex];
			FindMemberTraits(eltMem, size, align);
			size *= mem.Array.m_Count;
		}
		return;
	case psoType::TYPE_ATFIXEDARRAY:
		{
			psoRscMemberSchemaData& eltMem = m_Members[mem.Array.m_ContentsIndex];
			FindMemberTraits(eltMem, size, align);
			size *= mem.Array.m_Count;
			align = Max(align, __alignof(int));
		}
		return;
	case psoType::TYPE_BITSET8:
		size = RoundUp<8>(mem.Bitset.m_BitCount)/8;
		align = __alignof(u8);
		return;
	case psoType::TYPE_BITSET16:
		size = RoundUp<16>(mem.Bitset.m_BitCount)/8;
		align = __alignof(u16);
		return;
	case psoType::TYPE_BITSET32:
		size = RoundUp<32>(mem.Bitset.m_BitCount)/8;
		align = __alignof(u32);
		return;
	default:
		type.PrintUnexpectedTypeError("Finding size for member", "type with unknown size");
	}
	size = 0;
	align = 0;
}

psoRscMemberSchemaData& psoRscBuilderStructSchema::AddMember(atLiteralHashValue namehash, psoType type, ptrdiff_t /*offset*/)
{
	FastAssert(m_UnderConstruction);
	psoRscMemberSchemaData& mem = m_Members.Grow();
	mem.m_NameHash = namehash;
	mem.Generic.m_Type = (u8)type.GetEnum();
	return mem;
}

int psoRscBuilderStructSchema::AddMemberGeneric(atLiteralHashValue namehash, psoType type, ptrdiff_t offset /* = -1 */)
{
	parAssertf(psoRscMemberSchemaData::GetMemberDataType(type) == psoRscMemberSchemaData::MEM_GENERIC, "Invalid type %d %s", type.GetEnum(), type.GetName());

	psoRscMemberSchemaData& mem = AddMember(namehash, type, offset);
	ComputeOffsetAndUpdateSize(mem, offset);
	return m_Members.GetCount()-1;
}


int psoRscBuilderStructSchema::AddMemberEnum( atLiteralHashValue namehash, psoType type, atLiteralHashValue enumName, ptrdiff_t offset /*= -1*/ )
{
	parAssertf(type.IsEnum(), "Invalid type %d %s", type.GetEnum(), type.GetName());

	psoRscMemberSchemaData& mem = AddMember(namehash, type, offset);
	mem.EnumOrStruct.m_ReferentHash = enumName.GetHash();
	ComputeOffsetAndUpdateSize(mem, offset);
	return m_Members.GetCount()-1;
}

int psoRscBuilderStructSchema::AddMemberStruct( atLiteralHashValue namehash, psoType type, atLiteralHashValue innerStructName, ptrdiff_t offset /*= -1*/ )
{
	parAssertf(type == psoType::TYPE_STRUCT, "Invalid type %d %s", type.GetEnum(), type.GetName());

	psoRscMemberSchemaData& mem = AddMember(namehash, type, offset);
	mem.EnumOrStruct.m_ReferentHash = innerStructName.GetHash();
	ComputeOffsetAndUpdateSize(mem, offset);
	return m_Members.GetCount()-1;
}

int psoRscBuilderStructSchema::AddMemberString( atLiteralHashValue namehash, psoType type, u32 elementCount /*= 0*/, ptrdiff_t offset /*= -1*/, u8 namespaceIndex /* = 0*/ )
{
	parAssertf(type.IsString(), "Invalid type %d %s", type.GetEnum(), type.GetName());

	psoRscMemberSchemaData& mem = AddMember(namehash, type, offset);
	mem.String.m_Count = elementCount;
	mem.String.m_NamespaceIndex = namespaceIndex;
	ComputeOffsetAndUpdateSize(mem, offset);
	return m_Members.GetCount()-1;
}

int psoRscBuilderStructSchema::AddMemberArray( atLiteralHashValue namehash, psoType type, int indexOfArrayContentMember, size_t alignment /*= 1*/, ptrdiff_t offset /*= -1*/, ArrayLocation location /*= ARRAY_IN_VIRTUAL_MEMORY */)
{
	parAssertf(type.IsArray() && !type.IsMaxSizeArray() && !type.IsArrayWithExternalCount(), "Invalid type %d %s", type.GetEnum(), type.GetName());
	parAssertf(indexOfArrayContentMember >= 0 && indexOfArrayContentMember < m_Members.GetCount(), "Index of array content %d isn't valid", indexOfArrayContentMember);
	parAssertf(psoConstants::IsAnonymous(m_Members[indexOfArrayContentMember].m_NameHash), "Array content member must be anonymous");

	psoRscMemberSchemaData& mem = AddMember(namehash, type, offset);
	mem.Array.m_ContentsIndex = (u16)indexOfArrayContentMember;
	u8 flags = 0;
	flags |= (location == ARRAY_IN_PHYSICAL_MEMORY) ? (1 << psoRscMemberSchemaData::ARRAY_FLAG_PHYSICAL_MEMORY) : 0;
	mem.Array.SetAlignPowerAndFlags(psoRscMemberSchemaData::FindAlignPower(alignment), flags);
	ComputeOffsetAndUpdateSize(mem, offset);
	return m_Members.GetCount()-1;
}

int psoRscBuilderStructSchema::AddMemberMaxSizeArray( atLiteralHashValue namehash, psoType type, int indexOfArrayContentMember, u32 maxCount, size_t alignment /*=1*/, ptrdiff_t offset /*= -1*/, ArrayLocation location /*= ARRAY_IN_VIRTUAL_MEMORY */ )
{
	parAssertf(type.IsMaxSizeArray(), "Invalid type %d %s", type.GetEnum(), type.GetName());
	parAssertf(indexOfArrayContentMember >= 0 && indexOfArrayContentMember < m_Members.GetCount(), "Index of array content %d isn't valid", indexOfArrayContentMember);
	parAssertf(psoConstants::IsAnonymous(m_Members[indexOfArrayContentMember].m_NameHash), "Array content member must be anonymous");

	psoRscMemberSchemaData& mem = AddMember(namehash, type, offset);
	mem.Array.m_ContentsIndex = (u16)indexOfArrayContentMember;
	u8 flags = 0;
	flags |= (location == ARRAY_IN_PHYSICAL_MEMORY) ? (1 << psoRscMemberSchemaData::ARRAY_FLAG_PHYSICAL_MEMORY) : 0;
	mem.Array.SetAlignPowerAndFlags(psoRscMemberSchemaData::FindAlignPower(alignment), flags);
	mem.Array.m_Count = maxCount;
	ComputeOffsetAndUpdateSize(mem, offset);
	return m_Members.GetCount()-1;
}

int psoRscBuilderStructSchema::AddMemberArrayWithCounter( atLiteralHashValue namehash, psoType type, int indexOfArrayContentMember, size_t offsetOfCounterMember, size_t alignment /*=1*/, ptrdiff_t offset /*= -1*/, ArrayLocation location /*= ARRAY_IN_VIRTUAL_MEMORY */ )
{
	parAssertf(type.IsArrayWithExternalCount(), "Invalid type %d %s", type.GetEnum(), type.GetName());
	parAssertf(indexOfArrayContentMember >= 0 && indexOfArrayContentMember < m_Members.GetCount(), "Index of array content %d isn't valid", indexOfArrayContentMember);
	parAssertf(psoConstants::IsAnonymous(m_Members[indexOfArrayContentMember].m_NameHash), "Array content member must be anonymous");

	psoRscMemberSchemaData& mem = AddMember(namehash, type, offset);
	mem.Array.m_ContentsIndex = (u16)indexOfArrayContentMember;
	u8 flags = 0;
	flags |= (location == ARRAY_IN_PHYSICAL_MEMORY) ? (1 << psoRscMemberSchemaData::ARRAY_FLAG_PHYSICAL_MEMORY) : 0;
	mem.Array.SetAlignPowerAndFlags(psoRscMemberSchemaData::FindAlignPower(alignment), flags);
	mem.Array.m_CounterOffsetForConstructionOnly = (u32)offsetOfCounterMember;
	ComputeOffsetAndUpdateSize(mem, offset);
	return m_Members.GetCount()-1;
}

int psoRscBuilderStructSchema::AddMemberBitset( atLiteralHashValue namehash, psoType type, atLiteralHashValue enumHash, u16 numBits, ptrdiff_t offset /*= -1*/ )
{
	parAssertf(type.IsBitset(), "Invalid type %d %s", type.GetEnum(), type.GetName());

	psoRscMemberSchemaData& mem = AddMember(namehash, type, offset);
	mem.Bitset.m_BitCount = numBits;
	mem.Bitset.m_EnumHash = enumHash.GetHash();
	ComputeOffsetAndUpdateSize(mem, offset);
	return m_Members.GetCount()-1;
}

int psoRscBuilderStructSchema::AddMemberMap( atLiteralHashValue namehash, psoType type, int indexOfKeyMember, int indexOfValueMember, ptrdiff_t offset /*= -1*/ )
{
	parAssertf(type.IsMap(), "Invalid type %d %s", type.GetEnum(), type.GetName());
	parAssertf(indexOfKeyMember >= 0 && indexOfKeyMember < m_Members.GetCount(), "Index of key member %d isn't valid", indexOfKeyMember);
	parAssertf(psoConstants::IsAnonymous(m_Members[indexOfKeyMember].m_NameHash), "Key member must be anonymous");
	parAssertf(indexOfValueMember >= 0 && indexOfValueMember < m_Members.GetCount(), "Index of value member %d isn't valid", indexOfValueMember);
	parAssertf(psoConstants::IsAnonymous(m_Members[indexOfValueMember].m_NameHash), "Value member must be anonymous");

	psoRscMemberSchemaData& mem = AddMember(namehash, type, offset);
	mem.Map.m_KeyIndex = (u16)indexOfKeyMember;
	mem.Map.m_ValueIndex = (u16)indexOfValueMember;
	ComputeOffsetAndUpdateSize(mem, offset);
	return m_Members.GetCount()-1;
}


int psoRscBuilderStructSchema::CopyMember(psoRscMemberSchemaData& member, atLiteralHashValue newname /* = atLiteralHashValue::Null */, ptrdiff_t offset /* = -1 */)
{
	psoRscMemberSchemaData& mem = AddMember(newname, member.GetType(), offset);

	// Re-copies the type, that's ok.
	sysMemCpy((char*)&mem.Generic, (char*)&member.Generic, sizeof(member.Generic));
	ComputeOffsetAndUpdateSize(mem, offset);

	return m_Members.GetCount()-1;
}

void psoRscBuilderStructSchema::FinishBuilding()
{
	FastAssert(m_UnderConstruction);
	m_UnderConstruction = false;

	// Check for any array-with-counter members, find the proper member indices for their counters
	for(int i = 0; i < m_Members.GetCount(); i++)
	{
		psoType type = m_Members[i].GetType();
		if (type.IsArrayWithExternalCount())
		{
			psoRscMemberSchemaData& arrayMember = m_Members[i];
			u32 offset = arrayMember.Array.m_CounterOffsetForConstructionOnly;

			bool found = false;
			for(int countIdx = 0; countIdx < m_Members.GetCount(); countIdx++)
			{
				psoRscMemberSchemaData& countMember = m_Members[countIdx];
				if (!psoConstants::IsReserved(countMember.m_NameHash) && // if its named...
					(countMember.GetType().IsArrayCounterType()) &&		// ...and of the right type...
					countMember.m_Offset == offset) {		  // ...and has the correct offset

					arrayMember.Array.m_CounterIndex = (u16)countIdx;
					found = true;
					break;
				}
			}
			if (!parVerifyf(found, "Couldn't find a named member variable with offset %d to use as an array counter for member with hash 0x%08x", offset, arrayMember.m_NameHash.GetHash()))
			{
				arrayMember.Array.m_CounterIndex = 0xFFFF;
			}
		}
	}

	// This would be the time to reorganize the members if necessary

	// Make sure the size we have is a multiple of alignment, boost the size if not
	if (m_AutoPack)
	{
		m_Structure.m_Size = (u32)AlignPow2(m_Structure.m_Size, 1 << m_Structure.m_AlignPower);
	}
	else
	{
		parAssertAligned(m_Structure.m_Size, 1 << m_Structure.m_AlignPower, "size of PSO schema structure");
	}
}

void psoRscBuilderStructSchema::CopyTo(psoRscStructSchemaData& dest)
{
	FastAssert(!m_UnderConstruction);
	FastAssert(dest.m_Members.GetPtr() == NULL);

	sysMemCpy(&dest, &m_Structure, sizeof(m_Structure));
	dest.m_NumMembers = (u16)m_Members.GetCount();
	dest.m_Members.NewArrayNoCookie(dest.m_NumMembers);
	sysMemCpy(dest.m_Members.GetPtr(), m_Members.GetElements(), sizeof(psoRscMemberSchemaData) * dest.m_NumMembers);
}

psoRscBuilderEnumSchema::psoRscBuilderEnumSchema() 
: m_UnderConstruction(true)
{
}

psoRscBuilderEnumSchema::~psoRscBuilderEnumSchema()
{
}

void psoRscBuilderEnumSchema::AddValue(atLiteralHashValue namehash, int value)
{
	psoRscEnumSchemaValueData& val = m_Values.Grow();
	val.m_NameHash = namehash;
	val.m_Value = value;
}

void psoRscBuilderEnumSchema::FinishBuilding()
{
	FastAssert(m_UnderConstruction);
	m_UnderConstruction = false;
}

void psoRscBuilderEnumSchema::CopyTo(psoRscEnumSchemaData& dest)
{
	FastAssert(!m_UnderConstruction);
	FastAssert(dest.m_Values.GetPtr() == NULL);
	sysMemCpy(&dest, &m_Enum, sizeof(m_Enum));
	dest.m_NumValues = (u16)m_Values.GetCount();
	if (dest.m_NumValues)
	{
		dest.m_Values.NewArrayNoCookie(dest.m_NumValues);
		sysMemCpy(dest.m_Values.GetPtr(), m_Values.GetElements(), sizeof(psoRscEnumSchemaValueData) * dest.m_NumValues);
	}
}

psoRscBuilderSchemaCatalog::~psoRscBuilderSchemaCatalog()
{
	for(int i = 0; i < m_StructSchemas.GetCount(); i++)
	{
		delete m_StructSchemas[i];
	}

	for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
	{
		delete m_EnumSchemas[i];
	}
}


psoRscBuilderInstance::psoRscBuilderInstance() 
	: m_Data(NULL)
	, m_Size(0)
	, m_OwnsData(false)
{}

psoRscBuilderInstance::~psoRscBuilderInstance()
{
	if (m_OwnsData)
	{
		delete [] m_Data;
	}
}


psoRscBuilderInstanceSet::~psoRscBuilderInstanceSet()
{
	for(int i = 0; i < m_Instances.GetCount(); i++)
	{
		delete m_Instances[i];
	}
}

psoRscBuilderInstanceDataCatalog::psoRscBuilderInstanceDataCatalog()
	: m_RootObject(NULL)
{}

psoRscBuilderInstanceDataCatalog::~psoRscBuilderInstanceDataCatalog()
{
	InstanceSetMap::Iterator iter = m_Instances.CreateIterator();
	while(!iter.AtEnd())
	{
		atArray<psoRscBuilderInstanceSet*>& arr = iter.GetData();
		for(int i = 0; i < arr.GetCount(); i++)
		{
			delete arr[i];
		}
		iter.Next();
	}

	for(int i = 0; i < m_NonNativeAlignedInstances.GetCount(); i++)
	{
		delete m_NonNativeAlignedInstances[i];
	}
}

psoRscBuilderStructSchema* psoRscBuilder::FindStructSchema(atLiteralHashValue hash)
{
	for(int i = 0; i < m_Schemas.m_StructSchemas.GetCount(); i++)
	{
		psoRscBuilderStructSchema* sch = m_Schemas.m_StructSchemas[i];
		if (sch->m_Structure.m_NameHash == hash)
		{
			return sch;
		}
	}

	return NULL;
}

psoRscBuilderEnumSchema* psoRscBuilder::FindEnumSchema(atLiteralHashValue hash)
{
	for(int i = 0; i < m_Schemas.m_EnumSchemas.GetCount(); i++)
	{
		psoRscBuilderEnumSchema* enm = m_Schemas.m_EnumSchemas[i];
		if (enm->m_Enum.m_NameHash == hash)
		{
			return enm;
		}
	}

	return NULL;
}

psoRscBuilderInstanceSet* psoRscBuilderInstanceDataCatalog::FindOrCreateInstanceSet(u32 hash, size_t nativeAlign, size_t sizeInBytes, size_t specifiedAlign, bool physicalHeap)
{
	psoRscBuilderInstanceSet* set = NULL;
	if (specifiedAlign == nativeAlign)
	{
		atArray<psoRscBuilderInstanceSet*>& setArray = m_Instances[hash];
		for(int i = 0; i < setArray.GetCount(); i++)
		{
			if (setArray[i]->CanHold(sizeInBytes) && setArray[i]->m_PhysicalHeap == physicalHeap)
			{
				set = setArray[i];
				break;
			}
		}
		if (!set)
		{
			set = rage_new psoRscBuilderInstanceSet;
			setArray.PushAndGrow(set);
		}
	}
	else
	{
		set = rage_new psoRscBuilderInstanceSet;
		m_NonNativeAlignedInstances.PushAndGrow(set);

	}
	set->m_Align = specifiedAlign; // in case we created a new set
	set->m_PhysicalHeap = physicalHeap; // ditto
	return set;
}


psoRscBuilderInstanceSet& psoRscBuilderInstanceDataCatalog::FindOrCreateInstanceSet(psoType podType, size_t sizeInBytes, size_t alignment, bool physicalHeap)
{
	psoRscBuilderInstanceSet* set = FindOrCreateInstanceSet(podType.GetEnum(), podType.GetAlign(), sizeInBytes, alignment, physicalHeap);
	set->m_PodType = podType;
	return *set;
}

psoRscBuilderInstanceSet& psoRscBuilderInstanceDataCatalog::FindOrCreateInstanceSet(psoRscBuilderStructSchema* structSchema, size_t count, size_t alignment, bool physicalHeap)
{
	size_t sizeInBytes = count * structSchema->GetSize();
	psoRscBuilderInstanceSet* set = FindOrCreateInstanceSet(structSchema->GetName().GetHash(), structSchema->GetAlign(), sizeInBytes, alignment, physicalHeap);
	set->m_StructSchema = structSchema;
	return *set;
}

psoRscBuilder::psoRscBuilder()
: m_UnderConstruction(true)
, m_IncludeChecksum(false)
, m_NextAnonId(psoConstants::FIRST_ANONYMOUS_ID)
{
}

psoRscBuilder::~psoRscBuilder()
{
}

psoRscBuilderStructSchema& psoRscBuilder::CreateStructSchema(atLiteralHashValue namehash, size_t size /* = -1 */, size_t align /* = 1 */)
{
	if (namehash.IsNull())
	{
		parAssertf(psoConstants::IsAnonymous(m_NextAnonId), "Ran out of anonymous structure IDs");
		namehash = atLiteralHashValue(m_NextAnonId);
		++m_NextAnonId;
	}

	parAssertf(!FindStructSchema(namehash), "Can't add a schema that's already been added!");
	psoRscBuilderStructSchema* sch = rage_new psoRscBuilderStructSchema(this);
	sch->m_Structure.m_NameHash = namehash;
	sch->m_Structure.m_AlignPower = psoRscMemberSchemaData::FindAlignPower(align);
	if ((int)size < 0)
	{
		sch->m_AutoPack = true;
		sch->m_Structure.m_Size = 0;
	}
	else
	{
		sch->m_Structure.m_Size = (u32)size;
	}

	return *sch;
}

psoRscBuilderEnumSchema& psoRscBuilder::CreateEnumSchema(atLiteralHashValue namehash)
{
	if (namehash.IsNull())
	{
		parAssertf(psoConstants::IsAnonymous(m_NextAnonId), "Ran out of anonymous structure IDs");
		namehash = atLiteralHashValue(m_NextAnonId);
		++m_NextAnonId;
	}

	parAssertf(!FindEnumSchema(namehash), "Can't add a schema that's already been added!");
	psoRscBuilderEnumSchema* enm = rage_new psoRscBuilderEnumSchema;

	enm->m_Enum.m_NameHash = namehash;

	return *enm;
}

// Adds a single instance (or single array of instances) to the instance data catalog
psoRscBuilderInstance& psoRscBuilder::AddStructInstances(psoRscBuilderStructSchema& schema, const char* instanceData, size_t count, size_t align, bool physicalHeap)
{
	size_t structAlign = 1U << schema.m_Structure.m_AlignPower;
	size_t realAlignment = Max(align, structAlign);
	psoRscBuilderInstanceSet& set = m_Instances.FindOrCreateInstanceSet(&schema, count, realAlignment, physicalHeap);

	psoRscBuilderInstance* newInst = rage_new psoRscBuilderInstance;
	newInst->m_Data = const_cast<char*>(instanceData);
	newInst->m_Size = count * schema.GetSize();
	newInst->m_OwnsData = false;

	Assert(newInst->m_Size > 0);

	set.AddInstance(newInst);
	return *set.m_Instances.Top();
}

// Adds a single instance (or single array of instances) to the instance data catalog
psoRscBuilderInstance& psoRscBuilder::CreateStructInstances(psoRscBuilderStructSchema& schema, size_t count, size_t align, bool physicalHeap)
{
	size_t storageSize = schema.m_Structure.m_Size * count;
	char* storage = rage_aligned_new(align) char[storageSize];

	psoRscBuilderInstance& newInst = AddStructInstances(schema, storage, count, align, physicalHeap);
	newInst.m_OwnsData = true;

	return newInst;
}

psoRscBuilderInstance& psoRscBuilder::AddPodArray(psoType type, const char* object, size_t sizeInBytes, size_t align, bool physicalHeap)
{
	parAssertf(type.IsStructArrayType(), "Type %d %s isn't a structarray type", type.GetEnum(), type.GetName());

	size_t nativeAlign = type.GetAlign();
	size_t realAlignment = Max(align, nativeAlign);
	psoRscBuilderInstanceSet& set = m_Instances.FindOrCreateInstanceSet(type, sizeInBytes, realAlignment, physicalHeap);

	psoRscBuilderInstance* newInst = rage_new psoRscBuilderInstance;
	newInst->m_Data = const_cast<char*>(object);
	newInst->m_Size = sizeInBytes;

	set.AddInstance(newInst);
	return *set.m_Instances.Top();
}

psoRscBuilderInstance& psoRscBuilder::CreatePodArray(psoType type, size_t sizeInBytes, size_t align, bool physicalHeap)
{
	parAssertf(type.IsStructArrayType(), "Type %d %s isn't a structarray type", type.GetEnum(), type.GetName());

	size_t nativeAlign = type.GetAlign();

	size_t realAlign = Max(nativeAlign, align);

	FastAssert(sizeInBytes % nativeAlign == 0);

	char* storage = rage_aligned_new(realAlign) char[sizeInBytes];
	psoRscBuilderInstance& newInst = AddPodArray(type, storage, sizeInBytes, realAlign, physicalHeap);
	newInst.m_OwnsData = true;

	return newInst;
}


void psoRscBuilder::AddExtraString(int nsIndex, u32 hash, const char* strData)
{
	if (hash != 0)
	{
		m_Instances.m_AllHashStrings[nsIndex][hash] = strData;
	}
}


void psoRscBuilder::SetRootObject(psoRscBuilderInstance& inst)
{
	m_Instances.m_RootObject = &inst;
}

void psoRscBuilder::FinishBuilding()
{
	FastAssert(m_UnderConstruction);
	m_UnderConstruction = false;
}


psoRscBuilderStructSchema& psoRscBuilder::FinishStructSchema( psoRscBuilderStructSchema& in )
{
	in.FinishBuilding();

	u32 inSig = in.m_Structure.ComputeBaseSignature(false); 
	// It should be OK that we're only using the base signature here, since we are only comparing structures from
	// within the same file, so if structure A references enum E, and structure B references enum E, we don't need to check 
	// the signatures of E since we know it's the same.

	for(int i = 0; i < m_Schemas.m_StructSchemas.GetCount(); i++)
	{
		psoRscStructSchemaData& schema = m_Schemas.m_StructSchemas[i]->m_Structure;
		if (psoConstants::IsAnonymous(schema.m_NameHash) && inSig == schema.ComputeBaseSignature(false))
		{
			// Found a likely match. How confident are we that it's a for-real match? Lets cross our fingers for now...
			delete &in;
			return *m_Schemas.m_StructSchemas[i];
		}
	}
	m_Schemas.m_StructSchemas.PushAndGrow(&in);

	Assert(in.m_Structure.m_Size > 0);

	return in;
}

psoRscBuilderEnumSchema& psoRscBuilder::FinishEnumSchema( psoRscBuilderEnumSchema& in )
{
	in.FinishBuilding();

	m_Schemas.m_EnumSchemas.PushAndGrow(&in);

	return in;
}


size_t psoRscBuilderInstanceSet::FindUnitSize() const
{
	size_t unitSize = 0;
	if (m_StructSchema)
	{
		FastAssert(!m_PodType.IsValid());
		unitSize = m_StructSchema->GetSize();
	}
	else
	{
		FastAssert(m_PodType.IsStructArrayType());
		unitSize = m_PodType.GetSize();
	}
	return unitSize;
}

size_t rage::psoRscBuilderInstanceSet::FindSetSize() const
{
	return m_Size;
}

bool rage::psoRscBuilderInstanceSet::CanHold(size_t sizeInBytes) const
{
	ptrdiff_t sizeRemaining = STRUCTARRAY_SPLIT_SIZE - FindSetSize(); // Can be negative if we added a bigger than usual instance
	return (ptrdiff_t)sizeInBytes <= sizeRemaining;
}

void psoRscBuilderInstanceSet::AddInstance(psoRscBuilderInstance* inst)
{
	m_Instances.PushAndGrow(inst);
	m_Size += inst->m_Size;
}

psoRscBuilderInstanceSet& rage::psoRscBuilderInstanceDataCatalog::Iterator::operator*()
{
	if (m_MapIter.AtEnd())
	{
		return *m_Cat.m_NonNativeAlignedInstances[m_NonNativeNum];
	}
	else
	{
		return *(*m_MapIter)[m_SetIdx];
	}
}

bool rage::psoRscBuilderInstanceDataCatalog::Iterator::AtEnd()
{
	return m_MapIter.AtEnd() && m_NonNativeNum >= m_Cat.m_NonNativeAlignedInstances.GetCount();
}

void rage::psoRscBuilderInstanceDataCatalog::Iterator::Next()
{
	if (!m_MapIter.AtEnd())
	{
		m_SetIdx++;
		if (m_SetIdx >= (*m_MapIter).GetCount())
		{
			m_SetIdx = 0;
			m_MapIter.Next();
		}
	}
	else
	{
		m_NonNativeNum++;
	}
}

psoResourceData* psoRscBuilder::ConstructResourceData()
{
	psoResourceData* rsc = rage_new psoResourceData;

	rsc->m_NumEnumSchemas = (u16)m_Schemas.m_EnumSchemas.GetCount();
	rsc->m_EnumSchemaTable.NewArrayNoCookie(rsc->m_NumEnumSchemas);
	for(int i = 0; i < rsc->m_NumEnumSchemas; i++)
	{
		m_Schemas.m_EnumSchemas[i]->CopyTo(rsc->m_EnumSchemaTable[i]);
	}

	rsc->m_NumStructSchemas = (u16)m_Schemas.m_StructSchemas.GetCount();
	rsc->m_StructSchemaTable.NewArrayNoCookie(rsc->m_NumStructSchemas);
	for(int i = 0; i < rsc->m_NumStructSchemas; i++)
	{
		m_Schemas.m_StructSchemas[i]->CopyTo(rsc->m_StructSchemaTable[i]);
	}

	m_Instances.ConvertHashstringsToInstances();
	m_Instances.AddInstancesToResource(rsc);

	// Add the original hash string (other namespaces get added via ConvertHashstringsToInstances)
	if (psoRscBuilderInstanceDataCatalog::StringMap* map = m_Instances.m_AllHashStrings.Access(HSNS_ATFINALHASHSTRING))
	{
		m_Instances.AddStringsToResource(rsc->m_FinalHashStrings, *map, false);
	}
#if !__FINAL
	if (psoRscBuilderInstanceDataCatalog::StringMap* map = m_Instances.m_AllHashStrings.Access(HSNS_ATHASHSTRING))
	{
		rsc->m_Flags.Set(psoResourceData::ENCRYPT_NONFINAL_STRINGS);
		m_Instances.AddStringsToResource(rsc->m_NonFinalHashStrings, *map, true);
	}
#endif

	psoSchemaCatalog cat(*rsc);
	cat.ComputeAllSignatures(false);

	ConvertPointersToStructIds(rsc);


	if (m_Instances.m_RootObject)
	{
		rsc->m_RootObjectId = m_Instances.m_RootObject->m_BaseId;
	}
	else
	{
		rsc->m_RootObjectId = psoStructId::Null();
	}

	return rsc;
}

psoResourceData* psoRscBuilder::CopyResourceData(psoResourceData* oldRsc, bool copyDebugStrings)
{
	psoResourceData* rsc = rage_new psoResourceData;
	*rsc = *oldRsc;

	rsc->m_NumEnumSchemas = oldRsc->m_NumEnumSchemas;
	rsc->m_EnumSchemaTable.NewArrayNoCookie(rsc->m_NumEnumSchemas);
	sysMemCpy(rsc->m_EnumSchemaTable.GetPtr(), oldRsc->m_EnumSchemaTable.GetPtr(), sizeof(psoRscEnumSchemaData) * rsc->m_NumEnumSchemas);
	for(int i = 0; i < rsc->m_NumEnumSchemas; i++)
	{
		psoRscEnumSchemaData& oldEnums = oldRsc->m_EnumSchemaTable[i];
		psoRscEnumSchemaData& newEnums = rsc->m_EnumSchemaTable[i];
		if (newEnums.m_NumValues)
		{
			newEnums.m_Values.NewArrayNoCookie(newEnums.m_NumValues);
			sysMemCpy(newEnums.m_Values.GetPtr(), oldEnums.m_Values.GetPtr(), sizeof(psoRscEnumSchemaValueData) * newEnums.m_NumValues);
		}
	}

	rsc->m_NumStructSchemas = oldRsc->m_NumStructSchemas;
	rsc->m_StructSchemaTable.NewArrayNoCookie(rsc->m_NumStructSchemas);
	sysMemCpy(rsc->m_StructSchemaTable.GetPtr(), oldRsc->m_StructSchemaTable.GetPtr(), sizeof(psoRscStructSchemaData) * rsc->m_NumStructSchemas);
	for(int i = 0; i < rsc->m_NumStructSchemas; i++)
	{
		psoRscStructSchemaData& oldStruct = oldRsc->m_StructSchemaTable[i];
		psoRscStructSchemaData& newStruct = rsc->m_StructSchemaTable[i];
		newStruct.m_Members.NewArrayNoCookie(newStruct.m_NumMembers);
		sysMemCpy(newStruct.m_Members.GetPtr(), oldStruct.m_Members.GetPtr(), sizeof(psoRscMemberSchemaData) * newStruct.m_NumMembers);
	}

	rsc->m_NumStructArrayEntries = oldRsc->m_NumStructArrayEntries;
	rsc->m_StructArrayTable.NewArrayNoCookie(rsc->m_NumStructArrayEntries);
	for(int i = 0; i < rsc->m_NumStructArrayEntries; i++)
	{
		psoRscStructArrayTableData& oldEntry = oldRsc->m_StructArrayTable[i];
		psoRscStructArrayTableData& newEntry = rsc->m_StructArrayTable[i];
		newEntry.m_NameHash = oldEntry.m_NameHash;
		newEntry.m_Size = oldEntry.m_Size;
		newEntry.m_Data.NewArrayNoCookie(newEntry.m_Size);
		sysMemCpy(newEntry.m_Data.GetPtr(), oldEntry.m_Data.GetPtr(), newEntry.m_Size);
	}

	size_t stringSize = 0;
	char* stringPtr = oldRsc->m_FinalHashStrings.GetPtr();
	if (stringPtr)
	{
		while(*stringPtr)
		{
			stringSize += strlen(stringPtr) + 1;
		}
		stringSize += 1;

		rsc->m_FinalHashStrings.NewArrayNoCookie(stringSize);
		sysMemCpy(rsc->m_FinalHashStrings.GetPtr(), oldRsc->m_FinalHashStrings.GetPtr(), stringSize);
	}

	stringSize = 0;
	stringPtr = oldRsc->m_NonFinalHashStrings.GetPtr();
	if (stringPtr && copyDebugStrings)
	{
		while(*stringPtr)
		{
			stringSize += strlen(stringPtr) + 1;
		}
		stringSize += 1;

		rsc->m_NonFinalHashStrings.NewArrayNoCookie(stringSize);
		sysMemCpy(rsc->m_NonFinalHashStrings.GetPtr(), oldRsc->m_NonFinalHashStrings.GetPtr(), stringSize);
	}


	return rsc;
}

#define DEFAULT_BUILD_SIZE (64 * 1024 * 1024)

#if __RESOURCECOMPILER
bool psoRscBuilder::SaveResource(const char* filename)
{
	char baseName[RAGE_MAX_PATH];
	safecpy(baseName, filename);
	const char* ext = ASSET.FindExtensionInPath(baseName);
	if (ext)
	{
		*const_cast<char*>(ext) = '.';
		++ext;
	}

	if (!parVerifyf(!m_UnderConstruction, "The PSO is still under construction"))
	{
		return false;
	}

	g_ByteSwap = sysGetByteSwap(g_sysPlatform);

	for(int pass = 0; pass <= pgRscBuilder::LAST_PASS; ++pass)
	{
		pgRscBuilder::SetBuildName(filename);
		pgRscBuilder::BeginBuild(pass == pgRscBuilder::LAST_PASS, DEFAULT_BUILD_SIZE, 0, 0, 0);

		psoResourceData* rscData = ConstructResourceData();

		if (sysGetByteSwap(g_sysPlatform))
		{
			psoSwapStructArrays(*rscData, false);
		}

		if (pass == pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(rscData);
			datTypeStruct myStruct;
			rscData->DeclareStruct(myStruct);

			pgRscBuilder::SaveBuild(baseName, ext, psoResourceData::RESOURCE_VERSION);
		}

		pgRscBuilder::EndBuild(rscData);
	}
	return true;
}

bool psoRscBuilder::SaveCopyOfResource(const char* filename, psoResourceData* data, bool copyDebugStrings)
{
	char baseName[RAGE_MAX_PATH];
	safecpy(baseName, filename);
	const char* ext = ASSET.FindExtensionInPath(baseName);
	if (ext)
	{
		*const_cast<char*>(ext) = '.';
		++ext;
	}

	g_ByteSwap = sysGetByteSwap(g_sysPlatform);

	for(int pass = 0; pass <= pgRscBuilder::LAST_PASS; ++pass)
	{
		pgRscBuilder::SetBuildName(filename);
		pgRscBuilder::BeginBuild(pass == pgRscBuilder::LAST_PASS, DEFAULT_BUILD_SIZE, 0, 0, 0);

		psoResourceData* rscData = CopyResourceData(data, copyDebugStrings);

		if (sysGetByteSwap(g_sysPlatform))
		{
			psoSwapStructArrays(*rscData, false);
		}

		if (pass == pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(rscData);
			datTypeStruct myStruct;
			rscData->DeclareStruct(myStruct);

			pgRscBuilder::SaveBuild(baseName, ext, psoResourceData::RESOURCE_VERSION);
		}

		pgRscBuilder::EndBuild(rscData);
	}
	return true;


}

#endif // __RESOURCECOMPILER


bool CompareSourceAddr(psoRscBuilderInstance* a, psoRscBuilderInstance* b)
{
	return (a->m_Data < b->m_Data);
}


void rage::psoRscBuilderInstanceDataCatalog::AddInstancesToResource( psoResourceData* rsc )
{
	int numStructArrays = 0;
	for(Iterator iter(*this); !iter.AtEnd(); iter.Next())
	{
		numStructArrays++;
	}
	rsc->CreateStructArrayTable(numStructArrays);

	int structArrayCtr = 0;
	for(Iterator iter(*this); !iter.AtEnd(); iter.Next())
	{
		AddInstanceToResource(*iter, structArrayCtr, rsc);
		structArrayCtr++;
	}

	std::sort(m_InstancesSortedBySourceAddr.begin(), m_InstancesSortedBySourceAddr.end(), CompareSourceAddr);
}

void rage::psoRscBuilderInstanceDataCatalog::AddInstanceToResource( psoRscBuilderInstanceSet& set, int structArrayIdx, psoResourceData* rsc )
{
	size_t storageSize = set.FindSetSize();

	char* storage = NULL;
	if (set.m_PhysicalHeap)
	{
		storage = parUtils::PhysicalAllocate(storageSize, set.m_Align);
	}
	else
	{
		storage = rage_aligned_new(set.m_Align) char[storageSize];
	}

	char* copyPtr = storage;
	for(int i = 0; i < set.m_Instances.GetCount(); i++)
	{
		psoRscBuilderInstance& inst = *set.m_Instances[i];
		// Now we know where the inst goes in the resource, record some info for lookup when doing pointer relocation
		inst.m_BaseId = psoStructId::Create(structArrayIdx, (u32)(copyPtr - storage));

		{
			sysMemAutoUseTempMemory useTemp; 
			m_InstancesSortedBySourceAddr.PushAndGrow(&inst);
		}

		// And copy the data into the resource
		sysMemCpy(copyPtr, inst.m_Data, inst.m_Size);
		copyPtr += inst.m_Size;
	}

	psoRscStructArrayTableData& sa = rsc->m_StructArrayTable[structArrayIdx];
	sa.m_Data = storage;
	sa.m_Size = (u32)storageSize;
	if (set.m_StructSchema)
	{
		sa.m_NameHash = set.m_StructSchema->GetName();
	}
	else
	{
		sa.m_NameHash.SetHash(set.m_PodType.GetEnum());
	}
}

void rage::psoRscBuilderInstanceDataCatalog::ConvertHashstringsToInstances()
{
	for(NamespaceMap::Iterator iter = m_AllHashStrings.CreateIterator(); !iter.AtEnd(); iter.Next())
	{
		int nsIndex = iter.GetKey();
		StringMap& strings = iter.GetData();

		if (nsIndex == HSNS_ATHASHSTRING || nsIndex == HSNS_ATFINALHASHSTRING)
		{
			// We'll save these out the old way
			continue;
		}

		size_t stringDataSize = 16; // Reserve 16b at the front of the strings for future use (flags?)
		for(StringMap::Iterator iter = strings.CreateIterator(); !iter.AtEnd(); iter.Next())
		{
			stringDataSize += strlen(*iter) + 1; // add 1 for NULL byte
		}
		stringDataSize += 1; // add 1 for the extra terminating NULL byte
		stringDataSize = RoundUp<16>(stringDataSize); // Round up to 16b in case we want to encrypt the strings using 16b blocks

		psoRscBuilderInstanceSet* instanceSet = FindOrCreateInstanceSet(psoConstants::FIRST_HASHSTRING_ID + nsIndex, 1, stringDataSize, 1, false);

		FastAssert(instanceSet);

		// Fill out the string data
		char* stringData = rage_new char[stringDataSize];
		sysMemSet(stringData, 0x0, stringDataSize);

		char* writeLoc = stringData + 16; // Skip first 16 (reserved) bytes
		char* end = stringData + stringDataSize;

		for(StringMap::Iterator iter = strings.CreateIterator(); !iter.AtEnd(); iter.Next())
		{
			size_t bytes = strlen(*iter) + 1;
			FastAssert(writeLoc + bytes < end);
			safecpy(writeLoc, *iter, end - writeLoc);
			writeLoc += bytes;
		}
		
		psoRscBuilderInstance* inst = rage_new psoRscBuilderInstance;
		inst->m_Data = stringData;
		inst->m_Size = stringDataSize;
		inst->m_OwnsData = true;

		instanceSet->AddInstance(inst);
	}
}

void rage::psoRscBuilderInstanceDataCatalog::AddStringsToResource( psoPtr<char>& dest, StringMap& map, bool NOTFINAL_ONLY(encrypt) )
{
	if (map.GetNumUsed() == 0)
	{
		dest = NULL;
		return;
	}

	size_t stringDataSize = 0;
	for(StringMap::Iterator iter = map.CreateIterator(); !iter.AtEnd(); iter.Next())
	{
		stringDataSize += strlen(*iter) + 1; // add 1 for NULL byte
	}
	stringDataSize += 1; // add 1 for the extra terminating NULL byte

#if !__FINAL
	if (encrypt)
	{
		stringDataSize = RoundUp<16>(stringDataSize);
		stringDataSize += 4;
	}
	size_t encryptBufferSize = stringDataSize - 4;
#endif

	dest.NewArrayNoCookie(stringDataSize);

	char* writeLoc = dest.GetPtr();
	char* end = writeLoc + stringDataSize;

#if !__FINAL
	if (encrypt)
	{
		// Write the size to the first 4 bytes
		*writeLoc = (encryptBufferSize >> 24) & 0xff;
		writeLoc++;
		*writeLoc = (encryptBufferSize >> 16) & 0xff;
		writeLoc++;
		*writeLoc = (encryptBufferSize >> 8) & 0xff;
		writeLoc++;
		*writeLoc = (encryptBufferSize >> 0) & 0xff;
		writeLoc++;
	}
#endif


	for(StringMap::Iterator iter = map.CreateIterator(); !iter.AtEnd(); iter.Next())
	{
		size_t bytes = strlen(*iter) + 1;
		safecpy(writeLoc, *iter, stringDataSize);
		stringDataSize -= bytes;
		writeLoc += bytes;
	}

	while(writeLoc != end)
	{
		*writeLoc = '\0';
		writeLoc++;
	}

#if !__FINAL
	if (encrypt)
	{
		AES aes(psoResourceData::NonfinalStringKey);
		aes.Encrypt(dest.GetPtr() + 4, (u32)encryptBufferSize); // Don't encrypt the first 4 bytes
	}
#endif
}

bool SearchForAddr(psoRscBuilderInstance* a, psoRscBuilderInstance* b)
{
	return (a->m_Data < b->m_Data);
}

// Can't call this for non-natively sized data (yet?)
#if __64BIT
void psoRscBuilder::ConvertPointerToStructId(psoFake64::CharPtr & ptr)
#else
void psoRscBuilder::ConvertPointerToStructId(psoFake32::CharPtr & ptr)
#endif
{
	if (!ptr.m_Pointer)
	{
		ptr.m_StructId = psoStructId::Null(); // Really the same thing as 0x0, but lets be explicit
		return;
	}

	psoRscBuilderInstance search; search.m_Data = ptr.m_Pointer;
	psoRscBuilderInstance** lowerBoundIter = std::lower_bound(m_Instances.m_InstancesSortedBySourceAddr.begin(), m_Instances.m_InstancesSortedBySourceAddr.end(), &search, SearchForAddr);
	if (parVerifyf(lowerBoundIter != m_Instances.m_InstancesSortedBySourceAddr.end(), "Couldn't find address %p in instances", ptr.m_Pointer))
	{
		psoRscBuilderInstance& inst = *(*lowerBoundIter);
		psoStructId id = inst.m_BaseId;
		ptrdiff_t offset = ptr.m_Pointer - inst.m_Data;
		u32 tableId = id.GetTableIndex();
		u32 arrayOffset = id.GetArrayOffset() + (u32)(offset);

		parAssertf(offset < (ptrdiff_t)inst.m_Size, "The address %p isn't inside any instance", ptr.m_Pointer );

		ptr.m_StructId = psoStructId::Create(tableId, arrayOffset);
	}
}

// This is a helper macro that maybe could be a template if I were more clever about how to pass in
// which member needs the fixup
#define CONVERT_PODARRAY(type, member)							\
{																\
	type* elements = (type*)structArray.m_Data.GetPtr();		\
	int numElements = structArray.m_Size / sizeof(type);		\
	for(int i = 0; i < numElements; i++)						\
	{															\
		type& elt = elements[i];								\
		ConvertPointerToStructId(member);						\
	}															\
}																\
//END

void psoRscBuilder::ConvertPointersToStructIds_PodArray( psoRscStructArrayTableData& structArray ) 
{
	psoType type = (psoType::Type)structArray.m_NameHash.GetHash();
	if (parVerifyf(type.IsStructArrayType(), "Type 0x%x is not a structarray type", (int)type.GetEnum()))
	{
		switch(type.GetEnum())
		{
#if __64BIT
			case psoType::TYPE_POINTER64:
			case psoType::TYPE_STRING_POINTER64:
			case psoType::TYPE_WIDE_STRING_POINTER64:
			case psoType::TYPE_ARRAY_POINTER64:
				CONVERT_PODARRAY(psoFake64::CharPtr, elt);
				break;

			case psoType::TYPE_ATSTRING64:			CONVERT_PODARRAY(psoFake64::AtString, elt.m_Data);		break;
			case psoType::TYPE_ATWIDESTRING64:		CONVERT_PODARRAY(psoFake64::AtWideString, elt.m_Data);	break;
			case psoType::TYPE_ATARRAY64:			CONVERT_PODARRAY(psoFake64::AtArray16, elt.m_Elements);	break;
			case psoType::TYPE_ATARRAY64_32BITIDX:	CONVERT_PODARRAY(psoFake64::AtArray32, elt.m_Elements);	break;
			case psoType::TYPE_ATBINARYMAP64:		CONVERT_PODARRAY(psoFake64::AtBinMap, elt.m_Data.m_Elements); break;
#else
			case psoType::TYPE_POINTER32:
			case psoType::TYPE_STRING_POINTER32:
			case psoType::TYPE_WIDE_STRING_POINTER32:
			case psoType::TYPE_ARRAY_POINTER32:
				CONVERT_PODARRAY(psoFake32::CharPtr, elt);
				break;

			case psoType::TYPE_ATSTRING32:			CONVERT_PODARRAY(psoFake32::AtString, elt.m_Data);		break;
			case psoType::TYPE_ATWIDESTRING32:		CONVERT_PODARRAY(psoFake32::AtWideString, elt.m_Data);	break;
			case psoType::TYPE_ATARRAY32:			CONVERT_PODARRAY(psoFake32::AtArray16, elt.m_Elements);	break;
			case psoType::TYPE_ATARRAY32_32BITIDX:	CONVERT_PODARRAY(psoFake32::AtArray32, elt.m_Elements);	break;
			case psoType::TYPE_ATBINARYMAP32:		CONVERT_PODARRAY(psoFake32::AtBinMap, elt.m_Data.m_Elements); break;
#endif
			default:
				// Doesn't contain a pointer, no fixups necessary
				parAssertf(type.IsNative(), "Shouldn't have any non-native sized pointers");
				parAssertf(!type.HasPointers(), "Shouldn't have a pointer-containing type here, it didn't get fixed up");
		}
	}
}
#undef CONVERT_PODARRAY

void psoRscBuilder::ConvertPointersToStructIds_Member(psoStruct& str, psoMember& mem)
{
	psoType type = mem.GetType();
	switch(type.GetEnum())
	{
#if __64BIT
	case psoType::TYPE_POINTER64:					ConvertPointerToStructId(mem.GetDataAs<psoFake64::CharPtr>()); break;
	case psoType::TYPE_STRING_POINTER64:			ConvertPointerToStructId(mem.GetDataAs<psoFake64::CharPtr>()); break;
	case psoType::TYPE_ATSTRING64:					ConvertPointerToStructId(mem.GetDataAs<psoFake64::AtString>().m_Data); break;
	case psoType::TYPE_WIDE_STRING_POINTER64:		ConvertPointerToStructId(mem.GetDataAs<psoFake64::CharPtr>()); break;
	case psoType::TYPE_ATWIDESTRING64:				ConvertPointerToStructId(mem.GetDataAs<psoFake64::AtWideString>().m_Data); break;
	case psoType::TYPE_ATARRAY64:					ConvertPointerToStructId(mem.GetDataAs<psoFake64::AtArray16>().m_Elements); break;
	case psoType::TYPE_ATARRAY64_32BITIDX:			ConvertPointerToStructId(mem.GetDataAs<psoFake64::AtArray32>().m_Elements); break;
	case psoType::TYPE_ARRAY_POINTER64:				ConvertPointerToStructId(mem.GetDataAs<psoFake64::CharPtr>()); break;
	case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:	ConvertPointerToStructId(mem.GetDataAs<psoFake64::CharPtr>()); break;
	case psoType::TYPE_ATBITSET64:					
		{
			psoFake64::AtBitset& bitset = mem.GetDataAs<psoFake64::AtBitset>();
			psoFake64::CharPtr& ptr = reinterpret_cast<psoFake64::CharPtr&>(bitset.m_Bits);
			ConvertPointerToStructId(ptr);
		}
		break;
	case psoType::TYPE_ATBINARYMAP64:				ConvertPointerToStructId(mem.GetDataAs<psoFake64::AtBinMap>().m_Data.m_Elements); break;
#else
	case psoType::TYPE_POINTER32:					ConvertPointerToStructId(mem.GetDataAs<psoFake32::CharPtr>()); break;
	case psoType::TYPE_STRING_POINTER32:			ConvertPointerToStructId(mem.GetDataAs<psoFake32::CharPtr>()); break;
	case psoType::TYPE_ATSTRING32:					ConvertPointerToStructId(mem.GetDataAs<psoFake32::AtString>().m_Data); break;
	case psoType::TYPE_WIDE_STRING_POINTER32:		ConvertPointerToStructId(mem.GetDataAs<psoFake32::CharPtr>()); break;
	case psoType::TYPE_ATWIDESTRING32:				ConvertPointerToStructId(mem.GetDataAs<psoFake32::AtWideString>().m_Data); break;
	case psoType::TYPE_ATARRAY32:					ConvertPointerToStructId(mem.GetDataAs<psoFake32::AtArray16>().m_Elements); break;
	case psoType::TYPE_ATARRAY32_32BITIDX:			ConvertPointerToStructId(mem.GetDataAs<psoFake32::AtArray32>().m_Elements); break;
	case psoType::TYPE_ARRAY_POINTER32:				ConvertPointerToStructId(mem.GetDataAs<psoFake32::CharPtr>()); break;
	case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:	ConvertPointerToStructId(mem.GetDataAs<psoFake32::CharPtr>()); break;
	case psoType::TYPE_ATBITSET32:					
		{
			psoFake32::AtBitset& bitset = mem.GetDataAs<psoFake32::AtBitset>();
			psoFake32::CharPtr& ptr = reinterpret_cast<psoFake32::CharPtr&>(bitset.m_Bits);
			ConvertPointerToStructId(ptr);
		}
		break;
	case psoType::TYPE_ATBINARYMAP32:				ConvertPointerToStructId(mem.GetDataAs<psoFake32::AtBinMap>().m_Data.m_Elements); break;
#endif
	case psoType::TYPE_STRUCT:
		{
			psoStruct subStr = mem.GetSubStructure();
			if (subStr.IsValid())
			{
				ConvertPointersToStructIds_Struct(subStr);
			}
		}
		break;
	case psoType::TYPE_ARRAY_MEMBER:
	case psoType::TYPE_ATFIXEDARRAY:
		{
			psoMemberArrayInterface memArray(str, mem);
			size_t count = memArray.GetArrayCount();
			for(size_t i = 0; i < count; i++)
			{
				psoMember elt = memArray.GetElement(i);
				ConvertPointersToStructIds_Member(str, elt);
			}
		}
		break;
	default:
		parAssertf(!type.HasPointers() && !type.IsDirectContainer(), "How did this type slip through? %x", type.GetEnum());
	}
}

void psoRscBuilder::ConvertPointersToStructIds_Struct(psoStruct& str)
{
	for(int i = 0; i < str.GetSchema().GetNumMembers(); i++)
	{
		psoMember mem = str.GetMemberByIndex(i);
		if (!psoConstants::IsReserved(mem.GetSchema().GetNameHash()))
		{
			ConvertPointersToStructIds_Member(str, mem);
		}
	}
}

void PrintPsoDescription(rage::psoFile& f);
void PrintPsoSchemaCatalog(rage::psoFile& file, rage::psoSchemaCatalog cat);

void psoRscBuilder::ConvertPointersToStructIds( psoResourceData* rscData ) 
{
	psoFile fakeFile;
	fakeFile.Init(rscData, false, false);
	psoSchemaCatalog cat = fakeFile.GetSchemaCatalog();

	for(int i = 0; i < rscData->m_NumStructArrayEntries; i++)
	{
		psoRscStructArrayTableData& structArray = rscData->m_StructArrayTable[i];
		if (psoConstants::IsPsoType(structArray.m_NameHash.GetHash()))
		{
			ConvertPointersToStructIds_PodArray(structArray);
		}
		else
		{
			psoStructureSchema schema = cat.FindStructureSchema(structArray.m_NameHash);
			int eltSize = (int)schema.GetSize();
			int numElts = structArray.m_Size / eltSize;
			for(int elt = 0; elt < numElts; elt++)
			{
				psoStructId instId = psoStructId::Create(i, elt * eltSize);
				psoStruct str = fakeFile.GetInstance(instId);
				ConvertPointersToStructIds_Struct(str);
			}
		}
	}
}
