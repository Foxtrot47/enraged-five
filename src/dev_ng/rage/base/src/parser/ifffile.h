// 
// parser/ifffile.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_IFFFILE_H
#define PARSER_IFFFILE_H

#include "parser/macros.h"
#include "system/endian.h"
#include "system/magicnumber.h"

namespace rage {

using namespace sysEndian;

class parIffHeader {
public:
	u32 GetMagic() const { return BtoN(m_Magic); }
	void SetMagic(u32 val) {m_Magic = NtoB(val); }

	u32 GetSize() const { return BtoN(m_Size); }
	void SetSize(u32 val) {m_Size = NtoB(val); }

protected:
	u32 m_Magic;
	u32 m_Size;
};

class parIffFileReadIterator {
public:
	parIffFileReadIterator(char* startOfChunks, char* endOfChunks)
		: m_CurrentPosition(startOfChunks)
		, m_EndOfChunks(endOfChunks)
		, m_EndOfCurrentChunk(NULL)
	{
		OUTPUT_ONLY(m_StartOfFile = startOfChunks;)
		char* endOfFirstChunk = m_CurrentPosition ? m_CurrentPosition + GetChunkSize() : NULL;
		if (endOfFirstChunk > m_EndOfChunks)
		{
			// EXPAND_STATE_KEY is EXPAND_MAGIC_NUMBER but in the 'big endian' order that we used in MAKE_MAGIC_NUMBER_BE
			parWarningf("Invalid IFF tag or truncated file at offset %d/%d: tag '%c%c%c%c', chunk size %d", 
				(int)ptrdiff_t_to_int(m_CurrentPosition - m_StartOfFile), 
				(int)ptrdiff_t_to_int(m_EndOfChunks - m_StartOfFile),
				EXPAND_STATE_KEY(GetMagicNumber()),
				GetChunkSize());
			m_CurrentPosition = NULL;
		}
		else
		{
			m_EndOfCurrentChunk = endOfFirstChunk ;
		}
	}

	u32 GetMagicNumber() const {return GetHeader().GetMagic();}
	u32 GetChunkSize() const {return GetHeader().GetSize();}

	char* GetEndOfLastChunkRead() { return m_EndOfCurrentChunk; }

	bool NextChunk()
	{
		if (m_CurrentPosition && (m_EndOfCurrentChunk < m_EndOfChunks)) {
			m_CurrentPosition = m_EndOfCurrentChunk;
			if (m_CurrentPosition + sizeof(parIffHeader) > m_EndOfChunks)
			{
				parDebugf1("Not enough room left in the file for an IFF header - assuming this is padding");
				m_CurrentPosition = NULL;
				return false;
			}
			if (GetMagicNumber() == MAKE_MAGIC_NUMBER('p', 'p', 'p', 'p'))
			{
				parDebugf1("Found some padding, assuming we're done with the file");
				m_CurrentPosition = NULL;
				return false;
			}
			char* endOfNextChunk = m_CurrentPosition + GetChunkSize();
			if (endOfNextChunk > m_EndOfChunks || endOfNextChunk < (m_CurrentPosition + sizeof(parIffHeader)))
			{
				// EXPAND_STATE_KEY is EXPAND_MAGIC_NUMBER but in the 'big endian' order that we used in MAKE_MAGIC_NUMBER_BE
				parWarningf("Invalid IFF tag or truncated file at offset %d/%d: tag '%c%c%c%c', chunk size %d", 
					(int)ptrdiff_t_to_int(m_CurrentPosition - m_StartOfFile), 
					(int)ptrdiff_t_to_int(m_EndOfChunks - m_StartOfFile),
					EXPAND_STATE_KEY(GetMagicNumber()),
					GetChunkSize());
				m_CurrentPosition = NULL;
				return false;
			}
			m_EndOfCurrentChunk = endOfNextChunk;
			return true;
		}
		m_CurrentPosition = NULL;
		return false;
	}
	
	char* GetChunk()
	{
		return m_CurrentPosition;
	}

	char* GetChunkData()
	{
		if (!m_CurrentPosition) 
		{
			return NULL;
		}
		return m_CurrentPosition + sizeof(parIffHeader);
	}

private:
	const parIffHeader& GetHeader() const {return *reinterpret_cast<const parIffHeader*>(m_CurrentPosition); }

	char* m_CurrentPosition;
	char* m_EndOfChunks;
	char* m_EndOfCurrentChunk;
	OUTPUT_ONLY(char* m_StartOfFile;)
};

}

#endif
