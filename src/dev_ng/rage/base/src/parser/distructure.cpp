// 
// parser/distructure.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "distructure.h"

#include "manager.h"
#include "member.h"
#include "optimisations.h"
#include "tree.h"
#include "treenode.h"


#include "parsercore/attribute.h"

PARSER_OPTIMISATIONS();

using namespace rage;

/* For custodial pointers:
  create from bottom up, assign from top down

  That is: determine the type of the pointer by looking in the
  child file first, and then looking in the ancestors as necessary.
  Then do data assignment by starting in the parents and overwriting
  it with data from the child.
*/

void parDIStructure::CreatePointerMembers(parTreeNode* node, parPtrToStructure structAddr)
{
	parStructure::BaseList baseInfo;
	baseInfo.Init(this, structAddr);

	parTreeNode::ChildNodeIterator startNode = node->BeginChildren();

	parTreeNode::ChildNodeIterator nodeListBegin = node->BeginChildren();
	parTreeNode::ChildNodeIterator nodeListEnd = node->EndChildren();

	for(int baseNum = baseInfo.GetNumBases()-1; baseNum >= 0; baseNum--) {
		parStructure* currStruct = baseInfo.GetBaseStructure(baseNum);
		parPtrToStructure currAddr = baseInfo.GetBaseAddress(baseNum);

		for(int i = 0; i < currStruct->GetNumMembers(); i++)
		{
			if (currStruct->GetMember(i)->GetType() == parMemberType::TYPE_STRUCT)
			{
				parMemberStruct& structMetadata = currStruct->GetMember(i)->AsStructOrPointerRef();
				if (structMetadata.IsParsableData() && structMetadata.IsOwnerPointer() && structMetadata.GetObjAddr(currAddr) == NULL) 
				{
					bool looped = false;
					parTreeNode::ChildNodeIterator currNode = startNode;
					while(!looped || startNode != currNode)
					{
						u32 hash = atLiteralStringHash((*currNode)->GetElement().GetName());
						if (hash == structMetadata.GetNameHash()) {
							parPtrToStructure newObj = structMetadata.CreateNewData(*currNode, 0x0);
							structMetadata.SetObjAddr(structAddr, newObj);
						}
						looped |= parTreeNode::FindNextNodeCircular(currNode, nodeListBegin, nodeListEnd);
					}
					parTreeNode::FindNextNodeCircular(startNode, nodeListBegin, nodeListEnd);
				}
			}
		}

	}

}

void parDIStructure::ReadMembersFromTree(parTreeNode* node, parPtrToStructure structAddr)
{
	CreatePointerMembers(node, structAddr);

	// if there's a parent attribute, read the parent tree and apply that data first.
	parAttribute* parentAttr = node->GetElement().FindAttribute("parent");
	if (parentAttr)
	{
		parTree* parentTree = PARSER.LoadTree(parentAttr->GetStringValue(), "");
		if (parVerifyf(this->IsSubclassOf(PARSER.FindStructure(parentTree->GetRoot()->GetElement().GetName())), "This class %s is not a subclass of class %s from file", this->GetName(), parentTree->GetRoot()->GetElement().GetName())) 
		{
			ReadMembersFromTree(parentTree->GetRoot(), structAddr);
		}
		delete parentTree;
	}

	parStructure::ReadMembersFromTree(node, structAddr);
}

