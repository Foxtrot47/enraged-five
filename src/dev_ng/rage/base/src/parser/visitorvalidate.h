// 
// parser/visitorvalidate.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_VISITORVALIDATE_H 
#define PARSER_VISITORVALIDATE_H 

#include "visitor.h"

#if __BANK && __DEV

namespace rage {

// PURPOSE: Given an instance of a parsable object, run some validation checks on every
//			member, sub-object, and array item within that instance.
// PARAMS:
//			obj - A reference to a parsable object.
// RETURNS: true if the validation tests were successful. False if the test on any member failed.
// NOTES:
//			For scalar and vector values, we check that the values are in the [min,max] range.
//			For enumerations, we check that the value is actually in the enum list (i.e.
//			check that the value corresponds to a name)
//			For pointers we check against some known-bad pointer patterns (0xCCCCCCCC, 0xCDCDCDCD, etc.)
template<typename _Container>
bool parValidateInstance(_Container& obj);


/////////////////////////////////////////////////////////////////////////////////////////////
// Implementation below

// PURPOSE: The visitor for implementing the parValidateInstance traversal, for validating
// the data within a parsable object.
class parValidateVisitor : public parInstanceVisitor
{
public:
	parValidateVisitor()
		: m_Success(true) {}

	virtual ~parValidateVisitor() {}

	virtual void SimpleMember			(parPtrToMember ptrToMember,	parMemberSimple& metadata);
	virtual void VectorMember			(parPtrToMember ptrToMember,	parMemberVector& metadata);
	virtual void MatrixMember			(parPtrToMember ptrToMember,	parMemberMatrix& metadata);
	virtual void EnumMember				(int& data,			parMemberEnum& metadata);
	virtual void BitsetMember			(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata);
	virtual void StringMember			(parPtrToMember ptrToMember, const char* ptrToString, parMemberString& metadata);

	virtual void ExternalPointerMember	(void*& data,		parMemberStruct& metadata);

	virtual bool BeginArrayMember		(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);

	virtual bool BeginPointerMember		(parPtrToStructure& ptrRef, parMemberStruct& metadata);

	bool m_Success;

protected:
	bool ValidatePointer(const void* pointerValue, parMember& member);
};

template<typename _Container>
bool parValidateInstance(_Container& obj)
{
	parValidateVisitor val;
	val.Visit(obj);
	return val.m_Success;
}

} // namespace rage

#endif // __BANK && __DEV

#endif // PARSER_VISITORVALIDATE_H 
