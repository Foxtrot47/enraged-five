// 
// parser/psoschema.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOSCHEMA_H
#define PARSER_PSOSCHEMA_H

#include "ifffile.h"

#include "memberdata.h"
#include "psoconsts.h"
#include "psoptr.h"
#include "psoresource.h"
#include "psoschemadata.h"


#include "atl/bitset.h"
#include "atl/hashstring.h"
#include "parsercore/versioninfo.h"

namespace rage {

class psoResourceData;
class psoSchemaCatalog;
class psoStructureSchema;
struct psoSchemaStructureData;

class psoMemberSchema 
{
public:
	psoMemberSchema() : m_MemberData(&sm_NullData) { }
	explicit psoMemberSchema(psoRscMemberSchemaData& data ) : m_MemberData(&data) { }
	~psoMemberSchema() { }

	atLiteralHashValue GetNameHash() const;
	psoType	GetType() const;
	ptrdiff_t GetOffset() const;

	atLiteralHashValue GetReferentHash() const; // For POINTER and ENUM types

	u32 GetFixedArrayCount() const; // For fixed sized ARRAY types
	int GetArrayCounterSchemaIndex() const; // For ARRAY_WITH_COUNT types
	int GetArrayElementSchemaIndex() const; // For ARRAY types - what is this an array of
	u8 GetArrayAlignmentPower() const;
	bool GetArrayIsPhysical() const;
	
	atLiteralHashValue GetBitsetEnumHash() const; // For BITSET types - the ENUM member describing the bits
	u32 GetBitsetCount() const;

	int GetKeySchemaIndex() const; // For MAP types
	int GetValueSchemaIndex() const; // For MAP types

	u32 GetStringCount() const;
	atHashStringNamespaces GetStringNamespaceIndex() const;

	bool IsValid() const;
	
	bool IsEquivalentTo(const psoMemberSchema& other, bool considerNames, bool considerOffsets) const;

	const psoRscMemberSchemaData* internal_GetRawData() { return m_MemberData; }
private:
	void CheckMemberData(psoRscMemberSchemaData::MemberDataType ASSERT_ONLY(t)) const;

	psoRscMemberSchemaData* m_MemberData;

	static psoRscMemberSchemaData sm_NullData;
};

class psoStructureSchema {
public:
	psoStructureSchema() : m_SchemaData(&sm_NullData) {}
	explicit psoStructureSchema(psoRscStructSchemaData& data) : m_SchemaData(&data) {}
	
	atLiteralHashValue	GetNameHash() const {return m_SchemaData->m_NameHash;}
	size_t				GetSize() const {return m_SchemaData->m_Size;}
	bool				IsValid() const {return m_SchemaData != &sm_NullData;}
	parVersionInfo		GetVersion() const { return parVersionInfo(m_SchemaData->m_MajorVersion, 0); }
	const atFixedBitSet8&	GetFlags() const { return reinterpret_cast<const atFixedBitSet8&>(m_SchemaData->m_Flags); }
	atFixedBitSet8&		GetFlagsRef() { return reinterpret_cast<atFixedBitSet8&>(m_SchemaData->m_Flags); }
	bool				CanUseFastLoading() const { return GetFlags().IsSet(psoSchemaStructureData::FLAG_USE_FAST_LOADING); }
	size_t				GetAlignment() const {return (size_t)1 << m_SchemaData->m_AlignPower; }

	int				GetNumMembers() const {return m_SchemaData->m_NumMembers;}
	psoMemberSchema GetMemberByIndex(int i) const;  // Note: member's aren't stored in any particular order.
	psoMemberSchema GetMemberByName(atLiteralHashValue hash) const;

	u32 GetSignature() const { return m_SchemaData->m_Signature; }
	u32 ComputeBaseSignature() const; 
private:
	psoRscStructSchemaData* m_SchemaData;

	static psoRscStructSchemaData sm_NullData;
};

class psoEnumSchema {
public:
	psoEnumSchema() : m_SchemaData(&sm_NullData) {}
	explicit psoEnumSchema(psoRscEnumSchemaData& data) : m_SchemaData(&data) {}

	atLiteralHashValue	GetNameHash() const {return m_SchemaData->m_NameHash;}
	bool				IsValid() const {return m_SchemaData != &sm_NullData;}

	int GetNumEnums() const {return m_SchemaData->m_NumValues;}

	atLiteralHashValue GetMemberNameHash(int n) const { return m_SchemaData->m_Values[n].m_NameHash;}
	int GetMemberValue(int n) const { return m_SchemaData->m_Values[n].m_Value; }

	bool NameFromValue(int value, atLiteralHashValue& outHash) const;
	bool ValueFromName(atLiteralHashValue hash, int& outResult) const;

	u32 GetSignature() const { return m_SchemaData->m_Signature; }
	u32 ComputeBaseSignature() const;

private:
	psoRscEnumSchemaData* m_SchemaData;

	static psoRscEnumSchemaData sm_NullData;
};

// This class now just provides a convenient interface to access the schema data within a psoResourceData
class psoSchemaCatalog {
	friend class psoFile;
public:
	psoStructureSchema FindStructureSchema(atLiteralHashValue schemaName) const;
	psoEnumSchema FindEnumSchema(atLiteralHashValue schemaName) const;

	psoSchemaCatalog(psoResourceData& data)
		: m_ResourceData(data)
	{
	}

	size_t FindMemberSize(psoStructureSchema& str, psoMemberSchema& mem);

	void ComputeAllSignatures(bool forRuntimeDestination);
	u32 ComputeSignature(const psoStructureSchema& str, bool forRuntimeDestination) const;
	u32 ComputeMemberSignature(const psoStructureSchema& str, const psoMemberSchema& mem, bool forRuntimeDestination) const;

	struct StructureSchemaIterator 
	{
		StructureSchemaIterator(psoSchemaCatalog& cat, u32 index);
		StructureSchemaIterator& operator++();

		psoStructureSchema operator*();

		bool operator==(const StructureSchemaIterator& other);
		bool operator!=(const StructureSchemaIterator& other);

	protected:
		u32 m_Index;
		u32 m_End;
		psoSchemaCatalog& m_Cat;
	};

	struct EnumSchemaIterator
	{
		EnumSchemaIterator(psoSchemaCatalog& cat, u32 index);
		EnumSchemaIterator& operator++();

		psoEnumSchema operator*();

		bool operator==(const EnumSchemaIterator& other);
		bool operator!=(const EnumSchemaIterator& other);

	protected:
		u32 m_Index;
		u32 m_End;
		psoSchemaCatalog& m_Cat;
	};

	StructureSchemaIterator BeginSchemas() { return StructureSchemaIterator(*this, 0); }
	StructureSchemaIterator EndSchemas() { return StructureSchemaIterator(*this, m_ResourceData.m_NumStructSchemas); }
	
	EnumSchemaIterator BeginEnums() { return EnumSchemaIterator(*this, 0); }
	EnumSchemaIterator EndEnums() { return EnumSchemaIterator(*this, m_ResourceData.m_NumEnumSchemas); }

	psoStructureSchema GetStructureSchema(int i);
	psoEnumSchema GetEnumSchema(int i);

private:

	psoResourceData& m_ResourceData;
};




////////////////////////////////////////////////////////////////////////////
// Inline implementations

inline rage::atLiteralHashValue rage::psoMemberSchema::GetNameHash() const
{
	return atLiteralHashValue(m_MemberData->m_NameHash);
}

inline rage::psoType rage::psoMemberSchema::GetType() const
{
	return m_MemberData->GetType();
}

inline ptrdiff_t rage::psoMemberSchema::GetOffset() const
{
	return (ptrdiff_t)m_MemberData->m_Offset;
}

inline rage::atLiteralHashValue rage::psoMemberSchema::GetReferentHash() const
{
	CheckMemberData(psoRscMemberSchemaData::MEM_ENUM_OR_STRUCT); 
	return atLiteralHashValue(m_MemberData->EnumOrStruct.m_ReferentHash);
}

inline u32 rage::psoMemberSchema::GetFixedArrayCount() const
{
	CheckMemberData(psoRscMemberSchemaData::MEM_ARRAY); 
	return m_MemberData->Array.m_Count;
}

inline int rage::psoMemberSchema::GetArrayCounterSchemaIndex() const
{
	CheckMemberData(psoRscMemberSchemaData::MEM_ARRAY_WITH_COUNTER); 
	return (ptrdiff_t)m_MemberData->Array.m_CounterIndex;
}

inline int rage::psoMemberSchema::GetArrayElementSchemaIndex() const
{
	ASSERT_ONLY(psoRscMemberSchemaData::MemberDataType memType = psoRscMemberSchemaData::GetMemberDataType(GetType()));
	FastAssert(memType == psoRscMemberSchemaData::MEM_ARRAY || memType == psoRscMemberSchemaData::MEM_ARRAY_WITH_COUNTER);
	return m_MemberData->Array.m_ContentsIndex;
}

inline u8 rage::psoMemberSchema::GetArrayAlignmentPower() const
{
	ASSERT_ONLY(psoRscMemberSchemaData::MemberDataType memType = psoRscMemberSchemaData::GetMemberDataType(GetType()));
	FastAssert(memType == psoRscMemberSchemaData::MEM_ARRAY || memType == psoRscMemberSchemaData::MEM_ARRAY_WITH_COUNTER);
	return m_MemberData->Array.GetAlignPower();
}

inline bool rage::psoMemberSchema::GetArrayIsPhysical() const
{
	ASSERT_ONLY(psoRscMemberSchemaData::MemberDataType memType = psoRscMemberSchemaData::GetMemberDataType(GetType()));
	FastAssert(memType == psoRscMemberSchemaData::MEM_ARRAY || memType == psoRscMemberSchemaData::MEM_ARRAY_WITH_COUNTER);
	return (m_MemberData->Array.GetFlags() & (1 << psoRscMemberSchemaData::ARRAY_FLAG_PHYSICAL_MEMORY)) != 0;
}

inline rage::atLiteralHashValue rage::psoMemberSchema::GetBitsetEnumHash() const
{
	CheckMemberData(psoRscMemberSchemaData::MEM_BITSET); 
	return atLiteralHashValue(m_MemberData->Bitset.m_EnumHash);
}

inline u32 rage::psoMemberSchema::GetBitsetCount() const
{
	CheckMemberData(psoRscMemberSchemaData::MEM_BITSET); 
	return m_MemberData->Bitset.m_BitCount;
}

inline int rage::psoMemberSchema::GetKeySchemaIndex() const
{
	CheckMemberData(psoRscMemberSchemaData::MEM_MAP); 
	return m_MemberData->Map.m_KeyIndex;
}

inline int rage::psoMemberSchema::GetValueSchemaIndex() const
{
	CheckMemberData(psoRscMemberSchemaData::MEM_MAP); 
	return m_MemberData->Map.m_ValueIndex;
}

inline u32 rage::psoMemberSchema::GetStringCount() const
{
	CheckMemberData(psoRscMemberSchemaData::MEM_STRING); 
	return m_MemberData->String.m_Count;
}

inline atHashStringNamespaces rage::psoMemberSchema::GetStringNamespaceIndex() const
{
	CheckMemberData(psoRscMemberSchemaData::MEM_STRING); 
	return (atHashStringNamespaces)m_MemberData->String.m_NamespaceIndex;
}

inline bool rage::psoMemberSchema::IsValid() const
{
	return GetType().IsValid();
}

inline void rage::psoMemberSchema::CheckMemberData( psoRscMemberSchemaData::MemberDataType ASSERT_ONLY(t) ) const
{
	FastAssert(psoRscMemberSchemaData::GetMemberDataType(GetType()) == t);
}


inline psoSchemaCatalog::StructureSchemaIterator::StructureSchemaIterator( psoSchemaCatalog& cat, u32 index ) : m_Cat(cat)
{
	m_End = m_Cat.m_ResourceData.m_NumStructSchemas;
	m_Index = index;
}

inline psoSchemaCatalog::StructureSchemaIterator& psoSchemaCatalog::StructureSchemaIterator::operator++()
{
	++m_Index;
	return *this;
}

inline psoStructureSchema rage::psoSchemaCatalog::StructureSchemaIterator::operator*()
{
	return m_Cat.GetStructureSchema(m_Index);
}

inline bool psoSchemaCatalog::StructureSchemaIterator::operator==( const StructureSchemaIterator& other )
{
	return &m_Cat == &other.m_Cat && m_Index == other.m_Index;
}

inline bool psoSchemaCatalog::StructureSchemaIterator::operator!=( const StructureSchemaIterator& other )
{
	return &m_Cat != &other.m_Cat || m_Index != other.m_Index;
}



inline psoSchemaCatalog::EnumSchemaIterator::EnumSchemaIterator( psoSchemaCatalog& cat, u32 index ) : m_Cat(cat)
{
	m_End = m_Cat.m_ResourceData.m_NumEnumSchemas;
	m_Index = index;
}

inline psoSchemaCatalog::EnumSchemaIterator& psoSchemaCatalog::EnumSchemaIterator::operator++()
{
	++m_Index;
	return *this;
}

inline psoEnumSchema rage::psoSchemaCatalog::EnumSchemaIterator::operator*()
{
	return m_Cat.GetEnumSchema(m_Index);
}

inline bool psoSchemaCatalog::EnumSchemaIterator::operator==( const EnumSchemaIterator& other )
{
	return &m_Cat == &other.m_Cat && m_Index == other.m_Index;
}

inline bool psoSchemaCatalog::EnumSchemaIterator::operator!=( const EnumSchemaIterator& other )
{
	return &m_Cat != &other.m_Cat || m_Index != other.m_Index;
}


} // namespace rage



#endif // PARSER_PSOSCHEMA_H
