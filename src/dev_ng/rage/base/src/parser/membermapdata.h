// 
// parser/membersimpledata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MEMBERMAPDATA_H 
#define PARSER_MEMBERMAPDATA_H 

#include "memberdata.h"
#include "membermapiterator.h"
#include "membermapinterface.h"

#include "parser/macros.h"
#include "data/callback.h"
#include "atl/delegate.h"

namespace rage {

namespace parMemberMapSubType
{
    // PURPOSE: What kind of map is this?
    enum Enum {
        SUBTYPE_ATMAP,			// atMap<keyfoo, datafoo> m_Member;
        SUBTYPE_ATBINARYMAP,    // atBinaryMap<datafoo, keyfoo> m_Member;
    };
}

// PURPOSE: All of the data that a parMemberMap needs to describe the member.
struct parMemberMapData {

    typedef atDelegate<parMemberMapIterator* (parMemberMap&, void*)> CreateIteratorDelegate; // (mapAddr)
    typedef atDelegate<parMemberMapInterface* (parMemberMap&, void*)> CreateInterfaceDelegate; // (mapAddr)
    
	STANDARD_PARMEMBER_DATA_CONTENTS;
	CreateIteratorDelegate* m_CreateIteratorDelegate;
    CreateInterfaceDelegate* m_CreateInterfaceDelegate;

    void* m_Key;		// The parMember???::Data describing an element in the map
    void* m_Data;		// The parMember???::Data describing an element in the map

	void Init();
	void PreLoad(parTreeNode*) {Init();}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_SIMPLE_PARSABLE;
#endif
};

namespace parMemberMapInterfaces
{
    // PURPOSE: A function that creates an interface for an atMap
    // PARAMS:
    //		_KeyType - The type of key for the map.
    //		_DataType - The type of the data in the map
    //		atMapAddr - The address of the atMap (an atMap<K,D>*)
    // NOTE:
    //      make sure to DELETE the interface once you are done with it
    template<typename _KeyType, typename _DataType>
    static parMemberMapInterface* CreateInterfaceAtMap(parMemberMap&, void* atMapAddr);

    // PURPOSE: A function that creates an interface for an atBinaryMap
    // PARAMS:
    //		_KeyType - The type of key for the map.
    //		_DataType - The type of the data in the map
    //		atBinaryMapAddr - The address of the atBinaryMap (an atBinaryMap<D,K>*)
    // NOTE:
    //      make sure to DELETE the interface once you are done with it
    parMemberMapInterface* CreateInterfaceAtBinaryMap(parMemberMap&, void* atBinaryMapAddr);
}

namespace parMemberMapIterators
{
    // PURPOSE: A function that creates an iterator for an atMap
    // PARAMS:
    //		_KeyType - The type of key for the map.
    //		_DataType - The type of the data in the map
    //		atMapAddr - The address of the atMap (an atMap<K,D>*)
    // NOTE:
    //      make sure to DELETE the iterator once you are done with it
    template<typename _KeyType, typename _DataType>
    static parMemberMapIterator* CreateIteratorAtMap(parMemberMap&, void* atMapAddr);

    // PURPOSE: A function that creates an iterator for an atBinaryMap
    // PARAMS:
    //		_KeyType - The type of key for the map.
    //		_DataType - The type of the data in the map
    //		atBinaryMapAddr - The address of the atBinaryMap (an atBinaryMap<D,K>*)
    // NOTE:
    //      make sure to DELETE the iterator once you are done with it
    parMemberMapIterator* CreateIteratorAtBinaryMap(parMemberMap&, void* atBinaryMapAddr);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// parMemberMapInterfaces ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename _KeyType, typename _DataType>
parMemberMapInterface* parMemberMapInterfaces::CreateInterfaceAtMap(parMemberMap&, void* atMapAddr)
{
    parAssertf(atMapAddr, "CreateInterfaceAtMap atMapAddr is invalid param.");

    typedef atMap<_KeyType, _DataType> MapType;
    MapType& map = *reinterpret_cast<MapType*>(atMapAddr);
    return rage_new parMemberMapInterface_atMap<_KeyType, _DataType>(map);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// parMemberMapIterators ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename _KeyType, typename _DataType>
parMemberMapIterator* parMemberMapIterators::CreateIteratorAtMap(parMemberMap&, void* atMapAddr)
{
    parAssertf(atMapAddr, "CreateIteratorAtMap atMapAddr is invalid param.");

    typedef atMap<_KeyType, _DataType> MapType;
    MapType& map = *reinterpret_cast<MapType*>(atMapAddr);
    return rage_new parMemberMapIterator_atMap<_KeyType, _DataType>(map);
}

} // namespace rage

#endif // PARSER_MEMBERMAPDATA_H 
