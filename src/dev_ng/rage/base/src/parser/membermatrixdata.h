// 
// parser/membermatrixdata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MEMBERMATRIXDATA_H 
#define PARSER_MEMBERMATRIXDATA_H 

#include "memberdata.h"

#include "parser/macros.h"
#include "data/callback.h"

namespace rage {

namespace parMemberMatrixSubType
{
	enum Enum
	{
		// no subtypes
	};
}


// PURPOSE: All of the data that a parMemberSimple needs to describe the member.
struct parMemberMatrixData {
	enum TypeFlags
	{
		FLAG_HIGH_PRECISION
	};
	
	STANDARD_PARMEMBER_DATA_CONTENTS;

	 // Initial values
	float m_InitAX, m_InitAY, m_InitAZ, m_InitAW,
		m_InitBX, m_InitBY, m_InitBZ, m_InitBW,
		m_InitCX, m_InitCY, m_InitCZ, m_InitCW,
		m_InitDX, m_InitDY, m_InitDZ, m_InitDW;
#if __BANK
	const char* m_Description;		// Text description of the member. Used for tooltips in the bank
	float m_Min;					// Minimum value of the member. For vectors, the minimum for all components.
	float m_Max;					// Maximum value of the member. For vectors, the maximum for all components.
	float m_Step;					// The smallest delta when manipulating the widgets.
	datCallback* m_WidgetCb;		// Called whenever the widget changes
#endif
	void Init();
	void PreLoad(parTreeNode*) {Init();}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_SIMPLE_PARSABLE;
#endif
};

} // namespace rage

#endif // PARSER_MEMBERMATRIXDATA_H 
