// 
// parser/psodata.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psodata.h"

#include "manager.h"
#include "memberarraydata.h"
#include "memberenumdata.h"
#include "membermapdata.h"
#include "memberstructdata.h"
#include "optimisations.h"
#include "psofile.h"

#include "atl/binmap.h"
#include "atl/bitset.h"
#include "atl/hashstring.h"
#include "atl/string.h"
#include "atl/wstring.h"
#include "math/simplemath.h"
#include "system/endian.h"
#include "string/string.h"

PARSER_OPTIMISATIONS();


using namespace rage;

void psoIffToRscConversion::ConvertToNewFormat(const psoStructureMapData& map, psoStructureData* startOfInstanceData, psoResourceData& newData, char sourcePlatform)
{
	newData.CreateStructArrayTable(map.m_NumStructArrays);
	for(int i = 0; i < newData.m_NumStructArrayEntries; i++)
	{
		psoRscStructArrayTableData& newEntry = newData.m_StructArrayTable[i];
		const psoStructArrayTableData& oldEntry = map.GetStructArrayEntry(i);
		
		if (psoConstants::IsParMemberType(oldEntry.m_TypeHash))
		{
			psoType newType = psoType::ConvertOldStructArrayTypeToPsoType(oldEntry.m_TypeHash, sysIs64Bit(sourcePlatform));
			newEntry.m_NameHash.SetHash(newType.GetEnum());
		}
		else
		{
			newEntry.m_NameHash.SetHash(oldEntry.m_TypeHash);
		}
		newEntry.m_Size = oldEntry.m_Size;
		newEntry.m_Data = reinterpret_cast<char*>(startOfInstanceData) + oldEntry.m_Offset;
	}
}

psoMemberArrayInterface::psoMemberArrayInterface(psoStruct& str, psoMember& mem)
: m_Member(mem)
{
	FastAssert(mem.GetType().IsArray());
	m_MembersStructure = str.GetSchema();
	m_ElementSchema = str.GetSchema().GetMemberByIndex(m_Member.GetSchema().GetArrayElementSchemaIndex());
	m_ElementSize = str.GetFile().GetSchemaCatalog().FindMemberSize(str.GetSchema(), m_ElementSchema);
}

size_t psoMemberArrayInterface::GetArrayCount()
{
	using namespace parMemberArraySubType;

	FastAssert(m_Member.GetType().IsArray());

	switch(m_Member.GetType().GetEnum())
	{
	case psoType::TYPE_ATARRAY32:
		{
			return m_Member.GetDataAs<psoFake32::AtArray16>().m_Count;
		}
	case psoType::TYPE_ATARRAY64:
		{
			return m_Member.GetDataAs<psoFake64::AtArray16>().m_Count;
		}

	case psoType::TYPE_ATFIXEDARRAY:
		{
			// Find the max size of the array. The count element is stored just past that. 
			u32 arraySize = (u32)m_ElementSize * m_Member.GetSchema().GetFixedArrayCount();

			arraySize = RoundUp<4>(arraySize);

			u32* countElt = reinterpret_cast<u32*>(m_Member.GetRawDataPtr() + arraySize);
			return *countElt;
		}

	case psoType::TYPE_ARRAY_POINTER32:
		{
			psoFake32::VoidPtr& id = m_Member.GetDataAs<psoFake32::VoidPtr>();
			// if id exists, we have an array of N elements. Otherwise it's a NULL pointer
			return id.m_StructId.IsNull() ? 0 : m_Member.GetSchema().GetFixedArrayCount();
		}

	case psoType::TYPE_ARRAY_POINTER64:
		{
			psoFake64::VoidPtr& id = m_Member.GetDataAs<psoFake64::VoidPtr>();
			// if id exists, we have an array of N elements. Otherwise it's a NULL pointer
			return id.m_StructId.IsNull() ? 0 : m_Member.GetSchema().GetFixedArrayCount();
		}

	case psoType::TYPE_ARRAY_MEMBER:
		return m_Member.GetSchema().GetFixedArrayCount();

	case psoType::TYPE_ATARRAY32_32BITIDX:
		{
			return m_Member.GetDataAs<psoFake32::AtArray32>().m_Count;
		}
	case psoType::TYPE_ATARRAY64_32BITIDX:
		{
			return m_Member.GetDataAs<psoFake64::AtArray32>().m_Count;
		}

	case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
	case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
		// The "array count" actually holds the offset of the count variable.
		{
			// Get the address of the beginning of the structure - by taking the member address and subtracting the member's offset
			char* structAddr = reinterpret_cast<char*>(m_Member.GetRawDataPtr()) - m_Member.GetSchema().GetOffset();

			psoMemberSchema counterSchema = m_MembersStructure.GetMemberByIndex(m_Member.GetSchema().GetArrayCounterSchemaIndex());
			ptrdiff_t counterOffset = counterSchema.GetOffset();

			switch(counterSchema.GetType().GetEnum())
			{
			case psoType::TYPE_U8: return *reinterpret_cast<u8*>(structAddr + counterOffset);
			case psoType::TYPE_U16: return *reinterpret_cast<u16*>(structAddr + counterOffset);
			case psoType::TYPE_U32: return *reinterpret_cast<u32*>(structAddr + counterOffset);
			default:
				counterSchema.GetType().PrintUnexpectedTypeError("getting array count", "counter type");
				return 0;
			}
		}

	default:
		m_Member.GetType().PrintUnexpectedTypeError("GetArrayCount", "array");
	}
	return 0;
}

parPtrToArray psoMemberArrayInterface::GetArrayContents()
{
	using namespace parMemberArraySubType;

	FastAssert(m_Member.GetType().IsArray());

	switch(m_Member.GetType().GetEnum())
	{
	case psoType::TYPE_ATARRAY32:
		{
			psoStructId id = m_Member.GetDataAs<psoFake32::AtArray16>().m_Elements.m_StructId;
			psoStruct arrayInst = m_Member.GetFile().GetInstance(id);
			return reinterpret_cast<parPtrToArray>(arrayInst.GetInstanceData());
		}

	case psoType::TYPE_ATARRAY64:
		{
			psoStructId id = m_Member.GetDataAs<psoFake64::AtArray16>().m_Elements.m_StructId;
			psoStruct arrayInst = m_Member.GetFile().GetInstance(id);
			return reinterpret_cast<parPtrToArray>(arrayInst.GetInstanceData());
		}

	case psoType::TYPE_ARRAY_MEMBER:
	case psoType::TYPE_ATFIXEDARRAY:
		return reinterpret_cast<parPtrToArray>(m_Member.GetRawDataPtr());


	case psoType::TYPE_ARRAY_POINTER32:
	case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
		{
			psoStructId id = m_Member.GetDataAs<psoFake32::VoidPtr>().m_StructId;
			psoStruct arrayInst = m_Member.GetFile().GetInstance(id);
			return reinterpret_cast<parPtrToArray>(arrayInst.GetInstanceData());
		}

	case psoType::TYPE_ARRAY_POINTER64:
	case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
		{
			psoStructId id = m_Member.GetDataAs<psoFake64::VoidPtr>().m_StructId;
			psoStruct arrayInst = m_Member.GetFile().GetInstance(id);
			return reinterpret_cast<parPtrToArray>(arrayInst.GetInstanceData());
		}

	case psoType::TYPE_ATARRAY32_32BITIDX:
		{
			psoStructId id = m_Member.GetDataAs<psoFake32::AtArray32>().m_Elements.m_StructId;
			psoStruct arrayInst = m_Member.GetFile().GetInstance(id);
			return reinterpret_cast<parPtrToArray>(arrayInst.GetInstanceData());
		}

	case psoType::TYPE_ATARRAY64_32BITIDX:
		{
			psoStructId id = m_Member.GetDataAs<psoFake64::AtArray32>().m_Elements.m_StructId;
			psoStruct arrayInst = m_Member.GetFile().GetInstance(id);
			return reinterpret_cast<parPtrToArray>(arrayInst.GetInstanceData());
		}

	default:
		m_Member.GetType().PrintUnexpectedTypeError("GetElement", "array");
	}
	return NULL;
}


psoMember psoMemberArrayInterface::GetElement(size_t i)
{
	psoMemberSchema eltSchema = GetArrayElementSchema();
	parPtrToArray arrayContents = GetArrayContents();
	if (arrayContents)
	{
		char* elementAddr = reinterpret_cast<char*>(arrayContents) + m_ElementSize * i;
		return psoMember(eltSchema, reinterpret_cast<parPtrToMember>(elementAddr), &m_Member.GetFile());
	}
	else
	{
		return psoMember(eltSchema, NULL, NULL);
	}
}


psoMemberMapInterface::psoMemberMapInterface( psoStruct& str, psoMember& mem )
: m_Member(mem)
{
	// The structure has key,value pairs in it, but we can't us 'em (hmm why do I put them there?)
	// We have to go fetch the structId for the map contents array, get the schema from that, and then we're all set.
	FastAssert(mem.GetType().IsMap());
	
	switch(mem.GetType().GetEnum())
	{
	case psoType::TYPE_ATBINARYMAP32:
		{
			psoFake32::AtBinMap& binmap = m_Member.GetDataAs<psoFake32::AtBinMap>();
			int itemCount = binmap.m_Data.m_Count;
			if (itemCount > 0)
			{
				psoStructId elementsId = binmap.m_Data.m_Elements.m_StructId;
				psoStruct elements = str.GetFile().GetInstance(elementsId);

				parAssertf(elements.IsValid(), "Couldn't get a valid array of map elements");

				psoStructureSchema eltSchema = elements.GetSchema();
				m_KeySchema = eltSchema.GetMemberByIndex(0);
				m_ValueSchema = eltSchema.GetMemberByIndex(1);
				m_ElementSize = eltSchema.GetSize();
			}
		}
		break;
	case psoType::TYPE_ATBINARYMAP64:
		{
			psoFake64::AtBinMap& binmap = m_Member.GetDataAs<psoFake64::AtBinMap>();
			int itemCount = binmap.m_Data.m_Count;
			if (itemCount > 0)
			{
				psoStructId elementsId = binmap.m_Data.m_Elements.m_StructId;
				psoStruct elements = str.GetFile().GetInstance(elementsId);

				parAssertf(elements.IsValid(), "Couldn't get a valid array of map elements");

				psoStructureSchema eltSchema = elements.GetSchema();
				m_KeySchema = eltSchema.GetMemberByIndex(0);
				m_ValueSchema = eltSchema.GetMemberByIndex(1);
				m_ElementSize = eltSchema.GetSize();
			}
		}
		break;
	default:
		mem.GetType().PrintUnexpectedTypeError("MapInterface", "map");
	}
}

size_t psoMemberMapInterface::GetMapCount()
{
	switch(m_Member.GetType().GetEnum())
	{
	case psoType::TYPE_ATBINARYMAP32:
		{
			psoFake32::AtBinMap& binmap = m_Member.GetDataAs<psoFake32::AtBinMap>();
			return binmap.m_Data.m_Count;
		}
	case psoType::TYPE_ATBINARYMAP64:
		{
			psoFake64::AtBinMap& binmap = m_Member.GetDataAs<psoFake64::AtBinMap>();
			return binmap.m_Data.m_Count;
		}
	default:
		m_Member.GetType().PrintUnexpectedTypeError("MapCount", "map");
	}
	return 0;
}

psoStructId psoMemberMapInterface::GetIdForElement( size_t i )
{
	parAssertf(i < GetMapCount(), "Invalid count %" SIZETFMT "d for map with %" SIZETFMT "d items", i, GetMapCount());
	switch(m_Member.GetType().GetEnum())
	{
	case psoType::TYPE_ATBINARYMAP32:
		{
			psoFake32::AtBinMap& binmap = m_Member.GetDataAs<psoFake32::AtBinMap>();
			psoStructId elementsId = binmap.m_Data.m_Elements.m_StructId;

			// increment the element offset to get the ith member
			return psoStructId::Create(
				elementsId.GetTableIndex(),
				u32(elementsId.GetArrayOffset() + i * m_ElementSize));
		}
	case psoType::TYPE_ATBINARYMAP64:
		{
			psoFake64::AtBinMap& binmap = m_Member.GetDataAs<psoFake64::AtBinMap>();
			psoStructId elementsId = binmap.m_Data.m_Elements.m_StructId;

			// increment the element offset to get the ith member
			return psoStructId::Create(
				elementsId.GetTableIndex(),
				u32(elementsId.GetArrayOffset() + i * m_ElementSize));
		}
	default:
		m_Member.GetType().PrintUnexpectedTypeError("GetIdForElement", "map");
	}
	return psoStructId::Null();
}


psoMember psoMemberMapInterface::GetKey( size_t i )
{
	psoStructId elementId = GetIdForElement(i);
	psoStruct element = m_Member.GetFile().GetInstance(elementId);
	return element.GetMemberByIndex(0);
}

psoMember psoMemberMapInterface::GetValue( size_t i )
{
	psoStructId elementId = GetIdForElement(i);
	psoStruct element = m_Member.GetFile().GetInstance(elementId);
	return element.GetMemberByIndex(1);
}

psoStruct psoMemberMapInterface::GetKeyValueStruct( size_t i )
{
	psoStructId elementId = GetIdForElement(i);
	return m_Member.GetFile().GetInstance(elementId);
}

psoMemberBitsetInterface::psoMemberBitsetInterface(psoStruct& str, psoMember& mem)
	: m_Member(mem)
{
	atLiteralHashValue enumHash = m_Member.GetSchema().GetBitsetEnumHash();
	if (enumHash.IsNull())
	{
		// don't change m_Enums
	}
	else
	{
		m_Enums = str.GetFile().GetSchemaCatalog().FindEnumSchema(enumHash);
	}
}

size_t psoMemberBitsetInterface::GetBitCount()
{
	switch(m_Member.GetType().GetEnum())
	{
	case psoType::TYPE_BITSET8:
	case psoType::TYPE_BITSET16:
	case psoType::TYPE_BITSET32:
		return m_Member.GetSchema().GetBitsetCount();
	case psoType::TYPE_ATBITSET32:
		{
			psoFake32::AtBitset& bits = m_Member.GetDataAs<psoFake32::AtBitset>();
			return bits.m_BitSize;
		}
	case psoType::TYPE_ATBITSET64:
		{
			psoFake64::AtBitset& bits = m_Member.GetDataAs<psoFake64::AtBitset>();
			return bits.m_BitSize;
		}
	default:
		m_Member.GetType().PrintUnexpectedTypeError("GetBitCount", "bitset");
	}
	return 0;
}

typedef atFixedBitSet<USHRT_MAX, u8> MaxFixedBitset8;
typedef atFixedBitSet<USHRT_MAX, u16> MaxFixedBitset16;
typedef atFixedBitSet<USHRT_MAX, u32> MaxFixedBitset32;

void psoMemberBitsetInterface::SetBit(size_t i, bool newValue)
{
	FastAssert(i < GetBitCount());
	u32 bitIndex = (u32)i;

	switch(m_Member.GetType().GetEnum())
	{
	case psoType::TYPE_BITSET8:
		m_Member.GetDataAs<MaxFixedBitset8>().Set(bitIndex, newValue);
		break;
	case psoType::TYPE_BITSET16:
		m_Member.GetDataAs<MaxFixedBitset16>().Set(bitIndex, newValue);
		break;
	case psoType::TYPE_BITSET32:
		m_Member.GetDataAs<MaxFixedBitset32>().Set(bitIndex, newValue);
		break;
	case psoType::TYPE_ATBITSET32:
		{
			// Can't use GetDataAs here because there's a pointer we need to deal with
			u32 blockIdx = (u32)i >> 5;
			u32 bitInBlock = (u32)i & ((1 << 5) - 1);
			psoFake32::AtBitset& bitset = m_Member.GetDataAs<psoFake32::AtBitset>();
			u32* blocks = m_Member.GetFile().GetInstanceDataAs<u32*>(bitset.m_Bits.m_StructId);
			blocks[blockIdx] ^= (u32)(newValue << bitInBlock);
		}
		break;
	case psoType::TYPE_ATBITSET64:
		{
			// Can't use GetDataAs here because there's a pointer we need to deal with
			u32 blockIdx = (u32)i >> 5;
			u32 bitInBlock = (u32)i & ((1 << 5) - 1);
			psoFake64::AtBitset& bitset = m_Member.GetDataAs<psoFake64::AtBitset>();
			u32* blocks = m_Member.GetFile().GetInstanceDataAs<u32*>(bitset.m_Bits.m_StructId);
			blocks[blockIdx] ^= (u32)(newValue << bitInBlock);
		}
		break;
	default:
		m_Member.GetType().PrintUnexpectedTypeError("SetBit", "bitset");
	}
}

bool psoMemberBitsetInterface::GetBit(size_t i)
{
	FastAssert(i < GetBitCount());
	u32 bitIndex = (u32)i;

	switch(m_Member.GetType().GetEnum())
	{
	case psoType::TYPE_BITSET8:
		return m_Member.GetDataAs<MaxFixedBitset8>().IsSet(bitIndex);
	case psoType::TYPE_BITSET16:
		return m_Member.GetDataAs<MaxFixedBitset16>().IsSet(bitIndex);
	case psoType::TYPE_BITSET32:
		return m_Member.GetDataAs<MaxFixedBitset32>().IsSet(bitIndex);
	case psoType::TYPE_ATBITSET32:
		{
			// Can't use GetDataAs here because there's a pointer we need to deal with
			u32 blockIdx = (u32)i >> 5;
			u32 bitInBlock = (u32)i & ((1 << 5) - 1);
			psoFake32::AtBitset& bitset = m_Member.GetDataAs<psoFake32::AtBitset>();
			u32* blocks = m_Member.GetFile().GetInstanceDataAs<u32*>(bitset.m_Bits.m_StructId);
			return (blocks[blockIdx] & (1 << bitInBlock)) != 0;
		}
	case psoType::TYPE_ATBITSET64:
		{
			// Can't use GetDataAs here because there's a pointer we need to deal with
			u32 blockIdx = (u32)i >> 5;
			u32 bitInBlock = (u32)i & ((1 << 5) - 1);
			psoFake64::AtBitset& bitset = m_Member.GetDataAs<psoFake64::AtBitset>();
			u32* blocks = m_Member.GetFile().GetInstanceDataAs<u32*>(bitset.m_Bits.m_StructId);
			return (blocks[blockIdx] & (1 << bitInBlock)) != 0;
		}
	default:
		m_Member.GetType().PrintUnexpectedTypeError("GetBit", "bitset");
	}
	return false;
}

atLiteralHashValue psoMemberBitsetInterface::GetBitName(size_t i)
{
	atLiteralHashValue hash;
	if (m_Enums.IsValid())
	{
		m_Enums.NameFromValue((int)i, hash);
	}
	return hash;
}

psoStruct psoMember::GetSubStructure()
{
	FastAssert(GetType().HasSubStructure());

	switch(GetType().GetEnum())
	{
	case psoType::TYPE_STRUCT:
		{
			psoStruct ret(GetFile());
			ret.m_Schema = GetReferentStructure();
			ret.m_InstanceData = reinterpret_cast<parPtrToStructure>(GetRawDataPtr());
			return ret;
		}
	case psoType::TYPE_POINTER32:
		{
			psoStructId id = GetDataAs<psoFake32::VoidPtr>().m_StructId;
			if (id.IsNull())
			{
				psoStruct ret(GetFile());
				ret.m_InstanceData = NULL;
				return ret;
			}
			else
			{
				return GetFile().GetInstance(id);
			}
		}
	case psoType::TYPE_POINTER64:
		{
			psoStructId id = GetDataAs<psoFake64::VoidPtr>().m_StructId;
			if (id.IsNull())
			{
				psoStruct ret(GetFile());
				ret.m_InstanceData = NULL;
				return ret;
			}
			else
			{
				return GetFile().GetInstance(id);
			}
		}

	default:
		GetType().PrintUnexpectedTypeError("GetSubStructure", "struct or pointer");
	}

	return psoStruct(GetFile());
}

psoMember rage::psoStruct::GetMemberByIndex( int i )
{
	FastAssert(IsValid());

	psoMemberSchema schemaMember = m_Schema.GetMemberByIndex(i);
	char* memberAddress = reinterpret_cast<char*>(m_InstanceData) + schemaMember.GetOffset();
	return psoMember(schemaMember, reinterpret_cast<parPtrToMember>(memberAddress), m_File);
}

psoMember psoStruct::GetMemberByName(atLiteralHashValue name)
{
	FastAssert(IsValid());

	psoMemberSchema schemaMember = m_Schema.GetMemberByName(name);
	if (schemaMember.IsValid())
	{
		return psoMember(schemaMember, reinterpret_cast<parPtrToMember>(reinterpret_cast<char*>(m_InstanceData) + schemaMember.GetOffset()), m_File);
	}

	return psoMember(psoMemberSchema(), NULL, NULL);
}

rage::psoStructureSchema rage::psoMember::GetReferentStructure()
{
	return m_File->GetSchemaCatalog().FindStructureSchema(GetSchema().GetReferentHash());
}

rage::psoEnumSchema rage::psoMember::GetReferentEnum()
{
	return m_File->GetSchemaCatalog().FindEnumSchema(GetSchema().GetReferentHash());
}

const char* rage::psoMember::GetStringData()
{
	switch(GetType().GetEnum())
	{
	case psoType::TYPE_STRING_MEMBER:
		{
			return &GetDataAs<char>();
		}
	case psoType::TYPE_STRING_POINTER32:
		{
			psoFake32::CharPtr& fakeString = GetDataAs<psoFake32::CharPtr>();
			return GetFile().GetInstanceDataAs<char*>(fakeString.m_StructId);
		}
	case psoType::TYPE_STRING_POINTER64:
		{
			psoFake64::CharPtr& fakeString = GetDataAs<psoFake64::CharPtr>();
			return GetFile().GetInstanceDataAs<char*>(fakeString.m_StructId);
		}
	case psoType::TYPE_ATSTRING32:
		{
			psoFake32::AtString& fakeString = GetDataAs<psoFake32::AtString>();
			return GetFile().GetInstanceDataAs<char*>(fakeString.m_Data.m_StructId);
		}
	case psoType::TYPE_ATSTRING64:
		{
			psoFake64::AtString& fakeString = GetDataAs<psoFake64::AtString>();
			return GetFile().GetInstanceDataAs<char*>(fakeString.m_Data.m_StructId);
		}
	case psoType::TYPE_STRINGHASH:
		{
			return parUtils::FindStringFromHash(GetDataAs<atHashValue>());
		}
	case psoType::TYPE_NSSTRINGHASH:
		{
			atNamespacedHashStringBase hash = GetDataAs<atNamespacedHashStringBase>();
			return atHashStringNamespaceSupport::TryGetString((atHashStringNamespaces)GetSchema().GetStringNamespaceIndex(), hash.GetHash());
		}
	default:
		GetType().PrintUnexpectedTypeError("Reading string value", "string type");
		return NULL;
	}
}

const char16* rage::psoMember::GetWideStringData()
{
	switch(GetType().GetEnum())
	{
	case psoType::TYPE_WIDE_STRING_MEMBER:
		{
			return &GetDataAs<char16>();
		}
	case psoType::TYPE_WIDE_STRING_POINTER32:
		{
			psoFake32::CharPtr& fakeString = GetDataAs<psoFake32::CharPtr>();
			return GetFile().GetInstanceDataAs<char16*>(fakeString.m_StructId);
		}
	case psoType::TYPE_WIDE_STRING_POINTER64:
		{
			psoFake64::CharPtr& fakeString = GetDataAs<psoFake64::CharPtr>();
			return GetFile().GetInstanceDataAs<char16*>(fakeString.m_StructId);
		}
	case psoType::TYPE_ATWIDESTRING32:
		{
			psoFake32::AtString& fakeString = GetDataAs<psoFake32::AtString>();
			return GetFile().GetInstanceDataAs<char16*>(fakeString.m_Data.m_StructId);
		}
	case psoType::TYPE_ATWIDESTRING64:
		{
			psoFake64::AtString& fakeString = GetDataAs<psoFake64::AtString>();
			return GetFile().GetInstanceDataAs<char16*>(fakeString.m_Data.m_StructId);
		}
	default:
		GetType().PrintUnexpectedTypeError("Reading string value", "string type");
		return NULL;
	}
}

void psoMember::SetStringData(const char* value)
{
	switch(GetType().GetEnum())
	{
	case psoType::TYPE_STRING_MEMBER:
		{
			char* ptr = &GetDataAs<char>();

			safecpy(ptr, value, GetSchema().GetStringCount());
		}
		break;
	default:
		GetType().PrintUnexpectedTypeError("SetStringData", "member string");
	}
}

bool psoMember::GetEnumValueAndName(int& outVal, atLiteralHashValue& outName)
{
	FastAssert(GetType().IsEnum());

	// Get the file value, convert to the file namehash
	int filevalue = 0;
	switch(GetType().GetEnum())
	{
	case psoType::TYPE_ENUM32:
		filevalue = GetDataAs<s32>();
		break;
	case psoType::TYPE_ENUM16:
		filevalue = GetDataAs<s16>();
		break;
	case psoType::TYPE_ENUM8:
		filevalue = GetDataAs<s8>();
		break;
	default:
		GetType().PrintUnexpectedTypeError("GetEnumValueAndName", "enum");
	}

	outVal = filevalue;

	psoEnumSchema fileEnum = GetReferentEnum();

	atLiteralHashValue namehash;

	if (fileEnum.IsValid())
	{
		return fileEnum.NameFromValue(filevalue, outName);
	}

	return false;
}

