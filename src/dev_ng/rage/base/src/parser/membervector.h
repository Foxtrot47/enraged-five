// 
// parser/membervector.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBERVECTOR_H
#define PARSER_MEMBERVECTOR_H

#include "member.h"

#include "membervectordata.h"

namespace rage {

// PURPOSE: A parMember subclass for describing vector data types
class parMemberVector : public parMember
{
public:
	typedef parMemberVectorSubType::Enum SubType;
	typedef parMemberVectorData Data;

	parMemberVector() {}
	explicit parMemberVector(Data& data) : parMember(&data) {}
	virtual ~parMemberVector() {}

	virtual void ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const;

	virtual size_t GetSize() const;

public:

	STANDARD_PARMEMBER_DATA_FUNCS_DECL(parMemberVector);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_PARSABLE;
#endif
};

// This can't be defined until the derived class is.
inline parMemberVector*	parMember::AsVector()			{ return parMemberType::IsVector(GetType()) ? smart_cast<parMemberVector*>(this) : NULL; }

}

#endif
