// 
// parser/memberstructdata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MEMBERSTRUCTDATA_H 
#define PARSER_MEMBERSTRUCTDATA_H 

#include "memberdata.h"

#include "parser/macros.h"
#include "data/callback.h"

namespace rage {

namespace parMemberStructSubType
{
	enum Enum
	{
		SUBTYPE_STRUCTURE,
		SUBTYPE_EXTERNAL_NAMED_POINTER, // pointer to external data. Uses NameToPtr and PtrToName for conversion
		SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL, // pointer to external data, User handles the 'null' case
		SUBTYPE_POINTER,
		SUBTYPE_SIMPLE_POINTER, // can't derive
	};
}
// PURPOSE: All of the data that a parMemberStruct needs to describe the member.
struct parMemberStructData
{
	STANDARD_PARMEMBER_DATA_CONTENTS;
#if __BANK
	const char* m_Description;			// Text description of the member. Used for tooltips in the bank
#endif
	parStructure* m_StructurePtr;		// Pointer to the parStructure metadata for the base class.
	void* (*m_NameToPtrCB)(const char*);
	const char* (*m_PtrToNameCB)(const void*);
	void* (*m_UnknownTypeCB)(parTreeNode* data);

	void Init();
	void PreLoad(parTreeNode*) {Init();}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_SIMPLE_PARSABLE;
#endif
};

} // namespace rage

#endif // PARSER_MEMBERSTRUCTDATA_H 
