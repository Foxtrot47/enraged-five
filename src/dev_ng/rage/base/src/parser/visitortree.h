// 
// parser/visitortree.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_VISITORTREE_H
#define PARSER_VISITORTREE_H

#include "tree.h"
#include "visitor.h"

namespace rage
{

// PURPOSE: A visitor that can determine whether or not we need to build
// a parTree to do output or if stream-based output will suffice.
class parNeedsTreeForOutputVisitor : public parInstanceVisitor
{
public:
	parNeedsTreeForOutputVisitor()
		: m_Result(false)
	{
	}

	virtual ~parNeedsTreeForOutputVisitor() {}

	virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata);

	bool m_Result;
};

// PURPOSE: A visitor for building a parTree given a parsable object
class parBuildTreeVisitor : public parInstanceVisitor
{
public:
	parBuildTreeVisitor(parSettings settings)
		: m_CurrContainer(NULL)
		, m_TopLevelName(NULL)
		, m_Settings(settings)
	{
	}

	virtual ~parBuildTreeVisitor() {}

	virtual void VisitMember(parPtrToStructure containingStructPtr, parMember& metadata);

	virtual void SimpleMember(parPtrToMember dataPtr, parMemberSimple& metadata);
	virtual void VecBoolVMember(VecBoolV& data, parMemberVector& metadata);
	virtual void Vec2VMember(Vec2V& data, parMemberVector& metadata);
	virtual void Vec3VMember(Vec3V& data, parMemberVector& metadata);
	virtual void Vec4VMember(Vec4V& data, parMemberVector& metadata);
	virtual void Vector2Member(Vector2& data, parMemberVector& metadata);
	virtual void Vector3Member(Vector3& data, parMemberVector& metadata);
	virtual void Vector4Member(Vector4& data, parMemberVector& metadata);
	virtual void Mat33VMember(Mat33V& data, parMemberMatrix& metadata);
	virtual void Mat34VMember(Mat34V& data, parMemberMatrix& metadata);
	virtual void Mat44VMember(Mat44V& data, parMemberMatrix& metadata);
	virtual void Matrix34Member(Matrix34& data, parMemberMatrix& metadata);
	virtual void Matrix44Member(Matrix44& data, parMemberMatrix& metadata);

	virtual void EnumMember(int& data, parMemberEnum& metadata);
	virtual void BitsetMember(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata);
	virtual void StringMember(parPtrToMember ptrToMember, const char* ptrToString, parMemberString& metadata);
	virtual void WideStringMember(parPtrToMember ptrToMember, const char16* ptrToString, parMemberString& metadata);

	virtual void AtHashValueMember(atHashValue& stringData, parMemberString& metadata);
	virtual void AtPartialHashValueMember(u32& stringData, parMemberString& metadata);

	virtual void ExternalPointerMember(void*& data, parMemberStruct& metadata);

	virtual bool BeginStructMember(parPtrToStructure structAddr, parMemberStruct& metadata);
	virtual void EndStructMember(parPtrToStructure structAddr, parMemberStruct& metadata);

	virtual bool BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);
	virtual void EndPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata);

	virtual bool BeginArrayMember(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);
	virtual void EndArrayMember(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata);

    virtual bool BeginMapMember(parPtrToStructure structAddr, parMemberMap& metadata);
    virtual void EndMapMember(parPtrToStructure structAddr, parMemberMap& metadata);
    virtual void VisitMapMember(parPtrToStructure structAddr, parPtrToStructure mapKeyAddress, parPtrToStructure mapDataAddress, parMemberMap& metadata);

	virtual bool BeginToplevelStruct(parPtrToStructure ptrToStruct, parStructure& metadata);
	virtual void EndToplevelStruct(parPtrToStructure ptrToStruct, parStructure& metadata);

	virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata);

	parTreeNode* m_CurrContainer;
	parTree m_Tree;
	const char* m_TopLevelName; // Set to non-null if you want the top level structure to look like <Item type="Foo"> instead of <Foo>
	parSettings m_Settings;
#if RSG_ASSERT
	static bool sm_AssertOnUnsavableTypes;
#endif
};

}

#endif
