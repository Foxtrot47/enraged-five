// 
// parser/psotypes.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "psotypes.h"

#include "memberarraydata.h"
#include "memberenumdata.h"
#include "membermapdata.h"
#include "memberstringdata.h"
#include "memberstructdata.h"
#include "psofaketypes.h"

#include "math/float16.h"
#include "system/bit.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/boolv.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"
#include "vectormath/scalarv.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"

using namespace rage;

namespace psoTypeTraits
{
enum Flags
{
	USED		= BIT0,	// Flags will be 0 initted, so this lets us know if the table entry corresponds to a real type or not
	POD			= BIT1,
	MAP_KEY		= BIT2,
	PTR32		= BIT3,	// Does this type contain 32-bit pointers
	PTR64		= BIT4,	// Does this type contain 64-bit pointers
	NON_MEMBER	= BIT5,	// Not allowed as a member of a PSO struct
	SA			= BIT6, // Can we have a StructArray of this type?
};

struct Traits
{

	u8 m_Size, m_Align;
	u16 m_Flags;
	OUTPUT_ONLY(const char* m_Name);
};

#if !__NO_OUTPUT

// Regular item - specify everything
#define ITEM(name, size, align, flags) { size, align, flags | USED, name }

// Specify a name and type, derive some traits from the type
#define ITEM_NT(name, type, flags) { sizeof(type), __alignof(type), flags | USED, name }

#else
#define ITEM(name, size, align, flags) { size, align, flags | USED }
#define ITEM_NT(name, type, flags) { sizeof(type), __alignof(type), flags | USED }
#endif

Traits g_PsoTypeTraits[psoType::MAX_CATEGORY][psoType::TYPES_PER_CATEGORY] = {
	{ // CATEGORY_CORE
		/*00 TYPE_INVALID */	ITEM("INVALID",		0, 0,					NON_MEMBER),
		/*01 TYPE_BOOL */		ITEM_NT("BOOL",		bool,					POD | SA),
		/*02 TYPE_BOOLV */		ITEM_NT("BOOLV",	BoolV,					POD | SA),
		/*03 TYPE_VECBOOLV */	ITEM_NT("VECBOOLV",	VecBoolV,				POD | SA),
		/*04 TYPE_STRUCTID */	ITEM("STRUCTID",	4, 4,					NON_MEMBER | SA),
		/*05 TYPE_STRUCT */		ITEM("STRUCT",		0, 0,					0),
		/*06 TYPE_POINTER32 */	ITEM_NT("POINTER_32",	psoFake32::VoidPtr,		PTR32 | SA),
		/*07 TYPE_POINTER64 */	ITEM_NT("POINTER_64",	psoFake64::VoidPtr,		PTR64 | SA),
	},

	{ // CATEGORY_INTEGER
		/*10 TYPE_S8 */			ITEM_NT("S8",	s8,					POD | MAP_KEY | SA),
		/*11 TYPE_U8 */			ITEM_NT("U8",	u8,					POD | MAP_KEY | SA),
		/*12 TYPE_S16 */		ITEM_NT("S16",	s16,				POD | MAP_KEY | SA),
		/*13 TYPE_U16 */		ITEM_NT("U16",	u16,				POD | MAP_KEY | SA),
		/*14 TYPE_S32 */		ITEM_NT("S32",	s32,				POD | MAP_KEY | SA),
		/*15 TYPE_U32 */		ITEM_NT("U32",	u32,				POD | MAP_KEY | SA),
		/*16 TYPE_S64 */		ITEM_NT("S64",	s64,				POD | MAP_KEY | SA),
		/*17 TYPE_U64  */		ITEM_NT("U64",	u64,				POD | MAP_KEY | SA),
		/*18 TYPE_S128 */		// TBD
		/*19 TYPE_U128 */		// TBD
	},

	{ // CATEGORY_FLOAT
		/*20 TYPE_FLOAT16 */	ITEM_NT("FLOAT16",	Float16,		POD | SA),
		/*21 TYPE_FLOAT	*/		ITEM_NT("FLOAT",	float,			POD | SA),
		/*23 TYPE_SCALARV */	ITEM_NT("SCALARV",	ScalarV,		POD | SA),
		/*22 TYPE_DOUBLE */		ITEM_NT("DOUBLE",	double,			POD | SA),
	},

	{ // CATEGORY_VECTOR
		/*30 TYPE_VEC2 */		ITEM_NT("VEC2",		Vec::Vector_2,	POD | SA),
		/*31 TYPE_VEC2V, */		ITEM_NT("VEC2V",	Vec2V,			POD | SA),
		/*32 TYPE_VEC3, */		ITEM_NT("VEC3",		Vec::Vector_3,	POD | SA),
		/*33 TYPE_VEC3V, */		ITEM_NT("VEC3V",	Vec3V,			POD | SA),
		/*34 TYPE_VEC4V, */		ITEM_NT("VEC4V",	Vec4V,			POD | SA),
		/*35 TYPE_MAT33V */		ITEM_NT("MAT33V",	Mat33V,			POD | SA),
		/*36 TYPE_MAT34V */		ITEM_NT("MAT34V",	Mat34V,			POD | SA),
		/*37 TYPE_MAT43 */		ITEM("MAT43",		48, 16,			POD | SA),
		/*38 TYPE_MAT44V */		ITEM_NT("MAT44V",	Mat44V,			POD | SA),
	},

	{ // CATEGORY_STRING
		/*40 TYPE_STRING_MEMBER */			ITEM("STRINGMEM",			0, 0,						POD | SA),
		/*41 TYPE_STRING_POINTER32 */		ITEM_NT("STRINGPTR_32",		psoFake32::CharPtr,			PTR32 | SA),
		/*42 TYPE_STRING_POINTER64 */		ITEM_NT("STRINGPTR_64",		psoFake64::CharPtr,			PTR64 | SA),
		/*43 TYPE_ATSTRING32 */				ITEM_NT("ATSTRING_32",		psoFake32::AtString,		PTR32 | SA),
		/*44 TYPE_ATSTRING64 */				ITEM_NT("ATSTRING_64",		psoFake64::AtString,		PTR64 | SA),
		/*45 TYPE_WIDE_STRING_MEMBER */		ITEM("WSTRINGMEM",			0, 0,						POD | SA),
		/*46 TYPE_WIDE_STRING_POINTER32 */	ITEM_NT("WSTRINGPTR_32",	psoFake32::CharPtr,			PTR32 | SA),
		/*47 TYPE_WIDE_STRING_POINTER64 */	ITEM_NT("WSTRINGPTR_64",	psoFake64::CharPtr,			PTR64 | SA),
		/*48 TYPE_ATWIDESTRING32 */			ITEM_NT("ATWIDESTRING_32",	psoFake32::AtWideString,	PTR32 | SA),
		/*49 TYPE_ATWIDESTRING64 */			ITEM_NT("ATWIDESTRING_64",	psoFake64::AtWideString,	PTR64 | SA),
		/*4a TYPE_STRINGHASH */				ITEM_NT("STRINGHASH",		atHashValue,				POD | MAP_KEY | SA),
		/*4b TYPE_PARTIALSTRINGHASH */		ITEM("PARTSTRINGHASH",		4, 4,						POD | SA),
		/*4c TYPE_LITERALSTRINGHASH */		ITEM_NT("LITSTRINGHASH",	atLiteralHashValue,			POD | MAP_KEY | SA),
		/*4d TYPE_NSSTRINGHASH */			ITEM_NT("NSSTRINGHASH",		atNamespacedHashStringBase,	POD | MAP_KEY | SA),
	},

	{ // CATEGORY_ARRAY
		/*50 TYPE_ARRAY_MEMBER */					ITEM("ARRAYMEM",				0, 0,					0),
		/*51 TYPE_ATARRAY32 */						ITEM_NT("ATARRAY_32",			psoFake32::AtArray16,	PTR32 | SA),
		/*52 TYPE_ATARRAY64 */						ITEM_NT("ATARRAY_64",			psoFake64::AtArray16,	PTR64 | SA),
		/*53 TYPE_ATFIXEDARRAY */					ITEM("ATFIXEDARRAY",			0, 0,					0),
		/*54 TYPE_ATARRAY32_32BITIDX */				ITEM_NT("ATARRAY32_32",			psoFake32::AtArray32,	PTR32 | SA),
		/*55 TYPE_ATARRAY64_32BITIDX */				ITEM_NT("ATARRAY32_64",			psoFake64::AtArray32,	PTR64 | SA),
		/*56 TYPE_ARRAY_POINTER32 */				ITEM_NT("ARRAYPTR_32",			psoFake32::VoidPtr,		PTR32 | SA),
		/*57 TYPE_ARRAY_POINTER64 */				ITEM_NT("ARRAYPTR_64",			psoFake64::VoidPtr,		PTR64 | SA),
		/*58 TYPE_ARRAY_POINTER32_WITH_COUNT */		ITEM_NT("ARRAYPTR_CT_32",		psoFake32::VoidPtr,		PTR32),
		/*59 TYPE_ARRAY_POINTER64_WITH_COUNT */		ITEM_NT("ARRAYPTR_CT_64",		psoFake64::VoidPtr,		PTR64),
	},

	{ // CATEGORY_ENUM
		/*60 TYPE_ENUM8 */			ITEM_NT("ENUM8",		u8,						POD),
		/*61 TYPE_ENUM16 */			ITEM_NT("ENUM16",		u16,					POD),
		/*62 TYPE_ENUM32 */			ITEM_NT("ENUM32",		u32,					POD),
		/*63 TYPE_BITSET8 */		ITEM("BITSET8",			0, 1,					POD),
		/*64 TYPE_BITSET16 */		ITEM("BITSET16",		0, 2,					POD),
		/*65 TYPE_BITSET32 */		ITEM("BITSET32",		0, 4,					POD),
		/*66 TYPE_ATBITSET32 */		ITEM_NT("ATBITSET_32",	psoFake32::AtBitset,	PTR32),
		/*67 TYPE_ATBITSET64 */		ITEM_NT("ATBITSET_64",	psoFake64::AtBitset,	PTR64), 
	},

	{ // CATEGORY_MAP
		/*70 TYPE_ATBINARYMAP32 */	ITEM_NT("ATBINMAP_32",	psoFake32::AtBinMap, PTR32 | SA),
		/*71 TYPE_ATBINARYMAP64 */	ITEM_NT("ATBINMAP_64",	psoFake64::AtBinMap, PTR64 | SA),
	},

};

const Traits& GetTrait(psoType t)
{

	return psoTypeTraits::g_PsoTypeTraits[t.GetRaw()>>psoType::CATEGORY_SHIFT][t.GetRaw() & psoType::TYPE_IN_CATEGORY_MASK];
}

bool GetTraitFlag(psoType t, u32 bit) 
{
	return ( GetTrait(t).m_Flags & bit ) != 0;
}

} // namespace psoTypeTraits

using namespace psoTypeTraits;

size_t psoType::GetSize() const
{
	return GetTrait(*this).m_Size;
}

size_t psoType::GetAlign() const
{
	return GetTrait(*this).m_Align;
}

#if !__NO_OUTPUT
const char* psoType::GetName() const
{
	return GetTrait(*this).m_Name;
}
#endif

bool psoType::IsValid() const
{
	return GetTraitFlag(*this, USED) && m_Type != TYPE_INVALID;
}

bool psoType::IsPod() const
{
	return GetTraitFlag(*this, POD);
}

bool psoType::IsMapKey() const
{
	return GetTraitFlag(*this, MAP_KEY);
}

bool psoType::IsStructArrayType() const
{
	return GetTraitFlag(*this, SA);
}

bool psoType::IsNative() const
{
#if __64BIT
	return !GetTraitFlag(*this, PTR32);
#else
	return !GetTraitFlag(*this, PTR64);
#endif
}

bool psoType::HasPointers() const
{
	return GetTraitFlag(*this, PTR32) || GetTraitFlag(*this, PTR64);
}

bool psoType::IsMemberType() const
{
	return IsValid() && !GetTraitFlag(*this, NON_MEMBER);
}

psoType psoType::ConvertOldStructArrayTypeToPsoType(u32 saType, bool is64Bit)
{
	FastAssert(psoConstants::IsParMemberType(saType));

	switch(saType)
	{
	case parMemberType::TYPE_STRING:
	case parMemberType::TYPE_ARRAY:
	case parMemberType::TYPE_ENUM:
	case parMemberType::TYPE_BITSET:
	case parMemberType::TYPE_MAP:
		// These types couldn't be structarrays before
		return psoType::TYPE_INVALID;
	case parMemberType::TYPE_STRUCT:
		// This meant "array of pointers"
		return is64Bit ? psoType::TYPE_STRING_POINTER64 : psoType::TYPE_STRING_POINTER32;
	case psoConstants::TYPE_ATSTRING_IN_ARRAY:
		return is64Bit ? psoType::TYPE_ATSTRING64 : psoType::TYPE_ATSTRING32;
	case psoConstants::TYPE_ATWIDESTRING_IN_ARRAY:
		return is64Bit ? psoType::TYPE_ATWIDESTRING64 : psoType::TYPE_ATWIDESTRING32;
	default: // POD types
		return ConvertParserTypeToPsoType((parMemberType::Enum)saType, 0, is64Bit);
	}
}

psoType psoType::ConvertParserTypeToPsoType( parMemberType::Enum type, u8 subtype, bool is64Bit )
{
	switch(type)
	{
	case parMemberType::TYPE_BOOL:		return psoType::TYPE_BOOL;
	case parMemberType::TYPE_CHAR:		return psoType::TYPE_S8;
	case parMemberType::TYPE_UCHAR:		return psoType::TYPE_U8;
	case parMemberType::TYPE_SHORT:		return psoType::TYPE_S16;
	case parMemberType::TYPE_USHORT:	return psoType::TYPE_U16;
	case parMemberType::TYPE_INT:		return psoType::TYPE_S32;
	case parMemberType::TYPE_UINT:		return psoType::TYPE_U32;
	case parMemberType::TYPE_INT64:		return psoType::TYPE_S64;
	case parMemberType::TYPE_UINT64:	return psoType::TYPE_U64;
	case parMemberType::TYPE_FLOAT:		return psoType::TYPE_FLOAT;
	case parMemberType::TYPE_DOUBLE:	return psoType::TYPE_DOUBLE;
	case parMemberType::TYPE_VECTOR2:	return psoType::TYPE_VEC2;
	case parMemberType::TYPE_VECTOR3:	return psoType::TYPE_VEC3V;
	case parMemberType::TYPE_VECTOR4:	return psoType::TYPE_VEC4V;
	case parMemberType::TYPE_STRING:
		switch((parMemberStringSubType::Enum)subtype)
		{
		case parMemberStringSubType::SUBTYPE_MEMBER:		return psoType::TYPE_STRING_MEMBER;
		case parMemberStringSubType::SUBTYPE_POINTER:		return is64Bit ? psoType::TYPE_STRING_POINTER64 : psoType::TYPE_STRING_POINTER32;
		case parMemberStringSubType::SUBTYPE_CONST_STRING:	return is64Bit ? psoType::TYPE_STRING_POINTER64 : psoType::TYPE_STRING_POINTER32;
		case parMemberStringSubType::SUBTYPE_ATSTRING:		return is64Bit ? psoType::TYPE_ATSTRING64 : psoType::TYPE_ATSTRING32;
		case parMemberStringSubType::SUBTYPE_WIDE_MEMBER:	return psoType::TYPE_WIDE_STRING_MEMBER;
		case parMemberStringSubType::SUBTYPE_WIDE_POINTER:	return is64Bit ? psoType::TYPE_WIDE_STRING_POINTER64 : psoType::TYPE_WIDE_STRING_POINTER32;
		case parMemberStringSubType::SUBTYPE_ATWIDESTRING:	return is64Bit ? psoType::TYPE_ATWIDESTRING64 : psoType::TYPE_ATWIDESTRING32;
		case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING: return psoType::TYPE_STRINGHASH;
		case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING: return psoType::TYPE_STRINGHASH;
		case parMemberStringSubType::SUBTYPE_ATHASHVALUE:	return psoType::TYPE_STRINGHASH;
		case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:	return psoType::TYPE_PARTIALSTRINGHASH;
		case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:		return psoType::TYPE_NSSTRINGHASH;
		case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:			return psoType::TYPE_NSSTRINGHASH;
		}
		break;
	case parMemberType::TYPE_STRUCT:
		switch((parMemberStructSubType::Enum)subtype)
		{
		case parMemberStructSubType::SUBTYPE_STRUCTURE:					return psoType::TYPE_STRUCT;
		case parMemberStructSubType::SUBTYPE_EXTERNAL_NAMED_POINTER:	
		case parMemberStructSubType::SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL: 
		case parMemberStructSubType::SUBTYPE_POINTER:
		case parMemberStructSubType::SUBTYPE_SIMPLE_POINTER:
			return is64Bit? psoType::TYPE_POINTER64 : psoType::TYPE_POINTER32;
		}
		break;
	case parMemberType::TYPE_ARRAY:
		switch((parMemberArraySubType::Enum)subtype)
		{
		case parMemberArraySubType::SUBTYPE_ATARRAY:			return is64Bit ? psoType::TYPE_ATARRAY64 : psoType::TYPE_ATARRAY32;
		case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:		return psoType::TYPE_ATFIXEDARRAY;
		case parMemberArraySubType::SUBTYPE_ATRANGEARRAY:		return psoType::TYPE_ARRAY_MEMBER;
		case parMemberArraySubType::SUBTYPE_POINTER:			return is64Bit ? psoType::TYPE_ARRAY_POINTER64 : psoType::TYPE_ARRAY_POINTER32;
		case parMemberArraySubType::SUBTYPE_MEMBER:				return psoType::TYPE_ARRAY_MEMBER;
		case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:	return is64Bit ? psoType::TYPE_ATARRAY64_32BITIDX : psoType::TYPE_ATARRAY32_32BITIDX;
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:	return is64Bit ? psoType::TYPE_ARRAY_POINTER64_WITH_COUNT : psoType::TYPE_ARRAY_POINTER32_WITH_COUNT;
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX: return is64Bit ? psoType::TYPE_ARRAY_POINTER64_WITH_COUNT : psoType::TYPE_ARRAY_POINTER32_WITH_COUNT;
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX: return is64Bit ? psoType::TYPE_ARRAY_POINTER64_WITH_COUNT : psoType::TYPE_ARRAY_POINTER32_WITH_COUNT;
		case parMemberArraySubType::SUBTYPE_VIRTUAL:			break; // INVALID
		}
		break;
	case parMemberType::TYPE_ENUM:	
		switch((parMemberEnumSubType::Enum)subtype)
		{
		case parMemberEnumSubType::SUBTYPE_32BIT:	return psoType::TYPE_ENUM32;
		case parMemberEnumSubType::SUBTYPE_16BIT:	return psoType::TYPE_ENUM16;
		case parMemberEnumSubType::SUBTYPE_8BIT:	return psoType::TYPE_ENUM8;
		}
		break;
	case parMemberType::TYPE_BITSET:
		switch((parMemberBitsetSubType::Enum)subtype)
		{
		case parMemberBitsetSubType::SUBTYPE_32BIT_FIXED:	return psoType::TYPE_BITSET32;
		case parMemberBitsetSubType::SUBTYPE_16BIT_FIXED:	return psoType::TYPE_BITSET16;
		case parMemberBitsetSubType::SUBTYPE_8BIT_FIXED:	return psoType::TYPE_BITSET8;
		case parMemberBitsetSubType::SUBTYPE_ATBITSET:		return is64Bit ? psoType::TYPE_ATBITSET64 : psoType::TYPE_ATBITSET32;
		}
		break;
	case parMemberType::TYPE_MAP:
		switch((parMemberMapSubType::Enum)subtype)
		{
		case parMemberMapSubType::SUBTYPE_ATMAP: break; // INVALID
		case parMemberMapSubType::SUBTYPE_ATBINARYMAP:	return is64Bit ? psoType::TYPE_ATBINARYMAP64 : psoType::TYPE_ATBINARYMAP32;
		}
		break;
	case parMemberType::TYPE_MATRIX34:	return psoType::TYPE_MAT34V;
	case parMemberType::TYPE_MATRIX44:	return psoType::TYPE_MAT44V;
	case parMemberType::TYPE_VEC2V:		return psoType::TYPE_VEC2V;
	case parMemberType::TYPE_VEC3V:		return psoType::TYPE_VEC3V;
	case parMemberType::TYPE_VEC4V:		return psoType::TYPE_VEC4V;
	case parMemberType::TYPE_MAT33V:	return psoType::TYPE_MAT33V;
	case parMemberType::TYPE_MAT34V:	return psoType::TYPE_MAT34V;
	case parMemberType::TYPE_MAT44V:	return psoType::TYPE_MAT44V;
	case parMemberType::TYPE_SCALARV:	return psoType::TYPE_SCALARV;
	case parMemberType::TYPE_BOOLV:		return psoType::TYPE_BOOLV;
	case parMemberType::TYPE_VECBOOLV:	return psoType::TYPE_VECBOOLV;
	case parMemberType::TYPE_PTRDIFFT:	return is64Bit ? psoType::TYPE_S64 : psoType::TYPE_S32;
	case parMemberType::TYPE_SIZET:		return is64Bit ? psoType::TYPE_U64 : psoType::TYPE_U32;
	case parMemberType::TYPE_FLOAT16:	return psoType::TYPE_FLOAT16;
	case parMemberType::INVALID_TYPE:	break; // INVALID
	}

	parErrorf("Invalid type %d subtype %d for PSO file", type, subtype);

	return psoType::TYPE_INVALID;
}

#if !__FINAL
void psoType::PrintUnexpectedTypeError(const char* context, const char* expected) const
{
	parErrorf("Unexpected type in %s. Got type %x ('%s'), expected %s", context, GetRaw(), GetTrait(*this).m_Name, expected);
}
#endif

