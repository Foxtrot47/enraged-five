// 
// parser/treenode.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_TREENODE_H
#define PARSER_TREENODE_H

#include "atl/array.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "string/unicode.h"

namespace rage {

class Vector2;
class Vector3;
class Vector4;
class Matrix34;
class Matrix44;

class Vec2V;
class Vec3V;
class Vec4V;
class Mat33V;
class Mat34V;
class Mat44V;

class parTree;

// PURPOSE: A parTreeNode is a single node in the node tree. Each node contains a parElement
// and could also contain a void* data pointer if there is a data block inside the parElement.
// NOTES:
//   Parents own their children.
class parTreeNode
{
public:
	parTreeNode();

	// PURPOSE: Creates a new named tree node
	// PARAMS:
	//		name - Name of the new tree node
	//		copyName - If true, parser copies the name string, if false, it just references it.
	explicit parTreeNode(const char* name, bool copyName=true);

	// PURPOSE: Deletes this node and all child nodes
	~parTreeNode();

	// RETURNS: This node's element
	parElement&			GetElement();

	// RETURNS: This node's element
	const parElement&	GetElement() const;

	static parTreeNode* CreateStdLeaf(const char* name, bool value, bool copyName=true); // Creates a leaf node <code><name value="val"/></code>
	static parTreeNode* CreateStdLeaf(const char* name, float value, bool copyName=true); // Creates a leaf node <code><name value="val"/></code>
	static parTreeNode* CreateStdLeaf(const char* name, double value, bool copyName=true); // Creates a leaf node <code><name value="val"/></code>
	static parTreeNode* CreateStdLeaf(const char* name, s64 value, bool copyName=true); // Creates a leaf node <code><name value="val"/></code>
	static parTreeNode* CreateStdLeaf(const char* name, const char* value, bool copyName=true); // Creates a leaf node <code><name content="ascii">val</name></code>
	static parTreeNode* CreateStdLeaf(const char* name, const char16* value, bool copyName=true); // Creates a leaf node <code><name content="utf16">val</name></code>

	static parTreeNode* CreateStdLeaf(const char* name, const Vector2& value, bool copyName=true); // Creates a leaf node <code><name x="x" y="y"/></code>
	static parTreeNode* CreateStdLeaf(const char* name, const Vector3& value, bool copyName=true); // Creates a leaf node <code><name x="x" y="y" z="z"/></code>
	static parTreeNode* CreateStdLeaf(const char* name, const Vector4& value, bool copyName=true); // Creates a leaf node <code><name x="x" y="y" z="z" w="w"/></code>
	static parTreeNode* CreateStdLeaf(const char* name, const Matrix34& value, bool copyName=true); // Creates a leaf node <code><name content="vector3_array">val0 val1 ... </name></code>
	static parTreeNode* CreateStdLeaf(const char* name, const Matrix44& value, bool copyName=true); // Creates a leaf node <code><name content="vector4_array">val0 val1 ... </name></code>

	static parTreeNode* CreateStdLeaf(const char* name, const Vec2V& value, bool copyName=true);
	static parTreeNode* CreateStdLeaf(const char* name, const Vec3V& value, bool copyName=true);
	static parTreeNode* CreateStdLeaf(const char* name, const Vec4V& value, bool copyName=true);
	static parTreeNode* CreateStdLeaf(const char* name, const Mat33V& value, bool copyName=true);
	static parTreeNode* CreateStdLeaf(const char* name, const Mat34V& value, bool copyName=true);
	static parTreeNode* CreateStdLeaf(const char* name, const Mat44V& value, bool copyName=true);

	// PURPOSE: Creates a new child node (as the first child) with simple data
	template<typename _Type>
	parTreeNode* InsertStdLeafChild(const char* name, const _Type& value, bool copyName=true)
	{
		parTreeNode* leaf = parTreeNode::CreateStdLeaf(name, value, copyName);
		leaf->InsertAsChildOf(this);
		return leaf;
	}

	// PURPOSE: Creates a new child node (as the last child) with simple data
	template<typename _Type>
	parTreeNode* AppendStdLeafChild(const char* name, const _Type& value, bool copyName=true)
	{
		parTreeNode* leaf = parTreeNode::CreateStdLeaf(name, value, copyName);
		leaf->AppendAsChildOf(this);
		return leaf;
	}

	bool	ReadStdLeafBool() const;	// Reads and returns the value held by a parTreeNode (assuming it was formatted as per CreateStdLeaf)
	float	ReadStdLeafFloat() const;	// <ALIAS ReadStdLeafBool>
	double	ReadStdLeafDouble() const;	// <ALIAS ReadStdLeafBool>
	int		ReadStdLeafInt() const;	// <ALIAS ReadStdLeafBool>
	s64		ReadStdLeafInt64() const;	// <ALIAS ReadStdLeafBool>
	const char*	ReadStdLeafString() const;	// <ALIAS ReadStdLeafBool>
	const char16*	ReadStdLeafWideString() const;	// <ALIAS ReadStdLeafBool>

	Vector2 ReadStdLeafVector2() const;	// <ALIAS ReadStdLeafBool>
	Vector3 ReadStdLeafVector3() const;	// <ALIAS ReadStdLeafBool>
	Vector4 ReadStdLeafVector4() const;	// <ALIAS ReadStdLeafBool>
	// doesn't return a copy for efficiency reasons
	void	ReadStdLeafMatrix34(Matrix34&) const;	// <ALIAS ReadStdLeafBool>
	void	ReadStdLeafMatrix44(Matrix44&) const;	// <ALIAS ReadStdLeafBool>

	Vec2V ReadStdLeafVec2() const;
	Vec3V ReadStdLeafVec3() const;
	Vec4V ReadStdLeafVec4() const;
	void ReadStdLeafMat33(Mat33V&) const;
	void ReadStdLeafMat34(Mat34V&) const;
	void ReadStdLeafMat44(Mat44V&) const;

	// PURPOSE: Returns the root node of this tree.
	parTreeNode*			FindRoot();

	// PURPOSE: Finds a tree node using a subset of XPath syntax
	// PARAMS:
	//		path - The XPath-style search path
	// RETURNS:
	//		A pointer to the matching tree node, or NULL if none were found
	// NOTES:
	//		The subset of XPath that we support is:
	//		<table>
	//		Path Part			Example									Purpose
	//		-----------			----------------------------			----------------------------
	//		<Name>				"Position", "Size"						The child element named <Name>
	//		[N]					"[1]"									The Nth child node, starting from 1
	//		<Name>[N]			"Item[3]"								The Nth child named <Name>, starting from 1
	//		<expr>/<expr>		"Player/Position", "Data/Item[3]/Size"	Evals the left expr, continues the search w/ the right expr.
	//		(leading) /<Name>	"/Player", "/Props/Data/Item[3]"		Find the tree root named <Name>
	//		(leading) /*		"/*", "/*/Data/Item[3]"					Find the tree root - regardless of name
	//		.					".", "./Name"							The current node
	//		..					"..", "../../Size"						Finds the parent node
	//		</table>
	//		Also note that you can only find elements, not attributes or data.
	parTreeNode*			FindFromXPath(const char* path);

	// PURPOSE: Finds an attribute reference using a subset of XPath syntax
	// PARAMS:
	//		path - The XPath-style search path
	// RETURNS:
	//		A pointer to the matching attribute, nor NULL if it wasn't found
	// NOTES:
	//		See above for the subset of XPath that we support. In addition all paths sent to this function
	//		must end in "@attrName". For example:
	//		"Item[3]/@value"
	//		"/Player/Position/@y"
	parAttribute*			FindAttributeFromXPath(const char* path);

	// PURPOSE: Finds a data value using a subset of XPath syntax
	// PARAMS:
	//		path - the XPath-style search path
	//		outVal - the found value
	//		buffer - If specified, a buffer of at least 50 bytes to hold the string version of a non-string attribute. If null, no conversion will be done
	// RETURNS:
	//		true if a value was found
	// NOTES:
	//		See FindFromXPath for description of the XPath subset. Normally this finds a value from a node that
	//		in the standard rage style (i.e. the style that CreateStdLeaf creates)
	//		For the versions that return an int, bool, float or const char* there is one additional option. 
	//		The path can contain an XPath-style attribute reference. E.g. "Player/Position/@x"
	//		If a particular attribute is specified we'll use that instead of expecting a standard-rage-format string
	bool					FindValueFromPath(const char* path, int& outVal);
	bool					FindValueFromPath(const char* path, s64& outVal);
	bool					FindValueFromPath(const char* path, bool& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, float& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, double& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, const char*& outVal, char* buffer = NULL, int size = 0);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, Vector2& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, Vector3& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, Vector4& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, Matrix34& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, Matrix44& outVal);	// <COMBINE FindValueFromPath@const char*@int&>

	bool					FindValueFromPath(const char* path, Vec2V& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, Vec3V& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, Vec4V& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, const Mat33V& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, const Mat34V& outVal);	// <COMBINE FindValueFromPath@const char*@int&>
	bool					FindValueFromPath(const char* path, const Mat44V& outVal);	// <COMBINE FindValueFromPath@const char*@int&>

	// PURPOSE: Finds a child element with the given name.
	// PARAMS:
	//		name - the name to search for.
	//		start - The starting point of the search. If NULL, starts at the beginning, otherwise starts at the sibling _after_ start.
	//		caseInsensitive - If true, do a case-insensitive search
	// RETURNS:
	//		A pointer to the matching child, or NULL if none were found.
	parTreeNode*			FindChildWithName(const char* name, parTreeNode* start = NULL, bool caseSensitive = true) const;

    // PURPOSE: Same as FindChildWithName except ignore namespaces.
	parTreeNode*			FindChildWithNameIgnoreNs(const char* name, parTreeNode* start = NULL, bool caseSensitive = true) const;

	// PURPOSE: Finds the ith child element
	// PARAMS:
	//		i - the index to search for.
	// RETURNS:
	//		A pointer to the ith child, or NULL if there weren't that many children.
	parTreeNode*			FindChildWithIndex(int i) const;

	// PURPOSE: Finds a child element with the given attribute.
	// PARAMS:
	//		name - the attribute name to search for.
	//		start - The starting point of the search. If NULL, starts at the beginning, otherwise starts at the sibling _after_ start.
	// RETURNS:
	//		A pointer to the matching child, or NULL if none were found.
	parTreeNode*			FindChildWithAttribute(const char* name, parTreeNode* start = NULL) const;

	// PURPOSE: Finds a child element with the given attribute and value.
	// PARAMS:
	//		name - the attribute name to search for.
	//		value - the attribute value to search for.
	//		start - The starting point of the search. If NULL, starts at the beginning, otherwise starts at the sibling _after_ start.
	// RETURNS:
	//		A pointer to the matching child, or NULL if none were found.
	parTreeNode*			FindChildWithAttribute(const char* name, const char* value, parTreeNode* start = NULL) const;

	// PURPOSE: Finds a child element with the given attribute and value.
	// PARAMS:
	//		name - the attribute name to search for.
	//		value - the attribute value to search for.
	//		start - The starting point of the search. If NULL, starts at the beginning, otherwise starts at the sibling _after_ start.
	// RETURNS:
	//		A pointer to the matching child, or NULL if none were found.
	parTreeNode*			FindChildWithAttribute(const char* name, int value, parTreeNode* start = NULL) const;

	// PURPOSE: Finds a child element with the given attribute and value.
	// PARAMS:
	//		name - the attribute name to search for.
	//		value - the attribute value to search for.
	//		start - The starting point of the search. If NULL, starts at the beginning, otherwise starts at the sibling _after_ start.
	// RETURNS:
	//		A pointer to the matching child, or NULL if none were found.
	parTreeNode*			FindChildWithAttribute(const char* name, bool value, parTreeNode* start = NULL) const;

	// PURPOSE: Swaps the contents of two tree nodes - does not swap custodial pointers
	void SwapWith(parTreeNode& other, bool fixupLinkage = true);

	// PURPOSE: An iterator to iterate over all child nodes.
	struct ChildNodeIterator {
		// PURPOSE: Create a new iterator starting at n
		ChildNodeIterator(parTreeNode* n) : m_CurrNode(n) {}

		// PURPOSE: Move the iterator to the next child.
		ChildNodeIterator& operator++() {
			m_CurrNode = m_CurrNode->GetSibling();
			return *this;
		}

		// RETURNS: The parTreeNode this iterator points to.
		parTreeNode* operator*() {
			return m_CurrNode;
		}

		bool operator==(const ChildNodeIterator& other) {
			return m_CurrNode == other.m_CurrNode;
		}
		bool operator!=(const ChildNodeIterator& other) {
			return m_CurrNode != other.m_CurrNode;
		}

		ChildNodeIterator& operator=(const ChildNodeIterator& other) {
			m_CurrNode = other.m_CurrNode;
			return *this;
		}

	protected:
		parTreeNode* m_CurrNode;
	};

	// RETURNS: An iterator that points to the beginning of this child list.
	ChildNodeIterator		BeginChildren() const {return ChildNodeIterator(GetChild());}

	// RETURNS: An iterator one past the end of the child list.
	ChildNodeIterator		EndChildren() const {return ChildNodeIterator(NULL);}

	// RETURNS: The number of children in a tree node
	int FindNumChildren() const;

	// RETURNS: True if this node has a raw data block
	bool HasData() const;

	// RETURNS: A pointer to the raw data block
	char*		GetData();

	// RETURNS: A pointer to the raw data block
	const char* GetData() const;

	// RETURNS: The size (in bytes) of the raw data block.
	u32			GetDataSize() const;

	// RETURNS: The data encoding or UNSPECIFIED
	parStream::DataEncoding			FindDataEncoding() const;

	// PURPOSE: Sets the raw data block to a copy of data.
	// PARAMS:
	//		data - Pointer to the data to add to this node
	//		size - Size (in bytes) of the data
	//		datatype - If its specified, sets the appropriate content="" attribute on the node. 
	//				   If its unspecified, don't modify the content="" attribute
	void		SetData(const char* data, u32 size, parStream::DataEncoding datatype=parStream::UNSPECIFIED);

	// PURPOSE: Truncates the raw data block to contain newSize bytes
	void		TruncateData(u32 newSize);

	// PURPOSE: Clears the data block (and the HasData flag)
	void		ClearData();

	// PURPOSE: Sets the raw data block to the array you specify, and takes ownership of the data
	// PARAMS:
	//		data - Pointer to the data to add to this node
	//		size - Size (in bytes) of the data
	//		datatype - If its specified, sets the appropriate content="" attribute on the node. 
	//				   If its unspecified, don't modify the content="" attribute
	// NOTES:
	//		Any existing data owned by this node will be freed
	void		AssumeData(char* newData, u32 newSize, parStream::DataEncoding datatype=parStream::UNSPECIFIED);

	// RETURNS: The parent node, or NULL if there is no parent
	parTreeNode* GetParent() const;

	// RETURNS: The next sibling node, or NULL if there is no sibling
	parTreeNode* GetSibling() const;

	// RETURNS: The first child node, or NULL if there are no children.
	parTreeNode* GetChild() const;

	// PURPOSE: Removes this node from the node tree. (only changes linkage, does not destroy nodes themselves)
	void RemoveSelf();

	// PURPOSE: Inserts this node as the first child of parent
	void InsertAsChildOf(parTreeNode *parent);

	// PURPOSE: Inserts this node as the next sibling of sibling
	void InsertAsSiblingOf(parTreeNode *sibling);

	// PURPOSE: Appends this node to the end of the parent list.
	void AppendAsChildOf(parTreeNode *parent);

	// PURPOSE: Removes all children and any data that may exist
	void ClearChildrenAndData();

	// PURPOSE: Creates and returns a deep copy of this tree
	parTreeNode* Clone();

	// PURPOSE: Finds the next child node. If the end of the list is reached, starts over at the beginning
	// RETURNS: true if iter just looped from the end of the list back to the beginning
	static bool FindNextNodeCircular(parTreeNode::ChildNodeIterator& iter, const parTreeNode::ChildNodeIterator& begin, const parTreeNode::ChildNodeIterator& end);

	// PURPOSE: Gets the number of data values we want to write per line.
	// See parStreamOut::WriteDataOptions
	u8& GetDataWriteValsPerLine() {return GetElement().GetUserData1();}
	const u8& GetDataWriteValsPerLine() const {return GetElement().GetUserData1();}

	enum Flags
	{
		FLAG_HAS_DATA = parElement::FLAG_ELT_OWNER_FLAGS,

		FLAG_DATA_WRITE_HIGH_PRECISION,		// See parStreamOut::WriteDataOptions
		FLAG_DATA_WRITE_HEXADECIMAL,		// See parStreamOut::WriteDataOptions
		FLAG_DATA_WRITE_UNSIGNED,			// See parStreamOut::WriteDataOptions
	};

	bool GetDataWriteFlag(int flag)
	{
		FastAssert(FLAG_DATA_WRITE_HIGH_PRECISION <= flag && flag <= FLAG_DATA_WRITE_UNSIGNED);
		return GetFlags().IsSet(flag);
	}

	void SetDataWriteFlag(int flag, bool b)
	{
		FastAssert(FLAG_DATA_WRITE_HIGH_PRECISION <= flag && flag <= FLAG_DATA_WRITE_UNSIGNED);
		GetFlags().Set(flag, b);
	}

	// RETURNS: -1 if a < b, 0 if a == b, 1 if a > b
	typedef atDelegate<int (const parTreeNode& a, const parTreeNode& b)> NodeSortDel;

	void SortChildren(NodeSortDel fn);

	void SortChildrenByName();
	void SortChildrenByAttribute(const char* attrName, parAttribute::Type sortAs = parAttribute::STRING, bool compareUnsigned = false);
		
protected:

	// If you pass in an attribute pointer _and_ the path spec ends in an attribute - 
	// returns NULL and fills out the attribute value.
	// Else returns a tree node (or NULL if nothing could be found)
	parTreeNode* FindFromXPathInternal(const char* path, parAttribute** attr);

	atFixedBitSet16& GetFlags() {return m_Element.GetFlags();}
	const atFixedBitSet16& GetFlags() const {return m_Element.GetFlags();}

	parTreeNode* SortListRecursive(parTreeNode *list, NodeSortDel compare);

	friend class parTree;

	parElement		m_Element;

	parTreeNode*	m_Parent;
	parTreeNode*	m_Sibling;
	parTreeNode*	m_Child;	// TODO: This could be in a union with m_DataArray

	atArray<char, 0, u32>	m_DataArray;
};

namespace parTreeNodeUtils
{

	// PURPOSE:
	//  Searches a parTree for any <xi:include> instance and resolves it
	// PARAMS: 
	//	node - The parTree node to start at
	void FindAndResolveAllXIncludes(parTreeNode& node);


	// PURPOSE:
	// Given a <xi:include> node, this function will resolve the include and replace the node
	// with the included (or fallback) data.
	// PARAMS:
	//		node - A reference to an <xi:include> node
	//		outReplNode - The node containing the replacement data (for XML replacement only)
	// RETURNS:
	//		true if the XInclude was resolved successfully, false if not (i.e. we may have used fallback data)
	// NOTES:
	//		node may be destroyed in this process. Don't use it anymore. If it is destroyed, outReplNode will contain
	//		its replacement, if it were replaced by XML data
	bool ResolveXInclude(parTreeNode& node, parTreeNode*& outReplNode);

	// PURPOSE: Given an xpointer string like "xpointer(/Root/Child[3]/Something)" extracts just the XPath portion
	// NOTES: 
	void ExtractXPathFromXPointer(char* xpath, int bufSize, const char* xpointer);

}

inline parTreeNode::parTreeNode()
: m_Parent(NULL)
, m_Sibling(NULL)
, m_Child(NULL)
{
	GetDataWriteValsPerLine() = 1;
}

inline parTreeNode::parTreeNode(const char* name, bool copyName)
: m_Parent(NULL)
, m_Sibling(NULL)
, m_Child(NULL)
{
	GetDataWriteValsPerLine() = 1;
	GetElement().SetName(name, copyName);
}

inline parElement& parTreeNode::GetElement()
{
	return m_Element;
}

inline const parElement& parTreeNode::GetElement() const
{
	return m_Element;
}

inline bool parTreeNode::HasData() const
{
	return GetFlags().IsSet(FLAG_HAS_DATA);
}

inline char* parTreeNode::GetData()
{
	return m_DataArray.GetCount() ? &m_DataArray[0] : NULL;
}

inline const char* parTreeNode::GetData() const
{
	return m_DataArray.GetCount() ? &m_DataArray[0] : NULL;
}

inline u32 parTreeNode::GetDataSize() const
{
	return m_DataArray.GetCount();
}

inline void parTreeNode::SetData(const char* data, u32 size, parStream::DataEncoding encoding /* = parStream::UNSPECIFIED */)
{
//	m_Data = (char*)operator new[](size, 16);
	if (encoding != parStream::UNSPECIFIED)
	{
		m_Element.AddAttribute("content", parStream::FindEncodingName(encoding), false, false);
	}

	if (m_DataArray.GetCapacity() < (int)size)
	{
		m_DataArray.Reset();
	}

	m_DataArray.Resize(size);
	memcpy(GetData(), data, size);
	GetFlags().Set(FLAG_HAS_DATA);
}

inline void parTreeNode::AssumeData(char* data, u32 size, parStream::DataEncoding encoding /* = parStream::UNSPECIFIED */)
{
	//	m_Data = (char*)operator new[](size, 16);
	if (encoding != parStream::UNSPECIFIED)
	{
		m_Element.AddAttribute("content", parStream::FindEncodingName(encoding), false, false);
	}

	atUserArray<char, 0, u32> userArr(data, size, true);
	m_DataArray.Assume(userArr);

	GetFlags().Set(FLAG_HAS_DATA);
}

inline void parTreeNode::ClearData() 
{
	GetFlags().Clear(FLAG_HAS_DATA);
	m_DataArray.Reset();
}


inline void parTreeNode::TruncateData(u32 newSize)
{
	FastAssert(HasData());
	FastAssert(newSize <= GetDataSize());
	m_DataArray.Resize(newSize);
}

inline parTreeNode* parTreeNode::GetParent() const
{
	return m_Parent; 
}

inline parTreeNode* parTreeNode::GetSibling() const
{
	return m_Sibling; 
}

inline parTreeNode* parTreeNode::GetChild() const
{
	return m_Child; 
}

inline parTreeNode* parTreeNode::FindFromXPath(const char* path)
{
	return FindFromXPathInternal(path, NULL);
}

inline parAttribute* parTreeNode::FindAttributeFromXPath(const char* path)
{
	parAttribute* ret = NULL;
	FindFromXPathInternal(path, &ret);
	return ret;
}

inline parTreeNode* parTreeNode::CreateStdLeaf(const char* name, float value, bool copyName/* =true */)
{
	return CreateStdLeaf(name, (double)value, copyName);
}

inline int parTreeNode::ReadStdLeafInt() const { return (int)ReadStdLeafInt64(); }
inline float parTreeNode::ReadStdLeafFloat() const { return (float)ReadStdLeafDouble(); }

} // namespace rage

#endif // PARSER_TREENODE_H
