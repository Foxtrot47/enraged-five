// 
// parser/membermap.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "membermap.h"
#include "memberstring.h"

#include "manager.h"
#include "psodata.h"
#include "optimisations.h"
#include "structure.h"
#include "treenode.h"
#include "visitorinit.h"
#include "visitorutils.h"

#include "bank/bank.h"
#include "parsercore/attribute.h"
#include "atl/map.h"
#include "math/simplemath.h"

#include <limits.h>
#include <math.h>

PARSER_OPTIMISATIONS();

using namespace rage;

STANDARD_PARMEMBER_DATA_FUNCS_DEFN(parMemberMap);

void parMemberMap::Data::Init()
{
	StdInit();
    m_Data = NULL;
    m_Key = NULL;
    m_CreateIteratorDelegate = NULL;
    m_CreateInterfaceDelegate = NULL;
}

parMemberMap::parMemberMap(Data& data) : 
parMember(&data)
{
    parAssertf(m_Data, "Passed in a NULL-reference to data?");
	if (GetData()->m_Key)
	{
	    m_KeyParMember = parStructure::CreateMemberFromMemberData(reinterpret_cast<parMember::CommonData*>(GetData()->m_Key));
	}
	if (GetData()->m_Data)
	{
	    m_DataParMember = parStructure::CreateMemberFromMemberData(reinterpret_cast<parMember::CommonData*>(GetData()->m_Data));
	}
}

parMemberMap::~parMemberMap()
{
	delete m_KeyParMember;
	delete m_DataParMember;
}

void parMemberMap::ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const
{
	if (!parVerifyf(node, "Can't handle NULLs, leaving member %s alone", GetName()))
	{
		return;
	}

    if (
        GetSubtype() == parMemberMapSubType::SUBTYPE_ATMAP ||
        GetSubtype() == parMemberMapSubType::SUBTYPE_ATBINARYMAP
        )
    {
        parMemberMapInterface* mapInterface = NULL;

		{ // scope for AutoUseTempMemory
			parManager::AutoUseTempMemory temp;
			mapInterface = const_cast<parMemberMap*>(this)->CreateInterface(structAddr);
		}


		int numChildren = 0;
		for(parTreeNode* kid = node->GetChild(); kid != NULL; kid = kid->GetSibling())
		{
			parAttribute* keyAttr = kid->FindAttributeFromXPath("@key");
			if (keyAttr)
			{
				numChildren ++;
			}
		}
		mapInterface->Reset();
		mapInterface->Reserve(numChildren);

        for(parTreeNode* kid = node->GetChild(); kid != NULL; kid = kid->GetSibling())
        {
            parAttribute* keyAttr = kid->FindAttributeFromXPath("@key");
            if (!keyAttr)
            {
                parAssertf(0, "failed to find [key] attribute for map %s check parsed data. Skipping the item.", GetName());
                continue;
            }

 
            parPtrToStructure dataValueAddr = NULL;
            switch (m_KeyParMember->GetType())
            {
            //These are the limited supported types for key values
            case parMemberType::TYPE_CHAR:
            case parMemberType::TYPE_UCHAR:
            case parMemberType::TYPE_SHORT:
            case parMemberType::TYPE_USHORT:
            case parMemberType::TYPE_INT:
            case parMemberType::TYPE_UINT:
			case parMemberType::TYPE_PTRDIFFT:
			case parMemberType::TYPE_SIZET:
			case parMemberType::TYPE_INT64:
			case parMemberType::TYPE_UINT64:
                {
                    s64 key = keyAttr->FindInt64Value();
                    dataValueAddr = mapInterface->InsertKey(&key);
                }
                break;
			case parMemberType::TYPE_ENUM:
				{
					s64 key = 0;
					if (keyAttr->GetType() == parAttribute::STRING)
					{
						parMemberEnum& memberEnum = m_KeyParMember->AsEnumRef();
						key = memberEnum.ValueFromName(keyAttr->GetStringValue());
					}
					else if (keyAttr->GetType() == parAttribute::INT64)
					{
						key = keyAttr->GetIntValue();
					}
					dataValueAddr = mapInterface->InsertKey(&key);
				}
				break;
            case parMemberType::TYPE_STRING:
                {
                    parMemberString& strParMember = m_KeyParMember->AsStringRef();
                    switch (strParMember.GetSubtype())
                    {
#if !__FINAL
                    case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
                        {
                            const char* key = keyAttr->GetStringValue();
                            atHashString temp(key);
                            dataValueAddr = mapInterface->InsertKey(&temp);
                        }
						break;
#endif
					case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
						{
							const char* key = keyAttr->GetStringValue();
							atFinalHashString temp(key);
							dataValueAddr = mapInterface->InsertKey(&temp);
						}
						break;
					case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
						{
							const char* key = keyAttr->GetStringValue();
							atHashValue temp(key);
							dataValueAddr = mapInterface->InsertKey(&temp);
						}
						break;
					case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
						{
							const char* key = keyAttr->GetStringValue();
							u32 temp = atPartialStringHash(key);
							dataValueAddr = mapInterface->InsertKey(&temp);
						}
						break;
					case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
					case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
						{
							const char* key = keyAttr->GetStringValue();
							u32 temp = atHashStringNamespaceSupport::ComputeHash(strParMember.GetData()->GetNamespaceIndex(), key);
							dataValueAddr = mapInterface->InsertKey(&temp);
						}
						break;
                    default:
                        parAssertf(0,"Unsupported key string type %d being used for map %s", strParMember.GetSubtype(), GetName());
                        break;
                    };
                }
                break;
            default:
                parAssertf(0,"Unsupported key type %d being used for map %s", m_KeyParMember->GetType(), GetName());
                break;
            };

            if (dataValueAddr)
            {
                //Init the value of the data
                parInitVisitor initter;
                initter.VisitMember(dataValueAddr, *m_DataParMember);

                //Read in the data from the tree node
                m_DataParMember->ReadTreeNode(kid, dataValueAddr);
            }
        }

        //This allows the tree to be sorted if that functionality exists
        mapInterface->PostInsert();

		{ // scope for AutoUseTempMemory
			parManager::AutoUseTempMemory temp;
	        delete mapInterface;
		}
    }
    else
    {
        parAssertf(0, "Unsupported Map Subtype %d", GetSubtype());
    }
}

size_t parMemberMap::GetSize() const
{
    switch(GetSubtype())
    {
    case parMemberMapSubType::SUBTYPE_ATMAP:
        return sizeof(atMap<char,char>);
    case parMemberMapSubType::SUBTYPE_ATBINARYMAP:
        return sizeof(atBinaryMap<char,char>);
    default:
        break;
    }
	return 0;
}


size_t rage::parMemberMap::FindAlign() const
{
	switch(GetSubtype())
	{
	case parMemberMapSubType::SUBTYPE_ATMAP:
		return __alignof(atMap<char,char>);
	case parMemberMapSubType::SUBTYPE_ATBINARYMAP:
		return __alignof(atBinaryMap<char, char>);
	}
	return 0;
}

parMemberMapIterator* parMemberMap::CreateIterator(parPtrToStructure structAddr)
{
    parAssertf(GetData(), "parMemberMap Data is not valid");
    parAssertf(GetData()->m_CreateIteratorDelegate, "CreateIteratorDelegate is not valid");
    return (*(GetData()->m_CreateIteratorDelegate))(*this, &GetMemberFromStruct<void*>(structAddr));
}

parMemberMapInterface* parMemberMap::CreateInterface(parPtrToStructure structAddr)
{
    parAssertf(GetData(), "parMemberMap Data is not valid");
    parAssertf(GetData()->m_CreateInterfaceDelegate, "CreateInterfaceDelegate is not valid");
    return (*(GetData()->m_CreateInterfaceDelegate))(*this, &GetMemberFromStruct<void*>(structAddr));
}


template<typename _Type>
int CompareKeyHelper(const void* eltA, const void* eltB)
{
	_Type a = *(_Type*)eltA;
	_Type b = *(_Type*)eltB;

	return a < b ? -1 : (a == b ? 0 : 1);
}

parMemberAtBinaryMapGenericInterface::parMemberAtBinaryMapGenericInterface(parMemberMap& member, void* instanceData)
{
	m_MapData = (parFake::AtBinMap*)instanceData;
	m_KeyMember = member.GetKeyMember();
	m_DataMember = member.GetDataMember();
	m_KeySize = m_KeyMember->GetSize();
	m_DataSize = m_DataMember->GetSize();

	// Compute the offset of the data member. 
	// The memory layout of one item in the array is 
	// [key]  [padding?]   [data]   [padding?]
	// This first padding is needed to align [data], the second padding pads up the the highest alignment of either key or data

	size_t keyAlign = m_KeyMember->FindAlign();
	size_t dataAlign = m_DataMember->FindAlign();

	m_DataOffset = m_KeySize;
	m_DataOffset = AlignPow2(m_DataOffset, dataAlign);

	m_ElementSize = m_DataOffset + m_DataSize;
	m_ElementSize = AlignPow2(m_ElementSize, Max(keyAlign, dataAlign));


	// Based on the key type, set a comparison function pointer that we can use later on for sorting and searching
	m_ComparisonFunction = NULL;
	switch(m_KeyMember->GetType())
	{
	case parMemberType::TYPE_CHAR:
		m_ComparisonFunction = &CompareKeyHelper<char>;
		break;
	case parMemberType::TYPE_UCHAR:
		m_ComparisonFunction = &CompareKeyHelper<u8>;
		break;
	case parMemberType::TYPE_SHORT:
		m_ComparisonFunction = &CompareKeyHelper<s16>;
		break;
	case parMemberType::TYPE_USHORT:
		m_ComparisonFunction = &CompareKeyHelper<u16>;
		break;
	case parMemberType::TYPE_INT:
		m_ComparisonFunction = &CompareKeyHelper<int>;
		break;
	case parMemberType::TYPE_PTRDIFFT:
		m_ComparisonFunction = &CompareKeyHelper<ptrdiff_t>;
		break;
	case parMemberType::TYPE_SIZET:
		m_ComparisonFunction = &CompareKeyHelper<size_t>;
		break;
	case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_STRING: // == atHashString - only type of string key allowed
		m_ComparisonFunction = &CompareKeyHelper<u32>;
		break;
	case parMemberType::TYPE_ENUM:
		{
			parMemberEnumSubType::Enum subtype = m_KeyMember->AsEnumRef().GetSubtype();
			switch(subtype)
			{
			case parMemberEnumSubType::SUBTYPE_8BIT:
				m_ComparisonFunction = &CompareKeyHelper<char>;
				break;
			case parMemberEnumSubType::SUBTYPE_16BIT:
				m_ComparisonFunction = &CompareKeyHelper<s16>;
				break;
			case parMemberEnumSubType::SUBTYPE_32BIT:
				m_ComparisonFunction = &CompareKeyHelper<int>;
				break;
			}
		}
		break;
	case parMemberType::TYPE_INT64:
		m_ComparisonFunction = &CompareKeyHelper<s64>;
		break;
	case parMemberType::TYPE_UINT64:
		m_ComparisonFunction = &CompareKeyHelper<u64>;
		break;
	default:
		parAssertf(0, "Invalid type for key!");
		break;
	}
}

char* parMemberAtBinaryMapGenericInterface::GetArrayElement(int i)
{
	return m_MapData->m_Data.m_Elements.m_Pointer + (i * m_ElementSize);
}

void parMemberAtBinaryMapGenericInterface::Reserve(int capacity)
{
	Assert(m_MapData->m_Data.m_Count == 0);
	Assert(m_MapData->m_Data.m_Capacity == 0);
	Assert(m_MapData->m_Data.m_Elements.m_Pointer == NULL);

	m_MapData->m_Data.m_Capacity = static_cast< u16 >(capacity);
	m_MapData->m_Data.m_Elements.m_Pointer = rage_aligned_new(16) char[m_MapData->m_Data.m_Capacity * m_ElementSize];

	// Default initialize the array, in case the values had non-POD types in them)
	parMemberPlacementVisitor placeVis;
	for(int i = 0; i < capacity; i++)
	{
		char* newElt = GetArrayElement(i);
		parPtrToStructure addrOfKey = reinterpret_cast<parPtrToStructure>(newElt);
		parPtrToStructure addrOfValue = reinterpret_cast<parPtrToStructure>(newElt + m_DataOffset);

		placeVis.VisitMember(addrOfKey, *m_KeyMember);

		placeVis.VisitMember(addrOfValue, *m_DataMember);
	}
}

parPtrToStructure parMemberAtBinaryMapGenericInterface::InsertKey(void* keyValue)
{
	// This has to emulate the following operation:
	// Sorted = false
	// Data.Grow()
	// Data.Top().Key = *keyValue;
	// return Data.Top().Data

	m_MapData->m_Sorted = false;

	// Grow the array if necessary - same as atArray::Grow
	if (m_MapData->m_Data.m_Count == m_MapData->m_Data.m_Capacity)
	{
		int origCapacity = m_MapData->m_Data.m_Capacity;
		m_MapData->m_Data.m_Capacity += 4;
		char* newElts = rage_aligned_new(16) char[m_MapData->m_Data.m_Capacity * m_ElementSize];

		sysMemCpy(newElts, m_MapData->m_Data.m_Elements.m_Pointer, m_MapData->m_Data.m_Count * m_ElementSize);
		delete [] m_MapData->m_Data.m_Elements.m_Pointer;
		m_MapData->m_Data.m_Elements.m_Pointer = newElts;

		// Default initialize the new elements we added
		parMemberPlacementVisitor placeVis;
		for(int i = origCapacity; i < m_MapData->m_Data.m_Capacity; i++)
		{
			char* newElt = GetArrayElement(i);
			parPtrToStructure addrOfKey = reinterpret_cast<parPtrToStructure>(newElt);
			parPtrToStructure addrOfValue = reinterpret_cast<parPtrToStructure>(newElt + m_DataOffset);

			placeVis.VisitMember(addrOfKey, *m_KeyMember);

			placeVis.VisitMember(addrOfValue, *m_DataMember);
		}
	}

	m_MapData->m_Data.m_Count++;
	// Find the address of the new element
	char* newElt = GetArrayElement(m_MapData->m_Data.m_Count-1);

	// From there get the key and value addresses
	parPtrToStructure addrOfKey = reinterpret_cast<parPtrToStructure>(newElt);
	parPtrToStructure addrOfValue = reinterpret_cast<parPtrToStructure>(newElt + m_DataOffset);

	// Assume key is a POD - so a sysMemCpy should suffice to set the value
	sysMemCpy(addrOfKey, keyValue, m_KeySize);

	return addrOfValue;
}


void parMemberAtBinaryMapGenericInterface::PostInsert() {
	// This has to emulate the following operation
	// 	std::sort(Data.begin(), Data.end(), DataPair::SortCompare);
	//  Sorted = true

	// use qsort because it lets us specify the stride
	qsort(m_MapData->m_Data.m_Elements.m_Pointer, m_MapData->m_Data.m_Count, m_ElementSize, m_ComparisonFunction);

	m_MapData->m_Sorted = true;
}

void parMemberAtBinaryMapGenericInterface::Reset() {
	// This has to emulate the following operation
	// Data.Reset()
	// Sorted = true

	// Basically this is all atArray::Reset
	for(int i = 0; i < m_MapData->m_Data.m_Count; i++)
	{
		char* element = GetArrayElement(i);
		parPtrToStructure addrOfValue = reinterpret_cast<parPtrToStructure>(element + m_DataOffset);

		parDeleteMember(addrOfValue, *m_DataMember);
	}

	delete [] m_MapData->m_Data.m_Elements.m_Pointer;
	m_MapData->m_Data.m_Elements.m_Pointer = NULL;
	m_MapData->m_Data.m_Count = 0;
	m_MapData->m_Data.m_Capacity = 0;
	m_MapData->m_Sorted = true;
}

parPtrToStructure parMemberAtBinaryMapGenericInterface::GetDataPtr (void *keyValue) {
	// This has to emulate the following operation
	// if (Sorted)
	//   std::lower_bound(Data.begin(), Data.end(), searchKey, DataPair::SearchCompare);
	// else
	//   loop over data, comparing.
	// But for now just assert that it's sorted

	FastAssert(m_MapData->m_Sorted);

	void* foundKey = NULL;

	if (m_MapData->m_Sorted)
	{
		foundKey = bsearch(keyValue, m_MapData->m_Data.m_Elements.m_Pointer, m_MapData->m_Data.m_Count, m_ElementSize, m_ComparisonFunction);
	}
	else
	{
		// Do a linear search
		for(int i = 0; i < m_MapData->m_Data.m_Count; i++)
		{
			char* keyPtr = GetArrayElement(i);
			int compareValue = m_ComparisonFunction(keyPtr, keyValue);
			if (compareValue == 0)
			{
				foundKey = keyPtr;
				break;
			}
		}
	}

	if (foundKey)
	{
		return reinterpret_cast<parPtrToStructure>(reinterpret_cast<char*>(foundKey) + m_DataOffset);
	}
	return NULL;
}

bool parMemberAtBinaryMapGenericInterface::Remove (void * keyValue, bool dataNeedsDelete) 
{
	parPtrToStructure dataPtr = GetDataPtr(keyValue);

	if (!dataPtr)
	{
		return false;
	}

	if (dataNeedsDelete)
	{
		// TODO: Right now if this is a pointer member it recursively deletes the child objects,
		// should we just run the normal destructor?
		parDeleteMember(dataPtr, *m_DataMember);
	}

	char* keyPtr = reinterpret_cast<char*>(dataPtr) - m_DataOffset;

	// Everybody moves down one element
	char* endOfArray = GetArrayElement(m_MapData->m_Data.m_Count);
	memmove(keyPtr, keyPtr + m_ElementSize, endOfArray - keyPtr);

	m_MapData->m_Data.m_Count--;

	return true;
}


parMemberMapInterface* parMemberMapInterfaces::CreateInterfaceAtBinaryMap(parMemberMap& map, void* atBinaryMapAddr)
{
	parAssertf(atBinaryMapAddr, "CreateInterfaceAtBinaryMap atBinaryMapAddr is invalid param.");
	return rage_new parMemberAtBinaryMapGenericInterface(map, atBinaryMapAddr);
}

parMemberAtBinaryMapGenericIterator::parMemberAtBinaryMapGenericIterator(parMemberMap& member, void* instanceData)
{
	m_MapData = (parFake::AtBinMap*)instanceData;
	parMember* keyMember = member.GetKeyMember();
	parMember* dataMember = member.GetDataMember();
	size_t keySize = keyMember->GetSize();
	size_t dataSize = dataMember->GetSize();

	// Compute the offset of the data member. 
	// The memory layout of one item in the array is 
	// [key]  [padding?]   [data]   [padding?]
	// This first padding is needed to align [data], the second padding pads up the the highest alignment of either key or data

	size_t keyAlign = keyMember->FindAlign();
	size_t dataAlign = dataMember->FindAlign();

	m_DataOffset = keySize;
	m_DataOffset = AlignPow2(m_DataOffset, dataAlign);

	m_ElementSize = m_DataOffset + dataSize;
	m_ElementSize = AlignPow2(m_ElementSize, Max(keyAlign, dataAlign));

	m_CurrElement = 0;
}

void parMemberAtBinaryMapGenericIterator::Begin() {
	m_CurrElement = 0;
}

void parMemberAtBinaryMapGenericIterator::Next() {
	m_CurrElement++;
}

char* parMemberAtBinaryMapGenericIterator::GetArrayElement(int i)
{
	return m_MapData->m_Data.m_Elements.m_Pointer + (i * m_ElementSize);
}

parPtrToStructure parMemberAtBinaryMapGenericIterator::GetKeyPtr() {
	return reinterpret_cast<parPtrToStructure>(GetArrayElement(m_CurrElement));
}

parPtrToStructure parMemberAtBinaryMapGenericIterator::GetDataPtr() {
	return reinterpret_cast<parPtrToStructure>(GetArrayElement(m_CurrElement) + m_DataOffset);
}

bool parMemberAtBinaryMapGenericIterator::AtEnd() {
	return m_CurrElement == m_MapData->m_Data.m_Count;
}


parMemberMapIterator* parMemberMapIterators::CreateIteratorAtBinaryMap(parMemberMap& map, void* atBinaryMapAddr)
{
	parAssertf(atBinaryMapAddr, "CreateIteratorAtBinaryMap atBinaryMapAddr is invalid param.");
	return rage_new parMemberAtBinaryMapGenericIterator(map, atBinaryMapAddr);
}
