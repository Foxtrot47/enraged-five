// 
// parser/psoschemabuilder.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psoschemabuilder.h"

#include "member.h"
#include "psobyteswap.h"
#include "psoconsts.h"
#include "psodata.h"
#include "psofile.h"
#include "psoschema.h"
#include "structure.h"
#include "visitor.h"

#include "atl/array.h"
#include "atl/map.h"
#include "diag/output.h"
#include "file/asset.h"
#include "math/simplemath.h"
#include "system/endian.h"
#include "system/magicnumber.h"
#include "system/memops.h"


using namespace rage;

#if __ASSERT
void parAssertAligned(u32 value, u32 alignment, const char* message) 
{
	parAssertf((value & (alignment - 1)) == 0, "Incorrect alignment for %s, expected %d. Value is 0x%x", message, alignment, value);
}
#else
#define parAssertAligned(x,y,z)
#endif

using namespace sysEndian;

struct psoBuilderStructSchema
{
	psoSchemaStructureData m_Structure;
	atArray<psoSchemaMemberData> m_Members;
	u32 m_Offset;
	u32	m_Size;
	const parStructure* m_OrigStructure;

	psoBuilderStructSchema()
		: m_Offset(0)
		, m_Size(0)
		, m_OrigStructure(NULL)
	{}

	u32 ComputeSizeAndOffset(u32 prevOffset) {
		parAssertAligned(prevOffset, 4, "psoSchemaStructureData");
		m_Offset = prevOffset;
		m_Size = sizeof(psoSchemaStructureData) + sizeof(psoSchemaMemberData) * m_Members.GetCount();
		return m_Size;
	}

	void Serialize(char* dest) {
		psoSchemaStructureData* schemaBuffer = reinterpret_cast<psoSchemaStructureData*>(dest + m_Offset);

		*schemaBuffer = m_Structure;
		sysMemCpy(schemaBuffer+1, m_Members.GetElements(), sizeof(psoSchemaMemberData) * m_Members.GetCount());
	}
};

struct psoBuilderEnumSchema
{
	psoSchemaEnumData	m_Enum;
	atArray<psoEnumTableEntry> m_Values;
	atLiteralHashValue m_NameHash;
	u32 m_Offset;
	u32	m_Size;

	psoBuilderEnumSchema() 
		: m_Offset(0)
		, m_Size(0)
	{}

	u32 ComputeSizeAndOffset(u32 prevOffset) { 
		parAssertAligned(prevOffset, 4, "psoSchemaEnumData");
		m_Offset = prevOffset;
		m_Size = sizeof(psoSchemaEnumData) + sizeof(psoEnumTableEntry) * m_Values.GetCount(); 
		return m_Size;
	}

	void Serialize(char* dest) {
		psoSchemaEnumData* enumBuffer = reinterpret_cast<psoSchemaEnumData*>(dest + m_Offset);

		*enumBuffer = m_Enum;
		sysMemCpy(enumBuffer+1, m_Values.GetElements(), sizeof(psoEnumTableEntry) * m_Values.GetCount());
	}
};

void rage::psoBuildSchemaMemberData(const parMember& mem, psoSchemaMemberData& outMemData)
{
	using namespace parMemberType;

	outMemData.m_NameHash = mem.GetNameHash();
	outMemData.m_Type = (u8)mem.GetType();
	outMemData.m_SubType = mem.GetCommonData()->m_Subtype;
	parAssertf(mem.GetOffset() < USHRT_MAX, "Invalid offset for member variable: %d", mem.GetOffset());
	outMemData.m_Offset = (u16)mem.GetOffset();


	switch(mem.GetType())
	{
	case TYPE_ENUM:
		{
			const parMemberEnum& enumMember = *smart_cast<const parMemberEnum*>(&mem);
			outMemData.m_ReferentHash = enumMember.GetData()->m_EnumData->m_NameHash;
		}
		break;
	case TYPE_STRUCT:
		{
			const parMemberStruct& structMember = *smart_cast<const parMemberStruct*>(&mem);
			if (structMember.GetBaseStructure())
			{
				outMemData.m_ReferentHash = structMember.GetBaseStructure()->GetNameHash();
			}
		}
		break;
	case TYPE_BITSET:
		{
			const parMemberBitset& bitsetMember = *smart_cast<const parMemberBitset*>(&mem);
			outMemData.m_Count = bitsetMember.GetData()->m_NumBits;
		}
		break;
	case TYPE_ARRAY:
		{
			const parMemberArray& arrayMember = *smart_cast<const parMemberArray*>(&mem);
			parAssertf(arrayMember.GetData()->m_NumElements < USHRT_MAX, "Too many elements for a PSO array");
			outMemData.m_Count = (u16)arrayMember.GetData()->m_NumElements;
			outMemData.m_MemberIndex = 0; // Needs to be filled out by calling code
		}
		break;
	case TYPE_STRING:
		{
			const parMemberString& stringMember = *smart_cast<const parMemberString*>(&mem);
			parAssertf(stringMember.GetData()->m_Length < USHRT_MAX, "Too many elements for a PSO array");
			outMemData.m_Count = (u16)stringMember.GetData()->m_Length;
			outMemData.m_MemberIndex = 0;
		}
		break;
	default:
		outMemData.m_ReferentHash = 0;
	}

}

void AddMemberToSchema(psoBuilderStructSchema& sch, const parMember& mem)
{
	psoBuildSchemaMemberData(mem, sch.m_Members.Grow());

	if (mem.GetType() == parMemberType::TYPE_ARRAY)
	{
		const parMemberArray& arrayMem = static_cast<const parMemberArray&>(mem);
		int arrayIndex = sch.m_Members.GetCount()-1; // the element we just added
		int elementIndex = arrayIndex + 1;

		arrayMem.SetPrototypeMemberToIndex(NULL, NULL, 0); // sets the member's offset to 0 (yeah you can do that for const objects too)

		AddMemberToSchema(sch, *arrayMem.GetPrototypeMember());

		sch.m_Members[elementIndex].m_NameHash = psoConstants::FIRST_ANONYMOUS_ID;
		sch.m_Members[arrayIndex].m_MemberIndex = (u16)elementIndex; 
	}
}

void rage::psoBuildSchemaStructData(const parStructure& str, psoSchemaStructureData& outStrData)
{
	outStrData.m_Type = psoConstants::ST_STRUCTURE;
	outStrData.m_Flags = str.GetFlags().GetRawBlock(0);
	FastAssert(str.GetSize() < (1<<16));
	outStrData.m_Size = (u16)str.GetSize();
	outStrData.m_NumMembers = 0; // this gets filled in later
	outStrData.m_MajorVersion = (u16)str.GetVersion().GetMajor();
	outStrData.m_Pad = 0;
}

psoBuilderStructSchema BuildSchemaFromStructure(const parStructure& str)
{
	psoBuilderStructSchema schema;

	schema.m_OrigStructure = &str;

	psoSchemaStructureData& strData = schema.m_Structure;
	psoBuildSchemaStructData(str, strData);

	parStructure::BaseList bases;
	bases.Init(const_cast<parStructure*>(&str), NULL);

	// Go in reverse order (from base class to most dervied class)
	for(int baseNum = bases.GetNumBases()-1; baseNum >= 0 ; baseNum--)
	{
		parStructure* baseStr = bases.GetBaseStructure(baseNum);

		for(int memIdx = 0; memIdx < baseStr->GetNumMembers(); memIdx++)
		{
			AddMemberToSchema(schema, *baseStr->GetMember(memIdx));
		}
	}

	FastAssert(schema.m_Members.GetCount() < (1<<16));
	strData.m_NumMembers = (u16)schema.m_Members.GetCount();

	// Any particular way we should sort the members list? By namehash? By offset?

	return schema;
}

void AddEnumToSchema(psoBuilderEnumSchema& sch, u32 namehash, int value)
{
	psoEnumTableEntry entry;
	entry.m_NameHash = namehash;
	entry.m_Value = value;
	sch.m_Values.PushAndGrow(entry);
}

void rage::psoBuildSchemaEnumData(const parEnumData& parenum, psoSchemaEnumData& outEnumData)
{
	outEnumData.m_Type = psoConstants::ST_ENUM;
	outEnumData.m_Pad = 0;
	outEnumData.m_NumEnums = parenum.m_NumEnums;
}

psoBuilderEnumSchema BuildSchemaFromEnum(const parEnumData& parenum)
{
	psoBuilderEnumSchema schema;

	psoSchemaEnumData& enumData = schema.m_Enum;
	psoBuildSchemaEnumData(parenum, enumData);
	schema.m_NameHash = parenum.m_NameHash;

	for(int i = 0; i < parenum.m_NumEnums; i++)
	{
		AddEnumToSchema(schema, parenum.m_Enums[i].m_NameKey, parenum.m_Enums[i].m_Value);
	}

	return schema;
}

struct psoBuilderSchemaCatalog
{
	atArray<psoBuilderStructSchema> m_StructSchemas;
	atArray<psoBuilderEnumSchema> m_EnumSchemas;
	u32	m_Size;

	psoBuilderSchemaCatalog()
		: m_Size(0)
	{}

	psoBuilderStructSchema* FindStruct(u32 hash)
	{
		for(int i = 0; i < m_StructSchemas.GetCount(); i++)
		{
			if (m_StructSchemas[i].m_OrigStructure->GetNameHash() == hash)
			{
				return &m_StructSchemas[i];
			}
		}
		return NULL;
	}

	psoBuilderEnumSchema* FindEnum(u32 hash)
	{
		for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
		{
			if (m_EnumSchemas[i].m_NameHash == hash)
			{
				return &m_EnumSchemas[i];
			}
		}
		return NULL;
	}

	void AddStructSchema(const parStructure& metadata)
	{
		if (FindStruct(metadata.GetNameHash()))
		{
			return;
		}
		m_StructSchemas.PushAndGrow(BuildSchemaFromStructure(metadata));
	}

	void AddEnumSchema(const parEnumData& metadata)
	{
		if (FindEnum(metadata.m_NameHash))
		{
			return;
		}
		m_EnumSchemas.PushAndGrow(BuildSchemaFromEnum(metadata));
	}

	// Note this doesn't take an offset parameter because all offsets for all child objects are
	// supposed to be relative to this object (which also means relative to this IFF chunk)
	u32 ComputeSizeAndOffset() 
	{
		u32 offset = 0;
		u32 headerSize = sizeof(psoSchemaCatalogData);
		u32 typeTableSize = sizeof(psoTypeTableData) * (m_StructSchemas.GetCount() + m_EnumSchemas.GetCount());
		offset += headerSize + typeTableSize;

		// rsTODO: If we ever sort these by ID, make sure that we combine the struct schemas and the enum schemas into a single list.

		u32 childSizes = 0;
		for(int i = 0; i < m_StructSchemas.GetCount(); i++)
		{
			u32 newSize = m_StructSchemas[i].ComputeSizeAndOffset(offset);
			offset += newSize;
			childSizes += newSize;
		}

		for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
		{
			u32 newSize = m_EnumSchemas[i].ComputeSizeAndOffset(offset);
			offset += newSize;
			childSizes += newSize;
		}

		m_Size = RoundUp<4>(headerSize + typeTableSize + childSizes);

		return m_Size;
	}

	void Serialize(char* dest)
	{
		psoSchemaCatalogData* catBuffer = reinterpret_cast<psoSchemaCatalogData*>(dest);

		catBuffer->m_IffTag.SetMagic( psoConstants::PSCH_MAGIC_NUMBER); 
		catBuffer->m_IffTag.SetSize(m_Size);

		catBuffer->m_NumTypes = m_StructSchemas.GetCount() + m_EnumSchemas.GetCount();

		// build up the type table now
		atArray<psoTypeTableData> typeTable;
		for(int i = 0; i < m_StructSchemas.GetCount(); i++)
		{
			psoTypeTableData entry;
			entry.m_TypeHash = m_StructSchemas[i].m_OrigStructure->GetNameHash();
			entry.m_Offset = m_StructSchemas[i].m_Offset;
			typeTable.PushAndGrow(entry);
		}

		for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
		{
			psoTypeTableData entry;
			entry.m_TypeHash = m_EnumSchemas[i].m_NameHash;
			entry.m_Offset = m_EnumSchemas[i].m_Offset;
			typeTable.PushAndGrow(entry);
		}

		// rsTODO: Sort the type table here?
		int totalTypes = m_EnumSchemas.GetCount() + m_StructSchemas.GetCount();

		sysMemCpy(catBuffer+1, typeTable.GetElements(), sizeof(psoTypeTableData) * totalTypes	);

		for(int i = 0; i < m_StructSchemas.GetCount(); i++)
		{
			m_StructSchemas[i].Serialize(dest);
		}

		for(int i = 0; i < m_EnumSchemas.GetCount(); i++)
		{
			m_EnumSchemas[i].Serialize(dest);
		}


	}
};


u32 rage::psoComputeLayoutHash(psoSchemaStructure& str, psoSchemaCatalog& /*catalog*/)
{
	u32 hash = 0;

	hash = atPartialDataHash((char*)(str.internal_GetRawData()), sizeof(psoSchemaStructureData), hash);

	hash = atPartialDataHash((char*)(str.internal_GetMemberArray()), sizeof(psoSchemaMemberData) * str.GetNumMembers(), hash);

	// find all the members with references that would affect memory layout or data interpretation.
	// AFAIK those are:
	// TYPE_STRUCT | SUBTYPE_STRUCTURE
	// TYPE_ENUM 
	// TYPE_BITSET
#if 0 // rsTODO: Implement this!
	for(int i = 0; i < str.GetNumMembers(); i++)
	{
		psoSchemaMember mem = str.GetMemberByIndex(i);
		u32 subSignature = 0;
		if (mem.GetType() == parMemberType::TYPE_STRUCT &&
			mem.GetSubType() == parMemberStructSubType::SUBTYPE_STRUCTURE)
		{
			subSignature = psoComputeLayoutHash(catalog.FindSchemaStructure(mem.GetReferentHash()), catalog);
		}
		else if (mem.GetType() == parMemberType::TYPE_ENUM ||
			mem.GetType() == parMemberType::TYPE_BITSET)
		{
			subSignature = psoComputeLayoutHash(catalog.FindSchemaEnum(mem.GetReferentHash()), catalog);
		}

		if (subSignature != 0)
		{
			hash = atPartialDataHash((char*)(&subSignature), sizeof(u32), hash);
		}
	}
#endif

	return atFinalizeHash(hash);
}

u32 rage::psoComputeLayoutHash(parStructure& str)
{
	u32 hash = 0;
	if (str.GetBaseStructure())
	{
		hash = psoComputeLayoutHash(*str.GetBaseStructure());
	}

	psoSchemaStructureData structData;
	psoBuildSchemaStructData(str, structData);
	hash = atPartialDataHash((char*)&structData, sizeof(psoSchemaStructureData), hash);

	for(int i = 0; i < str.GetNumMembers(); i++)
	{
		psoSchemaMemberData memData;
		psoBuildSchemaMemberData(*str.GetMember(i), memData);
		hash = atPartialDataHash((char*)&memData, sizeof(psoSchemaMemberData), hash);
	}

	return atFinalizeHash(hash);
}

// Data for a single instance (or single array of instances)
struct psoBuilderInstance
{
	const char*		m_Data;
	psoBuilderStructSchema* m_StructSchema;
	const parStructure*    m_Structure;
	parMemberType::Enum		m_PodType;
	u32				m_Alignment;
	u32				m_Count;
	u32				m_Offset;
	u32				m_Size;
	psoStructId		m_Id;

	struct IdFixupInfo
	{
		psoStructId*		m_AddressOfStructId;	 // Address of a 4-byte location that currently holds a pointer but will be replaced with a structId
		psoBuilderInstance* m_ReferentData;
	};

	atArray<IdFixupInfo> m_IdFixups;

	psoBuilderInstance() 
		: m_Data(NULL)
		, m_StructSchema(NULL)
		, m_Structure(NULL)
		, m_Alignment(0)
		, m_Count(0)
		, m_Offset(0)
		, m_Size(0)
	{}

	u32 ComputeSizeAndOffset(u32 prevOffset)
	{
		if (m_Structure)
		{
			parAssertAligned(prevOffset, m_Alignment, m_Structure->GetName());
			m_Offset = prevOffset;
			m_Size = (int) m_Structure->GetSize() * m_Count;
		}
		else
		{
			parAssertAligned(prevOffset, m_Alignment, "POD array");
			m_Offset = prevOffset;
			parAssertf(m_Size, "Size must be specified in advance for POD instances");
		}
		return m_Size;
	}

	void Serialize(char* dest)
	{
		sysMemCpy(dest + m_Offset, m_Data, m_Size);

		for(int i = 0; i < m_IdFixups.GetCount(); i++)
		{
			// find the new address of the offset variable
			size_t offsetOfStructIdVar = reinterpret_cast<const char*>(m_IdFixups[i].m_AddressOfStructId) - m_Data;
			parAssertf(offsetOfStructIdVar < m_Size, "The offset variable is not within this set of instances!");
			psoStructId* addrOfPsoStructId = reinterpret_cast<psoStructId*>(dest + m_Offset + offsetOfStructIdVar);

			if (m_IdFixups[i].m_ReferentData)
			{
				*addrOfPsoStructId = m_IdFixups[i].m_ReferentData->m_Id;
			}
			else
			{
				*addrOfPsoStructId = psoStructId::Null();
			}
		}
	}

	// PURPOSE: Records a fixup for later use
	void AddStructIdFixup(psoStructId& offsetVar, psoBuilderInstance* referentData)
	{
		IdFixupInfo fixup;
		fixup.m_AddressOfStructId = &offsetVar;
		fixup.m_ReferentData = referentData;
		m_IdFixups.PushAndGrow(fixup);
	}
};

struct psoBuilderInstanceSet
{
	u32		m_Offset;
	u32		m_Size;
	atArray<psoBuilderInstance*> m_Instances;

	psoBuilderInstanceSet()
		: m_Offset(0)
		, m_Size(0)
	{}

	~psoBuilderInstanceSet()
	{
		for(int i = 0; i < m_Instances.GetCount(); i++)
		{
			delete m_Instances[i];
		}
	}

	u32 ComputeSizeAndOffset(u32 prevOffset, u32 tableIndex)
	{
		// All instance sets should be 16 byte aligned
		parAssertAligned(prevOffset, 16, "Instance set");
		m_Offset = prevOffset;

		int childSizes = 0;
		for(int i = 0; i < m_Instances.GetCount(); i++)
		{
			u32 newSize = m_Instances[i]->ComputeSizeAndOffset(m_Offset + childSizes);
			m_Instances[i]->m_Id = psoStructId::Create(tableIndex, childSizes); // 'childSizes' here is also the offset into this structArray
			childSizes += newSize;
		}

		m_Size = childSizes;
		return m_Size;
	}

	void Serialize(char* dest)
	{
		for(int i = 0; i < m_Instances.GetCount(); i++)
		{
			m_Instances[i]->Serialize(dest);
		}
	}
};

struct psoBuilderInstanceDataCatalog
{
	atMap<u32, psoBuilderInstanceSet> m_Instances;
	psoBuilderInstance*		m_RootObject;
	atMap<u32, const char*> m_HashStrings;
	u32	m_InstanceSize;
	u32 m_MapSize;
	u32 m_HashStringSize;

	psoBuilderInstanceDataCatalog()
		: m_RootObject(NULL)
		, m_InstanceSize(0)
		, m_MapSize(0)
	{}

	// Adds a single instance (or single array of instances) to the instance data catalog
	psoBuilderInstance& AddInstances(psoBuilderSchemaCatalog& schemaCat, const parStructure& type, const char* instanceData, u32 count)
	{
		psoBuilderInstanceSet& set = m_Instances[type.GetNameHash()];

		psoBuilderInstance* newInst = rage_new psoBuilderInstance;
		newInst->m_Alignment = 1; // Assume the structure sizes are padded-to-alignment, so as long as a structarray starts on a 16 byte boundary everything should be properly aligned.
		newInst->m_Data = instanceData;
		newInst->m_Structure = &type;
		newInst->m_StructSchema = schemaCat.FindStruct(type.GetNameHash());
		newInst->m_Count = count;

		set.m_Instances.PushAndGrow(newInst);
		return *set.m_Instances.Top();
	}

	psoBuilderInstance& AddPodArray(const char* object, u32 sizeInBytes, parMemberType::Enum type)
	{
		psoBuilderInstanceSet& set = m_Instances[type];

		int alignment = 16;
		switch(type)
		{
		case parMemberType::TYPE_BOOL:		
		case parMemberType::TYPE_CHAR:		
		case parMemberType::TYPE_UCHAR:		
			alignment = 1; break;
		case parMemberType::TYPE_SHORT:	
		case parMemberType::TYPE_USHORT:
			alignment = 2; break;
		case parMemberType::TYPE_INT:
		case parMemberType::TYPE_UINT:
		case parMemberType::TYPE_FLOAT:		
		case parMemberType::TYPE_VECTOR2:	
			alignment = 4; break;
		case parMemberType::TYPE_STRUCT:
			alignment = 4; break; // Special case - A POD array of TYPE_STRUCT really means an array of psoStructIds (i.e. pointers)
		case parMemberType::TYPE_SCALARV:	
		case parMemberType::TYPE_BOOLV:		
		case parMemberType::TYPE_VECTOR3:	
		case parMemberType::TYPE_VECTOR4:
		case parMemberType::TYPE_VEC2V:
		case parMemberType::TYPE_VEC3V:
		case parMemberType::TYPE_VEC4V:
		case parMemberType::TYPE_VECBOOLV:
		case parMemberType::TYPE_MATRIX34:
		case parMemberType::TYPE_MATRIX44:
		case parMemberType::TYPE_MAT33V:
		case parMemberType::TYPE_MAT34V:
		case parMemberType::TYPE_MAT44V:
		default:
			alignment = 16; break;
		}

		psoBuilderInstance* newInst = rage_new psoBuilderInstance;
		newInst->m_Alignment = alignment;
		newInst->m_Data = object;
		newInst->m_Size = sizeInBytes;
		newInst->m_PodType = type;

		set.m_Instances.PushAndGrow(newInst);

		return *set.m_Instances.Top();
	}

	void AddHashString(atHashString& s)
	{
		if (s.IsNotNull())
		{
			m_HashStrings[s.GetHash()] = s.GetCStr();
		}
	}

	// Only callable after construction is complete.
	psoStructId FindPodArray(parMemberType::Enum type, const char* originalData)
	{
		int instSetIndex = 0;
		atMap<u32, psoBuilderInstanceSet>::Iterator iter = m_Instances.CreateIterator();
		for(iter.Start(); !iter.AtEnd(); iter.Next())
		{
			psoBuilderInstanceSet& instSet = iter.GetData();
			if (instSet.m_Instances.GetCount() > 0)
			{
				if (instSet.m_Instances[0]->m_Structure || instSet.m_Instances[0]->m_PodType != type)
				{
					continue;
				}
				// loop over the instances to see if instanceData is there
				for(int i = 0; i < instSet.m_Instances.GetCount(); i++)
				{
					if (instSet.m_Instances[i]->m_Data == originalData)
					{
						return instSet.m_Instances[i]->m_Id;
					}
				}
			}
			instSetIndex++;
		}

		return psoStructId();
	}

	// Only callable after construction is complete?
	psoStructId FindObjectId(const parStructure& type, const char* instanceData)
	{
		int instSetIndex = 0;
		atMap<u32, psoBuilderInstanceSet>::Iterator iter = m_Instances.CreateIterator();
		for(iter.Start(); !iter.AtEnd(); iter.Next())
		{
			psoBuilderInstanceSet& instSet = iter.GetData();
			if (instSet.m_Instances.GetCount() > 0)
			{
				const parStructure* instSetStructure = instSet.m_Instances[0]->m_Structure;
				if (instSetStructure == &type)
				{
					// loop over the instances to see if instanceData is there
					for(int i = 0; i < instSet.m_Instances.GetCount(); i++)
					{
						if (instSet.m_Instances[i]->m_Data == instanceData)
						{
							return instSet.m_Instances[i]->m_Id;
						}
					}
				}
			}
			instSetIndex++;
		}

		return psoStructId::Null();
	}

	// Note doesn't take an offset parameter here because all of the offsets we compute for the
	// child objects should be relative to this one
	void ComputeSizeAndOffset()
	{
		u32 mapHeaderSize = sizeof(psoStructureMapData);
		u32 structArrayTableSize = sizeof(psoStructArrayTableData) * m_Instances.GetNumUsed();

		m_MapSize = RoundUp<4>(mapHeaderSize + structArrayTableSize);

		u32 instHeaderSize = sizeof(psoStructureData);

		u32 instanceOffset = instHeaderSize;

		atMap<u32, psoBuilderInstanceSet>::Iterator instSetIter = m_Instances.CreateIterator();
		u32 tableIndex = 0;
		for(instSetIter.Start(); !instSetIter.AtEnd(); instSetIter.Next())
		{
			instanceOffset = RoundUp<16>(instanceOffset); // All instance sets need to start on a 16 byte boundary
			psoBuilderInstanceSet& set = instSetIter.GetData();
			u32 instanceSetSize = set.ComputeSizeAndOffset(instanceOffset, tableIndex);
			instanceOffset += instanceSetSize;
			tableIndex++;
		}

		m_InstanceSize = RoundUp<4>(instanceOffset);

		m_HashStringSize = sizeof(parIffHeader);
		atMap<u32, const char*>::Iterator stringIter = m_HashStrings.CreateIterator();
		for(stringIter.Start(); !stringIter.AtEnd(); stringIter.Next())
		{
			m_HashStringSize += (u32)strlen(stringIter.GetData()) + 1;
		}
		m_HashStringSize = RoundUp<4>(m_HashStringSize);
	}

	void SerializeMap(char* dest)
	{
		psoStructureMapData* mapHeader = reinterpret_cast<psoStructureMapData*>(dest);
		mapHeader->m_Header.SetMagic(psoConstants::PMAP_MAGIC_NUMBER);
		mapHeader->m_Header.SetSize(m_MapSize);

		psoStructId root = FindObjectId(*m_RootObject->m_Structure, m_RootObject->m_Data);
		mapHeader->m_RootObject = root;

		psoStructArrayTableData* structArrayTableData = reinterpret_cast<psoStructArrayTableData*>(dest + sizeof(psoStructureMapData));

		atMap<u32, psoBuilderInstanceSet>::Iterator iter = m_Instances.CreateIterator();

		u16 numArrays = 0;

		for(iter.Start(); !iter.AtEnd(); iter.Next())
		{
			psoBuilderInstanceSet& set = iter.GetData();
			structArrayTableData->m_TypeHash = iter.GetKey();
			structArrayTableData->m_Offset = set.m_Offset;
			structArrayTableData->m_Size = set.m_Size;
			structArrayTableData++;
			numArrays++;
		}

		mapHeader->m_NumStructArrays = numArrays;
	}


	void SerializeInstances(char* dest)
	{
		psoStructureData* instHeader = reinterpret_cast<psoStructureData*>(dest);
		instHeader->m_Header.SetMagic( psoConstants::PSIN_MAGIC_NUMBER ); 
		instHeader->m_Header.SetSize( m_InstanceSize );

		atMap<u32, psoBuilderInstanceSet>::Iterator iter = m_Instances.CreateIterator();

		for(iter.Start(); !iter.AtEnd(); iter.Next())
		{
			iter.GetData().Serialize(dest);
		}
	}

	void SerializeHashStrings(char* dest)
	{
		parIffHeader* stringsHeader = reinterpret_cast<parIffHeader*>(dest);
		stringsHeader->SetMagic(psoConstants::STRS_MAGIC_NUMBER);
		stringsHeader->SetSize(m_HashStringSize);

		atMap<u32, const char*>::Iterator iter = m_HashStrings.CreateIterator();

		dest += sizeof(parIffHeader);

		for(iter.Start(); !iter.AtEnd(); iter.Next())
		{
			size_t len = strlen(iter.GetData()) + 1;
			sysMemCpy(dest, iter.GetData(), len);
			dest += len;
		}
	}
};


// This one iterates over all of the sub-objects in a structure graph and builds up the instance catalog
// containing a description of all of our instance data
class BuildCatalogsVisitor : public parInstanceVisitor
{
public:
	BuildCatalogsVisitor(psoBuilderInstanceDataCatalog& instanceCat, psoBuilderSchemaCatalog& schemaCat)
		: m_InstanceCatalog(&instanceCat)
		, m_SchemaCatalog(&schemaCat)
	{
	}

	virtual void VisitToplevelStructure(void* instanceDataPtr, parStructure& metadata)
	{
		m_SchemaCatalog->AddStructSchema(metadata);
		psoBuilderInstance& inst = m_InstanceCatalog->AddInstances(*m_SchemaCatalog, metadata, reinterpret_cast<const char*>(instanceDataPtr), 1);

		m_InstanceStack.PushAndGrow(&inst);
		m_InstanceCatalog->m_RootObject = &inst;

		parInstanceVisitor::VisitToplevelStructure(instanceDataPtr, metadata);
	}

	virtual bool BeginStructMember(void* /*ptrToStruct*/, parMemberStruct& metadata)
	{
		m_SchemaCatalog->AddStructSchema(*metadata.GetBaseStructure());
		return true;
	}

	virtual bool BeginPointerMember(void*& ptrRef, parMemberStruct& metadata)
	{
		if (ptrRef)
		{
			parStructure* concreteStruct = metadata.GetConcreteStructure(ptrRef);
			m_SchemaCatalog->AddStructSchema(*concreteStruct);
			psoBuilderInstance& ptrTarget = m_InstanceCatalog->AddInstances(*m_SchemaCatalog, *concreteStruct, reinterpret_cast<const char*>(ptrRef), 1);
			m_InstanceStack.Top()->AddStructIdFixup(*reinterpret_cast<psoStructId*>(&ptrRef), &ptrTarget);
			m_InstanceStack.PushAndGrow(&ptrTarget);
		}
		else
		{
			m_InstanceStack.PushAndGrow(NULL); // push something so we pop something later
		}

		return true;
	}

	virtual void EndPointerMember(void*& /*ptrRef*/, parMemberStruct& /*metadata*/)
	{
		m_InstanceStack.Pop();
	}

	virtual bool BeginArrayMember(void* ptrToMember, void* arrayContentsVoidPtr, u32 numElements, parMemberArray& metadata)
	{
		using namespace parMemberType;

		if (!metadata.HasExternalStorage())
		{
			return true; // Don't have to do anything special for these
		}

		char* arrayContents = reinterpret_cast<char*>(arrayContentsVoidPtr);

		// The 'parent' here is the thing that contains the array 'header'. The child is the thing that contains the array data.
		psoBuilderInstance* parentInstance = m_InstanceStack.Top();

		// Add a placeholder for the instance data for this array (though we may not actually use it, we need to make sure something's
		// on the stack to pop off in EndArrayMember
		psoBuilderInstance*& childInstances = m_InstanceStack.Grow();
		childInstances = NULL;

		psoStructId* addressOfId = NULL; // the address of the pointer, which will also be the address of the structId we'll store here
		switch(metadata.GetSubtype())
		{
		case parMemberArraySubType::SUBTYPE_POINTER:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
		case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
			addressOfId = reinterpret_cast<psoStructId*>(ptrToMember);
			break;
		case parMemberArraySubType::SUBTYPE_ATARRAY:
			{
				psoFakeAtArray16* fakeArray = reinterpret_cast<psoFakeAtArray16*>(ptrToMember);
				addressOfId = &fakeArray->m_StructId;
			}
			break;
		case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:
			{
				psoFakeAtArray32* fakeArray = reinterpret_cast<psoFakeAtArray32*>(ptrToMember);
				addressOfId = &fakeArray->m_StructId;
			}
			break;
		case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:
		case parMemberArraySubType::SUBTYPE_ATRANGEARRAY:
		case parMemberArraySubType::SUBTYPE_MEMBER:
			parErrorf("Shouldn't get here - has internal storage!");
			return true;

		case parMemberArraySubType::SUBTYPE_VIRTUAL:
			parErrorf("Arrays of type %d can't go in PSO files", metadata.GetSubtype());
			return false;
		}

		if (numElements == 0)
		{
			// Need to make sure we're actually pointing to NULL - not just an empty array
			parentInstance->AddStructIdFixup(*addressOfId, NULL);
			return false;
		}

		size_t arraySizeInBytes = numElements * metadata.GetPrototypeMember()->GetSize();

		bool visitKids = false;

		// This switch has to do a couple things...
		// 1) Add any array data to the instance catalog, and set the 'childInstances' pointer
		// 2) Add any schemas to the schema catalog
		// 3) Determine if we need to recurse into the child elements

		switch(metadata.GetPrototypeMember()->GetType())
		{
		case TYPE_BOOL:		
		case TYPE_CHAR:		
		case TYPE_UCHAR:		
		case TYPE_SHORT:	
		case TYPE_USHORT:
		case TYPE_INT:
		case TYPE_UINT:
		case TYPE_FLOAT:		
		case TYPE_SCALARV:	
		case TYPE_BOOLV:		
		case TYPE_VECTOR2:	
		case TYPE_VECTOR3:	
		case TYPE_VECTOR4:
		case TYPE_VEC2V:
		case TYPE_VEC3V:
		case TYPE_VEC4V:
		case TYPE_VECBOOLV:
		case TYPE_MATRIX34:
		case TYPE_MATRIX44:
		case TYPE_MAT33V:
		case TYPE_MAT34V:
		case TYPE_MAT44V:
			childInstances = &m_InstanceCatalog->AddPodArray(arrayContents, (int)arraySizeInBytes, metadata.GetPrototypeMember()->GetType());
			break;

		case parMemberType::TYPE_STRUCT:
			{
				parMemberStruct& protoMember = *smart_cast<parMemberStruct*>(metadata.GetPrototypeMember());

				switch(protoMember.GetSubtype())
				{
				case parMemberStructSubType::SUBTYPE_STRUCTURE:
					m_SchemaCatalog->AddStructSchema(*protoMember.GetBaseStructure());
					childInstances = &m_InstanceCatalog->AddInstances(*m_SchemaCatalog, *protoMember.GetBaseStructure(), reinterpret_cast<char*>(arrayContents), numElements);
					visitKids = true;
					break;
				case parMemberStructSubType::SUBTYPE_POINTER:
				case parMemberStructSubType::SUBTYPE_SIMPLE_POINTER:
					// HACK - write these out as a POD array of TYPE_STRUCT for now. They're really an array of psoStructIds.
					childInstances = &m_InstanceCatalog->AddPodArray(arrayContents, (int)arraySizeInBytes, TYPE_STRUCT);
					visitKids = true;
					break;
				default:
					parErrorf("Not yet supported");
					break;
				}
			}
			break;

		case parMemberType::TYPE_BITSET:
			{
				parMemberBitset& protoMember = *smart_cast<parMemberBitset*>(metadata.GetPrototypeMember());

				m_SchemaCatalog->AddEnumSchema(*(protoMember.GetData()->m_EnumData));

				parMemberType::Enum podType = INVALID_TYPE;
				switch((parMemberBitsetSubType::Enum)protoMember.GetSubtype())
				{
				case parMemberBitsetSubType::SUBTYPE_32BIT_FIXED: podType = TYPE_UINT; break;
				case parMemberBitsetSubType::SUBTYPE_16BIT_FIXED: podType = TYPE_USHORT; break;
				case parMemberBitsetSubType::SUBTYPE_8BIT_FIXED: podType = TYPE_UCHAR; break;
				case parMemberBitsetSubType::SUBTYPE_ATBITSET:
					parErrorf("Not yet supported");
					break;
				}

				if (podType != parMemberType::INVALID_TYPE)
				{
					childInstances = &m_InstanceCatalog->AddPodArray(arrayContents, (int)arraySizeInBytes, podType);
				}
			}
			break;

		case parMemberType::TYPE_ENUM:
			{
				parMemberEnum& protoMember = *smart_cast<parMemberEnum*>(metadata.GetPrototypeMember());

				m_SchemaCatalog->AddEnumSchema(*(protoMember.GetData()->m_EnumData));

				parMemberType::Enum podType = INVALID_TYPE;
				switch((parMemberEnumSubType::Enum)protoMember.GetSubtype())
				{
				case parMemberEnumSubType::SUBTYPE_32BIT: podType = TYPE_UINT; break;
				case parMemberEnumSubType::SUBTYPE_16BIT: podType = TYPE_USHORT; break;
				case parMemberEnumSubType::SUBTYPE_8BIT: podType = TYPE_UCHAR; break;
				}

				childInstances = &m_InstanceCatalog->AddPodArray(arrayContents, (int)arraySizeInBytes, podType);
			}
			break;

		case parMemberType::TYPE_STRING:
			{
				parMemberString& protoMember = *smart_cast<parMemberString*>(metadata.GetPrototypeMember());

				switch((parMemberStringSubType::Enum)protoMember.GetSubtype())
				{
				case parMemberStringSubType::SUBTYPE_MEMBER:
					childInstances = &m_InstanceCatalog->AddPodArray(arrayContents, (int)arraySizeInBytes, TYPE_CHAR);
					break;
				case parMemberStringSubType::SUBTYPE_WIDE_MEMBER:
					childInstances = &m_InstanceCatalog->AddPodArray(arrayContents, (int)arraySizeInBytes, TYPE_UCHAR);
					break;
				case parMemberStringSubType::SUBTYPE_ATHASHSTRING:
					childInstances = &m_InstanceCatalog->AddPodArray(arrayContents, (int)arraySizeInBytes, TYPE_UINT);
					visitKids = true;
					break;	  
				case parMemberStringSubType::SUBTYPE_POINTER:
				case parMemberStringSubType::SUBTYPE_CONST_STRING:
				case parMemberStringSubType::SUBTYPE_WIDE_POINTER:
					// Child data is an array of pointers - which becomes an array of psoStructIds - aka TYPE_STRUCT
					childInstances = &m_InstanceCatalog->AddPodArray(arrayContents, (int)arraySizeInBytes, TYPE_STRUCT);
					visitKids = true; // to add pointer fixups
					break;
				case parMemberStringSubType::SUBTYPE_ATSTRING:
				case parMemberStringSubType::SUBTYPE_ATWIDESTRING:
					parErrorf("Arrays of strings of these types don't work in PSO files");
					break;
				}
				break;
			}
		case parMemberType::TYPE_ARRAY:
			parErrorf("Arrays of these types don't work in PSO files yet. Use an array of structures that contain these things instead.");
			break;
		case parMemberType::INVALID_TYPE:
			parErrorf("Shouldn't get here");
			break;
		}

		// Wherever the array pointer was (addressOfId), replace that with a psoStructId that points to childInstances
		parentInstance->AddStructIdFixup(*addressOfId, childInstances);

		return visitKids;
	}

	virtual void EndArrayMember(void* /*ptrToMember*/, void* /*arrayContents*/, u32 /*numElements*/, parMemberArray& metadata)
	{
		if (!metadata.HasExternalStorage())
		{
			return; // Don't have to do anything special for these
		}

		m_InstanceStack.Pop();
	}

	virtual void EnumMember(int& /*data*/, parMemberEnum& metadata)
	{
		m_SchemaCatalog->AddEnumSchema(*(metadata.GetData()->m_EnumData));
	}

	virtual void PointerStringMember(char*& stringData, parMemberString& /*metadata*/)
	{
		psoBuilderInstance* arrContentsInfo = stringData ? &m_InstanceCatalog->AddPodArray(stringData, (int) strlen(stringData) + 1, parMemberType::TYPE_CHAR) : NULL;
		m_InstanceStack.Top()->AddStructIdFixup(reinterpret_cast<psoStructId&>(stringData), arrContentsInfo);
	}

	virtual void ConstStringMember(ConstString& stringData, parMemberString& /*metadata*/)
	{
		psoBuilderInstance* arrContentsInfo = stringData ? &m_InstanceCatalog->AddPodArray(stringData.c_str(), (int) strlen(stringData.c_str()) + 1, parMemberType::TYPE_CHAR) : NULL;
		m_InstanceStack.Top()->AddStructIdFixup(reinterpret_cast<psoStructId&>(stringData), arrContentsInfo);
	}

	virtual void AtHashStringMember(atHashString& stringData, parMemberString& /*metadata*/)
	{
		m_InstanceCatalog->AddHashString(stringData);
	}

	virtual void WidePointerStringMember(char16*& stringData, parMemberString& /*metadata*/)
	{
		psoBuilderInstance* arrContentsInfo = stringData ? &m_InstanceCatalog->AddPodArray(reinterpret_cast<char*>(stringData), sizeof(char16) * ( (int) wcslen(stringData) + 1), parMemberType::TYPE_USHORT) : NULL;
		m_InstanceStack.Top()->AddStructIdFixup(reinterpret_cast<psoStructId&>(stringData), arrContentsInfo);
	}

	virtual void AtStringMember(atString& stringData, parMemberString& /*metadata*/)
	{
		psoFakeAtString& fakeString = reinterpret_cast<psoFakeAtString&>(stringData);
		if (stringData.GetLength())
		{
			psoBuilderInstance* arrContentsInfo = &m_InstanceCatalog->AddPodArray(const_cast<char*>(stringData.c_str()), stringData.GetLength()+1, parMemberType::TYPE_CHAR);
			m_InstanceStack.Top()->AddStructIdFixup(fakeString.m_StructId, arrContentsInfo);
		}
		else
		{
			// Need to actually point to NULL, not to an empty string
			m_InstanceStack.Top()->AddStructIdFixup(fakeString.m_StructId, NULL);
		}
	}

	virtual void AtWideStringMember(atWideString& stringData, parMemberString& /*metadata*/)
	{
		psoFakeAtString& fakeString = reinterpret_cast<psoFakeAtString&>(stringData);
		if (stringData.GetLength())
		{
			const char16* realStringData = stringData;
			char16* nonConstStringData = const_cast<char16*>(realStringData);
			psoBuilderInstance* arrContentsInfo = &m_InstanceCatalog->AddPodArray(reinterpret_cast<char*>(nonConstStringData), sizeof(char16) * (stringData.GetLength()+1), parMemberType::TYPE_USHORT);
			m_InstanceStack.Top()->AddStructIdFixup(fakeString.m_StructId, arrContentsInfo);
		}
		else
		{
			// Need to actually point to NULL, not to an empty string
			m_InstanceStack.Top()->AddStructIdFixup(fakeString.m_StructId, NULL);
		}
	}

	virtual void BitsetMember(void* ptrToMember, void* ptrToBits, int numBits, parMemberBitset& metadata)
	{
		m_SchemaCatalog->AddEnumSchema(*(metadata.GetData()->m_EnumData));

		if (metadata.GetSubtype() == parMemberBitsetSubType::SUBTYPE_ATBITSET)
		{
			psoFakeAtBitset* fakeBitset = reinterpret_cast<psoFakeAtBitset*>(ptrToMember);
			if (numBits > 0)
			{
				int numWords = (numBits + 31) / 32;
				psoBuilderInstance* arrContentsInfo = &m_InstanceCatalog->AddPodArray(reinterpret_cast<char*>(ptrToBits), numWords * sizeof(u32), parMemberType::TYPE_UINT);
				m_InstanceStack.Top()->AddStructIdFixup(fakeBitset->m_StructId, arrContentsInfo);
			}
			else
			{
				m_InstanceStack.Top()->AddStructIdFixup(fakeBitset->m_StructId, NULL);
			}
		}
	}

private:
	atArray<psoBuilderInstance*>	m_InstanceStack;
	psoBuilderInstanceDataCatalog* m_InstanceCatalog;
	psoBuilderSchemaCatalog* m_SchemaCatalog;

};

bool rage::psoSaveFromStructure(const char* filename, const parStructure& structure, const void* obj)
{
	parDebugf1("Saving to file '%s' with structure '%s'", filename, structure.GetName());
	DIAG_CONTEXT_MESSAGE("Saving to file '%s' with structure '%s'", filename, structure.GetName());
	fiSafeStream S(ASSET.Create(filename, ""));
	if (!S)
	{
		return false;
	}

	// Creation happens in a few stages...
	// 1: Build the schema and instance catalogs
	// 2: Find all the sizes and offsets of all the member objects that will be in those catalogs
	// 3: All the objects write out data to their sections of the file
	// 4: Convert what used to be pointers in the file data into offsets or objectIDs, and byte swap to little endian

	psoBuilderSchemaCatalog schemaCat;
	psoBuilderInstanceDataCatalog instanceCat;

	BuildCatalogsVisitor catalogBuilder(instanceCat, schemaCat);
	catalogBuilder.VisitToplevelStructure(const_cast<void*>(obj), const_cast<parStructure&>(structure));


	schemaCat.ComputeSizeAndOffset();
	instanceCat.ComputeSizeAndOffset();

	// Note! If the instance data section is ever not at the beginning
	// of the file, make sure it starts on a 16 byte boundary

	u32 totalSize = schemaCat.m_Size + instanceCat.m_InstanceSize + instanceCat.m_MapSize + instanceCat.m_HashStringSize;

	char* buff = rage_aligned_new(16) char[totalSize];
	
	sysMemSet(buff, 0x70, totalSize);

	char* startOfInstanceCat = buff;
	char* startOfMap = startOfInstanceCat + instanceCat.m_InstanceSize;
	char* startOfSchemaCat = startOfMap + instanceCat.m_MapSize;
	char* startOfHashStrings = startOfSchemaCat + schemaCat.m_Size;

	instanceCat.SerializeInstances(startOfInstanceCat);

	instanceCat.SerializeMap(startOfMap);

	schemaCat.Serialize(startOfSchemaCat);

	instanceCat.SerializeHashStrings(startOfHashStrings);

#if !__BE
	psoFile f;
	f.Init(buff, totalSize, false, false);
	psoSwapFromNative(f);
#endif

	S->Write(buff, totalSize);

	S->Flush();

	delete [] buff;

	return true;
}
