// 
// parser/visitorvalidate.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "visitorvalidate.h"

#include "optimisations.h"

#include "math/float16.h"
#include "vectormath/scalarv.h"

PARSER_OPTIMISATIONS();

namespace rage {

#if __BANK && __DEV

void parValidateVisitor::SimpleMember(parPtrToMember ptrToMember, parMemberSimple& metadata )
{
	float fvalue = 0.0f; // the mins and maxes are stored as floats, so convert the member val to a float to compare
	switch(metadata.GetType())
	{
	case parMemberType::TYPE_INT:	fvalue = float(*reinterpret_cast<s32*>(ptrToMember)); break;
	case parMemberType::TYPE_UINT:	fvalue = float(*reinterpret_cast<u32*>(ptrToMember)); break;
	case parMemberType::TYPE_PTRDIFFT:	fvalue = float(*reinterpret_cast<ptrdiff_t*>(ptrToMember)); break;
	case parMemberType::TYPE_SIZET:	fvalue = float(*reinterpret_cast<size_t*>(ptrToMember)); break;
	case parMemberType::TYPE_SHORT:	fvalue = float(*reinterpret_cast<s16*>(ptrToMember)); break;
	case parMemberType::TYPE_USHORT:	fvalue = float(*reinterpret_cast<u16*>(ptrToMember)); break;
	case parMemberType::TYPE_CHAR:	fvalue = float(*reinterpret_cast<s8*>(ptrToMember)); break;
	case parMemberType::TYPE_UCHAR:	fvalue = float(*reinterpret_cast<u8*>(ptrToMember)); break;
	case parMemberType::TYPE_FLOAT:	fvalue = float(*reinterpret_cast<float*>(ptrToMember)); break;
	case parMemberType::TYPE_FLOAT16:	fvalue = reinterpret_cast<Float16*>(ptrToMember)->GetFloat32_FromFloat16(); break;
	case parMemberType::TYPE_SCALARV:	fvalue = reinterpret_cast<ScalarV*>(ptrToMember)->Getf(); break;
	case parMemberType::TYPE_INT64:	fvalue = float(*reinterpret_cast<s64*>(ptrToMember)); break;
	case parMemberType::TYPE_UINT64:	fvalue = float(*reinterpret_cast<u64*>(ptrToMember)); break;
	case parMemberType::TYPE_DOUBLE:	fvalue = float(*reinterpret_cast<double*>(ptrToMember)); break;
	case parMemberType::TYPE_BOOL:
	case parMemberType::TYPE_BOOLV:
		// nothing to validate for bools
		return;
	default:
		parErrorf("Unknown type %d for simple member %s in structure %s", metadata.GetType(), metadata.GetName(), m_ContainingStructureMetadata->GetName());
	}

	if (fvalue < metadata.GetData()->m_Min ||
		fvalue > metadata.GetData()->m_Max) {
		parWarningf("Value %f is out of range [%f,%f] for member %s in structure %s", fvalue, metadata.GetData()->m_Min, metadata.GetData()->m_Max, metadata.GetName(), m_ContainingStructureMetadata->GetName());
		m_Success = false;
	}
}

void parValidateVisitor::VectorMember( parPtrToMember ptrToMember, parMemberVector& metadata )
{
	float* floatData = reinterpret_cast<float*>(ptrToMember);
	int elementCount = 0;
	switch(metadata.GetType())
	{
	case parMemberType::TYPE_VECBOOLV: elementCount = 0; break; // No validation necessary
	case parMemberType::TYPE_VEC2V: elementCount = 2; break;
	case parMemberType::TYPE_VEC3V: elementCount = 3; break;
	case parMemberType::TYPE_VEC4V: elementCount = 4; break;
	case parMemberType::TYPE_VECTOR2: elementCount = 2; break;
	case parMemberType::TYPE_VECTOR3: elementCount = 3; break;
	case parMemberType::TYPE_VECTOR4: elementCount = 4; break;
	default:
		parErrorf("Unknown type %d for vector member %s in structure %s", metadata.GetType(), metadata.GetName(), m_ContainingStructureMetadata->GetName());
	}

	float min = metadata.GetData()->m_Min;
	float max = metadata.GetData()->m_Max;

	const char channels[] = {'x','y','z','w'};

	for(int i = 0; i < elementCount; i++)
	{
		if (floatData[i] < min || floatData[i] > max)
		{
			parWarningf("Value %f is out of range [%f, %f] for member %s.%c in structure %s",
				floatData[i], min, max, 
				metadata.GetName(), channels[i], m_ContainingStructureMetadata->GetName());
			m_Success = false;
		}
	}
}

void parValidateVisitor::MatrixMember( parPtrToMember ptrToMember, parMemberMatrix& metadata )
{
	float* floatData = reinterpret_cast<float*>(ptrToMember);
	int rowCount = 0;
	int colCount = 0;
	int stride = 0;
	bool rowMajor = false;
	switch(metadata.GetType())
	{
	case parMemberType::TYPE_MAT33V: rowMajor = false; rowCount=3; colCount=3; stride=4; break;
	case parMemberType::TYPE_MAT34V: rowMajor = false; rowCount=3; colCount=4; stride=4; break;
	case parMemberType::TYPE_MAT44V: rowMajor = false; rowCount=4; colCount=4; stride=4; break;
	case parMemberType::TYPE_MATRIX34: rowMajor = true; rowCount=4; colCount=3; stride=4; break;
	case parMemberType::TYPE_MATRIX44: rowMajor = true; rowCount=4; colCount=4; stride=4; break;
	default:
		parErrorf("Unknown type %d for matrix member %s in structure %s", metadata.GetType(), metadata.GetName(), m_ContainingStructureMetadata->GetName());
	}

	float min = metadata.GetData()->m_Min;
	float max = metadata.GetData()->m_Max;

	const char rows[] = {'a','b','c','d'};
	const char columns[] = {'x','y','z','w'};

	for(int row = 0; row < rowCount; row++)
	{
		for(int col = 0; col < colCount; col++)
		{
			if (rowMajor)
			{
				int itemIdx = row * stride + col;
				if (floatData[itemIdx] < min || floatData[itemIdx] > max)
				{
					parWarningf("Value %f is out of range [%f, %f] for member %s.%c.%c in structure %s",
						floatData[itemIdx], min, max, 
						metadata.GetName(), rows[row], columns[col], m_ContainingStructureMetadata->GetName());
					m_Success = false;
				}
			}
			else
			{
				int itemIdx = col * stride + row;
				if (floatData[itemIdx] < min || floatData[itemIdx] > max)
				{
					parWarningf("Value %f is out of range [%f, %f] for member %s[%d].%c in structure %s",
						floatData[itemIdx], min, max, 
						metadata.GetName(), col, rows[row], m_ContainingStructureMetadata->GetName());
					m_Success = false;
				}
			}
		}
		floatData += stride;
	}

}

void parValidateVisitor::EnumMember( int& data, parMemberEnum& metadata )
{
	const char* valName = metadata.NameFromValue(data);
	if (!valName)
	{
		parWarningf("Value %d wasn't found in enumeration for member %s in structure %s",
			data, metadata.GetName(), m_ContainingStructureMetadata->GetName());
		m_Success = false;
	}
}

void parValidateVisitor::BitsetMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*ptrToBits*/, size_t numBits, parMemberBitset& metadata)
{
	if (metadata.GetData()->m_EnumData->m_Names)
	{
		// If we have names, make sure every 'true' bit is named.
		for(int i = 0; i < (int)numBits; i++)
		{
			if (metadata.GetBit(m_ContainingStructureAddress, i))
			{
				if (metadata.NameFromValue(i) == NULL)
				{
					parWarningf("Couldn't find a name for bit %d in member %s in structure %s", i, metadata.GetName(), m_ContainingStructureMetadata->GetName());
				}
			}
		}
	}
}


bool parValidateVisitor::ValidatePointer(const void* data, parMember& member)
{
	// Not too many things to check here...
	// just look for some known bad patterns
	if (data == (void*)0xCCCCCCCC ||
		data == (void*)0xCDCDCDCD ||
		data == (void*)0xDDDDDDDD ||
		data == (void*)0xEEEEEEEE)
	{
		parWarningf("Validation failed for pointer %s in structure %s. Pointer value was 0x%p", member.GetName(), m_ContainingStructureMetadata->GetName(), data);
		m_Success = false;
		return false;
	}
	return true;
}

void parValidateVisitor::ExternalPointerMember( void*& data, parMemberStruct& metadata )
{
	ValidatePointer(data, metadata);
}

void parValidateVisitor::StringMember( parPtrToMember /*ptrToMember*/, const char* ptrToString, parMemberString& metadata )
{
	ValidatePointer(ptrToString, metadata);
}

bool parValidateVisitor::BeginArrayMember( parPtrToMember /*ptrToMember*/, parPtrToArray arrayContents, size_t /*numElements*/, parMemberArray& metadata )
{
	return ValidatePointer(arrayContents, metadata);
}

bool parValidateVisitor::BeginPointerMember( parPtrToStructure& ptrRef, parMemberStruct& metadata )
{
	return ValidatePointer(ptrRef, metadata);
}

#endif // __BANK && __DEV

} // namespace rage
