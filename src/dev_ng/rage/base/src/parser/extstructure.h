// 
// parser/extstructure.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_EXTSTRUCTURE_H 
#define PARSER_EXTSTRUCTURE_H 

#include "macros.h"
#include "structdefs.h"

#include "atl/array.h"
#include "atl/map.h"

namespace rage {

struct parEnumData;
class parTreeNode;
class parStructure;

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS

void parAddReflectionClassesToManager(parManager& mgr);

// PURPOSE: This class is an auxiliary class for parManager, for handling 
//			external structures. These are parsable structures that don't exist in
//			this instance of the executable, but have been defined elsewhere.
// NOTES: The parser doesn't know the difference between an external structure
//			and a structure that represents a real C++ object that's linked in this
//			executable, but we need to keep some bookkeeping data around.
class parExternalStructureManager
{
public:
	parExternalStructureManager();
	void Init();
	void Shutdown();

	// PURPOSE:
	//	Loads external structures from the specified file
	// PARAMS:
	//		filename - The .esd file to load structures from
	void LoadExternalStructureDefns(const char* filename);

	// PURPOSE:
	//  Loads external structures from PSC files - they will be transformed into actual parStructures on demand
	void LoadStructdefs(const char* rootPath);

	void AddInstance(const void* address, parStructure* type);
	void RemoveInstance(const void* address); // Doesn't free memory - just removes it from our "RTTI" map

	parEnumData* FindOrBuildFromEnumdef(u32 enumhash);

protected:
	struct ExternalStructure;
	struct LoadingData;

	friend struct ExternalStructure;
	friend struct LoadingData;
	friend class parStructure;
	friend class parManager;

	parStructure* BuildFromStructdef(u32 structhash);

	atArray<ExternalStructure*> m_ExternalStructures; // A list of all the external structures, along with some extra data

	typedef atMap<const void*, parStructure*> InstanceMap;
	InstanceMap m_Instances; // Instances of external structures

	// For existing (non-external) structures, map from them to a ExternalStructure instance that's
	// just used to hold some delegate data - so that we can patch the GetStructure callbacks for existing classes.
	typedef atMap<parStructure*, ExternalStructure*> PatchMap;
	PatchMap m_PatchedStructures; 

	parStructdefCatalog m_Structdefs;

	typedef atMap<u32, parEnumData*> EnumMap;
	EnumMap m_Enums;
};

#endif // PARSER_USES_EXTERNAL_STRUCTURE_DEFNS

} // namespace rage


#endif // PARSER_EXTSTRUCTURE_H 
