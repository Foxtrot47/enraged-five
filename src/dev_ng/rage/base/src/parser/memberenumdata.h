// 
// parser/memberenumdata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MEMBERENUMDATA_H 
#define PARSER_MEMBERENUMDATA_H 

#include "memberdata.h"

#include "parser/macros.h"
#include "data/callback.h"

namespace rage {

namespace parMemberEnumSubType
{
	enum Enum
	{
		SUBTYPE_32BIT,
		SUBTYPE_16BIT,
		SUBTYPE_8BIT,
	};
}

struct parEnumListEntry {
	u32 m_NameKey;
	int m_Value;
#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_SIMPLE_PARSABLE;
#endif
};

namespace parEnumFlags
{
	enum Enum {
		ENUM_STATIC, // if true, do not delete this member's data, someone else manages it (possibly it's in static data)
		ENUM_HAS_NAMES,
		ENUM_ALWAYS_HAS_NAMES,
	};
}

struct parEnumData {
	enum ValueFromNameStatus
	{
		VFN_NAMED,
		VFN_NUMERIC,
		VFN_ERROR,
		VFN_EMPTY,
	};

	parEnumListEntry* m_Enums;
	const char** m_Names;
	u16 m_NumEnums;
	u16 m_Flags;
	u32 m_NameHash; // hash of the name of the enum
	const char* m_EnumName; // Name of the enum itself. Not always filled out (it depends on the build, and code generation settings)

	int ValueFromName(const char* name, const char** nameEndOut = NULL, ValueFromNameStatus* result = NULL) const;
	int ValueFromName(u32 namehash, ValueFromNameStatus* result = NULL) const;

#if PARSER_ALL_METADATA_HAS_NAMES
	const char* NameFromValue(int i) const { return NameFromValueUnsafe(i); }
#endif

	// RETURNS: The name of the member, but MAY NOT WORK if the structure doesn't have strings (only hashes)
	// NOTES: You have to check the m_Names to see if there are strings or not.
	const char* NameFromValueUnsafe(int i) const;

	u32 HashFromValue(int i) const;

	int MinValue() const;
	int MaxValue() const;

	bool VerifyNamesExist(bool allBuilds, const char* action) const;

	static parEnumData Empty;
	
#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_SIMPLE_PARSABLE
#endif
};

// PURPOSE: All of the data that a parMemberSimple needs to describe the member.
struct parMemberEnumData {
	STANDARD_PARMEMBER_DATA_CONTENTS;
	int m_Init;						// Initial value.
	parEnumData*	m_EnumData;
	u16			   m_NumBits;		// For bitsets (see below) - how many bits are in use
#if __BANK
	const char* m_Description;		// Text description of the member. Used for tooltips in the bank
	datCallback* m_WidgetCb;
#endif
	void Init();
	void PreLoad(parTreeNode*) {Init();}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_SIMPLE_PARSABLE;
#endif
};

namespace parMemberBitsetSubType
{
	enum Enum
	{
		SUBTYPE_32BIT_FIXED, 
		SUBTYPE_16BIT_FIXED,
		SUBTYPE_8BIT_FIXED,
		SUBTYPE_ATBITSET,
	};
}



} // namespace rage

#endif // PARSER_MEMBERENUMDATA_H 
