// 
// parser/psobuilder.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOBUILDER_H
#define PARSER_PSOBUILDER_H

#include "psobuilderstructs.h"
#include "psofaketypes.h"
#include "psoschema.h"
#include "psodata.h"

#include "memberarraydata.h"
#include "memberenumdata.h"
#include "membermapdata.h"
#include "memberstringdata.h"
#include "memberstructdata.h"

namespace rage {

class psoBuilder;
class psoBuilderInstance;

class psoBuilderStructSchema
{
public:
	int AddMemberSimple(atLiteralHashValue namehash, parMemberType::Enum type, u8 subtype, ptrdiff_t offset = -1);
	int AddMemberString(atLiteralHashValue namehash, parMemberStringSubType::Enum subtype, u16 fixedSizeCount, ptrdiff_t offset = -1, u8 namespaceIndex = 0);
	int AddMemberEnum(atLiteralHashValue namehash, parMemberEnumSubType::Enum subtype, atLiteralHashValue enumSchemaNamehash, ptrdiff_t offset = -1);
	int AddMemberStruct(atLiteralHashValue namehash, psoBuilderStructSchema& memberSchema, ptrdiff_t offset = -1);
	int AddMemberStruct(atLiteralHashValue namehash, atLiteralHashValue nameOfTheStruct, ptrdiff_t offset = -1);
	int AddMemberPointer(atLiteralHashValue namehash, parMemberStructSubType::Enum subtype, ptrdiff_t offset = -1);
	int AddMemberArray(atLiteralHashValue namehash, parMemberArraySubType::Enum subtype, int indexOfArrayContentMember, u16 fixedSizeCount, ptrdiff_t offset = -1, u32 alignmentPower = 0);
	int AddMemberBitset(atLiteralHashValue namehash, parMemberBitsetSubType::Enum subtype, int indexOfBitsetEnumMember, u16 numBits, ptrdiff_t offset = -1);
	int AddMemberMap(atLiteralHashValue namehash, parMemberMapSubType::Enum subtype, int indexOfKeyMember, int indexOfValueMember, ptrdiff_t offset = -1);

	// NOTES: 
	// If you pass 0 for the new name, we just copy the old name
	// Does NOT copy the offset. Pass in member.GetOffset() if you really want it to.
	int CopyMember(psoSchemaMemberData& member, atLiteralHashValue newName = atLiteralHashValue::Null(), ptrdiff_t offset = -1);

	ptrdiff_t AddPadding(size_t pad, size_t align = 1); // Inserts N bytes of padding, plus addtional padding to align the padding space if necessary

	void FinishBuilding();

	psoSchemaStructureData* GetSchemaData() { return m_UnderConstruction ? NULL : m_Structure; }

	~psoBuilderStructSchema();

protected:
	friend psoBuilder;
	friend psoBuilderSchemaCatalog;
	friend psoBuilderInstance;

	psoBuilderStructSchema(psoBuilder* builder);

	int AddMemberRawRefHash(atLiteralHashValue namehash, parMemberType::Enum type, int subtype, ptrdiff_t offset, atLiteralHashValue refhash);
	int AddMemberRawMemIdxCount(atLiteralHashValue namehash, parMemberType::Enum type, int subtype, ptrdiff_t offset, u16 memIdx, u16 count, u32 alignPower);
	int AddMemberRawKeyValueMemIndices(atLiteralHashValue namehash, parMemberType::Enum type, int subtype, ptrdiff_t offset, u16 keyMemIdx, u16 valueMemIdx);

	u32 ComputeSize();

	u32 ComputeFileLocation(u32 prevOffset);
	void Serialize(char* dest);

	void ComputeOffsetAndUpdateSize(psoSchemaMemberData& mem, ptrdiff_t offset);
	void FindMemberTraits(psoSchemaMemberData& mem, size_t& size, size_t& align);

	psoBuilder*	m_Builder;
	atLiteralHashValue m_NameHash;
	psoSchemaStructureData* m_Structure;	// Under construction - points to just the data, after construction points to data + all member data
	atArray<psoSchemaMemberData> m_Members;	// Only valid when under construction
	psoFileLocation m_Location;
	u16 m_Alignment;						
	bool m_UnderConstruction;
	bool m_AutoPack;
};




class psoBuilderEnumSchema
{
public:
	void AddValue(atLiteralHashValue namehash, int value);

	void FinishBuilding();

	~psoBuilderEnumSchema();

protected:
	friend psoBuilder;
	friend psoBuilderSchemaCatalog;

	psoBuilderEnumSchema();

	u32 ComputeSize();

	u32 ComputeFileLocation(u32 prevOffset);
	void Serialize(char* dest);

	atLiteralHashValue m_NameHash;
	psoSchemaEnumData*	m_Enum;
	atArray<psoEnumTableEntry> m_Values;
	psoFileLocation m_Location;
	bool m_UnderConstruction;
};

class psoBuilderInstance
{
public:
	parPtrToArray GetRawData() {return m_Data;}

	psoBuilderStructSchema* GetSchema() { return m_StructSchema; }

	// PURPOSE: 
	//  Declares a (pointer, referent) relationship that gets fixed up when the file is saved out
	// PARAMS:
	//	referenceToThePointer - This is a reference to the pointer that needs to be replaced with a psoStructId in the PSO file
	//  targetOfThePointer - The thing the pointer points to
	// NOTES:
	//	Typically you'll have some sort of pointer type, a Foo*, in your source data. When saving the PSO file this 32bit Foo* gets
	//  replaced with a 32bit psoStructId. referenceToThePointer should be a reference to the actual Foo* cast to a psoStructId. 
	//  For complex types that contain pointers within them, like atArrays, we have the psoFake* types that put a void* pointer
	//	and a psoStructId in a union together to help with this casting. For other types, just reinterpret_cast your pointer to a 
	//  psoStructId.
	template<typename _Type>
	void AddFixup(_Type*& referenceToThePointer, psoBuilderInstance* targetOfThePointer) {
		AddFixupRaw(reinterpret_cast<parPtrToStructure&>(referenceToThePointer), targetOfThePointer);
	}

	// PURPOSE:
	//	Helper function for declaring a (pointer, referent) relationship for a named member variable.
	// PARAMS:
	//	index - The index of the structure in this struct array (remember all psoBuilderInstances can represent arrays of one or more psoStructs)
	//	memberName - The name of the pointer member variable
	//	targetOfThePointer - The thing the member variable should point to
	// NOTES:
	//	If you're creating a psoBuilderInstance from scratch this can be easier than 
	void AddFixup(int index, const char* memberName, psoBuilderInstance* targetOfThePointer);

	void AddFixupRaw(parPtrToStructure& referenceToThePointer, psoBuilderInstance* targetOfThePointer);

	~psoBuilderInstance();

protected:
	friend psoBuilder;
	friend psoBuilderInstanceSet;
	friend psoBuilderInstanceDataCatalog;

	psoBuilderInstance();

	u32 ComputeSize();

	u32 ComputeFileLocation(u32 prevOffset);
	void Serialize(char* dest);

	struct IdFixupInfo
	{
		psoFakePtr*			m_AddressOfStructId;	 // Address of a 4-byte location that currently holds a pointer but will be replaced with a structId
		psoBuilderInstance* m_ReferentData;
	};

	parPtrToArray	m_Data; // parPtrToArray because it could be instances, could be POD data, etc.
	psoBuilderStructSchema* m_StructSchema;
	atArray<IdFixupInfo> m_IdFixups;
	psoStructId		m_Id;
	psoFileLocation m_Location;
	u16				m_Count;
	u8				m_PodType;
	bool			m_OwnsData;
	size_t			m_Align;
};

class psoBuilder
{
public:
	psoBuilder();

	// If namehash is 0, an anonymous ID will be assigned
	psoBuilderStructSchema&		CreateStructSchema(atLiteralHashValue namehash, size_t size = (size_t)(-1), size_t align = 1);

	// If namehash is 0, an anonymous ID will be assigned
	psoBuilderEnumSchema&		CreateEnumSchema(atLiteralHashValue namehash);

	psoBuilderStructSchema*		FindStructSchema(atLiteralHashValue namehash);
	psoBuilderEnumSchema*		FindEnumSchema(atLiteralHashValue namehash);

	psoBuilderInstance& AddStructInstances(psoBuilderStructSchema& schema, parConstPtrToStructure data, size_t count = 1, size_t align = 0);
	psoBuilderInstance& CreateStructInstances(psoBuilderStructSchema& schema, size_t count = 1, size_t align = 0);

	psoBuilderInstance& AddPodArray(parMemberType::Enum type, parConstPtrToArray data, size_t sizeInBytes, size_t align = 0);
	psoBuilderInstance& CreatePodArray(parMemberType::Enum type, size_t sizeInBytes, size_t align = 0);

	void AddExtraString(u32 hash, const char* str);

	void AddExtraDebugString(u32 hash, const char* str);

	void IncludeChecksum() { m_IncludeChecksum = true; }

	void SetRootObject(psoBuilderInstance& inst);

	void FinishBuilding();

	u32 GetFileSize();

	//PURPOSE
	//  Returns the total size of the file. If the pso is still under construction
	//   it recalculates the current size.
	u32 ComputeSize();

	bool Save(const char* filename);

	bool SaveToBuffer(char* buffer, size_t sizeInBytes);

	u32 FindSignature(psoStructureSchema& sch);

protected:
	u32 FindMemberSignature(psoStructureSchema& sch, psoMemberSchema& mem);

	psoBuilderSchemaCatalog			m_Schemas;
	psoBuilderInstanceDataCatalog	m_Instances;
	bool							m_UnderConstruction;
	bool							m_IncludeChecksum;
	u16								m_NextAnonId;
	u32                             m_FileSize;
};



} // namespace rage

#endif // PARSER_PSOBUILDER_H

