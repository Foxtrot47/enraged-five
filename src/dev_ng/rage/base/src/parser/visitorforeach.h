// 
// parser/visitorforeach.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_VISITORFOREACH_H 
#define PARSER_VISITORFOREACH_H 

#include "visitor.h"

#include "atl/delegate.h"

namespace rage {

template<typename _TypeToFind>
class parForEachVisitor : public parInstanceVisitor
{
public:
	virtual bool BeginStructMember(void* ptrToStruct, parMemberStruct& metadata)
	{
		if (metadata.GetConcreteStructure(ptrToStruct)->IsSubclassOf(_TypeToFind::parser_GetStaticStructure()))
		{
			_TypeToFind* found = reinterpret_cast<_TypeToFind*>(ptrToStruct);
			FoundStruct(*found);
		}
		return true;
	}
	virtual void FoundStruct(_TypeToFind&) = 0;
};

} // namespace rage

#endif // PARSER_VISITORFOREACH_H 
