// 
// parser/visitorutils.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "visitorutils.h"

#include "manager.h"
#include "optimisations.h"

#include "math/simplemath.h"

PARSER_OPTIMISATIONS();

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
EXTERN_PARSER_ENUM(rage__parMember__Type);
EXTERN_PARSER_ENUM(rage__parMemberArray__Subtype);
EXTERN_PARSER_ENUM(rage__parMemberEnum__Subtype);
EXTERN_PARSER_ENUM(rage__parMemberMap__Subtype);
EXTERN_PARSER_ENUM(rage__parMemberString__Subtype);
EXTERN_PARSER_ENUM(rage__parMemberStruct__Subtype);
#endif

namespace rage {

	parCountInstancesVisitor::parCountInstancesVisitor(parStructure& queryStruct)
		: m_QueryStruct(queryStruct)
		, m_Count(0)
	{
		parInstanceVisitor::TypeMask typeMask(false);
		typeMask.Set(parMemberType::TYPE_STRUCT);
		SetMemberTypeMask(typeMask);
	}

	void parCountInstancesVisitor::VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
	{
		if (metadata.GetConcreteStructure(dataPtr)->IsSubclassOf(&m_QueryStruct))
		{
			m_Count++;
		}

		parInstanceVisitor::VisitStructure(dataPtr, metadata);
	}


/////////////////////////////////////////////////////////////////////////////////////////////

	parFindTotalSizeVisitor::parFindTotalSizeVisitor()
		: m_Size(0)
		, m_NumAllocs(0)
	{
		parInstanceVisitor::TypeMask typeMask(false);
		typeMask.Set(parMemberType::TYPE_STRUCT);
		typeMask.Set(parMemberType::TYPE_ARRAY);
		typeMask.Set(parMemberType::TYPE_STRING);
		typeMask.Set(parMemberType::TYPE_BITSET);
		SetMemberTypeMask(typeMask);
	}

	bool parFindTotalSizeVisitor::BeginToplevelStruct(parPtrToStructure ptrToStruct, parStructure& metadata)
	{
		m_Size += metadata.GetSize();
		m_NumAllocs++;
		return parInstanceVisitor::BeginToplevelStruct(ptrToStruct, metadata);
	}

	bool parFindTotalSizeVisitor::BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata)
	{
		if (ptrRef)
		{
			m_Size += metadata.GetConcreteStructure(ptrRef)->GetSize();
			m_NumAllocs++;
		}
		return parInstanceVisitor::BeginPointerMember(ptrRef, metadata);
	}

	bool parFindTotalSizeVisitor::BeginArrayMember(parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata)
	{
		if (metadata.HasExternalStorage())
		{
			m_Size += numElements * metadata.GetPrototypeMember()->GetSize();
			m_NumAllocs++;
		}
		return parInstanceVisitor::BeginArrayMember(ptrToMember, arrayContents, numElements, metadata);
	}

	void parFindTotalSizeVisitor::StringMember(parPtrToMember /*ptrToMember*/, const char* stringData, parMemberString& metadata)
	{
		if (metadata.HasExternalStorage() && stringData)
		{
			m_Size += strlen(stringData) + 1;
			m_NumAllocs++;
		}
	}

	void parFindTotalSizeVisitor::WideStringMember(parPtrToMember /*ptrToMember*/, const char16* stringData, parMemberString& metadata)
	{
		if (metadata.HasExternalStorage() && stringData)
		{
			m_Size += sizeof(char16) * (wcslen(stringData) + 1);
			m_NumAllocs++;
		}
	}

	void parFindTotalSizeVisitor::BitsetMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*ptrToBits*/, size_t /*numBits*/, parMemberBitset& metadata)
	{
		if (!metadata.IsFixedSize())
		{
			m_Size += sizeof(u32) * metadata.GetNumBlocks(m_ContainingStructureAddress);
			m_NumAllocs++;
		}
	}

    bool parFindTotalSizeVisitor::BeginMapMember (parPtrToStructure structAddr,	parMemberMap& metadata)
    {
        if (metadata.GetSubtype() == parMemberMapSubType::SUBTYPE_ATMAP)
        {
            //Expected data layout for DataPair
            //  NO VIRTUAL TABLE
            //  _Key key;
            //  _Data data;
            //  atMapEntry *next;
            size_t keySize = metadata.GetKeyMember()->GetSize();
            size_t keyAlign = metadata.GetKeyMember()->FindAlign();

            size_t dataSize = metadata.GetDataMember()->GetSize();
            size_t dataAlign = metadata.GetDataMember()->FindAlign();
            keySize = AlignPow2(keySize, dataAlign);

            size_t nextSize = sizeof(char*); //it is a pointer so data type does not matter
            size_t nextAlign = __alignof(char*); //it is a pointer so data type does not matter
            dataSize = AlignPow2(dataSize, nextAlign);
            
            size_t maxAlign = Max(keyAlign, dataAlign);
            size_t entrySize = AlignPow2(keySize + dataSize + nextSize, Max(maxAlign, nextAlign));

            //This should be legal because we do not need to walk the data.
            parPtrToMember atMapAddr = metadata.GetPointerToMember(structAddr);
            atMap<char, char>& map = *reinterpret_cast<atMap<char, char>*>(atMapAddr);

            //Table head allocate
            m_Size += map.GetNumSlots() * sizeof(char*);//pointer to pointers so data type matters not
            m_NumAllocs++;

            //Map Entries
            m_Size += map.GetNumUsed() * entrySize;
            m_NumAllocs += map.GetNumUsed();
        }
        else if (metadata.GetSubtype() == parMemberMapSubType::SUBTYPE_ATBINARYMAP)
        {
            //Expected data layout for DataPair
            //  NO VIRTUAL TABLE
            //  _KeyType key;
            //  _T data;
            size_t keySize = metadata.GetKeyMember()->GetSize();
            size_t keyAlign = metadata.GetKeyMember()->FindAlign();

            size_t dataSize = metadata.GetDataMember()->GetSize();
            size_t dataAlign = metadata.GetDataMember()->FindAlign();
            keySize = AlignPow2(keySize, dataAlign);
            
            size_t dataPairSize = AlignPow2(keySize + dataSize, Max(keyAlign, dataAlign));

            //This should be legal because we do not need to walk the data.
            parPtrToMember atBinaryMapAddr = metadata.GetPointerToMember(structAddr);
            atBinaryMap<char, char>& map = *reinterpret_cast<atBinaryMap<char, char>*>(atBinaryMapAddr);

            //Array Allocate
            m_Size += map.GetRawDataArray().GetCapacity() * dataPairSize;
            m_NumAllocs++;
        }

        return parInstanceVisitor::BeginMapMember(structAddr, metadata);
    }

/////////////////////////////////////////////////////////////////////////////////////////////
	parDeepCopyVisitor::parDeepCopyVisitor() 
		: m_ExistingObjectPtr(NULL)
	{
		parInstanceVisitor::TypeMask typeMask(false);
		typeMask.Set(parMemberType::TYPE_STRUCT);
		typeMask.Set(parMemberType::TYPE_ARRAY);
		typeMask.Set(parMemberType::TYPE_STRING);
		typeMask.Set(parMemberType::TYPE_BITSET);
		typeMask.Set(parMemberType::TYPE_MAP);
		SetMemberTypeMask(typeMask);
	}

	bool parDeepCopyVisitor::BeginStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata)
	{
		// Even though we're not doing any allocations here we need to push and pop the m_BuildingStack, so that
		// any members' relative offsets are correct
		parPtrToStructure nestedStruct = metadata.GetObjAddr(m_BuildingStack.Top());
		m_BuildingStack.Push(nestedStruct);
		return parInstanceVisitor::BeginStructMember(ptrToStruct, metadata);
	}

	void parDeepCopyVisitor::EndStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata)
	{
		m_BuildingStack.Pop();
		parInstanceVisitor::EndStructMember(ptrToStruct, metadata);
	}

	bool parDeepCopyVisitor::BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata)
	{
		if (ptrRef)
		{
			parStructure* concreteStruct = metadata.GetConcreteStructure(ptrRef);
			if (concreteStruct)
			{
				parPtrToStructure newData = concreteStruct->Create();
				sysMemCpy(newData, ptrRef, concreteStruct->GetSize());
				metadata.SetObjAddr(m_BuildingStack.Top(), newData);
				m_BuildingStack.Push(newData);
			}
		}
		return true;
	}

	void parDeepCopyVisitor::EndPointerMember(parPtrToStructure& ptrRef, parMemberStruct& /*metadata*/)
	{
		if (ptrRef)
		{
			m_BuildingStack.Pop();
		}
	}

	bool parDeepCopyVisitor::BeginToplevelStruct(parPtrToStructure ptrToStruct, parStructure& metadata)
	{
		parPtrToStructure newData = m_ExistingObjectPtr ? m_ExistingObjectPtr : metadata.Create();
		m_BuildingStack.Push(newData);
		sysMemCpy(m_BuildingStack.Top(), ptrToStruct, metadata.GetSize());
		return parInstanceVisitor::BeginToplevelStruct(ptrToStruct, metadata);
	}

	void parDeepCopyVisitor::StringMember(parPtrToMember /*ptrToMember*/, const char* ptrToString, parMemberString& metadata)
	{
		switch(metadata.GetSubtype())
		{
		case parMemberStringSubType::SUBTYPE_ATSTRING:
			{
				atString& newString = metadata.GetMemberFromStruct<atString>(m_BuildingStack.Top());
				rage_placement_new(&newString) atString;	// placement new a string, to overwrite anything that was already at the newString location
				newString = ptrToString;
			}
			break;
		case parMemberStringSubType::SUBTYPE_CONST_STRING:
			{
				ConstString& newString = metadata.GetMemberFromStruct<ConstString>(m_BuildingStack.Top());
				rage_placement_new(&newString) ConstString;	// placement new a string, to overwrite anything that was already at the newString location
				newString = ptrToString;
			}
			break;
		case parMemberStringSubType::SUBTYPE_POINTER:
			{
				char*& newString = metadata.GetMemberFromStruct<char*>(m_BuildingStack.Top());
				newString = StringDuplicate(ptrToString);
			}
			break;
		case parMemberStringSubType::SUBTYPE_MEMBER:
		case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
		case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
		case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
		case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
		case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
		case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
			// don't need to allocate, data has already been copied
			break;
		default:
			parErrorf("Invalid subtype (%d) for string member", metadata.GetSubtype());
		}
	}

	void parDeepCopyVisitor::WideStringMember(parPtrToMember /*ptrToMember*/, const char16* ptrToString, parMemberString& metadata)
	{
		using namespace parMemberStringSubType;
		switch(metadata.GetSubtype())
		{
		case SUBTYPE_ATWIDESTRING:
			{
				atWideString& newString = metadata.GetMemberFromStruct<atWideString>(m_BuildingStack.Top());
				rage_placement_new(&newString) atWideString;	// placement new a string, to overwrite anything that was already at the newString location
				newString = ptrToString;
			}
			break;
		case SUBTYPE_WIDE_MEMBER:
			// don't need to allocate
			break;
		case SUBTYPE_WIDE_POINTER:
			{
				char16*& newString = metadata.GetMemberFromStruct<char16*>(m_BuildingStack.Top());
				newString = WideStringDuplicate(ptrToString);
			}
			break;
		default:
			parErrorf("Invalid subtype (%d) for wide string member", metadata.GetSubtype());
		}
	}

	void parDeepCopyVisitor::BitsetMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*ptrToBits*/, size_t /*numBits*/, parMemberBitset& metadata)
	{
		using namespace parMemberBitsetSubType;
		if (metadata.GetSubtype() == SUBTYPE_ATBITSET)
		{
			atBitSet& srcBitset = metadata.GetMemberFromStruct<atBitSet>(m_ContainingStructureAddress);
			atBitSet& destBitset = metadata.GetMemberFromStruct<atBitSet>(m_BuildingStack.Top());
			rage_placement_new(&destBitset) atBitSet();		// placement new the atBitSet, to overwrite any pointer values that were already at the destBitset location
			destBitset = srcBitset;
		}
	}

    bool parDeepCopyVisitor::BeginMapMember	(parPtrToStructure structAddr,	parMemberMap& metadata)
    {
		using namespace parMemberMapSubType;

		switch(metadata.GetSubtype())
		{
		case SUBTYPE_ATBINARYMAP:
			{
				parFake::AtBinMap& srcMap = metadata.GetMemberFromStruct<parFake::AtBinMap>(structAddr);
				parFake::AtBinMap& destMap = metadata.GetMemberFromStruct<parFake::AtBinMap>(m_BuildingStack.Top());

				// We memcpyed data into the dest map which isn't relevant, so fix it.
				destMap.m_Data.m_Capacity = destMap.m_Data.m_Count = 0;
				destMap.m_Data.m_Elements.m_Pointer = NULL;
				
				parMemberAtBinaryMapGenericInterface srcInterface(metadata, &srcMap); 
				parMemberAtBinaryMapGenericInterface destInterface(metadata, &destMap);

				if (srcInterface.GetCount() > 0)
				{
					destInterface.Reserve((int)srcInterface.GetCount());
					destMap.m_Data.m_Count = (u16)srcInterface.GetCount(); // we don't have an interface method to set the count directly
					sysMemCpy(destInterface.GetArrayElement(0), srcInterface.GetArrayElement(0), srcInterface.GetArrayElementSize() * srcInterface.GetCount());

				}
			}
			break;
		case SUBTYPE_ATMAP:
			{
				parErrorf("Can't deep copy atMaps yet");
				atMap<char,char>& destMember = metadata.GetMemberFromStruct<atMap<char,char> >(m_BuildingStack.Top());
				rage_placement_new(&destMember) atMap<char,char>;
				return false;
			}
			break;
		}

		parMemberMapIterator* srcIter = NULL;
		parMemberMapIterator* destIter = NULL;
		{
			sysMemAutoUseTempMemory temp;
			srcIter = metadata.CreateIterator(structAddr);
			destIter = metadata.CreateIterator(m_BuildingStack.Top());
		}

		while(!srcIter->AtEnd())
		{
			if (GetMemberTypeMask().IsSet(metadata.GetKeyMember()->GetType()))
			{
				m_BuildingStack.Push(destIter->GetKeyPtr());
				VisitMember(srcIter->GetKeyPtr(), *metadata.GetKeyMember());
				m_BuildingStack.Pop();
			}

			if (GetMemberTypeMask().IsSet(metadata.GetDataMember()->GetType()))
			{
				m_BuildingStack.Push(destIter->GetDataPtr());
				VisitMember(srcIter->GetDataPtr(), *metadata.GetDataMember());
				m_BuildingStack.Pop();
			}

			srcIter->Next();
			destIter->Next();
		}

		{
			sysMemAutoUseTempMemory temp;
			delete srcIter;
			delete destIter;
		}

        return false; // Don't need any recursion, we just did it ourselves.
    }

	bool parDeepCopyVisitor::BeginArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata)
	{
		using namespace parMemberArraySubType;

		if (metadata.HasExternalStorage() && numElements > 0)
		{
			void*& newPtrToMember = metadata.GetMemberFromStruct<void*>(m_BuildingStack.Top());

			switch(metadata.GetSubtype())
			{
			case SUBTYPE_ATARRAY:
				rage_placement_new(&newPtrToMember) atArray<char>;
				break;
			case SUBTYPE_POINTER:
				newPtrToMember = NULL;
				break;
			case SUBTYPE_ATARRAY_32BIT_IDX:
				rage_placement_new(&newPtrToMember) atArray<char, 0, u32>;
				break;
			case SUBTYPE_POINTER_WITH_COUNT:
			case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
			case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
				newPtrToMember = NULL;
				metadata.SetCountForPointerWithCount(m_BuildingStack.Top(), 0);
				break;
			default:
				parErrorf("Invalid type %d", metadata.GetSubtype());
			}

			// When setting the array size - pass 0 to not init the new allocations. We don't need to because
			// we'll be overwriting all the data anyway
			metadata.SetArraySize(m_BuildingStack.Top(), numElements, 0); 

			parPtrToArray newArrayContents = NULL;
			size_t newNumElements = 0;
			metadata.GetArrayContentsAndCountFromStruct(m_BuildingStack.Top(), newArrayContents, newNumElements);

			parAssertf(newNumElements == numElements, "Mismatch in the number of elements in a copied array. Old array %" SIZETFMT "d, new array %" SIZETFMT "d", numElements, newNumElements);

			sysMemCpy(newArrayContents, arrayContents, metadata.GetPrototypeMember()->GetSize() * numElements);
		}
		else
		{
			// The data was already memcpyed, so don't do that again
		}


		// BeginArrayMember may have modified the array - so get the arrayItemsAddress and numItems again, to make sure we're up to date
		if (GetMemberTypeMask().IsSet(metadata.GetPrototypeMember()->GetType()))
		{
			size_t numItems;
			parPtrToArray srcArrayItemsAddress = NULL;
			parPtrToArray destArrayItemsAddress = NULL;
			metadata.GetArrayContentsAndCountFromStruct(m_ContainingStructureAddress, srcArrayItemsAddress, numItems);
			metadata.GetArrayContentsAndCountFromStruct(m_BuildingStack.Top(), destArrayItemsAddress, numItems);

			for(int i = 0; i < (int)numItems; i++)
			{
				parPtrToStructure srcElementAddr = metadata.GetAddressOfNthElement(srcArrayItemsAddress, i);
				parPtrToStructure destElementAddr = metadata.GetAddressOfNthElement(destArrayItemsAddress, i);
				m_BuildingStack.Push(destElementAddr);

				VisitMember(srcElementAddr, *(metadata.GetPrototypeMember()));

				m_BuildingStack.Pop();
			}
		}

		return false; // Don't need any recursion, we just did it ourselves
	}

////////////////////////////////////////////////////////////////////////////////////////////

	void parDeleteMember(parPtrToStructure object, parMember& metadata)
	{
		parDeleteAllVisitor deleter(true);
		deleter.VisitMember(object, metadata);
	}

	parDeleteAllVisitor::parDeleteAllVisitor(bool runDestructors)
	: m_RunDestructors(runDestructors)
	{
		parInstanceVisitor::TypeMask typeMask(false);
		typeMask.Set(parMemberType::TYPE_STRUCT);
		typeMask.Set(parMemberType::TYPE_ARRAY);
		typeMask.Set(parMemberType::TYPE_STRING);
		typeMask.Set(parMemberType::TYPE_BITSET);
		SetMemberTypeMask(typeMask);
	}

	void parDeleteAllVisitor::DestructArrayElements(parPtrToArray arrayPointer, size_t startIndex, size_t endIndex, const parMemberArray& arrayMetadata)
	{	
		FastAssert(startIndex <= endIndex);
		const parMember* prototypeMember = arrayMetadata.GetPrototypeMember();

		parDeleteAllVisitor deleter(true);

		// This loop is inspired by visitor.cpp and is essentially how we iterate over an array. 
		for(size_t i = startIndex; i < endIndex; i++)
		{
			parPtrToStructure elementAddr = arrayMetadata.GetAddressOfNthElement(arrayPointer, i);
			deleter.VisitMember(elementAddr, const_cast<parMember&>(*prototypeMember));
		}
	}

	void parDeleteAllVisitor::EndArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*arrayContents*/, size_t /*numElements*/, parMemberArray& metadata)
	{
		using namespace parMemberArraySubType;

		switch(metadata.GetSubtype())
		{
		case SUBTYPE_ATARRAY:
		case SUBTYPE_ATARRAY_32BIT_IDX:
			metadata.SetArraySize(m_ContainingStructureAddress, 0);
			break;
		case SUBTYPE_POINTER:
		case SUBTYPE_POINTER_WITH_COUNT:
		case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
			metadata.SetArraySize(m_ContainingStructureAddress, 0);
			metadata.GetMemberFromStruct<void*>(m_ContainingStructureAddress) = NULL;
			break;
		default:
			// Nothing
			break;
		}
	}

    void parDeleteAllVisitor::EndMapMember (parPtrToStructure structAddr, parMemberMap& metadata)
    {
        parMemberMapInterface* mapInterface = metadata.CreateInterface(structAddr);
        mapInterface->Reset();
        delete mapInterface;

    }

	void parDeleteAllVisitor::EndPointerMember(parPtrToStructure& ptrRef, parMemberStruct& /*metadata*/)
	{
		delete (char*) ptrRef;
		ptrRef = NULL;
	}

#if !__FINAL
	void parDeleteAllVisitor::AtNonFinalHashStringMember(atHashString& stringData, parMemberString& /*metadata*/)
	{
		stringData.Clear();
	}
#endif	//	!__FINAL

	void parDeleteAllVisitor::AtFinalHashStringMember(atFinalHashString& stringData, parMemberString& /*metadata*/)
	{
		stringData.Clear();
	}

	void parDeleteAllVisitor::AtStringMember(atString& stringData, parMemberString& /*metadata*/)
	{
		stringData.Clear();
	}

	void parDeleteAllVisitor::AtWideStringMember(atWideString& stringData, parMemberString& /*metadata*/)
	{
		stringData.Clear();
	}

	void parDeleteAllVisitor::BitsetMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*ptrToBits*/, size_t /*numBits*/, parMemberBitset& metadata)
	{
		if (metadata.GetSubtype() == parMemberBitsetSubType::SUBTYPE_ATBITSET)
		{
			metadata.GetMemberFromStruct<atBitSet>(m_ContainingStructureAddress).Kill();
		}
	}

	void parDeleteAllVisitor::ConstStringMember(ConstString& stringData, parMemberString& /*metadata*/)
	{
		stringData = NULL;
	}

	void parDeleteAllVisitor::PointerStringMember(char*& stringData, parMemberString& /*metadata*/)
	{
		delete [] stringData;
		stringData = NULL;
	}

	void parDeleteAllVisitor::WidePointerStringMember(char16*& stringData, parMemberString& /*metadata*/)
	{
		delete [] stringData;
		stringData = NULL;
	}

	bool parDeleteAllVisitor::BeginStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata)
	{
		if (m_RunDestructors)
		{
			parStructure* structure = metadata.GetConcreteStructure(ptrToStruct);
			structure->Destroy(ptrToStruct);
			// No recursion - the destructors have (supposedly) done this already
			return false;
		}
		return true;
	}

	void parDeleteAllVisitor::EndStructMember(parPtrToStructure ptrToStruct, parMemberStruct& metadata)
	{
		using namespace parMemberArraySubType;

		if (m_RunDestructors)
		{
			// shouldn't have done anything before - real d'tors did the work - so just return;
			return;
		}

		// Kind of bizarre special case - if you have multiple pointer-with-count arrays using the same count variable
		// we can't clear the count var until all the arrays have been traversed. So look for those arrays and clear them now.

		parStructure* structure = metadata.GetBaseStructure();
		int members = structure->GetNumMembers();
		for(int i = 0; i < members; i++)
		{
			parMember* member = structure->GetMember(i);
			if (member->GetType() == parMemberType::TYPE_ARRAY)
			{
				parMemberArray& memberArr = member->AsArrayRef();
				switch(memberArr.GetSubtype())
				{
				case SUBTYPE_POINTER_WITH_COUNT:
				case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
				case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
					memberArr.SetCountForPointerWithCount(ptrToStruct, 0);
					break;
				default:
					break;
				}
			}
		}
	}

/////////////////////////////////////////////////////////////////////////////////////////////

bool parFindMemberFromUriPath_Helper(parPtrToStructure containingStructAddr, parStructure* currType, atArray<atString>& pathComps, size_t index, parFindMemberData& outData)
{
	// Get the next member name - look up that member in the structure
	u16 sIndex = (u16)index;

	parMember* member = currType->FindMember(pathComps[sIndex].c_str(), true);

	bool needsToLoop = false;

	do // The loop is to loop over each array index, for multidimensional arrays. We handle nested structures with recursion
	{
		needsToLoop = false;

		if (!member)
		{
			parErrorf("Couldn't find member %s in structure %s", pathComps[sIndex].c_str(), currType->GetName());
			return false;
		}

		// If there aren't any more path components we're all done. Woo!
		if ((int)index == pathComps.GetCount()-1)
		{
			outData.m_ContainingStructureAddress = containingStructAddr;
			outData.m_ContainingStructureType = currType;
			outData.m_MemberAddress = member->GetPointerToMember(containingStructAddr);
			outData.m_MemberType = member;
			return true;
		}

		// More path components, gotta deal with them. What kind of member are we?
		switch (member->GetType())
		{
		case parMemberType::TYPE_STRUCT:
			// Next name should name a sub-member. Get the address of the referenced structure and recurse!
			{
				parMemberStruct& memberStruct = member->AsStructOrPointerRef();
				parPtrToStructure childObject = memberStruct.GetObjAddr(containingStructAddr);
				if (!childObject) 
				{
					parErrorf("Trying to dereference NULL pointer. Member %s in structure %s", pathComps[sIndex].c_str(), currType->GetName());
					return false;
				}
				return parFindMemberFromUriPath_Helper(childObject, memberStruct.GetConcreteStructure(childObject), pathComps, index+1, outData);
			}

		case parMemberType::TYPE_ARRAY:
			// Next name should name an array element (i.e. it should be an int). Get the address of a referenced structure and loop for multidim. arrays.
			{
				parMemberArray& memberArray = member->AsArrayRef();
				int nextIndex = parUtils::StringToInt(pathComps[sIndex+1]);

				parPtrToArray arrayItemsAddress;
				size_t numItems;

				memberArray.GetArrayContentsAndCountFromStruct(containingStructAddr, arrayItemsAddress, numItems);

				if (nextIndex < 0 || (size_t)nextIndex >= numItems)
				{
					parErrorf("Invalid index %d for array of size %" SIZETFMT "d in object %s", nextIndex, numItems, currType->GetName());
					return false;
				}

				parPtrToStructure elementAddr = memberArray.GetAddressOfNthElement(arrayItemsAddress, nextIndex);
				containingStructAddr = elementAddr;

				member = memberArray.GetPrototypeMember();
				index++;

				needsToLoop = true;
			}
			break;
        case parMemberType::TYPE_MAP:
            // Next name should name an map key element (i.e. it should be one of the key supported types).
            {
                parMemberMap& memberMap = member->AsMapRef();

                parMember* keyMember = memberMap.GetKeyMember();
                parMember* dataMember = memberMap.GetDataMember();

                parMemberMapInterface* mapInterface = memberMap.CreateInterface(containingStructAddr);

                parPtrToStructure dataValueAddr = NULL;
                switch (keyMember->GetType())
                {
                    //These are the limited supported types for key values
                case parMemberType::TYPE_CHAR:
                case parMemberType::TYPE_UCHAR:
                case parMemberType::TYPE_SHORT:
                case parMemberType::TYPE_USHORT:
                case parMemberType::TYPE_INT:
                case parMemberType::TYPE_UINT:
				case parMemberType::TYPE_PTRDIFFT:
				case parMemberType::TYPE_SIZET:
				case parMemberType::TYPE_INT64:
				case parMemberType::TYPE_UINT64:
                    {
                        s64 key = parUtils::StringToInt64(pathComps[sIndex+1]);
                        dataValueAddr = mapInterface->GetDataPtr(&key);
                    }
                    break;
				case parMemberType::TYPE_ENUM:
					{
						const char* enumStr = pathComps[sIndex+1];
						parMemberEnum& keyEnumMember = keyMember->AsEnumRef();
						parEnumData::ValueFromNameStatus result;
						int key = keyEnumMember.ValueFromName(enumStr, NULL, &result);
						if (result != parEnumData::VFN_EMPTY || result != parEnumData::VFN_ERROR)
						{
							dataValueAddr = mapInterface->GetDataPtr(&key);
						}
					}
                case parMemberType::TYPE_STRING:
                    {
                        parMemberString& strParMember = keyMember->AsStringRef();
                        switch (strParMember.GetSubtype())
                        {
#if !__FINAL
                        case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
                            {
                                atHashString key(pathComps[sIndex+1]);
                                dataValueAddr = mapInterface->GetDataPtr(&key);
                            }
							break;
#endif
						case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
							{
								atFinalHashString key(pathComps[sIndex+1]);
								dataValueAddr = mapInterface->GetDataPtr(&key);
							}
							break;
						case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
							{
								atHashValue key(pathComps[sIndex+1]);
								dataValueAddr = mapInterface->GetDataPtr(&key);
							}
							break;
						case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
							{
								u32 key = atPartialStringHash(pathComps[sIndex+1]);
								dataValueAddr = mapInterface->GetDataPtr(&key);
							}
							break;
						case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
						case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
							{
								u32 key = atHashStringNamespaceSupport::ComputeHash(strParMember.GetData()->GetNamespaceIndex(), pathComps[sIndex+1]);
								dataValueAddr = mapInterface->GetDataPtr(&key);
							}
							break;
                        default:
                            parAssertf(0,"Unsupported key string type %d being used for map %s", strParMember.GetSubtype(), memberMap.GetName());
                            break;
                        };
                    }
                    break;
                default:
                    parAssertf(0,"Unsupported key type %d being used for map %s", keyMember->GetType(), memberMap.GetName());
                    break;
                };
                delete mapInterface;
                
                if (dataValueAddr)
                {
                    member = dataMember;
					containingStructAddr = dataValueAddr;
                    index++;

                    needsToLoop = true;
                }
            }
            break;

		default:
			parErrorf("Found a leaf member variable (%s.%s), but the path specified a child (%s)", currType->GetName(), member->GetName(), pathComps[sIndex+1].c_str());
			return false;
		}
	} while(needsToLoop);

	return false;
}	

bool parFindMemberFromUriPath(parPtrToStructure object, parStructure& rootType, const char* path, parFindMemberData& outData)
{
	if (!path || path[0] == 0)
	{
		outData.m_ContainingStructureAddress = object;
		outData.m_ContainingStructureType = rootType.GetConcreteStructure(object);
		outData.m_MemberAddress = NULL;
		outData.m_MemberType = NULL;
		return true;
	}

	atString origPath(path);
	atArray<atString> pathComponents;

	origPath.Split(pathComponents, '/');

	return parFindMemberFromUriPath_Helper(object, rootType.GetConcreteStructure(object), pathComponents, 0, outData);
}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
const char* g_CompareAttr = "memoryLayoutMatch";

class CompareLayoutVisitor : public parNonGenericVisitor
{
public:
	CompareLayoutVisitor() : parNonGenericVisitor()
	{
		// We only care about struct members, don't bother calling any of the VisitFloatMember, VisitStringMember, etc. functions.
		parInstanceVisitor::TypeMask membersToVisit;
		membersToVisit.Set(parMemberType::TYPE_STRUCT);
		SetMemberTypeMask(membersToVisit);
	}

	const char* GetSubtypeName(int type, int subtype, char* tempBuf, int tempBufLen)
	{
		const char* subtypeName = NULL;

		switch(type)
		{
		case parMemberType::TYPE_ARRAY:		subtypeName = PARSER_ENUM(rage__parMemberArray__Subtype).NameFromValue(subtype);		break;
		case parMemberType::TYPE_ENUM:		subtypeName = PARSER_ENUM(rage__parMemberEnum__Subtype).NameFromValue(subtype);			break;
		case parMemberType::TYPE_BITSET:	subtypeName = PARSER_ENUM(rage__parMemberEnum__Subtype).NameFromValue(subtype);			break;
		case parMemberType::TYPE_MAP:		subtypeName = PARSER_ENUM(rage__parMemberMap__Subtype).NameFromValue(subtype);			break;
		case parMemberType::TYPE_STRING:	subtypeName = PARSER_ENUM(rage__parMemberString__Subtype).NameFromValue(subtype);		break;
		case parMemberType::TYPE_STRUCT:	subtypeName = PARSER_ENUM(rage__parMemberStruct__Subtype).NameFromValue(subtype);		break;
		default:
			break;
		}

		if (!subtypeName)
		{
			formatf(tempBuf, tempBufLen, "%d", subtype);
			subtypeName = &tempBuf[0];
		}

		return subtypeName;
	}

	void PrintMemberDescription(const char* /*descString*/, parMember& /*memData*/)
	{
		//if (!PARAM_verbose.Get())
		//{
		//	return;
		//}
		//int subtype = memData.GetCommonData()->m_Subtype;

		//char subtypeBuf[8];

		//const char* subtypeName = GetSubtypeName(subtype, subtypeBuf, sizeof(subtypeBuf));

		//size_t memSize = memData.GetSize();

		//Displayf("\t%s Name: \"%s\"\t\tType: %s/%s\t\tOffset: %d (0x%x)\t\tSize: %d (0x%x)", descString, memData.GetName(), 
		//	parser_rage__parMember__Type_Data.NameFromValue(memData.GetType()),
		//	subtypeName, 
		//	memData.GetOffset(), memData.GetOffset(),
		//	memSize, memSize);
	}

	static bool SortByOffset(const parMember* const & a, const parMember* const & b)
	{
		return a->GetOffset() < b->GetOffset();
	}

	void PrintMemSpan(size_t start, size_t size)
	{
		Printf("\t\t0x%04" SIZETFMT "x -> 0x%04" SIZETFMT "x (0x%04" SIZETFMT "x)    ", start, start + size - 1, size);
	}

	void PrintStructureLayout(parStructure& metadata)
	{
		int numMembers = metadata.GetNumMembers();

		size_t currOffset = 0;

		parStructure* baseClass = metadata.GetBaseStructure();
		size_t baseOffset = reinterpret_cast<size_t>(metadata.GetBaseAddress((parPtrToStructure)NULL));

		parMember** memberData = NULL;

		if (numMembers)
		{
			memberData = Alloca(parMember*, numMembers);

			for(int i = 0; i < numMembers; i++)
			{
				memberData[i] = metadata.GetMember(i);
			}

			std::sort(&memberData[0], &memberData[numMembers], SortByOffset);
		}

		if (baseClass)
		{
			PrintMemSpan(baseOffset, baseClass->GetSize());
			Displayf("<base class %s>", baseClass->GetName());
			currOffset += baseOffset + baseClass->GetSize();
		}

		for(int i = 0; i < numMembers; i++)
		{
			parMember* currMember = memberData[i];
			if (currOffset < currMember->GetOffset())
			{
				if (currOffset == 0)
				{
					PrintMemSpan(currOffset, currMember->GetOffset() - currOffset);
					Displayf("    <vptr? / padding>");
				}
				else
				{
					PrintMemSpan(currOffset, currMember->GetOffset() - currOffset);
					Displayf("    <padding>");
				}
				currOffset = currMember->GetOffset();
			}

			const char* typeName = parser_rage__parMember__Type_Data.NameFromValue(currMember->GetType());
			char subtypeBuf[8];
			const char* subtypeName = GetSubtypeName((int)currMember->GetType(), currMember->GetCommonData()->m_Subtype, subtypeBuf, sizeof(subtypeBuf));

			PrintMemSpan(currMember->GetOffset(), currMember->GetSize());
			Displayf("%s (%s / %s)", currMember->GetName(), typeName, subtypeName);
			currOffset = currMember->GetOffset() + currMember->GetSize();
		}

		if (currOffset < metadata.GetSize())
		{
			PrintMemSpan(currOffset, metadata.GetSize() - currOffset);
			Displayf("    <padding>");
		}

	}

	bool CompareStructures(parStructure& thisMetadata, parStructure& otherMetadata)
	{
		if (thisMetadata.GetExtraAttributes() && thisMetadata.GetExtraAttributes()->FindAttribute(g_CompareAttr))
		{
			return thisMetadata.GetExtraAttributes()->FindAttributeBoolValue(g_CompareAttr, false); // already did this one
		}

		// The thisMetadata is our reference. The otherMetadata needs to match.

		// Things that have to match:
		// base class layout, offset of base class, offset of all the members, structure sizes.

		bool structuresMatch = true;
		bool basesMatch = true;

		if (otherMetadata.GetBaseStructure() && thisMetadata.GetBaseStructure())
		{
			basesMatch = CompareStructures(*thisMetadata.GetBaseStructure(), *otherMetadata.GetBaseStructure());
		}

		Displayf("-------------------------------------------------------------------------------");
		Displayf("Type %s:", otherMetadata.GetName());

		if (!basesMatch)
		{
			Errorf("Base classes don't match (see above for more info)");
			Displayf("");
			structuresMatch = false;
		}

		bool thisHasVirtuals = thisMetadata.GetFlags().IsSet(parStructure::HAS_VIRTUALS);
		bool otherHasVirtuals = otherMetadata.GetFlags().IsSet(parStructure::HAS_VIRTUALS);
		if (thisHasVirtuals != otherHasVirtuals)
		{
			Errorf("Virtual-ness doesn't match: %s %s virtuals, %s %s virtuals - check PAR_PARSABLE/PAR_SIMPLE_PARSABLE and simple=\"true\" attributes", 
				m_ThisDescription, thisHasVirtuals ? "has" : "doesn't have",
				m_OtherDescription, otherHasVirtuals ? "has" : "doesn't have");
			structuresMatch = false;
		}

		if (thisMetadata.GetBaseStructure())
		{
			size_t baseClassAddrOther = reinterpret_cast<size_t>(otherMetadata.GetBaseAddress((parPtrToStructure)NULL));
			size_t baseClassAddrThis = reinterpret_cast<size_t>(thisMetadata.GetBaseAddress((parPtrToStructure)NULL));

			if (baseClassAddrOther != baseClassAddrThis)
			{
				Errorf("Base class %s was found a different offsets. %s: %" SIZETFMT "d (0x%" SIZETFMT "x), %s: %" SIZETFMT "d (0x%" SIZETFMT "x)", thisMetadata.GetBaseStructure()->GetName(), m_ThisDescription, baseClassAddrThis, baseClassAddrThis, m_OtherDescription, baseClassAddrOther, baseClassAddrOther);
				structuresMatch = false;
			}
		}

		size_t sizeOther = otherMetadata.GetSize();
		size_t sizeThis = thisMetadata.GetSize();

		if (sizeOther != sizeThis)
		{
			Errorf("Structure sizes don't match. %s: %" SIZETFMT "d (0x%" SIZETFMT "x), %s: %" SIZETFMT "d (0x%" SIZETFMT "x)", m_ThisDescription, sizeThis, sizeThis, m_OtherDescription, sizeOther, sizeOther);
			structuresMatch = false;
		}

		const char* structName = otherMetadata.GetName();

		// Check all the member variables
		atBitSet foundOtherMembers;
		foundOtherMembers.Init(otherMetadata.GetNumMembers());

		for(int thisIndex = 0; thisIndex < thisMetadata.GetNumMembers(); thisIndex++)
		{
			parMember* thisMember = thisMetadata.GetMember(thisIndex);

			parMember* otherMember = NULL;
			for(int otherIndex = 0; otherIndex < otherMetadata.GetNumMembers(); otherIndex++)
			{
				parMember* currOtherMember = otherMetadata.GetMember(otherIndex);
				if (currOtherMember->GetNameHash() == thisMember->GetNameHash())
				{
					otherMember = currOtherMember;
					foundOtherMembers.Set(otherIndex);
					break;
				}
			}

			if (!otherMember)
			{
				Errorf("Found a member %s::%s in the %s that wasn't in the %s", structName, thisMember->GetName(), m_ThisDescription, m_OtherDescription);
//				PrintMemberDescription(m_ThisDescription, *thisMember);
				structuresMatch = false;
				continue;
			}

			if ((otherMember->GetType() != thisMember->GetType()) ||
				(otherMember->GetCommonData()->m_Subtype != thisMember->GetCommonData()->m_Subtype))
			{
				Errorf("Types for member %s::%s don't match", structName, otherMember->GetName());
//				PrintMemberDescription(m_ThisDescription, *thisMember);
//				PrintMemberDescription(m_OtherDescription, *otherMember);
				structuresMatch = false;
			}

			if (otherMember->GetOffset() != thisMember->GetOffset())
			{
				Errorf("Offset for %s::%s doesn't match. %s is %" SIZETFMT "d, %s assumes %" SIZETFMT "d", structName, otherMember->GetName(), m_ThisDescription, thisMember->GetOffset(), m_OtherDescription, otherMember->GetOffset());
//				PrintMemberDescription(m_ThisDescription, *thisMember);
//				PrintMemberDescription(m_OtherDescription, *otherMember);
				structuresMatch = false;
			}

			if (otherMember->GetSize() != thisMember->GetSize())
			{
				Errorf("Sizes for %s::%s don't match. %s is %" SIZETFMT "d, %s assumes %" SIZETFMT "d", structName, otherMember->GetName(), m_ThisDescription, thisMember->GetSize(), m_OtherDescription, otherMember->GetSize());
//				PrintMemberDescription(m_ThisDescription, *thisMember);
//				PrintMemberDescription(m_OtherDescription, *otherMember);
				structuresMatch = false;
			}

		}

		for(int otherIndex = 0; otherIndex < otherMetadata.GetNumMembers(); otherIndex++)
		{
			if (foundOtherMembers.IsClear(otherIndex))
			{
				Errorf("Found %s::%s in the structdef that wasn't in the runtime", structName, otherMetadata.GetMember(otherIndex)->GetName());
				PrintMemberDescription("Structdef:", *otherMetadata.GetMember(otherIndex));
				structuresMatch = false;
			}
		}

		if (structuresMatch)
		{
			Displayf("Memory layouts match");
			if (m_Verbose)
			{
				Displayf("\tLayout (size = 0x%04" SIZETFMT "x, align = %" SIZETFMT "d)", thisMetadata.GetSize(), thisMetadata.FindAlign());
				PrintStructureLayout(thisMetadata);
			}
		}
		else
		{
			Errorf("Memory layouts don't match!");

			if (m_Verbose)
			{
				Displayf("\tLayout for %s (size = 0x%04" SIZETFMT "x, align = %" SIZETFMT "d)", m_ThisDescription, thisMetadata.GetSize(), thisMetadata.FindAlign());
				PrintStructureLayout(thisMetadata);
				Displayf("");
				Displayf("\tLayout from %s (size = 0x%04" SIZETFMT "x, align = %" SIZETFMT "d)", m_OtherDescription, otherMetadata.GetSize(), otherMetadata.FindAlign());
				PrintStructureLayout(otherMetadata);
			}
		}

		Displayf("-------------------------------------------------------------------------------");
		Displayf("");


		parAttributeList* attrs = thisMetadata.GetOrCreateExtraAttributes();
		attrs->AddAttribute(g_CompareAttr, structuresMatch, false);

		return structuresMatch;
	}

	parStructure* FindOtherStructure(u32 namehash)
	{
		// Make sure "othermanager" is active, because findstructure could instantiate a structure
		parManager* oldMgr = parManager::sm_Instance;
		parManager::sm_Instance = m_OtherManager;

		parStructure* otherMetadata = PARSER.FindStructure(namehash);

		parManager::sm_Instance = oldMgr;

		return otherMetadata;
	}

	virtual bool BeginMapMember(parPtrToStructure ,	parMemberMap& )
	{
		// TODO: Fill this out!
		return true;
	}

	virtual bool BeginArrayMember(parPtrToMember,	parPtrToArray , size_t, parMemberArray& metadata)
	{
		// Special case - check that array elements match even if there aren't any actual elements in this array.
		// Better to do more checking than less. 
		if (metadata.GetPrototypeMember()->GetType() == parMemberType::TYPE_STRUCT)
		{
			parMemberStruct& structMember = metadata.GetPrototypeMember()->AsStructOrPointerRef();
			if (structMember.GetSubtype() == parMemberStructSubType::SUBTYPE_STRUCTURE)
			{			
				parStructure* thisMetadata = structMember.GetBaseStructure();
				parStructure* otherMetadata = FindOtherStructure(thisMetadata->GetNameHash());
				CompareStructures(*thisMetadata, *otherMetadata);
			}
		}
		return true;
	}

	virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
	{
		if (!metadata.GetExtraAttributes() || !metadata.GetExtraAttributes()->FindAttribute(g_CompareAttr))
		{
			parStructure* thisMetadata = &metadata;

			parStructure* otherMetadata = FindOtherStructure(thisMetadata->GetNameHash());

			if (!otherMetadata)
			{
				Errorf("Found a data type '%s' that was in the object and in the %s, but not in the %s", thisMetadata->GetName(), m_ThisDescription, m_OtherDescription);
			}
			else
			{
				CompareStructures(*thisMetadata, *otherMetadata);
			}
			parNonGenericVisitor::VisitStructure(dataPtr, metadata);
		}

	}

	parManager* m_ThisManager;
	const char* m_ThisDescription;
	parManager* m_OtherManager;
	const char* m_OtherDescription;
	bool m_Verbose;
};


void parCompareMemoryLayouts(parPtrToStructure objectData, parStructure* structure, parManager* thisManager, const char* thisDescription, parManager* otherManager, const char* otherDescription, bool verbose)
{
	parAssertf(structure && otherManager && thisManager, "Something's missing!");
	parAssertf(thisManager->FindStructure(structure->GetNameHash()) == structure, "Structure %s needs to come from 'thisManager's list of structures", structure->GetName());
	CompareLayoutVisitor vis;
	vis.m_ThisDescription = thisDescription;
	vis.m_ThisManager = thisManager;
	vis.m_OtherDescription = otherDescription;
	vis.m_OtherManager = otherManager;
	vis.m_Verbose = verbose;
	if (objectData)
	{
		vis.VisitStructure(objectData, *structure);	
	}
	else
	{
		parStructure* otherMetadata = vis.FindOtherStructure(structure->GetNameHash());

		if (!otherMetadata)
		{
			Errorf("Found a data type '%s' that was in the %s, but not in the %s", structure->GetName(), thisDescription, otherDescription);
		}
		else
		{
			vis.CompareStructures(*structure, *otherMetadata);
		}
	}
}

#endif // __DEV || __TOOL

/////////////////////////////////////////////////////////////////////////////////////////////

// Whenever we allocate an array of external structures, we need to call this function because
// some of the fields in the array need to be default initted. Normally the constructor would do
// this, but these "objects" don't have constructors.
void parMemberPlacementVisitor::ConstructArrayElements(parPtrToArray arrayPointer, size_t startIndex, size_t endIndex, const parMemberArray& arrayMetadata)
{	
	FastAssert(startIndex <= endIndex);
	const parMember* prototypeMember = arrayMetadata.GetPrototypeMember();

	parMemberPlacementVisitor placer;

	// This loop is inspired by visitor.cpp and is essentially how we iterate over an array. The prototypeMember's
	// offset is adjusted such that it appears to the parser as if each array element is actually a member of the structure.
	// In this case we don't have a 'beginning of the structure' address - so we just set the offset and the structure address
	// as if the structure started at 0x0
	for(size_t i = startIndex; i < endIndex; i++)
	{
		parPtrToStructure elementAddr = arrayMetadata.GetAddressOfNthElement(arrayPointer, i);
		placer.VisitMember(elementAddr, const_cast<parMember&>(*prototypeMember));
	}
}

bool parMemberPlacementVisitor::BeginArrayMember( parPtrToMember ptrToMember, parPtrToArray arrayContents, size_t numElements, parMemberArray& metadata )
{
	// Some array elements need default construction even though they don't have a valid 'count' yet
	// For example an atFixedArray gets its count set to 0 here, but still has fields that wouldn't get
	// initted in any other way, so we have to do it here.


	switch(metadata.GetSubtype())
	{
	case parMemberArraySubType::SUBTYPE_ATARRAY:
		rage_placement_new(ptrToMember) atArray<char, 0, u16>();
		return false;
	case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:
		rage_placement_new(ptrToMember) atArray<char, 0, u16>();
		return false;
	case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:
		metadata.SetArraySize(m_ContainingStructureAddress, 0);
		// SetArraySize sets count to 0 but we still need to initialize the members, which is what C++ would do too. The visitor wouldn't normally traverse these items, so we need to do it with ConstructArrayElements
		ConstructArrayElements(arrayContents, 0, numElements, metadata);
		return false;
	case parMemberArraySubType::SUBTYPE_POINTER:
		*reinterpret_cast<void**>(ptrToMember) = NULL;
		return false;
	case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:
	case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
	case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
		*reinterpret_cast<void**>(ptrToMember) = NULL;
		metadata.SetCountForPointerWithCount(m_ContainingStructureAddress, 0);
		return false;
	default:
		break;
	}
	return true; // for other arrays, recurse into any members that might be present
}

bool parMemberPlacementVisitor::BeginPointerMember( parPtrToStructure& /*ptrRef*/, parMemberStruct& /*metadata*/ )
{
	return false; // don't recurse! pointer data is bogus.
}

bool parMemberPlacementVisitor::BeginStructMember( parPtrToStructure ptrToStruct, parMemberStruct& metadata )
{
	// Normally we'd want to call "GetConcreteStructure()" here to call the placement function for the 
	// most derived type. But in this case since we're just initting objects and not following pointers,
	// so we don't have the possibility of pointing to a derived type through a base class pointer. Instead
	// we have structure members, or arrays that contain structure members, and in therefore the base structure
	// should be the same as the concrete structure
	metadata.GetBaseStructure()->Place(ptrToStruct);
	return false;
}

void parMemberPlacementVisitor::BitsetMember( parPtrToMember ptrToMember, parPtrToArray /*ptrToBits*/, size_t /*numBits*/, parMemberBitset& metadata )
{
	if (metadata.GetSubtype() == parMemberBitsetSubType::SUBTYPE_ATBITSET)
	{
		rage_placement_new(ptrToMember) atBitSet;
	}
	else
	{
		// 0 out all the bits - thats what the bitset ctors do
		metadata.ClearBits(m_ContainingStructureAddress);
	}
}

void parMemberPlacementVisitor::ConstStringMember( ConstString& stringData, parMemberString& /*metadata*/ )
{
	rage_placement_new(&stringData) ConstString;
}

void parMemberPlacementVisitor::AtStringMember( atString& stringData, parMemberString& /*metadata*/ )
{
	rage_placement_new(&stringData) atString;
}

#if !__FINAL
void parMemberPlacementVisitor::AtNonFinalHashStringMember( atHashString& stringData, parMemberString& /*metadata*/ )
{
	rage_placement_new(&stringData) atHashString;
}
#endif	//	!__FINAL

void parMemberPlacementVisitor::AtFinalHashStringMember( atFinalHashString& stringData, parMemberString& /*metadata*/ )
{
	rage_placement_new(&stringData) atFinalHashString;
}

void parMemberPlacementVisitor::AtHashValueMember( atHashValue& stringData, parMemberString& /*metadata*/ )
{
	rage_placement_new(&stringData) atHashValue;
}

void parMemberPlacementVisitor::AtWideStringMember( atWideString& stringData, parMemberString& /*metadata*/ )
{
	rage_placement_new(&stringData) atWideString;
}

bool parMemberPlacementVisitor::BeginMapMember( parPtrToStructure structAddr, parMemberMap& metadata )
{
	void* ptrToMember = (char*)structAddr + metadata.GetOffset();

	switch(metadata.GetSubtype())
	{
	case parMemberMapSubType::SUBTYPE_ATBINARYMAP:
		rage_placement_new(ptrToMember) atBinaryMap<char, char>;
		break;
	case parMemberMapSubType::SUBTYPE_ATMAP:
		parAssertf(0, "Can't use atMaps in external structures"); // because each atMap member has a bunch of callbacks in it and we don't know what to set those to
		break;
	}
	return true;
}

// RS: I am not thrilled with these. All the other code up above limits itself to only running placement-new. Even the 
// pointer code leaves garbage pointers alone instead of NULLing them. We're supposed to be simulating an empty constructor.
// But that can leave us with garbage data in member strings, since nothing sets the string data during member placement
// (i.e. during 'fake construction'), and the init visitor will only change the string if there's an init="" attribute present
// and I don't want to clobber any strings that have been set in real constructors by setting it in the init visitor.
void parMemberPlacementVisitor::MemberStringMember( char* stringData, parMemberString& metadata )
{
	if (metadata.GetSize() > 0)
	{
		stringData[0] = '\0';
	}
}

void parMemberPlacementVisitor::WideMemberStringMember( char16* stringData, parMemberString& metadata )
{
	if (metadata.GetSize() > 0)
	{
		stringData[0] = '\0';
	}
}

} // namespace rage
