// 
// parser/psotypes.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_PSOTYPES_H
#define PARSER_PSOTYPES_H

#include "memberdata.h"

namespace rage
{
	class psoType // I wish I was an enum class
	{
	public:
		enum Category
		{
			CATEGORY_CORE,
			CATEGORY_INTEGER,
			CATEGORY_FLOAT,
			CATEGORY_VECTOR,
			CATEGORY_STRING,
			CATEGORY_ARRAY,
			CATEGORY_ENUM,
			CATEGORY_COLLECTIONS,

		};

		static const int MAX_CATEGORY = 16;
		static const int CATEGORY_SHIFT = 4;
		static const int TYPES_PER_CATEGORY = (1 << CATEGORY_SHIFT);
		static const int TYPE_IN_CATEGORY_MASK = (1 << CATEGORY_SHIFT) - 1;
		static const int MAX_TYPES = 255;

		enum Type
		{
			//////////////////////////////////////////////////////////////////////////
			// CORE (aka "misc") types
			TYPE_INVALID				= CATEGORY_CORE << CATEGORY_SHIFT,
			TYPE_BOOL,
			TYPE_BOOLV,
			TYPE_VECBOOLV,
			TYPE_STRUCTID,
			TYPE_STRUCT,
			TYPE_POINTER32,
			TYPE_POINTER64,

			//////////////////////////////////////////////////////////////////////////
			// INTEGER types
			TYPE_S8						= CATEGORY_INTEGER << CATEGORY_SHIFT,
			TYPE_U8,
			TYPE_S16,
			TYPE_U16,
			TYPE_S32,
			TYPE_U32,
			TYPE_S64,
			TYPE_U64,
//			TYPE_S128,		// Not implemented yet
//			TYPE_U128,		// Not implemented yet

			//////////////////////////////////////////////////////////////////////////
			// scalar FLOATing point types
			TYPE_FLOAT16				= CATEGORY_FLOAT << CATEGORY_SHIFT,
			TYPE_FLOAT,
			TYPE_SCALARV,
			TYPE_DOUBLE,

			//////////////////////////////////////////////////////////////////////////
			// VECTOR and MATRIX types
			TYPE_VEC2					= CATEGORY_VECTOR << CATEGORY_SHIFT,
			TYPE_VEC2V,
			TYPE_VEC3,
			TYPE_VEC3V,
			TYPE_VEC4V,
			TYPE_MAT33V,
			TYPE_MAT34V,
			TYPE_MAT43,
			TYPE_MAT44V,

			//////////////////////////////////////////////////////////////////////////
			// STRING types
			TYPE_STRING_MEMBER			= CATEGORY_STRING << CATEGORY_SHIFT,
			TYPE_STRING_POINTER32,
			TYPE_STRING_POINTER64,
			TYPE_ATSTRING32,
			TYPE_ATSTRING64,
			TYPE_WIDE_STRING_MEMBER,
			TYPE_WIDE_STRING_POINTER32,
			TYPE_WIDE_STRING_POINTER64,
			TYPE_ATWIDESTRING32,
			TYPE_ATWIDESTRING64,
			TYPE_STRINGHASH,
			TYPE_PARTIALSTRINGHASH,
			TYPE_LITERALSTRINGHASH,
			TYPE_NSSTRINGHASH,

			//////////////////////////////////////////////////////////////////////////
			// ARRAY types 
			TYPE_ARRAY_MEMBER			= CATEGORY_ARRAY << CATEGORY_SHIFT,
			TYPE_ATARRAY32,
			TYPE_ATARRAY64,
			TYPE_ATFIXEDARRAY,
			TYPE_ATARRAY32_32BITIDX,
			TYPE_ATARRAY64_32BITIDX,
			TYPE_ARRAY_POINTER32,
			TYPE_ARRAY_POINTER64,
			TYPE_ARRAY_POINTER32_WITH_COUNT,
			TYPE_ARRAY_POINTER64_WITH_COUNT,

			//////////////////////////////////////////////////////////////////////////
			// ENUM and BITSET types
			TYPE_ENUM8					= CATEGORY_ENUM << CATEGORY_SHIFT,
			TYPE_ENUM16,
			TYPE_ENUM32,
			TYPE_BITSET8,
			TYPE_BITSET16,
			TYPE_BITSET32,
			TYPE_ATBITSET32,
			TYPE_ATBITSET64,

			//////////////////////////////////////////////////////////////////////////
			// non-array COLLECTION types. E.g. Maps.
			TYPE_ATBINARYMAP32			= CATEGORY_COLLECTIONS << CATEGORY_SHIFT,
			TYPE_ATBINARYMAP64,
//			TYPE_ATMAP32, // Not implemented yet
//			TYPE_ATMAP64, // Not implemented yet
		};

		// Traits for the various types:

		size_t GetSize() const;	// Returns 0 if the type isn't a fixed size
		size_t GetAlign() const; // Returns 0 if the type isn't a fixed alignment

		bool IsValid() const; // Is this a valid type (from the list above) that is also currently implemented

		bool IsPod() const;	// Plain-old-data. No fixups necessary
		bool IsBitset() const;
		bool IsMap() const;
		bool IsString() const;
		bool IsArray() const;
		bool IsEnum() const;

		bool HasSubStructure() const; // a structure or a pointer to a structure
		bool IsMapKey() const; // Is this type usable as a map key
		bool IsNative() const; // True if this uses only natively sized data. I.e. no pointers, or the pointers are the right size for this build.
		bool IsMemberType() const; // Can this type be used as a member of a structure in a PSO file
		bool IsDirectContainer() const; // Does this type DIRECTLY contain other types (not via pointer) whose info is defined elsewhere. Current examples are STRUCT or MEMBER_ARRAY
		bool IsStructArrayType() const; // Can this type be the top-level type in a StructArray (vs. needing to be contained in a structure)
		bool HasPointers() const;

		bool IsNarrowString() const; // Is this a string of chars
		bool IsWideString() const; // Is this a string of char16s
		
		bool IsCArray() const; // Is a 'C'-style array (just a pointer, could need an array cookie)
		bool IsArrayWithExternalCount() const; 
		bool IsArrayCounterType() const;
		bool IsMaxSizeArray() const; // The array has a maximum number of elements (or a fixed number)

#if !__NO_OUTPUT
		const char* GetName() const; 
#endif

		static psoType ConvertParserTypeToPsoType(parMemberType::Enum type, u8 subtype, bool is64Bit);
		static psoType ConvertOldStructArrayTypeToPsoType(u32 saType, bool is64Bit);

		psoType(Type t) : m_Type((u8)t) {} // intentionally implicit
		explicit psoType(u8 t) : m_Type(t) {} 

		Type GetEnum() const { return (Type)m_Type; }


		bool operator==(const psoType& other) const { return m_Type == other.m_Type; }
		bool operator!=(const psoType& other) const { return m_Type != other.m_Type; }

		u8 GetRaw() const { return m_Type; }

#if !__FINAL
		void PrintUnexpectedTypeError(const char* context, const char* expected) const;
#else
		void PrintUnexpectedTypeError(const char*, const char*) const {}
#endif

	private:
		friend struct psoRscMemberSchemaData;
		Category GetCategory() const { return (Category)(m_Type >> CATEGORY_SHIFT); }

		psoType(int) { }; // NOT TO BE CALLED

		u8	m_Type;
	};


	inline bool psoType::IsBitset() const { return m_Type >= TYPE_BITSET8 && m_Type <= TYPE_ATBITSET64; }
	inline bool psoType::IsMap() const {	return m_Type >= TYPE_ATBINARYMAP32 && m_Type <= TYPE_ATBINARYMAP64; }
	inline bool psoType::IsArray() const { return GetCategory() == CATEGORY_ARRAY; }
	inline bool psoType::IsString() const { return GetCategory() == CATEGORY_STRING; }
	inline bool psoType::IsWideString() const { return m_Type >= TYPE_WIDE_STRING_MEMBER && m_Type <= TYPE_ATWIDESTRING64; }
	inline bool psoType::IsNarrowString() const { return IsString() && !IsWideString(); }
	inline bool psoType::HasSubStructure() const { return m_Type >= TYPE_STRUCT && m_Type <= TYPE_POINTER64; }
	inline bool psoType::IsEnum() const { return m_Type >= TYPE_ENUM8 && m_Type <= TYPE_ENUM32; }
	inline bool psoType::IsCArray() const { return m_Type >= TYPE_ARRAY_POINTER32 && m_Type <= TYPE_ARRAY_POINTER64_WITH_COUNT; }
	inline bool psoType::IsDirectContainer() const { return m_Type == TYPE_STRUCT || m_Type == TYPE_ARRAY_MEMBER || m_Type == TYPE_ATFIXEDARRAY; }
	inline bool psoType::IsArrayWithExternalCount() const { return m_Type == TYPE_ARRAY_POINTER32_WITH_COUNT || m_Type == TYPE_ARRAY_POINTER64_WITH_COUNT; }
	inline bool psoType::IsArrayCounterType() const { return m_Type == TYPE_U8 || m_Type == TYPE_U16 || m_Type == TYPE_U32; }
	inline bool psoType::IsMaxSizeArray() const { return m_Type == TYPE_ARRAY_MEMBER || m_Type == TYPE_ATFIXEDARRAY || m_Type == TYPE_ARRAY_POINTER32 || m_Type == TYPE_ARRAY_POINTER64; }
} // namespace rage

#endif // PARSER_PSOTYPES_H