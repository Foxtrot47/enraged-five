// 
// parser/psorscbuilder.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSORSCBUILDER_H
#define PARSER_PSORSCBUILDER_H

#include "psobuilderstructs.h"
#include "psofaketypes.h"
#include "psoschema.h"
#include "psodata.h"

#include "memberarraydata.h"
#include "memberenumdata.h"
#include "membermapdata.h"
#include "memberstringdata.h"
#include "memberstructdata.h"

namespace rage {

class psoRscBuilder;
class psoRscBuilderInstance;

class psoRscBuilderStructSchema
{
public:
	enum ArrayLocation
	{
		ARRAY_IN_VIRTUAL_MEMORY,
		ARRAY_IN_PHYSICAL_MEMORY
	};

	// All these functions return the index of the member that was just added
	int AddMemberGeneric(atLiteralHashValue namehash, psoType type, ptrdiff_t offset = -1);
	int AddMemberEnum(atLiteralHashValue namehash, psoType type, atLiteralHashValue enumName, ptrdiff_t offset = -1);
	int AddMemberStruct(atLiteralHashValue namehash, psoType type, atLiteralHashValue innerStructName, ptrdiff_t offset = -1);
	int AddMemberString(atLiteralHashValue namehash, psoType type, u32 elementCount = 0, ptrdiff_t offset = -1, u8 namespaceIndex = 0);
	int AddMemberArray(atLiteralHashValue namehash, psoType type, int indexOfArrayContentMember, size_t alignment = 1, ptrdiff_t offset = -1, ArrayLocation location = ARRAY_IN_VIRTUAL_MEMORY);
	int AddMemberMaxSizeArray(atLiteralHashValue namehash, psoType type, int indexOfArrayContentMember, u32 maxCount, size_t alignment = 1, ptrdiff_t offset = -1, ArrayLocation location = ARRAY_IN_VIRTUAL_MEMORY);
	int AddMemberArrayWithCounter(atLiteralHashValue namehash, psoType type, int indexOfArrayContentMember, size_t offsetOfCounterMember, size_t alignment = 1, ptrdiff_t offset = -1, ArrayLocation location = ARRAY_IN_VIRTUAL_MEMORY);
	int AddMemberBitset(atLiteralHashValue namehash, psoType type, atLiteralHashValue enumHash, u16 numBits, ptrdiff_t offset = -1);
	int AddMemberMap(atLiteralHashValue namehash, psoType type, int indexOfKeyMember, int indexOfValueMember, ptrdiff_t offset = -1);

	// NOTES: 
	// If you pass 0 for the new name, we just copy the old name
	// Does NOT copy the offset. Pass in member.GetOffset() if you really want it to.
	int CopyMember(psoRscMemberSchemaData& member, atLiteralHashValue newName = atLiteralHashValue::Null(), ptrdiff_t offset = -1);

	ptrdiff_t AddPadding(size_t pad, size_t align = 1); // Inserts N bytes of padding, plus addtional padding to align the padding space if necessary

	atLiteralHashValue GetName() { return m_Structure.m_NameHash; }
	size_t GetSize() { return m_Structure.m_Size; }
	size_t GetAlign() { return (size_t)(1 << m_Structure.m_AlignPower); }

	void CopyTo(psoRscStructSchemaData& dest);

	~psoRscBuilderStructSchema();

	atArray<psoRscMemberSchemaData>& GetMemberArray() { return m_Members; }

protected:
	friend psoRscBuilder;
	friend psoRscBuilderInstance;

	psoRscBuilderStructSchema(psoRscBuilder* builder);

	void FinishBuilding();

	psoRscMemberSchemaData& AddMember(atLiteralHashValue namehash, psoType type, ptrdiff_t offset);

	void ComputeOffsetAndUpdateSize(psoRscMemberSchemaData& mem, ptrdiff_t offset);
	void FindMemberTraits(const psoRscMemberSchemaData& mem, size_t& size, size_t& align);

	psoRscBuilder*	m_Builder;
	psoRscStructSchemaData m_Structure;
	atArray<psoRscMemberSchemaData> m_Members;
	bool m_UnderConstruction;
	bool m_AutoPack;
};


class psoRscBuilderEnumSchema
{
public:
	void AddValue(atLiteralHashValue namehash, int value);

	psoEnumSchema GetSchema();

	~psoRscBuilderEnumSchema();

	void CopyTo(psoRscEnumSchemaData& dest);

protected:
	friend psoRscBuilder;

	psoRscBuilderEnumSchema();

	void FinishBuilding();

	psoRscEnumSchemaData	m_Enum;
	atArray<psoRscEnumSchemaValueData> m_Values;
	bool m_UnderConstruction;
};


class psoRscBuilderSchemaCatalog
{
public:
	atArray<psoRscBuilderStructSchema*> m_StructSchemas;
	atArray<psoRscBuilderEnumSchema*> m_EnumSchemas;

	psoRscBuilderSchemaCatalog() {}
	~psoRscBuilderSchemaCatalog();
};

// Each InstanceSet contains a collection of instances or arrays of instances of one type, and
// each one becomes one StructArray entry in the final file
class psoRscBuilderInstanceSet
{
public:
	psoRscBuilderInstanceSet()
	: m_StructSchema(NULL)
	, m_PodType(psoType::TYPE_INVALID)
	, m_Align(0)
	, m_Size(0)
	, m_PhysicalHeap(false)
	{}
	~psoRscBuilderInstanceSet();

	size_t FindSetSize() const;  // Total size of all of the instances
	size_t FindUnitSize() const; // Returns the size of each unit (each instance) in this set

	bool CanHold(size_t sizeInBytes) const;

	void AddInstance(psoRscBuilderInstance* inst);

	atArray<psoRscBuilderInstance*> m_Instances;
	psoRscBuilderStructSchema* m_StructSchema;
	psoType			m_PodType;
	size_t			m_Align;
	size_t			m_Size;
	bool			m_PhysicalHeap;
};

class psoRscBuilderInstance
{
public:
	~psoRscBuilderInstance();
	psoRscBuilderInstance();

	char*			m_Data;
	size_t			m_Size;
	psoStructId		m_BaseId;
	bool			m_OwnsData;
};

class psoRscBuilderInstanceDataCatalog
{
public:
	psoRscBuilderInstance*		m_RootObject;

	typedef atMap<u32, const char*> StringMap;
	typedef atMap<int, StringMap> NamespaceMap;

	NamespaceMap m_AllHashStrings;

	psoRscBuilderInstanceDataCatalog();
	~psoRscBuilderInstanceDataCatalog();

	psoRscBuilderInstanceSet& FindOrCreateInstanceSet(psoRscBuilderStructSchema* structSchema, size_t count, size_t alignment, bool physicalHeap);
	psoRscBuilderInstanceSet& FindOrCreateInstanceSet(psoType podType, size_t sizeInBytes, size_t alignment, bool physicalHeap);

	void AddInstancesToResource(psoResourceData* rsc);
	void AddInstanceToResource( psoRscBuilderInstanceSet& set, int structArrayIdx, psoResourceData* rsc );
	void ConvertHashstringsToInstances();

	// For 'original' hashstring namespaces, atHashString and atFinalHashString
	void AddStringsToResource(psoPtr<char>& dest, StringMap& map, bool encrypt);

	typedef atMap<u32, atArray<psoRscBuilderInstanceSet*> > InstanceSetMap;

	struct Iterator
	{
		Iterator(psoRscBuilderInstanceDataCatalog& cat) : m_Cat(cat), m_NonNativeNum(0), m_MapIter(m_Cat.m_Instances.CreateIterator()), m_SetIdx(0) {}

		psoRscBuilderInstanceSet& operator*();
		bool AtEnd();
		void Next();

		psoRscBuilderInstanceDataCatalog& m_Cat;
		InstanceSetMap::Iterator m_MapIter;
		int m_SetIdx;
		int m_NonNativeNum;
	};

	InstanceSetMap m_Instances;

	// Non-native alignment is tricky because of the way we normally pack data together into 
	// "StructArrays". I.e. big arrays that contain all instances of a given type. Then doing fixups
	// is easy, we just step from one item to the next in the PSO file until we hit the end of the StructArray.
	// When we have non-native alignment this is harder. Imagine we are doing fixups on a 16b aligned, 4b object.
	// If we have two instances of those objects, they need to be 16b apart. Worse, if we have 2 _arrays_ of instances
	// the array elements are only 4b apart but the arrays are 16b apart. Putting these into a StructArray would
	// leave gaps and we don't want to fix up data that's in these gaps. So instead we'll put each non-natively
	// aligned structure into its own structarray (which here corresponds to a psoBuilderInstanceSet)
	atArray<psoRscBuilderInstanceSet*> m_NonNativeAlignedInstances;  

	// Only used during pointer fixup
	atArray<psoRscBuilderInstance*> m_InstancesSortedBySourceAddr;

private:
	psoRscBuilderInstanceSet* FindOrCreateInstanceSet(u32 hash, size_t nativeAlignment, size_t sizeInBytes, size_t requestedAlignment, bool physicalHeap);

};


class psoRscBuilder
{
public:
	psoRscBuilder();
	~psoRscBuilder();

	// If namehash is 0, an anonymous ID will be assigned
	psoRscBuilderStructSchema&		CreateStructSchema(atLiteralHashValue namehash, size_t size = (size_t)(-1), size_t align = 1);
	// If namehash is 0, an anonymous ID will be assigned
	psoRscBuilderEnumSchema&		CreateEnumSchema(atLiteralHashValue namehash);

	// IMPORTANT NOTE! This may return a different instance than the one you started with, if
	// you were building an anonymous instance and there was one with the same signature already
	// we'll delete the new one and return the old one
	psoRscBuilderStructSchema&		FinishStructSchema(psoRscBuilderStructSchema& in);
	psoRscBuilderEnumSchema&		FinishEnumSchema(psoRscBuilderEnumSchema& in);

	psoRscBuilderStructSchema*		FindStructSchema(atLiteralHashValue namehash);
	psoRscBuilderEnumSchema*		FindEnumSchema(atLiteralHashValue namehash);

	psoRscBuilderInstance& AddStructInstances(psoRscBuilderStructSchema& schema, const char* data, size_t count, size_t align, bool physicalHeap);
	psoRscBuilderInstance& CreateStructInstances(psoRscBuilderStructSchema& schema, size_t count, size_t align, bool physicalHeap);


	psoRscBuilderInstance& AddPodArray(psoType type, const char* data, size_t sizeInBytes, size_t align, bool physicalHeap);
	psoRscBuilderInstance& CreatePodArray(psoType type, size_t sizeInBytes, size_t align, bool physicalHeap);

	void AddExtraString(int nsIndex, u32 hash, const char* str);

	void SetRootObject(psoRscBuilderInstance& inst);

	void FinishBuilding();

	psoResourceData* ConstructResourceData();

	static psoResourceData* CopyResourceData(psoResourceData* data, bool copyDebugStrings);

#if __RESOURCECOMPILER
	bool SaveResource(const char* filename);
	static bool SaveCopyOfResource(const char* filename, psoResourceData* data, bool copyDebugStrings);
#endif

protected:
	void ConvertPointersToStructIds(psoResourceData* rscData); // Operates on the passed in data, not m_Data.
	void ConvertPointersToStructIds_PodArray(psoRscStructArrayTableData& structArray);
	void ConvertPointersToStructIds_Struct(psoStruct& str);
	void ConvertPointersToStructIds_Member(psoStruct& str, psoMember& mem);

	void ConvertPointerToStructId(psoFake32::CharPtr & ptr);
	void ConvertPointerToStructId(psoFake64::CharPtr & ptr);

	psoRscBuilderSchemaCatalog		m_Schemas;
	psoRscBuilderInstanceDataCatalog	m_Instances;
	bool							m_UnderConstruction;
	bool							m_IncludeChecksum;
	u16								m_NextAnonId;
};

} // namespace rage

#endif // PARSER_PSORSCBUILDER_H

