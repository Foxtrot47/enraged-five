// 
// parser/rtstructure.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_RTSTRUCTURE_H
#define PARSER_RTSTRUCTURE_H

#include "memberarray.h"
#include "memberenum.h"
#include "membermatrix.h"
#include "memberstring.h"
#include "memberstruct.h"
#include "membervector.h"
#include "structure.h"

namespace rage {
	class Float16;
	class BoolV;
	class VecBoolV;
	class ScalarV;
	class Mat33V;
	class Mat34V;
	class Mat44V;
	class Vec2V;
	class Vec3V;
	class Vec4V;
	class Matrix34;
	class Matrix44;
	class Vector2;
	class Vector3;
	class Vector4;
	
// PURPOSE: A subclass of parStructure that allows you to define a parStructure on the fly without registering it.
class parRTStructure : public parStructure
{
public:
	parRTStructure(int maxMembers = 32);
	parRTStructure(const char* name, int maxMembers = 32);

	virtual ~parRTStructure();
	
	void Reset();

	// PURPOSE: Adds another structure as a member. Generally you'd only use this to add
	//			another parRTStructure. For adding a parsable object to this structure
	//			use AddStructureInstance()
	// PARAMS:
	//		name - The name of the member
	//		str - A reference to the structure to add.
	parMemberStruct* AddStructure(const char* name, parStructure& str);

	// PURPOSE: Add a value from an already allocated structure as a member.
	// PARAMS:
	//		_Type - the type of value to add.
	//		name - Name of the member
	//		value - The address of the value.
	// NOTES:
	//		AddValue uses an absolute address for the memory, so you can't use the same parRTStructure to
	//		load values into two different objects if you've defined the members with AddValue.
	//		It's very useful for loading into static, stack or global data though. For example:
	//		<code>
	//		float f1, f2;
	//		int i1;
	//		parRTStructure str = parRTStructure("mystruct");
	//		str.AddValue("F1", &f1);
	//		str.AddValue("F2", &f2);
	//		str.AddValue("I1", &i1);
	//		PARSER.LoadFromStructure("somefile.xml", "", str);
	//		</code>
	//		
	//		Note: This function is overloaded for each of the supported types
	//		(e.g. vector)
	inline parMember* AddValue(const char* name, s8* valueAddr);
	inline parMember* AddValue(const char* name, u8* valueAddr);	
	inline parMember* AddValue(const char* name, s16* valueAddr);
	inline parMember* AddValue(const char* name, u16* valueAddr);
	inline parMember* AddValue(const char* name, int* valueAddr);
	inline parMember* AddValue(const char* name, u32* valueAddr);
	inline parMember* AddValue(const char* name, s64* valueAddr);
	inline parMember* AddValue(const char* name, float* valueAddr);
	inline parMember* AddValue(const char* name, Float16* valueAddr);
	inline parMember* AddValue(const char* name, bool* valueAddr);
	inline parMember* AddValue(const char* name, Vector2* valueAddr);
	inline parMember* AddValue(const char* name, Vector3* valueAddr);
	inline parMember* AddValue(const char* name, Vector4* valueAddr);
	inline parMember* AddValue(const char* name, BoolV* valueAddr);
	inline parMember* AddValue(const char* name, VecBoolV* valueAddr);
	inline parMember* AddValue(const char* name, ScalarV* valueAddr);
	inline parMember* AddValue(const char* name, Vec2V* valueAddr);
	inline parMember* AddValue(const char* name, Vec3V* valueAddr);
	inline parMember* AddValue(const char* name, Vec4V* valueAddr);
	inline parMember* AddValue(const char* name, Matrix34* valueAddr);
	inline parMember* AddValue(const char* name, Matrix44* valueAddr);
	inline parMember* AddValue(const char* name, Mat33V* valueAddr);
	inline parMember* AddValue(const char* name, Mat34V* valueAddr);
	inline parMember* AddValue(const char* name, Mat44V* valueAddr);
	inline parMember* AddValue(const char* name, ConstString* valueAddr);

	// PURPOSE: Adds a fixed-length string
	// PARAMS:
	//		name - Name of the member
	//		valueAddr - Address of the string
	//		size - Length of the string
	// NOTES:
	//		For variable length strings, use AddValue with a rage::atString 
	inline parMember* AddStringValue(const char* name, char* valueAddr, int size); 

	// PURPOSE: Adds an array of values to an rtStrcture. The array must be of a fixed size
	// PARAMS:
	//		_Type - the type of value in the array
	//		name - The name of the array
	//		array - The address of the array's first element
	//		count - The number of items in the array
	template<typename _Type>
	inline parMember* AddValueArray(const char* name, _Type* array, int count);

	template<typename _Type>
	inline parMember* AddValueAtArray(const char* name, atArray<_Type>* atarray);

	// PURPOSE: Adds a pointer to external named data.
	// PARAMS:
	//		_Type - The type of pointer
	//		name - The name of the member
	//		nameToPtr - Callback that converts a const char* string to an pointer
	//		ptrToName - Callback that converts a pointer to a string
	template<typename _Type>
	inline parMemberStruct* AddExternalNamedPtr(const char* name, _Type** pointerAddr, _Type*(*nameToPtr)(const char*), const char* (*ptrToName)(const _Type*)) {
		parMemberStruct* str = AddPointerMember(name, NULL, (parPtrToMember)pointerAddr);
		str->GetData()->m_NameToPtrCB = reinterpret_cast< void*(*)(const char*) > ( nameToPtr );
		str->GetData()->m_PtrToNameCB = reinterpret_cast< const char*(*)(const void*) > ( ptrToName );
		str->GetData()->m_Subtype = parMemberStructSubType::SUBTYPE_EXTERNAL_NAMED_POINTER;
		return str;
	}

	// PURPOSE: Adds an instance of a parsable class or struct.
	// PARAMS:
	//		_Type - The type of the parsable class to add
	//		name - The name of the member
	//		instanceRef - A reference to the instance.
	template<typename _Type>
	inline parMemberStruct* AddStructureInstance(const char* name, _Type& instanceRef) {
		parMemberStruct* pms = AddStructure(name, *(instanceRef.parser_GetStructure()));
		pms->GetData()->m_Offset = reinterpret_cast<char*>(&instanceRef) - reinterpret_cast<char*>(NULL);
		return pms;
	}

	// PURPOSE: Adds a pointer to parsable data
	// PARAMS:
	//		_Type - The type of pointer to add 
	//		name - The name of the member
	//		pointerAddr - The address of the pointer
	// NOTES: The actual pointed-to data may be derived from _Type
	template<typename _Type>
	inline parMemberStruct* AddOwningPtr(const char* name, _Type** pointerAddr) {
		parMemberStruct* str = AddPointerMember(name, NULL, (parPtrToMember)pointerAddr);
		str->GetData()->m_StructurePtr = _Type::parser_GetStaticStructure();
		return str;
	}

	// PURPOSE: Add a new simple member to the structure.Use this when AddValue isn't enough
	// PARAMS:
	//		name - The name of the member
	//		structAddr - The base address of the structure
	//		memberAddr - The pointer to the member
	//		type - The type of member to add (must be simple member type)
	// NOTES:
	//		structAddr may be NULL if there is no containing structure (e.g. you're loading and saving an
	//		rtstructure created on the stack containing stack and/or global data)
	parMemberSimple* AddSimpleMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr, parMember::Type type);

	// PURPOSE: Add a new vector member to the structure. Use this when AddValue isn't enough
	// PARAMS:
	//		name - The name of the member
	//		structAddr - The base address of the structure
	//		memberAddr - The pointer to the member
	//		type - The type of member to add (must be simple member type)
	// NOTES:
	//		structAddr may be NULL if there is no containing structure (e.g. you're loading and saving an
	//		rtstructure created on the stack containing stack and/or global data)
	parMemberVector* AddVectorMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr, parMember::Type type);

	// PURPOSE: Add a new matrix member to the structure. Use this when AddValue isn't enough
	// PARAMS:
	//		name - The name of the member
	//		structAddr - The base address of the structure
	//		memberAddr - The pointer to the member
	//		type - The type of member to add (must be simple member type)
	// NOTES:
	//		structAddr may be NULL if there is no containing structure (e.g. you're loading and saving an
	//		rtstructure created on the stack containing stack and/or global data)
	parMemberMatrix* AddMatrixMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr, parMember::Type type);

	// PURPOSE: Add a new string member to the structure. Use this when AddValue isn't enough
	// PARAMS:
	//		name - The name of the member
	//		structAddr - The base address of the structure
	//		memberAddr - Offset of the member
	//		subtype - What kind of string
	// NOTES:
	//		structAddr may be NULL if there is no containing structure (e.g. you're loading and saving an
	//		rtstructure created on the stack containing stack and/or global data)
	parMemberString* AddStringMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr, parMemberString::SubType subtype, int size = -1);

	// PURPOSE: Add a new array member to the structure.
	// PARAMS:
	//		name - The name of the member
	//		baseAddr - The address of the first member in the array (or, for atArrays, the address of the array)
	//		count - The number of items in the array (-1 for atArrays)
	//		size - The size of each array item
	//		prototype - Description of the array contents
	//		arrayType - What type of array (currently SUBTYPE_MEMBER and SUBTYPE_ATARRAY are supported)
	parMemberArray* AddArrayMember(const char* name, parPtrToArray baseAddr, size_t count, size_t size, parMember* prototype, parMemberArray::SubType arrayType);

	// PURPOSE: Converts the most recently added member into an array of that member.
	parMemberArray* ArrayifyLastMember(const char* name, parPtrToArray valueAddr, size_t count, size_t sizeOfElement, parMemberArray::SubType arrayType);	

	// PURPOSE: Given a parPtrToStructure pointer to an instance of the described class or any derived class,
	// returns the parStructure that describes the concrete class of the instance.
	virtual parStructure* GetConcreteStructure(parConstPtrToStructure UNUSED_PARAM( structAddr ) )
	{
		return this;
	}

protected:
	parMemberStruct* AddPointerMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr);

	struct AnyData
	{
		union {
			// we make sure the struct is big enough to hold any data
			char	StructDataBuf	[sizeof(parMemberStruct::Data)];
			char	ArrayDataBuf	[sizeof(parMemberArray::Data)];
			char	EnumDataBuf		[sizeof(parMemberEnum::Data)];
			char	SimpleDataBuf	[sizeof(parMemberSimple::Data)];
			char	StringDataBuf	[sizeof(parMemberString::Data)];
			char	VectorDataBuf	[sizeof(parMemberVector::Data)];
			char	MatrixDataBuf	[sizeof(parMemberMatrix::Data)];
		};
	};

	atArray<AnyData> m_MemberData;
};

inline parMember* parRTStructure::AddValue(const char* name, s8* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_CHAR);
}

inline parMember* parRTStructure::AddValue(const char* name, u8* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_UCHAR);
}

inline parMember* parRTStructure::AddValue(const char* name, s16* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_SHORT);
}

inline parMember* parRTStructure::AddValue(const char* name, u16* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_USHORT);
}

inline parMember* parRTStructure::AddValue(const char* name, int* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_INT);
}

inline parMember* parRTStructure::AddValue(const char* name, u32* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_UINT);
}

inline parMember* parRTStructure::AddValue(const char* name, s64* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_INT64);
}

inline parMember* parRTStructure::AddValue(const char* name, float* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_FLOAT);
}

inline parMember* parRTStructure::AddValue(const char* name, Float16* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_FLOAT16);
}

inline parMember* parRTStructure::AddValue(const char* name, bool* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_BOOL);
}

inline parMember* parRTStructure::AddValue(const char* name, ScalarV* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_SCALARV);
}

inline parMember* parRTStructure::AddValue(const char* name, BoolV* valueAddr) {
	return AddSimpleMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_BOOLV);
}

inline parMember* parRTStructure::AddValue(const char* name, Vector2* valueAddr) {
	return AddVectorMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_VECTOR2);
}

inline parMember* parRTStructure::AddValue(const char* name, Vector3* valueAddr) {
	return AddVectorMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_VECTOR3);
}

inline parMember* parRTStructure::AddValue(const char* name, Vector4* valueAddr) {
	return AddVectorMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_VECTOR4);
}

inline parMember* parRTStructure::AddValue(const char* name, VecBoolV* valueAddr) {
	return AddVectorMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_VECBOOLV);
}

inline parMember* parRTStructure::AddValue(const char* name, Vec2V* valueAddr) {
	return AddVectorMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_VEC2V);
}

inline parMember* parRTStructure::AddValue(const char* name, Vec3V* valueAddr) {
	return AddVectorMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_VEC3V);
}

inline parMember* parRTStructure::AddValue(const char* name, Vec4V* valueAddr) {
	return AddVectorMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_VEC4V);
}

inline parMember* parRTStructure::AddValue(const char* name, Matrix34* valueAddr) {
	return AddMatrixMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_MATRIX34);
}

inline parMember* parRTStructure::AddValue(const char* name, Matrix44* valueAddr) {
	return AddMatrixMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_MATRIX44);
}

inline parMember* parRTStructure::AddValue(const char* name, Mat33V* valueAddr) {
	return AddMatrixMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_MAT33V);
}

inline parMember* parRTStructure::AddValue(const char* name, Mat34V* valueAddr) {
	return AddMatrixMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_MAT34V);
}

inline parMember* parRTStructure::AddValue(const char* name, Mat44V* valueAddr) {
	return AddMatrixMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberType::TYPE_MAT44V);
}

inline parMember* parRTStructure::AddValue(const char* name, ConstString* valueAddr) {
	return AddStringMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberStringSubType::SUBTYPE_CONST_STRING, -1);
}


#if 0
template<typename _Type>
inline parMember* parRTStructure::AddValueArray(const char* name, _Type* valueAddr, int count) {
	AddValue("Item", reinterpret_cast<_Type*>(NULL));
	parMemberArray* pma = ArrayifyLastMember(name, valueAddr, count, sizeof(_Type), parMemberArraySubType::SUBTYPE_MEMBER);
	pma->GetData()->m_AllocateArray = rage_new atDelegate<void (void*, u32)>(&parMemberArrayCreators::CreateRawArray<_Type>);
	return pma;
}
#endif

template<typename _Type>
inline parMember* parRTStructure::AddValueAtArray(const char* name, atArray<_Type>* atarray)
{
	AddStructure("Item", *_Type::parser_GetStaticStructure());
	return ArrayifyLastMember(name, atarray, -1, sizeof(_Type), parMemberArraySubType::SUBTYPE_ATARRAY);
}

parMember* parRTStructure::AddStringValue(const char* name, char* valueAddr, int size)
{
	return AddStringMember(name, NULL, reinterpret_cast<parPtrToMember>(valueAddr), parMemberStringSubType::SUBTYPE_MEMBER, size);
}


}

#endif
