// 
// parser/tree.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_TREE_H
#define PARSER_TREE_H

#include "atl/array.h"
#include "parsercore/stream.h"

namespace rage {

class parTreeNode;

// PURPOSE: A parTree represents an entire tree of parTreeNodes. It has a root node, and can also have a
// nametable for sharing strings among the nodes.
class parTree
{
public:
	parTree();
	~parTree();

	// RETURNS: A pointer to the root node in the tree.
	parTreeNode*		GetRoot();

	// PURPOSE: Sets the root node of the tree.
	void				SetRoot(parTreeNode* node);

	// RETURNS: A pointer to a newly created root node.
	parTreeNode*		CreateRoot();


protected:
	friend class parManager;
	friend class parInputFormatInfo;
	friend class parOutputFormatInfo;

	static parTree*		LoadFromStream(parStreamIn* stream);
	static bool			SaveToStream(parStreamOut* stream, parTree* tree);
	static bool			SaveToStream(parStreamOut* stream, parTreeNode* treenode);

	parTreeNode*		m_Root;
	parNameTable		m_NameTable;

	// Used during tree construction
	parTreeNode*		m_Current;
	parTreeNode*		m_Sibling;

	void				ReadBeginElement(parElement& elt, bool leaf);
	void				ReadEndElement(bool leaf);
	void				ReadData(char* data, int size, bool moreData);
};

inline parTreeNode* parTree::GetRoot()
{
	return m_Root;
}

inline void parTree::SetRoot(parTreeNode* node)
{
	m_Root = node;
}

} // namespace rage

#endif // PARSER_TREE_H
