// 
// parser/psobyteswap.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psobyteswap.h"


#include "manager.h"
#include "memberarray.h"
#include "memberenum.h"
#include "membermap.h"
#include "membermatrix.h"
#include "membersimple.h"
#include "memberstring.h"
#include "memberstruct.h"
#include "membervector.h"
#include "optimisations.h"
#include "psofile.h"

#include "atl/bitset.h"
#include "atl/array.h"
#include "math/float16.h"
#include "string/unicode.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/vectortypes.h"
#include "vectormath/boolv.h"
#include "vectormath/scalarv.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"
#include "vectormath/vecboolv.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"

PARSER_OPTIMISATIONS();

using namespace rage;

// ByteSwapping helper functions
namespace rage 
{
namespace sysEndian
{

	// OLD-style schema data
	// (new-style schemas do their byte swapping from their resource ctors)
	template<> inline void SwapMe(psoStructureMapData& d)
	{
		// Don't swap the header - it's always bigendian
		SwapMe(d.m_RootObject);
		SwapMe(d.m_NumStructArrays);
		SwapMe(d.m_Flags);
	}

	template<> inline void SwapMe(psoStructArrayTableData& d)
	{
		SwapMe(d.m_TypeHash);
		SwapMe(d.m_Offset);
		SwapMe(d.m_Size);
	}

	template<> inline void SwapMe(psoSchemaMemberData& d)
	{
		SwapMe(d.m_NameHash);
		SwapMe(d.m_Type);
//		SwapMe(d.m_SubType); // Intentionally not swapping this, the DECLARE_BITFIELD macros should do the right thing for us here
		SwapMe(d.m_OffsetLowBits);

		CompileTimeAssert(sizeof(d.m_Type) == 1);
		// OK to read d.m_Type before or after swapping since it's only 1 byte
		if (d.m_Type == parMemberType::TYPE_ENUM || d.m_Type == parMemberType::TYPE_STRUCT)
		{
			SwapMe(d.m_ReferentHash);
		}
		else
		{
			SwapMe(d.m_Count);
			SwapMe(d.m_AlignmentAndMemberIndex);
		}

	}

	template<> inline void SwapMe(psoSchemaStructureData& d)
	{
		SwapMe(d.m_Type);
		SwapMe(d.m_Flags);
		SwapMe(d.m_NumMembers);
		SwapMe(d.m_Size);
		SwapMe(d.m_MajorVersion);
	}

	template<> inline void SwapMe(psoEnumTableEntry& d)
	{
		SwapMe(d.m_NameHash);
		SwapMe(d.m_Value);
	}

	template<> inline void SwapMe(psoSchemaEnumData& d)
	{
		SwapMe(d.m_Type);
		SwapMe(d.m_NumEnums);
	}

	template<> inline void SwapMe(psoSchemaCatalogData& d)
	{
		// IFF tag is already (and remains in) big-endian format
		SwapMe(d.m_NumTypes);
	}

	template<> inline void SwapMe(psoTypeTableData& d)
	{
		SwapMe(d.m_TypeHash);
		SwapMe(d.m_Offset);
	}

	// Fake 32-bit pointer types
	template<typename Type> inline void SwapMe(psoFake32::Ptr<Type>& d)
	{
		SwapMe(d.m_StructId);
	}

	template<> inline void SwapMe(psoFake32::AtString& d)
	{
		SwapMe(d.m_Data);
		SwapMe(d.m_Length);
		SwapMe(d.m_Allocated);
	}

	template<> inline void SwapMe(psoFake32::AtBitset& d)
	{
		SwapMe(d.m_Bits);
		SwapMe(d.m_Size);
		SwapMe(d.m_BitSize);
	}

	template<> inline void SwapMe(psoFake32::AtArray16& d)
	{
		SwapMe(d.m_Elements);
		SwapMe(d.m_Count);
		SwapMe(d.m_Capacity);
	}

	template<> inline void SwapMe(psoFake32::AtArray32& d)
	{
		SwapMe(d.m_Elements);
		SwapMe(d.m_Count);
		SwapMe(d.m_Capacity);
	}

	template<> inline void SwapMe(psoFake32::AtBinMap& d)
	{
		SwapMe(d.m_Data);
	}

	// Fake 64-bit pointer types
	template<typename Type> inline void SwapMe(psoFake64::Ptr<Type>& d)
	{
		SwapMe(d.m_StructId);
	}

	template<> inline void SwapMe(psoFake64::AtString& d)
	{
		SwapMe(d.m_Data);
		SwapMe(d.m_Length);
		SwapMe(d.m_Allocated);
	}

	template<> inline void SwapMe(psoFake64::AtBitset& d)
	{
		SwapMe(d.m_Bits);
		SwapMe(d.m_Size);
		SwapMe(d.m_BitSize);
	}

	template<> inline void SwapMe(psoFake64::AtArray16& d)
	{
		SwapMe(d.m_Elements);
		SwapMe(d.m_Count);
		SwapMe(d.m_Capacity);
	}

	template<> inline void SwapMe(psoFake64::AtArray32& d)
	{
		SwapMe(d.m_Elements);
		SwapMe(d.m_Count);
		SwapMe(d.m_Capacity);
	}

	template<> inline void SwapMe(psoFake64::AtBinMap& d)
	{
		SwapMe(d.m_Data);
	}

	template<> inline void SwapMe(Vec::Vector_2& d)
	{
		SwapMe(d.x);
		SwapMe(d.y);
	}

	template<> inline void SwapMe(Vec::Vector_3& d)
	{
		SwapMe(d.x);
		SwapMe(d.y);
		SwapMe(d.z);
	}

} // namespace sysEndian
} // namespace rage


template<typename _Type> void SwapArrayOf(char* rawPtr, size_t sizeInBytes)
{
	_Type* ptr = reinterpret_cast<_Type*>(rawPtr);
	for(size_t i = 0; i < sizeInBytes / sizeof(_Type); i++)
	{
		SwapMe(ptr[i]);
	}
}

static void SwapStructArray(psoType type, char* startOfArray, size_t sizeInBytes)
{
	FastAssert(type.IsStructArrayType());

	switch(type.GetEnum())
	{
	// 8-bit types
	case psoType::TYPE_BOOL:
	case psoType::TYPE_S8:
	case psoType::TYPE_U8:
	case psoType::TYPE_STRING_MEMBER:
	case psoType::TYPE_ENUM8:
		// No swap needed
		break;

	// 16-bit types
	case psoType::TYPE_S16:
	case psoType::TYPE_U16:
	case psoType::TYPE_FLOAT16:
	case psoType::TYPE_WIDE_STRING_MEMBER:
	case psoType::TYPE_ENUM16:
		SwapArrayOf<u16>(startOfArray, sizeInBytes);
		break;

	// 32-bit types
	case psoType::TYPE_S32:
	case psoType::TYPE_U32:
	case psoType::TYPE_ENUM32:
	case psoType::TYPE_STRINGHASH:
	case psoType::TYPE_PARTIALSTRINGHASH:
	case psoType::TYPE_LITERALSTRINGHASH:
	case psoType::TYPE_NSSTRINGHASH:
		SwapArrayOf<u32>(startOfArray, sizeInBytes);
		break;

	// 64-bit types
	case psoType::TYPE_S64:
	case psoType::TYPE_U64:
		SwapArrayOf<u64>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_STRUCTID:
		SwapArrayOf<psoFake32::VoidPtr>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_POINTER32:
	case psoType::TYPE_STRING_POINTER32:
	case psoType::TYPE_WIDE_STRING_POINTER32:
	case psoType::TYPE_ARRAY_POINTER32:
		SwapArrayOf<psoFake32::VoidPtr>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_POINTER64: 
	case psoType::TYPE_STRING_POINTER64:
	case psoType::TYPE_WIDE_STRING_POINTER64:
	case psoType::TYPE_ARRAY_POINTER64:
		SwapArrayOf<psoFake64::VoidPtr>(startOfArray, sizeInBytes);		
		break;

	// Treat all the vector and matrix types as if they are arrays of floats too because that still gets us the correct swapping 
	case psoType::TYPE_FLOAT:
	case psoType::TYPE_SCALARV: 
	case psoType::TYPE_VEC2: 
	case psoType::TYPE_VEC2V:
	case psoType::TYPE_VEC3:
	case psoType::TYPE_VEC3V:
	case psoType::TYPE_VEC4V:
	case psoType::TYPE_MAT33V:
	case psoType::TYPE_MAT34V:
	case psoType::TYPE_MAT43:
	case psoType::TYPE_MAT44V:
	case psoType::TYPE_BOOLV:
	case psoType::TYPE_VECBOOLV:
		SwapArrayOf<float>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_DOUBLE:
		SwapArrayOf<double>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATSTRING32:
	case psoType::TYPE_ATWIDESTRING32:
		SwapArrayOf<psoFake32::AtString>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATSTRING64:
	case psoType::TYPE_ATWIDESTRING64:
		SwapArrayOf<psoFake64::AtString>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATARRAY32:
		SwapArrayOf<psoFake32::AtArray16>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATARRAY32_32BITIDX:
		SwapArrayOf<psoFake32::AtArray32>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATARRAY64:
		SwapArrayOf<psoFake64::AtArray16>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATARRAY64_32BITIDX:
		SwapArrayOf<psoFake64::AtArray32>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATBITSET32:
		SwapArrayOf<psoFake32::AtBitset>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATBITSET64:
		SwapArrayOf<psoFake64::AtBitset>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATBINARYMAP32:
		SwapArrayOf<psoFake32::AtBinMap>(startOfArray, sizeInBytes);
		break;

	case psoType::TYPE_ATBINARYMAP64:
		SwapArrayOf<psoFake64::AtBinMap>(startOfArray, sizeInBytes);
		break;

		// Listing these all out so we get nice errors when adding new types
	case psoType::TYPE_INVALID:
	case psoType::TYPE_STRUCT:
	case psoType::TYPE_BITSET8:
	case psoType::TYPE_BITSET16:
	case psoType::TYPE_BITSET32:
	case psoType::TYPE_ARRAY_MEMBER:
	case psoType::TYPE_ATFIXEDARRAY:
	case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
	case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
		type.PrintUnexpectedTypeError("byteswapping structarray", "structarray type");
		break;
	}
}

static void SwapInstanceData(psoSchemaCatalog catalog, psoStructureSchema schema, char* structure, char* endOfStructure, bool toNative);

template<typename _Type> void SwapMeAs(char* memAddr)
{
	sysEndian::SwapMe(*reinterpret_cast<_Type*>(memAddr));
}

static void SwapInstanceMemberData(psoSchemaCatalog catalog, psoStructureSchema schema, psoMemberSchema mem, char* memAddr, bool toNative)
{
	switch(mem.GetType().GetEnum())
	{
	// 8-bit types
	case psoType::TYPE_BOOL:
	case psoType::TYPE_S8:
	case psoType::TYPE_U8:
	case psoType::TYPE_STRING_MEMBER:
	case psoType::TYPE_ENUM8:
		// No swap necessary
		break;

	// 16-bit types
	case psoType::TYPE_S16:
	case psoType::TYPE_U16:
	case psoType::TYPE_ENUM16:
		SwapMeAs<u16>(memAddr);
		break;

	// 32-bit types
	case psoType::TYPE_S32:
	case psoType::TYPE_U32:
	case psoType::TYPE_ENUM32:
	case psoType::TYPE_STRINGHASH:
	case psoType::TYPE_PARTIALSTRINGHASH:
	case psoType::TYPE_LITERALSTRINGHASH:
	case psoType::TYPE_NSSTRINGHASH:
		SwapMeAs<u32>(memAddr);
		break;

	// 64-bit types
	case psoType::TYPE_S64:
	case psoType::TYPE_U64:
		SwapMeAs<u64>(memAddr);
		break;

	// StructID types
	case psoType::TYPE_POINTER32:
	case psoType::TYPE_STRING_POINTER32:
	case psoType::TYPE_WIDE_STRING_POINTER32:
	case psoType::TYPE_ARRAY_POINTER32:
	case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
		SwapMeAs<psoFake32::VoidPtr>(memAddr);
		break;

	// 64-bit pointer types
	case psoType::TYPE_POINTER64:
	case psoType::TYPE_STRING_POINTER64:
	case psoType::TYPE_WIDE_STRING_POINTER64:
	case psoType::TYPE_ARRAY_POINTER64:
	case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
		SwapMeAs<psoFake64::VoidPtr>(memAddr);
		break;

	// Float types
	case psoType::TYPE_FLOAT16:		SwapMeAs<Float16>(memAddr); 		break;
	case psoType::TYPE_FLOAT:		SwapMeAs<float>(memAddr);			break;
	case psoType::TYPE_DOUBLE:		SwapMeAs<double>(memAddr);			break;
	case psoType::TYPE_VEC2:		SwapMeAs<Vec::Vector_2>(memAddr);	break;
	case psoType::TYPE_VEC3:		SwapMeAs<Vec::Vector_3>(memAddr);	break;

	// Vector types
	case psoType::TYPE_VEC2V:
	case psoType::TYPE_VEC3V:
	case psoType::TYPE_VEC4V:
	case psoType::TYPE_BOOLV:
	case psoType::TYPE_VECBOOLV:
	case psoType::TYPE_SCALARV:		
		SwapMeAs<Vec4V>(memAddr);	
		break;

	case psoType::TYPE_MAT33V:		SwapMeAs<Mat33V>(memAddr); break;
	case psoType::TYPE_MAT34V:		SwapMeAs<Mat34V>(memAddr); break;
	case psoType::TYPE_MAT44V:		SwapMeAs<Mat44V>(memAddr); break;

	case psoType::TYPE_MAT43:		SwapMeAs<Mat33V>(memAddr); break; // Note using Mat33 here should work, swaps 3 column vectors

	case psoType::TYPE_STRUCT:
		{
			psoStructureSchema subSchema = catalog.FindStructureSchema(mem.GetReferentHash());
			if (parVerifyf(subSchema.IsValid(), "Couldn't find valid schema for structure member"))
			{
				SwapInstanceData(catalog, subSchema, memAddr, memAddr + subSchema.GetSize(), toNative);
			}
		}
		break;

	case psoType::TYPE_ATSTRING32:		
	case psoType::TYPE_ATWIDESTRING32:
		SwapMeAs<psoFake32::AtString>(memAddr); 
		break;

	case psoType::TYPE_ATSTRING64:		
	case psoType::TYPE_ATWIDESTRING64:
		SwapMeAs<psoFake64::AtString>(memAddr); 
		break;

	case psoType::TYPE_WIDE_STRING_MEMBER: SwapArrayOf<u16>(memAddr, mem.GetStringCount()); break;

	case psoType::TYPE_ATBINARYMAP32:	SwapMeAs<psoFake32::AtBinMap>(memAddr);	break;
	case psoType::TYPE_ATBINARYMAP64:	SwapMeAs<psoFake64::AtBinMap>(memAddr);	break;

	case psoType::TYPE_ARRAY_MEMBER:
		{
			int count = mem.GetFixedArrayCount();
			psoMemberSchema elementMember = schema.GetMemberByIndex(mem.GetArrayElementSchemaIndex());
			size_t memberSize = catalog.FindMemberSize(schema, elementMember);

			char* arrayData = memAddr;
			for(int i = 0; i < count; i++)
			{
				SwapInstanceMemberData(catalog, schema, elementMember, arrayData, toNative);
				arrayData += memberSize;
			}
		}
		break;

	case psoType::TYPE_ATFIXEDARRAY:
		{
			// copied from parMemberArray::GetFixedArrayCountAddress

			// need to find the address of the counter (since the counter is stored after all the array elements)
			psoMemberSchema elementMember = schema.GetMemberByIndex(mem.GetArrayElementSchemaIndex());
			size_t memberSize = catalog.FindMemberSize(schema, elementMember);
			size_t arrayBytes =  memberSize * mem.GetFixedArrayCount();
			arrayBytes = RoundUp<4>((int)arrayBytes);

			int* countAddr = reinterpret_cast<int*>(memAddr + arrayBytes);

			if (toNative)
			{
				SwapMe(*countAddr);
			}

			char* arrayData = memAddr;
			for(int i = 0; i < *countAddr; i++)
			{
				SwapInstanceMemberData(catalog, schema, elementMember, arrayData, toNative);
				arrayData += memberSize;
			}

			if (!toNative)
			{
				SwapMe(*countAddr);
			}			
		}
		break;

	case psoType::TYPE_ATARRAY32:			SwapMeAs<psoFake32::AtArray16>(memAddr); break;
	case psoType::TYPE_ATARRAY64:			SwapMeAs<psoFake64::AtArray16>(memAddr); break;
	case psoType::TYPE_ATARRAY32_32BITIDX:	SwapMeAs<psoFake32::AtArray32>(memAddr); break;
	case psoType::TYPE_ATARRAY64_32BITIDX:	SwapMeAs<psoFake64::AtArray32>(memAddr); break;

	case psoType::TYPE_ATBITSET32:			SwapMeAs<psoFake32::AtBitset>(memAddr); break;
	case psoType::TYPE_ATBITSET64:			SwapMeAs<psoFake64::AtBitset>(memAddr); break;

	case psoType::TYPE_BITSET8:
		// Nothing to do;
		break;

	case psoType::TYPE_BITSET16:
		{
			int bytes = RoundUp<16>(mem.GetBitsetCount()) / 8; // round # bits up, divide by 8 bits/byte
			SwapArrayOf<u16>(memAddr, bytes);
		}
		break;

	case psoType::TYPE_BITSET32:
		{
			int bytes = RoundUp<32>(mem.GetBitsetCount()) / 8; // round # bits up, divide by 8 bits/byte
			SwapArrayOf<u32>(memAddr, bytes);
		}
		break;

	case psoType::TYPE_INVALID:
	case psoType::TYPE_STRUCTID:
		mem.GetType().PrintUnexpectedTypeError("byte swapping members", "member type");
		break;
	}
}

static void SwapInstanceData(psoSchemaCatalog catalog, psoStructureSchema schema, char* structure, char* ASSERT_ONLY(endOfStructure), bool toNative)
{
	for(int i = 0; i < schema.GetNumMembers(); i++)
	{
		psoMemberSchema mem = schema.GetMemberByIndex(i);
		if (!psoConstants::IsReserved(mem.GetNameHash()))
		{
			char* memAddr = structure + mem.GetOffset();
			FastAssert(memAddr >= structure && memAddr < endOfStructure);
			SwapInstanceMemberData(catalog, schema, mem, memAddr, toNative);
		}
	}
}

#if !__BE

static inline void SwapSchemaStructure(psoSchemaStructureData& str, bool toNative)
{
	if (toNative)
		BtoNMe(str);

	for(int i = 0; i < str.m_NumMembers; i++)
	{
		SwapMe(str.GetMember(i));
	}

	if (!toNative)
		NtoBMe(str);
}

static inline void SwapSchemaEnum(psoSchemaEnumData& en, bool toNative)
{
	if (toNative)
		BtoNMe(en);

	for(int i = 0; i < en.m_NumEnums; i++)
	{
		SwapMe(en.GetEnum(i));
	}

	if (!toNative)
		NtoBMe(en);
}


namespace rage
{

void psoSwapSchemaCatalog(psoSchemaCatalogData& cat, bool toNative)
{
	if (toNative)
		BtoNMe(cat);

	char* baseSchemaAddress = reinterpret_cast<char*>(&cat);

	// Swap the typetable entries
	if (toNative)
	{
		for(u32 i = 0; i < cat.m_NumTypes; i++)
		{
			BtoNMe(cat.GetType(i));
		}
	}

	// Swap all the schemas
	for(u32 i = 0; i < cat.m_NumTypes; i++)
	{
		psoSchemaStructureData* data = reinterpret_cast<psoSchemaStructureData*>(baseSchemaAddress + cat.GetType(i).m_Offset);
		u8 type = data->m_Type; // OK to read because it's a single byte
		switch(type)
		{
		case psoConstants::ST_STRUCTURE:
			{
				SwapSchemaStructure(*data, toNative);
			}
			break;
		case psoConstants::ST_ENUM:
			{
				psoSchemaEnumData* enumData = reinterpret_cast<psoSchemaEnumData*>(data);
				SwapSchemaEnum(*enumData, toNative);
			}
			break;
		default:
			parErrorf("Can't byte swap a schema of type %d", type);
		}
	}

	// Swap the typetable entries
	if (!toNative)
	{
		for(u32 i = 0; i < cat.m_NumTypes; i++)
		{
			NtoBMe(cat.GetType(i));
		}
	}

	if (!toNative)
		NtoBMe(cat);
}


void psoSwapStructureMap(psoStructureMapData& str, bool toNative)
{
	if (toNative)
		BtoNMe(str);

	for(u32 i = 0; i < str.m_NumStructArrays; i++)
	{
		SwapMe(str.GetStructArrayEntry(i));
	}

	if (!toNative)
		NtoBMe(str);
}

}
#endif // __BE


void rage::psoSwapStructArrays( char* instanceData, psoSchemaCatalog& cat, psoStructureMapData& map, bool toNative ) 
{
	// Loop over the structarrays swapping the contents
	for(u32 i = 0; i < map.m_NumStructArrays; i++)
	{
		psoStructArrayTableData& structArray = map.GetStructArrayEntry(i);
		if (psoConstants::IsPsoType(structArray.m_TypeHash))
		{
			psoType newType = psoType::ConvertOldStructArrayTypeToPsoType(structArray.m_TypeHash, false);
			if (parVerifyf(newType.IsValid(), "Invalid pso type coming from type hash %d", structArray.m_TypeHash))
			{
				SwapStructArray(newType, instanceData + structArray.m_Offset, structArray.m_Size);
			}
		}
		else
		{
			psoStructureSchema schema = cat.FindStructureSchema(atLiteralHashValue(structArray.m_TypeHash));
			int eltSize = (int)schema.GetSize(); 
			char* data = instanceData + structArray.m_Offset;
			char* endOfData = data + structArray.m_Size;

			if (AssertVerify(eltSize > 0))
			{
				while(data + eltSize <= endOfData)
				{
					SwapInstanceData(cat, schema, data, data + eltSize, toNative);
					data += eltSize;
				}
			}
		}
	}
}

void rage::psoSwapStructArrays( psoResourceData& rsc, bool toNative) 
{
	psoSchemaCatalog cat(rsc);

	// Loop over the structarrays swapping the contents
	for(u32 i = 0; i < rsc.m_NumStructArrayEntries; i++)
	{
		psoRscStructArrayTableData& structArray = rsc.m_StructArrayTable[i];

		if (psoConstants::IsPsoType(structArray.m_NameHash.GetHash()))
		{
			psoType type = (psoType::Type)structArray.m_NameHash.GetHash();
			if (parVerifyf(type.IsStructArrayType(), "Invalid pso type coming from type hash %d", structArray.m_NameHash.GetHash()))
			{
				SwapStructArray(type, structArray.m_Data.GetPtr(), structArray.m_Size);
			}
		}
		else
		{
			psoStructureSchema schema = cat.FindStructureSchema(structArray.m_NameHash);
			int eltSize = (int)schema.GetSize();
			char* data = structArray.m_Data.GetPtr();
			char* endOfData = data + structArray.m_Size;

			while(data + eltSize <= endOfData)
			{
				SwapInstanceData(cat, schema, data, data + eltSize, toNative);
				data += eltSize;
			}
		}
	}
}

