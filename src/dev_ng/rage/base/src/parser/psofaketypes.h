// 
// psofaketypes.h
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOFAKETYPES_H
#define PARSER_PSOFAKETYPES_H

#include "psoconsts.h"

#include "data/bitfield.h"
#include "data/struct.h"

#if __WIN32
#pragma warning ( push )
#pragma warning ( disable : 4201 )
#endif

namespace rage
{

	struct psoStructId
	{
	public:
		u32 GetTableIndex() {return m_TableIndex-1;}
		u32 GetArrayOffset() {return m_ArrayOffset;}

		inline bool operator==(const psoStructId& other) { return reinterpret_cast<const u32&>(other) == *reinterpret_cast<u32*>(this); }

		inline static psoStructId Create(u32 tableIndex, u32 arrayOffset);

		inline bool IsNull() { return *reinterpret_cast<const u32*>(this) == 0; }

		inline static psoStructId Null() { psoStructId ret; ret.m_TableIndex = 0; ret.m_ArrayOffset = 0; return ret;}

	protected:
		u32 DECLARE_BITFIELD_2(
			m_TableIndex, psoConstants::OBJECT_IDENTIFIER_STRUCTARRAYTABLE_INDEX_BITS, /* The index into the StructArrayTable that describes this structure */
			m_ArrayOffset, psoConstants::OBJECT_IDENTIFIER_ARRAY_OFFSET_BITS /* The offset (in bytes) to the start of this structure in the StructArray */
			);

	};

	namespace psoFake32
	{
		// PURPOSE: We use this when serializing pointers (pointers get replaced with psoStructIds in the PSO file, and this saves some ugly
		//   reinterpret_casting)
		template<typename _Type>
		struct Ptr
		{
			typedef _Type* PtrType;
			union {
				psoStructId m_StructId;
#if !__64BIT
				PtrType		m_Pointer;
#endif
			};
		};

		typedef Ptr<void> VoidPtr;
		typedef Ptr<char> CharPtr;

		// PURPOSE: We use this to serialize atStrings and atWideStrings. 
		// The string pointer is replaced with a structId, length is left alone, and allocated is set to 0.
		struct AtString
		{
			Ptr<char> m_Data;
			unsigned short m_Length,	// Current length of string
				m_Allocated;	// Current amount of space allocated for string
		};
		// these are the same:
		typedef AtString AtWideString;

		struct ConstString
		{
			Ptr<char> m_Data;
		};


		struct AtBitset
		{
			Ptr<unsigned> m_Bits;
			unsigned short m_Size,		// Current size of the bitset, in 32-bit words
				m_BitSize;		// Current size of the bitset in bits.  Used for exact range checking.
		};

		struct AtArray16
		{
			Ptr<char> m_Elements;
			u16 m_Count, m_Capacity;
		};

		struct AtArray32
		{
			Ptr<char> m_Elements;
			u32 m_Count, m_Capacity;
		};

		struct AtBinMap
		{
			bool m_Sorted;
			char m_Pad[3];
			AtArray16 m_Data;
		};

	} // namespace psoFake32

	namespace psoFake64
	{
		template<typename _Type>
		struct Ptr
		{
			typedef _Type* PtrType;
			union {
				struct  
				{
					psoStructId		m_StructId;
					char			m_Padding[4];
				};
				u64			m_ForAlignment;
#if __64BIT
				PtrType		m_Pointer;
#endif
			};
		};

		typedef Ptr<void>	VoidPtr;
		typedef Ptr<char>	CharPtr;

		// PURPOSE: We use this to serialize atStrings and atWideStrings. 
		// The string pointer is replaced with a structId, length is left alone, and allocated is set to 0.
		struct AtString
		{
			Ptr<char> m_Data;
			unsigned short m_Length,	// Current length of string
				m_Allocated;	// Current amount of space allocated for string
			datPadding<4> m_Padding;
		};
		// these are the same
		typedef AtString AtWideString;

		struct ConstString
		{
			Ptr<char> m_Data;
		};


		struct AtBitset
		{
			Ptr<unsigned> m_Bits;
			unsigned short m_Size,		// Current size of the bitset, in 32-bit words
				m_BitSize;		// Current size of the bitset in bits.  Used for exact range checking.
			datPadding<4> m_Padding;
		};

		struct AtArray16
		{
			Ptr<char> m_Elements;
			u16 m_Count, m_Capacity;
			datPadding<4> m_Padding;
		};

		struct AtArray32
		{
			Ptr<char> m_Elements;
			u32 m_Count, m_Capacity;
		};

		struct AtBinMap
		{
			bool m_Sorted;
			char m_Pad[3];
			AtArray16 m_Data;
			datPadding<4> m_Padding;
		};

	} // namespace psoFake64

	// Note that this namespace is for use OUTSIDE of PSO classes. Other parser classes can use the fake structs, but
	// they will always be using the native pointer sizes since they're dealing with runtime data. But PSO could potenitally
	// be working with 32-bit data on a 64-bit build (seel PSO_FORCE_32BIT) so should not use this namespace.
	namespace parFake 
	{
#if __64BIT
		using namespace psoFake64;
#else
		using namespace psoFake32;
#endif
	}

#if __64BIT && !PSO_FORCE_32BIT
	typedef psoFake64::Ptr<void>	psoFakePtr;
	typedef psoFake64::AtString		psoFakeAtString;
	typedef psoFake64::ConstString	psoFakeConstString;
	typedef psoFake64::AtBitset		psoFakeAtBitset;
	typedef psoFake64::AtArray16	psoFakeAtArray16;
	typedef psoFake64::AtArray32	psoFakeAtArray32;
	typedef psoFake64::AtBinMap		psoFakeAtBinMap;
#else
	typedef psoFake32::Ptr<void>	psoFakePtr;
	typedef psoFake32::AtString		psoFakeAtString;
	typedef psoFake32::ConstString	psoFakeConstString;
	typedef psoFake32::AtBitset		psoFakeAtBitset;
	typedef psoFake32::AtArray16	psoFakeAtArray16;
	typedef psoFake32::AtArray32	psoFakeAtArray32;
	typedef psoFake32::AtBinMap		psoFakeAtBinMap;
#endif
} // namespace rage

#if __WIN32
#pragma warning (pop)
#endif

#endif