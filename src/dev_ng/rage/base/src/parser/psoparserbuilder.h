// 
// parser/psoparserbuilder.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOPARSERBUILDER_H
#define PARSER_PSOPARSERBUILDER_H

#include "manager.h"

#if PSO_USING_NATIVE_BIT_SIZE

namespace rage {

class parStructure;
class psoBuilder;
class psoBuilderInstance;
class psoBuilderStructSchema;

// PURPOSE: Saves a parsable object as a PSO file
// PARAMS:
//		_Type - must be a type registered with the parser
//		filename - name of the file to create
//		obj - the object to save
template<typename _Type>
bool psoSaveObject(const char* filename, const _Type* obj, bool includeChecksum = false);


// PURPOSE: Saves an object as a PSO file
// PARAMS:
//		filename - name of the file to create
//		structure - The parStructure that describes obj
//		obj - The object to save out
bool psoSaveFromStructure(const char* filename, const parStructure& structure, parConstPtrToStructure obj, bool includeChecksum = false);

// PURPOSE: Builds a PSO schema object based on a parStructure
psoBuilderStructSchema&  psoBuildSchemaFromStructure(psoBuilder& builder, parStructure& str);

// PURPOSE: Adds an instance of a parsable class to the PSO file
template<typename _Type>
psoBuilderInstance* psoAddParsableObject(psoBuilder& builder, const _Type* obj);

psoBuilderInstance* psoAddParsableObjectFromStructure(psoBuilder& builder, const parStructure& structure, parConstPtrToStructure obj);

//////////////////////////////////////////////////////////////////////////
// Implementations:

template<typename _Type>
inline bool psoSaveObject(const char* filename, const _Type* obj, bool includeChecksum /* = false */)
{
	const parStructure* str = PARSER.GetStructure(obj);
	FastAssert(str);
	return psoSaveFromStructure(filename, *str, obj->parser_GetPointer(), includeChecksum);
}

template<typename _Type>
inline psoBuilderInstance* psoAddParsableObject(psoBuilder& builder, const _Type* obj)
{
	const parStructure* str = PARSER.GetStructure(obj);
	FastAssert(str);
	return psoAddParsableObjectFromStructure(builder, *str, obj->parser_GetPointer());
}


} // namespace rage

#endif // PSO_USING_NATIVE_BIT_SIZE

#endif // PARSER_PSOPARSERBUILDER_H

