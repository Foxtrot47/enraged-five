// 
// parser/rtstructure.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rtstructure.h"

#include "memberarray.h"
#include "memberenum.h"
#include "memberstring.h"
#include "membervector.h"
#include "optimisations.h"

PARSER_OPTIMISATIONS();

using namespace rage;

parRTStructure::parRTStructure(int maxMembers)
{
	// All rtstructures have names. (maybe we could change this in the future, but that's the way it is now.)
	m_Flags.Set(parStructure::HAS_NAMES);
	m_Flags.Set(parStructure::ALWAYS_HAS_NAMES);
	m_Members.Reserve(maxMembers);
	m_MemberData.Reserve(maxMembers);
}

parRTStructure::parRTStructure(const char* name, int maxMembers)
{
	// All rtstructures have names. (maybe we could change this in the future, but that's the way it is now.)
	m_Flags.Set(parStructure::HAS_NAMES);
	m_Flags.Set(parStructure::ALWAYS_HAS_NAMES);
	SetName(name);
	m_Members.Reserve(maxMembers);
	m_MemberData.Reserve(maxMembers);
}


parRTStructure::~parRTStructure()
{
	Reset();
}

void parRTStructure::Reset()
{
	// since the arrays are all allocated in the pool, 
	// delete the members here instead of in parStructure's
	// d'tor (which runs after the pools get nuked)
	for(int i = 0; i < m_Members.GetCount(); i++)
	{
		delete m_Members[i];
		m_Members[i] = NULL;
	}

	m_Members.Resize(0);
	m_MemberData.Resize(0);
}

parMemberStruct* parRTStructure::AddStructure(const char* name, parStructure& str)
{
	if (m_MemberData.GetCount() + 1 > m_MemberData.GetCapacity())
	{
		parErrorf("this parRTStructure (%s) is too small. Allocate more space in the constructor", GetName());
		return NULL;
	}
	parMemberStruct::Data& data = *reinterpret_cast<parMemberStruct::Data*>(&m_MemberData.Append());
	data.Init();
	data.m_NameHash = atLiteralHashString(name).GetHash(); // Create and assign the name, to add the string to the map so we can look it up later
	data.m_Offset = 0;
	data.m_Type = parMemberType::TYPE_STRUCT;
	data.m_StructurePtr = &str;
	data.m_Flags = (1 << parMemberFlags::MEMBER_STATIC);

	parMemberStruct* pms = rage_new parMemberStruct(data);


	m_Members.Push(pms);

	return pms;
}

parMemberArray* parRTStructure::AddArrayMember(const char* name, parPtrToArray baseAddr, size_t count, size_t size, parMember* prototype, parMemberArray::SubType arrayType)
{
	if (m_MemberData.GetCount() + 1 > m_MemberData.GetCapacity())
	{
		parErrorf("this parRTStructure (%s) is too small. Allocate more space in the constructor", GetName());
		return NULL;
	}
	parMemberArray::Data& data = *reinterpret_cast<parMemberArray::Data*>(&m_MemberData.Append());
	data.Init();
	data.m_NameHash = atLiteralHashString(name).GetHash(); // Create and assign the name, to add the string to the map so we can look it up later
	data.m_Offset = reinterpret_cast<size_t>(baseAddr);
	data.m_Flags = (1 << parMemberFlags::MEMBER_STATIC);
	data.m_Type = parMemberType::TYPE_ARRAY;
	data.m_Subtype = (u8)arrayType;
	data.m_ElementSize = size;
	FastAssert(count < UINT_MAX);
	data.m_NumElements = (u32)count;

	parMemberArray* pma = rage_new parMemberArray(data, prototype);

	m_Members.Push(pma);

	return pma;
}

// To add an array:
// Add the array contents
// 'pop' and add the array member (repeat as necessary for multidim arrays)

parMemberArray* parRTStructure::ArrayifyLastMember(const char* name, parPtrToArray baseAddr, size_t count, size_t sizePerItem, parMemberArray::SubType arrayType)
{
	return AddArrayMember(name, baseAddr, count, sizePerItem, m_Members.Pop(), arrayType);
}


parMemberSimple* parRTStructure::AddSimpleMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr, parMember::Type type)
{
	if (m_MemberData.GetCount() + 1 > m_MemberData.GetCapacity())
	{
		parErrorf("this parRTStructure (%s) is too small. Allocate more space in the constructor", GetName());
		return NULL;
	}
	parMemberSimple::Data& data = *reinterpret_cast<parMemberSimple::Data*>(&m_MemberData.Append());
	data.Init();
	data.m_NameHash = atLiteralHashString(name).GetHash(); // Create and assign the name, to add the string to the map so we can look it up later
	data.m_Offset = reinterpret_cast<char*>(memberAddr) - reinterpret_cast<char*>(structAddr);
	data.m_Flags = (1 << parMemberFlags::MEMBER_STATIC);
	data.m_Type = (u8)type;

	parMemberSimple* pms = rage_new parMemberSimple(data);

	m_Members.Push(pms);

	return pms;
}

parMemberVector* parRTStructure::AddVectorMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr, parMember::Type type)
{
	if (m_MemberData.GetCount() + 1 > m_MemberData.GetCapacity())
	{
		parErrorf("this parRTStructure (%s) is too small. Allocate more space in the constructor", GetName());
		return NULL;
	}
	parMemberVector::Data& data = *reinterpret_cast<parMemberVector::Data*>(&m_MemberData.Append());
	data.Init();
	data.m_NameHash = atLiteralHashString(name).GetHash(); // Create and assign the name, to add the string to the map so we can look it up later
	data.m_Offset = reinterpret_cast<char*>(memberAddr) - reinterpret_cast<char*>(structAddr);
	data.m_Flags = (1 << parMemberFlags::MEMBER_STATIC);
	data.m_Type = (u8)type;

	parMemberVector* pmv = rage_new parMemberVector(data);

	m_Members.Push(pmv);

	return pmv;
}

parMemberMatrix* parRTStructure::AddMatrixMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr, parMember::Type type)
{
	if (m_MemberData.GetCount() + 1 > m_MemberData.GetCapacity())
	{
		parErrorf("this parRTStructure (%s) is too small. Allocate more space in the constructor", GetName());
		return NULL;
	}
	parMemberMatrix::Data& data = *reinterpret_cast<parMemberMatrix::Data*>(&m_MemberData.Append());
	data.Init();
	data.m_NameHash = atLiteralHashString(name).GetHash(); // Create and assign the name, to add the string to the map so we can look it up later
	data.m_Offset = reinterpret_cast<char*>(memberAddr) - reinterpret_cast<char*>(structAddr);
	data.m_Flags = (1 << parMemberFlags::MEMBER_STATIC);
	data.m_Type = (u8)type;

	parMemberMatrix* pmm = rage_new parMemberMatrix(data);

	m_Members.Push(pmm);

	return pmm;
}

parMemberString* parRTStructure::AddStringMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr, parMemberString::SubType subtype, int size)
{
	if (m_MemberData.GetCount() + 1 > m_MemberData.GetCapacity())
	{
		parErrorf("this parRTStructure (%s) is too small. Allocate more space in the constructor", GetName());
		return NULL;
	}
	parMemberString::Data& data = *reinterpret_cast<parMemberString::Data*>(&m_MemberData.Append());
	data.Init();
	data.m_NameHash = atLiteralHashString(name).GetHash(); // Create and assign the name, to add the string to the map so we can look it up later
	data.m_Offset = reinterpret_cast<char*>(memberAddr) - reinterpret_cast<char*>(structAddr);
	data.m_Flags = (1 << parMemberFlags::MEMBER_STATIC);
	data.m_Type = parMemberType::TYPE_STRING;
	data.m_Subtype = (u8)subtype;
	data.m_Length = size;

	parMemberString* pms = rage_new parMemberString(data);

	m_Members.Push(pms);

	return pms;
}

parMemberStruct* parRTStructure::AddPointerMember(const char* name, parPtrToStructure structAddr, parPtrToMember memberAddr) {
	if (m_MemberData.GetCount() + 1 > m_MemberData.GetCapacity())
	{
		parErrorf("this parRTStructure (%s) is too small. Allocate more space in the constructor", GetName());
		return NULL;
	}
	parMemberStruct::Data& data = *reinterpret_cast<parMemberStruct::Data*>(&m_MemberData.Append());
	data.Init();
	data.m_NameHash = atLiteralHashString(name).GetHash(); // Create and assign the name, to add the string to the map so we can look it up later
	data.m_Offset = reinterpret_cast<char*>(memberAddr) - reinterpret_cast<char*>(structAddr);
	data.m_Flags = (1 << parMemberFlags::MEMBER_STATIC);
	data.m_Type = parMemberType::TYPE_STRUCT;
	data.m_Subtype = parMemberStructSubType::SUBTYPE_POINTER;

	parMemberStruct* pms = rage_new parMemberStruct(data);

	m_Members.Push(pms);

	return pms;
}
