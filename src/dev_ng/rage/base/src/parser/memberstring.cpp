// 
// parser/memberstring.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "memberstring.h"

#include "optimisations.h"
#include "treenode.h"

#include "atl/hashstring.h"
#include "atl/string.h"
#include "atl/wstring.h"
#include "bank/bank.h"
#include "diag/output.h"
#include "string/unicode.h"
#include "system/memory.h"
#include "math/amath.h"

PARSER_OPTIMISATIONS();

using namespace rage;

STANDARD_PARMEMBER_DATA_FUNCS_DEFN(parMemberString);

void parMemberString::Data::Init()
{
	StdInit();
	m_Length = 0;
#if __BANK
	m_Description = NULL;
	m_WidgetCb = NULL;
#endif
}

void parMemberString::ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const
{
	using namespace parMemberStringSubType;
	switch ((SubType)GetSubtype()) {
	case SUBTYPE_POINTER:
		{
			char*& dest = GetMemberFromStruct<char*>(structAddr);
			delete [] dest;
			if (node->HasData())
			{
				int len = node->GetDataSize();
				if (len && node->GetData()[len-1] == '\0')
					--len;
				dest = rage_aligned_new(1) char[len + 1];
				sysMemCpy(dest, node->GetData(), len);
				dest[len] = '\0';
			}
			else
			{
				dest = NULL;
			}
		}
		break;
	case SUBTYPE_WIDE_POINTER:
		{
			char16*& dest = GetMemberFromStruct<char16*>(structAddr);
			delete [] dest;
			if (node->HasData())
			{
				if (!parVerifyf(node->FindDataEncoding() == parStream::UTF16, "Data encoding %d is wrong for string %s, should be UTF16", node->FindDataEncoding(), GetName()))
				{
					dest = NULL;
					break;
				}
				dest = rage_aligned_new(2) char16[node->GetDataSize()/sizeof(char16) + 1];
				sysMemCpy(dest, node->GetData(), node->GetDataSize());
				dest[node->GetDataSize()/sizeof(char16)] = char16(0);
			}
			else
			{
				dest = NULL;
			}
		}
		break;
	case SUBTYPE_MEMBER:
		{
			char* dest = &GetMemberFromStruct<char>(structAddr);
			if (node->HasData())
			{
				if (node->GetDataSize() > GetData()->m_Length)
				{
					parErrorf("String for data member '%s' has length %d but string only holds %d chars.  String will be truncated!\n\nOriginal string:\n\n%s", GetName(), node->GetDataSize(), GetData()->m_Length, node->GetData());
				}
				sysMemCpy(dest, node->GetData(), Min(node->GetDataSize(), GetData()->m_Length));
				dest[GetData()->m_Length-1] = '\0';
			}
			else
			{
				dest[0] = '\0';
			}
		}
		break;
	case SUBTYPE_WIDE_MEMBER:
		{
			char16* dest = &GetMemberFromStruct<char16>(structAddr);
			if (node->HasData())
			{
				if (!parVerifyf(node->FindDataEncoding() == parStream::UTF16, "Data encoding %d is wrong for string %s, should be UTF16", node->FindDataEncoding(), GetName()))
				{
					break;
				}
				if (node->GetDataSize() > sizeof(char16)*GetData()->m_Length)
				{
					parErrorf("String for data member '%s' has length %" SIZETFMT "d but string only holds %d chars.  String will be truncated!\n\nOriginal string:\n\n%ls", GetName(), node->GetDataSize()/sizeof(char16), GetData()->m_Length, (wchar_t*)node->GetData());
				}
				sysMemCpy(dest, node->GetData(), Min((u32)node->GetDataSize(), (u32)(GetData()->m_Length * sizeof(char16))));
				dest[GetData()->m_Length-1] = char16(0);
			}
			else
			{
				dest[0] = char16(0);
			}
		}
		break;
	case SUBTYPE_CONST_STRING:
		{
			if (node->HasData())
			{
				GetMemberFromStruct<ConstString>(structAddr) = node->GetData();
			}
			else
			{
				GetMemberFromStruct<ConstString>(structAddr) = "";
			}
		}
		break;
	case SUBTYPE_ATSTRING:
		{
			if (node->HasData())
			{
				GetMemberFromStruct<atString>(structAddr) = node->GetData();
			}
			else
			{
				GetMemberFromStruct<atString>(structAddr) = atString();
			}
		}
		break;
	case SUBTYPE_ATWIDESTRING:
		{
			if(node->HasData())
			{
				if (!parVerifyf(node->FindDataEncoding() == parStream::UTF16, "Data encoding %d is wrong for string %s, should be UTF16", node->FindDataEncoding(), GetName()))
				{
					break;
				}
				GetMemberFromStruct<atWideString>(structAddr) = (char16*)node->GetData();
			}
			else
			{
				GetMemberFromStruct<atWideString>(structAddr) = atWideString();
			}
		}
		break;
#if !__FINAL
	case SUBTYPE_ATNONFINALHASHSTRING:
		{
			GetMemberFromStruct<atNonFinalHashString>(structAddr) = node->GetData(); // Could be NULL
		}
		break;
#endif
	case SUBTYPE_ATFINALHASHSTRING:
		{
			GetMemberFromStruct<atFinalHashString>(structAddr) = node->GetData(); // Could be NULL
		}
		break;
	case SUBTYPE_ATHASHVALUE:
		{
			GetMemberFromStruct<atHashValue>(structAddr) = node->GetData(); // Could be NULL
		}
		break;
	case SUBTYPE_ATPARTIALHASHVALUE:
		{
			GetMemberFromStruct<u32>(structAddr) = atPartialStringHash(node->GetData()); // Could be NULL
		}
		break;
	case SUBTYPE_ATNSHASHSTRING:
		{
			const char* stringVal = node->GetData();
			u32 hash = atHashStringNamespaceSupport::ComputeHash(GetData()->GetNamespaceIndex(), stringVal);
			atHashStringNamespaceSupport::AddString(GetData()->GetNamespaceIndex(), hash, stringVal);
			GetMemberFromStruct<u32>(structAddr) = hash; 
		}
		break;
	case SUBTYPE_ATNSHASHVALUE:
		{
			const char* stringVal = node->GetData();
			u32 hash = atHashStringNamespaceSupport::ComputeHash(GetData()->GetNamespaceIndex(), stringVal);
			GetMemberFromStruct<u32>(structAddr) = hash;
		}
		break;
	default:
		parErrorf("Unknown subtype of parMemberString: %d. Node is '%s'.", GetSubtype(), node->GetElement().GetName());
		break;
	}
}

const char* parMemberString::GetStringContentsFromStruct(parPtrToStructure containingStructureAddr) const
{
	using namespace parMemberStringSubType;
	switch(GetSubtype())
	{
	case SUBTYPE_MEMBER:
		return &GetMemberFromStruct<char>(containingStructureAddr);
	case SUBTYPE_POINTER:
		return GetMemberFromStruct<char*>(containingStructureAddr);
	case SUBTYPE_CONST_STRING:
		return GetMemberFromStruct<ConstString>(containingStructureAddr).c_str();
	case SUBTYPE_ATSTRING:
		return GetMemberFromStruct<atString>(containingStructureAddr).c_str();
#if !__FINAL
	case SUBTYPE_ATNONFINALHASHSTRING:
		return GetMemberFromStruct<atHashString>(containingStructureAddr).GetCStr();
#endif
	case SUBTYPE_ATFINALHASHSTRING:
		return GetMemberFromStruct<atFinalHashString>(containingStructureAddr).GetCStr();
	case SUBTYPE_ATNSHASHSTRING:
		return atHashStringNamespaceSupport::GetString(GetData()->GetNamespaceIndex(), GetMemberFromStruct<u32>(containingStructureAddr));
	case SUBTYPE_ATHASHVALUE: // not supported
	case SUBTYPE_ATPARTIALHASHVALUE: // not supported
	case SUBTYPE_ATNSHASHVALUE:
	default:
		parErrorf("Can't use GetStringContentsFromStruct with string of type %d", GetSubtype());
	}
	return NULL;
}

const char16* parMemberString::GetWideStringContentsFromStruct(parPtrToStructure containingStructureAddr) const
{
	using namespace parMemberStringSubType;
	switch(GetSubtype())
	{
	case SUBTYPE_WIDE_MEMBER:
		return &GetMemberFromStruct<char16>(containingStructureAddr);
	case SUBTYPE_WIDE_POINTER:
		return GetMemberFromStruct<char16*>(containingStructureAddr);
	case SUBTYPE_ATWIDESTRING:
		return GetMemberFromStruct<atWideString>(containingStructureAddr);
	default:
		parErrorf("Can't sue GetWideStringContentsFromStruct with string of type %d", GetSubtype());
	}
	return NULL;
};

void parMemberString::SetFromString(parPtrToStructure structAddr, const char* name) const
{
	using namespace parMemberStringSubType;
	USES_CONVERSION;
	switch ((SubType)GetSubtype()) {
	case SUBTYPE_POINTER:
		{
			int size = name ? StringLength(name) : 0;
			char*& dest = GetMemberFromStruct<char*>(structAddr);
			delete [] dest;
			if (size)
			{
				dest = StringDuplicate(name);
			}
			else
			{
				dest = NULL;
			}
		}
		break;
	case SUBTYPE_WIDE_POINTER:
		{
			int size = name ? CountUtf8Characters(name) : 0;
			char16*& dest = GetMemberFromStruct<char16*>(structAddr);
			delete [] dest;
			if (size)
			{
				dest = WideStringDuplicate(UTF8_TO_WIDE(name));
			}
			else
			{
				dest = NULL;
			}
		}
		break;
	case SUBTYPE_MEMBER:
		{
			size_t size = name ? strlen(name)+1 : 0;
			if(size > GetData()->m_Length)
			{
				parErrorf("String for data member '%s' has length %" SIZETFMT "d but string only holds %d chars.  String will be truncated!\n\nOriginal string:\n\n%s", GetName(), size, GetData()->m_Length, name);
			}
			char* dest = &GetMemberFromStruct<char>(structAddr);
			if (name)
			{
				safecpy(dest, name, GetData()->m_Length);
				dest[GetData()->m_Length-1] = '\0';
			}
			else
			{
				dest[0] = '\0';
			}
		}
		break;
	case SUBTYPE_WIDE_MEMBER:
		{
			int size = name ? CountUtf8Characters(name)+1 : 0;
			if (size > (int)GetData()->m_Length)
			{
				parErrorf("String for data member '%s' has length %d but string only holds %d chars.  String will be truncated!\n\nOriginal string:\n\n%s", GetName(), size, GetData()->m_Length, name);
			}
			char16* dest = &GetMemberFromStruct<char16>(structAddr);
			if (name)
			{
				sysMemCpy(dest, UTF8_TO_WIDE(name), Min(size, (int)GetData()->m_Length) * sizeof(char16));
				dest[GetData()->m_Length-1] = char16(0);
			}
			else
			{
				dest[0] = char16(0);
			}
		}
		break;
	case SUBTYPE_CONST_STRING:
		{
			GetMemberFromStruct<ConstString>(structAddr) = name;
		}
		break;
	case SUBTYPE_ATSTRING:
		{
			if (name)
			{
				GetMemberFromStruct<atString>(structAddr) = name;
			}
			else
			{
				GetMemberFromStruct<atString>(structAddr).Clear();
			}
		}
		break;
	case SUBTYPE_ATWIDESTRING:
		{
			if(name)
			{
				GetMemberFromStruct<atWideString>(structAddr) = UTF8_TO_WIDE(name);
			}
			else
			{
				GetMemberFromStruct<atWideString>(structAddr).Reset();
			}
		}
		break;
#if !__FINAL
	case SUBTYPE_ATNONFINALHASHSTRING:
		{
			GetMemberFromStruct<atHashString>(structAddr) = name;
		}
		break;
#endif
	case SUBTYPE_ATFINALHASHSTRING:
		{
			GetMemberFromStruct<atFinalHashString>(structAddr) = name;
		}
		break;
	case SUBTYPE_ATHASHVALUE:
		{
			GetMemberFromStruct<atHashValue>(structAddr) = name;
		}
		break;
	case SUBTYPE_ATPARTIALHASHVALUE:
		{
			GetMemberFromStruct<u32>(structAddr) = atPartialStringHash(name);
		}
		break;
	case SUBTYPE_ATNSHASHSTRING:
		{
			u32 hash = atHashStringNamespaceSupport::ComputeHash(GetData()->GetNamespaceIndex(), name);
			atHashStringNamespaceSupport::AddString(GetData()->GetNamespaceIndex(), hash, name);
			GetMemberFromStruct<u32>(structAddr) = hash;
		}
		break;
	case SUBTYPE_ATNSHASHVALUE:
		{
			u32 hash = atHashStringNamespaceSupport::ComputeHash(GetData()->GetNamespaceIndex(), name);
			GetMemberFromStruct<u32>(structAddr) = hash;
		}
		break;
	default:
		parErrorf("Unknown subtype of parMemberString: %d. Name is '%s'.", GetSubtype(), name);
		break;
	}
}


size_t rage::parMemberString::GetSize() const
{
	using namespace parMemberStringSubType;
	switch(GetSubtype())
	{
	case SUBTYPE_POINTER: return sizeof(char*);
	case SUBTYPE_WIDE_POINTER: return sizeof(char16*);
	case SUBTYPE_MEMBER: return sizeof(char) * GetData()->m_Length;
	case SUBTYPE_WIDE_MEMBER: return sizeof(char16) * GetData()->m_Length;
	case SUBTYPE_CONST_STRING: return sizeof(ConstString);
	case SUBTYPE_ATSTRING: return sizeof(atString);
	case SUBTYPE_ATWIDESTRING: return sizeof(atWideString);
#if !__FINAL
	case SUBTYPE_ATNONFINALHASHSTRING: return sizeof(atHashString);
#endif
	case SUBTYPE_ATFINALHASHSTRING: return sizeof(atFinalHashString);
	case SUBTYPE_ATHASHVALUE: return sizeof(atHashValue);
	case SUBTYPE_ATPARTIALHASHVALUE: return sizeof(u32);
	case SUBTYPE_ATNSHASHSTRING: return sizeof(u32);
	case SUBTYPE_ATNSHASHVALUE: return sizeof(u32);
	default:
		parErrorf("Unknown subtype of parMemberString: %d. Name is '%s'.", GetSubtype(), GetName());
		break;
	}
	return 0;
}

size_t rage::parMemberString::FindAlign() const
{
	using namespace parMemberStringSubType;
	switch(GetSubtype())
	{
	case SUBTYPE_POINTER: 
	case SUBTYPE_CONST_STRING:
	case SUBTYPE_WIDE_POINTER: 
		return __alignof(char*);
	case SUBTYPE_MEMBER: return __alignof(char);
	case SUBTYPE_WIDE_MEMBER: return __alignof(char16);
	case SUBTYPE_ATSTRING: return __alignof(atString);
	case SUBTYPE_ATWIDESTRING: return __alignof(atWideString);
#if !__FINAL
	case SUBTYPE_ATNONFINALHASHSTRING: return __alignof(atHashString);
#endif
	case SUBTYPE_ATFINALHASHSTRING: return __alignof(atFinalHashString);
	case SUBTYPE_ATHASHVALUE: return __alignof(atHashValue);
	case SUBTYPE_ATPARTIALHASHVALUE: return __alignof(u32);
	case SUBTYPE_ATNSHASHSTRING: return __alignof(u32);
	case SUBTYPE_ATNSHASHVALUE: return __alignof(u32);
	default:
		parErrorf("Unknown subtype of parMemberString: %d. Name is '%s'.", GetSubtype(), GetName());
		break;
	}
	return 0;
}

const char* parMemberString::GetInitValue()
{
	parAttributeList*& attrs = GetExtraAttributes();
	if (!attrs)
	{
		return NULL;
	}
	return attrs->FindAttributeStringValue("initValue", NULL, NULL, 0);
}

atHashStringNamespaces parMemberString::GetNamespaceIndex() const
{
	if (parVerifyf(GetSubtype() == parMemberStringSubType::SUBTYPE_ATNSHASHSTRING ||
		GetSubtype() == parMemberStringSubType::SUBTYPE_ATNSHASHVALUE, "Can only get namespace index for namespaced hash strings"))
	{
		return GetData()->GetNamespaceIndex();
	}

	return HSNS_INVALID;
}
