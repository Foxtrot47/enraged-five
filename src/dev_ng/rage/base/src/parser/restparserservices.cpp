// 
// parser/restparserservices.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "restparserservices.h"

#include "manager.h"
#include "optimisations.h"
#include "visitorutils.h"
#include "visitorxml.h"
#include "visitorinit.h"

#include "data/base64.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/limits.h"
#include "file/stream.h"

PARSER_OPTIMISATIONS();

using namespace rage;

#if __BANK
//////////////////////////////////////////////////////////////////////////////////////////
// GUID lookup services
//////////////////////////////////////////////////////////////////////////////////////////

namespace parRestGuids
{

struct ObjectRef
{
	parStructure*	m_Type;
	parPtrToStructure	m_Object;
};

atMap<u32, ObjectRef>				g_ParserGuidToObjectMap;
atMap<parPtrToStructure, u32>		g_ParserObjectToGuidMap;
u32									g_NextGuidIndex = 1; // 0 will mean invalid
u32									g_SessionId;

u32 GetNextGuid()
{
	FastAssert(g_NextGuidIndex != (u32)-1); // Used up too many GUIDs!
	return ++g_NextGuidIndex;
}

u32 RegisterGuid(parPtrToStructure object, parStructure* type)
{
	// Do we already have a GUID for this object?
	u32* guid = g_ParserObjectToGuidMap.Access(object);
	if (guid)
	{
		return *guid;
	}

	u32 newGuid = GetNextGuid();

	g_ParserObjectToGuidMap[object] = newGuid;
	ObjectRef& newRef = g_ParserGuidToObjectMap[newGuid];
	newRef.m_Object = object;
	newRef.m_Type = type;

	return newGuid;
}

// buf must be at least 16 bytes!

u32 DecodeFullGuidName(const char* buf)
{
	u64 fullGuid;
	u32 destLen = 0;

	datBase64::Decode(buf, sizeof(u64), reinterpret_cast<u8*>(&fullGuid), &destLen);
	FastAssert(destLen == sizeof(u64)); // If it's not 8, something really bad happened!

	u32 theSesionId = fullGuid & 0xffffffff;
	u32 theLocalId = (u32)(fullGuid >> 32);

	if (theSesionId != g_SessionId)
	{
		// session IDs don't match, bail!
		return 0;
	}
	else
	{
		return theLocalId;
	}
}

ObjectRef* LookupObjectFromGuid(u32 guid)
{
	return g_ParserGuidToObjectMap.Access(guid);
}


} // namespace parRestGuids

parRestParserGuidService* parRestParserGuidService::sm_Instance = NULL;

void parRestParserGuidService::InitClass(u32 sessionId, const char* name /* =  */, const char* path /* = */ )
{
	parRestParserGuidService* instance = rage_new parRestParserGuidService(name);
	restServiceDirectory* dir = REST.CreateDirectories(path);
	if (!dir)
	{
		delete instance;
		parErrorf("Couldn't create GUID service with path %s", path);
		return;
	}
	dir->AddChild(*instance);
	sm_Instance = instance;
	SetSessionId(sessionId);
}

void parRestParserGuidService::ShutdownClass()
{
	delete sm_Instance;
	sm_Instance = NULL;

	// Expire all the GUIDs
	parRestGuids::g_ParserGuidToObjectMap.Reset();
	parRestGuids::g_ParserObjectToGuidMap.Reset();
}

parRestParserGuidService::parRestParserGuidService(const char* name)
: restService(name)
{
	parAssertf(!sm_Instance, "Only one Parser GUID service is allowed!");
}

parRestParserGuidService::~parRestParserGuidService()
{
}

void parRestParserGuidService::SetSessionId(u32 sessionId)
{
	parRestGuids::g_SessionId = sessionId;
}


u32 parRestParserGuidService::LookupGuidForObject(parPtrToStructure object)
{
	u32* guid = parRestGuids::g_ParserObjectToGuidMap.Access(object);
	return guid ? *guid : 0;
}

namespace rage
{
	bool parRestExpireGuid(parPtrToStructure object)
	{
		using namespace parRestGuids;

		u32* guidPtr = g_ParserObjectToGuidMap.Access(object);
		if (guidPtr)
		{
			u32 guid = *guidPtr;
			Assert(g_ParserGuidToObjectMap.Access(guid));
			g_ParserObjectToGuidMap.Delete(object);
			g_ParserGuidToObjectMap.Delete(guid);
			return true;
		}
		return false;
	}
}

restStatus::Enum parRestParserGuidService::ProcessCommand(restCommand& command)
{
	if (command.IsAtEndOfPath())
	{
		command.ReportErrorMethodNotAllowed(0, "Need to specify a GUID to do anything with the GUID service");
		return restStatus::REST_METHOD_NOT_ALLOWED;
	}

	const char* guidName = command.GetCurrentPathComponent();
	command.NextPathComponent();

	u32 localGuid = parRestGuids::DecodeFullGuidName(guidName);
	if (localGuid == 0)
	{
		command.ReportError("Invalid GUID - could be from an old session?");
		return restStatus::REST_NOT_FOUND;
	}

	parRestGuids::ObjectRef* obj = parRestGuids::LookupObjectFromGuid(localGuid);
	if (!obj)
	{
		command.ReportError("Invalid GUID - doesn't correspond to an existing object");
		return restStatus::REST_NOT_FOUND;
	}

	return parRestProcessParsableObject(obj->m_Object, obj->m_Type, command);
}

void parRestParserGuidService::MakeFullGuidName(u32 localId, char* buf)
{
	u64 fullGuid = (u64)localId << 32 | parRestGuids::g_SessionId;

	u32 bufLen;
	datBase64::Encode(reinterpret_cast<u8*>(&fullGuid), sizeof(u64), buf, 16, &bufLen);
	buf[bufLen] = '\0';
}

//////////////////////////////////////////////////////////////////////////////////////////
// Parsable object REST services:
//////////////////////////////////////////////////////////////////////////////////////////

void rage::parRestRegisterSingleton(const char* servicePath, parPtrToStructure object, parStructure* type, const char* sourceFile)
{
	atString pathStr(servicePath);
	atArray<atString> paths;
 	pathStr.Split(paths, '/', true);

	restServiceDirectory* parentDir = REST.CreateDirectories(paths, paths.GetCount() - 1);

	if (!parVerifyf(parentDir, "Couldn't create parent directories for path %s", servicePath))
	{
		return;
	}

	restService* leaf = parentDir->FindChild(paths.Top().c_str()); // See if we're just updating an existing service
	if (leaf)
	{
		parRestServiceSingleton* singletonSvc = dynamic_cast<parRestServiceSingleton*>(leaf);
		if (!parVerifyf(singletonSvc, "Named service %s already exists but it's not a parRestServiceSingleton", servicePath))
		{
			return;
		}
		parAssertf(singletonSvc->GetType() == type, "Can't change parser types!");
		singletonSvc->SetObject(object);
	}
	else
	{
		parentDir->AddChild(*(rage_new parRestServiceSingleton(paths.Top().c_str(), object, type, sourceFile)));
	}
}

#endif

restStatus::Enum ProcessParsableObject_GetXmlData(restCommand& command, parFindMemberData& findData)
{
	parSettings settings = PARSER.Settings();
	settings.SetFlag(parSettings::WRITE_XML_HEADER, false);

	if (findData.m_MemberType)
	{
		parXmlWriterVisitor xmlWriter(command.m_OutputStream, &settings);
		xmlWriter.VisitMember(findData.m_ContainingStructureAddress, *findData.m_MemberType);
	}
	else
	{
		parNeedsTreeForOutputVisitor needsTree;

		needsTree.VisitToplevelStructure(findData.m_ContainingStructureAddress, *findData.m_ContainingStructureType);
		if (needsTree.m_Result)
		{
			PARSER.SaveFromStructure(command.m_OutputStream, *findData.m_ContainingStructureType, findData.m_ContainingStructureAddress, parManager::XML, &settings);
		}
		else
		{
			parXmlWriterVisitor xmlWriter(command.m_OutputStream, &settings);
			xmlWriter.VisitToplevelStructure(findData.m_ContainingStructureAddress, *findData.m_ContainingStructureType);
		}
	}

	return restStatus::REST_OK;
}

restStatus::Enum ProcessParsableObject_SetStructFromXmlTree(restCommand& command, parFindMemberData& findData, parTree* tree)
{
	parMemberStruct* memberStruct = NULL;
	if(findData.m_MemberType) 
	{
		memberStruct = findData.m_MemberType->AsStructOrPointer();
	}

	if (!tree || !tree->GetRoot())
	{
		command.ReportError("Couldn't parse XML data");
		delete tree;
		return restStatus::REST_BAD_REQUEST;
	}

	parPtrToStructure addressOfDestStructure = NULL;
	parStructure* typeOfDestStructure = NULL;

	// If we were loading data into a member variable, maybe we have to create a new instance?
	if (memberStruct) 
	{
		if (memberStruct->GetSubtype() == parMemberStructSubType::SUBTYPE_POINTER)
		{
			// Check the root node. It should be in one of two formats:
			// <MyClassName>
			// <MemberName type="MyClassName">
			// If it's the former, convert to the latter so that we have a type attribute we can read
			// I hope that works...
			if (!tree->GetRoot()->GetElement().FindAttribute("type"))
			{
				tree->GetRoot()->GetElement().AddAttribute("type", tree->GetRoot()->GetElement().GetName(), false, false);
			}

			parPtrToMember addressOfPointer = findData.m_MemberAddress ? findData.m_MemberAddress : memberStruct->GetPointerToMember(findData.m_ContainingStructureAddress);
			parPtrToStructure pointedToObject = memberStruct->GetObjAddr(findData.m_ContainingStructureAddress);

			if (!addressOfPointer)
			{
				command.ReportError("Couldn't find the pointer that was supposed to be modified.");
				return restStatus::REST_BAD_REQUEST;
			}

			// Simple rule for pointers for now. You can UPDATE them, or CREATE them, but you can not change their types
			// or delete them with this method. To do that you need to DELETE them first and recreate them.
			if (pointedToObject == NULL)
			{
				parPtrToStructure newData = memberStruct->CreateNewData(tree->GetRoot(), 0x0, &typeOfDestStructure);
				memberStruct->SetObjAddr(findData.m_ContainingStructureAddress, newData);
				addressOfDestStructure = newData;
			}
			else
			{
				// Make sure the type is compatible with what is specified
				const char* treeType = tree->GetRoot()->GetElement().FindRequiredAttribute("type").GetStringValue();
				addressOfDestStructure = pointedToObject;
				typeOfDestStructure = memberStruct->GetConcreteStructure(addressOfDestStructure);
				if (!typeOfDestStructure->IsSubclassOf(treeType))
				{
#if PARSER_ALL_METADATA_HAS_NAMES
					char buf[256];
					formatf(buf, "Can't load data of type %s into an object of type %s - types are incompatible.", treeType, typeOfDestStructure->GetName());
					command.ReportError(buf);
#endif
					return restStatus::REST_BAD_REQUEST;
				}
			}
		}
		else
		{
			parPtrToStructure pointedToObject = memberStruct->GetObjAddr(findData.m_ContainingStructureAddress);
			addressOfDestStructure = pointedToObject;
			typeOfDestStructure = memberStruct->GetBaseStructure();
		}
	}
	else // Not loading data into a member variable - loading into the root structure
	{
		addressOfDestStructure = findData.m_ContainingStructureAddress;
		typeOfDestStructure = findData.m_ContainingStructureType;
	}

	bool status = PARSER.LoadFromStructure(tree->GetRoot(), *typeOfDestStructure, addressOfDestStructure, false);

	delete tree;

	if (!status)
	{
		command.ReportError("Problem parsing command data or loading it into a structure (check console for more info)");
		return restStatus::REST_BAD_REQUEST;
	}

	return restStatus::REST_OK_NO_CONTENT;
}

restStatus::Enum ProcessParsableObject_SetArrayFromXmlTree(restCommand& command, parFindMemberData& findData, parTree* tree)
{

	parMemberArray& memberArray = findData.m_MemberType->AsArrayRef();

	if (!tree || !tree->GetRoot())
	{
		command.ReportError("Couldn't parse XML data");
		delete tree;
		return restStatus::REST_BAD_REQUEST;
	}

	memberArray.ReadTreeNode(tree->GetRoot(), findData.m_ContainingStructureAddress);

	delete tree;

	return restStatus::REST_OK_NO_CONTENT;
}


restStatus::Enum ProcessParsableObject_SetFromFile(restCommand& command, parFindMemberData& findData)
{
	if (command.m_Data)
	{
		command.ReportError("A POST request with a 'src' value should not have additional data");
		return restStatus::REST_BAD_REQUEST;
	}

	parSettings s = PARSER.Settings();
	s.SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);

	parTree* tree = PARSER.LoadTree(command.m_Query.FindAttribute("src")->GetStringValue(), "", &s);

	if (!tree || !tree->GetRoot())
	{
		command.ReportError("Couldn't load XML data from file");
		delete tree;
		return restStatus::REST_BAD_REQUEST;
	}

	if (findData.m_MemberType && findData.m_MemberType->GetType() == parMemberType::TYPE_ARRAY)
	{
		return ProcessParsableObject_SetArrayFromXmlTree(command, findData, tree);
	}
	else
	{
		return ProcessParsableObject_SetStructFromXmlTree(command, findData, tree);
	}
}

restStatus::Enum ProcessParsableObject_DeleteArrayItem(restCommand& command, parFindMemberData& findData)
{
	//// deletes are only valid for array items... Lets see if we have one of those.
	//if (findData.m_MemberType->GetType() != parMemberType::TYPE_ARRAY)
	//{
	//	command.ReportError("Only array elements can be deleted");
	//	return restStatus::REST_BAD_REQUEST;
	//}

	parMemberArray& arrayMember = findData.m_MemberType->AsArrayRef();

	if (arrayMember.IsFixedSize())
	{
		command.ReportError("Can't delete from a fixed size array");
		return restStatus::REST_BAD_REQUEST;
	}

	int indexToDelete = command.m_Query.FindAttribute("delete")->FindIntValue();

	parConstPtrToArray dummyData;
	size_t count;
	arrayMember.GetArrayContentsAndCountFromStruct(findData.m_ContainingStructureAddress, dummyData, count);

	if (indexToDelete < 0 || (u32)indexToDelete >= count)
	{
		command.ReportError("Invalid index to delete");
		return restStatus::REST_NOT_FOUND;
	}

	bool success = arrayMember.DeleteItem(findData.m_ContainingStructureAddress, indexToDelete);
	if (!success)
	{
		command.ReportError("Can't delete from array");
		return restStatus::REST_INTERNAL_ERROR;
	}

	return restStatus::REST_OK_NO_CONTENT;
}

restStatus::Enum ProcessParsableObject_DeleteMapItem(restCommand& command, parFindMemberData& findData)
{
    parMemberMap& memberMap = findData.m_MemberType->AsMapRef();

    //Figure out if the data needs to be deleted too
    bool dataNeedsDelete = false;
    parMember* dataMember = memberMap.GetDataMember();
    if (dataMember->GetType() == parMemberType::TYPE_STRUCT)
    {
        parMemberStruct& structMember = dataMember->AsStructOrPointerRef();
        if (structMember.IsOwnerPointer() && structMember.IsParsableData())
        {
            dataNeedsDelete = true;
        }
    }

    restStatus::Enum status = restStatus::REST_OK_NO_CONTENT;
    parMember* keyMember = memberMap.GetKeyMember();
    parMemberMapInterface* mapInterface = memberMap.CreateInterface(findData.m_ContainingStructureAddress);
    switch (keyMember->GetType())
    {
        //These are the limited supported types for key values
    case parMemberType::TYPE_CHAR:
    case parMemberType::TYPE_UCHAR:
    case parMemberType::TYPE_SHORT:
    case parMemberType::TYPE_USHORT:
    case parMemberType::TYPE_INT:
    case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_PTRDIFFT:
	case parMemberType::TYPE_SIZET:	
	case parMemberType::TYPE_INT64:
	case parMemberType::TYPE_UINT64:
		{
            int key = command.m_Query.FindAttribute("delete")->FindIntValue();
            if (!mapInterface->Remove(&key, dataNeedsDelete))
            {
                status = restStatus::REST_NOT_FOUND;
            }
        }
        break;
	case parMemberType::TYPE_ENUM:
		{
			const char* keyStr = command.m_Query.FindAttribute("delete")->GetStringValue();
			parMemberEnum* keyEnumMember = keyMember->AsEnum();
			parEnumData::ValueFromNameStatus result;
			int keyValue = keyEnumMember->ValueFromName(keyStr, NULL, &result);
			if (result != parEnumData::VFN_ERROR && result != parEnumData::VFN_EMPTY)
			{
				if (!mapInterface->Remove(&keyValue, dataNeedsDelete))
				{
					status = restStatus::REST_NOT_FOUND;
				}
			}
			else
			{
				status = restStatus::REST_BAD_REQUEST;
			}
		}
		break;
    case parMemberType::TYPE_STRING:
        {
            parMemberString* strParMember = keyMember->AsString();
			const char* keyName = command.m_Query.FindAttribute("delete")->GetStringValue();
            switch (strParMember->GetSubtype())
            {
            case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
                {
                    atHashString key(keyName);
                    if (!mapInterface->Remove(&key, dataNeedsDelete))
                    {
                        status = restStatus::REST_NOT_FOUND;
                    }
                }
                break;
			case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
				{
					atFinalHashString key(keyName);
					if (!mapInterface->Remove(&key, dataNeedsDelete))
					{
						status = restStatus::REST_NOT_FOUND;
					}
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
				{
					atHashValue key(keyName);
					if (!mapInterface->Remove(&key, dataNeedsDelete))
					{
						status = restStatus::REST_NOT_FOUND;
					}
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
				{
					u32 key = atPartialStringHash(keyName);
					if (!mapInterface->Remove(&key, dataNeedsDelete))
					{
						status = restStatus::REST_NOT_FOUND;
					}
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
			case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
				{
					u32 key = atHashStringNamespaceSupport::ComputeHash(strParMember->GetData()->GetNamespaceIndex(), keyName);
					if (!mapInterface->Remove(&key, dataNeedsDelete))
					{
						status = restStatus::REST_NOT_FOUND;
					}
				}
            default:
                parAssertf(0,"Unsupported key string type %d being used for map %s. See https://devstar.rockstargames.com/wiki/index.php/Map_PSC_tag", strParMember->GetSubtype(), memberMap.GetName());
                status = restStatus::REST_BAD_REQUEST;
                break;
            };
        }
        break;
    default:
        parAssertf(0,"Unsupported key type %d being used for map %s. See https://devstar.rockstargames.com/wiki/index.php/Map_PSC_tag", keyMember->GetType(), memberMap.GetName());
        status = restStatus::REST_BAD_REQUEST;
        break;
    };
    delete mapInterface;

    return status;
}

restStatus::Enum ProcessParsableObject_DeletePointer(restCommand& /*command*/, parFindMemberData& findData)
{
	parMemberStruct& structMember = findData.m_MemberType->AsStructOrPointerRef();
	parAssertf(structMember.IsOwnerPointer(), "Only use this function for owner pointers (policy = owner or simple_owner)");

	parPtrToStructure ptr = structMember.GetObjAddr(findData.m_ContainingStructureAddress);
	if (ptr)
	{
		structMember.GetBaseStructure()->Destroy(ptr);
		structMember.SetObjAddr(findData.m_ContainingStructureAddress, NULL);
	}
	return restStatus::REST_OK_NO_CONTENT;
}

restStatus::Enum ProcessParsableObject_SetFromStringData(restCommand& command, parFindMemberData& findData)
{
	if (!command.m_Data)
	{
		command.ReportError("Looks like you are trying to set data, but no new data was provided");
		return restStatus::REST_BAD_REQUEST;
	}

	if (!findData.m_MemberType)
	{
		// Must be setting the whole top-level structure

		// Data should be XML - load it in.
		char memFileName[RAGE_MAX_PATH];
		fiDevice::MakeMemoryFileName(memFileName, RAGE_MAX_PATH, command.m_Data, strlen(command.m_Data), false, "REST Command Data");

		parSettings s = PARSER.Settings();
		s.SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);

		parTree* tree = PARSER.LoadTree(memFileName, "", &s);

		return ProcessParsableObject_SetStructFromXmlTree(command, findData, tree);
	}
	else
	{
		using namespace parMemberType;

		switch(parMemberType::GetClass(findData.m_MemberType->GetType()))
		{
			// parMemberSimple types
		case CLASS_SIMPLE:
			{
				parMemberSimple& memberSimple = findData.m_MemberType->AsSimpleRef();
				memberSimple.SetFromString(findData.m_ContainingStructureAddress, command.m_Data);
				return restStatus::REST_OK_NO_CONTENT;
			}
			// parMemberString types
		case	CLASS_STRING:
			{
				parMemberString& memberString = findData.m_MemberType->AsStringRef();
				memberString.SetFromString(findData.m_ContainingStructureAddress, command.m_Data);
				return restStatus::REST_OK_NO_CONTENT;
			}
		case	CLASS_STRUCT:
			{
				parMemberStruct& memberStruct = findData.m_MemberType->AsStructOrPointerRef();
				if (memberStruct.IsOwner())
				{
					// Data should be XML - load it in.
					char memFileName[RAGE_MAX_PATH];
					fiDevice::MakeMemoryFileName(memFileName, RAGE_MAX_PATH, command.m_Data, strlen(command.m_Data), false, "REST Command Data");

					parSettings s = PARSER.Settings();
					s.SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);

					parTree* tree = PARSER.LoadTree(memFileName, "", &s);

					return ProcessParsableObject_SetStructFromXmlTree(command, findData, tree);
				}
				else
				{
					memberStruct.SetFromString(findData.m_ContainingStructureAddress, command.m_Data);
				}
				return restStatus::REST_OK_NO_CONTENT;
			}
			// parMemberEnum types
		case    CLASS_ENUM: 
			{ 
				parMemberEnum& memberEnum = findData.m_MemberType->AsEnumRef(); 
				memberEnum.SetFromString(findData.m_ContainingStructureAddress, command.m_Data); 
				return restStatus::REST_OK_NO_CONTENT; 
			}
		case    CLASS_BITSET: 
			{ 
				parMemberBitset& memberBitset = findData.m_MemberType->AsBitsetRef(); 
				memberBitset.ClearBits(findData.m_ContainingStructureAddress); 
				memberBitset.SetFromString(findData.m_ContainingStructureAddress, command.m_Data); 
				return restStatus::REST_OK_NO_CONTENT; 
			} 
			// parMemberArray types
		case	CLASS_ARRAY:
			{
				// Data should be XML - load it in.
				char memFileName[RAGE_MAX_PATH];
				fiDevice::MakeMemoryFileName(memFileName, RAGE_MAX_PATH, command.m_Data, strlen(command.m_Data), false, "REST Command Data");

				parSettings s = PARSER.Settings();
				s.SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);

				parTree* tree = PARSER.LoadTree(memFileName, "", &s);

				return ProcessParsableObject_SetArrayFromXmlTree(command, findData, tree);
			}
			// parMemberVector types
		case CLASS_VECTOR:
		case CLASS_MATRIX:
			{
				command.ReportErrorMethodNotAllowed(restCommand::REST_GET);
				return restStatus::REST_METHOD_NOT_ALLOWED;
			}
		case	INVALID_CLASS:
		default:
			{
				command.ReportError("Bad member object type");
				return restStatus::REST_INTERNAL_ERROR;
			}
		}
	}
}


restStatus::Enum ProcessParsableObject_InsertArrayItem(restCommand& command, parFindMemberData& findData)
{
	//// inserts are only valid for array items... Lets see if we have one of those
	//if (findData.m_MemberType->GetType() != parMemberType::TYPE_ARRAY)
	//{
	//	command.ReportError("Only array elements can be deleted");
	//	return restStatus::REST_BAD_REQUEST;
	//}

	parMemberArray& arrayMember = findData.m_MemberType->AsArrayRef();

	if (arrayMember.IsFixedSize())
	{
		command.ReportError("Can't insert into a fixed size array");
		return restStatus::REST_BAD_REQUEST;
	}

	parAttribute* attr = command.m_Query.FindAttribute("insert");
	size_t indexToInsert = parMemberArray::INSERT_AT_END;
	if (attr) // check for missing value => insert at end.
	{
		if (attr->GetType() == parAttribute::STRING && attr->GetStringValue() == NULL)
		{
			indexToInsert = parMemberArray::INSERT_AT_END;
		}
		else
		{
			indexToInsert = attr->FindIntValue();
		}
	}

	bool success = arrayMember.InsertItem(findData.m_ContainingStructureAddress, indexToInsert, true);
	if (!success)
	{
		command.ReportError("Can't insert into array");
		return restStatus::REST_INTERNAL_ERROR;
	}

	parPtrToArray outAddr;
	size_t count;
	arrayMember.GetArrayContentsAndCountFromStruct(findData.m_ContainingStructureAddress, outAddr, count);
	if (indexToInsert == parMemberArray::INSERT_AT_END) 
	{
		indexToInsert = count-1;
	}

	parFindMemberData arrayEltFindData;

	arrayEltFindData.m_ContainingStructureType = findData.m_ContainingStructureType;
	arrayEltFindData.m_ContainingStructureAddress = arrayMember.GetAddressOfNthElement(outAddr, indexToInsert);
	arrayEltFindData.m_MemberType = arrayMember.GetPrototypeMember();
	arrayEltFindData.m_MemberAddress = NULL;


	// Add the new location for the created data to the output header
	fprintf(command.m_OutputStream, "Location: ");
	for(int i = 0; i < command.m_UriComponents.GetCount(); i++)
	{
		fprintf(command.m_OutputStream, "%s/", command.m_UriComponents[i].c_str());
	}
	fprintf(command.m_OutputStream, "%d", indexToInsert);

	restStatus::Enum status;
	if (command.m_Query.FindAttribute("src"))
	{
		status = ProcessParsableObject_SetFromFile(command, arrayEltFindData);
	}
	else {
		status = ProcessParsableObject_SetFromStringData(command, arrayEltFindData);
	}

	if (restStatus::IsOk(status))
	{
		return restStatus::REST_CREATED;
	}
	else 
	{
		// Delete the item we just created
		arrayMember.DeleteItem(findData.m_ContainingStructureAddress, indexToInsert);
		return status;
	}
}

restStatus::Enum ProcessParsableObject_InsertMapItem(restCommand& command, parFindMemberData& findData)
{
    parMemberMap& memberMap = findData.m_MemberType->AsMapRef();

    parMember* keyMember = memberMap.GetKeyMember();
    parMemberMapInterface* mapInterface = memberMap.CreateInterface(findData.m_ContainingStructureAddress);
    parPtrToStructure dataValueAddr = NULL;
    restStatus::Enum status = restStatus::REST_CREATED;
    switch (keyMember->GetType())
    {
        //These are the limited supported types for key values
    case parMemberType::TYPE_CHAR:
    case parMemberType::TYPE_UCHAR:
    case parMemberType::TYPE_SHORT:
    case parMemberType::TYPE_USHORT:
    case parMemberType::TYPE_INT:
    case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_PTRDIFFT:
	case parMemberType::TYPE_SIZET:
	case parMemberType::TYPE_INT64:
	case parMemberType::TYPE_UINT64:
        {
            int key = command.m_Query.FindAttribute("insert")->FindIntValue();
            if (mapInterface->GetDataPtr(&key))
            {
                parErrorf("Key [%d] is already in the map %s", key, memberMap.GetName());
                status = restStatus::REST_FORBIDDEN;
            }
            else
            {
                dataValueAddr = mapInterface->InsertKey(&key);
            }
        }
        break;
	case parMemberType::TYPE_ENUM:
		{
			const char* enumStr = command.m_Query.FindAttribute("insert")->GetStringValue();
			parMemberEnum& keyEnumMember = keyMember->AsEnumRef();
			parEnumData::ValueFromNameStatus result;
			int enumVal = keyEnumMember.ValueFromName(enumStr, NULL, &result);
			if (result != parEnumData::VFN_ERROR && result != parEnumData::VFN_EMPTY)
			{
				if (mapInterface->GetDataPtr(&enumVal))
				{
					parErrorf("Key [\"%s\" (%d)] is already in the map %s", enumStr, enumVal, memberMap.GetName());
					status = restStatus::REST_FORBIDDEN;
				}
				else
				{
					dataValueAddr = mapInterface->InsertKey(&enumVal);
				}
			}
		}
    case parMemberType::TYPE_STRING:
        {
            parMemberString& strParMember = keyMember->AsStringRef();
			const char* keyName = command.m_Query.FindAttribute("insert")->GetStringValue();
            switch (strParMember.GetSubtype())
            {
            case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
				{
					atHashString key(keyName);
					if (mapInterface->GetDataPtr(&key))
					{
						parErrorf("Key [%s] is already in the map %s", keyName, memberMap.GetName());
						status = restStatus::REST_FORBIDDEN;
					}
					else
					{
						dataValueAddr = mapInterface->InsertKey(&key);
					}
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
				{
					atFinalHashString key(keyName);
					if (mapInterface->GetDataPtr(&key))
					{
						parErrorf("Key [%s] is already in the map %s", keyName, memberMap.GetName());
						status = restStatus::REST_FORBIDDEN;
					}
					else
					{
						dataValueAddr = mapInterface->InsertKey(&key);
					}
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
                {
                    atHashValue key(keyName);
                    if (mapInterface->GetDataPtr(&key))
                    {
                        parErrorf("Key [%s] is already in the map %s", keyName, memberMap.GetName());
                        status = restStatus::REST_FORBIDDEN;
                    }
                    else
                    {
                        dataValueAddr = mapInterface->InsertKey(&key);
                    }
                }
                break;
			case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
				{
					u32 key = atPartialStringHash(keyName);
					if (mapInterface->GetDataPtr(&key))
					{
						parErrorf("Key [%s] is already in the map %s", keyName, memberMap.GetName());
						status = restStatus::REST_FORBIDDEN;
					}
					else
					{
						dataValueAddr = mapInterface->InsertKey(&key);
					}
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
				{
					u32 key = atHashStringNamespaceSupport::ComputeHash(strParMember.GetData()->GetNamespaceIndex(), keyName);
					atHashStringNamespaceSupport::AddString(strParMember.GetData()->GetNamespaceIndex(), key, keyName);
					if (mapInterface->GetDataPtr(&key))
					{
						parErrorf("Key [%s] is already in the map %s", keyName, memberMap.GetName());
						status = restStatus::REST_FORBIDDEN;
					}
					else
					{
						dataValueAddr = mapInterface->InsertKey(&key);
					}
				}
				break;
			case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
				{
					u32 key = atHashStringNamespaceSupport::ComputeHash(strParMember.GetData()->GetNamespaceIndex(), command.m_Query.FindAttribute("insert")->GetStringValue());
					if (mapInterface->GetDataPtr(&key))
					{
						parErrorf("Key [%s] is already in the map %s", keyName, memberMap.GetName());
						status = restStatus::REST_FORBIDDEN;
					}
					else
					{
						dataValueAddr = mapInterface->InsertKey(&key);
					}
				}
				break;
            default:
                parAssertf(0,"Unsupported key string type %d being used for map %s. See https://devstar.rockstargames.com/wiki/index.php/Map_PSC_tag", strParMember.GetSubtype(), memberMap.GetName());
                status = restStatus::REST_BAD_REQUEST;
                break;
            };
        }
        break;
    default:
        parAssertf(0,"Unsupported key type %d being used for map %s. See https://devstar.rockstargames.com/wiki/index.php/Map_PSC_tag", keyMember->GetType(), memberMap.GetName());
        status = restStatus::REST_BAD_REQUEST;
        break;
    };

    if (dataValueAddr)
    {
        parMember* dataMember = memberMap.GetDataMember();

        //Init the value of the data
        parInitVisitor initter;
        initter.VisitMember(dataValueAddr, *dataMember);

        // Now set the new value... Change findData to point to the new member
        findData.m_MemberType = dataMember;
        findData.m_MemberAddress = NULL;
		findData.m_ContainingStructureAddress = dataValueAddr;

        if (command.m_Query.FindAttribute("src"))
        {
            status = ProcessParsableObject_SetFromFile(command, findData);
        }
        else {
            status = ProcessParsableObject_SetFromStringData(command, findData);
        }
    }

	mapInterface->PostInsert();
	delete mapInterface;

    if (restStatus::IsOk(status))
    {
        return restStatus::REST_CREATED;
    }
    else 
    {
        return status;
    }
}

restStatus::Enum rage::parRestProcessParsableObject(parPtrToStructure object, parStructure* type, restCommand& command)
{
	parFindMemberData findData;

	// Reassemble the URI path from the path components
	atString uriPath;
	for(int i = command.m_UriIndex; i < command.m_UriComponents.GetCount(); i++)
	{
		uriPath += command.m_UriComponents[i];
		if (i < command.m_UriComponents.GetCount()-1)
		{
			uriPath += "/";
		}
	}

	bool found = parFindMemberFromUriPath(object, *type, uriPath.c_str(), findData);

	if (!found)
	{
		return restStatus::REST_NOT_FOUND;
	}

	bool canGet = true;
	bool canPut = true;
	bool canPost = ( // only valid for array or map members
                        (findData.m_MemberType && (findData.m_MemberType->GetType() == parMemberType::TYPE_ARRAY)) ||
                        (findData.m_MemberType && (findData.m_MemberType->GetType() == parMemberType::TYPE_MAP))
                   );
	bool canDelete = false;

	parMemberStruct* memberStruct = NULL;
	if (findData.m_MemberType && (findData.m_MemberType->GetType() == parMemberType::TYPE_STRUCT))
	{
		memberStruct = findData.m_MemberType->AsStructOrPointer();
		if (memberStruct && memberStruct->IsOwnerPointer())
		{
			canDelete = true;
		}
	}

	// Figure out what to do
	switch (command.m_Verb)
	{
	case restCommand::REST_GET:
		if (canGet) {
#if __BANK
			if (command.m_Query.FindAttribute("guid"))
			{
				if (findData.m_MemberType == NULL && findData.m_ContainingStructureType != NULL)
				{
					parFatalAssertf(parRestParserGuidService::GetInstance(), "GUID service has not been initialized!");

					u32 localGuid = parRestGuids::RegisterGuid(findData.m_ContainingStructureAddress, findData.m_ContainingStructureType);
					char guidName[32];
					parRestParserGuidService::MakeFullGuidName(localGuid, guidName);
					parRestParserGuidService* guidSvc = parRestParserGuidService::GetInstance();
					fprintf(command.m_OutputStream, "Location: ");
					guidSvc->PrintFullName(command.m_OutputStream); // Normally prints "/ParGUID/"
					fprintf(command.m_OutputStream, "%s", guidName);
					return restStatus::REST_OK;
				}
				else
				{
					command.ReportError("Can only get GUIDs for top level parsable objects");
					return restStatus::REST_BAD_REQUEST;
				}
			}
			else
#endif
			{
#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
				if (command.m_Query.FindAttribute("schema"))
				{
					parFindMemberData metadata;
					if (findData.m_MemberType)
					{
						// return the parMember description
						metadata.m_ContainingStructureAddress = findData.m_MemberType->parser_GetPointer();
						metadata.m_ContainingStructureType = findData.m_MemberType->parser_GetStructure();
					}
					else
					{
						// return the parStructure description
						metadata.m_ContainingStructureAddress = findData.m_ContainingStructureType->parser_GetPointer();
						metadata.m_ContainingStructureType = findData.m_ContainingStructureType->parser_GetStructure();
					}
					metadata.m_MemberAddress = NULL;
					metadata.m_MemberType = NULL;

					return ProcessParsableObject_GetXmlData(command, metadata);
				}
#endif // PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
			}

			return ProcessParsableObject_GetXmlData(command, findData);
		}
		break;
	case restCommand::REST_PUT:
		if (canPut) {
			if (command.m_Query.FindAttribute("src"))
			{
				return ProcessParsableObject_SetFromFile(command, findData);
			}
			else {
				return ProcessParsableObject_SetFromStringData(command, findData);
			}
		}
		break;
	case restCommand::REST_POST:
		if (canPost) {
			// OK we support POST
			if (command.m_Query.FindAttribute("delete"))
			{
                if (findData.m_MemberType && (findData.m_MemberType->GetType() == parMemberType::TYPE_ARRAY))
                {
				    return ProcessParsableObject_DeleteArrayItem(command, findData);
                }
                else if (findData.m_MemberType && (findData.m_MemberType->GetType() == parMemberType::TYPE_MAP))
                {
                    return ProcessParsableObject_DeleteMapItem(command, findData);
                }
			}
			else
			{
				if (findData.m_MemberType && (findData.m_MemberType->GetType() == parMemberType::TYPE_ARRAY))
                {
                    if (!command.m_Query.FindAttribute("insert"))
                    {
                        command.m_Query.AddAttribute("insert", -1, false); // insert=-1 means append
                    }
                    return ProcessParsableObject_InsertArrayItem(command, findData);
                }
                else if (findData.m_MemberType && (findData.m_MemberType->GetType() == parMemberType::TYPE_MAP))
                {
                    return ProcessParsableObject_InsertMapItem(command, findData);
                }
			}
			// if we get here, we can't really POST to this object
			canPost = false;
		}
		break;
	case restCommand::REST_DELETE:
		if (canDelete)
		{
			return ProcessParsableObject_DeletePointer(command, findData);
		}
		break;
	default:
		break;
	}
	
	// Didn't return early - method must not be allowed
	command.ReportErrorMethodNotAllowed(
		(canGet ? restCommand::REST_GET : 0) | 
		(canPost ? restCommand::REST_POST : 0) | 
		(canPut ? restCommand::REST_PUT : 0) |
		(canDelete ? restCommand::REST_DELETE : 0)
		);
	return restStatus::REST_METHOD_NOT_ALLOWED;
}

#if __BANK

parRestServiceSingleton::parRestServiceSingleton( const char* name, parPtrToStructure object, parStructure* type, const char* sourceFile )
: restService(name)
, m_Type(type)
, m_SourceFile(sourceFile)
, m_Object(object)
{
	parAssertf(m_Type, "Type is required!");
}


rage::parRestServiceSingleton::~parRestServiceSingleton()
{
	parRestExpireGuid(m_Object);
}


restStatus::Enum parRestServiceSingleton::ProcessCommand( restCommand& command )
{
	return parRestProcessParsableObject(m_Object, m_Type, command);
}

void parRestServiceSingleton::PrintExtraDirInfo(fiStream* stream)
{
	fprintf(stream, "sourceFile=\"%s\"", m_SourceFile.c_str());
}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
restStatus::Enum parRestParserSchemaService::ProcessCommand(restCommand& command)
{
	if (command.IsAtEndOfPath())
	{
		// Print out a list of all known schemas

		fiStream* out = command.m_OutputStream;
		fprintf(out, "<Schemas><Structures>\n");

		for(parManager::StructureMap::ConstIterator it = PARSER.GetStructureMap().CreateIterator(); !it.AtEnd(); it.Next())
		{
			parStructure* structure = it.GetData();
			fprintf(out, "  <Item><Name>%s</Name><Size value=\"%d\"/>", structure->GetName(), structure->GetSize());
			if (structure->GetBaseStructure())
			{
				fprintf(out, "<Base>%s</Base>", structure->GetBaseStructure()->GetName());
			}
			fprintf(out, "</Item>\n");
		}

		fprintf(out, "</Structures></Schemas>\n");

		return restStatus::REST_OK;
	}

	const char* qName = command.GetCurrentPathComponent();
	command.NextPathComponent();
	if (!command.IsAtEndOfPath())
	{
		return restStatus::REST_NOT_FOUND;
	}

	parStructure* structure = PARSER.FindStructure(qName);

	if (!structure)
	{
		return restStatus::REST_NOT_FOUND;
	}

	parFindMemberData findData;
	findData.m_ContainingStructureAddress = structure->parser_GetPointer();
	findData.m_ContainingStructureType = structure->parser_GetStructure();

	return ProcessParsableObject_GetXmlData(command, findData);
}
#endif // PARSER_USES_EXTERNAL_STRUCTURE_DEFNS


#endif // __BANK
