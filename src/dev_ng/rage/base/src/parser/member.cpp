// 
// parser/member.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "member.h"

#include "manager.h"
#include "optimisations.h"

#include "math/float16.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/boolv.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"
#include "vectormath/scalarv.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"

PARSER_OPTIMISATIONS();

using namespace rage;


#if !__FINAL 
namespace rage
{
	struct parMemberDebugHashContainer { atLiteralHashString m_Name; };
}
#endif

rage::parMemberType::Traits rage::parMemberType::g_Traits[] = {
	{sizeof(bool),		__alignof(bool),		parMemberType::CLASS_SIMPLE},	// TYPE_BOOL
	{sizeof(char),		__alignof(char),		parMemberType::CLASS_SIMPLE},	// TYPE_CHAR
	{sizeof(u8),		__alignof(u8),			parMemberType::CLASS_SIMPLE},	//TYPE_UCHAR,	
	{sizeof(s16),		__alignof(s16),			parMemberType::CLASS_SIMPLE},	//TYPE_SHORT,	
	{sizeof(u16),		__alignof(u16),			parMemberType::CLASS_SIMPLE},	//TYPE_USHORT,	
	{sizeof(int),		__alignof(int),			parMemberType::CLASS_SIMPLE},	//TYPE_INT,		
	{sizeof(u32),		__alignof(u32),			parMemberType::CLASS_SIMPLE},	//TYPE_UINT,	
	{sizeof(float),		__alignof(float),		parMemberType::CLASS_SIMPLE},	//TYPE_FLOAT,	
	{sizeof(Vector2),	__alignof(Vector2),		parMemberType::CLASS_VECTOR},	//TYPE_VECTOR2,	
	{sizeof(Vector3),	__alignof(Vector3),		parMemberType::CLASS_VECTOR},	//TYPE_VECTOR3,	
	{sizeof(Vector4),	__alignof(Vector4),		parMemberType::CLASS_VECTOR},	//TYPE_VECTOR4,	
	{0,					0,						parMemberType::CLASS_STRING},	//TYPE_STRING,	
	{0,					0,						parMemberType::CLASS_STRUCT},	//TYPE_STRUCT,	
	{0,					0,						parMemberType::CLASS_ARRAY},	//TYPE_ARRAY,	
	{0,					0,						parMemberType::CLASS_ENUM},	//TYPE_ENUM,	
	{0,					0,						parMemberType::CLASS_BITSET},	//TYPE_BITSET,	
	{0,					0,						parMemberType::CLASS_MAP},		//TYPE_MAP,
	{sizeof(Matrix34),	__alignof(Matrix34),	parMemberType::CLASS_MATRIX},	//TYPE_MATRIX34,
	{sizeof(Matrix44),	__alignof(Matrix44),	parMemberType::CLASS_MATRIX},	//TYPE_MATRIX44,
	{sizeof(Vec2V),		__alignof(Vec2V),		parMemberType::CLASS_VECTOR},	//TYPE_VEC2V,	
	{sizeof(Vec3V),		__alignof(Vec3V),		parMemberType::CLASS_VECTOR},	//TYPE_VEC3V,	
	{sizeof(Vec4V),		__alignof(Vec4V),		parMemberType::CLASS_VECTOR},	//TYPE_VEC4V,	
	{sizeof(Mat33V),	__alignof(Mat33V),		parMemberType::CLASS_MATRIX},	//TYPE_MAT33V,	
	{sizeof(Mat34V),	__alignof(Mat34V),		parMemberType::CLASS_MATRIX},	//TYPE_MAT34V,	
	{sizeof(Mat44V),	__alignof(Mat44V),		parMemberType::CLASS_MATRIX},	//TYPE_MAT44V,	
	{sizeof(ScalarV),	__alignof(ScalarV),		parMemberType::CLASS_SIMPLE},	//TYPE_SCALARV,	
	{sizeof(BoolV),		__alignof(BoolV),		parMemberType::CLASS_SIMPLE},	//TYPE_BOOLV,	
	{sizeof(VecBoolV),	__alignof(VecBoolV),	parMemberType::CLASS_VECTOR},	//TYPE_VECBOOLV,
	{sizeof(ptrdiff_t),	__alignof(ptrdiff_t),	parMemberType::CLASS_SIMPLE},	//TYPE_PTRDIFFT
	{sizeof(size_t),	__alignof(size_t),		parMemberType::CLASS_SIMPLE},	//TYPE_SIZET
	{sizeof(Float16),	__alignof(Float16),		parMemberType::CLASS_SIMPLE},	//TYPE_FLOAT16
	{sizeof(s64),		__alignof(s64),			parMemberType::CLASS_SIMPLE},  //TYPE_INT64
	{sizeof(u64),		__alignof(u64),			parMemberType::CLASS_SIMPLE},	//TYPE_UINT64
	{sizeof(double),	__alignof(double),		parMemberType::CLASS_SIMPLE},	//TYPE_DOUBLE
};


// Subclasses should delete any subclass-specific data first.
parMember::~parMember()
{
	delete GetExtraAttributes();
	if (!IsStatic()) { 
		delete reinterpret_cast<CommonData*>(m_Data);
	}
}

void parMember::LoadExtraAttributes(parTreeNode* node)
{
	parTreeNode* attrNode = node->FindChildWithName("Attributes");
	parAttributeList* attrs = NULL;
	PARSER.LoadObjectPtr(attrNode, attrs);

#if !__FINAL
	// This is pointless, but apparently necessary to get debug info for the debugname class
	// (otherwise the linker strips it), and we hardly ever call this function
	if (m_DebugName == (void*)0x1)
	{
		parDebugf3("Loading extra attributes for %s", m_DebugName->m_Name.GetCStr());
	}
#endif

	if (attrs)
	{
		if (GetExtraAttributes())
		{
			GetExtraAttributes()->MergeFrom(*attrs);
		}
		else
		{
			GetExtraAttributes() = rage_new parAttributeList;
			GetExtraAttributes()->CopyFrom(*attrs);
		}
	}
	
}

void parMember::InitExtraAttributes()
{
	if (!GetExtraAttributes())
	{
		GetExtraAttributes() = rage_new parAttributeList;
	}
}


#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
void parMember::PostSave(parTreeNode* node)
{
	node->AppendStdLeafChild("ComputedSize", (s64)GetSize());
	node->AppendStdLeafChild("ComputedAlign", (s64)FindAlign());
}
#endif
