// 
// parser/visitorwidgets.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "visitorwidgets.h"

#include "optimisations.h"

#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

PARSER_OPTIMISATIONS();

#if __BANK && PARSER_ALL_METADATA_HAS_NAMES

#include "bank/bank.h"

using namespace rage;

void parBuildWidgetsVisitor::VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
{
	metadata.Invoke<bkBank&>(dataPtr, "PreAddWidget", *m_CurrBank);
	parInstanceVisitor::VisitStructure(dataPtr, metadata);
	metadata.Invoke<bkBank&>(dataPtr, "PostAddWidget", *m_CurrBank);
}

void parBuildWidgetsVisitor::SimpleMember			(parPtrToMember ptrToMember,	parMemberSimple& metadata)
{
	parMemberSimple::Data* metadatadata = metadata.GetData();

	datCallback cb = metadatadata->m_WidgetCb ? *metadatadata->m_WidgetCb : NullCallback;
	cb.SetBase(*reinterpret_cast<datBase*>(m_ContainingStructureAddress));

	switch(metadata.GetType())
	{
	case parMemberType::TYPE_BOOL:	m_CurrBank->AddToggle(metadata.GetName(), reinterpret_cast<bool*>(ptrToMember), cb, metadatadata->m_Description); break;
	case parMemberType::TYPE_CHAR:	m_CurrBank->AddSlider(metadata.GetName(), reinterpret_cast<s8*>(ptrToMember), (s8)metadatadata->m_Min, (s8)metadatadata->m_Max, (s8)metadatadata->m_Step, cb, metadatadata->m_Description); break;
	case parMemberType::TYPE_UCHAR:	m_CurrBank->AddSlider(metadata.GetName(), reinterpret_cast<u8*>(ptrToMember), (u8)metadatadata->m_Min, (u8)metadatadata->m_Max, (u8)metadatadata->m_Step, cb, metadatadata->m_Description); break;
	case parMemberType::TYPE_SHORT:	m_CurrBank->AddSlider(metadata.GetName(), reinterpret_cast<s16*>(ptrToMember), (s16)metadatadata->m_Min, (s16)metadatadata->m_Max, (s16)metadatadata->m_Step, cb, metadatadata->m_Description); break;
	case parMemberType::TYPE_USHORT:	m_CurrBank->AddSlider(metadata.GetName(), reinterpret_cast<u16*>(ptrToMember), (u16)metadatadata->m_Min, (u16)metadatadata->m_Max, (u16)metadatadata->m_Step, cb, metadatadata->m_Description); break;
	case parMemberType::TYPE_INT:	m_CurrBank->AddSlider(metadata.GetName(), reinterpret_cast<s32*>(ptrToMember), (s32)metadatadata->m_Min, (s32)metadatadata->m_Max, (s32)metadatadata->m_Step, cb, metadatadata->m_Description); break;
	case parMemberType::TYPE_FLOAT16:	m_CurrBank->AddSlider(metadata.GetName(), reinterpret_cast<Float16*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description); break;
	case parMemberType::TYPE_UINT:
		if (metadata.GetSubtype() == parMemberSimpleSubType::SUBTYPE_COLOR)
		{
			m_CurrBank->AddColor(metadata.GetName(), reinterpret_cast<Color32*>(ptrToMember), cb, metadatadata->m_Description);
		}
		else
		{
			// work around problems with converting UINT_MAX from u32 to float to u32 (it could end up becoming 0)
			float max = Min<float>(metadatadata->m_Max, (float)(UINT_MAX - 10000));
			m_CurrBank->AddSlider(metadata.GetName(), reinterpret_cast<u32*>(ptrToMember), (u32)metadatadata->m_Min, (u32)max, (u32)metadatadata->m_Step, cb, metadatadata->m_Description);
		}
		break;
	case parMemberType::TYPE_FLOAT:
		if (metadata.GetSubtype() == parMemberSimpleSubType::SUBTYPE_ANGLE)
		{
			m_CurrBank->AddAngle(metadata.GetName(), reinterpret_cast<float*>(ptrToMember), bkAngleType::RADIANS, cb, metadatadata->m_Description);
		}
		else
		{
			m_CurrBank->AddSlider(metadata.GetName(), reinterpret_cast<float*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description);
		}
		break;

	case parMemberType::TYPE_SCALARV:
	case parMemberType::TYPE_BOOLV:
	case parMemberType::TYPE_INT64:
	case parMemberType::TYPE_UINT64:
	case parMemberType::TYPE_DOUBLE:
		parErrorf("Can't create widgets for type %d yet (member %s)", metadata.GetType(), metadata.GetName());
		break;
	default:
		{
			Quitf("Unknown type (%d) for simple member", metadata.GetType());
		}
	}
}

void parBuildWidgetsVisitor::VectorMember(parPtrToMember ptrToMember,	parMemberVector& metadata)
{
	parMemberVector::Data* metadatadata = metadata.GetData();

	datCallback cb = metadatadata->m_WidgetCb ? *metadatadata->m_WidgetCb : NullCallback;
	cb.SetBase(*reinterpret_cast<datBase*>(m_ContainingStructureAddress));

	switch(metadata.GetType())
	{
	case parMemberType::TYPE_VECTOR2:
	case parMemberType::TYPE_VEC2V: // this is a cheat - cast as a Vector2 until we get vectormath support in bank
		m_CurrBank->AddVector(metadata.GetName(), reinterpret_cast<Vector2*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description); 
		break;
	case parMemberType::TYPE_VECTOR3:	
	case parMemberType::TYPE_VEC3V: // this is a cheat - cast as a Vector3 until we get vectormath support in bank
		if (metadata.GetSubtype() == parMemberVectorSubType::SUBTYPE_COLOR)
		{
			m_CurrBank->AddColor(metadata.GetName(), vector_cast<Vector3*>(ptrToMember), cb, metadatadata->m_Description);
		}
		else
		{
			m_CurrBank->AddVector(metadata.GetName(), vector_cast<Vector3*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description);
		}
		break;
	case parMemberType::TYPE_VECTOR4:
	case parMemberType::TYPE_VEC4V: // this is a cheat - cast as a Vector4 until we get vectormath support in bank
		if (metadata.GetSubtype() == parMemberVectorSubType::SUBTYPE_COLOR) 
		{
			m_CurrBank->AddColor(metadata.GetName(), vector_cast<Vector4*>(ptrToMember), cb, metadatadata->m_Description);
		}
		else 
		{
			m_CurrBank->AddVector(metadata.GetName(), vector_cast<Vector4*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description); 
		}
		break;
	case parMemberType::TYPE_VECBOOLV:
		parErrorf("Can't create widgets for VecBoolV member %s", metadata.GetName());
		break;
	default:
		{
			Quitf("Invalid type for vector member");
		}
	}
}

void parBuildWidgetsVisitor::MatrixMember(parPtrToMember ptrToMember, parMemberMatrix& metadata)
{
	parMemberMatrix::Data* metadatadata = metadata.GetData();

	datCallback cb = metadatadata->m_WidgetCb ? *metadatadata->m_WidgetCb : NullCallback;
	cb.SetBase(*reinterpret_cast<datBase*>(m_ContainingStructureAddress));

	switch ( metadata.GetType() )
	{
	case parMemberType::TYPE_MAT33V:
		parErrorf("Can't create proper widgets for new-style matrices. Using old style widgets instead - matrices will appear transposed!");
		m_CurrBank->AddMatrix(metadata.GetName(), reinterpret_cast<Matrix33*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description); 
		break;
	case parMemberType::TYPE_MAT34V:
		parErrorf("Can't create proper widgets for new-style matrices. Using old style widgets instead - matrices will appear transposed!");
		m_CurrBank->AddMatrix(metadata.GetName(), reinterpret_cast<Matrix34*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description); 
		break;
	case parMemberType::TYPE_MAT44V:
		parErrorf("Can't create proper widgets for new-style matrices. Using old style widgets instead - matrices will appear transposed!");
		m_CurrBank->AddMatrix(metadata.GetName(), reinterpret_cast<Matrix44*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description); 
		break;
	case parMemberType::TYPE_MATRIX34:
		m_CurrBank->AddMatrix(metadata.GetName(), reinterpret_cast<Matrix34*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description); 
		break;
	case parMemberType::TYPE_MATRIX44:
		m_CurrBank->AddMatrix(metadata.GetName(), reinterpret_cast<Matrix44*>(ptrToMember), metadatadata->m_Min, metadatadata->m_Max, metadatadata->m_Step, cb, metadatadata->m_Description); 
		break;
	default:
		{
			Quitf("Invalid type for matrix member");
		}
	}
}

void parBuildWidgetsVisitor::EnumMember(int& data, parMemberEnum& metadata)
{
	parMemberEnum::Data* metadatadata = metadata.GetData();

	datCallback cb = metadatadata->m_WidgetCb ? *metadatadata->m_WidgetCb : NullCallback;
	cb.SetBase(*reinterpret_cast<datBase*>(m_ContainingStructureAddress));

	int i = 0;	
	parAssertf(metadatadata->m_EnumData->m_Names, "Can't build a widget for an enum with no names!");
	while(metadatadata->m_EnumData->m_Names[i] != NULL) {
		i++;
	}

	if (metadata.GetSubtype() != parMemberEnumSubType::SUBTYPE_32BIT)
	{
		parErrorf("Can't create widgets for non-32-bit enums (yet)");
		return;
	}

	int expectedValue = metadatadata->m_EnumData->m_Enums[0].m_Value;
	for(int i = 0; i < metadatadata->m_EnumData->m_NumEnums; i++)
	{
		if (metadatadata->m_EnumData->m_Enums[i].m_Value != expectedValue)
		{
			m_CurrBank->AddTitle("%s: %s (read-only enum)", metadata.GetName(), metadata.NameFromValue(data));
			parErrorf("Can't create a combo widget for an enum (%s) with non-sequential values", metadata.GetName());
			return;
		}
		expectedValue++;
	}

	m_CurrBank->AddCombo(metadata.GetName(), &data, i, metadatadata->m_EnumData->m_Names, metadatadata->m_EnumData->m_Enums[0].m_Value, cb, metadatadata->m_Description);
}

void parBuildWidgetsVisitor::BitsetMember(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t /*numBits*/, parMemberBitset& metadata)
{
	using namespace parMemberBitsetSubType;

	parMemberEnum::Data* metadatadata = metadata.GetData();

	datCallback cb = metadatadata->m_WidgetCb ? *metadatadata->m_WidgetCb : NullCallback;
	cb.SetBase(*reinterpret_cast<datBase*>(m_ContainingStructureAddress));

	if (metadatadata->m_EnumData->m_Names)
	{
		// generate widgets for each bit
		m_CurrBank->PushGroup(metadata.GetName(), false, metadatadata->m_Description);
		switch(metadata.GetSubtype())
		{
		case SUBTYPE_ATBITSET:
			{
				int bitcount = Min((int)metadatadata->m_EnumData->m_NumEnums, reinterpret_cast<atBitSet*>(ptrToMember)->GetNumBits());
				for(int i = 0; i < bitcount; i++)
				{
					// For each potential bit, see if the bit has a name in the enum, and add a widget if it does
					const char* name = metadata.GetData()->m_EnumData->NameFromValue(i);
					if (name)
					{
						parAssertf(i <= UCHAR_MAX, "Enum value is too big for widgets: %d", i);
						m_CurrBank->AddToggle(name, reinterpret_cast<atBitSet*>(ptrToMember), (u8)i, cb);
					}
				}
			}
			break;
		case SUBTYPE_8BIT_FIXED:
            {
                int bitsPerBlock = metadata.GetBitsPerBlock();
			    for(int i = 0; i < metadatadata->m_EnumData->m_NumEnums; i++)
			    {
				    int val = metadata.GetData()->m_EnumData->m_Enums[i].m_Value;
                    int block = val / bitsPerBlock;
                    int bitInBlock = val - (block * bitsPerBlock);
				    m_CurrBank->AddToggle(metadatadata->m_EnumData->m_Names[i], (reinterpret_cast<u8*>(ptrToBits) + block), (u8)(1 << bitInBlock), cb);
			    }
            }
			break;
		case SUBTYPE_16BIT_FIXED:
            {
                int bitsPerBlock = metadata.GetBitsPerBlock();
			    for(int i = 0; i < metadatadata->m_EnumData->m_NumEnums; i++)
			    {
				    int val = metadata.GetData()->m_EnumData->m_Enums[i].m_Value;
				    int block = val / bitsPerBlock;
				    int bitInBlock = val - (block * bitsPerBlock);
				    m_CurrBank->AddToggle(metadatadata->m_EnumData->m_Names[i], (reinterpret_cast<u16*>(ptrToBits) + block), (u16)(1 << bitInBlock), cb);
			    }
            }
			break;
		case SUBTYPE_32BIT_FIXED:
            {
                int bitsPerBlock = metadata.GetBitsPerBlock();
			    for(int i = 0; i < metadatadata->m_EnumData->m_NumEnums; i++)
			    {
				    int val = metadata.GetData()->m_EnumData->m_Enums[i].m_Value;
                    int block = val / bitsPerBlock;
                    int bitInBlock = val - (block * bitsPerBlock);
				    m_CurrBank->AddToggle(metadatadata->m_EnumData->m_Names[i], (reinterpret_cast<u32*>(ptrToBits) + block), (u32)(1 << bitInBlock), cb);
			    }
            }
			break;
		}
		m_CurrBank->PopGroup();
	}
	else
	{
		int numBlocks = metadata.GetNumBlocks(m_ContainingStructureAddress);

		// generate int widgets for each block
		switch(metadata.GetSubtype())
		{
		case SUBTYPE_8BIT_FIXED:
			for(int i = 0; i < numBlocks; i++)
			{
				m_CurrBank->AddSlider(metadata.GetName(), ((u8*)ptrToBits) + i, 0, UCHAR_MAX, 1, cb, metadatadata->m_Description);
			}
			break;
		case SUBTYPE_16BIT_FIXED:
			for(int i = 0; i < numBlocks; i++)
			{
				m_CurrBank->AddSlider(metadata.GetName(), ((u16*)ptrToBits) + i, 0, USHRT_MAX, 1, cb, metadatadata->m_Description);
			}
			break;
		case SUBTYPE_32BIT_FIXED:
		case SUBTYPE_ATBITSET:
			for(int i = 0; i < numBlocks; i++)
			{
				m_CurrBank->AddSlider(metadata.GetName(), ((u32*)ptrToBits) + i, 0, UINT_MAX, 1, cb, metadatadata->m_Description);
			}
			break;
		}
	}
}

void parBuildWidgetsVisitor::StringMember(parPtrToMember /*ptrToMember*/, const char* ptrToString, parMemberString& metadata)
{
	if (!ptrToString)
	{
		m_CurrBank->AddROText(metadata.GetName(), "*NULL*", 7);
		return;
	}

	parMemberString::Data* metadatadata = metadata.GetData();

	datCallback cb = metadatadata->m_WidgetCb ? *metadatadata->m_WidgetCb : NullCallback;
	cb.SetBase(*reinterpret_cast<datBase*>(m_ContainingStructureAddress));

	bool isReadOnly = metadata.GetSubtype() != parMemberStringSubType::SUBTYPE_MEMBER;
	int stringLength = metadatadata->m_Length;
	if(isReadOnly)
	{
		stringLength = (int)strlen(ptrToString)+ 1;
	}

	m_CurrBank->AddText(metadata.GetName(), const_cast<char*>(ptrToString), stringLength, isReadOnly, cb);
}

void parBuildWidgetsVisitor::AtNonFinalHashStringMember(atHashString& stringData, parMemberString& metadata)
{
	parMemberString::Data* metadatadata = metadata.GetData();

	datCallback cb = metadatadata->m_WidgetCb ? *metadatadata->m_WidgetCb : NullCallback;
	cb.SetBase(*reinterpret_cast<datBase*>(m_ContainingStructureAddress));

	m_CurrBank->AddText(metadata.GetName(), &stringData, false, cb);
}

void parBuildWidgetsVisitor::AtFinalHashStringMember(atFinalHashString& stringData, parMemberString& metadata)
{
	parMemberString::Data* metadatadata = metadata.GetData();

	datCallback cb = metadatadata->m_WidgetCb ? *metadatadata->m_WidgetCb : NullCallback;
	cb.SetBase(*reinterpret_cast<datBase*>(m_ContainingStructureAddress));

	m_CurrBank->AddText(metadata.GetName(), &stringData, false, cb);
}

void parBuildWidgetsVisitor::AtNamespacedHashStringMember(atNamespacedHashStringBase& , parMemberString& ASSERT_ONLY(metadata))
{
	parAssertf(0, "Can't create widgets for %s member (%s)", atHashStringNamespaceSupport::GetNamespaceName(metadata.GetData()->GetNamespaceIndex()), metadata.GetName());
}

void parBuildWidgetsVisitor::AtNamespacedHashValueMember(atNamespacedHashValueBase& , parMemberString& ASSERT_ONLY(metadata))
{
	parAssertf(0, "Can't create widgets for %s member (%s)", atHashStringNamespaceSupport::GetNamespaceValueName(metadata.GetData()->GetNamespaceIndex()), metadata.GetName());
}

void parBuildWidgetsVisitor::AtHashValueMember(atHashValue& /*stringData*/, parMemberString& ASSERT_ONLY(metadata))
{
	parAssertf(0, "Can't create widgets for atHashValue member (%s)", metadata.GetName());
}

void parBuildWidgetsVisitor::AtPartialHashValueMember(u32& /*stringData*/, parMemberString& ASSERT_ONLY(metadata))
{
	parAssertf(0, "Can't create widgets for atPartialHashValue member (%s)", metadata.GetName());
}

bool parBuildWidgetsVisitor::BeginStructMember		(parPtrToStructure /*ptrToStruct*/,	parMemberStruct& metadata)
{
	if (metadata.CanAddGroupWidget())
	{
		m_CurrBank->PushGroup(metadata.GetName(), false, NULL);
	}

	return true;
}

void parBuildWidgetsVisitor::EndStructMember		(parPtrToStructure /*ptrToStruct*/,	parMemberStruct& metadata)
{
	if (metadata.CanAddGroupWidget())
	{
		m_CurrBank->PopGroup();
	}
}

bool parBuildWidgetsVisitor::BeginPointerMember		(parPtrToStructure& ptrRef,	parMemberStruct& metadata)
{
	if (metadata.CanAddGroupWidget())
	{
		if (metadata.CanDerive())
		{
			char fullName[256];
			formatf(fullName, "%s (%s)", metadata.GetName(), metadata.GetConcreteStructure(ptrRef)->GetName());
			m_CurrBank->PushGroup(fullName, false, NULL);
		}
		else
		{
			m_CurrBank->PushGroup(metadata.GetName(), false, NULL);
		}
	}

	return (ptrRef != NULL);
}

void parBuildWidgetsVisitor::EndPointerMember		(parPtrToStructure& /*ptrRef*/,	parMemberStruct& metadata)
{
	if (metadata.CanAddGroupWidget())
	{
		m_CurrBank->PopGroup();
	}
}


bool parBuildWidgetsVisitor::BeginToplevelStruct	(parPtrToStructure /*ptrToStruct*/,	parStructure& /*metadata*/)
{
	return true;
}

void parBuildWidgetsVisitor::EndToplevelStruct		(parPtrToStructure /*ptrToStruct*/,	parStructure& /*metadata*/)
{

}

bool parBuildWidgetsVisitor::BeginArrayMember		(parPtrToMember /*ptrToMember*/,	parPtrToArray /*arrayContents*/, size_t /*numElements*/, parMemberArray& metadata)
{
	if (metadata.CanAddGroupWidget())
	{
		m_CurrBank->PushGroup(metadata.GetName(), false, NULL);
	}

	return true;

}

void parBuildWidgetsVisitor::EndArrayMember			(parPtrToMember /*ptrToMember*/, parPtrToArray /*arrayContents*/, size_t /*numElements*/, parMemberArray& metadata)
{
	if (metadata.CanAddGroupWidget())
	{
		m_CurrBank->PopGroup();
	}
}

bool parBuildWidgetsVisitor::BeginMapMember		(parPtrToStructure /*structAddr*/,	parMemberMap& metadata)
{
    if (metadata.CanAddGroupWidget())
    {
        m_CurrBank->PushGroup(metadata.GetName(), false, NULL);
    }

    return true;

}

void parBuildWidgetsVisitor::EndMapMember			(parPtrToStructure /*structAddr*/, parMemberMap& metadata)
{
    if (metadata.CanAddGroupWidget())
    {
        m_CurrBank->PopGroup();
    }
}

void parBuildWidgetsVisitor::VisitMapMember(parPtrToStructure /*structAddr*/, parPtrToStructure mapKeyAddress, parPtrToStructure mapDataAddress, parMemberMap& metadata)
{
    char groupName[256];
    groupName[0] = 0;
    switch (metadata.GetKeyMember()->GetType())
    {
        //These are the limited supported types for key values
    case parMemberType::TYPE_CHAR:
        {
            s8 value = *((s8*)mapKeyAddress);
            formatf(groupName, "%d", value);
        }
        break;
    case parMemberType::TYPE_UCHAR:
        {
            u8 value = *((u8*)mapKeyAddress);
            formatf(groupName, "%u", value);
        }
        break;
    case parMemberType::TYPE_SHORT:
        {
            s16 value = *((s16*)mapKeyAddress);
            formatf(groupName, "%d", value);
        }
        break;
    case parMemberType::TYPE_USHORT:
        {
            u16 value = *((u16*)mapKeyAddress);
            formatf(groupName, "%u", value);
        }
        break;
    case parMemberType::TYPE_INT:
        {
            s32 value = *((s32*)mapKeyAddress);
            formatf(groupName, "%d", value);
        }
        break;
    case parMemberType::TYPE_UINT:
        {
            u32 value = *((u32*)mapKeyAddress);
            formatf(groupName, "%u", value);
        }
        break;
	case parMemberType::TYPE_INT64:
		{
			s64 value = *((s64*)mapKeyAddress);
			formatf(groupName, "%" I64FMT "d", value);
		}
		break;
	case parMemberType::TYPE_UINT64:
		{
			u64 value = *((u64*)mapKeyAddress);
			formatf(groupName, "%" I64FMT "u", value);
		}
		break;
	case parMemberType::TYPE_ENUM:
		{
			parMemberEnum& keyEnumMember = metadata.GetKeyMember()->AsEnumRef();
		
			int value = 0;
			switch(keyEnumMember.GetSubtype())
			{
			case parMemberEnumSubType::SUBTYPE_8BIT:
				{
					value = *((u8*)mapKeyAddress);
				}
				break;
			case parMemberEnumSubType::SUBTYPE_16BIT:
				{
					value = *((u16*)mapKeyAddress);
				}
				break;
			case parMemberEnumSubType::SUBTYPE_32BIT:
				{
					value = *((int*)mapKeyAddress);
				}
				break;
			}

			const char* pName = keyEnumMember.NameFromValue( value );
			if (pName) 
				formatf(groupName, "%s", pName);
			else
				formatf(groupName, "%d", value);

		}
		break;
    case parMemberType::TYPE_STRING:
        {
            parMemberString& strParMember = metadata.GetKeyMember()->AsStringRef();
            switch (strParMember.GetSubtype())
            {
#if !__FINAL
            case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
                formatf(groupName, "%s", ((atHashString*)mapKeyAddress)->GetCStr());
                break;
#endif
			case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
				formatf(groupName, "%s", ((atFinalHashString*)mapKeyAddress)->GetCStr());
				break;
			case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
				formatf(groupName, "%s", atHashStringNamespaceSupport::GetString(strParMember.GetData()->GetNamespaceIndex(), *((u32*)mapKeyAddress)));
				break;
			case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
				parAssertf(0, "Can't create widgets for a map (%s) that contains atHashValue keys", metadata.GetName());
				break;
			case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
				parAssertf(0, "Can't create widgets for a map (%s) that contains atPartialHashValue keys", metadata.GetName());
				break;
			case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
				parAssertf(0, "Can't create widgets for a map (%s) that contains %s keys", metadata.GetName(), atHashStringNamespaceSupport::GetNamespaceValueName(strParMember.GetData()->GetNamespaceIndex()));
				break;
			default:
                parAssertf(0,"Unsupported key string type %d being used for map %s", strParMember.GetSubtype(), metadata.GetName());
                break;
            };
        }
        break;
    default:
        parAssertf(0,"Unsupported key type %d being used for map %s", metadata.GetKeyMember()->GetType(), metadata.GetName());
        break;
    };

    m_CurrBank->PushGroup(groupName, false, NULL);

    VisitMember(mapDataAddress, *(metadata.GetDataMember()));

    m_CurrBank->PopGroup();
}

void parBuildWidgetsVisitor::ExternalPointerMember(void*& data, parMemberStruct& metadata)
{
	if (metadata.IsExternalPointer())
	{
		const char* ptrName = metadata.GetData()->m_PtrToNameCB(data);
		if (ptrName) {
			m_CurrBank->AddText(metadata.GetName(), const_cast<char*>(ptrName), (u32)strlen(ptrName)+1, true);
		}
		else {
			m_CurrBank->AddROText(metadata.GetName(), "NULL", 5);
		}
	}
	// what to do with links? do rev. lookup here?
}

#endif	// __BANK
