// 
// parser/memberarray.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBERARRAY_H
#define PARSER_MEMBERARRAY_H

#include "member.h"

#include "membersimple.h"

#include "memberarraydata.h"

#include "atl/delegate.h"


namespace rage {

// PURPOSE: A subclass of parMember that represents an array of data. A parMemberArray has a pointer to
// another parMember that describes the contents of the array. 
class parMemberArray : public parMember
{
	friend class parInstanceVisitor;
	friend class parDeepCopyVisitor;

public:
	typedef parMemberArraySubType::Enum SubType;
	typedef parMemberArrayData Data;

	enum
	{
		MAX_TABLE_COLUMNS = 100,
	};

	parMemberArray() : m_PrototypeMember(NULL) {}
	explicit parMemberArray(Data& data, parMember* prototype=NULL); // If you pass in a prototype, this object takes ownership of the prototype. It'll delete it later on.
	virtual ~parMemberArray();

	virtual void ReadTreeNode(parTreeNode* node, parPtrToStructure data) const;

	virtual void LoadExtraAttributes(parTreeNode* node);

	parMember* GetPrototypeMember() {
		return m_PrototypeMember;
	}

	const parMember* GetPrototypeMember() const {
		return m_PrototypeMember;
	}

	// RETURNS: True if this array can be expressed as a 2d table
	bool Is2dTable();

	int CountNumCsvRows(const char* csvData) const;
	int CountNumCsvCols(const char* csvData) const;

	// PURPOSE: Generally shouldn't be used by client code - needed for parRTStructure
	void SetPrototypeMember(parMember* member) {
		m_PrototypeMember = member;
	}

	virtual size_t GetSize() const;

	virtual size_t FindAlign() const;

	bool IsFixedSize() const {
		using namespace parMemberArraySubType;
		return (
			GetSubtype() == SUBTYPE_MEMBER ||
			GetSubtype() == SUBTYPE_ATRANGEARRAY ||
			GetSubtype() == SUBTYPE_POINTER ||
			GetSubtype() == SUBTYPE_VIRTUAL			// "Fixed" at size=0
			);
	}

	// Does m_NumElements represent the max number of items we can put in the array
	bool HasMaxSize() const {
		using namespace parMemberArraySubType;
		return (
			GetSubtype() == SUBTYPE_MEMBER ||
			GetSubtype() == SUBTYPE_ATRANGEARRAY ||
			GetSubtype() == SUBTYPE_ATFIXEDARRAY ||
			GetSubtype() == SUBTYPE_POINTER || 
			GetSubtype() == SUBTYPE_VIRTUAL);
	}

	// Is the array's storage part of the same structure that contains the array, or allocated externally?
	bool HasExternalStorage() const {
		using namespace parMemberArraySubType;
		return !(
			// these are the only internal storage types, so return "not these".
			GetSubtype() == SUBTYPE_MEMBER || 
			GetSubtype() == SUBTYPE_ATRANGEARRAY ||
			GetSubtype() == SUBTYPE_ATFIXEDARRAY ||
			GetSubtype() == SUBTYPE_VIRTUAL);
	}

	// PURPOSE: given the base address of a structure containing one of these members, get
	// the address of the actual array contents, and the number of items to expect there.
	void GetArrayContentsAndCountFromStruct(parConstPtrToStructure structAddr, parConstPtrToArray& outAddr, size_t& outElements) const;
	void GetArrayContentsAndCountFromStruct(parPtrToStructure structAddr, parPtrToArray& outAddr, size_t& outElements) const;

	// PURPOSE: Only for the POINTER_WITH_COUNT subtypes, gets the current count variable
	u32 GetCountFromPointerWithCount(parConstPtrToStructure structAddr) const;

	// PURPOSE: Only for the POINTER_WITH_COUNT subtypes, sets the current count variable
	void SetCountForPointerWithCount(parPtrToStructure structAddr, size_t newCount) const;

	enum ArrayResizeFlags
	{
		CONSTRUCT_NEW_ELEMENTS,
		INITIALIZE_NEW_ELEMENTS,
		DESTRUCT_OLD_ELEMENTS,
	};

	bool SetArraySize(parPtrToStructure structAddr, size_t size, u8 flags = 
		((1 << CONSTRUCT_NEW_ELEMENTS) | 
		(1 << INITIALIZE_NEW_ELEMENTS) | 
		(1 << DESTRUCT_OLD_ELEMENTS))) const;

	// PURPOSE: Convenience function, only for ATFIXEDARRAY, returns the address of the count variable
	int* GetFixedArrayCountAddress(parPtrToStructure structAddr) const;
	const int* GetFixedArrayCountAddress(parConstPtrToStructure structAddr) const;

	// PURPOSE: Deletes one item from the array (calling its destructor if necessary) and shifts all
	// other elements down one spot.
	bool DeleteItem(parPtrToStructure structAddr, size_t i);

	// PURPOSE: Inserts an item into the array. Technically this inserts a 'hole' where the item goes, and
	// modifies the prototype member so that it points to that new location.
	// PARAMS:
	//	structAddr - address of the structure that contains the array member variable
	//  i - location for the new array item. If < 0, appends to the end of the array
	bool InsertItem(parPtrToStructure structAddr, size_t i, bool initNewElement = true);
	static const size_t INSERT_AT_END = (size_t)(ptrdiff_t)-1;

	static int sm_NoSanityChecks; // Turn off in special circumstances (like visiting uninitialized memory) - 


	// PURPOSE: Gets the address of an array element
	// PARAMS:
	//		arrayAddr - start address of the array contents
	//		i - array index
	// NOTES:
	//		This returns a parPtrToStructure, though it does not really point to the beginning of a parsable structure. For example if you had 
	//		struct Foo { int m_Ints[5]; }, GetAddressOfNthElement(..., 3) would return a pointer to m_Ints[3], a member variable. The reason
	//		it's returned as a parPtrToStructure and not a parPtrToMember is because can then be used in combination with GetPrototypeMember() for
	//		further parser operations, like so:
	//			parPtrToStructure ppts = GetAddressOfNthElement(myArray, 3);
	//			int intVal = GetPrototypeMember()->GetMemberFromStruct<int>(ppts);
	//		So this address is effectively the address of the "virtual" struct that GetPrototypeMember() is a member of
	parPtrToStructure GetAddressOfNthElement(parPtrToArray arrayAddr, size_t i) const;

    //PURPOSE
    //  Parses a CSV string.  Call repeatedly to parse the entire string.
    //  Each call results in a value being copied to buf.
    //PARAMS
    //  buf     - Output buffer
    //  bufLen  - IN:   Max length of buf.
    //            OUT:  String length of result in buf.
    //  csvData - The CSV string.  The pointer will be advanced as each
    //            value in the CSV is parsed.
    //RETURNS
    //  True when the end of the CSV string is reached.
	static bool ReadCsvDatum(char* buf, unsigned* bufLen, const char*& csvData);

	size_t FindContentAlign() const;

	// Returns a value x = 0..15 such that the requested alignment is 1<<x
	// (The actual alignment is the stricter of this value and the native alignment for the data type,
	// i.e. usually the alignment power is 0 since we just rely on the native value)
	u32 GetAlignmentPower() const;

protected:
	parMember* m_PrototypeMember;

	void CreateArrayFromNode(parTreeNode* node, parPtrToStructure structAddr) const;
	int CountKids(parTreeNode* node) const;

	static bool NeedsArrayCookie(parMemberType::Enum type, u16 subtype);

	static void SkipWhitespace(const char*& csvData, bool skipNl = false);

	void Resize16BitAtArray(parPtrToMember arrayAddr, size_t count, size_t oldCount, u8 flags) const;
	void Resize32BitAtArray(parPtrToMember arrayAddr, size_t count, size_t oldCount, u8 flags) const;
	void ResizeRawArray(parPtrToMember arrayAddr, size_t count, size_t oldCount, u8 flags) const;
	parPtrToArray AllocStorage(size_t size, size_t align) const;
	void FreeStorage(parPtrToArray ptr) const;

public:

	STANDARD_PARMEMBER_DATA_FUNCS_DECL(parMemberArray);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_PARSABLE;
#endif
};

// This can't be defined until the derived class is.
inline parMemberArray*	parMember::AsArray()			{ return parMemberType::IsArray(GetType()) ? smart_cast<parMemberArray*>(this) : NULL; }

}

#endif
