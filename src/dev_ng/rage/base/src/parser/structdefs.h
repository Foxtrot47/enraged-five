// 
// parser/structdefs.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_STRUCTDEFS_H
#define PARSER_STRUCTDEFS_H

#include "macros.h"

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS

#include "psobuilder.h"

#include "atl/array.h"
#include "atl/hashstring.h"
#include "atl/map.h"

namespace rage {

class parTree;
class parTreeNode;
class psoBuilderStructSchema;

class parStructdefCatalog
{
public:
	parStructdefCatalog() {}

	~parStructdefCatalog();

	void LoadPscFile(const char* filename);
	void LoadAllPscFiles(const char* rootPath);

	void LoadEsdFile(const char* filename);

	void DeleteStructdef(atLiteralHashValue namehash);
	void DeleteEnumdef(atLiteralHashValue namehash);

	parTreeNode* FindStructdef(atLiteralHashValue namehash);
	parTreeNode* FindEnumdef(atLiteralHashValue namehash);

	void BuildStructure(parStructure& structure, parTreeNode& structdefXml);

	parEnumData* BuildEnumData(parTreeNode& enumdefXml);

	parStructure* FindStructureWithTypeHash(atLiteralHashValue typeHash);

	typedef atCaseSensitiveStringMap<int> ConstTable;

protected:
	// RETURNS: Total padding
	int ComputeSizeAndOffset_MSVC(parStructure&, parTreeNode&);
	int ComputeSizeAndOffset_GCC(parStructure&, parTreeNode&);

	struct PscFileInfo
	{
		parTree* m_Root;
		ConstTable m_ConstTable;
	};

	atArray<PscFileInfo*> m_FileInfos;

	// We need this because in structdefs there might be a <structdef type="::some::long::name" name="shortname"/> and then later
	// a <struct type="::some::long::name"/> member var. 
	// When we find this case, we add an entry that maps "::some::long::name" to "shortname", so we can use "shortname" from then on.
	typedef atMap<u32, u32> TypeHashToNameHashMap;
	TypeHashToNameHashMap m_TypeHashToNameHash;

	atMap<u32, parTreeNode*> m_Structdefs;
	atMap<u32, parTreeNode*> m_Enumdefs;
};


} // namespace rage

#endif // PARSER_USES_EXTERNAL_STRUCTURE_DEFNS

#endif // PARSER_STRUCTDEFS_H