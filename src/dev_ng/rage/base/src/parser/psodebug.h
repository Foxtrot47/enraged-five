// 
// parser/psodebug.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "psofile.h"

#include "diag\restservice.h"

#if !__FINAL
namespace rage
{
	class psoDebugJsonWriter
	{
	public:
		psoDebugJsonWriter(fiStream& stream);

		void WriteSchemaCatalog(const psoSchemaCatalog& cat);
		void WriteStructureSchema(const psoStructureSchema& schema);
		void WriteEnumSchema(const psoEnumSchema& schema);
		void WriteEnumValue(const psoEnumSchema& schema, int index);
		void WriteMemberSchema(const psoMemberSchema& schema);
		const char* GetHashType(atLiteralHashValue hash);
	private:
		fiStream* m_Stream;
	};

	class psoDebugMismatchedFiles
	{
	public:
		psoDebugMismatchedFiles();
		~psoDebugMismatchedFiles();

		static const int MaxMismatchedFiles = 100;

		static void InitClass();
		static void ShutdownClass();
		static bool IsInitted() { return sm_Instance != NULL; }
		static psoDebugMismatchedFiles& GetInstance() { FastAssert(sm_Instance); return *sm_Instance; }

		void AddMismatchedFile(psoFile& file);

		enum ResourceDataType
		{
			PSO,	// The PSO file we loaded that didn't match
			PSC,	// The definition as per the PSC file
			CPP,
		};

#if __BANK
		class RestInterface : public restService
		{
		public:
			RestInterface(const char* name) : restService(name) {}
			virtual restStatus::Enum ProcessCommand(restCommand& command);
			virtual const char* GetServiceType() {return "PSO Mismatch Debugger";}

		private:
			restStatus::Enum GetFileSchema(restCommand& command, psoResourceData* data);
		};
#endif // __BANK
		
	protected:
		psoResourceData* BuildNewSchemasOnlyResource(psoResourceData& input, ResourceDataType sourceForNewSchemas);
		psoResourceData* BuildNewSchemasOnlyResourceFromParManager(psoResourceData& input);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
		void LoadExternalPscFiles(const char* dir);
#endif
		parManager*							m_PscFileDefinitions;
		atMap<atString, psoResourceData*>	m_MismatchedFiles; 
#if __BANK
		RestInterface						m_RestInterface;
#endif // __BANK
		static psoDebugMismatchedFiles* sm_Instance;
	};
}
#endif
