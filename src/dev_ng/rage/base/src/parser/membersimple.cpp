// 
// parser/membersimple.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "membersimple.h"

#include "optimisations.h"
#include "treenode.h"

#include "bank/bank.h"
#include "math/float16.h"
#include "parsercore/attribute.h"
#include "vectormath/boolv.h"
#include "vectormath/scalarv.h"

#include <limits.h>
#include <math.h>

PARSER_OPTIMISATIONS();

using namespace rage;

STANDARD_PARMEMBER_DATA_FUNCS_DEFN(parMemberSimple);

void parMemberSimple::Data::Init()
{
	StdInit();
	m_Init = 0.0f;
#if __BANK
	m_Description = NULL;
	m_Min = -FLT_MAX;
	m_Max = FLT_MAX;
	m_Step = 0.01f;
	m_WidgetCb = NULL;
#endif
}

parMemberSimple::~parMemberSimple()
{
	if (!IsStatic())
	{
#if __BANK
		delete GetData()->m_Description;
#endif
	}
#if __BANK
	delete GetData()->m_WidgetCb;
#endif
}

void parMemberSimple::ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const
{
	if (!parVerifyf(node, "Can't handle NULLs, leaving member %s alone", GetName()))
	{
		return;
	}

	switch(GetType())
	{
	case parMemberType::TYPE_FLOAT:		GetMemberFromStruct<float>(structAddr) = node->ReadStdLeafFloat(); break;
	case parMemberType::TYPE_FLOAT16:	GetMemberFromStruct<Float16>(structAddr) = Float16(node->ReadStdLeafFloat()); break;
	case parMemberType::TYPE_CHAR:		GetMemberFromStruct<s8>(structAddr) = (s8)node->ReadStdLeafInt(); break;
	case parMemberType::TYPE_SHORT:		GetMemberFromStruct<s16>(structAddr) = (s16)node->ReadStdLeafInt(); break;
	case parMemberType::TYPE_INT:		GetMemberFromStruct<s32>(structAddr) = (s32)node->ReadStdLeafInt(); break;
	case parMemberType::TYPE_UCHAR:		GetMemberFromStruct<u8>(structAddr) = (u8)node->ReadStdLeafInt(); break;
	case parMemberType::TYPE_USHORT:	GetMemberFromStruct<u16>(structAddr) = (u16)node->ReadStdLeafInt(); break;
	case parMemberType::TYPE_UINT:		GetMemberFromStruct<u32>(structAddr) = (u32)node->ReadStdLeafInt(); break;
	case parMemberType::TYPE_PTRDIFFT:	GetMemberFromStruct<ptrdiff_t>(structAddr) = (ptrdiff_t)node->ReadStdLeafInt(); break;	// not sure if correct
	case parMemberType::TYPE_SIZET:		GetMemberFromStruct<size_t>(structAddr) = (size_t)node->ReadStdLeafInt(); break;	// not sure if correct
	case parMemberType::TYPE_BOOL:		GetMemberFromStruct<bool>(structAddr) = node->ReadStdLeafBool(); break;
	case parMemberType::TYPE_SCALARV:	GetMemberFromStruct<ScalarV>(structAddr).Setf(node->ReadStdLeafFloat()); break;
	case parMemberType::TYPE_BOOLV:		GetMemberFromStruct<BoolV>(structAddr) = node->ReadStdLeafBool() ? BoolV(V_TRUE) : BoolV(V_FALSE); break;
	case parMemberType::TYPE_INT64:		GetMemberFromStruct<s64>(structAddr) = node->ReadStdLeafInt64(); break;
	case parMemberType::TYPE_UINT64:	GetMemberFromStruct<u64>(structAddr) = (u64)node->ReadStdLeafInt64(); break;
	case parMemberType::TYPE_DOUBLE:	GetMemberFromStruct<double>(structAddr) = node->ReadStdLeafDouble(); break;
	default:
		parErrorf("Invalid type %d for simple member %s", GetType(), GetName());
	}
}

void parMemberSimple::SetFromString(parPtrToStructure structAddr, const char* name) const
{
	if (!parVerifyf(name, "Can't handle empty string, leaving member %s alone", GetName()))
	{
		return;
	}

	switch(GetType())
	{
	case parMemberType::TYPE_BOOL:		GetMemberFromStruct<bool>(structAddr) = parUtils::StringToBool(name); break;
	case parMemberType::TYPE_FLOAT:		GetMemberFromStruct<float>(structAddr) = parUtils::StringToFloat(name); break;
	case parMemberType::TYPE_FLOAT16:	GetMemberFromStruct<Float16>(structAddr) = Float16(parUtils::StringToFloat(name)); break;
	case parMemberType::TYPE_CHAR:		GetMemberFromStruct<s8>(structAddr) = (s8)parUtils::StringToInt(name); break;
	case parMemberType::TYPE_UCHAR:		GetMemberFromStruct<u8>(structAddr) = (u8)parUtils::StringToInt(name); break;
	case parMemberType::TYPE_SHORT:		GetMemberFromStruct<s16>(structAddr) = (s16)parUtils::StringToInt(name); break;
	case parMemberType::TYPE_USHORT:	GetMemberFromStruct<u16>(structAddr) = (u16)parUtils::StringToInt(name); break;
	case parMemberType::TYPE_INT:		GetMemberFromStruct<s32>(structAddr) = (s32)parUtils::StringToInt(name); break;
	case parMemberType::TYPE_UINT:		GetMemberFromStruct<u32>(structAddr) = (u32)parUtils::StringToInt(name); break;
	case parMemberType::TYPE_PTRDIFFT:	GetMemberFromStruct<ptrdiff_t>(structAddr) = (ptrdiff_t)parUtils::StringToInt(name); break;	// not sure if correct
	case parMemberType::TYPE_SIZET:		GetMemberFromStruct<size_t>(structAddr) = (size_t)parUtils::StringToInt(name); break;	// not sure if correct
	case parMemberType::TYPE_SCALARV:	GetMemberFromStruct<ScalarV>(structAddr).Setf(parUtils::StringToFloat(name)); break;
	case parMemberType::TYPE_BOOLV:		GetMemberFromStruct<BoolV>(structAddr) = parUtils::StringToBool(name) ? BoolV(V_TRUE) : BoolV(V_FALSE); break;
	case parMemberType::TYPE_INT64:		GetMemberFromStruct<s64>(structAddr) = parUtils::StringToInt64(name); break;
	case parMemberType::TYPE_UINT64:	GetMemberFromStruct<u64>(structAddr) = (u64)parUtils::StringToInt64(name); break;
	case parMemberType::TYPE_DOUBLE:	GetMemberFromStruct<double>(structAddr) = parUtils::StringToDouble(name); break;
	default:
		parErrorf("Invalid type %d for simple member %s", GetType(), GetName());
	}
}

size_t rage::parMemberSimple::GetSize() const
{
	return parMemberType::GetSize(GetType());
}