// 
// parser/memberstringdata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MEMBERSTRINGDATA_H 
#define PARSER_MEMBERSTRINGDATA_H 

#include "memberdata.h"

#include "atl/hashstring.h"
#include "parser/macros.h"
#include "data/callback.h"

namespace rage {

namespace parMemberStringSubType
{
	enum Enum
	{
		SUBTYPE_MEMBER,		// char m_String[32]
		SUBTYPE_POINTER,	// char* m_String
		SUBTYPE_CONST_STRING,	// ConstString m_String
		SUBTYPE_ATSTRING,		// atString m_String
		SUBTYPE_WIDE_MEMBER,		// TCHAR m_String[32]
		SUBTYPE_WIDE_POINTER,		// TCHAR m_String*
		SUBTYPE_ATWIDESTRING,		// atWideString m_String
		SUBTYPE_ATNONFINALHASHSTRING,	// atNonFinalHashString m_String
		SUBTYPE_ATFINALHASHSTRING,	// atFinalHashString m_String
		SUBTYPE_ATHASHVALUE,		// atHashValue m_String
		SUBTYPE_ATPARTIALHASHVALUE,	// u32 m_String
		SUBTYPE_ATNSHASHSTRING,		// atNamespacedHashString<NS>
		SUBTYPE_ATNSHASHVALUE,		// atNamespacedHashValue<NS>
	};
}

// PURPOSE: All of the data that a parMemberString needs to describe the member.
struct parMemberStringData
{
	STANDARD_PARMEMBER_DATA_CONTENTS;
	u32 m_Length;						// Maximum number of characters in the string (not necessarily bytes)
#if __BANK
	const char* m_Description;			// Text description of the member. Used for tooltips in the bank
	datCallback* m_WidgetCb;
#endif
	void Init();
	void PreLoad(parTreeNode*){Init();}

	// Bottom 8 bits of m_TypeFlags are the namespace index
	atHashStringNamespaces GetNamespaceIndex() const {return (atHashStringNamespaces)(m_TypeFlags & 0xFF);}
	void SetNamespaceIndex(atHashStringNamespaces val) { m_TypeFlags = (m_TypeFlags & ~0xFF) | (u8)val; }

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_SIMPLE_PARSABLE;
#endif
};


} // namespace rage

#endif // PARSER_MEMBERSTRINGDATA_H 
