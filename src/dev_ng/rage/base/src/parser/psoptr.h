// 
// parser/psoptr.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_PSOPTR_H
#define PARSER_PSOPTR_H

#include "data/resource.h"
#include "data/safestruct.h"
#include "math/amath.h"
#include "system/endian.h"

namespace rage {

// PURPOSE: psoPtr is a template class that's used to hold a platform-neutral pointer that can be loaded from a resource.
// It is 8 bytes wide and 8 byte aligned, so that it can hold pointers from 32-bit and 64-bit machines. And it can be loaded from resources
// generated in either endianness.
enum psoPtrNeedsSwap
{
	PSOPTR_NO_SWAP, 
	PSOPTR_SWAP
};

enum psoPtrOriginalSize
{
	PSOPTR_32BIT, 
	PSOPTR_64BIT
};

template<typename T>
class psoPtr
{
public:
	static const u32 MagicNumber = 0x11113333;

	typedef psoPtr<T> MyType;

	psoPtr() {
		m_Ptr = NULL;
#if !__64BIT
		m_PadAfterPtr = MagicNumber;
#endif
	}

	~psoPtr() { 
#if !__64BIT
		FastAssert(m_PadAfterPtr == MagicNumber);
#endif
	};

	psoPtr(datResource& rsc, psoPtrNeedsSwap doSwap, psoPtrOriginalSize ptrSize);

	T& operator*() { return *m_Ptr; }
	const T& operator*() const { return *m_Ptr; }
	T* operator->() { return m_Ptr; }
	const T* operator->() const  { return m_Ptr; }

	psoPtr<T>& operator=(T* t) { m_Ptr = t; return *this; }

	T& operator[](size_t x) { return m_Ptr[x]; }
	const T& operator[](size_t x)  const { return m_Ptr[x]; }

	operator bool() { return m_Ptr != NULL; }

	T* GetPtr() { return m_Ptr; }
	const T* GetPtr() const { return m_Ptr; }

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

	// Sets this pointer to a new array, and allocates the array /without/ creating an array cookie
	void NewArrayNoCookie(size_t count);
	void DeleteArrayNoCookie(size_t count);

#if !RSG_ORBIS && !__SPU
#pragma warning(push)
#pragma warning(disable:4201)
#endif // !RSG_ORBIS
	union
	{
#if __64BIT
		T* m_Ptr;
#else
		struct 
		{
			T* m_Ptr;
			u32 m_PadAfterPtr;
		};
#endif
		struct  
		{
			u32 m_Low32Bits;
			u32 m_High32Bits;
		};
		u64 m_Pad64;
	};
#if !RSG_ORBIS && !__SPU
#pragma warning(pop)
#endif // !RSG_ORBIS
};

template<typename T> psoPtr<T>::psoPtr(datResource& /*rsc*/, psoPtrNeedsSwap doSwap, psoPtrOriginalSize ptrSize)
{
	if (ptrSize == PSOPTR_32BIT)
	{
		if (doSwap == PSOPTR_SWAP)
		{
			sysEndian::SwapMe(m_Low32Bits); // swap the pointer
			sysEndian::SwapMe(m_High32Bits);
		}
		FastAssert(m_High32Bits == MagicNumber); // make sure we didn't screw anything up
	}
	else
	{
		if (doSwap == PSOPTR_SWAP)
		{
			sysEndian::SwapMe(m_Pad64);
#if !__64BIT
			// originating size was 64, we swapped all the "good" bits into the high 32bits, swap them back into the low 32 which overlaps the pointer
			SwapEm(m_Low32Bits, m_High32Bits);
#endif
		}
		FastAssert(m_High32Bits == 0x0); // rsc offsets shouldn't get this big, high 32bits should be all 0
	}

	datResource::Fixup(m_Ptr);
}

#if __DECLARESTRUCT
template<typename T> void psoPtr<T>::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(MyType)
		SSTRUCT_FIELD_AS(MyType, m_Ptr, char*) /* cast to char* gets the pointer relocated */
#if !__64BIT
		SSTRUCT_FIELD(MyType, m_PadAfterPtr)
#endif
	SSTRUCT_END(MyType)
}
#endif


template<typename Type>
void psoPtr<Type>::NewArrayNoCookie(size_t count)
{
	if (count == 0)
	{
		m_Ptr = NULL;
		return;
	}
	size_t allocSize = sizeof(Type) * count;
	m_Ptr = (Type*)rage_aligned_new(__alignof(Type)) char[allocSize];
	sysMemSet(m_Ptr, 0x0, allocSize);
	for(size_t i = 0; i < count; i++)
	{
		rage_placement_new(&m_Ptr[i]) Type;
	}
}

template<typename Type>
void psoPtr<Type>::DeleteArrayNoCookie(size_t count)
{
	for(size_t i = 0; i < count; i++)
	{
		m_Ptr[i].~Type();
	}
	delete reinterpret_cast<char*>(m_Ptr);
	m_Ptr = NULL;
}


} // namespace rage

#endif // PARSER_PSOPTR_H