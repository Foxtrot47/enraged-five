// 
// parser/optimisations.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_OPTIMISATIONS_H
#define PARSER_OPTIMISATIONS_H

#define PARSER_OPTIMISATIONS_OFF	(0 && !__FINAL)

#if PARSER_OPTIMISATIONS_OFF
#define PARSER_OPTIMISATIONS()	PRAGMA_OPTIMIZE_OFF()   // PRAGMA-OPTIMIZE-ALLOW
#else
#define PARSER_OPTIMISATIONS()
#endif // PARSER_OPTIMISATIONS_OFF

#endif