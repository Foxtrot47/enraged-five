// 
// parser/psodebug.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#include "psodebug.h"
#include "psorscbuilder.h"
#include "psorscparserbuilder.h"

#include "system/timer.h"

#if !__FINAL

using namespace rage;
psoDebugJsonWriter::psoDebugJsonWriter(fiStream& stream)
	: m_Stream(&stream)
{

}

#define PRINT_STRING(name, value)		fprintf(m_Stream, "%s\"" name "\":\"%s\"", delim, value); delim=",";
#define PRINT_UINT(name, value)		fprintf(m_Stream, "%s\"" name "\":%u", delim, value); delim=",";
#define PRINT_INT(name, value)		fprintf(m_Stream, "%s\"" name "\":%d", delim, value); delim=",";
#define PRINT_BOOL(name, value)		fprintf(m_Stream, "%s\"" name "\":%s", delim, value ? "true" : "false"); delim=",";
#define PRINT_HASH(name, value)	{										\
	atLiteralHashValue hash = value;									\
	PRINT_UINT(name "Hash", hash.GetHash());							\
	const char* str = atLiteralHashString::TryGetString(hash.GetHash());	\
	if (str)															\
	{																	\
		PRINT_STRING(name, str);										\
	}																	\
}

const char* psoDebugJsonWriter::GetHashType(atLiteralHashValue hash)
{
	if (psoConstants::IsPsoType(hash))
	{
		return "psoType";
	}
	else if (psoConstants::IsAnonymous(hash))
	{
		return "anonymous";
	}
	else if (psoConstants::IsHashstringId(hash))
	{
		return "hashstring";
	}
	else if (psoConstants::IsReserved(hash))
	{
		return "reserved"; // Shouldn't really get here, the cases above should cover all the reserved values
	}
	return "named";
}

void psoDebugJsonWriter::WriteMemberSchema(const psoMemberSchema& mem)
{
	psoType memType = mem.GetType();
	const char* delim = "";

	fprintf(m_Stream, "{");
	PRINT_HASH("name", mem.GetNameHash());
	PRINT_STRING("hashType", GetHashType(mem.GetNameHash()));
	PRINT_STRING("typeName", memType.GetName());
	PRINT_UINT("type", memType.GetRaw());
	PRINT_UINT("offset", mem.GetOffset());

	if (memType == psoType::TYPE_STRUCT)
	{
		PRINT_HASH("referentStructure", mem.GetReferentHash());
	}
	else if (memType.IsEnum())
	{
		PRINT_HASH("referentEnum", mem.GetReferentHash());
	}
	else if (memType.IsArray())
	{
		if (memType.IsMaxSizeArray())
		{
			PRINT_UINT("maxArrayCount", mem.GetFixedArrayCount());
		}
		if (memType.IsArrayWithExternalCount())
		{
			PRINT_UINT("arrayCounterIndex", mem.GetArrayCounterSchemaIndex());
		}
		PRINT_UINT("arrayContentsIndex", mem.GetArrayElementSchemaIndex());
		PRINT_UINT("arrayContentsAlignment", 1 << mem.GetArrayAlignmentPower());
		PRINT_UINT("arrayFlags", const_cast<psoMemberSchema&>(mem).internal_GetRawData()->Array.GetFlags());
	}
	else if (memType.IsBitset())
	{
		PRINT_HASH("bitsetNamesEnum", mem.GetBitsetEnumHash());
		PRINT_UINT("bitsetCount", mem.GetBitsetCount());
	}
	else if (memType.IsString())
	{
		PRINT_UINT("stringCount", mem.GetStringCount());
		if (memType == psoType::TYPE_NSSTRINGHASH)
		{
			PRINT_UINT("stringNamespace", mem.GetStringNamespaceIndex());
		}
	}
	else if (memType.IsMap())
	{
		PRINT_UINT("keyIndex", mem.GetKeySchemaIndex());
		PRINT_UINT("valueIndex", mem.GetValueSchemaIndex());
	}

	fprintf(m_Stream, "}");
}

void psoDebugJsonWriter::WriteStructureSchema(const psoStructureSchema& schema)
{
	fprintf(m_Stream, "{");
	const char* delim = "";
	PRINT_HASH("name", schema.GetNameHash());
	PRINT_STRING("hashtype", GetHashType(schema.GetNameHash()));
	PRINT_UINT("size", schema.GetSize());
	PRINT_UINT("version", schema.GetVersion().GetMajor());
	PRINT_UINT("signature", schema.GetSignature());
	PRINT_UINT("flags", schema.GetFlags().GetRawBlock(0));
	PRINT_UINT("alignment", schema.GetAlignment());

	fprintf(m_Stream, "%s\"members\":[", delim);
	for(int memIdx = 0; memIdx < schema.GetNumMembers(); memIdx++)
	{
		if (memIdx != 0)
		{
			fprintf(m_Stream, ",");
		}
		WriteMemberSchema(schema.GetMemberByIndex(memIdx));
	}
	fprintf(m_Stream, "]");

	fprintf(m_Stream, "}");
}

void psoDebugJsonWriter::WriteEnumValue(const psoEnumSchema& schema, int enumIdx)
{
	const char* delim = ""; 
	fprintf(m_Stream, "{");
	PRINT_HASH("name", schema.GetMemberNameHash(enumIdx));
	PRINT_INT("value", schema.GetMemberValue(enumIdx));
	fprintf(m_Stream, "}");
}

void psoDebugJsonWriter::WriteEnumSchema(const psoEnumSchema& schema)
{
	fprintf(m_Stream, "{");
	const char* delim = "";

	PRINT_HASH("name", schema.GetNameHash());
	PRINT_STRING("hashtype", GetHashType(schema.GetNameHash()));
	PRINT_UINT("signature", schema.GetSignature());
	
	fprintf(m_Stream, "%s\"enums\":[", delim);
	for(int enumIdx = 0; enumIdx < schema.GetNumEnums(); enumIdx++)
	{
		if (enumIdx != 0)
		{
			fprintf(m_Stream, ",");
		}
		WriteEnumValue(schema, enumIdx);
	}
	fprintf(m_Stream, "]");

	fprintf(m_Stream, "}");
}

void psoDebugJsonWriter::WriteSchemaCatalog(const psoSchemaCatalog& constCat)
{
	fprintf(m_Stream, "{\"schemas\":[");
	bool first = true;
	psoSchemaCatalog& cat = const_cast<psoSchemaCatalog&>(constCat);
	for(psoSchemaCatalog::StructureSchemaIterator iter = cat.BeginSchemas(); iter != cat.EndSchemas(); ++iter)
	{
		if (!first)
		{
			fprintf(m_Stream, ",");
		}
		first = false;
		WriteStructureSchema(*iter);
	}
	fprintf(m_Stream,"],\"enums\":[");
	first = true;
	for(psoSchemaCatalog::EnumSchemaIterator iter = cat.BeginEnums(); iter != cat.EndEnums(); ++iter)
	{
		if (!first)
		{
			fprintf(m_Stream, ",");
		}
		first = false;
		WriteEnumSchema(*iter);
	}
	fprintf(m_Stream, "]}");
}


#undef PRINT_STRING
#undef PRINT_UINT
#undef PRINT_INT
#undef PRINT_BOOL

psoDebugMismatchedFiles* psoDebugMismatchedFiles::sm_Instance = NULL;

psoDebugMismatchedFiles::psoDebugMismatchedFiles()
	: m_PscFileDefinitions(NULL)
#if __BANK	
	, m_RestInterface("PsoMismatch")
#endif // __BANK	
{
}

psoDebugMismatchedFiles::~psoDebugMismatchedFiles()
{
	for(atMap<atString, psoResourceData*>::Iterator iter = m_MismatchedFiles.CreateIterator(); iter; ++iter)
	{
		delete iter.GetData();
	}
	delete m_PscFileDefinitions;
}

void psoDebugMismatchedFiles::InitClass()
{
	sm_Instance = rage_new psoDebugMismatchedFiles();
#if __BANK
	REST.CreateDirectories("Parser")->AddChild(sm_Instance->m_RestInterface);
#endif // __BANK	
}

void psoDebugMismatchedFiles::ShutdownClass()
{
	// TODO: remove the rest service
	delete sm_Instance; 
}

void psoDebugMismatchedFiles::AddMismatchedFile(psoFile& file)
{
	if (m_MismatchedFiles.GetNumUsed() >= MaxMismatchedFiles)
	{
		return;
	}


	atString name(file.GetFileName());
	m_MismatchedFiles.Insert(name, BuildNewSchemasOnlyResource(*file.internal_GetResourceData(), PSO));
}

psoResourceData* psoDebugMismatchedFiles::BuildNewSchemasOnlyResource(psoResourceData& input, ResourceDataType source)
{
	switch(source)
	{
	case PSO:
		{
			psoResourceData* rsc = rage_new psoResourceData;
			input.CopySchemaDataTo(*rsc);
			return rsc;
		}
	case CPP:
		return BuildNewSchemasOnlyResourceFromParManager(input);
	case PSC:
		{
			if (!parVerifyf(m_PscFileDefinitions, "You haven't loaded any PSC definitions yet"))
			{
				return NULL;
			}
			parManager* oldMgr = parManager::sm_Instance;
			parManager::sm_Instance = m_PscFileDefinitions;
			psoResourceData* rsc = BuildNewSchemasOnlyResourceFromParManager(input);
			parManager::sm_Instance = oldMgr;
			return rsc;
		}
	}
	return NULL;
}

psoResourceData* psoDebugMismatchedFiles::BuildNewSchemasOnlyResourceFromParManager(psoResourceData& input)
{
	psoRscBuilder builder;

	psoSchemaCatalog sourceCat(input);
	for(psoSchemaCatalog::StructureSchemaIterator iter = sourceCat.BeginSchemas(); iter != sourceCat.EndSchemas(); ++iter)
	{
		atLiteralHashValue hash = (*iter).GetNameHash();
		if (psoConstants::IsReserved(hash))
		{
			continue;
		}

		parStructure* structure = PARSER.FindStructure(hash);
		if (structure)
		{
			psoBuildSchemaFromStructure(builder, *structure);
		}
		else
		{
			parWarningf("PSO file had a structure with hash 0x%08x, and that's not in the parManager", hash.GetHash());
		}
	}

	builder.FinishBuilding();
	return builder.ConstructResourceData();
}

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
void psoDebugMismatchedFiles::LoadExternalPscFiles(const char* dir)
{
	if (m_PscFileDefinitions)
	{
		return; // already loaded.
	}

	m_PscFileDefinitions = rage_new parManager;
	m_PscFileDefinitions->Initialize(parSettings::sm_StandardSettings, false);

	// Need to change the active instance temporarily
	parManager* originalMgr = parManager::sm_Instance;
	parManager::sm_Instance = m_PscFileDefinitions;
	parAddReflectionClassesToManager(*m_PscFileDefinitions);

	sysTimer t;
	parDisplayf("Loading parser schemas from %s", dir);
	PARSER.GetExternalStructureManager().LoadStructdefs(dir);
	parDisplayf("Loaded structdefs in %fs", t.GetTime());

	parManager::sm_Instance = originalMgr;
};
#endif // PARSER_USES_EXTERNAL_STRUCTURE_DEFNS


#if __BANK

restStatus::Enum psoDebugMismatchedFiles::RestInterface::GetFileSchema(restCommand& command, psoResourceData* sourceData)
{
	const char* source = command.m_Query.FindAttributeStringValue("source", "PSO", NULL, 0);
	psoDebugMismatchedFiles::ResourceDataType dataType = psoDebugMismatchedFiles::PSO;
	if (!strcmp(source, "CPP"))
	{
		dataType = psoDebugMismatchedFiles::CPP;
	}
	else if (!strcmp(source, "PSC"))
	{
		dataType = psoDebugMismatchedFiles::PSC;
	}
	else if (!strcmp(source, "PSO"))
	{
		dataType = psoDebugMismatchedFiles::PSO;
	}

	psoResourceData* resource = psoDebugMismatchedFiles::GetInstance().BuildNewSchemasOnlyResource(*sourceData, dataType);

	if (!resource)
	{
		return restStatus::REST_BAD_REQUEST;
	}


	psoDebugJsonWriter writer(*command.m_OutputStream);
	writer.WriteSchemaCatalog(psoSchemaCatalog(*resource));

	delete resource;

	return restStatus::REST_OK;
}

restStatus::Enum psoDebugMismatchedFiles::RestInterface::ProcessCommand(restCommand& command)
{
	sysMemAutoUseDebugMemory useDebug;

	if (command.m_Verb != restCommand::REST_GET)
	{
		command.ReportErrorMethodNotAllowed(restCommand::REST_GET, "PSO Mismatched file debugger is read-only");
		return restStatus::REST_METHOD_NOT_ALLOWED;
	}

	command.m_OutputType = "application/json";
	
	if (!strcmp(command.GetCurrentPathComponent(), "FileSchemas"))
	{
		command.NextPathComponent();

		if (command.IsAtEndOfPath())
		{
			// Return a list of files in JSON format
			fprintf(command.m_OutputStream, "[");
			const char* delim="";
			for(atMap<atString, psoResourceData*>::Iterator iter = psoDebugMismatchedFiles::GetInstance().m_MismatchedFiles.CreateIterator(); iter; ++iter)
			{
				fprintf(command.m_OutputStream, "%s\"%s\"", delim, iter.GetKey().c_str());
				delim = ",";
			}
			fprintf(command.m_OutputStream, "]");

			return restStatus::REST_OK;
		}

		// The rest of the API looks like this:
		// /FileSchemas/<filename>?source=PSO ?source=PSC ?source=CPP

		atString filename = atString::Join(command.m_UriComponents, command.m_UriIndex, -1, "/");
										
		psoResourceData* const * resData = psoDebugMismatchedFiles::GetInstance().m_MismatchedFiles.Access(filename);
		if (resData)
		{
			return GetFileSchema(command, *resData);
		}
	}


	return restStatus::REST_NOT_FOUND;
}
#endif // __BANK

#endif