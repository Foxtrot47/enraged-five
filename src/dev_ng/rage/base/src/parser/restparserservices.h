// 
// parser/restparserservices.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_RESTPARSERSERVICES_H 
#define PARSER_RESTPARSERSERVICES_H 

#include "diag/restservice.h"

#if !__BANK

#define parRestRegisterSingleton(...) do {} while(false)
#define parRestUnregisterSingleton(...) do {} while(false)

#endif // !__BANK

namespace rage
{
	extern restStatus::Enum parRestProcessParsableObject(parPtrToStructure object, parStructure* type, restCommand& command);
}

#if __BANK

namespace rage
{
	// PURPOSE: Call this whenever an object is at the end of its life - to make sure any associated GUIDs will be invalid afterwards
	// RETURNS: True if a known guid was deleted
	// NOTES:
	//		This is intended for any parsable objects that might have GUIDs assigned to them, and come in and out of existance
	//		at various times over the course of a game.
	extern bool parRestExpireGuid(parPtrToStructure object);

	// PURPOSE: Register a single parsable object as a REST service. (i.e. make it network browsable/editable)
	// PARAMS:
	//		svcPathToObject - the path as it should appear in a REST request. 'Directories' can be used, seperating them with '/' characters,
	//						and any non-existant directories will be created.
	//		object - a pointer to the object that you're registering
	//		type - the parStructure for the object you're registering. Normally you can get this from PARSER.GetStructure()
	// NOTES:
	//		This function is intended for making global / singleton objects editable. Things like game-wide tuning variables that get
	//		loaded once per game or once per level.
	//		The template version of this function is the same as the non-template version, but is a little more convenient, automatically
	//		discovering the parStructure object description.
	extern void parRestRegisterSingleton(const char* svcPathToObject, parPtrToStructure object, parStructure* type, const char* sourceFile);

	template<typename _Type>
	void parRestRegisterSingleton(const char* pathToObject, _Type& object, const char* sourceFile)
	{
		parRestRegisterSingleton(pathToObject, object.parser_GetPointer(), _Type::parser_GetStaticStructure(), sourceFile);
	}

	// PURPOSE: Unregister a singleton object, when it should no longer be browsable or editable
	// PARAMS:
	//		pathToObject - the path as it appeared in REST requests.
	inline void parRestUnregisterSingleton(const char* pathToObject, parPtrToStructure object) { REST.RemoveAndDeleteService(pathToObject); parRestExpireGuid(object); }

	/////////////////////////////////////////////////////
	// The parser services
	/////////////////////////////////////////////////////

	// PURPOSE: A REST service that provides access to a single parsable object.
	class parRestServiceSingleton : public restService
	{
	public:
		parRestServiceSingleton(const char* name, parPtrToStructure object, parStructure* type, const char* sourceFile);
		virtual ~parRestServiceSingleton();

		virtual restStatus::Enum ProcessCommand(restCommand& command);

		virtual const char* GetServiceType() { return "ParsableObject"; }

		virtual void PrintExtraDirInfo(fiStream* stream);

		inline parPtrToStructure GetObject();
		inline void SetObject(parPtrToStructure object);

		inline parStructure* GetType();

	private:
		parStructure*		m_Type;
		atString			m_SourceFile;
		parPtrToStructure	m_Object;
	};

	// PURPOSE: A REST service that provides GUID lookups, for any parsable object that you wanted to 
	// get a GUID for.
	class parRestParserGuidService : public restService
	{
	public:
		static void InitClass(u32 sessionId, const char* name = "GUID", const char* path = "Parser");
		static void ShutdownClass();


		virtual restStatus::Enum ProcessCommand(restCommand& command);

		virtual const char* GetServiceType() { return "ParsableObjectGUIDs"; }

		static parRestParserGuidService* GetInstance() { return sm_Instance; }

		static void SetSessionId(u32 sessionId); // This should be fairly unique - to ensure that we don't get messages that were meant for an old session.

		// Mainly for internal / debugging use.
		static u32 LookupGuidForObject(parPtrToStructure object);

		// Buf must be at least 16 chars.
		static void MakeFullGuidName(u32 guid, char* buf); 
	protected:
		parRestParserGuidService(const char* name);
		virtual ~parRestParserGuidService();

		static parRestParserGuidService* sm_Instance;
	};


#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	class parRestParserSchemaService : public restService
	{
	public:
		parRestParserSchemaService(const char* name) : restService(name) {}
		virtual ~parRestParserSchemaService() {}

		virtual restStatus::Enum ProcessCommand(restCommand& command);
		virtual const char* GetServiceType() { return "ParsableObjectGUIDs"; }
	};
#endif

	/////////////////////////////////////////////////////
	// Implementations
	/////////////////////////////////////////////////////

	parPtrToStructure parRestServiceSingleton::GetObject()
	{
		return m_Object;
	}

	void parRestServiceSingleton::SetObject(parPtrToStructure object)
	{
		m_Object = object;
	}

	parStructure* parRestServiceSingleton::GetType()
	{
		return m_Type;
	}

}

#endif // __BANK

#endif // PARSER_RESTPARSERSERVICES_H


