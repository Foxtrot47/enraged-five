// 
// parser/membermatrix.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "membermatrix.h"

#include "optimisations.h"
#include "treenode.h"

#include "parsercore/attribute.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"

PARSER_OPTIMISATIONS();

using namespace rage;

STANDARD_PARMEMBER_DATA_FUNCS_DEFN(parMemberMatrix);

void parMemberMatrix::Data::Init()
{
	StdInit();

	// identity
	m_InitAX = m_InitBY = m_InitCZ = m_InitDW = 1.0f;
	m_InitAY = m_InitAZ = m_InitAW =
		m_InitBX = m_InitBZ = m_InitBW =
		m_InitCX = m_InitCY = m_InitCW =
		m_InitDX = m_InitDY = m_InitDZ = 0.0f;

#if __BANK
	m_Min = -FLT_MAX;
	m_Max = FLT_MAX;
	m_Step = 0.01f;
	m_Description = NULL;
#endif
}


void parMemberMatrix::ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const
{
	if (!parVerifyf(node, "Can't handle NULLs - leaving member %s alone", GetName()))
	{
		return;
	}

	switch(GetType())
	{
	case parMemberType::TYPE_MATRIX34:	node->ReadStdLeafMatrix34(GetMemberFromStruct<Matrix34>(structAddr)); break;
	case parMemberType::TYPE_MATRIX44:	node->ReadStdLeafMatrix44(GetMemberFromStruct<Matrix44>(structAddr)); break;
	case parMemberType::TYPE_MAT33V: node->ReadStdLeafMat33(GetMemberFromStruct<Mat33V>(structAddr)); break;
	case parMemberType::TYPE_MAT34V: node->ReadStdLeafMat34(GetMemberFromStruct<Mat34V>(structAddr)); break;
	case parMemberType::TYPE_MAT44V: node->ReadStdLeafMat44(GetMemberFromStruct<Mat44V>(structAddr)); break;
	default:
		{
#if __NO_OUTPUT
			Quitf(ERR_PAR_INVALID_1,"Invalid type %d for matrix member", GetType());
#else
			Quitf(ERR_PAR_INVALID_1,"Invalid type %d for matrix member %s", GetType(), GetName());
#endif // __NO_OUTPUT
		}
	}
}

size_t rage::parMemberMatrix::GetSize() const
{
	return parMemberType::GetSize(GetType());
}
