// 
// parser/psofile.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOFILE_H
#define PARSER_PSOFILE_H

#include "psodata.h"
#include "psoresource.h"
#include "manager.h"

#include "atl/array.h"
#include "file/limits.h"

namespace rage {
class parSettings;
class parStructure;
class psoFile;

#if !__NO_OUTPUT
struct psoLoadStats
{
	int m_LayoutMatchingLoads; // Number of loads where the PSO layout matched the runtime layout
	int m_LayoutMismatchLoads; // Number of loads where the PSO layout did not match, and we had to do memberwise loading
	atRangeArray<parStructure*, 4> m_MismatchedStructures; // A list of mismatch structures (there may be more than this, we just record the first 4)
};

extern psoLoadStats g_psoLoadStats; // filled out after each PSO load
#endif

enum psoLoadAction
{
	PSOLOAD_NO_PREP,
	PSOLOAD_PREP_FOR_PARSER_LOADING,
	PSOLOAD_ASSUME_IN_PLACE_LOADING,
};

enum psoLoadFileStatus
{
	PSOLOAD_SUCCESS,
	PSOLOAD_COULDNT_READ_FILE,
	PSOLOAD_EMPTY,
	PSOLOAD_MISSING_REQUIRED_DATA,
	PSOLOAD_CHECKSUM_MISSING,
	PSOLOAD_CHECKSUM_MISMATCH,
	PSOLOAD_FILESIZE_MISMATCH,
	PSOLOAD_REDUNDANT_DATA,
	PSOLOAD_UNKNOWN_DATA,
};


// PURPOSE: Given a path to a PSO file, loads the file and returns a psoFile object.
// PARAMS:
//		filename - the name of the file to load (including extension)
//		storage - a pointer to a memory buffer representing a PSO
//		size - the size of the memory buffer
//		loadAction - pass in one of the enums to perform an action after loading the file
//		psoOwnsData - if true, the storage buffer will be deleted when the psoFile is deleted
// NOTES: All data in the file is preloaded and gets allocated in one contiguous block
//		The available actions are: 
//		PSOLOAD_NO_PREP - does nothing. 
//		PSOLOAD_PREP_FOR_PARSER_LOADING - use this if you intend to convert objects in the PSO into real 
//		C++ objects via the parser, it does some prep work to see which structures can be in-place loaded. 
//		PSOLOAD_ASSUME_IN_PLACE_LOADING - assumes that all structures in the PSO file can be in-place loaded
//		so it skips over the prep work. Be very careful with this - if the memory layouts don't match you'll get
//		bad data or memory stomps. Mostly just use it for files you had just loaded in-place successfully.
psoFile* psoLoadFile(const char* filename, psoLoadAction loadAction = PSOLOAD_PREP_FOR_PARSER_LOADING, atFixedBitSet32 initFlags = atFixedBitSet32(), psoLoadFileStatus* outResult = NULL);
psoFile* psoLoadFileFromBuffer(char* storage, int size, const char* OUTPUT_ONLY(debugName), psoLoadAction loadAction = PSOLOAD_PREP_FOR_PARSER_LOADING, bool psoOwnsData = false, atFixedBitSet32 initFlags = atFixedBitSet32(), psoLoadFileStatus* outResult = NULL);

// PURPOSE: Given a path to a RESOURCED pso file, loads it and returns a psoFile object
psoFile* psoLoadResource(const char* filename, const char* ext, psoLoadAction loadAction = PSOLOAD_PREP_FOR_PARSER_LOADING, atFixedBitSet32 initFlags = atFixedBitSet32(), psoLoadFileStatus* outResult = NULL);

// PURPOSE: Given a psoResourceData that was streamed in, initialize it and return a psoFile that can read data out of it
// NOTES: The PSO file just points into the psoResourceData, so don't delete the resource data until you're done with the psoFile
psoFile* psoInitFromResource(psoResourceData* resource, const char* OUTPUT_ONLY(debugFilename), psoLoadAction loadAction = PSOLOAD_PREP_FOR_PARSER_LOADING, atFixedBitSet32 initFlags = atFixedBitSet32(), psoLoadFileStatus* outResult = NULL);

#if PSO_USING_NATIVE_BIT_SIZE

// PURPOSE: Given a path to a PSO file, and a pointer to some data, load the file and create an
// instead of the type specified in the file, returning a pointer to it.
// PARAMS:
//		_Type - the declared type of the data - this may be a base class of the actual type specified in the file
//		file - The pso file to load from (loads from the root object within the file)
//		srcStruct - One specific psoStruct to load as a parsable object (also loads the children of that struct)
//		outObject - On return, will point to a newly created instance of _Type or one if its subclasses (or NULL)
// RETURNS:
//		true on success, false on error
template<typename _Type> bool psoLoadObjectPtr(psoFile& file, _Type*& outObject);
template<typename _Type> bool psoLoadObjectPtr(psoStruct& srcStruct, _Type*& outObject);

// PURPOSE: Given a path to a PSO file and a reference to an existing object, load data into that object.
// PARAMS:
//		_Type - the declared type of the data - this may be a base class of the actual type specified in the file
//		file - The pso file to load from (loads from the root object within the file)
//		srcStruct - One specific psoStruct to load as a parsable object (also loads the children of that struct)
//		outObject - On return, will point to a newly created instance of _Type or one if its subclasses (or NULL)
// RETURNS:
//		true on success, false on error
template<typename _Type> bool psoLoadObject(psoFile& file, _Type& obj);
template<typename _Type> bool psoLoadObject(psoStruct& srcStruct, _Type& obj);

// PURPOSE: Given a path to a PSO file and a reference to an existing object, reinitialize and load data into that object.
// PARAMS:
//		_Type - the type of data to load up. This may be a derived class of the actual type specified in the file
//		file - The pso file to load from (loads from the root object within the file)
//		srcStruct - One specific psoStruct to load as a parsable object (also loads the children of that struct)
//		obj - A reference to the object that will have new data loaded into it
// RETURNS:
//		true on success, false on error
template<typename _Type> bool psoInitAndLoadObject(psoFile& file, _Type& obj);
template<typename _Type> bool psoInitAndLoadObject(psoStruct& srcStruct, _Type& obj);

// PURPOSE: Loads an object using the object description in structure.
// PARAMS:
//		file - The pso file to load from (loads from the root object within the file)
//		srcStruct - One specific psoStruct to load as a parsable object (also loads the children of that struct)
//		destParStructure - The parStructure that describes the object to load.
//		obj - Pointer to the object to load.
//		verifyTypes - If false, assume the top level type in the file is compatible with obj.
// RETURNS:
//		true on success.
// NOTES:
//		Will not allocate space for the object. obj can be NULL, if parStructure contains 
//		specific memory addresses to load into.
bool psoLoadFromStructure(psoFile& file, parStructure& destParStructure, parPtrToStructure obj, bool verifyTypes = false);
bool psoLoadFromStructure(psoStruct& srcStruct, parStructure& destParStructure, parPtrToStructure obj, bool verifyTypes = false);

// PURPOSE: Loads an object using the object description in structure.
// PARAMS:
//		file - The pso file to load from (loads from the root object within the file)
//		srcStruct - One specific psoStruct to load as a parsable object (also loads the children of that struct)
//		destParStructure - The parStructure that describes the object to load - may be NULL to create whatever is specified in the srcStruct or file
//		outObject - On return, will point to a newly created instance of _Type or one if its subclasses (or NULL)
//		verifyTypes - If false, assume the top level type in the file is compatible with obj.
// RETURNS:
//		true on success.
// NOTES:
//		Allocate space for the object. obj can be NULL, if parStructure contains 
//		specific memory addresses to load into.
bool psoCreateAndLoadFromStructure(psoFile& file, parStructure* destParStructure, parPtrToStructure& outObject);
bool psoCreateAndLoadFromStructure(psoStruct& srcStruct, parStructure* destParStructure, parPtrToStructure& outObject);

struct psoLoadInPlaceResult
{
	enum Status // for psoLoadInPlace
	{
		PSOLOAD_ERROR,
		PSOLOAD_IN_PLACE,
		PSOLOAD_NOT_IN_PLACE,
	};

	psoLoadInPlaceResult();

	bool			IsOk() { return m_Status != PSOLOAD_ERROR; }

	Status				m_Status;
	parPtrToStructure	m_RootObject;
	parStructure*		m_RootObjectType;
	void*				m_InplaceBuffer;
	size_t				m_InplaceBufferSize;
};

enum psoHeap
{
	PSO_DONT_ALLOCATE,
	PSO_HEAP_CURRENT,
	PSO_HEAP_TEMP,
	PSO_HEAP_DEBUG,
};

// PURPOSE: Loads the contents of a PSO file in-place if possible
// PARAMS:
//		file - The file containing data to load in-place
//		inPlaceHeap - The heap to use for the single allocation if we can load in-placeObjecO
//		notInPlaceHeap - The heap to use for constructing individual sub-objects if we can't load in-place
// NOTES:
//		If inPlaceHeap is PSO_DONT_ALLOCATE and the file can be in-place loaded, we will not allocate any new storage for the objects
//		contained in the psoFile, and will modify the contents of the PSO file directly, changing any psoStructIds back into real pointers.
//		As a result some parts of the PSO file API will no longer work correctly on that file.
//		If notInPlaceHeap is PSO_DONT_ALLOCATE and the file can not be in-place loaded, it will not be loaded at all.
//		If the load was not successful, the result's m_Status will be PSO_ERROR
//		If the load was successful, m_Status says whether it was an in-place or not-in-place load. m_RootObject always points to
//		the root object in the file. After an in-place load m_InPlaceBuffer will point to the beginning of the memory buffer that
//		contains all the objects from the file.
psoLoadInPlaceResult psoLoadObjectInPlace(psoFile& file, psoHeap inPlaceHeap = PSO_HEAP_CURRENT, psoHeap notInPlaceHeap = PSO_HEAP_CURRENT);

#endif

// PURPOSE: A psoFile is the top level container for Parser Serialized Object data. There are two major
// sections within it, the schema catalog and the struct section. The schema catalog contains the schema 
// descriptions for all of the data types contained within the file, and the struct section contains 
// all of the data for the instances of structures in the file
class psoFile
{
public:
	// PURPOSE: Default constructor - creates a new (invalid) file
	psoFile();

	// PURPOSE: Destructor. May free the file data, if ownsData was true when initializing the file
	~psoFile();

	enum InitFlags
	{
		NO_BYTE_SWAP_DATA,
		NO_INIT_HASH_STRING,
		IGNORE_CHECKSUM,
		REQUIRE_CHECKSUM,
	};

	// PURPOSE: Initializes the file. Takes in a memory buffer and parses data out of it.
	// If ownsData is true, deleting the file will delete the buffer. Either way do not delete the 
	// buffer until you're all done reading data from the file.
	// PARAMS:
	//		memory - Pointer to the beginning of a block of memory containing the PSO data file
	//		memorySize - The size of the data file (in bytes)
	//		ownsData - If true, the file will delete the data in its destructor
	//		swapData - Byte swap the contents of the file if necessary
	// RETURNS:
	//		true if the file appears valid (all the required sections of the file are present)
	bool Init(char* memory, u32 memorySize, bool ownsData, atFixedBitSet32 initFlags = atFixedBitSet32(), psoLoadFileStatus* outResult = NULL);

	// PURPOSE: Given a psoStructId identifier for some object within this file, returns the corresponding psoStruct
	// (kind of like dereferencing a pointer)
	// PARAMS:
	//		psoStructId - a struct id that comes from somewhere in this file (you can't use struct IDs between files)
	// RETURNS:
	//		A new psoStruct object that lets you access the referenced structure. It may be null or invalid.
	inline psoStruct GetInstance(psoStructId id);

	// PURPOSE: Given a psoStructId identifier for some object within this file, return the raw data pointer
	// (for when you don't need a whole psoStruct which contains type information too)
	// PARAMS:
	//		_Type - must be a pointer type
	//		id - The id of the data you're getting
	template<typename _Type> _Type GetInstanceDataAs(psoStructId id);
	
	// RETURNS: The root level structure in this file
	inline psoStruct GetRootInstance();

	// RETURNS: The schema catalog containing all schema information in this file
	psoSchemaCatalog GetSchemaCatalog() { return psoSchemaCatalog(*m_ResourceData); }

	u16 GetNumStructArrays() { return m_ResourceData->m_NumStructArrayEntries; }
	psoRscStructArrayTableData& GetStructArrayEntry(int i) { return m_ResourceData->m_StructArrayTable[i]; }

	enum Flags
	{
		FLAG_COMPARED_SCHEMAS_WITH_RUNTIME,
		FLAG_ALL_SCHEMAS_MATCH,
		FLAG_IFF_FORMAT,
	};

	atFixedBitSet8& GetFlagsRef() { return m_Flags; }

#if !__NO_OUTPUT
	const char*	GetFileName() const { return m_FileName; }
#endif

	bool SaveFile(const char* filename);

	psoResourceData* internal_GetResourceData() { return m_ResourceData; }
	char* internal_GetStorage() { return m_Storage; }

	// psoTODO: THis shouldn't be public, clean up.
	psoLoadInPlaceResult CopyAndFixupMatchingPsoContents();
	psoLoadInPlaceResult DirectFixupMatchingPsoContents();

	bool Init(psoResourceData* resourceData, bool ownsData, bool registerStrings = true);

private:

	psoResourceData*		m_ResourceData;

	void ConvertToNewFormat(psoStructureData* instanceData, psoSchemaCatalog& schemaCatalog, psoStructureMap& structureMap, u32* signatures);

	atFixedBitSet8			m_Flags;
	char*					m_Storage;
	u32						m_StorageDataSize;
	
	char*					m_IffInstanceData;
	size_t					m_IffInstanceDataSize;

	char*					m_StructureMapData;
	size_t					m_StructureMapDataSize;

	char*					m_SchemaCatalogData;
	size_t					m_SchemaCatalogDataSize;

	char*					m_CheckSumData;
	size_t					m_CheckSumDataSize;

	char*					m_EncryptedStringData;
	u32						m_EncryptedStringDataSize;

#if !__NO_OUTPUT
	char					m_FileName[RAGE_MAX_PATH];
#endif

	// There has GOT to be a better way to do this...
	friend psoFile* psoInitFromResource(psoResourceData* resource, const char* OUTPUT_ONLY(debugFilename), psoLoadAction loadAction, atFixedBitSet32 initFlags, psoLoadFileStatus* outResult);
	friend psoFile* psoLoadFile(const char* filename, psoLoadAction loadAction, atFixedBitSet32 initFlags, psoLoadFileStatus* outResult);
	friend psoFile* psoLoadFileFromBuffer(char* storage, int size, const char* OUTPUT_ONLY(debugFilename), psoLoadAction loadAction, bool psoOwnsData, atFixedBitSet32 initFlags, psoLoadFileStatus* outResult);
	friend psoFile* psoLoadResource(const char* filename, const char* extn, psoLoadAction loadaction, atFixedBitSet32 initFlags, psoLoadFileStatus* outResult);
};

///////////////////////////////////////////////////////////////////////////////
// Implementations
///////////////////////////////////////////////////////////////////////////////

inline psoStruct psoFile::GetInstance( psoStructId id )
{
	psoStruct inst(*this);

	if (id.IsNull())
	{
		return inst;
	}
	FastAssert(id.GetTableIndex() < m_ResourceData->m_NumStructArrayEntries);

	// Find the type instance in the type table...
	psoRscStructArrayTableData& structArrayEntry = m_ResourceData->m_StructArrayTable[id.GetTableIndex()];
	FastAssert(id.GetArrayOffset() < structArrayEntry.m_Size);
	inst.m_Schema = GetSchemaCatalog().FindStructureSchema(structArrayEntry.m_NameHash);

	inst.m_InstanceData = reinterpret_cast<parPtrToStructure>(reinterpret_cast<char*>(structArrayEntry.m_Data.GetPtr()) + id.GetArrayOffset());

	return inst;
}

template<typename _Type>
_Type psoFile::GetInstanceDataAs(psoStructId id)
{
	if (id.IsNull())
	{
		return NULL;
	}

	// Find the type instance in the type table...
	psoRscStructArrayTableData& structArrayEntry = m_ResourceData->m_StructArrayTable[id.GetTableIndex()];

	return reinterpret_cast<_Type>(structArrayEntry.m_Data.GetPtr() + id.GetArrayOffset());
}

inline psoStruct psoFile::GetRootInstance()
{
	return GetInstance(m_ResourceData->m_RootObjectId);
}

#if PSO_USING_NATIVE_BIT_SIZE && !__SPU

template<typename _Type>
bool psoLoadObjectPtr(psoFile& file, _Type*& obj)
{
	parPtrToStructure newObj = NULL;
	bool result = psoCreateAndLoadFromStructure(file, _Type::parser_GetStaticStructure(), newObj);
	obj = reinterpret_cast<_Type*>(newObj);
	return result;
}

template<typename _Type>
bool psoLoadObject(psoFile& file, _Type& obj)
{
	return psoLoadFromStructure(file, *PARSER.GetStructure(&obj), obj.parser_GetPointer(), true);
}

template<typename _Type>
bool psoLoadObject(psoStruct& str, _Type& obj)
{
	return psoLoadFromStructure(str, *_Type::parser_GetStaticStructure(), obj.parser_GetPointer(), true);
}

template<typename _Type>
bool psoInitAndLoadObject(psoFile& file, _Type& obj)
{
	PARSER.InitObject(obj);
	return psoLoadObject(file, obj);
}

#endif

} // namespace rage

#endif // PARSER_PSOFILE_H
