// 
// parser/psoschemadata.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOSCHEMADATA_H
#define PARSER_PSOSCHEMADATA_H

#include "ifffile.h"
#include "psoptr.h"
#include "psotypes.h"

#include "atl/hashstring.h"
#include "data/struct.h"

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4201)
#endif

namespace rage
{
	class datResource;
	class datTypeStruct;
	class psoResourceData;

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	// psoRscMemberSchemaData
	// PURPOSE: Describes one member variable
	struct psoRscMemberSchemaData
	{
		psoRscMemberSchemaData();
		atLiteralHashValue m_NameHash;
		u32 m_Offset;

		enum MemberDataType
		{
			MEM_GENERIC,
			MEM_ENUM_OR_STRUCT,
			MEM_STRING,
			MEM_ARRAY,
			MEM_ARRAY_WITH_COUNTER,
			MEM_BITSET,
			MEM_MAP,
		};

		enum
		{
			ARRAY_ALIGN_POWER_SHIFT = 0,
			ARRAY_ALIGN_POWER_MASK = 0x1f, // that gives us alignment powers from 0 to 31
			ARRAY_FLAGS_SHIFT = 5,
			ARRAY_FLAGS_MASK = 0x7,
		};

		enum ArrayFlags {
			ARRAY_FLAG_PHYSICAL_MEMORY = 0, // Should the contents of this array be allocated in physical memory?
			ARRAY_FLAG_UNUSED1 = 1,
			ARRAY_FLAG_UNUSED2 = 2,
		};

		union
		{
			struct 
			{
				u8 m_Type;
				char m_Pad[7];
			} Generic;

			struct 
			{
				u8 m_Type;
				char m_Pad[3];
				u32 m_ReferentHash;
			} EnumOrStruct;

			struct 
			{
				u8 m_Type;
				u8 m_NamespaceIndex;
				char m_Pad[2];
				u32 m_Count; // Count of # of string elements (char or char16)
			} String;

			struct 
			{
				u8 m_Type;
				u8 m_AlignPowerAndFlags;
				u16 m_ContentsIndex;
				union 
				{
					u32 m_Count; // Count of # of array items
					struct  
					{
						u16 m_CounterIndex; // Offset in bytes
						char m_Pad[2];
					};
					u32 m_CounterOffsetForConstructionOnly; // Like the name says, only used temporarily, for construction of a StructSchema
				};


				u8 GetAlignPower() const
				{
					return (m_AlignPowerAndFlags >> ARRAY_ALIGN_POWER_SHIFT) & ARRAY_ALIGN_POWER_MASK;
				}

				u8 GetFlags() const
				{
					return (m_AlignPowerAndFlags >> ARRAY_FLAGS_SHIFT) & ARRAY_FLAGS_MASK;
				}

				void SetAlignPowerAndFlags(u8 alignPower, u8 flags)
				{
					m_AlignPowerAndFlags = ((flags & ARRAY_FLAGS_MASK) << ARRAY_FLAGS_SHIFT) | ((alignPower & ARRAY_ALIGN_POWER_MASK) << ARRAY_ALIGN_POWER_SHIFT);
				}

			} Array;

			struct
			{
				u8 m_Type;
				char m_Pad[1];
				u16 m_BitCount; // Count of # of useful bits (could be less than the # of bits for storage)
				u32 m_EnumHash;
			} Bitset;

			struct
			{
				u8 m_Type;
				char m_Pad[3];
				u16 m_KeyIndex;
				u16 m_ValueIndex;
			} Map;
		};

		psoRscMemberSchemaData(datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		static void Place(psoRscMemberSchemaData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);

		u32 ComputePartialSignature(u32 partialSig);

		static u8 FindAlignPower(size_t alignment);


		// Generally these classes should just be data stores and not have much functionality of their own
		// but there are a couple convenience functions here so we don't have to repeat ourselves too often
		inline psoType GetType() const {return psoType(Generic.m_Type); }
		inline void SetType(psoType t) { FastAssert(GetType() == psoType::TYPE_INVALID); Generic.m_Type = t.GetRaw(); } // can't change types this way
		static inline MemberDataType GetMemberDataType(psoType type);
	};

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	// psoRscStructSchemaData
	// PURPOSE: Describes a structure, contains an array of psoRscSchemaMemberDatas to
	// describe the member variables
	struct psoRscStructSchemaData
	{
		psoRscStructSchemaData();
	
		psoRscStructSchemaData(datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		static void Place(psoRscStructSchemaData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);

		u32 ComputeBaseSignature(bool considerName = false);

		psoRscMemberSchemaData* CreateMembersTable(int numMembers);

		static const u32 HashSalt = 0x6122a4e0; // arbitrary number

		atLiteralHashValue m_NameHash;
		u32 m_Signature;
		u8 m_Flags;
		u8 m_AlignPower;
		datPadding<6> m_Pad;
		psoPtr<psoRscMemberSchemaData> m_Members;
		u32 m_Size;
		u16 m_MajorVersion;
		u16 m_NumMembers;
	};
	
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	// psoRscEnumSchemaValueData
	// PURPOSE: One name value pair in an enum
	struct psoRscEnumSchemaValueData
	{
		psoRscEnumSchemaValueData() : m_Value(0) { }

		atLiteralHashValue m_NameHash;
		int m_Value;

		psoRscEnumSchemaValueData(datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		static void Place(psoRscEnumSchemaValueData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);

	};

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	// psoRscSchemaEnumData
	// PURPOSE: A table of name-value pairs for one enum
	struct psoRscEnumSchemaData
	{
		psoRscEnumSchemaData() : m_Signature(0), m_NumValues(0), m_Flags(0) {}

		psoRscEnumSchemaData(datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		static void Place(psoRscEnumSchemaData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);

		u32 ComputeBaseSignature();

		static const u32 HashSalt = 0x11788af9; // arbitrary number

		atLiteralHashValue m_NameHash;
		u32 m_Signature;
		psoPtr<psoRscEnumSchemaValueData> m_Values;
		u16 m_NumValues;
		u8 m_Flags;
		datPadding<5> m_Pad;
	};


	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	// psoRscStructArrayTableData
	// PURPOSE:
	// One entry in the StructArray table. This is an array of data all sharing the same type
	struct psoRscStructArrayTableData
	{
		psoRscStructArrayTableData() : m_Size(0) {}

		atLiteralHashValue m_NameHash;
		u32 m_Size; // Size in BYTES 
		psoPtr<char> m_Data;

		psoRscStructArrayTableData(datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		static void Place(psoRscStructArrayTableData* ptr, datResource& rsc, psoPtrNeedsSwap swap, psoPtrOriginalSize size);
	};

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	/// Old IFF classes



	struct psoTypeTableData
	{
		u32 m_TypeHash;
		u32 m_Offset;
	};

	struct psoChecksumData
	{
		parIffHeader m_IffTag;
		u32 m_Filesize;
		u32 m_Checksum;
	};

	struct psoChecksumData2 : public psoChecksumData
	{
		char m_PlatformId;
		char pad[3];

		static const u32 ChecksumSalt = 0x3fac7125;
	};

	struct psoSchemaCatalogData 
	{
		parIffHeader m_IffTag;
		u32			 m_NumTypes;

		psoTypeTableData& GetType(int i) { return reinterpret_cast<psoTypeTableData*>(this+1)[i]; }
		const psoTypeTableData& GetType(int i) const { return reinterpret_cast<const psoTypeTableData*>(this+1)[i]; }
	};


	struct psoSchemaMemberData
	{
		u32 m_NameHash;
		u8  m_Type;
		u8  m_OffsetHighBitsAndSubType; 
		u16 m_OffsetLowBits;
		union 
		{
			u32 m_ReferentHash;		// Used for ENUMs and STRUCTUREs - to refer to the enum type or struct type


			struct {				// Used for STRINGs to hold the character count, used for BITSETs to hold the bitset count and the index of the ENUM member describing the bits
				u16 m_Count;		// and used for ARRAYs to hold the array count, and the index of the member describing the array contents.
				u16 m_AlignmentAndMemberIndex; // top 4 bits are alignment power, bottom 12 are the member index
			};

			struct {				// Used for MAPs to hold the index of the member describing the keys, and the index describing the values
				u16 m_KeyIndex;
				u16 m_ValueIndex;
			};
		};

		static const int MAX_OFFSET = (1 << 18);
		static const int OFFSET_HIGH_BITS_SHIFT = 6;
		static const int OFFSET_HIGH_BITS_MASK = 0x3;
		static const int SUBTYPE_BITS_MASK = 0x3f;

		static const int MAX_ALIGMENT_POWER = 15;
		static const int ALIGNPOWER_BITS_SHIFT = 12;
		static const int MEMBERINDEX_BIT_MASK = (1 << ALIGNPOWER_BITS_SHIFT)-1;
		static const int MAX_MEMBER_INDEX = (1 << 12);

		static const int NO_MEMBER_INDEX = MAX_MEMBER_INDEX-1; // Used to indicate a non-existant member reference

		u8 GetSubType() const { return m_OffsetHighBitsAndSubType & SUBTYPE_BITS_MASK; }
		void SetSubType(u8 subtype) {
			FastAssert(subtype < (1 << OFFSET_HIGH_BITS_SHIFT));
			m_OffsetHighBitsAndSubType = (m_OffsetHighBitsAndSubType & ~SUBTYPE_BITS_MASK ) | subtype;
		}

		u32 GetOffset() const { return (((u32)m_OffsetHighBitsAndSubType >> OFFSET_HIGH_BITS_SHIFT) << 16) | (u32)m_OffsetLowBits; }
		void SetOffset(u32 offset) { 
			Assertf(offset < (u32)MAX_OFFSET, "Offset %d too large for a PSO file", offset);
			m_OffsetLowBits = (u16)offset;
			u32 offsetHighBits = (u32)offset >> 16;
			m_OffsetHighBitsAndSubType = (u8)((offsetHighBits << OFFSET_HIGH_BITS_SHIFT) | (m_OffsetHighBitsAndSubType & SUBTYPE_BITS_MASK));
		}

		u32 GetMemberIndex() const { return m_AlignmentAndMemberIndex & MEMBERINDEX_BIT_MASK; }
		void SetMemberIndex(u32 index)
		{
			Assertf(index < (u32)MAX_MEMBER_INDEX, "Index %d too large for a PSO file", index);
			m_AlignmentAndMemberIndex = (m_AlignmentAndMemberIndex  & ~MEMBERINDEX_BIT_MASK) | (u16)index;
		}

		u32 GetAlignmentPower() const { return m_AlignmentAndMemberIndex >> ALIGNPOWER_BITS_SHIFT; }
		void SetAlignmentPower(u32 power)
		{
			Assertf(power <= (u32)MAX_ALIGMENT_POWER, "Alignment power %d too large for a PSO file", power);
			m_AlignmentAndMemberIndex = (u16)((power << ALIGNPOWER_BITS_SHIFT) | (m_AlignmentAndMemberIndex & MEMBERINDEX_BIT_MASK));
		}

		bool IsEquivalentTo(const psoSchemaMemberData& other, bool considerNames, bool considerOffsets) const
		{
			if (m_Type != other.m_Type || 
				GetSubType() != other.GetSubType() ||
				m_ReferentHash != other.m_ReferentHash)
			{
				return false;
			}
			if (considerNames && (m_NameHash != other.m_NameHash))
			{
				return false;
			}
			if (considerOffsets && (GetOffset() != other.GetOffset()))
			{
				return false;
			}

			return true;
		}

	};

	struct psoSchemaStructureData
	{
		// rsTODO: Split these flags up into saved flags and runtime flags. Maybe use some of the padding for runtime flags?
		enum Flags
		{
			FLAG_USE_FAST_LOADING,
			FLAG_NEEDS_VPTR_FIXUP,
			FLAG_MAP_CONTENTS, // This structure was generated to represent the contents of a MAP 
		};

		u8 m_Type; // SCHEMA
		u8 m_Flags;
		u16 m_NumMembers;
		u32 m_Size;
		u16 m_MajorVersion;
		u16 m_Pad;

		// This object is immediately followed by a list of psoSchemaMemberDatas
		psoSchemaMemberData& GetMember(int i) { return reinterpret_cast<psoSchemaMemberData*>(this + 1)[i]; }
		const psoSchemaMemberData& GetMember(int i) const { return reinterpret_cast<const psoSchemaMemberData*>(this + 1)[i]; }
	};

	struct psoEnumTableEntry
	{
		u32 m_NameHash;
		int m_Value;
	};

	struct psoSchemaEnumData
	{
		u8 m_Type; // ENUM
		u8 m_Flags;
		u16 m_NumEnums;

		// This object is immediately followed by a list of psoEnumTableEntrys
		psoEnumTableEntry& GetEnum(int i) { return reinterpret_cast<psoEnumTableEntry*>(this+1)[i]; }
		const psoEnumTableEntry& GetEnum(int i) const { return reinterpret_cast<const psoEnumTableEntry*>(this+1)[i]; }
	};


	namespace psoIffToRscConversion
	{
		void ConvertToNewFormat(const psoSchemaMemberData& oldMember, const psoSchemaStructureData& oldStructure, psoRscMemberSchemaData& newMember, char sourcePlatform);
		void ConvertToNewFormat(const psoSchemaStructureData& oldStructure, atLiteralHashValue nameHash, psoRscStructSchemaData& newStructure, char sourcePlatform);
		void ConvertToNewFormat(const psoSchemaEnumData& oldEnum, atLiteralHashValue nameHash, psoRscEnumSchemaData& newEnum);
		void ConvertToNewFormat(const psoSchemaCatalogData& schemaCatalog, psoResourceData& newData, char sourcePlatform);
	}

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	// Inline definitions
	psoRscMemberSchemaData::MemberDataType psoRscMemberSchemaData::GetMemberDataType(psoType type)
	{
		// Would this be faster as a trait lookup?
		switch(type.GetCategory())
		{
		case psoType::CATEGORY_CORE:
			return (type == psoType::TYPE_STRUCT) ? MEM_ENUM_OR_STRUCT : MEM_GENERIC;
		case psoType::CATEGORY_INTEGER:
		case psoType::CATEGORY_FLOAT:
		case psoType::CATEGORY_VECTOR:
			return MEM_GENERIC;
		case psoType::CATEGORY_STRING:
			return MEM_STRING;
		case psoType::CATEGORY_ARRAY:
			return type.IsArrayWithExternalCount() ? MEM_ARRAY_WITH_COUNTER : MEM_ARRAY;
		case psoType::CATEGORY_ENUM:
			return type.IsBitset() ? MEM_BITSET : MEM_ENUM_OR_STRUCT;
		case psoType::CATEGORY_COLLECTIONS:
			FastAssert(type.IsMap());
			return MEM_MAP;
		}
		return MEM_GENERIC;
	}
} // namespace rage

#if __WIN32
#pragma warning(pop)
#endif

#endif // PARSER_PSOSCHEMADATA_H
