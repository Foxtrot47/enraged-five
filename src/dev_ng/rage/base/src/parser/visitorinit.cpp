// 
// parser/visitorinit.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "visitorinit.h"

#include "optimisations.h"

#include "math/amath.h"
#include "math/float16.h"
#include "math/nan.h"

#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/boolv.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"
#include "vectormath/scalarv.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"

PARSER_OPTIMISATIONS();

using namespace rage;

void parInitVisitor::VisitMember(parPtrToStructure containingStructPtr, parMember& metadata)
{
	if (metadata.CanInit())
	{
		parInstanceVisitor::VisitMember(containingStructPtr, metadata);
	}
	else if (m_Mode == COMPARE)
	{
		FailComparison();
	}
}

// Scalars
template<> bool parInitVisitor::IsEqual(const float& a, const float& b) { return fabsf(a - b) < SMALL_FLOAT; }
template<> bool parInitVisitor::IsEqual(const double& a, const double& b) { return abs(a - b) < VERY_SMALL_FLOAT; }
// Vectors
template<> bool parInitVisitor::IsEqual(const ScalarV& a, const ScalarV& b) { return IsCloseAll(a, b, ScalarV(V_FLT_SMALL_6)) > 0; }
template<> bool parInitVisitor::IsEqual(const Vec2V& a, const Vec2V& b) { return IsCloseAll(a, b, ScalarV(V_FLT_SMALL_6)) > 0; }
template<> bool parInitVisitor::IsEqual(const Vec3V& a, const Vec3V& b) { return IsCloseAll(a, b, ScalarV(V_FLT_SMALL_6)) > 0; }
template<> bool parInitVisitor::IsEqual(const Vec4V& a, const Vec4V& b) { return IsCloseAll(a, b, ScalarV(V_FLT_SMALL_6)) > 0; }
template<> bool parInitVisitor::IsEqual(const QuatV& a, const QuatV& b) { return IsCloseAll(a, b, ScalarV(V_FLT_SMALL_6)) > 0; }
template<> bool parInitVisitor::IsEqual(const BoolV& a, const BoolV& b) { return (a == b).Getb(); }
template<> bool parInitVisitor::IsEqual(const VecBoolV& a, const VecBoolV& b) { return Vec::V4IsEqualAll(a.GetIntrin128(), b.GetIntrin128()) > 0; }
// Matrices
template<> bool parInitVisitor::IsEqual(const Mat34V& a, const Mat34V& b) { return IsCloseAll(a, b, ScalarV(V_FLT_SMALL_6)) > 0; }
template<> bool parInitVisitor::IsEqual(const Mat33V& a, const Mat33V& b) { return IsCloseAll(a, b, ScalarV(V_FLT_SMALL_6)) > 0; }
template<> bool parInitVisitor::IsEqual(const Mat44V& a, const Mat44V& b) { return IsCloseAll(a, b, ScalarV(V_FLT_SMALL_6)) > 0; }


void parInitVisitor::SimpleMember(parPtrToMember dataPtr, parMemberSimple& metadata)
{
	switch(metadata.GetType())
	{
	case parMemberType::TYPE_FLOAT:		InitValue(*reinterpret_cast<float*>(dataPtr), (float)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_FLOAT16:	InitValue(*reinterpret_cast<Float16*>(dataPtr), Float16((float)metadata.GetData()->m_Init)); break;
	case parMemberType::TYPE_CHAR:		InitValue(*reinterpret_cast<s8*>(dataPtr), (s8)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_UCHAR:		InitValue(*reinterpret_cast<u8*>(dataPtr), (u8)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_SHORT:		InitValue(*reinterpret_cast<s16*>(dataPtr), (s16)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_USHORT:	InitValue(*reinterpret_cast<u16*>(dataPtr), (u16)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_INT:		InitValue(*reinterpret_cast<s32*>(dataPtr), (s32)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_UINT:		InitValue(*reinterpret_cast<u32*>(dataPtr), (u32)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_PTRDIFFT:	InitValue(*reinterpret_cast<ptrdiff_t*>(dataPtr), (ptrdiff_t)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_SIZET:		InitValue(*reinterpret_cast<size_t*>(dataPtr), (size_t)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_BOOL:		InitValue(*reinterpret_cast<bool*>(dataPtr), fabsf((float)metadata.GetData()->m_Init) > 0.001f); break;
	case parMemberType::TYPE_SCALARV:	InitValue(*reinterpret_cast<ScalarV*>(dataPtr), ScalarV((float)metadata.GetData()->m_Init)); break;
	case parMemberType::TYPE_BOOLV:     InitValue(*reinterpret_cast<BoolV*>(dataPtr), fabsf((float)metadata.GetData()->m_Init) > 0.001f ? BoolV(V_TRUE) : BoolV(V_FALSE)); break;

	// rsTODO: for all >32 bit types, use a flag to indicate that we should look in the extra attributes for the init values
	// (also do this for large ints and uints that can't be held in the m_Init float)
	case parMemberType::TYPE_INT64:		InitValue(*reinterpret_cast<s64*>(dataPtr), (s64)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_UINT64:	InitValue(*reinterpret_cast<u64*>(dataPtr), (u64)metadata.GetData()->m_Init); break;
	case parMemberType::TYPE_DOUBLE:	InitValue(*reinterpret_cast<double*>(dataPtr), metadata.GetData()->m_Init); break;
	default:
		{
			Quitf(ERR_PAR_INVALID_4,"Invalid type for simple member");
		}
	}
}

void parInitVisitor::VectorMember(parPtrToMember dataPtr, parMemberVector& metadata)
{
#if __INIT_NAN
	float nan;
	MakeNan(nan);
#endif

	switch(metadata.GetType())
	{
	case parMemberType::TYPE_VECBOOLV:	
		{
			VecBoolV initValue;
			BoolV t(V_TRUE);
			BoolV f(V_FALSE);
			initValue.SetX(fabsf(metadata.GetData()->m_InitX) > 0.001f ? t : f);
			initValue.SetY(fabsf(metadata.GetData()->m_InitY) > 0.001f ? t : f);
			initValue.SetZ(fabsf(metadata.GetData()->m_InitZ) > 0.001f ? t : f);
			initValue.SetW(fabsf(metadata.GetData()->m_InitW) > 0.001f ? t : f);
			InitValue(*reinterpret_cast<VecBoolV*>(dataPtr), initValue);
		}
		break;
	case parMemberType::TYPE_VEC2V:		InitValue(*reinterpret_cast<Vec2V*>(dataPtr), Vec2V(metadata.GetData()->m_InitX, metadata.GetData()->m_InitY)); break;
	case parMemberType::TYPE_VEC3V:		InitValue(*reinterpret_cast<Vec3V*>(dataPtr), Vec3V(metadata.GetData()->m_InitX, metadata.GetData()->m_InitY, metadata.GetData()->m_InitZ)); break;
	case parMemberType::TYPE_VEC4V:		InitValue(*reinterpret_cast<Vec4V*>(dataPtr), Vec4V(metadata.GetData()->m_InitX, metadata.GetData()->m_InitY, metadata.GetData()->m_InitZ, metadata.GetData()->m_InitW)); break;
	case parMemberType::TYPE_VECTOR2:	InitValue(*reinterpret_cast<Vector2*>(dataPtr), Vector2(metadata.GetData()->m_InitX, metadata.GetData()->m_InitY)); break;
#if __INIT_NAN
	case parMemberType::TYPE_VECTOR3:	InitValue(*reinterpret_cast<Vector4*>(dataPtr), Vector4(metadata.GetData()->m_InitX, metadata.GetData()->m_InitY, metadata.GetData()->m_InitZ, nan)); break;
#else
	case parMemberType::TYPE_VECTOR3:	InitValue(*reinterpret_cast<Vector3*>(dataPtr), Vector3(metadata.GetData()->m_InitX, metadata.GetData()->m_InitY, metadata.GetData()->m_InitZ)); break;
#endif
	case parMemberType::TYPE_VECTOR4:	InitValue(*reinterpret_cast<Vector4*>(dataPtr), Vector4(metadata.GetData()->m_InitX, metadata.GetData()->m_InitY, metadata.GetData()->m_InitZ, metadata.GetData()->m_InitW)); break;
	default:
		{
			Quitf(ERR_PAR_INVALID_5,"Invalid type for vector member");
		}
	}
}

void parInitVisitor::MatrixMember(parPtrToMember dataPtr, parMemberMatrix& metadata)
{
#if __INIT_NAN
	float nan;
	MakeNan(nan);
#endif

	switch(metadata.GetType())
	{
		case parMemberType::TYPE_MAT33V:
		{
			Mat33V initMatrix = Mat33V(V_ROW_MAJOR,
				metadata.GetData()->m_InitAX, metadata.GetData()->m_InitAY, metadata.GetData()->m_InitAZ,
				metadata.GetData()->m_InitBX, metadata.GetData()->m_InitBY, metadata.GetData()->m_InitBZ,
				metadata.GetData()->m_InitCX, metadata.GetData()->m_InitCY, metadata.GetData()->m_InitCZ);
			InitValue(*reinterpret_cast<Mat33V*>(dataPtr), initMatrix);
			break;
		}
		case parMemberType::TYPE_MAT34V:
			{
				Mat34V initMatrix = Mat34V(V_ROW_MAJOR,
					metadata.GetData()->m_InitAX, metadata.GetData()->m_InitAY, metadata.GetData()->m_InitAZ, metadata.GetData()->m_InitAW,
					metadata.GetData()->m_InitBX, metadata.GetData()->m_InitBY, metadata.GetData()->m_InitBZ, metadata.GetData()->m_InitBW,
					metadata.GetData()->m_InitCX, metadata.GetData()->m_InitCY, metadata.GetData()->m_InitCZ, metadata.GetData()->m_InitCW);
				InitValue(*reinterpret_cast<Mat34V*>(dataPtr), initMatrix);
			}
			break;
	case parMemberType::TYPE_MATRIX34:
#if __INIT_NAN
		{
			Mat44V initMatrix = Mat44V(
				metadata.GetData()->m_InitAX, metadata.GetData()->m_InitAY, metadata.GetData()->m_InitAZ, nan,
				metadata.GetData()->m_InitBX, metadata.GetData()->m_InitBY, metadata.GetData()->m_InitBZ, nan,
				metadata.GetData()->m_InitCX, metadata.GetData()->m_InitCY, metadata.GetData()->m_InitCZ, nan,
				metadata.GetData()->m_InitDX, metadata.GetData()->m_InitDY, metadata.GetData()->m_InitDZ, nan); 
			InitValue(*reinterpret_cast<Mat44V*>(dataPtr), initMatrix);
		}
		break;
#else
			{
				Mat34V initMatrix = Mat34V(
					metadata.GetData()->m_InitAX, metadata.GetData()->m_InitAY, metadata.GetData()->m_InitAZ,
					metadata.GetData()->m_InitBX, metadata.GetData()->m_InitBY, metadata.GetData()->m_InitBZ,
					metadata.GetData()->m_InitCX, metadata.GetData()->m_InitCY, metadata.GetData()->m_InitCZ,
					metadata.GetData()->m_InitDX, metadata.GetData()->m_InitDY, metadata.GetData()->m_InitDZ); 
				InitValue(*reinterpret_cast<Mat34V*>(dataPtr), initMatrix);
			}
			break;
#endif
	case parMemberType::TYPE_MATRIX44:
		{
			Mat44V initMatrix = Mat44V(
				metadata.GetData()->m_InitAX, metadata.GetData()->m_InitAY, metadata.GetData()->m_InitAZ, metadata.GetData()->m_InitAW,
				metadata.GetData()->m_InitBX, metadata.GetData()->m_InitBY, metadata.GetData()->m_InitBZ, metadata.GetData()->m_InitBW,
				metadata.GetData()->m_InitCX, metadata.GetData()->m_InitCY, metadata.GetData()->m_InitCZ, metadata.GetData()->m_InitCW,
				metadata.GetData()->m_InitDX, metadata.GetData()->m_InitDY, metadata.GetData()->m_InitDZ, metadata.GetData()->m_InitDW);
			InitValue(*reinterpret_cast<Mat44V*>(dataPtr), initMatrix);
		}
		break;

		default:
		{
			Quitf(ERR_PAR_INVALID_6,"Invalid type for matrix member");
		}
	}
}

void parInitVisitor::EnumMember(int& data, parMemberEnum& metadata)
{
	int initValue = static_cast<int>(metadata.GetData()->m_Init);
	InitValue(data, initValue);
}

void parInitVisitor::BitsetMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*ptrToBits*/, size_t /*numBits*/, parMemberBitset& metadata)
{
	switch (m_Mode)
	{
	case rage::parInitVisitor::WRITE:
		metadata.ClearBits(m_ContainingStructureAddress);
		metadata.SetFromInt(m_ContainingStructureAddress, metadata.GetData()->m_Init);
		break;
	case rage::parInitVisitor::COMPARE:
		parPtrToArray outAddr;
		u32 numBits;
		metadata.GetBitsetContentsAndCountFromStruct(m_ContainingStructureAddress, outAddr, numBits);
		int sizeBytes = metadata.GetBlockSize() * metadata.GetNumBlocks(m_ContainingStructureAddress);
		if (sizeBytes <= sizeof(metadata.GetData()->m_Init))
		{
			const u8* currentBits = reinterpret_cast<const u8*>(outAddr);
			const u8* initBits = (const u8*)&metadata.GetData()->m_Init;
			CheckComparisonResult(memcmp(currentBits, initBits, sizeBytes) == 0);
		}
		else
		{
			FailComparison();
		}
		break;
	}
}
