// 
// parser/memberenum.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBERENUM_H
#define PARSER_MEMBERENUM_H

#include "member.h"

#include "memberenumdata.h"

namespace rage {
	// PURPOSE: A parMember subclass for describing an enumerated type
	class parMemberEnum : public parMember
	{
	public:
		typedef parMemberEnumSubType::Enum SubType;
		typedef parMemberEnumData Data;

		parMemberEnum() {}
		explicit parMemberEnum(Data& data) : parMember(&data) {}
		virtual ~parMemberEnum();

		virtual void ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const;

		// RETURNS: value corresponding to name. -1 if name isn't found
		// PARAMS:
		//	name - The name value to search for
		//	nameEndOut - If non-NULL, it gets the address of the end of the token in the 'name' string.
		//  result - If a result pointer is passed it, it gets filled out with what type of string we read
		int ValueFromName(const char* name, const char** nameEndOut = NULL, parEnumData::ValueFromNameStatus* result = NULL) const;

		// RETURNS: value corresponding to hashed name. -1 if name isn't found
		// PARAMS:
		//	namehash - The name hash value to search for (use atDataHash if converting from a string)
		//  result - If a result pointer is passed it, it gets filled out with what type of string we read
		int ValueFromName(u32 namehash, parEnumData::ValueFromNameStatus* result = NULL) const;

		// RETURNS: name corresponding to value i. NULL if not found
#if PARSER_ALL_METADATA_HAS_NAMES
		const char* NameFromValue(int i) const;
#endif
		// RETURNS: The name corresponding to value i, but MAY NOT WORK if the structure doesn't have strings (only hashes)
		// NOTES: You have to check the enum data to see if there are strings or not.
		const char* NameFromValueUnsafe(int i) const;

		// RETURNS: namehash corresponding to value i. 0 if not found
		u32 HashFromValue(int val) const;

		void SetFromString(parPtrToStructure containingStructureAddr, const char* newVal) const;

		virtual size_t GetSize() const;
		virtual size_t FindAlign() const;

		int GetValue(parPtrToStructure structAddr) const;

		parEnumData* GetEnum();
	public:

		STANDARD_PARMEMBER_DATA_FUNCS_DECL(parMemberEnum);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
		PAR_PARSABLE;
#endif
	};

	class atString;

	// PURPOSE: A parMember subclass describing a fixed- or variable-length bitset
	class parMemberBitset : public parMemberEnum
	{
	public:
		typedef parMemberBitsetSubType::Enum SubType;

		parMemberBitset() {}
		explicit parMemberBitset(parMemberEnum::Data& data) : parMemberEnum(data) {}
		virtual ~parMemberBitset() {};

		virtual void ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const;

		virtual size_t GetSize() const;
		virtual size_t FindAlign() const;

		void SetFromString(parPtrToStructure containingStructureAddr, const char* newVal) const;
		void SetFromInt(parPtrToStructure containingStructureAddr, u32 newVal) const;

		void SetBit(parPtrToStructure containingStructureAddr, int bitToSet, bool value=true) const;
		bool GetBit(parPtrToStructure containingStructureAddr, int bitToGet) const;
		void ClearBits(parPtrToStructure containingStructureAddr) const;

		// PURPOSE: given the base address of a structure containing a bitset, get the
		// address of the actual bits, and the number of items to expect there.
		// NOTES:
		//		Underlying storage for the bits could be a u8, u16, or u32 array
		void GetBitsetContentsAndCountFromStruct(parPtrToStructure containingStructureAddr, parPtrToArray& outAddr, u32& outBits) const;

		// Note! Not using the STANDARD_PARMEMBER_ macros here because we want to inherit the Data function and Subtype

		SubType			GetSubtype() const {return (SubType)GetCommonData()->m_Subtype;}

		int GetNumBlocks(parPtrToStructure containingStructureAddr) const;
		int	GetBlockSize() const; // Returns the size of a block in bytes
        int	GetBitsPerBlock() const; // Returns the number of bits per block

		enum StringReprStatus
		{
			SR_EMPTY,
			SR_ALL_NAMED,
			SR_NONE_NAMED,
			SR_MIXED
		};

		// Returns false if no special string representation is necessary (i.e. if there are no named bits set)
		StringReprStatus GenerateStringRepr(parPtrToStructure containingStructureAddr, rage::atString& stringOut, int namesPerLine = 6, int intsPerLine = 4, int indent = 0) const;

		bool IsFixedSize() const {return GetSubtype() != parMemberBitsetSubType::SUBTYPE_ATBITSET;}

		bool AreBitsNamed() const { return GetData()->m_EnumData && GetData()->m_EnumData != &parEnumData::Empty; }

	protected:
		void UnionBlock(parPtrToStructure containingStructureAddr, int blockToSet, u32 blockValue) const; // unions ('or's) bits into a block

		template<typename _Type> void SetFromNumericArray(parPtrToStructure containingStructureAddr, const char* data, u32 size, int encoding, const char* nodeName) const;

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
		PAR_PARSABLE;
#endif
	};

	// This can't be defined until the derived class is.
	inline parMemberEnum*	parMember::AsEnum()			{ return parMemberType::IsEnum(GetType()) ? smart_cast<parMemberEnum*>(this) : NULL; }
	inline parMemberBitset*	parMember::AsBitset()	{ return parMemberType::IsBitset(GetType()) ? smart_cast<parMemberBitset*>(this) : NULL; }

	inline int parMemberEnum::ValueFromName( const char* name, const char** nameEndOut /*= NULL*/, parEnumData::ValueFromNameStatus* result /*= NULL*/ ) const
	{
		parAssertf(GetData()->m_EnumData, "Enum member %s doesn't point to an enum table", GetNameUnsafe());
		return GetData()->m_EnumData->ValueFromName(name, nameEndOut, result);
	}

	inline int parMemberEnum::ValueFromName( u32 namehash, parEnumData::ValueFromNameStatus* result /*= NULL*/ ) const
	{
		parAssertf(GetData()->m_EnumData, "Enum member %s doesn't point to an enum table", GetNameUnsafe());
		return GetData()->m_EnumData->ValueFromName(namehash, result);
	}

	inline const char* parMemberEnum::NameFromValueUnsafe( int i ) const
	{
		parAssertf(GetData()->m_EnumData, "Enum member %s doesn't point to an enum table", GetNameUnsafe());
		return GetData()->m_EnumData->NameFromValueUnsafe(i);
	}

#if PARSER_ALL_METADATA_HAS_NAMES
	inline const char* parMemberEnum::NameFromValue( int i ) const { return NameFromValueUnsafe(i); }
#endif

	inline u32 parMemberEnum::HashFromValue( int val ) const
	{
		parAssertf(GetData()->m_EnumData, "Enum member %s doesn't point to an enum table", GetNameUnsafe());
		return GetData()->m_EnumData->HashFromValue(val);
	}

}

#endif
