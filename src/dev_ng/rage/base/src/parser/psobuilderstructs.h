// 
// parser/psobuilderstructs.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_PSOBUILDERSTRUCTS_H
#define PARSER_PSOBUILDERSTRUCTS_H

#include "atl/array.h"
#include "atl/map.h"

namespace rage
{
class parIffHeader;
class psoBuilder;
class psoBuilderStructSchema;
class psoBuilderEnumSchema;
class psoBuilderInstance;
struct psoSchemaCatalogData;
struct psoStructureData;
struct psoStructureMapData;

// PURPOSE: The location of this object inside a PSO file
struct psoFileLocation
{
	u32 m_Offset;
	u32	m_Size;

	psoFileLocation() : m_Offset(0), m_Size(0) {}
};

struct psoBuilderSchemaCatalog
{
	atArray<psoBuilderStructSchema*> m_StructSchemas;
	atArray<psoBuilderEnumSchema*> m_EnumSchemas;
	psoFileLocation m_Location;
	psoFileLocation m_SignatureLocation;

	psoBuilderSchemaCatalog() {}
	~psoBuilderSchemaCatalog();

	u32 ComputeSize();

	void ComputeFileLocations();
	size_t Serialize(psoBuilder& builder, char* data, psoSchemaCatalogData*& outSchemaCat, parIffHeader*& outSignatures);
};

struct psoBuilderInstanceSet
{
	psoFileLocation m_Location;
	atArray<psoBuilderInstance*> m_Instances;
	size_t m_Size;

	psoBuilderInstanceSet() : m_Size(0) {}
	~psoBuilderInstanceSet();

	static const int STRUCTARRAY_IFF_SPLIT_SIZE = 512 * 1024; // Split at 512k. We should be able to handle up to 1024k, but just in case there's any problems...

	bool CanHold(size_t sizeInBytes) { return sizeInBytes < (STRUCTARRAY_IFF_SPLIT_SIZE - m_Size); }

	u32 ComputeSize();

	u32 ComputeFileLocations(u32 prevOffset, u32 tableIndex); // This fills out the locations _and_ determines the psoStructIds for all the instances
	void Serialize(char* data);
};

struct psoBuilderInstanceDataCatalog
{
	psoBuilderInstance*		m_RootObject;
	atMap<u32, const char*> m_HashStrings;
	atMap<u32, const char*> m_DebugHashStrings;
	u32	m_InstanceSize;
	u32 m_MapSize;
	u32 m_HashStringSize, m_DebugHashStringSize;

	psoBuilderInstanceDataCatalog();
	~psoBuilderInstanceDataCatalog();

	psoBuilderInstanceSet& FindOrCreateInstanceSet(u32 namehashOrPodType, size_t size, size_t align, bool useNonNativeAlignment);

	u32 ComputeSize();

	void ComputeFileLocations();
	size_t SerializeInstances(char* data, psoStructureData*& outInstances);
	size_t SerializeMap(char* data, psoStructureMapData*& outMap);
	size_t SerializeHashStrings(char* data);

private:
	// Non-native alignment is tricky because of the way we normally pack data together into 
	// "StructArrays". I.e. big arrays that contain all instances of a given type. Then doing fixups
	// is easy, we just step from one item to the next in the PSO file until we hit the end of the StructArray.
	// When we have non-native alignment this is harder. Imagine we are doing fixups on a 16b aligned, 4b object.
	// If we have two instances of those objects, they need to be 16b apart. Worse, if we have 2 _arrays_ of instances
	// the array elements are only 4b apart but the arrays are 16b apart. Putting these into a StructArray would
	// leave gaps and we don't want to fix up data that's in these gaps. So instead we'll put each non-natively
	// aligned structure into its own structarray (which here corresponds to a psoBuilderInstanceSet)

	struct NonNativeAlignedInstance 
	{
		NonNativeAlignedInstance() : m_Instances(NULL) {}
		~NonNativeAlignedInstance() { }
		u32 m_Type;
		psoBuilderInstanceSet* m_Instances; // This "set" should just have one instance in it
		u32 m_Alignment;
	};

	atMap<u32, atArray<psoBuilderInstanceSet*> > m_Instances;
	atArray<NonNativeAlignedInstance> m_NonNativeAlignedInstances; 
};

} // namespace rage


#endif

