// 
// parser/visitorinit.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_VISITORINIT_H
#define PARSER_VISITORINIT_H

#include "visitor.h"

namespace rage
{

// PURPOSE: A visitor for initializing every member in a parsable structure.
// Pointers will get NULLed when possible, and other members will be set to their
// init value, if one was specified in the structdef.
class parInitVisitor : public parInstanceVisitor
{
public:
	enum parInitMode : u8
	{
		WRITE,
		COMPARE,
	};
private:
	parInitMode m_Mode;
	bool m_HasNonDefaultMembers;

	void FailComparison()
	{
		m_HasNonDefaultMembers = true;
		EndTraversal();
	}

	void CheckComparisonResult(bool result)
	{
		if (!result)
			FailComparison();
	}

	void DefaultStringMember(parMemberString& metadata)
	{
		const char* initString = metadata.GetInitValue();
		switch (m_Mode)
		{
		case WRITE:
			if (initString)
			{
				metadata.SetFromString(m_ContainingStructureAddress, initString);
			}
			break;
		case COMPARE:
			const char* valueString = metadata.GetStringContentsFromStruct(m_ContainingStructureAddress);
			bool isInitEmpty = initString == nullptr || initString[0] == '\0';
			bool isValueEmpty = valueString == nullptr || valueString[0] == '\0';
			CheckComparisonResult((isInitEmpty || isValueEmpty) ? (isInitEmpty == isValueEmpty) : (strcmp(valueString, initString) == 0));
			break;
		}
	}

	template <class TValueA, class TValueB>
	inline bool IsEqual(const TValueA& valueA, const TValueB& valueB)
	{
		return (valueA == valueB);
	}

	template <class TValueOut, class TValueIn>
	inline void InitValue(TValueOut& outValue, const TValueIn& initValue)
	{
		switch (m_Mode)
		{
		case WRITE:
			outValue = initValue;
			break;
		case COMPARE:
			CheckComparisonResult(IsEqual(outValue, initValue));
			break;
		}
	}

public:
	parInitVisitor(parInitMode mode = WRITE) : m_Mode(mode), m_HasNonDefaultMembers(false) {}
	virtual ~parInitVisitor() {}

	// Only calls the base class VisitMember (which then calls the *Member() functions)
	// if CanInit is true.
	virtual void VisitMember(parPtrToStructure containingStructPtr, parMember& metadata);

	virtual void SimpleMember(parPtrToMember dataPtr, parMemberSimple& member);
	virtual void VectorMember(parPtrToMember dataPtr, parMemberVector& member);
	virtual void MatrixMember(parPtrToMember dataPtr, parMemberMatrix& member);
	virtual void EnumMember(int& data, parMemberEnum& metadata);
	virtual void BitsetMember(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata);

	virtual void ExternalPointerMember(void*& ptr, parMemberStruct&)
	{
		InitValue(ptr, (void*)NULL);
	}

	virtual bool BeginPointerMember(parPtrToStructure& ptr, parMemberStruct&)
	{
		InitValue(ptr, (parPtrToStructure)NULL);
		return false;
	}

	virtual void StringMember(parPtrToMember /*ptrToMember*/,	const char* /*ptrToString*/, parMemberString& metadata)
	{
		DefaultStringMember(metadata);
	}

	virtual void WideStringMember(parPtrToMember /*ptrToMember*/, const char16* /*ptrToString*/, parMemberString& metadata)
	{
		DefaultStringMember(metadata);
	}

	virtual void AtPartialHashValueMember(u32& stringData, parMemberString& metadata)
	{
		u32 initData = metadata.GetInitValue() ? atPartialStringHash(metadata.GetInitValue(), 0) : 0;
		InitValue(stringData, initData);
	}

	virtual void AtHashValue16UMember(u16& stringData, parMemberString& metadata)
	{
		u16 initData = metadata.GetInitValue() ? atHash16U(metadata.GetInitValue()) : 0;
		InitValue(stringData, initData);
	}

	virtual void PointerStringMember(char*& stringData, parMemberString& metadata)
	{
		InitValue(stringData, (char*)NULL);
		DefaultStringMember(metadata);
	}

	virtual void WidePointerStringMember(char16*& stringData, parMemberString& metadata)
	{
		InitValue(stringData, (char16*)NULL);
		DefaultStringMember(metadata);
	}	

	bool FindInitHashValue(parMemberString& metadata, u32& hashVal)
	{
		// First check for an init hash
		parAttributeList* attrs = metadata.GetExtraAttributes();
		if (attrs) 
		{
			parAttribute* initAttr = attrs->FindAttribute("initHashValue");
			if (initAttr)
			{
				hashVal = (u32)initAttr->GetIntValue();
				return true;
			}
		}
		return false;
	}

#if !__FINAL
	virtual void AtNonFinalHashStringMember(atHashString& stringData, parMemberString& metadata)
	{
		u32 hash = 0;
		if (FindInitHashValue(metadata, hash))
		{
			InitValue(stringData, hash);
		}
		else
		{
			// Fallback behavior (calls StringMember)
			parInstanceVisitor::AtNonFinalHashStringMember(stringData, metadata);
		}
	}
#endif

	virtual void AtHashValueMember		(atHashValue& stringData, parMemberString& metadata) 
	{
		u32 hash = 0;
		if (FindInitHashValue(metadata, hash))
		{
			InitValue(stringData, hash);
		}
		else
		{
			// Fallback behavior (calls StringMember)
			parInstanceVisitor::AtHashValueMember(stringData, metadata);
		}
	}

	virtual bool BeginArrayMember(parPtrToMember ptrToMember, parPtrToArray /*arrayContents*/, size_t numElements, parMemberArray& metadata)
	{
		using namespace parMemberArraySubType;

		if (m_Mode == COMPARE)
		{
			CheckComparisonResult(numElements == 0);
		}

		switch(metadata.GetSubtype())
		{
		case SUBTYPE_POINTER:
			InitValue(*reinterpret_cast<void**>(ptrToMember), (void*)NULL);
			return false;
		case SUBTYPE_POINTER_WITH_COUNT:
		case SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
		case SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
			InitValue(*reinterpret_cast<void**>(ptrToMember), (void*)NULL);
			switch (m_Mode)
			{
			case WRITE:
				metadata.SetCountForPointerWithCount(m_ContainingStructureAddress, 0);
				break;
			case COMPARE:
				CheckComparisonResult(metadata.GetCountFromPointerWithCount(m_ContainingStructureAddress) == 0);
				break;
			}
			break;
		default:
			break;
		}
		return true;
	}

	bool HasNonDefaultMembers() const
	{
		return m_HasNonDefaultMembers;
	}
};

}

#endif
