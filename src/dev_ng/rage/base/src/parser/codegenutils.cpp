// 
// parser/codegenutils.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "codegenutils.h"

#include "distructure.h"
#include "manager.h"
#include "macros.h"
#include "optimisations.h"

#include "atl/delegate.h"

PARSER_OPTIMISATIONS();

using namespace rage;

parCguAutoRegistrationNode* parCguAutoRegistrationNode::sm_Head;

#if 0
void rage::parCguWarnOnMultipleRegistration(parStructureStaticData& OUTPUT_ONLY(data))
{
	if ( PARSER.Settings().GetFlag(::rage::parSettings::WARN_ON_MULTIPLE_REGISTRATION))
	{
		parDisplayf("Parsable class %s (0x%x) has already been registered", data.m_StructureName, data.m_StructureNameHash);
	}
}
#endif

void rage::parCguRegisterStructure(parStructure* structure)
{
	PARSER.RegisterStructure(structure->GetNameHash(), structure);
}

void rage::parCguRegisterEnum(parEnumData* enumData)
{
	PARSER.RegisterEnum(enumData);
}

void rage::parCguSignalOutOfOrderRegistration(const char* OUTPUT_ONLY(structureName), const char* OUTPUT_ONLY(requiredBy))
{
	if (PARSER.Settings().GetFlag(parSettings::ASSERT_ON_OUT_OF_ORDER_REGISTRATION))
	{
		parAssertf(0, "%s must be registered before %s is", structureName, requiredBy);
	}
	else if (PARSER.Settings().GetFlag(parSettings::WARN_ON_OUT_OF_ORDER_REGISTRATION))
	{
		parWarningf("%s should be registered before %s", structureName, requiredBy);
	}
}

parStructure* parCguStructure::CreateStructure(size_t size)
{
	parStructure* ret = rage_new parStructure;
	ret->SetSize(size);
	return ret;
}

parStructure* parCguStructure::CreateDiStructure(size_t size)
{
	parStructure* ret = rage_new parDIStructure;
	ret->SetSize(size);
	return ret;
}

parStructure*  parCguStructure::SetFactories(parStructure* str, FactoryFunction factoryFn, PlacementFunction placeFn, DestructorFunction destroyFn)
{
	str->SetFactory(FactoryDelegate(factoryFn));
	str->SetPlacementFactory(PlacementDelegate(placeFn));
	str->SetDestructor(DestructorDelegate(destroyFn));
	return str;
}

parStructure*  parCguStructure::SetGetStructureCB(parStructure* str, GetStructureFunction fn)
{
	str->SetGetStructureCB(GetStructureDelegate(fn));
	return str;
}

parStructure*  parCguStructure::SetBaseClass(parStructure* str, parStructure* baseMetadata, int baseOffset)
{
	str->SetBaseClass(baseMetadata, baseOffset);
	return str;
}

u32 parCguStructure::GetMajorVersion(parStructure* str)
{
	return str->GetVersion().GetMajor();
}

parStructure*  parCguStructure::BeginAddingInitialDelegates(parStructure* str, int numToAdd)
{
	str->BeginAddingInitialDelegates(numToAdd);
	return str;
}

parStructure*  parCguStructure::AddDelegateHolder(parStructure* str, const char* name, parDelegateHolderBase* base)
{
	str->AddDelegateHolder(name, base);
	return str;
}

parStructure*  parCguStructure::EndAddingInitialDelegates(parStructure* str)
{
	str->EndAddingInitialDelegates();
	return str;
}

parStructure*  parCguStructure::BuildStructureFromStaticData(void* /*dummy*/, parStructureStaticData& staticDataRef, u32 majorVersion, u32 minorVersion)
{
	staticDataRef.m_Structure->SetVersion(parVersionInfo(majorVersion, minorVersion));
	staticDataRef.m_Structure->BuildStructureFromStaticData(staticDataRef);
	return staticDataRef.m_Structure;
}

parStructure* parCguStructure::CreateExtraAttributes(parStructure* str)
{
	str->GetExtraAttributes() = rage_new parAttributeList;
	return str;
}

parStructure* parCguStructure::AddExtraAttribute(parStructure* str, const char* key, const char* value)
{
	str->GetExtraAttributes()->AddAttribute(key, value, false, false);
	return str;
}

parStructure* parCguStructure::SortExtraAttributes(parStructure* str)
{
	str->GetExtraAttributes()->SortAttributes();
	return str;
}

parAttributeList** parCguStructure::AddOneExtraAttribute(parAttributeList** attr, const char* key, const char* value)
{
	if (!(*attr))
	{
		(*attr) = rage_new parAttributeList;
	}

	(*attr)->AddAttribute(key, value, false, false);
	(*attr)->SortAttributes();
	return attr;
}

parAttributeList** parCguStructure::AddOneExtraAttribute(parAttributeList** attr, const char* key, int value)
{
	if (!(*attr))
	{
		(*attr) = rage_new parAttributeList;
	}

	(*attr)->AddAttribute(key, value);
	(*attr)->SortAttributes();
	return attr;
}

