// 
// parser/membermap.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBERMAP_H
#define PARSER_MEMBERMAP_H

#include "member.h"

#include "MemberMapdata.h"

#include "data/callback.h"

namespace rage {

// PURPOSE: A parMember subclass for describing maps (atMap, atBinaryMap).
class parMemberMap : public parMember
{
public:
    typedef parMemberMapSubType::Enum SubType;
	typedef parMemberMapData Data;

	parMemberMap() {}
	explicit parMemberMap(Data& data);
	virtual ~parMemberMap();

	virtual void    ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const;
	virtual size_t  GetSize     () const;
	virtual size_t FindAlign() const;

	// PURPOSE: Generally shouldn't be used by client code 
	void SetKeyMember(parMember* member) {
		m_KeyParMember = member;
	}
	void SetDataMember(parMember* member) {
		m_DataParMember = member;
	}


    parMember* GetKeyMember () { return m_KeyParMember; }
    parMember* GetDataMember() { return m_DataParMember; }

    parMemberMapIterator*  CreateIterator (parPtrToStructure structAddr);
    parMemberMapInterface* CreateInterface(parPtrToStructure structAddr);
protected:
    parMember* m_KeyParMember;
    parMember* m_DataParMember;

public:

	STANDARD_PARMEMBER_DATA_FUNCS_DECL(parMemberMap);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_PARSABLE;
#endif
};

// This can't be defined until the derived class is.
inline parMemberMap*		parMember::AsMap()				{ return parMemberType::IsMap(GetType()) ? smart_cast<parMemberMap*>(this) : NULL; }

}

#endif // PARSER_MEMBERMAP_H
