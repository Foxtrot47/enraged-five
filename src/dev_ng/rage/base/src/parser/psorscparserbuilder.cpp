// 
// parser/psorscparserbuilder.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psorscparserbuilder.h"

#include "member.h"
#include "memberarray.h"
#include "memberenum.h"
#include "membermap.h"
#include "memberstring.h"
#include "memberstruct.h"
#include "optimisations.h"
#include "psorscbuilder.h"
#include "structure.h"
#include "visitor.h"

#include "diag/output.h"
#include "file/asset.h"
#include "file/stream.h"

PARSER_OPTIMISATIONS();

using namespace rage;

psoRscBuilderEnumSchema& BuildSchemaFromEnum(psoRscBuilder& builder, parEnumData& enumData);

int AddMemberToSchema(psoRscBuilder& builder, psoRscBuilderStructSchema& schema, parMember& mem, bool isAnon, bool autoPack, bool addNewSchemas)
{
	atLiteralHashValue nameHash(isAnon ? psoConstants::FIRST_ANONYMOUS_ID : mem.GetNameHash());
	parMemberType::Enum srcType = mem.GetType();
	size_t offset = isAnon ? 0 : (autoPack ? (size_t)-1 : mem.GetOffset());

	// Are there any times where this automatic conversion isn't enough? Where we need to look at more data in the member to get the
	// destination type? So far no, but in case there in the future I'm switching on the parMember types and subtypes below instead of
	// on the psoType.
	psoType destType = psoType::ConvertParserTypeToPsoType(srcType, mem.GetCommonData()->m_Subtype, __64BIT);

	switch(srcType)
	{
	case parMemberType::TYPE_ARRAY:
		{
			// Get the prototype member info
			parMemberArray& arrMember = mem.AsArrayRef();
			parMember* proto = arrMember.GetPrototypeMember();
			FastAssert(proto);
			// If the array has external storage, we don't need a schema for the array elements yet, we'll add those when 
			// we come to them - that way we don't add schemas for empty arrays.
			bool usesExternalStorage = arrMember.HasExternalStorage();
			int memberIndex = AddMemberToSchema(builder, schema, *proto, true, autoPack, !usesExternalStorage);
			FastAssert(memberIndex >= 0 && memberIndex < USHRT_MAX);
			FastAssert(arrMember.GetData()->m_NumElements < USHRT_MAX);

			psoRscBuilderStructSchema::ArrayLocation arrayLoc = (arrMember.GetTypeFlags().IsSet(parMemberArrayData::FLAG_ALLOCATE_IN_PHYSICAL_MEMORY) ? 
				psoRscBuilderStructSchema::ARRAY_IN_PHYSICAL_MEMORY :
				psoRscBuilderStructSchema::ARRAY_IN_VIRTUAL_MEMORY);

			size_t alignment = 1 << arrMember.GetAlignmentPower();

			switch(arrMember.GetSubtype())
			{
			case parMemberArraySubType::SUBTYPE_ATARRAY:
			case parMemberArraySubType::SUBTYPE_ATARRAY_32BIT_IDX:
				return schema.AddMemberArray(nameHash, destType, memberIndex, alignment, offset, arrayLoc);
			case parMemberArraySubType::SUBTYPE_ATFIXEDARRAY:
			case parMemberArraySubType::SUBTYPE_ATRANGEARRAY:
			case parMemberArraySubType::SUBTYPE_MEMBER:
			case parMemberArraySubType::SUBTYPE_POINTER:
				return schema.AddMemberMaxSizeArray(nameHash, destType, memberIndex, arrMember.GetData()->m_NumElements, alignment, offset, arrayLoc);
			case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT:
			case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_8BIT_IDX:
			case parMemberArraySubType::SUBTYPE_POINTER_WITH_COUNT_16BIT_IDX:
				return schema.AddMemberArrayWithCounter(nameHash, destType, memberIndex, arrMember.GetData()->m_CountMemberOffset, alignment, offset, arrayLoc);
			case parMemberArraySubType::SUBTYPE_VIRTUAL:
				parWarningf("Virtual arrays can't be put into PSOs");
				return -1;
			}
		}
	case parMemberType::TYPE_MAP:
		{
			// Get the prototype members
			parMemberMap& mapMember = mem.AsMapRef();

			parMember* keyProto = mapMember.GetKeyMember();
			FastAssert(keyProto);
			int keyIndex = AddMemberToSchema(builder, schema, *keyProto, true, autoPack, false);
			FastAssert(keyIndex >= 0 && keyIndex < USHRT_MAX);

			parMember* valueProto = mapMember.GetDataMember();
			FastAssert(valueProto);
			int valueIndex = AddMemberToSchema(builder, schema, *valueProto, true, autoPack, false);
			FastAssert(valueIndex >= 0 && valueIndex < USHRT_MAX);

			switch(mapMember.GetSubtype())
			{
			case parMemberMapSubType::SUBTYPE_ATBINARYMAP:
				return schema.AddMemberMap(nameHash, destType, keyIndex, valueIndex, offset);
			case parMemberMapSubType::SUBTYPE_ATMAP:
				parWarningf("atMaps can't be put into PSOs");
				return -1;
			}
		}
	case parMemberType::TYPE_ENUM:
		{
			const parMemberEnum& enumMember = mem.AsEnumRef();

			BuildSchemaFromEnum(builder, *(enumMember.GetData()->m_EnumData));

			return schema.AddMemberEnum(nameHash, destType, atLiteralHashValue(enumMember.GetData()->m_EnumData->m_NameHash), offset);
		}
	case parMemberType::TYPE_STRUCT:
		{
			const parMemberStruct& structMember = mem.AsStructOrPointerRef();
			switch(structMember.GetSubtype())
			{
			case parMemberStructSubType::SUBTYPE_STRUCTURE:
				{
					if (addNewSchemas)
					{
						psoRscBuilderStructSchema& structSchema = psoBuildSchemaFromStructure(builder, *structMember.GetBaseStructure());
						return schema.AddMemberStruct(nameHash, destType, atLiteralHashValue(structSchema.GetName()), offset);
					}
					else
					{
						return schema.AddMemberStruct(nameHash, destType, atLiteralHashValue(structMember.GetBaseStructure()->GetNameHash()), offset);
					}
				}
			case parMemberStructSubType::SUBTYPE_POINTER:
			case parMemberStructSubType::SUBTYPE_SIMPLE_POINTER:
				return schema.AddMemberGeneric(nameHash, destType, offset);
			default:
				parWarningf("Couldn't add struct member with subtype %d", structMember.GetSubtype());
				return -1;
			}
		}
	case parMemberType::TYPE_BITSET:
		{
			const parMemberBitset& bitsetMember = mem.AsBitsetRef();

			atLiteralHashValue enumHash;
			if (bitsetMember.AreBitsNamed())
			{
				enumHash = atLiteralHashValue(bitsetMember.GetData()->m_EnumData->m_NameHash);

				BuildSchemaFromEnum(builder, *(bitsetMember.GetData()->m_EnumData));
			}

			return schema.AddMemberBitset(nameHash, destType, enumHash, bitsetMember.GetData()->m_NumBits, offset);
		}
	case parMemberType::TYPE_STRING:
		{
			const parMemberString& stringMember = mem.AsStringRef();
			parAssertf(stringMember.GetData()->m_Length < USHRT_MAX, "Too many elements for a PSO array. See https://devstar.rockstargames.com/wiki/index.php/ParserSerializedObjectFile#File_Structure");
			u16 count = (u16)stringMember.GetData()->m_Length;
			u8 nsIndex = 0;
			if (stringMember.GetSubtype() == parMemberStringSubType::SUBTYPE_ATNSHASHSTRING ||
				stringMember.GetSubtype() == parMemberStringSubType::SUBTYPE_ATNSHASHVALUE)
			{
				nsIndex = (u8)stringMember.GetNamespaceIndex();
			}
			return schema.AddMemberString(nameHash, destType, count, offset, nsIndex);
		}
	default:
		{
			return schema.AddMemberGeneric(nameHash, destType, offset);
		}
	}
}

psoRscBuilderStructSchema&  rage::psoBuildSchemaFromStructure(psoRscBuilder& builder, parStructure& str)
{
	psoRscBuilderStructSchema* schema = builder.FindStructSchema(atLiteralHashValue(str.GetNameHash()));
	if (schema)
	{
		return *schema;
	}

	bool autoPack = (str.GetSize() == (size_t)-1);

	schema = &builder.CreateStructSchema(atLiteralHashValue(str.GetNameHash()), str.GetSize(), str.FindAlign());

	// TODO: Fill out other data here - flags, major version

	parStructure::BaseList bases;
	bases.Init(&str, NULL);
	for(int baseIdx = bases.GetNumBases() - 1; baseIdx >= 0; baseIdx--)
	{
		parStructure* currStruct = bases.GetBaseStructure(baseIdx);

		for(int memIdx = 0; memIdx < currStruct->GetNumMembers(); memIdx++)
		{
			parMember* mem = currStruct->GetMember(memIdx);

			AddMemberToSchema(builder, *schema, *mem, false, autoPack, true);
		}
	}

	return builder.FinishStructSchema(*schema);
}

psoRscBuilderEnumSchema& BuildSchemaFromEnum(psoRscBuilder& builder, parEnumData& enumData)
{
	psoRscBuilderEnumSchema* schema = builder.FindEnumSchema(atLiteralHashValue(enumData.m_NameHash));
	if (schema)
	{
		return *schema;
	}

	schema = &builder.CreateEnumSchema(atLiteralHashValue(enumData.m_NameHash));

	for(int i = 0; i < enumData.m_NumEnums; i++)
	{
		schema->AddValue(atLiteralHashValue(enumData.m_Enums[i].m_NameKey), enumData.m_Enums[i].m_Value);
	}

	return builder.FinishEnumSchema(*schema);
}



// This one iterates over all of the sub-objects in a structure graph and builds up the instance catalog
// containing a description of all of our instance data
class BuildPsoRscVisitor : public parInstanceVisitor
{
public:
	BuildPsoRscVisitor(psoRscBuilder& builder)
		: m_Builder(&builder)
	{
	}

	// NOTE: Must be called after VisitToplevelStructure
	psoRscBuilderInstance* GetToplevelInstance()
	{
		return m_TopInstance;
	}

	virtual void VisitToplevelStructure(parPtrToStructure instanceDataPtr, parStructure& metadata)
	{
		psoRscBuilderStructSchema& schema = psoBuildSchemaFromStructure(*m_Builder, metadata);

		m_TopInstance = &m_Builder->AddStructInstances(schema, reinterpret_cast<char*>(instanceDataPtr), 1, 0, false);

		m_SchemaStack.PushAndGrow(&schema);

		parInstanceVisitor::VisitToplevelStructure(instanceDataPtr, metadata);
	}

	virtual bool BeginStructMember(parPtrToStructure /*ptrToStruct*/, parMemberStruct& metadata)
	{
		psoRscBuilderStructSchema& schema = psoBuildSchemaFromStructure(*m_Builder, *metadata.GetBaseStructure());
		m_SchemaStack.PushAndGrow(&schema);
		return true;
	}

	virtual void EndStructMember(parPtrToStructure /*ptrToStruct*/, parMemberStruct& /*metadata*/)
	{
		m_SchemaStack.Pop();
	}

	virtual bool BeginPointerMember(parPtrToStructure& ptrRef, parMemberStruct& metadata)
	{
		if (ptrRef)
		{
			parStructure* concreteStruct = metadata.GetConcreteStructure(ptrRef);

			psoRscBuilderStructSchema& schema = psoBuildSchemaFromStructure(*m_Builder, *concreteStruct);

			m_Builder->AddStructInstances(schema, reinterpret_cast<char*>(ptrRef), 1, 0, false);
			m_SchemaStack.PushAndGrow(&schema);
		}
		else
		{
			m_SchemaStack.PushAndGrow(NULL);
		}

		return true;
	}

	virtual void EndPointerMember(parPtrToStructure& /*ptrRef*/, parMemberStruct& /*metadata*/)
	{
		m_SchemaStack.Pop();
	}

	virtual bool BeginArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray arrayContentsVoidPtr, size_t numElements, parMemberArray& metadata)
	{
		using namespace parMemberType;

		if (!metadata.HasExternalStorage())
		{
			return true; // Don't have to do anything special for these
		}

		char* arrayContents = reinterpret_cast<char*>(arrayContentsVoidPtr);

		if (numElements == 0)
		{
			return false;
		}

		size_t arraySizeInBytes = numElements * metadata.GetPrototypeMember()->GetSize();

		size_t contentAlignment = metadata.FindContentAlign();

		bool visitKids = false;

		// This block has to do a couple things...
		// 1) Add any array data to the instance catalog
		// 2) Add any schemas to the schema catalog
		// 3) Determine if we need to recurse into the child elements

		psoType destType = psoType::ConvertParserTypeToPsoType(metadata.GetPrototypeMember()->GetType(), metadata.GetPrototypeMember()->GetCommonData()->m_Subtype, __64BIT);

		bool physicalHeap = metadata.GetTypeFlags().IsSet(parMemberArrayData::FLAG_ALLOCATE_IN_PHYSICAL_MEMORY);

		switch(destType.GetEnum())
		{
		// These are all POD array, nothing fancy needed
		case psoType::TYPE_BOOL:
		case psoType::TYPE_BOOLV:
		case psoType::TYPE_VECBOOLV:
		case psoType::TYPE_S8:
		case psoType::TYPE_U8:
		case psoType::TYPE_S16:
		case psoType::TYPE_U16:
		case psoType::TYPE_S32:
		case psoType::TYPE_U32:
		case psoType::TYPE_S64:
		case psoType::TYPE_U64:
		case psoType::TYPE_FLOAT16:
		case psoType::TYPE_FLOAT:
		case psoType::TYPE_DOUBLE:
		case psoType::TYPE_SCALARV:
		case psoType::TYPE_VEC2:
		case psoType::TYPE_VEC2V:
		case psoType::TYPE_VEC3:
		case psoType::TYPE_VEC3V:
		case psoType::TYPE_VEC4V:
		case psoType::TYPE_MAT33V:
		case psoType::TYPE_MAT34V:
		case psoType::TYPE_MAT43:
		case psoType::TYPE_MAT44V:
		case psoType::TYPE_PARTIALSTRINGHASH:
		case psoType::TYPE_LITERALSTRINGHASH:
			m_Builder->AddPodArray(destType, arrayContents, arraySizeInBytes, contentAlignment, physicalHeap);
			break;

		// atArray<CWhatever> - make sure we have a schema for CWhatever and add all the instances.
		case psoType::TYPE_STRUCT:
			{
				parMemberStruct& protoMember = metadata.GetPrototypeMember()->AsStructOrPointerRef();
				psoRscBuilderStructSchema& schema = psoBuildSchemaFromStructure(*m_Builder, *protoMember.GetBaseStructure());
				m_Builder->AddStructInstances(schema, arrayContents, numElements, contentAlignment, physicalHeap);
				visitKids = true;
			}
			break;
		
		// Basic pointer and string types. These are more or less like the POD types, but we need to recurse to
		// add the child data to the PSO file too
		case psoType::TYPE_POINTER32:
		case psoType::TYPE_POINTER64:
		case psoType::TYPE_STRING_POINTER32: // arguably could serialize string pointers as TYPE_POINTER32 too
		case psoType::TYPE_STRING_POINTER64:
		case psoType::TYPE_WIDE_STRING_POINTER32:
		case psoType::TYPE_WIDE_STRING_POINTER64: 
		case psoType::TYPE_ATSTRING32:
		case psoType::TYPE_ATSTRING64:
		case psoType::TYPE_ATWIDESTRING32:
		case psoType::TYPE_ATWIDESTRING64:
		case psoType::TYPE_STRINGHASH:
		case psoType::TYPE_NSSTRINGHASH:
			{
				m_Builder->AddPodArray(destType, arrayContents, arraySizeInBytes, contentAlignment, physicalHeap);
				visitKids = true;
			}
			break;

		// atArray<char[5]> - write this as just a single string
		case psoType::TYPE_STRING_MEMBER:
			m_Builder->AddPodArray(psoType::TYPE_S8, arrayContents, arraySizeInBytes, contentAlignment, physicalHeap);
			break;
		case psoType::TYPE_WIDE_STRING_MEMBER:
			m_Builder->AddPodArray(psoType::TYPE_U16, arrayContents, arraySizeInBytes, contentAlignment, physicalHeap);
			break;

		// Nested arrays. Right now we don't support these, mainly because something like
		// atArray<atFixedArray<CWhatever, 5> > can't be represented, since what do we make a structarray of? We'd have to 
		// add anonymous struct types to hold the atFixedArray<CWhatever, 5>
		// Some of these would probably work ok, like atArray<atArray<CWhatever> >, since we'd just have a structarray of atArrays<>
		// and those don't depend on the element types to know any size or layout info. But for now it'll just be a blanket restriction
		case psoType::TYPE_ARRAY_MEMBER:	// Do we even need to do anything here. Just visit kids right? Support this one then?
		case psoType::TYPE_ATFIXEDARRAY:	// As above
		case psoType::TYPE_ARRAY_POINTER32_WITH_COUNT:
		case psoType::TYPE_ARRAY_POINTER64_WITH_COUNT:
		case psoType::TYPE_ATARRAY32:		// Why not support these array types from here...
		case psoType::TYPE_ATARRAY64:		
		case psoType::TYPE_ATARRAY32_32BITIDX:
		case psoType::TYPE_ATARRAY64_32BITIDX:
		case psoType::TYPE_ARRAY_POINTER32:
		case psoType::TYPE_ARRAY_POINTER64:	// ...to here? Should work since they're just pointers to other data
			destType.PrintUnexpectedTypeError("Adding an array to a PSO", "a structarray type (nested arrays aren't supported (yet?))");
			break;

		case psoType::TYPE_ENUM8:
		case psoType::TYPE_ENUM16:
		case psoType::TYPE_ENUM32:
			{
				parMemberEnum& protoMember = metadata.GetPrototypeMember()->AsEnumRef();
				// Make an anonymous struct that contains the enum, and add an array of that struct
				// (so that we can do fixups in case the underlying enum changes on us)
				psoRscBuilderStructSchema& anonContainer = m_Builder->CreateStructSchema(atLiteralHashValue::Null());
				anonContainer.AddMemberEnum(atLiteralHashValue("Enum"), destType, atLiteralHashValue(protoMember.GetData()->m_EnumData->m_NameHash));
				psoRscBuilderStructSchema& finalContainer = m_Builder->FinishStructSchema(anonContainer);

				m_Builder->AddStructInstances(finalContainer, arrayContents, arraySizeInBytes / finalContainer.GetSize(), contentAlignment, physicalHeap);
			}
			break;

		case psoType::TYPE_BITSET8:
		case psoType::TYPE_BITSET16:
		case psoType::TYPE_BITSET32:
			{
				parMemberBitset& protoMember = metadata.GetPrototypeMember()->AsBitsetRef();
				if (protoMember.AreBitsNamed())
				{
					// Make an anonymous struct that contains the bitset, and add an array of that struct
					// (so that we can do fixups in case the underlying enum changes on us)
					psoRscBuilderStructSchema& anonContainer = m_Builder->CreateStructSchema(atLiteralHashValue());
					anonContainer.AddMemberBitset(atLiteralHashValue("Bitset"), destType, atLiteralHashValue(protoMember.GetData()->m_EnumData->m_NameHash), protoMember.GetData()->m_NumBits);
					psoRscBuilderStructSchema& finalContainer = m_Builder->FinishStructSchema(anonContainer);

					m_Builder->AddStructInstances(finalContainer, arrayContents, arraySizeInBytes / finalContainer.GetSize(), contentAlignment, physicalHeap);
				}
				else
				{
					// unnamed bits, write them out as an int array
					psoType outType(psoType::TYPE_INVALID);
					switch(destType.GetEnum())
					{
					case psoType::TYPE_BITSET8:	outType = psoType::TYPE_U8; break;
					case psoType::TYPE_BITSET16: outType = psoType::TYPE_U16; break;
					case psoType::TYPE_BITSET32: outType = psoType::TYPE_U32; break;
					default:
						break;
					}
					m_Builder->AddPodArray(outType, arrayContents, arraySizeInBytes, contentAlignment, physicalHeap);
				}
			}
			break;

		case psoType::TYPE_ATBITSET32:
		case psoType::TYPE_ATBITSET64:
			destType.PrintUnexpectedTypeError("Adding an array to a PSO", "a structarray type (arrays of atBitsets aren't supported)");
			break;

		case psoType::TYPE_ATBINARYMAP32:
		case psoType::TYPE_ATBINARYMAP64:
			// These would probably work without too much trouble, but lets save that for another day
			destType.PrintUnexpectedTypeError("Adding an array to a PSO", "a structarray type (arrays of atBinaryMaps aren't supported)");
			break;

		case psoType::TYPE_INVALID:
		case psoType::TYPE_STRUCTID:
			destType.PrintUnexpectedTypeError("Adding an array to a PSO", "a type suitable for PSO arrays (mainly not another array)");
		}

		return visitKids;
	}

	virtual void EndArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*arrayContents*/, size_t /*numElements*/, parMemberArray& metadata)
	{
		if (!metadata.HasExternalStorage())
		{
			return; // Don't have to do anything special for these
		}
	}

	psoRscBuilderStructSchema* FindOrCreateAtBinaryMapElementSchema(psoRscMemberSchemaData& keySchema, psoRscMemberSchemaData& valueSchema)
	{
		// None of the existing map schemas matched, so build a new one
		psoRscBuilderStructSchema& mapBuildSchema = m_Builder->CreateStructSchema(atLiteralHashValue::Null());

		// Use AddMemberRaw to copy all the data, regardless of type
		mapBuildSchema.CopyMember(keySchema, atLiteralHashValue("Key"));
		mapBuildSchema.CopyMember(valueSchema, atLiteralHashValue("Item"));

		return &m_Builder->FinishStructSchema(mapBuildSchema);
	}

	psoRscBuilderStructSchema* FindOrCreateAtMapElementSchema(psoRscMemberSchemaData& keySchema, psoRscMemberSchemaData& valueSchema)
	{
		// None of the existing map schemas matched, so build a new one
		psoRscBuilderStructSchema& mapBuildSchema = m_Builder->CreateStructSchema(atLiteralHashValue::Null());

		// Use AddMemberRaw to copy all the data, regardless of type
		mapBuildSchema.CopyMember(keySchema, atLiteralHashValue("Key"));
		mapBuildSchema.CopyMember(valueSchema, atLiteralHashValue("Item"));
		mapBuildSchema.AddMemberGeneric(atLiteralHashValue("next"), __64BIT ? psoType::TYPE_POINTER64 : psoType::TYPE_POINTER32);

		return &m_Builder->FinishStructSchema(mapBuildSchema);
	}

	virtual bool BeginMapMember(parPtrToStructure structAddr, parMemberMap& metadata)
	{
		// Find this member in the PSO schema, so we can get to the key and value members
		psoRscBuilderStructSchema* bldSchema = m_SchemaStack.Top(); 

		psoRscMemberSchemaData* mapSchema = NULL;
		for(int i = 0; i < bldSchema->GetMemberArray().GetCount(); i++)
		{
			if (metadata.GetNameHash() == bldSchema->GetMemberArray()[i].m_NameHash.GetHash())
			{
				mapSchema = &bldSchema->GetMemberArray()[i];
				break;
			}
		}
		if (!mapSchema)
		{
			parErrorf("Couldn't find a member in the PSO with hash 0x%08x", metadata.GetNameHash());
			return false;
		}

		psoRscMemberSchemaData& keySchema = bldSchema->GetMemberArray()[mapSchema->Map.m_KeyIndex];
		psoRscMemberSchemaData& valueSchema = bldSchema->GetMemberArray()[mapSchema->Map.m_ValueIndex];

		switch(metadata.GetSubtype())
		{
		case parMemberMapSubType::SUBTYPE_ATBINARYMAP:
			{
				psoFakeAtBinMap& binmap = metadata.GetMemberFromStruct<psoFakeAtBinMap>(structAddr);
				if (binmap.m_Data.m_Count > 0)
				{
					parMemberStruct* valueMember = metadata.GetDataMember()->AsStructOrPointer();
					if (valueMember && valueMember->GetSubtype() == parMemberStructSubType::SUBTYPE_STRUCTURE)
					{
						psoBuildSchemaFromStructure(*m_Builder, *valueMember->GetBaseStructure());
					}
					psoRscBuilderStructSchema* mapElementContainerSchema = FindOrCreateAtBinaryMapElementSchema(keySchema, valueSchema);
					// Add the array of map data to the PSO
					m_Builder->AddStructInstances(*mapElementContainerSchema, binmap.m_Data.m_Elements.m_Pointer, binmap.m_Data.m_Count, 0, false);
				}
			}
			break;
		case parMemberMapSubType::SUBTYPE_ATMAP:
			// Add an anonymous structure that contains {key, value, next}. Then add an array of those?
			// TODO: atMap support
			parErrorf("Can't add atMaps to PSO files yet (can you use atBinaryMap?)");
			return false;
		}

		return true;
	}

	virtual void EndMapMember(parPtrToStructure /*structAddr*/, parMemberMap& /*metadata*/)
	{
	}

	virtual void PointerStringMember(char*& stringData, parMemberString& /*metadata*/)
	{
		if (stringData)
		{
			m_Builder->AddPodArray(psoType::TYPE_S8, stringData, (int) strlen(stringData) + 1, 0, false);
		}
	}

	virtual void ConstStringMember(ConstString& stringData, parMemberString& /*metadata*/)
	{
		if (stringData.c_str())
		{
			m_Builder->AddPodArray(psoType::TYPE_S8, stringData.c_str(), (int) strlen(stringData.c_str()) + 1, 0, false);
		}
	}

#if !__FINAL
	virtual void AtNonFinalHashStringMember(atHashString& stringData, parMemberString& /*metadata*/)
	{
		m_Builder->AddExtraString(HSNS_ATHASHSTRING, stringData.GetHash(), stringData.GetCStr());
	}
#endif	//	!__FINAL

	virtual void AtFinalHashStringMember(atFinalHashString& stringData, parMemberString& /*metadata*/)
	{
		m_Builder->AddExtraString(HSNS_ATFINALHASHSTRING, stringData.GetHash(), stringData.GetCStr());
	}

	virtual void AtNamespacedHashStringMember(atNamespacedHashStringBase& stringData, parMemberString& metadata)
	{
		atHashStringNamespaces nsIndex = (atHashStringNamespaces)metadata.GetNamespaceIndex();
		u32 hash = stringData.GetHash();
		m_Builder->AddExtraString(nsIndex, hash, atHashStringNamespaceSupport::GetString(nsIndex, hash));
	}

	virtual void WidePointerStringMember(char16*& stringData, parMemberString& /*metadata*/)
	{
		if (stringData)
		{
			m_Builder->AddPodArray(psoType::TYPE_U16, reinterpret_cast<char*>(stringData), sizeof(char16) * ( (int) wcslen(stringData) + 1), 0, false);
		}
	}

	virtual void AtStringMember(atString& stringData, parMemberString& /*metadata*/)
	{
		if (stringData.GetLength())
		{
			m_Builder->AddPodArray(psoType::TYPE_S8, const_cast<char*>(stringData.c_str()), stringData.GetLength()+1, 0, false);
		}
	}

	virtual void AtWideStringMember(atWideString& stringData, parMemberString& /*metadata*/)
	{
		if (stringData.GetLength())
		{
			const char16* realStringData = stringData;
			char16* nonConstStringData = const_cast<char16*>(realStringData);
			m_Builder->AddPodArray(psoType::TYPE_U16, reinterpret_cast<char*>(nonConstStringData), sizeof(char16) * (stringData.GetLength()+1), 0, false);
		}
	}

	virtual void BitsetMember(parPtrToMember ptrToMember, parPtrToArray ptrToBits, size_t numBits, parMemberBitset& metadata)
	{
		size_t numWords = (numBits + 31) / 32;

		if (metadata.GetSubtype() == parMemberBitsetSubType::SUBTYPE_ATBITSET && numBits > 0)
		{
			if (metadata.AreBitsNamed())
			{
				// Create an anon struct: struct { atFixedBitset32<numBits> Bitset; }
				// And write out a pointer to _that_ struct, so that we know what enum names all the bits. 
				// Note that numBits is a runtime size here, not compiletime. So two instances of the same bitset might end up
				// using two different anonymous structs when we write them out. That should be OK as long as the bitset sizes don't
				// vary /too/ much between instances.
				psoFakeAtBitset* fakeBitset = reinterpret_cast<psoFakeAtBitset*>(ptrToMember);
				psoRscBuilderStructSchema& anonContainer = m_Builder->CreateStructSchema(atLiteralHashValue());
				anonContainer.AddMemberBitset(atLiteralHashValue("Bitset"), psoType::TYPE_BITSET32, atLiteralHashValue(metadata.GetData()->m_EnumData->m_NameHash), fakeBitset->m_BitSize);
				psoRscBuilderStructSchema& finalContainer = m_Builder->FinishStructSchema(anonContainer);

				m_Builder->AddStructInstances(finalContainer, reinterpret_cast<char*>(ptrToBits), numWords * sizeof(u32), 0, false);
			}
			else
			{
				m_Builder->AddPodArray(psoType::TYPE_U32, reinterpret_cast<char*>(ptrToBits), numWords * sizeof(u32), 0, false);
			}
		}
	}

private:
	psoRscBuilder* m_Builder;
	psoRscBuilderInstance* m_TopInstance;
	atArray<psoRscBuilderStructSchema*>	m_SchemaStack;
};


psoRscBuilderInstance* rage::psoAddParsableObjectFromStructure(psoRscBuilder& builder, const parStructure& structure, parConstPtrToStructure obj)
{
	BuildPsoRscVisitor psoVisitor(builder);
	psoVisitor.VisitToplevelStructure(const_cast<parPtrToStructure>(obj), const_cast<parStructure&>(structure));
	
	return psoVisitor.GetToplevelInstance();
}

#if __RESOURCECOMPILER
bool rage::psoRscSaveFromStructure(const char* filename, const parStructure& structure, parConstPtrToStructure obj, bool /*includeChecksum*/ /* = false */)
{
	psoRscBuilder builder;

	BuildPsoRscVisitor psoVisitor(builder);
	psoVisitor.VisitToplevelStructure(const_cast<parPtrToStructure>(obj), const_cast<parStructure&>(structure));

	builder.SetRootObject(*psoVisitor.GetToplevelInstance());

	builder.FinishBuilding();

	builder.SaveResource(filename);

	return true;
}
#endif
