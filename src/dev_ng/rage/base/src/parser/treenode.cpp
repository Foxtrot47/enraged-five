// 
// parser/treenode.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "treenode.h"

#include "manager.h"
#include "optimisations.h"

#include "file/stream.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "system/cache.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"

PARSER_OPTIMISATIONS();

using namespace rage;

parTreeNode::~parTreeNode()
{
	RemoveSelf();
	ClearChildrenAndData();
}

void parTreeNode::ClearChildrenAndData()
{
	ClearData();
	while(m_Child)
	{
		PrefetchDC(m_Child->m_Sibling);
		delete m_Child;
	}
}

parTreeNode* parTreeNode::FindRoot()
{
	parTreeNode* node = this;
	while(node->m_Parent)
	{
		node = node->m_Parent;
	}
	return node;
}

parTreeNode* parTreeNode::FindFromXPathInternal(const char* path, parAttribute** attr)
{
	if (!parVerifyf(path, "path shouldn't be NULL"))
	{
		return NULL;
	}
	
	if(path[0] == '\0')
	{
		return this;
	}
	else if(path[0] == '/')
	{
		parTreeNode* root = FindRoot();
		parAssertf(root, "Every tree node has to have a root");
		// Path should look like this "/RootNode/ChildNode" or this "/*/ChildNode"
		if (path[1] == '*' )
		{
			if (path[2] == '/')
				return root->FindFromXPathInternal(path+3, attr);
			else if (path[2] == '\0')
				return root;
		}
		const char* rootName = root->GetElement().GetName();
		size_t rootNameLen = strlen(rootName);
		if (!strncmp(path+1, rootName, rootNameLen))
		{
			if (path[rootNameLen+1] == '/')
				return root->FindFromXPathInternal(path+rootNameLen+2, attr);
			else if (path[rootNameLen+1] == '\0')
				return root;
		}
		return NULL;
	}
	else if (attr && path[0] == '@')
	{
		*attr = GetElement().FindAttribute(path+1);
		return NULL;
	}
	else
	{
		parTreeNode* node = NULL;

		char tok[256];
		int i = 0;
		int index = 1;
		for(i = 0; path[i] != '/' && path[i] != '\0'; i++)
		{
			if (path[i] == '[')
			{
				index = atoi(path+i+1);
				while(path[i] != ']')
				{
					i++;
				}
				i++;
				parAssertf(path[i] == '/' || path[i] == '\0', "Bad format for query string: %s", path); // nothing allowed after array index
				break;
			}
			else
			{
				tok[i] = path[i];
			}
		}
		bool moreData = path[i] == '/';
		const char* remainingPath = moreData ? path + i + 1 : NULL;
		tok[i] = '\0';
		if (!strcmp(tok, "."))
		{
			node = this;
		}
		else if (!strcmp(tok, ".."))
		{
			node = GetParent();
		}
		else 
		{
			parAssertf(index > 0, "Invalid index - indices should start at 1, got %d", index);
			if (tok[0] == '\0') 
			{
				node = FindChildWithIndex(index-1);
			}
			else
			{
				// if no index is specified, it'll be 1.
				node = NULL;
				int i = 1;
				do 
				{
					node = FindChildWithName(tok, node);
					i++;
				} while(i < index && node != NULL);
			}
		}
		return (node && remainingPath) ? node->FindFromXPathInternal(remainingPath, attr) : node;
	}
	
}

parTreeNode* parTreeNode::FindChildWithName(const char* name, parTreeNode* start, bool caseSensitive) const
{
	bool caseInsensitive = !caseSensitive;

	ChildNodeIterator i = start != NULL ? ChildNodeIterator(start->GetSibling()) : BeginChildren();
	for(; i != EndChildren(); ++i) {
		if (parUtils::StringEquals(name, (*i)->GetElement().GetName(), caseInsensitive)) {
			return *i;
		}
	}
	return NULL;
}

parTreeNode* parTreeNode::FindChildWithNameIgnoreNs(const char* name, parTreeNode* start, bool caseSensitive) const
{
	bool caseInsensitive = !caseSensitive;

	ChildNodeIterator i = start != NULL ? ChildNodeIterator(start->GetSibling()) : BeginChildren();
	for(; i != EndChildren(); ++i) {
		if (parUtils::StringEquals(name, (*i)->GetElement().GetNameNoNs(), caseInsensitive)) {
			return *i;
		}
	}
	return NULL;
}

parTreeNode* parTreeNode::FindChildWithIndex(int index) const
{
	int num = 0;
	for(ChildNodeIterator i = BeginChildren(); i != EndChildren(); ++i)
	{
		if (num == index) {
			return *i;
		}
		++num;
	}
	return NULL;
}

parTreeNode* parTreeNode::FindChildWithAttribute(const char* name, parTreeNode* start) const
{
	ChildNodeIterator i = start != NULL ? ChildNodeIterator(start->GetSibling()) : BeginChildren();

	for(; i != EndChildren(); ++i) {
		if ((*i)->GetElement().FindAttribute(name) != NULL) {
			return *i;
		}
	}
	return NULL;
}

parTreeNode* parTreeNode::FindChildWithAttribute(const char* name, const char* value, parTreeNode* start) const
{
	ChildNodeIterator i = start != NULL ? ChildNodeIterator(start->GetSibling()) : BeginChildren();

	for(; i != EndChildren(); ++i) {
		parAttribute* attr = (*i)->GetElement().FindAttribute(name);
		if (attr) {
			if (!strcmp(attr->GetStringValue(), value)) {
				return *i;
			}
		}
	}
	return NULL;
}

parTreeNode* parTreeNode::FindChildWithAttribute(const char* name, int value, parTreeNode* start) const
{
	ChildNodeIterator i = start != NULL ? ChildNodeIterator(start->GetSibling()) : BeginChildren();

	for(; i != EndChildren(); ++i) {
		parAttribute* attr = (*i)->GetElement().FindAttribute(name);
		if (attr) {
			if (attr->FindIntValue() == value) {
				return *i;
			}
		}
	}
	return NULL;
}

parTreeNode* parTreeNode::FindChildWithAttribute(const char* name, bool value, parTreeNode* start) const
{
	ChildNodeIterator i = start != NULL ? ChildNodeIterator(start->GetSibling()) : BeginChildren();

	for(; i != EndChildren(); ++i) {
		parAttribute* attr = (*i)->GetElement().FindAttribute(name);
		if (attr) {
			if (attr->FindBoolValue() == value) {
				return *i;
			}
		}
	}
	return NULL;
}



void parTreeNode::RemoveSelf() {
	if (m_Parent) {
		parTreeNode **owner = &m_Parent->m_Child;
		while (*owner != this)
			owner = &(*owner)->m_Sibling;
		*owner = m_Sibling;
	}
	m_Parent = m_Sibling = 0;
}


void parTreeNode::InsertAsChildOf(parTreeNode *parent) {
	FastAssert(parent);
	if (parent != this) {
		RemoveSelf();
		m_Parent = parent;
		m_Sibling = m_Parent->m_Child;
		m_Parent->m_Child = this;
	}
}


void parTreeNode::InsertAsSiblingOf(parTreeNode *sibling) {
	FastAssert(sibling);
	if (sibling != this) {
		RemoveSelf();
		m_Parent = sibling->m_Parent;
		m_Sibling = sibling->m_Sibling;
		sibling->m_Sibling = this;
	}
}


void parTreeNode::AppendAsChildOf(parTreeNode *parent) {
	FastAssert(parent);
	if (parent != this) {
		RemoveSelf();
		m_Parent = parent;
		parTreeNode **owner = &m_Parent->m_Child;
		while (*owner)
			owner = &(*owner)->m_Sibling;
		*owner = this;
	}
}

bool parTreeNode::FindNextNodeCircular(parTreeNode::ChildNodeIterator& iter, const parTreeNode::ChildNodeIterator& begin, const parTreeNode::ChildNodeIterator& end)
{
	++iter;
	if (iter == end) {
		iter = begin;
		return true;
	}
	return false;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, double value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	newNode->GetElement().AddAttribute("value", value, false);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, s64 value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	newNode->GetElement().AddAttribute("value", value, false);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, bool value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	newNode->GetElement().AddAttribute("value", value, false);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const char* value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	// Always add content="ascii" if the string is all whitespace
	if (parUtils::IsAllWhitespace(value))
	{
		// write the content attribute. This ensures we'll read the string as ascii
		// when we read it back in.
		newNode->GetElement().AddAttribute("content", "ascii", false, false);
		// also write a special "xml:space" attribute, in case any XML processors
		// will be processing this data
		newNode->GetElement().AddAttribute("xml:space", "preserve", true, true);
	}
	if (value && value[0] != '\0')
	{
		newNode->SetData(value, (u32)strlen(value)+1);
	}
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const char16* value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);

	if (value && value[0] != 0)
	{
		newNode->SetData(reinterpret_cast<const char*>(value), ((u32)wcslen(value)+1)* sizeof(char16), parStream::UTF16);
	}
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Vector2& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	newNode->GetElement().AddAttribute("x", value.x, false);
	newNode->GetElement().AddAttribute("y", value.y, false);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Vector3& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	newNode->GetElement().AddAttribute("x", value.x, false);
	newNode->GetElement().AddAttribute("y", value.y, false);
	newNode->GetElement().AddAttribute("z", value.z, false);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Vector4& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	newNode->GetElement().AddAttribute("x", value.x, false);
	newNode->GetElement().AddAttribute("y", value.y, false);
	newNode->GetElement().AddAttribute("z", value.z, false);
	newNode->GetElement().AddAttribute("w", value.w, false);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Matrix34& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);

	// Could probably just cast the matrix addr to a Vector3*
	// but this is a little safer.
	Vector3 data[4];
	data[0] = value.a;
	data[1] = value.b;
	data[2] = value.c;
	data[3] = value.d;
	newNode->SetData(reinterpret_cast<const char*>(data), sizeof(Vector3) * 4, parStream::VECTOR3_ARRAY);

	return newNode;
}


parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Matrix44& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);

	// Could probably just cast the matrix addr to a Vector4*
	// but this is a little safer.
	Vector4 data[4];
	data[0] = value.a;
	data[1] = value.b;
	data[2] = value.c;
	data[3] = value.d;
	newNode->SetData(reinterpret_cast<const char*>(data), sizeof(Vector4) * 4, parStream::VECTOR4_ARRAY);

	return newNode;
}


parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Vec2V& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	newNode->GetElement().AddAttribute("x", value.GetXf(), false);
	newNode->GetElement().AddAttribute("y", value.GetYf(), false);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Vec3V& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	newNode->GetElement().AddAttribute("x", value.GetXf(), false);
	newNode->GetElement().AddAttribute("y", value.GetYf(), false);
	newNode->GetElement().AddAttribute("z", value.GetZf(), false);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Vec4V& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);
	newNode->GetElement().AddAttribute("x", value.GetXf(), false);
	newNode->GetElement().AddAttribute("y", value.GetYf(), false);
	newNode->GetElement().AddAttribute("z", value.GetZf(), false);
	newNode->GetElement().AddAttribute("w", value.GetWf(), false);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Mat33V& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);

	// Data needs to be in column major order for content="matrix33" and it already is - so a cast is enough
	newNode->SetData(reinterpret_cast<const char*>(&value), sizeof(Mat33V), parStream::MATRIX33);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Mat34V& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);

	// Data needs to be in column major order for content="matrix34" and it already is - so a cast is enough
	newNode->SetData(reinterpret_cast<const char*>(&value), sizeof(Mat34V), parStream::MATRIX34);
	return newNode;
}

parTreeNode* parTreeNode::CreateStdLeaf(const char* name, const Mat44V& value, bool copyName)
{
	parTreeNode* newNode = rage_new parTreeNode(name, copyName);

	// Data needs to be in column major order for content="matrix44" and it already is - so a cast is enough
	newNode->SetData(reinterpret_cast<const char*>(&value), sizeof(Mat44V), parStream::MATRIX44);
	return newNode;
}

bool parTreeNode::ReadStdLeafBool() const
{
	return GetElement().FindAttributeBoolValue("value", false, true);
}

double parTreeNode::ReadStdLeafDouble() const
{
	return GetElement().FindAttributeDoubleValue("value", 0.0, true);
}

s64 parTreeNode::ReadStdLeafInt64() const
{
	return GetElement().FindAttributeInt64Value("value", 0, true);
}

const char* parTreeNode::ReadStdLeafString() const
{
	return GetData();
}

const char16* parTreeNode::ReadStdLeafWideString() const
{
	parAssertf(FindDataEncoding() == parStream::UTF16, "In node %s, found encoding %s and expected utf16", GetElement().GetName(), parStream::FindEncodingName(FindDataEncoding()));
	return reinterpret_cast<const char16*>(GetData());
}

Vector2 parTreeNode::ReadStdLeafVector2() const
{
	Vector2 vec(
		GetElement().FindAttributeFloatValue("x", 0.0f),
		GetElement().FindAttributeFloatValue("y", 0.0f)
		);
	return vec;
}

Vector3 parTreeNode::ReadStdLeafVector3() const
{
	Vector3 vec(
		GetElement().FindAttributeFloatValue("x", 0.0f),
		GetElement().FindAttributeFloatValue("y", 0.0f),
		GetElement().FindAttributeFloatValue("z", 0.0f)
		);
	return vec;
}

Vector4 parTreeNode::ReadStdLeafVector4() const
{
	Vector4 vec(
		GetElement().FindAttributeFloatValue("x", 0.0f),
		GetElement().FindAttributeFloatValue("y", 0.0f),
		GetElement().FindAttributeFloatValue("z", 0.0f),
		GetElement().FindAttributeFloatValue("w", 0.0f)
		);
	return vec;
}

void parTreeNode::ReadStdLeafMatrix34(Matrix34& value) const
{
	parStream::DataEncoding encoding = FindDataEncoding();
	if (encoding == parStream::MATRIX43)
	{
		FastAssert(HasData() && GetDataSize() == (sizeof(float) * 12));
		// column major encoding, row major matrix... need to transpose.
		const float* floats = reinterpret_cast<const float*>(GetData());

		value.a.x = floats[0];  value.a.y = floats[4];	value.a.z = floats[8];
		value.b.x = floats[1];	value.b.y = floats[5];	value.b.z = floats[9];
		value.c.x = floats[2];	value.c.y = floats[6];	value.c.z = floats[10];
		value.d.x = floats[3];	value.d.y = floats[7];	value.d.z = floats[11];
	}
	else
	{
		FastAssert(HasData() && GetDataSize() == (sizeof(Vector3) * 4));
		parAssertf(encoding == parStream::VECTOR3_ARRAY, "Found an unexpected encoding (%d) for a matrix", encoding);
		// row major encoding, row major matrix... ready to go
		const Vector3* vecs = reinterpret_cast<const Vector3*>(GetData());
		value.Set(vecs[0], vecs[1], vecs[2], vecs[3]);
	}

}

void parTreeNode::ReadStdLeafMatrix44(Matrix44& value) const
{
	parStream::DataEncoding encoding = FindDataEncoding();
	if (encoding == parStream::MATRIX44)
	{
		FastAssert(HasData() && GetDataSize() == (sizeof(float) * 12));
		// column major encoding, row major matrix... need to transpose.
		const float* floats = reinterpret_cast<const float*>(GetData());

		value.a.x = floats[0];  value.a.y = floats[4];	value.a.z = floats[8];		value.a.w = floats[12];
		value.b.x = floats[1];	value.b.y = floats[5];	value.b.z = floats[9];		value.b.w = floats[13];
		value.c.x = floats[2];	value.c.y = floats[6];	value.c.z = floats[10];		value.c.w = floats[14];
		value.d.x = floats[3];	value.d.y = floats[7];	value.d.z = floats[11];		value.d.w = floats[15];
	}
	else
	{
		FastAssert(HasData() && GetDataSize() == (sizeof(Vector4) * 4));
		parAssertf(encoding == parStream::VECTOR4_ARRAY, "Found an unexpected encoding (%d) for a matrix", encoding);
		const Vector4* vecs = reinterpret_cast<const Vector4*>(GetData());
		value.a.Set(vecs[0]);
		value.b.Set(vecs[1]);
		value.c.Set(vecs[2]);
		value.d.Set(vecs[3]);
	}
}

Vec2V parTreeNode::ReadStdLeafVec2() const
{
	Vec2V vec(
		GetElement().FindAttributeFloatValue("x", 0.0f),
		GetElement().FindAttributeFloatValue("y", 0.0f)
		);
	return vec;
}

Vec3V parTreeNode::ReadStdLeafVec3() const
{
	Vec3V vec(
		GetElement().FindAttributeFloatValue("x", 0.0f),
		GetElement().FindAttributeFloatValue("y", 0.0f),
		GetElement().FindAttributeFloatValue("z", 0.0f)
		);
	return vec;
}

Vec4V parTreeNode::ReadStdLeafVec4() const
{
	Vec4V vec(
		GetElement().FindAttributeFloatValue("x", 0.0f),
		GetElement().FindAttributeFloatValue("y", 0.0f),
		GetElement().FindAttributeFloatValue("z", 0.0f),
		GetElement().FindAttributeFloatValue("w", 0.0f)
		);
	return vec;
}

void parTreeNode::ReadStdLeafMat33(Mat33V& value) const
{
	parStream::DataEncoding encoding = FindDataEncoding();
	if (encoding == parStream::MATRIX33)
	{
		FastAssert(HasData() && GetDataSize() == sizeof(Mat33V));
		// column major encoding, column major matrix... ready to go
		value = *reinterpret_cast<const Mat33V*>(GetData());
	}
	else
	{
		// 'memory order' encoding, column major matrix... ready to go
		FastAssert(HasData() && GetDataSize() == sizeof(Mat33V));
		parAssertf(encoding == parStream::VECTOR3_ARRAY, "Found an unexpected encoding (%d) for a matrix", encoding);
		value = *reinterpret_cast<const Mat33V*>(GetData());
	}
}

void parTreeNode::ReadStdLeafMat34(Mat34V& value) const
{
	parStream::DataEncoding encoding = FindDataEncoding();
	if (encoding == parStream::MATRIX34)
	{
		FastAssert(HasData() && GetDataSize() == sizeof(Mat34V));
		// column major encoding, column major matrix... ready to go
		value = *reinterpret_cast<const Mat34V*>(GetData());
	}
	else if (encoding == parStream::VECTOR3_ARRAY)
	{
		// 'memory order' encoding, column major matrix... ready to go
		FastAssert(HasData() && GetDataSize() == sizeof(Mat34V));
		parAssertf(encoding == parStream::VECTOR3_ARRAY, "Found an unexpected encoding (%d) for a matrix", encoding);
		value = *reinterpret_cast<const Mat34V*>(GetData());
	}
}

void parTreeNode::ReadStdLeafMat44(Mat44V& value) const
{
	parStream::DataEncoding encoding = FindDataEncoding();
	if (encoding == parStream::MATRIX44)
	{
		FastAssert(HasData() && GetDataSize() == sizeof(Mat44V));
		// column major encoding, column major matrix... ready to go
		value = *reinterpret_cast<const Mat44V*>(GetData());
	}
	else
	{
		// 'memory order' encoding, column major matrix... ready to go
		FastAssert(HasData() && GetDataSize() == sizeof(Mat44V));
		parAssertf(encoding == parStream::VECTOR4_ARRAY, "Found an unexpected encoding (%d) for a matrix", encoding);
		value = *reinterpret_cast<const Mat44V*>(GetData());
	}
}

bool parTreeNode::FindValueFromPath(const char* path, bool& outVal)
{
	parAttribute* attr = NULL;
	parTreeNode* node = FindFromXPathInternal(path, &attr);
	if (attr)
	{
		outVal = attr->FindBoolValue();
		return true;
	}
	else if (node)
	{
		outVal = node->ReadStdLeafBool();
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, int& outVal)
{
	parAttribute* attr = NULL;
	parTreeNode* node = FindFromXPathInternal(path, &attr);
	if (attr)
	{
		outVal = attr->FindIntValue();
		return true;
	}
	else if (node)
	{
		outVal = node->ReadStdLeafInt();
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, float& outVal)
{
	parAttribute* attr = NULL;
	parTreeNode* node = FindFromXPathInternal(path, &attr);
	if (attr)
	{
		outVal = attr->FindFloatValue();
		return true;
	}
	else if (node)
	{
		outVal = node->ReadStdLeafFloat();
		return true;
	}
	return false;
}


bool parTreeNode::FindValueFromPath(const char* path, const char*& outVal, char* buffer /*= NULL*/, int size /*= 0*/)
{
	parAttribute* attr = NULL;
	parTreeNode* node = FindFromXPathInternal(path, &attr);
	if (attr)
	{
		if (buffer)
		{
			parAssertf(size >= 50, "Size must be 50 bytes or more to hold values converted from non-string types");
			outVal = attr->GetStringRepr(buffer, size);
		}
		else
		{
			if (attr->GetType() != parAttribute::STRING)
			{
				parErrorf("Can't get value of non-STRING attribute at path %s without an extra buffer", path);
				return false;
			}
			outVal = attr->GetStringValue();
		}
		return outVal != NULL;
	}
	else if (node)
	{
		outVal = node->ReadStdLeafString();
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, Vector2& outVal)
{
	parAssertf(!strchr(path, '@'), "Attribute specifiers aren't allowed when finding a complex value. Path was \"%s\"", path);
	parTreeNode* node = FindFromXPath(path);
	if (node)
	{
		outVal = node->ReadStdLeafVector2();
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, Vector3& outVal)
{
	parAssertf(!strchr(path, '@'), "Attribute specifiers aren't allowed when finding a complex value. Path was \"%s\"", path);
	parTreeNode* node = FindFromXPath(path);
	if (node)
	{
		outVal = node->ReadStdLeafVector3();
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, Vector4& outVal)
{
	parAssertf(!strchr(path, '@'), "Attribute specifiers aren't allowed when finding a complex value. Path was \"%s\"", path);
	parTreeNode* node = FindFromXPath(path);
	if (node)
	{
		outVal = node->ReadStdLeafVector4();
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, Vec2V& outVal)
{
	parAssertf(!strchr(path, '@'), "Attribute specifiers aren't allowed when finding a complex value. Path was \"%s\"", path);
	parTreeNode* node = FindFromXPath(path);
	if (node)
	{
		outVal = node->ReadStdLeafVec2();
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, Vec3V& outVal)
{
	parAssertf(!strchr(path, '@'), "Attribute specifiers aren't allowed when finding a complex value. Path was \"%s\"", path);
	parTreeNode* node = FindFromXPath(path);
	if (node)
	{
		outVal = node->ReadStdLeafVec3();
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, Vec4V& outVal)
{
	parAssertf(!strchr(path, '@'), "Attribute specifiers aren't allowed when finding a complex value. Path was \"%s\"", path);
	parTreeNode* node = FindFromXPath(path);
	if (node)
	{
		outVal = node->ReadStdLeafVec4();
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, Matrix34& outVal)
{
	parAssertf(!strchr(path, '@'), "Attribute specifiers aren't allowed when finding a complex value. Path was \"%s\"", path);
	parTreeNode* node = FindFromXPath(path);
	if (node)
	{
		node->ReadStdLeafMatrix34(outVal);
		return true;
	}
	return false;
}

bool parTreeNode::FindValueFromPath(const char* path, Matrix44& outVal) 
{
	parAssertf(!strchr(path, '@'), "Attribute specifiers aren't allowed when finding a complex value. Path was \"%s\"", path);
	parTreeNode* node = FindFromXPath(path);
	if (node)
	{
		node->ReadStdLeafMatrix44(outVal);
		return true;
	}
	return false;
}

parTreeNode* parTreeNode::Clone()
{
	parTreeNode* newNode = rage_new parTreeNode;
	newNode->GetElement().CopyFrom(m_Element);
	if (HasData())
	{
		newNode->SetData(GetData(), GetDataSize());
	}

	parTreeNode* currChild = NULL;
	for(ChildNodeIterator cni = BeginChildren(); cni != EndChildren(); ++cni)
	{
		parTreeNode* child = (*cni)->Clone();
		if (currChild == NULL)
		{
			child->InsertAsChildOf(newNode);
		}
		else
		{
			child->InsertAsSiblingOf(currChild);
		}
		currChild = child;
	}

	return newNode;
}

int parTreeNode::FindNumChildren() const
{
	int count = 0;
	for(ChildNodeIterator ci = BeginChildren(); ci != EndChildren(); ++ci)
	{
		count++;
	}
	return count;
}

parStream::DataEncoding parTreeNode::FindDataEncoding() const
{
	const parAttribute* attr = GetElement().FindAttribute("content");
	if (attr && attr->GetStringValue())
	{
		return parStream::FindEncoding(attr->GetStringValue());
	}
	return parStream::UNSPECIFIED;
}

void parTreeNode::SwapWith(parTreeNode& other, bool fixupLinkage)
{
	if (this == &other)
	{
		return;
	}

	m_DataArray.Swap(other.m_DataArray);
//	SwapEm(m_Parent, other.m_Parent);		// DO NOT swap not-custodial pointers
//	SwapEm(m_Sibling, other.m_Sibling);		// so that the tree keeps the same structure, just one node + its children changes
	m_Element.SwapWith(other.m_Element);

	if (fixupLinkage)
	{
		SwapEm(m_Child, other.m_Child);

		// all the children of other need to point to this, all the children of this need to point to other
		parTreeNode* oldThisKid = other.m_Child; // we swapped already, so use other.m_Child
		while(oldThisKid)
		{
			oldThisKid->m_Parent = &other;
			oldThisKid = oldThisKid->m_Sibling;
		}

		parTreeNode* oldOtherKid = this->m_Child;
		while(oldOtherKid)
		{
			oldOtherKid->m_Parent = this;
			oldOtherKid = oldOtherKid->m_Sibling;
		}
	}
}


// From http://stackoverflow.com/questions/7685/merge-sort-a-linked-list
// (and reformatted for clarity)
parTreeNode* parTreeNode::SortListRecursive(parTreeNode *list,NodeSortDel compare)
{
	if (!list || !list->GetSibling()) return list;  // Trivial case.

	parTreeNode* right=list;
	parTreeNode* temp=list;
	parTreeNode* last=list;
	parTreeNode* result=NULL;
	parTreeNode* next=NULL;
	parTreeNode* tail=NULL;

	// Find halfway through the list (by running two pointers, one at twice the speed of the other).
	while (temp && temp->GetSibling()) 
	{
		last = right;
		right = right->GetSibling();
		temp = temp->GetSibling()->GetSibling();
	}
	last->m_Sibling = NULL;

	// Recurse on the two smaller lists:
	list = SortListRecursive(list,compare);
	right = SortListRecursive(right,compare);

	// Merge:
	while (list || right)
	{
		// Take from empty lists, or compare:
		if (!right) {
			next = list;
			list = list->GetSibling();
		}
		else if (!list) {
			next = right;
			right = right->GetSibling();
		}
		else if (compare(*list,*right)<=0) {   // Doesn't match the stackoverflow code, but <= should give a stable sort
			next = list;
			list = list->GetSibling();
		}
		else {
			next = right;
			right = right->GetSibling();
		}
		if (!result) {
			result = next; 
		}
		else {
			tail->m_Sibling = next;
		}
		tail = next;
	}
	return result;
}

void parTreeNode::SortChildren(NodeSortDel fn)
{
	m_Child = SortListRecursive(GetChild(), fn);
}

int TreeSortCompareNames(const parTreeNode& a, const parTreeNode& b)
{
	return stricmp(a.GetElement().GetName(), b.GetElement().GetName());
}

struct TreeSortAttributeComparer {
	const char* m_AttrName;

	TreeSortAttributeComparer(const char* attrName) : m_AttrName(attrName) {}
	
	int CompareS64(const parTreeNode& a, const parTreeNode& b)
	{
		const parAttribute* attrA = a.GetElement().FindAttribute(m_AttrName);
		const parAttribute* attrB = b.GetElement().FindAttribute(m_AttrName);

		if (!attrA) 
		{
			return attrB ? 1 : 0; // NULL > b, or NULL == NULL
		}
		if (!attrB)
		{
			return -1; // a < NULL
		}

		s64 valueA = attrA->FindInt64Value();
		s64 valueB = attrB->FindInt64Value();
		return valueA < valueB ? -1 : (valueA == valueB ? 0 : 1);
	}

	int CompareU64(const parTreeNode& a, const parTreeNode& b)
	{
		const parAttribute* attrA = a.GetElement().FindAttribute(m_AttrName);
		const parAttribute* attrB = b.GetElement().FindAttribute(m_AttrName);

		if (!attrA) 
		{
			return attrB ? 1 : 0; // NULL > b, or NULL == NULL
		}
		if (!attrB)
		{
			return -1; // a < NULL
		}

		u64 valueA = (u64)attrA->FindInt64Value();
		u64 valueB = (u64)attrB->FindInt64Value();
		return valueA < valueB ? -1 : (valueA == valueB ? 0 : 1);
	}

	int CompareDouble(const parTreeNode& a, const parTreeNode& b)
	{
		const parAttribute* attrA = a.GetElement().FindAttribute(m_AttrName);
		const parAttribute* attrB = b.GetElement().FindAttribute(m_AttrName);

		if (!attrA) 
		{
			return attrB ? 1 : 0; // NULL > b, or NULL == NULL
		}
		if (!attrB)
		{
			return -1; // a < NULL
		}

		double valueA = attrA->FindDoubleValue();
		double valueB = attrB->FindDoubleValue();
		return valueA < valueB ? -1 : (valueA == valueB ? 0 : 1);
	}

	int CompareBool(const parTreeNode& a, const parTreeNode& b)
	{
		const parAttribute* attrA = a.GetElement().FindAttribute(m_AttrName);
		const parAttribute* attrB = b.GetElement().FindAttribute(m_AttrName);

		if (!attrA) 
		{
			return attrB ? 1 : 0; // NULL > b, or NULL == NULL
		}
		if (!attrB)
		{
			return -1; // a < NULL
		}

		bool valueA = attrA->FindBoolValue();
		bool valueB = attrB->FindBoolValue();
		return valueA ? (valueB ? 0 : 1) : (valueB ? -1 : 0); // false < true
	}

	int CompareString(const parTreeNode& a, const parTreeNode& b)
	{
		const parAttribute* attrA = a.GetElement().FindAttribute(m_AttrName);
		const parAttribute* attrB = b.GetElement().FindAttribute(m_AttrName);

		if (!attrA) 
		{
			return attrB ? 1 : 0; // NULL > b, or NULL == NULL
		}
		if (!attrB)
		{
			return -1; // a < NULL
		}

		char bufA[64];
		char bufB[64];
		const char* valueA = attrA->GetStringRepr(bufA, 64);
		const char* valueB = attrB->GetStringRepr(bufB, 64);
		return stricmp(valueA, valueB);
	}
};

void parTreeNode::SortChildrenByName()
{
	NodeSortDel del(&TreeSortCompareNames);
	SortChildren(del);
}

void parTreeNode::SortChildrenByAttribute(const char* attrName, parAttribute::Type sortAs, bool unsignedInts)
{
	TreeSortAttributeComparer c(attrName);
	NodeSortDel del;
	switch(sortAs)
	{
	case parAttribute::STRING:
		del = NodeSortDel(&c, &TreeSortAttributeComparer::CompareString);
		break;
	case parAttribute::INT64:
		if (unsignedInts)
		{
			del = NodeSortDel(&c, &TreeSortAttributeComparer::CompareU64);
		}
		else
		{
			del = NodeSortDel(&c, &TreeSortAttributeComparer::CompareS64);
		}
		break;
	case parAttribute::DOUBLE:
		del = NodeSortDel(&c, &TreeSortAttributeComparer::CompareDouble);
		break;
	case parAttribute::BOOL:
		del = NodeSortDel(&c, &TreeSortAttributeComparer::CompareBool);
		break;
	}
	SortChildren(del);
}


bool parTreeNodeUtils::ResolveXInclude(parTreeNode& node, parTreeNode*& outReplNode)
{
	bool resolvedSuccessfully = false;
	outReplNode = NULL;

	parAssertf(!strcmp(node.GetElement().GetName(), "xi:include"), "Only call ResolveXInclude on actual XInclude nodes");

	parTreeNode* parent = node.GetParent();		// may be NULL, if we are replacing the root node.

	const char* filename = NULL;
	node.FindValueFromPath("@href", filename);

	const char* parseType = "xml";
	node.FindValueFromPath("@parse", parseType);

	const char* xpointer = "";
	node.FindValueFromPath("@xpointer", xpointer);

	char xpath[RAGE_MAX_PATH];
	ExtractXPathFromXPointer(xpath, RAGE_MAX_PATH, xpointer);


	parTreeNode* fallback = node.FindFromXPath("xi:fallback");

	bool resourceError = false; // If this is true later we need to use the fallback instead

	// Are we parsing XML or text?
	bool parseXml = true;
	if (strcmp(parseType, "xml") != 0)
	{
		parAssertf(!strcmp(parseType, "text"), "Parse attribute must be \"xml\" or \"text\", not %s", parseType);
		parseXml = false;
	}

	if (parseXml)
	{
		parTree* includeTree = NULL;
		includeTree = PARSER.LoadTree(filename, ""); // Will do XInclude replacement internally on the loaded data

		if (!includeTree || !includeTree->GetRoot())
		{
			resourceError = true;
		}
		else
		{
			parTreeNode* includeNode = includeTree->GetRoot()->FindFromXPath(xpath);
			resolvedSuccessfully = true;

			if (includeNode)
			{
				node.SwapWith(*includeNode);
				outReplNode = &node;
			}
			else
			{
				delete &node; // deletes the xi:include
			}
		}

		delete includeTree; // includeTree contains includeNode, which may now have the old contents of 'node' - the xi:include
	}
	else
	{
		// Make sure that text really can go here
		parAssertf(parent && parent->FindNumChildren() == 1, "Can't include text here because the <xi:include> is the root element or has siblings");

#if __ASSERT
		parStream::DataEncoding parentEncoding = parent->FindDataEncoding();
		parAssertf(parentEncoding == parStream::UNSPECIFIED || parentEncoding == parStream::ASCII, "Destination for inclusion can currently only use ascii or unspecified encoding, found %s", parStream::FindEncodingName(parentEncoding));
#endif

		fiStream* stream = ASSET.Open(filename, "");
		if (stream)
		{
			fiSafeStream s(stream);
			int size = s->Size();

			char* data = rage_new char[size];
			s->Read(data, size);

			node.RemoveSelf();
			delete &node;		// the xi:include

			parent->AssumeData(data, size); // ascii, binary? not sure.				
			resolvedSuccessfully = true;
		}
		else
		{
			resourceError = true;
			// Don't delete node, we need it for later
		}
	}

	// We check this now instead of checking it in the parseXml and !parseXml blocks above because
	// XInclude lets you specify text content as a fallback even when parse="xml" and vice versa.
	// Not sure how useful that is for us, but its easy enough to support, just makes the logic a little
	// weirder than it would normally be.
	if (resourceError)
	{
		if (fallback)
		{
			fallback->RemoveSelf(); // remove from the tree so we don't forget to do it later

			if (fallback->HasData())
			{
				// Fallback is text, make sure we can replace node with text
				parAssertf(parent && parent->FindNumChildren() == 1, "Can't include text here because the <xi:include> is the root element or has siblings");

				node.RemoveSelf();

#if __ASSERT
				parStream::DataEncoding encoding = fallback->FindDataEncoding();
				parAssertf(encoding == parStream::UNSPECIFIED || encoding == parStream::ASCII, "Invalid encoding for fallback. Only ascii data allowed for now, found %s", parStream::FindEncodingName(encoding));
#endif

				parent->SetData(fallback->GetData(), fallback->GetDataSize());

				delete &node; // the xi:include
			}
			else
			{
				parAssertf(fallback->FindNumChildren() <= 1, "Currently only one element per XInclude is supported");

				parTreeNode* includeNode = fallback->GetChild();
				if (includeNode)
				{
					node.SwapWith(*includeNode);
					outReplNode = &node;
				}

				delete includeNode; // old contents of node (i.e. xi:include)
			}

			delete fallback;
		}
		else
		{
			// signal an error here?
			parErrorf("Couldn't resolve XInclude for href=\"%s\" and no fallback was provided", filename);

			node.RemoveSelf();
			delete &node; // the xi:include
		}
	}


	return resolvedSuccessfully;
}

void parTreeNodeUtils::FindAndResolveAllXIncludes(parTreeNode& node)
{
	// simple DFS to find everything...

	if (!strcmp(node.GetElement().GetName(), "xi:include"))
	{
		parTreeNode* replNode = NULL;
		bool resolvedSuccessfully = ResolveXInclude(node, replNode);
		if (!resolvedSuccessfully && replNode) // We had to use a fallback, so check if the fallback has xincludes
		{
			FindAndResolveAllXIncludes(*replNode);
		}
	}
	else
	{
		parTreeNode* kid = node.GetChild();
		parTreeNode* next = NULL;
		while(kid)
		{
			next = kid->GetSibling(); // get the next node early, cause XInclude processing could mean 'kid' doesn't exist anymore so we can't deref it
			FindAndResolveAllXIncludes(*kid);
			kid = next;
		}
	}
}

void parTreeNodeUtils::ExtractXPathFromXPointer( char * xpath, int bufSize, const char* xpointer )
{
	xpath[0] = '\0';
	const size_t ptrTypeLen = strlen("xpointer(");
	if (!strncmp(xpointer, "xpointer(", ptrTypeLen))
	{
		int destIndex = 0;
		const char* src = xpointer + ptrTypeLen;
		while(destIndex < bufSize - 1 && *src != ')')
		{
			parAssertf(*src != '(', "Parenthesis aren't allowed in rage XPath expressions");
			xpath[destIndex] = *src;
			++src;
			++destIndex;
		}
		xpath[destIndex] = '\0';
	}
}