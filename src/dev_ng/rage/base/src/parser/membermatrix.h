// 
// parser/membermatrix.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MEMBERMATRIX_H
#define PARSER_MEMBERMATRIX_H

#include "member.h"
#include "membermatrixdata.h"

namespace rage
{

// PURPOSE: A parMember subclass for describing Matrix data types
class parMemberMatrix : public parMember
{
public:
	typedef parMemberMatrixSubType::Enum SubType;
	typedef parMemberMatrixData Data;

	parMemberMatrix() {}
	explicit parMemberMatrix(Data& data) : parMember(&data) {}
	virtual ~parMemberMatrix() {}

	virtual void ReadTreeNode(parTreeNode* node, parPtrToStructure structAddr) const;


	virtual size_t GetSize() const;

public:

	STANDARD_PARMEMBER_DATA_FUNCS_DECL(parMemberMatrix);

#if PARSER_USES_EXTERNAL_STRUCTURE_DEFNS
	PAR_PARSABLE;
#endif
};

// This can't be defined until the derived class is.
inline parMemberMatrix*	parMember::AsMatrix()			{ return parMemberType::IsMatrix(GetType()) ? smart_cast<parMemberMatrix*>(this) : NULL; }

}

#endif
