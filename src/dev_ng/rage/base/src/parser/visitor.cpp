// 
// parser/visitor.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "visitor.h"

#include "optimisations.h"
#include "structure.h"

#include "diag/output.h"
#include "system/typeinfo.h"

PARSER_OPTIMISATIONS();

using namespace rage;

parInstanceVisitor::parInstanceVisitor()
	: m_SkipSiblings(false)
	, m_EndTraversal(false)
	, m_ContainingStructureAddress(NULL)
	, m_ContainingStructureMetadata(NULL)
{
	m_VisitMask.SetBits(0xFFFF);
	m_TypeMask.SetBits(0xFFFFFFFF);
}

void parInstanceVisitor::VisitToplevelStructure(parPtrToStructure instanceDataPtr, parStructure& metadata)
{
	bool recurse = BeginToplevelStruct(instanceDataPtr, metadata);
	if (recurse)
	{
		VisitStructure(instanceDataPtr, metadata);
	}
	EndToplevelStruct(instanceDataPtr, metadata);
}

#if PARSER_ALL_METADATA_HAS_NAMES
const char* parInstanceVisitor::PrintContext(void* arg1, void* /*arg2*/)
{
	parInstanceVisitor* visitor = reinterpret_cast<parInstanceVisitor*>(arg1);

	const int maxPathLen = 256;
	static char messageBuf[maxPathLen];
	int len = 0;
	for(int i = 0; i < visitor->m_Path.GetCount() && len < maxPathLen; i++)
	{
		len += formatf_n(messageBuf + len, maxPathLen - len, "/%s", visitor->m_Path[i].GetCStr());
	}
	return &messageBuf[0];
}
#endif

void parInstanceVisitor::VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
{
	// see if the metadata specifies a different visitation function
	if (metadata.HasDelegate("Visitor"))
	{
		// If so, use that structures visitor function instead of the build in one.
		// it's the callee's responsibility to call the right virtual functions
		metadata.Invoke(dataPtr, "Visitor", this);
		return;
	}


#if PARSER_ALL_METADATA_HAS_NAMES
	if (m_Path.IsEmpty())
	{
		BANK_ONLY(diagContextMessage::Push("Visiting parsable object %s", (char*)this, NULL, PrintContext));
	}
	bool pushed = !m_Path.IsFull();
	if (pushed)
	{
		m_Path.Push(atLiteralHashString(metadata.GetNameHash()));
	}
#endif

	parStructure::BaseList baseInfo;
	baseInfo.Init(&metadata, dataPtr);

	bool prevSkipSiblings = m_SkipSiblings;
	m_SkipSiblings = false;

	// go from base structure to derived
	for(int baseNum = baseInfo.GetNumBases()-1; baseNum >= 0 && !m_SkipSiblings && !m_EndTraversal; --baseNum)
	{
		parStructure* currMetadata = baseInfo.GetBaseStructure(baseNum);
		int numMembers = currMetadata->GetNumMembers();
		for(int i = 0; i < numMembers && !m_SkipSiblings && !m_EndTraversal; i++) {
			parMember* member = currMetadata->GetMember(i);
			if (m_TypeMask.IsSet(member->GetType()))
			{
				m_ContainingStructureMetadata = &metadata;
				parAssertf(member, "Couldn't get member %d from structure %s", i, metadata.GetName());
				VisitMember(baseInfo.GetBaseAddress(baseNum), *member);
			}
		}
	}

	m_SkipSiblings = prevSkipSiblings;
	m_ContainingStructureMetadata = NULL;

#if PARSER_ALL_METADATA_HAS_NAMES
	if (pushed)
	{
		m_Path.Pop();
	}
	if (m_Path.IsEmpty())
	{
		BANK_ONLY(diagContextMessage::Pop());
	}
#endif
}

void parInstanceVisitor::VisitMember(parPtrToStructure containingStructAddr, parMember& metadata)
{
	if (!m_VisitMask.DoesOverlap(metadata.GetVisitFlags()))
	{
		return;
	}

#if PARSER_ALL_METADATA_HAS_NAMES
	if (m_Path.IsEmpty())
	{
		BANK_ONLY(diagContextMessage::Push("Visiting parsable object %s", (char*)this, NULL, PrintContext));
	}
	bool pushed = !m_Path.IsFull();
	if (pushed)
	{
		m_Path.Push(atLiteralHashString(metadata.GetNameHash()));
	}
#endif

	m_ContainingStructureAddress = containingStructAddr;

	switch(metadata.GetType())
	{
	case parMemberType::TYPE_BOOL:		BoolMember(		metadata.GetMemberFromStruct<bool>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_CHAR:		CharMember(		metadata.GetMemberFromStruct<s8>(containingStructAddr),			metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_UCHAR:		UCharMember(	metadata.GetMemberFromStruct<u8>(containingStructAddr),			metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_SHORT:		ShortMember(	metadata.GetMemberFromStruct<s16>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_USHORT:	UShortMember(	metadata.GetMemberFromStruct<u16>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_INT:		IntMember(		metadata.GetMemberFromStruct<s32>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_UINT:		UIntMember(		metadata.GetMemberFromStruct<u32>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_PTRDIFFT:	IntMember(		metadata.GetMemberFromStruct<ptrdiff_t>(containingStructAddr),	metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_SIZET:		UIntMember(		metadata.GetMemberFromStruct<size_t>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_FLOAT:		FloatMember(	metadata.GetMemberFromStruct<float>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_FLOAT16:	Float16Member(	metadata.GetMemberFromStruct<Float16>(containingStructAddr),	metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_SCALARV:	ScalarVMember(	metadata.GetMemberFromStruct<ScalarV>(containingStructAddr),	metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_BOOLV:		BoolVMember(	metadata.GetMemberFromStruct<BoolV>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_VECTOR2:	Vector2Member(	metadata.GetMemberFromStruct<Vector2>(containingStructAddr),	metadata.AsVectorRef()); break;
	case parMemberType::TYPE_VECTOR3:	Vector3Member(	metadata.GetMemberFromStruct<Vector3>(containingStructAddr),	metadata.AsVectorRef()); break;
	case parMemberType::TYPE_VECTOR4:	Vector4Member(	metadata.GetMemberFromStruct<Vector4>(containingStructAddr),	metadata.AsVectorRef()); break;
	case parMemberType::TYPE_VEC2V:		Vec2VMember(	metadata.GetMemberFromStruct<Vec2V>(containingStructAddr),		metadata.AsVectorRef()); break;
	case parMemberType::TYPE_VEC3V:		Vec3VMember(	metadata.GetMemberFromStruct<Vec3V>(containingStructAddr),		metadata.AsVectorRef()); break;
	case parMemberType::TYPE_VEC4V:		Vec4VMember(	metadata.GetMemberFromStruct<Vec4V>(containingStructAddr),		metadata.AsVectorRef()); break;
	case parMemberType::TYPE_VECBOOLV:	VecBoolVMember(	metadata.GetMemberFromStruct<VecBoolV>(containingStructAddr),	metadata.AsVectorRef()); break;
	case parMemberType::TYPE_MATRIX34:	Matrix34Member(	metadata.GetMemberFromStruct<Matrix34>(containingStructAddr),	metadata.AsMatrixRef()); break;
	case parMemberType::TYPE_MATRIX44:	Matrix44Member(	metadata.GetMemberFromStruct<Matrix44>(containingStructAddr),	metadata.AsMatrixRef()); break;
	case parMemberType::TYPE_MAT33V:	Mat33VMember(	metadata.GetMemberFromStruct<Mat33V>(containingStructAddr),		metadata.AsMatrixRef()); break;
	case parMemberType::TYPE_MAT34V:	Mat34VMember(	metadata.GetMemberFromStruct<Mat34V>(containingStructAddr),		metadata.AsMatrixRef()); break;
	case parMemberType::TYPE_MAT44V:	Mat44VMember(	metadata.GetMemberFromStruct<Mat44V>(containingStructAddr),		metadata.AsMatrixRef()); break;
	case parMemberType::TYPE_INT64:		IntMember(		metadata.GetMemberFromStruct<s64>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_UINT64:	UIntMember(		metadata.GetMemberFromStruct<u64>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_DOUBLE:	DoubleMember(	metadata.GetMemberFromStruct<double>(containingStructAddr),		metadata.AsSimpleRef()); break;
	case parMemberType::TYPE_STRING:
		{
			parMemberString& memberString = metadata.AsStringRef();
			switch((parMemberString::SubType)memberString.GetSubtype())
			{
			case parMemberStringSubType::SUBTYPE_CONST_STRING:
				ConstStringMember(metadata.GetMemberFromStruct<ConstString>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_MEMBER:
				MemberStringMember(&metadata.GetMemberFromStruct<char>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_POINTER:
				PointerStringMember(metadata.GetMemberFromStruct<char*>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_ATSTRING:
				AtStringMember(metadata.GetMemberFromStruct<atString>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_WIDE_MEMBER:
				WideMemberStringMember(&metadata.GetMemberFromStruct<char16>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_WIDE_POINTER:
				WidePointerStringMember(metadata.GetMemberFromStruct<char16*>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_ATWIDESTRING:
				AtWideStringMember(metadata.GetMemberFromStruct<atWideString>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
#if !__FINAL
				AtNonFinalHashStringMember(metadata.GetMemberFromStruct<atHashString>(containingStructAddr), memberString);
#endif
				break;
			case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
				AtFinalHashStringMember(metadata.GetMemberFromStruct<atFinalHashString>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
				AtHashValueMember(metadata.GetMemberFromStruct<atHashValue>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
				AtPartialHashValueMember(metadata.GetMemberFromStruct<u32>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
				AtNamespacedHashStringMember(metadata.GetMemberFromStruct<atNamespacedHashStringBase>(containingStructAddr), memberString);
				break;
			case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
				AtNamespacedHashValueMember(metadata.GetMemberFromStruct<atNamespacedHashValueBase>(containingStructAddr), memberString);
				break;
			}
		}
		break;

	case parMemberType::TYPE_ENUM:
		{
			parMemberEnum& memberEnum = metadata.AsEnumRef();
			switch(memberEnum.GetSubtype())
			{
			case parMemberEnumSubType::SUBTYPE_32BIT:
				// just call it
				EnumMember(metadata.GetMemberFromStruct<int>(containingStructAddr), memberEnum);
				break;
			case parMemberEnumSubType::SUBTYPE_16BIT:
				{
					int data = metadata.GetMemberFromStruct<u16>(containingStructAddr);
					EnumMember(data, memberEnum);
					if (data != metadata.GetMemberFromStruct<u16>(containingStructAddr))
					{
						metadata.GetMemberFromStruct<u16>(containingStructAddr) = (u16)data;
					}
				}
				break;
			case parMemberEnumSubType::SUBTYPE_8BIT:
				{
					int data = metadata.GetMemberFromStruct<u8>(containingStructAddr);
					EnumMember(data, memberEnum);
					if (data != metadata.GetMemberFromStruct<u8>(containingStructAddr))
					{
						metadata.GetMemberFromStruct<u8>(containingStructAddr) = (u8)data;
					}
				}
				break;
			}
		}
		break;

	case parMemberType::TYPE_BITSET:
		{
			parMemberBitset& memberBitset = metadata.AsBitsetRef();
			parPtrToArray contents;
			u32 numBits;
			memberBitset.GetBitsetContentsAndCountFromStruct(containingStructAddr, contents, numBits);
			BitsetMember(memberBitset.GetPointerToMember(containingStructAddr), contents, numBits, memberBitset);
		}
		break;

	case parMemberType::TYPE_STRUCT:
		{
			parMemberStruct& memberStruct = metadata.AsStructOrPointerRef();
			switch((parMemberStruct::SubType)memberStruct.GetSubtype())
			{
			case parMemberStructSubType::SUBTYPE_STRUCTURE:
				{
					parPtrToStructure addressOfStructure = memberStruct.GetObjAddr(containingStructAddr);
					bool recurse = BeginStructMember(addressOfStructure, memberStruct);
					if (recurse) {
						VisitStructure(addressOfStructure, *memberStruct.GetConcreteStructure(addressOfStructure));
					}
					EndStructMember(addressOfStructure, memberStruct);
				}
				break;
			case parMemberStructSubType::SUBTYPE_POINTER:
			case parMemberStructSubType::SUBTYPE_SIMPLE_POINTER:
				{				
					parPtrToStructure& pointerRef = memberStruct.GetMemberFromStruct<parPtrToStructure>(containingStructAddr); // Can't use GetObjAddr because I need a reference to the pointer here
					bool recurse = BeginPointerMember(pointerRef, memberStruct);
					if (recurse && pointerRef != NULL)
					{
						VisitStructure(pointerRef, *memberStruct.GetConcreteStructure(pointerRef));
					}
					EndPointerMember(pointerRef, memberStruct);
				}
				break;
			case parMemberStructSubType::SUBTYPE_EXTERNAL_NAMED_POINTER:
			case parMemberStructSubType::SUBTYPE_EXTERNAL_NAMED_POINTER_USERNULL:
				ExternalPointerMember(memberStruct.GetMemberFromStruct<void*>(containingStructAddr), memberStruct);
				break;
			}
		}
		break;
	
	case parMemberType::TYPE_ARRAY:
		{
			parPtrToArray arrayItemsAddress;
			size_t numItems;
			parMemberArray& memberArray = metadata.AsArrayRef();

			if (Unlikely(memberArray.GetSubtype() == parMemberArraySubType::SUBTYPE_VIRTUAL))
			{
				// No visiting virtual array members, they don't really exist.
				parWarningf("Can't visit virtual array elements (member %s in structure %s)", metadata.GetName(), m_ContainingStructureMetadata->GetName());
				break;
			}

			memberArray.GetArrayContentsAndCountFromStruct(containingStructAddr, arrayItemsAddress, numItems);
			parPtrToMember memberAddress = memberArray.GetPointerToMember(containingStructAddr);

			bool recurse = true;

			recurse = BeginArrayMember(memberAddress, arrayItemsAddress, numItems, memberArray);

			parStructure* oldContainingStructureMetadata = m_ContainingStructureMetadata;

			if (recurse && m_TypeMask.IsSet(memberArray.GetPrototypeMember()->GetType()))
			{
				// BeginArrayMember may have modified the array - so get the arrayItemsAddress and numItems again, to make sure we're up to date
				memberArray.GetArrayContentsAndCountFromStruct(containingStructAddr, arrayItemsAddress, numItems);

				bool prevSkipSiblings = m_SkipSiblings;
				m_SkipSiblings = false;

#if !__NO_OUTPUT
				if ((int)numItems < 0)
				{
					parErrorf("Found an invalid num items for array %s::%s: %" SIZETFMT "d (0x%" SIZETFMT "x)", m_ContainingStructureMetadata->GetName(), memberArray.GetName(), numItems, numItems);
				}
#endif

				for(int i = 0; i < (int)numItems; i++)
				{
					parPtrToStructure elementAddr = memberArray.GetAddressOfNthElement(arrayItemsAddress, i);
					VisitMember(elementAddr, *(memberArray.GetPrototypeMember()));

					if (m_SkipSiblings || m_EndTraversal)
					{
						break;
					}
				}

				m_SkipSiblings = prevSkipSiblings;
			}

			// VisitMember could clear these - EndArrayMember might need them
			m_ContainingStructureAddress = containingStructAddr;
			m_ContainingStructureMetadata = oldContainingStructureMetadata;

			EndArrayMember(memberAddress, arrayItemsAddress, numItems, memberArray);
		}
		break;
    case parMemberType::TYPE_MAP:
        {
            parMemberMap& memberMap = metadata.AsMapRef();

            bool recurse = true;
            recurse = BeginMapMember(containingStructAddr, memberMap);

            parStructure* oldContainingStructureMetadata = m_ContainingStructureMetadata;

            if (
                recurse && 
                (m_TypeMask.IsSet(memberMap.GetKeyMember()->GetType()) || 
                m_TypeMask.IsSet(memberMap.GetDataMember()->GetType()))
               )
            {
                bool prevSkipSiblings = m_SkipSiblings;
                m_SkipSiblings = false;

                parMemberMapIterator* memberMapIterator = memberMap.CreateIterator(containingStructAddr);
                memberMapIterator->Begin();

                while(!memberMapIterator->AtEnd())
                {
                    VisitMapMember(containingStructAddr, memberMapIterator->GetKeyPtr(), memberMapIterator->GetDataPtr(), memberMap);

                    if (m_SkipSiblings || m_EndTraversal)
                    {
                        break;
                    }

                    memberMapIterator->Next();
                }
                delete memberMapIterator;

                m_SkipSiblings = prevSkipSiblings;
            }

            // VisitMember could clear these - EndMapMember might need them
            m_ContainingStructureAddress = containingStructAddr;
            m_ContainingStructureMetadata = oldContainingStructureMetadata;

            EndMapMember(containingStructAddr, memberMap);
        }
        break;
	case parMemberType::INVALID_TYPE:
		parAssertf(0 , "Don't know what to do with an invalid member");
		break;

	}

	m_ContainingStructureAddress = NULL;

#if PARSER_ALL_METADATA_HAS_NAMES
	if (pushed)
	{
		m_Path.Pop();
	}
	if (m_Path.IsEmpty())
	{
		BANK_ONLY(diagContextMessage::Pop());
	}
#endif

}

void parInstanceVisitor::AnyMember(parPtrToMember /*dataPtr*/, parMember& /*metadata*/)
{
}

void parInstanceVisitor::SimpleMember(parPtrToMember dataPtr, parMemberSimple& metadata)
{
	AnyMember(dataPtr, metadata);
}

void parInstanceVisitor::VectorMember(parPtrToMember dataPtr, parMemberVector& metadata)
{
	AnyMember(dataPtr, metadata);
}

void parInstanceVisitor::MatrixMember(parPtrToMember dataPtr, parMemberMatrix& metadata)
{
	AnyMember(dataPtr, metadata);
}

void parInstanceVisitor::BoolMember(bool& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::CharMember(s8& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::UCharMember(u8& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::ShortMember(s16& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::UShortMember(u16& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::IntMember(s32& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::IntMember(s64& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::UIntMember(u32& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::UIntMember(u64& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::FloatMember(float& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::DoubleMember(double& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Float16Member(Float16& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::ScalarVMember(ScalarV& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);
}

void parInstanceVisitor::BoolVMember(BoolV& data, parMemberSimple& metadata)
{
	SimpleMember((parPtrToMember)&data, metadata);
}

void parInstanceVisitor::VecBoolVMember(VecBoolV& data, parMemberVector& metadata)
{
	VectorMember((parPtrToMember)&data, metadata);
}

void parInstanceVisitor::Vec2VMember(Vec2V& data, parMemberVector& metadata)
{
	VectorMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Vec3VMember(Vec3V& data, parMemberVector& metadata)
{
	VectorMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Vec4VMember(Vec4V& data, parMemberVector& metadata)
{
	VectorMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Vector2Member(Vector2& data, parMemberVector& metadata)
{
	VectorMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Vector3Member(Vector3& data, parMemberVector& metadata)
{
	VectorMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Vector4Member(Vector4& data, parMemberVector& metadata)
{
	VectorMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Mat33VMember(Mat33V& data, parMemberMatrix& metadata)
{
	MatrixMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Mat34VMember(Mat34V& data, parMemberMatrix& metadata)
{
	MatrixMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Mat44VMember(Mat44V& data, parMemberMatrix& metadata)
{
	MatrixMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Matrix34Member(Matrix34& data, parMemberMatrix& metadata)
{
	MatrixMember((parPtrToMember)&data, metadata);	
}

void parInstanceVisitor::Matrix44Member(Matrix44& data, parMemberMatrix& metadata)
{
	MatrixMember((parPtrToMember)&data, metadata);	
}

bool parInstanceVisitor::BeginStructMember(parPtrToStructure /*structAddr*/, parMemberStruct& metadata)
{
	AnyMember(metadata.GetPointerToMember(m_ContainingStructureAddress), metadata); return true;
}

void parInstanceVisitor::EndStructMember(parPtrToStructure /*structAddr*/, parMemberStruct& /*metadata*/)
{
}

bool parInstanceVisitor::BeginPointerMember(parPtrToStructure& ptr, parMemberStruct& metadata)
{
	if (ptr) {
		return BeginStructMember(ptr, metadata);
	}
	return true;
}

void parInstanceVisitor::EndPointerMember(parPtrToStructure& ptr, parMemberStruct& metadata)
{
	if (ptr) {
		EndStructMember(ptr, metadata);
	}
}

bool parInstanceVisitor::BeginToplevelStruct(parPtrToStructure /*structAddr*/, parStructure& /*metadata*/)
{
	return true;
}

void parInstanceVisitor::EndToplevelStruct(parPtrToStructure /*structAddr*/, parStructure& /*metadata*/)
{
}

void parInstanceVisitor::StringMember(parPtrToMember ptrToMember, const char* /*ptrToString*/, parMemberString& metadata)
{
	AnyMember(ptrToMember, metadata);
}

void parInstanceVisitor::MemberStringMember(char* stringData,	parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), stringData, metadata);
}

void parInstanceVisitor::PointerStringMember(char*& stringData,	parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), stringData, metadata);
}

void parInstanceVisitor::ConstStringMember(ConstString& stringData, parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), stringData.m_String, metadata);
}

void parInstanceVisitor::AtStringMember(atString& stringData, parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), stringData.c_str(), metadata);
}

#if !__FINAL
void parInstanceVisitor::AtNonFinalHashStringMember(atHashString& stringData, parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), stringData.GetCStr(), metadata);
}
#endif	//	!__FINAL

void parInstanceVisitor::AtFinalHashStringMember(atFinalHashString& stringData, parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), stringData.GetCStr(), metadata);
}

void parInstanceVisitor::AtHashValueMember(atHashValue& stringData, parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), NULL, metadata);
}

void parInstanceVisitor::AtPartialHashValueMember(u32& stringData, parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), NULL, metadata);
}

void parInstanceVisitor::WideStringMember(parPtrToMember ptrToMember, const char16* /*ptrToString*/, parMemberString& metadata)
{
	AnyMember(ptrToMember, metadata);
}

void parInstanceVisitor::AtWideStringMember(atWideString& stringData, parMemberString& metadata)
{
	WideStringMember(reinterpret_cast<parPtrToMember>(&stringData), (const char16*)stringData, metadata);
}

void parInstanceVisitor::WideMemberStringMember(char16* stringData, parMemberString& metadata)
{
	WideStringMember(reinterpret_cast<parPtrToMember>(&stringData), stringData, metadata);
}

void parInstanceVisitor::WidePointerStringMember(char16*& stringData, parMemberString& metadata)
{
	WideStringMember(reinterpret_cast<parPtrToMember>(&stringData), stringData, metadata);
}

void parInstanceVisitor::AtNamespacedHashStringMember(atNamespacedHashStringBase& stringData, parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), atHashStringNamespaceSupport::GetString(metadata.GetData()->GetNamespaceIndex(), stringData.GetHash()), metadata);
}

void parInstanceVisitor::AtNamespacedHashValueMember(atNamespacedHashValueBase& stringData, parMemberString& metadata)
{
	StringMember(reinterpret_cast<parPtrToMember>(&stringData), NULL, metadata);
}

void parInstanceVisitor::EnumMember(int& /*data*/, parMemberEnum& metadata)
{
	// 'data' could be a temporary. Use GetPointerToMember to get the real address
	AnyMember(metadata.GetPointerToMember(m_ContainingStructureAddress), metadata); // getting the object as a char& should give us the same addr regardless of actual type
}

void parInstanceVisitor::BitsetMember(parPtrToMember ptrToMember, parPtrToArray /*ptrToBits*/, size_t /*numBits*/, parMemberBitset& metadata)
{
	AnyMember(ptrToMember, metadata);
}

bool parInstanceVisitor::BeginArrayMember(parPtrToMember ptrToMember,	parPtrToArray /*arrayContents*/, size_t /*numElements*/, parMemberArray& metadata)
{
	AnyMember(ptrToMember, metadata); return true;
}

void parInstanceVisitor::EndArrayMember(parPtrToMember /*ptrToMember*/, parPtrToArray /*arrayContents*/, size_t /*numElements*/, parMemberArray& /*metadata*/)
{
}

bool parInstanceVisitor::BeginMapMember	(parPtrToStructure structAddr,	parMemberMap& metadata)
{
	//TODO: WHy are we doing this and not just passing a pointer to the member?
	AnyMember(metadata.GetPointerToMember(structAddr), metadata); return true;
}

void parInstanceVisitor::EndMapMember (parPtrToStructure /*structAddr*/, parMemberMap& /*metadata*/)
{

}

void parInstanceVisitor::VisitMapMember (parPtrToStructure /*structAddr*/, parPtrToStructure mapKeyAddress, parPtrToStructure mapDataAddress, parMemberMap& metadata)
{
    VisitMember(mapKeyAddress, *(metadata.GetKeyMember()));
    VisitMember(mapDataAddress, *(metadata.GetDataMember()));
}

void parInstanceVisitor::ExternalPointerMember(void*& data, parMemberStruct& metadata)
{
	AnyMember(reinterpret_cast<parPtrToMember>(&data), metadata);
}

