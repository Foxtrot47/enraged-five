<?xml version="1.0"?>

<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">
  
  <structdef type="::rage::parMember" name="parMember" constructable="false" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS" onPostSave="PostSave">
  </structdef>
  
  <enumdef type="::rage::parMember::Type">
    <enumval name="BOOL"/>
    <enumval name="CHAR"/>
    <enumval name="UCHAR"/>
    <enumval name="SHORT"/>
    <enumval name="USHORT"/>
    <enumval name="INT"/>
    <enumval name="UINT"/>
    <enumval name="FLOAT"/>
    <enumval name="VECTOR2"/>
    <enumval name="VECTOR3"/>
    <enumval name="VECTOR4"/>
    <enumval name="STRING"/>
    <enumval name="STRUCT"/>
    <enumval name="ARRAY"/>
    <enumval name="ENUM"/>
    <enumval name="BITSET"/>
    <enumval name="MAP"/>
    <enumval name="MATRIX34"/>
    <enumval name="MATRIX44"/>
    <enumval name="VEC2V"/>
    <enumval name="VEC3V"/>
    <enumval name="VEC4V"/>
    <enumval name="MAT33V"/>
    <enumval name="MAT34V"/>
    <enumval name="MAT44V"/>
    <enumval name="SCALARV"/>
    <enumval name="BOOLV"/>
    <enumval name="VECBOOLV"/>
    <enumval name="PTRDIFFT"/>
    <enumval name="SIZET"/>
    <enumval name="FLOAT16"/>
    <enumval name="INT64"/>
    <enumval name="UINT64"/>
    <enumval name="DOUBLE"/>
  </enumdef>

  <enumdef type="::rage::parMemberArray::Subtype">
    <enumval name="ATARRAY"/>
    <enumval name="ATFIXEDARRAY"/>
    <enumval name="ATRANGEARRAY"/>
    <enumval name="POINTER"/>
    <enumval name="MEMBER"/>
    <enumval name="ATARRAY_32BIT_INDEX"/>
    <enumval name="POINTER_WITH_COUNT"/>
    <enumval name="POINTER_WITH_COUNT_8BIT_IDX"/>
    <enumval name="POINTER_WITH_COUNT_16BIT_IDX"/>
    <enumval name="VIRTUAL"/>
  </enumdef>

  <structdef type="::rage::parMemberArrayData" onPreLoad="PreLoad" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameHash"/>
    <ptrdiff_t name="m_Offset"/>
    <enum name="m_Type" type="::rage::parMember::Type" size="8"/>
    <enum name="m_Subtype" type="::rage::parMemberArray::Subtype" size="8"/>
    <u16 name="m_Flags"/>
    <u16 name="m_VisitFlags"/>
    <u16 name="m_TypeFlags"/>
    <pointer name="m_ExtraAttributes" type="::rage::parAttributeList" policy="simple_owner"/>
    <size_t name="m_ElementSize"/>
    <u32 name="m_NumElements"/>
  </structdef>

  <structdef type="::rage::parMemberArray" name="parMemberArray" base="::rage::parMember" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <pointer name="m_Data" type="::rage::parMemberArrayData" policy="simple_owner"/>
    <pointer name="m_PrototypeMember" type="::rage::parMember" policy="owner"/>
  </structdef>


  <enumdef type="::rage::parMemberEnum::Subtype">
    <enumval name="32BIT"/>
    <enumval name="16BIT"/>
    <enumval name="8BIT"/>
    <enumval name="ATBITSET"/>
    <!-- Not really one of the subtypes but it lets us combine the enum data with the bitset data easier -->
  </enumdef>

  <enumdef type="::rage::parEnumFlags::Enum">
    <enumval name="ENUM_STATIC"/>
    <enumval name="ENUM_HAS_NAMES"/>
    <enumval name="ENUM_ALWAYS_HAS_NAMES"/>
  </enumdef>

  <structdef type="::rage::parEnumListEntry" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameKey"/>
    <int name="m_Value"/>
  </structdef>

  <structdef type="::rage::parEnumData" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <array name="m_Enums" type="pointer" sizeVar="m_NumEnums">
      <struct type="::rage::parEnumListEntry"/>
    </array>
    <array name="m_Names" type="pointer" sizeVar="m_NumEnums">
      <string type="pointer"/>
    </array>
    <u16 name="m_NumEnums"/>
    <bitset name="m_Flags" type="fixed16" values="::rage::parEnumFlags::Enum"/>
    <u32 name="m_NameHash"/>
  </structdef>

  <structdef type="::rage::parMemberEnumData" onPreLoad="PreLoad" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameHash"/>
    <ptrdiff_t name="m_Offset"/>
    <enum name="m_Type" type="::rage::parMember::Type" size="8"/>
    <enum name="m_Subtype" type="::rage::parMemberEnum::Subtype" size="8"/>
    <u16 name="m_Flags"/>
    <u16 name="m_VisitFlags"/>
    <u16 name="m_TypeFlags"/>
    <pointer name="m_ExtraAttributes" type="::rage::parAttributeList" policy="simple_owner"/>
    <int name="m_Init"/>
    <pointer name="m_EnumData" type="::rage::parEnumData" policy="simple_owner"/>
    <!-- should this really be simple_owner, or a shared (external_named) pointer? -->
    <u16 name="m_NumBits"/>
  </structdef>

  <structdef type="::rage::parMemberEnum" name="parMemberEnum" base="::rage::parMember" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <pointer name="m_Data" type="::rage::parMemberEnumData" policy="simple_owner"/>
  </structdef>

  <structdef type="::rage::parMemberBitset" name="parMemberBitset" base="::rage::parMemberEnum" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
  </structdef>

  <enumdef type="::rage::parMemberMap::Subtype">
    <enumval name="ATMAP"/>
    <enumval name="ATBINARYMAP"/>
  </enumdef>

  <structdef type="::rage::parMemberMapData" onPreLoad="PreLoad" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameHash"/>
    <ptrdiff_t name="m_Offset"/>
    <enum name="m_Type" type="::rage::parMember::Type" size="8"/>
    <u8 name="m_Subtype"/>
    <u16 name="m_Flags"/>
    <u16 name="m_VisitFlags"/>
    <u16 name="m_TypeFlags"/>
    <pointer name="m_ExtraAttributes" type="::rage::parAttributeList" policy="simple_owner"/>
  </structdef>

  <structdef type="::rage::parMemberMap" name="parMemberMap" base="::rage::parMember" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <pointer name="m_Data" type="::rage::parMemberMapData" policy="simple_owner"/>
    <pointer name="m_KeyParMember" type="::rage::parMember" policy="owner"/>
    <pointer name="m_DataParMember" type="::rage::parMember" policy="owner"/>
  </structdef>

  <structdef type="::rage::parMemberMatrixData" onPreLoad="PreLoad" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameHash"/>
    <ptrdiff_t name="m_Offset"/>
    <enum name="m_Type" type="::rage::parMember::Type" size="8"/>
    <u8 name="m_Subtype"/>
    <u16 name="m_Flags"/>
    <u16 name="m_VisitFlags"/>
    <u16 name="m_TypeFlags"/>
    <pointer name="m_ExtraAttributes" type="::rage::parAttributeList" policy="simple_owner"/>
    <float name="m_InitAX"/>
    <float name="m_InitAY"/>
    <float name="m_InitAZ"/>
    <float name="m_InitAW"/>
    <float name="m_InitBX"/>
    <float name="m_InitBY"/>
    <float name="m_InitBZ"/>
    <float name="m_InitBW"/>
    <float name="m_InitCX"/>
    <float name="m_InitCY"/>
    <float name="m_InitCZ"/>
    <float name="m_InitCW"/>
    <float name="m_InitDX"/>
    <float name="m_InitDY"/>
    <float name="m_InitDZ"/>
    <float name="m_InitDW"/>
  </structdef>

  <structdef type="::rage::parMemberMatrix" name="parMemberMatrix" base="::rage::parMember" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <pointer name="m_Data" type="::rage::parMemberMatrixData" policy="simple_owner"/>
  </structdef>

  <structdef type="::rage::parMemberSimpleData" onPreLoad="PreLoad" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameHash"/>
    <ptrdiff_t name="m_Offset"/>
    <enum name="m_Type" type="::rage::parMember::Type" size="8"/>
    <u8 name="m_Subtype"/>
    <u16 name="m_Flags"/>
    <u16 name="m_VisitFlags"/>
    <u16 name="m_TypeFlags"/>
    <pointer name="m_ExtraAttributes" type="::rage::parAttributeList" policy="simple_owner"/>
    <float name="m_Init"/>
  </structdef>

  <structdef type="::rage::parMemberSimple" name="parMemberSimple" base="::rage::parMember" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <pointer name="m_Data" type="::rage::parMemberSimpleData" policy="simple_owner"/>
  </structdef>

  <enumdef type="::rage::parMemberString::Subtype">
    <enumval name="MEMBER"/>
    <enumval name="POINTER"/>
    <enumval name="CONST_STRING"/>
    <enumval name="ATSTRING"/>
    <enumval name="WIDE_MEMBER"/>
    <enumval name="WIDE_POINTER"/>
    <enumval name="ATWIDESTRING"/>
    <enumval name="ATNONFINALHASHSTRING"/>
    <enumval name="ATFINALHASHSTRING"/>
    <enumval name="ATHASHVALUE"/>
    <enumval name="ATPARTIALHASHVALUE"/>
    <enumval name="ATNSHASHSTRING"/>
    <enumval name="ATNSHASHVALUE"/>
  </enumdef>

  <structdef type="::rage::parMemberStringData" onPreLoad="PreLoad" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameHash"/>
    <ptrdiff_t name="m_Offset"/>
    <enum name="m_Type" type="::rage::parMember::Type" size="8"/>
    <enum name="m_Subtype" type="::rage::parMemberString::Subtype" size="8"/>
    <u16 name="m_Flags"/>
    <u16 name="m_VisitFlags"/>
    <u16 name="m_TypeFlags"/>
    <pointer name="m_ExtraAttributes" type="::rage::parAttributeList" policy="simple_owner"/>
    <u32 name="m_Length"/>
  </structdef>

  <structdef type="::rage::parMemberString" name="parMemberString" base="::rage::parMember" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <pointer name="m_Data" type="::rage::parMemberStringData" policy="simple_owner"/>
  </structdef>


  <enumdef type="::rage::parMemberStruct::Subtype">
    <enumval name="STRUCTURE"/>
    <enumval name="EXTERNAL_NAMED"/>
    <enumval name="EXTERNAL_NAMED_USERNULL"/>
    <enumval name="POINTER"/>
    <enumval name="SIMPLE_POINTER"/>
  </enumdef>

  <structdef type="::rage::parMemberStructData" onPreLoad="PreLoad" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameHash"/>
    <ptrdiff_t name="m_Offset"/>
    <enum name="m_Type" type="::rage::parMember::Type" size="8"/>
    <enum name="m_Subtype" type="::rage::parMemberStruct::Subtype" size="8"/>
    <u16 name="m_Flags"/>
    <u16 name="m_VisitFlags"/>
    <u16 name="m_TypeFlags"/>
    <pointer name="m_ExtraAttributes" type="::rage::parAttributeList" policy="simple_owner"/>
    <pointer name="m_StructurePtr" type="::rage::parStructure" policy="external_named" toString="::rage::parStructure::internal_GetStructureName" fromString="::rage::parStructure::internal_FindNamedStructure"/>
  </structdef>

  <structdef type="::rage::parMemberStruct" name="parMemberStruct" base="::rage::parMember" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <pointer name="m_Data" type="::rage::parMemberStructData" policy="simple_owner"/>
  </structdef>

  <structdef type="::rage::parMemberVectorData" onPreLoad="PreLoad" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameHash"/>
    <ptrdiff_t name="m_Offset"/>
    <enum name="m_Type" type="::rage::parMember::Type" size="8"/>
    <u8 name="m_Subtype"/>
    <u16 name="m_Flags"/>
    <u16 name="m_VisitFlags"/>
    <u16 name="m_TypeFlags"/>
    <pointer name="m_ExtraAttributes" type="::rage::parAttributeList" policy="simple_owner"/>
    <float name="m_InitX"/>
    <float name="m_InitY"/>
    <float name="m_InitZ"/>
    <float name="m_InitW"/>
  </structdef>

  <structdef type="::rage::parMemberVector" name="parMemberVector" base="::rage::parMember" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <pointer name="m_Data" type="::rage::parMemberVectorData" policy="simple_owner"/>
  </structdef>

  <enumdef type="::rage::parStructure::Flags">
    <enumval name="HAS_VIRTUALS"/>
    <enumval name="HAS_NAMES"/>
    <enumval name="ALWAYS_HAS_NAMES"/>
    <enumval name="IS_EXTERNAL"/>
    <enumval name="IS_CONSTRUCTIBLE"/>
    <enumval name="NOT_IN_PLACE_LOADABLE"/>
  </enumdef>

  <structdef type="::rage::parStructure" name="parStructure" onPreLoad="PreLoad" onPostSave="PostSave" cond="PARSER_USES_EXTERNAL_STRUCTURE_DEFNS">
    <u32 name="m_NameHash"/>
    <pointer name="m_Base" type="::rage::parStructure" policy="external_named" toString="::rage::parStructure::internal_GetStructureName" fromString="::rage::parStructure::internal_FindNamedStructure"/>
    <ptrdiff_t name="m_OffsetOfBase"/>
    <size_t name="m_Size"/>
    <bitset name="m_Flags" type="fixed8" values="::rage::parStructure::Flags"/>
    <array name="m_Members" type="atArray">
      <pointer type="::rage::parMember" policy="owner"/>
    </array>
    <pointer name="m_ExtraAttributes" type="::rage::parAttributeList" policy="simple_owner"/>
  </structdef>

</ParserSchema>

  
