// 
// parser/membermapiterator.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef PARSER_MEMBERMAPITERATOR_H
#define PARSER_MEMBERMAPITERATOR_H

#include "psofaketypes.h"

#include "atl/array.h"
#include "atl/binmap.h"
#include "atl/map.h"

namespace rage {

class parMemberMap;

//////////////////////////////////////////////////////////////////////////
class parMemberMapIterator
{
public:
	        parMemberMapIterator    () {}
    virtual ~parMemberMapIterator   () {}

    virtual void  Begin     ()=0;
    virtual void  Next      ()=0;
    virtual parPtrToStructure GetKeyPtr ()=0;
    virtual parPtrToStructure GetDataPtr()=0;
    virtual bool  AtEnd     ()=0;
};

//////////////////////////////////////////////////////////////////////////
template<typename _KeyType, typename _DataType>
class parMemberMapIterator_atMap : public parMemberMapIterator
{
public:
    typedef atMap<_KeyType, _DataType> MapType;

            parMemberMapIterator_atMap    (MapType & map);
    virtual ~parMemberMapIterator_atMap   (){}

    virtual void  Begin     ();
    virtual void  Next      ();
    virtual parPtrToStructure GetKeyPtr ();
    virtual parPtrToStructure GetDataPtr();
    virtual bool  AtEnd     ();

private:
    typedef atMapEntry<_KeyType, _DataType> Entry;

    int     m_CurrentHashPosition;
    Entry*  m_CurrentNode;
    MapType*m_Map;
};

//////////////////////////////////////////////////////////////////////////
class parMemberAtBinaryMapGenericIterator : public parMemberMapIterator
{
public:
	parMemberAtBinaryMapGenericIterator(parMemberMap& member, void* instanceData);
    virtual ~parMemberAtBinaryMapGenericIterator   (){}

    virtual void  Begin     ();
    virtual void  Next      ();
    virtual parPtrToStructure GetKeyPtr ();
    virtual parPtrToStructure GetDataPtr();
    virtual bool  AtEnd     ();
	
private:
	char* GetArrayElement(int i);

	parFake::AtBinMap* m_MapData;
	size_t			m_DataOffset;  // keysize + padding
	size_t			m_ElementSize; // keysize + padding + datasize + padding

	int				m_CurrElement;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// parMemberMapIterator_atMap ///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename _KeyType, typename _DataType>
parMemberMapIterator_atMap<_KeyType,_DataType>::parMemberMapIterator_atMap (MapType &map) 
{
    m_Map = &map;
    m_CurrentNode = 0;
    m_CurrentHashPosition = 0;
}

template<typename _KeyType, typename _DataType>
void parMemberMapIterator_atMap<_KeyType,_DataType>::Begin()
{
    for (m_CurrentHashPosition = 0; m_CurrentHashPosition < m_Map->GetNumSlots(); m_CurrentHashPosition++) {
        m_CurrentNode = m_Map->GetEntry(m_CurrentHashPosition);
        if(m_CurrentNode) break;
    }
}

template<typename _KeyType, typename _DataType>
void parMemberMapIterator_atMap<_KeyType,_DataType>::Next()
{
    if(AtEnd()) return;
    m_CurrentNode = m_CurrentNode->next;
    const int numSlots = m_Map->GetNumSlots();
    while (!m_CurrentNode && (m_CurrentHashPosition + 1) < numSlots) {
        m_CurrentHashPosition++;
        m_CurrentNode = m_Map->GetEntry(m_CurrentHashPosition);
    }
}

template<typename _KeyType, typename _DataType>
parPtrToStructure parMemberMapIterator_atMap<_KeyType,_DataType>::GetKeyPtr()
{
    FastAssert(m_CurrentNode); return reinterpret_cast<parPtrToStructure>(&m_CurrentNode->key);
}

template<typename _KeyType, typename _DataType>
parPtrToStructure parMemberMapIterator_atMap<_KeyType,_DataType>::GetDataPtr()
{
    FastAssert(m_CurrentNode); return reinterpret_cast<parPtrToStructure>(&m_CurrentNode->data);
}

template<typename _KeyType, typename _DataType>
bool parMemberMapIterator_atMap<_KeyType,_DataType>::AtEnd()
{
    return !m_CurrentNode;
}


}

#endif // PARSER_MEMBERMAPITERATOR_H
