// 
// parser/memberdata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PARSER_MEMBERDATA_H 
#define PARSER_MEMBERDATA_H 

#include "macros.h"
#include "data/callback.h"

#include <cstddef>

namespace rage {
	class parTreeNode;
	class parAttributeList;

// PURPOSE:
//	All parMember???::Data structure MUST begin with this macro. These fields are
//  required for all parMember???::Datas
//  The reason each has to have a copy instead of subclassing from a base class which
//  defines these is that with a copy we can use the myStruct = {a,b,c} style initialization,
//  so that all the data for all the parMembers can exist in static data, rather than existing
//  once in generated code and once again in constructed member data.
#define STANDARD_PARMEMBER_DATA_CONTENTS \
	u32 m_NameHash; \
	size_t m_Offset; \
	u8 m_Type; \
	u8 m_Subtype; \
	u16 m_Flags; \
	u16 m_VisitFlags; \
	u16 m_TypeFlags;	\
	parAttributeList* m_ExtraAttributes; \
	void StdInit(); \
	//END

////////////////////////////////////////////////////////////////////////////////////
// parMember
////////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Every member has these flags, but some fields are only relavent to some subclasses.
namespace parMemberFlags
{
	enum Enum
	{
		MEMBER_STATIC,			// if true, do not delete this member's data, someone else manages it (possibly it's in static data)
		MEMBER_READONLY,		// when widgets are created, can this member be edited?
		MEMBER_NO_INIT,			// if set, don't initialize this member
		MEMBER_ADD_GROUP_WIDGET,// for complex data types, should they be enclosed in a group?

		MEMBER_ARRAY_ALIGN_BIT1,	// These flags are only valid for parMemberArray and can be reused for other types
		MEMBER_ARRAY_ALIGN_BIT2, // 4 bits specify the power of 2 to align the array contents on (between 1<<0 and 1<<15)
		MEMBER_ARRAY_ALIGN_BIT4, 
		MEMBER_ARRAY_ALIGN_BIT8,

		MEMBER_MAX_FLAGS = 16	// Can't have any more flags than this.
	};

	enum OtherValues
	{
		MEMBERARRAY_ALIGN_SHIFT = MEMBER_ARRAY_ALIGN_BIT1, // shift flags down by this amount
		MEMBERARRAY_ALIGN_MASK = (1 << (MEMBER_ARRAY_ALIGN_BIT8 + 1 - MEMBER_ARRAY_ALIGN_BIT1))-1, // then '&' with this mask
	};
}

// PURPOSE: Describes the type of this member.
namespace parMemberType
{
    //NOTE: if you add types here make sure to update parMemberType::g_Sizes 
    //  and parMemberType::g_Alignments in member.cpp
	enum Enum {
		TYPE_BOOL = 0,		// bool
		TYPE_CHAR,			// s8
		TYPE_UCHAR,			// u8
		TYPE_SHORT,			// s16
		TYPE_USHORT,		// u16
		TYPE_INT,			// s32
		TYPE_UINT,			// u32
		TYPE_FLOAT,			// float
		TYPE_VECTOR2,		// Vector2
		TYPE_VECTOR3,		// Vector3
		TYPE_VECTOR4,		// Vector4
		TYPE_STRING,		// char*
		TYPE_STRUCT,		// Another structure. Could be a nested structure or a owning pointer to another structure.
		TYPE_ARRAY,			// An array. Could be an atArray, atRangeArray, raw pointer, etc.
		TYPE_ENUM,			// enumerated type
		TYPE_BITSET,		// A bitset (atFixedBitSet or atBitSet)
		TYPE_MAP,   		// A map (atMap)
        TYPE_MATRIX34,		// Matrix34
		TYPE_MATRIX44,		// Matrix44
		TYPE_VEC2V,			// Vec2V
		TYPE_VEC3V,			// Vec3V
		TYPE_VEC4V,			// Vec4V
		TYPE_MAT33V,		// Mat33V
		TYPE_MAT34V,		// Mat34V
		TYPE_MAT44V,		// Mat44V
		TYPE_SCALARV,		// ScalarV
		TYPE_BOOLV,			// BoolV
		TYPE_VECBOOLV,		// VecBoolV
		TYPE_PTRDIFFT,		// ptrdiff_t
		TYPE_SIZET,			// size_t
		TYPE_FLOAT16,		// float16
		TYPE_INT64,			// s64
		TYPE_UINT64,		// u64
		TYPE_DOUBLE,		// double
        
		LAST_TYPE = TYPE_DOUBLE,
		
		// MAX_TYPES = 254
		INVALID_TYPE = 255
	};

	// Traits functions
	enum MemberClass {
		CLASS_SIMPLE,
		CLASS_VECTOR,
		CLASS_STRING,
		CLASS_STRUCT,
		CLASS_ARRAY,
		CLASS_ENUM,
		CLASS_BITSET,
		CLASS_MAP,
		CLASS_MATRIX,
		INVALID_CLASS,
	};

	struct Traits {
		u8 m_Size, m_Align, m_Subclass;
	};

	extern Traits g_Traits[LAST_TYPE+1];
	
	// PURPOSE: Gets the size of one of the types listed above, or 0 if the type alone isn't enough information
	inline size_t GetSize(Enum type) { return type <= LAST_TYPE ? g_Traits[type].m_Size : 0; }
	
	// PURPOSE: Gets the alignment of one of the types listed above, or 0 if the type alone isn't enough information
	inline size_t GetAlign(Enum type) { return type <= LAST_TYPE ? g_Traits[type].m_Align : 0; } 

	inline MemberClass GetClass(Enum type) { return type <= LAST_TYPE ? (MemberClass)g_Traits[type].m_Subclass : INVALID_CLASS;  }

	inline bool IsSimple(Enum type) { return GetClass(type) == CLASS_SIMPLE; }
	inline bool IsVector(Enum type) { return GetClass(type) == CLASS_VECTOR; }
	inline bool IsString(Enum type) { return type == TYPE_STRING;}
	inline bool IsStructOrPointer(Enum type) { return type == TYPE_STRUCT; }
	inline bool IsArray(Enum type) { return type == TYPE_ARRAY; }
	inline bool IsEnum(Enum type) { return type == TYPE_ENUM; }
	inline bool IsBitset(Enum type) { return type == TYPE_BITSET; }
	inline bool IsMap(Enum type) { return type == TYPE_MAP; }
	inline bool IsMatrix(Enum type) { return GetClass(type) == CLASS_MATRIX; }
}

// PURPOSE: Flags specifying which visitors can see each member variable
namespace parMemberVisitorFlags
{
	// These will normally all be ON. 
	// A visitor will have a mask for which members it visits.
	// If any of the VisitorFlags matches the mask, the member will get visited
	enum Enum
	{
		VISIT_FILE,	
		VISIT_NET,	
		VISIT_SCRIPT,	
		VISIT_TOOL, 
		VISIT_BANK,	

		VISIT_USER0 = 8, // games can define visit flags from here up
		VISIT_MAX_FLAGS = 16
	};
}


// The common data that each subclasses' Data objects should begin with.
// The subclasses will all use the ..._CONTENTS macro so they won't inherit from 
// CommonData. CommonData itself should never be constructed, which is why its
// default c'tor is private.
struct parMemberCommonData
{
	STANDARD_PARMEMBER_DATA_CONTENTS;
private:
	parMemberCommonData() {};
};


} // namespace rage

#endif // PARSER_MEMBERDATA_H 
