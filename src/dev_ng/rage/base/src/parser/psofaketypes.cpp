// 
// psofaketypes.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 
#if 0
#include "psofaketypes.h"

#include "optimisations.h"

#include "atl/array.h"
#include "atl/binmap.h"
#include "atl/string.h"
#include "string/string.h"

PARSER_OPTIMISATIONS();

using namespace rage;

CompileTimeAssert(sizeof(psoFakePtr) == sizeof(char*));
CompileTimeAssert(sizeof(psoFakeAtString) == sizeof(atString));
CompileTimeAssert(sizeof(psoFakeConstString) == sizeof(ConstString));
CompileTimeAssert(sizeof(psoFakeAtBitset) == sizeof(atBitSet));
CompileTimeAssert(sizeof(psoFakeAtArray16) == sizeof(atArray<char, 0, u16>));
CompileTimeAssert(sizeof(psoFakeAtArray32) == sizeof(atArray<char, 0, u32>));
CompileTimeAssert(sizeof(psoFakeAtBinMap) == sizeof(atBinaryMap<char, char>));
#endif