// 
// parser/psoresource.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#include "psoresource.h"

#include "psobyteswap.h"
#include "psoschemadata.h"

#include "data/safestruct.h"
#include "system/platform.h"

#include <climits>

using namespace rage;

#if !__FINAL
const u8 psoResourceData::NonfinalStringKey[32] = {
	0xa6,	0xda,	0xa3,	0xa1,	0xad,	0xf1,	0x7d,	0x06,
	0x5a,	0xcf,	0xd3,	0xf4,	0xfe,	0xb7,	0x58,	0x2f,
	0xd9,	0xd4,	0xda,	0x81,	0xee,	0x9a,	0x70,	0xc1,
	0xda,	0x95,	0x14,	0x64,	0x33,	0x03,	0x66,	0xa9
};
#endif

psoResourceData::psoResourceData()
{
	m_Identifier = psoConstants::PRD0_MAGIC_NUMBER; // The current version
	m_Checksum = 0;
	m_NumStructSchemas = 0;
	m_NumEnumSchemas = 0;
	m_NumStructArrayEntries = 0;

	m_PlatformId = g_sysPlatform;
	m_RootObjectId = psoStructId::Null();
}

void psoResourceData::Place(psoResourceData* that, datResource& rsc)
{
	pgBasePlatformNeutral::BeforePlace(that, that->m_PlatformId);

	psoPtrNeedsSwap needsSwap = sysGetByteSwap(that->m_PlatformId) ? PSOPTR_SWAP : PSOPTR_NO_SWAP;
	psoPtrOriginalSize ptrSize = sysIs64Bit(that->m_PlatformId) ? PSOPTR_64BIT : PSOPTR_32BIT;
	::new(that) psoResourceData(rsc, needsSwap, ptrSize);
}

#if __DECLARESTRUCT
void psoResourceData::DeclareStruct(datTypeStruct& s)
{
	for(int i = 0; i < m_NumStructSchemas; i++)
	{
		datTypeStruct s2;
		m_StructSchemaTable[i].DeclareStruct(s2);
	}

	for(int i = 0; i < m_NumEnumSchemas; i++)
	{
		datTypeStruct s2;
		m_EnumSchemaTable[i].DeclareStruct(s2);
	}

	for(int i = 0; i < m_NumStructArrayEntries; i++)
	{
		datTypeStruct s2;
		m_StructArrayTable[i].DeclareStruct(s2);
	}

	pgBasePlatformNeutral::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(psoResourceData, pgBasePlatformNeutral)
		SSTRUCT_FIELD(psoResourceData, m_Identifier)
		SSTRUCT_FIELD(psoResourceData, m_PlatformId)
		SSTRUCT_IGNORE(psoResourceData, m_Pad)
		SSTRUCT_FIELD(psoResourceData, m_Flags)
		SSTRUCT_FIELD(psoResourceData, m_Checksum)
		SSTRUCT_FIELD_AS(psoResourceData, m_RootObjectId, u32)
		SSTRUCT_FIELD(psoResourceData, m_StructSchemaTable)
		SSTRUCT_FIELD(psoResourceData, m_EnumSchemaTable)
		SSTRUCT_FIELD(psoResourceData, m_StructArrayTable)
		SSTRUCT_FIELD(psoResourceData, m_FinalHashStrings)
		SSTRUCT_FIELD(psoResourceData, m_NonFinalHashStrings)
		SSTRUCT_FIELD(psoResourceData, m_NumStructSchemas)
		SSTRUCT_FIELD(psoResourceData, m_NumEnumSchemas)
		SSTRUCT_FIELD(psoResourceData, m_NumStructArrayEntries)
		SSTRUCT_IGNORE(psoResourceData, m_Reserved)
	SSTRUCT_END(psoResourceData)
}
#endif // __DECLARESTRUCT

psoResourceData::psoResourceData(datResource& rsc, psoPtrNeedsSwap needsSwap, psoPtrOriginalSize ptrSize)
: m_StructSchemaTable(rsc, needsSwap, ptrSize)
, m_EnumSchemaTable(rsc, needsSwap, ptrSize)
, m_StructArrayTable(rsc, needsSwap, ptrSize)
, m_FinalHashStrings(rsc, needsSwap, ptrSize)
, m_NonFinalHashStrings(rsc, needsSwap, ptrSize)
, m_Flags(rsc)
{
	if (needsSwap == PSOPTR_SWAP)
	{
		sysEndian::SwapMe(m_Identifier);
		sysEndian::SwapMe(m_Flags);
		sysEndian::SwapMe(m_Checksum);
		sysEndian::SwapMe(m_RootObjectId);
		sysEndian::SwapMe(m_NumStructSchemas);
		sysEndian::SwapMe(m_NumEnumSchemas);
		sysEndian::SwapMe(m_NumStructArrayEntries);
	}

	for(int i = 0; i < m_NumStructSchemas; i++)
	{
		psoRscStructSchemaData::Place(&m_StructSchemaTable[i], rsc, needsSwap, ptrSize);
	}
	for(int i = 0; i < m_NumEnumSchemas; i++)
	{
		psoRscEnumSchemaData::Place(&m_EnumSchemaTable[i], rsc, needsSwap, ptrSize); 
	}
	for(int i = 0; i < m_NumStructArrayEntries; i++)
	{
		psoRscStructArrayTableData::Place(&m_StructArrayTable[i], rsc, needsSwap, ptrSize);
	}
}

void psoResourceData::DeleteInstances()
{
	for(int i = 0; i < m_NumStructArrayEntries; i++)
	{
		delete [] (char*)m_StructArrayTable[i].m_Data.GetPtr();
		m_StructArrayTable[i].m_Data = NULL;
	}

	delete [] m_FinalHashStrings.GetPtr();
	m_FinalHashStrings = NULL;
	delete [] m_NonFinalHashStrings.GetPtr();
	m_NonFinalHashStrings = NULL;
}

void psoResourceData::DeleteStructArrays()
{
	m_StructArrayTable.DeleteArrayNoCookie(m_NumStructArrayEntries);
	m_NumStructArrayEntries = 0;
}

void psoResourceData::DeleteSchemaData()
{
	// Need to call any destructors?
	for(int i = 0; i < m_NumEnumSchemas; i++)
	{
		m_EnumSchemaTable[i].m_Values.DeleteArrayNoCookie(m_EnumSchemaTable[i].m_NumValues);
	}
	m_EnumSchemaTable.DeleteArrayNoCookie(m_NumEnumSchemas);
	m_NumEnumSchemas = 0;

	for(int i = 0; i < m_NumStructSchemas; i++)
	{
		m_StructSchemaTable[i].m_Members.DeleteArrayNoCookie(m_StructSchemaTable[i].m_NumMembers);
	}
	m_StructSchemaTable.DeleteArrayNoCookie(m_NumStructSchemas);
	m_NumStructSchemas = 0;
}

void psoResourceData::CreateStructArrayTable(size_t count)
{
	FastAssert(count < USHRT_MAX);
	m_StructArrayTable.NewArrayNoCookie(count);
	m_NumStructArrayEntries = (u16)count;
}

void psoResourceData::CreateStructSchemaTable(size_t count)
{
	FastAssert(count < USHRT_MAX);
	m_StructSchemaTable.NewArrayNoCookie(count);
	m_NumStructSchemas = (u16)count;
}

void psoResourceData::CreateEnumSchemaTable(size_t count)
{
	FastAssert(count < USHRT_MAX);
	m_EnumSchemaTable.NewArrayNoCookie(count);
	m_NumEnumSchemas = (u16)count;
}

void psoResourceData::CopyStructSchema( psoRscStructSchemaData& src, psoRscStructSchemaData& dest )
{
	sysMemCpy(&dest, &src, sizeof(dest));
	dest.m_Members.NewArrayNoCookie(src.m_NumMembers);
	sysMemCpy(dest.m_Members.GetPtr(), src.m_Members.GetPtr(), sizeof(psoRscMemberSchemaData) * src.m_NumMembers);
}

void psoResourceData::CopyEnumSchema( psoRscEnumSchemaData& src, psoRscEnumSchemaData& dest )
{
	sysMemCpy(&dest, &src, sizeof(dest));
	dest.m_Values.NewArrayNoCookie(src.m_NumValues);
	sysMemCpy(dest.m_Values.GetPtr(), src.m_Values.GetPtr(), sizeof(psoRscEnumSchemaValueData) * src.m_NumValues);
}

void psoResourceData::CopySchemaDataTo(psoResourceData& dest)
{
	dest.CreateEnumSchemaTable(m_NumEnumSchemas);
	for(int i = 0; i < dest.m_NumEnumSchemas; i++)
	{
		CopyEnumSchema(m_EnumSchemaTable.GetPtr()[i], dest.m_EnumSchemaTable.GetPtr()[i]);
	}

	dest.CreateStructSchemaTable(m_NumStructSchemas);
	for(int i = 0; i < dest.m_NumStructSchemas; i++)
	{
		CopyStructSchema(m_StructSchemaTable.GetPtr()[i], dest.m_StructSchemaTable.GetPtr()[i]);
	}
}