// 
// parser/psofile_parser.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "psofile.h"

#include "manager.h"
#include "memberenumdata.h"
#include "membermap.h"
#include "optimisations.h"
#include "psobuilder.h"
#include "psobyteswap.h"
#include "psoparserbuilder.h"
#include "structure.h"
#include "visitorinit.h"


#include "diag/output.h"
#include "file/asset.h"
#include "file/stream.h"
#include "math/float16.h"
#include "system/endian.h"
#include "system/magicnumber.h"
#include "system/nelem.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vectormath/boolv.h"
#include "vectormath/mat33v.h"
#include "vectormath/mat34v.h"
#include "vectormath/mat44v.h"
#include "vectormath/scalarv.h"
#include "vectormath/vec2v.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"

PARSER_OPTIMISATIONS();

#if PSO_USING_NATIVE_BIT_SIZE

using namespace rage;

namespace psoFileReader
{

void ReadParMemberFromPso( parMember* destMemberDesc, parPtrToStructure destStructAddr, psoStruct& srcStructure, psoMember& srcMember );
void FixupArrayWithExternalStorage( parPtrToStructure destStructAddr, psoStruct &srcStructure, psoMember &mem, psoMemberArrayInterface &memArr );
bool IsFixupPod(parMemberType::Enum type, u8 subtype);
void FixupPsoMember(psoStruct& destStructure, psoMember& mem);

template<typename _Type>
void CopyIntoBuffer(void* RESTRICT dest, const _Type& src)
{
	*(_Type*)dest = src;
}

#define SIMPLE_CONVERSION(srcType, nativeType) \
	if (srcMember.GetType() == srcType)	{ CopyIntoBuffer(destMemberAddress, srcMember.GetDataAs<nativeType>()); converted = true; }


bool ReadStringFromPso(parMemberString& destMemberDesc, parPtrToStructure destStructAddr, psoMember& srcMember)
{
	USES_CONVERSION;

	// Copy string hashes directly, without going through a real string, if src is a stringhash and 
	// dest is one of the original hash types...
	if (srcMember.GetType() == psoType::TYPE_STRINGHASH && (
#if !__FINAL
		destMemberDesc.GetSubtype() == parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING ||
#endif
		destMemberDesc.GetSubtype() == parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING ||
		destMemberDesc.GetSubtype() == parMemberStringSubType::SUBTYPE_ATHASHVALUE))
	{
		destMemberDesc.GetMemberFromStruct<u32>(destStructAddr) = srcMember.GetDataAs<u32>();
		return true;
	}

	// ...or src and dest are namespaced hash strings/values and the namespace is the same
	if (srcMember.GetType() == psoType::TYPE_NSSTRINGHASH && srcMember.GetSchema().GetStringNamespaceIndex() == destMemberDesc.GetData()->GetNamespaceIndex() &&
			(destMemberDesc.GetSubtype() == parMemberStringSubType::SUBTYPE_ATNSHASHSTRING ||
			destMemberDesc.GetSubtype() == parMemberStringSubType::SUBTYPE_ATHASHVALUE) ) 
	{
		destMemberDesc.GetMemberFromStruct<u32>(destStructAddr) = srcMember.GetDataAs<u32>();
		return true;
	}

	const char* srcString = NULL;
	if (srcMember.GetType().IsWideString())
	{
		srcString = WIDE_TO_UTF8(srcMember.GetWideStringData());
	}
	else
	{
		srcString = srcMember.GetStringData();
	}

	destMemberDesc.SetFromString(destStructAddr, srcString);

	return true;
}

bool ReadBitsetFromPso(parMemberBitset& destMemberDesc, parPtrToStructure destStructAddr, psoStruct& srcStructure, psoMember& srcMember)
{
	using namespace parMemberBitsetSubType;

	if (!srcMember.GetType().IsBitset())
	{
		return false;
	}

	psoMemberBitsetInterface psobits(srcStructure, srcMember);

	bool converted = false;

	if (psobits.AreBitsNamed() && destMemberDesc.GetData()->m_EnumData)
	{
		// For each bit, find the bit's name(hash) in the PSO file, then set the corresponding name(hash) in the parBitset
		size_t numPsoBits = psobits.GetBitCount();

		if (destMemberDesc.GetSubtype() == parMemberBitsetSubType::SUBTYPE_ATBITSET)
		{
			atBitSet& realBitset = destMemberDesc.GetMemberFromStruct<atBitSet>(destStructAddr);
			realBitset.Init((int)numPsoBits);
		}
		else
		{
			destMemberDesc.ClearBits(destStructAddr);
		}
		parPtrToArray unused;
		u32 numDestBits;
		destMemberDesc.GetBitsetContentsAndCountFromStruct(destStructAddr, unused, numDestBits);

		for(int i = 0; i < (int)numPsoBits; i++)
		{
			if (psobits.GetBit(i))
			{
				atLiteralHashValue bitname = psobits.GetBitName(i);
				parEnumData::ValueFromNameStatus status = parEnumData::VFN_ERROR;
				int destbit = destMemberDesc.ValueFromName(bitname.GetHash(), &status);
				if (bitname.IsNotNull() && status == parEnumData::VFN_NAMED)
				{
					if (parVerifyf((u32)destbit < numDestBits, "Can't set bit %d in a bitset (%s) that only contains %d bits", destbit, destMemberDesc.GetName(), numDestBits))
					{
						destMemberDesc.SetBit(destStructAddr, destbit);
					}
				}
				else
				{
					if (bitname.IsNull() && destMemberDesc.HashFromValue(i) == 0)
					{
						// Found a bit with no name in the PSO file, and it had no name in the runtime. Why not set it?
						if (parVerifyf((u32)i< numDestBits, "Can't set bit %d in a bitset (%s) that only contains %d bits", i, destMemberDesc.GetName(), numDestBits))
						{
							destMemberDesc.SetBit(destStructAddr, i);
						}
					}
					else
					{
						parWarningf("PSO data has bit %d set (bit namehash = 0x%x). Can't find a bit with this name in the runtime. NOT SETTING THIS BIT", i, bitname.GetHash() /*, destMemberDesc.GetName()*/ );
					}
				}
			}
		}
		converted = true;
	}
	else
	{
		parAssertf(psobits.AreBitsNamed() == destMemberDesc.AreBitsNamed(), "Reading bitset member '%s' from PSO file - expected either both file and runtime have named bits, or neither do", destMemberDesc.GetNameUnsafe());

		parPtrToMember destMemberAddress = destMemberDesc.GetPointerToMember(destStructAddr);

		switch(destMemberDesc.GetSubtype())
		{
		case SUBTYPE_32BIT_FIXED:
			if (srcMember.GetType() == psoType::TYPE_BITSET32)
			{
				parPtrToArray junk;
				u32 numBits;
				destMemberDesc.GetBitsetContentsAndCountFromStruct(destStructAddr, junk, numBits);
				if (numBits == srcMember.GetSchema().GetBitsetCount())
				{
					sysMemCpy(destMemberAddress, srcMember.GetRawDataPtr(), destMemberDesc.parMemberBitset::GetSize());
					converted = true;
				}
			}
			break;
		case SUBTYPE_16BIT_FIXED:
			if (srcMember.GetType() == psoType::TYPE_BITSET16)
			{
				parPtrToArray junk;
				u32 numBits;
				destMemberDesc.GetBitsetContentsAndCountFromStruct(destStructAddr, junk, numBits);
				if (numBits == srcMember.GetSchema().GetBitsetCount())
				{
					sysMemCpy(destMemberAddress, srcMember.GetRawDataPtr(), destMemberDesc.parMemberBitset::GetSize());
					converted = true;
				}
			}
			break;
		case SUBTYPE_8BIT_FIXED:
			if (srcMember.GetType() == psoType::TYPE_BITSET8)
			{
				parPtrToArray junk;
				u32 numBits;
				destMemberDesc.GetBitsetContentsAndCountFromStruct(destStructAddr, junk, numBits);
				if (numBits == srcMember.GetSchema().GetBitsetCount())
				{
					sysMemCpy(destMemberAddress, srcMember.GetRawDataPtr(), destMemberDesc.parMemberBitset::GetSize());
					converted = true;
				}
			}
			break;
		case SUBTYPE_ATBITSET:
			if (srcMember.GetType() == psoType::TYPE_ATBITSET32) 
			{
				psoFakeAtBitset& destBitset = destMemberDesc.GetMemberFromStruct<psoFakeAtBitset>(destStructAddr);
				psoFake32::AtBitset& srcBitset = srcMember.GetDataAs<psoFake32::AtBitset>();

				destBitset.m_Size = srcBitset.m_Size;
				destBitset.m_BitSize = srcBitset.m_BitSize;
				
				u32* newBits = rage_new u32[srcBitset.m_Size];
				psoStruct bitsData = srcMember.GetFile().GetInstance(srcBitset.m_Bits.m_StructId);
				sysMemCpy(newBits, bitsData.GetInstanceData(), srcBitset.m_Size * sizeof(u32));
				destBitset.m_Bits.m_Pointer = newBits;

				converted = true;
			}
			else if (srcMember.GetType() == psoType::TYPE_ATBITSET64)
			{
				psoFakeAtBitset& destBitset = destMemberDesc.GetMemberFromStruct<psoFakeAtBitset>(destStructAddr);
				psoFake64::AtBitset& srcBitset = srcMember.GetDataAs<psoFake64::AtBitset>();

				destBitset.m_Size = srcBitset.m_Size;
				destBitset.m_BitSize = srcBitset.m_BitSize;

				u32* newBits = rage_new u32[srcBitset.m_Size];
				psoStruct bitsData = srcMember.GetFile().GetInstance(srcBitset.m_Bits.m_StructId);
				sysMemCpy(newBits, bitsData.GetInstanceData(), srcBitset.m_Size * sizeof(u32));
				destBitset.m_Bits.m_Pointer = newBits;

				converted = true;
			}
			break;
		}
	}

	return converted;
}

bool ReadMapFromPso(parMemberMap& destMemberDesc, parPtrToStructure deststructaddr, psoStruct& srcStructure, psoMemberMapInterface& srcMember)
{
    using namespace parMemberMapSubType;

	parMemberMapInterface* destInterface = destMemberDesc.CreateInterface(deststructaddr);

	destInterface->Reset();

	size_t itemCount = srcMember.GetMapCount();

	destInterface->Reserve((int)itemCount);

	for(int i = 0; i < (int)itemCount; i++)
	{
		psoMember srcKeyMember = srcMember.GetKey(i);
		psoMember srcDataMember = srcMember.GetValue(i);

		parMember* destKeyMember = destMemberDesc.GetKeyMember();
		parMember* destDataMember = destMemberDesc.GetDataMember();

		// No type conversion supported

		parAssertf(srcKeyMember.GetType().IsMapKey() && srcKeyMember.GetType() == psoType::ConvertParserTypeToPsoType(destKeyMember->GetType(), destKeyMember->GetCommonData()->m_Subtype, PSO_USING_NATIVE_BIT_SIZE ? __64BIT : false), "Can't convert key types");
		parAssertf(srcDataMember.GetType() == psoType::ConvertParserTypeToPsoType(destDataMember->GetType(), destDataMember->GetCommonData()->m_Subtype, PSO_USING_NATIVE_BIT_SIZE ? __64BIT : false), "Can't convert value types");

		u64 keyValue = 0;

		switch(destKeyMember->GetType())
		{
			//These are the limited supported types for key values
		case parMemberType::TYPE_CHAR:		keyValue = (u64)srcKeyMember.GetDataAs<s8>(); break;
		case parMemberType::TYPE_UCHAR:		keyValue = (u64)srcKeyMember.GetDataAs<u8>(); break;
		case parMemberType::TYPE_SHORT:		keyValue = (u64)srcKeyMember.GetDataAs<s16>(); break;
		case parMemberType::TYPE_USHORT:	keyValue = (u64)srcKeyMember.GetDataAs<u16>(); break;
		case parMemberType::TYPE_INT:		keyValue = (u64)srcKeyMember.GetDataAs<s32>(); break;
		case parMemberType::TYPE_UINT:		keyValue = (u64)srcKeyMember.GetDataAs<u32>(); break;
		case parMemberType::TYPE_INT64:		keyValue = (u64)srcKeyMember.GetDataAs<s64>(); break;
		case parMemberType::TYPE_UINT64:	keyValue = (u64)srcKeyMember.GetDataAs<u64>(); break;
		case parMemberType::TYPE_STRING:
			{
				switch (destKeyMember->GetCommonData()->m_Subtype)
				{
#if !__FINAL
				case parMemberStringSubType::SUBTYPE_ATNONFINALHASHSTRING:
#endif
				case parMemberStringSubType::SUBTYPE_ATFINALHASHSTRING:
				case parMemberStringSubType::SUBTYPE_ATHASHVALUE:
				case parMemberStringSubType::SUBTYPE_ATPARTIALHASHVALUE:
				case parMemberStringSubType::SUBTYPE_ATNSHASHSTRING:
				case parMemberStringSubType::SUBTYPE_ATNSHASHVALUE:
					{
						keyValue = (u64)srcKeyMember.GetDataAs<u32>();
					}
					break;
				default:
					return false;
				};
			}
			break;
		case parMemberType::TYPE_ENUM:
			{
				int filevalue = 0;
				atLiteralHashValue enumName;
				bool found = srcKeyMember.GetEnumValueAndName(filevalue, enumName);
				if (found)
				{
					parMemberEnum& destKeyEnumMember = destKeyMember->AsEnumRef();
					parEnumData::ValueFromNameStatus status;
					keyValue = destKeyEnumMember.ValueFromName(enumName.GetHash(), &status);
					Assertf(status == parEnumData::VFN_NAMED, "PSO file had enum value %d, namehash 0x%x. Couldn't find this name in the parser metadata.", filevalue, enumName.GetHash());
				}
			}
			break;
		default:
			return false;
		};

		parPtrToStructure dataValueAddr = destInterface->InsertKey(&keyValue);

		// Init the new member. Note - we do something similar with arrays, but for arrays we can pass in a bool for whether or not to init the new member, 
		// we don't get that ability here.
		parInitVisitor initter;
		initter.VisitMember(dataValueAddr, *destDataMember);

		ReadParMemberFromPso(destDataMember, dataValueAddr, srcStructure, srcDataMember);
	}

	destInterface->PostInsert();

	delete destInterface;

	return true;
}

bool ReadEnumFromPso(parMemberEnum& destMemberDesc, parPtrToStructure destStructAddr, psoMember& srcMember)
{
	if (!srcMember.GetType().IsEnum())
	{
		return false;
	}

	using namespace parMemberEnumSubType;

	atLiteralHashValue namehash;
	int filevalue = 0;

	bool found = srcMember.GetEnumValueAndName(filevalue, namehash);

	int codevalue = filevalue;

	if (found)
	{
		parEnumData::ValueFromNameStatus status;
		int newValue = destMemberDesc.ValueFromName(namehash.GetHash(), &status);
		if (status == parEnumData::VFN_NAMED)
		{
			// good conversion
			codevalue = newValue;
		}
		else
		{
			parWarningf("Couldn't find an enum value with name hash %x in member %s", namehash.GetHash(), destMemberDesc.GetName());
		}
	}
	else
	{
		parWarningf("Couldn't find an enum name for value %d in PSO file (enum type namehash = %x)", filevalue, srcMember.GetSchema().GetReferentHash().GetHash());
	}

	switch(destMemberDesc.GetSubtype())
	{
	case SUBTYPE_32BIT:
		destMemberDesc.GetMemberFromStruct<s32>(destStructAddr) = codevalue;
		break;
	case SUBTYPE_16BIT:
		destMemberDesc.GetMemberFromStruct<s16>(destStructAddr) = (s16)codevalue;
		break;
	case SUBTYPE_8BIT:
		destMemberDesc.GetMemberFromStruct<s8>(destStructAddr) = (s8)codevalue;
		break;
	}

	return true;
}

bool ReadArrayFromPso(parMemberArray& destMemberDesc, parPtrToStructure destStructAddr, psoStruct& srcStruct, psoMemberArrayInterface& srcMemberArrayInt, psoMember& /*srcMember*/)
{
	size_t arraySize = srcMemberArrayInt.GetArrayCount();

	bool didFastFixup = false;

	if (destMemberDesc.HasExternalStorage() && arraySize > 0)
	{
		psoFile& file = srcStruct.GetFile();
		psoMemberSchema eltSchema = srcMemberArrayInt.GetArrayElementSchema();
		bool isPodArray = eltSchema.GetType().IsPod();

		bool isFastLoadableStruct = false;
		if (eltSchema.GetType() == psoType::TYPE_STRUCT)
		{
			psoStructureSchema eltStructSchema = file.GetSchemaCatalog().FindStructureSchema(eltSchema.GetReferentHash());
			isFastLoadableStruct = eltStructSchema.CanUseFastLoading();
		}

		if (isPodArray)
		{
			// Make sure the types are compatible too
			if (eltSchema.GetType() != psoType::ConvertParserTypeToPsoType(destMemberDesc.GetType(), destMemberDesc.GetCommonData()->m_Subtype, PSO_USING_NATIVE_BIT_SIZE ? __64BIT : false))
			{
				isPodArray = false;
			}
		}

		if (isPodArray || isFastLoadableStruct)
		{
			u8 flags = 0; // no construction, initialization, anything
			destMemberDesc.SetArraySize(destStructAddr, arraySize, flags);
			parPtrToArray destArrayContents;
			size_t destArrayCount;
			destMemberDesc.GetArrayContentsAndCountFromStruct(destStructAddr, destArrayContents, destArrayCount);

			size_t eltSize = file.GetSchemaCatalog().FindMemberSize(srcStruct.GetSchema(), eltSchema);
			size_t storageSize = arraySize * eltSize;

			sysMemCpy(destArrayContents, srcMemberArrayInt.GetArrayContents(), storageSize);

			if (isFastLoadableStruct)
			{
				for(u32 i = 0; i < destArrayCount; i++)
				{
					psoMember arrayElt(eltSchema, reinterpret_cast<parPtrToMember>(reinterpret_cast<char*>(destArrayContents) + i * eltSize), &file);
					// Need to create a fake "parent" structure that the array elements are a member of.
					psoStruct destStructure = srcStruct;
					destStructure.SetInstanceDataPtr(destStructAddr);

					FixupPsoMember(destStructure, arrayElt);
				}
			}
			didFastFixup = true;
		}
	}


	if (!didFastFixup)
	{
		destMemberDesc.SetArraySize(destStructAddr, arraySize);

		parPtrToArray destArrayContents;
		size_t destArrayCount;
		destMemberDesc.GetArrayContentsAndCountFromStruct(destStructAddr, destArrayContents, destArrayCount);

		parMember* protoMember = destMemberDesc.GetPrototypeMember();

		for(int i = 0; i < (int)arraySize; i++)
		{
			parPtrToStructure destElementAddr = destMemberDesc.GetAddressOfNthElement(destArrayContents, i);

			psoMember elt = srcMemberArrayInt.GetElement(i);
			ReadParMemberFromPso(protoMember, destElementAddr, srcStruct, elt);
		}
	}

	return true;
}

bool  ReadStructMemberFromPso(parMemberStruct& destMemberDesc, parPtrToStructure destStructAddr, psoMember& srcMember)
{
	psoStruct srcSubStruct = srcMember.GetSubStructure();

	parAssertf(srcSubStruct.IsValid(), "Couldn't find valid substructure");

	if (destMemberDesc.IsOwnerPointer())
	{
		if (srcSubStruct.IsNull())
		{
			destMemberDesc.SetObjAddr(destStructAddr, NULL);
		}
		else
		{
			parPtrToStructure newStruct;
			psoCreateAndLoadFromStructure(srcSubStruct, destMemberDesc.GetBaseStructure(), newStruct);
			destMemberDesc.SetObjAddr(destStructAddr, newStruct);
		}
	}
	else
	{
		psoLoadFromStructure(srcSubStruct, *destMemberDesc.GetBaseStructure(), destMemberDesc.GetObjAddr(destStructAddr), true);
	}
	return true;
}

bool ReadIntFromPso( parMember* destMemberDesc, parPtrToStructure destStructAddr, psoStruct& /*srcStructure*/, psoMember& srcMember ) 
{
	// If the source is an integer, read it into a 64 bit variable (large enough to
	// cover 32 bit signed or unsigned).
	s64 intVal = 0;

	switch(srcMember.GetType().GetEnum())
	{
	case psoType::TYPE_BOOL:	intVal = srcMember.GetDataAs<bool>();	break;
	case psoType::TYPE_S8:		intVal = srcMember.GetDataAs<s8>();		break;
	case psoType::TYPE_U8:		intVal = srcMember.GetDataAs<u8>();		break;
	case psoType::TYPE_S16:		intVal = srcMember.GetDataAs<s16>();	break;
	case psoType::TYPE_U16:		intVal = srcMember.GetDataAs<u16>();	break;
	case psoType::TYPE_S32:		intVal = srcMember.GetDataAs<s32>();	break;
	case psoType::TYPE_U32:		intVal = srcMember.GetDataAs<u32>();	break;
	case psoType::TYPE_S64:		intVal = srcMember.GetDataAs<s64>();	break;
	case psoType::TYPE_U64:		intVal = (s64)srcMember.GetDataAs<u64>();	break; // Note the dodgy cast here. Large u64s become negative intVals.
	default:
		return false; // no conversion possible
	}
	
	parPtrToMember destMemberAddress = destMemberDesc->GetPointerToMember(destStructAddr);

	ASSERT_ONLY(s64 destVal = 0);

	switch(destMemberDesc->GetType())
	{
	case parMemberType::TYPE_BOOL:
		{
			bool tmp = (intVal != 0);
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_CHAR:
		{
			s8 tmp = (s8)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_UCHAR:
		{
			u8 tmp = (u8)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_SHORT:
		{
			s16 tmp = (s16)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_USHORT:
		{
			u16 tmp = (u16)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_INT:
		{
			s32 tmp = (s32)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_UINT:
		{
			u32 tmp = (u32)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_SIZET:
		{
			size_t tmp = (size_t)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_PTRDIFFT:
		{
			ptrdiff_t tmp = (ptrdiff_t)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_INT64:
		{
			s64 tmp = (s64)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	case parMemberType::TYPE_UINT64:
		{
			u64 tmp = (u64)intVal;
			CopyIntoBuffer(destMemberAddress, tmp);
			ASSERT_ONLY(destVal = tmp);
		}
		break;
	default:
		return false;
	}

	Assertf(intVal == destVal, "Input value %" I64FMT "d (type %s) was truncated to %" I64FMT "d (parser type %d)", intVal, srcMember.GetType().GetName(), destVal, destMemberDesc->GetType());
	return true;
}


void ReadParMemberFromPso( parMember* destMemberDesc, parPtrToStructure destStructAddr, psoStruct& srcStructure, psoMember& srcMember ) 
{
	bool converted = false;

	parPtrToMember destMemberAddress = destMemberDesc->GetPointerToMember(destStructAddr);

	// Do all the simple types without calling any virtual functions. Allow some type conversions too
	switch(destMemberDesc->GetType())
	{
	case parMemberType::TYPE_BOOL:
	case parMemberType::TYPE_CHAR:
	case parMemberType::TYPE_UCHAR:
	case parMemberType::TYPE_SHORT:
	case parMemberType::TYPE_USHORT:
	case parMemberType::TYPE_INT:
	case parMemberType::TYPE_UINT:
	case parMemberType::TYPE_PTRDIFFT:
	case parMemberType::TYPE_SIZET:
	case parMemberType::TYPE_INT64:
	case parMemberType::TYPE_UINT64:
		converted = ReadIntFromPso(destMemberDesc, destStructAddr, srcStructure, srcMember);
		break;

	case parMemberType::TYPE_FLOAT:
		SIMPLE_CONVERSION(psoType::TYPE_FLOAT, float);
		break;
	case parMemberType::TYPE_FLOAT16:
		SIMPLE_CONVERSION(psoType::TYPE_FLOAT16, Float16);
		break;
	case parMemberType::TYPE_DOUBLE:
		SIMPLE_CONVERSION(psoType::TYPE_DOUBLE, double);
		break;
	case parMemberType::TYPE_VECTOR2:
		SIMPLE_CONVERSION(psoType::TYPE_VEC2, Vector2);
		break;
	case parMemberType::TYPE_VECTOR3:
		SIMPLE_CONVERSION(psoType::TYPE_VEC3V, Vector3);
		break;
	case parMemberType::TYPE_VECTOR4:
		SIMPLE_CONVERSION(psoType::TYPE_VEC4V, Vector4);
		break;
	case parMemberType::TYPE_STRING:
		converted = ReadStringFromPso(destMemberDesc->AsStringRef(), destStructAddr, srcMember);
		break;

	case parMemberType::TYPE_ARRAY:
		if (srcMember.GetType().IsArray()) 
		{
			psoMemberArrayInterface srcArrayMember(srcStructure, srcMember);
			converted = ReadArrayFromPso(destMemberDesc->AsArrayRef(), destStructAddr, srcStructure, srcArrayMember, srcMember);
		}
		break;

	case parMemberType::TYPE_STRUCT:
		if (srcMember.GetType().HasSubStructure())
		{
			converted = ReadStructMemberFromPso(destMemberDesc->AsStructOrPointerRef(), destStructAddr, srcMember);
		}
		break;

	case parMemberType::TYPE_ENUM:
		if (srcMember.GetType().IsEnum())
		{
			converted = ReadEnumFromPso(destMemberDesc->AsEnumRef(), destStructAddr, srcMember);
		}
		break;

	case parMemberType::TYPE_BITSET:
		if (srcMember.GetType().IsBitset())
		{
			converted = ReadBitsetFromPso(destMemberDesc->AsBitsetRef(), destStructAddr, srcStructure, srcMember);
		}
		break;

    case parMemberType::TYPE_MAP:
		if (srcMember.GetType().IsMap())
		{
			psoMemberMapInterface srcMapMember(srcStructure, srcMember);
	        converted = ReadMapFromPso(destMemberDesc->AsMapRef(), destStructAddr, srcStructure, srcMapMember);
		}
        break;

	case parMemberType::TYPE_MATRIX34:
		SIMPLE_CONVERSION(psoType::TYPE_MAT34V, Matrix34);
		break;
	case parMemberType::TYPE_MATRIX44:
		SIMPLE_CONVERSION(psoType::TYPE_MAT44V, Matrix44);
		break;
	case parMemberType::TYPE_VEC2V:
		SIMPLE_CONVERSION(psoType::TYPE_VEC2V, Vec2V);
		break;
	case parMemberType::TYPE_VEC3V:
		SIMPLE_CONVERSION(psoType::TYPE_VEC3V, Vec3V);
		break;
	case parMemberType::TYPE_VEC4V:
		SIMPLE_CONVERSION(psoType::TYPE_VEC4V, Vec4V);
		break;
	case parMemberType::TYPE_MAT33V:
		SIMPLE_CONVERSION(psoType::TYPE_MAT33V, Mat33V);
		break;
	case parMemberType::TYPE_MAT34V:
		SIMPLE_CONVERSION(psoType::TYPE_MAT34V, Mat34V);
		break;
	case parMemberType::TYPE_MAT44V:
		SIMPLE_CONVERSION(psoType::TYPE_MAT44V, Mat44V);
		break;
	case parMemberType::TYPE_SCALARV:
		SIMPLE_CONVERSION(psoType::TYPE_SCALARV, ScalarV);
		break;
	case parMemberType::TYPE_BOOLV:
		SIMPLE_CONVERSION(psoType::TYPE_BOOLV, BoolV);
		break;
	case parMemberType::TYPE_VECBOOLV:
		SIMPLE_CONVERSION(psoType::TYPE_VECBOOLV, VecBoolV);
		break;
	case parMemberType::INVALID_TYPE:
		parErrorf("Invalid type %d", destMemberDesc->GetType());
		break;
	}

	if (!converted)
	{
#if !__NO_OUTPUT
		static int spewCount = 0;
		spewCount++;
		if (spewCount < 50)
		{
			parErrorf("Couldn't convert a PSO value %s::%s from type %s (%d) into a runtime type %d,%d. Member hash 0x%08x::0x%08x", 
				atLiteralHashString::TryGetString(srcStructure.GetSchema().GetNameHash().GetHash()), 
				atLiteralHashString::TryGetString(srcMember.GetSchema().GetNameHash().GetHash()),
				srcMember.GetType().GetName(), srcMember.GetType().GetRaw(), 
				destMemberDesc->GetType(), destMemberDesc->GetCommonData()->m_Subtype,
				srcStructure.GetSchema().GetNameHash().GetHash(),
				srcMember.GetSchema().GetNameHash().GetHash());
		}
		else if (spewCount == 0)
		{
			parErrorf("Too much type conversion spew, stopping");
		}
#endif
	}
}

#undef SIMPLE_CONVERSION

} // namespace psoFileReader

// This is basically doing what a visitor does, but I'm not using a visitor here to try and avoid virtual
// function calls. Loading PSOs is intended to be low overhead and adding one or two virtual calls per member
// variable would be more overhead than I want.
bool rage::psoLoadFromStructure(psoStruct& srcStructure, parStructure& destStructDesc, parPtrToStructure destObjAddr, bool verifyTypes /* = false */)
{
	using namespace parMemberType;

	if (!destStructDesc.internal_CheckVersionForLoad(srcStructure.GetSchema().GetVersion()))
	{
		return false;
	}

	if (verifyTypes && !PARSER.Settings().GetFlag(parSettings::LOAD_UNKNOWN_TYPES))
	{
		// Make sure that srcStructure's schema corresponds to an existing type in the parser _and_ that we can 
		// load data from that type into an instance of destStructureDesc
		parStructure* srcParStructure = PARSER.FindStructure(srcStructure.GetSchema().GetNameHash());

		if (!srcParStructure)
		{
			parErrorf("Couldn't find an existing structure with hash %x in the parser", srcStructure.GetSchema().GetNameHash().GetHash());
			return false;
		}
		else if (!destStructDesc.IsSubclassOf(srcParStructure))
		{
			parErrorf("Can't load file type %s into an object of type %s", srcParStructure->GetName(), destStructDesc.GetName());
			return false;
		}
	}

	// make an array of all base types, traverse the array in backwards order.
	parStructure::BaseList baseInfo;
	baseInfo.Init(&destStructDesc, destObjAddr);

	for(int baseNum = baseInfo.GetNumBases()-1; baseNum >= 0; baseNum--) {
		parStructure* currStruct = baseInfo.GetBaseStructure(baseNum);
		parPtrToStructure currDestStructAddr = baseInfo.GetBaseAddress(baseNum);

		// currStruct is the current structure we're trying to load.
		// Loop through each one of its members. For each member, loop through
		// each node in the linked list (starting at one past the previous node)

		for(int memIdx = 0; memIdx < currStruct->GetNumMembers(); memIdx++)
		{
			parMember* currDestMemberDesc = currStruct->GetMember(memIdx);
			bool foundMemberData = false;

			psoMember srcMember = srcStructure[atLiteralHashValue(currDestMemberDesc->GetNameHash())]; // This could be a slow lookup. If speed is an issue try to go through the member list in order.

			if (srcMember.IsValid())
			{
				psoFileReader::ReadParMemberFromPso(currDestMemberDesc, currDestStructAddr, srcStructure, srcMember);
			}

			if (!foundMemberData && PARSER.Settings().GetFlag(parSettings::WARN_ON_MISSING_DATA))
			{
				parWarningf("No data found for member %s in structure %s", currDestMemberDesc->GetName(), currStruct->GetName());
			}
		}
	}

	return true;
}

#endif // PSO_USING_NATIVE_BIT_SIZE

